


# clear the queues
&((Split-Path $MyInvocation.InvocationName) + '\clear-all-queues.ps1')

# start the processors/api
start-process "cmd.exe" '/c "C:\Program Files\IIS Express\iisexpress" /config:C:\Repos\visitpay\Source\.vs\config\applicationhost.config /site:Ivh.Web.ClientDataApi'
start-process "cmd.exe" '/c "C:\Program Files\IIS Express\iisexpress" /config:C:\Repos\visitpay\Source\.vs\config\applicationhost.config /site:Ivh.Web.Patient'
start-process "cmd.exe" '/c "C:\Program Files\IIS Express\iisexpress" /config:C:\Repos\visitpay\Source\.vs\config\applicationhost.config /site:Ivh.Web.Client'
start-process "cmd.exe" '/c "C:\Program Files\IIS Express\iisexpress" /config:C:\Repos\visitpay\Source\.vs\config\applicationhost.config /site:Ivh.Web.GuestPay'
start-process "cmd.exe" '/c "C:\Program Files\IIS Express\iisexpress" /config:C:\Repos\visitpay\Source\.vs\config\applicationhost.config /site:Ivh.Web.QATools'
start-process "cmd.exe" "/c C:\Repos\visitpay\Source\Presentation\Console\Ivh.Console.UniversalProcessor\bin\Debug\Ivh.Console.UniversalProcessor.exe"
start-process "cmd.exe" "/c C:\Repos\visitpay\Source\Presentation\Console\Ivh.Console.ScoringProcessor\bin\Debug\Ivh.Console.ScoringProcessor.exe"

# Example of how to run job runner.  Someone could write a script to take the JobRunnerTypeEnum as an input paramter if they want.
#start-process "cmd.exe" "/c C:\Repos\visitpay\Source\Presentation\Console\Ivh.Console.JobRunner\bin\Debug\Ivh.Console.JobRunner.exe" Inbound_ScoringAndSegmentation
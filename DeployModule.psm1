#region Stop-WindowsServicefunction Stop-WindowsService($serviceNameInd) {    Write-Output "Stopping $serviceNameInd..."    $serviceInstance = Get-Service $serviceNameInd -ErrorAction SilentlyContinue    if ($serviceInstance -ne $null) {        Stop-Service $serviceNameInd -Force -ErrorAction SilentlyContinue        Try {  # For some reason, we cannot use -ErrorAction after the next statement:            $serviceInstance.WaitForStatus('Stopped', '00:01:00')        } Catch {            Write-host "After waiting for 1 minute, $serviceNameInd failed to stop."        }        Write-Output "Service $serviceNameInd stopped."    } else {        Write-Output "The $serviceName service could not be located."    }}function Stop-WindowsService2nd($serviceNameInd) {    #Second way, this will ensure all processes are dead    $serviceInstance = Get-Service $serviceNameInd -ErrorAction SilentlyContinue    if ($serviceInstance -ne $null) {        try        {            $result = & "sc.exe" "queryex",  "$serviceNameInd"            $result                        foreach($item in $result | Where-Object { $_ -like '*PID*'})            {                Write-Output "The Item: $item"                $local = $item -replace "PID", "" -replace ":", ""                $local = $local.Trim()                            if($local -ne "0")                {                    Write-Output "Found a PID that could block"                    & "taskkill" "/pid", "$local", "/f"                }                else                {                    Write-Output "PID was 0, not killing anything"                }            }        }        catch        {            Write-Output "Something threw, but this is the secondary method and were ignoring this error."        }    }}function Stop-WindowsServices([string[]]$ServiceNames){        $separator = ","    $option = [System.StringSplitOptions]::RemoveEmptyEntries    $AllServices = $ServiceNames.Split($separator, $option)        Write-Output "Killing all MMC Services windows"    #kill any mmc windows that are looking at services, this can cause issues when trying to delete services.    $killServicesMmc = Get-Process | Where-Object { $_.ProcessName -eq 'mmc' -and $_.MainWindowTitle -eq 'Services'}     foreach($proc in $killServicesMmc)    {        Stop-Process -id $proc.Id -Force    }        Write-Output "Service Names: $ServiceNames"        foreach ($serviceNameInd in $AllServices) {        Stop-WindowsService -serviceNameInd $serviceNameInd        Stop-WindowsService2nd -serviceNameInd $serviceNameInd    }}
#endregion

#region Remove-WindowsService

function Remove-WindowsService($serviceNameInd) {
    $TheService = Get-Service $serviceNameInd -ErrorAction SilentlyContinue
    if ($TheService)
    {
        Write-Host "Windows Service ""$serviceNameInd"" found, removing service."
        if ($TheService.Status -eq "Running")
        {
            Write-Host "Stopping $serviceNameInd ..."
            $TheService.Stop()
        }
        sc.exe delete $TheService
        Write-Host "Service ""$serviceNameInd"" removed."
    }
    else
    {
        Write-Host "Windows Service ""$serviceNameInd"" not found."
    }
}

function Remove-WindowsServices([string[]] $ServiceNames)
{
    $separator = ","
    $option = [System.StringSplitOptions]::RemoveEmptyEntries
    $AllServices = $ServiceNames.Split($separator, $option)
    
    Write-Output "Service Names: $ServiceNames"
    
    foreach ($serviceNameInd in $AllServices) {
        Remove-WindowsService -serviceNameInd $serviceNameInd
    }
}

#endregion

#region InstallTopShelfService

function Install-TopShelfService
{
param(
    [parameter(Mandatory=$true)][string]$Exe_Directory_Processor, 
    [parameter(Mandatory=$false)][string]$SecondaryExe_Directory_Processor
)
    #Look for File with .exe extension 
    #Execute it with {uninstall, install, start}
    $fileExtentionToLookForRootName = ".exe"
    
    
    Function GetListOfExeFiles ($pDir)
    {
    	$DirObj = get-childitem $pDir
    	$InternalList = @($DirObj | where { $_.extension -eq $fileExtentionToLookForRootName }) 
    	$InternalList
    }

    Write-Host "First dir checked! $Exe_Directory_Processor"
    $RawList = GetListOfExeFiles $Exe_Directory_Processor
    $RawList | format-table name
    
    if($RawList.length -lt 1 -and ![String]::IsNullOrWhiteSpace($SecondaryExe_Directory_Processor)){
    	$Exe_Directory_Processor = $SecondaryExe_Directory_Processor
    	#$Exe_Directory_Processor = "C:\repos\visitpay\Source\Presentation\Console\Ivh.Console.HsDataProcessor\bin\Debug"
    	Write-Host "Second dir checked! $Exe_Directory_Processor"
    	$RawList = GetListOfExeFiles $Exe_Directory_Processor
    	$RawList | format-table name
    }
    Write-Host "List"
    $List = @()
    for($i=0; $i -lt $RawList.length; $i++)
    {
    	$NameToCheck = $RawList[$i].Name
    	Write-Host "$NameToCheck"
    	if ($NameToCheck -NotMatch "vshost")
    	{
    		$List += @($RawList[$i])
    	}
    }
    
    $len = $List.length
    
    Write-Host "$len"
    
    if($List.length -lt 1) {
    	throw "Didn't find rootFile with base extension of $fileExtentionToLookForRootName" 
    }
    if($List.length -gt 1) {
    	throw "Found too many rootFiles with base extension of $fileExtentionToLookForRootName" 
    }
    $rootItemName = $List[0].Name
    
    Write-Host $rootItemName
    
    $fileWithPath = $Exe_Directory_Processor +'\'+ $rootItemName
    
    & $fileWithPath uninstall
    if(![String]::IsNullOrWhitespace($serviceUsername) -and ![String]::IsNullOrWhitespace($servicePassword)){
        & $fileWithPath install -username $serviceUsername -password $servicePassword
    }
    else
    {
        & $fileWithPath install
    }
    & $fileWithPath install
}

#endregion

#region SetRecovery

function Set-Recovery{
param(
    [parameter(Mandatory=$true)][string]$ServiceName
)
    & "sc.exe" failure $ServiceName reset= 3600 actions= restart/5000/restart/60000//1000
#reset period is in seconds
#Action time is in milliseconds
#https://docs.microsoft.com/en-us/previous-versions/windows/it-pro/windows-xp/bb490995(v=technet.10)
}

#endregion

#region ConfigEncrypt

function Protect-EncryptConfig
{
param(
    [parameter(Mandatory=$true)][string]$websiteDirectory,
    [parameter(Mandatory=$true)][string]$sectionToEncrypt,
    [parameter(Mandatory=$false)][string]$provider,
    [parameter(Mandatory=$false)][string]$configFile = "web.config",
    [parameter(Mandatory=$false)][string[]]$otherFiles
)
    $ErrorActionPreference = "Stop" 
    
    function HandleError($message) {
    	if (!$whatIf) {
    		throw $message
    	} else {
    		Write-Host $message -Foreground Yellow
    	}
    }
    
    Write-Host "Configuration - Encrypt .config"
    Write-Host "WebsiteDirectory: $websiteDirectory"
    Write-Host "SectionToEncrypt: $sectionToEncrypt"
    Write-Host "Provider: $provider"
    Write-Host "ConfigFile: $configFile"
    
    
    if (!(Test-Path $websiteDirectory)) {
    	HandleError "The directory $websiteDirectory must exist"
    }
    
    $configFilePath = Join-Path $websiteDirectory $configFile
    Write-Host "configFilePath: $configFilePath"
    if (!(Test-Path $configFilePath)) {
    	HandleError "Specified file $configFile or a Web.Config file must exist in the directory $websiteDirectory"
    }
    
    $frameworkPath = [System.Runtime.InteropServices.RuntimeEnvironment]::GetRuntimeDirectory();
    $regiis = "$frameworkPath\aspnet_regiis.exe"
    
    if (!(Test-Path $regiis)) {
    	HandleError "The tool aspnet_regiis does not exist in the directory $frameworkPath"
    }
    
    # Create a temp directory to work out of and copy our config file to web.config
    $tempPath = Join-Path $websiteDirectory $([guid]::NewGuid()).ToString()
    if (!$whatIf) {
    	New-Item $tempPath -ItemType "directory"
    } else {
    	Write-Host "WhatIf: New-Item $tempPath -ItemType ""directory""" -Foreground Yellow
    }
    
    $tempFile = Join-Path $tempPath "web.config"
    if (!$whatIf) {
    	Copy-Item $configFilePath $tempFile
    } else {
    	Write-Host "WhatIf: Copy-Item $configFilePath $tempFile" -Foreground Yellow
    }
    
    Foreach($fileName in $otherFiles){
      if (!$whatIf) {
    	 Copy-Item (Join-Path $websiteDirectory $fileName) (Join-Path $tempPath $fileName)
      } else {
    	 Write-Host "WhatIf: Copy-Item $configFilePath $tempFile" -Foreground Yellow
      }
    }
    
    # Determine arguments
    if ($provider) {
    	$args = "-pef", $sectionToEncrypt, $tempPath, "-prov", $provider
    } else {
    	$args = "-pef", $sectionToEncrypt, $tempPath
    }
    
    # Encrypt Web.Config file in directory
    if (!$whatIf) {
    	& $regiis $args
    } else {
    	Write-Host "WhatIf: $regiis $args" -Foreground Yellow
    }
    
    # Copy the web.config back to original file and delete the temp dir
    if (!$whatIf) {
    	Copy-Item $tempFile $configFilePath -Force
    
      Foreach($fileName in $otherFiles){
        if (!$whatIf) {
      	 Copy-Item (Join-Path $tempPath $fileName) (Join-Path $websiteDirectory $fileName) -Force
        } else {
      	 Write-Host "WhatIf: Copy-Item $configFilePath $tempFile" -Foreground Yellow
        }
      }
    
    	Remove-Item $tempPath -Recurse
    } else {
    	Write-Host "WhatIf: Copy-Item $tempFile $configFilePath -Force" -Foreground Yellow
    	Write-Host "WhatIf: Remove-Item $tempPath -Recurse" -Foreground Yellow
    }

}

#endregion


param(
    [Parameter(Mandatory=$false)] [switch]$KeepQueues = $false
)

# clear the queues
if(!$KeepQueues)
{
    &('C:\Repos\visitpay\Source\clear-all-queues.ps1')
}

# set the paths... 
$RepoSourcePath = 'C:\Repos\visitpay\Source\'
$DebugUniversalPath  = 'Presentation\Console\Ivh.Console.UniversalProcessor\bin\Debug\'
$PlatformPath = 'net461\'
$ExeName = 'Ivh.Console.UniversalProcessor.exe'

# Non-platform specific
$DebugPath = $RepoSourcePath + $DebugUniversalPath + $ExeName
# Platform specific
$DebugPathPlatformSpecific = $RepoSourcePath + $DebugUniversalPath + $PlatformPath  + $ExeName

if([System.IO.File]::Exists($DebugPath)) {
start-process $DebugPath
} else {
start-process $DebugPathPlatformSpecific 
}

# start the processor
#"/c C:\Repos\visitpay\Source\Presentation\Console\Ivh.Console.UniversalProcessor\bin\Debug\net461\Ivh.Console.UniversalProcessor.exe"

﻿namespace Ivh.Common.VisitPay.Attributes
{
    using System;
    using Enums;

    [AttributeUsage(AttributeTargets.Field)]
    public class DatumLocationAttribute : AttributeBase
    {
      public DatumLocationEnum DatumLocation { get; private set; }

        public DatumLocationAttribute(DatumLocationEnum datumLocation)
        {
            this.DatumLocation = datumLocation;
        }
    }
}

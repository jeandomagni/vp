﻿namespace Ivh.Common.VisitPay.Attributes
{
    using System;

    [AttributeUsage(AttributeTargets.Property)]
    public abstract class AttributeBase : Attribute
    {
        public string Name { get; protected set; }
        public int Order { get; protected set; }
    }
}

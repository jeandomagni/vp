﻿namespace Ivh.Common.VisitPay.Attributes
{
    using System;
    
    [AttributeUsage(AttributeTargets.Property)]
    public sealed class MaskAttribute : Attribute
    {
    }
}

﻿namespace Ivh.Common.VisitPay.Attributes
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;

    public class RedactFieldAttribute : Attribute
    {
        private int? MaxLength { get; set; }
        public RedactFieldAttribute()
        {
        }

        public RedactFieldAttribute(int maxLength)
        {
            this.MaxLength = maxLength;
        }

        public static void RedactFields(object fieldsObject, string replacementValue)
        {
            Type attributeType = typeof (RedactFieldAttribute);
            Type objectType = fieldsObject.GetType();

            IEnumerable<PropertyInfo> propertyInfos = objectType.GetProperties(BindingFlags.Public | BindingFlags.Instance)
                .Where(x => IsDefined(x, attributeType));

            foreach (PropertyInfo propertyInfo in propertyInfos)
            {
                RedactFieldAttribute obfuscateFieldAttribute = propertyInfo.GetCustomAttributes<RedactFieldAttribute>().First();
                propertyInfo.SetValue(fieldsObject, obfuscateFieldAttribute.MaxLength.HasValue ? replacementValue.Substring(0, Math.Min(obfuscateFieldAttribute.MaxLength.Value, replacementValue.Length)) : replacementValue);
            }
        }
    }
}
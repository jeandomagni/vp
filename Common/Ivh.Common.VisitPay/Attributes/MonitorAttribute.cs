﻿namespace Ivh.Common.VisitPay.Attributes
{
    using System;
    using Enums;

    [AttributeUsage(AttributeTargets.Class)]
    public class MonitorAttribute : Attribute
    {
        public MonitorEnum Monitor { get; private set; }

        public MonitorAttribute(MonitorEnum monitor)
        {
            this.Monitor = monitor;
        }
    }
}

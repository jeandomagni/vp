﻿namespace Ivh.Common.VisitPay.Attributes
{
    using System;

    public class ScoringStatisticalMonitoringMetricAttribute : Attribute
    {
        public string MetricName { get; }
        public string IsACount { get; }
        public string MinValue { get; }
        public string MaxValue { get; }

        public ScoringStatisticalMonitoringMetricAttribute(string metricName, string isACount, string minValue, string maxValue)
        {
            this.MetricName = metricName;
            this.IsACount = isACount;
            this.MinValue = minValue;
            this.MaxValue = maxValue;
        }

    }
}

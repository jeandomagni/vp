﻿namespace Ivh.Common.VisitPay.Attributes
{
    using System;
    using Enums;

    [AttributeUsage(AttributeTargets.Property)]
    public class ParameterAttribute : AttributeBase
    {
        public ParameterEnum Parameter { get; private set; }

        public ParameterAttribute(ParameterEnum parameter)
        {
            this.Parameter = parameter;
            this.Name = this.Parameter.ToString();
        }
    }
}

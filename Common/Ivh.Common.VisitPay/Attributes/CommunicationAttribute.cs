﻿namespace Ivh.Common.VisitPay.Attributes
{
    using System;
    using Enums;

    public class CommunicationAttribute : Attribute
    {
        public bool SendEmail { get; set; } = true;
        
        public bool SendSms { get; set; } = false;

        public bool SendMail { get; set; } = false;

        public bool SendManaging { get; set; } = false;

        public bool SendToVpccUser { get; set; } = false;

        public bool HideLogin { get; set; } = false;

        public bool SuppressOnRegistrationDay { get; set; } = false;

        public NotificationFrequencyEnum Frequency { get; set; } = NotificationFrequencyEnum.None;
    }
}
﻿namespace Ivh.Common.VisitPay.Attributes
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;

    public class GuarantorMatchField : Attribute
    {
        public string Name { get; private set; }
        public string Format { get; private set; }
        public string UiDisplayName { get; private set; }
        public int UiDisplayOrder { get; private set; }

        public GuarantorMatchField(string name, string format = null, string uiDisplayName = null, int uiDisplayOrder = 100)
        {
            this.Name = name;
            this.Format = format;
            this.UiDisplayName = string.IsNullOrEmpty(uiDisplayName) ? name : uiDisplayName;
            this.UiDisplayOrder = uiDisplayOrder;
        }

        public static IDictionary<string, string> GetGuantorMatchFields(object fieldsObject, IList<string> matchFields)
        {
            Type attributeType = typeof(GuarantorMatchField);
            Type objectType = fieldsObject.GetType();

            return objectType.GetProperties(BindingFlags.Public | BindingFlags.Instance)
                .Where(x => Attribute.IsDefined(x, attributeType))
                .Where(x =>
                {
                    GuarantorMatchField guarantorMatchField = (GuarantorMatchField) Attribute.GetCustomAttribute(x, attributeType);
                    return matchFields.Contains(guarantorMatchField.Name, StringComparer.OrdinalIgnoreCase);
                })
                .Select(x =>
                {
                    GuarantorMatchField guarantorMatchField = (GuarantorMatchField) Attribute.GetCustomAttribute(x, attributeType);

                    string matchFieldValueString = string.Empty;
                    object matchFieldValue = x.GetValue(fieldsObject, null);

                    if (matchFieldValue != null)
                    {
                        if (matchFieldValue is DateTime?)
                        {
                            matchFieldValue = (DateTime?) matchFieldValue ?? default(DateTime);
                        }

                        if (matchFieldValue is DateTime)
                        {
                            matchFieldValueString = ((DateTime) matchFieldValue).ToString(guarantorMatchField.Format ?? VisitPay.Strings.Format.DateFormat);
                        }

                        if (matchFieldValue is DateTime?)
                        {
                            matchFieldValue = (DateTime?) matchFieldValue ?? default(DateTime);
                        }

                        if (matchFieldValue is DateTime)
                        {
                            matchFieldValueString = ((DateTime) matchFieldValue).ToString(guarantorMatchField.Format ?? VisitPay.Strings.Format.DateFormat);
                        }

                        if (!(matchFieldValue is DateTime
                              || matchFieldValue is DateTime))
                        {
                            matchFieldValueString = matchFieldValue.ToString();
                        }
                    }

                    return new
                    {
                        MatchFieldName = guarantorMatchField.Name,
                        MatchFieldValue = matchFieldValueString
                    };
                })
                .ToDictionary(k => k.MatchFieldName, v => v.MatchFieldValue, StringComparer.OrdinalIgnoreCase);
        }

        public static IDictionary<string, string> GetGuarantorMatchFieldsForDisplay(object fieldsObject, IList<string> matchFields)
        {
            Type attributeType = typeof (GuarantorMatchField);
            Type objectType = fieldsObject.GetType();

            return objectType.GetProperties(BindingFlags.Public | BindingFlags.Instance)
                .Where(x => IsDefined(x, attributeType))
                .Where(x =>
                {
                    GuarantorMatchField guarantorMatchField = (GuarantorMatchField) GetCustomAttribute(x, attributeType);
                    return matchFields.Contains(guarantorMatchField.Name, StringComparer.OrdinalIgnoreCase);
                })
                .Select(x =>
                {
                    GuarantorMatchField guarantorMatchField = (GuarantorMatchField) GetCustomAttribute(x, attributeType);

                    return guarantorMatchField;
                })
                .OrderBy(x => x.UiDisplayOrder)
                .ToDictionary(k => k.Name, v => v.UiDisplayName);
        }

        public static bool AreEqual(object first, object second, IList<string> matchFields)
        {
            IDictionary<string, string> fieldValues1 = GetGuantorMatchFields(first, matchFields);
            IDictionary<string, string> fieldValues2 = GetGuantorMatchFields(second, matchFields);

            return fieldValues1.SequenceEqual(fieldValues2);
        }
    }
}
﻿namespace Ivh.Common.VisitPay.Attributes
{
    using System;
    using System.Linq;

    [AttributeUsage(AttributeTargets.Field)]
    public class EnumCategoryAttribute : Attribute
    {
        public string[] Categories { get; private set; }
        public EnumCategoryAttribute(string category)
        {
            this.Categories = new string[] { category };
        }
        public EnumCategoryAttribute(params string[] categories)
        {
            this.Categories = categories.Distinct().ToArray();
        }
    }
}

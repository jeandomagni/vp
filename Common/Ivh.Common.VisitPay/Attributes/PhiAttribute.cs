﻿namespace Ivh.Common.VisitPay.Attributes
{
    using System;

    /// <inheritdoc />
    /// <summary>
    ///     Indicates that data assigned to the decorated property
    ///     is PHI (Protected Health Information).
    /// </summary>
    /// <remarks>
    ///     In the future, we may reflect off this attribute
    ///     to monitor and/or prohibit transmission of PHI data.
    /// </remarks>
    public class PhiAttribute : Attribute
    {
    }
}
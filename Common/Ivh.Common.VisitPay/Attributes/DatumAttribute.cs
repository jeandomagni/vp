﻿namespace Ivh.Common.VisitPay.Attributes
{
    using System;
    using Enums;

    [AttributeUsage(AttributeTargets.Property)]
    public class DatumAttribute : AttributeBase
    {
        public DatumEnum Datum { get; private set; }

        public DatumAttribute(DatumEnum datum, int order = 0)
        {
            this.Datum = datum;
            this.Name = this.Datum.ToString();
            this.Order = order;
        }
    }
}

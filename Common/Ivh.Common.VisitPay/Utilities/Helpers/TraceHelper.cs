﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Diagnostics;
using System.Runtime.CompilerServices;

namespace Ivh.Common.VisitPay.Utilities.Helpers
{
    using System.Reflection;

    public static class TraceHelper
    {
        public static void TraceThis(
            string message,
            [CallerMemberName] string memberName = "",
            [CallerFilePath] string sourceFilePath = "",
            [CallerLineNumber] int sourceLineNumber = 0)
        {
#if DEBUG
            Trace.WriteLine("time: " + DateTime.UtcNow);
            Trace.WriteLine("message: " + message);
            Trace.WriteLine("member name: " + memberName);
            Trace.WriteLine("source file path: " + sourceFilePath);
            Trace.WriteLine("source line number: " + sourceLineNumber);
            Trace.WriteLine("v--------v");
#endif
        }

        public static void TraceMe(MethodBase method, params object[] values)
        {
            ParameterInfo[] parameters = method.GetParameters();
            object[] nameValues = new object[2 * parameters.Length];

            string msg = method.Name + "(";
            for (int i = 0, j = 0; i < parameters.Length; i++, j += 2)
            {
                msg += "{" + j + "}={" + (j + 1) + "}, ";
                nameValues[j] = parameters[i].Name;
                if (i < values.Length)
                {
                    nameValues[j + 1] = values[i].ToString();
                }               
            }
            Trace.WriteLine(string.Format(msg, nameValues));
        }
    }
}

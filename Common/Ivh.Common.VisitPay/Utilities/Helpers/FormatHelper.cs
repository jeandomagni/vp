﻿namespace Ivh.Common.VisitPay.Utilities.Helpers
{
    using System.Text.RegularExpressions;
    using Extensions;
    using VisitPay.Constants;
    using VisitPay.Enums;
    using VisitPay.Strings;

    public static class FormatHelper
    {
        /// <summary>
        /// Returns formatted name like "John Doe"
        /// </summary>
        /// <param name="firstName"></param>
        /// <param name="lastName"></param>
        /// <returns></returns>
        public static string FirstNameLastName(string firstName, string lastName)
        {
            return string.Format(Format.FirstNameLastNamePattern, firstName, lastName);
        }

        /// <summary>
        /// Returns formatted name like "Doe, John"
        /// </summary>
        /// <param name="lastName"></param>
        /// <param name="firstName"></param>
        /// <returns></returns>
        public static string LastNameFirstName(string lastName, string firstName)
        {
            return string.Format(Format.LastNameFirstNamePattern, lastName, firstName);
        }

        /// <summary>
        /// Returns formatted name like "John Q. Public"
        /// </summary>
        /// <param name="firstName"></param>
        /// <param name="middleName">Null or white space will be omitted in the string returned</param>
        /// <param name="lastName"></param>
        /// <returns></returns>
        public static string FullNameMiddleInitial(string firstName, string middleName, string lastName)
        {
            string middleInitial = !string.IsNullOrWhiteSpace(middleName) ? string.Concat(middleName.ToUpper().Substring(0, 1), ".") : "";
            return string.Format(Format.FullNamePattern, firstName, middleInitial, lastName).Replace("  ", " ");
        }

        /// <summary>
        /// Returns formatted name like "John Quincy Adams"
        /// </summary>
        /// <param name="firstName"></param>
        /// <param name="middleName">Null or white space will be omitted in the string returned</param>
        /// <param name="lastName"></param>
        /// <returns></returns>
        public static string FullName(string firstName, string middleName, string lastName)
        {
            string middle = !string.IsNullOrWhiteSpace(middleName) ? middleName : "";
            return string.Format(Format.FullNamePattern, firstName, middle, lastName).Replace("  ", " ");
        }

        /// <summary>
        /// Returns formatted address like "City, State 12345", or an empty string if any parameter is null or empty
        /// </summary>
        /// <param name="city"></param>
        /// <param name="state"></param>
        /// <param name="zip"></param>
        /// <returns></returns>
        public static string CityStateZip(string city, string state, string zip)
        {
            if (city.IsNullOrEmpty() || state.IsNullOrEmpty() || zip.IsNullOrEmpty())
            {
                return "";
            }

            return $"{city.Trim()}, {state.Trim()} {zip.FormatAsZip()}";
        }

        /// <summary>
        /// Returns formatted url like websitename.net
        /// </summary>
        /// <param name="fullUrl"></param>
        /// <returns></returns>
        public static string DisplayUrl(string fullUrl)
        {
            if (fullUrl.IsNullOrEmpty())
            {
                return string.Empty;
            }

            Regex urlRegex = new Regex(@"https?://(www.)?");

            return urlRegex.Replace(fullUrl, string.Empty, 1, 0).TrimEnd('/');
        }

        public static string PaymentAllocationTypeDisplay(PaymentAllocationTypeEnum paymentAllocationType)
        {
            switch (paymentAllocationType)
            {
                case PaymentAllocationTypeEnum.VisitPrincipal:
                    return TextRegionConstants.Principal;
                case PaymentAllocationTypeEnum.VisitInterest:
                    return TextRegionConstants.Interest;
                case PaymentAllocationTypeEnum.VisitDiscount:
                    return TextRegionConstants.Discount;
                case PaymentAllocationTypeEnum.Unknown:
                default:
                    return string.Empty;
            }
        }

        public static string PaymentMethodDisplay(PaymentMethodTypeEnum? paymentMethodType, string lastFour)
        {
            string desc = string.Empty;
            if (paymentMethodType.HasValue)
            {
                desc = paymentMethodType.Value.GetDescription();
            }
            
            if (!string.IsNullOrWhiteSpace(lastFour))
            {
                desc = string.Concat(string.IsNullOrWhiteSpace(desc) ? string.Empty : desc + " ", "X-", lastFour);
            }

            return desc;
        }

        public static string PaymentTypeDisplay(PaymentTypeEnum paymentType)
        {
            switch (paymentType)
            {
                case PaymentTypeEnum.RecurringPaymentFinancePlan:
                    return TextRegionConstants.RecurringFinancePlan;
                case PaymentTypeEnum.PartialRefund:
                    return TextRegionConstants.PartialRefund;
                case PaymentTypeEnum.Refund:
                    return TextRegionConstants.Refund;
                case PaymentTypeEnum.Void:
                    return TextRegionConstants.Void;
                case PaymentTypeEnum.Unknown:
                case PaymentTypeEnum.ManualPromptSpecificAmount:
                case PaymentTypeEnum.ManualPromptSpecificVisits:
                case PaymentTypeEnum.ManualPromptSpecificFinancePlans:
                case PaymentTypeEnum.ManualPromptCurrentBalanceInFull:
                case PaymentTypeEnum.ManualScheduledSpecificAmount:
                case PaymentTypeEnum.ManualScheduledSpecificVisits:
                case PaymentTypeEnum.ManualScheduledSpecificFinancePlans:
                case PaymentTypeEnum.ManualScheduledCurrentBalanceInFull:
                case PaymentTypeEnum.ResubmitPayments:
                default:
                    return TextRegionConstants.Manual;
            }
        }


        /// <summary>
        /// From Twilio format +15554443333 to visit pay format (555) 444-3333      
        /// </summary>
        /// <param name="twilioPhoneNumber"></param>
        /// <returns>Formatted phone number or null if it does not meet the pattern</returns>
        public static string TwilioPhoneNumberToVisitPayUserPhoneNumberFormat(string twilioPhoneNumber)
        {
            bool matchesTwilioPattern = Regex.IsMatch(twilioPhoneNumber, @"^\+\d{11}$");
            if (!matchesTwilioPattern)
            {
                return null;
            }
    
            string number = $"({twilioPhoneNumber.Substring(2, 3)}) {twilioPhoneNumber.Substring(5, 3)}-{twilioPhoneNumber.Substring(8, 4)}";
            return number;
        }

        public static string VisitPayUserPhoneNumberToTwilioPhoneNumberFormat(string visipayPhoneNumber)
        {
            bool matchesTwilioPattern = Regex.IsMatch(visipayPhoneNumber, @"((\(\d{3}\) ?)|(\d{3}-))?\d{3}-\d{4}");
            if (!matchesTwilioPattern)
            {
                return null;
            }
            string twilioNumber = visipayPhoneNumber.Replace("(", "").Replace(")", "").Replace("-","").Replace(" ", "");

            return $"+1{twilioNumber}";
        }

        public static string ConfigurationItemKeyToEnvironmentVariableFormat(string key)
        {
            return key.ToVpEnvironmentVariableFormat();
        }
    }
}

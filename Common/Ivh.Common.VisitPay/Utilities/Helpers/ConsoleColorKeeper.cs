﻿namespace Ivh.Common.VisitPay.Utilities.Helpers
{
    using System;

    internal class ConsoleColorKeeper : IDisposable
    {
        public static ConsoleColorKeeper Keep(ConsoleColor foregroundColor, ConsoleColor backgroundColor)
        {
            ConsoleColorKeeper consoleColorKeeper = new ConsoleColorKeeper(foregroundColor, backgroundColor);
            return consoleColorKeeper;
        }

        public static ConsoleColorKeeper KeepForeground(ConsoleColor foregroundColor)
        {
            ConsoleColorKeeper consoleColorKeeper = new ConsoleColorKeeper(foregroundColor, Console.BackgroundColor);
            return consoleColorKeeper;
        }

        public static ConsoleColorKeeper KeepBackground(ConsoleColor backgroundColor)
        {
            ConsoleColorKeeper consoleColorKeeper = new ConsoleColorKeeper(Console.ForegroundColor, backgroundColor);
            return consoleColorKeeper;
        }

        public ConsoleColor CurrentForegroundColor { get; private set; }
        public ConsoleColor CurrentBackgroundColor { get; private set; }
        
        private ConsoleColorKeeper(ConsoleColor foregroundColor, ConsoleColor backgroundColor)
        {
            this.CurrentForegroundColor = Console.ForegroundColor;
            this.CurrentBackgroundColor = Console.BackgroundColor;
            Console.ForegroundColor = foregroundColor;
            Console.BackgroundColor = backgroundColor;
        }

        public void Dispose()
        {
            Console.ForegroundColor = this.CurrentForegroundColor;
            Console.BackgroundColor = this.CurrentBackgroundColor;
        }
    }
}

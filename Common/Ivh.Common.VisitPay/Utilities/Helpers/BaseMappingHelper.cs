﻿namespace Ivh.Common.VisitPay.Utilities.Helpers
{
    public static class BaseMappingHelper
    {
        public static string MapNullableToString(int? value)
        {
            return value?.ToString();
        }
    }
}
namespace Ivh.Common.VisitPay.Utilities
{
    using System;

    public class CacheObject<T> where T : class
    {
        private readonly TimeSpan _expires;

        public CacheObject(TimeSpan expires)
        {
            this._expires = expires;
        }

        private DateTime? ExpirationDateUtc { get; set; }

        private T Object { get; set; }

        public T Get(Func<T> factory)
        {
            if (DateTime.UtcNow < this.ExpirationDateUtc.GetValueOrDefault(DateTime.MinValue))
            {
                if (this.Object != default(T))
                {
                    return this.Object;
                }
            }

            this.Object = factory();

            if (this.Object != default(T))
            {
                this.ExpirationDateUtc = DateTime.UtcNow.Add(this._expires);
            }

            return this.Object;
        }
    }
}
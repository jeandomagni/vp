﻿namespace Ivh.Common.VisitPay.Utilities.Extensions
{
    using System;
    using System.Globalization;

    public static class ArgsExt
    {
        //todo: would be nice if we defined expected arg position/datatype in jobrunner jobs somewhere
        public static DateTime? TryParseDateArg(this string[] args, int argsIndex)
        {
            if (args.IsNotNullOrEmpty() && args?.Length > argsIndex)
            {
                if (DateTime.TryParse(args[argsIndex], CultureInfo.CurrentCulture, DateTimeStyles.None, out DateTime parsedDateTime))
                {
                    return parsedDateTime;
                }
            }
            return null;
        }

    }
}


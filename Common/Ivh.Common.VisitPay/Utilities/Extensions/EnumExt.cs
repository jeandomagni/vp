﻿namespace Ivh.Common.VisitPay.Utilities.Extensions
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Reflection;
    using Helpers;

    /// <summary>
    /// This is a copy from Ivh.Common.Base. Leave this one as internal
    /// </summary>
    internal static class EnumExt
    {
        public static string GetDescription(this Enum value)
        {
            Type type = value.GetType();
            string name = Enum.GetName(type, value);
            if (name == null)
            {
                return null;
            }

            FieldInfo field = type.GetField(name);
            return field == null
                ? (string)null
                : Attribute.GetCustomAttribute(field, typeof(DescriptionAttribute)) is DescriptionAttribute attr ? attr.Description : null;
        }

        public static bool IsInCategory<TEnum>(this TEnum value, string category) where TEnum : struct, IConvertible
        {
            return EnumHelper<TEnum>.Contains(value, category);
        }

        public static bool IsNotInCategory<TEnum>(this TEnum value, string category) where TEnum : struct, IConvertible
        {
            return !EnumHelper<TEnum>.Contains(value, category);
        }

        public static bool IsInCategory<TEnum>(this TEnum? value, string category) where TEnum : struct, IConvertible
        {
            return value.HasValue && EnumHelper<TEnum>.Contains(value.Value, category);
        }

        public static IEnumerable<TEnum> GetAllItems<TEnum>(this Enum value)
        {
            foreach (object item in Enum.GetValues(typeof(TEnum)))
            {
                yield return (TEnum)item;
            }
        }
        public static IEnumerable<TEnum> GetAllItems<TEnum>() where TEnum : struct
        {
            foreach (object item in Enum.GetValues(typeof(TEnum)))
            {
                yield return (TEnum)item;
            }
        }

        public static IEnumerable<TEnum> GetAllInCategory<TEnum>(string category) where TEnum : struct, IConvertible
        {
            foreach (TEnum theEnum in Enum.GetValues(typeof(TEnum)))
            {
                if (theEnum.IsInCategory<TEnum>(category))
                {
                    yield return (TEnum)theEnum;
                }
            }
        }
    }
}

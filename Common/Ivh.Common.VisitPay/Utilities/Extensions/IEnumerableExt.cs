﻿namespace Ivh.Common.VisitPay.Utilities.Extensions
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    internal static class IEnumerableExt
    {
        public static bool IsNullOrEmpty<T>(this IEnumerable<T> enumerable)
        {
            return enumerable == null || !enumerable.Any();
        }

        public static bool IsNotNullOrEmpty<T>(this IEnumerable<T> enumerable, Func<T, bool> predicate = null)
        {
            return enumerable != null && (predicate != null ? enumerable.Any(predicate) : enumerable.Any());
        }

        public static IEnumerable<T> TakeRandom<T>(this IEnumerable<T> enumerable, int count)
        {
            T[] takeRandom = enumerable as T[] ?? enumerable.ToArray();
            count = Math.Min(count, takeRandom.Count());
            for (int i = 0; i < count; i++)
            {
                int rand = new Random().Next(0, count);
                yield return takeRandom[rand];
            }
        }

        public static IEnumerable<T> EmptyIfNull<T>(this IEnumerable<T> enumerable)
        {
            return enumerable ?? Enumerable.Empty<T>();
        }


        public static IEnumerable<T> IntersectAll<T>(this IEnumerable<IEnumerable<T>> lists)
        {
            HashSet<T> hashSet = null;
            foreach (IEnumerable<T> list in lists)
            {
                if (hashSet == null)
                {
                    hashSet = new HashSet<T>(list);
                }
                else
                {
                    hashSet.IntersectWith(list);
                }
            }
            return hashSet == null ? new List<T>() : hashSet.ToList();
        }

        public static IOrderedEnumerable<TSource> OrderBy<TSource, TValue>(this IEnumerable<TSource> source, Func<TSource, TValue> selector, bool asc)
        {
            return asc ? source.OrderBy(selector) : source.OrderByDescending(selector);
        }

        public static IEnumerable<IEnumerable<T>> Chunk<T>(this IEnumerable<T> source, int chunksize)
        {
            while (source.Any())
            {
                yield return source.Take(chunksize);
                source = source.Skip(chunksize);
            }
        }

        public static IEnumerable<IEnumerable<T>> SplitIntoChunks<T>(this IEnumerable<T> source, int chunksize)
        {
            int pos = 0;
            while (source.Skip(pos).Any())
            {
                yield return source.Skip(pos).Take(chunksize);
                pos += chunksize;
            }
        }

        public static void ForEach<T>(this IEnumerable<T> source, Action<T> action)
        {
            foreach (T item in source)
            {
                action(item);
            }
        }

        public static IEnumerable<T> And<T>(this IEnumerable<T> range1, params IEnumerable<T>[] ranges)
        {
            if (range1 != null)
            {
                foreach (T item in range1)
                {
                    yield return item;
                }
            }

            if (ranges != null)
            {
                foreach (IEnumerable<T> range in ranges)
                {
                    if (range != null)
                    {
                        foreach (T item in range)
                        {
                            yield return item;
                        }
                    }
                }
            }
        }
    }
}

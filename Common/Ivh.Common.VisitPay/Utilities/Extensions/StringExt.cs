﻿namespace Ivh.Common.VisitPay.Utilities.Extensions
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Security;
    using System.Text;
    using System.Text.RegularExpressions;
    using VisitPay.Strings;

    internal static class StringExt
    {
        public static T ToEnum<T>(this string str, T failure)
        {
            try
            {
                T returnValue = (T)Enum.Parse(typeof(T), str, true);
                return returnValue;
            }
            catch
            {
                return failure;
            }
        }

        public static string Format(this string str, params object[] arguments)
        {
            return String.Format(str, arguments);
        }

        public static string Format(this string str, Func<object[]> fn)
        {
            object[] args = fn();
            return String.Format(str, args);
        }

        /// <summary>
        /// Converts to phone number format  (999) 999-9999
        /// </summary>
        /// <param name="phone"></param>
        /// <returns></returns>
        public static string FormatAsPhone(this string phone)
        {
            if (!String.IsNullOrEmpty(phone) && phone.Length >= 10)
            {
                phone = Regex.Replace(phone, @"\D", String.Empty);
                phone = phone.Insert(0, "(").Insert(4, ") ").Insert(9, "-");
            }
            return phone;
        }

        /// <summary>
        /// Converts zip format 99999-9999
        /// </summary>
        /// <param name="phone"></param>
        /// <returns></returns>
        public static string FormatAsZip(this string zip)
        {
            if (!String.IsNullOrEmpty(zip) && zip.Length >= 6)
            {
                zip = Regex.Replace(zip, @"\D", String.Empty);
                zip = zip.Insert(5, "-");
            }
            return zip;
        }

        /// <summary>
        /// Format string amount to negative amount with a minus in front of the amount
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static string FormatCurrency(this string value)
        {
            decimal val = 0;
            if (!String.IsNullOrEmpty(value))
            {
                Decimal.TryParse(value, out val);
            }
            return val.FormatCurrency();
        }

        public static byte[] GetBytes(this string input)
        {
            return input.GetBytes<ASCIIEncoding>();
        }

        public static byte[] GetBytes<TEncoding>(this string input) where TEncoding : Encoding, new()
        {
            return new TEncoding().GetBytes(input);
        }

        public static string RemoveSpecialCharacters(this string str)
        {
            StringBuilder sb = new StringBuilder();
            foreach (char c in str)
            {
                if ((c >= '0' && c <= '9') || (c >= 'A' && c <= 'Z') || (c >= 'a' && c <= 'z') || c == '.' || c == '_')
                {
                    sb.Append(c);
                }
            }
            return sb.ToString();
        }

        public static string RemoveNonAlphaNumericCharacters(this string str)
        {
            if (str.IsNullOrEmpty())
            {
                return str;
            }
            return new string(str.IterateOverString(char.IsLetterOrDigit).ToArray());
        }

        public static string RemoveNonNumericCharacters(this string str)
        {
            if (str.IsNullOrEmpty())
            {
                return str;
            }

            return new string(str.IterateOverString(char.IsDigit).ToArray());
        }

        private static IEnumerable<char> IterateOverString(this string str, Func<char, bool> comparison)
        {
            if (str.IsNullOrEmpty())
            {
                yield break;
            }

            foreach (char c in str.Where(comparison))
            {
                yield return c;
            }
        }

        public static string Last(this string input, int numChars)
        {
            if (string.IsNullOrEmpty(input))
            {
                return input;
            }

            input = input.Trim();

            return input.Substring(Math.Max(0, input.Length - numChars));
        }

        public static string First(this string input, int numChars)
        {
            return input.Substring(0, Math.Min(numChars, input.Length));
        }

        public static bool IsBase64Encoded(this string input)
        {
            input = input.Trim();
            return (input.Length % 4 == 0) && Regex.IsMatch(input, RegexStrings.Base64Encoded, RegexOptions.Compiled);
        }

        public static string TrimNullSafe(this string input)
        {
            return string.IsNullOrEmpty(input) ? null : input.Trim();
        }

        public static bool TryChangeType<TOutput>(this string input, out TOutput output)
        {
            try
            {
                if (typeof(TOutput) == typeof(bool))
                {
                    bool boolOutput = default(bool);
                    if (input.TryParseEx(out boolOutput))
                    {
                        input = boolOutput.ToString();
                    }
                }
                output = (TOutput)Convert.ChangeType(input, typeof(TOutput));
                return true;
            }
            catch (Exception)
            {
                output = default(TOutput);
                return false;
            }
        }
        public static string InsertSpaceBeforeCapitalLetter(this string input)
        {
            return Regex.Replace(input, "[A-Z]", " $0", RegexOptions.Compiled);
        }

        public static string InsertSpaceBetweenLowerAndCapitalLetter(this string input)
        {
            return Regex.Replace(input, "([a-z])_?([A-Z])", "$1 $2", RegexOptions.Compiled);
        }

        // Convert the string to Pascal case.
        public static string ToPascalCase(this string input)
        {
            // If there are 0 or 1 characters, just return the string.
            if (input == null)
            {
                return input;
            }
            if (input.Length < 2)
            {
                return input.ToUpper();
            }

            // Split the string into words.
            string[] words = input.Split(
                new char[] { },
                StringSplitOptions.RemoveEmptyEntries);

            // Combine the words.
            string result = "";
            foreach (string word in words)
            {
                result +=
                    word.Substring(0, 1).ToUpper() +
                    word.Substring(1);
            }

            return result;
        }

        // Convert the string to camel case.
        public static string ToCamelCase(this string input)
        {
            // If there are 0 or 1 characters, just return the string.
            if (input == null || input.Length < 2)
            {
                return input;
            }

            // Split the string into words.
            string[] words = input.Split(
                new char[] { },
                StringSplitOptions.RemoveEmptyEntries);

            // Combine the words.
            string result = words[0].ToLower();
            for (int i = 1; i < words.Length; i++)
            {
                result +=
                    words[i].Substring(0, 1).ToUpper() +
                    words[i].Substring(1);
            }

            return result;
        }

        // Capitalize the first character and add a space before
        // each capitalized letter (except the first character).
        public static string ToProperCase(this string input)
        {
            // If there are 0 or 1 characters, just return the string.
            if (input == null)
            {
                return input;
            }
            if (input.Length < 2)
            {
                return input.ToUpper();
            }

            // Start with the first character.
            string result = input.Substring(0, 1).ToUpper();

            // Add the remaining characters.
            for (int i = 1; i < input.Length; i++)
            {
                if (char.IsUpper(input[i]))
                {
                    result += " ";
                }
                result += input[i];
            }

            return result;
        }

        // Convert the string to title case
        public static string ToTitleCase(this string input)
        {
            // If there are 0 or 1 characters, just return the string.
            if (input == null)
            {
                return input;
            }
            if (input.Length < 2)
            {
                return input.ToUpper();
            }

            // Split the string into words.
            string[] words = input.Split(
                new char[] { },
                StringSplitOptions.RemoveEmptyEntries);

            // Combine the words.
            string result = "";
            foreach (string word in words)
            {
                result +=
                    word.Substring(0, 1).ToUpper() +
                    word.Substring(1) +
                    " ";
            }

            return result.Trim();
        }

        // Convert string back to IDictionary<string,string>
        public static IDictionary<string, string> AsAdditionalDataDictionary(this string additionalData)
        {
            return additionalData.IsNullOrEmpty() ? null : additionalData.Split('|').Select(nvp => nvp.Split(':')).Where(array => array.Length >= 2).ToDictionary(array => array[0], array => array[1]);
        }

        public static string GetValueOrDefault(this string input, string value)
        {
            if (!string.IsNullOrEmpty(input))
            {
                return input;
            }

            return value;
        }

        public static string Base64Encode(this string plainText)
        {
            byte[] plainTextBytes = System.Text.Encoding.UTF8.GetBytes(plainText);
            return System.Convert.ToBase64String(plainTextBytes);
        }

        public static string Base64Decode(this string base64EncodedData)
        {
            byte[] base64EncodedBytes = System.Convert.FromBase64String(base64EncodedData);
            return System.Text.Encoding.UTF8.GetString(base64EncodedBytes);
        }

        public static SecureString ToSecureString(this string insecureString)
        {
            SecureString secureString = new SecureString();
            foreach (char c in insecureString)
            {
                secureString.AppendChar(c);
            }
            secureString.MakeReadOnly();
            return secureString;
        }

        public static int GetConsistentHashCode(this string str)
        {
            unchecked
            {
                int hash1 = 5381;
                int hash2 = hash1;

                for (int i = 0; i < str.Length && str[i] != '\0'; i += 2)
                {
                    hash1 = ((hash1 << 5) + hash1) ^ str[i];
                    if (i == str.Length - 1 || str[i + 1] == '\0')
                    {
                        break;
                    }
                    hash2 = ((hash2 << 5) + hash2) ^ str[i + 1];
                }

                return hash1 + (hash2 * 1566083941);
            }
        }

        public static string ToString(this string value, IFormatProvider formatProvider, string format)
        {
            return string.Format(formatProvider, format, value);
        }

        public static bool IsEmailAddress(this string input)
        {
            if (string.IsNullOrWhiteSpace(input))
            {
                return false;
            }
            Regex r = new Regex(RegexStrings.EmailAddress);
            return r.IsMatch(input);
        }

        /// <summary>
        /// Applies a naming convention for environment variables to the given key.
        /// </summary>
        public static string ToVpEnvironmentVariableFormat(this string originalValue)
        {
            if (string.IsNullOrEmpty(originalValue))
            {
                return string.Empty;
            }

            const string variablePrefix = "VP";
            string keyWithUnderscores = originalValue.Replace(" ", "_");

            return keyWithUnderscores.StartsWith($"{variablePrefix}_")
                ? keyWithUnderscores
                : $"{variablePrefix}_{keyWithUnderscores.ToUpperInvariant()}";
        }
    }
}

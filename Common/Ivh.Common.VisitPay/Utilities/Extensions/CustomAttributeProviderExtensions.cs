﻿namespace Ivh.Common.VisitPay.Utilities.Extensions
{
    using System;
    using System.Reflection;

    public static class CustomAttributeProviderExtensions
    {
        public static bool HasAttribute<TAttribute>(this ICustomAttributeProvider customAttributeProvider) where TAttribute : Attribute
        {
            return customAttributeProvider.IsDefined(typeof(TAttribute), true);
        }
    }
}

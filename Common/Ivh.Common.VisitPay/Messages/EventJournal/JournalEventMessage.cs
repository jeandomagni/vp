﻿namespace Ivh.Common.VisitPay.Messages.EventJournal
{
    using System;
    using System.Runtime.Serialization;
    using Enums;
    using ServiceBus.Common.Messages;

    [Serializable]
    [DataContract]
    public class JournalEventMessage : UniversalMessage<JournalEventMessage>
    {
        [DataMember(Order = 1)]
        public int JournalEventId { get; set; }
        [DataMember(Order = 2)]
        public DateTime EventDateTime { get; set; }
        [DataMember(Order = 3)]
        public JournalEventTypeEnum JournalEventType { get; set; }
        [DataMember(Order = 4)]
        public JournalEventCategoryEnum JournalEventCategory { get; set; }
        [DataMember(Order = 5)]
        public string EventTags { get; set; }
        [DataMember(Order = 6)]
        public ApplicationEnum Application { get; set; }
        [DataMember(Order = 7)]
        public int EventVisitPayUserId { get; set; }

        [DataMember(Order = 8)]
        public DeviceTypeEnum? DeviceType { get; set; }
        [DataMember(Order = 9)]
        public string UserAgentString { get; set; }
        [DataMember(Order = 10)]
        public string UserHostAddress { get; set; }

       
        [DataMember(Order = 11)]
        public string Message { get; set; }
        [DataMember(Order = 12)]
        public string AdditionalData { get; set; }

        [DataMember(Order = 13)]
        public int? VisitPayUserId { get; set; }
        [DataMember(Order = 14)]
        public int? VpGuarantorId { get; set; }
        [DataMember(Order = 15)]
        public int? HsGuarantorId { get; set; }
        [DataMember(Order = 16)]
        public int? VisitId { get; set; }
        [DataMember(Order = 17)]
        public int? VisitTransactionId { get; set; }
        [DataMember(Order = 18)]
        public int? PaymentId { get; set; }
        [DataMember(Order = 19)]
        public int? FinancePlanId { get; set; }

        [DataMember(Order = 20)]
        public string JournalEventContext { get; set; }
        [DataMember(Order = 21)]
        public string JournalUserEventContext { get; set; }

        [IgnoreDataMember]
		public override string ErrorMessage => this.FormatErrorMessage(this, x => x.EventVisitPayUserId, x => x.JournalEventType, x => x.JournalEventCategory, x => x.EventTags);
    }
}

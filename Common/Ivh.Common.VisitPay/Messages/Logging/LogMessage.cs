﻿namespace Ivh.Common.VisitPay.Messages.Logging
{
    using System;
    using System.Runtime.Serialization;
    using Enums;
    using Newtonsoft.Json;
    using ServiceBus.Common.Messages;

    [Serializable]
    [DataContract]
    public class LogMessage : UniversalMessage<LogMessage>
    {
        [DataMember(Order = 1)]
        public LogMessageTypeEnum LogMessageTypeEnum { get; set; }
        [DataMember(Order = 2)]
        public string Message { get; set; }
        [DataMember(Order = 3)]
        public object[] Parameters { get; set; }
        [DataMember(Order = 4)]
        public string ApplicationName { get; set; }
        [DataMember(Order = 5)]
        public string ApplicationVersion { get; set; }
        [DataMember(Order = 6)]
        public string StackTrace { get; set; }
        [DataMember(Order = 7)]
        public int ThreadId { get; set; }
        [DataMember(Order = 8)]
        public DateTime Date { get; set; }
        [IgnoreDataMember]
		public override string ErrorMessage => $"{nameof(LogMessage)} = {JsonConvert.SerializeObject(this)}";
        [IgnoreDataMember]
        public override string TraceMessage => $"{nameof(LogMessage)}: {this.LogMessageTypeEnum}";
    }
}

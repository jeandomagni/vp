﻿namespace Ivh.Common.VisitPay.Messages.AppIntelligence
{
    using System;
    using System.Runtime.Serialization;
    using Enums;
    using ServiceBus.Common.Messages;
    
    [Serializable]
    [DataContract]
    public class FinancePlanTermsDisplayedMessage : UniversalMessage<FinancePlanTermsDisplayedMessage>
    {
        [DataMember(Order = 1)]
        public int VpGuarantorId { get; set; }
        
        [DataMember(Order = 2)]
        public DateTime EffectiveDate { get; set; }
        
        [DataMember(Order = 3)]
        public DateTime FinalPaymentDate { get; set; }
        
        [DataMember(Order = 4)]
        public decimal? FinancePlanBalance { get; set; }
        
        [DataMember(Order = 5)]
        public FinancePlanOfferSetTypeEnum FinancePlanOfferSetType { get; set; }
        
        [DataMember(Order = 6)]
        public DateTime InsertDate { get; set; }
        
        [DataMember(Order = 7)]
        public decimal InterestRate { get; set; }

        [DataMember(Order = 8)]
        public bool IsCombined { get; set; }
        
        [DataMember(Order = 9)]
        public int? MaximumNumberOfPayments { get; set; }
        
        [DataMember(Order = 10)]
        public decimal? MinimumMonthlyPaymentAmount { get; set; }
        
        [DataMember(Order = 11)]
        public decimal MonthlyPaymentAmount { get; set; }
        
        [DataMember(Order = 12)]
        public decimal MonthlyPaymentAmountOtherPlans { get; set; }
        
        [DataMember(Order = 13)]
        public decimal MonthlyPaymentAmountPrevious { get; set; }

        [DataMember(Order = 14)]
        public decimal MonthlyPaymentAmountTotal { get; set; }
        
        [DataMember(Order = 15)]
        public int NumberMonthlyPayments { get; set; }
        
        [DataMember(Order = 16)]
        public decimal TotalInterest { get; set; }

        [DataMember(Order = 17)]
        public bool IsClient { get; set; }
        
        [IgnoreDataMember]
		public override string ErrorMessage => this.FormatErrorMessage(this, x => x.VpGuarantorId);
    }
}
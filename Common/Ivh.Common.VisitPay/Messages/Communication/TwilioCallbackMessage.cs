﻿namespace Ivh.Common.VisitPay.Messages.Communication
{
    using System;
    using System.Runtime.Serialization;
    using ServiceBus.Attributes;
    using ServiceBus.Common.Messages;

    [Serializable]
    [DataContract]
    [UniversalMessage(consumable: false)]
    public class TwilioCallbackMessage : UniversalMessage<TwilioCallbackMessage>
    {
        [DataMember(Order = 1)]
        public string MessageSid { get; set; }
        [DataMember(Order = 2)]
        public string SmsSid { get; set; }
        [DataMember(Order = 3)]
        public string AccountSid { get; set; }
        [DataMember(Order = 4)]
        public string MessagingServiceSid { get; set; }
        [DataMember(Order = 5)]
        public string From { get; set; }
        [DataMember(Order = 6)]
        public string To { get; set; }
        [DataMember(Order = 7)]
        public string Body { get; set; }
        [DataMember(Order = 8)]
        public string NumMedia { get; set; }
        [DataMember(Order = 9)]
        public string RawData { get; set; }
        [DataMember(Order = 10)]
        public string SmsMessageSid { get; set; }

        [IgnoreDataMember]
		public override string ErrorMessage => this.FormatErrorMessage(this, x => x.MessageSid, x => x.SmsSid);
    }
}
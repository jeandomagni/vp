﻿namespace Ivh.Common.VisitPay.Messages.Communication
{
    using System;
    using System.Runtime.Serialization;
    using ServiceBus.Common.Messages;

    [Serializable]
    [DataContract]
    public class SendClientSupportRequestAdminNotificationEmailMessage : UniversalMessage<SendClientSupportRequestAdminNotificationEmailMessage>
    {
        [DataMember(Order = 1)]
        public string ClientSupportRequestDisplayId { get; set; }

        [IgnoreDataMember]
		public override string ErrorMessage => this.FormatErrorMessage(this, x => x.ClientSupportRequestDisplayId);
    }
}
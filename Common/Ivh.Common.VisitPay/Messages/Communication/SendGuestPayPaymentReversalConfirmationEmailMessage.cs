﻿namespace Ivh.Common.VisitPay.Messages.Communication
{
    using System;
    using System.Runtime.Serialization;
    using Enums;
    using ServiceBus.Common.Messages;

    [DataContract]
    [Serializable]
    public class SendGuestPayPaymentReversalConfirmationEmailMessage : UniversalMessage<SendGuestPayPaymentReversalConfirmationEmailMessage>
    {
        [DataMember(Order = 1)]
        public string EmailAddress { get; set; }

        [DataMember(Order = 2)]
        public string GuarantorFirstName { get; set; }

        [DataMember(Order = 3)]
        public int PaymentUnitId { get; set; }

        [DataMember(Order = 4)]
        public string TransactionId { get; set; }
        
        [DataMember(Order = 5)]
        public PaymentTypeEnum PaymentType { get; set; }

        [IgnoreDataMember]
		public override string ErrorMessage => this.FormatErrorMessage(this, x => x.PaymentUnitId);
    }
}

﻿namespace Ivh.Common.VisitPay.Messages.Communication
{
    using System;
    using System.Runtime.Serialization;
    using ServiceBus.Common.Messages;

    [DataContract]
    [Serializable]
    public class SendGuestPayPaymentFailureEmailMessage : UniversalMessage<SendGuestPayPaymentFailureEmailMessage>
    {
        [DataMember(Order = 1)]
        public string EmailAddress { get; set; }

        [DataMember(Order = 2)]
        public string GuarantorFirstName { get; set; }
        
        [DataMember(Order = 3)]
        public int PaymentUnitId { get; set; }

        [DataMember(Order = 4)]
        public string TransactionId { get; set; }

        [IgnoreDataMember]
		public override string ErrorMessage => this.FormatErrorMessage(this, x => x.PaymentUnitId);
    }
}

﻿namespace Ivh.Common.VisitPay.Messages.Communication
{
    using System;
    using System.Runtime.Serialization;
    using ServiceBus.Common.Messages;

    [Serializable]
    [DataContract]
    public class SendClientSupportRequestMessageAdminNotificationEmailMessage : UniversalMessage<SendClientSupportRequestMessageAdminNotificationEmailMessage>
    {
        [DataMember(Order = 1)]
        public string ClientSupportRequestDisplayId { get; set; }

        [DataMember(Order = 2)]
        public DateTime InsertDate { get; set; }

        [IgnoreDataMember]
        public override string ErrorMessage => this.FormatErrorMessage(this, x => x.ClientSupportRequestDisplayId);
    }
}
﻿namespace Ivh.Common.VisitPay.Messages.Communication
{
    using System;
    using System.Collections.Generic;
    using System.Runtime.Serialization;
    using Enums;
    using ServiceBus.Attributes;
    using ServiceBus.Common.Messages;

    [ClientFriendlyDeliveryRange]
    [Serializable]
    [DataContract]
    public class FinancePlanStatementNotificationMessage : UniversalMessage<FinancePlanStatementNotificationMessage>
    {
        [DataMember(Order = 1)]
        public int VpGuarantorId { get; set; }

        [DataMember(Order = 2)]
        public DateTime StatementDate { get; set; }

        [DataMember(Order = 3)]
        public DateTime StatementDueDate { get; set; }

        [DataMember(Order = 4)]
        public decimal StatementBalance { get; set; }

        [DataMember(Order = 5)]
        public int VpStatementId { get; set; }

        [DataMember(Order = 6)]
        public StatementNotificationTierEnum StatementNotificationTierEnum  { get; set; }

        [DataMember(Order = 6)]
        public bool IsOfflineGuarantor { get; set; }

        [DataMember(Order = 7)]
        public List<int> VisitIds { get; set; }

        [IgnoreDataMember]
        public override string ErrorMessage => this.FormatErrorMessage(this, x => x.VpGuarantorId, x => x.VpStatementId);
    }
}

﻿namespace Ivh.Common.VisitPay.Messages.Communication
{
    using System;
    using System.Runtime.Serialization;
    using Enums;
    using ServiceBus.Common.Messages;

    [Serializable]
    [DataContract]
    public class SendClientSupportRequestStatusNotificationEmailMessage : UniversalMessage<SendClientSupportRequestStatusNotificationEmailMessage>
    {
        [DataMember(Order = 1)]
        public int VisitPayUserId { get; set; }

        [DataMember(Order = 2)]
        public string ClientSupportRequestDisplayId { get; set; }

        [DataMember(Order = 3)]
        public DateTime InsertDate { get; set; }

        [DataMember(Order = 4)]
        public ClientSupportRequestStatusEnum Status { get; set; }

        [IgnoreDataMember]
		public override string ErrorMessage => this.FormatErrorMessage(this, x => x.VisitPayUserId);
    }
}
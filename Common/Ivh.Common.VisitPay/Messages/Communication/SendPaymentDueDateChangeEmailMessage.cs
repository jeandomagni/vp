﻿namespace Ivh.Common.VisitPay.Messages.Communication
{
    using System;
    using System.Runtime.Serialization;
    using ServiceBus.Common.Messages;

    [Serializable]
    [DataContract]
    public class SendPaymentDueDateChangeEmailMessage : UniversalMessage<SendPaymentDueDateChangeEmailMessage>
    {
        [DataMember(Order = 1)]
        public int VpGuarantorId { get; set; }

        [DataMember(Order = 2)]
        public DateTime NextPaymentDueDate { get; set; }

        [IgnoreDataMember]
		public override string ErrorMessage => this.FormatErrorMessage(this, x => x.VpGuarantorId);
    }
}
﻿namespace Ivh.Common.VisitPay.Messages.Communication
{
    using System;
    using System.Runtime.Serialization;
    using ServiceBus.Common.Messages;

    [DataContract]
    [Serializable]
    public class SupportRequestRedactMessage : UniversalMessage<SupportRequestRedactMessage>
    {
        [DataMember(Order = 1)]
        public int VpGuarantorId { get; set; }
        [DataMember(Order = 2)]
        public int VisitId { get; set; }

        [IgnoreDataMember]
		public override string ErrorMessage => this.FormatErrorMessage(this, x => x.VpGuarantorId, x => x.VisitId);
    }
}
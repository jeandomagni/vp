﻿namespace Ivh.Common.VisitPay.Messages.Communication
{
    using System;
    using System.Runtime.Serialization;
    using ServiceBus.Common.Messages;

    [Serializable]
    [DataContract]
    public class SendClientSupportRequestConfirmationEmailMessage : UniversalMessage<SendClientSupportRequestConfirmationEmailMessage>
    {
        [DataMember(Order = 1)]
        public int VisitPayUserId { get; set; }

        [DataMember(Order = 2)]
        public string ClientSupportRequestDisplayId { get; set; }

        [IgnoreDataMember]
		public override string ErrorMessage => this.FormatErrorMessage(this, x => x.VisitPayUserId);
    }
}
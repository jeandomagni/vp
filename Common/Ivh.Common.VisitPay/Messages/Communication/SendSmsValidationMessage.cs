﻿namespace Ivh.Common.VisitPay.Messages.Communication
{
    using System;
    using System.Runtime.Serialization;
    using ServiceBus.Common.Messages;

    [Serializable]
    [DataContract]
    public class SendSmsValidationMessage : UniversalMessage<SendSmsValidationMessage>
    {
        [DataMember(Order = 1)]
        public string Phone { get; set; }
        [DataMember(Order = 2)]
        public int VisitPayUserId { get; set; }
        [DataMember(Order = 3)]
        public string Token { get; set; }

        [IgnoreDataMember]
		public override string ErrorMessage => this.FormatErrorMessage(this, x => x.VisitPayUserId);
    }
}

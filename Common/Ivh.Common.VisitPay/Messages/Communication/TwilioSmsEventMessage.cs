﻿namespace Ivh.Common.VisitPay.Messages.Communication
{
    using System;
    using System.Runtime.Serialization;
    using ServiceBus.Common.Messages;

    [Serializable]
    [DataContract]
    public class TwilioSmsEventMessage : UniversalMessage<TwilioSmsEventMessage>
    {
        /// <summary>
        ///     Gets or sets the number of attempts
        /// </summary>
        [DataMember(Order = 0, Name = "attempt")]
        public string Attempt { get; set; }

        /// <summary>
        ///     Gets or sets the message category
        ///     Note: this could be an array if we pass in an array of categories, if it is converted to , delimited
        /// </summary>
        [DataMember(Order = 1, Name = "category")]
        public string Category { get; set; }

        /// <summary>
        ///     Gets or sets email address of the intended recipient
        /// </summary>
        [DataMember(Order = 2, Name = "email")]
        public string EmailAddress { get; set; }

        /// <summary>
        ///     Gets or sets the SendGrid internal message event id
        /// </summary>
        [DataMember(Order = 3, Name = "sg_event_id")]
        public string EventId { get; set; }

        /// <summary>
        ///     Gets or sets the send mail web hook event
        /// </summary>
        [DataMember(Order = 4, Name = "event")]
        public string EventName { get; set; }

        /// <summary>
        ///     Gets or sets the SendGrid message id
        /// </summary>
        [DataMember(Order = 5, Name = "type")]
        public string EventType { get; set; }

        /// <summary>
        ///     Gets or sets the group id for group unsubscribe/resubscribe
        /// </summary>
        [DataMember(Order = 6, Name = "asm_group_id")]
        public string GroupId { get; set; }

        /// <summary>
        ///     Gets or sets the ip address
        /// </summary>
        [DataMember(Order = 7, Name = "ip")]
        public string IP { get; set; }

        /// <summary>
        ///     Gets or sets the SendGrid internal message id
        /// </summary>
        [DataMember(Order = 8, Name = "sg_message_id")]
        public string MessageId { get; set; }

        /// <summary>
        ///     Gets or sets the response
        /// </summary>
        [DataMember(Order = 9, Name = "response")]
        public string Response { get; set; }

        /// <summary>
        ///     Gets or sets the id attached to hte message by the originating system
        /// </summary>
        [DataMember(Order = 10, Name = "smtp-id")]
        public string SmtpId { get; set; }

        /// <summary>
        ///     Gets or sets the source application (e.g. VisitPay)
        /// </summary>
        [DataMember(Order = 11)]
        public string SourceApp { get; set; }

        /// <summary>
        ///     Gets or sets the event timestamp (seconds since Jan 1, 1970)
        /// </summary>
        [DataMember(Order = 12, Name = "timestamp")]
        public string Timestamp { get; set; }

        /// <summary>
        ///     Gets or sets the unique id sent in the header and stored in the database as
        ///     an attribute of the email
        /// </summary>
        [DataMember(Order = 13)]
        public Guid UniqueId { get; set; }

        /// <summary>
        ///     Gets or sets the url
        /// </summary>
        [DataMember(Order = 14, Name = "url")]
        public string Url { get; set; }

        /// <summary>
        ///     Gets or sets the user agent responsible for the event
        ///     e.g. “Mozilla/5.0 (Macintosh; Intel Mac OS X 10_8_4)
        ///     AppleWebKit/537.36 (KHTML, like Gecko)
        ///     Chrome/28.0.1500.95 Safari/537.36”
        /// </summary>
        [DataMember(Order = 15, Name = "useragent")]
        public string UserAgent { get; set; }

        /// <summary>
        /// Gets or sets the raw data from SendGrid
        /// </summary>
        [DataMember(Order = 16)]
        public string RawData { get; set; }

        [IgnoreDataMember]
		public override string ErrorMessage => this.FormatErrorMessage(this, x => x.MessageId, x => x.UniqueId);
    }
}
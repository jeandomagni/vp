﻿namespace Ivh.Common.VisitPay.Messages.Communication
{
    using System;
    using System.Runtime.Serialization;
    using ServiceBus.Attributes;
    using ServiceBus.Common.Messages;

    [ClientFriendlyDeliveryRange]
    [Serializable]
    [DataContract]
    public class SendSupportRequestUpdatedEmailMessage : UniversalMessage<SendSupportRequestUpdatedEmailMessage>
    {
        [DataMember(Order = 1)]
        public int VpGuarantorId { get; set; }
        [DataMember(Order = 2)]
        public string SupportRequestDisplayId { get; set; }
        [DataMember(Order = 3)]
        public string InsertDate { get; set; }
        [DataMember(Order = 4)]
        public string ModificationStatus { get; set; }

        [IgnoreDataMember]
		public override string ErrorMessage => this.FormatErrorMessage(this, x => x.VpGuarantorId);
    }
}

﻿namespace Ivh.Common.VisitPay.Messages.Communication
{
    using System;
    using System.Runtime.Serialization;
    using Enums;
    using ServiceBus.Common.Messages;

    [Serializable]
    [DataContract]
    public class SendPaymentReversalConfirmationEmailMessage : UniversalMessage<SendPaymentReversalConfirmationEmailMessage>
    {
        [DataMember(Order = 1)]
        public int VpGuarantorId { get; set; }
        [DataMember(Order = 2)]
        public string TransactionNumber { get; set; }
        [DataMember(Order = 3)]
        public PaymentTypeEnum PaymentType { get; set; }

        [IgnoreDataMember]
		public override string ErrorMessage => this.FormatErrorMessage(this, x => x.VpGuarantorId);
    }
}

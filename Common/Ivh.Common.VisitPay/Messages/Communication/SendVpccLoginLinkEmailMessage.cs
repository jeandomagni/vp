﻿namespace Ivh.Common.VisitPay.Messages.Communication
{
    using System;
    using System.Runtime.Serialization;
    using ServiceBus.Common.Messages;

    [DataContract]
    [Serializable]
    public class SendVpccLoginLinkEmailMessage : UniversalMessage<SendVpccLoginLinkEmailMessage>
    {
        [DataMember(Order = 1)]
        public string TempPassword { get; set; }

        [DataMember(Order = 2)]
        public int VpGuarantorId { get; set; }

        [IgnoreDataMember]
        public override string ErrorMessage => this.FormatErrorMessage(this, x => x.VpGuarantorId);
    }
}

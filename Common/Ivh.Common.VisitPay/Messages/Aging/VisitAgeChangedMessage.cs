﻿namespace Ivh.Common.VisitPay.Messages.Aging
{
    using System;
    using System.Runtime.Serialization;
    using Enums;
    using ServiceBus.Common.Messages;

    [Serializable]
    [DataContract]
    public class VisitAgeChangedMessage : UniversalMessage<VisitAgeChangedMessage>, IVisitAgeChangedMessage
    {
        [DataMember(Order = 1)]
        public string VisitSourceSystemKey { get;set; }
        [DataMember(Order = 2)]
        public int VisitBillingSystemId { get;set; }
        [DataMember(Order = 3)]
        public AgingTierEnum AgingTier { get; set; }
        //Idea here is to add some data why it's changing.
        [DataMember(Order = 4)]
        public string AgingTierMessage { get; set; }
        [DataMember(Order = 5)]
        public int? ActionVisitPayUserId { get; set; }

        [IgnoreDataMember]
        public override string ErrorMessage => this.FormatErrorMessage(this, x => x.VisitSourceSystemKey, x => x.VisitBillingSystemId, x => x.AgingTier);
    }
}
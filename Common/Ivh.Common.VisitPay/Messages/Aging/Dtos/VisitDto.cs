﻿namespace Ivh.Common.VisitPay.Messages.Aging.Dtos
{
    using System;
    using System.Runtime.Serialization;

    [Serializable]
    [DataContract]
    public class VisitDto
    {
        [DataMember(Order = 1)]
        public string VisitSourceSystemKey { get; set; }

        [DataMember(Order = 2)]
        public int VisitBillingSystemId { get; set; }

    }
}

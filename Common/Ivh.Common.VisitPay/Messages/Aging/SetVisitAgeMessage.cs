﻿namespace Ivh.Common.VisitPay.Messages.Aging
{
    using System;
    using System.Runtime.Serialization;
    using Dtos;
    using Enums;
    using ServiceBus.Common.Messages;

    [Serializable]
    [DataContract]
    public class SetVisitAgeMessage : UniversalMessage<SetVisitAgeMessage>
    {
        [DataMember(Order = 1)]
        public VisitDto Visit { get; set; }

        [DataMember(Order = 2)]
        public AgingTierEnum AgingTierEnum { get; set; }

        [DataMember(Order = 3)]
        public int ActionVisitPayUserId { get; set; }

        // Upon message consumption the visit may not be eligible to age 
        // because it has not received another message that would set it to an eligible condition
        [DataMember(Order = 4)]
        public bool SetToActive { get; set; }

        [IgnoreDataMember]
        public override string ErrorMessage => this.FormatErrorMessage(this, x => x.Visit.VisitSourceSystemKey, x => x.Visit.VisitBillingSystemId, x => x.AgingTierEnum, x => x.ActionVisitPayUserId);

    }
}

﻿namespace Ivh.Common.VisitPay.Messages.Aging
{
    using Enums;

    public interface IVisitAgeChangedMessage
    {
        string VisitSourceSystemKey { get; set; }
        int VisitBillingSystemId { get; set; }
        AgingTierEnum AgingTier { get; set; }
        //Idea here is to add some data why it's changing.
        string AgingTierMessage { get; set; }

        /// <summary>
        /// VisitPayUserId of the user triggering the age change, or null for the SystemUser
        /// </summary>
        int? ActionVisitPayUserId { get; set; }

        string ErrorMessage { get; }
    }
}

﻿namespace Ivh.Common.VisitPay.Messages.Aging
{
    using System;
    using System.Runtime.Serialization;
    using Dtos;
    using ServiceBus.Common.Messages;

    [Serializable]
    [DataContract]
    public class ResetVisitAgeMessage : UniversalMessage<ResetVisitAgeMessage>
    {
        [DataMember(Order = 1)]
        public VisitDto Visit { get; set; }

        [DataMember(Order = 2)]
        public int ActionVisitPayUserId { get; set; }

        [IgnoreDataMember]
        public override string ErrorMessage => this.FormatErrorMessage(this, x => x.Visit.VisitSourceSystemKey, x => x.Visit.VisitBillingSystemId, x => x.ActionVisitPayUserId);
    }
}

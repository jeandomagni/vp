﻿namespace Ivh.Common.VisitPay.Messages.Aging
{
    using System;
    using System.Runtime.Serialization;
    using ServiceBus.Common.Messages;

    [Serializable]
    [DataContract]
    public class AgeVisitMessage: UniversalMessage<AgeVisitMessage>
    {
        [DataMember(Order = 1)]
        public int VisitId { get; set; }

        [DataMember(Order = 2)]
        public DateTime DateToProcess { get; set; }

        [IgnoreDataMember]
        public override string ErrorMessage => this.FormatErrorMessage(this, x => x.VisitId);
    }
}

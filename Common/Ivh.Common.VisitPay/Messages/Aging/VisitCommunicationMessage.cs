﻿namespace Ivh.Common.VisitPay.Messages.Aging
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Runtime.Serialization;
    using Dtos;
    using Enums;
    using ServiceBus.Common.Messages;

    [Serializable]
    [DataContract]
    public class VisitCommunicationMessage : UniversalMessage<VisitCommunicationMessage>
    {
        [DataMember(Order = 1)]
        public IList<VisitDto> Visits { get; set; }

        [DataMember(Order = 2)]
        public AgingCommunicationTypeEnum CommunicationType { get; set; }

        [DataMember(Order = 3)]
        public DateTime CommunicationDateTime { get; set; }

        [IgnoreDataMember]
        public override string ErrorMessage => this.FormatErrorMessage(this, x => x.CommunicationType) 
            + $": {nameof(this.Visits)}{{SourceSystemKey, BillingSystemId}}={string.Join(",", this.Visits.Select(x => $"{{{x.VisitSourceSystemKey}, {x.VisitBillingSystemId}}}"))}";
    }
}
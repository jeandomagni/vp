﻿namespace Ivh.Common.VisitPay.Messages.Matching
{
    using System;
    using System.Runtime.Serialization;
    using ServiceBus.Common.Messages;

    [Serializable]
    [DataContract]
    public class NewGuarantorMatchCreatedMessageList : UniversalMessageList<NewGuarantorMatchCreatedMessage>
    {
    }
}

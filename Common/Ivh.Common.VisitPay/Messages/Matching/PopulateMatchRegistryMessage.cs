﻿namespace Ivh.Common.VisitPay.Messages.Matching
{
    using System;
    using System.Runtime.Serialization;
    using ServiceBus.Common.Messages;

    [Serializable]
    [DataContract]
    public class PopulateMatchRegistryMessage : UniversalMessage<PopulateMatchRegistryMessage>
    {
        [DataMember(Order = 1)]
        public int HsGuarantorId { get; set; }

        [DataMember(Order = 2)]
        public DateTime? ProcessDate { get; set; }

        [DataMember(Order = 3)]
        public bool IsUpdate { get; set; }

        [IgnoreDataMember]
        public override string ErrorMessage => this.FormatErrorMessage(this, x => x.HsGuarantorId);
    }
}

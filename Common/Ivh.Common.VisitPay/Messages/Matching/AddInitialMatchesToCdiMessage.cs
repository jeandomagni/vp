﻿namespace Ivh.Common.VisitPay.Messages.Matching
{
    using System;
    using System.Runtime.Serialization;
    using Dtos;
    using ServiceBus.Common.Messages;

    [Serializable]
    [DataContract]
    public class AddInitialMatchesToCdiMessage : UniversalMessage<AddInitialMatchesToCdiMessage>
    {
        [DataMember(Order = 1)]
        public MatchGuarantorDto MatchGuarantorDto { get; set; }

        [DataMember(Order = 2)]
        public MatchPatientDto MatchPatientDto { get; set; }

        [DataMember(Order = 3)]
        public int VpGuarantorId { get; set; }

        [IgnoreDataMember]
        public override string ErrorMessage => this.FormatErrorMessage(this, x => x.VpGuarantorId);
    }
}

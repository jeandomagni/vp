﻿namespace Ivh.Common.VisitPay.Messages.Matching
{
    using System;
    using System.Runtime.Serialization;
    using ServiceBus.Common.Messages;

    [Serializable]
    [DataContract]
    public class PopulateMatchRegistryMessageList : UniversalMessageList<PopulateMatchRegistryMessage>
    {
        [DataMember(Order = 1)]
        public DateTime ProcessDate { get; set; }

        [DataMember(Order = 2)]
        public bool IsUpdate { get; set; }
    }
}

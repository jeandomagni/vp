﻿namespace Ivh.Common.VisitPay.Messages.Matching
{
    using System;
    using System.Runtime.Serialization;
    using Dtos;
    using ServiceBus.Common.Messages;

    [Serializable]
    [DataContract]
    public class NewGuarantorMatchCreatedMessage : UniversalMessage<NewGuarantorMatchCreatedMessage>
    {
        [DataMember(Order = 1)]
        public GuarantorMatchResult MatchResult { get; set; }

        [IgnoreDataMember]
        public override string ErrorMessage => this.FormatErrorMessage(this,
            x => x.MatchResult.VpGuarantorId.ToString(),
            x => x.MatchResult.MatchedHsGuarantorSourceSystemKey,
            x => x.MatchResult.MatchedHsGuarantorBillingSystemId.ToString());
    }
}

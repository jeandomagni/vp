﻿namespace Ivh.Common.VisitPay.Messages.Matching
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Runtime.Serialization;
    using HospitalData.Dtos;
    using ServiceBus.Common.Messages;

    [Serializable]
    [DataContract]
    public class GenerateChangeEventForVpGuarantorMessage : UniversalMessage<GenerateChangeEventForVpGuarantorMessage>
    {
        [DataMember(Order = 1)]
        public int VpGuarantorId { get; set; }

        [DataMember(Order = 2)]
        public List<EnrollmentResponseDto> EnrollmentResponses { get; set; }

        [IgnoreDataMember]
        public override string ErrorMessage => this.FormatErrorMessage(this,
            x => x.VpGuarantorId.ToString(),
            x => string.Join(";", x.EnrollmentResponses.Select(y => y.GuarantorSourceSystemKey)));
    }
}
﻿namespace Ivh.Common.VisitPay.Messages.Matching
{
    using System;
    using System.Runtime.Serialization;
    using Dtos;
    using Enums;
    using ServiceBus.Common.Messages;

    [Serializable]
    [DataContract]
    public class UpdateMatchInfoMessage : UniversalMessage<UpdateMatchInfoMessage>
    {
        [DataMember(Order = 1)]
        public MatchGuarantorDto MatchGuarantorDto { get; set; }

        [DataMember(Order = 2)]
        public int VpGuarantorId { get; set; }

        [DataMember(Order = 3)]
        public ApplicationEnum Application { get; set; }

        [IgnoreDataMember]
        public override string ErrorMessage => this.FormatErrorMessage(this, x => x.VpGuarantorId);

    }
}

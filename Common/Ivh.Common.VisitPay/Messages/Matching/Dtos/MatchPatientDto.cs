﻿namespace Ivh.Common.VisitPay.Messages.Matching.Dtos
{
    using System;

    public class MatchPatientDto
    {
        public DateTime? PatientDOB { get; set; }
    }
}

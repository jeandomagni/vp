﻿namespace Ivh.Common.VisitPay.Messages.Matching.Dtos
{
    using Enums;

    public class MatchDataDto
    {
        public ApplicationEnum ApplicationEnum { get; set; }
        public MatchGuarantorDto Guarantor { get; set; }
        public MatchPatientDto Patient { get; set; }
        public MatchGuestPayAuthenticationDto GuestPayAuthentication { get; set; }
    }
}

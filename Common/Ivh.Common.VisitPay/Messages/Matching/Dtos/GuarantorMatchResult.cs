﻿namespace Ivh.Common.VisitPay.Messages.Matching.Dtos
{
    using System;
    using System.Runtime.Serialization;
    using Enums;

    [Serializable]
    [DataContract]
    public class GuarantorMatchResult : IEquatable<GuarantorMatchResult>
    {
        /// <summary>
        /// found VpGuarantorId to match to
        /// </summary>
        [DataMember(Order = 1)]
        public int? VpGuarantorId { get; set; }

        [DataMember(Order = 2)]
        public string MatchedHsGuarantorSourceSystemKey { get; set; }

        [DataMember(Order = 3)]
        public int MatchedHsGuarantorBillingSystemId { get; set; }

        [DataMember(Order = 4)]
        public int MatchedHsGuarantorId { get; set; }

        [DataMember(Order = 5)]
        public GuarantorMatchResultEnum GuarantorMatchResultEnum { get; set; }

        [DataMember(Order = 6)]
        public PatternUseEnum MatchType { get; set; }

        [DataMember(Order = 7)]
        public MatchOptionEnum MatchOption { get; set; }

        public static GuarantorMatchResult NoMatchFound => new GuarantorMatchResult{ GuarantorMatchResultEnum = GuarantorMatchResultEnum.NoMatchFound };
        public static GuarantorMatchResult Ambiguous => new GuarantorMatchResult { GuarantorMatchResultEnum = GuarantorMatchResultEnum.Ambiguous };
        

        #region Equality
        public bool Equals(GuarantorMatchResult other)
        {
            if (ReferenceEquals(null, other))
            {
                return false;
            }

            if (ReferenceEquals(this, other))
            {
                return true;
            }

            return this.VpGuarantorId == other.VpGuarantorId && 
                   string.Equals(this.MatchedHsGuarantorSourceSystemKey, other.MatchedHsGuarantorSourceSystemKey) && 
                   this.MatchedHsGuarantorBillingSystemId == other.MatchedHsGuarantorBillingSystemId && 
                   this.GuarantorMatchResultEnum == other.GuarantorMatchResultEnum && 
                   this.MatchType == other.MatchType;
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj))
            {
                return false;
            }

            if (ReferenceEquals(this, obj))
            {
                return true;
            }

            if (obj.GetType() != this.GetType())
            {
                return false;
            }

            return this.Equals((GuarantorMatchResult)obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                int hashCode = this.VpGuarantorId.GetHashCode();
                hashCode = (hashCode * 397) ^ (this.MatchedHsGuarantorSourceSystemKey != null ? this.MatchedHsGuarantorSourceSystemKey.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ this.MatchedHsGuarantorBillingSystemId;
                hashCode = (hashCode * 397) ^ (int)this.GuarantorMatchResultEnum;
                hashCode = (hashCode * 397) ^ (int)this.MatchType;
                return hashCode;
            }
        }

        /*
        public bool Equals(GuarantorMatchResult other)
        {
            if (ReferenceEquals(null, other))
            {
                return false;
            }

            if (ReferenceEquals(this, other))
            {
                return true;
            }

            return string.Equals(this.MatchedHsGuarantorSourceSystemKey, other.MatchedHsGuarantorSourceSystemKey) 
                   && this.MatchedHsGuarantorBillingSystemId == other.MatchedHsGuarantorBillingSystemId 
                   && this.GuarantorMatchResultEnum == other.GuarantorMatchResultEnum 
                   && this.MatchType == other.MatchType;
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj))
            {
                return false;
            }

            if (ReferenceEquals(this, obj))
            {
                return true;
            }

            if (obj.GetType() != this.GetType())
            {
                return false;
            }

            return Equals((GuarantorMatchResult)obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                int hashCode = (this.MatchedHsGuarantorSourceSystemKey != null ? this.MatchedHsGuarantorSourceSystemKey.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ this.MatchedHsGuarantorBillingSystemId;
                hashCode = (hashCode * 397) ^ (int)this.GuarantorMatchResultEnum;
                hashCode = (hashCode * 397) ^ (int)this.MatchType;
                return hashCode;
            }
        }
        */
        #endregion Equality

    }
}

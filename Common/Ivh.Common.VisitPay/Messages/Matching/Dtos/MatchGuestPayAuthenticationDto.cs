﻿namespace Ivh.Common.VisitPay.Messages.Matching.Dtos
{
    public class MatchGuestPayAuthenticationDto
    {
        public int StatementIdentifierId { get; set; }
    }
}

﻿namespace Ivh.Common.VisitPay.Messages.Matching.Dtos
{
    using System;

    public class MatchGuarantorDto
    {
        public int HsBillingSystemId { get; set; }
        public string SourceSystemKey { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public DateTime? DOB { get; set; }
        public string SSN { get; set; }
        public string SSN4 { get; set; }
        public string AddressLine1 { get; set; }
        public string AddressLine2 { get; set; }
        public string City { get; set; }
        public string StateProvince { get; set; }
        public string PostalCode { get; set; }
    }
}

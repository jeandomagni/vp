﻿namespace Ivh.Common.VisitPay.Messages.FinanceManagement
{
    using System;
    using System.Runtime.Serialization;
    using ServiceBus.Common.Messages;

    [Serializable]
    [DataContract]
    public class SendPrimaryPaymentMethodChangeEmailMessage : UniversalMessage<SendPrimaryPaymentMethodChangeEmailMessage>
    {
        [DataMember(Order = 1)]
        public int VpGuarantorId { get; set; }

        [IgnoreDataMember]
		public override string ErrorMessage => this.FormatErrorMessage(this, x => x.VpGuarantorId);
    }
}

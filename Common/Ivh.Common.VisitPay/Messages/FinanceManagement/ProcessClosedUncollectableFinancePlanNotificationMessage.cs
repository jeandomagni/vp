namespace Ivh.Common.VisitPay.Messages.FinanceManagement
{
    using System;
    using System.Collections.Generic;
    using System.Runtime.Serialization;
    using ServiceBus.Common.Messages;

    [Serializable]
    [DataContract]
    public class ProcessClosedUncollectableFinancePlanNotificationMessage : UniversalMessage<ProcessClosedUncollectableFinancePlanNotificationMessage>
    {
        [DataMember(Order = 1)]
        public int VpGuarantorId { get; set; }

        [DataMember(Order = 2)]
        public int FinancePlanId { get; set; }

        [DataMember(Order = 3)]
        public DateTime DateToProcess { get; set; }

        [IgnoreDataMember]
		public override string ErrorMessage => this.FormatErrorMessage(this, x => x.VpGuarantorId, x => x.FinancePlanId);
    }
}
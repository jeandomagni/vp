namespace Ivh.Common.VisitPay.Messages.FinanceManagement
{
    using System;
    using System.Runtime.Serialization;
    using ServiceBus.Common.Messages;

    [Serializable]
    [DataContract]
    public class ProcessClosedUncollectableFinancePlanNotificationMessageList : UniversalMessageList<ProcessClosedUncollectableFinancePlanNotificationMessage>
    {
    }
}
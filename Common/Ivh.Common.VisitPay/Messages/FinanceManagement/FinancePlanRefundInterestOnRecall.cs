﻿

namespace Ivh.Common.VisitPay.Messages.FinanceManagement
{
    using System;
    using System.Runtime.Serialization;
    using ServiceBus.Common.Messages;

    [Serializable]
    [DataContract]
    public class FinancePlanRefundInterestOnRecall: UniversalMessage<FinancePlanRefundInterestOnRecall>
    {
        [DataMember(Order = 1)]
        public int FinancePlanVisitId { get; set; }

        [DataMember(Order = 2)]
        public int FinancePlanId { get; set; }

        [IgnoreDataMember]
        public override string ErrorMessage => this.FormatErrorMessage(this, x => x.FinancePlanVisitId, x => x.FinancePlanId);
    }
}

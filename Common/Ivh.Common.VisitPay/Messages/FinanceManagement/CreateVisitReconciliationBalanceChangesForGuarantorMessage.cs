﻿namespace Ivh.Common.VisitPay.Messages.FinanceManagement
{
    using System;
    using System.Collections.Generic;
    using System.Runtime.Serialization;
    using ServiceBus.Common.Messages;

    [Serializable]
    [DataContract]
    public class CreateVisitReconciliationBalanceChangesForGuarantorMessage : UniversalMessage<CreateVisitReconciliationBalanceChangesForGuarantorMessage>
    {
        [DataMember(Order = 1)]
        public IList<int> VisitIds { get; set; }
        [DataMember(Order = 2)]
        public int GuarantorId { get; set; }
        [IgnoreDataMember]
		public override string ErrorMessage => this.FormatErrorMessage(this, x => x.GuarantorId) + $": {nameof(this.VisitIds)}={string.Join(",", this.VisitIds)}";
    }
}
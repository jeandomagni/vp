﻿namespace Ivh.Common.VisitPay.Messages.FinanceManagement
{
    using System;
    using System.Runtime.Serialization;
    using ServiceBus.Attributes;
    using ServiceBus.Common.Messages;

    [ClientFriendlyDeliveryRange]
    [Serializable]
    [DataContract]
    public class SendPaymentFailureEmailMessage : UniversalMessage<SendPaymentFailureEmailMessage>
    {
        [DataMember(Order = 1)]
        public int VpGuarantorId { get; set; }
        [IgnoreDataMember]
		public override string ErrorMessage => this.FormatErrorMessage(this, x => x.VpGuarantorId);
    }
}

﻿namespace Ivh.Common.VisitPay.Messages.FinanceManagement
{
    using System;
    using System.Runtime.Serialization;
    using ServiceBus.Common.Messages;

    [DataContract]
    [Serializable]
    public class SendVpccFinancePlanCreationSmsMessage : UniversalMessage<SendVpccFinancePlanCreationSmsMessage>
    {
        [DataMember(Order = 1)]
        public string TempPassword { get; set; }

        [DataMember(Order = 2)]
        public int VpGuarantorId { get; set; }

        [DataMember(Order = 3)]
        public string PhoneNumber { get; set; }

        [IgnoreDataMember]
        public override string ErrorMessage => this.FormatErrorMessage(this, x => x.VpGuarantorId);
    }
}
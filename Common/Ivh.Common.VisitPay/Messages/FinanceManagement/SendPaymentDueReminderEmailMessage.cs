﻿namespace Ivh.Common.VisitPay.Messages.FinanceManagement
{
    using System;
    using System.Runtime.Serialization;
    using ServiceBus.Attributes;
    using ServiceBus.Common.Messages;

    [ClientFriendlyDeliveryRange]
    [Serializable]
    [DataContract]
    public class SendPaymentDueReminderEmailMessage : UniversalMessage<SendPaymentDueReminderEmailMessage>
    {
        [DataMember(Order = 1)]
        public int VpGuarantorId { get; set; }
        [DataMember(Order = 2)]
        public DateTime PaymentDate { get; set; }

        [IgnoreDataMember]
		public override string ErrorMessage => this.FormatErrorMessage(this, x => x.VpGuarantorId);
    }
}

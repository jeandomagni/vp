namespace Ivh.Common.VisitPay.Messages.FinanceManagement
{
    using System;
    using System.Runtime.Serialization;
    using ServiceBus.Common.Messages;

    /// <summary>
    /// Indicates either assessment or reversal of interest.
    /// </summary>
    [Serializable]
    [DataContract]
    public class InterestEventMessage : UniversalMessage<InterestEventMessage>
    {
        [DataMember(Order = 1)]
        public DateTime InsertDate { get; set; }

        [DataMember(Order = 2)]
        public DateTime? ProcessedDate { get; set; }

        [DataMember(Order = 3)]
        public int VisitBillingSystemId { get; set; }

        [DataMember(Order = 4)]
        public int VisitId { get; set; }

        [DataMember(Order = 5)]
        public string VisitSourceSystemKey { get; set; }

        [DataMember(Order = 6)]
        public int VpFinancePlanId { get; set; }

        [DataMember(Order = 7)]
        public decimal VpFinancePlanVisitInterestAmount { get; set; }

        [DataMember(Order = 8)]
        public int VpFinancePlanVisitInterestDueId { get; set; }

        [DataMember(Order = 9)]
        public int? VpFinancePlanVisitInterestDueParentId { get; set; }

        [DataMember(Order = 10)]
        public int VpFinancePlanVisitInterestDueTypeId { get; set; }

        [DataMember(Order = 11)]
        public DateTime VpFinancePlanVisitInterestInsertDate { get; set; }

        [DataMember(Order = 12)]
        public int VpGuarantorId { get; set; }


        [IgnoreDataMember]
        public override string ErrorMessage => this.FormatErrorMessage(this, x => x.VpFinancePlanVisitInterestDueId);
    }
}
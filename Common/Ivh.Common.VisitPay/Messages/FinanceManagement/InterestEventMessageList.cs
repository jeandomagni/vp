namespace Ivh.Common.VisitPay.Messages.FinanceManagement
{
    using System;
    using System.Collections.Generic;
    using System.Runtime.Serialization;
    using ServiceBus.Common.Messages;

    [Serializable]
    [DataContract]
    public class InterestEventMessageList : UniversalMessageList<InterestEventMessage>
    {
        public InterestEventMessageList(IList<InterestEventMessage> messages = null)
            : base(messages)
        {
        }
    }
}
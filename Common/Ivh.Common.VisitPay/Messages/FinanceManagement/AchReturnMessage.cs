﻿namespace Ivh.Common.VisitPay.Messages.FinanceManagement
{
    using System;
    using System.Runtime.Serialization;
    using ServiceBus.Attributes;
    using ServiceBus.Common.Messages;

    [Serializable]
    [DataContract]
    [UniversalMessage(consumable: false)]
    public class AchReturnMessage : UniversalMessage<AchReturnMessage>
    {
        [DataMember(Order = 1)]
        public string TransactionId { get; set; }
        [DataMember(Order = 3)]
        public int AchReturnDetailId { get; set; }
        [DataMember(Order = 4)]
        public string ReturnReasonCode { get; set; }
        [IgnoreDataMember]
		public override string ErrorMessage => this.FormatErrorMessage(this, x => x.AchReturnDetailId, x => x.TransactionId, x => x.ReturnReasonCode);
    }
}
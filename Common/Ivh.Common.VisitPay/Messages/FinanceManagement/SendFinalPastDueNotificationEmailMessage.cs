﻿namespace Ivh.Common.VisitPay.Messages.FinanceManagement
{
    using System;
    using System.Collections.Generic;
    using System.Runtime.Serialization;
    using ServiceBus.Attributes;
    using ServiceBus.Common.Messages;

    [ClientFriendlyDeliveryRange]
    [Serializable]
    [DataContract]
    public class SendFinalPastDueNotificationEmailMessage : UniversalMessage<SendFinalPastDueNotificationEmailMessage>
    {
        [DataMember(Order = 1)]
        public int VpGuarantorId { get; set; }

        [DataMember(Order = 2)]
        public IList<int> VisitIds { get; set; }

        [DataMember(Order = 3)]
        public bool IsOnFinancePlan { get; set; }

        [IgnoreDataMember]
        public override string ErrorMessage => this.FormatErrorMessage(this, x => x.VpGuarantorId);
    }
}
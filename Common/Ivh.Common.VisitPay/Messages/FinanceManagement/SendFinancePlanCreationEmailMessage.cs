﻿namespace Ivh.Common.VisitPay.Messages.FinanceManagement
{
    using System;
    using System.Runtime.Serialization;
    using ServiceBus.Common.Messages;

    [DataContract]
    [Serializable]
    public class SendFinancePlanCreationEmailMessage : UniversalMessage<SendFinancePlanCreationEmailMessage>
    {
        [DataMember(Order = 1)]
        public DateTime ActionDate { get; set; }
        [DataMember(Order = 2)]
        public int VpGuarantorId { get; set; }
        [IgnoreDataMember]
		public override string ErrorMessage => this.FormatErrorMessage(this, x => x.VpGuarantorId);
    }
}
﻿namespace Ivh.Common.VisitPay.Messages.FinanceManagement
{
    using System;
    using System.Runtime.Serialization;
    using ServiceBus.Attributes;

    [ClientFriendlyDeliveryRange]
    [DataContract]
    [Serializable]
    public class ScheduledSendPaymentConfirmationEmailMessage : BaseSendPaymentConfirmationEmailMessage<ScheduledSendPaymentConfirmationEmailMessage>
    {
    }
}

namespace Ivh.Common.VisitPay.Messages.FinanceManagement
{
    using System;
    using System.Runtime.Serialization;
    using ServiceBus.Common.Messages;

    [Serializable]
    [DataContract]
    public class FinancePlanPaymentAmountChangedMessage : UniversalMessage<FinancePlanPaymentAmountChangedMessage>
    {
        [DataMember(Order = 1)]
        public int FinancePlanId { get; set; }
        [DataMember(Order = 2)]
        public decimal NewPaymentAmount { get; set; }

        [IgnoreDataMember]
		public override string ErrorMessage => this.FormatErrorMessage(this, x => x.FinancePlanId);
    }
}
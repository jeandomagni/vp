﻿namespace Ivh.Common.VisitPay.Messages.FinanceManagement
{
    using System;
    using System.Runtime.Serialization;
    using ServiceBus.Common.Messages;

    [DataContract]
    [Serializable]
    public class SendVpccFinancePlanModificationEmailMessage : UniversalMessage<SendVpccFinancePlanModificationEmailMessage>
    {
        [DataMember(Order = 1)]
        public DateTime NextPaymentDueDate { get; set; }

        [DataMember(Order = 2)]
        public int VpGuarantorId { get; set; }
        
        [DataMember(Order = 3)]
        public string TempPassword { get; set; }

        [IgnoreDataMember]
        public override string ErrorMessage => this.FormatErrorMessage(this, x => x.VpGuarantorId);
    }
}
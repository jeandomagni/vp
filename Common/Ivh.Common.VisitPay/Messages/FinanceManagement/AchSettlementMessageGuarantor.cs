﻿namespace Ivh.Common.VisitPay.Messages.FinanceManagement
{
    using System;
    using System.Runtime.Serialization;
    using ServiceBus.Common.Messages;

    [Serializable]
    [DataContract]
    public class AchSettlementMessageGuarantor : UniversalMessageList<AchSettlementMessage>
    {
        [DataMember(Order = 1)]
        public int VpGuarantorId { get; set; }

        public override string ErrorMessage => $"{nameof(this.VpGuarantorId)}={this.VpGuarantorId}: {this.FormatErrorMessage(this.Messages)}";
    }
}
﻿namespace Ivh.Common.VisitPay.Messages.FinanceManagement
{
    using System;
    using System.Collections.Generic;
    using System.Runtime.Serialization;
    using ServiceBus.Common.Messages;

    [Serializable]
    [DataContract]
    public abstract class BaseSendPaymentConfirmationEmailMessage<TMessage> : UniversalMessage<TMessage> where TMessage : BaseSendPaymentConfirmationEmailMessage<TMessage>// Not published seperately. No Consumer.
    {
        [DataMember(Order = 1)]
        public int VpGuarantorId { get; set; }

        [DataMember(Order = 3)]
        public string TransactionNumber { get; set; }

        [DataMember(Order = 15)]
        public bool IsAchType { get; set; }

        [DataMember(Order = 16)]
        public IList<int> PaymentProcessorResponseIds { get; set; }

        [DataMember(Order = 17)]
        public bool IsPromptPayment { get; set; }

        [DataMember(Order = 18)]
        public bool IsTextToPayPayment{ get; set; }

        [IgnoreDataMember]
		public override string ErrorMessage => $"{typeof(TMessage).Name};{nameof(this.VpGuarantorId)}={this.VpGuarantorId};{nameof(this.TransactionNumber)}={this.TransactionNumber}";
    }
}
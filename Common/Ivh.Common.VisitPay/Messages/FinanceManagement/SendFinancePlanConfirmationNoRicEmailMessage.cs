﻿namespace Ivh.Common.VisitPay.Messages.FinanceManagement
{
    using System;
    using System.Runtime.Serialization;
    using ServiceBus.Common.Messages;

    [DataContract]
    [Serializable]
    public class SendFinancePlanConfirmationSimpleTermsEmailMessage : UniversalMessage<SendFinancePlanConfirmationSimpleTermsEmailMessage>
    {
        [DataMember(Order = 1)]
        public int VpGuarantorId { get; set; }

        [DataMember(Order = 2)]
        public int NumberMonthlyPayments { get; set; }

        [IgnoreDataMember]
        public override string ErrorMessage => this.FormatErrorMessage(this, x => x.VpGuarantorId);
    }
}
﻿namespace Ivh.Common.VisitPay.Messages.FinanceManagement
{
    using System;
    using System.Runtime.Serialization;
    using ServiceBus.Attributes;
    using ServiceBus.Common.Messages;

    [Serializable]
    [DataContract]
    [UniversalMessage(consumable: false)]
    public class PaymentImportMessage : UniversalMessage<PaymentImportMessage>
    {
        [DataMember(Order = 1)]
        public string LockBoxNumber { get; set; }

        [DataMember(Order = 2)]
        public DateTime BatchCreditDate { get; set; }

        [DataMember(Order = 3)]
        public string GuarantorAccountNumber { get; set; }

        [DataMember(Order = 4)]
        public string GuarantorFirstName { get; set; }

        [DataMember(Order = 5)]
        public string GuarantorLastName { get; set; }

        [DataMember(Order = 6)]
        public decimal? InvoiceAmount { get; set; }

        [DataMember(Order = 7)]
        public string BatchNumber { get; set; }

        [DataMember(Order = 8)]
        public string PaymentSequenceNumber { get; set; }

        [DataMember(Order = 9)]
        public string CheckPaymentNumber { get; set; }

        [DataMember(Order = 10)]
        public string CheckAccountNumber { get; set; }

        [DataMember(Order = 11)]
        public string CheckRouting { get; set; }

        [DataMember(Order = 12)]
        public string CheckRemitter { get; set; }

        [DataMember(Order = 13)]
        public string PsrId { get; set; }

        [DataMember(Order = 14)]
        public string PaymentComment { get; set; }

        [DataMember(Order = 15)]
        public string EmailAddress { get; set; }

        [DataMember(Order = 16)]
        public string DraftReturnCode { get; set; }

        [DataMember(Order = 17)]
        public string DraftReturnInfo { get; set; }

        [DataMember(Order = 18)]
        public string RdfiBankName { get; set; }

        [IgnoreDataMember]
        public override string ErrorMessage => this.FormatErrorMessage(this, x => x.BatchCreditDate.ToShortDateString(), x => x.BatchCreditDate.ToShortDateString());
    }
}

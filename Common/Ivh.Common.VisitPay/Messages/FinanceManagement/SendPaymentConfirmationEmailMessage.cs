﻿namespace Ivh.Common.VisitPay.Messages.FinanceManagement
{
    using System;
    using System.Runtime.Serialization;

    [DataContract]
    [Serializable]
    public class SendPaymentConfirmationEmailMessage : BaseSendPaymentConfirmationEmailMessage<SendPaymentConfirmationEmailMessage>
    {
    }
}
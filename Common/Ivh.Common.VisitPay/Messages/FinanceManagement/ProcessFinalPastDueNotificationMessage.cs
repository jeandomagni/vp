namespace Ivh.Common.VisitPay.Messages.FinanceManagement
{
    using System;
    using System.Collections.Generic;
    using System.Runtime.Serialization;
    using ServiceBus.Common.Messages;

    [Serializable]
    [DataContract]
    public class ProcessFinalPastDueNotificationMessage : UniversalMessage<ProcessFinalPastDueNotificationMessage>
    {
        [DataMember(Order = 1)]
        public int VpGuarantorId { get; set; }

        [DataMember(Order = 2)]
        public DateTime DateToProcess { get; set; }

        [DataMember(Order = 3)]
        public IList<int> VisitIds { get; set; }

        [DataMember(Order = 4)]
        public bool IsOnFinancePlan { get; set; }

        [IgnoreDataMember]
        public override string ErrorMessage => this.FormatErrorMessage(this, x => x.VpGuarantorId) + $": {nameof(this.VisitIds)}={string.Join(",", this.VisitIds)}";
    }
}
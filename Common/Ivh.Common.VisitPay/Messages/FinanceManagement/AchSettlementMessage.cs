﻿namespace Ivh.Common.VisitPay.Messages.FinanceManagement
{
    using System;
    using System.Runtime.Serialization;
    using ServiceBus.Attributes;
    using ServiceBus.Common.Messages;

    [Serializable]
    [DataContract]
    [UniversalMessage(consumable: false)]
    public class AchSettlementMessage : UniversalMessage<AchSettlementMessage>
    {
        [DataMember(Order = 1)]
        public string TransactionId { get; set; }
        [DataMember(Order = 3)]
        public int AchSettlementDetailId { get; set; }
        [IgnoreDataMember]
		public override string ErrorMessage => this.FormatErrorMessage(this, x => x.AchSettlementDetailId);
    }
}
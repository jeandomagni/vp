﻿namespace Ivh.Common.VisitPay.Messages.FinanceManagement
{
    using System;
    using System.Runtime.Serialization;
    using ServiceBus.Common.Messages;

    [Serializable]
    [DataContract]
    public class BalanceTransferSetVisitActiveMessage : UniversalMessage<BalanceTransferSetVisitActiveMessage>
    {
        [DataMember(Order = 1)]
        public string VisitSourceSystemKey { get; set; }
        [DataMember(Order = 2)]
        public int VisitBillingSystemId { get; set; }
        [IgnoreDataMember]
		public override string ErrorMessage => this.FormatErrorMessage(this, x => x.VisitSourceSystemKey, x => x.VisitBillingSystemId);
    }
}
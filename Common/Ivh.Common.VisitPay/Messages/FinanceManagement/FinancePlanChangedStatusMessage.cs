namespace Ivh.Common.VisitPay.Messages.FinanceManagement
{
    using System;
    using System.Runtime.Serialization;
    using Enums;
    using ServiceBus.Common.Messages;

    [Serializable]
    [DataContract]
    public class FinancePlanChangedStatusMessage : UniversalMessage<FinancePlanChangedStatusMessage>
    {
        [DataMember(Order = 1)]
        public int FinancePlanId { get; set; }
        [DataMember(Order = 2)]
        public FinancePlanStatusEnum PreviousStatus { get; set; }
        [DataMember(Order = 3)]
        public FinancePlanStatusEnum NewStatus { get; set; }
        [DataMember(Order = 4)]
        public DateTime EventDate { get; set; }

        [IgnoreDataMember]
		public override string ErrorMessage => this.FormatErrorMessage(this, x => x.FinancePlanId, x => x.PreviousStatus, x => x.NewStatus);
    }
}
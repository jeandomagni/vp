﻿namespace Ivh.Common.VisitPay.Messages.FinanceManagement
{
    using System;
    using System.Collections.Generic;
    using System.Runtime.Serialization;
    using ServiceBus.Common.Messages;

    [Serializable]
    [DataContract]
    public class ChangeEventProcessedForVpGuarantor : UniversalMessage<ChangeEventProcessedForVpGuarantor>
    {
        [DataMember(Order = 1)]
        public int ChangeSetId { get; set; }

        [DataMember(Order = 2)]
        public string TypeOfChangeSet { get; set; }

        [DataMember(Order = 3)]
        public List<int> VpGuarantorIds { get; set; }
        
        [DataMember(Order = 4)]
        public List<int> VisitIds { get; set; }

        [IgnoreDataMember]
        public override string ErrorMessage => this.FormatErrorMessage(this, x => x.ChangeSetId);
    }
}
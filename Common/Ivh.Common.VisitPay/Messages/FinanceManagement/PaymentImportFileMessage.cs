﻿namespace Ivh.Common.VisitPay.Messages.FinanceManagement
{
    using System;
    using System.Runtime.Serialization;
    using ServiceBus.Common.Messages;

    [Serializable]
    [DataContract]
    public class PaymentImportFileMessage : UniversalMessageList<PaymentImportMessage>
    {
        [DataMember(Order = 2)]
        public int TotalBatchCount { get; set; }

        [DataMember(Order = 3)]
        public int TotalCheckCount { get; set; }

        [DataMember(Order = 4)]
        public decimal TotalAmount { get; set; }
    }
}

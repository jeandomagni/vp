﻿namespace Ivh.Common.VisitPay.Messages.FinanceManagement
{
    using Ivh.Common.VisitPay.Attributes;
    using ServiceBus.Common.Messages;
    using System;
    using System.Runtime.Serialization;

    [DataContract]
    [Serializable]
    public class SendFinancePlanConfirmationEmailMessage : UniversalMessage<SendFinancePlanConfirmationEmailMessage>
    {
        [DataMember(Order = 1)]
        [Phi]
        public string FinancePlanPublicIdPhi { get; set; }

        [DataMember(Order = 2)]
        public int VpGuarantorId { get; set; }

        [IgnoreDataMember]
        public override string ErrorMessage => this.FormatErrorMessage(this, x => x.VpGuarantorId);
    }
}

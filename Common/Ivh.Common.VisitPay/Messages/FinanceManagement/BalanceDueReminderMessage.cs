namespace Ivh.Common.VisitPay.Messages.FinanceManagement
{
    using System;
    using System.Runtime.Serialization;
    using ServiceBus.Common.Messages;

    [DataContract]
    [Serializable]
    public class BalanceDueReminderMessage : UniversalMessage<BalanceDueReminderMessage>
    {
        [DataMember(Order = 1)]
        public int VpStatementId { get; set; }
        [DataMember(Order = 2)]
        public int VpGuarantorId { get; set; }
        [DataMember(Order = 3)]
        public DateTime PaymentDueDate { get; set; }
        [IgnoreDataMember]
		public override string ErrorMessage => this.FormatErrorMessage(this, x => x.VpGuarantorId, x => x.VpStatementId);
    }
}
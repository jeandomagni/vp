﻿namespace Ivh.Common.VisitPay.Messages.FinanceManagement
{
    using System;
    using System.Runtime.Serialization;
    using ServiceBus.Common.Messages;

    [Serializable]
    [DataContract]
    public class AchReturnMessageGuarantor : UniversalMessageList<AchReturnMessage>
    {
        [DataMember(Order = 1)]
        public int VpGuarantorId { get; set; }
    }
}
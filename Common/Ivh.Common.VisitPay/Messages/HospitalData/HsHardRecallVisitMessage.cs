﻿namespace Ivh.Common.VisitPay.Messages.HospitalData
{
    using System;
    using System.Runtime.Serialization;
    using ServiceBus.Common.Messages;

    [Serializable]
    [DataContract]
    public class HsVisitVpEligibleMessage : UniversalMessage<HsVisitVpEligibleMessage>
    {
        [DataMember(Order = 1)]
        public string SystemSourceKey { get; set; }
        [DataMember(Order = 2)]
        public string HardRecallReason { get; set; }
        [DataMember(Order = 3)]
        public int BillingSystemId { get; set; }
        [DataMember(Order = 4)]
        public bool VpEligible { get; set; }
        [IgnoreDataMember]
		public override string ErrorMessage => this.FormatErrorMessage(this, x => x.BillingSystemId, x => x.SystemSourceKey);

    }
}
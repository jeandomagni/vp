﻿namespace Ivh.Common.VisitPay.Messages.HospitalData
{
    using System;
    using System.Runtime.Serialization;
    using ServiceBus.Common.Messages;

    [DataContract]
    [Serializable]
    public class ChangeSetEventsAggregatedMessage : UniversalMessageList<ChangeSetEventsMessage>
    {
    }
}

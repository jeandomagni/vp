﻿namespace Ivh.Common.VisitPay.Messages.HospitalData
{
    using System;
    using System.Collections.Generic;
    using System.Runtime.Serialization;
    using Dtos;
    using ServiceBus.Common.Messages;

    [Serializable]
    [DataContract]
    public class RealTimeVisitEnrollmentMessage : UniversalMessage<RealTimeVisitEnrollmentMessage>
    {
        [DataMember(Order = 1)]
        public string HsGuarantorSourceSystemKey { get; set; }
        [DataMember(Order = 2)]
        public IList<RealTimeEnrollmentAccountDto> GuarantorAccounts { get; set; }
        [DataMember(Order = 3)]
        public int VpGuarantorId { get; set; }
        [DataMember(Order = 4)]
        public EnrollmentResponseDto Response { get; set; }

        [IgnoreDataMember]
        public override string ErrorMessage => this.FormatErrorMessage(this);
    }
}
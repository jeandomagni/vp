namespace Ivh.Common.VisitPay.Messages.HospitalData
{
    using System;
    using System.Runtime.Serialization;
    using ServiceBus.Common.Messages;

    [Serializable]
    [DataContract]
    public class UpdateReconfiguredFinancePlanInterestMessage : UniversalMessage<UpdateReconfiguredFinancePlanInterestMessage>
    {
        [DataMember(Order = 1)]
        public int ReconfiguredFinancePlanId { get; set; }
        [DataMember(Order = 2)]
        public int ParentFinancePlanId { get; set; }
        [DataMember(Order = 3)]
        public decimal ParentFinancePlanInterest { get; set; }
        [DataMember(Order = 4)]
        public string Reason { get; set; }

        [IgnoreDataMember]
		public override string ErrorMessage => this.FormatErrorMessage(this, x => x.ParentFinancePlanId, x => x.ReconfiguredFinancePlanId);
    }
}
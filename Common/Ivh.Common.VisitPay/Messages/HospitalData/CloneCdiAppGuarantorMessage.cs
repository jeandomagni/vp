﻿namespace Ivh.Common.VisitPay.Messages.HospitalData
{
    using System;
    using System.Runtime.Serialization;
    using ServiceBus.Common.Messages;

    [Serializable]
    [DataContract]
    public class CloneCdiAppGuarantorMessage : UniversalMessage<CloneCdiAppGuarantorMessage>
    {
        [DataMember(Order = 1)]
        public int HsGuarantorId { get; set; }

        [DataMember(Order = 2)]
        public int VpGuarantorId { get; set; }

        [DataMember(Order = 3)]
        public string AppendValue { get; set; }

        [DataMember(Order = 4)]
        public bool Consolidate { get; set; }

        [DataMember(Order = 5)]
        public string UserName { get; set; }

        [IgnoreDataMember]
		public override string ErrorMessage => this.FormatErrorMessage(this, "Failed to clone", x => x.HsGuarantorId, x => x.HsGuarantorId);
    }
}
﻿namespace Ivh.Common.VisitPay.Messages.HospitalData
{
    using System;
    using System.Collections.Generic;
    using System.Runtime.Serialization;
    using ServiceBus.Common.Messages;

    [Serializable]
    [DataContract]
    public class VpccGuarantorEnrolledInVisitPayMessage : UniversalMessage<VpccGuarantorEnrolledInVisitPayMessage>
    {
        [DataMember(Order = 1)]
        public IList<int> HsGuarantorIds { get; set; }
        [DataMember(Order = 2)]
        public int VpGuarantorId { get; set; }
        [IgnoreDataMember]
        public override string ErrorMessage => this.FormatErrorMessage(this, x => x.VpGuarantorId) + $": {nameof(this.HsGuarantorIds)}={string.Join(",", this.HsGuarantorIds)}";
    }
}

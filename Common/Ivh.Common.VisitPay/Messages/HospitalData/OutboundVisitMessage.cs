﻿namespace Ivh.Common.VisitPay.Messages.HospitalData
{
    using System;
    using System.Runtime.Serialization;
    using Enums;
    using ServiceBus.Common.Messages;

    [Serializable]
    [DataContract]
    public class OutboundVisitMessage : UniversalMessage<OutboundVisitMessage>
    {
        [DataMember(Order = 1)]
        public int VpVisitId { get; set; }

        [DataMember(Order = 2)]
        public int BillingSystemId { get; set; }

        [DataMember(Order = 3)]
        public string SourceSystemKey { get; set; }

        [DataMember(Order = 4)]
        public VpOutboundVisitMessageTypeEnum VpOutboundVisitMessageType { get; set; }

        [DataMember(Order = 5)]
        public DateTime ActionDate { get; set; }

        [DataMember(Order = 6)]
        public string OutboundValue { get; set; }

        [IgnoreDataMember]
		public override string ErrorMessage => this.FormatErrorMessage(this, x => x.BillingSystemId, x => x.SourceSystemKey);
        
        [DataMember(Order = 7)]
        public FinancePlanClosedReasonEnum? FinancePlanClosedReasonEnum { get; set; }

        [DataMember(Order = 8)]
        public int? OriginalFinancePlanDuration { get; set; }

        [DataMember(Order = 9)]
        public bool? IsCallCenter { get; set; }

        [DataMember(Order = 10)]
        public bool? IsAutoPay { get; set; }
    }
}
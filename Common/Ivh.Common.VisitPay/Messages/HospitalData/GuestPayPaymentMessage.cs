﻿namespace Ivh.Common.VisitPay.Messages.HospitalData
{
    using System;
    using System.Runtime.Serialization;
    using Enums;
    using ServiceBus.Common.Messages;

    [Serializable]
    [DataContract]
    public class GuestPayPaymentMessage : UniversalMessage<GuestPayPaymentMessage>
    {
        [DataMember(Order = 1)]
        public string VpGuarantorEmailAddress { get; set; }
        [DataMember(Order = 2)]
        public string VpGuarantorNumber { get; set; }
        [DataMember(Order = 3)]
        public string VpGuarantorLastName { get; set; }
        [DataMember(Order = 4)]
        public string VpGuarantorSourceSystemKey { get; set; }
        [DataMember(Order = 5)]
        public int VpPaymentUnitBillingSystemId { get; set; }
        [DataMember(Order = 6)]
        public string VpPaymentUnitSourceSystemKey { get; set; }
        [DataMember(Order = 7)]
        public int? VpPaymentUnitId { get; set; }
        [DataMember(Order = 8)]
        public int VpPaymentId { get; set; }
        [DataMember(Order = 9)]
        public decimal VpPaymentAmount { get; set; }
        [DataMember(Order = 10)]
        public DateTime? VpPaymentDate { get; set; }
        [DataMember(Order = 11)]
        public int VpPaymentMethodTypeId { get; set; }
        [DataMember(Order = 12)]
        public int? MerchantAccountId { get; set; }
        [DataMember(Order=13)]
        public int StatementIdentifierId { get; set; }
        [DataMember(Order=14)]
        public virtual PaymentSystemTypeEnum PaymentSystemType { get; set; }
        [DataMember(Order=15)]
        public virtual AchSettlementTypeEnum? AchSettlementType { get; set; }
        [IgnoreDataMember]
		public override string ErrorMessage => this.FormatErrorMessage(this, x => x.VpPaymentId, x => x.VpPaymentUnitId);
    }
}
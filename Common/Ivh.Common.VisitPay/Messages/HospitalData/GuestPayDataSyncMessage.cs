﻿namespace Ivh.Common.VisitPay.Messages.HospitalData
{
    using System;
    using System.Runtime.Serialization;
    using ServiceBus.Common.Messages;

    [Serializable]
    [DataContract]
    public class GuestPayDataSyncMessage : UniversalMessage<GuestPayDataSyncMessage>
    {
        [IgnoreDataMember]
        public override string ErrorMessage => this.FormatErrorMessage(this);
    }
}

﻿namespace Ivh.Common.VisitPay.Messages.HospitalData
{
    using System;
    using System.Collections.Generic;
    using System.Runtime.Serialization;
    using ServiceBus.Common.Messages;

    /// <summary>
    /// This message is ment for to distribute the processing of the internal visit changes for the normalized hospital data.
    /// Aka:  Communicate from the hospital layer (ETL) back to the Hospital layer that a new ChangeEvent has been generated.
    /// </summary>
    [Serializable]
    [DataContract]
    public class ChangeSetEventsMessage : UniversalMessage<ChangeSetEventsMessage>
    {
        [DataMember(Order = 1)]
        public IList<int> ChangeEventIds { get; set; }

        [IgnoreDataMember]
		public override string ErrorMessage => this.FormatErrorMessage(this, x => string.Join(",", x.ChangeEventIds));
    }
}
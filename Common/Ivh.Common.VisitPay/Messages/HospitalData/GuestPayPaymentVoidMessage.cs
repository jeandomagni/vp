﻿namespace Ivh.Common.VisitPay.Messages.HospitalData
{
    using System;
    using System.Runtime.Serialization;
    using ServiceBus.Common.Messages;

    [Serializable]
    [DataContract]
    public class GuestPayPaymentVoidMessage : UniversalMessage<GuestPayPaymentVoidMessage>
    {
        [DataMember(Order = 1)]
        public int VpPaymentUnitId { get; set; }
        [DataMember(Order = 2)]
        public int VpPaymentId { get; set; }
        [IgnoreDataMember]
		public override string ErrorMessage => this.FormatErrorMessage(this, x => x.VpPaymentId, x => x.VpPaymentUnitId);
    }
}
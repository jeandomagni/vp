﻿namespace Ivh.Common.VisitPay.Messages.HospitalData
{
    using System;
    using System.Runtime.Serialization;
    using ServiceBus.Common.Messages;

    [Serializable]
    [DataContract]
    public class FinancePlanClosedMessage : UniversalMessage<FinancePlanClosedMessage>
    {
        [DataMember(Order = 1)]
        public int FinancePlanId { get; set; }

        [DataMember(Order = 2)]
        public int VpGuarantorId { get; set; }

        [IgnoreDataMember]
		public override string ErrorMessage => this.FormatErrorMessage(this, x => x.FinancePlanId, x => x.VpGuarantorId);
    }
}
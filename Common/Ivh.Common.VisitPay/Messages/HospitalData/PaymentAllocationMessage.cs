﻿namespace Ivh.Common.VisitPay.Messages.HospitalData
{
    using System;
    using System.Runtime.Serialization;
    using Enums;
    using ServiceBus.Common.Messages;

    [Serializable]
    [DataContract]
    public class PaymentAllocationMessage : UniversalMessage<PaymentAllocationMessage>
    {
        //[DataMember(Order = 1)]
        //public int VpVisitTransactionId { get; set; }

        [DataMember(Order = 2)]
        public int? VpPaymentAllocationId { get; set; }

        //[DataMember(Order = 3)]
        //public int VpTransactionTypeId { get; set; }

        //[DataMember(Order = 4)]
        //public DateTime TransactionDate { get; set; }

        [DataMember(Order = 5)]
        public decimal AllocationAmount { get; set; }

        [DataMember(Order = 6)]
        public int VisitBillingSystemId { get; set; }

        [DataMember(Order = 7)]
        public string VisitSourceSystemKey { get; set; }

        [DataMember(Order = 8)]
        public int? VpPaymentId { get; set; }

        [DataMember(Order = 9)]
        public PaymentTypeEnum VpPaymentType { get; set; }

        [DataMember(Order = 10)]
        public DateTime? VpPaymentActualPaymentDate { get; set; }

        [DataMember(Order = 11)]
        public decimal VpPaymentAmount { get; set; }

        [DataMember(Order = 12)]
        public DateTime? VpPaymentAllocationInsertDate { get; set; }

        //[DataMember(Order = 13)]
        //public int? VpVisitTransactionOpposingVisitTransactionId { get; set; }

        [DataMember(Order = 14)]
        public int? VpParentPaymentId { get; set; }

        [DataMember(Order = 15)]
        public PaymentMethodTypeEnum? VpPaymentMethodType { get; set; }

        [DataMember(Order = 16)]
        public int? VpFinancePlanId { get; set; }

        [DataMember(Order = 17)]
        public int? VpFinancePlanCurrentTotalDuration { get; set; }

        [DataMember(Order = 18)]
        public int? VpGuarantorId { get; set; }

        [DataMember(Order = 19)]
        public int? BalanceTransferStatusId { get; set; }

        [DataMember(Order = 20)]
        public int? MerchantAccountId { get; set; }

        [DataMember(Order = 21)]
        public int VpPaymentAllocationTypeId { get; set; }

        [DataMember(Order = 22)]
        public int? VpPaymentAllocationOpposingPaymentAllocationId { get; set; }

        [DataMember(Order = 23)]
        public int? VpFinancePlanOriginalDuration { get; set; }

        [DataMember(Order = 24)]
        public AchSettlementTypeEnum? AchSettlementType { get; set; }

        [IgnoreDataMember]
		public override string ErrorMessage => this.FormatErrorMessage(this, x => x.VpGuarantorId, x => x.VpPaymentAllocationId);

    }
}
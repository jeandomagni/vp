namespace Ivh.Common.VisitPay.Messages.HospitalData
{
    using System;
    using System.Collections.Generic;
    using System.Runtime.Serialization;
    using Dtos;
    using ServiceBus.Common.Messages;

    /// <summary>
    /// This message is to communicate the normalized visit data to other systems
    /// Example:  From the ETL layer to VisitPay.
    /// </summary>
    [Serializable]
    [DataContract]
    public class ChangeSetMessage : UniversalMessage<ChangeSetMessage>
    {
        [DataMember(Order = 1)]
        public ChangeEventMessage ChangeEvent { get; set; }
        [DataMember(Order = 2)]
        public IList<VisitChangeDto> VisitChanges { get; set; }
        [DataMember(Order = 3)]
        public IList<VisitTransactionChangeDto> VisitTransactionChanges { get; set; }
        [DataMember(Order = 4)]
        public IList<HsGuarantorDto> HsGuarantors { get; set; }
        [DataMember(Order = 5)]
        public IList<VpGuarantorHsMatchDto> VpGuarantorHsMatches { get; set; }
        [DataMember(Order = 6)]
        public IList<HsGuarantorChangeDto> HsGuarantorChanges { get; set; }

        [IgnoreDataMember]
        public override string ErrorMessage => this.FormatErrorMessage(this) + $"{nameof(this.ChangeEvent.ChangeEventId)}={this.ChangeEvent.ChangeEventId}";
    }
}
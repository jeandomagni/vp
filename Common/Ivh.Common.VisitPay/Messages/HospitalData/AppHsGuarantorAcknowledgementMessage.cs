﻿namespace Ivh.Common.VisitPay.Messages.HospitalData
{
    using System;
    using System.Runtime.Serialization;
    using Dtos;
    using ServiceBus.Common.Messages;

    [Serializable]
    [DataContract]
    public class AppHsGuarantorAcknowledgementMessage: UniversalMessage<AppHsGuarantorAcknowledgementMessage>
    {
        [DataMember(Order = 1)]
        public HsGuarantorAcknowledgementDto HsGuarantorAcknowledgement { get; set; }

        [IgnoreDataMember]
		public override string ErrorMessage => this.FormatErrorMessage(this) + $"{nameof(this.HsGuarantorAcknowledgement.VpGuarantorId)}={this.HsGuarantorAcknowledgement.VpGuarantorId};{nameof(this.HsGuarantorAcknowledgement.HsGuarantorId)}={this.HsGuarantorAcknowledgement.HsGuarantorId}";
    }
}
﻿
namespace Ivh.Common.VisitPay.Messages.HospitalData
{
    using ServiceBus.Common.Messages;
    using System;
    using System.Runtime.Serialization;

    [Serializable]
    [DataContract]
    public class OutboundPaperFileMessage : UniversalMessage<OutboundPaperFileMessage>
    {
        [DataMember(Order = 1)]
        public Guid FileStorageExternalKey { get; set; }

        [DataMember(Order = 2)]
        public string FileName { get; set; }

        [DataMember(Order = 3)]
        public int FileTypeId { get; set; }

        [IgnoreDataMember]
        public override string ErrorMessage => this.FormatErrorMessage(this, x => x.FileStorageExternalKey);
    }
}

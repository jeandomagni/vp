﻿namespace Ivh.Common.VisitPay.Messages.HospitalData
{
    using System;
    using System.Runtime.Serialization;
    using ServiceBus.Common.Messages;

    [Serializable]
    [DataContract]
    public class HsGuarantorRematchMessage : UniversalMessage<HsGuarantorRematchMessage>
    {
        [DataMember(Order = 1)]
        public int HsGuarantorId { get; set; }
        [DataMember(Order = 2)]
        public int VpGuarantorId { get; set; }
        [IgnoreDataMember]
		public override string ErrorMessage => this.FormatErrorMessage(this, x => x.VpGuarantorId, x => x.HsGuarantorId);
    }
}
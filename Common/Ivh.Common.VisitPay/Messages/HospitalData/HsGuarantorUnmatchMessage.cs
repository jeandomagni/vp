﻿namespace Ivh.Common.VisitPay.Messages.HospitalData
{
    using System;
    using System.Runtime.Serialization;
    using Enums;
    using ServiceBus.Common.Messages;

    [Serializable]
    [DataContract]
    public class HsGuarantorUnmatchMessage : UniversalMessage<HsGuarantorUnmatchMessage>
    {
        [DataMember(Order = 1)]
        public int VpGuarantorId { get; set; }
        [DataMember(Order = 2)]
        public int HsGuarantorId { get; set; }
        [DataMember(Order = 3)]
        public HsGuarantorMatchStatusEnum HsGuarantorMatchStatus { get; set; }

        [IgnoreDataMember]
		public override string ErrorMessage => this.FormatErrorMessage(this, x => x.VpGuarantorId, x => x.HsGuarantorId);
    }
}
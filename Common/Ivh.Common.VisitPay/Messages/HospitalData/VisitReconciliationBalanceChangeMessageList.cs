﻿namespace Ivh.Common.VisitPay.Messages.HospitalData
{
    using System;
    using System.Runtime.Serialization;
    using ServiceBus.Common.Messages;

    [Serializable]
    [DataContract]
    public class VisitReconciliationBalanceChangeMessageList : UniversalMessageList<VisitReconciliationBalanceChangeMessage>
    {
    }
}
﻿namespace Ivh.Common.VisitPay.Messages.HospitalData
{
    using System;
    using System.Runtime.Serialization;
    using Enums;
    using ServiceBus.Attributes;
    using ServiceBus.Common.Messages;

    /// <summary>
    /// This message is ment for to distribute the processing of the internal visit changes for the normalized hospital data.
    /// Aka:  Communicate from the hospital layer (ETL) back to the Hospital layer that a new ChangeEvent has been generated.
    /// </summary>
    [Serializable]
    [DataContract]
    [UniversalMessage(consumable: false)]
    public class ChangeEventMessage : UniversalMessage<ChangeEventMessage>
    // Not published seperately. No Consumer.
    {
        [DataMember(Order = 1)]
        public int ChangeEventId { get; set; }
        [DataMember(Order = 2)]
        public ChangeEventTypeEnum ChangeEventType { get; set; }
        [DataMember(Order = 3)]
        public ChangeEventStatusEnum ChangeEventStatus { get; set; }
        [DataMember(Order = 4)]
        public DateTime ChangeEventDateTime { get; set; }
        [DataMember(Order = 5)]
        public string EventTriggerDescription { get; set; }
        [DataMember(Order = 6)]
        public int? VisitId { get; set; }
        [DataMember(Order = 7)]
        public int? HsGuarantorId { get; set; }

        [IgnoreDataMember]
		public override string ErrorMessage => this.FormatErrorMessage(this, x => x.ChangeEventId);
    }
}
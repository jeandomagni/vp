﻿namespace Ivh.Common.VisitPay.Messages.HospitalData
{
    using System;
    using System.Runtime.Serialization;
    using Enums;
    using ServiceBus.Common.Messages;

    [Serializable]
    [DataContract]
    public class VisitReconciliationBalanceChangeMessage : UniversalMessage<VisitReconciliationBalanceChangeMessage>
    {
        [DataMember(Order = 1)]
        public int VpVisitId { get; set; }
        [DataMember(Order = 2)]
        public int VpVisitBillingSystemId { get; set; }
        [DataMember(Order = 3)]
        public string VpVisitSourceSystemKey { get; set; }


        [DataMember(Order = 4)]
        public decimal VpVisitBalance { get; set; }
        [DataMember(Order = 5)]
        public decimal SumOfVppInterestPaid { get; set; }
        [DataMember(Order = 6)]
        public decimal SumOfVppInterestAssessed { get; set; }
        [DataMember(Order = 7)]
        public decimal SumOfAchUnsettledPrincipal { get; set; }
        [DataMember(Order = 8)]
        public decimal SumOfAchUnsettledInterest { get; set; }
        [DataMember(Order = 9)]
        public DateTime LastTransactionDate { get; set; }
        [DataMember(Order = 10)]
        public VisitStateEnum VisitState { get; set; }

        [IgnoreDataMember]
		public override string ErrorMessage => this.FormatErrorMessage(this, x => x.VpVisitId);
    }
}
﻿namespace Ivh.Common.VisitPay.Messages.HospitalData
{
    using System;
    using System.Runtime.Serialization;
    using ServiceBus.Common.Messages;

    [Serializable]
    [DataContract]
    public class ChangeEventProcessedMessage : UniversalMessage<ChangeEventProcessedMessage>
    {
        [DataMember(Order = 1)]
        public int ChangeEventId { get; set; }

        [IgnoreDataMember]
		public override string ErrorMessage => this.FormatErrorMessage(this, x => x.ChangeEventId);
    }
}
﻿namespace Ivh.Common.VisitPay.Messages.HospitalData
{
    using System;
    using System.Runtime.Serialization;
    using Enums;
    using ServiceBus.Common.Messages;

    [Serializable]
    [DataContract]
    public class PaymentMessage : UniversalMessage<PaymentMessage>
    {
        [DataMember(Order = 1)]
        public int VpGuarantorId { get; set; }

        [DataMember(Order = 2)]
        public int VpPaymentId { get; set; }

        [DataMember(Order = 3)]
        public int VpPaymentMethodTypeId { get; set; }

        [DataMember(Order = 4)]
        public int VpPaymentStatusId { get; set; }

        [DataMember(Order = 5)]
        public int VpPaymentTypeId { get; set; }

        [DataMember(Order = 6)]
        public decimal FullPaymentAmount { get; set; }

        [DataMember(Order = 7)]
        public DateTime? ActualPaymentDate { get; set; }

        [DataMember(Order = 8)]
        public int? OffsetPaymentId { get; set; }

        [DataMember(Order = 9)]
        public decimal? OffsetPaymentAmount { get; set; }

        [DataMember(Order = 10)]
        public DateTime? OffsetPaymentDate { get; set; }

        [DataMember(Order = 11)]
        public AchSettlementTypeEnum? AchSettlementType { get; set; }

        [IgnoreDataMember]
		public override string ErrorMessage => this.FormatErrorMessage(this, x => x.VpGuarantorId, x => x.VpPaymentId);
    }
}
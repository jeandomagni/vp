﻿namespace Ivh.Common.VisitPay.Messages.HospitalData
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Runtime.Serialization;
    using ServiceBus.Common.Messages;

    [Serializable]
    [DataContract]
    public class MatchReconciliationMessage : UniversalMessage<MatchReconciliationMessage>
    {
        [DataMember(Order = 1)]
        public IList<int> VpGuarantorIds { get; set; }

        [IgnoreDataMember]
        public override string ErrorMessage => this.FormatErrorMessage(this, string.Join(",", this.VpGuarantorIds.Select(x => x.ToString())));
    }
}
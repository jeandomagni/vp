﻿namespace Ivh.Common.VisitPay.Messages.HospitalData
{
    using System;
    using System.Runtime.Serialization;
    using ServiceBus.Common.Messages;

    [Serializable]
    [DataContract]
    public class EtlHsGuarantorCancelMessage : UniversalMessage<EtlHsGuarantorCancelMessage>
    {
        [DataMember(Order = 1)]
        public int VpGuarantorId { get; set; }

        [IgnoreDataMember]
		public override string ErrorMessage => this.FormatErrorMessage(this, x => x.VpGuarantorId);
    }
}
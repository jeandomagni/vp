﻿namespace Ivh.Common.VisitPay.Messages.HospitalData
{
    using System;
    using System.Runtime.Serialization;
    using Enums;
    using ServiceBus.Common.Messages;

    [Serializable]
    [DataContract]
    public class HsGuarantorVisitUnmatchMessage : UniversalMessage<HsGuarantorVisitUnmatchMessage>
    {
        [DataMember(Order = 1)]
        public int VpGuarantorId { get; set; }
        [DataMember(Order = 2)]
        public int HsGuarantorId { get; set; }
        [DataMember(Order = 3)]
        public int BillingSystemId { get; set; }
        [DataMember(Order = 4)]
        public string SourceSystemKey { get; set; }
        [DataMember(Order = 5)]
        public HsGuarantorMatchStatusEnum HsGuarantorMatchStatus { get; set; }
        [DataMember(Order = 6)]
        public int VpVisitId { get; set; }
        [DataMember(Order = 7)]
        public int? ActionVisitPayUserId { get; set; }
        [IgnoreDataMember]
		public override string ErrorMessage => this.FormatErrorMessage(this, x => x.VpGuarantorId, x => x.HsGuarantorId);
    }
}
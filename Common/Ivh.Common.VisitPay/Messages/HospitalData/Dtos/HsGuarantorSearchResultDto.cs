namespace Ivh.Common.VisitPay.Messages.HospitalData.Dtos
{
    using System;
    using System.Runtime.Serialization;
    using Utilities.Helpers;

    [Serializable]
    [DataContract]
    public class HsGuarantorSearchResultDto
    {
        [DataMember(Order = 1)]
        public int HsGuarantorID { get; set; }
        [DataMember(Order = 2)]
        public int HsBillingSystemId { get; set; }
        [DataMember(Order = 3)]
        public string SourceSystemKey { get; set; }
        [DataMember(Order = 4)]
        public string FirstName { get; set; }
        [DataMember(Order = 5)]
        public string LastName { get; set; }
        [DataMember(Order = 6)]
        public virtual int? VpGuarantorID { get; set; }
        [DataMember(Order = 7)]
        public virtual int VisitCount { get; set; }
        [IgnoreDataMember]
        public string LastNameFirstName => FormatHelper.LastNameFirstName(this.LastName, this.FirstName);
        [IgnoreDataMember]
        public string FirstNameLastName => FormatHelper.FirstNameLastName(this.FirstName, this.LastName);
    }
}
﻿namespace Ivh.Common.VisitPay.Messages.HospitalData.Dtos
{
    using Attributes;

    public class ScoringStatisticalMonitoringDto
    {
        public int ScoringStatisticalMonitoringId { get; set; }
        public int ScoringMonitoringTypeId { get; set; }
        public int ScoreCount { get; set; }
        public int? ScoringBatchId { get; set; }
        [ScoringStatisticalMonitoringMetric("ScoringMonitor.GuarantorAllBalance30Days.0Count", "true", nameof(ScoringMonitoringThresholdsDto.GuarantorAllBalance30DaysPercent0Min), nameof(ScoringMonitoringThresholdsDto.GuarantorAllBalance30DaysPercent0Max))]
        public decimal GuarantorAllBalance30Days0Count { get; set; }
        [ScoringStatisticalMonitoringMetric("ScoringMonitor.GuarantorAllBalance30Days.OutsideMedianRange", "false", nameof(ScoringMonitoringThresholdsDto.GuarantorAllBalance30DaysMedianMin), nameof(ScoringMonitoringThresholdsDto.GuarantorAllBalance30DaysMedianMax))]
        public decimal GuarantorAllBalance30DaysMedian { get; set; }
        [ScoringStatisticalMonitoringMetric("ScoringMonitor.GuarantorAllBalance31To60Days.0Count", "true", nameof(ScoringMonitoringThresholdsDto.GuarantorAllBalance31To60DaysPercent0Min), nameof(ScoringMonitoringThresholdsDto.GuarantorAllBalance31To60DaysPercent0Max))]
        public decimal GuarantorAllBalance31To60Days0Count { get; set; }
        [ScoringStatisticalMonitoringMetric("ScoringMonitor.GuarantorAllBalance31To60Days.OutsideMedianRange", "false", nameof(ScoringMonitoringThresholdsDto.GuarantorAllBalance31To60DaysMedianMin), nameof(ScoringMonitoringThresholdsDto.GuarantorAllBalance31To60DaysMedianMax))]
        public decimal GuarantorAllBalance31To60DaysMedian { get; set; }
        [ScoringStatisticalMonitoringMetric("ScoringMonitor.GuarantorAllBalance61To90Days.0Count", "true", nameof(ScoringMonitoringThresholdsDto.GuarantorAllBalance61To90DaysPercent0Min), nameof(ScoringMonitoringThresholdsDto.GuarantorAllBalance61To90DaysPercent0Max))]
        public decimal GuarantorAllBalance61To90Days0Count { get; set; }
        [ScoringStatisticalMonitoringMetric("ScoringMonitor.GuarantorAllBalance61To90Days.OutsideMedianRange", "false", nameof(ScoringMonitoringThresholdsDto.GuarantorAllBalance61To90DaysMedianMin), nameof(ScoringMonitoringThresholdsDto.GuarantorAllBalance61To90DaysMedianMax))]
        public decimal GuarantorAllBalance61To90DaysMedian { get; set; }
        [ScoringStatisticalMonitoringMetric("ScoringMonitor.GuarantorAllBalance91To180Days.0Count", "true", nameof(ScoringMonitoringThresholdsDto.GuarantorAllBalance91To180DaysPercent0Min), nameof(ScoringMonitoringThresholdsDto.GuarantorAllBalance91To180DaysPercent0Max))]
        public decimal GuarantorAllBalance91To180Days0Count { get; set; }
        [ScoringStatisticalMonitoringMetric("ScoringMonitor.GuarantorAllBalance91To180Days.OutsideMedianRange", "false", nameof(ScoringMonitoringThresholdsDto.GuarantorAllBalance91To180DaysMedianMin), nameof(ScoringMonitoringThresholdsDto.GuarantorAllBalance91To180DaysMedianMax))]
        public decimal GuarantorAllBalance91To180DaysMedian { get; set; }
        [ScoringStatisticalMonitoringMetric("ScoringMonitor.GuarantorAllBalance181To270Days.0Count", "true", nameof(ScoringMonitoringThresholdsDto.GuarantorAllBalance181To270DaysPercent0Min), nameof(ScoringMonitoringThresholdsDto.GuarantorAllBalance181To270DaysPercent0Max))]
        public decimal GuarantorAllBalance181To270Days0Count { get; set; }
        [ScoringStatisticalMonitoringMetric("ScoringMonitor.GuarantorAllBalance181To270Days.OutsideMedianRange", "false", nameof(ScoringMonitoringThresholdsDto.GuarantorAllBalance181To270DaysMedianMin), nameof(ScoringMonitoringThresholdsDto.GuarantorAllBalance181To270DaysMedianMax))]
        public decimal GuarantorAllBalance181To270DaysMedian { get; set; }
        [ScoringStatisticalMonitoringMetric("ScoringMonitor.GuarantorAllBalance271To365Days.0Count", "true", nameof(ScoringMonitoringThresholdsDto.GuarantorAllBalance271To365DaysPercent0Min), nameof(ScoringMonitoringThresholdsDto.GuarantorAllBalance271To365DaysPercent0Max))]
        public decimal GuarantorAllBalance271To365Days0Count { get; set; }
        [ScoringStatisticalMonitoringMetric("ScoringMonitor.GuarantorAllBalance271To365Days.OutsideMedianRange", "false", nameof(ScoringMonitoringThresholdsDto.GuarantorAllBalance271To365DaysMedianMin), nameof(ScoringMonitoringThresholdsDto.GuarantorAllBalance271To365DaysMedianMax))]
        public decimal GuarantorAllBalance271To365DaysMedian { get; set; }
        [ScoringStatisticalMonitoringMetric("ScoringMonitor.GuarantorInactive120DaysBalance91To180Days.0Count", "true", nameof(ScoringMonitoringThresholdsDto.GuarantorInactive120DaysBalance91To180DaysPercent0Min), nameof(ScoringMonitoringThresholdsDto.GuarantorInactive120DaysBalance91To180DaysPercent0Max))]
        public decimal GuarantorInactive120DaysBalance91To180Days0Count { get; set; }
        [ScoringStatisticalMonitoringMetric("ScoringMonitor.GuarantorInactive120DaysBalance91To180Days.OutsideMedianRange", "false", nameof(ScoringMonitoringThresholdsDto.GuarantorInactive120DaysBalance91To180DaysMedianMin), nameof(ScoringMonitoringThresholdsDto.GuarantorInactive120DaysBalance91To180DaysMedianMax))]
        public decimal GuarantorInactive120DaysBalance91To180DaysMedian { get; set; }
        [ScoringStatisticalMonitoringMetric("ScoringMonitor.GuarantorInactive120DaysBalance181To270Days.0Count", "true", nameof(ScoringMonitoringThresholdsDto.GuarantorInactive120DaysBalance181To270DaysPercent0Min), nameof(ScoringMonitoringThresholdsDto.GuarantorInactive120DaysBalance181To270DaysPercent0Max))]
        public decimal GuarantorInactive120DaysBalance181To270Days0Count { get; set; }
        [ScoringStatisticalMonitoringMetric("ScoringMonitor.GuarantorInactive120DaysBalance181To270Days.OutsideMedianRange", "false", nameof(ScoringMonitoringThresholdsDto.GuarantorInactive120DaysBalance181To270DaysMedianMin), nameof(ScoringMonitoringThresholdsDto.GuarantorInactive120DaysBalance181To270DaysMedianMax))]
        public decimal GuarantorInactive120DaysBalance181To270DaysMedian { get; set; }
        [ScoringStatisticalMonitoringMetric("ScoringMonitor.GuarantorInactive120DaysBalance271To365Days.0Count", "true", nameof(ScoringMonitoringThresholdsDto.GuarantorInactive120DaysBalance271To365DaysPercent0Min), nameof(ScoringMonitoringThresholdsDto.GuarantorInactive120DaysBalance271To365DaysPercent0Max))]
        public decimal GuarantorInactive120DaysBalance271To365Days0Count { get; set; }
        [ScoringStatisticalMonitoringMetric("ScoringMonitor.GuarantorInactive120DaysBalance271To365Days.OutsideMedianRange", "false", nameof(ScoringMonitoringThresholdsDto.GuarantorInactive120DaysBalance271To365DaysMedianMin), nameof(ScoringMonitoringThresholdsDto.GuarantorInactive120DaysBalance271To365DaysMedianMax))]
        public decimal GuarantorInactive120DaysBalance271To365DaysMedian { get; set; }
        [ScoringStatisticalMonitoringMetric("ScoringMonitor.GuarantorPaidOffAccountsWPatientPaymentsNum30Days.0Count", "true", nameof(ScoringMonitoringThresholdsDto.GuarantorPaidOffAccountsWPatientPaymentsNum30DaysPercent0Min), nameof(ScoringMonitoringThresholdsDto.GuarantorPaidOffAccountsWPatientPaymentsNum30DaysPercent0Max))]
        public decimal GuarantorPaidOffAccountsWPatientPaymentsNum30Days0Count { get; set; }
        [ScoringStatisticalMonitoringMetric("ScoringMonitor.GuarantorPaidOffAccountsWPatientPaymentsNum30Days.OutsideMedianRange", "false", nameof(ScoringMonitoringThresholdsDto.GuarantorPaidOffAccountsWPatientPaymentsNum30DaysMedianMin), nameof(ScoringMonitoringThresholdsDto.GuarantorPaidOffAccountsWPatientPaymentsNum30DaysMedianMax))]
        public decimal GuarantorPaidOffAccountsWPatientPaymentsNum30DaysMedian { get; set; }
        [ScoringStatisticalMonitoringMetric("ScoringMonitor.GuarantorPaidOffAccountsWPatientPaymentsNum31To60Days.0Count", "true", nameof(ScoringMonitoringThresholdsDto.GuarantorPaidOffAccountsWPatientPaymentsNum31To60DaysPercent0Min), nameof(ScoringMonitoringThresholdsDto.GuarantorPaidOffAccountsWPatientPaymentsNum31To60DaysPercent0Max))]
        public decimal GuarantorPaidOffAccountsWPatientPaymentsNum31To60Days0Count { get; set; }
        [ScoringStatisticalMonitoringMetric("ScoringMonitor.GuarantorPaidOffAccountsWPatientPaymentsNum31To60Days.OutsideMedianRange", "false", nameof(ScoringMonitoringThresholdsDto.GuarantorPaidOffAccountsWPatientPaymentsNum31To60DaysMedianMin), nameof(ScoringMonitoringThresholdsDto.GuarantorPaidOffAccountsWPatientPaymentsNum31To60DaysMedianMax))]
        public decimal GuarantorPaidOffAccountsWPatientPaymentsNum31To60DaysMedian { get; set; }
        [ScoringStatisticalMonitoringMetric("ScoringMonitor.GuarantorPaidOffAccountsWPatientPaymentsNum61To90Days.0Count", "true", nameof(ScoringMonitoringThresholdsDto.GuarantorPaidOffAccountsWPatientPaymentsNum61To90DaysPercent0Min), nameof(ScoringMonitoringThresholdsDto.GuarantorPaidOffAccountsWPatientPaymentsNum61To90DaysPercent0Max))]
        public decimal GuarantorPaidOffAccountsWPatientPaymentsNum61To90Days0Count { get; set; }
        [ScoringStatisticalMonitoringMetric("ScoringMonitor.GuarantorPaidOffAccountsWPatientPaymentsNum61To90Days.OutsideMedianRange", "false", nameof(ScoringMonitoringThresholdsDto.GuarantorPaidOffAccountsWPatientPaymentsNum61To90DaysMedianMin), nameof(ScoringMonitoringThresholdsDto.GuarantorPaidOffAccountsWPatientPaymentsNum61To90DaysMedianMax))]
        public decimal GuarantorPaidOffAccountsWPatientPaymentsNum61To90DaysMedian { get; set; }
        [ScoringStatisticalMonitoringMetric("ScoringMonitor.GuarantorPaidOffAccountsWPatientPaymentsNum91To180Days.0Count", "true", nameof(ScoringMonitoringThresholdsDto.GuarantorPaidOffAccountsWPatientPaymentsNum91To180DaysPercent0Min), nameof(ScoringMonitoringThresholdsDto.GuarantorPaidOffAccountsWPatientPaymentsNum91To180DaysPercent0Max))]
        public decimal GuarantorPaidOffAccountsWPatientPaymentsNum91To180Days0Count { get; set; }
        [ScoringStatisticalMonitoringMetric("ScoringMonitor.GuarantorPaidOffAccountsWPatientPaymentsNum91To180Days.OutsideMedianRange", "false", nameof(ScoringMonitoringThresholdsDto.GuarantorPaidOffAccountsWPatientPaymentsNum91To180DaysMedianMin), nameof(ScoringMonitoringThresholdsDto.GuarantorPaidOffAccountsWPatientPaymentsNum91To180DaysMedianMax))]
        public decimal GuarantorPaidOffAccountsWPatientPaymentsNum91To180DaysMedian { get; set; }
        [ScoringStatisticalMonitoringMetric("ScoringMonitor.GuarantorPaidOffAccountsPatientPayments30Days.0Count", "true", nameof(ScoringMonitoringThresholdsDto.GuarantorPaidOffAccountsPatientPayments30DaysPercent0Min), nameof(ScoringMonitoringThresholdsDto.GuarantorPaidOffAccountsPatientPayments30DaysPercent0Max))]
        public decimal GuarantorPaidOffAccountsPatientPayments30Days0Count { get; set; }
        [ScoringStatisticalMonitoringMetric("ScoringMonitor.GuarantorPaidOffAccountsPatientPayments30Days.OutsideMedianRange", "false", nameof(ScoringMonitoringThresholdsDto.GuarantorPaidOffAccountsPatientPayments30DaysMedianMin), nameof(ScoringMonitoringThresholdsDto.GuarantorPaidOffAccountsPatientPayments30DaysMedianMax))]
        public decimal GuarantorPaidOffAccountsPatientPayments30DaysMedian { get; set; }
        [ScoringStatisticalMonitoringMetric("ScoringMonitor.GuarantorPaidOffAccountsPatientPayments31To60Days.0Count", "true", nameof(ScoringMonitoringThresholdsDto.GuarantorPaidOffAccountsPatientPayments31To60DaysPercent0Min), nameof(ScoringMonitoringThresholdsDto.GuarantorPaidOffAccountsPatientPayments31To60DaysPercent0Max))]
        public decimal GuarantorPaidOffAccountsPatientPayments31To60Days0Count { get; set; }
        [ScoringStatisticalMonitoringMetric("ScoringMonitor.GuarantorPaidOffAccountsPatientPayments31To60Days.OutsideMedianRange", "false", nameof(ScoringMonitoringThresholdsDto.GuarantorPaidOffAccountsPatientPayments31To60DaysMedianMin), nameof(ScoringMonitoringThresholdsDto.GuarantorPaidOffAccountsPatientPayments31To60DaysMedianMax))]
        public decimal GuarantorPaidOffAccountsPatientPayments31To60DaysMedian { get; set; }
        [ScoringStatisticalMonitoringMetric("ScoringMonitor.GuarantorPaidOffAccountsPatientPayments61To90Days.0Count", "true", nameof(ScoringMonitoringThresholdsDto.GuarantorPaidOffAccountsPatientPayments61To90DaysPercent0Min), nameof(ScoringMonitoringThresholdsDto.GuarantorPaidOffAccountsPatientPayments61To90DaysPercent0Max))]
        public decimal GuarantorPaidOffAccountsPatientPayments61To90Days0Count { get; set; }
        [ScoringStatisticalMonitoringMetric("ScoringMonitor.GuarantorPaidOffAccountsPatientPayments61To90Days.OutsideMedianRange", "false", nameof(ScoringMonitoringThresholdsDto.GuarantorPaidOffAccountsPatientPayments61To90DaysMedianMin), nameof(ScoringMonitoringThresholdsDto.GuarantorPaidOffAccountsPatientPayments61To90DaysMedianMax))]
        public decimal GuarantorPaidOffAccountsPatientPayments61To90DaysMedian { get; set; }
        [ScoringStatisticalMonitoringMetric("ScoringMonitor.GuarantorPaidOffAccountsPatientPayments91To180Days.0Count", "true", nameof(ScoringMonitoringThresholdsDto.GuarantorPaidOffAccountsPatientPayments91To180DaysPercent0Min), nameof(ScoringMonitoringThresholdsDto.GuarantorPaidOffAccountsPatientPayments91To180DaysPercent0Max))]
        public decimal GuarantorPaidOffAccountsPatientPayments91To180Days0Count { get; set; }
        [ScoringStatisticalMonitoringMetric("ScoringMonitor.GuarantorPaidOffAccountsPatientPayments91To180Days.OutsideMedianRange", "false", nameof(ScoringMonitoringThresholdsDto.GuarantorPaidOffAccountsPatientPayments91To180DaysMedianMin), nameof(ScoringMonitoringThresholdsDto.GuarantorPaidOffAccountsPatientPayments91To180DaysMedianMax))]
        public decimal GuarantorPaidOffAccountsPatientPayments91To180DaysMedian { get; set; }
        [ScoringStatisticalMonitoringMetric("ScoringMonitor.GuarantorPaymentsByDischarge30Days.0Count", "true", nameof(ScoringMonitoringThresholdsDto.GuarantorPaymentsByDischarge30DaysPercent0Min), nameof(ScoringMonitoringThresholdsDto.GuarantorPaymentsByDischarge30DaysPercent0Max))]
        public decimal GuarantorPaymentsByDischarge30Days0Count { get; set; }
        [ScoringStatisticalMonitoringMetric("ScoringMonitor.GuarantorPaymentsByDischarge30Days.OutsideMedianRange", "false", nameof(ScoringMonitoringThresholdsDto.GuarantorPaymentsByDischarge30DaysMedianMin), nameof(ScoringMonitoringThresholdsDto.GuarantorPaymentsByDischarge30DaysMedianMax))]
        public decimal GuarantorPaymentsByDischarge30DaysMedian { get; set; }
        [ScoringStatisticalMonitoringMetric("ScoringMonitor.GuarantorPaymentsByDischarge31To60Days.0Count", "true", nameof(ScoringMonitoringThresholdsDto.GuarantorPaymentsByDischarge31To60DaysPercent0Min), nameof(ScoringMonitoringThresholdsDto.GuarantorPaymentsByDischarge31To60DaysPercent0Max))]
        public decimal GuarantorPaymentsByDischarge31To60Days0Count { get; set; }
        [ScoringStatisticalMonitoringMetric("ScoringMonitor.GuarantorPaymentsByDischarge31To60Days.OutsideMedianRange", "false", nameof(ScoringMonitoringThresholdsDto.GuarantorPaymentsByDischarge31To60DaysMedianMin), nameof(ScoringMonitoringThresholdsDto.GuarantorPaymentsByDischarge31To60DaysMedianMax))]
        public decimal GuarantorPaymentsByDischarge31To60DaysMedian { get; set; }
        [ScoringStatisticalMonitoringMetric("ScoringMonitor.GuarantorPaymentsByDischarge61To90Days.0Count", "true", nameof(ScoringMonitoringThresholdsDto.GuarantorPaymentsByDischarge61To90DaysPercent0Min), nameof(ScoringMonitoringThresholdsDto.GuarantorPaymentsByDischarge61To90DaysPercent0Max))]
        public decimal GuarantorPaymentsByDischarge61To90Days0Count { get; set; }
        [ScoringStatisticalMonitoringMetric("ScoringMonitor.GuarantorPaymentsByDischarge61To90Days.OutsideMedianRange", "false", nameof(ScoringMonitoringThresholdsDto.GuarantorPaymentsByDischarge61To90DaysMedianMin), nameof(ScoringMonitoringThresholdsDto.GuarantorPaymentsByDischarge61To90DaysMedianMax))]
        public decimal GuarantorPaymentsByDischarge61To90DaysMedian { get; set; }
        [ScoringStatisticalMonitoringMetric("ScoringMonitor.GuarantorPaymentsByDischarge91To180Days.0Count", "true", nameof(ScoringMonitoringThresholdsDto.GuarantorPaymentsByDischarge91To180DaysPercent0Min), nameof(ScoringMonitoringThresholdsDto.GuarantorPaymentsByDischarge91To180DaysPercent0Max))]
        public decimal GuarantorPaymentsByDischarge91To180Days0Count { get; set; }
        [ScoringStatisticalMonitoringMetric("ScoringMonitor.GuarantorPaymentsByDischarge91To180Days.OutsideMedianRange", "false", nameof(ScoringMonitoringThresholdsDto.GuarantorPaymentsByDischarge91To180DaysMedianMin), nameof(ScoringMonitoringThresholdsDto.GuarantorPaymentsByDischarge91To180DaysMedianMax))]
        public decimal GuarantorPaymentsByDischarge91To180DaysMedian { get; set; }
        [ScoringStatisticalMonitoringMetric("ScoringMonitor.NoPriorData30Days.OutsideMeanRange", "false", nameof(ScoringMonitoringThresholdsDto.NoPriorData30DaysMeanMin), nameof(ScoringMonitoringThresholdsDto.NoPriorData30DaysMeanMax))]
        public decimal NoPriorData30DaysMean { get; set; }
        [ScoringStatisticalMonitoringMetric("ScoringMonitor.NoPriorData31To60Days.OutsideMeanRange", "false", nameof(ScoringMonitoringThresholdsDto.NoPriorData31To60DaysMeanMin), nameof(ScoringMonitoringThresholdsDto.NoPriorData31To60DaysMeanMax))]
        public decimal NoPriorData31To60DaysMean { get; set; }
        [ScoringStatisticalMonitoringMetric("ScoringMonitor.NoPriorData61To90Days.OutsideMeanRange", "false", nameof(ScoringMonitoringThresholdsDto.NoPriorData61To90DaysMeanMin), nameof(ScoringMonitoringThresholdsDto.NoPriorData61To90DaysMeanMax))]
        public decimal NoPriorData61To90DaysMean { get; set; }
        [ScoringStatisticalMonitoringMetric("ScoringMonitor.NoPriorData91To180Days.OutsideMeanRange", "false", nameof(ScoringMonitoringThresholdsDto.NoPriorData91To180DaysMeanMin), nameof(ScoringMonitoringThresholdsDto.NoPriorData91To180DaysMeanMax))]
        public decimal NoPriorData91To180DaysMean { get; set; }
        [ScoringStatisticalMonitoringMetric("ScoringMonitor.PatientType2.NullCount", "true", nameof(ScoringMonitoringThresholdsDto.PatientType2PercentNullMin), nameof(ScoringMonitoringThresholdsDto.PatientType2PercentNullMax))]
        public decimal PatientType2NullCount { get; set; }
        [ScoringStatisticalMonitoringMetric("ScoringMonitor.PatientType2.OutsideMeanRange", "false", nameof(ScoringMonitoringThresholdsDto.PatientType2MeanMin), nameof(ScoringMonitoringThresholdsDto.PatientType2MeanMax))]
        public decimal PatientType2Mean { get; set; }
        [ScoringStatisticalMonitoringMetric("ScoringMonitor.PrimaryInsuranceTypeN.NullCount", "true", nameof(ScoringMonitoringThresholdsDto.PrimaryInsuranceTypeNPercentNullMin), nameof(ScoringMonitoringThresholdsDto.PrimaryInsuranceTypeNPercentNullMax))]
        public decimal PrimaryInsuranceTypeNNullCount { get; set; }
        [ScoringStatisticalMonitoringMetric("ScoringMonitor.PrimaryInsuranceTypeN.OutsideMeanRange", "false", nameof(ScoringMonitoringThresholdsDto.PrimaryInsuranceTypeNMeanMin), nameof(ScoringMonitoringThresholdsDto.PrimaryInsuranceTypeNMeanMax))]
        public decimal PrimaryInsuranceTypeNMean { get; set; }
        [ScoringStatisticalMonitoringMetric("ScoringMonitor.PrimaryInsuranceTypeA.NullCount", "true", nameof(ScoringMonitoringThresholdsDto.PrimaryInsuranceTypeAPercentNullMin), nameof(ScoringMonitoringThresholdsDto.PrimaryInsuranceTypeAPercentNullMax))]
        public decimal PrimaryInsuranceTypeANullCount { get; set; }
        [ScoringStatisticalMonitoringMetric("ScoringMonitor.PrimaryInsuranceTypeA.OutsideMeanRange", "false", nameof(ScoringMonitoringThresholdsDto.PrimaryInsuranceTypeAMeanMin), nameof(ScoringMonitoringThresholdsDto.PrimaryInsuranceTypeAMeanMax))]
        public decimal PrimaryInsuranceTypeAMean { get; set; }
        [ScoringStatisticalMonitoringMetric("ScoringMonitor.PrimaryInsuranceType1.NullCount", "true", nameof(ScoringMonitoringThresholdsDto.PrimaryInsuranceType1PercentNullMin), nameof(ScoringMonitoringThresholdsDto.PrimaryInsuranceType1PercentNullMax))]
        public decimal PrimaryInsuranceType1NullCount { get; set; }
        [ScoringStatisticalMonitoringMetric("ScoringMonitor.PrimaryInsuranceType1.OutsideMeanRange", "false", nameof(ScoringMonitoringThresholdsDto.PrimaryInsuranceType1MeanMin), nameof(ScoringMonitoringThresholdsDto.PrimaryInsuranceType1MeanMax))]
        public decimal PrimaryInsuranceType1Mean { get; set; }

        public int ThirdPartyDataCount { get; set; }
        [ScoringStatisticalMonitoringMetric("ScoringMonitor.ProbabilityScore.OutsideMedianRange", "false", nameof(ScoringMonitoringThresholdsDto.ProbabilityScoreMedianMin), nameof(ScoringMonitoringThresholdsDto.ProbabilityScoreMedianMax))]
        public decimal ProbabilityScoreMedian { get; set; }
        [ScoringStatisticalMonitoringMetric("ScoringMonitor.OwnRent.NullCount", "true", nameof(ScoringMonitoringThresholdsDto.OwnRentPercentNullMin), nameof(ScoringMonitoringThresholdsDto.OwnRentPercentNullMax))]
        public decimal OwnRentNullCount { get; set; }
        [ScoringStatisticalMonitoringMetric("ScoringMonitor.OwnRentMean.OutsideMeanRange", "false", nameof(ScoringMonitoringThresholdsDto.OwnRentMeanMin), nameof(ScoringMonitoringThresholdsDto.OwnRentMeanMax))]
        public decimal OwnRentMean { get; set; }
        [ScoringStatisticalMonitoringMetric("ScoringMonitor.HouseholdIncome.NullCount", "true", nameof(ScoringMonitoringThresholdsDto.HouseholdIncomePercentNullMin), nameof(ScoringMonitoringThresholdsDto.HouseholdIncomePercentNullMax))]
        public decimal HouseholdIncomeNullCount { get; set; }
        [ScoringStatisticalMonitoringMetric("ScoringMonitor.HouseholdIncome.OutsideMeanRange", "false", nameof(ScoringMonitoringThresholdsDto.HouseholdIncomeMeanMin), nameof(ScoringMonitoringThresholdsDto.HouseholdIncomeMeanMax))]
        public decimal HouseholdIncomeMean { get; set; }
        [ScoringStatisticalMonitoringMetric("ScoringMonitor.HouseholdMemberCount.NullCount", "true", nameof(ScoringMonitoringThresholdsDto.HouseholdMemberCountPercentNullMin), nameof(ScoringMonitoringThresholdsDto.HouseholdMemberCountPercentNullMax))]
        public decimal HouseholdMemberCountNullCount { get; set; }
        [ScoringStatisticalMonitoringMetric("ScoringMonitor.HouseholdMemberCount.OutsideMeanRange", "false", nameof(ScoringMonitoringThresholdsDto.HouseholdMemberCountMeanMin), nameof(ScoringMonitoringThresholdsDto.HouseholdMemberCountMeanMax))]
        public decimal HouseholdMemberCountMean { get; set; }
    }
}
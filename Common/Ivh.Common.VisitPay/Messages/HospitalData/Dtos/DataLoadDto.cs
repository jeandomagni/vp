﻿namespace Ivh.Common.VisitPay.Messages.HospitalData.Dtos
{
    using System;
    using System.Collections.Generic;

    public class DataLoadDto
    {
        public int DataLoadId { get; set; }
        public DateTime DataLoadDateTime { get; set; }
        public IList<ChangeSetDto> ChangeSets { get; set; }
    }
}

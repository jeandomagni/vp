﻿
namespace Ivh.Common.VisitPay.Messages.HospitalData.Dtos
{
    public class ScoringScriptStorage
    {
        public string SourceRScript { get; set; }
        public string RdataUrls { get; set; }
        public string RwrapperPath { get; set; }
    }
}

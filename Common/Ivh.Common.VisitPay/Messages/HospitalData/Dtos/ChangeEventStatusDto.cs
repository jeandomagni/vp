﻿namespace Ivh.Common.VisitPay.Messages.HospitalData.Dtos
{
    public class ChangeEventStatusDto
    {
        public int ChangeEventStatusId { get; set; }
        public string ChangeEventStatusName { get; set; }
        public string ChangeEventStatusDescription { get; set; }
    }
}

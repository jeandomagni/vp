﻿namespace Ivh.Common.VisitPay.Messages.HospitalData.Dtos
{
    public class SegmentationActivePassiveDto
    {
        public int HsGuarantorId { get; set; }
        public int SegmentationBatchId { get; set; }
        public bool HasCashPayment { get; set; }
        public bool AllOldVisitsHavePriorPayment { get; set; }
        public bool SelfPayDatesWithinBounds { get; set; }
        public decimal HsCurrentBalance { get; set; }
        public decimal? PtpScore { get; set; }
        public string ScriptVersion { get; set; }
        public bool IsActive { get; set; }
        public string AssignedAgencyIds { get; set; }
    }
}
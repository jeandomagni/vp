﻿namespace Ivh.Common.VisitPay.Messages.HospitalData.Dtos
{
    public class RealTimeEnrollmentAccountDto
    {
        public string SourceSystemKey { get; set; }
        public int BillingSystemId { get; set; }
        public bool BillingHold { get; set; }
    }
}
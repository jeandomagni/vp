﻿namespace Ivh.Common.VisitPay.Messages.HospitalData.Dtos
{
    using System;
    using System.Runtime.Serialization;
    using Enums;

    [Serializable]
    [DataContract]
    public class VisitTransactionChangeDto
    {
        [DataMember(Order = 1)]
        public int DataLoadId { get; set; }
        [DataMember(Order = 2)]
        public int ChangeEventId { get; set; }
        [DataMember(Order = 3)]
        public ChangeTypeEnum ChangeType { get; set; }
        [DataMember(Order = 4)]
        public int BillingSystemId { get; set; }
        [DataMember(Order = 5)]
        public string SourceSystemKey { get; set; }
        [DataMember(Order = 6)]
        public int VisitId { get; set; }
        [DataMember(Order = 7)]
        public DateTime TransactionDate { get; set; }
        [DataMember(Order = 8)]
        public DateTime PostDate { get; set; }
        [DataMember(Order = 9)]
        public decimal TransactionAmount { get; set; }
        [DataMember(Order = 10)]
        public int VpTransactionTypeId { get; set; }
        [DataMember(Order = 11)]
        public string TransactionDescription { get; set; }
        [DataMember(Order = 12)]
        public int? PaymentAllocationId { get; set; }
        [DataMember(Order = 13)]
        public int TransactionCodeId { get; set; }
        [DataMember(Order = 14)]
        public int VisitTransactionId { get; set; }
        [DataMember(Order = 15)]
        public string TransactionCodeSourceSystemKey { get; set; }
        [DataMember(Order = 16)]
        public int TransactionCodeBillingSystemId { get; set; }
        [DataMember(Order = 18)]
        public bool ReassessmentExempt { get; set; }
    }
}
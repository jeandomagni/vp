namespace Ivh.Common.VisitPay.Messages.HospitalData.Dtos
{
    using System;
    using System.Runtime.Serialization;

    [Serializable]
    [DataContract]
    public class LifeCycleStageDto
    {
        [DataMember(Order = 1)]
        public int LifeCycleStageId { get; set; }
        [DataMember(Order = 2)]
        public string LifeCycleStageName { get; set; }
    }
}
﻿namespace Ivh.Common.VisitPay.Messages.HospitalData.Dtos
{
    public class ProtoScoreDto
    {
        public int ProtoScoreId { get; set; }
        public decimal ProtoScore1 { get; set; }
        public decimal ProtoScore2 { get; set; }
    }
}
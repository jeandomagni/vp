﻿namespace Ivh.Common.VisitPay.Messages.HospitalData.Dtos
{
    using System;

    public class ScoringCalculatedInputDto
    {
        public int HsGuarantorId { get; set; }
        public int VisitId { get; set; }
        public int ScoringBatchId { get; set; }
        public int ScoreId { get; set; }
        public DateTime ScoringDate { get; set; }
        public DateTime FirstSelfPayDate { get; set; }
        public decimal SelfPayAllBalance { get; set; }
        public string PrimaryInsuranceTypeN { get; set; }
        public string PrimaryInsuranceTypeA { get; set; }
        public string PrimaryInsuranceType1 { get; set; }
        public string PatientType2 { get; set; }
        public bool ScoringAccountHasPayorTrans { get; set; }
        public decimal GuarantorAllBalance30Days { get; set; }
        public decimal GuarantorAllBalance31To60Days { get; set; }
        public decimal GuarantorAllBalance61To90Days { get; set; }
        public decimal GuarantorAllBalance91To180Days { get; set; }
        public decimal GuarantorAllBalance181To270Days { get; set; }
        public decimal GuarantorAllBalance271To365Days { get; set; }
        public decimal GuarantorInactive120DaysBalance91To180Days { get; set; }
        public decimal GuarantorInactive120DaysBalance181To270Days { get; set; }
        public decimal GuarantorInactive120DaysBalance271To365Days { get; set; }
        public int GuarantorPaidOffAccountsWPatientPaymentsNum30Days { get; set; }
        public int GuarantorPaidOffAccountsWPatientPaymentsNum31To60Days { get; set; }
        public int GuarantorPaidOffAccountsWPatientPaymentsNum61To90Days { get; set; }
        public int GuarantorPaidOffAccountsWPatientPaymentsNum91To180Days { get; set; }
        public decimal GuarantorPaidOffAccountsPatientPayments30Days { get; set; }
        public decimal GuarantorPaidOffAccountsPatientPayments31To60Days { get; set; }
        public decimal GuarantorPaidOffAccountsPatientPayments61To90Days { get; set; }
        public decimal GuarantorPaidOffAccountsPatientPayments91To180Days { get; set; }
        public decimal GuarantorPaymentsByDischarge30Days { get; set; }
        public decimal GuarantorPaymentsByDischarge31To60Days { get; set; }
        public decimal GuarantorPaymentsByDischarge61To90Days { get; set; }
        public decimal GuarantorPaymentsByDischarge91To180Days { get; set; }
        public bool NoPriorData30Days { get; set; }
        public bool NoPriorData31To60Days { get; set; }
        public bool NoPriorData61To90Days { get; set; }
        public bool NoPriorData91To180Days { get; set; }
        public string Phase1ScriptVersion { get; set; }
        public string Phase2ScriptVersion { get; set; }
    }
}

﻿namespace Ivh.Common.VisitPay.Messages.HospitalData.Dtos
{
    public class ApplicationDto
    {
        public int ApplicationId { get; set; }
        public string ApplicationName { get; set; }
    }
}
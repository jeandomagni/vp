﻿namespace Ivh.Common.VisitPay.Messages.HospitalData.Dtos
{
    using System;
    using System.Collections.Generic;
    using System.Runtime.Serialization;
    using Utilities.Helpers;

    [Serializable]
    [DataContract]
    public class HsVisitDto
    {
        public int VisitId { get; set; }
        public int HsGuarantorId { get; set; }
        public int BillingSystemId { get; set; }
        public string SourceSystemKey { get; set; }
        public string SourceSystemKeyDisplay { get; set; }
        public string VisitDescription { get; set; }
        public DateTime? AdmitDate { get; set; }
        public DateTime? DischargeDate { get; set; }
        public decimal TotalCharges { get; set; }
        public decimal HsCurrentBalance { get; set; }
        public decimal InsuranceBalance { get; set; }
        public decimal SelfPayBalance { get; set; }
        public DateTime? FirstSelfPayDate { get; set; }
        public string PatientFirstName { get; set; }
        public string PatientLastName { get; set; }
        public DateTime? PatientDOB { get; set; }
        public string BillingApplication { get; set; }
        public HsBillingSystemDto BillingSystem { get; set; }
        public LifeCycleStageDto LifeCycleStage { get; set; }
        public PatientTypeDto PatientType { get; set; }
        public PrimaryInsuranceTypeDto PrimaryInsuranceType { get; set; }
        public RevenueWorkCompanyDto RevenueWorkCompany { get; set; }
        public FacilityDto Facility { get; set; }
        public IList<HsVisitTransactionDto> VisitTransactions { get; set; }
        public IList<ChangeEventDto> ChangeEvents { get; set; }
        public IList<VpOutboundVisitTransactionDto> OutboundVisitTransactionDtos { get; set; }
        public IList<VpOutboundVisitDto> OutboundVisitDtos { get; set; }
        public IList<VisitInsurancePlanDto> VisitInsurancePlans { get; set; }
        public IList<VisitPaymentAllocationDto> VisitPaymentAllocations { get; set; }
        public bool? IsPatientGuarantor { get; set; }
        public bool? IsPatientMinor { get; set; }
        public bool? InterestZero { get; set; }
        public bool? InterestRefund { get; set; }
        public bool? VpEligible { get; set; }
        public bool? BillingHold { get; set; }
        public bool? Redact { get; set; }
        public int? AgingTierId { get; set; }
        public bool ReassessmentExempt { get; set; }
        public int? ServiceGroupId { get; set; }
        public int? PaymentPriority { get; set; }

        [IgnoreDataMember]
        public string PatientLastNameFirstName => FormatHelper.LastNameFirstName(this.PatientLastName, this.PatientFirstName);
        [IgnoreDataMember]
        public string PatientFirstNameLastName => FormatHelper.FirstNameLastName(this.PatientFirstName, this.PatientLastName);
    }
}

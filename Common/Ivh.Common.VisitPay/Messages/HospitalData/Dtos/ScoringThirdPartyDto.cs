﻿
namespace Ivh.Common.VisitPay.Messages.HospitalData.Dtos
{
    public class ScoringThirdPartyDto
    {
        public int ThirdPartyDataId { get; set; }
        public int HsGuarantorId { get; set; }
        public int ThirdPartyProvider { get; set; }
        public decimal ProbabilityScore { get; set; }
        public int WriteOffs5Years { get; set; }
        public int WriteOffsContinuity5Years { get; set; }
        public int CreditCardPayments1Year { get; set; }
        public int CreditCardPayments5Year { get; set; }
        public int ContinuityContractWriteOffs5Years { get; set; }
        public string HouseholdIncome { get; set; }
        public int HouseholdMemberCount { get; set; }
        public int OwnRent { get; set; }
        public string ImputedHouseholdIncome { get; set; }
        public string ImputedMemberCount { get; set; }
    }
}

﻿namespace Ivh.Common.VisitPay.Messages.HospitalData.Dtos
{
    public class HsGuarantorAcknowledgementDto
    {
        public int VpGuarantorId { get; set; }
        public int HsGuarantorId { get; set; }
        public int HsBillingSystemId { get; set; }
        public string SourceSystemKey { get; set; }
    }
}
﻿namespace Ivh.Common.VisitPay.Messages.HospitalData.Dtos
{
    using System;
    using System.Runtime.Serialization;

    [Serializable]
    [DataContract]
    public class FacilityDto
    {
        [DataMember(Order = 1)]
        public virtual int? FacilityId { get; set; }

        [DataMember(Order = 2)]
        public virtual string FacilityDescription { get; set; }

        [DataMember(Order = 3)]
        public virtual string StateCode { get; set; }

        [DataMember(Order = 4)]
        public virtual string SourceSystemKey { get; set; }
    }
}
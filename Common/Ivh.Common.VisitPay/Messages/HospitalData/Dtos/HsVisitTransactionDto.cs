﻿namespace Ivh.Common.VisitPay.Messages.HospitalData.Dtos
{
    using System;

    public class HsVisitTransactionDto
    {
        public int VisitTransactionId { get; set; }
        public int BillingSystemId { get; set; }
        public string SourceSystemKey { get; set; }
        public int VisitId { get; set; }
        public DateTime TransactionDate { get; set; }
        public DateTime PostDate { get; set; }
        public decimal TransactionAmount { get; set; }
        public int VpTransactionTypeId { get; set; }
        public string VpTransactionTypeName { get; set; }
        public string TransactionDescription { get; set; }
        public int? VpPaymentAllocationId { get; set; }
        public int TransactionCodeId { get; set; }
        public VpTransactionTypeDto VpTransactionType { get; set; }
        public bool ReassessmentExempt { get; set; }
    }
}

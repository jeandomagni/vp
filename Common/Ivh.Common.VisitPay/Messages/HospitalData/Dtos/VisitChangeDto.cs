﻿namespace Ivh.Common.VisitPay.Messages.HospitalData.Dtos
{
    using System;
    using System.Collections.Generic;
    using System.Runtime.Serialization;
    using Enums;
    using Utilities.Helpers;

    [Serializable]
    [DataContract]
    public class VisitChangeDto
    {
        [DataMember(Order = 1)]
        public int ChangeEventId { get; set; }

        [DataMember(Order = 1)]
        public int DataLoadId { get; set; }

        [DataMember(Order = 2)]
        public int VisitId { get; set; }

        [DataMember(Order = 3)]
        public ChangeEventDto ChangeEvent { get; set; }

        [DataMember(Order = 4)]
        public ChangeTypeEnum ChangeType { get; set; }

        [DataMember(Order = 5)]
        public int HsGuarantorId { get; set; }

        [DataMember(Order = 6)]
        public int BillingSystemId { get; set; }

        [DataMember(Order = 7)]
        public string SourceSystemKey { get; set; }

        [DataMember(Order = 8)]
        public string VisitDescription { get; set; }

        [DataMember(Order = 9)]
        public DateTime? AdmitDate { get; set; }

        [DataMember(Order = 10)]
        public DateTime? DischargeDate { get; set; }

        [DataMember(Order = 11)]
        public decimal HsCurrentBalance { get; set; }

        [DataMember(Order = 12)]
        public decimal InsuranceBalance { get; set; }

        [DataMember(Order = 13)]
        public DateTime? FirstSelfPayDate { get; set; }

        [DataMember(Order = 14)]
        public decimal SelfPayBalance { get; set; }

        [DataMember(Order = 15)]
        public string PatientFirstName { get; set; }

        [DataMember(Order = 16)]
        public string PatientLastName { get; set; }

        [DataMember(Order = 17)]
        public DateTime? PatientDOB { get; set; }

        [DataMember(Order = 18)]
        public virtual string BillingApplication { get; set; }

        [DataMember(Order = 20)]
        public virtual PrimaryInsuranceTypeDto PrimaryInsuranceType { get; set; }

        [DataMember(Order = 21)]
        public virtual RevenueWorkCompanyDto RevenueWorkCompany { get; set; }

        [DataMember(Order = 22)]
        public virtual PatientTypeDto PatientType { get; set; }

        [DataMember(Order = 23)]
        public virtual LifeCycleStageDto LifeCycleStage { get; set; }

        [DataMember(Order = 24)]
        public virtual bool IsChanged { get; set; }

        [DataMember(Order = 25)]
        public virtual IList<VisitTransactionChangeDto> VisitTransactions { get; set; }

        [DataMember(Order = 26)]
        public virtual IList<VisitInsurancePlanChangeDto> VisitInsurancePlans { get; set; }

        [DataMember(Order = 27)]
        public virtual string SourceSystemKeyDisplay { get; set; }

        [DataMember(Order = 28)]
        public bool? IsPatientGuarantor { get; set; }

        [DataMember(Order = 29)]
        public bool? IsPatientMinor { get; set; }

        [DataMember(Order = 30)]
        public virtual bool? InterestRefund { get; set; }

        [DataMember(Order = 31)]
        public virtual bool? InterestZero { get; set; }

        [DataMember(Order = 32)]
        public virtual int? ServiceGroupId { get; set; }

        [DataMember(Order = 33)]
        public virtual int? AgingCount { get; set; }

        [DataMember(Order = 34)]
        public virtual IList<VisitPaymentAllocationChangeDto> VisitPaymentAllocations { get; set; }

        [DataMember(Order = 35)]
        public virtual bool? OnPrePaymentPlan { get; set; }

        [DataMember(Order = 36)]
        public bool VpEligible { get; set; }
        [DataMember(Order = 37)]
        public bool BillingHold { get; set; }
        [DataMember(Order = 38)]
        public bool Redact { get; set; }
        [DataMember(Order = 39)]
        public AgingTierEnum AgingTier { get; set; }//NOT SURE THIS GOES HERE. Pending Aging Service Definition.

        [DataMember(Order = 42)]
        public bool ReassessmentExempt { get; set; }

        [DataMember(Order = 43)]
        public virtual FacilityDto Facility { get; set; }

        [DataMember(Order = 44)]
        public virtual int? PaymentPriority { get; set; }

        [IgnoreDataMember]
        public string PatientLastNameFirstName => FormatHelper.LastNameFirstName(this.PatientLastName, this.PatientFirstName);

        [IgnoreDataMember]
        public string PatientFirstNameLastName => FormatHelper.FirstNameLastName(this.PatientFirstName, this.PatientLastName);

        [IgnoreDataMember]
        public bool HasSystemSourceKey => !string.IsNullOrWhiteSpace(this.SourceSystemKey);
    }
}
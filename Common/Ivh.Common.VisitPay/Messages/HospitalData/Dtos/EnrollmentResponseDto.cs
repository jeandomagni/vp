﻿namespace Ivh.Common.VisitPay.Messages.HospitalData.Dtos
{
    using System.Collections.Generic;
    using Enums;

    public class EnrollmentResponseDto
    {
        public EnrollmentResultEnum EnrollmentResult { get; set; }
        public string GuarantorSourceSystemKey { get; set; }
        public int? GuarantorBillingId { get; set; }
        public bool EnrollmentStatus { get; set; }
        public string RawResponse { get; set; }
        public bool PublishChangeEvent { get; set; } = true;
        public List<EnrollmentAccountResponseDto> Accounts { get; set; }
    }
}
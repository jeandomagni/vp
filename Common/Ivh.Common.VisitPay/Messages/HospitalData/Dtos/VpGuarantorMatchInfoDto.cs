﻿namespace Ivh.Common.VisitPay.Messages.HospitalData.Dtos
{
    using System;

    public class VpGuarantorMatchInfoDto
    {
        public int VpGuarantorId { get; set; }
        public int HsBillingSystemID { get; set; }
        public string RegistrationMatchString { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string SSN { get; set; }
        public string SSN4 { get; set; }
        public DateTime DOB { get; set; }
        public DateTime? PatientDOB { get; set; }
        public string City { get; set; }
        public string StateProvince { get; set; }
        public string AddressLine1 { get; set; }
        public string PostalCodeShort { get; set; }
        public int HsGuarantorId { get; set; }
        public DateTime? CancelledDate { get; set; }
    }
}
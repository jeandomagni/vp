﻿namespace Ivh.Common.VisitPay.Messages.HospitalData.Dtos
{
    public class HsBillingSystemDto
    {
        public int BillingSystemId { get; set; }
        public string BillingSystemName { get; set; }
    }
}

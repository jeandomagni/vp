namespace Ivh.Common.VisitPay.Messages.HospitalData.Dtos
{
    using System;
    using System.Runtime.Serialization;
    using Enums;

    [Serializable]
    [DataContract]
    public class VisitMatchStatusDto
    {
        [DataMember(Order = 1)]
        public int VisitMatchStatusId { get; set; }
        [DataMember(Order = 2)]
        public DateTime InsertDate { get; set; }
        [DataMember(Order = 3)]
        public int VisitId { get; set; }
        [DataMember(Order = 4)]
        public HsGuarantorMatchStatusEnum HsGuarantorMatchStatus { get; set; }
    }
}
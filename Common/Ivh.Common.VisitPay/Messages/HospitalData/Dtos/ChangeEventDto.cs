﻿namespace Ivh.Common.VisitPay.Messages.HospitalData.Dtos
{
    using System;
    using Enums;

    public class ChangeEventDto
    {
        public int ChangeEventId { get; set; }
        public ChangeEventTypeDto ChangeEventType { get; set; }
        public string EventTriggerDescription { get; set; }
        public DateTime ChangeEventDateTime { get; set; }
        public ChangeEventStatusEnum ChangeEventStatus { get; set; }
        public int? VisitId { get; set; }
        public int? HsGuarantorId { get; set; }
    }
}
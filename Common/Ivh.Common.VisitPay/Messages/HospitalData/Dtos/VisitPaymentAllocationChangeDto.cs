﻿namespace Ivh.Common.VisitPay.Messages.HospitalData.Dtos
{
    using System;
    using System.Runtime.Serialization;

    [Serializable]
    [DataContract]
    public class VisitPaymentAllocationChangeDto
    {
        [DataMember(Order = 1)]
        public int ChangeEventId { get; set; }

        [DataMember(Order = 2)]
        public int DataLoadId { get; set; }

        [DataMember(Order = 3)]
        public int VisitId { get; set; }

        [DataMember(Order = 4)]
        public int PaymentAllocationId { get; set; }
    }
}
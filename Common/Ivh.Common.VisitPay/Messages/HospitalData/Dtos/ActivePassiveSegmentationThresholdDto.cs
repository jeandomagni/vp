﻿namespace Ivh.Common.VisitPay.Messages.HospitalData.Dtos
{
    using System;

    public class ActivePassiveSegmentationThresholdDto
    {
        public int ActivePassiveSegmentationThresholdId { get; set; }
        public int DaysSinceSelfPayDate { get; set; }
        public decimal PtpScore { get; set; }
        public decimal HsGuarantorBalanceLowerLimit { get; set; }
        public decimal HsGuarantorBalanceUpperLimit { get; set; }
        public decimal HsGuarantorBalanceNoPtpLimit { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
    }
}
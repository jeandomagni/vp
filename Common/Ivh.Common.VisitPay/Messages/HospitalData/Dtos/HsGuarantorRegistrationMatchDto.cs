namespace Ivh.Common.VisitPay.Messages.HospitalData.Dtos
{
    using System;
    using System.Runtime.Serialization;

    [Serializable]
    [DataContract]
    public class HsGuarantorRegistrationMatchDto
    {
        [DataMember(Order = 1)]
        public int HsBillingSystemId { get; set; }
        [DataMember(Order = 2)]
        public string MatchString { get; set; }
        [DataMember(Order = 3)]
        public int HsGuarantorId { get; set; }
    }
}
﻿namespace Ivh.Common.VisitPay.Messages.HospitalData.Dtos
{
    using System;
    using System.Runtime.Serialization;

    [Serializable]
    [DataContract]
    public class VisitInsurancePlanChangeDto
    {
        [DataMember(Order = 1)]
        public int DataLoadId { get; set; }
        [DataMember(Order = 2)]
        public int ChangeEventId { get; set; }
        [DataMember(Order = 3)]
        public int InsurancePlanId { get; set; }
        [DataMember(Order = 4)]
        public int VisitId { get; set; }
        [DataMember(Order = 5)]
        public int PayOrder { get; set; }

        /// <summary>
        /// read-only in mappings, set InsurancePlanId instead
        /// </summary>
        [DataMember(Order = 6)]
        public InsurancePlanDto InsurancePlan { get; protected set; }
    }
}
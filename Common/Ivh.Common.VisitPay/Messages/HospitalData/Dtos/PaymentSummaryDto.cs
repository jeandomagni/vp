﻿namespace Ivh.Common.VisitPay.Messages.HospitalData.Dtos
{
    using System;
    using System.Collections.Generic;
    using System.Runtime.Serialization;

    [Serializable]
    [DataContract]
    public class PaymentSummaryDto
    {
        [DataMember(Order = 1)]
        public int VpGuarantorId { get; set; }
        [DataMember(Order = 2)]
        public DateTime PostDateBegin { get; set; }
        [DataMember(Order = 3)]
        public DateTime PostDateEnd { get; set; }
        [DataMember(Order = 4)]
        public List<PaymentSummaryTransactionDto> Payments { get; set; }
    }
}

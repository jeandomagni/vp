﻿namespace Ivh.Common.VisitPay.Messages.HospitalData.Dtos
{
    public class HsGuarantorFilterDto
    {
        public int? VPGID { get; set; }
        public string HsSourceKey { get; set; }
        public string LastName { get; set; }
        public bool VpRegistered { get; set; }
        public bool VpUnregistered { get; set; }
    }
}

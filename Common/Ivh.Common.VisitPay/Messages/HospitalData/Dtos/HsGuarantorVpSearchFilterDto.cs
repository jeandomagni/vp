﻿namespace Ivh.Common.VisitPay.Messages.HospitalData.Dtos
{
    using Utilities.Helpers;

    public class HsGuarantorVpSearchFilterDto
    {
        public string HsGuarantorIds { get; set; }
        public string SourceSystemKey { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Ssn4 { get; set; }
        public string DOB { get; set; }
        public string SortField { get; set; }
        public string SortOrder { get; set; }
        public int Page { get; set; }
        public int Rows { get; set; }
        public string LastNameFirstName => FormatHelper.LastNameFirstName(this.LastName, this.FirstName);
        public string FirstNameLastName => FormatHelper.FirstNameLastName(this.FirstName, this.LastName);
    }
}

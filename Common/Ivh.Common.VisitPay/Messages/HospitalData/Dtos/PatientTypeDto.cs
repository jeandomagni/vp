﻿namespace Ivh.Common.VisitPay.Messages.HospitalData.Dtos
{
    using System;
    using System.Runtime.Serialization;

    [Serializable]
    [DataContract]
    public class PatientTypeDto
    {
        [DataMember(Order = 1)]
        public int PatientTypeId { get; set; }
        [DataMember(Order = 2)]
        public string PatientTypeName { get; set; }
    }
}
﻿namespace Ivh.Common.VisitPay.Messages.HospitalData.Dtos
{
    public class ScoringGuarantorScoreDto
    {
        public int HsGuarantorId { get; set; }
        public int VisitId { get; set; }
        public decimal PtpScore { get; set; }
        public bool Am1Flag { get; set; }
        public string S1Version { get; set; }
        public string S2Version { get; set; }
        public ProtoScoreDto ProtoScore { get; set; }
    }
}

﻿namespace Ivh.Common.VisitPay.Messages.HospitalData.Dtos
{
    public class TransactionCodeDto
    {
        public int TransactionCodeId { get; set; }
        public string SourceSystemKey { get; set; }
        public string TransactionCodeName { get; set; }
        public int VpTransactionTypeId { get; set; }
        public int BillingSystemId { get; set; }
        public short AmountMultiplier { get; set; }
        public string TranSskAppend { get; set; }
    }
}
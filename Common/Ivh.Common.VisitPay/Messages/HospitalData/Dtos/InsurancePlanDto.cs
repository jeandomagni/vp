﻿namespace Ivh.Common.VisitPay.Messages.HospitalData.Dtos
{
    using System;
    using System.Runtime.Serialization;

    [Serializable]
    [DataContract]
    public class InsurancePlanDto
    {
        [DataMember(Order = 1)]
        public int InsurancePlanId { get; set; }

        [DataMember(Order = 2)]
        public int BillingSystemId { get; set; }

        [DataMember(Order = 3)]
        public string SourceSystemKey { get; set; }

        [DataMember(Order = 4)]
        public string InsurancePlanName { get; set; }

        [DataMember(Order = 5)]
        public PrimaryInsuranceTypeDto PrimaryInsuranceType { get; set; }
    }
}
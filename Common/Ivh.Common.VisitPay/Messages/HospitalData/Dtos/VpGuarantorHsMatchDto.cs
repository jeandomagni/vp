namespace Ivh.Common.VisitPay.Messages.HospitalData.Dtos
{
    using System;
    using System.Runtime.InteropServices;
    using System.Runtime.Serialization;
    using Enums;

    [Serializable]
    [DataContract]
    public class VpGuarantorHsMatchDto
    {
        [DataMember(Order = 1)]
        public int VpGuarantorId { get; set; }

        [DataMember(Order = 2)]
        public int HsGuarantorId { get; set; }

        [DataMember(Order = 3)]
        public DateTime MatchedOn { get; set; }

        [DataMember(Order = 4)]
        public DateTime? SentToHsOn { get; set; }

        [DataMember(Order = 5)]
        public DateTime? HsConfirmedOn { get; set; }

        [DataMember(Order = 6)]
        public bool IsActive { get; set; }

        [DataMember(Order = 7)]
        public string LastName { get; set; }

        [DataMember(Order = 8)]
        public DateTime DOB { get; set; }

        [DataMember(Order = 9)]
        public int Ssn4 { get; set; }

        [DataMember(Order = 10)]
        public int PostalCode { get; set; }

        [DataMember(Order = 11)]
        public DateTime? UnmatchedOn { get; set; }

        [DataMember(Order = 12)]
        public DateTime? UnmatchedSentToHsOn { get; set; }

        [DataMember(Order = 13)]
        public DateTime? UnmatchedHsConfirmedOn { get; set; }

        [DataMember(Order = 14)]
        public HsGuarantorMatchStatusEnum HsGuarantorMatchStatus { get; set; }

        [DataMember(Order = 15)]
        public int VpGuarantorHsMatchId { get; set; }

        [DataMember(Order = 16)]
        public DateTime PatientDOB { get; set; }

        [DataMember(Order = 17)]
        public MatchOptionEnum? MatchOption { get; set; }

        [DataMember(Order = 18)]
        public string FirstName { get; set; }

        [DataMember(Order = 19)]
        public string AddressLine1 { get; set; }

        [DataMember(Order = 20)]
        public string City { get; set; }

        [DataMember(Order = 21)]
        public string StateProvince { get; set; }

    }
}
﻿namespace Ivh.Common.VisitPay.Messages.HospitalData.Dtos
{
    using System;
    using System.Runtime.Serialization;

    [Serializable]
    [DataContract]
    public class VisitPaymentAllocationDto
    {
        [DataMember(Order = 1)]
        public int VisitPaymentAllocationId { get; set; }

        [DataMember(Order = 2)]
        public int VisitId { get; set; }

        [DataMember(Order = 3)]
        public virtual int PaymentAllocationId { get; set; }
    }
}
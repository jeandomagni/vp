﻿namespace Ivh.Common.VisitPay.Messages.HospitalData.Dtos
{
    using System.Collections.Generic;

    public class ScoringGuarantorDto
    {
        public int HsGuarantorId { get; set; }
        public IList<ScoringGuarantorVisitBatchDto> Visits { get; set; }
    }
}

namespace Ivh.Common.VisitPay.Messages.HospitalData.Dtos
{
    using System;
    using System.Runtime.Serialization;

    [Serializable]
    [DataContract]
    public class VpTransactionTypeDto
    {
        [DataMember(Order = 1)]
        public int VpTransactionTypeId { get; set; }
        [DataMember(Order = 2)]
        public string TransactionType { get; set; }
        [DataMember(Order = 3)]
        public string TransactionGroup { get; set; }
        [DataMember(Order = 4)]
        public string VpTransactionTypeName { get; set; }
        [DataMember(Order = 5)]
        public string DisplayName { get; set; }
        [DataMember(Order = 6)]
        public string TransactionSource { get; set; }
        [DataMember(Order = 7)]
        public string Notes { get; set; }
    }
}
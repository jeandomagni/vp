﻿namespace Ivh.Common.VisitPay.Messages.HospitalData.Dtos
{
    using System;
    using System.Runtime.Serialization;

    [Serializable]
    [DataContract]
    public class RevenueWorkCompanyDto
    {
        [DataMember(Order = 1)]
        public int RevenueWorkCompanyId { get; set; }
        [DataMember(Order = 2)]
        public string RevenueWorkCompanyName { get; set; }
    }
}
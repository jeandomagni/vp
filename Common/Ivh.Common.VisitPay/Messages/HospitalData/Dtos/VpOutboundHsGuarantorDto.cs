﻿namespace Ivh.Common.VisitPay.Messages.HospitalData.Dtos
{
    using System;
    using Enums;

    public class VpOutboundHsGuarantorDto
    {
        public int VpOutboundHsGuarantorId { get; set; }
        public string Notes { get; set; }
        public DateTime InsertDate { get; set; }
        public DateTime? ProcessedDate { get; set; }
        public VpOutboundHsGuarantorMessageTypeEnum VpOutboundHsGuarantorMessageType { get; set; }
        public HsGuarantorDto HsGuarantor { get; set; }
    }
}
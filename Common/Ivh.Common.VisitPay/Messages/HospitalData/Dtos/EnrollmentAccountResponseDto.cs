namespace Ivh.Common.VisitPay.Messages.HospitalData.Dtos
{
    using System.Globalization;
    using Enums;

    public class EnrollmentAccountResponseDto : IEnrollmentResponse
    {
        //Todo:probably should make this more generic (less intermountain specific)
        public string facilityId { get; set; }
        public string accountNumber { get; set; }
        public EnrollmentAccountEligibilityEnum eligiblitStatus { get; set; }
        public string originatingSystem { get; set; }
        
        private string _AccountMatchString = null;
        public string AccountMatchString
        {
            get
            {
                if(this._AccountMatchString != null) return this._AccountMatchString;

                if(this.facilityId == null || this.accountNumber == null || this.originatingSystem == null) return null;
                string invarFacility = this.facilityId.ToString(CultureInfo.InvariantCulture);
                int tryFacility = -1;
                if (int.TryParse(invarFacility, out tryFacility))
                {
                    invarFacility = tryFacility.ToString("D3", CultureInfo.InvariantCulture);
                }

                return this.originatingSystem.ToUpperInvariant() + "_" + invarFacility + "_" + this.accountNumber.ToString(CultureInfo.InvariantCulture);
            }
            set { this._AccountMatchString = value; }
        }
    }

    public interface IEnrollmentResponse
    {
        EnrollmentAccountEligibilityEnum eligiblitStatus { get; set; }
        string AccountMatchString { get; }
    }
}
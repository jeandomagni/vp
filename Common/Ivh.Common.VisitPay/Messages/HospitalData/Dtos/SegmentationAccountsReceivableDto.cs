﻿namespace Ivh.Common.VisitPay.Messages.HospitalData.Dtos
{
    public class SegmentationAccountsReceivableDto
    {
        public int HsGuarantorId { get; set; }
        public int SegmentationBatchId { get; set; }
        public decimal HsCurrentBalance { get; set; }
        public decimal? PtpScore { get; set; }
        public string RawSegmentationScore { get; set; }
        public string ScriptVersion { get; set; }
        public string AssignedAgencyIds { get; set; }
    }
}
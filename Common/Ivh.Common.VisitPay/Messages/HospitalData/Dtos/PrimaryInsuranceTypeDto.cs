﻿namespace Ivh.Common.VisitPay.Messages.HospitalData.Dtos
{
    using System;
    using System.Runtime.Serialization;
    using Enums;

    [Serializable]
    [DataContract]
    public class PrimaryInsuranceTypeDto
    {
        [DataMember(Order = 1)]
        public int PrimaryInsuranceTypeId { get; set; }
        [DataMember(Order = 2)]
        public string PrimaryInsuranceTypeName { get; set; }
        [DataMember(Order = 3)]
        public SelfPayClassEnum SelfPayClass { get; set; }
    }
}
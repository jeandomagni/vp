﻿namespace Ivh.Common.VisitPay.Messages.HospitalData.Dtos
{
    using System;
    using System.Runtime.Serialization;

    public class ScoringGuarantorVisitBatchDto
    {
        public int VisitBatchId { get; set; }
        public int VisitId { get; set; }
        public int HsGuarantorId { get; set; }
        public int ScoringBatchId { get; set; }
        public bool TriggeredScoring { get; set; }
        public bool IsEligibleToScore { get; set; }
        public int BillingSystemId { get; set; }
        public string SourceSystemKey { get; set; }
        public DateTime DischargeDate { get; set; }
        public decimal HsCurrentBalance { get; set; }
        public decimal SelfPayBalance { get; set; }
        public string BillingApplication { get; set; }
        public int LifeCycleStageId { get; set; }
        public int PatientTypeId { get; set; }
        [DataMember(Order = 15)]
        public int SelfPayClassId { get; set; }
        [DataMember(Order = 16)]
        public DateTime FirstSelfPayDate { get; set; }
        public int FacilityId { get; set; }
        public string FacilityDescription { get; set; }
        public string StateCode { get; set; }
        public int? RegionId { get; set; }
        public string RegionScope { get; set; }
        public string BillingSystemName { get; set; }
    }
}

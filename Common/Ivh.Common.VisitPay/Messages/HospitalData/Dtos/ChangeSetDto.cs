﻿namespace Ivh.Common.VisitPay.Messages.HospitalData.Dtos
{
    using System;
    using System.Collections.Generic;
    using Enums;

    public class ChangeSetDto
    {
        public int ChangeSetId { get; set; }
        public ChangeSetTypeEnum ChangeSetType { get; set; }
        public int DataLoadId { get; set; }
        public DateTime ChangeSetDateTime { get; set; }
        public IList<HsGuarantorChangeDto> HsGuarantorChanges { get; set; }
        public IList<VisitChangeDto> VisitChanges { get; set; }
        public IList<ChangeEventDto> ChangeEvents { get; set; }
    }
}
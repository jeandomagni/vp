namespace Ivh.Common.VisitPay.Messages.HospitalData.Dtos
{
    using System;
    using System.Runtime.Serialization;
    using Attributes;
    using Utilities.Helpers;

    public class HsGuarantorDto
    {
        [DataMember(Order = 1)]
        public int HsGuarantorId { get; set; }
        [DataMember(Order = 2)]
        public int HsBillingSystemId { get; set; }
        [DataMember(Order = 3)]
        public string SourceSystemKey { get; set; }
        [DataMember(Order = 4), GuarantorMatchField("FirstName")]
        public string FirstName { get; set; }
        [DataMember(Order = 5), GuarantorMatchField("LastName")]
        public string LastName { get; set; }
        [DataMember(Order = 6), GuarantorMatchField("DOB")]
        public DateTime? DOB { get; set; }
        [DataMember(Order = 7), GuarantorMatchField("SSN4")]
        public string SSN4 { get; set; }
        [DataMember(Order = 8), GuarantorMatchField("AddressLine1")]
        public string AddressLine1 { get; set; }
        [DataMember(Order = 9), GuarantorMatchField("AddressLine2")]
        public string AddressLine2 { get; set; }
        [DataMember(Order = 10), GuarantorMatchField("City")]
        public string City { get; set; }
        [DataMember(Order = 11), GuarantorMatchField("StateProvince")]
        public string StateProvince { get; set; }
        [DataMember(Order = 12), GuarantorMatchField("PostalCode")]
        public string PostalCode { get; set; }
        [DataMember(Order = 13), GuarantorMatchField("Country")]
        public string Country { get; set; }
        [DataMember(Order = 14), GuarantorMatchField("SSN")]
        public string SSN { get; set; }
        [DataMember(Order = 15)]
        public bool VpEligible { get; set; }
        public int? GenderId { get; set; }
        public string Email { get; set; }
        [IgnoreDataMember]
        public string LastNameFirstName => FormatHelper.LastNameFirstName(this.LastName, this.FirstName);
        [IgnoreDataMember]
        public string FirstNameLastName => FormatHelper.FirstNameLastName(this.FirstName, this.LastName);
    }
}
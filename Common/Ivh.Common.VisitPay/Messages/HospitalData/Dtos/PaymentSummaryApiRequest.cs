﻿namespace Ivh.Common.VisitPay.Messages.HospitalData.Dtos
{
    using System;

    public class PaymentSummaryApiRequest
    {
        public int VpGuarantorId { get; set; }
        public DateTime? PostDateBegin { get; set; }
        public DateTime? PostDateEnd { get; set; }
    }
}
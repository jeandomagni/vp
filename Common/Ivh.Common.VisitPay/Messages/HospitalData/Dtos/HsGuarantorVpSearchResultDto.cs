﻿namespace Ivh.Common.VisitPay.Messages.HospitalData.Dtos
{
    using System;
    using System.Runtime.Serialization;
    using Utilities.Helpers;

    public class HsGuarantorVpSearchResultDto
    {
        public int HsGuarantorId { get; set; }
        public int HsBillingSystemId { get; set; }
        public string SourceSystemKey { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public DateTime? DOB { get; set; }
        public string Ssn4 { get; set; }
        [IgnoreDataMember]
        public string LastNameFirstName => FormatHelper.LastNameFirstName(this.LastName, this.FirstName);
        [IgnoreDataMember]
        public string FirstNameLastName => FormatHelper.FirstNameLastName(this.FirstName, this.LastName);
    }
}

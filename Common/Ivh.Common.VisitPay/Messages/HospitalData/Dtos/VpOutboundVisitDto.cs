﻿namespace Ivh.Common.VisitPay.Messages.HospitalData.Dtos
{
    using System;
    using System.Runtime.Serialization;

    public class VpOutboundVisitDto
    {
        [DataMember(Order = 1)]
        public int VpOutboundVisitId { get; set; }
        [DataMember(Order = 2)]
        public HsVisitDto Visit { get; set; }
        [DataMember(Order = 3)]
        public int BillingSystemId { get; set; }
        [DataMember(Order = 4)]
        public string SourceSystemKey { get; set; }
        [DataMember(Order = 5)]
        public int VpVisitId { get; set; }
        [DataMember(Order = 6)]
        public string VpOutboundVisitMessageTypeName { get; set; }
        [DataMember(Order = 7)]
        public int VpOutboundVisitMessageTypeId { get; set; }
        [DataMember(Order = 8)]
        public DateTime ActionDate { get; set; }
        [DataMember(Order = 9)]
        public DateTime InsertDate { get; set; }
        [DataMember(Order = 10)]
        public  bool? IsCallCenter { get; set; }
        [DataMember(Order = 11)]
        public  bool? IsAutoPay { get; set; }
    }
}

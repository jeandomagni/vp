﻿namespace Ivh.Common.VisitPay.Messages.HospitalData.Dtos
{
    using System;
    using System.Runtime.Serialization;

    public class VpOutboundVisitTransactionDto
    {
        [DataMember(Order = 1)]
        public int VpOutboundVisitTransactionId { get; set; }
        [DataMember(Order = 3)]
        public int VpPaymentAllocationId { get; set; }
        [DataMember(Order = 6)]
        public DateTime InsertDate { get; set; }
        [DataMember(Order = 7)]
        public decimal TransactionAmount { get; set; }
        [DataMember(Order = 8)]
        public DateTime ProcessedDate { get; set; }
        [DataMember(Order = 10)]
        public int VisitBillingSystemId { get; set; }
        [DataMember(Order = 11)]
        public string VisitSourceSystemKey { get; set; }
        [DataMember(Order = 12)]
        public HsVisitDto Visit { get; set; }
        [DataMember(Order = 13)]
        public int? VpParentPaymentId { get; set; }
        [DataMember(Order = 14)]
        public BalanceTransferStatusDto BalanceTransferStatus { get; set; }
    }
}
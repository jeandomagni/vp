﻿namespace Ivh.Common.VisitPay.Messages.HospitalData.Dtos
{
    public class BalanceTransferStatusDto
    {
        public int BalanceTransferStatusId { get; set; }
        public string BalanceTransferStatusName { get; set; }
        public string BalanceTransferStatusDisplayName { get; set; }
    }
}
﻿namespace Ivh.Common.VisitPay.Messages.HospitalData.Dtos
{
    public class ChangeEventTypeDto
    {
        public int ChangeEventTypeId { get; set; }
        public ApplicationDto Application { get; set; }
        public string ChangeEventTypeName { get; set; }
        public string ChangeEventTypeDescription { get; set; }
        public int ApplicationProcessingSortOrder { get; set; }
    }
}

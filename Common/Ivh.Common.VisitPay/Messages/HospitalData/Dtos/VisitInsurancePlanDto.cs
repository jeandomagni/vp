﻿namespace Ivh.Common.VisitPay.Messages.HospitalData.Dtos
{
    using System;
    using System.Runtime.Serialization;

    [Serializable]
    [DataContract]
    public class VisitInsurancePlanDto
    {
        [DataMember(Order = 1)]
        public int VisitInsurancePlanId { get; set; }

        [DataMember(Order = 2)]
        public int VisitId { get; set; }

        [DataMember(Order = 3)]
        public int PayOrder { get; set; }

        [DataMember(Order = 4)]
        public InsurancePlanDto InsurancePlan { get; set; }
    }
}
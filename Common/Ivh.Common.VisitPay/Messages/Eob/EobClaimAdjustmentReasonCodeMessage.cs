﻿namespace Ivh.Common.VisitPay.Messages.Eob
{
    using System;
    using System.Runtime.Serialization;
    using ServiceBus.Common.Messages;

    [Serializable]
    [DataContract]
    public class EobClaimAdjustmentReasonCodeMessage : UniversalMessage<EobClaimAdjustmentReasonCodeMessage>
    {
        [DataMember(Order = 1)]
        public int EobClaimAdjustmentReasonCodeId { get; set; }
        [DataMember(Order = 2)]
        public string ClaimAdjustmentReasonCode { get; set; }
        [DataMember(Order = 3)]
        public string Description { get; set; }
        [DataMember(Order = 4)]
        public DateTime? StartDate { get; set; }
        [DataMember(Order = 5)]
        public DateTime? EndDate { get; set; }
        [IgnoreDataMember]
        public override string ErrorMessage => this.FormatErrorMessage(this, x => x.EobClaimAdjustmentReasonCodeId);
    }
}

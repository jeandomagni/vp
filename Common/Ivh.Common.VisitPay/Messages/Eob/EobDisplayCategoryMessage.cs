﻿namespace Ivh.Common.VisitPay.Messages.Eob
{
    using System;
    using System.Runtime.Serialization;
    using ServiceBus.Common.Messages;

    [Serializable]
    [DataContract]
    public class EobDisplayCategoryMessage : UniversalMessage<EobDisplayCategoryMessage>
    {
        [DataMember(Order = 1)]
        public int EobDisplayCategoryId { get; set; }
        [DataMember(Order = 2)]
        public string DisplayCategoryName { get; set; }
        [IgnoreDataMember]
        public override string ErrorMessage => this.FormatErrorMessage(this, x => x.EobDisplayCategoryId);
    }
}

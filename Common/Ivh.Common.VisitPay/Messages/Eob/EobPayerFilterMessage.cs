﻿namespace Ivh.Common.VisitPay.Messages.Eob
{
    using System;
    using System.Runtime.Serialization;
    using ServiceBus.Common.Messages;

    [Serializable]
    [DataContract]
    public class EobPayerFilterMessage : UniversalMessage<EobPayerFilterMessage>
    {
        [DataMember(Order = 1)]
        public int EobPayerFilterId { get; set; }
        [DataMember(Order = 2)]
        public string UniquePayerValue { get; set; }
        [DataMember(Order = 3)]
        public bool IsActive { get; set; }
        [IgnoreDataMember]
        public override string ErrorMessage => this.FormatErrorMessage(this, x => x.EobPayerFilterId);
    }
}

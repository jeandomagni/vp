﻿namespace Ivh.Common.VisitPay.Messages.Eob
{
    using System;
    using System.Runtime.Serialization;
    using ServiceBus.Common.Messages;

    [Serializable]
    [DataContract]
    public class EobExternalLinkMessage : UniversalMessage<EobExternalLinkMessage>
    {
        [DataMember(Order = 1)]
        public int EobExternalLinkId { get; set; }
        [DataMember(Order = 2)]
        public string Url { get; set; }
        [DataMember(Order = 3)]
        public string Text { get; set; }
        [DataMember(Order = 4)]
        public string Icon { get; set; }
        [DataMember(Order = 5)]
        public int PersistDays { get; set; }
        [DataMember(Order = 6)]
        public string ClaimNumberLocation { get; set; }
        [IgnoreDataMember]
        public override string ErrorMessage => this.FormatErrorMessage(this, x => x.EobExternalLinkId);
    }
}

﻿namespace Ivh.Common.VisitPay.Messages.Eob
{
    using System;
    using System.Runtime.Serialization;
    using ServiceBus.Common.Messages;

    [Serializable]
    [DataContract]
    public class EobClaimPaymentMessage : UniversalMessage<EobClaimPaymentMessage>
    {
        [DataMember(Order = 1)]
        public string VisitSourceSystemKey { get; set; }
        [DataMember(Order = 2)]
        public int BillingSystemId { get; set; }
        [DataMember(Order = 3)]
        public string InterchangeSenderId { get; set; }
        [DataMember(Order = 4)]
        public string InterchangeReceiverId { get; set; }
        [DataMember(Order = 5)]
        public DateTime InterchangeDateTime { get; set; }
        [DataMember(Order = 6)]
        public int InterchangeControlNumber { get; set; }
        [DataMember(Order = 7)]
        public int GroupControlNumber { get; set; }
        [DataMember(Order = 8)]
        public DateTime? TransactionDate { get; set; }
        [DataMember(Order = 9)]
        public string TransactionSetControlNumber { get; set; }
        [DataMember(Order = 10)]
        public int TransactionSetLineNumber { get; set; }
        [DataMember(Order = 11)]
        public int EobPayerFilterId { get; set; }
        [DataMember(Order = 12)]
        public string PayerName { get; set; }
        [DataMember(Order = 13)]
        public string ClaimNumber { get; set; }
        [DataMember(Order = 14)]
        public string ClaimPaymentInformationJson { get; set; }
        [IgnoreDataMember]
        public override string ErrorMessage => this.FormatErrorMessage(this, x => $"{x.ClaimNumber} - {x.VisitSourceSystemKey} - {x.BillingSystemId}");
    }
}

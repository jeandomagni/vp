﻿namespace Ivh.Common.VisitPay.Messages.Eob
{
    using System;
    using System.Runtime.Serialization;
    using ServiceBus.Common.Messages;

    [Serializable]
    [DataContract]
    public class EobClaimAdjustmentReasonCodeDisplayMapMessage : UniversalMessage<EobClaimAdjustmentReasonCodeDisplayMapMessage>
    {
        [DataMember(Order = 1)]
        public int EobClaimAdjustmentReasonCodeDisplayMapId { get; set; }
        [DataMember(Order = 2)]
        public string ClaimAdjustmentReasonCode { get; set; }
        [DataMember(Order = 3)]
        public int EobDisplayCategoryId { get; set; }
        [DataMember(Order = 4)]
        public string UniquePayerValue { get; set; }
        [DataMember(Order = 5)]
        public string ClaimStatusCode { get; set; }
        [DataMember(Order = 6)]
        public string ClaimAdjustmentGroupCode { get; set; }
        [IgnoreDataMember]
        public override string ErrorMessage => this.FormatErrorMessage(this, x => x.EobClaimAdjustmentReasonCodeDisplayMapId);
    }
}

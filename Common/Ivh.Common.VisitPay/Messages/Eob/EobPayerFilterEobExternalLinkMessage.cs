﻿namespace Ivh.Common.VisitPay.Messages.Eob
{
    using System;
    using System.Runtime.Serialization;
    using ServiceBus.Common.Messages;

    [Serializable]
    [DataContract]
    public class EobPayerFilterEobExternalLinkMessage : UniversalMessage<EobPayerFilterEobExternalLinkMessage>
    {
        [DataMember(Order = 1)]
        public int EobPayerFilterEobExternalLinkId { get; set; }
        [DataMember(Order = 2)]
        public int EobPayerFilterId { get; set; }
        [DataMember(Order = 3)]
        public int EobExternalLinkId { get; set; }
        [DataMember(Order = 4)]
        public int? InsurancePlanBillingSystemId { get; set; }
        [DataMember(Order = 5)]
        public string InsurancePlanSourceSystemKey { get; set; }
        [IgnoreDataMember]
        public override string ErrorMessage => this.FormatErrorMessage(this, x => x.EobPayerFilterEobExternalLinkId);
    }
}

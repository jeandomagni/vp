﻿namespace Ivh.Common.VisitPay.Messages.Eob
{
    using System;
    using System.Runtime.Serialization;
    using ServiceBus.Common.Messages;

    [Serializable]
    [DataContract]
    public class EobInterchangeMigrationMessage  : UniversalMessage<EobInterchangeMigrationMessage>
    {
        [DataMember(Order = 1)]
        public int InterchangeControlHeaderTrailerId { get; set; }

        [IgnoreDataMember]
        public override string ErrorMessage => this.FormatErrorMessage(this, x => $"{x.InterchangeControlHeaderTrailerId}");
    }
}

﻿namespace Ivh.Common.VisitPay.Messages.Tests
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Runtime.Serialization;
    using GreenPipes.Internals.Extensions;
    using NUnit.Framework;
    using ServiceBus.Common.Messages;
    using Utilities.Extensions;
    using Utilities.Helpers;

    [TestFixture]
    public class MessageValidationTest
    {
        [Test]
        public void ValidateMessages()
        {
            bool passed = true;
            // Get List of UniversalMessages
            IEnumerable<ValidationResult> classTypes = Assembly.GetExecutingAssembly().GetTypes().Where(x => x.IsClass && x.IsConcreteAndAssignableTo<UniversalMessage>()).Distinct().Select(x => new ValidationResult(x)).ToList();
            foreach (ValidationResult validationResult in classTypes.OrderBy(x => x.ClassType.FullName))
            {
                if (validationResult.ClassType == typeof(MessageValidationTest) || validationResult.ClassType == typeof(ValidationResult))
                {
                    continue;
                }
                if (validationResult.ProperlyConfigured)
                {
                    using (ConsoleColorKeeper.KeepForeground(ConsoleColor.Green))
                    {
                        //Console.WriteLine($"{validationResult.ClassType.Name}: is properly configured");
                    }
                }
                else
                {
                    using (ConsoleColorKeeper.KeepForeground(ConsoleColor.Red))
                    {
                        Console.WriteLine($"{validationResult.ClassType.Name}: " +
                                          $"{nameof(ValidationResult.IsSerializable)}={validationResult.IsSerializable} " +
                                          $"{nameof(ValidationResult.IsDataContract)}={validationResult.IsDataContract} " +
                                          $"{nameof(ValidationResult.PropertyDataMemberAttribute)}={validationResult.PropertyDataMemberAttribute} " +
                                          $"{nameof(ValidationResult.ErrorMessageOverride)}={validationResult.ErrorMessageOverride}");
                    }

                    passed = false;
                }
            }
            Assert.IsTrue(passed);
        }
    }

    public class ValidationResult
    {
        public ValidationResult(Type type)
        {
            this.ClassType = type;
        }
        public Type ClassType { get; private set; }
        public ClassCategoryEnum ClassCategory => this.ClassType.IsConcreteAndAssignableTo<UniversalMessage>() ? ClassCategoryEnum.Message : ClassCategoryEnum.Dto;
        public bool IsSerializable => TypeExtensions.HasAttribute<SerializableAttribute>(this.ClassType);
        public bool IsDataContract => TypeExtensions.HasAttribute<DataContractAttribute>(this.ClassType);

        public PropertyInfoEnum PropertyDataMemberAttribute
        {
            get
            {
                PropertyInfo[] members = this.ClassType.GetProperties(BindingFlags.Public | BindingFlags.Instance | BindingFlags.DeclaredOnly);
                members = members.Where(x => x.Name != nameof(UniversalMessage.ErrorMessage) && !TypeExtensions.HasAttribute<IgnoreDataMemberAttribute>(x)).ToArray();
                if (members.All(x => TypeExtensions.HasAttribute<DataMemberAttribute>(x)))
                {
                    return PropertyInfoEnum.All;
                }
                else if (members.Any(x => TypeExtensions.HasAttribute<DataMemberAttribute>(x)))
                {
                    return PropertyInfoEnum.Some;
                }
                else
                {
                    return PropertyInfoEnum.None;
                }
            }
        }

        public bool? ErrorMessageOverride
        {
            get
            {
                if (this.ClassCategory == ClassCategoryEnum.Message)
                {
                    PropertyInfo errorMessageProperty = this.ClassType.GetProperty("ErrorMessage", BindingFlags.Public | BindingFlags.Instance | BindingFlags.DeclaredOnly);
                    if (errorMessageProperty != null)
                    {
                        MethodInfo m1 = errorMessageProperty.GetGetMethod(false);
                        return m1 != m1.GetBaseDefinition();
                    }
                    else
                    {
                        if (this.ClassType.BaseType.Name != typeof(UniversalMessage<>).Name)
                        {
                            errorMessageProperty = this.ClassType.BaseType.GetProperty("ErrorMessage", BindingFlags.Public | BindingFlags.Instance | BindingFlags.DeclaredOnly);
                            if (errorMessageProperty != null)
                            {
                                MethodInfo m1 = errorMessageProperty.GetGetMethod(false);
                                return m1 != m1.GetBaseDefinition();
                            }
                        }
                    }

                    return false;
                }

                return null;
            }
        }

        public bool ProperlyConfigured => this.IsDataContract
                                          && this.IsSerializable
                                          && this.PropertyDataMemberAttribute == PropertyInfoEnum.All
                                          && ((this.ClassCategory == ClassCategoryEnum.Dto) || (this.ClassCategory == ClassCategoryEnum.Message && this.ErrorMessageOverride.HasValue && this.ErrorMessageOverride.Value));
    }

    public enum ClassCategoryEnum
    {
        Message,
        Dto
    }

    public enum PropertyInfoEnum
    {
        All,
        Some,
        None
    }
}

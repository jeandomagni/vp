namespace Ivh.Common.VisitPay.Messages.Core
{
    using System;
    using System.Runtime.Serialization;
    using ServiceBus.Common.Messages;

    [Serializable]
    [DataContract]
    public class VisitBalanceChangedMessageList : UniversalMessageList<VisitBalanceChangedMessage>
    {
    }
}
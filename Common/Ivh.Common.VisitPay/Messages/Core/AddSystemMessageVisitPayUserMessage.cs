﻿namespace Ivh.Common.VisitPay.Messages.Core
{
    using System;
    using System.Runtime.Serialization;
    using Enums;
    using ServiceBus.Common.Messages;

    [Serializable]
    [DataContract]
    public class AddSystemMessageVisitPayUserMessage : UniversalMessage<AddSystemMessageVisitPayUserMessage>
    {
        [DataMember(Order = 1)]
        public int VisitPayUserId { get; set; }
        [DataMember(Order = 2)]
        public SystemMessageEnum SystemMessageEnum { get; set; }
        [IgnoreDataMember]
		public override string ErrorMessage => this.FormatErrorMessage(this, x => x.VisitPayUserId);
    }
}
namespace Ivh.Common.VisitPay.Messages.Core
{
    using System;
    using System.Runtime.Serialization;
    using ServiceBus.Common.Messages;

    [Serializable]
    [DataContract]
    public class VisitBalanceChangedMessage : UniversalMessage<VisitBalanceChangedMessage>
    {
        [DataMember(Order = 0)]
        public int VpGuarantorId { get; set; }

        [DataMember(Order = 1)]
        public int VisitId { get; set; }

        [IgnoreDataMember]
		public override string ErrorMessage => this.FormatErrorMessage(this, x => x.VpGuarantorId, x => x.VisitId);
    }
}
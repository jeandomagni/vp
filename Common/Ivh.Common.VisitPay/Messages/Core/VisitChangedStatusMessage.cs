﻿namespace Ivh.Common.VisitPay.Messages.Core
{
    using System;
    using System.Runtime.Serialization;
    using Enums;
    using ServiceBus.Common.Messages;

    [Serializable]
    [DataContract]
    public class VisitChangedStatusMessage : UniversalMessage<VisitChangedStatusMessage>
    {
        [DataMember(Order = 1)]
        public int VisitId { get; set; }
        [DataMember(Order = 2)]
        public VisitStateEnum PreviousState { get; set; }
        [DataMember(Order = 3)]
        public VisitStateEnum NewState { get; set; }
        [DataMember(Order = 4)]
        public DateTime EventDate { get; set; }
        [IgnoreDataMember]
		public override string ErrorMessage => this.FormatErrorMessage(this, x => x.VisitId);
    }
}
﻿namespace Ivh.Common.VisitPay.Messages.Core
{
    using System;
    using System.Collections.Generic;
    using System.Runtime.Serialization;
    using ServiceBus.Common.Messages;

    [Serializable]
    [DataContract]
    public class AuditEventFinancePlanMessage : UniversalMessage<AuditEventFinancePlanMessage>
    {
        [DataMember(Order = 1)]
        public int FinancePlanId { get; set; }
        [DataMember(Order = 2)]
        public IList<string> AuditEvents { get; set; }
        [IgnoreDataMember]
		public override string ErrorMessage => this.FormatErrorMessage(this, x => x.FinancePlanId);
    }
}

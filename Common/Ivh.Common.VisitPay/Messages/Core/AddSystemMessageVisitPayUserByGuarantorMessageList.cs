﻿namespace Ivh.Common.VisitPay.Messages.Core
{
    using System;
    using System.Runtime.Serialization;
    using ServiceBus.Common.Messages;

    [Serializable]
    [DataContract]
    public class AddSystemMessageVisitPayUserByGuarantorMessageList : UniversalMessageList<AddSystemMessageVisitPayUserByGuarantorMessage>
    {
        [IgnoreDataMember]
		public override string ErrorMessage => this.FormatErrorMessage(this.Messages);
    }
}
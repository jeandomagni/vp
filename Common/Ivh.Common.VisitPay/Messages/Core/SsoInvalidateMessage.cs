﻿namespace Ivh.Common.VisitPay.Messages.Core
{
    using System;
    using System.Runtime.Serialization;
    using ServiceBus.Common.Messages;

    [Serializable]
    [DataContract]
    public class SsoInvalidateMessage : UniversalMessage<SsoInvalidateMessage>
    {
        [DataMember(Order = 1)]
        public int VisitPayUserId { get; set; }
        [DataMember(Order = 2)]
        public int VpGuarantorId { get; set; }
        [DataMember(Order = 3)]
        public int HsGuarantorId { get; set; }
        [IgnoreDataMember]
		public override string ErrorMessage => this.FormatErrorMessage(this, x => x.VisitPayUserId, x => x.VpGuarantorId, x => x.HsGuarantorId);
    }
}

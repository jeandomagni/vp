﻿namespace Ivh.Common.VisitPay.Messages.Core
{
    using System;
    using System.Runtime.Serialization;
    using ServiceBus.Common.Messages;

    [Serializable]
    [DataContract]
    public class SearchTextLogMessage : UniversalMessage<SearchTextLogMessage>
    {
        [DataMember(Order = 1)]
        public string SearchText { get; set; }

        [DataMember(Order = 2)]
        public DateTime Date { get; set; }

        [DataMember(Order = 3)]
        public int SearchTextId { get; set; }
        
        [IgnoreDataMember]
		public override string ErrorMessage => this.FormatErrorMessage(this, x => x.SearchTextId);
    }
}

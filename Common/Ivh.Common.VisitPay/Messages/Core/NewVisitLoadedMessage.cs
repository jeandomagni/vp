﻿namespace Ivh.Common.VisitPay.Messages.Core
{
    using System;
    using System.Runtime.Serialization;
    using ServiceBus.Attributes;
    using ServiceBus.Common.Messages;

    [ClientFriendlyDeliveryRange]
    [Serializable]
    [DataContract]
    public class NewVisitLoadedMessage : UniversalMessage<NewVisitLoadedMessage>
    {
        [DataMember(Order = 1)]
        public int VpGuarantorId { get; set; }

        [IgnoreDataMember]
        public override string ErrorMessage => this.FormatErrorMessage(this, x => x.VpGuarantorId);
    }
}

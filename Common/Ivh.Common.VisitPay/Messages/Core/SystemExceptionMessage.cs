﻿namespace Ivh.Common.VisitPay.Messages.Core
{
    using System;
    using System.Runtime.Serialization;
    using Enums;
    using ServiceBus.Common.Messages;

    [Serializable]
    [DataContract]
    public class SystemExceptionMessage : UniversalMessage<SystemExceptionMessage>
    {
        //General Fields
        [DataMember(Order = 1)]
        public SystemExceptionTypeEnum SystemExceptionType { get; set; }
        [DataMember(Order = 2)]
        public string SystemExceptionDescription { get; set; }

        //Sparse Fields
        [DataMember(Order = 3)]
        public int? VpGuarantorId { get; set; }
        [DataMember(Order = 5)]
        public int? PaymentId { get; set; }
        [DataMember(Order = 6)]
        public string TransactionId { get; set; }
        [DataMember(Order = 7)]
        public int? FinancePlanId { get; set; }
        [IgnoreDataMember]
		public override string ErrorMessage => this.FormatErrorMessage(this, x => x.VpGuarantorId, x => x.FinancePlanId);

    }
}

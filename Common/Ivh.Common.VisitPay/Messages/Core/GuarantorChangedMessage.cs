﻿namespace Ivh.Common.VisitPay.Messages.Core
{
    using System;
    using System.Collections.Generic;
    using System.Runtime.Serialization;
    using Enums;
    using ServiceBus.Common.Messages;

    [Serializable]
    [DataContract]
    public class GuarantorChangedMessage : UniversalMessage<GuarantorChangedMessage>
    {
        [DataMember(Order = 1)]
        public int VpGuarantorId { get; set; }

        [DataMember(Order = 2)]
        public VpGuarantorStatusEnum VpGuarantorStatus { get; set; }

        [DataMember(Order = 3)]
        public GuarantorTypeEnum GuarantorTypeEnum { get; set; }

        [DataMember(Order = 4)]
        public DateTime EventDate { get; set; }

        [IgnoreDataMember]
        public override string ErrorMessage => this.FormatErrorMessage(this, x => x.VpGuarantorId);
    }
}

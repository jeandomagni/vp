﻿namespace Ivh.Common.VisitPay.Messages.Core
{
    using System;
    using System.Runtime.Serialization;
    using ServiceBus.Common.Messages;

    [Serializable]
    [DataContract]
    public class SendGuarantorAccountLockoutEmailMessage : UniversalMessage<SendGuarantorAccountLockoutEmailMessage>
    {
        [DataMember(Order = 1)]
        public int VisitPayUserId { get; set; }

        [DataMember(Order = 2)]
        public string Email { get; set; }

        [DataMember(Order = 3)]
        public string Name { get; set; }
        [IgnoreDataMember]
		public override string ErrorMessage => this.FormatErrorMessage(this, x => x.VisitPayUserId);
    }
}

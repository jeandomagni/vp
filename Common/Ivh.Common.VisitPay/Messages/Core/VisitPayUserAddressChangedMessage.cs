﻿namespace Ivh.Common.VisitPay.Messages.Core
{
    using System;
    using System.Runtime.Serialization;
    using ServiceBus.Common.Messages;

    [Serializable]
    [DataContract]
    public class VisitPayUserAddressChangedMessage : UniversalMessage<VisitPayUserAddressChangedMessage>
    {
        [DataMember(Order = 1)]
        public int VisitPayUserId { get; set; }
        [IgnoreDataMember]
		public override string ErrorMessage => this.FormatErrorMessage(this, x => x.VisitPayUserId);
    }
}
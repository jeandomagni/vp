﻿namespace Ivh.Common.VisitPay.Messages.Core
{
    using System;
    using System.Runtime.Serialization;
    using ServiceBus.Common.Messages;

    [Serializable]
    [DataContract]
    public class ReceiveTextToPayPaymentOptionMessage : UniversalMessage<ReceiveTextToPayPaymentOptionMessage>
    {
        /// <summary>
        /// The raw phone number that comes from TwilioCallbackMessage.From
        /// expected format +12082441234
        /// </summary>
        [DataMember(Order = 1)]
        public string PhoneNumber { get; set; }
        
        /// <summary>
        /// The raw body that comes from TwilioCallbackMessage.Body
        /// expected "Pay" but we may parse more in the future
        /// </summary>
        [DataMember(Order = 2)]
        public string RawBodyText { get; set; }
        
        [IgnoreDataMember]
		public override string ErrorMessage => this.FormatErrorMessage(this, x => x.PhoneNumber);
    }
}
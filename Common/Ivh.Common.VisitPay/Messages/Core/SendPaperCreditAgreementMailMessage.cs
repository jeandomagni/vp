﻿namespace Ivh.Common.VisitPay.Messages.Core
{
    using System;
    using System.Runtime.Serialization;
    using ServiceBus.Common.Messages;

    [Serializable]
    [DataContract]
    public class SendPaperCreditAgreementMailMessage: UniversalMessage<SendPaperCreditAgreementMailMessage>
    {
        [DataMember(Order = 1)]
        public int VisitPayUserId { get; set; }

        [DataMember(Order = 2)]
        public string Name { get; set; }

        [DataMember(Order = 3)]
        public int FinancePlanOfferId { get; set; }

        [DataMember(Order = 4)]
        public DateTime ActionDate { get; set; }

        [IgnoreDataMember]
        public override string ErrorMessage => this.FormatErrorMessage(this, x => x.VisitPayUserId, x => x.FinancePlanOfferId);
    }
}

namespace Ivh.Common.VisitPay.Messages.Core
{
    using System;
    using System.Runtime.Serialization;
    using ServiceBus.Common.Messages;

    [Serializable]
    [DataContract]
    public class ResendEmailMessage : UniversalMessage<ResendEmailMessage>
    {
        [DataMember(Order = 1)]
        public int EmailId { get; set; }

        [DataMember(Order = 2)]
        public string ToAddress { get; set; }

        [DataMember(Order = 3)]
        public int? VpGuarantorId { get; set; }

        [DataMember(Order = 4)]
        public int? SentByVisitPayUserId { get; set; }
        [IgnoreDataMember]
		public override string ErrorMessage => this.FormatErrorMessage(this, x => x.VpGuarantorId);
    }
}
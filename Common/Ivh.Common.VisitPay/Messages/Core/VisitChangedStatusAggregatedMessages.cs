﻿namespace Ivh.Common.VisitPay.Messages.Core
{
    using System;
    using System.Collections.Generic;
    using System.Runtime.Serialization;
    using ServiceBus.Common.Messages;

    [Serializable]
    [DataContract]
    public class VisitChangedStatusAggregatedMessages : UniversalMessageList<VisitChangedStatusMessage>
    {
        public VisitChangedStatusAggregatedMessages(IList<VisitChangedStatusMessage> messages = null) : base(messages)
        {

        }
    }
}
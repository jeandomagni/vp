﻿namespace Ivh.Common.VisitPay.Messages.Core
{
    using System;
    using System.Runtime.Serialization;
    using ServiceBus.Common.Messages;

    [Serializable]
    [DataContract]
    public class TextToPayPaymentOptionMessage : UniversalMessage<TextToPayPaymentOptionMessage>
    {
        [DataMember(Order = 1)]
        public int VpGuarantorId { get; set; }
        
        //TODO: we could add the payment type id here
        
        [IgnoreDataMember]
		public override string ErrorMessage => this.FormatErrorMessage(this, x => x.VpGuarantorId);
    }
}
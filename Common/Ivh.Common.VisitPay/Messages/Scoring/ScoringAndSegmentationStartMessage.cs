namespace Ivh.Common.VisitPay.Messages.Scoring
{
    using System;
    using System.Collections.Generic;
    using System.Runtime.Serialization;
    using HospitalData.Dtos;
    using MassTransit;

    [DataContract]
    [Serializable]
    public class ScoringAndSegmentationStartMessage : CorrelatedBy<Guid>
    {
        [DataMember(Order = 1)]
        public Guid CorrelationId { get; set; }
        [DataMember(Order = 2)]
        public List<List<ScoringGuarantorDto>> ScoringGuarantorDtos { get; set; }
        [DataMember(Order = 3)]
        public DateTime StartTime { get; set; }
        [DataMember(Order = 4)]
        public int ScoringBatchId { get; set; }
        [DataMember(Order = 5)]
        public SegmentationEnable SegmentationEnable { get; set; }
    }
}
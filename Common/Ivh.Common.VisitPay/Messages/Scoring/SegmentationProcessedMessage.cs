namespace Ivh.Common.VisitPay.Messages.Scoring
{
    using System;
    using System.Runtime.Serialization;
    using MassTransit;

    [DataContract]
    [Serializable]
    public class SegmentationProcessedMessage : CorrelatedBy<Guid>
    {
        [DataMember(Order = 1)]
        public Guid CorrelationId { get; set;  }
    }
}
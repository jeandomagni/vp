﻿namespace Ivh.Common.VisitPay.Messages.Scoring
{
    using System;
    using System.Collections.Generic;
    using System.Runtime.Serialization;
    using HospitalData.Dtos;
    using MassTransit;

    [DataContract]
    [Serializable]
    public class SegmentationActivePassiveResultMessage : CorrelatedBy<Guid>
    {
        [DataMember(Order = 1)]
        public Guid CorrelationId { get; set; }
        [DataMember(Order = 2)]
        public IList<SegmentationActivePassiveDto> SegmentationActivePassiveVisits { get; set; }
    }
}

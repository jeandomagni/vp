﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ivh.Common.VisitPay.Messages.Scoring.Dtos
{
    public class SegmentationDto
    {
        public int HsGuarantorId { get; set; }
        public int SegmentationBatchId { get; set; }
        public decimal HsCurrentBalance { get; set; }
        public decimal? PtpScore { get; set; }
        public string RawSegmentationScore { get; set; }
        public string ScriptVersion { get; set; }
        public string AssignedAgencyIds { get; set; }
    }
}

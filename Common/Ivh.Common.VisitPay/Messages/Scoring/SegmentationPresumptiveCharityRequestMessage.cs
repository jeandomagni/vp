﻿namespace Ivh.Common.VisitPay.Messages.Scoring
{
    using System;
    using System.Runtime.Serialization;
    using MassTransit;

    [DataContract]
    [Serializable]
    public class SegmentationPresumptiveCharityRequestMessage : CorrelatedBy<Guid>
    {
        [DataMember(Order = 1)]
        public Guid CorrelationId { get; set; }
        [DataMember(Order = 2)]
        public Guid ProcessId { get; set; }
        [DataMember(Order = 3)]
        public SegmentationEnable SegmentationEnable { get; set; }
        [DataMember(Order = 4)]
        public int BatchId { get; set; }
        [DataMember(Order = 5)]
        public bool IsProcessed { get; set; }
    }
}
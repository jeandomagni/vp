﻿namespace Ivh.Common.VisitPay.Messages.Scoring
{
    using System;
    using System.Collections.Generic;
    using System.Runtime.Serialization;
    using HospitalData.Dtos;
    using MassTransit;

    [DataContract]
    [Serializable]
    public class SegmentationAccountsReceivableRequestMessage : CorrelatedBy<Guid>
    {
        [DataMember(Order = 1)]
        public Guid CorrelationId { get; set; }
        [DataMember(Order = 2)]
        public Guid ProcessId { get; set; }
        [DataMember(Order = 3)]
        public SegmentationEnable SegmentationEnable { get; set; }
        [DataMember(Order = 4)]
        public List<SegmentationAccountsReceivableDto> AccountsReceivableDtos { get; set; }
        [DataMember(Order = 5)]
        public int BatchId { get; set; }
        [DataMember(Order = 6)]
        public bool IsProcessed { get; set; }
    }
}
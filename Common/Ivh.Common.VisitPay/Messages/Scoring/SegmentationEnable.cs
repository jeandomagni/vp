﻿namespace Ivh.Common.VisitPay.Messages.Scoring
{
    using System;

    [Serializable]
    public class SegmentationEnable
    {
        public bool EnableBadDebtSegmentation { get; set; }
        public bool EnableAccountsReceivableSegmentation { get; set; }
        public bool EnableActivePassiveSegmentation { get; set; }
        public bool EnablePresumptiveCharitySegmentation { get; set; }
    }
}

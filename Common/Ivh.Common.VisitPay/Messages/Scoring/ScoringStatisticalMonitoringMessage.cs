﻿namespace Ivh.Common.VisitPay.Messages.Scoring
{
    using System;
    using System.Runtime.Serialization;
    using Enums;
    using HospitalData.Dtos;
    using ServiceBus.Common.Messages;

    [Serializable]
    [DataContract]
    public class ScoringStatisticalMonitoringMessage : UniversalMessage<ScoringStatisticalMonitoringMessage>
    {
        [DataMember(Order = 1)]
        public ScoringStatisticalMonitoringDto ScoringStatisticalMonitoringDto { get; set; }

        [DataMember(Order = 2)]
        public ScoringMonitoringThresholdsDto ScoringMonitoringThresholdsDto { get; set; }

        [DataMember(Order = 3)]
        public ScoringMonitoringTypeEnum ScoringMonitoringTypeEnum { get; set; }

        [IgnoreDataMember]
		public override string ErrorMessage => this.FormatErrorMessage(this);
    }
}
﻿namespace Ivh.Common.VisitPay.Messages.Monitoring
{
    using System;
    using System.Collections.Generic;
    using System.Runtime.Serialization;
    using ServiceBus.Common.Messages;

    [Serializable]
    [DataContract]
    public class ControllerTimingMessages : UniversalMessage<ControllerTimingMessages>
    {
        [DataMember(Order = 1)]
        public List<ControllerTimingMessage> Messages { get; set; }

        [IgnoreDataMember]
		public override string ErrorMessage => this.FormatErrorMessage(this);
    }
}
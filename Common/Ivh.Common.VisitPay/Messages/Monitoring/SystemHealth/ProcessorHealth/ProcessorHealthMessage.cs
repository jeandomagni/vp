﻿namespace Ivh.Common.VisitPay.Messages.Monitoring.SystemHealth.ProcessorHealth
{
    using System;
    using System.Runtime.Serialization;
    using Enums;
    using ServiceBus.Common.Messages;

    [Serializable]
    [DataContract]
    public class ProcessorHealthMessage : UniversalMessage<ProcessorHealthMessage>
    {
        [DataMember(Order = 1)]
        public Guid RoundTripToken { get; set; }
        [DataMember(Order = 1)]
        public ApplicationEnum Application { get; set; }
        [IgnoreDataMember]
        public override string ErrorMessage => this.FormatErrorMessage(this);
    }
}
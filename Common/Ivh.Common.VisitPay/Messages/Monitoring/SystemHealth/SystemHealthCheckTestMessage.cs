﻿namespace Ivh.Common.VisitPay.Messages.Monitoring.SystemHealth
{
    using System;
    using System.Runtime.Serialization;
    using ServiceBus.Common.Messages;

    [Serializable]
    [DataContract]
    public class SystemHealthCheckTestMessage : UniversalMessage<SystemHealthCheckTestMessage>
    {
        [IgnoreDataMember]
        public override string ErrorMessage => this.FormatErrorMessage(this);
    }
}
﻿namespace Ivh.Common.VisitPay.Messages.Monitoring
{
    using System;
    using System.Runtime.Serialization;
    using ServiceBus.Common.Messages;

    [Serializable]
    [DataContract]
    public class ControllerTimingMessage : UniversalMessage<ControllerTimingMessage>
    {
        [DataMember(Order = 1)]
        public string Name { get; set; }
        [DataMember(Order = 2)]
        public DateTime TimeActionExecuting { get; set; }
        [DataMember(Order = 3)]
        public DateTime TimeActionExecuted { get; set; }
        [DataMember(Order = 4)]
        public DateTime TimeResultExecuted { get; set; }

        [IgnoreDataMember]
		public override string ErrorMessage => this.FormatErrorMessage(this);
    }
}
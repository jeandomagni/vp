﻿namespace Ivh.Common.VisitPay.Messages.Consolidation
{
    using System;
    using System.Runtime.Serialization;

    [Serializable]
    [DataContract]
    public class ConsolidationHouseholdRequestCancelledByManagingToManagedMessage : ConsolidationBase<ConsolidationHouseholdRequestCancelledByManagingToManagedMessage>
    {
    }
}
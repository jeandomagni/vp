﻿namespace Ivh.Common.VisitPay.Messages.Consolidation
{
    using System;
    using System.Runtime.Serialization;

    [Serializable]
    [DataContract]
    public class ConsolidationHouseholdRequestAcceptedByManagedWithFpToManagingMessage : ConsolidationBase<ConsolidationHouseholdRequestAcceptedByManagedWithFpToManagingMessage>
    {
        [DataMember(Order = 2)]
        public string ConsolidationRequestExpiryDay { get; set; }
    }
}
﻿namespace Ivh.Common.VisitPay.Messages.Consolidation
{
    using System;
    using System.Runtime.Serialization;

    [Serializable]
    [DataContract]
    public class ConsolidationHouseholdRequestToManagingMessage : ConsolidationBase<ConsolidationHouseholdRequestToManagingMessage>
    {
        [DataMember(Order = 2)]
        public string ConsolidationRequestExpiryDay { get; set; }
    }
}
﻿namespace Ivh.Common.VisitPay.Messages.Consolidation
{
    using System;
    using System.Runtime.Serialization;
    using ServiceBus.Common.Messages;

    [Serializable]
    [DataContract]
    public class ConsolidationExpiredMessage : UniversalMessage<ConsolidationExpiredMessage>
    {
        [DataMember(Order = 0)]
        public int ConsolidationGuarantorId { get; set; }

        [DataMember(Order = 1)]
        public DateTime ExpirationDateTime { get; set; }

        [IgnoreDataMember]
		public override string ErrorMessage => this.FormatErrorMessage(this, x => x.ConsolidationGuarantorId);
    }
}
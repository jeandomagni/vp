﻿namespace Ivh.Common.VisitPay.Messages.Consolidation
{
    using System;
    using System.Runtime.Serialization;

    [Serializable]
    [DataContract]
    public class ConsolidationHouseholdRequestToManagedMessage : ConsolidationBase<ConsolidationHouseholdRequestToManagedMessage>
    {
        [DataMember(Order = 2)]
        public string ConsolidationRequestExpiryDay { get; set; }
    }
}
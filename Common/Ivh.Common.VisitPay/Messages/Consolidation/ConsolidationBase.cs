﻿namespace Ivh.Common.VisitPay.Messages.Consolidation
{
    using System;
    using System.Runtime.Serialization;
    using ServiceBus.Common.Messages;

    [Serializable]
    [DataContract]
    public abstract class ConsolidationBase<TMessage> : UniversalMessage<TMessage> where TMessage : ConsolidationBase<TMessage>// Not published seperately. No Consumer.
    {
        [DataMember(Order = 0)]
        public int ManagingVpGuarantorId { get; set; }

        [DataMember(Order = 1)]
        public int ManagedVpGuarantorId { get; set; }
        [IgnoreDataMember]
		public override string ErrorMessage => $"{typeof(TMessage).Name};{nameof(this.ManagingVpGuarantorId)}={this.ManagingVpGuarantorId};{nameof(this.ManagedVpGuarantorId)}={this.ManagedVpGuarantorId}";
    }
}
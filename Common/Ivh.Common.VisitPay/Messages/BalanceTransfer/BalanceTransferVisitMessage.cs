﻿namespace Ivh.Common.VisitPay.Messages.BalanceTransfer
{
    using System;
    using System.Runtime.Serialization;
    using ServiceBus.Attributes;
    using ServiceBus.Common.Messages;

    [Serializable]
    [DataContract]
    [UniversalMessage(consumable: false)]
    public class BalanceTransferVisitMessage : UniversalMessage<BalanceTransferVisitMessage>
    {
        [DataMember(Order = 1)]
        public int VisitId { get; set; }
        [DataMember(Order = 2)]
        public string VisitSourceSystemKey { get; set; }
        [DataMember(Order = 3)]
        public int VisitBillingSystemId { get; set; }
        [DataMember(Order = 4)]
        public decimal PrincipalPaymentAmount { get; set; }
        [DataMember(Order = 5)]
        public decimal InterestPaymentAmount { get; set; }
        [DataMember(Order = 6)]
        public int BalanceTransferStatusId { get; set; }

        [IgnoreDataMember]
		public override string ErrorMessage => this.FormatErrorMessage(this, x => x.VisitId);
    }
}
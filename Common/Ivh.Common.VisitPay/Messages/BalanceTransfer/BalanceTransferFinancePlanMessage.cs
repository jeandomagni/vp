﻿namespace Ivh.Common.VisitPay.Messages.BalanceTransfer
{
    using System;
    using System.Runtime.Serialization;
    using ServiceBus.Attributes;
    using ServiceBus.Common.Messages;

    [Serializable]
    [DataContract]
    [UniversalMessage(consumable: false)]
    public class BalanceTransferFinancePlanMessage : UniversalMessage<BalanceTransferFinancePlanMessage>
    {
        [DataMember(Order = 1)]
        public int FinancePlanId { get; set; }
        [DataMember(Order = 2)]
        public int FinancePlanStatusId { get; set; }
        [DataMember(Order = 3)]
        public int DurationRangeStart { get; set; }
        [DataMember(Order = 4)]
        public int DurationRangeEnd { get; set; }
        [IgnoreDataMember]
		public override string ErrorMessage => this.FormatErrorMessage(this, x => x.FinancePlanId);
    }
}
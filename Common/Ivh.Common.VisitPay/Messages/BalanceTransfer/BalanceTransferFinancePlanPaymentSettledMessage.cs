﻿namespace Ivh.Common.VisitPay.Messages.BalanceTransfer
{
    using System;
    using System.Collections.Generic;
    using System.Runtime.Serialization;
    using ServiceBus.Common.Messages;

    [Serializable]
    [DataContract]
    public class BalanceTransferFinancePlanPaymentSettledMessage : UniversalMessage<BalanceTransferFinancePlanPaymentSettledMessage>
    {
        [DataMember(Order = 1)]
        public IList<BalanceTransferVisitMessage> Visits { get; set; }

        [DataMember(Order = 2)]
        public BalanceTransferFinancePlanMessage FinancePlan { get; set; }

        [DataMember(Order = 3)]
        public BalanceTransferPaymentMessage Payment { get; set; }

        [IgnoreDataMember]
		public override string ErrorMessage => this.FormatErrorMessage(this, x => x.FinancePlan.FinancePlanId);
    }
}
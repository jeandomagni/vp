﻿namespace Ivh.Common.VisitPay.Messages.BalanceTransfer
{
    using System;
    using System.Runtime.Serialization;
    using Enums;
    using ServiceBus.Attributes;
    using ServiceBus.Common.Messages;

    [Serializable]
    [DataContract]
    [UniversalMessage(consumable: false)]
    public class BalanceTransferPaymentMessage : UniversalMessage<BalanceTransferPaymentMessage>
    {
        [DataMember(Order = 1)]
        public PaymentTypeEnum PaymentType { get; set; }

        [IgnoreDataMember]
		public override string ErrorMessage => this.FormatErrorMessage(this, x => x.PaymentType);
    }
}
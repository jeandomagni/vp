﻿namespace Ivh.Common.VisitPay.Strings
{
    public static class SsoMessages
    {
        public const string FailureNoHsa = "Connection not established, we were unable to detect an HSA. If you only have an HRA or FSA, SSO is not available to you at this time.";
        public const string FailureSsn = "Connection not established, we were unable to authenticate your account.";
        public const string FailureEndpoint = "We were unable to connect, Single Sign-On unsuccessful.";
        public const string FailureUnknown = "We weren't able to connect to HealthEquity.";
    }
}
﻿namespace Ivh.Common.VisitPay.Strings
{
    /// <summary>
    /// These are string constants. Ideally these would be in a resource file and localizable.
    /// </summary>
    public static class Format
    {
        public const string DateFormat = "MM/dd/yyyy";
        public const string DateFormat2 = "yyyy-MM-dd";
        public const string DateFormatMonthYear = "MM/yyyy";
        public const string DateFormatShortYear = "MM/dd/yy";
        public const string DateFormatDayFirst = "dd/MM/yyyy";
        public const string DateTimeFormat = "MM/dd/yyyy hh:mm tt";
        public const string DateTimeFormatExcel = "MM/dd/yyyy hh:mm AM/PM";
        public const string DateTimeFormatSql = "yyyy-MM-dd HH:mm:ss";
        public const string DateTimeFormat_MMddyyyyHHmmss = "MM-dd-yyyy HH:mm:ss";
        public const string ExportMoneyFormat = "$#,##0.00";
        public const string TimeFormat = "hh:mm tt";
        public const string MonthYearFormat = "MMM yyyy";
        public const string FullMonthYearFormat = "MMMM yyyy";
        public const string MonthFormat = "MMMM";
        public const string MonthDayYearFormat = "MMMM dd, yyyy";
        public const string MonthDayYearFormat1 = "MMM dd, yyyy";
        public const string FirstNameLastNamePattern = "{0} {1}";
        public const string LastNameFirstNamePattern = "{0}, {1}";
        public const string FullNamePattern = "{0} {1} {2}";
        public const string FolderDate = "yyyyMMdd";
        public const string FolderTime = "HHmmss";
        public const string YearMonthDayHourMinuteSecondMillisecond = "yyyy-MM-dd HH:mm:ss.fff";
        public const string YearMonthDay = "yyyyMMdd";
    }
}

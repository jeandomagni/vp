﻿namespace Ivh.Common.VisitPay.Strings
{
    public static class VisitPayRoleStrings
    {
        public static class Admin
        {

            /// <summary>
            /// Administrator
            /// </summary>
            public const string Administrator = "Administrator";
        }

        public static class System
        {
            /// <summary>
            /// System Roles
            /// </summary>
            public const string Client = "Client";

            public const string Patient = "Patient";
            public const string QATools = "QATools";
        }

        public static class Csr
        {
            /// <summary>
            /// CSR related Roles
            /// </summary>
            public const string CSR = "CSR"; //DEFAULT
            
            public const string ArrangePayment = "ArrangePayment";
            public const string ChangePaymentDueDay = "ChangePaymentDueDay";
            public const string CancelFP = "CancelFP";
            public const string CancelRecurringPayment = "CancelRecurringPayment";
            public const string ExtendedFinancePlanOfferSets = "ExtendedFinancePlanOfferSets";
            public const string ManagePaymentMethods = "ManagePaymentMethods";
            public const string ReconfigureFP = "ReconfigureFP";
            public const string RescheduleRecurringPayment = "RescheduleRecurringPayment";
            public const string SupportAdmin = "SupportAdmin";
            public const string SupportCreate = "SupportCreate";
            public const string SupportTemplateAdmin = "SupportTemplateAdmin";
            public const string SupportView = "SupportView";
            public const string UserAdmin = "UserAdmin";
            public const string ResetVisitAging = "ResetVisitAging";
            public const string CallCenter = "CallCenter";
            public const string ChatOperator = "ChatOperator";
            public const string ChatOperatorAdmin = "ChatOperatorAdmin";
            public const string PaymentQueue = "PaymentQueue";
        }

        public static class Payment
        {
            /// <summary>
            /// Payment Related Roles
            /// </summary>
            public const string PaymentRefund = "PaymentRefund";
            public const string PaymentVoid = "PaymentVoid";
            public const string CardReader = "CardReader";
            public const string PaymentRefundReturnedCheck = "PaymentRefundReturnedCheck";
        }

        public static class Miscellaneous
        {
            /// <summary>
            /// Miscellaneous roles
            /// </summary>
            public const string ClientUserManagement = "ClientUserManagement";

            public const string IvhAdmin = "IvhAdmin";

            public const string IvhEmployee = "IvhEmployee";

            public const string Reports = "Reports";

            public const string Unmatch = "Unmatch";

            public const string VpSupportTickets = "VpSupportTickets";

            public const string VpSupportTicketsManage = "VpSupportTicketsManage";

            public const string VpSupportTicketAdmin = "VpSupportTicketAdmin";

            public const string VisitPayDocument = "VisitPayDocument";

        }

        public static class Restricted
        {
            public const string IvhPciAuditor = "IvhPciAuditor";

            public const string ClientPciAuditor = "ClientPciAuditor";

            public const string AuditReviewer = "AuditReviewer";

            public const string IvhUserAuditor = "IvhUserAuditor";

            public const string ClientUserAuditor = "ClientUserAuditor";

            public const string AuditEventLogViewer = "AuditEventLogViewer";

            public const string VisitPayReports = "VisitPayReports";

            public const string AnalyticReports = "AnalyticReports";

        }
    }
}
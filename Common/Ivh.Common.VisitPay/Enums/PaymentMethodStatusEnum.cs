﻿namespace Ivh.Common.VisitPay.Enums
{
    public enum PaymentMethodStatusEnum
    {
        Activated = 1,
        Modified = 2,
        SetPrimary = 3,
        Deactivated = 4,

        AddedToGroup = 5,
        ModifiedInGroup = 6,
        RemovedFromGroup = 7
    }
}
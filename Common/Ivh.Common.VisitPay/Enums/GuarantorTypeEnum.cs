﻿namespace Ivh.Common.VisitPay.Enums
{
    public enum GuarantorTypeEnum
    {
        Online = 1,
        Offline = 2,
        Lite = 3
    }
}
﻿namespace Ivh.Common.VisitPay.Enums
{
    public enum ConsolidationGuarantorStatusEnum
    {
        Pending = 1,
        PendingFinancePlan = 2,
        Accepted = 3,
        Rejected = 4,
        Cancelled = 5
    }
}
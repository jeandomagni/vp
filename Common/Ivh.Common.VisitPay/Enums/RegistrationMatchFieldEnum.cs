﻿namespace Ivh.Common.VisitPay.Enums
{
    public enum RegistrationMatchFieldEnum
    {
        Ssn4 = 1,
        PostalCode = 2,
        PatientDateOfBirth = 3
    }
}

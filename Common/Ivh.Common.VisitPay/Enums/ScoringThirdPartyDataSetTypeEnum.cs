﻿namespace Ivh.Common.VisitPay.Enums
{
    public enum ScoringThirdPartyDataSetTypeEnum
    {
        Scoring = 1,
        Segmentation = 2
    }
}
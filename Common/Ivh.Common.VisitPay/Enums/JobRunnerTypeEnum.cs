﻿namespace Ivh.Common.VisitPay.Enums
{
    using System.ComponentModel;

    [DefaultValue(Unknown)]
    public enum JobRunnerTypeEnum
    {
        QueueAgeVisits,
        QueueScheduledPaymentsByGuarantor,
        QueueStatements,
        QueueVisitChangeSet,
        QueueScheduledPaymentReminders,
        QueuePendingUncollectableNotifications,
        QueueClosedUncollectables,
        QueueAchDownload,
        QueueAchPublish,
        QueueSystemExceptionLogging,
        QueuePaymentReconciliation,
        QueueConsolidationExpiration,
        QueueConsolidationReminders,
        QueuePaymentMethod,
        QueueVisitReconciliation,
        QueueBalanceDueReminders,
        QueueHeldEmailForResend,
        GuarantorSnapshotAndHist,
        AccountSnapshotAndHist,
        TransactionSnapshotAndHist,
        PublishCdiEtlToVpEtl,
        PublishMonitorData,
        PublishGaugesByName,
        PublishGaugeSetsByName,
        QueueChangeEvents,
        GuarantorStageTruncInsert,
        VisitStageTruncInsert,
        VisitTransactionStageTruncInsert,
        HsGuarantor,
        Visit,
        VisitTransaction,
        JamsIntegrationTest,
        FixPaymentAllocationsForPayment,
        RemovePaymentBucketsForFinancePlan,
        DeactivateInactiveClientUsers,

        Inbound_LoadBase,
        Inbound_LoadBaseStage,
        Inbound_LoadChangeEvents_Part1,
        Inbound_LoadChangeEvents_Part2,
        Inbound_LoadFiles,
        Inbound_LoadHistory,
        Inbound_LoadSnapshotAndDelta,
        Inbound_LoadClaimsFiles,
        Inbound_ScoringAndSegmentation,
        Inbound_PaymentVendorFile,

        Outbound_PaperFiles,

        QueueMatchRegistryPopulation,
        QueueFindNewHsGuarantorMatchesJobRunner,
        QueueMatchReconiliationJobRunner,

        //HSPersonDailyRecordCount,
        //HSPersonDeltaRecordCount,
        //HSPersonSnapshotRecordCount,
        //HSGuarantorStageRecordCount,
        //HSGuarantorRecordCdiCount,

        //HSiSeriesARAccountDailyRecordCount,
        //HSiSeriesBDAccountDailyRecordCount,
        //HSCernerAccountDailyRecordCount,
        //HSAccountDeltasRecordCount,
        //HSAccountSnapshotsRecordCount,

        //HSiSeriesARTransactionDailyRecordCount,
        //HSCernerChargesDailyRecordCount,
        //HSCernerTransactionDailyRecordCount,
        //HSTransactionDeltasRecordCount,
        //HSTransactionSnapshotsRecordCount,

        //TransactionCountActual,
        //TransactionCountExpected,
        //WorkQueueCountActual,
        //FinancePlanChangesExpected,
        //ReturnAccountsCountActual,
        //UncollectableAccountsCountExpected,
        //AccountReconciliationCountActual,
        //AccountReconciliationCountExpected,
        //InvoiceReconciliationCountActual,
        //InvoiceReconciliationCountExpected,

        FileStorageImport,
        FileStorageVisitMatch,
        StatementExportBatchProcess,
        PaperMailExportBatchProcess,

        Unknown,

        ContentApiTester
    }
}

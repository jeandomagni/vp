﻿namespace Ivh.Common.VisitPay.Enums
{
    using Attributes;

    public static class PaymentValidationMessageEnumCategory
    {
        public const string PromptUser = nameof(PromptUser);
    }

    public enum PaymentValidationMessageEnum
    {
        None,
        
        GeneralError,
        
        FinancePlanBalanceHasChanged,
        
        VisitBalanceHasChanged,
        
        PaymentMethodNearExpiry,

        PaymentMethodExpired,
        
        [EnumCategory(PaymentValidationMessageEnumCategory.PromptUser)]
        PastDueVisits,

        [EnumCategory(PaymentValidationMessageEnumCategory.PromptUser)]
        PastDueFinancePlans,

        InvalidDate
    }
}
namespace Ivh.Common.VisitPay.Enums
{
    public enum OAuthTokenTypeEnum
    {
        AccessToken = 1,
        RefreshToken = 2
    }
}
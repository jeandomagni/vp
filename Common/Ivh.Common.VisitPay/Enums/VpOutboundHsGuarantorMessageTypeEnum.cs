﻿namespace Ivh.Common.VisitPay.Enums
{
    public enum VpOutboundHsGuarantorMessageTypeEnum
    {
        VpGuarantorMatchCreated = 1,
        VpGuarantorCancelation = 2,
        VppPayment = 3,
        VppVoid = 4,
        VppRefund = 5,
        VppAchReturn = 6,
        VppInterestReassessment = 7,
        VpGuarantorUnmatch = 8,
        HsGuarantorReactivation = 9
    }
}
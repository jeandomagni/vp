﻿namespace Ivh.Common.VisitPay.Enums
{
    using Attributes;

    public static class BalanceTransferStatusEnumCategory
    {
        public const string Unknown = "Unknown";
        public const string Active = "Active";
    }

    public enum BalanceTransferStatusEnum
    {
        [EnumCategory(BalanceTransferStatusEnumCategory.Unknown)]
        Eligible = 1,
        [EnumCategory(BalanceTransferStatusEnumCategory.Active)]
        ActiveBalanceTransfer = 2,
        [EnumCategory(BalanceTransferStatusEnumCategory.Unknown)]
        Ineligible = 3
    }
}

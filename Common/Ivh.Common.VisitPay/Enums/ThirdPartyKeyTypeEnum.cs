﻿namespace Ivh.Common.VisitPay.Enums
{
    public enum ThirdPartyKeyTypeEnum
    {
        CardReaderDeviceKey = 1,
        ApiKey = 2,
        UserAccount = 3
    }
}

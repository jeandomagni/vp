﻿namespace Ivh.Common.VisitPay.Enums
{
    public enum PaymentMatchUnitResponseStatusEnum
    {
        Unknown = 0,
        Success = 1,
        NotFound = 2,
        Ambiguous = 3,
        PaymentApiNotEnabled = 4
    }
}

﻿namespace Ivh.Common.VisitPay.Enums
{
    public enum ClientSupportRequestMessageTypeEnum
    {
        FromClient = 1,
        ToClient = 2,
        InternalNote = 3,
        OpenMessage = 4,
        CloseMessage = 5
    }
}
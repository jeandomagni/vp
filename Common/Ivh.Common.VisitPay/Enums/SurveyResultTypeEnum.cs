﻿namespace Ivh.Common.VisitPay.Enums
{
    public enum SurveyResultTypeEnum
    {
        Ignored = 1,
        Submitted = 2
    }
}

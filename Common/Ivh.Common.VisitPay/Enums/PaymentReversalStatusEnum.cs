﻿namespace Ivh.Common.VisitPay.Enums
{
    public enum PaymentReversalStatusEnum
    {
        NotReversable = 1,
        Voidable = 2,
        Refundable = 3
    }
}

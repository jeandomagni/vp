﻿namespace Ivh.Common.VisitPay.Enums
{
    public enum MatchOptionEnum
    {
        //og matching
        AuthenticationMatch = -1,
        SsnDobLname = 201,
        DobLnameNoSsn = 202,
        LnameSsnNoDob = 203,
        LnameMinorNoSsnNoDob = 204,

        //matching B
        Add1CityDobFnameLnameState = 1,
        Add1CityFnameLnameState = 2, //low
        CityLnameSsnState = 3,
        DobLnameSsk = 4,
        DobLnameSskZip = 5,
        DobLnameSsn = 6,
        DobLnameSsn4SskZip = 7,
        DobLnameSsnZip = 8,
        LnameSsk = 9,
        LnamePatientDobSskZip = 10,
    }
}
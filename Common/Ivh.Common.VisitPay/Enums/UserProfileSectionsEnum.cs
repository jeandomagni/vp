﻿
namespace Ivh.Common.VisitPay.Enums
{
    using System.ComponentModel;

    public static class UserProfileEditSectionsEnumDescription
    {
        public const string Name = "Edit Name";
        public const string Username = "Edit Username";
        public const string Email = "Edit Email";
        public const string Address = "Edit Address";
        public const string PhoneNumber = "Edit Phone";
    }

    public enum UserProfileEditSectionsEnum
    {
        [Description(UserProfileEditSectionsEnumDescription.Name)]
        Name = 1,

        [Description(UserProfileEditSectionsEnumDescription.Username)]
        Username = 2,

        [Description(UserProfileEditSectionsEnumDescription.Email)]
        Email = 3,

        [Description(UserProfileEditSectionsEnumDescription.Address)]
        Address = 4,

        [Description(UserProfileEditSectionsEnumDescription.PhoneNumber)]
        PhoneNumber = 5
    }
}

﻿namespace Ivh.Common.VisitPay.Enums
{
    public enum StatementNotificationTierEnum
    {
        Good = 1,
        PastDue = 2,
        FinalPastDue = 3,
        Uncollectable = 4
    }
}

﻿namespace Ivh.Common.VisitPay.Enums
{
    public enum EnrollmentAccountEligibilityEnum
    {
        NotEligible, // N
        Eligible, //A or S
        Suspended // S
    }
}
namespace Ivh.Common.VisitPay.Enums
{
    public enum VpStatementVersionEnum
    {
        Unknown = 0,
        Vp2 = 1,
        Vp3V1 = 2
    }
}
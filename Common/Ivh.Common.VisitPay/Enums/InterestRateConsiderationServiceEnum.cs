﻿namespace Ivh.Common.VisitPay.Enums
{
    public enum InterestRateConsiderationServiceEnum
    {
        InterestRateConsiderationFinancePlansBalanceService,
        InterestRateConsiderationNoneService
    }
}
﻿namespace Ivh.Common.VisitPay.Enums
{
    public enum DiscountModelTypeEnum
    {
        Unknown,
        Balance,
        Insurance
    }
}
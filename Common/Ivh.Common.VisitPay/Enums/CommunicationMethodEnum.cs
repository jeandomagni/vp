﻿namespace Ivh.Common.VisitPay.Enums
{
    using System.ComponentModel;

    public static class CommunicationMethodEnumDescription 
    {
        public const string Email = "Email";
        public const string Sms = "Sms";
        public const string Mail = "Mail";
        public const string None = "Not set";
    }
    public enum CommunicationMethodEnum
    {
        [Description(CommunicationMethodEnumDescription.Email)]
        Email = 1,
        [Description(CommunicationMethodEnumDescription.Sms)]
        Sms = 2,
        [Description(CommunicationMethodEnumDescription.Mail)]
        Mail = 3,
    }
}
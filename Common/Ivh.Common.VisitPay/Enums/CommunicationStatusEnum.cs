﻿namespace Ivh.Common.VisitPay.Enums
{
    public enum CommunicationStatusEnum
    {
        Created = 1,
        Hold = 2,
        Sent = 3,
        Failed = 4
    }
}
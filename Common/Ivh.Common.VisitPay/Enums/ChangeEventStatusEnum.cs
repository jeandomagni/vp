﻿namespace Ivh.Common.VisitPay.Enums
{
    public enum ChangeEventStatusEnum
    {
        Unprocessed = 0,
        Queued = 1,
        Processed = 2,
        GuarantorMismatch = 3,
        VisitMismatch = 4,
        SentToVpApp = 5
    }
}
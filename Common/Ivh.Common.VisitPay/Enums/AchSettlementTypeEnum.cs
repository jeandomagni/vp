﻿namespace Ivh.Common.VisitPay.Enums
{
    using Ivh.Common.VisitPay.Attributes;

    public static class AchSettlementTypeEnumCategory
    {
        public const string File = "File";
        public const string Optimistic = "Optimistic";
    }

    public enum AchSettlementTypeEnum
    {
        None = -1,
        [EnumCategory(AchSettlementTypeEnumCategory.File)]
        FileSettlement = 1,
        [EnumCategory(AchSettlementTypeEnumCategory.File)]
        FileReturn = 2,
        [EnumCategory(AchSettlementTypeEnumCategory.Optimistic)]
        OptimisticSettlement = 3
    }
}
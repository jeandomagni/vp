namespace Ivh.Common.VisitPay.Enums
{
    /// <summary>
    /// Copy from the HQY SOAP generated structures.  Assuming the structure is going to be the same for OAuth balance service.
    /// </summary>
    public enum AccountTypeFlagApiModelEnum
    {
        HSA = 1,
        HRA = 2,
        FSA = 4,
        DCRA = 8,
    }
}
﻿namespace Ivh.Common.VisitPay.Enums
{
    public enum UnmatchActionTypeEnum
    {
        GuarantorUnmatch = 1,
        GuarantorValidationMatch = 2
    }
}
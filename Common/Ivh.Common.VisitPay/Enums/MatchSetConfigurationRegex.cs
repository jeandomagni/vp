﻿namespace Ivh.Common.VisitPay.Enums
{
    public enum MatchSetConfigurationRegexEnum
    {
        /// <summary>
        /// Remove facility prefix from SourceSystemKeys. eg. "Prefix-SourceSystemKey" => "SourceSystemKey"
        /// </summary>
        RemoveMinistryPrefix = 1
    }
}

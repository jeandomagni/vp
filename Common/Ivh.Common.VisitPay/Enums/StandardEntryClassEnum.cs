﻿namespace Ivh.Common.VisitPay.Enums
{
    public enum StandardEntryClassEnum
    {
        PPD,
        CCD,
        CTX,
        TEL,
        WEB
    }
}
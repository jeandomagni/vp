﻿namespace Ivh.Common.VisitPay.Enums
{
    public enum ScoringThirdPartyDatasetEnum
    {
        ScoringAndSegmentation = 1,
        Scoring = 2,
        PresumptiveCharity = 3,
    }
}
﻿namespace Ivh.Common.VisitPay.Enums
{
    public enum AddressStatusEnum
    {
        Unverified = 1,
        Verified = 2,
        Rejected = 3,
        Returned = 4
    }
}
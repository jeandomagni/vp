﻿namespace Ivh.Common.VisitPay.Enums
{
    using System.ComponentModel;
    using Attributes;

    public static class PaymentAllocationStatusEnumCategory
    {
        public const string Merge = "Merge";
        public const string Create = "Create";
    }

    public static class PaymentAllocationStatusEnumDescription
    {
        public const string PendingHsAcknowledgement = "Pending HS Acknowledgement";
        public const string HsConfirmed = "HS Confirmed";
        public const string DiscrepancyResolved = "Discrepancy - Resolved";
        public const string DiscrepancyUnallocatedFunds = "Discrepancy - Unallocated Funds";
    }

    public enum PaymentAllocationStatusEnum
    {
        [Description(PaymentAllocationStatusEnumDescription.PendingHsAcknowledgement)]
        [EnumCategory(PaymentAllocationStatusEnumCategory.Merge)]
        PendingHsAcknowledgement = 1,

        [Description(PaymentAllocationStatusEnumDescription.HsConfirmed)]
        [EnumCategory(PaymentAllocationStatusEnumCategory.Create)]
        HsConfirmed = 2,

        [Description(PaymentAllocationStatusEnumDescription.DiscrepancyResolved)]
        [EnumCategory(PaymentAllocationStatusEnumCategory.Create)]
        DiscrepancyResolved = 3,

        [Description(PaymentAllocationStatusEnumDescription.DiscrepancyUnallocatedFunds)]
        [EnumCategory(PaymentAllocationStatusEnumCategory.Create)]
        DiscrepancyUnallocatedFunds = 4
    }
}
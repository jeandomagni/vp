﻿namespace Ivh.Common.VisitPay.Enums
{
    public enum DataCorrectionTypeEnum
    {
        Unknown,
        InsertAdjustingTransaction,
        AchDownload,
        RetroScoring,
        MigrateHsEobToAppEob,
        SendOptimisticAchSettlements
    }
}

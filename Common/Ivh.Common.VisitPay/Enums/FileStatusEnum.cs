﻿namespace Ivh.Common.VisitPay.Enums
{
    public enum FileStatusEnum
    {
        InProgress = 1,
        Completed = 2,
        Failed = 3,
        NotLoaded = 4
    }
}
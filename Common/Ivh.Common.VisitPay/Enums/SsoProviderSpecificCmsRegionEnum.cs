﻿namespace Ivh.Common.VisitPay.Enums
{
    public enum SsoProviderSpecificCmsRegionEnum
    {
        LandingContentSso,
        RegistrationWelcomeInstruction,
    }
}
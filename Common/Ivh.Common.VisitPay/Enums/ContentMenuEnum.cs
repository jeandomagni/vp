﻿namespace Ivh.Common.VisitPay.Enums
{
    public enum ContentMenuEnum
    {
        LegalAgreementsPatient = 1,
        LegalAgreementsMobile = 2,
        LegalAgreementsClient = 3,
        LegalAgreementsGuestPay = 4,
        HelpCenter = 7,
        HelpCenterMobile = 8
    }
}
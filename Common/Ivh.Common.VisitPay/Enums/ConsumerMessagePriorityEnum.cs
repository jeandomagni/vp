﻿namespace Ivh.Common.VisitPay.Enums
{
    public enum ConsumerMessagePriorityEnum
    {
        Priority10,
        Priority20,
        Priority30,
        Priority40,
        All
    }
}

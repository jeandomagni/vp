﻿namespace Ivh.Common.VisitPay.Enums
{
    public enum JournalEventCategoryEnum
    {
        AuditAction,
        ClientUserAction,
        SystemAction,
        VpAdminAction,
        VpGuarantorUserAction,                   
    }
}

namespace Ivh.Common.VisitPay.Enums
{
    public enum SmsResponseActionEnum
    {
        Unknown = 0,
        Stop = 1,
        Info = 2,
    }
}
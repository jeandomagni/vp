﻿namespace Ivh.Common.VisitPay.Enums
{
    using Attributes;

    public static class GuarantorConsolidationStatusEnumCategory
    {
        public const string Mask = "ObfuscatePatient";
    }

    public enum GuarantorConsolidationStatusEnum
    {
        NotConsolidated = 1,
        
        [EnumCategory(GuarantorConsolidationStatusEnumCategory.Mask)]
        Managed = 2,

        Managing = 3,
        
        [EnumCategory(GuarantorConsolidationStatusEnumCategory.Mask)]
        PendingManaged = 4,

        PendingManaging = 5,
    }
}
﻿namespace Ivh.Common.VisitPay.Enums
{
    using System.ComponentModel;
    using Attributes;

    public static class PaymentMethodTypeEnumCategory
    {
        public const string Card = "Card";
        public const string Ach = "Ach";
        public const string LockBox = "LockBox";
    }

    public static class PaymentMethodTypeEnumDescription
    {
        public const string Visa = "Visa";
        public const string MasterCard = "MasterCard";
        public const string AmericanExpress = "American Express";
        public const string Discover = "Discover";
        public const string Checking = "Checking";
        public const string Savings = "Savings";
        public const string MailedIn = "Mailed In";
    }

    public enum PaymentMethodTypeEnum
    {
        [EnumCategory(PaymentMethodTypeEnumCategory.Card), Description(PaymentMethodTypeEnumDescription.Visa)]
        Visa = 1,

        [EnumCategory(PaymentMethodTypeEnumCategory.Card), Description(PaymentMethodTypeEnumDescription.MasterCard)]
        Mastercard = 2,

        [EnumCategory(PaymentMethodTypeEnumCategory.Card), Description(PaymentMethodTypeEnumDescription.AmericanExpress)]
        AmericanExpress = 3,

        [EnumCategory(PaymentMethodTypeEnumCategory.Card), Description(PaymentMethodTypeEnumDescription.Discover)]
        Discover = 4,

        [EnumCategory(PaymentMethodTypeEnumCategory.Ach), Description(PaymentMethodTypeEnumDescription.Checking)]
        AchChecking = 5,

        [EnumCategory(PaymentMethodTypeEnumCategory.Ach), Description(PaymentMethodTypeEnumDescription.Savings)]
        AchSavings = 6,

        [EnumCategory(PaymentMethodTypeEnumCategory.LockBox), Description(PaymentMethodTypeEnumDescription.MailedIn)]
        LockBoxDeposit = 7,

        Unknown = -1
    }
}

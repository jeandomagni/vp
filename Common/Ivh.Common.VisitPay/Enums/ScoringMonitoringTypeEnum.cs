﻿namespace Ivh.Common.VisitPay.Enums
{
    public enum ScoringMonitoringTypeEnum
    {
        Daily = 1,
        Weekly = 2
    }
}
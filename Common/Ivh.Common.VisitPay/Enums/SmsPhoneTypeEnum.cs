﻿namespace Ivh.Common.VisitPay.Enums
{
    public enum SmsPhoneTypeEnum
    {
        Primary = 0,
        Secondary = 1
    }
}
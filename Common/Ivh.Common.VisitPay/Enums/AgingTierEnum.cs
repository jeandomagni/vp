﻿namespace Ivh.Common.VisitPay.Enums
{
    public enum AgingTierEnum
    {
        NotStatemented = 0,
        Good = 1,
        PastDue = 2,
        FinalPastDue = 3,
        MaxAge = 4
    }
}
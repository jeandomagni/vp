﻿namespace Ivh.Common.VisitPay.Enums
{
    public enum AuditEventTypeFinancePlanEnum
    {
        Default,
        Origination,
        BalanceChange,
        Reconfiguration,
        InterestAssessed,
        InterestReassessed,
        PaymentHoliday,
        ReAging,
        AgingChangedByClient
    }
}

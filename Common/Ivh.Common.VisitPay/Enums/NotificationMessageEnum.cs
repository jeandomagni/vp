﻿namespace Ivh.Common.VisitPay.Enums
{
    public enum NotificationMessageEnum
    {
        IsNotConsolidated = 3,
        IsEligibleForSso = 6
    }
}

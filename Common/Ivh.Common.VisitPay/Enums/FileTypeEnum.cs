﻿namespace Ivh.Common.VisitPay.Enums
{
    public enum FileTypeEnum
    {
        Eob835Remit = 35,
        JPMorganLockboxVendorFile = 1000
    }
}
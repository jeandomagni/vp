﻿namespace Ivh.Common.VisitPay.Enums
{
    using Attributes;

    public static class PaymentStatusEnumCategory
    {
        public const string Active = "Active";
        public const string ProcessRecurring = "ProcessRecurring";
        public const string Pending = "Pending";
        public const string Search = "Search";
        public const string Paid = "Paid";
        public const string ConsolidationCancellation = "ConsolidationCancellation";
        public const string Closed = "Closed";
        public const string Cancelled = "Cancelled";
    }

    public enum PaymentStatusEnum
    {
        [EnumCategory(PaymentStatusEnumCategory.Active, PaymentStatusEnumCategory.ProcessRecurring)]
        ActivePending = 1,
        [EnumCategory(PaymentStatusEnumCategory.Active, PaymentStatusEnumCategory.ProcessRecurring)]
        ActiveGatewayError = 2,
        [EnumCategory(PaymentStatusEnumCategory.Active, PaymentStatusEnumCategory.Pending, PaymentStatusEnumCategory.Search, PaymentStatusEnumCategory.Paid)]
        ActivePendingACHApproval = 3,
        [EnumCategory(PaymentStatusEnumCategory.Active, PaymentStatusEnumCategory.Pending, PaymentStatusEnumCategory.Search)]
        ActivePendingLinkFailure = 4,
        [EnumCategory(PaymentStatusEnumCategory.Active, PaymentStatusEnumCategory.Pending, PaymentStatusEnumCategory.ConsolidationCancellation, PaymentStatusEnumCategory.Search)]
        ActivePendingGuarantorAction = 5,
        [EnumCategory(PaymentStatusEnumCategory.Closed, PaymentStatusEnumCategory.Search, PaymentStatusEnumCategory.Paid)]
        ClosedPaid = 6,
        [EnumCategory(PaymentStatusEnumCategory.Closed, PaymentStatusEnumCategory.Search)]
        ClosedFailed = 7,
        [EnumCategory(PaymentStatusEnumCategory.Closed, PaymentStatusEnumCategory.Search, PaymentStatusEnumCategory.Cancelled)]
        ClosedCancelled = 8,
        [EnumCategory(PaymentStatusEnumCategory.Active, PaymentStatusEnumCategory.Pending)]
        PaymentPendingGuarantorResponse = 9,
    }
}
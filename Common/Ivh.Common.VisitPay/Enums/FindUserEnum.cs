﻿namespace Ivh.Common.VisitPay.Enums
{
    public enum FindUserEnum
    {
        Found = 0,
        NotFound = 1,
        MoreThanOneFound = 2,
        AccountLocked = 3,
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ivh.Common.VisitPay.Enums
{
    public enum PaymentRefundableTypeEnum
    {
        Default = 1,
        ReturnedCheck = 2
    }
}

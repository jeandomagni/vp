﻿namespace Ivh.Common.VisitPay.Enums
{
    public enum LoadStatusEnum
    {
        Unknown = -1,
        InProgress = 1,
        Completed = 2,
        Failed = 3,
        Canceled = 4
    }
}
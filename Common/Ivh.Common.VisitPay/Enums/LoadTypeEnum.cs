﻿namespace Ivh.Common.VisitPay.Enums
{
    public enum LoadTypeEnum
    {
        QaToolsChangeSets = -100,
        Inbound = 1,
        Daily835RemitInboundProcess = 9,
        Realtime = 11,
        PaymentVendorFileProcess = 12,
    }
}
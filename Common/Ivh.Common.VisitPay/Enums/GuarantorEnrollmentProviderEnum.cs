﻿namespace Ivh.Common.VisitPay.Enums
{
    public enum GuarantorEnrollmentProviderEnum
    {
        DefaultGuarantorEnrollmentProvider,
        IntermountainGuarantorEnrollmentProvider,
        InternalGuarantorEnrollmentProvider
    }
}
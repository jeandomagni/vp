﻿namespace Ivh.Common.VisitPay.Enums
{
    using Attributes;

    public static class HsGuarantorUnmatchReasonEnumCategory
    {
        public const string Client = "Client";
        public const string System = "System";
    }

    public enum HsGuarantorUnmatchReasonEnum
    {
        NotSet = 0,
        [EnumCategory(HsGuarantorUnmatchReasonEnumCategory.Client)]
        ClientUnmatchedMatchCriteriaChanged = 1,
        [EnumCategory(HsGuarantorUnmatchReasonEnumCategory.Client)]
        ClientUnmatchedGuarantorManual = 2,
        [EnumCategory(HsGuarantorUnmatchReasonEnumCategory.System)]
        SystemUnmatchedGuarantorTypeChange = 3,
        [EnumCategory(HsGuarantorUnmatchReasonEnumCategory.Client)]
        ClientUnmatchedVisitManual = 4,
        [EnumCategory(HsGuarantorUnmatchReasonEnumCategory.System)]
        SystemUnmatchedHSGIDUpdated = 5,
        [EnumCategory(HsGuarantorUnmatchReasonEnumCategory.Client)]
        ClientGuarantorMatchingCriteriaValidated = 6,
        [EnumCategory(HsGuarantorUnmatchReasonEnumCategory.Client)]
        ClientUnmatchedVisitRedacted = 7,
    }
}

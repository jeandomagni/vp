﻿namespace Ivh.Common.VisitPay.Enums
{
    public enum PaymentProcessorResponseStatusEnum
    {
        Unknown = 0,
        Approved = 1,
        Accepted = 2,
        Decline = 3,
        BadData = 4,
        Error = 5,
        CallToProcessorFailed = 6,
        LinkFailure = 7,
    }
}
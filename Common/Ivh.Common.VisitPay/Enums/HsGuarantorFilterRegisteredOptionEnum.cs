﻿namespace Ivh.Common.VisitPay.Enums
{
    public enum HsGuarantorFilterRegisteredOptionEnum
    {
        All,
        Registered,
        Unregistered
    }
}

namespace Ivh.Common.VisitPay.Enums
{
    /// <summary>
    /// Copy from the HQY SOAP generated structures.  Assuming the structure is going to be the same for OAuth balance service.
    /// </summary>
    public enum AccountStatusApiModelEnum
    {
        Open = 1,
        Pending = 2,
        Active = 4,
    }
}
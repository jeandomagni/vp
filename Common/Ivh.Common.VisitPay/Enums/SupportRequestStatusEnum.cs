﻿namespace Ivh.Common.VisitPay.Enums
{
    using System.ComponentModel;

    public static class SupportRequestStatusEnumDescription
    {
        public const string Open = "Open";
        public const string Closed = "Closed";
    }

    public enum SupportRequestStatusEnum
    {
        [Description(SupportRequestStatusEnumDescription.Open)]
        Open = 1,
        [Description(SupportRequestStatusEnumDescription.Closed)]
        Closed = 2
    }
}

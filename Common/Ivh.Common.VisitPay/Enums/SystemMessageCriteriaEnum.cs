﻿namespace Ivh.Common.VisitPay.Enums
{
    public enum SystemMessageCriteriaEnum
    {
        IsEligibleForFinancePlan = 1,
        IsNotConsolidated = 2,
        HasBalanceNoPaymentMethods = 3,
        IsEligibleForSso = 4,
        IsSmsEnabled = 5,
        CanAddVisitToExistingPlan = 7,
        HasNewPaymentSummary = 8,
        HasNewEob = 9,
        HasExistingBadDebt = 10,
        OnPrePaymentPlan = 11,
        PaymentSummary = 12
    }
}
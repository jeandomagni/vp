﻿namespace Ivh.Common.VisitPay.Enums
{
    public enum NotificationFrequencyEnum
    {
        None = 0,
        OncePerDay = 1,
        OncePerWeek = 2,
        OncePerMonth = 3,
        OncePerHour = 4
    }
}
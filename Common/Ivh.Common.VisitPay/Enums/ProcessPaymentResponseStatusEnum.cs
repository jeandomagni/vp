﻿namespace Ivh.Common.VisitPay.Enums
{
    public enum ProcessPaymentResponseStatusEnum
    {
        Unknown = 0,
        Success = 1,
        PaymentFailed = 2,
        InvalidSubmission = 3,
        PaymentMethodExpired = 4,
        AmountGreaterThanBalance = 5,
        PaymentApiNotEnabled = 6,
        ExpirationDateBadFormat = 7,
        MissingGuarantor = 8,
        FatalErrorMaybeChargedCallSupport = 9,
        ActivePendingLinkFailureMaybeChargedCallSupport = 10,
        SystemNotRespondingActiveGatewayErrorTryAgain = 11,
        SystemNotRespondingTryAgainCallSupport = 12,
    }
}

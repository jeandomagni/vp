﻿namespace Ivh.Common.VisitPay.Enums
{
    public enum LogMessageTypeEnum
    {
        Debug = 1,
        Info = 2,
        Warn = 3,
        Fatal = 4
    }
}

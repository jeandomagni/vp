﻿namespace Ivh.Common.VisitPay.Enums
{
    using Attributes;

    public static class VpTransactionTypeEnumCategory
    {
        public const string Hospital = "Hospital";
        public const string Adjustment = "Adjustment";
        public const string PaymentSummary = "PaymentSummary";
        public const string Insurance = "Insurance";
        public const string Interest = "Interest";
        public const string VisitPay = "VisitPay";
        public const string Principal = "Principal";
        public const string Discount = "Discount";
        public const string PublishInterestAssessment = "PublishInterestAssessment";
        public const string ScoringPatientCash = "PatientCash";
    }
    
    public enum VpTransactionTypeEnum
    {
        [EnumCategory(VpTransactionTypeEnumCategory.Hospital, VpTransactionTypeEnumCategory.Adjustment)]
        HsCharge = 1,
        [EnumCategory(VpTransactionTypeEnumCategory.Hospital, VpTransactionTypeEnumCategory.Adjustment)]
        HsNonPresumptiveCharity = 2,
        [EnumCategory(VpTransactionTypeEnumCategory.Hospital, VpTransactionTypeEnumCategory.Adjustment)]
        HsCharity = 15,
        [EnumCategory(VpTransactionTypeEnumCategory.Hospital)]
        HsInterestAssessed = 3,
        [EnumCategory(VpTransactionTypeEnumCategory.Hospital, VpTransactionTypeEnumCategory.PaymentSummary, VpTransactionTypeEnumCategory.ScoringPatientCash)]
        HsPatientCash = 4,
        [EnumCategory(VpTransactionTypeEnumCategory.Hospital, VpTransactionTypeEnumCategory.Adjustment)]
        HsPatientNonCash = 5,
        [EnumCategory(VpTransactionTypeEnumCategory.Hospital, VpTransactionTypeEnumCategory.Adjustment, VpTransactionTypeEnumCategory.Insurance)]
        HsPayorCash = 6,
        [EnumCategory(VpTransactionTypeEnumCategory.Hospital, VpTransactionTypeEnumCategory.Adjustment, VpTransactionTypeEnumCategory.Insurance)]
        HsPayorNonCash = 7,
        [EnumCategory(VpTransactionTypeEnumCategory.Hospital, VpTransactionTypeEnumCategory.Adjustment)]
        OtherTransaction = 8,
        [EnumCategory(VpTransactionTypeEnumCategory.Hospital, VpTransactionTypeEnumCategory.Interest)]
        HsInterestPayment = 9,
        [EnumCategory(VpTransactionTypeEnumCategory.Hospital, VpTransactionTypeEnumCategory.Adjustment)]
        HsWriteoffTransaction = 11,

        [EnumCategory(VpTransactionTypeEnumCategory.VisitPay, VpTransactionTypeEnumCategory.Principal, VpTransactionTypeEnumCategory.PaymentSummary, VpTransactionTypeEnumCategory.ScoringPatientCash)]
        VppPrincipalPayment = 100,
        [EnumCategory(VpTransactionTypeEnumCategory.VisitPay, VpTransactionTypeEnumCategory.Principal, VpTransactionTypeEnumCategory.PaymentSummary, VpTransactionTypeEnumCategory.ScoringPatientCash)]
        VppPrincipalPaymentHsReversal = 101,
        [EnumCategory(VpTransactionTypeEnumCategory.VisitPay, VpTransactionTypeEnumCategory.Principal, VpTransactionTypeEnumCategory.PaymentSummary)]
        VppPrincipalPaymentHsReallocation = 102,
        [EnumCategory(VpTransactionTypeEnumCategory.VisitPay, VpTransactionTypeEnumCategory.Principal, VpTransactionTypeEnumCategory.PaymentSummary)]
        VppPrincipalPaymentReassessmentReversal = 103,
        [EnumCategory(VpTransactionTypeEnumCategory.VisitPay, VpTransactionTypeEnumCategory.Principal, VpTransactionTypeEnumCategory.PaymentSummary)]
        VppPrincipalPaymentReassessmentReallocation = 104,

        [EnumCategory(VpTransactionTypeEnumCategory.VisitPay, VpTransactionTypeEnumCategory.Interest, VpTransactionTypeEnumCategory.ScoringPatientCash)]
        VppInterestPayment = 200,
        [EnumCategory(VpTransactionTypeEnumCategory.VisitPay, VpTransactionTypeEnumCategory.Interest, VpTransactionTypeEnumCategory.ScoringPatientCash)]
        VppInterestPaymentHsReversal = 201,
        [EnumCategory(VpTransactionTypeEnumCategory.VisitPay, VpTransactionTypeEnumCategory.Interest)]
        VppInterestPaymentHsReallocation = 202,
        [EnumCategory(VpTransactionTypeEnumCategory.VisitPay, VpTransactionTypeEnumCategory.Interest)]
        VppInterestPaymentReassessmentReversal = 203,
        [EnumCategory(VpTransactionTypeEnumCategory.VisitPay, VpTransactionTypeEnumCategory.Interest)]
        VppInterestPaymentReassessmentReallocation = 204,

        [EnumCategory(VpTransactionTypeEnumCategory.VisitPay, VpTransactionTypeEnumCategory.Discount)]
        VppDiscount = 300,
        [EnumCategory(VpTransactionTypeEnumCategory.VisitPay, VpTransactionTypeEnumCategory.Discount)]
        VppDiscountHsReversal = 301,
        [EnumCategory(VpTransactionTypeEnumCategory.VisitPay, VpTransactionTypeEnumCategory.Discount)]
        VppDiscountHsReallocation = 302,

        [EnumCategory(VpTransactionTypeEnumCategory.VisitPay, VpTransactionTypeEnumCategory.Interest, VpTransactionTypeEnumCategory.PublishInterestAssessment)]
        VppInterestCharge = 14,
        [EnumCategory(VpTransactionTypeEnumCategory.VisitPay, VpTransactionTypeEnumCategory.Interest, VpTransactionTypeEnumCategory.PublishInterestAssessment)]
        VppInterestAssessmentReallocation = 12,
        [EnumCategory(VpTransactionTypeEnumCategory.VisitPay, VpTransactionTypeEnumCategory.Interest, VpTransactionTypeEnumCategory.PublishInterestAssessment)]
        VppInterestAssessmentReversal = 13,
        [EnumCategory(VpTransactionTypeEnumCategory.VisitPay, VpTransactionTypeEnumCategory.Interest, VpTransactionTypeEnumCategory.PublishInterestAssessment)]
        VppInterestWriteoff = 29,
    }
}
﻿namespace Ivh.Common.VisitPay.Enums
{
    public enum VpStatementTypeEnum
    {
        Standard = 1,
        Archived = 2,
        MigratedManagedUser = 3,
    }
}

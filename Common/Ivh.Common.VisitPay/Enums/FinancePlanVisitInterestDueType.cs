﻿namespace Ivh.Common.VisitPay.Enums
{
    using System.ComponentModel;
    using Attributes;

    public static class FinancePlanVisitInterestDueTypeEnumCategory
    {
        public const string InterestAssessment = "InterestAssessment";
        public const string InterestPayment = "InterestPayment";
        public const string Reallocation = "Reallocation";
    }
    public static class FinancePlanVisitInterestDueTypeDescription
    {
        public const string InterestAssessment = "Interest Assessment";
        public const string InterestPayment = "Interest Payment";
        public const string InterestReallocation = "Interest Reallocation";
        public const string PrincipalReallocation = "Principal Reallocation";
    }



    public enum FinancePlanVisitInterestDueTypeEnum
    {
        [Description(FinancePlanVisitInterestDueTypeDescription.InterestAssessment)]
        [EnumCategory(FinancePlanVisitInterestDueTypeEnumCategory.InterestAssessment)]
        InterestAssessment = 1,

        [Description(FinancePlanVisitInterestDueTypeDescription.InterestPayment)]
        [EnumCategory(FinancePlanVisitInterestDueTypeEnumCategory.InterestPayment)]
        InterestPayment = 2,

        [Description(FinancePlanVisitInterestDueTypeDescription.InterestReallocation)]
        [EnumCategory(FinancePlanVisitInterestDueTypeEnumCategory.Reallocation, FinancePlanVisitInterestDueTypeEnumCategory.InterestPayment)]
        InterestReallocation = 3,

        [Description(FinancePlanVisitInterestDueTypeDescription.PrincipalReallocation)]
        [EnumCategory(FinancePlanVisitInterestDueTypeEnumCategory.Reallocation)]
        PrincipalReallocation = 4
    }
}
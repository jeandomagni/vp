namespace Ivh.Common.VisitPay.Enums
{
    public enum SsoResponseErrorEnum
    {
        Unknown = 0,
        FailedToVerifyTimeStamp = 1,
        FailedToDecrypt = 2,
        MissingFields = 3,
    }
}
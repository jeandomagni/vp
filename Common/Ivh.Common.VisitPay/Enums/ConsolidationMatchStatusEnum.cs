﻿namespace Ivh.Common.VisitPay.Enums
{
    public enum ConsolidationMatchStatusEnum
    {
        NoMatchFound = 0,
        NotEligible = 1,
        Matched = 2
    }
}
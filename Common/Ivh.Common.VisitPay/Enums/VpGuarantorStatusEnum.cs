﻿namespace Ivh.Common.VisitPay.Enums
{
    public enum VpGuarantorStatusEnum
    {
        NotSet = -1,
        Active = 1,
        Closed = 2,
    }
}
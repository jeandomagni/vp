﻿namespace Ivh.Common.VisitPay.Enums
{
    using Attributes;

    public static class AuthenticationMatchFieldTypeEnumCategory
    {
        public const string DisplayOnly = nameof(DisplayOnly);
    }

    public enum AuthenticationMatchFieldTypeEnum
    {
        InputText = 1,
        Statement = 2,
        StatementIdentifierLookup = 3,
        [EnumCategory(AuthenticationMatchFieldTypeEnumCategory.DisplayOnly)]
        StatementReferenceImageSelector = 4,
    }
}
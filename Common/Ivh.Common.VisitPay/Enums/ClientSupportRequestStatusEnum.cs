﻿namespace Ivh.Common.VisitPay.Enums
{
    using System.ComponentModel;

    public static class ClientSupportRequestStatusEnumCategory
    {
        public const string Open = "Open";
        public const string Closed = "Closed";
    }

    public enum ClientSupportRequestStatusEnum
    {
        [Description(ClientSupportRequestStatusEnumCategory.Open)]
        Open = 1,
        [Description(ClientSupportRequestStatusEnumCategory.Closed)]
        Closed = 2
    }
}
﻿namespace Ivh.Common.VisitPay.Enums
{
    public enum SurveyGroupEnum
    {
        FinancePlanCreation = 1,
        ManualPayment = 2,
        PayInFull = 3
    }
}
﻿namespace Ivh.Common.VisitPay.Enums
{
    using Attributes;

    public static class IvhEnvironmentTypeEnumCategory
    {
        public const string Lower = "Lower";
    }

    public enum IvhEnvironmentTypeEnum
    {
        Unknown,
        [EnumCategory(IvhEnvironmentTypeEnumCategory.Lower)]
        Dev,
        [EnumCategory(IvhEnvironmentTypeEnumCategory.Lower)]
        Qa,
        Phi,
        Pci
    }
}
﻿namespace Ivh.Common.VisitPay.Enums
{
    public enum GuarantorMatchResultEnum
    {
        NoMatchFound = 0,
        Matched = 1,
        AlreadyRegistered = 2,
        Ambiguous = 3,
    }
}
﻿namespace Ivh.Common.VisitPay.Enums
{
    public enum PaymentAllocationModelTypeEnum
    {
        Unknown,
        ProRata,
        BillingApplication
    }
}

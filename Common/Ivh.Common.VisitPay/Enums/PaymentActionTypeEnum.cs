﻿namespace Ivh.Common.VisitPay.Enums
{
    public enum PaymentActionTypeEnum
    {
        Reschedule = 1,
        Cancel = 2
    }
}
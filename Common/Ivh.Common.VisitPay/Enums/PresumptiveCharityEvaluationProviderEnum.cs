﻿namespace Ivh.Common.VisitPay.Enums
{
    public enum PresumptiveCharityEvaluationProviderEnum
    {
        DefaultPresumptiveCharityEvaluationProvider,
        EvaluateForPcFlagPresumptiveCharityEvaluationProvider,
        SelfPayClassPresumptiveCharityEvaluationProvider
    }
}
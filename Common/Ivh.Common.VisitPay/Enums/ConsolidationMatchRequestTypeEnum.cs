﻿namespace Ivh.Common.VisitPay.Enums
{
    public enum ConsolidationMatchRequestTypeEnum
    {
        Unknown = 0,
        RequestToBeManaged = 1,
        RequestToBeManaging = 2
    }
}
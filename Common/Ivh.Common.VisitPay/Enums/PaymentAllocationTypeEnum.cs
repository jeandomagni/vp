﻿namespace Ivh.Common.VisitPay.Enums
{
    public enum PaymentAllocationTypeEnum
    {
        Unknown = 0,
        VisitPrincipal = 1,
        VisitInterest = 2,
        VisitDiscount= 3
    }
}
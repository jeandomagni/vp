﻿namespace Ivh.Common.VisitPay.Enums
{
    public enum DeviceTypeEnum
    {
        Unknown = 0,
        Web = 1,
        Mobile = 2,
        Tablet = 3
    }
}

﻿namespace Ivh.Common.VisitPay.Enums
{
    public enum AchFileTypeEnum
    {
        Settlement = 1,
        Return = 2
    }
}

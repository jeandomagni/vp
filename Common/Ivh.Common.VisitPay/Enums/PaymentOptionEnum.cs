﻿namespace Ivh.Common.VisitPay.Enums
{
    using Attributes;

    public static class PaymentOptionEnumCategory
    {
        public const string Household = "Household";
        public const string CurrentNonFinancedBalance = "CurrentNonFinancedBalance";
    }

    public enum PaymentOptionEnum
    {
        Unknown = 0,
        Resubmit = 1,
        FinancePlan = 4,
        SpecificAmount = 5,
        SpecificVisits = 6,
        SpecificFinancePlans = 7,
        AccountBalance = 8,
        [EnumCategory(PaymentOptionEnumCategory.CurrentNonFinancedBalance)]
        CurrentNonFinancedBalance = 9,
        [EnumCategory(PaymentOptionEnumCategory.Household, PaymentOptionEnumCategory.CurrentNonFinancedBalance)]
        HouseholdCurrentNonFinancedBalance = 10
    }
}
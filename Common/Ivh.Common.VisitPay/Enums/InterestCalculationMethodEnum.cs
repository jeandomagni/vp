﻿namespace Ivh.Common.VisitPay.Enums
{
    public enum InterestCalculationMethodEnum
    {
        Daily = 1,
        Monthly = 2
    }
}
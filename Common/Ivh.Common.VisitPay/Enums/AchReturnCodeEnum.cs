﻿namespace Ivh.Common.VisitPay.Enums
{
    using Attributes;

    public static class AchReturnCodeEnumGroup
    {
        public const string ReturnCode = "ReturnCode";
        public const string NocCode = "NocCode";
    }

    public enum AchReturnCodeEnum
    {
        None,
        //RETURN CODES
        [EnumCategory(AchReturnCodeEnumGroup.ReturnCode)]
        R00_Unknown,
        [EnumCategory(AchReturnCodeEnumGroup.ReturnCode)]
        R01_InsufficientFunds,
        [EnumCategory(AchReturnCodeEnumGroup.ReturnCode)]
        R02_AccountClosed,
        [EnumCategory(AchReturnCodeEnumGroup.ReturnCode)]
        R03_NoAccount,
        [EnumCategory(AchReturnCodeEnumGroup.ReturnCode)]
        R04_InvalidAccountNumber,
        [EnumCategory(AchReturnCodeEnumGroup.ReturnCode)]
        R05_UnauthorizedDebitEntry,
        [EnumCategory(AchReturnCodeEnumGroup.ReturnCode)]
        R06_ReturnedPerODFIsRequest,
        [EnumCategory(AchReturnCodeEnumGroup.ReturnCode)]
        R07_AuthorizationRevoked,
        [EnumCategory(AchReturnCodeEnumGroup.ReturnCode)]
        R08_PaymentStopped,
        [EnumCategory(AchReturnCodeEnumGroup.ReturnCode)]
        R09_UncollectedFunds,
        [EnumCategory(AchReturnCodeEnumGroup.ReturnCode)]
        R10_CustomerAdvisesNotAuthorized,
        [EnumCategory(AchReturnCodeEnumGroup.ReturnCode)]
        R11_CheckSafekeepingEntryReturn,
        [EnumCategory(AchReturnCodeEnumGroup.ReturnCode)]
        R12_BranchSoldToAnotherDFI,
        [EnumCategory(AchReturnCodeEnumGroup.ReturnCode)]
        R13_RDFINotQualifiedToParticipate,
        [EnumCategory(AchReturnCodeEnumGroup.ReturnCode)]
        R14_AccountHolderDeceased,
        [EnumCategory(AchReturnCodeEnumGroup.ReturnCode)]
        R16_AccountFrozen,
        [EnumCategory(AchReturnCodeEnumGroup.ReturnCode)]
        R17_FileRecordEditCriteria,
        [EnumCategory(AchReturnCodeEnumGroup.ReturnCode)]
        R18_ImproperEffectiveEntryDate,
        [EnumCategory(AchReturnCodeEnumGroup.ReturnCode)]
        R19_AmountFieldError,
        [EnumCategory(AchReturnCodeEnumGroup.ReturnCode)]
        R20_NonTransactionAccount,
        [EnumCategory(AchReturnCodeEnumGroup.ReturnCode)]
        R21_InvalidCompanyIdentification,
        [EnumCategory(AchReturnCodeEnumGroup.ReturnCode)]
        R22_InvalidIndividualIDNumber,
        [EnumCategory(AchReturnCodeEnumGroup.ReturnCode)]
        R23_CreditRefusedByReceiver,
        [EnumCategory(AchReturnCodeEnumGroup.ReturnCode)]
        R24_DuplicateEntry,
        [EnumCategory(AchReturnCodeEnumGroup.ReturnCode)]
        R26_MandatoryFieldError,
        [EnumCategory(AchReturnCodeEnumGroup.ReturnCode)]
        R28_RoutingNumberCheckDigitError,
        [EnumCategory(AchReturnCodeEnumGroup.ReturnCode)]
        R29_CorporateCustomerAdvisesNotAuthorized,
        [EnumCategory(AchReturnCodeEnumGroup.ReturnCode)]
        R31_PermissibleReturnEntry,
        [EnumCategory(AchReturnCodeEnumGroup.ReturnCode)]
        R32_RDFINonSettlement,
        [EnumCategory(AchReturnCodeEnumGroup.ReturnCode)]
        R33_ReturnOfXCKEntry,
        [EnumCategory(AchReturnCodeEnumGroup.ReturnCode)]
        R34_LimitedParticipationDFI,
        [EnumCategory(AchReturnCodeEnumGroup.ReturnCode)]
        R37_SourceDocumentPresentedForPayment,
        [EnumCategory(AchReturnCodeEnumGroup.ReturnCode)]
        R38_StopPaymentOnSourceDocument,
        [EnumCategory(AchReturnCodeEnumGroup.ReturnCode)]
        R39_ImproperSourceDocumentSourceDocument,
        [EnumCategory(AchReturnCodeEnumGroup.ReturnCode)]
        R50_StateLawAffectingRCKAcceptance,
        [EnumCategory(AchReturnCodeEnumGroup.ReturnCode)]
        R51_ItemRelatedToRCKEntryIsIneligible,
        [EnumCategory(AchReturnCodeEnumGroup.ReturnCode)]
        R52_StopPaymentOnItemRelatedToRCKEntry,
        [EnumCategory(AchReturnCodeEnumGroup.ReturnCode)]
        R53_ItemAndRCKEntryPresentedForPayment,
        [EnumCategory(AchReturnCodeEnumGroup.ReturnCode)]
        R80_IATEntryCodingError,
        [EnumCategory(AchReturnCodeEnumGroup.ReturnCode)]
        R81_NonParticipantInIATProgram,
        [EnumCategory(AchReturnCodeEnumGroup.ReturnCode)]
        R82_InvalidForeignReceivingDFIIdentification,
        [EnumCategory(AchReturnCodeEnumGroup.ReturnCode)]
        R83_ForeignReceivingDFIUnableToSettle,
        [EnumCategory(AchReturnCodeEnumGroup.ReturnCode)]
        R84_EntryNotProcessedByGateway,
        [EnumCategory(AchReturnCodeEnumGroup.ReturnCode)]
        R85_IncorrectlyCodedOutboundInternationalPayment,
        [EnumCategory(AchReturnCodeEnumGroup.ReturnCode)]
        R61_MisroutedReturn,
        [EnumCategory(AchReturnCodeEnumGroup.ReturnCode)]
        R67_DuplicateReturn,
        [EnumCategory(AchReturnCodeEnumGroup.ReturnCode)]
        R68_UntimelyReturn,
        [EnumCategory(AchReturnCodeEnumGroup.ReturnCode)]
        R69_FieldError,
        [EnumCategory(AchReturnCodeEnumGroup.ReturnCode)]
        R70_PermissibleReturnEntryNotAcceptedReturnNotRequestedByODFI,

        // VPNG-16835 Removing AchReturnCodeEnumGroup.ReturnCode from Notice of Change Codes "C[XX]"
        // These codes do not necessarily indicate a return (rejected) and are typically coupled 
        // with a Status of "Corr/Paid" which are included as part of the settlement report
        [EnumCategory(AchReturnCodeEnumGroup.NocCode)]
        C00_Unknown,
        [EnumCategory(AchReturnCodeEnumGroup.NocCode)]
        C01_IncorrectDFIAccount,
        [EnumCategory(AchReturnCodeEnumGroup.NocCode)]
        C02_IncorrectRoutingNumber,
        [EnumCategory(AchReturnCodeEnumGroup.NocCode)]
        C03_IncorrectRoutingNumberAndDFIAccountNumber,
        [EnumCategory(AchReturnCodeEnumGroup.NocCode)]
        C04_IncorrectIndividualCompanyName,
        [EnumCategory(AchReturnCodeEnumGroup.NocCode)]
        C05_IncorrectTransactionCode,
        [EnumCategory(AchReturnCodeEnumGroup.NocCode)]
        C06_IncorrectAccountNumberAndTransactionCode,
        [EnumCategory( AchReturnCodeEnumGroup.NocCode)]
        C07_IncorrectRoutingNumberIncorrectDFIAccountNumberAndIncorrectTransactionCode,
        [EnumCategory(AchReturnCodeEnumGroup.NocCode)]
        C08_IncorrectReceivingDFIdentificationIATOnly,
        [EnumCategory(AchReturnCodeEnumGroup.NocCode)]
        C13_AddendaFormatError,
        [EnumCategory(AchReturnCodeEnumGroup.NocCode)]
        C14_IncorrectSECCodeForOutboundInternationalPayment
    }
}

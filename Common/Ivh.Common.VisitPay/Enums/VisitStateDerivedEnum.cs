﻿namespace Ivh.Common.VisitPay.Enums
{
    public enum VisitStateDerivedEnum
    {
        Active = 1,
        Inactive = 2,
        OnHold = 3,
        MaxAge = 4,
        OnFinancePlan = 5,
        Unmatched = 6,
        MaxAgeClosed = 7
    }
}

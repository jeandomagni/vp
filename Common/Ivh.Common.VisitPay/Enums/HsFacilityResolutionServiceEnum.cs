﻿namespace Ivh.Common.VisitPay.Enums
{
    public enum HsFacilityResolutionServiceEnum
    {
        HsFacilityResolutionService,
        IntermountainHsFacilityResolutionService
    }
}
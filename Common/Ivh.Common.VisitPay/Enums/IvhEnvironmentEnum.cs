﻿// ReSharper disable InconsistentNaming
namespace Ivh.Common.VisitPay.Enums
{
    using Attributes;

    public static class IvhEnvironmentEnumCategory
    {
        public const string Unknown = "Unknown";
        public const string Dev = "Dev";
        public const string Qa = "Qa";
        public const string Phi = "Phi";
        public const string Pci = "Pci";
    }

    public enum IvhEnvironmentEnum
    {
        //this should be deprecated in favor IvhEnvironmentType
        [EnumCategory(IvhEnvironmentEnumCategory.Unknown)]
        Unknown,
        [EnumCategory(IvhEnvironmentEnumCategory.Dev)]
        Dev,
        [EnumCategory(IvhEnvironmentEnumCategory.Dev)]
        Development,

        [EnumCategory(IvhEnvironmentEnumCategory.Qa)]
        Test,
        [EnumCategory(IvhEnvironmentEnumCategory.Qa)]
        test01,
        [EnumCategory(IvhEnvironmentEnumCategory.Qa)]
        test02,
        [EnumCategory(IvhEnvironmentEnumCategory.Qa)]
        Qa,
        [EnumCategory(IvhEnvironmentEnumCategory.Qa)]
        Qa2,
        [EnumCategory(IvhEnvironmentEnumCategory.Qa)]
        Qaa,
        [EnumCategory(IvhEnvironmentEnumCategory.Qa)]
        Demo,
        [EnumCategory(IvhEnvironmentEnumCategory.Qa)]
        Stage,
        [EnumCategory(IvhEnvironmentEnumCategory.Qa)]
        LoadTest,
        [EnumCategory(IvhEnvironmentEnumCategory.Qa)]
        Training,
        [EnumCategory(IvhEnvironmentEnumCategory.Qa)]
        DQM,
        [EnumCategory(IvhEnvironmentEnumCategory.Qa)]
        qaa01,
        [EnumCategory(IvhEnvironmentEnumCategory.Qa)]
        qaa02,
        [EnumCategory(IvhEnvironmentEnumCategory.Qa)]
        qaa03,
        [EnumCategory(IvhEnvironmentEnumCategory.Qa)]
        qaa04,
        [EnumCategory(IvhEnvironmentEnumCategory.Qa)]
        qa01,
        [EnumCategory(IvhEnvironmentEnumCategory.Qa)]
        qa02,
        [EnumCategory(IvhEnvironmentEnumCategory.Qa)]
        qa03,
        [EnumCategory(IvhEnvironmentEnumCategory.Qa)]
        qa04,
        [EnumCategory(IvhEnvironmentEnumCategory.Qa)]
        demo01,
        [EnumCategory(IvhEnvironmentEnumCategory.Qa)]
        demo02,
        [EnumCategory(IvhEnvironmentEnumCategory.Qa)]
        demo03,
        [EnumCategory(IvhEnvironmentEnumCategory.Qa)]
        demo04,
        [EnumCategory(IvhEnvironmentEnumCategory.Qa)]
        loadtest01,
        [EnumCategory(IvhEnvironmentEnumCategory.Qa)]
        loadtest02,
        [EnumCategory(IvhEnvironmentEnumCategory.Qa)]
        training01,
        [EnumCategory(IvhEnvironmentEnumCategory.Qa)]
        training02,
        [EnumCategory(IvhEnvironmentEnumCategory.Qa)]
        dqm01,
        [EnumCategory(IvhEnvironmentEnumCategory.Qa)]
        dqm02,


        [EnumCategory(IvhEnvironmentEnumCategory.Phi)]
        Integration,
        [EnumCategory(IvhEnvironmentEnumCategory.Phi)]
        Integration02,
        [EnumCategory(IvhEnvironmentEnumCategory.Phi)]
        Integration03,
        [EnumCategory(IvhEnvironmentEnumCategory.Phi)]
        Migration,
        [EnumCategory(IvhEnvironmentEnumCategory.Phi)]
        Support,
        [EnumCategory(IvhEnvironmentEnumCategory.Phi)]
        support01,
        [EnumCategory(IvhEnvironmentEnumCategory.Phi)]
        support02,
        [EnumCategory(IvhEnvironmentEnumCategory.Phi)]
        support03,
        [EnumCategory(IvhEnvironmentEnumCategory.Phi)]
        int01,
        [EnumCategory(IvhEnvironmentEnumCategory.Phi)]
        int02,
        [EnumCategory(IvhEnvironmentEnumCategory.Phi)]
        int03,
        [EnumCategory(IvhEnvironmentEnumCategory.Phi)]
        int04,

        [EnumCategory(IvhEnvironmentEnumCategory.Pci)]
        PreProd,
        [EnumCategory(IvhEnvironmentEnumCategory.Pci)]
        preprod01,
        [EnumCategory(IvhEnvironmentEnumCategory.Pci)]
        preprod02,
        [EnumCategory(IvhEnvironmentEnumCategory.Pci)]
        PreProduction,
        [EnumCategory(IvhEnvironmentEnumCategory.Pci)]
        Prod,
        [EnumCategory(IvhEnvironmentEnumCategory.Pci)]
        prod01,
        [EnumCategory(IvhEnvironmentEnumCategory.Pci)]
        prod02,
        [EnumCategory(IvhEnvironmentEnumCategory.Pci)]
        Production,
    }
}
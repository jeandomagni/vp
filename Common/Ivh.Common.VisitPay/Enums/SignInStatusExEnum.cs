﻿namespace Ivh.Common.VisitPay.Enums
{
    using Attributes;

    public static class SignInStatusExEnumCategory
    {
        public const string Success = "Success";
        public const string Failure = "Failure";
    }

    public enum SignInStatusExEnum
    {
        [EnumCategory(SignInStatusExEnumCategory.Success)]
        PasswordExpired,
        [EnumCategory(SignInStatusExEnumCategory.Success)]
        RequiresVerification,
        [EnumCategory(SignInStatusExEnumCategory.Success)]
        RequiresSecurityQuestions,
        [EnumCategory(SignInStatusExEnumCategory.Success)]
        Success,

        [EnumCategory(SignInStatusExEnumCategory.Failure)]
        AccountClosed,
        [EnumCategory(SignInStatusExEnumCategory.Failure)]
        Failure,
        [EnumCategory(SignInStatusExEnumCategory.Failure)]
        LockedOut
    }
}
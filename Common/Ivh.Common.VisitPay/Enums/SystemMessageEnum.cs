﻿namespace Ivh.Common.VisitPay.Enums
{
    public enum SystemMessageEnum
    {
        WelcomeMessage = 1,
        Sso = 2,
        Consolidate = 3,
        Sms = 4,
        Hqy = 6,
        PaymentSummary = 8,
        Eob = 9,
        HasExistingBadDebt = 10,
        OnPrePaymentPlan = 11,
        PaymentSummaryNotification = 12
    }
}
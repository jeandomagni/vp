﻿namespace Ivh.Common.VisitPay.Enums
{
    public enum AccountRecoveryMatchFieldEnum
    {
        Ssn4 = 1,
        FirstName = 2
    }
}

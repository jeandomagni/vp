﻿namespace Ivh.Common.VisitPay.Enums
{
    public enum SsoProviderEnum
    {
        Unknown = 0,
        OpenEpic = 1,
        HealthEquityOutbound = 2,
        /// <summary>
        /// mock HQY provider
        /// </summary>
        HealthEquityOAuthImplicit = 3,
        HealthEquityOAuthGrant = 4,
    }
}

﻿namespace Ivh.Common.VisitPay.Enums
{
    using Attributes;

    public static class VisitPayFeatureEnumCategory
    {
        public const string DemoFeature = "DemoFeature";
        public const string UsabilityFeature = "UsabilityFeature";
    }

    public enum VisitPayFeatureEnum
    {
        Consolidation = 1,
        Itemization = 2,
        HouseholdBalance = 3,
        DemoIsAlert = 4,
        MyChartSso = 5,
        PaymentSummary = 6,
        RescheduleRecurringPaymentGuarantor = 7,
        ConsolidationMasking = 8,
        PaymentsAch = 9,
        PaymentsCreditCard = 10,
        RescheduleRecurringPaymentClient = 12,
        CancelRecurringPaymentClient = 13,
        CancelRecurringPaymentGuarantor = 14,
        CancelRecurringPaymentPdGuarantor = 15,
        Surveys = 16,
        HealthEquityFeature = 17,
        HealthEquityShowOutboundSso = 18,
        HealthEquityShowBalance = 19,
        Sms = 20,
        FpOfferSetTypesIsEnabledClient = 21,
        CombineFinancePlans = 22,
        ExplanationOfBenefitsImport = 23,
        OfflineVisitPay = 25,
        ExplanationOfBenefitsUserInterface = 26,
        CalculateCombinedFinancePlanMinimumIsEnabled = 27,
        BalanceTransfer = 28,
        MerchantAccountSplittingIsEnabled = 32,
        GuarantorSecurityQuestionsIsEnabled = 33,
        VisitPayReportsIsEnabled = 34,
        FeatureShowBadDebtNotificationIsEnabled = 37,
        AlternatePublicPages = 38,
        FeatureShowPrePaymentPlanNotificationIsEnabled = 39,
        FeatureRestrictIpAddressByCountry = 40,
        FeatureVisitPayAgingIsEnabled = 41,
        FeatureHideRedactedVisitsIsEnabled = 42,
        FeaturePaymentApiIsEnabled = 44,
        FinancePlanVisitSelectionIsEnabled = 45,
        FeatureExternalFinancePlanPaymentEnabled = 46,
        FeatureRedirectInternationalUserRegistration = 47,
        FeaturePageHelpIsEnabled = 48,
        FeatureTextToPayIsEnabled = 49,
        FeatureVisitUnmatchingMethodSelection = 50,
        FeatureVisitPayDocumentIsEnabled = 51,
        FeatureMatchingApiIsEnabled = 52,
        FeatureAnalyticReportsIsEnabled = 53,
        FeatureCardReaderIsEnabled = 54,
        FeatureIsLockBoxEnabled = 55,
        FeatureChatIsEnabled = 56,
        FeatureMailIsEnabled = 57,
        FeatureFacilityListIsEnabled = 58,
        FeatureOfflineComboOnly = 59,
        FeatureOptimisticAchSettlementIsEnabled = 60,
        FeatureRefundReturnedChecks = 61,
        FeatureResetAgingTierForReactivatedMaxAgeVisits = 62,
        FeatureFacilityContactUsGridIsEnabled = 63,
        FeatureSuppressDuplicatePrincipalPaymentsInUi = 64,
        FeatureSimpleTermsIsEnabled = 65,
        FinancePlanRefundInterestIncentive = 66,

        #region demo features

        [EnumCategory(VisitPayFeatureEnumCategory.DemoFeature)]
        DemoPreServiceUiIsEnabled = 1000,

        [EnumCategory(VisitPayFeatureEnumCategory.DemoFeature)]
        DemoMyChartSSOUiIsEnabled = 1001,

        [EnumCategory(VisitPayFeatureEnumCategory.DemoFeature)]
        DemoInsuranceAccrualUiIsEnabled = 1002,

        [EnumCategory(VisitPayFeatureEnumCategory.DemoFeature)]
        DemoClientReportDashboardIsEnabled = 1003,

        [EnumCategory(VisitPayFeatureEnumCategory.DemoFeature)]
        DemoHealthEquityIsEnabled = 1004,

        #endregion

        #region usability

        [EnumCategory(VisitPayFeatureEnumCategory.UsabilityFeature)]
        FeatureDebrandedUiIsEnabled = 2000,

        #endregion

    }
}

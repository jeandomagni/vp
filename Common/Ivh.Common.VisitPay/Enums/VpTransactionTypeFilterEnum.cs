﻿namespace Ivh.Common.VisitPay.Enums
{
    public enum VpTransactionTypeFilterEnum
    {
        Charges = 1,
        Insurance = 2,
        Interest = 3,
        Other = 4,
        Payments = 5
    }
}
﻿namespace Ivh.Common.VisitPay.Enums
{
    using Attributes;

    public static class CmsTypeEnumCategory
    {
        public const string ReplacementValues = "ReplacementValues";
    }

    public enum CmsTypeEnum
    {
        [EnumCategory(CmsTypeEnumCategory.ReplacementValues)]
        WebsiteContent = 1,

        [EnumCategory(CmsTypeEnumCategory.ReplacementValues)]
        LegalDocs = 2,

        EmailContent = 3,

        [EnumCategory(CmsTypeEnumCategory.ReplacementValues)]
        SupportRequestTemplate = 4,

        WebsiteLabel = 5,

        [EnumCategory(CmsTypeEnumCategory.ReplacementValues)]
        StylesheetContent = 7,

        PaperLetterContent = 8
    }
}

﻿namespace Ivh.Common.VisitPay.Enums
{
    public enum ResultEnum
    {
        NotFound,
        Found,
        MoreThanOneFound
    }
}
﻿namespace Ivh.Common.VisitPay.Enums
{
    public enum SurveyRatingGroupTypeEnum
    {
        AgreeDisagreeSurveyFormat = 1,
        QuestionInputSurveyFormat = 2,
        NetPromoterScoreFormat = 3,
        YesNoSurveyFormat = 4,
        CustomerEffortScoreFormat = 5,
        TimeOnTaskFormat = 6
    }
}
﻿namespace Ivh.Common.VisitPay.Enums
{
    public enum VpOutboundVisitMessageTypeEnum
    {
        UncollectableNotification = 1,
        VisitAddedToFinancePlan = 4,
        VisitRemovedFromFinancePlan = 5,
        VpVisitUnmatch = 6,
        VisitAgingCountChange = 7,
        BalanceTransferAgencyAssume = 8,
        BalanceTransferAgencyReturn = 9,
        VisitStatementedNotification = 10,
        VpReceivedVisit = 11,
        VisitStatusSetToActive = 12,
        VisitStatusSetToInactive = 13,
        FinancePlanVisitStatusChanged = 14,
        FinancePlanVisitBucketChanged = 15
    }
}
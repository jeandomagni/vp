﻿namespace Ivh.Common.VisitPay.Enums
{
    public enum ChangeSetTypeEnum
    {
        Guarantor = 1,
        Visit = 2,
    }
}

﻿namespace Ivh.Common.VisitPay.Enums
{
    public enum EnrollmentResultEnum
    {
        Success = 0,
        AlreadyEnrolled = 1,
        AlreadyCancelled = 2,
        NoGuarantor = 3,
        Invalid = 4,
        Error = -1,
    }
}
﻿namespace Ivh.Common.VisitPay.Enums
{

    public enum VisitStateEnum
    {
        NotSet = 0,
        Active = 1,
        Inactive = 2
    }
}
﻿namespace Ivh.Common.VisitPay.Enums
{
    using Attributes;

    public static class HsGuarantorMatchStatusEnumCategory
    {
        public const string Prohibited = "Prohibited";
        public const string Matched = "Matched";
        public const string Allowed = "Allowed";
    }

    public enum HsGuarantorMatchStatusEnum
    {
        [EnumCategory(HsGuarantorMatchStatusEnumCategory.Prohibited)]
        NotSet = 0,
        [EnumCategory(HsGuarantorMatchStatusEnumCategory.Matched, HsGuarantorMatchStatusEnumCategory.Allowed)]
        Matched = 1,
        [EnumCategory(HsGuarantorMatchStatusEnumCategory.Allowed)]
        UnmatchedSoft = 2,
        [EnumCategory(HsGuarantorMatchStatusEnumCategory.Prohibited)]
        UnmatchedHard = 3,
        [EnumCategory(HsGuarantorMatchStatusEnumCategory.Matched, HsGuarantorMatchStatusEnumCategory.Allowed)]
        VpccPending = 4
    }
}

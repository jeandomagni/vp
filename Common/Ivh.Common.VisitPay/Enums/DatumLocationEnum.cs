﻿namespace Ivh.Common.VisitPay.Enums
{
    public enum DatumLocationEnum
    {
        Unknown,
        NoDb,
        App,
        Etl,
        Cdi,
        Express,
        Logging,
    }
}

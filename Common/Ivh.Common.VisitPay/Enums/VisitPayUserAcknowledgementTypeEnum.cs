﻿namespace Ivh.Common.VisitPay.Enums
{
    public enum VisitPayUserAcknowledgementTypeEnum
    {
        TermsOfUse = 1,
        EsignAct = 2,
        SmsAccept = 3
    }
}
﻿namespace Ivh.Common.VisitPay.Enums
{
    /// <summary>
    /// String enumeration used by Identity Framework to identify the purpose of a token create / validate call
    /// </summary>
    public enum TokenPurposeEnum
    {
        /// <summary>
        /// Checking / Creating token to reset a password
        /// </summary>
         ResetPassword = 0,

        /// <summary>
        /// Checking / Creating a token to emulate a user
        /// </summary>
         EmulateUser = 1,

        /// <summary>
        /// Checking a token for login using a temporary password
        /// </summary>
         TempLogin = 2,

        /// <summary>
        /// used to provide a vpcc user temporary access to accept a finance plan
        /// </summary>
        VpccLogin = 3
    }
}
﻿namespace Ivh.Common.VisitPay.Enums
{
    public enum VisitPayUserAddressTypeEnum
    {
        Mailing = 1,
        Physical = 2
    }
}
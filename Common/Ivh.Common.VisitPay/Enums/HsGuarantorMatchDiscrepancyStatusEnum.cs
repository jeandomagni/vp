﻿namespace Ivh.Common.VisitPay.Enums
{
    public enum HsGuarantorMatchDiscrepancyStatusEnum
    {
        NotSet = 0,
        Open = 1,
        ValidMatch = 2,
        Unmatched = 3,
        Cancelled = 4,
    }
}
﻿namespace Ivh.Common.VisitPay.Enums
{
    public enum PaymentSystemTypeEnum
    {
        VisitPay = 1,
        GuestPay = 2,
        PaymentApi = 3
    }
}

﻿namespace Ivh.Common.VisitPay.Enums
{
    public enum FirstInterestPeriodEnum
    {
        /// <summary>
        /// in awaiting statement period of created statement
        /// </summary>
        NoAction = 0,

        /// <summary>
        /// in grace period of created statement
        /// </summary>
        PushOnePeriod = 1,
    }
}
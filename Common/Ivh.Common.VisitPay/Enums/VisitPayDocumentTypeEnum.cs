﻿namespace Ivh.Common.VisitPay.Enums
{
    using System.ComponentModel;

    public enum VisitPayDocumentTypeEnum
    {
        [Description("Release Notes")]
        ReleaseNotes = 1,
        [Description("Training Materials")]
        TrainingMaterials = 2
    }
}

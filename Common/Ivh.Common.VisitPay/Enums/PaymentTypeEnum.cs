﻿namespace Ivh.Common.VisitPay.Enums
{
    using Attributes;

    public static class PaymentTypeEnumCategory
    {
        public const string Unknown = "Unknown";
        public const string Manual = "Manual";
        public const string Payment = "Payment";
        public const string Prompt = "Prompt";
        public const string DiscountEligible = "DiscountEligible";
        public const string Scheduled = "Scheduled";
        public const string ConsolidationCancellation = "ConsolidationCancellation";
        public const string Recurring = "Recurring";
        public const string Reversal = "Reversal";
        public const string Resubmit = "Resubmit";
        public const string Household = "Household";
        public const string CurrentNonFinancedBalanceInFull = "CurrentNonFinancedBalanceInFull";
        public const string SpecificFinancePlan = "SpecificFinancePlan";
        public const string SpecificVisits = "SpecificVisits";
    }

    // WARNING: Coupling. When adding a new PaymentType be sure to add it in CDI_ETL database ([Vp].[PaymentType])
    public enum PaymentTypeEnum
    {
        [EnumCategory(PaymentTypeEnumCategory.Unknown)]
        Unknown = 0,

        [EnumCategory(PaymentTypeEnumCategory.Manual, PaymentTypeEnumCategory.Payment, PaymentTypeEnumCategory.Prompt)]
        ManualPromptSpecificAmount = 1,

        [EnumCategory(PaymentTypeEnumCategory.Manual, PaymentTypeEnumCategory.Payment, PaymentTypeEnumCategory.Prompt, PaymentTypeEnumCategory.SpecificVisits)]
        ManualPromptSpecificVisits = 2,

        [EnumCategory(PaymentTypeEnumCategory.Manual, PaymentTypeEnumCategory.Payment, PaymentTypeEnumCategory.Prompt, PaymentTypeEnumCategory.SpecificFinancePlan)]
        ManualPromptSpecificFinancePlans = 3,

        [EnumCategory(PaymentTypeEnumCategory.Manual, PaymentTypeEnumCategory.Payment, PaymentTypeEnumCategory.Prompt, PaymentTypeEnumCategory.DiscountEligible)]
        ManualPromptCurrentBalanceInFull = 5,

        [EnumCategory(PaymentTypeEnumCategory.Manual, PaymentTypeEnumCategory.Payment, PaymentTypeEnumCategory.Scheduled, PaymentTypeEnumCategory.ConsolidationCancellation)]
        ManualScheduledSpecificAmount = 6,

        [EnumCategory(PaymentTypeEnumCategory.Manual, PaymentTypeEnumCategory.Payment, PaymentTypeEnumCategory.Scheduled, PaymentTypeEnumCategory.SpecificVisits, PaymentTypeEnumCategory.ConsolidationCancellation)]
        ManualScheduledSpecificVisits = 7,

        [EnumCategory(PaymentTypeEnumCategory.Manual, PaymentTypeEnumCategory.Payment, PaymentTypeEnumCategory.Scheduled, PaymentTypeEnumCategory.SpecificFinancePlan, PaymentTypeEnumCategory.ConsolidationCancellation)]
        ManualScheduledSpecificFinancePlans = 8,

        [EnumCategory(PaymentTypeEnumCategory.Manual, PaymentTypeEnumCategory.Payment, PaymentTypeEnumCategory.Scheduled, PaymentTypeEnumCategory.ConsolidationCancellation, PaymentTypeEnumCategory.DiscountEligible)]
        ManualScheduledCurrentBalanceInFull = 10,

        [EnumCategory(PaymentTypeEnumCategory.Payment, PaymentTypeEnumCategory.Recurring)]
        RecurringPaymentFinancePlan = 11,

        [EnumCategory(PaymentTypeEnumCategory.Manual, PaymentTypeEnumCategory.Payment, PaymentTypeEnumCategory.Prompt, PaymentTypeEnumCategory.DiscountEligible, PaymentTypeEnumCategory.CurrentNonFinancedBalanceInFull)]
        ManualPromptCurrentNonFinancedBalanceInFull = 12,

        [EnumCategory(PaymentTypeEnumCategory.Manual, PaymentTypeEnumCategory.Payment, PaymentTypeEnumCategory.Scheduled, PaymentTypeEnumCategory.ConsolidationCancellation, PaymentTypeEnumCategory.DiscountEligible,PaymentTypeEnumCategory.CurrentNonFinancedBalanceInFull)]
        ManualScheduledCurrentNonFinancedBalanceInFull = 13,

        [EnumCategory(PaymentTypeEnumCategory.Manual, PaymentTypeEnumCategory.Payment, PaymentTypeEnumCategory.Prompt, PaymentTypeEnumCategory.SpecificFinancePlan)]
        ManualPromptSpecificFinancePlansBucketZero = 14,

        [EnumCategory(PaymentTypeEnumCategory.Manual, PaymentTypeEnumCategory.Payment, PaymentTypeEnumCategory.Prompt, PaymentTypeEnumCategory.SpecificFinancePlan)]
        LockBoxManualPromptSpecificFinancePlansBucketZero = 15,

        // Reversal Types
        [EnumCategory(PaymentTypeEnumCategory.Reversal)]
        Void = 40,

        [EnumCategory(PaymentTypeEnumCategory.Reversal)]
        Refund = 41,

        [EnumCategory(PaymentTypeEnumCategory.Reversal)]
        PartialRefund = 42,

        ReallocationFromInterestToPrincipal = 43,
        
        //These aren't in the database.  These are special types.
        [EnumCategory(PaymentTypeEnumCategory.Resubmit)]
        ResubmitPayments = 50,

        [EnumCategory(PaymentTypeEnumCategory.Household, PaymentTypeEnumCategory.CurrentNonFinancedBalanceInFull)]
        HouseholdBalanceCurrentNonFinancedBalance = 52


    }
}
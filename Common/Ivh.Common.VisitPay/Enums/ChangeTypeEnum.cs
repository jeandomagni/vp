﻿namespace Ivh.Common.VisitPay.Enums
{
    public enum ChangeTypeEnum
    {
        Update,
        Insert,
        Delete
    }
}
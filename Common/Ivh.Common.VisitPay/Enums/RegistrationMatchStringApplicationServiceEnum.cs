﻿namespace Ivh.Common.VisitPay.Enums
{
    public enum RegistrationMatchStringApplicationServiceEnum
    {
        DefaultRegistrationMatchStringApplicationService,
        IntermountainRegistrationMatchStringApplicationService
    }
}
﻿namespace Ivh.Common.VisitPay.Enums
{
    public enum FinancePlanClosedReasonEnum
    {
        Suspended = 1,
        FpClosed = 2,
    }
}
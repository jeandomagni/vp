﻿namespace Ivh.Common.VisitPay.Enums
{
    using Attributes;

    public static class ChangeEventTypeEnumCategory
    {
        public const string Scoring = nameof(Scoring);
        public const string Visit = nameof(Visit);
        public const string Unmatch = nameof(Unmatch);
    }

    public enum ChangeEventTypeEnum
    {
        [EnumCategory(ChangeEventTypeEnumCategory.Visit, ChangeEventTypeEnumCategory.Unmatch)]
        DataChange = 2,
        [EnumCategory(ChangeEventTypeEnumCategory.Unmatch)]
        HsGuarantorMatchDiscrepancy = 7,
        [EnumCategory(ChangeEventTypeEnumCategory.Unmatch)]
        HsGuarantorUnmatch = 9,
        [EnumCategory(ChangeEventTypeEnumCategory.Scoring)]
        NewFirstSelfPayDate = 11
    }
}
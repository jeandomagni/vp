﻿namespace Ivh.Common.VisitPay.Enums
{
    public enum HsBillingSystemResolutionProviderEnum
    {
        HsBillingSystemResolutionProvider,
        IntermountainHsBillingSystemResolutionProvider
    }
}
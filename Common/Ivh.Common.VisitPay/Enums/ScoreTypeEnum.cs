﻿namespace Ivh.Common.VisitPay.Enums
{
    public enum ScoreTypeEnum
    {
        Unknown = 0,
        PtpOriginal = 1,
        ProtoScore = 2
    }
}
﻿namespace Ivh.Common.VisitPay.Enums
{
    public enum UnmatchSourceEnum
    {
        Unknown = 0,
        GuarantorAttributeChangeQueue = 1,
        GuarantorAccountDetails = 2,
    }
}
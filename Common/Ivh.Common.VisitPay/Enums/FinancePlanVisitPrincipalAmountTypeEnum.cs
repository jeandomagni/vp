﻿namespace Ivh.Common.VisitPay.Enums
{
    using System.ComponentModel;

    public static class FinancePlanVisitPrincipalAmountTypeDescription
    {
        public const string FinancePlanCreated = "Finance Plan Created";
        public const string Payment = "Payment";
        public const string Adjustment = "HS Adjustment";
        public const string FinancePlanClosed = "Finance Plan Closed";
    }

    public enum FinancePlanVisitPrincipalAmountTypeEnum
    {
        [Description(FinancePlanVisitPrincipalAmountTypeDescription.FinancePlanCreated)]
        FinancePlanCreated = 1,

        [Description(FinancePlanVisitPrincipalAmountTypeDescription.Payment)]
        Payment = 2,

        [Description(FinancePlanVisitPrincipalAmountTypeDescription.Adjustment)]
        Adjustment = 3,

        [Description(FinancePlanVisitPrincipalAmountTypeDescription.FinancePlanClosed)]
        FinancePlanClosed = 4
    }
}
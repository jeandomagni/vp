﻿namespace Ivh.Common.VisitPay.Enums
{
    using Attributes;

    public static class FinancePlanStatusEnumCategory
    {
        public const string Pending = "Pending";
        public const string Active = "Active";
        public const string GoodStanding = "GoodStanding";
        public const string PastDue = "PastDue";
        public const string Closed = "Closed";
        public const string Canceled = "Canceled";
        public const string Open = "Open";
    }

    public enum FinancePlanStatusEnum
    {
        [EnumCategory(FinancePlanStatusEnumCategory.Pending)]
        PendingAcceptance = 1,
        [EnumCategory(FinancePlanStatusEnumCategory.Pending)]
        TermsAccepted = 2,
        [EnumCategory(FinancePlanStatusEnumCategory.Active, FinancePlanStatusEnumCategory.GoodStanding, FinancePlanStatusEnumCategory.Open)]
        GoodStanding = 3,
        [EnumCategory(FinancePlanStatusEnumCategory.Active, FinancePlanStatusEnumCategory.PastDue, FinancePlanStatusEnumCategory.Open)]
        PastDue = 4,
        [EnumCategory(FinancePlanStatusEnumCategory.Closed)]
        UncollectableClosed = 5,
        [EnumCategory(FinancePlanStatusEnumCategory.Closed, FinancePlanStatusEnumCategory.Canceled)]
        Canceled = 6,
        [EnumCategory(FinancePlanStatusEnumCategory.Closed)]
        NonPaidInFull = 7,
        [EnumCategory(FinancePlanStatusEnumCategory.Closed)]
        PaidInFull = 8,
        [EnumCategory(FinancePlanStatusEnumCategory.Active, FinancePlanStatusEnumCategory.PastDue, FinancePlanStatusEnumCategory.Open)]
        UncollectableActive = 9
    }
}
﻿namespace Ivh.Common.VisitPay.Enums
{
    public enum AlertTypeEnum
    {
        ActionRequired = 1,
        Information = 2,
        Welcome = 3

    }
}
﻿namespace Ivh.Common.VisitPay.Enums
{
    public enum PaperStatementTypeEnum
    {
        FinancePlanStatement = 1,
        NonFinancedVisitStatement = 2
    }
}

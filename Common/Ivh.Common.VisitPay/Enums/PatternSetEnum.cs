﻿namespace Ivh.Common.VisitPay.Enums
{
    using Attributes;

    public static class PatternSetEnumCategory
    {
        public const string NonSsnMatching = "NonSsnMatching";
    }

    public enum PatternSetEnum
    {
        S001 = 1,
        S002 = 2,
        [EnumCategory(PatternSetEnumCategory.NonSsnMatching)]
        S003 = 3,
        S004 = 4,
        [EnumCategory(PatternSetEnumCategory.NonSsnMatching)]
        S005 = 5,
    }
}

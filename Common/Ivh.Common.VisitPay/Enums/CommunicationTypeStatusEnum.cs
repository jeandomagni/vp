﻿namespace Ivh.Common.VisitPay.Enums
{
    public enum CommunicationTypeStatusEnum
    {
        Active = 1,
        Inactive = 2
    }
}
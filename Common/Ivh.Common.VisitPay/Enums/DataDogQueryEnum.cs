﻿namespace Ivh.Common.VisitPay.Enums
{
    public enum MetricQueryEnum
    {
        RunDailyEtl,
        RunHourlyEtl,
        RunDailyApp,
        RunHourlyApp,
        RunEvery10App,
        Unknown
    }
}
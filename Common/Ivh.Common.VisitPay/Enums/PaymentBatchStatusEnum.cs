﻿namespace Ivh.Common.VisitPay.Enums
{
    public enum PaymentBatchStatusEnum
    {
        Pending = 1,
        Cleared = 2
    }
}
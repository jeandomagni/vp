﻿namespace Ivh.Common.VisitPay.Enums
{
    public enum FinancePlanTypeEnum
    {
        PaperlessAutoPay = 1,
        PaperlessNonAutoPay = 2,
        PaperAutoPay = 3,
        PaperNonAutoPay = 4
    }
}

﻿namespace Ivh.Common.VisitPay.Enums
{
    public enum ScoringStatusEnum
    {
        Queued = 1,
        InPhaseOne = 2,
        AwaitingThirdPartyData = 3,
        InPhaseTwo = 4,
        Scored = 5,
        ExcludedReconsider = 6,
        ExcludedDontReconsider = 7,
        ExcludedMatched = 8
    }
}
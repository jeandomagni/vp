﻿namespace Ivh.Common.VisitPay.Enums
{
    public enum SsoStatusEnum
    {
        Unknown = 0,
        Accepted = 1,
        Rejected = 2,
        Invalidated = 3,
        Invalidated_Acknowledged = 4
    }
}

﻿namespace Ivh.Common.VisitPay.Enums
{
    public enum SupportRequestMessageTypeEnum
    {
        FromGuarantor = 1,
        ToGuarantor = 2,
        InternalNote = 3,
        OpenMessage = 4,
        CloseMessage = 5,
        DraftMessage = 6
    }
}

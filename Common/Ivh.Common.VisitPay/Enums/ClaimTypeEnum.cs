﻿namespace Ivh.Common.VisitPay.Enums
{
    public enum ClaimTypeEnum
    {
        ClientUsername,
        ClientUserId,
        ClientFirstName,
        ClientLastName,
        ConsolidationStatus,
        EmulatedUser,
        EmulatedUserFirstName,
        EmulatedUserLastName,
        EmulationToken,
        GuarantorId,
        GuarantorStatus,
        GuarantorType,
        LastLoginDate,
        RequiresAcknowledgement,
        RequiresSsoAuthorization,
        SsoProvider,
        HealthEquityOutbound,
        OfflineUserToken,
        ObfuscationKeySessionKey,
        VisitPayUserGuid,
        VisitPayUserInsertDate
    }
}
﻿namespace Ivh.Common.VisitPay.Enums
{
    public enum RandomizedTestEnum
    {
        //CombineFinancePlansAddVisits = 2,
        Homepage = 3,
        RandomizedInterestRateOffers = 1000,
        //EobExpansion = 4,
        NotificationTesting = 5,
        FinancePlanSuggestedDuration = 6,
        PartialPaymentFinancePlanOffer = 8,
        FinancePlanSuggestedAmount = 9,
        RegistrationSurvey = 10,
        FinancePlanScoreBasedSuggestedAmount = 11,
        BalanceBasedDiscount = 12,

        // demo
        FeatureDemoMyChartSsoUi = 1001,
        FeatureDemoPreServiceUi = 1002,
        FeatureDemoInsuranceAccrualUi = 1003,
        FeatureDemoHealthEquity = 1004
    }
}

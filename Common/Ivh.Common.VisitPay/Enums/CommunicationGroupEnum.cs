﻿namespace Ivh.Common.VisitPay.Enums
{
    public enum CommunicationGroupEnum
    {
        AlwaysSend = 0,
        StatementAndBalanceDue = 1,
        PaymentMethods = 2,
        PastDueBalance = 3,
        Payments = 4,
        SupportRequests = 5
    }
}

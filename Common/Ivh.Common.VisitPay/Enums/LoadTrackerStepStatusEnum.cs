﻿namespace Ivh.Common.VisitPay.Enums
{
    public enum LoadTrackerStepStatusEnum
    {
        InProgress = 1,
        Completed = 2,
        Failed = 3,
        Skip = 4
    }
}

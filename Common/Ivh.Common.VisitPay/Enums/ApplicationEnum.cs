﻿namespace Ivh.Common.VisitPay.Enums
{
    using Attributes;

    public static class ApplicationEnumCategory
    {
        public const string SystemApplication = "SystemApplication";
    }

    public enum ApplicationEnum
    {
        Unknown = 0,
        [EnumCategory(ApplicationEnumCategory.SystemApplication)]
        BIReporting = 1,
        [EnumCategory(ApplicationEnumCategory.SystemApplication)]
        DataWarehouse = 2,
        [EnumCategory(ApplicationEnumCategory.SystemApplication)]
        Scoring = 3,
        VisitPay = 4,
        GuestPay = 5,
        Client = 6,
        QATools = 7,
        [EnumCategory(ApplicationEnumCategory.SystemApplication)]
        HsDataProcessor = 8,
        [EnumCategory(ApplicationEnumCategory.SystemApplication)]
        JobRunner = 9,
        [EnumCategory(ApplicationEnumCategory.SystemApplication)]
        VisitPayProcessor = 10,
        [EnumCategory(ApplicationEnumCategory.SystemApplication)]
        SendMailWebHookEvents = 11,
        [EnumCategory(ApplicationEnumCategory.SystemApplication)]
        MigrationTool = 12,
        [EnumCategory(ApplicationEnumCategory.SystemApplication)]
        MetricQuery = 13,
        [EnumCategory(ApplicationEnumCategory.SystemApplication)]
        ScheduleService = 14,
        [EnumCategory(ApplicationEnumCategory.SystemApplication)]
        ClientDataApi = 15,
        [EnumCategory(ApplicationEnumCategory.SystemApplication)]
        AuthorizationServer = 18,
        [EnumCategory(ApplicationEnumCategory.SystemApplication)]
        BalanceTransferProcessor = 19,
        [EnumCategory(ApplicationEnumCategory.SystemApplication)]
        ScoringProcessor = 20,
        [EnumCategory(ApplicationEnumCategory.SystemApplication)]
        UniversalProcessor = 21,
        [EnumCategory(ApplicationEnumCategory.SystemApplication)]
        DataCorrectionUtility = 22,
    }
}
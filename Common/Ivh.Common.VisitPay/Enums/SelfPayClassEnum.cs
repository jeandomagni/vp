﻿namespace Ivh.Common.VisitPay.Enums
{
    using Attributes;

    public static class SelfPayClassEnumCategory
    {
        public const string HasInsurance = "HasInsurance";
        public const string Uninsured = "Uninsured";
    }

    public enum SelfPayClassEnum
    {
        [EnumCategory(SelfPayClassEnumCategory.HasInsurance)]
        SelfPayAfterInsurance = 1,
        [EnumCategory(SelfPayClassEnumCategory.HasInsurance)]
        SelfPayAfterMedicare = 2,
        [EnumCategory(SelfPayClassEnumCategory.Uninsured)]
        PureSelfPay = 3
    }
}
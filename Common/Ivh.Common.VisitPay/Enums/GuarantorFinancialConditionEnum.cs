﻿namespace Ivh.Common.VisitPay.Enums
{
    using Attributes;

    public static class GuarantorFinancialConditionEnumCategory
    {
        public const string Discount = "Discount";
    }

    public enum GuarantorFinancialConditionEnum
    {
        Unknown,

        Canceled,
        PendingHsAck,
        HsAck,

        AccountActiveNoVisits,
        VisitsLoadedNoStatement,

        AwaitingStatementNoBalance,
        AwaitingStatementGoodStanding,
        [EnumCategory(GuarantorFinancialConditionEnumCategory.Discount)]
        AwaitingStatementGoodStandingDiscountEligible,
        AwaitingStatementGoodStandingBifScheduled,
        AwaitingStatementGoodStandingPartialScheduled,
        [EnumCategory(GuarantorFinancialConditionEnumCategory.Discount)]
        AwaitingStatementPastDueDiscountEligible,
        AwaitingStatementPastDueNoScheduled,
        AwaitingStatementPastDueBifScheduled,
        AwaitingStatementPastDuePartialScheduled,

        GracePeriodNoBalance,

        [EnumCategory(GuarantorFinancialConditionEnumCategory.Discount)]
        GracePeriodGoodStandingDiscountEligibleNoScheduled,
        [EnumCategory(GuarantorFinancialConditionEnumCategory.Discount)]
        GracePeriodGoodStandingDiscountEligibleNoScheduledDueToday,
        GracePeriodGoodStandingNotDiscountEligibleNoScheduled,
        GracePeriodGoodStandingNotDiscountEligibleNoScheduledDueToday,
        GracePeriodGoodStandingBifScheduled,
        GracePeriodGoodStandingFpScheduled,
        GracePeriodGoodStandingPartialScheduled,
        GracePeriodGoodStandingPartialScheduledDueToday,
        [EnumCategory(GuarantorFinancialConditionEnumCategory.Discount)]
        GracePeriodPastDueDiscountEligibleNoScheduled,
        GracePeriodPastDueNotDiscountEligibleNoScheduled,
        GracePeriodPastDueBifScheduled,
        GracePeriodPastDueFpScheduled,
        GracePeriodPastDuePartialScheduled
    }
}

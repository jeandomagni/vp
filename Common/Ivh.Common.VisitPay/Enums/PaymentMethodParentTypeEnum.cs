﻿namespace Ivh.Common.VisitPay.Enums
{
    using System.ComponentModel;
    using Constants;

    public enum PaymentMethodParentTypeEnum
    {
        [Description(TextRegionConstants.CreditDebitCard)]
        CreditDebitCard = 1,
        [Description(TextRegionConstants.BankAccount)]
        Ach = 2
    }
}
﻿namespace Ivh.Common.VisitPay.Enums
{
    using Attributes;

    public static class CommunicationTypeEnumCategory
    {
        public const string PaymentFailure = "PaymentFailure";
        public const string Cancellation = "Cancellation";
        public const string PaymentConfirmation = "PaymentConfirmation";
        public const string Aging = "Aging";
    }

    public enum CommunicationTypeEnum
    {
        #region headers and footers

        ClientFinancialAssistancePolicyPlainLanguage = 1005,

        ConsolidationEmailFooter = 1001,

        EmailFooter = 1000,

        EmailFooterAdmin = 1003,

        EmailFooterClient = 1002,

        EmailFooterNoLogin = 1004,

        GuestPayEmailFooter = 2199,

        MailFooter = 1006,

        #endregion

        #region client support

        [Communication]
        ClientSupportRequestAdminNotification = 148,

        [Communication]
        ClientSupportRequestConfirmation = 147,

        [Communication]
        ClientSupportRequestMessageNotification = 149,

        [Communication]
        ClientSupportRequestStatusNotification = 150,

        #endregion

        #region consolidation

        [Communication]
        ConsolidateHouseholdRequestToManaging = 200,

        [Communication]
        ConsolidateHouseholdRequestToManaged = 201,

        [Communication]
        ConsolidateHouseholdRequestCompleteToManaged = 203,

        [Communication]
        ConsolidateHouseholdRequestAcceptedByManagedToManaging = 204,

        [Communication]
        ConsolidateHouseholdRequestAcceptedByManagedWithFpToManaging = 205,

        [Communication]
        ConsolidateHouseholdRequestDeclinedToManaged = 206,

        [Communication]
        ConsolidateHouseholdRequestDeclinedToManaging = 207,

        [Communication]
        ConsolidateHouseholdRequestCancelledByManagedToManaging = 208,

        [Communication]
        ConsolidateHouseholdRequestCancelledByManagingToManaged = 209,

        [Communication]
        ConsolidateHouseholdRequestCancelledByManagedToManagingWithPendingFpTerms = 210,

        [Communication]
        ConsolidateLinkageCancelledByManagingToManaging = 211,

        [Communication]
        ConsolidateLinkageCancelledByManagingToManaged = 212,

        [Communication]
        ConsolidateLinkageCancelledByManagingToManagedWithFp = 213,

        [Communication]
        ConsolidateLinkageCancelledByManagedToManaging = 214,

        [Communication]
        ConsolidateLinkageCancelledByManagedToManaged = 215,

        [Communication]
        ConsolidateLinkageCancelledByManagedToManagedWithFp = 216,

        #endregion

        #region finance plans

        [Communication(SendManaging = true)]
        FinancePlanAdditionalBalance = 163,

        [Communication(SendManaging = true)]
        FinancePlanClosed = 159,

        [Communication]
        FinancePlanConfirmation = 164,

        [Communication]
        FinancePlanConfirmationSimpleTerms = 165,

        [Communication]
        FinancePlanCreation = 129,

        [Communication]
        FinancePlanModification = 130,

        [Communication(SendManaging = true, Frequency = NotificationFrequencyEnum.OncePerDay)]
        FinancePlanTermination = 139,

        [Communication(SendManaging = true)]
        FinancePlanVisitNotEligible = 162,

        [Communication(SendManaging = true, Frequency = NotificationFrequencyEnum.OncePerDay)]
        FinancePlanVisitSuspended = 160,

        #endregion

        #region payments / payment methods

        [Communication(SendSms = true, SendManaging = true, Frequency = NotificationFrequencyEnum.OncePerDay)]
        BalanceDueReminder = 136,

        [EnumCategory(CommunicationTypeEnumCategory.PaymentFailure)]
        [Communication(SendSms = true, SendManaging = true, SendToVpccUser = true, Frequency = NotificationFrequencyEnum.OncePerDay)]
        PaymentFailure = 119,

        [EnumCategory(CommunicationTypeEnumCategory.PaymentFailure)]
        [Communication(SendSms = true, Frequency = NotificationFrequencyEnum.OncePerDay)]
        PaymentFailureCcManaging = 244,

        [Communication(SendSms = true, SendManaging = true, SendToVpccUser = true, Frequency = NotificationFrequencyEnum.OncePerDay)]
        PendingPayment = 118,

        [Communication(SendSms = true, SendToVpccUser = true)]
        CardExpired = 132,

        [Communication(SendSms = true, SendToVpccUser = true)]
        CardExpiring = 133,

        [Communication]
        PaymentDueDateChange = 131,

        [Communication(SendManaging = true)]
        PaymentReversalNotification = 144,  // vpcc equivalent is 3006

        [Communication(Frequency = NotificationFrequencyEnum.OncePerHour)]
        PaymentMethodChange = 143,

        [Communication]
        PrimaryPaymentMethodChange = 141,

        // this communication is different from the rest, and is implemented as a one off.
        [EnumCategory(CommunicationTypeEnumCategory.PaymentConfirmation)]
        [Communication(SendSms = true, SendToVpccUser = true)]
        PaymentSubmittedConfirmation = 137,

        #endregion

        #region statements

        [EnumCategory(CommunicationTypeEnumCategory.Aging)]
        StatementNotificationActionRequired = 104,

        [EnumCategory(CommunicationTypeEnumCategory.Aging)]
        [Communication(SendSms = true, SendManaging = true, Frequency = NotificationFrequencyEnum.OncePerDay)]
        StatementNotificationFinalPastDue = 227,

        [Communication(SendSms = true, Frequency = NotificationFrequencyEnum.OncePerDay)]
        StatementNotificationFinalPastDueToManaging = 235,

        [EnumCategory(CommunicationTypeEnumCategory.Aging)]
        [Communication(SendSms = true, SendManaging = true, Frequency = NotificationFrequencyEnum.OncePerDay)]
        StatementNotificationGoodStanding = 225,

        [Communication(SendSms = true, Frequency = NotificationFrequencyEnum.OncePerDay)]
        StatementNotificationGoodStandingToManaging = 233,

        [EnumCategory(CommunicationTypeEnumCategory.Aging)]
        [Communication(SendSms = true, SendManaging = true, Frequency = NotificationFrequencyEnum.OncePerDay)]
        StatementNotificationPastDue = 226,

        [Communication(SendSms = true, Frequency = NotificationFrequencyEnum.OncePerDay)]
        StatementNotificationPastDueToManaging = 234,

        [EnumCategory(CommunicationTypeEnumCategory.Aging)]
        [Communication(SendSms = true, SendManaging = true, Frequency = NotificationFrequencyEnum.OncePerDay)]
        StatementNotificationUncollectable = 228,

        [Communication(SendSms = true, Frequency = NotificationFrequencyEnum.OncePerDay)]
        StatementNotificationUncollectableToManaging = 236,

        [Communication(SendSms = true, SendManaging = true, Frequency = NotificationFrequencyEnum.OncePerDay)]
        FinancePlanStatementNotificationGoodStanding = 229,

        [Communication(SendSms = true, Frequency = NotificationFrequencyEnum.OncePerDay)]
        FinancePlanStatementNotificationGoodStandingToManaging = 237,

        [Communication(SendSms = true, SendManaging = true, SendToVpccUser = true, Frequency = NotificationFrequencyEnum.OncePerDay)]
        FinancePlanStatementNotificationFinalPastDue = 231,

        [Communication(SendSms = true, Frequency = NotificationFrequencyEnum.OncePerDay)]
        FinancePlanStatementNotificationFinalPastDueToManaging = 239,

        [Communication(SendSms = true, SendManaging = true, Frequency = NotificationFrequencyEnum.OncePerDay)]
        FinancePlanStatementNotificationPastDue = 230,

        [Communication(SendSms = true, Frequency = NotificationFrequencyEnum.OncePerDay)]
        FinancePlanStatementNotificationPastDueToManaging = 238,

        [Communication(SendSms = true, SendManaging = true, SendToVpccUser = true, Frequency = NotificationFrequencyEnum.OncePerDay)]
        FinancePlanStatementNotificationUncollectable = 232,

        [Communication(SendSms = true, Frequency = NotificationFrequencyEnum.OncePerDay)]
        FinancePlanStatementNotificationUncollectableToManaging = 240,

        #endregion

        #region support requests

        [Communication]
        SupportRequestConfirmation = 122,

        [Communication(SendSms = true)]
        SupportRequestUpdate = 138,

        #endregion

        #region visits

        [Communication(SendSms = true, SendManaging = true, Frequency = NotificationFrequencyEnum.OncePerDay)]
        FinalPastDueNotificationFinancePlan = 241,

        [EnumCategory(CommunicationTypeEnumCategory.Aging)]
        [Communication(SendSms = true, SendManaging = true, SendToVpccUser = true, Frequency = NotificationFrequencyEnum.OncePerDay)]
        FinalPastDueNotificationNonFinancePlan = 242,

        [Communication(SendEmail = false, SendSms = true)]
        TextToPayPaymentOption = 243,

        [Communication(SendManaging = true, Frequency = NotificationFrequencyEnum.OncePerDay, SuppressOnRegistrationDay = true)]
        NewVisitLoaded = 161,

        [Communication(SendSms = true, SendManaging = true, Frequency = NotificationFrequencyEnum.OncePerDay)]
        UncollectableBalanceNotification = 108,

        [Communication(SendSms = true, SendManaging = true, Frequency = NotificationFrequencyEnum.OncePerDay)]
        VisitsPastDue = 106,

        #endregion

        #region users

        [EnumCategory(CommunicationTypeEnumCategory.Cancellation)]
        [Communication]
        AccountCancelationComplete = 120,

        [Communication]
        AccountCredentialChangeConfirmationPatient = 123,

        [Communication]
        AccountReactivationComplete = 219,

        [Communication]
        ClientAccountLockout = 158,

        [Communication]
        ClientProfileChange = 217,

        [Communication(HideLogin = true)]
        ForgotPasswordWithoutPinClient = 126,

        [Communication(HideLogin = true)]
        ForgotPasswordWithoutPinPatient = 128,

        [Communication]
        ForgotUsername = 140,

        [Communication]
        GuarantorAccountLockout = 157,

        [Communication(HideLogin = true)]
        NewUserInvitation = 101,

        [Communication(HideLogin = true)]
        NewUserWithoutPinClient = 224,

        [Communication]
        RegistrationSuccess = 102,

        [Communication]
        PasswordResetConfirmationClient = 127,

        [Communication]
        SmsActivateEmailAlert = 220,

        [Communication]
        SmsDeactivateEmailAlert = 221,

        // this communication is different from the rest, and is implemented as a one off.
        [Communication(SendEmail = false, SendSms = true)]
        SmsValidationMessage = 151,

        [Communication(Frequency = NotificationFrequencyEnum.OncePerHour)]
        UserProfileChange = 125,

        #endregion

        #region guestpay

        [Communication]
        GuestPayPaymentConfirmation = 2100,

        [Communication]
        GuestPayPaymentFailure = 2101,

        [Communication]
        GuestPayPaymentReversalNotification = 2102,

        #endregion

        #region vpcc

        [Communication(SendToVpccUser = true, SendMail = true, SendEmail = false)]
        VpccCreditAgreement = 3000,

        [Communication(SendToVpccUser = true, HideLogin = true)]
        VpccFinancePlanCreation = 3001,

        //VPCC Statement notifications
        [Communication(SendToVpccUser = true)]
        VpccFinancePlanStatementNotificationGoodStanding = 3002,

        [Communication(SendToVpccUser = true, HideLogin = true, SendEmail = false, SendSms = true)]
        VpccFinancePlanCreationSms = 3003,

        [Communication(SendToVpccUser = true, HideLogin = true)]
        VpccLoginLink = 3004,

        [Communication(SendToVpccUser = true, HideLogin = true, SendEmail = false, SendSms = true)]
        VpccLoginLinkSms = 3005,

        [Communication(SendToVpccUser = true, HideLogin = true)]
        VpccPaymentReversalNotification = 3006,

        [Communication(SendToVpccUser = true, SendMail = true, SendEmail = true)]
        VpccFinancePlanNonAutoPayStatementNotificationPastDue = 3007,

        [Communication(SendToVpccUser = true, SendMail = true, SendEmail = true)]
        VpccFinancePlanAutoPayStatementNotificationPastDue = 3008,

        [Communication(SendToVpccUser = true, SendMail = true, SendEmail = true)]
        VpccFinancePlanNotificationFinalPastDue = 3009,

        [Communication(SendToVpccUser = true, SendMail = true, SendEmail = false)]
        VpccFinancePlanNonAutoPayBalanceDueReminder = 3010,

        [Communication(SendToVpccUser = true, SendMail = true, SendEmail = false)]
        VpccNotificationCardExpiring = 3011,

        [Communication(SendToVpccUser = true, SendMail = true, SendEmail = true)]
        VpccFinancePlanAdditionalBalance = 3012,

        [Communication(SendToVpccUser = true)]
        VpccFinancePlanConfirmationSimpleTerms = 3013,

        [Communication(SendToVpccUser = true)]
        VpccFinancePlanModification = 3014

        #endregion
    }
}
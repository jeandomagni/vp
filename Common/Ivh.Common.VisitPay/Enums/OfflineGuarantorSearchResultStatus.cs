﻿namespace Ivh.Common.VisitPay.Enums
{
    public enum OfflineGuarantorSearchResultStatusEnum
    {
        Found = 0,
        NotFound = 1,
        Ambiguous = 2
    }
}
﻿namespace Ivh.Common.VisitPay.Enums
{
    public enum FinancePlanOfferSetTypeEnum
    {
        GuarantorOffers = 1,
        ExtendedOffers = 2,
        ScoreBasedOffer = 3
    }
}
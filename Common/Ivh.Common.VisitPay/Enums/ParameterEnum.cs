﻿namespace Ivh.Common.VisitPay.Enums
{
    public enum ParameterEnum
    {
        SampleMonitorCountMax,
        SampleMonitorCountMin,
        GuarantorDailyRecordMin,
        AccountDailyRecordMin,
        TransactionDailyRecordMin,
        HsCurrentBalanceVisitBalanceMismatchMaxAllowable
    }
}

﻿namespace Ivh.Common.VisitPay.Enums
{
    public enum FailedPaymentErrorCodeEnum
    {
        Unknown = 0,

        DeclineType_decline,
        DeclineType_avs,
        DeclineType_cvv,
        DeclineType_call,
        DeclineType_expiredcard,
        DeclineType_carderror,
        DeclineType_authexpired,
        DeclineType_fraud,
        DeclineType_blacklist,
        DeclineType_velocity,

        BadData_missingfields,
        BadData_extrafields,
        BadData_badformat,
        BadData_badlength,
        BadData_merchantaccept,
        BadData_mismatch,

        ErrorType_cantconnect,
        ErrorType_dnsfailure,
        ErrorType_linkfailure,
        ErrorType_failtoprocess,
        ErrorType_notallowed,

        ConnectionError_ApiDown,

        ConnectionError_LinkFailure,

        ResponseCode_500,
        ResponseCode_102

    }
}

﻿namespace Ivh.Common.VisitPay.Enums
{
    using Attributes;

    public static class AlertCriteriaEnumCategory
    {
        public const string FailedPayment = "FailedPayment";
        public const string Vpcc = "Vpcc";
    }

    public enum AlertCriteriaEnum
    {
        [EnumCategory(AlertCriteriaEnumCategory.FailedPayment)]
        FailedPayment = 1,
        [EnumCategory(AlertCriteriaEnumCategory.FailedPayment)]
        FailedPaymentManagedGuarantor = 2,
        [EnumCategory(AlertCriteriaEnumCategory.FailedPayment)]
        FailedPaymentManagingGuarantor = 3,
        CardExpiring = 4,
        CardExpired = 5,
        FinancePlanPendingAcceptance = 6,
        ManagedConsolidationCancelledNoPaymentMethod = 7,
        ReconfiguredFinancePlan = 8,
        PendingRequestToBeManaged = 9,
        PendingRequestToBeManaging = 10,
        PendingRequestToBeManagingFp = 11,
        Demo = 12,
        SsoInvalidated = 17,
        SsoHqySiteDown = 18,
        ClientFinancePlanOfferPendingAcceptance = 19,
        UnreadSupportRequestMessage = 20,
        HealthEquityGetStarted = 21,
        [EnumCategory(AlertCriteriaEnumCategory.Vpcc)]
        ReconfigurationAvailable = 22
    }
}
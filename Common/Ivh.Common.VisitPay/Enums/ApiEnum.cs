﻿namespace Ivh.Common.VisitPay.Enums
{
    public enum ApiEnum
    {
        Unknown = 0,
        ClientData = 1,
        Settings = 2,
        Content = 3
    }
}

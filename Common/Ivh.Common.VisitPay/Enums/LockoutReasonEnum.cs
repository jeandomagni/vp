﻿namespace Ivh.Common.VisitPay.Enums
{
    public enum LockoutReasonEnum
    {
        LoginFailed = 1,
        AccountDisabled = 2,
        AccountPendingCancellation = 3,
        AccountCancellation = 4,
        AccountPendingReactivation = 5,
        Offline = 6,
        OfflinePending = 7
    }
}

﻿namespace Ivh.Common.VisitPay.Enums
{
    using Attributes;

    public static class RandomizedTestGroupEnumCategory
    {
        public const string ControlGroup = nameof(ControlGroup);
    }


    public enum RandomizedTestGroupEnum
    {
        FinancePlan1 = 1,
        FinancePlan2 = 2,
        FinancePlan3 = 3,
        CombineFPsandAddVisitsGroupA = 4,
        CombineFPsandAddVisitsGroupB = 5,
        TestPageExpanded = 6,
        DefaultPage = 7,
        DefaultPageExpanded = 8,
        ExpandedExplainationofBenefits = 9,
        CollapsedExplainationofBenefits = 10,
        MessageViaAlert = 11,
        MessageViaNotification = 12,
        FinancePlanSuggestedDurationA = 13,
        FinancePlanSuggestedDurationB = 14,
        [EnumCategory(RandomizedTestGroupEnumCategory.ControlGroup)]
        FinancePlanSuggestedDurationC = 15,
        NoSupportPhoneNumberOnHomePage = 16,
        PartialPaymentFinancePlanOfferA = 18,
        PartialPaymentFinancePlanOfferB = 19,
        FinancePlanSuggestedAmountA = 20,
        FinancePlanSuggestedAmountB = 21,
        [EnumCategory(RandomizedTestGroupEnumCategory.ControlGroup)]
        FinancePlanSuggestedAmountC = 22,
        RegistrationSurveyShouldSurvey = 23,
        RegistrationSurveyShouldNotSurvey = 24,
        FinancePlanScoreBasedSuggestedAmount = 25,
        FinancePlanScoreBasedSuggestedAmountNone = 26,
        BalanceBasedDiscount = 27,
        BalanceBasedDiscountNone = 28,



        GroupA = 1001,
        GroupB = 1002,
        FeatureDemoMyChartSsoUiGroupA = 1003,
        FeatureDemoPreServiceUiGroupA = 1004,
        FeatureDemoInsuranceAccrualUiGroupA = 1005,
        FeatureDemoHealthEquityGroupA = 1006,
    }
}

﻿namespace Ivh.Common.VisitPay.Enums
{
    public enum OfferCalculationStrategyEnum
    {
        MonthlyPayment = 1,
        NumberOfPayments = 2
    }
}
﻿namespace Ivh.Common.VisitPay.Enums
{
    public enum ConsolidationGuarantorCancellationReasonEnum
    {
        ExpiredBySystem = 1,
        GuarantorAccountCancelled = 2,
        CancelledByManaging = 3,
        CancelledByManaged = 4,
    }
}
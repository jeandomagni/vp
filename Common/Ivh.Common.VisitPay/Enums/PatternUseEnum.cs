﻿namespace Ivh.Common.VisitPay.Enums
{
    public enum PatternUseEnum
    {
        Initial = 1,
        Ongoing = 2,
    }
}

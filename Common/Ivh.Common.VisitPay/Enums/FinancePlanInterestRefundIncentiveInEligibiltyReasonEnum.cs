﻿namespace Ivh.Common.VisitPay.Enums
{
    using Attributes;

    public static class FinancePlanInterestRefundIncentiveEligibilityEnumCategory
    {
        public const string IsEligible = "IsEligible";
        public const string IsNotEligible = "IsNotEligible";
    }

    public enum FinancePlanInterestRefundIncentiveEligibilityEnum
    {
        [EnumCategory(FinancePlanInterestRefundIncentiveEligibilityEnumCategory.IsNotEligible)]
        FeatureIsNotEnabled,
        [EnumCategory(FinancePlanInterestRefundIncentiveEligibilityEnumCategory.IsNotEligible)]
        FinancePlanIsNotInGoodStanding,
        [EnumCategory(FinancePlanInterestRefundIncentiveEligibilityEnumCategory.IsNotEligible)]
        HasNoPaidInterestAndInterestRateIsZero,
        [EnumCategory(FinancePlanInterestRefundIncentiveEligibilityEnumCategory.IsNotEligible)]
        FinancePlanDoesNotMeetRefundTriggerCriteria,
        [EnumCategory(FinancePlanInterestRefundIncentiveEligibilityEnumCategory.IsEligible)]
        IsEligible
    }
}

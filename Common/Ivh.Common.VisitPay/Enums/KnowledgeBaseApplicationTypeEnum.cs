﻿namespace Ivh.Common.VisitPay.Enums
{
    public enum KnowledgeBaseApplicationTypeEnum
    {
        Mobile,
        Client,
        Web
    }
}
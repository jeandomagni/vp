﻿namespace Ivh.Common.VisitPay.Enums
{
    public enum PaymentImportStateEnum
    {
        Pending = 1,
        Success = 2,
        Cancelled = 3,
        Failed = 4
    }
}

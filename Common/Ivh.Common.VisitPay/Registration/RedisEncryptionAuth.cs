﻿namespace Ivh.Common.VisitPay.Registration
{
    public class RedisEncryptionAuth : SingleValue<string>
    {
        public RedisEncryptionAuth(string value) : base(value)
        {
        }
    }
}
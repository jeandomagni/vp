﻿namespace Ivh.Common.VisitPay.Registration
{
    public abstract class SingleValue<TValue>
    {
        protected SingleValue(TValue value)
        {
            this.Value = value;
        }

        public TValue Value { get; set; }
    }
}

﻿namespace Ivh.Common.VisitPay.Registration
{
    public class RedisEncryptionMaster : SingleValue<string>
    {
        public RedisEncryptionMaster(string value) : base(value)
        {
        }
    }

    public class FileStorageIncomingFolder : SingleValue<string>
    {
        public FileStorageIncomingFolder(string value) : base(value)
        {
        }
    }
    
    public class FileStorageArchiveFolder : SingleValue<string>
    {
        public FileStorageArchiveFolder(string value) : base(value)
        {
        }
    }
}

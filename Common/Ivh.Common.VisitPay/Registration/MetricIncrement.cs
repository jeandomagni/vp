﻿namespace Ivh.Common.VisitPay.Registration
{
    using System;

    public class MetricIncrement : SingleValue<Action<string>>
    {
        public MetricIncrement(Action<string> value) : base(value)
        {
        }
    }
}

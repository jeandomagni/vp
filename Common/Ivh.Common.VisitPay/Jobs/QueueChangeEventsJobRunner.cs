﻿namespace Ivh.Common.VisitPay.Jobs
{
    using Attributes;
    using Autofac;
    using Enums;
    using JobRunner.Common;
    using JobRunner.Common.Interfaces;

    [JobRunner(JobRunnerTypeEnum.QueueChangeEvents)]
    public class QueueChangeEventsJobRunner : Job, IJob
    {
        public QueueChangeEventsJobRunner(IComponentContext context) : base(context)
        {
        }
    }
}

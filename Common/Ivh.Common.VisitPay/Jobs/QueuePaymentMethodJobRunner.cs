﻿namespace Ivh.Common.VisitPay.Jobs
{
    using Attributes;
    using Autofac;
    using Enums;
    using JobRunner.Common;
    using JobRunner.Common.Interfaces;

    [JobRunner(JobRunnerTypeEnum.QueuePaymentMethod)]
    public class QueuePaymentMethodJobRunner : Job, IJob
    {
        public QueuePaymentMethodJobRunner(IComponentContext context) : base(context)
        {
        }
    }
}
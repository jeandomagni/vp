﻿namespace Ivh.Common.VisitPay.Jobs
{
    using Attributes;
    using Autofac;
    using Enums;
    using JobRunner.Common;
    using JobRunner.Common.Interfaces;

    [JobRunner(JobRunnerTypeEnum.QueueVisitReconciliation)]
    public class QueueVisitReconciliationJobRunner : Job, IJob
    {
        public QueueVisitReconciliationJobRunner(IComponentContext context) : base(context)
        {
        }
    }
}
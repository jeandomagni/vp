﻿namespace Ivh.Common.VisitPay.Jobs
{
    using Attributes;
    using Autofac;
    using Enums;
    using JobRunner.Common;
    using JobRunner.Common.Interfaces;

    [JobRunner(JobRunnerTypeEnum.QueuePaymentReconciliation)]
    public class QueuePaymentReconciliationJobRunner : Job, IJob
    {
        public QueuePaymentReconciliationJobRunner(IComponentContext context) : base(context)
        {
        }
    }
}
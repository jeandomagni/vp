﻿namespace Ivh.Common.VisitPay.Jobs
{
    using System;
    using System.Net.Mime;
    using Attributes;
    using Autofac;
    using Enums;
    using JobRunner.Common;
    using JobRunner.Common.Interfaces;

    [JobRunner(JobRunnerTypeEnum.StatementExportBatchProcess)]
    public class StatementExportBatchProcessJobRunner : Job, IJob
    {
        public StatementExportBatchProcessJobRunner(IComponentContext context) : base(context)
        {
        }
    }
}

﻿namespace Ivh.Common.VisitPay.Jobs
{
    using Attributes;
    using Autofac;
    using Enums;
    using JobRunner.Common;
    using JobRunner.Common.Interfaces;

    [JobRunner(JobRunnerTypeEnum.QueueMatchReconiliationJobRunner)]
    public class QueueMatchReconiliationJobRunner : Job, IJob
    {
        public QueueMatchReconiliationJobRunner(IComponentContext context) : base(context)
        {
        }
    }
}

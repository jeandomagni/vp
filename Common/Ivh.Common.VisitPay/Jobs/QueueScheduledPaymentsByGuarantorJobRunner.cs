﻿namespace Ivh.Common.VisitPay.Jobs
{
    using Attributes;
    using Autofac;
    using Enums;
    using JobRunner.Common;
    using JobRunner.Common.Interfaces;

    [JobRunner(JobRunnerTypeEnum.QueueScheduledPaymentsByGuarantor, -(24 * 60 * 60 * 1000), -10, (30 * 60))]
    public class QueueScheduledPaymentsByGuarantorJobRunner : Job, IJob
    {
        public QueueScheduledPaymentsByGuarantorJobRunner(IComponentContext context) : base(context)
        {
        }
    }
}
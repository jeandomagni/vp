﻿namespace Ivh.Common.VisitPay.Jobs
{
    using Attributes;
    using Autofac;
    using Enums;
    using JobRunner.Common;
    using JobRunner.Common.Interfaces;

    [JobRunner(JobRunnerTypeEnum.QueueStatements, 0, 0, (30 * 60))]
    public class QueueStatementsJobRunner : Job, IJob
    {
        public QueueStatementsJobRunner(IComponentContext context) : base(context)
        {
        }
    }
}
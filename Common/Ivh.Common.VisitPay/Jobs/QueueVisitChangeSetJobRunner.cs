﻿namespace Ivh.Common.VisitPay.Jobs
{
    using Attributes;
    using Autofac;
    using Enums;
    using JobRunner.Common;
    using JobRunner.Common.Interfaces;

    [JobRunner(JobRunnerTypeEnum.QueueVisitChangeSet, 0, 0, (30 * 60))]
    public class QueueVisitChangeSetJobRunner : Job, IJob
    {
        public QueueVisitChangeSetJobRunner(IComponentContext context) : base(context)
        {
        }
    }
}
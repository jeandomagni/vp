﻿
namespace Ivh.Common.VisitPay.Jobs
{
    using Attributes;
    using Autofac;
    using Enums;
    using JobRunner.Common;
    using JobRunner.Common.Interfaces;

    [JobRunner(JobRunnerTypeEnum.Inbound_LoadClaimsFiles)]
    public class InboundLoadClaimsDataJobRunner : Job, IJob
    {
        public InboundLoadClaimsDataJobRunner(IComponentContext context) : base(context)
        {
        }
    }
}

﻿namespace Ivh.Common.VisitPay.Jobs
{
    using Attributes;
    using Autofac;
    using Enums;
    using JobRunner.Common;
    using JobRunner.Common.Interfaces;

    [JobRunner(JobRunnerTypeEnum.Visit)]
    public class VisitJobRunner : Job, IJob
    {
        public VisitJobRunner(IComponentContext context) : base(context)
        {
        }
    }
}

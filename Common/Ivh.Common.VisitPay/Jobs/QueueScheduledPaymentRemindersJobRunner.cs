﻿namespace Ivh.Common.VisitPay.Jobs
{
    using Attributes;
    using Autofac;
    using Enums;
    using JobRunner.Common;
    using JobRunner.Common.Interfaces;

    [JobRunner(JobRunnerTypeEnum.QueueScheduledPaymentReminders)]
    public class QueueScheduledPaymentRemindersJobRunner : Job, IJob
    {
        public QueueScheduledPaymentRemindersJobRunner(IComponentContext context) : base(context)
        {
        }
    }
}
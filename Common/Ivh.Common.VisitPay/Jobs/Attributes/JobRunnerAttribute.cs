﻿﻿namespace Ivh.Common.VisitPay.Jobs.Attributes
{
    using System;
    using Constants;
    using Enums;

    [AttributeUsage(AttributeTargets.Class)]
    public class JobRunnerAttribute : Attribute
    {
        public int? ExitTimeoutSeconds { get; set; }
        public int? ExitCheckIntervalSeconds { get; set; }

        public int? DateTimeBeginOffset { get; private set; }
        public int? DateTimeEndOffset { get; private set; }

        public string JobName { get; private set; }

        public JobRunnerTypeEnum JobRunnerType { get; private set; }     

        public JobRunnerAttribute(
            JobRunnerTypeEnum jobRunnerType,
            int dateTimeBeginOffset = 0,
            int dateTimeEndOffset = 0,
            int exitTimeoutSeconds = 0,
            int exitCheckIntervalSeconds = 0)
        {
            this.InitJobRunnerAttribute(jobRunnerType, dateTimeBeginOffset, dateTimeEndOffset, exitTimeoutSeconds, exitCheckIntervalSeconds);
        }

        private void InitJobRunnerAttribute(JobRunnerTypeEnum jobRunnerType,
            int dateTimeBeginOffset = 0,
            int dateTimeEndOffset = 0,
            int exitTimeoutSeconds = 0,
            int exitCheckIntervalSeconds = 0)
        {
            this.ExitCheckIntervalSeconds = exitCheckIntervalSeconds != 0 ? new int?(exitCheckIntervalSeconds) : default(int?);
            this.ExitTimeoutSeconds = exitTimeoutSeconds != 0 ? new int?(exitTimeoutSeconds) : default(int?);
            this.DateTimeBeginOffset = dateTimeBeginOffset;
            this.DateTimeEndOffset = dateTimeEndOffset;
            this.JobRunnerType = jobRunnerType;
            this.JobName = this.JobRunnerType.ToString();
        }
    }
}
﻿namespace Ivh.Common.VisitPay.Jobs
{
    using Attributes;
    using Autofac;
    using Enums;
    using JobRunner.Common;
    using JobRunner.Common.Interfaces;

    [JobRunner(JobRunnerTypeEnum.PublishMonitorData)]
    public class PublishMonitorDataJobRunner : Job, IJob
    {
        public PublishMonitorDataJobRunner(IComponentContext context) : base(context)
        {
        }
    }
}

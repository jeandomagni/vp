﻿namespace Ivh.Common.VisitPay.Jobs
{
    using Attributes;
    using Autofac;
    using Enums;
    using JobRunner.Common;
    using JobRunner.Common.Interfaces;

    [JobRunner(JobRunnerTypeEnum.QueueMatchRegistryPopulation)]
    public class QueueMatchRegistryPopulationJobRunner : Job, IJob
    {
        public QueueMatchRegistryPopulationJobRunner(IComponentContext context) : base(context)
        {
        }
    }
}

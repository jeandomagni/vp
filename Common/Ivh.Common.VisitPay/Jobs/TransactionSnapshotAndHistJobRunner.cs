﻿namespace Ivh.Common.VisitPay.Jobs
{
    using Attributes;
    using Autofac;
    using Enums;
    using JobRunner.Common;
    using JobRunner.Common.Interfaces;

    [JobRunner(JobRunnerTypeEnum.TransactionSnapshotAndHist)]
    public class TransactionSnapshotAndHistJobRunner : Job, IJob
    {
        public TransactionSnapshotAndHistJobRunner(IComponentContext context) : base(context)
        {
        }
    }
}

﻿namespace Ivh.Common.VisitPay.Jobs
{
    using System;
    using System.Net.Mime;
    using Attributes;
    using Autofac;
    using Enums;
    using JobRunner.Common;
    using JobRunner.Common.Interfaces;

    [JobRunner(JobRunnerTypeEnum.Inbound_LoadHistory)]
    //[JobRunner(JobRunnerTypeEnum.Inbound_LoadHistory, DatumEnum.?, DatumEnum.?)]
    //[JobRunnerAfterExecuteMonitors(true, MonitorEnum.?)]
    public class InboundLoadHistoryJobRunner : Job, IJob
    {
        public InboundLoadHistoryJobRunner(IComponentContext context) : base(context)
        {
        }
    }
}

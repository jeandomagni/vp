﻿namespace Ivh.Common.VisitPay.Jobs
{
    using Attributes;
    using Autofac;
    using Enums;
    using JobRunner.Common;
    using JobRunner.Common.Interfaces;

    [JobRunner(JobRunnerTypeEnum.Inbound_PaymentVendorFile)]
    public class InboundPaymentVendorFileJobRunner : Job, IJob
    {
        public InboundPaymentVendorFileJobRunner(IComponentContext context) : base(context)
        {
        }
    }
}
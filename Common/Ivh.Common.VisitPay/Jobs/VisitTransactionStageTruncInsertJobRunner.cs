﻿namespace Ivh.Common.VisitPay.Jobs
{
    using Attributes;
    using Autofac;
    using Enums;
    using JobRunner.Common;
    using JobRunner.Common.Interfaces;

    [JobRunner(JobRunnerTypeEnum.VisitTransactionStageTruncInsert)]
    public class VisitTransactionStageTruncInsertJobRunner : Job, IJob
    {
        public VisitTransactionStageTruncInsertJobRunner(IComponentContext context) : base(context)
        {
        }
    }
}

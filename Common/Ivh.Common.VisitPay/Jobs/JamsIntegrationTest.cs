﻿namespace Ivh.Common.VisitPay.Jobs
{
    using Attributes;
    using Autofac;
    using Enums;
    using JobRunner.Common;
    using JobRunner.Common.Interfaces;

    [JobRunner(JobRunnerTypeEnum.JamsIntegrationTest)]
    public class JamsIntegrationTest : Job, IJob
    {
        public JamsIntegrationTest(IComponentContext context) : base(context)
        {
        }
    }
}
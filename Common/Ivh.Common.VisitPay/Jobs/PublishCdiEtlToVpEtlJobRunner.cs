﻿namespace Ivh.Common.VisitPay.Jobs
{
    using Attributes;
    using Autofac;
    using Enums;
    using JobRunner.Common;
    using JobRunner.Common.Interfaces;

    [JobRunner(JobRunnerTypeEnum.PublishCdiEtlToVpEtl)]
    public class PublishCdiEtlToVpEtlJobRunner : Job, IJob
    {
        public PublishCdiEtlToVpEtlJobRunner(IComponentContext context) : base(context)
        {
        }
    }
}

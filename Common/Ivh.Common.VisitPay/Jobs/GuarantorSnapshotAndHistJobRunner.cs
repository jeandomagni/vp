﻿namespace Ivh.Common.VisitPay.Jobs
{
    using Attributes;
    using Autofac;
    using Enums;
    using JobRunner.Common;
    using JobRunner.Common.Interfaces;


    [JobRunner(JobRunnerTypeEnum.GuarantorSnapshotAndHist)]
    public class GuarantorSnapshotAndHistJobRunner : Job, IJob
    {
        public GuarantorSnapshotAndHistJobRunner(IComponentContext context) : base(context)
        {
        }
    }
}

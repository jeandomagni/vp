﻿namespace Ivh.Common.VisitPay.Jobs
{
    using Attributes;
    using Autofac;
    using Enums;
    using JobRunner.Common;
    using JobRunner.Common.Interfaces;

    [JobRunner(JobRunnerTypeEnum.QueueConsolidationExpiration)]
    public class QueueConsolidationExpirationJobRunner : Job, IJob
    {
        public QueueConsolidationExpirationJobRunner(IComponentContext context) : base(context)
        {
        }
    }
}
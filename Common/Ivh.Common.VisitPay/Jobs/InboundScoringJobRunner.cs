﻿
namespace Ivh.Common.VisitPay.Jobs
{
    using Attributes;
    using Autofac;
    using Enums;
    using JobRunner.Common;
    using JobRunner.Common.Interfaces;

    [JobRunner(JobRunnerTypeEnum.Inbound_ScoringAndSegmentation)]
    public class InboundScoringJobRunner : Job, IJob
    {
        public InboundScoringJobRunner(IComponentContext context) : base(context)
        {
        }
    }
}

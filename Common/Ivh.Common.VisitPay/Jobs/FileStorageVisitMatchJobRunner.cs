﻿namespace Ivh.Common.VisitPay.Jobs
{
    using Attributes;
    using Autofac;
    using Enums;
    using JobRunner.Common;
    using JobRunner.Common.Interfaces;

    [JobRunner(JobRunnerTypeEnum.FileStorageVisitMatch, 0, 0, 60 * 5, 15)]
    public class FileStorageVisitMatchJobRunner : Job, IJob
    {
        public FileStorageVisitMatchJobRunner(IComponentContext context) : base(context)
        {
        }
    }
}
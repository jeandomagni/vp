﻿namespace Ivh.Common.VisitPay.Jobs
{
    using Attributes;
    using Autofac;
    using Enums;
    using JobRunner.Common;
    using JobRunner.Common.Interfaces;

    [JobRunner(JobRunnerTypeEnum.Inbound_LoadChangeEvents_Part1)]
    //[JobRunner(JobRunnerTypeEnum.Inbound_LoadChangeEvents_Part1, DatumEnum.?, DatumEnum.?)]
    //[JobRunnerAfterExecuteMonitors(true, MonitorEnum.?)]
    public class InboundLoadChangeEventsPart1JobRunner : Job, IJob
    {
        public InboundLoadChangeEventsPart1JobRunner(IComponentContext context) : base(context)
        {
        }
    }
}

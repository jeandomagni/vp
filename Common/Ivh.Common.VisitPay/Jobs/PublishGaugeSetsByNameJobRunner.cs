﻿namespace Ivh.Common.VisitPay.Jobs
{
    using Attributes;
    using Autofac;
    using Enums;
    using JobRunner.Common;
    using JobRunner.Common.Interfaces;

    [JobRunner(JobRunnerTypeEnum.PublishGaugeSetsByName)]
    public class PublishGaugeSetsByNameJobRunner : Job, IJob
    {
        public PublishGaugeSetsByNameJobRunner(IComponentContext context) : base(context)
        {
        }
    }
}

﻿namespace Ivh.Common.VisitPay.Jobs
{
    using Attributes;
    using Autofac;
    using Enums;
    using JobRunner.Common;
    using JobRunner.Common.Interfaces;

    [JobRunner(JobRunnerTypeEnum.Inbound_LoadSnapshotAndDelta)]
    //[JobRunner(JobRunnerTypeEnum.Inbound_LoadSnapshotAndDelta, DatumEnum.?, DatumEnum.?)]
    //[JobRunnerAfterExecuteMonitors(true, MonitorEnum.?)]
    public class InboundLoadSnapshotAndDeltaJobRunner : Job, IJob
    {
        public InboundLoadSnapshotAndDeltaJobRunner(IComponentContext context) : base(context)
        {
        }
    }
}

﻿namespace Ivh.Common.VisitPay.Jobs
{
    using Attributes;
    using Autofac;
    using Enums;
    using JobRunner.Common;
    using JobRunner.Common.Interfaces;

    [JobRunner(JobRunnerTypeEnum.PaperMailExportBatchProcess)]
    public class PaperMailExportBatchProcessJobRunner : Job, IJob
    {
        public PaperMailExportBatchProcessJobRunner(IComponentContext context) : base(context)
        {
        }
    }
}

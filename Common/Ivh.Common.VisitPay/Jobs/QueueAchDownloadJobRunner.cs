﻿namespace Ivh.Common.VisitPay.Jobs
{
    using Attributes;
    using Autofac;
    using Enums;
    using JobRunner.Common;
    using JobRunner.Common.Interfaces;

    [JobRunner(JobRunnerTypeEnum.QueueAchDownload)]
    public class QueueAchDownloadJobRunner : Job, IJob
    {
        public QueueAchDownloadJobRunner(IComponentContext context) : base(context)
        {
        }
    }
}
﻿namespace Ivh.Common.VisitPay.Jobs
{
    using System;
    using System.Net.Mime;
    using Attributes;
    using Autofac;
    using Enums;
    using JobRunner.Common;
    using JobRunner.Common.Interfaces;

    [JobRunner(JobRunnerTypeEnum.GuarantorStageTruncInsert)]
    public class GuarantorStageTruncInsertJobRunner : Job, IJob
    {
        public GuarantorStageTruncInsertJobRunner(IComponentContext context) : base(context)
        {
        }
    }
}

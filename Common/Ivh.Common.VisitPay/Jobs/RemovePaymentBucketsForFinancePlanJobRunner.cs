﻿namespace Ivh.Common.VisitPay.Jobs
{
    using Attributes;
    using Autofac;
    using Enums;
    using JobRunner.Common;
    using JobRunner.Common.Interfaces;

    [JobRunner(JobRunnerTypeEnum.RemovePaymentBucketsForFinancePlan)]
    public class RemovePaymentBucketsForFinancePlanJobRunner : Job, IJob
    {
        public RemovePaymentBucketsForFinancePlanJobRunner(IComponentContext context) : base(context)
        {
        }
    }
}
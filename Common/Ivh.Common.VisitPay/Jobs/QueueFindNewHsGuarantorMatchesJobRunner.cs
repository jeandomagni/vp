﻿namespace Ivh.Common.VisitPay.Jobs
{
    using Attributes;
    using Autofac;
    using Enums;
    using JobRunner.Common;
    using JobRunner.Common.Interfaces;

    [JobRunner(JobRunnerTypeEnum.QueueFindNewHsGuarantorMatchesJobRunner)]
    public class QueueFindNewHsGuarantorMatchesJobRunner : Job, IJob
    {
        public QueueFindNewHsGuarantorMatchesJobRunner(IComponentContext context) : base(context)
        {
        }
    }
}

﻿namespace Ivh.Common.VisitPay.Jobs
{
    using Attributes;
    using Autofac;
    using Enums;
    using JobRunner.Common;
    using JobRunner.Common.Interfaces;

    [JobRunner(JobRunnerTypeEnum.Outbound_PaperFiles)]
    public class OutboundPaperFilesJobRunner : Job, IJob
    {
        public OutboundPaperFilesJobRunner(IComponentContext context) : base(context)
        {
        }
    }
}
﻿namespace Ivh.Common.VisitPay.Jobs
{
    using Attributes;
    using Autofac;
    using Enums;
    using JobRunner.Common;
    using JobRunner.Common.Interfaces;

    [JobRunner(JobRunnerTypeEnum.QueueAchPublish, 0, 0, 60 * 5, 15)]
    public class QueueAchPublishJobRunner : Job, IJob
    {
        public QueueAchPublishJobRunner(IComponentContext context) : base(context)
        {
        }
    }
}
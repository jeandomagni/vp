﻿namespace Ivh.Common.VisitPay.Jobs
{
    using Attributes;
    using Autofac;
    using Enums;
    using JobRunner.Common;
    using JobRunner.Common.Interfaces;

    [JobRunner(JobRunnerTypeEnum.PublishGaugesByName)]
    public class PublishGaugesByNameJobRunner : Job, IJob
    {
        public PublishGaugesByNameJobRunner(IComponentContext context) : base(context)
        {
        }
    }
}

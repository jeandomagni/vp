﻿namespace Ivh.Common.VisitPay.Jobs
{
    using Attributes;
    using Autofac;
    using Enums;
    using JobRunner.Common;
    using JobRunner.Common.Interfaces;

    [JobRunner(JobRunnerTypeEnum.QueueSystemExceptionLogging)]
    public class QueueSystemExceptionLoggingJobRunner : Job, IJob
    {
        public QueueSystemExceptionLoggingJobRunner(IComponentContext context) : base(context)
        {
        }
    }
}
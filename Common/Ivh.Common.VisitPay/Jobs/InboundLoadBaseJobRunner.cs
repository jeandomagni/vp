﻿namespace Ivh.Common.VisitPay.Jobs
{
    using System;
    using System.Net.Mime;
    using Attributes;
    using Autofac;
    using Enums;
    using JobRunner.Common;
    using JobRunner.Common.Interfaces;

    [JobRunner(JobRunnerTypeEnum.Inbound_LoadBase)]
    //[JobRunner(JobRunnerTypeEnum.Inbound_LoadBase, DatumEnum.?, DatumEnum.?)]
    //[JobRunnerAfterExecuteMonitors(true, MonitorEnum.?)]
    public class InboundLoadBaseJobRunner : Job, IJob
    {
        public InboundLoadBaseJobRunner(IComponentContext context) : base(context)
        {
        }
    }
}

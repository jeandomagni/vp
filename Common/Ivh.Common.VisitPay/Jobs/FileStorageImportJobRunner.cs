﻿namespace Ivh.Common.VisitPay.Jobs
{
    using System.IO;
    using Attributes;
    using Autofac;
    using Enums;
    using JobRunner.Common;
    using Registration;

    [JobRunner(JobRunnerTypeEnum.FileStorageImport)]
    public class FileStorageImportJobRunner : Job
    {
        private readonly string _incomingFolder;
        private readonly string _archiveFolder;
        private string[] _files;

        public FileStorageImportJobRunner(IComponentContext componentContext)
            : base(componentContext)
        {

            this._incomingFolder = this.Context.Resolve<FileStorageIncomingFolder>().Value;
            this._archiveFolder = this.Context.Resolve<FileStorageArchiveFolder>().Value;
        }

        /// <summary>
        /// Count the number of files that are in the incoming SFTP Folder and return as Expected count
        /// </summary>
        /// <returns></returns>
        public override decimal? ResultsExpectedValue()
        {
            this._files = Directory.GetFiles(this._incomingFolder);
            return this._files.Length;
        }
        

        /// <summary>
        /// Count the number of files to be processed that end up in the archive Folder after processing
        /// and return as the Actual count.
        /// </summary>
        /// <returns></returns>
        public override decimal? ResultsActualValue()
        {
            decimal count = 0;
            foreach(string f in this._files)
            {
                if (File.Exists(Path.Combine(this._archiveFolder, f)))
                {
                    count++;
                }
            }
            return count;
        }
    }
}
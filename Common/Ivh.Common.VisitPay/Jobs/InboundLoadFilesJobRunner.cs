﻿namespace Ivh.Common.VisitPay.Jobs
{
    using Attributes;
    using Autofac;
    using Enums;
    using JobRunner.Common;
    using JobRunner.Common.Interfaces;

    [JobRunner(JobRunnerTypeEnum.Inbound_LoadFiles)]
    //[JobRunner(JobRunnerTypeEnum.Inbound_LoadFiles, DatumEnum.?, DatumEnum.?)]
    //[JobRunnerAfterExecuteMonitors(true, MonitorEnum.?)]
    public class InboundLoadFilesJobRunner : Job, IJob
    {
        public InboundLoadFilesJobRunner(IComponentContext context) : base(context)
        {
        }
    }
}

﻿namespace Ivh.Common.VisitPay.Jobs
{
    using Attributes;
    using Autofac;
    using Enums;
    using JobRunner.Common;
    using JobRunner.Common.Interfaces;

    [JobRunner(JobRunnerTypeEnum.QueueHeldEmailForResend)]
    public class QueueHeldEmailForResendJobRunner : Job, IJob
    {
        public QueueHeldEmailForResendJobRunner(IComponentContext context) : base(context)
        {
        }
    }
}
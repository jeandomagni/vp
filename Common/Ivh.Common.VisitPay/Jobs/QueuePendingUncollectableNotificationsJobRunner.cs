﻿namespace Ivh.Common.VisitPay.Jobs
{
    using Attributes;
    using Autofac;
    using Enums;
    using JobRunner.Common;
    using JobRunner.Common.Interfaces;

    [JobRunner(JobRunnerTypeEnum.QueuePendingUncollectableNotifications)]
    public class QueuePendingUncollectableNotificationsJobRunner : Job, IJob
    {
        public QueuePendingUncollectableNotificationsJobRunner(IComponentContext context) : base(context)
        {
        }
    }
}
﻿namespace Ivh.Common.VisitPay.Jobs
{
    using Attributes;
    using Autofac;
    using Enums;
    using JobRunner.Common;
    using JobRunner.Common.Interfaces;

    [JobRunner(JobRunnerTypeEnum.HsGuarantor)]
    public class HsGuarantorJobRunner : Job, IJob
    {
        public HsGuarantorJobRunner(IComponentContext context) : base(context)
        {
        }
    }
}

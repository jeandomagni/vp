﻿namespace Ivh.Common.VisitPay.Jobs
{
    using Attributes;
    using Autofac;
    using Enums;
    using JobRunner.Common;
    using JobRunner.Common.Interfaces;

    [JobRunner(JobRunnerTypeEnum.FixPaymentAllocationsForPayment)]
    public class FixPaymentAllocationsForPaymentJobRunner : Job, IJob
    {
        public FixPaymentAllocationsForPaymentJobRunner(IComponentContext context) : base(context)
        {
        }
    }
}
﻿namespace Ivh.Common.VisitPay.Jobs
{
    using Attributes;
    using Autofac;
    using Enums;
    using JobRunner.Common;
    using JobRunner.Common.Interfaces;

    [JobRunner(JobRunnerTypeEnum.Inbound_LoadChangeEvents_Part2)]
    //[JobRunner(JobRunnerTypeEnum.Inbound_LoadChangeEvents_Part2, DatumEnum.?, DatumEnum.?)]
    //[JobRunnerAfterExecuteMonitors(true, MonitorEnum.?)]
    public class InboundLoadChangeEventsPart2JobRunner : Job, IJob
    {
        public InboundLoadChangeEventsPart2JobRunner(IComponentContext context) : base(context)
        {
        }
    }
}

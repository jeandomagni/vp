﻿namespace Ivh.Common.VisitPay.Jobs
{
    using Attributes;
    using Autofac;
    using Enums;
    using JobRunner.Common;
    using JobRunner.Common.Interfaces;

    [JobRunner(JobRunnerTypeEnum.AccountSnapshotAndHist)]
    public class AccountSnapshotAndHistJobRunner : Job, IJob
    {
        public AccountSnapshotAndHistJobRunner(IComponentContext context) : base(context)
        {
        }
    }
}

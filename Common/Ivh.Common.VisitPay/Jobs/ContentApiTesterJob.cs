﻿namespace Ivh.Common.VisitPay.Jobs
{
    using Attributes;
    using Autofac;
    using Enums;
    using JobRunner.Common;
    using JobRunner.Common.Interfaces;

    [JobRunner(JobRunnerTypeEnum.ContentApiTester)]
    public class ContentApiTesterJob : Job, IJob
    {
        public ContentApiTesterJob(IComponentContext context) : base(context)
        {
        }
    }
}
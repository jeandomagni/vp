﻿namespace Ivh.Common.VisitPay.Jobs
{
    using Attributes;
    using Autofac;
    using Enums;
    using JobRunner.Common;
    using JobRunner.Common.Interfaces;

    [JobRunner(JobRunnerTypeEnum.QueueConsolidationReminders)]
    public class QueueConsolidationRemindersJobRunner : Job, IJob
    {
        public QueueConsolidationRemindersJobRunner(IComponentContext context) : base(context)
        {
        }
    }
}
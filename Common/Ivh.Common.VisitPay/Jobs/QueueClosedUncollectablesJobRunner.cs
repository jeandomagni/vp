﻿namespace Ivh.Common.VisitPay.Jobs
{
    using Attributes;
    using Autofac;
    using Enums;
    using JobRunner.Common;
    using JobRunner.Common.Interfaces;

    [JobRunner(JobRunnerTypeEnum.QueueClosedUncollectables)]
    public class QueueClosedUncollectablesJobRunner : Job, IJob
    {
        public QueueClosedUncollectablesJobRunner(IComponentContext context) : base(context)
        {
        }
    }
}
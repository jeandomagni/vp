﻿namespace Ivh.Common.VisitPay.Jobs
{
    using Attributes;
    using Autofac;
    using Enums;
    using JobRunner.Common;
    using JobRunner.Common.Interfaces;

    [JobRunner(JobRunnerTypeEnum.VisitStageTruncInsert)]
    public class VisitStageTruncInsertJobRunner : Job, IJob
    {
        public VisitStageTruncInsertJobRunner(IComponentContext context) : base(context)
        {
        }
    }
}

﻿namespace Ivh.Common.VisitPay.Jobs
{
    using Attributes;
    using Autofac;
    using Enums;
    using JobRunner.Common;
    using JobRunner.Common.Interfaces;

    [JobRunner(JobRunnerTypeEnum.QueueAgeVisits)]
    public class QueueAgeVisitsJobRunner : Job, IJob
    {
        public QueueAgeVisitsJobRunner(IComponentContext context) : base(context)
        {
        }
    }
}
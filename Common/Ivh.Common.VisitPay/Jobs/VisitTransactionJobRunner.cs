﻿namespace Ivh.Common.VisitPay.Jobs
{
    using Attributes;
    using Autofac;
    using Enums;
    using JobRunner.Common;
    using JobRunner.Common.Interfaces;

    [JobRunner(JobRunnerTypeEnum.VisitTransaction)]
    public class VisitTransactionJobRunner : Job, IJob
    {
        public VisitTransactionJobRunner(IComponentContext context) : base(context)
        {
        }
    }
}

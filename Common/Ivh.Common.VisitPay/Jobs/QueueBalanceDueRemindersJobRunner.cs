﻿namespace Ivh.Common.VisitPay.Jobs
{
    using Attributes;
    using Autofac;
    using Enums;
    using JobRunner.Common;
    using JobRunner.Common.Interfaces;

    [JobRunner(JobRunnerTypeEnum.QueueBalanceDueReminders)]
    public class QueueBalanceDueRemindersJobRunner : Job, IJob
    {
        public QueueBalanceDueRemindersJobRunner(IComponentContext context) : base(context)
        {
        }
    }
}
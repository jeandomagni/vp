﻿namespace Ivh.Common.VisitPay.Jobs
{
    using Attributes;
    using Autofac;
    using Enums;
    using Ivh.Common.JobRunner.Common.Interfaces;
    using JobRunner.Common;

    [JobRunner(JobRunnerTypeEnum.DeactivateInactiveClientUsers)]
    public class DeactivateInactiveClientUsersJobRunner : Job, IJob
    {
        public DeactivateInactiveClientUsersJobRunner(IComponentContext context) : base(context)
        {
        }
    }
}
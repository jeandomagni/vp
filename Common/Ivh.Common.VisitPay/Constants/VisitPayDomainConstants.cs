﻿namespace Ivh.Common.VisitPay.Constants
{
    public class VisitPayDomainConstants
    {
        public static readonly string[] Domains = new string[] { "ivincihealth.com", "visitpay.com" };
    }
}

﻿namespace Ivh.Common.VisitPay.Constants
{
    public static class AchFileFormats
    {
        public const string Ach = "ACH";
        public const string Csv = "CSV";
    }
}

﻿namespace Ivh.Common.VisitPay.Constants
{
    public static class PaymentInterceptorCacheKeys
    {
        public const string DefaultSuccessful = "Ppi.DefaultSuccessful";
        public const string DelayMs = "Ppi.DelayMs";
        public const string OverrideUrl = "Ppi.OverrideUrl";

        public static class Messaging
        {
            public const string IsActivatedChannel = "Ppi.Channel.IsActivated";
            public const string PaymentComplete = "Ppi.Channel.PaymentComplete";
            public const string PaymentQueued = "Ppi.Channel.PaymentQueued";
        }
    }
}
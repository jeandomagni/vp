﻿namespace Ivh.Common.VisitPay.Constants
{
    public static class DateConstants
    {
        public const int EndOfMonthDay = 31;
        public const int LatestValidPaymentDueDay = 27;
    }
}

﻿namespace Ivh.Common.VisitPay.Constants
{
    public static class ObsoleteDescription
    {
        public const string VisitTransactions = "Visit transactions are being used. Please use another means to get amounts from visits.";
        public const string VisitCurrentBalance = "Visit.CurrentBalance is being used. If you need to show amounts only known to VisitPay, you will need to refactor.";
        public const string VisitTransactionVisitPayPayment = "VisitPay payments are being derived from visit transactions. You will need to refactor.";
        public const string VisitTransactionVisitPayDiscount = "VisitPay discounts are being derived from visit transactions. You will need to refactor.";
        public const string VisitTransactionVisitPayPrincipalPayment = "VisitPay principal payments are being derived from visit transactions. You will need to refactor.";
        public const string VisitTransactionVisitPayInterestPayment = "VisitPay interest payments are being derived from visit transactions. You will need to refactor.";
        public const string JobRunnerMonitors = "Monitors are disabled";
        public const string InterestDeferredFinancePlans = "Interest deferred finance plan behavior is likely to change.  VP3 will not indicate any visit is interest deferred until further notice.";
        public const string VisitStateMachineTest = "Need to refactor test to use new visit statuses and state machine";
        public const string VisitStateMachine = "The new visit state machine no longer supports a status used here. You will need to refactor if this will be used in VP3.";
    }
}
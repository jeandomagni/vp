﻿namespace Ivh.Common.VisitPay.Constants
{
    using Enums;

    public static class Metrics
    {
        public static class Increment
        {
            public static class SignInStatus
            {
                public const string PasswordExpired = nameof(SignInStatus) + "." + nameof(SignInStatusExEnum.PasswordExpired);
                public const string RequiresVerification = nameof(SignInStatus) + "." + nameof(SignInStatusExEnum.RequiresVerification);
                public const string RequiresSecurityQuestions = nameof(SignInStatus) + "." + nameof(SignInStatusExEnum.RequiresSecurityQuestions);
                public const string Success = nameof(SignInStatus) + "." + nameof(SignInStatusExEnum.Success);
                public const string AccountClosed = nameof(SignInStatus) + "." + nameof(SignInStatusExEnum.AccountClosed);
                public const string Failure = nameof(SignInStatus) + "." + nameof(SignInStatusExEnum.Failure);
                public const string LockedOut = nameof(SignInStatus) + "." + nameof(SignInStatusExEnum.LockedOut);
            }

            public static class SignIn
            {
                public const string BruteForceDelay = nameof(SignIn) + "." + nameof(BruteForceDelay);
            }

            public static class General
            {
                public const string Failure = "Failure";
            }

            public static class Logging
            {
                public const string Debug = nameof(Debug);
                public const string Fatal = nameof(Fatal);
                public const string Info = nameof(Info);
                public const string Warn = nameof(Warn);
            }

            public static class Sso
            {
                public const string Registration = "Sso.Registration";
                public const string Verified = "Sso.Verified";

                public static class NewSso
                {
                    public const string Verification = "Sso.New.Verification";
                    public const string Supplantation = "Sso.New.Supplantation";
                }

                public static class FailedSso
                {
                    public const string Decryption = "Sso.Failed.Decryption";
                    public const string TimeStamp = "Sso.Failed.TimeStamp";
                    public const string MissingFields = "Sso.Failed.MissingFields";
                }
            }

            public static class SecurePan
            {
                public static class PaymentMethod
                {
                    public const string FailedValidation = "SecurePAN.PaymentMethod.FailedValidation";
                    public const string CallFailed = "SecurePAN.PaymentMethod.CallFailed";
                    public const string AvsFailed = "SecurePAN.PaymentMethod.AvsFailed";
                    public const string Success = "SecurePAN.PaymentMethod.Success";
                }

                public const string GatewayDown = "TrustCommerce.GatewayDown";
            }

            public static class Hqy
            {
                public const string NotAuthenticated = "Hqy.NotAuthenticated";
                public const string Authenticated = "Hqy.Authenticated";
                public const string AuthenticateFailed = "Hqy.AuthenticateFailed";
                public const string TouDeclined = "Hqy.TouDeclined";
                public const string TouAccepted = "Hqy.TouAccepted";
                public const string WidgetShown = "Hqy.WidgetShown";
                public const string BalanceFailed = "Hqy.BalanceFailed";
            }

            public static class Communication
            {
                public static class Sms
                {
                    public const string StoppedViaSms = "Sms.StoppedViaSms";
                    public const string ShowTerms = "Sms.ShowTerms";
                    public const string ValidAuthCode = "Sms.ValidAuthCode";
                    public const string InvalidAuthCode = "Sms.InvalidAuthCode";
                    public const string AuthCodeSent = "Sms.AuthCodeSent";
                    public const string OptIn = "Sms.OptIn";
                    public const string OptOut = "Sms.OptOut";
                    public const string SendFailures = "Sms.SendFailures";
                }

                public static class Email
                {
                    public const string SendSuccesses = "Email.SendSuccesses";
                }
            }

            public static class Scoring
            {
                public const string ChangeEventExpected = "Scoring.ChangeEventExpected";
                public const string ChangeEventActual = "Scoring.ChangeEventActual";
                public const string ScoreExpected = "Scoring.ScoreExpected";
                public const string ThirdPartyDataInvalidRequestInput = "Scoring.ThirdPartyDataInvalidRequestInput";
                public const string ThirdPartyDataFromAmericaInvalid = "Scoring.ThirdPartyDataFromAmericaInvalid";
                public const string ThirdPartyDataFromDB = "Scoring.ThirdPartyDataFromDB";
                public const string ThirdPartyDataExpected = "Scoring.ThirdPartyDataExpected";
                public const string PtpScores = "Scoring.PtpScores";
                public const string ProtoScores = "Scoring.ProtoScores";

                public const string ScoringBatchId = "Scoring.BatchId";
                public const string ScoringTotal = "Scoring.Total";
                public const string ScoringSuccesses = "Scoring.Successes";
                public const string ScoringFailures = "Scoring.Failures";
                public const string SegmentationBadDebtBatchId = "SegmentationBadDebt.BatchId";
                public const string SegmentationBadDebtTotal = "SegmentationBadDebt.Total";
                public const string SegmentationBadDebtSuccesses = "SegmentationBadDebt.Successes";
                public const string SegmentationBadDebtFailures = "SegmentationBadDebt.Failures";
                public const string SegmentationAccountsReceivableBatchId = "SegmentationAccountsReceivable.BatchId";
                public const string SegmentationAccountsReceivableTotal = "SegmentationAccountsReceivable.Total";
                public const string SegmentationAccountsReceivableSuccesses = "SegmentationAccountsReceivable.Successes";
                public const string SegmentationAccountsReceivableFailures = "SegmentationAccountsReceivable.Failures";
                public const string SegmentationActivePassiveBatchId = "SegmentationActivePassive.BatchId";
                public const string SegmentationActivePassiveTotal = "SegmentationActivePassive.Total";
                public const string SegmentationActivePassiveSuccesses = "SegmentationActivePassive.Successes";
                public const string SegmentationActivePassiveFailures = "SegmentationActivePassive.Failures";
                public const string SegmentationPresumptiveCharityBatchId = "SegmentationPresumptiveCharity.BatchId";
                public const string SegmentationPresumptiveCharityTotal = "SegmentationPresumptiveCharity.Total";
                public const string SegmentationPresumptiveCharitySuccesses = "SegmentationPresumptiveCharity.Successes";
                public const string SegmentationPresumptiveCharityFailures = "SegmentationPresumptiveCharity.Failures";
            }

            public static class VisitPayUser
            {
                public static class NewUser
                {
                    public const string OfflineVisitPayUser = "OfflineVisitPayUser.New";
                    public const string VisitPayUser = "VisitPayUser.New";
                }
            }

            public static class PaymentImport
            {
                public const string PaymentImportInbound = "PaymentImport.Inbound";
                public const string PaymentImportProcessed = "PaymentImport.ImportProcessed";
                public const string PaymentImportPaymentProcessed = "PaymentImport.PaymentProcessed";
            }

            public static class Offline
            {
                public const string AutoPay = "Offline.AutoPay";
                public const string NonAutoPay = "Offline.NonAutoPay";
            }

            public static class GuestPay
            {
                public static class PaymentApi
                {
                    public const string AchPaymentSuccessful = "ACHPaymentAPI_Successful_Payment";
                    public const string AchPaymentFailure = "ACHPaymentAPI_Failure_Payment";
                    public const string CreditCardPaymentSuccessful = "CreditCardPaymentAPI_Successful_Payment";
                    public const string CreditCardPaymentFailure = "CreditCardPaymentAPI_Failure_Payment";
                    public const string GuarantorPaymentLookupSuccessful = "GuarantorPaymentAPILookup_successful";
                    public const string GuarantorPaymentLookupFailure = "GuarantorPaymentAPILookup_Failure";
                    public const string PaymentApiAmountMismatch = "PaymentAPIAmount_Mismatch";
                }

                public static class Payment
                {
                    public const string AchPaymentSuccessful = "ACH_Successful_Payment";
                    public const string AchPaymentFailure = "ACH_Failure_Payment";
                    public const string CreditCardPaymentSuccessful = "Credit_Card_Successful_Payment";
                    public const string CreditCardPaymentFailure = "Credit_Card_Failure_Payment";
                }
            }

            public static class FinancePlan
            {
                public const string ClientExtendedFinancePlanOffer = "ClientExtendedFinancePlanOffer";
                public const string OriginationWithRetailInstallmentContract = "FinancePlan.Originated.WithRetailInstallmentContract";
                public const string OriginationWithoutRetailInstallmentContract = "FinancePlan.Originated.WithoutRetailInstallmentContract";
                public const string FinancePlanIncentiveRefundInterest = "FinancePlan.FinancePlanIncentiveRefundInterest";
            }

            public static class Matching
            {
                public static class RegistryPopulation
                {
                    public const string QueuedChanges = "Matching.Registry.Changes.Queued";
                    public const string ProcessedChanges = "Matching.Registry.Changes.Processed";
                }
                public static class AddInitialMatch
                {
                    public const string AddInitialMatchCount = "Matching.AddInitialMatch.Count";
                    public const string AddInitialMatchMatchOption = "Matching.AddInitialMatch.MatchOption";
                }
                public static class OngoingMatch
                {
                    public const string OngoingMatchMatchOption = "Matching.Ongoing.MatchOption";
                }
                public static class UnMatch
                {
                    public const string UnMatchCount = "Matching.UnMatch.Count";
                }
                public static class Reconciliation
                {
                    public const string Queued = "Matching.Reconciliation.Queued";
                    public const string Processed = "Matching.Reconciliation.Processed";
                }
            }

            public static class PaymentMethods
            {
                public const string NewPaymentMethod = "PaymentMethod.New";
            }

            public static class Trace
            {
                public const string BlackoutStopSuccess = "TraceApi.BlackoutStop.Success";
                public const string BlackoutStopFailure = "TraceApi.BlackoutStop.Failure";
                public const string BlackoutStartSuccess = "TraceApi.BlackoutStart.Success";
                public const string BlackoutStartFailure = "TraceApi.BlackoutStart.Failure";
                public const string BlackoutGetStatSuccess = "TraceApi.BlackoutGetStat.Success";
                public const string BlackoutGetStatFailure = "TraceApi.BlackoutGetStat.Failure";
                public const string BlackoutStopSuccessResponseTime = "TraceApi.BlackoutStop.Success.ResponseTime";
                public const string BlackoutStopFailureResponseTime = "TraceApi.BlackoutStop.Failure.ResponseTime";
                public const string BlackoutStartSuccessResponseTime = "TraceApi.BlackoutStart.Success.ResponseTime";
                public const string BlackoutStartFailureResponseTime = "TraceApi.BlackoutStart.Failure.ResponseTime";
                public const string BlackoutGetStateSuccessResponseTime = "TraceApi.BlackoutGetState.Success.ResponseTime";
                public const string BlackoutGetStateFailureResponseTime = "TraceApi.BlackoutGetState.Failure.ResponseTime";
            }

            public static class Usps
            {
                public static class AddressVerification
                {
                    public const string Success = nameof(Usps) + "." + nameof(AddressVerification) + ".Success";
                    public const string Failure = nameof(Usps) + "." + nameof(AddressVerification) + ".Failure";
                }
            }

            public static class Message
            {
                public const string Published = "UniversalMessage.Published";
                public const string Consumed = "UniversalMessage.Consumed";
                public const string Exception = "UniversalMessage.Exception";

                public static class Timing
                {
                    public const string Queue = "Message.Timing.Queue.Milliseconds";
                    public const string Consume = "Message.Timing.Consume.Milliseconds";
                }
            }
        }
    }
}

﻿namespace Ivh.Common.VisitPay.Constants
{
    public static class SecurePanConstants
    {
        public const string ErrorMessagePartialInvalidCardNumber = "INVALID CREDIT NUMBER";
        public const string ErrorMessagePartialInvalidCardNumber19 = "MORE THAN 16 DIGITS";
        public const string ErrorMessagePartialIncorrectInformation = "PAYMENT INFORMATION INCORRECT";
        public const string ErrorMessagePartialUnableToProcess = "UNABLE TO PROCESS";
        public const string ErrorMessagePartialAvsFailed = "ADDRESS IS NOT VALID";
    }
}
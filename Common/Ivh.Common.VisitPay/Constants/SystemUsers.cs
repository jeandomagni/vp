﻿namespace Ivh.Common.VisitPay.Constants
{
    public static class SystemUsers
    {
        public const int SystemUserId = -1;
        public const string SystemUserName = "sysuser";
    }
}

﻿namespace Ivh.Common.VisitPay.Constants
{
    public static class AchDateTypeId
    {
        public const string EffectiveDate = "1";
        public const string EntryDate = "2";
        public const string ProcessedDate = "3";
        public const string SettlementDate = "4";
        public const string CreatedOnDate = "5";
        public const string ResolveDate = "6";
    }
}

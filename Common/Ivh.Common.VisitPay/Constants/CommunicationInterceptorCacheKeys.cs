﻿namespace Ivh.Common.VisitPay.Constants
{
    public class CommunicationInterceptorCacheKeys
    {
        public const string SmsOverrideUrl = "Sms.OverrideUrl";

        public static class Messaging
        {
            public const string IsActivatedChannel = "Sms.Channel.IsActivated";
        }
    }
}
﻿namespace Ivh.Common.VisitPay.Constants
{
    public static class ClientSettingConstants
    {
        public const string EncryptionKey = "ClientSetting.EncryptionKey";
        public const string AuthKey = "ClientSetting.AuthKey";
        public const int RandomnessLength = 15; //15 to keep backward compatibility "iVinciIsTheBest" = 15 chars
    }
}

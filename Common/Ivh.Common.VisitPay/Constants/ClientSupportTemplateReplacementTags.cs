﻿namespace Ivh.Common.VisitPay.Constants
{
    public class ClientSupportTemplateReplacementTags
    {
        public const string ClientUserFirstName = "%Client User - First Name%";
        public const string GuarantorFullName = "%Guarantor Name%";
        public const string GuarantorFirstName = "%Guarantor First Name%";
        public const string GuarantorNextStatementDate = "%Guarantor Next Statement Date%";
        public const string GuarantorPaymentDay = "%Guarantor Payment Day%";
        public const string GuarantorNextPaymentDate = "%Guarantor Next Payment Date%";
        public const string PatientCurrentBalance = "%Patient Current Balance%";
    }
}

﻿namespace Ivh.Common.VisitPay.Constants
{
    public static class AchInterceptorCacheKeys
    {
        public const string NachaGatewayOverrideUri = "NachaGatewayOverrideUri";
        public const string NachaId = "nachaId";
        public const string NachaSecurityToken = "nachaSecurityToken";
    }
}

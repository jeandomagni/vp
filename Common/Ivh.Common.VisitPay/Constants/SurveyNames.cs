﻿namespace Ivh.Common.VisitPay.Constants
{
    public static class SurveyNames
    {
        public const string FinancePlanCreation = "FinancePlanCreation";
        public const string ManualPayment = "ManualPayment";
        public const string StackedFpCreationChoice = "StackedFpCreationChoice";
        public const string NetPromoterScore = "NetPromoterScore";
        public const string PaperStatementData = "PaperStatementData";
    }
}

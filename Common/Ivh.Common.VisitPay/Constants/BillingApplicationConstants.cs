﻿namespace Ivh.Common.VisitPay.Constants
{
    public static class BillingApplicationConstants
    {
        public const string HB = "HB";
        public const string HBDisplay = "Hospital Services";
        public const string PB = "PB";
        public const string PBDisplay = "Physician Services";
    }
}
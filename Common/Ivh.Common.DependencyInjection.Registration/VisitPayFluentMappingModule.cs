namespace Ivh.Common.DependencyInjection.Registration
{
    using System.Reflection;
    using Common.Data;
    using Domain.AppIntelligence.Entities;
    using Domain.AppIntelligence.Survey.Mappings;
    using Domain.Content.Entities;
    using Domain.Core.KnowledgeBase.Entities;
    using Domain.User.Entities;
    using Domain.Visit.Entities;
    using Domain.Enterprise.Monitoring.Entities;
    using Domain.Monitoring.Entities;
    using Domain.Email.Entities;
    using Domain.Enterprise.Api.Entities;
    using Domain.Eob.Entities;
    using Domain.EventJournal.Entities;
    using Domain.FinanceManagement.Ach.Entities;
    using Domain.FinanceManagement.Entities;
    using Domain.GuestPay.Entities;
    using Domain.HospitalData.VpGuarantor.Entities;
    using Domain.SecureCommunication.GuarantorSupportRequests.Entities;
    using Domain.Settings.Entities;
    using FluentNHibernate.Cfg;
    using FluentNHibernate.Conventions.Helpers;
    using Domain.FileStorage.Entities;
    using Domain.Logging.Entities;
    using Domain.Matching.Entities;

    public class VisitPayFluentMappingModule : IFluentMappingModule
    {
        public virtual void MapAssemblies(MappingConfiguration m)
        {
            //Mapping files should live in the domain!   If a new domain project is added add one of the classes here to register the whole assembly
            m.FluentMappings.Conventions.Setup(a => a.Add(AutoImport.Never()));// this is to prevent name collisions for entities with the same unqualified names in different namespaces.
            m.FluentMappings.AddFromAssembly(Assembly.GetExecutingAssembly());
            m.FluentMappings.AddFromAssembly(Assembly.GetAssembly(typeof(VisitPayUser)));
            m.FluentMappings.AddFromAssembly(Assembly.GetAssembly(typeof(Ivh.Domain.Guarantor.Entities.Guarantor)));
            m.FluentMappings.AddFromAssembly(Assembly.GetAssembly(typeof(Visit)));
            m.FluentMappings.AddFromAssembly(Assembly.GetAssembly(typeof(Communication)));
            m.FluentMappings.AddFromAssembly(Assembly.GetAssembly(typeof(Payment)));
            m.FluentMappings.AddFromAssembly(Assembly.GetAssembly(typeof(PaymentMethod)));
            m.FluentMappings.AddFromAssembly(Assembly.GetAssembly(typeof(GuarantorMatchingLog)));
            m.FluentMappings.AddFromAssembly(Assembly.GetAssembly(typeof(Visit)));
            m.FluentMappings.AddFromAssembly(Assembly.GetAssembly(typeof(SupportRequest)));
            m.FluentMappings.AddFromAssembly(Assembly.GetAssembly(typeof(AchFile)));
            m.FluentMappings.AddFromAssembly(Assembly.GetAssembly(typeof(Monitor)));
            m.FluentMappings.AddFromAssembly(Assembly.GetAssembly(typeof(FileStored)));
            m.FluentMappings.AddFromAssembly(Assembly.GetAssembly(typeof(Gauge)));
            m.FluentMappings.AddFromAssembly(Assembly.GetAssembly(typeof(RandomizedTest)));
            m.FluentMappings.AddFromAssembly(Assembly.GetAssembly(typeof(RandomizedTestGroup)));
            m.FluentMappings.AddFromAssembly(Assembly.GetAssembly(typeof(RandomizedTestGroupMembership)));
            m.FluentMappings.AddFromAssembly(Assembly.GetAssembly(typeof(ClientSetting)));
            m.FluentMappings.AddFromAssembly(Assembly.GetAssembly(typeof(CmsVersion)));
            m.FluentMappings.AddFromAssembly(Assembly.GetAssembly(typeof(SurveyMap)));
            m.FluentMappings.AddFromAssembly(Assembly.GetAssembly(typeof(SurveyQuestionMap)));
            m.FluentMappings.AddFromAssembly(Assembly.GetAssembly(typeof(SurveyResultAnswerMap)));
            m.FluentMappings.AddFromAssembly(Assembly.GetAssembly(typeof(SurveyResultTypeMap)));
            m.FluentMappings.AddFromAssembly(Assembly.GetAssembly(typeof(SurveyResultMap)));
            m.FluentMappings.AddFromAssembly(Assembly.GetAssembly(typeof(KnowledgeBaseCategory)));
            m.FluentMappings.AddFromAssembly(Assembly.GetAssembly(typeof(ClientSetting)));
            m.FluentMappings.AddFromAssembly(Assembly.GetAssembly(typeof(ApplicationRegistration)));
            m.FluentMappings.AddFromAssembly(Assembly.GetAssembly(typeof(LogStackTrace)));
            m.FluentMappings.AddFromAssembly(Assembly.GetAssembly(typeof(JournalEvent)));
            m.FluentMappings.AddFromAssembly(Assembly.GetAssembly(typeof(Ivh.Domain.Rager.Entities.Visit)));
            m.FluentMappings.AddFromAssembly(Assembly.GetAssembly(typeof(MatchRegistry)));
            m.FluentMappings.AddFromAssembly(Assembly.GetAssembly(typeof(ClaimPaymentInformationHeader)));
        }
    }
}
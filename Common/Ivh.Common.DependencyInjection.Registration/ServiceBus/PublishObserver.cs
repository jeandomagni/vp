﻿namespace Ivh.Common.DependencyInjection.Registration.ServiceBus
{
    using System;
    using System.Threading.Tasks;
    using Autofac;
    using Ivh.Common.VisitPay.Constants;
    using Ivh.Domain.Logging.Interfaces;
    using MassTransit;

    public class PublishObserver : IPublishObserver
    {
        private IMetricsProvider _metricsProvider;
        public PublishObserver()
        {
        }

        public Task PostPublish<T>(PublishContext<T> context) where T : class
        {
            if (_metricsProvider == null)
            {
                _metricsProvider = IvinciContainer.Instance.Container().Resolve<IMetricsProvider>();
            }

            _metricsProvider.Increment(Metrics.Increment.Message.Published, tags: $"MessageType:{typeof(T).Name}");
            return Task.FromResult(0);
        }

        public Task PrePublish<T>(PublishContext<T> context) where T : class
        {
            return Task.FromResult(0);
        }

        public Task PublishFault<T>(PublishContext<T> context, Exception exception) where T : class
        {
            return Task.FromResult(0);
        }
    }
}

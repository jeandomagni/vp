﻿namespace Ivh.Common.DependencyInjection.Registration.ServiceBus
{
    using System;
    using System.Collections.Generic;
    using System.Configuration;
    using System.Linq;
    using System.Security.Authentication;
    using System.Security.Cryptography.X509Certificates;
    using Autofac;
    using global::Ivh.Common.Base.Utilities.Extensions;
    using Common.ServiceBus.Common.Messages;
    using EasyNetQ.Management.Client;
    using EasyNetQ.Management.Client.Model;
    using GreenPipes;
    using MassTransit;
    using MassTransit.RabbitMqTransport;
    using MassTransit.Scheduling;
    using Universal;

    public class BusInitializer
    {
        private readonly string _serviceBusConnectionString;
        private readonly string _serviceBusVirtualHost;
        private readonly string _serviceBusUserName;
        private readonly string _serviceBusPassword;
        private readonly bool _serviceBusEnableSsl;
        private readonly string _serviceBusSslCertificatePath;
        private readonly string _serviceBusSslCertificatePassword;
        private readonly string _serviceBusServiceName;
        private readonly string _serviceBusScheduleServiceName;

        public BusInitializer(
            string serviceBusConnectionString,
            string serviceBusVirtualHost,
            string serviceBusUserName,
            string serviceBusPassword,
            bool serviceBusEnableSsl,
            string serviceBusSslCertificatePath,
            string serviceBusSslCertificatePassword,
            string serviceBusServiceName,
            string serviceBusScheduleServiceName
            )
        {
            this._serviceBusConnectionString = serviceBusConnectionString;
            this._serviceBusVirtualHost = serviceBusVirtualHost;
            this._serviceBusUserName = serviceBusUserName;
            this._serviceBusPassword = serviceBusPassword;
            this._serviceBusEnableSsl = serviceBusEnableSsl;
            this._serviceBusSslCertificatePath = serviceBusSslCertificatePath;
            this._serviceBusSslCertificatePassword = serviceBusSslCertificatePassword;
            this._serviceBusServiceName = serviceBusServiceName;
            this._serviceBusScheduleServiceName = serviceBusScheduleServiceName;
        }

        public IBusControl CreateBus(IComponentContext context = null, int? concurrencyLimit = null)
        {
            return this.CreateBus(string.Empty, context, concurrencyLimit);
        }

        private IBusControl CreateBus(IList<Action<IRabbitMqBusFactoryConfigurator, IRabbitMqHost, Func<IRabbitMqBusFactoryConfigurator, IRabbitMqHost, string, bool>>> configureEndpoints, IComponentContext context = null, int? concurrencyLimit = null, Func<IRabbitMqBusFactoryConfigurator, IRabbitMqHost, string, bool> additonalInit = null)
        {
            //TODO: Need to figure out how logging works in the Azure Functions world
            //Log4NetLogger.Use();
#if DEBUG
            string vhost = this.VerifyVirtualHost().ToLowerInvariant();
#else
            string vhost = this._serviceBusVirtualHost.ToLowerInvariant();
#endif

            IBusControl bus = Bus.Factory.CreateUsingRabbitMq(x =>
            {
                string url = string.Concat(this._serviceBusConnectionString.TrimEnd('/'), "/", vhost);

                IRabbitMqHost host = x.Host(new Uri(url), h =>
                {
                    h.Username(this._serviceBusUserName);
                    h.PublisherConfirmation = true;
                    h.Password(this._serviceBusPassword);
                    if (this._serviceBusEnableSsl)
                    {
                        if (this._serviceBusSslCertificatePath.IsNotNullOrEmpty())
                        {
                            h.UseSsl(config =>
                            {
                                config.ServerName = this.GetCommonNameFromCert();
                                config.Protocol = SslProtocols.Tls12;
                                config.CertificatePath = this._serviceBusSslCertificatePath;
                                config.CertificatePassphrase = this._serviceBusSslCertificatePassword;
                            });
                        }
                        else
                        {
                            throw new ConfigurationErrorsException($"ServiceBusEnableSsl is set to true but ServiceBusSslCertificatePath is null or empty.");
                        }
                    }
                });

                x.UseMessageScheduler(new Uri(string.Concat(url.TrimEnd('/'), "/", this._serviceBusScheduleServiceName)));
                x.UseRetry(config => config.Immediate(3));
                configureEndpoints.ForEach(y => y?.Invoke(x, host, additonalInit));

                if (concurrencyLimit.HasValue)
                {
                    x.UseConcurrencyLimit(concurrencyLimit.Value);
                }
            });

            var publishObserver = new PublishObserver();
            bus.ConnectPublishObserver(publishObserver);

            return bus;
        }

        private string GetCommonNameFromCert()
        {
            string commonName = String.Empty;

            try
            {
                X509Certificate cert = new X509Certificate();

                cert.Import(this._serviceBusSslCertificatePath, this._serviceBusSslCertificatePassword, X509KeyStorageFlags.DefaultKeySet);

                // Get the value.
                string[] split = cert.Subject.Split(',');
                foreach (string certSubjectItem in split)
                {
                    if (certSubjectItem.Contains("CN="))
                    {
                        commonName = certSubjectItem.Replace("CN=", "").Trim();
                        break;
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
            
            return commonName;
        }

        public IBusControl CreateBus(string queueName, IComponentContext context = null, int? concurrencyLimit = null, Func<IRabbitMqBusFactoryConfigurator, IRabbitMqHost, string, bool> additonalInit = null)
        {
            IList<Action<IRabbitMqBusFactoryConfigurator, IRabbitMqHost, Func<IRabbitMqBusFactoryConfigurator, IRabbitMqHost, string, bool>>> configurationActions = new List<Action<IRabbitMqBusFactoryConfigurator, IRabbitMqHost, Func<IRabbitMqBusFactoryConfigurator, IRabbitMqHost, string, bool>>>();
            void ConfigureQueue(IRabbitMqBusFactoryConfigurator configurator, IRabbitMqHost host, Func<IRabbitMqBusFactoryConfigurator, IRabbitMqHost, string, bool> additionalConfig)
            {
                queueName = (string.IsNullOrWhiteSpace(queueName)) ? this._serviceBusServiceName : $"{this._serviceBusServiceName}.{queueName}";

                configurator.ReceiveEndpoint(host, queueName, endpoint =>
                {
                    configurator.PrefetchCount = (ushort)16;
                    if (context != null)
                    {
                        endpoint.LoadFrom(context);
                    }

                    /*
                     Queues cannot be altered once created, so we cannot enable this as it will throw an exception on an existing queue. 
                     We will need to drop and recreate the queues or come up with some kind of strategy to gracefully transition from 
                     unprioritized queues to prioritized queues.
                     */
                    //endpoint.EnablePriority(MessagePriorityAttribute.High);
                });

                //whatever else
                additionalConfig?.Invoke(configurator, host, queueName);
            }
            configurationActions.Add(ConfigureQueue);
            return this.CreateBus(configurationActions, context, concurrencyLimit, additonalInit);
        }

        public IBusControl CreateBusWithQueues(IList<Tuple<string, IList<Type>>> queueNameConsumersList, IList<string> universalConsumerAllowedQueues,  IComponentContext context = null, int? concurrencyLimit = null, Func<IRabbitMqBusFactoryConfigurator, IRabbitMqHost, string, bool> additionalInit = null)
        {
            IList<Action<IRabbitMqBusFactoryConfigurator, IRabbitMqHost, Func<IRabbitMqBusFactoryConfigurator, IRabbitMqHost, string, bool>>> configurationActions = new List<Action<IRabbitMqBusFactoryConfigurator, IRabbitMqHost, Func<IRabbitMqBusFactoryConfigurator, IRabbitMqHost, string, bool>>>();

            void ConfigureQueues(IRabbitMqBusFactoryConfigurator configurator, IRabbitMqHost host, Func<IRabbitMqBusFactoryConfigurator, IRabbitMqHost, string, bool> additionalConfig)
            {
                foreach (Tuple<string, IList<Type>> queueNameConsumers in queueNameConsumersList)
                {
                    string queueName = queueNameConsumers.Item1;
                    IList<Type> consumers = queueNameConsumers.Item2;

                    //setup the queue
                    configurator.ReceiveEndpoint(host, queueName, endpoint =>
                    {
                        configurator.PrefetchCount = 16;

                        foreach (Type consumer in consumers.Where(c => !c.IsAbstract))
                        {
                            //get the interface for this consumer of type IConsumer<TMessage>
                            IEnumerable<Type> consumerImplementedInterfaces = consumer.GetInterfaces().Where(i =>
                            {
                                IList<Type> messageTypes = i.GetGenericArguments().ToList();
                                if (!messageTypes.Any())
                                {
                                    return false;
                                }

                                Type genericConsumerType = typeof(IConsumer<>);
                                Type messageType = messageTypes.FirstOrDefault(m =>
                                    typeof(UniversalMessage).IsAssignableFrom(m) || 
                                    typeof(ScheduleMessage).IsAssignableFrom(m) || 
                                    typeof(ScheduleRecurringMessage).IsAssignableFrom(m)
                                );

                                //consumer implements IConsumer<TMessage> where TMessage : PrioritizedMessage or ScheduleMessage
                                Type t = genericConsumerType.MakeGenericType(messageType);
                                return messageType != null && t.IsAssignableFrom(i);
                            });

                            object ConsumerFactory(Type type)
                            {
                                return context.Resolve(type);
                            };

                            //register the consumer on this queue
                            foreach (Type consumerImplementedInterface in consumerImplementedInterfaces)
                            {
                                endpoint.Consumer(consumerImplementedInterface, ConsumerFactory);
                            }
                        }
                    });

                    //whatever else
                    additionalConfig?.Invoke(configurator, host, queueName);
                }
            }

            configurationActions.Add(ConfigureQueues);

            void ConfigureUniversalConsumerQueues(IRabbitMqBusFactoryConfigurator configurator, IRabbitMqHost host, Func<IRabbitMqBusFactoryConfigurator, IRabbitMqHost, string, bool> additionalConfig)
            {
                if (context != null)
                {
                    UniversalConsumerRegistrations universalConsumerRegistrations = new UniversalConsumerRegistrations(configurator, host, context, universalConsumerAllowedQueues);
                    universalConsumerRegistrations.Register();

                    foreach (string queueName in universalConsumerRegistrations.Select(x => x.QueueName).Distinct())
                    {
                        additionalConfig?.Invoke(arg1: configurator, arg2: host, arg3: queueName);
                    }
                }
            }
            configurationActions.Add(ConfigureUniversalConsumerQueues);

            return this.CreateBus(configurationActions, context, concurrencyLimit, additionalInit);
        }


        private string VerifyVirtualHost()
        {
            int retry = 2;
            while (retry > 0)
            {
                try
                {
                    string host = this.GetRabbitMqHost();
                    
                    ManagementClient managementClient = new ManagementClient(host,
                        this._serviceBusUserName,
                        this._serviceBusPassword);

                    User user = managementClient.GetUser(this._serviceBusUserName);

                    Vhost vhost = managementClient.GetVHosts().FirstOrDefault(x => string.Equals(x.Name, this._serviceBusVirtualHost, StringComparison.CurrentCultureIgnoreCase));
                    if (vhost != null)
                    {
                        return vhost.Name;
                    }

                    vhost = managementClient.CreateVirtualHost(this._serviceBusVirtualHost.ToLowerInvariant());
                    managementClient.CreatePermission(new PermissionInfo(user, vhost));

                    return vhost.Name;
                }
                catch (Exception)
                {
                    if (retry > 0)
                    {
                        System.Threading.Thread.Sleep(1000);
                        retry--;
                    }
                    else
                    {
                        throw;
                    }
                }
            }
            throw new ConfigurationErrorsException($"{nameof(BusInitializer)}::{nameof(this.VerifyVirtualHost)}");
        }

        private string GetRabbitMqHost()
        {
            string host = string.Empty;
            try
            {
                Uri rabbitConnectionString = new Uri(this._serviceBusConnectionString);

                if (!string.IsNullOrEmpty(rabbitConnectionString.Host))
                {
                    host = rabbitConnectionString.Host.TrimEnd('/');
                }
                else
                {
                    host = this._serviceBusConnectionString.Replace("rabbitmq://", "http://").TrimEnd('/');
                }
            }
            catch (Exception e)
            {
                //backup plan
                host = this._serviceBusConnectionString.Replace("rabbitmq://", "http://").TrimEnd('/');
                Console.WriteLine(e);
            }
            
            return host;
        }
    }
}
﻿namespace Ivh.Common.DependencyInjection.Registration.ServiceBus.Universal
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using Autofac;
    using Autofac.Core;
    using global::Ivh.Common.Base.Utilities.Extensions;
    using global::Ivh.Common.Base.Utilities.Helpers;
    using Common.ServiceBus.Attributes;
    using Common.ServiceBus.Interfaces;
    using Common.ServiceBus.Common.Messages;
    using Common.VisitPay.Messages.Tests;
    using MassTransit;
    using MassTransit.Internals.Extensions;
    using MassTransit.RabbitMqTransport;

    public class UniversalConsumerRegistrations : List<UniversalConsumerRegistration>
    {
        private readonly IRabbitMqBusFactoryConfigurator _configurator;
        private readonly IRabbitMqHost _rabbitMqHost;
        private readonly IComponentContext _context;
        private readonly IEnumerable<string> _allowedQueues;

        public UniversalConsumerRegistrations(IRabbitMqBusFactoryConfigurator configurator, IRabbitMqHost rabbitMqHost, IComponentContext context, IEnumerable<string> allowedQueues)
        {
            this._configurator = configurator;
            this._rabbitMqHost = rabbitMqHost;
            this._context = context;
            this._allowedQueues = allowedQueues;
        }

        public void AddRegistration(UniversalConsumerAttribute universalConsumerAttribute, Type messageType, IServiceWithType serviceType)
        {
            UniversalConsumerRegistration universalConsumerRegistration = UniversalConsumerRegistration.BuildRegistration(universalConsumerAttribute, messageType, serviceType);
            if (this.All(x => x.RegistrationHash != universalConsumerRegistration.RegistrationHash))
            {
                this.Add(universalConsumerRegistration);
            }
        }

        public void Register()
        {
#if DEBUG
            IEnumerable<Type> GetMessageTypes()
            {
                foreach (Type messageType in typeof(MessageValidationTest).Assembly.GetDerivedTypes<UniversalMessage>(true).Where(x => !x.IsAbstract))
                {
                    yield return messageType;
                }
            }

            Dictionary<Type, List<Tuple<string, Type, bool>>> messageConsumers = GetMessageTypes().Distinct().ToDictionary(k => k, v => new List<Tuple<string, Type, bool>>());
#endif
            // find all of the UniversalConsumer Services
            foreach (IComponentRegistration componentRegistration in this._context.ComponentRegistry.Registrations)
            {
                foreach (IServiceWithType service in componentRegistration.Services
                    .Where(predicate: x => x is IServiceWithType)
                    .Cast<IServiceWithType>()
                    .Where(x => x.ServiceType.IsAssignableTo<IUniversalConsumerService>()).Distinct())
                {
                    foreach (Type universalConsumerService in service.ServiceType.GetInterfaces().Where(x => x.IsAssignableTo<IUniversalConsumerService>() && x.GenericTypeArguments.Length == 1))
                    {
                        InterfaceMapping interfaceMapping = componentRegistration.Activator.LimitType.GetInterfaceMap(universalConsumerService);

                        foreach (MethodInfo consumeMethod in interfaceMapping.TargetMethods
                            .Where(x => x.HasAttribute<UniversalConsumerAttribute>())
                            .OrderByDescending(x => (byte)x.GetCustomAttribute<UniversalConsumerAttribute>().Priority)
                        )
                        {
                            ParameterInfo[] parametersInfo = consumeMethod.GetParameters();
                            if (parametersInfo.Length != 2 || !parametersInfo[0].ParameterType.IsAssignableTo<UniversalMessage>() || !parametersInfo[1].ParameterType.IsAssignableTo<ConsumeContext>())
                            {
                                continue;
                            }

                            Type messageType = parametersInfo[0].ParameterType;
                            UniversalConsumerAttribute universalConsumerAttribute = consumeMethod.GetCustomAttribute<UniversalConsumerAttribute>();
                            this.AddRegistration(universalConsumerAttribute, messageType, service);
                        }
                    }
                }
            }

            foreach (IGrouping<string, UniversalConsumerRegistration> queueGrouping in this.GroupBy(x => x.QueueName))
            {
                if (this._allowedQueues.Any(x => $"{UniversalConsumerRegistration.QueueNamePrefix}.{x}".Equals(queueGrouping.Key, StringComparison.OrdinalIgnoreCase)))
                {
                    this._configurator.PrefetchCount = (ushort)16;
                    this._configurator.ReceiveEndpoint(host: this._rabbitMqHost, queueName: queueGrouping.Key, configure: endpoint =>
                    {
                        queueGrouping.ForEach(x =>
                        {
                            x.Register(endpoint);
#if DEBUG
                            messageConsumers[x.MessageType].Add(new Tuple<string, Type, bool>(queueGrouping.Key, x.ServiceType, true));
#endif
                        });
                    });
                }
                else
                {
                    queueGrouping.ForEach(x =>
                    {
#if DEBUG
                        messageConsumers[x.MessageType].Add(new Tuple<string, Type, bool>(queueGrouping.Key, x.ServiceType, false));
#endif
                    });
                }
            }
#if DEBUG
            foreach (KeyValuePair<Type, List<Tuple<string, Type, bool>>> messageConsumer in messageConsumers.OrderBy(x => x.Key.FullName))
            {
                using (ConsoleColorKeeper.KeepForeground(ConsoleColor.White))
                {
                    Console.Write(messageConsumer.Key.Name);
                    if (!messageConsumer.Value.Any())
                    {
                        using (ConsoleColorKeeper.KeepForeground(ConsoleColor.Yellow))
                        {
                            if (messageConsumer.Key.HasAttribute<UniversalMessageAttribute>())
                            {
                                if (!messageConsumer.Key.GetAttribute<UniversalMessageAttribute>().Consumable)
                                {
                                    Console.WriteLine("\tmessage type marked as not consumable and no consumers found");
                                }
                                else
                                {
                                    Console.WriteLine("\tmessage type marked as consumable but no consumers found");
                                }
                            }
                            else
                            {
                                Console.WriteLine("\tmessage type marked as consumable but no consumers found");
                            }
                        }
                    }
                    else
                    {
                        Console.Write("\tconsumed by ");
                        using (ConsoleColorKeeper.KeepForeground(messageConsumer.Value.Count > 1 ? ConsoleColor.Cyan : ConsoleColor.Green))
                        {
                            Console.Write(messageConsumer.Value.Count(x => x.Item3));
                        }

                        Console.WriteLine(" consumer(s)");
                        foreach (Tuple<string, Type, bool> consumer in messageConsumer.Value.OrderBy(x => x.Item1).ThenBy(x => x.Item2.Name))
                        {
                            if (consumer.Item3)
                            {
                                using (ConsoleColorKeeper.KeepForeground(ConsoleColor.Green))
                                {
                                    Console.WriteLine($"\t{consumer.Item1}:{consumer.Item2.Name}");
                                }
                            }
                            else
                            {
                                using (ConsoleColorKeeper.KeepForeground(ConsoleColor.Yellow))
                                {
                                    Console.WriteLine($"\t{consumer.Item1}:{consumer.Item2.Name} NOT ALLOWED BY CONFIGURATION");
                                }
                            }
                        }
                    }
                }
            }
#endif
        }
    }

    public class UniversalConsumerRegistration
    {
        public const string QueueNamePrefix = "VisitPay::Ivh.Console";

        private readonly UniversalConsumerAttribute _universalConsumerAttribute;
        public Type MessageType { get; private set; }
        public Type ServiceType { get; private set; }

        public string QueueName => $"{QueueNamePrefix}.{this._universalConsumerAttribute.Queue}.{this._universalConsumerAttribute.Priority}";
        public int RegistrationHash => (this.QueueName + this.MessageType.AssemblyQualifiedName + this.ServiceType.AssemblyQualifiedName).GetHashCode();
        public Type ConsumerType => typeof(IConsumer<>).MakeGenericType(this.MessageType);
        public Type FaultConsumerType => typeof(IConsumer<>).MakeGenericType(typeof(Fault<>).MakeGenericType(this.MessageType));
        public Type ConsumeMessageObserverType => typeof(IConsumeMessageObserver<>).MakeGenericType(this.MessageType);
        public Type UniversalConsumerType => typeof(UniversalConsumer<>).MakeGenericType(this.MessageType);


        public static UniversalConsumerRegistration BuildRegistration(UniversalConsumerAttribute universalConsumerAttribute, Type messageType, IServiceWithType serviceWithType)
        {
            return new UniversalConsumerRegistration(universalConsumerAttribute, messageType, serviceWithType);
        }

        private UniversalConsumerRegistration(UniversalConsumerAttribute universalConsumerAttribute, Type messageType, IServiceWithType serviceWithType)
        {
            this._universalConsumerAttribute = universalConsumerAttribute;
            this.MessageType = messageType;
            this.ServiceType = serviceWithType.ServiceType;
        }

        public void Register(IRabbitMqReceiveEndpointConfigurator endpoint)
        {
            void ConfigureEndpoint(IRabbitMqReceiveEndpointConfigurator e, Type t)
            {
                if (t.IsAssignableFrom(c: this.UniversalConsumerType))
                {
                    endpoint.Consumer(consumerType: t, consumerFactory: type => Activator.CreateInstance(this.UniversalConsumerType, this.ServiceType));
                }
            }

            ConfigureEndpoint(endpoint, this.ConsumerType);
            ConfigureEndpoint(endpoint, this.FaultConsumerType);
            //This is not necessary and doesn't work right, but we may want it in the future
            //ConfigureEndpoint(endpoint, this.ConsumeMessageObserverType);
        }
    }
}
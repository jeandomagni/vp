﻿﻿namespace Ivh.Common.DependencyInjection.Registration.ServiceBus.Universal
{
    using System;
    using System.Configuration;
    using System.Diagnostics;
    using System.Threading.Tasks;
    using Application.Logging.Common.Interfaces;
    using Autofac;
    using Base.Interfaces;
    using Base.Utilities;
    using Base.Utilities.Extensions;
    using Base.Utilities.Helpers;
    using Common.ServiceBus.Interfaces;
    using Common.ServiceBus.Common.Messages;
    using DependencyInjection;
    using Domain.Logging.Interfaces;
    using Domain.Settings.Entities;
    using Domain.Settings.Interfaces;
    using MassTransit;
    using Newtonsoft.Json;
    using CorrelationManager = Base.Logging.CorrelationManager;
    using Ivh.Common.VisitPay.Constants;

    public class UniversalConsumer<TMessage> : IUniversalConsumer<TMessage> where TMessage : UniversalMessage//, IUniversalMessage<TMessage>
    {
        private const string MtRedeliveryCount = "MT-Redelivery-Count";
        private const int MtRedeliveryMax = 3;
        private readonly Type _universalConsumerServiceType;

        public UniversalConsumer(Type universalConsumerServiceType)
        {
            this._universalConsumerServiceType = universalConsumerServiceType;
        }

        private ILifetimeScope _lifetimeScope = default(ILifetimeScope);
#if DEBUG
        private static readonly object DebugLock = new object();
#endif
        private static volatile int ConsumerCount = 0;
        protected TService Resolve<TService>() where TService : IApplicationService
        {
            return this._lifetimeScope.Resolve<TService>();
        }

        private Lazy<ILoggingApplicationService> _logger = null;
        protected ILoggingApplicationService Logger
        {
            get
            {
                if (this._logger == null)
                {
                    this._logger = new Lazy<ILoggingApplicationService>(() => this._lifetimeScope.Resolve<ILoggingApplicationService>());
                }
                return this._logger.Value;
            }
        }

        protected TService ResolveKeyed<TService>(object serviceKey) where TService : IApplicationService
        {
            return this._lifetimeScope.ResolveKeyed<TService>(serviceKey);
        }

        protected TService ResolveNamed<TService>(string serviceName) where TService : IApplicationService
        {
            return this._lifetimeScope.ResolveNamed<TService>(serviceName);
        }

        public Task Consume(ConsumeContext<Fault<TMessage>> consumeContext)
        {
            if (consumeContext?.Message?.Message != null)
            {
                TMessage message = consumeContext.Message.Message;
                CorrelationManager.Initialize(message.CorrelationId);
                using (this._lifetimeScope = IvinciContainer.Instance.Container().BeginLifetimeScope())
                {
                    string messageTypeName = typeof(TMessage).Name;
                    string derivedTypeName = this.GetType().Name;
                    string metricsCounterName = $"RabbitMqFault.{messageTypeName}";

                    IMetricsProvider metricsProvider = this._lifetimeScope.Resolve<IMetricsProvider>();
                    metricsProvider.Increment(metricsCounterName);
                    metricsProvider.Increment(Metrics.Increment.Message.Exception, 1, 1, $"MessageType:{typeof(TMessage).Name}", $"ConsumerServiceType:{_universalConsumerServiceType.Name}", $"Consumer:{consumeContext.ReceiveContext.InputAddress.LocalPath}");

                    string json = consumeContext.ToJSON(true, false, true);

                    string logMessage () => $"{derivedTypeName}::Consume<Fault<{messageTypeName}>\n{json}";
                    this.Logger.Fatal(logMessage, Environment.StackTrace);
                }
            }

            return Task.CompletedTask;
        }

        public Task Consume(ConsumeContext<TMessage> consumeContext)
        {
#if DEBUG
            // THIS IS TO SERIALIZE THE MESSAGE PROCESSING IN DEBUG
            lock (DebugLock)
            {
#endif
                using (this._lifetimeScope = IvinciContainer.Instance.Container().BeginLifetimeScope())
                {
                    if (consumeContext?.Message != null && !this.RedeliverIfScheduled(consumeContext))
                    {
                        TMessage message = consumeContext.Message;
                        message.Timing.BeginConsume = DateTime.UtcNow;
                        CorrelationManager.Initialize(message.CorrelationId);

                        IMetricsProvider metricsProvider = this._lifetimeScope.Resolve<IMetricsProvider>();
                        metricsProvider.Gauge(Metrics.Increment.Message.Timing.Queue, message.Timing.Queue.TotalMilliseconds, 1, $"MessageType:{typeof(TMessage).Name}", $"ConsumerServiceType:{_universalConsumerServiceType.Name}", $"Consumer:{consumeContext.ReceiveContext.InputAddress.LocalPath}");

                        try
                        {

                            int counter = 0;
                            int max = 2;
                            for (counter = 0; counter <= max; counter++)
                            {
                                try
                                {
                                    IUniversalConsumerService<TMessage> service = this._lifetimeScope.Resolve(this._universalConsumerServiceType) as IUniversalConsumerService<TMessage>;
                                    if (service == null)
                                    {
                                        throw new ConfigurationErrorsException($"{nameof(UniversalConsumer<TMessage>)} could not resolve IUniversalConsumerService");
                                    }

                                    if (service?.IsMessageValid(message, consumeContext) ?? false)
                                    {
                                        service?.ConsumeMessage(message, consumeContext);
                                    }
                                    else
                                    {
                                        this.LogInvalidMessage(message);
                                    }

                                    counter = max;
                                    break;
                                }
                                catch (Exception ex)
                                {
                                    if (ex.IsOfTypeRecursive<System.Data.SqlClient.SqlException>()
                                        || ex.IsOfTypeRecursive<NHibernate.Exceptions.GenericADOException>()
                                        || ex.IsOfTypeRecursive<NHibernate.TransactionException>())
                                    {
                                        if (counter >= max)
                                        {
                                            throw;
                                        }
                                    }
                                    else
                                    {
                                        throw;
                                    }
                                }
                                finally
                                {
                                    message.Timing.EndConsume = DateTime.UtcNow;
                                    metricsProvider.Gauge(Metrics.Increment.Message.Timing.Consume, message.Timing.Consume.TotalMilliseconds, 1, $"MessageType:{typeof(TMessage).Name}", $"ConsumerServiceType:{_universalConsumerServiceType.Name}", $"Consumer:{consumeContext.ReceiveContext.InputAddress.LocalPath}");
                                    ++ConsumerCount;
                                }
                            }
                            Trace.TraceInformation($"{message.TraceMessage} ({ConsumerCount}) SUCCESS\t{message.Timing.Time} ");

                            
                            metricsProvider.Increment(Metrics.Increment.Message.Consumed, 1, 1, $"MessageType:{typeof(TMessage).Name}", $"ConsumerServiceType:{_universalConsumerServiceType.Name}", $"Consumer:{consumeContext.ReceiveContext.InputAddress.LocalPath}");
                        }
                        catch (Exception e)
                        {
                            using (ConsoleColorKeeper.KeepForeground(ConsoleColor.Red))
                            {
                                Trace.TraceInformation($"{message.TraceMessage} ({++ConsumerCount}) FAILED\t{message.Timing.Time} ");
                            }

                            string retryCount = consumeContext.GetRetryAttempt().ToString();
                            string exceptionMessage = ExceptionHelper.AggregateExceptionToString(e);
                            this.Logger.Fatal(() => $"{this.GetType().Name}::Consume<{typeof(TMessage).Name}> failed for - {message.ErrorMessage}, retry count = {retryCount}, {nameof(Exception)} = {exceptionMessage}");
                            throw;
                        }
                    }
                }
                return Task.CompletedTask;
#if DEBUG
            }
#endif
        }

        private void LogInvalidMessage<T>(T message)
        {
            this.Logger.Warn(() => $"Invalid message {typeof(T).FullName}: {JsonConvert.SerializeObject(message)}");
        }

        private bool RedeliverIfScheduled(ConsumeContext<TMessage> context)
        {
            if (context.Message.IsSchedulable)
            {
                Client client = this._lifetimeScope.Resolve<IClientService>().GetClient();
                DateTime currentClientDateTime = client.TimeZoneHelper.Value.Client.Now;

                if (context.Message.ShouldBeScheduled(client.FriendlyCommunicationDeliveryRange, currentClientDateTime))
                {
                    int? maxAttempts = context.Headers.Get(MtRedeliveryCount, default(int?));
                    if (maxAttempts.HasValue && maxAttempts.Value >= MtRedeliveryMax)
                    {
                        this.Logger.Fatal(() => $"{this.GetType().Name}::Consume<{typeof(TMessage).Name}> Redelivery failed for - {context.Message.CorrelationId}, redelivery count = {maxAttempts.Value}");
                    }
                    else
                    {
                        context.Redeliver(context.Message.GetDeliveryDelay(client.FriendlyCommunicationDeliveryRange, currentClientDateTime));
                        this.Logger.Info(() => $"{this.GetType().Name}::Consume<{typeof(TMessage).Name}> Redelivery between {client.FriendlyCommunicationDeliveryRange} scheduled for - {context.Message.CorrelationId}, client time is {currentClientDateTime.ToString()}");
                    }

                    return true;
                }
            }

            return false;
        }

        public Task PreConsume(ConsumeContext<TMessage> consumeContext)
        {
            return Task.CompletedTask;
        }

        public Task PostConsume(ConsumeContext<TMessage> consumeContext)
        {
            return Task.CompletedTask;
        }

        public Task ConsumeFault(ConsumeContext<TMessage> consumeContext, Exception exception)
        {
            using (this._lifetimeScope = IvinciContainer.Instance.Container().BeginLifetimeScope())
            {
                string logMessage () => $"{nameof(UniversalConsumer<TMessage>)}::ConsumeFault {nameof(ConsumeContext<TMessage>)} = {consumeContext.ToJSON(true, false, true)}, {nameof(Exception)} = {ExceptionHelper.AggregateExceptionToString(exception)}";
                this.Logger.Fatal(logMessage, Environment.StackTrace);
            }
            return Task.CompletedTask;
        }
        public void Dispose()
        {
        }

        public void Consume(TMessage message)
        {
            //nothing for now
#if DEBUG
            Trace.TraceInformation($"{typeof(TMessage).Name} in IMessageConsumer.Consumer");
#endif
        }
    }
}
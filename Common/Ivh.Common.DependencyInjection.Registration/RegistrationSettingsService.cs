﻿namespace Ivh.Common.DependencyInjection.Registration
{
    using System;
    using System.Collections.Generic;
    using System.Configuration;
    using System.Linq;
    using System.Net.Http;
    using System.Reflection;
    using System.Threading;
    using System.Threading.Tasks;
    using Api.Client;
    using Base.Utilities.Extensions;
    using Base.Utilities.Helpers;
    using Configuration;
    using Data.Services;
    using Newtonsoft.Json;
    using VisitPay.Enums;

    public static class RegistrationSettingsService
    {
        public static RegistrationSettings GetRegistrationSettings(bool local = false)
        {
            RegistrationSettings registrationSettings = new RegistrationSettings();

            //clientdataapi
            registrationSettings.ClientDataApiUrl = AppSettingsProvider.Instance.Get("ClientDataApi.Url");
            registrationSettings.ClientDataApiAppId = AppSettingsProvider.Instance.Get("ClientDataApi.AppId");
            registrationSettings.ClientDataApiAppKey = AppSettingsProvider.Instance.Get("ClientDataApi.AppKey");
            registrationSettings.ClientDataApiTimeToLive = int.Parse(AppSettingsProvider.Instance.Get("ClientDataApi.TimeToLive") ?? "15");

            // Client
            registrationSettings.Client = AppSettingsProvider.Instance.Get(nameof(RegistrationSettings.Client));

            //Application
            registrationSettings.Application = Properties.Settings.Default.Application.ParseEnum<ApplicationEnum>(ApplicationEnum.Unknown);
            registrationSettings.ApplicationName = Properties.Settings.Default.ApplicationName;

            //Environment
            registrationSettings.EnvironmentType = IvhEnvironment.CurrentEnvironmentType;

            IDictionary<string, string> applicationSettings = new Dictionary<string, string>(
                AppSettingsProvider.Instance.AppSettings.ToEnumerable().ToDictionary(k => k.Key, v => v.Value),
                StringComparer.OrdinalIgnoreCase
                );

            if (!local)
            {
                foreach (KeyValuePair<string, string> kvp in GetApplicationSettings(registrationSettings))
                {
                    applicationSettings[kvp.Key] = kvp.Value;
                }
            }

            //Service Bus
            registrationSettings.ServiceBusConnectionString = ConnectionStringService.Instance.RabbitMq.Value;
            registrationSettings.RabbitMqUserName = applicationSettings[nameof(RegistrationSettings.RabbitMqUserName)];
            registrationSettings.RabbitMqPassword = applicationSettings[nameof(RegistrationSettings.RabbitMqPassword)];
            registrationSettings.RabbitMqEnableSsl = applicationSettings[nameof(RegistrationSettings.RabbitMqEnableSsl)].ParseBool(true);

            registrationSettings.RabbitMqSslClientCertPath = applicationSettings[nameof(RegistrationSettings.RabbitMqSslClientCertPath)];
            registrationSettings.RabbitMqSslClientCertPassword = applicationSettings[nameof(RegistrationSettings.RabbitMqSslClientCertPassword)];

            string rabbitMqQueueNamePrefix = applicationSettings["RabbitMqQueueNamePrefix"];
            string serviceBusNameInternal = string.Empty;
            Assembly assembly = Assembly.GetEntryAssembly();
            if (assembly != null)
            {
                serviceBusNameInternal = assembly.FullName.Split(','.ToArrayOfOne(), StringSplitOptions.RemoveEmptyEntries).FirstOrDefault()?.Trim();
            }
            string rabbitMqScheduleQueue = applicationSettings["RabbitMqScheduleQueue"];

            registrationSettings.ServiceBusName = $"{rabbitMqQueueNamePrefix}::{serviceBusNameInternal}";
            registrationSettings.ScheduleServiceBusName = $"{rabbitMqQueueNamePrefix}::{rabbitMqScheduleQueue}";

            registrationSettings.RedisEncryptionMaster = applicationSettings[nameof(RegistrationSettings.RedisEncryptionMaster)];
            registrationSettings.RedisEncryptionAuth = applicationSettings[nameof(RegistrationSettings.RedisEncryptionAuth)];
            registrationSettings.RedisMainCacheInstances = int.Parse((applicationSettings[nameof(RegistrationSettings.RedisMainCacheInstances)] ?? "3"));
            registrationSettings.DistributedCacheAuthKeyString = applicationSettings[nameof(RegistrationSettings.DistributedCacheAuthKeyString)];

            registrationSettings.HsFacilityResolutionService = applicationSettings[nameof(RegistrationSettings.HsFacilityResolutionService)].ParseEnum<HsFacilityResolutionServiceEnum>();
            registrationSettings.HsBillingSystemResolutionProvider = applicationSettings[nameof(RegistrationSettings.HsBillingSystemResolutionProvider)].ParseEnum<HsBillingSystemResolutionProviderEnum>();
            registrationSettings.InterestRateConsiderationService = applicationSettings[nameof(RegistrationSettings.InterestRateConsiderationService)].ParseEnum<InterestRateConsiderationServiceEnum>();
            registrationSettings.PaymentAllocationModelType = applicationSettings[nameof(RegistrationSettings.PaymentAllocationModelType)].ParseEnum<PaymentAllocationModelTypeEnum>();
            registrationSettings.DiscountModelType = applicationSettings[nameof(RegistrationSettings.DiscountModelType)].ParseEnum<DiscountModelTypeEnum>();
            registrationSettings.GuarantorEnrollmentProvider = applicationSettings[nameof(RegistrationSettings.GuarantorEnrollmentProvider)].ParseEnum<GuarantorEnrollmentProviderEnum>();
            registrationSettings.RegistrationMatchStringApplicationService = applicationSettings[nameof(RegistrationSettings.RegistrationMatchStringApplicationService)].ParseEnum<RegistrationMatchStringApplicationServiceEnum>();
            registrationSettings.PresumptiveCharityEvaluationProvider = applicationSettings[nameof(RegistrationSettings.PresumptiveCharityEvaluationProvider)].ParseEnum<PresumptiveCharityEvaluationProviderEnum>();

            registrationSettings.FileStorageIncomingFolder = applicationSettings["fileStorage:IncomingFolder"];
            registrationSettings.FileStorageArchiveFolder = applicationSettings["fileStorage:ArchiveFolder"];

            return registrationSettings;
        }

        //TODO: need resilience policy
        private static IDictionary<string, string> GetApplicationSettings(RegistrationSettings registrationSettings)
        {
            HttpResponseMessage response = null;
            const int iterations = 10;
            int pause = 50;
            DateTime start = DateTime.UtcNow;
            for (int i = iterations; i > 0; i--)
            {
                HttpClient client = HttpClientFactory.Create(new ApiHandler(registrationSettings.ClientDataApiAppId, registrationSettings.ClientDataApiAppKey, registrationSettings.ApplicationName));
                client.Timeout = TimeSpan.FromSeconds(registrationSettings.ClientDataApiTimeToLive);
                try
                {
                    response = Task.Run(async () => await client.GetAsync(registrationSettings.ClientDataApiUrl + "api/settings/getsettings")).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        HttpResponseMessage response1 = response;
                        string responseString = Task.Run(async () => await response1.Content.ReadAsStringAsync().ConfigureAwait(false)).Result;
                        return JsonConvert.DeserializeObject<Dictionary<string, string>>(responseString);
                    }
                }
                catch (AggregateException ex) when (ex.InnerException is TaskCanceledException)
                {
                    Thread.Sleep(TimeSpan.FromMilliseconds(pause *= 2));
                }
            }
            throw new ConfigurationErrorsException($"RegistrationSettingsService::GetApplicationSettings Failed to call the Settings API after {iterations} attempt and {(DateTime.UtcNow - start).TotalSeconds} seconds because of response code. WebResponse: {response.AggregateWebResponse(MethodBase.GetCurrentMethod().Name)}");
        }
    }
}
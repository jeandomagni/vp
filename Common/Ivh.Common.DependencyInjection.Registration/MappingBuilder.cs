﻿namespace Ivh.Common.DependencyInjection.Registration
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Linq.Expressions;
    using System.Reflection;
    using Application.Base.Common.Interfaces;
    using Application.Content.Mappings;
    using AutoMapper;
    using AutoMapper.Configuration;
    using Common.VisitPay.Attributes;

    public class MappingBuilder : IDisposable
    {
        public readonly MapperConfigurationExpression Config;

        public MappingBuilder()
        {
            this.Config = new MapperConfigurationExpression
            {
                CreateMissingTypeMaps = false
            };
        }

        public void Dispose()
        {
            this.Config.ForAllMaps((map, expression) => { map.MaxDepth = 3; });

            Mapper.Initialize(this.Config);
        }

        public MappingBuilder WithBase()
        {
            this.WithHospitalMappings()
                .WithProviderMappings()
                .WithDomainMappings()
                .WithApplicationMappings();

            return this;
        }

        public MappingBuilder WithApplicationMappings()
        {
            this.Config.AddProfile<AutomapperProfile>();
            this.Config.AddProfile<Application.AppIntelligence.Mappings.AutomapperProfile>();
            this.Config.AddProfile<Application.Core.Mappings.AutomapperProfile>();
            this.Config.AddProfile<Application.Email.Mappings.AutomapperProfile>();
            this.Config.AddProfile<Application.Enterprise.Api.Mappings.AutomapperProfile>();
            this.Config.AddProfile<Application.EventJournal.Mappings.AutomapperProfile>();
            this.Config.AddProfile<Application.FileStorage.Mappings.AutomapperProfile>();
            this.Config.AddProfile<Application.FinanceManagement.Mappings.AutomapperProfile>();
            this.Config.AddProfile<Application.GuestPay.Mappings.AutomapperProfile>();
            this.Config.AddProfile<Application.SecureCommunication.Mappings.AutomapperProfile>();

            return this;
        }

        public MappingBuilder WithDomainMappings()
        {
            this.Config.AddProfile<Domain.Core.Mappings.AutomapperProfile>();
            this.Config.AddProfile<Domain.Email.Mappings.AutomapperProfile>();
            this.Config.AddProfile<Domain.FinanceManagement.Mappings.AutomapperProfile>();
            this.Config.AddProfile<Domain.Matching.Mappings.AutomapperProfile>();

            return this;
        }

        public MappingBuilder WithHospitalMappings()
        {
            this.Config.AddProfile<Domain.HospitalData.Eob.Mappings.AutomapperProfile>();
            this.Config.AddProfile<Domain.HospitalData.HsGuarantor.Mappings.AutomapperProfile>();
            this.Config.AddProfile<Domain.HospitalData.Visit.Mappings.AutomapperProfile>();
            this.Config.AddProfile<Application.HospitalData.Mappings.AutomapperProfile>();

            return this;
        }

        public MappingBuilder WithProviderMappings()
        {
            this.WithProfile<Provider.Enrollment.Intermountain.Mappings.AutomapperProfile>();

            return this;
        }

        public MappingBuilder WithProfile<T>() where T : Profile
        {
            this.Config.AddProfile(typeof(T));

            return this;
        }

        public MappingBuilder WithMasking(string[] destinationNamespaces, Action<object, object, IList<PropertyInfo>, IList<string>> action)
        {
            Type maskInterface = typeof(IMask);
            IList<Type> selfMapsCreated = new List<Type>();
            foreach (ITypeMapConfiguration typeMap in this.Config.Profiles.SelectMany(x => x.TypeMapConfigs))
            {
                if (!maskInterface.IsAssignableFrom(typeMap.SourceType))
                {
                    continue;
                }

                if (selfMapsCreated.Contains(typeMap.SourceType))
                {
                    continue;
                }

                this.Config.CreateMap(typeMap.SourceType, typeMap.SourceType);
                selfMapsCreated.Add(typeMap.SourceType);
            }

            this.Config.ForAllMaps((typeMap, e) =>
            {
                string destinationTypeNamespace = typeMap.DestinationType.Namespace ?? string.Empty;
                
                if (typeMap.DestinationType != typeMap.SourceType && !destinationNamespaces.Any(x => destinationTypeNamespace.StartsWith(x)))
                {
                    return;
                }

                IList<string> destinationFieldsToMask = GetMaskedDestinationMappings(typeMap).ToList();
                if (destinationFieldsToMask.Any())
                {
                    // register AfterMap if: property on the SourceType implements any IMask
                    // ex: StatementVisitDto.Visit
                    IList<PropertyInfo> childProperties = new List<PropertyInfo>();
                    List<PropertyInfo> children = typeMap.SourceType.GetProperties().Where(x => maskInterface.IsAssignableFrom(x.PropertyType)).ToList();
                    foreach (PropertyInfo child in children)
                    {
                        childProperties.Add(child);
                    }

                    // register AfterMap if: SourceType implements any IMask
                    // ex: VisitDto : IMaskConsolidated
                    if (maskInterface.IsAssignableFrom(typeMap.SourceType) || childProperties.Any())
                    {
                        Expression<Action<object, object>> exp = (src, dest) => action(src, dest, childProperties, destinationFieldsToMask);
                        typeMap.AddAfterMapAction(exp);
                    }
                }
            });

            return this;
        }

        /// <summary>
        /// find all destination mappings that map from [Mask]
        /// </summary>
        private static IEnumerable<string> GetMaskedDestinationMappings(TypeMap typeMap)
        {
            IEnumerable<PropertyMap> applicable = typeMap.GetPropertyMaps().Where(x => x.SourceMember != null).ToList();
            foreach (PropertyMap field in applicable)
            {
                if (field.CustomExpression != null)
                {
                    MemberExpression me = field.CustomExpression.Body as MemberExpression;
                    if (me?.Member is PropertyInfo pi && pi.GetCustomAttribute<MaskAttribute>() != null)
                    {
                        yield return field.DestinationProperty.Name;
                    }
                }
                else if (field.SourceMember.GetCustomAttribute<MaskAttribute>() != null)
                {
                    yield return field.DestinationProperty.Name;
                }
            }
        }
    }
}
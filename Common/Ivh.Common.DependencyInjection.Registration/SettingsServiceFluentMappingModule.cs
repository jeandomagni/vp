﻿namespace Ivh.Common.DependencyInjection.Registration
{
    using System.Reflection;
    using Common.Data;
    using Domain.Enterprise.Api.Entities;
    using Domain.Logging.Entities;
    using Domain.Settings.Entities;
    using FluentNHibernate.Cfg;
    using FluentNHibernate.Conventions.Helpers;

    public class SettingsServiceFluentMappingModule : IFluentMappingModule
    {
        public virtual void MapAssemblies(MappingConfiguration m)
        {
            //Mapping files should live in the domain!   If a new domain project is added add one of the classes here to register the whole assembly
            m.FluentMappings.Conventions.Setup(a => a.Add(AutoImport.Never()));// this is to prevent name collisions for entities with the same unqualified names in different namespaces.
            m.FluentMappings.AddFromAssembly(Assembly.GetExecutingAssembly());
            m.FluentMappings.AddFromAssembly(Assembly.GetAssembly(typeof(ClientSetting)));
            m.FluentMappings.AddFromAssembly(Assembly.GetAssembly(typeof(ApplicationRegistration)));
            m.FluentMappings.AddFromAssembly(Assembly.GetAssembly(typeof(LogStackTrace)));
        }
    }
}
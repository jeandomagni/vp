﻿namespace Ivh.Common.DependencyInjection.Registration.Register
{
    using Application.Core;
    using Application.Core.Common.Interfaces;
    using Autofac;
    using Autofac.Integration.Mvc;
    using Domain.User.Entities;
    using Domain.User.Interfaces;
    using Domain.User.Services;
    using Microsoft.AspNet.Identity;
    using Microsoft.Owin.Security.DataProtection;
    using Owin;
    using Provider.User;
    using Web.Interfaces;
    using Web.Services;

    public static class Mvc
    {
        public static void Register(ContainerBuilder builder, IAppBuilder appBuilder)
        {
            // Register your MVC controllers.
            //builder.RegisterControllers(typeof(AccountController).Assembly);

            //// OPTIONAL: Register model binders that require DI.
            //builder.RegisterModelBinders(Assembly.GetExecutingAssembly());
            //builder.RegisterModelBinderProvider();

            // OPTIONAL: Register web abstractions like HttpContextBase.
            builder.RegisterModule<AutofacWebTypesModule>();

            //// OPTIONAL: Enable property injection in view pages.
            //builder.RegisterSource(new ViewRegistrationSource());

            //// OPTIONAL: Enable property injection into action filters.
            //builder.RegisterFilterProvider();

            // REGISTER OWIN DEPENDENCIES
            builder.RegisterType<VisitPayUserRepository<VisitPayUser>>().As<IUserStore<VisitPayUser>>().InstancePerRequest();
            builder.RegisterType<VisitPayUserRepository<VisitPayUser>>().As<IUserTokenProvider<VisitPayUser, string>>().InstancePerRequest();
            builder.RegisterType<VisitPayUserRepository<VisitPayUser>>().As<IVisitPayUserRepository>().InstancePerRequest();
            builder.RegisterType<VisitPayUserService>().AsSelf().InstancePerRequest();
            builder.RegisterType<VisitPaySignInService>().AsSelf().InstancePerRequest();
            builder.RegisterType<IdentityEmailService>().As<IIdentityEmailService>().InstancePerRequest();
            builder.RegisterType<IdentitySmsService>().As<IIdentitySmsService>().InstancePerRequest();
            builder.Register(c => appBuilder.GetDataProtectionProvider()).InstancePerRequest();
            builder.RegisterType<VisitPayUserApplicationService>().As<IVisitPayUserApplicationService>().InstancePerRequest();
            builder.RegisterType<VisitPayUserApplicationLightweightService>().As<IVisitPayUserApplicationLightweightService>().InstancePerRequest();
            builder.RegisterType<SecurityStampValidatorApplicationService>().As<ISecurityStampValidatorApplicationService>().InstancePerRequest();
            builder.RegisterType<VisitPayUserJournalEventApplicationService>().As<IVisitPayUserJournalEventApplicationService>().InstancePerRequest();

            builder.RegisterType<BruteForceDelayService>().As<IBruteForceDelayService>().InstancePerRequest();
        }
    }
}
﻿namespace Ivh.Common.DependencyInjection.Registration.Register
{
    using Application.Core;
    using Application.Core.ApplicationServices;
    using Application.Core.Common.Interfaces;
    using Autofac;
    using AutofacModules;
    using Cache;
    using Common.Base.Utilities.Helpers;
    using Common.Data;
    using Common.VisitPay.Enums;
    using Domain.Content.Modules;
    using Domain.Email.Interfaces;
    using Domain.FinanceManagement.Ach.Interfaces;
    using Domain.FinanceManagement.Interfaces;
    using Domain.Settings.Interfaces;
    using Domain.Settings.Services;
    using Extensions;
    using Provider.Content.Modules;
    using Provider.Email.SendGrid;
    using Provider.FinanceManagement.Ach;
    using Provider.FinanceManagement.TrustCommerce;
    using Provider.Pdf;
    using ResiliencePolicies;
    using ResiliencePolicies.Interfaces;
    using ResiliencePolicies.Policies;
    using Session;

    public static class VisitPay
    {
        public static void Register(ContainerBuilder builder, RegistrationSettings registrationSettings, bool registerAllFluentMappings)
        {
            builder.RegisterType<WebHelper>().As<IWebHelper>();
            builder.RegisterType<PaymentConfigurationService>().As<IPaymentConfigurationService>();

            builder.RegisterAssemblyModules(typeof(Application.Base.Modules.Module).Assembly);
            builder.RegisterAssemblyModules(typeof(Domain.Base.Modules.Module).Assembly);
            //TODO:
            //builder.RegisterAssemblyModules(typeof(Provider.Base.Modules.Module).Assembly);

            builder.RegisterAssemblyModules(typeof(Application.Logging.Modules.Module).Assembly);
            builder.RegisterAssemblyModules(typeof(Domain.Logging.Modules.Module).Assembly);
            builder.RegisterAssemblyModules(typeof(Provider.Logging.Modules.Module).Assembly);

            builder.RegisterAssemblyModules(typeof(Application.Core.Modules.Module).Assembly);
            builder.RegisterAssemblyModules(typeof(Domain.Core.Modules.Module).Assembly);
            builder.RegisterAssemblyModules(typeof(Provider.Core.Modules.Module).Assembly);

            //builder.RegisterAssemblyModules(typeof(Application.Guarantor.Modules.Module).Assembly);
            builder.RegisterAssemblyModules(typeof(Domain.Guarantor.Modules.Module).Assembly);
            builder.RegisterAssemblyModules(typeof(Provider.Guarantor.Modules.Module).Assembly);

            builder.RegisterAssemblyModules(typeof(Application.Email.Modules.Module).Assembly);
            builder.RegisterAssemblyModules(typeof(Domain.Email.Module.Module).Assembly);
            builder.RegisterAssemblyModules(typeof(Provider.Email.Modules.Module).Assembly);
            builder.RegisterAssemblyModules(typeof(Provider.Email.SendGrid.Modules.Module).Assembly);

            builder.RegisterAssemblyModules(typeof(Application.FinanceManagement.Modules.Module).Assembly);
            builder.RegisterAssemblyModules(typeof(Provider.FinanceManagement.Modules.Module).Assembly);
            builder.RegisterAssemblyModules(typeof(Provider.FinanceManagement.Ach.Modules.Module).Assembly);
            builder.RegisterAssemblyModules(typeof(Provider.FinanceManagement.TrustCommerce.Modules.Module).Assembly);

            builder.RegisterAssemblyModules(typeof(Application.Qat.Modules.Module).Assembly);

            builder.RegisterAssemblyModules(typeof(Application.SecureCommunication.Modules.Module).Assembly);
            builder.RegisterAssemblyModules(typeof(Domain.SecureCommunication.Module).Assembly);
            builder.RegisterAssemblyModules(typeof(Provider.SecureCommunication.Module).Assembly);

            builder.RegisterAssemblyModules(typeof(Application.Monitoring.Modules.Module).Assembly);
            builder.RegisterAssemblyModules(typeof(Domain.Monitoring.Modules.Module).Assembly);
            builder.RegisterAssemblyModules(typeof(Provider.Monitoring.Modules.Module).Assembly);

            builder.RegisterAssemblyModules(typeof(Ivh.Application.Enterprise.Monitoring.Modules.Module).Assembly);
            builder.RegisterAssemblyModules(typeof(Domain.Enterprise.Monitoring.Modules.Module).Assembly);
            builder.RegisterAssemblyModules(typeof(Provider.Enterprise.Monitoring.Modules.Module).Assembly);

            builder.RegisterAssemblyModules(typeof(Application.Enterprise.Api.Modules.Module).Assembly);
            builder.RegisterAssemblyModules(typeof(Domain.Enterprise.Api.Modules.Module).Assembly);
            builder.RegisterAssemblyModules(typeof(Provider.Enterprise.Api.Modules.Module).Assembly);

            builder.RegisterAssemblyModules(typeof(Application.Content.Modules.Module).Assembly);
            builder.RegisterModule(new ContentModule(registrationSettings));

            builder.RegisterAssemblyModules(typeof(Application.AppIntelligence.Modules.Module).Assembly);
            builder.RegisterAssemblyModules(typeof(Domain.AppIntelligence.Modules.Module).Assembly);
            builder.RegisterAssemblyModules(typeof(Provider.AppIntelligence.Modules.Module).Assembly);

            //TODO: break this out
            if (registerAllFluentMappings)
            {
                builder.RegisterType<VisitPayFluentMappingModule>().As<IFluentMappingModule>();
            }

            builder.RegisterType<IvinciMemoryCache>().As<IMemoryCache>().As<ICache>();
            builder.RegisterType<MemoryCacheProvider>().As<ICacheProvider>();

            // resilience policies
            builder.RegisterType<HttpRequestPolicy>().As<IHttpRequestPolicy>();
            builder.RegisterType<SimpleCircuitBreakerPolicy>().As<ISimpleCircuitBreakerPolicy>();

            //Using variables like this because I'm trying to keep the applicationSettingsService reference out of autofac registration so it doesnt hold on to it
            string encryptionMaster = registrationSettings.RedisEncryptionMaster;
            string encryptionAuth = registrationSettings.RedisEncryptionAuth;
            int redisInstances = registrationSettings.RedisMainCacheInstances;
            string distributedCacheAuthKeyString = registrationSettings.DistributedCacheAuthKeyString;
            string client = registrationSettings.Client;

            //Cant use injected ApplicationSettingsSerivce for this as it will cause deadlocks since the SettingsProvider uses the distributed cache
            //This also means that these values are static for these constructors.
            builder.RegisterType<RedisCache>().As<IDistributedCache>().WithParameters(new[]
            {
                new NamedParameter("encryptionKey", encryptionMaster),
                new NamedParameter("authKeystring", encryptionAuth),
                new NamedParameter("cacheInstances", redisInstances),
                new NamedParameter("distributedCacheAuthKeyString", distributedCacheAuthKeyString),
                new NamedParameter("client", client)
            });


            builder.RegisterType<InstanceCache>().As<IInstanceCache>().InstanceWebPerRequestElsePerLifetimeScope();

            builder.RegisterType<SessionFacadeBase>().As<ISessionFacade>();
            builder.RegisterType<TuesPechkinPdfConverter>().As<IPdfConverter>();
            builder.RegisterType<SendGridEmailServiceProvider>().As<IEmailServiceProvider>();

            builder.RegisterType<GuarantorApplicationService>().As<IGuarantorApplicationService>();

            RegisterPaymentProvider(builder);

            builder.RegisterModule(new SettingsModule());
            builder.RegisterModule(new AppIntelligenceModule());
            builder.RegisterModule(new HospitalDataModule(registrationSettings));
            builder.RegisterModule(new FinanceManagementModule(registrationSettings));
            builder.RegisterModule(new CommunicationModule());

            if (registrationSettings.Application == ApplicationEnum.QATools)
            {
                builder.RegisterModule(new QatModule());
            }

            builder.RegisterModule(new CoreModule(registrationSettings));
            builder.RegisterModule(new UserModule());
            builder.RegisterModule(new GuarantorModule());
            builder.RegisterModule(new VisitModule());
            builder.RegisterModule(new EobModule());

            builder.RegisterModule(new GuestPayModule());
            builder.RegisterModule(new FileStorageModule());

            builder.RegisterType<VisitCreationApplicationService>().As<IVisitCreationApplicationService>();
            builder.RegisterType<ChangeSetApplicationService>().As<IChangeSetApplicationService>();

            builder.RegisterType<SystemExceptionApplicationService>().As<ISystemExceptionApplicationService>();

            builder.RegisterModule(new MonitoringModule());
            builder.RegisterModule(new SchedulingModule());
            builder.RegisterModule(new PowershellModule());

            builder.RegisterModule(new MatchingModule());

            builder.RegisterModule(new ContentProviderModule(registrationSettings));
        }

        public static void RegisterPaymentProvider(ContainerBuilder builder)
        {
            builder.RegisterType<TrustCommercePaymentProvider>().As<IPaymentProvider>();
            builder.RegisterType<TrustCommercePaymentProvider>().As<IPaymentBatchOperationsProvider>();
            builder.RegisterType<TrustCommercePaymentProvider>().As<Domain.GuestPay.Interfaces.IPaymentProvider>();
            builder.RegisterType<TrustCommercePaymentProvider>().As<IPaymentReversalProvider>();

            builder.RegisterType<AchProvider>().As<IAchProvider>();

            builder.RegisterType<NachaGateway>().As<INachaGateway>();
        }
    }
}
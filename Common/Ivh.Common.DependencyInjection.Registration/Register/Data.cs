﻿namespace Ivh.Common.DependencyInjection.Registration.Register
{
    using Autofac;
    using AutofacModules;
    using Common.Data;
    using Domain.User.Entities;
    using System;
    using Domain.Content.Modules;
    using Domain.Enterprise.Modules;
    using Domain.EventJournal.Modules;
    using Domain.Guarantor.Modules;
    using Domain.HospitalData.Modules;
    using Domain.Logging.Modules;
    using Domain.Matching.Modules;
    using Domain.Qat.Modules;
    using Domain.Settings.Modules;
    using Domain.User.Modules;
    using Domain.Visit.Modules;
    using Connections = Ivh.Common.Data.Connection.Connections;
    using Ivh.Domain.Eob.Modules;

    public static class Data
    {
        public static ContainerBuilder Register(ContainerBuilder builder, RegistrationSettings registrationSettings)
        {
            builder.RegisterType<UnitOfWork>().As<IUnitOfWork>();
            builder.RegisterModule(new SystemHealthModule());
            
            return builder;
        }

        public static ContainerBuilder WithFluentMappingModules(this ContainerBuilder builder, params Type[] fluentMappingModules)
        {
            foreach (Type fluentMappingModule in fluentMappingModules)
            {
                builder.RegisterType(fluentMappingModule).As<IFluentMappingModule>();
            }
            return builder;
        }

        public static ContainerBuilder WithQaToolsFluentMappingModules(this ContainerBuilder builder)
        {
            builder.WithFluentMappingModules(typeof(QatFluentMappingModule));

            return builder;
        }

        /* TODO: VP-4424 - will supplant this implementation
        public static ContainerBuilder AddClientDataApiFluentMappingModules(this ContainerBuilder builder)
        {
            builder.WithFluentMappingModules(
                typeof(ContentFluentMappingModule),
                typeof(EnterpriseFluentMappingModule),
                typeof(EventJournalFluentMappingModule),
                typeof(LoggingFluentMappingModule),
                typeof(SettingsFluentMappingModule),
                typeof(HospitalDataFluentMappingModule),
                typeof(MatchingFluentMappingModule),
                typeof(EobFluentMappingModule)
                );

            return builder;
        }
        */

        public static ContainerBuilder AddVisitPayDatabase(this ContainerBuilder builder)
        {
            builder.RegisterSessionFactory<Connections.VisitPay>(typeof(VisitPayUser));
            return builder;
        }
        public static ContainerBuilder AddLoggingDatabase(this ContainerBuilder builder)
        {
            builder.RegisterSessionFactory<Connections.Logging>(typeof(VisitPayUser));
            return builder;
        }

        public static ContainerBuilder AddGuestPayDatabase(this ContainerBuilder builder)
        {
            builder.RegisterSessionFactory<Connections.GuestPay>(typeof(VisitPayUser));
            return builder;
        }

        public static ContainerBuilder AddStorageDatabase(this ContainerBuilder builder)
        {
            builder.RegisterSessionFactory<Connections.Storage>(typeof(VisitPayUser));
            return builder;
        }

        public static ContainerBuilder AddCdiEtlDatabase(this ContainerBuilder builder)
        {
            builder.RegisterSessionFactory<Connections.CdiEtl>(typeof(VisitPayUser));
            return builder;
        }
        
        public static ContainerBuilder AddEnterpriseDatabase(this ContainerBuilder builder)
        {
            builder.RegisterSessionFactory<Connections.Enterprise>(typeof(VisitPayUser));
            return builder;
        }
    }
}

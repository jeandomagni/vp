﻿namespace Ivh.Common.DependencyInjection.Registration.Register
{
    using System.Net;
    using Api.Http;
    using Autofac;
    using Autofac.Extras.AttributeMetadata;
    using AutofacModules;
    using Common.Data.Services;
    using Common.VisitPay.Enums;
    using Common.VisitPay.Registration;
    using Domain.Logging.Interfaces;
    using Domain.Settings.Entities;
    using MassTransit.Log4NetIntegration.Logging;
    using Newtonsoft.Json;
    using Client = Common.VisitPay.Registration.Client;

    public static class Base
    {
        static Base()
        {
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls;
        }

        public static void Register(ContainerBuilder builder, RegistrationSettings registrationSettings, bool registerServiceBus = true)
        {
            Log4NetLogger.Use();
            SslConfigure.Configure();

            JsonConvert.DefaultSettings = () =>
            {
                JsonSerializerSettings settings = new JsonSerializerSettings
                {
                    Formatting = Formatting.None,
                    ReferenceLoopHandling = ReferenceLoopHandling.Ignore
                };
                return settings;
            };

            //Using variables like this because I'm trying to keep the this.ApplicationSettingsService reference out of autofac registration so it doesnt hold on to it

            builder.RegisterInstance(ConnectionStringService.Instance);
            builder.RegisterInstance(new ApplicationSetting<ApplicationEnum>(registrationSettings.Application));
            if (registerServiceBus)
            {
                ServiceBus.Register(builder, registrationSettings);
            }
            builder.RegisterModule<AttributedMetadataModule>();
            builder.RegisterModule(new DateTimeModule());
            builder.RegisterModule(new LoggingModule());
            builder.AddLoggingDatabase();
            builder.RegisterModule(new EventJournalModule(registrationSettings));

            RegisterSingleValues(builder, registrationSettings);
        }

        private static void RegisterSingleValues(ContainerBuilder builder, RegistrationSettings registrationSettings)
        {
            // this section registers singleton instances of single values
            builder.RegisterInstance(new Client(registrationSettings.Client));
            builder.RegisterInstance(new RedisEncryptionAuth(registrationSettings.RedisEncryptionAuth));
            builder.RegisterInstance(new RedisEncryptionMaster(registrationSettings.RedisEncryptionMaster));
            builder.RegisterInstance(new FileStorageIncomingFolder(registrationSettings.FileStorageIncomingFolder));
            builder.RegisterInstance(new FileStorageArchiveFolder(registrationSettings.FileStorageArchiveFolder));

            builder.Register((c, p) =>
            {
                IComponentContext componentContext = c.Resolve<IComponentContext>();
                return new MetricIncrement(x =>
                {
                    IMetricsProvider metricsProvider = componentContext.Resolve<IMetricsProvider>();
                    metricsProvider.Increment(x);
                });
            }).SingleInstance();
        }
    }
}
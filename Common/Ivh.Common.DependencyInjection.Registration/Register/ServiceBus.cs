﻿namespace Ivh.Common.DependencyInjection.Registration.Register
{
    using System;
    using System.Collections.Generic;
    using Autofac;
    using MassTransit;
    using MassTransit.RabbitMqTransport;
    using Registration.ServiceBus;

    public static class ServiceBus
    {
        public static IBusControl Register(ContainerBuilder builder,
            RegistrationSettings registrationSettings, 
            IComponentContext context = null, 
            int? concurrencyLimit = null)
        {
            return Register(builder, registrationSettings, string.Empty, context, concurrencyLimit);
        }

        public static IBusControl Register(
            ContainerBuilder builder,
            RegistrationSettings registrationSettings,
            IList<Tuple<string, IList<Type>>> queueNameConsumersList,
            IList<string> universalConsumerAllowedQueues,
            IComponentContext context = null,
            int? concurrencyLimit = null)
        {
            BusInitializer busInitializer = CreateBusInitializer(registrationSettings);
            IBusControl busControl = busInitializer.CreateBusWithQueues(queueNameConsumersList, universalConsumerAllowedQueues, context, concurrencyLimit);
            builder.RegisterInstance(busControl).SingleInstance().As<IBusControl>().As<IBus>();
            builder.RegisterType<Common.ServiceBus.Bus>().As<Common.ServiceBus.Interfaces.IBus>();
            return busControl;
        }

        public static IBusControl Register(
            ContainerBuilder builder,
            RegistrationSettings registrationSettings,
            string queueName, 
            IComponentContext context = null, 
            int? concurrencyLimit = null,
            Func<IRabbitMqBusFactoryConfigurator, IRabbitMqHost, string, bool> additionalInit = null)
        {
            BusInitializer busInitializer = CreateBusInitializer(registrationSettings);
            IBusControl busControl = busInitializer.CreateBus(queueName, context, concurrencyLimit, additionalInit);
            builder.RegisterInstance(busControl).SingleInstance().As<IBusControl>().As<IBus>();
            builder.RegisterType<Common.ServiceBus.Bus>().As<Common.ServiceBus.Interfaces.IBus>();
            return busControl;
        }

        private static BusInitializer CreateBusInitializer(RegistrationSettings registrationSettings)
        {
            return new BusInitializer(
                registrationSettings.ServiceBusConnectionString,
                registrationSettings.ServiceBusVirtualHost,
                registrationSettings.RabbitMqUserName,
                registrationSettings.RabbitMqPassword,
                registrationSettings.RabbitMqEnableSsl,
                registrationSettings.RabbitMqSslClientCertPath,
                registrationSettings.RabbitMqSslClientCertPassword,
                registrationSettings.ServiceBusName,
                registrationSettings.ScheduleServiceBusName);
        }
    }
}
﻿namespace Ivh.Common.DependencyInjection.Registration.AutofacModules
{
    using Application.Core.ApplicationServices;
    using Application.Core.Common.Interfaces;
    using Application.Monitoring.Common.Interfaces;
    using Application.Monitoring.Services;
    using Autofac;
    using Domain.Settings.Interfaces;

    public class SystemHealthModule : BaseModule
    {
        public SystemHealthModule()
            : base()
        {
        }

        protected override void Load(ContainerBuilder builder)
        {
           builder.RegisterType<SystemHealthApplicationService>().As<ISystemHealthApplicationService>();
        }
    }
}
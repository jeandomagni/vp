﻿namespace Ivh.Common.DependencyInjection.Registration.AutofacModules
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Application.Settings;
    using Application.Settings.Common.Interfaces;
    using Autofac;
    using Autofac.Core;
    using Autofac.Features.Metadata;
    using VisitPay.Enums;
    using Configuration;
    using Configuration.Interfaces;
    using global::Ivh.Common.Base.Utilities.Extensions;
    using Domain.Logging.Enums;
    using Domain.Logging.Interfaces;
    using Domain.Settings.Enums;
    using Domain.Settings.Interfaces;
    using Domain.Settings.Services;
    using Provider.Settings;
    using SettingsProvider = Provider.Settings.SettingsProvider;

    public class SettingsModule : BaseModule
    {
        private static readonly ApplicationEnum Application = Properties.Settings.Default.Application.ToEnum<ApplicationEnum>(ApplicationEnum.ClientDataApi);
        private static readonly string ClientDataApiUrl = AppSettingsProvider.Instance.Get("ClientDataApi.Url");
        private static readonly string ClientDataApiAppId = AppSettingsProvider.Instance.Get("ClientDataApi.AppId");
        private static readonly string ClientDataApiAppKey = AppSettingsProvider.Instance.Get("ClientDataApi.AppKey");
        private static readonly int ClientDataApiTimeToLive = AppSettingsProvider.Instance.Get("ClientDataApi.TimeToLive").ToInt32(SettingsProviderParameters.DefaultTimeToLive);
        private static readonly string EnvironmentTypeString = AppSettingsProvider.Instance.Get("EnvironmentType");

        public SettingsModule()
            : base()
        {
        }

        protected override void Load(ContainerBuilder builder)
        {
            SettingsProviderParameters settingsParams = new SettingsProviderParameters
            {
                Application = Application,
                ApplicationName = Ivh.Common.Properties.Settings.Default.ApplicationName,
                ClientDataApiAppId = ClientDataApiAppId,
                ClientDataApiAppKey = ClientDataApiAppKey,
                ClientDataApiTimeToLive = ClientDataApiTimeToLive,
                ClientDataApiUrl = ClientDataApiUrl
            };

            if(Application == ApplicationEnum.ClientDataApi)
            {
                builder.RegisterType<SettingsProvider>()
                .As<ISettingsProvider>()
                .WithParameter(
                    new ResolvedParameter(
                        (pi, ctx) => pi.ParameterType == typeof(ILoggingService),
                        (pi, ctx) => ctx.Resolve<IEnumerable<Meta<ILoggingService>>>().First(a => a.Metadata["LoggerType"].Equals(LoggerTypeEnum.Database)).Value))
                .WithParameters(new[]
                {
                    new NamedParameter("parameters", settingsParams),
                });
            }
            else
            {
                builder.RegisterType<SettingsProviderClient>()
                .As<ISettingsProvider>()
                .WithParameter(
                    new ResolvedParameter(
                        (pi, ctx) => pi.ParameterType == typeof(ILoggingService),
                        (pi, ctx) => ctx.Resolve<IEnumerable<Meta<ILoggingService>>>().First(a => a.Metadata["LoggerType"].Equals(LoggerTypeEnum.Database)).Value))
                .WithParameters(new[]
                {
                    new NamedParameter("parameters", settingsParams),
                });
            }

            builder.RegisterType<SettingsApplicationService>().As<ISettingsApplicationService>();

            builder.RegisterType<SettingsService>().As<ISettingsService>();

            builder.RegisterType<ApplicationSettingsService>().As<IApplicationSettingsService>();

            IvhEnvironmentTypeEnum environmentTypeEnum;
            if (Enum.TryParse(EnvironmentTypeString, out environmentTypeEnum))
            {
                if ((environmentTypeEnum == IvhEnvironmentTypeEnum.Dev || environmentTypeEnum == IvhEnvironmentTypeEnum.Qa)
                    && (Enum.GetValues(typeof(UrlEnum)).Cast<UrlEnum>().Select(x => x.GetAttribute<UrlEnumAttribute>()).Any(x => x != null && x.CanIntercept)))
                {
                    if (string.Equals(settingsParams.ApplicationName, "QaTools", StringComparison.InvariantCultureIgnoreCase))
                    {
                        // QATools needs the real UrlService always, and 
                        // UrlInterceptService should only register as IIUrlInterceptService there.
                        builder.RegisterType<UrlService>().As<IUrlService>();
                        builder.RegisterType<UrlInterceptService>().As<IUrlInterceptService>();
                    }
                    else
                    {
                        builder.RegisterType<UrlInterceptService>().As<IUrlService>();
                    }
                }
                else
                {
                    builder.RegisterType<UrlService>().As<IUrlService>();
                }
            }

            builder.RegisterType<SettingsApplicationService>().As<ISettingsApplicationService>();

            builder.RegisterType<ClientSettingRepository>().As<IClientSettingRepository>();

            builder.RegisterType<AppSettingsProvider>().As<IAppSettingsProvider>();
        }
    }
}

﻿namespace Ivh.Common.DependencyInjection.Registration.AutofacModules
{
    using Application.FileStorage;
    using Application.FileStorage.Common.Interfaces;
    using Autofac;
    using global::Ivh.Common.Base.Utilities.Extensions;
    using Domain.FileStorage.Interfaces;
    using Domain.FileStorage.Services;
    using Ivh.Provider.FileStorage;

    public class FileStorageModule : BaseModule
    {
        public FileStorageModule()
            : base()
        {
        }

        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<FileStorageRepository>().As<IFileStorageRepository>();
            builder.RegisterType<FileStorageService>().As<IFileStorageService>();
            builder.RegisterType<FileStorageApplicationService>().As<IFileStorageApplicationService>();
            builder.RegisterType<StatementExportService>().As<IStatementExportService>();
            builder.RegisterType<FileManifestProvider>().As<IFileManifestProvider>();
        }
    }
}

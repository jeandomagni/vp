﻿namespace Ivh.Common.DependencyInjection.Registration.AutofacModules
{
    using System;
    using Application.FinanceManagement.ApplicationServices;
    using Application.FinanceManagement.Common.Interfaces;
    using Autofac;
    using Common.VisitPay.Enums;
    using global::Ivh.Common.Base.Enums;
    using Domain.Core.SystemException.Interfaces;
    using Domain.Visit.Interfaces;
    using Domain.Visit.Services;
    using Domain.FinanceManagement.Ach.Interfaces;
    using Domain.FinanceManagement.Ach.Services;
    using Domain.FinanceManagement.Consolidation.Interfaces;
    using Domain.FinanceManagement.Consolidation.Services;
    using Domain.FinanceManagement.Discount.Interfaces;
    using Domain.FinanceManagement.Discount.Services;
    using Domain.FinanceManagement.FinancePlan.Interfaces;
    using Domain.FinanceManagement.FinancePlan.Services;
    using Domain.FinanceManagement.Interfaces;
    using Domain.FinanceManagement.Payment.Interfaces;
    using Domain.FinanceManagement.Payment.Services;
    using Domain.FinanceManagement.Payment.Services.PaymentAllocation;
    using Domain.FinanceManagement.RoboRefund.Interfaces;
    using Domain.FinanceManagement.RoboRefund.Services;
    using Domain.FinanceManagement.ServiceGroup.Interfaces;
    using Domain.FinanceManagement.ServiceGroup.Services;
    using Domain.FinanceManagement.Services;
    using Domain.FinanceManagement.Statement.Interfaces;
    using Domain.FinanceManagement.Statement.Services;
    using Domain.Settings.Interfaces;
    using Provider.FinanceManagement;
    using Provider.FinanceManagement.Ach;
    using Provider.FinanceManagement.Consolidation;
    using Provider.FinanceManagement.Discount;
    using Provider.FinanceManagement.Payment;
    using Provider.FinanceManagement.HealthEquity;
    using Provider.FinanceManagement.RoboRefund;
    using Provider.FinanceManagement.ServiceGroup;
    using Provider.FinanceManagement.Statement;
    using Provider.Visit;

    public class FinanceManagementModule : BaseModule
    {
        private readonly RegistrationSettings _registrationSettings;

        public FinanceManagementModule(RegistrationSettings registrationSettings)
            : base()
        {
            this._registrationSettings = registrationSettings;
        }

        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<PaymentMethodsService>().As<IPaymentMethodsService>();
            builder.RegisterType<PaymentMethodAccountTypeService>().As<IPaymentMethodAccountTypeService>();
            builder.RegisterType<PaymentMethodProviderTypeService>().As<IPaymentMethodProviderTypeService>();
            builder.RegisterType<TextToPayApplicationService>().As<ITextToPayApplicationService>();

            builder.RegisterType<VpStatementService>().As<IVpStatementService>();
            builder.RegisterType<VpStatementDocumentService>().As<IVpStatementDocumentService>();
            builder.RegisterType<FinancePlanStatementService>().As<IFinancePlanStatementService>();
            builder.RegisterType<StatementDateService>().As<IStatementDateService>();
            builder.RegisterType<StatementCreationApplicationService>().As<IStatementCreationApplicationService>();
            builder.RegisterType<StatementSnapshotCreationVp3V1ApplicationService>().As<IStatementSnapshotCreationApplicationService>();
            builder.RegisterType<StatementExportApplicationService>().As<IStatementExportApplicationService>();
            builder.RegisterType<ScheduledPaymentApplicationService>().As<IScheduledPaymentApplicationService>();
            builder.RegisterType<PastDueUncollectableApplicationService>().As<IPastDueUncollectableApplicationService>();
            builder.RegisterType<VpStatementRepository>().As<IVpStatementRepository>();
            builder.RegisterType<VpStatementDocumentRepository>().As<IVpStatementDocumentRepository>();
            builder.RegisterType<FinancePlanStatementRepository>().As<IFinancePlanStatementRepository>();
            builder.RegisterType<VisitStateService>().As<IVisitStateService>();

            builder.RegisterType<AchApplicationService>().As<IAchApplicationService>();
            builder.RegisterType<AchService>().As<IAchService>();
            builder.RegisterType<AchProvider>().As<IAchProvider>();

            builder.RegisterType<FinancialDataSummaryApplicationService>().As<IFinancialDataSummaryApplicationService>();

            builder.RegisterType<PaymentEventRepository>().As<IPaymentEventRepository>();
            builder.RegisterType<PaymentEventService>().As<IPaymentEventService>();

            builder.RegisterType<PaymentService>().As<IPaymentService>();
            builder.RegisterType<PaymentActionService>().As<IPaymentActionService>();
            builder.RegisterType<PaymentBatchOperationsService>().As<IPaymentBatchOperationsService>();
            builder.RegisterType<PaymentEventService>().As<IPaymentEventService>();
            builder.RegisterType<PaymentOptionService>().As<IPaymentOptionService>();
            builder.RegisterType<PaymentReversalService>().As<IPaymentReversalService>();
            builder.RegisterType<PaymentRepository>().As<IPaymentRepository>();
            builder.RegisterType<PaymentVisitRepository>().As<IPaymentVisitRepository>();
            builder.RegisterType<PaymentFinancePlanRepository>().As<IPaymentFinancePlanRepository>();
            
            builder.RegisterType<PaymentProcessorResponseRepository>().As<IPaymentProcessorResponseRepository>();
            builder.RegisterType<PaymentProcessorResponseService>().As<IPaymentProcessorResponseService>();

            builder.RegisterType<MerchantAccountAllocationService>().As<IMerchantAccountAllocationService>();
            builder.RegisterType<MerchantAccountRepository>().As<IMerchantAccountRepository>();
            builder.RegisterType<BalanceTransferStatusMerchantAccountRepository>().As<IBalanceTransferStatusMerchantAccountRepository>();
            builder.RegisterType<ServiceGroupMerchantAccountRepository>().As<IServiceGroupMerchantAccountRepository>();

            builder.RegisterType<AmortizationService>().As<IAmortizationService>();
            builder.RegisterType<FinancePlanService>().As<IFinancePlanService>();
            builder.RegisterType<FinancePlanIncentiveService>().As<IFinancePlanIncentiveService>();

            builder.RegisterType<InterestService>().As<IInterestService>();
            
            builder.RegisterType<RoboRefundService>().As<IRoboRefundService>();
            builder.RegisterType<RoboRefundRepository>().As<IRoboRefundRepository>();

            builder.RegisterType<FinancePlanOfferService>().As<IFinancePlanOfferService>();
            builder.RegisterType<FinancePlanStatusApplicationService>().As<IFinancePlanStatusApplicationService>();
            builder.RegisterType<VisitTransactionChangesApplicationService>().As<IVisitTransactionChangesApplicationService>();

            builder.RegisterType<GenericDiscountRepository>().As<IGenericDiscountRepository>();

            builder.RegisterType<StatementExportApplicationService>().As<IStatementExportApplicationService>();

            builder.RegisterType<BaseVisitService>().As<IBaseVisitService>();
            builder.RegisterType<BaseVisitRepository>().As<IBaseVisitRepository>();
            builder.RegisterType<PaymentSummaryApplicationService>().As<IPaymentSummaryApplicationService>();
            builder.RegisterType<VisitEventTrackerService>().As<IVisitEventTrackerService>();
            switch (this._registrationSettings.InterestRateConsiderationService)
            {
                case InterestRateConsiderationServiceEnum.InterestRateConsiderationFinancePlansBalanceService:
                    builder.RegisterType<InterestRateConsiderationFinancePlansBalanceService>().As<IInterestRateConsiderationService>();
                    break;
                case InterestRateConsiderationServiceEnum.InterestRateConsiderationNoneService:
                    builder.RegisterType<InterestRateConsiderationNoneService>().As<IInterestRateConsiderationService>();
                    break;
                default:
                    throw new Exception("InterestRateConsiderationService::Must have an Interest Rate Consideration Service defined!");
            }

            switch (this._registrationSettings.PaymentAllocationModelType)
            {
                case PaymentAllocationModelTypeEnum.ProRata:
                    builder.RegisterType<ProRataPaymentAllocationService>().As<IPaymentAllocationService>();
                    break;
                case PaymentAllocationModelTypeEnum.BillingApplication:
                    builder.RegisterType<BillingApplicationPaymentAllocationService>().As<IPaymentAllocationService>();
                    break;
                case PaymentAllocationModelTypeEnum.Unknown:
                default:
                    throw new Exception("FinanceManagementModule::Must have a payment allocation model type defined!");
            }

            switch (this._registrationSettings.DiscountModelType)
            {
                case DiscountModelTypeEnum.Insurance:
                    builder.RegisterType<InsuranceDiscountService>().As<IDiscountService>();
                    break;
                case DiscountModelTypeEnum.Balance:
                    builder.RegisterType<BalanceDiscountService>().As<IDiscountService>();
                    break;
                case DiscountModelTypeEnum.Unknown:
                default:
                    throw new Exception("FinanceManagementModule::Must have a discount model type defined!");
            }

            // register all the ISystemExceptionProviders will be resolved as IEnumerable<ISystemExceptionProvider>
            builder.RegisterType<PaymentService>().As<ISystemExceptionProvider>();

            builder.RegisterType<ConsolidationGuarantorRepository>().As<IConsolidationGuarantorRepository>();
            builder.RegisterType<ConsolidationGuarantorService>().As<IConsolidationGuarantorService>();
            
            builder.RegisterType<ConsolidationGuarantorApplicationService>().As<IConsolidationGuarantorApplicationService>();
            builder.RegisterType<ManagedUserApplicationService>().As<IManagedUserApplicationService>();
            builder.RegisterType<ManagingUserApplicationService>().As<IManagingUserApplicationService>();

            builder.RegisterType<HealthEquityBalanceWebServiceProvider>().As<IHsaBalanceProvider>().As<ISsoTokenProvider>();
            
            builder.RegisterType<ServiceGroupApplicationService>().As<IServiceGroupApplicationService>();
            builder.RegisterType<ServiceGroupService>().As<IServiceGroupService>();
        }
    }
}
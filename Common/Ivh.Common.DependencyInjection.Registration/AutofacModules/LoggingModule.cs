﻿namespace Ivh.Common.DependencyInjection.Registration.AutofacModules
{
    using Autofac;
    using Autofac.Integration.Mef;

    public class LoggingModule : BaseModule
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterMetadataRegistrationSources();
            builder.RegisterModule(new Ivh.Application.Logging.Modules.Module());
            builder.RegisterModule(new Ivh.Domain.Logging.Modules.Module());
            builder.RegisterModule(new Ivh.Provider.Logging.Modules.Module());
            builder.RegisterModule(new Ivh.Provider.Logging.Database.Modules.Module());
            builder.RegisterMetadataRegistrationSources();
        }
    }
}

﻿namespace Ivh.Common.DependencyInjection.Registration.AutofacModules
{
    using System;
    using Autofac;
    using Domain.Email.Interfaces;
    using Domain.Email.Services;
    using Domain.Settings.Interfaces;
    using Provider.Communication.Twilio;

    public class BalanceTransferModule : BaseModule
    {
        public BalanceTransferModule()
            : base()
        {
        }

        protected override void Load(ContainerBuilder builder)
        {
        }
    }
}

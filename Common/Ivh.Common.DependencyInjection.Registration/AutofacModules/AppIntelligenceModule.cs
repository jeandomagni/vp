﻿namespace Ivh.Common.DependencyInjection.Registration.AutofacModules
{
    using Autofac;
    using Domain.AppIntelligence.Interfaces;
    using Domain.AppIntelligence.Services;
    using Domain.Settings.Interfaces;
    using Provider.AppIntelligence;

    public class AppIntelligenceModule : BaseModule
    {
        public AppIntelligenceModule()
            : base()
        {
        }

        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<RandomizedTestService>().As<IRandomizedTestService>();
            builder.RegisterType<RandomizedTestRepository>().As<IRandomizedTestRepository>();
            builder.RegisterType<RandomizedTestGroupMembershipRepository>().As<IRandomizedTestGroupMembershipRepository>();
        }
    }
}

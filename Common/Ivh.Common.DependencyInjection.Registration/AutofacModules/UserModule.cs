﻿namespace Ivh.Common.DependencyInjection.Registration.AutofacModules
{
    using Autofac;

    public class UserModule : BaseModule
    {
        public UserModule()
            : base()
        {
        }

        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterAssemblyModules(typeof(Application.User.Modules.Module).Assembly);
            builder.RegisterAssemblyModules(typeof(Domain.User.Modules.Module).Assembly);
            builder.RegisterAssemblyModules(typeof(Provider.User.Modules.Module).Assembly);
        }
    }
}
﻿namespace Ivh.Common.DependencyInjection.Registration.AutofacModules
{
    using Autofac;


    public class MatchingModule : BaseModule
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterModule(new Ivh.Domain.Matching.Modules.Module());
            builder.RegisterModule(new Ivh.Application.Matching.Modules.Module());
            builder.RegisterModule(new Ivh.Provider.Matching.Modules.Module());
        }
    }
}

namespace Ivh.Common.DependencyInjection.Registration.AutofacModules
{
    using Autofac;

    public class VisitModule : BaseModule
    {
        public VisitModule()
            : base()
        {
        }

        protected override void Load(ContainerBuilder builder)
        {
            //builder.RegisterAssemblyModules(typeof(Application.Visit.Modules.Module).Assembly);
            builder.RegisterAssemblyModules(typeof(Domain.Visit.Modules.Module).Assembly);
            builder.RegisterAssemblyModules(typeof(Provider.Visit.Modules.Module).Assembly);
        }
    }
}
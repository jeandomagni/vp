﻿namespace Ivh.Common.DependencyInjection.Registration.AutofacModules
{
    using Application.Qat.ApplicationServices;
    using Application.Qat.Common.Interfaces;
    using Autofac;
    using Configuration;
    using Configuration.Interfaces;
    using Domain.FinanceManagement.Interfaces;
    using Domain.HospitalData.Eob.Interfaces;
    using Domain.HospitalData.Eob.Services;
    using Domain.Qat.Interfaces;
    using Domain.Qat.Services;
    using Provider.HospitalData.Eob;
    using Provider.Qat;

    public class QatModule : BaseModule
    {
        public QatModule()
            : base()
        {
        }

        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<PaymentProcessorInterceptorService>().As<IPaymentProcessorInterceptorService>();
            builder.RegisterType<AchInterceptorService>().As<IAchInterceptorService>();

            builder.RegisterType<CommunicationInterceptorService>().As<ICommunicationInterceptorService>();

            builder.RegisterType<TestPaymentMethodsService>().As<ITestPaymentMethodsService>();

            builder.RegisterType<DemoVisitPayUserRepository>().As<IDemoVisitPayUserRepository>();
            builder.RegisterType<DemoVisitPayUserService>().As<IDemoVisitPayUserService>();
            builder.RegisterType<QaVisitPayUserRepository>().As<IQaVisitPayUserRepository>();
            builder.RegisterType<DemoVisitPayUserApplicationService>().As<IDemoVisitPayUserApplicationService>();
            builder.RegisterType<QaPaymentBatchApplicationService>().As<IQaPaymentBatchApplicationService>();
            builder.RegisterType<QaMerchantAccountApplicationService>().As<IQaMerchantAccountApplicationService>();

            builder.RegisterType<QaFacilityApplicationService>().As<IQaFacilityApplicationService>();
            builder.RegisterType<QaVisitApplicationService>().As<IQaVisitApplicationService>();
            builder.RegisterType<ExternalLinkRepository>().As<IExternalLinkRepository>();
            builder.RegisterType<ExternalLinkService>().As<IExternalLinkService>();

            builder.RegisterType<QaVisitService>().As<IQaVisitService>();
            builder.RegisterType<QaHsVisitRepository>().As<IQaHsVisitRepository>();

            builder.RegisterType<QaPaymentService>().As<IQaPaymentService>();
            builder.RegisterType<QaCommunicationService>().As<IQaCommunicationService>();

            builder.RegisterType<QaSettingsService>().As<IQaSettingsService>();

            builder.RegisterType<ThrowsErrorsPaymentProvider>().As<IPaymentBatchOperationsProvider>();

            builder.RegisterType<IhcEnrollmentService>().As<IIhcEnrollmentService>();

            builder.RegisterType<AppSettingsProvider>().As<IAppSettingsProvider>();
        }
    }
}

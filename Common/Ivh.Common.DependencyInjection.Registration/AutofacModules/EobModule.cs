﻿namespace Ivh.Common.DependencyInjection.Registration.AutofacModules
{
    using Autofac;

    public class EobModule : BaseModule
    {
        public EobModule()
            : base()
        {
        }
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterAssemblyModules(typeof(Domain.Eob.Modules.Module).Assembly);
            builder.RegisterAssemblyModules(typeof(Provider.Eob.Modules.Module).Assembly);
        }
    }
}

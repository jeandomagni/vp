﻿namespace Ivh.Common.DependencyInjection.Registration.AutofacModules
{
    using Autofac;
    
    public class PowershellModule : BaseModule
    {
        public PowershellModule()
            : base()
        {
        }

        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterAssemblyModules(typeof(Ivh.Application.Powershell.Modules.Module).Assembly);
            builder.RegisterAssemblyModules(typeof(Domain.Powershell.Modules.Module).Assembly);
            builder.RegisterAssemblyModules(typeof(Provider.Powershell.Modules.Module).Assembly);
        }
    }
}
﻿namespace Ivh.Common.DependencyInjection.Registration.AutofacModules
{
    using Application.GuestPay;
    using Application.GuestPay.Common.Interfaces;
    using Autofac;
    using global::Ivh.Common.Base.Utilities.Extensions;
    using Domain.FinanceManagement.Payment.Interfaces;
    using Domain.GuestPay.Interfaces;
    using Domain.GuestPay.Services;
    using Domain.Settings.Interfaces;
    using Provider.FinanceManagement.Payment;
    using Provider.GuestPay;
    using IPaymentRepository = Domain.GuestPay.Interfaces.IPaymentRepository;
    using PaymentRepository = Provider.GuestPay.PaymentRepository;

    public class GuestPayModule : BaseModule
    {
        //TODO: seems silly to inject all of these connection names through the constructor even when they aren't related
        //The base class forces this.
        public GuestPayModule()
            : base()
        {
        }

        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<PaymentRepository>().As<IPaymentRepository>();
            builder.RegisterType<PaymentProcessorResponseRepository>().As<IPaymentProcessorResponseRepository>();
            builder.RegisterType<PaymentService>().As<IPaymentService>();
            builder.RegisterType<PaymentReversalService>().As<IPaymentReversalService>();
            builder.RegisterType<PaymentApplicationService>().As<IPaymentApplicationService>();

            builder.RegisterType<PaymentUnitAuthenticationRepository>().As<IPaymentUnitAuthenticationRepository>();
            builder.RegisterType<PaymentUnitService>().As<IPaymentUnitService>();

            builder.RegisterType<GuestPayJournalEventService>().As<IGuestPayJournalEventService>();
            builder.RegisterType<GuestPayJournalEventApplicationService>().As<IGuestPayJournalEventApplicationService>();
            builder.RegisterType<GuarantorAuthenticationService>().As<IGuarantorAuthenticationService>();
            builder.RegisterType<GuestPayGuarantorAuthenticationApplicationService>().As<IGuestPayGuarantorAuthenticationApplicationService>();
        }
    }
}
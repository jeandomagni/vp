﻿namespace Ivh.Common.DependencyInjection.Registration.AutofacModules
{
    using Autofac;

    public class EventJournalModule : BaseModule
    {
        private readonly RegistrationSettings _registrationSettings;

        public EventJournalModule(RegistrationSettings registrationSettings)
        {
            this._registrationSettings = registrationSettings;
        }

        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterModule(new Ivh.Application.EventJournal.Modules.Module());
            builder.RegisterModule(new Ivh.Domain.EventJournal.Modules.Module(this._registrationSettings.Application));
            builder.RegisterModule(new Ivh.Provider.EventJournal.Modules.Module());
        }
    }
}

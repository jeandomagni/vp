﻿namespace Ivh.Common.DependencyInjection.Registration.AutofacModules
{
    using Application.Aging.ApplicationServices;
    using Application.Aging.Common.Interfaces;
    using Autofac;
    using Domain.Rager.Interfaces;
    using Domain.Rager.Services;
    using Provider.Rager;

    public class AgingModule : BaseModule
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<AgingApplicationService>().As<IAgingApplicationService>();
            builder.RegisterType<VisitAgeService>().As<IVisitAgeService>();
            builder.RegisterType<VisitRepository>().As<IVisitRepository>();
            builder.RegisterType<AgingTierConfigurationRepository>().As<IAgingTierConfigurationRepository>();
            builder.RegisterType<AgingGuarantorStateRepository>().As<IAgingGuarantorStateRepository>();
            builder.RegisterType<AgingGuarantorTypeRepository>().As<IAgingGuarantorTypeRepository>();
            builder.RegisterType<AgingGuarantorService>().As<IAgingGuarantorService>();
        }
    }
}
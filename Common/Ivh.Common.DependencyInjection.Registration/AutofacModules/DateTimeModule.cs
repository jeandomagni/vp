﻿namespace Ivh.Common.DependencyInjection.Registration.AutofacModules
{
    using System;
    using Autofac;
    using Domain.Settings.Interfaces;
    using global::Ivh.Common.Base.Utilities.Helpers;

    public class DateTimeModule : BaseModule
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.Register<TimeZoneHelper>((ctx) =>
            {
                IApplicationSettingsService applicationSettingsService = ctx.Resolve<IApplicationSettingsService>();
                return new TimeZoneHelper(
                    applicationSettingsService.ClientTimeZone.Value,
                    TimeZoneHelper.TimeZones.MountainStandardTime);
            }).SingleInstance();
        }
    }
}
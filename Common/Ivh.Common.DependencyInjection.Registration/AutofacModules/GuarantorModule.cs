namespace Ivh.Common.DependencyInjection.Registration.AutofacModules
{
    using Autofac;

    public class GuarantorModule : BaseModule
    {
        public GuarantorModule()
            : base()
        {
        }

        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterAssemblyModules(typeof(Application.Guarantor.Modules.Module).Assembly);
            builder.RegisterAssemblyModules(typeof(Domain.Guarantor.Modules.Module).Assembly);
            builder.RegisterAssemblyModules(typeof(Provider.Guarantor.Modules.Module).Assembly);
        }
    }
}
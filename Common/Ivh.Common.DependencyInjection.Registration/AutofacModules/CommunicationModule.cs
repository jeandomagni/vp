﻿namespace Ivh.Common.DependencyInjection.Registration.AutofacModules
{
    using System;
    using Autofac;
    using Domain.Email.Interfaces;
    using Domain.Email.Services;
    using Domain.Settings.Interfaces;
    using Provider.Communication.Twilio;

    public class CommunicationModule : BaseModule
    {
        public CommunicationModule()
            : base()
        {
        }

        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<SmsSender>().As<ISmsSender>();
            builder.RegisterType<TwilioSmsProvider>().As<ISmsCommunicationProvider>();
        }
    }
}

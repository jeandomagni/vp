﻿namespace Ivh.Common.DependencyInjection.Registration.AutofacModules
{
    using System;
    using Application.Core;
    using Application.Core.ApplicationServices;
    using Application.Core.Common.Interfaces;
    using Application.EventJournal.Common.Interfaces;
    using Application.EventJournal.Services;
    using Application.FinanceManagement.ApplicationServices;
    using Application.FinanceManagement.Common.Interfaces;
    using Autofac;
    using VisitPay.Enums;
    using Configuration;
    using Configuration.Interfaces;
    using Domain.Core.Encryption.Interfaces;
    using Domain.Core.Encryption.Services;
    using Domain.Guarantor.Interfaces;
    using Domain.Core.LegacyUser.Interfaces;
    using Domain.Core.LegacyUser.Services;
    using Domain.Core.RestrictedCountryAccess.Interfaces;
    using Domain.Core.RestrictedCountryAccess.Services;
    using Domain.Core.Sso.Interfaces;
    using Domain.Core.Sso.Services;
    using Domain.EventJournal.Interfaces;
    using Domain.EventJournal.Services;
    using Domain.User.Interfaces;
    using Domain.User.Services;
    using Domain.FinanceManagement.Interfaces;
    using Domain.FinanceManagement.Services;
    using Domain.Guarantor.Services;
    using Domain.Visit.Unmatching.Interfaces;
    using Domain.Visit.Unmatching.Services;
    using Encryption.Security.Cryptography;
    using Encryption.Security.Interfaces;
    using Provider.Core.LegacyUser;
    using Provider.Core.RestrictedCountryAccess;
    using Provider.Core.Sso;
    using Provider.Core.Unmatching;
    using Provider.Enrollment;
    using Provider.Enrollment.Intermountain;
    using Provider.Guarantor;
    using Provider.Network.Maxmind;
    using Provider.User;
    using Web.Cookies;

    public class CoreModule : BaseModule
    {
        private readonly RegistrationSettings _registrationSettings;

        public CoreModule(RegistrationSettings registrationSettings)
            : base()
        {
            this._registrationSettings = registrationSettings;
        }

        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<SsoOauthTokenGenerator>().As<ISsoOauthTokenGenerator>();
            builder.RegisterType<SsoOauthIssuerService>().As<ISsoOauthIssuerService>();
            builder.RegisterType<SsoVisitPayUserOauthTokenRepository>().As<ISsoVisitPayUserOauthTokenRepository>();
            builder.RegisterType<SsoOauthIssuerApplicationService>().As<ISsoOauthIssuerApplicationService>();
            builder.RegisterType<HsGuarantorUnmatchReasonRepository>().As<IHsGuarantorUnmatchReasonRepository>();
            builder.RegisterType<VisitApplicationService>().As<IVisitApplicationService>();
            builder.RegisterType<VisitTransactionApplicationService>().As<IVisitTransactionApplicationService>();
            builder.RegisterType<BillingSystemRepository>().As<IBillingSystemRepository>();
            builder.RegisterType<VisitStateApplicationService>().As<IVisitStateApplicationService>();
            builder.RegisterType<VisitBillingHoldApplicationService>().As<IVisitBillingHoldApplicationService>();
            builder.RegisterType<BalanceTransferService>().As<IBalanceTransferService>();
            builder.RegisterType<BalanceTransferApplicationService>().As<IBalanceTransferApplicationService>();
            builder.RegisterType<ConnectionStringsProvider>().As<IConnectionStringsProvider>();
            builder.RegisterType<VisitPayGuarantorAuditLogService>().As<IVisitPayGuarantorAuditLogService>();
            builder.RegisterType<VisitPayGuarantorAuditLogRepository>().As<IVisitPayGuarantorAuditLogRepository>();
            builder.RegisterType<VisitPayGuarantorAuditLogApplicationService>().As<IVisitPayGuarantorAuditLogApplicatonService>();

            builder.RegisterType<VisitPayUserService>().As<IVisitPayUserService>();
            builder.RegisterType<PasswordHasherService>().As<IPasswordHasherService>();

            builder.RegisterType<VisitPayUserIssueResultApplicationService>().As<IVisitPayUserIssueResultApplicationService>();

            builder.RegisterType<BaseHsGuarantorService>().As<IBaseHsGuarantorService>();
            builder.RegisterType<BaseHsGuarantorRepository>().As<IBaseHsGuarantorRepository>();

            builder.RegisterType<HsGuarantorMatchDiscrepancyRepository>().As<IHsGuarantorMatchDiscrepancyRepository>();
            builder.RegisterType<UnmatchingService>().As<IUnmatchingService>();
            builder.RegisterType<RedactionService>().As<IRedactionService>();
            builder.RegisterType<RedactionApplicationService>().As<IRedactionApplicationService>();

            builder.RegisterType<LegacyUserService>().As<ILegacyUserService>();
            builder.RegisterType<LegacyUserRepository>().As<ILegacyUserRepository>();

            builder.RegisterType<GuarantorScoreService>().As<IGuarantorScoreService>();
            builder.RegisterType<GuarantorEnrollmentService>().As<IGuarantorEnrollmentService>();
            builder.RegisterType<GuarantorEnrollmentApplicationService>().As<IGuarantorEnrollmentApplicationService>();

            builder.RegisterType<FacilityApplicationService>().As<IFacilityApplicationService>();

            builder.RegisterType<SsoProviderRepository>().As<ISsoProviderRepository>();
            builder.RegisterType<SsoVisitPayUserSettingRepository>().As<ISsoVisitPayUserSettingRepository>();

            builder.RegisterType<SsoService>().As<ISsoService>();
            builder.RegisterType<SsoApplicationService>().As<ISsoApplicationService>();

            builder.RegisterType<HealthEquityEmployerIdResolutionService>().As<IHealthEquityEmployerIdResolutionService>();
            builder.RegisterType<HealthEquityEmployerIdResolutionProvider>().As<IHealthEquityEmployerIdResolutionProvider>();
            builder.RegisterType<SamlService>().As<ISamlService>();

            builder.RegisterType<EventJournalQueryService>().As<IEventJournalQueryService>();
            builder.RegisterType<JournalEventQueryApplicationService>().As<IJournalEventQueryApplicationService>();
            builder.RegisterType<SearchTextLogApplicationService>().As<ISearchTextLogApplicationService>();

            builder.RegisterType<VisitPayUserDetailChangedService>().As<IVisitPayUserDetailChangedService>();
            builder.RegisterType<VisitPayUserDetailChangedRepository>().As<IVisitPayUserDetailChangedRepository>();

            builder.RegisterType<VpGuarantorScoreRepository>().As<IVpGuarantorScoreRepository>();

            //Move this to SLHS specific
            builder.RegisterType<StLukesEncryptionService>().As<IEncryptionService>();

            builder.RegisterType<StLukesOpenEpicSsoSpecificService>().As<ISsoSpecificService>();
            builder.RegisterType<StLukesEncryptionService>().As<IEncryptionService>();

            builder.RegisterType<SsoOpenEpicRepository>().As<ISsoSpecificRespository>();
            builder.RegisterType<WebCookieFacade>().As<IWebCookieFacade>();

            builder.RegisterType<GuarantorRepository>().As<IGuarantorRepository>();

            builder.RegisterType<RestrictedCountryAccessService>().As<IRestrictedCountryAccessService>();
            builder.RegisterType<CountryIpLookupProvider>().As<ICountryIpLookupProvider>();
            builder.RegisterType<RestrictedCountryAccessRepository>().As<IRestrictedCountryAccessRepository>();
            builder.RegisterType<RestrictedCountryAccessApplicationService>().As<IRestrictedCountryAccessApplicationService>();

            switch (this._registrationSettings.GuarantorEnrollmentProvider)
            {
                case GuarantorEnrollmentProviderEnum.DefaultGuarantorEnrollmentProvider:
                    builder.RegisterType<DefaultGuarantorEnrollmentProvider>().As<IGuarantorEnrollmentProvider>();
                    break;
                case GuarantorEnrollmentProviderEnum.IntermountainGuarantorEnrollmentProvider:
                    builder.RegisterType<IntermountainGuarantorEnrollmentProvider>().As<IGuarantorEnrollmentProvider>();
                    break;
                case GuarantorEnrollmentProviderEnum.InternalGuarantorEnrollmentProvider:
                    builder.RegisterType<InternalGuarantorEnrollmentProvider>().As<IGuarantorEnrollmentProvider>();
                    break;
                default:
                    throw new Exception("GuarantorEnrollmentProvider::Must have a Guarantor Enrollment Provider defined!");
            }

            switch (this._registrationSettings.RegistrationMatchStringApplicationService)
            {
                case RegistrationMatchStringApplicationServiceEnum.IntermountainRegistrationMatchStringApplicationService:
                    builder.RegisterType<IntermountainRegistrationMatchStringApplicationService>().As<IRegistrationMatchStringApplicationService>();
                    break;
                case RegistrationMatchStringApplicationServiceEnum.DefaultRegistrationMatchStringApplicationService:
                    builder.RegisterType<DefaultRegistrationMatchStringApplicationService>().As<IRegistrationMatchStringApplicationService>();
                    break;
                default:
                    throw new Exception("RegistrationMatchStringApplicationService::Must have a RegistrationMatchStringApplicationService defined!");
            }
        }
    }
}
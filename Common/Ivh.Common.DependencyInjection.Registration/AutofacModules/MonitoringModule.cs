﻿namespace Ivh.Common.DependencyInjection.Registration.AutofacModules
{
    using System.Collections.Generic;
    using System.Linq;
    using Autofac;
    using Domain.Monitoring.Interfaces;
    using Domain.Settings.Interfaces;
    using Provider.Monitoring;


    public class MonitoringModule : BaseModule
    {
        public MonitoringModule()
            : base()
        {
        }

        protected override void Load(ContainerBuilder builder)
        {
            IDictionary<string, string> sessionParameters = new Dictionary<string, string>()
            {
                {"loggingSession", this.LoggingConnectionName},
                {"cdiEtlSession", this.CdiEtlConnectionName},
                {"storageSession", this.StorageConnectionName},
            };

            builder.RegisterType<MonitoringRepository>().As<IMonitoringRepository>();
        }
    }
}
﻿namespace Ivh.Common.DependencyInjection.Registration.AutofacModules
{
    using Application.Scheduling.Common.Interfaces;
    using Application.Scheduling.Interfaces;
    using Application.Scheduling.Services;
    using Autofac;
    using Domain.Settings.Interfaces;
    using Provider.Scheduling.Quartz;

    public class SchedulingModule : BaseModule
    {
        public SchedulingModule()
            : base()
        {
        }

        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<SchedulingApplicationService>().As<ISchedulingApplicationService>();
            builder.RegisterType<MassTransitMessageScheduler>().As<IMassTransitMessageScheduler>();
        }
    }
}
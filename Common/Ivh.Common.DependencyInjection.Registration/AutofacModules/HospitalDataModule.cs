﻿namespace Ivh.Common.DependencyInjection.Registration.AutofacModules
{
    using System;
    using Application.Core.Common.Interfaces;
    using Application.HospitalData;
    using Application.HospitalData.Common.Interfaces;
    using Autofac;
    using Domain.HospitalData.Analytics.Interfaces;
    using Domain.HospitalData.Change.Interfaces;
    using Domain.HospitalData.Change.Services;
    using Domain.HospitalData.Eob.Interfaces;
    using Domain.HospitalData.Eob.Services;
    using Domain.HospitalData.FileLoadTracking.Interfaces;
    using Domain.HospitalData.FileLoadTracking.Services;
    using Domain.HospitalData.HsGuarantor.Interfaces;
    using Domain.HospitalData.HsGuarantor.Services;
    using Domain.HospitalData.Inbound.Interfaces;
    using Domain.HospitalData.Inbound.Services;
    using Domain.HospitalData.Outbound.Interfaces;
    using Domain.HospitalData.Outbound.Services;
    using Domain.HospitalData.Scoring.Interfaces;
    using Domain.HospitalData.Scoring.Services;
    using Domain.HospitalData.Segmentation.Interfaces;
    using Domain.HospitalData.Segmentation.Services;
    using Domain.HospitalData.Visit.Interfaces;
    using Domain.HospitalData.Visit.Services;
    using Domain.HospitalData.VpGuarantor.Interfaces;
    using Domain.Visit.Interfaces;
    using Provider.HospitalData.Analytics;
    using Provider.HospitalData.Change;
    using Provider.HospitalData.DataLoad;
    using Provider.HospitalData.Eob;
    using Provider.HospitalData.FileLoadTracking;
    using Provider.HospitalData.HsGuarantor;
    using Provider.HospitalData.Inbound;
    using Provider.HospitalData.Jobs;
    using Provider.HospitalData.Outbound;
    using Provider.HospitalData.Scoring;
    using Provider.HospitalData.Segmentation;
    using Provider.HospitalData.Segmentation.PresumptiveCharityEvaluationProviders;
    using Provider.HospitalData.Visit;
    using Provider.HospitalData.VpGuarantorMatch;
    using VisitPay.Enums;
    using ChangeSetApplicationService = Application.HospitalData.ChangeSetApplicationService;
    using FacilityRepository = Provider.HospitalData.Visit.FacilityRepository;
    using IChangeSetApplicationService = Application.HospitalData.Common.Interfaces.IChangeSetApplicationService;
    using IFacilityRepository = Domain.HospitalData.Visit.Interfaces.IFacilityRepository;
    using IFacilityService = Domain.HospitalData.Visit.Interfaces.IFacilityService;
    using IHsGuarantorApplicationService = Application.HospitalData.Common.Interfaces.IHsGuarantorApplicationService;
    using IInsurancePlanRepository = Domain.HospitalData.Visit.Interfaces.IInsurancePlanRepository;
    using IPrimaryInsuranceTypeRepository = Domain.HospitalData.Visit.Interfaces.IPrimaryInsuranceTypeRepository;
    using IRedactionApplicationService = Application.HospitalData.Common.Interfaces.IRedactionApplicationService;
    using IVisitInsurancePlanRepository = Domain.HospitalData.Visit.Interfaces.IVisitInsurancePlanRepository;
    using IVisitRepository = Domain.HospitalData.Visit.Interfaces.IVisitRepository;
    using IVisitService = Domain.HospitalData.Visit.Interfaces.IVisitService;
    using IVisitTransactionRepository = Domain.HospitalData.Visit.Interfaces.IVisitTransactionRepository;
    using IVisitTransactionService = Domain.HospitalData.Visit.Interfaces.IVisitTransactionService;
    using RedactionApplicationService = Application.HospitalData.RedactionApplicationService;


    public class HospitalDataModule : BaseModule
    {
        private readonly RegistrationSettings _registrationSettings;

        public HospitalDataModule(RegistrationSettings registrationSettings)
            : base()
        {
            this._registrationSettings = registrationSettings;
        }

        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<VisitChangeRepository>().As<IVisitChangeRepository>();
            builder.RegisterType<TransactionCodeRepository>().As<ITransactionCodeRepository>();

            builder.RegisterType<Domain.HospitalData.VpGuarantor.Services.HsGuarantorService>().As<Domain.HospitalData.VpGuarantor.Interfaces.IHsGuarantorService>();
            builder.RegisterType<HospitalGuarantorRepository>().As<IHospitalGuarantorRepository>();

            builder.RegisterType<VpGuarantorHsMatchRepository>().As<IVpGuarantorHsMatchRepository>();

            builder.RegisterType<GuarantorMatchingLogRepository>().As<IGuarantorMatchingLogRepository>();

            builder.RegisterType<DataLoadRepository>().As<IDataLoadRepository>();

            builder.RegisterType<DataLoadService>().As<IDataLoadService>();

            builder.RegisterType<ChangeEventRepository>().As<IChangeEventRepository>();

            builder.RegisterType<ChangeEventService>().As<IChangeEventService>();
            builder.RegisterType<ChangeEventApplicationService>().As<IChangeEventApplicationService>();

            // inbound file and load tracking
            builder.RegisterType<FileTrackerRepository>().As<IFileTrackerRepository>();
            builder.RegisterType<FileStatusRepository>().As<IFileStatusRepository>();
            builder.RegisterType<FileTypeRepository>().As<IFileTypeRepository>();
            builder.RegisterType<LoadTrackerRepository>().As<ILoadTrackerRepository>();
            builder.RegisterType<LoadStatusRepository>().As<ILoadStatusRepository>();
            builder.RegisterType<LoadTypeRepository>().As<ILoadTypeRepository>();
            builder.RegisterType<LoadTrackerStepHistoryRepository>().As<ILoadTrackerStepHistoryRepository>();
            builder.RegisterType<HsDataInboundService>().As<IHsDataInboundService>();
            builder.RegisterType<HsDataInboundApplicationService>().As<IHsDataInboundApplicationService>();
            builder.RegisterType<FileTrackerService>().As<IFileTrackerService>();
            builder.RegisterType<LoadTrackerService>().As<ILoadTrackerService>();

            builder.RegisterType<Eob835Service>().As<IEob835Service>();
            builder.RegisterType<FileHandlingService>().As<IFileHandlingService>();

            builder.RegisterType<EobClaimsLinkRepository>().As<IEobClaimsLinkRepository>();
            builder.RegisterType<EobClaimAdjustmentReasonCodeRepository>().As<IEobClaimAdjustmentReasonCodeRepository>();
            builder.RegisterType<EobRemittanceAdviceRemarkCodeRepository>().As<IEobRemittanceAdviceRemarkCodeRepository>();
            builder.RegisterType<Eob835Repository>().As<IEob835Repository>();
            builder.RegisterType<EobPayerFilterRepository>().As<IEobPayerFilterRepository>();
            builder.RegisterType<ExternalLinkRepository>().As<IExternalLinkRepository>();
            builder.RegisterType<EobDisplayCategoryRepository>().As<IEobDisplayCategoryRepository>();
            builder.RegisterType<EobClaimAdjustmentReasonCodeDisplayMapRepository>().As<IEobClaimAdjustmentReasonCodeDisplayMapRepository>();

            // inbound
            builder.RegisterType<BaseLockboxVendorFileRepository>().As<IBaseLockboxVendorFileRepository>();
            builder.RegisterType<EtlLockboxVendorFileRepository>().As<IEtlLockboxVendorFileRepository>();
            builder.RegisterType<FileTypeConfigInboundRepository>().As<IFileTypeConfigInboundRepository>();
            builder.RegisterType<InboundFileService>().As<IInboundFileService>();
            builder.RegisterType<PaymentVendorFileService>().As<IPaymentVendorFileService>();

            // outbound
            builder.RegisterType<FileTypeConfigOutboundRepository>().As<IFileTypeConfigOutboundRepository>();
            builder.RegisterType<OutboundPaperFileService>().As< IOutboundPaperFileService>();
            builder.RegisterType<VpOutboundFileRepository>().As<IVpOutboundFileRepository>();
            builder.RegisterType<OutboundZipFileProvider>().As<IOutboundFileProvider>();

            // scoring and segmentation application services
            builder.RegisterType<HsGuarantorApplicationScoringService>().As<IHsGuarantorScoringApplicationService>();
            builder.RegisterType<HsSegmentationApplicationService>().As<IHsSegmentationApplicationService>();
            builder.RegisterType<RScriptEngine>().As<IRscriptEngine>();
            builder.RegisterType<ScoringApplicationService>().As<IScoringApplicationService>();
            builder.RegisterType<SegmentationBadDebtService>().As<ISegmentationBadDebtService>();
            builder.RegisterType<SegmentationAccountsReceivableService>().As<ISegmentationAccountsReceivableService>();
            builder.RegisterType<SegmentationActivePassiveService>().As<ISegmentationActivePassiveService>();
            builder.RegisterType<SegmentationPresumptiveCharityService>().As<ISegmentationPresumptiveCharityService>();

            // scoring provider and domain services
            builder.RegisterType<GuarantorBatchRepository>().As<IGuarantorBatchRepository>();
            builder.RegisterType<HouseholdIncomeCodeToIncomeAmountMapRepository>().As<IHouseholdIncomeCodeToIncomeAmountMapRepository>();
            builder.RegisterType<MatchedGuarantorsVisitBatchRepository>().As<IMatchedGuarantorsVisitBatchRepository>();
            builder.RegisterType<RetroScoreRepository>().As<IRetroScoreRepository>();
            builder.RegisterType<RetroScoreService>().As<IRetroScoreService>();
            builder.RegisterType<ScoreRepository>().As<IScoreRepository>();
            builder.RegisterType<BaseScoreRepository>().As<IBaseScoreRepository>();
            builder.RegisterType<ScoringBatchRepository>().As<IScoringBatchRepository>();
            builder.RegisterType<ScoringGuarantorMatchingService>().As<IScoringGuarantorMatchingService>();
            builder.RegisterType<ScoringMonitoringThresholdsRepository>().As<IScoringMonitoringThresholdsRepository>();
            builder.RegisterType<ScoringMonitoringThresholdsService>().As<IScoringMonitoringThresholdsService>();
            builder.RegisterType<ScoringService>().As<IScoringService>();
            builder.RegisterType<ScoringStatisticalMonitoringRepository>().As<IScoringStatisticalMonitoringRepository>();
            builder.RegisterType<ScoringStatisticalMonitoringService>().As<IScoringStatisticalMonitoringService>();
            builder.RegisterType<ThirdPartyDataService>().As<IThirdPartyDataService>();
            builder.RegisterType<ThirdPartyDataProvider>().As<IThirdPartyDataProvider>();
            builder.RegisterType<ThirdPartyDataRepository>().As<IThirdPartyDataRepository>();
            builder.RegisterType<VisitBatchRepository>().As<IVisitBatchRepository>();
            builder.RegisterType<ScoringSagaLogRepository>().As<ISagaLogRepository>();
            builder.RegisterType<ScoringSagaLogger>().As<IScoringSagaLogger>();
            builder.RegisterType<ScoringMatchedGuarantorScoreHistoryRepository>().As<IScoringMatchedGuarantorScoreHistoryRepository>();
            builder.RegisterType<ScoringMatchedGuarantorScoreHistoryService>().As<IScoringMatchedGuarantorScoreHistoryService>();
            builder.RegisterType<ScoringAndSegmentationSagaRepository>().As<IScoringAndSegmentationSagaRepository>();

            // segmentation provider and domain services
            builder.RegisterType<AccountsReceivableSegmentationThresholdRepository>().As<IAccountsReceivableSegmentationThresholdRepository>();
            builder.RegisterType<AccountsReceivableVisitDailyRepository>().As<IAccountsReceivableVisitDailyRepository>();
            builder.RegisterType<ActivePassiveSegmentationThresholdRepository>().As<IActivePassiveSegmentationThresholdRepository>();
            builder.RegisterType<ActivePassiveVisitDailyRepository>().As<IActivePassiveVisitDailyRepository>();
            builder.RegisterType<BadDebtSegmentationValueRepository>().As<IBadDebtSegmentationValueRepository>();
            builder.RegisterType<BadDebtSegmentationValueService>().As<IBadDebtSegmentationValueService>();
            builder.RegisterType<BadDebtVisitDailyRepository>().As<IBadDebtVisitDailyRepository>();
            builder.RegisterType<GuarantorSegmentationDailyRepository>().As<IGuarantorSegmentationDailyRepository>();
            builder.RegisterType<GuarantorSegmentationDailyService>().As<IGuarantorSegmentationDailyService>();
            builder.RegisterType<SegmentationBatchRepository>().As<ISegmentationBatchRepository>();
            builder.RegisterType<MatchedGuarantorsRepository>().As<IMatchedGuarantorsRepository>();
            builder.RegisterType<PresumptiveCharityGuarantorVisitDailyRepository>().As<IPresumptiveCharityGuarantorVisitDailyRepository>();
            builder.RegisterType<PresumptiveCharityRepository>().As<IPresumptiveCharityRepository>();
            builder.RegisterType<SegmentationDailyProcessService>().As<ISegmentationDailyProcessService>();
            builder.RegisterType<SegmentationProcessProvider>().As<ISegmentationProcessProvider>();
            builder.RegisterType<SegmentationService>().As<ISegmentationProcessService>();
            // PresumptiveCharityEvaluationProviders
            switch (this._registrationSettings.PresumptiveCharityEvaluationProvider)
            {
                case PresumptiveCharityEvaluationProviderEnum.DefaultPresumptiveCharityEvaluationProvider:
                    builder.RegisterType<DefaultPresumptiveCharityEvaluationProvider>().As<IPresumptiveCharityEvaluationProvider>();
                    break;
                case PresumptiveCharityEvaluationProviderEnum.EvaluateForPcFlagPresumptiveCharityEvaluationProvider:
                    builder.RegisterType<EvaluateForPcFlagPresumptiveCharityEvaluationProvider>().As<IPresumptiveCharityEvaluationProvider>();
                    break;
                case PresumptiveCharityEvaluationProviderEnum.SelfPayClassPresumptiveCharityEvaluationProvider:
                    builder.RegisterType<SelfPayClassPresumptiveCharityEvaluationProvider>().As<IPresumptiveCharityEvaluationProvider>();
                    break;
                default:
                    throw new Exception("PresumptiveCharityEvaluationProvider::Must have a Presumptive Charity Evaluation Provider defined!");
            }


            // Analytic reports provider services
            builder.RegisterType<AnalyticReportRepository>().As<IAnalyticReportRepository>();
            builder.RegisterType<AnalyticReportClassificationRepository>().As<IAnalyticReportClassificationRepository>();

            builder.RegisterType<HsGuarantorAcknowledgementApplicationService>().As<IHsGuarantorAcknowledgementApplicationService>();

            builder.RegisterType<GuarantorEventApplicationService>().As<IGuarantorEventApplicationService>();

            builder.RegisterType<VisitService>().As<IVisitService>();
            builder.RegisterType<VisitRepository>().As<IVisitRepository>();

            builder.RegisterType<FacilityService>().As<IFacilityService>();
            builder.RegisterType<FacilityRepository>().As<IFacilityRepository>();

            builder.RegisterType<RealTimeVisitEnrollmentService>().As<IRealTimeVisitEnrollmentService>();
            builder.RegisterType<RealTimeVisitEnrollmentRepository>().As<IRealTimeVisitEnrollmentRepository>();
            builder.RegisterType<RealTimeVisitEnrollmentApplicationService>().As<IRealTimeVisitEnrollmentApplicationService>();

            builder.RegisterType<VisitTransactionService>().As<IVisitTransactionService>();
            builder.RegisterType<VisitTransactionRepository>().As<IVisitTransactionRepository>();
            builder.RegisterType<VisitTransactionChangeRepository>().As<IVisitTransactionChangeRepository>();

            builder.RegisterType<VisitPaymentAllocationRepository>().As<IVisitPaymentAllocationRepository>();

            builder.RegisterType<ChangeSetApplicationService>().As<IChangeSetApplicationService>();

            builder.RegisterType<HsGuarantorApplicationService>().As<IHsGuarantorApplicationService>();
            builder.RegisterType<HsGuarantorService>().As<Domain.HospitalData.HsGuarantor.Interfaces.IHsGuarantorService>();
            builder.RegisterType<HsGuarantorRepository>().As<IHsGuarantorRepository>();
            builder.RegisterType<HsGuarantorChangeRepository>().As<IHsGuarantorChangeRepository>();

            builder.RegisterType<DataLoadService>().As<IDataLoadService>();
            builder.RegisterType<DataLoadRepository>().As<IDataLoadRepository>();

            builder.RegisterType<OutboundVisitTransactionService>().As<IOutboundVisitTransactionService>();
            builder.RegisterType<OutboundVisitTransactionRepository>().As<IOutboundVisitTransactionRepository>();

            builder.RegisterType<OutboundGuestPayPaymentService>().As<IOutboundGuestPayPaymentService>();
            builder.RegisterType<OutboundGuestPayPaymentRepository>().As<IOutboundGuestPayPaymentRepository>();

            builder.RegisterType<OutboundVisitService>().As<IOutboundVisitService>();
            builder.RegisterType<OutboundVisitRepository>().As<IOutboundVisitRepository>();

            builder.RegisterType<VpOutboundVisitApplicationService>().As<IVpOutboundVisitApplicationService>();
            builder.RegisterType<GpOutboundPaymentApplicationService>().As<IGpOutboundPaymentApplicationService>();

            builder.RegisterType<VpOutboundPaperFileApplicationService>().As<IVpOutboundPaperFileApplicationService>();

            builder.RegisterType<VpGuarantorHsMatchApplicationService>().As<IVpGuarantorHsMatchApplicationService>();
            builder.RegisterType<VpGuarantorHsMatchService>().As<IVpGuarantorHsMatchService>();

            builder.RegisterType<LifeCycleStageRepository>().As<ILifeCycleStageRepository>();
            builder.RegisterType<RevenueWorkCompanyRepository>().As<IRevenueWorkCompanyRepository>();
            builder.RegisterType<PrimaryInsuranceTypeRepository>().As<IPrimaryInsuranceTypeRepository>();
            builder.RegisterType<PatientTypeRepository>().As<IPatientTypeRepository>();
            builder.RegisterType<InsurancePlanRepository>().As<IInsurancePlanRepository>();
            builder.RegisterType<VisitInsurancePlanChangeRepository>().As<IVisitInsurancePlanChangeRepository>();
            builder.RegisterType<VisitInsurancePlanRepository>().As<IVisitInsurancePlanRepository>();
            builder.RegisterType<VisitMatchStatusService>().As<IVisitMatchStatusService>();
            builder.RegisterType<VisitMatchStatusRepository>().As<IVisitMatchStatusRepository>();
            builder.RegisterType<VpOutboundHsGuarantorApplicationService>().As<IVpOutboundHsGuarantorApplicationService>();
            builder.RegisterType<VpOutboundHsGuarantorService>().As<IVpOutboundHsGuarantorService>();
            builder.RegisterType<VpOutboundHsGuarantorRepository>().As<IVpOutboundHsGuarantorRepository>();

            builder.RegisterType<RedactionApplicationService>().As<IRedactionApplicationService>();

            builder.RegisterType<HospitalDataJobsProvider>().As<IHospitalDataJobsProvider>();

            builder.RegisterType<CdiEtlJobsProvider>().As<ICdiEtlJobsProvider>();

            builder.RegisterType<JobsApplicationService>().As<IJobsApplicationService>();

            builder.RegisterType<VpGuarantorMatchInfoApplicationService>().As<IVpGuarantorMatchInfoApplicationService>();
            builder.RegisterType<VpGuarantorHsMatchApiApplicationService>().As<IVpGuarantorHsMatchApiApplicationService>();
            builder.RegisterType<HsGuarantorEnrollmentApiService>().As<IHsGuarantorEnrollmentApiService>();
            builder.RegisterType<HsVisitApplicationService>().As<IHsVisitApplicationService>();
            builder.RegisterType<Eob835ApplicationService>().As<IEob835ApplicationService>();

            builder.RegisterType<Application.Core.ApplicationServices.AnalyticReportApplicationService>().As<IAnalyticReportApplicationService>();

            switch (this._registrationSettings.HsFacilityResolutionService)
            {
                case HsFacilityResolutionServiceEnum.HsFacilityResolutionService:
                    builder.RegisterType<Domain.Visit.Services.HsResolutionServices.HsFacilityResolutionService>().As<IHsFacilityResolutionService>();
                    break;
                case HsFacilityResolutionServiceEnum.IntermountainHsFacilityResolutionService:
                    builder.RegisterType<Domain.Visit.Services.HsResolutionServices.Client.Intermountain.HsFacilityResolutionService>().As<IHsFacilityResolutionService>();
                    break;
                default:
                    throw new Exception("HsFacilityResolutionService::Must have a Hospital Facility Resolution Service defined!");
            }

            switch (this._registrationSettings.HsBillingSystemResolutionProvider)
            {
                case HsBillingSystemResolutionProviderEnum.HsBillingSystemResolutionProvider:
                    builder.RegisterType<Provider.Core.HsResolutionProviders.HsBillingSystemResolutionProvider>().As<IHsBillingSystemResolutionProvider>();
                    break;
                case HsBillingSystemResolutionProviderEnum.IntermountainHsBillingSystemResolutionProvider:
                    builder.RegisterType<Provider.Core.HsResolutionProviders.Client.Intermountain.HsBillingSystemResolutionProvider>().As<IHsBillingSystemResolutionProvider>();
                    break;
                default:
                    throw new Exception("HsBillingSystemResolutionProvider::Must have a Hospital Billing System Resolution Provider defined!");
            }
        }
    }
}
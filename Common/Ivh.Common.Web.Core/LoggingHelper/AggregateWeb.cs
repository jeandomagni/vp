﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;

namespace Ivh.Common.Web.Core.LoggingHelper
{
    public class AggregateWeb
    {
        public static string AggregateWebResponse(string callingMethod, HttpResponseMessage response)
        {
            StringBuilder builder = new StringBuilder();
            builder.AppendLine(string.Format("Method: {0}", callingMethod));
            if (response == null)
            {
                builder.AppendLine("Request or Request.QueryString was null");
                return builder.ToString();
            }

            if (response.Headers != null)
            {
                Dictionary<string, IEnumerable<string>> headers = response.Headers.ToDictionary(kv => kv.Key, kv => kv.Value, StringComparer.OrdinalIgnoreCase);

                builder.AppendLine("Start: Printing out Request.Headers keys");
                foreach (string key in headers.Keys)
                {
                    builder.AppendLine(string.Format("Key: {0}  - Value: {1}", key, string.Join(",", headers[key])));
                }
                builder.AppendLine("End: Printing out Request.Headers keys");
            }

            return builder.ToString();
        }
    }
}

﻿namespace Ivh.Common.DependencyInjection.Core.Interfaces
{
    using Microsoft.Extensions.DependencyInjection;

    public interface IModule
    {
        void Register(IServiceCollection serviceCollection);
    }
}

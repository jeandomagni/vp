﻿namespace Ivh.Common.Encryption.Core.Enums
{
    public enum HmacHashEnum
    {
        HMACSHA1 = 1,
        HMACSHA256 = 2,
        HMACSHA384 = 3,
        HMACSHA512 = 4
    }
}
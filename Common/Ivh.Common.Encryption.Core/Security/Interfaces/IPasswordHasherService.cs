﻿using System.Threading.Tasks;
using Ivh.Common.Encryption.Core.Enums;

namespace Ivh.Common.Encryption.Core.Security.Interfaces
{
    public interface IPasswordHasherService
    {
        string HashPassword(string password, int saltLength, int iterations, HmacHashEnum hmacHashEnum);
        Task<bool> VerifyHashedPasswordAsync(string hashedPassword, string providedPassword, int saltLength, int iterations, HmacHashEnum hmacHashEnum);
        bool VerifyHashedPassword(string hashedPassword, string providedPassword, int saltLength, int iterations, HmacHashEnum hmacHashEnum);
    }
}
﻿using System;
using System.Security;
using System.Security.Cryptography;
using System.Text;
using Ivh.Common.Encryption.Core.Enums;

namespace Ivh.Common.Encryption.Core.Security.Cryptography
{
    public class Rfc2898DeriveBytes : DeriveBytes
    {
        private byte[] _buffer;
        private byte[] _salt;
        private HMAC _hmac;
        private byte[] _password;
        private uint _iterations;
        private uint _block;
        private int _startIndex;
        private int _endIndex;

        private const int BlockSize = 20;

        [SecuritySafeCritical]
        public Rfc2898DeriveBytes(string password, int saltSize, int iterations, HmacHashEnum hmacHashEnum)
        {
            if (saltSize < 0)
                throw new ArgumentOutOfRangeException("saltSize", "Saltsize is out of range. This parameter requires a non-negative number.");

            using (RNGCryptoServiceProvider rngCsp = new RNGCryptoServiceProvider())
            {
                byte[] data = new byte[saltSize];
                rngCsp.GetBytes(data);
                this.Salt = data;
                this.IterationCount = iterations;
                this._password = new UTF8Encoding(false).GetBytes(password);
                this._hmac = this.GetHashFunction(hmacHashEnum);
                this.Initialize();
            }
        }


        [SecuritySafeCritical]
        public Rfc2898DeriveBytes(string password, byte[] salt, int iterations, HmacHashEnum hmacHashEnum)
        {
            this.Salt = salt;
            this.IterationCount = iterations;
            this._password = new UTF8Encoding(false).GetBytes(password);
            this._hmac = this.GetHashFunction(hmacHashEnum);
            this.Initialize();
        }

        private HMAC GetHashFunction(HmacHashEnum hmacHashEnum)
        {
            switch (hmacHashEnum)
            {
                case HmacHashEnum.HMACSHA1:
                    return new HMACSHA1(this._password);
                case HmacHashEnum.HMACSHA256:
                    return new HMACSHA256(this._password);
                case HmacHashEnum.HMACSHA384:
                    return new HMACSHA384(this._password);
                case HmacHashEnum.HMACSHA512:
                    return new HMACSHA512(this._password);
                default:
                    throw new ArgumentOutOfRangeException(nameof(hmacHashEnum), hmacHashEnum, null);
            }
        }

        public byte[] Salt
        {
            get
            {
                return (byte[])this._salt.Clone();
            }
            set
            {
                if (value == null)
                    throw new ArgumentNullException("value", "The salt is null.");
                if (value.Length < 8)
                    throw new ArgumentException("The specified salt size is smaller than 8 bytes or the iteration count is less than 1.");
                this._salt = (byte[])value.Clone();
                this.Initialize();
            }
        }

        public int IterationCount
        {
            get
            {
                return (int)this._iterations;
            }
            set
            {
                if (value <= 0)
                    throw new ArgumentOutOfRangeException("value", "IterationCount is out of range. This parameter requires a non-negative number");
                this._iterations = (uint)value;
                this.Initialize();
            }
        }

        private void Initialize()
        {
            if (this._buffer != null)
                Array.Clear((Array)this._buffer, 0, this._buffer.Length);
            this._buffer = new byte[BlockSize];
            this._block = 1U;
            this._startIndex = this._endIndex = 0;
        }


        public override byte[] GetBytes(int cb)
        {
            if (cb <= 0)
                throw new ArgumentOutOfRangeException("cb", "Byte count is out of range. This parameter requires a non-negative number.");
            byte[] password = new byte[cb];

            int offset = 0;
            int size = this._endIndex - this._startIndex;
            if (size > 0)
            {
                if (cb >= size)
                {
                    Buffer.BlockCopy(this._buffer, this._startIndex, password, 0, size);
                    this._startIndex = this._endIndex = 0;
                    offset += size;
                }
                else
                {
                    Buffer.BlockCopy(this._buffer, this._startIndex, password, 0, cb);
                    this._startIndex += cb;
                    return password;
                }
            }
            while (offset < cb)
            {
                byte[] tBlock = this.Func();
                int remainder = cb - offset;
                if (remainder > BlockSize)
                {
                    Buffer.BlockCopy(tBlock, 0, password, offset, BlockSize);
                    offset += BlockSize;
                }
                else
                {
                    Buffer.BlockCopy(tBlock, 0, password, offset, remainder);
                    offset += remainder;
                    Buffer.BlockCopy(tBlock, remainder, this._buffer, this._startIndex, BlockSize - remainder);
                    this._endIndex += (BlockSize - remainder);
                    return password;
                }
            }
            return password;
        }

        private byte[] Func()
        {
            byte[] intBlock = this.Int(this._block);

            this._hmac.TransformBlock(this._salt, 0, this._salt.Length, null, 0);
            this._hmac.TransformBlock(intBlock, 0, intBlock.Length, null, 0);
            this._hmac.TransformFinalBlock(new byte[0], 0, 0);
            byte[] temp = this._hmac.Hash;
            this._hmac.Initialize();

            byte[] ret = temp;
            for (int i = 2; i <= this._iterations; i++)
            {
                this._hmac.TransformBlock(temp, 0, temp.Length, null, 0);
                this._hmac.TransformFinalBlock(new byte[0], 0, 0);
                temp = this._hmac.Hash;
                for (int j = 0; j < BlockSize; j++)
                {
                    ret[j] ^= temp[j];
                }
                this._hmac.Initialize();
            }

            // increment the block count.
            this._block++;
            return ret;
        }

        // encodes the integer i into a 4-byte array, in big endian.
        private byte[] Int(uint i)
        {
            return unchecked(new byte[] { (byte)(i >> 24), (byte)(i >> 16), (byte)(i >> 8), (byte)i });
        }

        public override void Reset()
        {
            this.Initialize();
        }

        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);

            if (disposing)
            {
                if (this._hmac != null)
                {
                    ((IDisposable) this._hmac).Dispose();
                }

                if (this._buffer != null)
                {
                    Array.Clear(this._buffer, 0, this._buffer.Length);
                }
                if (this._salt != null)
                {
                    Array.Clear(this._salt, 0, this._salt.Length);
                }
            }
        }
    }
}
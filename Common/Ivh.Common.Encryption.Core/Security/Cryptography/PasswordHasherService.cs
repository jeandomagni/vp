﻿using System;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using Ivh.Common.Encryption.Core.Enums;
using Ivh.Common.Encryption.Core.Security.Interfaces;

namespace Ivh.Common.Encryption.Core.Security.Cryptography
{
    public class PasswordHasherService : IPasswordHasherService
    {

        /// <summary>Hash a password</summary>
        public string HashPassword(string password, int saltLength, int iterations, HmacHashEnum hmacHashEnum)
        {
            if (password == null)
                throw new ArgumentNullException("password");
            byte[] salt;
            byte[] bytes;
            using (Rfc2898DeriveBytes rfc2898DeriveBytes = new Rfc2898DeriveBytes(password, saltLength, iterations, hmacHashEnum))
            {
                salt = rfc2898DeriveBytes.Salt;
                bytes = rfc2898DeriveBytes.GetBytes(32);
            }
            byte[] inArray = new byte[saltLength + 33];
            Buffer.BlockCopy((Array)salt, 0, (Array)inArray, 1, saltLength);
            Buffer.BlockCopy((Array)bytes, 0, (Array)inArray, saltLength + 1, 32);
            return Convert.ToBase64String(inArray);
        }

        public async Task<bool> VerifyHashedPasswordAsync(string hashedPassword, string providedPassword, int saltLength, int iterations, HmacHashEnum hmacHashEnum)
        {
            return await Task.FromResult(this.VerifyHashedPassword(hashedPassword, providedPassword, saltLength, iterations, hmacHashEnum));
        }

        /// <summary>Verify that a password matches the hashedPassword</summary>
        public bool VerifyHashedPassword(string hashedPassword, string providedPassword, int saltLength, int iterations, HmacHashEnum hmacHashEnum)
        {
            if (hashedPassword == null)
                return false;
            if (providedPassword == null)
                throw new ArgumentNullException("password");
            byte[] numArray = Convert.FromBase64String(hashedPassword);
            if (numArray.Length != saltLength + 33 || (int)numArray[0] != 0)
                return false;
            byte[] salt = new byte[saltLength];
            Buffer.BlockCopy((Array)numArray, 1, (Array)salt, 0, saltLength);
            byte[] a = new byte[32];
            Buffer.BlockCopy((Array)numArray, saltLength + 1, (Array)a, 0, 32);
            byte[] bytes;
            using (Rfc2898DeriveBytes rfc2898DeriveBytes = new Rfc2898DeriveBytes(providedPassword, salt, iterations, hmacHashEnum))
                bytes = rfc2898DeriveBytes.GetBytes(32);
            return this.ByteArraysEqual(a, bytes);
        }

        [MethodImpl(MethodImplOptions.NoOptimization)]
        private bool ByteArraysEqual(byte[] a, byte[] b)
        {
            if (ReferenceEquals((object)a, (object)b))
                return true;
            if (a == null || b == null || a.Length != b.Length)
                return false;
            bool flag = true;
            for (int index = 0; index < a.Length; ++index)
                flag &= (int)a[index] == (int)b[index];
            return flag;
        }

    }
}

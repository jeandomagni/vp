﻿namespace Ivh.Common.Base.Core.Formatters
{
    using System;

    public static class FormatProviders
    {
        public static IFormatProvider LeadingTrailing = new LeadingTrailingFormatProvider();
    }
}
﻿namespace Ivh.Common.Base.Core.Formatters
{
    using System;

    public class LeadingTrailingFormatProvider : IFormatProvider, ICustomFormatter
    {
        public object GetFormat(Type formatType)
        {
            if (formatType == typeof(ICustomFormatter))
            {
                return this;
            }
            return null;
        }

        public string Format(string format, object arg, IFormatProvider formatProvider)
        {
            if (arg == null)
            {
                throw new ArgumentNullException(nameof(arg));
            }
            string value = arg.GetType() != typeof(string) ? arg.ToString() : (string)arg;

            //"{0:L:..:T}"
            string[] parts = format.Split(':');
            if (parts.Length != 3)
            {
                throw new Exception();
            }

            if (!int.TryParse(parts[0], out int leadingCharsCount) || !int.TryParse(parts[2], out int trailingCharsCount))
            {
                throw new ArgumentException(nameof(format));
            }

            if (value.Length < leadingCharsCount + trailingCharsCount + parts[1].Length)
            {
                return value;
            }

            return value.Substring(0, leadingCharsCount) + parts[1] + value.Substring(value.Length - trailingCharsCount - 1, trailingCharsCount);
        }
    }
}

﻿using System;

namespace Ivh.Common.Base.Core.Enums
{
    [AttributeUsage(AttributeTargets.Field)]
    public class EnumCategoryAttribute : Attribute
    {
        public string[] Categories { get; private set; }
        public EnumCategoryAttribute(string category)
        {
            this.Categories = new string[] { category };
        }
        public EnumCategoryAttribute(params string[] categories)
        {
            this.Categories = categories;
        }
    }
}

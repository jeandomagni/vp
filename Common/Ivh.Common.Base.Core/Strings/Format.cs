﻿namespace Ivh.Common.Base.Core.Strings
{
    /// <summary>
    /// These are string constants. Ideally these would be in a resource file and localizable.
    /// </summary>
    public static class Format
    {
        public const string DateFormat = "MM/dd/yyyy";
        public const string DateFormat2 = "yyyy-MM-dd";
        public const string DateFormatShortYear = "MM/dd/yy";
        public const string DateFormatDayFirst = "dd/MM/yyyy";
        public const string DateTimeFormat = "MM/dd/yyyy hh:mm tt";
        public const string DateTimeFormatSql = "yyyy-MM-dd HH:mm:ss";
        public const string TimeFormat = "hh:mm tt";
        public const string MonthYearFormat = "MMM yyyy";
        public const string MonthDayYearFormat = "MMMM dd, yyyy";
        public const string FirstNameLastNamePattern = "{0} {1}";
        public const string LastNameFirstNamePattern = "{0}, {1}";
        public const string FullNamePattern = "{0} {1} {2}";
        public const string FolderDate = "yyyyMMdd";
    }
}
﻿namespace Ivh.Common.Base.Core.Strings
{
    public static class RegexStrings
    {
        public const string Base64Encoded = "^([A-Za-z0-9+/]{4})*([A-Za-z0-9+/]{4}|[A-Za-z0-9+/]{3}=|[A-Za-z0-9+/]{2}==)$";
        public const string EmailAddress = @"^(?!.*\.{2})[A-Za-z0-9_!#$%&'=/?^_`{}|~+-]+[A-Za-z0-9_!#$%&'=/?^_`{}|~\.]*[A-Za-z0-9_!#$%&'=/?^_`{}|~+-]*@[A-Za-z0-9_!#$%&'=/?^_`{}|~+-]+(\.[A-Za-z0-9_!#$%&'=/?^_`{}|~+-]+)*(\.[a-zA-Z]{2,4})$";
        public const string NonAlphaNumeric = "[^a-zA-Z0-9 -]";
        public const string WhiteSpace = @"\s+";

        //source: https://gist.github.com/dalethedeveloper/1503252
        public const string UserAgentMobile = "/Mobile|iP(hone|od|ad)|Android|BlackBerry|IEMobile|Kindle|NetFront|Silk-Accelerated|(hpw|web)OS|Fennec|Minimo|Opera M(obi|ini)|Blazer|Dolfin|Dolphin|Skyfire|Zune";
        public const string UserAgentTablet = "(tablet|ipad|playbook|silk)|(android(?!.*mobile))";
    }
}
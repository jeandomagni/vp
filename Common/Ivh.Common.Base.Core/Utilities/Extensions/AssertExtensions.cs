﻿using System;
using System.Linq;
using System.Reflection;
using NUnit.Framework;

namespace Ivh.Common.Base.Core.Utilities.Extensions
{
    public static class AssertExtensions
    {
        public static void AssertNoAsyncVoidMethods(Assembly assembly)
        {
            System.Collections.Generic.List<string> messages = assembly
                .GetAsyncVoidMethods()
                .Select(method =>
                    String.Format("'{0}.{1}' is an async void method.",
                        method.DeclaringType.Name,
                        method.Name))
                .ToList();
            Assert.False(messages.Any(),
                "Async void methods found!" + Environment.NewLine + String.Join(Environment.NewLine, messages));
        }
    }
}
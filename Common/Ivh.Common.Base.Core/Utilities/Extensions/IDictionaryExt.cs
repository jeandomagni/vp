﻿namespace Ivh.Common.Base.Core.Utilities.Extensions
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Security.Cryptography;
    using System.Text;
    using Newtonsoft.Json;

    public static class IDictionaryExt
    {
        public static IDictionary<TKey, TValue> Merge<TKey, TValue>(this IDictionary<TKey, TValue> dest, IDictionary<TKey, TValue> source)
        {
            if (dest == null)
            {
                return source;
            }

            if (source != null)
            {
                foreach (KeyValuePair<TKey, TValue> kvp in source)
                {
                    dest[kvp.Key] = kvp.Value;
                }
            }

            return dest;
        }

        public static bool SequenceEqual<TKey, TValue>(this IDictionary<TKey, TValue> dictionary1, IDictionary<TKey, TValue> dictionary2)
        {
            return (dictionary2 ?? new Dictionary<TKey, TValue>())
             .OrderBy(kvp => kvp.Key)
             .SequenceEqual((dictionary1 ?? new Dictionary<TKey, TValue>())
                                .OrderBy(kvp => kvp.Key));
        }

        public static bool IsNotNullAndContainsKey<TKey, TValue>(this IDictionary<TKey, TValue> dictionary, TKey key)
        {
            return dictionary != null && dictionary.ContainsKey(key);
        }

        public static IDictionary<TKey, TValue> ToDictionaryOfOne<TKey, TValue>(this KeyValuePair<TKey, TValue> kvp)
        {
            return new Dictionary<TKey, TValue>() { { kvp.Key, kvp.Value } };
        }

        public static string AsAdditionalDataString(this IDictionary<string, string> additionalData)
        {
            return additionalData.IsNullOrEmpty() ? null : string.Join("|", additionalData.Select(x => string.Format("{0}:{1}", x.Key, x.Value)));
        }

        public static TValue GetValue<TKey, TValue>(this IDictionary<TKey, TValue> dictionary, TKey key)
        {
            return dictionary.TryGetValue(key, out TValue value) ? value : default(TValue);
        }

        public static byte[] DictionaryHashCodeBytes<TKey, TValue, TEncoding, THashAlgorithm>(this IDictionary<TKey, TValue> dictionary)
            where TEncoding : Encoding, new()
            where THashAlgorithm : HashAlgorithm, new()
        {
            return JsonConvert.SerializeObject(dictionary).GetBytes<TEncoding>().GetHashCodeBytes<THashAlgorithm>();
        }

        public static string DictionaryHashCode<TKey, TValue, TEncoding, THashAlgorithm>(this IDictionary<TKey, TValue> dictionary)
            where TEncoding : Encoding, new()
            where THashAlgorithm : HashAlgorithm, new()
        {
            JsonSerializerSettings jsonSerializerSettings = new JsonSerializerSettings() { Formatting = Formatting.None };
            return JsonConvert
                .SerializeObject(new SortedDictionary<TKey, TValue>(dictionary), jsonSerializerSettings)
                .GetBytes<TEncoding>().GetHashCode<THashAlgorithm>();
        }

        public static string DictionaryHashCodeHex<TKey, TValue, TEncoding, THashAlgorithm>(this IDictionary<TKey, TValue> dictionary)
            where TEncoding : Encoding, new()
            where THashAlgorithm : HashAlgorithm, new()
        {
            JsonSerializerSettings jsonSerializerSettings = new JsonSerializerSettings() { Formatting = Formatting.None };
            return JsonConvert
                .SerializeObject(new SortedDictionary<TKey, TValue>(dictionary), jsonSerializerSettings)
                .GetBytes<TEncoding>().GetHashCodeHex<THashAlgorithm>();

        }
    }
}

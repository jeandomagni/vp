﻿using System.Collections.Generic;
using System.Linq;

namespace Ivh.Common.Base.Core.Utilities.Extensions
{
    public static class IListExt
    {
        public static void AddRange<T>(this IList<T> list, IEnumerable<T> items)
        {
            foreach (T item in items)
            {
                list.Add(item);
            }
        }

        public static void SafeClear<T>(this IList<T> list)
        {
            if (list.IsNotNullOrEmpty())
            {
                list.Clear();
            }
        }

        public static void AddRangeDistinct<T>(this IList<T> list, IEnumerable<T> items)
        {
            list.AddRange(items.Where(x => !list.Contains(x)));
        }

        public static string ToCommaSeparatedString(this IList<string> list, bool withAnd)
        {
            if (!list.Any())
            {
                return string.Empty;
            }

            if (withAnd && list.Count > 1)
            {
                list[list.Count - 1] = string.Concat("and ", list[list.Count - 1]);
            }

            return string.Join(", ", list);
        }
    }
}
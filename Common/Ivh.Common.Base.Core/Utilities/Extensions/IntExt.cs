﻿namespace Ivh.Common.Base.Core.Utilities.Extensions
{
    public static class IntExt
    {
        //http://csharphelper.com/blog/2014/11/convert-an-integer-into-an-ordinal-in-c/
        public static string ToOrdinal(this int value)
        {
            // start with the most common extension.
            string extension = "th";

            // examine the last 2 digits.
            int lastDigits = value % 100;

            // if the last digits are 11, 12, or 13, use th.
            if (lastDigits >= 11 && lastDigits <= 13)
            {
                return $"{value}{extension}";
            }

            // otherwise, check the last digit.
            switch (lastDigits % 10)
            {
                case 1:
                    extension = "st";
                    break;
                case 2:
                    extension = "nd";
                    break;
                case 3:
                    extension = "rd";
                    break;
            }

            return $"{value}{extension}";
        }

        public static string ToOrdinalPaymentDueDay(this int value)
        {
            if (value == 31)
            {
                return "last day";
            }

            return value.ToOrdinal();
        }
    }
}
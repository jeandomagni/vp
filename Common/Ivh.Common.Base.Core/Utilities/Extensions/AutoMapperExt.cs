﻿using AutoMapper;

namespace Ivh.Common.Base.Core.Utilities.Extensions
{
    public static class AutoMapperExt
    {
        public static void CreateMapBidirectional<TSource, TDestination>(this Profile profile)
        {
            profile.CreateMap<TSource, TDestination>();
            profile.CreateMap<TDestination, TSource>();
        }
    }
}
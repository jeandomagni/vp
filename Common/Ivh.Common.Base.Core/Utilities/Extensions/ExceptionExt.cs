﻿using System;
using System.Linq;

namespace Ivh.Common.Base.Core.Utilities.Extensions
{
    using System.Collections.Generic;
    using System.Reflection;

    public static class ExceptionExt
    {
        public static string GetExceptionDetails(this Exception exception)
        {
            PropertyInfo[] properties = exception.GetType()
                                    .GetProperties();
            IEnumerable<string> fields = properties
                             .Select(property => new
                             {
                                 Name = property.Name,
                                 Value = property.GetValue(exception, null)
                             })
                             .Select(x => String.Format(
                                 "{0} = {1}",
                                 x.Name,
                                 x.Value != null ? x.Value.ToString() : String.Empty
                             ));
            return String.Join("\n", fields);
        }
    }
}
﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace Ivh.Common.Base.Core.Utilities.Extensions
{
    public static class ObjectExt
    {
        public static bool IsNullable<T>(this T obj)
        {
            if (obj == null) return true; // obvious
            Type type = typeof(T);
            if (!type.IsValueType) return true; // ref-type
            if (Nullable.GetUnderlyingType(type) != null) return true; // Nullable<T>
            return false; // value-type
        }

        public static T[] ToArrayOfOne<T>(this T obj)
        {
            return new T[] { obj };
        }
        public static IList<T> ToListOfOne<T>(this T obj)
        {
            return new List<T>() { obj };
        }

        public static TReturn ConvertType<T, TReturn>(T value) where T : IConvertible
        {
            return (TReturn)Convert.ChangeType(value, typeof(TReturn));
        }

        public static string ToJSON<T>(this T obj, bool indented = false, bool ignoreNulls = false, bool ignoreErrors = false)
        {
            Formatting formatting = indented ? Formatting.Indented : Formatting.None;
            JsonSerializerSettings settings = new JsonSerializerSettings();
            settings.NullValueHandling = ignoreNulls ? NullValueHandling.Ignore : NullValueHandling.Include;
            settings.ReferenceLoopHandling = ReferenceLoopHandling.Serialize;
            settings.PreserveReferencesHandling = PreserveReferencesHandling.Objects;
            if (ignoreErrors)
            {
                settings.Error = delegate (object sender, ErrorEventArgs args)
                {
                    args.ErrorContext.Handled = true; // allow the serialization to continue
                };
            }
            string json = JsonConvert.SerializeObject(obj, formatting, settings);
            return json;
        }
    }
}

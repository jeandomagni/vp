﻿using System;
using System.ComponentModel;
using Ivh.Common.Base.Core.Utilities.Helpers;

namespace Ivh.Common.Base.Core.Utilities.Extensions
{
    using System.Reflection;

    public static class EnumExt
    {
        public static string GetDescription(this Enum value)
        {
            Type type = value.GetType();
            string name = Enum.GetName(type, value);
            if (name == null)
                return null;

            FieldInfo field = type.GetField(name);
            if (field == null)
                return null;

            DescriptionAttribute attr = Attribute.GetCustomAttribute(field, typeof(DescriptionAttribute)) as DescriptionAttribute;
            return attr != null ? attr.Description : null;
        }

        public static bool IsInCategory<TEnum>(this TEnum value, string category) where TEnum : struct, IConvertible
        {
            return EnumHelper<TEnum>.Contains(value, category);
        }

        public static bool IsInCategory<TEnum>(this TEnum? value, string category) where TEnum : struct, IConvertible
        {
            return value.HasValue && EnumHelper<TEnum>.Contains(value.Value, category);
        }

    }
}

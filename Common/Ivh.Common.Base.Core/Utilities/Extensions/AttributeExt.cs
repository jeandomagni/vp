﻿using System;
using System.Linq;

namespace Ivh.Common.Base.Core.Utilities.Extensions
{
    public static class AttributeExt
    {
        public static T GetAttribute<T>(this Type current) where T : Attribute
        {
            T attribute = (T)Attribute.GetCustomAttribute(current, typeof(T));
            if (attribute == null) throw new ArgumentNullException($"Attribute {typeof(T).Name} not set on class {current.Name}");
            return attribute;
        }

        public static TValue GetAttributeValue<T, TValue>(this Type current, Func<T, TValue> value) where T : Attribute
        {
            T attribute = (T)Attribute.GetCustomAttribute(current, typeof(T));
            if (attribute == null) throw new ArgumentNullException($"Attribute {typeof(T).Name} not set on class {current.Name}");
            return value(attribute);
        }

        public static bool TryGetAttribute<T>(this Type current, out T attribute) where T : Attribute
        {
            attribute = (T)Attribute.GetCustomAttribute(current, typeof(T));
            return (attribute != null);
        }

        public static bool TryGetAttributeValue<T, TValue>(this Type current, Func<T, TValue> valueFunc, out TValue value) where T : Attribute
        {
            T attribute = (T)Attribute.GetCustomAttribute(current, typeof(T));
            if (attribute != null)
            {
                try
                {
                    value = valueFunc(attribute);
                    return true;
                }
                catch (Exception)
                {
                    // ignored
                }
            }
            value = default(TValue);
            return false;
        }

        public static T GetAttribute<T>(this object current) where T : Attribute
        {
            return GetAttribute<T>(current.GetType());
        }
        public static TValue GetAttributeValue<T, TValue>(this object current, Func<T, TValue> value) where T : Attribute
        {
            T attribute = GetAttribute<T>(current.GetType());
            return value(attribute);
        }

        public static bool TryGetAttribute<T>(this object current, out T attribute) where T : Attribute
        {
            return TryGetAttribute(current.GetType(), out attribute);
        }

        public static bool TryGetAttributeValue<T, TValue>(this object current, Func<T, TValue> valueFunc, out TValue value) where T : Attribute
        {
            return TryGetAttributeValue(current.GetType(), valueFunc, out value);
        }


        public static T GetAttribute<T>(this Enum value) where T : Attribute
        {
            Type type = value.GetType();
            string name = Enum.GetName(type, value);
            return type.GetField(name) // I prefer to get attributes this way
                .GetCustomAttributes(false)
                .OfType<T>()
                .SingleOrDefault();
        }
    }
}

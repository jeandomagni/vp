﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Runtime.CompilerServices;

namespace Ivh.Common.Base.Core.Utilities.Extensions
{
    public static class AssemblyExtensions
    {
        public static bool IsCompanyAssembly(this Assembly assembly)
        {
            return assembly.HasAttribute<AssemblyCompanyAttribute>();
        }

        public static IEnumerable<AssemblyName> GetLoadedAssemblies(this Assembly assembly)
        {
            AssemblyName assemblyName = assembly.GetName();
            yield return assemblyName;
            IList<AssemblyName> tracker = new List<AssemblyName>();
            foreach (AssemblyName referencedAssemblyName in GetReferencedAssemblies(assemblyName, tracker))
            {
                yield return referencedAssemblyName;
            }
        }

        private static IEnumerable<AssemblyName> GetReferencedAssemblies(this AssemblyName assemblyName, ICollection<AssemblyName> tracker)
        {
            foreach (AssemblyName referencedAssemblyName in Assembly.Load(assemblyName).GetReferencedAssemblies())
            {
                if (tracker.All(x => x.FullName != referencedAssemblyName.FullName))
                {
                    tracker.Add(referencedAssemblyName);
                    yield return referencedAssemblyName;
                }
            }
        }

        public static IEnumerable<Type> GetLoadableTypes(this Assembly assembly)
        {
            if (assembly == null) throw new ArgumentNullException("assembly");
            try
            {
                return assembly.GetTypes();
            }
            catch (ReflectionTypeLoadException e)
            {
                return e.Types.Where(t => t != null);
            }
        }

        public static IEnumerable<Type> GetDerivedTypes<T>(this Assembly assembly, bool recursive = false)
        {
            return assembly.GetDerivedTypes(typeof(T), recursive);
        }

        public static IEnumerable<Type> GetDerivedTypes(this Assembly assembly, Type baseType, bool recursive = false)
        {
            foreach (Type derivedType in assembly.GetTypes().Where(t => t != baseType && baseType.IsAssignableFrom(t)))
            {
                yield return derivedType;
                if (recursive)
                {
                    foreach (Type derivedType2 in assembly.GetDerivedTypes(derivedType))
                    {
                        yield return derivedType2;
                    }
                }
            }

        }

        public static bool HasAttribute<TAttribute>(this Assembly assembly) where TAttribute : Attribute
        {
            return assembly.CustomAttributes.Any(x => x.AttributeType == typeof(TAttribute));
        }

        public static bool HasAttribute<TAttribute>(this MethodInfo method) where TAttribute : Attribute
        {
            return method.GetCustomAttributes(typeof(TAttribute), false).Any();
        }

        public static IEnumerable<MethodInfo> GetAsyncVoidMethods(this Assembly assembly)
        {
            return assembly.GetLoadableTypes()
                .SelectMany(type => type.GetMethods(
                    BindingFlags.NonPublic
                    | BindingFlags.Public
                    | BindingFlags.Instance
                    | BindingFlags.Static
                    | BindingFlags.DeclaredOnly))
                .Where(method => method.HasAttribute<AsyncStateMachineAttribute>())
                .Where(method => method.ReturnType == typeof(void));
        }

        public static bool IsAssemblyDebugBuild(this Assembly assembly)
        {
            return assembly.GetCustomAttributes(false).OfType<DebuggableAttribute>().Select(da => da.IsJITTrackingEnabled).FirstOrDefault();
        }
    }
}
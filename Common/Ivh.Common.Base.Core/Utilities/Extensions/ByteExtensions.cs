﻿namespace Ivh.Common.Base.Core.Utilities.Extensions
{
    using System;
    using System.Security.Cryptography;

    public static class ByteExtensions
    {
        public static string GetHashCode<THashAlgorithm>(this byte[] bytes) where THashAlgorithm : HashAlgorithm, new()
        {
            return Convert.ToBase64String(bytes.GetHashCodeBytes<THashAlgorithm>());
        }

        public static string GetHashCodeHex<THashAlgorithm>(this byte[] bytes, bool hexOnly = true) where THashAlgorithm : HashAlgorithm, new()
        {
            string returnValue = BitConverter.ToString(bytes.GetHashCodeBytes<THashAlgorithm>()).ToLower();
            return hexOnly ? returnValue.Replace("-", string.Empty) : returnValue;
        }

        public static byte[] GetHashCodeBytes<THashAlgorithm>(this byte[] bytes) where THashAlgorithm : HashAlgorithm, new()
        {
            using (THashAlgorithm provider = new THashAlgorithm())
            {
                return provider.ComputeHash(bytes);
            }
        }
    }
}

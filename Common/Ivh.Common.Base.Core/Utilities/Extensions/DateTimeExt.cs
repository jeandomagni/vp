﻿using System;
using System.Text.RegularExpressions;
using Ivh.Common.Base.Core.Strings;

namespace Ivh.Common.Base.Core.Utilities.Extensions
{
    public static class DateTimeExt
    {
        /// <summary>
        ///     This will convert a datetime object to preferred timezone datetimeoffset
        /// </summary>
        /// <param name="dateTime"></param>
        /// <param name="timeZone"></param>
        /// <returns></returns>
        public static DateTimeOffset ToDateTimeOffset(this DateTime dateTime, string timeZone)
        {
            return TimeZoneInfo.ConvertTimeBySystemTimeZoneId(dateTime.ToUniversalTime(), timeZone);
        }

        /// <summary>
        /// This converts the desired datetime in the timezone passed in into the local computers timezone.
        /// EG.  Pass in 1/1/2016 1pm (Pacific) and local timezone of mountain, it'll return 1/1/2016 2pm, because when it's 1pm pacific it'll be 2pm in mountain time.
        /// </summary>
        public static DateTimeOffset ToLocalDateTimeOffset(this DateTime clientDateTime, string clientTimeZone)
        {
            TimeZoneInfo foundTimeZoneInfo = TimeZoneInfo.FindSystemTimeZoneById(!string.IsNullOrEmpty(clientTimeZone) ? clientTimeZone : "Mountain Standard Time");

            TimeZoneInfo localTimeZone = TimeZoneInfo.Local;

            TimeSpan diff = localTimeZone.BaseUtcOffset - foundTimeZoneInfo.BaseUtcOffset;

            return new DateTimeOffset(clientDateTime).Add(diff);
        }

        /// <summary>
        /// This converts from local datetimeoffset (Something ideally created with ToLocalDateTimeOffset) back to client time.
        /// </summary>
        public static DateTime ToClientDateTime_fromLocalDateTimeOffset(this DateTimeOffset localDateTimeOffset, string clientTimeZone)
        {
            TimeZoneInfo foundTimeZoneInfo = TimeZoneInfo.FindSystemTimeZoneById(!string.IsNullOrEmpty(clientTimeZone) ? clientTimeZone : "Mountain Standard Time");

            TimeZoneInfo localTimeZone = TimeZoneInfo.Local;

            TimeSpan diff = foundTimeZoneInfo.BaseUtcOffset - localTimeZone.BaseUtcOffset;

            return localDateTimeOffset.DateTime.Add(diff);
        }

        /// <summary>
        /// Convert the datetimeoffset to the last second of the day
        /// </summary>
        public static DateTimeOffset? ToEndOfDay(this DateTimeOffset? dateTime)
        {
            return dateTime?.ToEndOfDay();
        }

        /// <summary>
        /// Convert the datetime to the last second of the day
        /// </summary>
        public static DateTime? ToEndOfDay(this DateTime? dateTime)
        {
            return dateTime?.ToEndOfDay();
        }

        /// <summary>
        /// Convert the datetimeoffset to the last second of the day
        /// </summary>
        public static DateTimeOffset ToEndOfDay(this DateTimeOffset dateTime)
        {
            return new DateTimeOffset(dateTime.Year, dateTime.Month, dateTime.Day, 23, 59, 59, dateTime.Offset);
        }

        /// <summary>
        /// Conver the datetime to the last second of the day
        /// </summary>
        /// <param name="dateTime"></param>
        /// <returns></returns>
        public static DateTime ToEndOfDay(this DateTime dateTime)
        {
            return new DateTime(dateTime.Year, dateTime.Month, dateTime.Day, 23, 59, 59);
        }


        #region IsLastDayOfMonth

        /// <summary>
        ///     Is the date the last day of the month
        /// </summary>
        /// <param name="date">Date to verify</param>
        /// <returns>True of false</returns>
        public static bool IsLastDayOfMonth(this DateTimeOffset date)
        {
            return date.AddDays(1).Day == 1;
        }

        /// <summary>
        ///     Is the date the last day of the month
        /// </summary>
        /// <param name="date">Date to verify</param>
        /// <returns>True of false</returns>
        public static bool IsLastDayOfMonth(this DateTimeOffset? date)
        {
            return date.HasValue && date.Value.IsLastDayOfMonth();
        }

        /// <summary>
        ///     Is the date the last day of the month
        /// </summary>
        /// <param name="date">Date to verify</param>
        /// <returns>True of false</returns>
        public static bool IsLastDayOfMonth(this DateTime date)
        {
            return date.AddDays(1).Day == 1;
        }

        /// <summary>
        ///     Is the date the last day of the month
        /// </summary>
        /// <param name="date">Date to verify</param>
        /// <returns>True of false</returns>
        public static bool IsLastDayOfMonth(this DateTime? date)
        {
            return date.HasValue && date.Value.IsLastDayOfMonth();
        }

        #endregion

        #region ToExpDate MMyy

        /// <summary>
        ///     Coverts a date to the expiration date format used in payment methods
        /// </summary>
        /// <param name="date">date to convert</param>
        /// <returns>string representing a payment method expiration date</returns>
        public static string ToExpDate(this DateTime date)
        {
            return date.ToString("MMyy");
        }

        /// <summary>
        ///     Coverts a date to the expiration date format used in payment methods
        /// </summary>
        /// <param name="date">date to convert</param>
        /// <returns>string representing a payment method expiration date</returns>
        public static string ToExpDate(this DateTime? date)
        {
            return date.HasValue ? date.Value.ToExpDate() : string.Empty;
        }

        /// <summary>
        ///     Coverts a date to the expiration date format used in payment methods
        /// </summary>
        /// <param name="date">date to convert</param>
        /// <returns>string representing a payment method expiration date</returns>
        public static string ToExpDate(this DateTimeOffset date)
        {
            return date.ToString("MMyy");
        }

        /// <summary>
        ///     Coverts a date to the expiration date format used in payment methods
        /// </summary>
        /// <param name="date">date to convert</param>
        /// <returns>string representing a payment method expiration date</returns>
        public static string ToExpDate(this DateTimeOffset? date)
        {
            return date.HasValue ? date.Value.ToExpDate() : string.Empty;
        }

        #endregion

        #region ToExpDateDisplay MM/yyyy

        /// <summary>
        ///     Coverts a date to the expiration date display format used in payment methods
        /// </summary>
        /// <param name="date">date to convert</param>
        /// <returns>string representing a payment method expiration date</returns>
        public static string ToExpDateDisplay(this DateTime date)
        {
            return date.ToString("MM/yyyy");
        }

        /// <summary>
        ///     Coverts a date to the expiration date display format used in payment methods
        /// </summary>
        /// <param name="date">date to convert</param>
        /// <returns>string representing a payment method expiration date</returns>
        public static string ToExpDateDisplay(this DateTime? date)
        {
            return date.HasValue ? date.Value.ToExpDate() : string.Empty;
        }

        /// <summary>
        ///     Coverts a date to the expiration date display format used in payment methods
        /// </summary>
        /// <param name="date">date to convert</param>
        /// <returns>string representing a payment method expiration date</returns>
        public static string ToExpDateDisplay(this DateTimeOffset date)
        {
            return date.ToString("MM/yyyy");
        }

        /// <summary>
        ///     Coverts a date to the expiration date display format used in payment methods
        /// </summary>
        /// <param name="date">date to convert</param>
        /// <returns>string representing a payment method expiration date</returns>
        public static string ToExpDateDisplay(this DateTimeOffset? date)
        {
            return date.HasValue ? date.Value.ToExpDate() : string.Empty;
        }

        public static bool IsBetween(this DateTimeOffset dateTimeOffset, DateTimeOffset start, DateTimeOffset end, bool inclusive = true)
        {
            return (inclusive && dateTimeOffset >= start && dateTimeOffset <= end)
                   || (!inclusive && dateTimeOffset > start && dateTimeOffset < end);
        }

        public static bool IsBetween(this DateTime date, DateTime start, DateTime end, bool inclusive = true)
        {
            return (inclusive && date >= start && date <= end)
                   || (!inclusive && date > start && date < end);
        }

        public static DateTimeOffset SetTime(this DateTimeOffset date, DateTimeOffset time)
        {
            return new DateTimeOffset(date.Date.Year, date.Date.Month, date.Date.Day, time.Hour, time.Minute, time.Second, time.Offset);
        }

        public static DateTime SetTime(this DateTime date, DateTime time)
        {
            return new DateTime(date.Date.Year, date.Date.Month, date.Date.Day, time.Hour, time.Minute, time.Second);
        }

        #endregion

        #region ToClientTime

        public static DateTimeOffset ToClientTime(this DateTimeOffset dateTime, string clientTimeZone)
        {
            TimeZoneInfo tzi = GetClientTimeZone(clientTimeZone);

            return TimeZoneInfo.ConvertTime(dateTime, tzi);
        }

        public static DateTimeOffset? ToClientTime(this DateTimeOffset? dateTime, string clientTimeZone)
        {
            return dateTime?.ToClientTime(clientTimeZone);
        }

        public static string ToClientTimeString(this DateTimeOffset dateTime, string clientTimeZone, string format = null)
        {
            DateTimeOffset dt = dateTime.ToClientTime(clientTimeZone);
            TimeZoneInfo tzi = GetClientTimeZone(clientTimeZone);
            return ToFormattedStringWithTimeZone(dt, tzi, format);
        }

        private static string ToFormattedStringWithTimeZone(DateTimeOffset dt, TimeZoneInfo tzi, string format = null)
        {
            string abbrev = GetTimeZoneAbbreviation(dt, tzi);
            return $"{dt.ToString(format ?? Format.DateTimeFormat)} {abbrev}";
        }

        private static string GetTimeZoneAbbreviation(DateTimeOffset dt, TimeZoneInfo tzi)
        {
            // todo: something more robust
            string name = tzi.IsDaylightSavingTime(dt) ? tzi.DaylightName : tzi.StandardName;
            string abbrev = Regex.Replace(name, "([a-z])", "").Replace(" ", "").Trim();
            return abbrev;
        }

        public static string ToClientTimeString(this DateTimeOffset? dateTime, string clientTimeZone, string format = null)
        {
            DateTimeOffset? dt = dateTime.ToClientTime(clientTimeZone);
            return dt.HasValue ? dt.Value.ToClientTimeString(clientTimeZone, format) : "";
        }

        private static TimeZoneInfo GetClientTimeZone(string clientTimeZone)
        {
            TimeZoneInfo tzi = null;

            try
            {
                tzi = TimeZoneInfo.FindSystemTimeZoneById(clientTimeZone);
            }
            catch (Exception)
            {
                //
            }
            finally
            {
                tzi = tzi ?? TimeZoneInfo.Local;
            }

            return tzi;
        }

        #endregion

        #region "ToMountainTime"

        /// <summary>
        /// Converts DateTimeOffset to Mountain Time Zone with auto Daylight Savings Time applied if applicable
        /// </summary>
        /// <param name="dt"></param>
        /// <returns></returns>
        public static DateTimeOffset ToMountainDateTimeOffset(this DateTimeOffset dt)
        {
            TimeZoneInfo mountain = GetMountainTimeZone();
            TimeSpan baseUtcOffset = mountain.GetUtcOffset(dt);
            DateTimeOffset mountainDateTimeOffset = dt.ToOffset(baseUtcOffset); // ToOffset() supports Daylight Savings Time
            return mountainDateTimeOffset;
        }

        /// <summary>
        /// Returns mountain time zone converted date formatted like: 01/31/2017 03:15 PM MST/MDT
        /// </summary>
        /// <param name="dt"></param>
        /// <returns></returns>
        public static string ToMountainDateTimeString(this DateTimeOffset dt)
        {
            DateTimeOffset mdt = ToMountainDateTimeOffset(dt);
            TimeZoneInfo tzi = GetMountainTimeZone();
            string result = ToFormattedStringWithTimeZone(mdt, tzi);
            return result;
        }

        /// <summary>
        /// Gets the TimeZoneInfo for "Mountain Standard Time" that observes Daylight Savings Time
        /// </summary>
        /// <returns></returns>
        private static TimeZoneInfo GetMountainTimeZone()
        {
            // "Mountain Standard Time" observes Daylight Savings Time
            // "US Mountain Standard Time" DOES NOT observe Daylight Savings Time
            TimeZoneInfo tzi = TimeZoneInfo.FindSystemTimeZoneById("Mountain Standard Time");
            return tzi;
        }

        #endregion
    }
}
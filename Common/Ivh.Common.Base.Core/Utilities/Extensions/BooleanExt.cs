﻿namespace Ivh.Common.Base.Core.Utilities.Extensions
{
    public static class BooleanExt
    {
        public static bool TryParseEx(this string value, out bool result)
        {
            value = value.Trim().ToLower();
            if (bool.TryParse(value, out result))
            {
                return true;
            }
            switch (value)
            {
                case "yes":
                case "y":
                case "1":
                case "on":
                case "t":
                    result = true;
                    return true;
                case "no":
                case "n":
                case "0":
                case "off":
                case "f":
                    result = false;
                    return true;
            }
            return false;
        }
    }
}

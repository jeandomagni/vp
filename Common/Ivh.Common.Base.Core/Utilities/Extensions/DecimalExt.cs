﻿using System;
using System.Globalization;

namespace Ivh.Common.Base.Core.Utilities.Extensions
{
    public static class DecimalExt
    {
        /// <summary>
        /// Format decimal amount to negative amount with a minus in front of the amount
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static string FormatCurrency(this decimal value)
        {
            NumberFormatInfo currencyFormat = new CultureInfo(CultureInfo.CurrentCulture.ToString()).NumberFormat;
            currencyFormat.CurrencyNegativePattern = 1;
            return String.Format(currencyFormat, "{0:c}", value);
        }

        public static string FormatCurrency(this decimal? value)
        {
            return value.GetValueOrDefault(0).FormatCurrency();
        }

        public static bool IsBetween(this decimal value, decimal min, decimal max, bool inclusive = true)
        {
            return (value > min && value < max) || (inclusive && (value == min || value == max));
        }
    }
}

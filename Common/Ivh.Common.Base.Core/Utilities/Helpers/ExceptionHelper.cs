﻿using System;
using System.Text;
using Ivh.Common.Base.Core.Utilities.Extensions;

namespace Ivh.Common.Base.Core.Utilities.Helpers
{
    public static class ExceptionHelper
    {
        public static string AggregateExceptionToString(Exception e)
        {
            StringBuilder returnString = new StringBuilder();
            Exception currentException = e;
            while (currentException != null)
            {
                returnString.AppendLine(currentException.GetExceptionDetails());

                if (currentException.Data.Count > 0)
                {
                    foreach (object key in currentException.Data)
                    {
                        returnString.AppendLine(string.Format("{0}: {1}", key, currentException.Data[key]));
                    }
                }

                currentException = currentException.InnerException;
            }
            return returnString.ToString();
        }
    }
}
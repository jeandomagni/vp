﻿using System;
using System.Collections.Generic;

namespace Ivh.Common.Base.Core.Utilities.Helpers
{
    public static class TypeHelper
    {
        public static Type GetTypeFromPrimitiveType(string primitiveTypeName)
        {
            Dictionary<string, Type> primitiveTypes = new Dictionary<string, Type>()
            {
                {"object",typeof(object)} ,
                {"string",typeof(string)} ,
                {"bool",typeof(bool)} ,
                {"byte",typeof(byte)} ,
                {"char",typeof(char)} ,
                {"DateTime",typeof(DateTime)} ,
                {"decimal",typeof(decimal)} ,
                {"double",typeof(double)} ,
                {"short",typeof(short)} ,
                {"int",typeof(int)} ,
                {"long",typeof(long)} ,
                {"sbyte",typeof(sbyte)} ,
                {"float",typeof(float)} ,
                {"ushort",typeof(ushort)} ,
                {"uint",typeof(uint)} ,
                {"ulong",typeof(ulong)} ,
                {"void",typeof(void)} ,
            };
            return primitiveTypes[primitiveTypeName];
        }

        public static List<string>  GetPrimitiveTypeNames()
        {
            return new List<string>(new string[] { "byte", "sbyte", "int", "uint", "short", "ushort", "long", "ulong", "float", "double", "char", "bool", "object", "string", "decimal", "DateTime" });
        }
    }
}

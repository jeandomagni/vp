﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Ivh.Common.Base.Core.Utilities.Helpers
{
    public static class MergeScriptHelper
    {

        public static string CreateMergeScript<T>(string schemaName, string tableName, string keyColumnName, IList<T> data, string hashCode, bool deleteUnmatchedRows = false)
        {
            List<String> columnNames = data.First().GetType().GetProperties().Select(x => x.Name).ToList();
            StringBuilder sb = new StringBuilder();
            sb.AppendLine($"--EXEC [dbo].[sp_generate_merge_testdata] @table_name = '{tableName}', @schema = '{schemaName}', @include_use_db = 0, @nologo = 1");
            sb.AppendLine();
            sb.AppendLine();
            sb.AppendLine("SET NOCOUNT ON");
            sb.AppendLine();
            sb.AppendLine("-- HashCode:" + hashCode);
            sb.AppendLine($"MERGE INTO [{schemaName}].[{tableName}] AS Target");
            sb.AppendLine("USING (VALUES");

            for (int i = 0; i < data.Count; i++)
            {
                T row = data[i];
                string linePrefix = i == 0 ? "  (" : " ,(";
                IEnumerable<object> rowValues = columnNames.Select(x => row.GetType().GetProperty(x).GetValue(row, null));
                IEnumerable<string> rowStringValues = rowValues.Select(x => x == null ? "NULL" : "'" + x.ToString().Replace("'", "''") + "'");
                string lineValues = string.Join(",", rowStringValues);
                string lineSuffix = ")";
                sb.AppendLine(linePrefix + lineValues + lineSuffix);
            }

            string columnNamesDelimited = string.Join(",", columnNames.Select(x=>$"[{x}]"));
            sb.AppendLine($") AS Source ({columnNamesDelimited})");
            sb.AppendLine($"ON (Target.[{keyColumnName}] = Source.[{keyColumnName}])");

            //MATCH
            sb.AppendLine("WHEN MATCHED AND (");
            List<string> columnNamesWithoutKeyColumn = columnNames.Where(x => x != keyColumnName).ToList();
            for (int i = 0; i < columnNamesWithoutKeyColumn.Count; i++)
            {
                string lineSuffix = (i == columnNamesWithoutKeyColumn.Count - 1) ? ") THEN" : " OR";
                string columnName = columnNamesWithoutKeyColumn[i];
                sb.AppendLine($"	NULLIF(Source.[{columnName}], Target.[{columnName}]) IS NOT NULL OR NULLIF(Target.[{columnName}], Source.[{columnName}]) IS NOT NULL{lineSuffix}");
            }

            //UPDATE
            sb.AppendLine(" UPDATE SET");
            for (int i = 0; i < columnNamesWithoutKeyColumn.Count; i++)
            {
                string lineSuffix = (i == columnNamesWithoutKeyColumn.Count - 1) ? String.Empty : ",";
                string columnName = columnNamesWithoutKeyColumn[i];
                sb.AppendLine($"   [{columnName}] = Source.[{columnName}]{lineSuffix}");
            }

            //INSERT
            sb.AppendLine("WHEN NOT MATCHED BY TARGET THEN");
            sb.Append($" INSERT({columnNamesDelimited})");

            string sourceColumnNamesDelimited = string.Join(",", columnNames.Select(x => $"Source.[{x}]"));
            sb.AppendLine($" VALUES({sourceColumnNamesDelimited})" + (deleteUnmatchedRows ? "" : ";"));

            //DELETE
            sb.AppendLine((deleteUnmatchedRows ? "" : "--") + "WHEN NOT MATCHED BY SOURCE THEN ");
            sb.AppendLine((deleteUnmatchedRows ? "" : "--") + " DELETE;");

            //ERROR HANDLING
            sb.AppendLine("DECLARE @mergeError int");
            sb.AppendLine(" , @mergeCount int");
            sb.AppendLine("SELECT @mergeError = @@ERROR, @mergeCount = @@ROWCOUNT");
            sb.AppendLine("IF @mergeError != 0");
            sb.AppendLine(" BEGIN");
            sb.AppendLine($" PRINT 'ERROR OCCURRED IN MERGE FOR {schemaName}].[{tableName}]. Rows affected: ' + CAST(@mergeCount AS VARCHAR(100)); -- SQL should always return zero rows affected");
            sb.AppendLine(" END");
            sb.AppendLine("ELSE");
            sb.AppendLine(" BEGIN");
            sb.AppendLine($" PRINT '[{schemaName}].[{tableName}] rows affected by MERGE: ' + CAST(@mergeCount AS VARCHAR(100));");
            sb.AppendLine(" END");
            sb.AppendLine("");

            return sb.ToString();
        }

    }
}

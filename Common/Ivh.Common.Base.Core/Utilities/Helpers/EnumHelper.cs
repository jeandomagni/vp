﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Ivh.Common.Base.Core.Enums;
using Ivh.Common.Base.Core.Utilities.Extensions;

namespace Ivh.Common.Base.Core.Utilities.Helpers
{
    public static class EnumHelper<TEnum> where TEnum : struct, IConvertible
    {
        private static IEnumerable<IGrouping<string, TEnum>> _cachedGrouping;
        private static readonly object _lock = new object();

        public static IEnumerable<TEnum> GetValues(string category)
        {
            if (_cachedGrouping == null)
            {
                lock (_lock)
                {
                    if (_cachedGrouping == null)
                    {
                        Type enumType = typeof(TEnum);
                        Type attributeType = typeof(EnumCategoryAttribute);
                        _cachedGrouping = Enum.GetValues(enumType)
                            .Cast<TEnum>()
                            .SelectMany(x =>
                            {
                                MemberInfo[] info = enumType.GetMember(x.ToString());
                                object[] attributes = info[0].GetCustomAttributes(attributeType, false);
                                if (attributes.IsNotNullOrEmpty())
                                {
                                    return ((EnumCategoryAttribute)attributes[0]).Categories.Select(y => new Entry { Category = y, EnumValue = x });
                                }
                                return Enumerable.Empty<Entry>();
                            })
                            .Where(x => !string.IsNullOrEmpty(x.Category))
                            .GroupBy(x => x.Category, x => x.EnumValue)
                            .ToList();

                        if (_cachedGrouping.IsNullOrEmpty())
                        {
                            _cachedGrouping = Enumerable.Empty<IGrouping<string, TEnum>>();
                        }
                    }
                }
            }
            return _cachedGrouping.Where(x => x.Key.Equals(category, StringComparison.OrdinalIgnoreCase)).SelectMany(x => x);
        }

        public static IEnumerable<TEnum> GetValues(params string[] categories)
        {
            return categories.Select(GetValues).SelectMany(x => x).Distinct();
        }

        public static IEnumerable<TEnum> GetValuesIntersect(params string[] categories)
        {
            return categories.Select(GetValues).IntersectAll();
        }

        public static IEnumerable<TEnum> GetValuesNot(params string[] categories)
        {
            Type enumType = typeof(TEnum);
            Type attributeType = typeof(EnumCategoryAttribute);

            List<TEnum> allValues = Enum.GetValues(enumType).Cast<TEnum>().ToList();
            List<TEnum> acceptedValues = new List<TEnum>();
            allValues.ForEach(x =>
            {
                MemberInfo[] info = enumType.GetMember(x.ToString());
                object[] attributes = info[0].GetCustomAttributes(attributeType, false);
                if (!attributes.IsNotNullOrEmpty())
                    return;

                EnumCategoryAttribute attribute = (EnumCategoryAttribute)attributes[0];
                string[] array = attribute.Categories;
                if (!array.Intersect(categories).Any())
                    acceptedValues.Add(x);
            });

            return acceptedValues;
        }

        public static bool Contains(TEnum value, string category)
        {
            return GetValues(category).Contains(value);
        }

        public static bool Contains(TEnum value, params string[] categories)
        {
            return GetValues(categories).Contains(value);
        }

        public static bool ContainsIntersect(TEnum value, params string[] categories)
        {
            return GetValuesIntersect(categories).Contains(value);
        }

        private class Entry
        {
            public string Category { get; set; }
            public TEnum EnumValue { get; set; }
        }
    }
}
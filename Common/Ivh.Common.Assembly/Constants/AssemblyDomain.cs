﻿namespace Ivh.Common.Assembly.Constants
{
    public static class AssemblyDomain
    {
        public const string Hospital = nameof(Hospital);
        public const string VisitPay = nameof(VisitPay);
        public const string BalanceTransfer = nameof(BalanceTransfer);
        public const string Common = nameof(Common);
        public const string Test = nameof(Test);
        public const string Scoring = nameof(Scoring);
    }
}

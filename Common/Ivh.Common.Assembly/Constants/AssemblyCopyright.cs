﻿namespace Ivh.Common.Assembly.Constants
{
    public static class AssemblyCopyright
    {
        public const string Ivh = "Copyright © iVinci Health, LLC 2019";
    }
}

﻿namespace Ivh.Common.Assembly.Constants
{
    public static class AssemblyApplicationLayer
    {
        public const string Common = nameof(Common);
        public const string Web = nameof(Web);
        public const string Console = nameof(Console);
        public const string Application = nameof(Application);
        public const string Domain = nameof(Domain);
        public const string Provider = nameof(Provider);

    }
}

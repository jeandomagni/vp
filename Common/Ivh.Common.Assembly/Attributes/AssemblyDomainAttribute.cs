﻿namespace Ivh.Common.Assembly.Attributes
{
    using System;

    [AttributeUsage(AttributeTargets.Assembly)]
    public class AssemblyDomainAttribute : Attribute
    {
        public string Value { get; private set; }

        public AssemblyDomainAttribute() : this(string.Empty) { }
        public AssemblyDomainAttribute(string value) { this.Value = value; }
    }
}

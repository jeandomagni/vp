﻿namespace Ivh.Common.Assembly.Attributes
{
    using System;

    [AttributeUsage(AttributeTargets.Assembly)]
    public class AssemblyApplicationLayerAttribute : Attribute
    {
        public string Value { get; private set; }

        public AssemblyApplicationLayerAttribute() : this(string.Empty) { }
        public AssemblyApplicationLayerAttribute(string value) { this.Value = value; }
    }
}

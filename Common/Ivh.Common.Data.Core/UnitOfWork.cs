﻿namespace Ivh.Common.Data.Core
{
    using System;
    using System.Data;
    using Interfaces;
    using NHibernate;

    public class UnitOfWork : IUnitOfWork
    {
        private readonly ISession _session;
        private readonly ITransaction _transaction;
        private readonly bool _topLevel = true;

        public UnitOfWork(ISession session) : this(session, Constants.IsolationLevel.Default)
        {

        }

        public UnitOfWork(ISession session, IsolationLevel isolationLevel)
        {
            this._session = session;
            if (this._session.Transaction == null || !this._session.Transaction.IsActive)
            {
                this._transaction = this._session.BeginTransaction(isolationLevel);
                this._topLevel = true;
            }
            else
            {
                this._topLevel = false;
            }
        }

        public void Commit()
        {
            if (this._topLevel)
            {
                if (!this._session.IsConnected)
                {
                    throw new InvalidOperationException("Oops! We don't have a connected session");
                }
                if (!this._session.IsOpen)
                {
                    throw new InvalidOperationException("Oops! We don't have an open session");
                }
                if (!this._transaction.IsActive)
                {
                    throw new InvalidOperationException("Oops! We don't have an active transaction");
                }
                this._session.Flush();
                this._transaction.Commit();
            }
        }

        public void Rollback()
        {
            if (this._topLevel
                && this._session.IsConnected
                && this._session.IsOpen
                && this._transaction.IsActive)
            {
                this._session.Flush();
                this._transaction.Rollback();
            }
        }

        public void Dispose()
        {
            //If the transaction is still open it will automatically roll it back.
            this.Rollback();
        }
    }
}
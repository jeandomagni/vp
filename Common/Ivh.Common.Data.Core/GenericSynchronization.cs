﻿using System;

namespace Ivh.Common.Data.Core
{
    public class GenericSynchronization : NHibernate.Transaction.ISynchronization
    {
        private Action CallMeBackCommit { get; set; }
        private Action CallMeBackRollback { get; set; }
        public GenericSynchronization(Action myCommitAction, Action myRollbackAction)
        {
            this.CallMeBackCommit = myCommitAction;
            this.CallMeBackRollback = myRollbackAction;
        }
        public void BeforeCompletion()
        {
        }

        public void AfterCompletion(bool success)
        {
            if (success)
            {
                this.CallMeBackCommit?.Invoke();
            }
            if (!success)
            {
                this.CallMeBackRollback?.Invoke();
            }
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using Ivh.Common.Data.Core.Interfaces;
using NHibernate;

namespace Ivh.Common.Data.Core
{
    public abstract class RepositoryBase<T> : IRepository<T> where T : class
    {
        private readonly ISession _session;
        private readonly IStatelessSession _statelessSession;
        private NHibernate.Transaction.ISynchronization _baseSynchronization;

        protected RepositoryBase(ISession session)
        {
            this._session = session;
        }

        protected RepositoryBase(ISession session, IStatelessSession statelessSession)
        {
            this._session = session;
            this._statelessSession = statelessSession;
        }

        protected Action CallBackAfterCommit { get; set; }
        protected Action CallBackAfterRollback { get; set; }

        protected ISession Session => this._session;

        protected IStatelessSession StatelessSession => this._statelessSession;

        public virtual void InsertOrUpdate(T entity)
        {
            if (this._session.Transaction.IsActive)
            {
                this.CheckIfCallBackNeedsRegistration();
                this._session.SaveOrUpdate(entity);
            }
            else
            {
                using (ITransaction transaction = this._session.BeginTransaction(Constants.IsolationLevel.Default))
                {
                    this.CheckIfCallBackNeedsRegistration(true);
                    this._session.SaveOrUpdate(entity);
                    transaction.Commit();
                }
            }
        }

        private void CheckIfCallBackNeedsRegistration(bool force = false)
        {
            if ((this._baseSynchronization == null || force) && (this.CallBackAfterCommit != null || this.CallBackAfterRollback != null))
            {
                if (this._baseSynchronization == null)
                {
                    this._baseSynchronization = new GenericSynchronization(this.CallBackAfterCommit, this.CallBackAfterRollback);
                }
                this._session.Transaction.RegisterSynchronization(this._baseSynchronization);
            }
        }

        public virtual void Update(T entity)
        {
            if (this._session.Transaction.IsActive)
            {
                this.CheckIfCallBackNeedsRegistration();
                this._session.Update(entity);
            }
            else
            {
                using (ITransaction transaction = this._session.BeginTransaction(Constants.IsolationLevel.Default))
                {
                    this.CheckIfCallBackNeedsRegistration(true);
                    this._session.Update(entity);
                    transaction.Commit();
                }
            }
        }

        public virtual void Insert(T entity)
        {
            if (this._session.Transaction.IsActive)
            {
                this.CheckIfCallBackNeedsRegistration();
                this._session.Save(entity);
            }
            else
            {
                using (ITransaction transaction = this._session.BeginTransaction(Constants.IsolationLevel.Default))
                {
                    this.CheckIfCallBackNeedsRegistration(true);
                    this._session.Save(entity);
                    transaction.Commit();
                }
            }
        }

        public virtual void Delete(T entity)
        {
            if (this._session.Transaction.IsActive)
            {
                this.CheckIfCallBackNeedsRegistration();
                this._session.Delete(entity);
            }
            else
            {
                using (ITransaction transaction = this._session.BeginTransaction(Constants.IsolationLevel.Default))
                {
                    this.CheckIfCallBackNeedsRegistration(true);
                    this._session.Delete(entity);
                    transaction.Commit();
                }
            }
        }

        public virtual void BulkInsert(IList<T> entityList)
        {
            if (this._statelessSession == null)
            {
                throw new NullReferenceException("Stateless Session has not been initalized.");
            }
            using (ITransaction transaction = this._statelessSession.BeginTransaction())
            {
                foreach (T entity in entityList)
                {
                    this._statelessSession.Insert(entity);
                }
                transaction.Commit();
            }
        }

        public virtual IQueryable<T> GetAll()
        {
            return this._session.Query<T>();
        }

        public virtual T GetById(int id)
        {
            return this._session.Get<T>(id);
        }

        public virtual IQueryable<T> GetQueryable()
        {
            return this._session.Query<T>();
        }
    }
}

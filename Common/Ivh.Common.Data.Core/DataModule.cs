﻿namespace Ivh.Common.Data.Core
{
    using System;
    using Conventions;
    using FluentNHibernate.Cfg;
    using FluentNHibernate.Cfg.Db;
    using Interfaces;
    using NHibernate;
    using Microsoft.Extensions.DependencyInjection;

    public class DataModule
    {
        
        public static ISessionFactory CreateSessionfactory(IFluentMappingModule module, string connectionString, string applicationName)
        {
            string commandTimeout = "30";

            FluentConfiguration fluentConfiguration = Fluently.Configure();
            MsSqlConfiguration msSqlConfiguration = MsSqlConfiguration
                .MsSql2012.ConnectionString(connectionString);
            ISessionFactory sessionFactory = fluentConfiguration
                .Database(msSqlConfiguration)
                .Mappings(module.MapAssemblies)
                .Mappings(m => m.FluentMappings.Conventions.Add<DateTimeConvention>())
                .ExposeConfiguration(
                    cfg =>
                    {
                        cfg.SetProperty("command_timeout", commandTimeout);
                        cfg.SetProperty("adonet.batch_size", "1");

                        string cs = cfg.GetProperty("connection.connection_string");
                        cfg.SetProperty("connection.connection_string", cs + $";Application Name={applicationName}");
                    })
                .BuildSessionFactory();
            return sessionFactory;
        }

        public static ISession OpenSession(IServiceProvider serviceProvider)
        {
            ISession session = serviceProvider.GetRequiredService<ISessionFactory>().OpenSession();
            SetupSession(session);
            return session;
        }

        //public static ISession OpenSessionNamed(ServiceProvider context, string name)
        //{
        //    ISession session = context.GetRequiredService<ISessionFactory>(name).OpenSession();
        //    SetupSession(session);

        //    return session;
        //}

        public static IStatelessSession OpenStatelessSession(IServiceProvider serviceProvider)
        {
            IStatelessSession statelessSession = serviceProvider.GetRequiredService<ISessionFactory>().OpenStatelessSession();
            return statelessSession;
        }

        //public static IStatelessSession OpenStatelessSessionNamed(ServiceProvider context, string name)
        //{
        //    IStatelessSession statelessSession = context.GetRequiredService<ISessionFactory>(name).OpenStatelessSession();
        //    return statelessSession;
        //}

        private static void SetupSession(ISession session)
        {
            //This doesnt seem to help all that much.  But seems safer.
            //session.FlushMode = FlushMode.Always;
        }
    }
}
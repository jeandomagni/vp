﻿namespace Ivh.Common.Data.Core.Constants
{
    public static class IsolationLevel
    {
        // This is the default isolation level for the system. Putting this in in anticipation of testing ReadCommitted
        public const System.Data.IsolationLevel Default = System.Data.IsolationLevel.ReadUncommitted;
        public const System.Data.IsolationLevel ReadUncommitted = System.Data.IsolationLevel.ReadUncommitted;
        public const System.Data.IsolationLevel ReadCommitted = System.Data.IsolationLevel.ReadCommitted;
    }
}

﻿using System.Collections.Generic;
using System.Linq;

namespace Ivh.Common.Data.Core.Interfaces
{
    public interface IRepository<T>
    {
        void InsertOrUpdate(T entity);
        void Update(T entity);
        void Insert(T entity);
        void Delete(T entity);
        IQueryable<T> GetAll();
        T GetById(int id);
        void BulkInsert(IList<T> entityList);
        IQueryable<T> GetQueryable();

    }
}
﻿using System;

namespace Ivh.Common.Data.Core.Interfaces
{
    public interface IUnitOfWork : IDisposable
    {
        void Commit();
        void Rollback();
    }
}

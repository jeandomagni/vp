﻿namespace Ivh.Common.Data.Core.Interfaces
{
    using FluentNHibernate.Cfg;

    public interface IFluentMappingModule
    {
        void MapAssemblies(MappingConfiguration m);
    }
}
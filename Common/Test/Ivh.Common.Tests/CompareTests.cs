﻿namespace Ivh.Common.Tests
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Base.Utilities.Helpers;
    using NUnit.Framework;

    [TestFixture]
    public class CompareTests
    {
        [Test]
        public void BasicCompareChangesNoChanges()
        {
            TestClass testClass = TestClass.GetTestClass();
            Compare<TestClass> compare = Compare<TestClass>.InitializeWith(testClass);
            compare.CompareTo(testClass);

            Assert.IsFalse(compare.HasChanged(x => x.DateTimeMember));
            Assert.IsFalse(compare.HasChanged(x => x.NullableDateTimeMember));
            Assert.IsFalse(compare.HasChanged(x => x.IntegerMember));
            Assert.IsFalse(compare.HasChanged(x => x.NullableIntegerMember));
            Assert.IsFalse(compare.HasChanged(x => x.DecimalMember));
            Assert.IsFalse(compare.HasChanged(x => x.NullableDecimalMember));
            Assert.IsFalse(compare.HasChanged(x => x.BoolMember));
            Assert.IsFalse(compare.HasChanged(x => x.NullableBoolMember));
            Assert.IsFalse(compare.HasChanged(x => x.StringMember));
        }

        [Test]
        public void BasicCompareChangesAllChanged()
        {
            TestClass testClass = TestClass.GetTestClass();
            Compare<TestClass> compare = Compare<TestClass>.InitializeWith(testClass);
            testClass.ChangeAllValues();
            compare.CompareTo(testClass);

            Assert.IsTrue(compare.HasChanged(x => x.DateTimeMember));
            Assert.IsTrue(compare.HasChanged(x => x.NullableDateTimeMember));
            Assert.IsTrue(compare.HasChanged(x => x.IntegerMember));
            Assert.IsTrue(compare.HasChanged(x => x.NullableIntegerMember));
            Assert.IsTrue(compare.HasChanged(x => x.DecimalMember));
            Assert.IsTrue(compare.HasChanged(x => x.NullableDecimalMember));
            Assert.IsTrue(compare.HasChanged(x => x.BoolMember));
            Assert.IsTrue(compare.HasChanged(x => x.NullableBoolMember));
            Assert.IsTrue(compare.HasChanged(x => x.StringMember));
        }

        [Test]
        public void BasicCompareChangesChangedToNull()
        {
            TestClass testClass = TestClass.GetTestClass();
            Compare<TestClass> compare = Compare<TestClass>.InitializeWith(testClass);
            testClass.NullableBoolMember = null;
            testClass.NullableDateTimeMember = null;
            testClass.NullableDecimalMember = null;
            testClass.NullableIntegerMember = null;
            compare.CompareTo(testClass);

            Assert.IsFalse(compare.HasChanged(x => x.DateTimeMember));
            Assert.IsTrue(compare.HasChanged(x => x.NullableDateTimeMember));
            Assert.IsFalse(compare.HasChanged(x => x.IntegerMember));
            Assert.IsTrue(compare.HasChanged(x => x.NullableIntegerMember));
            Assert.IsFalse(compare.HasChanged(x => x.DecimalMember));
            Assert.IsTrue(compare.HasChanged(x => x.NullableDecimalMember));
            Assert.IsFalse(compare.HasChanged(x => x.BoolMember));
            Assert.IsTrue(compare.HasChanged(x => x.NullableBoolMember));
            Assert.IsFalse(compare.HasChanged(x => x.StringMember));
        }


        [Test]
        public void BasicCompareListChangesNoChanges()
        {
            int count = 5;
            TestClassList testClasses = TestClassList.GetTestClassList(count);
            CompareList<TestClass> compare = CompareList<TestClass>.InitializeWith(testClasses);
            compare.CompareTo(testClasses, x => x.Id);
            count = 0;
            Assert.AreEqual(count,compare.HasChanged(x => x.DateTimeMember).Count());
            Assert.AreEqual(count,compare.HasChanged(x => x.NullableDateTimeMember).Count());
            Assert.AreEqual(count,compare.HasChanged(x => x.IntegerMember).Count());
            Assert.AreEqual(count,compare.HasChanged(x => x.NullableIntegerMember).Count());
            Assert.AreEqual(count,compare.HasChanged(x => x.DecimalMember).Count());
            Assert.AreEqual(count,compare.HasChanged(x => x.NullableDecimalMember).Count());
            Assert.AreEqual(count,compare.HasChanged(x => x.BoolMember).Count());
            Assert.AreEqual(count,compare.HasChanged(x => x.NullableBoolMember).Count());
            Assert.AreEqual(count,compare.HasChanged(x => x.StringMember).Count());
        }

        [Test]
        public void BasicCompareListChangesAllChanged()
        {
            int count = 5;
            TestClassList testClasses = TestClassList.GetTestClassList(count);
            CompareList<TestClass> compare = CompareList<TestClass>.InitializeWith(testClasses);
            testClasses.ChangeAllValues();
            compare.CompareTo(testClasses, x => x.Id);
            
            Assert.AreEqual(count,compare.HasChanged(x => x.DateTimeMember).Count());
            Assert.AreEqual(count,compare.HasChanged(x => x.NullableDateTimeMember).Count());
            Assert.AreEqual(count,compare.HasChanged(x => x.IntegerMember).Count());
            Assert.AreEqual(count,compare.HasChanged(x => x.NullableIntegerMember).Count());
            Assert.AreEqual(count,compare.HasChanged(x => x.DecimalMember).Count());
            Assert.AreEqual(count,compare.HasChanged(x => x.NullableDecimalMember).Count());
            Assert.AreEqual(count,compare.HasChanged(x => x.BoolMember).Count());
            Assert.AreEqual(count,compare.HasChanged(x => x.NullableBoolMember).Count());
            Assert.AreEqual(count,compare.HasChanged(x => x.StringMember).Count());
        }

        public class TestClassList : List<TestClass>
        {
            public static TestClassList GetTestClassList(int count)
            {
                TestClassList testClassList = new TestClassList();
                for (int i = 0; i < count; i++)
                {
                    testClassList.Add(new TestClass()
                    {
                        DateTimeMember = DateTime.MinValue,
                        NullableDateTimeMember = DateTime.MinValue,
                        IntegerMember = 0,
                        NullableIntegerMember = 0,
                        DecimalMember = 0m,
                        NullableDecimalMember = 0m,
                        BoolMember = false,
                        NullableBoolMember = false,
                        StringMember = nameof(TestClass.StringMember)
                    });
                }

                return testClassList;
            }

            public void ChangeAllValues()
            {
                foreach (TestClass testClass in this)
                {
                    testClass.ChangeAllValues();
                }
            }
        }

        [Serializable]
        public class TestClass
        {
            public static TestClass GetTestClass(int increment = 0)
            {
                return new TestClass()
                {
                    Id = increment,
                    DateTimeMember = DateTime.MinValue.Add(TimeSpan.FromMinutes(Convert.ToDouble(increment))),
                    NullableDateTimeMember = DateTime.MinValue.Add(TimeSpan.FromMinutes(Convert.ToDouble(increment))),
                    IntegerMember = increment,
                    NullableIntegerMember = increment,
                    DecimalMember = Convert.ToDecimal(increment),
                    NullableDecimalMember = Convert.ToDecimal(increment),
                    BoolMember = increment % 1 == 0,
                    NullableBoolMember = increment % 1 == 0,
                    StringMember = nameof(StringMember)
                };
            }

            public void ChangeAllValues()
            {
                this.DateTimeMember = DateTime.MaxValue;
                this.NullableDateTimeMember = DateTime.MaxValue;
                this.IntegerMember++;
                this.NullableIntegerMember++;
                this.DecimalMember++;
                this.NullableDecimalMember++;
                this.BoolMember = !this.BoolMember;
                this.NullableBoolMember = !this.NullableBoolMember;
                this.StringMember = this.StringMember + "T";
            }

            public int Id { get; set; }

            public DateTime DateTimeMember { get; set; }
            public DateTime? NullableDateTimeMember { get; set; }
            public int IntegerMember { get; set; }
            public int? NullableIntegerMember { get; set; }
            public decimal DecimalMember { get; set; }
            public decimal? NullableDecimalMember { get; set; }
            public bool BoolMember { get; set; }
            public bool? NullableBoolMember { get; set; }
            public string StringMember { get; set; }
        }
    }
}

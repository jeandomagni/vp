﻿using System;
using NUnit.Framework;
using System.Linq;
using System.IO;
using System.Collections.Generic;

namespace Ivh.Common.Tests
{
    using System.Reflection;
    using Assembly.Attributes;
    using Assembly.Constants;

    [TestFixture]
    public class DomainReferenceTest
    {
        // This method works correctly when using the NUnit Test Runner 
        // Resharper's test runner does not work because all the assemblies are not put into the same folder
        private List<Assembly> GetIvhAssemblies()
        {
            List<Assembly> ivhAssemblies = new List<Assembly>();
            string path = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
            DirectoryInfo di = new DirectoryInfo(path);
            FileInfo[] dlls = di.GetFiles("*.dll", SearchOption.AllDirectories);
            FileInfo[] exes = di.GetFiles("*.exe", SearchOption.AllDirectories);

            Dictionary<string, string> ivhAssemblyDict = new Dictionary<string, string>();
            IEnumerable<FileInfo> ivhFileInfosDll = dlls.Where(f => f.Name.ToUpper().StartsWith("IVH"));
            IEnumerable<FileInfo> ivhFileInfosExe = exes.Where(f => f.Name.ToUpper().StartsWith("IVH"));
            IEnumerable<FileInfo> ivhFileInfos = ivhFileInfosDll.Concat(ivhFileInfosExe);

            foreach (FileInfo f in ivhFileInfos)
            {
                if (!ivhAssemblyDict.ContainsKey(f.Name))
                {
                    ivhAssemblyDict.Add(f.Name, f.FullName);
                }
            }

            foreach (KeyValuePair<string, string> assemblyName in ivhAssemblyDict)
            {
                ivhAssemblies.Add(Assembly.LoadFile(assemblyName.Value));
            }
            return ivhAssemblies;
        }

        [Test]
        public void Assemblies_AllHaveIvhDomainAttribute()
        {

            List<Assembly> ivhAssemblies = this.GetIvhAssemblies();

            List<Assembly> ivhAssembliesMissingDomainAttribute = 
                ivhAssemblies.Where(a => !a.IsDefined(typeof(AssemblyDomainAttribute), false)).ToList(); // Does not have an assembly attribute named "AssemblyDomain"
            bool hasErrors = ivhAssembliesMissingDomainAttribute.Any();
            string message = "";
            if (hasErrors)
            {
                message = "The following assemblies do not have the AssemblyDomain attribute:" + Environment.NewLine 
                    + string.Join(Environment.NewLine, ivhAssembliesMissingDomainAttribute.Select(x => x.GetName().Name));
            }
            Assert.IsFalse(hasErrors, message);
        }

        [Test]
        [Ignore("Not ready until assemblies have been changed to allow it to pass")]
        public void Assemblies_VisitPayToHospitalDirect()
        {

            List<Assembly> ivhAssemblies = GetIvhAssemblies();

            List<string> visitPay2HospitalList = new List<string>();
            List<string> noDomainAttributeList = new List<string>();

            foreach (Assembly a in ivhAssemblies)
            {
                if (a.IsDefined(typeof(AssemblyDomainAttribute), false))
                {
                    AssemblyDomainAttribute attribute = (AssemblyDomainAttribute)a.GetCustomAttributes(typeof(AssemblyDomainAttribute), false).First();
                    if (attribute.Value == AssemblyDomain.VisitPay)
                    {
                        var referencedDomainAssemblyNames = a
                            .GetReferencedAssemblies()
                            .Select(b => new { FullName = b.FullName, Name = b.Name})
                            .Where(b => b.Name.ToUpper().StartsWith("IVH"))
                            .ToList();

                       IEnumerable<Assembly> referencedDomainAssemblies = referencedDomainAssemblyNames.Select(x => Assembly.Load(x.FullName));

                        foreach (Assembly b in referencedDomainAssemblies)
                        {
                            if (b.IsDefined(typeof(AssemblyDomainAttribute), false))
                            {
                                AssemblyDomainAttribute refAttribute = (AssemblyDomainAttribute)b.GetCustomAttributes(typeof(AssemblyDomainAttribute), false).First();
                                bool isReferencingHospital = refAttribute.Value == AssemblyDomain.Hospital;
                                if (isReferencingHospital)
                                {
                                    visitPay2HospitalList.Add($"{a.GetName().Name} --> {b.GetName().Name}");
                                }
                            }
                            else
                            {
                                noDomainAttributeList.Add(b.GetName().Name);
                            }
                        }
                    }
                }
                else
                {
                    noDomainAttributeList.Add(a.GetName().Name);
                }
            }

            Assert.IsFalse(noDomainAttributeList.Any());
            Assert.IsFalse(visitPay2HospitalList.Any());
        }

        [Test]
        [Ignore("Not ready until assemblies have been changed to allow it to pass")]
        public void Assemblies_HospitalToVisitPayDirect()
        {
            List<Assembly> ivhAssemblies = GetIvhAssemblies();

            List<string> hospitalList2VisitPay = new List<string>();
            List<string> noDomainAttributeList = new List<string>();

            foreach (Assembly a in ivhAssemblies)
            {
                if (a.IsDefined(typeof(AssemblyDomainAttribute), false))
                {
                    AssemblyDomainAttribute attribute = (AssemblyDomainAttribute)a.GetCustomAttributes(typeof(AssemblyDomainAttribute), false).First();
                    if (attribute.Value == AssemblyDomain.Hospital)
                    {
                        var referencedDomainAssemblyNames = a
                            .GetReferencedAssemblies()
                            .Select(b => new { FullName = b.FullName, Name = b.Name })
                            .Where(b => b.Name.ToUpper().StartsWith("IVH"))
                            .ToList();

                        IEnumerable<Assembly> referencedDomainAssemblies = referencedDomainAssemblyNames.Select(x => Assembly.Load(x.FullName));

                        foreach (Assembly b in referencedDomainAssemblies)
                        {
                            if (b.IsDefined(typeof(AssemblyDomainAttribute), false))
                            {
                                AssemblyDomainAttribute refAttribute = (AssemblyDomainAttribute)b.GetCustomAttributes(typeof(AssemblyDomainAttribute), false).First();
                                bool isReferencingVisitPay = refAttribute.Value == AssemblyDomain.VisitPay;
                                if (isReferencingVisitPay)
                                {
                                    hospitalList2VisitPay.Add($"{a.GetName().Name} --> {b.GetName().Name}");
                                }
                            }
                            else
                            {
                                noDomainAttributeList.Add(b.GetName().Name);
                            }
                        }
                    }
                }
                else
                {
                    noDomainAttributeList.Add(a.GetName().Name);
                }
            }

            Assert.IsFalse(noDomainAttributeList.Any());
            Assert.IsFalse(hospitalList2VisitPay.Any());
        }
    }
}


﻿namespace Ivh.Common.Tests
{
    using System;
    using System.Collections.Generic;
    using System.Dynamic;
    using System.Text.RegularExpressions;
    using Base.Constants;
    using Base.Enums;
    using Base.Utilities.Extensions;
    using Domain.EventJournal.Interfaces;
    using EventJournal;
    using EventJournal.Constants;
    using EventJournal.Exceptions;
    using Helpers.JournalEvent;
    using Newtonsoft.Json;
    using NUnit.Framework;
    using VisitPay.Constants;
    using VisitPay.Enums;

    [TestFixture]
    public class JournalEventTests
    {

        private static IEventJournalService CreateService()
        {
            return new EventJournalServiceMockBuilder().Setup(builder =>
            {
                builder.ApplicationEnum = ApplicationEnum.VisitPay;
            }).CreateService();
        }

        public static JournalEventContext GetJournalEventContext(int? visitPayUserId, int? vpGuarantorId)
        {
            return new JournalEventContext() { VpGuarantorId = vpGuarantorId, VisitPayUserId = visitPayUserId };
        }

        public static JournalEventUserContext GetSystemJournalEventUserContext()
        {
            return new JournalEventUserContext() { UserName = SystemUsers.SystemUserName, EventVisitPayUserId = SystemUsers.SystemUserId, IsSystemUser = true };
        }

        [Test]
        public void JournalEvent_MissingJournalEventContextVisitPayUserId_ClientUserAction_ThrowsCorrectExceptionType()
        {
            JournalEventContext journalEventContext = GetJournalEventContext(null, null);
            JournalEventUserContext journalEventUserContext = GetSystemJournalEventUserContext();
            Type exType = typeof(Exception);
            try
            {
                CreateService().AddEvent<EventTestingJournalEventClientUserAction, JournalEventParameters>(
                    EventTestingJournalEventClientUserAction.GetParameters(
                        tag1: "Tag1Value",
                        tag2: "Tag2Value",
                        additionalInfo1: "AdditionalInfoValue1",
                        additionalInfo2: "AdditionalInfoValue2"
                    ),
                    journalEventUserContext,
                    journalEventContext
                );
            }
            catch (Exception e)
            {
                exType = e.GetType();
            }

            Assert.IsTrue(exType.Equals(typeof(JournalEventContextValueMissingException)), "Missing GuarantorId should throw an exception for missing context value for category VpGuarantorUserAction");
        }

        [Test]
        public void JournalEvent_MissingTag_ThrowsCorrectExceptionType()
        {
            JournalEventContext journalEventContext = GetJournalEventContext(1, 1);
            JournalEventUserContext journalEventUserContext = GetSystemJournalEventUserContext();
            Type exType = typeof(Exception);
            try
            {
                CreateService().AddEvent<EventTestingJournalEventMissingTag, JournalEventParameters>(
                    EventTestingJournalEventMissingTag.GetParameters(
                        tag2: "Tag2Value",
                        additionalInfo1: "AdditionalInfoValue1",
                        additionalInfo2: "AdditionalInfoValue2"
                    ),
                    journalEventUserContext,
                    journalEventContext
                );
            }
            catch (Exception e)
            {
                exType = e.GetType();
            }

            Assert.IsTrue(exType.Equals(typeof(JournalEventTagMissingException)), "Missing Tag1 should throw an exception for missing tag value");
        }

        [Test]
        public void JournalEvent_MissingAdditionalInfo_ThrowsCorrectExceptionType()
        {
            JournalEventContext journalEventContext = GetJournalEventContext(1, 1);
            JournalEventUserContext journalEventUserContext = GetSystemJournalEventUserContext();
            Type exType = typeof(Exception);
            try
            {
                CreateService().AddEvent<EventTestingJournalEventMissingAdditionalInfo, JournalEventParameters>(
                    EventTestingJournalEventMissingAdditionalInfo.GetParameters(
                        tag1: "Tag1Value",
                        tag2: "Tag2Value",
                        additionalInfo2: "AdditionalInfoValue2"
                    ),
                    journalEventUserContext,
                    journalEventContext
                );
            }
            catch (Exception e)
            {
                exType = e.GetType();
            }

            Assert.IsTrue(exType.Equals(typeof(JournalEventTagMissingException)), "Missing AdditionalInfo1 should throw an exception for missing additional info value");
        }

        [Test]
        public void JournalEvent_VpGuarantorUserAction_CreatesMessage()
        {
            JournalEventContext journalEventContext = GetJournalEventContext(1, 1);
            JournalEventUserContext journalEventUserContext = GetSystemJournalEventUserContext();
            Type exType = typeof(Exception);
            JournalEvent journalEvent = CreateService().AddEvent<EventTestingJournalEventClientUserAction, JournalEventParameters>(
                EventTestingJournalEventClientUserAction.GetParameters(
                    tag1: "Tag1Value",
                    tag2: "Tag2Value",
                    additionalInfo1: "AdditionalInfoValue1",
                    additionalInfo2: "AdditionalInfoValue2"
                ),
                journalEventUserContext,
                journalEventContext
            );

            Assert.IsFalse(Regex.IsMatch(journalEvent.Message,"\\[.*?\\]") ,"Message should have all tags replaced");
        }

        [Test]
        public void JournalEvent_VpGuarantorUserAction_CreatesAdditionalInfoJson()
        {
            JournalEventContext journalEventContext = GetJournalEventContext(1, 1);
            JournalEventUserContext journalEventUserContext = GetSystemJournalEventUserContext();
            Type exType = typeof(Exception);
            JournalEvent journalEvent = CreateService().AddEvent<EventTestingJournalEventClientUserAction, JournalEventParameters>(
                EventTestingJournalEventClientUserAction.GetParameters(
                    tag1: "Tag1Value",
                    tag2: "Tag2Value",
                    additionalInfo1: "AdditionalInfoValue1",
                    additionalInfo2: "AdditionalInfoValue2"
                ),
                journalEventUserContext,
                journalEventContext
            );

            ExpandoObject additionalData = JsonConvert.DeserializeObject<ExpandoObject>(journalEvent.AdditionalData);
            int count = ((ICollection<KeyValuePair<string, Object>>)additionalData).Count;
            
            Assert.IsTrue(count == 2 ,"JournalEvent should have two items in the additional info collection");
        }

    }

    #region JournalEvent test classes

    public class EventTestingJournalEventVpGuarantorUserAction : JournalEventTemplate<JournalEventParameters>
    {
        public static JournalEventParameters GetParameters(string tag1, string tag2, string additionalInfo1, string additionalInfo2)
        {
            //Property names are case sensitive and must match AdditionalInfo and HumanReadableDescription [tags] exactly
            dynamic parameters = new ExpandoObject();
            parameters.Tag1 = tag1;
            parameters.Tag2 = tag2;
            parameters.AdditionalInfo1 = additionalInfo1;
            parameters.AdditionalInfo2 = additionalInfo2;
            return new JournalEventParameters(parameters);
        }

        public override JournalEventCategoryEnum EventRuleTrigger => JournalEventCategoryEnum.VpGuarantorUserAction;
        public override string[] EventRuleTags => new[] { JournalEventTags.AcctMngmnt };
        public override string EventRuleName => "EventTestingJournalEvent";
        public override JournalEventTypeEnum AuditLogId => JournalEventTypeEnum.GuarantorModifiedUserName;
        public override string Description => "Generated when we are testing JournalEvents";
        public override bool TriggeredByEventUserId => true;
        public override bool AppliedToHsGId => true;
        public override bool AppliedToVpGId => true;
        public override bool AppliedToVpUserIdClientUserId => true;
        public override bool AppliedToFpId => false;
        public override bool AppliedToVisitId => false;
        public override bool AppliedToTransId => false;
        public override bool AppliedToPaymentId => false;
        public override string[] AdditionalInfo => new string[] { "AdditionalInfo1", "AdditionalInfo2" };
        public override bool Audit => false;
        public override string HumanReadableDescription => "Tag [Tag1] should be replaced with the value of [Tag2]";
    }

    public class EventTestingJournalEventClientUserAction : JournalEventTemplate<JournalEventParameters>
    {
        public static JournalEventParameters GetParameters(string tag1, string tag2, string additionalInfo1, string additionalInfo2)
        {
            //Property names are case sensitive and must match AdditionalInfo and HumanReadableDescription [tags] exactly
            dynamic parameters = new ExpandoObject();
            parameters.Tag1 = tag1;
            parameters.Tag2 = tag2;
            parameters.AdditionalInfo1 = additionalInfo1;
            parameters.AdditionalInfo2 = additionalInfo2;
            return new JournalEventParameters(parameters);
        }

        public override JournalEventCategoryEnum EventRuleTrigger => JournalEventCategoryEnum.ClientUserAction;
        public override string[] EventRuleTags => new[] { JournalEventTags.FinancePlans };
        public override string EventRuleName => "EventTestingJournalEvent";
        public override JournalEventTypeEnum AuditLogId => JournalEventTypeEnum.ClientCanceledAFinancePlan;
        public override string Description => "Generated when we are testing JournalEvents";
        public override bool TriggeredByEventUserId => true;
        public override bool AppliedToHsGId => true;
        public override bool AppliedToVpGId => true;
        public override bool AppliedToVpUserIdClientUserId => true;
        public override bool AppliedToFpId => false;
        public override bool AppliedToVisitId => false;
        public override bool AppliedToTransId => false;
        public override bool AppliedToPaymentId => false;
        public override string[] AdditionalInfo => new string[] { "AdditionalInfo1", "AdditionalInfo2" };
        public override bool Audit => false;
        public override string HumanReadableDescription => "Tag [Tag1] should be replaced with the value of [Tag2]";
    }

    public class EventTestingJournalEventMissingTag : JournalEventTemplate<JournalEventParameters>
    {
        public static JournalEventParameters GetParameters(string tag2, string additionalInfo1, string additionalInfo2)
        {
            //Property names are case sensitive and must match AdditionalInfo and HumanReadableDescription [tags] exactly
            dynamic parameters = new ExpandoObject();
            parameters.Tag2 = tag2;
            parameters.AdditionalInfo1 = additionalInfo1;
            parameters.AdditionalInfo2 = additionalInfo2;
            return new JournalEventParameters(parameters);
        }

        public override JournalEventCategoryEnum EventRuleTrigger => JournalEventCategoryEnum.ClientUserAction;
        public override string[] EventRuleTags => new[] { JournalEventTags.FinancePlans };
        public override string EventRuleName => "EventTestingJournalEvent";
        public override JournalEventTypeEnum AuditLogId => JournalEventTypeEnum.ClientCanceledAFinancePlan;
        public override string Description => "Generated when we are testing JournalEvents";
        public override bool TriggeredByEventUserId => true;
        public override bool AppliedToHsGId => true;
        public override bool AppliedToVpGId => true;
        public override bool AppliedToVpUserIdClientUserId => true;
        public override bool AppliedToFpId => false;
        public override bool AppliedToVisitId => false;
        public override bool AppliedToTransId => false;
        public override bool AppliedToPaymentId => false;
        public override string[] AdditionalInfo => new string[] { "AdditionalInfo1", "AdditionalInfo2" };
        public override bool Audit => false;
        public override string HumanReadableDescription => "Tag [Tag1] should be replaced with the value of [Tag2]";
    }

    public class EventTestingJournalEventMissingAdditionalInfo : JournalEventTemplate<JournalEventParameters>
    {
        public static JournalEventParameters GetParameters(string tag1, string tag2, string additionalInfo2)
        {
            //Property names are case sensitive and must match AdditionalInfo and HumanReadableDescription [tags] exactly
            dynamic parameters = new ExpandoObject();
            parameters.Tag2 = tag1;
            parameters.Tag2 = tag2;
            parameters.AdditionalInfo2 = additionalInfo2;
            return new JournalEventParameters(parameters);
        }

        public override JournalEventCategoryEnum EventRuleTrigger => JournalEventCategoryEnum.ClientUserAction;
        public override string[] EventRuleTags => new[] { JournalEventTags.FinancePlans };
        public override string EventRuleName => "EventTestingJournalEvent";
        public override JournalEventTypeEnum AuditLogId => JournalEventTypeEnum.ClientCanceledAFinancePlan;
        public override string Description => "Generated when we are testing JournalEvents";
        public override bool TriggeredByEventUserId => true;
        public override bool AppliedToHsGId => true;
        public override bool AppliedToVpGId => true;
        public override bool AppliedToVpUserIdClientUserId => true;
        public override bool AppliedToFpId => false;
        public override bool AppliedToVisitId => false;
        public override bool AppliedToTransId => false;
        public override bool AppliedToPaymentId => false;
        public override string[] AdditionalInfo => new string[] { "AdditionalInfo1", "AdditionalInfo2" };
        public override bool Audit => false;
        public override string HumanReadableDescription => "Tag [Tag1] should be replaced with the value of [Tag2]";
    }

    #endregion region


}

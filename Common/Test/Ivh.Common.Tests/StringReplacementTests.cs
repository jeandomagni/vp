﻿namespace Ivh.Common.Tests
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Linq;
    using Base.Utilities.Helpers;
    using JetBrains.dotMemoryUnit;
    using NUnit.Framework;

    //[TestFixture]
    public class StringReplacementTests
    {
        const int Mb = 1024 * 1024;
        //[DotMemoryUnit(SavingStrategy = SavingStrategy.OnCheckFail, Directory = @"C:\temp\")]
        [AssertTraffic(AllocatedSizeInBytes = 3 * Mb)]
        //[Test]
        public void ReplaceDoesntLeakMemory()
        {
            //basically testing this
            //textRegionString = ReplacementUtility.Replace(textRegionString, clientReplacementValues, null, null);
            Dictionary<string, string> buildADictionary = new Dictionary<string, string>();//EG: {{"key1", "value1"},{"key1", "value1"}}
            for (int i = 0; i < 1000; i++)
            {
                buildADictionary.Add($"key{i}",$"value{i}" );
            }

            IReadOnlyDictionary<string, string> replacementValues = new ReadOnlyDictionary<string, string>(buildADictionary);
            IDictionary<string, string> clientReplacementValues = replacementValues.ToDictionary(kvp => $"[[{kvp.Key}]]", kvp => kvp.Value);

            var memoryCheckPoint1 = dotMemory.Check();
            for (int i = 0; i < 1000; i++)
            {
                string result = ReplacementUtility.Replace($"[[key{i}]]", clientReplacementValues, null, null);
                Assert.AreEqual($"value{i}", result);
            }

            dotMemory.Check(memory =>
            {
                Assert.That(memory.GetDifference(memoryCheckPoint1)
                    .GetSurvivedObjects()
                    .GetObjects(where => where.Namespace.Like("Ivh.Common.Base"))
                    .ObjectsCount, Is.EqualTo(0));
            });

        }
    }
}

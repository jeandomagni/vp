﻿namespace Ivh.Common.Tests
{
    using System;
    using System.Text;
    using Encryption;
    using NUnit.Framework;

    [TestFixture]
    public class EncryptionAesThenHmacTest
    {
        [Test]
        public void EncryptAStringAndDecryptAString()
        {
            string secretString = "MyString i want to protect!";

            //This is some non-secret authentication string.  Apparently this helps with man in the middle attacks.
            string iVinciAuthString = "iVinciIsTheBest";
            byte[] iVinciAuthBytes = Encoding.UTF8.GetBytes(iVinciAuthString);
            int iVinciAuthStringLen = iVinciAuthString.Length;

            //This is the master key, this should be very safe
            byte[] cryptKey = AESThenHMAC.NewKey();

            //This is the auth key, not 100% sure why this is needed, but again it's supposed to be for man in the middle attacks.
            //This should be stored in a different way than the master key.
            byte[] authKey = AESThenHMAC.NewKey();


            //opposite: Convert.FromBase64String
            string cryptKeyString = Convert.ToBase64String(cryptKey);
            string authKeyString = Convert.ToBase64String(authKey);

            Console.Write("Cryptkey: \"{0}\"", cryptKeyString);
            Console.Write("Authkey: \"{0}\"", authKeyString);


            var encrypted = AESThenHMAC.SimpleEncrypt(secretString, cryptKey, authKey, iVinciAuthBytes);

            Assert.AreNotEqual(encrypted, secretString);

            var decrypted = AESThenHMAC.SimpleDecrypt(encrypted, cryptKey, authKey, iVinciAuthStringLen);

            Assert.AreEqual(decrypted, secretString);
        }
    }
}
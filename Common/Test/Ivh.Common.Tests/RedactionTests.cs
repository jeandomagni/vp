﻿namespace Ivh.Common.Tests
{
    using NUnit.Framework;
    using VisitPay.Attributes;

    [TestFixture]
    public class RedactionTests
    {
        [Test]
        public void TruncationTest()
        {
            TestClass testClass = new TestClass()
            {
                Test1 = "12313123123123123",
                Test2 = "sdfsdfsdfsdfsfdsdf",
                Test3 = "ssdfsdfsdf",
                Test4 = "OriginalValue"
            };

            string replacementValue = "ReplacementValue";
            RedactFieldAttribute.RedactFields(testClass, replacementValue);
            Assert.AreEqual(replacementValue, testClass.Test1);
            Assert.AreEqual(replacementValue, testClass.Test2);
            Assert.AreEqual(replacementValue.Substring(0, 10), testClass.Test3);
            Assert.AreEqual("OriginalValue", testClass.Test4);
        }

        private class TestClass
        {
            [RedactField]
            public string Test1 { get; set; }
            [RedactField]
            public string Test2 { get; set; }
            [RedactField(10)]
            public string Test3 { get; set; }

            public string Test4 { get; set; }
        }
    }
}

﻿namespace Ivh.Common.Tests
{
    using System.Globalization;
    using System.Threading;
    using Base.Utilities.Helpers;
    using NUnit.Framework;

    [TestFixture]
    public class CultureKeeperTests
    {
        [Test]
        public void CultureKeeper_Works()
        {
            string culture = Thread.CurrentThread.CurrentUICulture.Name;

            using (new CultureKeeper(new CultureInfo("is-IS")))
            {
                Assert.AreEqual("is-IS", Thread.CurrentThread.CurrentUICulture.Name);
            }

            Assert.AreEqual(culture, Thread.CurrentThread.CurrentUICulture.Name);
        }
    }
}
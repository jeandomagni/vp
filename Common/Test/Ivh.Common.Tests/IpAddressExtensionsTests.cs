﻿using NUnit.Framework;

namespace Ivh.Common.Tests
{
    using System.Net;
    using Web.Extensions;

    [TestFixture]
    public class IpAddressExtensionsTests
    {
        [Test]
        public void IpAddressParsing_LegalAddress()
        {
            string ipAddressString = "192.168.0.1";
            IPAddress ipAddress = ipAddressString.ToIPAddress();
            Assert.AreEqual(ipAddressString, ipAddress.ToString());
        }

        [Test]
        public void IpAddressParsing_LegalAddressWithWhitespace()
        {
            string ipAddressString = "  192.168.0.1 ";
            IPAddress ipAddress = ipAddressString.ToIPAddress();
            Assert.AreEqual(ipAddressString.Trim(), ipAddress.ToString());
        }

        [Test]
        public void IpAddressParsing_IllegalAddress()
        {
            string ipAddressString = "192.168.0.1000";
            IPAddress ipAddress = ipAddressString.ToIPAddress();
            Assert.IsNull(ipAddress);
        }
        [Test]
        public void IpAddressParsing_ExtraneousCharacters()
        {
            string ipAddressString = "+192.168.0.1";
            IPAddress ipAddress = ipAddressString.ToIPAddress();
            Assert.AreEqual("192.168.0.1", ipAddress.ToString());
        }

        [Test]
        public void IpAddressParsing_LegalAddressWithPort()
        {
            string ipAddressString = "192.168.0.1:1234";
            IPAddress ipAddress = ipAddressString.ToIPAddress();
            Assert.AreEqual("192.168.0.1", ipAddress.ToString());
        }
        [Test]
        public void IpAddressParsing_IllegalAddressWithPort()
        {
            string ipAddressString = "192.168.0.1000:1234";
            IPAddress ipAddress = ipAddressString.ToIPAddress();
            Assert.IsNull(ipAddress);
        }
        [Test]
        public void IpAddressParsing_ExtraneousCharactersWithPort()
        {
            string ipAddressString = "+192.168.0.1:1234";
            IPAddress ipAddress = ipAddressString.ToIPAddress();
            Assert.AreEqual("192.168.0.1", ipAddress.ToString());
        }
    }
}
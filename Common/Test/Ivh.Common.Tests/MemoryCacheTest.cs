﻿namespace Ivh.Common.Tests
{
    using System.Collections.Generic;
    using System.Linq;
    using Cache;
    using Domain.Content.Entities;
    using NUnit.Framework;
    using ProtoBuf;

    [TestFixture]
    public class MemoryCacheTest
    {
        [Test]
        public void SaveAndGetSimpleObjects()
        {
            ICache cache = new IvinciMemoryCache();

            string myTestKey = "SaveAndGetSimpleObjects";
            string myTestString = "ILoveToCacheThings!";

            cache.SetStringAsync(myTestKey, myTestString).Wait();
            string response = cache.GetStringAsync(myTestKey).Result;

            Assert.AreEqual(response, myTestString);

        }

        [Test]
        public void SaveAndGetComplexObjects()
        {
            ICache cache = new IvinciMemoryCache();

            string myTestKey = "SaveAndGetComplexObjects";
            TestCacheComplexObject myTestObject = new TestCacheComplexObject() { Email = "Email", Name = "Name" };

            cache.SetObjectAsync(myTestKey, myTestObject).Wait();
            TestCacheComplexObject response = cache.GetObjectAsync<TestCacheComplexObject>(myTestKey).Result;

            Assert.AreEqual(response.Email, myTestObject.Email);
            Assert.AreEqual(response.Name, myTestObject.Name);

        }

        [Test]
        public void SaveAndGetComplexObjects_with_graph()
        {
            string key = "complexWithChildrenReferencingParent";

            ICache cache = new IvinciMemoryCache();
            ContentMenu contentMenu = new ContentMenu
            {
                ContentMenuId = 1,
                ContentMenuName = "foo"
            };

            //introduce a graph with a parent ref
            List<ContentMenuItem> contentMenuItems = new List<ContentMenuItem>
            {
                new ContentMenuItem { ContentMenuItemId = 2,ContentMenu = contentMenu},
                new ContentMenuItem { ContentMenuItemId = 3,ContentMenu = contentMenu}
            };
            contentMenu.ContentMenuItems = contentMenuItems;

            cache.SetObjectAsync(key, contentMenu).Wait();
            ContentMenu response = cache.GetObjectAsync<ContentMenu>(key).Result;
            Assert.AreEqual(contentMenu.ContentMenuId, response.ContentMenuId);
            Assert.AreEqual(contentMenu.ContentMenuName, response.ContentMenuName);
            Assert.AreEqual(contentMenu.ContentMenuItems.Count, response.ContentMenuItems.Count);
            var firstCachedMenu = response.ContentMenuItems.First();
            var firstMenu = contentMenuItems.First();
            var lastCachedMenu = response.ContentMenuItems.Last();
            var lastMenu = contentMenuItems.Last();

            Assert.AreEqual(firstMenu.ContentMenu.ContentMenuId,firstCachedMenu.ContentMenu.ContentMenuId);
            Assert.AreEqual(firstMenu.ContentMenuItemId, firstCachedMenu.ContentMenuItemId);
            Assert.AreEqual(lastMenu.ContentMenu.ContentMenuId, lastCachedMenu.ContentMenu.ContentMenuId);
            Assert.AreEqual(lastMenu.ContentMenuItemId, lastCachedMenu.ContentMenuItemId);

            bool referenceEquals = object.ReferenceEquals(response, contentMenu);
            Assert.False(referenceEquals);
        }
    }

    // Note the original test passed with 
    // DataContract and DataMember attributes. 
    // ProtoBuf can serialize with these attributes but 
    // switch explicitly to Protobuf to help document the standard
    [ProtoContract]
    internal class TestCacheComplexObject
    {
        [ProtoMember(1)]
        public string Name { get; set; }
        [ProtoMember(2)]
        public string Email { get; set; }
    }
}

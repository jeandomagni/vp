﻿using NUnit.Framework;

namespace Ivh.Common.Tests
{
    using System;
    using Base.Utilities.Helpers;
    using Moq;
    using ServiceBus.Attributes;

    [TestFixture]
    public class ScheduledDeliveryRangeTests
    {
        private const string Timezone = "Central Standard Time";
        private const int DeliveryRangeBegin = 8 * 60;
        private const int DeliveryRangeEnd = 20 * 60;
        [TestCase(DeliveryRangeBegin, DeliveryRangeEnd, "2018-06-08 7:59:00", "2018-06-08 8:00:00", false)]
        [TestCase(DeliveryRangeBegin, DeliveryRangeEnd, "2018-06-08 8:00:00", "2018-06-08 8:00:00", true)]
        [TestCase(DeliveryRangeBegin, DeliveryRangeEnd, "2018-06-08 8:01:00", "2018-06-08 8:01:00", true)]
        [TestCase(DeliveryRangeBegin, DeliveryRangeEnd, "2018-06-08 19:59:00", "2018-06-08 19:59:00", true)]
        [TestCase(DeliveryRangeBegin, DeliveryRangeEnd, "2018-06-08 20:00:00", "2018-06-09 8:00:00", false)]
        [TestCase(DeliveryRangeBegin, DeliveryRangeEnd, "2018-06-09 00:00:01", "2018-06-09 8:00:00", false)]
        public void BasicTest(int localDateRangeBeginHours, int localDateRangeEndHours, string currentLocalTime, string expectedLocalTime, bool isInDeliveryRange)
        {
            ScheduledDeliveryRangeAttribute scheduledDeliveryRangeAttribute = new ScheduledDeliveryRangeAttribute();

            DateTime currentTimeUtc = DateTime.Parse(currentLocalTime);
            currentTimeUtc = new DateTime(currentTimeUtc.Ticks, DateTimeKind.Local);
            DateTime expectedTimeUtc = DateTime.Parse(expectedLocalTime);
            expectedTimeUtc = new DateTime(expectedTimeUtc.Ticks, DateTimeKind.Local);
            Assert.AreEqual(isInDeliveryRange, scheduledDeliveryRangeAttribute.IsInDeliveryRange(TimeSpan.FromMinutes(localDateRangeBeginHours), TimeSpan.FromMinutes(localDateRangeEndHours), currentTimeUtc));
            TimeSpan deliveryDelay = scheduledDeliveryRangeAttribute.GetDeliveryDelay(TimeSpan.FromMinutes(localDateRangeBeginHours), TimeSpan.FromMinutes(localDateRangeEndHours), currentTimeUtc);
            Assert.AreEqual(expectedTimeUtc, currentTimeUtc.Add(deliveryDelay));
        }

        [TestCase("2018-06-08 7:59:00", "2018-06-08 8:00:00", false, Timezone)]
        [TestCase("2018-06-08 8:00:00", "2018-06-08 8:00:00", true, Timezone)]
        [TestCase("2018-06-08 8:01:00", "2018-06-08 8:01:00", true, Timezone)]
        [TestCase("2018-06-08 19:59:00", "2018-06-08 19:59:00", true, Timezone)]
        [TestCase("2018-06-08 20:00:00", "2018-06-09 8:00:00", false, Timezone)]
        [TestCase("2018-06-09 00:00:01", "2018-06-09 8:00:00", false, Timezone)]
        public void BasicTestClientFriendlyDeliveryRange(string currentLocalTime, string expectedLocalTime, bool isInDeliveryRange, string timezone)
        {
            TimeZoneHelper timeZoneHelper = new TimeZoneHelper(Timezone);
            string friendlyCommunicationDeliveryRange = "08:00-20:00";
            ClientFriendlyDeliveryRangeAttribute scheduledDeliveryRangeAttribute = new ClientFriendlyDeliveryRangeAttribute();

            DateTime currentTimeUtc = DateTime.Parse(currentLocalTime);
            currentTimeUtc = new DateTime(currentTimeUtc.Ticks, DateTimeKind.Local);
            DateTime expectedTimeUtc = DateTime.Parse(expectedLocalTime);
            expectedTimeUtc = new DateTime(expectedTimeUtc.Ticks, DateTimeKind.Local);
            Assert.AreEqual(isInDeliveryRange, scheduledDeliveryRangeAttribute.IsInDeliveryRange(friendlyCommunicationDeliveryRange, currentTimeUtc));
            TimeSpan deliveryDelay = scheduledDeliveryRangeAttribute.GetDeliveryDelay(friendlyCommunicationDeliveryRange, currentTimeUtc);
            Assert.AreEqual(expectedTimeUtc, currentTimeUtc.Add(deliveryDelay));
        }
    }
}
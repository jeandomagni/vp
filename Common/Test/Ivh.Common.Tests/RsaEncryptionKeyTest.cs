﻿namespace Ivh.Common.Tests
{
    using System;
    using System.Text;
    using Base.Utilities.Extensions;
    using Encryption;
    using NUnit.Framework;

    [TestFixture]
    public class RsaEncryptionKeyTest
    {


        [Test]
        public void CreateANewPrivateKeyRsa()
        {
            var rsaEncryptionHelper = new IvinciRsaEncryption();

            var provider = rsaEncryptionHelper.CreateRsaCryptoServiceProvider();

            string exportRsaParameters = rsaEncryptionHelper.ExportRsaParameters(provider, true);
            string xmlBase64ed = exportRsaParameters.Base64Encode();
            Console.WriteLine("With private key {0}", xmlBase64ed);
            string xmlDecoded = xmlBase64ed.Base64Decode();
            var newProvider = rsaEncryptionHelper.GetRsaCryptoServiceProviderWithXmlString(xmlDecoded);

            Assert.AreEqual(rsaEncryptionHelper.ExportRsaParameters(provider, false), rsaEncryptionHelper.ExportRsaParameters(newProvider, false));
        }

        [Test]
        public void TestEncryptAndDecrypt()
        {
            string stringToEncrypt = "TestString";
            var plainText = Encoding.UTF8.GetBytes(stringToEncrypt);

            var rsaEncryptionHelper = new IvinciRsaEncryption();
            var provider = rsaEncryptionHelper.CreateRsaCryptoServiceProvider();

            string exportRsaParameters = rsaEncryptionHelper.ExportRsaParameters(provider, true);
            var newProvider = rsaEncryptionHelper.GetRsaCryptoServiceProviderWithXmlString(exportRsaParameters);

            var encrypted = newProvider.Encrypt(plainText, true);

            var decrypted = newProvider.Decrypt(encrypted, true);

            string shouldBeTheSameAsStringToEncrypt = Encoding.UTF8.GetString(decrypted);
            
            Assert.AreEqual(stringToEncrypt, shouldBeTheSameAsStringToEncrypt);

        }

    }
}
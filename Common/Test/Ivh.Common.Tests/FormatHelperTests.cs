﻿namespace Ivh.Common.Tests
{
    using Ivh.Common.Base.Utilities.Helpers;
    using NUnit.Framework;

    [TestFixture]
    public class FormatHelperTests
    {
        [Test]
        public void CityStateZipLenient_HaveCityStateZip_FormattedResult()
        {
            const string city = "BOISE";
            const string state = "ID";
            const string zip = "83702";
            string expected = $"{city}, {state} {zip}";

            string formattedResult = FormatHelper.CityStateZipLenient(city, state, zip);
            Assert.AreEqual(expected, formattedResult);
        }

        [Test]
        public void CityStateZipLenient_HaveCityState_FormattedResult()
        {
            const string city = "BOISE";
            const string state = "ID";
            string zip = string.Empty;
            string expected = $"{city}, {state}";

            string formattedResult = FormatHelper.CityStateZipLenient(city, state, zip);
            Assert.AreEqual(expected, formattedResult);
        }

        [Test]
        public void CityStateZipLenient_HaveStateZip_FormattedResult()
        {
            string city = string.Empty;
            const string state = "ID";
            const string zip = "83702";
            string expected = $"{state} {zip}";

            string formattedResult = FormatHelper.CityStateZipLenient(city, state, zip);
            Assert.AreEqual(expected, formattedResult);
        }

        [Test]
        public void CityStateZipLenient_HaveCityZip_FormattedResult()
        {
            const string city = "BOISE";
            string state = string.Empty;
            const string zip = "83702";
            string expected = $"{city} {zip}";

            string formattedResult = FormatHelper.CityStateZipLenient(city, state, zip);
            Assert.AreEqual(expected, formattedResult);
        }

        [Test]
        public void CityStateZipLenient_NullValuesProvided_EmptyStringResult()
        {
            string city = null;
            string state = null;
            string zip = null;
            string expected = string.Empty;

            string formattedResult = FormatHelper.CityStateZipLenient(city, state, zip);
            Assert.AreEqual(expected, formattedResult);
        }

        [Test]
        public void CityStateZipLenient_EmptyValuesProvided_EmptyStringResult()
        {
            string city = string.Empty;
            string state = string.Empty;
            string zip = string.Empty;
            string expected = string.Empty;

            string formattedResult = FormatHelper.CityStateZipLenient(city, state, zip);
            Assert.AreEqual(expected, formattedResult);
        }

        [Test]
        public void CityStateZipLenient_WhiteSpaceAndEmptyValuesProvided_EmptyStringResult()
        {
            string city = " ";
            string state = " ";
            string zip = string.Empty;
            string expected = string.Empty;

            string formattedResult = FormatHelper.CityStateZipLenient(city, state, zip);
            Assert.AreEqual(expected, formattedResult);
        }
    }
}
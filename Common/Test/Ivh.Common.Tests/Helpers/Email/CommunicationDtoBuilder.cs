﻿namespace Ivh.Common.Tests.Helpers.Email
{
    using System;
    using Application.Email.Common.Dtos;
    using Builders;
    using Common.Base.Enums;
    using Faker;
    using VisitPay.Enums;

    public class CommunicationDtoBuilder : EntityBuilder<CommunicationDtoBuilder, CommunicationDto>
    {
        public CommunicationDtoBuilder WithDefaultValues(CommunicationTypeEnum communicationTypeEnum, CommunicationMethodEnum communicationMethodEnum)
        {
            this.Builder.With(x =>
            {
                x.Body = Lorem.Paragraph(4);
                x.Subject = Lorem.Sentence(8);
                x.CreatedDate = DateTime.UtcNow.AddHours(-1);
                x.CommunicationStatus = CommunicationStatusEnum.Created;
                x.SendFailureCnt = 0;
                x.ToAddress = "e@mail.com";
                x.FromAddress = "billpay@email.com";
                x.FromName = "bilpay";
                x.UniqueId = Guid.NewGuid();
                x.CommunicationType = new CommunicationTypeDto{ CommunicationTypeId = communicationTypeEnum, CommunicationMethodId = communicationMethodEnum};
                x.SentToUserId = 1;
            });

            return this.Builder;
        }

        public CommunicationDtoBuilder SendTo(int sendToUserId, string sendToAddress = null)
        {
            this.Builder.With(x =>
            {
                x.ToAddress = sendToAddress ?? x.ToAddress;
                x.SentToUserId = sendToUserId;
            });

            return this.Builder;
        }
    }
}

﻿namespace Ivh.Common.Tests.Helpers.Email
{
    using Application.Email.Internal.Implementations;
    using Application.Email.Internal.Interfaces;
    using Domain.Content.Interfaces;
    using Domain.Email.Interfaces;
    using Domain.Logging.Interfaces;
    using Domain.Settings.Interfaces;
    using Domain.Visit.Interfaces;
    using Ivh.Application.FinanceManagement.Common.Interfaces;
    using Moq;
    using System;

    public class CommunicationTemplateLoaderMockBuilder 
        : IMockBuilder<ICommunicationTemplateLoader, CommunicationTemplateLoaderMockBuilder>
    {
        public ICommunicationTypeRepository CommunicationTypeRepository;
        public IContentService ContentService;
        public IFeatureService FeatureService;
        public ILoggingService Logger;
        public IVisitService VisitService;
        public ICreditAgreementApplicationService CreditAgreementApplicationService;

        public Mock<ICommunicationTypeRepository> CommunicationTypeRepositoryMock;
        public Mock<IContentService> ContentServiceMock;
        public Mock<IFeatureService> FeatureServiceMock;
        public Mock<ILoggingService> LoggerMock;
        public Mock<IVisitService> VisitServiceMock;
        public Mock<ICreditAgreementApplicationService> CreditAgreementApplicationServiceMock;

        public CommunicationTemplateLoaderMockBuilder()
        {
            this.CommunicationTypeRepositoryMock = new Mock<ICommunicationTypeRepository>();
            this.ContentServiceMock = new Mock<IContentService>();
            this.FeatureServiceMock = new Mock<IFeatureService>();
            this.LoggerMock = new Mock<ILoggingService>();
            this.VisitServiceMock = new Mock<IVisitService>();
            this.CreditAgreementApplicationServiceMock = new Mock<ICreditAgreementApplicationService>();
        }


        public ICommunicationTemplateLoader CreateService()
        {
            return new CommunicationTemplateLoader(
                new Lazy<ICommunicationTypeRepository>(() => this.CommunicationTypeRepository ?? this.CommunicationTypeRepositoryMock.Object),
                new Lazy<IContentService>(() => this.ContentService ?? this.ContentServiceMock.Object),
                new Lazy<ILoggingService>(() => this.Logger ?? this.LoggerMock.Object),
                new Lazy<IVisitService>(() => this.VisitService ?? this.VisitServiceMock.Object),
                new Lazy<IFeatureService>(() => this.FeatureService ?? this.FeatureServiceMock.Object),
                new Lazy<ICreditAgreementApplicationService>(() => this.CreditAgreementApplicationService ?? this.CreditAgreementApplicationServiceMock.Object)
            );
        }

        public CommunicationTemplateLoaderMockBuilder Setup(Action<CommunicationTemplateLoaderMockBuilder> configAction)
        {
            configAction(this);
            return this;
        }
    }
}

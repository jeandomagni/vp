﻿namespace Ivh.Common.Tests.Helpers.Email
{
    using Application.Email.Internal.Interfaces;
    using Base;
    using Domain.Email.Interfaces;
    using Domain.Guarantor.Interfaces;
    using Domain.User.Interfaces;
    using Moq;
   
    public abstract class EmailsApplicationServiceMockBuilder
    {
        public ICommunicationSender CommunicationSender;
        public ICommunicationTemplateLoader CommunicationTemplateLoader;
        public ICommunicationService CommunicationService;
        public IEmailService EmailService;
        public IGuarantorService GuarantorService;
        public IVisitPayUserRepository VisitPayUserRepository;
        public IVisitPayUserService VisitPayUserService;

        public ApplicationServiceCommonServiceMockBuilder ApplicationServiceCommonServiceMockBuilder;
        public Mock<ICommunicationSender> CommunicationSenderMock;
        public Mock<ICommunicationTemplateLoader> CommunicationTemplateLoaderMock;
        public Mock<ICommunicationService> CommunicationServiceMock;
        public Mock<IEmailService> EmailServiceMock;
        public Mock<IGuarantorService> GuarantorServiceMock;
        public Mock<IVisitPayUserRepository> VisitPayUserRepositorMock;
        public Mock<IVisitPayUserService> VisitPayUserServiceMock;

        protected EmailsApplicationServiceMockBuilder()
        {
            this.ApplicationServiceCommonServiceMockBuilder = new ApplicationServiceCommonServiceMockBuilder();
            this.CommunicationSenderMock = new Mock<ICommunicationSender>();
            this.CommunicationTemplateLoaderMock = new Mock<ICommunicationTemplateLoader>();
            this.CommunicationServiceMock = new Mock<ICommunicationService>();
            this.EmailServiceMock = new Mock<IEmailService>();
            this.GuarantorServiceMock = new Mock<IGuarantorService>();
            this.VisitPayUserRepositorMock = new Mock<IVisitPayUserRepository>();
            this.VisitPayUserServiceMock = new Mock<IVisitPayUserService>();
        }
    }
}
﻿namespace Ivh.Common.Tests.Helpers.Features
{
    using System;
    using Domain.Logging.Interfaces;
    using Domain.Settings.Interfaces;
    using Domain.Settings.Services;
    using Moq;

    public class FeatureServiceMockBuilder : IMockBuilder<IFeatureService, FeatureServiceMockBuilder>
    {
        public IApplicationSettingsService ApplicationSettingsService;
        public IClientService ClientService;
        public ILoggingProvider Logger;
        public IPaymentConfigurationService PaymentConfigurationService;

        public Mock<IApplicationSettingsService> ApplicationSettingsServiceMock;
        public Mock<IClientService> ClientServiceMock;
        public Mock<ILoggingProvider> LoggerMock;
        public Mock<IPaymentConfigurationService> PaymentConfigurationServiceMock;

        public FeatureServiceMockBuilder()
        {
            this.ApplicationSettingsServiceMock = new Mock<IApplicationSettingsService>();
            this.ClientServiceMock = new Mock<IClientService>();
            this.LoggerMock = new Mock<ILoggingProvider>();
            this.PaymentConfigurationServiceMock = new Mock<IPaymentConfigurationService>();
        }

        public IFeatureService CreateService()
        {
            return new FeatureService(
                new Lazy<IApplicationSettingsService>(() => this.ApplicationSettingsService ?? this.ApplicationSettingsServiceMock.Object),
                new Lazy<IClientService>(() => this.ClientService ?? this.ClientServiceMock.Object),
                new Lazy<IPaymentConfigurationService>(() => this.PaymentConfigurationService ?? this.PaymentConfigurationServiceMock.Object),
                new Lazy<ILoggingProvider>(() => this.Logger ?? this.LoggerMock.Object)
            );
        }

        public FeatureServiceMockBuilder Setup(Action<FeatureServiceMockBuilder> configAction)
        {
            configAction(this);
            return this;
        }
    }
}
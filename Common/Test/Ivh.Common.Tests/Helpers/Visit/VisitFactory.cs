namespace Ivh.Common.Tests.Helpers.Visit
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Common.Base.Constants;
    using Common.Base.Enums;
    using Common.Base.Utilities.Helpers;
    using Domain.Visit.Entities;
    using Domain.Guarantor.Entities;
    using State.Interfaces;
    using VisitPay.Enums;

    public class VisitFactory
    {
        private static int visitCounter = 0;

        public static Tuple<Visit, IList<VisitTransaction>> GenerateActiveVisit(decimal currentBalance, int numberOfTransactionsToUse, VisitStateEnum visitState = VisitStateEnum.Active, DateTime? insertDate = null, string billingApplication = "PB", int? visitId = null, int? agingCount = null,  int? paymentPriority = null, Guarantor guarantor = null)
        {
            Visit currentVisit = GetBaseVisit(visitState, billingApplication, visitId, agingCount, paymentPriority, guarantor);
            IList<VisitTransaction> visitTransactions = AddCurrentBalanceOntoVisit(currentVisit, currentBalance, numberOfTransactionsToUse, insertDate);

            if (insertDate.HasValue)
            {
                currentVisit.InsertDate = insertDate.Value;
                currentVisit.DischargeDate = insertDate.Value;
                int secondCounter = -1;
                foreach (VisitStateHistory visitStateHistory in currentVisit.VisitStates)
                {
                    visitStateHistory.DateTime = insertDate.Value.AddSeconds(secondCounter);
                    secondCounter++;
                }
            }
            return new Tuple<Visit, IList<VisitTransaction>>(currentVisit, visitTransactions);
        }

        public static Tuple<Visit, IList<VisitTransaction>> GenerateSuspendedVisit(decimal currentBalance, int numberOfTransactionsToUse, DateTime? insertDate = null, string billingApplication = "PB", int? visitId = null, int? agingCount = null, int? paymentPriority = null, Guarantor guarantor = null)
        {
            Visit currentVisit = GetBaseVisit(VisitStateEnum.Inactive, billingApplication, visitId, agingCount, paymentPriority, guarantor);
            IList<VisitTransaction> visitTransactions = AddCurrentBalanceOntoVisit(currentVisit, currentBalance, numberOfTransactionsToUse, insertDate);
            currentVisit.BillingHold = true;
            return new Tuple<Visit, IList<VisitTransaction>>(currentVisit, visitTransactions);
        }

        public static Tuple<Visit, IList<VisitTransaction>> GenerateIneligibleVisit(decimal currentBalance, int numberOfTransactionsToUse, DateTime? insertDate = null, string billingApplication = "PB", int? visitId = null, int? agingCount = null, int? paymentPriority = null, Guarantor guarantor = null)
        {
            Tuple<Visit, IList<VisitTransaction>> currentVisit = GenerateActiveVisit(currentBalance, numberOfTransactionsToUse, VisitStateEnum.Inactive, insertDate, billingApplication, visitId, agingCount, paymentPriority, guarantor);
            currentVisit.Item1.VpEligible = false;
            return currentVisit;
        }

        public static Tuple<Visit, IList<VisitTransaction>> GenerateUncollectableVisit(decimal currentBalance, int numberOfTransactionsToUse, DateTime? insertDate = null, string billingApplication = "PB", int? visitId = null, int? agingCount = null, int? paymentPriority = null, Guarantor guarantor = null)
        {
            Tuple<Visit, IList<VisitTransaction>> currentVisit = GenerateActiveVisit(currentBalance, numberOfTransactionsToUse, VisitStateEnum.Inactive, insertDate, billingApplication, visitId, agingCount, paymentPriority, guarantor);
            currentVisit.Item1.IsMaxAge = true;
            return currentVisit;
        }

        public static Tuple<Visit, IList<VisitTransaction>> GenerateInactiveVisit(decimal currentBalance, int numberOfTransactionsToUse, VisitStateEnum visitState = VisitStateEnum.Inactive, string billingApplication = "PB", int? visitId = null, int? agingCount = null, int? paymentPriority = null, Guarantor guarantor = null, DateTime? insertDate = null)
        {
            Tuple<Visit, IList<VisitTransaction>> currentVisit = GenerateActiveVisit(currentBalance, numberOfTransactionsToUse, VisitStateEnum.Inactive, insertDate, billingApplication, visitId, agingCount, paymentPriority, guarantor);
            return currentVisit;
        }

        public static Tuple<Visit, IList<VisitTransaction>> GenerateActiveUncollectableVisit(decimal currentBalance, int numberOfTransactionsToUse, DateTime? insertDate = null, string billingApplication = "PB", int? visitId = null, int? agingCount = null, int? paymentPriority = null, Guarantor guarantor = null)
        {
            Visit currentVisit = GetBaseVisit(VisitStateEnum.Active, billingApplication, visitId, agingCount, paymentPriority, guarantor);
            currentVisit.SetAgingTier(AgingTierEnum.MaxAge,"");
            IList<VisitTransaction> visitTransactions = AddCurrentBalanceOntoVisit(currentVisit, currentBalance, numberOfTransactionsToUse, insertDate);
            return new Tuple<Visit, IList<VisitTransaction>>(currentVisit, visitTransactions);
        }

        private static IList<VisitTransaction> AddCurrentBalanceOntoVisit(Visit currentVisit, decimal currentBalance, int numberOfTransactionsToUse, DateTime? insertDate = null)
        {
            if (numberOfTransactionsToUse - 1 < 0) throw new Exception("Not Enough transactions were used.");
            IList<VisitTransaction> visitTransactions = new List<VisitTransaction>();
            if (numberOfTransactionsToUse > 1)
            {
                for (int i = 0; i < numberOfTransactionsToUse - 1; i++)
                {
                    visitTransactions.Add(VisitTransactionFactory.GenerateVisitTransaction(Math.Round(currentBalance / (numberOfTransactionsToUse - 1))));
                }
            }
            decimal remainder = currentBalance - visitTransactions.Sum(x => x.VisitTransactionAmount.TransactionAmount);
            visitTransactions.Add(VisitTransactionFactory.GenerateVisitTransaction(remainder));
            currentVisit.SetCurrentBalance(currentBalance, insertDate);
            return visitTransactions;
        }

        private static Visit GetBaseVisit( VisitStateEnum visitState, string billingApplication, int? visitId, int? agingCount, int? paymentPriority, Guarantor guarantor)
        {
            visitCounter++;
            if(guarantor == null)
            {
                guarantor = new Guarantor();
            }

            Visit visit = new Visit()
            {
                VisitId = visitId ?? visitCounter,
                BillingApplication = billingApplication,
                PaymentPriority = paymentPriority,
                VPGuarantor = guarantor
            };

            ((IEntity<VisitStateEnum>) visit).SetState(visitState, "");

            AgingTierEnum agingTier = (AgingTierEnum) agingCount.GetValueOrDefault(0);
            visit.SetAgingTier(agingTier, "");

            VisitAgingHistory firstHistory = visit.VisitAgingHistories.FirstOrDefault();
            if (firstHistory != null) firstHistory.InsertDate = firstHistory.InsertDate.AddSeconds(-1);
            //Removing a second from this status as it's inserted at exactly the same time as other statuses in unit testing.
            VisitStateHistory visitStateHistory = visit.VisitStates.FirstOrDefault();
            if (visitStateHistory != null) visitStateHistory.DateTime = visitStateHistory.DateTime.AddSeconds(-1);

            visit.SourceSystemKey = RandomSourceSystemKeyGenerator.New("NNNNNNNNNN");
            visit.BillingSystem = new BillingSystem(){ BillingSystemId = 1};
            return visit;
        }

        public static void SetStatusFromAgingCount(Tuple<Visit, IList<VisitTransaction>> visitAndTransactions, AgingTierEnum agingTier)
        {
            VisitStateEnum currentVisitStateEnum = VisitStateEnum.Active;
            visitAndTransactions.Item1.SetAgingTier(agingTier, "");
            ((IEntity<VisitStateEnum>)visitAndTransactions.Item1).SetState(currentVisitStateEnum, "");
        }

    }

    public static class VisitFactoryExtensions
    {
        public static Tuple<Visit, IList<VisitTransaction>> WithServiceGroup(this Tuple<Visit, IList<VisitTransaction>> visit, int serviceGroupId)
        {
            visit.Item1.ServiceGroupId = serviceGroupId;
            return visit;
        }

        public static Tuple<Visit, IList<VisitTransaction>> WithActiveBalanceTransferStatus(this Tuple<Visit, IList<VisitTransaction>> visit)
        {
            visit.Item1.SetBalanceTransferStatus(new BalanceTransferStatusHistory { BalanceTransferStatus = new BalanceTransferStatus { BalanceTransferStatusId = (int)BalanceTransferStatusEnum.ActiveBalanceTransfer } });
            return visit;
        }
    }
}
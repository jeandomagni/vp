﻿namespace Ivh.Common.Tests.Helpers.Visit
{
    using System;
    using Application.Base.Common.Interfaces;
    using Application.Core.ApplicationServices;
    using Application.Core.Common.Interfaces;
    using Data.Connection;
    using Data.Connection.Connections;
    using Domain.Visit.Interfaces;
    using Domain.FinanceManagement.FinancePlan.Interfaces;
    using Domain.FinanceManagement.Statement.Interfaces;
    using Moq;
    using NHibernate;

    public class VisitStateApplicationServiceMockBuilder : IMockBuilder<IVisitStateApplicationService, VisitStateApplicationServiceMockBuilder>
    {
        public IApplicationServiceCommonService ApplicationServiceCommonService;
        public IVisitService VisitService;
        public IFinancePlanService FinancePlanService;
        public ISession Session;

        public Mock<IApplicationServiceCommonService> ApplicationServiceCommonServiceMock;
        public Mock<IVisitService> VisitServiceMock;
        public Mock<IFinancePlanService> FinancePlanServiceMock;
        public Mock<ISession> SessionMock;

        public VisitStateApplicationServiceMockBuilder()
        {
            this.ApplicationServiceCommonServiceMock = new Mock<IApplicationServiceCommonService>();
            this.VisitServiceMock = new Mock<IVisitService>();
            this.FinancePlanServiceMock = new Mock<IFinancePlanService>();
            this.SessionMock = new SessionMockBuilder().SessionMock;
        }

        public IVisitStateApplicationService CreateService()
        {
            return new VisitStateApplicationService(
                new Lazy<IApplicationServiceCommonService>(() => this.ApplicationServiceCommonService ?? this.ApplicationServiceCommonServiceMock.Object),
                new Lazy<IVisitService>(() => this.VisitService ?? this.VisitServiceMock.Object),
                new Lazy<IFinancePlanService>(() => this.FinancePlanService ?? this.FinancePlanServiceMock.Object),
                new SessionContext<VisitPay>(this.Session ?? this.SessionMock.Object));
        }

        public VisitStateApplicationServiceMockBuilder Setup(Action<VisitStateApplicationServiceMockBuilder> configAction)
        {
            configAction(this);
            return this;
        }
    }
}
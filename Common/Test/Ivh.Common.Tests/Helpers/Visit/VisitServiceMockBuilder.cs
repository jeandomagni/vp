﻿namespace Ivh.Common.Tests.Helpers.Visit
{
    using System;
    using Base;
    using Domain.Base.Interfaces;
    using Domain.Settings.Interfaces;
    using Domain.Visit.Interfaces;
    using Domain.Visit.Services;
    using Moq;

    public class VisitServiceMockBuilder : IMockBuilder<VisitService, VisitServiceMockBuilder>
    {
        public IDomainServiceCommonService DomainServiceCommonService;
        public IVisitRepository VisitRepository;
        public IVisitTransactionRepository VisitTransactionRepository;
        public IVisitStateService VisitStateService;

        public Mock<IApplicationSettingsService> ApplicationSettingsServiceMock;
        public Mock<IVisitRepository> VisitRepositoryMock;
        public Mock<IVisitTransactionRepository> VisitTransactionRepositoryMock;
        public Mock<IVisitStateService> VisitStateServiceMock;

        public VisitServiceMockBuilder()
        {
            this.VisitRepositoryMock = new Mock<IVisitRepository>();
            this.VisitTransactionRepositoryMock = new Mock<IVisitTransactionRepository>();
            this.VisitStateServiceMock = new Mock<IVisitStateService>();

            this.DomainServiceCommonService = DomainServiceCommonServices.CreateDomainServiceCommonService(this.ApplicationSettingsServiceMock);
        }

        public VisitService CreateService()
        {
            return new VisitService(
                new Lazy<IDomainServiceCommonService>(() => this.DomainServiceCommonService),
                new Lazy<IVisitRepository>(() => this.VisitRepository ?? this.VisitRepositoryMock.Object),
                new Lazy<IVisitTransactionRepository>(() => this.VisitTransactionRepository ?? this.VisitTransactionRepositoryMock.Object),
                new Lazy<IVisitStateService>(() => this.VisitStateService ?? this.VisitStateServiceMock.Object)
            );
        }

        public VisitServiceMockBuilder Setup(Action<VisitServiceMockBuilder> configAction)
        {
            configAction(this);
            return this;
        }
    }
}
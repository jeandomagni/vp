﻿namespace Ivh.Common.Tests.Helpers.Visit
{
    using System;
    using Base;
    using Client;
    using Domain.Base.Interfaces;
    using Domain.Visit.Interfaces;
    using Domain.Visit.Services;
    using Domain.Settings.Interfaces;
    using Moq;

    public class VisitStateServiceMockBuilder : IMockBuilder<VisitStateService, VisitStateServiceMockBuilder>
    {
        public IApplicationSettingsService ApplicationSettingsService;
        public IClientService ClientService;
        public IVisitRepository VisitRepository;
        public IVisitStateMachineService VisitStateMachineService;
        public IDomainServiceCommonService DomainServiceCommonService;

        public Mock<IApplicationSettingsService> ApplicationSettingsServiceMock;
        public Mock<IClientService> ClientServiceMock;
        public Mock<IVisitRepository> VisitRepositoryMock;
        public Mock<IVisitStateMachineService> VisitStateMachineServiceMock;

        public VisitStateServiceMockBuilder()
        {
            this.VisitRepositoryMock = new VisitRepositoryMockBuilder().CreateMock();
            this.ClientServiceMock = new ClientServiceMockBuilder().CreateMock();
            this.ApplicationSettingsServiceMock = new ApplicationSettingsServiceMockBuilder().CreateMock();
            this.DomainServiceCommonService = DomainServiceCommonServices.CreateDomainServiceCommonService(this.ApplicationSettingsServiceMock, null, this.ClientServiceMock);
        }

        public VisitStateService CreateService()
        {
            return new VisitStateService(
                new Lazy<IDomainServiceCommonService>(() => this.DomainServiceCommonService),
                new Lazy<IVisitRepository>(() => this.VisitRepository ?? this.VisitRepositoryMock.Object),
                new Lazy<IVisitStateMachineService>(() => this.VisitStateMachineService ?? this.VisitStateMachineServiceMock.Object));
        }

        public VisitStateServiceMockBuilder Setup(Action<VisitStateServiceMockBuilder> configAction)
        {
            configAction(this);
            return this;
        }
    }
}
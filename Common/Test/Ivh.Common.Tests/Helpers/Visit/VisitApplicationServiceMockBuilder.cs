﻿namespace Ivh.Common.Tests.Helpers.Visit
{
    using System;
    using Application.Base.Common.Interfaces;
    using Application.Core.ApplicationServices;
    using Application.Core.Common.Interfaces;
    using Application.FinanceManagement.Common.Interfaces;
    using Domain.Content.Interfaces;
    using Domain.FinanceManagement.FinancePlan.Interfaces;
    using Domain.Visit.Interfaces;
    using Domain.Guarantor.Interfaces;
	using NHibernate;
	using Base;
    using Data.Connection;
    using Data.Connection.Connections;
    using Domain.Eob.Interfaces;
    using Moq;

    public class VisitApplicationServiceMockBuilder : IMockBuilder<IVisitApplicationService, VisitApplicationServiceMockBuilder>
    {
        public ApplicationServiceCommonServiceMockBuilder ApplicationServiceCommonServiceMockBuilder;
        public IVisitBalanceBaseApplicationService VisitBalanceBaseApplicationService;
        public IVisitService VisitService;
        public IGuarantorService GuarantorService;
        public IImageService ImageService;
        public IFinancePlanService FinancePlanService;
        public IContentService ContentService;
        public IVisitTransactionChangesApplicationService VisitTransactionChangesApplicationService;
        public IFinancePlanApplicationService FinancePlanApplicationService;
        public IVisitPayUserJournalEventApplicationService VisitPayUserJournalEventApplicationService;
        public ISession Session;

        public Mock<IVisitBalanceBaseApplicationService> VisitBalanceBaseApplicationServiceMock;
        public Mock<IVisitService> VisitServiceMock;
        public Mock<IGuarantorService> GuarantorServiceMock;
        public Mock<IImageService> ImageServiceMock;
        public Mock<IFinancePlanService> FinancePlanServiceMock;
        public Mock<IContentService> ContentServiceMock;
        public Mock<IVisitTransactionChangesApplicationService> VisitTransactionChangesApplicationServiceMock;
        public Mock<IFinancePlanApplicationService> FinancePlanApplicationServiceMock;
        public Mock<IVisitPayUserJournalEventApplicationService> VisitPayUserJournalEventApplicationServiceMock;
        public Mock<ISession> SessionMock;

        public VisitApplicationServiceMockBuilder()
        {
            this.ApplicationServiceCommonServiceMockBuilder = new ApplicationServiceCommonServiceMockBuilder();
            this.VisitBalanceBaseApplicationServiceMock = new Mock<IVisitBalanceBaseApplicationService>();
            this.VisitServiceMock = new Mock<IVisitService>();
            this.GuarantorServiceMock = new Mock<IGuarantorService>();
            this.VisitServiceMock = new Mock<IVisitService>();
            this.ImageServiceMock = new Mock<IImageService>();
            this.VisitTransactionChangesApplicationServiceMock = new Mock<IVisitTransactionChangesApplicationService>();
            this.FinancePlanApplicationServiceMock = new Mock<IFinancePlanApplicationService>();
            this.FinancePlanServiceMock = new Mock<IFinancePlanService>();
            this.ContentServiceMock = new Mock<IContentService>();
            this.VisitPayUserJournalEventApplicationServiceMock = new Mock<IVisitPayUserJournalEventApplicationService>();
            this.SessionMock = new Mock<ISession>();
        }
        
        public IVisitApplicationService CreateService()
        {
            return new VisitApplicationService(
                this.ApplicationServiceCommonServiceMockBuilder.CreateServiceLazy(),
                new Lazy<IVisitBalanceBaseApplicationService>(() => this.VisitBalanceBaseApplicationService ?? this.VisitBalanceBaseApplicationServiceMock.Object),
                new Lazy<IVisitService>(() => this.VisitService ?? this.VisitServiceMock.Object),
                new Lazy<IGuarantorService>(() => this.GuarantorService ?? this.GuarantorServiceMock.Object),
                new Lazy<IImageService>(() => this.ImageService ?? this.ImageServiceMock.Object),
                new Lazy<IFinancePlanService>(() => this.FinancePlanService ?? this.FinancePlanServiceMock.Object),
                new Lazy<IContentService>(() => this.ContentService ?? this.ContentServiceMock.Object),
                new Lazy<IVisitTransactionChangesApplicationService>(() => this.VisitTransactionChangesApplicationService ?? this.VisitTransactionChangesApplicationServiceMock.Object),
                new Lazy<IFinancePlanApplicationService>(() => this.FinancePlanApplicationService ?? this.FinancePlanApplicationServiceMock.Object),
                new Lazy<IVisitPayUserJournalEventApplicationService>(() => this.VisitPayUserJournalEventApplicationService ?? this.VisitPayUserJournalEventApplicationServiceMock.Object),
                new SessionContext<VisitPay>(this.Session ?? this.SessionMock.Object));
        }

        public VisitApplicationServiceMockBuilder Setup(Action<VisitApplicationServiceMockBuilder> configAction)
        {
            configAction(this);
            return this;
        }
    }
}
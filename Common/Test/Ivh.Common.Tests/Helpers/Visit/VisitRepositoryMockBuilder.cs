﻿namespace Ivh.Common.Tests.Helpers.Visit
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Common.Base.Enums;
    using Domain.Visit.Entities;
    using Domain.Visit.Interfaces;
    using Moq;

    public class VisitRepositoryMockBuilder
    {
        public Mock<IVisitRepository> CreateMock(Action<Mock<IVisitRepository>> configAction = null)
        {
            Mock<IVisitRepository> mock = new Mock<IVisitRepository>();

            configAction?.Invoke(mock);
            
            return mock;
        }
    }
}
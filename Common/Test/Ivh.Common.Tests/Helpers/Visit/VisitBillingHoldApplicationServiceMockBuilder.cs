﻿namespace Ivh.Common.Tests.Helpers.Visit
{
    using System;
    using Application.Base.Common.Interfaces;
    using Application.Core.ApplicationServices;
    using Application.Core.Common.Interfaces;
    using Client;
    using Data.Connection;
    using Data.Connection.Connections;
    using Domain.Content.Interfaces;
    using Domain.FinanceManagement.FinancePlan.Interfaces;
    using Domain.Guarantor.Interfaces;
    using Domain.Visit.Interfaces;
    using Domain.FinanceManagement.Statement.Interfaces;
    using Domain.Settings.Interfaces;
    using MassTransit;
    using Moq;
    using NHibernate;

    public class VisitBillingHoldApplicationServiceMockBuilder : IMockBuilder<IVisitBillingHoldApplicationService, VisitBillingHoldApplicationServiceMockBuilder>
    {
        public IApplicationServiceCommonService ApplicationServiceCommonService;
        public IVisitService VisitService;
        public IVpStatementService StatementService;
        public IGuarantorService GuarantorService;
        public IFinancePlanService FinancePlanService;
        public IFeatureApplicationService FeatureService;
        public IClientService ClientService;
        public IVisitTransactionService VisitTransactionService;
        public IBus Bus;
        public ISession Session;
        public IImageProvider ImageProvider;
        public IVisitPayUserJournalEventApplicationService VisitPayUserJournalEventApplicationService;

        public Mock<IApplicationServiceCommonService> ApplicationServiceCommonServiceMock;
        public Mock<IVisitService> VisitServiceMock;
        public Mock<IVpStatementService> StatementServiceMock;
        public Mock<IGuarantorService> GuarantorServiceMock;
        public Mock<IFinancePlanService> FinancePlanServiceMock;
        public Mock<IFeatureApplicationService> FeatureServiceMock;
        public Mock<IClientService> ClientServiceMock;
        public Mock<IVisitTransactionService> VisitTransactionServiceMock;
        public Mock<IBus> BusMock;
        public Mock<ISession> SessionMock;
        public Mock<IImageProvider> ImageProviderMock;
        public Mock<IVisitPayUserJournalEventApplicationService> VisitPayUserJournalEventApplicationServiceMock;

        public VisitBillingHoldApplicationServiceMockBuilder()
        {
            this.VisitServiceMock = new Mock<IVisitService>();
            this.StatementServiceMock = new Mock<IVpStatementService>();
            this.GuarantorServiceMock = new Mock<IGuarantorService>();
            this.FinancePlanServiceMock = new Mock<IFinancePlanService>();
            this.FeatureServiceMock = new Mock<IFeatureApplicationService>();
            this.ClientServiceMock = new ClientServiceMockBuilder().CreateMock();
            this.VisitTransactionServiceMock = new Mock<IVisitTransactionService>();
            this.BusMock = new Mock<IBus>();
            this.SessionMock = new SessionMockBuilder().SessionMock;
            this.ImageProviderMock = new Mock<IImageProvider>();
            this.VisitPayUserJournalEventApplicationServiceMock = new Mock<IVisitPayUserJournalEventApplicationService>();
        }

        public IVisitBillingHoldApplicationService CreateService()
        {
            return new VisitBillingHoldApplicationService(
                new Lazy<IApplicationServiceCommonService>(() => this.ApplicationServiceCommonService ?? this.ApplicationServiceCommonServiceMock.Object),
                new Lazy<IVisitService>(() => this.VisitService ?? this.VisitServiceMock.Object),
                new Lazy<IFinancePlanService>(() => this.FinancePlanService ?? this.FinancePlanServiceMock.Object), 
                new SessionContext<VisitPay>(this.Session ?? this.SessionMock.Object));
        }

        public VisitBillingHoldApplicationServiceMockBuilder Setup(Action<VisitBillingHoldApplicationServiceMockBuilder> configAction)
        {
            configAction(this);
            return this;
        }
    }
}
namespace Ivh.Common.Tests.Helpers.Visit
{
    using System;
    using Common.Base.Utilities.Helpers;
    using Domain.Visit.Entities;
    using VisitPay.Enums;

    public class VisitTransactionFactory
    {
        private static int _visitTransactionIdCounter = 1;

        public static VisitTransaction GenerateVisitTransaction(decimal amount)
        {
            // removed VpTransactionTypeEnum.VppInterestCharge as this is no longer tracked on transactions
            return GenerateVisitTransaction(amount, VpTransactionTypeEnum.HsCharge);
        }
        
        public static VisitTransaction GenerateVisitTransaction(decimal amount, VpTransactionTypeEnum vpTransactionType)
        {
            VisitTransaction visitTransaction = new VisitTransaction()
            {
                VisitTransactionId = _visitTransactionIdCounter++,
                VpTransactionType = new VpTransactionType() { VpTransactionTypeId = (int)vpTransactionType },
                InsertDate = DateTime.UtcNow
            };
            visitTransaction.SetSourceSystemKey(RandomSourceSystemKeyGenerator.New("NNNNNNNNN"));
            visitTransaction.SetTransactionAmount(amount);
            return visitTransaction;
        }
    }
}
﻿namespace Ivh.Common.Tests.Helpers.Visit
{
    using System;
    using Application.FinanceManagement.Common.Interfaces;
    using Ivh.Domain.FinanceManagement.Statement.Interfaces;
    using Ivh.Domain.Visit.Interfaces;
    using Ivh.Domain.FinanceManagement.FinancePlan.Interfaces;
    using Ivh.Domain.Guarantor.Interfaces;
    using NHibernate;
    using Ivh.Application.FinanceManagement.ApplicationServices;
    using Base;
    using Data.Connection;
    using Data.Connection.Connections;
    using Moq;

    public class PastDueUncollectableApplicationServiceMockBuilder : IMockBuilder<IPastDueUncollectableApplicationService, PastDueUncollectableApplicationServiceMockBuilder>
    {
        public ApplicationServiceCommonServiceMockBuilder ApplicationServiceCommonServiceMockBuilder;
        public IVpStatementService VpStatementService;
        public IVisitService VisitService;
        public IFinancePlanService FinancePlanService;
        public IGuarantorService GuarantorService;
        public ISession Session;

        public Mock<IVpStatementService> VpStatementServiceMock;
        public Mock<IVisitService> VisitServiceMock;
        public Mock<IFinancePlanService> FinancePlanServiceMock;
        public Mock<IGuarantorService> GuarantorServiceMock;
        public Mock<ISession> SessionMock;

        public PastDueUncollectableApplicationServiceMockBuilder()
        {
            this.ApplicationServiceCommonServiceMockBuilder = new ApplicationServiceCommonServiceMockBuilder();
            this.VpStatementServiceMock = new Mock<IVpStatementService>();
            this.VisitServiceMock = new Mock<IVisitService>();
            this.FinancePlanServiceMock = new Mock<IFinancePlanService>();
            this.GuarantorServiceMock = new Mock<IGuarantorService>();
            this.SessionMock = new SessionMockBuilder().SessionMock;
        }

        public IPastDueUncollectableApplicationService CreateService()
        {
            return new PastDueUncollectableApplicationService(
                this.ApplicationServiceCommonServiceMockBuilder.CreateServiceLazy(),
                new Lazy<IVpStatementService>(() => this.VpStatementService ?? this.VpStatementServiceMock.Object),
                new Lazy<IVisitService>(() => this.VisitService ?? this.VisitServiceMock.Object),
                new Lazy<IFinancePlanService>(() => this.FinancePlanService ?? this.FinancePlanServiceMock.Object),
                new Lazy<IGuarantorService>(() => this.GuarantorService ?? this.GuarantorServiceMock.Object),
                new SessionContext<VisitPay>(this.Session ?? this.SessionMock.Object)
            );
        }

        public PastDueUncollectableApplicationServiceMockBuilder Setup(Action<PastDueUncollectableApplicationServiceMockBuilder> configAction)
        {
            configAction(this);
            return this;
        }
    }
}
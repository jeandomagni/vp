﻿namespace Ivh.Common.Tests.Helpers.ExternalLink
{
    using System;
    using Application.Base.Common.Interfaces;
    using Application.Core.ApplicationServices;
    using Application.Core.Common.Interfaces;
    using Domain.Visit.Interfaces;
    using Domain.Visit.Services;
    using Moq;

    public class ExternalLinkApplicationServiceMockBuilder : IMockBuilder<IExternalLinkApplicationService, ExternalLinkApplicationServiceMockBuilder>
    {
        public IApplicationServiceCommonService ApplicationServiceCommonService;
        public InsuranceService InsuranceService;

        public Mock<IApplicationServiceCommonService> ApplicationServiceCommonServiceMock;
        public Mock<IInsuranceService> InsuranceServiceMock;

        public ExternalLinkApplicationServiceMockBuilder()
        {
            this.ApplicationServiceCommonServiceMock = new Mock<IApplicationServiceCommonService>();
            this.InsuranceServiceMock = new Mock<IInsuranceService>();
        }

        public IExternalLinkApplicationService CreateService()
        {
            return new ExternalLinkApplicationService(
                new Lazy<IApplicationServiceCommonService>(() => this.ApplicationServiceCommonService ?? this.ApplicationServiceCommonServiceMock.Object),
                new Lazy<IInsuranceService>(() => this.InsuranceService ?? this.InsuranceServiceMock.Object)
            );
        }

        public ExternalLinkApplicationServiceMockBuilder Setup(Action<ExternalLinkApplicationServiceMockBuilder> configAction)
        {
            configAction(this);
            return this;
        }
    }
}
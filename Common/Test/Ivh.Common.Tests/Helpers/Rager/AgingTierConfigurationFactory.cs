﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ivh.Common.Tests.Helpers.Rager
{
    using System.Collections;
    using Common.Base.Enums;
    using Domain.Rager.Entities;
    using VisitPay.Enums;

    public class AgingTierConfigurationFactory
    {
        public static IList<AgingTierConfiguration> AgingTierConfigurations()
        {
            DateTime activeDate = DateTime.UtcNow.AddDays(-30);
           
            // need to call this.AddAgingTiers() first;
            //not statemented
            MinimumCommunicationConfiguration tier0CommunicationConfiguration = new MinimumCommunicationConfiguration()
            {
                MinimumCommunicationTypeCounts = new Dictionary<AgingCommunicationTypeEnum, int?>
                {
                    [AgingCommunicationTypeEnum.PastDueNotification] = 0,
                    [AgingCommunicationTypeEnum.StatementNotification] = 0
                }
            };
            // good standing
            MinimumCommunicationConfiguration tier1CommunicationConfiguration = new MinimumCommunicationConfiguration()
            {
                MinimumCommunicationTypeCounts = new Dictionary<AgingCommunicationTypeEnum, int?>
                {
                    [AgingCommunicationTypeEnum.PastDueNotification] = 0,
                    [AgingCommunicationTypeEnum.StatementNotification] = 1
                }
            };
            // past due
            MinimumCommunicationConfiguration tier2CommunicationConfiguration = new MinimumCommunicationConfiguration()
            {
                MinimumCommunicationTypeCounts = new Dictionary<AgingCommunicationTypeEnum, int?>
                {
                    [AgingCommunicationTypeEnum.PastDueNotification] = 0,
                    [AgingCommunicationTypeEnum.StatementNotification] = 1
                }
            };
            // final past due
            MinimumCommunicationConfiguration tier3CommunicationConfiguration = new MinimumCommunicationConfiguration()
            {
                MinimumCommunicationTypeCounts = new Dictionary<AgingCommunicationTypeEnum, int?>
                {
                    [AgingCommunicationTypeEnum.PastDueNotification] = 0,
                    [AgingCommunicationTypeEnum.StatementNotification] = 3
                }
            };
            // uncollectable
            MinimumCommunicationConfiguration tier4CommunicationConfiguration = new MinimumCommunicationConfiguration()
            {
                MinimumCommunicationTypeCounts = new Dictionary<AgingCommunicationTypeEnum, int?>
                {
                    [AgingCommunicationTypeEnum.PastDueNotification] = 1,
                    [AgingCommunicationTypeEnum.StatementNotification] = 4
                }
            };

            IList<AgingTierConfiguration> agingTierConfigurations = new List<AgingTierConfiguration>
            {
                new AgingTierConfiguration
                {
                    ActiveDate = activeDate,
                    MinAge = 0,
                    MaxAge = 1000000,
                    MinimumCommunicationConfiguration = tier0CommunicationConfiguration.JsonData,
                    ResultingAgingTier = AgingTiers[0]
                },
                new AgingTierConfiguration
                {
                    ActiveDate = activeDate,
                    MinAge = 0,
                    MaxAge = 1000000,
                    MinimumCommunicationConfiguration = tier1CommunicationConfiguration.JsonData,
                    ResultingAgingTier = AgingTiers[1]
                },
                new AgingTierConfiguration
                {
                    ActiveDate = activeDate,
                    MinAge = 46,
                    MaxAge = 1000000,
                    MinimumCommunicationConfiguration = tier2CommunicationConfiguration.JsonData,
                    ResultingAgingTier = AgingTiers[2]
                },
                new AgingTierConfiguration
                {
                    ActiveDate = activeDate,
                    MinAge = 100,
                    MaxAge = 1000000,
                    MinimumCommunicationConfiguration = tier3CommunicationConfiguration.JsonData,
                    ResultingAgingTier = AgingTiers[3]
                },
                new AgingTierConfiguration
                {
                    ActiveDate = activeDate,
                    MinAge = 111,
                    MaxAge = 1000000,
                    MinimumCommunicationConfiguration = tier4CommunicationConfiguration.JsonData,
                    ResultingAgingTier = AgingTiers[4]
                }
            };

            return agingTierConfigurations;

        }

        public static Dictionary<int, AgingTier> AgingTiers =  new Dictionary<int, AgingTier>()
            {
                [0] = new AgingTier { AgingTierId = 0, Description = "Not Statemented" },
                [1] = new AgingTier { AgingTierId = 1, Description = "Good Standing" },
                [2] = new AgingTier { AgingTierId = 2, Description = "Past Due" },
                [3] = new AgingTier { AgingTierId = 3, Description = "Final Past Due" },
                [4] = new AgingTier { AgingTierId = 4, Description = "Uncollectable" },
            };
     

    }
}

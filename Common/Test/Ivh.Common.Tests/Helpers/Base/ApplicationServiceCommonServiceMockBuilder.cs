﻿namespace Ivh.Common.Tests.Helpers.Base
{
    using System;
    using Application.Base.Common.Interfaces;
    using Domain.EventJournal.Interfaces;
    using Domain.Settings.Entities;
    using Ivh.Domain.Settings.Interfaces;
    using Ivh.Domain.Logging.Interfaces;
    using Ivh.Common.Cache;
    using Ivh.Common.Base.Utilities.Helpers;
    using Ivh.Application.Base.Services;
    using Moq;
    using ServiceBus.Interfaces;
    using VisitPay.Enums;

    public class ApplicationServiceCommonServiceMockBuilder : IMockBuilder<IApplicationServiceCommonService, ApplicationServiceCommonServiceMockBuilder>
    {
        public IApplicationSettingsService ApplicationSettingsService;
        public IFeatureService FeatureService;
        public IClientService ClientService;
        public ILoggingService LoggingService;
        public IEventJournalService EventJournalService;
        public IBus Bus;
        public IDistributedCache DistributedCache;
        public TimeZoneHelper TimeZoneHelper;

        public Mock<IApplicationSettingsService> ApplicationSettingsServiceMock;
        public Mock<IFeatureService> FeatureServiceMock;
        public Mock<IClientService> ClientServiceMock;
        public Mock<ILoggingService> LoggingServiceMock;
        public Mock<IEventJournalService> EventJournalServiceMock;
        public Mock<IBus> BusMock;
        public Mock<IDistributedCache> DistributedCacheMock;
        public Mock<TimeZoneHelper> TimeZoneHelperMock;

        public ApplicationServiceCommonServiceMockBuilder()
        {
            this.ApplicationSettingsServiceMock = new Mock<IApplicationSettingsService>();
            this.ApplicationSettingsServiceMock.Setup(x => x.PastDueAgingCount).Returns(new ApplicationSetting<int>(() => new Lazy<int>(() => 1)));

            this.FeatureServiceMock = new Mock<IFeatureService>();
            this.ClientServiceMock = new Mock<IClientService>();
            this.LoggingServiceMock = new Mock<ILoggingService>();
            this.EventJournalServiceMock = new Mock<IEventJournalService>();
            this.BusMock = new Mock<IBus>();
            this.DistributedCacheMock = new Mock<IDistributedCache>();
            this.TimeZoneHelperMock = new Mock<TimeZoneHelper>();
        }

        public IApplicationServiceCommonService CreateService()
        {
            return new ApplicationServiceCommonService(
            new Lazy<IApplicationSettingsService>(() => this.ApplicationSettingsService ?? this.ApplicationSettingsServiceMock.Object),
            new Lazy<IFeatureService>(() => this.FeatureService ?? this.FeatureServiceMock.Object),
            new Lazy<IClientService>(() => this.ClientService ?? this.ClientServiceMock.Object),
            new Lazy<ILoggingService>(() => this.LoggingService ?? this.LoggingServiceMock.Object),
            new Lazy<IEventJournalService>(() => this.EventJournalService ?? this.EventJournalServiceMock.Object),
            new Lazy<IBus>(() => this.Bus ?? this.BusMock.Object),
            new Lazy<IDistributedCache>(() => this.DistributedCache ?? this.DistributedCacheMock.Object),
            new Lazy<TimeZoneHelper>(() => this.TimeZoneHelper ?? new Mock<TimeZoneHelper>(TimeZoneHelper.TimeZones.MountainStandardTime, TimeZoneHelper.TimeZones.MountainStandardTime).Object),
            new Lazy<IInstanceCache>(() => new InstanceCache()));
        }

        public ApplicationServiceCommonServiceMockBuilder Setup(Action<ApplicationServiceCommonServiceMockBuilder> configAction)
        {
            configAction(this);
            return this;
        }

        public  Lazy<IApplicationServiceCommonService> CreateServiceLazy()
        {
            return new Lazy<IApplicationServiceCommonService>(this.CreateService);
        }

        public ApplicationServiceCommonServiceMockBuilder EnableFeatures(params VisitPayFeatureEnum[] features)
        {
            foreach (VisitPayFeatureEnum feature in features)
            {
                this.FeatureServiceMock.Setup(t => t.IsFeatureEnabled(feature)).Returns<VisitPayFeatureEnum>((f) => true);
            }

            return this;
        }

        public ApplicationServiceCommonServiceMockBuilder EnableAllFeatures()
        {
            this.FeatureServiceMock.Setup(t => t.IsFeatureEnabled(It.IsAny<VisitPayFeatureEnum>())).Returns<VisitPayFeatureEnum>((feature) => true);
            return this;
        }
    }
}
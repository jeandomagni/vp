﻿namespace Ivh.Common.Tests.Helpers.Base
{
    using System;
    using Domain.AppIntelligence.Interfaces;
    using Domain.AppIntelligence.Services;
    using Domain.Base.Interfaces;
    using Domain.Guarantor.Interfaces;
    using Domain.Settings.Entities;
    using Ivh.Domain.Settings.Interfaces;
    using Ivh.Domain.Logging.Interfaces;
    using Ivh.Common.Cache;
    using Ivh.Common.Base.Utilities.Helpers;
    using Ivh.Domain.Base.Services;
    using Moq;
    using ServiceBus.Interfaces;
    using VisitPay.Enums;

    public class DomainServiceCommonServiceMockBuilder : IMockBuilder<IDomainServiceCommonService, DomainServiceCommonServiceMockBuilder>
    {
        public IApplicationSettingsService ApplicationSettingsService;
        public IFeatureService FeatureService;
        public IClientService ClientService;
        public ILoggingService LoggingService;
        public IBus Bus;
        public IDistributedCache DistributedCache;
        public IGuarantorScoreService ScoringService;
        public IRandomizedTestService RandomizedTestService;
        public TimeZoneHelper TimeZoneHelper;

        public Mock<IApplicationSettingsService> ApplicationSettingsServiceMock;
        public Mock<IFeatureService> FeatureServiceMock;
        public Mock<IClientService> ClientServiceMock;
        public Mock<ILoggingService> LoggingServiceMock;
        public Mock<IBus> BusMock;
        public Mock<IDistributedCache> DistributedCacheMock;
        public Mock<TimeZoneHelper> TimeZoneHelperMock;
        public Mock<IGuarantorScoreService> ScoringServiceMock;
        public Mock<IRandomizedTestService> RandomizedTestServiceMock;

        public DomainServiceCommonServiceMockBuilder()
        {
            this.ApplicationSettingsServiceMock = new Mock<IApplicationSettingsService>();
            this.ApplicationSettingsServiceMock.Setup(x => x.PastDueAgingCount).Returns(new ApplicationSetting<int>(() => new Lazy<int>(() => 1)));

            this.FeatureServiceMock = new Mock<IFeatureService>();
            this.ClientServiceMock = new Mock<IClientService>();
            this.LoggingServiceMock = new Mock<ILoggingService>();
            this.BusMock = new Mock<IBus>();
            this.DistributedCacheMock = new Mock<IDistributedCache>();
            this.TimeZoneHelperMock = new Mock<TimeZoneHelper>();
            this.RandomizedTestServiceMock = new Mock<IRandomizedTestService>();
            this.ScoringServiceMock = new Mock<IGuarantorScoreService>();
        }

        public IDomainServiceCommonService CreateService()
        {
            return new DomainServiceCommonService(
                new Lazy<IApplicationSettingsService>(() => this.ApplicationSettingsService ?? this.ApplicationSettingsServiceMock.Object),
                new Lazy<IFeatureService>(() => this.FeatureService ?? this.FeatureServiceMock.Object),
                new Lazy<IClientService>(() => this.ClientService ?? this.ClientServiceMock.Object),
                new Lazy<ILoggingService>(() => this.LoggingService ?? this.LoggingServiceMock.Object),
                new Lazy<IBus>(() => this.Bus ?? this.BusMock.Object),
                new Lazy<IDistributedCache>(() => this.DistributedCache ?? this.DistributedCacheMock.Object),
                new Lazy<TimeZoneHelper>(() => this.TimeZoneHelper ?? this.TimeZoneHelperMock.Object),
                new Lazy<IInstanceCache>(() => new InstanceCache()));
        }

        public DomainServiceCommonServiceMockBuilder Setup(Action<DomainServiceCommonServiceMockBuilder> configAction)
        {
            configAction(this);
            return this;
        }

        public Lazy<IDomainServiceCommonService> CreateServiceLazy()
        {
            return new Lazy<IDomainServiceCommonService>(this.CreateService);
        }

        public Lazy<IGuarantorScoreService> CreateScoringServiceLazy()
        {
            return new Lazy<IGuarantorScoreService>(() => this.ScoringService ?? this.ScoringServiceMock.Object);
        }

        public Lazy<IRandomizedTestService> CreateRandomizedTestServiceLazy()
        {
            return new Lazy<IRandomizedTestService>(() => this.RandomizedTestService ?? this.RandomizedTestServiceMock.Object);
        }

        public DomainServiceCommonServiceMockBuilder EnableFeatures(params VisitPayFeatureEnum[] features)
        {
            foreach (VisitPayFeatureEnum feature in features)
            {
                this.FeatureServiceMock.Setup(t => t.IsFeatureEnabled(feature)).Returns<VisitPayFeatureEnum>((f) => true);
            }

            return this;
        }

        public DomainServiceCommonServiceMockBuilder EnableAllFeatures()
        {
            this.FeatureServiceMock.Setup(t => t.IsFeatureEnabled(It.IsAny<VisitPayFeatureEnum>())).Returns<VisitPayFeatureEnum>((feature) => true);
            return this;
        }
    }
}
﻿namespace Ivh.Common.Tests.Helpers.Base
{
    using System;
    using Cache;
    using Common.Base.Utilities.Helpers;
    using Domain.Base.Services;
    using Domain.Logging.Interfaces;
    using Domain.Settings.Entities;
    using Domain.Settings.Interfaces;
    using Ivh.Domain.Base.Interfaces;
    using Moq;
    using ServiceBus.Interfaces;

    public class DomainServiceCommonServices
    {
        public static IDomainServiceCommonService CreateDomainServiceCommonService(
            Mock<IApplicationSettingsService> applicationSettingsServiceMock = null,
            Mock<IFeatureService> featureServiceMock = null,
            Mock<IClientService> clientServiceMock = null,
            Mock<ILoggingService> loggingServiceMock = null,
            Mock<IBus> busMock = null,
            Mock<IDistributedCache> cacheMock = null,
            Mock<TimeZoneHelper> timeZoneHelperMock = null,
            Mock<IInstanceCache> instanceCacheMock = null
        )
        {
            Mock<IApplicationSettingsService> defaultApplicationSettingsServiceMock = new Mock<IApplicationSettingsService>();

            if (applicationSettingsServiceMock == null)
            {
                defaultApplicationSettingsServiceMock.Setup(x => x.PastDueAgingCount).Returns(new ApplicationSetting<int>(() => new Lazy<int>(() => 1)));
            }

            return new DomainServiceCommonService(
                new Lazy<IApplicationSettingsService>(() => (applicationSettingsServiceMock ?? defaultApplicationSettingsServiceMock).Object),
                new Lazy<IFeatureService>(() => (featureServiceMock ?? new Mock<IFeatureService>()).Object),
                new Lazy<IClientService>(() => (clientServiceMock ?? new Mock<IClientService>()).Object),
                new Lazy<ILoggingService>(() => (loggingServiceMock ?? new Mock<ILoggingService>()).Object),
                new Lazy<IBus>(() => (busMock ?? new Mock<IBus>()).Object),
                new Lazy<IDistributedCache>(() => (cacheMock ?? new Mock<IDistributedCache>()).Object),
                new Lazy<TimeZoneHelper>(() => (timeZoneHelperMock ?? new Mock<TimeZoneHelper>(TimeZoneHelper.TimeZones.MountainStandardTime, TimeZoneHelper.TimeZones.MountainStandardTime)).Object),
                new Lazy<IInstanceCache>(() => (instanceCacheMock ?? new Mock<IInstanceCache>()).Object)
            );
        }
        public static Lazy<IDomainServiceCommonService> CreateDomainServiceCommonServiceLazy(
            Mock<IApplicationSettingsService> applicationSettingsServiceMock = null,
            Mock<IFeatureService> featureServiceMock = null,
            Mock<IClientService> clientServiceMock = null,
            Mock<ILoggingService> loggingServiceMock = null,
            Mock<IBus> busMock = null,
            Mock<IDistributedCache> cacheMock = null,
            Mock<TimeZoneHelper> timeZoneHelperMock = null,
            Mock<IInstanceCache> instanceCacheMock = null
        )
        {
            return new Lazy<IDomainServiceCommonService>(() => CreateDomainServiceCommonService(
                applicationSettingsServiceMock,
                featureServiceMock,
                clientServiceMock,
                loggingServiceMock,
                busMock,
                cacheMock,
                timeZoneHelperMock,
                instanceCacheMock));
        }
    }
}

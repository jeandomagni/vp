﻿namespace Ivh.Common.Tests.Helpers.Base
{
    using System;
    using System.Data;
    using Ivh.Application.Base.Common.Interfaces;
    using Ivh.Domain.FinanceManagement.FinancePlan.Interfaces;
    using Ivh.Domain.FinanceManagement.Payment.Interfaces;
    using Ivh.Domain.Visit.Interfaces;
    using Ivh.Application.Base.Services;
    using Base;
    using Moq;

    public class VisitBalanceBaseApplicationServiceMockBuilder : IMockBuilder<IVisitBalanceBaseApplicationService, VisitBalanceBaseApplicationServiceMockBuilder>
    {
        public ApplicationServiceCommonServiceMockBuilder ApplicationServiceCommonServiceMockBuilder;
        public IFinancePlanService FinancePlanService;
        public IPaymentAllocationService PaymentAllocationService;
        public IVisitService VisitService;

        public Mock<IFinancePlanService> FinancePlanServiceMock;
        public Mock<IPaymentAllocationService> PaymentAllocationServiceMock;
        public Mock<IVisitService> VisitServiceMock;

        public VisitBalanceBaseApplicationServiceMockBuilder()
        {
            this.ApplicationServiceCommonServiceMockBuilder = new ApplicationServiceCommonServiceMockBuilder();
            this.FinancePlanServiceMock = new Mock<IFinancePlanService>();
            this.PaymentAllocationServiceMock = new Mock<IPaymentAllocationService>();
            this.VisitServiceMock = new Mock<IVisitService>();
        }

        public IVisitBalanceBaseApplicationService CreateService()
        {
            return new VisitBalanceBaseApplicationService(
                this.ApplicationServiceCommonServiceMockBuilder.CreateServiceLazy(),
                new Lazy<IFinancePlanService>(() => this.FinancePlanService ?? this.FinancePlanServiceMock.Object),
                new Lazy<IPaymentAllocationService>(() => this.PaymentAllocationService ?? this.PaymentAllocationServiceMock.Object),
                new Lazy<IVisitService>(() => this.VisitService ?? this.VisitServiceMock.Object)
            );
        }

        public VisitBalanceBaseApplicationServiceMockBuilder Setup(Action<VisitBalanceBaseApplicationServiceMockBuilder> configAction)
        {
            configAction(this);
            return this;
        }
    }
}

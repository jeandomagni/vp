﻿namespace Ivh.Common.Tests.Helpers.RestrictedCountryAccess
{
    using System;
    using System.Data;
    using Domain.Logging.Interfaces;
    using Domain.Settings.Interfaces;
    using Ivh.Domain.Core.RestrictedCountryAccess.Interfaces;
	using Ivh.Common.Cache;
	using Ivh.Domain.Core.RestrictedCountryAccess.Services;
    using Moq;

    public class RestrictedCountryAccessServiceMockBuilder : IMockBuilder<IRestrictedCountryAccessService, RestrictedCountryAccessServiceMockBuilder>
    {
        public ICountryIpLookupProvider CountryIpLookupProvider;
		public IDistributedCache DistributedCache;
		public IRestrictedCountryAccessRepository RestrictedCountryAccessRepository;
		public IFeatureService FeatureService;
		public ILoggingService LoggingService;
        public Mock<ICountryIpLookupProvider> CountryIpLookupProviderMock;
		public Mock<IDistributedCache> DistributedCacheMock;
		public Mock<IRestrictedCountryAccessRepository> RestrictedCountryAccessRepositoryMock;
		public Mock<IFeatureService> FeatureServiceMock;
		public Mock<ILoggingService> LoggingServiceMock;

        public RestrictedCountryAccessServiceMockBuilder()
        {
            this.CountryIpLookupProviderMock = new Mock<ICountryIpLookupProvider>();
			this.DistributedCacheMock = new Mock<IDistributedCache>();
			this.RestrictedCountryAccessRepositoryMock = new Mock<IRestrictedCountryAccessRepository>();
			this.FeatureServiceMock = new Mock<IFeatureService>();
			this.LoggingServiceMock = new Mock<ILoggingService>();
        }
        
        public IRestrictedCountryAccessService CreateService()
        {
            return new RestrictedCountryAccessService(
                new Lazy<ICountryIpLookupProvider>(() => this.CountryIpLookupProvider ?? this.CountryIpLookupProviderMock.Object),
				new Lazy<IDistributedCache>(() => this.DistributedCache ?? this.DistributedCacheMock.Object),
				new Lazy<IRestrictedCountryAccessRepository>(() => this.RestrictedCountryAccessRepository ?? this.RestrictedCountryAccessRepositoryMock.Object),
				new Lazy<IFeatureService>(() => this.FeatureService ?? this.FeatureServiceMock.Object),
				new Lazy<ILoggingService>(() => this.LoggingService ?? this.LoggingServiceMock.Object)
            ); 
         }
        
        public RestrictedCountryAccessServiceMockBuilder Setup(Action<RestrictedCountryAccessServiceMockBuilder> configAction)
        {
            configAction(this);
            return this;
        }
    }
}
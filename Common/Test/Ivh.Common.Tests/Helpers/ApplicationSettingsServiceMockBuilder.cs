﻿namespace Ivh.Common.Tests.Helpers
{
    using System;
    using System.Linq.Expressions;
    using Domain.Settings.Entities;
    using Domain.Settings.Interfaces;
    using Moq;

    public class ApplicationSettingsServiceMockBuilder : IMockBuilder<IApplicationSettingsService, ApplicationSettingsServiceMockBuilder>
    {
        public Mock<IApplicationSettingsService> ApplicationSettingsServiceMock;

        public ApplicationSettingsServiceMockBuilder()
        {
            this.ApplicationSettingsServiceMock = new Mock<IApplicationSettingsService>();
            this.ApplicationSettingsServiceMock.Setup(x => x.IsClientApplication).Returns(() => new ApplicationSetting<bool>(() => new Lazy<bool>(false)));
            this.ApplicationSettingsServiceMock.Setup(x => x.PastDueAgingCount).Returns(() => new ApplicationSetting<int>(() => new Lazy<int>(() => 1)));
        }

        public IApplicationSettingsService CreateService()
        {
            return this.ApplicationSettingsServiceMock.Object;
        }

        public Mock<IApplicationSettingsService> CreateMock()
        {
            return this.ApplicationSettingsServiceMock;
        }

        public ApplicationSettingsServiceMockBuilder Setup(Action<ApplicationSettingsServiceMockBuilder> configAction)
        {
            configAction(this);
            return this;
        }

        public ApplicationSettingsServiceMockBuilder SetSetting<T>(Expression<Func<IApplicationSettingsService, IApplicationSetting<T>>> expression, Func<T> factory)
        {
            this.ApplicationSettingsServiceMock.WithSetting(expression, factory);
            return this;
        }
    }

    public static class ApplicationSettingsMockExtensions
    {
        public static void WithSetting<T>(this Mock<IApplicationSettingsService> mock, 
            Expression<Func<IApplicationSettingsService, IApplicationSetting<T>>> expression, Func<T> factory)
        {
            Mock<IApplicationSetting<T>> mockSetting = new Mock<IApplicationSetting<T>>();
            mockSetting.Setup(x => x.Value).Returns(factory());
            mock.Setup(expression).Returns(mockSetting.Object);
        }
    }
}
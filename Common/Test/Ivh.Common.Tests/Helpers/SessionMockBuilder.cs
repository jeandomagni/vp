﻿namespace Ivh.Common.Tests.Helpers
{
    using System;
    using System.Data;
    using Moq;
    using NHibernate;
    using NHibernate.Transaction;

    public class SessionMockBuilder : IMockBuilder<ISession, SessionMockBuilder>
    {
        public Mock<ISession> SessionMock;
        public Mock<ITransaction> TransactionMock;
        
        public SessionMockBuilder()
        {
            this.TransactionMock = new Mock<ITransaction>();
            this.TransactionMock.Setup(t => t.IsActive).Returns(() => true);
            
            this.SessionMock = new Mock<ISession>();
            this.SessionMock.Setup(t => t.Transaction).Returns(() => this.TransactionMock.Object);
            this.SessionMock.Setup(t => t.BeginTransaction(Ivh.Common.Data.Constants.IsolationLevel.Default)).Returns(() => this.TransactionMock.Object);
            this.SessionMock.Setup(t => t.BeginTransaction(IsolationLevel.ReadCommitted)).Returns(() => this.TransactionMock.Object);
        }

        public ISession CreateService()
        {
            return this.SessionMock.Object;
        }

        public SessionMockBuilder Setup(Action<SessionMockBuilder> configAction)
        {
            configAction(this);
            return this;
        }

        public SessionMockBuilder UseSynchronizations()
        {
            // this will make RegisterPostCommit events fire
            this.TransactionMock.Setup(x => x.RegisterSynchronization(It.IsAny<ISynchronization>())).Callback((ISynchronization a) =>
            {
                a.AfterCompletion(true);
            });

            return this;
        }

        public Mock<ISession> CreateMock()
        {
            return this.SessionMock;
        }
    }
}
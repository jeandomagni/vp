﻿namespace Ivh.Common.Tests.Helpers.FinancePlan
{
    using System;
    using System.Data;
    using Application.Core.Common.Interfaces;
    using Application.FinanceManagement.Common.Interfaces;
    using Ivh.Application.Base.Common.Interfaces;
    using Ivh.Domain.FinanceManagement.FinancePlan.Interfaces;
    using Ivh.Domain.FinanceManagement.Interfaces;
    using Ivh.Domain.FinanceManagement.Payment.Interfaces;
    using Ivh.Domain.Guarantor.Interfaces;
    using Ivh.Domain.FinanceManagement.Statement.Interfaces;
    using Ivh.Domain.User.Services;
    using NHibernate;
    using Ivh.Application.FinanceManagement.ApplicationServices;
    using Base;
    using Data.Connection;
    using Data.Connection.Connections;
    using Domain.User.Interfaces;
    using Moq;

    public class PaymentReversalApplicationServiceMockBuilder : IMockBuilder<PaymentReversalApplicationService, PaymentReversalApplicationServiceMockBuilder>
    {
        public ApplicationServiceCommonServiceMockBuilder ApplicationServiceCommonServiceMockBuilder;
        public IFinancePlanService FinancePlanService;
        public IPaymentReversalService PaymentReversalService;
        public IPaymentProcessorResponseService PaymentProcessorResponseService;
        public IPaymentService PaymentService;
        public IGuarantorService GuarantorService;
        public IVpStatementService VpStatementService;
        public VisitPayUserService VisitPayUserService = null;
        public ISession Session;
        public IVisitPayUserJournalEventApplicationService JournalEventApplicationService;

        public Mock<IFinancePlanService> FinancePlanServiceMock;
        public Mock<IPaymentReversalService> PaymentReversalServiceMock;
        public Mock<IPaymentProcessorResponseService> PaymentProcessorResponseServiceMock;
        public Mock<IPaymentService> PaymentServiceMock;
        public Mock<IGuarantorService> GuarantorServiceMock;
        public Mock<IVpStatementService> VpStatementServiceMock;
        public Mock<ISession> SessionMock;
        public Mock<IVisitPayUserJournalEventApplicationService> JournalApplicationServiceMock;

        public PaymentReversalApplicationServiceMockBuilder()
        {
            this.ApplicationServiceCommonServiceMockBuilder = new ApplicationServiceCommonServiceMockBuilder();
            this.FinancePlanServiceMock = new Mock<IFinancePlanService>();
            this.PaymentReversalServiceMock = new Mock<IPaymentReversalService>();
            this.PaymentProcessorResponseServiceMock = new Mock<IPaymentProcessorResponseService>();
            this.PaymentServiceMock = new Mock<IPaymentService>();
            this.GuarantorServiceMock = new Mock<IGuarantorService>();
            this.VpStatementServiceMock = new Mock<IVpStatementService>();
            this.SessionMock = new SessionMockBuilder().SessionMock;
            this.JournalApplicationServiceMock = new Mock<IVisitPayUserJournalEventApplicationService>();
        }

        public PaymentReversalApplicationService CreateService()
        {
            return new PaymentReversalApplicationService(
                this.ApplicationServiceCommonServiceMockBuilder.CreateServiceLazy(),
                new Lazy<IFinancePlanService>(() => this.FinancePlanService ?? this.FinancePlanServiceMock.Object),
                new Lazy<IPaymentReversalService>(() => this.PaymentReversalService ?? this.PaymentReversalServiceMock.Object),
                new Lazy<IPaymentProcessorResponseService>(() => this.PaymentProcessorResponseService ?? this.PaymentProcessorResponseServiceMock.Object),
                new Lazy<IPaymentService>(() => this.PaymentService ?? this.PaymentServiceMock.Object),
                new Lazy<IGuarantorService>(() => this.GuarantorService ?? this.GuarantorServiceMock.Object),
                new Lazy<IVpStatementService>(() => this.VpStatementService ?? this.VpStatementServiceMock.Object),
                this.VisitPayUserService,
                new Lazy<IVisitPayUserJournalEventApplicationService>(()=> this.JournalEventApplicationService ?? this.JournalApplicationServiceMock.Object), 
                new SessionContext<VisitPay>(this.Session ?? this.SessionMock.Object)
            );
        }

        public PaymentReversalApplicationServiceMockBuilder Setup(Action<PaymentReversalApplicationServiceMockBuilder> configAction)
        {
            configAction(this);
            return this;
        }
    }
}


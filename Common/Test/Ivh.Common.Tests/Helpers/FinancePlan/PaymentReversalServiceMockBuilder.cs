﻿namespace Ivh.Common.Tests.Helpers.FinancePlan
{
    using System;
    using System.Data;
    using Ivh.Domain.Base.Interfaces;
    using Ivh.Domain.FinanceManagement.Payment.Interfaces;
    using Ivh.Domain.FinanceManagement.Interfaces;
    using Ivh.Domain.FinanceManagement.FinancePlan.Interfaces;
    using NHibernate;
    using Ivh.Domain.FinanceManagement.Services;
    using Base;
    using Data.Connection;
    using Data.Connection.Connections;
    using Moq;
    using Ivh.Domain.FinanceManagement.Ach.Interfaces;

    public class PaymentReversalServiceMockBuilder : IMockBuilder<IPaymentReversalService, PaymentReversalServiceMockBuilder>
    {
        public DomainServiceCommonServiceMockBuilder DomainServiceCommonServiceMockBuilder;
        public IPaymentRepository PaymentRepository;
        public IPaymentEventService PaymentEventService;
        public IPaymentReversalProvider PaymentReversalProvider;
        public IPaymentProcessorRepository PaymentProcessorRepository;
        public IPaymentProcessorResponseRepository PaymentProcessorResponseRepository;
        public IPaymentAllocationService PaymentAllocationService;
		public IPaymentProvider PaymentProvider;
        public IPaymentMethodsService PaymentMethodsService;
        public IFinancePlanService FinancePlanService;
        public IAchService AchService;
        public ISession Session;

        public Mock<IPaymentRepository> PaymentRepositoryMock;
        public Mock<IPaymentEventService> PaymentEventServiceMock;
        public Mock<IPaymentReversalProvider> PaymentReversalProviderMock;
        public Mock<IPaymentProcessorRepository> PaymentProcessorRepositoryMock;
        public Mock<IPaymentProcessorResponseRepository> PaymentProcessorResponseRepositoryMock;
        public Mock<IPaymentAllocationService> PaymentAllocationServiceMock;
        public Mock<IPaymentProvider> PaymentProviderMock;
        public Mock<IPaymentMethodsService> PaymentMethodsServiceMock;
        public Mock<IFinancePlanService> FinancePlanServiceMock;
        public Mock<IAchService> AchServiceMock;
        public Mock<ISession> SessionMock;

        public PaymentReversalServiceMockBuilder()
        {
            this.DomainServiceCommonServiceMockBuilder = new DomainServiceCommonServiceMockBuilder();
            this.PaymentRepositoryMock = new Mock<IPaymentRepository>();
            this.PaymentEventServiceMock = new Mock<IPaymentEventService>();
            this.PaymentReversalProviderMock = new Mock<IPaymentReversalProvider>();
            this.PaymentProcessorRepositoryMock = new Mock<IPaymentProcessorRepository>();
            this.PaymentProcessorResponseRepositoryMock = new Mock<IPaymentProcessorResponseRepository>();
            this.PaymentAllocationServiceMock = new Mock<IPaymentAllocationService>();
            this.PaymentProviderMock = new Mock<IPaymentProvider>();
            this.PaymentMethodsServiceMock = new Mock<IPaymentMethodsService>();
            this.FinancePlanServiceMock = new Mock<IFinancePlanService>();
            this.AchServiceMock = new Mock<IAchService>();
            this.SessionMock = new SessionMockBuilder().SessionMock;
        }

        public IPaymentReversalService CreateService()
        {
            return new PaymentReversalService(
                this.DomainServiceCommonServiceMockBuilder.CreateServiceLazy(),
                new Lazy<IPaymentRepository>(() => this.PaymentRepository ?? this.PaymentRepositoryMock.Object),
                new Lazy<IPaymentEventService>(() => this.PaymentEventService ?? this.PaymentEventServiceMock.Object),
                new Lazy<IPaymentReversalProvider>(() => this.PaymentReversalProvider ?? this.PaymentReversalProviderMock.Object),
                new Lazy<IPaymentProcessorRepository>(() => this.PaymentProcessorRepository ?? this.PaymentProcessorRepositoryMock.Object),
                new Lazy<IPaymentProcessorResponseRepository>(() => this.PaymentProcessorResponseRepository ?? this.PaymentProcessorResponseRepositoryMock.Object),
                new Lazy<IPaymentAllocationService>(() => this.PaymentAllocationService ?? this.PaymentAllocationServiceMock.Object),
                new Lazy<IPaymentProvider>(() => this.PaymentProvider ?? this.PaymentProviderMock.Object),
                new Lazy<IPaymentMethodsService>(() => this.PaymentMethodsService ?? this.PaymentMethodsServiceMock.Object),
                new Lazy<IFinancePlanService>(() => this.FinancePlanService ?? this.FinancePlanServiceMock.Object),
                new Lazy<IAchService>(() => this.AchService ?? this.AchServiceMock.Object),
                new SessionContext<VisitPay>(this.Session ?? this.SessionMock.Object)
            );
        }

        public PaymentReversalServiceMockBuilder Setup(Action<PaymentReversalServiceMockBuilder> configAction)
        {
            configAction(this);
            return this;
        }
    }
}

﻿namespace Ivh.Common.Tests.Helpers.FinancePlan
{
    using System;
    using Application.Content.Common.Interfaces;
    using Application.FinanceManagement.Common.Interfaces;
    using Ivh.Domain.FinanceManagement.FinancePlan.Interfaces;
    using Ivh.Domain.FinanceManagement.Statement.Interfaces;
    using Ivh.Domain.Guarantor.Interfaces;
    using NHibernate;
    using Ivh.Domain.FinanceManagement.Interfaces;
    using Ivh.Domain.User.Interfaces;
    using Ivh.Application.FinanceManagement.ApplicationServices;
    using Base;
    using Data.Connection;
    using Data.Connection.Connections;
    using Domain.Visit.Interfaces;
    using Moq;

    public class ReconfigureFinancePlanApplicationServiceMockBuilder : IMockBuilder<IReconfigureFinancePlanApplicationService, ReconfigureFinancePlanApplicationServiceMockBuilder>
    {
        public ApplicationServiceCommonServiceMockBuilder ApplicationServiceCommonServiceMockBuilder;
        public IAmortizationService AmortizationService;
        public IFinancePlanService FinancePlanService;
        public IFinancePlanOfferService FinancePlanOfferService;
        public IVpStatementService VpStatementService;
        public IGuarantorService GuarantorService;
        public ISession Session;
        public IPaymentService PaymentService;
        public IInterestService InterestService;
        public IVisitPayUserJournalEventService VisitPayUserJournalEventService;
        public IContentApplicationService ContentApplicationService;
        public ICreditAgreementApplicationService CreditAgreementApplicationService;
        public IVisitService VisitService;
        public IVisitPayUserService VisitPayUserService;

        public Mock<IAmortizationService> AmortizationServiceMock;
        public Mock<IFinancePlanService> FinancePlanServiceMock;
        public Mock<IFinancePlanOfferService> FinancePlanOfferServiceMock;
        public Mock<IVpStatementService> VpStatementServiceMock;
        public Mock<IGuarantorService> GuarantorServiceMock;
        public Mock<ISession> SessionMock;
        public Mock<IPaymentService> PaymentServiceMock;
        public Mock<IInterestService> InterestServiceMock;
        public Mock<IVisitPayUserJournalEventService> VisitPayUserJournalEventServiceMock;
        public Mock<IContentApplicationService> ContentApplicationServiceMock;
        public Mock<ICreditAgreementApplicationService> CreditAgreementApplicationServiceMock;
        public Mock<IVisitService> VisitServiceMock;
        public Mock<IVisitPayUserService> VisitPayUserServiceMock;

        public ReconfigureFinancePlanApplicationServiceMockBuilder()
        {
            this.ApplicationServiceCommonServiceMockBuilder = new ApplicationServiceCommonServiceMockBuilder();
            this.AmortizationServiceMock = new Mock<IAmortizationService>();
            this.FinancePlanServiceMock = new Mock<IFinancePlanService>();
            this.FinancePlanOfferServiceMock = new Mock<IFinancePlanOfferService>();
            this.VpStatementServiceMock = new Mock<IVpStatementService>();
            this.GuarantorServiceMock = new Mock<IGuarantorService>();
            this.SessionMock = new SessionMockBuilder().SessionMock;
            this.PaymentServiceMock = new Mock<IPaymentService>();
            this.InterestServiceMock = new Mock<IInterestService>();
            this.VisitPayUserJournalEventServiceMock = new Mock<IVisitPayUserJournalEventService>();
            this.ContentApplicationServiceMock = new Mock<IContentApplicationService>();
            this.CreditAgreementApplicationServiceMock = new Mock<ICreditAgreementApplicationService>();
            this.VisitServiceMock = new Mock<IVisitService>();
            this.VisitPayUserServiceMock = new Mock<IVisitPayUserService>();
        }

        public IReconfigureFinancePlanApplicationService CreateService()
        {
            return new ReconfigureFinancePlanApplicationService(
                this.ApplicationServiceCommonServiceMockBuilder.CreateServiceLazy(),
                new Lazy<IAmortizationService>(() => this.AmortizationService ?? this.AmortizationServiceMock.Object),
                new Lazy<IFinancePlanService>(() => this.FinancePlanService ?? this.FinancePlanServiceMock.Object),
                new Lazy<IFinancePlanOfferService>(() => this.FinancePlanOfferService ?? this.FinancePlanOfferServiceMock.Object),
                new Lazy<IVpStatementService>(() => this.VpStatementService ?? this.VpStatementServiceMock.Object),
                new Lazy<IGuarantorService>(() => this.GuarantorService ?? this.GuarantorServiceMock.Object),
                new SessionContext<VisitPay>(this.Session ?? this.SessionMock.Object),
                new Lazy<IPaymentService>(() => this.PaymentService ?? this.PaymentServiceMock.Object),
                new Lazy<IInterestService>(() => this.InterestService ?? this.InterestServiceMock.Object),
                new Lazy<IVisitPayUserJournalEventService>(() => this.VisitPayUserJournalEventService ?? this.VisitPayUserJournalEventServiceMock.Object),
                new Lazy<IContentApplicationService>(() => this.ContentApplicationService ?? this.ContentApplicationServiceMock.Object),
                new Lazy<ICreditAgreementApplicationService>(() => this.CreditAgreementApplicationService ?? this.CreditAgreementApplicationServiceMock.Object),
                new Lazy<IVisitService>(() => this.VisitService ?? this.VisitServiceMock.Object),
                new Lazy<IVisitPayUserService>(() => this.VisitPayUserService ?? this.VisitPayUserServiceMock.Object)
            );
        }

        public ReconfigureFinancePlanApplicationServiceMockBuilder Setup(Action<ReconfigureFinancePlanApplicationServiceMockBuilder> configAction)
        {
            configAction(this);
            return this;
        }
    }
}
namespace Ivh.Common.Tests.Helpers.FinancePlan
{
    using System;
    using System.Threading.Tasks;
    using Base;
    using Domain.AppIntelligence.Interfaces;
    using Domain.Base.Interfaces;
    using Domain.FinanceManagement.FinancePlan.Entities;
    using Domain.FinanceManagement.FinancePlan.Interfaces;
    using Domain.FinanceManagement.FinancePlan.Services;
    using Domain.FinanceManagement.Statement.Interfaces;
    using Domain.Logging.Interfaces;
    using Domain.Settings.Interfaces;
    using Moq;

    public class FinancePlanOfferServiceMockBuilder : IMockBuilder<FinancePlanOfferService, FinancePlanOfferServiceMockBuilder>
    {
        public IFinancePlanMinimumPaymentTierRepository MinimumPaymentTierRepository;
        public IInterestService InterestService;
        public IFinancePlanOfferRepository FinancePlanOfferRepository;
        public IFinancePlanOfferSetTypeRepository FinancePlanOfferSetTypeRepository;
        public IRandomizedTestService RandomizedTestService;
        public IFeatureService FeatureService;
        public IApplicationSettingsService ApplicationSettingsService;
        public ILoggingProvider Logger;
        public IAmortizationService AmortizationService;
        public IStatementDateService StatementDateService;

        public Mock<IFinancePlanMinimumPaymentTierRepository> MinimumPaymentTierRepositoryMock;
        public Mock<IInterestService> InterestServiceMock;
        public Mock<IFinancePlanOfferRepository> FinancePlanOfferRepositoryMock;
        public Mock<IFinancePlanOfferSetTypeRepository> FinancePlanOfferSetTypeRepositoryMock;
        public Mock<IRandomizedTestService> RandomizedTestServiceMock;
        public Mock<IFeatureService> FeatureServiceMock;
        public Mock<IApplicationSettingsService> ApplicationSettingsServiceMock;
        public Mock<ILoggingProvider> LoggerMock;
        public Mock<ILoggingService> LoggerServiceMock;
        public Mock<IAmortizationService> AmortizationServiceMock;
        public Mock<IStatementDateService> StatementDateServiceMock;
        
        public IDomainServiceCommonService DomainServiceCommonService;

        public FinancePlanOfferServiceMockBuilder()
        {
            this.MinimumPaymentTierRepositoryMock = new Mock<IFinancePlanMinimumPaymentTierRepository>();
            this.InterestServiceMock = new Mock<IInterestService>();
            this.FinancePlanOfferRepositoryMock = new Mock<IFinancePlanOfferRepository>();
            this.FinancePlanOfferSetTypeRepositoryMock = new Mock<IFinancePlanOfferSetTypeRepository>();
            this.RandomizedTestServiceMock = new Mock<IRandomizedTestService>();
            this.FeatureServiceMock = new Mock<IFeatureService>();
            this.ApplicationSettingsServiceMock = new Mock<IApplicationSettingsService>();
            this.LoggerMock = new Mock<ILoggingProvider>();
            this.LoggerServiceMock = new Mock<ILoggingService>();
            this.AmortizationServiceMock = new Mock<IAmortizationService>();
            this.StatementDateServiceMock = new Mock<IStatementDateService>();

            this.DomainServiceCommonService = DomainServiceCommonServices.CreateDomainServiceCommonService(
                applicationSettingsServiceMock: this.ApplicationSettingsServiceMock,
                featureServiceMock: this.FeatureServiceMock,
                clientServiceMock: null,
                loggingServiceMock: this.LoggerServiceMock,
                busMock: null,
                cacheMock: null,
                timeZoneHelperMock: null);
        }

        public FinancePlanOfferService CreateService()
        {
            return new FinancePlanOfferService(
                new Lazy<IDomainServiceCommonService>(() => this.DomainServiceCommonService),
                new Lazy<IFinancePlanMinimumPaymentTierRepository>(() => this.MinimumPaymentTierRepository ?? this.MinimumPaymentTierRepositoryMock.Object),
                new Lazy<IInterestService>(() => this.InterestService ?? this.InterestServiceMock.Object),
                new Lazy<IFinancePlanOfferRepository>(() => this.FinancePlanOfferRepository ?? this.FinancePlanOfferRepositoryMock.Object),
                new Lazy<IFinancePlanOfferSetTypeRepository>(() => this.FinancePlanOfferSetTypeRepository ?? this.FinancePlanOfferSetTypeRepositoryMock.Object),
                new Lazy<IRandomizedTestService>(() => this.RandomizedTestService ?? this.RandomizedTestServiceMock.Object),
                new Lazy<IAmortizationService>(() => this.AmortizationService ?? this.AmortizationServiceMock.Object),
                new Lazy<IStatementDateService>(() => this.StatementDateService ?? this.StatementDateServiceMock.Object));
        }

        public FinancePlanOfferServiceMockBuilder Setup(Action<FinancePlanOfferServiceMockBuilder> configAction)
        {
            this.FinancePlanOfferRepositoryMock.Setup(x => x.ManagedInsert(It.IsAny<FinancePlanOffer>())).Returns((FinancePlanOffer offer) => offer);

            configAction(this);
            return this;
        }
    }
}
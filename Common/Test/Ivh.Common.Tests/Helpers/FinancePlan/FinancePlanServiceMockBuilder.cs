﻿namespace Ivh.Common.Tests.Helpers.FinancePlan
{
    using System;
    using System.Collections.Generic;
    using Base;
    using Data.Connection;
    using Data.Connection.Connections;
    using Domain.Base.Interfaces;
    using Domain.FinanceManagement.FinancePlan.Entities;
    using Domain.User.Interfaces;
    using Domain.Visit.Interfaces;
    using Domain.FinanceManagement.FinancePlan.Interfaces;
    using Domain.FinanceManagement.FinancePlan.Services;
    using Domain.FinanceManagement.Statement.Interfaces;
    using Domain.Logging.Interfaces;
    using Domain.Settings.Interfaces;
    using Moq;
    using NHibernate;
    using ServiceBus.Interfaces;
    using VisitPay.Enums;

    public class FinancePlanServiceMockBuilder : IMockBuilder<IFinancePlanService, FinancePlanServiceMockBuilder>
    {
        public IAmortizationService AmortizationService;
        public IApplicationSettingsService ApplicationSettings;
        public IBus Bus;
        public IClientService ClientService;
        public IMetricsProvider MetricsProvider;
        public IFeatureService FeatureService;
        public IFinancePlanOfferService FinancePlanOfferService;
        public IFinancePlanRepository FinancePlanRepository;
        public IFinancePlanReadOnlyRepository FinancePlanReadOnlyRepository;
        public IInterestService InterestService;
        public ISession Session;
        public IVisitService VisitService;
        public IVisitStateService VisitStateService;
        public IVisitTransactionService VisitTransactionService;
        public IVisitPayUserJournalEventService VisitPayUserJournalEventService;
        public IVisitTransactionRepository VisitTransactionRepository;
        public IVpStatementService StatementService;
        public IStatementDateService StatementDateService;
        public IFacilityService FacilityService;

        private IDomainServiceCommonService _domainServiceCommonService;

        public Mock<IAmortizationService> AmortizationServiceMock;
        public Mock<IApplicationSettingsService> ApplicationSettingsMock;
        public Mock<IBus> BusMock;
        public Mock<IClientService> ClientServiceMock;
        public Mock<IMetricsProvider> MetricsProviderMock;
        public Mock<IFeatureService> FeatureServiceMock;
        public Mock<IFinancePlanOfferService> FinancePlanOfferServiceMock;
        public Mock<IFinancePlanRepository> FinancePlanRepositoryMock;
        public Mock<IFinancePlanReadOnlyRepository> FinancePlanReadOnlyRepositoryMock;
        public Mock<IInterestService> InterestServiceMock;
        public Mock<ISession> SessionMock;
        public Mock<IVisitService> VisitServiceMock;
        public Mock<IVisitStateService> VisitStateServiceMock;
        public Mock<IVisitTransactionService> VisitTransactionServiceMock;
        public Mock<IVisitPayUserJournalEventService> VisitPayUserJournalEventServiceMock;
        public Mock<ILoggingService> LoggerService;
        public Mock<IVisitTransactionRepository> VisitTransactionRepositoryMock;
        public Mock<IVpStatementService> StatementServiceMock;
        public Mock<IStatementDateService> StatementDateServiceMock;
        public Mock<IFacilityService> FacilityServiceMock;
        
        public FinancePlanServiceMockBuilder()
        {
            this.SessionMock = new SessionMockBuilder().UseSynchronizations().CreateMock();

            this.AmortizationServiceMock = new Mock<IAmortizationService>();
            this.ApplicationSettingsMock = new Mock<IApplicationSettingsService>();
            this.BusMock = new Mock<IBus>();
            this.ClientServiceMock = new Mock<IClientService>();
            this.MetricsProviderMock = new Mock<IMetricsProvider>();
            this.FeatureServiceMock = new Mock<IFeatureService>();
            this.FinancePlanOfferServiceMock = new Mock<IFinancePlanOfferService>();
            this.FinancePlanRepositoryMock = new Mock<IFinancePlanRepository>();
            this.FinancePlanReadOnlyRepositoryMock = new Mock<IFinancePlanReadOnlyRepository>();
            this.InterestServiceMock = new Mock<IInterestService>();
            this.VisitServiceMock = new Mock<IVisitService>();
            this.VisitStateServiceMock = new Mock<IVisitStateService>();
            this.VisitTransactionServiceMock = new Mock<IVisitTransactionService>();
            this.VisitPayUserJournalEventServiceMock = new Mock<IVisitPayUserJournalEventService>();
            this.LoggerService = new Mock<ILoggingService>();
            this.VisitTransactionRepositoryMock = new Mock<IVisitTransactionRepository>();
            this.StatementServiceMock = new Mock<IVpStatementService>();
            this.StatementDateServiceMock = new Mock<IStatementDateService>();
            this.FacilityServiceMock = new Mock<IFacilityService>();
        }

        public IFinancePlanService CreateService()
        {
            this._domainServiceCommonService = DomainServiceCommonServices.CreateDomainServiceCommonService(
                applicationSettingsServiceMock: this.ApplicationSettingsMock,
                featureServiceMock: this.FeatureServiceMock,
                clientServiceMock: this.ClientServiceMock,
                loggingServiceMock: this.LoggerService,
                busMock: this.BusMock,
                cacheMock: null,
                timeZoneHelperMock: null);

            return new FinancePlanService(
                new Lazy<IDomainServiceCommonService>(() => this._domainServiceCommonService),
                new Lazy<IAmortizationService>(() => this.AmortizationService ?? this.AmortizationServiceMock.Object),
                new Lazy<IFinancePlanOfferService>(() => this.FinancePlanOfferService ?? this.FinancePlanOfferServiceMock.Object),
                new Lazy<IFinancePlanRepository>(() => this.FinancePlanRepository ?? this.FinancePlanRepositoryMock.Object),
                new Lazy<IFinancePlanReadOnlyRepository>(() => this.FinancePlanReadOnlyRepository ?? this.FinancePlanReadOnlyRepositoryMock.Object),
                new Lazy<IInterestService>(() => this.InterestService ?? this.InterestServiceMock.Object),
                new Lazy<IVisitService>(() => this.VisitService ?? this.VisitServiceMock.Object),
                new Lazy<IVisitPayUserJournalEventService>(() => this.VisitPayUserJournalEventService ?? this.VisitPayUserJournalEventServiceMock.Object),
                new Lazy<IVpStatementService>(() => this.StatementService ?? this.StatementServiceMock.Object),
                new Lazy<IStatementDateService>(() => this.StatementDateService ?? this.StatementDateServiceMock.Object),
                new SessionContext<VisitPay>(this.Session ?? this.SessionMock.Object),
                new Lazy<IMetricsProvider>(() => this.MetricsProvider ?? this.MetricsProviderMock.Object),
                new Lazy<IFacilityService>(() => this.FacilityService ?? this.FacilityServiceMock.Object)
            ); 
        }
        
        public FinancePlanServiceMockBuilder Setup(Action<FinancePlanServiceMockBuilder> configAction)
        {
            configAction(this);
            return this;
        }

        public FinancePlanServiceMockBuilder WithFinancePlanStatuses()
        {
            this.FinancePlanRepositoryMock.Setup(x => x.GetFinancePlanStatuses()).Returns(new List<FinancePlanStatus>()
            {
                FinancePlanFactory.GenerateFinancePlanStatus(FinancePlanStatusEnum.GoodStanding),
                FinancePlanFactory.GenerateFinancePlanStatus(FinancePlanStatusEnum.PendingAcceptance),
                FinancePlanFactory.GenerateFinancePlanStatus(FinancePlanStatusEnum.Canceled),
                FinancePlanFactory.GenerateFinancePlanStatus(FinancePlanStatusEnum.PendingAcceptance),
                FinancePlanFactory.GenerateFinancePlanStatus(FinancePlanStatusEnum.NonPaidInFull),
                FinancePlanFactory.GenerateFinancePlanStatus(FinancePlanStatusEnum.PastDue),
                FinancePlanFactory.GenerateFinancePlanStatus(FinancePlanStatusEnum.PaidInFull),
                FinancePlanFactory.GenerateFinancePlanStatus(FinancePlanStatusEnum.TermsAccepted),
                FinancePlanFactory.GenerateFinancePlanStatus(FinancePlanStatusEnum.UncollectableActive),
                FinancePlanFactory.GenerateFinancePlanStatus(FinancePlanStatusEnum.UncollectableClosed),
            });

            return this;
        }
    }
}
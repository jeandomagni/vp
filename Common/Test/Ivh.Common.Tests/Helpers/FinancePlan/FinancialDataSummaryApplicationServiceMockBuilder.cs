namespace Ivh.Common.Tests.Helpers.FinancePlan
{
    using System;
    using Ivh.Application.Base.Common.Interfaces;
    using Ivh.Domain.Guarantor.Interfaces;
    using Ivh.Domain.Visit.Interfaces;
    using Domain.FinanceManagement.Statement.Interfaces;
    using Domain.FinanceManagement.Interfaces;
    using Ivh.Application.FinanceManagement.Common.Interfaces;
    using NHibernate;
    using Application.FinanceManagement.ApplicationServices;
    using Base;
    using Data.Connection;
    using Data.Connection.Connections;
    using Domain.FinanceManagement.FinancePlan.Interfaces;
    using Moq;

    public class FinancialDataSummaryApplicationServiceMockBuilder : IMockBuilder<IFinancialDataSummaryApplicationService, FinancialDataSummaryApplicationServiceMockBuilder>
    {
        public ApplicationServiceCommonServiceMockBuilder ApplicationServiceCommonServiceMockBuilder;
		public IGuarantorService GuarantorService;
		public IVisitService VisitService;
		public IVpStatementService VpStatementService;
		public IPaymentSubmissionApplicationService PaymentSubmissionApplicationService;
		public IStatementApplicationService StatementApplicationService;
		public IFinancePlanService FinancePlanService;
		public IPaymentService PaymentService;
		public IVisitBalanceBaseApplicationService VisitBalanceBaseApplicationService;
		public IStatementDateService StatementDateService;
		public ISession Session;
        
		public Mock<IGuarantorService> GuarantorServiceMock;
		public Mock<IVisitService> VisitServiceMock;
		public Mock<IVpStatementService> VpStatementServiceMock;
		public Mock<IPaymentSubmissionApplicationService> PaymentSubmissionApplicationServiceMock;
		public Mock<IStatementApplicationService> StatementApplicationServiceMock;
		public Mock<IFinancePlanService> FinancePlanServiceMock;
		public Mock<IPaymentService> PaymentServiceMock;
		public Mock<IVisitBalanceBaseApplicationService> VisitBalanceBaseApplicationServiceMock;
		public Mock<IStatementDateService> StatementDateServiceMock;
		public Mock<ISession> SessionMock;
        
        public FinancialDataSummaryApplicationServiceMockBuilder()
        {
            this.ApplicationServiceCommonServiceMockBuilder = new ApplicationServiceCommonServiceMockBuilder();
			this.GuarantorServiceMock = new Mock<IGuarantorService>();
			this.VisitServiceMock = new Mock<IVisitService>();
			this.VpStatementServiceMock = new Mock<IVpStatementService>();
			this.PaymentSubmissionApplicationServiceMock = new Mock<IPaymentSubmissionApplicationService>();
			this.StatementApplicationServiceMock = new Mock<IStatementApplicationService>();
			this.FinancePlanServiceMock = new Mock<IFinancePlanService>();
			this.PaymentServiceMock = new Mock<IPaymentService>();
			this.VisitBalanceBaseApplicationServiceMock = new Mock<IVisitBalanceBaseApplicationService>();
			this.StatementDateServiceMock = new Mock<IStatementDateService>();
			this.SessionMock = new SessionMockBuilder().SessionMock;
        }
        
        public IFinancialDataSummaryApplicationService CreateService()
        {
            return new FinancialDataSummaryApplicationService(
                this.ApplicationServiceCommonServiceMockBuilder.CreateServiceLazy(),
				new Lazy<IGuarantorService>(() => this.GuarantorService ?? this.GuarantorServiceMock.Object),
				new Lazy<IVisitService>(() => this.VisitService ?? this.VisitServiceMock.Object),
				new Lazy<IVpStatementService>(() => this.VpStatementService ?? this.VpStatementServiceMock.Object),
				new Lazy<IPaymentSubmissionApplicationService>(() => this.PaymentSubmissionApplicationService ?? this.PaymentSubmissionApplicationServiceMock.Object),
				new Lazy<IStatementApplicationService>(() => this.StatementApplicationService ?? this.StatementApplicationServiceMock.Object),
				new Lazy<IFinancePlanService>(() => this.FinancePlanService ?? this.FinancePlanServiceMock.Object),
				new Lazy<IPaymentService>(() => this.PaymentService ?? this.PaymentServiceMock.Object),
				new Lazy<IVisitBalanceBaseApplicationService>(() => this.VisitBalanceBaseApplicationService ?? this.VisitBalanceBaseApplicationServiceMock.Object),
				new Lazy<IStatementDateService>(() => this.StatementDateService ?? this.StatementDateServiceMock.Object),
                new SessionContext<VisitPay>(this.Session ?? this.SessionMock.Object)
            ); 
         }
        
        public FinancialDataSummaryApplicationServiceMockBuilder Setup(Action<FinancialDataSummaryApplicationServiceMockBuilder> configAction)
        {
            configAction(this);
            return this;
        }
    }
}
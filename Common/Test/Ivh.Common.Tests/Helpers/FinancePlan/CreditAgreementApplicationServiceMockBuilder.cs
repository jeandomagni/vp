﻿namespace Ivh.Common.Tests.Helpers.FinancePlan
{
    using System;
    using Application.Content.Common.Interfaces;
    using Application.FinanceManagement.ApplicationServices;
    using Application.FinanceManagement.Common.Interfaces;
    using Base;
    using Domain.FinanceManagement.FinancePlan.Interfaces;
    using Domain.Guarantor.Interfaces;
    using Domain.Visit.Interfaces;
    using Moq;

    public class CreditAgreementApplicationServiceMockBuilder : IMockBuilder<ICreditAgreementApplicationService, CreditAgreementApplicationServiceMockBuilder>
    {
        public ApplicationServiceCommonServiceMockBuilder ApplicationServiceCommonServiceMockBuilder;
        public IContentApplicationService ContentApplicationService;
        public IFinancePlanService FinancePlanService;
        public IFinancePlanOfferService FinancePlanOfferService;
        public IGuarantorService GuarantorService;
		public IVisitService VisitService;

		public Mock<IContentApplicationService> ContentApplicationServiceMock;
        public Mock<IFinancePlanService> FinancePlanServiceMock;
        public Mock<IFinancePlanOfferService> FinancePlanOfferServiceMock;
        public Mock<IGuarantorService> GuarantorServiceMock;
        public Mock<IVisitService> VisitServiceMock;


		public CreditAgreementApplicationServiceMockBuilder()
        {
            this.ApplicationServiceCommonServiceMockBuilder = new ApplicationServiceCommonServiceMockBuilder();
            this.ContentApplicationServiceMock = new Mock<IContentApplicationService>();
            this.FinancePlanServiceMock = new Mock<IFinancePlanService>();
            this.FinancePlanOfferServiceMock = new Mock<IFinancePlanOfferService>();
            this.GuarantorServiceMock = new Mock<IGuarantorService>();
            this.VisitServiceMock = new Mock<IVisitService>();
        }

        public ICreditAgreementApplicationService CreateService()
        {
            return new CreditAgreementApplicationService(
                this.ApplicationServiceCommonServiceMockBuilder.CreateServiceLazy(),
                new Lazy<IContentApplicationService>(() => this.ContentApplicationService ?? this.ContentApplicationServiceMock.Object),
                new Lazy<IFinancePlanService>(() => this.FinancePlanService ?? this.FinancePlanServiceMock.Object), 
                new Lazy<IFinancePlanOfferService>(() => this.FinancePlanOfferService ?? this.FinancePlanOfferServiceMock.Object),
                new Lazy<IGuarantorService>(() => this.GuarantorService ?? this.GuarantorServiceMock.Object),
                new Lazy<IVisitService>(() => this.VisitService ?? this.VisitServiceMock.Object)
			);
        }

        public CreditAgreementApplicationServiceMockBuilder Setup(Action<CreditAgreementApplicationServiceMockBuilder> configAction)
        {
            configAction(this);
            return this;
        }
    }
}
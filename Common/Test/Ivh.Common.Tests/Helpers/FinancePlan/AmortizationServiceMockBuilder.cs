namespace Ivh.Common.Tests.Helpers.FinancePlan
{
    using System;
    using Base;
    using Domain.Base.Interfaces;
    using Domain.Core.SystemException.Interfaces;
    using Domain.FinanceManagement.FinancePlan.Interfaces;
    using Domain.FinanceManagement.FinancePlan.Services;
    using Domain.FinanceManagement.Payment.Interfaces;
    using Domain.Logging.Interfaces;
    using Domain.Settings.Entities;
    using Domain.Settings.Interfaces;
    using Moq;

    public class AmortizationServiceMockBuilder
    {
        public IInterestService InterestService;
        public IPaymentAllocationService PaymentAllocationService;
        public ISystemExceptionService SystemExceptionService;

        public IDomainServiceCommonService DomainServiceCommonService;

        public Mock<IInterestService> InterestServiceMock;
        public Mock<IPaymentAllocationService> PaymentAllocationServiceMock;
        public Mock<ISystemExceptionService> SystemExceptionServiceMock;
        public Mock<IApplicationSettingsService> ApplicationSettingsServiceMock;
        public Mock<ILoggingService> LoggingServiceMock;
        public Mock<IClientService> ClientServiceMock;

        public AmortizationServiceMockBuilder()
        {
            this.InterestServiceMock = new Mock<IInterestService>();
            this.PaymentAllocationServiceMock = new Mock<IPaymentAllocationService>();
            this.SystemExceptionServiceMock = new Mock<ISystemExceptionService>();

            this.ClientServiceMock = new Mock<IClientService>();
            Mock<Client> clientMock = new Mock<Client>();
            clientMock.Setup(t => t.GracePeriodLength).Returns(21);
            this.ClientServiceMock.Setup(x => x.GetClient()).Returns(clientMock.Object);
        }

        public IAmortizationService CreateService()
        {
            this.DomainServiceCommonService = DomainServiceCommonServices.CreateDomainServiceCommonService(
                applicationSettingsServiceMock: this.ApplicationSettingsServiceMock,
                featureServiceMock: null,
                clientServiceMock: this.ClientServiceMock,
                loggingServiceMock: this.LoggingServiceMock,
                busMock: null,
                cacheMock: null,
                timeZoneHelperMock: null);

            return new AmortizationService(
                new Lazy<IDomainServiceCommonService>(() => this.DomainServiceCommonService),
                new Lazy<IInterestService>(() => this.InterestService ?? this.InterestServiceMock.Object),
                new Lazy<IPaymentAllocationService>(() => this.PaymentAllocationService ?? this.PaymentAllocationServiceMock.Object),
                new Lazy<ISystemExceptionService>(() => this.SystemExceptionService ?? this.SystemExceptionServiceMock.Object));
        }

        public AmortizationServiceMockBuilder Setup(Action<AmortizationServiceMockBuilder> configAction)
        {
            configAction(this);
            return this;
        }
    }
}
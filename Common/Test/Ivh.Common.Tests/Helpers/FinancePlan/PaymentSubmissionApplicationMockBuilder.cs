﻿namespace Ivh.Common.Tests.Helpers.FinancePlan
{
    using System;
    using Application.Base.Common.Interfaces;
    using Application.Core.Common.Interfaces;
    using Ivh.Domain.FinanceManagement.Interfaces;
    using Ivh.Domain.Guarantor.Interfaces;
    using Ivh.Domain.FinanceManagement.Statement.Interfaces;
    using Ivh.Domain.FinanceManagement.FinancePlan.Interfaces;
    using Ivh.Application.FinanceManagement.Common.Interfaces;
    using Ivh.Application.FinanceManagement.ApplicationServices;
    using Base;
    using Data.Connection;
    using Data.Connection.Connections;
    using Data.Interfaces;
    using Domain.FinanceManagement.Discount.Interfaces;
    using Domain.Logging.Interfaces;
    using Domain.Visit.Interfaces;
    using Moq;
    using Ivh.Application.Content.Common.Interfaces;
    using NHibernate;

    public class PaymentSubmissionApplicationServiceMockBuilder : IMockBuilder<PaymentSubmissionApplicationService, PaymentSubmissionApplicationServiceMockBuilder>
    {
        public ApplicationServiceCommonServiceMockBuilder ApplicationServiceCommonServiceMockBuilder;
        public IPaymentService PaymentService;
        public IGuarantorService GuarantorService;
        public IPaymentMethodsService PaymentMethodsService;
        public IPaymentMethodsApplicationService PaymentMethodsApplicationService;
        public IVpStatementService VpStatementService;
        public IStatementApplicationService StatementApplicationService;
        public IFinancePlanService FinancePlanService;
        public IVisitService VisitService;
        public IDiscountService DiscountService;
        public IConsolidationGuarantorApplicationService ConsolidationGuarantorApplicationService;
        public IVisitBalanceBaseApplicationService VisitBalanceBaseApplicationService;
        public IContentApplicationService ContentApplicationService;
        public IPaymentAllocationApplicationService PaymentAllocationApplicationService;
        public ISession Session;
        public IMetricsProvider MetricsProvider;

        public Mock<IPaymentService> PaymentServiceMock;
        public Mock<IGuarantorService> GuarantorServiceMock;
        public Mock<IPaymentMethodsService> PaymentMethodsServiceMock;
        public Mock<IPaymentMethodsApplicationService> PaymentMethodsApplicationServiceMock;
        public Mock<IVpStatementService> VpStatementServiceMock;
        public Mock<IStatementApplicationService> StatementApplicationServiceMock;
        public Mock<IFinancePlanService> FinancePlanServiceMock;
        public Mock<IVisitService> VisitServiceMock;
        public Mock<IDiscountService> DiscountServiceMock;
        public Mock<IConsolidationGuarantorApplicationService> ConsolidationGuarantorApplicationServiceMock;
        public Mock<IVisitBalanceBaseApplicationService> VisitBalanceBaseApplicationServiceMock;
        public Mock<IContentApplicationService> ContentApplicationServiceMock;
        public Mock<IPaymentAllocationApplicationService> PaymentAllocationApplicationServiceMock;
        public Mock<ISession> SessionMock;
        public Mock<IMetricsProvider> MetricsProviderMock;

        public PaymentSubmissionApplicationServiceMockBuilder()
        {
            this.ApplicationServiceCommonServiceMockBuilder = new ApplicationServiceCommonServiceMockBuilder();
            this.PaymentServiceMock = new Mock<IPaymentService>();
            this.GuarantorServiceMock = new Mock<IGuarantorService>();
            this.PaymentMethodsServiceMock = new Mock<IPaymentMethodsService>();
            this.PaymentMethodsApplicationServiceMock = new Mock<IPaymentMethodsApplicationService>();
            this.VpStatementServiceMock = new Mock<IVpStatementService>();
            this.StatementApplicationServiceMock = new Mock<IStatementApplicationService>();
            this.FinancePlanServiceMock = new Mock<IFinancePlanService>();
            this.VisitServiceMock = new Mock<IVisitService>();
            this.DiscountServiceMock = new Mock<IDiscountService>();
            this.ConsolidationGuarantorApplicationServiceMock = new Mock<IConsolidationGuarantorApplicationService>();
            this.VisitBalanceBaseApplicationServiceMock = new Mock<IVisitBalanceBaseApplicationService>();
            this.ContentApplicationServiceMock = new Mock<IContentApplicationService>();
            this.PaymentAllocationApplicationServiceMock = new Mock<IPaymentAllocationApplicationService>();
            this.SessionMock = new Mock<ISession>();
            this.MetricsProviderMock = new Mock<IMetricsProvider>();
        }

        public PaymentSubmissionApplicationService CreateService()
        {
            return new PaymentSubmissionApplicationService(
                this.ApplicationServiceCommonServiceMockBuilder.CreateServiceLazy(),
                new Lazy<IPaymentService>(() => this.PaymentService ?? this.PaymentServiceMock.Object),
                new Lazy<IGuarantorService>(() => this.GuarantorService ?? this.GuarantorServiceMock.Object),
                new Lazy<IPaymentMethodsService>(() => this.PaymentMethodsService ?? this.PaymentMethodsServiceMock.Object),
                new Lazy<IPaymentMethodsApplicationService>(() => this.PaymentMethodsApplicationService ?? this.PaymentMethodsApplicationServiceMock.Object),
                new Lazy<IPaymentAllocationApplicationService>(() => this.PaymentAllocationApplicationService ?? this.PaymentAllocationApplicationServiceMock.Object),
                new Lazy<IVpStatementService>(() => this.VpStatementService ?? this.VpStatementServiceMock.Object),
                new Lazy<IFinancePlanService>(() => this.FinancePlanService ?? this.FinancePlanServiceMock.Object),
                new Lazy<IStatementApplicationService>(() => this.StatementApplicationService ?? this.StatementApplicationServiceMock.Object),
                new Lazy<IVisitService>(() => this.VisitService ?? this.VisitServiceMock.Object),
                new Lazy<IDiscountService>(() => this.DiscountService ?? this.DiscountServiceMock.Object), 
                new Lazy<IConsolidationGuarantorApplicationService>(() => this.ConsolidationGuarantorApplicationService ?? this.ConsolidationGuarantorApplicationServiceMock.Object),
                new Lazy<IVisitBalanceBaseApplicationService>(() => this.VisitBalanceBaseApplicationService ?? this.VisitBalanceBaseApplicationServiceMock.Object),
                new Lazy<IContentApplicationService>(() => this.ContentApplicationService ?? this.ContentApplicationServiceMock.Object),
                new SessionContext<VisitPay>(this.Session ?? this.SessionMock.Object),
                new Lazy<IMetricsProvider>(() => this.MetricsProvider ?? this.MetricsProviderMock.Object)
            );
        }

        public PaymentSubmissionApplicationServiceMockBuilder Setup(Action<PaymentSubmissionApplicationServiceMockBuilder> configAction)
        {
            configAction(this);
            return this;
        }
    }
}
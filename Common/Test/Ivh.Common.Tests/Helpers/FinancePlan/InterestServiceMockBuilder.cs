namespace Ivh.Common.Tests.Helpers.FinancePlan
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Base;
    using Domain.AppIntelligence.Interfaces;
    using Domain.Base.Interfaces;
    using Domain.FinanceManagement.FinancePlan.Entities;
    using Domain.FinanceManagement.FinancePlan.Interfaces;
    using Domain.FinanceManagement.FinancePlan.Services;
    using Domain.Logging.Interfaces;
    using Domain.Settings.Entities;
    using Domain.Settings.Interfaces;
    using Moq;

    public class InterestServiceMockBuilder
    {
        public IFinancePlanOfferSetTypeRepository FinancePlanOfferSetTypeRepository;
        public IInterestRateRepository InterestRateRepository;
        public IInterestRateConsiderationService InterestRateConsiderationService;
        public IApplicationSettingsService ApplicationSettingsService;
        public IRandomizedTestService RandomizedTestService;
        public ILoggingService LoggingService;

        public IDomainServiceCommonService DomainServiceCommonService;

        public Mock<IFinancePlanOfferSetTypeRepository> FinancePlanOfferSetTypeRepositoryMock;
        public Mock<IInterestRateRepository> InterestRateRepositoryMock;
        public Mock<IInterestRateConsiderationService> InterestRateConsiderationServiceMock;
        public Mock<IApplicationSettingsService> ApplicationSettingsServiceMock;
        public Mock<IRandomizedTestService> RandomizedTestServiceMock;
        public Mock<ILoggingService> LoggingServiceMock;

        public InterestServiceMockBuilder()
        {
            this.FinancePlanOfferSetTypeRepositoryMock = new Mock<IFinancePlanOfferSetTypeRepository>();
            this.InterestRateRepositoryMock = new Mock<IInterestRateRepository>();
            this.InterestRateConsiderationServiceMock = new Mock<IInterestRateConsiderationService>();
            this.ApplicationSettingsServiceMock = new Mock<IApplicationSettingsService>();
            this.RandomizedTestServiceMock = new Mock<IRandomizedTestService>();
            this.LoggingServiceMock = new Mock<ILoggingService>();
        }

        public IInterestService CreateService()
        {
            this.DomainServiceCommonService = DomainServiceCommonServices.CreateDomainServiceCommonService(
                applicationSettingsServiceMock: this.ApplicationSettingsServiceMock,
                featureServiceMock: null,
                clientServiceMock: null,
                loggingServiceMock: this.LoggingServiceMock,
                busMock: null,
                cacheMock: null,
                timeZoneHelperMock: null);

            return new InterestService(
                new Lazy<IDomainServiceCommonService>(() => this.DomainServiceCommonService),
                new Lazy<IFinancePlanOfferSetTypeRepository>(() => this.FinancePlanOfferSetTypeRepository ?? this.FinancePlanOfferSetTypeRepositoryMock.Object),
                new Lazy<IInterestRateRepository>(() => this.InterestRateRepository ?? this.InterestRateRepositoryMock.Object),
                new Lazy<IInterestRateConsiderationService>(() => this.InterestRateConsiderationService ?? this.InterestRateConsiderationServiceMock.Object),
                new Lazy<IRandomizedTestService>(() => this.RandomizedTestService ?? this.RandomizedTestServiceMock.Object));
        }

        public InterestServiceMockBuilder Setup(Action<InterestServiceMockBuilder> configAction)
        {
            configAction(this);
            return this;
        }

        public InterestServiceMockBuilder SetupWithStLukesRates(Action<InterestServiceMockBuilder> configAction)
        {
            FinancePlanOfferSetType financePlanOfferSetType = new FinancePlanOfferSetType
            {
                FinancePlanOfferSetTypeId = 1,
                IsActive = true,
                IsPatient = true,
                IsClient = true,
                IsDefault = true
            };

            List<InterestRate> interestRates = new List<InterestRate>
            {
                new InterestRate
                {
                    InterestRateId = 0,
                    DurationRangeStart = 0,
                    DurationRangeEnd = 36,
                    Rate = 0,
                    IsCharity = true,
                    FinancePlanOfferSetType = financePlanOfferSetType,
                },
                new InterestRate
                {
                    InterestRateId = 1,
                    DurationRangeStart = 1,
                    DurationRangeEnd = 12,
                    Rate = 0,
                    IsCharity = false,
                    FinancePlanOfferSetType = financePlanOfferSetType,
                },
                new InterestRate
                {
                    InterestRateId = 1,
                    DurationRangeStart = 13,
                    DurationRangeEnd = 24,
                    Rate = 0.05m,
                    IsCharity = false,
                    FinancePlanOfferSetType = financePlanOfferSetType,
                },
                new InterestRate
                {
                    InterestRateId = 1,
                    DurationRangeStart = 25,
                    DurationRangeEnd = 36,
                    Rate = 0.08m,
                    IsCharity = false,
                    FinancePlanOfferSetType = financePlanOfferSetType,
                }
            };

            this.ApplicationSettingsServiceMock.Setup(x => x.EnableFinancialAssistanceInterestRates).Returns(() => new ApplicationSetting<bool>(() => new Lazy<bool>(() => false)));
            this.InterestRateConsiderationServiceMock.Setup(x => x.GetTotalConsiderationAmount(It.IsAny<decimal>(), It.IsAny<IList<FinancePlan>>())).Returns((decimal totalAmount, IList<FinancePlan> allApplicableFinancePlans) => totalAmount);
            this.FinancePlanOfferSetTypeRepositoryMock.Setup(x => x.GetQueryable()).Returns(() => new List<FinancePlanOfferSetType> { financePlanOfferSetType }.AsQueryable());
            this.FinancePlanOfferSetTypeRepositoryMock.Setup(x => x.GetById(It.IsAny<int>())).Returns((int id) => new List<FinancePlanOfferSetType> {financePlanOfferSetType}.FirstOrDefault(x => x.FinancePlanOfferSetTypeId == id));
            this.InterestRateRepositoryMock.Setup(x => x.GetQueryable()).Returns(() => interestRates.AsQueryable());

            configAction(this);
            return this;
        }
    }
}
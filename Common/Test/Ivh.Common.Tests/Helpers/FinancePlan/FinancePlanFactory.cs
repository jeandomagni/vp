namespace Ivh.Common.Tests.Helpers.FinancePlan
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Common.Base.Enums;
    using Domain.Visit.Entities;
    using Domain.FinanceManagement.FinancePlan.Entities;
    using Domain.FinanceManagement.FinancePlan.Interfaces;
    using Domain.FinanceManagement.Statement.Entities;
    using Domain.Guarantor.Entities;
    using Guarantor;
    using VisitPay.Enums;

    public class FinancePlanFactory
    {
        private static int _primaryKeyCounter = 1;
        private static int _financePlanVisitKeyCounter = 1;

        public static FinancePlanStatus GenerateFinancePlanStatus(FinancePlanStatusEnum statusEnum)
        {
            return new FinancePlanStatus { FinancePlanStatusId = (int)statusEnum };
        }

        public static FinancePlan GenerateWithStatus(IList<Tuple<Visit, IList<VisitTransaction>>> visitsAndTransactions, FinancePlanOffer offer, DateTime originationDate, VpStatement statement, decimal monthlyPaymentAmount, FinancePlanStatusEnum statusEnum, DateTime? insertDate = null)
        {
            //Needs Offer
            //Needs FPVisits
            //-These Visits need transactions so that we can calculate the total amount
            //-These Visits should be active unless we want the FP to cancel
            //-These Visits should have a Guarantor
            //Need status of pending or terms accepted
            //Needs payment amount
            //Needs Statement with PaymentDueDate

            FinancePlan newFp = new FinancePlan
            {
                FinancePlanId = _primaryKeyCounter,
                FinancePlanOffer = offer,
                InsertDate = insertDate ?? originationDate.AddDays(-5),
                VpGuarantor = visitsAndTransactions.Select(x => x.Item1).FirstOrDefault().VPGuarantor ?? GuarantorFactory.GenerateOnline(),
                OriginationDate = originationDate,
                CreatedVpStatement = statement,
                PaymentAmount = monthlyPaymentAmount,
            };
            _primaryKeyCounter++;
            newFp.SetInterestRate(offer.InterestRate, "");
            newFp.SetFinancePlanStatus(new FinancePlanStatus { FinancePlanStatusId = (int)statusEnum }, "");
            //Make sure this first status is the same as the origination date.
            FinancePlanStatusHistory firstStatus = newFp.FinancePlanStatusHistory.FirstOrDefault(x => x.FinancePlanStatus.FinancePlanStatusEnum == statusEnum);
            if (firstStatus != null && firstStatus.FinancePlanStatus.IsActiveOriginated())
            {
                firstStatus.InsertDate = originationDate;
            }
            else if(firstStatus != null)
            {
                firstStatus.InsertDate = DateTime.UtcNow.AddMilliseconds(-100);
            }

            foreach (Tuple<Visit, IList<VisitTransaction>> visitAndTransactions in visitsAndTransactions)
            {
                FinancePlanVisit fpVisit = VisitToFinancePlanVisit(visitAndTransactions, newFp);
                fpVisit.InsertDate = visitAndTransactions.Item1.InsertDate;
                fpVisit.FinancePlanVisitId = _financePlanVisitKeyCounter;
                _financePlanVisitKeyCounter++;
            }
            newFp.OriginatedFinancePlanBalance = newFp.CurrentFinancePlanBalance;


            return newFp;
        }

        public static FinancePlanVisit GenerateFinancePlanVisit(Visit visit, FinancePlan financePlan)
        {
            if (visit == null || financePlan == null || visit.VPGuarantor == null)
            {
                throw new Exception("Visit.VPGuarantor, Visit, and FinancePlan must not be null");
            }

            FinancePlanVisit fpVisit = new FinancePlanVisit
            {
                FinancePlan = financePlan,
                FinancePlanVisitId = _financePlanVisitKeyCounter,
                InsertDate = visit.InsertDate,
                VpGuarantorId = visit.VPGuarantor.VpGuarantorId,
            };

            fpVisit.FinancePlanVisitInterestRateHistories = new List<FinancePlanVisitInterestRateHistory>
            {
                new FinancePlanVisitInterestRateHistory
                {
                    FinancePlanVisit = fpVisit,
                    InsertDate = DateTime.UtcNow.AddSeconds(-1),
                    InterestRate = financePlan.CurrentInterestRate,
                    OverrideInterestRate = null,
                    OverridesRemaining = 0
                }
            };

            _financePlanVisitKeyCounter++;

            financePlan.FinancePlanVisits.Add(fpVisit);

            return fpVisit;
        }

        public static void AddVisitsToFinancePlan(IList<Visit> visitsToUse, FinancePlan currentFinancePlan)
        {
            //Add these to the FP as FPVisits
            foreach (Visit visit in visitsToUse)
            {
                FinancePlanFactory.GenerateFinancePlanVisit(visit, currentFinancePlan);
            }
        }

        public static void SetFinancePlanIntoStatusEnum(FinancePlan financePlan, FinancePlanStatusEnum statusEnum)
        {
            financePlan.FinancePlanStatus = GenerateFinancePlanStatus(statusEnum);
        }
        
        public static FinancePlan CreateFinancePlan(decimal paymentAmount, DateTime? originationDate = null)
        {
            return CreateFinancePlan(new List<Tuple<Visit, IList<VisitTransaction>>>(), 0, paymentAmount, originationDate);
        }

        public static FinancePlan CreateFinancePlan(IList<Tuple<Visit, IList<VisitTransaction>>> visitsAndTransactions, int numberOfBuckets, decimal paymentAmount, DateTime? originationDate = null, decimal? interestRate = null)
        {
            List<Visit> visits = visitsAndTransactions.Select(x => x.Item1).ToList();
            return CreateFinancePlan(visits, numberOfBuckets, paymentAmount, originationDate, interestRate);
        }

        public static FinancePlan CreateFinancePlan(List<Visit> visits, int numberOfBuckets, decimal paymentAmount, DateTime? originationDate = null, decimal? interestRate = null)
        {
            Guarantor guarantor = visits.Where(g => g.VPGuarantor != null).Select(y => y.VPGuarantor).FirstOrDefault();
            FinancePlan financePlan = new FinancePlan
            {
                FinancePlanId = _primaryKeyCounter++,
                InsertDate = DateTime.UtcNow,
                OriginationDate = originationDate ?? DateTime.UtcNow,
                VpGuarantor = guarantor ?? GuarantorFactory.GenerateOnline(),
                CreatedVpStatement = new VpStatement()
            };

            if (interestRate != null)
            {
                financePlan.SetInterestRate(interestRate.Value, string.Empty);
            }

            foreach (Visit visit in visits)
            {
                VisitToFinancePlanVisit(visit, financePlan);
            }

            financePlan.PaymentAmount = paymentAmount;

            SetNumberOfBuckets(financePlan, numberOfBuckets);

            return financePlan;
        }

        public static void SetNumberOfBuckets(FinancePlan financePlan, int numberOfBuckets)
        {
            int statementId = new Random().Next(1000, 10000000);

            //So: 1 bucket => 0
            //So: 2 bucket => 1, 0
            for (int i = numberOfBuckets - 1; i >= 0; i--)
            {
                financePlan.AddFinancePlanAmountsDueStatement(financePlan.PaymentAmount, DateTime.UtcNow.AddMonths(-i), statementId++);
            }

            foreach (FinancePlanStatusHistory fpsh in financePlan.FinancePlanStatusHistory)
            {
                // adjust previous histories b/c we don't have the id to rely on for ordering
                // unit tests are fast enough that two statuses can insert at the same time it seems
                // this will just make sure the last one in is the most current
                fpsh.InsertDate = financePlan.OriginationDate?.AddSeconds(-1) ?? fpsh.InsertDate.AddSeconds(-1);
            }

            if (financePlan.Buckets.Count > 1)
            {
                financePlan.FinancePlanStatus = new FinancePlanStatus { FinancePlanStatusId = (int)FinancePlanStatusEnum.PastDue };
            }
            else
            {
                financePlan.FinancePlanStatus = new FinancePlanStatus { FinancePlanStatusId = (int)FinancePlanStatusEnum.GoodStanding };
            }

            //Set this first originated status to have an insert date at the time of origination.
            FinancePlanStatusHistory history = financePlan.FinancePlanStatusHistory.OrderByDescending(x => x.FinancePlanStatusHistoryId).FirstOrDefault();
            if (history != null && financePlan.OriginationDate.HasValue)
            {
                history.InsertDate = financePlan.OriginationDate.Value;
            }
        }

        public static FinancePlan CreateFinancePlanWithDuration(int durationRangeStart, int durationRangeEnd)
        {
            return new FinancePlan
            {
                FinancePlanOffer = new FinancePlanOffer
                {
                    DurationRangeStart = durationRangeStart,
                    DurationRangeEnd = durationRangeEnd
                }
            };
        }

        public static FinancePlanVisit VisitToFinancePlanVisit(Visit visit, FinancePlan financePlan)
        {
            _financePlanVisitKeyCounter = _financePlanVisitKeyCounter + 1;

            FinancePlanCalculationParameters financePlanCalculationParameters = new FinancePlanCalculationParameters(financePlan);

            IFinancePlanVisitBalance found = financePlanCalculationParameters.Visits.FirstOrDefault(x => x.VisitId == visit.VisitId);
            if (found == null)
            {
                financePlanCalculationParameters.Visits.Add(new FinancePlanSetupVisit(visit.VisitId,
                    visit.SourceSystemKey,
                    visit.BillingSystemId ?? default(int),
                    visit.CurrentBalanceAsOf(DateTime.MaxValue), // for tests set in the future
                    0,//uncleared payment sum
                    0,//InterestDue
                    visit.CurrentVisitState,
                    visit.BillingApplication,
                    visit.InterestZero,
                    null,//Override interest rate
                    1));
            }

            DateTime nowMinus1 = (financePlan.OriginationDate ?? DateTime.UtcNow).AddSeconds(-1);
            financePlan.AddFinancePlanVisits(financePlanCalculationParameters, null);
            foreach (FinancePlanVisit financePlanVisit in financePlan.FinancePlanVisits)
            {
                //Backdate stuffs
                foreach (FinancePlanVisitPrincipalAmount principalAmount in financePlanVisit.FinancePlanVisitPrincipalAmounts)
                {
                    principalAmount.InsertDate = nowMinus1;
                }
                foreach (FinancePlanAmountDue financePlanAmountDue in financePlanVisit.FinancePlanAmountDues)
                {
                    financePlanAmountDue.InsertDate = nowMinus1;
                }
                foreach (FinancePlanVisitInterestDue due in financePlanVisit.FinancePlanVisitInterestDues)
                {
                    due.InsertDate = nowMinus1;
                }
                foreach (FinancePlanVisitInterestRateHistory interestRateHistory in financePlanVisit.FinancePlanVisitInterestRateHistories)
                {
                    interestRateHistory.InsertDate = nowMinus1;
                }
            }

            FinancePlanVisit visitToFinancePlanVisit = financePlan.FinancePlanVisits.FirstOrDefault(x => x.VisitId == visit.VisitId);
            visitToFinancePlanVisit.FinancePlanVisitId = _financePlanVisitKeyCounter;
            visitToFinancePlanVisit.VisitId = visit.VisitId;
            return visitToFinancePlanVisit;
        }

        public static FinancePlanVisit VisitToFinancePlanVisit(Tuple<Visit, IList<VisitTransaction>> visitAndTransactions, FinancePlan financePlan)
        {
            return VisitToFinancePlanVisit(visitAndTransactions.Item1, financePlan);
        }

        public static FinancePlanVisit VisitToFinancePlanVisit(Tuple<Visit, IList<VisitTransaction>> visitAndTransactions, FinancePlan financePlan, decimal interestDue)
        {
            FinancePlanVisit fpVisit = VisitToFinancePlanVisit(visitAndTransactions, financePlan);
            fpVisit.FinancePlanVisitInterestDues.Add(new FinancePlanVisitInterestDue
            {
                FinancePlan = fpVisit.FinancePlan,
                FinancePlanVisit = fpVisit,
                InsertDate = DateTime.UtcNow,
                InterestDue = interestDue,
                VpStatementId = 0
            });

            return fpVisit;
        }
    }
}
namespace Ivh.Common.Tests.Helpers.FinancePlan
{
    using System;
    using System.Data;
    using Ivh.Application.Base.Common.Interfaces;
    using Ivh.Application.Content.Common.Interfaces;
    using Ivh.Domain.FinanceManagement.FinancePlan.Interfaces;
    using Ivh.Domain.FinanceManagement.Interfaces;
    using Ivh.Domain.Guarantor.Interfaces;
    using Ivh.Application.FinanceManagement.Common.Interfaces;
    using Ivh.Domain.FinanceManagement.Statement.Interfaces;
    using NHibernate;
    using Ivh.Domain.Visit.Interfaces;
    using Ivh.Domain.User.Interfaces;
    using Ivh.Domain.Content.Interfaces;
    using Ivh.Domain.Logging.Interfaces;
    using Ivh.Domain.FinanceManagement.RoboRefund.Interfaces;
    using Ivh.Application.FinanceManagement.ApplicationServices;
    using Base;
    using Data.Connection;
    using Data.Connection.Connections;
    using Moq;
    using Ivh.Domain.AppIntelligence.Interfaces;

    public class FinancePlanApplicationServiceMockBuilder : IMockBuilder<IFinancePlanApplicationService, FinancePlanApplicationServiceMockBuilder>
    {
        public ApplicationServiceCommonServiceMockBuilder ApplicationServiceCommonServiceMockBuilder;
        public IContentApplicationService ContentApplicationService;
        public IAmortizationService AmortizationService;
        public IBalanceTransferService BalanceTransferService;
        public IFinancePlanService FinancePlanService;
        public IFinancePlanOfferService FinancePlanOfferService;
        public IGuarantorService GuarantorService;
        public IPaymentService PaymentService;
        public IPaymentReversalApplicationService PaymentReversalApplicationService;
        public IVpStatementService VpStatementService;
        public IStatementDateService StatementDateService;
        public IVisitBalanceBaseApplicationService VisitBalanceBaseApplicationService;
        public ISession Session;
        public IVisitService VisitService;
        public IInterestService InterestService;
        public IVisitPayUserJournalEventService VisitPayUserJournalEventService;
        public IImageService ImageService;
        public IMetricsProvider MetricsProvider;
        public ICreditAgreementApplicationService CreditAgreementApplicationService;
        public IRoboRefundService RoboRefundService;
        public IRandomizedTestService RandomizedTestService;
        public IGuarantorScoreService ScoringService;
        public IFinancePlanIncentiveService FinancePlanIncentiveService;

        public Mock<IContentApplicationService> ContentApplicationServiceMock;
        public Mock<IAmortizationService> AmortizationServiceMock;
        public Mock<IBalanceTransferService> BalanceTransferServiceMock;
        public Mock<IFinancePlanService> FinancePlanServiceMock;
        public Mock<IFinancePlanOfferService> FinancePlanOfferServiceMock;
        public Mock<IGuarantorService> GuarantorServiceMock;
        public Mock<IPaymentService> PaymentServiceMock;
        public Mock<IPaymentReversalApplicationService> PaymentReversalApplicationServiceMock;
        public Mock<IVpStatementService> VpStatementServiceMock;
        public Mock<IStatementDateService> StatementDateServiceMock;
        public Mock<IVisitBalanceBaseApplicationService> VisitBalanceBaseApplicationServiceMock;
        public Mock<ISession> SessionMock;
        public Mock<IVisitService> VisitServiceMock;
        public Mock<IInterestService> InterestServiceMock;
        public Mock<IVisitPayUserJournalEventService> VisitPayUserJournalEventServiceMock;
        public Mock<IImageService> ImageServiceMock;
        public Mock<IMetricsProvider> MetricsProviderMock;
        public Mock<ICreditAgreementApplicationService> CreditAgreementApplicationServiceMock;
        public Mock<IRoboRefundService> RoboRefundServiceMock;
        public Mock<IRandomizedTestService> RandomizedTestServiceMock;
        public Mock<IGuarantorScoreService> ScoringServiceMock;
        public Mock<IFinancePlanIncentiveService> FinancePlanIncentiveServiceMock;

        public FinancePlanApplicationServiceMockBuilder()
        {
            this.ApplicationServiceCommonServiceMockBuilder = new ApplicationServiceCommonServiceMockBuilder();
            this.ContentApplicationServiceMock = new Mock<IContentApplicationService>();
            this.AmortizationServiceMock = new Mock<IAmortizationService>();
            this.BalanceTransferServiceMock = new Mock<IBalanceTransferService>();
            this.FinancePlanServiceMock = new Mock<IFinancePlanService>();
            this.FinancePlanOfferServiceMock = new Mock<IFinancePlanOfferService>();
            this.GuarantorServiceMock = new Mock<IGuarantorService>();
            this.PaymentServiceMock = new Mock<IPaymentService>();
            this.PaymentReversalApplicationServiceMock = new Mock<IPaymentReversalApplicationService>();
            this.VpStatementServiceMock = new Mock<IVpStatementService>();
            this.StatementDateServiceMock = new Mock<IStatementDateService>();
            this.VisitBalanceBaseApplicationServiceMock = new Mock<IVisitBalanceBaseApplicationService>();
            this.SessionMock = new SessionMockBuilder().SessionMock;
            this.VisitServiceMock = new Mock<IVisitService>();
            this.InterestServiceMock = new Mock<IInterestService>();
            this.VisitPayUserJournalEventServiceMock = new Mock<IVisitPayUserJournalEventService>();
            this.ImageServiceMock = new Mock<IImageService>();
            this.MetricsProviderMock = new Mock<IMetricsProvider>();
            this.CreditAgreementApplicationServiceMock = new Mock<ICreditAgreementApplicationService>();
            this.RoboRefundServiceMock = new Mock<IRoboRefundService>();
            this.RandomizedTestServiceMock = new Mock<IRandomizedTestService>();
            this.ScoringServiceMock = new Mock<IGuarantorScoreService>();
            this.FinancePlanIncentiveServiceMock = new Mock<IFinancePlanIncentiveService>();
        }

        public IFinancePlanApplicationService CreateService()
        {
            return new FinancePlanApplicationService(
                this.ApplicationServiceCommonServiceMockBuilder.CreateServiceLazy(),
                new Lazy<IContentApplicationService>(() => this.ContentApplicationService ?? this.ContentApplicationServiceMock.Object),
                new Lazy<IAmortizationService>(() => this.AmortizationService ?? this.AmortizationServiceMock.Object),
                new Lazy<IBalanceTransferService>(() => this.BalanceTransferService ?? this.BalanceTransferServiceMock.Object),
                new Lazy<IFinancePlanService>(() => this.FinancePlanService ?? this.FinancePlanServiceMock.Object),
                new Lazy<IFinancePlanOfferService>(() => this.FinancePlanOfferService ?? this.FinancePlanOfferServiceMock.Object),
                new Lazy<IGuarantorService>(() => this.GuarantorService ?? this.GuarantorServiceMock.Object),
                new Lazy<IPaymentService>(() => this.PaymentService ?? this.PaymentServiceMock.Object),
                new Lazy<IPaymentReversalApplicationService>(() => this.PaymentReversalApplicationService ?? this.PaymentReversalApplicationServiceMock.Object),
                new Lazy<IVpStatementService>(() => this.VpStatementService ?? this.VpStatementServiceMock.Object),
                new Lazy<IStatementDateService>(() => this.StatementDateService ?? this.StatementDateServiceMock.Object),
                new Lazy<IVisitBalanceBaseApplicationService>(() => this.VisitBalanceBaseApplicationService ?? this.VisitBalanceBaseApplicationServiceMock.Object),
                new SessionContext<VisitPay>(this.Session ?? this.SessionMock.Object),
                new Lazy<IVisitService>(() => this.VisitService ?? this.VisitServiceMock.Object),
                new Lazy<IInterestService>(() => this.InterestService ?? this.InterestServiceMock.Object),
                new Lazy<IVisitPayUserJournalEventService>(() => this.VisitPayUserJournalEventService ?? this.VisitPayUserJournalEventServiceMock.Object),
                new Lazy<IImageService>(() => this.ImageService ?? this.ImageServiceMock.Object),
                new Lazy<IMetricsProvider>(() => this.MetricsProvider ?? this.MetricsProviderMock.Object),
                new Lazy<ICreditAgreementApplicationService>(() => this.CreditAgreementApplicationService ?? this.CreditAgreementApplicationServiceMock.Object),
                new Lazy<IRoboRefundService>(() => this.RoboRefundService ?? this.RoboRefundServiceMock.Object),
                new Lazy<IRandomizedTestService>(() => this.RandomizedTestService ?? this.RandomizedTestServiceMock.Object),
                new Lazy<IGuarantorScoreService>(() => this.ScoringService ?? this.ScoringServiceMock.Object),
                new Lazy<IFinancePlanIncentiveService>(() => this.FinancePlanIncentiveService ?? this.FinancePlanIncentiveServiceMock.Object)
            );
        }

        public FinancePlanApplicationServiceMockBuilder Setup(Action<FinancePlanApplicationServiceMockBuilder> configAction)
        {
            configAction(this);
            return this;
        }
    }
}
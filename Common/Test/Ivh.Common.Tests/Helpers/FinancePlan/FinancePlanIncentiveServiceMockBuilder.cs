﻿namespace Ivh.Common.Tests.Helpers.FinancePlan
{
    using System;
    using Application.Settings.Common.Dtos;
    using Ivh.Domain.FinanceManagement.FinancePlan.Services;
    using Base;
    using Domain.FinanceManagement.FinancePlan.Interfaces;
    using VisitPay.Enums;

    public class FinancePlanIncentiveServiceMockBuilder : IMockBuilder<IFinancePlanIncentiveService, FinancePlanIncentiveServiceMockBuilder>
    {
        public DomainServiceCommonServiceMockBuilder DomainServiceCommonServiceMockBuilder;


        public FinancePlanIncentiveServiceMockBuilder()
        {
            this.DomainServiceCommonServiceMockBuilder = new DomainServiceCommonServiceMockBuilder();
        }

        public IFinancePlanIncentiveService CreateService()
        {
            return new FinancePlanIncentiveService(
                this.DomainServiceCommonServiceMockBuilder.CreateServiceLazy()
            );
        }

        public FinancePlanIncentiveServiceMockBuilder Setup(Action<FinancePlanIncentiveServiceMockBuilder> configAction)
        {
            configAction(this);
            return this;
        }

        public FinancePlanIncentiveServiceMockBuilder WithFeatureEnabled(bool isEnabled = true)
        {
            this.DomainServiceCommonServiceMockBuilder
                .FeatureServiceMock.Setup(x => x.IsFeatureEnabled(VisitPayFeatureEnum.FinancePlanRefundInterestIncentive))
                .Returns(isEnabled);

            return this;
        }

        public FinancePlanIncentiveServiceMockBuilder WithDefaultFeatureConfiguration()
        {
            FinancePlanIncentiveConfigurationDto dto = new FinancePlanIncentiveConfigurationDto
            {
                FinancePlanIncentiveInterestRefundMethod = FinancePlanIncentiveInterestRefundMethodEnum.NumberOfMonthsLeftInFinancePlan,
                Value = 2
            };
            this.DomainServiceCommonServiceMockBuilder.ClientServiceMock.Setup(x => x.GetClient().FinancePlanIncentiveConfiguration).Returns(dto);

            return this;
        }

    }
}
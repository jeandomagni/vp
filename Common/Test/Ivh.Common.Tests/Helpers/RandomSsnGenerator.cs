﻿namespace Ivh.Common.Tests.Helpers
{
    using Common.Base.Utilities.Helpers;
    using Domain.Matching.Validators;

    public static class RandomSsnGenerator
    {
        private static readonly SsnValidator SsnValidator;

        static RandomSsnGenerator()
        {
            SsnValidator = new SsnValidator();
        }

        public static string New()
        {
            string ssn = RandomSourceSystemKeyGenerator.New("NNNNNNNNN");

            for (; !SsnValidator.IsValid(ssn); ssn = RandomSourceSystemKeyGenerator.New("NNNNNNNNN")){}

            return ssn;
        }
    }
}

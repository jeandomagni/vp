﻿namespace Ivh.Common.Tests.Helpers
{
    using System;
    using System.Data;
    using Moq;
    using NHibernate;
    using NHibernate.Transaction;

    public class StatelessSessionMockBuilder : IMockBuilder<IStatelessSession, StatelessSessionMockBuilder>
    {
        public Mock<IStatelessSession> StatelessSessionMock;
        public Mock<ITransaction> TransactionMock;

        public StatelessSessionMockBuilder()
        {
            this.TransactionMock = new Mock<ITransaction>();
            this.TransactionMock.Setup(t => t.IsActive).Returns(() => true);



            this.StatelessSessionMock = new Mock<IStatelessSession>();
            this.StatelessSessionMock.Setup(t => t.Transaction).Returns(() => this.TransactionMock.Object);
            this.StatelessSessionMock.Setup(t => t.BeginTransaction(Ivh.Common.Data.Constants.IsolationLevel.Default)).Returns(() => this.TransactionMock.Object);
            this.StatelessSessionMock.Setup(t => t.BeginTransaction(IsolationLevel.ReadCommitted)).Returns(() => this.TransactionMock.Object);

        }

        public IStatelessSession CreateService()
        {
            return this.StatelessSessionMock.Object;
        }

        public StatelessSessionMockBuilder Setup(Action<StatelessSessionMockBuilder> configAction)
        {
            configAction(this);
            return this;
        }

        public StatelessSessionMockBuilder UseSynchronizations()
        {
            // this will make RegisterPostCommit events fire
            this.TransactionMock.Setup(x => x.RegisterSynchronization(It.IsAny<ISynchronization>())).Callback((ISynchronization a) =>
            {
                a.AfterCompletion(true);
            });

            return this;
        }

        public Mock<IStatelessSession> CreateMock()
        {
            return this.StatelessSessionMock;
        }
    }
}
﻿namespace Ivh.Common.Tests.Helpers.JournalEvent
{
    using System;
    using System.Data;
    using Data.Connection;
    using Data.Connection.Connections;
    using Ivh.Common.Base.Enums;
	using NHibernate;
	using Ivh.Domain.EventJournal.Interfaces;
	using Ivh.Domain.EventJournal.Services;
    using Moq;
    using ServiceBus.Interfaces;
    using VisitPay.Enums;

    public class EventJournalServiceMockBuilder : IMockBuilder<IEventJournalService, EventJournalServiceMockBuilder>
    {
        public ApplicationEnum ApplicationEnum;
		public IBus Bus;
		public ISession Session;
		public IJournalEventRepository JournalEventRepository;
		public IUserAgentRepository UserAgentRepository;
		public Mock<IBus> BusMock;
		public Mock<ISession> SessionMock;
		public Mock<IJournalEventRepository> JournalEventRepositoryMock;
		public Mock<IUserAgentRepository> UserAgentRepositoryMock;

        public EventJournalServiceMockBuilder()
        {
			this.BusMock = new Mock<IBus>();
			this.SessionMock = new SessionMockBuilder().SessionMock;
			this.JournalEventRepositoryMock = new Mock<IJournalEventRepository>();
			this.UserAgentRepositoryMock = new Mock<IUserAgentRepository>();
        }
        
        public IEventJournalService CreateService()
        {
            return new EventJournalService(
                this.ApplicationEnum,
				new Lazy<IBus>(() => this.Bus ?? this.BusMock.Object),
                new SessionContext<Logging>(this.Session ?? this.SessionMock.Object),
				new Lazy<IJournalEventRepository>(() => this.JournalEventRepository ?? this.JournalEventRepositoryMock.Object),
				new Lazy<IUserAgentRepository>(() => this.UserAgentRepository ?? this.UserAgentRepositoryMock.Object)
            ); 
         }
        
        public EventJournalServiceMockBuilder Setup(Action<EventJournalServiceMockBuilder> configAction)
        {
            configAction(this);
            return this;
        }
    }
}
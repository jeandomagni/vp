﻿namespace Ivh.Common.Tests.Helpers.Ach
{
    using System;
    using Application.FinanceManagement.ApplicationServices;
    using Application.FinanceManagement.Common.Interfaces;
    using Base;
    using Data.Connection;
    using Data.Connection.Connections;
    using Domain.Core.SystemException.Interfaces;
    using Domain.FinanceManagement.Ach.Interfaces;
    using Domain.FinanceManagement.Interfaces;
    using Domain.FinanceManagement.Statement.Interfaces;
    using Domain.Guarantor.Interfaces;
    using Moq;
    using NHibernate;

    public class AchApplicationServiceMockBuilder : IMockBuilder<IAchApplicationService, AchApplicationServiceMockBuilder>
    {
        public IAchService AchService;
        public IGuarantorService GuarantorService;
        public IVisitTransactionChangesApplicationService VisitTransactionChangesApplicationService;
        public IPaymentReversalApplicationService PaymentReversalApplicationService;
        public IPaymentService PaymentService;
        public Domain.GuestPay.Interfaces.IPaymentService GpPaymentService;
        public ISession Session;
        public IVpStatementService VpStatementService;
        public ISystemExceptionService SystemExceptionService;

        public ApplicationServiceCommonServiceMockBuilder ApplicationServiceCommonServiceMockBuilder;
        public Mock<IAchService> AchServiceMock;
        public Mock<IGuarantorService> GuarantorServiceMock;
        public Mock<IVisitTransactionChangesApplicationService> VisitTransactionChangesApplicationServiceMock;
        public Mock<IPaymentReversalApplicationService> PaymentReversalApplicationServiceMock;
        public Mock<IPaymentService> PaymentServiceMock;
        public Mock<Domain.GuestPay.Interfaces.IPaymentService> GpPaymentServiceMock;
        public Mock<IVpStatementService> VpStatementServiceMock;
        public Mock<ISystemExceptionService> SystemExceptionServiceMock;

        public AchApplicationServiceMockBuilder()
        {
            this.ApplicationServiceCommonServiceMockBuilder = new ApplicationServiceCommonServiceMockBuilder();
            this.Session = new SessionMockBuilder().UseSynchronizations().CreateService();

            this.AchServiceMock = new Mock<IAchService>();
            this.GuarantorServiceMock = new Mock<IGuarantorService>();
            this.VisitTransactionChangesApplicationServiceMock = new Mock<IVisitTransactionChangesApplicationService>();
            this.PaymentReversalApplicationServiceMock = new Mock<IPaymentReversalApplicationService>();
            this.PaymentServiceMock = new Mock<IPaymentService>();
            this.GpPaymentServiceMock = new Mock<Domain.GuestPay.Interfaces.IPaymentService>();
            this.VpStatementServiceMock = new Mock<IVpStatementService>();
            this.SystemExceptionServiceMock = new Mock<ISystemExceptionService>();
        }

        public IAchApplicationService CreateService()
        {
            return new AchApplicationService(
                this.ApplicationServiceCommonServiceMockBuilder.CreateServiceLazy(),
                new Lazy<IAchService>(() => this.AchService ?? this.AchServiceMock.Object),
                new Lazy<IPaymentService>(() => this.PaymentService ?? this.PaymentServiceMock.Object),
                new Lazy<Domain.GuestPay.Interfaces.IPaymentService>(() => this.GpPaymentService ?? this.GpPaymentServiceMock.Object),
                new Lazy<IVpStatementService>(() => this.VpStatementService ?? this.VpStatementServiceMock.Object),
                new Lazy<IGuarantorService>(() => this.GuarantorService ?? this.GuarantorServiceMock.Object),
                new Lazy<ISystemExceptionService>(() => this.SystemExceptionService ?? this.SystemExceptionServiceMock.Object),
                new Lazy<IVisitTransactionChangesApplicationService>(() => this.VisitTransactionChangesApplicationService ?? this.VisitTransactionChangesApplicationServiceMock.Object),
                new Lazy<IPaymentReversalApplicationService>(() => this.PaymentReversalApplicationService ?? this.PaymentReversalApplicationServiceMock.Object),
                new SessionContext<VisitPay>(this.Session)
            );
        }

        public AchApplicationServiceMockBuilder Setup(Action<AchApplicationServiceMockBuilder> configAction)
        {
            configAction(this);
            return this;
        }
    }
}
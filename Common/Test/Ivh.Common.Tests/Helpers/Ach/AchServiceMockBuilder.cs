﻿namespace Ivh.Common.Tests.Helpers.Ach
{
    using System;
    using Base;
    using Data.Connection;
    using Data.Connection.Connections;
    using Domain.FinanceManagement.Ach.Interfaces;
    using Domain.FinanceManagement.Ach.Services;
    using Moq;
    using NHibernate;

    public class AchServiceMockBuilder : IMockBuilder<IAchService, AchServiceMockBuilder>
    {
        public IAchFileRepository AchFileRepository;
        public IAchProvider AchProvider;
        public IAchReturnDetailRepository AchReturnDetailRepository;
        public IAchSettlementDetailRepository AchSettlementDetailRepository;
        public ISession Session;
        
        public DomainServiceCommonServiceMockBuilder DomainServiceCommonServiceMockBuilder;
        public Mock<IAchFileRepository> AchFileRepositoryMock;
        public Mock<IAchProvider> AchProviderMock;
        public Mock<IAchReturnDetailRepository> AchReturnDetailRepositoryMock;
        public Mock<IAchSettlementDetailRepository> AchSettlementDetailRepositoryMock;

        public AchServiceMockBuilder()
        {
            this.DomainServiceCommonServiceMockBuilder = new DomainServiceCommonServiceMockBuilder();
            this.Session = new SessionMockBuilder().UseSynchronizations().CreateService();

            this.AchFileRepositoryMock = new Mock<IAchFileRepository>();
            this.AchProviderMock = new Mock<IAchProvider>();
            this.AchReturnDetailRepositoryMock = new Mock<IAchReturnDetailRepository>();
            this.AchSettlementDetailRepositoryMock = new Mock<IAchSettlementDetailRepository>();
        }

        public IAchService CreateService()
        {
            return new AchService(
                this.DomainServiceCommonServiceMockBuilder.CreateServiceLazy(),
                new SessionContext<VisitPay>(this.Session),
                new Lazy<IAchFileRepository>(() => this.AchFileRepository ?? this.AchFileRepositoryMock.Object),
                new Lazy<IAchReturnDetailRepository>(() => this.AchReturnDetailRepository ?? this.AchReturnDetailRepositoryMock.Object),
                new Lazy<IAchSettlementDetailRepository>(() => this.AchSettlementDetailRepository ?? this.AchSettlementDetailRepositoryMock.Object),
                new Lazy<IAchProvider>(() => this.AchProvider ?? this.AchProviderMock.Object)
            );
        }

        public AchServiceMockBuilder Setup(Action<AchServiceMockBuilder> configAction)
        {
            configAction(this);
            return this;
        }
    }
}
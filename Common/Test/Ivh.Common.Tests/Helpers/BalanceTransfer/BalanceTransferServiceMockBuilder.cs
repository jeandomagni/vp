﻿namespace Ivh.Common.Tests.Helpers.BalanceTransfer
{
    using System;
    using System.Data;
    using System.Linq;
    using Ivh.Domain.Base.Interfaces;
    using Ivh.Domain.FinanceManagement.FinancePlan.Interfaces;
    using Ivh.Domain.Visit.Interfaces;
    using Ivh.Domain.FinanceManagement.Services;
    using Base;
    using Common.Base.Enums;
    using Domain.FinanceManagement.Interfaces;
    using Domain.Settings.Entities;
    using Domain.Visit.Entities;
    using Moq;
    using VisitPay.Enums;

    public class BalanceTransferServiceMockBuilder : IMockBuilder<IBalanceTransferService, BalanceTransferServiceMockBuilder>
    {
        public DomainServiceCommonServiceMockBuilder DomainServiceCommonServiceMockBuilder;
        public IFinancePlanRepository FinancePlanRepository;
        public IVisitRepository VisitRepository;

        public Mock<IFinancePlanRepository> FinancePlanRepositoryMock;
        public Mock<IVisitRepository> VisitRepositoryMock;

        public BalanceTransferServiceMockBuilder()
        {
            this.DomainServiceCommonServiceMockBuilder = new DomainServiceCommonServiceMockBuilder();
            this.FinancePlanRepositoryMock = new Mock<IFinancePlanRepository>();
            this.VisitRepositoryMock = new Mock<IVisitRepository>();
        }

        public IBalanceTransferService CreateService()
        {
            return new BalanceTransferService(
                this.DomainServiceCommonServiceMockBuilder.CreateServiceLazy(),
                new Lazy<IFinancePlanRepository>(() => this.FinancePlanRepository ?? this.FinancePlanRepositoryMock.Object),
                new Lazy<IVisitRepository>(() => this.VisitRepository ?? this.VisitRepositoryMock.Object)
            );
        }

        public BalanceTransferServiceMockBuilder Setup(Action<BalanceTransferServiceMockBuilder> configAction)
        {
            configAction(this);
            return this;
        }

        public BalanceTransferServiceMockBuilder WithDefaults()
        {
            Mock<Client> clientMock = new Mock<Client>();
            clientMock.Setup(t => t.BalanceTransferFinancePlanMinDuration).Returns(13);
            clientMock.Setup(t => t.BalanceTransferFinancePlanMaxDuration).Returns(36);
            this.DomainServiceCommonServiceMockBuilder.ClientServiceMock.Setup(t => t.GetClient()).Returns(clientMock.Object);

            this.DomainServiceCommonServiceMockBuilder.FeatureServiceMock.Setup(t => t.IsFeatureEnabled(It.IsAny<VisitPayFeatureEnum>())).Returns(true);
            return this;
        }

       
    }
}
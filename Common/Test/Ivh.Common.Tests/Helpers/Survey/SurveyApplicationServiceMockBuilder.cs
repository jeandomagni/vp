﻿namespace Ivh.Common.Tests.Helpers.Survey
{
    using System;
    using Application.Core.Common.Interfaces;
    using Ivh.Domain.AppIntelligence.Survey.Interfaces;
    using Ivh.Application.FinanceManagement.Common.Interfaces;
    using Ivh.Domain.Guarantor.Interfaces;
    using Ivh.Domain.Content.Interfaces;
    using Ivh.Application.Core.ApplicationServices;
    using Base;
    using Moq;

    public class SurveyApplicationServiceMockBuilder : IMockBuilder<ISurveyApplicationService, SurveyApplicationServiceMockBuilder>
    {
        public ApplicationServiceCommonServiceMockBuilder ApplicationServiceCommonServiceMockBuilder;
        public ISurveyService SurveyService;
        public IStatementApplicationService StatementApplicationService;
        public IGuarantorService GuarantorService;
        public IContentService ContentService;

        public Mock<ISurveyService> SurveyServiceMock;
        public Mock<IStatementApplicationService> StatementApplicationServiceMock;
        public Mock<IGuarantorService> GuarantorServiceMock;
        public Mock<IContentService> ContentServiceMock;

        public SurveyApplicationServiceMockBuilder()
        {
            this.ApplicationServiceCommonServiceMockBuilder = new ApplicationServiceCommonServiceMockBuilder();
            this.SurveyServiceMock = new Mock<ISurveyService>();
            this.StatementApplicationServiceMock = new Mock<IStatementApplicationService>();
            this.GuarantorServiceMock = new Mock<IGuarantorService>();
            this.ContentServiceMock = new Mock<IContentService>();
        }

        public ISurveyApplicationService CreateService()
        {
            return new SurveyApplicationService(
                this.ApplicationServiceCommonServiceMockBuilder.CreateServiceLazy(),
                new Lazy<ISurveyService>(() => this.SurveyService ?? this.SurveyServiceMock.Object),
                new Lazy<IStatementApplicationService>(() => this.StatementApplicationService ?? this.StatementApplicationServiceMock.Object),
                new Lazy<IGuarantorService>(() => this.GuarantorService ?? this.GuarantorServiceMock.Object),
                new Lazy<IContentService>(() => this.ContentService ?? this.ContentServiceMock.Object)
            );
        }

        public SurveyApplicationServiceMockBuilder Setup(Action<SurveyApplicationServiceMockBuilder> configAction)
        {
            configAction(this);
            return this;
        }
    }
}
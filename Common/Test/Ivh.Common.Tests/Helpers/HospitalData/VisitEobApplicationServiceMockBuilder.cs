﻿namespace Ivh.Common.Tests.Helpers.HospitalData
{
    using System;
    using Application.Core;
    using Application.Core.Common.Interfaces;
    using Base;
    using Domain.Core.SystemException.Interfaces;
    using Domain.Core.SystemException.Services;
    using Domain.Eob.Interfaces;
    using Domain.Eob.Services;
    using Ivh.Application.Core.ApplicationServices;
    using Ivh.Domain.Visit.Interfaces;
    using Ivh.Domain.Visit.Services;
    using Moq;

    public class VisitEobApplicationServiceMockBuilder : IMockBuilder<IVisitEobApplicationService, VisitEobApplicationServiceMockBuilder>
    {
        public ClientApplicationService ClientApplicationService;
        public ClaimPaymentInformationService Eob835Service;
        public VisitService VisitService;
        public SystemExceptionService SystemExceptionService;

        public Mock<IClaimPaymentInformationService> ClaimPaymentInformationServiceMock;
        public Mock<IVisitService> VisitServiceMock;
        public Mock<ISystemExceptionService> SystemExceptionServiceMock;

        public VisitEobApplicationServiceMockBuilder()
        {
            this.ClaimPaymentInformationServiceMock = new Mock<IClaimPaymentInformationService>();
            this.VisitServiceMock = new Mock<IVisitService>();
        }

        public IVisitEobApplicationService CreateService()
        {
            return new VisitEobApplicationService(
                new ApplicationServiceCommonServiceMockBuilder().CreateServiceLazy(),
                new Lazy<IVisitService>(() => this.VisitService ?? this.VisitServiceMock.Object),
                new Lazy<IClaimPaymentInformationService>(() => this.Eob835Service ?? this.ClaimPaymentInformationServiceMock.Object),
                new Lazy<ISystemExceptionService>(() => this.SystemExceptionService ?? this.SystemExceptionServiceMock.Object)
            );
        }

        public VisitEobApplicationServiceMockBuilder Setup(Action<VisitEobApplicationServiceMockBuilder> configAction)
        {
            configAction(this);
            return this;
        }
    }
}
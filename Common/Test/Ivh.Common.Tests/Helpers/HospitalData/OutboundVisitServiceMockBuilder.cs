﻿namespace Ivh.Common.Tests.Helpers.HospitalData
{
    using System;
    using Base;
    using Domain.HospitalData.Visit.Interfaces;
    using Domain.HospitalData.Visit.Services;
    using Moq;

    public class OutboundVisitServiceMockBuilder : IMockBuilder<IOutboundVisitService, OutboundVisitServiceMockBuilder>
    {
        public DomainServiceCommonServiceMockBuilder DomainServiceCommonServiceMockBuilder;
        public IOutboundVisitRepository OutboundVisitRepository;
        public IVisitService VisitService;

        public Mock<IOutboundVisitRepository> OutboundVisitRepositoryMock;
        public Mock<IVisitService> VisitServiceMock;
        
        public OutboundVisitServiceMockBuilder()
        {
            this.DomainServiceCommonServiceMockBuilder = new DomainServiceCommonServiceMockBuilder();

            this.OutboundVisitRepositoryMock = new Mock<IOutboundVisitRepository>();
            this.VisitServiceMock = new Mock<IVisitService>();
        }
        
        public IOutboundVisitService CreateService()
        {
            return new OutboundVisitService(
                this.DomainServiceCommonServiceMockBuilder.CreateServiceLazy(),
                new Lazy<IOutboundVisitRepository>(() => this.OutboundVisitRepository ?? this.OutboundVisitRepositoryMock.Object),
                new Lazy<IVisitService>(() => this.VisitService ?? this.VisitServiceMock.Object)
            );
        }

        public OutboundVisitServiceMockBuilder Setup(Action<OutboundVisitServiceMockBuilder> configAction)
        {
            configAction(this);
            return this;
        }
    }
}
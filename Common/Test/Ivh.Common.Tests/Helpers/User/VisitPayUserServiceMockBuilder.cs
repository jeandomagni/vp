﻿namespace Ivh.Common.Tests.Helpers.User
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Security.Claims;
    using Base;
    using Client;
    using Common.Base.Utilities.Extensions;
    using Domain.Base.Interfaces;
    using Domain.User.Entities;
    using Domain.User.Interfaces;
    using Domain.User.Services;
    using Domain.Logging.Interfaces;
    using Domain.Settings.Entities;
    using Domain.Settings.Interfaces;
    using Encryption.Security.Interfaces;
    using Microsoft.AspNet.Identity;
    using Moq;

    public class VisitPayUserServiceMockBuilder : IMockBuilder<VisitPayUserService, VisitPayUserServiceMockBuilder>
    {
        public IUserStore<VisitPayUser> Store;
        public IIdentityEmailService IdentityEmailService;
        public IIdentitySmsService IdentitySmsService;
        public IClientService ClientService;
        public IVisitPayUserRepository VisitPayUserRepository;
        public IVisitPayUserPasswordHistoryRepository VisitPayUserPasswordHistoryRepository;
        public IApplicationSettingsService ApplicationSettingsService;
        public IPasswordHasherService PasswordHasherService;
        public IUserTokenProvider<VisitPayUser, string> UserTokenProvider;
        public ILoggingProvider LoggingProvider;
        public ILoggingService LoggingService;
        public IDomainServiceCommonService DomainServiceCommonService;

        public Mock<IUserStore<VisitPayUser>> StoreMock;
        public Mock<IIdentityEmailService> IdentityEmailServiceMock;
        public Mock<IIdentitySmsService> IdentitySmsServiceMock;
        public Mock<IClientService> ClientServiceMock;
        public Mock<IVisitPayUserRepository> VisitPayUserRepositoryMock;
        public Mock<IVisitPayUserPasswordHistoryRepository> VisitPayUserPasswordHistoryRepositoryMock;
        public Mock<IApplicationSettingsService> ApplicationSettingsServiceMock;
        public Mock<IPasswordHasherService> PasswordHasherServiceMock;
        public Mock<IUserTokenProvider<VisitPayUser, string>> UserTokenProviderMock;
        public Mock<ILoggingProvider> LoggingProviderMock;
        public Mock<ILoggingService> LoggingServiceMock;
        public Mock<IPasswordHasher> PasswordHasherMock;
        public Mock<IClaimsIdentityFactory<VisitPayUser, string>> ClaimsIdentityFactoryMock;
        
        
        
        
        public VisitPayUserServiceMockBuilder()
        {
            this.StoreMock = new Mock<IUserStore<VisitPayUser>>();
            this.IdentityEmailServiceMock = new Mock<IIdentityEmailService>();
            this.IdentitySmsServiceMock = new Mock<IIdentitySmsService>();
            this.ClientServiceMock = new ClientServiceMockBuilder().CreateMock();
            this.VisitPayUserRepositoryMock = new Mock<IVisitPayUserRepository>();
            this.VisitPayUserPasswordHistoryRepositoryMock = new Mock<IVisitPayUserPasswordHistoryRepository>();
            this.ApplicationSettingsServiceMock = new ApplicationSettingsServiceMockBuilder().CreateMock();
            this.PasswordHasherServiceMock = new Mock<IPasswordHasherService>();
            this.UserTokenProviderMock = new Mock<IUserTokenProvider<VisitPayUser, string>>();
            this.LoggingProviderMock = new Mock<ILoggingProvider>();
            this.LoggingServiceMock = new Mock<ILoggingService>();

            this.ClientServiceMock.Setup(x => x.GetClient()).Returns(new Mock<Client>().Object);

            this.ApplicationSettingsServiceMock.WithSetting(x => x.IsClientApplication, () => false);
            this.ApplicationSettingsServiceMock.WithSetting(x => x.PasswordHashConfigurations, () => new Dictionary<int, PasswordHashConfiguration>
            {
                // copied from app settings
                {0, new PasswordHashConfiguration {Iterations = 1000, HashFunction = "HMACSHA1", SaltLength = 16}},
                {1, new PasswordHashConfiguration {Iterations = 10000, HashFunction = "HMACSHA256", SaltLength = 16}}
            });

            this.DomainServiceCommonService = DomainServiceCommonServices.CreateDomainServiceCommonService(
                applicationSettingsServiceMock: this.ApplicationSettingsServiceMock,
                featureServiceMock: null,
                clientServiceMock: this.ClientServiceMock,
                loggingServiceMock: this.LoggingServiceMock,
                busMock: null,
                cacheMock: null,
                timeZoneHelperMock: null);
        }

        public VisitPayUserService CreateService()
        {
            VisitPayUserService svc = new VisitPayUserService(
                new Lazy<IDomainServiceCommonService>(() => this.DomainServiceCommonService),

                this.Store ?? this.StoreMock.Object,
                new Lazy<IIdentityEmailService>(() => this.IdentityEmailService ?? this.IdentityEmailServiceMock.Object),
                new Lazy<IIdentitySmsService>(() => this.IdentitySmsService ?? this.IdentitySmsServiceMock.Object),
                new Lazy<IVisitPayUserRepository>(() => this.VisitPayUserRepository ?? this.VisitPayUserRepositoryMock.Object),
                new Lazy<IVisitPayUserPasswordHistoryRepository>(() => this.VisitPayUserPasswordHistoryRepository ?? this.VisitPayUserPasswordHistoryRepositoryMock.Object),
                new Lazy<IPasswordHasherService>(() => this.PasswordHasherService ?? this.PasswordHasherServiceMock.Object),
                this.UserTokenProvider ?? this.UserTokenProviderMock.Object
            );

            


            if (this.PasswordHasherMock != null)
            {
                svc.PasswordHasher = this.PasswordHasherMock.Object;
            }

            if (this.ClaimsIdentityFactoryMock != null)
            {
                svc.ClaimsIdentityFactory = this.ClaimsIdentityFactoryMock.Object;
            }

            return svc;
        }

        public VisitPayUserServiceMockBuilder Setup(Action<VisitPayUserServiceMockBuilder> configAction)
        {
            configAction(this);
            return this;
        }
        
        public VisitPayUserServiceMockBuilder WithLoginIdentity(ClaimsIdentity identity)
        {
            this.ClaimsIdentityFactoryMock = new Mock<IClaimsIdentityFactory<VisitPayUser, string>>();
            this.ClaimsIdentityFactoryMock.Setup(x => x.CreateAsync(It.IsAny<UserManager<VisitPayUser, string>>(), It.IsAny<VisitPayUser>(), It.IsAny<string>())).ReturnsAsync(identity);
            return this;
        }

        public VisitPayUserServiceMockBuilder WithUser(VisitPayUser user)
        {
            this.StoreMock.Setup(s => s.FindByIdAsync(user.VisitPayUserId.ToString())).ReturnsAsync(() => user);
            return this;
        }

        public VisitPayUserServiceMockBuilder WithMatchingUsers(IReadOnlyList<VisitPayUser> users)
        {
            this.VisitPayUserRepositoryMock
                .Setup(r => r.FindByNameAndDob(It.IsAny<string>(), It.IsAny<DateTime>()))
                .Returns(users);

            return this;
        }

        public VisitPayUserServiceMockBuilder WithUserMatchedOnDetailsAndAccessTokenPhi(VisitPayUser user)
        {
            this.VisitPayUserRepositoryMock
                .Setup(r => r.FindByDetailsAndAccessTokenPhi(It.IsAny<string>(), It.IsAny<DateTime>(), It.IsAny<string>()))
                .Returns(user.ToListOfOne().ToList());

            return this;
        }
    }
    
}
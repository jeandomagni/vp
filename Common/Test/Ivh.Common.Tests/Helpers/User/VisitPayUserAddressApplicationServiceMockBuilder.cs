﻿namespace Ivh.Common.Tests.Helpers.User
{
    using System;
    using Application.Base.Common.Interfaces;
    using Application.User.Common.Interfaces;
    using Application.User.Services;
    using Domain.User.Interfaces;
    using Moq;

    public class VisitPayUserAddressApplicationServiceMockBuilder
        : IMockBuilder<IVisitPayUserAddressApplicationService, VisitPayUserAddressApplicationServiceMockBuilder>
    {
        public IApplicationServiceCommonService ApplicationServiceCommonService;
        public IVisitPayUserAddressVerificationService VisitPayUserAddressVerificationService;
        public IVisitPayUserService VisitPayUserService;

        public Mock<IApplicationServiceCommonService> ApplicationServiceCommonServiceMock;
        public Mock<IVisitPayUserAddressVerificationService> VisitPayUserAddressVerificationServiceMock;
        public Mock<IVisitPayUserService> VisitPayUserServiceMock;

        public VisitPayUserAddressApplicationServiceMockBuilder()
        {
            this.ApplicationServiceCommonServiceMock = new Mock<IApplicationServiceCommonService>();
            this.VisitPayUserServiceMock = new Mock<IVisitPayUserService>();
            this.VisitPayUserAddressVerificationServiceMock = new Mock<IVisitPayUserAddressVerificationService>();
        }

        public IVisitPayUserAddressApplicationService CreateService()
        {
            return new VisitPayUserAddressApplicationService(
                new Lazy<IApplicationServiceCommonService>(() => this.ApplicationServiceCommonService ?? this.ApplicationServiceCommonServiceMock.Object),
                new Lazy<IVisitPayUserService>(() => this.VisitPayUserService ?? this.VisitPayUserServiceMock.Object),
                new Lazy<IVisitPayUserAddressVerificationService>(() => this.VisitPayUserAddressVerificationService ?? this.VisitPayUserAddressVerificationServiceMock.Object));
        }

        public VisitPayUserAddressApplicationServiceMockBuilder Setup(
            Action<VisitPayUserAddressApplicationServiceMockBuilder> configAction)
        {
            configAction(this);
            return this;
        }
    }
}
﻿namespace Ivh.Common.Tests.Helpers.User
{
    using System;
    using Application.Core;
    using Application.Core.Common.Interfaces;
    using Application.FinanceManagement.Common.Interfaces;
    using Base;
    using Data.Connection;
    using Data.Connection.Connections;
    using Domain.Content.Interfaces;
    using Domain.Core.LegacyUser.Interfaces;
    using Domain.Email.Interfaces;
    using Domain.Guarantor.Interfaces;
    using Domain.Logging.Interfaces;
    using Domain.User.Interfaces;
    using Domain.User.Services;
    using Ivh.Application.User.Common.Interfaces;
    using Microsoft.Owin.Security;
    using Moq;
    using NHibernate;
    using VisitPay.Enums;
    using Web.Cookies;
    using IHsGuarantorService = Domain.HospitalData.VpGuarantor.Interfaces.IHsGuarantorService;

    public class VisitPayUserApplicationServiceMockBuilder : IMockBuilder<IVisitPayUserApplicationService, VisitPayUserApplicationServiceMockBuilder>
    {
        public ApplicationServiceCommonServiceMockBuilder ApplicationServiceCommonServiceMockBuilder;
        public IAuthenticationManager AuthenticationManager;
        public Mock<IAuthenticationManager> AuthenticationManagerMock;
        public ICommunicationService CommunicationService;
        public Mock<ICommunicationService> CommunicationServiceMock;
        public IContentService ContentService;
        public Mock<IContentService> ContentServiceMock;
        public IMetricsProvider MetricsProvider;
        public Mock<IMetricsProvider> MetricsProviderMock;
        public IGuarantorEnrollmentApplicationService GuarantorEnrollmentApplicationService;
        public Mock<IGuarantorEnrollmentApplicationService> GuarantorEnrollmentApplicationServiceMock;
        public IGuarantorService GuarantorService;
        public Mock<IGuarantorService> GuarantorServiceMock;
        public IHsGuarantorService HsGuarantorService;
        public Mock<IHsGuarantorService> HsGuarantorServiceMock;
        public IImageService ImageService;
        public Mock<IImageService> ImageServiceMock;
        public ILegacyUserService LegacyUserService;
        public Mock<ILegacyUserService> LegacyUserServiceMock;
        public ILocalizationCookieFacade LocalizationCookieFacade;
        public Mock<ILocalizationCookieFacade> LocalizationCookieFacadeMock;
        public ISession Session;
        public Mock<ISession> SessionMock;
        public IStatementApplicationService StatementApplicationService;
        public Mock<IStatementApplicationService> StatementApplicationServiceMock;
        public IVisitPayRoleService VisitPayRoleService;
        public Mock<IVisitPayRoleService> VisitPayRoleServiceMock;
        public VisitPaySignInService VisitPaySignInService;
        public Mock<VisitPaySignInService> VisitPaySignInServiceMock;
        public IVisitPayUserJournalEventService VisitPayUserJournalEventService;
        public Mock<IVisitPayUserJournalEventService> VisitPayUserJournalEventServiceMock;

        public VisitPayUserServiceMockBuilder VisitPayUserServiceMockBuilder;
        public IVpUserLoginSummaryService VpUserLoginSummaryService;
        public Mock<IVpUserLoginSummaryService> VpUserLoginSummaryServiceMock;
        public IVisitPayUserAddressApplicationService VisitPayUserAddressApplicationService;
        public Mock<IVisitPayUserAddressApplicationService> VisitPayUserAddressApplicationServiceMock;

        public VisitPayUserApplicationServiceMockBuilder()
        {
            this.ApplicationServiceCommonServiceMockBuilder = new ApplicationServiceCommonServiceMockBuilder();
            this.VisitPayUserServiceMockBuilder = new VisitPayUserServiceMockBuilder();



            this.VisitPaySignInServiceMock = new Mock<VisitPaySignInService>();
            this.VisitPayRoleServiceMock = new Mock<IVisitPayRoleService>();
            this.VisitPayUserJournalEventServiceMock = new Mock<IVisitPayUserJournalEventService>();
            this.VpUserLoginSummaryServiceMock = new Mock<IVpUserLoginSummaryService>();
            this.GuarantorServiceMock = new Mock<IGuarantorService>();
            this.SessionMock = new SessionMockBuilder().SessionMock;
            this.HsGuarantorServiceMock = new Mock<IHsGuarantorService>();
            this.ContentServiceMock = new Mock<IContentService>();
            this.LegacyUserServiceMock = new Mock<ILegacyUserService>();
            this.StatementApplicationServiceMock = new Mock<IStatementApplicationService>();
            this.GuarantorEnrollmentApplicationServiceMock = new Mock<IGuarantorEnrollmentApplicationService>();
            this.MetricsProviderMock = new Mock<IMetricsProvider>();
            this.ImageServiceMock = new Mock<IImageService>();
            this.CommunicationServiceMock = new Mock<ICommunicationService>();
            this.AuthenticationManagerMock = new Mock<IAuthenticationManager>();
            this.LocalizationCookieFacadeMock = new Mock<ILocalizationCookieFacade>();
            this.VisitPayUserAddressApplicationServiceMock = new Mock<IVisitPayUserAddressApplicationService>();
        }

        public IVisitPayUserApplicationService CreateService()
        {
            return new VisitPayUserApplicationService(
                this.ApplicationServiceCommonServiceMockBuilder.CreateServiceLazy(),
                new Lazy<VisitPayUserService>(() => this.VisitPayUserServiceMockBuilder.CreateService()),
                new Lazy<VisitPaySignInService>(() => this.VisitPaySignInService ?? this.VisitPaySignInServiceMock.Object),
                new Lazy<IVisitPayRoleService>(() => this.VisitPayRoleService ?? this.VisitPayRoleServiceMock.Object),
                new Lazy<IVisitPayUserJournalEventService>(() => this.VisitPayUserJournalEventService ?? this.VisitPayUserJournalEventServiceMock.Object),
                new Lazy<IVpUserLoginSummaryService>(() => this.VpUserLoginSummaryService ?? this.VpUserLoginSummaryServiceMock.Object),
                new Lazy<IGuarantorService>(() => this.GuarantorService ?? this.GuarantorServiceMock.Object),
                new SessionContext<VisitPay>(this.Session ?? this.SessionMock.Object),
                new Lazy<IHsGuarantorService>(() => this.HsGuarantorService ?? this.HsGuarantorServiceMock.Object),
                new Lazy<IContentService>(() => this.ContentService ?? this.ContentServiceMock.Object),
                new Lazy<ILegacyUserService>(() => this.LegacyUserService ?? this.LegacyUserServiceMock.Object),
                new Lazy<IStatementApplicationService>(() => this.StatementApplicationService ?? this.StatementApplicationServiceMock.Object),
                new Lazy<IGuarantorEnrollmentApplicationService>(() => this.GuarantorEnrollmentApplicationService ?? this.GuarantorEnrollmentApplicationServiceMock.Object),
                new Lazy<IMetricsProvider>(() => this.MetricsProvider ?? this.MetricsProviderMock.Object),
                new Lazy<IImageService>(() => this.ImageService ?? this.ImageServiceMock.Object),
                new Lazy<ICommunicationService>(() => this.CommunicationService ?? this.CommunicationServiceMock.Object),
                new Lazy<IVisitPayUserAddressApplicationService>(() => this.VisitPayUserAddressApplicationService ?? this.VisitPayUserAddressApplicationServiceMock.Object),
                new Lazy<IAuthenticationManager>(() => this.AuthenticationManager ?? this.AuthenticationManagerMock.Object),
                new Lazy<ILocalizationCookieFacade>(() => this.LocalizationCookieFacade ?? this.LocalizationCookieFacadeMock.Object)
            );
        }

        public VisitPayUserApplicationServiceMockBuilder Setup(Action<VisitPayUserApplicationServiceMockBuilder> configAction)
        {
            configAction(this);
            return this;
        }
        
        public void SetFeature(VisitPayFeatureEnum visitPayFeatureEnum, bool enabled)
        {
            this.ApplicationServiceCommonServiceMockBuilder.Setup(b =>
            {
                b.FeatureServiceMock.Setup(x => x.IsFeatureEnabled(visitPayFeatureEnum)).Returns(enabled);
            });
        }
    }
}
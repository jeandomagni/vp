﻿namespace Ivh.Common.Tests.Helpers.User
{
    using System;
    using Application.User.Common.Interfaces;
    using Application.User.Services;
    using Base;
    using Data.Connection;
    using Data.Connection.Connections;
    using Domain.Guarantor.Interfaces;
    using Domain.User.Interfaces;
    using Domain.User.Services;
    using Microsoft.AspNet.Identity;
    using Microsoft.Owin.Security;
    using Moq;
    using NHibernate;
    using VisitPay.Enums;

    public class OfflineUserApplicationServiceMockBuilder : IMockBuilder<IOfflineUserApplicationService, OfflineUserApplicationServiceMockBuilder>
    {
        public ApplicationServiceCommonServiceMockBuilder ApplicationServiceCommonServiceMockBuilder;
        public IGuarantorService GuarantorService;
        public Mock<IGuarantorService> GuarantorServiceMock;
        public IBaseHsGuarantorService HsGuarantorService;
        public Mock<IBaseHsGuarantorService> HsGuarantorServiceMock;
        public ISession Session;
        public Mock<ISession> SessionMock;
        public VisitPaySignInService VisitPaySignInService;
        public Mock<VisitPaySignInService> VisitPaySignInServiceMock;
        public IVisitPayUserJournalEventService VisitPayUserJournalEventService;
        public Mock<IVisitPayUserJournalEventService> VisitPayUserJournalEventServiceMock;
        public VisitPayUserServiceMockBuilder VisitPayUserServiceMockBuilder;
        public Mock<IPasswordHasher> PasswordHasherMock;
        public Mock<IAuthenticationManager> AuthenticationManagerMock;

        public OfflineUserApplicationServiceMockBuilder()
        {
            this.ApplicationServiceCommonServiceMockBuilder = new ApplicationServiceCommonServiceMockBuilder();
            this.GuarantorServiceMock = new Mock<IGuarantorService>();
            this.HsGuarantorServiceMock = new Mock<IBaseHsGuarantorService>();
            this.SessionMock = new SessionMockBuilder().UseSynchronizations().CreateMock();
            this.VisitPaySignInServiceMock = new Mock<VisitPaySignInService>();
            this.VisitPayUserJournalEventServiceMock = new Mock<IVisitPayUserJournalEventService>();
            this.VisitPayUserServiceMockBuilder = new VisitPayUserServiceMockBuilder();
            this.PasswordHasherMock = new Mock<IPasswordHasher>();
            this.AuthenticationManagerMock = new Mock<IAuthenticationManager>();
        }

        public IOfflineUserApplicationService CreateService()
        {
            this.VisitPayUserServiceMockBuilder.PasswordHasherMock = this.PasswordHasherMock;
            VisitPayUserService visitPayUserService = this.VisitPayUserServiceMockBuilder.CreateService();
            this.VisitPaySignInService = new VisitPaySignInService(visitPayUserService, new Lazy<IVisitPayUserService>(() => new Mock<IVisitPayUserService>().Object), this.AuthenticationManagerMock.Object);

            return new OfflineUserApplicationService(
                this.ApplicationServiceCommonServiceMockBuilder.CreateServiceLazy(),
                new SessionContext<VisitPay>(this.Session ?? this.SessionMock.Object),
                new Lazy<IGuarantorService>(() => this.GuarantorService ?? this.GuarantorServiceMock.Object),
                new Lazy<IBaseHsGuarantorService>(() => this.HsGuarantorService ?? this.HsGuarantorServiceMock.Object),
                new Lazy<VisitPaySignInService>(() => this.VisitPaySignInService ?? this.VisitPaySignInServiceMock.Object),
                new Lazy<IVisitPayUserJournalEventService>(() => this.VisitPayUserJournalEventService ?? this.VisitPayUserJournalEventServiceMock.Object),
                new Lazy<VisitPayUserService>(() => visitPayUserService),
                new Lazy<IAuthenticationManager>(() => this.AuthenticationManagerMock.Object)
            );
        }
        
        public OfflineUserApplicationServiceMockBuilder Setup(Action<OfflineUserApplicationServiceMockBuilder> configAction)
        {
            configAction(this);
            return this;
        }
        
        public void SetFeature(VisitPayFeatureEnum visitPayFeatureEnum, bool enabled)
        {
            this.ApplicationServiceCommonServiceMockBuilder.Setup(b =>
            {
                b.FeatureServiceMock.Setup(x => x.IsFeatureEnabled(visitPayFeatureEnum)).Returns(enabled);
            });
        }
    }
}
﻿namespace Ivh.Common.Tests.Helpers.Guarantor
{
    using System;
    using Application.Core;
    using Application.Core.Common.Interfaces;
    using Application.HospitalData.Common.Interfaces;
    using Base;
    using Data.Connection;
    using Data.Connection.Connections;
    using Domain.Core.Sso.Interfaces;
    using Domain.Core.SystemException.Interfaces;
    using Domain.FinanceManagement.Consolidation.Interfaces;
    using Domain.FinanceManagement.FinancePlan.Interfaces;
    using Domain.FinanceManagement.Interfaces;
    using Domain.FinanceManagement.Statement.Interfaces;
    using Domain.Guarantor.Interfaces;
    using Domain.Logging.Interfaces;
    using Domain.SecureCommunication.GuarantorSupportRequests.Interfaces;
    using Domain.User.Interfaces;
    using Domain.User.Services;
    using Moq;
    using NHibernate;
    using IHsGuarantorService = Domain.HospitalData.VpGuarantor.Interfaces.IHsGuarantorService;

    public class GuarantorApplicationServiceMockBuilder : IMockBuilder<GuarantorApplicationService, GuarantorApplicationServiceMockBuilder>
    {
        public ApplicationServiceCommonServiceMockBuilder ApplicationServiceCommonServiceMockBuilder;
        
        public IHsGuarantorService HsGuarantorService;
        public IGuarantorService GuarantorService;
        public IGuarantorInvitationService GuarantorInvitationService;
        public IConsolidationGuarantorService ConsolidationGuarantorService;
        public IGuarantorStateMachineService GuarantorStateMachineService;
        public IVisitPayUserJournalEventService UserJournalEventService;
        public IPaymentService PaymentService;
        public VisitPayUserService UserService;
        public IVpStatementService StatementService;
        public IFinancePlanService FinancePlanService;
        public IGuarantorEnrollmentApplicationService GuarantorEnrollmentApplicationService;
        public ISystemExceptionService SystemExceptionService;
        public ISsoApplicationService SsoApplicationService;
        public ISupportRequestService SupportRequestService;
        public ISession Session;
        public IHsaBalanceProvider HsaBalanceProvider;
        public IMetricsProvider MetricsProvider;
        public IHealthEquityEmployerIdResolutionService HealthEquityEmployerIdResolutionService;
        public IStatementDateService StatementDateService;
        public IChangeEventApplicationService ChangeEventApplicationService;
        public IVpGuarantorHsMatchApiApplicationService VpGuarantorHsMatchApiApplicationService;
        
        public Mock<IHsGuarantorService> HsGuarantorServiceMock;
        public Mock<IGuarantorService> GuarantorServiceMock;
        public Mock<IGuarantorInvitationService> GuarantorInvitationServiceMock;
        public Mock<IConsolidationGuarantorService> ConsolidationGuarantorServiceMock;
        public Mock<IGuarantorStateMachineService> GuarantorStateMachineServiceMock;
        public Mock<IVisitPayUserJournalEventService> UserJournalEventServiceMock;
        public Mock<IPaymentService> PaymentServiceMock;
        public Mock<VisitPayUserService> UserServiceMock;
        public Mock<IVpStatementService> StatementServiceMock;
        public Mock<IFinancePlanService> FinancePlanServiceMock;
        public Mock<IGuarantorEnrollmentApplicationService> GuarantorEnrollmentApplicationServiceMock;
        public Mock<ISystemExceptionService> SystemExceptionServiceMock;
        public Mock<ISsoApplicationService> SsoApplicationServiceMock;
        public Mock<ISupportRequestService> SupportRequestServiceMock;
        public Mock<ISession> SessionMock;
        public Mock<IHsaBalanceProvider> HsaBalanceProviderMock;
        public Mock<IMetricsProvider> MetricsProviderMock;
        public Mock<IHealthEquityEmployerIdResolutionService> HealthEquityEmployerIdResolutionServiceMock;
        public Mock<IStatementDateService> StatementDateServiceMock;
        public Mock<IChangeEventApplicationService> ChangeEventApplicationServiceMock;
        public Mock<IVpGuarantorHsMatchApiApplicationService> VpGuarantorHsMatchApiApplicationServiceMock;


        public GuarantorApplicationServiceMockBuilder()
        {
            this.ApplicationServiceCommonServiceMockBuilder = new ApplicationServiceCommonServiceMockBuilder();
            this.HsGuarantorServiceMock = new Mock<IHsGuarantorService>();
            this.GuarantorServiceMock = new Mock<IGuarantorService>();
            this.GuarantorInvitationServiceMock = new Mock<IGuarantorInvitationService>();
            this.ConsolidationGuarantorServiceMock = new Mock<IConsolidationGuarantorService>();
            this.GuarantorStateMachineServiceMock = new Mock<IGuarantorStateMachineService>();
            this.UserJournalEventServiceMock = new Mock<IVisitPayUserJournalEventService>();
            this.PaymentServiceMock = new Mock<IPaymentService>();
            this.UserServiceMock = new Mock<VisitPayUserService>();
            this.StatementServiceMock = new Mock<IVpStatementService>();
            this.FinancePlanServiceMock = new Mock<IFinancePlanService>();
            this.GuarantorEnrollmentApplicationServiceMock = new Mock<IGuarantorEnrollmentApplicationService>();
            this.SystemExceptionServiceMock = new Mock<ISystemExceptionService>();
            this.SsoApplicationServiceMock = new Mock<ISsoApplicationService>();
            this.SupportRequestServiceMock = new Mock<ISupportRequestService>();
            this.SessionMock = new Mock<ISession>();
            this.HsaBalanceProviderMock = new Mock<IHsaBalanceProvider>();
            this.MetricsProviderMock = new Mock<IMetricsProvider>();
            this.HealthEquityEmployerIdResolutionServiceMock = new Mock<IHealthEquityEmployerIdResolutionService>();
            this.StatementDateServiceMock = new Mock<IStatementDateService>();
            this.ChangeEventApplicationServiceMock = new Mock<IChangeEventApplicationService>();
        }

        public GuarantorApplicationService CreateService()
        {
            return new GuarantorApplicationService(
                this.ApplicationServiceCommonServiceMockBuilder.CreateServiceLazy(),
                new Lazy<IHsGuarantorService>(() => this.HsGuarantorService ?? this.HsGuarantorServiceMock.Object),
                new Lazy<IGuarantorService>(() => this.GuarantorService ?? this.GuarantorServiceMock.Object),
                new Lazy<IGuarantorInvitationService>(() => this.GuarantorInvitationService ?? this.GuarantorInvitationServiceMock.Object),
                new Lazy<IConsolidationGuarantorService>(() => this.ConsolidationGuarantorService ?? this.ConsolidationGuarantorServiceMock.Object),
                new Lazy<IGuarantorStateMachineService>(() => this.GuarantorStateMachineService ?? this.GuarantorStateMachineServiceMock.Object),
                new Lazy<IVisitPayUserJournalEventService>(() => this.UserJournalEventService ?? this.UserJournalEventServiceMock.Object),
                new Lazy<IPaymentService>(() => this.PaymentService ?? this.PaymentServiceMock.Object),
                new Lazy<VisitPayUserService>(() => this.UserService ?? this.UserServiceMock.Object),
                new Lazy<IVpStatementService>(() => this.StatementService ?? this.StatementServiceMock.Object),
                new Lazy<IFinancePlanService>(() => this.FinancePlanService ?? this.FinancePlanServiceMock.Object),
                new Lazy<IGuarantorEnrollmentApplicationService>(() => this.GuarantorEnrollmentApplicationService ?? this.GuarantorEnrollmentApplicationServiceMock.Object),
                new Lazy<ISystemExceptionService>(() => this.SystemExceptionService ?? this.SystemExceptionServiceMock.Object),
                new Lazy<ISsoApplicationService>(() => this.SsoApplicationService ?? this.SsoApplicationServiceMock.Object),
                new Lazy<ISupportRequestService>(() => this.SupportRequestService ?? this.SupportRequestServiceMock.Object),
                new SessionContext<VisitPay>(this.Session ?? this.SessionMock.Object),
                new Lazy<IHsaBalanceProvider>(() => this.HsaBalanceProvider ?? this.HsaBalanceProviderMock.Object),
                new Lazy<IMetricsProvider>(() => this.MetricsProvider ?? this.MetricsProviderMock.Object),
                new Lazy<IHealthEquityEmployerIdResolutionService>(() => this.HealthEquityEmployerIdResolutionService ?? this.HealthEquityEmployerIdResolutionServiceMock.Object),
                new Lazy<IStatementDateService>(() => this.StatementDateService ?? this.StatementDateServiceMock.Object),
                new Lazy<IChangeEventApplicationService>(() => this.ChangeEventApplicationService ?? this.ChangeEventApplicationServiceMock.Object),
                new Lazy<IVpGuarantorHsMatchApiApplicationService>(() => this.VpGuarantorHsMatchApiApplicationService ?? this.VpGuarantorHsMatchApiApplicationServiceMock.Object)
            );
        }

        public GuarantorApplicationServiceMockBuilder Setup(Action<GuarantorApplicationServiceMockBuilder> configAction)
        {
            configAction(this);
            return this;
        }
    }
}
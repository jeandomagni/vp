namespace Ivh.Common.Tests.Helpers.Guarantor
{
    using Builders;
    using Domain.Guarantor.Entities;

    public class GuarantorFactory
    {
        public static Guarantor GenerateOnline()
        {
            Guarantor guar = new GuarantorBuilder().WithDefaults().AsOnline();

            return guar;
        }

        public static Guarantor GenerateOffline()
        {
            Guarantor guar = new GuarantorBuilder().WithDefaults().AsOffline();

            return guar;
        }
    }
}
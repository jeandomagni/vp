﻿namespace Ivh.Common.Tests.Helpers.Matching
{
    using System;
    using Ivh.Domain.Matching.Interfaces;
    using Ivh.Application.Matching;
    using Base;
    using Domain.Logging.Interfaces;
    using Moq;

    public class MatchingApplicationServiceMockBuilder : IMockBuilder<MatchingApplicationService, MatchingApplicationServiceMockBuilder>
    {
        public ApplicationServiceCommonServiceMockBuilder ApplicationServiceCommonServiceMockBuilder;
        public IMatchingService MatchingService;
        public IInitialMatchService InitialMatchService;
        public IMetricsProvider MetricsProvider;
        public IMatchReconciliationService MatchReconciliationService;

        public Mock<IMatchingService> MatchingServiceMock;
        public Mock<IInitialMatchService> InitialMatchServiceMock;
        public Mock<IMetricsProvider> MetricsProviderMock;
        public Mock<IMatchReconciliationService> MatchReconciliationServiceMock;

        public MatchingApplicationServiceMockBuilder()
        {
            this.ApplicationServiceCommonServiceMockBuilder = new ApplicationServiceCommonServiceMockBuilder();
            this.MatchingServiceMock = new Mock<IMatchingService>();
            this.InitialMatchServiceMock = new Mock<IInitialMatchService>();
            this.MetricsProviderMock = new Mock<IMetricsProvider>();
        }

        public MatchingApplicationService CreateService()
        {
            return new MatchingApplicationService(
                this.ApplicationServiceCommonServiceMockBuilder.CreateServiceLazy(),
                new Lazy<IMatchingService>(() => this.MatchingService ?? this.MatchingServiceMock.Object),
                new Lazy<IInitialMatchService>(() => this.InitialMatchService ?? this.InitialMatchServiceMock.Object),
                new Lazy<IMatchReconciliationService>(() => this.MatchReconciliationService ?? this.MatchReconciliationServiceMock.Object),
                new Lazy<IMetricsProvider>(() => this.MetricsProvider ?? this.MetricsProviderMock.Object)
            );
        }

        //implicitly build the service when assigning the builder to service type
        public static implicit operator MatchingApplicationService(MatchingApplicationServiceMockBuilder instance)
        {
            return instance.CreateService();
        }

        public MatchingApplicationServiceMockBuilder Setup(Action<MatchingApplicationServiceMockBuilder> configAction)
        {
            configAction(this);
            return this;
        }
    }
}
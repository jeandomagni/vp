﻿namespace Ivh.Common.Tests.Helpers.Matching
{
    using System;
    using Ivh.Domain.Matching.Interfaces;
    using NHibernate;
    using Ivh.Domain.Matching.Services;
    using Base;
    using Data.Connection;
    using Data.Connection.Connections;
    using Domain.Logging.Interfaces;
    using Moq;

    public class MatchingServiceMockBuilder : IMockBuilder<IMatchingService, MatchingServiceMockBuilder>
    {
        public DomainServiceCommonServiceMockBuilder DomainServiceCommonServiceMockBuilder;
        public IMatchRegistryRepository MatchRegistryRepository;
        public IMatchGuarantorRepository MatchGuarantorRepository;
        public IMatchVpGuarantorMatchInfoRepository MatchVpGuarantorMatchInfoRepository;
        public IMetricsProvider MetricsProvider;
        public ISession Session;
        public IStatelessSession StatelessSession;

        public Mock<IMatchRegistryRepository> MatchRegistryRepositoryMock;
        public Mock<IMatchGuarantorRepository> MatchGuarantorRepositoryMock;
        public Mock<IMatchVpGuarantorMatchInfoRepository> MatchVpGuarantorMatchInfoRepositoryMock;
        public Mock<IMetricsProvider> MetricsProviderMock;
        public Mock<ISession> SessionMock;
        public Mock<IStatelessSession> StatelessSessionMock;

        public MatchingServiceMockBuilder()
        {
            this.DomainServiceCommonServiceMockBuilder = new DomainServiceCommonServiceMockBuilder();
            this.MatchRegistryRepositoryMock = new Mock<IMatchRegistryRepository>();
            this.MatchGuarantorRepositoryMock = new Mock<IMatchGuarantorRepository>();
            this.MatchVpGuarantorMatchInfoRepositoryMock = new Mock<IMatchVpGuarantorMatchInfoRepository>();
            this.MetricsProviderMock = new Mock<IMetricsProvider>();
            this.SessionMock = new SessionMockBuilder().SessionMock;
            this.StatelessSessionMock = new StatelessSessionMockBuilder().StatelessSessionMock;
        }

        public IMatchingService CreateService()
        {
            return new MatchingService(
                this.DomainServiceCommonServiceMockBuilder.CreateServiceLazy(),
                new Lazy<IMatchRegistryRepository>(() => this.MatchRegistryRepository ?? this.MatchRegistryRepositoryMock.Object),
                new Lazy<IMatchGuarantorRepository>(() => this.MatchGuarantorRepository ?? this.MatchGuarantorRepositoryMock.Object),
                new Lazy<IMatchVpGuarantorMatchInfoRepository>(() => this.MatchVpGuarantorMatchInfoRepository ?? this.MatchVpGuarantorMatchInfoRepositoryMock.Object),
                new Lazy<IMetricsProvider>(() => this.MetricsProvider ?? this.MetricsProviderMock.Object),
                new StatelessSessionContext<VisitPay>(this.StatelessSession ?? this.StatelessSessionMock.Object),
                new SessionContext<VisitPay>(this.Session ?? this.SessionMock.Object)
            );
        }

        public MatchingServiceMockBuilder Setup(Action<MatchingServiceMockBuilder> configAction)
        {
            configAction(this);
            return this;
        }
    }
}

﻿namespace Ivh.Common.Tests.Helpers.Matching
{
    using System.Collections.Generic;
    using Builders.Matching;
    using Domain.Matching.Match;
    using VisitPay.Enums;

    public class MatchingTestCases
    {
        public static IEnumerable<MatchConfigTestCase> MatchConfigurationTestCases()
        {
            yield return new MatchConfigTestCase()
            {
                Client = "StLukes and Inova",
                Config = new List<Set>
                {
                    GuarantorMatchingSetBuilder.VisitPay(PatternSetEnum.S001),
                    GuarantorMatchingSetBuilder.GuestPay(),
                }
            };
            yield return new MatchConfigTestCase()
            {
                Client = "Intermountain",
                Config = new List<Set>
                {
                    GuarantorMatchingSetBuilder.VisitPay(PatternSetEnum.S002),
                    GuarantorMatchingSetBuilder.GuestPay(),
                }
            };
            yield return new MatchConfigTestCase()
            {
                Client = "Methodist",
                Config = new List<Set>
                {
                    GuarantorMatchingSetBuilder.VisitPay(PatternSetEnum.S003),
                    GuarantorMatchingSetBuilder.GuestPay(),
                }
            };
            yield return new MatchConfigTestCase()
            {
                Client = "R1 Waco",
                Config = new List<Set>
                {
                    GuarantorMatchingSetBuilder.VisitPay(PatternSetEnum.S003, MatchSetConfigurationRegexEnum.RemoveMinistryPrefix),
                    GuarantorMatchingSetBuilder.GuestPay(MatchSetConfigurationRegexEnum.RemoveMinistryPrefix),
                }
            };
            yield return new MatchConfigTestCase()
            {
                Client = "THR",
                Config = new List<Set>
                {
                    GuarantorMatchingSetBuilder.VisitPay(PatternSetEnum.S005),
                    GuarantorMatchingSetBuilder.GuestPay(),
                }
            };

        }

        public class MatchConfigTestCase
        {
            public string Client { get; set; }
            public List<Set> Config { get; set; }

            public MatchConfigTestCase()
            {
            }

            public MatchConfigTestCase(string client, List<Set> config)
            {
                this.Config = config;
                this.Client = client;
            }

            public override string ToString()
            {
                return this.Client;
            }
        }

    }
}

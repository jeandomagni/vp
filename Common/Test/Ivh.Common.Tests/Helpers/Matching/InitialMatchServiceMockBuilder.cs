﻿namespace Ivh.Common.Tests.Helpers.Matching
{
    using System;
    using Base;
    using Domain.Matching.Interfaces;
    using Domain.Matching.Services;
    using Moq;

    public class InitialMatchServiceMockBuilder : IMockBuilder<IInitialMatchService, InitialMatchServiceMockBuilder>
    {
        public DomainServiceCommonServiceMockBuilder DomainServiceCommonServiceMockBuilder;
        public IMatchRegistryRepository MatchRegistryRepository;
        public IMatchVpGuarantorMatchInfoRepository MatchVpGuarantorMatchInfoRepository;

        public Mock<IMatchRegistryRepository> MatchRegistryRepositoryMock;
        public Mock<IMatchVpGuarantorMatchInfoRepository> MatchVpGuarantorMatchInfoRepositoryMock;

        public InitialMatchServiceMockBuilder()
        {
            this.DomainServiceCommonServiceMockBuilder = new DomainServiceCommonServiceMockBuilder();
            this.MatchRegistryRepositoryMock = new Mock<IMatchRegistryRepository>();
            this.MatchVpGuarantorMatchInfoRepositoryMock = new Mock<IMatchVpGuarantorMatchInfoRepository>();
        }

        public IInitialMatchService CreateService()
        {
            return new InitialMatchService(
                new Lazy<IMatchRegistryRepository>(() => this.MatchRegistryRepository ?? this.MatchRegistryRepositoryMock.Object),
                new Lazy<IMatchVpGuarantorMatchInfoRepository>(() => this.MatchVpGuarantorMatchInfoRepository ?? this.MatchVpGuarantorMatchInfoRepositoryMock.Object),
                this.DomainServiceCommonServiceMockBuilder.CreateServiceLazy()
            );
        }

        public InitialMatchServiceMockBuilder Setup(Action<InitialMatchServiceMockBuilder> configAction)
        {
            configAction(this);
            return this;
        }
    }
}

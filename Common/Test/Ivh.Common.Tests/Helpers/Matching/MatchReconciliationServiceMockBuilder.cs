﻿namespace Ivh.Common.Tests.Helpers.Matching
{
    using System;
    using Base;
    using Data.Connection;
    using Data.Connection.Connections;
    using Domain.Logging.Interfaces;
    using Domain.Matching.Interfaces;
    using Domain.Matching.Services;
    using Moq;
    using NHibernate;

    public class MatchReconciliationServiceMockBuilder : IMockBuilder<IMatchReconciliationService, MatchReconciliationServiceMockBuilder>
    {
        public DomainServiceCommonServiceMockBuilder DomainServiceCommonServiceMockBuilder;
        public IMatchRegistryRepository MatchRegistryRepository;
        public IMatchReconciliationRepository MatchReconciliationRepository;
        public IMatchVpGuarantorMatchInfoRepository MatchVpGuarantorMatchInfoRepository;
        public IMetricsProvider MetricsProvider;
        public ISession Session;

        public Mock<IMatchRegistryRepository> MatchRegistryRepositoryMock;
        public Mock<IMatchReconciliationRepository> MatchReconciliationRepositoryMock;
        public Mock<IMatchVpGuarantorMatchInfoRepository> MatchVpGuarantorMatchInfoRepositoryMock;
        public Mock<IMetricsProvider> MetricsProviderMock;
        public Mock<ISession> SessionMock;

        public MatchReconciliationServiceMockBuilder()
        {
            this.DomainServiceCommonServiceMockBuilder = new DomainServiceCommonServiceMockBuilder();
            this.MatchRegistryRepositoryMock = new Mock<IMatchRegistryRepository>();
            this.MatchReconciliationRepositoryMock = new Mock<IMatchReconciliationRepository>();
            this.MatchVpGuarantorMatchInfoRepositoryMock = new Mock<IMatchVpGuarantorMatchInfoRepository>();
            this.MetricsProviderMock = new Mock<IMetricsProvider>();
            this.SessionMock = new SessionMockBuilder().SessionMock;
        }

        public IMatchReconciliationService CreateService()
        {
            return new MatchReconciliationService(
                this.DomainServiceCommonServiceMockBuilder.CreateServiceLazy(),
                new Lazy<IMatchReconciliationRepository>(() => this.MatchReconciliationRepository ?? this.MatchReconciliationRepositoryMock.Object),
                new Lazy<IMatchVpGuarantorMatchInfoRepository>(() => this.MatchVpGuarantorMatchInfoRepository ?? this.MatchVpGuarantorMatchInfoRepositoryMock.Object),
                new Lazy<IMatchRegistryRepository>(() => this.MatchRegistryRepository ?? this.MatchRegistryRepositoryMock.Object),
                new Lazy<IMetricsProvider>(() => this.MetricsProvider ?? this.MetricsProviderMock.Object),
                new SessionContext<VisitPay>(this.Session ?? this.SessionMock.Object)
            );
        }

        public MatchReconciliationServiceMockBuilder Setup(Action<MatchReconciliationServiceMockBuilder> configAction)
        {
            configAction(this);
            return this;
        }
    }
}

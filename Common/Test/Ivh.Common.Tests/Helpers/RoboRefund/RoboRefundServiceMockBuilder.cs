﻿namespace Ivh.Common.Tests.Helpers.RoboRefund
{
    using System;
    using System.Data;
    using Ivh.Domain.FinanceManagement.RoboRefund.Interfaces;
	using Ivh.Domain.FinanceManagement.FinancePlan.Interfaces;
	using Ivh.Domain.FinanceManagement.Statement.Interfaces;
	using Ivh.Domain.FinanceManagement.RoboRefund.Services;
    using Moq;

    public class RoboRefundServiceMockBuilder : IMockBuilder<IRoboRefundService, RoboRefundServiceMockBuilder>
    {
        public IRoboRefundRepository RoboRefundRepository;
		public IFinancePlanRepository FinancePlanRepository;
		public IFinancePlanService FinancePlanService;
		public IVpStatementRepository VpStatementRepository;
        public Mock<IRoboRefundRepository> RoboRefundRepositoryMock;
		public Mock<IFinancePlanRepository> FinancePlanRepositoryMock;
		public Mock<IFinancePlanService> FinancePlanServiceMock;
		public Mock<IVpStatementRepository> VpStatementRepositoryMock;

        public RoboRefundServiceMockBuilder()
        {
            this.RoboRefundRepositoryMock = new Mock<IRoboRefundRepository>();
			this.FinancePlanRepositoryMock = new Mock<IFinancePlanRepository>();
			this.FinancePlanServiceMock = new Mock<IFinancePlanService>();
			this.VpStatementRepositoryMock = new Mock<IVpStatementRepository>();
        }
        
        public IRoboRefundService CreateService()
        {
            return new RoboRefundService(
                new Lazy<IRoboRefundRepository>(() => this.RoboRefundRepository ?? this.RoboRefundRepositoryMock.Object),
				new Lazy<IFinancePlanRepository>(() => this.FinancePlanRepository ?? this.FinancePlanRepositoryMock.Object),
				new Lazy<IFinancePlanService>(() => this.FinancePlanService ?? this.FinancePlanServiceMock.Object),
				new Lazy<IVpStatementRepository>(() => this.VpStatementRepository ?? this.VpStatementRepositoryMock.Object)
            ); 
         }
        
        public RoboRefundServiceMockBuilder Setup(Action<RoboRefundServiceMockBuilder> configAction)
        {
            configAction(this);
            return this;
        }
    }
}
﻿namespace Ivh.Common.Tests.Helpers.VpStatement
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Common.Base.Constants;
    using Common.Base.Enums;
    using Domain.Visit.Entities;
    using Domain.FinanceManagement.Statement.Entities;
    using VisitPay.Enums;

    public class VpStatementFactory
    {
        private static int _vpStatementIdCounter = 1;

        public static VpStatementVisit GenerateVpStatementVisit(Tuple<Visit, IList<VisitTransaction>> visitAndTransactions, VpStatement statement)
        {
            if (visitAndTransactions.Item1 == null)
            {
                throw new Exception("Proper arguments were not supplied.");                
            }

            VpStatementVisit vpStatementVisit = new VpStatementVisit()
            {
                Visit = visitAndTransactions.Item1,
                VpStatement = statement,
                StatementedSnapshotVisitState = visitAndTransactions.Item1.CurrentVisitState
            };

            statement.VpStatementVisits.Add(vpStatementVisit);

            return vpStatementVisit;
        }

        public static void AddVisitsToStatement(IList<Tuple<Visit, IList<VisitTransaction>>> visitsAndTransactionsToUse, VpStatement statement)
        {
            //Add these to the statement as StatementVisits
            foreach (Tuple<Visit, IList<VisitTransaction>> visitAndTransactionsToUse in visitsAndTransactionsToUse)
            {
                GenerateVpStatementVisit(visitAndTransactionsToUse, statement);
            }
        }

        public static VpStatement GenerateStatementPaymentDueDateInXDays(int days)
        {
            VpStatement statement = new VpStatement()
            {
                VpStatementId = _vpStatementIdCounter,
                PaymentDueDate = DateTime.UtcNow.AddDays(days),
                PeriodEndDate = DateTime.UtcNow.AddDays(1),//Might make the statement a little screwy, but gives us an SCVB
                StatementVersion = VpStatementVersionEnum.Vp2,
            };
            _vpStatementIdCounter++;
            
            return statement;
        }

        public static VpStatement GenerateStatementPaymentDueDate(DateTime paymentDueDate)
        {
            VpStatement statement = new VpStatement()
            {
                VpStatementId = _vpStatementIdCounter,
                PaymentDueDate = paymentDueDate.Date,
                PeriodEndDate = paymentDueDate.AddDays(1),
                StatementVersion = VpStatementVersionEnum.Vp2,
            };

            _vpStatementIdCounter++;

            return statement;
        }

        public static VpStatement CreateStatement(List<Tuple<Visit, IList<VisitTransaction>>> visits)
        {
            VpStatement current = new VpStatement
            {
                VpStatementType = VpStatementTypeEnum.Standard,
                StatementVersion = VpStatementVersionEnum.Vp2
            };

            foreach (Visit visit in visits.Select(x => x.Item1))
            {
                VpStatementVisit vpStatementVisit = new VpStatementVisit()
                {
                    StatementedSnapshotVisitState = visit.CurrentVisitState,
                    Visit = visit,
                    VpStatement = current,
                };
                ((VpStatementVisitSnapshotVp2) vpStatementVisit.GetSnapshotObject()).StatementedSnapshotAgingCount = visit.AgingCount;
                ((VpStatementVisitSnapshotVp2) vpStatementVisit.GetSnapshotObject()).StatementedSnapshotVisitBalance = visit.CurrentBalance;
                current.VpStatementVisits.Add(vpStatementVisit);
                
            }

            if (visits.Any())
            {
                DateTime? dateTimeOffset = visits.First().Item1.InsertDate;
                if (dateTimeOffset != null)
                {
                    current.PeriodEndDate = dateTimeOffset.Value.AddDays(1);
                }
            }

            return current;
        }
    }
}
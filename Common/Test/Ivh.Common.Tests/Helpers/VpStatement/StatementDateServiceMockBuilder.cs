﻿namespace Ivh.Common.Tests.Helpers.VpStatement
{
    using System;
    using Base;
    using Domain.FinanceManagement.Statement.Interfaces;
    using Domain.FinanceManagement.Statement.Services;
    using Domain.Settings.Entities;
    using Moq;

    public class StatementDateServiceMockBuilder : IMockBuilder<IStatementDateService, StatementDateServiceMockBuilder>
    {
        public DomainServiceCommonServiceMockBuilder DomainServiceCommonServiceMockBuilder;
        

        public StatementDateServiceMockBuilder()
        {
            this.DomainServiceCommonServiceMockBuilder = new DomainServiceCommonServiceMockBuilder();
        }
        
        public IStatementDateService CreateService()
        {
            this.DefaultClientSetup();
            return new StatementDateService(
                this.DomainServiceCommonServiceMockBuilder.CreateServiceLazy()
            );
        }
        
        public StatementDateServiceMockBuilder Setup(Action<StatementDateServiceMockBuilder> configAction)
        {
            configAction(this);
            return this;
        }

        public void DefaultClientSetup()
        {
            Mock<Client> clientMock = new Mock<Client>();
            clientMock.Setup(t => t.StatementGenerationDays).Returns(2);
            clientMock.Setup(t => t.GracePeriodLength).Returns(21);
            this.DomainServiceCommonServiceMockBuilder.ClientServiceMock.Setup(t => t.GetClient()).Returns(clientMock.Object);
        }
    }
}
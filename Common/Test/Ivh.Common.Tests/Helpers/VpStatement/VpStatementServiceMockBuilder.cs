﻿namespace Ivh.Common.Tests.Helpers.VpStatement
{
    using System;
    using Base;
    using Domain.Base.Interfaces;
    using Domain.Guarantor.Interfaces;
    using Domain.Visit.Interfaces;
    using Domain.FinanceManagement.FinancePlan.Interfaces;
    using Domain.FinanceManagement.Interfaces;
    using Domain.FinanceManagement.Payment.Interfaces;
    using Domain.FinanceManagement.Statement.Interfaces;
    using Domain.FinanceManagement.Statement.Services;
    using Domain.Settings.Entities;
    using Domain.Settings.Interfaces;
    using Moq;
    using NHibernate;
    using ServiceBus.Interfaces;

    public class VpStatementServiceMockBuilder
    {
        public IFinancePlanService FinancePlanService;
        public IDomainServiceCommonService DomainServiceCommonService;

        public Mock<IAmortizationService> AmortizationServiceMock;
        public Mock<IFinancePlanStatementRepository> FinancePlanStatementRepositoryMock;
        public Mock<IVpStatementRepository> VpStatementRepositoryMock;
        public Mock<IVisitService> VisitServiceMock;
        public Mock<IVisitRepository> VisitRepositoryMock;
        public Mock<ISession> SessionMock;
        public Mock<IClientService> ClientServiceMock;
        public Mock<IFinancePlanService> FinancePlanServiceMock;
        public Mock<IPaymentAllocationRepository> PaymentAllocationRepositoryMock;
        public Mock<IPaymentRepository> PaymentRepositoryMock;
        public Mock<IPaymentService> PaymentServiceMock;
        public IVisitTransactionService VisitTransactionService;
        public Mock<IVisitTransactionRepository> VisitTransactionRepositoryMock;
        public Mock<IVisitTransactionWriteRepository> VisitTransactionWriteRepositoryMock;
        public Mock<IGuarantorService> GuarantorServiceMock;
        public IStatementDateService StatementDateService;
        public Mock<IStatementDateService> StatementDateServiceMock;
        public Mock<IBus> BusMock;

        public VpStatementServiceMockBuilder()
        {
            this.AmortizationServiceMock = new Mock<IAmortizationService>();
            this.FinancePlanStatementRepositoryMock = new Mock<IFinancePlanStatementRepository>();
            this.FinancePlanServiceMock = new Mock<IFinancePlanService>();
            this.VpStatementRepositoryMock = new Mock<IVpStatementRepository>();
            this.VisitServiceMock = new Mock<IVisitService>();
            this.VisitRepositoryMock = new Mock<IVisitRepository>();
            this.PaymentRepositoryMock = new Mock<IPaymentRepository>();
            this.PaymentAllocationRepositoryMock = new Mock<IPaymentAllocationRepository>();
            this.VisitTransactionRepositoryMock = new Mock<IVisitTransactionRepository>();
            this.VisitTransactionWriteRepositoryMock = new Mock<IVisitTransactionWriteRepository>();
            this.ClientServiceMock = new Mock<IClientService>();
            this.GuarantorServiceMock = new Mock<IGuarantorService>();
            this.StatementDateServiceMock = new Mock<IStatementDateService>();
            this.BusMock = new Mock<IBus>();

            this.SessionMock = new Mock<ISession>();
            Mock<ITransaction> transactionMock = new Mock<ITransaction>();
            transactionMock.SetupGet(x => x.IsActive).Returns(true);
            this.SessionMock.SetupGet(x => x.Transaction).Returns(transactionMock.Object);
            this.SessionMock.Setup(x => x.BeginTransaction()).Returns(transactionMock.Object);

            Mock<Client> clientMock = new Mock<Client>();
            clientMock.Setup(t => t.StatementGenerationDays).Returns(2);
            clientMock.Setup(t => t.GracePeriodLength).Returns(21);
            this.ClientServiceMock.Setup(t => t.GetClient()).Returns(clientMock.Object);

            this.DomainServiceCommonService = DomainServiceCommonServices.CreateDomainServiceCommonService(
                applicationSettingsServiceMock: null,
                featureServiceMock: null,
                clientServiceMock: this.ClientServiceMock,
                loggingServiceMock: null,
                busMock: this.BusMock);

            this.VisitTransactionService = new Ivh.Domain.Visit.Services.VisitTransactionService(
                new Lazy<IDomainServiceCommonService>(() => this.DomainServiceCommonService),
                new Lazy<IVisitTransactionRepository>(() => this.VisitTransactionRepositoryMock.Object),
                new Lazy<IVisitTransactionWriteRepository>(() => this.VisitTransactionWriteRepositoryMock.Object),
                new Lazy<IVisitRepository>(() => this.VisitRepositoryMock.Object)
                );
        }

        public VpStatementService GetMockedVpStatementService()
        {
            return new VpStatementService(
                new Lazy<IDomainServiceCommonService>(() => this.DomainServiceCommonService),
                new Lazy<IFinancePlanStatementRepository>(() => this.FinancePlanStatementRepositoryMock.Object), 
                new Lazy<IVpStatementRepository>(() => this.VpStatementRepositoryMock.Object),
                new Lazy<IGuarantorService>(() => this.GuarantorServiceMock.Object),
                new Lazy<IFinancePlanService>(() => this.FinancePlanService ?? this.FinancePlanServiceMock.Object),
                new Lazy<IStatementDateService>(() => this.StatementDateService ?? this.StatementDateServiceMock.Object)
                );
        }

        public VpStatementServiceMockBuilder Setup(Action<VpStatementServiceMockBuilder> configAction)
        {
            configAction(this);
            return this;
        }
    }
}
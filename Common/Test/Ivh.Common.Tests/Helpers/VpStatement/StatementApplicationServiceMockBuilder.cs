﻿namespace Ivh.Common.Tests.Helpers.VpStatement
{
    using System;
    using System.Data;
    using Application.Core.Common.Interfaces;
    using Application.FinanceManagement.Common.Interfaces;
    using Ivh.Application.Base.Common.Interfaces;
	using Ivh.Domain.FinanceManagement.Statement.Interfaces;
	using Ivh.Domain.Guarantor.Interfaces;
	using Ivh.Domain.FinanceManagement.Statement.Services;
	using Ivh.Domain.Visit.Interfaces;
	using Ivh.Application.FinanceManagement.ApplicationServices;
	using Base;
    using Moq;

    public class StatementApplicationServiceMockBuilder : IMockBuilder<IStatementApplicationService, StatementApplicationServiceMockBuilder>
    {
        public ApplicationServiceCommonServiceMockBuilder ApplicationServiceCommonServiceMockBuilder;
        public IVpStatementService VpStatementService;
        public IGuarantorService GuarantorService;
        public IVisitService VisitService;
        public IVisitApplicationService VisitApplicationService;
        public IStatementDateService StatementDateService;
        public IFinancePlanStatementService FinancePlanStatementService;
        public IServiceGroupApplicationService ServiceGroupApplicationService;

		public Mock<IVpStatementService> VpStatementServiceMock;
		public Mock<IGuarantorService> GuarantorServiceMock;
        public Mock<IVisitService> VisitServiceMock;
        public Mock<IVisitApplicationService> VisitApplicationServiceMock;
        public Mock<IStatementDateService> StatementDateServiceMock;
		public Mock<IFinancePlanStatementService> FinancePlanStatementServiceMock;
        public Mock<IServiceGroupApplicationService> ServiceGroupApplicationServiceMock;

        public StatementApplicationServiceMockBuilder()
        {
            this.ApplicationServiceCommonServiceMockBuilder = new ApplicationServiceCommonServiceMockBuilder();
            this.VpStatementServiceMock = new Mock<IVpStatementService>();
            this.GuarantorServiceMock = new Mock<IGuarantorService>();
            this.VisitServiceMock = new Mock<IVisitService>();
            this.VisitApplicationServiceMock = new Mock<IVisitApplicationService>();
			this.VpStatementServiceMock = new Mock<IVpStatementService>();
			this.GuarantorServiceMock = new Mock<IGuarantorService>();
			this.StatementDateServiceMock = new Mock<IStatementDateService>();
			this.VisitServiceMock = new Mock<IVisitService>();
			this.FinancePlanStatementServiceMock = new Mock<IFinancePlanStatementService>();
            this.ServiceGroupApplicationServiceMock = new Mock<IServiceGroupApplicationService>();
        }
        
        public IStatementApplicationService CreateService()
        {
            return new StatementApplicationService(
                this.ApplicationServiceCommonServiceMockBuilder.CreateServiceLazy(),
                new Lazy<IVpStatementService>(() => this.VpStatementService ?? this.VpStatementServiceMock.Object),
                new Lazy<IGuarantorService>(() => this.GuarantorService ?? this.GuarantorServiceMock.Object),
                new Lazy<IVisitService>(() => this.VisitService ?? this.VisitServiceMock.Object),
                new Lazy<IVisitApplicationService>(() => this.VisitApplicationService ?? this.VisitApplicationServiceMock.Object),
				new Lazy<IStatementDateService>(() => this.StatementDateService ?? this.StatementDateServiceMock.Object),
				new Lazy<IFinancePlanStatementService>(() => this.FinancePlanStatementService ?? this.FinancePlanStatementServiceMock.Object),
                new Lazy<IServiceGroupApplicationService>(() => this.ServiceGroupApplicationService ?? this.ServiceGroupApplicationServiceMock.Object)
            );
        }

        
        public StatementApplicationServiceMockBuilder Setup(Action<StatementApplicationServiceMockBuilder> configAction)
        {
            configAction(this);
            return this;
        }
    }
}
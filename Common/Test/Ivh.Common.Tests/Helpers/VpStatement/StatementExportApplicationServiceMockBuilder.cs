﻿namespace Ivh.Common.Tests.Helpers.VpStatement
{
    using System;
    using Application.Content.Common.Interfaces;
    using Application.Core.Common.Interfaces;
    using Application.FinanceManagement.ApplicationServices;
    using Application.FinanceManagement.Common.Interfaces;
    using Base;
    using Data.Connection;
    using Data.Connection.Connections;
    using Domain.FileStorage.Interfaces;
    using Domain.FinanceManagement.Statement.Interfaces;
    using Domain.Settings.Interfaces;
    using Moq;
    using NHibernate;

    public class StatementExportApplicationServiceMockBuilder : IMockBuilder<IStatementExportApplicationService, StatementExportApplicationServiceMockBuilder>
    {
        public ApplicationServiceCommonServiceMockBuilder ApplicationServiceCommonServiceMockBuilder;
        public IApplicationSettingsService ApplicationSettingsService;
        public IContentApplicationService ContentApplicationService;
        public IGuarantorApplicationService GuarantorApplicationService;
        public ISession Session;
        public IStatementExportService StatementExportService;
        public IVpStatementService VpStatementService;
        public IFinancePlanApplicationService FinancePlanApplicationService;
        public IClientApplicationService ClientApplicationService;
        public IStatementApplicationService StatementApplicationService;
        public IVisitTransactionApplicationService VisitTransactionApplicationService;
        public IFinancePlanStatementService FinancePlanStatementService;
        public IVpStatementDocumentService StatementDocumentService;
        
        public Mock<IApplicationSettingsService> ApplicationSettingsServiceMock;
        public Mock<IContentApplicationService> ContentApplicationServiceMock;
        public Mock<IGuarantorApplicationService> GuarantorApplicationServiceMock;
        public Mock<ISession> SessionMock;
        public Mock<IStatementExportService> StatementExportServiceMock;
        public Mock<IVpStatementService> VpStatementServiceMock;
        public Mock<IFinancePlanApplicationService> FinancePlanApplicationServiceMock;
        public Mock<IClientApplicationService> ClientApplicationServiceMock;
        public Mock<IStatementApplicationService> StatementApplicationServiceMock;
        public Mock<IVisitTransactionApplicationService> VisitTransactionApplicationServiceMock;
        public Mock<IFinancePlanStatementService> FinancePlanStatementServiceMock;
        public Mock<IVpStatementDocumentService> StatementDocumentServiceMock;

        public StatementExportApplicationServiceMockBuilder()
        {
            this.ApplicationServiceCommonServiceMockBuilder = new ApplicationServiceCommonServiceMockBuilder();
            this.ContentApplicationServiceMock = new Mock<IContentApplicationService>();
            this.GuarantorApplicationServiceMock = new Mock<IGuarantorApplicationService>();
            this.SessionMock = new SessionMockBuilder().SessionMock;
            this.StatementExportServiceMock = new Mock<IStatementExportService>();
            this.VpStatementServiceMock = new Mock<IVpStatementService>();
            this.FinancePlanApplicationServiceMock = new Mock<IFinancePlanApplicationService>();
            this.ClientApplicationServiceMock = new Mock<IClientApplicationService>();
            this.StatementApplicationServiceMock = new Mock<IStatementApplicationService>();
            this.VisitTransactionApplicationServiceMock = new Mock<IVisitTransactionApplicationService>();
            this.FinancePlanStatementServiceMock = new Mock<IFinancePlanStatementService>();
            this.StatementDocumentServiceMock = new Mock<IVpStatementDocumentService>();
        }

        public IStatementExportApplicationService CreateService()
        {
            return new StatementExportApplicationService(
                this.ApplicationServiceCommonServiceMockBuilder.CreateServiceLazy(),
                new Lazy<IApplicationSettingsService>(() => this.ApplicationSettingsService ?? this.ApplicationSettingsServiceMock.Object),
                new Lazy<IContentApplicationService>(() => this.ContentApplicationService ?? this.ContentApplicationServiceMock.Object),
                new Lazy<IGuarantorApplicationService>(() => this.GuarantorApplicationService ?? this.GuarantorApplicationServiceMock.Object),
                new SessionContext<VisitPay>(this.Session ?? this.SessionMock.Object),
                new Lazy<IStatementExportService>(() => this.StatementExportService ?? this.StatementExportServiceMock.Object),
                new Lazy<IVpStatementService>(() => this.VpStatementService ?? this.VpStatementServiceMock.Object),
                new Lazy<IFinancePlanApplicationService>(() => this.FinancePlanApplicationService ?? this.FinancePlanApplicationServiceMock.Object),
                new Lazy<IClientApplicationService>(() => this.ClientApplicationService ?? this.ClientApplicationServiceMock.Object),
                new Lazy<IStatementApplicationService>(() => this.StatementApplicationService ?? this.StatementApplicationServiceMock.Object),
                new Lazy<IVisitTransactionApplicationService>(() => this.VisitTransactionApplicationService ?? this.VisitTransactionApplicationServiceMock.Object),
                new Lazy<IFinancePlanStatementService>(() => this.FinancePlanStatementService ?? this.FinancePlanStatementServiceMock.Object),
                new Lazy<IVpStatementDocumentService>(() => this.StatementDocumentService ?? this.StatementDocumentServiceMock.Object)
            );
        }

        public StatementExportApplicationServiceMockBuilder Setup(Action<StatementExportApplicationServiceMockBuilder> configAction)
        {
            configAction(this);
            return this;
        }
    }
}
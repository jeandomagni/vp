﻿namespace Ivh.Common.Tests.Helpers.PaymentMethod
{
    using System;
    using Application.Base.Common.Interfaces;
    using Application.Content.Common.Interfaces;
    using Application.Core.Common.Interfaces;
    using Application.FinanceManagement.ApplicationServices;
    using Base;
    using Data.Connection;
    using Data.Connection.Connections;
    using Domain.FinanceManagement.FinancePlan.Interfaces;
    using Domain.FinanceManagement.Interfaces;
    using Domain.Guarantor.Interfaces;
    using Moq;
    using NHibernate;

    public class PaymentMethodsApplicationServiceMockBuilder : IMockBuilder<PaymentMethodsApplicationService, PaymentMethodsApplicationServiceMockBuilder>
    {
        public ApplicationServiceCommonServiceMockBuilder ApplicationServiceCommonServiceMockBuilder;
        public IContentApplicationService ContentApplicationService;
        public Mock<IContentApplicationService> ContentApplicationServiceMock;
        public IFinancePlanService FinancePlanService;
        public Mock<IFinancePlanService> FinancePlanServiceMock;
        public IGuarantorService GuarantorService;
        public Mock<IGuarantorService> GuarantorServiceMock;
        public IPaymentMethodAccountTypeService PaymentMethodAccountTypeService;
        public Mock<IPaymentMethodAccountTypeService> PaymentMethodAccountTypeServiceMock;
        public IPaymentMethodProviderTypeService PaymentMethodProviderTypeService;
        public Mock<IPaymentMethodProviderTypeService> PaymentMethodProviderTypeServiceMock;
        public IPaymentMethodsService PaymentMethodsService;

        public Mock<IPaymentMethodsService> PaymentMethodsServiceMock;
        public IPaymentProvider PaymentProvider;
        public Mock<IPaymentProvider> PaymentProviderMock;
        public ISession Session;
        public Mock<ISession> SessionMock;
        public ISsoApplicationService SsoApplicationService;
        public Mock<ISsoApplicationService> SsoApplicationServiceMock;
        public IVisitBalanceBaseApplicationService VisitBalanceBaseApplicationService;
        public Mock<IVisitBalanceBaseApplicationService> VisitBalanceBaseApplicationServiceMock;
        public IVisitPayUserJournalEventApplicationService VisitPayUserJournalEventApplicationService;
        public Mock<IVisitPayUserJournalEventApplicationService> VisitPayUserJournalEventApplicationServiceMock;

        public PaymentMethodsApplicationServiceMockBuilder()
        {
            this.Session = new SessionMockBuilder().UseSynchronizations().CreateService();
            this.ApplicationServiceCommonServiceMockBuilder = new ApplicationServiceCommonServiceMockBuilder();

            this.PaymentMethodsServiceMock = new Mock<IPaymentMethodsService>();
            this.PaymentMethodAccountTypeServiceMock = new Mock<IPaymentMethodAccountTypeService>();
            this.PaymentMethodProviderTypeServiceMock = new Mock<IPaymentMethodProviderTypeService>();
            this.PaymentProviderMock = new Mock<IPaymentProvider>();
            this.GuarantorServiceMock = new Mock<IGuarantorService>();
            this.VisitPayUserJournalEventApplicationServiceMock = new Mock<IVisitPayUserJournalEventApplicationService>();
            this.SsoApplicationServiceMock = new Mock<ISsoApplicationService>();
            this.FinancePlanServiceMock = new Mock<IFinancePlanService>();
            this.VisitBalanceBaseApplicationServiceMock = new Mock<IVisitBalanceBaseApplicationService>();
            this.ContentApplicationServiceMock = new Mock<IContentApplicationService>();
        }

        public PaymentMethodsApplicationService CreateService()
        {
            return new PaymentMethodsApplicationService(
                this.ApplicationServiceCommonServiceMockBuilder.CreateServiceLazy(),
                new Lazy<IPaymentMethodsService>(() => this.PaymentMethodsService ?? this.PaymentMethodsServiceMock.Object),
                new Lazy<IPaymentMethodAccountTypeService>(() => this.PaymentMethodAccountTypeService ?? this.PaymentMethodAccountTypeServiceMock.Object),
                new Lazy<IPaymentMethodProviderTypeService>(() => this.PaymentMethodProviderTypeService ?? this.PaymentMethodProviderTypeServiceMock.Object),
                new Lazy<IPaymentProvider>(() => this.PaymentProvider ?? this.PaymentProviderMock.Object),
                new Lazy<IGuarantorService>(() => this.GuarantorService ?? this.GuarantorServiceMock.Object),
                new Lazy<IVisitPayUserJournalEventApplicationService>(() => this.VisitPayUserJournalEventApplicationService ?? this.VisitPayUserJournalEventApplicationServiceMock.Object),
                new SessionContext<VisitPay>(this.Session),
                new Lazy<ISsoApplicationService>(() => this.SsoApplicationService ?? this.SsoApplicationServiceMock.Object),
                new Lazy<IFinancePlanService>(() => this.FinancePlanService ?? this.FinancePlanServiceMock.Object),
                new Lazy<IVisitBalanceBaseApplicationService>(() => this.VisitBalanceBaseApplicationService ?? this.VisitBalanceBaseApplicationServiceMock.Object),
                new Lazy<IContentApplicationService>(() => this.ContentApplicationService ?? this.ContentApplicationServiceMock.Object)
            );
        }

        public PaymentMethodsApplicationServiceMockBuilder Setup(Action<PaymentMethodsApplicationServiceMockBuilder> configAction)
        {
            configAction(this);
            return this;
        }
    }
}
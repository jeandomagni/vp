﻿namespace Ivh.Common.Tests.Helpers.PaymentMethod
{
    using System;
    using System.Collections.Generic;
    using Base;
    using Data.Connection;
    using Data.Connection.Connections;
    using Domain.Content.Interfaces;
    using Domain.FinanceManagement.Consolidation.Interfaces;
    using Domain.FinanceManagement.FinancePlan.Interfaces;
    using Domain.FinanceManagement.Interfaces;
    using Domain.FinanceManagement.Payment.Interfaces;
    using Domain.FinanceManagement.Services;
    using Domain.Guarantor.Interfaces;
    using Domain.Logging.Interfaces;
    using Domain.Settings.Interfaces;
    using Moq;
    using NHibernate;
    using VisitPay.Enums;

    public class PaymentMethodsServiceMockBuilder : IMockBuilder<PaymentMethodsService, PaymentMethodsServiceMockBuilder>
    {
        public ISession Session;
        public IPaymentMethodsRepository PaymentMethodsRepository;
        public IPaymentMethodEventRepository PaymentMethodEventRepository;
        public IPaymentMethodAccountTypeRepository PaymentMethodAccountTypeRepository;
        public IPaymentMethodProviderTypeRepository PaymentMethodProviderTypeRepository;
        public IFinancePlanRepository FinancePlanRepository;
        public IGuarantorService GuarantorService;
        public IPaymentConfigurationService PaymentConfigurationService;
        public IConsolidationGuarantorRepository ConsolidationGuarantorRepository;
        public IPaymentRepository PaymentRepository;
        public IPaymentProvider PaymentProvider;
        public IContentService ContentService;
        public IMetricsProvider MetricsProvider;

        public DomainServiceCommonServiceMockBuilder DomainServiceCommonServiceMockBuilder;    
        public Mock<IPaymentMethodsRepository> PaymentMethodsRepositoryMock;
        public Mock<IPaymentMethodEventRepository> PaymentMethodEventRepositoryMock;
        public Mock<IPaymentMethodAccountTypeRepository> PaymentMethodAccountTypeRepositoryMock;
        public Mock<IPaymentMethodProviderTypeRepository> PaymentMethodProviderTypeRepositoryMock;
        public Mock<IFinancePlanRepository> FinancePlanRepositoryMock;
        public Mock<IGuarantorService> GuarantorServiceMock;
        public Mock<IPaymentConfigurationService> PaymentConfigurationServiceMock;
        public Mock<IConsolidationGuarantorRepository> ConsolidationGuarantorRepositoryMock;
        public Mock<IPaymentRepository> PaymentRepositoryMock;
        public Mock<IPaymentProvider> PaymentProviderMock;
        public Mock<IContentService> ContentServiceMock;
        public Mock<IMetricsProvider> MetricsProviderMock;

        public PaymentMethodsServiceMockBuilder()
        {
            this.Session = new SessionMockBuilder().UseSynchronizations().CreateService();
            
            this.DomainServiceCommonServiceMockBuilder = new DomainServiceCommonServiceMockBuilder();
            this.PaymentMethodsRepositoryMock = new Mock<IPaymentMethodsRepository>();
            this.PaymentMethodEventRepositoryMock = new Mock<IPaymentMethodEventRepository>();
            this.PaymentMethodAccountTypeRepositoryMock = new Mock<IPaymentMethodAccountTypeRepository>();
            this.PaymentMethodProviderTypeRepositoryMock = new Mock<IPaymentMethodProviderTypeRepository>();
            this.FinancePlanRepositoryMock = new Mock<IFinancePlanRepository>();
            this.GuarantorServiceMock = new Mock<IGuarantorService>();
            this.PaymentConfigurationServiceMock = new Mock<IPaymentConfigurationService>();
            this.ConsolidationGuarantorRepositoryMock = new Mock<IConsolidationGuarantorRepository>();
            this.PaymentRepositoryMock = new Mock<IPaymentRepository>();
            this.PaymentProviderMock = new Mock<IPaymentProvider>();
            this.ContentServiceMock = new Mock<IContentService>();
            this.MetricsProviderMock = new Mock<IMetricsProvider>();
        }

        public PaymentMethodsService CreateService()
        {
            return new PaymentMethodsService(
                this.DomainServiceCommonServiceMockBuilder.CreateServiceLazy(),
                new SessionContext<VisitPay>(this.Session),
                new Lazy<IPaymentMethodsRepository>(() => this.PaymentMethodsRepository ?? this.PaymentMethodsRepositoryMock.Object),
                new Lazy<IPaymentMethodEventRepository>(() => this.PaymentMethodEventRepository ?? this.PaymentMethodEventRepositoryMock.Object),
                new Lazy<IPaymentMethodAccountTypeRepository>(() => this.PaymentMethodAccountTypeRepository ?? this.PaymentMethodAccountTypeRepositoryMock.Object),
                new Lazy<IPaymentMethodProviderTypeRepository>(() => this.PaymentMethodProviderTypeRepository ?? this.PaymentMethodProviderTypeRepositoryMock.Object),
                new Lazy<IFinancePlanRepository>(() => this.FinancePlanRepository ?? this.FinancePlanRepositoryMock.Object),
                new Lazy<IGuarantorService>(() => this.GuarantorService ?? this.GuarantorServiceMock.Object),
                new Lazy<IPaymentConfigurationService>(() => this.PaymentConfigurationService ?? this.PaymentConfigurationServiceMock.Object),
                new Lazy<IConsolidationGuarantorRepository>(() => this.ConsolidationGuarantorRepository ?? this.ConsolidationGuarantorRepositoryMock.Object),
                new Lazy<IPaymentRepository>(() => this.PaymentRepository ?? this.PaymentRepositoryMock.Object),
                new Lazy<IPaymentProvider>(() => this.PaymentProvider ?? this.PaymentProviderMock.Object),
                new Lazy<IContentService>(() => this.ContentService ?? this.ContentServiceMock.Object),
                new Lazy<IMetricsProvider>(() => this.MetricsProvider ?? this.MetricsProviderMock.Object)
            );
        }

        public PaymentMethodsServiceMockBuilder Setup(Action<PaymentMethodsServiceMockBuilder> configAction)
        {
            configAction(this);
            return this;
        }

        public PaymentMethodsServiceMockBuilder WithAcceptedPaymentTypes(IList<PaymentMethodTypeEnum> paymentMethodTypes)
        {
            this.PaymentConfigurationServiceMock.Setup(t => t.AcceptedPaymentMethodTypes).Returns(() => paymentMethodTypes);
            this.PaymentConfigurationServiceMock.Setup(t => t.IsPaymentMethodTypeAccepted(It.IsAny<PaymentMethodTypeEnum>())).Returns<PaymentMethodTypeEnum>(paymentMethodTypes.Contains);

            return this;
        }
    }
}
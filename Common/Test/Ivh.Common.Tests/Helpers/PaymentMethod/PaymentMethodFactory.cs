﻿

namespace Ivh.Common.Tests.Helpers.PaymentMethod
{
    using System;
    using Domain.FinanceManagement.Entities;

    public class PaymentMethodFactory
    {
        public static PaymentMethod GetNonExpiringPaymentMethod(bool isPrimary = true)
        {
            return new PaymentMethod() { ExpDate = DateTime.UtcNow.AddMonths(2).ToString("ddMMyy"), IsPrimary = isPrimary, IsActive = true };
        }
    }
}

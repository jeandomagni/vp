﻿namespace Ivh.Common.Tests.Helpers.Registration
{
    using System;
    using Application.Content.Common.Interfaces;
    using Application.Core.Common.Interfaces;
    using Application.FinanceManagement.Common.Interfaces;
    using Ivh.Application.Matching.Common.Interfaces;
    using Moq;
    using Web.Interfaces;
    using Web.Services;

    public class RegistrationServiceMockBuilder : IMockBuilder<IRegistrationService, RegistrationServiceMockBuilder>
    {
        public IClientApplicationService ClientApplicationService;
        public Mock<IClientApplicationService> ClientApplicationServiceMock;
        public IContentApplicationService ContentApplicationService;
        public Mock<IContentApplicationService> ContentApplicationServiceMock;
        public IGuarantorApplicationService GuarantorApplicationService;
        public Mock<IGuarantorApplicationService> GuarantorApplicationServiceMock;
        public IStatementApplicationService StatementApplicationService;
        public Mock<IStatementApplicationService> StatementApplicationServiceMock;
        public IFeatureApplicationService FeatureApplicationService;
        public Mock<IFeatureApplicationService> FeatureApplicationServiceMock;
        public IMatchingApplicationService MatchingApplicationService;
        public Mock<IMatchingApplicationService> MatchingApplicationServiceMock;

        public RegistrationServiceMockBuilder()
        {
            this.ClientApplicationServiceMock = new Mock<IClientApplicationService>();
            this.ContentApplicationServiceMock = new Mock<IContentApplicationService>();

        }

        public IRegistrationService CreateService()
        {
            return new RegistrationService(
                new Lazy<IClientApplicationService>(() => this.ClientApplicationService ?? this.ClientApplicationServiceMock.Object),
                new Lazy<IContentApplicationService>(() => this.ContentApplicationService ?? this.ContentApplicationServiceMock.Object),
                new Lazy<IGuarantorApplicationService>(() => this.GuarantorApplicationService ?? this.GuarantorApplicationServiceMock.Object), 
                new Lazy<IStatementApplicationService>(() => this.StatementApplicationService ?? this.StatementApplicationServiceMock.Object),
                new Lazy<IMatchingApplicationService>(() => this.MatchingApplicationService ?? this.MatchingApplicationServiceMock.Object),
                new Lazy<IFeatureApplicationService>(() => this.FeatureApplicationService ?? this.FeatureApplicationServiceMock.Object)
            );
        }

        public RegistrationServiceMockBuilder Setup(Action<RegistrationServiceMockBuilder> configAction)
        {
            configAction(this);
            return this;
        }
    }
}
﻿namespace Ivh.Common.Tests.Helpers.VisitAge
{
    using System;
    using System.Data;
    using Ivh.Domain.Base.Interfaces;
    using Ivh.Domain.Rager.Interfaces;
    using Ivh.Domain.Rager.Services;
    using Moq;

    public class VisitAgeServiceMockBuilder : IMockBuilder<IVisitAgeService, VisitAgeServiceMockBuilder>
    {
        public IDomainServiceCommonService DomainServiceCommonService;
        public IVisitRepository VisitRepository;
        public IAgingTierConfigurationRepository AgingTierConfigurationRepository;
        public Mock<IDomainServiceCommonService> DomainServiceCommonServiceMock;
        public Mock<IVisitRepository> VisitRepositoryMock;
        public Mock<IAgingTierConfigurationRepository> AgingTierConfigurationRepositoryMock;

        public VisitAgeServiceMockBuilder()
        {
            this.DomainServiceCommonServiceMock = new Mock<IDomainServiceCommonService>();
            this.VisitRepositoryMock = new Mock<IVisitRepository>();
            this.AgingTierConfigurationRepositoryMock = new Mock<IAgingTierConfigurationRepository>();
        }

        public IVisitAgeService CreateService()
        {
            return new VisitAgeService(
                new Lazy<IDomainServiceCommonService>(() => this.DomainServiceCommonService ?? this.DomainServiceCommonServiceMock.Object),
                this.VisitRepository ?? this.VisitRepositoryMock.Object,
                this.AgingTierConfigurationRepository ?? this.AgingTierConfigurationRepositoryMock.Object
            );
        }

        public VisitAgeServiceMockBuilder Setup(Action<VisitAgeServiceMockBuilder> configAction)
        {
            configAction(this);
            return this;
        }
    }
}
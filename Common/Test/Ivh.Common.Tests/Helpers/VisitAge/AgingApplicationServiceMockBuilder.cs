﻿namespace Ivh.Common.Tests.Helpers.VisitAge
{
    using System;
    using Application.Aging.Common.Interfaces;
    using Ivh.Domain.Rager.Interfaces;
    using NHibernate;
    using Ivh.Application.Aging.ApplicationServices;
    using Base;
    using Data.Connection;
    using Data.Connection.Connections;
    using Moq;

    public class AgingApplicationServiceMockBuilder : IMockBuilder<IAgingApplicationService, AgingApplicationServiceMockBuilder>
    {
        public ApplicationServiceCommonServiceMockBuilder ApplicationServiceCommonServiceMockBuilder;
        public IVisitAgeService VisitAgeService;
        public IAgingGuarantorService AgingGuarantorService;
        public ISession Session;

        public Mock<IVisitAgeService> VisitAgeServiceMock;
        public Mock<IAgingGuarantorService> AgingGuarantorServiceMock;
        public Mock<ISession> SessionMock;

        public AgingApplicationServiceMockBuilder()
        {
            this.ApplicationServiceCommonServiceMockBuilder = new ApplicationServiceCommonServiceMockBuilder();
            this.VisitAgeServiceMock = new Mock<IVisitAgeService>();
            this.SessionMock = new SessionMockBuilder().SessionMock;
        }

        public IAgingApplicationService CreateService()
        {
            return new AgingApplicationService(
                this.ApplicationServiceCommonServiceMockBuilder.CreateServiceLazy(),
                new Lazy<IVisitAgeService>(() => this.VisitAgeService ?? this.VisitAgeServiceMock.Object),
                new Lazy<IAgingGuarantorService>(() => this.AgingGuarantorService ?? this.AgingGuarantorServiceMock.Object),
                new SessionContext<CdiEtl>(this.Session ?? this.SessionMock.Object));
        }

        public AgingApplicationServiceMockBuilder Setup(Action<AgingApplicationServiceMockBuilder> configAction)
        {
            configAction(this);
            return this;
        }
    }
}
﻿namespace Ivh.Common.Tests.Helpers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using Application.Base.Services;
    using Castle.Core.Internal;
    using Domain.Base.Services;

    public static class MockBuilderCodeGenerator
    {
        /// <summary>
        /// Builds sting of a concrete implementation of IMockBuilder
        /// ex string myMock  = MockBuilderCodeGenerator.GenerateIMockBuilder(typeof(PaymentActionApplicationService), "FinancePlan");
        /// </summary>
        /// <param name="serviceToBuild">typeof([your concrete class)</param>
        /// <param name="nameSpace">Assumes Ivh.Common.Tests.Helpers. Provide a folder within this namespace</param>
        /// <returns></returns>
        public static string GenerateIMockBuilder(Type serviceToBuild, string nameSpace)
        {
            HashSet<string> usingList = new HashSet<string>();

            List<string> publicPropertiesList = new List<string>();
            List<string> publicMocksList = new List<string>();
            List<string> buildersConstructorList = new List<string>();
            List<string> createServiceList = new List<string>();

            List<string> baseClasses = new List<string> {nameof(ApplicationService), nameof(DomainService) };

            string baseClassName = baseClasses.FirstOrDefault(x => serviceToBuild.BaseType.Name.Contains(x));
            bool implementsBase = !string.IsNullOrEmpty(baseClassName);

            ConstructorInfo[] ctors = serviceToBuild.GetConstructors();
            ConstructorInfo ctor = ctors[0];
            string namespaceOfConcreteService = "";
            foreach (ParameterInfo param in ctor.GetParameters())
            {
                if (namespaceOfConcreteService.IsNullOrEmpty())
                {
                    namespaceOfConcreteService = param.Member.DeclaringType.Namespace;
                }
                PropertyHelper propertyHelper = new PropertyHelper(param);

                publicPropertiesList.Add(propertyHelper.FormatPublicProperty(implementsBase, baseClassName));
                publicMocksList.Add(propertyHelper.FormatPublicMockProperty(implementsBase, baseClassName));
                createServiceList.Add(propertyHelper.FormatServiceConstructorParameters(implementsBase, baseClassName));

                buildersConstructorList.Add(propertyHelper.FormatBuilderConstructorParameters(implementsBase, baseClassName));
                string namesapce = param.ParameterType.GenericTypeArguments.Length > 0
                    ? param.ParameterType.GenericTypeArguments[0].Namespace
                    : param.ParameterType.Namespace;
                usingList.Add($"using {namesapce};");
            }
            usingList.Add($"using {namespaceOfConcreteService};");
            if (implementsBase)
            {
                usingList.Add("using Base;");
            }

            string usingStatements = string.Join(Environment.NewLine + Tab(1), usingList);
            string publicPropertiesString = string.Join(Environment.NewLine + Tab(2), publicPropertiesList);
            string publicMocksString = string.Join(Environment.NewLine + Tab(2), publicMocksList);
            string buildersConstructorString = string.Join(Environment.NewLine + Tab(3), buildersConstructorList);
            string createServiceString = string.Join(Environment.NewLine + Tab(4), createServiceList);

            string theClass = BuildTheClass(nameSpace,
                serviceToBuild.Name,
                usingStatements,
                publicPropertiesString,
                publicMocksString,
                buildersConstructorString,
                createServiceString.Trim().TrimEnd(',')
            );

            return theClass;
        }

        private static string Tab(int numberOfTabs)
        {
            string tab = "\t";
            IEnumerable<string> tabs = Enumerable.Range(1, numberOfTabs).Select(x => tab);
            return string.Join("", tabs);
        }

        private static string BuildTheClass(string testNameSpace,
            string serviceName,
            string usingStatements,
            string buildersPublicProperties,
            string buildersPublicMockProperties,
            string buildersConstructorString,
            string servicesConstructorString
        )
        {
            return
                $@"namespace Ivh.Common.Tests.Helpers.{testNameSpace}
{{
    using System;
    using System.Data;
    {usingStatements}
    using Moq;

    public class {serviceName}MockBuilder : IMockBuilder<I{serviceName}, {serviceName}MockBuilder>
    {{
        {buildersPublicProperties}
        {buildersPublicMockProperties}

        public {serviceName}MockBuilder()
        {{
            {buildersConstructorString}
        }}
        
        public I{serviceName} CreateService()
        {{
            return new {serviceName}(
                {servicesConstructorString}
            ); 
         }}
        
        public {serviceName}MockBuilder Setup(Action<{serviceName}MockBuilder> configAction)
        {{
            configAction(this);
            return this;
        }}
    }}
}}";
        }
    }

    public class PropertyHelper
    {
        private const string baseBuilderSuffix = "CommonServiceMockBuilder";
        private readonly string _basePropertyName;
        private readonly ParameterInfo _param;

        public PropertyHelper(ParameterInfo param)
        {
            this._basePropertyName = param.ParameterType.GenericTypeArguments.Length > 0 ? param.ParameterType.GenericTypeArguments[0].Name : param.ParameterType.Name;
            this._param = param;
        }

        public string PropertyName => (this._basePropertyName.StartsWith("I")) ? this._basePropertyName.Substring(1) : this._basePropertyName.TrimStart();

        public string MockPropertyName => $"{this.PropertyName}Mock";

        public string InterfaceName => $"I{this.PropertyName}";

        private string FormatBaseInterface(string baseName)
        {
            return $"I{baseName}CommonService";
        }

        public string FormatPublicProperty(bool implementsBase, string baseName)
        {
            if (this.ShouldBuildForBase(implementsBase, baseName))
            {
                string builderName = $"{baseName}{baseBuilderSuffix}";
                return $"public {builderName} {builderName};";
            }
            return $"public {this.InterfaceName} {this.PropertyName};";
        }

        public string FormatPublicMockProperty(bool implementsBase, string baseName)
        {
            if (this.ShouldBuildForBase(implementsBase, baseName))
            {
                //we are going to rely on the builder to return an implementation
                return "";
            }
            return $"public Mock<{this.InterfaceName}> {this.MockPropertyName};";
        }

        public string FormatBuilderConstructorParameters(bool implementsBase, string baseName)
        {
            if (this.ShouldBuildForBase(implementsBase, baseName))
            {
                string builderName = $"{baseName}{baseBuilderSuffix}";
                return $"this.{builderName} = new {builderName}();";
            }
            if (this.InterfaceName == "ISession")
            {
                return "this.SessionMock = new SessionMockBuilder().SessionMock;";
            }
            return $"this.{this.MockPropertyName} = new Mock<{this.InterfaceName}>();";
        }

        public string FormatServiceConstructorParameters(bool implementsBase, string baseName)
        {
            if (this.ShouldBuildForBase(implementsBase,baseName))
            {
                string builderName = $"{baseName}{baseBuilderSuffix}";
                return $"this.{builderName}.CreateServiceLazy(),";
            }

            if (this._param.ParameterType.Name.Contains("Lazy"))
            {
                return $"new Lazy<{this.InterfaceName}>(() => this.{this.PropertyName} ?? this.{this.MockPropertyName}.Object),";
            }

            return $"this.{this.PropertyName} ?? this.{this.MockPropertyName}.Object,";
        }

        private bool ShouldBuildForBase(bool implementsBase, string baseName)
        {
            return implementsBase && this._param.ParameterType.FullName.ToLower().Contains(this.FormatBaseInterface(baseName).ToLower());
        }
    }
}
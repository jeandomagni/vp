﻿namespace Ivh.Common.Tests.Helpers.VisitPayDocument
{
    using System;
    using System.Collections.Generic;
    using Base;
    using Domain.Core.VisitPayDocument.Interfaces;
    using Domain.Core.VisitPayDocument.Services;
    using Moq;

    public class VisitPayDocumentServiceMockBuilder : IMockBuilder<IVisitPayDocumentService, VisitPayDocumentServiceMockBuilder>
    {
        public DomainServiceCommonServiceMockBuilder DomainServiceCommonServiceMockBuilder;
        public IVisitPayDocumentRepository VisitPayDocumentRepository;

        public Mock<IVisitPayDocumentRepository> VisitPayDocumentRepositoryMock;

        public VisitPayDocumentServiceMockBuilder()
        {
            this.DomainServiceCommonServiceMockBuilder = new DomainServiceCommonServiceMockBuilder();
            this.VisitPayDocumentRepositoryMock = new Mock<IVisitPayDocumentRepository>();
        }

        public IVisitPayDocumentService CreateService()
        {
            return new VisitPayDocumentService(
                this.DomainServiceCommonServiceMockBuilder.CreateServiceLazy(),
                new Lazy<IVisitPayDocumentRepository>(() => this.VisitPayDocumentRepository ?? this.VisitPayDocumentRepositoryMock.Object)
            );
        }

        public VisitPayDocumentServiceMockBuilder Setup(Action<VisitPayDocumentServiceMockBuilder> configAction)
        {
            configAction(this);
            return this;
        }

    }
}
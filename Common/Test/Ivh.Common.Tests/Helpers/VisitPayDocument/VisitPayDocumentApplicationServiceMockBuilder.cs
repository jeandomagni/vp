﻿namespace Ivh.Common.Tests.Helpers.VisitPayDocument
{
    using System;
    using Application.Core.ApplicationServices;
    using Application.Core.Common.Interfaces;
    using Base;
    using Domain.Core.VisitPayDocument.Interfaces;
    using Domain.User.Services;
    using Moq;

    public class VisitPayDocumentApplicationServiceMockBuilder : IMockBuilder<IVisitPayDocumentApplicationService, VisitPayDocumentApplicationServiceMockBuilder>
    {
        public ApplicationServiceCommonServiceMockBuilder ApplicationServiceCommonServiceMockBuilder;
        public IVisitPayDocumentService VisitPayDocumentService;

        public Mock<IVisitPayDocumentService> VisitPayDocumentServiceMock;
        public VisitPayUserService VisitPayUserService;
        public Mock<VisitPayUserService> VisitPayUserServiceMock;

        public VisitPayDocumentApplicationServiceMockBuilder()
        {
            this.ApplicationServiceCommonServiceMockBuilder = new ApplicationServiceCommonServiceMockBuilder();
            this.VisitPayDocumentServiceMock = new Mock<IVisitPayDocumentService>();
            this.VisitPayUserServiceMock = new Mock<VisitPayUserService>();
        }

        public IVisitPayDocumentApplicationService CreateService()
        {
            return new VisitPayDocumentApplicationService(
                this.ApplicationServiceCommonServiceMockBuilder.CreateServiceLazy(),
                new Lazy<IVisitPayDocumentService>(() => this.VisitPayDocumentService ?? this.VisitPayDocumentServiceMock.Object),
                new Lazy<VisitPayUserService>(() => this.VisitPayUserService ?? this.VisitPayUserServiceMock.Object)
            );
        }

        public VisitPayDocumentApplicationServiceMockBuilder Setup(Action<VisitPayDocumentApplicationServiceMockBuilder> configAction)
        {
            configAction(this);
            return this;
        }
    }
}
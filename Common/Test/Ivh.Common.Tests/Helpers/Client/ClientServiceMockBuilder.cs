namespace Ivh.Common.Tests.Helpers.Client
{
    using System;
    using Domain.Settings.Entities;
    using Domain.Settings.Interfaces;
    using Moq;

    public class ClientServiceMockBuilder : IMockBuilder<IClientService, ClientServiceMockBuilder>
    {
        public Mock<IClientService> ClientServiceMock;
        public ClientMockBuilder ClientMockBuilder;

        public ClientServiceMockBuilder()
        {
            this.ClientServiceMock = new Mock<IClientService>();
            this.ClientMockBuilder = new ClientMockBuilder();
        }

        public IClientService CreateService()
        {
            return this.CreateMock().Object;
        }

        public Mock<IClientService> CreateMock()
        {
            this.ClientServiceMock.Setup(x => x.GetClient()).Returns(this.ClientMockBuilder.CreateService());
            return this.ClientServiceMock;
        }

        public ClientServiceMockBuilder Setup(Action<ClientServiceMockBuilder> configAction)
        {
            configAction(this);
            return this;
        }
    }

    public class ClientMockBuilder : IMockBuilder<Client, ClientMockBuilder>
    {
        public Mock<Client> ClientMock;

        public ClientMockBuilder()
        {
            this.ClientMock = new Mock<Client>();
            this.ClientMock.Setup(x => x.UncollectableMaxAgingCountFinancePlans).Returns(4);
            this.ClientMock.Setup(x => x.ClientIsStrongPassword).Returns(true);
            this.ClientMock.Setup(x => x.ClientPasswordMinLength).Returns(0);
            this.ClientMock.Setup(x => x.GuarantorIsStrongPassword).Returns(true);
            this.ClientMock.Setup(x => x.GuarantorPasswordMinLength).Returns(0);
            this.ClientMock.Setup(x => x.ClosedUncollectableDays).Returns(9);
            this.ClientMock.Setup(x => x.MaxFuturePaymentIntervalInDays).Returns(30);
            this.ClientMock.Setup(x => x.GracePeriodLength).Returns(21);
        }

        public Client CreateService()
        {
            return this.ClientMock.Object;
        }

        public ClientMockBuilder Setup(Action<ClientMockBuilder> configAction)
        {
            configAction(this);
            return this;
        }
    }
}
﻿namespace Ivh.Common.Tests.Helpers
{
    using System;

    public interface IMockBuilder<out TService, out TBuilder>
    {
        TService CreateService();
        TBuilder Setup(Action<TBuilder> configAction);
    }
}
﻿namespace Ivh.Common.Tests.Helpers.Payment
{
    using System;
    using Base;
    using Data.Connection.Connections;
    using Data.Interfaces;
    using Domain.Base.Interfaces;
    using Domain.Visit.Interfaces;
    using Domain.FinanceManagement.FinancePlan.Interfaces;
    using Domain.FinanceManagement.Payment.Interfaces;
    using Domain.FinanceManagement.Statement.Interfaces;
    using Domain.Logging.Interfaces;
    using Domain.Settings.Interfaces;
    using Moq;
    using NHibernate;
    using Ivh.Common.ServiceBus.Interfaces;

    public class PaymentAllocationServiceMockBuilder
    {
        public Mock<IFinancePlanService> FinancePlanServiceMock;
        public Mock<IVisitService> VisitServiceMock;
        public Mock<IVpStatementService> VpStatementServiceMock;
        public Mock<IPaymentRepository> PaymentRepository;
        public Mock<IVisitTransactionRepository> VisitTransactionRepository;
        public Mock<IVisitTransactionService> VisitTransactionService;
        public Mock<IMerchantAccountAllocationService> MerchantAccountAllocationService;
        public Mock<IBus> Bus;
        public Mock<ISessionContext<VisitPay>> Session;
        public Mock<ILoggingService> Logger;
        public Mock<IPaymentAllocationRepository> PaymentAllocationRepositoryMock;
        public Mock<IPaymentVisitRepository> PaymentVisitRepositoryMock;
        public Mock<IPaymentFinancePlanRepository> PaymentFinancePlanRepository;
        public Mock<IClientService> ClientService;


        public IDomainServiceCommonService DomainServiceCommonService;

        public PaymentAllocationServiceMockBuilder()
        {
            this.VisitServiceMock = new Mock<IVisitService>();
            this.FinancePlanServiceMock = new Mock<IFinancePlanService>();
            this.VpStatementServiceMock = new Mock<IVpStatementService>();
            this.PaymentRepository = new Mock<IPaymentRepository>();
            this.VisitTransactionRepository = new Mock<IVisitTransactionRepository>();
            this.VisitTransactionService = new Mock<IVisitTransactionService>();
            this.MerchantAccountAllocationService = new Mock<IMerchantAccountAllocationService>();
            this.Bus = new Mock<IBus>();
            this.Session = new Mock<ISessionContext<VisitPay>>();
            this.Logger = new Mock<ILoggingService>();
            this.PaymentAllocationRepositoryMock = new Mock<IPaymentAllocationRepository>();
            this.PaymentVisitRepositoryMock = new Mock<IPaymentVisitRepository>();
            this.ClientService = new Mock<IClientService>();

            this.DomainServiceCommonService = DomainServiceCommonServices.CreateDomainServiceCommonService(
                applicationSettingsServiceMock: null,
                featureServiceMock: null,
                clientServiceMock: this.ClientService,
                loggingServiceMock: this.Logger,
                busMock: this.Bus,
                cacheMock: null,
                timeZoneHelperMock: null);
            this.PaymentFinancePlanRepository= new Mock<IPaymentFinancePlanRepository>();

        }

        public IPaymentAllocationService GetPaymentAllocationService(Type t)
        {
            return (IPaymentAllocationService)Activator.CreateInstance(t,
                new Lazy<IDomainServiceCommonService>(() => this.DomainServiceCommonService),
                new Lazy<IVisitService>(() => this.VisitServiceMock.Object),
                new Lazy<IVpStatementService>(() => this.VpStatementServiceMock.Object),
                new Lazy<IFinancePlanService>(() => this.FinancePlanServiceMock.Object),
                new Lazy<IPaymentRepository>(() => this.PaymentRepository.Object),
                new Lazy<IVisitTransactionRepository>(() => this.VisitTransactionRepository.Object),
                new Lazy<IVisitTransactionService>(() => this.VisitTransactionService.Object),
                new Lazy<IMerchantAccountAllocationService>(() => this.MerchantAccountAllocationService.Object),
                this.Session.Object,
                new Lazy<IPaymentAllocationRepository>(() => this.PaymentAllocationRepositoryMock.Object),
                new Lazy<IPaymentVisitRepository> (() => this.PaymentVisitRepositoryMock.Object),
                new Lazy<IPaymentFinancePlanRepository>(() => this.PaymentFinancePlanRepository.Object)
                );
        }

        public PaymentAllocationServiceMockBuilder Setup(Action<PaymentAllocationServiceMockBuilder> configAction)
        {
            configAction(this);
            return this;
        }
    }
}
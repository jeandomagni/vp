﻿namespace Ivh.Common.Tests.Helpers.Payment
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using AutoMapper;
    using Builders;
    using Common.Base.Enums;
    using Common.Base.Utilities.Extensions;
    using Domain.FinanceManagement.Entities;
    using Domain.FinanceManagement.Payment.Entities;
    using Domain.Guarantor.Entities;
    using Domain.Visit.Entities;
    using VisitPay.Enums;

    public class PaymentBuilder : EntityBuilder<PaymentBuilder, Payment>
    {
        public PaymentBuilder WithDefaultValues()
        {
            // add your defaults here 
            this.Builder.With(x =>
            {
                x.InsertDate = DateTime.UtcNow;
                x.PaymentId = RandomIdGenerator.Next();
                x.ActualPaymentAmount = 0m;
                x.Guarantor = new Guarantor{ VpGuarantorId = 1 };
                x.PaymentMethod = new PaymentMethod();
            });

            return this.Builder;
        }

        public PaymentBuilder AsManualScheduledSpecificVisits(params PaymentScheduledAmount[] paymentScheduledAmounts)
        {
            this.Builder.With(x => 
            {
                x.PaymentType = PaymentTypeEnum.ManualScheduledSpecificVisits;
                x.PaymentScheduledAmounts.AddRange(paymentScheduledAmounts);
            });

            return this.Builder;
        }

        /// <summary>
        /// sets payment allocations using provided list, or generates naive allocations based on existing paymentScheduledAmounts
        /// </summary>
        /// <param name="paymentAllocations"></param>
        /// <returns></returns>
        public PaymentBuilder WithPaymentAllocations(IList<PaymentAllocation> paymentAllocations = null)
        {
            this.Builder.With(x => 
            {
                x.PaymentAllocations = paymentAllocations ?? 
                                       x.PaymentScheduledAmounts
                                           .SelectMany(psa => new PaymentAllocationBuilder().FromPaymentScheduledAmount(psa))
                                           .ToList();
            });

            this.Builder.CalculateActualPaymentAmount();

            return this.Builder;
        }

        public PaymentBuilder CalculateActualPaymentAmount()
        {
            this.Builder.With(x => 
            {
                x.ActualPaymentAmount = x.PaymentAllocations
                    .Where(pa => pa.PaymentAllocationType != PaymentAllocationTypeEnum.VisitDiscount)
                    .Sum(pa => pa.ActualAmount);
            });

            return this.Builder;
        }

        public static implicit operator Payment(PaymentBuilder instance)
        {
            return instance.Build();
        }
    }
}

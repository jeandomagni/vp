﻿namespace Ivh.Common.Tests.Helpers.Payment
{
    using System;
    using Application.FinanceManagement.ApplicationServices;
    using Application.FinanceManagement.Common.Interfaces;
    using Base;
    using Data.Connection;
    using Data.Connection.Connections;
    using Domain.FinanceManagement.FinancePlan.Interfaces;
    using Domain.FinanceManagement.Interfaces;
    using Domain.FinanceManagement.Statement.Interfaces;
    using Domain.Guarantor.Interfaces;
    using Moq;
    using NHibernate;

    public class ScheduledPaymentApplicationServiceMockBuilder : IMockBuilder<IScheduledPaymentApplicationService, ScheduledPaymentApplicationServiceMockBuilder>
    {
        public ApplicationServiceCommonServiceMockBuilder ApplicationServiceCommonServiceMockBuilder;
        public IFinancePlanService FinancePlanService;
        public IGuarantorService GuarantorService;
        public IPaymentService PaymentService;
        public IVpStatementService StatementService;
        public ISession Session;

        public Mock<IFinancePlanService> FinancePlanServiceMock;
        public Mock<IGuarantorService> GuarantorServiceMock;
        public Mock<IPaymentService> PaymentServiceMock;
        public Mock<IVpStatementService> StatementServiceMock;
        public Mock<ISession> SessionMock;
        
        public ScheduledPaymentApplicationServiceMockBuilder()
        {
            this.ApplicationServiceCommonServiceMockBuilder = new ApplicationServiceCommonServiceMockBuilder();
            this.FinancePlanServiceMock = new Mock<IFinancePlanService>();
            this.GuarantorServiceMock = new Mock<IGuarantorService>();
            this.PaymentServiceMock = new Mock<IPaymentService>();
            this.StatementServiceMock = new Mock<IVpStatementService>();
            this.SessionMock = new SessionMockBuilder().SessionMock;
        }

        public IScheduledPaymentApplicationService CreateService()
        {
            return new ScheduledPaymentApplicationService(
                this.ApplicationServiceCommonServiceMockBuilder.CreateServiceLazy(),
                new SessionContext<VisitPay>(this.Session ?? this.SessionMock.Object),
                new Lazy<IVpStatementService>(() => this.StatementService ?? this.StatementServiceMock.Object),
                new Lazy<IPaymentService>(() => this.PaymentService ?? this.PaymentServiceMock.Object),
                new Lazy<IGuarantorService>(() => this.GuarantorService ?? this.GuarantorServiceMock.Object),
                new Lazy<IFinancePlanService>(() => this.FinancePlanService ?? this.FinancePlanServiceMock.Object)
            );
        }

        public ScheduledPaymentApplicationServiceMockBuilder Setup(Action<ScheduledPaymentApplicationServiceMockBuilder> configAction)
        {
            configAction(this);
            return this;
        }
    }
}
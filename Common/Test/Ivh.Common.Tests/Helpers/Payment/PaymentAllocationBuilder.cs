﻿namespace Ivh.Common.Tests.Helpers.Payment
{
    using System;
    using Builders;
    using Common.Base.Enums;
    using Domain.FinanceManagement.Payment.Entities;
    using VisitPay.Enums;

    public class PaymentAllocationBuilder : EntityBuilder<PaymentAllocationBuilder, PaymentAllocation>
    {
        private PaymentVisit DefaultPaymentVisitWithId(int visitId)
        {
            PaymentVisit visit = new PaymentVisitBuilder().WithVisitIdValues(visitId).Build();
            return visit;
        }

        public PaymentAllocationBuilder WithDefaultValues(int visitId)
        {
            // add your defaults here 
            this.Builder.With(x =>
            {
                int paymentAllocationId = RandomIdGenerator.Next();
                x.InsertDate = DateTime.UtcNow;
                x.PaymentAllocationId = paymentAllocationId;
                x.ActualAmount = 0m;
                x.OriginalAmount = 0m;
                x.PaymentAllocationType = PaymentAllocationTypeEnum.VisitPrincipal;
                x.PaymentVisit = this.DefaultPaymentVisitWithId(visitId);
                x.Payment = PaymentFactory.CreatePayment(PaymentTypeEnum.ManualPromptSpecificAmount);
            });

            return this.Builder;
        }

        /// <summary>
        /// builds two payment allocations based on a payment scheduled amount
        /// </summary>
        /// <param name="paymentScheduledAmount"></param>
        /// <returns></returns>
        public PaymentAllocation[] FromPaymentScheduledAmount(PaymentScheduledAmount paymentScheduledAmount)
        {
            return new []
            {
                new PaymentAllocationBuilder().FromPaymentScheduledAmountPrincipal(paymentScheduledAmount).Build(),
                new PaymentAllocationBuilder().FromPaymentScheduledAmountInterest(paymentScheduledAmount).Build()
            };
        }

        public PaymentAllocationBuilder FromPaymentScheduledAmountInterest(PaymentScheduledAmount paymentScheduledAmount)
        {
            this.Builder.With(x =>
            {
                int paymentAllocationId = RandomIdGenerator.Next();
                x.InsertDate = DateTime.UtcNow;
                x.PaymentAllocationId = paymentAllocationId;
                x.ActualAmount = paymentScheduledAmount.ScheduledInterest;
                x.OriginalAmount = paymentScheduledAmount.ScheduledInterest;
                x.PaymentAllocationType = PaymentAllocationTypeEnum.VisitInterest;
                x.PaymentVisit = paymentScheduledAmount.PaymentVisit;
                x.Payment = paymentScheduledAmount.Payment;
            });

            return this.Builder;
        }

        public PaymentAllocationBuilder FromPaymentScheduledAmountPrincipal(PaymentScheduledAmount paymentScheduledAmount)
        {
            this.Builder.With(x =>
            {
                int paymentAllocationId = RandomIdGenerator.Next();
                x.InsertDate = DateTime.UtcNow;
                x.PaymentAllocationId = paymentAllocationId;
                x.ActualAmount = paymentScheduledAmount.ScheduledPrincipal;
                x.OriginalAmount = paymentScheduledAmount.ScheduledPrincipal;
                x.PaymentAllocationType = PaymentAllocationTypeEnum.VisitPrincipal;
                x.PaymentVisit = paymentScheduledAmount.PaymentVisit;
                x.Payment = paymentScheduledAmount.Payment;
            });

            return this.Builder;
        }

        public PaymentAllocationBuilder AsRecurringFinancePlanPayment()
        {
            this.Builder.With(x =>
            {
                x.Payment = PaymentFactory.CreatePayment(PaymentTypeEnum.RecurringPaymentFinancePlan);
            });

            return this.Builder;
        }

        public PaymentAllocationBuilder AsInterest()
        {
            return this.WithPaymentAllocationType(PaymentAllocationTypeEnum.VisitInterest);
        }

        public PaymentAllocationBuilder WithPaymentAllocationType(PaymentAllocationTypeEnum paymentAllocationType)
        {
            this.Builder.With(x => x.PaymentAllocationType = paymentAllocationType);

            return this.Builder;
        }

        public PaymentAllocationBuilder WithAmount(decimal amount)
        {
            this.Builder.With(x =>
            {
                x.ActualAmount = amount;
                x.OriginalAmount = amount;
            });
            return this.Builder;
        }

        public static implicit operator PaymentAllocation(PaymentAllocationBuilder instance)
        {
            return instance.Build();
        }

    }
}
﻿namespace Ivh.Common.Tests.Helpers.Payment
{
    using System;
    using System.Collections.Generic;
    using Base;
    using Common.Base.Utilities.Helpers;
    using Data.Connection;
    using Data.Connection.Connections;
    using Domain.Base.Interfaces;
    using Domain.Core.SystemException.Interfaces;
    using Domain.FinanceManagement.FinancePlan.Interfaces;
    using Domain.FinanceManagement.Interfaces;
    using Domain.FinanceManagement.Payment.Entities;
    using Domain.FinanceManagement.Payment.Interfaces;
    using Domain.FinanceManagement.Services;
    using Domain.FinanceManagement.Statement.Interfaces;
    using Domain.Guarantor.Entities;
    using Domain.Logging.Interfaces;
    using Domain.Settings.Entities;
    using Domain.Settings.Interfaces;
    using Domain.Visit.Interfaces;
    using ServiceBus.Interfaces;
    using Moq;
    using NHibernate;
    using VisitPay.Enums;
    using Ivh.Domain.FinanceManagement.Ach.Interfaces;

    public class PaymentServiceMockBuilder
    {
        public Mock<IPaymentProvider> PaymentProviderMock;
        public Mock<IPaymentRepository> PaymentRepositoryMock;
        public Mock<IVpStatementService> VpStatementServiceMock;
        public Mock<IPaymentProcessorResponseRepository> PaymentProcessorResponseRepositoryMock;
        public Mock<IPaymentAllocationService> PaymentAllocationServiceMock;
        public Mock<IPaymentProcessorRepository> PaymentProcessorRepositoryMock;
        public Mock<IPaymentMethodsService> PaymentMethodsServiceMock;
        public Mock<ISystemExceptionService> SystemExceptionService;
        public Mock<IPaymentEventService> PaymentEventServiceMock;
        public Mock<IBus> Bus;
        public Mock<IClientService> ClientServiceMock;
        public Mock<IFinancePlanService> FinancePlanServiceMock;
        public Mock<ISession> Session;
        public Mock<ILoggingService> LoggingService;
        public Mock<IMetricsProvider> MetricsProvider;
        public Mock<IApplicationSettingsService> ApplicationSettingsService;
        public Mock<IVpStatementService> VpStatementService;
        public Mock<IFeatureService> FeatureServiceMock;
        public Mock<IMerchantAccountAllocationService> MerchantAccountAllocationServiceMock;
        public Mock<IPaymentFinancePlanRepository> PaymentFinancePlanRepositoryMock;
        public Mock<IPaymentImportRepository> PaymentImportRepositoryMock;
        public Mock<IPaymentVisitRepository> PaymentVisitRepositoryMock;
        public Mock<IPaymentAllocationRepository> PaymentAllocationRepositoryMock;
        public Mock<IAchService> AchServiceMock;
        public TimeZoneHelper TimeZoneHelper;

        public IVisitStateService StatusServiceOverride;
        public IDomainServiceCommonService DomainServiceCommonService;

        public PaymentServiceMockBuilder()
        {
            this.PaymentAllocationServiceMock = new Mock<IPaymentAllocationService>();
            this.PaymentAllocationServiceMock.Setup(x => x.AllocatePayment(It.IsAny<Payment>())).Returns(new PaymentAllocationResult());
            this.PaymentAllocationServiceMock.Setup(x => x.AllocatePayments(It.IsAny<IList<Payment>>())).Returns(new List<PaymentAllocationResult>());
            this.PaymentAllocationServiceMock.Setup(x => x.AllocatePaymentReverse(It.IsAny<Payment>(), It.IsAny<Payment>())).Returns(new PaymentAllocationResult());
            this.PaymentProcessorRepositoryMock = new Mock<IPaymentProcessorRepository>();
            this.PaymentProcessorResponseRepositoryMock = new Mock<IPaymentProcessorResponseRepository>();
            this.PaymentProviderMock = new Mock<IPaymentProvider>();
            this.PaymentRepositoryMock = new Mock<IPaymentRepository>();
            this.PaymentMethodsServiceMock = new Mock<IPaymentMethodsService>();
            this.PaymentEventServiceMock = new Mock<IPaymentEventService>();
            this.SystemExceptionService = new Mock<ISystemExceptionService>();
            this.Bus = new Mock<IBus>();
            this.FinancePlanServiceMock = new Mock<IFinancePlanService>();
            this.FeatureServiceMock = new Mock<IFeatureService>();
            this.FeatureServiceMock.Setup(t => t.IsFeatureEnabled(It.IsAny<VisitPayFeatureEnum>())).Returns(true);
            this.LoggingService = new Mock<ILoggingService>();
            this.VpStatementServiceMock = new Mock<IVpStatementService>();
            this.VpStatementServiceMock.Setup(t => t.GetMostRecentStatement(It.IsAny<Guarantor>())).Returns(() => null);
            this.MerchantAccountAllocationServiceMock = new Mock<IMerchantAccountAllocationService>();
            this.PaymentVisitRepositoryMock = new Mock<IPaymentVisitRepository>();
            this.PaymentFinancePlanRepositoryMock = new Mock<IPaymentFinancePlanRepository>();
            this.PaymentImportRepositoryMock = new Mock<IPaymentImportRepository>();
            this.PaymentAllocationRepositoryMock = new Mock<IPaymentAllocationRepository>();
            this.AchServiceMock = new Mock<IAchService>();
            this.TimeZoneHelper = new TimeZoneHelper(TimeZoneHelper.TimeZones.MountainStandardTime, TimeZoneHelper.TimeZones.Utc);

            this.ClientServiceMock = new Mock<IClientService>();
            Mock<Client> clientMock = new Mock<Client>();
            clientMock.Setup(t => t.PaymentMaxAttemptCount).Returns(50);
            clientMock.Setup(t => t.BalanceTransferIsEnabled).Returns(true);
            this.ClientServiceMock.Setup(t => t.GetClient()).Returns(clientMock.Object);

            Mock<ITransaction> transactionMock = new Mock<ITransaction>();
            transactionMock.SetupGet(x => x.IsActive).Returns(true);
            this.Session = new Mock<ISession>();
            this.Session.SetupGet(x => x.Transaction).Returns(transactionMock.Object);
            this.Session.Setup(x => x.BeginTransaction()).Returns(transactionMock.Object);
            this.MetricsProvider = new Mock<IMetricsProvider>();

            this.ApplicationSettingsService = new Mock<IApplicationSettingsService>();
            this.ApplicationSettingsService.Setup(t => t.PastDueAgingCount).Returns(() => new ApplicationSetting<int>(() => new Lazy<int>(() => 1)));

            this.DomainServiceCommonService = DomainServiceCommonServices.CreateDomainServiceCommonService(
                this.ApplicationSettingsService,
                this.FeatureServiceMock,
                this.ClientServiceMock,
                this.LoggingService,
                this.Bus,
                null,
                null
            );
        }

        public PaymentService CreatePaymentService()
        {
            return new PaymentService(
                new Lazy<IDomainServiceCommonService>(() => this.DomainServiceCommonService),
                new Lazy<IPaymentProvider>(() => this.PaymentProviderMock.Object),
                new Lazy<IPaymentRepository>(() => this.PaymentRepositoryMock.Object),
                new Lazy<IPaymentAllocationService>(() => this.PaymentAllocationServiceMock.Object),
                new Lazy<IPaymentProcessorRepository>(() => this.PaymentProcessorRepositoryMock.Object),
                new Lazy<IPaymentMethodsService>(() => this.PaymentMethodsServiceMock.Object),
                new Lazy<IPaymentProcessorResponseRepository>(() => this.PaymentProcessorResponseRepositoryMock.Object),
                new Lazy<IVpStatementService>(() => this.VpStatementServiceMock.Object),
                new Lazy<IPaymentEventService>(() => this.PaymentEventServiceMock.Object),
                new Lazy<IFinancePlanService>(() => this.FinancePlanServiceMock.Object),
                new Lazy<ISystemExceptionService>(() => this.SystemExceptionService.Object),
                new Lazy<IMetricsProvider>(() => this.MetricsProvider.Object),
                new Lazy<IMerchantAccountAllocationService>(() => this.MerchantAccountAllocationServiceMock.Object),
                new Lazy<IPaymentAllocationRepository>(() => this.PaymentAllocationRepositoryMock.Object),
                new Lazy<IPaymentVisitRepository>(() => this.PaymentVisitRepositoryMock.Object),
                new Lazy<IPaymentFinancePlanRepository>(() => this.PaymentFinancePlanRepositoryMock.Object),
                new Lazy<IPaymentImportRepository>(() => this.PaymentImportRepositoryMock.Object),
                new Lazy<IAchService>(() => this.AchServiceMock.Object),
                new Lazy<TimeZoneHelper>(() => this.TimeZoneHelper),
                new SessionContext<VisitPay>(this.Session.Object));
        }

        public PaymentServiceMockBuilder Setup(Action<PaymentServiceMockBuilder> configAction)
        {
            configAction(this);
            return this;
        }
    }
}
﻿namespace Ivh.Common.Tests.Helpers.Payment
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using AutoMapper;
    using Common.Base.Enums;
    using Domain.FinanceManagement.Entities;
    using Domain.FinanceManagement.FinancePlan.Entities;
    using Domain.FinanceManagement.Payment.Entities;
    using Domain.Guarantor.Entities;
    using Domain.Visit.Entities;
    using VisitPay.Enums;

    public class PaymentFactory
    {
        private static int _startingPaymentId = 1;

        public static Payment CreatePayment(PaymentTypeEnum paymentType, IDictionary<Tuple<Visit, IList<VisitTransaction>>, decimal> visitsWithAmounts = null, IDictionary<FinancePlan, decimal> financePlansWithAmounts = null, decimal? specificAmount = 0)
        {
            Payment payment = new Payment
            {
                PaymentId = _startingPaymentId++,
                PaymentType = paymentType,
                ActualPaymentAmount = 0,
                Guarantor = new Guarantor {VpGuarantorId = 1},
                PaymentMethod = new PaymentMethod()
            };

            switch (paymentType)
            {
                case PaymentTypeEnum.ManualPromptSpecificAmount:
                case PaymentTypeEnum.ManualScheduledSpecificAmount:
                    payment.PaymentScheduledAmounts.Add(new PaymentScheduledAmount
                    {
                        ScheduledAmount = specificAmount.GetValueOrDefault(0)
                    });

                    return payment;
                case PaymentTypeEnum.ManualPromptSpecificFinancePlans:
                case PaymentTypeEnum.ManualScheduledSpecificFinancePlans:
                case PaymentTypeEnum.RecurringPaymentFinancePlan:
                    financePlansWithAmounts?.ToList().ForEach(x =>
                    {
                        decimal interest = x.Key.FinancePlanVisits.Sum(z => z.InterestDue);
                        payment.PaymentScheduledAmounts.Add(new PaymentScheduledAmount
                        {
                            ScheduledAmount = x.Value,
                            ScheduledPrincipal = x.Value - interest,
                            ScheduledInterest = interest,
                            PaymentFinancePlan = new PaymentFinancePlan {FinancePlanId = x.Key.FinancePlanId, CurrentBucket = x.Key.CurrentBucket}
                        });
                    });

                    return payment;

                case PaymentTypeEnum.ManualScheduledSpecificVisits:
                    visitsWithAmounts?.ToList().ForEach(x =>
                    {
                        payment.PaymentScheduledAmounts.Add(new PaymentScheduledAmount
                        {
                            ScheduledAmount = x.Value,
                            PaymentVisit = Mapper.Map<PaymentVisit>(x.Key.Item1)
                        });
                    });

                    return payment;

                case PaymentTypeEnum.ManualScheduledCurrentBalanceInFull:
                case PaymentTypeEnum.ManualScheduledCurrentNonFinancedBalanceInFull:
                    return payment;

                default:
                    throw new ArgumentOutOfRangeException(nameof(paymentType));
            }
        }
    }
}
﻿namespace Ivh.Common.Tests.Helpers.Payment
{
    using System;
    using Base;
    using Domain.FinanceManagement.Payment.Interfaces;
    using Domain.FinanceManagement.Payment.Services;
    using Moq;

    public class PaymentActionServiceMockBuilder : IMockBuilder<PaymentActionService, PaymentActionServiceMockBuilder>
    {
        public DomainServiceCommonServiceMockBuilder DomainServiceCommonServiceMockBuilder;
        public IPaymentActionHistoryRepository PaymentActionHistoryRepository;
        public IPaymentActionReasonRepository PaymentActionReasonRepository;

        public Mock<IPaymentActionHistoryRepository> PaymentActionHistoryRepositoryMock;
        public Mock<IPaymentActionReasonRepository> PaymentActionReasonRepositoryMock;

        public PaymentActionServiceMockBuilder()
        {
            this.DomainServiceCommonServiceMockBuilder = new DomainServiceCommonServiceMockBuilder();
            this.PaymentActionHistoryRepositoryMock = new Mock<IPaymentActionHistoryRepository>();
            this.PaymentActionReasonRepositoryMock = new Mock<IPaymentActionReasonRepository>();
        }

        public PaymentActionService CreateService()
        {
            return new PaymentActionService(
                this.DomainServiceCommonServiceMockBuilder.CreateServiceLazy(),
                new Lazy<IPaymentActionHistoryRepository>(() => this.PaymentActionHistoryRepository ?? this.PaymentActionHistoryRepositoryMock.Object),
                new Lazy<IPaymentActionReasonRepository>(() => this.PaymentActionReasonRepository ?? this.PaymentActionReasonRepositoryMock.Object)
            );
        }

        public PaymentActionServiceMockBuilder Setup(Action<PaymentActionServiceMockBuilder> configAction)
        {
            configAction(this);
            return this;
        }
    }
}
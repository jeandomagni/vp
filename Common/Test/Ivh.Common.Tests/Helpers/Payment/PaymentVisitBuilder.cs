﻿namespace Ivh.Common.Tests.Helpers.Payment
{
    using System;
    using Builders;
    using Common.Base.Enums;
    using Domain.FinanceManagement.Payment.Entities;
    using VisitPay.Enums;

    public class PaymentVisitBuilder : EntityBuilder<PaymentVisitBuilder, PaymentVisit>
    {

        public PaymentVisitBuilder WithVisitIdValues(int visitId)
        {
            // add your defaults here 
            this.Builder.With(x =>
            {
                x.InsertDate = DateTime.UtcNow;
                x.VisitId = visitId;
                x.CurrentVisitState = VisitStateEnum.Active;
            });

            return this.Builder;
        }
        public PaymentVisitBuilder WithDefaultValues()
        {
            // add your defaults here 
            this.Builder.With(x =>
            {
                int paymentVisitId = RandomIdGenerator.Next();
                x.InsertDate = DateTime.UtcNow;
                x.VisitId = paymentVisitId;
                x.CurrentVisitState = VisitStateEnum.Active;
            });

            return this.Builder;
        }
        
        public static implicit operator PaymentVisit(PaymentVisitBuilder instance)
        {
            return instance.Build();
        }

    }
}
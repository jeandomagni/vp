﻿namespace Ivh.Common.Tests.Helpers.Discount
{
    using System;
    using Ivh.Domain.FinanceManagement.Discount.Services;
    using Base;
    using Common.Base.Enums;
    using Domain.FinanceManagement.Discount.Interfaces;
    using Domain.Settings.Entities;
    using Moq;
    using VisitPay.Enums;

    public class InsuranceDiscountServiceMockBuilder : IMockBuilder<IDiscountService, InsuranceDiscountServiceMockBuilder>
    {
        public DomainServiceCommonServiceMockBuilder DomainServiceCommonServiceMockBuilder;
    
        private AgingTierEnum _discountMaximumAgingTier = AgingTierEnum.Good;
        private bool _discountPromptPayOnly = false;

        public InsuranceDiscountServiceMockBuilder()
        {
            this.DomainServiceCommonServiceMockBuilder = new DomainServiceCommonServiceMockBuilder();
        }

        public IDiscountService CreateService()
        {
            this.DomainServiceCommonServiceMockBuilder.ClientServiceMock.Setup(x => x.GetClient()).Returns(() => 
            { 
                Mock<Client> moqClient = new Mock<Client>();
                moqClient.Setup(x => x.DiscountMaximumAgingTier).Returns(() => this._discountMaximumAgingTier);
                moqClient.Setup(x => x.DiscountPromptPayOnly).Returns(() => this._discountPromptPayOnly);

                return moqClient.Object;
            });

            return new InsuranceDiscountService(
                this.DomainServiceCommonServiceMockBuilder.CreateServiceLazy(),
                this.DomainServiceCommonServiceMockBuilder.CreateScoringServiceLazy(),
                this.DomainServiceCommonServiceMockBuilder.CreateRandomizedTestServiceLazy()
            );
        }

        public InsuranceDiscountServiceMockBuilder Setup(Action<InsuranceDiscountServiceMockBuilder> configAction)
        {
            configAction(this);
            return this;
        }
        
        public InsuranceDiscountServiceMockBuilder WithDiscountMaximumAgingTier(AgingTierEnum discountMaximumAgingTier)
        {
            this._discountMaximumAgingTier = discountMaximumAgingTier;

            return this;
        }

        public InsuranceDiscountServiceMockBuilder WithDiscountPromptPayOnly(bool discountPromptPayOnly)
        {
            this._discountPromptPayOnly = discountPromptPayOnly;

            return this;
        }
    }
}
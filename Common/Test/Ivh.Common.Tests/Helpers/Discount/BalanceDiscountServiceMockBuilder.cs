﻿namespace Ivh.Common.Tests.Helpers.Discount
{
    using System;
    using Ivh.Domain.FinanceManagement.Discount.Interfaces;
    using Ivh.Domain.FinanceManagement.Discount.Services;
    using Base;
    using Common.Base.Enums;
    using Domain.Guarantor.Interfaces;
    using Domain.Settings.Entities;
    using Moq;
    using VisitPay.Enums;

    public class BalanceDiscountServiceMockBuilder
    {
        public DomainServiceCommonServiceMockBuilder DomainServiceCommonServiceMockBuilder;
        public IDiscountBalanceTierRepository DiscountBalanceTierRepository;

        public Mock<IDiscountBalanceTierRepository> DiscountBalanceTierRepositoryMock;

        private AgingTierEnum _discountMaximumAgingTier = AgingTierEnum.Good;
        private bool _discountPromptPayOnly = false;

        public BalanceDiscountServiceMockBuilder()
        {
            this.DomainServiceCommonServiceMockBuilder = new DomainServiceCommonServiceMockBuilder();
            this.DiscountBalanceTierRepositoryMock = new Mock<IDiscountBalanceTierRepository>();
        }

        public IDiscountService CreateService()
        {
            this.DomainServiceCommonServiceMockBuilder.ClientServiceMock.Setup(x => x.GetClient()).Returns(() => 
            { 
                Mock<Client> moqClient = new Mock<Client>();
                moqClient.Setup(x => x.DiscountMaximumAgingTier).Returns(() => this._discountMaximumAgingTier);
                moqClient.Setup(x => x.DiscountPromptPayOnly).Returns(() => this._discountPromptPayOnly);

                return moqClient.Object;
            });

            return new BalanceDiscountService(
                this.DomainServiceCommonServiceMockBuilder.CreateServiceLazy(),
                new Lazy<IDiscountBalanceTierRepository>(() => this.DiscountBalanceTierRepository ?? this.DiscountBalanceTierRepositoryMock.Object),
                this.DomainServiceCommonServiceMockBuilder.CreateScoringServiceLazy(),
                this.DomainServiceCommonServiceMockBuilder.CreateRandomizedTestServiceLazy()
            );
        }

        public BalanceDiscountServiceMockBuilder Setup(Action<BalanceDiscountServiceMockBuilder> configAction)
        {
            configAction(this);
            return this;
        }

        public BalanceDiscountServiceMockBuilder WithDiscountMaximumAgingTier(AgingTierEnum discountMaximumAgingTier)
        {
            this._discountMaximumAgingTier = discountMaximumAgingTier;

            return this;
        }

        public BalanceDiscountServiceMockBuilder WithDiscountPromptPayOnly(bool discountPromptPayOnly)
        {
            this._discountPromptPayOnly = discountPromptPayOnly;

            return this;
        }
    }
}

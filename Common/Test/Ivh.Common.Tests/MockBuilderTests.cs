﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ivh.Common.Tests
{
    using Application.Core;
    using Application.Core.ApplicationServices;
    using Application.FinanceManagement.ApplicationServices;
    using Domain.Base.Services;
    using Domain.Core.Alert.Services;
    using Domain.EventJournal.Services;
    using Domain.FinanceManagement.FinancePlan.Services;
    using Domain.FinanceManagement.Payment.Services.PaymentAllocation;
    using Domain.FinanceManagement.Services;
    using Domain.Rager.Services;
    using Helpers;
    using Helpers.FinancePlan;
    using NUnit.Framework;
    using Console = System.Console;

    [TestFixture]
    public class MockBuilderTests
    {
        //[Test]
        public void GenerateIMock()
        {
            //provide type of service you want to build
            //provide the folder name (assumes Ivh.Common.Tests.Helpers)
            //string service = MockBuilderCodeGenerator.GenerateIMockBuilder(typeof(VisitAgeService), "VisitAge");
            string service = MockBuilderCodeGenerator.GenerateIMockBuilder(typeof(FinancePlanIncentiveService), "FinancePlan");
            Console.WriteLine(service);
        }

    }
}

﻿namespace Ivh.Common.Tests
{
    using AutoMapper;
    using NUnit.Framework;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using DependencyInjection.Registration;

    public static class MappingValidator
    {
        private const bool AllowEnumToInt = true;
        private const bool AllowIntToEnum = true;
        private const bool AllowIntToString = true;
        private const bool AllowNullableToNotNullable = true;

        public static void Validate(Action<MappingBuilder> setup)
        {
            using (MappingBuilder mappingBuilder = new MappingBuilder())
            {
                setup?.Invoke(mappingBuilder);
            }

            // run automappers built in validation
            RunAutomapperValidation();

            // run custom validations
            IList<MappingValidationResult> results = new List<MappingValidationResult>();
            foreach (TypeMap typeMap in Mapper.Configuration.GetAllTypeMaps())
            {
                foreach (PropertyMap propertyMap in typeMap.GetPropertyMaps().Where(x => x.SourceType != null && x.SourceType.IsValueType))
                {
                    MappingValidationResult validate = new MappingValidationResult(typeMap, propertyMap);
                    if (!validate.IsValid())
                    {
                        results.Add(validate);
                    }
                }
            }

            Mapper.Reset();

            foreach (MappingValidationResult result in results)
            {
                Console.WriteLine(result.ToString());
                Console.WriteLine();
            }

            Assert.AreEqual(0, results.Count);
        }

        private static void RunAutomapperValidation()
        {
            // disable member list checking
            foreach (TypeMap map in Mapper.Configuration.GetAllTypeMaps())
            {
                map.ConfiguredMemberList = MemberList.None;
            }

            Assert.DoesNotThrow(() => Mapper.Configuration.AssertConfigurationIsValid());
        }

        private class MappingValidationResult
        {
            private readonly PropertyMap _propertyMap;

            public MappingValidationResult(TypeMap typeMap, PropertyMap propertyMap)
            {
                this._propertyMap = propertyMap;
                this.SourceType = typeMap.SourceType;
                this.DestinationType = typeMap.DestinationType;

                this.SourcePropertyType = propertyMap.SourceType;
                this.DestinationPropertyType = propertyMap.DestinationPropertyType;

                this.SourceMemberName = propertyMap.SourceMember?.Name;
                this.DestinationMemberName = propertyMap.DestinationProperty.Name;
            }

            private Type SourceType { get; }

            private Type DestinationType { get; }

            private Type SourcePropertyType { get; }

            private Type DestinationPropertyType { get; }

            private string SourceMemberName { get; }

            private string DestinationMemberName { get; }

            public bool IsValid()
            {
                if (this._propertyMap.Ignored)
                {
                    return true;
                }

                if (this.DestinationPropertyType.IsAssignableFrom(this.SourcePropertyType))
                {
                    return true;
                }
                
                if (AllowEnumToInt)
                {
                    if (this.DestinationPropertyType == typeof(int) && this.SourcePropertyType.IsEnum)
                    {
                        return true;
                    }
                }

                if (AllowIntToString)
                {
                    if (this.DestinationPropertyType == typeof(string) && this.SourcePropertyType == typeof(int))
                    {
                        return true;
                    }
                }

                if (AllowIntToEnum)
                {
                    if (this.DestinationPropertyType.IsEnum && this.SourcePropertyType == typeof(int))
                    {
                        return true;
                    }
                }

                Type checkSourcePropertyType = this.SourcePropertyType;
                if (AllowNullableToNotNullable)
                {
                    if (this.SourcePropertyType.IsGenericType &&
                        this.SourcePropertyType.GetGenericTypeDefinition() == typeof(Nullable<>))
                    {
                        checkSourcePropertyType = Nullable.GetUnderlyingType(this.SourcePropertyType);
                    }
                }

                return checkSourcePropertyType == this.DestinationPropertyType;
            }

            public override string ToString()
            {
                string s = $"{this.SourceType}, {this.DestinationType}{Environment.NewLine}";
                s += $"{this.SourceMemberName} ({this.SourcePropertyType}) => {this.DestinationMemberName} ({this.DestinationPropertyType})";

                s = s.Replace("Ivh.", "");

                return s;
            }
        }
    }
}

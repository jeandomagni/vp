﻿namespace Ivh.Common.Tests.Builders
{
    using System;
    using System.Collections.Generic;
    using Base.Enums;
    using Base.Utilities.Helpers;
    using Domain.Guarantor.Entities;
    using Domain.Visit.Entities;
    using Faker;
    using State.Interfaces;
    using VisitPay.Enums;

    public class VisitBuilder : EntityBuilder<VisitBuilder, Visit>
    {
        /// <summary>
        ///     Sets up a Visit with Default values with an initial charging transaction of $500
        /// </summary>
        public VisitBuilder WithDefaultValues(decimal initialAmount = 500m)
        {
            this.Builder.With(visit =>
            {
                visit.VisitId = RandomIdGenerator.Next();
                visit.VisitDescription = Lorem.Sentence(4);
                visit.BillingSystem = new BillingSystem {BillingSystemId = 4, BillingSystemName = "Providence Epic"};
                visit.SourceSystemKey = RandomSourceSystemKeyGenerator.New("NNNNNNNNNN");
                visit.BillingApplication = "HB";
                visit.DischargeDate = DateTime.UtcNow.AddDays(-15);
                this.WithVisitTransActions(this.InitialTransaction(initialAmount));
                this.SetCurrentBalance(initialAmount);
                this.AddVisitState(VisitStateEnum.Active, DateTime.UtcNow);
            });

            return this.Builder;
        }

        public VisitBuilder AddVisitState(VisitStateEnum visitStateEnum, DateTime insertDate)
        {
            ((IEntity<VisitStateEnum>) this.Entity).SetState(visitStateEnum, Lorem.Word());
            ((IEntity<VisitStateEnum>) this.Entity).GetCurrentStateHistory().DateTime = insertDate;

            return this.Builder;
        }

        public VisitBuilder SetCurrentBalance(decimal currentBalance)
        {
            this.Entity.SetCurrentBalance(currentBalance);
            return this.Builder;
        }

        public VisitBuilder WithVisitTransActions(params VisitTransaction[] transactions)
        {
            this.Entity.UnpublishedVisitBalanceChanged = new List<VisitBalanceChangedEvent>();
            foreach (VisitTransaction transaction in transactions)
            {
                //this.Entity.AddVisitTransaction(transaction);
            }

            return this.Builder;
        }

        public VisitBuilder AddVisitTransActions(params VisitTransaction[] transactions)
        {
            foreach (VisitTransaction transaction in transactions)
            {
                //this.Entity.AddVisitTransaction(transaction);
            }

            return this.Builder;
        }

        public VisitBuilder WithGuarantor(Guarantor guarantor)
        {
            this.Entity.VPGuarantor = guarantor;
            return this.Builder;
        }


        private VisitTransaction InitialTransaction(decimal initialAmount)
        {
            DateTime commonDate = DateTime.UtcNow.AddDays(-5);
            VisitTransaction visitTransaction = new VisitTransactionBuilder().AsCharge(this.Entity, initialAmount, commonDate);
            return visitTransaction;
        }
    }
}
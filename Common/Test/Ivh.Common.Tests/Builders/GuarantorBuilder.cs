﻿namespace Ivh.Common.Tests.Builders
{
    using Domain.Guarantor.Entities;
    using Domain.Guarantor.Interfaces;
    using Domain.User.Entities;
    using VisitPay.Enums;

    public class GuarantorBuilder : EntityBuilder<GuarantorBuilder, Guarantor>
    {
        private static volatile int _lastGuarantorId = 0;
        private static volatile int _lastUserId = 0;

        public GuarantorBuilder WithDefaults(int? vpGuarantorId = null, int? visitPayUserId = null)
        {
            this.Builder.With(x =>
            {
                x.VpGuarantorId = vpGuarantorId ?? ++_lastGuarantorId;
            });

            this.Builder.WithUser(new VisitPayUser
            {
                VisitPayUserId = visitPayUserId ?? ++_lastUserId
            });

            return this.Builder;
        }

        public GuarantorBuilder WithUser(VisitPayUser user)
        {
            this.Builder.With(x => 
            {
                x.User = user;
            });

            return this.Builder;
        }

        public GuarantorBuilder WithType(GuarantorTypeEnum guarantorTypeEnum)
        {
            this.Builder.With(x =>
            {
                x.SetGuarantorType(guarantorTypeEnum);
            });

            return this.Builder;
        }

        public GuarantorBuilder AsOffline()
        {
            this.Builder.WithType(GuarantorTypeEnum.Offline);

            return this.Builder;
        }

        public GuarantorBuilder AsOnline()
        {
            this.Builder.WithType(GuarantorTypeEnum.Online);

            return this.Builder;
        }

        public GuarantorBuilder WithInitialStatus(VpGuarantorStatusEnum initialStatusEnum)
        {
            this.Builder.With(x => 
            {
                x.VpGuarantorStatus = initialStatusEnum;
            });

            return this.Builder;
        }

        public GuarantorBuilder UseAutopay(bool useAutopay = true)
        {
            this.Builder.With(x =>
            {
                x.UseAutoPay = useAutopay;
            });

            return this.Builder;
        }
    }
}

﻿namespace Ivh.Common.Tests.Builders
{
    using System;
    using Cache;
    using Helpers;
    using Moq;
    using ResiliencePolicies.Interfaces;
    using ResiliencePolicies.Policies;

    public class HttpRequestPolicyMockBuilder : IMockBuilder<HttpRequestPolicy, HttpRequestPolicyMockBuilder>
    {
        public ISimpleCircuitBreakerPolicy SimpleCircuitBreakerPolicy;

        public Mock<IDistributedCache> DistributedCacheMock;
        public Mock<ICacheProvider> CacheProviderMock;
        public Mock<ISimpleCircuitBreakerPolicy> SimpleCircuitBreakerPolicyMock;

        public HttpRequestPolicyMockBuilder()
        {
            this.DistributedCacheMock = new Mock<IDistributedCache>();
            this.CacheProviderMock = new Mock<ICacheProvider>();
            this.SimpleCircuitBreakerPolicyMock = new Mock<ISimpleCircuitBreakerPolicy>();
        }

        public HttpRequestPolicy CreateService()
        {
            return new HttpRequestPolicy(
                this.CacheProviderMock.Object,
                this.DistributedCacheMock.Object,
                this.SimpleCircuitBreakerPolicy ?? this.SimpleCircuitBreakerPolicyMock.Object
                );
        }

        public HttpRequestPolicyMockBuilder Setup(Action<HttpRequestPolicyMockBuilder> configAction)
        {
            configAction(this);
            return this;
        }
    }
}

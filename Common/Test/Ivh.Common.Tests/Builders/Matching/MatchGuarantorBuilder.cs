﻿namespace Ivh.Common.Tests.Builders.Matching
{
    using System;
    using Domain.Matching.Entities;

    public class MatchGuarantorBuilder : EntityBuilder<MatchGuarantorBuilder, Guarantor>
    {
        public MatchGuarantorBuilder WithDefaults()
        {
            this.Builder.With(x =>
            {
                x.FirstName = "FirstName";
                x.LastName = "LastName";
                x.DOB = new DateTime(1970, 1, 1);
                x.SourceSystemKey = "guarantorSsk";
                x.HsBillingSystemId = 1;
            });

            this.WithAddress("123 test st", "Somewhere", "ID", "83705");
            this.WithSsn("123-45-7890");

            return this.Builder;
        }

        public MatchGuarantorBuilder WithInformation(string firstName, string lastName, DateTime dateOfBirth, string sourceSystemKey, string ssn)
        {
            this.Builder.With(x =>
            {
                x.FirstName = firstName;
                x.LastName = lastName;
                x.DOB = dateOfBirth;
                x.SourceSystemKey = sourceSystemKey;
                x.HsBillingSystemId = 1;
            });

            this.WithSsn(ssn);

            return this.Builder;
        }

        public MatchGuarantorBuilder WithSsn(string ssn)
        {
            this.Builder.With(x =>
            {
                x.SSN = ssn;

                x.SSN4 = ssn?.Substring(ssn.Length - 4, 4);

            });

            return this.Builder;
        }

        public MatchGuarantorBuilder WithAddress(string address1, string city, string state, string zip)
        {
            this.Builder.With(x =>
            {
                x.AddressLine1 = address1;
                x.City = city;
                x.StateProvince = state;
                x.PostalCode = zip;
            });

            return this.Builder;
        }
    }
}

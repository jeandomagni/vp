﻿namespace Ivh.Common.Tests.Builders.Matching
{
    using System.Collections.Generic;
    using System.Linq;
    using Base.Utilities.Extensions;
    using Domain.Matching.Entities;

    public class MatchDataGroupBuilder : EntityBuilder<MatchDataGroupBuilder, MatchDataGroup>
    {
        public MatchDataGroupBuilder WithGuarantor(Guarantor matchGuarantor)
        {
            this.Builder.With(x =>
            {
                x.Guarantor = matchGuarantor;
            });

            return this.Builder;
        }

        public MatchDataGroupBuilder WithPatients(ICollection<Patient> patients)
        {
            this.Builder.With(x =>
            {
                x.Visits = x.Visits ?? new List<MatchVisitData>();
                x.Visits.AddRange(patients.Select(p => new MatchVisitData{Patient = p}));
            });

            return this.Builder;
        }

        public MatchDataGroupBuilder WithPatients(params Patient[] patients)
        {
            return this.Builder.WithPatients(patients.ToList());
        }
    }
}

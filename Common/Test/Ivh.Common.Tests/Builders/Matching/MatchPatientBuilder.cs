﻿namespace Ivh.Common.Tests.Builders.Matching
{
    using System;
    using Domain.Matching.Entities;

    public class MatchPatientBuilder : EntityBuilder<MatchPatientBuilder, Patient>
    {
        public MatchPatientBuilder WithDefaults()
        {
            this.Builder.With(x =>
            {
                x.HsGuarantorId = RandomIdGenerator.Next();
                x.VisitId = RandomIdGenerator.Next();
            });

            return this.Builder;
        }

        public MatchPatientBuilder WithDob(DateTime dateOfBirth)
        {
            this.Builder.With(x =>
            {
                x.PatientDOB = dateOfBirth;
            });

            return this.Builder;
        }

        public MatchPatientBuilder WithHsGuarantorId(int id)
        {
            this.Builder.With(x =>
            {
                x.HsGuarantorId = id;
            });

            return this.Builder;
        }
    }
}

﻿namespace Ivh.Common.Tests.Builders.Matching
{
    using System.Collections.Generic;
    using Base.Utilities.Extensions;
    using Domain.Matching.Match;
    using Domain.Matching.Sets;
    using VisitPay.Enums;

    public class GuarantorMatchingSetBuilder : EntityBuilder<GuarantorMatchingSetBuilder, Set>
    {
        private static readonly IDictionary<string, MatchSetConfigurationRegexEnum> EmptyConfig = new Dictionary<string, MatchSetConfigurationRegexEnum>();
        private static readonly IDictionary<string, MatchSetConfigurationRegexEnum> RemoveAppendedFacilityCodeConfiguration = new Dictionary<string, MatchSetConfigurationRegexEnum>()
        {
            ["Matching.Field.Guarantor.SourceSystemKey.Regex"] = MatchSetConfigurationRegexEnum.RemoveMinistryPrefix
        };

        private IDictionary<string, MatchSetConfigurationRegexEnum> GetMatchFieldRegexConfiguration(MatchSetConfigurationRegexEnum? configEnum)
        {
            switch (configEnum)
            {
                case MatchSetConfigurationRegexEnum.RemoveMinistryPrefix:
                    return RemoveAppendedFacilityCodeConfiguration;
                default:
                    return EmptyConfig;
            }
        }

        public GuarantorMatchingSetBuilder ForGuestPay(MatchSetConfigurationRegexEnum? matchSetConfigurationRegexEnum = null)
        {
            IDictionary<string, MatchSetConfigurationRegexEnum> config = this.GetMatchFieldRegexConfiguration(matchSetConfigurationRegexEnum);

            this.Entity = new GuarantorMatchingSet004(config, ApplicationEnum.GuestPay.ToListOfOne());

            return this.Builder;
        }

        public GuarantorMatchingSetBuilder ForVisitPay(PatternSetEnum setEnum, MatchSetConfigurationRegexEnum? matchSetConfigurationRegexEnum = null)
        {
            Set GetSetForEnum(PatternSetEnum patternSetEnum)
            {
                IList<ApplicationEnum> applications = ApplicationEnum.VisitPay.ToListOfOne();
                IDictionary<string, MatchSetConfigurationRegexEnum> config = this.GetMatchFieldRegexConfiguration(matchSetConfigurationRegexEnum);
                switch (patternSetEnum)
                {
                    case PatternSetEnum.S001:
                        return new GuarantorMatchingSet001(config, applications);
                    case PatternSetEnum.S002:                                  
                        return new GuarantorMatchingSet002(config, applications);
                    case PatternSetEnum.S003:                                  
                        return new GuarantorMatchingSet003(config, applications);
                    case PatternSetEnum.S004:                                  
                        return new GuarantorMatchingSet004(config, applications);
                    case PatternSetEnum.S005:                                  
                        return new GuarantorMatchingSet005(config, applications);
                    default:
                        return null;
                }
            }

            this.Entity = GetSetForEnum(setEnum);

            return this.Builder;
        }

        #region static helpers
        public static Set VisitPay(PatternSetEnum setEnum, MatchSetConfigurationRegexEnum? matchSetConfigurationRegexEnum = null)
        {
            return new GuarantorMatchingSetBuilder().ForVisitPay(setEnum, matchSetConfigurationRegexEnum);
        }

        public static Set GuestPay(MatchSetConfigurationRegexEnum? matchSetConfigurationRegexEnum = null)
        {
            return new GuarantorMatchingSetBuilder().ForGuestPay(matchSetConfigurationRegexEnum);
        }
        #endregion
    }
}

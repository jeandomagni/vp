﻿namespace Ivh.Common.Tests.Builders
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Application.FinanceManagement.Common.Dtos;
    using Domain.Visit.Entities;
    using Domain.FinanceManagement.FinancePlan.Entities;
    using Domain.FinanceManagement.Statement.Entities;
    using Helpers.FinancePlan;
    using Helpers.Guarantor;
    using VisitPay.Enums;

    public class FinancePlanBuilder : EntityBuilder<FinancePlanBuilder, FinancePlan>
    {
        /// <summary>
        /// Sets up a FinancePlan with interest rate and a FinancePlanVisit with a charge of 500
        /// This should be close to a working graph needed for unit testing. 
        /// If you find properties that would generally be set in real world scenarios, add them here
        /// </summary>
        public FinancePlanBuilder WithDefaultValues()
        {
            // add your defaults here 
            this.Builder.With(x =>
            {
                int financePlanId = RandomIdGenerator.Next();
                x.InsertDate = DateTime.UtcNow;
                x.VpGuarantor = GuarantorFactory.GenerateOnline();
                x.CreatedVpStatement = new VpStatement();
                x.FinancePlanId = financePlanId;
                x.OriginationDate = DateTime.UtcNow;
                x.FinancePlanVisits = new List<FinancePlanVisit> {this.DefaultFinancePlanVisit()};
                x.FinancePlanStatus = new FinancePlanStatus {FinancePlanStatusId = (int) FinancePlanStatusEnum.TermsAccepted};
                x.SetCreditAgreement(new FinancePlanCreditAgreementCollectionDto
                {
                    UserLocale = new FinancePlanCreditAgreementDto
                    {
                        AgreementText = string.Empty,
                        ContractNumber = Guid.NewGuid(),
                        Locale = "en-US",
                        TermsCmsVersionId = 0
                    }
                }, DateTime.UtcNow, 0);
            });

            return this.Builder;
        }

        public FinancePlanBuilder AddFinancePlanInterestRateHistory(params decimal[] amounts)
        {
            foreach (decimal amount in amounts)
            {
                int nextId = this.GenerateNextId(this.Entity.FinancePlanInterestRateHistory?.OrderByDescending(x => x.FinancePlanInterestRateHistoryId).FirstOrDefault()?.FinancePlanInterestRateHistoryId);
                FinancePlanInterestRateHistory rateHistory = new FinancePlanInterestRateHistory
                {
                    FinancePlanInterestRateHistoryId = nextId,
                    FinancePlan = this.Entity,
                    InterestRate = amount
                };
                IList<FinancePlanInterestRateHistory> entityFinancePlanInterestRateHistory = this.Entity.FinancePlanInterestRateHistory;
                entityFinancePlanInterestRateHistory?.Add(rateHistory);
            }
            return this.Builder;
        }

        // Usage: Select the last id from a collection. If your collection is null it will generate an id, otherwise it will increment it
        private int GenerateNextId(int? lastId)
        {
            if (lastId.HasValue)
            {
                return lastId.Value + 1;
            }

            return Faker.Number.RandomNumber();
        }

        public FinancePlanBuilder AddClosedFinancePlanStatusHistory(DateTime closedDate)
        {
            return this.AddFinancePlanStatusHistory(closedDate, FinancePlanStatusEnum.Canceled);
        }

        public FinancePlanBuilder AddFinancePlanStatusHistory(DateTime insertDate, FinancePlanStatusEnum financePlan)
        {
            FinancePlanStatus financePlanStatus = new FinancePlanStatus { FinancePlanStatusId = (int)financePlan };
            FinancePlanStatusHistory financePlanStatusHistory = new FinancePlanStatusHistory
            {
                InsertDate = insertDate,
                FinancePlan = this.Entity,
                FinancePlanStatus = financePlanStatus
            };
            this.Entity.FinancePlanStatusHistory.Add(financePlanStatusHistory);
            return this.Builder;
        }

        public static implicit operator FinancePlan(FinancePlanBuilder instance)
        {
            return instance.Build();
        }

        private FinancePlanVisit DefaultFinancePlanVisit()
        {
            Visit visit = new VisitBuilder().WithDefaultValues().AddVisitState(VisitStateEnum.Active, DateTime.UtcNow.AddDays(-1)).Build();
            FinancePlanVisit fpVisit = FinancePlanFactory.VisitToFinancePlanVisit(new Tuple<Visit, IList<VisitTransaction>>(visit, new List<VisitTransaction>()), this.Entity);
            fpVisit.FinancePlanVisitId = Faker.Number.RandomNumber();
            fpVisit.InsertDate = DateTime.UtcNow;
            fpVisit.OriginatedSnapshotVisitBalance = fpVisit.CurrentBalance;

            return fpVisit;
        }

    }
}

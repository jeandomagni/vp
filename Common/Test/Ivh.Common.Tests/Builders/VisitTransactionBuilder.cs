﻿namespace Ivh.Common.Tests.Builders
{
    using System;
    using Base.Enums;
    using Domain.Visit.Entities;
    using VisitPay.Enums;

    public class VisitTransactionBuilder : EntityBuilder<VisitTransactionBuilder, VisitTransaction>
    {
        public VisitTransactionBuilder()
        {
            this.Entity.VisitTransactionId = Faker.Number.RandomNumber();
        }

        public VisitTransaction AsCharge(Visit visit, decimal transactionAmount, DateTime? transactionDate = null )
        {
            return this.AddTransaction(visit, transactionAmount, VpTransactionTypeEnum.HsCharge, transactionDate);
        }
        public VisitTransaction AsPrincipalPayment(Visit visit, decimal transactionAmount, DateTime? transactionDate = null)
        {
            return this.AddTransaction(visit, transactionAmount *-1, VpTransactionTypeEnum.HsPayorCash, transactionDate);
        }

        public VisitTransaction AsInterestPayment(Visit visit, decimal transactionAmount, DateTime? transactionDate = null)
        {
            return this.AddTransaction(visit, transactionAmount * -1, VpTransactionTypeEnum.HsInterestPayment, transactionDate);
        }

        public VisitTransaction AsAssessedInterest(Visit visit, decimal transactionAmount, DateTime? transactionDate = null)
        {
            return this.AddTransaction(visit, transactionAmount, VpTransactionTypeEnum.HsInterestAssessed, transactionDate);
        }

        private VisitTransaction AddTransaction(Visit visit, decimal transactionAmount, VpTransactionTypeEnum vpTransactionTypeEnum, DateTime? transactionDate = null)
        {
             DateTime commonDate = DateTime.UtcNow;
            if (transactionDate.HasValue) commonDate = transactionDate.Value;

            VpTransactionType vpTransactionType = new VpTransactionType { VpTransactionTypeId = (int)vpTransactionTypeEnum };
            this.Builder.With(transaction =>
            {
                transaction.OverrideInsertDate = commonDate;
                transaction.Visit = visit;
                transaction.TransactionDate = commonDate.Date;
                transaction.PostDate = commonDate.Date;
                transaction.VpTransactionType = vpTransactionType;
                transaction.TransactionDescription = $"{vpTransactionType.ToString()} for VisitId {visit.VisitId}";
                transaction.VisitTransactionAmount = new VisitTransactionAmount
                {
                    VisitTransactionAmountId = Faker.Number.RandomNumber(),
                    InsertDate = commonDate,
                    TransactionAmount = transactionAmount
                };
            });
            return this.Builder.Build();
        }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ivh.Common.Tests.Builders.Rager
{
    using Base.Enums;
    using Base.Utilities.Helpers;
    using Domain.Rager.Entities;
    using VisitPay.Enums;

    public class VisitBuilder : EntityBuilder<VisitBuilder, Visit>
    {
        public VisitBuilder WithDefaultValues()
        {
            this.Builder.With(visit =>
            {
                visit.VisitId = RandomIdGenerator.Next();
                visit.VisitSourceSystemKey = RandomSourceSystemKeyGenerator.New("NNNNNNNNNN");
                visit.VisitBillingSystemId = 1;
            });

            return this.Builder;
        }

        public VisitBuilder WithVisitAges(int visitAgeCount)
        {
            IEnumerable<int> iterations = Enumerable.Range(0, visitAgeCount);
            int snapshotAge = 0;
            foreach(int i in iterations)
            {
                snapshotAge++;
                VisitAge visitAge = new VisitAge {Age = 1, Visit = this.Entity};
                this.Entity.VisitAges.Add(visitAge);
            }
            return this.Builder;
        }

        public VisitBuilder WithCommunications(Dictionary<AgingCommunicationTypeEnum, int> communications)
        {
            foreach (KeyValuePair<AgingCommunicationTypeEnum, int> keyValuePair in communications)
            {
                IEnumerable<int> iterations = Enumerable.Range(0, keyValuePair.Value);
                foreach(int i in iterations)
                {
                    CommunicationType communicationType = new CommunicationType { CommunicationTypeId = (int)keyValuePair.Key };
                    this.Entity.VisitCommunications.Add(new VisitCommunication{CommunicationType = communicationType, CommunciationIncrement = 1, Visit = this.Entity});
                }
            }
            return this.Builder;
        }

    }
}

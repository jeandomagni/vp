﻿namespace Ivh.Common.Tests.Builders
{
    using System;

    public class EntityBuilder<TBuilder, TEntity>
        where TBuilder : EntityBuilder<TBuilder, TEntity>
        where TEntity : class, new()
    {
        protected TBuilder Builder;
        protected TEntity Entity;
        public static Random RandomIdGenerator = new Random();
        protected EntityBuilder()
        {
            this.Entity = new TEntity();
            this.Builder = (TBuilder)this;
        }

        public TEntity Build()
        {
            TEntity result = this.Entity;
            // I am commenting "this.Entity = null" for now. 
            // By not setting the Entity to null allows us to make asserts
            // then call further methods on the builder make additional asserts 
            // without creating a new builder and reproducing a graph
            //this.Entity = null;
            return result;
        }

        public EntityBuilder<TBuilder, TEntity> With(Action<TEntity> action)
        {
            action(this.Entity);
            return this;
        }

        public EntityBuilder<TBuilder, TEntity> Clear()
        {
            this.Entity = Activator.CreateInstance<TEntity>();
            return this.Builder;
        }

        //implicitly build the entity when assigning the builder to entity type
        public static implicit operator TEntity(EntityBuilder<TBuilder, TEntity> instance)
        {
            return instance.Build();
        }

    }
}
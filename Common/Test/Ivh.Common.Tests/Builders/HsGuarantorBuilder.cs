﻿namespace Ivh.Common.Tests.Builders
{
    using System;
    using Application.Base.Common.Interfaces.Entities.HsGuarantor;
    using Base.Utilities.Helpers;
    using Domain.HospitalData.HsGuarantor.Entities;
    using Faker;
    using Helpers;

    public class HsGuarantorBuilder : EntityBuilder<HsGuarantorBuilder, HsGuarantor>
    {
        private static volatile int _lastHsGuarantorId = 0;

        public HsGuarantorBuilder WithRandomPersonalInformation()
        {
            this.Builder.With(x =>
            {
                x.SourceSystemKey = RandomSourceSystemKeyGenerator.New("AAA-BANANA");
                x.FirstName = Lorem.Word();
                x.LastName = Lorem.Word();
                x.AddressLine1 = Lorem.Sentence();
                x.City = RandomSourceSystemKeyGenerator.New("CityAAAAA");
                x.StateProvince = RandomSourceSystemKeyGenerator.New("St8AAAAAA");
                x.PostalCode = RandomSourceSystemKeyGenerator.New("NNNNN");
                
                // 18-110 years old
                x.DOB = DateTime.UtcNow.AddYears(-18).AddDays(-RandomIdGenerator.Next(365 * (110-18)));
            });

            this.WithValidSsn();

            return this.Builder;
        }

        public HsGuarantorBuilder WithSsn(string ssn)
        {
            this.Builder.With(x =>
            {
                x.SSN = ssn;
                x.SSN4 = x.SSN.Substring(x.SSN.Length - 4, 4);
            });

            return this.Builder;
        }

        public HsGuarantorBuilder WithValidSsn()
        {
            this.Builder.WithSsn(RandomSsnGenerator.New());

            return this.Builder;
        }

        public HsGuarantorBuilder WithInvalidSsn()
        {
            this.Builder.WithSsn("999999999");

            return this.Builder;
        }

        public HsGuarantorBuilder WithDefaults(int? id = null)
        {
            this.Builder.With(x =>
            {
                x.HsGuarantorId = id ?? ++_lastHsGuarantorId;
                x.HsBillingSystemId = 1;
                x.DataChangeDate = DateTime.UtcNow.AddDays(-1);
            });

            return this.Builder;
        }

        public HsGuarantorBuilder ThatMatchesHsGuarantorWithHighConfidence(IHsGuarantor guarantor)
        {
            this.WithDefaults();

            this.Builder.With(x =>
            {
                x.SourceSystemKey = RandomSourceSystemKeyGenerator.New("AAA-BANANA");
                x.FirstName = guarantor.FirstName;
                x.LastName = guarantor.LastName;
                x.SSN = guarantor.SSN;
                x.SSN4 = guarantor.SSN4;
                x.AddressLine1 = guarantor.AddressLine1;
                x.City = guarantor.City;
                x.StateProvince = guarantor.StateProvince;
                x.PostalCode = guarantor.PostalCode;
                x.DOB = guarantor.DOB;
            });

            return this.Builder;
        }

        public HsGuarantorBuilder ThatMatchesHsGuarantorWithLowConfidence(IHsGuarantor guarantor)
        {
            this.WithDefaults();

            this.Builder.With(x =>
            {
                x.SourceSystemKey = RandomSourceSystemKeyGenerator.New("AAA-BANANA");
                x.FirstName = guarantor.FirstName;
                x.LastName = guarantor.LastName;
                x.SSN = guarantor.SSN;
                x.SSN4 = guarantor.SSN4;
                x.AddressLine1 = guarantor.AddressLine1;
                x.City = guarantor.City;
                x.StateProvince = guarantor.StateProvince;
                x.PostalCode = RandomSourceSystemKeyGenerator.New("NNNNN");
                x.DOB = guarantor.DOB?.AddYears(7) ?? DateTime.UtcNow.Date;
            });

            return this.Builder;
        }


        public HsGuarantorBuilder ThatAlmostMatchesHsGuarantor(IHsGuarantor guarantor)
        {
            this.WithDefaults();

            this.Builder.With(x =>
            {
                x.SourceSystemKey = RandomSourceSystemKeyGenerator.New("AAA-BANANA");
                x.FirstName = guarantor.FirstName;
                x.LastName = guarantor.LastName;
                x.AddressLine1 = guarantor.AddressLine1;
                x.City = guarantor.City;
                x.StateProvince = guarantor.StateProvince;
                x.PostalCode = guarantor.PostalCode;
                x.DOB = guarantor.DOB;
            });

            this.WithValidSsn();

            return this.Builder;
        }

        public HsGuarantorBuilder ThatMatchesHsGuarantorLastName(IHsGuarantor guarantor)
        {
            this.WithDefaults();

            this.Builder.With(x =>
            {
                x.SourceSystemKey = RandomSourceSystemKeyGenerator.New("AAA-BANANA");
                x.FirstName = Lorem.Word();
                x.LastName = guarantor.LastName;
                x.AddressLine1 = Lorem.Sentence();
                x.City = Lorem.Word();
                x.StateProvince = guarantor.StateProvince;
                x.PostalCode = RandomSourceSystemKeyGenerator.New("NNNNN");
                x.DOB = guarantor.DOB;
            });

            this.WithValidSsn();

            return this.Builder;
        }
    }
}

﻿namespace Ivh.Common.Tests.Builders
{
    using System;
    using Base.Utilities.Helpers;
    using Domain.HospitalData.Visit.Entities;
    using Faker;
    using VisitPay.Enums;

    public class HsVisitBuilder : EntityBuilder<HsVisitBuilder, Visit>
    {

        /// <summary>
        ///     Sets up a Cdi Hospital Visit with Default values
        /// </summary>
        public HsVisitBuilder WithDefaultValues(
            int daysSinceFirstSelfPayDate = 30,
            SelfPayClassEnum primaryPayClassEnum = SelfPayClassEnum.SelfPayAfterInsurance,
            bool evaluateForPcFlag = false,
            int? visitId = null,
            decimal initialAmount = 500m)
        {
            this.Builder.With(visit =>
            {
                this.SetVisitId(visitId);
                visit.VisitDescription = Lorem.Sentence(4);
                visit.SourceSystemKey = RandomSourceSystemKeyGenerator.New("NNNNNNNNNN");
                visit.DischargeDate = DateTime.UtcNow.AddDays(-15);
                visit.EvaluateForPCSegmentation = evaluateForPcFlag;
                this.SetCurrentBalance(initialAmount);
                this.SetPrimaryInsuranceType(primaryPayClassEnum);
                this.SetFirstSelfPayDate(daysSinceFirstSelfPayDate);
            });

            return this.Builder;
        }

        private void SetVisitId(int? visitId)
        {
            this.Entity.VisitId = visitId ?? RandomIdGenerator.Next();
        }

        public HsVisitBuilder SetCurrentBalance(decimal currentBalance)
        {
            this.Entity.HsCurrentBalance = currentBalance;
            this.Entity.SelfPayBalance = currentBalance;
            return this.Builder;
        }

        public HsVisitBuilder SetPrimaryInsuranceType(SelfPayClassEnum primaryPayClassEnum)
        {
            this.Entity.PrimaryInsuranceType = new PrimaryInsuranceType()
            {
                SelfPayClass = primaryPayClassEnum
            };
            return this.Builder;
        }

        public HsVisitBuilder SetFirstSelfPayDate(int daysSinceFirstSelfPayDate)
        {
            this.Entity.FirstSelfPayDate = DateTime.Today.AddDays(-daysSinceFirstSelfPayDate);
            return this.Builder;
        }
    }
}


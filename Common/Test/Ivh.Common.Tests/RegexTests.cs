﻿namespace Ivh.Common.Tests
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Text.RegularExpressions;
    using NUnit.Framework;
    using VisitPay.Strings;

    [TestFixture]
    public class RegexTests
    {
        [Test]
        public void EmailRegexTest()
        {
            // business rule UM-103
            Regex r = new Regex(RegexStrings.EmailAddress);

            IDictionary<string, bool> emailAddresses = new Dictionary<string, bool>
            {
                { "moliver@ivincihealth.com", true },
                { "moliver@ivincihealth.co", true },
                { "moliver@ivincihealth.info", true },
                { "aaa@aaa.com", true },
                { "aaa@aa.com", true },
                { "aaa@a.com", true },
                { "aa@aaa.com", true },
                { "a@aaa.com", true },
                { "a!@a.com", true},
                { "!a@a.com", true},
                { "a+@a.com", true},
                { "+a@a.com", true},
                { "a.a@a.com", true},
                { "a.@a.com", true},
                
                { "moliver@ivincihealth.c", false },
                { "moliver@ivincihealth.infoo", false },
                { "moliver@ivincihealth.123", false },
                { " moliver@ivincihealth.com", false },
                { "moliver @ivincihealth.com", false },
                { "moliver@ ivincihealth.com", false },
                { "@a.com", false},
                { "aaa@a,a.com", false },
                { "aaa@a,.com", false },
                { "a,a@a.com", false },
                { "a,@a.com", false },
                { ",a@a.com", false },
                { ".a@a.com", false},
            };

            emailAddresses.ToList().ForEach(address =>
            {
                Match m = r.Match(address.Key);
                Assert.AreEqual(address.Value, m.Success);
                //Trace.WriteLine(m.Success, address.Key);
            });
        }
    }
}

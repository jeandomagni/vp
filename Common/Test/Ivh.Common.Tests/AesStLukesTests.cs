﻿namespace Ivh.Common.Tests
{
    using System;
    using System.Linq;
    using System.Security.Cryptography;
    using System.Text;
    using System.Web;
    using Encryption;
    using NUnit.Framework;

    [TestFixture]
    public class AesStLukesTests
    {

        [Test]
        public void TestEncryptAndDecrypt_AesStLukes_GetString()
        {
            byte[] aesKey = AESThenHMAC.NewKey();

            string stringToEncryptAndDecript = "TestString";

            var myEncryptedBytes = AesStLukes.AesEncryptBytes(AesStLukes.GetBytes(stringToEncryptAndDecript), aesKey);

            Assert.IsNotNull(myEncryptedBytes);

            var unencryptedBytes = AesStLukes.AesDecryptBytes(myEncryptedBytes, aesKey);

            var unencryptedString = AesStLukes.GetString(unencryptedBytes);

            Assert.AreEqual(stringToEncryptAndDecript, unencryptedString);
        }

        [Test]
        public void TestEncryptAndDecrypt_AesStLukes_GetString_Utf8_WithStLukesLikeData()
        {
            //This is the key in the dev webconfig
            string aesKey = "0A80B203F9654211B1732C32C31A3315";

            string stringToEncryptAndDecript = "time=" + HttpUtility.UrlEncode("20160908004800") +
                "&MyChartID=" + HttpUtility.UrlEncode("1234") +
                "&FNAME=" + HttpUtility.UrlEncode("Statement") + 
                "&LNAME=" + HttpUtility.UrlEncode("GUY") + 
                "&birthdate=" + HttpUtility.UrlEncode("9/8/1976");

            var myEncryptedBytes = AesStLukes.AesEncryptBytes(System.Text.Encoding.UTF8.GetBytes(stringToEncryptAndDecript), aesKey);

            Console.WriteLine("EncryptedString(Base64 & UrlEncoded) = {0}", HttpUtility.UrlEncode(Convert.ToBase64String(myEncryptedBytes)));

            Assert.IsNotNull(myEncryptedBytes);

            var unencryptedBytes = AesStLukes.AesDecryptBytes(myEncryptedBytes, aesKey);

            var unencryptedString = System.Text.Encoding.UTF8.GetString(unencryptedBytes);

            Assert.AreEqual(stringToEncryptAndDecript, unencryptedString);
        }

        [Test]
        public void TestEncryptAndDecrypt_AesStLukes_Base64()
        {
            string keyString = "0A80B203F9654211B1732C32C31A3315";
            byte[] aesKey = AesStLukes.AesCryptDeriveKey(keyString);

            string stringToEncryptAndDecript = "TestString2";
            byte[] myBytesToEncrypt = System.Text.Encoding.UTF8.GetBytes(stringToEncryptAndDecript);
            byte[] myEncryptedBytes = AesStLukes.AesEncryptBytes(myBytesToEncrypt, aesKey);
            Assert.IsNotNull(myEncryptedBytes);

            string encodedEncryptedString = Convert.ToBase64String(myEncryptedBytes);
            //This is what could potentially hit the wire.
            Assert.IsNotNull(encodedEncryptedString);

            //This is what the controller would recieve
            byte[] encryptedBytesAgain = Convert.FromBase64String(encodedEncryptedString);
            var unencryptedBytes = AesStLukes.AesDecryptBytes(encryptedBytesAgain, aesKey);
            var unencryptedString = System.Text.Encoding.UTF8.GetString(unencryptedBytes);

            //Make sure the whole round trip worked.
            Assert.AreEqual(stringToEncryptAndDecript, unencryptedString);
        }


        [Test]
        public void EpicExample()
        {
            //This example is from: https://open.epic.com/Launchpad/HttpGetSso
            string shhhThisIsASecret = "shhh-this is a secret!";
            byte[] aesKey = AesStLukes.AesCryptDeriveKey(shhhThisIsASecret);

            //string stringToDecrypt = "xmV4yrj3MYe6A0SbNuo5%2F%2BGbNfKXRBBD545kEXXwiwspE9lMAVDs9uitKDi2n5hK#";
            string stringToDecrypt = "xmV4yrj3MYe6A0SbNuo5%2F%2BGbNfKXRBBD545kEXXwiwspE9lMAVDs9uitKDi2n5hK";
            string decodedUrl = HttpUtility.UrlDecode(stringToDecrypt);
            byte[] encryptedBytes = Convert.FromBase64String(decodedUrl);

            byte[] unencryptedBytes = AesStLukes.AesDecryptBytes(encryptedBytes, aesKey);
            string unencryptedString = System.Text.Encoding.UTF8.GetString(unencryptedBytes);


            Assert.AreEqual("encounter=20708&mrn=E3878&user=OT", unencryptedString);
        }
    }
}
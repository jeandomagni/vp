﻿using NUnit.Framework;

namespace Ivh.Common.Tests
{
    using System.Collections.Generic;
    using System.Collections.Specialized;
    using System.Linq;
    using System.Net;
    using Web.Constants;
    using Web.Extensions;

    [TestFixture]
    public class HttpRequestExtentionsTests
    {
        public void ForwardedForAllValid()
        {
            string originalClientAddress = "192.168.0.1";
            string intermediateProxyAddress = "192.168.0.2";
            string httpRequestUserHostAddress = "192.168.0.3";
            NameValueCollection headers = new NameValueCollection
            {
                {HttpHeaders.Request.XForwardedFor, $"{intermediateProxyAddress},{originalClientAddress}"}
            };
            IEnumerable<IPAddress> proxiedAddressChain = HttpRequestExtensions.GetProxiedAddressChain(headers, httpRequestUserHostAddress);
            IPAddress xForwardedFor = proxiedAddressChain.Last();
            Assert.AreEqual(originalClientAddress, xForwardedFor.ToString());
        }

        [Test]
        public void ForwardedForExtraneousInvalid()
        {
            string originalClientAddress = "192.168.0.100000";
            string intermediateProxyAddress = "192.168.0.2";
            string httpRequestUserHostAddress = "192.168.0.3";
            NameValueCollection headers = new NameValueCollection
            {
                {HttpHeaders.Request.XForwardedFor, $"{intermediateProxyAddress},{originalClientAddress}"}
            };
            IEnumerable<IPAddress> proxiedAddressChain = HttpRequestExtensions.GetProxiedAddressChain(headers, httpRequestUserHostAddress);
            IPAddress xForwardedFor = proxiedAddressChain.Last();
            Assert.AreEqual(intermediateProxyAddress, xForwardedFor.ToString());
        }

        public void ForwardedForExtraneousChars()
        {
            string originalClientAddress = "+192.168.0.1";
            string intermediateProxyAddress = "192.168.0.2";
            string httpRequestUserHostAddress = "192.168.0.3";
            NameValueCollection headers = new NameValueCollection
            {
                {HttpHeaders.Request.XForwardedFor, $"{intermediateProxyAddress},{originalClientAddress}"}
            };
            IEnumerable<IPAddress> proxiedAddressChain = HttpRequestExtensions.GetProxiedAddressChain(headers, httpRequestUserHostAddress);
            IPAddress xForwardedFor = proxiedAddressChain.Last();
            Assert.AreEqual("192.168.0.1", xForwardedFor.ToString());
        }
    }
}
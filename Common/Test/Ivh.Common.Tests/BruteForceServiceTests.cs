﻿namespace Ivh.Common.Tests
{
    using System;
    using System.Collections.Concurrent;
    using System.Threading.Tasks;
    using Cache;
    using Moq;
    using NUnit.Framework;
    using Web.Interfaces;
    using Web.Services;

    [TestFixture]
    public class BruteForceServiceTests
    {
        //private static int attemptCount = default(int);
        //private static DateTime? lastAttempt = default(DateTime?);

        private IBruteForceDelayService BuildBruteForceDelayService(Action<Mock<IDistributedCache>> supplementalConfiguration = null)
        {
            Mock<IDistributedCache> distributedCacheMock = new Mock<IDistributedCache>();

            ConcurrentDictionary<string, DateTime> cacheLastAttempt = new ConcurrentDictionary<string, DateTime>();
            ConcurrentDictionary<string, int> cacheAttemptCount = new ConcurrentDictionary<string, int>();

            distributedCacheMock.Setup(x => x.SetObjectAsync(It.IsAny<string>(), It.IsAny<int>(), It.IsAny<int>()))
                .Callback<string, object, int>((string key, object value, int timeout) =>
            {
                cacheAttemptCount[key] = (int)value;
            }).ReturnsAsync(true);

            distributedCacheMock.Setup(x => x.SetObjectAsync(It.IsAny<string>(), It.IsAny<DateTime>(), It.IsAny<int>())).Callback<string, object, int>((string key, object value, int timeout) =>
            {
                cacheLastAttempt[key] = (DateTime)value;
            }).ReturnsAsync(true);

            distributedCacheMock.Setup(x => x.GetObjectAsync<int>(It.IsAny<string>())).ReturnsAsync((string x) =>
            {
                if (cacheAttemptCount.ContainsKey(x))
                    return cacheAttemptCount[x];
                return default(int);
            });
            distributedCacheMock.Setup(x => x.GetObjectAsync<DateTime?>(It.IsAny<string>())).ReturnsAsync((string x) =>
            {

                if (cacheLastAttempt.ContainsKey(x))
                    return cacheLastAttempt[x];
                return default(DateTime);
            });

            if (supplementalConfiguration != null)
            {
                supplementalConfiguration(distributedCacheMock);
            }

            return new BruteForceDelayService(new Lazy<IDistributedCache>(() => distributedCacheMock.Object));
        }

        [Test]
        public async Task NoDelay()
        {
            IBruteForceDelayService bruteForceDelayService = this.BuildBruteForceDelayService();
            string ipAddress = "127.0.0.1";
            TimeSpan delay = await bruteForceDelayService.DelayAsync(ipAddress, () => false, () => true);
            Assert.AreEqual(0, delay.TotalMilliseconds);
        }

        [TestCase(1, 0, 1)]
        [TestCase(2, 0, 3)]
        [TestCase(3, 0, 9)]
        [TestCase(4, 0, 27)]
        [TestCase(1, 2000, 1)]
        [TestCase(2, 2000, 2)]
        [TestCase(3, 2000, 4)]
        [TestCase(4, 2000, 8)]
        public async Task Delayed(int iterations, int pauseBetween, int finalDelay)
        {
            IBruteForceDelayService bruteForceDelayService = this.BuildBruteForceDelayService();
            string ipAddress = "127.0.0.1";
            TimeSpan delay = default(TimeSpan);

            for (int i = 0; i < iterations; i++)
            {
                delay = await bruteForceDelayService.DelayAsync(ipAddress, () => true, () => false);
                if (pauseBetween > 0)
                {
                    await Task.Delay(TimeSpan.FromMilliseconds(pauseBetween));
                }
            }

            Assert.AreEqual(finalDelay, delay.TotalMilliseconds);
        }

        [Test]
        public async Task ExceptionThrown()
        {
            IBruteForceDelayService bruteForceDelayService = this.BuildBruteForceDelayService(y =>
            {
                y.Setup(x => x.GetObjectAsync<int>(It.IsAny<string>())).Throws<NullReferenceException>();
            });
            string ipAddress = "127.0.0.1";
            TimeSpan delay = await bruteForceDelayService.DelayAsync(ipAddress, () => true);
            Assert.AreEqual(BruteForceDelayService.ErrorConditionDefaultDelayMilliseconds, delay.TotalMilliseconds);
        }
    }
}

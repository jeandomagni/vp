﻿namespace Ivh.Common.Tests
{
    using Base.Versioning;
    using NUnit.Framework;

    [TestFixture]
    public class ApplicationVersionTests
    {
        [Test]
        public void StringEquals()
        {
            string version = "1.1.1.1";
            ApplicationVersion version1 = new ApplicationVersion(version);

            Assert.AreEqual(version1.ToString(), version);
        }

        [Test]
        public void Equals()
        {
            ApplicationVersion version1 = new ApplicationVersion("1.1.1.1");
            ApplicationVersion version2 = new ApplicationVersion("1.1.1.1");

            Assert.AreEqual(version1, version2);
        }

        [Test]
        public void NotEquals()
        {
            ApplicationVersion version1 = new ApplicationVersion("1.1.1.1");
            ApplicationVersion version2 = new ApplicationVersion("1.1.1.2");

            Assert.AreNotEqual(version1, version2);
        }

        [Test]
        public void GreaterThan()
        {
            ApplicationVersion version1 = new ApplicationVersion("1.1.1.1");
            ApplicationVersion version2 = new ApplicationVersion("1.1.1.2");
            
            Assert.IsTrue(version2 > version1);
        }

        [Test]
        public void LessThan()
        {
            ApplicationVersion version1 = new ApplicationVersion("1.1.1.1");
            ApplicationVersion version2 = new ApplicationVersion("1.1.1.2");

            Assert.IsTrue(version2 > version1);
        }

        [Test]
        public void GreaterThanEqualWhereGreaterThan()
        {
            ApplicationVersion version1 = new ApplicationVersion("1.1.1.1");
            ApplicationVersion version2 = new ApplicationVersion("1.1.1.2");

            Assert.IsTrue(version2 >= version1);
        }

        [Test]
        public void LessThanEqualWhereLessThan()
        {
            ApplicationVersion version1 = new ApplicationVersion("1.1.1.1");
            ApplicationVersion version2 = new ApplicationVersion("1.1.1.2");

            Assert.IsTrue(version2 >= version1);
        }

        [Test]
        public void GreaterThanEqualWhereEqual()
        {
            ApplicationVersion version1 = new ApplicationVersion("1.1.1.1");
            ApplicationVersion version2 = new ApplicationVersion("1.1.1.1");

            Assert.IsTrue(version2 >= version1);
        }

        [Test]
        public void LessThanEqualWhereEqual()
        {
            ApplicationVersion version1 = new ApplicationVersion("1.1.1.1");
            ApplicationVersion version2 = new ApplicationVersion("1.1.1.1");

            Assert.IsTrue(version2 >= version1);
        }

        [Test]
        public void CompareToEquals()
        {
            ApplicationVersion version1 = new ApplicationVersion("1.1.1.1");
            ApplicationVersion version2 = new ApplicationVersion("1.1.1.1");

            Assert.IsTrue(version1.CompareTo(version2) == 0);
        }

        [Test]
        public void CompareToGreaterThan()
        {
            ApplicationVersion version1 = new ApplicationVersion("1.1.1.1");
            ApplicationVersion version2 = new ApplicationVersion("1.1.1.2");
            int relative = version2.CompareTo(version1);
            Assert.IsTrue( relative > 0);
        }

        [Test]
        public void CompareToLessThan()
        {
            ApplicationVersion version1 = new ApplicationVersion("1.1.1.1");
            ApplicationVersion version2 = new ApplicationVersion("1.1.1.2");
            int relative = version1.CompareTo(version2);
            Assert.IsTrue(relative < 0);
        }
    }
}

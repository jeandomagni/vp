﻿namespace Ivh.Common.Tests
{
    using System;
    using Base.Utilities.Extensions;
    using Base.Utilities.Helpers;
    using NUnit.Framework;

    [TestFixture]
    public class DateTests
    {
        [Test]
        public void ToLocalDateTime_AdjustsGivenTimeIntoLocal_Andback_Eastern()
        {
            DateTime timeToCheckAgainst = DateTime.UtcNow.Date; //Get Midnight
            
            string easternStandardTime = "Eastern Standard Time";
            DateTime valueToCheck = timeToCheckAgainst.ToLocalDateTime(DateTimeHelper.TimeZoneOffset(easternStandardTime));
            DateTime backAgain = valueToCheck.ToUtc(DateTimeHelper.TimeZoneOffset(easternStandardTime));
            
            Assert.AreEqual(backAgain.ToUniversalTime(), timeToCheckAgainst.ToUniversalTime());
        }

        [Test]
        public void ToLocalDateTime_AdjustsGivenTimeIntoLocal_Andback_Operations()
        {
            DateTime timeToCheckAgainst = DateTime.UtcNow.Date; //Get Midnight
            
            string standardTime = TimeZoneHelper.TimeZones.Operations;
            DateTime valueToCheck = timeToCheckAgainst.ToLocalDateTime(DateTimeHelper.TimeZoneOffset(standardTime));
            DateTime backAgain = valueToCheck.ToUtc(DateTimeHelper.TimeZoneOffset(standardTime));

            Assert.AreEqual(backAgain.ToUniversalTime(), timeToCheckAgainst.ToUniversalTime());
        }

        [Test]
        public void ToLocalDateTime_AdjustsGivenTimeIntoLocal_Andback_Pacific()
        {
            DateTime timeToCheckAgainst = new DateTime(2016, 04, 7, 13, 0, 0); //Get Midnight
            //This example illistrates the example in the documentation for ToLocalDateTime

            string standardTime = "Pacific Standard Time";
            DateTime valueToCheck = timeToCheckAgainst.ToLocalDateTime(DateTimeHelper.TimeZoneOffset(standardTime));
            DateTime backAgain = valueToCheck.ToUtc(DateTimeHelper.TimeZoneOffset(standardTime));
            
            Assert.AreEqual(backAgain.ToUniversalTime(), timeToCheckAgainst.ToUniversalTime());
        }

        //[Test]
        [Obsolete]
        public void ToMountainDaylightTime()
        {
            //TimeZoneInfo pst = TimeZoneInfo.FindSystemTimeZoneById("Pacific Standard Time");
            //DateTime dt = new DateTime(2017, 7, 4, 13, 0, 0); //2017 independence day
            //TimeSpan offset = pst.GetUtcOffset(dt);
            //DateTime independencePst = new DateTime(dt, offset);
            //DateTime independenceMst = independencePst.ToMountainDateTime();

            //Assert.IsTrue(independenceMst.Hours == -6); //Mountain Daylight Time Offset
            //Assert.IsTrue(independenceMst.Hour == 14); //Mountain Daylight Time hour
            //Assert.IsTrue(independenceMst.Minute == 0); //Mountain Daylight Time minutes
            //Assert.IsTrue(independenceMst.Second == 0); //Mountain Daylight Time seconds
            //Assert.IsTrue(independenceMst.Year == 2017); //Mountain Daylight Time year
            //Assert.IsTrue(independenceMst.Month == 7); //Mountain Daylight Time month
            //Assert.IsTrue(independenceMst.Day == 4); //Mountain Daylight Time day
        }

        //[Test]
        [Obsolete]
        public void ToMountainStandardTime()
        {
            //TimeZoneInfo est = TimeZoneInfo.FindSystemTimeZoneById("Eastern Standard Time");
            //DateTime dt = new DateTime(2017, 1, 20, 12, 0, 0); //2017 inauguration
            //TimeSpan offset = est.GetUtcOffset(dt);
            //DateTime inaugurationEst = new DateTime(dt, offset);
            //DateTime inaugurationMst = inaugurationEst.ToMountainDateTime();

            //Assert.IsTrue(inaugurationMst.Offset.Hours == -7); //Mountain Standard Time Offset
            //Assert.IsTrue(inaugurationMst.Hour == 10); //Mountain Standard Time hour
            //Assert.IsTrue(inaugurationMst.Minute == 0); //Mountain Standard Time minutes
            //Assert.IsTrue(inaugurationMst.Second == 0); //Mountain Standard Time seconds
            //Assert.IsTrue(inaugurationMst.Year == 2017); //Mountain Standard Time year
            //Assert.IsTrue(inaugurationMst.Month == 1); //Mountain Standard Time month
            //Assert.IsTrue(inaugurationMst.Day == 20); //Mountain Standard Time day
        }
    }
}
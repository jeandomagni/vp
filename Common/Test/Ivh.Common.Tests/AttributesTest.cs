﻿namespace Ivh.Common.Tests
{
    using System;
    using System.Collections.Generic;
    using Base.Attributes;
    using Base.Enums;
    using Newtonsoft.Json;
    using NUnit.Framework;
    using VisitPay.Enums;

    [TestFixture]
    public class AttributesTest
    {
        [Test]
        public void AttributeTest()
        {
            bool HARType = true;
            int AccountStatus = 12345;
            DateTime AdmittingProvider = new DateTime(2016, 07, 14, 1, 3, 4);
            string CentralTeam = "this is a test string";

            IDictionary<string, Tuple<string, string>> values = new Dictionary<string, Tuple<string, string>>
            {
                {AttributeTypeEnum.HARType.ToString(), new Tuple<string, string>(HARType.ToString(),HARType.ToString())},
                {AttributeTypeEnum.AccountStatus.ToString(),  new Tuple<string, string>(AccountStatus.ToString(),AccountStatus.ToString())},
                {AttributeTypeEnum.AdmittingProvider.ToString(),  new Tuple<string, string>(AdmittingProvider.ToString(),AdmittingProvider.ToString())},
                {AttributeTypeEnum.CentralTeam.ToString(),  new Tuple<string, string>(CentralTeam.ToString(),CentralTeam.ToString())}
            };
            string jsonValues = JsonConvert.SerializeObject(values);
            TestAttributes testAttributes = AttributesFactory<TestAttributes>.GetAttributes(jsonValues);

            Assert.IsTrue(testAttributes.HasKeys);
            Assert.IsTrue(testAttributes.HasValues);
            Assert.AreEqual(testAttributes.TestAttributeGroup.HARType.Value, HARType);
            Assert.AreEqual(testAttributes.TestAttributeGroup.AccountStatus.Value, AccountStatus);
            Assert.AreEqual(testAttributes.TestAttributeGroup.AdmittingProvider.Value, AdmittingProvider);
            Assert.AreEqual(testAttributes.TestAttributeGroup.CentralTeam.Value, CentralTeam);
            Assert.AreEqual(testAttributes.TestAttributeGroup.HARType.Description, HARType.ToString());
            Assert.AreEqual(testAttributes.TestAttributeGroup.AccountStatus.Description, AccountStatus.ToString());
            Assert.AreEqual(testAttributes.TestAttributeGroup.AdmittingProvider.Description, AdmittingProvider.ToString());
            Assert.AreEqual(testAttributes.TestAttributeGroup.CentralTeam.Description, CentralTeam.ToString());
            Assert.IsTrue(testAttributes.TestAttributeGroup.HasKeys);
            Assert.IsTrue(testAttributes.TestAttributeGroup.HasValues);
        }

        public class TestAttributeGroup : AttributeGroup
        {
            public Attribute<bool> HARType = new Attribute<bool>(AttributeTypeEnum.HARType.ToString());
            public Attribute<int> AccountStatus = new Attribute<int>(AttributeTypeEnum.AccountStatus.ToString());
            public Attribute<DateTime> AdmittingProvider = new Attribute<DateTime>(AttributeTypeEnum.AdmittingProvider.ToString());
            public Attribute<string> CentralTeam = new Attribute<string>(AttributeTypeEnum.CentralTeam.ToString());
        }

        public class TestAttributes : Attributes
        {
            public TestAttributeGroup TestAttributeGroup = new TestAttributeGroup();
        }

        private enum AttributeTypeEnum
        {
            HARType = 6,
            AccountStatus = 8,
            AdmittingProvider = 10,
            CentralTeam = 14
        }

    }
}
﻿namespace Ivh.Common.JobRunner.Common.Interfaces
{
    using System;

    public interface IJobRunner : IDisposable
    {
        void Execute(string[] args = null);
    }
}
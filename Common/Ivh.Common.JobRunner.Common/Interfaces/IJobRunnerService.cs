﻿namespace Ivh.Common.JobRunner.Common.Interfaces
{
    using System;

    public interface IJobRunnerService
    {

    }

    public interface IJobRunnerService<in TJob> : IJobRunnerService
        where TJob : IJob
    {
        void Execute(DateTime begin, DateTime end, string[] args);
    }
}
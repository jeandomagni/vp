﻿namespace Ivh.Common.JobRunner.Common.Interfaces
{
    using Autofac;

    public interface IJob
    {
        IComponentContext Context { get; }

        decimal? ResultsActualValue();
        decimal? ResultsExpectedValue();
    }
}

﻿namespace Ivh.Common.JobRunner.Common
{
    using Autofac;
    using Interfaces;

    public abstract class Job : IJob
    {
        protected Job(IComponentContext context)
        {
            this.Context = context;
        }

        public IComponentContext Context { get; }
        public virtual decimal? ResultsActualValue()
        {
            return null;
        }

        public virtual decimal? ResultsExpectedValue()
        {
            return null;
        }
    }
}
﻿namespace Ivh.Common.ServiceBus.Common.Extensions
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Linq.Expressions;
    using System.Reflection;
    using System.Text;

    public static class Extensions
    {
        public static string GetExceptionDetails(this Exception exception)
        {
            PropertyInfo[] properties = exception.GetType()
                .GetProperties();
            IEnumerable<string> fields = properties
                .Select(property => new
                {
                    Name = property.Name,
                    Value = property.GetValue(exception, null)
                })
                .Select(x => $"{x.Name} = {(x.Value != null ? x.Value.ToString() : string.Empty)}");
            return string.Join("\n", fields);
        }

        public static string AggregateExceptionToString(this Exception e)
        {
            StringBuilder returnString = new StringBuilder();
            Exception currentException = e;
            while (currentException != null)
            {
                returnString.AppendLine(GetExceptionDetails(currentException));

                if (currentException.Data.Count > 0)
                {
                    foreach (object key in currentException.Data)
                    {
                        returnString.AppendLine($"{key}: {currentException.Data[key]}");
                    }
                }

                currentException = currentException.InnerException;
            }
            return returnString.ToString();
        }

        public static string GetPropertyName<T>(this Expression<Func<T, object>> expression)
        {
            if (expression.Body is MemberExpression memberExpression)
            {
                return memberExpression.Member.Name;
            }
            else if (expression.Body is MethodCallExpression callExpression)
            {
                return callExpression.Method.Name;
            }
            else if (expression.Body is UnaryExpression unaryExpression)
            {
                Expression op = unaryExpression.Operand;
                return ((MemberExpression)op).Member.Name;
            }
            else
            {
                return $"Error In {nameof(GetPropertyName)}";
            }
        }
    }
}

﻿namespace Ivh.Common.ServiceBus.Attributes
{
    using System;

    public class ScheduledDeliveryRangeAttribute : Attribute
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="now"></param>
        /// <returns></returns>
        public virtual bool IsInDeliveryRange(TimeSpan startAfterMidnight, TimeSpan endAfterMidnight, DateTime now)
        {
            return now.CompareTo(now.Date.Add(startAfterMidnight)) >= 0 && now.CompareTo(now.Date.Add(endAfterMidnight)) < 0;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="now"></param>
        /// <returns></returns>
        public virtual TimeSpan GetDeliveryDelay(TimeSpan startAfterMidnight, TimeSpan endAfterMidnight, DateTime now)
        {
            if (now.TimeOfDay.TotalMinutes <= startAfterMidnight.TotalMinutes)
            {
                return now.Date.AddMinutes(startAfterMidnight.TotalMinutes) - now;
            }
            else if (now.TimeOfDay.TotalMinutes >= endAfterMidnight.TotalMinutes)
            {
                return now.Date.AddMinutes(startAfterMidnight.TotalMinutes).AddDays(1) - now;
            }
            else
            {
                return now - now;
            }
        }
    }
}
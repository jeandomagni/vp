﻿namespace Ivh.Common.ServiceBus.Attributes
{
    using System;
    using System.Globalization;

    public class ClientFriendlyDeliveryRangeAttribute : ScheduledDeliveryRangeAttribute
    {
        public bool IsInDeliveryRange(string friendlyCommunicationDeliveryRange, DateTime clientNow)
        {
            Tuple<TimeSpan, TimeSpan> range = this.ParseFriendlyCommunicationDeliveryRange(friendlyCommunicationDeliveryRange);
            return base.IsInDeliveryRange(range.Item1, range.Item2, clientNow);
        }

        public virtual TimeSpan GetDeliveryDelay(string friendlyCommunicationDeliveryRange, DateTime clientNow)
        {
            Tuple<TimeSpan, TimeSpan> range = this.ParseFriendlyCommunicationDeliveryRange(friendlyCommunicationDeliveryRange);
            return base.GetDeliveryDelay(range.Item1, range.Item2, clientNow);
        }

        public Tuple<TimeSpan, TimeSpan> ParseFriendlyCommunicationDeliveryRange(string friendlyCommunicationDeliveryRange)
        {
            string clientFriendlyDelivery = friendlyCommunicationDeliveryRange;
            string[] range = clientFriendlyDelivery.Split("-".ToCharArray());

            DateTime startTime, endTime;
            if (DateTime.TryParseExact(range[0], "HH:mm", new CultureInfo("en-US"), DateTimeStyles.None, out startTime)
                && DateTime.TryParseExact(range[1], "HH:mm", new CultureInfo("en-US"), DateTimeStyles.None, out endTime))
            {
                TimeSpan startAfterMidnight = TimeSpan.FromMinutes(startTime.TimeOfDay.TotalMinutes);
                TimeSpan endAfterMidnight = TimeSpan.FromMinutes(endTime.TimeOfDay.TotalMinutes);
                return new Tuple<TimeSpan, TimeSpan>(startAfterMidnight, endAfterMidnight);
            }
            throw new Exception($"{nameof(ClientFriendlyDeliveryRangeAttribute)}::{nameof(this.ParseFriendlyCommunicationDeliveryRange)}: Could not parse {friendlyCommunicationDeliveryRange}");
        }
    }
}

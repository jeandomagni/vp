﻿namespace Ivh.Common.ServiceBus.Attributes
{
    using System;

    [AttributeUsage(AttributeTargets.Class)]
    public class UniversalMessageAttribute : Attribute
    {
        public bool Consumable { get; private set; }


        public UniversalMessageAttribute(bool consumable = true)
        {
            this.Consumable = consumable;
        }
    }
}

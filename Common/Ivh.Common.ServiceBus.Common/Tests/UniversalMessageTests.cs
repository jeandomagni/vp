﻿namespace Ivh.Common.ServiceBus.Common.Tests
{
    using System;
    using System.Runtime.Serialization;
    using System.Threading;
    using Messages;
    using NUnit.Framework;

    [TestFixture]
    public class UniversalMessageTests
    {
        [Test]
        public void TestErrorMessage()
        {
            Assert.DoesNotThrow(() =>
            {
                TestMessage testMessage = BuildTestMessage();
                string errorMessage = testMessage.ErrorMessage;

                Assert.IsNotNull(errorMessage);
                Assert.AreNotEqual(0, errorMessage.Length);
            });
        }

        [Test]
        public void TestErrorMessageWithNull()
        {
            Assert.DoesNotThrow(() =>
            {
                TestMessage testMessage = BuildTestMessage();
                testMessage.Value1 = null;
                string errorMessage = testMessage.ErrorMessage;

                Assert.IsNotNull(errorMessage);
                Assert.AreNotEqual(0, errorMessage.Length);
            });
        }

        [Test]
        public void TestErrorMessageWithOverrider()
        {
            Assert.DoesNotThrow(() =>
            {
                TestMessageWithOVerride testMessage = BuildTestMessageWithOVerride();
                string errorMessage = testMessage.ErrorMessage;

                Assert.IsNotNull(errorMessage);
                Assert.AreNotEqual(0, errorMessage.Length);
            });
        }
         [Test]
        public void TestErrorMessageWithOverrideWithNull()
        {
            Assert.DoesNotThrow(() =>
            {
                TestMessageWithOVerride testMessage = BuildTestMessageWithOVerride();
                testMessage.Value1 = null;
                string errorMessage = testMessage.ErrorMessage;

                Assert.IsNotNull(errorMessage);
                Assert.AreNotEqual(0, errorMessage.Length);
            });
        }


        [Test]
        public void TestErrorMessageList()
        {
            Assert.DoesNotThrow(() =>
            {
                TestMessageList testMessageList = BuildTestMessageList(5);
                string errorMessage = testMessageList.ErrorMessage;

                Assert.IsNotNull(errorMessage);
                Assert.AreNotEqual(0, errorMessage.Length);
            });
        }

        [Test]
        public void TestErrorMessageWithOverrideList()
        {
            Assert.DoesNotThrow(() =>
            {
                TestMessageWithOverrideList testMessageList = BuildTestMessageWithOverrideList(5);
                string errorMessage = testMessageList.ErrorMessage;

                Assert.IsNotNull(errorMessage);
                Assert.AreNotEqual(0, errorMessage.Length);
            });
        }

        private static TestMessage BuildTestMessage()
        {
            Thread.Sleep(1);
            return new TestMessage() {CorrelationId = Guid.NewGuid(), Value1 = "ABCDEF", Value2 = 12345, Value3 = DateTime.UtcNow, Value4 = Guid.NewGuid()};
        }

        private static TestMessageWithOVerride BuildTestMessageWithOVerride()
        {
            Thread.Sleep(1);
            return new TestMessageWithOVerride() {CorrelationId = Guid.NewGuid(), Value1 = "ABCDEF", Value2 = 12345, Value3 = DateTime.UtcNow, Value4 = Guid.NewGuid()};
        }

        private static TestMessageList BuildTestMessageList(int count)
        {
            TestMessageList testMessageList = new TestMessageList();
            for (int i = 0; i < count; i++)
            {
                testMessageList.Messages.Add(BuildTestMessage());
            }
            return testMessageList;
        }

        private static TestMessageWithOverrideList BuildTestMessageWithOverrideList(int count)
        {
            TestMessageWithOverrideList testMessageList = new TestMessageWithOverrideList();
            for (int i = 0; i < count; i++)
            {
                testMessageList.Messages.Add(BuildTestMessageWithOVerride());
            }
            return testMessageList;
        }
    }

    [DataContract]
    [Serializable]
    public class TestMessage : UniversalMessage<TestMessage>
    {
        [DataMember]
        public string Value1 { get; set; }
        [DataMember]
        public int Value2 { get; set; }
        [DataMember]
        public DateTime Value3 { get; set; }
        [DataMember]
        public Guid Value4 { get; set; }

    }

    [DataContract]
    [Serializable]
    public class TestMessageWithOVerride : UniversalMessage<TestMessageWithOVerride>
    {
        [DataMember]
        public string Value1 { get; set; }
        [DataMember]
        public int Value2 { get; set; }
        [DataMember]
        public DateTime Value3 { get; set; }
        [DataMember]
        public Guid Value4 { get; set; }
        [IgnoreDataMember]
        public override string ErrorMessage => this.FormatErrorMessage(this, x => x.Value1, x => x.Value2, x => x.Value3, x => x.Value4);
    }

    [DataContract]
    [Serializable]
    public class TestMessageList : UniversalMessageList<TestMessage>
    {
    }

    [DataContract]
    [Serializable]
    public class TestMessageWithOverrideList : UniversalMessageList<TestMessageWithOVerride>
    {
    }
}

﻿namespace Ivh.Common.ServiceBus.Common.Messages
{
    using System;
    using System.Reflection;
    using System.Runtime.Serialization;
    using Attributes;
    using GreenPipes.Internals.Extensions;

    [Serializable]
    [DataContract]
    public abstract class PrioritizedMessage
    {
        public bool IsSchedulable => TypeExtensions.HasAttribute<ClientFriendlyDeliveryRangeAttribute>(this.GetType());

        public virtual TimeSpan GetDeliveryDelay(string friendlyCommunicationDeliveryRange, DateTime clientNow)
        {
            TimeSpan deliveryDelay;
            this.TryGetAttributeValue<ClientFriendlyDeliveryRangeAttribute, TimeSpan>(this, x => x?.GetDeliveryDelay(friendlyCommunicationDeliveryRange, clientNow) ?? TimeSpan.FromDays(1), out deliveryDelay);
            return deliveryDelay;
        }

        public virtual bool ShouldBeScheduled(string friendlyCommunicationDeliveryRange, DateTime clientNow)
        {
            bool shouldBeScheduled = false;
            this.TryGetAttributeValue<ClientFriendlyDeliveryRangeAttribute, bool>(this, x => !x?.IsInDeliveryRange(friendlyCommunicationDeliveryRange, clientNow) ?? false, out shouldBeScheduled);
            return shouldBeScheduled;
        }

        [DataMember(Order = int.MaxValue)]
        public Guid CorrelationId { get; set; }

        #region helpers
        private bool TryGetAttributeValue<T, TValue>(object current, Func<T, TValue> valueFunc, out TValue value) where T : Attribute
        {
            return this.TryGetAttributeValue(current.GetType(), valueFunc, out value);
        }

        private bool TryGetAttributeValue<T, TValue>(Type current, Func<T, TValue> valueFunc, out TValue value) where T : Attribute
        {
            T attribute = (T)Attribute.GetCustomAttribute(current, typeof(T));
            if (attribute != null)
            {
                try
                {
                    value = valueFunc(attribute);
                    return true;
                }
                catch (Exception)
                {
                    // ignored
                }
            }
            value = default(TValue);
            return false;
        }
        #endregion
    }
}

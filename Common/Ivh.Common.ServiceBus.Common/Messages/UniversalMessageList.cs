﻿namespace Ivh.Common.ServiceBus.Common.Messages
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Runtime.Serialization;

    [Serializable]
    [DataContract]
    public abstract class UniversalMessageList<TMessage> : UniversalMessage where TMessage : UniversalMessage
    {
        protected UniversalMessageList(IList<TMessage> messages = null)
        {
            this.Messages = messages ?? new List<TMessage>();
        }

        [DataMember(Order = 1)]
        public IList<TMessage> Messages { get; set; }

        private string DefaultErrorMessage => $"{typeof(TMessage).Name} [{nameof(this.CorrelationId)}: {this.CorrelationId}]";
        [IgnoreDataMember]
        public override string ErrorMessage => $"{this.FormatErrorMessage(this.Messages)}";

        private string DefaultTraceMessage => $"{typeof(TMessage).Name} [{nameof(this.CorrelationId)}: {this.CorrelationId}]";
        [IgnoreDataMember]
        public override string TraceMessage => this.DefaultTraceMessage;

        protected string FormatErrorMessage(IList<TMessage> messageList, string additionalText = null)
        {
            IEnumerable<string> parts()
            {
                yield return this.DefaultErrorMessage;
                if (!string.IsNullOrEmpty(additionalText))
                {
                    yield return additionalText;
                }
                if (messageList != null && messageList.Any())
                {
                    yield return typeof(TMessage).Name;
                    foreach (TMessage message in messageList)
                    {
                        yield return message.ErrorMessage;
                    }
                }
            }

            try
            {
                return string.Join("\r\n\t", parts());
            }
            catch (Exception ex)
            {
                try
                {
                    return $"{this.DefaultErrorMessage} : Exception {Extensions.Extensions.AggregateExceptionToString(new Exception($"Exception thrown in UniversalMessage<{typeof(TMessage).Name}>.{nameof(this.FormatErrorMessage)}", ex))}";
                }
                catch
                {
                    return $"{this.DefaultErrorMessage}";
                }
            }
        }
    }
}

﻿namespace Ivh.Common.ServiceBus.Common.Messages
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Linq.Expressions;
    using System.Reflection;
    using System.Runtime.Serialization;
    using System.Text;

    [Serializable]
    [DataContract]
    public abstract class UniversalMessage : PrioritizedMessage
    {
        public abstract string ErrorMessage { get; }
        public abstract string TraceMessage { get; }

        [DataMember(Order = int.MaxValue - 100)]
        public MessageTiming Timing = new MessageTiming();
    }

    [Serializable]
    [DataContract]
    public class MessageTiming
    {
        public MessageTiming()
        {
            this.Publish = DateTime.UtcNow;
            this.BeginConsume = DateTime.UtcNow;
            this.EndConsume = DateTime.UtcNow;
        }

        [DataMember(Order = 1)]
        public DateTime Publish { get; set; }
        [DataMember(Order = 2)]
        public DateTime BeginConsume { get; set; }
        [DataMember(Order = 3)]
        public DateTime EndConsume { get; set; }

        public TimeSpan Queue => this.BeginConsume - this.Publish;
        public TimeSpan Consume => this.EndConsume - this.BeginConsume;
        public TimeSpan Total => this.EndConsume - this.Publish;

        public string Time => $"Queue Time: {(this.Queue.Ticks / (double)TimeSpan.TicksPerMillisecond).ToString("0.00").PadLeft(7)} ms / Consume Time: {(this.Consume.Ticks / (double)TimeSpan.TicksPerMillisecond).ToString("0.00").PadLeft(7)} ms";
    }

    [Serializable]
    [DataContract]
    public abstract class UniversalMessage<TMessage> : UniversalMessage where TMessage : UniversalMessage
    {
        private string DefaultErrorMessage => $"{typeof(TMessage).Name} [{nameof(this.CorrelationId)}: {this.CorrelationId}]";
        [IgnoreDataMember]
        public override string ErrorMessage => this.DefaultErrorMessage;

        private string DefaultTraceMessage => $"{typeof(TMessage).Name} [{nameof(this.CorrelationId)}: {this.CorrelationId}]";
        [IgnoreDataMember]
        public override string TraceMessage => this.DefaultTraceMessage;

        protected string FormatErrorMessage(TMessage message, params Expression<Func<TMessage, object>>[] members)
        {
            return this.FormatErrorMessage(message, null, members);
        }

        protected string FormatErrorMessage(TMessage message, string additionalText = null, params Expression<Func<TMessage, object>>[] members)
        {
            IEnumerable<string> parts()
            {
                yield return this.DefaultErrorMessage;
                if (!string.IsNullOrEmpty(additionalText))
                {
                    yield return additionalText;
                }
                if (members != null && members.Any())
                {
                    foreach (Expression<Func<TMessage, object>> member in members)
                    {
                        string propertyName = Extensions.Extensions.GetPropertyName(member);
                        // To prevent infinite recursion
                        if (!propertyName.Equals(nameof(this.ErrorMessage)))
                        {
                            yield return $"{propertyName}={member.Compile()(message)}";
                        }
                    }
                }
            }

            try
            {
                return string.Join(";", parts());
            }
            catch (Exception ex)
            {
                try
                {
                    return $"{this.DefaultErrorMessage} : Exception {Extensions.Extensions.AggregateExceptionToString(new Exception($"Exception thrown in UniversalMessage<{typeof(TMessage).Name}>.{nameof(this.FormatErrorMessage)}", ex))}";
                }
                catch
                {
                    return $"{this.DefaultErrorMessage}";
                }
            }
        }
    }
}
﻿using System;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Security.Cryptography;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Web;

namespace Ivh.Common.Api.Core.Client
{
    public class ApiHandler : DelegatingHandler
    {
        private readonly string _appId = null;
        private readonly string _appKey = null;
        private readonly string _requestingApplicationName = null;

        public ApiHandler(string appId, string appKey, string requestingApplicationName)
        {
            this._appId = appId;
            this._appKey = appKey;
            this._requestingApplicationName = requestingApplicationName;
            this.InnerHandler = new HttpClientHandler();
        }

        protected override async Task<HttpResponseMessage> SendAsync(HttpRequestMessage request, CancellationToken cancellationToken)
        {
            HttpResponseMessage response = null;
            string requestContentBase64String = string.Empty;

            string requestUri = HttpUtility.UrlEncode(request.RequestUri.AbsoluteUri.ToLower());

            string requestHttpMethod = request.Method.Method;

            //Calculate UNIX time
            DateTime epochStart = new DateTime(1970, 01, 01, 0, 0, 0, 0, DateTimeKind.Utc);
            TimeSpan timeSpan = DateTime.UtcNow - epochStart;
            string requestTimeStamp = Convert.ToUInt64(timeSpan.TotalSeconds).ToString();

            //create random nonce for each request
            string nonce = Guid.NewGuid().ToString("N");

            //Checking if the request contains body, usually will be null wiht HTTP GET and DELETE
            if (request.Content != null)
            {
                byte[] content = await request.Content.ReadAsByteArrayAsync();
                SHA256 sha256 = SHA256.Create();
                //Hashing the request body, any change in request body will result in different hash, we'll incure message integrity
                byte[] requestContentHash = sha256.ComputeHash(content);
                requestContentBase64String = Convert.ToBase64String(requestContentHash);
            }

            //Creating the raw signature string
            string signatureRawData = string.Format("{0}{1}{2}{3}{4}{5}", this._appId, requestHttpMethod, requestUri, requestTimeStamp, nonce, requestContentBase64String);

            byte[] secretKeyByteArray = Convert.FromBase64String(this._appKey);

            byte[] signature = Encoding.UTF8.GetBytes(signatureRawData);

            using (HMACSHA256 hmac = new HMACSHA256(secretKeyByteArray))
            {
                byte[] signatureBytes = hmac.ComputeHash(signature);
                string requestSignatureBase64String = Convert.ToBase64String(signatureBytes);
                //Setting the values in the Authorization header using custom scheme (amx)
                request.Headers.Authorization = new AuthenticationHeaderValue(Common.AuthenticationScheme, string.Format("{0}:{1}:{2}:{3}", this._appId, requestSignatureBase64String, nonce, requestTimeStamp));
                request.Headers.Add(Common.RequestHeader.RequestingApplicationName, this._requestingApplicationName);
            }

            response = await base.SendAsync(request, cancellationToken);

            return response;
        }

    }
}

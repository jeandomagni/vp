﻿namespace Ivh.Common.Api.Core
{
    internal static class Common
    {
        internal const string AuthenticationScheme = "amx";

        internal static class RequestHeader
        {
            internal const string RequestingApplicationName = "VP-RequestingApplicationName";
        }

        internal static class Claim
        {
            internal const string ApplicationRegistrationName = "ApplicationRegistrationName";
        }
    }
}

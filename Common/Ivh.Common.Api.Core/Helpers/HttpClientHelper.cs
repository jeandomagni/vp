﻿using System;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using Ivh.Common.Web.Core.LoggingHelper;
using Newtonsoft.Json;

namespace Ivh.Common.Api.Core.Helpers
{
    public class HttpClientHelper
    {
        public static TResponse CallApiWith<TResponse, TSend>(string baseUrl, string url, HttpMethod method, TSend objectToSend, Action<HttpResponseMessage, TResponse> successAction, Action<HttpResponseMessage, string> errorAction, Func<HttpClient> generateHttpClient, bool throwOnError = true)
        {
            using (HttpClient client = generateHttpClient())
            {
                client.BaseAddress = new Uri(baseUrl);//This would need to be refactored for others to use
                HttpResponseMessage response = null;
                string result = string.Empty;
                Task.Run(async () =>
                {
                    if (method == HttpMethod.Get)
                    {
                        response = await client.GetAsync(url).ConfigureAwait(false);
                        result = await response.Content.ReadAsStringAsync().ConfigureAwait(false);
                    }
                    else if (method == HttpMethod.Post)
                    {
                        string sendJson = JsonConvert.SerializeObject(objectToSend);
                        client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                        response = await client.PostAsync(url, new StringContent(sendJson, Encoding.UTF8, "application/json")).ConfigureAwait(false);

                        //response = await client.PostAsync(url, objectToSend).ConfigureAwait(false);
                        result = await response.Content.ReadAsStringAsync().ConfigureAwait(false);
                    }
                    else
                    {
                        throw new NotSupportedException(string.Format("Method {0} is not supported.", method.ToString()));
                    }

                }).Wait();
                if (response.IsSuccessStatusCode)
                {
                    if (!string.IsNullOrEmpty(result))
                    {
                        TResponse infoDto = JsonConvert.DeserializeObject<TResponse>(result);
                        successAction(response, infoDto);
                        return infoDto;
                    }
                    return default(TResponse);
                }

                errorAction(response, result);
                if (throwOnError)
                    throw new Exception(string.Format("Call to api({1}) failed: {0}", AggregateWeb.AggregateWebResponse(System.Reflection.MethodBase.GetCurrentMethod().Name, response), url));
            }
            return default(TResponse);
        }
    }
}

﻿namespace Ivh.Common.Web.Services
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web.Mvc;
    using AutoMapper;
    using Base.Utilities.Helpers;
    using Interfaces;
    using Ivh.Application.Content.Common.Interfaces;
    using Ivh.Application.Core.Common.Dtos;
    using Ivh.Application.Core.Common.Interfaces;
    using Ivh.Application.FinanceManagement.Common.Interfaces;
    using Ivh.Application.Guarantor.Common.Dtos;
    using Ivh.Application.Matching.Common.Interfaces;
    using Models;
    using Newtonsoft.Json;
    using Newtonsoft.Json.Linq;
    using VisitPay.Constants;
    using VisitPay.Enums;

    public class RegistrationService : IRegistrationService
    {
        private readonly Lazy<IClientApplicationService> _clientApplicationService;
        private readonly Lazy<IContentApplicationService> _contentApplicationService;
        private readonly Lazy<IGuarantorApplicationService> _guarantorApplicationService;
        private readonly Lazy<IStatementApplicationService> _statementApplicationService;
        private readonly Lazy<IMatchingApplicationService> _matchingApplicationService;
        private readonly Lazy<IFeatureApplicationService> _featureApplicationService;

        private ClientDto _clientDto;
        private ClientDto ClientDto 
        {
            get => this._clientDto ?? (this.ClientDto = this._clientApplicationService.Value.GetClient());
            set => this._clientDto = value;
        }
        
        public RegistrationService(
            Lazy<IClientApplicationService> clientApplicationService,
            Lazy<IContentApplicationService> contentApplicationService,
            Lazy<IGuarantorApplicationService> guarantorApplicationService,
            Lazy<IStatementApplicationService> statementApplicationService,
            Lazy<IMatchingApplicationService> matchingApplicationService,
            Lazy<IFeatureApplicationService> featureApplicationService
        )
        {
            this._clientApplicationService = clientApplicationService;
            this._contentApplicationService = contentApplicationService;
            this._guarantorApplicationService = guarantorApplicationService;
            this._statementApplicationService = statementApplicationService;
            this._matchingApplicationService = matchingApplicationService;
            this._featureApplicationService = featureApplicationService;
        }
        
        public RegisterPersonalInformationViewModel GetRegisterPersonalInformationViewModel(bool isSso)
        {
            JArray registrationIdConfiguration = (JArray) JsonConvert.DeserializeObject(this.ClientDto.RegistrationIdConfiguration);

            RegisterViewModelElementConfig ssnConfig = this.GetRegisterViewModelElementConfig(RegistrationMatchFieldEnum.Ssn4);
            RegisterViewModelElementConfig zipConfig = this.GetRegisterViewModelElementConfig(RegistrationMatchFieldEnum.PostalCode);
            RegisterViewModelElementConfig patientDobConfig = this.GetRegisterViewModelElementConfig(RegistrationMatchFieldEnum.PatientDateOfBirth, false);

            RegisterPersonalInformationViewModel model = new RegisterPersonalInformationViewModel(zipConfig, ssnConfig, patientDobConfig)
            {
                DateOfBirthMonth = string.Empty,
                IsSso = isSso,
                GuarantorId = new string[registrationIdConfiguration.Count],
                State = string.Empty
            };

            return model;
        }

        public void ValidateRecords(ModelStateDictionary modelState, RegisterPersonalInformationViewModel model)
        {
            // validate dob
            DateTime dateOfBirth = DateTimeHelper.FromValues(model.DateOfBirthMonth, model.DateOfBirthDay, model.DateOfBirthYear);
            if (dateOfBirth == default(DateTime))
            {
                string dobMessage = this._contentApplicationService.Value.GetTextRegion(TextRegionConstants.DobInvalid);
                modelState.AddModelError(nameof(RegisterPersonalInformationViewModel.DateOfBirthMonth), dobMessage);
                modelState.AddModelError(nameof(RegisterPersonalInformationViewModel.DateOfBirthDay), string.Empty);
                modelState.AddModelError(nameof(RegisterPersonalInformationViewModel.DateOfBirthYear), string.Empty);
            }
            else if (dateOfBirth.Date > DateTime.UtcNow.Date.AddYears(-18))
            {
                string dobMessage = this._contentApplicationService.Value.GetTextRegion(TextRegionConstants.DobMinAge);
                modelState.AddModelError(nameof(RegisterPersonalInformationViewModel.DateOfBirthMonth), dobMessage);
                modelState.AddModelError(nameof(RegisterPersonalInformationViewModel.DateOfBirthDay), string.Empty);
                modelState.AddModelError(nameof(RegisterPersonalInformationViewModel.DateOfBirthYear), string.Empty);
            }

            if (!model.IsSso)
            {
                // validate guarantor id - client side catches this, but server side does not
                if (model.GuarantorId == null || model.GuarantorId.Any(string.IsNullOrEmpty))
                {
                    JArray registrationIdConfiguration = (JArray) JsonConvert.DeserializeObject(this.ClientDto.RegistrationIdConfiguration);
                    for (int i = 0; i < registrationIdConfiguration.Count; i++)
                    {
                        modelState.AddModelError($"{nameof(RegisterPersonalInformationViewModel.GuarantorId)}{i}", $"Your {this.ClientDto.RegistrationGuarantorIdentifierTextLong} is invalid.");
                    }
                }
            }
        }

        public DateTime? GetPatientDateOfBirth(RegisterPersonalInformationViewModel model, DateTime dateOfBirth)
        {
            if (model.ShowPatientDateOfBirth)
            {
                return DateTimeHelper.FromValues(model.PatientDateOfBirthMonth, model.PatientDateOfBirthDay, model.PatientDateOfBirthYear);
            }

            DateTime? patientDateOfBirth = null;
            RegisterViewModelElementConfig patientDobConfig = this.GetRegisterViewModelElementConfig(RegistrationMatchFieldEnum.PatientDateOfBirth, false);
            if (patientDobConfig.Visible)
            {
                // VPNG-24457 - CTI can't handle a NULL patient dob.
                // we need to explicity set it here when it's the same as the guarantor dob
                // but only when the UI presents a patient dob option
                patientDateOfBirth = dateOfBirth;
            }

            return patientDateOfBirth;
        }

        private RegisterViewModelElementConfig GetRegisterViewModelElementConfig(RegistrationMatchFieldEnum registrationMatchField, bool showIfNull = true)
        {
            RegistrationMatchConfigurationDto matchConfiguration = this.GetRegistrationMatchConfiguration();

            RegistrationMatchFieldConfigurationDto fieldConfigurationDto = matchConfiguration?.ConfigurationForField(registrationMatchField);
            if (fieldConfigurationDto != null)
            {
                return Mapper.Map<RegisterViewModelElementConfig>(fieldConfigurationDto);
            }

            if (!showIfNull)
            {
                //override defaults which are true
                return new RegisterViewModelElementConfig {Required = false, Visible = false};
            }

            return new RegisterViewModelElementConfig();
        }

        private RegistrationMatchConfigurationDto _registrationMatchConfiguration;
        private RegistrationMatchConfigurationDto GetRegistrationMatchConfiguration()
        {
            if (this._registrationMatchConfiguration != null)
            {
                return this._registrationMatchConfiguration;
            }

            RegistrationMatchConfigurationDto matchConfiguration = null;

            //TODO: VP-3619 - cleanup feature, remove RegistrationMatchFieldEnum and maps
            if (this._featureApplicationService.Value.IsFeatureEnabled(VisitPayFeatureEnum.FeatureMatchingApiIsEnabled))
            {
                IList<string> requiredGuarantorFields = this._matchingApplicationService.Value.GetRequiredMatchFieldsGuarantor(ApplicationEnum.VisitPay, PatternUseEnum.Initial);
                IList<string> requiredPatientFields = this._matchingApplicationService.Value.GetRequiredMatchFieldsPatient(ApplicationEnum.VisitPay, PatternUseEnum.Initial);

                // default to not required
                matchConfiguration = new RegistrationMatchConfigurationDto
                {
                    new RegistrationMatchFieldConfigurationDto(){ Field = RegistrationMatchFieldEnum.PatientDateOfBirth, Required = false, Visible = false },
                    new RegistrationMatchFieldConfigurationDto(){ Field = RegistrationMatchFieldEnum.PostalCode, Required = false, Visible = false },
                    new RegistrationMatchFieldConfigurationDto(){ Field = RegistrationMatchFieldEnum.Ssn4, Required = false, Visible = false },
                };

                void MapConfiguration(IList<string> requiredFieldNames, RegistrationMatchConfigurationDto configuration, Func<string, RegistrationMatchFieldEnum?> nameToEnumMap)
                {
                    foreach (string requiredFieldName in requiredFieldNames)
                    {
                        RegistrationMatchFieldEnum? matchFieldEnum = nameToEnumMap(requiredFieldName);

                        foreach (RegistrationMatchFieldConfigurationDto registrationMatchFieldConfigurationDto in configuration.Where(x => x.Field == matchFieldEnum))
                        {
                            registrationMatchFieldConfigurationDto.Required = true;
                            registrationMatchFieldConfigurationDto.Visible = true;
                        }
                    }
                }

                MapConfiguration(requiredGuarantorFields, matchConfiguration, x =>
                {
                    switch (x)
                    {
                        case "Ssn4":
                            return RegistrationMatchFieldEnum.Ssn4;
                        case "PostalCode":
                            return RegistrationMatchFieldEnum.PostalCode;
                        default:
                            return null;
                    }
                });

                MapConfiguration(requiredPatientFields, matchConfiguration, x =>
                {
                    switch (x)
                    {
                        case "DOB":
                            return RegistrationMatchFieldEnum.PatientDateOfBirth;
                        default:
                            return null;
                    }
                });
            }
            else
            {
                matchConfiguration = JsonConvert.DeserializeObject<RegistrationMatchConfigurationDto>(this.ClientDto.RegistrationMatchConfiguration);
            }

            return this._registrationMatchConfiguration = matchConfiguration;
        }

        public bool HasStatement(int visitPayUserId)
        {
            bool hasStatement = false;
            int vpGuarantorId = this._guarantorApplicationService.Value.GetGuarantorId(visitPayUserId);
            if (vpGuarantorId != 0)
            {
                hasStatement = this._statementApplicationService.Value.DoesGuarantorHaveStatement(vpGuarantorId);
            }

            return hasStatement;
        }
    }
}
﻿namespace Ivh.Common.Web.Services
{
    using System;
    using Ivh.Application.Content.Common.Interfaces;
    using Ivh.Application.Core.Common.Interfaces;
    using Ivh.Application.Logging.Common.Interfaces;
    using Base.Utilities.Helpers;
    using Domain.Logging.Interfaces;
    using Domain.Settings.Interfaces;
    using Ivh.Application.AppIntelligence.Common.Interfaces;
    using Session;
    using Web.Interfaces;

    public class BaseControllerService : IBaseControllerService
    {
        public BaseControllerService(
            Lazy<ISessionFacade> sessionFacade, 
            Lazy<IClientApplicationService> clientApplicationService, 
            Lazy<IFeatureApplicationService> featureApplicationService, 
            Lazy<IApplicationSettingsService> applicationSettingsService,
            Lazy<IThemeApplicationService> themeApplicationService,
            Lazy<IVisitPayUserJournalEventApplicationService> visitPayUserJournalEventApplicationService,
            Lazy<IGuarantorApplicationService> guarantorApplicationService,
            Lazy<IBruteForceDelayService> bruteForceDelayService,
            Lazy<ILoggingApplicationService> logger, 
            Lazy<TimeZoneHelper> timeZoneHelper,
            Lazy<IRandomizedTestApplicationService> randomizedTestApplicationService, 
            Lazy<IMetricsProvider> metricsProvider)
        {
            this.SessionFacade = sessionFacade;
            this.ClientApplicationService = clientApplicationService;
            this.FeatureApplicationService = featureApplicationService;
            this.ApplicationSettingsService = applicationSettingsService;
            this.ThemeApplicationService = themeApplicationService;
            this.VisitPayUserJournalEventApplicationService = visitPayUserJournalEventApplicationService;
            this.GuarantorApplicationService = guarantorApplicationService;
            this.BruteForceDelayService = bruteForceDelayService;
            this.Logger = logger;
            this.TimeZoneHelper = timeZoneHelper;
            this.RandomizedTestApplicationService = randomizedTestApplicationService;
            this.MetricsProvider = metricsProvider;
        }

        public Lazy<ISessionFacade> SessionFacade { get; }

        public Lazy<IClientApplicationService> ClientApplicationService { get; }

        public Lazy<IFeatureApplicationService> FeatureApplicationService { get; }

        public Lazy<IApplicationSettingsService> ApplicationSettingsService { get; }

        public Lazy<IThemeApplicationService> ThemeApplicationService { get; }

        public Lazy<IVisitPayUserJournalEventApplicationService> VisitPayUserJournalEventApplicationService { get; }

        public Lazy<IGuarantorApplicationService> GuarantorApplicationService { get; }
        public Lazy<IBruteForceDelayService> BruteForceDelayService { get; }
        public Lazy<IMetricsProvider> MetricsProvider { get; }

        public Lazy<ILoggingApplicationService> Logger { get; }

        public Lazy<TimeZoneHelper> TimeZoneHelper { get; }

        public Lazy<IRandomizedTestApplicationService> RandomizedTestApplicationService { get; set; }
    }
}

﻿namespace Ivh.Common.Web.Services
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Security.Claims;
    using System.Web;
    using Interfaces;
    using Ivh.Application.Base.Common.Interfaces;
    using Ivh.Application.Core.Common.Interfaces;
    using VisitPay.Enums;

    public class MaskingService : IMaskingService
    {
        private readonly Lazy<IClientApplicationService> _clientApplicationService;
        private readonly Lazy<IFeatureApplicationService> _featureApplicationService;

        public MaskingService(
            Lazy<IClientApplicationService> clientApplicationService,
            Lazy<IFeatureApplicationService> featureApplicationService)
        {
            this._clientApplicationService = clientApplicationService;
            this._featureApplicationService = featureApplicationService;
        }

        public void Mask(object source, object destination, IList<string> destinationFieldsToMask)
        {
            if (source == null || destination == null)
            {
                return;
            } 
            
            this.MaskConsolidation(source, destination, destinationFieldsToMask);
            this.MaskMatching(source, destination, destinationFieldsToMask);
        }

        private void MaskConsolidation(object source, object destination, IList<string> destinationFieldsToMask)
        {
            if (!(source is IMaskConsolidated obj) || !obj.IsMaskedConsolidated)
            {
                return;
            }

            if (!this._featureApplicationService.Value.IsFeatureEnabled(VisitPayFeatureEnum.ConsolidationMasking))
            {
                return;
            }

            if (!(HttpContext.Current?.User?.Identity is ClaimsIdentity claimsIdentity))
            {
                return;
            }

            Claim claim = claimsIdentity.FindFirst(nameof(ClaimTypeEnum.GuarantorId));
            if (claim == null || !int.TryParse(claim.Value, out int currentVpGuarantorId))
            {
                return;
            }

            if (currentVpGuarantorId == obj.VpGuarantorId)
            {
                return;
            }

            string replacementValue = this._clientApplicationService.Value.GetClient().MaskedReplacementValue;
            this.Mask(destination, destinationFieldsToMask, replacementValue);
        }

        private void MaskMatching(object source, object destination, IList<string> destinationFieldsToMask)
        {
            if (!(source is IMaskMatching obj))
            {
                return;
            }

            if (!obj.MatchOption.HasValue)
            {
                return;
            }

            IList<int> matchOptionMaskingConfiguration = this._clientApplicationService.Value.GetClient().MatchOptionMaskingConfiguration;
            if (!matchOptionMaskingConfiguration.Contains((int) obj.MatchOption.Value))
            {
                return;
            }

            string replacementValue = this._clientApplicationService.Value.GetClient().MaskedReplacementValue;
            this.Mask(destination, destinationFieldsToMask, replacementValue);
        }

        private void Mask(object destination, IList<string> destinationFieldsToMask, string replacementValue)
        {
            PropertyInfo[] destinationProperties = destination.GetType().GetProperties();
            foreach (string destinationFieldToMask in destinationFieldsToMask)
            {
                PropertyInfo destinationProperty = destinationProperties.FirstOrDefault(x => x.Name == destinationFieldToMask);
                destinationProperty?.SetValue(destination, replacementValue);
            }
        }
    }
}

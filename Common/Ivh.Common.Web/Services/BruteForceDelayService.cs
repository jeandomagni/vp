﻿namespace Ivh.Common.Web.Services
{
    using System;
    using System.Diagnostics;
    using System.Threading.Tasks;
    using Cache;
    using Interfaces;

    public class BruteForceDelayService : IBruteForceDelayService
    {
        public const int BruteForceDelayKeyExpiration = 60 * 5; // minutes
        public const int ErrorConditionDefaultDelayMilliseconds = 1000;

        private readonly Lazy<IDistributedCache> _cache;
        private static readonly string BruteForceDelayAttemptCountKeyPrefix = $"{nameof(BruteForceDelayService)}::{nameof(BruteForceDelayAttemptCountKeyPrefix)}::";
        private static readonly string BruteForceDelayLastAttemptDateTimeKeyPrefix = $"{nameof(BruteForceDelayService)}::{nameof(BruteForceDelayLastAttemptDateTimeKeyPrefix)}::";
        private static readonly TimeSpan ErrorConditionDefaultDelay = TimeSpan.FromMilliseconds(ErrorConditionDefaultDelayMilliseconds);

        public BruteForceDelayService(Lazy<IDistributedCache> cache)
        {
            this._cache = cache;
        }

        public async Task<TimeSpan> DelayAsync(string ipAddress, Func<bool> delayFunc, Func<bool> clearFunc = null)
        {
            TimeSpan delay = TimeSpan.Zero;
            try
            {
                if (delayFunc())
                {
                    delay = await this.DelayAsync(ipAddress);
                }
                if (clearFunc != null && clearFunc())
                {
                    delay = await this.ClearAsync(ipAddress);
                }
            }
            catch
            {
                delay = ErrorConditionDefaultDelay;
                await Task.Delay(delay);
            }

            return delay;
        }

        private async Task<TimeSpan> ClearAsync(string ipAddress)
        {
            string bruteForceDelayAttemptCountKey = $"{BruteForceDelayAttemptCountKeyPrefix}{ipAddress}";
            string bruteForceDelayLastAttemptDateTimeKey = $"{BruteForceDelayLastAttemptDateTimeKeyPrefix}{ipAddress}";
            // upon successful log-in, remove cached values
            // if this is not called, the values will expire from the cache after [BruteForceDelayKeyExpiration] seconds
            await this._cache.Value.RemoveAsync(bruteForceDelayAttemptCountKey);
            await this._cache.Value.RemoveAsync(bruteForceDelayLastAttemptDateTimeKey);
            return TimeSpan.Zero;
        }

        private async Task<TimeSpan> DelayAsync(string ipAddress)
        {
            string bruteForceDelayAttemptCountKey = $"{BruteForceDelayAttemptCountKeyPrefix}{ipAddress}";
            string bruteForceDelayLastAttemptDateTimeKey = $"{BruteForceDelayLastAttemptDateTimeKeyPrefix}{ipAddress}";

            // get the total previous attempts count
            int attemptCount = await this._cache.Value.GetObjectAsync<int>(bruteForceDelayAttemptCountKey);
            // get the timestamp of the most recent attempt
            DateTime lastAttempt = await this._cache.Value.GetObjectAsync<DateTime?>(bruteForceDelayLastAttemptDateTimeKey) ?? DateTime.MinValue;

            /*
             we will introduce a delay that grows exponentially with each attempt
             the "base" will default to 2 unless there is less than 2 seconds since the last attempt--suggesting automated attempts--in which case the "base" will be 3
             the "exponent" will be the current attempt count
            */
            int b = (DateTime.UtcNow - lastAttempt).Seconds < 2 ? 3 : 2;
            int e = attemptCount++;

            // delay b to the power of e
            TimeSpan delay = TimeSpan.FromMilliseconds(Math.Pow(b, e));

            Debug.WriteLine($"{nameof(this.DelayAsync)}: {delay.TotalMilliseconds} ({b}^{e}) ms for {ipAddress} after {attemptCount} attempt(s)");
            await Task.Delay(delay);

            // cache the attempt count
            await this._cache.Value.SetObjectAsync(bruteForceDelayAttemptCountKey, attemptCount, BruteForceDelayKeyExpiration);
            // cache the current attempt timestamp
            await this._cache.Value.SetObjectAsync(bruteForceDelayLastAttemptDateTimeKey, DateTime.UtcNow, BruteForceDelayKeyExpiration);

            return delay;
        }
    }
}

﻿namespace Ivh.Common.Web.Extensions
{
    using System.Web.Mvc;
    using Autofac.Integration.Mvc;
    using DependencyInjection;

    public static class IvinciContainerExtensions
    {
        public static IDependencyResolver GetResolver(this IvinciContainer ivinciContainer)
        {
            return new AutofacDependencyResolver(ivinciContainer.Container());
        }
    }
}
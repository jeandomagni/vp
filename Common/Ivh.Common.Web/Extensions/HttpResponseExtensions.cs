﻿namespace Ivh.Common.Web.Extensions
{
    using System;
    using System.Web;
    using System.Web.SessionState;
    using Cookies;

    public static class HttpResponseExtensions
    {
        internal const int DefaultSessionMinutesToLive = 60 * 24;
        internal const int DefaultSessionInvalidationMinutesInPast = 60 * 24 * 365 * -1;
        
        public static void SetSessionCookie(this HttpResponse httpResponse, int minutesToLive = HttpResponseExtensions.DefaultSessionMinutesToLive)
        {
            httpResponse.Cookies.Add(CookieFactory.GetCookie(CookieNames.SessionCookieName, new SessionIDManager().CreateSessionID(HttpContext.Current), TimeSpan.FromMinutes(minutesToLive)));
        }

        public static void InvalidateSessionCookie(this HttpResponse httpResponse, int minutesInPast = HttpResponseExtensions.DefaultSessionInvalidationMinutesInPast)
        {
            HttpCookie sessionCookie = httpResponse.Cookies[CookieNames.SessionCookieName];
            if (sessionCookie != null)
            {
                httpResponse.Cookies.Remove(CookieNames.SessionCookieName);
            }
            httpResponse.SetSessionCookie(minutesInPast);
        }
    }
}

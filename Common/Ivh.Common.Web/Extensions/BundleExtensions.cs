﻿namespace Ivh.Common.Web.Extensions
{
    using System.Web.Optimization;
    using Bundles;

    public static class BundleExtensions
    {
        public static Bundle WithFileHash(this Bundle bundle)
        {
            bundle.Transforms.Add(new FileHashVersionBundleTransform());

            return bundle;
        }
    }
}
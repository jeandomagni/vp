﻿namespace Ivh.Common.Web.Extensions
{
    using System.Collections;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web.Mvc;

    public static class ModelStateExtensions
    {
        public static Hashtable GetErrors(this ModelStateDictionary modelState)
        {
            Hashtable errors = new Hashtable();
            foreach (KeyValuePair<string, ModelState> pair in modelState)
            {
                if (pair.Value.Errors.Count > 0)
                {
                    errors[pair.Key.Replace("model.", "")] = pair.Value.Errors.Select(error => error.ErrorMessage).ToList();
                }
            }
            return errors;
        }
    }
}
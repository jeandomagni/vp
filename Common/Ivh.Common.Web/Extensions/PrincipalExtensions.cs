﻿namespace Ivh.Common.Web.Extensions
{
    using System;
    using System.Linq;
    using System.Security.Claims;
    using System.Security.Principal;
    using System.Web.Mvc.Routing.Constraints;
    using Base.Enums;
    using Microsoft.AspNet.Identity;
    using VisitPay.Enums;

    public static class PrincipalExtensions
    {
        public static int CurrentUserId(this IPrincipal user)
        {
            return user.Identity.CurrentUserId();
        }

        public static string CurrentUserIdString(this IPrincipal user)
        {
            return user.Identity.GetUserId();
        }

        public static string CurrentUserName(this IPrincipal user)
        {
            return user.Identity.GetUserName();
        }

        public static int? CurrentGuarantorId(this IPrincipal user)
        {
            string value = user.GetClaimValue(ClaimTypeEnum.GuarantorId);
            int output;
            if (int.TryParse(value, out output))
            {
                return output;
            }

            return null;
        }

        public static Guid? CurrentUserGuid(this IPrincipal user)
        {
            string value = user.GetClaimValue(ClaimTypeEnum.VisitPayUserGuid);
            Guid output;
            if (Guid.TryParse(value, out output))
            {
                return output;
            }

            return null;
        }

        public static VpGuarantorStatusEnum? CurrentGuarantorStatus(this IPrincipal user)
        {
            string value = user.GetClaimValue(ClaimTypeEnum.GuarantorStatus);
            VpGuarantorStatusEnum output;
            if (Enum.TryParse<VpGuarantorStatusEnum>(value, out output))
            {
                return output;
            }

            return null;
        }

        public static GuarantorTypeEnum? CurrentGuarantorType(this IPrincipal user)
        {
            string value = user.GetClaimValue(ClaimTypeEnum.GuarantorType);
            GuarantorTypeEnum output;
            if (Enum.TryParse<GuarantorTypeEnum>(value, out output))
            {
                return output;
            }

            return null;
        }

        public static string GetClaimValue(this IPrincipal user, ClaimTypeEnum claimTypeEnum)
        {
            if (user.Identity is ClaimsIdentity identity)
            {
                Claim claim = identity.FindFirst(claimTypeEnum.ToString());
                if (claim != null)
                {
                    return claim.Value;
                }
            }
            return null;
        }

        public static bool IsInARole(this ClaimsPrincipal user, params string[] roles)
        {
            return roles.Any(user.IsInRole);
        }

    }
}

﻿namespace Ivh.Common.Web.Extensions
{
    using System.Web;
    using Microsoft.Owin.Security;

    public static class HttpContextExtensions
    {
        public static int CurrentUserId(this HttpContext httpContext)
        {
            return httpContext.User.CurrentUserId();
        }

        public static string CurrentUserName(this HttpContext httpContext)
        {
            return httpContext.User.CurrentUserName();
        }

        public static void SetSessionCookie(this HttpContext httpContext, int minutesToLive = HttpResponseExtensions.DefaultSessionMinutesToLive)
        {
            httpContext.Response.SetSessionCookie(minutesToLive);
        }

        public static void InvalidateSessionCookie(this HttpContext httpContext, int minutesInPast = HttpResponseExtensions.DefaultSessionInvalidationMinutesInPast)
        {
            httpContext.Response.InvalidateSessionCookie(minutesInPast);
        }

        /// <summary>
        /// This will Abandon the Current Session (if any) and Initialize a new one
        /// </summary>
        /// <param name="httpContext"></param>
        /// <param name="abandonCurrentSession"></param>
        /// <param name="minutesToLive"></param>
        public static void InializeSession(this HttpContext httpContext, bool abandonCurrentSession = true, int minutesToLive = HttpResponseExtensions.DefaultSessionMinutesToLive)
        {
            if (abandonCurrentSession)
            {
                httpContext.Session.Abandon();
            }

            httpContext.SetSessionCookie(minutesToLive);
        }

        /// <summary>
        /// This will Abandon the Session and Invalidate the Session Cookie
        /// </summary>
        /// <param name="httpContext"></param>
        /// <param name="minutesInPast"></param>
        public static void AbandonSession(this HttpContext httpContext, int minutesInPast = HttpResponseExtensions.DefaultSessionInvalidationMinutesInPast)
        {
            httpContext.Session.Clear();
            httpContext.Session.Abandon();
            if (httpContext.GetOwinContext().Authentication.User.Identity.IsAuthenticated)
            {
                httpContext.GetOwinContext().Authentication.SignOut(new AuthenticationProperties() { });
            }
            httpContext.InvalidateSessionCookie(minutesInPast);
        }
    }
}

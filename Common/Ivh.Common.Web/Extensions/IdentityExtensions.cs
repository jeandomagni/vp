﻿namespace Ivh.Common.Web.Extensions
{
    using System;
    using System.Linq;
    using System.Security.Claims;
    using System.Security.Principal;
    using Base.Enums;
    using Ivh.Common.Base.Utilities.Extensions;
    using Microsoft.AspNet.Identity;
    using VisitPay.Enums;

    public static class IdentityExtensions
    {
        public static bool TryGetClaim(this IIdentity identity, ClaimTypeEnum claimTypeEnum, out Claim claim)
        {
            claim = default(Claim);
            if (!(identity is ClaimsIdentity))
            {
                return false;
            }
            claim = identity.GetClaim(claimTypeEnum);
            return claim != default(Claim);
        }

        public static bool TryGetClaimValue<TReturn>(this IIdentity identity, ClaimTypeEnum claimTypeEnum, out TReturn value)
        {
            Claim claim = default(Claim);
            value = default(TReturn);
            if (identity.TryGetClaim(claimTypeEnum, out claim))
            {
                value = ObjectExt.ConvertType<string, TReturn>(claim.Value);
            }
            return false;
        }

        public static Claim GetClaim(this IIdentity identity, ClaimTypeEnum claimTypeEnum)
        {
            ClaimsIdentity claimsIdentity = new ClaimsIdentity(identity);
            return claimsIdentity.FindFirst(x => x.Type.Equals(claimTypeEnum.ToString(), StringComparison.OrdinalIgnoreCase));
        }

        public static bool HasClaim(this IIdentity identity, ClaimTypeEnum claimTypeEnum)
        {
            if (!(identity is ClaimsIdentity))
            {
                return false;
            }
            ClaimsIdentity claimsIdentity = new ClaimsIdentity(identity);
            return claimsIdentity.HasClaim(claimTypeEnum);
        }

        public static void AddClaim(this IIdentity identity, ClaimTypeEnum claimTypeEnum, string value)
        {
            ClaimsIdentity claimsIdentity = new ClaimsIdentity(identity);
            claimsIdentity.AddClaim(new Claim(claimTypeEnum.ToString(), value));
        }

        public static int CurrentUserId(this IIdentity identity)
        {
            return identity.GetUserId<int>();
        }

        public static string CurrentUserIdString(this IIdentity identity)
        {
            return identity.GetUserId();
        }

        public static string CurrentUserName(this IIdentity identity)
        {
            return identity.GetUserName();
        }

        #region ClaimsIdentity

        public static bool TryAddClaim(this ClaimsIdentity claimsIdentity, ClaimTypeEnum claimTypeEnum, string value)
        {
            if (!claimsIdentity.HasClaim(claimTypeEnum))
            {
                claimsIdentity.AddClaim(claimTypeEnum, value);
                return true;
            }

            return false;
        }

        public static void AddClaim(this ClaimsIdentity claimsIdentity, ClaimTypeEnum claimTypeEnum, string value)
        {
            claimsIdentity.AddClaim(new Claim(claimTypeEnum.ToString(), value));
        }

        public static bool HasClaim(this ClaimsIdentity claimsIdentity, ClaimTypeEnum claimTypeEnum)
        {
            return claimsIdentity.HasClaim(c => c.Type.Equals(claimTypeEnum.ToString(), StringComparison.OrdinalIgnoreCase));
        }

        #endregion ClaimsIdentity

    }
}

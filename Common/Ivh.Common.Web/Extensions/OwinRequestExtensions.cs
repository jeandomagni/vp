﻿namespace Ivh.Common.Web.Extensions
{
    using System.Web;
    using Microsoft.Owin;

    public static class OwinRequestExtensions
    {
        private const string XRequestedWith = "X-Requested-With";
        private const string XmlHttpRequest = "XMLHttpRequest";

        public static bool IsAjaxRequest(this IOwinRequest request)
        {
            IReadableStringCollection query = request.Query;
            if ((query != null) && (query[OwinRequestExtensions.XRequestedWith] == XmlHttpRequest))
            {
                return true;
            }

            IHeaderDictionary headers = request.Headers;
            return ((headers != null) && (headers[OwinRequestExtensions.XRequestedWith] == XmlHttpRequest));
        }
    }
}
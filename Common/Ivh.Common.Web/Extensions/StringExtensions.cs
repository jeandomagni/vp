﻿namespace Ivh.Common.Web.Extensions
{
    using System.Web.Mvc;
    using Ivh.Application.Content.Common.Interfaces;
    using Base.Utilities.Helpers;
    using System;
    using System.Linq;
    using System.Net;
    using System.Text;
    using System.Text.RegularExpressions;

    public static class StringExtensions
    {
        internal static string ReplaceClientValues(this string input)
        {
            if (string.IsNullOrEmpty(input) || !(input.Contains("[[") && input.Contains("]]")))
            {
                return input;
            }

            IContentApplicationService contentApplicationService = DependencyResolver.Current.GetService<IContentApplicationService>();
            return ReplacementUtility.Replace(input, contentApplicationService.ClientReplacementValues(), null, false);
        }

        public static byte[] HexToByteArray(this string hex)
        {
            int numberChars = hex.Length;
            byte[] bytes = new byte[numberChars / 2];
            for (int i = 0; i < numberChars; i += 2)
                bytes[i / 2] = Convert.ToByte(hex.Substring(i, 2), 16);
            return bytes;
        }

        public static byte[] ToByteArrayUTF8(this string val)
        {
            return Encoding.UTF8.GetBytes(val);
        }

        public static IPAddress ToIPAddress(this string ipString)
        {
            //cleanup the input by removing all non-numeric, non-separator ('.', ':')
            ipString = Regex.Replace(ipString, "[^:.0-9]", string.Empty, RegexOptions.Compiled);
            //we need to check for the cases where it starts with "::1". This is an ipv6 localhost address.
            if (IPAddress.TryParse(ipString, out IPAddress ipAddress))
            {
                return ipAddress;
            }

            return IPAddress.TryParse((ipString ?? string.Empty).Split(':').First(), out ipAddress) ? ipAddress : default(IPAddress);
        }
    }
}


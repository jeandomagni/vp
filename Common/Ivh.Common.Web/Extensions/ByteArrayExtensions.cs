﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ivh.Common.Web.Extensions
{
    public static class ByteArrayExtensions
    {

        public static string ToHex(this byte[] byteArray)
        {
            string hex = BitConverter.ToString(byteArray);
            return hex.Replace("-", "");
        }

        public static string ToStringUTF8(this byte[] byteArray)
        {
            return Encoding.UTF8.GetString(byteArray);
        }

    }
}

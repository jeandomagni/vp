﻿namespace Ivh.Common.Web.Extensions
{
    using System;
    using System.Web.Mvc;

    public static class AuthorizationContextExtensions
    {
        public static bool IsAttributeDefined<T>(this AuthorizationContext context) where T : Attribute
        {
            if (context.ActionDescriptor.IsDefined(typeof(T), true))
            {
                return true;
            }

            if (context.ActionDescriptor.ControllerDescriptor.IsDefined(typeof(T), true))
            {
                return true;
            }

            return false;
        }
    }
}
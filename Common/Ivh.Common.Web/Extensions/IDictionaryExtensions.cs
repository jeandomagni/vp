﻿namespace Ivh.Common.Web.Extensions
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Web.Mvc;

    public static class IDictionaryExtensions
    {
        public static IEnumerable<SelectListItem> ToSelectList<TKey, TValue>(this IDictionary<TKey, TValue> dictionary)
        {
            return dictionary.Select(kvp => new SelectListItem(){ Value = kvp.Key.ToString(), Text =  kvp.Value.ToString()});
        }
    }
}

﻿namespace Ivh.Common.Web.Extensions
{
    using System;
    using System.Collections.Generic;
    using System.Collections.Specialized;
    using System.Linq;
    using System.Net;
    using System.Web;
    using Base.Utilities.Extensions;
    using Constants;

    public static class HttpRequestExtensions
    {
        public static IPAddress RequestUserHostAddress(this HttpRequestBase httpRequest)
        {
            return httpRequest.ForwardedFor().LastOrDefault();
        }

        public static IPAddress RequestUserHostAddress(this HttpRequest httpRequest)
        {
            return httpRequest.ForwardedFor().LastOrDefault();
        }

        public static string RequestUserHostAddressString(this HttpRequestBase httpRequest)
        {
            IPAddress ipAddress = httpRequest.RequestUserHostAddress();

            return ipAddress == default(IPAddress) ? string.Empty : httpRequest.RequestUserHostAddress().ToString();
        }

        public static string RequestUserHostAddressString(this HttpRequest httpRequest)
        {
            return httpRequest.RequestUserHostAddress().ToString();
        }

        public static IEnumerable<IPAddress> ForwardedFor(this HttpRequestBase httpRequest)
        {
            return GetProxiedAddressChain(httpRequest.Headers, httpRequest.UserHostAddress);
        }

        public static IEnumerable<IPAddress> ForwardedFor(this HttpRequest httpRequest)
        {
            return GetProxiedAddressChain(httpRequest.Headers, httpRequest.UserHostAddress);
        }

        private static string GetXForwardedForHeader(NameValueCollection headers)
        {
            return headers.AllKeys.FirstOrDefault(x => x.Equals(HttpHeaders.Request.XForwardedFor, StringComparison.OrdinalIgnoreCase));
        }

        public static IEnumerable<IPAddress> GetProxiedAddressChain(NameValueCollection headers, string userHostAddress)
        {
            yield return userHostAddress.ToIPAddress();
            string key = GetXForwardedForHeader(headers);
            if (key.IsNotNullOrEmpty() && headers[key].IsNotNullOrEmpty())
            {
                foreach (string ipString in headers[key].Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries))
                {
                    IPAddress ipAddress = ipString.ToIPAddress();
                    if (ipAddress != null
                        && !ipAddress.Equals(default(IPAddress))
                        && !ipAddress.IsLoopback())
                    {
                        yield return ipAddress;
                    }
                }
            }
        }

        public static IEnumerable<KeyValuePair<string, string>> Values(this HttpRequest httpRequest)
        {
            if (httpRequest.ContentType != null && httpRequest.Form != null)
            {
                return httpRequest.Form.ToEnumerable().And(httpRequest.QueryString.ToEnumerable(), httpRequest.Headers.ToEnumerable());
            }
            if (httpRequest.QueryString != null)
            {
                return httpRequest.QueryString.ToEnumerable().And(httpRequest.Headers.ToEnumerable());
            }
            if (httpRequest.Headers != null)
            {
                return httpRequest.Headers.ToEnumerable();
            }
            return Enumerable.Empty<KeyValuePair<string, string>>();
        }
    }
}

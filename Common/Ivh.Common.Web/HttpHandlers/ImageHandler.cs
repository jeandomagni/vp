﻿namespace Ivh.Common.Web.HttpHandlers
{
    using System.Web;
    using System.Web.Mvc;
    using Ivh.Application.Content.Common.Dtos;
    using Ivh.Application.Content.Common.Interfaces;

    public class ImageHandler : IHttpHandler
    {
        public bool IsReusable => false;

        public void ProcessRequest(HttpContext context)
        {
            if (!context.User.Identity.IsAuthenticated && context.Request.FilePath.EndsWith(".pdf"))
            {
                context.Response.RedirectToRoute("Default");
            }
            else
            {
                ImageDto image = DependencyResolver.Current.GetService<IImageApplicationService>().GetImageAsync(context.Request.FilePath).Result;
                if (image != null)
                {
                    context.Response.Cache.SetCacheability(HttpCacheability.Public);
                    context.Response.Cache.SetValidUntilExpires(true);
                    context.Response.Cache.SetExpires(image.Expires);
                    context.Response.ContentType = image.ContentType;
                    context.Response.BinaryWrite(image.ImageBytes);
                }
                else
                {
                    context.Response.StatusCode = 404;
                }
            }

            context.Response.End();
        }
    }
}

﻿namespace Ivh.Common.Web.HttpHandlers
{
    using System;
    using System.Web;
    using System.Web.Mvc;
    using Ivh.Application.Content.Common.Interfaces;

    public class CssHandler : IHttpHandler
    {
        public bool IsReusable
        {
            get { return false; }
        }

        public void ProcessRequest(HttpContext context)
        {
            string output = DependencyResolver.Current.GetService<IThemeApplicationService>().GetStylesheet(context.Request.FilePath);
            context.Response.Cache.SetCacheability(HttpCacheability.Public);
            context.Response.Cache.SetValidUntilExpires(true);
            context.Response.Cache.SetExpires(DateTime.UtcNow.AddHours(24));
            context.Response.ContentType = "text/css";
            context.Response.Write(output);
            context.Response.End();
        }
    }
}

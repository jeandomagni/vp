﻿﻿namespace Ivh.Common.Web.HttpHandlers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Net;
    using System.Web;
    using System.Web.Mvc;
    using Base.Utilities.Extensions;
    using Constants;
    using Domain.Settings.Interfaces;
    using Extensions;
    using Ivh.Application.Content.Common.Dtos;
    using Ivh.Application.Content.Common.Interfaces;

    public class PingHandler : IHttpHandler
    {
        public bool IsReusable => false;

        private string _apiKey;
        private const string AuthorizationKey = "AuthorizationKey";
        private Guid? _authorizationKey;

        private static readonly object Lock = new object();

        public void ProcessRequest(HttpContext httpContext)
        {
            if (this._authorizationKey == null || this._apiKey == null)
            {
                lock (Lock)
                {
                    IApplicationSettingsService applicationSettingsService = DependencyResolver.Current.GetService<IApplicationSettingsService>();
                    this._authorizationKey = applicationSettingsService.PingAuthorizationKey.Value;
                    //this is super hacky as it ties this to GuestPay....needs to be abstracted
                    this._apiKey = applicationSettingsService.GuestPayRemotePayApiKey.Value;
                }
            }

            if (httpContext.Request.Path.StartsWith("/Ping", StringComparison.OrdinalIgnoreCase) && this._authorizationKey.HasValue)
            {
                KeyValuePair<string, string> authorizationKeyValuePair = httpContext.Request.Values().FirstOrDefault(x => x.Key.Equals(AuthorizationKey, StringComparison.OrdinalIgnoreCase));
                if (!authorizationKeyValuePair.Equals(default(KeyValuePair<string, string>))
                    && (Guid.TryParse(authorizationKeyValuePair.Value, out Guid authorizationKey)
                    && !this._authorizationKey.Equals(default(Guid))
                    && authorizationKey.Equals(this._authorizationKey.Value)))
                {
                    httpContext.Response.StatusCode = (int)HttpStatusCode.OK;
                }
                else if (this.IsAuthorizedByApiKey(httpContext))
                {
                    httpContext.Response.StatusCode = (int)HttpStatusCode.OK;
                }
                else
                {
                    httpContext.Response.StatusCode = (int)HttpStatusCode.NotFound;
                }
                httpContext.Response.End();
            }
        }

        private bool IsAuthorizedByApiKey(HttpContext httpContext)
        {
            return this._apiKey.IsNotNullOrEmpty()
                   && httpContext.Request.Headers.HasKeys()
                   && httpContext.Request.Headers.AllKeys.Contains(HttpHeaders.Request.Authorization, StringComparer.OrdinalIgnoreCase)
                   && httpContext.Request.Headers[HttpHeaders.Request.Authorization].Contains(this._apiKey);
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ivh.Common.Web.Models.Payment
{
    public class PaymentResultsModel: ResultMessage
    {
        public PaymentResultsModel(bool result, string message) : base(result, message)
        {
        }
        public int? PaymentId { get; set; }
    }
}

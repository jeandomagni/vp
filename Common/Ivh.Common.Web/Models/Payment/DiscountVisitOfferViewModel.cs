﻿namespace Ivh.Common.Web.Models.Payment
{
    public class DiscountVisitOfferViewModel
    {
        public string BillingApplicationText { get; set; }

        public decimal DiscountAmount { get; set; }

        public decimal DiscountPercent { get; set; }

        public string SourceSystemKeyDisplay { get; set; }

        public string TotalBalance { get; set; }
    }
}
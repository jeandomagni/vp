namespace Ivh.Common.Web.Models.Payment
{
    using System.Web.WebPages;

    public class PaymentMethodListItemViewModel : PaymentMethodDisplayViewModel
    {
        public PaymentMethodAccountTypeViewModel PaymentMethodAccountType { get; set; }

        public PaymentMethodProviderTypeViewModel PaymentMethodProviderType { get; set; }

        public string ProviderTypeImage { get; set; }

        public string DisplayAccountTypeText => this.PaymentMethodAccountType?.PaymentMethodAccountTypeSelectedDisplayText;

        public string DisplayProviderTypeText => this.PaymentMethodProviderType?.PaymentMethodProviderTypeDisplayText;

        public string DisplayProviderTypeImage
        {
            get
            {
                if (this.ProviderTypeImage.IsEmpty())
                {
                    return "";
                }

                return $"/Content/Images/{this.ProviderTypeImage}";
            }
        }

        public string ExpDateForDisplay { get; set; }
        
        public bool IsAchType { get; set; }

        public bool IsCardType { get; set; }

        public bool IsActive { get; set; }

        public bool CanSetPrimary { get; set; }

        public bool IsPrimary { get; set; }

        public bool IsRemoveable { get; set; }

        public string LastFour { get; set; }

        public string RemoveableMessage { get; set; }

        public string ExpirationMessage { get; set; }

        public string Amount { get; set; }

        public string AccountNickName { get; set; }

        public string BankName { get; set; }
    }
}
﻿namespace Ivh.Common.Web.Models.Payment
{
    using System.Collections.Generic;
    using System.Linq;

    public class DiscountOfferViewModel
    {
        private IList<DiscountVisitOfferViewModel> _visitOffers;

        public bool IsGuarantorEligible { get; set; }

        public bool GuarantorHasEligibleVisits { get; set; }

        public string FirstNameLastName { get; set; }

        public decimal DiscountTotal { get; set; }

        public IList<DiscountVisitOfferViewModel> VisitOffers
        {
            get { return (this._visitOffers ?? (this.VisitOffers = new List<DiscountVisitOfferViewModel>())).OrderByDescending(x => x.DiscountAmount).ToList(); }
            set => this._visitOffers = value;
        }
    }
}
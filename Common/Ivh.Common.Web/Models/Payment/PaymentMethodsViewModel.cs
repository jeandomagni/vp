namespace Ivh.Common.Web.Models.Payment
{
    using System.Collections.Generic;
    using Shared;

    public class PaymentMethodsViewModel
    {
        public PaymentMethodsViewModel()
        {
            this.AllAccounts = new List<PaymentMethodListItemViewModel>();
            this.AllowSelection = false;
            this.PaymentMethodAccountTypes = new List<PaymentMethodAccountTypeViewModel>();
            this.PaymentMethodProviderTypes = new List<PaymentMethodProviderTypeViewModel>();
        }

        public IList<PaymentMethodListItemViewModel> AllAccounts { get; set; }
        public CmsViewModel HsaInterestDisclaimerCms { get; set; }
        public CmsViewModel AccountRemoveCms { get; set; }
        public CmsViewModel PrimaryChangedCms { get; set; }
        public bool AllowSelection { get; set; }
        public IList<PaymentMethodAccountTypeViewModel> PaymentMethodAccountTypes { get; set; }
        public IList<PaymentMethodProviderTypeViewModel> PaymentMethodProviderTypes { get; set; }
    }
}
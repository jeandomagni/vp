﻿namespace Ivh.Common.Web.Models.Payment
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using Base.Enums;
    using VisitPay.Enums;

    public class ArrangePaymentSubmitViewModel
    {
        [Required]
        public PaymentOptionEnum PaymentOptionId { get; set; }

        [Required]
        public decimal? PaymentAmount { get; set; }

        [Required]
        public DateTime PaymentDate { get; set; }

        [Required]
        public int PaymentMethodId { get; set; }

        public IDictionary<int, decimal> HouseholdBalances { get; set; }

        public IDictionary<int, decimal> FinancePlans { get; set; }

        public IDictionary<int, decimal> Payments { get; set; }

        public IDictionary<int, decimal> Visits { get; set; }

        public bool PayBucketZero { get; set; }
    }
}
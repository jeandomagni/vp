﻿namespace Ivh.Common.Web.Models.Payment
{
    using System.Collections.Generic;

    public class PaymentMethodResponseViewModel<TPaymentMethod>
    {
        public bool Result { get; set; }
        public string Message { get; set; }
        public int PaymentMethodId { get; set; }
        public bool PrimaryChanged { get; set; }

        public TPaymentMethod SingleUsePaymentMethod { get; set; }
        public PaymentMethodsViewModel Model { get; set; }
    }
}
namespace Ivh.Common.Web.Models.Payment
{
    using VisitPay.Enums;

    public class PaymentMethodDisplayViewModel
    {
        public string DisplayName { get; set; }

        public int PaymentMethodId { get; set; }

        public PaymentMethodTypeEnum PaymentMethodType { get; set; }

        public string DisplayImage
        {
            get
            {
                switch (this.PaymentMethodType)
                {
                    case PaymentMethodTypeEnum.Visa:
                        return "/Content/Images/visa.png";
                    case PaymentMethodTypeEnum.Mastercard:
                        return "/Content/Images/mastercard.png";
                    case PaymentMethodTypeEnum.AmericanExpress:
                        return "/Content/Images/amex.png";
                    case PaymentMethodTypeEnum.Discover:
                        return "/Content/Images/discover.png";
                    case PaymentMethodTypeEnum.AchChecking:
                    case PaymentMethodTypeEnum.AchSavings:
                    case PaymentMethodTypeEnum.LockBoxDeposit:
                        return "/Content/Images/bank.png";
                }

                return string.Empty;
            }
        }
    }
}
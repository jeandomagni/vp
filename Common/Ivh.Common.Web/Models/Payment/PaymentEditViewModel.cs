namespace Ivh.Common.Web.Models.Payment
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using Attributes;
    using Interfaces;
    using Ivh.Application.FinanceManagement.Common.Dtos;
    using Shared;
    using VisitPay.Constants;
    using VisitPay.Enums;

    public class PaymentEditViewModel : IPaymentCancelViewModel, IPaymentRescheduleViewModel
    {
        public PaymentEditViewModel()
        {
            this.CmsPaymentEditCancel = new CmsViewModel();
            this.CmsPaymentEditPaymentMethod = new CmsViewModel();
            this.CmsPaymentEditReschedule = new CmsViewModel();
            this.PaymentRescheduleReasons = new List<PaymentActionReasonDto>();
        }
        
        [LocalizedDisplayName(TextRegionConstants.ScheduledDate)]
        public string ScheduledPaymentDate { get; set; }

        [LocalizedDisplayName(TextRegionConstants.RescheduleDate)]
        public string OverrideRescheduledPaymentDate { get; set; }

        [LocalizedDisplayName(TextRegionConstants.RescheduleDate)]
        [Required]
        public DateTime? RescheduledPaymentDate { get; set; }

        [LocalizedDisplayName(TextRegionConstants.RescheduleReason)]
        [Required]
        public int? PaymentRescheduleReasonId { get; set; }

        [Required]
        public string PaymentRescheduleDescription { get; set; }
        
        public int? PaymentMethodId { get; set; }
        
        public bool IsRecurringPayment => this.PaymentType == PaymentTypeEnum.RecurringPaymentFinancePlan;
        
        public PaymentTypeEnum PaymentType { get; set; }
        
        public bool AllowRescheduleDateChange { get; set; }

        public bool ShowPastDueFinancePlanMessage { get; set; }
        
        public string MaxPaymentDate { get; set; }
        
        public CmsViewModel CmsPaymentEditCancel { get; set; }

        public CmsViewModel CmsPaymentEditPaymentMethod { get; set; }

        public CmsViewModel CmsPaymentEditReschedule { get; set; }
        
        public IList<PaymentActionReasonDto> PaymentRescheduleReasons { get; set; }
        
        // cancel permissions
        public bool IsCancelEnabled { get; set; }

        public bool IsEligibleToCancel { get; set; }

        public bool IsUserPermittedCancel { get; set; }

        // reschedule permissions
        public bool IsRescheduleEnabled { get; set; }
        
        public bool IsEligibleToReschedule { get; set; }

        public bool IsUserPermittedReschedule { get; set; }

        // payment method permissions
        public bool IsPaymentMethodsEnabled { get; set; }

        // any
        public bool IsAnyOptionEnabled =>
            this.IsPaymentMethodsEnabled ||
            this.IsRescheduleEnabled ||
            this.IsCancelEnabled;
    }
}
namespace Ivh.Common.Web.Models.Payment
{
    using System;

    [Serializable]
    public class PaymentSummaryViewModel
    {
        public string ActualPaymentAmount { get; set; }
        public string ScheduledPaymentAmount { get; set; }
        public string TransactionId { get; set; }
        public bool IsScheduledPayment { get; set; }
        public bool IsFinancePlan { get; set; }
        public bool IsAchType { get; set; }
    }
}
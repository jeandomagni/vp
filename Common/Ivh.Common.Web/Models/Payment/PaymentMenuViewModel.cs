﻿namespace Ivh.Common.Web.Models.Payment
{
    using System.Collections.Generic;

    public class PaymentMenuViewModel
    {
        public int? SelectedPaymentMenuItemId { get; set; }
        public IList<PaymentMenuItemViewModel> PaymentMenuItems { get; set; }
    }
}
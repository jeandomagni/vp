namespace Ivh.Common.Web.Models.Payment
{
    using System.Collections.Generic;
    using Base.Constants;
    using Base.Enums;
    using Utilities;
    using VisitPay.Constants;
    using VisitPay.Enums;

    public class PaymentMenuItemViewModel
    {
        private string _iconCssClass;

        public int PaymentMenuItemId { get; set; }
        public int? ParentPaymentMenuItemId { get; set; }
        public PaymentOptionEnum? PaymentOptionId { get; set; }
        public bool IsEnabled { get; set; }
        public string Description { get; set; }
        public string Title { get; set; }
        public decimal? UserApplicableBalance { get; set; }
        public string IconCssClass
        {
            get
            {
                if (string.IsNullOrEmpty(this._iconCssClass))
                {
                    return null;
                }

                if (this._iconCssClass.Contains("glyphicon-"))
                {
                    return string.Concat("glyphicon ", this._iconCssClass);
                }

                return this._iconCssClass;
            }
            set { this._iconCssClass = value; }
        }

        public string TextNotAvailable
        {
            get
            {
                if (this.IsEnabled)
                {
                    return null;
                }

                return LocalizationHelper.GetLocalizedString(TextRegionConstants.NotAvailable);
            }
        }

        public bool ShowTextNotAvailable
        {
            get
            {
                if (this.IsEnabled)
                {
                    return false;
                }

                return !string.IsNullOrWhiteSpace(this.TextNotAvailable);
            }
        }
        
        public IList<PaymentMenuItemViewModel> PaymentMenuItems { get; set; }
    }
}
namespace Ivh.Common.Web.Models.Payment
{
    public class PaymentMethodResultViewModel
    {
        public PaymentMethodResultViewModel(bool isSuccess, string errorMessage = null, bool primaryChanged = false, PaymentMethodListItemViewModel paymentMethod = null)
        {
            this.IsError = !isSuccess;
            this.IsSuccess = isSuccess;
            this.ErrorMessage = errorMessage;
            this.PrimaryChanged = primaryChanged;
            this.PaymentMethod = paymentMethod;
        }

        public bool IsError { get; private set; }
        public bool IsSuccess { get; private set; }
        public string ErrorMessage { get; private set; }
        public bool PrimaryChanged { get; private set; }
        public PaymentMethodListItemViewModel PaymentMethod { get; private set; }
    }
}
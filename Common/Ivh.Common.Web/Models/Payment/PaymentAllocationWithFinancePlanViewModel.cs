namespace Ivh.Common.Web.Models.Payment
{
    public class PaymentAllocationWithFinancePlanViewModel : PaymentAllocationViewModel
    {
        public int? FinancePlanId { get; set; }
    }
}
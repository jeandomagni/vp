namespace Ivh.Common.Web.Models.Payment
{
    using System.Collections.Generic;
    using System.Linq;
    using Account;
    using FinancePlan;
    using VisitPay.Enums;

    public class PaymentOptionViewModel 
    {
        public PaymentOptionViewModel()
        {
            this.FinancePlans = new List<FinancePlansSearchResultViewModel>();
            this.Payments = new List<ResubmitPaymentViewModel>();
            this.Visits = new List<VisitsSearchResultViewModel>();
        }

        //
        public PaymentOptionEnum PaymentOptionId { get; set; }
        public decimal UserApplicableBalance { get; set; }
        public string UserApplicableBalanceText { get; set; }
        public string DateBoundary { get; set; }
        public string Description { get; set; }
        public bool IsAmountReadonly { get; set; }
        public bool IsDateReadonly { get; set; }

        //
        public decimal? PaymentAmount { get; set; }
        public decimal? MaximumPaymentAmount { get; set; }
        public string PaymentDate { get; set; }

        // discount
        public bool IsDiscountEligible { get; set; }
        public bool IsDiscountPromptPayOnly { get; set; }
        public decimal? DiscountAmount { get; set; }
        public DiscountOfferViewModel DiscountGuarantorOffer { get; set; }
        public IList<DiscountOfferViewModel> DiscountManagedOffers { get; set; }

        // specific
        public IList<FinancePlansSearchResultViewModel> FinancePlans { get; set; }
        public IList<HouseholdBalanceViewModel> HouseholdBalances { get; set; }
        public IList<ResubmitPaymentViewModel> Payments { get; set; }
        public IList<VisitsSearchResultViewModel> Visits { get; set; }

        // finance plan
        public FinancePlanConfigurationViewModel FinancePlanDefaultConfiguration { get; set; }
        public FinancePlanConfigurationViewModel FinancePlanCombinedConfiguration { get; set; }
        public decimal FinancePlanTotalMonthlyPaymentAmount { get; set; }
        public bool CanSetPaymentDueDay { get; set; }
        public int SuggestedPaymentDueDay { get; set; }
        public bool HasPendingResubmitPayments { get; set; }
        public bool HasBucketZero => this.FinancePlans.Any(y => y.HasAmountDue);
        public IList<VisitsSearchResultViewModel> FinancePlanVisits { get; set; }
        public IList<VisitsSearchResultViewModel> SelectedVisits { get; set; }
        public decimal? AmountToFinance { get; set; }
        public string FinancePlanOptionStateName { get; set; }
        public string FinancePlanOptionStateCode { get;set; }
        public bool OtherFinancePlanStatesAreAvailable { get; set; }
    }
}
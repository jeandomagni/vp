namespace Ivh.Common.Web.Models.Payment
{
    using System.Collections.Generic;
    using Base.Enums;
    using VisitPay.Enums;

    public class PaymentHistorySearchResultViewModel
    {
        public string IdDisplay { get; set; }
        public string InsertDate { get; set; }
        public string NetPaymentAmount { get; set; }
        public List<int> PaymentIds { get; set; }
        public string PaymentProcessorResponseId { get; set; }
        public string PaymentProcessorSourceKey { get; set; }
        public string PaymentStatusDisplayName { get; set; }
        public int PaymentStatusId { get; set; }
        public string PaymentType { get; set; }
        public int PaymentTypeId { get; set; }
        public string SnapshotTotalPaymentAmount { get; set; }
        public int PaymentReversalStatus { get; set; }
        public bool CanTakeAction { get; set; }
        public bool CanRefund { get; set; }
        public int PaymentRefundableType { get; set; }
        public bool CanVoid { get; set; }
        public bool CanRefundReturnedCheck { get; set; }
        public string ActionTakenBy { get; set; }
        public int VisitPayUserId { get; set; }
        public string GuarantorName { get; set; }
        public string MadeByGuarantorName { get; set; }
        public int MadeByVisitPayUserId { get; set; }
        public bool PreviousConsolidation { get; set; }
        public bool IsCancelled { get; set; }
        public string PaymentStatusMessage { get; set; }
        public PaymentMethodDisplayViewModel PaymentMethod { get; set; }
        public bool IsFailedOrCancelled => this.PaymentStatusId == (int)PaymentStatusEnum.ClosedFailed ||
                                           this.PaymentStatusId == (int)PaymentStatusEnum.ClosedCancelled;
    }
}
﻿namespace Ivh.Common.Web.Models.Payment
{
    using System.Collections.Generic;

    public class PaymentHistorySearchResultsViewModel : SearchResult
    {
        public IList<PaymentHistorySearchResultViewModel> Payments { get; set; }
    }
}
namespace Ivh.Common.Web.Models.Payment
{
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using Attributes;
    using Interfaces;
    using Ivh.Application.FinanceManagement.Common.Dtos;
    using Shared;
    using VisitPay.Constants;
    using VisitPay.Enums;

    public class PaymentCancelViewModel : IPaymentCancelViewModel
    {
        public PaymentCancelViewModel()
        {
            this.CmsCancel = new CmsViewModel();
            this.PaymentCancelReasons = new List<PaymentActionReasonDto>();
        }
        
        [LocalizedDisplayName(TextRegionConstants.CancelReason)]
        [Required]
        public int? PaymentCancelReasonId { get; set; }

        [Required]
        public string PaymentCancelDescription { get; set; }

        public CmsViewModel CmsCancel { get; set; }
        
        public IList<PaymentActionReasonDto> PaymentCancelReasons { get; set; }

        public bool IsRecurringPayment { get; set; }

        public PaymentTypeEnum PaymentType { get; set; }

        public string PaymentDueDate { get; set; }

        public string AmountDuePrior { get; set; }

        public string AmountDueAfter { get; set; }

        public bool IsCancelEnabled { get; set; }

        public bool IsEligibleToCancel { get; set; }

        public bool IsUserPermittedCancel { get; set; }
    }
}
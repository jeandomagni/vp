namespace Ivh.Common.Web.Models.Payment
{
    public class ResubmitPaymentViewModel
    {
        //Theory: dont need this, but might for different infos?  Seems like the amount and the type is enough?  maybe not for scheduled visit type, but we dont support that right now
        public int PaymentProcessorResponseId { get; set; }
        public int FinancePlanId { get; set; }
        public string ScheduledPaymentAmount { get; set; }
        public decimal ScheduledPaymentAmountDecimal { get; set; }
        public string LastPaymentFailureDate { get; set; }
        public string PaymentType { get; set; }
    }
}
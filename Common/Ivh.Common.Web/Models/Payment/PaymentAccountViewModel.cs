namespace Ivh.Common.Web.Models.Payment
{
    using System.ComponentModel.DataAnnotations;
    using Attributes;
    using Base.Constants;
    using VisitPay.Constants;


    public class PaymentAccountViewModel
    {
        public PaymentAccountViewModel()
        {
            this.IsActive = true;
        }

        public int PaymentMethodId { get; set; }
        
        [LocalizedDisplayName(TextRegionConstants.AccountNickname)]
        [LocalizedPrompt(TextRegionConstants.AccountNickname)]
        [StringLength(70, ErrorMessage = "{0} is limited to {1} characters")]
        public string AccountNickName { get; set; }
        
        public virtual string BillingId { get; set; }

        public string GatewayToken { get; set; }
        
        [LocalizedDisplayName(TextRegionConstants.SavePaymentMethod)]
        public bool IsActive { get; set; }
        
        [LocalizedDisplayName(TextRegionConstants.SetAsPrimary)]
        public bool IsPrimary { get; set; }

        public string LastFour { get; set; }
        
        [LocalizedDisplayName(TextRegionConstants.AccountType)]
        [Required(ErrorMessage = "Account Type is required.")]
        public int PaymentMethodTypeId { get; set; }

        public string ExpirationMessage { get; set; }

        public bool IsRemoveable { get; set; }

        public bool HasExistingPrimary { get; set; }
    }
}
namespace Ivh.Common.Web.Models.Payment
{
    public class OfflineFinancePlanConfigurationViewModel : FinancePlanConfigurationViewModel
    {
        public bool IsCombined { get; set; }
        public int EsignCmsVersionId { get; set; }
        public bool GuarantorUseAutoPay { get; set;}
    }
}
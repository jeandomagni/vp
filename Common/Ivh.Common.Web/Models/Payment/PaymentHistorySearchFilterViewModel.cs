namespace Ivh.Common.Web.Models.Payment
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.Web.Mvc;
    using Attributes;
    using VisitPay.Constants;
    using VisitPay.Enums;

    public class PaymentHistorySearchFilterViewModel
    {
        public PaymentHistorySearchFilterViewModel()
        {
            this.PaymentMethods = new List<SelectListItem>();
            this.PaymentStatuses = new List<SelectListItem>();

            this.PaymentTypes = new List<SelectListItem>
            {
                new SelectListItem {Text = "Recurring Finance Plan", Value = ((int)PaymentTypeEnum.RecurringPaymentFinancePlan).ToString()},
                new SelectListItem {Text = "Manual", Value = "1"},
                new SelectListItem {Text = "Void", Value = ((int)PaymentTypeEnum.Void).ToString()},
                new SelectListItem {Text = "Refund", Value = ((int)PaymentTypeEnum.Refund).ToString()},
                new SelectListItem {Text = "Partial Refund", Value = ((int)PaymentTypeEnum.PartialRefund).ToString()}
            };

            this.TransactionRanges = new List<SelectListItem>
            {
                new SelectListItem {Text = "30 Days", Value = "30"},
                new SelectListItem {Text = "60 Days", Value = "60"},
                new SelectListItem {Text = "90 Days", Value = "90"},
                new SelectListItem {Text = "120 Days", Value = "120"},
                new SelectListItem {Text = "All", Value = "", Selected = true}
            };
        }
        
        [LocalizedDisplayName(TextRegionConstants.ConfirmationNumber)]
        [LocalizedPrompt(TextRegionConstants.ConfirmationNumber)]
        public string TransactionId { get; set; }

        [LocalizedPrompt(TextRegionConstants.StartDate)]
        [DateRangeCompare(false, "*.PaymentDateRangeFrom", "*.PaymentDateRangeTo", DateRangeCompareType.LessThanOrEqual, ErrorMessage = "Invalid Date Range")]
        [DataType(DataType.Date)]
        public DateTime? PaymentDateRangeFrom { get; set; }

        [LocalizedPrompt(TextRegionConstants.EndDate)]
        [DateRangeCompare(false, "*.PaymentDateRangeTo", "*.PaymentDateRangeFrom", DateRangeCompareType.GreaterThanOrEqual, ErrorMessage = "Invalid Date Range")]
        [DataType(DataType.Date)]
        public DateTime? PaymentDateRangeTo { get; set; }
        
        [LocalizedDisplayName(TextRegionConstants.PaymentType)]
        public int? PaymentTypeId { get; set; }

        [LocalizedDisplayName(TextRegionConstants.PaymentMethod)]
        public int? PaymentMethodId { get; set; }

        [LocalizedDisplayName(TextRegionConstants.Status)]
        public int? PaymentStatusId { get; set; }

        [LocalizedDisplayName(TextRegionConstants.TransactionsInTheLast)]
        public int? TransactionRange { get; set; }

        [LocalizedDisplayName(TextRegionConstants.IdNumber)]
        [LocalizedPrompt(TextRegionConstants.IdNumber)]
        public string PaymentId { get; set; }
        
        public IList<SelectListItem> PaymentStatuses { get; set; }
        public IList<SelectListItem> PaymentTypes { get; set; }
        public IList<SelectListItem> PaymentMethods { get; set; }
        public IList<SelectListItem> TransactionRanges { get; set; }
    }
}
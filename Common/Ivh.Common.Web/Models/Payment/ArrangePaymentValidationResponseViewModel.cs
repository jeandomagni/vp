﻿
using Ivh.Common.Web.Models.Shared;

namespace Ivh.Common.Web.Models.Payment
{
    public class ArrangePaymentValidationResponseViewModel
    {
        public bool PaymentIsValid { get; set; }

        public bool Prompt { get; set; }

        public string Message { get; set; }

        public bool FinancePlanIsAvailable { get; set; }

        public decimal MonthlyPaymentAmountTotal { get; set; }

        public int NumberOfMonthlyPayments { get; set; }

        public CmsViewModel FinancePlanOfferMessage { get; set; } = new CmsViewModel();
    }
}

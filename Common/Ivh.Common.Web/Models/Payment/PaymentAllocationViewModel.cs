namespace Ivh.Common.Web.Models.Payment
{
    public class PaymentAllocationViewModel
    {
        public string AllocationAmount { get; set; }
        public string DisplayStatus { get; set; }
        public string SourceSystemKey { get; set; }
        public string SourceSystemKeyDisplay { get; set; }
        public string MatchedSourceSystemKey { get; set; }
        public int VisitId { get; set; }
        public int VisitPayUserId { get; set; }
        public bool IsVisitRemoved { get; set; }
        public bool ShowVisitLink { get; set; }
    }
}
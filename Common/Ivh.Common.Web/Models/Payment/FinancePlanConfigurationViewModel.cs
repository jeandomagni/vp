namespace Ivh.Common.Web.Models.Payment
{
    using System.Collections.Generic;
    using System.Web.Mvc;
    using FinancePlan;
    using VisitPay.Enums;

    public class FinancePlanConfigurationViewModel
    {
        public FinancePlanConfigurationViewModel()
        {
            this.FinancePlanOfferSetTypes = new List<SelectListItem>();
            this.FinancePlanTypes = new List<FinancePlanTypeViewModel>();
            this.IsEditable = true;
        }
        
        public int? FinancePlanId { get; set; }

        public int StatementId { get; set; }

        public int? EsignCmsVersionId  { get; set; }

        public int TermsCmsVersionId { get; set; }

        public int MaximumMonthlyPayments { get; set; }

        public decimal MinimumPaymentAmount { get; set; }

        public decimal AmountToFinance { get; set; }

        public OfferCalculationStrategyEnum OfferCalculationStrategy { get; set; }

        public string TextClientOfferSet { get; set; }

        public FinancePlanTermsViewModel Terms { get; set; }

        public IList<InterestRateViewModel> InterestRates { get; set; }
        
        public bool IsEditable { get; set; }

        public int FinancePlanOfferSetTypeId { get; set; }

        public IList<SelectListItem> FinancePlanOfferSetTypes { get; set; }

        public int DisplayOrder { get; set; }

        public int ActiveFinancePlanCount { get; set; }

        public decimal ActiveFinancePlanBalance { get; set; }

        public string OptionText { get; set; }

        public string OptionTooltip { get; set; }

        public decimal? UserMonthlyPaymentAmount => this.Terms?.MonthlyPaymentAmount;

        public int? UserNumberMonthlyPayments => this.Terms?.NumberMonthlyPayments;

        public decimal? SuggestedMonthlyPaymentAmount { get; set; }

        public decimal? SuggestedNumberMonthlyPayments { get; set; }
        
        public IList<FinancePlanTypeViewModel> FinancePlanTypes { get; set; }
    }
    
    public class FinancePlanTypeViewModel
    {
        public int FinancePlanTypeId { get; set; }

        public string Name { get; set; }

        public bool RequireCreditAgreement { get; set; }

        public bool RequireEsign { get; set; }

        public bool RequirePaymentMethod { get; set; }

        public bool RequireEmailCommunication { get; set; }

        public bool RequirePaperCommunication { get; set; }

        public bool IsAutoPay { get; set; }

        public bool IsPaymentMethodEditable { get; set; }
    }
}
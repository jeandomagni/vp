namespace Ivh.Common.Web.Models.Payment
{
    public class MyPaymentsViewModel
    {
        public MyPaymentsViewModel()
        {
            this.PaymentHistorySearchFilter = new PaymentHistorySearchFilterViewModel();
            this.PaymentPending = new PaymentPendingViewModel();
        }

        public PaymentHistorySearchFilterViewModel PaymentHistorySearchFilter { get; set; }
        public PaymentPendingViewModel PaymentPending { get; set; }
    }
}
namespace Ivh.Common.Web.Models.Payment
{
    public class PaymentPendingSearchResultViewModel
    {
        public int? PaymentId { get; set; }
        public string PaymentType { get; set; }
        public string ScheduledPaymentAmount { get; set; }
        public string ScheduledPaymentDate { get; set; }
        
        public PaymentMethodDisplayViewModel PaymentMethod { get; set; }
        
        // made for (the guarantor on the payment)
        public int VisitPayUserId { get; set; }
        public string GuarantorName { get; set; }
        public string GuarantorFirstName { get; set; }
        public string GuarantorLastName { get; set; }

        // made by (the payment method used)
        public int MadeByVisitPayUserId { get; set; }
        public string MadeByGuarantorName { get; set; }
    }
}
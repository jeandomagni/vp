﻿namespace Ivh.Common.Web.Models.Payment
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    [Serializable]
    public class PaymentSummaryListViewModel
    {
        public PaymentSummaryListViewModel()
        {
            this.PaymentSummaries = new List<PaymentSummaryViewModel>();
            this.ErrorMessages = new List<string>();
            this.WarnPaymentMethods = new List<string>();
        }

        public IList<PaymentSummaryViewModel> PaymentSummaries { get; set; }
        public IList<string> ErrorMessages { get; set; }
        public IList<string> WarnPaymentMethods { get; set; }

        public string Email { get; set; }
        public string RedirectUrl { get; set; }
        public bool IsScheduledPayments { get { return this.PaymentSummaries.Any() && this.PaymentSummaries.All(x => x.IsScheduledPayment); } }
        public bool IsFinancePlanWithoutPayment { get; set; }
        public DateTime FinancePlanFirstPaymentDueDate { get; set; }
    }
}
namespace Ivh.Common.Web.Models.Payment
{
    using System.Collections.Generic;

    public class PaymentDetailViewModel : PaymentHistorySearchResultViewModel
    {
        public PaymentDetailViewModel()
        {
            this.PaymentAllocations = new List<PaymentAllocationViewModel>();
        }

        public IList<PaymentAllocationViewModel> PaymentAllocations { get; set; }
    }
}
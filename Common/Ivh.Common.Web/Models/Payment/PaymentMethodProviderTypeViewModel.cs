﻿
namespace Ivh.Common.Web.Models.Payment
{
    using System.Web.WebPages;

    public class PaymentMethodProviderTypeViewModel
    {
        public int PaymentMethodProviderTypeId { get; set; }
        public string PaymentMethodProviderTypeText { get; set; }
        public string PaymentMethodProviderTypeDisplayText { get; set; }
        public string ImageName { get; set; }
        public int DisplayOrder { get; set; }

        public string DisplayImage
        {
            get
            {
                if (this.ImageName.IsEmpty())
                    return "";

                return $"/Content/Images/{this.ImageName}";
            }
        }

    }
}

namespace Ivh.Common.Web.Models.Payment
{
    public class HouseholdBalanceViewModel
    {
        public int VpStatementId { get; set; }
        public string VisitPayUserFullName { get; set; }
        public string TotalBalance { get; set; }
        public decimal TotalBalanceDecimal { get; set; }
        public decimal DiscountAmount { get; set; }
        public bool IsManaged { get; set; }
        public bool IsDiscountEligible { get; set; }
        public DiscountOfferViewModel DiscountOffer { get; set; }
    }
}
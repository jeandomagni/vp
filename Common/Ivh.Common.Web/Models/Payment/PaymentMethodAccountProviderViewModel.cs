﻿
namespace Ivh.Common.Web.Models.Payment
{
    using System.Collections.Generic;
    using System.Web.Mvc;
    using System.Linq;

    public class PaymentMethodAccountProviderViewModel
    {
        public IList<PaymentMethodAccountTypeViewModel> PaymentMethodAccountTypes { get; set; }
        public IList<PaymentMethodProviderTypeViewModel> PaymentMethodProviderTypes { get; set; }

        public IList<SelectListItem> AccountTypes
        {
            get
            {
                return this.PaymentMethodAccountTypes.Select(x => new SelectListItem() { Text = x.PaymentMethodAccountTypeListDisplayText, Value = x.PaymentMethodAccountTypeId.ToString() }).ToList();
            }
        }

        public IList<SelectListItem> ProviderTypes
        {
            get
            {
                return this.PaymentMethodProviderTypes.Select(x => new SelectListItem { Text = x.PaymentMethodProviderTypeDisplayText, Value = x.PaymentMethodProviderTypeId.ToString() }).ToList();
            }
        }

        public int SelectedAccountTypeId { get; set; }
        public int SelectedProviderTypeId { get; set; }

    }
}

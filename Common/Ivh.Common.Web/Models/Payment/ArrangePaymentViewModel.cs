﻿namespace Ivh.Common.Web.Models.Payment
{
    using System.Collections.Generic;
    using System.Linq;

    public class ArrangePaymentViewModel
    {
        public ArrangePaymentViewModel()
        {
            this.PaymentOptions = new List<PaymentOptionViewModel>();
        }

        public PaymentMenuViewModel PaymentMenu { get; set; }
        public IList<PaymentOptionViewModel> PaymentOptions { get; set; }
        public string DataUrl { get; set; }
        public string OptionUrl { get; set; }
        public string HomeUrl { get; set; }
        public string HomeButtonText { get; set; }
        public string FinancePlanSubmitUrl { get; set; }
        public string FinancePlanValidateUrl { get; set; }
        public string PaymentSubmitUrl { get; set; }
        public string PaymentValidateUrl { get; set; }
        public int? VisitPayUserId { get; set; }
        public bool IsOfflineGuarantor { get; set; }
        public bool HasCombinedOption => this.PaymentOptions.Any(x => x.FinancePlanCombinedConfiguration != null);
        public bool HasDefaultOption => this.PaymentOptions.Any(x => x.FinancePlanDefaultConfiguration != null);
    }
}
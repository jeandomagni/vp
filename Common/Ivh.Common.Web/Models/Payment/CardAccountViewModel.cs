namespace Ivh.Common.Web.Models.Payment
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.Globalization;
    using System.Linq;
    using System.Web.Mvc;
    using Attributes;
    using Utilities;
    using VisitPay.Constants;

    public class CardAccountViewModel : PaymentAccountViewModel
    {
        public CardAccountViewModel()
        {
            this.PaymentMethodAccountTypes = new List<PaymentMethodAccountTypeViewModel>();
            this.PaymentMethodProviderTypes = new List<PaymentMethodProviderTypeViewModel>();
        }

        [LocalizedDisplayName(TextRegionConstants.BillingAddress)]
        [LocalizedPrompt(TextRegionConstants.AddressLine1)]
        [Required(ErrorMessage = "Billing Address Line 1 is required.")]
        public string AddressLine1 { get; set; }

        [LocalizedPrompt(TextRegionConstants.AddressLine2)]
        public string AddressLine2 { get; set; }

        /// <summary>
        /// not required when adding a device payment method, from the client portal
        /// </summary>
        public bool CardCodeRequired { get; set; } = true;

        [Obsolete]
        [LocalizedDisplayName(TextRegionConstants.SecurityCodeCvv)]
        [LocalizedPrompt(TextRegionConstants.Cvv)]
        [RequiredIf(nameof(CardCodeRequired), "Security Code (CVV) is required.")]
        [VpRange(typeof(string), "0000", "9999", TextRegionConstants.CardSecurityCodeInvalid)]
        public string CardCode { get; set; }

        [LocalizedPrompt(TextRegionConstants.AddressCity)]
        [Required(ErrorMessage = "City is required.")]
        public string City { get; set; }

        [LocalizedDisplayName(TextRegionConstants.ExpirationDate)]
        [Required(ErrorMessage = "Expiration Date (Month) is required.")]
        public int ExpirationMonth { get; set; }

        [Required(ErrorMessage = "Expiration Date (Year) is required.")]
        public int ExpirationYear { get; set; }

        [LocalizedDisplayName(TextRegionConstants.NameOnCard)]
        [LocalizedPrompt(TextRegionConstants.NameOnCard)]
        [Required(ErrorMessage = "Name on Card is required.")]
        [VpStringLength(60, TextRegionConstants.CardNameLengthErrorMessage)]
        public string NameOnCard { get; set; }

        [Required(ErrorMessage = "State is required.")]
        public string State { get; set; }

        [LocalizedPrompt(TextRegionConstants.AddressZip)]
        [Required(ErrorMessage = "Zip is required.")]
        [RegularExpressionZipCode(TextRegionConstants.InvalidZip)]
        public string Zip { get; set; }
        
        [LocalizedDisplayName(TextRegionConstants.CardNumber)]
        [LocalizedPrompt(TextRegionConstants.CardNumber)]
        [Required(ErrorMessage = "Card Number is required.")]
        public override string BillingId { get; set; }

        public IList<SelectListItem> Months
        {
            get
            {
                List<SelectListItem> items = new List<SelectListItem>();
                for (int i = 1; i <= 12; i++)
                {
                    items.Add(new SelectListItem
                    {
                        Text = $"{i.ToString().PadLeft(2, '0')} - {CultureInfo.CurrentUICulture.DateTimeFormat.GetMonthName(i)}",
                        Value = i.ToString()
                    });
                }

                return items;
            }
        }

        public IList<SelectListItem> Years
        {
            get
            {
                List<SelectListItem> items = new List<SelectListItem>();
                for (int i = DateTime.UtcNow.Year; i <= DateTime.UtcNow.Year + 10; i++)
                {
                    items.Add(new SelectListItem
                    {
                        Text = i.ToString(),
                        Value = i.ToString().Substring(2, 2)
                    });
                }

                return items;
            }
        }

        public IList<SelectListItem> AccountTypes
        {
            get
            {
                List<SelectListItem> list = this.PaymentMethodAccountTypes.Select(x => new SelectListItem
                {
                    Text = x.PaymentMethodAccountTypeListDisplayText,
                    Value = x.PaymentMethodAccountTypeId.ToString()
                }).ToList();

                list.Insert(0, new SelectListItem
                {
                    Text = $"--{LocalizationHelper.GetLocalizedString(TextRegionConstants.AccountType)}--",
                    Value = "0"
                });

                return list;
            }
        }

        public IList<SelectListItem> ProviderTypes
        {
            get 
            {
                return this.PaymentMethodProviderTypes.Select(x => new SelectListItem 
                { 
                    Text = x.PaymentMethodProviderTypeDisplayText, Value = x.PaymentMethodProviderTypeId.ToString() 
                }).ToList();
            }
        }

        public IList<PaymentMethodAccountTypeViewModel> PaymentMethodAccountTypes { get; set; }

        public int AccountTypeId { get; set; }

        public IList<PaymentMethodProviderTypeViewModel> PaymentMethodProviderTypes { get; set; }

        [Required(ErrorMessage = "Provider type is required.")]
        public int ProviderTypeId { get; set; }

        public string AvsCode { get; set; }
    }
}
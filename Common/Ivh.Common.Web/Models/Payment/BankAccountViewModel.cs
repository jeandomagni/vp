namespace Ivh.Common.Web.Models.Payment
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.Linq;
    using System.Web.Mvc;
    using Attributes;
    using Base.Constants;
    using Base.Enums;
    using Utilities;
    using VisitPay.Constants;
    using VisitPay.Enums;

    public class BankAccountViewModel : PaymentAccountViewModel
    {
        [LocalizedDisplayName(TextRegionConstants.AccountNumber)]
        [LocalizedPrompt(TextRegionConstants.AccountNumber)]
        [RequiredIf("PaymentMethodId", 0, ErrorMessage = "Account Number is required.")]
        [DataType("BankAccountNumber")]
        public string AccountNumber { get; set; }

        [LocalizedDisplayName(TextRegionConstants.ReEnterAccountNumber)]
        [LocalizedPrompt(TextRegionConstants.ReEnterAccountNumber)]
        [RequiredIf("PaymentMethodId", 0, ErrorMessage = "Re-enter Account Number is required.")]
        [CompareToIfNotEqual("AccountNumber", "AccountNumber", "AccountNumberConfirm", false, TextRegionConstants.AccountNumbersDoNotMatch)]
        [DataType("BankAccountNumber")]
        public string AccountNumberConfirm { get; set; }

        [LocalizedDisplayName(TextRegionConstants.BankName)]
        [LocalizedPrompt(TextRegionConstants.BankName)]
        [Required(ErrorMessage = "Bank Name is required.")]
        public string BankName { get; set; }

        [LocalizedDisplayName(TextRegionConstants.NameFirst)]
        [LocalizedPrompt(TextRegionConstants.NameFirst)]
        [Required(ErrorMessage = "Account Holder First Name is required.")]
        public string FirstName { get; set; }

        [LocalizedDisplayName(TextRegionConstants.NameLast)]
        [LocalizedPrompt(TextRegionConstants.NameLast)]
        [Required(ErrorMessage = "Account Holder Last Name is required.")]
        public string LastName { get; set; }

        [LocalizedDisplayName(TextRegionConstants.RoutingNumber)]
        [LocalizedPrompt(TextRegionConstants.RoutingNumber)]
        [RequiredIf("PaymentMethodId", 0, ErrorMessage = "Routing Number is required.")]
        [DataType("BankRoutingNumber")]
        public string RoutingNumber { get; set; }

        public IList<SelectListItem> AchTypes => new List<SelectListItem>
        {
            new SelectListItem {Text = LocalizationHelper.GetLocalizedString(TextRegionConstants.Checking), Value = ((int) PaymentMethodTypeEnum.AchChecking).ToString()},
            new SelectListItem {Text = LocalizationHelper.GetLocalizedString(TextRegionConstants.Savings), Value = ((int) PaymentMethodTypeEnum.AchSavings).ToString()}
        };

        public string AchTypeString
        {
            get
            {
                SelectListItem type = this.AchTypes.FirstOrDefault(x => Convert.ToInt32(x.Value) == this.PaymentMethodTypeId);
                return type == null ? "" : type.Text;
            }
        }
    }
}
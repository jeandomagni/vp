﻿
namespace Ivh.Common.Web.Models.Payment
{
    public class PaymentMethodAccountTypeViewModel
    {
        public int PaymentMethodAccountTypeId { get; set; }
        public string PaymentMethodAccountTypeListDisplayText { get; set; }
        public string PaymentMethodAccountTypeSelectedDisplayText { get; set; }
        public int DisplayOrder { get; set; }
    }
}

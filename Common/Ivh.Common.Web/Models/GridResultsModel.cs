﻿namespace Ivh.Common.Web.Models
{
    using System;
    using System.Collections.Generic;

    public class GridResultsModel<T> where T : class
    {
        public GridResultsModel(int page, int rows, int records)
        {
            this.Results = new List<T>();
            this.page = page;
            this.total = records == 0 ? 0 : Convert.ToInt32(Math.Ceiling((decimal) records/rows));
            this.records = records;
        }

        public IList<T> Results { get; set; }

        // ReSharper disable once InconsistentNaming
        /// <summary>
        /// current page
        /// </summary>
        public int page { get; private set; }

        // ReSharper disable once InconsistentNaming
        /// <summary>
        /// total number of pages
        /// </summary>
        public int total { get; private set; }

        // ReSharper disable once InconsistentNaming
        /// <summary>
        /// total number of items
        /// </summary>
        public int records { get; private set; }
    }
}
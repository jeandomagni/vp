﻿namespace Ivh.Common.Web.Models.Support
{
    using System.Linq;

    public class SupportRequestResultsViewModel : GridResultsModel<SupportRequestResultViewModel>
    {
        public SupportRequestResultsViewModel(int page, int rows, int records)
            : base(page, rows, records)
        {
        }

        public int SupportRequestsWithUnreadMessages
        {
            get { return this.Results.Count(x => x.HasMessages && x.UnreadMessages > 0); }
        }
    }
}

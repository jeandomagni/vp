﻿namespace Ivh.Common.Web.Models.Support
{
    using System.Collections.Generic;
    using Ivh.Application.Core.Common.Dtos;

    public class ContactUsResultsViewModel
    {
        public IList<FacilityDto> Facilities { get; set; } = new List<FacilityDto>();
    }
}

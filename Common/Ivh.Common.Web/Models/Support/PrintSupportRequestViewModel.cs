﻿namespace Ivh.Common.Web.Models.Support
{
    public class PrintSupportRequestViewModel
    {
        public SupportRequestViewModel SupportRequest { get; set; }
        public string UserName { get; set; }
        public int RegistrationYear { get; set; }
    }
}
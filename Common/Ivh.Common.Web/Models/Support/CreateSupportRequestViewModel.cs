﻿namespace Ivh.Common.Web.Models.Support
{
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.Web.Mvc;
    using Account;
    using Ivh.Application.Content.Common.Dtos;
    using Base.Enums;
    using VisitPay.Enums;

    public class CreateSupportRequestViewModel
    {
        public bool IsMobile { get; set; }

        public string GuarantorName { get; set; }

        [Required]
        public string MessageBody { get; set; }

        public int? SubmittedForVisitPayUserId { get; set; }

        public IList<SelectListItem> Users { get; set; }

        public VisitDetailsViewModel Visit { get; set; }

        public int? VisitId { get; set; }

        public CmsVersionDto SelfServiceTextCmsVersionDto { get; set; }

        public Dictionary<string, string> ReplacementDictionary => new Dictionary<string, string>
        {
            ["[[Link1]]"] = "/KnowledgeBase?subCategoryId=19&categoryId=0",
            ["[[Link2]]"] = "/KnowledgeBase?subCategoryId=12&categoryId=0",
            ["[[Link3]]"] = "/KnowledgeBase?subCategoryId=20&categoryId=0"
        };

        public CmsRegionEnum SelfServiceCmsRegion => this.IsMobile ? CmsRegionEnum.CreateSupportRequestModalSelfServeTextMobile : CmsRegionEnum.CreateSupportRequestModalSelfServeText;
    }
}
﻿namespace Ivh.Common.Web.Models.Support
{
    public class SupportRequestResultViewModel
    {
        public int SupportRequestId { get; set; }
        public string SupportRequestDisplayId { get; set; }
        public string InsertDate { get; set; }
        public string SupportRequestStatus { get; set; }
        public string SupportTopic { get; set; }
        public string MessageBody { get; set; }
        public bool HasAttachments { get; set; }
        public bool HasMessages { get; set; }
        public int UnreadMessages { get; set; }
        public string SubmittedFor { get; set; }
    }
}
﻿namespace Ivh.Common.Web.Models.Support
{
    using System.ComponentModel.DataAnnotations;

    public class SupportRequestGenericMessageViewModel
    {
        public int SupportRequestId { get; set; }

        [Required]
        public string MessageBody { get; set; }
    }
}

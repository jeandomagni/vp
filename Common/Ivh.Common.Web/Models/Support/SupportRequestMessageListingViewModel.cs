﻿namespace Ivh.Common.Web.Models.Support
{
    public class SupportRequestMessageListingViewModel
    {
        public int SupportRequestId { get; set; }
        public string SupportRequestDisplayId { get; set; }
        public string InsertDate { get; set; }
        public string MessageBody { get; set; }
    }
}
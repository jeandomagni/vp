﻿namespace Ivh.Common.Web.Models.Support
{
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.Linq;
    using System.Web.Mvc;
    using Attributes;
    using Base.Constants;
    using Base.Enums;
    using VisitPay.Constants;
    using VisitPay.Enums;

    public class SupportRequestViewModel
    {
        public SupportRequestViewModel()
        {
            this.GuarantorMessages = new List<SupportRequestMessageViewModel>();
            this.InternalMessages = new List<SupportRequestMessageViewModel>();
            this.SupportAdminUsers = new List<SelectListItem>();
        }

        public int? AssignedVisitPayUserId { get; set; }
        public string ClosedBy { get; set; }
        public string ClosedDate { get; set; }
        public string CreatedDate { get; set; }
        public string GuarantorFullName { get; set; }
        public bool IsPreviousConsolidation { get; set; }
        public string PatientFullName { get; set; }
        public string SubmittedFor { get; set; }
        public int SupportRequestId { get; set; }
        public string SupportRequestDisplayId { get; set; }
        public int SupportRequestStatusId { get; set; }
        public string SupportRequestStatusName { get; set; }
        public string TreatmentDate { get; set; }
        public string TreatmentLocation { get; set; }
        public string VisitSourceSystemKey { get; set; }
        public string VisitSourceSystemKeyDisplay { get; set; }
        public string VisitMatchedSourceSystemKey { get; set; }
        public string VisitDescription { get; set; }
        public string DraftMessage { get; set; }
        public bool IsVisitRemoved { get; set; }
        public bool IsFlaggedForFollowUp { get; set; }
        public bool HasVisit { get; set; }
        public int? SubmittedForVpGuarantorId { get; set; }
        public int? SubmittedForVisitPayUserId { get; set; }
        public int? FromVpGuarantorId { get; set; }
        public int? VisitId { get; set; }
        
        [LocalizedDisplayName(TextRegionConstants.RequestTopic)]
        [Required]
        public int? SupportTopicId { get; set; }

        [LocalizedDisplayName(TextRegionConstants.RequestSubtopic)]
        [Required]
        public int? SupportSubTopicId { get; set; }
        public string Topic { get; set; }

        public IList<SupportRequestMessageViewModel> GuarantorMessages { get; set; }

        public IList<SupportRequestMessageViewModel> InternalMessages { get; set; }
        public IList<SelectListItem> SupportAdminUsers { get; set; }

        public IList<SupportRequestMessageAttachmentViewModel> Attachments
        {
            get { return this.GuarantorMessages.SelectMany(x => x.Attachments).Union(this.InternalMessages.SelectMany(x => x.Attachments)).ToList(); }
        }

        public bool IsOpen => ((SupportRequestStatusEnum) this.SupportRequestStatusId) == SupportRequestStatusEnum.Open;

        public bool IsClosed => ((SupportRequestStatusEnum)this.SupportRequestStatusId) == SupportRequestStatusEnum.Closed;
    }
}
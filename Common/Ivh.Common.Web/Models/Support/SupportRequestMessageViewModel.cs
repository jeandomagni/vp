﻿namespace Ivh.Common.Web.Models.Support
{
    using System.Collections.Generic;
    using System.Linq;

    public class SupportRequestMessageViewModel
    {
        public SupportRequestMessageViewModel()
        {
            this.Attachments = new List<SupportRequestMessageAttachmentViewModel>();
        }

        public string InsertDate { get; set; }
        public string MessageBody { get; set; }
        public int SupportRequestMessageId { get; set; }
        public int SupportRequestMessageType { get; set; }
        public string VisitPayUserName { get; set; }

        public IList<SupportRequestMessageAttachmentViewModel> Attachments { get; set; }

        public bool HasAttachments { get { return this.Attachments.Any(); } }
    }
}
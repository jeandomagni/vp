﻿namespace Ivh.Common.Web.Models.Support
{
    using System.Collections.Generic;
    using Ivh.Application.Content.Common.Dtos;

    public class SupportRequestWidgetViewModel
    {
        private readonly bool _isMobile;

        public SupportRequestWidgetViewModel(bool isMobile)
        {
            this._isMobile = isMobile;
        }

        public List<SupportRequestMessageListingViewModel> SupportRequestMessages { get; set; } = new List<SupportRequestMessageListingViewModel>();

        public CmsVersionDto SelfServiceTextCmsVersionDto { get; set; }

        private static string MobileUrl => "/mobile/Home/Faqs#";

        private static string WebUrl => "/Home/Help";

        public Dictionary<string, string> ReplacementDictionary => new Dictionary<string, string>
        {
            ["[[SupportUrl]]"] = this._isMobile ? MobileUrl : WebUrl
        };
    }
}
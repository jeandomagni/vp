﻿using Ivh.Common.VisitPay.Enums;

namespace Ivh.Common.Web.Models.Support
{
    public class SupportRequestMessageAttachmentViewModel
    {
        public int SupportRequestMessageAttachmentId { get; set; }
        public string AttachmentFileName { get; set; }
        public string MimeType { get; set; }
        public int FileSize { get; set; }
        public string InsertDate { get; set; }
        public MalwareScanStatusEnum MalwareScanStatus { get; set; }
    }
}
﻿namespace Ivh.Common.Web.Models.Support
{
    using System.ComponentModel.DataAnnotations;
    using Base.Enums;
    using VisitPay.Enums;

    public class SupportRequestActionBaseModel
    {
        public SupportRequestStatusEnum CurrentSupportRequestStatus { get; set; }
        public int SupportRequestId { get; set; }
        public string SupportRequestDisplayId { get; set; }

        [Required(ErrorMessage = "Note is required")]
        public string MessageBody { get; set; }

        public string ActionName
        {
            get { return this.CurrentSupportRequestStatus == SupportRequestStatusEnum.Closed ? "Re-Open" : "Close"; }
        }
    }
}

﻿namespace Ivh.Common.Web.Models
{
    public class ResultMessage
    {
        public bool Result { get; set; }
        public string Message { get; set; }

        public ResultMessage(bool result, string message)
        {
            this.Result = result;
            this.Message = message;
        }
    }

    public class ResultMessage<T>
    {
        public bool Result { get; set; }
        public T Message { get; set; }

        public ResultMessage(bool result, T message)
        {
            this.Result = result;
            this.Message = message;
        }
    }
}
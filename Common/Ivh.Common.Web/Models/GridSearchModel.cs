﻿namespace Ivh.Common.Web.Models
{
    using System;

    public class GridSearchModel<T> where T : class
    {
        public GridSearchModel()
        {
            this.Filter = (T) Activator.CreateInstance(typeof(T));
        }

        private T _filter;

        /// <summary>
        /// web uses "Filter"
        /// </summary>
        public T Filter
        {
            get { return this._filter; }
            set { this._filter = value; }
        }

        /// <summary>
        /// mobile uses "Model"
        /// </summary>
        public T Model
        {
            get { return this._filter; }
            set { this._filter = value; }
        }

        /// <summary>
        /// page index (1, not 0)
        /// </summary>
        public int Page { get; set; } 

        /// <summary>
        /// rows per page
        /// </summary>
        public int Rows { get; set; }
        
        /// <summary>
        /// sort field property name
        /// </summary>
        public string Sidx { get; set; }

        /// <summary>
        /// asc or desc
        /// </summary>
        public string Sord { get; set; }
    }
}
﻿namespace Ivh.Common.Web.Models.Guarantor
{
    public class CommunicationGroupSelectionViewModel
    {
        public string VisitPayUserId { get; set; }
        public int CommunicationGroupId { get; set; }
        public string CommunicationGroupName { get; set; }
        public string IconType { get; set; }
        public string Description { get; set; }
        public bool Enabled { get; set; }
    }
}
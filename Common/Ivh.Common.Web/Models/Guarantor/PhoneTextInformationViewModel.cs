namespace Ivh.Common.Web.Models.Guarantor
{
    using System.Collections.Generic;
    using Base.Enums;
    using VisitPay.Enums;

    public class PhoneTextInformationViewModel
    {
        public string VisitPayUserId { get; set; }
        
        public string PhoneNumber { get; set; }
        
        public string PhoneNumberType { get; set; }

        public bool PhoneNumberTypeIsMobile { get; set; }
        
        public string PhoneNumberSecondary { get; set; }
        
        public string PhoneNumberSecondaryType { get; set; }

        public bool PhoneNumberSecondaryTypeIsMobile { get; set; }

        public bool SmsModuleEnabled { get; set; }

        public SmsPhoneTypeEnum? SmsEnabledFor { get; set; }

        public int CmsVersionId { get; set; }

        public IList<CommunicationGroupSelectionViewModel> Preferences { get; set; }

        public bool HasPrimaryPhone => !string.IsNullOrWhiteSpace(this.PhoneNumber);

        public bool HasSecondaryPhone => !string.IsNullOrWhiteSpace(this.PhoneNumberSecondary);

        public string PrimarySmsEnabledText => this.SmsEnabledFor.HasValue && this.SmsEnabledFor.Value == SmsPhoneTypeEnum.Primary ? "Enabled" : "Disabled";
        public string SecondarySmsEnabledText => this.SmsEnabledFor.HasValue && this.SmsEnabledFor.Value == SmsPhoneTypeEnum.Secondary ? "Enabled" : "Disabled";

        public string PrimaryTypeIcon => this.PhoneNumberTypeIsMobile ? "glyphicon-phone" : "glyphicon-earphone";
        public string SecondaryTypeIcon => this.PhoneNumberSecondaryTypeIsMobile ? "glyphicon-phone" : "glyphicon-earphone";
    }
}
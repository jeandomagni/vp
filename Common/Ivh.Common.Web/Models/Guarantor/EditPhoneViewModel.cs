namespace Ivh.Common.Web.Models.Guarantor
{
    using System.ComponentModel.DataAnnotations;
    using Attributes;
    using Base.Constants;
    using Base.Enums;
    using VisitPay.Constants;
    using VisitPay.Enums;

    public class EditPhoneViewModel
    {
        public string VisitPayUserId { get; set; }

        [LocalizedDisplayName(TextRegionConstants.PhoneNumber)]
        [LocalizedPrompt(TextRegionConstants.PhoneNumber)]
        [Required(ErrorMessage = "Phone Number is required.")]
        [DataType(DataType.PhoneNumber)]
        public string PhoneNumber { get; set; }

        [LocalizedDisplayName(TextRegionConstants.PhoneType)]
        [LocalizedPrompt(TextRegionConstants.PhoneType)]
        [Required(ErrorMessage = "Phone Type is required.")]
        public string PhoneNumberType { get; set; }

        public bool IsSmsEnabled { get; set; }

        public bool IsSmsActivateEnabled { get; set; }

        public bool IsSmsSwitchToEnabled { get; set; }

        public SmsPhoneTypeEnum SmsPhoneType { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ivh.Common.Web.Models.Account
{
    using System.ComponentModel.DataAnnotations;
    using Attributes;
    using Base.Constants;
    using VisitPay.Constants;

    public class ProfileEditUserNameViewModel
    {
        [LocalizedDisplayName(TextRegionConstants.Username)]
        [LocalizedPrompt(TextRegionConstants.Username)]
        [Required(ErrorMessage = "Username is required.")]
        [MinLength(6, ErrorMessage = "This is an invalid username.  Please try again.")]
        public string UserName { get; set; }

        public string UserNameOriginal { get; set; }

        [LocalizedDisplayName(TextRegionConstants.ReEnterUsername)]
        [LocalizedPrompt(TextRegionConstants.ReEnterUsername)]
        [CompareToIfNotEqual("UserName", "UserNameOriginal", "UserName", false, TextRegionConstants.UsernamesDoNotMatch)]
        public string UserNameConfirm { get; set; }
    }
}

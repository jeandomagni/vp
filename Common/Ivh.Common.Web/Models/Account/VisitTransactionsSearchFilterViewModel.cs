namespace Ivh.Common.Web.Models.Account
{
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using Attributes;
    using Base.Constants;
    using Base.Enums;
    using VisitPay.Constants;
    using VisitPay.Enums;

    public class VisitTransactionsSearchFilterViewModel
    {
        public VisitTransactionsSearchFilterViewModel()
        {
            this.TransactionType = new List<VpTransactionTypeFilterEnum>();
        }

        public int VisitId { get; set; }

        public int? VisitPayUserId { get; set; }

        [LocalizedDisplayName(TextRegionConstants.FilterByTransactionType)]
        public IList<VpTransactionTypeFilterEnum> TransactionType { get; set; }
    }
}
﻿namespace Ivh.Common.Web.Models.Account
{
    using FinancePlan;

    public class FinancePlanHistoryResultsModel : GridResultsModel<FinancePlansSearchResultViewModel>
    {
        public FinancePlanHistoryResultsModel(int page, int rows, int records)
            : base(page, rows, records)
        {
        }
    }
}
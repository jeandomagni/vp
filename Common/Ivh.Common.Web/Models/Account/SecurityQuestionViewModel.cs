namespace Ivh.Common.Web.Models.Account
{
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.Web.Mvc;
    using Attributes;
    using Base.Constants;
    using VisitPay.Constants;

    public class SecurityQuestionViewModel
    {
        public IList<SelectListItem> SecurityQuestions { get; set; }

        public SecurityQuestionViewModel()
        {
            this.SecurityQuestions = new List<SelectListItem>();
        }

        [Required]
        [LocalizedDisplayName(TextRegionConstants.SecurityQuestions)]
        [LocalizedPrompt(TextRegionConstants.ChooseAQuestion)]
        public int SecurityQuestionId { get; set; }

        [Required]
        [LocalizedDisplayName(TextRegionConstants.SecurityQuestionAnswer)]
        [LocalizedPrompt(TextRegionConstants.EnterYourAnswer)]
        public string Answer { get; set; }
    }
}
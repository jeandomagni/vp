﻿namespace Ivh.Common.Web.Models.Account
{
    using System.ComponentModel.DataAnnotations;
    using Attributes;
    using Base.Constants;
    using VisitPay.Constants;

    public class ProfileEditViewModel
    {
        [LocalizedDisplayName(TextRegionConstants.NameFirst)]
        [LocalizedPrompt(TextRegionConstants.NameFirst)]
        [Required(ErrorMessage = "First Name is required.")]
        public string FirstName { get; set; }
        
        [LocalizedDisplayName(TextRegionConstants.NameMiddle)]
        [LocalizedPrompt(TextRegionConstants.NameMiddle)]
        public string MiddleName { get; set; }
        
        [LocalizedDisplayName(TextRegionConstants.NameLast)]
        [LocalizedPrompt(TextRegionConstants.NameLast)]
        [Required(ErrorMessage = "Last Name is required.")]
        public string LastName { get; set; }
        
        [LocalizedDisplayName(TextRegionConstants.Username)]
        [LocalizedPrompt(TextRegionConstants.Username)]
        [Required(ErrorMessage = "Username is required.")]
        [MinLength(6, ErrorMessage = "This is an invalid username. Please try again.")]
        public string UserName { get; set; }
        
        [LocalizedDisplayName(TextRegionConstants.EmailAddress)]
        [LocalizedPrompt(TextRegionConstants.EmailAddress)]
        [RegularExpressionEmailAddress(TextRegionConstants.NotificationEmailInvalid)]
        [RequiredIf("IsOfflineUser", false)]
        [DataType(DataType.EmailAddress)]
        public string Email { get; set; }

        public string EmailOriginal { get; set; }
        
        [LocalizedDisplayName(TextRegionConstants.ConfirmEmailAddress)]
        [LocalizedPrompt(TextRegionConstants.ConfirmEmailAddress)]
        [DataType(DataType.EmailAddress)]
        [CompareToIfNotEqual("Email", "EmailOriginal", "Email", false, TextRegionConstants.NotificationEmailsDoNotMatch)]
        public string EmailConfirm { get; set; }
        
        [LocalizedDisplayName(TextRegionConstants.Address)]
        [LocalizedPrompt(TextRegionConstants.AddressStreetPo)]
        [Required(ErrorMessage = "Street or PO Box is required.")]
        public string AddressStreet1 { get; set; }
        
        [LocalizedPrompt(TextRegionConstants.AptSuiteOrBuildingNumber)]
        public string AddressStreet2 { get; set; }
        
        [LocalizedDisplayName(TextRegionConstants.AddressCity)]
        [LocalizedPrompt(TextRegionConstants.AddressCity)]
        [Required(ErrorMessage = "City is required.")]
        public string City { get; set; }
        
        [LocalizedDisplayName(TextRegionConstants.State)]
        [Required(ErrorMessage = "State is required.")]
        public string State { get; set; }
        
        [LocalizedDisplayName(TextRegionConstants.AddressZip)]
        [LocalizedPrompt(TextRegionConstants.AddressZip)]
        [Required(ErrorMessage = "Zip is required.")]
        [RegularExpressionZipCode(TextRegionConstants.InvalidZip)]
        public string Zip { get; set; }

        [LocalizedDisplayName(TextRegionConstants.Address)]
        [LocalizedPrompt(TextRegionConstants.AddressStreetPo)]
        //[Required(ErrorMessage = "Street or PO Box is required.")]
        public string MailingAddressStreet1 { get; set; }
        
        [LocalizedPrompt(TextRegionConstants.AptSuiteOrBuildingNumber)]
        public string MailingAddressStreet2 { get; set; }
        
        [LocalizedDisplayName(TextRegionConstants.AddressCity)]
        [LocalizedPrompt(TextRegionConstants.AddressCity)]
        //[Required(ErrorMessage = "City is required.")]
        public string MailingCity { get; set; }
        
        [LocalizedDisplayName(TextRegionConstants.State)]
        //[Required(ErrorMessage = "State is required.")]
        public string MailingState { get; set; }
        
        [LocalizedDisplayName(TextRegionConstants.AddressZip)]
        [LocalizedPrompt(TextRegionConstants.AddressZip)]
        //[Required(ErrorMessage = "Zip is required.")]
        //[RegularExpressionZipCode(TextRegionConstants.InvalidZip)]
        public string MailingZip { get; set; }

        public bool IsOfflineUser { get; set; }
    }
}
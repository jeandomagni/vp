﻿namespace Ivh.Common.Web.Models.Account
{
    using Base.Enums;
    using VisitPay.Enums;

    public abstract class VisitSearchResultBaseViewModel
    {
        public string DisplayBalance { get; set; }
        public decimal DisplayBalanceDecimal { get; set; }
        public string DischargeDate { get; set; }
        public string DischargeDateMonth { get; set; }
        public string DischargeDateDay { get; set; }
        public string DischargeDateYear { get; set; }
        public string EobLogoUrl { get; set; }
        public string FacilityDescription { get; set; }
        public string FacilityLogoFilename { get; set; }
        public bool HasFacilityInfo => !string.IsNullOrWhiteSpace(this.FacilityDescription);
        public bool HasFacilityImage => !string.IsNullOrWhiteSpace(this.FacilityLogoFilename);
        public bool HasVisitEobDetails { get; set; }
		public bool IsHbVisit { get; set; }
        public bool IsPbVisit { get; set; }
        public string PatientName { get; set; }
        public string SourceSystemKey { get; set; }
        public string SourceSystemKeyDisplay { get; set; }
        public string StatusDisplay { get; set; }
        public string VisitDescription { get; set; }
        public int VisitId { get; set; }
        public VisitStateEnum VisitStateId { get; set; }
        public int? ServiceGroupId { get; set; }
        public string ServiceGroupLogoUrl { get; set; }
        public string ServiceGroupDescription { get; set; }
        public bool ShowServiceGroup => !string.IsNullOrWhiteSpace(this.ServiceGroupLogoUrl);
    }
}
﻿namespace Ivh.Common.Web.Models.Account
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.Web.Mvc;
    using Attributes;
    using VisitPay.Constants;

    public class PaymentSummarySearchFilterViewModel
    {
        [LocalizedDisplayName(TextRegionConstants.PaymentPeriod)]
        public string PaymentPeriod { get; set; }

        [LocalizedPrompt(TextRegionConstants.StartDate)]
        [DateRangeCompare(false, "PostDateBegin", "PostDateEnd", DateRangeCompareType.LessThanOrEqual, ErrorMessage = "Invalid Date Range")]
        [DataType(DataType.Date)]
        public DateTime? PostDateBegin { get; set; }

        [LocalizedPrompt(TextRegionConstants.EndDate)]
        [DateRangeCompare(false, "PostDateEnd", "PostDateBegin", DateRangeCompareType.GreaterThanOrEqual, ErrorMessage = "Invalid Date Range")]
        [DataType(DataType.Date)]
        public DateTime? PostDateEnd { get; set; }

        public IList<SelectListItem> PaymentPeriods => new List<SelectListItem>
        {
            new SelectListItem {Text = "Year to Date", Value = "YTD"},
            new SelectListItem {Text = "Previous Year", Value = "PY"},
            new SelectListItem {Text = "Custom", Value = "C"},
        };
    }
}
﻿namespace Ivh.Common.Web.Models.Account
{
    using System.ComponentModel.DataAnnotations;
    using Attributes;
    using VisitPay.Constants;

    public class AddressViewModel
    {
        [LocalizedDisplayName(TextRegionConstants.Address)]
        [LocalizedPrompt(TextRegionConstants.AddressStreetPo)]
        [Required(ErrorMessage = "Street or PO Box is required.")]
        public string AddressStreet1 { get; set; }
        
        [LocalizedPrompt(TextRegionConstants.AptSuiteOrBuildingNumber)]
        public string AddressStreet2 { get; set; }
        
        [LocalizedDisplayName(TextRegionConstants.AddressCity)]
        [LocalizedPrompt(TextRegionConstants.AddressCity)]
        [Required(ErrorMessage = "City is required.")]
        public string City { get; set; }
        
        [LocalizedDisplayName(TextRegionConstants.State)]
        [Required(ErrorMessage = "State is required.")]
        public string StateProvince { get; set; }
        
        [LocalizedDisplayName(TextRegionConstants.AddressZip)]
        [LocalizedPrompt(TextRegionConstants.AddressZip)]
        [Required(ErrorMessage = "Postal Code is required.")]
        [RegularExpressionZipCode(TextRegionConstants.InvalidZip)]
        public string PostalCode { get; set; }

        public string UspsAddressStreet1 { get; set; }
        public string UspsAddressStreet2 { get; set; }
        public string UspsCity { get; set; }
        public string UspsStateProvince { get; set; }
        public string UspsPostalCode { get; set; }

        public bool IncludesUspsStandardizedAddress { get; set; }
        public bool IsUspsFormatSelected { get; set; }

        public bool ShowRemoveEmailPreferences { get; set; }
        public bool RemoveEmailPreferences { get; set; }
    }
}
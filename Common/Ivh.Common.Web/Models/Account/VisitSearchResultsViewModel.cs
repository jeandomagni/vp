﻿namespace Ivh.Common.Web.Models.Account
{
    using System.Collections.Generic;

    public class VisitSearchResultsViewModel : SearchResult
    {
        public IList<VisitsSearchResultViewModel> Visits { get; set; }
        public string CurrentTotalBalance { get; set; }
    }
}
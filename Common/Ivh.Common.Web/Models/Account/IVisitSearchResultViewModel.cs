﻿namespace Ivh.Common.Web.Models.Account
{
    using Base.Enums;
    using VisitPay.Enums;

    public interface IVisitSearchResultViewModel
    {
        // implemented on VisitSearchResultBaseViewModel
        string DisplayBalance { get; }
        decimal DisplayBalanceDecimal { get; }
        string DischargeDate { get; }
        string DischargeDateMonth { get; }
        string DischargeDateDay { get; }
        string DischargeDateYear { get; }
        string EobLogoUrl { get; }
        bool HasVisitEobDetails { get; }
        bool IsHbVisit { get; }
        bool IsPbVisit { get; }
        string PatientName { get; }
        string SourceSystemKey { get; }
        string SourceSystemKeyDisplay { get; }
        string StatusDisplay { get; }
        string VisitDescription { get; }
        VisitStateEnum VisitStateId { get; }

        // per implementation
        bool IsPastDue { get; }
        bool ShowStatus { get; }
        bool BillingHold { get; }
    }
}
﻿namespace Ivh.Common.Web.Models.Account
{
    using Payment;

    public class PaymentHistoryResultsModel : GridResultsModel<PaymentHistorySearchResultViewModel>
    {
        public PaymentHistoryResultsModel(int page, int rows, int records)
            : base(page, rows, records)
        {
        }
    }
}
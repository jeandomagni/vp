namespace Ivh.Common.Web.Models.Account
{
    public class VisitTransactionsSearchResultViewModel
    {
        // The formatting for DisplayDate is performed in the automapper profile
        public string DisplayDate { get; set; }
        public string TransactionType { get; set; }
        public string TransactionDescription { get; set; }
        public string TransactionAmount { get; set; }
    }
}
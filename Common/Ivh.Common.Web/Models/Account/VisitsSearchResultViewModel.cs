﻿namespace Ivh.Common.Web.Models.Account
{
    using Base.Utilities.Extensions;

    public class VisitsSearchResultViewModel : VisitSearchResultBaseViewModel, IVisitSearchResultViewModel
    {
        public bool BillingHold { get; set; }

        public int BillingSystemId { get; set; }
        
		public bool IsActive { get; set; }

        public bool IsFinanced { get; set; }

        public bool IsMaxAge { get; set;}

        public bool IsPastDue { get; set; }

        public bool ShowStatus => this.StatusDisplay.IsNotNullOrEmpty();
	}
}
﻿namespace Ivh.Common.Web.Models.Account
{
    public class PaymentSummaryGroupedTransactionViewModel
    {
        public string PostDate { get; set; }
        public string TransactionAmount { get; set; }
    }
}
﻿namespace Ivh.Common.Web.Models.Account
{
    public class VisitDetailsViewModel
    {
        public string DischargeDate { get; set; }
        public string GuarantorName { get; set; }
        public string PatientName { get; set; }
        public string SourceSystemKey { get; set; }
        public string SourceSystemKeyDisplay { get; set; }
        public int VisitId { get; set; }
        public int VisitPayUserId { get; set; }
        public string VisitDescription { get; set; }
        public string VisitStateFullDisplayName { get; set; }

        public decimal SummaryCharges { get; set; }
        public decimal SummaryInterest { get; set; }
        public decimal SummaryOther { get; set; }
        public decimal SummaryInsurance { get; set; }
        public decimal SummaryAllPayments { get; set; }

        public bool HasEob { get; set; }
        public string EobLogoUrl { get; set; }
    }
}

        
﻿namespace Ivh.Common.Web.Models.Account
{
    using Base.Utilities.Extensions;

    public class PaymentSummaryResultsViewModel : GridResultsModel<PaymentSummaryGroupedViewModel>
    {
        public PaymentSummaryResultsViewModel(int page, int rows, int records, decimal summaryBalanceTotal)
            : base(page, rows, records)
        {
            this.SummaryBalanceTotal = summaryBalanceTotal.FormatCurrency();
        }

        public string SummaryBalanceTotal { get; set; }
    }
}
﻿namespace Ivh.Common.Web.Models.Account
{
    using Payment;

    public class PaymentPendingResultsModel : GridResultsModel<PaymentPendingSearchResultViewModel>
    {
        public PaymentPendingResultsModel(int page, int rows, int records) 
            : base(page, rows, records)
        {
        }
    }
}
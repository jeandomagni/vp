﻿namespace Ivh.Common.Web.Models.Account
{
    using System.Collections.Generic;

    public class PaymentSummaryGroupedViewModel
    {
        public PaymentSummaryGroupedViewModel()
        {
            this.Payments = new List<PaymentSummaryGroupedTransactionViewModel>();
        }

        public string PatientName { get; set; }
        public string VisitDate { get; set; }
        public string VisitDescription { get; set; }
        public string VisitSourceSystemKeyDisplay { get; set; }
        public string TotalTransactionAmount { get; set; }

        public IList<PaymentSummaryGroupedTransactionViewModel> Payments { get; set; }
    }
}
﻿namespace Ivh.Common.Web.Models.Account
{
    public class VisitTransactionResultsModel : GridResultsModel<VisitTransactionsSearchResultViewModel>
    {
        public VisitTransactionResultsModel(int page, int rows, int records)
            : base(page, rows, records)
        {
        }
    }
}
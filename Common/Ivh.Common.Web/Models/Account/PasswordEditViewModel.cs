namespace Ivh.Common.Web.Models.Account
{
    using System.ComponentModel.DataAnnotations;
    using Attributes;
    using Base.Constants;
    using VisitPay.Constants;

    public class PasswordEditViewModel
    {
        [LocalizedDisplayName(TextRegionConstants.OldPassword)]
        [Required(ErrorMessage = "Old Password is required.")]
        [DataType(DataType.Password)]
        public string OldPassword { get; set; }

        [LocalizedDisplayName(TextRegionConstants.NewPassword)]
        [Required(ErrorMessage = "New Password is required.")]
        [DataType(DataType.Password)]
        public string NewPassword { get; set; }

        [LocalizedDisplayName(TextRegionConstants.ConfirmNewPassword)]
        [Required(ErrorMessage = "Confirm Password is required.")]
        [VpCompare("NewPassword", TextRegionConstants.PasswordsDoNotMatch)]
        [DataType(DataType.Password)]
        public string NewPasswordConfirm { get; set; }
    }
}
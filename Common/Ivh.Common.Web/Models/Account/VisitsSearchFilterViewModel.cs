﻿namespace Ivh.Common.Web.Models.Account
{
    using System.Collections.Generic;
    using System.Web.Mvc;
    using Base.Enums;
    using VisitPay.Enums;

    public class VisitsSearchFilterViewModel
    {
        public VisitsSearchFilterViewModel()
        {
            this.VisitStates = new List<SelectListItem>();
        }
        
        public IList<VisitStateEnum> VisitStateIds { get; set; }

        public IList<SelectListItem> VisitStates { get; set; }

        public bool? IsOnActiveFinancePlan { get; set; }

        public int? FinancePlanId { get; set; }
    }
}

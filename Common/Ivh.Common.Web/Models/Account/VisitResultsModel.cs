﻿namespace Ivh.Common.Web.Models.Account
{
    public class VisitResultsModel : GridResultsModel<VisitsSearchResultViewModel>
    {
        public VisitResultsModel(int page, int rows, int records)
            : base(page, rows, records)
        {
        }
    }
}
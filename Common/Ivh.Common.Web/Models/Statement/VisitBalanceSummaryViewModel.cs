﻿namespace Ivh.Common.Web.Models.Statement
{
    using System.Collections.Generic;

    public class VisitBalanceSummaryViewModel
    {
        public VisitBalanceSummaryViewModel()
        {
            this.Visits = new List<VisitBalanceViewModel>();
        }

        public IList<VisitBalanceViewModel> Visits { get; set; }

        public string PeriodBeginDate { get; set; }

        public string PeriodEndDate { get; set; }

        public string TotalVisitBalanceAmount { get; set; }

        public string CreditBalanceAmount { get; set; }

        public bool HasCreditBalance { get; set; }

        public string GuarantorFirstNameLastName { get; set; }
        public string GuarantorAddressStreet1 { get; set; }
        public string GuarantorAddressStreet2 { get; set; }
        public string GuarantorCityStateZip { get; set; }
        public string DisplayUrl { get; set; }
    }
}
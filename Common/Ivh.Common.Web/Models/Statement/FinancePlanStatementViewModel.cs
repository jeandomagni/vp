﻿namespace Ivh.Common.Web.Models.Statement
{
    using System;
    using System.Collections.Generic;

    public class FinancePlanStatementViewModel : BaseStatementViewModel
    {
        public FinancePlanStatementViewModel()
        {
            this.Alerts = new List<StatementAlertViewModel>();
            this.FinancePlanStatementFinancePlans = new List<FinancePlanStatementFinancePlanViewModel>();
        }
        
        // snapshots
        public string StatementedSnapshotFinancePlanFeesChargedForStatementYearSum { get; set; }
        public string StatementedSnapshotFinancePlanInterestChargedForStatementYearSum { get; set; }
        public string StatementedSnapshotFinancePlanPaymentDueSum { get; set; }

        public IList<StatementAlertViewModel> Alerts { get; set; }

        public IList<FinancePlanStatementFinancePlanViewModel> FinancePlanStatementFinancePlans { get; set; }

        public IList<FinancePlanStatementVisitViewModel> FinancePlanStatementVisits { get; set; }
    }
}
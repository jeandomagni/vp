﻿namespace Ivh.Common.Web.Models.Statement
{
    public class FinancePlanStatementVisitViewModel
    {
        public int FinancePlanId { get; set; }
        public string StatementedSnapshotTotalBalance { get; set; }
        public  string VisitDisplayDate { get; set; }
        public  string PatientName { get; set; }
        public  string SourceSystemKey { get; set; }
        public  string VisitDescription { get; set; }
        public string ServiceGroupIndicator { get; set; }
        public bool ShowServiceGroupIndicator => !string.IsNullOrWhiteSpace(this.ServiceGroupIndicator);
    }
}
﻿namespace Ivh.Common.Web.Models.Statement
{
    public class VisitBalanceViewModel
    {
        public string DisplayBalance { get; set; }
        public string DischargeDate { get; set; }
        public string PatientName { get; set; }
        public string VisitDescription { get; set; }
        public bool IsPastDue { get; set; }
        public bool IsFinalPastDue { get; set; }
    }
}

﻿namespace Ivh.Common.Web.Models.Statement
{
    using System.Web;
    using System.Web.Mvc;
    using Base.Enums;
    using Microsoft.Ajax.Utilities;
    using VisitPay.Enums;

    public class StatementAlertViewModel
    {

        private string _alertMessageWeb;
        UrlHelper urlHelper = new UrlHelper(HttpContext.Current.Request.RequestContext);

        public AlertTypeEnum AlertType { get; set; }

        public string AlertMessageWeb
        {
            get
            {
                return (RequireUrlReplacement)?this.ReplaceValues():this._alertMessageWeb;
            } 
            set { this._alertMessageWeb = value; }
        }

        public string AlertMessagePdf { get; set; }
        public string CssClass
        {
            get
            {
                switch (this.AlertType)
                {
                    case AlertTypeEnum.ActionRequired:
                        return "statement-alert-action-required";
                    case AlertTypeEnum.Information:
                        return "statement-alert-information";
                    default:
                        return string.Empty;
                }
            }
        }

        public string ControllerAction { get; set; }
        public string ControllerName { get; set; }
        public bool RequireUrlReplacement { get; set; }

        private string ReplaceValues()
        {
            if(ControllerAction.IsNullOrWhiteSpace() && ControllerName.IsNullOrWhiteSpace() ) return this._alertMessageWeb;
            var url = urlHelper.Action(ControllerAction, ControllerName);
            return this._alertMessageWeb.Replace("[[UrlPlaceholder]]", url);
        }
    }
}
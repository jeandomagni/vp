﻿namespace Ivh.Common.Web.Models.Statement
{
    using System.Collections.Generic;
    using System.Linq;

    public class StatementViewModel : BaseStatementViewModel
    {
        public StatementViewModel()
        {
            this.Alerts = new List<StatementAlertViewModel>();
            this.FinancePlans = new List<StatementFinancePlanViewModel>();
            this.VisitsNotOnFinancePlan = new List<StatementVisitViewModel>();
            this.ActiveVisitsNotOnFinancePlan = new List<StatementVisitViewModel>();
            this.Notifications = new List<string>();
        }

        // snapshots
        public string StatementedSnapshotDiscountsAndAdjustmentsSum { get; set; }
        public string StatementedSnapshotFinancePlanBalanceSum { get; set; }
        public string StatementedSnapshotFinancePlanInterestChargedSum { get; set; }
        public string StatementedSnapshotFinancePlanInterestDueSum { get; set; }
        public string StatementedSnapshotFinancePlanPrincipalDueSum { get; set; }
        public string StatementedSnapshotMinimumAmountDue { get; set; }
        public string StatementedSnapshotNewVisitBalanceSum { get; set; }
        public string StatementedSnapshotPaymentsAppliedSum { get; set; }
        public string StatementedSnapshotPriorTotalBalance { get; set; }
        public string StatementedSnapshotTotalBalance { get; set; }
        public string StatementedSnapshotTotalInterestChargedYTD { get; set; }
        public string StatementedSnapshotVisitBalanceSum { get; set; }
        public string StatementedSnapshotFinancePlanPaymentDueSum { get; set; }
        public string StatementedSnapshotVisitCreditBalanceSum { get; set; }
        public string StatementedSnapshotSuspendedClosedVisitBalanceSum { get; set; }
        public string StatementedSnapshotVpFeesAndInterest { get; set; }
        public string StatementedSnapshotVpPaymentsAndDiscounts { get; set; }
        public string StatementedSnapshotVisitHsTransactionsPaymentSum { get; set; }
        public string StatementedSnapshotHsAdjustmentsAndHsCharges { get; set; }
        public string StatementedSnapshotVisitHsTransactionsAdjustmentSum { get; set; }
        public string StatementedSnapshotVisitHsTransactionsAdjustmentsExcludingInsuranceSum { get; set; }
        public string StatementedSnapshotVisitHsTransactionsAdjustmentsInsuranceSum { get; set; }
        public string StatementedSnapshotVisitHsTransactionsChargeSum { get; set; }
        public IList<StatementAlertViewModel> Alerts { get; set; }
        public IList<string> Notifications { get; set; }

        public IList<StatementFinancePlanViewModel> FinancePlans { get; set; }

        public IList<StatementVisitViewModel> VisitsNotOnFinancePlan { get; set; }
        public IList<StatementVisitViewModel> ActiveVisitsNotOnFinancePlan { get; set; }
        
        public bool HasInsuranceAdjustmentDetail { get; set; }
        
        //vp2 specific
        public bool IsSoftStatement { get; set; }
    }
}
﻿namespace Ivh.Common.Web.Models.Statement
{
    public class FinancePlanStatementFinancePlanViewModel
    {
        public int FinancePlanId { get; set; }
        public string FinancePlanStatusDisplayName { get; set; }
        public string StatementedSnapshotFinancePlanEffectiveDate { get; set; }
        public string StatementedSnapshotFinancePlanInterestRate { get; set; }
        public string StatementedSnapshotPriorFinancePlanBalance { get; set; }
        public string StatementedSnapshotFinancePlanInterestCharged { get; set; }
        public string StatementedSnapshotFinancePlanInterestPaid { get; set; }
        public string StatementedSnapshotFinancePlanPrincipalPaid { get; set; }
        public string StatementedSnapshotFinancePlanOtherBalanceCharges { get; set; }
        public string StatementedSnapshotFinancePlanBalance { get; set; }
        public string StatementedSnapshotFinancePlanMonthlyPaymentAmount { get; set; }
        public string StatementedSnapshotFinancePlanPastDueAmount { get; set; }
        public string StatementedSnapshotFinancePlanPaymentDue { get; set; }
        public bool IsPastDue { get; set; }
        public string StatementedSnapshotFinancePlanOriginalBalance { get; set; }
        public bool IsFirstStatement { get; set; }
    }
}
﻿namespace Ivh.Common.Web.Models.Statement
{
    using System.Collections.Generic;
    using System.Linq;

    public class StatementListViewModel
    {
        public StatementListViewModel()
        {
            this.FinancePlanStatements = new List<StatementListItemViewModel>();
            this.VisitBalanceSummaryStatements = new List<StatementListItemViewModel>();
        }

        public IList<StatementListItemViewModel> FinancePlanStatements { get; set; }

        public IList<StatementListItemViewModel> VisitBalanceSummaryStatements { get; set; }

        public IList<StatementListItemViewModel> ArchivedStatements
        {
            get { return this.VisitBalanceSummaryStatements.Where(x => x.IsArchived).ToList(); }
        }

        public IList<StatementListItemViewModel> NotArchivedStatements {
            get { return this.VisitBalanceSummaryStatements.Where(x => !x.IsArchived).ToList(); }
        }

        public bool HasStatements => this.VisitBalanceSummaryStatements.Any() || this.FinancePlanStatements.Any();

        public string NextStatementDate { get; set; }
    }
}
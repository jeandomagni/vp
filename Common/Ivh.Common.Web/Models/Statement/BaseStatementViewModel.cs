﻿namespace Ivh.Common.Web.Models.Statement
{
    using Base.Utilities.Helpers;
    using System.Collections.Generic;
    using System.Linq;
    using Ivh.Application.Core.Common.Dtos;

    public class BaseStatementViewModel
    {
        public BaseStatementViewModel()
        {
            this.ServiceGroupDisclaimers = new List<string>();
        }

        // guarantor
        public int GuarantorRegistrationYear { get; set; }
        public string GuarantorVisitPayUserFirstName { get; set; }
        public string GuarantorVisitPayUserLastName { get; set; }
        public string GuarantorAddressStreet1 { get; set; }
        public string GuarantorAddressStreet2 { get; set; }
        public string GuarantorCityStateZip { get; set; }
        public string GuarantorVisitPayUserLastNameFirstName => FormatHelper.LastNameFirstName(this.GuarantorVisitPayUserLastName,this.GuarantorVisitPayUserFirstName);
        public string GuarantorVisitPayUserFirstNameLastName => FormatHelper.FirstNameLastName(this.GuarantorVisitPayUserFirstName,this.GuarantorVisitPayUserLastName);
        
        // facilities
        public IList<FacilityDto> Facilities { get; set; } = new List<FacilityDto>();

        // statement
        public int DaysInBillingCycle { get; set; }
        public string PaymentDueDate { get; set; }
        public string PeriodEndDate { get; set; }
        public string PeriodStartDate { get; set; }
        public string PriorStatementDate { get; set; }
        public string StatementDate { get; set; }
        public string StatementDateMMddyyy { get; set; }
        public int StatementYear { get; set; }
        public int VpStatementId { get; set; }
        public bool IsArchived { get; set; }
        public bool IsLastStatement { get; set; }
        public int DaysToNextStatement { get; set; }
        public string NextStatementDate { get; set; }
        public IList<string> ServiceGroupDisclaimers { get; set; }
        public bool ShowServiceGroupDisclaimer => this.ServiceGroupDisclaimers.Any();

        //Consolidation
        public string ConsolidatedUserMessageTitle { get; set; }
        public string ConsolidatedUserMessage { get; set; }

        public IList<string> Notifications { get; set; }
    }
}
﻿namespace Ivh.Common.Web.Models.Statement
{
    public class StatementListItemViewModel : IStatementListItemViewModel
    {
        public int? StatementId { get; set; }
        public int? FinancePlanStatementId { get; set; } 
        public string StatementDate { get; set; }
        public string DisplayStatementBalance { get; set; }
        public bool IsArchived { get; set; }
    }
}
﻿namespace Ivh.Common.Web.Models.Statement
{
    using Base.Enums;
    using VisitPay.Enums;

    public class StatementFinancePlanViewModel
    {
        public string StatementedSnapshotFinancePlanAdjustmentsSum { get; set; }
        public string StatementedSnapshotFinancePlanBalance { get; set; }
        public string StatementedSnapshotFinancePlanEffectiveDate { get; set; }
        public string StatementedSnapshotFinancePlanInterestCharged { get; set; }
        public string StatementedSnapshotFinancePlanInterestPaid { get; set; }
        public string StatementedSnapshotFinancePlanInterestRate { get; set; }
        public string StatementedSnapshotFinancePlanMonthlyPaymentAmount { get; set; }
        public string StatementedSnapshotFinancePlanPastDueAmount { get; set; }
        public string StatementedSnapshotFinancePlanPaymentDue { get; set; }
        public string StatementedSnapshotFinancePlanStatus { get; set; }
        public string StatementedSnapshotFinancePlanPrincipalPaid { get; set; }
        public string StatementedSnapshotPriorFinancePlanBalance { get; set; }
        public int StatementedSnapshotFinancePlanStatusId { get; set; }
        public string FinancePlanPayments { get; set; }

        public bool IsPastDue => (FinancePlanStatusEnum)this.StatementedSnapshotFinancePlanStatusId == FinancePlanStatusEnum.PastDue;

        // ??
        public string StatementedSnapshotFinancePlanPayments { get; set; }
    }
}
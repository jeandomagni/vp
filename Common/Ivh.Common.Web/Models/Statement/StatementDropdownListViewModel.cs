﻿namespace Ivh.Common.Web.Models.Statement
{
    using System.Collections.Generic;
    using System.Linq;

    public class StatementDropdownListViewModel
    {
        public IList<IStatementListItemViewModel> Statements { get; set; }
        public string StatementType { get; set; }
        public int DropDownNumber { get;set; }

        public IList<IStatementListItemViewModel> ArchivedStatements
        {
            get { return this.Statements.Where(x => x.IsArchived).ToList(); }
        }

        public IList<IStatementListItemViewModel> NotArchivedStatements
        {
            get { return this.Statements.Where(x => !x.IsArchived).ToList(); }
        }
    }
}

﻿namespace Ivh.Common.Web.Models.Statement
{
    using VisitPay.Enums;

    public class StatementVisitViewModel
    {
        public string Description { get; set; }
        public string Patient { get; set; }
        public string SourceSystemKeyDisplay { get; set; }
        public string VisitDate { get; set; }
        public string FacilityDescription { get; set; }
        public string StatementedSnapshotVisitBalance { get; set; }
        public string StatementedSnapshotVisitState { get; set; }
        public int StatementedSnapshotVisitStateId { get; set; }
        public int StatementedSnapshotAgingCount { get; set; }
        public string StatementedSnapshotDisplayStatus { get; set; }
        public bool IsPastDue => this.StatementedSnapshotAgingCount > 1;
        public bool IsFinalPastDue => this.StatementedSnapshotAgingCount == (int)AgingTierEnum.FinalPastDue;
        public string ServiceGroupIndicator { get; set; }
        public bool ShowServiceGroupIndicator => !string.IsNullOrWhiteSpace(this.ServiceGroupIndicator);
    }
}
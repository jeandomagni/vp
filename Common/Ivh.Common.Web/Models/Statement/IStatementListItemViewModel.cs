﻿namespace Ivh.Common.Web.Models.Statement
{
    public interface IStatementListItemViewModel
    {
        int? StatementId { get; set; }
        int? FinancePlanStatementId { get; set; }
        string StatementDate { get; set; }
        string DisplayStatementBalance { get; set; }
        bool IsArchived { get; set; }
    }
}

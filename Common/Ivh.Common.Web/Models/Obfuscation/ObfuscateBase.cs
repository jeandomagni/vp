﻿using Ivh.Common.Web.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ivh.Common.Web.Models.Obfuscation

        /// <summary>
{
    using System.Web;

    public abstract class ObfuscateBase
    {

        private readonly Obfuscator _obfuscator = new Obfuscator();
        /// Recursively iterate through object graph and obfuscate any property value that has the [ObfuscateMember] attribute
        /// </summary>
        public void Obfuscate(HttpContextBase httpContext)
        {
            this._obfuscator.Obfuscate(this, httpContext);
        }

        /// <summary>
        /// Recursively iterate through object graph and deobfuscate any property value that has the [ObfuscateMember] attribute
        /// </summary>
        public void Deobfuscate(HttpContextBase httpContext)
        {
            this._obfuscator.Deobfuscate(this, httpContext);
        }

    }
}

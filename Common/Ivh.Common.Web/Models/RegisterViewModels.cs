﻿namespace Ivh.Common.Web.Models
{
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using Account;
    using Attributes;
    using VisitPay.Constants;

    public class RegisterViewModel
    {
        public RegisterViewModel()
        {
            this.Account = new RegisterAccountViewModel();
            this.PersonalInformation = new RegisterPersonalInformationViewModel();
            this.Terms = new RegisterTermsViewModel();
        }
        
        public RegisterAccountViewModel Account { get; set; }
        
        public RegisterPersonalInformationViewModel PersonalInformation { get; set; }
        
        public RegisterTermsViewModel Terms { get; set; }
    }
    
    public class RegisterPersonalInformationViewModel
    {
        public RegisterPersonalInformationViewModel()
        {
            
        }

        public RegisterPersonalInformationViewModel(
            RegisterViewModelElementConfig configZip,
            RegisterViewModelElementConfig configSsn,
            RegisterViewModelElementConfig configPatientDateOfBirth
        )
        {
            this.ConfigZip = configZip;
            this.ConfigSsn = configSsn;
            this.ConfigPatientDateOfBirth = configPatientDateOfBirth;

            this.RequireSsn = configSsn?.Required ?? true;
            this.RequireZip = configZip?.Required ?? true;
            this.RequirePatientDateOfBirthDefault = (configPatientDateOfBirth?.Required ?? false);
        }
        
        public bool IsSso { get; set; }
        
        [RequiredIf("IsSso", false)]
        public string[] GuarantorId { get; set; }
        
        [LocalizedDisplayName(TextRegionConstants.NameFirst)]
        [Required]
        public string FirstName { get; set; }
        
        [LocalizedDisplayName(TextRegionConstants.NameLast)]
        [Required]
        public string LastName { get; set; }

        [LocalizedDisplayName(TextRegionConstants.Address)]
        [LocalizedPrompt(TextRegionConstants.AddressStreetPo)]
        [Required]
        public string AddressStreet1 { get; set; }
        
        [LocalizedPrompt(TextRegionConstants.AddressLine2)]
        public string AddressStreet2 { get; set; }
        
        [LocalizedPrompt(TextRegionConstants.AddressCity)]
        [Required]
        public string City { get; set; }

        [Required]
        public string State { get; set; }

        [LocalizedPrompt(TextRegionConstants.AddressZip)]
        [RequiredIf(nameof(RequireZip), true)]
        [RegularExpressionZipCode(TextRegionConstants.InvalidZip)]
        public string Zip { get; set; }

        [LocalizedDisplayName(TextRegionConstants.Address)]
        [LocalizedPrompt(TextRegionConstants.AddressStreetPo)]
        //[Required]
        public string MailingAddressStreet1 { get; set; }

        [LocalizedPrompt(TextRegionConstants.AptSuiteOrBuildingNumber)]
        public string MailingAddressStreet2 { get; set; }
        
        [LocalizedPrompt(TextRegionConstants.AddressCity)]
        //[Required]
        public string MailingCity { get; set; }

        //[Required]
        public string MailingState { get; set; }

        [LocalizedPrompt(TextRegionConstants.AddressZip)]
        //[RequiredIf(nameof(RequireZip), true)]
        //[RegularExpressionZipCode(TextRegionConstants.InvalidZip)]
        public string MailingZip { get; set; }
        
        [LocalizedDisplayName(TextRegionConstants.Ssn4)]
        [RequiredIf(nameof(RequireSsn), true)]
        [RegularExpressionSsn4]
        public string Ssn4 { get; set; }

        [LocalizedPrompt(TextRegionConstants.DobYear)]
        [Required]
        [MinLength(4)]
        [MaxLength(4)]
        public string DateOfBirthYear { get; set; }

        [LocalizedDisplayName(TextRegionConstants.Dob)]
        [LocalizedPrompt(TextRegionConstants.DobMonth)]
        [Required]
        public string DateOfBirthMonth { get; set; }

        [LocalizedPrompt(TextRegionConstants.DobDay)]
        [Required]
        [Range(1, 31)]
        [MaxLength(2)]
        public string DateOfBirthDay { get; set; }

        #region PatientDateOfBirth
        [LocalizedPrompt(TextRegionConstants.DobYear)]
        [RequiredIf(nameof(RequirePatientDateOfBirth), true)]
        [MinLength(4)]
        [MaxLength(4)]
        public string PatientDateOfBirthYear { get; set; }

        [LocalizedDisplayName(TextRegionConstants.PatientDateOfBirth)]
        [LocalizedPrompt(TextRegionConstants.DobMonth)]
        [RequiredIf(nameof(RequirePatientDateOfBirth), true)]
        public string PatientDateOfBirthMonth { get; set; }

        [LocalizedPrompt(TextRegionConstants.DobDay)]
        [RequiredIf(nameof(RequirePatientDateOfBirth), true)]
        [Range(1, 31)]
        [MaxLength(2)]
        public string PatientDateOfBirthDay { get; set; } 
        #endregion
        
        [LocalizedDisplayName(TextRegionConstants.AcknowledgeInformation)]
        [RequireChecked]
        public bool GuarantorAcknowledged { get; set; }

        public bool RequireSsn { get; set; } = true;

        public bool RequireZip { get; set; } = true;
        
        public bool RequirePatientDateOfBirth => this.RequirePatientDateOfBirthDefault  && this.ShowPatientDateOfBirth;
        
        public bool RequirePatientDateOfBirthDefault { get; set; }

        public bool ShowPatientDateOfBirth { get; set; }
        
        public RegisterViewModelElementConfig ConfigZip { get; set; }

        public RegisterViewModelElementConfig ConfigSsn { get; set; }

        public RegisterViewModelElementConfig ConfigPatientDateOfBirth { get; set; }
    }

    public class RegisterViewModelElementConfig
    {
        public bool Required { get; set; } = true;
        public bool Visible { get; set; } = true;
    }

    public class RegisterAccountViewModel
    {
        public RegisterAccountViewModel()
        {
            this.SecurityQuestions = new List<SecurityQuestionViewModel>();
        }
        
        [LocalizedDisplayName(TextRegionConstants.EmailAddress)]
        [Required]
        [RegularExpressionEmailAddress(TextRegionConstants.EmailInvalid)]
        public string Email { get; set; }
        
        [LocalizedDisplayName(TextRegionConstants.Password)]
        [Required]
        public string Password { get; set; }
        
        [LocalizedDisplayName(TextRegionConstants.Username)]
        [Required]
        public string Username { get; set; }

        public IList<SecurityQuestionViewModel> SecurityQuestions { get; set; }
    }
    
    public class RegisterTermsViewModel
    {
        public int EsignTermsCmsVersionId { get; set; }

        public int TermsOfUseCmsVersionId { get; set; }
        
        [LocalizedDisplayName(TextRegionConstants.TermsEsignAgree)]
        [RequireChecked]
        public bool EsignTermsAgreed { get; set; }
        
        [RequireChecked]
        public bool TermsOfUseAgreed { get; set; }
    }
}
﻿namespace Ivh.Common.Web.Models
{
    public abstract class SearchResult
    {
        // lower case because of javascript
        public int page { get; set; }
        public int total { get; set; }
        public int records { get; set; }
    }
}

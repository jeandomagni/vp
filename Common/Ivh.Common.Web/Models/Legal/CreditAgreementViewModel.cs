﻿namespace Ivh.Common.Web.Models.Legal
{
    public class CreditAgreementViewModel
    {
        public string Text { get; set; }
        public string Title { get; set; }
        public bool IsExport { get; set; }
        public int FinancePlanOfferId { get; set; }
    }
}
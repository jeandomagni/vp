﻿namespace Ivh.Common.Web.Models.Shared
{
    public class TermsOfUseViewModel : TextViewModel
    {
        public string UpdatedDate { get; set; }
    }
}
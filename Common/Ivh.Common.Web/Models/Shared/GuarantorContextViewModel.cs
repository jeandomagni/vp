﻿using Ivh.Common.Base.Utilities.Helpers;

namespace Ivh.Common.Web.Models.Shared
{
    public class GuarantorContextViewModel
    {
        public int VpGuarantorId { get; set; }
        public string UserFirstName { get; set; }
        public string UserMiddleName { get; set; }
        public string UserLastName { get; set; }
        public string Email { get; set; }

        public string UserLastNameFirstName => FormatHelper.LastNameFirstName(this.UserLastName,this.UserFirstName);
        public string UserFirstNameLastName => FormatHelper.FirstNameLastName(this.UserFirstName,this.UserLastName);
        public string UserFullNameDisplay => FormatHelper.FullNameMiddleInitial(this.UserFirstName, this.UserMiddleName, this.UserLastName);
    }
}
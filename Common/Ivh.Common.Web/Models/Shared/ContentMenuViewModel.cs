﻿using System.Collections.Generic;

namespace Ivh.Common.Web.Models.Shared
{
    public class ContentMenuViewModel
    {
        public ContentMenuViewModel()
        {
            this.ContentMenuItems = new List<ContentMenuItemViewModel>();
        }

        public IList<ContentMenuItemViewModel> ContentMenuItems { get; set; }
    }
}
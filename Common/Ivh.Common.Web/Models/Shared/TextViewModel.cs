﻿namespace Ivh.Common.Web.Models.Shared
{
    public class TextViewModel
    {
        public string Text { get; set; }
        public bool IsExport { get; set; }
        public int? Version { get; set; }
    }
}
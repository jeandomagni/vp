﻿using Ivh.Common.Base.Utilities.Helpers;

namespace Ivh.Common.Web.Models.Shared
{
    using System;
    using Base.Utilities.Extensions;

    public class CurrentUserViewModel
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string UserName { get; set; }
        public string LastLoginDate { get; set; }
        public string FullNameDisplay => this.FirstNameLastName;
        public string LastNameFirstName => FormatHelper.LastNameFirstName(this.LastName, this.FirstName);
        public string FirstNameLastName => FormatHelper.FirstNameLastName(this.FirstName, this.LastName);

        public bool IsFirstTimeLogin =>(this.InsertDate.IsNotNullOrEmpty()) && DateTime.Parse(this.InsertDate).Date == DateTime.UtcNow.Date;
        public string InsertDate { get; set; }
    }
}
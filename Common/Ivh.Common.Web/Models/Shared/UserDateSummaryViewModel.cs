namespace Ivh.Common.Web.Models.Shared
{
    using System;

    public class UserDateSummaryViewModel
    {
        public UserDateSummaryViewModel(bool isGracePeriod, bool isAwaitingStatement, DateTime nextStatementDate, DateTime? paymentDueDate)
        {
            this.IsGracePeriod = isGracePeriod;
            this.IsAwaitingStatement = isAwaitingStatement;
            this.NextStatementDate = nextStatementDate;
            this.PaymentDueDate = paymentDueDate;
        }

        public string NextDateText => (this.PaymentDueDate.HasValue) ? $"Your payment due date is <span>{this.PaymentDueDate.Value:MMMMM d, yyyy}</span>" : string.Empty;

        public string NextDateTextShort => this.PaymentDueDate.HasValue ? $"{this.PaymentDueDate.Value:MMM dd}" : string.Empty;

        public bool IsAwaitingStatement { get; }

        public bool IsGracePeriod { get; }

        public DateTime NextStatementDate { get; }

        public DateTime? PaymentDueDate { get; }
    }
}
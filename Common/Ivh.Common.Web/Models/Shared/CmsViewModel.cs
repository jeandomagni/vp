﻿namespace Ivh.Common.Web.Models.Shared
{
    public class CmsViewModel
    {
        public string ContentTitle { get; set; }
        public string ContentBody { get; set; }
    }
}
﻿namespace Ivh.Common.Web.Models.Shared
{
    public class ActiveEmulationsEnum
    {
        public const string Key = "VisitPayActiveEmulations";
        public const string Lock = "VisitPayActiveEmulationsLock";
    }
}
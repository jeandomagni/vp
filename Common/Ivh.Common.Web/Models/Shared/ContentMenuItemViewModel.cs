﻿namespace Ivh.Common.Web.Models.Shared
{
    public class ContentMenuItemViewModel
    {
        public string LinkText { get; set; }
        public string LinkUrl { get; set; }
        public string IconCssClass { get; set; }
        public string Target => this.LinkUrlOpensNewWindow ? "_blank" : "";
        public bool LinkUrlOpensNewWindow { get; set; }
    }
}
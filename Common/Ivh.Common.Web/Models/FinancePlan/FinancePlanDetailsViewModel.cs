﻿namespace Ivh.Common.Web.Models.FinancePlan
{
    using System.Collections.Generic;
    using Account;
    using Ivh.Common.Base.Utilities.Extensions;
    using VisitPay.Enums;

    public class FinancePlanDetailsViewModel
    {
        public FinancePlanDetailsViewModel()
        {
            this.ChangeHistory = new List<FinancePlanInterestRateHistoryViewModel>();
            this.Visits = new List<VisitsSearchResultViewModel>();
        }
        
        public string CurrentFinancePlanBalance { get; set; }
        public string CurrentInterestRate { get; set; }
        public FinancePlanStatusEnum FinancePlanStatus { get; set; }
        public int? FinancePlanId { get; set; }
        public int FinancePlanOfferId { get; set; }
        public string InterestAssessed { get; set; }
        public string InterestPaidToDate { get; set; }
        public bool IsManaged { get; set; }
        public string LastPaymentDate { get; set; }
        public int MonthsRemaining { get; set; }
        public string OriginationDate { get; set; }
        public string OriginatedFinancePlanBalance { get; set; }
        public string PaymentAmount { get; set; }
        public string AmountDue { get; set; }
        public string PastDueAmount { get; set; }
        public string AdjustmentsSinceOrigination { get; set; }
        public bool HasAdjustments { get; set; }
        public string PrincipalPaidToDate { get; set; }
        public string StatusDisplay { get; set; }
        public string Terms { get; set; }
        public bool IsCanceled { get; set; }
        public bool IsPending { get; set; }
        public bool IsOriginated => this.FinancePlanStatus.IsInCategory(FinancePlanStatusEnumCategory.Active);
        public bool IsPastDue { get; set; }
        public bool IsRetailInstallmentContract { get; set; }
        public IList<VisitsSearchResultViewModel> Visits { get; set; }
        public IList<FinancePlanInterestRateHistoryViewModel> ChangeHistory { get; set; }
    }
}
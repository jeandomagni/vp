﻿namespace Ivh.Common.Web.Models.FinancePlan
{
    public class FinancePlanTermsDisplayViewModel
    {
        public string FinalPaymentDate { get; set; }
        public string InterestRate { get; set; }
        public string MonthlyPaymentAmount { get; set; }
        public decimal MonthlyPaymentAmountDecimal { get; set; }
        public string MonthlyPaymentAmountLast { get; set; }
        public string NextPaymentDueDate { get; set; }
        public int NumberMonthlyPayments { get; set; }
        public string TotalInterest { get; set; }
        public string MonthlyPaymentAmountOtherPlans { get; set; }
        public string MonthlyPaymentAmountTotal { get; set; }
        public int NumberMonthlyPaymentsEqual => this.MonthlyPaymentAmount != this.MonthlyPaymentAmountLast ? this.NumberMonthlyPayments - 1 : this.NumberMonthlyPayments;
    }
}
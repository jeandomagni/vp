﻿namespace Ivh.Common.Web.Models.FinancePlan
{
    using Ivh.Application.FinanceManagement.Common.Dtos;

    public class ReconfigurationActionResponseViewModel
    {
        public ReconfigurationActionResponseViewModel(ReconfigurationActionResponseDto responseDto, string redirectUrl = null)
        {
            this.IsError = responseDto.IsError;
            if (!this.IsError)
            {
                this.RedirectUrl = redirectUrl;
            }
        } 
        
        public bool IsError { get; }

        public string RedirectUrl { get; }
    }
}
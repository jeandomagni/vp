﻿namespace Ivh.Common.Web.Models.FinancePlan
{
    public class InterestRateViewModel
    {
        public string DisplayText { get; set; }
        public int InterestFreePeriod { get; set; }
        public string Rate { get; set; }
        public bool IsCharity { get; set; }
    }
}
﻿namespace Ivh.Common.Web.Models.FinancePlan
{
    using System.Collections.Generic;
    using VisitPay.Enums;

    public class ConfirmFinancePlanViewModel
    {
        public OfferCalculationStrategyEnum OfferCalculationStrategy { get; set; }
        public int? FinancePlanId { get; set; }
        public int FinancePlanOfferId { get; set; }
        public int? FinancePlanOfferSetTypeId { get; set; }
        public decimal MonthlyPaymentAmount { get; set; }
        public int StatementId { get; set; }
        public int TermsCmsVersionId { get; set; }
        public int? EsignCmsVersionId { get; set; }
        public bool CombineFinancePlans { get; set; }
        public int? PaymentDueDay { get; set; }
        public IList<int> SelectedVisits { get; set; }
        public int? FinancePlanTypeId { get; set; }
        public string FinancePlanOptionStateCode { get; set; }
    }
}

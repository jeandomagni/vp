﻿namespace Ivh.Common.Web.Models.FinancePlan
{
    using System.Collections.Generic;

    public class FinancePlansSearchResultsViewModel : SearchResult
    {
        public IList<FinancePlansSearchResultViewModel> FinancePlans { get; set; }
        public string TotalCurrentBalance { get; set; }
    }
}
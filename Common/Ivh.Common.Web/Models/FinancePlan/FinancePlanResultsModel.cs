﻿namespace Ivh.Common.Web.Models.FinancePlan
{
    public class FinancePlanResultsModel : ResultMessage
    {
        public FinancePlanResultsModel(bool result, string message) : base(result, message)
        {
        }

        public bool IsCombined { get; set; }
        public bool StackedAndCombinedFinancePlanOptionsPresented { get; set; }
        public int? FinancePlanId { get; set; }
        public int? PaymentId { get; set; }
        public int VpGuarantorId { get; set; }
    }
}
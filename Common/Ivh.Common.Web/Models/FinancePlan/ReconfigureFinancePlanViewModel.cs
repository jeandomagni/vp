﻿namespace Ivh.Common.Web.Models.FinancePlan
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Web.Mvc;
    using Base.Utilities.Helpers;
    using Payment;

    public class ReconfigureFinancePlanViewModel
    {
        public ReconfigureFinancePlanViewModel()
        {
            this.InterestRates = new List<InterestRateViewModel>();
            this.FinancePlanOfferSetTypes = new List<SelectListItem>();
        }

        public int FinancePlanIdToReconfigure { get; set; }

        public int VpGuarantorId { get; set; }
        
        public string GuarantorFirstName { get; set; }

        public string GuarantorLastName { get; set; }

        public bool IsEditable { get; set; } = true;

        public int MaximumMonthlyPayments { get; set; }

        public decimal MinimumPaymentAmount { get; set; }

        public string CurrentFinancePlanPaymentAmount { get; set; }

        public string CurrentFinancePlanInterestRate { get; set; }

        public string CurrentFinancePlanOriginalBalance { get; set; }

        public string CurrentFinancePlanBalance { get; set; }

        public decimal CurrentFinancePlanBalanceDecimal { get; set; }

        public string CurrentFinancePlanNewCharges { get; set; }

        public string NewFinancePlanBalance { get; set; }

        public int FinancePlanOfferSetTypeId { get; set; }

        public IList<SelectListItem> FinancePlanOfferSetTypes { get; set; }

        public List<InterestRateViewModel> InterestRates { get; set; }

        public FinancePlanTermsViewModel Terms { get; set; }

        public bool HasCharityOffer
        {
            get { return this.InterestRates.Any(x => x.IsCharity); }
        }

        public string CharityOfferRate
        {
            get { return this.InterestRates.Any(x => x.IsCharity) ? this.InterestRates.First(x => x.IsCharity).Rate : string.Empty; }
        }

        public string ReconfigureAcceptUrl { get; set; }
        
        public string ReconfigureCancelUrl { get; set; }

        public string ReconfigureModelUrl { get; set; }

        public string ReconfigureSubmitUrl { get; set; }

        public string ReconfigureValidateUrl { get; set; }

        public int? TermsCmsVersionId { get; set; }

        public string ErrorMessage { get;set; }

        public PaymentMethodListItemViewModel PrimaryPaymentMethod { get; set; }

        public string GuarantorLastNameFirstName => FormatHelper.LastNameFirstName(this.GuarantorLastName, this.GuarantorFirstName);

        public string GuarantorFirstNameLastName => FormatHelper.FirstNameLastName(this.GuarantorFirstName, this.GuarantorLastName);

        public bool HasErrorMessage => !string.IsNullOrWhiteSpace(this.ErrorMessage);
    }
}
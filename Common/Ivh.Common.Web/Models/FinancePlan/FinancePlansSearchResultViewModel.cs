﻿namespace Ivh.Common.Web.Models.FinancePlan
{
    using System.Collections.Generic;
    using Ivh.Application.FinanceManagement.Common.Dtos;

    public class FinancePlansSearchResultViewModel
    {
        public FinancePlansSearchResultViewModel()
        {
            this.VisitIds = new List<int>();
        }
        
        public string CurrentFinancePlanBalance { get; set; }
        public decimal CurrentFinancePlanBalanceDecimal { get; set; }
        public int FinancePlanId { get; set; }
        public string FinancePlanStatusFullDisplayName { get; set; }
        public string InterestAssessed { get; set; }
        public string InterestPaidToDate { get; set; }
        public string InterestRate { get; set; }
        public string OriginatedFinancePlanBalance { get; set; }
        public string OriginationDate { get; set; }
        public string PaymentAmount { get; set; }
        public string PrincipalPaidToDate { get; set; }
        public string Terms { get; set; }
        public IList<int> VisitIds { get; set; }
        public FinancePlanStatusDto FinancePlanStatus { get; set; }
        public bool IsCanceled { get; set; }
        public bool IsPending { get; set; }
        public bool IsPastDue { get; set; }
        public bool IsActiveUncollectable { get; set; }
        public bool IsUncollectable { get; set; }
        public bool IsManaged { get; set; }
        public string PastDueAmount { get; set; }
        public string AmountDue { get; set; }
        public decimal AmountDueDecimal { get; set; }
        public bool HasAmountDue { get; set; }
    }
}
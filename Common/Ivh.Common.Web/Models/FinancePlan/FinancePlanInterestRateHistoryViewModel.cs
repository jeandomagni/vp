﻿namespace Ivh.Common.Web.Models.FinancePlan
{
    public class FinancePlanInterestRateHistoryViewModel
    {
        public string InsertDate { get; set; }
        public string InterestRate { get; set; }
        public string PaymentAmount { get; set; }
        public string ChangeDescription { get; set; }
    }
}

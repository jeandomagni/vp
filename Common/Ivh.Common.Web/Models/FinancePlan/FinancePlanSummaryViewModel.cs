﻿namespace Ivh.Common.Web.Models.FinancePlan
{
    using System.Collections.Generic;
    using System.Linq;
    using Payment;

    public class FinancePlanSummaryViewModel
    {
        public bool HasAmountDue { get; set; }

        public bool HasFinancePlans => this.FinancePlanDetailsViewModels.Any();
        
        public decimal FinancePlansBalance { get; set; }

        public decimal AmountDueSum { get; set; }

        public int MonthsRemaining { get; set; }

        public string StatusDisplay { get; set; }

        public string PaymentDueDate { get; set; }

        public bool CanUserTakeAction { get; set; }

        public PaymentMethodDisplayViewModel PaymentMethod { get; set; }

        public List<FinancePlanDetailsViewModel> FinancePlanDetailsViewModels { get; set; } = new List<FinancePlanDetailsViewModel>();
    }
}
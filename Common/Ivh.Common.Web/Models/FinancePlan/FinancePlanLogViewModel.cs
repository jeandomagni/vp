﻿namespace Ivh.Common.Web.Models.FinancePlan
{
    using Base.Enums;

    public class FinancePlanLogViewModel
    {
        public string InsertDate { get; set; }
        public int VisitId { get; set; }
        public string Amount { get; set; } //FinancePlanVisitPrincipalAmount.Amount or FinancePlanVisitInterestDue.InterestDue
        public string VisitBalanceAtTimeOfAssessement { get; set; }
        public string InterestRateUsedForAssessement { get; set; }
        public string FinancePlanLogType { get; set; }
        public string BillingApplication { get; set; }
        public string SourceSystemKey { get; set; }

    }
}
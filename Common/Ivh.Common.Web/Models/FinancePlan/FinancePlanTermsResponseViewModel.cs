﻿namespace Ivh.Common.Web.Models.FinancePlan
{
    public class FinancePlanTermsResponseViewModel
    {
        public FinancePlanTermsResponseViewModel(FinancePlanTermsViewModel terms, decimal? minimumPaymentAmount, int? maximumMonthlyPayments)
        {
            this.IsError = false;
            this.Terms = terms;
            this.MinimumPaymentAmount = minimumPaymentAmount;
            this.MaximumMonthlyPayments = maximumMonthlyPayments;
        }

        public FinancePlanTermsResponseViewModel(string errorMessage, decimal? minimumPaymentAmount, int? maximumMonthlyPayments)
        {
            this.IsError = true;
            this.ErrorMessage = errorMessage;
            this.MinimumPaymentAmount = minimumPaymentAmount;
            this.MaximumMonthlyPayments = maximumMonthlyPayments;
        }

        public FinancePlanTermsViewModel Terms { get; }

        public bool IsError { get; }

        public string ErrorMessage { get; }

        public decimal? MinimumPaymentAmount { get; }

        public int? MaximumMonthlyPayments { get; }
    }
}
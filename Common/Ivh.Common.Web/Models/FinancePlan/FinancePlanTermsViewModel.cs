﻿namespace Ivh.Common.Web.Models.FinancePlan
{
    public class FinancePlanTermsViewModel
    {
        public string EffectiveDate { get; set; }
        
        public int? FinancePlanOfferId { get; set; }

        public int? FinancePlanOfferSetTypeId { get; set; }

        public string FinalPaymentDate { get; set; }

        public int GuarantorPaymentDueDay { get; set; }
        
        public decimal? InterestRate { get; set; }

        public decimal? MonthlyPaymentAmount { get; set; }

        public decimal? MonthlyPaymentAmountLast { get; set; }

        public decimal? MonthlyPaymentAmountOtherPlans { get; set; }

        public decimal? MonthlyPaymentAmountPrevious { get; set; }

        public decimal? MonthlyPaymentAmountTotal { get; set; }

        public string NextPaymentDueDate { get; set; }

        public int? NumberMonthlyPayments { get; set; }

        public string PaymentDueDayOrdinal { get; set; }

        public bool RequireCreditAgreement { get; set; }

        public decimal? TotalInterest { get; set; }
        
        public string TermsAgreementTitle { get; set; }

        public int TermsCmsVersionId { get; set; }

        public bool IsRetailInstallmentContract { get; set; }

        public bool HasInterest => this.InterestRate.GetValueOrDefault(0) > 0 || this.TotalInterest.GetValueOrDefault(0) > 0;

        public bool LastPaymentAmountIsDifferentThanMonthlyAmount => (this.MonthlyPaymentAmount.GetValueOrDefault(0) != this.MonthlyPaymentAmountLast.GetValueOrDefault(0));

        public int NumberOfEqualPayments => (this.LastPaymentAmountIsDifferentThanMonthlyAmount)
            ? this.NumberMonthlyPayments.GetValueOrDefault(0) - 1 
            : this.NumberMonthlyPayments.GetValueOrDefault(0);
    }
}
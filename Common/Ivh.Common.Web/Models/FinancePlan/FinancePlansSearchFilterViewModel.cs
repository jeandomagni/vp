﻿namespace Ivh.Common.Web.Models.FinancePlan
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.Web.Mvc;
    using Attributes;
    using Base.Constants;
    using VisitPay.Constants;

    public class FinancePlansSearchFilterViewModel
    {
        public FinancePlansSearchFilterViewModel()
        {
            this.FinancePlanStatuses = new List<SelectListItem>();
        }

        [LocalizedDisplayName(TextRegionConstants.Status)]
        public IList<int> FinancePlanStatusIds { get; set; }

        [LocalizedDisplayName(TextRegionConstants.FinancePlansInTheLast)]
        public string LastXRange { get; set; }

        [LocalizedPrompt(TextRegionConstants.StartDate)]
        [DateRangeCompare(false, "*.OriginationDateRangeFrom", "*.OriginationDateRangeTo", DateRangeCompareType.LessThanOrEqual, ErrorMessage = "Invalid Date Range")]
        [DataType(DataType.Date)]
        public DateTime? OriginationDateRangeFrom { get; set; }

        [LocalizedPrompt(TextRegionConstants.EndDate)]
        [DateRangeCompare(false, "*.OriginationDateRangeTo", "*.OriginationDateRangeFrom", DateRangeCompareType.GreaterThanOrEqual, ErrorMessage = "Invalid Date Range")]
        [DataType(DataType.Date)]
        public DateTime? OriginationDateRangeTo { get; set; }

        public IList<SelectListItem> LastXRanges
        {
            get
            {
                return new List<SelectListItem>
                {
                    new SelectListItem {Text = "90 Days", Value = "90d"},
                    new SelectListItem {Text = "6 Months", Value = "6m"},
                    new SelectListItem {Text = "1 Year", Value = "1y"},
                    new SelectListItem {Text = "All", Value = ""}
                };
            }
        }

        public IList<SelectListItem> FinancePlanStatuses { get; set; }

        public bool AllowFinancePlanSearch { get; set; }
    }
}
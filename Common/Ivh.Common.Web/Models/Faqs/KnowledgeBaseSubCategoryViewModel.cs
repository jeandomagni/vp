﻿namespace Ivh.Common.Web.Models.Faq
{
    using System.Collections.Generic;

    public class KnowledgeBaseSubCategoryViewModel
    {
        public string Name { get; set; }

        public int DisplayOrder { get; set; }

        public IList<KnowledgeBaseQuestionAnswerViewModel> QuestionAnswers { get; set; }
    }
}
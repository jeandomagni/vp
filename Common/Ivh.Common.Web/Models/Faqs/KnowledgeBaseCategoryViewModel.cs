namespace Ivh.Common.Web.Models.Faq
{
    using System.Collections.Generic;
    using System.Linq;
    using VisitPay.Enums;

    public class KnowledgeBaseCategoryViewModel 
    {
        public string Name { get; set; }

        public int DisplayOrder { get; set; }

        public CmsRegionEnum CmsRegion { get; set; }

        public List<KnowledgeBaseSubCategoryViewModel> KnowledgeBaseSubCategories { get; set; }

        public List<KnowledgeBaseSubCategoryViewModel> KnowledgeBaseSubCategoriesOrdered => this.KnowledgeBaseSubCategories.OrderBy(x => x.DisplayOrder).ToList();
    }
}
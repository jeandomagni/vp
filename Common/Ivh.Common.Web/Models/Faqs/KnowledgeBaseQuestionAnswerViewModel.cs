namespace Ivh.Common.Web.Models.Faq
{
    using Common.Base.Enums;

    public class KnowledgeBaseQuestionAnswerViewModel 
    {
        public string AnswerContentBody { get; set; }
        public string QuestionContentBody { get; set; }
    }
}
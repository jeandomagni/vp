namespace Ivh.Common.Web.Models.Alerts
{
    using System.Collections.Generic;

    public class AlertsViewModel
    {
        public int Count { get; set; }

        public IEnumerable<AlertViewModel> Alerts { get; set; }
    }
}
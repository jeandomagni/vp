﻿namespace Ivh.Common.Web.Models.Alerts
{
    using Base.Enums;
    using VisitPay.Enums;

    public class AlertViewModel
    {
        public int AlertId { get; set; }
        public string ContentBody { get; set; }
        public string ContentTitle { get; set; }

        public AlertTypeEnum AlertType { get; set; }

        public string CssClass
        {
            get
            {
                switch (this.AlertType)
                {
                    case AlertTypeEnum.ActionRequired:
                        return "action-required";
                    case AlertTypeEnum.Information:
                        return "information";
                    case AlertTypeEnum.Welcome:
                        return "welcome-alert";
                    default:
                        return string.Empty;
                }
            }
        }
        public bool IsDismissable { get; set; }
    }
}
﻿namespace Ivh.Common.Web.Models.Monitoring
{
    using System.ComponentModel.DataAnnotations;

    public class SystemHealthViewModel : SystemInfoViewModel
    {
        [Display(Name = "Release Build")]
        public bool ReleaseBuild { get; set; }

        [Display(Name = "Rabbit MQ")]
        public bool Queue { get; set; }

        [Display(Name = "Redis Cache (local)")]
        public bool MemoryCache { get; set; }

        [Display(Name = "Redis Cache (distributed)")]
        public bool DistributedCache { get; set; }

        [Display(Name = "Application Database")]
        public bool ApplicationConnect { get; set; }

        [Display(Name = "GuestPay Database")]
        public bool GuestPayConnect { get; set; }

        [Display(Name = "Logging Database")]
        public bool LogConnect { get; set; }

        [Display(Name = "Storage Database")]
        public bool StorageConnect { get; set; }

        [Display(Name = "Quartz Database")]
        public bool QuartzConnect { get; set; }

        [Display(Name = "CDI ETL Database")]
        public bool CdiEtlConnect { get; set; }

        [Display(Name = "Settings")]
        public bool Settings { get; set; }

        [Display(Name = "Content")]
        public bool Content { get; set; }

        [Display(Name = "Universal Processor")]
        public bool UniversalProcessor { get; set; }

        [Display(Name = "Scoring Processor")]
        public bool ScoringProcessor { get; set; }

        [Display(Name = "Client Reports Server")]
        public bool ClientReportsServer { get; set; }

        [Display(Name = "GuestPay API")]
        public bool GuestPayApi { get; set; }

        [Display(Name = "SecurePan API")]
        public bool SecurePanApi { get; set; }

        [Display(Name = "Payment API")]
        public bool PaymentApi { get; set; }
    }
}

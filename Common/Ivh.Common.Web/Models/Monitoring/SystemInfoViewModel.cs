﻿namespace Ivh.Common.Web.Models.Monitoring
{
    using System.ComponentModel.DataAnnotations;

    public class SystemInfoViewModel
    {
        [Display(Name = "Client")]
        public string Client { get; set; }

        [Display(Name = "Environment")]
        public string Environment { get; set; }

        [Display(Name = "Application")]
        public string Application { get; set; }
        
        [Display(Name = "Application Version")]
        public string AppVersion { get; set; }

        [Display(Name = "ClientData API Version")]
        public string ClientDataApiVersion { get; set; }

        [Display(Name = "SecurePan API Version")]
        public string SecurePanApiVersion { get; set; }

        [Display(Name = "Client Settings Hash")]
        public string ClientSettingsHash { get; set; }

        [Display(Name = "Raw")]
        public string Raw { get; set; }
    }
}

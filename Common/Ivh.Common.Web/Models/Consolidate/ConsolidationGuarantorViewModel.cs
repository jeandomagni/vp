﻿namespace Ivh.Common.Web.Models.Consolidate
{
    using System;
    using System.Collections.Generic;
    using System.Web.Mvc;
    using Ivh.Application.Core.Common.Dtos;
    using Base.Constants;
    using Base.Enums;
    using VisitPay.Constants;
    using VisitPay.Enums;

    public class ConsolidationGuarantorViewModel
    {
        private string _cancelledByUserName;
        private readonly string _clientShortName;

        public ConsolidationGuarantorViewModel(ClientDto clientDto)
        {
            this._clientShortName = clientDto.ClientShortName;
        }

        public Guid ConsolidationGuarantorId { get; set; }

        public string ManagedGuarantorName { get; set; }

        public int ManagedGuarantorId { get; set; }

        public string ManagingGuarantorName { get; set; }

        public int ManagingGuarantorId { get; set; }

        public string ConsolidationGuarantorStatus { get; set; }

        public ConsolidationGuarantorStatusEnum ConsolidationGuarantorStatusEnum { get; set; }

        public string DateAcceptedByManagedGuarantor { get; set; }

        public string DateAcceptedByManagingGuarantor { get; set; }

        public string DateAcceptedFinancePlanTermsByManagingGuarantor { get; set; }

        public int? CancelledByUserId { get; set; }

        public string CancelledByUserName
        {
            get
            {
                if (string.IsNullOrEmpty(this._cancelledByUserName))
                    return "";

                if (this.CancelledByUserId == SystemUsers.SystemUserId)
                    return string.Format("{0} {1}", this._clientShortName, this._cancelledByUserName);

                return this._cancelledByUserName;
            }
            set { this._cancelledByUserName = value; }
        }

        public string CancelledOn { get; set; }

        public bool IsActive { get; set; }

        public bool IsInactive { get; set; }

        public bool CanCancelConsolidation { get; set; }

        public bool CanCancelPending { get; set; }

        public bool CanAcceptFinancePlan { get; set; }

        public bool CanAccept { get; set; }

        public bool CanDecline { get; set; }

        public bool InitiatedByCurrentUser { get; set; }

        public bool IsManagingUser { get; set; }

        public bool HasActiveFinancePlans { get; set; }

        public List<SelectListItem> ActionOptions
        {
            get
            {
                List<SelectListItem> items = new List<SelectListItem>();

                if (this.CanCancelPending)
                    items.Add(new SelectListItem {Text = "Cancel Request", Value = "1"});

                if (this.CanAcceptFinancePlan)
                {
                    string text = this.InitiatedByCurrentUser ? "Review Finance Plan Terms" : "Review Request";
                    items.Add(new SelectListItem {Text = text, Value = "4"});
                }

                if (this.CanAccept)
                    items.Add(new SelectListItem {Text = "Review Request", Value = "2 "});

                if (this.CanDecline)
                    items.Add(new SelectListItem {Text = "Decline Request", Value = "3 "});

                if (this.CanCancelConsolidation)
                {
                    if (this.HasActiveFinancePlans && !this.IsManagingUser)
                        items.Add(new SelectListItem {Text = "Cancel Consolidation", Value = "6"});
                    else
                        items.Add(new SelectListItem {Text = "Cancel Consolidation", Value = "5"});
                }

                return items;
            }
        }
    }
}

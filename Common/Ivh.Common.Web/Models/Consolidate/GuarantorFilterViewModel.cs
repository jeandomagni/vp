﻿namespace Ivh.Common.Web.Models.Consolidate
{
    using System.Collections.Generic;
    using System.Web.Mvc;

    public class GuarantorFilterViewModel
    {
        public GuarantorFilterViewModel()
        {
            this.Items = new List<SelectListItem>();
        }

        public string Title { get; set; }

        public decimal? Balance { get; set; }

        public IList<SelectListItem> Items { get; set; }
    }
}
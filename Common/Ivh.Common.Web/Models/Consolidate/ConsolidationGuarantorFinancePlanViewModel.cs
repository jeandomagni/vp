﻿namespace Ivh.Common.Web.Models.Consolidate
{
    public class ConsolidationGuarantorFinancePlanViewModel
    {
        public int FinancePlanOfferId { get; set; }
        public string CurrentFinancePlanBalance { get; set; }

        public string CurrentInterestRate { get; set; }

        public string FinalPaymentDate { get; set; }

        public string MonthlyPaymentAmount { get; set; }

        public string OriginatedFinancePlanBalance { get; set; }

        public int CreditAgreementVersionNum { get; set; }

        public int CreditAgreementCmsVersionId { get; set; }
    }
}
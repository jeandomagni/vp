﻿namespace Ivh.Common.Web.Models.FailPayment
{
    public class FailPaymentViewModel
    {
        public bool FailPaymentProviderIsActive { get; set; }
    }
}
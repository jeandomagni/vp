﻿namespace Ivh.Common.Web.Models.Communication
{
    using VisitPay.Enums;

    public class VisitPayUserCommunicationPreferenceViewModel
    {
        public int VisitPayUserCommunicationPreferenceId { get; set; }
        public int VisitPayUserId { get; set; }
        public int CommunicationGroupId { get; set; }
        public int CommunicationMethodId { get; set; }
    }
}

﻿namespace Ivh.Common.Web.Models.Itemization
{
    using System.Collections.Generic;

    public class ItemizationSearchResultsViewModel : GridResultsModel<ItemizationSearchResultViewModel>
    {
        public ItemizationSearchResultsViewModel(int page, int rows, int records)
            : base(page, rows, records)
        {
        }
    }
}
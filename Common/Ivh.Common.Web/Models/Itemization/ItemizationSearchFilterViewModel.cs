﻿namespace Ivh.Common.Web.Models.Itemization
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.Web.Mvc;
    using Attributes;
    using Base.Constants;
    using VisitPay.Constants;

    public class ItemizationSearchFilterViewModel
    {
        public ItemizationSearchFilterViewModel()
        {
            this.DateRanges = new List<SelectListItem>
            {
                new SelectListItem {Text = "30 Days", Value = "30"},
                new SelectListItem {Text = "60 Days", Value = "60"},
                new SelectListItem {Text = "90 Days", Value = "90"},
                new SelectListItem {Text = "All", Value = "", Selected = true}
            };
            this.Facilities = new List<SelectListItem>();
            this.Patients = new List<SelectListItem>();
        }

        [LocalizedDisplayName(TextRegionConstants.ItemizationsInTheLast)]
        public int? DateRange { get; set; }

        [LocalizedPrompt(TextRegionConstants.StartDate)]
        [DateRangeCompare(false, "DateRangeFrom", "DateRangeTo", DateRangeCompareType.LessThanOrEqual, ErrorMessage = "Invalid Date Range")]
        [DataType(DataType.Date)]
        public DateTime? DateRangeFrom { get; set; }

        [LocalizedPrompt(TextRegionConstants.EndDate)]
        [DateRangeCompare(false, "DateRangeTo", "DateRangeFrom", DateRangeCompareType.GreaterThanOrEqual, ErrorMessage = "Invalid Date Range")]
        [DataType(DataType.Date)]
        public DateTime? DateRangeTo { get; set; }

        [LocalizedDisplayName(TextRegionConstants.Facility)]
        public string FacilityCode { get; set; }

        [LocalizedDisplayName(TextRegionConstants.Patient)]
        public string PatientName { get; set; }

        public int VisitPayUserId { get; set; }

        public IList<SelectListItem> DateRanges { get; set; }

        public IList<SelectListItem> Facilities { get; set; }

        public IList<SelectListItem> Patients { get; set; }
    }
}
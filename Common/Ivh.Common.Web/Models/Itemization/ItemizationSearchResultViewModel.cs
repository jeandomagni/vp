﻿namespace Ivh.Common.Web.Models.Itemization
{
    using System;

    public class ItemizationSearchResultViewModel
    {
        public string DischargeDate { get; set; }
        public string AccountNumber { get; set; }
        public string Empi { get; set; }
        public string MatchedEmpi { get; set; }
        public string MatchedAccountNumber { get; set; }
        public string PatientName { get; set; }
        public string FacilityCode { get; set; }
        public Guid VisitFileStoredId { get; set; }
        public bool IsRemoved { get; set; }
        public string VisitSourceSystemKeyDisplay { get; set; }
        public string MatchedVisitSourceSystemKeyDisplay { get; set; }
    }
}
﻿namespace Ivh.Common.Web.Models.Sso
{
    using System.ComponentModel.DataAnnotations;
    using Base.Enums;
    using Shared;
    using System.Web.Mvc;
    using Attributes;
    using Base.Constants;
    using VisitPay.Constants;
    using VisitPay.Enums;

    public class SsoProviderWithUserSettingsViewModel
    {
        public SsoProviderWithUserSettingsViewModel()
        {
            this.Body = new CmsViewModel();
            this.Decline = new CmsViewModel();
        }

        public string ActionVisitPayUser { get; set; }

        public string ActionDateTime { get; set; }

        public bool IsAccepted { get; set; }

        public bool IsDeclined { get; set; }
        
        [LocalizedDisplayName(TextRegionConstants.DoNotOfferSso)]
        public bool IsIgnored { get; set; }

        public int SsoProviderId { get; set; }
        public SsoProviderEnum SsoProviderEnum { get; set; }

        public string SsoProviderActionUrl { get; set; }

        public string SsoProviderName { get; set; }

        public CmsViewModel Body { get; set; }

        public CmsViewModel Decline { get; set; }

        public string SourceSystemKeyLabel { get; set; }

        public bool EnableFromManageSso { get; set; }

        public string ShortDescription { get; set; }
    }
}
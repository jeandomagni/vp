﻿namespace Ivh.Common.Web.Models.Sso
{
    public class SamlResponseViewModel
    {
        public string Url { get; set; }
        public string Value { get; set; }
        public string RelayState { get; set; }
        public bool IsError { get; set; }
        public string ErrorMessage { get; set; }
    }
}

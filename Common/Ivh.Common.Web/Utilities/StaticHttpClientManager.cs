﻿namespace Ivh.Common.Web.Utilities
{
    using Interfaces;

    public class StaticHttpClientManager : IStaticHttpClientManager
    {
        public bool UseExistingStaticHttpClient => true;
    }
}
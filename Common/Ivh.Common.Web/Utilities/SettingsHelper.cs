﻿namespace Ivh.Common.Web.Utilities
{
    using System;
    using System.Linq.Expressions;
    using System.Reflection;
    using System.Web.Mvc;
    using Cache;
    using Domain.Settings.Interfaces;
    using Ivh.Application.Core.Common.Dtos;
    using Ivh.Application.Core.Common.Interfaces;

    public static class SettingsHelper
    {
        private static readonly Lazy<ICache> Cache = new Lazy<ICache>(() => DependencyResolver.Current.GetService<IMemoryCache>());
        private const int CacheExpiration = 60 * 15;

        private static bool? _isClientApplication;

        public static bool IsClientApplication
        {
            get => _isClientApplication ?? (IsClientApplication = GetIsClientApplication());
            set => _isClientApplication = value;
        }
        
        public static string RedactedVisitReplacementValue => GetClientSetting(x => x.RedactedVisitReplacementValue);

        private static bool GetIsClientApplication()
        {
            IApplicationSettingsService applicationSettingsService = DependencyResolver.Current.GetService<IApplicationSettingsService>();

            return applicationSettingsService.IsClientApplication.Value;
        }
        
        private static string GetClientSetting<T>(Expression<Func<ClientDto, T>> expression)
        {
            PropertyInfo propertyInfo = (PropertyInfo)((MemberExpression)expression.Body).Member;

            string cacheKey = $"{nameof(SettingsHelper)}.ClientSetting.{propertyInfo.Name}";
            string value = Cache.Value.GetString(cacheKey);

            if (value != null)
            {
                return value;
            }

            IClientApplicationService clientApplicationService = DependencyResolver.Current.GetService<IClientApplicationService>();
            ClientDto clientDto = clientApplicationService.GetClient();

            object obj = propertyInfo.GetValue(clientDto);
            value = (obj ?? "").ToString();

            Cache.Value.SetString(cacheKey, value, CacheExpiration);

            return value;
        }
    }
}
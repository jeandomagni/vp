﻿

namespace Ivh.Common.Web.Utilities
{
    using System.Linq;
    using System.Security.Cryptography;
    using Base.Utilities.Extensions;
    using System;
    using System.Collections;
    using System.Reflection;
    using System.Security.Claims;
    using System.Security.Principal;
    using System.Web;
    using Base.Enums;
    using Ivh.Common.Web.Attributes;
    using Ivh.Common.Web.Extensions;
    using VisitPay.Enums;

    public class Obfuscator
    {
        private const int ObfuscationKeySize = 16;

        #region "Obfuscation Methods"

        /// <summary>
        /// Recursively iterate through object graph and obfuscate any property value that has the [Obfuscate] attribute
        /// </summary>
        /// <param name="obj">object graph that needs property values to be obfuscated</param>
        /// <param name="httpContext"></param>
        public void Obfuscate(object obj, HttpContextBase httpContext)
        {
            //TODO: Add processed objects to HashSet and check if they exist to avoid infinite recursion (ie. pointer to parent)
            if (obj != null)
            {
                Type t = obj.GetType();
                if (IsEnumerable(t))
                {
                    foreach (object sub in (IEnumerable)obj)
                    {
                        this.Obfuscate(sub, httpContext); //recurse
                    }
                }
                foreach (PropertyInfo p in t.GetProperties())
                {
                    if (IsEnumerable(p.PropertyType) && (IEnumerable)p.GetValue(obj, null) != null)
                    {
                        foreach (object sub in (IEnumerable)p.GetValue(obj, null))
                        {
                            this.Obfuscate(sub, httpContext); //recurse
                        }
                    }
                    else if (p.PropertyType.IsValueType || p.PropertyType == typeof(string)) //value type or string reference type
                    {
                        this.ObfuscatePropertyInfo(p, obj, httpContext);
                    }
                    else //reference type
                    {
                        object sub = p.GetValue(obj, null);
                        this.Obfuscate(sub, httpContext); //recurse
                    }
                }
            }
        }

        /// <summary>
        /// Recursively iterate through object graph and deobfuscate any property value that has the [Obfuscate] attribute
        /// </summary>
        /// <param name="obj">object graph that needs property values to be deobfuscated</param>
        /// <param name="httpContext">d</param>
        public void Deobfuscate(object obj, HttpContextBase httpContext)
        {
            //TODO: Add processed objects to HashSet and check if they exist to avoid infinite recursion (ie. pointer to parent)
            if (obj != null)
            {
                Type t = obj.GetType();
                if (IsEnumerable(t))
                {
                    foreach (object sub in (IEnumerable)obj)
                    {
                        this.Deobfuscate(sub, httpContext); //recurse
                    }
                }
                foreach (PropertyInfo p in t.GetProperties())
                {
                    if (IsEnumerable(p.PropertyType) && (IEnumerable)p.GetValue(obj, null) != null)
                    {
                        foreach (object sub in (IEnumerable)p.GetValue(obj, null))
                        {
                            this.Deobfuscate(sub, httpContext); //recurse
                        }
                    }
                    else if (p.PropertyType.IsValueType || p.PropertyType == typeof(string)) //value type or string reference type
                    {
                        this.DeobfuscatePropertyInfo(p, obj, httpContext);
                    }
                    else //reference type
                    {
                        object sub = p.GetValue(obj, null);
                        this.Deobfuscate(sub, httpContext); //recurse
                    }
                }
            }
        }

        public string DeobfuscateText(string value, HttpContextBase httpContext)
        {
            byte[] valueBytes = value.HexToByteArray();
            byte[] keyBytes = this.GetSessionObfuscationKey(httpContext);
            byte[] deobfuscatedBytes = XorBytes(keyBytes, valueBytes);
            byte[] originalBytes;
            if (TryCompareChecksum(deobfuscatedBytes, out originalBytes))
            {
                string deobfuscatedValue = deobfuscatedBytes.ToStringUTF8();
                return deobfuscatedValue;
            }
            throw new CryptographicException("Deobfuscated value does not match checksum.");
            //return val.Replace("Encrypted",""); //easier to test 
        }

        public string ObfuscateText(string value, HttpContextBase httpContext)
        {
            byte[] valueBytes = AppendChecksum(value.ToByteArrayUTF8());
            byte[] keyBytes = this.GetSessionObfuscationKey(httpContext);
            byte[] obfuscatedBytes = XorBytes(keyBytes, valueBytes);
            string obfuscatedValue = obfuscatedBytes.ToHex();
            return obfuscatedValue;
            //return val + "Encrypted"; //easier to test
        }

        #endregion

        #region "Helper Methods"

        private static bool IsEnumerable(Type t)
        {
            //Need to ignore string since it implements IEnumerable<Char>
            return !(t == typeof(string)) && typeof(IEnumerable).IsAssignableFrom(t);
        }

        private static bool IsObfuscateMember(PropertyInfo p)
        {
            return p.IsDefined(typeof(ObfuscateMemberAttribute), false);
        }

        #endregion

        #region "Obfuscation Methods"

        /// <summary>
        /// If the ObfuscateMember attribute is found, the PropertyInfo value is obfuscated
        /// </summary>
        /// <param name="propInfo"></param>
        /// <param name="o"></param>
        /// <param name="httpContext"></param>
        private void ObfuscatePropertyInfo(PropertyInfo propInfo, object o, HttpContextBase httpContext)
        {
            if (IsObfuscateMember(propInfo) && propInfo.GetValue(o) != null) //short circuit if ObfuscateMember attribute is not found
            {
                string propValue = propInfo.GetValue(o).ToString();
                string obfuscatedValue = this.ObfuscateText(propValue, httpContext);
                propInfo.SetValue(o, obfuscatedValue);
            }
        }

        /// <summary>
        /// If the ObfuscateMember attribute is found, the PropertyInfo value is deobfuscated
        /// </summary>
        /// <param name="propInfo"></param>
        /// <param name="o"></param>
        /// <param name="httpContext"></param>
        private void DeobfuscatePropertyInfo(PropertyInfo propInfo, object o, HttpContextBase httpContext)
        {
            if (IsObfuscateMember(propInfo) && propInfo.GetValue(o) != null) //short circuit if ObfuscateMember attribute is not found
            {
                string propValue = propInfo.GetValue(o).ToString();
                string deobfuscatedValue = this.DeobfuscateText(propValue, httpContext);
                propInfo.SetValue(o, deobfuscatedValue);
            }
        }

        private static byte[] XorBytes(byte[] key, byte[] value)
        {
            byte[] xorValueBytes = new byte[value.Length];
            for (int i = 0; i < value.Length; i++)
            {
                int passwordIndex = i % key.Length;
                xorValueBytes[i] = (byte)(value[i] ^ key[passwordIndex]);
            }
            return xorValueBytes;
        }

        private static byte CalculateChecksum(byte[] value)
        {
            long longSum = value.Sum(x => (long)x);
            return unchecked((byte)longSum);
        }

        private static byte[] AppendChecksum(byte[] value)
        {
            value.AddRange(new []{ CalculateChecksum(value) });
            return value;
        }

        private static bool TryCompareChecksum(byte[] value, out byte[] originalValue)
        {
            //remove final byte
            byte checksum = value.Last();
            originalValue = value.Take(value.Length - 1).ToArray();
            return checksum == CalculateChecksum(originalValue);
        }

        #endregion

        #region "Encryption Password Management"
        public byte[] GetSessionObfuscationKey(HttpContextBase httpContext)
        {
            string keyHexString = default(string);
            if (!httpContext.User.Identity.TryGetClaimValue(ClaimTypeEnum.ObfuscationKeySessionKey, out keyHexString))
            {
                byte[] keyBytes = new byte[ObfuscationKeySize];
                new RNGCryptoServiceProvider().GetBytes(keyBytes);
                httpContext.User.Identity.AddClaim(ClaimTypeEnum.ObfuscationKeySessionKey, keyBytes.ToHex());
                return keyBytes;
            }
            return keyHexString.HexToByteArray();
        }
        #endregion
    }
}

﻿namespace Ivh.Common.Web.Utilities
{
    using System.Collections.Generic;
    using System.Reflection;
    using System.Web.Mvc;
    using Interfaces;

    public static class MaskingHelper
    {
        public static void AfterMapAction(object src, object dest, IList<PropertyInfo> childProperties, IList<string> destinationFieldsToMask)
        {
            IMaskingService maskingService = DependencyResolver.Current.GetService<IMaskingService>();
            maskingService.Mask(src, dest, destinationFieldsToMask);

            foreach (PropertyInfo childProperty in childProperties)
            {
                object source = childProperty.GetValue(src, null);
                maskingService.Mask(source, dest, destinationFieldsToMask);
            }
        }
    }
}
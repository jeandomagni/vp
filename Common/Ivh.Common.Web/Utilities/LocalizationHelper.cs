﻿namespace Ivh.Common.Web.Utilities
{
    using System.Collections.Generic;
    using System.Web;
    using System.Web.Mvc;
    using Ivh.Application.Content.Common.Interfaces;

    public static class LocalizationHelper
    {
        /// <summary>
        /// Retrieves the localized version of the given string from the ContentApplicationService, if it exists
        /// and the service is available. Otherwise, it will return the string passed in.
        /// </summary>
        /// <param name="stringToGet"></param>
        /// <returns>The localized version of the string from ContentApplicationService if it exists, else the stringToGet passed in.</returns>
        public static string GetLocalizedString(string stringToGet)
        {
            return GetLocalizedString(stringToGet, null);
        }

        /// <summary>
        /// Retrieves the localized version of the given string from the ContentApplicationService, if it exists
        /// and the service is available. Otherwise, it will return the string passed in.
        /// </summary>
        /// <param name="stringToGet"></param>
        /// <param name="additionalValues"></param>
        /// <returns>The localized version of the string from ContentApplicationService if it exists, else the stringToGet passed in.</returns>
        public static string GetLocalizedString(string stringToGet, IDictionary<string, string> additionalValues)
        {
            //Default to the given string
            string localizedString = stringToGet;

            if (HttpContext.Current == null)
            {
                return localizedString;
            }

            try
            {
                IContentApplicationService contentApplicationService = DependencyResolver.Current.GetService<IContentApplicationService>();
                string foundLocalizedString = contentApplicationService.GetTextRegion(stringToGet, additionalValues);

                if (!string.IsNullOrEmpty(foundLocalizedString))
                {
                    //We found a localized version of the given string, so use it
                    localizedString = foundLocalizedString;
                }
            }
            catch
            {
                //Do nothing, will use stringToGet by default
            }

            return localizedString;
        }
    }
}

﻿namespace Ivh.Common.Web.HtmlHelpers
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Web.Mvc;
    using System.Web.Mvc.Html;
    using System.Web.Routing;
    using Enums;

    public static partial class HtmlHelperExtensions
    {
        public static MvcHtmlString VpActionLink(this HtmlHelper helper, string id, string text, string actionName, string controllerName, object htmlAttributes = null)
        {
            return helper.VpActionLink(id, text, actionName, controllerName, VpActionLinkTypeEnum.Default, null, htmlAttributes);
        }

        public static MvcHtmlString VpActionLink(this HtmlHelper helper, string id, string text, string actionName, string controllerName, VpActionLinkTypeEnum actionLinkType, VpButtonClassEnum? buttonClass, object htmlAttributes = null)
        {
            RouteValueDictionary attrs = HtmlHelper.AnonymousObjectToHtmlAttributes(htmlAttributes);
            attrs.Add("id", id);
            attrs.Add("title", text);

            if (actionLinkType == VpActionLinkTypeEnum.Button && buttonClass.HasValue)
            {
                List<string> classNames = GetClassNames(buttonClass.Value);
                if (classNames.Any())
                {
                    attrs.Add("class", string.Join(" ", classNames));
                }
            }

            return helper.ActionLink(text, actionName, controllerName, new RouteValueDictionary(), attrs);
        }
        
        public static MvcHtmlString VpButton(this HtmlHelper helper, string id, string text, VpButtonTypeEnum buttonType, VpButtonClassEnum buttonClass = VpButtonClassEnum.Primary, object htmlAttributes = null)
        {
            string type;

            switch (buttonType)
            {
                case VpButtonTypeEnum.Submit:
                    type = "submit";
                    break;
                case VpButtonTypeEnum.Button:
                default:
                    type = "button";
                    break;
            }

            TagBuilder tagBuilder = new TagBuilder("button");
            tagBuilder.MergeAttribute("type", type, true);
            tagBuilder.MergeAttribute("id", id, true);
            tagBuilder.MergeAttribute("title", text);
            tagBuilder.SetInnerText(text);
            
            RouteValueDictionary attrs = HtmlHelper.AnonymousObjectToHtmlAttributes(htmlAttributes);
            foreach (KeyValuePair<string, object> attr in attrs)
            {
                tagBuilder.MergeAttribute(attr.Key, attr.Value.ToString(), true);
            }

            List<string> classNames = GetClassNames(buttonClass);
            if (classNames.Any())
            {
                classNames.Reverse();
                classNames.ForEach(tagBuilder.AddCssClass);
            }

            return new MvcHtmlString(tagBuilder.ToString(TagRenderMode.Normal));
        }

        private static List<string> GetClassNames(VpButtonClassEnum buttonClass)
        {
            List<string> classNames = new List<string>();

            switch (buttonClass)
            {
                case VpButtonClassEnum.AccentOutline:
                    classNames.Add("btn");
                    classNames.Add("btn-accent");
                    classNames.Add("btn-outline");
                    break;
                case VpButtonClassEnum.DefaultOutline:
                    classNames.Add("btn");
                    classNames.Add("btn-default");
                    classNames.Add("btn-outline");
                    break;
                case VpButtonClassEnum.Primary:
                default:
                    classNames.Add("btn");
                    classNames.Add("btn-primary");
                    break;
            }

            return classNames;
        }
    }
}
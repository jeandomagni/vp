﻿namespace Ivh.Common.Web.HtmlHelpers
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Web.Mvc;
    using Base.Utilities.Lookup;

    public static partial class HtmlHelperExtensions
    {
        //JUST THE STATIC CONSTRUCTOR HERE TO INITIALIZE ANY STATIC LISTS
        private static readonly List<SelectListItem> StatesListItems;
        
        static HtmlHelperExtensions()
        {
            StatesListItems = Geo.GetStates().Select(x => new SelectListItem {Text = x.Value.ToString(), Value = x.Key.ToString()}).ToList();
        }
    }
}
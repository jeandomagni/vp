﻿
namespace Ivh.Common.Web.HtmlHelpers
{
    using Extensions;
    using System.Security.Principal;
    using System.Web.Mvc;

    public static partial class HtmlHelperExtensions
    {
        public static string GetUserChatToken(this HtmlHelper helper, IPrincipal user)
        {
            if (user.CurrentUserGuid().HasValue)
            {
                return user.CurrentUserGuid().Value.ToString();
            }

            return null;
        }
    }
}

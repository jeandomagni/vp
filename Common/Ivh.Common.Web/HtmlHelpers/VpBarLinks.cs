﻿namespace Ivh.Common.Web.HtmlHelpers
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Web.Mvc;

    public static partial class HtmlHelperExtensions
    {
        public static MvcHtmlString VpBarLinks(this HtmlHelper helper, params object[] links)
        {
            if (links == null || links.Length == 0)
            {
                return MvcHtmlString.Empty;
            }

            return helper.VpBarLinks(links.ToList());
        }

        public static MvcHtmlString VpBarLinks(this HtmlHelper helper, IList<object> links)
        {
            if (links == null || links.Count == 0)
            {
                return MvcHtmlString.Empty;
            }

            TagBuilder ul = new TagBuilder("ul");
            ul.AddCssClass("links");

            foreach (string link in links.Select(x => x.ToString()))
            {
                ul.InnerHtml += new TagBuilder("li")
                {
                    InnerHtml = link
                }.ToString(TagRenderMode.Normal);
            }

            return new MvcHtmlString(ul.ToString(TagRenderMode.Normal));
        }
    }
}
﻿namespace Ivh.Common.Web.HtmlHelpers
{
    using System;
    using System.Web.Mvc;

    public static partial class HtmlHelperExtensions
    {
        public static MvcHtmlString VpMenuItem(this HtmlHelper helper, string url, string text, string id, string icon = null)
        {
            TagBuilder a = new TagBuilder("a")
            {
                InnerHtml = text
            };

            a.MergeAttribute("href", url);
            a.MergeAttribute("title", text);

            if (!string.IsNullOrEmpty(id))
            {
                a.MergeAttribute("id", id);
            }

            if (!string.IsNullOrEmpty(icon))
            {
                TagBuilder em = new TagBuilder("em");
                em.AddCssClass("glyphicon");
                em.AddCssClass(icon);

                a.InnerHtml = string.Join("", em.ToString(TagRenderMode.Normal), a.InnerHtml);
            }

            TagBuilder li = new TagBuilder("li")
            {
                InnerHtml = a.ToString(TagRenderMode.Normal)
            };

            if (string.Equals(helper.ViewContext.HttpContext.Request.RawUrl.Split('?')[0], url, StringComparison.CurrentCultureIgnoreCase))
            {
                li.AddCssClass("active");
            }
            
            return new MvcHtmlString(li.ToString(TagRenderMode.Normal));
        }
    }
}
﻿namespace Ivh.Common.Web.HtmlHelpers
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Web.Mvc;
    using System.Web.Routing;
    using Base.Utilities.Extensions;

    public static partial class HtmlHelperExtensions
    {
        public static MvcHtmlString VpValidationSummaryCustomErrors<TModel>(this HtmlHelper<TModel> helper, string message = null, object htmlAttributes = null)
        {
            RouteValueDictionary attrs = new RouteValueDictionary();
            MergeAttributes(attrs, htmlAttributes, null);

            IList<string> classes = new List<string>
            {
                "validation-summary-custom-errors",
                "validation-summary-valid"
            };

            if (attrs.ContainsKey("class"))
            {
                string[] split = (attrs["class"]?.ToString() ?? string.Empty).Split(' ');
                if (split.Any())
                {
                    classes.AddRange(split);
                }
            }
            attrs["class"] = string.Join(" ", classes);
            attrs["data-valmsg-summary-custom"] = "true";

            TagBuilder divBuilder = new TagBuilder("div");
            divBuilder.MergeAttributes(attrs);

            if (!string.IsNullOrEmpty(message))
            {
                TagBuilder messageBuilder = new TagBuilder("span");
                messageBuilder.SetInnerText(message);

                divBuilder.InnerHtml += messageBuilder.ToString(TagRenderMode.Normal);
            }

            TagBuilder listBuilder = new TagBuilder("ul");
            divBuilder.InnerHtml += listBuilder.ToString(TagRenderMode.Normal);

            return new MvcHtmlString(divBuilder.ToString(TagRenderMode.Normal));
        }
    }
}
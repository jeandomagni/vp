﻿namespace Ivh.Common.Web.HtmlHelpers
{
    using System.Text;
    using System.Web.Mvc;

    public static partial class HtmlHelperExtensions
    {
        public static MvcHtmlString GridElements(this HtmlHelper helper, string gridName, string alertText, bool withPager = true)
        {
            gridName = gridName ?? "";

            StringBuilder sb = new StringBuilder();

            TagBuilder table = new TagBuilder("table");
            table.MergeAttribute("id", $"jqGrid{gridName}");
            sb.Append(table.ToString(TagRenderMode.Normal));

            if (withPager)
            {
                TagBuilder div = new TagBuilder("div");
                div.MergeAttribute("id", $"jqGridPager{gridName}");
                sb.Append(div.ToString(TagRenderMode.Normal));
            }

            if (!string.IsNullOrEmpty(alertText))
            {
                MvcHtmlString alert = helper.GridAlert($"jqGridNoRecords{gridName}", alertText);
                sb.Append(alert);
            }

            return new MvcHtmlString(sb.ToString());
        }
    }
}
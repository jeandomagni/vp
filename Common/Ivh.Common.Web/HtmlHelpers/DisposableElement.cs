﻿namespace Ivh.Common.Web.HtmlHelpers
{
    using System;
    using System.Text;
    using System.Web.Mvc;

    internal class DisposableElement : IDisposable
    {
        private readonly StringBuilder _elements;
        private readonly HtmlHelper _helper;

        public DisposableElement(HtmlHelper helper, StringBuilder elements)
        {
            this._helper = helper;
            this._elements = elements;
        }

        public void Dispose()
        {
            this._helper.ViewContext.Writer.Write(this._elements.ToString());
        }
    }
}
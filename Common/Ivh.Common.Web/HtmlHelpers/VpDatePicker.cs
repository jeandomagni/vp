﻿namespace Ivh.Common.Web.HtmlHelpers
{
    using System;
    using System.Linq.Expressions;
    using System.Web.Mvc;

    public static partial class HtmlHelperExtensions
    {
        public static MvcHtmlString VpDatePickerFor<TModel, TProperty>(this HtmlHelper<TModel> helper, Expression<Func<TModel, TProperty>> expression, object htmlAttributes)
        {
            TagBuilder div = new TagBuilder("div");
            div.AddCssClass("input-group");
            div.AddCssClass("date");

            MvcHtmlString textbox = helper.VpTextBoxFor(expression, htmlAttributes);

            div.InnerHtml = textbox.ToString();

            TagBuilder span2 = new TagBuilder("span");
            span2.AddCssClass("glyphicon");
            span2.AddCssClass("glyphicon-calendar");

            TagBuilder span1 = new TagBuilder("span");
            span1.AddCssClass("input-group-addon");
            span1.InnerHtml = span2.ToString(TagRenderMode.Normal);

            div.InnerHtml += span1.ToString(TagRenderMode.Normal);

            return new MvcHtmlString(div.ToString(TagRenderMode.Normal));
        }
    }
}
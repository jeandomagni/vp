﻿namespace Ivh.Common.Web.HtmlHelpers
{
    using System;
    using System.Linq.Expressions;
    using System.Web.Mvc;
    using System.Web.Mvc.Html;
    using Ivh.Common.Base.Constants;
    using Utilities;
    using VisitPay.Constants;

    public static partial class HtmlHelperExtensions
    {
        public static MvcHtmlString VpStatesDropDownListFor<TModel, TValue>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, TValue>> expression, object htmlAttributes = null)
        {
            return htmlHelper.DropDownListFor(expression, StatesListItems, $"--{LocalizationHelper.GetLocalizedString(TextRegionConstants.State)}--", htmlAttributes);
        }
    }
}
﻿namespace Ivh.Common.Web.HtmlHelpers
{
    using System;
    using System.ComponentModel.DataAnnotations;
    using System.Linq.Expressions;
    using System.Web.Mvc;
    using System.Web.Mvc.Html;
    using System.Web.Routing;
    using Extensions;

    public static partial class HtmlHelperExtensions
    {
        public static MvcHtmlString VpTextBoxFor<TModel, TProperty>(this HtmlHelper<TModel> helper, Expression<Func<TModel, TProperty>> expression)
        {
            return VpTextBoxFor(helper, expression, null, null);
        }

        public static MvcHtmlString VpTextBoxFor<TModel, TProperty>(this HtmlHelper<TModel> helper, Expression<Func<TModel, TProperty>> expression, string format)
        {
            return VpTextBoxFor(helper, expression, format, null);
        }

        public static MvcHtmlString VpTextBoxFor<TModel, TProperty>(this HtmlHelper<TModel> helper, Expression<Func<TModel, TProperty>> expression, object htmlAttributes)
        {
            return VpTextBoxFor(helper, expression, null, htmlAttributes);
        }
        
        public static MvcHtmlString VpTextBoxFor<TModel, TProperty>(this HtmlHelper<TModel> helper, Expression<Func<TModel, TProperty>> expression, string format, object htmlAttributes)
        {
            ModelMetadata metadata = ModelMetadata.FromLambdaExpression(expression, helper.ViewData);

            RouteValueDictionary attrs = HtmlHelper.AnonymousObjectToHtmlAttributes(htmlAttributes);
            if (!string.IsNullOrEmpty(metadata.Watermark))
            {
                attrs.Add("placeholder", metadata.Watermark.ReplaceClientValues());
            }

            if (!attrs.ContainsKey("class"))
            {
                attrs.Add("class", "form-control");
            }

            if (!string.IsNullOrEmpty(metadata.DataTypeName))
            {
                if (metadata.DataTypeName == DataType.Date.ToString())
                {
                    attrs.Add("data-mask", "99/99/9999");
                }
                else if (metadata.DataTypeName == DataType.PhoneNumber.ToString())
                {
                    attrs.Add("data-mask", "(999) 999-9999");
                }
                else
                {
                    switch (metadata.DataTypeName.ToUpper())
                    {
                        case "ACCESSTOKENPHI":
                            attrs.Add("data-mask", "9999-9999");
                            break;
                        case "BANKROUTINGNUMBER":
                            attrs.Add("data-mask", "999999999");
                            break;
                        case "BANKACCOUNTNUMBER":
                            attrs.Add("data-mask", "9999?9999999999999");
                            break;
                        case "SSN4":
                            attrs.Add("data-mask", "9999");
                            break;
                        case "ZIP":
                            attrs.Add("data-mask", "99999?-9999");
                            break;
                    }
                }
            }

            return helper.TextBoxFor(expression, format, attrs);
        }

        public static MvcHtmlString VpSsnBoxFor<TModel, TProperty>(this HtmlHelper<TModel> helper, Expression<Func<TModel, TProperty>> expression)
        {
            return helper.VpTextBoxFor(expression, Ssn4Attributes);
        }

        private static object Ssn4Attributes => new
        {
            @class = "form-control ssn4",
            autocomplete = "off",
            type = "tel"
        };
    }
}
﻿namespace Ivh.Common.Web.HtmlHelpers
{
    using System;
    using System.Web.Mvc;
    using System.Web.Mvc.Html;
    using System.Web.Routing;

    public static partial class HtmlHelperExtensions
    {
        public static IDisposable VpForm(this HtmlHelper helper, string actionName, string controllerName, FormMethod formMethod = FormMethod.Post, string id = null, object htmlAttributes = null, params string[] vueAttributes)
        {
            RouteValueDictionary attrs = new RouteValueDictionary();
            MergeAttributes(attrs, htmlAttributes, vueAttributes);

            if (!string.IsNullOrEmpty(id))
            {
                attrs["id"] = id;
            }

            if (string.IsNullOrEmpty(actionName) && !attrs.ContainsKey("action"))
            {
                controllerName = null;
                actionName = null;
                attrs["action"] = "javascript:;";
            }
            
            MvcForm form = helper.BeginForm(actionName, controllerName, formMethod, attrs);
            helper.ViewContext.Writer.Write(helper.AntiForgeryToken().ToHtmlString());
            
            return form;
        }

        public static IDisposable VpForm(this HtmlHelper helper, string id = null, object htmlAttributes = null, params string[] vueAttributes)
        {
            return VpForm(helper, null, null, FormMethod.Post, id, htmlAttributes, vueAttributes);
        }
    }
}

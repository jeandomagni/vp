﻿namespace Ivh.Common.Web.HtmlHelpers
{
    using System;
    using System.Collections.Generic;
    using System.Linq.Expressions;
    using System.Web.Mvc;
    using System.Web.Mvc.Html;
    using Base.Constants;
    using Utilities;
    using VisitPay.Constants;

    public static partial class HtmlHelperExtensions
    {
        public static MvcHtmlString VpPhoneNumberTypesDropDownListFor<TModel, TValue>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, TValue>> expression, object htmlAttributes = null)
        {
            string mobileText = LocalizationHelper.GetLocalizedString(TextRegionConstants.Mobile);
            string otherText = LocalizationHelper.GetLocalizedString(TextRegionConstants.Other);

            List<SelectListItem> items = new List<SelectListItem>
            {
                new SelectListItem {Text = mobileText, Value = "Mobile"},
                new SelectListItem {Text = otherText, Value = "Other"}
            };

            return htmlHelper.DropDownListFor(expression, items, $"--{LocalizationHelper.GetLocalizedString(TextRegionConstants.PhoneType)}--", htmlAttributes);
        }
    }
}
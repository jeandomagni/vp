﻿namespace Ivh.Common.Web.HtmlHelpers
{
    using System;
    using System.Text;
    using System.Web.Mvc;

    public static partial class HtmlHelperExtensions
    {
        public static IDisposable BeginVpPageHeading(this HtmlHelper helper, string pageTitle, bool withBar = false)
        {
            TagBuilder h1 = CreateHeaderTag(withBar);

            helper.ViewContext.Writer.Write(h1.ToString(TagRenderMode.StartTag));
            helper.ViewContext.Writer.Write(pageTitle);

            return new DisposableElement(helper, new StringBuilder(h1.ToString(TagRenderMode.EndTag)));
        }

        public static MvcHtmlString VpPageHeading(this HtmlHelper helper, string pageTitle, bool withBar = false)
        {
            TagBuilder h1 = CreateHeaderTag(withBar);
            h1.InnerHtml = pageTitle;

            return new MvcHtmlString(h1.ToString(TagRenderMode.Normal));
        }

        private static TagBuilder CreateHeaderTag(bool withBar)
        {
            TagBuilder h1 = new TagBuilder("h1");
            if (withBar)
            {
                h1.AddCssClass("bar");
            }
            else
            {
                h1.AddCssClass("no-bar");
            }

            return h1;
        }
    }
}
﻿namespace Ivh.Common.Web.HtmlHelpers
{
    using System;
    using System.Web.Mvc;

    public static partial class HtmlHelperExtensions
    {
        public static string CurrentArea(this HtmlHelper helper)
        {
            object area = helper.ViewContext.RouteData.DataTokens["area"];
            return area != null ? (string) area : string.Empty;
        }

        public static bool InArea(this HtmlHelper helper, string area)
        {
            return helper.CurrentArea().Equals(area, StringComparison.OrdinalIgnoreCase);
        }

        public static bool InMobile(this HtmlHelper helper)
        {
            return helper.InArea("mobile");
        }
    }
}
﻿namespace Ivh.Common.Web.HtmlHelpers
{
    using System.Web.Mvc;
    using Ivh.Application.Content.Common.Interfaces;
    using Ivh.Common.Base.Constants;
    using VisitPay.Constants;

    public static partial class HtmlHelperExtensions
    {
        public static MvcHtmlString ExportButton(this HtmlHelper helper, string id, string text = null, object htmlAttributes = null)
        {
            //Set default localized Export text, if needed
            if (string.IsNullOrEmpty(text))
            {
                IContentApplicationService contentApplicationService = DependencyResolver.Current.GetService<IContentApplicationService>();
                string textValue = contentApplicationService.GetTextRegion(TextRegionConstants.Export);
                text = textValue;
            }

            TagBuilder a = new TagBuilder("a");
            a.AddCssClass("export-excel");
            a.MergeAttribute("href", "javascript:;");
            a.MergeAttribute("title", text);

            if (htmlAttributes != null)
            {
                a.MergeAttributes(HtmlHelper.AnonymousObjectToHtmlAttributes(htmlAttributes));
            }

            a.SetInnerText(text);
            if (!string.IsNullOrEmpty(id))
            {
                a.MergeAttribute("id", id);
            }

            return new MvcHtmlString(a.ToString(TagRenderMode.Normal));
        }

        public static MvcHtmlString ExportIconExcel(this HtmlHelper helper, string id, string piwikEventCategory, string text = "Export", object htmlAttributes = null)
        {
            return ExportIcon(id, "/Content/Images/FileTypes/icon-excel.png", "export-excel", piwikEventCategory, "Export", text, htmlAttributes);
        }

        public static MvcHtmlString ExportIconPdf(this HtmlHelper helper, string id, string piwikEventCategory, string text = "Export", object htmlAttributes = null)
        {
            return ExportIcon(id, "/Content/Images/FileTypes/icon-pdf.png", "export-pdf", piwikEventCategory, "ExportPdf", text, htmlAttributes);
        }

        private static MvcHtmlString ExportIcon(string id, string imagePath, string cssClass, string piwikEventCategory, string piwikEventName, string text = "Export", object htmlAttributes = null)
        {
            TagBuilder input = new TagBuilder("input");
            input.AddCssClass(cssClass);
            input.MergeAttribute("href", "javascript:;");
            input.MergeAttribute("title", text);
            input.MergeAttribute("type", "image");
            input.MergeAttribute("src", imagePath);
            input.MergeAttribute("data-piwik-click","");
            input.MergeAttribute("data-piwik-event-category", piwikEventCategory);
            input.MergeAttribute("data-piwik-event-name", piwikEventName);

            if (htmlAttributes != null)
            {
                input.MergeAttributes(HtmlHelper.AnonymousObjectToHtmlAttributes(htmlAttributes));
            }

            if (!string.IsNullOrEmpty(id))
            {
                input.MergeAttribute("id", id);
            }

            return new MvcHtmlString(input.ToString(TagRenderMode.Normal));
        }
    }
}
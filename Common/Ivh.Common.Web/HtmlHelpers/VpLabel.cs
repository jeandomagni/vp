﻿namespace Ivh.Common.Web.HtmlHelpers
{
    using System;
    using System.Linq.Expressions;
    using System.Web.Mvc;
    using System.Web.Mvc.Html;
    using System.Web.Routing;
    using Extensions;

    public static partial class HtmlHelperExtensions
    {
        public static MvcHtmlString VpLabelFor<TModel, TValue>(this HtmlHelper<TModel> helper, Expression<Func<TModel, TValue>> expression)
        {
            return helper.VpLabelFor(expression, null, null);
        }

        /// <summary>
        /// When the control is conditionally required the required class will be added/removed to the label based off the required parameter
        /// </summary>
        /// <typeparam name="TModel"></typeparam>
        /// <typeparam name="TValue"></typeparam>
        /// <param name="helper"></param>
        /// <param name="expression"></param>
        /// <param name="required">true will add the 'required' class to the label element</param>
        /// <returns></returns>
        public static MvcHtmlString VpLabelFor<TModel, TValue>(this HtmlHelper<TModel> helper, Expression<Func<TModel, TValue>> expression, bool required)
        {
            return helper.VpLabelFor(expression, null, null, required);
        }

        public static MvcHtmlString VpLabelFor<TModel, TValue>(this HtmlHelper<TModel> helper, Expression<Func<TModel, TValue>> expression, object htmlAttributes)
        {
            return helper.VpLabelFor(expression, null, htmlAttributes);
        }

        public static MvcHtmlString VpLabelFor<TModel, TValue>(this HtmlHelper<TModel> helper, Expression<Func<TModel, TValue>> expression, string labelText, object htmlAttributes)
        {
            ModelMetadata metadata = ModelMetadata.FromLambdaExpression(expression, helper.ViewData);
            RouteValueDictionary attrs = HtmlHelper.AnonymousObjectToHtmlAttributes(htmlAttributes);

            if (string.IsNullOrEmpty(labelText))
            {
                labelText = metadata.DisplayName.ReplaceClientValues();
            }

            if (metadata.IsRequired)
            {
                if (!attrs.ContainsKey("class"))
                {
                    attrs.Add("class", "required");
                }
                else
                {
                    attrs["class"] = string.Concat(attrs["class"], " ", "required");
                }
            }


            return !string.IsNullOrEmpty(labelText) ? helper.LabelFor(expression, labelText, attrs) : helper.LabelFor(expression, attrs);
        }
        
        public static MvcHtmlString VpLabelFor<TModel, TValue>(this HtmlHelper<TModel> helper, Expression<Func<TModel, TValue>> expression, string labelText, object htmlAttributes, bool required)
        {
            ModelMetadata metadata = ModelMetadata.FromLambdaExpression(expression, helper.ViewData);
            RouteValueDictionary attrs = HtmlHelper.AnonymousObjectToHtmlAttributes(htmlAttributes);

            if (string.IsNullOrEmpty(labelText))
            {
                labelText = metadata.DisplayName.ReplaceClientValues();
            }

            if (required)
            {
                if (!attrs.ContainsKey("class"))
                {
                    attrs.Add("class", "required");
                }
                else
                {
                    attrs["class"] = string.Concat(attrs["class"], " ", "required");
                }
            }


            return !string.IsNullOrEmpty(labelText) ? helper.LabelFor(expression, labelText, attrs) : helper.LabelFor(expression, attrs);
        }
    }
}
﻿namespace Ivh.Common.Web.HtmlHelpers
{
    using System.Web.Mvc;
    using Ivh.Application.Core.Common.Interfaces;

    public static partial class HtmlHelperExtensions
    {
        public static MvcHtmlString GuarantorPasswordRequirements(this HtmlHelper helper)
        {
            IVisitPayUserApplicationService visitPayUserApplicationService = DependencyResolver.Current.GetService<IVisitPayUserApplicationService>();
            string guarantorPasswordRequirements = visitPayUserApplicationService.GetGuarantorPasswordRequirements().Result;

            return new MvcHtmlString(guarantorPasswordRequirements);
        }

        public static MvcHtmlString GuarantorUsernameRequirements(this HtmlHelper helper)
        {
            IVisitPayUserApplicationService visitPayUserApplicationService = DependencyResolver.Current.GetService<IVisitPayUserApplicationService>();
            string guarantorUsernameRequirements = visitPayUserApplicationService.GetGuarantorUsernameRequirements().Result;

            return new MvcHtmlString(guarantorUsernameRequirements);
        }

        public static MvcHtmlString ClientPasswordRequirements(this HtmlHelper helper)
        {
            IVisitPayUserApplicationService visitPayUserApplicationService = DependencyResolver.Current.GetService<IVisitPayUserApplicationService>();
            string clientPasswordRequirements = visitPayUserApplicationService.GetClientPasswordRequirements();

            return new MvcHtmlString(clientPasswordRequirements);
        }
    }
}
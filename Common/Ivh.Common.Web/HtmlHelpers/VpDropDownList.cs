﻿namespace Ivh.Common.Web.HtmlHelpers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Linq.Expressions;
    using System.Web.Mvc;
    using System.Web.Mvc.Html;
    using System.Web.Routing;
    using Base.Utilities.Extensions;
    using Extensions;
    using Utilities;
    using VisitPay.Constants;

    public static partial class HtmlHelperExtensions
    {
        public static MvcHtmlString VpDropDownListFor<TModel, TProperty>(this HtmlHelper<TModel> helper, Expression<Func<TModel, TProperty>> expression, IEnumerable<SelectListItem> selectList, object htmlAttributes = null)
        {
            ModelMetadata metadata = ModelMetadata.FromLambdaExpression(expression, helper.ViewData);

            string optionLabel = null;
            if (!string.IsNullOrEmpty(metadata.Watermark))
            {
                optionLabel = metadata.Watermark.ReplaceClientValues();
            }

            return helper.VpDropDownListFor(expression, selectList, optionLabel, htmlAttributes);
        }

        public static MvcHtmlString VpDropDownListFor<TModel, TProperty>(this HtmlHelper<TModel> helper, Expression<Func<TModel, TProperty>> expression, IEnumerable<SelectListItem> selectList, string optionLabel, object htmlAttributes = null)
        {
            RouteValueDictionary attrs = new RouteValueDictionary();
            MergeAttributes(attrs, htmlAttributes, null);

            if (!attrs.ContainsKey("class"))
            {
                attrs.Add("class", "form-control");
            }

            optionLabel = string.IsNullOrWhiteSpace(optionLabel) ? "-" : optionLabel;

            return helper.DropDownListFor(expression, selectList, optionLabel, attrs);
        }

        public static MvcHtmlString VpPaymentDueDayDropDownList(this HtmlHelper htmlHelper, string name, object htmlAttributes = null)
        {
            IList<SelectListItem> selectListItems = (DateConstants.LatestValidPaymentDueDay + 1).Select(x =>
            {
                x = x < DateConstants.LatestValidPaymentDueDay ? x + 1 : DateConstants.EndOfMonthDay;
                return new SelectListItem {Text = x.ToOrdinalPaymentDueDay(LocalizationHelper.GetLocalizedString(TextRegionConstants.LastDay)), Value = x.ToString()};
            }).ToList();

            return htmlHelper.DropDownList(name, selectListItems, htmlAttributes);
        }
    }
}
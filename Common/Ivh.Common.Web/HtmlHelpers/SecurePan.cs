﻿namespace Ivh.Common.Web.HtmlHelpers
{
    using System;
    using System.Collections.Generic;
    using System.Security.Cryptography;
    using System.Threading;
    using System.Web;
    using System.Web.Mvc;
    using Common.Encryption;
    using Domain.Settings.Enums;
    using Domain.Settings.Interfaces;
    using Newtonsoft.Json;

    public class SecurePanAuthPayload
    {
        public string Identifier { get; set; }
        public DateTime Date { get; set; }
        public string SourceDomain { get; set; }
    }

    public static class SecurePanAuthorization
    {
        private const int RandomnessLength = 15; //15 to keep backward compatibility "iVinciIsTheBest" = 15 chars

        public static string GenerateAuthToken(IApplicationSettingsService applicationSettings)
        {
            SecurePanAuthPayload payload = new SecurePanAuthPayload
            {
                Identifier = Guid.NewGuid().ToString(),
                Date = DateTime.UtcNow,
                SourceDomain = $"{HttpContext.Current.Request.Url.Scheme}://{HttpContext.Current.Request.Url.Authority}"
            };

            byte[] randomBytes = new byte[RandomnessLength];
            new RNGCryptoServiceProvider().GetBytes(randomBytes);
            string authToken = AESThenHMAC.SimpleEncrypt(JsonConvert.SerializeObject(payload), applicationSettings.SecurePanEncryptionKey.Value, applicationSettings.SecurePanAuthKey.Value, randomBytes);

            return authToken;
        }

        public static string GenerateUrl(IApplicationSettingsService applicationSettings)
        {
            string authToken = GenerateAuthToken(applicationSettings);

            string locale = Thread.CurrentThread.CurrentUICulture.Name;
            return applicationSettings.GetUrl(UrlEnum.SecurePan).AbsoluteUri + $"?auth={HttpUtility.UrlEncode(authToken)}&locale={locale}";
        }
    }

    public static partial class HtmlHelperExtensions
    {
        // <iframe class="form-control secure-pan-frame" src="..." frameborder="0" scrolling="no"></iframe>
        public static MvcHtmlString SecurePan(this HtmlHelper helper)
        {
            IApplicationSettingsService applicationSettings = DependencyResolver.Current.GetService<IApplicationSettingsService>();

            string url = SecurePanAuthorization.GenerateUrl(applicationSettings);
            TagBuilder tagBuilder = new TagBuilder("iframe");
            tagBuilder.AddCssClass("form-control");
            tagBuilder.AddCssClass("secure-pan-frame");
            tagBuilder.Attributes.Add(new KeyValuePair<string, string>("src", url));
            tagBuilder.Attributes.Add("frameborder", "0");
            tagBuilder.Attributes.Add("scrolling", "no");
            return new MvcHtmlString(tagBuilder.ToString());
        }
    }
}
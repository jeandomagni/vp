﻿namespace Ivh.Common.Web.HtmlHelpers
{
    using System.Collections.Generic;
    using System.Web.Mvc;
    using Ivh.Application.Content.Common.Dtos;
    using Ivh.Application.Content.Common.Interfaces;
    using Ivh.Application.Core.Common.Dtos;
    using Base.Enums;
    using Base.Utilities.Extensions;
    using Base.Utilities.Helpers;
    using Models.Shared;
    using Utilities;
    using VisitPay.Enums;
    using HtmlHelper = System.Web.Mvc.HtmlHelper;

    public static partial class HtmlHelperExtensions
    {
        public static MvcHtmlString CmsLabel(this HtmlHelper helper, string regionName, params object[] values)
        {
            return helper.CmsLabel(regionName, string.Empty, values);
        }
        public static MvcHtmlString CmsLabel(this HtmlHelper helper, string regionName, string defaultLabel, params object[] values)
        {
            IContentApplicationService contentApplicationService = DependencyResolver.Current.GetService<IContentApplicationService>();

            CmsVersionDto cmsVersion = contentApplicationService.GetLabelAsync(regionName, defaultLabel).Result;
            if (!string.IsNullOrEmpty(cmsVersion.ContentBody))
            {
                TagBuilder tagBuilder = new TagBuilder("span");
                tagBuilder.Attributes.Add(new KeyValuePair<string, string>("data-region", regionName));
                tagBuilder.Attributes.Add(new KeyValuePair<string, string>("data-CmsRegionId", cmsVersion.CmsRegion.CmsRegionId.ToString()));
                tagBuilder.InnerHtml = string.Format(cmsVersion.ContentBody, values);

                return new MvcHtmlString(tagBuilder.ToString());
            }

            return new MvcHtmlString(string.Empty);
        }

        public static MvcHtmlString CmsLabelFor(this HtmlHelper helper, string expression, string regionName, params object[] values)
        {
            return helper.CmsLabelFor(expression, regionName, string.Empty, values);
        }
        public static MvcHtmlString CmsLabelFor(this HtmlHelper helper, string expression, string regionName, string defaultLabel, params object[] values)
        {
            IContentApplicationService contentApplicationService = DependencyResolver.Current.GetService<IContentApplicationService>();

            CmsVersionDto cmsVersion = contentApplicationService.GetLabelAsync(regionName, defaultLabel).Result;
            if (!string.IsNullOrEmpty(cmsVersion.ContentBody))
            {
                TagBuilder tagBuilder = new TagBuilder("label");
                tagBuilder.Attributes.Add(new KeyValuePair<string, string>("data-region", regionName));
                tagBuilder.Attributes.Add(new KeyValuePair<string, string>("data-CmsRegionId", cmsVersion.CmsRegion.CmsRegionId.ToString()));
                tagBuilder.Attributes.Add(new KeyValuePair<string, string>("for", expression));
                tagBuilder.InnerHtml = string.Format(cmsVersion.ContentBody, values);

                return new MvcHtmlString(tagBuilder.ToString());
            }

            return new MvcHtmlString(string.Empty);
        }

        public static MvcHtmlString CmsRegion(this HtmlHelper helper, CmsRegionEnum cmsRegion, params object[] values)
        {
            IContentApplicationService contentApplicationService = DependencyResolver.Current.GetService<IContentApplicationService>();
            string contentBody = contentApplicationService.GetCurrentVersionAsync(cmsRegion).Result.ContentBody;
            
            return values.IsNotNullOrEmpty() ? new MvcHtmlString(string.Format(contentBody, values)) : new MvcHtmlString(contentBody);
        }

        public static MvcHtmlString CmsRegionWithDefaultValue(this HtmlHelper helper, CmsRegionEnum? cmsRegion, string defaultValue)
        {
            if (!cmsRegion.HasValue)
            {
                return new MvcHtmlString(defaultValue ?? string.Empty);
            }

            return helper.CmsRegion(cmsRegion.Value);
        }

        public static CmsViewModel CmsModel(this HtmlHelper helper, CmsRegionEnum cmsRegion, params object[] values)
        {
            IContentApplicationService contentApplicationService = DependencyResolver.Current.GetService<IContentApplicationService>();
            CmsVersionDto cmsVersionDto = contentApplicationService.GetCurrentVersionAsync(cmsRegion).Result;

            CmsViewModel model = new CmsViewModel
            {
                ContentBody = values.IsNotNullOrEmpty() ? string.Format(cmsVersionDto.ContentBody ?? string.Empty, values) : cmsVersionDto.ContentBody,
                ContentTitle = cmsVersionDto.ContentTitle
            };

            return model;
        }

        public static CmsViewModel CmsModelWithAdditionalValues(this HtmlHelper helper, CmsRegionEnum cmsRegion, IDictionary<string, string> additionalValues)
        {
            IContentApplicationService contentApplicationService = DependencyResolver.Current.GetService<IContentApplicationService>();
            CmsVersionDto cmsVersionDto = contentApplicationService.GetCurrentVersionAsync(cmsRegion).Result;

            CmsViewModel model = new CmsViewModel
            {
                ContentBody = ReplacementUtility.Replace(cmsVersionDto.ContentBody, additionalValues, htmlEncode: false),
                ContentTitle = cmsVersionDto.ContentTitle
            };

            return model;
        }

        public static MvcHtmlString CmsRegionWithAdditionalValues(this HtmlHelper helper, CmsRegionEnum cmsRegion, IDictionary<string, string> additionalValues)
        {
            IContentApplicationService contentApplicationService = DependencyResolver.Current.GetService<IContentApplicationService>();
            string contentBody = contentApplicationService.GetCurrentVersionAsync(cmsRegion).Result.ContentBody;

            return new MvcHtmlString(ReplacementUtility.Replace(contentBody, additionalValues, htmlEncode: false));
        }

        public static string TextRegion(this HtmlHelper helper, string textRegion)
        {
            return helper.TextRegion(textRegion, null);
        }

        public static string TextRegion(this HtmlHelper helper, string textRegion, IDictionary<string, string> additionalValues)
        {
            string text = LocalizationHelper.GetLocalizedString(textRegion, additionalValues);

            return text;
        }

        public static string TextRegion(this HtmlHelper helper, string textRegion, string replacementTag, string replacementValue)
        {
            string text = LocalizationHelper.GetLocalizedString(textRegion, new Dictionary<string, string> {{replacementTag, replacementValue}});

            return text;
        }
    }
}
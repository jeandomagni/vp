﻿namespace Ivh.Common.Web.HtmlHelpers
{
    using System.Web;
    using System.Web.Mvc;

    public static partial class HtmlHelperExtensions
    {
        public static HtmlString DisabledIf(this HtmlHelper html, bool condition)
        {
            return new HtmlString(condition ? "disabled=\"disabled\"" : "");
        }
    }
}
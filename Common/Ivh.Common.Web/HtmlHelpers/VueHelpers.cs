﻿namespace Ivh.Common.Web.HtmlHelpers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Linq.Expressions;
    using System.Web.Mvc;
    using System.Web.Mvc.Html;
    using System.Web.Routing;
    using Base.Constants;
    using Base.Utilities.Extensions;
    using Extensions;
    using Utilities;
    using VisitPay.Constants;

    public static partial class HtmlHelperExtensions
    {
        public static MvcHtmlString VueStatesDropDownListFor<TModel, TProperty>(this HtmlHelper<TModel> helper, Expression<Func<TModel, TProperty>> expression, bool withBinding = true, object htmlAttributes = null, params string[] vueAttributes)
        {
            RouteValueDictionary attrs = new RouteValueDictionary();
            MergeAttributes(attrs, htmlAttributes, vueAttributes);

            if (!attrs.ContainsKey("class"))
            {
                attrs.Add("class", "form-control");
            }

            if (withBinding)
            {
                AddModelAttribute(attrs, helper, expression);
            }

            return helper.DropDownListFor(expression, StatesListItems, $"--{LocalizationHelper.GetLocalizedString(TextRegionConstants.State)}--", attrs);
        }

        public static MvcHtmlString VueCheckBoxFor<TModel>(this HtmlHelper<TModel> helper, Expression<Func<TModel, bool>> expression, object htmlAttributes, params string[] vueAttributes)
        {
            return helper.VueCheckBoxFor(expression, true, htmlAttributes, vueAttributes);
        }

        public static MvcHtmlString VueCheckBoxFor<TModel>(this HtmlHelper<TModel> helper, Expression<Func<TModel, bool>> expression, bool withBinding = true, object htmlAttributes = null, params string[] vueAttributes)
        {
            RouteValueDictionary attrs = new RouteValueDictionary();
            MergeAttributes(attrs, htmlAttributes, vueAttributes);

            if (withBinding)
            {
                AddModelAttribute(attrs, helper, expression);
            }

            return helper.CheckBoxFor(expression, attrs);
        }

        public static MvcHtmlString VueDropDownListFor<TModel, TProperty>(this HtmlHelper<TModel> helper, Expression<Func<TModel, TProperty>> expression, IEnumerable<SelectListItem> selectList, bool withBinding = true, object htmlAttributes = null, params string[] vueAttributes)
        {
            ModelMetadata metadata = ModelMetadata.FromLambdaExpression(expression, helper.ViewData);

            string optionLabel = null;
            if (!string.IsNullOrEmpty(metadata.Watermark))
            {
                optionLabel = metadata.Watermark.ReplaceClientValues();
            }

            return helper.VueDropDownListFor(expression, selectList, optionLabel, withBinding, htmlAttributes, vueAttributes);
        }

        public static MvcHtmlString VueDropDownListFor<TModel, TProperty>(this HtmlHelper<TModel> helper, Expression<Func<TModel, TProperty>> expression, IEnumerable<SelectListItem> selectList, string optionLabel, bool withBinding = true, object htmlAttributes = null, params string[] vueAttributes)
        {
            RouteValueDictionary attrs = new RouteValueDictionary();
            MergeAttributes(attrs, htmlAttributes, vueAttributes);

            if (!attrs.ContainsKey("class"))
            {
                attrs.Add("class", "form-control");
            }

            if (withBinding)
            {
                AddModelAttribute(attrs, helper, expression);
            }

            optionLabel = string.IsNullOrWhiteSpace(optionLabel) ? "-" : optionLabel;

            return helper.DropDownListFor(expression, selectList, optionLabel, attrs);
        }

        public static MvcHtmlString VueHiddenFor<TModel, TProperty>(this HtmlHelper<TModel> helper, Expression<Func<TModel, TProperty>> expression, bool withBinding = true, object htmlAttributes = null, params string[] vueAttributes)
        {
            RouteValueDictionary attrs = new RouteValueDictionary();
            MergeAttributes(attrs, htmlAttributes, vueAttributes);

            if (withBinding)
            {
                AddModelAttribute(attrs, helper, expression);
            }

            return helper.HiddenFor(expression, attrs);
        }

        public static MvcHtmlString VuePasswordFor<TModel, TProperty>(this HtmlHelper<TModel> helper, Expression<Func<TModel, TProperty>> expression, object htmlAttributes = null, params string[] vueAttributes)
        {
            return helper.VuePasswordFor(expression, true, htmlAttributes, vueAttributes);
        }

        public static MvcHtmlString VuePasswordFor<TModel, TProperty>(this HtmlHelper<TModel> helper, Expression<Func<TModel, TProperty>> expression, bool withBinding = true, object htmlAttributes = null, params string[] vueAttributes)
        {
            ModelMetadata metadata = ModelMetadata.FromLambdaExpression(expression, helper.ViewData);

            RouteValueDictionary attrs = new RouteValueDictionary();
            MergeAttributes(attrs, htmlAttributes, vueAttributes);

            if (!string.IsNullOrEmpty(metadata.Watermark) && !attrs.ContainsKey("placeholder"))
            {
                attrs.Add("placeholder", metadata.Watermark.ReplaceClientValues());
            }

            if (!attrs.ContainsKey("class"))
            {
                attrs.Add("class", "form-control");
            }

            attrs.Add("autocomplete", "off");

            if (withBinding)
            {
                AddModelAttribute(attrs, helper, expression);
            }

            return helper.PasswordFor(expression, attrs);
        }

        public static MvcHtmlString VueTextBoxFor<TModel, TProperty>(this HtmlHelper<TModel> helper, Expression<Func<TModel, TProperty>> expression, object htmlAttributes, params string[] vueAttributes)
        {
            return helper.VueTextBoxFor(expression, true, htmlAttributes, vueAttributes);
        }

        public static MvcHtmlString VueTextBoxFor<TModel, TProperty>(this HtmlHelper<TModel> helper, Expression<Func<TModel, TProperty>> expression, bool withBinding = true, object htmlAttributes = null, params string[] vueAttributes)
        {
            ModelMetadata metadata = ModelMetadata.FromLambdaExpression(expression, helper.ViewData);

            RouteValueDictionary attrs = new RouteValueDictionary();
            MergeAttributes(attrs, htmlAttributes, vueAttributes);

            if (!string.IsNullOrEmpty(metadata.Watermark) && !attrs.ContainsKey("placeholder"))
            {
                attrs.Add("placeholder", metadata.Watermark.ReplaceClientValues());
            }

            if (!attrs.ContainsKey("class"))
            {
                attrs.Add("class", "form-control");
            }

            if (withBinding)
            {
                AddModelAttribute(attrs, helper, expression);
            }

            return helper.TextBoxFor(expression, attrs);
        }

        public static MvcHtmlString VueSsnBoxFor<TModel, TProperty>(this HtmlHelper<TModel> helper, Expression<Func<TModel, TProperty>> expression, params string[] vueAttributes)
        {
            IList<string> vueAttributesList = new List<string>(vueAttributes);
            vueAttributesList.Add("v-ssn4");
            vueAttributes = vueAttributesList.ToArray();

            return helper.VueTextBoxFor(expression, true, Ssn4Attributes, vueAttributes);
        }

        private static void MergeAttributes(RouteValueDictionary attrs, object htmlAttributes, string[] vueAttributes)
        {
            if (htmlAttributes != null)
            {
                HtmlHelper.AnonymousObjectToHtmlAttributes(htmlAttributes).ToList().ForEach(x => attrs.Add(x.Key, x.Value));
            }

            if (vueAttributes != null)
            {
                foreach (string vueAttribute in vueAttributes)
                {
                    string[] parts = vueAttribute?.Split('=') ?? new string[0];
                    if (parts.Length == 0)
                    {
                        continue;
                    }

                    string key = parts[0].TrimNullSafe();
                    string value = parts.Length > 1 ? parts[1].TrimNullSafe() : string.Empty;

                    attrs.Add(key, value);
                }
            }
        }

        private static void AddModelAttribute<TModel, TProperty>(RouteValueDictionary attrs, HtmlHelper<TModel> helper, Expression<Func<TModel, TProperty>> expression)
        {
            if (!attrs.ContainsKey("v-model") && !attrs.ContainsKey("v-model.lazy"))
            {
                attrs.Add("v-model.lazy", helper.NameFor(expression).ToString());
            }
        }
    }
}

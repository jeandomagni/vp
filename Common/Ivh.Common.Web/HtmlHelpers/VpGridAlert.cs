﻿namespace Ivh.Common.Web.HtmlHelpers
{
    using System.Web.Mvc;

    public static partial class HtmlHelperExtensions
    {
        public static MvcHtmlString GridAlert(this HtmlHelper helper, string id, string text, object htmlAttributes = null, bool visible = false)
        {
            TagBuilder div = new TagBuilder("div");
            div.AddCssClass("alert");
            div.AddCssClass("alert-light");

            if (!visible)
            {
                div.AddCssClass("collapse"); // todo: this just keeps everything working but probably should get refactored
            }

            if (htmlAttributes != null)
                div.MergeAttributes(HtmlHelper.AnonymousObjectToHtmlAttributes(htmlAttributes));

            div.InnerHtml = text;
            if (!string.IsNullOrEmpty(id))
                div.MergeAttribute("id", id);

            return new MvcHtmlString(div.ToString(TagRenderMode.Normal));
        }
    }
}
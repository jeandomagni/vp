﻿namespace Ivh.Common.Web.HtmlHelpers
{
    using System;
    using System.Text;
    using System.Web.Mvc;

    public static partial class HtmlHelperExtensions
    {
        public static IDisposable BeginGridFilter(this HtmlHelper helper, string id = null, object htmlAttributes = null, bool showRunResults = true)
        {
            TagBuilder form = new TagBuilder("form");
            form.AddCssClass("jqGridFilter");
            form.AddCssClass("container");
            form.MergeAttribute("autocomplete", "off");

            if (htmlAttributes != null)
                form.MergeAttributes(HtmlHelper.AnonymousObjectToHtmlAttributes(htmlAttributes));

            if (!string.IsNullOrEmpty(id))
                form.MergeAttribute("id", id);

            helper.ViewContext.Writer.Write(form.ToString(TagRenderMode.StartTag));

            if (showRunResults)
            {
                TagBuilder div = new TagBuilder("div");
                div.AddCssClass("run-results");
                div.AddCssClass("collapse");
                div.MergeAttribute("data-bind", "css: { collapse: RunResults() == false }");
                div.InnerHtml = "Click Run to update your results";

                helper.ViewContext.Writer.Write(div.ToString(TagRenderMode.Normal));
            }

            return new DisposableElement(helper, new StringBuilder(form.ToString(TagRenderMode.EndTag)));
        }
    }
}
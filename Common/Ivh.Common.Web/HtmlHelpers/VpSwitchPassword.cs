﻿namespace Ivh.Common.Web.HtmlHelpers
{
    using System.Collections.Generic;
    using System.Web.Mvc;
    using System.Web.Routing;
    using Ivh.Application.Content.Common.Interfaces;
    using Ivh.Common.Base.Constants;
    using VisitPay.Constants;

    public static partial class HtmlHelperExtensions
    {
        public static MvcHtmlString VpSwitchPassword(this HtmlHelper helper, object htmlAttributes = null, params string[] vueAttributes)
        {
            //Get localized text
            IContentApplicationService contentApplicationService = DependencyResolver.Current.GetService<IContentApplicationService>();
            string passwordSwitchShowText = contentApplicationService.GetTextRegion(TextRegionConstants.PasswordSwitchShow);
            string passwordSwitchHideText = contentApplicationService.GetTextRegion(TextRegionConstants.PasswordSwitchHide);

            TagBuilder tagBuilder = new TagBuilder("a");
            tagBuilder.MergeAttribute("id", "btnSwitchPassword");
            tagBuilder.MergeAttribute("href", "javascript:;", true);
            tagBuilder.MergeAttribute("title", passwordSwitchShowText);
            tagBuilder.MergeAttribute("tabindex", "-1", true);
            tagBuilder.MergeAttribute("data-switch-hide", passwordSwitchHideText);
            tagBuilder.MergeAttribute("data-switch-show", passwordSwitchShowText);
            tagBuilder.AddCssClass("switch-password");

            RouteValueDictionary attrs = new RouteValueDictionary();
            MergeAttributes(attrs, htmlAttributes, vueAttributes);
            foreach (KeyValuePair<string, object> attr in attrs)
            {
                tagBuilder.MergeAttribute(attr.Key, attr.Value.ToString(), true);
            }

            tagBuilder.SetInnerText(passwordSwitchShowText);

            return new MvcHtmlString(tagBuilder.ToString(TagRenderMode.Normal));
        }
    }
}
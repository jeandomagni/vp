﻿namespace Ivh.Common.Web.HtmlHelpers
{
    using System.Linq;
    using System.Web.Mvc;
    using Models.Payment;

    public static partial class HtmlHelperExtensions
    {
        public static MvcHtmlString MobilePaymentMenuButton(this HtmlHelper helper, PaymentMenuItemViewModel paymentMenuItem)
        {
            // button
            TagBuilder button = new TagBuilder("button");
            button.AddCssClass("btn");
            button.AddCssClass("btn-grid-link"); 

            // button attributes
            if (paymentMenuItem.PaymentOptionId.HasValue)
            {
                button.MergeAttribute("onclick", $"window.location.href = '/mobile/Payment/ArrangePayment?paymentOption={paymentMenuItem.PaymentOptionId}'");
            }

            if (!paymentMenuItem.IsEnabled)
            {
                button.MergeAttribute("disabled", "disabled");
            }

            // button text
            button.InnerHtml += new TagBuilder("span") { InnerHtml = paymentMenuItem.Title }.ToString(TagRenderMode.Normal);

            if (paymentMenuItem.ShowTextNotAvailable)
            {
                TagBuilder tagBuilder = new TagBuilder("span") {InnerHtml = paymentMenuItem.TextNotAvailable};
                tagBuilder.AddCssClass("not-available");
                
                button.InnerHtml += tagBuilder.ToString(TagRenderMode.Normal);
            }
            
            // icon
            if (!string.IsNullOrEmpty(paymentMenuItem.IconCssClass))
            {
                TagBuilder icon = new TagBuilder("em");
                icon.AddCssClass(paymentMenuItem.IconCssClass);
                button.InnerHtml += icon.ToString(TagRenderMode.Normal);
            }

            if (paymentMenuItem.PaymentMenuItems.Any())
            {
                button.AddCssClass("parent-item");
            }

            return new MvcHtmlString(button.ToString());
        }
    }
}
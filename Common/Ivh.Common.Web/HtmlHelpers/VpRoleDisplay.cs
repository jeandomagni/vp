﻿namespace Ivh.Common.Web.HtmlHelpers
{
    using System.Linq;
    using System.Security.Principal;
    using System.Web.Mvc;

    public static partial class HtmlHelperExtensions
    {
        public static MvcHtmlString VpRoleHtml(this HtmlHelper helper, string text, IPrincipal user, params string[] roles)
        {
            if (roles == null || !roles.Any())
                return null;

            return roles.Any(user.IsInRole) ? new MvcHtmlString(text) : null;
        }
    }
}
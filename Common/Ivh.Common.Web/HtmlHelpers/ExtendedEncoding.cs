﻿namespace Ivh.Common.Web.HtmlHelpers
{
    using System.Collections.Generic;
    using System.Web;
    using Base.Utilities.Helpers;
    using HtmlHelper = System.Web.Mvc.HtmlHelper;

    public static partial class HtmlHelperExtensions
    {
        /// <summary>
        /// A fuller html encoding than the built in method, will encode a large number of characters ignored by the built in method including apostrophes.
        /// </summary>
        /// <param name="helper"></param>
        /// <param name="html"></param>
        /// <returns></returns>
        public static IHtmlString EncodeFull(this HtmlHelper helper, string html)
        {
            string result = helper.Encode(html);

            return new HtmlString(ReplacementUtility.EncodeFull(result));
        }
    }
}
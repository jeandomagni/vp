﻿namespace Ivh.Common.Web.Cookies
{
    using System.Threading.Tasks;
    using System.Web;
    using Ivh.Application.Core.Common.Dtos;

    public interface IWebCookieFacade : ICookieFacade
    {
        Task<bool> SetSsoResponseDtoAsync(HttpContext context, SsoResponseDto dto);
        Task<SsoResponseDto> GetSsoResponseDtoAsync(HttpContext context);

    }
}
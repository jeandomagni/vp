﻿namespace Ivh.Common.Web.Cookies
{
    using System;
    using System.Web;

    public class CookieFactory
    {
        private const string MaxAge = "max-age";
        public static HttpCookie GetCookie(string name, string value = null, TimeSpan? expires = null, bool secure = true, bool httpOnly = true, bool shareable = false, bool persistent = false)
        {
            HttpCookie httpCookie = new HttpCookie(name, value)
            {
                Secure = secure,
                HttpOnly = httpOnly,
                Shareable = shareable
            };

#if DEBUG
            httpCookie.Secure = false;
#endif


            if (persistent)
            {
                httpCookie.Expires = DateTime.UtcNow.Add(expires ?? TimeSpan.FromTicks(0));
                TimeSpan maxAge = httpCookie.Expires - DateTime.UtcNow;
                httpCookie.Values.Add(MaxAge, $"{(int)maxAge.TotalSeconds}");
            }

            return httpCookie;
        }
    }
}

﻿namespace Ivh.Common.Web.Cookies
{
    using System;
    using System.Linq;
    using System.Threading.Tasks;
    using System.Web;
    using Common.Cache;

    public class CookieFacadeBase
    {
        private readonly IDistributedCache _distributedCache;

        public CookieFacadeBase(IDistributedCache distributedCache)
        {
            this._distributedCache = distributedCache;
        }

        protected async Task<T> GetDistributedCacheViaCookieValue<T>(HttpContext context, string key)
        {
            if (!context.Response.Cookies.AllKeys.Contains(key) && !context.Request.Cookies.AllKeys.Contains(key))
            {
                return default(T);
            }

            HttpCookie check = this.FindCookie(context, key);

            if (check == null)
                return default(T);

            T cacheValue = default(T);
            if (check.Value != null)
            {
                cacheValue = await this._distributedCache.GetObjectAsync<T>(check.Value).ConfigureAwait(true);
                if (cacheValue == null)
                {
                    return default(T);
                }
            }

            return (T)cacheValue;
        }

        private HttpCookie FindCookie(HttpContext context, string key)
        {
            HttpCookie check = null;
            if (context.Response.Cookies.AllKeys.Contains(key))
            {
                check = context.Response.Cookies[key];
            }
            else if (context.Request.Cookies.AllKeys.Contains(key))
            {
                check = context.Request.Cookies[key];
            }
            return check;
        }

        protected async Task<bool> SetDistributedCacheViaCookieValue(HttpContext context, string key, object value, int expirationInMinutes)
        {
            HttpCookie aCookie = CookieFactory.GetCookie(key);
            aCookie.Value = this.GenerateCookieKey(Guid.NewGuid().ToString());

            //If the value is null remove it
            if (value == null)
            {
                HttpCookie check = this.FindCookie(context, key);
                if (check != null)
                {
                    aCookie.Value = check.Value;
                    aCookie.Expires = DateTime.UtcNow.AddDays(-1);
                    context.Response.Cookies.Add(aCookie);
                }
                return await this._distributedCache.RemoveAsync(aCookie.Value).ConfigureAwait(true);
            }

            if (await this._distributedCache.SetObjectAsync(aCookie.Value, value, expirationInMinutes*60).ConfigureAwait(true))
            {
                aCookie.Expires = DateTime.UtcNow.AddMinutes(expirationInMinutes);
                context.Response.Cookies.Add(aCookie);
                return true;
            }
            return false;
        }

        private string GenerateCookieKey(string baseString)
        {
            return $"{baseString}-Cookie";
        }
    }
}
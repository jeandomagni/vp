﻿namespace Ivh.Common.Web.Cookies
{
    using System;

    public interface IGuestPayCookieFacade : ICookieFacade
    {
        int AuthenticationAttempts { get; }
        bool IncrementAuthenticationAttempts(int maxValue, DateTime expiration);
        void ResetAuthenticationAttempts();
    }
}
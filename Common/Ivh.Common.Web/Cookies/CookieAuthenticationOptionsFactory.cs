﻿namespace Ivh.Common.Web.Cookies
{
    using System;
    using System.Threading.Tasks;
    using Microsoft.AspNet.Identity;
    using Microsoft.Owin;
    using Microsoft.Owin.Security.Cookies;

    public static class CookieAuthenticationOptionsFactory
    {
        public static CookieAuthenticationOptions GetCookieAuthenticationOptions(
            string cookieName,
            string loginPath,
            TimeSpan validateInterval,
            Func<CookieValidateIdentityContext, Task> onValidateIdentity,
            Action<CookieApplyRedirectContext> onApplyRedirect = null,
            string defaultAuthenticationType = DefaultAuthenticationTypes.ApplicationCookie)
        {
            return new CookieAuthenticationOptions
            {
                CookieName = cookieName,
                LoginPath = new PathString(loginPath),
                Provider = new CookieAuthenticationProvider
                {
                    // Enables the application to validate the security stamp when the user logs in.
                    // This is a security feature which is used when you change a password or add an external login to your account.  
                    OnValidateIdentity = onValidateIdentity,
                    OnApplyRedirect = onApplyRedirect ?? (x => { })
                },
                ExpireTimeSpan = validateInterval,
                AuthenticationType = defaultAuthenticationType,
                SlidingExpiration = true,
                CookieHttpOnly = true,
                CookieSecure = CookieSecureOption.SameAsRequest,
            };
        }
    }
}
﻿namespace Ivh.Common.Web.Cookies
{
    public static class CookieNames
    {
        public const string SessionCookieName = "ASP.NET_SessionId";
        public const string SsoInvalidateActiveSessionCookieName = "SsoInvalidateActiveSession";

        private const string RequestVerificationTokenPrefix = "__RequestVerificationToken-";
        private const string AuthenticationSuffix = "Authentication";

        public static class Patient
        {
            public const string AntiForgeryString = CookieNames.RequestVerificationTokenPrefix + "Patient";
            public const string Authentication = "Patient" + CookieNames.AuthenticationSuffix;
        }

        public static class Client
        {
            public const string AntiForgeryString = CookieNames.RequestVerificationTokenPrefix + "Client";
            public const string Authentication = "Client" + CookieNames.AuthenticationSuffix;
        }

        public static class GuestPay
        {
            public const string AntiForgeryString = CookieNames.RequestVerificationTokenPrefix + "GuestPay";
            public const string Authentication = "GuestPay" + CookieNames.AuthenticationSuffix;
        }

        public static class QATools
        {
            public const string AntiForgeryString = CookieNames.RequestVerificationTokenPrefix + "QATools";
            public const string Authentication = "QATools" + CookieNames.AuthenticationSuffix;
        }

        public static class SendMail
        {
            public const string AntiForgeryString = CookieNames.RequestVerificationTokenPrefix + "SendMail";
            public const string Authentication = "SendMail" + CookieNames.AuthenticationSuffix;
        }

        public static class ClientDataApi
        {
            public const string AntiForgeryString = CookieNames.RequestVerificationTokenPrefix + "ClientDataApi";
            public const string Authentication = "ClientDataApi" + CookieNames.AuthenticationSuffix;
        }

        public static class AuthorizationServer
        {
            public const string AntiForgeryString = CookieNames.RequestVerificationTokenPrefix + "AuthorizationServer";
            public const string Authentication = "AuthorizationServer" + CookieNames.AuthenticationSuffix;
        }
    }
}
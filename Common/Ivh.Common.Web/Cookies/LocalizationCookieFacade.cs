﻿namespace Ivh.Common.Web.Cookies
{
    using System;
    using System.Collections.Specialized;
    using System.Web;
    using Base.Constants;

    public class LocalizationCookieFacade : ILocalizationCookieFacade
    {
        private const string LocalizationCookie = "VisitPay.Localization";
        private const string ValueLocale = "Locale";
        private const string ValueLocaleSetByUser = "LocaleSetByUser";

        public string Locale
        {
            get
            {
                string locale = null;

                HttpCookie cookie = HttpContext.Current.Request.Cookies[LocalizationCookie];
                if (cookie != null)
                {
                    locale = cookie.Values[ValueLocale];
                }

                return locale;
            }
        }

        public bool LocaleWasSetByUser
        {
            get
            {
                bool setByUser = false;

                HttpCookie cookie = HttpContext.Current.Request.Cookies[LocalizationCookie];
                if (cookie != null)
                {
                    bool.TryParse(cookie.Values[ValueLocaleSetByUser], out setByUser);
                }

                return setByUser;
            }
        }

        public void SetLocale(string locale, bool explicitlySetByUser = false)
        {
            HttpCookie cookie = HttpContext.Current.Request.Cookies[LocalizationCookie] ?? CookieFactory.GetCookie(LocalizationCookie);
            cookie.Expires = DateTime.UtcNow.AddDays(SystemConstants.LocalizationCookieExpirationDays);

            //Set the cookie values
            cookie.Values[ValueLocale] = locale;
            cookie.Values[ValueLocaleSetByUser] = explicitlySetByUser.ToString();

            HttpContext.Current.Response.SetCookie(cookie);
        }
    }
}

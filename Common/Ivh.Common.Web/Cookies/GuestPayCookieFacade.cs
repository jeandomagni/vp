﻿namespace Ivh.Common.Web.Cookies
{
    using System;
    using System.Web;

    public class GuestPayCookieFacade : IGuestPayCookieFacade
    {
        private const string AuthenticationAttemptsCookie = "GuestPay.Authentication";

        public int AuthenticationAttempts
        {
            get
            {
                int attempts = 0;

                HttpCookie cookie = HttpContext.Current.Request.Cookies[AuthenticationAttemptsCookie];
                if (cookie != null)
                {
                    int.TryParse(cookie.Value, out attempts);
                }

                return attempts;
            }
        }

        public bool IncrementAuthenticationAttempts(int maxValue, DateTime expiration)
        {
            int attempts = this.AuthenticationAttempts;

            attempts = attempts + 1;

            this.SetAuthenticationAttempts(attempts, expiration);

            return attempts >= maxValue;
        }

        public void ResetAuthenticationAttempts()
        {
            this.SetAuthenticationAttempts(0, DateTime.UtcNow.AddYears(-1));
        }

        private void SetAuthenticationAttempts(int attempts, DateTime expiration)
        {
            HttpCookie cookie = HttpContext.Current.Request.Cookies[AuthenticationAttemptsCookie] ?? CookieFactory.GetCookie(AuthenticationAttemptsCookie);
            cookie.Expires = expiration;
            cookie.Value = attempts.ToString();

            HttpContext.Current.Response.SetCookie(cookie);
        }
    }
}
﻿namespace Ivh.Common.Web.Cookies
{
    using System.Threading.Tasks;
    using System.Web;
    using Ivh.Application.Core.Common.Dtos;
    using Common.Cache;

    public class WebCookieFacade : CookieFacadeBase, IWebCookieFacade
    {
        private const string SsoResponseDtoKey = "SsoResponseDto";
        private const int SsoResponseDtoTimeToKeepMinutes = 30;

        public WebCookieFacade(IDistributedCache distributedCache) : base(distributedCache)
        {
            
        }

        public async Task<SsoResponseDto> GetSsoResponseDtoAsync(HttpContext context)
        {
            return await this.GetDistributedCacheViaCookieValue<SsoResponseDto>(context, SsoResponseDtoKey);
        }

        public async Task<bool> SetSsoResponseDtoAsync(HttpContext context, SsoResponseDto dto)
        {
            return await this.SetDistributedCacheViaCookieValue(context, SsoResponseDtoKey, dto, SsoResponseDtoTimeToKeepMinutes);
        }
    }
}
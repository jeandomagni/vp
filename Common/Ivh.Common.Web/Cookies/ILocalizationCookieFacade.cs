﻿namespace Ivh.Common.Web.Cookies
{
    using System;

    public interface ILocalizationCookieFacade : ICookieFacade
    {
        string Locale { get; }
        bool LocaleWasSetByUser { get; }
        void SetLocale(string locale, bool explicitlySetByUser = false);
    }
}

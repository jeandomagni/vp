﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace Ivh.Common.Web.Attributes
{
    using System.Reflection;
    using Utilities;

    public sealed class NotEqualAttribute : ValidationAttribute, IClientValidatable
    {
        public string OtherProperty { get; private set; }
        private readonly string _errorMessage;

        public NotEqualAttribute(string otherProperty, string textRegionErrorMessage)
        {
            this.OtherProperty = otherProperty;
            this._errorMessage = textRegionErrorMessage;
        }

        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            PropertyInfo property = validationContext.ObjectType.GetProperty(this.OtherProperty);
            if (property == null)
                return null;

            object otherValue = property.GetValue(validationContext.ObjectInstance, null);
            string errorMessageToUse = LocalizationHelper.GetLocalizedString(this._errorMessage);
            return Equals(value, otherValue) ? new ValidationResult(errorMessageToUse) : null;
        }

        public IEnumerable<ModelClientValidationRule> GetClientValidationRules(ModelMetadata metadata, ControllerContext context)
        {
            string errorMessageToUse = LocalizationHelper.GetLocalizedString(this._errorMessage);
            ModelClientValidationRule rule = new ModelClientValidationRule
            {
                ErrorMessage = errorMessageToUse,
                ValidationType = "notequalto",
            };
            rule.ValidationParameters["other"] = this.OtherProperty;
            yield return rule;
        }
    }
}
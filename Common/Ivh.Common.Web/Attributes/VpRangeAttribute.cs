﻿namespace Ivh.Common.Web.Attributes
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.Web.Mvc;
    using Utilities;

    public class VpRangeAttribute : RangeAttribute, IClientValidatable
    {
        private readonly string _errorMessage;

        public VpRangeAttribute(Type type, string minimum, string maximum, string textRegionErrorMessage) : base(type, minimum, maximum)
        {
            this._errorMessage = textRegionErrorMessage;
        }

        public override string FormatErrorMessage(string name)
        {
            return LocalizationHelper.GetLocalizedString(name);
        }

        public IEnumerable<ModelClientValidationRule> GetClientValidationRules(ModelMetadata metadata, ControllerContext context)
        {
            yield return new ModelClientValidationRangeRule(this.FormatErrorMessage(this._errorMessage), this.Minimum, this.Maximum);
        }
    }
}

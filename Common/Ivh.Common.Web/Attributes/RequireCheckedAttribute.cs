﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace Ivh.Common.Web.Attributes
{
    using Utilities;

    public sealed class RequireCheckedAttribute : ValidationAttribute, IClientValidatable
    {
        public override bool IsValid(object value)
        {
            if (value == null || value.GetType() != typeof(bool))
            {
                return false;
            }

            return (bool) value;
        }

        public IEnumerable<ModelClientValidationRule> GetClientValidationRules(ModelMetadata metadata, ControllerContext context)
        {
            yield return new ModelClientValidationRule
            {
                ErrorMessage = LocalizationHelper.GetLocalizedString(this.ErrorMessage), 
                ValidationType = "requirechecked"
            };
        }
    }
}
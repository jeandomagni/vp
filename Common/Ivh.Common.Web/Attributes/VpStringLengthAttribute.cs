﻿namespace Ivh.Common.Web.Attributes
{
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.Web.Mvc;
    using Utilities;

    public class VpStringLengthAttribute : StringLengthAttribute, IClientValidatable
    {
        private readonly string _errorMessage;

        public VpStringLengthAttribute(int maximumLength, string textRegionErrorMessage) : base(maximumLength)
        {
            this._errorMessage = textRegionErrorMessage;
        }

        public override string FormatErrorMessage(string name)
        {
            return LocalizationHelper.GetLocalizedString(name);
        }

        public IEnumerable<ModelClientValidationRule> GetClientValidationRules(ModelMetadata metadata, ControllerContext context)
        {
            yield return new ModelClientValidationStringLengthRule(this.FormatErrorMessage(this._errorMessage), this.MinimumLength, this.MaximumLength);
        }
    }
}

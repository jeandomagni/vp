﻿namespace Ivh.Common.Web.Attributes
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.Reflection;
    using System.Web.Mvc;
    using Extensions;

    public class RequiredIfAttribute : ValidationAttribute, IClientValidatable
    {
        private readonly RequiredAttribute _innerAttribute = new RequiredAttribute();

        public RequiredIfAttribute(string dependentProperty, object targetValue)
        {
            this.DependentProperty = dependentProperty;
            this.TargetValue = targetValue;
        }

        public string DependentProperty { get; set; }
        public object TargetValue { get; set; }

        public virtual IEnumerable<ModelClientValidationRule> GetClientValidationRules(ModelMetadata metadata, ControllerContext context)
        {
            ModelClientValidationRule rule = new ModelClientValidationRule
            {
                ErrorMessage = this.FormatErrorMessage(metadata.GetDisplayName()),
                ValidationType = "requiredif"
            };

            string depProp = this.BuildDependentPropertyId(metadata, context as ViewContext);
            string targetValue = (this.TargetValue ?? "").ToString();
            object value = this.TargetValue;
            if (value is bool)
            {
                targetValue = targetValue.ToLower();
            }

            rule.ValidationParameters.Add("dependentproperty", depProp);
            rule.ValidationParameters.Add("targetvalue", targetValue);

            yield return rule;
        }

        public override string FormatErrorMessage(string name)
        {
            return base.FormatErrorMessage(name).ReplaceClientValues();
        }

        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            Type containerType = validationContext.ObjectInstance.GetType();
            PropertyInfo field = containerType.GetProperty(this.DependentProperty);

            if (field == null)
            {
                return ValidationResult.Success;
            }

            object dependentvalue = field.GetValue(validationContext.ObjectInstance, null);

            if ((dependentvalue != null || this.TargetValue != null) && (dependentvalue == null || !dependentvalue.Equals(this.TargetValue)))
            {
                return ValidationResult.Success;
            }

            return !this._innerAttribute.IsValid(value) ? new ValidationResult(this.ErrorMessage, new[] {validationContext.MemberName}) : ValidationResult.Success;
        }

        private string BuildDependentPropertyId(ModelMetadata metadata, ViewContext viewContext)
        {
            string depProp = viewContext.ViewData.TemplateInfo.GetFullHtmlFieldId(this.DependentProperty);
            string thisField = metadata.PropertyName + "_";
            if (depProp.StartsWith(thisField, StringComparison.Ordinal))
            {
                depProp = depProp.Substring(thisField.Length);
            }

            return depProp;
        }
    }
}
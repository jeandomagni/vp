﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace Ivh.Common.Web.Attributes
{
    public sealed class AgeDateRangeAttribute : ValidationAttribute, IClientValidatable
    {
        public int MinimumAge { get; set; }
        public int MaximumAge { get; set; }

        public override bool IsValid(object value)
        {
            if (value == null)
                return false;

            DateTime date;
            if (!DateTime.TryParse(value.ToString(), out date))
                return false;

            DateTime now = DateTime.UtcNow;
            DateTime maxDate = now.AddYears(-this.MinimumAge);
            DateTime minDate = now.AddYears(-this.MaximumAge);

            return (DateTime.Compare(date, maxDate) <= 0 || DateTime.Compare(date, minDate) >= 0);
        }

        public IEnumerable<ModelClientValidationRule> GetClientValidationRules(ModelMetadata metadata, ControllerContext context)
        {
            ModelClientValidationRule rule = new ModelClientValidationRule
            {
                ErrorMessage = this.ErrorMessageString,
                ValidationType = "agedaterange"
            };

            rule.ValidationParameters.Add("minimumage", this.MinimumAge);
            rule.ValidationParameters.Add("maximumage", this.MaximumAge);

            yield return rule;
        }
    }
}

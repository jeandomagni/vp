﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ivh.Common.Web.Attributes
{
    using System.ComponentModel.DataAnnotations;
    using System.Reflection;
    using System.Web.Mvc;

    public sealed class RequiredIfNotAttribute : ValidationAttribute, IClientValidatable
    {
        private readonly RequiredAttribute _innerAttribute = new RequiredAttribute();

        public string DependentProperty { get; set; }
        public object TargetValue { get; set; }

        public RequiredIfNotAttribute(string dependentProperty, object targetValue)
        {
            this.DependentProperty = dependentProperty;
            this.TargetValue = targetValue;
        }

        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            Type containerType = validationContext.ObjectInstance.GetType();
            PropertyInfo field = containerType.GetProperty(this.DependentProperty);

            if (field == null)
                return ValidationResult.Success;

            object dependentvalue = field.GetValue(validationContext.ObjectInstance, null);

            if ((dependentvalue != null || this.TargetValue != null) && (dependentvalue == null || dependentvalue.Equals(this.TargetValue)))
                return ValidationResult.Success;

            return !this._innerAttribute.IsValid(value) ? new ValidationResult(this.ErrorMessage, new[] { validationContext.MemberName }) : ValidationResult.Success;
        }

        public IEnumerable<ModelClientValidationRule> GetClientValidationRules(ModelMetadata metadata, ControllerContext context)
        {
            ModelClientValidationRule rule = new ModelClientValidationRule()
            {
                ErrorMessage = this.FormatErrorMessage(metadata.GetDisplayName()),
                ValidationType = "requiredifnot",
            };

            string depProp = this.BuildDependentPropertyId(metadata, context as ViewContext);
            string targetValue = (this.TargetValue ?? "").ToString();
            object value = this.TargetValue;
            if (value is bool)
                targetValue = targetValue.ToLower();

            rule.ValidationParameters.Add("dependentproperty", depProp);
            rule.ValidationParameters.Add("targetvalue", targetValue);

            yield return rule;
        }

        private string BuildDependentPropertyId(ModelMetadata metadata, ViewContext viewContext)
        {
            string depProp = viewContext.ViewData.TemplateInfo.GetFullHtmlFieldId(this.DependentProperty);
            string thisField = metadata.PropertyName + "_";
            if (depProp.StartsWith(thisField, StringComparison.Ordinal))
                depProp = depProp.Substring(thisField.Length);

            return depProp;
        }
    }
}
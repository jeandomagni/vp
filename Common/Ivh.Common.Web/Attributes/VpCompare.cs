﻿namespace Ivh.Common.Web.Attributes
{
    using System.Collections.Generic;
    using System.Web.Mvc;
    using Extensions;
    using Utilities;

    public sealed class VpCompare : System.ComponentModel.DataAnnotations.CompareAttribute, IClientValidatable
    {
        private readonly string _errorMessage;

        public VpCompare(string otherProperty, string textRegionErrorMessage) : base(otherProperty)
        {
            this._errorMessage = textRegionErrorMessage;
        }

        public override string FormatErrorMessage(string name)
        {
            return LocalizationHelper.GetLocalizedString(name);
        }

        public IEnumerable<ModelClientValidationRule> GetClientValidationRules(ModelMetadata metadata, ControllerContext context)
        {
            yield return new ModelClientValidationEqualToRule(this.FormatErrorMessage(this._errorMessage), string.Concat("*.", this.OtherProperty));
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace Ivh.Common.Web.Attributes
{
    public sealed class DateRangeCompareAttribute : ValidationAttribute, IClientValidatable
    {
        public bool Required { get; set; }
        public string Date1 { get; set; }
        public string Date2 { get; set; }
        public DateRangeCompareType DateRangeCompareType { get; set; }

        public DateRangeCompareAttribute(bool required, string date1, string date2, DateRangeCompareType dateRangeCompareType)
        {
            this.Required = required;
            this.Date1 = date1;
            this.Date2 = date2;
            this.DateRangeCompareType = dateRangeCompareType;
        }

        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            if (!this.Required && (string.IsNullOrWhiteSpace(value as string) || string.IsNullOrEmpty(value as string)))
                return ValidationResult.Success;

            var date1 = validationContext.ObjectInstance.GetType().GetProperty(this.Date1);
            var date2 = validationContext.ObjectInstance.GetType().GetProperty(this.Date2);

            var date1Value = date1.GetValue(validationContext.ObjectInstance, null);
            var date2Value = date2.GetValue(validationContext.ObjectInstance, null);

            if (value is DateTime && ((date1Value is DateTime) && (date2Value is DateTime)))
            {
                if (this.DateRangeCompareType == DateRangeCompareType.EqualTo)
                {
                    var valid = ((DateTime) date1Value) == ((DateTime) date2Value);
                    if (!valid)
                        return new ValidationResult(this.ErrorMessage);
                }
                else if (this.DateRangeCompareType == DateRangeCompareType.GreaterThanOrEqual)
                {
                    var valid = ((DateTime)date1Value) >= ((DateTime)date2Value);
                    if (!valid)
                        return new ValidationResult(this.ErrorMessage);
                }
                else if (this.DateRangeCompareType == DateRangeCompareType.LessThanOrEqual)
                {
                    var valid = ((DateTime)date1Value) <= ((DateTime)date2Value);
                    if (!valid)
                        return new ValidationResult(this.ErrorMessage);
                }

                return ValidationResult.Success;
            }

            return new ValidationResult(this.ErrorMessage);
        }

        public IEnumerable<ModelClientValidationRule> GetClientValidationRules(ModelMetadata metadata, ControllerContext context)
        {
            var clientValidationRule = new ModelClientValidationRule()
            {
                ErrorMessage = this.ErrorMessage,
                ValidationType = "daterangecompare"
            };

            clientValidationRule.ValidationParameters.Add("required", this.Required);
            clientValidationRule.ValidationParameters.Add("date1", this.Date1);
            clientValidationRule.ValidationParameters.Add("date2", this.Date2);
            clientValidationRule.ValidationParameters.Add("daterangecomparetype", this.DateRangeCompareType);

            return new[] {clientValidationRule};
        }
    }

    public enum DateRangeCompareType
    {
        EqualTo,
        GreaterThanOrEqual,
        LessThanOrEqual
    }
}

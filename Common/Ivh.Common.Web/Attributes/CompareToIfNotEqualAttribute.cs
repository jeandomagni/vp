﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace Ivh.Common.Web.Attributes
{
    using Utilities;

    public sealed class CompareToIfNotEqualAttribute : ValidationAttribute, IClientValidatable
    {
        private string CompareToPropertyName { get; set; }
        private string Property1Name { get; set; }
        private string Property2Name { get; set; }
        private bool CaseSensitive { get; set; }
        private readonly string _errorMessage;

        public CompareToIfNotEqualAttribute(string compareToPropertyName, string property1Name, string property2Name, bool caseSensitive, string textRegionErrorMessage)
        {
            this.CompareToPropertyName = compareToPropertyName;
            this.Property1Name = property1Name;
            this.Property2Name = property2Name;
            this.CaseSensitive = caseSensitive;
            this._errorMessage = textRegionErrorMessage;
        }

        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            var compareTo = validationContext.ObjectType.GetProperty(this.CompareToPropertyName);
            if (compareTo == null)
                return new ValidationResult(string.Format("Unknown property {0}", this.CompareToPropertyName));

            var property1 = validationContext.ObjectType.GetProperty(this.Property1Name);
            if (property1 == null)
                return new ValidationResult(string.Format("Unknown property {0}", this.Property1Name));

            var property2 = validationContext.ObjectType.GetProperty(this.Property2Name);
            if (property2 == null)
                return new ValidationResult(string.Format("Unknown property {0}", this.Property2Name));

            var property1Value = property1.GetValue(validationContext.ObjectInstance, null);
            var property2Value = property2.GetValue(validationContext.ObjectInstance, null);
            var compareToValue = compareTo.GetValue(validationContext.ObjectInstance, null);

            var valid = false;

            if (!this.CaseSensitive)
            {
                property1Value = (property1Value != null) ? property1Value.ToString().ToUpper() : null;
                property2Value = (property2Value != null) ? property2Value.ToString().ToUpper() : null;
                compareToValue = (compareToValue != null) ? compareToValue.ToString().ToUpper() : null;
                value = (value != null) ? value.ToString().ToUpper() : null;
            }

            if (
                (
                    property1Value != null 
                    && property2Value != null 
                    && property1Value.ToString() == property2Value.ToString() 
                    && (value == null || string.IsNullOrEmpty(value.ToString()))
                ) || value?.ToString() == compareToValue?.ToString()
                )
                valid = true;

            string errorMessageToUse = LocalizationHelper.GetLocalizedString(this._errorMessage);
            return (valid) ? ValidationResult.Success : new ValidationResult(errorMessageToUse);
        }

        public IEnumerable<ModelClientValidationRule> GetClientValidationRules(ModelMetadata metadata, ControllerContext context)
        {
            string errorMessageToUse = LocalizationHelper.GetLocalizedString(this._errorMessage);
            var rule = new ModelClientValidationRule
            {
                ErrorMessage = errorMessageToUse,
                ValidationType = "comparetoifnotequal"
            };

            rule.ValidationParameters["comparetoproperty"] = this.CompareToPropertyName;
            rule.ValidationParameters["property1"] = this.Property1Name;
            rule.ValidationParameters["property2"] = this.Property2Name;
            rule.ValidationParameters["casesensitive"] = this.CaseSensitive;

            yield return rule;
        }
    }
}
﻿namespace Ivh.Common.Web.Attributes
{
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.Web.Mvc;
    using Utilities;

    public sealed class RegularExpressionZipCodeAttribute : RegularExpressionAttribute, IClientValidatable
    {
        private readonly string _errorMessage;

        public RegularExpressionZipCodeAttribute(string textRegionErrorMessage) : base(@"^\d{5}(-\d{4})?$")
        {
            this._errorMessage = textRegionErrorMessage;
        }

        public IEnumerable<ModelClientValidationRule> GetClientValidationRules(ModelMetadata metadata, ControllerContext context)
        {
            string errorMessageToUse = LocalizationHelper.GetLocalizedString(this._errorMessage);
            ModelClientValidationRegexRule rule = new ModelClientValidationRegexRule(errorMessageToUse, this.Pattern);
            return new[] { rule };
        }
    }
}
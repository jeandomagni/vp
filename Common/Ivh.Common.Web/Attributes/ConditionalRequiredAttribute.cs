﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace Ivh.Common.Web.Attributes
{
    public sealed class ConditionalRequiredAttribute : ValidationAttribute, IClientValidatable
    {
        private readonly string _dependentPropertyPrefix;
        private readonly string _dependentPropertyName;

        public ConditionalRequiredAttribute(string dependentPropertyPrefix, string dependentPropertyName)
        {
            this._dependentPropertyPrefix = dependentPropertyPrefix;
            this._dependentPropertyName = dependentPropertyName;
        }

        protected override ValidationResult IsValid(object item, ValidationContext validationContext)
        {
            var property = validationContext.ObjectInstance.GetType().GetProperty(this._dependentPropertyName);
            var dependentPropertyValue = property.GetValue(validationContext.ObjectInstance, null);

            if (dependentPropertyValue != null && item == null)
                return new ValidationResult(string.Format(this.ErrorMessageString, validationContext.DisplayName));

            return ValidationResult.Success;
        }

        public IEnumerable<ModelClientValidationRule> GetClientValidationRules(ModelMetadata metadata, ControllerContext context)
        {
            var rule = new ModelClientValidationRule
            {
                ErrorMessage = this.FormatErrorMessage(metadata.GetDisplayName()),
                ValidationType = "conditionalrequired",
            };

            rule.ValidationParameters.Add("requiredpropertyprefix", this._dependentPropertyPrefix);
            rule.ValidationParameters.Add("requiredproperty", this._dependentPropertyName);

            yield return rule;
        }
    }
}

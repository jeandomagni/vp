﻿namespace Ivh.Common.Web.Attributes
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.Reflection;
    using System.Web.Mvc;

    public sealed class DateBetweenAttribute : ValidationAttribute, IClientValidatable
    {
        public DateBetweenAttribute(bool required, string date1, string date2)
        {
            this.Required = required;
            this.Date1 = date1;
            this.Date2 = date2;
        }

        public bool Required { get; set; }
        public string Date1 { get; set; }
        public string Date2 { get; set; }

        public IEnumerable<ModelClientValidationRule> GetClientValidationRules(ModelMetadata metadata, ControllerContext context)
        {
            ModelClientValidationRule clientValidationRule = new ModelClientValidationRule
            {
                ErrorMessage = this.ErrorMessage,
                ValidationType = "datebetween"
            };

            clientValidationRule.ValidationParameters.Add("required", this.Required);
            clientValidationRule.ValidationParameters.Add("date1", this.Date1);
            clientValidationRule.ValidationParameters.Add("date2", this.Date2);

            return new[] {clientValidationRule};
        }

        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            if (!this.Required && (string.IsNullOrWhiteSpace(value as string) || string.IsNullOrEmpty((string) value)))
                return ValidationResult.Success;

            PropertyInfo date1 = validationContext.ObjectInstance.GetType().GetProperty(this.Date1);
            PropertyInfo date2 = validationContext.ObjectInstance.GetType().GetProperty(this.Date2);

            object date1Value = date1.GetValue(validationContext.ObjectInstance, null);
            object date2Value = date2.GetValue(validationContext.ObjectInstance, null);

            DateTime dtValue;
            DateTime dt1;
            DateTime dt2;

            if (DateTime.TryParse(value.ToString(), out dtValue) &&
                DateTime.TryParse(date1Value.ToString(), out dt1) &&
                DateTime.TryParse(date2Value.ToString(), out dt2) &&
                dtValue.CompareTo(dt1) >= 0 && dtValue.CompareTo(dt2) <= 0)
            {
                return ValidationResult.Success;
            }

            return new ValidationResult(this.ErrorMessage);
        }
    }
}
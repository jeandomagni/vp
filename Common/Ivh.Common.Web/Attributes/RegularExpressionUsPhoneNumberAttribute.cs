﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ivh.Common.Web.Attributes
{
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.Web.Mvc;
    using Utilities;

    public sealed class RegularExpressionUsPhoneNumberAttribute : RegularExpressionAttribute, IClientValidatable
    {
        private readonly string _errorMessage;

        public RegularExpressionUsPhoneNumberAttribute(string textRegionErrorMessage) : base(@"^\D?(\d{3})\D?\D?(\d{3})\D?(\d{4})$")
        {
            this._errorMessage = textRegionErrorMessage;
        }

        public IEnumerable<ModelClientValidationRule> GetClientValidationRules(ModelMetadata metadata, ControllerContext context)
        {
            string errorMessageToUse = LocalizationHelper.GetLocalizedString(this._errorMessage);
            ModelClientValidationRegexRule rule = new ModelClientValidationRegexRule(errorMessageToUse, this.Pattern);
            return new[] { rule };
        }
    }
}

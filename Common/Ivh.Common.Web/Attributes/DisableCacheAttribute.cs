﻿namespace Ivh.Common.Web.Attributes
{
    using System;
    using System.Diagnostics;
    using System.Web;
    using System.Web.Mvc;

    public class DisableCacheAttribute : ActionFilterAttribute
    {
        // Uncomment this attribute to avoid having to step through this during debugging
        //[DebuggerStepThrough]
        public override void OnResultExecuting(ResultExecutingContext filterContext)
        {
            filterContext.HttpContext.Response.Cache.SetExpires(DateTime.UtcNow.AddDays(-1));
            filterContext.HttpContext.Response.Cache.SetValidUntilExpires(false);
            filterContext.HttpContext.Response.Cache.SetRevalidation(HttpCacheRevalidation.AllCaches);
            filterContext.HttpContext.Response.Cache.SetCacheability(HttpCacheability.NoCache);
            filterContext.HttpContext.Response.Cache.SetNoStore();

            base.OnResultExecuting(filterContext);
        }
    }
}
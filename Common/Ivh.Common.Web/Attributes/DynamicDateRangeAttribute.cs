﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace Ivh.Common.Web.Attributes
{
    public sealed class DynamicDateRangeAttribute : ValidationAttribute, IClientValidatable
    {
        public bool Required { get; set; }
        public int MinDateDaysFromToday { get; set; }
        public int MaxDateDaysFromToday { get; set; }

        public DynamicDateRangeAttribute(bool required, int minDateDaysFromToday, int maxDateDaysFromToday)
        {
            this.Required = required;
            this.MinDateDaysFromToday = minDateDaysFromToday;
            this.MaxDateDaysFromToday = maxDateDaysFromToday;
        }

        public override bool IsValid(object value)
        {
            if (value == null)
                return false;

            DateTime date;
            if (!DateTime.TryParse(value.ToString(), out date))
                return false;

            DateTime minDate = DateTime.Today.AddDays(this.MinDateDaysFromToday);
            DateTime maxDate = DateTime.Today.AddDays(this.MaxDateDaysFromToday);

            bool isValid = date >= minDate && date <= maxDate;
            return isValid;
        }

        public IEnumerable<ModelClientValidationRule> GetClientValidationRules(ModelMetadata metadata, ControllerContext context)
        {
            ModelClientValidationRule rule = new ModelClientValidationRule
            {
                ErrorMessage = this.ErrorMessageString,
                ValidationType = "dynamicdaterange"
            };

            rule.ValidationParameters.Add("minimumdatedaysfromtoday", this.MinDateDaysFromToday);
            rule.ValidationParameters.Add("maximumdatedaysfromtoday", this.MaxDateDaysFromToday);

            yield return rule;
        }
    }
}

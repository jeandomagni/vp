﻿namespace Ivh.Common.Web.Attributes
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.Web.Mvc;

    public class ValueRangeDoubleAttribute : ValidationAttribute, IClientValidatable
    {
        private readonly double _maxValue;
        private readonly double _minValue;

        public ValueRangeDoubleAttribute(double minValue = double.MinValue, double maxValue = double.MaxValue)
        {
            this._minValue = minValue;
            this._maxValue = maxValue;
            this.ErrorMessage = this.GetErrorMessage();
        }

        public IEnumerable<ModelClientValidationRule> GetClientValidationRules(ModelMetadata metadata, ControllerContext context)
        {
            ModelClientValidationRule rule = new ModelClientValidationRule
            {
                ErrorMessage = this.GetErrorMessage()
            };
            rule.ValidationParameters.Add("min", this._minValue);
            rule.ValidationParameters.Add("max", this._maxValue);
            rule.ValidationType = "range";
            yield return rule;
        }

        public override bool IsValid(object value)
        {
            return Convert.ToDouble(value) >= this._minValue && Convert.ToDouble(value) <= this._maxValue;
        }

        private string GetErrorMessage()
        {
            return string.Format(this.ErrorMessage ?? "Enter a value between {0} and {1}", this._minValue.ToString("N"), this._maxValue.ToString("N"));
        }
    }
}
﻿namespace Ivh.Common.Web.Attributes
{
    using System.ComponentModel;
    using Utilities;

    public class LocalizedDisplayNameAttribute : DisplayNameAttribute
    {
        private readonly string _textRegionDisplayName;

        /// <summary>
        /// Prevents MVC caching from keeping the DisplayName cached after first load. Instead this
        /// will be called and get the localized version every time.
        /// </summary>
        public override string DisplayName
        {
            get
            {
                return LocalizationHelper.GetLocalizedString(this._textRegionDisplayName);
            }
        }

        public LocalizedDisplayNameAttribute(string textRegionDisplayName)
            : base(GetMessageFromResource(textRegionDisplayName))
        {
            this._textRegionDisplayName = textRegionDisplayName;
        }

        private static string GetMessageFromResource(string textRegion)
        {
            return LocalizationHelper.GetLocalizedString(textRegion);
        }
    }
}
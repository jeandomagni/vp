﻿namespace Ivh.Common.Web.Attributes
{
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.Web.Mvc;
    using Extensions;

    public sealed class VpRequiredAttribute : RequiredAttribute, IClientValidatable
    {
        public override string FormatErrorMessage(string name)
        {
            return base.FormatErrorMessage(name).ReplaceClientValues();
        }

        public IEnumerable<ModelClientValidationRule> GetClientValidationRules(ModelMetadata metadata, ControllerContext context)
        {
            yield return new ModelClientValidationRequiredRule(this.FormatErrorMessage(metadata.GetDisplayName()));
        }
    }
}
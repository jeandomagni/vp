﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace Ivh.Common.Web.Attributes
{
    public sealed class RegularExpressionSsn4Attribute : RegularExpressionAttribute, IClientValidatable
    {
        public RegularExpressionSsn4Attribute()
            : base(@"^(?!0000)\d{4}$")
        {
        }

        public IEnumerable<ModelClientValidationRule> GetClientValidationRules(ModelMetadata metadata, ControllerContext context)
        {
            ModelClientValidationRegexRule rule = new ModelClientValidationRegexRule(this.ErrorMessage, this.Pattern);
            return new[] {rule};
        }
    }
}
    
    
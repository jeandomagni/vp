﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Ivh.Common.Web.Utilities;
using Ivh.Common.Web.Interfaces;

namespace Ivh.Common.Web.Attributes
{
    [AttributeUsage(AttributeTargets.Method)]
    public class ObfuscateControllerAttribute : ActionFilterAttribute
    {

        private readonly Obfuscator _obfuscator = new Obfuscator();

        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {

            if (filterContext.ActionParameters.Any())
            {

                //Deobfuscate values passed in as parameters with the DeobfuscateParameter attribute
                IEnumerable<ParameterDescriptor> pars = filterContext.ActionDescriptor.GetParameters()
                    .Where(p => p.GetCustomAttributes(typeof(DeobfuscateParameterAttribute), false).Any());

                foreach (ParameterDescriptor p in pars)
                {
                    ValueProviderResult valueProviderResult = filterContext.Controller.ValueProvider.GetValue(p.ParameterName);
                    string paramValue = valueProviderResult.AttemptedValue;
                    paramValue = this._obfuscator.DeobfuscateText(paramValue, filterContext.HttpContext);
                    filterContext.ActionParameters[p.ParameterName] = paramValue;
                }

                //Deobfuscate objects passed in as parameters that implement IObfuscate (short circuits if IObfuscate is not implemented)
                foreach (KeyValuePair<string, object> arg in filterContext.ActionParameters)
                {
                    this.Deobfuscate(arg.Value);
                }
            }

            base.OnActionExecuting(filterContext);
        }

        public override void OnResultExecuting(ResultExecutingContext filterContext)
        {

            if (filterContext.Result is JsonResult)
            {
                JsonResult jsonResult = (JsonResult)filterContext.Result;
                this.Obfuscate(jsonResult.Data);
            }

            if (filterContext.Result is ViewResult)
            {
                ViewResult viewResult = (ViewResult)filterContext.Result;
                this.Obfuscate(viewResult.Model);
            }

            if (filterContext.Result is PartialViewResult)
            {
                PartialViewResult partialViewResult = (PartialViewResult)filterContext.Result;
                this.Obfuscate(partialViewResult.Model);
            }

            //Other filterContext.Result types can get evaluated here

            base.OnResultExecuting(filterContext);
        }

        /// <summary>
        /// Deobfuscates objects that implement IObfuscate
        /// </summary>
        /// <param name="data">Object that implement IObfuscate</param>
        void Deobfuscate(object data)
        {
            if (data is IObfuscate)
            {
                IObfuscate obj = (IObfuscate)data;
                obj.Deobfuscate();
            }
        }

        /// <summary>
        /// Obfuscates objects that implement IObfuscate
        /// </summary>
        /// <param name="data">Object that implement IObfuscat</param>
        void Obfuscate(object data)
        {
            if (data is IObfuscate)
            {
                IObfuscate obj = (IObfuscate)data;
                obj.Obfuscate();
            }
        }

    }
}

﻿namespace Ivh.Common.Web.Attributes
{
    using System.Web.Mvc;
    using Ivh.Application.Logging.Common.Interfaces;
    using Base.Utilities.Helpers;
    using Domain.Logging.Interfaces;

    public class IvinciHandleErrorAttribute : HandleErrorAttribute, IExceptionFilter
    {
        public override void OnException(ExceptionContext filterContext)
        {
            ILoggingApplicationService logger = DependencyResolver.Current.GetService<ILoggingApplicationService>();
            if (logger != null && filterContext != null && filterContext.Exception != null)
            {
                logger.Fatal(() => $"Exception = {ExceptionHelper.AggregateExceptionToString(filterContext.Exception)}, IsHandled = {filterContext.ExceptionHandled}, ");
            }
            base.OnException(filterContext);
        }
    }
}

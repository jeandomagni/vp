﻿namespace Ivh.Common.Web.Attributes
{
    using System;

    public class MapKoAttribute : Attribute
    {
        public MapKoAttribute(string filename)
        {
            this.Filename = filename;
        }

        public string Filename { get; }
    }
}
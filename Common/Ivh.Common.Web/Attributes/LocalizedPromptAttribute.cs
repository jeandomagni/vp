﻿namespace Ivh.Common.Web.Attributes
{
    using System;
    using System.Web.Mvc;
    using Utilities;

    /// <summary>
    ///     Used to set the "Prompt" attribute property, which is used as a prompt or watermark
    ///     for a UI element
    ///     <para />
    ///     Reference: https://stackoverflow.com/questions/5824124/html5-placeholders-with-net-mvc-3-razor-editorfor-extension
    /// </summary>
    public class LocalizedPromptAttribute : Attribute, IMetadataAware
    {
        private readonly string _prompt;

        public LocalizedPromptAttribute(string textRegionPrompt)
        {
            this._prompt = textRegionPrompt;
        }

        public void OnMetadataCreated(ModelMetadata metadata)
        {
            metadata.Watermark = LocalizationHelper.GetLocalizedString(this._prompt);
        }
    }
}
﻿
namespace Ivh.Common.Web.Attributes
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.ComponentModel.DataAnnotations;
    using System.Reflection;
    using System.Web.Mvc;

    public sealed class TimeSpanCompareAttribute : ValidationAttribute, IClientValidatable
    {
        public bool Required { get; set; }
        public string TimeSpan1 { get; set; }
        public string TimeSpan2 { get; set; }
        public TimeSpanCompareType TimeSpanCompareType { get; set; }

        public TimeSpanCompareAttribute(bool required, string date1, string date2, TimeSpanCompareType timeSpanCompareType)
        {
            this.Required = required;
            this.TimeSpan1 = date1;
            this.TimeSpan2 = date2;
            this.TimeSpanCompareType = timeSpanCompareType;
        }

        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            if (!this.Required && value == null)
                return ValidationResult.Success;

            PropertyInfo timespan1 = validationContext.ObjectInstance.GetType().GetProperty(this.TimeSpan1);
            PropertyInfo timespan2 = validationContext.ObjectInstance.GetType().GetProperty(this.TimeSpan2);

            object timespan1Value = timespan1.GetValue(validationContext.ObjectInstance, null);
            object timespan2Value = timespan2.GetValue(validationContext.ObjectInstance, null);

            if (value is TimeSpan && ((timespan1Value is TimeSpan) && (timespan2Value is TimeSpan)))
            {
                if (this.TimeSpanCompareType == TimeSpanCompareType.EqualTo)
                {
                    bool valid = ((TimeSpan)timespan1Value) == ((TimeSpan)timespan2Value);
                    if (!valid)
                        return new ValidationResult(this.ErrorMessage);
                }
                else if (this.TimeSpanCompareType == TimeSpanCompareType.GreaterThan)
                {
                    bool valid = ((TimeSpan)timespan1Value) > ((TimeSpan)timespan2Value);
                    if (!valid)
                        return new ValidationResult(this.ErrorMessage);
                }
                else if (this.TimeSpanCompareType == TimeSpanCompareType.LessThan)
                {
                    bool valid = ((TimeSpan)timespan1Value) < ((TimeSpan)timespan2Value);
                    if (!valid)
                        return new ValidationResult(this.ErrorMessage);
                }

                return ValidationResult.Success;
            }

            return new ValidationResult(this.ErrorMessage);
        }

        public IEnumerable<ModelClientValidationRule> GetClientValidationRules(ModelMetadata metadata, ControllerContext context)
        {
            ModelClientValidationRule clientValidationRule = new ModelClientValidationRule()
            {
                ErrorMessage = this.ErrorMessage,
                ValidationType = "daterangecompare"
            };

            clientValidationRule.ValidationParameters.Add("required", this.Required);
            clientValidationRule.ValidationParameters.Add("timespan1", this.TimeSpan1);
            clientValidationRule.ValidationParameters.Add("timespan2", this.TimeSpan2);
            clientValidationRule.ValidationParameters.Add("timespancomparetype", this.TimeSpanCompareType);

            return new[] { clientValidationRule };
        }
    }

    public enum TimeSpanCompareType
    {
        EqualTo,
        GreaterThan,
        LessThan
    }
}

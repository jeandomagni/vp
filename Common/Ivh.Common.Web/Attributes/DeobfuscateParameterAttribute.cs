﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ivh.Common.Web.Attributes
{
    [AttributeUsage(AttributeTargets.Parameter)]
    public class DeobfuscateParameterAttribute : System.Attribute
    {
    }
}

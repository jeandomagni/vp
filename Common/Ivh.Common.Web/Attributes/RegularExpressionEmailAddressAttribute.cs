﻿namespace Ivh.Common.Web.Attributes
{
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.Web.Mvc;
    using Utilities;
    using VisitPay.Strings;

    public sealed class RegularExpressionEmailAddressAttribute : RegularExpressionAttribute, IClientValidatable
    {
        private readonly string _errorMessage;

        public RegularExpressionEmailAddressAttribute(string textRegionErrorMessage)
            : base(RegexStrings.EmailAddress)
        {
            this._errorMessage = textRegionErrorMessage;
        }

        public IEnumerable<ModelClientValidationRule> GetClientValidationRules(ModelMetadata metadata, ControllerContext context)
        {
            string errorMessageToUse = LocalizationHelper.GetLocalizedString(this._errorMessage);
            ModelClientValidationRegexRule rule = new ModelClientValidationRegexRule(errorMessageToUse, this.Pattern);
            return new[] {rule};
        }
    }
}
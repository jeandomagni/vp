﻿namespace Ivh.Common.Web.Application
{
    using System;
    using Autofac;
    using DependencyInjection;
    using MassTransit;
    using ServiceBus.Constants;

    /// <summary>
    /// Applications that use the service bus should inherit HttpApplicationBase instead of System.Web.HttpApplication
    /// </summary>
    public class HttpApplicationBase : System.Web.HttpApplication
    {
        private static IBusControl _busControl = null;

        protected virtual void Application_Start()
        {
        }

        protected virtual void Application_BeginRequest(object sender, EventArgs e)
        {
            if (_busControl == null)
            {
                _busControl = IvinciContainer.Instance.Container().Resolve<IBusControl>();
            }
        }

        protected virtual void Application_End(object sender, EventArgs e)
        {
            _busControl?.Stop(TimeSpan.FromSeconds(ServiceBus.StopTimeoutSeconds));
            IvinciContainer.Instance.Container().Dispose();
        }
    }
}
﻿namespace Ivh.Common.Web.Mapping
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web.Mvc;
    using Ivh.Application.Content.Common.Dtos;
    using Ivh.Application.Core.Common.Dtos;
    using Ivh.Application.FinanceManagement.Common.Dtos;
    using Ivh.Application.SecureCommunication.Common.Dtos;
    using AutoMapper;
    using Base.Utilities.Extensions;
    using Base.Utilities.Helpers;
    using Ivh.Application.Base.Common.Dtos;
    using Ivh.Application.Guarantor.Common.Dtos;
    using Models;
    using Models.Account;
    using Models.Consolidate;
    using Models.FinancePlan;
    using Models.Itemization;
    using Models.Monitoring;
    using Models.Payment;
    using Models.Shared;
    using Models.Sso;
    using Models.Statement;
    using Models.Support;
    using Ivh.Application.Monitoring.Common.Dtos;
    using Ivh.Application.User.Common.Dtos;
    using Utilities;
    using VisitPay.Constants;
    using VisitPay.Enums;
    using VisitPay.Strings;
    using Ivh.Common.VisitPay.Utilities.Extensions;
    using Ivh.Domain.User.Entities;

    public abstract class CommonWebMappings : Profile
    {
        protected CommonWebMappings()
        {
            // user date summary
            this.CreateMap<FinancialDataSummaryDto, UserDateSummaryViewModel>()
                .ConstructUsing(src => new UserDateSummaryViewModel(src.IsGracePeriod, src.IsAwaitingStatement, src.NextStatementDate, src.NextPaymentDateForDisplay));

            // arrange payment
            this.CreateMap<PaymentMenuItemDto, PaymentMenuItemViewModel>()
                .ForMember(dest => dest.Description, opts => opts.MapFrom(src => src.UserPaymentOption.CmsVersion == null ? null : src.UserPaymentOption.CmsVersion.ContentBody))
                .ForMember(dest => dest.PaymentOptionId, opts => opts.MapFrom(src => src.UserPaymentOption == null ? (PaymentOptionEnum?) null : src.UserPaymentOption.PaymentOptionId))
                .ForMember(dest => dest.Title, opts => opts.MapFrom(src => src.CmsVersion.ContentBody))
                .ForMember(dest => dest.UserApplicableBalance, opts => opts.MapFrom(src => src.UserPaymentOption == null ? null : src.UserPaymentOption.UserApplicableBalance));

            this.CreateMap<UserPaymentOptionDto, PaymentOptionViewModel>()
                .ForMember(dest => dest.Description, opts => opts.MapFrom(src => src.CmsVersion == null ? null : src.CmsVersion.ContentBody))
                .ForMember(dest => dest.UserApplicableBalanceText, opts => opts.MapFrom(src => src.UserApplicableBalance.HasValue ? src.UserApplicableBalance.Value.FormatCurrency() : null))
                .ForMember(dest => dest.PaymentAmount, opts => opts.MapFrom(src => src.IsAmountReadonly ? src.UserApplicableBalance : (decimal?) null))
                .ForMember(dest => dest.PaymentDate, opts => opts.MapFrom(src => MappingHelper.MapToClientDateText(src.DefaultPaymentDate)))
                .ForMember(dest => dest.DateBoundary, opts => opts.MapFrom(src => MappingHelper.MapToClientDateText(src.DateBoundary)))
                .ForMember(dest => dest.DiscountGuarantorOffer, opts => opts.MapFrom(src => src.DiscountOffer))
                .ForMember(dest => dest.DiscountManagedOffers, opts => opts.Ignore())
                .AfterMap((src, dest) =>
                {
                    dest.DiscountManagedOffers = new List<DiscountOfferViewModel>();
                    foreach (KeyValuePair<VisitPayUserDto, DiscountOfferDto> item in src.ManagedOffers)
                    {
                        DiscountOfferViewModel mapped = Mapper.Map<DiscountOfferViewModel>(item.Value);
                        mapped.FirstNameLastName = item.Key.DisplayFirstNameLastName;
                        dest.DiscountManagedOffers.Add(mapped);
                    }
                });

            this.CreateMap<FinancePlanConfigurationDto, OfflineFinancePlanConfigurationViewModel>()
                .IncludeBase<FinancePlanConfigurationDto, FinancePlanConfigurationViewModel>()
                .ForMember(dest => dest.IsCombined, opts => opts.MapFrom(src => src.IsCombined));

            this.CreateMap<FinancePlanConfigurationDto, FinancePlanConfigurationViewModel>()
                .ForMember(dest => dest.EsignCmsVersionId, opts => opts.MapFrom(src => src.EsignCmsVersionId))
                .ForMember(dest => dest.FinancePlanId, opts => opts.MapFrom(src => src.FinancePlanId))
                .ForMember(dest => dest.InterestRates, opts => opts.MapFrom(src => src.InterestRates))
                .ForMember(dest => dest.MaximumMonthlyPayments, opts => opts.MapFrom(src => src.MaximumMonthlyPayments))
                .ForMember(dest => dest.MinimumPaymentAmount, opts => opts.MapFrom(src => src.MinimumPaymentAmount))
                .ForMember(dest => dest.OfferCalculationStrategy, opts => opts.MapFrom(src => src.OfferCalculationStrategy))
                .ForMember(dest => dest.Terms, opts => opts.MapFrom(src => src.Terms))
                .ForMember(dest => dest.TermsCmsVersionId, opts => opts.MapFrom(src => src.TermsCmsVersionId))
                .ForMember(dest => dest.FinancePlanTypes, opts => opts.MapFrom(src => src.FinancePlanTypes))
                .AfterMap((src, dest) => { dest.Terms = dest.Terms ?? new FinancePlanTermsViewModel(); });

            this.CreateMap<FinancePlanTypeDto, FinancePlanTypeViewModel>();

            this.CreateMap<FinancePlanTermsDto, FinancePlanTermsViewModel>()
                .ForMember(dest => dest.EffectiveDate, opts => opts.MapFrom(src => src.EffectiveDate != default(DateTime) ? src.EffectiveDate.ToString(Format.DateFormat) : null))
                .ForMember(dest => dest.FinalPaymentDate, opts => opts.MapFrom(src => src.FinalPaymentDate != default(DateTime) ? src.FinalPaymentDate.ToString(Format.DateFormat) : null))
                .ForMember(dest => dest.FinancePlanOfferSetTypeId, opts => opts.MapFrom(src => src.FinancePlanOfferSetType == null ? (int?) null : src.FinancePlanOfferSetType.FinancePlanOfferSetTypeId))
                .ForMember(dest => dest.GuarantorPaymentDueDay, opts => opts.MapFrom(src => src.PaymentDueDay))
                .ForMember(dest => dest.NextPaymentDueDate, opts => opts.MapFrom(src => src.NextPaymentDueDate != default(DateTime) ? src.NextPaymentDueDate.ToString(Format.DateFormat) : null))
                .ForMember(dest => dest.PaymentDueDayOrdinal, opts => opts.MapFrom(src => src.PaymentDueDay.ToOrdinalPaymentDueDay(LocalizationHelper.GetLocalizedString(TextRegionConstants.LastDay))));

            // cms
            this.CreateMap<CmsVersionDto, CmsViewModel>();

            // discount offers
            this.CreateMap<DiscountOfferDto, DiscountOfferViewModel>();

            this.CreateMap<DiscountVisitOfferDto, DiscountVisitOfferViewModel>()
                .ForMember(dest => dest.BillingApplicationText, opts => opts.MapFrom(src => MappingHelper.MapVisitBillingApplication(src.PaymentVisit)))
                .ForMember(dest => dest.SourceSystemKeyDisplay, opts => opts.MapFrom(src => !string.IsNullOrEmpty(src.PaymentVisit.SourceSystemKeyDisplay) ? MappingHelper.MapVisitSourceSystemKey(src.PaymentVisit, x => x.SourceSystemKeyDisplay) : MappingHelper.MapVisitSourceSystemKey(src.PaymentVisit)))
                .ForMember(dest => dest.TotalBalance, opts => opts.MapFrom(src => src.TotalBalance.ToString("0.00")));

            // finance plan
            this.CreateMap<FinancePlanDto, FinancePlanDetailsViewModel>()
                .ForMember(dest => dest.CurrentFinancePlanBalance, opts => opts.MapFrom(src => src.CurrentFinancePlanBalance.FormatCurrency()))
                .ForMember(dest => dest.CurrentInterestRate, opts => opts.MapFrom(src => src.CurrentInterestRate.FormatPercentage()))
                .ForMember(dest => dest.FinancePlanOfferId, opts => opts.MapFrom(src => src.FinancePlanOffer.FinancePlanOfferId))
                .ForMember(dest => dest.FinancePlanStatus, opts => opts.MapFrom(src => src.FinancePlanStatus.FinancePlanStatusId))
                .ForMember(dest => dest.InterestAssessed, opts => opts.MapFrom(src => src.InterestAssessed.FormatCurrency()))
                .ForMember(dest => dest.InterestPaidToDate, opts => opts.MapFrom(src => src.InterestPaidToDate.FormatCurrency()))
                .ForMember(dest => dest.LastPaymentDate, opts => opts.MapFrom(src => MappingHelper.MapToFormattedClientDateTimeText(src.LastPaymentDate, "MMM yyyy")))
                .ForMember(dest => dest.OriginatedFinancePlanBalance, opts => opts.MapFrom(src => MappingHelper.MapFinancePlanOriginatedBalance(src)))
                .ForMember(dest => dest.OriginationDate, opts => opts.MapFrom(src => src.OriginationDate.ToDateText()))
                .ForMember(dest => dest.PaymentAmount, opts => opts.MapFrom(src => src.PaymentAmount.FormatCurrency()))
                .ForMember(dest => dest.AmountDue, opts => opts.MapFrom(src => src.AmountDue.FormatCurrency()))
                .ForMember(dest => dest.PastDueAmount, opts => opts.MapFrom(src => src.PastDueAmount.FormatCurrency()))
                .ForMember(dest => dest.AdjustmentsSinceOrigination, opts => opts.MapFrom(src => src.AdjustmentsSinceOrigination.FormatCurrency()))
                .ForMember(dest => dest.HasAdjustments, opts => opts.MapFrom(src => src.AdjustmentsSinceOrigination.HasValue && src.AdjustmentsSinceOrigination != 0))
                .ForMember(dest => dest.PrincipalPaidToDate, opts => opts.MapFrom(src => src.PrincipalPaidToDate.FormatCurrency()))
                .ForMember(dest => dest.StatusDisplay, opts => opts.MapFrom(src => src.FinancePlanStatus.FinancePlanStatusDisplayName))
                .ForMember(dest => dest.Terms, opts => opts.MapFrom(src => MappingHelper.MapFinancePlanTerms(src)));

            this.CreateMap<FinancePlanSummaryDto, FinancePlanSummaryViewModel>()
                .ForMember(dest => dest.FinancePlanDetailsViewModels, opts => opts.MapFrom(src => src.FinancePlans))
                .ForMember(dest => dest.PaymentDueDate, opts => opts.MapFrom(src => src.PaymentDueDate.HasValue ? src.PaymentDueDate.Value.ToString(Format.DateFormat) : ""));

            // finance plan terms
            this.CreateMap<FinancePlanTermsDto, FinancePlanTermsDisplayViewModel>()
                .ForMember(dest => dest.FinalPaymentDate, opts => opts.MapFrom(src => MappingHelper.MapToFormattedClientDateTimeText(src.FinalPaymentDate, Format.MonthYearFormat)))
                .ForMember(dest => dest.InterestRate, opts => opts.MapFrom(src => src.InterestRate.FormatPercentage()))
                .ForMember(dest => dest.MonthlyPaymentAmount, opts => opts.MapFrom(src => src.MonthlyPaymentAmount.FormatCurrency()))
                .ForMember(dest => dest.MonthlyPaymentAmountDecimal, opts => opts.MapFrom(src => src.MonthlyPaymentAmount))
                .ForMember(dest => dest.MonthlyPaymentAmountLast, opts => opts.MapFrom(src => src.MonthlyPaymentAmountLast.FormatCurrency()))
                .ForMember(dest => dest.NextPaymentDueDate, opts => opts.MapFrom(src => src.NextPaymentDueDate.ToDateText()))
                .ForMember(dest => dest.TotalInterest, opts => opts.MapFrom(src => src.TotalInterest.FormatCurrency()))
                .ForMember(dest => dest.MonthlyPaymentAmountOtherPlans, opts => opts.MapFrom(src => src.MonthlyPaymentAmountOtherPlans.FormatCurrency()))
                .ForMember(dest => dest.MonthlyPaymentAmountTotal, opts => opts.MapFrom(src => src.MonthlyPaymentAmountTotal.FormatCurrency()));

            // finance plan offer sets
            this.CreateMap<FinancePlanOfferSetTypeDto, SelectListItem>()
                .ForMember(dest => dest.Text, opts => opts.MapFrom(src => src.Name))
                .ForMember(dest => dest.Value, opts => opts.MapFrom(src => src.FinancePlanOfferSetTypeId));

            // household balance
            this.CreateMap<HouseholdBalanceDto, HouseholdBalanceViewModel>()
                .ForMember(dest => dest.VisitPayUserFullName, opts => opts.MapFrom(src => src.VisitPayUser.DisplayFirstNameLastName))
                .ForMember(dest => dest.TotalBalance, opts => opts.MapFrom(src => src.TotalBalance.FormatCurrency()))
                .ForMember(dest => dest.TotalBalanceDecimal, opts => opts.MapFrom(src => src.TotalBalance))
                .AfterMap((src, dest) =>
                {
                    if (dest.DiscountOffer != null)
                    {
                        dest.DiscountOffer.FirstNameLastName = dest.VisitPayUserFullName;
                    }
                });

            // interest rates
            this.CreateMap<InterestRateDto, InterestRateViewModel>()
                .ForMember(dest => dest.Rate, opts => opts.MapFrom(src => src.Rate.FormatPercentage()))
                .ForMember(dest => dest.IsCharity, opts => opts.MapFrom(src => src.IsCharity.HasValue && src.IsCharity.Value));

            // sso
            this.CreateMap<SsoVisitPayUserSettingDto, SsoProviderWithUserSettingsViewModel>()
                .ForMember(dest => dest.ActionDateTime, opts => opts.MapFrom(src => src.ActionDateTime.ToString(Format.DateTimeFormat)))
                .ForMember(dest => dest.ActionVisitPayUser, opts => opts.MapFrom(src => src.ActionVisitPayUser.DisplayFirstNameLastName))
                .ForMember(dest => dest.IsAccepted, opts => opts.MapFrom(src => src.SsoStatus == SsoStatusEnum.Accepted))
                .ForMember(dest => dest.IsDeclined, opts => opts.MapFrom(src => src.SsoStatus == SsoStatusEnum.Rejected))
                .ForMember(dest => dest.IsIgnored, opts => opts.MapFrom(src => src.IsIgnored))
                .ForMember(dest => dest.SsoProviderId, opts => opts.MapFrom(src => src.SsoProvider.SsoProviderId))
                .ForMember(dest => dest.SsoProviderEnum, opts => opts.MapFrom(src => src.SsoProvider.SsoProviderEnum))
                .ForMember(dest => dest.SsoProviderName, opts => opts.MapFrom(src => src.SsoProvider.SsoProviderDisplayName))
                .ForMember(dest => dest.EnableFromManageSso, opts => opts.MapFrom(src => src.SsoProvider.EnableFromManageSso))
                .ForMember(dest => dest.ShortDescription, opts => opts.MapFrom(src => src.SsoProvider.ShortDescription));

            this.CreateMap<SsoProviderWithUserSettingsDto, SsoProviderWithUserSettingsViewModel>()
                .ForMember(dest => dest.SsoProviderId, opts => opts.MapFrom(src => src.Provider.SsoProviderId))
                .ForMember(dest => dest.SsoProviderEnum, opts => opts.MapFrom(src => src.Provider.SsoProviderEnum))
                .ForMember(dest => dest.SsoProviderName, opts => opts.MapFrom(src => src.Provider.SsoProviderDisplayName))
                .ForMember(dest => dest.EnableFromManageSso, opts => opts.MapFrom(src => src.Provider.EnableFromManageSso))
                .ForMember(dest => dest.ShortDescription, opts => opts.MapFrom(src => src.Provider.ShortDescription))
                .ForMember(dest => dest.SourceSystemKeyLabel, opts => opts.MapFrom(src => src.Provider.SourceSystemKeyLabel))
                .AfterMap((src, dest) => { Mapper.Map(src.UserSettings, dest); });

            // visits
            this.CreateMap<VisitDto, VisitDetailsViewModel>()
                .ForMember(dest => dest.DischargeDate, opts => opts.MapFrom(src => src.VisitDisplayDate))
                .ForMember(dest => dest.SourceSystemKey, opts => opts.MapFrom(src => MappingHelper.MapVisitSourceSystemKey(src)))
                .ForMember(dest => dest.VisitStateFullDisplayName, opts => opts.MapFrom(src => src.CurrentVisitState.ToString()))
                .ForMember(dest => dest.VisitPayUserId, opts => opts.MapFrom(src => src.VPGuarantor.User.VisitPayUserId));
            this.CreateMap<VisitTransactionSummaryDto, VisitDetailsViewModel>()
                .ForMember(dest => dest.SummaryCharges, opts => opts.MapFrom(src => src.Charges))
                .ForMember(dest => dest.SummaryOther, opts => opts.MapFrom(src => src.Other))
                .ForMember(dest => dest.SummaryInsurance, opts => opts.MapFrom(src => src.Insurance))
                .ForMember(dest => dest.SummaryInterest, opts => opts.MapFrom(src => src.Interest))
                .ForMember(dest => dest.SummaryAllPayments, opts => opts.MapFrom(src => src.AllPayments));

            this.CreateMap<GuarantorDto, SelectListItem>()
                .ForMember(dest => dest.Text, opts => opts.MapFrom(x => x.User.DisplayFirstNameLastName))
                .ForMember(dest => dest.Value, opts => opts.MapFrom(x => x.VpGuarantorId));

            this.CreateMap<PaymentAllocationGroupedDto, PaymentAllocationViewModel>()
                .ForMember(dest => dest.AllocationAmount, opts => opts.MapFrom(x => x.AllocationAmount.FormatCurrency()))
                .ForMember(dest => dest.SourceSystemKey, opts => opts.MapFrom(x => MappingHelper.MapVisitSourceSystemKey(x.Visit)))
                .ForMember(dest => dest.SourceSystemKeyDisplay, opts => opts.MapFrom(x => !string.IsNullOrEmpty(x.Visit.SourceSystemKeyDisplay) ? MappingHelper.MapVisitSourceSystemKey(x.Visit, z => z.SourceSystemKeyDisplay) : MappingHelper.MapVisitSourceSystemKey(x.Visit)))
                .ForMember(dest => dest.MatchedSourceSystemKey, opts => opts.MapFrom(x => x.Visit.MatchedSourceSystemKey))
                .ForMember(dest => dest.DisplayStatus, opts => opts.MapFrom(x => LocalizationHelper.GetLocalizedString(FormatHelper.PaymentAllocationTypeDisplay(x.PaymentAllocationType))))
                .ForMember(dest => dest.IsVisitRemoved, opts => opts.MapFrom(x => x.Visit.IsUnmatched))
                .ForMember(dest => dest.VisitId, opts => opts.MapFrom(src => src.Visit.VisitId));

            this.CreateMap<PaymentAllocationGroupedDto, PaymentAllocationWithFinancePlanViewModel>()
                .IncludeBase<PaymentAllocationGroupedDto, PaymentAllocationViewModel>();

            this.CreateMap<PaymentHistorySearchFilterViewModel, PaymentFilterDto>()
                .ForMember(dest => dest.PaymentStatus, opts => opts.MapFrom(x => x.PaymentStatusId.HasValue ? new List<PaymentStatusEnum> {(PaymentStatusEnum) x.PaymentStatusId} : x.PaymentStatuses.Select(z => (PaymentStatusEnum) Convert.ToInt32(z.Value))));

            this.CreateMap<PaymentHistoryDto, PaymentHistorySearchResultViewModel>()
                .ForMember(dest => dest.GuarantorName, opts => opts.MapFrom(x => x.GuarantorDto.User.DisplayFirstNameLastName))
                .ForMember(dest => dest.MadeByGuarantorName, opts => opts.MapFrom(x => x.MadeByGuarantorDto.User.DisplayFirstNameLastName))
                .ForMember(dest => dest.MadeByVisitPayUserId, opts => opts.MapFrom(x => x.MadeByGuarantorDto.User.VisitPayUserId))
                .ForMember(dest => dest.IdDisplay, opts => opts.MapFrom(x => x.PaymentProcessorResponseId <= 0 ? string.Empty : x.PaymentProcessorResponseId.ToString()))
                .ForMember(dest => dest.InsertDate, opts => opts.MapFrom(x => MappingHelper.MapToClientDateText(x.InsertDate)))
                .ForMember(dest => dest.IsCancelled, opts => opts.MapFrom(x => x.PaymentStatus == PaymentStatusEnum.ClosedCancelled))
                .ForMember(dest => dest.NetPaymentAmount, opts => opts.MapFrom(x => x.NetPaymentAmount.FormatCurrency()))
                .ForMember(dest => dest.PaymentMethod, opts => opts.MapFrom(x => x.PaymentMethod))
                .ForMember(dest => dest.PaymentStatusDisplayName, opts => opts.MapFrom(x => LocalizationHelper.GetLocalizedString(x.PaymentStatusDisplayName)))
                .ForMember(dest => dest.PaymentStatusId, opts => opts.MapFrom(x => (int) x.PaymentStatus))
                .ForMember(dest => dest.PaymentReversalStatus, opts => opts.MapFrom(x => (int) x.PaymentReversalStatusEnum))
                .ForMember(dest => dest.PaymentRefundableType, opts => opts.MapFrom(x => (int) x.PaymentRefundableType))
                .ForMember(dest => dest.PaymentStatusMessage, opts => opts.MapFrom(x => string.IsNullOrEmpty(x.PaymentStatusMessage) ? LocalizationHelper.GetLocalizedString(x.PaymentStatusDisplayName) : x.PaymentStatusMessage))
                .ForMember(dest => dest.PaymentType, opts => opts.MapFrom(x => LocalizationHelper.GetLocalizedString(FormatHelper.PaymentTypeDisplay(x.PaymentType))))
                .ForMember(dest => dest.PaymentTypeId, opts => opts.MapFrom(x => (int) x.PaymentType))
                .ForMember(dest => dest.SnapshotTotalPaymentAmount, opts => opts.MapFrom(x => x.SnapshotTotalPaymentAmount.FormatCurrency()))
                .ForMember(dest => dest.VisitPayUserId, opts => opts.MapFrom(x => x.GuarantorDto.User.VisitPayUserId));

            this.CreateMap<PaymentDetailDto, PaymentDetailViewModel>()
                .IncludeBase<PaymentHistoryDto, PaymentHistorySearchResultViewModel>();

            this.CreateMap<PaymentHistorySearchFilterViewModel, PaymentProcessorResponseFilterDto>()
                .ForMember(dest => dest.InsertDateRange, opts => opts.MapFrom(x => x.TransactionRange))
                .ForMember(dest => dest.InsertDateRangeFrom, opts => opts.MapFrom(x => x.PaymentDateRangeFrom))
                .ForMember(dest => dest.InsertDateRangeTo, opts => opts.MapFrom(x => x.PaymentDateRangeTo))
                .ForMember(dest => dest.PaymentProcessorResponseId, opts => opts.MapFrom(x => x.PaymentId))
                .ForMember(dest => dest.PaymentProcessorSourceKey, opts => opts.MapFrom(x => x.TransactionId))
                .ForMember(dest => dest.PaymentStatus, opts => opts.MapFrom(x => x.PaymentStatusId.HasValue ? new List<PaymentStatusEnum> {(PaymentStatusEnum) x.PaymentStatusId} : x.PaymentStatuses.Select(z => (PaymentStatusEnum) Convert.ToInt32(z.Value))))
                .ForMember(dest => dest.PaymentType, opts => opts.MapFrom(x => MappingHelper.MapPaymentType(x.PaymentTypeId)));

            this.CreateMap<ScheduledPaymentDto, PaymentPendingSearchResultViewModel>()
                .ForMember(dest => dest.VisitPayUserId, opts => opts.MapFrom(x => x.MadeForGuarantor.User.VisitPayUserId))
                .ForMember(dest => dest.GuarantorName, opts => opts.MapFrom(x => x.MadeForGuarantor.User.DisplayFirstNameLastName))
                .ForMember(dest => dest.GuarantorFirstName, opts => opts.MapFrom(x => x.MadeForGuarantor.User.FirstName))
                .ForMember(dest => dest.GuarantorLastName, opts => opts.MapFrom(x => x.MadeForGuarantor.User.LastName))
                .ForMember(dest => dest.MadeByGuarantorName, opts => opts.MapFrom(x => x.MadeByGuarantor.User.DisplayFirstNameLastName))
                .ForMember(dest => dest.MadeByVisitPayUserId, opts => opts.MapFrom(x => x.MadeByGuarantor.User.VisitPayUserId))
                .ForMember(dest => dest.PaymentMethod, opts => opts.MapFrom(x => x.PaymentMethod))
                .ForMember(dest => dest.PaymentType, opts => opts.MapFrom(x => LocalizationHelper.GetLocalizedString(FormatHelper.PaymentTypeDisplay(x.PaymentType))))
                .ForMember(dest => dest.ScheduledPaymentAmount, opts => opts.MapFrom(x => x.ScheduledPaymentAmount.FormatCurrency()))
                .ForMember(dest => dest.ScheduledPaymentDate, opts => opts.MapFrom(x => x.ScheduledPaymentDate.ToDateText()))
                .ForMember(dest => dest.PaymentId, opts => opts.MapFrom(x => x.PaymentId));

            this.CreateMap<PaymentMethodDto, PaymentMethodDisplayViewModel>()
                .ForMember(dest => dest.DisplayName, opts => opts.MapFrom(x => FormatHelper.PaymentMethodDisplay(x.PaymentMethodType, x.LastFour)));

            this.CreateMap<PaymentMethodDto, SelectListItem>()
                .ForMember(dest => dest.Text, opts => opts.MapFrom(x => x != null ? FormatHelper.PaymentMethodDisplay(x.PaymentMethodType, x.LastFour) : string.Empty))
                .ForMember(dest => dest.Value, opts => opts.MapFrom(x => x.PaymentMethodId.HasValue ? x.PaymentMethodId.Value.ToString() : string.Empty));

            this.CreateMap<PaymentDto, PaymentSummaryViewModel>()
                .ForMember(dest => dest.ActualPaymentAmount, opts => opts.MapFrom(x => x.ActualPaymentAmount.FormatCurrency()))
                .ForMember(dest => dest.ScheduledPaymentAmount, opts => opts.MapFrom(x => x.ScheduledPaymentAmount.FormatCurrency()))
                .ForMember(dest => dest.IsScheduledPayment, opts => opts.MapFrom(x => MappingHelper.IsScheduledPayment(x.PaymentType)));

            this.CreateMap<FinancePlanDto, FinancePlansSearchResultViewModel>()
                .ForMember(dest => dest.CurrentFinancePlanBalance, opts => opts.MapFrom(x => x.CurrentFinancePlanBalance.FormatCurrency()))
                .ForMember(dest => dest.CurrentFinancePlanBalanceDecimal, opts => opts.MapFrom(x => x.CurrentFinancePlanBalance))
                .ForMember(dest => dest.FinancePlanStatusFullDisplayName, opts => opts.MapFrom(x => LocalizationHelper.GetLocalizedString(x.FinancePlanStatus.FinancePlanStatusDisplayName)))
                .ForMember(dest => dest.InterestAssessed, opts => opts.MapFrom(x => x.InterestAssessed.FormatCurrency()))
                .ForMember(dest => dest.InterestPaidToDate, opts => opts.MapFrom(x => x.InterestPaidToDate.FormatCurrency()))
                .ForMember(dest => dest.InterestRate, opts => opts.MapFrom(x => x.CurrentInterestRate.FormatPercentage()))
                .ForMember(dest => dest.OriginatedFinancePlanBalance, opts => opts.MapFrom(x => MappingHelper.MapFinancePlanOriginatedBalance(x.FinancePlanStatus.FinancePlanStatusEnum, x.OriginatedFinancePlanBalance)))
                .ForMember(dest => dest.OriginationDate, opts => opts.MapFrom(x => x.OriginationDate.ToDateText()))
                .ForMember(dest => dest.PastDueAmount, opts => opts.MapFrom(x => x.PastDueAmount == 0m ? "N/A" : x.PastDueAmount.FormatCurrency()))
                .ForMember(dest => dest.AmountDue, opts => opts.MapFrom(x => x.AmountDue == 0m ? "N/A" : x.AmountDue.FormatCurrency()))
                .ForMember(dest => dest.AmountDueDecimal, opts => opts.MapFrom(x => x.AmountDue))
                .ForMember(dest => dest.HasAmountDue, opts => opts.MapFrom(x => x.AmountDue > 0m))
                .ForMember(dest => dest.PaymentAmount, opts => opts.MapFrom(x => x.PaymentAmount.FormatCurrency()))
                .ForMember(dest => dest.PrincipalPaidToDate, opts => opts.MapFrom(x => x.PrincipalPaidToDate.FormatCurrency()))
                .ForMember(dest => dest.Terms, opts => opts.MapFrom(x => $"{x.CurrentInterestRate.FormatPercentage()} - {MappingHelper.MapToFormattedClientDateTimeText(x.LastPaymentDate, Format.MonthYearFormat)}"))
                .ForMember(dest => dest.VisitIds, opts => opts.MapFrom(x => x.FinancePlanVisits.Select(z => z.VisitId)));

            this.CreateMap<FinancePlansSearchFilterViewModel, FinancePlanFilterDto>()
                .ForMember(dest => dest.OriginationDateSince, opts => opts.MapFrom(x => x.LastXRange == "90d" ? DateTime.UtcNow.AddDays(-90) : x.LastXRange == "6m" ? DateTime.UtcNow.AddMonths(-6) : x.LastXRange == "1y" ? DateTime.UtcNow.AddYears(-1) : (DateTime?) null));

            this.CreateMap<FinancePlanInterestRateHistoryDto, FinancePlanInterestRateHistoryViewModel>()
                .ForMember(dest => dest.InsertDate, opts => opts.MapFrom(x => MappingHelper.MapToClientDateTimeText(x.InsertDate)))
                .ForMember(dest => dest.InterestRate, opts => opts.MapFrom(x => x.InterestRate.FormatPercentage()))
                .ForMember(dest => dest.PaymentAmount, opts => opts.MapFrom(x => x.PaymentAmount.FormatCurrency()));

            this.CreateMap<FinancePlanLogDto, FinancePlanLogViewModel>()
                .ForMember(dest => dest.InsertDate, opts => opts.MapFrom(x => MappingHelper.MapToClientDateTimeText(x.InsertDate)))
                .ForMember(dest => dest.InterestRateUsedForAssessement, opts => opts.MapFrom(x => (x.InterestRateUsedForAssessement != null) ? x.InterestRateUsedForAssessement.FormatPercentage() : "N/A"))
                .ForMember(dest => dest.Amount, opts => opts.MapFrom(x => x.Amount.FormatCurrency()))
                .ForMember(dest => dest.VisitBalanceAtTimeOfAssessement, opts => opts.MapFrom(x => (x.VisitBalanceAtTimeOfAssessement != null) ? x.VisitBalanceAtTimeOfAssessement.FormatCurrency() : "N/A"));

            this.CreateMap<TreatmentLocationDto, SelectListItem>()
                .ForMember(dest => dest.Text, opts => opts.MapFrom(x => x.TreatmentLocationName))
                .ForMember(dest => dest.Value, opts => opts.MapFrom(x => x.TreatmentLocationId));

            this.CreateMap<SupportRequestDto, SupportRequestViewModel>()
                .ForMember(dest => dest.ClosedDate, opts => opts.MapFrom(x => x.ClosedDate.HasValue ? x.ClosedDate.ToString() : null))
                .ForMember(dest => dest.CreatedDate, opts => opts.MapFrom(x => MappingHelper.MapToClientDateText(x.InsertDate)))
                .ForMember(dest => dest.GuarantorFullName, opts => opts.MapFrom(x => x.VpGuarantor.User.DisplayFirstNameLastName))
                .ForMember(dest => dest.GuarantorMessages, opts => opts.MapFrom(x => x.GuarantorMessages.OrderByDescending(z => z.InsertDate)))
                .ForMember(dest => dest.InternalMessages, opts => opts.MapFrom(x => x.InternalMessages.OrderByDescending(z => z.InsertDate)))
                .ForMember(dest => dest.PatientFullName, opts => opts.MapFrom(x => !string.IsNullOrEmpty(x.PatientFirstName) || !string.IsNullOrEmpty(x.PatientLastName) ? x.PatientFirstNameLastName.TrimStart(", ".ToCharArray()).TrimEnd(", ".ToCharArray()) : "N/A"))
                .ForMember(dest => dest.SupportRequestStatusId, opts => opts.MapFrom(x => (int) x.SupportRequestStatus))
                .ForMember(dest => dest.SupportRequestStatusName, opts => opts.MapFrom(x => x.SupportRequestStatusSource.SupportRequestStatusName))
                .ForMember(dest => dest.TreatmentDate, opts => opts.MapFrom(x => x.TreatmentDate.HasValue ? x.TreatmentDate.ToDateText() : "N/A"))
                .ForMember(dest => dest.TreatmentLocation, opts => opts.MapFrom(x => x.TreatmentLocation != null ? x.TreatmentLocation.TreatmentLocationName : null))
                .ForMember(dest => dest.SubmittedFor, opts => opts.MapFrom(x => MappingHelper.MapSupportRequestSubmittedFor(x, false)))
                .ForMember(dest => dest.IsPreviousConsolidation, opts => opts.MapFrom(x => x.SubmittedForVpGuarantor != null && x.SubmittedForVpGuarantor.ConsolidationStatus == GuarantorConsolidationStatusEnum.NotConsolidated))
                .ForMember(dest => dest.SupportTopicId, opts => opts.MapFrom(x => x.SupportTopic == null ? null : x.SupportTopic.ParentSupportTopicId))
                .ForMember(dest => dest.SupportSubTopicId, opts => opts.MapFrom(x => x.SupportTopic == null ? (int?) null : x.SupportTopic.SupportTopicId))
                .ForMember(dest => dest.Topic, opts => opts.MapFrom(x => x.SupportTopic == null ? string.Empty : x.SupportTopic.SupportTopicName))
                .ForMember(dest => dest.IsFlaggedForFollowUp, opts => opts.MapFrom(x => x.IsFlaggedForFollowUp))
                // ignore in case of obfuscation, aftermap will handle it
                .ForMember(dest => dest.VisitSourceSystemKey, opts => opts.Ignore())
                .ForMember(dest => dest.VisitSourceSystemKeyDisplay, opts => opts.Ignore())
                .ForMember(dest => dest.VisitMatchedSourceSystemKey, opts => opts.Ignore())
                .ForMember(dest => dest.VisitDescription, opts => opts.Ignore())
                .ForMember(dest => dest.IsVisitRemoved, opts => opts.Ignore())
                .ForMember(dest => dest.HasVisit, opts => opts.Ignore())
                .AfterMap((src, dest) =>
                {
                    // obfuscate if necessary
                    VisitDto visit = Mapper.Map<VisitDto>(src.Visit);
                    Mapper.Map(visit, dest);
                });

            this.CreateMap<VisitDto, SupportRequestViewModel>()
                .ForMember(dest => dest.VisitSourceSystemKey, opts => opts.MapFrom(x => x != null ? MappingHelper.MapVisitSourceSystemKey(x) : "N/A"))
                .ForMember(dest => dest.VisitSourceSystemKeyDisplay, opts => opts.MapFrom(x => x != null ? MappingHelper.MapVisitSourceSystemKey(x, z => z.SourceSystemKeyDisplay) : "N/A"))
                .ForMember(dest => dest.VisitMatchedSourceSystemKey, opts => opts.MapFrom(x => x != null ? x.MatchedSourceSystemKey : ""))
                .ForMember(dest => dest.VisitDescription, opts => opts.MapFrom(x => x != null ? x.VisitDescription : ""))
                .ForMember(dest => dest.IsVisitRemoved, opts => opts.MapFrom(x => x != null && x.IsUnmatched))
                .ForMember(dest => dest.HasVisit, opts => opts.MapFrom(x => x != null));

            this.CreateMap<SupportRequestMessageDto, SupportRequestMessageViewModel>()
                .ForMember(dest => dest.Attachments, opts => opts.MapFrom(x => x.ActiveSupportRequestMessageAttachments))
                .ForMember(dest => dest.InsertDate, opts => opts.MapFrom(x => MappingHelper.MapToClientDateTimeText(x.InsertDate)))
                .ForMember(dest => dest.MessageBody, opts => opts.MapFrom(x => x.MessageBody.Replace(Environment.NewLine, "<br />").Replace("\n", "<br />").Replace("<br /><br />", "<br />")))
                .ForMember(dest => dest.SupportRequestMessageType, opts => opts.MapFrom(x => (int) x.SupportRequestMessageType))
                .ForMember(dest => dest.VisitPayUserName, opts => opts.MapFrom(x => x.VisitPayUser.DisplayFirstNameLastName));

            this.CreateMap<SupportRequestMessageAttachmentDto, SupportRequestMessageAttachmentViewModel>()
                .ForMember(dest => dest.InsertDate, opts => opts.MapFrom(x => MappingHelper.MapToClientDateTimeText(x.SupportRequestMessage.InsertDate)));

            // statements
            this.CreateMap<FinancePlanStatementDto, StatementListItemViewModel>()
                .ForMember(dest => dest.DisplayStatementBalance, opts => opts.MapFrom(x => x.DisplayStatementBalance.FormatCurrency()))
                .ForMember(dest => dest.StatementDate, opts => opts.MapFrom(x => x.StatementDate.ToFormattedDateTimeText(Format.MonthDayYearFormat)))
                .ForMember(dest => dest.FinancePlanStatementId, opts => opts.MapFrom(x => x.FinancePlanStatementId));

            this.CreateMap<StatementDto, StatementListItemViewModel>()
                .ForMember(dest => dest.DisplayStatementBalance, opts => opts.MapFrom(x => x.DisplayStatementBalance.FormatCurrency()))
                .ForMember(dest => dest.StatementDate, opts => opts.MapFrom(x => x.StatementDate.ToFormattedDateTimeText(Format.MonthDayYearFormat)))
                .ForMember(dest => dest.StatementId, opts => opts.MapFrom(x => x.VpStatementId));

            this.CreateMap<StatementDto, StatementViewModel>()
                .ForMember(dest => dest.PaymentDueDate, opts => opts.MapFrom(x => x.PaymentDueDate.ToDateText()))
                .ForMember(dest => dest.PeriodEndDate, opts => opts.MapFrom(x => x.PeriodEndDate.ToDateText()))
                .ForMember(dest => dest.PeriodStartDate, opts => opts.MapFrom(x => x.PeriodStartDate.ToDateText()))
                .ForMember(dest => dest.PriorStatementDate, opts => opts.MapFrom(x => x.PriorStatementDate.ToDateText()))
                .ForMember(dest => dest.StatementDate, opts => opts.MapFrom(x => x.StatementDate.ToFormattedDateTimeText(Format.MonthDayYearFormat)))
                .ForMember(dest => dest.StatementDateMMddyyy, opts => opts.MapFrom(x => x.StatementDate.ToFormattedDateTimeText(Format.DateFormat)))
                .ForMember(dest => dest.DaysToNextStatement, opts => opts.MapFrom(x => x.NextStatementDate.HasValue ? Convert.ToInt32(Math.Ceiling(x.NextStatementDate.Value.Subtract(DateTime.UtcNow).TotalDays)) : 0))
                .ForMember(dest => dest.NextStatementDate, opts => opts.MapFrom(x => x.NextStatementDate.HasValue ? x.NextStatementDate.Value.ToString(Format.DateFormatShortYear) : ""))
                .ForMember(dest => dest.StatementedSnapshotDiscountsAndAdjustmentsSum, opts => opts.MapFrom(x => (-x.StatementedSnapshotDiscountsAndAdjustmentsSum).FormatCurrency())) // show as negative
                .ForMember(dest => dest.StatementedSnapshotFinancePlanBalanceSum, opts => opts.MapFrom(x => x.StatementedSnapshotFinancePlanBalanceSum.FormatCurrency()))
                .ForMember(dest => dest.StatementedSnapshotFinancePlanInterestChargedSum, opts => opts.MapFrom(x => x.StatementedSnapshotFinancePlanInterestChargedSum.FormatCurrency()))
                .ForMember(dest => dest.StatementedSnapshotFinancePlanInterestDueSum, opts => opts.MapFrom(x => x.StatementedSnapshotFinancePlanInterestDueSum.FormatCurrency()))
                .ForMember(dest => dest.StatementedSnapshotFinancePlanPrincipalDueSum, opts => opts.MapFrom(x => x.StatementedSnapshotFinancePlanPrincipalDueSum.FormatCurrency()))
                .ForMember(dest => dest.StatementedSnapshotMinimumAmountDue, opts => opts.MapFrom(x => x.StatementedSnapshotMinimumAmountDue.FormatCurrency()))
                .ForMember(dest => dest.StatementedSnapshotNewVisitBalanceSum, opts => opts.MapFrom(x => x.StatementedSnapshotNewVisitBalanceSum.FormatCurrency()))
                .ForMember(dest => dest.StatementedSnapshotPaymentsAppliedSum, opts => opts.MapFrom(x => (-x.StatementedSnapshotPaymentsAppliedSum).FormatCurrency())) // show as negative
                .ForMember(dest => dest.StatementedSnapshotPriorTotalBalance, opts => opts.MapFrom(x => x.StatementedSnapshotPriorTotalBalance.FormatCurrency()))
                .ForMember(dest => dest.StatementedSnapshotTotalBalance, opts => opts.MapFrom(x => x.StatementedSnapshotTotalBalance.FormatCurrency()))
                .ForMember(dest => dest.StatementedSnapshotTotalInterestChargedYTD, opts => opts.MapFrom(x => x.StatementedSnapshotTotalInterestChargedYTD.FormatCurrency()))
                .ForMember(dest => dest.StatementedSnapshotVisitBalanceSum, opts => opts.MapFrom(x => x.StatementedSnapshotVisitBalanceSum.FormatCurrency()))
                .ForMember(dest => dest.StatementedSnapshotFinancePlanPaymentDueSum, opts => opts.MapFrom(x => x.StatementedSnapshotFinancePlanPaymentDueSum.FormatCurrency()))
                .ForMember(dest => dest.StatementedSnapshotVisitCreditBalanceSum, opts => opts.MapFrom(x => x.StatementedSnapshotVisitCreditBalanceSum.FormatCurrency()))
                .ForMember(dest => dest.StatementedSnapshotSuspendedClosedVisitBalanceSum, opts => opts.MapFrom(x => x.StatementedSnapshotSuspendedClosedVisitBalanceSum.FormatCurrency()))
                .ForMember(dest => dest.StatementedSnapshotVpFeesAndInterest, opts => opts.MapFrom(x => x.StatementedSnapshotVpFeesAndInterest.FormatCurrency()))
                .ForMember(dest => dest.StatementedSnapshotVpPaymentsAndDiscounts, opts => opts.MapFrom(x => x.StatementedSnapshotVpPaymentsAndDiscounts.FormatCurrency()))
                .ForMember(dest => dest.StatementedSnapshotVisitHsTransactionsPaymentSum, opts => opts.MapFrom(x => x.StatementedSnapshotVisitHsTransactionsPaymentSum.FormatCurrency()))
                .ForMember(dest => dest.StatementedSnapshotHsAdjustmentsAndHsCharges, opts => opts.MapFrom(x => x.StatementedSnapshotHsAdjustmentsAndHsCharges.FormatCurrency()))
                .ForMember(dest => dest.StatementedSnapshotVisitHsTransactionsAdjustmentSum, opts => opts.MapFrom(x => x.StatementedSnapshotVisitHsTransactionsAdjustmentSum.FormatCurrency()))
                .ForMember(dest => dest.StatementedSnapshotVisitHsTransactionsAdjustmentsExcludingInsuranceSum, opts => opts.MapFrom(x => x.StatementedSnapshotVisitHsTransactionsAdjustmentsExcludingInsuranceSum.FormatCurrency()))
                .ForMember(dest => dest.StatementedSnapshotVisitHsTransactionsAdjustmentsInsuranceSum, opts => opts.MapFrom(x => x.StatementedSnapshotVisitHsTransactionsAdjustmentsInsuranceSum.FormatCurrency()))
                .ForMember(dest => dest.StatementedSnapshotVisitHsTransactionsChargeSum, opts => opts.MapFrom(x => x.StatementedSnapshotVisitHsTransactionsChargeSum.FormatCurrency()));

            this.CreateMap<FinancePlanStatementDto, FinancePlanStatementViewModel>()
                .ForMember(dest => dest.PaymentDueDate, opts => opts.MapFrom(x => x.PaymentDueDate.ToDateText()))
                .ForMember(dest => dest.PeriodEndDate, opts => opts.MapFrom(x => x.PeriodEndDate.ToDateText()))
                .ForMember(dest => dest.PeriodStartDate, opts => opts.MapFrom(x => x.PeriodStartDate.ToDateText()))
                .ForMember(dest => dest.StatementDate, opts => opts.MapFrom(x => x.StatementDate.ToFormattedDateTimeText(Format.MonthDayYearFormat)))
                .ForMember(dest => dest.StatementDateMMddyyy, opts => opts.MapFrom(x => x.StatementDate.ToFormattedDateTimeText(Format.DateFormat)))
                .ForMember(dest => dest.StatementedSnapshotFinancePlanFeesChargedForStatementYearSum, opts => opts.MapFrom(x => x.StatementedSnapshotFinancePlanFeesChargedForStatementYearSum.FormatCurrency()))
                .ForMember(dest => dest.StatementedSnapshotFinancePlanInterestChargedForStatementYearSum, opts => opts.MapFrom(x => x.StatementedSnapshotFinancePlanInterestChargedForStatementYearSum.FormatCurrency()))
                .ForMember(dest => dest.StatementedSnapshotFinancePlanPaymentDueSum, opts => opts.MapFrom(x => x.StatementedSnapshotFinancePlanPaymentDueSum.FormatCurrency()));

            this.CreateMap<FinancePlanStatementFinancePlanDto, FinancePlanStatementFinancePlanViewModel>()
                .ForMember(dest => dest.FinancePlanStatusDisplayName, opts => opts.MapFrom(x => x.StatementedSnapshotFinancePlanStatus.FinancePlanStatusDisplayName))
                .ForMember(dest => dest.IsPastDue, opts => opts.MapFrom(x => x.StatementedSnapshotFinancePlanStatus.FinancePlanStatusEnum.IsInCategory(FinancePlanStatusEnumCategory.PastDue)))
                .ForMember(dest => dest.StatementedSnapshotFinancePlanEffectiveDate, opts => opts.MapFrom(x => x.StatementedSnapshotFinancePlanEffectiveDate.ToDateText()))
                .ForMember(dest => dest.StatementedSnapshotFinancePlanInterestRate, opts => opts.MapFrom(x => x.StatementedSnapshotFinancePlanInterestRate.FormatPercentage()))
                .ForMember(dest => dest.StatementedSnapshotFinancePlanOriginalBalance, opts => opts.MapFrom(x => x.StatementedSnapshotFinancePlanOriginalBalance.FormatCurrency()))
                .ForMember(dest => dest.StatementedSnapshotPriorFinancePlanBalance, opts => opts.MapFrom(x => x.StatementedSnapshotPriorFinancePlanBalance.FormatCurrency()))
                .ForMember(dest => dest.StatementedSnapshotFinancePlanInterestCharged, opts => opts.MapFrom(x => x.StatementedSnapshotFinancePlanInterestCharged.FormatCurrency()))
                .ForMember(dest => dest.StatementedSnapshotFinancePlanInterestPaid, opts => opts.MapFrom(x => x.StatementedSnapshotFinancePlanInterestPaid.FormatCurrency()))
                .ForMember(dest => dest.StatementedSnapshotFinancePlanPrincipalPaid, opts => opts.MapFrom(x => x.StatementedSnapshotFinancePlanPrincipalPaid.FormatCurrency()))
                .ForMember(dest => dest.StatementedSnapshotFinancePlanOtherBalanceCharges, opts => opts.MapFrom(x => x.StatementedSnapshotFinancePlanOtherBalanceCharges.FormatCurrency()))
                .ForMember(dest => dest.StatementedSnapshotFinancePlanBalance, opts => opts.MapFrom(x => x.StatementedSnapshotFinancePlanBalance.FormatCurrency()))
                .ForMember(dest => dest.StatementedSnapshotFinancePlanMonthlyPaymentAmount, opts => opts.MapFrom(x => x.StatementedSnapshotFinancePlanMonthlyPaymentAmount.FormatCurrency()))
                .ForMember(dest => dest.StatementedSnapshotFinancePlanPastDueAmount, opts => opts.MapFrom(x => x.StatementedSnapshotFinancePlanPastDueAmount.FormatCurrency()))
                .ForMember(dest => dest.StatementedSnapshotFinancePlanPaymentDue, opts => opts.MapFrom(x => x.StatementedSnapshotFinancePlanPaymentDue.FormatCurrency()));

            this.CreateMap<FinancePlanStatementVisitDto, FinancePlanStatementVisitViewModel>()
                .ForMember(dest => dest.StatementedSnapshotTotalBalance, opts => opts.MapFrom(x => x.StatementedSnapshotTotalBalance.FormatCurrency()));

            this.CreateMap<StatementAlertDto, StatementAlertViewModel>();

            this.CreateMap<StatementFinancePlanDto, StatementFinancePlanViewModel>()
                .ForMember(dest => dest.StatementedSnapshotFinancePlanAdjustmentsSum, opts => opts.MapFrom(x => x.StatementedSnapshotFinancePlanAdjustmentsSum.FormatCurrency()))
                .ForMember(dest => dest.StatementedSnapshotFinancePlanBalance, opts => opts.MapFrom(x => x.StatementedSnapshotFinancePlanBalance.FormatCurrency()))
                .ForMember(dest => dest.StatementedSnapshotFinancePlanEffectiveDate, opts => opts.MapFrom(x => x.StatementedSnapshotFinancePlanEffectiveDate.ToDateText()))
                .ForMember(dest => dest.StatementedSnapshotFinancePlanInterestCharged, opts => opts.MapFrom(x => x.StatementedSnapshotFinancePlanInterestCharged.FormatCurrency()))
                .ForMember(dest => dest.StatementedSnapshotFinancePlanInterestRate, opts => opts.MapFrom(x => x.StatementedSnapshotFinancePlanInterestRate.FormatPercentage()))
                .ForMember(dest => dest.StatementedSnapshotFinancePlanInterestPaid, opts => opts.MapFrom(x => x.StatementedSnapshotFinancePlanInterestPaid.FormatCurrency()))
                .ForMember(dest => dest.StatementedSnapshotFinancePlanMonthlyPaymentAmount, opts => opts.MapFrom(x => x.StatementedSnapshotFinancePlanMonthlyPaymentAmount.FormatCurrency()))
                .ForMember(dest => dest.StatementedSnapshotFinancePlanPastDueAmount, opts => opts.MapFrom(x => x.StatementedSnapshotFinancePlanPastDueAmount.FormatCurrency()))
                .ForMember(dest => dest.FinancePlanPayments, opts => opts.MapFrom(x => x.FinancePlanPayments.FormatCurrency()))
                .ForMember(dest => dest.StatementedSnapshotFinancePlanPaymentDue, opts => opts.MapFrom(x => x.StatementedSnapshotFinancePlanPaymentDue.FormatCurrency()))
                .ForMember(dest => dest.StatementedSnapshotFinancePlanPrincipalPaid, opts => opts.MapFrom(x => x.StatementedSnapshotFinancePlanPrincipalPaid.FormatCurrency()))
                .ForMember(dest => dest.StatementedSnapshotFinancePlanStatus, opts => opts.MapFrom(x => x.StatementedSnapshotFinancePlanStatusDisplayName))
                .ForMember(dest => dest.StatementedSnapshotPriorFinancePlanBalance, opts => opts.MapFrom(x => x.StatementedSnapshotPriorFinancePlanBalance.FormatCurrency()))
                .ForMember(dest => dest.StatementedSnapshotFinancePlanStatusId, opts => opts.MapFrom(x => x.StatementedSnapshotFinancePlanStatusId));

            this.CreateMap<StatementVisitDto, StatementVisitViewModel>()
                .ForMember(dest => dest.Description, opts => opts.MapFrom(x => x.Visit.VisitDescription))
                .ForMember(dest => dest.Patient, opts => opts.MapFrom(x => x.Visit.PatientName))
                .ForMember(dest => dest.FacilityDescription, opts => opts.MapFrom(x => x.Visit.Facility.FacilityDescription))
                .ForMember(dest => dest.SourceSystemKeyDisplay, opts => opts.MapFrom(x => MappingHelper.MapVisitSourceSystemKey(x.Visit, z => z.SourceSystemKeyDisplay)))
                .ForMember(dest => dest.VisitDate, opts => opts.MapFrom(x => x.Visit.VisitDisplayDate))
                .ForMember(dest => dest.StatementedSnapshotVisitBalance, opts => opts.MapFrom(x => x.StatementedSnapshotVisitBalance.FormatCurrency()))
                .ForMember(dest => dest.StatementedSnapshotVisitState, opts => opts.MapFrom(x => x.StatementedSnapshotVisitState.ToString()))
                .ForMember(dest => dest.StatementedSnapshotVisitStateId, opts => opts.MapFrom(x => x.StatementedSnapshotVisitState));

            this.CreateMap<StatementVisitDto, VisitBalanceViewModel>()
                .ForMember(dest => dest.VisitDescription, opts => opts.MapFrom(x => x.Visit.VisitDescription))
                .ForMember(dest => dest.DisplayBalance, opts => opts.MapFrom(x => x.StatementedSnapshotVisitBalance.FormatCurrency()))
                .ForMember(dest => dest.DischargeDate, opts => opts.MapFrom(x => x.Visit.DischargeDate.ToFormattedDateTimeText(Format.DateFormat)))
                .ForMember(dest => dest.PatientName, opts => opts.MapFrom(x => x.Visit.PatientName))
                .ForMember(dest => dest.IsPastDue, opts => opts.MapFrom(x => x.Visit.IsPastDue))
                .ForMember(dest => dest.IsFinalPastDue, opts => opts.MapFrom(x => x.Visit.IsFinalPastDue));

            this.CreateMap<PaymentDto, ResubmitPaymentViewModel>()
                .ForMember(dest => dest.LastPaymentFailureDate, opts => opts.MapFrom(x => MappingHelper.MapToClientDateText(x.LastPaymentFailureDate)))
                .ForMember(dest => dest.PaymentType, opts => opts.MapFrom(x => LocalizationHelper.GetLocalizedString(FormatHelper.PaymentTypeDisplay(x.PaymentType))))
                .ForMember(dest => dest.ScheduledPaymentAmountDecimal, opts => opts.MapFrom(x => x.ScheduledPaymentAmount))
                .ForMember(dest => dest.ScheduledPaymentAmount, opts => opts.MapFrom(x => x.ScheduledPaymentAmount.FormatCurrency()));

            this.CreateMap<ResubmitPaymentDto, ResubmitPaymentViewModel>()
                .ForMember(dest => dest.LastPaymentFailureDate, opts => opts.MapFrom(x => MappingHelper.MapToClientDateText(x.LastPaymentFailureDate)))
                .ForMember(dest => dest.PaymentType, opts => opts.MapFrom(x => LocalizationHelper.GetLocalizedString(FormatHelper.PaymentTypeDisplay(x.PaymentType))))
                .ForMember(dest => dest.ScheduledPaymentAmountDecimal, opts => opts.MapFrom(x => x.SnapshotTotalPaymentAmount))
                .ForMember(dest => dest.ScheduledPaymentAmount, opts => opts.MapFrom(x => x.SnapshotTotalPaymentAmount.FormatCurrency()));

            this.CreateMap<ConsolidationGuarantorDto, ConsolidationGuarantorViewModel>()
                .ForMember(dest => dest.ManagedGuarantorName, opts => opts.MapFrom(src => src.ManagedGuarantor.User.DisplayFirstNameLastName))
                .ForMember(dest => dest.ManagedGuarantorId, opts => opts.MapFrom(src => src.ManagedGuarantor.VpGuarantorId))
                .ForMember(dest => dest.ManagingGuarantorName, opts => opts.MapFrom(src => src.ManagingGuarantor.User.DisplayFirstNameLastName))
                .ForMember(dest => dest.ManagingGuarantorId, opts => opts.MapFrom(src => src.ManagingGuarantor.VpGuarantorId))
                .ForMember(dest => dest.ConsolidationGuarantorStatusEnum, opts => opts.MapFrom(src => src.ConsolidationGuarantorStatus))
                .ForMember(dest => dest.ConsolidationGuarantorStatus, opts => opts.MapFrom(src => MappingHelper.MapConsolidationGuarantorStatus(src)))
                .ForMember(dest => dest.DateAcceptedByManagedGuarantor, opts => opts.MapFrom(src => MappingHelper.MapConsolidationGuarantorField(src, MappingHelper.MapToClientDateText(src.DateAcceptedByManagedGuarantor), false)))
                .ForMember(dest => dest.DateAcceptedByManagingGuarantor, opts => opts.MapFrom(src => MappingHelper.MapConsolidationGuarantorField(src, MappingHelper.MapToClientDateText(src.DateAcceptedByManagingGuarantor), false)))
                .ForMember(dest => dest.DateAcceptedFinancePlanTermsByManagingGuarantor, opts => opts.MapFrom(src => MappingHelper.MapConsolidationGuarantorField(src, MappingHelper.MapToClientDateText(src.DateAcceptedFinancePlanTermsByManagingGuarantor), false)))
                .ForMember(dest => dest.CancelledByUserId, opts => opts.MapFrom(src => src.CancelledByUser != null ? src.CancelledByUser.VisitPayUserId : (int?) null))
                .ForMember(dest => dest.CancelledByUserName, opts => opts.MapFrom(src => MappingHelper.MapConsolidationGuarantorField(src, src.CancelledByUser.DisplayFirstNameLastName, true)))
                .ForMember(dest => dest.CancelledOn, opts => opts.MapFrom(src => MappingHelper.MapConsolidationGuarantorField(src, MappingHelper.MapToClientDateText(src.CancelledOn), false)))
                .ForMember(dest => dest.ConsolidationGuarantorId, opts => opts.Ignore());

            this.CreateMap<PaymentStatusDto, SelectListItem>()
                .ForMember(dest => dest.Text, opts => opts.MapFrom(src => LocalizationHelper.GetLocalizedString(src.PaymentStatusDisplayName)))
                .ForMember(dest => dest.Value, opts => opts.MapFrom(src => (int) src.PaymentStatusEnum));

            this.CreateMap<VisitItemizationStorageDto, ItemizationSearchResultViewModel>()
                .ForMember(dest => dest.DischargeDate, opts => opts.MapFrom(src => src.VisitDischargeDate.ToDateText()))
                .ForMember(dest => dest.PatientName, opts => opts.MapFrom(src => src.PatientName))
                .ForMember(dest => dest.FacilityCode, opts => opts.MapFrom(src => MappingHelper.MapItemizationFacilityCode(src)))
                .ForMember(dest => dest.VisitFileStoredId, opts => opts.Ignore());

            this.CreateMap<ItemizationSearchFilterViewModel, VisitItemizationStorageFilterDto>();

            this.CreateMap<SecurityQuestionViewModel, SecurityQuestionAnswerDto>().ForMember(dest => dest.SecurityQuestion, opts => opts.MapFrom(x => new SecurityQuestionDto {SecurityQuestionId = x.SecurityQuestionId}));

            this.CreateMap<PaymentMethodResultDto, PaymentMethodResultViewModel>()
                .ConstructUsing(src => new PaymentMethodResultViewModel(src.IsSuccess, src.ErrorMessage, src.PrimaryChanged, Mapper.Map<PaymentMethodListItemViewModel>(src.PaymentMethod)));

            this.CreateMap<PaymentMethodDto, PaymentMethodListItemViewModel>()
                .IncludeBase<PaymentMethodDto, PaymentMethodDisplayViewModel>()
                .ForMember(dest => dest.PaymentMethodId, opts => opts.MapFrom(src => src.PaymentMethodId.GetValueOrDefault(0)))
                .ForMember(dest => dest.ProviderTypeImage, opts => opts.MapFrom(src => src.PaymentMethodProviderType.ImageName));

            this.CreateMap<PaymentMethodAccountTypeDto, PaymentMethodAccountTypeViewModel>()
                .AfterMap((src, dest) =>
                {
                    if (dest.PaymentMethodAccountTypeId == 0 ||
                        dest.PaymentMethodAccountTypeId == 3)
                    {
                        if (!string.IsNullOrEmpty(dest.PaymentMethodAccountTypeListDisplayText))
                        {
                            dest.PaymentMethodAccountTypeListDisplayText = LocalizationHelper.GetLocalizedString(dest.PaymentMethodAccountTypeListDisplayText);
                        }

                        if (!string.IsNullOrEmpty(dest.PaymentMethodAccountTypeSelectedDisplayText))
                        {
                            dest.PaymentMethodAccountTypeSelectedDisplayText = LocalizationHelper.GetLocalizedString(dest.PaymentMethodAccountTypeSelectedDisplayText);
                        }
                    }
                });

            this.CreateMap<PaymentMethodProviderTypeDto, PaymentMethodProviderTypeViewModel>()
                .AfterMap((src, dest) =>
                {
                    if (dest.PaymentMethodProviderTypeId == 2)
                    {
                        if (!string.IsNullOrEmpty(dest.PaymentMethodProviderTypeText))
                        {
                            dest.PaymentMethodProviderTypeText = LocalizationHelper.GetLocalizedString(dest.PaymentMethodProviderTypeText);
                        }

                        if (!string.IsNullOrEmpty(dest.PaymentMethodProviderTypeDisplayText))
                        {
                            dest.PaymentMethodProviderTypeDisplayText = LocalizationHelper.GetLocalizedString(dest.PaymentMethodProviderTypeDisplayText);
                        }
                    }
                });

            this.CreateMap<PaymentMethodDto, BankAccountViewModel>()
                .ForMember(dest => dest.PaymentMethodId, opts => opts.MapFrom(src => src.PaymentMethodId.GetValueOrDefault(0)))
                .ForMember(dest => dest.PaymentMethodTypeId, opts => opts.MapFrom(src => (int) src.PaymentMethodType));

            this.CreateMap<BankAccountViewModel, PaymentMethodDto>()
                .ForMember(dest => dest.PaymentMethodType, opts => opts.MapFrom(src => (PaymentMethodTypeEnum) src.PaymentMethodTypeId));

            this.CreateMap<PaymentMethodDto, CardAccountViewModel>()
                .ForMember(dest => dest.ExpirationMonth, opts => opts.MapFrom(src => !string.IsNullOrEmpty(src.ExpDate) ? Convert.ToInt32(src.ExpDate.Substring(0, 2)) : 0))
                .ForMember(dest => dest.ExpirationYear, opts => opts.MapFrom(src => !string.IsNullOrEmpty(src.ExpDate) ? Convert.ToInt32(src.ExpDate.Substring(2, 2)) : 0))
                .ForMember(dest => dest.PaymentMethodId, opts => opts.MapFrom(src => src.PaymentMethodId.GetValueOrDefault(0)))
                .ForMember(dest => dest.PaymentMethodTypeId, opts => opts.MapFrom(src => (int) src.PaymentMethodType))
                .AfterMap((src, dest) =>
                {
                    PaymentMethodBillingAddressDto billingAddress = src.BillingAddresses.FirstOrDefault();
                    if (billingAddress != null)
                    {
                        dest.AddressLine1 = billingAddress.Address1;
                        dest.AddressLine2 = billingAddress.Address2;
                        dest.City = billingAddress.City;
                        dest.State = billingAddress.State;
                        dest.Zip = billingAddress.Zip;
                    }
                });

            this.CreateMap<CardAccountViewModel, PaymentMethodDto>()
                .ForMember(dest => dest.ExpDate, opts => opts.MapFrom(src => $"{src.ExpirationMonth.ToString().PadLeft(2, '0')}{src.ExpirationYear}"))
                .ForMember(dest => dest.PaymentMethodType, opts => opts.MapFrom(src => (PaymentMethodTypeEnum) src.PaymentMethodTypeId))
                .AfterMap((src, dest) =>
                {
                    if (!dest.BillingAddresses.Any())
                    {
                        dest.BillingAddresses.Add(new PaymentMethodBillingAddressDto());
                    }

                    PaymentMethodBillingAddressDto billingAddressDto = dest.BillingAddresses.First();
                    billingAddressDto.Address1 = src.AddressLine1;
                    billingAddressDto.Address2 = src.AddressLine2;
                    billingAddressDto.City = src.City;
                    billingAddressDto.State = src.State;
                    billingAddressDto.Zip = src.Zip;
                });

            this.CreateMap<PaymentAccountViewModel, BankAccountViewModel>();

            this.CreateMap<PaymentAccountViewModel, CardAccountViewModel>();

            this.CreateMap<SystemHealthDetailsDto, SystemHealthViewModel>();
            this.CreateMap<SystemInfoDetailsDto, SystemInfoViewModel>();

            this.CreateMap<ContentMenuDto, ContentMenuViewModel>();
            this.CreateMap<ContentMenuItemDto, ContentMenuItemViewModel>();

            // payment summary
            this.CreateMap<PaymentSummarySearchFilterViewModel, PaymentSummaryFilterDto>();
            this.CreateMap<GridSearchModel<PaymentSummarySearchFilterViewModel>, PaymentSummaryFilterDto>()
                .AfterMap((src, dest) => { Mapper.Map(src.Filter, dest); });

            this.CreateMap<PaymentSummaryGroupedDto, PaymentSummaryGroupedViewModel>()
                .ForMember(dest => dest.TotalTransactionAmount, opts => opts.MapFrom(src => src.TotalTransactionAmount.FormatCurrency()))
                .ForMember(dest => dest.VisitDate, opts => opts.MapFrom(src => src.VisitDate.ToDateText()));

            this.CreateMap<PaymentSummaryGroupedTransactionDto, PaymentSummaryGroupedTransactionViewModel>()
                .ForMember(dest => dest.PostDate, opts => opts.MapFrom(src => src.PostDate.ToDateText()))
                .ForMember(dest => dest.TransactionAmount, opts => opts.MapFrom(src => src.TransactionAmount.FormatCurrency()));

            // visits
            this.CreateMap<VisitsSearchFilterViewModel, VisitFilterDto>();
            this.CreateMap<VisitDto, VisitsSearchResultViewModel>()
                .ForMember(dest => dest.DisplayBalance, opts => opts.MapFrom(src => src.UnclearedBalance.FormatCurrency()))
                .ForMember(dest => dest.DisplayBalanceDecimal, opts => opts.MapFrom(src => src.UnclearedBalance))
                .ForMember(dest => dest.DischargeDate, opts => opts.MapFrom(src => src.VisitDisplayDate))
                .ForMember(dest => dest.DischargeDateMonth, opts => opts.MapFrom(src => src.VisitDisplayDateUnformatted.ToFormattedDateTimeText("MMM")))
                .ForMember(dest => dest.DischargeDateDay, opts => opts.MapFrom(src => src.VisitDisplayDateUnformatted.ToFormattedDateTimeText("dd")))
                .ForMember(dest => dest.DischargeDateYear, opts => opts.MapFrom(src => src.VisitDisplayDateUnformatted.ToFormattedDateTimeText("yyyy")))
                .ForMember(dest => dest.IsActive, opts => opts.MapFrom(x => x.CurrentVisitState == VisitStateEnum.Active))
                .ForMember(dest => dest.IsFinanced, opts => opts.MapFrom(x => x.CurrentVisitStateDerivedEnum == VisitStateDerivedEnum.OnFinancePlan))
                .ForMember(dest => dest.IsMaxAge, opts => opts.MapFrom(x => x.CurrentVisitStateDerivedEnum == VisitStateDerivedEnum.MaxAge))
                .ForMember(dest => dest.IsHbVisit, opts => opts.MapFrom(src => BillingApplicationConstants.HB.Equals(src.BillingApplication, StringComparison.CurrentCultureIgnoreCase)))
                .ForMember(dest => dest.IsPbVisit, opts => opts.MapFrom(src => BillingApplicationConstants.PB.Equals(src.BillingApplication, StringComparison.CurrentCultureIgnoreCase)))
                .ForMember(dest => dest.SourceSystemKey, opts => opts.MapFrom(src => MappingHelper.MapVisitSourceSystemKey(src)))
                .ForMember(dest => dest.StatusDisplay, opts => opts.MapFrom(src => MappingHelper.MapVisitDisplayStatus(src)))
                .ForMember(dest => dest.VisitStateId, opts => opts.MapFrom(src => src.CurrentVisitState))
                .ForMember(dest => dest.FacilityLogoFilename, opts => opts.MapFrom(src => MappingHelper.MapFacilityLogo(src.Facility)))
                .ForMember(dest => dest.FacilityDescription, opts => opts.MapFrom(src => MappingHelper.MapFacilityDescription(src.Facility)));

            // visit transactions
            this.CreateMap<VisitTransactionsSearchFilterViewModel, VisitTransactionFilterDto>();
            this.CreateMap<GridSearchModel<VisitTransactionsSearchFilterViewModel>, VisitTransactionFilterDto>()
                .ForMember(dest => dest.SortField, opts => opts.MapFrom(src => src.Sidx))
                .ForMember(dest => dest.SortOrder, opts => opts.MapFrom(src => src.Sord))
                .AfterMap((src, dest) => { Mapper.Map(src.Filter, dest); });

            this.CreateMap<VisitTransactionDto, VisitTransactionsSearchResultViewModel>()
                .ForMember(dest => dest.DisplayDate, opts => opts.MapFrom(x => x.DisplayDate.ToDateText()))
                .ForMember(dest => dest.TransactionAmount, opts => opts.MapFrom(x => x.TransactionAmount.FormatCurrency()));

            this.CreateMap<RegistrationMatchFieldConfigurationDto, RegisterViewModelElementConfig>();

            this.CreateMap<AddressViewModel, AddressDto>();
            this.CreateMap<AddressDto, AddressViewModel>();
            this.CreateMap<VisitPayUserAddress, AddressDto>()
                .ForMember(dest => dest.PostalCode, opts => opts.MapFrom(src => src.PostalCode));

            // reconfig
            this.CreateMap<ReconfigureFinancePlanDto, ReconfigureFinancePlanViewModel>()
                .ForMember(dest => dest.FinancePlanIdToReconfigure, opts => opts.MapFrom(src => src.FinancePlanIdToReconfigure))
                .ForMember(dest => dest.FinancePlanOfferSetTypeId, opts => opts.MapFrom(src => MappingHelper.MapReconfiguredOfferType(src)))
                .ForMember(dest => dest.FinancePlanOfferSetTypes, opts => opts.MapFrom(src => src.FinancePlanOfferSetTypes))
                .ForMember(dest => dest.GuarantorFirstName, opts => opts.MapFrom(src => src.Guarantor.User.FirstName))
                .ForMember(dest => dest.GuarantorLastName, opts => opts.MapFrom(src => src.Guarantor.User.LastName))
                .ForMember(dest => dest.InterestRates, opts => opts.MapFrom(src => src.InterestRates))
                .ForMember(dest => dest.MaximumMonthlyPayments, opts => opts.MapFrom(src => src.FinancePlanBoundary.MaximumNumberOfPayments))
                .ForMember(dest => dest.MinimumPaymentAmount, opts => opts.MapFrom(src => src.FinancePlanBoundary.MinimumPaymentAmount))
                .ForMember(dest => dest.VpGuarantorId, opts => opts.MapFrom(src => src.Guarantor.VpGuarantorId))
                // original fp
                .ForMember(dest => dest.CurrentFinancePlanBalance, opts => opts.MapFrom(src => src.CurrentFinancePlanBalance.FormatCurrency()))
                .ForMember(dest => dest.CurrentFinancePlanBalanceDecimal, opts => opts.MapFrom(src => src.CurrentFinancePlanBalance))
                .ForMember(dest => dest.CurrentFinancePlanInterestRate, opts => opts.MapFrom(src => src.CurrentFinancePlanInterestRate.FormatPercentage()))
                .ForMember(dest => dest.CurrentFinancePlanOriginalBalance, opts => opts.MapFrom(src => src.CurrentFinancePlanOriginatedBalance.FormatCurrency()))
                .ForMember(dest => dest.CurrentFinancePlanNewCharges, opts => opts.MapFrom(src => src.CurrentFinancePlanNewCharges.FormatCurrency()))
                .ForMember(dest => dest.CurrentFinancePlanPaymentAmount, opts => opts.MapFrom(src => src.CurrentFinancePlanPaymentAmount.FormatCurrency()))
                // new fp
                .ForMember(dest => dest.NewFinancePlanBalance, opts => opts.MapFrom(src => src.NewFinancePlanBalance.FormatCurrency()))
                // terms
                .ForMember(dest => dest.Terms, opts => opts.MapFrom(src => src.ReconfiguredTerms));
        }
    }
}
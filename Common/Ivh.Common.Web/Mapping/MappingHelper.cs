﻿namespace Ivh.Common.Web.Mapping
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Linq.Expressions;
    using System.Reflection;
    using System.Web.Mvc;
    using Base.Utilities.Extensions;
    using Base.Utilities.Helpers;
    using Constants;
    using Ivh.Application.Base.Common.Interfaces.Entities.Visit;
    using Ivh.Application.Core.Common.Dtos;
    using Ivh.Application.FinanceManagement.Common.Dtos;
    using Ivh.Application.SecureCommunication.Common.Dtos;
    using Ivh.Application.User.Common.Dtos;
    using Utilities;
    using VisitPay.Constants;
    using VisitPay.Enums;
    using VisitPay.Strings;

    public static class MappingHelper
    {
        #region date helpers
        
        private static TimeZoneHelper _timeZoneHelper;

        private static TimeZoneHelper TimeZoneHelper => _timeZoneHelper ?? (_timeZoneHelper = DependencyResolver.Current.GetService<TimeZoneHelper>());

        /// <summary>
        /// Returns "MM/dd/yyyy" formatted "date without time" string in the client's time zone
        /// </summary>
        public static string MapToClientDateText(DateTime? value)
        {
            return MapToFormattedClientDateTimeText(value, Format.DateFormat);
        }

        /// <summary>
        /// Returns "MM/dd/yyyy hh:mm tt" formatted "date with time" string in the client's time zone
        /// </summary>
        public static string MapToClientDateTimeText(DateTime? value)
        {
            return MapToFormattedClientDateTimeText(value, Format.DateTimeFormat);
        }

        /// <summary>
        /// Returns string in the client's time zone in the specified format
        /// </summary>
        public static string MapToFormattedClientDateTimeText(DateTime? value, string format)
        {
            DateTime? clientDateTime = MapToClientDateTime(value);
            string formattedDateTime = clientDateTime.HasValue ? clientDateTime.Value.ToString(format) : "";
            return formattedDateTime;
        }

        /// <summary>
        /// Returns "MM/dd/yyyy hh:mm tt" formatted "date with time" string in VisitPay's operational time zone (currently Mountain Time)
        /// </summary>
        public static string MapToVisitPayDateTimeText(DateTime? value)
        {
            DateTime? utcValue = GetUtcDateTime(value);
            return utcValue.HasValue ? TimeZoneHelper.Operations.ToLocalDateTimeString(utcValue.Value) : "";
        }

        public static DateTime? MapToClientDateTime(DateTime? value)
        {
            DateTime? utcValue = GetUtcDateTime(value);
            return utcValue.HasValue ? TimeZoneHelper.Client.ToLocalDateTime(utcValue.Value) : (DateTime?)null;
        }

        private static DateTime? GetUtcDateTime(DateTime? value)
        {
            if (value.HasValue && value.Value.Kind == DateTimeKind.Local)
            {
                return value.Value.ToUniversalTime();
            }

            return value;
        }

        #endregion
        
        public static bool IsScheduledPayment(PaymentTypeEnum paymentType)
        {
            return paymentType.IsInCategory(PaymentTypeEnumCategory.Scheduled);
        }

        public static string MapClientUserName(VisitPayUserDto clientUser)
        {
            if (clientUser == null)
            {
                return "";
            }

            if (clientUser.VisitPayUserId == SystemUsers.SystemUserId)
            {
                return "System";
            }

            return clientUser.UserName;
        }
        
        public static string MapConsolidationGuarantorField(ConsolidationGuarantorDto consolidationGuarantorDto, string value, bool cancelledByField = false)
        {
            if (consolidationGuarantorDto.IsInactive && (string.IsNullOrEmpty(value) || string.IsNullOrWhiteSpace(value)))
            {
                if (cancelledByField && consolidationGuarantorDto.CancelledByUser.VisitPayUserId == SystemUsers.SystemUserId)
                {
                    switch (consolidationGuarantorDto.ConsolidationGuarantorCancellationReason)
                    {
                        case ConsolidationGuarantorCancellationReasonEnum.ExpiredBySystem:
                            return "Request Expired";
                        case ConsolidationGuarantorCancellationReasonEnum.GuarantorAccountCancelled:
                            return "Account Cancellation";
                    }
                }
                return "N/A";
            }

            return value;
        }
        
        public static string MapConsolidationGuarantorStatus(ConsolidationGuarantorDto consolidationGuarantorDto)
        {
            if (consolidationGuarantorDto.IsPending)
            {
                return "Pending";
            }

            if (consolidationGuarantorDto.IsInactive)
            {
                return "Inactive";
            }

            if (consolidationGuarantorDto.ConsolidationGuarantorStatus == ConsolidationGuarantorStatusEnum.Accepted)
            {
                return "Active";
            }

            return consolidationGuarantorDto.ConsolidationGuarantorStatus.ToString();
        }

        public static string MapFacilityDescription(FacilityDto facilityDto)
        {
            return (facilityDto?.FacilityDescription ?? string.Empty).Trim();
        }

        public static string MapFacilityLogo(FacilityDto facilityDto)
        {
            if (facilityDto == null)
            {
                return null;
            }

            if (string.IsNullOrWhiteSpace(facilityDto.LogoFilename))
            {
                return null;
            }

            return $"{UrlPath.ClientImagesPath}{facilityDto.LogoFilename}";
        }

        public static string MapItemizationFacilityCode(VisitItemizationStorageDto visitItemizationStorageDto)
        {
            return visitItemizationStorageDto.FacilityDescription.IsNotNullOrEmpty() ? visitItemizationStorageDto.FacilityDescription : visitItemizationStorageDto.FacilityCode;
        }

        public static IList<PaymentTypeEnum> MapPaymentType(int? paymentTypeId)
        {
            if (!paymentTypeId.HasValue)
            {
                return null;
            }

            switch (paymentTypeId.Value)
            {
                case 1:
                    return EnumHelper<PaymentTypeEnum>.GetValues(PaymentTypeEnumCategory.Manual).ToList();
                case 11:
                    return new List<PaymentTypeEnum> { PaymentTypeEnum.RecurringPaymentFinancePlan };
                case 40:
                    return new List<PaymentTypeEnum> { PaymentTypeEnum.Void };
                case 41:
                    return new List<PaymentTypeEnum> { PaymentTypeEnum.Refund };
                case 42:
                    return new List<PaymentTypeEnum> { PaymentTypeEnum.PartialRefund };
            }

            return null;
        }

        public static string MapFinancePlanOriginatedBalance(FinancePlanDto financePlan)
        {
            return MapFinancePlanOriginatedBalance(financePlan.FinancePlanStatus.FinancePlanStatusEnum, financePlan.OriginatedFinancePlanBalance);
        }

        public static string MapFinancePlanOriginatedBalance(FinancePlanStatusEnum financePlanStatus, decimal originatedFinancePlanBalance)
        {
            return financePlanStatus.IsInCategory(FinancePlanStatusEnumCategory.Pending) ? "Pending" : originatedFinancePlanBalance.FormatCurrency();
        }
        
        public static string MapFinancePlanTerms(FinancePlanDto financePlan)
        {
            return $"{financePlan.CurrentInterestRate.FormatPercentage()} - {MapToClientDateText(financePlan.LastPaymentDate)}";
        }

        public static int MapReconfiguredOfferType(ReconfigureFinancePlanDto src)
        {
            return src.ReconfiguredTerms?.FinancePlanOfferSetType?.FinancePlanOfferSetTypeId ?? (int)FinancePlanOfferSetTypeEnum.GuarantorOffers;
        }

        public static string MapPaymentDueDayChangedBy(GuarantorPaymentDueDayHistoryDto guarantorPaymentDueDayHistoryDto)
        {
            if (guarantorPaymentDueDayHistoryDto.InsertUser.VisitPayUserId == SystemUsers.SystemUserId
                && !string.IsNullOrEmpty(guarantorPaymentDueDayHistoryDto.ChangeReason))
            {
                return guarantorPaymentDueDayHistoryDto.ChangeReason;
            }

            return guarantorPaymentDueDayHistoryDto.InsertUser.DisplayFirstNameLastName;
        }
        
        public static string MapSupportRequestSubmittedFor(SupportRequestDto supportRequestDto, bool reverse)
        {
            GuarantorDto guarantor = supportRequestDto.SubmittedForVpGuarantor ?? supportRequestDto.VpGuarantor;
            return reverse ? guarantor.User.DisplayLastNameFirstName : guarantor.User.DisplayFirstNameLastName;
        }
        
        public static string MapUserFullName(VisitPayUserDto visitPayUserDto)
        {
            if (visitPayUserDto == null)
            {
                return string.Empty;
            }

            return visitPayUserDto.DisplayFirstNameLastName;
        }
        
        public static string MapVisitBillingApplication(IVisit visitDto)
        {
            return MapVisitBillingApplication(visitDto.BillingApplication);
        }

        public static string MapVisitBillingApplication(string billingApplication)
        {
            return string.Equals(billingApplication, BillingApplicationConstants.HB, StringComparison.CurrentCultureIgnoreCase) ? BillingApplicationConstants.HBDisplay : BillingApplicationConstants.PBDisplay;
        }
        
        public static string MapVisitDisplayStatus(VisitDto visit)
        {
            string displayStatus = string.Empty;

            // finance plan shouldn't show status, if any
            if (visit.CurrentVisitStateDerivedEnum == VisitStateDerivedEnum.OnFinancePlan)
            {
                return LocalizationHelper.GetLocalizedString(TextRegionConstants.Financed);
            }

            // ascending display priority order
            if (visit.IsPastDue)
            {
                displayStatus = LocalizationHelper.GetLocalizedString(TextRegionConstants.PastDue);
            }

            if (visit.IsFinalPastDue || visit.IsMaxAgeActive)
            {
                displayStatus = LocalizationHelper.GetLocalizedString(TextRegionConstants.FinalPastDue);
            }
            
            if (visit.CurrentVisitState == VisitStateEnum.Inactive)
            {
                displayStatus = LocalizationHelper.GetLocalizedString(TextRegionConstants.Closed);
            }

            if (visit.BillingHold)
            {
                displayStatus = LocalizationHelper.GetLocalizedString(TextRegionConstants.StatusDisplayOnHold);
            }

            if (visit.CurrentVisitState == VisitStateEnum.Active && string.IsNullOrWhiteSpace(displayStatus))
            {
                displayStatus = LocalizationHelper.GetLocalizedString(TextRegionConstants.DueNow);
            }
            
            return displayStatus;
        }

        public static string MapVisitSourceSystemKey(PaymentVisitDto paymentVisitDto)
        {
            return MapVisitSourceSystemKey(paymentVisitDto, x => x.SourceSystemKey);
        }

        public static string MapVisitSourceSystemKey(VisitDto visit)
        {
            return MapVisitSourceSystemKey(visit, x => x.SourceSystemKey);
        }

        public static string MapVisitSourceSystemKey<T>(T visit, Expression<Func<T, object>> expression)
        {
            if (visit == null)
            {
                return string.Empty;
            }

            PropertyInfo propertyInfo = (PropertyInfo)((MemberExpression)expression.Body).Member;
            
            // redact
            string redactedVisitReplacementValue = SettingsHelper.RedactedVisitReplacementValue;
            string sourceSystemKey = propertyInfo.GetValue(visit, null) as string;
            if (string.IsNullOrEmpty(sourceSystemKey))
            {
                return redactedVisitReplacementValue;
            }
            
            return sourceSystemKey;
        }
    }
}

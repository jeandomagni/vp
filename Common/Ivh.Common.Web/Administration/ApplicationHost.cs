﻿namespace Ivh.Common.Web.Administration
{
    using System;
    using System.Collections.Generic;
    using System.Web.Hosting;
    using Microsoft.Web.Administration;

    public class ApplicationHost
    {
        public static bool ConfigureStartUp()
        {
            try
            {
                using (ServerManager serverManager = new ServerManager())
                {
                    Site site = serverManager.Sites[HostingEnvironment.SiteName];
                    if (site?.Applications != null)
                    {
                        foreach (Application application in site.Applications)
                        {
                            if (application != null)
                            {
                                application["preloadEnabled"] = true;

                                ApplicationPool applicationPool = serverManager.ApplicationPools[application.ApplicationPoolName];
                                {
                                    if (applicationPool != null)
                                    {
                                        applicationPool["startMode"] = "AlwaysRunning";
                                    }
                                }
                            }
                        }

                        serverManager.CommitChanges();

                        return true;
                    }

                    return false;
                }
            }
            catch (Exception)
            {
                return false;
            }
        }
    }
}
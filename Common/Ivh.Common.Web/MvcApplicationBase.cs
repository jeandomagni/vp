﻿namespace Ivh.Common.Web
{
    using System;
    using System.Diagnostics;
    using System.Runtime.InteropServices;
    using System.Web;
    using System.Web.Mvc;
    using Application;
    using Autofac;
    using Base.Utilities.Helpers;
    using Controllers;
    using DependencyInjection;
    using Ivh.Application.Logging.Common.Interfaces;

    public abstract class MvcApplicationBase : HttpApplicationBase
    {
        private readonly string _applicationName;

        protected MvcApplicationBase(string applicationName)
        {
            this._applicationName = applicationName;
        }

        protected override void Application_Start()
        {
            ViewEngines.Engines.Clear();
            ViewEngines.Engines.Add(new RazorViewEngine());
            /*
            var ve = new RazorViewEngine();
            ve.ViewLocationCache = new TwoLevelViewCache(ve.ViewLocationCache);
            ViewEngines.Engines.Add(ve);
             */

            ControllerBuilder.Current.SetControllerFactory(typeof(SessionStateControllerFactory));

            base.Application_Start();
        }

        protected override void Application_End(object sender, EventArgs e)
        {
            /*
             * Have to unload the libsass DLL when the application ends, otherwise it can be
             * locked by IIS worker process and it will cause builds to fail.
             * 
             * Reference: https://github.com/sass/libsass-net/issues/55
             */
            UnloadImportedDll("libsass.DLL");

            base.Application_End(sender, e);
        }

        protected void Application_EndRequest()
        {
            HttpContextWrapper context = new HttpContextWrapper(this.Context);
            // If we're an ajax request, and doing a 302, then we actually need to do a 401
            if (this.Context.Response.StatusCode != 302 ||
                !context.Request.IsAjaxRequest())
            {
                return;
            }

            this.Context.Response.Clear();
            this.Context.Response.StatusCode = 401;
        }

        protected void Application_Error(object sender, EventArgs e)
        {
            try
            {
                Exception ex = this.Server.GetLastError();
                ILoggingApplicationService logger = IvinciContainer.Instance.Container().Resolve<ILoggingApplicationService>();
                logger.Fatal(() => $"{this._applicationName}::Application_Error, exception = {ExceptionHelper.AggregateExceptionToString(ex)}");
            }
            catch (Exception)
            {
                //
            }
        }
        
        [DllImport("kernel32", SetLastError = true)]
        private static extern bool FreeLibrary(IntPtr hModule);

        public static void UnloadImportedDll(string moduleName)
        {
            try
            {
                ProcessModuleCollection modules = Process.GetCurrentProcess().Modules;
                foreach (ProcessModule mod in modules)
                {
                    if (string.Equals(mod.ModuleName, moduleName, StringComparison.CurrentCultureIgnoreCase))
                    {
                        FreeLibrary(mod.BaseAddress);
                    }
                }
            }
            catch (Exception)
            {
                // suppress
            }
        }
    }
}
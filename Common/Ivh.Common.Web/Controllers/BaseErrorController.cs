﻿namespace Ivh.Common.Web.Controllers
{
    using System;
    using System.Web.Mvc;
    using System.Web.SessionState;
    using Interfaces;

    public class BaseErrorController : BaseWebController
    {
        protected BaseErrorController(Lazy<IBaseControllerService> baseControllerService)
            : base(baseControllerService)
        {
        }

        public virtual ActionResult Index()
        {
            this.Response.StatusCode = 403;
            return this.View();
        }

        public virtual ActionResult NotFound()
        {
            this.Response.StatusCode = 404;
            return this.View();
        }

        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult WindowOnError(string message, string file, string line, string col, string error)
        {
            try
            {
                this.Logger.Value.Fatal(() => $"message: {message}, file: {file}, line: {line}, col: {col}, error: {error}");
            }
            catch
            {
                // just in case
            }
            return new EmptyResult();
        }
        
        [ValidateAntiForgeryToken]
        [HttpPost]
        [AllowAnonymous]
        public virtual JsonResult LogSecurePanError(string errorMessage)
        {
            try
            {
                this.Logger.Value.Fatal(() => $"SecurePanError: {errorMessage}; UserAgent = {this.Request.UserAgent}");
            }
            catch
            {
                // just in case
            }
            return this.Json(true);
        }
    }
}
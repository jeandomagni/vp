﻿namespace Ivh.Common.Web.Controllers
{
    using System;
    using System.Threading.Tasks;
    using System.Web;
    using System.Web.Mvc;
    using Ivh.Application.Content.Common.Interfaces;
    using Ivh.Application.Core.Common.Dtos;
    using Ivh.Application.Core.Common.Interfaces;
    using Ivh.Application.Monitoring.Common.Dtos;
    using Ivh.Application.Monitoring.Common.Interfaces;
    using Ivh.Application.Settings.Common.Interfaces;
    using AutoMapper;
    using Base.Utilities.Extensions;
    using Base.Utilities.Helpers;
    using Domain.Settings.Interfaces;
    using Models.Monitoring;
    using Newtonsoft.Json;

    [AllowAnonymous]
    public abstract class BaseMonitoringController : Controller
    {
        private readonly Lazy<ISystemHealthApplicationService> _systemHealthApplicationService;
        private readonly Lazy<IApplicationSettingsService> _applicationSettingsService;
        private readonly Lazy<ISettingsApplicationService> _settingsApplicationService;
        private readonly Lazy<IContentApplicationService> _contentApplicationService;

        protected BaseMonitoringController(
            Lazy<ISystemHealthApplicationService> systemHealthApplicationService,
            Lazy<IApplicationSettingsService> applicationSettingsService,
            Lazy<ISettingsApplicationService> settingsApplicationService,
            Lazy<IContentApplicationService> contentApplicationService
            )
        {
            this._systemHealthApplicationService = systemHealthApplicationService;
            this._applicationSettingsService = applicationSettingsService;
            this._settingsApplicationService = settingsApplicationService;
            this._contentApplicationService = contentApplicationService;
        }

        [HttpGet]
        public virtual ActionResult Index(Guid? authenticationToken)
        {
            return this.SystemHealth(authenticationToken);
        }

        [HttpGet]
        public virtual ActionResult SystemHealth(Guid? authenticationToken)
        {
            return this.Authenticate(authenticationToken, () =>
            {
                SystemHealthDetailsDto systemHealthDetailsDto = this._systemHealthApplicationService.Value.GetSystemHealthDetails();
                SystemHealthViewModel monitoringViewModel = Mapper.Map<SystemHealthViewModel>(systemHealthDetailsDto);
                monitoringViewModel.Raw = JsonConvert.SerializeObject(systemHealthDetailsDto, Formatting.Indented)
                    .Replace("true", "success")
                    .Replace("false", "failed");

                return this.View("SystemHealth", monitoringViewModel);
            });
        }

        [HttpGet]
        public virtual ActionResult SystemInfo(Guid? authenticationToken)
        {
            return this.Authenticate(authenticationToken, () =>
            {
                SystemInfoDetailsDto systemInfoDetailsDto = this._systemHealthApplicationService.Value.GetSystemInfoDetails();
                SystemInfoViewModel systemInfoViewModel = Mapper.Map<SystemInfoViewModel>(systemInfoDetailsDto);
                systemInfoViewModel.Raw = JsonConvert.SerializeObject(systemInfoDetailsDto, Formatting.Indented);

                return this.View("SystemInfo", systemInfoViewModel);
            });
        }

        [HttpGet]
        public virtual JsonResult Json(Guid? authenticationToken)
        {
            return this.Authenticate(authenticationToken, () =>
            {
               SystemHealthDetailsDto systemHealthDetailsDto = this._systemHealthApplicationService.Value.GetSystemHealthDetails();
                return this.Json(systemHealthDetailsDto, JsonRequestBehavior.AllowGet);
            });
        }

        public virtual ActionResult InvalidateSettingsCache(Guid? authenticationToken)
        {
            return this.Authenticate(authenticationToken, () =>
            {
                if (Task.Run(async () => await this._settingsApplicationService.Value.InvalidateCacheAsync()).Result)
                {
                    return this.Content("Settings Cache Invalidation Succeeded");
                }
                return this.Content("Settings Cache Invalidation Failed");
            });
        }

        public virtual ActionResult InvalidateContentCache(Guid? authenticationToken)
        {
            return this.Authenticate(authenticationToken, () =>
            {
                if (Task.Run(async () => await this._contentApplicationService.Value.InvalidateCacheAsync()).Result)
                {
                    return this.Content("Content Cache Invalidation Succeeded");
                }
                return this.Content("Content Cache Invalidation Failed");
            });
        }

        private ActionResult Authenticate(Guid? authenticationToken, Func<ActionResult> func)
        {
            string monitoringAuthenticationToken = this._applicationSettingsService.Value.MonitoringAuthenticationToken.Value;
            Guid authenticationTokenGuid = default(Guid);
            if (IvhEnvironment.IsDevelopmentOrQualityAssurance
                || (authenticationToken.HasValue
                    && monitoringAuthenticationToken.IsNotNullOrEmpty()
                    && Guid.TryParse(monitoringAuthenticationToken, out authenticationTokenGuid)
                    && authenticationTokenGuid == authenticationToken))
            {
                return func();
            }
            else
            {
                throw new HttpException(404, string.Empty);
            }
        }

        private JsonResult Authenticate(Guid? authenticationToken, Func<JsonResult> func)
        {
            string monitoringAuthenticationToken = this._applicationSettingsService.Value.MonitoringAuthenticationToken.Value;
            Guid authenticationTokenGuid = default(Guid);
            if (IvhEnvironment.IsDevelopmentOrQualityAssurance
                || (authenticationToken.HasValue
                    && monitoringAuthenticationToken.IsNotNullOrEmpty()
                    && Guid.TryParse(monitoringAuthenticationToken, out authenticationTokenGuid)
                    && authenticationTokenGuid == authenticationToken))
            {
                return func();
            }
            else
            {
                throw new HttpException(404, string.Empty);
            }
        }
    }
}
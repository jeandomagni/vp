﻿namespace Ivh.Common.Web.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.Linq;
    using System.Threading.Tasks;
    using System.Web;
    using System.Web.Mvc;
    using AutoMapper;
    using Base.Utilities.Extensions;
    using Constants;
    using EventJournal;
    using Interfaces;
    using Ivh.Application.Content.Common.Interfaces;
    using Ivh.Application.Core.Common.Dtos;
    using Ivh.Application.Core.Common.Interfaces;
    using Ivh.Application.FinanceManagement.Common.Dtos;
    using Ivh.Application.FinanceManagement.Common.Interfaces;
    using Models.FinancePlan;
    using Models.Payment;
    using Provider.Pdf;
    using VisitPay.Constants;
    using VisitPay.Enums;
    using VisitPay.Strings;

    [Authorize(Roles = VisitPayRoleStrings.System.Patient + "," + VisitPayRoleStrings.Admin.Administrator)]
    public abstract class BaseFinancePlanController : BaseWebController
    {
        protected readonly Lazy<IContentApplicationService> ContentApplicationService;
        protected readonly Lazy<IGuarantorApplicationService> GuarantorApplicationService;
        protected readonly Lazy<IFinancialDataSummaryApplicationService> FinancialDataSummaryApplicationService;
        protected readonly Lazy<IFinancePlanApplicationService> FinancePlanApplicationService;
        protected readonly Lazy<IPaymentDetailApplicationService> PaymentDetailApplicationService;
        protected readonly Lazy<IPaymentMethodsApplicationService> PaymentMethodsApplicationService;
        protected readonly Lazy<IReconfigureFinancePlanApplicationService> ReconfigureFinancePlanApplicationService;
        protected readonly Lazy<IVisitPayUserApplicationService> VisitPayUserApplicationService;
        protected readonly Lazy<IVisitApplicationService> VisitApplicationService;
        protected readonly Lazy<IPdfConverter> PdfConverter;

        protected BaseFinancePlanController(
            Lazy<IBaseControllerService> baseControllerService,
            Lazy<IContentApplicationService> contentApplicationService,
            Lazy<IFinancialDataSummaryApplicationService> financialDataSummaryApplicationService,
            Lazy<IFinancePlanApplicationService> financePlanApplicationService,
            Lazy<IGuarantorApplicationService> guarantorApplicationService,
            Lazy<IPaymentDetailApplicationService> paymentDetailApplicationService,
            Lazy<IPaymentMethodsApplicationService> paymentMethodsApplicationService,
            Lazy<IReconfigureFinancePlanApplicationService> reconfigureFinancePlanApplicationService,
            Lazy<IVisitApplicationService> visitApplicationService,
            Lazy<IVisitPayUserApplicationService> visitPayUserApplicationService,
            Lazy<IPdfConverter> pdfConverter)
            : base(baseControllerService)
        {
            this.ContentApplicationService = contentApplicationService;
            this.FinancialDataSummaryApplicationService = financialDataSummaryApplicationService;
            this.FinancePlanApplicationService = financePlanApplicationService;
            this.GuarantorApplicationService = guarantorApplicationService;
            this.PaymentDetailApplicationService = paymentDetailApplicationService;
            this.PaymentMethodsApplicationService = paymentMethodsApplicationService;
            this.ReconfigureFinancePlanApplicationService = reconfigureFinancePlanApplicationService;
            this.VisitApplicationService = visitApplicationService;
            this.VisitPayUserApplicationService = visitPayUserApplicationService;
            this.PdfConverter = pdfConverter;
        }

        [NonAction]
        protected async Task<FinancePlanTermsResponseViewModel> ValidateFinancePlanAsync(int vpGuarantorId, int statementId, decimal? monthlyPaymentAmount, int? numberMonthlyPayments, int? financePlanOfferSetTypeId, bool combineFinancePlans, int? paymentDueDay, List<int> selectedVisits, string financePlanOptionStateCode)
        {
            if (!monthlyPaymentAmount.HasValue && !numberMonthlyPayments.HasValue)
            {
                return new FinancePlanTermsResponseViewModel(string.Empty, null, null);
            }

            //VP-4757 - default to PaymentDueDateDefaultDaysInFuture days out
            if (!paymentDueDay.HasValue)
            {
                int suggestedDay = DateTime.UtcNow.AddDays(this.ClientDto.PaymentDueDateDefaultDaysInFuture).Day;
                paymentDueDay = suggestedDay.ToValidPaymentDueDay();
            }

            CalculateFinancePlanTermsResponseDto response;
            if (monthlyPaymentAmount.HasValue)
            {
                FinancePlanCalculationParametersDto terms = FinancePlanCalculationParametersDto.CreateForMonthlyPayment(vpGuarantorId, statementId, monthlyPaymentAmount.Value, financePlanOfferSetTypeId, combineFinancePlans, paymentDueDay, selectedVisits, financePlanOptionStateCode);
                response = await this.FinancePlanApplicationService.Value.GetTermsByMonthlyPaymentAmountAsync(terms).ConfigureAwait(false);
            }
            else
            {
                FinancePlanCalculationParametersDto terms = FinancePlanCalculationParametersDto.CreateForNumberOfPayments(vpGuarantorId, statementId, numberMonthlyPayments.Value, financePlanOfferSetTypeId, combineFinancePlans, paymentDueDay, selectedVisits, financePlanOptionStateCode);
                response = await this.FinancePlanApplicationService.Value.GetTermsByNumberOfMonthlyPaymentsAsync(terms).ConfigureAwait(false);
            }

            if (response.IsError)
            {
                return new FinancePlanTermsResponseViewModel(response.ErrorMessage, response.MinimumPaymentAmount, response.MaximumMonthlyPayments);
            }
            
            await this.FinancePlanApplicationService.Value.LogTermsDisplayedAsync(response.Terms, response.MinimumPaymentAmount, response.MaximumMonthlyPayments);
            
            FinancePlanTermsViewModel model = Mapper.Map<FinancePlanTermsViewModel>(response.Terms);

            return new FinancePlanTermsResponseViewModel(model, response.MinimumPaymentAmount, response.MaximumMonthlyPayments);
        }

        [NonAction]
        protected async Task<FinancePlanResultsModel> FinancePlanSubmitAsync(ConfirmFinancePlanViewModel model, int filteredVisitPayUserId, int vpGuarantorId)
        {
            int actionVisitPayUserId = this.CurrentUserId;
            HttpContext httpContext = System.Web.HttpContext.Current;

            CreateFinancePlanResponse response;
            
            FinancePlanCalculationParametersDto financePlanCalculationParametersDto = FinancePlanCalculationParametersDto.CreateForSubmit(
                vpGuarantorId,
                model.StatementId,
                model.MonthlyPaymentAmount,
                model.FinancePlanOfferSetTypeId,
                model.OfferCalculationStrategy,
                model.CombineFinancePlans,
                model.FinancePlanOptionStateCode);

            if (model.FinancePlanId.HasValue)
            {
                response = await this.FinancePlanApplicationService.Value.UpdatePendingFinancePlanAsync(
                    financePlanCalculationParametersDto,
                    model.FinancePlanId.Value,
                    model.FinancePlanOfferId,
                    filteredVisitPayUserId,
                    model.TermsCmsVersionId,
                    actionVisitPayUserId,
                    Mapper.Map<JournalEventHttpContextDto>(httpContext)).ConfigureAwait(true);
            }
            else
            {
                response = await this.FinancePlanApplicationService.Value.CreateFinancePlanAsync(
                    this.CurrentUserId,
                    filteredVisitPayUserId,
                    financePlanCalculationParametersDto,
                    model.TermsCmsVersionId,
                    model.FinancePlanOfferId,
                    actionVisitPayUserId,
                    Mapper.Map<JournalEventHttpContextDto>(httpContext)).ConfigureAwait(true);
            }

            if (response.IsError)
            {
                return new FinancePlanResultsModel(false, response.ErrorMessage);
            }

            PaymentSummaryListViewModel summaries = new PaymentSummaryListViewModel
            {
                Email = this.VisitPayUserApplicationService.Value.FindById(filteredVisitPayUserId.ToString()).Email,
                RedirectUrl = this.Url.Action("Index", "Home"),
                IsFinancePlanWithoutPayment = true,
                FinancePlanFirstPaymentDueDate = response.FinancePlan.FirstPaymentDueDate ?? DateTime.UtcNow
            };
            
            this.ViewData.Model = summaries;

            string partialViewName = PartialViewConstants.ArrangePaymentReceipt;
            if (this.IsFeatureEnabled(VisitPayFeatureEnum.OfflineVisitPay))
            {
                if (response.FinancePlan?.VpGuarantor?.IsOfflineGuarantor ?? false)
                {
                    partialViewName = PartialViewConstants.OfflineFinancePlanReceipt;
                    this.GuarantorApplicationService.Value.SetVpccPendingMatchesToMatched(response.FinancePlan.VpGuarantor.VpGuarantorId);
                }
            }

            FinancePlanResultsModel financePlanResults = new FinancePlanResultsModel(true, this.RenderPartialViewToString(partialViewName))
            {
                IsCombined = response.FinancePlan != null && response.FinancePlan.IsCombined,
                FinancePlanId = response.FinancePlan?.FinancePlanId,
                PaymentId = response.Payments != null && response.Payments.Any(x => !x.IsError && x.Payment != null) ? (int?)response.Payments.First(x => !x.IsError && x.Payment != null).Payment.PaymentId : null
            };

            return financePlanResults;
        }
        
        [NonAction]
        protected async Task<ReconfigurationActionResponseDto> SubmitReconfiguredFinancePlanAsync(int vpGuarantorId, int financePlanIdToReconfigure, decimal monthlyPaymentAmount, int? financePlanOfferSetTypeId)
        {
            int loggedInVisitPayUserId = this.CurrentUserId;
            HttpContext context = System.Web.HttpContext.Current;
            JournalEventHttpContextDto journalContext = Mapper.Map<JournalEventHttpContextDto>(context);
            bool sendEmail = this.ApplicationSettingsService.Value.IsClientApplication.Value;

            FinancePlanCalculationParametersDto financePlanCalculationParametersDto = FinancePlanCalculationParametersDto.CreateForReconfigurationTermsCheck(vpGuarantorId, monthlyPaymentAmount, null, financePlanOfferSetTypeId);
            ReconfigurationActionResponseDto responseDto = await this.ReconfigureFinancePlanApplicationService.Value.ReconfigureFinancePlanAsync(financePlanIdToReconfigure, financePlanCalculationParametersDto, loggedInVisitPayUserId, journalContext, sendEmail);

            return responseDto;
        }

        [NonAction]
        protected async Task<FinancePlanTermsResponseViewModel> ValidateReconfiguredFinancePlanAsync(int vpGuarantorId, int financePlanId, decimal? monthlyPaymentAmount, int? numberMonthlyPayments, int? financePlanOfferSetTypeId)
        {
            if (!monthlyPaymentAmount.HasValue && !numberMonthlyPayments.HasValue)
            {
                return new FinancePlanTermsResponseViewModel(string.Empty, null, null);
            }
            
            FinancePlanCalculationParametersDto parametersDto = FinancePlanCalculationParametersDto.CreateForReconfigurationTermsCheck(vpGuarantorId, monthlyPaymentAmount, numberMonthlyPayments, financePlanOfferSetTypeId);
            CalculateFinancePlanTermsResponseDto response;
            if (monthlyPaymentAmount.HasValue)
            {
                response = await this.ReconfigureFinancePlanApplicationService.Value.GetTermsByMonthlyPaymentAmountAsync(financePlanId, parametersDto);
            }
            else
            {
                response = await this.ReconfigureFinancePlanApplicationService.Value.GetTermsByNumberOfMonthlyPaymentsAsync(financePlanId, parametersDto);
            }

            if (response.IsError)
            {
                return new FinancePlanTermsResponseViewModel(response.ErrorMessage, response.MinimumPaymentAmount, response.MaximumMonthlyPayments);
            }
            
            await this.FinancePlanApplicationService.Value.LogTermsDisplayedAsync(response.Terms, response.MinimumPaymentAmount, response.MaximumMonthlyPayments);
            
            FinancePlanTermsViewModel model = Mapper.Map<FinancePlanTermsViewModel>(response.Terms);

            return new FinancePlanTermsResponseViewModel(model, response.MinimumPaymentAmount, response.MaximumMonthlyPayments);
        }
        
        [NonAction]
        protected async Task<ReconfigureFinancePlanViewModel> GetReconfigureTermsModelAsync(int financePlanIdToReconfigure, int vpGuarantorId, int? financePlanOfferSetTypeId)
        {
            ReconfigureFinancePlanDto reconfigureInformationDto = await this.ReconfigureFinancePlanApplicationService.Value.GetReconfigurationInformation(vpGuarantorId, financePlanIdToReconfigure, financePlanOfferSetTypeId, this.ApplicationSettingsService.Value.IsClientApplication.Value);
            if (reconfigureInformationDto == null)
            {
                this.Logger.Value.Fatal(() => $"{nameof(this.GetReconfigureTermsModelAsync)}: {nameof(reconfigureInformationDto)} is null");
                string errorMessage = await this.ContentApplicationService.Value.GetTextRegionAsync(TextRegionConstants.ReconfigurationNotEligible);
                return new ReconfigureFinancePlanViewModel { ErrorMessage = errorMessage };
            }

            if (reconfigureInformationDto.FinancePlanBoundary.MinimumPaymentAmount <= 0m)
            {
                this.Logger.Value.Fatal(() => $"{nameof(this.GetReconfigureTermsModelAsync)}: minimum payment amount <= 0");
                string errorMessage = await this.ContentApplicationService.Value.GetTextRegionAsync(TextRegionConstants.ReconfigurationNotEligible);
                return new ReconfigureFinancePlanViewModel { ErrorMessage = errorMessage };
            }

            ReconfigureFinancePlanViewModel model = Mapper.Map<ReconfigureFinancePlanViewModel>(reconfigureInformationDto);
            
            return model;
        }

        [NonAction]
        protected FinancePlansSearchResultsViewModel FinancePlans(int vpGuarantorId, FinancePlansSearchFilterViewModel model, int page, int rows, string sidx, string sord, int? currentGuarantorId = null)
        {
            FinancePlanFilterDto financePlanFilterDto = new FinancePlanFilterDto
            {
                SortField = sidx,
                SortOrder = sord
            };
            Mapper.Map(model, financePlanFilterDto);

            GuarantorDto currentGuarantor = this.GuarantorApplicationService.Value.GetGuarantor(currentGuarantorId.GetValueOrDefault(vpGuarantorId));

            FinancePlanResultsDto financePlanResultsDto = this.FinancePlanApplicationService.Value.GetFinancePlans(0, financePlanFilterDto, page, rows, vpGuarantorId);
            List<FinancePlansSearchResultViewModel> viewModels = Mapper.Map<List<FinancePlansSearchResultViewModel>>(financePlanResultsDto.FinancePlans);
            foreach (FinancePlansSearchResultViewModel financePlansSearchResultViewModel in viewModels)
            {
                financePlansSearchResultViewModel.IsManaged = currentGuarantor.ConsolidationStatus == GuarantorConsolidationStatusEnum.Managed;
            }

            return new FinancePlansSearchResultsViewModel
            {
                FinancePlans = viewModels,
                TotalCurrentBalance = financePlanResultsDto.TotalCurrentBalance.ToString("C", new NumberFormatInfo {CurrencySymbol = "$", CurrencyNegativePattern = 1}),
                page = page,
                total = Convert.ToInt32(Math.Ceiling((decimal) financePlanResultsDto.TotalRecords/rows)),
                records = financePlanResultsDto.TotalRecords
            };
        }

        [NonAction]
        protected JsonResult FinancePlanInterestRateHistoryData(int financePlanId, int guarantorVisitPayUserId, int page, int rows, string sidx, string sord)
        {
            IList<FinancePlanInterestRateHistoryDto> financePlanInterestRateHistoryDto = this.FinancePlanApplicationService.Value.GetFinancePlanInterestRateHistory(guarantorVisitPayUserId, financePlanId);
            decimal total = Math.Ceiling((decimal) financePlanInterestRateHistoryDto.Count/rows);
            int records = financePlanInterestRateHistoryDto.Count;

            #region sort

            Func<FinancePlanInterestRateHistoryDto, object> orderByFunc = history => history.InsertDate;

            switch (sidx ?? string.Empty)
            {
                case "InsertDate":
                {
                    orderByFunc = history => history.InsertDate;
                    break;
                }
                case "InterestRate":
                {
                    orderByFunc = history => history.InterestRate;
                    break;
                }
                case "PaymentAmount":
                {
                    orderByFunc = history => history.PaymentAmount;
                    break;
                }
            }

            bool isAscendingOrder = "asc".Equals(sord, StringComparison.InvariantCultureIgnoreCase);
            financePlanInterestRateHistoryDto = ((isAscendingOrder) ? financePlanInterestRateHistoryDto.OrderBy(orderByFunc) : financePlanInterestRateHistoryDto.OrderByDescending(orderByFunc)).ToList();

            #endregion

            return this.Json(new
            {
                FinancePlanInterestRateHistory = Mapper.Map<List<FinancePlanInterestRateHistoryViewModel>>(financePlanInterestRateHistoryDto).Skip((page - 1)*rows).Take(rows),
                page,
                total,
                records
            });
        }
    }
}
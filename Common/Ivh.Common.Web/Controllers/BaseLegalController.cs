﻿namespace Ivh.Common.Web.Controllers
{
    using System;
    using System.Threading.Tasks;
    using System.Web.Mvc;
    using Ivh.Application.Content.Common.Dtos;
    using Ivh.Application.Content.Common.Interfaces;
    using Ivh.Application.Core.Common.Interfaces;
    using Ivh.Application.FinanceManagement.Common.Interfaces;
    using AutoMapper;
    using Base.Utilities.Helpers;
    using Interfaces;
    using Models.Shared;
    using Provider.Pdf;
    using VisitPay.Enums;
    using VisitPay.Strings;

    [Authorize(Roles = VisitPayRoleStrings.System.Patient + "," + VisitPayRoleStrings.Admin.Administrator)]
    public abstract class BaseLegalController : BaseWebController
    {
        protected readonly Lazy<IContentApplicationService> ContentApplicationService;
        protected readonly Lazy<ICreditAgreementApplicationService> CreditAgreementApplicationService;
        protected readonly Lazy<IGuarantorApplicationService> GuarantorApplicationService;
        protected readonly Lazy<IPdfConverter> PdfConverter;

        protected BaseLegalController(
            Lazy<IBaseControllerService> baseControllerService,
            Lazy<IGuarantorApplicationService> guarantorApplicationService,
            Lazy<IContentApplicationService> contentApplicationService,
            Lazy<ICreditAgreementApplicationService> creditAgreementApplicationService,
            Lazy<IPdfConverter> pdfConverter
            )
            : base(baseControllerService)
        {
            this.ContentApplicationService = contentApplicationService;
            this.CreditAgreementApplicationService = creditAgreementApplicationService;
            this.GuarantorApplicationService = guarantorApplicationService;
            this.PdfConverter = pdfConverter;
        }

        [HttpGet]
        [AllowAnonymous]
        public virtual async Task<ActionResult> Index()
        {
            string view;
            ContentMenuEnum contentMenuEnum;

            if (this.User.Identity.IsAuthenticated)
            {
                // normal web view (logged in - but mobile doesn't differentiate)
                view = this.IsMobile ? "Index" : "IndexAuth";
                contentMenuEnum = this.IsMobile ? ContentMenuEnum.LegalAgreementsMobile : ContentMenuEnum.LegalAgreementsPatient;
            }
            else
            {
                if (this.IsFeatureEnabled(VisitPayFeatureEnum.AlternatePublicPages))
                {
                    // responsive web view
                    view = "IndexPublicAlt";
                    contentMenuEnum = ContentMenuEnum.LegalAgreementsPatient;
                }
                else
                {
                    // normal web view (public - but mobile doesn't differentiate)
                    view = this.IsMobile ? "Index" : "IndexPublic";
                    contentMenuEnum = this.IsMobile ? ContentMenuEnum.LegalAgreementsMobile : ContentMenuEnum.LegalAgreementsPatient;
                }
            }
            
            ContentMenuDto contentMenuDto = await this.ContentApplicationService.Value.GetContentMenuAsync(contentMenuEnum, this.User.Identity.IsAuthenticated);

            return this.View(view, Mapper.Map<ContentMenuViewModel>(contentMenuDto));
        }

        #region esign

        [HttpGet]
        [AllowAnonymous]
        public ActionResult Esign()
        {
            return this.PartialView("_Esign", this.GetEsign());
        }

        [HttpGet]
        [AllowAnonymous]
        public ActionResult EsignPartial(bool showButton = true)
        {
            return this.PartialView("_Esign", this.GetEsign(showButton));
        }

        [HttpGet]
        [AllowAnonymous]
        public ActionResult EsignPdf()
        {
            this.ViewData.Model = this.GetEsign(false);

            string fileName = ReplacementUtility.ReplaceWhiteSpace(ReplacementUtility.RemoveNonAlphaNumeric(this.ClientDto.ClientName), "_") + "_EsignActTerms.pdf";
            this.WritePdfInline(this.PdfConverter.Value.CreatePdfFromHtml(this.ClientDto.ClientName + " E-Sign Act Terms", this.RenderViewToString("~/Views/Legal/_Esign.cshtml", "~/Views/Shared/_LayoutPrint.cshtml")), fileName);

            return this.Json(new { }, JsonRequestBehavior.AllowGet);
        }

        protected TextViewModel GetEsign(bool showButton = true)
        {
            CmsVersionDto cmsVersion = this.ContentApplicationService.Value.GetCurrentVersionAsync(CmsRegionEnum.EsignTerms).Result;

            TextViewModel model = new TextViewModel
            {
                Text = cmsVersion.ContentBody,
                IsExport = !showButton
            };

            if (this.IsMobile)
            {
                model.IsExport = true; // hide the button
            }

            return model;
        }

        #endregion

        #region terms of  use

        [HttpGet]
        [AllowAnonymous]
        public ActionResult TermsOfUse()
        {
            return this.View(this.GetTerms());
        }

        [HttpGet]
        [AllowAnonymous]
        public ActionResult TermsPartial(bool showButton = true)
        {
            return this.PartialView("_Terms", this.GetTerms(showButton));
        }

        [HttpGet]
        [AllowAnonymous]
        public ActionResult TermsPdf()
        {
            this.ViewData.Model = this.GetTerms(false);

            string fileName = ReplacementUtility.ReplaceWhiteSpace(ReplacementUtility.RemoveNonAlphaNumeric(this.ClientDto.ClientName), "_") + "_TermsOfUse.pdf";
            this.WritePdfInline(this.PdfConverter.Value.CreatePdfFromHtml(this.ClientDto.ClientName + " Terms of Use", this.RenderViewToString("~/Views/Legal/_Terms.cshtml", "~/Views/Shared/_LayoutPrint.cshtml")), fileName);

            return this.Json(new { }, JsonRequestBehavior.AllowGet);
        }

        protected TermsOfUseViewModel GetTerms(bool showButton = true)
        {
            CmsVersionDto cmsVersion = this.ContentApplicationService.Value.GetCurrentVersionAsync(CmsRegionEnum.VppTermsAndConditions).Result;

            TermsOfUseViewModel model = new TermsOfUseViewModel
            {
                Text = cmsVersion.ContentBody,
                IsExport = !showButton,
                UpdatedDate = cmsVersion.UpdatedDate.ToString(Format.MonthDayYearFormat)
            };

            if (this.IsMobile)
            {
                model.IsExport = true; // hide the button
            }

            return model;
        }

        #endregion
    }
}
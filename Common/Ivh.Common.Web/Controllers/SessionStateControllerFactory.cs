﻿namespace Ivh.Common.Web.Controllers
{
    using System;
    using System.Linq;
    using System.Reflection;
    using System.Web.Mvc;
    using System.Web.Routing;
    using System.Web.SessionState;
    using SessionStateAttribute = Attributes.SessionStateAttribute;

    public class SessionStateControllerFactory : DefaultControllerFactory
    {
        protected override SessionStateBehavior GetControllerSessionBehavior(RequestContext requestContext, Type controllerType)
        {
            if (controllerType == null)
            {
                return SessionStateBehavior.Default;
            }

            string actionName = requestContext.RouteData.Values["action"].ToString();
            MethodInfo actionMethodInfo;

            try
            {
                actionMethodInfo = controllerType.GetMethod(actionName, BindingFlags.IgnoreCase | BindingFlags.Public | BindingFlags.Instance);
            }
            catch (AmbiguousMatchException)
            {
                Type httpRequestTypeAttr =
                    requestContext.HttpContext.Request.RequestType.Equals("POST")
                        ? typeof (HttpPostAttribute)
                        : typeof (HttpGetAttribute);

                actionMethodInfo =
                    controllerType.GetMethods().FirstOrDefault(
                        mi =>
                            mi.Name.Equals(actionName, StringComparison.CurrentCultureIgnoreCase) && mi.GetCustomAttributes(httpRequestTypeAttr, false).Length > 0);
            }


            if (actionMethodInfo != null)
            {
                SessionStateAttribute actionSessionStateAttr = actionMethodInfo.GetCustomAttributes(typeof (SessionStateAttribute), false)
                    .OfType<SessionStateAttribute>()
                    .FirstOrDefault();

                if (actionSessionStateAttr != null)
                {
                    return actionSessionStateAttr.Behavior;
                }
            }

            return base.GetControllerSessionBehavior(requestContext, controllerType);
        }
    }
}
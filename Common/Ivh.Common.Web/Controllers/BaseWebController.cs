﻿namespace Ivh.Common.Web.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Reflection;
    using System.Security.Claims;
    using System.Threading.Tasks;
    using System.Web;
    using System.Web.Mvc;
    using Ivh.Application.Content.Common.Interfaces;
    using Ivh.Application.Core.Common.Dtos;
    using Ivh.Application.Core.Common.Interfaces;
    using Ivh.Application.Logging.Common.Interfaces;
    using Base.Constants;
    using Base.Utilities.Extensions;
    using Base.Utilities.Helpers;
    using Constants;
    using Domain.Logging.Interfaces;
    using Domain.Settings.Enums;
    using Domain.Settings.Interfaces;
    using Extensions;
    using Interfaces;
    using Ivh.Application.AppIntelligence.Common.Dtos;
    using Ivh.Application.AppIntelligence.Common.Interfaces;
    using Models.Shared;
    using Session;
    using Utilities;
    using VisitPay.Constants;
    using VisitPay.Enums;
    using VisitPay.Strings;
    using System.Web.SessionState;

    [SessionState(SessionStateBehavior.ReadOnly)]
    public abstract class BaseWebController : Controller
    {
        private AnaltyicsDto _analyticsDto;
        private ClientDto _clientDto;
        private CurrentUserViewModel _currentUser;
        private string _browserNameString;
        private bool? _showBuildBanner;
        private readonly Lazy<IBaseControllerService> _baseControllerService;
        private static string _vpInfo = null;
        private static readonly object Lock = new object();

        public string GenericErrorMessage => LocalizationHelper.GetLocalizedString(TextRegionConstants.PaymentValidationGeneralError);

        protected BaseWebController(Lazy<IBaseControllerService> baseControllerService)
        {
            this._baseControllerService = baseControllerService;
        }

        protected Lazy<IApplicationSettingsService> ApplicationSettingsService => this._baseControllerService.Value.ApplicationSettingsService;
        protected Lazy<IClientApplicationService> ClientApplicationService => this._baseControllerService.Value.ClientApplicationService;
        
        protected Lazy<IFeatureApplicationService> FeatureApplicationService => this._baseControllerService.Value.FeatureApplicationService;

        protected Lazy<IThemeApplicationService> ThemeApplicationService => this._baseControllerService.Value.ThemeApplicationService;
        protected Lazy<IMetricsProvider> MetricsProvider => this._baseControllerService.Value.MetricsProvider;

        protected Lazy<IVisitPayUserJournalEventApplicationService> VisitPayUserJournalEventApplicationService => this._baseControllerService.Value.VisitPayUserJournalEventApplicationService;

        protected Lazy<ILoggingApplicationService> Logger => this._baseControllerService.Value.Logger;

        protected Lazy<IRandomizedTestApplicationService> RandomizedTestApplicationService => this._baseControllerService.Value.RandomizedTestApplicationService;

        protected async Task BruteForceDelayAsync(string ipAddress, Func<bool> delayFunc, Func<bool> clearFunc = null)
        {
            TimeSpan delay = await this._baseControllerService.Value.BruteForceDelayService.Value.DelayAsync(ipAddress, delayFunc, clearFunc);
            if (delay != TimeSpan.Zero)
            {
                this.MetricsProvider.Value.Increment(Metrics.Increment.SignIn.BruteForceDelay);
            }
        }

        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            if (_vpInfo == null)
            {
                lock (Lock)
                {
                    if (_vpInfo == null)
                    {
                        _vpInfo = $"{Environment.MachineName};{Assembly.GetExecutingAssembly().GetName().Version}";
                    }
                }
            }

            if (_vpInfo != null)
            {
                filterContext.HttpContext.Response.Headers.Set(HttpHeaders.Response.VpInfo, $"{_vpInfo};{this.Session.SessionID}");
            }

            base.OnActionExecuting(filterContext);
        }

        public AnaltyicsDto AnalyticsDto => this._analyticsDto ?? (this._analyticsDto = new AnaltyicsDto()
        {
            AnalyticsId = this.ApplicationSettingsService.Value.AnalyticsId.Value,
            BaseUrl = this.ApplicationSettingsService.Value.GetUrl(UrlEnum.AnalyticsBase).AbsoluteUri,
        });

        public ClientDto ClientDto => this._clientDto ?? (this._clientDto = this.ClientApplicationService.Value.GetClient());

        public TimeZoneHelper TimeZoneHelper => this._baseControllerService.Value.TimeZoneHelper.Value;

        public DateTime SystemNow => this.TimeZoneHelper.Server.Now;
        public DateTime ClientNow => this.TimeZoneHelper.Client.Now;
        public DateTime OperationsNow => this.TimeZoneHelper.Operations.Now;

        public string ThemeStylesheetHash(string requestedFile)
        {
            return requestedFile.IsNotNullOrEmpty() ? this.ThemeApplicationService.Value.GetStylesheetHash(requestedFile) : string.Empty;
        }

        public string ThemeStylesheetRaw(string requestedFile)
        {
            if (requestedFile.IsNotNullOrEmpty())
            {
                return this.ThemeApplicationService.Value.GetStylesheet(requestedFile);
            }
            return string.Empty;
        }

        public string CurrentArea
        {
            get
            {
                object area = this.ControllerContext.RouteData.DataTokens["area"];
                return area != null ? (string)area : string.Empty;
            }
        }

        public string RequestUserAgent => this.HttpContext.Request.UserAgent;

        public string RequestUserHostAddressString => this.HttpContext.Request.RequestUserHostAddressString();

        public bool InArea(string area)
        {
            return this.CurrentArea.Equals(area, StringComparison.OrdinalIgnoreCase);
        }

        public const string MobileAreaPath = "Areas/Mobile";

        public bool IsMobile => this.InArea("mobile");

        public int CurrentUserId => this.User.CurrentUserId();

        public string CurrentUserIdString => this.User.CurrentUserIdString();

        public string CurrentUserName => this.User.CurrentUserName();

        public int? CurrentGuarantorId
        {
            get
            {
                if (this.HttpContext.User.IsInRole(VisitPayRoleStrings.System.Patient))
                {
                    return this.HttpContext.User.CurrentGuarantorId() ?? this._baseControllerService.Value.GuarantorApplicationService.Value.GetGuarantorId(this.CurrentUserId);
                }
                return null;
            }
        }

        public bool IsCurrentUserEmulating()
        {
            if (!(this.User.Identity is ClaimsIdentity))
            {
                return false;
            }

            ClaimsIdentity claimsIdentity = (ClaimsIdentity)this.User.Identity;
            if (claimsIdentity.HasClaim(x => x.Type.Equals(ClaimTypeEnum.EmulatedUser.ToString(), StringComparison.OrdinalIgnoreCase)))
            {
                return true;
            }

            return false;
        }

        public CurrentUserViewModel CurrentUser => this._currentUser ?? (this._currentUser = this.GetCurrentUser());

        public Lazy<ISessionFacade> SessionFacade => this._baseControllerService.Value.SessionFacade;

        public void SetTempData(string key, object value)
        {
            if (this.Session.IsReadOnly)
            {
                throw new HttpUnhandledException(SessionConstants.ReadOnlyWriteError);
            }
            this.TempData[key] = value;
        }

        public bool IsFeatureEnabled(VisitPayFeatureEnum feature)
        {
            return this.FeatureApplicationService.Value.IsFeatureEnabled(feature);
        }

        public RandomizedTestGroupEnum GetRandomizedTestGroup(RandomizedTestEnum randomizedTest)
        {
            RandomizedTestGroupEnum randomizedTestGroupEnum = this.SessionFacade.Value.GetRandomizedTestGroupEnum(randomizedTest);
            if (randomizedTestGroupEnum == 0)
            {
                int guarantorId = this.GetVpGuarantorId(this.CurrentUserId);
                RandomizedTestGroupDto randomizedTestGroupDto = this.RandomizedTestApplicationService.Value.GetRandomizedTestGroupMembership(randomizedTest, guarantorId, true);
                randomizedTestGroupEnum = (RandomizedTestGroupEnum)randomizedTestGroupDto.RandomizedTestGroupId;

                SessionFacade.Value.SetRandomizedTestGroupEnum(randomizedTest, randomizedTestGroupEnum);
                return randomizedTestGroupEnum;
            }

            return randomizedTestGroupEnum;
        }

        public bool ShowBuildBanner
        {
            get
            {
                if (this._showBuildBanner.HasValue)
                {
                    return this._showBuildBanner.Value;
                }

                bool showBuildBanner = this.ApplicationSettingsService.Value.ShowBuildBanner.Value;
                this._showBuildBanner = showBuildBanner;

                return showBuildBanner;
            }
        }

        #region render / write methods

        protected string RenderViewToString(string viewName, string masterName)
        {
            if (string.IsNullOrEmpty(viewName))
            {
                viewName = this.ControllerContext.RouteData.GetRequiredString("action");
            }

            using (StringWriter sw = new StringWriter())
            {
                ViewEngineResult viewResult = ViewEngines.Engines.FindView(this.ControllerContext, viewName, masterName);
                ViewContext viewContext = new ViewContext(this.ControllerContext, viewResult.View, this.ViewData, this.TempData, sw);
                viewResult.View.Render(viewContext, sw);

                return sw.GetStringBuilder().ToString();
            }
        }

        protected string RenderPartialViewToString(string viewName)
        {
            if (string.IsNullOrEmpty(viewName))
            {
                viewName = this.ControllerContext.RouteData.GetRequiredString("action");
            }

            using (StringWriter sw = new StringWriter())
            {
                ViewEngineResult viewResult = ViewEngines.Engines.FindPartialView(this.ControllerContext, viewName);
                ViewContext viewContext = new ViewContext(this.ControllerContext, viewResult.View, this.ViewData, this.TempData, sw);
                viewResult.View.Render(viewContext, sw);

                return sw.GetStringBuilder().ToString();
            }
        }

        protected void WriteFileToResponse(byte[] data, string filename)
        {
            this.WriteBinaryInline(data, filename, MimeTypes.Application.OctetStream);
        }

        protected void WritePdfInline(byte[] data, string filename)
        {
            this.WriteBinaryInline(data, filename, MimeTypes.Application.Pdf);
        }

        protected void WriteExcelInline(byte[] data, string filename)
        {
            this.WriteBinaryInline(data, filename, MimeTypes.Application.Excel);
        }

        private void WriteBinaryInline(byte[] data, string filename, string contentType)
        {
            this.Response.ContentType = contentType;
            this.Response.AddHeader("content-disposition", "inline; filename=" + filename);
            this.Response.AddHeader("content-length", data.Length.ToString());
            this.Response.Cache.SetCacheability(HttpCacheability.Private);
            /*
                HttpResponse.Clear() and HttpResponse.ClearContent() do exactly the same thing. 
                They only clear the response body and not headers.
                Switching to HttpResponse.ClearContent() for clarity.
            */
            this.Response.ClearContent();
            this.Response.BufferOutput = true;
            this.Response.BinaryWrite(data);
            this.Response.End();
        }

        #endregion

        #region browser

        private const string BrowserNameKey = "Name";
        private const string BrowserVersionMajorKey = "VersionMajor";
        private const string BrowserUserAgentKey = "UserAgent";
        private static readonly string[] InternetExplorerStrings = { "IE", "INTERNETEXPLORER", "TRIDENT" };

        public string BrowserNameString => this._browserNameString ?? (this._browserNameString = this.GetBrowserNameString());

        private bool? _isBrowserSupported;
        public bool IsBrowserSupported
        {
            get => this._isBrowserSupported ?? (this.IsBrowserSupported = this.GetIsBrowserSupported());
            private set => this._isBrowserSupported = value;
        }

        private bool? _isBrowserInternetExplorer;

        public bool IsBrowserInternetExplorer
        {
            get => this._isBrowserInternetExplorer ?? (this.IsBrowserInternetExplorer = this.GetIsBrowserInternetExplorer());
            private set => this._isBrowserInternetExplorer = value;
        }

        private bool? _isBrowserSafari;
        public bool IsBrowserSafari
        {
            get => this._isBrowserSafari ?? (this.IsBrowserSafari = this.GetIsBrowserSafari());
            private set => this._isBrowserSafari = value;
        }

        private IDictionary<string, string> BrowserAttributes
        {
            get
            {
                if (this.HttpContext == null)
                {
                    return null;
                }

                try
                {
                    string userAgent = this.HttpContext.Request.UserAgent;

                    HttpBrowserCapabilities browserCapabilities = new HttpBrowserCapabilities();
                    HttpBrowserCapabilitiesBase browser = this.HttpContext.Request.Browser;
                    browserCapabilities.Capabilities = browser.Capabilities;

                    return new Dictionary<string, string>
                    {
                        {BrowserNameKey, browser.Browser},
                        {BrowserVersionMajorKey, browser.MajorVersion.ToString()},
                        {BrowserUserAgentKey, userAgent}
                    };
                }
                catch
                {
                    return new Dictionary<string, string>();
                }
            }
        }

        private string GetBrowserNameString()
        {
            try
            {
                this.BrowserAttributes.TryGetValue(BrowserNameKey, out string browserName);
                this.BrowserAttributes.TryGetValue(BrowserVersionMajorKey, out string versionMajor);

                if (browserName == null)
                {
                    return string.Empty;
                }

                return InternetExplorerStrings.Contains(browserName.ToUpper()) ? $"Internet Explorer {versionMajor}" : $"{browserName} {versionMajor}";
            }
            catch
            {
                return "Your browser";
            }
        }

        private bool GetIsBrowserInternetExplorer()
        {
            try
            {

                this.BrowserAttributes.TryGetValue(BrowserNameKey, out string browserName);
                this.BrowserAttributes.TryGetValue(BrowserVersionMajorKey, out string versionMajor);

                if (string.IsNullOrWhiteSpace(browserName))
                {
                    return false;
                }

                if (InternetExplorerStrings.Contains(browserName.ToUpper()))
                {
                    //it's IE
                    return true;
                }

                return false;
            }
            catch
            {
                return false;
            }
        }

        private bool GetIsBrowserSafari()
        {
            try
            {
                this.BrowserAttributes.TryGetValue(BrowserNameKey, out string browserName);

                if (string.IsNullOrWhiteSpace(browserName))
                {
                    return false;
                }

                if (browserName.ToUpper().Contains("SAFARI"))
                {
                    //it's safari
                    return true;
                }

                return false;
            }
            catch
            {
                return false;
            }
        }

        private bool GetIsBrowserSupported()
        {
            try
            {

                this.BrowserAttributes.TryGetValue(BrowserNameKey, out string browserName);
                this.BrowserAttributes.TryGetValue(BrowserVersionMajorKey, out string versionMajor);

                if (string.IsNullOrWhiteSpace(browserName))
                {
                    return true;
                }

                if (!InternetExplorerStrings.Contains(browserName.ToUpper()))
                {
                    // it's not IE, allow it
                    return true;
                }

                // it's IE
                if (versionMajor == null || Convert.ToInt32(versionMajor) >= 11)
                {
                    // it's IE >= 11
                    return true;
                }

                this.BrowserAttributes.TryGetValue(BrowserUserAgentKey, out string userAgent);
                if (!string.IsNullOrWhiteSpace(userAgent))
                {
                    if (userAgent.ToUpper().Contains("TRIDENT/7"))
                    {
                        // trident/7.0 is IE11
                        // IE compatibility mode doesn't mask this, but will otherwise identify as IE7
                        return true;
                    }
                }

                this.Logger.Value.Info(() => $"{nameof(BaseWebController)}:{nameof(this.GetIsBrowserSupported)} - unsupported browser detected: {userAgent ?? string.Empty}");
                return false;
            }
            catch
            {
                return true;
            }
        }

        #endregion

        protected bool ValidateModelState(string failedValidationMessage)
        {
            if (this.ModelState.IsValid)
            {
                return true;
            }

            try
            {
                IEnumerable<string> errorList = this.ModelState
                    .Where(x => x.Value?.Errors != null && x.Value.Errors.Any())
                    .ToDictionary(k => k.Key, v => v.Value.Errors.Where(e => !string.IsNullOrWhiteSpace(e?.ErrorMessage)).Select(e => e.ErrorMessage))
                    .Select(x => $"{x.Key}: {string.Join(", ", x.Value)}");

                string errorString = string.Join("; ", errorList);
                this.Logger.Value.Info(() => $"{failedValidationMessage} - {errorString}");
            }
            catch
            {
                // ignored
            }

            return false;
        }

        [NonAction]
        public int GetVpGuarantorId(int visitPayUserId)
        {
            if (visitPayUserId == this.CurrentUserId)
            {
                return this.CurrentGuarantorId ?? this._baseControllerService.Value.GuarantorApplicationService.Value.GetGuarantorId(visitPayUserId);
            }
            else
            {
                return this._baseControllerService.Value.GuarantorApplicationService.Value.GetGuarantorId(visitPayUserId);
            }
        }

        private CurrentUserViewModel GetCurrentUser()
        {
            if (!(this.User.Identity is ClaimsIdentity))
            {
                return new CurrentUserViewModel();
            }

            if (!(this.HttpContext.User.Identity is ClaimsIdentity identity))
            {
                return new CurrentUserViewModel();
            }

            string firstName;
            string lastName;
            string userName;

            if (this.IsCurrentUserEmulating())
            {
                firstName = identity.Claims.FirstOrDefault(c => c.Type.Equals(ClaimTypeEnum.EmulatedUserFirstName.ToString(), StringComparison.OrdinalIgnoreCase))?.Value;
                lastName = identity.Claims.FirstOrDefault(c => c.Type.Equals(ClaimTypeEnum.EmulatedUserLastName.ToString(), StringComparison.OrdinalIgnoreCase))?.Value;
                userName = "";
            }
            else
            {
                firstName = identity.Claims.FirstOrDefault(c => c.Type.Equals(ClaimTypeEnum.ClientFirstName.ToString(), StringComparison.OrdinalIgnoreCase))?.Value;
                lastName = identity.Claims.FirstOrDefault(c => c.Type.Equals(ClaimTypeEnum.ClientLastName.ToString(), StringComparison.OrdinalIgnoreCase))?.Value;
                userName = identity.Claims.FirstOrDefault(c => c.Type.Equals(ClaimTypeEnum.ClientUsername.ToString(), StringComparison.OrdinalIgnoreCase))?.Value;
            }

            DateTime? dt = null;
            if (identity.HasClaim(c => c.Type.Equals(ClaimTypeEnum.LastLoginDate.ToString(), StringComparison.OrdinalIgnoreCase)))
            {
                dt = DateTime.Parse(identity.Claims.First(c => c.Type.Equals(ClaimTypeEnum.LastLoginDate.ToString(), StringComparison.OrdinalIgnoreCase)).Value);
            }

            string visitPayUserInsertDate = identity.Claims.FirstOrDefault(c => c.Type.Equals(ClaimTypeEnum.VisitPayUserInsertDate.ToString(), StringComparison.OrdinalIgnoreCase))?.Value;

            CurrentUserViewModel currentUser = new CurrentUserViewModel
            {
                FirstName = firstName,
                LastName = lastName,
                UserName = userName,
                LastLoginDate = (dt ?? DateTime.UtcNow).ToString(Format.DateFormat),
                InsertDate = visitPayUserInsertDate
            };

            return currentUser;
        }
    }
}
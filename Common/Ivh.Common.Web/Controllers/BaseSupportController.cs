﻿namespace Ivh.Common.Web.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Threading.Tasks;
    using System.Web;
    using System.Web.Mvc;
    using Ivh.Application.Content.Common.Dtos;
    using Ivh.Application.Content.Common.Interfaces;
    using Ivh.Application.Core.Common.Dtos;
    using Ivh.Application.Core.Common.Interfaces;
    using Ivh.Application.FinanceManagement.Common.Dtos;
    using Ivh.Application.FinanceManagement.Common.Interfaces;
    using Ivh.Application.SecureCommunication.Common.Dtos;
    using Ivh.Application.SecureCommunication.Common.Interfaces;
    using Ivh.Application.User.Common.Dtos;
    using AutoMapper;
    using Base.Enums;
    using Base.Utilities.Extensions;
    using Base.Utilities.Helpers;
    using Extensions;
    using Interfaces;
    using Microsoft.AspNet.Identity;
    using Models;
    using Models.Account;
    using Models.Shared;
    using Models.Support;
    using VisitPay.Enums;
    using VisitPay.Strings;
    using System.Web.SessionState;

    public class BaseSupportController : BaseWebController
    {
        protected readonly Lazy<IContentApplicationService> ContentApplicationService;
        protected readonly Lazy<IConsolidationGuarantorApplicationService> ConsolidationGuarantorApplicationService;
        protected readonly Lazy<ISupportRequestApplicationService> SupportRequestApplicationService;
        protected readonly Lazy<ISupportTopicApplicationService> SupportTopicApplicationService;
        protected readonly Lazy<ITreatmentLocationApplicationService> TreatmentLocationApplicationService;
        protected readonly Lazy<IVisitApplicationService> VisitApplicationService;
        protected readonly Lazy<IVisitPayUserApplicationService> VisitPayUserApplicationService;
        
        public BaseSupportController(
            Lazy<IBaseControllerService> baseControllerService,
            Lazy<IContentApplicationService> contentApplicationService,
            Lazy<IConsolidationGuarantorApplicationService> consolidationGuarantorApplicationService,
            Lazy<ISupportRequestApplicationService> supportRequestApplicationService,
            Lazy<ISupportTopicApplicationService> supportTopicApplicationService,
            Lazy<ITreatmentLocationApplicationService> treatmentLocationApplicationService,
            Lazy<IVisitApplicationService> visitApplicationService,
            Lazy<IVisitPayUserApplicationService> visitPayUserApplicationService,
            Lazy<IVisitPayUserJournalEventApplicationService> visitPayUserJournalEventApplicationService)
            : base(baseControllerService)
        {
            this.ContentApplicationService = contentApplicationService;
            this.ConsolidationGuarantorApplicationService = consolidationGuarantorApplicationService;
            this.SupportRequestApplicationService = supportRequestApplicationService;
            this.SupportTopicApplicationService = supportTopicApplicationService;
            this.TreatmentLocationApplicationService = treatmentLocationApplicationService;
            this.VisitApplicationService = visitApplicationService;
            this.VisitPayUserApplicationService = visitPayUserApplicationService;
        }

        const string SupportRequestAttachmentDownloadKey = "SupportRequestAttachmentDownload";
        const string PrintSupportRequestIdKey = "PrintSupportRequestId";
        const string PrintSupportRequestVisitPayUserIdKey = "PrintSupportRequestVisitPayUserId";
        const string PrintSupportRequestSortOrderGuarantorKey = "PrintSupportRequestSortOrderGuarantor";
        const string PrintSupportRequestSortOrderInternalKey = "PrintSupportRequestSortOrderInternal";

        #region create

        [NonAction]
        public CreateSupportRequestViewModel CreateSupportRequestModel(int visitPayUserId, int vpGuarantorId, int? visitId, bool isMobile)
        {
            GuarantorDto guarantorDto = this.VisitPayUserApplicationService.Value.GetGuarantorFromVisitPayUserId(visitPayUserId);
            VisitDto visitDto = visitId.HasValue ? this.VisitApplicationService.Value.GetVisit(this.CurrentUserId, visitId.Value, vpGuarantorId) : null;

            CreateSupportRequestViewModel model = new CreateSupportRequestViewModel
            {
                IsMobile = isMobile,
                GuarantorName = guarantorDto.User.DisplayFirstNameLastName,
                Visit = visitDto == null ? null : Mapper.Map<VisitDetailsViewModel>(Mapper.Map<VisitDto>(visitDto)),
                VisitId = visitDto?.VisitId,
                Users = new List<SelectListItem>
                {
                    new SelectListItem
                    {
                        Text = guarantorDto.User.DisplayFirstNameLastName,
                        Value = guarantorDto.User.VisitPayUserId.ToString(),
                        Selected = visitDto == null || guarantorDto.VpGuarantorId == visitDto.VpGuarantorId
                    }
                }
            };

            List<ConsolidationGuarantorDto> managedUsers = this.ConsolidationGuarantorApplicationService.Value.GetAllManagedGuarantors(this.CurrentUserId).Where(x => x.IsActive && !x.IsPending).ToList();
            model.Users.AddRange(managedUsers.Select(x => new SelectListItem
            {
                Text = x.ManagedGuarantor.User.DisplayFirstNameLastName,
                Value = x.ManagedGuarantor.User.VisitPayUserId.ToString(),
                Selected = visitDto != null && x.ManagedGuarantor.VpGuarantorId == visitDto.VpGuarantorId
            }));

            model.SubmittedForVisitPayUserId = Convert.ToInt32(model.Users.First(x => x.Selected).Value);
            model.SelfServiceTextCmsVersionDto = this.ContentApplicationService.Value.GetCurrentVersionAsync(model.SelfServiceCmsRegion, true, model.ReplacementDictionary).Result;
            return model;
        }
        
        [NonAction]
        public JsonResult CreateSupportRequest(SupportRequestCreateDto supportRequestCreateDto)
        {
            SupportRequestDto supportRequestDto = this.SupportRequestApplicationService.Value.CreateSupportRequest(supportRequestCreateDto);

            ResultMessage<dynamic> resultMessage = new ResultMessage<dynamic>(true, new
            {
                SupportRequestId = supportRequestDto.SupportRequestId,
                SupportRequestMessageId = supportRequestDto.SupportRequestMessages.First().SupportRequestMessageId
            });

            return this.Json(resultMessage);
        }

        [NonAction]
        public async Task<JsonResult> CreateSupportRequestConfirmation(int visitPayUserId)
        {
            VisitPayUserDto visitPayUserDto = this.VisitPayUserApplicationService.Value.FindById(visitPayUserId.ToString());

            return this.Json(Mapper.Map<CmsViewModel>(await this.ContentApplicationService.Value.GetCurrentVersionAsync(CmsRegionEnum.CreateSupportRequestConfirmation, true, new Dictionary<string, string>
            {
                {"[[VisitPayUserEmailAddress]]", visitPayUserDto.Email}
            })));
        }
        
        #endregion

        #region details

        [HttpGet]
        public PartialViewResult SupportRequest()
        {
            return this.PartialView("~/Views/Support/_SupportRequest.cshtml", new SupportRequestGenericMessageViewModel());
        }

        /// <summary>
        /// post, returns data to load support request details
        /// </summary>
        /// <param name="supportRequestId"></param>
        /// <param name="visitPayUserId"></param>
        /// <param name="isClient"></param>
        /// <returns></returns>
        [NonAction]
        public SupportRequestViewModel GetSupportRequest(int supportRequestId, int visitPayUserId, bool isClient)
        {
            SupportRequestDto supportRequestDto = this.SupportRequestApplicationService.Value.GetSupportRequest(supportRequestId, visitPayUserId);

            if (!isClient && supportRequestDto.GuarantorMessages != null)
            {
                //For patient, don't ever return attachments that have had malware detected. Don't want to show them in the list.
                foreach (SupportRequestMessageDto msg in supportRequestDto.GuarantorMessages)
                {
                    msg.SupportRequestMessageAttachments = msg.SupportRequestMessageAttachments.Where(a => MalwareScanStatusEnum.Detected != a.MalwareScanStatus).ToList();
                    msg.ActiveSupportRequestMessageAttachments = msg.ActiveSupportRequestMessageAttachments.Where(a => MalwareScanStatusEnum.Detected != a.MalwareScanStatus).ToList();
                }
            }

            SupportRequestViewModel model = Mapper.Map<SupportRequestViewModel>(supportRequestDto);
            if (this.ApplicationSettingsService.Value.IsClientApplication.Value)
            {
                model.SubmittedForVpGuarantorId = supportRequestDto.SubmittedForVpGuarantor?.VpGuarantorId ?? supportRequestDto.VpGuarantor.VpGuarantorId;
                model.SubmittedForVisitPayUserId = supportRequestDto.SubmittedForVpGuarantor?.User.VisitPayUserId ?? supportRequestDto.VpGuarantor.User.VisitPayUserId;
                model.FromVpGuarantorId = supportRequestDto.VpGuarantor.VpGuarantorId;
                model.VisitId = supportRequestDto.Visit?.VisitId;
            }
            SupportRequestMessageDto closeMessage = this.SupportRequestApplicationService.Value.GetClosedMessage(supportRequestDto);

            if (closeMessage != null)
            {
                //VPNG-18700
                model.ClosedBy = (isClient)? closeMessage.VisitPayUser.DisplayFirstNameLastName: closeMessage.VisitPayUser.FirstName;
                model.ClosedDate = closeMessage.InsertDate.ToString(Format.DateFormat);
            }

            SupportRequestMessageDto supportRequestMessageDto = this.SupportRequestApplicationService.Value.GetDraftMessage(supportRequestId, this.User.CurrentUserId());
            if (supportRequestMessageDto != null)
            {
                model.DraftMessage = supportRequestMessageDto.MessageBody;
            }

            return model;
        }

        [HttpPost]
        public PartialViewResult GetAttachments(int supportRequestId, int? supportRequestMessageId, int visitPayUserId, bool showInternalAttachments)
        {
            IList<SupportRequestMessageAttachmentDto> attachmentsDto = this.SupportRequestApplicationService.Value.GetAttachments(supportRequestId, visitPayUserId, supportRequestMessageId, showInternalAttachments);
            IList<SupportRequestMessageAttachmentViewModel> model = Mapper.Map<List<SupportRequestMessageAttachmentViewModel>>(attachmentsDto);

            return this.PartialView("~/Views/Support/_SupportAttachments.cshtml", model.OrderBy(x => x.InsertDate).ThenBy(x => x.AttachmentFileName).ToList());
        }

        [NonAction]
        [Common.Web.Attributes.SessionState(SessionStateBehavior.Required)]
        public JsonResult AttachmentPrepareDownload(int supportRequestId, int? supportRequestMessageAttachmentId, int visitPayUserId, bool showInternalAttachments)
        {
            AttachmentDownloadDto attachmentDownloadDto = this.SupportRequestApplicationService.Value.PrepareDownload(supportRequestId, supportRequestMessageAttachmentId, visitPayUserId, showInternalAttachments);

            if (attachmentDownloadDto == null)
            {
                return this.Json(new ResultMessage(false, "No attachments available for download."));
            }

            this.SetTempData(SupportRequestAttachmentDownloadKey, attachmentDownloadDto);

            return this.Json(new ResultMessage(true, this.Url.Action("DownloadAttachment", "Support")));
        }

        [HttpGet]
        public ActionResult DownloadAttachment()
        {
            AttachmentDownloadDto attachmentDownloadDto = this.TempData[SupportRequestAttachmentDownloadKey] as AttachmentDownloadDto;
            return attachmentDownloadDto != null ? this.File(attachmentDownloadDto.Bytes, attachmentDownloadDto.MimeType, attachmentDownloadDto.FileName) : null;
        }

        #endregion

        #region actions: close/reopen

        [NonAction]
        public JsonResult CompleteSupportRequestAction(SupportRequestActionBaseModel model, int visitPayUserId)
        {
            if (model.CurrentSupportRequestStatus == SupportRequestStatusEnum.Open)
                this.SupportRequestApplicationService.Value.CloseSupportRequest(model.SupportRequestId, visitPayUserId, model.MessageBody, this.CurrentUserId);
            else
                this.SupportRequestApplicationService.Value.ReopenSupportRequest(model.SupportRequestId, visitPayUserId, model.MessageBody, this.CurrentUserId);

            return this.Json(true);
        }

        [NonAction]
        public PartialViewResult SupportRequestAction(SupportRequestStatusEnum supportRequestStatus, int supportRequestId, string supportRequestDisplayId, SupportRequestActionBaseModel model)
        {
            model.CurrentSupportRequestStatus = supportRequestStatus;
            model.SupportRequestId = supportRequestId;
            model.SupportRequestDisplayId = supportRequestDisplayId;

            return this.PartialView("/Views/Support/_SupportRequestAction.cshtml", model);
        }

        #endregion

        #region shared
        
        [NonAction]
        public JsonResult UploadSupportRequestMessageAttachment(HttpRequestBase httpRequest, int visitPayUserId)
        {
            List<string> acceptedTypes = new List<string>
            {
                "application/msword", // .doc
                "application/vnd.openxmlformats-officedocument.wordprocessingml.document", // .docx
                "application/vnd.ms-excel", // .xls
                "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", // .xlsx
                "application/vnd.ms-powerpoint", // .ppt
                "application/vnd.openxmlformats-officedocument.presentationml.presentation", // .pptx
                "application/pdf",
                "application/x-zip-compressed",
                "image/gif",
                "image/jpg",
                "image/jpeg",
                "image/png",
                "text/plain"
            };

            int supportRequestId;
            int supportRequestMessageId;

            if (!int.TryParse(httpRequest.Form["SupportRequestId"], out supportRequestId) ||
                !int.TryParse(httpRequest.Form["SupportRequestMessageId"], out supportRequestMessageId) ||
                httpRequest.Files.Count < 1 ||
                httpRequest.Files[0] == null ||
                !acceptedTypes.Contains(httpRequest.Files[0].ContentType))
            {
                return this.Json(false);
            }

            HttpPostedFileBase postedFile = httpRequest.Files[0];

            byte[] bytes;
            Stream stream = postedFile.InputStream;
            using (BinaryReader reader = new BinaryReader(stream))
            {
                bytes = reader.ReadBytes((int)stream.Length);
            }

            if (bytes.Length <= 0 || bytes.Length > 5000000)
                return this.Json(false);

            SupportRequestMessageAttachmentDto attachmentDto = new SupportRequestMessageAttachmentDto
            {
                AttachmentFileName = postedFile.FileName,
                FileSize = bytes.Length,
                MimeType = postedFile.ContentType,
                QuarantinedBytes = bytes
            };

            this.SupportRequestApplicationService.Value.SaveMessageAttachment(attachmentDto, supportRequestMessageId, supportRequestId, visitPayUserId);

            return this.Json(true);
        }

        #endregion

        #region print

        [NonAction]
        [Common.Web.Attributes.SessionState(SessionStateBehavior.Required)]
        public ActionResult PrintSupportRequest(int supportRequestId, int visitPayUserId, string sortOrderGuarantor, string sortOrderInternal)
        {
            this.SetTempData(PrintSupportRequestIdKey, supportRequestId);
            this.SetTempData(PrintSupportRequestVisitPayUserIdKey, visitPayUserId);
            this.SetTempData(PrintSupportRequestSortOrderGuarantorKey, sortOrderGuarantor);
            this.SetTempData(PrintSupportRequestSortOrderInternalKey, sortOrderInternal);

            return this.Json(this.Url.Action("PrintSupportRequest", "Support"));
        }

        [NonAction]
        public PrintSupportRequestViewModel PrintSupportRequest(bool isClient)
        {
            int supportRequestId;
            int visitPayUserId;

            if (this.TempData[PrintSupportRequestIdKey] == null || !int.TryParse(this.TempData[PrintSupportRequestIdKey].ToString(), out supportRequestId) ||
                this.TempData[PrintSupportRequestVisitPayUserIdKey] == null || !int.TryParse(this.TempData[PrintSupportRequestVisitPayUserIdKey].ToString(), out visitPayUserId))
                return null;

            SupportRequestDto supportRequestDto = this.SupportRequestApplicationService.Value.GetSupportRequest(supportRequestId, visitPayUserId);

            PrintSupportRequestViewModel model = new PrintSupportRequestViewModel
            {
                SupportRequest = Mapper.Map<SupportRequestViewModel>(supportRequestDto),
                RegistrationYear = supportRequestDto.VpGuarantor.RegistrationDate.Year,
                UserName = supportRequestDto.VpGuarantor.User.UserName
            };

            string sortOrderGuarantor = this.TempData[PrintSupportRequestSortOrderGuarantorKey] as string;
            if (!string.IsNullOrEmpty(sortOrderGuarantor))
            {
                model.SupportRequest.GuarantorMessages = sortOrderGuarantor == "desc" ?
                    model.SupportRequest.GuarantorMessages.OrderByDescending(x => x.InsertDate).ToList() :
                    model.SupportRequest.GuarantorMessages.OrderBy(x => x.InsertDate).ToList();
            }

            string sortOrderInternal = this.TempData[PrintSupportRequestSortOrderInternalKey] as string;
            if (!string.IsNullOrEmpty(sortOrderInternal))
            {
                model.SupportRequest.InternalMessages = sortOrderInternal == "desc" ?
                    model.SupportRequest.InternalMessages.OrderByDescending(x => x.InsertDate).ToList() :
                    model.SupportRequest.InternalMessages.OrderBy(x => x.InsertDate).ToList();
            }

            SupportRequestMessageDto closeMessage = this.SupportRequestApplicationService.Value.GetClosedMessage(supportRequestDto);

            if (closeMessage != null)
            {
                //VPNG-18700
                model.SupportRequest.ClosedBy = (isClient)? closeMessage.VisitPayUser.DisplayFirstNameLastName: closeMessage.VisitPayUser.FirstName;
                model.SupportRequest.ClosedDate = closeMessage.InsertDate.ToString(Format.DateFormat);
            }

            return model;
        }

        #endregion

        [NonAction]
        public SupportRequestWidgetViewModel CreateSupportRequestWidgetViewModel(bool isMobile)
        {
            SupportRequestResultsDto supportRequestResultsDto = this.SupportRequestApplicationService.Value.GetSupportRequests(new SupportRequestFilterDto
            {
                SortField = "InsertDate",
                SortOrder = "desc",
                SupportRequestStatus = SupportRequestStatusEnum.Open
            }, 0, 25, this.CurrentUserId);

            List<SupportRequestMessageDto> supportRequestMessagesDto = supportRequestResultsDto.SupportRequests.Select(x => x.SupportRequestMessages.OrderBy(m => m.InsertDate).FirstOrDefault()).Where(x => x != null).ToList();
            List<SupportRequestMessageListingViewModel> supportRequestMessages = Mapper.Map<List<SupportRequestMessageListingViewModel>>(supportRequestMessagesDto);

            SupportRequestWidgetViewModel supportRequestWidgetViewModel = new SupportRequestWidgetViewModel(isMobile) {SupportRequestMessages = supportRequestMessages};

            CmsVersionDto supportWidgetSelfServiceText = this.ContentApplicationService.Value.GetCurrentVersionAsync(CmsRegionEnum.SupportWidgetSelfServiceText, true, supportRequestWidgetViewModel.ReplacementDictionary).Result;
            supportRequestWidgetViewModel.SelfServiceTextCmsVersionDto = supportWidgetSelfServiceText;
            return supportRequestWidgetViewModel;
        }
    }
}

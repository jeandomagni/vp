﻿namespace Ivh.Common.Web.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using System.Web.Mvc;
    using Ivh.Application.Content.Common.Dtos;
    using Ivh.Application.Content.Common.Interfaces;
    using Ivh.Application.Core.Common.Dtos;
    using Ivh.Application.Core.Common.Interfaces;
    using Ivh.Application.FileStorage.Common.Dtos;
    using Ivh.Application.FileStorage.Common.Interfaces;
    using AutoMapper;
    using Base.Enums;
    using Base.Utilities.Extensions;
    using Extensions;
    using Filters;
    using Interfaces;
    using Models.Itemization;
    using VisitPay.Enums;
    using VisitPay.Strings;

    [Authorize(Roles = VisitPayRoleStrings.System.Patient + "," + VisitPayRoleStrings.System.Client + "," + VisitPayRoleStrings.Admin.Administrator)]
    [RequireFeature(VisitPayFeatureEnum.Itemization)]
    public class BaseItemizationController : BaseWebController
    {
        private readonly Lazy<IContentApplicationService> _contentApplicationService;
        private readonly Lazy<IFileStorageApplicationService> _fileStorageApplicationService;
        private readonly Lazy<IVisitItemizationStorageApplicationService> _visitItemizationStorageApplicationService;

        public BaseItemizationController(
            Lazy<IBaseControllerService> baseControllerService,
            Lazy<IContentApplicationService> contentApplicationService,
            Lazy<IFileStorageApplicationService> fileStorageApplicationService,
            Lazy<IVisitItemizationStorageApplicationService> visitItemizationStorageApplicationService)
            : base(baseControllerService)
        {
            this._contentApplicationService = contentApplicationService;
            this._fileStorageApplicationService = fileStorageApplicationService;
            this._visitItemizationStorageApplicationService = visitItemizationStorageApplicationService;
        }

        [NonAction]
        public ItemizationSearchFilterViewModel GetItemizationSearchFilter(int vpGuarantorId)
        {
            ItemizationSearchFilterViewModel model = new ItemizationSearchFilterViewModel();

            List<string> patientNames = this._visitItemizationStorageApplicationService.Value.GetAllPatientNames(vpGuarantorId, this.CurrentUserId).ToList();
            patientNames.ForEach(x => model.Patients.Add(new SelectListItem { Text = x, Value = x }));

            IDictionary<string, string> facilities = this._visitItemizationStorageApplicationService.Value.GetAllFacilities(vpGuarantorId, this.CurrentUserId);
            model.Facilities.AddRange(facilities.ToSelectList());

            return model;
        }

        [NonAction]
        public ItemizationSearchResultsViewModel GetItemizations(int vpGuarantorId, ItemizationSearchFilterViewModel filterModel, int page, int rows, string sidx, string sord)
        {
            VisitItemizationStorageFilterDto filter = new VisitItemizationStorageFilterDto()
            {
                SortField = sidx,
                SortOrder = sord,
                CurrentVisitPayUserId = this.CurrentUserId
            };

            Mapper.Map(filterModel, filter);

            VisitItemizationStorageResultsDto results = this._visitItemizationStorageApplicationService.Value.GetItemizations(vpGuarantorId, filter, page, rows);

            return new ItemizationSearchResultsViewModel(page, rows, results.TotalRecords)
            {
                Results = results.Itemizations.Select(x =>
                {
                    ItemizationSearchResultViewModel itemizationSearchResultViewModel = Mapper.Map<ItemizationSearchResultViewModel>(x);
                    itemizationSearchResultViewModel.VisitFileStoredId = this.SessionFacade.Value.VisitItemizationObfuscator.Obfuscate(x.VisitFileStoredId);
                    return itemizationSearchResultViewModel;
                }).ToList()
            };
        }

        [HttpGet]
        public async Task<JsonResult> ItemizationDownloadModal()
        {
            CmsVersionDto cmsVersion = await this._contentApplicationService.Value.GetCurrentVersionAsync(CmsRegionEnum.ItemizationDownload).ConfigureAwait(true);

            return this.Json(new
            {
                ContentTitle = cmsVersion.ContentTitle,
                ContentBody = cmsVersion.ContentBody
            }, JsonRequestBehavior.AllowGet);
        }

        public virtual async Task<ActionResult> ItemizationDownload(Guid visitFileStoreUniqueId, int vpGuarantorId, int currentVisitPayUserId)
        {
            int visitFileStoreId = this.SessionFacade.Value.VisitItemizationObfuscator.Clarify(visitFileStoreUniqueId);
            VisitItemizationStorageDto visitItemizationStorageDto = this._visitItemizationStorageApplicationService.Value.GetItemization(vpGuarantorId, visitFileStoreId, currentVisitPayUserId);
            if (visitItemizationStorageDto == null)
            {
                return this.RedirectToAction("NotFound", "Error");
            }

            FileStorageGetResponseDto response = await this._fileStorageApplicationService.Value.GetFileAsync(visitItemizationStorageDto.ExternalStorageKey);
            if (response != null && response.FileStorage != null)
            {
                this.WritePdfInline(response.FileStorage.FileContents, response.FileStorage.Filename);
            }

            return null;
        }
    }
}
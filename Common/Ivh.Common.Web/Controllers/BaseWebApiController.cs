﻿namespace Ivh.Common.Web.Controllers
{
    using System;
    using System.Web.Http;
    using Base.Enums;
    using Base.Utilities.Helpers;
    using Domain.Settings.Enums;
    using Domain.Settings.Interfaces;
    using Interfaces;
    using Ivh.Application.Core.Common.Dtos;
    using Ivh.Application.Core.Common.Interfaces;
    using Ivh.Application.Logging.Common.Interfaces;
    using Session;
    using VisitPay.Enums;

    public abstract class BaseWebApiController : ApiController
    {
        private AnaltyicsDto _analyticsDto;
        private ClientDto _clientDto;

        private readonly Lazy<IBaseControllerService> _baseControllerService;

        protected BaseWebApiController(Lazy<IBaseControllerService> baseControllerService)
        {
            this._baseControllerService = baseControllerService;
        }

        protected Lazy<IApplicationSettingsService> ApplicationSettingsService => this._baseControllerService.Value.ApplicationSettingsService;

        protected Lazy<IClientApplicationService> ClientApplicationService => this._baseControllerService.Value.ClientApplicationService;

        protected Lazy<IFeatureApplicationService> FeatureApplicationService => this._baseControllerService.Value.FeatureApplicationService;

        protected Lazy<IVisitPayUserJournalEventApplicationService> VisitPayUserJournalEventApplicationService => this._baseControllerService.Value.VisitPayUserJournalEventApplicationService;

        protected Lazy<ILoggingApplicationService> Logger => this._baseControllerService.Value.Logger;

        public AnaltyicsDto AnalyticsDto => this._analyticsDto ?? (this._analyticsDto = new AnaltyicsDto()
        {
            AnalyticsId = this.ApplicationSettingsService.Value.AnalyticsId.Value,
            BaseUrl = this.ApplicationSettingsService.Value.GetUrl(UrlEnum.AnalyticsBase).AbsoluteUri,
        });

        public ClientDto ClientDto => this._clientDto ?? (this._clientDto = this.ClientApplicationService.Value.GetClient());

        public TimeZoneHelper TimeZoneHelper => this._baseControllerService.Value.TimeZoneHelper.Value;

        public DateTime SystemNow => this.TimeZoneHelper.Server.Now;
        public DateTime ClientNow => this.TimeZoneHelper.Client.Now;
        public DateTime OperationsNow => this.TimeZoneHelper.Operations.Now;

        public Lazy<ISessionFacade> SessionFacade => this._baseControllerService.Value.SessionFacade;

        protected bool IsFeatureEnabled(VisitPayFeatureEnum feature)
        {
            return this.FeatureApplicationService.Value.IsFeatureEnabled(feature);
        }
    }
}

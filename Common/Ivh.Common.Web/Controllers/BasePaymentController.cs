﻿namespace Ivh.Common.Web.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using System.Web.Mvc;
    using System.Web.SessionState;
    using Ivh.Application.Content.Common.Interfaces;
    using Ivh.Application.Core.Common.Dtos;
    using Ivh.Application.Core.Common.Interfaces;
    using Ivh.Application.FinanceManagement.Common.Dtos;
    using Ivh.Application.FinanceManagement.Common.Interfaces;
    using Ivh.Application.User.Common.Dtos;
    using AutoMapper;
    using Base.Utilities.Extensions;
    using Domain.Settings.Interfaces;
    using EventJournal;
    using Interfaces;
    using Microsoft.AspNet.Identity;
    using Models;
    using Models.Payment;
    using Models.Shared;
    using VisitPay.Constants;
    using VisitPay.Enums;
    using VisitPay.Strings;

    public abstract class BasePaymentController : BaseWebController
    {
        private const string PaymentDetailExportKey = "PaymentDetailExport";
        private const string PaymentDetailExportFileName = "PaymentReceipt.xlsx";

        private const string PaymentPendingExportKey = "PaymentPendingExport";
        private const string PaymentPendingExportFileName = "ScheduledPayments.xlsx";

        private const string PaymentHistoryExportKey = "PaymentHistoryExport";
        private const string PaymentHistoryExportFileName = "PaymentHistory.xlsx";

        protected readonly Lazy<IContentApplicationService> ContentApplicationService;
        protected readonly Lazy<IFinancePlanApplicationService> FinancePlanApplicationService;
        protected readonly Lazy<IGuarantorApplicationService> GuarantorApplicationService;
        protected readonly Lazy<IPaymentActionApplicationService> PaymentActionApplicationService;
        protected readonly Lazy<IPaymentConfigurationService> PaymentConfigurationService;
        protected readonly Lazy<IPaymentDetailApplicationService> PaymentDetailApplicationService;
        protected readonly Lazy<IPaymentOptionApplicationService> PaymentOptionApplicationService;
        protected readonly Lazy<IPaymentMethodsApplicationService> PaymentMethodsApplicationService;
        protected readonly Lazy<IPaymentMethodAccountTypeApplicationService> PaymentMethodAccountTypeApplicationService;
        protected readonly Lazy<IPaymentMethodProviderTypeApplicationService> PaymentMethodProviderTypeApplicationService;
        protected readonly Lazy<IPaymentReversalApplicationService> PaymentReversalApplicationService;
        protected readonly Lazy<IPaymentSubmissionApplicationService> PaymentSubmissionApplicationService;
        protected readonly Lazy<IVisitApplicationService> VisitApplicationService;
        protected readonly Lazy<IVisitPayUserApplicationService> VisitPayUserApplicationService;

        protected BasePaymentController(
            Lazy<IBaseControllerService> baseControllerService,
            Lazy<IContentApplicationService> contentApplicationService,
            Lazy<IFinancePlanApplicationService> financePlanApplicationService,
            Lazy<IGuarantorApplicationService> guarantorApplicationService,
            Lazy<IPaymentActionApplicationService> paymentActionApplicationService,
            Lazy<IPaymentConfigurationService> paymentConfigurationService,
            Lazy<IPaymentDetailApplicationService> paymentDetailApplicationService,
            Lazy<IPaymentMethodsApplicationService> paymentMethodsApplicationService,
            Lazy<IPaymentMethodAccountTypeApplicationService> paymentMethodAccountTypeApplicationService,
            Lazy<IPaymentMethodProviderTypeApplicationService> paymentMethodProviderTypeApplicationService,
            Lazy<IPaymentOptionApplicationService> paymentOptionApplicationService,
            Lazy<IPaymentReversalApplicationService> paymentReversalApplicationService,
            Lazy<IPaymentSubmissionApplicationService> paymentSubmissionApplicationService,
            Lazy<IVisitApplicationService> visitApplicationService,
            Lazy<IVisitPayUserApplicationService> visitPayUserApplicationService)
            : base(baseControllerService)
        {
            this.ContentApplicationService = contentApplicationService;
            this.FinancePlanApplicationService = financePlanApplicationService;
            this.GuarantorApplicationService = guarantorApplicationService;
            this.PaymentActionApplicationService = paymentActionApplicationService;
            this.PaymentConfigurationService = paymentConfigurationService;
            this.PaymentDetailApplicationService = paymentDetailApplicationService;
            this.PaymentMethodsApplicationService = paymentMethodsApplicationService;
            this.PaymentMethodAccountTypeApplicationService = paymentMethodAccountTypeApplicationService;
            this.PaymentMethodProviderTypeApplicationService = paymentMethodProviderTypeApplicationService;
            this.PaymentOptionApplicationService = paymentOptionApplicationService;
            this.PaymentReversalApplicationService = paymentReversalApplicationService;
            this.PaymentSubmissionApplicationService = paymentSubmissionApplicationService;
            this.VisitApplicationService = visitApplicationService;
            this.VisitPayUserApplicationService = visitPayUserApplicationService;
        }

        #region arrange payment

        [NonAction]
        protected virtual async Task<ArrangePaymentViewModel> GetArrangePaymentModelAsync(PaymentOptionEnum? selectedPaymentOption, int vpGuarantorId, int currentVisitPayUserId)
        {
            IList<UserPaymentOptionDto> options = await this.PaymentOptionApplicationService.Value.GetPaymentOptionsAsync(vpGuarantorId, currentVisitPayUserId);
            PaymentMenuDto menu = this.PaymentOptionApplicationService.Value.GetPaymentOptionMenu(options, selectedPaymentOption);

            ArrangePaymentViewModel model = new ArrangePaymentViewModel
            {
                PaymentMenu = new PaymentMenuViewModel
                {
                    PaymentMenuItems = Mapper.Map<IList<PaymentMenuItemViewModel>>(menu.PaymentMenuItems),
                    SelectedPaymentMenuItemId = menu.SelectedPaymentMenuItemId
                },
                PaymentOptions = Mapper.Map<IList<PaymentOptionViewModel>>(options)
            };

            return model;
        }

        [NonAction]
        protected ResultMessage SubmitArrangePayment(ArrangePaymentSubmitViewModel model, int visitPayUserId, int vpGuarantorId, string receiptViewToRender, int actionVisitPayUserId)
        {
            if (!this.ModelState.IsValid)
            {
                return new ResultMessage(false, this.GenericErrorMessage);
            }

            PaymentSubmissionDto paymentSubmissionDto = this.CreateArrangePaymentSubmissionDto(model, visitPayUserId, vpGuarantorId);

            List<ProcessPaymentResponse> processPaymentResponseList = new List<ProcessPaymentResponse>();

            // process
            if (model.PaymentOptionId.IsInCategory(PaymentOptionEnumCategory.Household))
            {
                IList<ProcessPaymentResponse> processPaymentResponses = this.PaymentSubmissionApplicationService.Value.ProcessHouseholdBalancePayment(paymentSubmissionDto, visitPayUserId, actionVisitPayUserId);
                processPaymentResponseList.AddRange(processPaymentResponses);
            }
            else
            {
                IList<ProcessPaymentResponse> processPaymentResponses = this.PaymentSubmissionApplicationService.Value.ProcessPayment(paymentSubmissionDto, visitPayUserId, actionVisitPayUserId, vpGuarantorId);
                processPaymentResponseList.AddRange(processPaymentResponses);
            }

            // errors
            if (!processPaymentResponseList.Any())
            {
                return new ResultMessage(false, this.GenericErrorMessage);
            }

            if (processPaymentResponseList.All(x => x.IsError))
            {
                string defaultMessage = this.ContentApplicationService.Value.GetTextRegion(TextRegionConstants.PleaseEnsureInformationIsCorrect);
                string failedMessage = this.ContentApplicationService.Value.GetTextRegion(TextRegionConstants.YourPaymentFailed);
                string errorMessage = processPaymentResponseList.Last().ErrorMessage.GetValueOrDefault($"{failedMessage} {defaultMessage}");
                return new ResultMessage(false, errorMessage);
            }

            // summary
            PaymentSummaryListViewModel summary = new PaymentSummaryListViewModel
            {
                Email = this.VisitPayUserApplicationService.Value.FindById(visitPayUserId.ToString()).Email,
                RedirectUrl = this.Url.Action("Index", "Home"),
                ErrorMessages = new List<string>()
            };

            if (processPaymentResponseList.All(x => x.Payment.ActualPaymentDate == null))
            {
                // scheduled payment
                summary.PaymentSummaries = processPaymentResponseList.Where(x => !x.IsError).Select(response => Mapper.Map<PaymentSummaryViewModel>(response.Payment)).ToList();
            }
            else
            {
                List<ProcessPaymentResponse> successfulPayments = processPaymentResponseList.Where(x => !x.IsError).ToList();
                if (successfulPayments.Any())
                {
                    List<IGrouping<string, ProcessPaymentResponse>> grouped = successfulPayments.GroupBy(x => x.Payment.TransactionId).ToList();

                    string confirmationString = grouped.Select(x => $"{x.First().Payment.TransactionId} ({x.Sum(z => z.Payment.ActualPaymentAmount).FormatCurrency()}, X-{x.First().Payment.PaymentMethod.LastFour})").ToList().ToCommaSeparatedString(true);

                    summary.PaymentSummaries = new PaymentSummaryViewModel
                    {
                        ActualPaymentAmount = successfulPayments.Sum(x => x.Payment.ActualPaymentAmount).FormatCurrency(),
                        IsAchType = successfulPayments.Any(x => x.Payment.PaymentMethod != null && x.Payment.PaymentMethod.IsAchType),
                        TransactionId = confirmationString
                    }.ToListOfOne();
                }

                // error messages
                bool isHousehold = processPaymentResponseList.All(x => x.Payment.IsHouseholdBalance.GetValueOrDefault(false));
                if (isHousehold)
                {
                    List<IGrouping<int, ProcessPaymentResponse>> responsesByGuarantor = processPaymentResponseList.GroupBy(x => x.Payment.Guarantor.VpGuarantorId).ToList();
                    List<PaymentMethodDto> warnPaymentMethods = new List<PaymentMethodDto>();
                    bool anyError = false;
                    foreach (IGrouping<int, ProcessPaymentResponse> grouping in responsesByGuarantor)
                    {
                        List<ProcessPaymentResponse> failedPayments = processPaymentResponseList.Where(x => x.IsError).ToList();
                        IGrouping<int?, ProcessPaymentResponse> lastResponsePayments = grouping.GroupBy(x => x.Payment.PaymentMethod.PaymentMethodId).Last();
                        if (lastResponsePayments.All(x => x.IsError))
                        {
                            // the ultimate or only payment failed, total payment amount was not satisfied
                            // presents an an error
                            decimal scheduledAmount = lastResponsePayments.Sum(x => x.Payment.ScheduledPaymentAmount);
                            PaymentMethodDto paymentMethodDto = lastResponsePayments.First().Payment.PaymentMethod;
                            GuarantorDto guarantorDto = lastResponsePayments.Last().Payment.Guarantor;

                            string defaultMessage = this.ContentApplicationService.Value.GetTextRegion(TextRegionConstants.PleaseEnsureInformationIsCorrect);
                            string errorMessage = lastResponsePayments.Last().ErrorMessage.GetValueOrDefault(defaultMessage);

                            string guarantorMessage = this.ContentApplicationService.Value.GetTextRegion(TextRegionConstants.YourPaymentForOfFailed, new Dictionary<string, string>
                            {
                                { "[[GuarantorName]]", guarantorDto.User.DisplayFirstNameLastName },
                                { "[[PaymentAmount]]", scheduledAmount.FormatCurrency() },
                                { "[[Last4]]", paymentMethodDto.LastFour }
                            });

                            summary.ErrorMessages.Add($"{guarantorMessage} {errorMessage}");

                            anyError = true;
                        }
                        else if (failedPayments.Any())
                        {
                            // some payments failed, but the ultimate was successful.  total payment amount has been satisfied
                            // presents as a warning
                            foreach (PaymentMethodDto failedPaymentMethod in failedPayments.Select(x => x.Payment.PaymentMethod))
                            {
                                if (warnPaymentMethods.Any(x => x.PaymentMethodId == failedPaymentMethod.PaymentMethodId))
                                {
                                    continue;
                                }
                                warnPaymentMethods.Add(failedPaymentMethod);
                            }
                        }
                    }

                    if (!anyError && warnPaymentMethods.Any())
                    {
                        // some payments failed, but the ultimate was successful.  total payment amount has been satisfied
                        // presents as a warning
                        summary.WarnPaymentMethods = warnPaymentMethods
                            .Distinct()
                            .Select(x => $"{x.DisplayName} X-{x.LastFour}".Trim())
                            .ToList()
                            .ToCommaSeparatedString(true)
                            .ToListOfOne();
                    }
                }
                else
                {
                    List<ProcessPaymentResponse> failedPayments = processPaymentResponseList.Where(x => x.IsError).ToList();
                    IGrouping<int?, ProcessPaymentResponse> lastResponsePayments = processPaymentResponseList.GroupBy(x => x.Payment.PaymentMethod.PaymentMethodId).Last();
                    if (failedPayments.Any() && lastResponsePayments.All(x => !x.IsError))
                    {
                        // some payments failed, but the ultimate was successful.  total payment amount has been satisfied
                        // presents as a warning
                        summary.WarnPaymentMethods = failedPayments
                            .Select(x => x.Payment.PaymentMethod)
                            .Distinct()
                            .Select(x => $"{x.DisplayName} X-{x.LastFour}".Trim())
                            .ToList()
                            .ToCommaSeparatedString(true)
                            .ToListOfOne();
                    }
                    else if (lastResponsePayments.All(x => x.IsError))
                    {
                        // the ultimate or only payment failed, total payment amount was not satisfied
                        // presents an an error
                        decimal scheduledAmount = lastResponsePayments.Sum(x => x.Payment.ScheduledPaymentAmount);
                        PaymentMethodDto paymentMethodDto = lastResponsePayments.First().Payment.PaymentMethod;

                        string defaultMessage = this.ContentApplicationService.Value.GetTextRegion(TextRegionConstants.PleaseEnsureInformationIsCorrect);
                        string errorMessage = lastResponsePayments.Last().ErrorMessage.GetValueOrDefault(defaultMessage);

                        string guarantorMessage = this.ContentApplicationService.Value.GetTextRegion(TextRegionConstants.YourPaymentOfFailed, new Dictionary<string, string>
                        {
                            { "[[PaymentAmount]]", scheduledAmount.FormatCurrency() },
                            { "[[Last4]]", paymentMethodDto.LastFour }
                        });

                        summary.ErrorMessages = $"{guarantorMessage} {errorMessage}".ToListOfOne();
                    }
                }
            }

            this.ViewData.Model = summary;
            string receiptHtml = this.RenderPartialViewToString(receiptViewToRender);

            PaymentResultsModel paymentResults = new PaymentResultsModel(true, receiptHtml);
            ProcessPaymentResponse paymentResponse = processPaymentResponseList.FirstOrDefault();

            if (paymentResponse?.Payment != null)
            {
                paymentResults.PaymentId = paymentResponse.Payment.PaymentId;
            }

            return paymentResults;
        }

        [NonAction]
        protected ValidatePaymentResponse ValidateArrangePayment(ArrangePaymentSubmitViewModel model, int visitPayUserId, int vpGuarantorId)
        {
            if (!this.ValidateModelState("BasePaymentController.ValidateArrangePayment"))
            {
                string message = this.ContentApplicationService.Value.GetTextRegion(TextRegionConstants.PaymentValidationGeneralError);
                return new ValidatePaymentResponse(false, message);
            }

            PaymentSubmissionDto paymentSubmissionDto = this.CreateArrangePaymentSubmissionDto(model, visitPayUserId, vpGuarantorId);
            return this.PaymentSubmissionApplicationService.Value.ValidatePayment(paymentSubmissionDto, vpGuarantorId);
        }

        [NonAction]
        private PaymentSubmissionDto CreateArrangePaymentSubmissionDto(ArrangePaymentSubmitViewModel model, int visitPayUserId, int vpGuarantorId)
        {
            IList<ResubmitPaymentDto> resubmitModels = new List<ResubmitPaymentDto>();
            if (model.Payments != null)
            {
                IList<ResubmitPaymentDto> resubmitData = this.FinancePlanApplicationService.Value.GetPaymentResubmitDtosForFinancePlans(vpGuarantorId);
                foreach (KeyValuePair<int, decimal> modelPayment in model.Payments) // loop through what the user selected in the UI
                {
                    ResubmitPaymentDto found = resubmitData.FirstOrDefault(y => y.FinancePlanId == modelPayment.Key); // Match to the generated data
                    if (found != null)
                    {
                        found.SnapshotTotalPaymentAmount = Math.Min(found.SnapshotTotalPaymentAmount, modelPayment.Value); // If the user selected something less, use it.
                        resubmitModels.Add(found);
                    }
                }
            }

            IList<VisitPaymentDto> visitPayments = (model.Visits ?? new Dictionary<int, decimal>())
                .Select(x => new VisitPaymentDto
                {
                    PaymentAmount = x.Value,
                    VisitId = x.Key
                })
                .ToList();

            IList<FinancePlanPaymentDto> financePlanPayments = (model.FinancePlans ?? new Dictionary<int, decimal>())
                .Select(x => new FinancePlanPaymentDto
                {
                    PaymentAmount = x.Value,
                    FinancePlanId = x.Key
                })
                .ToList();

            PaymentTypeEnum paymentType = this.PaymentOptionApplicationService.Value.ConvertPaymentOptionToPaymentType(model.PaymentOptionId, model.PayBucketZero);
            IList<int> householdStatements = (model.HouseholdBalances ?? new Dictionary<int, decimal>()).Select(x => x.Key).ToList();

            PaymentSubmissionDto paymentSubmissionDto = new PaymentSubmissionDto
            {
                PaymentDate = model.PaymentDate,
                PaymentMethodId = model.PaymentMethodId,
                PaymentType = paymentType,
                TotalPaymentAmount = model.PaymentAmount.GetValueOrDefault(0),
                PaymentsToResubmit = resubmitModels,
                HouseholdStatements = householdStatements,
                VisitPayments = visitPayments,
                FinancePlanPayments = financePlanPayments,
            };

            return paymentSubmissionDto;
        }

        #endregion

        #region scheduled payments

        [NonAction]
        public IList<PaymentPendingSearchResultViewModel> GetScheduledPayments(int madeForVpGuarantorId, int currentVisitPayUserId)
        {
            IList<ScheduledPaymentDto> scheduledPaymentDtos = this.PaymentDetailApplicationService.Value.GetScheduledPayments(madeForVpGuarantorId);
            if (scheduledPaymentDtos.IsNullOrEmpty())
            {
                return new List<PaymentPendingSearchResultViewModel>(0);
            }

            List<PaymentPendingSearchResultViewModel> model = new List<PaymentPendingSearchResultViewModel>(scheduledPaymentDtos.Count);
            model.AddRange(scheduledPaymentDtos.Select(Mapper.Map<PaymentPendingSearchResultViewModel>));

            return model;
        }

        [NonAction]
        [Common.Web.Attributes.SessionState(SessionStateBehavior.Required)]
        public async Task<JsonResult> GetPendingPaymentsExportAsync(int madeForVpGuarantorId)
        {
            byte[] bytes = await this.PaymentDetailApplicationService.Value.ExportPaymentPendingAsync(madeForVpGuarantorId, this.User.Identity.GetUserName());
            this.SetTempData(PaymentPendingExportKey, bytes);

            return this.Json(new ResultMessage(true, this.Url.Action("PaymentPendingExportDownload")));
        }

        [HttpGet]
        public ActionResult PaymentPendingExportDownload()
        {
            object tempData = this.TempData[PaymentPendingExportKey];
            if (tempData != null)
            {
                this.WriteExcelInline((byte[])tempData, PaymentPendingExportFileName);
            }

            return this.Json(new { }, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region payment history

        [NonAction]
        public PaymentHistorySearchResultsViewModel GetPaymentHistory(int madeForVpGuarantorId, PaymentHistorySearchFilterViewModel model, int page, int rows, string sidx, string sord, int currentVisitPayUserId)
        {
            PaymentProcessorResponseFilterDto filterDto = new PaymentProcessorResponseFilterDto
            {
                SortField = sidx,
                SortOrder = sord
            };

            Mapper.Map(model, filterDto);

            int currentVpGuarantorId = this.GuarantorApplicationService.Value.GetGuarantorId(currentVisitPayUserId);
            PaymentHistoryResultsDto paymentResultsDto = this.PaymentDetailApplicationService.Value.GetPaymentHistory(madeForVpGuarantorId, currentVpGuarantorId, filterDto, page, rows);

            IList<int> paymentProcessorResponseIds = paymentResultsDto.Payments.Where(x => x.PaymentProcessorResponseId > 0).Select(x => x.PaymentProcessorResponseId).ToList();
            IDictionary<int, RefundPaymentStatusInfoDto> paymentReversalStatuses = this.PaymentReversalApplicationService.Value.GetPaymentReversalStatus(paymentProcessorResponseIds);
            foreach (PaymentHistoryDto payment in paymentResultsDto.Payments)
            {
                RefundPaymentStatusInfoDto refundPaymentStatusInfo = (this.User.IsInRole(VisitPayRoleStrings.Payment.PaymentRefund) || this.User.IsInRole(VisitPayRoleStrings.Payment.PaymentVoid) || this.User.IsInRole(VisitPayRoleStrings.Payment.PaymentRefundReturnedCheck)) && paymentReversalStatuses.ContainsKey(payment.PaymentProcessorResponseId) ?
                    paymentReversalStatuses[payment.PaymentProcessorResponseId] : null;
                payment.PaymentReversalStatusEnum = refundPaymentStatusInfo?.PaymentReversalStatus ?? PaymentReversalStatusEnum.NotReversable;
                payment.PaymentRefundableType = refundPaymentStatusInfo?.PaymentRedundableType ?? PaymentRefundableTypeEnum.Default;
            }

            List<PaymentHistorySearchResultViewModel> paymentHistorySearchResultViewModel = new List<PaymentHistorySearchResultViewModel>(paymentResultsDto.TotalRecords);
            paymentHistorySearchResultViewModel.AddRange(paymentResultsDto.Payments.Select(Mapper.Map<PaymentHistorySearchResultViewModel>));
            paymentHistorySearchResultViewModel.ForEach(x =>
            {
                x.CanTakeAction = x.MadeByVisitPayUserId == currentVisitPayUserId;
                x.CanRefund = this.User.IsInRole(VisitPayRoleStrings.Payment.PaymentRefund);
                x.CanVoid = this.User.IsInRole(VisitPayRoleStrings.Payment.PaymentVoid);
                x.CanRefundReturnedCheck = this.User.IsInRole(VisitPayRoleStrings.Payment.PaymentRefundReturnedCheck);
            });

            this.AssignLastActionToPaymentHistory(paymentHistorySearchResultViewModel);

            return new PaymentHistorySearchResultsViewModel
            {
                Payments = paymentHistorySearchResultViewModel,
                page = page,
                total = Convert.ToInt32(Math.Ceiling((decimal)paymentResultsDto.TotalRecords / rows)),
                records = paymentResultsDto.TotalRecords
            };
        }

        [NonAction]
        [Common.Web.Attributes.SessionState(SessionStateBehavior.Required)]
        public async Task<JsonResult> GetPaymentHistoryExportAsync(int madeForVpGuarantorId, PaymentHistorySearchFilterViewModel model, string sidx, string sord, int currentVisitPayUserId)
        {
            PaymentProcessorResponseFilterDto filterDto = new PaymentProcessorResponseFilterDto
            {
                SortField = sidx,
                SortOrder = sord
            };
            Mapper.Map(model, filterDto);

            int currentVpGuarantorId = this.GuarantorApplicationService.Value.GetGuarantorId(currentVisitPayUserId);
            byte[] bytes = await this.PaymentDetailApplicationService.Value.ExportPaymentHistoryAsync(madeForVpGuarantorId, currentVpGuarantorId, filterDto, this.User.Identity.GetUserName());
            this.SetTempData(PaymentHistoryExportKey, bytes);

            return this.Json(new ResultMessage(true, this.Url.Action("PaymentHistoryExportDownload")));
        }

        [HttpGet]
        public ActionResult PaymentHistoryExportDownload()
        {
            object tempData = this.TempData[PaymentHistoryExportKey];
            if (tempData != null)
            {
                this.WriteExcelInline((byte[])tempData, PaymentHistoryExportFileName);
            }

            return this.Json(new { }, JsonRequestBehavior.AllowGet);
        }

        private void AssignLastActionToPaymentHistory(List<PaymentHistorySearchResultViewModel> paymentHistorySearchResultViewModel)
        {
            if (this.ApplicationSettingsService.Value.IsClientApplication.Value)
            {
                IList<PaymentEventDto> paymentEvents = new List<PaymentEventDto>();

                List<IGrouping<int, PaymentHistorySearchResultViewModel>> grouping = paymentHistorySearchResultViewModel.Where(x => x.PaymentTypeId == (int)PaymentTypeEnum.PartialRefund ||
                                                                                                                                    x.PaymentTypeId == (int)PaymentTypeEnum.Refund ||
                                                                                                                                    x.PaymentTypeId == (int)PaymentTypeEnum.Void).GroupBy(x => x.VisitPayUserId).ToList();

                grouping.ForEach(x =>
                {
                    int vpGuarantorId = this.VisitPayUserApplicationService.Value.GetGuarantorIdFromVisitPayUserId(x.Key);
                    IList<PaymentEventDto> events = this.PaymentDetailApplicationService.Value.GetPaymentEvents(x.SelectMany(z => z.PaymentIds).ToList(), vpGuarantorId);
                    if (events.Any())
                        paymentEvents.AddRange(events);
                });

                IReadOnlyList<VisitPayUserDto> visitPayClientUsers = this.VisitPayUserApplicationService.Value.GetVisitPayClientUsers();
                foreach (PaymentEventDto paymentEvent in paymentEvents)
                {
                    VisitPayUserDto visitPayUser = visitPayClientUsers.FirstOrDefault(x => x.VisitPayUserId == paymentEvent.VisitPayUserId);
                    if (visitPayUser != null)
                    {
                        paymentHistorySearchResultViewModel.Where(x => x.PaymentIds.Contains(paymentEvent.Payment.PaymentId)).ToList().ForEach(x => x.ActionTakenBy = visitPayUser.DisplayFirstNameLastName);
                    }
                }
            }
            else
            {
                string clientBrandName = this.ClientDto.ClientBrandName;
                paymentHistorySearchResultViewModel.Where(x => x.PaymentTypeId == (int)PaymentTypeEnum.PartialRefund ||
                                                               x.PaymentTypeId == (int)PaymentTypeEnum.Refund ||
                                                               x.PaymentTypeId == (int)PaymentTypeEnum.Void).ToList().ForEach(x => x.ActionTakenBy = clientBrandName);
            }
        }

        #endregion

        #region payment detail

        [NonAction]
        public PartialViewResult GetPaymentDetail(int paymentProcessorResponseId, int madeForVpGuarantorId, int currentVisitPayUserId)
        {
            int currentVpGuarantorId = this.GuarantorApplicationService.Value.GetGuarantorId(currentVisitPayUserId);
            PaymentDetailDto paymentDetailDto = this.PaymentDetailApplicationService.Value.GetPaymentDetail(madeForVpGuarantorId, currentVpGuarantorId, paymentProcessorResponseId);
            if (paymentDetailDto == null)
            {
                return this.PartialView("_PaymentDetail", new PaymentDetailViewModel());
            }

            PaymentDetailViewModel model = Mapper.Map<PaymentDetailViewModel>(paymentDetailDto);

            foreach (PaymentAllocationViewModel paymentAllocation in model.PaymentAllocations)
            {
                paymentAllocation.ShowVisitLink = true;
                if (paymentDetailDto.IsPreviousConsolidation || paymentAllocation.IsVisitRemoved)
                {
                    paymentAllocation.ShowVisitLink = false;
                }
            }

            return this.PartialView("_PaymentDetail", model);
        }

        [NonAction]
        [Common.Web.Attributes.SessionState(SessionStateBehavior.Required)]
        public async Task<JsonResult> GetPaymentDetailExportAsync(int paymentProcessorResponseId, int madeForVpGuarantorId, int currentVisitPayUserId)
        {
            int currentVpGuarantorId = this.GuarantorApplicationService.Value.GetGuarantorId(currentVisitPayUserId);
            byte[] bytes = await this.PaymentDetailApplicationService.Value.ExportPaymentDetailAsync(madeForVpGuarantorId, currentVpGuarantorId, paymentProcessorResponseId, this.User.Identity.GetUserName());
            this.SetTempData(PaymentDetailExportKey, bytes);

            return this.Json(new ResultMessage(true, this.Url.Action("PaymentDetailExportDownload")));
        }

        [HttpGet]
        public ActionResult PaymentDetailExportDownload()
        {
            object tempData = this.TempData[PaymentDetailExportKey];
            if (tempData != null)
            {
                this.WriteExcelInline((byte[])tempData, PaymentDetailExportFileName);
            }

            return this.Json(new { }, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region payment edit/reschedule/cancel

        [NonAction]
        public virtual JsonResult ValidatePaymentEdit(PaymentRescheduleDto paymentRescheduleDto, int vpGuarantorId)
        {
            if (paymentRescheduleDto.PaymentType == PaymentTypeEnum.RecurringPaymentFinancePlan)
            {
                return this.Json(true);
            }

            if (!paymentRescheduleDto.PaymentMethodId.HasValue ||
                !paymentRescheduleDto.RescheduledPaymentDate.HasValue)
            {
                return this.Json(true);
            }

            ValidatePaymentResponse validatePaymentResponse = this.PaymentSubmissionApplicationService.Value.ValidateRescheduledPayment(
                paymentRescheduleDto.PaymentId.GetValueOrDefault(0),
                paymentRescheduleDto.PaymentMethodId.Value,
                paymentRescheduleDto.RescheduledPaymentDate.Value,
                vpGuarantorId);

            return validatePaymentResponse == null ? this.Json(true) : this.Json(validatePaymentResponse);
        }

        [NonAction]
        public async Task<PaymentCancelViewModel> GetPaymentCancelModelAsync(bool isRecurringPayment, int? paymentId, int vpGuarantorId)
        {
            bool isEligibleToCancel;
            PaymentTypeEnum paymentType;
            CmsRegionEnum cmsRegionEnum;

            PaymentCancelViewModel model = new PaymentCancelViewModel();

            if (isRecurringPayment)
            {
                paymentType = PaymentTypeEnum.RecurringPaymentFinancePlan;

                isEligibleToCancel = this.ApplicationSettingsService.Value.IsClientApplication.Value || this.PaymentActionApplicationService.Value.IsEligibleToCancel(vpGuarantorId, this.CurrentUserId, PaymentTypeEnum.RecurringPaymentFinancePlan);
                if (!isEligibleToCancel)
                {
                    cmsRegionEnum = CmsRegionEnum.PaymentEditCancelRecurringNotEligible;
                }
                else
                {
                    FinancePlanCancelPaymentDto financePlanCancelPaymentDto = this.FinancePlanApplicationService.Value.GetFinancePlanCancelPayment(vpGuarantorId);

                    model.PaymentDueDate = financePlanCancelPaymentDto.PaymentDueDate.ToString(Format.DateFormat);
                    model.AmountDueAfter = financePlanCancelPaymentDto.AmountDueAfter.FormatCurrency();
                    model.AmountDuePrior = financePlanCancelPaymentDto.AmountDueBefore.FormatCurrency();

                    cmsRegionEnum = financePlanCancelPaymentDto.HasPastDue ? CmsRegionEnum.PaymentCancelRecurringPastDue : CmsRegionEnum.PaymentCancelRecurring;
                }
            }
            else
            {
                if (!paymentId.HasValue)
                {
                    return null;
                }

                PaymentDto paymentDto = this.PaymentDetailApplicationService.Value.GetPayment(vpGuarantorId, paymentId.Value);
                if (paymentDto == null)
                {
                    return null;
                }

                paymentType = paymentDto.PaymentType;
                isEligibleToCancel = this.ApplicationSettingsService.Value.IsClientApplication.Value || this.PaymentActionApplicationService.Value.IsEligibleToCancel(vpGuarantorId, this.CurrentUserId, paymentType);
                cmsRegionEnum = CmsRegionEnum.PaymentCancelManual;
            }

            model.IsRecurringPayment = isRecurringPayment;
            model.IsEligibleToCancel = isEligibleToCancel;
            model.PaymentCancelReasons = isEligibleToCancel ? this.PaymentActionApplicationService.Value.GetPaymentCancelReasons(paymentType) : new List<PaymentActionReasonDto>();
            model.PaymentType = paymentType;
            model.CmsCancel = Mapper.Map<CmsViewModel>(await this.ContentApplicationService.Value.GetCurrentVersionAsync(cmsRegionEnum).ConfigureAwait(true));

            return model;
        }

        [NonAction]
        public async Task<PaymentEditViewModel> GetPaymentEditModalAsync(bool isRecurringPayment, int? paymentId, int vpGuarantorId)
        {
            ScheduledPaymentDto scheduledPaymentDto = null;
            if (isRecurringPayment)
            {
                scheduledPaymentDto = this.PaymentDetailApplicationService.Value.GetScheduledRecurringPayment(vpGuarantorId);
                if (scheduledPaymentDto == null || scheduledPaymentDto.ScheduledPaymentAmount <= 0m)
                {
                    // allow primary change without recurring payment
                    return new PaymentEditViewModel
                    {
                        IsPaymentMethodsEnabled = this.UserHasPaymentMethodPermissions(PaymentTypeEnum.RecurringPaymentFinancePlan),
                        IsCancelEnabled = false,
                        IsRescheduleEnabled = false,
                        PaymentType = PaymentTypeEnum.RecurringPaymentFinancePlan
                    };
                }
            }
            else if (paymentId.HasValue)
            {
                scheduledPaymentDto = this.PaymentDetailApplicationService.Value.GetScheduledManualPayment(vpGuarantorId, paymentId.Value);
            }

            if (scheduledPaymentDto == null)
            {
                return new PaymentEditViewModel
                {
                    IsPaymentMethodsEnabled = false,
                    IsCancelEnabled = false,
                    IsRescheduleEnabled = false
                };
            }

            DateTime maxRescheduleDate = this.PaymentActionApplicationService.Value.GetMaxAllowedRescheduleDate(vpGuarantorId, scheduledPaymentDto.PaymentType).Date;
            DateTime? overrideRescheduledDate = isRecurringPayment ? maxRescheduleDate : (DateTime?)null;

            // model
            PaymentEditViewModel model = new PaymentEditViewModel
            {
                MaxPaymentDate = maxRescheduleDate.ToString(Format.DateFormat),
                PaymentMethodId = scheduledPaymentDto.PaymentMethod?.PaymentMethodId,
                PaymentType = scheduledPaymentDto.PaymentType,
                OverrideRescheduledPaymentDate = overrideRescheduledDate?.ToString(Format.DateFormat),
            };

            // cancel
            this.SetPaymentCancelEnabled(model, scheduledPaymentDto);
            if (model.IsCancelEnabled)
            {
                if (model.IsEligibleToCancel)
                {
                    if (scheduledPaymentDto.PaymentType == PaymentTypeEnum.RecurringPaymentFinancePlan &&
                        !this.ApplicationSettingsService.Value.IsClientApplication.Value &&
                        !this.FeatureApplicationService.Value.IsFeatureEnabled(VisitPayFeatureEnum.CancelRecurringPaymentPdGuarantor))
                    {
                        // this should just be a query
                        IList<FinancePlanDto> financePlans = this.FinancePlanApplicationService.Value.GetOpenFinancePlansForGuarantor(vpGuarantorId);
                        model.ShowPastDueFinancePlanMessage = financePlans.Any(x => x.IsPastDue);
                    }
                }
            }

            // reschedule
            this.SetPaymentRescheduleEnabled(model, scheduledPaymentDto);
            if (model.IsRescheduleEnabled)
            {
                model.AllowRescheduleDateChange = scheduledPaymentDto.PaymentType != PaymentTypeEnum.RecurringPaymentFinancePlan;
                model.PaymentRescheduleReasons = this.PaymentActionApplicationService.Value.GetPaymentRescheduleReasons(scheduledPaymentDto.PaymentType);
                model.ScheduledPaymentDate = scheduledPaymentDto.ScheduledPaymentDate.Date.ToString(Format.DateFormat);

                if (model.PaymentType != PaymentTypeEnum.RecurringPaymentFinancePlan)
                {
                    // reschedule always enabled for manually scheduled payments
                    model.IsRescheduleEnabled = true;
                }
                else if (model.ScheduledPaymentDate != model.OverrideRescheduledPaymentDate)
                {
                    model.IsRescheduleEnabled = true;
                }
            }

            // payment methods
            this.SetPaymentMethodsEnabled(model, scheduledPaymentDto);

            // cms
            await this.SetCms(model, scheduledPaymentDto.PaymentType);

            //
            return model;
        }

        private async Task SetCms(PaymentEditViewModel model, PaymentTypeEnum paymentType)
        {
            CmsRegionEnum cmsPaymentEditCancel;
            CmsRegionEnum cmsPaymentEditPaymentMethod;
            CmsRegionEnum cmsPaymentEditReschedule;

            if (paymentType == PaymentTypeEnum.RecurringPaymentFinancePlan)
            {
                if (model.ShowPastDueFinancePlanMessage)
                {
                    cmsPaymentEditCancel = CmsRegionEnum.PaymentEditCancelRecurringPastDue;
                }
                else
                {
                    cmsPaymentEditCancel = model.IsEligibleToCancel ? CmsRegionEnum.PaymentEditCancelRecurring : CmsRegionEnum.PaymentEditCancelRecurringNotEligible;
                }

                cmsPaymentEditPaymentMethod = CmsRegionEnum.PaymentEditPaymentMethodRecurring;
                cmsPaymentEditReschedule = model.IsEligibleToReschedule ? CmsRegionEnum.PaymentEditRescheduleRecurring : CmsRegionEnum.PaymentEditRescheduleNotEligible;
            }
            else
            {
                cmsPaymentEditCancel = CmsRegionEnum.PaymentEditCancelManual;
                cmsPaymentEditPaymentMethod = CmsRegionEnum.PaymentEditPaymentMethodManual;
                cmsPaymentEditReschedule = CmsRegionEnum.PaymentEditRescheduleManual;
            }

            model.CmsPaymentEditCancel = Mapper.Map<CmsViewModel>(await this.ContentApplicationService.Value.GetCurrentVersionAsync(cmsPaymentEditCancel));
            model.CmsPaymentEditPaymentMethod = Mapper.Map<CmsViewModel>(await this.ContentApplicationService.Value.GetCurrentVersionAsync(cmsPaymentEditPaymentMethod));
            model.CmsPaymentEditReschedule = Mapper.Map<CmsViewModel>(await this.ContentApplicationService.Value.GetCurrentVersionAsync(cmsPaymentEditReschedule));
        }

        #region payment action permissions

        [NonAction]
        protected void SetPaymentCancelEnabled(IPaymentCancelViewModel model, ScheduledPaymentDto scheduledPaymentDto)
        {
            if (scheduledPaymentDto.PaymentType == PaymentTypeEnum.RecurringPaymentFinancePlan)
            {
                VisitPayFeatureEnum featureEnum = this.ApplicationSettingsService.Value.IsClientApplication.Value ? VisitPayFeatureEnum.CancelRecurringPaymentClient : VisitPayFeatureEnum.CancelRecurringPaymentGuarantor;
                model.IsCancelEnabled = this.IsFeatureEnabled(featureEnum);
            }
            else
            {
                // always enabled for manual payments
                model.IsCancelEnabled = true;
            }

            // consolidation check - ensure current user made the payment
            model.IsCancelEnabled = model.IsCancelEnabled && this.IsCurrentUserPayment(scheduledPaymentDto);

            if (!model.IsCancelEnabled)
            {
                return;
            }

            model.IsUserPermittedCancel = this.UserHasCancelPaymentPermissions(scheduledPaymentDto.PaymentType);
            if (model.IsUserPermittedCancel)
            {
                if (scheduledPaymentDto.MadeByGuarantor != null)
                {
                    model.IsEligibleToCancel = this.ApplicationSettingsService.Value.IsClientApplication.Value ||
                                               this.PaymentActionApplicationService.Value.IsEligibleToCancel(scheduledPaymentDto.MadeByGuarantor.VpGuarantorId, this.CurrentUserId, scheduledPaymentDto.PaymentType);
                }
            }
        }

        [NonAction]
        protected void SetPaymentRescheduleEnabled(IPaymentRescheduleViewModel model, ScheduledPaymentDto scheduledPaymentDto)
        {
            if (scheduledPaymentDto.PaymentType == PaymentTypeEnum.RecurringPaymentFinancePlan)
            {
                VisitPayFeatureEnum featureEnum = this.ApplicationSettingsService.Value.IsClientApplication.Value ? VisitPayFeatureEnum.RescheduleRecurringPaymentClient : VisitPayFeatureEnum.RescheduleRecurringPaymentGuarantor;
                model.IsRescheduleEnabled = this.IsFeatureEnabled(featureEnum);
            }
            else
            {
                // always enabled for manual payments
                model.IsRescheduleEnabled = true;
            }

            // consolidation check - ensure current user made the payment
            model.IsRescheduleEnabled = model.IsRescheduleEnabled && this.IsCurrentUserPayment(scheduledPaymentDto);

            if (!model.IsRescheduleEnabled)
            {
                return;
            }

            model.IsUserPermittedReschedule = this.UserHasReschedulePaymentPermissions(scheduledPaymentDto.PaymentType);
            if (model.IsUserPermittedReschedule)
            {
                if (scheduledPaymentDto.MadeByGuarantor != null)
                {
                    model.IsEligibleToReschedule = this.ApplicationSettingsService.Value.IsClientApplication.Value ||
                                                   this.PaymentActionApplicationService.Value.IsEligibleToReschedule(scheduledPaymentDto.MadeByGuarantor.VpGuarantorId, this.CurrentUserId, scheduledPaymentDto.PaymentType);
                }
            }
        }

        [NonAction]
        protected void SetPaymentMethodsEnabled(PaymentEditViewModel model, ScheduledPaymentDto scheduledPaymentDto)
        {
            // consolidation check - ensure current user made the payment
            model.IsPaymentMethodsEnabled = scheduledPaymentDto.MadeByGuarantor == null || this.IsCurrentUserPayment(scheduledPaymentDto);

            if (model.IsPaymentMethodsEnabled)
            {
                model.IsPaymentMethodsEnabled = this.UserHasPaymentMethodPermissions(scheduledPaymentDto.PaymentType);
            }
        }

        [NonAction]
        protected bool UserHasCancelPaymentPermissions(PaymentTypeEnum paymentType)
        {
            if (this.User.IsInRole(VisitPayRoleStrings.System.Patient))
            {
                return true;
            }

            if (this.User.IsInRole(VisitPayRoleStrings.System.Client))
            {
                if (paymentType == PaymentTypeEnum.RecurringPaymentFinancePlan)
                {
                    return this.User.IsInRole(VisitPayRoleStrings.Csr.CancelRecurringPayment);
                }

                return this.User.IsInRole(VisitPayRoleStrings.Csr.ArrangePayment);
            }

            return false;
        }

        [NonAction]
        protected bool UserHasReschedulePaymentPermissions(PaymentTypeEnum paymentType)
        {
            if (this.User.IsInRole(VisitPayRoleStrings.System.Patient))
            {
                return true;
            }

            if (this.User.IsInRole(VisitPayRoleStrings.System.Client))
            {
                if (paymentType == PaymentTypeEnum.RecurringPaymentFinancePlan)
                {
                    return this.User.IsInRole(VisitPayRoleStrings.Csr.RescheduleRecurringPayment);
                }

                return this.User.IsInRole(VisitPayRoleStrings.Csr.ArrangePayment);
            }

            return false;
        }

        [NonAction]
        protected bool UserHasPaymentMethodPermissions(PaymentTypeEnum paymentType)
        {
            if (this.User.IsInRole(VisitPayRoleStrings.System.Patient))
            {
                return true;
            }

            if (this.User.IsInRole(VisitPayRoleStrings.System.Client))
            {
                if (paymentType == PaymentTypeEnum.RecurringPaymentFinancePlan)
                {
                    return false;
                }

                return this.User.IsInRole(VisitPayRoleStrings.Csr.ManagePaymentMethods);
            }

            return false;
        }

        [NonAction]
        protected bool IsCurrentUserPayment(ScheduledPaymentDto scheduledPaymentDto)
        {
            if (this.User.IsInRole(VisitPayRoleStrings.System.Patient))
            {
                return this.CurrentUserId == scheduledPaymentDto.MadeByGuarantor?.User.VisitPayUserId;
            }

            return true;
        }

        #endregion

        #endregion

        #region payment methods

        [NonAction]
        protected async Task<PaymentMethodResultDto> SavePaymentMethodBank(BankAccountViewModel model, int vpGuarantorId, int actionVisitPayUserId)
        {
            PaymentMethodDto paymentMethodDto = model.PaymentMethodId == default(int) ? new PaymentMethodDto() : this.PaymentMethodsApplicationService.Value.GetPaymentMethodById(model.PaymentMethodId, vpGuarantorId);
            Mapper.Map(model, paymentMethodDto);

            return await this.PaymentMethodsApplicationService.Value.SaveBankPaymentMethodAsync(Mapper.Map<JournalEventHttpContextDto>(System.Web.HttpContext.Current), paymentMethodDto, vpGuarantorId, actionVisitPayUserId, model.AccountNumber, model.RoutingNumber);
        }

        [NonAction]
        protected PaymentMethodResultDto SavePaymentMethodCard(CardAccountViewModel model, int vpGuarantorId, int actionVisitPayUserId)
        {
            PaymentMethodDto paymentMethodDto = model.PaymentMethodId == default(int) ? new PaymentMethodDto() : this.PaymentMethodsApplicationService.Value.GetPaymentMethodById(model.PaymentMethodId, vpGuarantorId);
            Mapper.Map(model, paymentMethodDto);

            return this.PaymentMethodsApplicationService.Value.SaveCardPaymentMethod(Mapper.Map<JournalEventHttpContextDto>(System.Web.HttpContext.Current), paymentMethodDto, vpGuarantorId, actionVisitPayUserId);
        }

        [NonAction]
        protected void LogInvalidCreditCard(int? visitPayUserId, int? vpGuarantorId, string errorMessage, bool isTimeout)
        {
            if (errorMessage.IsNotNullOrEmpty() && !isTimeout)
            {
                string upper = errorMessage.ToUpper();
                if (upper.Contains(SecurePanConstants.ErrorMessagePartialInvalidCardNumber) ||
                    upper.Contains(SecurePanConstants.ErrorMessagePartialInvalidCardNumber19) ||
                    upper.Contains(SecurePanConstants.ErrorMessagePartialIncorrectInformation))
                {
                    this.MetricsProvider.Value.Increment(Metrics.Increment.SecurePan.PaymentMethod.FailedValidation);
                }
                else if (upper.Contains(SecurePanConstants.ErrorMessagePartialUnableToProcess))
                {
                    this.MetricsProvider.Value.Increment(Metrics.Increment.SecurePan.PaymentMethod.CallFailed);
                }
                else if (upper.Contains(SecurePanConstants.ErrorMessagePartialAvsFailed))
                {
                    this.MetricsProvider.Value.Increment(Metrics.Increment.SecurePan.PaymentMethod.AvsFailed);
                }
            }

            if (isTimeout)
            {
                this.MetricsProvider.Value.Increment(Metrics.Increment.SecurePan.GatewayDown);
            }

            if (visitPayUserId.HasValue && vpGuarantorId.HasValue)
            {
                this.VisitPayUserApplicationService.Value.LogInvalidBillingIdEvent(errorMessage, visitPayUserId.Value.ToString(), vpGuarantorId.Value);
            }
        }

        #endregion
    }
}
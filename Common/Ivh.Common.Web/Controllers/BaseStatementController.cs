﻿namespace Ivh.Common.Web.Controllers
{
    using System;
    using System.Web.Mvc;
    using Ivh.Application.Content.Common.Dtos;
    using Ivh.Application.Content.Common.Interfaces;
    using Ivh.Application.Core.Common.Dtos;
    using Ivh.Application.Core.Common.Interfaces;
    using Ivh.Application.FinanceManagement.Common.Dtos;
    using Ivh.Application.FinanceManagement.Common.Interfaces;
    using AutoMapper;
    using EventJournal;
    using Interfaces;
    using Models.Statement;
    using Provider.Pdf;
    using VisitPay.Enums;
    using VisitPay.Strings;

    [Authorize(Roles = VisitPayRoleStrings.System.Patient + "," + VisitPayRoleStrings.System.Client + "," + VisitPayRoleStrings.Admin.Administrator)]
    public class BaseStatementController : BaseWebController
    {
        protected readonly Lazy<IContentApplicationService> ContentApplicationService;
        protected readonly Lazy<IPdfConverter> PdfConverter;
        protected readonly Lazy<IStatementApplicationService> StatementApplicationService;
        protected readonly Lazy<IVisitPayUserApplicationService> VisitPayUserApplicationService;
        protected readonly Lazy<IVisitApplicationService> VisitApplicationService;

        public BaseStatementController(
            Lazy<IBaseControllerService> baseControllerService,
            Lazy<IContentApplicationService> contentApplicationService,
            Lazy<IStatementApplicationService> statementApplicationService,
            Lazy<IVisitPayUserApplicationService> visitPayUserApplicationService,
            Lazy<IVisitPayUserJournalEventApplicationService> visitPayUserJournalEventApplicationService,
            Lazy<IPdfConverter> pdfConverter,
            Lazy<IVisitApplicationService> visitApplicationService)
            : base(baseControllerService)
        {
            this.ContentApplicationService = contentApplicationService;
            this.StatementApplicationService = statementApplicationService;
            this.VisitPayUserApplicationService = visitPayUserApplicationService;
            this.PdfConverter = pdfConverter;
            this.VisitApplicationService = visitApplicationService;
        }

        [NonAction]
        public StatementViewModel GetStatement(int statementId, int guarantorVisitPayUserId)
        {
            StatementDto statementDto = this.StatementApplicationService.Value.GetStatement(statementId, guarantorVisitPayUserId);

            StatementViewModel viewmodel = Mapper.Map<StatementViewModel>(statementDto);
            viewmodel.IsLastStatement = viewmodel.VpStatementId == this.StatementApplicationService.Value.GetLastStatement(this.VisitPayUserApplicationService.Value.GetGuarantorIdFromVisitPayUserId(guarantorVisitPayUserId)).VpStatementId;

            return viewmodel;
        }

        [NonAction]
        public byte[] StatementDownload(int statementId, int guarantorVisitPayUserId, int? clientVisitPayUserId)
        {
            // log download
            this.VisitPayUserJournalEventApplicationService.Value.LogStatementDownload(guarantorVisitPayUserId, clientVisitPayUserId, isSummary: true, context: Mapper.Map<JournalEventHttpContextDto>(System.Web.HttpContext.Current));

            //
            StatementDto statementDto = this.StatementApplicationService.Value.GetStatement(statementId, guarantorVisitPayUserId);
            GuarantorDto guarantor = this.VisitPayUserApplicationService.Value.GetGuarantorFromVisitPayUserId(guarantorVisitPayUserId);

            if (statementDto.IsArchived)
            {
                return this.StatementApplicationService.Value.GetStatementContent(guarantor.VpGuarantorId, statementDto.VpStatementId);
            }

            StatementViewModel statementViewModel = Mapper.Map<StatementViewModel>(statementDto);
            this.AddFacilities(statementViewModel, guarantor.VpGuarantorId);

            statementViewModel.IsLastStatement = statementViewModel.VpStatementId == this.StatementApplicationService.Value.GetLastStatement(this.VisitPayUserApplicationService.Value.GetGuarantorIdFromVisitPayUserId(guarantorVisitPayUserId)).VpStatementId;

            if ((guarantor.ConsolidationStatus == GuarantorConsolidationStatusEnum.Managing) || (guarantor.ConsolidationStatus == GuarantorConsolidationStatusEnum.Managed))
            {
                //TODO: check users are in an active consolidation at the time of statement generation (VPNG-2302)
                CmsVersionDto cmsVersion = this.ContentApplicationService.Value.GetCurrentVersionAsync(guarantor.ConsolidationStatus == GuarantorConsolidationStatusEnum.Managing ? CmsRegionEnum.StatementPDFManagingUserMessage : CmsRegionEnum.StatementPDFManagedUserMessage).Result;
                statementViewModel.ConsolidatedUserMessage = cmsVersion.ContentBody;
                statementViewModel.ConsolidatedUserMessageTitle = cmsVersion.ContentTitle;
            }

            this.ViewData.Model = statementViewModel;
            return this.PdfConverter.Value.CreatePdfFromHtml("Visit Balance Summary", this.RenderViewToString($"~/Views/Statement/StatementPdf_{statementDto.StatementVersion.ToString()}.cshtml", null));
        }

        [NonAction]
        public byte[] FinancePlanStatementDownload(int financePlanStatementId, int guarantorVisitPayUserId, int? clientVisitPayUserId)
        {
            // log download
            this.VisitPayUserJournalEventApplicationService.Value.LogStatementDownload(guarantorVisitPayUserId, clientVisitPayUserId, isSummary: false, context: Mapper.Map<JournalEventHttpContextDto>(System.Web.HttpContext.Current));

            //
            FinancePlanStatementDto fpStatementDto = this.StatementApplicationService.Value.GetFinancePlanStatement(financePlanStatementId, guarantorVisitPayUserId);
            GuarantorDto guarantor = this.VisitPayUserApplicationService.Value.GetGuarantorFromVisitPayUserId(guarantorVisitPayUserId);

            FinancePlanStatementViewModel statementViewModel = Mapper.Map<FinancePlanStatementViewModel>(fpStatementDto);
            this.AddFacilities(statementViewModel, guarantor.VpGuarantorId);

            this.ViewData.Model = statementViewModel;
            string statementHtml = this.RenderViewToString($"~/Views/Statement/FinancePlanStatementPdf_{fpStatementDto.StatementVersion.ToString()}.cshtml", null);
            return this.PdfConverter.Value.CreatePdfFromHtml("Finance Plan Statement", statementHtml);
        }

        private void AddFacilities(BaseStatementViewModel model, int vpGuarantorId)
        {
            if (this.IsFeatureEnabled(VisitPayFeatureEnum.FeatureFacilityContactUsGridIsEnabled))
            {
                model.Facilities = this.VisitApplicationService.Value.GetUniqueFacilityVisits(vpGuarantorId);
            }
        }
    }
}
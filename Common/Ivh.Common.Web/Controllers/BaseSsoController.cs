﻿namespace Ivh.Common.Web.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Web;
    using System.Web.Mvc;
    using Ivh.Application.Base.Common.Dtos;
    using Ivh.Application.Content.Common.Interfaces;
    using Ivh.Application.Core.Common.Dtos;
    using Ivh.Application.Core.Common.Interfaces;
    using Ivh.Application.User.Common.Dtos;
    using AutoMapper;
    using Base.Enums;
    using Base.Utilities.Extensions;
    using Base.Utilities.Responses;
    using Cache;
    using Interfaces;
    using Models.Shared;
    using Models.Sso;
    using VisitPay.Enums;
    using VisitPay.Strings;

    public abstract class BaseSsoController : BaseWebController
    {
        protected readonly Lazy<ISsoApplicationService> SsoApplicationService;
        protected readonly Lazy<IContentApplicationService> ContentApplicationService;
        protected readonly Lazy<IGuarantorApplicationService> GuarantorApplicationService;
        private readonly Lazy<IDistributedCache> _distributedCache;

        protected BaseSsoController(
            Lazy<IBaseControllerService> baseControllerService,
            Lazy<ISsoApplicationService> ssoApplicationService,
            Lazy<IContentApplicationService> contentApplicationService,
            Lazy<IGuarantorApplicationService> guarantorApplicationService,
            Lazy<IDistributedCache> distributedCache 
            )
            : base(baseControllerService)
        {
            this.SsoApplicationService = ssoApplicationService;
            this.ContentApplicationService = contentApplicationService;
            this.GuarantorApplicationService = guarantorApplicationService;
            this._distributedCache = distributedCache;
        }

        [NonAction]
        protected SsoVisitPayUserSettingDto Decline(int visitPayUserId, SsoProviderEnum ssoProvider, int? actionTakenByVisitPayUserId = null)
        {
            SsoVisitPayUserSettingDto userSetting = this.SsoApplicationService.Value.DeclineSso(Mapper.Map<HttpContextDto>(System.Web.HttpContext.Current), ssoProvider, visitPayUserId, actionTakenByVisitPayUserId);

            return userSetting;
        }

        [NonAction]
        protected SsoVisitPayUserSettingDto Ignore(int visitPayUserId, SsoProviderEnum ssoProvider, bool isIgnored, int? actionTakenByVisitPayUserId = null)
        {
            SsoVisitPayUserSettingDto userSetting = this.SsoApplicationService.Value.IgnoreSso(ssoProvider, visitPayUserId, isIgnored, actionTakenByVisitPayUserId);

            return userSetting;
        }

        [NonAction]
        protected SsoVisitPayUserSettingDto AcknowledgedSsoInvalidation(int visitPayUserId, SsoProviderEnum ssoProvider, int? actionTakenByVisitPayUserId = null)
        {
            SsoVisitPayUserSettingDto userSetting = this.SsoApplicationService.Value.AcknowledgeInvalidatedSso(ssoProvider, visitPayUserId, actionTakenByVisitPayUserId);

            return userSetting;
        }

        [NonAction]
        protected CmsViewModel CmsView(SsoProviderWithUserSettingsViewModel settings, bool isClient)
        {
            IDictionary<string, string> additionalValues = new Dictionary<string, string>
            {
                {"[[SsoProviderDisplayName]]", settings.SsoProviderName}
            };

            // Translations to toggle between the client and the patient messages in the 3 different states
            IDictionary<string, CmsRegionEnum> cmsRegionTranslation = new Dictionary<string, CmsRegionEnum>
            {
                {"accepted", isClient ? CmsRegionEnum.SsoClientAccepted : CmsRegionEnum.SsoGuarantorAccepted},
                {"declined", isClient ? CmsRegionEnum.SsoClientDeclined : CmsRegionEnum.SsoGuarantorDeclined},
                {"notAcceptedNotDeclined", isClient ? CmsRegionEnum.SsoClientNotAcceptedNotDeclined : CmsRegionEnum.SsoGuarantorNotAcceptedNotDeclined},
            };

            CmsRegionEnum cmsRegion;
            if ((SsoProviderEnum)settings.SsoProviderId == SsoProviderEnum.OpenEpic && !settings.IsAccepted && !settings.IsDeclined)
            {
                // per VPNG-13726: accessing VP directly (not through SSO) upon the user clicking Settings > Manage SSO (assumes HS Client is enabled for SSO)
                cmsRegion = cmsRegionTranslation["notAcceptedNotDeclined"];
            }
            else
            {
                cmsRegion = settings.IsAccepted ? cmsRegionTranslation["accepted"] : cmsRegionTranslation["declined"];
            }

            return Mapper.Map<CmsViewModel>(this.ContentApplicationService.Value.GetCurrentVersionAsync(cmsRegion, true, additionalValues).Result);
        }

        [NonAction]
        protected CmsViewModel CmsDeclineView(string ssoProviderName, bool isOutbound)
        {
            IDictionary<string, string> additionalValues = new Dictionary<string, string>
            {
                {"[[SsoProviderDisplayName]]", ssoProviderName}
            };

            CmsRegionEnum cmsRegion = isOutbound ? CmsRegionEnum.SsoOutboundDisableConfirmation : CmsRegionEnum.SsoDeclineConfirmation;

            return Mapper.Map<CmsViewModel>(this.ContentApplicationService.Value.GetCurrentVersionAsync(cmsRegion, true, additionalValues).Result);
        }

        /*
        // 0 references
        [NonAction]
        protected CmsViewModel CmsAcceptView(string ssoProviderName, bool isOutbound)
        {
            IDictionary<string, string> additionalValues = new Dictionary<string, string>
                {
                    {"[[SsoProviderDisplayName]]", ssoProviderName}
                };
            return isOutbound ? Mapper.Map<CmsViewModel>(this.ContentApplicationService.Value.GetCurrentVersionAsync(CmsRegionEnum.SsoTermsCmsHealthEquityOutbound, true, additionalValues).Result) :
                Mapper.Map<CmsViewModel>(this.ContentApplicationService.Value.GetCurrentVersionAsync(CmsRegionEnum.SsoDeclineConfirmation, true, additionalValues).Result);
        }*/

        [NonAction]
        protected SamlResponseViewModel CreateSamlResponse(SamlResponseTargetEnum target, string samlRequest = null, string relayState = null)
        {
            HttpContext context = System.Web.HttpContext.Current;
            int visitPayUserId = this.CurrentUserId;
            this.SsoApplicationService.Value.AttemptedSso(Mapper.Map<HttpContextDto>(context), visitPayUserId);

            Response<SamlResponseDto> samlResponseDto = this.SsoApplicationService.Value.GetSamlResponse(visitPayUserId, target);

            bool isError = samlResponseDto?.Object == null || samlResponseDto.IsError || samlResponseDto.Object.Value.IsNullOrEmpty() || samlResponseDto.Object.Url.IsNullOrEmpty();
            if (isError)
            {
                // Need to log SSO failure
                this.SetHqySsoStatus(visitPayUserId, false);
            }
            else
            {
                // Need to log SSO success
                this.SetHqySsoStatus(visitPayUserId, true);
            }

            SamlResponseViewModel model = Mapper.Map<SamlResponseViewModel>(samlResponseDto?.Object ?? new SamlResponseDto());
            model.IsError = isError;
            model.ErrorMessage = isError ? samlResponseDto?.ErrorMessage ?? SsoMessages.FailureUnknown : string.Empty;

            return model;
        }

        private static readonly int HealthEquityTimeToKeepBalance = 30 * 60; //30 minutes in seconds
        private void SetHqySsoStatus(int visitPayUserId, bool isHqySsoUp)
        {
            string keyForCache = $"HealthEquitySsoStatus-{visitPayUserId}";
            HealthEquitySsoStatusDto status = new HealthEquitySsoStatusDto
            {
                IsHqySsoUp = isHqySsoUp,
                LastUpdated = DateTime.UtcNow
            };
            this._distributedCache.Value.SetObjectAsync(keyForCache, status, HealthEquityTimeToKeepBalance);
        }


    }
}
﻿
namespace Ivh.Common.Web.Controllers
{
    using Filters;
    using Interfaces;
    using Ivh.Application.Core.Common.Interfaces;
    using Microsoft.IdentityModel.Tokens;
    using System;
    using System.Collections.Generic;
    using System.IdentityModel.Tokens.Jwt;
    using System.Security.Claims;
    using System.Text;
    using System.Web.Mvc;
    using VisitPay.Enums;
    using VisitPay.Strings;

    [RequireFeature(VisitPayFeatureEnum.FeatureChatIsEnabled)]
    public class BaseChatController : BaseWebController
    {
        protected readonly Lazy<IChatApplicationService> ChatApplicationService;
        private readonly SymmetricSecurityKey _chatGatewayJwtSigningKey;


        protected BaseChatController(
            Lazy<IBaseControllerService> baseControllerService,
            Lazy<IChatApplicationService> chatApplicationService
        ) : base(baseControllerService)
        {
            this.ChatApplicationService = chatApplicationService;

            //Init Jwt signing key
            string secretKey = this.ClientDto.ChatGatewayJwtSigningKey;
            this._chatGatewayJwtSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(secretKey));
        }

        [HttpPost]
        public JsonResult IsChatAvailable()
        {
            bool isAvailable = this.ChatApplicationService.Value.IsChatAvailable();
            return this.Json(new
            {
                IsAvailable = isAvailable
            });
        }

        [HttpPost]
        public JsonResult GenerateChatGatewayToken()
        {
            //Determine which claims to use for the token
            List<Claim> claims = new List<Claim>();
            if (this.User.IsInRole(VisitPayRoleStrings.Csr.ChatOperatorAdmin))
            {
                claims.Add(new Claim(ClaimTypes.Role, VisitPayRoleStrings.Csr.ChatOperatorAdmin));
            }

            if (this.User.IsInRole(VisitPayRoleStrings.Csr.ChatOperator))
            {
                claims.Add(new Claim(ClaimTypes.Role, VisitPayRoleStrings.Csr.ChatOperator));
            }

            if (this.User.IsInRole(VisitPayRoleStrings.System.Patient))
            {
                claims.Add(new Claim(ClaimTypes.Role, VisitPayRoleStrings.System.Patient));
            }

            if (claims.Count == 0)
            {
                //User isn't in valid role for chat API token
                throw new UnauthorizedAccessException();
            }

            //User is in valid role for chat API token
            JwtSecurityToken token = new JwtSecurityToken(
                issuer: "VisitPay",
                audience: "ChatGateway",
                claims: claims,
                notBefore: DateTime.Now,
                expires: DateTime.Now.AddDays(28),
                signingCredentials: new SigningCredentials(this._chatGatewayJwtSigningKey, SecurityAlgorithms.HmacSha256)
            );

            string tokenResponse = new JwtSecurityTokenHandler().WriteToken(token);
            return this.Json(new
            {
                token = tokenResponse
            });
        }
    }
}

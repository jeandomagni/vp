﻿namespace Ivh.Common.Web.Interfaces
{
    using System;
    using System.Threading.Tasks;

    public interface IBruteForceDelayService
    {
        Task<TimeSpan> DelayAsync(string ipAddress, Func<bool> delayFunc, Func<bool> clearFunc = null);
    }
}
﻿namespace Ivh.Common.Web.Interfaces
{
    using System;
    using Ivh.Application.Content.Common.Interfaces;
    using Ivh.Application.Core.Common.Interfaces;
    using Ivh.Application.Logging.Common.Interfaces;
    using Base.Utilities.Helpers;
    using Domain.Logging.Interfaces;
    using Domain.Settings.Interfaces;
    using Ivh.Application.AppIntelligence.Common.Interfaces;
    using Session;

    public interface IBaseControllerService
    {
        Lazy<ISessionFacade> SessionFacade { get; }
        Lazy<IClientApplicationService> ClientApplicationService { get; }
        Lazy<IFeatureApplicationService> FeatureApplicationService { get; }
        Lazy<IApplicationSettingsService> ApplicationSettingsService { get; }
        Lazy<IThemeApplicationService> ThemeApplicationService { get; }
        Lazy<IVisitPayUserJournalEventApplicationService> VisitPayUserJournalEventApplicationService { get; }
        Lazy<IGuarantorApplicationService> GuarantorApplicationService { get; }
        Lazy<IBruteForceDelayService> BruteForceDelayService { get; }
        Lazy<IMetricsProvider> MetricsProvider { get; }
        Lazy<ILoggingApplicationService> Logger { get; }
        Lazy<TimeZoneHelper> TimeZoneHelper { get; }
        Lazy<IRandomizedTestApplicationService> RandomizedTestApplicationService { get; }
    }
}

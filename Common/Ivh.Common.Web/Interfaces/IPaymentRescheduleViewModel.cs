namespace Ivh.Common.Web.Interfaces
{
    public interface IPaymentRescheduleViewModel
    {
        bool IsRescheduleEnabled { get; set; }
        bool IsEligibleToReschedule { get; set; }
        bool IsUserPermittedReschedule { get; set; }
    }
}
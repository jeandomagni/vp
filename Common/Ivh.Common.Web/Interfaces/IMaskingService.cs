﻿namespace Ivh.Common.Web.Interfaces
{
    using System.Collections.Generic;

    public interface IMaskingService
    {
        void Mask(object source, object destination, IList<string> destinationFieldsToMask);
    }
}
﻿namespace Ivh.Common.Web.Interfaces
{
    using System.Net.Http;

    public interface IHttpMessageHandlerFactory
    {
        HttpMessageHandler Create();
    }
}
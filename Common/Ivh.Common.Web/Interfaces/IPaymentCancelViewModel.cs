namespace Ivh.Common.Web.Interfaces
{
    public interface IPaymentCancelViewModel
    {
        bool IsCancelEnabled { get; set; }
        bool IsEligibleToCancel { get; set; }
        bool IsUserPermittedCancel { get; set; }
    }
}
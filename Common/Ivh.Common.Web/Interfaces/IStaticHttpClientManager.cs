﻿namespace Ivh.Common.Web.Interfaces
{
    using System.Net.Http;

    public interface IStaticHttpClientManager
    {
        /// <summary>
        /// A static <see cref="HttpClient"/> is preferred for better TCP connection management,
        /// but for related unit tests, this flag can be used to prevent the same <see cref="HttpMessageHandler"/>
        /// from being reused if varying results are expected.
        /// </summary>
        bool UseExistingStaticHttpClient { get; }
    }
}
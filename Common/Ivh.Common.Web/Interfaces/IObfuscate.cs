﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ivh.Common.Web.Interfaces
{
    public interface IObfuscate
    {
        /// <summary>
        /// Recursively iterate through object graph and obfuscate any property value that has the [Obfuscate] attribute
        /// </summary>
        void Obfuscate();

        /// <summary>
        /// Recursively iterate through object graph and deobfuscate any property value that has the [Obfuscate] attribute
        /// </summary>
        void Deobfuscate();
    }
}

﻿namespace Ivh.Common.Web.Interfaces
{
    using System;
    using System.Web.Mvc;
    using Models;

    public interface IRegistrationService
    {
        RegisterPersonalInformationViewModel GetRegisterPersonalInformationViewModel(bool isSso);
        void ValidateRecords(ModelStateDictionary modelState, RegisterPersonalInformationViewModel model);
        DateTime? GetPatientDateOfBirth(RegisterPersonalInformationViewModel model, DateTime dateOfBirth);
        bool HasStatement(int visitPayUserId);
    }
}
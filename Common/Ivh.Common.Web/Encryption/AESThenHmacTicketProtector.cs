namespace Ivh.Common.Web.Encryption
{
    using System;
    using System.Security.Cryptography;
    using Base.Utilities.Extensions;
    using Common.Encryption;
    using Microsoft.Owin.Security.DataProtection;

    public class AESThenHmacTicketProtector : IDataProtector
    {
        private readonly string _encryptionCryptKeyBase64;
        private readonly string _encryptionAuthKeyBase64;
        public AESThenHmacTicketProtector(string encryptionCryptKeyBase64, string encryptionAuthKeyBase64)
        {
            this._encryptionCryptKeyBase64 = encryptionCryptKeyBase64;
            this._encryptionAuthKeyBase64 = encryptionAuthKeyBase64;
        }

        public byte[] Protect(byte[] userData)
        {
            return AESThenHMAC.SimpleEncrypt(userData, Convert.FromBase64String(this._encryptionCryptKeyBase64), Convert.FromBase64String(this._encryptionAuthKeyBase64));
        }

        public byte[] Unprotect(byte[] protectedData)
        {
            return AESThenHMAC.SimpleDecrypt(protectedData, Convert.FromBase64String(this._encryptionCryptKeyBase64), Convert.FromBase64String(this._encryptionAuthKeyBase64));
        }
    }
}
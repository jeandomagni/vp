﻿namespace Ivh.Common.Web.Constants
{
    using System.Collections.Generic;
    using System.Globalization;
    using System.Linq;
    using System.Web.Mvc;
    using Ivh.Application.Content.Common.Interfaces;
    using Ivh.Common.Base.Constants;
    using VisitPay.Constants;

    public static class Dates
    {
        public static IList<SelectListItem> MonthsAbbreviated()
        {
            IList<SelectListItem> selectListItems = ToSelectListWithIndex(CultureInfo.CurrentUICulture.DateTimeFormat.AbbreviatedMonthNames);
              
            return selectListItems;
        }
        
        public static IList<SelectListItem> PaymentDueDays
        {
            get
            {
                List<SelectListItem> days = new List<SelectListItem>();

                for (int i = 1; i <= DateConstants.LatestValidPaymentDueDay; i++)
                {
                    days.Add(new SelectListItem { Text = i.ToString(), Value = i.ToString() });
                }

                IContentApplicationService contentApplicationService = DependencyResolver.Current.GetService<IContentApplicationService>();
                string lastDayOfMonthText = contentApplicationService.GetTextRegion(TextRegionConstants.LastDayMonth);
                days.Add(new SelectListItem { Text = lastDayOfMonthText, Value = "31" });

                return days;
            }
        }

        private static IList<SelectListItem> ToSelectListWithIndex(IEnumerable<string> strings)
        {
            return strings
                .Where(p => !string.IsNullOrEmpty(p))
                .Select((item, index) => new SelectListItem
                    {
                        Value = (index + 1).ToString(),
                        Text = item
                    }
                )
                .ToList();
        }
    }
}
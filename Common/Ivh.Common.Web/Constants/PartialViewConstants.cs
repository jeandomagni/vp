﻿namespace Ivh.Common.Web.Constants
{
    public static class PartialViewConstants
    {
        public const string ArrangePaymentReceipt = "~/Views/Payment/_ArrangePaymentReceipt.cshtml";
        public const string OfflineFinancePlanReceipt = "~/Views/Offline/_FinancePlanReceipt.cshtml";
    }
}
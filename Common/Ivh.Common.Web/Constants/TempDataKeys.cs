﻿namespace Ivh.Common.Web.Constants
{
    public static class TempDataKeys
    {
        public const string Username = "Username";
        public const string Password = "Password";
    }
}
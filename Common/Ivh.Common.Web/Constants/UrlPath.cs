﻿namespace Ivh.Common.Web.Constants
{
    public static class UrlPath
    {
        public static string GuarantorResetPassword = "Account/ResetPassword?p=";
        public static string GuarantorForgotPassword = "Account/RequestPassword";
        public static string CreateFinancePlan = "Payment/ArrangePayment?paymentOption=4";
        public static string ManageHouseholdUrl = "Consolidate/ManageHousehold";
        public static string ClientResetPassword = "Account/ResetPassword?p=";
        public static string ClientForgotPassword = "Account/ForgotPassword";
        public static string GuarantorVpccTokenLogin = "Offline/Login?t=";
        public static string GuarantorVpccAccessTokenPhiLogin = "example.com/TBD";
        public static string ClientImagesPath = "/content/client/";
    }
}
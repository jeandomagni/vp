﻿namespace Ivh.Common.Web.Constants
{
    public class HttpHeaders
    {
        public class Request
        {
            public const string XForwardedFor = "X-Forwarded-For";
            public const string Authorization = "Authorization";
        }

        public class Response
        {
            public const string EmulatedUserNotAuthorized = "EmulatedUserNotAuthorized";
            public const string SuppressRedirect = "Suppress-Redirect";
            public const string VpInfo = "VpInfo";
            public const string FrameOptions = "X-Frame-Options";
            public const string ContentSecurityPolicy = "Content-Security-Policy";
            public const string RestrictedCountry = "X-RestrictedCountry";
            public const string StrictTransportSecurity = "Strict-Transport-Security";
            public const string WwwAuthenticate = "WWW-Authenticate";
        }
    }
}
﻿namespace Ivh.Common.Web.Factories
{
    using Interfaces;
    using System.Net.Http;

    public class HttpClientHandlerFactory : IHttpMessageHandlerFactory
    {
        public HttpMessageHandler Create() => new HttpClientHandler();
    }
}

﻿namespace Ivh.Common.Web.Enums
{
    public enum VpButtonClassEnum
    {
        Primary = 0,
        AccentOutline = 1,
        DefaultOutline = 2
    }
}
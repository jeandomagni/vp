﻿namespace Ivh.Common.Web.Filters
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web.Mvc;
    using Base.Utilities.Extensions;
    using Domain.Settings.Interfaces;

    public class RedirectAttribute : ActionFilterAttribute
    {
        private static IDictionary<string, string> _initialRedirects;
        private static IDictionary<int, string> _redirectsFrom;
        private static IDictionary<int, string> _redirectsTo;
        private static readonly bool HasRedirects;

        static RedirectAttribute()
        {
            try
            {
                IApplicationSettingsService svc = DependencyResolver.Current.GetService<IApplicationSettingsService>();
                // svc is always null it seems

                _initialRedirects = svc?.Redirect?.Value?.ToDictionary(x => x.Key.ToLower(), x => x.Value.ToLower());
                HasRedirects = _initialRedirects != null && _initialRedirects.Any();
            }
            catch (Exception)
            {
                _redirectsFrom = new Dictionary<int, string>();
                _redirectsTo = new Dictionary<int, string>();
                HasRedirects = false;
            }
        }

        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            if (HasRedirects && filterContext?.HttpContext?.Request != null && filterContext.HttpContext.Request.Url != null && filterContext.HttpContext.Request.Url.Host.IsNotNullOrEmpty())
            {
                if (_redirectsFrom == null || _redirectsTo == null)
                {
                    _initialRedirects = _initialRedirects.ToDictionary(k => string.Format(k.Key, filterContext.HttpContext.Request.Url.Port), v => string.Format(v.Value, filterContext.HttpContext.Request.Url.Port));
                    if (filterContext.HttpContext.Request.Url.IsDefaultPort)
                    {
                        _initialRedirects = _initialRedirects.ToDictionary(k => k.Key.Replace(":" + filterContext.HttpContext.Request.Url.Port.ToString(), ""), v => v.Value.Replace(":" + filterContext.HttpContext.Request.Url.Port.ToString(), ""));
                    }

                    _redirectsFrom = _initialRedirects.ToDictionary(k => k.Key.GetHashCode(), v => v.Key);
                    _redirectsTo = _initialRedirects.ToDictionary(k => k.Key.GetHashCode(), v => v.Value);
                }

                IList<string> segments = new List<string>();
                segments.Add(HostAndPort(filterContext.HttpContext.Request.Url));
                foreach (string segment in filterContext.HttpContext.Request.Url.Segments)
                {
                    segments.Add(segments[segments.Count - 1] + segment);
                }

                foreach (string segment in segments.Reverse())
                {
                    int redirectHashCode = segment.ToLower().GetHashCode();

                    if (_redirectsFrom.ContainsKey(redirectHashCode))
                    {
                        UriBuilder redirectUri = new UriBuilder(filterContext.HttpContext.Request.Url.AbsoluteUri.ToLower().Replace(_redirectsFrom[redirectHashCode], _redirectsTo[redirectHashCode]));
                        filterContext.HttpContext.Response.RedirectPermanent(redirectUri.Uri.AbsoluteUri);
                    }
                }
            }
            base.OnActionExecuting(filterContext);
        }

        private static string HostAndPort(Uri uri)
        {
            return uri.IsDefaultPort ? uri.Host : uri.Host + ":" + uri.Port;
        }
    }
}

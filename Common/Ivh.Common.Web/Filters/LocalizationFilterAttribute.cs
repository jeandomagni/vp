﻿namespace Ivh.Common.Web.Filters
{
    using Cookies;
    using Ivh.Application.Core.Common.Dtos;
    using Ivh.Application.Core.Common.Interfaces;
    using Ivh.Application.Settings.Common.Dtos;
    using Microsoft.AspNet.Identity;
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.Linq;
    using System.Threading;
    using System.Web.Mvc;
    using Extensions;

    public sealed class LocalizationFilterAttribute : ActionFilterAttribute
    {
        private readonly Func<ClientDto, string> _appLocaleDefault;
        private readonly Func<ClientDto, List<ClientCultureInfoDto>> _appAvailableLocales;

        public LocalizationFilterAttribute(Func<ClientDto, string> appLocaleDefault, Func<ClientDto, List<ClientCultureInfoDto>> appAvailableLocales)
        {
            this._appLocaleDefault = appLocaleDefault;
            this._appAvailableLocales = appAvailableLocales;
        }

        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            base.OnActionExecuting(filterContext);

            if (filterContext.IsAttributeDefined<NoLocalizationAttribute>())
            {
                // short circuit
                return;
            }

            string locale;
            
            /*
             Use cookie locale if we have it, to prevent hitting DB every time. If no cookie, see if the user
             has a locale set and use that - also setting a cookie with that locale so future requests don't
             hit the DB every time. If no cookie and no user locale, check the user's preferred browser
             languages and use the first supported. Finally, if no preferred browser language is supported then
             use the client default.
             */
            ILocalizationCookieFacade localizationCookie = DependencyResolver.Current.GetService<ILocalizationCookieFacade>();
            if (!string.IsNullOrEmpty(localizationCookie.Locale))
            {
                //We have locale in the cookie, use that
                locale = localizationCookie.Locale;
            }
            else
            {
                //No locale set in the cookie, try getting from user
                locale = this.GetUserLocale(filterContext);
                if (string.IsNullOrEmpty(locale))
                {
                    //No locale set for the user, fall back to client default. Client default will get overridden below if we find a user preferred language.
                    IClientApplicationService clientApplicationService = DependencyResolver.Current.GetService<IClientApplicationService>();
                    ClientDto clientDto = clientApplicationService.GetClient();
                    locale = this._appLocaleDefault(clientDto);

                    //Check if any of the user's preferred browser languages are supported. If so, use the first one in their list.
                    string[] userBroswerLanguages = filterContext.HttpContext.Request.UserLanguages;
                    if (userBroswerLanguages != null)
                    {
                        List<string> availableLocales = this._appAvailableLocales(clientDto).Select(l => l.Name).ToList();
                        //Loop through each language the user prefers and see if we support it. Take the first supported.
                        foreach (string userPreferredLanguage in userBroswerLanguages)
                        {
                            //Only match on the first 2 letters of the locale, since there are many variations available coming from the browser (i.e. es, es-CR, es-CO...)
                            string matchingPreferred = availableLocales.FirstOrDefault(l => userPreferredLanguage.ToLower().StartsWith(l.Substring(0, 2).ToLower()));
                            if (!string.IsNullOrEmpty(matchingPreferred))
                            {
                                //Found a preferred language in the supported languages. Use that instead of the client default.
                                locale = matchingPreferred;
                                break;
                            }
                        }
                    }
                }

                //Set locale on cookie for future requests
                localizationCookie.SetLocale(locale);
            }
            
            //Set locale on the thread
            if (!string.IsNullOrEmpty(locale))
            {
                //Got a locale, set it on the thread
                CultureInfo culture = new CultureInfo(locale);
                Thread.CurrentThread.CurrentUICulture = culture;
            }
        }
        
        private string GetUserLocale(ActionExecutingContext filterContext)
        {
            string userLocale = null;

            if (filterContext.HttpContext.User != null && filterContext.HttpContext.User.Identity.IsAuthenticated)
            {
                //User is logged in, load locale from VP User Application Service
                IVisitPayUserApplicationService visitPayUserApplicationService = DependencyResolver.Current.GetService<IVisitPayUserApplicationService>();
                string userId = filterContext.HttpContext.User.Identity.GetUserId();
                userLocale = visitPayUserApplicationService.GetUserLocale(userId);
            }

            return userLocale;
        }
    }
}

﻿namespace Ivh.Common.Web.Filters
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web.Mvc;
    using MassTransit;
    using VisitPay.Messages.Monitoring;

    public sealed class ControllerTimingAttribute : ActionFilterAttribute
    {
        private const string TimingKey = "Timings";

        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            try
            {
                AddTiming(filterContext, new ControllerTimingMessage
                {
                    Name = GetName(filterContext),
                    TimeActionExecuting = DateTime.UtcNow
                });
            }
            catch
            {
                //
            }

            base.OnActionExecuting(filterContext);
        }

        public override void OnActionExecuted(ActionExecutedContext filterContext)
        {
            try
            {
                GetTiming(filterContext).TimeActionExecuted = DateTime.UtcNow;
            }
            catch
            {
                //
            }

            base.OnActionExecuted(filterContext);
        }

        public override void OnResultExecuted(ResultExecutedContext filterContext)
        {
            try
            {
                GetTiming(filterContext).TimeResultExecuted = DateTime.UtcNow;

                if (!filterContext.IsChildAction)
                    Publish(filterContext);
            }
            catch
            {
                //
            }

            base.OnResultExecuted(filterContext);
        }

        private static string GetName(ControllerContext context)
        {
            return $"{context.HttpContext.Request.RequestType}::{context.Controller}/{context.RequestContext.RouteData.Values["action"]}";
        }

        private static void AddTiming(ControllerContext context, ControllerTimingMessage timing)
        {
            context.HttpContext.Items[TimingKey] = context.HttpContext.Items[TimingKey] ?? new List<ControllerTimingMessage>();
            ((List<ControllerTimingMessage>) context.HttpContext.Items[TimingKey]).Add(timing);
        }

        private static ControllerTimingMessage GetTiming(ControllerContext context)
        {
            return ((List<ControllerTimingMessage>) context.HttpContext.Items[TimingKey]).First(x => x.Name == GetName(context));
        }

        private static void Publish(ControllerContext context)
        {
            try
            {
                ControllerTimingMessages timings = new ControllerTimingMessages
                {
                    Messages = ((List<ControllerTimingMessage>) context.HttpContext.Items[TimingKey]).Select(x => new ControllerTimingMessage
                    {
                        Name = x.Name,
                        TimeActionExecuting = x.TimeActionExecuting,
                        TimeActionExecuted = x.TimeActionExecuted,
                        TimeResultExecuted = x.TimeResultExecuted
                    }).ToList()
                };

                context.HttpContext.Items[TimingKey] = null;

                IBus bus = DependencyResolver.Current.GetService<IBus>();
                bus.Publish(timings);
            }
            catch
            {
                //
            }
        }
    }
}
﻿﻿namespace Ivh.Common.Web.Filters
{
    using System.Globalization;
    using System.Web.Mvc;
    using Constants;
    using Ivh.Application.Core.Common.Dtos;
    using Ivh.Application.Core.Common.Interfaces;

    public class ContentSecurityPolicyFilterAttribute : ActionFilterAttribute
    {
        private readonly bool _applyPolicy = true;
        private static string _clientUrl = null;
        private static string _frameAncestors = null;
        private static readonly object Lock = new object();

        public ContentSecurityPolicyFilterAttribute()
        {

        }

        public ContentSecurityPolicyFilterAttribute(bool applyPolicy)
        {
            this._applyPolicy = applyPolicy;
        }

        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            if (this._applyPolicy)
            {
                if (_clientUrl == null)
                {
                    lock (Lock)
                    {
                        if (_clientUrl == null)
                        {
                            IClientApplicationService clientAppSvc = DependencyResolver.Current.GetService<IClientApplicationService>();
                            ClientDto clientDto = clientAppSvc.GetClient();
                            if (clientDto != null)
                            {
                                _clientUrl = clientDto.AppClientUrlHost.ToLowerInvariant().TrimEnd('/');
                                _frameAncestors = $"frame-ancestors {_clientUrl};";
                            }
                        }
                    }
                }

                if (_clientUrl != null && _frameAncestors != null)
                {


                    string xfoValue = filterContext.HttpContext.Response.Headers.Get(HttpHeaders.Response.FrameOptions);
                    string cspValue = filterContext.HttpContext.Response.Headers.Get(HttpHeaders.Response.ContentSecurityPolicy);

                    //https://ivinci.atlassian.net/browse/VPNG-12222
                    //http://stackoverflow.com/questions/10658435/x-frame-options-allow-from-in-firefox-and-chrome
                    //Chrome/Firefox/Safari do not allow the Allow-From tag, we would need that to not break emulation.
                    //IE on the other hand does.  This is why we only have browsers that start with I
                    if (string.IsNullOrEmpty(xfoValue) && filterContext.RequestContext.HttpContext.Request.Browser.Browser.StartsWith("I", true, CultureInfo.InvariantCulture))
                    {
                        filterContext.HttpContext.Response.Headers.Set(HttpHeaders.Response.FrameOptions, _clientUrl);
                    }

                    if (string.IsNullOrEmpty(cspValue))
                    {
                        filterContext.HttpContext.Response.Headers.Set(HttpHeaders.Response.ContentSecurityPolicy, $"frame-ancestors {_clientUrl};");
                    }

                    //filterContext.HttpContext.Response.Headers.Set(HttpHeaders.Response.ContentSecurityPolicy, "frame-ancestors 'self'");
                    //filterContext.HttpContext.Response.Headers.Set(HttpHeaders.Response.FrameOptions, "SAMEORIGIN");
                }

            }

            base.OnActionExecuting(filterContext);
        }
    }
}
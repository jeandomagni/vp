﻿namespace Ivh.Common.Web.Filters
{
    using System.Linq;
    using System.Web;
    using System.Web.Mvc;
    using Ivh.Application.Core.Common.Interfaces;
    using Autofac;
    using DependencyInjection;
    using Extensions;

    public sealed class IpAccessCheckAttribute : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            //this limits how we can use autofac.  Since this gets cached by the application before an actual request causes autofac to try to resolve this with the root scope, which fails...
            //Apparently you can use a trick:  ~actionContext.Request.GetDependencyScope().GetService(typeof(IOurService)
            //Clearly didnt implement this.  Using autofac to just inject a new session into this repository specificly.
            //http://stackoverflow.com/questions/23659108/webapi-autofac-system-web-http-filters-actionfilterattribute-instance-per-requ
            if (HttpContext.Current != null)
            {
                IIpAccessApplicationService ipAccessApplicationService = IvinciContainer.Instance.Container().Resolve<IIpAccessApplicationService>();
                bool allowed = HttpContext.Current.Request.ForwardedFor().Reverse().Any(x => ipAccessApplicationService.IsIpAddressAllowed(x.ToString()));
                if (!allowed)
                {
                    filterContext.Result = new HttpStatusCodeResult(403);
                }
            }

            base.OnActionExecuting(filterContext);
        }
    }
}
﻿namespace Ivh.Common.Web.Filters
{
    using System;
    using System.Linq;
    using System.Security.Claims;
    using System.Web.Mvc;
    using Base.Constants;
    using Base.Enums;
    using Constants;
    using VisitPay.Constants;
    using VisitPay.Enums;

    public sealed class EmulateUserAttribute : AuthorizeAttribute
    {
        public EmulateUserAttribute()
        {
            this.AllowGet = true;
            this.AllowPost = false;
            this.AllowPut = false;
            this.AllowDelete = false;
        }

        public bool AllowPost { get; set; }
        public bool AllowPut { get; set; }
        public bool AllowDelete { get; set; }
        public bool AllowGet { get; set; }
        //protected override bool AuthorizeCore(HttpContextBase httpContext)
        //{
        //    if (httpContext.User == null ||
        //        httpContext.User.Identity == null ||
        //        (httpContext.User.Identity is ClaimsIdentity && !((ClaimsIdentity) httpContext.User.Identity).HasClaim(c => c.Type == ClaimTypeEnum.EmulatedUser)))
        //    {
        //        return base.AuthorizeCore(httpContext);
        //    }
        //    httpContext.Items[ClaimTypeEnum.ClientUsername] = ((ClaimsIdentity) httpContext.User.Identity).Claims.Where(c => c.Type == ClaimTypeEnum.ClientUsername).Select(c => c.Value).FirstOrDefault();
        //    switch (httpContext.Request.HttpMethod)
        //    {
        //        case "GET":
        //            return this.AllowGet;
        //        case "POST":
        //            return this.AllowPost;
        //        case "PUT":
        //            return this.AllowPut;
        //        case "DELETE":
        //            return this.AllowDelete;
        //        default:
        //            return false;
        //    }
        //}

        public override void OnAuthorization(AuthorizationContext filterContext)
        {
            base.OnAuthorization(filterContext);
            if (filterContext.HttpContext.User != null &&
                filterContext.HttpContext.User.Identity != null &&
                (filterContext.HttpContext.User.Identity is ClaimsIdentity && ((ClaimsIdentity) filterContext.HttpContext.User.Identity).HasClaim(c => c.Type.Equals(ClaimTypeEnum.EmulatedUser.ToString(), StringComparison.OrdinalIgnoreCase ))))
            {
                filterContext.HttpContext.Items[ClaimTypeEnum.ClientUsername] = ((ClaimsIdentity) filterContext.HttpContext.User.Identity).Claims.Where(c => c.Type.Equals(ClaimTypeEnum.ClientUsername.ToString(), StringComparison.OrdinalIgnoreCase)).Select(c => c.Value).FirstOrDefault();
                bool authorized;
                switch (filterContext.HttpContext.Request.HttpMethod)
                {
                    case "GET":
                        authorized = this.AllowGet;
                        break;
                    case "POST":
                        authorized = this.AllowPost;
                        break;
                    case "PUT":
                        authorized = this.AllowPut;
                        break;
                    case "DELETE":
                        authorized = this.AllowDelete;
                        break;
                    default:
                        authorized = false;
                        break;
                }
                if (!authorized)
                {
                    /*
                        Bug VP-2575 Emulate user view in the client portal error message is missing text
                        https://tools.ietf.org/html/rfc7540#section-8.1.2.4
                        HTTP / 2 does not define a way to carry the version or reason phrase
                        that is included in an HTTP/ 1.1 status line.
                    */
                    filterContext.Result = new HttpUnauthorizedResult();//still need to trigger a 401, however, the message will not be passed with http/2
                    filterContext.HttpContext.Response.AddHeader(HttpHeaders.Response.EmulatedUserNotAuthorized, TextRegionConstants.EmulatedUserNotAuthorized);
                    filterContext.Controller.ViewData.ModelState.AddModelError("", TextRegionConstants.EmulatedUserNotAuthorized);
                }
            }
        }
    }
}
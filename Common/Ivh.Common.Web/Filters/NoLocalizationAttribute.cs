﻿namespace Ivh.Common.Web.Filters
{
    using System.Globalization;
    using System.Threading;
    using System.Web.Mvc;
    using Cookies;
    using Ivh.Application.Core.Common.Dtos;
    using Ivh.Application.Core.Common.Interfaces;

    public sealed class NoLocalizationAttribute : ActionFilterAttribute
    {
        private static string _defaultLocale = string.Empty;
        
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            base.OnActionExecuting(filterContext);

            if (string.IsNullOrWhiteSpace(_defaultLocale))
            {
                IClientApplicationService clientApplicationService = DependencyResolver.Current.GetService<IClientApplicationService>();
                ClientDto clientDto = clientApplicationService.GetClient();
                _defaultLocale = clientDto.LanguageAppDefaultName;
            }

            ILocalizationCookieFacade localizationCookie = DependencyResolver.Current.GetService<ILocalizationCookieFacade>();
            if ((localizationCookie.Locale ?? string.Empty) != _defaultLocale)
            {
                localizationCookie.SetLocale(_defaultLocale);
            }

            if (!string.IsNullOrWhiteSpace(_defaultLocale))
            {
                CultureInfo culture = new CultureInfo(_defaultLocale);
                Thread.CurrentThread.CurrentUICulture = culture;
            }
        }
    }
}
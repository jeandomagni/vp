﻿namespace Ivh.Common.Web.Filters
{
    using System.Web;
    using System.Web.Mvc;
    using Ivh.Application.Core.Common.Interfaces;

    public class ForceLogoutFilterAttribute : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            if (filterContext.Controller.ControllerContext.IsChildAction)
            {
                // if it's a child action, the parent just checked.
                return;
            }

            IVisitPayUserApplicationLightweightService visitPayUserApplicationLightweightService = DependencyResolver.Current.GetService<IVisitPayUserApplicationLightweightService>();
            visitPayUserApplicationLightweightService.CheckIfVisitPayUserShouldSignOut(filterContext.HttpContext.GetOwinContext());
        }
    }
}
﻿namespace Ivh.Common.Web.Filters
{
    using System.Web.Mvc;
    using Base.Utilities.Helpers;
    using Constants;

    public class TransportSecurityPolicyFilterAttribute : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            if (!filterContext.IsChildAction)
            {
                if (!IvhEnvironment.IsDevelopment)
                {

                    //Strict-Transport-Security: max-age=31536000;
                    //For a year.
                    const string value = "max-age=31536000";
                    filterContext.HttpContext.Response.Headers.Set(HttpHeaders.Response.StrictTransportSecurity, value);
                }
            }
            base.OnActionExecuting(filterContext);
        }
    }
}
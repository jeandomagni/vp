﻿namespace Ivh.Common.Web.Filters
{
    using System;
    using System.Web.Mvc;
    using Base.Logging;

    public class LoggingCorrelationAttribute : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            CorrelationManager.Initialize(Guid.NewGuid(), filterContext.HttpContext.Session.SessionID);
            base.OnActionExecuting(filterContext);
        }
    }
}
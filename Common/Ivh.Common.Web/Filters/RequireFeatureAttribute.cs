﻿namespace Ivh.Common.Web.Filters
{
    using System.Linq;
    using System.Web.Mvc;
    using Ivh.Application.Core.Common.Interfaces;
    using Base.Enums;
    using VisitPay.Enums;

    public class RequireFeatureAttribute : ActionFilterAttribute
    {
        private readonly VisitPayFeatureEnum[] _features;

        public RequireFeatureAttribute(params VisitPayFeatureEnum[] features)
        {
            this._features = features;
        }

        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            if (this._features == null || !this._features.Any())
            {
                return;
            }

            IFeatureApplicationService featureApplicationService = DependencyResolver.Current.GetService<IFeatureApplicationService>();

            if (this._features.All(feature => featureApplicationService.IsFeatureEnabled(feature)))
            {
                base.OnActionExecuting(filterContext);
                return;
            }

            filterContext.Result = new HttpStatusCodeResult(404);
        }
    }
}
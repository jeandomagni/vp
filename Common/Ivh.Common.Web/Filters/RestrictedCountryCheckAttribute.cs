﻿﻿namespace Ivh.Common.Web.Filters
{
    using System;
    using System.Linq;
    using System.Web;
    using System.Web.Mvc;
    using System.Web.Routing;
    using Ivh.Application.Core.Common.Interfaces;
    using Ivh.Application.Logging.Common.Interfaces;
    using Base.Enums;
    using Base.Utilities.Helpers;
    using Constants;
    using Extensions;
    using VisitPay.Enums;

    public sealed class RestrictedCountryCheckAttribute : ActionFilterAttribute
    {
        private readonly ApplicationEnum _application;

        public RestrictedCountryCheckAttribute(ApplicationEnum application) : base()
        {
            this._application = application;
        }
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            string requestUserHostAddressString = null;
            bool? isIpRestrictedByCountry = null;
            if (HttpContext.Current != null 
                && !filterContext.ActionDescriptor.GetCustomAttributes(typeof(SkipRestrictedCountryCheckAttribute), false).Any())
            {
                try
                {
                    if (!HttpContext.Current.Request.IsLocal)
                    {
                        IRestrictedCountryAccessApplicationService restrictedCountryAccessApplicationService = DependencyResolver.Current.GetService<IRestrictedCountryAccessApplicationService>();

                        //Double-if makes it easier to debug, just drag down a line.
                        requestUserHostAddressString = HttpContext.Current.Request.RequestUserHostAddressString();
                        isIpRestrictedByCountry = restrictedCountryAccessApplicationService.IsIpRestrictedByCountry(requestUserHostAddressString);
                        if (isIpRestrictedByCountry.Value)
                        {
                            if (this._application == ApplicationEnum.VisitPay 
                                || this._application == ApplicationEnum.GuestPay)
                            {
                                filterContext.Result = new RedirectToRouteResult(
                                    new RouteValueDictionary
                                    {
                                        {"action", "ContactUs"},
                                        {"controller", "Account"},
                                        {"countryRestricted", "true"}
                                    });
                            }
                            else
                            {
                                if (!filterContext.HttpContext.Response.HeadersWritten)
                                {
                                    filterContext.HttpContext.Response.Headers.Set(HttpHeaders.Response.RestrictedCountry, "true");
                                }

                                filterContext.Result = new HttpStatusCodeResult(403);
                            }
                        }
                    }
                }
                catch(Exception e)
                {
                    //Ignore all errors here
                    ILoggingApplicationService loggingApplicationService = DependencyResolver.Current.GetService<ILoggingApplicationService>();
                    loggingApplicationService.Fatal(() => string.Format("RestrictedCountryCheckAttribute::OnActionExecuting - App: {0}, IP: {1}, isIpRestrictedByCountry: {2}, Exception: {3}",this._application,requestUserHostAddressString,isIpRestrictedByCountry, ExceptionHelper.AggregateExceptionToString(e)));
                }
            }

            base.OnActionExecuting(filterContext);
        }
    }
}
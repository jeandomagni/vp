﻿namespace Ivh.Common.Web.Views
{
    using System;
    using System.Linq;
    using System.Web;
    using System.Web.Mvc;
    using Ivh.Application.Core.Common.Dtos;
    using Base.Enums;
    using Base.Utilities.Helpers;
    using Controllers;
    using Models.Shared;
    using VisitPay.Enums;

    public class BaseViewPage<T> : WebViewPage<T>
    {
        private BaseWebController Controller { get; set; }

        protected string ThemeStylesheet { get; set; }

        protected AnaltyicsDto AnalyticsDto => this.Controller.AnalyticsDto;
        protected ClientDto ClientDto => this.Controller.ClientDto;
        protected TimeZoneHelper TimeZoneHelper => this.Controller.TimeZoneHelper;
        protected CurrentUserViewModel CurrentUser => this.Controller.CurrentUser;

        protected bool IsBrowserSupported => this.Controller.IsBrowserSupported;
        protected string BrowserNameString => this.Controller.BrowserNameString;
        protected bool IsBrowserInternetExplorer => this.Controller.IsBrowserInternetExplorer;
        protected bool IsBrowserSafari => this.Controller.IsBrowserSafari;

        public DateTime BuildDateTime => this.IsBuildBannerVisible ? AssemblyHelper.BuildDate : default(DateTime);
        public string BuildVersion => this.IsBuildBannerVisible ? AssemblyHelper.BuildVersion : null;

        const string HideBuildBannerKey = "HideBuildBanner";
        public bool IsBuildBannerVisible => this.Controller.ShowBuildBanner && (this.Session[HideBuildBannerKey] == null || !Convert.ToBoolean(this.Session[HideBuildBannerKey]));

        protected string ThemeStylesheetHash
        {
            get
            {
                BaseWebController controller = this.ViewContext.Controller as BaseWebController;
                return controller != null ? controller.ThemeStylesheetHash(this.ThemeStylesheet) : string.Empty;
            }
        }

        protected string ThemeStylesheetRaw
        {
            get
            {
                BaseWebController controller = this.ViewContext.Controller as BaseWebController;
                return controller != null ? controller.ThemeStylesheetRaw(this.ThemeStylesheet) : string.Empty;
            }
        }


        protected override void InitializePage()
        {
            base.InitializePage();

            BaseWebController controller = this.ViewContext.Controller as BaseWebController;
            if (controller == null)
            {
                return;
            }

            this.Controller = controller;
        }

        public override void Execute()
        {
        }

        protected bool IsFeatureEnabled(params VisitPayFeatureEnum[] features)
        {
            BaseWebController controller = this.ViewContext.Controller as BaseWebController;
            return controller == null || features.Any(x => controller.IsFeatureEnabled(x));
        }

        protected RandomizedTestGroupEnum? GetRandomizedTestGroup(RandomizedTestEnum randomizedTestEnum)
        {
            BaseWebController controller = this.ViewContext.Controller as BaseWebController;
            return controller?.GetRandomizedTestGroup(randomizedTestEnum);
        }

        protected bool IsCurrentUserEmulating()
        {
            BaseWebController controller = this.ViewContext.Controller as BaseWebController;
            return controller != null && controller.IsCurrentUserEmulating();
        }

        protected string ServerUrl => HttpContext.Current.Request.Url.AbsoluteUri.Replace(HttpContext.Current.Request.Url.PathAndQuery, "");

        protected string GuarantorUrlDisplay => FormatHelper.DisplayUrl(this.ClientDto.AppGuarantorUrlHost);

    }

    public class BaseViewPage : BaseViewPage<dynamic>
    {
    }
}
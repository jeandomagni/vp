﻿namespace Ivh.Common.Web.Views
{
    using System.Web;
    using System.Web.Mvc;
    using System.Collections.Generic;

    public class TwoLevelViewCache : IViewLocationCache
    {
        private static readonly object SKey = new object();
        private readonly IViewLocationCache _cache;

        public TwoLevelViewCache(IViewLocationCache cache)
        {
            _cache = cache;
        }

        private static IDictionary<string, string> GetRequestCache(HttpContextBase httpContext)
        {
            if (!(httpContext.Items[SKey] is IDictionary<string, string> d))
            {
                d = new Dictionary<string, string>();
                httpContext.Items[SKey] = d;
            }
            return d;
        }

        public string GetViewLocation(HttpContextBase httpContext, string key)
        {
            IDictionary<string, string> d = GetRequestCache(httpContext);
            string location;
            if (!d.TryGetValue(key, out location))
            {
                location = this._cache.GetViewLocation(httpContext, key);
                d[key] = location;
            }
            return location;
        }

        public void InsertViewLocation(HttpContextBase httpContext, string key, string virtualPath)
        {
            this._cache.InsertViewLocation(httpContext, key, virtualPath);
        }
    }
}

﻿namespace Ivh.Common.Api.Controllers
{
    using System;
    using System.Web.Http;
    using Autofac.Integration.WebApi;
    using Base.Utilities.Extensions;
    using Filters;

    [AutofacControllerConfiguration]
    public class BaseApiController : ApiController
    {
        protected Lazy<string> ApplicationName { get; private set; }
        protected Lazy<string> RequestUri { get; private set; }

        protected BaseApiController()
        {
        }

        [HmacAuthentication]
        [HttpGet]
        public virtual IHttpActionResult GetAssemblyVersion()
        {
            return this.Ok($"{this.GetType().Assembly.GetAssemblyVersionAndDate()}");
        }
    }
}

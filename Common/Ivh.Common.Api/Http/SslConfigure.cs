﻿namespace Ivh.Common.Api.Http
{
    using System.Net;

    public class SslConfigure
    {
        public static void Configure()
        {
            System.Net.ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls;

        }
    }
}

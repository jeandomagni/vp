﻿namespace Ivh.Common.Api.Client
{
    using System.Net.Http;
    using System.Security.Claims;
    using System.Threading;
    using System.Threading.Tasks;
    using System.Web;
    using Base.Utilities.Extensions;

    public class SimpleApiKeyHandler : DelegatingHandler
    {
        private const string UserName = "Authorization";
        private readonly string _apiKeyValue = null;

        public SimpleApiKeyHandler(string apiKeyValue)
        {
            this._apiKeyValue = apiKeyValue;
        }

        protected override Task<HttpResponseMessage> SendAsync(HttpRequestMessage request, CancellationToken cancellationToken)
        {
            if (request.Headers?.Authorization != null)
            {
                string apiKey = request.Headers.Authorization.Parameter.IsNotNullOrEmpty() ? request.Headers?.Authorization.Parameter
                    : request.Headers.Authorization.Scheme.IsNotNullOrEmpty() ? request.Headers?.Authorization.Scheme
                    : string.Empty ?? string.Empty;

                if (apiKey.IsNotNullOrEmpty() 
                    && this._apiKeyValue.IsNotNullOrEmpty() 
                    && apiKey.Equals(this._apiKeyValue))
                {
                    Claim usernameClaim = new Claim(ClaimTypes.Name, UserName);
                    ClaimsIdentity identity = new ClaimsIdentity(new[] { usernameClaim }, UserName);
                    ClaimsPrincipal principal = new ClaimsPrincipal(identity);

                    Thread.CurrentPrincipal = principal;
                    if (HttpContext.Current != null)
                    {
                        HttpContext.Current.User = principal;
                    }
                }
            }

            return base.SendAsync(request, cancellationToken);
        }
    }
}

﻿namespace Ivh.Common.Api.Filters
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.SqlClient;
    using System.Net.Http;
    using System.Net.Http.Headers;
    using System.Security.Claims;
    using System.Security.Cryptography;
    using System.Security.Principal;
    using System.Text;
    using System.Threading;
    using System.Threading.Tasks;
    using System.Web;
    using System.Web.Http.Filters;
    using System.Web.Http.Results;
    using Application.Enterprise.Common.Api.Dtos;
    using Base.Utilities.Extensions;
    using Base.Utilities.Helpers;
    using Configuration;
    using Http;
    using Data.Services;
    using VisitPay.Enums;

    /// <summary>
    /// http://bitoftech.net/2014/12/15/secure-asp-net-web-api-using-api-key-authentication-hmac-authentication/
    /// </summary>
    public class HmacAuthenticationAttribute : Attribute, IAuthenticationFilter
    {
        private static IDictionary<string, ApplicationRegistrationDto> _allowedApps = new Dictionary<string, ApplicationRegistrationDto>();
        private static ApiEnum? _api = null;
        private const ulong RequestMaxAgeInSeconds = 300; //5 mins
        private static bool? _requireAuthentication = null;
        private static readonly object ObjectLock = new object();
        private IWebHelper _webHelper = new WebHelper();

        public Task AuthenticateAsync(HttpAuthenticationContext context, CancellationToken cancellationToken)
        {
            this.SetApiEnum();
            if (!_requireAuthentication.HasValue)
            {
                lock (ObjectLock)
                {
                    _requireAuthentication = true;
                    bool requireAuthenticationTemp = true;

                    const string apiRequireAuthenticationKey = "Api.RequireAuthentication";
                    string apiRequireAuthNSetting = AppSettingsProvider.Instance.Get(apiRequireAuthenticationKey);

                    if (apiRequireAuthNSetting.IsNotNullOrEmpty()
                        && bool.TryParse(apiRequireAuthNSetting, out requireAuthenticationTemp))
                    {
                        _requireAuthentication = requireAuthenticationTemp;
                    }
                }
            };

            if (!_requireAuthentication.Value) return Task.FromResult(0);
            HttpRequestMessage httpRequestMessage = context.Request;

            if (httpRequestMessage.Headers.Authorization != null
                && Common.AuthenticationScheme.Equals(httpRequestMessage.Headers.Authorization.Scheme, StringComparison.OrdinalIgnoreCase))
            {
                string rawAuthorizationHeader = httpRequestMessage.Headers.Authorization.Parameter;
                string[] authorizationHeaders = this.GetAuthorizationHeaderValues(rawAuthorizationHeader);
                if (authorizationHeaders != null)
                {
                    string appId = authorizationHeaders[0];
                    string incomingBase64Signature = authorizationHeaders[1];
                    string nonce = authorizationHeaders[2];
                    string requestTimeStamp = authorizationHeaders[3];
                    if (this.IsValidRequest(httpRequestMessage, appId, incomingBase64Signature, nonce, requestTimeStamp))
                    {
                        GenericPrincipal currentPrincipal = new GenericPrincipal(new GenericIdentity(appId), null);
                        ClaimsIdentity claimsIdentity = currentPrincipal.Identity as ClaimsIdentity;
                        if (claimsIdentity != null)
                        {
                            claimsIdentity.AddClaim(new Claim(Common.Claim.ApplicationRegistrationName, _allowedApps[appId].ApplicationName));
                        }
                        context.Principal = currentPrincipal;
                    }
                    else
                    {
                        context.ErrorResult = new UnauthorizedResult(new AuthenticationHeaderValue[0], context.Request);
                    }
                }
                else
                {
                    context.ErrorResult = new UnauthorizedResult(new AuthenticationHeaderValue[0], context.Request);
                }
            }
            else
            {
                context.ErrorResult = new UnauthorizedResult(new AuthenticationHeaderValue[0], context.Request);
            }
            return Task.FromResult(0);
        }

        public Task ChallengeAsync(HttpAuthenticationChallengeContext context, CancellationToken cancellationToken)
        {
            context.Result = new ResultWithChallenge(context.Result);
            return Task.FromResult(0);
        }

        public bool AllowMultiple
        {
            get { return false; }
        }

        private ApplicationRegistrationDto GetAppKey(string appId)
        {
            if (_allowedApps == null)
            {
                lock (ObjectLock)
                {
                    if (_allowedApps == null)
                    {
                        _allowedApps = new Dictionary<string, ApplicationRegistrationDto>(StringComparer.OrdinalIgnoreCase);
                    }
                }
            }

            this.SetApiEnum();

            if (!_allowedApps.ContainsKey(appId) && _api.GetValueOrDefault(ApiEnum.Unknown) != ApiEnum.Unknown)
            {
                lock (ObjectLock)
                {
                    if (!_allowedApps.ContainsKey(appId) && _api.GetValueOrDefault(ApiEnum.Unknown) != ApiEnum.Unknown)
                    {
                        using (SqlConnection sqlConnection = new SqlConnection(ConnectionStringService.Instance.EnterpriseConnection.Value))
                        {
                            try
                            {
                                using (SqlCommand sqlCommand = new SqlCommand(" SELECT [ApplicationRegistrationId],[InsertDate],[ApiId],[ApplicationName],[AppId],[AppKey],[Disabled],[ValidFrom],[ValidUntil]" +
                                                                              " FROM [api].[ApplicationRegistration]" +
                                                                              " WHERE AppId = @appId AND ApiId = @apiId",
                                    sqlConnection))
                                {
                                    sqlCommand.Parameters.Add(new SqlParameter("@appId", appId));
                                    sqlCommand.Parameters.Add(new SqlParameter("@apiId", (int)_api.Value));

                                    DataTable dataTable = new DataTable();
                                    sqlConnection.Open();
                                    dataTable.Load(sqlCommand.ExecuteReader());
                                    if (dataTable.Rows.Count > 0)
                                    {
                                        ApplicationRegistrationDto applicationRegistrationDto = new ApplicationRegistrationDto()
                                        {
                                            ApplicationRegistrationId = (int)dataTable.Rows[0]["ApplicationRegistrationId"],
                                            InsertDate = (DateTime)dataTable.Rows[0]["InsertDate"],
                                            Api = (ApiEnum)(int)dataTable.Rows[0]["ApiId"],
                                            ApplicationName = (string)dataTable.Rows[0]["ApplicationName"],
                                            AppId = (string)dataTable.Rows[0]["AppId"],
                                            AppKey = (string)dataTable.Rows[0]["AppKey"],
                                            Disabled = (bool)dataTable.Rows[0]["Disabled"],
                                            ValidFrom = (DateTime)dataTable.Rows[0]["ValidFrom"],
                                            ValidUntil = (DateTime)dataTable.Rows[0]["ValidUntil"],
                                        };
                                        _allowedApps[appId] = applicationRegistrationDto;
                                    }
                                }
                            }
                            finally
                            {
                                sqlConnection.Close();
                            }
                        }
                    }
                }
            }

            return _allowedApps[appId];
        }

        private void SetApiEnum()
        {
            if (_api.HasValue) return;
            lock (ObjectLock)
            {
                if (this._webHelper.IsWebApplication)
                {
                    ApiEnum api = ApiEnum.Unknown;
                    if (Enum.TryParse(Ivh.Common.Properties.Settings.Default.Api, out api))
                    {
                        _api = api;
                    }
                }
            }
        }

        private string[] GetAuthorizationHeaderValues(string rawAuthorizationHeader)
        {
            string[] credentialsArray = rawAuthorizationHeader.Split(':');

            return credentialsArray.Length == 4 ? credentialsArray : null;
        }

        private bool IsValidRequest(HttpRequestMessage req, string appId, string incomingBase64Signature, string nonce, string requestTimeStamp)
        {
            string requestContentBase64String = string.Empty;
            string requestUri = HttpUtility.UrlEncode(req.RequestUri.AbsoluteUri.ToLower());
            string requestHttpMethod = req.Method.Method;

            ApplicationRegistrationDto applicationRegistrationDto = this.GetAppKey(appId);
            if (applicationRegistrationDto == null || !applicationRegistrationDto.IsValid || applicationRegistrationDto.AppKey.IsNullOrEmpty())
            {
                return false;
            }

            if (this.IsReplayRequest(nonce, requestTimeStamp))
            {
                return false;
            }

            byte[] hash = this.ComputeHash(req.Content);

            if (hash != null)
            {
                requestContentBase64String = Convert.ToBase64String(hash);
            }

            string data = string.Format("{0}{1}{2}{3}{4}{5}", appId, requestHttpMethod, requestUri, requestTimeStamp, nonce, requestContentBase64String);

            byte[] signature = Encoding.UTF8.GetBytes(data);

            using (HMACSHA256 hmac = new HMACSHA256(applicationRegistrationDto.AppKeyBytes))
            {
                byte[] signatureBytes = hmac.ComputeHash(signature);

                return (incomingBase64Signature.Equals(Convert.ToBase64String(signatureBytes), StringComparison.Ordinal));
            }

        }

        private bool IsReplayRequest(string nonce, string requestTimeStamp)
        {
            if (System.Runtime.Caching.MemoryCache.Default.Contains(nonce))
            {
                return true;
            }

            DateTime epochStart = new DateTime(1970, 01, 01, 0, 0, 0, 0, DateTimeKind.Utc);
            TimeSpan currentTs = DateTime.UtcNow - epochStart;

            ulong serverTotalSeconds = Convert.ToUInt64(currentTs.TotalSeconds);
            ulong requestTotalSeconds = Convert.ToUInt64(requestTimeStamp);

            if ((serverTotalSeconds - requestTotalSeconds) > RequestMaxAgeInSeconds)
            {
                return true;
            }

            System.Runtime.Caching.MemoryCache.Default.Add(nonce, requestTimeStamp, DateTime.UtcNow.AddSeconds(RequestMaxAgeInSeconds));

            return false;
        }

        private byte[] ComputeHash(HttpContent httpContent)
        {
            using (SHA256 sha256 = SHA256.Create())
            {
                byte[] hash = null;
                byte[] content = httpContent.ReadAsByteArrayAsync().Result;
                if (content.Length != 0)
                {
                    hash = sha256.ComputeHash(content);
                }
                return hash;
            }
        }
    }
}
﻿namespace Ivh.Common.Api.Filters
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Security.Claims;
    using System.Web.Http.Dependencies;
    using System.Web.Http.Filters;
    using Application.Logging.Common.Interfaces;
    using Domain.Logging.Interfaces;

    public class LogActionAttribute : ActionFilterAttribute
    {
        public override void OnActionExecuted(HttpActionExecutedContext actionExecutedContext)
        {
            base.OnActionExecuted(actionExecutedContext);

            string applicationRegistration = "[anonymous]";
            var requestContext = actionExecutedContext.ActionContext.RequestContext;
            if (requestContext.Principal != null)
            {
                ClaimsIdentity claimsIdentity = requestContext.Principal.Identity as ClaimsIdentity;
                if (claimsIdentity != null)
                {
                    if (claimsIdentity.HasClaim((x) => x.Type == Common.Claim.ApplicationRegistrationName))
                    {
                        Claim claim = claimsIdentity.FindFirst((x) => x.Type == Common.Claim.ApplicationRegistrationName);
                        if (claim != null)
                        {
                            applicationRegistration = claim.Value;
                        }
                    }
                }
            }

            string requestingApplicationName = "not specified";
            if (actionExecutedContext.Request.Headers.Contains(Common.RequestHeader.RequestingApplicationName))
            {
                KeyValuePair<string, IEnumerable<string>> header = actionExecutedContext.Request.Headers.First(x => x.Key == Common.RequestHeader.RequestingApplicationName);
                requestingApplicationName = string.Join(",", header.Value);
            }

            string controllerAction = string.Format("{0}/{1}",
                actionExecutedContext.ActionContext.ControllerContext.ControllerDescriptor.ControllerName,
                actionExecutedContext.ActionContext.ActionDescriptor.ActionName);

            using (IDependencyScope requestScope = actionExecutedContext.ActionContext.RequestContext.Configuration.DependencyResolver.BeginScope())
            {
                ILoggingApplicationService logger = (ILoggingApplicationService)requestScope.GetService(typeof(ILoggingApplicationService));
                logger.Info(() => $"ApplicationRegistration: {applicationRegistration}; RequestingApplicationName: {requestingApplicationName}; Action: {controllerAction}; DateTime: {DateTime.UtcNow}");
            }
        }
    }
}

﻿using System;
using System.Text;
using System.Threading.Tasks;

namespace Ivh.Common.Api.Helpers
{
    using System.Net.Http;
    using System.Net.Http.Headers;
    using Base.Constants;
    using Base.Utilities.Extensions;
    using Newtonsoft.Json;

    public class HttpClientHelper
    {
        public static TResponse CallApiWith<TResponse, TSend>(string baseUrl, string url, HttpMethod method, TSend objectToSend, Action<HttpResponseMessage, TResponse> successAction, Action<HttpResponseMessage, string> errorAction, Func<HttpClient> generateHttpClient, bool throwOnError = true)
        {
            using (HttpClient client = generateHttpClient())
            {
                client.BaseAddress = new Uri(baseUrl);//This would need to be refactored for others to use
                HttpResponseMessage response = null;
                string result = string.Empty;
                Task.Run(async () =>
                {
                    if (method == HttpMethod.Get)
                    {
                        response = await client.GetAsync(url).ConfigureAwait(false);
                        result = await response.Content.ReadAsStringAsync().ConfigureAwait(false);
                    }
                    else if (method == HttpMethod.Post)
                    {
                        var sendJson = JsonConvert.SerializeObject(objectToSend);
                        client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(MimeTypes.Application.Json));
                        response = await client.PostAsync(url, new StringContent(sendJson, Encoding.UTF8, MimeTypes.Application.Json)).ConfigureAwait(false);

                        //response = await client.PostAsync(url, objectToSend).ConfigureAwait(false);
                        result = await response.Content.ReadAsStringAsync().ConfigureAwait(false);
                    }
                    else
                    {
                        throw new NotSupportedException(string.Format("Method {0} is not supported.", method.ToString()));
                    }

                }).Wait();
                if (response.IsSuccessStatusCode)
                {
                    if (!string.IsNullOrEmpty(result))
                    {
                        var infoDto = JsonConvert.DeserializeObject<TResponse>(result);
                        successAction(response, infoDto);
                        return infoDto;
                    }
                    return default(TResponse);
                }

                errorAction(response, result);
                if (throwOnError)
                    throw new Exception(string.Format("Call to api({1}) failed: {0}", response.AggregateWebResponse(System.Reflection.MethodBase.GetCurrentMethod().Name), url));
            }
            return default(TResponse);
        }
    }
}

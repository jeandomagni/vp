makecert.exe ^
-n "CN=%1,O=iVinci Health,OU=Dev,C=US,S=ID,L=Boise" ^
-iv CARoot.pvk ^
-ic CARoot.cer ^
-pe ^
-a sha512 ^
-len 4096 ^
-b 01/01/2017 ^
-e 12/31/2037 ^
-sky exchange ^
-eku 1.3.6.1.5.5.7.3.2 ^
-sv %1.pvk ^
%1.cer
pause
pvk2pfx.exe ^
-pvk %1.pvk ^
-spc %1.cer ^
-pfx %1.pfx ^
-po cp0s7m2526
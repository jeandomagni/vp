﻿namespace Ivh.Common.State
{
    using Interfaces;

    /// <inheritdoc />
    /// <summary>
    /// </summary>
    /// <typeparam name="TEntity"></typeparam>
    /// <typeparam name="TState"></typeparam>
    public class Context<TEntity, TState> : IContext<TEntity, TState>
        where TEntity : class, IEntity<TState>
        where TState : struct
    {
        public Context(TEntity entity)
        {
            this.Entity = entity;
        }

        public TEntity Entity { get; }
    }
}
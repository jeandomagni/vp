﻿namespace Ivh.Common.State
{
    using System;
    using Interfaces;
    public class History<TEntity, TState> : IHistory<TEntity, TState>
        where TEntity : class, IEntity<TState>
        where TState : struct
    {
        public History(TEntity entity)
        {
            this.Entity = entity;
        }

        public int HistoryId { get; set; }
        public TEntity Entity { get; set; }
        public TState InitialState { get; set; }
        public TState EvaluatedState { get; set; }
        public DateTime DateTime { get; set; }
        public string RuleName { get; set; }
    }
}

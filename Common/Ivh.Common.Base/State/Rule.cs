﻿namespace Ivh.Common.State
{
    using System;
    using System.Linq.Expressions;
    using Base.Utilities.Extensions;
    using EventJournal.Interfaces;
    using Interfaces;

    /// <summary>
    /// IRule defines an applicable InitialState and a predicat expression that, if true, defines the Resulting state
    /// </summary>
    /// <typeparam name="TEntity">Must be of type <c>IEntity<typeparamref name="TState"/></c></typeparam>
    /// <typeparam name="TState">Must be of type <c>Enum</c></typeparam>
    public class Rule<TEntity, TState> : IRule<TEntity, TState>
        where TEntity : class, IEntity<TState>
        where TState : struct
    {
        public string RuleName { get; set; }
        public TState InitialState { get; set; }
        public Expression<Func<IEvaluationContext<TEntity, TState>, bool>> Expression { get; set; }
        public Func<IEvaluationContext<TEntity, TState>, bool> Predicate { get; set; }
        public TState ResultingState { get; set; }
        public Action<IEventJournalService,IEvaluationContext<TEntity, TState>> JournalEvent { get; set; }

        public IRule<TEntity, TState> When(TState initialState)
        {
            this.InitialState = initialState;
            return this;
        }
        
        public IRule<TEntity, TState> And(Expression<Func<IEvaluationContext<TEntity, TState>, bool>> expression)
        {
            this.Expression = this.Expression == null ? expression : this.Expression.Compose(expression, System.Linq.Expressions.Expression.And);
            if (this.Expression.CanReduce)
            {
                this.Expression = (Expression<Func<IEvaluationContext<TEntity, TState>, bool>>) this.Expression.Reduce();
            }
            this.Predicate = this.Expression.Compile();

            return this;
        }

        public IRule<TEntity, TState> Then(TState resultingState, Action<IEventJournalService, IEvaluationContext<TEntity, TState>> journalEvent = null)
        {
            this.ResultingState = resultingState;
            this.JournalEvent = journalEvent;
#if DEBUG
            Console.WriteLine($"{this.RuleName}: When {this.InitialState} And {this.Expression?.Body} Then {this.ResultingState}");
#endif
            return this;
        }
    }
}
﻿namespace Ivh.Common.State.Interfaces
{
    using System;

    /// <summary>
    /// All implementations of <c>IMachine</c> must provide <c>IHistory</c>.
    /// The <c>IHistory</c> provides all the previous states for the <c>IEntity</c>
    /// </summary>
    /// <typeparam name="TEntity">Must be of type <c>IEntity<typeparamref name="TState"/></c></typeparam>
    /// <typeparam name="TState">Must be of type <c>Enum</c></typeparam>
    public interface IHistory<TEntity, TState>
        where TEntity : class, IEntity<TState>
        where TState : struct
    {
        /// <summary>
        /// The unique identity for an <c>IHistory</c>
        /// </summary>
        int HistoryId { get; set; }
        /// <summary>
        /// The <c>IEntity</c> related to this <c>IHistory</c>
        /// </summary>
        TEntity Entity { get; set; }
        /// The <typeparamref name="TState"/> prior to a state change
        TState InitialState { get; set; }
        /// The <typeparamref name="TState"/> after a state change
        TState EvaluatedState { get; set; }
        /// The <c>DateTime</c> when the state changed
        DateTime DateTime { get; set; }
        /// <summary>
        /// The name of the Rule that invoked the change in state
        /// </summary>
        string RuleName { get; set; }
    }
}

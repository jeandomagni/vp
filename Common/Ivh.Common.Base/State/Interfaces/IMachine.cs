﻿namespace Ivh.Common.State.Interfaces
{
    /// <summary>
    /// IMachine defines the base implementation of a StateMachine
    /// </summary>
    /// <typeparam name="TEntity">Must be of type <c>IEntity<typeparamref name="TState"/></c></typeparam>
    /// <typeparam name="TState">Must be of type <c>Enum</c></typeparam>
    public interface IMachine<TEntity, TState>
        where TEntity : class, IEntity<TState>
        where TState : struct
    {
        /// <summary>
        /// The <c>IEvaluationContext</c> that <c>IMachine</c> passes to the <c>IRule</c>
        /// </summary>
        IEvaluationContext<TEntity, TState> EvaluationContext { get; set; }
        /// <summary>
        /// The <c>IContextProvider</c> that <c>IMachine</c> uses to retrieve <c>IContext</c>
        /// </summary>
        IContextProvider<TEntity, TState> ContextProvider { get; set; }
        /// <summary>
        /// The list of rules that the <c>IMachine</c> uses to evaluate state
        /// </summary>
        Rules<TEntity, TState> Rules { get; set; }
        /// <summary>
        /// The <c>Evaluate</c> method tells the <c>IMachine</c> to evaluate the state for <paramref name="entity"/>
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        TState Evaluate(TEntity entity);
        /// <summary>
        /// <c>DefaultState</c> is the <typeparamref name="TState"/> returned if no rules return true.
        /// </summary>
        TState DefaultState { get; set; }
        /// <summary>
        /// <c>DefaultMessage</c> is the default message logged to the state history provider if no rules return true.
        /// </summary>
        string DefaultMessage { get; set; }
    }
}

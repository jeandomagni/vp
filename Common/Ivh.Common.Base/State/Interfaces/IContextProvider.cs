﻿namespace Ivh.Common.State.Interfaces
{
    /// <summary>
    /// All implementations of <c>IMachine</c> must provide <c>IContextProvider</c>.
    /// IContextProvider is a service that will return an <c>IContext</c>
    /// </summary>
    /// <typeparam name="TEntity">Must be of type <c>IEntity<typeparamref name="TState"/></c></typeparam>
    /// <typeparam name="TState">Must be of type <c>Enum</c></typeparam>
    public interface IContextProvider<TEntity, TState>
        where TEntity : class, IEntity<TState>
        where TState : struct
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="entity">The input parameter that is used by the <c>IContextProvider</c> implementation to generate the supplemental <c>IContext</c></param>
        /// <returns></returns>
        IContext<TEntity, TState> GetContext(TEntity entity);
    }
}
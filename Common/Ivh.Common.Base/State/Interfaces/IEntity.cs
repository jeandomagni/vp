﻿namespace Ivh.Common.State.Interfaces
{
    /// <summary>
    /// All entities that use the <c>IMachine</c> must implement <c>IEntity</c>. This is the entity being evaluated.
    /// </summary>
    /// <typeparam name="TState">Must be of type <c>Enum</c></typeparam>
    public interface IEntity<TState> where TState : struct
    {
        TState GetState();

        /// <summary>
        /// </summary>
        /// <param name="state"></param>
        /// <param name="ruleName"></param>
        /// <returns></returns>
        void SetState(TState state, string ruleName);

        IHistory<IEntity<TState>,TState> GetCurrentStateHistory();
    }
}
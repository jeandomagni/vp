﻿namespace Ivh.Common.State.Interfaces
{
    using System;
    using System.Linq.Expressions;
    using EventJournal;
    using EventJournal.Interfaces;

    public interface IRule<TEntity, TState>
    where TEntity : class, IEntity<TState>
    where TState : struct
    {
        string RuleName { get; set; }
        TState InitialState { get; set; }
        TState ResultingState { get; set; }
        Func<IEvaluationContext<TEntity, TState>, bool> Predicate { get; set; }
        Action<IEventJournalService, IEvaluationContext<TEntity, TState>> JournalEvent { get; set; }
        IRule<TEntity, TState> Then(TState resultingState, Action<IEventJournalService, IEvaluationContext<TEntity, TState>> journalEvent = null);
        IRule<TEntity, TState> And(Expression<Func<IEvaluationContext<TEntity, TState>, bool>> expression);
        IRule<TEntity, TState> When(TState initialState);
    }
}

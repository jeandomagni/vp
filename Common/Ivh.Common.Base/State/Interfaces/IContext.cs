﻿namespace Ivh.Common.State.Interfaces
{
    /// <summary>
    /// All implementations of <c>IMachine</c> must provide <c>IContext</c>.
    /// The <c>IContext</c> provides additional/supplemental decision making information that is not available in the <c>IEntity</c>
    /// </summary>
    /// <typeparam name="TEntity">Must be of type <c>IEntity<typeparamref name="TState"/></c></typeparam>
    /// <typeparam name="TState">Must be of type <c>Enum</c></typeparam>
    public interface IContext<out TEntity,TState>
        where TEntity : class, IEntity<TState>
        where TState : struct
    {
        TEntity Entity { get; }
    }
}
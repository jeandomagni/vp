﻿namespace Ivh.Common.State.Interfaces
{
    using System;
    using System.Collections.Generic;

    /// <summary>
    /// All implementations of <c>IMachine</c> must provide <c>IEvaluationContext&lt;<typeparamref name="TEntity"/>,<typeparamref name="TState"/>,<typeparamref name="TContext"/>,<typeparamref name="THistory"/>&gt;</c>.
    /// <c>IEvaluationContext</c> is the context given to each <c>IRule</c>.
    /// </summary>
    /// <typeparam name="TEntity">Must be of type <c>IEntity<typeparamref name="TState"/></c></typeparam>
    /// <typeparam name="TState">Must be of type <c>Enum</c></typeparam>
    public interface IEvaluationContext<TEntity, TState>
        where TEntity : class, IEntity<TState>
        where TState : struct
    {
        /// <summary>
        /// The <c>IEntity</c> being evaluated.
        /// </summary>
        TEntity Entity { get; set; }
        /// <summary>
        /// The <c>IEntity</c> state before evaluation.
        /// </summary>
        TState InitialState { get; set; }
        /// <summary>
        /// The <c>IEntity</c> state after evaluation
        /// </summary>
        TState ResultingState { get; set; }
        /// <summary>
        /// Any supplemental information needed (outside of the <c>IEntity</c>) to evaluate the current state
        /// </summary>
        Lazy<IContext<TEntity, TState>> Context { get; set; }
        /// <summary>
        /// A list of all previous states for this <c>IEntity</c>
        /// </summary>
        Lazy<IList<IHistory<TEntity, TState>>> History { get; set; }
    }
}

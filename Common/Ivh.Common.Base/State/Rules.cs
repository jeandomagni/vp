﻿namespace Ivh.Common.State
{
    using System.Collections.Generic;
    using Interfaces;

    /// <inheritdoc />
    /// <summary>
    /// </summary>
    /// <typeparam name="TEntity">Must be of type <c>IEntity<typeparamref name="TState" /></c></typeparam>
    /// <typeparam name="TState">Must be of type <c>Enum</c></typeparam>
    public class Rules<TEntity, TState> :
        List<IRule<TEntity, TState>>
        //IRules<TEntity, TState, TContext, THistory>
        where TEntity : class, IEntity<TState>
        where TState : struct
    {
        public Rule<TEntity, TState> AddRule(string ruleName)
        {
            Rule<TEntity, TState> rule = new Rule<TEntity, TState>() { RuleName = ruleName };
            this.Add(rule);
            return rule;
        }
    }
}
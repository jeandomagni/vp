﻿namespace Ivh.Common.State.Test
{
    using System;
    using EventJournal.Interfaces;
    using Interfaces;
    using Moq;
    using NUnit.Framework;

    /// <summary>
    /// The <c>UnitTestBase</c> is a base class helper for setting up statemachine unit tests
    /// </summary>
    /// <typeparam name="TEntity">Must be of type <c>IEntity<typeparamref name="TState"/></c></typeparam>
    /// <typeparam name="TState">Must be of type <c>Enum</c></typeparam>
    /// <typeparam name="TRules">Must be of type Machine IRules&lt;TEntity, TState, TContext, THistory&gt;</typeparam>
    /// <typeparam name="TMachine">Must be of type Machine &lt;TEntity, TState, TContext, THistory&gt;</typeparam>
    [TestFixture]
    public abstract class UnitTestBase<TEntity, TState, TRules, TMachine>
        where TEntity : class, IEntity<TState>
        where TState : struct
        where TRules : Rules<TEntity, TState>, new()
        where TMachine : IMachine<TEntity, TState>
    {
        private readonly TState _defaultState;
        private readonly string _defaultMessage;
        protected TMachine Machine = default(TMachine);
        protected Mock<IContextProvider<TEntity, TState>> ContextProvider = null;
        protected Mock<IEventJournalService> EventJournalService = null;
        protected string RuleName = string.Empty;

        protected UnitTestBase(TState defaultState, string defaultMessage)
        {
            this._defaultState = defaultState;
            this._defaultMessage = defaultMessage;
        }

        [OneTimeSetUp]
        public void BeforeAllTests()
        {
            this.ContextProvider = new Mock<IContextProvider<TEntity, TState>>();
            this.EventJournalService = new Mock<IEventJournalService>();

            this.Machine = (TMachine)Machine<TEntity, TState>.Create(
                this.ContextProvider.Object,
                new Lazy<IEventJournalService>(() => this.EventJournalService.Object),
                new TRules(), 
                this._defaultState, 
                this._defaultMessage);
        }

        [SetUp]
        public void BeforeEachTest()
        {
            this.RuleName = string.Empty;
        }
    }
}

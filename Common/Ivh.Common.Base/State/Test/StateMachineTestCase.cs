﻿using System;

namespace Ivh.Common.State.Test
{
    using Interfaces;

    public class StateMachineTestCase<TEntity, TState>
        where TEntity : class, IEntity<TState>, new()
        where TState : struct
    {
        public TEntity Entity { get; set; }
        public string RuleName { get; set; }
        public TState ExpectedStateAfterEvaluation { get; set; }
        public string TestCaseDescription { get; set; }

        public static StateMachineTestCase<TEntity, TState> TestRule(string ruleName)
        {
            return new StateMachineTestCase<TEntity, TState>() { RuleName = ruleName, Entity = new TEntity() };
        }

        public StateMachineTestCase<TEntity, TState> WithInitialState(TState state)
        {
            Rule<IEntity<TState>, TState> rule = new Rule<IEntity<TState>, TState>{ RuleName = string.Empty, ResultingState = state };
            this.TestCaseDescription = $"{this.Entity.GetType().Name} with initial state {rule.ResultingState}";
            this.Entity.SetState(rule.ResultingState, rule.RuleName);
            this.Entity.GetCurrentStateHistory().HistoryId = 1;
            return this;
        }

        public StateMachineTestCase<TEntity, TState> And(Func<TEntity, TEntity> decorator)
        {
            this.TestCaseDescription += $" and {decorator.Method.Name}";
            this.Entity = decorator(this.Entity);
            return this;
        }

        public StateMachineTestCase<TEntity, TState> ExpectsFinalState(TState state)
        {
            this.ExpectedStateAfterEvaluation = state;
            return this;
        }

        public override string ToString()
        {
            return $"{this.TestCaseDescription} moves to {this.ExpectedStateAfterEvaluation} due to {this.RuleName}";
        }
    }
}

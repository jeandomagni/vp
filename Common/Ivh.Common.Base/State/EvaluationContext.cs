﻿namespace Ivh.Common.State
{
    using System;
    using System.Collections.Generic;
    using Interfaces;

    /// <inheritdoc />
    /// <summary>
    /// </summary>
    /// <typeparam name="TEntity"></typeparam>
    /// <typeparam name="TState"></typeparam>
    public class EvaluationContext<TEntity, TState> : IEvaluationContext<TEntity, TState>
        where TEntity : class, IEntity<TState>
        where TState : struct
    {
        public TEntity Entity { get; set; }
        public TState InitialState { get; set; }
        public TState ResultingState { get; set; }
        public Lazy<IContext<TEntity, TState>> Context { get; set; }
        public Lazy<IList<IHistory<TEntity, TState>>> History { get; set; }
    }
}

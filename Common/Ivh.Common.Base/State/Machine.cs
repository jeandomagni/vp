﻿namespace Ivh.Common.State
{
    using System;
    using System.Collections.Generic;
    using System.Configuration;
    using System.Linq;
    using EventJournal.Interfaces;
    using Interfaces;

    /// <inheritdoc />
    /// <summary>
    /// </summary>
    /// <typeparam name="TEntity"></typeparam>
    /// <typeparam name="TState"></typeparam>
    public class Machine<TEntity, TState> : IMachine<TEntity, TState>
        where TEntity : class, IEntity<TState>
        where TState : struct
    {
        private readonly Lazy<IEventJournalService> _eventJournalService;

        public static IMachine<TEntity, TState> Create(
            IContextProvider<TEntity, TState> contextProvider,
            Lazy<IEventJournalService> eventJournalService,
            Rules<TEntity, TState> rules,
            TState defaultState,
            string defaultMessage = "DEFAULT")
        {
            return new Machine<TEntity, TState>(contextProvider, rules, eventJournalService, defaultState, defaultMessage);
        }


        public IContextProvider<TEntity, TState> ContextProvider { get; set; }
        public Rules<TEntity, TState> Rules { get; set; }
        public IEvaluationContext<TEntity, TState> EvaluationContext { get; set; }
        public TState DefaultState { get; set; }
        public string DefaultMessage { get; set; }
        
        public Machine(
            IContextProvider<TEntity, TState> contextProvider,
            Rules<TEntity, TState> rules,
            Lazy<IEventJournalService> eventJournalService,
            TState defaultState,
            string defaultMessage = "DEFAULT")
        {
            this.ContextProvider = contextProvider;
            this.Rules = rules;
            this.DefaultState = defaultState;
            this.DefaultMessage = defaultMessage;
            this._eventJournalService = eventJournalService;
        }

        public Machine()
        {
        }

        public TState Evaluate(TEntity entity)
        {
            TState state = ((IEntity<TState>)entity).GetState();
            this.EvaluationContext = new EvaluationContext<TEntity, TState>()
            {
                Entity = entity,
                Context = new Lazy<IContext<TEntity, TState>>(() => this.ContextProvider.GetContext(this.EvaluationContext.Entity)),
                InitialState = state,
                ResultingState = state,
            };

            IRule<TEntity, TState> rule = this.Rules.FirstOrDefault((r) => EqualityComparer<TState>.Default.Equals(((IEntity<TState>)this.EvaluationContext.Entity).GetState(), r.InitialState)
                                               && r.Predicate(this.EvaluationContext));

            this.EvaluationContext.ResultingState = rule?.ResultingState ?? this.DefaultState;

            if (rule != null)
            {
                // IF the resulting state and rule are the same, short circuit
                if (EqualityComparer<TState>.Default.Equals(entity.GetState(), this.EvaluationContext.ResultingState))
                {
                    IHistory<IEntity<TState>, TState> currentState = entity.GetCurrentStateHistory();
                    if (currentState != null 
                        && currentState.HistoryId != default(int) 
                        && StringComparer.OrdinalIgnoreCase.Equals(rule.RuleName, currentState.RuleName))
                    {
                        return this.EvaluationContext.ResultingState;
                    }
                }

                entity.SetState(rule.ResultingState, rule.RuleName);

                if (rule?.JournalEvent != null)
                {
                    if (this._eventJournalService != null)
                    {
                        rule.JournalEvent(this._eventJournalService.Value, this.EvaluationContext);
                    }
                    else
                    {
                        throw new ConfigurationErrorsException("Rule specifies a JournalEvent, but the EventJournalService is null.");
                    }
                }
            }

            return this.EvaluationContext.ResultingState;
        }
    }
}
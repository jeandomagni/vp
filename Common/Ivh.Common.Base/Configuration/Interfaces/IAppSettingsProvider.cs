﻿namespace Ivh.Common.Configuration.Interfaces
{
    public interface IAppSettingsProvider
    {
        string[] AllKeys { get; }
 
        string Get(string key);
    }
}
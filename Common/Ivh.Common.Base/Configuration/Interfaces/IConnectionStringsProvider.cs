﻿namespace Ivh.Common.Configuration.Interfaces
{
    using System.Configuration;

    public interface IConnectionStringsProvider
    {
        /// <summary>
        /// Gets the "connectionString" value from the <see cref="ConnectionStringSettings"/> with the given name.
        /// </summary>
        string GetConnectionString(string name);
    }
}
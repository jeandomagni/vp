﻿namespace Ivh.Common.Configuration
{
    using System;
    using System.Configuration;

    public class BaseConfigurationProvider
    {
        protected const EnvironmentVariableTarget Target = EnvironmentVariableTarget.Machine;
        protected const string UseEnvironmentVariablesOnlyMessage = "The current configuration derives values from environment variables only.";
        private const string UseEnvironmentVariablesOnlyKey = "VP_USE_ENVARS_ONLY";

        /// <summary>
        /// Specifies whether to get values from environment variables exclusively.
        /// If false, or not set, allows fallback on <see cref="ConfigurationManager"/>.
        /// </summary>
        public bool UseEnvironmentVariablesOnly()
        {
            string useEnVarsOnlySetting = Environment.GetEnvironmentVariable(UseEnvironmentVariablesOnlyKey, Target);
            bool isValueBoolean = bool.TryParse(useEnVarsOnlySetting, out bool result);
            return isValueBoolean && result;
        }

        protected string GetEnVarRetrievalSuccessMessage(string variableName, string settingType)
        {
            return $"Retrieved value for {settingType} \"{variableName}\" from environment variables in the {Target.ToString()} context.";
        }

        protected string GetEnVarRetrievalFailureMessage(string variableName, string settingType)
        {
            return $"The {settingType} environment variable with key \"{variableName}\" was not found in the {Target.ToString()} context.";
        }

        protected string GetConfigManagerRetrievalFailureMessage(string key, string settingType)
        {
            return $"{nameof(ConfigurationManager)} could not retrieve value for {settingType} \"{key}\".";
        }

        protected string GetConfigManagerRetrievalSuccessMessage(string key, string settingType)
        {
            return $"Retrieved value for {settingType} \"{key}\" via {nameof(ConfigurationManager)}.";
        }
    }
}
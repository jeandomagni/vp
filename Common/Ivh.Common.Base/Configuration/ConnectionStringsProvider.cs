﻿namespace Ivh.Common.Configuration
{
    using Base.Utilities.Extensions;
    using Interfaces;
    using System;
    using System.Configuration;
    using VisitPay.Utilities.Helpers;

    public class ConnectionStringsProvider : BaseConfigurationProvider, IConnectionStringsProvider
    {
        private const string SettingTypeName = "Connection String";

        /// <inheritdoc />
        /// <summary>
        /// Attempts to retrieve the connection string value with the given name from environment variables.
        /// If value is unavailable, and configuration allows, this falls back on <see cref="ConfigurationManager" /> to get the value.
        /// </summary>
        public string GetConnectionString(string name)
        {
            string variableName = FormatHelper.ConfigurationItemKeyToEnvironmentVariableFormat(name);
            string enVarValue = Environment.GetEnvironmentVariable(variableName, Target);

            if (enVarValue.IsNotNullOrEmpty())
            {
                Console.WriteLine(this.GetEnVarRetrievalSuccessMessage(variableName, SettingTypeName));
                return enVarValue;
            }

            if (this.UseEnvironmentVariablesOnly())
            {
                throw new ConfigurationErrorsException(
                    $"{UseEnvironmentVariablesOnlyMessage} {this.GetEnVarRetrievalFailureMessage(variableName, SettingTypeName)}");
            }

            ConnectionStringSettings settings = ConfigurationManager.ConnectionStrings[name];

            if (settings == null)
            {
                Console.WriteLine(this.GetConfigManagerRetrievalFailureMessage(name, SettingTypeName));
                return null;
            }

            Console.WriteLine(this.GetConfigManagerRetrievalSuccessMessage(name, SettingTypeName));
            return settings.ConnectionString;
        }

        public static ConnectionStringsProvider Instance => new ConnectionStringsProvider();
    }
}
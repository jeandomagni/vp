﻿namespace Ivh.Common.Configuration
{
    using Base.Utilities.Extensions;
    using Interfaces;
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Collections.Specialized;
    using System.Configuration;
    using System.Linq;
    using VisitPay.Utilities.Helpers;

    public class AppSettingsProvider : BaseConfigurationProvider, IAppSettingsProvider
    {
        private const string SettingTypeName = "App Setting";

        /// <summary>
        /// Equivalent to ConfigurationManager.AppSettings.AllKeys,
        /// but includes all environment variable keys along with those from
        /// app settings accessible through <see cref="ConfigurationManager" />.
        /// (If the "VP_USE_ENVARS_ONLY" environment variable is set,
        /// returns environment variable keys exclusively.)
        /// </summary>
        public string[] AllKeys
        {
            get
            {
                List<string> allKeysFromEnvironmentVariables = GetAllKeysFromEnvironmentVariables().ToList();

                if (this.UseEnvironmentVariablesOnly())
                {
                    // don't fall back on config manager
                    Console.WriteLine($"{nameof(AppSettingsProvider)}: Getting all keys from environment variables.");
                    return allKeysFromEnvironmentVariables.ToArray();
                }

                // if envars-only flag isn't set, send back the combined list of environment variables and app settings 
                // so callers can potentially fall back as they can through get method
                List<string> allKeysFromConfigurationManager = ConfigurationManager.AppSettings.AllKeys.ToList();
                IEnumerable<string> allKeys = allKeysFromEnvironmentVariables.Union(allKeysFromConfigurationManager);

                Console.WriteLine($"{nameof(AppSettingsProvider)}: Getting all keys from {nameof(ConfigurationManager)}.");
                return allKeys.ToArray();
            }
        }

        /// <summary>
        /// Equivalent to ConfigurationManager.AppSettings,
        /// but includes all environment variable key/value pairs along with
        /// app settings accessible through <see cref="ConfigurationManager" />.
        /// (If the "VP_USE_ENVARS_ONLY" environment variable is set,
        /// returns environment variable key/values exclusively.)
        /// </summary>
        public NameValueCollection AppSettings
        {
            get
            {
                NameValueCollection appSettings = new NameValueCollection();
                IDictionary enVars = Environment.GetEnvironmentVariables(Target);

                foreach (DictionaryEntry entry in enVars)
                {
                    string name = entry.Key.ToString();
                    string value = entry.Value.ToString();
                    appSettings.Add(name, value);
                }

                if (this.UseEnvironmentVariablesOnly())
                {
                    // don't fall back on config manager
                    return appSettings;
                }

                // if not restricted to environment variables,
                // add key/value pairs from app settings accessible through ConfigurationManager
                foreach (string key in ConfigurationManager.AppSettings.AllKeys)
                {
                    string value = ConfigurationManager.AppSettings.Get(key);
                    appSettings.Add(key, value);
                }

                return appSettings;
            }
        }

        /// <inheritdoc />
        /// <summary>
        /// Attempts to retrieve the app setting value for the given key from environment variables.
        /// If value is unavailable, and configuration allows, this falls back on <see cref="ConfigurationManager" /> to get the value.
        /// </summary>
        public string Get(string key)
        {
            string variableName = FormatHelper.ConfigurationItemKeyToEnvironmentVariableFormat(key);
            string enVarValue = Environment.GetEnvironmentVariable(variableName, Target);

            if (enVarValue.IsNotNullOrEmpty())
            {
                Console.WriteLine(this.GetEnVarRetrievalSuccessMessage(variableName, SettingTypeName));
                return enVarValue;
            }

            if (this.UseEnvironmentVariablesOnly())
            {
                throw new ConfigurationErrorsException(
                    $"{UseEnvironmentVariablesOnlyMessage} {this.GetEnVarRetrievalFailureMessage(variableName, SettingTypeName)}");
            }

            string valueFromAppSettingsConfig = ConfigurationManager.AppSettings.Get(key);

            if (valueFromAppSettingsConfig.IsNullOrEmpty())
            {
                Console.WriteLine(this.GetConfigManagerRetrievalFailureMessage(key, SettingTypeName));
                return null;
            }

            Console.WriteLine(this.GetConfigManagerRetrievalSuccessMessage(key, SettingTypeName));
            return valueFromAppSettingsConfig;
        }

        public static AppSettingsProvider Instance => new AppSettingsProvider();

        private static IEnumerable<string> GetAllKeysFromEnvironmentVariables()
        {
            List<string> allKeys = new List<string>();
            IDictionary enVars = Environment.GetEnvironmentVariables(Target);

            foreach (DictionaryEntry entry in enVars)
            {
                allKeys.Add(entry.Key.ToString());
            }

            return allKeys.ToArray();
        }
    }
}
﻿namespace Ivh.Common.Session.Obfuscation
{
    using System;
    using System.Web.SessionState;

    [ObfuscatorKeyAttribute("VisitItemizationObfuscator")]
    public class VisitItemizationObfuscator : Obfuscator<int,Guid>
    {
        public VisitItemizationObfuscator()
            : base()
        {

        }

        public VisitItemizationObfuscator(HttpSessionState session)
            : base(session)
        {
        }

        public override Guid GenerateObfuscatedValue()
        {
            return Guid.NewGuid();
        }
    }
}

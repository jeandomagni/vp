﻿namespace Ivh.Common.Session.Obfuscation
{
    using System;
    using System.Web.SessionState;

    [ObfuscatorKeyAttribute("ConsolidationGuarantorObfuscator")]
    public class ConsolidationGuarantorObfuscator : Obfuscator<int,Guid>
    {
        public ConsolidationGuarantorObfuscator()
            : base()
        {

        }

        public ConsolidationGuarantorObfuscator(HttpSessionState session)
            : base(session)
        {
        }

        public override Guid GenerateObfuscatedValue()
        {
            return Guid.NewGuid();
        }
    }
}

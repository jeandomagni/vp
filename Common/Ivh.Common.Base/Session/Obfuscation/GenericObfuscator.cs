﻿namespace Ivh.Common.Session.Obfuscation
{
    using System;
    using System.Web.SessionState;

    [ObfuscatorKey("GenericObfuscator")]
    public class GenericObfuscator<T> : Obfuscator<T, Guid>
    {
        public GenericObfuscator()
        {
        }

        public GenericObfuscator(HttpSessionState session)
            : base(session)
        {
        }

        public override Guid GenerateObfuscatedValue()
        {
            return Guid.NewGuid();
        }
    }
}
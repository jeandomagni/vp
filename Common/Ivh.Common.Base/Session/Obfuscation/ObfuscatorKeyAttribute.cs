﻿namespace Ivh.Common.Session.Obfuscation
{
    using System;

    [AttributeUsage(AttributeTargets.Class)]
    public class ObfuscatorKeyAttribute : Attribute
    {
        public ObfuscatorKeyAttribute(string key)
        {
            this.Key = key;
        }

        public string Key { get; private set; }
    }
}
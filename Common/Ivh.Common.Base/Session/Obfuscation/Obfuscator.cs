﻿namespace Ivh.Common.Session.Obfuscation
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Web;
    using System.Web.SessionState;
    using Base.Collections;
    using Base.Utilities.Extensions;
    using Ivh.Common.Base.Constants;

    public abstract class Obfuscator<T1, T2>
    {
        private readonly HttpSessionState _session;

        private Map<T1, T2> _map;

        protected Obfuscator()
        {
            this._session = HttpContext.Current.Session;
            this._key = this.GetAttribute<ObfuscatorKeyAttribute>().Key;
        }

        protected Obfuscator(HttpSessionState session)
        {
            this._session = session;
            this._key = this.GetAttribute<ObfuscatorKeyAttribute>().Key;
        }

        private string _key { get; set; }
        private static readonly object _objLock = new object();

        public Map<T1, T2> Map
        {
            get
            {
                if (this._map == null)
                {
                    lock (_objLock)
                    {
                        if (this._map == null)
                        {
                            object check = this._session[this._key];
                            this._map = check == null ? new Map<T1, T2>() : new Map<T1, T2>((IDictionary<T1, T2>) check);
                        }
                    }
                }
                return this._map;
            }
        }


        public T2 Obfuscate(T1 clear)
        {
            return this.Obfuscate(new List<T1> {clear}).First().Value;
        }

        public IDictionary<T1, T2> Obfuscate(IList<T1> clear)
        {
            bool added = false;
            foreach (T1 c in clear.Where(c => !this.Map.Forward.ContainsKey(c)))
            {
                this.Map.Add(c, this.GenerateObfuscatedValue());
                added = true;
            }
            if (added && !this._session.IsReadOnly)
            {
                this._session[this._key] = this.Map.Forward;
            }
            if (added && this._session.IsReadOnly)
            {
                throw new HttpUnhandledException(SessionConstants.ReadOnlyWriteError);
            }

            return this.Map.Forward.Where(x => clear.Contains(x.Key)).ToDictionary(k => k.Key, v => v.Value);
        }

        public T1 Clarify(T2 obfuscated)
        {
            IDictionary<T2, T1> clarified = this.Clarify(new List<T2> {obfuscated});
            return clarified.Any() ? clarified.First().Value : default(T1);
        }

        public IDictionary<T2, T1> Clarify(IList<T2> obfuscated)
        {
            return this.Map.Reverse.Where(x => obfuscated.Contains(x.Key)).ToDictionary(k => k.Key, v => v.Value);
        }

        public abstract T2 GenerateObfuscatedValue();
    }
}
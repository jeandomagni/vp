﻿namespace Ivh.Common.Session.Obfuscation
{
    using System;
    using System.Web.SessionState;

    [ObfuscatorKeyAttribute("VpGuarantorObfuscator")]
    public class VpGuarantorObfuscator : Obfuscator<int,Guid>
    {
        public VpGuarantorObfuscator()
            : base()
        {

        }

        public VpGuarantorObfuscator(HttpSessionState session)
            : base(session)
        {
        }

        public override Guid GenerateObfuscatedValue()
        {
            return Guid.NewGuid();
        }
    }
}

﻿namespace Ivh.Common.Session
{
    using System.Web;
    using Obfuscation;
    using VisitPay.Enums;
    using Ivh.Common.Base.Constants;
    using System.Diagnostics;

    public class SessionFacadeBase : ISessionFacade
    {
        private const string ClientIdSessionKey = "ClientId";
        private const string VpGuarantorIdSessionKey = "VpGuarantorId";
        private const string FilteredVpGuarantorIdSessionKey = "FilteredVpGuarantorId";

        public int ClientId
        {
            get { return this.GetSessionValue<int>(ClientIdSessionKey); }
            set { this.SetSessionValue(ClientIdSessionKey, value); }
        }

        public void ClearSession()
        {
            HttpContext.Current.Session.Clear();
            HttpContext.Current.Session.Abandon();
            if (HttpContext.Current.Session.Count > 0)
            {
                throw new HttpUnhandledException(SessionConstants.CountNotEmptyClearError);
            }
#if DEBUG
            Trace.WriteLine("HttpContext.Current.Session has been cleared");
#endif
        }

        public int VpGuarantorId
        {
            get { return this.GetSessionValue<int>(VpGuarantorIdSessionKey); }
            set { this.SetSessionValue(VpGuarantorIdSessionKey, value); }
        }

        protected T GetSessionValue<T>(string key)
        {
            object check = HttpContext.Current.Session[key];
            if (check == null)
                return default(T);
            return (T)check;
        }

        protected string GetQueryStringValue(string key)
        {
            string check = HttpContext.Current.Request.QueryString[key];
            return check;
        }

        public void SetSessionValue(string key, object value)
        {
            if (HttpContext.Current.Session.IsReadOnly)
            {
                throw new HttpUnhandledException(SessionConstants.ReadOnlyWriteError);
            }
            HttpContext.Current.Session[key] = value;
        }

        private ConsolidationGuarantorObfuscator _consolidationGuarantorObfuscator = null;

        public ConsolidationGuarantorObfuscator ConsolidationGuarantorObfuscator
        {
            get { return this._consolidationGuarantorObfuscator ?? (this._consolidationGuarantorObfuscator = new ConsolidationGuarantorObfuscator(HttpContext.Current.Session)); }
        }

        private VpGuarantorObfuscator _vpGuarantorObfuscator = null;

        public VpGuarantorObfuscator GuarantorObfuscator
        {
            get { return this._vpGuarantorObfuscator ?? (this._vpGuarantorObfuscator = new VpGuarantorObfuscator(HttpContext.Current.Session)); }
        }

        private VisitItemizationObfuscator _visitItemizationObfuscator = null;

        public VisitItemizationObfuscator VisitItemizationObfuscator
        {
            get { return this._visitItemizationObfuscator ?? (this._visitItemizationObfuscator = new VisitItemizationObfuscator(HttpContext.Current.Session)); }
        }

        public RandomizedTestGroupEnum GetRandomizedTestGroupEnum(RandomizedTestEnum randomizedTest)
        {
            return this.GetSessionValue<RandomizedTestGroupEnum>(randomizedTest.ToString());
        }
        public void SetRandomizedTestGroupEnum(RandomizedTestEnum randomizedTest, RandomizedTestGroupEnum randomizedTestGroup)
        {
            this.SetSessionValue(randomizedTest.ToString(), randomizedTestGroup);
        }
    }
}
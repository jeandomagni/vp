﻿namespace Ivh.Common.Session
{
    using Base.Enums;
    using Obfuscation;
    using VisitPay.Enums;

    public interface ISessionFacade
    {
        int ClientId { get; set; }
        int VpGuarantorId { get; set; }
        void ClearSession();
        void SetSessionValue(string key, object value);
        ConsolidationGuarantorObfuscator ConsolidationGuarantorObfuscator { get; }
        VpGuarantorObfuscator GuarantorObfuscator { get; }
        VisitItemizationObfuscator VisitItemizationObfuscator { get; }
        RandomizedTestGroupEnum GetRandomizedTestGroupEnum(RandomizedTestEnum randomizedTest);
        void SetRandomizedTestGroupEnum(RandomizedTestEnum randomizedTest, RandomizedTestGroupEnum randomizedTestGroup);
    }
}
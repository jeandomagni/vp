﻿namespace Ivh.Common.Session
{
    using System.Collections.Generic;

    public interface IWebClientSessionFacade : ISessionFacade
    {
        IDictionary<int, int> FilteredVpGuarantorId { get; }
        void SetFilteredVpGuarantorId(int currentVisitPayUserId, int? filteredVpGuarantorId);
    }
}
﻿namespace Ivh.Common.Session
{
    using System.Collections.Generic;

    public class WebClientSessionFacade : SessionFacadeBase, IWebClientSessionFacade
    {
        private const string FilteredVpGuarantorIdSessionKey = "FilteredVpGuarantorId";

        public IDictionary<int, int> FilteredVpGuarantorId
        {
            get
            {
                object obj = this.GetSessionValue<object>(FilteredVpGuarantorIdSessionKey);
                if (obj == null)
                    return new Dictionary<int, int>();

                return (IDictionary<int, int>) obj;
            }
        }

        public void SetFilteredVpGuarantorId(int currentVisitPayUserId, int? filteredVpGuarantorId)
        {
            object obj = this.GetSessionValue<object>(FilteredVpGuarantorIdSessionKey);
            IDictionary<int, int> dict = (obj == null) ? new Dictionary<int, int>() : (IDictionary<int, int>) obj;

            if (dict.ContainsKey(currentVisitPayUserId))
            {
                if (filteredVpGuarantorId.HasValue)
                    dict[currentVisitPayUserId] = filteredVpGuarantorId.Value;
                else
                    dict.Remove(currentVisitPayUserId);
            }
            else if (filteredVpGuarantorId.HasValue)
            {
                dict.Add(currentVisitPayUserId, filteredVpGuarantorId.Value);
            }

            this.SetSessionValue(FilteredVpGuarantorIdSessionKey, dict);
        }
    }
}

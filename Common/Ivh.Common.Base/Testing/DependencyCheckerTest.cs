﻿namespace Ivh.Common.Testing
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Reflection;
    using Base.Utilities.Extensions;
    using NUnit.Framework;

    [TestFixture]
    public abstract class DependencyCheckerTest
    {

        private readonly IList<KeyValuePair<string, string>> _globalIgnoreList = new List<KeyValuePair<string, string>>
        {
            new KeyValuePair<string, string>("MySql.Data","Elmah"),
            new KeyValuePair<string, string>("Npgsql","Elmah"),
            new KeyValuePair<string, string>("System.Data.SQLite","Elmah"),
        };

        protected virtual void CheckDependencies(AssemblyName entryPoint, IList<KeyValuePair<string, string>> ignoreList = null)
        {
            DateTime begin = new DateTime(DateTime.UtcNow.Year, DateTime.UtcNow.Month, DateTime.UtcNow.Day, 9,0,0);
            DateTime end = new DateTime(DateTime.UtcNow.Year, DateTime.UtcNow.Month, DateTime.UtcNow.Day, 17, 0, 0);
#if DEBUG
            if(true)
#else
            if (DateTime.UtcNow.IsBetween(begin, end))
#endif
            {
                return;
            }

#pragma warning disable 162
            IList<DependencyDetails> referencedAssemblies = this.GetReferencedAssemblies(entryPoint);
            if (ignoreList != null)
            {
                this._globalIgnoreList.AddRangeDistinct(ignoreList);
            }

            IList<string> results = new List<string>();
            if (referencedAssemblies.Any(x => !x.CanLoad))
            {
                foreach (DependencyDetails missingAssembly in referencedAssemblies.Where(x => !x.CanLoad))
                {
                    if (!this._globalIgnoreList.Any(x => x.Key.Equals(missingAssembly.Dependency.Name, StringComparison.OrdinalIgnoreCase)
                        && x.Value.Equals(missingAssembly.ImmediateDependent.Name, StringComparison.OrdinalIgnoreCase)))
                    {
                        missingAssembly.DependencyChain.Reverse();
                        string dependencyChain = string.Join(" => ", missingAssembly.DependencyChain.Select(x => x.Name));
                        results.Add($"Referenced Assembly missing. {missingAssembly.Dependency.Name} referenced by: {dependencyChain}");
                    }
                }
            }
            Assert.IsTrue(results.Count == 0, string.Join("\r\n", results.Distinct()));
#pragma warning restore 162
        }

        private IList<DependencyDetails> _tracker = null;

        private IList<DependencyDetails> GetReferencedAssemblies(AssemblyName entryPoint)
        {
            AssemblyResolver.Initialize();

            this._tracker = new List<DependencyDetails>();
            this.RecurseDependencies(new DependencyDetails() { Dependency = entryPoint });
            return this._tracker;
        }

        private void RecurseDependencies(DependencyDetails dependencyDetails)
        {
            if (!this._tracker.Any(x =>
            {
                string a = dependencyDetails.Dependency.FullName;
                string b = x.Dependency.FullName;
                return string.Equals(a, b, StringComparison.OrdinalIgnoreCase);
            }))
            {
                try
                {
                    Assembly assembly = Assembly.Load(dependencyDetails.Dependency);
                    dependencyDetails.CanLoad = true;
                    this._tracker.Add(dependencyDetails);
                    if (assembly != null)
                    {
                        foreach (AssemblyName referencedAssemblyName in assembly.GetReferencedAssemblies())
                        {
                            DependencyDetails dependencyDetails1 = new DependencyDetails()
                            {
                                Dependency = referencedAssemblyName,
                                ImmediateDependent = dependencyDetails.Dependency
                            };
                            dependencyDetails1.DependencyChain.AddRange(dependencyDetails.DependencyChain);
                            dependencyDetails1.DependencyChain.Add(dependencyDetails.Dependency);
                            this.RecurseDependencies(dependencyDetails1);
                        }
                    }
                }
                catch
                {
                    dependencyDetails.CanLoad = false;
                    this._tracker.Add(dependencyDetails);
                }
            }
        }
    }

    public class DependencyDetails
    {
        public AssemblyName Dependency { get; set; }
        public AssemblyName ImmediateDependent { get; set; }
        public bool CanLoad { get; set; }
        public List<AssemblyName> DependencyChain = new List<AssemblyName>();
    }

    public static class AssemblyResolver
    {
        private const string AssemblyName = "NSass.Wrapper";
        private const string AssembliesDir = "NSass.Wrapper";

        public static void Initialize()
        {
            string assemblyDir = AppDomain.CurrentDomain.SetupInformation.PrivateBinPath;
            // PrivateBinPath is empty in test scenarios; use BaseDirectory instead
            if (string.IsNullOrEmpty(assemblyDir))
            {
                assemblyDir = AppDomain.CurrentDomain.BaseDirectory;
            }
            assemblyDir = Path.Combine(assemblyDir, AssembliesDir);

            AppDomain.CurrentDomain.AssemblyResolve += (sender, args) =>
            {
                if (args.Name.StartsWith($"{AssemblyName}.proxy,", StringComparison.OrdinalIgnoreCase))
                {
                    if (IsProxyPresent(assemblyDir))
                    {
                        throw new InvalidOperationException(string.Format("Found {0}.proxy.dll which cannot exist. Must instead have {0}.x86.dll and {0}.x64.dll. Check your build settings." + assemblyDir, AssemblyName));
                    }
                    string achitecture = Environment.Is64BitProcess ? "x64" : "x86";
                    string fileName = Path.Combine(assemblyDir, $"{AssemblyName}.{achitecture}.dll");

                    if (!File.Exists(fileName))
                    {
                        throw new InvalidOperationException($"Could not find the wrapper assembly. It may not have been copied to the output directory. Path='{fileName}'");
                    }

                    return Assembly.LoadFile(fileName);
                }

                return null;
            };
        }

        private static bool IsProxyPresent(string assemblyDir)
        {
            return File.Exists(Path.Combine(assemblyDir, AssemblyName + ".proxy.dll"));
        }
    }

}

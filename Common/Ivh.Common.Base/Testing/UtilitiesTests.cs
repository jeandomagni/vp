﻿namespace Ivh.Common.Testing
{
    using System;
    using Base.Utilities.Extensions;
    using Base.Utilities.Helpers;
    using NUnit.Framework;

    [TestFixture]
    public class UtilitiesTests
    {
        [Test]
        public void TestDateTimeMax()
        {
            DateTime? min = DateTime.UtcNow;
            DateTime? mid = min.Value.AddHours(50);
            DateTime? max = min.Value.AddHours(100);
            DateTime? nullValue = null;

            DateTime? result = DateTimeHelper.Max(min, mid, max, nullValue);

            Assert.IsNotNull(result);
            Assert.AreEqual(max.Value, result.Value);
        }
        
        [Test]
        public void TestDateTimeMin()
        {
            DateTime? min = DateTime.UtcNow;
            DateTime? mid = min.Value.AddHours(50);
            DateTime? max = min.Value.AddHours(100);
            DateTime? nullValue = null;

            DateTime? result = DateTimeHelper.Min(min, mid, max, nullValue);

            Assert.IsNotNull(result);
            Assert.AreEqual(min.Value, result.Value);
        }

        [TestCase("1234 1234578903 013120043 000981734",0)]
        [TestCase("000000000SCHULER0000000000RONALD00000000134206660000000314",8)]
        [TestCase("000000000DIEHARD00000000000BRUCE00000002002882871000000001",5)]
        [Test]
        public void TestMod10CheckDigit(string text, int expectedCheckDigit)
        {
            int checkDigit = text.GetMod10CheckDigitJpmc();
            Assert.AreEqual(expectedCheckDigit, checkDigit);
        }
    }
}

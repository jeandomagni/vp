﻿using System;
using Autofac;
using Ivh.Common.DependencyInjection;

namespace Ivh.Common.Testing
{
    public class TestDoubleRegistrationModule : IRegistrationModule
    {
       private readonly Action<ContainerBuilder> _registrationAction;

       public TestDoubleRegistrationModule(Action<ContainerBuilder> registrationAction)
       {
           this._registrationAction = registrationAction;
       }

       public void Register(ContainerBuilder builder)
       {
           this._registrationAction(builder);
       }
    }
}

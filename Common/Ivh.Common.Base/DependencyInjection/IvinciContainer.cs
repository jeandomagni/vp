﻿namespace Ivh.Common.DependencyInjection
{
    using System;
    using Autofac;
    using Extensions;

    /// <summary>
    ///     Singleton Inversion of Control container.
    ///     Does NOT use the double check locking pattern, which is unnessessary in C#
    ///     (see <see cref="http://csharpindepth.com/Articles/General/Singleton.aspx#dcl" />).
    /// </summary>
    public class IvinciContainer
    {
        #region Static

        private static IvinciContainer _instance = new IvinciContainer();

        static IvinciContainer()
        {
            // Explicit static constructor to tell C# compiler
            // not to mark type as beforefieldinit
        }

        public static IvinciContainer Instance
        {
            get { return _instance; }
        }

        public static void ResetForTestingPurposes()
        {
            _instance = new IvinciContainer();
        }

        #endregion Static

        #region Instance

        private readonly ContainerBuilder _builder = new ContainerBuilder();
        private readonly Lazy<IContainer> _container;

        private IvinciContainer()
        {
            // Deliberately private.
            this._container = new Lazy<IContainer>(this.CreateContainer);
        }

        private IContainer CreateContainer()
        {
            return ContainerBuilderExtensions.Build(this._builder);
        }

        public void RegisterComponent(IRegistrationModule mod)
        {
            if (mod != null)
            {
                if (this._container.IsValueCreated)
                {
                    throw new Exception("Registration failed because the container has already been initialized.");
                }

                mod.Register(this._builder);
            }
        }

        public IContainer Container()
        {
            try
            {
                return this._container.Value;
            }
            catch (Exception)
            {
                throw;
            }
        }

        #endregion Instance
    }
}
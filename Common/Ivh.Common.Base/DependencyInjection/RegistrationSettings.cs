﻿namespace Ivh.Common.DependencyInjection
{
    using Base.Utilities.Extensions;
    using VisitPay.Enums;

    public class RegistrationSettings
    {
        public string ClientDataApiUrl { get;set;}
        public string ClientDataApiAppId { get;set;}
        public string ClientDataApiAppKey { get;set;}
        public int ClientDataApiTimeToLive { get;set;}
        
        public ApplicationEnum Application { get; set; }
        public string ApplicationName { get; set; }
        public bool IsSystemApplication => this.Application.IsInCategory(ApplicationEnumCategory.SystemApplication);
        public IvhEnvironmentTypeEnum EnvironmentType { get; set; }

        public string Client { get; set; }
        
        public string ServiceBusConnectionString { get; set; }
        public string ServiceBusVirtualHost => this.Client;//by convention

        public string RabbitMqUserName { get; set; }
        public string RabbitMqPassword { get; set; }
        public bool RabbitMqEnableSsl { get; set; }
        public string RabbitMqSslClientCertPath { get; set; }
        public string RabbitMqSslClientCertPassword { get; set; }
        public string ServiceBusName { get; set; }
        public string ScheduleServiceBusName { get; set; }


        public string RedisEncryptionMaster { get; set; }
        public string RedisEncryptionAuth { get; set; }
        public int RedisMainCacheInstances { get; set; }
        public string DistributedCacheAuthKeyString { get; set; }
        

        public HsFacilityResolutionServiceEnum HsFacilityResolutionService { get; set; }

        public HsBillingSystemResolutionProviderEnum HsBillingSystemResolutionProvider { get; set; }

        public InterestRateConsiderationServiceEnum InterestRateConsiderationService { get; set; }

        public PaymentAllocationModelTypeEnum PaymentAllocationModelType { get; set; }

        public DiscountModelTypeEnum DiscountModelType { get; set; }

        public GuarantorEnrollmentProviderEnum GuarantorEnrollmentProvider { get; set; }

        public RegistrationMatchStringApplicationServiceEnum RegistrationMatchStringApplicationService { get; set; }

        public PresumptiveCharityEvaluationProviderEnum PresumptiveCharityEvaluationProvider { get; set; }

        public string FileStorageIncomingFolder { get; set; }

        public string FileStorageArchiveFolder { get; set; }
    }
}

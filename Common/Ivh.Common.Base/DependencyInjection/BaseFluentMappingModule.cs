﻿namespace Ivh.Common.DependencyInjection
{
    using FluentNHibernate.Cfg;
    using FluentNHibernate.Conventions.Helpers;
    using Ivh.Common.Data;

    public abstract class BaseFluentMappingModule : IFluentMappingModule
    {
        public void MapAssemblies(MappingConfiguration m)
        {
            m.FluentMappings.Conventions.Setup(a => a.Add(AutoImport.Never()));
            m.FluentMappings.AddFromAssembly(this.GetType().Assembly);
        }
    }
}

﻿namespace Ivh.Common.DependencyInjection.Extensions
{
    using Autofac;
    using System;
    using Autofac.Builder;
    using Base.Utilities.Helpers;
    using Interfaces;

    public static class RegistrationBuilderExt
    {
        public static IRegistrationBuilder<TLimit, TActivatorData, TStyle> InstanceWebPerRequestElsePerLifetimeScope<TLimit, TActivatorData, TStyle>(
            this IRegistrationBuilder<TLimit, TActivatorData, TStyle> registration,
            params object[] lifetimeScopeTags)
        {
            return registration.InstanceAutoDetect(w => w.InstancePerRequest(lifetimeScopeTags), d => d.InstancePerLifetimeScope(), lifetimeScopeTags);
        }

        public static IRegistrationBuilder<TLimit, TActivatorData, TStyle> InstanceWebPerRequestElsePerMatchingLifetimeScope<TLimit, TActivatorData, TStyle>(
            this IRegistrationBuilder<TLimit, TActivatorData, TStyle> registration,
            params object[] lifetimeScopeTags)
        {
            return registration.InstanceAutoDetect(w => w.InstancePerRequest(lifetimeScopeTags), d => d.InstancePerMatchingLifetimeScope(lifetimeScopeTags), lifetimeScopeTags);
        }

        public static IRegistrationBuilder<TLimit, TActivatorData, TStyle> InstanceWebPerRequestElseDefault<TLimit, TActivatorData, TStyle>(
            this IRegistrationBuilder<TLimit, TActivatorData, TStyle> registration,
            params object[] lifetimeScopeTags)
        {
            return registration.InstanceAutoDetect(w => w.InstancePerRequest(lifetimeScopeTags), d => d.InstancePerDependency(), lifetimeScopeTags);
        }

        public static IRegistrationBuilder<TLimit, TActivatorData, TStyle> InstanceAutoDetect<TLimit, TActivatorData, TStyle>(
            this IRegistrationBuilder<TLimit, TActivatorData, TStyle> registration,
            Func<IRegistrationBuilder<TLimit, TActivatorData, TStyle>, IRegistrationBuilder<TLimit, TActivatorData, TStyle>> webOverride,
            Func<IRegistrationBuilder<TLimit, TActivatorData, TStyle>, IRegistrationBuilder<TLimit, TActivatorData, TStyle>> defaultOverride,
                params object[] lifetimeScopeTags)
        {
            if (WebHelper.IsWebApp)
            {
                return webOverride(registration);
            }
            else
            {
                return defaultOverride(registration); ;
            }
        }
    }
}

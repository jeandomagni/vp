﻿using System;
namespace Ivh.Common.DependencyInjection.Extensions
{
    using System.Diagnostics;
    using Autofac;
    using Autofac.Builder;
    using Data;

    public static class ContainerBuilderExtensions
    {
        const decimal BytesPerMegabyte = 1024 * 1024;
        public static IContainer Build(this ContainerBuilder containerBuilder)
        {
#if DEBUG
            long before = GC.GetTotalMemory(true);
            Trace.TraceInformation($"Before ContainerBuilder.Build() {(decimal)before / (1024 * 1024)} MB");
#endif
            IContainer container = containerBuilder.Build();
            DataModule.ValidateRepositorySessions(container);
#if DEBUG
            long after = GC.GetTotalMemory(true);
            Trace.TraceInformation($"After ContainerBuilder.Build() {(decimal)after / BytesPerMegabyte} MB");
            Trace.TraceInformation($"ContainerBuilder.Build() Additional Memory {((decimal)after - (decimal)before) / BytesPerMegabyte} MB");
#endif
            return container;
        }
    }
}

﻿namespace Ivh.Common.DependencyInjection
{
    using Autofac;
    using Base.Utilities.Helpers;
    using Data.Connection;

    public class BaseModule : Module
    {
        private readonly IWebHelper _webHelper = new WebHelper();

        protected string GuestPayConnectionName { get; private set; }
        protected string LoggingConnectionName { get; private set; }
        protected string StorageConnectionName { get; private set; }
        protected string QuartzConnectionName { get; private set; }
        protected string CdiEtlConnectionName { get; private set; }
        protected string BalanceTransferConnection { get; private set; }

        protected bool IsWebApplication { get; private set; }

        protected BaseModule()
        {
            this.GuestPayConnectionName = ConnectionNames.GuestPay;
            this.LoggingConnectionName = ConnectionNames.Logging;
            this.StorageConnectionName = ConnectionNames.Storage;
            this.QuartzConnectionName = ConnectionNames.Quartz;
            this.CdiEtlConnectionName = ConnectionNames.CdiEtl;
            this.IsWebApplication = this._webHelper.IsWebApplication;
            this.BalanceTransferConnection = ConnectionNames.BalanceTransfer;
        }
    }
}

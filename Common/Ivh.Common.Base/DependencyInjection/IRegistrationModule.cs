﻿namespace Ivh.Common.DependencyInjection
{
    using Autofac;

    public interface IRegistrationModule
    {
        void Register(ContainerBuilder builder);
    }
}
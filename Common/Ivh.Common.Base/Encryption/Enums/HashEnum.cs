﻿namespace Ivh.Common.Encryption.Enums
{
    public enum HashEnum
    {
        HMACSHA1 = 1,
        HMACSHA256 = 2,
        HMACSHA384 = 3,
        HMACSHA512 = 4,
        PBKDF2 = 5
    }
}
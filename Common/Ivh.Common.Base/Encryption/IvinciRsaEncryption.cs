﻿namespace Ivh.Common.Encryption
{
    using System.Security.Cryptography;

    /// <summary>
    /// FYI: Using RSA is only ment for small amounts of data.
    /// 1024 bit RSA keys, and PKCS # 1 V1.5 padding, you can encrypt 117 bytes at most, with a 2048 RSA key, you can encrypt 245 bytes
    /// </summary>
    public class IvinciRsaEncryption
    {
        public string ExportRsaParameters(RSACryptoServiceProvider keyPair, bool withPrivateKey)
        {
            return keyPair.ToXmlString(withPrivateKey);
        }

        public RSACryptoServiceProvider CreateRsaCryptoServiceProvider()
        {
            // This is how you could generate your own public/private key pair.  
            // As we generate a new random key, we need to set the UseMachineKeyStore flag so that this doesn't
            // crash on IIS. For more information: 
            // http://social.msdn.microsoft.com/Forums/en-US/clr/thread/7ea48fd0-8d6b-43ed-b272-1a0249ae490f?prof=required
            var cspParameters = new CspParameters();
            cspParameters.Flags = CspProviderFlags.UseArchivableKey | CspProviderFlags.UseMachineKeyStore;
            RSACryptoServiceProvider keyPair = new RSACryptoServiceProvider(cspParameters);
            return keyPair;
        }

        public RSACryptoServiceProvider GetRsaCryptoServiceProviderWithXmlString(string xmlString)
        {
            var provider = new RSACryptoServiceProvider();
            provider.FromXmlString(xmlString);
            return provider;
        }
    }
}
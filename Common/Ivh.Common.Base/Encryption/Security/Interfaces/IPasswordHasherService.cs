﻿namespace Ivh.Common.Encryption.Security.Interfaces
{
    using System.Threading.Tasks;
    using Enums;

    public interface IPasswordHasherService
    {
        string HashPassword(string password, int saltLength, int iterations, HashEnum hashEnum);
        Task<bool> VerifyHashedPasswordAsync(string hashedPassword, string providedPassword, int saltLength, int iterations, HashEnum hashEnum);
        bool VerifyHashedPassword(string hashedPassword, string providedPassword, int saltLength, int iterations, HashEnum hashEnum);
    }
}
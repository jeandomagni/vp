﻿namespace Ivh.Common.Encryption
{
    using System.IO;
    using System.Linq;
    using System.Security.Cryptography;
    using System.Text;

    public class AesStLukes
    {
        public static byte[] AesDecryptBytes(byte[] encryptedBytes, string key)
        {
            return AesDecryptBytes(encryptedBytes, AesStLukes.AesCryptDeriveKey(key));
        }

        public static byte[] AesDecryptBytes(byte[] encryptedBytes, byte[] key)
        {
            byte[] iv = new byte[16];

            RijndaelManaged aes = new RijndaelManaged
            {
                Mode = CipherMode.CBC,
                Padding = PaddingMode.PKCS7,
                KeySize = key.Length * 8,
                IV = iv,
                Key = key,
            };
            ICryptoTransform decryptorCryptoTransform = aes.CreateDecryptor();

            byte[] plainBytes;

            using (MemoryStream memoryStream = new MemoryStream())
            {
                using (CryptoStream cryptoStream = new CryptoStream(memoryStream, decryptorCryptoTransform, CryptoStreamMode.Write))
                {
                    cryptoStream.Write(encryptedBytes, 0, encryptedBytes.Length);
                }
                plainBytes = memoryStream.ToArray();
            }

            return plainBytes;
        }

        public static byte[] AesEncryptBytes(byte[] encryptedBytes, string key)
        {
            return AesEncryptBytes(encryptedBytes, AesStLukes.AesCryptDeriveKey(key));
        }

        public static byte[] AesEncryptBytes(byte[] bytesToEncrypt, byte[] key)
        {
            byte[] iv = new byte[16];

            RijndaelManaged aes = new RijndaelManaged
            {
                Mode = CipherMode.CBC,
                Padding = PaddingMode.PKCS7,
                KeySize = key.Length * 8,
                IV = iv,
                Key = key,
            };
            ICryptoTransform decryptorCryptoTransform = aes.CreateEncryptor();

            byte[] plainBytes;

            using (MemoryStream memoryStream = new MemoryStream())
            {
                using (CryptoStream cryptoStream = new CryptoStream(memoryStream, decryptorCryptoTransform, CryptoStreamMode.Write))
                {
                    cryptoStream.Write(bytesToEncrypt, 0, bytesToEncrypt.Length);
                }
                plainBytes = memoryStream.ToArray();
            }

            return plainBytes;
        }

        public static byte[] AesCryptDeriveKey(string plainTest)
        {
            using (var aes = new RijndaelManaged())
            using (SHA1 sha1 = SHA1.Create())
            {
                aes.IV = new byte[16];
                aes.Mode = CipherMode.CBC;
                aes.Padding = PaddingMode.PKCS7;
                aes.KeySize = 128;
                // The following algorithm is taken from:
                // <link>http://msdn.microsoft.com/en-us/library/windows/desktop/aa379916%28v=vs.85%29.aspx</link>
                byte[] baseData = sha1.ComputeHash(Encoding.UTF8.GetBytes(plainTest));
                byte[] buffer1 = new byte[64];
                byte[] buffer2 = new byte[64];

                for (int i = 0; i < 64; i++)
                {
                    buffer1[i] = 0x36;
                    buffer2[i] = 0x5C;
                    if (i < baseData.Length)
                    {
                        buffer1[i] ^= baseData[i];
                        buffer2[i] ^= baseData[i];
                    }
                }

                byte[] buffer1Hash = sha1.ComputeHash(buffer1);
                byte[] buffer2Hash = sha1.ComputeHash(buffer2);

                return buffer1Hash.Concat(buffer2Hash).Take(aes.KeySize / 8).ToArray();
            }
        }

        public static byte[] GetBytes(string str)
        {
            byte[] bytes = new byte[str.Length * sizeof(char)];
            System.Buffer.BlockCopy(str.ToCharArray(), 0, bytes, 0, bytes.Length);
            return bytes;
        }
        public static string GetString(byte[] bytes)
        {
            char[] chars = new char[bytes.Length / sizeof(char)];
            System.Buffer.BlockCopy(bytes, 0, chars, 0, bytes.Length);
            return new string(chars);
        }
    }
}

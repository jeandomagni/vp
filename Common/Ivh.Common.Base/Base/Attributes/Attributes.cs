﻿namespace Ivh.Common.Base.Attributes
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using Utilities.Extensions;

    public abstract class Attributes : IAttributes
    {
        private IDictionary<string, Tuple<string, string>> _values;

        public IDictionary<string, Tuple<string, string>> Values
        {
            get { return this._values; }
            set
            {
                this._values = value;
                Type type = this.GetType();
                IEnumerable<FieldInfo> fields = type.GetFields(BindingFlags.Public | BindingFlags.Instance).Where(field => typeof(IAttributeGroup).IsAssignableFrom(field.FieldType));
                foreach (FieldInfo field in fields)
                {
                    IAttributeGroup attributeGroup = field.GetValue(this) as IAttributeGroup;
                    if (attributeGroup != null)
                    {
                        attributeGroup.Values = value;
                    }
                }
            }
        }

        public bool HasValues
        {
            get { return this.Values.IsNotNullOrEmpty() && this.Values.Values.IsNotNullOrEmpty(); }
        }
        public bool HasKeys
        {
            get { return this.Values.IsNotNullOrEmpty() && this.Values.Keys.IsNotNullOrEmpty(); }
        }
    }
}
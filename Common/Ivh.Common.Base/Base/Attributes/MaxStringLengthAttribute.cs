namespace Ivh.Common.Base.Attributes
{
    using System;

    public class MaxStringLengthAttribute : Attribute
    {
        public int MaxLength { get; set; }
        public MaxStringLengthAttribute(int length) { this.MaxLength = length; }
    }

    public class MaxStringLengthClassAttribute : Attribute
    {
        public MaxStringLengthClassAttribute() { }
    }
}
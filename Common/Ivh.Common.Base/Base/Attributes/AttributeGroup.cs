﻿namespace Ivh.Common.Base.Attributes
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;

    public abstract class AttributeGroup : IAttributeGroup
    {
         private IDictionary<string, Tuple<string, string>> _values;

        public IDictionary<string, Tuple<string, string>> Values
        {
            get { return this._values; }
            set
            {
                this._values = value;
                Type type = this.GetType();
                IEnumerable<FieldInfo> fields = type.GetFields(BindingFlags.Public | BindingFlags.Instance).Where(field => typeof(IAttribute).IsAssignableFrom(field.FieldType));
                foreach (FieldInfo field in fields)
                {
                    IAttribute attribute = field.GetValue(this) as IAttribute;
                    if (attribute != null)
                    {
                        attribute.Values = value;
                    }
                }
            }
        }
        public bool HasKeys
        {
            get
            {
                try
                {
                    Type type = this.GetType().DeclaringType;
                    IEnumerable<FieldInfo> fields = type.GetFields(BindingFlags.Public | BindingFlags.Instance).Where(field => typeof(IAttribute).IsAssignableFrom(field.FieldType));
                    return fields.All(x =>
                    {
                        IAttribute attribute = x.GetValue(this) as IAttribute;
                        return attribute != null && attribute.HasKey;
                    });
                }
                catch (Exception)
                {
                }
                return false;
            }
        }

        public bool HasValues
        {
            get
            {
                try
                {
                    Type type = this.GetType().DeclaringType;
                    IEnumerable<FieldInfo> fields = type.GetFields(BindingFlags.Public | BindingFlags.Instance).Where(field => typeof(IAttribute).IsAssignableFrom(field.FieldType));
                    return fields.All(x =>
                    {
                        IAttribute attribute = x.GetValue(this) as IAttribute;
                        return attribute != null && attribute.HasValue;
                    });
                }
                catch (Exception)
                {
                }
                return false;
            }
        }
    }
}

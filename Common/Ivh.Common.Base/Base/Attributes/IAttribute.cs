﻿namespace Ivh.Common.Base.Attributes
{
    using System;
    using System.Collections.Generic;

    public interface IAttribute
    {
        bool HasValue { get; }
        bool HasKey { get; }
        IDictionary<string, Tuple<string, string>> Values { get; set; }
    }
}

﻿namespace Ivh.Common.Base.Attributes
{
    using System;
    [AttributeUsage(AttributeTargets.Field)]
    public class AttributeKeyAttribute : Attribute
    {
        public string Key { get; private set; }

        public AttributeKeyAttribute(string key)
        {
            this.Key = key;
        }
    }
}

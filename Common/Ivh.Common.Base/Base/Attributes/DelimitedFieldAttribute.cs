﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ivh.Common.Base.Attributes
{
    public class DelimitedFieldAttribute : Attribute
    {
        public int OrdinalPosition { get; set; }
    }
}

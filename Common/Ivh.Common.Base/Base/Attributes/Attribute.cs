﻿namespace Ivh.Common.Base.Attributes
{
    using System;
    using System.Collections.Generic;
    using Utilities.Extensions;

    public class Attribute<TAttribute> : IGenericAttribute<TAttribute>
    {
        public IDictionary<string, Tuple<string,string>> Values { get; set; }
        private bool _valid = false;
        private readonly Lazy<TAttribute> _value;
        private readonly Lazy<string> _description;
        private readonly Lazy<string> _key;

        public TAttribute Value
        {
            get { return this._value.Value; }
        }

        public string Description
        {
            get { return this._description.Value; }
        }

        public bool HasValue
        {
            get
            {
                TAttribute value = this._value.Value;
                return this.HasKey && this._valid;
            }
        }

        public bool HasKey
        {
            get { return this.Values.IsNotNullAndContainsKey(this._key.Value); }
        }

        public Attribute(string key)
        {
            this._key = new Lazy<string>(() => key);
           
            this._value = new Lazy<TAttribute>(() =>
            {
                try
                {
                    if (this.HasKey)
                    {
                        Tuple<string, string> tuple = this.Values[this._key.Value];
                        TAttribute value;
                        if (tuple.Item1.IsNotNullOrEmpty() && tuple.Item1.TryChangeType(out value))
                        {
                            this._valid = true;
                            return value;
                        }
                    }
                }
                catch (Exception)
                {
                    
                }
                this._valid = false;
                return default(TAttribute);
            });

            this._description = new Lazy<string>(() =>
            {
                try
                {
                    if (this.HasKey)
                    {
                        Tuple<string, string> tuple = this.Values[this._key.Value];
                        if (tuple.Item2.IsNotNullOrEmpty())
                        {
                            return tuple.Item2;
                        }
                    }
                }
                catch (Exception)
                {

                }
                this._valid = false;
                return default(string);
            });
        }

        public bool Equals(Attribute<TAttribute> other)
        {
            return other != null
                   && this.HasKey == other.HasKey
                   && this._key.Value == other._key.Value
                   && this.HasValue == other.HasValue
                   && this.Value.Equals(other.Value);

        }
    }

    
}

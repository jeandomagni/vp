﻿namespace Ivh.Common.Base.Attributes
{
    using System;
    using System.Collections.Generic;

    public interface IAttributes
    {
        bool HasKeys { get; }
        bool HasValues { get; }
        IDictionary<string, Tuple<string, string>> Values { get; set; }
    }
}

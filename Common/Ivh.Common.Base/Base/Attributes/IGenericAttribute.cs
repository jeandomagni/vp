﻿namespace Ivh.Common.Base.Attributes
{
    public interface IGenericAttribute<out TAttribute> : IAttribute
    {
        TAttribute Value { get; }
        string Description { get; }
    }
}

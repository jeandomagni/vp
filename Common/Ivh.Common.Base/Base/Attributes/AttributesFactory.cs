﻿namespace Ivh.Common.Base.Attributes
{
    using System;
    using System.Collections.Generic;
    using Newtonsoft.Json;
    using Utilities.Extensions;

    public static class AttributesFactory<TAttributes> where TAttributes : IAttributes, new()
    {
        public static TAttributes GetAttributes(string attributesJson)
        {
            try
            {
                if (attributesJson.IsNotNullOrEmpty())
                {
                    IDictionary<string, Tuple<string, string>> values = JsonConvert.DeserializeObject<Dictionary<string, Tuple<string, string>>>(attributesJson);
                    if (values.IsNotNullOrEmpty())
                    {
                        TAttributes attributes = new TAttributes {Values = values};
                        return attributes;
                    }
                }
            }
            catch (Exception)
            {
                
            }
            return new TAttributes();
        }
    }
}

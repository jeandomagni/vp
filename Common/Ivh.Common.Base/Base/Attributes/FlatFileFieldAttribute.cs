﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ivh.Common.Base.Attributes
{
    public class FlatFileFieldAttribute : Attribute
    {
        /// <summary>
        /// Position in the line to start reading data. This uses ONE based indexing to match industry spec.
        /// </summary>
        public int Index { get; set; }
        public int Length { get; set; }
    }
}

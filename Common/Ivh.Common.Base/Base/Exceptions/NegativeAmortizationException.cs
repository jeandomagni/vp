﻿namespace Ivh.Common.Base.Exceptions
{
    using System;

    public class NegativeAmortizationException : Exception
    {
        private readonly decimal _interestPaymentAmount;
        private readonly decimal _principalPaymentAmount;

        public NegativeAmortizationException(decimal principalPaymentAmount, decimal interestPaymentAmount)
        {
            this._interestPaymentAmount = interestPaymentAmount;
            this._principalPaymentAmount = principalPaymentAmount;
        }

        public decimal InterestPaymentAmount
        {
            get { return this._interestPaymentAmount; }
        }

        public decimal PrincipalPaymentAmount
        {
            get { return this._principalPaymentAmount; }
        }
    }
}
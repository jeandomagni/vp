﻿namespace Ivh.Common.Base.Logging
{
    using System;
    using System.Security.Cryptography;
    using System.Text;

    public static class CorrelationManager
    {
        public static Guid CorrelationId
        {
            get
            {
                if (System.Diagnostics.Trace.CorrelationManager.ActivityId == default(Guid))
                {
                    Initialize();
                }
                return System.Diagnostics.Trace.CorrelationManager.ActivityId;
            }
        }

        public static byte[] CorrelationIdBytes => CorrelationId.ToByteArray();
      
        public static long SessionContextCorrelationId => BitConverter.ToInt64(CorrelationIdBytes, 0);

        public static long RequestContextCorrelationId => BitConverter.ToInt64(CorrelationIdBytes, 8);
        
        public static void Initialize(Guid correlationId = default(Guid), string sessionId = null)
        {
            correlationId = correlationId == default(Guid) ? Guid.NewGuid() : correlationId;

            if (sessionId != null)
            {
                byte[] s = GetHashBytes(sessionId);
                byte[] c = correlationId.ToByteArray();
                byte[] b = new byte[c.Length];
                Array.Copy(s, 0, b, 0, 8);
                Array.Copy(c, 8, b, 8, b.Length - 8);
                correlationId = new Guid(b);
            }

            System.Diagnostics.Trace.CorrelationManager.StartLogicalOperation();
            System.Diagnostics.Trace.CorrelationManager.ActivityId = correlationId;
        }

        private static byte[] GetHashBytes(string value)
        {
            using (SHA256 sha256 = SHA256.Create())
            {
                byte[] hash = null;
                byte[] content = Encoding.UTF8.GetBytes(value);
                if (content.Length != 0)
                {
                    hash = sha256.ComputeHash(content);
                }
                return hash;
            }
        }
    }
}

﻿namespace Ivh.Common.Base.Utilities.Helpers
{
    using System;
    using System.Globalization;
    using System.Threading;

    public class CultureKeeper : IDisposable
    {
        private readonly CultureInfo _previousCultureInfo;

        public CultureKeeper(CultureInfo cultureInfo)
        {
            this._previousCultureInfo = Thread.CurrentThread.CurrentUICulture;
            Thread.CurrentThread.CurrentUICulture = cultureInfo;
        }

        public void Dispose()
        {
            Thread.CurrentThread.CurrentUICulture = this._previousCultureInfo;
        }
    }
}
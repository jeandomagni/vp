﻿namespace Ivh.Common.Base.Utilities.Helpers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using AutoMapper;
    using Extensions;

    public static class AutoMapperHelper
    {
        public static void MapCollection<TSource, TDestination, TKey>(IList<TSource> source, IList<TDestination> destination, Func<TSource, TKey> sourceKey, Func<TDestination, TKey> destinationKey)
        {
            // missing in source. Present in destination => REMOVE FROM DESTINATION
            IList<TKey> removals = destination.Select(destinationKey).Where(x => !destination.Select(destinationKey).Contains(x)).ToList();
            foreach (TKey removal in removals)
            {
                TKey removal1 = removal;
                if (destination.Any(x => destinationKey(x).Equals(removal1)))
                {
                    TDestination dest = destination.First(x => destinationKey(x).Equals(removal1));
                    destination.Remove(dest);
                }
            }

            // present in source. present in destination => COPY SOURCE ONTO DESTINATION
            foreach (TDestination dest in destination)
            {
                if (source.Any(x => sourceKey(x).Equals(destinationKey(dest))))
                {
                    TSource src = source.FirstOrDefault(x => sourceKey(x).Equals(destinationKey(dest)));
                    Mapper.Map<TSource, TDestination>(src, dest);
                }
            }

            // present in source. missing in destination => ADD TO DESTINATION
            IList<TDestination> additions = source.Where(x => !destination.Select(destinationKey).Contains(sourceKey(x))).Select(x => Mapper.Map<TDestination>(x)).ToList();
            destination.AddRange(additions);
        }
    }
}

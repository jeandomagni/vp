﻿namespace Ivh.Common.Base.Utilities.Helpers
{
    using System;
    using System.Collections.Concurrent;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Text.RegularExpressions;
    using System.Web;
    using VisitPay.Strings;

    public static class ReplacementUtility
    {
        private static readonly Regex EmailRegex = new Regex(@"[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,6}", RegexOptions.IgnoreCase);

        public static string Replace(string input, IDictionary<string, string> replacementValues, IDictionary<string, string> additionalValues = null, bool htmlEncode = true)
        {
            if (input == null)
            {
                return null;
            }

            return Replace(input, replacementValues, additionalValues, s => htmlEncode ? HtmlEncode(s) : s, true);
        }

        public static string Replace(string input, IDictionary<string, string> replacementValues, IDictionary<string, string> additionalValues = null, Func<string, string> transform = null, bool compactWhitespace = false)
        {
            if (input == null)
            {
                return null;
            }
            
            StringBuilder stringBuilder = new StringBuilder(input);

            if (replacementValues != null)
            {
                foreach (KeyValuePair<string, string> replacementValue in replacementValues)
                {
                    stringBuilder.Replace(replacementValue.Key, transform != null ? transform(replacementValue.Value) : replacementValue.Value);
                }
            }

            if (additionalValues != null)
            {
                foreach (KeyValuePair<string, string> additionalValue in additionalValues)
                {
                    stringBuilder.Replace(additionalValue.Key, transform != null ? transform(additionalValue.Value) : additionalValue.Value);
                }
            }

            if (compactWhitespace)
            {
                CompactWhitespace(stringBuilder);
            }

            return stringBuilder.ToString();
        }

        public static string RemoveNonAlphaNumeric(string input)
        {
            return ReplaceNonAlphaNumeric(input, string.Empty);
        }

        public static string ReplaceNonAlphaNumeric(string input, string replacement)
        {
            if (input == null)
            {
                return null;
            }
            return Regex.Replace(input, RegexStrings.NonAlphaNumeric, replacement ?? string.Empty, RegexOptions.Compiled);
        }

        public static string RemoveWhiteSpace(string input)
        {
            if (input == null)
            {
                return null;
            }
            return ReplaceWhiteSpace(input, string.Empty);
        }

        public static string ReplaceWhiteSpace(string input, string replacement)
        {
            if (input == null)
            {
                return null;
            } 
            return Regex.Replace(input, RegexStrings.WhiteSpace, replacement ?? string.Empty, RegexOptions.Compiled);
        }
        
        private static void CompactWhitespace(StringBuilder sb)
        {
            if (sb.Length == 0)
                return;

            int start = 0;

            while (start < sb.Length)
            {
                if (char.IsWhiteSpace(sb[start]))
                    start++;
                else
                    break;
            }

            if (start == sb.Length)
            {
                sb.Length = 0;
                return;
            }

            int end = sb.Length - 1;

            while (end >= 0)
            {
                if (Char.IsWhiteSpace(sb[end]))
                    end--;
                else
                    break;
            }

            int dest = 0;
            bool previousIsWhitespace = false;

            for (int i = start; i <= end; i++)
            {
                if (Char.IsWhiteSpace(sb[i]))
                {
                    if (!previousIsWhitespace)
                    {
                        previousIsWhitespace = true;
                        sb[dest] = ' ';
                        dest++;
                    }
                }
                else
                {
                    previousIsWhitespace = false;
                    sb[dest] = sb[i];
                    dest++;
                }
            }

            sb.Length = dest;
        }

        #region encode full

        private static readonly ConcurrentDictionary<string, string> FullyEncodedValues = new ConcurrentDictionary<string, string>();
        
        public static string EncodeFull(string input)
        {
            if (string.IsNullOrEmpty(input))
            {
                return input;
            }

            if (!FullyEncodedValues.ContainsKey(input))
            {
                FullyEncodedValues[input] = EncodeFullValueFactory(input);
            }
            
            return FullyEncodedValues.TryGetValue(input, out string returnValue) ? returnValue : input;
        }

        private static string EncodeFullValueFactory(string input)
        {
            return string.Concat(input.Select(c => string.Concat("&#", (int) c, ";")));
        }

        #endregion

        #region html encode

        private static readonly ConcurrentDictionary<string, string> HtmlEncodedValues = new ConcurrentDictionary<string, string>();

        private static string HtmlEncode(string input)
        {
            if (string.IsNullOrEmpty(input))
            {
                return input;
            }

            if (!HtmlEncodedValues.ContainsKey(input))
            {
                HtmlEncodedValues[input] = HtmlEncodedValueFactory(input);
            }
            
            return HtmlEncodedValues.TryGetValue(input, out string returnValue) ? returnValue : input;
        }

        private static string HtmlEncodedValueFactory(string input)
        {
            if (input.Contains("@"))
            { 
                MatchCollection matches = EmailRegex.Matches(input);
                if (matches.Count > 0)
                {
                    StringBuilder sb = new StringBuilder(HttpUtility.HtmlEncode(input));

                    foreach (Match match in matches)
                    {
                        sb = sb.Replace(match.Value, EncodeFull(match.Value));
                    }

                    return sb.ToString();
                }
            }
            
            return HttpUtility.HtmlEncode(input);
        }

        #endregion
    }
}
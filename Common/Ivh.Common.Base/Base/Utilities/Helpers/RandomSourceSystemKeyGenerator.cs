﻿namespace Ivh.Common.Base.Utilities.Helpers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Security.Cryptography;

    public static class RandomSourceSystemKeyGenerator
    {
        private static readonly char[] AlphaLowerCaseChars = "abcdefghijklmnopqrstuvwxyz".ToCharArray();
        private static readonly char[] AlphaUpperCaseChars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ".ToCharArray();
        private static readonly char[] AlphaChars = AlphaLowerCaseChars.Concat(AlphaUpperCaseChars).ToArray();
        private static readonly char[] NumericChars = "0123456789".ToCharArray();
        private static readonly char[] AllChars = AlphaChars.Concat(NumericChars).ToArray();
        private static readonly byte[] SeedBytes = new byte[4];
        private static readonly Random Random;

        static RandomSourceSystemKeyGenerator()
        {
            new RNGCryptoServiceProvider().GetBytes(SeedBytes);
            Random = new Random(BitConverter.ToInt32(SeedBytes, 0));
        }

        public static string New(string mask, int seed)
        {
            Random seededRandom = new Random(seed);
            return new string(NewInternal(mask, seededRandom).ToArray());
        }
        
        public static string New(string mask)
        {
            return new string(NewInternal(mask).ToArray());
        }

        private static IEnumerable<char> NewInternal(string mask, Random rand = null)
        {
            rand = rand ?? Random;

            foreach (char c in mask)
            {
                switch (c)
                {
                    case 'N':
                        yield return NumericChars[rand.Next(0, NumericChars.Length - 1)];
                        break;
                    case 'A':
                        yield return AlphaUpperCaseChars[rand.Next(0, AlphaUpperCaseChars.Length - 1)];
                        break;
                    case 'a':
                        yield return AlphaLowerCaseChars[rand.Next(0, AlphaLowerCaseChars.Length - 1)];
                        break;
                    case 'Z':
                    case 'z':
                        yield return AlphaChars[rand.Next(0, AlphaChars.Length - 1)];
                        break;
                    case '*':
                        yield return AllChars[rand.Next(0, AllChars.Length - 1)];
                        break;
                    default:
                        yield return c;
                        break;
                }
            }
        }
    }
}
﻿namespace Ivh.Common.Base.Utilities.Helpers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Linq.Expressions;
    using System.Reflection;
    using Extensions;

    /// <summary>
    /// T must be marked as Serializable
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class Compare<T> where T : class, new()
    {
        public readonly T Original = null;
        public T Modified = null;

        private readonly IList<MemberInfo> _differences = new List<MemberInfo>();

        public static Compare<T> InitializeWith(T original)
        {
            return new Compare<T>(original);
        }

        public static CompareList<T> InitializeWith(IEnumerable<T> originals)
        {
            return CompareList<T>.InitializeWith(originals);
        }

        private Compare(T original)
        {
            if (original != default(T))
            {
                this.Original = original.DeepCopy();
            }
        }

        public void CompareTo(T modified)
        {
            this.Modified = modified;
            if (this.Original != default(T) && this.Modified != default(T))
            {
                foreach (FieldInfo memberInfo in typeof(T).GetFields())
                {
                    object originalValue = memberInfo.GetValue(this.Original);
                    object modifiedValue = memberInfo.GetValue(this.Modified);

                    if ((originalValue == null ^ modifiedValue == null) || ((originalValue != null) && !modifiedValue.Equals(originalValue)))
                    {
                        this._differences.Add(memberInfo);
                    }
                }

                foreach (PropertyInfo memberInfo in typeof(T).GetProperties())
                {
                    object originalValue = memberInfo.GetValue(this.Original);
                    object modifiedValue = memberInfo.GetValue(this.Modified);

                    if ((originalValue == null ^ modifiedValue == null) || ((originalValue != null) && !modifiedValue.Equals(originalValue)))
                    {
                        this._differences.Add(memberInfo);
                    }
                }
            }
        }

        public bool HasChanged(Expression<Func<T, object>> memberExpression)
        {
            if (this.Original == default(T) ^ this.Modified == default(T))
            {
                return true;
            }

            string name = memberExpression.GetPropertyName();
            return this._differences.IsNotNullOrEmpty() && this._differences.Any(x => x.Name == name);
        }
    }
}

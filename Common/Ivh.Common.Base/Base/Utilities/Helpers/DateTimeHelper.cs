﻿namespace Ivh.Common.Base.Utilities.Helpers
{
    using System;
    using System.Linq;

    public static class DateTimeHelper
    {
        public static DateTime? Max(params DateTime?[] values)
        {
            return values.Where(x => x.HasValue).OrderByDescending(x => x.Value).FirstOrDefault();
        }
        
        public static DateTime? Min(params DateTime?[] values)
        {
            return values.Where(x => x.HasValue).OrderBy(x => x.Value).FirstOrDefault();
        }

        public static DateTime FromValues(string month, string day, string year)
        {
            DateTime date;
            string dobString = $"{month}/{day}/{year}";

            if (DateTime.TryParse(dobString, out date))
            {
                return date;
            }

            return default(DateTime);
        }

        public static TimeSpan TimeZoneOffset(string timeZone = "Mountain Standard Time")
        {
            TimeZoneInfo foundTimeZoneInfo = TimeZoneInfo.FindSystemTimeZoneById(!string.IsNullOrEmpty(timeZone) ? timeZone : "Mountain Standard Time");
            return foundTimeZoneInfo.BaseUtcOffset;
        }
    }
}
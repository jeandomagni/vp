﻿namespace Ivh.Common.Base.Utilities.Helpers
{
    using System;
    using System.Collections.Generic;
    using VisitPay.Strings;

    public class TimeZoneHelper
    {
        public static class TimeZones
        {
            public const string Utc = "UTC";
            public const string EasternStandardTime = "Eastern Standard Time";
            public const string CentralStandardTime = "Central Standard Time";
            public const string MountainStandardTime = "Mountain Standard Time";
            public const string PacificStandardTime = "Pacific Standard Time";
            public const string Operations = MountainStandardTime;

            public static IDictionary<string, Tuple<string, string>> Abbreviations = new Dictionary<string, Tuple<string, string>>();
            static TimeZones()
            {
                Abbreviations.Add(Utc, new Tuple<string, string>("UTC", "UTC"));
                Abbreviations.Add(EasternStandardTime, new Tuple<string, string>("EST", "EDT"));
                Abbreviations.Add(CentralStandardTime, new Tuple<string, string>("CST", "CDT"));
                Abbreviations.Add(MountainStandardTime, new Tuple<string, string>("MST", "MDT"));
                Abbreviations.Add(PacificStandardTime, new Tuple<string, string>("PST", "PDT"));
            }
        }

        public class TimeZone
        {
            public TimeZone(string timeZone)
            {
                timeZone = TimeZones.EasternStandardTime;
                this.TimeZoneInfo = TimeZoneInfo.FindSystemTimeZoneById(!string.IsNullOrEmpty(timeZone) ? timeZone : TimeZones.Operations);
                this.StandardAbbreviation = TimeZones.Abbreviations.ContainsKey(this.TimeZoneInfo.StandardName) ? TimeZones.Abbreviations[this.TimeZoneInfo.StandardName].Item1 : TimeZones.Abbreviations[TimeZones.Utc].Item1;
                this.DaylightAbbreviation = TimeZones.Abbreviations.ContainsKey(this.TimeZoneInfo.StandardName) ? TimeZones.Abbreviations[this.TimeZoneInfo.StandardName].Item2 : TimeZones.Abbreviations[TimeZones.Utc].Item2;
            }

            public TimeZoneInfo TimeZoneInfo { get; }
            public DateTime Date => this.Now.Date;
            public DateTime Now => DateTime.UtcNow.Add(this.TimeZoneInfo.BaseUtcOffset);

            public string StandardAbbreviation { get; }
            public string DaylightAbbreviation { get; }

            public DateTime ToLocalDateTime(DateTime dateTime)
            {
                return TimeZoneHelper.Convert(dateTime, TimeZoneInfo.Utc, this.TimeZoneInfo);
            }

            public DateTime ToUniversalDateTime(DateTime dateTime)
            {
                return TimeZoneHelper.Convert(dateTime, this.TimeZoneInfo, TimeZoneInfo.Utc);
            }

            public string ToLocalDateTimeString(DateTime dateTime, string format = null)
            {
                DateTime dateTime2 = this.ToLocalDateTime(dateTime);
                string abbrev = this.GetCurrentTimeZoneAbbreviation(dateTime2, this.TimeZoneInfo);
                return $"{dateTime2.ToString(format ?? Format.DateTimeFormat)} {abbrev}";
            }

            public string ToUniversalDateTimeString(DateTime dateTime, string format = null)
            {
                DateTime dateTime2 = this.ToUniversalDateTime(dateTime);
                string abbrev = this.GetCurrentTimeZoneAbbreviation(dateTime2, this.TimeZoneInfo);
                return $"{dateTime2.ToString(format ?? Format.DateTimeFormat)} {abbrev}";
            }

            public bool IsDaylightSavingTime(DateTime dateTime)
            {
                return this.TimeZoneInfo.IsDaylightSavingTime(dateTime);
            }

            public string GetCurrentTimeZoneAbbreviation(DateTime dateTime, TimeZoneInfo tzi)
            {
                return this.IsDaylightSavingTime(dateTime) ? this.DaylightAbbreviation : this.StandardAbbreviation;
            }
        }

        public TimeZoneHelper(
            string clientTimeZone)
        {
            this.Utc = new TimeZone(TimeZoneInfo.Utc.StandardName);
            this.Server = new TimeZone(TimeZoneInfo.Local.StandardName);
            this.Client = new TimeZone(clientTimeZone);
            this.Operations = new TimeZone(TimeZoneHelper.TimeZones.Operations);
        }

        public TimeZoneHelper(
            string clientTimeZone,
            string operationsTimeZone)
        {
            this.Utc = new TimeZone(TimeZoneInfo.Utc.StandardName);
            this.Server = new TimeZone(TimeZoneInfo.Local.StandardName);
            this.Client = new TimeZone(clientTimeZone);
            this.Operations = new TimeZone(operationsTimeZone);
        }

        public TimeZone Utc { get; private set; }
        public TimeZone Server { get; private set; }
        public TimeZone Client { get; private set; }
        /// <summary>
        /// This time zone 
        /// </summary>
        public TimeZone Operations { get; private set; }

        public DateTime UtcToServer(DateTime dateTime)
        {
            return Convert(dateTime, this.Utc, this.Server);
        }

        public DateTime ServerToUtc(DateTime dateTime)
        {
            return Convert(dateTime, this.Server, this.Utc);
        }

        public DateTime UtcToClient(DateTime dateTime)
        {
            return Convert(dateTime, this.Utc, this.Client);
        }

        public DateTime ClientToUtc(DateTime dateTime)
        {
            return Convert(dateTime, this.Client, this.Utc);
        }

        public DateTime UtcToOperations(DateTime dateTime)
        {
            return Convert(dateTime, this.Utc, this.Operations);
        }

        public DateTime OperationsToUtc(DateTime dateTime)
        {
            return Convert(dateTime, this.Operations, this.Utc);
        }

        public DateTime ClientToOperations(DateTime dateTime)
        {
            return Convert(dateTime, this.Client, this.Operations);
        }

        public DateTime OperationsToClient(DateTime dateTime)
        {
            return Convert(dateTime, this.Operations, this.Client);
        }

        public static DateTime Convert(DateTime dateTime, TimeZone from, TimeZone to)
        {
            return Convert(dateTime, from.TimeZoneInfo, to.TimeZoneInfo);
        }

        public static DateTime Convert(DateTime dateTime, TimeZoneInfo from, TimeZoneInfo to)
        {
            return dateTime;
            //return TimeZoneInfo.ConvertTime(dateTime, from, to);
        }
    }
}
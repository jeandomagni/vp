﻿namespace Ivh.Common.Base.Utilities.Helpers
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Linq;
    using System.Reflection;
    using Extensions;

    public static class StackUtility
    {
        public static long GetStackHash()
        {
            return GetStackHash(1);
        }

        public static long GetStackHash(int skip)
        {
            ++skip;//ignore this frame
            long[] methodHashes = GetStack(skip)
                .ToArray()
                .Select(x => (long)x.GetHashCode())
                .ToArray();

            long stackHash = methodHashes.LongLength;
            if (stackHash == 0)
                return 0;

            for (int i = 0; i < methodHashes.Length; ++i)
            {
                stackHash = unchecked(stackHash * 314159 + methodHashes[i]);
            }
            return stackHash;
        }

        public static IList<string> GetStack()
        {
            return GetStack(1);
        }

        public static IList<string> GetStack(int skip)
        {
            ++skip;//ignore this frame
            StackFrame[] stackFrames = new StackTrace().GetFrames();
            if (stackFrames.IsNullOrEmpty())
                return new List<string>();

            IList<string> returnList = new List<string>(stackFrames.Length - skip);
            
            for (int i = 0 + skip; i < stackFrames.Length; i++)
            {
                MethodBase methodBase = stackFrames[i].GetMethod();
                if (methodBase != null && methodBase.ReflectedType != null)
                {
                    //may want to whitelist or black list assemblies so as not to include System.* or limit to ivh.*
                    FileVersionInfo assemblyInfo = FileVersionInfo.GetVersionInfo(methodBase.ReflectedType.Assembly.Location);
                    if (assemblyInfo.ProductName.StartsWith("Ivh."))
                    {
                        string parameters = string.Empty;
                        ParameterInfo[] parameterInfos = methodBase.GetParameters();
                        if (parameterInfos.IsNotNullOrEmpty())
                        {
                            parameters = string.Join(", ", methodBase.GetParameters().Select(x => x.ParameterType.FullName));
                        }
                        if (methodBase.ReflectedType != null)
                        {
                            returnList.Add(string.Format("{0}.{1}.{2}({3})", methodBase.ReflectedType.Namespace, methodBase.ReflectedType.Name, methodBase.Name, parameters));
                        }
                        else
                        {
                            returnList.Add(string.Format("{0}({1})", methodBase.Name, parameters));
                        }
                    }
                }
            }
            return returnList;
        }
    }
}

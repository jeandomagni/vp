﻿namespace Ivh.Common.Base.Utilities.Helpers
{
    using System;
    using System.Diagnostics;

    public class Timer : IDisposable
    {
        private readonly Action<Stopwatch> _beforeStart;
        private readonly Action<Stopwatch> _afterStop;
        public readonly Stopwatch Stopwatch = new Stopwatch();

        public static Timer Start(Action<Stopwatch> beforeStart, Action<Stopwatch> afterStop)
        {
            return new Timer(beforeStart, afterStop);
        }

        public static Timer Start(Action<Stopwatch> afterStop)
        {
            return new Timer(afterStop);
        }

        public Timer(Action<Stopwatch> beforeStart, Action<Stopwatch> afterStop)
        {
            this._beforeStart = beforeStart;
            this._afterStop = afterStop;
            this.Start();
        }

        public Timer(Action<Stopwatch> onStop)
        {
            this._afterStop = onStop;
            this.Start();
        }

        public void Dispose()
        {
            this.Stop();
        }

        private void Start()
        {
            this._beforeStart?.Invoke(this.Stopwatch);
            this.Stopwatch.Start();
        }

        private void Stop()
        {
            this.Stopwatch.Stop();
            this._afterStop.Invoke(this.Stopwatch);
        }
    }
}

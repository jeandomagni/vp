﻿namespace Ivh.Common.Base.Utilities.Helpers
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using Attributes;

    public class MaxLengthTruncator
    {
        public static void TruncateProperties<TValue>(TValue value)
        {
            HashSet<object> circularReferencePreventor = new HashSet<object>() { value };
            Queue<Tuple<PropertyInfo, object, bool>> myQueue = new Queue<Tuple<PropertyInfo, object, bool>>();

            var roottype = value.GetType();
            //Lets assume value is a parent and we need to check all of the children too
            var rootProperties = roottype.GetProperties(BindingFlags.Public | BindingFlags.Instance);
                //.Where(prop => Attribute.IsDefined((MemberInfo) prop, typeof(MaxStringLengthAttribute)));
            foreach (var property in rootProperties)
            {
                myQueue.Enqueue(new Tuple<PropertyInfo, object, bool>(property, value, false));
            }

            while (myQueue.Count > 0)
            {
                var current = myQueue.Dequeue();
                
                var atts = current.Item1.GetCustomAttributes(typeof(MaxStringLengthAttribute), true);
                object propvalue = null;
                //If it came from a list just use the item2
                if (current.Item3)
                {
                    propvalue = current.Item2;
                }
                else
                {
                    propvalue = current.Item1.GetValue(current.Item2, null);
                }

                string currentString = propvalue as string;
                if (currentString != null)
                {
                    var myAttrib = atts.FirstOrDefault() as MaxStringLengthAttribute;
                    if (myAttrib != null && currentString.Length > myAttrib.MaxLength)
                    {
                        current.Item1.SetValue(current.Item2, Convert.ChangeType(currentString.Substring(0, myAttrib.MaxLength), current.Item1.PropertyType), null);
                    }
                }

                var followClass = current.Item1.PropertyType.GetCustomAttributes(typeof(MaxStringLengthClassAttribute), true).FirstOrDefault();
                var testingIfList = propvalue is IList;
                if ( propvalue != null && !circularReferencePreventor.Contains(propvalue) && (followClass != null || testingIfList ))//Should I limit this?  Might need another attribute if so
                {
                    circularReferencePreventor.Add(propvalue);
                    var moreType = propvalue.GetType();
                    var moreProperties = moreType.GetProperties(BindingFlags.Public | BindingFlags.Instance);
                    
                    foreach (var property in moreProperties)
                    {
                        if (testingIfList)
                        {
                            foreach (object eachItemRatherThanList in propvalue as IList)
                            {
                                myQueue.Enqueue(new Tuple<PropertyInfo, object, bool>(property, eachItemRatherThanList, true));
                            }
                        }
                        //Only want to queue if the property is LengthClass or string
                        else
                        {
                            myQueue.Enqueue(new Tuple<PropertyInfo, object,bool>(property, propvalue, false));
                        }
                    }
                }
            }
            
            
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ivh.Common.Base.Utilities.Helpers
{
    using System.IO;

    public class StringReaderWrapper : StringReader
    {
        private Func<int, string, string> _readLine;
        private int _rowIndex = -1;
        public StringReaderWrapper(string s, Func<int, string, string> readLine)
            : base(s)
        {
            this._readLine = readLine;
        }

        public override string ReadLine()
        {
            this._rowIndex++;
            if (this._readLine == null)
            {
                return base.ReadLine();
            }
            else
            {
                return this._readLine(this._rowIndex, base.ReadLine());
            }
        }
    }
}

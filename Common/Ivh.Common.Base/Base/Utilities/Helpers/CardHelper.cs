﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ivh.Common.Base.Utilities.Helpers
{
    using System.Text.RegularExpressions;
    using Enums;
    using VisitPay.Enums;

    public class CardHelper
    {
        public const string RegexAmericanExpress = "^3[47][0-9]{13}$";
        public const string RegexDinersClub = "^3(?:0[0-5]|[68][0-9])[0-9]{11}$";
        public const string RegexDiscover = "^6(?:011|5[0-9]{2})[0-9]{12}$";
        public const string RegexJcb = "^(?:2131|1800|35\\d{3})\\d{11}$";
        public const string RegexMasterCard = "^5[1-5][0-9]{14}$";
        public const string RegexVisa = "^4[0-9]{12}(?:[0-9]{3})?$";

        public static PaymentMethodTypeEnum GetPaymentMethodType(string cardNumber)
        {
            if (Regex.IsMatch(cardNumber, RegexVisa, RegexOptions.Compiled))
                return PaymentMethodTypeEnum.Visa;
            if (Regex.IsMatch(cardNumber, RegexMasterCard, RegexOptions.Compiled))
                return PaymentMethodTypeEnum.Mastercard;
            if (Regex.IsMatch(cardNumber, RegexAmericanExpress, RegexOptions.Compiled))
                return PaymentMethodTypeEnum.AmericanExpress;
            if (Regex.IsMatch(cardNumber, RegexDiscover, RegexOptions.Compiled))
                return PaymentMethodTypeEnum.Discover;
            
            throw new ArgumentOutOfRangeException("cardNumber", "Invalid Card Number");
        }
    }
}

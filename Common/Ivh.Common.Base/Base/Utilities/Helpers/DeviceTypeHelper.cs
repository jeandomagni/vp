﻿namespace Ivh.Common.Base.Utilities.Helpers
{
    using System.Text.RegularExpressions;
    using Enums;
    using VisitPay.Enums;
    using VisitPay.Strings;

    public static class DeviceTypeHelper
    {
        public static DeviceTypeEnum GetDeviceType(string userAgent)
        {
            if (!string.IsNullOrEmpty(userAgent))
            {
                if (Regex.IsMatch(userAgent, RegexStrings.UserAgentMobile, RegexOptions.IgnoreCase))
                {
                    if (Regex.IsMatch(userAgent, RegexStrings.UserAgentTablet, RegexOptions.IgnoreCase))
                    {
                        return DeviceTypeEnum.Tablet;
                    }
                    return DeviceTypeEnum.Mobile;
                }
                return DeviceTypeEnum.Web;
            }
            return DeviceTypeEnum.Unknown;
        }
    }
}

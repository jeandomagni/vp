﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ivh.Common.Base.Utilities.Helpers
{
    using System.Globalization;
    using Extensions;

    public static class HtmlHelper
    {
        public static string HtmlEncapsulate(string htmlSection, string style = null)
        {
            htmlSection = htmlSection.Trim();
            bool html = false;
            bool body = false;
            if (htmlSection.StartsWith("<html", true, CultureInfo.CurrentUICulture))
                return htmlSection;
            else
                html = true;

            body = !htmlSection.StartsWith("<body", true, CultureInfo.CurrentUICulture);

            string styleAttribute = string.Empty;
            if (body && style.IsNotNullOrEmpty())
            {
                if (!style.StartsWith("style", true, CultureInfo.CurrentUICulture))
                {
                    styleAttribute = string.Format(" style=\"{0}\"", style);
                }
                else
                {
                    styleAttribute = " " + style;
                }
            }

            return (html ? "<html>" : string.Empty)
                   + (body ? string.Format("<body{0}>", styleAttribute) : string.Empty)
                   + htmlSection
                   + (body ? "</body>" : string.Empty)
                   + (html ? "</html>" : string.Empty);
        }
    }
}
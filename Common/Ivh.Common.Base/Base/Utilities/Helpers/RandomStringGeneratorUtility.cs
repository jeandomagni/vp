﻿namespace Ivh.Common.Base.Utilities.Helpers
{
    using System;
    using System.Linq;

    /// <summary>
    /// Random string generator utility class
    /// </summary>
    public class RandomStringGeneratorUtility
    {
        
        private static Random random =  new Random();
        /// <summary>
        /// Gets the random string.
        /// </summary>
        /// <param name="length">The length.</param>
        /// <returns></returns>
        public static string GetRandomString(int length)
        {
            var chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";

            var result = new string(
                Enumerable.Repeat(chars, length)
                          .Select(s => s[random.Next(s.Length)])
                          .ToArray());
            return result;
        }

        /// <summary>
        /// Gets the random string by eliminating the alpha "O" and numeric "0" for temporary passwords.
        /// </summary>
        /// <param name="length">The length.</param>
        /// <returns></returns>
        public static string GetRandomStringForTempPassword(int length)
        {
            var chars = "ABCDEFGHIJKLMNPQRSTUVWXYZ123456789";

            var result = new string(
                Enumerable.Repeat(chars, length)
                          .Select(s => s[random.Next(s.Length)])
                          .ToArray());
            return result;
        }

        /// <summary>
        /// Gets the random number string.
        /// </summary>
        /// <param name="length">The length.</param>
        /// <returns></returns>
        public static string GetRandomNumberString(int length)
        {
            var chars = "0123456789";
            var result = new string(
                Enumerable.Repeat(chars, length)
                          .Select(s => s[random.Next(s.Length)])
                          .ToArray());
            return result;
        }
    }
}

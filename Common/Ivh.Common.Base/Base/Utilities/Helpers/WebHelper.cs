﻿namespace Ivh.Common.Base.Utilities.Helpers
{
    using System.Web;
    using System.Web.Hosting;
    using Extensions;

    public class WebHelper : IWebHelper
    {
        public static readonly bool IsWebApp = HostingEnvironment.IsHosted && HttpRuntime.AppDomainAppId.IsNotNullOrEmpty();
        
        public bool IsWebApplication => IsWebApp;
    }

    public interface IWebHelper
    {
        bool IsWebApplication { get; }
    }
}

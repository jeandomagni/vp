﻿namespace Ivh.Common.Base.Utilities.Helpers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Linq.Expressions;

    public class CompareList<T> : List<Compare<T>> where T : class, new()
    {
        public static CompareList<T> InitializeWith(IEnumerable<T> originals)
        {
            CompareList<T> compareList = new CompareList<T>();
            foreach (T original in originals)
            {
                compareList.Add(Compare<T>.InitializeWith(original));
            }

            return compareList;
        }

        private CompareList()
        {
        }

        public void CompareTo<TKey>(IEnumerable<T> modifieds, Func<T, TKey> keySelector)
        {
            IEnumerable<T> enumerable = modifieds as T[] ?? modifieds.ToArray();
            foreach (T modified in enumerable)
            {
                List<Compare<T>> list = this.Where(x => x.Original != default(T)).ToList();

                if (list.Any(x => keySelector(modified).Equals(keySelector(x.Original))))
                {
                    foreach (Compare<T> compare in list.Where(x => keySelector(modified).Equals(keySelector(x.Original))))
                    {
                        compare.CompareTo(modified);
                    }
                }
                else
                {
                    //there was no original. new
                    Compare<T> original = Compare<T>.InitializeWith(default(T));
                    original.CompareTo(modified);
                    this.Add(original);
                }
            }
        }

        public IEnumerable<Compare<T>> HasChanged(Expression<Func<T, object>> memberExpression)
        {
            return this.Where(x => x.HasChanged(memberExpression));
        }
    }
}
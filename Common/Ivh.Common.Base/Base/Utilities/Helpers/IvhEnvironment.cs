﻿namespace Ivh.Common.Base.Utilities.Helpers
{
    using System;
    using Configuration;
    using VisitPay.Enums;

    public static class IvhEnvironment
    {
        private static IvhEnvironmentTypeEnum? _currentEnvironmentType;
        private static readonly object LockEnvironmentType = new object();

        public static IvhEnvironmentTypeEnum CurrentEnvironmentType
        {
            get
            {
                if (!_currentEnvironmentType.HasValue)
                {
                    lock (LockEnvironmentType)
                    {
                        if (_currentEnvironmentType.HasValue)
                        {
                            return _currentEnvironmentType ?? IvhEnvironmentTypeEnum.Unknown;
                        }

                        const string environmentTypeKey = "EnvironmentType";
                        string environmentTypeString = AppSettingsProvider.Instance.Get(environmentTypeKey);

                        if (Enum.TryParse(environmentTypeString, out IvhEnvironmentTypeEnum environmentType))
                        {
                            _currentEnvironmentType = environmentType;
                        }
                    }
                }

                return _currentEnvironmentType ?? IvhEnvironmentTypeEnum.Unknown;
            }
        }

        public static bool IsProduction
        {
            get { return CurrentEnvironmentType == IvhEnvironmentTypeEnum.Pci; }
        }

        public static bool IsNonProduction
        {
            get { return CurrentEnvironmentType != IvhEnvironmentTypeEnum.Pci; }
        }

        public static bool IsQualityAssurance
        {
            get { return CurrentEnvironmentType == IvhEnvironmentTypeEnum.Qa; }
        }

        public static bool IsDevelopment
        {
            get { return CurrentEnvironmentType == IvhEnvironmentTypeEnum.Dev; }
        }

        public static bool IsDevelopmentOrQualityAssurance
        {
            get { return IsQualityAssurance || IsDevelopment; }
        }
    }
}

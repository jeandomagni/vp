﻿namespace Ivh.Common.Base.Utilities.Helpers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading;
    
    public static class ConditionalWaiter
    {

        public static bool Wait(Func<bool> exitWhenTrue, Func<bool> jobProgress, TimeSpan timeout, TimeSpan pause)
        {
            bool timedOut = false;

            if (exitWhenTrue == null) throw new ArgumentNullException("exitWhenTrue");
            if (jobProgress == null) throw new ArgumentNullException("jobProgress");

            while (!jobProgress())
            {
                // when there has been no progress, start the timeout
                DateTime startTime = DateTime.UtcNow;
                while (!exitWhenTrue())
                {
                    Thread.Sleep(pause);
                    timedOut = (DateTime.UtcNow - startTime) > timeout;
                    if (timedOut)
                    {
                        break;
                    }
                    // if the job progresses while we are waiting on it, then reset
                    if (jobProgress())
                    {
                        break;
                    }
                };
            }


            return !timedOut;
        }

        public static bool Wait(Func<bool> exitWhenTrue, TimeSpan timeout, TimeSpan pause)
        {
            DateTime startTime = DateTime.UtcNow;
            bool timedOut = false;
            while (exitWhenTrue != null && !exitWhenTrue())
            {
                Thread.Sleep(pause);
                timedOut = (DateTime.UtcNow - startTime) > timeout;
                if (timedOut)
                {
                    break;
                }
            };

            return !timedOut;
        }

        public static bool Wait(IList<Func<bool>> exitWhenTrue, TimeSpan timeout, TimeSpan pause)
        {
            DateTime startTime = DateTime.UtcNow;
            bool timedOut = false;
            while (timedOut || !exitWhenTrue.All(x => x()))
            {
                Thread.Sleep(pause);
                timedOut = (DateTime.UtcNow - startTime) > timeout;
            };

            return !timedOut;
        }
    }
}


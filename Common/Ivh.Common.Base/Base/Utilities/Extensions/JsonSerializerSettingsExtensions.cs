﻿namespace Ivh.Common.Base.Utilities.Extensions
{
    using Newtonsoft.Json;

    public static class JsonSerializerSettingsExtensions
    {
        /// <summary>
        /// serializes Enums as their name, rather than value.
        /// </summary>
        /// <param name="settings"></param>
        /// <returns></returns>
        public static JsonSerializerSettings WithStringEnumConverter(this JsonSerializerSettings settings)
        {
            settings.Converters.Add(new Newtonsoft.Json.Converters.StringEnumConverter());
            return settings;
        }
    }
}

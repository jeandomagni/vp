﻿namespace Ivh.Common.Base.Utilities.Extensions
{
    using System;
    using System.Collections.Generic;
    using VisitPay.Strings;

    public static class DateTimeExt
    {
        /// <summary>
        ///     This will convert a datetime object to preferred timezone datetimeoffset
        /// </summary>
        /// <param name="dateTime"></param>
        /// <param name="timeZone"></param>
        /// <returns></returns>
        public static DateTime ToDateTime(this DateTime dateTime, string timeZone)
        {
            return TimeZoneInfo.ConvertTimeBySystemTimeZoneId(dateTime.ToUniversalTime(), timeZone);
        }

        public static DateTime ToLocalDateTime(this DateTime clientDateTime, TimeSpan timezoneOffset)
        {
            return clientDateTime.Add(timezoneOffset);
        }
        
        /// <summary>
        /// This converts from local datetimeoffset (Something ideally created with ToLocalDateTimeOffset) back to client time.
        /// </summary>
        public static DateTime ToUtc(this DateTime localDateTime, TimeSpan clientTimeZoneOffSet)
        {
            return localDateTime.Add(-clientTimeZoneOffSet);
        }

        /// <summary>
        /// Convert the datetime to 10 milliseconds before the end of the day
        /// </summary>
        /// <param name="dateTime"></param>
        /// <returns></returns>
        public static DateTime ToEndOfDay(this DateTime dateTime)
        {
            return new DateTime(dateTime.Year, dateTime.Month, dateTime.Day).AddDays(1).AddMilliseconds(-10);
        }

        public static int DaysInYear(this DateTime dateTime)
        {
            return DateTime.IsLeapYear(dateTime.Year) ? 366 : 365;
        }

        public static bool IsDateOnly(this DateTime? dateTime) => dateTime.HasValue ? dateTime.Value.IsDateOnly() : false;

        public static bool IsDateOnly(this DateTime dateTime) => dateTime.TimeOfDay.Ticks == 0;

        private static IEnumerable<DateTime> Through(this DateTime dateTime, DateTime throughDateTime, Func<DateTime,DateTime> advancementFunc)
        {
            DateTime date = dateTime;
            while (date.Date < throughDateTime.Date)
            {
                yield return date;

                date = advancementFunc(date);
            }

            yield return date;
        }

        public static IEnumerable<DateTime> ThroughEachMonthUntil(this DateTime dateTime, DateTime throughDateTime, Func<DateTime, DateTime> advancementFunc = null)
        {
            return dateTime.Through(throughDateTime, (d) => d.AddMonths(1));
        }

        public static IEnumerable<DateTime> ThroughEachDayUntil(this DateTime dateTime, DateTime throughDateTime, Func<DateTime, DateTime> advancementFunc = null)
        {
            return dateTime.Through(throughDateTime, (d) => d.AddDays(1));
        }

        public static DateTime ToPaymentDueDateForDay(this DateTime guessPaymentDueDate, int paymentDueDay)
        {
            int nextPaymentDueDay = paymentDueDay.ToValidPaymentDueDay(DateTime.DaysInMonth(guessPaymentDueDate.Year, guessPaymentDueDate.Month));

            return new DateTime(guessPaymentDueDate.Year, guessPaymentDueDate.Month, nextPaymentDueDay);
        }

        #region IsLastDayOfMonth

        /// <summary>
        ///     Is the date the last day of the month
        /// </summary>
        /// <param name="date">Date to verify</param>
        /// <returns>True of false</returns>
        public static bool IsLastDayOfMonth(this DateTime date)
        {
            return date.AddDays(1).Day == 1;
        }

        /// <summary>
        ///     Is the date the last day of the month
        /// </summary>
        /// <param name="date">Date to verify</param>
        /// <returns>True of false</returns>
        public static bool IsLastDayOfMonth(this DateTime? date)
        {
            return date.HasValue && date.Value.IsLastDayOfMonth();
        }

        #endregion

        #region ToExpDate MMyy

        /// <summary>
        ///     Coverts a date to the expiration date format used in payment methods
        /// </summary>
        /// <param name="date">date to convert</param>
        /// <returns>string representing a payment method expiration date</returns>
        public static string ToExpDate(this DateTime date)
        {
            return date.ToString("MMyy");
        }

        /// <summary>
        ///     Coverts a date to the expiration date format used in payment methods
        /// </summary>
        /// <param name="date">date to convert</param>
        /// <returns>string representing a payment method expiration date</returns>
        public static string ToExpDate(this DateTime? date)
        {
            return date.HasValue ? date.Value.ToExpDate() : string.Empty;
        }

        #endregion

        #region ToExpDateDisplay MM/yyyy

        /// <summary>
        ///     Coverts a date to the expiration date display format used in payment methods
        /// </summary>
        /// <param name="date">date to convert</param>
        /// <returns>string representing a payment method expiration date</returns>
        public static string ToExpDateDisplay(this DateTime date)
        {
            return date.ToString("MM/yyyy");
        }

        /// <summary>
        ///     Coverts a date to the expiration date display format used in payment methods
        /// </summary>
        /// <param name="date">date to convert</param>
        /// <returns>string representing a payment method expiration date</returns>
        public static string ToExpDateDisplay(this DateTime? date)
        {
            return date.HasValue ? date.Value.ToExpDate() : string.Empty;
        }

        public static bool IsBetween(this DateTime date, DateTime start, DateTime end, bool inclusive = true)
        {
            return (inclusive && date >= start && date <= end)
                   || (!inclusive && date > start && date < end);
        }

        public static DateTime SetTime(this DateTime date, DateTime time)
        {
            return new DateTime(date.Date.Year, date.Date.Month, date.Date.Day, time.Hour, time.Minute, time.Second);
        }

        #endregion

        #region Formatting

        /// <summary>
        /// Returns "MM/dd/yyyy" formatted "date without time" string
        /// </summary>
        public static string ToDateText(this DateTime value)
        {
            return ToFormattedDateTimeText(value, Format.DateFormat);
        }        
        
        /// <summary>
        /// Returns "MM/dd/yyyy" formatted "date without time" string
        /// </summary>
        public static string ToDateText(this DateTime? value)
        {
            return ToFormattedDateTimeText(value, Format.DateFormat);
        }

        /// <summary>
        /// Returns "MM/dd/yyyy hh:mm tt" formatted "date with time" string
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static string ToDateTimeText(this DateTime value)
        {
            return ToFormattedDateTimeText(value, Format.DateTimeFormat);
        }

        /// <summary>
        /// Returns "MM/dd/yyyy hh:mm tt" formatted "date with time" string
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static string ToDateTimeText(this DateTime? value)
        {
            return ToFormattedDateTimeText(value, Format.DateTimeFormat);
        }

        /// <summary>
        /// Returns string in the specified format
        /// </summary>
        public static string ToFormattedDateTimeText(this DateTime value, string format)
        {
            string formattedDateTime = value.ToString(format);
            return formattedDateTime;
        }

        /// <summary>
        /// Returns string in the specified format
        /// </summary>
        public static string ToFormattedDateTimeText(this DateTime? value, string format)
        {
            string formattedDateTime = value.HasValue ? value.Value.ToString(format) : "";
            return formattedDateTime;
        }

        #endregion
    }
}
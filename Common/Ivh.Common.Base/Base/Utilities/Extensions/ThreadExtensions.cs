﻿namespace Ivh.Common.Base.Utilities.Extensions
{
    using System.Globalization;
    using System.Threading;

    public static class ThreadExtensions
    {
        public static string GetLocale(this Thread thread)
        {
            CultureInfo culture = thread.CurrentUICulture;
            return culture.Name;
        }
    }
}
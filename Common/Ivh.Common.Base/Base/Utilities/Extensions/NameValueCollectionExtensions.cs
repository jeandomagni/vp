﻿namespace Ivh.Common.Base.Utilities.Extensions
{
    using System;
    using System.Collections.Generic;
    using System.Collections.Specialized;
    using System.Linq;

    public static class NameValueCollectionExtensions
    {
        public static IEnumerable<KeyValuePair<string, string>> ToEnumerable(this NameValueCollection collection)
        {
            if(collection == null)
            {
                throw new ArgumentNullException("collection");
            }

            return collection.Cast<string>().Select(key => new KeyValuePair<string, string>(key, collection[key]));
        }
    }
}

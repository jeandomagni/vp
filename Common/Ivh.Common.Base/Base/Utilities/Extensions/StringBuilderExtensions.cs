﻿namespace Ivh.Common.Base.Utilities.Extensions
{
    using System;
    using System.Text;

    public static class StringBuilderExtensions
    {
        public static StringBuilder AppendKeyValuePair(this StringBuilder sbuilder, string key, object value)
        {
            sbuilder.Append(key);
            sbuilder.Append('=');
            sbuilder.Append(value);

            return sbuilder.AppendLine();
        }
    }
}

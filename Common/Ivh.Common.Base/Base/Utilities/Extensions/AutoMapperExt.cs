﻿namespace Ivh.Common.Base.Utilities.Extensions
{
    using AutoMapper;

    public static class AutoMapperExt
    {
        public static void CreateMapBidirectional<TSource, TDestination>(this Profile profile)
        {
            profile.CreateMap<TSource, TDestination>();
            profile.CreateMap<TDestination, TSource>();
        }
    }
}
﻿namespace Ivh.Common.Base.Utilities.Extensions
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Net.Http;
    using System.Net.Http.Headers;
    using System.Text;
    using System.Web;

    public static class HttpRequestMessageExtentions
    {
        /// <summary>
        /// Returns a dictionary of QueryStrings that's easier to work with 
        /// than GetQueryNameValuePairs KevValuePairs collection.
        /// 
        /// If you need to pull a few single values use GetQueryString instead.
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public static Dictionary<string, string> GetQueryStrings(this HttpRequestMessage request)
        {
            IEnumerable<KeyValuePair<string, string>> queryNameValuePairs = request.GetQueryNameValuePairs();
            return queryNameValuePairs?.ToDictionary(kv => kv.Key, kv => kv.Value, StringComparer.OrdinalIgnoreCase);

        }

        /// <summary>
        /// Returns an individual querystring value
        /// </summary>
        /// <param name="request"></param>
        /// <param name="key"></param>
        /// <returns></returns>
        public static string GetQueryString(this HttpRequestMessage request, string key)
        {
            // IEnumerable<KeyValuePair<string,string>> - right!
            IEnumerable<KeyValuePair<string, string>> queryStrings = request.GetQueryNameValuePairs();
            if (queryStrings == null)
            {
                return null;
            }

            KeyValuePair<string, string> match = queryStrings.FirstOrDefault(kv => string.Compare(kv.Key, key, true) == 0);
            if (string.IsNullOrEmpty(match.Value))
            {
                return null;
            }

            return match.Value;
        }

        /// <summary>
        /// Returns an individual HTTP Header value
        /// </summary>
        /// <param name="request"></param>
        /// <param name="key"></param>
        /// <returns></returns>
        public static string GetHeader(this HttpRequestMessage request, string key)
        {
            IEnumerable<string> keys = null;
            if (!request.Headers.TryGetValues(key, out keys))
            {
                return null;
            }

            return keys.First();
        }

        /// <summary>
        /// Retrieves an individual cookie from the cookies collection
        /// </summary>
        /// <param name="request"></param>
        /// <param name="cookieName"></param>
        /// <returns></returns>
        public static string GetCookie(this HttpRequestMessage request, string cookieName)
        {
            CookieHeaderValue cookie = request.Headers.GetCookies(cookieName).FirstOrDefault();
            return cookie?[cookieName].Value;
        }

        public static string AggregateWebRequest(this HttpRequestMessage httpRequestMessage, string callingMethod)
        {
            StringBuilder builder = new StringBuilder();
            builder.AppendLine($"Method: {callingMethod}");
            if (httpRequestMessage == null)
            {
                builder.AppendLine("Request or Request.QueryString was null");
                return builder.ToString();
            }
            Dictionary<string, string> queryStrings = httpRequestMessage.GetQueryStrings();
            
            if(queryStrings != null)
            {
                builder.AppendLine("Start: Printing out Request.QueryString keys");
                foreach (string key in queryStrings.Keys)
                {
                    builder.AppendLine($"Key: {key}  - Value: {queryStrings[key]}");
                }
                builder.AppendLine("End: Printing out Request.QueryString keys");
            }

            if ( httpRequestMessage.Headers != null)
            {
                Dictionary<string, IEnumerable<string>> headers = httpRequestMessage.Headers.ToDictionary(kv => kv.Key, kv => kv.Value, StringComparer.OrdinalIgnoreCase);
                
                builder.AppendLine("Start: Printing out Request.Headers keys");
                foreach (string key in headers.Keys)
                {
                    builder.AppendLine($"Key: {key}  - Value: {string.Join(",", headers[key])}");
                }
                builder.AppendLine("End: Printing out Request.Headers keys");
            }

            return builder.ToString();
        }
        public static string AggregateWebRequest(this HttpRequestBase httpRequestBase, string callingMethod)
        {
            StringBuilder builder = new StringBuilder();

            builder.AppendLine($"Method: {callingMethod}");

            if (httpRequestBase?.QueryString == null)
            {
                builder.AppendLine("Request or Request.QueryString was null");
            }
            else
            {
                builder.AppendLine("Start: Printing out Request.QueryString keys");
                foreach (string key in httpRequestBase.QueryString.AllKeys)
                {
                    builder.AppendLine($"Key: {key}  - Value: {httpRequestBase.QueryString[key]}");
                }
                builder.AppendLine("End: Printing out Request.QueryString keys");
            }
            if (httpRequestBase != null && httpRequestBase.Headers != null)
            {
                builder.AppendLine("Start: Printing out Request.Headers keys");
                foreach (string key in httpRequestBase.Headers.AllKeys)
                {
                    builder.AppendLine($"Key: {key}  - Value: {httpRequestBase.Headers[key]}");
                }
                builder.AppendLine("End: Printing out Request.Headers keys");
            }
            if (httpRequestBase != null && httpRequestBase.ContentType != null)
            {
                builder.AppendLine($"ContentType: {httpRequestBase.ContentType}");
            }
            if (httpRequestBase != null && httpRequestBase.ContentEncoding != null)
            {
                builder.AppendLine($"ContentEncoding: {httpRequestBase.ContentEncoding}");
            }
            if (httpRequestBase != null)
            {
                builder.AppendLine($"ContentLength: {httpRequestBase.ContentLength}");
            }
            if (httpRequestBase != null && httpRequestBase.Params != null)
            {
                builder.AppendLine("Start: Printing out Request.Params keys");
                foreach (string key in httpRequestBase.Params.AllKeys)
                {
                    builder.AppendLine($"Key: {key}  - Value: {httpRequestBase.Params[key]}");
                }
                builder.AppendLine("End: Printing out Request.Params keys");
            }

            return builder.ToString();
        }

        public static string AggregateWebResponse(this HttpResponseMessage response, string callingMethod)
        {
            StringBuilder builder = new StringBuilder();
            builder.AppendLine($"Method: {callingMethod}");
            if (response == null)
            {
                builder.AppendLine("Request or Request.QueryString was null");
                return builder.ToString();
            }

            if (response.Headers != null)
            {
                Dictionary<string, IEnumerable<string>> headers = response.Headers.ToDictionary(kv => kv.Key, kv => kv.Value, StringComparer.OrdinalIgnoreCase);

                builder.AppendLine("Start: Printing out Request.Headers keys");
                foreach (string key in headers.Keys)
                {
                    builder.AppendLine($"Key: {key}  - Value: {string.Join(",", headers[key])}");
                }
                builder.AppendLine("End: Printing out Request.Headers keys");
            }

            return builder.ToString();
        }
    }
}

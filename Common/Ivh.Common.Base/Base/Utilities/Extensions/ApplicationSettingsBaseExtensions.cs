﻿namespace Ivh.Common.Base.Utilities.Extensions
{
    using System.Configuration;

    public static class ApplicationSettingsBaseExtensions
    {
        public static string GetSettingValue(this ApplicationSettingsBase settings, string key)
        {
            if (settings.Properties[key] != null
                && settings[key] != null
                && settings[key].ToString().IsNotNullOrEmpty())
            {
                return (string)settings[key];
            }
            return null;
        }
    }
}

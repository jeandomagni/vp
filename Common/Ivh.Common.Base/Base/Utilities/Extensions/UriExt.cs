﻿namespace Ivh.Common.Base.Utilities.Extensions
{
    using System;

    public static class UriExt
    {
        public static Uri ToUri(this string uriString, UriKind uriKind = UriKind.RelativeOrAbsolute, bool? trailingSlash = null)
        {
            uriString = uriString.Trim();
            if (trailingSlash.HasValue)
            {
                Uri uri = new Uri(uriString, uriKind);
              
                if (trailingSlash.Value && !uri.AbsoluteUri.EndsWith("/"))
                {
                    return new Uri(uri.AbsoluteUri + "/", uriKind);
                }
                if (!trailingSlash.Value && uriString.EndsWith("/"))
                {
                    return new Uri(uriString.Substring(0, uriString.Length - 1), uriKind);
                }
            }
            return new Uri(uriString.Trim(), uriKind);
        }

        public static bool TryToUri(this string uriString, UriKind uriKind, bool ensureTrailingSlash, out Uri uri)
        {
            uri = null;
            if (uriString.IsNotNullOrEmpty())
            {
                try
                {
                    uri = uriString.ToUri(uriKind, ensureTrailingSlash);
                    return true;
                }
                catch
                {

                }
            }
            return false;
        }
    }
}
namespace Ivh.Common.Base.Utilities.Extensions
{
    using System;
    using System.Linq;
    using System.Reflection;
    using NUnit.Framework;

    public static class AssertExtensions
    {
        public static void AssertNoAsyncVoidMethods(Assembly assembly)
        {
            var messages = assembly
                .GetAsyncVoidMethods()
                .Select(method =>
                    String.Format("'{0}.{1}' is an async void method.",
                        method.DeclaringType.Name,
                        method.Name))
                .ToList();
            Assert.False(messages.Any(),
                "Async void methods found!" + Environment.NewLine + String.Join(Environment.NewLine, messages));
        }
    }
}
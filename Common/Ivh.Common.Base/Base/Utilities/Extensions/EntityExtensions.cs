﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ivh.Common.Base.Utilities.Extensions
{
    using Newtonsoft.Json;
    using Newtonsoft.Json.Serialization;
    using NHibernate.Proxy;

    public static class EntityExtensions
    {
        internal class NHibernateContractResolver : DefaultContractResolver
        {
            protected override JsonContract CreateContract(Type objectType)
            {
                JsonContract contract = null;
                if (typeof(INHibernateProxy).Assembly == objectType.Assembly || typeof(INHibernateProxy).IsAssignableFrom(objectType))
                {
                    contract = base.CreateContract(objectType.BaseType);
                }
                else
                {
                    contract = base.CreateContract(objectType);
                }

                return contract;
            }
        }

        /// <summary>
        /// ignores NHibernate proxy properties on serialization
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="obj"></param>
        /// <returns></returns>
        public static string EntityToJsonSafe<T>(this T obj)
        {
            return JsonConvert.SerializeObject(obj, new JsonSerializerSettings()
            {
                NullValueHandling = NullValueHandling.Ignore,
                ReferenceLoopHandling = ReferenceLoopHandling.Ignore,
                ContractResolver = new NHibernateContractResolver(),
            });
        }
    }
}

﻿namespace Ivh.Common.Base.Utilities.Extensions
{
    using System;
    using System.Configuration;

    public static class BooleanExt
    {
        public static bool ParseBool(this string boolString, bool? defaultValue = null)
        {
            bool returnValue;
            if (bool.TryParse(boolString, out returnValue))
            {
                return returnValue;
            }

            if (defaultValue.HasValue)
            {
                return defaultValue.Value;
            }

            throw new ConfigurationErrorsException($"Cannot parse {boolString} to enum type {typeof(bool).Name}");
        }

        public static bool TryParseEx(this string value, out bool result)
        {
            value = value.Trim().ToLower();
            if (bool.TryParse(value, out result))
            {
                return true;
            }
            switch (value)
            {
                case "yes":
                case "y":
                case "1":
                case "on":
                case "t":
                    result = true;
                    return true;
                case "no":
                case "n":
                case "0":
                case "off":
                case "f":
                    result = false;
                    return true;
            }
            return false;
        }
    }
}

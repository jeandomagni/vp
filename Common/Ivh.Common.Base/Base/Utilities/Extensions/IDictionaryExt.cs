﻿namespace Ivh.Common.Base.Utilities.Extensions
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Security.Cryptography;
    using System.Text;
    using Newtonsoft.Json;

    public static class IDictionaryExt
    {
        public static IDictionary<TKey, TValue> Merge<TKey, TValue>(this IDictionary<TKey, TValue> dest, IDictionary<TKey, TValue> source)
        {
            if (dest == null)
            {
                return source;
            }

            if (source != null)
            {
                foreach (KeyValuePair<TKey, TValue> kvp in source)
                {
                    dest[kvp.Key] = kvp.Value;
                }
            }

            return dest;
        }

        public static bool SequenceEqual<TKey, TValue>(this IDictionary<TKey, TValue> dictionary1, IDictionary<TKey, TValue> dictionary2)
        {
            return (dictionary2 ?? new Dictionary<TKey, TValue>())
             .OrderBy(kvp => kvp.Key)
             .SequenceEqual((dictionary1 ?? new Dictionary<TKey, TValue>())
                                .OrderBy(kvp => kvp.Key));
        }

        public static bool IsNotNullAndContainsKey<TKey, TValue>(this IDictionary<TKey, TValue> dictionary, TKey key)
        {
            return dictionary != null && dictionary.ContainsKey(key);
        }

        public static IDictionary<TKey, TValue> ToDictionaryOfOne<TKey, TValue>(this KeyValuePair<TKey, TValue> kvp)
        {
            return new Dictionary<TKey, TValue>() { { kvp.Key, kvp.Value } };
        }

        public static string AsAdditionalDataString(this IDictionary<string, string> additionalData)
        {
            return additionalData.IsNullOrEmpty() ? null : string.Join("|", additionalData.Select(x => string.Format("{0}:{1}", x.Key, x.Value)));
        }

        public static TValue GetValue<TKey, TValue>(this IDictionary<TKey, TValue> dictionary, TKey key)
        {
            TValue value;
            if (dictionary.TryGetValue(key, out value))
            {
                return value;
            }

            return default(TValue);
        }

        public static string GetHashCode<TKey, TValue, TEncoding, THashAlgorithm>(this IDictionary<TKey, TValue> dictionary)
            where TEncoding : Encoding, new()
            where THashAlgorithm : HashAlgorithm, new()
        {
            return JsonConvert
                .SerializeObject(new SortedDictionary<TKey, TValue>(dictionary), new JsonSerializerSettings() { Formatting = Formatting.None })
                .GetBytes<TEncoding>().GetHashCode<THashAlgorithm>();
        }

        public static string GetHashCodeHex<TKey, TValue, TEncoding, THashAlgorithm>(this IDictionary<TKey, TValue> dictionary)
            where TEncoding : Encoding, new()
            where THashAlgorithm : HashAlgorithm, new()
        {
            return JsonConvert
                .SerializeObject(new SortedDictionary<TKey, TValue>(dictionary), new JsonSerializerSettings() { Formatting = Formatting.None })
                .GetBytes<TEncoding>().GetHashCodeHex<THashAlgorithm>();
        }

        public static string GetHashCode<TKey, TValue, TEncoding, THashAlgorithm>(this IReadOnlyDictionary<TKey, TValue> dictionary, bool sort = true)
            where TEncoding : Encoding, new()
            where THashAlgorithm : HashAlgorithm, new()
        {
            return ((IDictionary<TKey, TValue>)dictionary).GetHashCode<TKey, TValue, TEncoding, THashAlgorithm>();
        }

        public static string GetHashCodeHex<TKey, TValue, TEncoding, THashAlgorithm>(this IReadOnlyDictionary<TKey, TValue> dictionary, bool sort = true)
            where TEncoding : Encoding, new()
            where THashAlgorithm : HashAlgorithm, new()
        {
            return ((IDictionary<TKey, TValue>)dictionary).GetHashCodeHex<TKey, TValue, TEncoding, THashAlgorithm>();
        }
    }
}
﻿namespace Ivh.Common.Base.Utilities.Extensions
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Runtime.Serialization;
    using System.Runtime.Serialization.Formatters.Binary;
    using System.Text;
    using System.Web;
    using System.Xml;
    using System.Xml.Serialization;
    using Newtonsoft.Json;
    using ErrorEventArgs = Newtonsoft.Json.Serialization.ErrorEventArgs;
    using Formatting = Newtonsoft.Json.Formatting;

    public static class ObjectExt
    {
        public static bool IsNullable<T>(this T obj)
        {
            if (obj == null)
            {
                return true; // obvious
            }

            Type type = typeof(T);
            if (!type.IsValueType)
            {
                return true; // ref-type
            }

            if (Nullable.GetUnderlyingType(type) != null)
            {
                return true; // Nullable<T>
            }

            return false; // value-type
        }

        public static T[] ToArrayOfOne<T>(this T obj)
        {
            return new[] {obj};
        }

        public static IList<T> ToListOfOne<T>(this T obj)
        {
            return new List<T> {obj};
        }

        public static TReturn ConvertType<T, TReturn>(T value) where T : IConvertible
        {
            return (TReturn) Convert.ChangeType(value, typeof(TReturn));
        }

        public static string ToJSON<T>(this T obj, bool indented = false, bool ignoreNulls = false, bool ignoreErrors = false)
        {
            Formatting formatting = indented ? Formatting.Indented : Formatting.None;
            JsonSerializerSettings settings = new JsonSerializerSettings();
            settings.NullValueHandling = ignoreNulls ? NullValueHandling.Ignore : NullValueHandling.Include;
            settings.ReferenceLoopHandling = ReferenceLoopHandling.Serialize;
            settings.PreserveReferencesHandling = PreserveReferencesHandling.Objects;
            if (ignoreErrors)
            {
                settings.Error = delegate(object sender, ErrorEventArgs args)
                {
                    args.ErrorContext.Handled = true; // allow the serialization to continue
                };
            }

            return obj.ToJSON(settings, indented);
        }

        public static string ToJSON<T>(this T obj, JsonSerializerSettings settings, bool indented = false)
        {
            Formatting formatting = indented ? Formatting.Indented : Formatting.None;

            string json = JsonConvert.SerializeObject(obj, formatting, settings);
            return json;
        }

        public static bool IsSerializable(this object obj)
        {
            return obj.TryGetAttribute<SerializableAttribute>(out _);
        }

        public static bool IsDataContract(this object obj)
        {
            return obj.TryGetAttribute<DataContractAttribute>(out _);
        }

        public static T DeepCopy<T>(this T obj)
        {
            using (MemoryStream ms = new MemoryStream())
            {
                BinaryFormatter formatter = new BinaryFormatter();
                formatter.Serialize(ms, obj);
                ms.Position = 0;

                return (T) formatter.Deserialize(ms);
            }
        }

        public static string SerializeToXml<T>(this T value)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(T));
            XmlWriterSettings settings = new XmlWriterSettings()
            {
                Encoding = new UTF8Encoding(false), // no BOM in a .NET string
                Indent = false,
                OmitXmlDeclaration = false
            };
            using (StringWriter textWriter = new StringWriter())
            {
                using (XmlWriter xmlWriter = XmlWriter.Create(textWriter, settings))
                {
                    serializer.Serialize(xmlWriter, value);
                }
                return textWriter.ToString();
            }
        }

        public static string GetQueryString(this object obj)
        {
            IEnumerable<string> properties = obj.GetType()
                .GetProperties()
                .Select(p => $"{p.Name}={HttpUtility.UrlEncode(p.GetValue(obj, null)?.ToString() ?? "")}");

            return string.Join("&", properties.ToArray());
        }

        public static int GetHashCodeSafe(this object value)
        {
            if (value == null)
            {
                return typeof(object).GetHashCode();
            }

            return value.GetHashCode(); 
        }

        public static bool EqualsSafe(this object value1, object value2)
        {
            if (value1 != null)
            {
                return value1.Equals(value2);
            }
            
            // value1 is null and value2 is null
            if (value2 == null)
            {
                return true;
            }

            // value1 is null and value2 is not null
            return false;
        }

    }
}
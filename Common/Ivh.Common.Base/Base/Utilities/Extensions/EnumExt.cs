﻿namespace Ivh.Common.Base.Utilities.Extensions
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Configuration;
    using System.Reflection;
    using Helpers;

    public static class EnumExt
    {
        public static TEnum ParseEnum<TEnum>(this string enumString, TEnum? defaultValue = null) where TEnum : struct
        {
            TEnum returnValue;
            if (Enum.TryParse<TEnum>(enumString, out returnValue))
            {
                return returnValue;
            }

            if (defaultValue.HasValue)
            {
                return defaultValue.Value;
            }

            throw new ConfigurationErrorsException($"Cannot parse {enumString} to enum type {typeof(TEnum).Name}");
        }

        public static string GetDescription(this Enum value)
        {
            Type type = value.GetType();
            string name = Enum.GetName(type, value);
            if (name == null)
                return null;

            FieldInfo field = type.GetField(name);
            if (field == null)
                return null;

            DescriptionAttribute attr = Attribute.GetCustomAttribute(field, typeof(DescriptionAttribute)) as DescriptionAttribute;
            return attr != null ? attr.Description : null;
        }

        public static bool IsInCategory<TEnum>(this TEnum value, string category) where TEnum : struct, IConvertible
        {
            return EnumHelper<TEnum>.Contains(value, category);
        }

        public static bool IsNotInCategory<TEnum>(this TEnum value, string category) where TEnum : struct, IConvertible
        {
            return !EnumHelper<TEnum>.Contains(value, category);
        }

        public static bool IsInCategory<TEnum>(this TEnum? value, string category) where TEnum : struct, IConvertible
        {
            return value.HasValue && EnumHelper<TEnum>.Contains(value.Value, category);
        }

        public static IEnumerable<TEnum> GetAllItems<TEnum>(this Enum value)
        {
            foreach (object item in Enum.GetValues(typeof(TEnum)))
            {
                yield return (TEnum)item;
            }
        }
        public static IEnumerable<TEnum> GetAllItems<TEnum>() where TEnum : struct
        {
            foreach (object item in Enum.GetValues(typeof(TEnum)))
            {
                yield return (TEnum)item;
            }
        }

        public static IEnumerable<TEnum> GetAllInCategory<TEnum>(string category) where TEnum : struct, IConvertible
        {
            foreach (TEnum theEnum in Enum.GetValues(typeof(TEnum)))
            {
                if (theEnum.IsInCategory<TEnum>(category))
                {
                    yield return (TEnum)theEnum;
                }
            }
        }
    }
}

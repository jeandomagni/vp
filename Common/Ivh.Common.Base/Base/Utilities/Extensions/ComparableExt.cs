﻿namespace Ivh.Common.Base.Utilities.Extensions
{
    using System;

    public static class ComparableExt
    {
        public static bool IsBetweenInclusive<TComparable>(this TComparable compThis, TComparable compStart, TComparable compEnd) where TComparable : IComparable<TComparable>
        {
            return compThis.CompareTo(compStart) >= 0 && compThis.CompareTo(compEnd) <= 0;
        }

        public static bool IsBetweenExclusive<TComparable>(this TComparable compThis, TComparable compStart, TComparable compEnd) where TComparable : IComparable<TComparable>
        {
            return compThis.CompareTo(compStart) > 0 && compThis.CompareTo(compEnd) < 0;
        }
        public static bool IsBetweenLeftInclusive<TComparable>(this TComparable compThis, TComparable compStart, TComparable compEnd) where TComparable : IComparable<TComparable>
        {
            return compThis.CompareTo(compStart) >= 0 && compThis.CompareTo(compEnd) < 0;
        }

        public static bool IsBetweenRightInclusive<TComparable>(this TComparable compThis, TComparable compStart, TComparable compEnd) where TComparable : IComparable<TComparable>
        {
            return compThis.CompareTo(compStart) > 0 && compThis.CompareTo(compEnd) <= 0;
        }
    }
}

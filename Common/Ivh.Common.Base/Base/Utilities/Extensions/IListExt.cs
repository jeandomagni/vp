﻿namespace Ivh.Common.Base.Utilities.Extensions
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Web;

    public static class IListExt
    {
        public static void AddRange<T>(this IList<T> list, IEnumerable<T> items)
        {
            if (items == null)
            {
                return;
            }
            foreach (T item in items)
            {
                list.Add(item);
            }
        }

        public static void SafeClear<T>(this IList<T> list)
        {
            if (list.IsNotNullOrEmpty())
            {
                list.Clear();
            }
        }

        public static void AddRangeDistinct<T>(this IList<T> list, IEnumerable<T> items)
        {
            list.AddRange(items.Where(x => !list.Contains(x)));
        }

        public static string ToCommaSeparatedString(this IList<string> list, bool withAnd)
        {
            if (!list.Any())
            {
                return string.Empty;
            }

            if (withAnd && list.Count > 1)
            {
                list[list.Count - 1] = string.Concat("and ", list[list.Count - 1]);
            }

            return string.Join(", ", list);
        }

        public static string ToOrderedList(this IList<string> listItems)
        {
	        if (!listItems.Any())
	        {
		        return string.Empty;
	        }

	        return "<ol>" + GetListItems(listItems) + "</ol>";
		}

        public static string ToUnorderedList(this IList<string> listItems)
        {
	        if (!listItems.Any())
	        {
		        return string.Empty;
	        }

	        return "<ul>" + GetListItems(listItems) + "</ul>";
        }

		private static string GetListItems(IList<string> listItems)
        {
	        StringBuilder sb = new StringBuilder();
	        foreach (string listItem in listItems)
	        {
		        sb.AppendFormat("<li>{0}</li>", HttpUtility.HtmlEncode(listItem));
	        }
	        return sb.ToString();
        }

        public static List<List<T>> BatchBy<T>(this IList<T> source, int batchSize)
        {
            return source.Select((x, i) => new { Index = i, Value = x })
                .GroupBy(x => x.Index / batchSize)
                .Select(x => x.Select(v => v.Value).ToList())
                .ToList();
        }

        public static void AddDistinct<T>(this IList<T> source, T item)
        {
            if (!source.Contains(item))
            {
                source.Add(item);
            }
        }

        public static int GetSequenceHashCode<T>(this IList<T> sequence)
        {           
            int seed = typeof(T).GetHashCode();
            const int modifier = 31;

            if (sequence == null)
            {
                return seed * modifier;
            }

            unchecked
            {
                return sequence.Aggregate(seed, (current, item) =>
                    (current*modifier) + item.GetHashCode());
            }            
        }

        public static bool SequencesAreEqual<T>(this IList<T> sequence1, IList<T> sequence2)
        {
            if (sequence1 == null && sequence2 == null)
            {
                return true;
            }

            if (sequence1 != null && sequence2 == null)
            {
                return false;
            }

            if (sequence2 != null && sequence1 == null)
            {
                return false;
            }

            return sequence1.SequenceEqual(sequence2);
        }
    }
}
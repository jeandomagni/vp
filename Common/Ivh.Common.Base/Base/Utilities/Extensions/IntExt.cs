﻿namespace Ivh.Common.Base.Utilities.Extensions
{
    using System;
    using System.Collections.Generic;
    using VisitPay.Constants;

    public static class IntExt
    {
        //http://csharphelper.com/blog/2014/11/convert-an-integer-into-an-ordinal-in-c/
        public static string ToOrdinal(this int value)
        {
            // start with the most common extension.
            string extension = "th";

            // examine the last 2 digits.
            int lastDigits = value % 100;

            // if the last digits are 11, 12, or 13, use th.
            if (lastDigits >= 11 && lastDigits <= 13)
            {
                return $"{value}{extension}";
            }

            // otherwise, check the last digit.
            switch (lastDigits % 10)
            {
                case 1:
                    extension = "st";
                    break;
                case 2:
                    extension = "nd";
                    break;
                case 3:
                    extension = "rd";
                    break;
            }

            return $"{value}{extension}";
        }

        public static string ToOrdinalPaymentDueDay(this int value, string lastDayText)
        {
            if (value == 31)
            {
                return lastDayText.ToLower();
            }

            return value.ToOrdinal();
        }

        public static int ToValidPaymentDueDay(this int paymentDueDay, int endOfMonthDay = DateConstants.EndOfMonthDay)
        {
            return paymentDueDay > DateConstants.LatestValidPaymentDueDay 
                ? endOfMonthDay
                : paymentDueDay;
        }

        public static void Do(this int count, Action<int> fn)
        {
            if (count < 0)
            {
                throw new InvalidOperationException("Cannot loop on negative value");
            }
            for (int i = 0; i < count; i++)
            {
                fn(i);
            }
        }

        public static IEnumerable<T> Select<T>(this int count, Func<int,T> fn)
        {
            if (count < 0)
            {
                throw new InvalidOperationException("Cannot loop on negative value");
            }
            for (int i = 0; i < count; i++)
            {
                yield return fn(i);
            }
        }
    }
}
﻿namespace Ivh.Common.Base.Utilities.Extensions
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;

    public static class ExceptionExt
    {
        public static string GetExceptionDetails(this Exception exception)
        {
            PropertyInfo[] properties = exception.GetType()
                                    .GetProperties();
            var fields = properties
                             .Select(property => new
                             {
                                 Name = property.Name,
                                 Value = property.GetValue(exception, null)
                             })
                             .Select(x => String.Format(
                                 "{0} = {1}",
                                 x.Name,
                                 x.Value != null ? x.Value.ToString() : String.Empty
                             ));
            return String.Join("\n", fields);
        }

        public static bool IsOfTypeRecursive<TException>(this Exception exception) where TException : Exception
        {
            return exception.GetType() == typeof(TException) || (exception.InnerException != null && exception.InnerException.IsOfTypeRecursive<TException>());
        }
    }
}
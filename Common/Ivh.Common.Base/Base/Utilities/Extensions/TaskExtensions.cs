﻿namespace Ivh.Common.Base.Utilities.Extensions
{
    using System.Threading.Tasks;
    public static class TaskExtensions
    {
        public static async void FireAndForget(this Task task)
        {
            try
            {
                await task;
            }
            catch
            {

            }
        }
    }
}

﻿using System;
using System.Globalization;

namespace Ivh.Common.Base.Utilities.Extensions
{
    public static class DecimalExt
    {
        private const string CurrencyFormat = "{0:c}";

        /// <summary>
        /// Format decimal amount to negative amount with a minus in front of the amount
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static string FormatCurrency(this decimal value)
        {
            NumberFormatInfo currencyFormat = new CultureInfo(CultureInfo.CurrentCulture.ToString()).NumberFormat;
            currencyFormat.CurrencyNegativePattern = 1;
            return string.Format(currencyFormat, CurrencyFormat, value);
        }

        public static string FormatCurrency(this decimal? value)
        {
            return value.GetValueOrDefault(0).FormatCurrency();
        }

        public static string FormatPercentage(this decimal value)
        {
            return value.ToString("P");
        }

        public static string FormatPercentage(this decimal? value)
        {
            return value.GetValueOrDefault(0).FormatPercentage();
        }

        public static bool IsBetween(this decimal value, decimal min, decimal max, bool inclusive = true)
        {
            return (value > min && value < max) || (inclusive && (value == min || value == max));
        }

        public static decimal RoundDown(this decimal value)
        {
            decimal times100 = value * 100m;
            times100 = Math.Floor(times100);
            return times100 / 100m;
        }

        public static decimal RoundUp(this decimal value)
        {
            decimal times100 = value * 100m;
            times100 = Math.Ceiling(times100);
            return times100 / 100m;
        }
    }
}

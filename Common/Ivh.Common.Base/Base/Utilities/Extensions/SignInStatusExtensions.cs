﻿namespace Ivh.Common.Base.Utilities.Extensions
{
    using Enums;
    using Microsoft.AspNet.Identity.Owin;
    using VisitPay.Enums;

    public static class SignInStatusExtensions
    {
        public static SignInStatusExEnum ToSignInStatusEx(this SignInStatus signInStatus)
        {
            switch (signInStatus)
            {
                case SignInStatus.Success:
                    return SignInStatusExEnum.Success;
                case SignInStatus.LockedOut:
                    return SignInStatusExEnum.LockedOut;
                case SignInStatus.RequiresVerification:
                    return SignInStatusExEnum.RequiresVerification;
                default:
                    return SignInStatusExEnum.Failure;
            }
        }
    }
}
﻿namespace Ivh.Common.Base.Utilities.Responses
{
    public class Response<T>
    {
        public Response(T obj, bool isSuccess, string errorMessage = null)
        {
            this.Object = obj;
            this.IsSuccess = isSuccess;
            this.ErrorMessage = errorMessage;
        }

        public T Object { get; }
        public string ErrorMessage { get; }
        public bool IsSuccess { get; }
        public bool IsError => !this.IsSuccess;
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ivh.Common.Base.Collections
{
    public class Map<T1, T2>
    {
       public IDictionary<T1, T2> Forward { get; private set; }
        public IDictionary<T2, T1> Reverse { get; private set; } 

        public Map()
        {
            this.Forward = new Dictionary<T1, T2>();
            this.Reverse = new Dictionary<T2, T1>();
        }

        public Map(IDictionary<T1, T2> forward)
        {
            this.Forward = forward;
            this.Reverse = forward.ToDictionary(k => k.Value, v => v.Key);
        }

        public void Add(T1 t1, T2 t2)
        {
            this.Forward[t1] = t2;
            this.Reverse[t2] = t1;
        }
    }
}

﻿namespace Ivh.Common.Base.Collections
{
    using System;
    using System.Collections.Generic;

    public class CaseInsensitiveDictionary<TValue> : Dictionary<string, TValue>
    {
        public CaseInsensitiveDictionary() : base(StringComparer.OrdinalIgnoreCase)
        {
        }
    }
}

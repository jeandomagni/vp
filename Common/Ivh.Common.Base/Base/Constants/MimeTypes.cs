﻿namespace Ivh.Common.Base.Constants
{
    public static class MimeTypes
    {
        public static class Application
        {
            public const string XWwwFormUrlEncoded = "application/x-www-form-urlencoded";
            public const string Json = "application/json";
            public const string Excel = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
            public const string OctetStream = "application/octet-stream";
            public const string Pdf = "application/pdf";

        }
    }
}
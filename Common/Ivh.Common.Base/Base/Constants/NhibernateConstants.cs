﻿namespace Ivh.Common.Base.Constants
{
    public static class NhibernateConstants
    {
        // Nhibernate will truncate nvarchar max fields unless the length is set over 4000
        public const int SqlNvarcharMax = 4001;
        public const int AdoNetBatchSize = 1;
        
        public static class SchemaNames
        {
            public const string Dbo = "dbo";
            public const string Match = "match";
            public const string Rager = "aging";
            public const string Base = "base";
            public const string Log = "log";
        }
    }
}

﻿namespace Ivh.Common.Base.Constants
{
    public static class SessionConstants
    {
        public const string ReadOnlyWriteError = "An attempt was made to write to a readonly session";
        public const string CountNotEmptyClearError = "An attempt was made to clear the session but keys are still present";
    }
}

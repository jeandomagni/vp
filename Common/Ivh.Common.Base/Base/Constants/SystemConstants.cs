﻿namespace Ivh.Common.Base.Constants
{
    public static class SystemConstants
    {
        public const string AccountNotFound = "We couldn't find your account.";
        public const int GuarantorUsernameMinLength = 6;
        public const int LocalizationCookieExpirationDays = 120;
        public const int CardReaderCookieExpirationDays = 120;
        public const string LocalizationDefaultLocale = "en-US";
    }
}
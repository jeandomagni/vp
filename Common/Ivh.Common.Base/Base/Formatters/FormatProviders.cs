﻿namespace Ivh.Common.Base.Formatters
{
    using System;
    public static class FormatProviders
    {
        public static readonly IFormatProvider LeadingTrailing = LeadingTrailingFormatProvider.FormatProvider;
    }
}
﻿namespace Ivh.Common.Base.Interfaces
{
    using System;
    using System.Collections.Concurrent;
    using System.Collections.Generic;
    using System.Linq;
    using System.Linq.Expressions;

    public interface IRepository<T> : IReadOnlyRepository<T>, IRepository
    {
        void InsertOrUpdate(T entity);
        void Update(T entity);
        void Insert(T entity);
        void Delete(T entity);
        void BulkInsert(IList<T> entityList);
        void BatchedInsert(T entity, int batchSize = 1);
        void BatchedInsert(T entity, Func<ConcurrentQueue<T>, bool> insertWhen);
    }

    public interface IReadOnlyRepository<T> : IRepository
    {
        IQueryable<T> GetQueryable();
        T GetById(int id);
        bool Exists(Expression<Func<T, bool>> predicate);
    }

    public interface IRepository
    {
        bool CanConnect();
    }

    //public interface IRepositoryCache<T, TKey>
    //{
    //    Expression<Func<T, TKey>> CacheKey { get; }
    //    Func<T, int> GetId { get; }
    //    T GetCached(TKey key, Func<T> source, int expiration = 300);
    //}

    //public interface IRepositoryCache<T, TKey1, TKey2>
    //{
    //    Tuple<Expression<Func<T, TKey1>>, Expression<Func<T, TKey2>>> CacheKey { get; }
    //    Func<T, int> GetId { get; }
    //    T GetCached(TKey1 key1, TKey2 key2, Func<T> source, int expiration = 5 * 60);

    //}

    //public interface IRepositoryCache<T, TKey1, TKey2, TKey3>
    //{
    //    Tuple<Expression<Func<T, TKey1>>, Expression<Func<T, TKey2>>, Expression<Func<T, TKey3>>> CacheKey { get; }
    //    Func<T, int> GetId { get; }
    //    T GetCached(TKey1 key1, TKey2 key2, TKey2 key3, Func<T> source, int expiration = 5 * 60);
    //}
}
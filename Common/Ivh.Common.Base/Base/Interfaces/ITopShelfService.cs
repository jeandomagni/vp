namespace Ivh.Common.Base.Interfaces
{
    public interface ITopShelfService
    {
        void Start();
        void Stop();
    }
}
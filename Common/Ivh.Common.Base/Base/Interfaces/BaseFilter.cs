﻿namespace Ivh.Common.Base.Interfaces
{
    public class BaseFilter
    {
        public int Page { get; set; }
        public int Rows { get; set; }
        public string Sord { get; set; }
        public string Sidx { get; set; }
    }
}
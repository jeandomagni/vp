﻿namespace Ivh.Common.Base.Versioning
{
    using System;
    using System.Collections.Generic;
    using System.Linq;


    public class ApplicationVersion : IEqualityComparer<ApplicationVersion>, IEquatable<ApplicationVersion>, IComparable<ApplicationVersion>
    {
        public int Major { get; set; }
        public int Minor { get; set; }
        public int Patch { get; set; }
        public int Build { get; set; }

        public ApplicationVersion(string version)
        {
           this.Initialize(version);
        }

        public ApplicationVersion(int major = 0, int minor = 0, int patch = 0, int build = 0)
        {
            this.Initialize(major,minor,patch,build);
        }

        private void Initialize(string version)
        {
            int[] versionParts = version.Split('.').Select(int.Parse).ToArray();
            this.Initialize(versionParts);
        }

        private void Initialize(int[] versionParts)
        {
            this.Initialize(
                versionParts.Length > 0 ? versionParts[0] : 0,
                versionParts.Length > 1 ? versionParts[1] : 0,
                versionParts.Length > 2 ? versionParts[2] : 0,
                versionParts.Length > 3 ? versionParts[3] : 0
            );
        }

        private void Initialize(int major = 0, int minor = 0, int patch = 0, int build = 0)
        {
            this.Major = major;
            this.Minor = minor;
            this.Patch = patch;
            this.Build = build;
        }

        public static ApplicationVersion Parse(string version)
        {
            return new ApplicationVersion(version);
        }

        public bool Equals(ApplicationVersion x, ApplicationVersion y)
        {
            if (ReferenceEquals(null, y))
            {
                return false;
            }
            if (ReferenceEquals(this, y))
            {
                return true;
            }
            return x != null && (x.Major == y.Major && x.Minor == y.Minor && x.Patch == y.Patch && x.Build == y.Build);
        }
        
        public bool Equals(ApplicationVersion other)
        {
            if (ReferenceEquals(null, other))
            {
                return false;
            }
            if (ReferenceEquals(this, other))
            {
                return true;
            }
            return this.GetHashCode() == other.GetHashCode();
        }

        public int CompareTo(ApplicationVersion other)
        {
            Func<ApplicationVersion, Guid> buildGuid = (av) =>
            {
                byte[] bytes = new byte[16];
                Array.Copy(BitConverter.GetBytes(av.Major), 0, bytes, 0, 4);
                Array.Copy(BitConverter.GetBytes(av.Minor), 0, bytes, 4, 4);
                Array.Copy(BitConverter.GetBytes(av.Patch), 0, bytes, 8, 4);
                Array.Copy(BitConverter.GetBytes(av.Build), 0, bytes, 12, 4);
                return new Guid(bytes);
            };
            
            return buildGuid(this).CompareTo(buildGuid(other));
        }

        public override bool Equals(object obj)
        {
            return this.Equals(this, (ApplicationVersion) obj);
        }

        public override int GetHashCode()
        {
            return this.GetHashCode(this);
        }

        public int GetHashCode(ApplicationVersion obj)
        {
            unchecked
            {
                int hashCode = obj.Major;
                hashCode = (hashCode * 397) ^ obj.Minor;
                hashCode = (hashCode * 397) ^ obj.Patch;
                hashCode = (hashCode * 397) ^ obj.Build;
                return hashCode;
            }
        }
        
        public static bool operator > (ApplicationVersion x, ApplicationVersion y)
        {
            return (x.Major > y.Major) || (x.Minor > y.Minor)  || (x.Patch > y.Patch) || (x.Build > y.Build);
        }

        public static bool operator <(ApplicationVersion x, ApplicationVersion y)
        {
            return (x.Major < y.Major) || (x.Minor < y.Minor) || (x.Patch < y.Patch) || (x.Build < y.Build);
        }

        public static bool operator ==(ApplicationVersion x, ApplicationVersion y)
        {
            return x != null && y != null && x.GetHashCode() == y.GetHashCode();
        }

        public static bool operator !=(ApplicationVersion x, ApplicationVersion y)
        {
            return x != null && y != null && x.GetHashCode() != y.GetHashCode();
        }

        public static bool operator >=(ApplicationVersion x, ApplicationVersion y)
        {
            return  (x.Major > y.Major) || (x.Minor > y.Minor) || (x.Patch > y.Patch) || (x.Build > y.Build || x.GetHashCode() == y.GetHashCode());
        }

        public static bool operator <=(ApplicationVersion x, ApplicationVersion y)
        {
            return (x.Major < y.Major) || (x.Minor < y.Minor) || (x.Patch < y.Patch) || (x.Build < y.Build || x.GetHashCode() == y.GetHashCode());
        }

        public override string ToString()
        {
            return $"{this.Major}.{this.Minor}.{this.Patch}.{this.Build}";
        }
    }
}

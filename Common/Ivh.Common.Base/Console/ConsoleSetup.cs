﻿namespace Ivh.Common.Console
{
    using System;
    using System.Collections.Generic;
    using System.Configuration;
    using System.Diagnostics;
    using System.Linq;
    using Autofac;
    using Base.Interfaces;
    using Configuration;
    using Topshelf;
    using VisitPay.Constants;
    using VisitPay.Enums;
    using VisitPay.Registration;

    public static class ConsoleSetup
    {
        public static void Run<TTopShelfService>(IContainer container, string priority, string nameSpace, Action<TTopShelfService> startAction = null) where TTopShelfService : class, ITopShelfService
        {
            HostFactory.Run(x =>
            {
                x.StartAutomaticallyDelayed();
                x.Service<TTopShelfService>(s =>
                {
                    s.ConstructUsing(name => container.Resolve<TTopShelfService>());

                    s.WhenStarted(tc =>
                    {
#if DEBUG
                        Trace.Listeners.Add(new ConsoleTraceListener());
#endif

                        if (startAction != null)
                        {
                            startAction(tc);
                        }
                        else
                        {
                            tc.Start();
                        }
                    });
                    s.WhenStopped(tc => tc.Stop());
                });

                x.RunAsLocalSystem();

                string client = container?.Resolve<Client>().Value ?? GetAppSettingValue("Client");

                string consoleProcessor = $"{nameSpace}.{priority}.{client}";
                x.SetServiceName(consoleProcessor);
                x.SetDescription(consoleProcessor);
                x.SetDisplayName(consoleProcessor);

                x.OnException(rc =>
                {
                    container.Resolve<MetricIncrement>().Value(Metrics.Increment.General.Failure);
                });

            });
        }


        public static string GetAppSettingValue(string key)
        {
            if (AppSettingsProvider.Instance.AllKeys.Contains(key))
            {
                return AppSettingsProvider.Instance.Get(key);
            }

            throw new Exception($"The appSetting of {key} does not exist.");
        }

        public static ConsumerMessagePriorityEnum ParseConsumerMessagePriorityFromAppConfigParameter(string priority)
        {
            try
            {
                ConsumerMessagePriorityEnum priorityEnum = (ConsumerMessagePriorityEnum)Enum.Parse(typeof(ConsumerMessagePriorityEnum), priority);
                return priorityEnum;
            }
            catch (Exception)
            {
                List<string> enums = Enum.GetValues(typeof(ConsumerMessagePriorityEnum))
                    .Cast<ConsumerMessagePriorityEnum>()
                    .Select(v => v.ToString())
                    .ToList();

                string validEnums = Environment.NewLine + string.Join(Environment.NewLine, enums);
                throw new ArgumentException($"The [Priority] key did not match any values in ConsumerMessagePriorityEnum. Value passed in: {priority}. Valid values are:{validEnums}");
            }
        }
    }
}
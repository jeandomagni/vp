﻿namespace Ivh.Common.ResiliencePolicies.Interfaces
{
    using System;
    using System.Net.Http;
    using System.Threading.Tasks;

    /// <summary>
    /// Resilience policy for HttpRequests which utilizes a memory cache as a first level cache, and a distributed cache as a fallback.
    /// </summary>
    public interface IHttpRequestPolicy
    {
        /// <summary>
        /// Make a get request with this policy.
        /// </summary>
        /// <typeparam name="T">Return type expected from request</typeparam>
        /// <param name="request">absolute url of get request. used as cacheKey if none provided</param>
        /// <param name="httpClient"></param>
        /// <param name="cacheKey">optional. cacheKey to use for this request.</param>
        /// <returns></returns>
        Task<T> GetAsync<T>(string request, HttpClient httpClient, string cacheKey = null);

        /// <summary>
        /// Clears underlying caches
        /// </summary>
        /// <returns></returns>
        Task<bool> InvalidateCache();
    }
}

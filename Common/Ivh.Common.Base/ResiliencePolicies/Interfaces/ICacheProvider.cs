﻿namespace Ivh.Common.ResiliencePolicies.Interfaces
{
    using System.Threading;
    using System.Threading.Tasks;
    using Polly.Caching;

    public interface ICacheProvider
    {
        object Get(string key);
        void Put(string key, object value, Ttl ttl);
        Task<object> GetAsync(string key, CancellationToken cancellationToken, bool continueOnCapturedContext);
        Task PutAsync(string key, object value, Ttl ttl, CancellationToken cancellationToken, bool continueOnCapturedContext);
        Task<bool> RemoveAsync(string key);
    }
}

﻿namespace Ivh.Common.ResiliencePolicies.Interfaces
{
    using System;
    using System.Threading.Tasks;

    public interface IPolicy<T>
    {
        Task<T> ExecuteAsync(Func<Task<T>> action);
        Task<T> ExecuteAsync(Func<string, Task<T>> action, string key);
        Task<T> ExecuteAsync(Func<string, Task<T>> action, string key, bool continueOnCapturedContext);
    }
}

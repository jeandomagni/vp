﻿namespace Ivh.Common.ResiliencePolicies.Interfaces
{
    public interface ISimpleCircuitBreakerPolicy
    {
        bool IsBroken();
        void Reset();
        void TryBreak();
    }
}

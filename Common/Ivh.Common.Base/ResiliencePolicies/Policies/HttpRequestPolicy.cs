﻿namespace Ivh.Common.ResiliencePolicies.Policies
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Net.Http;
    using System.Threading.Tasks;
    using Cache;
    using Interfaces;
    using Newtonsoft.Json;
    using Polly;
    using ProtoBuf;

    public class HttpRequestPolicy : IHttpRequestPolicy
    {

#if DEBUG
        private const int MemoryCacheTtlSeconds = 60 * 1;
#else
        private const int MemoryCacheTtlSeconds = 60 * 5;
#endif

        private readonly IDistributedCache _distributedCache;
        private readonly ICacheProvider _cacheProvider;
        private readonly ISimpleCircuitBreakerPolicy _serviceCallMaxRetryPolicy;
        private readonly Policy<object> _policyInternal;

        private IList<string> _cachedKeys = new SynchronizedCollection<string>();
        private readonly object _cachedKeysLock = new object();

        public HttpRequestPolicy(ICacheProvider memoryCache, IDistributedCache distributedCache, ISimpleCircuitBreakerPolicy circuitBreaker)
        {
            this._distributedCache = distributedCache;
            this._cacheProvider = memoryCache;
            this._serviceCallMaxRetryPolicy = circuitBreaker;

            this._policyInternal = this.BuildPolicy(memoryCache, TimeSpan.FromSeconds(MemoryCacheTtlSeconds)).PolicyInternal;
        }

        private PolicyFacade<object> BuildPolicy(ICacheProvider memoryCache, TimeSpan memoryCacheTimeToLive)
        {
            MemoryCachePolicy<object> cachePolicy = new MemoryCachePolicy<object>(memoryCache, memoryCacheTimeToLive, this.TrackCacheKeys);

            RetryPolicy<object, HttpRequestException> retryPolicy = new RetryPolicy<object, HttpRequestException>(this.GetRetryDelays());

            FallbackPolicy<object, Exception> fallbackPolicy = new FallbackPolicy<object, Exception>(this.FallbackToDistributedCache);

            //read/executed bottom -> up
            return new PolicyWrap<object>(
                fallbackPolicy, // fallback to redis
                retryPolicy,    // retry on exception
                cachePolicy     // check cache first
                );
        }

        private void TrackCacheKeys(string cacheKey)
        {
            //even though this is a ConcurrentDictionary, we are using an iterator in the cache invalidation and so need to lock.
            lock (this._cachedKeysLock)
            {
                if (!this._cachedKeys.Contains(cacheKey))
                {
                    this._cachedKeys.Add(cacheKey);
                }
            }
        }

        private IEnumerable<TimeSpan> GetRetryDelays()
        {
            yield return TimeSpan.FromMilliseconds(10 * 1000);
            yield return TimeSpan.FromMilliseconds(100);
            yield return TimeSpan.FromMilliseconds(50);
        }

        private async Task<object> FallbackToDistributedCache(string cacheKey)
        {
            // get from redis
            Task<string> getObjectTask = this._distributedCache.GetStringAsync(cacheKey);

            // track failures
            this._serviceCallMaxRetryPolicy.TryBreak();
            
            return await getObjectTask.ConfigureAwait(false);
        }

        private async Task<T> MakeGetRequestAsync<T>(string requestUrl, HttpClient client, string cacheKey)
        {
            cacheKey = cacheKey ?? requestUrl;

            if (this._serviceCallMaxRetryPolicy.IsBroken())
            {
                // TODO: it seems like we'd want to logging something here, maybe metricsProvider as well.
                // TODO: get specific settings version from redis on startup
                return await this._distributedCache.GetObjectAsync<T>(cacheKey).ConfigureAwait(false);
            }

            HttpResponseMessage response = await client.GetAsync(requestUrl).ConfigureAwait(false);

            if (response.IsSuccessStatusCode)
            {
                string responseString = await response.Content.ReadAsStringAsync().ConfigureAwait(false);

                T result = JsonConvert.DeserializeObject<T>(responseString);
                
                //TODO: should this be done on the server instead?
                // the line below deadlocks ocassionally.
                // await this._distributedCache.SetObjectAsync(cacheKey, result, (int)TimeSpan.FromDays(14).TotalSeconds).ConfigureAwait(false);
                this._distributedCache.SetObject(cacheKey, result, (int)TimeSpan.FromDays(14).TotalSeconds);

                // reset failures
                this._serviceCallMaxRetryPolicy.Reset();

                return result;
            }
            // todo: log something
            throw new HttpRequestException($"{response.StatusCode}");
        }

        public async Task<bool> InvalidateCache()
        {
            return await Task.Run(() =>
            {
                this._serviceCallMaxRetryPolicy.Reset();

                lock (this._cachedKeysLock)
                {
                    if (this._cachedKeys.Any())
                    {
                        if (this._cachedKeys.Any())
                        {
                            Task.Run(async () =>
                            {
                                foreach (string key in this._cachedKeys)
                                {
                                    await this._cacheProvider.RemoveAsync(key);
                                    await this._distributedCache.RemoveAsync(key);
                                }

                                this._cachedKeys = new SynchronizedCollection<string>();
                            }).Wait();
                        }
                    }
                }

                return true;
            });
        }

        public async Task<T> GetAsync<T>(string request, HttpClient httpClient, string cacheKey = null)
        {
            // use provided key or default to request string
            cacheKey = cacheKey ?? request;

            // execute with policy
            object resultObject = await this._policyInternal.ExecuteAsync(
                async () => await this.MakeGetRequestAsync<T>(request, httpClient, cacheKey), 
                new Context(cacheKey))
                .ConfigureAwait(false);

            // not found
            if (resultObject == null)
            {
                return default(T);
            }

            // return object found in cache or from http request
            if(resultObject is T result)
            {
                return result;
            }

            // return deserialized object from distributed cache
            if (resultObject is string resultString)
            {
                return this._distributedCache.Deserialize<T>(resultString);
            }

            // something bad happened
            throw new InvalidCastException();
        }
        
    }
}

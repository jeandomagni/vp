﻿namespace Ivh.Common.ResiliencePolicies.Policies
{
    using System;
    using Interfaces;
    using Polly.Caching;

    public class MemoryCachePolicy<T> : PolicyFacade<T>
    {
        public MemoryCachePolicy(ICacheProvider cache, TimeSpan timeout, Action<string> onPutAction)
        {
            if (cache is IAsyncCacheProvider asyncCache)
            {
                this.PolicyInternal = Polly.Policy.CacheAsync<T>(
                    asyncCache,
                    timeout,
                    (context, s) => { },
                    (context, s) => { },
                    (context, s) => onPutAction(context.ExecutionKey),
                    (context, s, e) => { },
                    (context, s, e) => { });
            }
            else
            {
                throw new ArgumentException($"cache must implement {nameof(IAsyncCacheProvider)}");
            }
        }

        public MemoryCachePolicy(ICacheProvider cache, TimeSpan timeout)
        {
            if (cache is IAsyncCacheProvider asyncCache)
            {
                this.PolicyInternal = Polly.Policy.CacheAsync<T>(asyncCache,timeout);
            }
            else
            {
                throw new ArgumentException($"cache must implement {nameof(IAsyncCacheProvider)}");
            }

        }
    }
}

﻿namespace Ivh.Common.ResiliencePolicies.Policies
{
    using System;
    using System.Linq;
    using Polly;

    public class PolicyWrap<T> : PolicyFacade<T>
    {
        public PolicyWrap(params PolicyFacade<T>[] policyFacades) : this(policyFacades?.Select(x => x.PolicyInternal).ToArray())
        {
        }

        internal PolicyWrap(params Policy<T>[] policies)
        {
            Policy<T> wrapPolicy = null;
            foreach (Policy<T> policy in policies)
            {
                wrapPolicy = wrapPolicy != null
                    ? wrapPolicy.WrapAsync(policy)
                    : policy;
            }

            this.PolicyInternal = wrapPolicy;
        }
    }
}

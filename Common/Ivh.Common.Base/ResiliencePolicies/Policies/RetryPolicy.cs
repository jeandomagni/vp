﻿namespace Ivh.Common.ResiliencePolicies.Policies
{
    using System;
    using System.Collections.Generic;
    using Polly;

    public class RetryPolicy<TReturn, TException> : PolicyFacade<TReturn>
        where TException : Exception
    {
        public RetryPolicy(int retryAttempts)
        {
            this.PolicyInternal = Policy<TReturn>
                .Handle<TException>()
                .RetryAsync(retryAttempts);
        }

        public RetryPolicy(IEnumerable<TimeSpan> retryDelays)
        {
            this.PolicyInternal = Policy<TReturn>
                .Handle<TException>()
                .WaitAndRetryAsync(retryDelays);
        }

        public RetryPolicy(IEnumerable<TimeSpan> retryDelays, Func<TReturn,bool> handleResultPredicate)
        {
            this.PolicyInternal = Policy<TReturn>
                .HandleResult(handleResultPredicate)
                .Or<TException>()
                .WaitAndRetryAsync(retryDelays);
        }
    }
}

﻿namespace Ivh.Common.ResiliencePolicies.Policies
{
    using System;
    using Interfaces;

    public class SimpleCircuitBreakerPolicy : ISimpleCircuitBreakerPolicy
    {
#if DEBUG
        private const int MaxFailures = 1;
        private const int TimeoutSeconds = 60 * 5;
#else
        private const int MaxFailures = 20;
        private const int TimeoutSeconds = 60 * 15;
#endif

        private readonly TimeSpan _timeout;
        private volatile int _failureCount;
        private DateTime _retryTime;

        public SimpleCircuitBreakerPolicy()
        {
            this._timeout = TimeSpan.FromSeconds(TimeoutSeconds);
            this._retryTime = DateTime.MinValue;
        }

        public bool IsBroken() => this._failureCount > MaxFailures && DateTime.UtcNow < this._retryTime;

        public void Reset()
        {
            this._retryTime = DateTime.MinValue;
            this._failureCount = 0;
        }

        public void TryBreak()
        {
            // increment failure
            this._failureCount++;

            // set retryTime if unset and max failures reached
            if (this._failureCount >= MaxFailures && this._retryTime == DateTime.MinValue)
            {
                this._retryTime = DateTime.UtcNow + this._timeout;
            }
        }
    }
}

﻿namespace Ivh.Common.ResiliencePolicies.Policies
{
    using System;
    using System.Threading;
    using System.Threading.Tasks;
    using Interfaces;
    using Polly;

    public abstract class PolicyFacade<T> : IPolicy<T>
    {
        internal Policy<T> PolicyInternal { get; set; }
        public async Task<T> ExecuteAsync(Func<Task<T>> action)
        {
            return await this.PolicyInternal.ExecuteAsync(action);
        }

        public async Task<T> ExecuteAsync(Func<string, Task<T>> action, string key)
        {
            return await this.PolicyInternal.ExecuteAsync(
                async (context, cancellationToken) => await Task.Run(async () => await action(context.ExecutionKey), cancellationToken),
                new Context(key),
                new CancellationToken());
        }

        public async Task<T> ExecuteAsync(Func<string, Task<T>> action, string key, bool continueOnCapturedContext)
        {
            return await this.PolicyInternal.ExecuteAsync(
                async (context, cancellationToken) => await Task.Run(async () => await action(context.ExecutionKey), cancellationToken),
                new Context(key),
                new CancellationToken(),
                continueOnCapturedContext);
        }
    }
}

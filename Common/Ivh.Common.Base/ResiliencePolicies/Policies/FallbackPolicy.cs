﻿namespace Ivh.Common.ResiliencePolicies.Policies
{
    using System;
    using System.Threading;
    using System.Threading.Tasks;
    using Polly;

    public class FallbackPolicy<TReturn, TException> : PolicyFacade<TReturn>
        where TException : Exception
    {
        public FallbackPolicy(Func<string, Task<TReturn>> fallback)
        {
            this.PolicyInternal = Policy<TReturn>
                .Handle<TException>()
                .FallbackAsync<TReturn>(
                    async (context, cancellationToken) => await this.ContextActionWrapper(context, cancellationToken, fallback),
                    (x, y) => Task.CompletedTask);
        }

        private async Task<TReturn> ContextActionWrapper(Polly.Context context, CancellationToken token, Func<string, Task<TReturn>> fallback)
        {
            return await Task.Run(async () => await fallback(context.ExecutionKey), token);
        }

        public FallbackPolicy(TReturn fallback)
        {
            this.PolicyInternal = Policy<TReturn>
                .Handle<TException>()
                .FallbackAsync<TReturn>(fallback);
        }
    }
}

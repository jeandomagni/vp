﻿namespace Ivh.Common.ResiliencePolicies
{
    using System.Runtime.Caching;
    using System.Threading;
    using System.Threading.Tasks;
    using Cache;
    using Interfaces;
    using Polly.Caching;

    //TODO: replace with serializing memory cache provider
    public class MemoryCacheProvider : IvinciMemoryCache, ICacheProvider, ISyncCacheProvider, IAsyncCacheProvider
    {
        public object Get(string key)
        {
            return Cache.Get(key);
        }

        public void Put(string key, object value, Ttl ttl)
        {
            Cache.Set(new CacheItem(key, value), this.GetCacheItemPolicy((int)ttl.Timespan.TotalSeconds));
        }

        public async Task<object> GetAsync(string key, CancellationToken cancellationToken, bool continueOnCapturedContext)
        {
            return await Task.Run(
                    () => this.Get(key),
                cancellationToken)
                .ConfigureAwait(continueOnCapturedContext);
        }

        public async Task PutAsync(string key, object value, Ttl ttl, CancellationToken cancellationToken, bool continueOnCapturedContext)
        {
            await Task.Run(
                    () => this.Put(key, value, ttl),
                cancellationToken)
                .ConfigureAwait(continueOnCapturedContext);
        }
    }
}

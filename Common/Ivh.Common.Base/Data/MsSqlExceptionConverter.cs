﻿namespace Ivh.Common.Data
{
    using System.Data.SqlClient;
    using NHibernate.Exceptions;
    using System;

    public class MsSqlExceptionConverter : ISQLExceptionConverter
    {      
        public Exception Convert(AdoExceptionContextInfo exInfo)
        {
            SqlException sqlException = (SqlException) ADOExceptionHelper.ExtractDbException(exInfo.SqlException);
            if(sqlException != null)
            {
                return sqlException;
            }
            return SQLStateConverter.HandledNonSpecificException(exInfo.SqlException, exInfo.Message, exInfo.Sql);
        }
    }
}

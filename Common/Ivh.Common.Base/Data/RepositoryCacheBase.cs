﻿namespace Ivh.Common.Data
{
    using System;
    using System.Linq.Expressions;
    using System.Threading.Tasks;
    using Cache;
    using Interfaces;

    public abstract class RepositoryCacheBase<TEntity, TConnection> : RepositoryBase<TEntity, TConnection>
        where TEntity : class
        where TConnection : IConnection, new()
    {
        private readonly Lazy<IDistributedCache> _cache;
        protected abstract int GetId(TEntity entity);
        
        protected RepositoryCacheBase(ISessionContext<TConnection> sessionContext, Lazy<IDistributedCache> cache) : base(sessionContext)
        {
            this._cache = cache;
        }

        protected RepositoryCacheBase(ISessionContext<TConnection> sessionContext, IStatelessSessionContext<TConnection> statelessSessionContext, Lazy<IDistributedCache> cache) : base(sessionContext, statelessSessionContext)
        {
            this._cache = cache;
        }
        
        public TEntity GetCached<TKey1>(
            Expression<Func<TEntity, TKey1>> cacheKey1, TKey1 key1,
            Func<TKey1, TEntity> source, int expiration = 300)
        {
            string cacheKey = this.CacheKey($"{this.GetKeyPart(cacheKey1, key1)}");

            TEntity Factory()
            {
                return source(key1);
            }

            return this.GetCached(cacheKey, Factory, expiration);
        }

        public TEntity GetCached<TKey1, TKey2>(
            Expression<Func<TEntity, TKey1>> cacheKey1, TKey1 key1,
            Expression<Func<TEntity, TKey2>> cacheKey2, TKey2 key2,
            Func<TKey1, TKey2, TEntity> source, int expiration = 300)
        {
            string cacheKey = this.CacheKey($"{this.GetKeyPart(cacheKey1, key1)}&{this.GetKeyPart(cacheKey2, key2)}");

            TEntity Factory()
            {
                return source(key1, key2);
            }

            return this.GetCached(cacheKey, Factory, expiration);
        }

        public TEntity GetCached<TKey1, TKey2, TKey3>(
            Expression<Func<TEntity, TKey1>> cacheKey1, TKey1 key1,
            Expression<Func<TEntity, TKey2>> cacheKey2, TKey2 key2,
            Expression<Func<TEntity, TKey3>> cacheKey3, TKey3 key3,
            Func<TKey1, TKey2, TKey3, TEntity> source, int expiration = 300)
        {
            string cacheKey = this.CacheKey($"{this.GetKeyPart(cacheKey1, key1)}&{this.GetKeyPart(cacheKey2, key2)}&{this.GetKeyPart(cacheKey3, key3)}");

            TEntity Factory()
            {
                return source(key1, key2, key3);
            }

            return this.GetCached(cacheKey, Factory, expiration);
        }

        public int GetCachedId<TKey1>(
            Expression<Func<TEntity, TKey1>> cacheKey1, TKey1 key1,
            Func<TKey1, TEntity> source, int expiration = 300)

        {
            string cacheKey = this.CacheKey(this.GetKeyPart(cacheKey1, key1));

            int id = default(int);
            if (this._cache != null)
            {
                id = Task.Run(async () => await this._cache.Value.GetObjectAsync<int>(cacheKey)).Result;
            }
            if (!id.Equals(default(int)))
            {
                return id;
            }
            else
            {
                TEntity returnValue = source(key1);
                if (returnValue != default(TEntity))
                {
                    id = this.GetId(returnValue);
                    Task.Run(async () => await this._cache.Value.SetObjectAsync(cacheKey, id, expiration));
                }

                return id;
            }
        }
        
        private string CacheKey(string suffix)
        {
            return $"RepositoryCacheBase<{typeof(TEntity).FullName}>.CacheKey.{suffix}";
        }

        private string GetKeyPart<TKey>(Expression<Func<TEntity, TKey>> cacheKey, TKey key)
        {
            MemberExpression expression = ((MemberExpression)cacheKey.Body);
            return $"{expression.Member.Name}={key}";
        }

        private TEntity GetCached(string cacheKey, Func<TEntity> factory, int expiration)
        {
            int id = default(int);
            
            if (this._cache != null)
            {
                id = this._cache.Value.GetObject<int>(cacheKey);
            }

            TEntity entity;

            if (!id.Equals(default(int)))
            {
                // found id in cache
                entity = this.GetById(id);
                if (entity != default(TEntity))
                {
                    // found entity from id
                    return entity;
                }
            }

            entity = factory();
            if (entity == default(TEntity))
            {
                // no result
                return default(TEntity);
            }

            if (this._cache != null)
            {
                // persist id to cache
                id = this.GetId(entity);
                if (id != default(int))
                {
                    this._cache.Value.SetObject(cacheKey, id, expiration);
                }
            }

            return entity;
        }
    }
}
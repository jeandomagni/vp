﻿namespace Ivh.Common.Data
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Runtime.CompilerServices;
    using Constants;
    using NHibernate;

    public class UnitsOfWork : IUnitOfWork
    {
        private readonly IList<UnitOfWork> _unitsOfWork;
        
        public UnitsOfWork(params ISession[] sessions)
            : this(sessions, IsolationLevel.Default)
        {
        }

        public UnitsOfWork(IList<ISession> sessions, System.Data.IsolationLevel isolationLevel)
        {
            this._unitsOfWork = sessions.Select(x => new UnitOfWork(x, isolationLevel)).ToList();
        }

#if CALLERINFO
        public void Commit([CallerMemberName] string memberName = "", [CallerFilePath] string sourceFilePath = "", [CallerLineNumber] int sourceLineNumber = 0)
#else
        public void Commit()
#endif
        {
            foreach (UnitOfWork unitOfWork in this._unitsOfWork)
            {
                unitOfWork.Commit();
            }
        }

#if CALLERINFO
        public void Rollback([CallerMemberName] string memberName = "", [CallerFilePath] string sourceFilePath = "", [CallerLineNumber] int sourceLineNumber = 0)
#else
        public void Rollback()
#endif
        {
            foreach (UnitOfWork unitOfWork in this._unitsOfWork)
            {
                unitOfWork.Rollback();
            }
        }

        public void Dispose()
        {
            //If the transaction is still open it will automatically roll it back.
            this.Rollback();
        }
    }
}

namespace Ivh.Common.Data
{
    using System;
    using System.Linq.Expressions;
    using NHibernate;
    using NHibernate.Criterion;

    public static class QueryOverExtensions
    {
        public static IQueryOver<TRoot, TSubType> If<TRoot, TSubType>(this IQueryOver<TRoot, TSubType> query, bool condition,
            Action<IQueryOver<TRoot, TSubType>> action)
        {
            if (condition)
            {
                action(query);
            }
            return query;
        }

        public static IQueryOver<TRoot, TSubType> WhereStringEq<TRoot, TSubType>(this IQueryOver<TRoot, TSubType> query, Expression<Func<TRoot, object>> column, string value)
        {
            return query.Where(Restrictions.Eq(Projections.Property(column), value));
        }
    }
}
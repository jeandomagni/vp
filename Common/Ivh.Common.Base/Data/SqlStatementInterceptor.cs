﻿namespace Ivh.Common.Data
{
    using Ivh.Common.VisitPay.Strings;
    using NHibernate;
    using System;
    using System.Diagnostics;

    public class SqlStatementInterceptor : EmptyInterceptor
    {
        public override NHibernate.SqlCommand.SqlString OnPrepareStatement(NHibernate.SqlCommand.SqlString sql)
        {
#if CALLERINFO

            bool showSql = false; // this is VERBOSE, so disabled by default
            if (showSql)
            {
                //StackTrace trace = new StackTrace(11, true);
                StackFrame frame = null; //trace.GetFrame(0);

                string memberName = frame?.GetMethod()?.Name ?? "Unknown";
                string sourceFilePath = frame?.GetFileName() ?? "Unknown";
                int sourceLineNumber = frame?.GetFileLineNumber() ?? 0;

                this.WriteTraceInfo(memberName, sourceFilePath, sourceLineNumber, sql.ToString(), "NHIBERNATE SQL");
            }
#endif                       
            return sql;
        }

#if CALLERINFO
        private void WriteTraceInfo(string memberName, string sourceFilePath, int sourceLineNumber, string sql, string eventName)
        {
            string eventDescription = $"----------------{eventName}----------------";
            string eventFooter = new String('-', eventDescription.Length);

            Trace.WriteLine("");
            Trace.WriteLine(eventDescription);
            Trace.WriteLine($"Caller: {memberName} at line number {sourceLineNumber} in {sourceFilePath}");
            Trace.WriteLine($"Sql: {sql}");
            Trace.WriteLine("Date and Time UTC: " + DateTime.UtcNow.ToString(Format.YearMonthDayHourMinuteSecondMillisecond));
            Trace.WriteLine(eventFooter);
            Trace.WriteLine("");
        }
#endif

    }
}

﻿namespace Ivh.Common.Data
{
    using System;
    using System.Data;
    using System.Data.Common;
    using System.Text;
    using Autofac;
    using Common.VisitPay.Registration;
    using DependencyInjection;
    using Encryption;
    using NHibernate;
    using NHibernate.Engine;
    using NHibernate.SqlTypes;
    using NHibernate.UserTypes;

    public class EncryptedString : IUserType
    {
        private const string IVinciAuthString = "iVinciIsTheBest";
        private static byte[] IVinciAuthBytes => Encoding.UTF8.GetBytes(IVinciAuthString);

        private static string CryptKey
        {
            get
            {
                string encryptionMaster;
                using (ILifetimeScope scope1 = IvinciContainer.Instance.Container().BeginLifetimeScope())
                {
                    RedisEncryptionMaster redisEncryptionMaster = scope1.Resolve<RedisEncryptionMaster>();
                    encryptionMaster = redisEncryptionMaster.Value;
                }
                return encryptionMaster;
            }
        }
        private static string AuthKey
        {
            get
            {
                string encryptionAuth;
                using (ILifetimeScope scope1 = IvinciContainer.Instance.Container().BeginLifetimeScope())
                {
                    RedisEncryptionAuth redisEncryptionAuth = scope1.Resolve<RedisEncryptionAuth>();
                    encryptionAuth = redisEncryptionAuth.Value;
                }
                return encryptionAuth;
            }
        }

        public new bool Equals(object x, object y)
        {
            if (ReferenceEquals(x, y))
            {
                return true;
            }

            if (x == null || y == null)
            {
                return false;
            }

            return x.Equals(y);
        }

        public int GetHashCode(object x)
        {
            return x.GetHashCode();
        }

        public object NullSafeGet(DbDataReader rs, string[] names, ISessionImplementor session, object owner)
        {
            string encrypted = NHibernateUtil.String.NullSafeGet(rs, names[0], session).ToString();
            return AESThenHMAC.SimpleDecrypt(encrypted, CryptKey, AuthKey, IVinciAuthString.Length);
        }

        public void NullSafeSet(DbCommand cmd, object value, int index, ISessionImplementor session)
        {
            string encrypted = AESThenHMAC.SimpleEncrypt(value.ToString(), CryptKey, AuthKey, IVinciAuthBytes);
            NHibernateUtil.String.NullSafeSet(cmd, encrypted, index, session);
        }

        public object DeepCopy(object value)
        {
            return value;
        }

        public object Replace(object original, object target, object owner)
        {
            return original;
        }

        public object Assemble(object cached, object owner)
        {
            return cached;
        }

        public object Disassemble(object value)
        {
            return value;
        }

        public SqlType[] SqlTypes => new[] { new SqlType(DbType.String) };
        public Type ReturnedType => typeof(string);
        public bool IsMutable => false;
    }
}
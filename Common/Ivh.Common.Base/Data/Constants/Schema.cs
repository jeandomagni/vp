﻿namespace Ivh.Common.Data.Constants
{
    public static class Schemas
    {
        public static class Cdi
        {
            public const string Base = "base";
            public const string Etl = "etl";
            public const string GuestPay = "guestpay";
            public const string Vp = "vp";
            public const string Aging = "aging";
        }

        public static class VisitPay
        {
            public const string Dbo = "dbo";
            public const string Base = "base";
        }
    }
}
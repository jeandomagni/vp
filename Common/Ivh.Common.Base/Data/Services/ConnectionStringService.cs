﻿namespace Ivh.Common.Data.Services
{
    using System;
    using Configuration;
    using Connection;
    using Interfaces;

    public class ConnectionStringService : IConnectionStringService
    {
        public static readonly IConnectionStringService Instance = new ConnectionStringService();

        public ConnectionStringService()
        {
            this.ApplicationConnection = new Lazy<string>(() => GetConnectionString($"{ConnectionNames.Application}Connection"));
            this.GuestPayConnection = new Lazy<string>(() => GetConnectionString($"{ConnectionNames.GuestPay}Connection"));
            this.CdiEtlConnection = new Lazy<string>(() => GetConnectionString($"{ConnectionNames.CdiEtl}Connection"));
            this.LoggingConnection = new Lazy<string>(() => GetConnectionString($"{ConnectionNames.Logging}Connection"));
            this.StorageConnection = new Lazy<string>(() => GetConnectionString($"{ConnectionNames.Storage}Connection"));
            this.QuartzConnection = new Lazy<string>(() => GetConnectionString($"{ConnectionNames.Quartz}Connection"));
            this.EnterpriseConnection = new Lazy<string>(() => GetConnectionString($"{ConnectionNames.Enterprise}Connection"));
            this.RedisCache0 = new Lazy<string>(() => GetConnectionString(ConnectionNames.RedisCache0));
            this.RedisCache1 = new Lazy<string>(() => GetConnectionString(ConnectionNames.RedisCache1));
            this.RedisCache2 = new Lazy<string>(() => GetConnectionString(ConnectionNames.RedisCache2));
            this.RabbitMq = new Lazy<string>(() => GetConnectionString(ConnectionNames.RabbitMq));
        }

        private static string GetConnectionString(string key)
        {
            return ConnectionStringsProvider.Instance.GetConnectionString(key);
        }

        public Lazy<string> ApplicationConnection { get; }
        public Lazy<string> GuestPayConnection { get; }
        public Lazy<string> CdiEtlConnection { get; }
        public Lazy<string> LoggingConnection { get; }
        public Lazy<string> StorageConnection { get; }
        public Lazy<string> QuartzConnection { get; }
        public Lazy<string> EnterpriseConnection { get; }
        public Lazy<string> RedisCache0 { get; }
        public Lazy<string> RedisCache1 { get; }
        public Lazy<string> RedisCache2 { get; }
        public Lazy<string> RabbitMq { get; }

        public string GetByKey(string key)
        {
            return ConnectionStringsProvider.Instance.GetConnectionString(key);
        }
    }
}

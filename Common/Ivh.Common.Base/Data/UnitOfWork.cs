﻿namespace Ivh.Common.Data
{
    using System;
    using System.Data;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using Ivh.Common.VisitPay.Strings;
    using NHibernate;
    using NUnit.Framework.Constraints;

    public class UnitOfWork : IUnitOfWork
    {
        private readonly ISession _session;
        private readonly ITransaction _transaction;
        private readonly bool _topLevel = true;
        private Exception _exception;

#if CALLERINFO
        public UnitOfWork(ISession session, IsolationLevel isolationLevel = Constants.IsolationLevel.Default, [CallerMemberName] string memberName = "", [CallerFilePath] string sourceFilePath = "", [CallerLineNumber] int sourceLineNumber = 0)
#else
        public UnitOfWork(ISession session, IsolationLevel isolationLevel = Constants.IsolationLevel.Default)
#endif
        {
            this._session = session;
            if (this._session.Transaction == null || !this._session.Transaction.IsActive)
            {
#if CALLERINFO
                this.WriteTraceInfo(memberName, sourceFilePath, sourceLineNumber, "OUTERMOST UNIT OF WORK CREATED");
#endif                
                this._transaction = this._session.BeginTransaction(isolationLevel);
                this._topLevel = true;
            }
            else
            {
#if CALLERINFO
                this.WriteTraceInfo(memberName, sourceFilePath, sourceLineNumber, "INNER UNIT OF WORK CREATED");
#endif
                this._topLevel = false;
            }
        }

#if CALLERINFO
        public void Commit([CallerMemberName] string memberName = "", [CallerFilePath] string sourceFilePath = "", [CallerLineNumber] int sourceLineNumber = 0)
#else
        public void Commit()
#endif
        {
            if (this._topLevel)
            {
#if CALLERINFO
                this.WriteTraceInfo(memberName, sourceFilePath, sourceLineNumber, "OUTERMOST UNIT OF WORK COMMIT REQUESTED");
#endif
                this.ValidateSession();
                this._session.Flush();
                this._transaction.Commit();
            }
            else
            {
#if CALLERINFO
                this.WriteTraceInfo(memberName, sourceFilePath, sourceLineNumber, "INNER UNIT OF WORK COMMIT IGNORED");
#endif
                // Execute any outstanding DB commands in case the inner UnitOfWork caller expects 
                // inserts, updates, or deletes to have occurred after calling Commit()
                this._session.Flush();
            }
        }

#if CALLERINFO
        public void Rollback([CallerMemberName] string memberName = "", [CallerFilePath] string sourceFilePath = "", [CallerLineNumber] int sourceLineNumber = 0)
#else
        public void Rollback()
#endif
        {
            if (this.IsTransactionActive())
            {
#if CALLERINFO
                this.WriteTraceInfo(memberName, sourceFilePath, sourceLineNumber, "UNIT OF WORK ROLLBACK REQUESTED");
#endif                
                // Flush is needed to purge any outstanding DB commands
                // Otherwise, outstanding DB commands will persist in session after rollback and execute in next transaction
                this.FlushSafe();
                this._transaction.Rollback();
            }
#if CALLERINFO
            else
            {
                this.WriteTraceInfo(memberName, sourceFilePath, sourceLineNumber, "UNIT OF WORK ROLLBACK WAS IGNORED");
            }
#endif
        }

        /// <summary>
        /// Flush (execute outstanding SQL commands) without throwing exception so we can continue executing to finalize the DB transaction afterwards.
        /// Otherwise, we leave an open transaction on the DB that locks the table(s) for all callers until the DB determines timeout has been reached.
        /// </summary>
        private void FlushSafe()
        {
            try
            {
                this._session.Flush();
            }
            catch (Exception e)
            {
                this._exception = e;
            }
        }

        private void ValidateSession()
        {
            if (!this._session.IsConnected)
            {
                throw new InvalidOperationException("Oops! We don't have a connected session");
            }
            if (!this._session.IsOpen)
            {
                throw new InvalidOperationException("Oops! We don't have an open session");
            }
            if (this._transaction == null || !this._transaction.IsActive)
            {
                throw new InvalidOperationException("Oops! We don't have an active transaction");
            }
        }

        private bool IsTransactionActive()
        {
            return this._session.IsConnected
                && this._session.IsOpen
                && this._transaction != null
                && this._transaction.IsActive;
        }

        public void Dispose()
        {
            // If the transaction is still active, it will need to be rolled back.
            // If an exception has been thrown, the Session must be discarded and cannot be reused.
            // Calls like "this._session.Flush()" or "this._session.Clear()" will not fix the problem and will likely fail themselves.
            // You must replace the Session with a new Session instance.
            // The following code patterns: "using (UnitOfWork unitOfWork = new UnitOfWork(this.Session)){}" or "try-finally"
            // will allow an exception to bubble up and make the Session unstable.
            if (this.IsTransactionActive())
            {
#if CALLERINFO
                StackTrace trace = new StackTrace(1, true);
                StackFrame frame = trace.GetFrame(0);

                string memberName = frame?.GetMethod()?.Name ?? "Unknown";
                string sourceFilePath = frame?.GetFileName() ?? "Unknown";
                int sourceLineNumber = frame?.GetFileLineNumber() ?? 0;

                this.WriteTraceInfo(memberName, sourceFilePath, sourceLineNumber, "UNIT OF WORK HAS ACTIVE TRANSACTION WHEN DISPOSED");
#endif
                this.Rollback();
            }
        }

#if CALLERINFO
        private void WriteTraceInfo(string memberName, string sourceFilePath, int sourceLineNumber, string eventName)
        {
            string eventDescription = $"----------------{eventName}----------------";
            string eventFooter = new String('-', eventDescription.Length);

            Trace.WriteLine("");
            Trace.WriteLine(eventDescription);
            Trace.WriteLine($"Caller: {memberName} at line number {sourceLineNumber} in {sourceFilePath}");
            Trace.WriteLine($"Status: IsConnected: {(this._session.IsConnected ? "Yes" : "No")}, IsOpen: {(this._session.IsOpen ? "Yes" : "No")}, HasTransaction: {(this._transaction != null ? "Yes" : "No")}, TransactionActive: {(this._transaction != null && this._transaction.IsActive ? "Yes" : "No")}");
            Trace.WriteLine("Date and Time UTC: " + DateTime.UtcNow.ToString(Format.YearMonthDayHourMinuteSecondMillisecond));
            Trace.WriteLine(eventFooter);
            Trace.WriteLine("");
        }
#endif
    }
}
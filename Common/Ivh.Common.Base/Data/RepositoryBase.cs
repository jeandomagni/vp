﻿namespace Ivh.Common.Data
{
    using System;
    using System.Collections.Concurrent;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Linq;
    using System.Linq.Expressions;
    using System.Threading;
    using Base.Interfaces;
    using Cache;
    using Interfaces;
    using NHibernate;
    using NHibernate.Criterion;

    public abstract class RepositoryBase<TEntity, TConnection> : IRepository<TEntity>
        where TEntity : class
        where TConnection : IConnection, new()
    {
        private static readonly SemaphoreSlim Semaphore = new SemaphoreSlim(1, 1);

        protected readonly ISession _session;
        private readonly IStatelessSession _statelessSession;
        private NHibernate.Transaction.ISynchronization _baseSynchronization;

        protected RepositoryBase(ISessionContext<TConnection> sessionContext)
        {
            this._session = sessionContext.Session;
        }

        protected RepositoryBase(
            ISessionContext<TConnection> sessionContext,
            IStatelessSessionContext<TConnection> statelessSessionContext,
            Lazy<IDistributedCache> cache = null)
        {
            this._session = sessionContext.Session;
            this._statelessSession = statelessSessionContext.Session;
        }
        protected RepositoryBase(
            IStatelessSessionContext<TConnection> statelessSessionContext,
            Lazy<IDistributedCache> cache = null)
        {
            this._statelessSession = statelessSessionContext.Session;
        }

        protected Action CallBackAfterCommit { get; set; }
        protected Action CallBackAfterRollback { get; set; }

        protected ISession Session => this._session;

        protected IStatelessSession StatelessSession => this._statelessSession;

        public virtual void InsertOrUpdate(TEntity entity)
        {
            if (this._session.Transaction.IsActive)
            {
                this.CheckIfCallBackNeedsRegistration();
                this._session.SaveOrUpdate(entity);
            }
            else
            {
                using (ITransaction transaction = this._session.BeginTransaction(Constants.IsolationLevel.Default))
                {
                    this.CheckIfCallBackNeedsRegistration(true);
                    this._session.SaveOrUpdate(entity);
                    transaction.Commit();
                }
            }
        }

        private void CheckIfCallBackNeedsRegistration(bool force = false)
        {
            if ((this._baseSynchronization == null || force) && (this.CallBackAfterCommit != null || this.CallBackAfterRollback != null))
            {
                if (this._baseSynchronization == null)
                {
                    this._baseSynchronization = new GenericSynchronization(this.CallBackAfterCommit, this.CallBackAfterRollback);
                }
                this._session.Transaction.RegisterSynchronization(this._baseSynchronization);
            }
        }

        public virtual void Update(TEntity entity)
        {
            if (this._session.Transaction.IsActive)
            {
                this.CheckIfCallBackNeedsRegistration();
                this._session.Update(entity);
            }
            else
            {
                using (ITransaction transaction = this._session.BeginTransaction(Constants.IsolationLevel.Default))
                {
                    this.CheckIfCallBackNeedsRegistration(true);
                    this._session.Update(entity);
                    transaction.Commit();
                }
            }
        }

        public virtual void Insert(TEntity entity)
        {
            if (this._session.Transaction.IsActive)
            {
                this.CheckIfCallBackNeedsRegistration();
                this._session.Save(entity);
            }
            else
            {
                using (ITransaction transaction = this._session.BeginTransaction(Constants.IsolationLevel.Default))
                {
                    this.CheckIfCallBackNeedsRegistration(true);
                    this._session.Save(entity);
                    transaction.Commit();
                }
            }
        }

        public virtual bool Exists(Expression<Func<TEntity, bool>> predicate)
        {
            if (this._statelessSession == null)
            {
                throw new NullReferenceException($"{nameof(this.StatelessSession)} has not been initialized.");
            }

            return this._statelessSession.Query<TEntity>().Count(predicate) > 0;

        }

        public virtual void Delete(TEntity entity)
        {
            if (this._session.Transaction.IsActive)
            {
                this.CheckIfCallBackNeedsRegistration();
                this._session.Delete(entity);
            }
            else
            {
                using (ITransaction transaction = this._session.BeginTransaction(Constants.IsolationLevel.Default))
                {
                    this.CheckIfCallBackNeedsRegistration(true);
                    this._session.Delete(entity);
                    transaction.Commit();
                }
            }
        }

        public virtual void BulkInsert(IList<TEntity> entityList)
        {
            if (this._statelessSession == null)
            {
                throw new NullReferenceException($"{nameof(this.StatelessSession)} has not been initialized.");
            }
            using (StatelessUnitOfWork unitOfWork = new StatelessUnitOfWork(this._statelessSession))
            {
                foreach (TEntity entity in entityList)
                {
                    this._statelessSession.Insert(entity);
                }
                unitOfWork.Commit();
            }
        }

        private static ConcurrentQueue<TEntity> _batchedInsertQueue = new ConcurrentQueue<TEntity>();

        public virtual void BatchedInsert(TEntity entity, int batchSize = 1)
        {
            if (this._statelessSession == null)
            {
                throw new NullReferenceException($"{nameof(this.StatelessSession)} has not been initialized.");
            }
            if (batchSize == 1)
            {
                this.StatelessSession.Insert(entity);
                return;
            }
            this.BatchedInsert(entity, x => x.Count >= batchSize);
        }
        public virtual void BatchedInsert(TEntity entity, Func<ConcurrentQueue<TEntity>, bool> insertWhen)
        {
            if (this._statelessSession == null)
            {
                throw new NullReferenceException($"{nameof(this.StatelessSession)} has not been initialized.");
            }

            _batchedInsertQueue.Enqueue(entity);

            if (insertWhen(_batchedInsertQueue))
            {
                try
                {
                    Semaphore.Wait();
                    Trace.TraceInformation($"{nameof(RepositoryBase<TEntity, TConnection>)}::{nameof(BatchedInsert)} inserting {_batchedInsertQueue.Count} {typeof(TEntity).Name} records");
                    using (StatelessUnitOfWork statelessUnitOfWork = new StatelessUnitOfWork(this._statelessSession))
                    {
                        TEntity e;
                        while (_batchedInsertQueue.TryDequeue(out e))
                        {
                            this._statelessSession.Insert(e);
                        }
                        statelessUnitOfWork.Commit();
                    }
                }
                finally
                {
                    Semaphore.Release();
                }
            }
        }

        public virtual IQueryable<TEntity> GetQueryable()
        {
            return this._session.Query<TEntity>();
        }

        public virtual TEntity GetById(int id)
        {
            return this._session.Get<TEntity>(id);
        }

        public virtual bool CanConnect()
        {
            TConnection connection = new TConnection();
            if (connection.Validate)
            {
                try
                {
                    this._session.CreateCriteria<TEntity>()
                        .SetMaxResults(0)
                        .UniqueResult<TEntity>();
                    return true;
                }
                catch (Exception ex)
                {
                    Trace.TraceError($"Repository: {this.GetType().FullName} for entity {typeof(TEntity).Name} cannot connect to {connection.Name} with connection {connection.ConnectionStringKey}", ex.Message);
                }

                return false;
            }
            return true;
        }
    }
}
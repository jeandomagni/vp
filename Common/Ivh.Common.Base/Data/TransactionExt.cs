﻿namespace Ivh.Common.Data
{
    using System;
    using NHibernate;
    using NHibernate.Transaction;

    public static class TransactionExt
    {
        public static void RegisterPostCommitSuccessAction(this ITransaction transaction, Action onSuccessAction)
        {
            if (onSuccessAction == null || transaction == null) return;
            if (transaction.IsActive)
            {
                transaction.RegisterSynchronization(new AfterTransactionCompletes((success) =>
                {
                    if (success)
                    {
                        onSuccessAction();
                    }
                }));
                return;
            }
            else
            {
                onSuccessAction();
            }
        }

        public static void RegisterPostCommitSuccessAction<T1>(this Lazy<ISession> session, Action<T1> onSuccessAction, T1 param1)
        {
            session.Value.Transaction.RegisterPostCommitSuccessAction(onSuccessAction, param1);
        }

        public static void RegisterPostCommitSuccessAction<T1>(this ISession session, Action<T1> onSuccessAction, T1 param1)
        {
            session.Transaction.RegisterPostCommitSuccessAction(onSuccessAction, param1);
        }

        public static void RegisterPostCommitSuccessAction<T1>(this ITransaction transaction, Action<T1> onSuccessAction, T1 param1)
        {
            if (onSuccessAction == null || transaction == null) return;
            if (transaction.IsActive)
            {
                transaction.RegisterSynchronization(new AfterTransactionCompletes((success) =>
                {
                    if (success)
                    {
                        onSuccessAction(param1);
                    }
                }));
                return;
            }
            else
            {
                onSuccessAction(param1);
            }
        }

        public static void RegisterPostCommitSuccessAction<T1, T2>(this Lazy<ISession> session, Action<T1,T2> onSuccessAction, T1 param1, T2 param2)
        {
            session.Value.Transaction.RegisterPostCommitSuccessAction(onSuccessAction, param1, param2);
        }

        public static void RegisterPostCommitSuccessAction<T1, T2>(this ISession session, Action<T1,T2> onSuccessAction, T1 param1, T2 param2)
        {
            session.Transaction.RegisterPostCommitSuccessAction(onSuccessAction, param1, param2);
        }

        public static void RegisterPostCommitSuccessAction<T1, T2>(this ITransaction transaction, Action<T1, T2> onSuccessAction, T1 param1, T2 param2)
        {
            if (onSuccessAction == null || transaction == null) return;
            if (transaction.IsActive)
            {
                transaction.RegisterSynchronization(new AfterTransactionCompletes((success) =>
                {
                    if (success)
                    {
                        onSuccessAction(param1, param2);
                    }
                }));
                return;
            }
            else
            {
                onSuccessAction(param1, param2);
            }
        }

        public static void RegisterPostCommitSuccessAction<T1, T2, T3>(this Lazy<ISession> session, Action<T1,T2, T3> onSuccessAction, T1 param1, T2 param2, T3 param3)
        {
            session.Value.Transaction.RegisterPostCommitSuccessAction(onSuccessAction, param1, param2, param3);
        }

        public static void RegisterPostCommitSuccessAction<T1, T2, T3>(this ISession session, Action<T1,T2, T3> onSuccessAction, T1 param1, T2 param2, T3 param3)
        {
            session.Transaction.RegisterPostCommitSuccessAction(onSuccessAction, param1, param2, param3);
        }

        public static void RegisterPostCommitSuccessAction<T1, T2, T3>(this ITransaction transaction, Action<T1, T2, T3> onSuccessAction, T1 param1, T2 param2, T3 param3)
        {
            if (onSuccessAction == null || transaction == null) return;
            if (transaction.IsActive)
            {
                transaction.RegisterSynchronization(new AfterTransactionCompletes((success) =>
                {
                    if (success)
                    {
                        onSuccessAction(param1, param2, param3);
                    }
                }));
                return;
            }
            else
            {
                onSuccessAction(param1, param2, param3);
            }
        }

        public static void RegisterPostCommitSuccessAction<T1, T2, T3, T4>(this Lazy<ISession> session, Action<T1,T2, T3, T4> onSuccessAction, T1 param1, T2 param2, T3 param3, T4 param4)
        {
            session.Value.Transaction.RegisterPostCommitSuccessAction(onSuccessAction, param1, param2, param3, param4);
        }

        public static void RegisterPostCommitSuccessAction<T1, T2, T3, T4>(this ISession session, Action<T1,T2, T3, T4> onSuccessAction, T1 param1, T2 param2, T3 param3, T4 param4)
        {
            session.Transaction.RegisterPostCommitSuccessAction(onSuccessAction, param1, param2, param3, param4);
        }

        public static void RegisterPostCommitSuccessAction<T1, T2, T3, T4>(this ITransaction transaction, Action<T1, T2, T3, T4> onSuccessAction, T1 param1, T2 param2, T3 param3, T4 param4)
        {
            if (onSuccessAction == null || transaction == null) return;
            if (transaction.IsActive)
            {
                transaction.RegisterSynchronization(new AfterTransactionCompletes((success) =>
                {
                    if (success)
                    {
                        onSuccessAction(param1, param2, param3, param4);
                    }
                }));
                return;
            }
            else
            {
                onSuccessAction(param1, param2, param3, param4);
            }
        }

        public static void RegisterPostCommitSuccessAction<T1, T2, T3, T4, T5>(this Lazy<ISession> session, Action<T1,T2, T3, T4, T5> onSuccessAction, T1 param1, T2 param2, T3 param3, T4 param4, T5 param5)
        {
            session.Value.Transaction.RegisterPostCommitSuccessAction(onSuccessAction, param1, param2, param3, param4, param5);
        }

        public static void RegisterPostCommitSuccessAction<T1, T2, T3, T4, T5>(this ISession session, Action<T1,T2, T3, T4, T5> onSuccessAction, T1 param1, T2 param2, T3 param3, T4 param4, T5 param5)
        {
            session.Transaction.RegisterPostCommitSuccessAction(onSuccessAction, param1, param2, param3, param4, param5);
        }

        public static void RegisterPostCommitSuccessAction<T1, T2, T3, T4, T5>(this ITransaction transaction, Action<T1, T2, T3, T4, T5> onSuccessAction, T1 param1, T2 param2, T3 param3, T4 param4, T5 param5)
        {
            if (onSuccessAction == null || transaction == null) return;
            if (transaction.IsActive)
            {
                transaction.RegisterSynchronization(new AfterTransactionCompletes((success) =>
                {
                    if (success)
                    {
                        onSuccessAction(param1, param2, param3, param4, param5);
                    }
                }));
                return;
            }
            else
            {
                onSuccessAction(param1, param2, param3, param4, param5);
            }
        }

        public static void RegisterPostCommitFailureAction(this ITransaction transaction, Action onFailureAction)
        {
            if (onFailureAction == null || transaction == null) return;
            if (transaction.IsActive)
            {
                transaction.RegisterSynchronization(new AfterTransactionCompletes((success) =>
                {
                    if (!success)
                    {
                        onFailureAction();
                    }
                }));
                return;
            }
        }

        public static void RegisterPostCommitFailureAction<T1>(this ITransaction transaction, Action<T1> onFailureAction, T1 param1)
        {
            if (onFailureAction == null || transaction == null) return;
            if (transaction.IsActive)
            {
                transaction.RegisterSynchronization(new AfterTransactionCompletes((success) =>
                {
                    if (!success)
                    {
                        onFailureAction(param1);
                    }
                }));
                return;
            }
        }

        public static void RegisterPostCommitFailureAction<T1, T2>(this ITransaction transaction, Action<T1, T2> onFailureAction, T1 param1, T2 param2)
        {
            if (onFailureAction == null || transaction == null) return;
            if (transaction.IsActive)
            {
                transaction.RegisterSynchronization(new AfterTransactionCompletes((success) =>
                {
                    if (!success)
                    {
                        onFailureAction(param1, param2);
                    }
                }));
                return;
            }
        }

        public static void RegisterPostCommitFailureAction<T1, T2, T3>(this ITransaction transaction, Action<T1, T2, T3> onFailureAction, T1 param1, T2 param2, T3 param3)
        {
            if (onFailureAction == null || transaction == null)
                return;

            if (transaction.IsActive)
            {
                transaction.RegisterSynchronization(new AfterTransactionCompletes((success) =>
                {
                    if (!success)
                    {
                        onFailureAction(param1, param2, param3);
                    }
                }));
                return;
            }
        }
    }
}

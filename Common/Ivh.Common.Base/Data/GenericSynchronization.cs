﻿namespace Ivh.Common.Data
{
    using System;

    public class GenericSynchronization : NHibernate.Transaction.ISynchronization
    {
        private Action CallMeBackCommit { get; set; }
        private Action CallMeBackRollback { get; set; }
        public GenericSynchronization(Action myCommitAction, Action myRollbackAction)
        {
            this.CallMeBackCommit = myCommitAction;
            this.CallMeBackRollback = myRollbackAction;
        }
        public void BeforeCompletion()
        {
        }

        public void AfterCompletion(bool success)
        {
            if (success && this.CallMeBackCommit != null)
            {
                this.CallMeBackCommit();
            }
            if (!success && this.CallMeBackRollback != null)
            {
                this.CallMeBackRollback();
            }
        }
    }
}
﻿namespace Ivh.Common.Data
{
    using System;
    using System.Diagnostics;
    using System.Linq;
    using System.Linq.Expressions;
    using Base.Interfaces;
    using Interfaces;
    using NHibernate;

    public abstract class ReadOnlyRepositoryBase<TEntity, TConnection> : IReadOnlyRepository<TEntity>
        where TEntity : class
        where TConnection : IConnection, new()
    {
        
        private readonly IStatelessSession _statelessSession;
        protected IStatelessSession StatelessSession => this._statelessSession;

        protected ReadOnlyRepositoryBase(IStatelessSessionContext<TConnection> statelessSessionContext)
        {
            this._statelessSession = statelessSessionContext.Session;
        }
        
        public bool CanConnect()
        {
            TConnection connection = new TConnection();
            if (connection.Validate)
            {
                try
                {
                    this._statelessSession.CreateCriteria<TEntity>()
                        .SetMaxResults(0)
                        .UniqueResult<TEntity>();
                    return true;
                }
                catch (Exception ex)
                {
                    Trace.TraceError($"Repository: {this.GetType().FullName} for entity {typeof(TEntity).Name} cannot connect to {connection.Name} with connection {connection.ConnectionStringKey}", ex.Message);
                }

                return false;
            }
            return true;
        }

        public bool Exists(Expression<Func<TEntity, bool>> predicate)
        {
            if (this._statelessSession == null)
            {
                throw new NullReferenceException($"{nameof(this._statelessSession)} has not been initialized.");
            }

            return this._statelessSession.Query<TEntity>().Count(predicate) > 0;
        }

        public IQueryable<TEntity> GetQueryable()
        {
            return this._statelessSession.Query<TEntity>();
        }

        public TEntity GetById(int id)
        {
            return this._statelessSession.Get<TEntity>(id);
        }
    }
}
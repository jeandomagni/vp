﻿namespace Ivh.Common.Data
{
    using FluentNHibernate.Cfg;

    public interface IFluentMappingModule
    {
        void MapAssemblies(MappingConfiguration m);
    }
}
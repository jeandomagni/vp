namespace Ivh.Common.Data
{
    using System;
    using System.Runtime.CompilerServices;

    public interface IUnitOfWork : IDisposable
    {
#if CALLERINFO
        void Commit([CallerMemberName] string memberName = "", [CallerFilePath] string sourceFilePath = "", [CallerLineNumber] int sourceLineNumber = 0);
        void Rollback([CallerMemberName] string memberName = "", [CallerFilePath] string sourceFilePath = "", [CallerLineNumber] int sourceLineNumber = 0);
#else
        void Commit();
        void Rollback();
#endif
    }
}
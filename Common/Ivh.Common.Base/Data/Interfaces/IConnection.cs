﻿namespace Ivh.Common.Data.Interfaces
{
    public interface IConnection
    {
        string FactoryName { get; }
        bool Default { get; }
        bool Validate { get; }
        string Name { get; }
        string ConnectionStringKey { get; }
        bool AllowedFromWeb { get; }
    }
}
﻿namespace Ivh.Common.Data.Interfaces
{
    using System;
    using System.Configuration;

    public interface IConnectionStringService
    {
        Lazy<string> ApplicationConnection { get; }
        Lazy<string> GuestPayConnection { get; }
        Lazy<string> CdiEtlConnection { get; }
        Lazy<string> LoggingConnection { get; }
        Lazy<string> StorageConnection { get; }
        Lazy<string> QuartzConnection { get; }
        Lazy<string> EnterpriseConnection { get; }
        Lazy<string> RedisCache0 { get; }
        Lazy<string> RedisCache1 { get; }
        Lazy<string> RedisCache2 { get; }
        Lazy<string> RabbitMq { get; }

        string GetByKey(string key);
    }
}

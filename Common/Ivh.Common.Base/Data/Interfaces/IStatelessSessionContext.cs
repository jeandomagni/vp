﻿namespace Ivh.Common.Data.Interfaces
{
    using NHibernate;

    public interface IStatelessSessionContext<TConnection> where TConnection : IConnection
    {
        IStatelessSession Session { get; }
        IConnection Connection { get; }
    }
}
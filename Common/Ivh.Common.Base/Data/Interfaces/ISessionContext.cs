﻿namespace Ivh.Common.Data.Interfaces
{
    using NHibernate;

    public interface ISessionContext<TConnection> where TConnection : IConnection
    {
        ISession Session { get; }
        IConnection Connection { get; }
    }
}
﻿namespace Ivh.Common.Data.Connection
{
    using System;
    using Interfaces;
    using NHibernate;

    public class StatelessSessionContext<TConnection> : IStatelessSessionContext<TConnection> where TConnection : IConnection, new()
    {
        public IStatelessSession Session { get; private set; }
        public IConnection Connection  => new Lazy<IConnection>(() => new TConnection()).Value;
        public StatelessSessionContext(IStatelessSession session)
        {
            this.Session = session;
        }
    }
}
﻿
namespace Ivh.Common.Data.Connection
{
    using Interfaces;

    public abstract class Connection<TConnection> : IConnection where TConnection : IConnection, new()
    {
        public static IConnection Instance = new TConnection();

        public string FactoryName => typeof(TConnection).Name;
        
        public virtual bool Default => false;
        public virtual bool Validate => true;
        public abstract string Name { get; }
        public abstract string ConnectionStringKey { get; }
        public virtual bool AllowedFromWeb => true;
    }
}

﻿namespace Ivh.Common.Data.Connection
{
    using System;
    using Interfaces;
    using NHibernate;

    public class SessionContext<TConnection> : ISessionContext<TConnection> where TConnection : IConnection, new()
    {
        public ISession Session { get; private set; }
        public IConnection Connection  => new Lazy<IConnection>(() => new TConnection()).Value;

        public SessionContext(ISession session)
        {
            this.Session = session;
        }
    }
}
﻿namespace Ivh.Common.Data.Connection.Connections
{
    public class CdiEtl : Connection<CdiEtl>
    {
        public override string Name => ConnectionNames.CdiEtl;
        public override string ConnectionStringKey => "CdiEtlConnection";
        public override bool AllowedFromWeb => false;
    }
}
﻿namespace Ivh.Common.Data.Connection.Connections
{
    public class Logging : Connection<Logging>
    {
        public override string Name => ConnectionNames.Logging;
        public override string ConnectionStringKey => "LoggingConnection";
    }
}
﻿namespace Ivh.Common.Data.Connection.Connections
{
    public class Enterprise : Connection<Enterprise>
    {
        public override string Name => ConnectionNames.Enterprise;
        public override string ConnectionStringKey => "EnterpriseConnection";
    }
}
﻿namespace Ivh.Common.Data.Connection.Connections
{
    public class Storage : Connection<Storage>
    {
        public override string Name => ConnectionNames.Storage;
        public override string ConnectionStringKey => "StorageConnection";
    }
}
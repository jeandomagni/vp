﻿namespace Ivh.Common.Data.Connection.Connections
{
    public class Quartz : Connection<Quartz>
    {
        public override string Name => ConnectionNames.Quartz;
        public override string ConnectionStringKey => "Quartz2Connection";
    }
}
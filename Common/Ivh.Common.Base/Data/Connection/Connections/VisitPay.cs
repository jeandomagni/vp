﻿namespace Ivh.Common.Data.Connection.Connections
{
    public class VisitPay : Connection<VisitPay>
    {
        public override bool Default => true;
        public override string Name => ConnectionNames.Application;
        public override string ConnectionStringKey => "LocalConnection";
    }
}
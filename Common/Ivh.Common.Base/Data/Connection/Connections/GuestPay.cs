﻿namespace Ivh.Common.Data.Connection.Connections
{
    public class GuestPay : Connection<GuestPay>
    {
        public override string Name => ConnectionNames.GuestPay;
        public override string ConnectionStringKey => "GuestPayConnection";
    }
}
﻿namespace Ivh.Common.Data.Connection
{
    using System.Data.SqlClient;

    public class ConnectionStringHelper
    {
        private readonly SqlConnectionStringBuilder _sqlConnectionStringBuilder;
        public ConnectionStringHelper(string connectionString)
        {
            this._sqlConnectionStringBuilder = new SqlConnectionStringBuilder(connectionString);
        }

        public string Database
        {
            get { return this._sqlConnectionStringBuilder.InitialCatalog; }
        }

        public string Server
        {
            get { return this._sqlConnectionStringBuilder.DataSource; }
        }

        public string UserName { get { return this._sqlConnectionStringBuilder.UserID; } }
        public string Password { get { return this._sqlConnectionStringBuilder.Password; } }

    }
}

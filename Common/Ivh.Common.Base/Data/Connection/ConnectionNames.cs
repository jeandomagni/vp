﻿namespace Ivh.Common.Data.Connection
{
    public static class ConnectionNames
    {
        public const string Application = "Local";
        public const string Logging = "Logging";
        //public const string HospitalData = "HospitalData";
        public const string CdiEtl = "CdiEtl";
        public const string Storage = "Storage";
        public const string Quartz = "Quartz2";//
        public const string GuestPay = "GuestPay";
        public const string RedisCache0 = "RedisCache0";
        public const string RedisCache1 = "RedisCache1";
        public const string RedisCache2 = "RedisCache2";
        public const string RabbitMq = "RabbitMq";
        public const string Enterprise = "Enterprise";
        public const string BalanceTransfer = "BalanceTransfer";
    }
}

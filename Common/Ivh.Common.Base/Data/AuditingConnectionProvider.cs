﻿namespace Ivh.Common.Data
{
    using System;
    using System.Data;
    using System.Data.Common;
    using System.Threading;
    using System.Threading.Tasks;
    using System.Web;
    using Common.Base.Utilities.Extensions;
    using Microsoft.AspNet.Identity;
    using NHibernate.Connection;

    public class AuditingConnectionProvider : DriverConnectionProvider
    {
        private const string SetContextSql = "declare @Length tinyint, @Ctx varbinary(128) " +
                                             "select @Length = len(@username) " +
                                             "select @Ctx = convert(binary(1), @Length) + convert(varbinary(127), @username) " +
                                             "set context_info @Ctx";

        private const string RemoveContextSql = "set context_info 0x";

        public override async Task<DbConnection> GetConnectionAsync(CancellationToken cancellationToken)
        {
            return await Task.Run(() => this.GetConnection(), cancellationToken);
        }

        public override DbConnection GetConnection()
        {
            DbConnection conn = base.GetConnection();
            if (HttpRuntime.AppDomainAppId != null)// WebApplication
            {
                string userId = string.Empty;
                if (HttpContext.Current != null && HttpContext.Current.User != null && HttpContext.Current.User.Identity != null)
                {
                    userId = HttpContext.Current.User.Identity.GetUserId() ?? "UnknownWebUser";
                }

                this.SetContext(conn, userId.RemoveSpecialCharacters());
            }
            return conn;
        }

        protected virtual void SetContext(IDbConnection conn, string userId)
        {
            IDbCommand cmd = conn.CreateCommand();
            IDbDataParameter param = cmd.CreateParameter();
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = SetContextSql;

            param.ParameterName = "@username";
            param.DbType = DbType.AnsiString;
            param.Size = 127;
            param.Value = userId.Substring(0, Math.Min(userId.Length, 127));
            cmd.Parameters.Add(param);

            cmd.ExecuteNonQuery();
        }

        protected virtual void RemoveContext(IDbConnection conn)
        {
            IDbCommand cmd = conn.CreateCommand();
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = RemoveContextSql;
            cmd.ExecuteNonQuery();
        }

        public override void CloseConnection(DbConnection conn)
        {
            this.RemoveContext(conn);
            base.CloseConnection(conn);
        }
    }
}
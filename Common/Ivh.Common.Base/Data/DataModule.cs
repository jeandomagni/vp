﻿namespace Ivh.Common.Data
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Globalization;
    using Autofac;
    using Base.Constants;
    using Base.Utilities.Extensions;
    using Base.Utilities.Helpers;
    using Connection;
    using DependencyInjection;
    using DependencyInjection.Extensions;
    using FluentNHibernate.Cfg;
    using FluentNHibernate.Cfg.Db;
    using Interfaces;
    using NHibernate;
    using NHibernate.AspNet.Identity.Helpers;
    using NHibernate.Caches.RtMemoryCache;
    using VisitPay.Enums;

    public static class DataModule
    {

        private static readonly IDictionary<string, int> SessionFactories = new Dictionary<string, int>();
        private static readonly Lazy<string> ApplicationName = new Lazy<string>(() => Ivh.Common.Properties.Settings.Default.ApplicationName ?? "Unknown");

        public static ContainerBuilder RegisterSessionFactory<TConnection>(this ContainerBuilder builder, params Type[] additionalTypes) where TConnection : IConnection, new()
        {
            TConnection connection = Activator.CreateInstance<TConnection>();

            builder.Register(x =>
            {
                ISessionFactory sessionFactory = CreateSessionFactory(x.Resolve<IEnumerable<IFluentMappingModule>>(), connection.ConnectionStringKey, additionalTypes);
                return sessionFactory;
            }).SingleInstance().Named<ISessionFactory>(typeof(TConnection).Name);

            builder.Register(x =>
            {
                ISessionContext<TConnection> session = OpenSession<TConnection>(x);
                return session;
            }).As<ISessionContext<TConnection>>().InstanceWebPerRequestElsePerLifetimeScope();

            builder.Register(x =>
            {
                IStatelessSessionContext<TConnection> statelessSession = OpenStatelessSession<TConnection>(x);
                return statelessSession;
            }).As<IStatelessSessionContext<TConnection>>().InstanceWebPerRequestElsePerLifetimeScope();

            return builder;
        }

        [Obsolete("Prefer RegisterSessionFactory<TConnection>")]
        public static void RegisterSessionFactory(this ContainerBuilder builder, string connectionString, params Type[] additionalTypes)
        {
            builder.Register(x => DataModule.CreateSessionFactory(x.Resolve<IEnumerable<IFluentMappingModule>>(), connectionString, additionalTypes)).SingleInstance().As<ISessionFactory>();
        }

        [Obsolete("Prefer RegisterSessionFactory<TConnection>")]
        public static void RegisterNamedSessionFactory(this ContainerBuilder builder, string connectionString, string name, params Type[] additionalTypes)
        {
            builder.Register(x => DataModule.CreateSessionFactory(x.Resolve<IEnumerable<IFluentMappingModule>>(), connectionString, additionalTypes)).SingleInstance().Named<ISessionFactory>(name);
        }

        public static ISessionFactory CreateSessionFactory(IEnumerable<IFluentMappingModule> fluentMappingModules, string connectionString, params Type[] additionalTypes)
        {
            string commandTimeout = Convert.ToInt32((TimeSpan.FromMinutes(Properties.Settings.Default.CommandTimeoutInMinutes).TotalSeconds)).ToString(CultureInfo.InvariantCulture);

            ISessionFactory sessionFactory = Fluently.Configure()
                .Database(MsSqlConfiguration
                        .MsSql2012
                        .ConnectionString(p => p.FromConnectionStringWithKey(connectionString))
                )
                .Mappings(x =>
               {
                   foreach (IFluentMappingModule fluentMappingModule in fluentMappingModules)
                   {
                       fluentMappingModule.MapAssemblies(x);
                   }
               })
                .Mappings(m => m.FluentMappings.Conventions.Add<Ivh.Common.Data.Conventions.DateTimeConvention>())
                .Cache(c =>
                {
                    // https://github.com/nhibernate/NHibernate-Caches/blob/master/RtMemoryCache/RtMemoryCache.cs
                    // appears that default second level cache expiration time is 300 seconds / 5 minutes
                    c.UseQueryCache().ProviderClass<RtMemoryCacheProvider>();
                    c.UseSecondLevelCache();
                })
                .ExposeConfiguration(
                    cfg =>
                    {
                        cfg.SetProperty("command_timeout", commandTimeout);
                        cfg.SetProperty("adonet.batch_size", NhibernateConstants.AdoNetBatchSize.ToString());

                        string cs = cfg.GetProperty("connection.connection_string");
                        cfg.SetProperty("connection.connection_string", cs + $";Application Name={ApplicationName.Value}");
#if DEBUG
                        cfg.SetProperty("sql_exception_converter", typeof(MsSqlExceptionConverter).AssemblyQualifiedName);
#endif
#if CALLERINFO
                        cfg.SetInterceptor(new SqlStatementInterceptor());
#endif
                    })
                .ExposeConfiguration(
                    cfg => cfg.AddDeserializedMapping(MappingHelper.GetIdentityMappings(additionalTypes: additionalTypes ?? new Type[] { }), null))
                .BuildSessionFactory();

            if (!SessionFactories.ContainsKey(connectionString))
            {
                SessionFactories[connectionString] = 0;
            }
            SessionFactories[connectionString]++;

            foreach (KeyValuePair<string, int> VARIABLE in SessionFactories)
            {
                Trace.TraceInformation($"{VARIABLE.Key}: {VARIABLE.Value}");
            }

            return sessionFactory;
        }

        public static ISessionContext<TConnection> OpenSession<TConnection>(IComponentContext context) where TConnection : IConnection, new()
        {
            ValidateCurrentContext<TConnection>();

            ISessionFactory sessionFactory = context.ResolveNamed<ISessionFactory>(typeof(TConnection).Name);
            ISession session = sessionFactory.OpenSession();
            SetupSession(session);
            return new SessionContext<TConnection>(session);
        }

        public static IStatelessSessionContext<TConnection> OpenStatelessSession<TConnection>(IComponentContext context) where TConnection : IConnection, new()
        {
            ValidateCurrentContext<TConnection>();

            IStatelessSession statelessSession = context.ResolveNamed<ISessionFactory>(typeof(TConnection).Name).OpenStatelessSession();
            return new StatelessSessionContext<TConnection>(statelessSession);
        }

        public static ISession OpenSession(IComponentContext context)
        {
            ISession session = context.Resolve<ISessionFactory>().OpenSession();
            SetupSession(session);
            return session;
        }

        public static ISession OpenSessionNamed(IComponentContext context, string name)
        {
            ISession session = context.ResolveNamed<ISessionFactory>(name).OpenSession();
            SetupSession(session);
            return session;
        }

        public static IStatelessSession OpenStatelessSession(IComponentContext context)
        {
            IStatelessSession statelessSession = context.Resolve<ISessionFactory>().OpenStatelessSession();
            return statelessSession;
        }

        public static IStatelessSession OpenStatelessSessionNamed(IComponentContext context, string name)
        {
            IStatelessSession statelessSession = context.ResolveNamed<ISessionFactory>(name).OpenStatelessSession();
            return statelessSession;
        }

        private static void SetupSession(ISession session)
        {
            //This doesnt seem to help all that much.  But seems safer.
            //session.FlushMode = FlushMode.Always;
        }

        public static bool ValidateRepositorySessions(IContainer container)
        {
            bool isValid = true;
            return isValid;
#if DEBUG
            /*foreach (IComponentRegistration componentRegistration in container.ComponentRegistry.Registrations)
            {
                foreach (IServiceWithType service in componentRegistration.Services
                    .Where(predicate: x => x is IServiceWithType)
                    .Cast<IServiceWithType>()
                    .Where(x => x.ServiceType.IsAssignableTo<IRepository>()).Distinct())
                {
                    using (ILifetimeScope lifetimeScope = container.BeginLifetimeScope("AutofacWebRequest"))
                    {
                        if (lifetimeScope.Resolve(service.ServiceType) is IRepository repository)
                        {
                            if (repository.CanConnect())
                            {
                                Trace.TraceInformation($"Repository: {repository.GetType().Name} can connect");
                            }
                            else
                            {
                                isValid = false;
                                Trace.TraceError($"Repository: {repository.GetType().Name} cannot connect");
                            }
                        }
                    }
                }
            }*/
#endif
            return isValid;
        }

        private static readonly Lazy<ApplicationEnum> Application = new Lazy<ApplicationEnum>(() =>
        {
            string applicationString = Ivh.Common.Properties.Settings.Default.GetSettingValue("Application");
            return Enum.TryParse<ApplicationEnum>(applicationString, out ApplicationEnum applicationEnum) ? applicationEnum : ApplicationEnum.Unknown;
        });


        private static void ValidateCurrentContext<TConnection>() where TConnection : IConnection, new()
        {
            if (IvhEnvironment.IsDevelopmentOrQualityAssurance)
            {
                if (WebHelper.IsWebApp)
                {
                    if (Application.Value != ApplicationEnum.QATools)
                    {
                        TConnection connection = new TConnection();
                        if (!connection.AllowedFromWeb)
                        {
                            if (IvhEnvironment.IsDevelopment)
                            {
                                //throw new Exception($"Connections to {connection.Name} not allowed from web context and may violate licensing terms.");
                            }
                            else
                            {
                                Trace.TraceWarning($"Connections to {connection.Name} not allowed from web context and may violate licensing terms.");
                            }
                        }
                    }
                }
            }
        }
    }
}
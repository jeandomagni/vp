﻿namespace Ivh.Common.Data
{
    using System;
    using System.Data.SqlClient;

    public class SqlRetry : IDisposable
    {
        private readonly int _maxRetry = 1;
        private int _retryCount = 0;

        public SqlRetry()
        {
            
        }
        
        public SqlRetry(int maxRetry)
        {
            this._maxRetry = maxRetry;
        }

        public bool CanRetry(Exception ex)
        {
            SqlException sqlException = ex.GetBaseException() as SqlException;
            if (sqlException == null) return false;
            switch (sqlException.Number)
            {
                case 1205://DEADLOCK
                case -2://TIMEOUT
                case -2147217871://TIMEOUT
                case 11://NETWORK ERROR
                {
                    return ++this._retryCount <= this._maxRetry;
                }
            }
            return false;
        }
        public void Dispose()
        {
        }
    }
}

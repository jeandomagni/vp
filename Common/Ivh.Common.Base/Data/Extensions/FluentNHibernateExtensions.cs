﻿namespace Ivh.Common.Data.Extensions
{
    using FluentNHibernate.Mapping;

    public static class FluentNHibernateExtensions
    {
        private const string StringClob = "StringClob";
        private const string VarcharMax = "varchar(max)";
        private const string NVarcharMax = "nvarchar(max)";

        public static PropertyPart AsVarcharMax(this PropertyPart propertyPart)
        {

            propertyPart.CustomType(StringClob).CustomSqlType(VarcharMax);
            return propertyPart;
        }

        public static PropertyPart AsNVarcharMax(this PropertyPart propertyPart)
        {

            propertyPart.CustomType(StringClob).CustomSqlType(NVarcharMax);
            return propertyPart;
        }
    }
}

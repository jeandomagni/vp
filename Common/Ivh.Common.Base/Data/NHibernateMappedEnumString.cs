﻿namespace Ivh.Common.Data
{
    using System;
    using System.Collections.Generic;
    using CuttingEdge.Conditions;
    using NHibernate.Type;

    public class NHibernateMappedEnumString<T> : EnumStringType<T> where T : struct, IConvertible
    {
        private readonly Dictionary<string, T> _mapping;
        private readonly Dictionary<T, string> _reverseMapping;

        public NHibernateMappedEnumString(Dictionary<string, T> mapping)
        {
            this._reverseMapping = new Dictionary<T, string>(mapping.Count);
            this._mapping = mapping;
            foreach (KeyValuePair<string, T> value in this._mapping)
            {
                this._reverseMapping.Add(value.Value, value.Key);
            }
        }

        public override object GetInstance(object code)
        {
            Condition.Requires(code, "code").IsOfType(typeof (string)).IsNotNull();
            string value = code as string;
            T enumValue;
            if (Enum.TryParse(value, out enumValue)) return enumValue;
            if (this._mapping.TryGetValue(value, out enumValue)) return enumValue;
            throw new ArgumentOutOfRangeException("code", string.Format("Enumeration value: \"{0} \" is invalid for enumeration: {1}", value, typeof (T).Name));
        }

        public override object GetValue(object code)
        {
            Condition.Requires(code, "code").IsOfType(typeof (T));
            T value = (T) code;
            string enumStringValue;
            if (this._reverseMapping.TryGetValue(value, out enumStringValue))
                return enumStringValue;
            return value.ToString();
        }
    }
}
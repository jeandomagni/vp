﻿namespace Ivh.Common.Data
{
    using System;
    using System.Data;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using Ivh.Common.VisitPay.Strings;
    using NHibernate;
    using NUnit.Framework.Constraints;

    public class StatelessUnitOfWork : IUnitOfWork
    {
        private readonly IStatelessSession _statelessSession;
        private readonly ITransaction _transaction;
        private readonly bool _topLevel = true;

#if CALLERINFO
        public StatelessUnitOfWork(IStatelessSession statelessSession, IsolationLevel isolationLevel = Constants.IsolationLevel.Default, [CallerMemberName] string memberName = "", [CallerFilePath] string sourceFilePath = "", [CallerLineNumber] int sourceLineNumber = 0)
#else
        public StatelessUnitOfWork(IStatelessSession statelessSession, IsolationLevel isolationLevel = Constants.IsolationLevel.Default)
#endif
        {
            this._statelessSession = statelessSession;
            if (this._statelessSession.Transaction == null || !this._statelessSession.Transaction.IsActive)
            {
#if CALLERINFO
                this.WriteTraceInfo(memberName, sourceFilePath, sourceLineNumber, "OUTERMOST UNIT OF WORK CREATED");
#endif  
                this._transaction = this._statelessSession.BeginTransaction(isolationLevel);
                this._topLevel = true;
            }
            else
            {
#if CALLERINFO
                this.WriteTraceInfo(memberName, sourceFilePath, sourceLineNumber, "INNER UNIT OF WORK CREATED");
#endif
                this._topLevel = false;
            }
        }

#if CALLERINFO
        public void Commit([CallerMemberName] string memberName = "", [CallerFilePath] string sourceFilePath = "", [CallerLineNumber] int sourceLineNumber = 0)
#else
        public void Commit()
#endif
        {
            if (this._topLevel)
            {
#if CALLERINFO
                this.WriteTraceInfo(memberName, sourceFilePath, sourceLineNumber, "OUTERMOST UNIT OF WORK COMMIT REQUESTED");
#endif
                this.ValidateSession();
                this._transaction.Commit();
            }
#if CALLERINFO
            else
            {
                this.WriteTraceInfo(memberName, sourceFilePath, sourceLineNumber, "INNER UNIT OF WORK COMMIT IGNORED");
            }
#endif  
        }

#if CALLERINFO
        public void Rollback([CallerMemberName] string memberName = "", [CallerFilePath] string sourceFilePath = "", [CallerLineNumber] int sourceLineNumber = 0)
#else
        public void Rollback()
#endif
        {
            if (this.IsTransactionActive())
            {
#if CALLERINFO
                this.WriteTraceInfo(memberName, sourceFilePath, sourceLineNumber, "UNIT OF WORK ROLLBACK REQUESTED");
#endif                
                this._transaction.Rollback();
            }
#if CALLERINFO
            else
            {
                this.WriteTraceInfo(memberName, sourceFilePath, sourceLineNumber, "UNIT OF WORK ROLLBACK WAS IGNORED");
            }
#endif
        }

        private void ValidateSession()
        {
            if (!this._statelessSession.IsConnected)
            {
                throw new InvalidOperationException("Oops! We don't have a connected session");
            }
            if (!this._statelessSession.IsOpen)
            {
                throw new InvalidOperationException("Oops! We don't have an open session");
            }
            if (this._transaction == null || !this._transaction.IsActive)
            {
                throw new InvalidOperationException("Oops! We don't have an active transaction");
            }
        }

        private bool IsTransactionActive()
        {
            return this._statelessSession.IsConnected
                && this._statelessSession.IsOpen
                && this._transaction != null
                && this._transaction.IsActive;
        }

        public void Dispose()
        {
            // If the transaction is still active, it will need to be rolled back.
            if (this.IsTransactionActive())
            {
#if CALLERINFO
                StackTrace trace = new StackTrace(1, true);
                StackFrame frame = trace.GetFrame(0);

                string memberName = frame?.GetMethod()?.Name ?? "Unknown";
                string sourceFilePath = frame?.GetFileName() ?? "Unknown";
                int sourceLineNumber = frame?.GetFileLineNumber() ?? 0;

                this.WriteTraceInfo(memberName, sourceFilePath, sourceLineNumber, "UNIT OF WORK HAS ACTIVE TRANSACTION WHEN DISPOSED");
#endif
                this.Rollback();
            }
        }

#if CALLERINFO
        private void WriteTraceInfo(string memberName, string sourceFilePath, int sourceLineNumber, string eventName)
        {
            string eventDescription = $"----------------STATELESS {eventName}----------------";
            string eventFooter = new String('-', eventDescription.Length);

            Trace.WriteLine("");
            Trace.WriteLine(eventDescription);
            Trace.WriteLine($"Caller: {memberName} at line number {sourceLineNumber} in {sourceFilePath}");
            Trace.WriteLine($"Status: IsConnected: {(this._statelessSession.IsConnected ? "Yes" : "No")}, IsOpen: {(this._statelessSession.IsOpen ? "Yes" : "No")}, HasTransaction: {(this._transaction != null ? "Yes" : "No")}, TransactionActive: {(this._transaction != null && this._transaction.IsActive ? "Yes" : "No")}");
            Trace.WriteLine("Date and Time UTC: " + DateTime.UtcNow.ToString(Format.YearMonthDayHourMinuteSecondMillisecond));
            Trace.WriteLine(eventFooter);
            Trace.WriteLine("");
        }
#endif
    }
}
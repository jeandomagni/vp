﻿using System;
using System.ComponentModel.Design;
using System.Web;
using Elmah;

namespace Ivh.Common.Elmah
{
    public static class ElmahConfig
    {
        private static string _applicationName;
        private static string _connectionString;

        public static void Configure(string connectionString)
        {
            _applicationName = Ivh.Common.Properties.Settings.Default.ApplicationName ?? "Unknown";
            _connectionString = connectionString;

            HttpApplication.RegisterModule(typeof(ErrorLogModule));
            HttpApplication.RegisterModule(typeof(ErrorFilterModule));

            ServiceCenter.Current = ElmahServiceProviderQueryHandler;
        }

        private static IServiceProvider ElmahServiceProviderQueryHandler(object context)
        {
            var container = new ServiceContainer(context as IServiceProvider);
            container.AddService(typeof(ErrorLog), new SqlErrorLog(_connectionString)
            {
                ApplicationName = _applicationName
            });

            return container;
        }
    }
}

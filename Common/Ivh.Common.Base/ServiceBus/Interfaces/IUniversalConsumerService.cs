﻿namespace Ivh.Common.ServiceBus.Interfaces
{
    using Ivh.Common.ServiceBus.Common.Messages;
    using MassTransit;

    public interface IUniversalConsumerService
    {
    }

    public interface IUniversalConsumerService<in TMessage> : IUniversalConsumerService
        where TMessage : UniversalMessage//, IUniversalMessage<TMessage>
    {
        void ConsumeMessage(TMessage message, ConsumeContext<TMessage> consumeContext);

        bool IsMessageValid(TMessage message, ConsumeContext<TMessage> consumeContext);
    }
}
﻿namespace Ivh.Common.ServiceBus.Interfaces
{
    using System;
    using Ivh.Common.ServiceBus.Common.Messages;
    using MassTransit;

    public interface IUniversalConsumer<in TMessage>: 
        IConsumer<Fault<TMessage>>,
        IConsumer<TMessage>,
        IConsumeMessageObserver<TMessage>,
        IMessageConsumer<TMessage>,
        IDisposable
        where TMessage : UniversalMessage
    {
    }
}

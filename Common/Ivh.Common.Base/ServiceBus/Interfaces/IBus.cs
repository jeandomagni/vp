﻿namespace Ivh.Common.ServiceBus.Interfaces
{
    using System.Runtime.CompilerServices;
    using System.Threading.Tasks;
    using Ivh.Common.ServiceBus.Common.Messages;
    using MassTransit;

    public interface IBus
    {
#if CALLERINFO
        Task PublishMessage<TMessage>(TMessage message, [CallerMemberName] string memberName = "", [CallerFilePath] string sourceFilePath = "", [CallerLineNumber] int sourceLineNumber = 0) where TMessage : UniversalMessage;
        Task PublishMessage<TMessage, TCorrelationId>(TMessage message, [CallerMemberName] string memberName = "", [CallerFilePath] string sourceFilePath = "", [CallerLineNumber] int sourceLineNumber = 0) where TMessage : class, CorrelatedBy<TCorrelationId>;
#else
        Task PublishMessage<TMessage>(TMessage message) where TMessage : UniversalMessage;
        Task PublishMessage<TMessage, TCorrelationId>(TMessage message) where TMessage : class, CorrelatedBy<TCorrelationId>;
#endif 

    }
}

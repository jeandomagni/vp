﻿namespace Ivh.Common.ServiceBus
{
    using System;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using System.Runtime.Serialization;
    using System.Threading.Tasks;
    using Base.Utilities.Extensions;
    using Ivh.Common.ServiceBus.Common.Messages;
    using Ivh.Common.VisitPay.Strings;
    using MassTransit;
    using VisitPay.Messages.Logging;
    using CorrelationManager = Base.Logging.CorrelationManager;

    public class Bus : Interfaces.IBus
    {
        private readonly IBus _bus;

        public Bus(MassTransit.IBus bus)
        {
            this._bus = bus;
        }
#if CALLERINFO
        public async Task PublishMessage<TMessage>(TMessage message, [CallerMemberName] string memberName = "", [CallerFilePath] string sourceFilePath = "", [CallerLineNumber] int sourceLineNumber = 0) where TMessage : UniversalMessage
#else
        public async Task PublishMessage<TMessage>(TMessage message) where TMessage : UniversalMessage
#endif
        {
            message.CorrelationId = CorrelationManager.CorrelationId;
            message.Timing.Publish = DateTime.UtcNow;
#if CALLERINFO
            this.WriteTraceInfo(message, memberName, sourceFilePath, sourceLineNumber);
#endif
#if DEBUG
            if (!message.IsSerializable())
            {
                throw new SerializationException($"nameof{message}: {typeof(TMessage).FullName} is not marked with the {nameof(SerializableAttribute)}");
            }
            if (!message.IsDataContract())
            {
                throw new SerializationException($"nameof{message}: {typeof(TMessage).FullName} is not marked with the {nameof(DataContractAttribute)}");
            }
#endif
            await this._bus.Publish<TMessage>(message).ConfigureAwait(false);
        }

#if CALLERINFO
        public async Task PublishMessage<TMessage, TCorrelationId>(TMessage message, [CallerMemberName] string memberName = "", [CallerFilePath] string sourceFilePath = "", [CallerLineNumber] int sourceLineNumber = 0) where TMessage : class, CorrelatedBy<TCorrelationId>
#else
        public async Task PublishMessage<TMessage, TCorrelationId>(TMessage message) where TMessage : class, CorrelatedBy<TCorrelationId>
#endif
        {
#if CALLERINFO
            this.WriteTraceInfo(message, memberName, sourceFilePath, sourceLineNumber);
#endif
            await this._bus.Publish(message).ConfigureAwait(false);
        }

#if CALLERINFO
        private void WriteTraceInfo<TMessage>(TMessage message, string memberName, string sourceFilePath, int sourceLineNumber)
        {
            if (typeof(TMessage) != typeof(LogMessage))
            {
                string eventDescription = $"----------------MESSAGE PUBLISHED {typeof(TMessage).Name}----------------";
                string eventFooter = new String('-', eventDescription.Length);

                Trace.WriteLine("");
                Trace.WriteLine(eventDescription);
                Trace.WriteLine($"Caller: {memberName} at line number {sourceLineNumber} in {sourceFilePath}");
                Trace.WriteLine($"Message {message.ToJSON(false, true, true)}");
                Trace.WriteLine("Date and Time UTC: " + DateTime.UtcNow.ToString(Format.YearMonthDayHourMinuteSecondMillisecond));
                Trace.WriteLine(eventFooter);
                Trace.WriteLine("");
            }
        }
#endif
    }
}

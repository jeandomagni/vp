﻿namespace Ivh.Common.ServiceBus.Attributes
{
    using System;
    using System.ComponentModel;
    using Enums;

    [AttributeUsage(AttributeTargets.Method)]
    public class UniversalConsumerAttribute : Attribute
    {
        public UniversalQueuesEnum Queue { get; private set; }
        public UniversalPriorityEnum Priority { get; private set; }


        public UniversalConsumerAttribute(UniversalQueuesEnum universalQueuesEnum, UniversalPriorityEnum universalPriorityEnum)
        {
            this.Queue = universalQueuesEnum;
            this.Priority = universalPriorityEnum;
        }

        public UniversalConsumerAttribute(UniversalQueuesEnum universalQueuesEnum)
        {
            this.Queue = universalQueuesEnum;

            Type t = typeof(UniversalPriorityEnum);
            DefaultValueAttribute[] attributes = (DefaultValueAttribute[])t.GetCustomAttributes(typeof(DefaultValueAttribute), false);
            if (attributes.Length > 0)
            {
                this.Priority = (UniversalPriorityEnum)attributes[0].Value;
            }
            else
            {
                this.Priority = default(UniversalPriorityEnum);
            }

        }
    }
}

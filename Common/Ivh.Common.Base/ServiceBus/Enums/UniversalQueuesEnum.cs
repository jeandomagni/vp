﻿namespace Ivh.Common.ServiceBus.Enums
{
    public enum UniversalQueuesEnum
    {
        SystemManagementProcessor,
        VisitPayProcessor,
        HsDataProcessor,
        BalanceTransferProcessor,
        ScheduleService,
        AgingProcessor,
        MatchingProcessor
    }
}

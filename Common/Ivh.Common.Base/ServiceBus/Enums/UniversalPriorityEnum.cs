﻿namespace Ivh.Common.ServiceBus.Enums
{
    using System;
    using System.ComponentModel;

    [DefaultValue(Priority30)]
    public enum UniversalPriorityEnum : byte
    {
        Priority00 = byte.MaxValue,
#pragma warning disable 618
        Priority10 = (Priority00 + Priority20) / 2,
        Priority20 = (Priority00 + Priority40) / 2,
#pragma warning restore 618
        Priority30 = (Priority20 + Priority40) / 2,
        Priority40 = byte.MinValue,
    }
}

﻿namespace Ivh.Common.EventJournal
{
    using System;
    using VisitPay.Constants;
    using VisitPay.Enums;

    public class JournalEvent
    {
        private int _eventVisitPayUserId;
        private int? _visitPayUserId;
        private int? _vpGuarantorId;

        public virtual int JournalEventId { get; set; }
        public virtual DateTime EventDateTime { get; set; }
        public virtual JournalEventTypeEnum JournalEventType { get; set; }
        public virtual JournalEventCategoryEnum JournalEventCategory { get; set; }
        public virtual string EventTags { get; set; }
        public virtual Guid CorrelationId { get; set; }
        public virtual ApplicationEnum Application { get; set; }

        public virtual int EventVisitPayUserId
        {
            get => this._eventVisitPayUserId == default(int) ? SystemUsers.SystemUserId : this._eventVisitPayUserId;
            set => this._eventVisitPayUserId = value == default(int) ? SystemUsers.SystemUserId : value;
        }

        public virtual DeviceTypeEnum? DeviceType { get; set; }
        public virtual string UserAgentString { get; set; }
        public virtual int UserAgentId { get; set; }
        public virtual string UserHostAddress { get; set; }
        public virtual string JournalUserEventContext { get; set; }

        public virtual int? VisitPayUserId
        {
            get => this._visitPayUserId.HasValue && this._visitPayUserId.Value == default(int) ? null : this._visitPayUserId;
            set => this._visitPayUserId = value.HasValue && value == default(int) ? null : value;
        }

        public virtual int? VpGuarantorId
        {
            get => this._vpGuarantorId.HasValue && this._vpGuarantorId.Value == default(int) ? null : this._vpGuarantorId;
            set => this._vpGuarantorId = value.HasValue && value == default(int) ? null : value;
        }

        public virtual int? HsGuarantorId { get; set; }
        public virtual int? VisitId { get; set; }
        public virtual int? VisitTransactionId { get; set; }
        public virtual int? PaymentId { get; set; }
        public virtual int? FinancePlanId { get; set; }
        public virtual string JournalEventContext { get; set; }

        public virtual string Message { get; set; }
        public virtual string AdditionalData { get; set; }
    }
}

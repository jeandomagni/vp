﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ivh.Common.EventJournal.Exceptions
{
    public class JournalEventContextValueMissingException : Exception
    {
        public JournalEventContextValueMissingException()
        {
        }

        public JournalEventContextValueMissingException(string message)
            : base(message)
        {
        }

        public JournalEventContextValueMissingException(string message, Exception inner)
            : base(message, inner)
        {
        }
    }
}

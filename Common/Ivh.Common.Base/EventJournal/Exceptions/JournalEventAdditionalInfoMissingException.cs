﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ivh.Common.EventJournal.Exceptions
{
    public class JournalEventAdditionalInfoMissingException : Exception
    {
        public JournalEventAdditionalInfoMissingException()
        {
        }

        public JournalEventAdditionalInfoMissingException(string message)
            : base(message)
        {
        }

        public JournalEventAdditionalInfoMissingException(string message, Exception inner)
            : base(message, inner)
        {
        }
    }
}

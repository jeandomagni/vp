﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ivh.Common.EventJournal.Exceptions
{
    public class JournalEventTagMissingException : Exception
    {
        public JournalEventTagMissingException()
        {
        }

        public JournalEventTagMissingException(string message)
            : base(message)
        {
        }

        public JournalEventTagMissingException(string message, Exception inner)
            : base(message, inner)
        {
        }
    }
}

namespace Ivh.Common.EventJournal.Templates.SystemAction
{
	using System.Dynamic;
	using Base.Enums;
	using EventJournal;
	using Constants;
	using VisitPay.Enums;


    public class EventSystemActionSsoInvalidated : JournalEventTemplate<JournalEventParameters>
	{
		public static JournalEventParameters GetParameters( string ssoProviderId, string ssoProviderName)
		{
			//Property names are case sensitive and must match AdditionalInfo and HumanReadableDescription [tags] exactly
			dynamic parameters = new ExpandoObject();
			parameters.SsoProviderId = ssoProviderId;
			parameters.SsoProviderName = ssoProviderName;
			return new JournalEventParameters(parameters);
		}

		public override JournalEventCategoryEnum EventRuleTrigger => JournalEventCategoryEnum.SystemAction;
		public override string[] EventRuleTags => new[] { JournalEventTags.Sso };
		public override string EventRuleName => "SystemActionSsoInvalidated";
		public override JournalEventTypeEnum AuditLogId => JournalEventTypeEnum.SystemActionSsoInvalidated;
		public override string Description => "Generated when the VP System disabled a user’s SSO because it revoked a certificate (SSN changed) VPNG-16328 (HQY)";
		public override bool TriggeredByEventUserId => true;
		public override bool AppliedToHsGId => true;
		public override bool AppliedToVpGId => true;
		public override bool AppliedToVpUserIdClientUserId => true;
		public override bool AppliedToFpId => false;
		public override bool AppliedToVisitId => false;
		public override bool AppliedToTransId => false;
		public override bool AppliedToPaymentId => false;
		public override string[] AdditionalInfo => new string[] { "SsoProviderId" };
		public override bool Audit => false;
		public override string HumanReadableDescription => "The System disabled VP User {VPUserName]'s Sso certificate from [SsoProviderName]";
	}
}
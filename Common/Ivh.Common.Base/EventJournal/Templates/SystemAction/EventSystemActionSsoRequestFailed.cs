namespace Ivh.Common.EventJournal.Templates.SystemAction
{
	using System.Dynamic;
	using Base.Enums;
	using EventJournal;
	using Constants;
	using VisitPay.Enums;


    public class EventSystemActionSsoRequestFailed : JournalEventTemplate<JournalEventParameters>
	{
		public static JournalEventParameters GetParameters( string dateOfBirth, string ssoProviderId, string ssoProviderName, string ssoSystemSourceKey)
		{
			//Property names are case sensitive and must match AdditionalInfo and HumanReadableDescription [tags] exactly
			dynamic parameters = new ExpandoObject();
			parameters.DateOfBirth = dateOfBirth;
			parameters.SsoProviderId = ssoProviderId;
			parameters.SsoProviderName = ssoProviderName;
			parameters.SsoSystemSourceKey = ssoSystemSourceKey;
			return new JournalEventParameters(parameters);
		}

		public override JournalEventCategoryEnum EventRuleTrigger => JournalEventCategoryEnum.SystemAction;
		public override string[] EventRuleTags => new[] { JournalEventTags.Sso };
		public override string EventRuleName => "SystemActionSsoRequestFailed";
		public override JournalEventTypeEnum AuditLogId => JournalEventTypeEnum.SystemActionSsoRequestFailed;
		public override string Description => "Generated when an SSO setup request is sent but the validating data doesn’t match, so we can’t continue";
		public override bool TriggeredByEventUserId => true;
		public override bool AppliedToHsGId => false;
		public override bool AppliedToVpGId => false;
		public override bool AppliedToVpUserIdClientUserId => false;
		public override bool AppliedToFpId => false;
		public override bool AppliedToVisitId => false;
		public override bool AppliedToTransId => false;
		public override bool AppliedToPaymentId => false;
		public override string[] AdditionalInfo => new string[] { "SsoProviderId", "DateOfBirth", "SsoSystemSourceKey" };
		public override bool Audit => false;
		public override string HumanReadableDescription => "The System failed to validate an Sso Request from [SsoProviderName] for SsoSystemSourceKey: [SsoSystemSourceKey]";
	}
}
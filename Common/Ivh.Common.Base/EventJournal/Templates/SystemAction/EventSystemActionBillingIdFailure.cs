namespace Ivh.Common.EventJournal.Templates.SystemAction
{
	using System.Dynamic;
	using Base.Enums;
	using EventJournal;
	using Constants;
	using VisitPay.Enums;


    public class EventSystemActionBillingIdFailure : JournalEventTemplate<JournalEventParameters>
	{
		public static JournalEventParameters GetParameters( string accountNumber, string responseInfo, string tCResponse, string vpUserName)
		{
			//Property names are case sensitive and must match AdditionalInfo and HumanReadableDescription [tags] exactly
			dynamic parameters = new ExpandoObject();
			parameters.AccountNumber = accountNumber;
			parameters.ResponseInfo = responseInfo;
			parameters.TCResponse = tCResponse;
			parameters.VpUserName = vpUserName;
			return new JournalEventParameters(parameters);
		}

		public override JournalEventCategoryEnum EventRuleTrigger => JournalEventCategoryEnum.SystemAction;
		public override string[] EventRuleTags => new[] { JournalEventTags.PaymentMethods };
		public override string EventRuleName => "SystemActionBillingIdFailure";
		public override JournalEventTypeEnum AuditLogId => JournalEventTypeEnum.SystemActionBillingIdFailure;
		public override string Description => "Generated when there is an error when the payment method is sent to TC. Capture the response information.  VPNG-8440";
		public override bool TriggeredByEventUserId => true;
		public override bool AppliedToHsGId => true;
		public override bool AppliedToVpGId => true;
		public override bool AppliedToVpUserIdClientUserId => true;
		public override bool AppliedToFpId => false;
		public override bool AppliedToVisitId => false;
		public override bool AppliedToTransId => false;
		public override bool AppliedToPaymentId => false;
		public override string[] AdditionalInfo => new string[] { "AccountNumber", "TCResponse", "ResponseInfo" };
		public override bool Audit => false;
		public override string HumanReadableDescription => "The Payment Gateway returned a Error Response when a Payment Method was added for Visit Pay User [VpUserName]: [ResponseInfo]";
	}
}
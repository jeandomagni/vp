namespace Ivh.Common.EventJournal.Templates.SystemAction
{
	using System.Dynamic;
	using Base.Enums;
	using EventJournal;
	using Constants;
	using VisitPay.Enums;


    public class EventSystemActionPaymentStateChange : JournalEventTemplate<JournalEventParameters>
	{
		public static JournalEventParameters GetParameters( string newState, string paymentId, string previousState)
		{
			//Property names are case sensitive and must match AdditionalInfo and HumanReadableDescription [tags] exactly
			dynamic parameters = new ExpandoObject();
			parameters.NewState = newState;
			parameters.PaymentId = paymentId;
			parameters.PreviousState = previousState;
			return new JournalEventParameters(parameters);
		}

		public override JournalEventCategoryEnum EventRuleTrigger => JournalEventCategoryEnum.SystemAction;
		public override string[] EventRuleTags => new[] { JournalEventTags.Payments };
		public override string EventRuleName => "SystemActionPaymentStateChange";
		public override JournalEventTypeEnum AuditLogId => JournalEventTypeEnum.SystemActionPaymentStateChange;
		public override string Description => "Generated when the state of a payment changes";
		public override bool TriggeredByEventUserId => true;
		public override bool AppliedToHsGId => true;
		public override bool AppliedToVpGId => true;
		public override bool AppliedToVpUserIdClientUserId => true;
		public override bool AppliedToFpId => false;
		public override bool AppliedToVisitId => false;
		public override bool AppliedToTransId => false;
		public override bool AppliedToPaymentId => true;
		public override string[] AdditionalInfo => new string[] { "PreviousState", "NewState" };
		public override bool Audit => false;
		public override string HumanReadableDescription => "The System changed the state of Payment [PaymentId] from [PreviousState] to [NewState]";
	}
}
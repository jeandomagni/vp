﻿namespace Ivh.Common.EventJournal.Templates.SystemAction
{
    using System.Dynamic;
    using Base.Enums;
    using EventJournal;
    using Constants;
    using VisitPay.Enums;

    public class EventSystemActionGuestPaySessionTimeOut : JournalEventTemplate<JournalEventParameters>
    {
        public static JournalEventParameters GetParameters( string hsGuarantorId)
        {
            //Property names are case sensitive and must match AdditionalInfo and HumanReadableDescription [tags] exactly
            dynamic parameters = new ExpandoObject();
            parameters.HsGuarantorId = hsGuarantorId;
            return new JournalEventParameters(parameters);
        }

        public override JournalEventCategoryEnum EventRuleTrigger => JournalEventCategoryEnum.SystemAction;
        public override string[] EventRuleTags => new[] { JournalEventTags.AppAccess };
        public override string EventRuleName => "SystemActionGuestPaySessionTimeOut";
        public override JournalEventTypeEnum AuditLogId => JournalEventTypeEnum.SystemActionGuestPaySessionTimeOut;
        public override string Description => "Generated when a Guestpay session times out";
        public override bool TriggeredByEventUserId => true;
        public override bool AppliedToHsGId => true;
        public override bool AppliedToVpGId => false;
        public override bool AppliedToVpUserIdClientUserId => false;
        public override bool AppliedToFpId => false;
        public override bool AppliedToVisitId => false;
        public override bool AppliedToTransId => false;
        public override bool AppliedToPaymentId => false;
        public override string[] AdditionalInfo => new string[] {};
        public override bool Audit => false;
        public override string HumanReadableDescription => "The System ended [HsGuarantorId]'s session timed out due to inactivity";
    }
}

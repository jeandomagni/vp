namespace Ivh.Common.EventJournal.Templates.SystemAction
{
	using System.Dynamic;
	using Base.Enums;
	using EventJournal;
	using Constants;
	using VisitPay.Enums;


    public class EventSystemActionVisitAgeChange : JournalEventTemplate<JournalEventParameters>
	{
		public static JournalEventParameters GetParameters( string newAge, string previousAge, string vpUserName, string vpVisitId)
		{
			//Property names are case sensitive and must match AdditionalInfo and HumanReadableDescription [tags] exactly
			dynamic parameters = new ExpandoObject();
			parameters.NewAge = newAge;
			parameters.PreviousAge = previousAge;
			parameters.VpUserName = vpUserName;
			parameters.VpVisitId = vpVisitId;
			return new JournalEventParameters(parameters);
		}

		public override JournalEventCategoryEnum EventRuleTrigger => JournalEventCategoryEnum.SystemAction;
		public override string[] EventRuleTags => new[] { JournalEventTags.Visits };
		public override string EventRuleName => "SystemActionVisitAgeChange";
		public override JournalEventTypeEnum AuditLogId => JournalEventTypeEnum.SystemActionVisitAgeChange;
		public override string Description => "Generated when the age of a Visit Changes VPNG-13591";
		public override bool TriggeredByEventUserId => true;
		public override bool AppliedToHsGId => true;
		public override bool AppliedToVpGId => true;
		public override bool AppliedToVpUserIdClientUserId => true;
		public override bool AppliedToFpId => false;
		public override bool AppliedToVisitId => true;
		public override bool AppliedToTransId => false;
		public override bool AppliedToPaymentId => false;
		public override string[] AdditionalInfo => new string[] { "PreviousAge", "NewAge" };
		public override bool Audit => false;
		public override string HumanReadableDescription => "The System changed the age of VP Visit [VpVisitId] from [PreviousAge] to [NewAge] for VP User [VpUserName]";
	}
}
namespace Ivh.Common.EventJournal.Templates.SystemAction
{
	using System.Dynamic;
	using Base.Enums;
	using EventJournal;
	using Constants;
	using VisitPay.Enums;


    public class EventSystemActionRegistrationCompleteSso : JournalEventTemplate<JournalEventParameters>
	{
		public static JournalEventParameters GetParameters( string dateOfBirth, string hsGuarantorId, string lastName, string sSN4, string ssoProviderId, string ssoProviderName, string vpGuarantorId, string vpUserName, string zip)
		{
			//Property names are case sensitive and must match AdditionalInfo and HumanReadableDescription [tags] exactly
			dynamic parameters = new ExpandoObject();
			parameters.DateOfBirth = dateOfBirth;
			parameters.HsGuarantorId = hsGuarantorId;
			parameters.LastName = lastName;
			parameters.SSN4 = sSN4;
			parameters.SsoProviderId = ssoProviderId;
			parameters.SsoProviderName = ssoProviderName;
			parameters.VpGuarantorId = vpGuarantorId;
			parameters.VpUserName = vpUserName;
			parameters.Zip = zip;
			return new JournalEventParameters(parameters);
		}

		public override JournalEventCategoryEnum EventRuleTrigger => JournalEventCategoryEnum.SystemAction;
		public override string[] EventRuleTags => new[] { JournalEventTags.Registration };
		public override string EventRuleName => "SystemActionRegistrationCompleteSso";
		public override JournalEventTypeEnum AuditLogId => JournalEventTypeEnum.SystemActionRegistrationCompleteSso;
		public override string Description => "Generated when Registration is completed - that came from an SSO source VPNG-13058, VPNG-16526";
		public override bool TriggeredByEventUserId => true;
		public override bool AppliedToHsGId => true;
		public override bool AppliedToVpGId => true;
		public override bool AppliedToVpUserIdClientUserId => true;
		public override bool AppliedToFpId => false;
		public override bool AppliedToVisitId => false;
		public override bool AppliedToTransId => false;
		public override bool AppliedToPaymentId => false;
		public override string[] AdditionalInfo => new string[] { "LastName", "Zip", "SSN4", "DateOfBirth", "VpGuarantorId", "SsoProviderId" };
		public override bool Audit => false;
		public override string HumanReadableDescription => "The System completed the Sso registration process for HSGID: [HsGuarantorId] - VP User: [VpUserName] from Sso Provider: [SsoProviderName]";
	}
}
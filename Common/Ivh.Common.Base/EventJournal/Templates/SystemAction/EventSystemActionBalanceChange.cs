namespace Ivh.Common.EventJournal.Templates.SystemAction
{
	using System.Dynamic;
	using Base.Enums;
	using EventJournal;
	using Constants;
	using VisitPay.Enums;


    public class EventSystemActionBalanceChange : JournalEventTemplate<JournalEventParameters>
	{
		public static JournalEventParameters GetParameters( string newBalance, string previousBalance, string vpUserName, string vpVisitId)
		{
			//Property names are case sensitive and must match AdditionalInfo and HumanReadableDescription [tags] exactly
			dynamic parameters = new ExpandoObject();
			parameters.NewBalance = newBalance;
			parameters.PreviousBalance = previousBalance;
			parameters.VpUserName = vpUserName;
			parameters.VpVisitId = vpVisitId;
			return new JournalEventParameters(parameters);
		}

		public override JournalEventCategoryEnum EventRuleTrigger => JournalEventCategoryEnum.SystemAction;
		public override string[] EventRuleTags => new[] { JournalEventTags.Visits };
		public override string EventRuleName => "SystemActionBalanceChange";
		public override JournalEventTypeEnum AuditLogId => JournalEventTypeEnum.SystemActionBalanceChange;
		public override string Description => "Generated when the HSBalance changes on a Visit";
		public override bool TriggeredByEventUserId => true;
		public override bool AppliedToHsGId => true;
		public override bool AppliedToVpGId => true;
		public override bool AppliedToVpUserIdClientUserId => true;
		public override bool AppliedToFpId => false;
		public override bool AppliedToVisitId => true;
		public override bool AppliedToTransId => false;
		public override bool AppliedToPaymentId => false;
		public override string[] AdditionalInfo => new string[] { "PreviousBalance", "NewBalance" };
		public override bool Audit => false;
		public override string HumanReadableDescription => "The System updated the HS Balance for VP Visit [VpVisitId] from [PreviousBalance] to [NewBalance] for VP User [VpUserName]";
	}
}
namespace Ivh.Common.EventJournal.Templates.SystemAction
{
	using System.Dynamic;
	using Base.Enums;
	using EventJournal;
	using Constants;
	using VisitPay.Enums;


    public class EventSystemActionClientUserAcctLockedDueToFailedLogins : JournalEventTemplate<JournalEventParameters>
	{
		public static JournalEventParameters GetParameters( string clientUserName)
		{
			//Property names are case sensitive and must match AdditionalInfo and HumanReadableDescription [tags] exactly
			dynamic parameters = new ExpandoObject();
			parameters.ClientUserName = clientUserName;
			return new JournalEventParameters(parameters);
		}

		public override JournalEventCategoryEnum EventRuleTrigger => JournalEventCategoryEnum.SystemAction;
		public override string[] EventRuleTags => new[] { JournalEventTags.AppAccess };
		public override string EventRuleName => "SystemActionClientUserAcctLockedDueToFailedLogins";
		public override JournalEventTypeEnum AuditLogId => JournalEventTypeEnum.SystemActionClientUserAcctLockedDueToFailedLogins;
		public override string Description => "Generated when a client user acct is locked when it exceeds the max number of allowed login attempts";
		public override bool TriggeredByEventUserId => true;
		public override bool AppliedToHsGId => false;
		public override bool AppliedToVpGId => false;
		public override bool AppliedToVpUserIdClientUserId => true;
		public override bool AppliedToFpId => false;
		public override bool AppliedToVisitId => false;
		public override bool AppliedToTransId => false;
		public override bool AppliedToPaymentId => false;
		public override string[] AdditionalInfo => new string[] {};
		public override bool Audit => false;
		public override string HumanReadableDescription => "The System locked [ClientUserName]'s account - exceeded the maximum number of failed login attempts";
	}
}
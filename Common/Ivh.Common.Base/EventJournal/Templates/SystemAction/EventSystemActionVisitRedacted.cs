namespace Ivh.Common.EventJournal.Templates.SystemAction
{
	using System.Dynamic;
	using Base.Enums;
	using EventJournal;
	using Constants;
	using VisitPay.Enums;


    public class EventSystemActionVisitRedacted : JournalEventTemplate<JournalEventParameters>
	{
		public static JournalEventParameters GetParameters( string visitSourceSystemKey, string vpVisitId)
		{
			//Property names are case sensitive and must match AdditionalInfo and HumanReadableDescription [tags] exactly
			dynamic parameters = new ExpandoObject();
			parameters.VisitSourceSystemKey = visitSourceSystemKey;
			parameters.VpVisitId = vpVisitId;
			return new JournalEventParameters(parameters);
		}

		public override JournalEventCategoryEnum EventRuleTrigger => JournalEventCategoryEnum.SystemAction;
		public override string[] EventRuleTags => new[] { JournalEventTags.MatchUnmatch };
		public override string EventRuleName => "SystemActionVisitRedacted";
		public override JournalEventTypeEnum AuditLogId => JournalEventTypeEnum.SystemActionVisitRedacted;
		public override string Description => "Generated when Visit Data is redacted (removed). This generally occurs after a Visit has been UnMatched (visis is in a Closed-Removed state) VPNG-4971";
		public override bool TriggeredByEventUserId => true;
		public override bool AppliedToHsGId => true;
		public override bool AppliedToVpGId => true;
		public override bool AppliedToVpUserIdClientUserId => true;
		public override bool AppliedToFpId => false;
		public override bool AppliedToVisitId => true;
		public override bool AppliedToTransId => false;
		public override bool AppliedToPaymentId => false;
		public override string[] AdditionalInfo => new string[] { "VisitSourceSystemKey" };
		public override bool Audit => false;
		public override string HumanReadableDescription => "The System redacted Visit [VpVisitId]";
	}
}
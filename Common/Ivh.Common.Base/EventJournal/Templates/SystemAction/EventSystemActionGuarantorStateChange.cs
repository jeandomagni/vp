namespace Ivh.Common.EventJournal.Templates.SystemAction
{
	using System.Dynamic;
	using Base.Enums;
	using EventJournal;
	using Constants;
	using VisitPay.Enums;


    public class EventSystemActionGuarantorStateChange : JournalEventTemplate<JournalEventParameters>
	{
		public static JournalEventParameters GetParameters( string newState, string previousState, string vpUserName)
		{
			//Property names are case sensitive and must match AdditionalInfo and HumanReadableDescription [tags] exactly
			dynamic parameters = new ExpandoObject();
			parameters.NewState = newState;
			parameters.PreviousState = previousState;
			parameters.VpUserName = vpUserName;
			return new JournalEventParameters(parameters);
		}

		public override JournalEventCategoryEnum EventRuleTrigger => JournalEventCategoryEnum.SystemAction;
		public override string[] EventRuleTags => new[] { JournalEventTags.Guarantor };
		public override string EventRuleName => "SystemActionGuarantorStateChange";
		public override JournalEventTypeEnum AuditLogId => JournalEventTypeEnum.SystemActionGuarantorStateChange;
		public override string Description => "Generated when the state of a Guarantor changes";
		public override bool TriggeredByEventUserId => true;
		public override bool AppliedToHsGId => true;
		public override bool AppliedToVpGId => true;
		public override bool AppliedToVpUserIdClientUserId => true;
		public override bool AppliedToFpId => false;
		public override bool AppliedToVisitId => false;
		public override bool AppliedToTransId => false;
		public override bool AppliedToPaymentId => false;
		public override string[] AdditionalInfo => new string[] { "PreviousState", "NewState" };
		public override bool Audit => false;
		public override string HumanReadableDescription => "The System changed the state of VP Guarantor [VpUserName] from [PreviousState] to [NewState]";
	}
}
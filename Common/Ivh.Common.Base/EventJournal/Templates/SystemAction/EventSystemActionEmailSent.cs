namespace Ivh.Common.EventJournal.Templates.SystemAction
{
	using System.Dynamic;
	using Base.Enums;
	using EventJournal;
	using Constants;
	using VisitPay.Enums;


    public class EventSystemActionEmailSent : JournalEventTemplate<JournalEventParameters>
	{
		public static JournalEventParameters GetParameters( string emailContentTitle, string emailId, string notificationEmail, string typeName, string vpUserName)
		{
			//Property names are case sensitive and must match AdditionalInfo and HumanReadableDescription [tags] exactly
			dynamic parameters = new ExpandoObject();
			parameters.EmailContentTitle = emailContentTitle;
			parameters.EmailId = emailId;
			parameters.NotificationEmail = notificationEmail;
			parameters.TypeName = typeName;
			parameters.VpUserName = vpUserName;
			return new JournalEventParameters(parameters);
		}

		public override JournalEventCategoryEnum EventRuleTrigger => JournalEventCategoryEnum.SystemAction;
		public override string[] EventRuleTags => new[] { JournalEventTags.Communication };
		public override string EventRuleName => "SystemActionEmailSent";
		public override JournalEventTypeEnum AuditLogId => JournalEventTypeEnum.SystemActionEmailSent;
		public override string Description => "Generated when an email is sent to a VPG";
		public override bool TriggeredByEventUserId => true;
		public override bool AppliedToHsGId => true;
		public override bool AppliedToVpGId => true;
		public override bool AppliedToVpUserIdClientUserId => true;
		public override bool AppliedToFpId => false;
		public override bool AppliedToVisitId => false;
		public override bool AppliedToTransId => false;
		public override bool AppliedToPaymentId => false;
		public override string[] AdditionalInfo => new string[] { "EmailId", "TypeName", "NotificationEmail" };
		public override bool Audit => false;
		public override string HumanReadableDescription => "The System sent a [EmailContentTitle] email to [NotificationEmail] for [VpUserName] ";
	}
}
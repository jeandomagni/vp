namespace Ivh.Common.EventJournal.Templates.SystemAction
{
	using System.Dynamic;
	using Base.Enums;
	using EventJournal;
	using Constants;
	using VisitPay.Enums;


    public class EventSystemActionRegistrationAuthFailure : JournalEventTemplate<JournalEventParameters>
	{
		public static JournalEventParameters GetParameters( string dateOfBirth, string hsGuarantorId, string lastName, string sSN4, string zip)
		{
			//Property names are case sensitive and must match AdditionalInfo and HumanReadableDescription [tags] exactly
			dynamic parameters = new ExpandoObject();
			parameters.DateOfBirth = dateOfBirth;
			parameters.HsGuarantorId = hsGuarantorId;
			parameters.LastName = lastName;
			parameters.SSN4 = sSN4;
			parameters.Zip = zip;
			return new JournalEventParameters(parameters);
		}

		public override JournalEventCategoryEnum EventRuleTrigger => JournalEventCategoryEnum.SystemAction;
		public override string[] EventRuleTags => new[] { JournalEventTags.Registration };
		public override string EventRuleName => "SystemActionRegistrationAuthFailure";
		public override JournalEventTypeEnum AuditLogId => JournalEventTypeEnum.SystemActionRegistrationAuthFailure;
		public override string Description => "Generated when the registration authentication fails";
		public override bool TriggeredByEventUserId => true;
		public override bool AppliedToHsGId => true;
		public override bool AppliedToVpGId => false;
		public override bool AppliedToVpUserIdClientUserId => false;
		public override bool AppliedToFpId => false;
		public override bool AppliedToVisitId => false;
		public override bool AppliedToTransId => false;
		public override bool AppliedToPaymentId => false;
		public override string[] AdditionalInfo => new string[] { "LastName", "Zip", "SSN4", "DateOfBirth" };
		public override bool Audit => false;
		public override string HumanReadableDescription => "The System failed to authenticate HSGID [HsGuarantorId] during registration.  Values: LastName = [LastName], Zip = [Zip], SSN = [SSN4], DateOfBirth = [DateOfBirth]";
	}
}
namespace Ivh.Common.EventJournal.Templates.SystemAction
{
	using System.Dynamic;
	using Base.Enums;
	using EventJournal;
	using Constants;
	using VisitPay.Enums;


    public class EventSystemActionSsoMatchedNotRegistered : JournalEventTemplate<JournalEventParameters>
	{
		public static JournalEventParameters GetParameters( string dateOfBirth, string hSGuarantorMatches, string ssoProviderId, string ssoProviderName, string ssoSystemSourceKey)
		{
			//Property names are case sensitive and must match AdditionalInfo and HumanReadableDescription [tags] exactly
			dynamic parameters = new ExpandoObject();
			parameters.DateOfBirth = dateOfBirth;
			parameters.HSGuarantorMatches = hSGuarantorMatches;
			parameters.SsoProviderId = ssoProviderId;
			parameters.SsoProviderName = ssoProviderName;
			parameters.SsoSystemSourceKey = ssoSystemSourceKey;
			return new JournalEventParameters(parameters);
		}

		public override JournalEventCategoryEnum EventRuleTrigger => JournalEventCategoryEnum.SystemAction;
		public override string[] EventRuleTags => new[] { JournalEventTags.Sso };
		public override string EventRuleName => "SystemActionSsoMatchedNotRegistered";
		public override JournalEventTypeEnum AuditLogId => JournalEventTypeEnum.SystemActionSsoMatchedNotRegistered;
		public override string Description => "Generated when an SSO setup request matches a valid HSGuarantor but does not match a VPGuarantor. They must complete registration before continuing VPNG-10603 (SLHS Mychart) ";
		public override bool TriggeredByEventUserId => true;
		public override bool AppliedToHsGId => true;
		public override bool AppliedToVpGId => false;
		public override bool AppliedToVpUserIdClientUserId => false;
		public override bool AppliedToFpId => false;
		public override bool AppliedToVisitId => false;
		public override bool AppliedToTransId => false;
		public override bool AppliedToPaymentId => false;
		public override string[] AdditionalInfo => new string[] { "SsoProviderId", "DateOfBirth", "SsoSystemSourceKey", "HSGuarantorMatches" };
		public override bool Audit => false;
		public override string HumanReadableDescription => "The System matched a valid Sso request to a HSGID without a matching VpGuarantorId,  from [SsoProviderName] for SsoSystemSourceKey: [SsoSystemSourceKey]";
	}
}
namespace Ivh.Common.EventJournal.Templates.SystemAction
{
	using System.Dynamic;
	using Base.Enums;
	using EventJournal;
	using Constants;
	using VisitPay.Enums;


    public class EventSystemActionFinancialAssistanceFlagApplied : JournalEventTemplate<JournalEventParameters>
	{
		public static JournalEventParameters GetParameters( string vpUserName, string vpVisitId)
		{
			//Property names are case sensitive and must match AdditionalInfo and HumanReadableDescription [tags] exactly
			dynamic parameters = new ExpandoObject();
			parameters.VpUserName = vpUserName;
			parameters.VpVisitId = vpVisitId;
			return new JournalEventParameters(parameters);
		}

		public override JournalEventCategoryEnum EventRuleTrigger => JournalEventCategoryEnum.SystemAction;
		public override string[] EventRuleTags => new[] { JournalEventTags.Visits };
		public override string EventRuleName => "SystemActionFinancialAssistanceFlagApplied";
		public override JournalEventTypeEnum AuditLogId => JournalEventTypeEnum.SystemActionFinancialAssistanceFlagApplied;
		public override string Description => "Generated when a Finance Assistance Flag is applied";
		public override bool TriggeredByEventUserId => true;
		public override bool AppliedToHsGId => true;
		public override bool AppliedToVpGId => true;
		public override bool AppliedToVpUserIdClientUserId => true;
		public override bool AppliedToFpId => false;
		public override bool AppliedToVisitId => true;
		public override bool AppliedToTransId => false;
		public override bool AppliedToPaymentId => false;
		public override string[] AdditionalInfo => new string[] {};
		public override bool Audit => false;
		public override string HumanReadableDescription => "The System applied a Finance Assistance flag to VP Visit [VpVisitId] for VP User [VpUserName]";
	}
}
namespace Ivh.Common.EventJournal.Templates.SystemAction
{
	using System.Dynamic;
	using Base.Enums;
	using EventJournal;
	using Constants;
	using VisitPay.Enums;


    public class EventSystemActionHsGuarantorIdVpGuarantorIdGuarantorMatched : JournalEventTemplate<JournalEventParameters>
	{
		public static JournalEventParameters GetParameters( string hsGuarantorId, string matchEvent, string vpGuarantorId)
		{
			//Property names are case sensitive and must match AdditionalInfo and HumanReadableDescription [tags] exactly
			dynamic parameters = new ExpandoObject();
			parameters.HsGuarantorId = hsGuarantorId;
			parameters.MatchEvent = matchEvent;
			parameters.VpGuarantorId = vpGuarantorId;
			return new JournalEventParameters(parameters);
		}

		public override JournalEventCategoryEnum EventRuleTrigger => JournalEventCategoryEnum.SystemAction;
		public override string[] EventRuleTags => new[] { JournalEventTags.MatchUnmatch };
		public override string EventRuleName => "SystemActionHsGuarantorIdVpGuarantorIdGuarantorMatched";
		public override JournalEventTypeEnum AuditLogId => JournalEventTypeEnum.SystemActionHsGuarantorIdVpGuarantorIdGuarantorMatched;
		public override string Description => "Generated when a HSGID and a VpGuarantorId are matched VPNG-5449";
		public override bool TriggeredByEventUserId => true;
		public override bool AppliedToHsGId => true;
		public override bool AppliedToVpGId => true;
		public override bool AppliedToVpUserIdClientUserId => true;
		public override bool AppliedToFpId => false;
		public override bool AppliedToVisitId => false;
		public override bool AppliedToTransId => false;
		public override bool AppliedToPaymentId => false;
		public override string[] AdditionalInfo => new string[] { "MatchEvent" };
		public override bool Audit => false;
		public override string HumanReadableDescription => "The System matched VpGuarantorId [VpGuarantorId] to HSGID [HsGuarantorId] during [MatchEvent]";
	}
}
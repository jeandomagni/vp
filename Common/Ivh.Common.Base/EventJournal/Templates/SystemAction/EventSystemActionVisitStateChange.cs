namespace Ivh.Common.EventJournal.Templates.SystemAction
{
	using System.Dynamic;
	using Base.Enums;
	using EventJournal;
	using Constants;
	using VisitPay.Enums;


    public class EventSystemActionVisitStateChange : JournalEventTemplate<JournalEventParameters>
	{
		public static JournalEventParameters GetParameters( string newState, string previousState, string vpUserName, string vpVisitId)
		{
			//Property names are case sensitive and must match AdditionalInfo and HumanReadableDescription [tags] exactly
			dynamic parameters = new ExpandoObject();
			parameters.NewState = newState;
			parameters.PreviousState = previousState;
			parameters.VpUserName = vpUserName;
			parameters.VpVisitId = vpVisitId;
			return new JournalEventParameters(parameters);
		}

		public override JournalEventCategoryEnum EventRuleTrigger => JournalEventCategoryEnum.SystemAction;
		public override string[] EventRuleTags => new[] { JournalEventTags.Visits };
		public override string EventRuleName => "SystemActionVisitStateChange";
		public override JournalEventTypeEnum AuditLogId => JournalEventTypeEnum.SystemActionVisitStateChange;
		public override string Description => "Generated when the state of a Visit Changes";
		public override bool TriggeredByEventUserId => true;
		public override bool AppliedToHsGId => true;
		public override bool AppliedToVpGId => true;
		public override bool AppliedToVpUserIdClientUserId => true;
		public override bool AppliedToFpId => false;
		public override bool AppliedToVisitId => true;
		public override bool AppliedToTransId => false;
		public override bool AppliedToPaymentId => false;
		public override string[] AdditionalInfo => new string[] { "PreviousState", "NewState" };
		public override bool Audit => false;
		public override string HumanReadableDescription => "The System changed the state of VP Visit [VpVisitId] from [PreviousState] to [NewState] for VP User [VpUserName]";
	}
}
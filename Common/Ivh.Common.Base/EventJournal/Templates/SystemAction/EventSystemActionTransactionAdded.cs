namespace Ivh.Common.EventJournal.Templates.SystemAction
{
	using System.Dynamic;
	using Base.Enums;
	using EventJournal;
	using Constants;
	using VisitPay.Enums;


    public class EventSystemActionTransactionAdded : JournalEventTemplate<JournalEventParameters>
	{
		public static JournalEventParameters GetParameters( string transactionAmount, string transactionId, string vpUserName, string vpVisitId)
		{
			//Property names are case sensitive and must match AdditionalInfo and HumanReadableDescription [tags] exactly
			dynamic parameters = new ExpandoObject();
			parameters.TransactionAmount = transactionAmount;
			parameters.TransactionId = transactionId;
			parameters.VpUserName = vpUserName;
			parameters.VpVisitId = vpVisitId;
			return new JournalEventParameters(parameters);
		}

		public override JournalEventCategoryEnum EventRuleTrigger => JournalEventCategoryEnum.SystemAction;
		public override string[] EventRuleTags => new[] { JournalEventTags.Transactions };
		public override string EventRuleName => "SystemActionTransactionAdded";
		public override JournalEventTypeEnum AuditLogId => JournalEventTypeEnum.SystemActionTransactionAdded;
		public override string Description => "Generated when a transaction is added to a Visit";
		public override bool TriggeredByEventUserId => true;
		public override bool AppliedToHsGId => true;
		public override bool AppliedToVpGId => true;
		public override bool AppliedToVpUserIdClientUserId => true;
		public override bool AppliedToFpId => false;
		public override bool AppliedToVisitId => true;
		public override bool AppliedToTransId => true;
		public override bool AppliedToPaymentId => true;
		public override string[] AdditionalInfo => new string[] {};
		public override bool Audit => false;
		public override string HumanReadableDescription => "The System applied Transaction [TransactionId] for [TransactionAmount] to VP Visit [VpVisitId] for VP User [VpUserName]";
	}
}
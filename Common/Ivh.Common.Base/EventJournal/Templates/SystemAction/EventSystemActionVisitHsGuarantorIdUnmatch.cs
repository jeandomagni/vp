namespace Ivh.Common.EventJournal.Templates.SystemAction
{
	using System.Dynamic;
	using Base.Enums;
	using EventJournal;
	using Constants;
	using VisitPay.Enums;


    public class EventSystemActionVisitHsGuarantorIdUnmatch : JournalEventTemplate<JournalEventParameters>
	{
		public static JournalEventParameters GetParameters( string hsGuarantorId, string visitSourceSystemKey, string vpVisitId)
		{
			//Property names are case sensitive and must match AdditionalInfo and HumanReadableDescription [tags] exactly
			dynamic parameters = new ExpandoObject();
			parameters.HsGuarantorId = hsGuarantorId;
			parameters.VisitSourceSystemKey = visitSourceSystemKey;
			parameters.VpVisitId = vpVisitId;
			return new JournalEventParameters(parameters);
		}

		public override JournalEventCategoryEnum EventRuleTrigger => JournalEventCategoryEnum.SystemAction;
		public override string[] EventRuleTags => new[] { JournalEventTags.MatchUnmatch };
		public override string EventRuleName => "SystemActionVisitHSGIDUnmatch";
		public override JournalEventTypeEnum AuditLogId => JournalEventTypeEnum.SystemActionVisitHsGIdUnmatch;
		public override string Description => "Generated when a current VPVisit is unmatched from the HSGID it was previously matched too. This happens when a visit is unmatched to the current HSGID, either by being matched to a new HSGID or unmatched if the HSGID and VpGuarantorId are unmatched. VPNG-4539, VPNG-770";
		public override bool TriggeredByEventUserId => true;
		public override bool AppliedToHsGId => true;
		public override bool AppliedToVpGId => true;
		public override bool AppliedToVpUserIdClientUserId => true;
		public override bool AppliedToFpId => false;
		public override bool AppliedToVisitId => true;
		public override bool AppliedToTransId => false;
		public override bool AppliedToPaymentId => false;
		public override string[] AdditionalInfo => new string[] { "VisitSourceSystemKey" };
		public override bool Audit => false;
		public override string HumanReadableDescription => "The System unmatched Visit [VpVisitId] from HSGID [HsGuarantorId]";
	}
}
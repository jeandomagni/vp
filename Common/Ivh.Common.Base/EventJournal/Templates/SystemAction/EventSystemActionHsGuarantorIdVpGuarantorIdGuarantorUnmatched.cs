namespace Ivh.Common.EventJournal.Templates.SystemAction
{
	using System.Dynamic;
	using Base.Enums;
	using EventJournal;
	using Constants;
	using VisitPay.Enums;


    public class EventSystemActionHsGuarantorIdVpGuarantorIdGuarantorUnmatched : JournalEventTemplate<JournalEventParameters>
	{
		public static JournalEventParameters GetParameters( string hsGuarantorId, string vpGuarantorId)
		{
			//Property names are case sensitive and must match AdditionalInfo and HumanReadableDescription [tags] exactly
			dynamic parameters = new ExpandoObject();
			parameters.HsGuarantorId = hsGuarantorId;
			parameters.VpGuarantorId = vpGuarantorId;
			return new JournalEventParameters(parameters);
		}

		public override JournalEventCategoryEnum EventRuleTrigger => JournalEventCategoryEnum.SystemAction;
		public override string[] EventRuleTags => new[] { JournalEventTags.MatchUnmatch };
		public override string EventRuleName => "SystemActionHsGuarantorIdVpGuarantorIdGuarantorUnmatched";
		public override JournalEventTypeEnum AuditLogId => JournalEventTypeEnum.SystemActionHsGuarantorIdVpGuarantorIdGuarantorUnmatched;
		public override string Description => "Generated when a HSGID is unmatched from a VpGuarantorId VPNG-4340";
		public override bool TriggeredByEventUserId => true;
		public override bool AppliedToHsGId => true;
		public override bool AppliedToVpGId => true;
		public override bool AppliedToVpUserIdClientUserId => true;
		public override bool AppliedToFpId => false;
		public override bool AppliedToVisitId => false;
		public override bool AppliedToTransId => false;
		public override bool AppliedToPaymentId => false;
		public override string[] AdditionalInfo => new string[] {};
		public override bool Audit => false;
		public override string HumanReadableDescription => "The System unmatched VpGuarantorId [VpGuarantorId] from HSGID [HsGuarantorId]";
	}
}
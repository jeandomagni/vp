namespace Ivh.Common.EventJournal.Templates.SystemAction
{
	using System.Dynamic;
	using Base.Enums;
	using EventJournal;
	using Constants;
	using VisitPay.Enums;


    public class EventSystemActionGuarantorAcctLockedDueToFailedLogins : JournalEventTemplate<JournalEventParameters>
	{
		public static JournalEventParameters GetParameters( string vpUserName)
		{
			//Property names are case sensitive and must match AdditionalInfo and HumanReadableDescription [tags] exactly
			dynamic parameters = new ExpandoObject();
			parameters.VpUserName = vpUserName;
			return new JournalEventParameters(parameters);
		}

		public override JournalEventCategoryEnum EventRuleTrigger => JournalEventCategoryEnum.SystemAction;
		public override string[] EventRuleTags => new[] { JournalEventTags.AppAccess };
		public override string EventRuleName => "SystemActionGuarantorAcctLockedDueToFailedLogins";
		public override JournalEventTypeEnum AuditLogId => JournalEventTypeEnum.SystemActionGuarantorAcctLockedDueToFailedLogins;
		public override string Description => "Generated when a guarantor acct is locked when it exceeds the max number of allowed login attempts";
		public override bool TriggeredByEventUserId => true;
		public override bool AppliedToHsGId => true;
		public override bool AppliedToVpGId => true;
		public override bool AppliedToVpUserIdClientUserId => true;
		public override bool AppliedToFpId => false;
		public override bool AppliedToVisitId => false;
		public override bool AppliedToTransId => false;
		public override bool AppliedToPaymentId => false;
		public override string[] AdditionalInfo => new string[] {};
		public override bool Audit => false;
		public override string HumanReadableDescription => "The System locked [VpUserName]'s account - exceeded the maximum number of failed login attempts";
	}
}
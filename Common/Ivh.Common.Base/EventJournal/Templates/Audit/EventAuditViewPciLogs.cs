namespace Ivh.Common.EventJournal.Templates.Audit
{
	using System.Dynamic;
	using Base.Enums;
	using EventJournal;
	using Constants;
	using VisitPay.Enums;

    public class EventAuditViewPciLogs : JournalEventTemplate<JournalEventParameters>
	{
		public static JournalEventParameters GetParameters( string eventUserName)
		{
			//Property names are case sensitive and must match AdditionalInfo and HumanReadableDescription [tags] exactly
			dynamic parameters = new ExpandoObject();
			parameters.EventUserName = eventUserName;
			return new JournalEventParameters(parameters);
		}

		public override JournalEventCategoryEnum EventRuleTrigger => JournalEventCategoryEnum.AuditAction;
		public override string[] EventRuleTags => new[] { JournalEventTags.AuditLogs };
		public override string EventRuleName => "AuditViewPciLogs";
		public override JournalEventTypeEnum AuditLogId => JournalEventTypeEnum.AuditViewPciLogs;
		public override string Description => "Generated when a IvhPciAuditor or ClientPciAuditor views the PCI Logs VPNG-12481";
		public override bool TriggeredByEventUserId => true;
		public override bool AppliedToHsGId => false;
		public override bool AppliedToVpGId => false;
		public override bool AppliedToVpUserIdClientUserId => false;
		public override bool AppliedToFpId => false;
		public override bool AppliedToVisitId => false;
		public override bool AppliedToTransId => false;
		public override bool AppliedToPaymentId => false;
		public override string[] AdditionalInfo => new string[] {};
		public override bool Audit => false;
		public override string HumanReadableDescription => "Client User [EventUserName] with a PciAudit role viewed the PCI Audit Logs";
	}
}
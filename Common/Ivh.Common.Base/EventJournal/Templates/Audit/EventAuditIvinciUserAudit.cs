namespace Ivh.Common.EventJournal.Templates.Audit
{
	using System.Dynamic;
	using Base.Enums;
	using EventJournal;
	using Constants;
	using VisitPay.Enums;

    public class EventAuditIvinciUserAudit : JournalEventTemplate<JournalEventParameters>
	{
		public static JournalEventParameters GetParameters( string eventUserName, string notes)
		{
			//Property names are case sensitive and must match AdditionalInfo and HumanReadableDescription [tags] exactly
			dynamic parameters = new ExpandoObject();
			parameters.EventUserName = eventUserName;
			parameters.Notes = notes;
			return new JournalEventParameters(parameters);
		}

		public override JournalEventCategoryEnum EventRuleTrigger => JournalEventCategoryEnum.AuditAction;
		public override string[] EventRuleTags => new[] { JournalEventTags.AuditLogs };
		public override string EventRuleName => "AuditIvinciUserAudit";
		public override JournalEventTypeEnum AuditLogId => JournalEventTypeEnum.AuditIvinciUserAudit;
		public override string Description => "Generated when a IvhUserAuditor Certifies the Audit of the Client User List VPNG-12486";
		public override bool TriggeredByEventUserId => true;
		public override bool AppliedToHsGId => false;
		public override bool AppliedToVpGId => false;
		public override bool AppliedToVpUserIdClientUserId => false;
		public override bool AppliedToFpId => false;
		public override bool AppliedToVisitId => false;
		public override bool AppliedToTransId => false;
		public override bool AppliedToPaymentId => false;
		public override string[] AdditionalInfo => new string[] { "Notes" };
		public override bool Audit => false;
		public override string HumanReadableDescription => "Client User [EventUserName] with the IvhUserAuditor role certified an audit of the iVinci User List";
	}
}
namespace Ivh.Common.EventJournal.Templates.Audit
{
	using System.Dynamic;
	using Base.Enums;
	using EventJournal;
	using Constants;
	using VisitPay.Enums;

    public class EventAuditIvinciAuditPciLogs : JournalEventTemplate<JournalEventParameters>
	{
		public static JournalEventParameters GetParameters( string beginDate, string endDate, string eventUserName, string notes)
		{
			//Property names are case sensitive and must match AdditionalInfo and HumanReadableDescription [tags] exactly
			dynamic parameters = new ExpandoObject();
			parameters.BeginDate = beginDate;
			parameters.EndDate = endDate;
			parameters.EventUserName = eventUserName;
			parameters.Notes = notes;
			return new JournalEventParameters(parameters);
		}

		public override JournalEventCategoryEnum EventRuleTrigger => JournalEventCategoryEnum.AuditAction;
		public override string[] EventRuleTags => new[] { JournalEventTags.AuditLogs };
		public override string EventRuleName => "AuditIvinciAuditPciLogs";
		public override JournalEventTypeEnum AuditLogId => JournalEventTypeEnum.AuditIvinciAuditPciLogs;
		public override string Description => "Generated when a IvhPciAuditor Confirms Completion of Audit on the iVinci PCI Logs";
		public override bool TriggeredByEventUserId => true;
		public override bool AppliedToHsGId => false;
		public override bool AppliedToVpGId => false;
		public override bool AppliedToVpUserIdClientUserId => false;
		public override bool AppliedToFpId => false;
		public override bool AppliedToVisitId => false;
		public override bool AppliedToTransId => false;
		public override bool AppliedToPaymentId => false;
		public override string[] AdditionalInfo => new string[] { "BeginDate", "EndDate", "Notes" };
		public override bool Audit => false;
		public override string HumanReadableDescription => "Client User [EventUserName] with the IvhPciAuditor role Confirmed an audit of the PCI Audit Logs for iVinci Users";
	}
}
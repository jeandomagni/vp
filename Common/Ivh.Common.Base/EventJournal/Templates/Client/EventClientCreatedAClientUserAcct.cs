namespace Ivh.Common.EventJournal.Templates.Client
{
	using System.Dynamic;
	using Base.Enums;
	using EventJournal;
	using Constants;
	using VisitPay.Enums;


    public class EventClientCreatedAClientUserAcct : JournalEventTemplate<JournalEventParameters>
	{
		public static JournalEventParameters GetParameters( string assignedRoles, string clientUserName, string eventUserName)
		{
			//Property names are case sensitive and must match AdditionalInfo and HumanReadableDescription [tags] exactly
			dynamic parameters = new ExpandoObject();
			parameters.AssignedRoles = assignedRoles;
			parameters.ClientUserName = clientUserName;
			parameters.EventUserName = eventUserName;
			return new JournalEventParameters(parameters);
		}

		public override JournalEventCategoryEnum EventRuleTrigger => JournalEventCategoryEnum.ClientUserAction;
		public override string[] EventRuleTags => new[] { JournalEventTags.AppAccess };
		public override string EventRuleName => "ClientCreatedAClientUserAcct";
		public override JournalEventTypeEnum AuditLogId => JournalEventTypeEnum.ClientCreatedAClientUserAcct;
		public override string Description => "Generated when a Client User Mngr Creates a Client User account VPNG-12497";
		public override bool TriggeredByEventUserId => true;
		public override bool AppliedToHsGId => false;
		public override bool AppliedToVpGId => false;
		public override bool AppliedToVpUserIdClientUserId => true;
		public override bool AppliedToFpId => false;
		public override bool AppliedToVisitId => false;
		public override bool AppliedToTransId => false;
		public override bool AppliedToPaymentId => false;
		public override string[] AdditionalInfo => new string[] { "AssignedRoles" };
		public override bool Audit => false;
		public override string HumanReadableDescription => "Client User [EventUserName] setup a Client User account for [ClientUserName]";
	}
}
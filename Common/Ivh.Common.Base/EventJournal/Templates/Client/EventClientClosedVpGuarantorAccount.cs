namespace Ivh.Common.EventJournal.Templates.Client
{
	using System.Dynamic;
	using Base.Enums;
	using EventJournal;
	using Constants;
	using VisitPay.Enums;


    public class EventClientClosedVpGuarantorAccount : JournalEventTemplate<JournalEventParameters>
	{
		public static JournalEventParameters GetParameters( string cancelReason, string eventUserName, string vpUserName)
		{
			//Property names are case sensitive and must match AdditionalInfo and HumanReadableDescription [tags] exactly
			dynamic parameters = new ExpandoObject();
			parameters.CancelReason = cancelReason;
			parameters.EventUserName = eventUserName;
			parameters.VpUserName = vpUserName;
			return new JournalEventParameters(parameters);
		}

		public override JournalEventCategoryEnum EventRuleTrigger => JournalEventCategoryEnum.ClientUserAction;
		public override string[] EventRuleTags => new[] { JournalEventTags.AcctMngmnt };
		public override string EventRuleName => "ClientClosedVpGuarantorAccount";
		public override JournalEventTypeEnum AuditLogId => JournalEventTypeEnum.ClientClosedVpGuarantorAccount;
		public override string Description => "Generated when a Client User closes a VPG account";
		public override bool TriggeredByEventUserId => true;
		public override bool AppliedToHsGId => true;
		public override bool AppliedToVpGId => true;
		public override bool AppliedToVpUserIdClientUserId => true;
		public override bool AppliedToFpId => false;
		public override bool AppliedToVisitId => false;
		public override bool AppliedToTransId => false;
		public override bool AppliedToPaymentId => false;
		public override string[] AdditionalInfo => new string[] { "CancelReason" };
		public override bool Audit => false;
		public override string HumanReadableDescription => "Client User [EventUserName] closed VP User [VpUserName]'s account.";
	}
}
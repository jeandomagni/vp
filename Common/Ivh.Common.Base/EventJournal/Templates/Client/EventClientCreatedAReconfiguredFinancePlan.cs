namespace Ivh.Common.EventJournal.Templates.Client
{
	using System.Dynamic;
	using Base.Enums;
	using EventJournal;
	using Constants;
	using VisitPay.Enums;


    public class EventClientCreatedAReconfiguredFinancePlan : JournalEventTemplate<JournalEventParameters>
	{
		public static JournalEventParameters GetParameters( string eventUserName, string newTerms, string vpUserName)
		{
			//Property names are case sensitive and must match AdditionalInfo and HumanReadableDescription [tags] exactly
			dynamic parameters = new ExpandoObject();
			parameters.EventUserName = eventUserName;
			parameters.NewTerms = newTerms;
			parameters.VpUserName = vpUserName;
			return new JournalEventParameters(parameters);
		}

		public override JournalEventCategoryEnum EventRuleTrigger => JournalEventCategoryEnum.ClientUserAction;
		public override string[] EventRuleTags => new[] { JournalEventTags.FinancePlans };
		public override string EventRuleName => "ClientCreatedAReconfiguredFinancePlan";
		public override JournalEventTypeEnum AuditLogId => JournalEventTypeEnum.ClientCreatedAReconfiguredFinancePlan;
		public override string Description => "Generated when a Client User creates a reconfigured FP";
		public override bool TriggeredByEventUserId => true;
		public override bool AppliedToHsGId => true;
		public override bool AppliedToVpGId => true;
		public override bool AppliedToVpUserIdClientUserId => true;
		public override bool AppliedToFpId => true;
		public override bool AppliedToVisitId => false;
		public override bool AppliedToTransId => false;
		public override bool AppliedToPaymentId => false;
		public override string[] AdditionalInfo => new string[] { "NewTerms" };
		public override bool Audit => false;
		public override string HumanReadableDescription => "Client User [EventUserName] reconfigured Finance Plan for [VpUserName]";
	}
}
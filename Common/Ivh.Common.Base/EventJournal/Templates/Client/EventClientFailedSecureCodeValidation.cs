namespace Ivh.Common.EventJournal.Templates.Client
{
	using System.Dynamic;
	using Base.Enums;
	using EventJournal;
	using Constants;
	using VisitPay.Enums;


    public class EventClientFailedSecureCodeValidation : JournalEventTemplate<JournalEventParameters>
	{
		public static JournalEventParameters GetParameters( string eventUserName)
		{
			//Property names are case sensitive and must match AdditionalInfo and HumanReadableDescription [tags] exactly
			dynamic parameters = new ExpandoObject();
			parameters.EventUserName = eventUserName;
			return new JournalEventParameters(parameters);
		}

		public override JournalEventCategoryEnum EventRuleTrigger => JournalEventCategoryEnum.ClientUserAction;
		public override string[] EventRuleTags => new[] { JournalEventTags.AppAccess };
		public override string EventRuleName => "ClientFailedSecureCodeValidation";
		public override JournalEventTypeEnum AuditLogId => JournalEventTypeEnum.ClientFailedSecureCodeValidation;
		public override string Description => "Generated when the Client User enters an invalid Secure Code when logging in the first time or after an account reset";
		public override bool TriggeredByEventUserId => true;
		public override bool AppliedToHsGId => false;
		public override bool AppliedToVpGId => false;
		public override bool AppliedToVpUserIdClientUserId => true;
		public override bool AppliedToFpId => false;
		public override bool AppliedToVisitId => false;
		public override bool AppliedToTransId => false;
		public override bool AppliedToPaymentId => false;
		public override string[] AdditionalInfo => new string[] {};
		public override bool Audit => false;
		public override string HumanReadableDescription => "Client User [EventUserName] entered an invalid Secure Code while resetting their password";
	}
}
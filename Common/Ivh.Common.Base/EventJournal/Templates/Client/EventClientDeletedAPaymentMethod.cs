namespace Ivh.Common.EventJournal.Templates.Client
{
	using System.Dynamic;
	using Base.Enums;
	using EventJournal;
	using Constants;
	using VisitPay.Enums;


    public class EventClientDeletedAPaymentMethod : JournalEventTemplate<JournalEventParameters>
	{
		public static JournalEventParameters GetParameters( string accountNumber, string eventUserName, string vpUserName)
		{
			//Property names are case sensitive and must match AdditionalInfo and HumanReadableDescription [tags] exactly
			dynamic parameters = new ExpandoObject();
			parameters.AccountNumber = accountNumber;
			parameters.EventUserName = eventUserName;
			parameters.VpUserName = vpUserName;
			return new JournalEventParameters(parameters);
		}

		public override JournalEventCategoryEnum EventRuleTrigger => JournalEventCategoryEnum.ClientUserAction;
		public override string[] EventRuleTags => new[] { JournalEventTags.PaymentMethods };
		public override string EventRuleName => "ClientDeletedAPaymentMethod";
		public override JournalEventTypeEnum AuditLogId => JournalEventTypeEnum.ClientDeletedAPaymentMethod;
		public override string Description => "Generated when a payment method is deleted by a Client User";
		public override bool TriggeredByEventUserId => true;
		public override bool AppliedToHsGId => true;
		public override bool AppliedToVpGId => true;
		public override bool AppliedToVpUserIdClientUserId => true;
		public override bool AppliedToFpId => false;
		public override bool AppliedToVisitId => false;
		public override bool AppliedToTransId => false;
		public override bool AppliedToPaymentId => false;
		public override string[] AdditionalInfo => new string[] { "AccountNumber" };
		public override bool Audit => false;
		public override string HumanReadableDescription => "Client User [EventUserName] deleted payment method [AccountNumber] for [VpUserName]";
	}
}
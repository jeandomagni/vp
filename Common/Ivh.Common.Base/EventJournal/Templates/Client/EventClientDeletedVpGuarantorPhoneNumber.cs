namespace Ivh.Common.EventJournal.Templates.Client
{
	using System.Dynamic;
	using Base.Enums;
	using EventJournal;
	using Constants;
	using VisitPay.Enums;


    public class EventClientDeletedVpGuarantorPhoneNumber : JournalEventTemplate<JournalEventParameters>
	{
		public static JournalEventParameters GetParameters( string deletedPhoneNumber, string eventUserName, string phoneType, string vpUserName)
		{
			//Property names are case sensitive and must match AdditionalInfo and HumanReadableDescription [tags] exactly
			dynamic parameters = new ExpandoObject();
			parameters.DeletedPhoneNumber = deletedPhoneNumber;
			parameters.EventUserName = eventUserName;
			parameters.PhoneType = phoneType;
			parameters.VpUserName = vpUserName;
			return new JournalEventParameters(parameters);
		}

		public override JournalEventCategoryEnum EventRuleTrigger => JournalEventCategoryEnum.ClientUserAction;
		public override string[] EventRuleTags => new[] { JournalEventTags.AcctMngmnt };
		public override string EventRuleName => "ClientDeletedVpGuarantorPhoneNumber";
		public override JournalEventTypeEnum AuditLogId => JournalEventTypeEnum.ClientDeletedVpGuarantorPhoneNumber;
		public override string Description => "Generated when a Client User deletes a VPG phone number";
		public override bool TriggeredByEventUserId => true;
		public override bool AppliedToHsGId => true;
		public override bool AppliedToVpGId => true;
		public override bool AppliedToVpUserIdClientUserId => true;
		public override bool AppliedToFpId => false;
		public override bool AppliedToVisitId => false;
		public override bool AppliedToTransId => false;
		public override bool AppliedToPaymentId => false;
		public override string[] AdditionalInfo => new string[] { "PhoneType", "DeletedPhoneNumber" };
		public override bool Audit => false;
		public override string HumanReadableDescription => "Client User [EventUserName] deleted VP User [VpUserName]'s [PhoneType] phone [DeletedPhoneNumber] ";
	}
}
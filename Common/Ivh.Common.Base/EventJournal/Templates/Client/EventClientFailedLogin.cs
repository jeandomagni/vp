namespace Ivh.Common.EventJournal.Templates.Client
{
	using System.Dynamic;
	using Base.Enums;
	using EventJournal;
	using Constants;
	using VisitPay.Enums;


    public class EventClientFailedLogin : JournalEventTemplate<JournalEventParameters>
	{
		public static JournalEventParameters GetParameters( string accountDisabled, string accountLocked, string loginName, string passwordHashed)
		{
			//Property names are case sensitive and must match AdditionalInfo and HumanReadableDescription [tags] exactly
			dynamic parameters = new ExpandoObject();
			parameters.AccountDisabled = accountDisabled;
			parameters.AccountLocked = accountLocked;
			parameters.LoginName = loginName;
			parameters.PasswordHashed = passwordHashed;
			return new JournalEventParameters(parameters);
		}

		public override JournalEventCategoryEnum EventRuleTrigger => JournalEventCategoryEnum.ClientUserAction;
		public override string[] EventRuleTags => new[] { JournalEventTags.AppAccess };
		public override string EventRuleName => "ClientFailedLogin";
		public override JournalEventTypeEnum AuditLogId => JournalEventTypeEnum.ClientFailedLogin;
		public override string Description => "Generated when a user attempts to login and fails Added LoginName to Additional Data field in VPNG-18649";
		public override bool TriggeredByEventUserId => true;
		public override bool AppliedToHsGId => false;
		public override bool AppliedToVpGId => false;
		public override bool AppliedToVpUserIdClientUserId => false;
		public override bool AppliedToFpId => false;
		public override bool AppliedToVisitId => false;
		public override bool AppliedToTransId => false;
		public override bool AppliedToPaymentId => false;
		public override string[] AdditionalInfo => new string[] { "LoginName", "PasswordHashed", "AccountLocked", "AccountDisabled" };
		public override bool Audit => false;
		public override string HumanReadableDescription => "User [LoginName] failed trying to login";
	}
}
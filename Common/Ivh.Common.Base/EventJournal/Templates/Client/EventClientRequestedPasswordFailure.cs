namespace Ivh.Common.EventJournal.Templates.Client
{
	using System.Dynamic;
	using Base.Enums;
	using EventJournal;
	using Constants;
	using VisitPay.Enums;


    public class EventClientRequestedPasswordFailure : JournalEventTemplate<JournalEventParameters>
	{
		public static JournalEventParameters GetParameters( string invalidUserName)
		{
			//Property names are case sensitive and must match AdditionalInfo and HumanReadableDescription [tags] exactly
			dynamic parameters = new ExpandoObject();
			parameters.InvalidUserName = invalidUserName;
			return new JournalEventParameters(parameters);
		}

		public override JournalEventCategoryEnum EventRuleTrigger => JournalEventCategoryEnum.ClientUserAction;
		public override string[] EventRuleTags => new[] { JournalEventTags.AppAccess };
		public override string EventRuleName => "ClientRequestedPasswordFailure";
		public override JournalEventTypeEnum AuditLogId => JournalEventTypeEnum.ClientRequestedPasswordFailure;
		public override string Description => "Generated when a Client User requests a password reset using an invalid UserName";
		public override bool TriggeredByEventUserId => false;
		public override bool AppliedToHsGId => false;
		public override bool AppliedToVpGId => false;
		public override bool AppliedToVpUserIdClientUserId => false;
		public override bool AppliedToFpId => false;
		public override bool AppliedToVisitId => false;
		public override bool AppliedToTransId => false;
		public override bool AppliedToPaymentId => false;
		public override string[] AdditionalInfo => new string[] { "InvalidUserName" };
		public override bool Audit => false;
		public override string HumanReadableDescription => "A user requested a password reset to the Client Portal using an invalid User Name of [InvalidUserName]";
	}
}
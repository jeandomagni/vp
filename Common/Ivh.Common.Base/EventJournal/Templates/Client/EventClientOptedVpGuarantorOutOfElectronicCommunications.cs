﻿namespace Ivh.Common.EventJournal.Templates.Client
{
    using System.Dynamic;
    using Base.Enums;
    using EventJournal;
    using Constants;
    using VisitPay.Enums;

    public class EventClientOptedVpGuarantorOutOfElectronicCommunications: JournalEventTemplate<JournalEventParameters>
    {
        public static JournalEventParameters GetParameters( string eventUserName, string vpGuarantorId)
        {
            //Property names are case sensitive and must match AdditionalInfo and HumanReadableDescription [tags] exactly
            dynamic parameters = new ExpandoObject();
            parameters.EventUserName = eventUserName;
            parameters.VpGuarantorId = vpGuarantorId;
            return new JournalEventParameters(parameters);
        }

        public override JournalEventCategoryEnum EventRuleTrigger => JournalEventCategoryEnum.ClientUserAction;
        public override string[] EventRuleTags => new[] { JournalEventTags.AcctMngmnt };
        public override string EventRuleName => "ClientOptedVpGuarantorOutOfElectronicCommunications";
        public override JournalEventTypeEnum AuditLogId => JournalEventTypeEnum.ClientOptedVpGuarantorOutOfElectronicCommunication;
        public override string Description => "Generated when a client user opts a vpguarantor out of electronic communications";
        public override bool TriggeredByEventUserId => true;
        public override bool AppliedToHsGId => true;
        public override bool AppliedToVpGId => true;
        public override bool AppliedToVpUserIdClientUserId => true;
        public override bool AppliedToFpId => false;
        public override bool AppliedToVisitId => false;
        public override bool AppliedToTransId => false;
        public override bool AppliedToPaymentId => false;
        public override string[] AdditionalInfo => new string[] {};
        public override bool Audit => false;
        public override string HumanReadableDescription => "Client User [EventUserName] opted out of Electronic Communications for [VpGuarantorId]";
    }
}

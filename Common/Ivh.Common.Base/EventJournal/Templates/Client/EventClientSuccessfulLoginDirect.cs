namespace Ivh.Common.EventJournal.Templates.Client
{
	using System.Dynamic;
	using Base.Enums;
	using EventJournal;
	using Constants;
	using VisitPay.Enums;


    public class EventClientSuccessfulLoginDirect : JournalEventTemplate<JournalEventParameters>
	{
		public static JournalEventParameters GetParameters( string applicationID, string deviceType, string eventUserName, string uRL)
		{
			//Property names are case sensitive and must match AdditionalInfo and HumanReadableDescription [tags] exactly
			dynamic parameters = new ExpandoObject();
			parameters.ApplicationID = applicationID;
			parameters.DeviceType = deviceType;
			parameters.EventUserName = eventUserName;
			parameters.URL = uRL;
			return new JournalEventParameters(parameters);
		}

		public override JournalEventCategoryEnum EventRuleTrigger => JournalEventCategoryEnum.ClientUserAction;
		public override string[] EventRuleTags => new[] { JournalEventTags.AppAccess };
		public override string EventRuleName => "ClientSuccessfulLoginDirect";
		public override JournalEventTypeEnum AuditLogId => JournalEventTypeEnum.ClientSuccessfulLoginDirect;
		public override string Description => "Generated when a user logs into VP (Client portal)";
		public override bool TriggeredByEventUserId => true;
		public override bool AppliedToHsGId => false;
		public override bool AppliedToVpGId => false;
		public override bool AppliedToVpUserIdClientUserId => true;
		public override bool AppliedToFpId => false;
		public override bool AppliedToVisitId => false;
		public override bool AppliedToTransId => false;
		public override bool AppliedToPaymentId => false;
		public override string[] AdditionalInfo => new string[] { "ApplicationID", "DeviceType", "URL" };
		public override bool Audit => false;
		public override string HumanReadableDescription => "Client User [EventUserName] logged into the Client Portal";
	}
}
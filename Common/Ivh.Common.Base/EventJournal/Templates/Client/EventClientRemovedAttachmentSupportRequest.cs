﻿namespace Ivh.Common.EventJournal.Templates.Client
{
    using System.Dynamic;
    using Base.Enums;
    using EventJournal;
    using Constants;
    using VisitPay.Enums;

    public class EventClientRemovedAttachmentSupportRequest : JournalEventTemplate<JournalEventParameters>
    {
        public static JournalEventParameters GetParameters( string eventUserName, string supportRequestId, string vpUserName)
        {
            //Property names are case sensitive and must match AdditionalInfo and HumanReadableDescription [tags] exactly
            dynamic parameters = new ExpandoObject();
            parameters.EventUserName = eventUserName;
            parameters.SupportRequestId = supportRequestId;
            parameters.VpUserName = vpUserName;
            return new JournalEventParameters(parameters);
        }

        public override JournalEventCategoryEnum EventRuleTrigger => JournalEventCategoryEnum.ClientUserAction;
        public override string[] EventRuleTags => new[] { JournalEventTags.EventLog };
        public override string EventRuleName => "ClientRemovedAttachmentSupportRequest";
        public override JournalEventTypeEnum AuditLogId => JournalEventTypeEnum.ClientRemovedAttachmentSupportRequest;
        public override string Description => "Generated when a client user removes an attachment from a support request";
        public override bool TriggeredByEventUserId => true;
        public override bool AppliedToHsGId => true;
        public override bool AppliedToVpGId => true;
        public override bool AppliedToVpUserIdClientUserId => true;
        public override bool AppliedToFpId => false;
        public override bool AppliedToVisitId => false;
        public override bool AppliedToTransId => false;
        public override bool AppliedToPaymentId => false;
        public override string[] AdditionalInfo => new string[] { "SupportRequestId" };
        public override bool Audit => false;
        public override string HumanReadableDescription => "Client User [EventUserName] removed an attachment from VP User [VpUserName]'s support request [SupportRequestId]";
    }
}

﻿
namespace Ivh.Common.EventJournal.Templates.Client
{
    using System.Dynamic;
    using EventJournal;
    using Constants;
    using VisitPay.Enums;

    public class EventClientUserConnectedToChatThread : JournalEventTemplate<JournalEventParameters>
    {
        public static JournalEventParameters GetParameters(string eventUserName, string chatThreadId, string vpUserName)
        {
            //Property names are case sensitive and must match AdditionalInfo and HumanReadableDescription [tags] exactly
            dynamic parameters = new ExpandoObject();
            parameters.EventUserName = eventUserName;
            parameters.ChatThreadId = chatThreadId;
            parameters.VpUserName = vpUserName;
            return new JournalEventParameters(parameters);
        }

        public override JournalEventCategoryEnum EventRuleTrigger => JournalEventCategoryEnum.ClientUserAction;
        public override string[] EventRuleTags => new[] { JournalEventTags.Chat };
        public override string EventRuleName => "ClientUserConnectedToChatThread";
        public override JournalEventTypeEnum AuditLogId => JournalEventTypeEnum.ClientUserConnectedToChatThread;
        public override string Description => "Generated when a Client User connects to a chat thread from a Guarantor";
        public override bool TriggeredByEventUserId => true;
        public override bool AppliedToHsGId => true;
        public override bool AppliedToVpGId => true;
        public override bool AppliedToVpUserIdClientUserId => true;
        public override bool AppliedToFpId => false;
        public override bool AppliedToVisitId => false;
        public override bool AppliedToTransId => false;
        public override bool AppliedToPaymentId => false;
        public override string[] AdditionalInfo => new string[] { "ChatThreadId" };
        public override bool Audit => false;
        public override string HumanReadableDescription => "Client User [EventUserName] connected to a chat thread with [VpUserName]";
    }
}

namespace Ivh.Common.EventJournal.Templates.Client
{
	using System.Dynamic;
	using Base.Enums;
	using EventJournal;
	using Constants;
	using VisitPay.Enums;


    public class EventClientModifiedAScheduledPaymentRescheduled : JournalEventTemplate<JournalEventParameters>
	{
		public static JournalEventParameters GetParameters( string changeReason, string eventUserName, string newScheduledDate, string paymentAmount, string paymentMethod, string previouslyScheduledDate, string scheduledAmount, string vpUserName)
		{
			//Property names are case sensitive and must match AdditionalInfo and HumanReadableDescription [tags] exactly
			dynamic parameters = new ExpandoObject();
			parameters.ChangeReason = changeReason;
			parameters.EventUserName = eventUserName;
			parameters.NewScheduledDate = newScheduledDate;
			parameters.PaymentAmount = paymentAmount;
			parameters.PaymentMethod = paymentMethod;
			parameters.PreviouslyScheduledDate = previouslyScheduledDate;
			parameters.ScheduledAmount = scheduledAmount;
			parameters.VpUserName = vpUserName;
			return new JournalEventParameters(parameters);
		}

		public override JournalEventCategoryEnum EventRuleTrigger => JournalEventCategoryEnum.ClientUserAction;
		public override string[] EventRuleTags => new[] { JournalEventTags.Payments };
		public override string EventRuleName => "ClientModifiedAScheduledPaymentRescheduled";
		public override JournalEventTypeEnum AuditLogId => JournalEventTypeEnum.ClientModifiedAScheduledPaymentRescheduled;
		public override string Description => "Generated when a Client User changes the scheduled date on a scheduled, manual payment";
		public override bool TriggeredByEventUserId => true;
		public override bool AppliedToHsGId => true;
		public override bool AppliedToVpGId => true;
		public override bool AppliedToVpUserIdClientUserId => true;
		public override bool AppliedToFpId => false;
		public override bool AppliedToVisitId => false;
		public override bool AppliedToTransId => false;
		public override bool AppliedToPaymentId => true;
		public override string[] AdditionalInfo => new string[] { "PreviouslyScheduledDate", "NewScheduledDate", "PaymentMethod", "ScheduledAmount", "ChangeReason" };
		public override bool Audit => false;
		public override string HumanReadableDescription => "Client User [EventUserName] changed the scheduled date on the payment of [PaymentAmount] for [VpUserName] from [PreviouslyScheduledDate] to [NewScheduledDate]";
	}
}
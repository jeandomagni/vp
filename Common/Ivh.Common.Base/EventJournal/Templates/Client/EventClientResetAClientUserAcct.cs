namespace Ivh.Common.EventJournal.Templates.Client
{
	using System.Dynamic;
	using Base.Enums;
	using EventJournal;
	using Constants;
	using VisitPay.Enums;


    public class EventClientResetAClientUserAcct : JournalEventTemplate<JournalEventParameters>
	{
		public static JournalEventParameters GetParameters( string clientUserName, string eventUserName)
		{
			//Property names are case sensitive and must match AdditionalInfo and HumanReadableDescription [tags] exactly
			dynamic parameters = new ExpandoObject();
			parameters.ClientUserName = clientUserName;
			parameters.EventUserName = eventUserName;
			return new JournalEventParameters(parameters);
		}

		public override JournalEventCategoryEnum EventRuleTrigger => JournalEventCategoryEnum.ClientUserAction;
		public override string[] EventRuleTags => new[] { JournalEventTags.AppAccess };
		public override string EventRuleName => "ClientResetAClientUserAcct";
		public override JournalEventTypeEnum AuditLogId => JournalEventTypeEnum.ClientResetAClientUserAcct;
		public override string Description => "Generated when a Client User Mngr Resets a Client User account VPNG-12497";
		public override bool TriggeredByEventUserId => true;
		public override bool AppliedToHsGId => false;
		public override bool AppliedToVpGId => false;
		public override bool AppliedToVpUserIdClientUserId => true;
		public override bool AppliedToFpId => false;
		public override bool AppliedToVisitId => false;
		public override bool AppliedToTransId => false;
		public override bool AppliedToPaymentId => false;
		public override string[] AdditionalInfo => new string[] {};
		public override bool Audit => false;
		public override string HumanReadableDescription => "Client User [EventUserName] reset [ClientUserName]'s account";
	}
}
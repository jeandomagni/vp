namespace Ivh.Common.EventJournal.Templates.Client
{
	using System.Dynamic;
	using Base.Enums;
	using EventJournal;
	using Constants;
	using VisitPay.Enums;


    public class EventClientViewEventLogDetail : JournalEventTemplate<JournalEventParameters>
	{
		public static JournalEventParameters GetParameters( string eventJournalId, string eventUserName)
		{
			//Property names are case sensitive and must match AdditionalInfo and HumanReadableDescription [tags] exactly
			dynamic parameters = new ExpandoObject();
			parameters.EventJournalId = eventJournalId;
			parameters.EventUserName = eventUserName;
			return new JournalEventParameters(parameters);
		}

		public override JournalEventCategoryEnum EventRuleTrigger => JournalEventCategoryEnum.ClientUserAction;
		public override string[] EventRuleTags => new[] { JournalEventTags.EventLog };
		public override string EventRuleName => "ClientViewEventLogDetail";
		public override JournalEventTypeEnum AuditLogId => JournalEventTypeEnum.ClientViewEventLogDetail;
		public override string Description => "Generated when a Client User views the Event Journal details";
		public override bool TriggeredByEventUserId => true;
		public override bool AppliedToHsGId => true;
		public override bool AppliedToVpGId => true;
		public override bool AppliedToVpUserIdClientUserId => true;
		public override bool AppliedToFpId => false;
		public override bool AppliedToVisitId => false;
		public override bool AppliedToTransId => false;
		public override bool AppliedToPaymentId => false;
		public override string[] AdditionalInfo => new string[] {};
		public override bool Audit => false;
		public override string HumanReadableDescription => "Client User [EventUserName] viewed the Event Journal details for [EventJournalId]";
	}
}
namespace Ivh.Common.EventJournal.Templates.Client
{
	using System.Dynamic;
	using Base.Enums;
	using EventJournal;
	using Constants;
	using VisitPay.Enums;


    public class EventClientSmsDeactivated : JournalEventTemplate<JournalEventParameters>
	{
		public static JournalEventParameters GetParameters( string eventUserName, string phoneNumber, string vpUserName)
		{
			//Property names are case sensitive and must match AdditionalInfo and HumanReadableDescription [tags] exactly
			dynamic parameters = new ExpandoObject();
			parameters.EventUserName = eventUserName;
			parameters.PhoneNumber = phoneNumber;
			parameters.VpUserName = vpUserName;
			return new JournalEventParameters(parameters);
		}

		public override JournalEventCategoryEnum EventRuleTrigger => JournalEventCategoryEnum.ClientUserAction;
		public override string[] EventRuleTags => new[] { JournalEventTags.Sms };
		public override string EventRuleName => "ClientSMSDeactivated";
		public override JournalEventTypeEnum AuditLogId => JournalEventTypeEnum.ClientSmsDeactivated;
		public override string Description => "Generated when a Client User deactivates a phone number for SMS. This can happen 1) when a different number if validated and 2) when the disable texting button is selected VPNG-15232";
		public override bool TriggeredByEventUserId => true;
		public override bool AppliedToHsGId => true;
		public override bool AppliedToVpGId => true;
		public override bool AppliedToVpUserIdClientUserId => true;
		public override bool AppliedToFpId => false;
		public override bool AppliedToVisitId => false;
		public override bool AppliedToTransId => false;
		public override bool AppliedToPaymentId => false;
		public override string[] AdditionalInfo => new string[] { "PhoneNumber" };
		public override bool Audit => false;
		public override string HumanReadableDescription => "Client User [EventUserName] deactivated phone number [PhoneNumber] as an SMS number for [VpUserName]";
	}
}
﻿namespace Ivh.Common.EventJournal.Templates.Client
{
    using System.Dynamic;
    using Base.Enums;
    using EventJournal;
    using Constants;
    using VisitPay.Enums;

    public class EventClientVisitPayReportViewed : JournalEventTemplate<JournalEventParameters>
    {
        public static JournalEventParameters GetParameters( string eventUserName, string reportName)
        {
            //Property names are case sensitive and must match AdditionalInfo and HumanReadableDescription [tags] exactly
            dynamic parameters = new ExpandoObject();
            parameters.EventUserName = eventUserName;
            parameters.ReportName = reportName;
            return new JournalEventParameters(parameters);
        }

        public override JournalEventCategoryEnum EventRuleTrigger => JournalEventCategoryEnum.ClientUserAction;
        public override string[] EventRuleTags => new[] { JournalEventTags.EventLog };
        public override string EventRuleName => "ClientVisitPayReportViewed";
        public override JournalEventTypeEnum AuditLogId => JournalEventTypeEnum.ClientVisitPayReportViewed;
        public override string Description => "Generated when the user views a Support Request";
        public override bool TriggeredByEventUserId => true;
        public override bool AppliedToHsGId => false;
        public override bool AppliedToVpGId => false;
        public override bool AppliedToVpUserIdClientUserId => true;
        public override bool AppliedToFpId => false;
        public override bool AppliedToVisitId => false;
        public override bool AppliedToTransId => false;
        public override bool AppliedToPaymentId => false;
        public override string[] AdditionalInfo => new string[] { "ReportName" };
        public override bool Audit => false;
        public override string HumanReadableDescription => "Client User [EventUserName] viewed the report [ReportName]";
    }
}

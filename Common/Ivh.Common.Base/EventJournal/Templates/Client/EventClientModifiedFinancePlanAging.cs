namespace Ivh.Common.EventJournal.Templates.Client
{
	using System.Dynamic;
	using Base.Enums;
	using EventJournal;
	using Constants;
	using VisitPay.Enums;


    public class EventClientModifiedFinancePlanAging : JournalEventTemplate<JournalEventParameters>
	{
		public static JournalEventParameters GetParameters( string eventUserName, string newAge, string previousAge, string vpUserName)
		{
			//Property names are case sensitive and must match AdditionalInfo and HumanReadableDescription [tags] exactly
			dynamic parameters = new ExpandoObject();
			parameters.EventUserName = eventUserName;
			parameters.NewAge = newAge;
			parameters.PreviousAge = previousAge;
			parameters.VpUserName = vpUserName;
			return new JournalEventParameters(parameters);
		}

		public override JournalEventCategoryEnum EventRuleTrigger => JournalEventCategoryEnum.ClientUserAction;
		public override string[] EventRuleTags => new[] { JournalEventTags.FinancePlans };
		public override string EventRuleName => "ClientModifiedFinancePlanAging";
		public override JournalEventTypeEnum AuditLogId => JournalEventTypeEnum.ClientModifiedFinancePlanAging;
		public override string Description => "Generated when a Client User modifies the aging of a Finance Plan";
		public override bool TriggeredByEventUserId => true;
		public override bool AppliedToHsGId => true;
		public override bool AppliedToVpGId => true;
		public override bool AppliedToVpUserIdClientUserId => true;
		public override bool AppliedToFpId => true;
		public override bool AppliedToVisitId => false;
		public override bool AppliedToTransId => false;
		public override bool AppliedToPaymentId => false;
		public override string[] AdditionalInfo => new string[] { "PreviousAge", "NewAge" };
		public override bool Audit => false;
		public override string HumanReadableDescription => "Client User [EventUserName] changed the Finance Plan aging from [PreviousAge] to [NewAge] for [VpUserName]";
	}
}
namespace Ivh.Common.EventJournal.Templates.Client
{
	using System.Dynamic;
	using Base.Enums;
	using EventJournal;
	using Constants;
	using VisitPay.Enums;


    public class EventClientRefundedPayment : JournalEventTemplate<JournalEventParameters>
	{
		public static JournalEventParameters GetParameters(string eventUserName, string vpUserId, string paymentId, string paymentType)
		{
            //Property names are case sensitive and must match AdditionalInfo and HumanReadableDescription [tags] exactly
            dynamic parameters = new ExpandoObject();
			parameters.EventUserName = eventUserName;
			parameters.VpUserId = vpUserId;
			parameters.PaymentId = paymentId;
			parameters.PaymentType = paymentType;
			return new JournalEventParameters(parameters);
		}

		public override JournalEventCategoryEnum EventRuleTrigger => JournalEventCategoryEnum.ClientUserAction;
		public override string[] EventRuleTags => new[] { JournalEventTags.PaymentMethods };
		public override string EventRuleName => "ClientRefundedPayment";
		public override JournalEventTypeEnum AuditLogId => JournalEventTypeEnum.ClientRefundedPayment;
		public override string Description => "Generated when a lockbox payment is returned by a Client User";
		public override bool TriggeredByEventUserId => true;
		public override bool AppliedToHsGId => true;
		public override bool AppliedToVpGId => true;
		public override bool AppliedToVpUserIdClientUserId => true;
		public override bool AppliedToFpId => false;
		public override bool AppliedToVisitId => false;
		public override bool AppliedToTransId => false;
		public override bool AppliedToPaymentId => false;
		public override string[] AdditionalInfo => new string[] {};
		public override bool Audit => false;
		public override string HumanReadableDescription => "Client User [EventUserName] refunded payment [PaymentId] of type [PaymentType] for [VpUserId]";
	}
}
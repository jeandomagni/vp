﻿namespace Ivh.Common.EventJournal.Templates.Client
{
    using System.Dynamic;
    using Base.Enums;
    using EventJournal;
    using Constants;
    using VisitPay.Enums;

    public class EventClientViewManagedThruManaging: JournalEventTemplate<JournalEventParameters>
    {
        public static JournalEventParameters GetParameters( string eventUserName, string managedVpGuarantorId, string managedVpUserName, string managingVpUserName)
        {
            //Property names are case sensitive and must match AdditionalInfo and HumanReadableDescription [tags] exactly
            dynamic parameters = new ExpandoObject();
            parameters.EventUserName = eventUserName;
            parameters.ManagedVpGuarantorId = managedVpGuarantorId;
            parameters.ManagedVpUserName = managedVpUserName;
            parameters.ManagingVpUserName = managingVpUserName;

            return new JournalEventParameters(parameters);
        }

        public override JournalEventCategoryEnum EventRuleTrigger => JournalEventCategoryEnum.ClientUserAction;
        public override string[] EventRuleTags => new[] { JournalEventTags.AppAccess };
        public override string EventRuleName => "ClientViewManagedThruManaging";
        public override JournalEventTypeEnum AuditLogId => JournalEventTypeEnum.ClientViewManagedThruManaging;
        public override string Description => "Generated when a client user views managed user account details by emulating a managing user";
        public override bool TriggeredByEventUserId => true;
        public override bool AppliedToHsGId => true;
        public override bool AppliedToVpGId => true;
        public override bool AppliedToVpUserIdClientUserId => true;
        public override bool AppliedToFpId => false;
        public override bool AppliedToVisitId => false;
        public override bool AppliedToTransId => false;
        public override bool AppliedToPaymentId => false;
        public override string[] AdditionalInfo => new string[] {"ManagedVpGuarantorId"};
        public override bool Audit => false;
        public override string HumanReadableDescription => "Client User [EventUserName] viewed managed user account [ManagedVpUserName] through managing user [ManagingVpUserName]";
    }
}

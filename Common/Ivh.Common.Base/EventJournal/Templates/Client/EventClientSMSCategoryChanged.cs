namespace Ivh.Common.EventJournal.Templates.Client
{
	using System.Dynamic;
	using Base.Enums;
	using EventJournal;
	using Constants;
	using VisitPay.Enums;


    public class EventClientSmsCategoryChanged : JournalEventTemplate<JournalEventParameters>
	{
		public static JournalEventParameters GetParameters( string eventUserName, string smsCategoryName, string smsCategorySwitchValue, string vpUserName)
		{
			//Property names are case sensitive and must match AdditionalInfo and HumanReadableDescription [tags] exactly
			dynamic parameters = new ExpandoObject();
			parameters.EventUserName = eventUserName;
			parameters.SMSCategoryName = smsCategoryName;
			parameters.SMSCategorySwitchValue = smsCategorySwitchValue;
			parameters.VpUserName = vpUserName;
			return new JournalEventParameters(parameters);
		}

		public override JournalEventCategoryEnum EventRuleTrigger => JournalEventCategoryEnum.ClientUserAction;
		public override string[] EventRuleTags => new[] { JournalEventTags.Sms };
		public override string EventRuleName => "ClientSMSCategoryChanged";
		public override JournalEventTypeEnum AuditLogId => JournalEventTypeEnum.ClientSmsCategoryChanged;
		public override string Description => "Generated when the Client User switches an SMS message category.  A record is created for each switch VPNG-15232";
		public override bool TriggeredByEventUserId => true;
		public override bool AppliedToHsGId => true;
		public override bool AppliedToVpGId => true;
		public override bool AppliedToVpUserIdClientUserId => true;
		public override bool AppliedToFpId => false;
		public override bool AppliedToVisitId => false;
		public override bool AppliedToTransId => false;
		public override bool AppliedToPaymentId => false;
		public override string[] AdditionalInfo => new string[] { "SMSCategoryName", "SMSCategorySwitchValue" };
		public override bool Audit => false;
		public override string HumanReadableDescription => "Client User [EventUserName] changed the SMS Category [SMSCategoryName] to [SMSCategorySwitchValue] for [VpUserName]";
	}
}
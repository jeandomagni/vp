namespace Ivh.Common.EventJournal.Templates.Client
{
	using System.Dynamic;
	using Base.Enums;
	using EventJournal;
	using Constants;
	using VisitPay.Enums;


    public class EventClientCanceledAReconfiguredFinancePlan : JournalEventTemplate<JournalEventParameters>
	{
		public static JournalEventParameters GetParameters( string canceledTerms, string eventUserName, string vpUserName)
		{
			//Property names are case sensitive and must match AdditionalInfo and HumanReadableDescription [tags] exactly
			dynamic parameters = new ExpandoObject();
			parameters.CanceledTerms = canceledTerms;
			parameters.EventUserName = eventUserName;
			parameters.VpUserName = vpUserName;
			return new JournalEventParameters(parameters);
		}

		public override JournalEventCategoryEnum EventRuleTrigger => JournalEventCategoryEnum.ClientUserAction;
		public override string[] EventRuleTags => new[] { JournalEventTags.FinancePlans };
		public override string EventRuleName => "ClientCanceledAReconfiguredFinancePlan";
		public override JournalEventTypeEnum AuditLogId => JournalEventTypeEnum.ClientCanceledAReconfiguredFinancePlan;
		public override string Description => "Generated when a Client User cancels a reconfigured FP before it originates";
		public override bool TriggeredByEventUserId => true;
		public override bool AppliedToHsGId => true;
		public override bool AppliedToVpGId => true;
		public override bool AppliedToVpUserIdClientUserId => true;
		public override bool AppliedToFpId => true;
		public override bool AppliedToVisitId => false;
		public override bool AppliedToTransId => false;
		public override bool AppliedToPaymentId => false;
		public override string[] AdditionalInfo => new string[] { "CanceledTerms" };
		public override bool Audit => false;
		public override string HumanReadableDescription => "Client User [EventUserName] cancelled a Reconfigured, non-Originated Finance Plan for [VpUserName]";
	}
}
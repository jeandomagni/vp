namespace Ivh.Common.EventJournal.Templates.Client
{
	using System.Dynamic;
	using Base.Enums;
	using EventJournal;
	using Constants;
	using VisitPay.Enums;


    public class EventClientScheduledAManualPaymentForTheVpGuarantor : JournalEventTemplate<JournalEventParameters>
	{
		public static JournalEventParameters GetParameters( string eventUserName, string paymentAmount, string scheduledDate, string vpUserName)
		{
			//Property names are case sensitive and must match AdditionalInfo and HumanReadableDescription [tags] exactly
			dynamic parameters = new ExpandoObject();
			parameters.EventUserName = eventUserName;
			parameters.PaymentAmount = paymentAmount;
			parameters.ScheduledDate = scheduledDate;
			parameters.VpUserName = vpUserName;
			return new JournalEventParameters(parameters);
		}

		public override JournalEventCategoryEnum EventRuleTrigger => JournalEventCategoryEnum.ClientUserAction;
		public override string[] EventRuleTags => new[] { JournalEventTags.Payments };
		public override string EventRuleName => "ClientScheduledAManualPaymentForTheVpGuarantor";
		public override JournalEventTypeEnum AuditLogId => JournalEventTypeEnum.ClientScheduledAManualPaymentForTheVpGuarantor;
		public override string Description => "Generated whan a Client User schedules a payment for a VPG from the Client Portal";
		public override bool TriggeredByEventUserId => true;
		public override bool AppliedToHsGId => true;
		public override bool AppliedToVpGId => true;
		public override bool AppliedToVpUserIdClientUserId => true;
		public override bool AppliedToFpId => false;
		public override bool AppliedToVisitId => false;
		public override bool AppliedToTransId => false;
		public override bool AppliedToPaymentId => true;
		public override string[] AdditionalInfo => new string[] {};
		public override bool Audit => false;
		public override string HumanReadableDescription => "Client User [EventUserName] scheduled a manual payment of [PaymentAmount] on [ScheduledDate] for [VpUserName]";
	}
}
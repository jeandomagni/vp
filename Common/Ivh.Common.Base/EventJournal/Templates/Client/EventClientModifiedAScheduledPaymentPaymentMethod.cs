namespace Ivh.Common.EventJournal.Templates.Client
{
	using System.Dynamic;
	using Base.Enums;
	using EventJournal;
	using Constants;
	using VisitPay.Enums;


    public class EventClientModifiedAScheduledPaymentPaymentMethod : JournalEventTemplate<JournalEventParameters>
	{
		public static JournalEventParameters GetParameters( string eventUserName, string newPaymentMethod, string paymentAmount, string previousPaymentMethod, string scheduledAmount, string scheduledDate, string vpUserName)
		{
			//Property names are case sensitive and must match AdditionalInfo and HumanReadableDescription [tags] exactly
			dynamic parameters = new ExpandoObject();
			parameters.EventUserName = eventUserName;
			parameters.NewPaymentMethod = newPaymentMethod;
			parameters.PaymentAmount = paymentAmount;
			parameters.PreviousPaymentMethod = previousPaymentMethod;
			parameters.ScheduledAmount = scheduledAmount;
			parameters.ScheduledDate = scheduledDate;
			parameters.VpUserName = vpUserName;
			return new JournalEventParameters(parameters);
		}

		public override JournalEventCategoryEnum EventRuleTrigger => JournalEventCategoryEnum.ClientUserAction;
		public override string[] EventRuleTags => new[] { JournalEventTags.Payments };
		public override string EventRuleName => "ClientModifiedAScheduledPaymentPaymentMethod";
		public override JournalEventTypeEnum AuditLogId => JournalEventTypeEnum.ClientModifiedAScheduledPaymentPaymentMethod;
		public override string Description => "Generated when a Client User changes to a different payment method on a scheduled, manual payment";
		public override bool TriggeredByEventUserId => true;
		public override bool AppliedToHsGId => true;
		public override bool AppliedToVpGId => true;
		public override bool AppliedToVpUserIdClientUserId => true;
		public override bool AppliedToFpId => false;
		public override bool AppliedToVisitId => false;
		public override bool AppliedToTransId => false;
		public override bool AppliedToPaymentId => true;
		public override string[] AdditionalInfo => new string[] { "ScheduledDate", "PreviousPaymentMethod", "NewPaymentMethod", "ScheduledAmount" };
		public override bool Audit => false;
		public override string HumanReadableDescription => "Client User [EventUserName] changed the payment method on the payment of [PaymentAmount] on [ScheduledDate] for [VpUserName] from [PreviousPaymentMethod] to [NewPaymentMethod]";
	}
}
﻿namespace Ivh.Common.EventJournal.Templates.Client
{
    using System.Dynamic;
    using Base.Enums;
    using Constants;
    using VisitPay.Enums;

    public class EventClientVisitAgingTierChange : JournalEventTemplate<JournalEventParameters>
    {
        public static JournalEventParameters GetParameters(string eventUserName, string newAge, string previousAge, string vpUserName,string reason)
        {
            //Property names are case sensitive and must match AdditionalInfo and HumanReadableDescription [tags] exactly
            dynamic parameters = new ExpandoObject();
            parameters.EventUserName = eventUserName;
            parameters.NewAge = newAge;
            parameters.PreviousAge = previousAge;
            parameters.VpUserName = vpUserName;
            parameters.Reason = reason;
            return new JournalEventParameters(parameters);
        }
        public override JournalEventCategoryEnum EventRuleTrigger => JournalEventCategoryEnum.ClientUserAction;
        public override string[] EventRuleTags => new[] { JournalEventTags.Visits };
        public override string EventRuleName => "ClientModifiedVisitAging";
        public override JournalEventTypeEnum AuditLogId => JournalEventTypeEnum.ClientModifiedVisitAging;
        public override string Description => "Generated when a Client User modifies the aging of a Visit";
        public override bool TriggeredByEventUserId => true;
        public override bool AppliedToHsGId => true;
        public override bool AppliedToVpGId => true;
        public override bool AppliedToVpUserIdClientUserId => true;
        public override bool AppliedToFpId => false;
        public override bool AppliedToVisitId => true;
        public override bool AppliedToTransId => false;
        public override bool AppliedToPaymentId => false;
        public override string[] AdditionalInfo => new string[] { "PreviousAge", "NewAge", "Reason" };
        public override bool Audit => false;
        public override string HumanReadableDescription => "Client User [EventUserName] changed the visit aging from [PreviousAge] to [NewAge] for [VpUserName]";
    }
}

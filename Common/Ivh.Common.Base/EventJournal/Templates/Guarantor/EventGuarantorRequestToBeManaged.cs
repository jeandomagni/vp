namespace Ivh.Common.EventJournal.Templates.Guarantor
{
	using System.Dynamic;
	using Base.Enums;
	using EventJournal;
	using Constants;
	using VisitPay.Enums;


    public class EventGuarantorRequestToBeManaged : JournalEventTemplate<JournalEventParameters>
	{
		public static JournalEventParameters GetParameters( string managingVpGuarantorId, string managingVPUserName, string vpUserName)
		{
			//Property names are case sensitive and must match AdditionalInfo and HumanReadableDescription [tags] exactly
			dynamic parameters = new ExpandoObject();
			parameters.ManagingVpGuarantorId = managingVpGuarantorId;
			parameters.ManagingVPUserName = managingVPUserName;
			parameters.VpUserName = vpUserName;
			return new JournalEventParameters(parameters);
		}

		public override JournalEventCategoryEnum EventRuleTrigger => JournalEventCategoryEnum.VpGuarantorUserAction;
		public override string[] EventRuleTags => new[] { JournalEventTags.Consolidation };
		public override string EventRuleName => "GuarantorRequestToBeManaged";
		public override JournalEventTypeEnum AuditLogId => JournalEventTypeEnum.GuarantorRequestToBeManaged;
		public override string Description => "Generated when a VPG requests to be managed by another VPG";
		public override bool TriggeredByEventUserId => true;
		public override bool AppliedToHsGId => true;
		public override bool AppliedToVpGId => true;
		public override bool AppliedToVpUserIdClientUserId => true;
		public override bool AppliedToFpId => false;
		public override bool AppliedToVisitId => false;
		public override bool AppliedToTransId => false;
		public override bool AppliedToPaymentId => false;
		public override string[] AdditionalInfo => new string[] { "ManagingVpGuarantorId" };
		public override bool Audit => false;
		public override string HumanReadableDescription => "VP User [VpUserName] requested to be managed by VP User [ManagingVPUserName]";
	}
}
namespace Ivh.Common.EventJournal.Templates.Guarantor
{
	using System.Dynamic;
	using Base.Enums;
	using EventJournal;
	using Constants;
	using VisitPay.Enums;


    public class EventGuarantorGuarantorViewedManagedUser : JournalEventTemplate<JournalEventParameters>
	{
		public static JournalEventParameters GetParameters( string managedVPUserName, string vpUserName)
		{
			//Property names are case sensitive and must match AdditionalInfo and HumanReadableDescription [tags] exactly
			dynamic parameters = new ExpandoObject();
			parameters.ManagedVPUserName = managedVPUserName;
			parameters.VpUserName = vpUserName;
			return new JournalEventParameters(parameters);
		}

		public override JournalEventCategoryEnum EventRuleTrigger => JournalEventCategoryEnum.VpGuarantorUserAction;
		public override string[] EventRuleTags => new[] { JournalEventTags.Consolidation };
		public override string EventRuleName => "GuarantorGuarantorViewedManagedUser";
		public override JournalEventTypeEnum AuditLogId => JournalEventTypeEnum.GuarantorGuarantorViewedManagedUser;
		public override string Description => "Generated when a Managing VPG views a Managed VPG";
		public override bool TriggeredByEventUserId => true;
		public override bool AppliedToHsGId => true;
		public override bool AppliedToVpGId => true;
		public override bool AppliedToVpUserIdClientUserId => true;
		public override bool AppliedToFpId => false;
		public override bool AppliedToVisitId => false;
		public override bool AppliedToTransId => false;
		public override bool AppliedToPaymentId => false;
		public override string[] AdditionalInfo => new string[] {};
		public override bool Audit => false;
		public override string HumanReadableDescription => "VP User [VpUserName], a managing user, view their managed user [ManagedVPUserName]";
	}
}
namespace Ivh.Common.EventJournal.Templates.Guarantor
{
	using System.Dynamic;
	using Base.Enums;
	using EventJournal;
	using Constants;
	using VisitPay.Enums;


    public class EventGuarantorCanceledAFinancePlan : JournalEventTemplate<JournalEventParameters>
	{
		public static JournalEventParameters GetParameters( string canceledTerms, string financePlanId, string vpUserName)
		{
			//Property names are case sensitive and must match AdditionalInfo and HumanReadableDescription [tags] exactly
			dynamic parameters = new ExpandoObject();
			parameters.CanceledTerms = canceledTerms;
			parameters.FinancePlanId = financePlanId;
			parameters.VpUserName = vpUserName;
			return new JournalEventParameters(parameters);
		}

		public override JournalEventCategoryEnum EventRuleTrigger => JournalEventCategoryEnum.VpGuarantorUserAction;
		public override string[] EventRuleTags => new[] { JournalEventTags.FinancePlans };
		public override string EventRuleName => "GuarantorCanceledAFinancePlan";
		public override JournalEventTypeEnum AuditLogId => JournalEventTypeEnum.GuarantorCanceledAFinancePlan;
		public override string Description => "Generated when the VPG Cancels a FP";
		public override bool TriggeredByEventUserId => true;
		public override bool AppliedToHsGId => true;
		public override bool AppliedToVpGId => true;
		public override bool AppliedToVpUserIdClientUserId => true;
		public override bool AppliedToFpId => true;
		public override bool AppliedToVisitId => false;
		public override bool AppliedToTransId => false;
		public override bool AppliedToPaymentId => false;
		public override string[] AdditionalInfo => new string[] { "CanceledTerms" };
		public override bool Audit => false;
		public override string HumanReadableDescription => "VP User [VpUserName] cancelled Finance Plan [FinancePlanId]";
	}
}
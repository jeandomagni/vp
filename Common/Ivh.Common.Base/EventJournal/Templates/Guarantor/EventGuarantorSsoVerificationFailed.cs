namespace Ivh.Common.EventJournal.Templates.Guarantor
{
	using System.Dynamic;
	using Base.Enums;
	using EventJournal;
	using Constants;
	using VisitPay.Enums;


    public class EventGuarantorSsoVerificationFailed : JournalEventTemplate<JournalEventParameters>
	{
		public static JournalEventParameters GetParameters( string dateOfBirth, string ssoProviderId, string ssoProviderName, string ssoSystemSourceKey, string vpUserName)
		{
			//Property names are case sensitive and must match AdditionalInfo and HumanReadableDescription [tags] exactly
			dynamic parameters = new ExpandoObject();
			parameters.DateOfBirth = dateOfBirth;
			parameters.SsoProviderId = ssoProviderId;
			parameters.SsoProviderName = ssoProviderName;
			parameters.SsoSystemSourceKey = ssoSystemSourceKey;
			parameters.VpUserName = vpUserName;
			return new JournalEventParameters(parameters);
		}

		public override JournalEventCategoryEnum EventRuleTrigger => JournalEventCategoryEnum.VpGuarantorUserAction;
		public override string[] EventRuleTags => new[] { JournalEventTags.Sso };
		public override string EventRuleName => "GuarantorSsoVerificationFailed";
		public override JournalEventTypeEnum AuditLogId => JournalEventTypeEnum.GuarantorSsoVerificationFailed;
		public override string Description => "Generated when a VPG logs into VP using a VpGuarantorId login that does not match the SSo validation data. (The SSO sent credentials for GID1, then when they entered the VP validation information, they entered login info for GID2 VPNG-10603 (SLHS Mychart) ";
		public override bool TriggeredByEventUserId => true;
		public override bool AppliedToHsGId => true;
		public override bool AppliedToVpGId => true;
		public override bool AppliedToVpUserIdClientUserId => true;
		public override bool AppliedToFpId => false;
		public override bool AppliedToVisitId => false;
		public override bool AppliedToTransId => false;
		public override bool AppliedToPaymentId => false;
		public override string[] AdditionalInfo => new string[] { "SsoProviderId", "DateOfBirth", "SsoSystemSourceKey" };
		public override bool Audit => false;
		public override string HumanReadableDescription => "VP User [VpUserName] accessed VisitPay via SSO Provider [SsoProviderName], but logged in to VisitPay using a VisitPay account that was not linked to the validated Sso account";
	}
}
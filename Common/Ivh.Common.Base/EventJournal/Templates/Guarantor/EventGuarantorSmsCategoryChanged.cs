namespace Ivh.Common.EventJournal.Templates.Guarantor
{
	using System.Dynamic;
	using Base.Enums;
	using EventJournal;
	using Constants;
	using VisitPay.Enums;


    public class EventGuarantorSmsCategoryChanged : JournalEventTemplate<JournalEventParameters>
	{
		public static JournalEventParameters GetParameters( string smsCategoryName, string smsCategorySwitchValue, string vpUserName)
		{
			//Property names are case sensitive and must match AdditionalInfo and HumanReadableDescription [tags] exactly
			dynamic parameters = new ExpandoObject();
			parameters.SMSCategoryName = smsCategoryName;
			parameters.SMSCategorySwitchValue = smsCategorySwitchValue;
			parameters.VpUserName = vpUserName;
			return new JournalEventParameters(parameters);
		}

		public override JournalEventCategoryEnum EventRuleTrigger => JournalEventCategoryEnum.VpGuarantorUserAction;
		public override string[] EventRuleTags => new[] { JournalEventTags.Sms };
		public override string EventRuleName => "GuarantorSmsCategoryChanged";
		public override JournalEventTypeEnum AuditLogId => JournalEventTypeEnum.GuarantorSmsCategoryChanged;
		public override string Description => "Generated when an SMS message category is switched. A record is created for each switch VPNG-15232";
		public override bool TriggeredByEventUserId => true;
		public override bool AppliedToHsGId => true;
		public override bool AppliedToVpGId => true;
		public override bool AppliedToVpUserIdClientUserId => true;
		public override bool AppliedToFpId => false;
		public override bool AppliedToVisitId => false;
		public override bool AppliedToTransId => false;
		public override bool AppliedToPaymentId => false;
		public override string[] AdditionalInfo => new string[] { "SMSCategoryName", "SMSCategorySwitchValue" };
		public override bool Audit => false;
		public override string HumanReadableDescription => "VP User [VpUserName] changed the SMS Category [SMSCategoryName] to [SMSCategorySwitchValue]";
	}
}
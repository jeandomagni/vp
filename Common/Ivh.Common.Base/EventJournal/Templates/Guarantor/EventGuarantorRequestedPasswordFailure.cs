namespace Ivh.Common.EventJournal.Templates.Guarantor
{
	using System.Dynamic;
	using Base.Enums;
	using EventJournal;
	using Constants;
	using VisitPay.Enums;


    public class EventGuarantorRequestedPasswordFailure : JournalEventTemplate<JournalEventParameters>
	{
		public static JournalEventParameters GetParameters( string dateOfBirth, string invalidUserName, string lastName, string sSN4)
		{
			//Property names are case sensitive and must match AdditionalInfo and HumanReadableDescription [tags] exactly
			dynamic parameters = new ExpandoObject();
			parameters.DateOfBirth = dateOfBirth;
			parameters.InvalidUserName = invalidUserName;
			parameters.LastName = lastName;
			parameters.SSN4 = sSN4;
			return new JournalEventParameters(parameters);
		}

		public override JournalEventCategoryEnum EventRuleTrigger => JournalEventCategoryEnum.VpGuarantorUserAction;
		public override string[] EventRuleTags => new[] { JournalEventTags.AppAccess };
		public override string EventRuleName => "GuarantorRequestedPasswordFailure";
		public override JournalEventTypeEnum AuditLogId => JournalEventTypeEnum.GuarantorRequestedPasswordFailure;
		public override string Description => "Generated when a VPG User requests a password reset using an invalid UserName or invalid validation info";
		public override bool TriggeredByEventUserId => true;
		public override bool AppliedToHsGId => false;
		public override bool AppliedToVpGId => false;
		public override bool AppliedToVpUserIdClientUserId => false;
		public override bool AppliedToFpId => false;
		public override bool AppliedToVisitId => false;
		public override bool AppliedToTransId => false;
		public override bool AppliedToPaymentId => false;
		public override string[] AdditionalInfo => new string[] { "InvalidUserName", "LastName", "DateOfBirth", "SSN4" };
		public override bool Audit => false;
		public override string HumanReadableDescription => "A user requested a password reset to the VP Patient Portal using an invalid User Name of [InvalidUserName]";
	}
}
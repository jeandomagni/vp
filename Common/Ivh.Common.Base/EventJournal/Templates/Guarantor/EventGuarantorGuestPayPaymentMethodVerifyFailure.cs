﻿namespace Ivh.Common.EventJournal.Templates.Guarantor
{
    using System.Dynamic;
    using Base.Enums;
    using EventJournal;
    using Constants;
    using VisitPay.Enums;

    public class EventGuarantorGuestPayPaymentMethodVerifyFailure : JournalEventTemplate<JournalEventParameters>
    {
        public static JournalEventParameters GetParameters(string hsGuarantorId, string paymentAuthenticationId)
        {
            //Property names are case sensitive and must match AdditionalInfo and HumanReadableDescription [tags] exactly
            dynamic parameters = new ExpandoObject();
            parameters.HsGuarantorId = hsGuarantorId;
            parameters.PaymentAuthenticationId = paymentAuthenticationId;
            return new JournalEventParameters(parameters);
        }

        public override JournalEventCategoryEnum EventRuleTrigger => JournalEventCategoryEnum.VpGuarantorUserAction;
        public override string[] EventRuleTags => new[] { JournalEventTags.PaymentMethods };
        public override string EventRuleName => "GuarantorGuestPayPaymentMethodVerifyFailure";
        public override JournalEventTypeEnum AuditLogId => JournalEventTypeEnum.GuarantorGuestPayPaymentMethodVerifyFailure;
        public override string Description => "Generated when a HsGuarantor enters a GuestPay payment method that isn't accepted";
        public override bool TriggeredByEventUserId => true;
        public override bool AppliedToHsGId => true;
        public override bool AppliedToVpGId => false;
        public override bool AppliedToVpUserIdClientUserId => false;
        public override bool AppliedToFpId => false;
        public override bool AppliedToVisitId => false;
        public override bool AppliedToTransId => false;
        public override bool AppliedToPaymentId => false;
        public override string[] AdditionalInfo => new string[] { "PaymentAuthenticationId" };
        public override bool Audit => false;
        public override string HumanReadableDescription => "HS Guarantor [HsGuarantorId] attempted to add a payment method and failed";
    }
}

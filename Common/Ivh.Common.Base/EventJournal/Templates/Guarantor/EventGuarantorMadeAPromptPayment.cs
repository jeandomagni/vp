namespace Ivh.Common.EventJournal.Templates.Guarantor
{
	using System.Dynamic;
	using Base.Enums;
	using EventJournal;
	using Constants;
	using VisitPay.Enums;


    public class EventGuarantorMadeAPromptPayment : JournalEventTemplate<JournalEventParameters>
	{
		public static JournalEventParameters GetParameters( string financePlanId, string paymentAmount, string paymentDate, string paymentMethod, string paymentOption, string vpUserName, string vpVisitId)
		{
			//Property names are case sensitive and must match AdditionalInfo and HumanReadableDescription [tags] exactly
			dynamic parameters = new ExpandoObject();
			parameters.FinancePlanId = financePlanId;
			parameters.PaymentAmount = paymentAmount;
			parameters.PaymentDate = paymentDate;
			parameters.PaymentMethod = paymentMethod;
			parameters.PaymentOption = paymentOption;
			parameters.VpUserName = vpUserName;
			parameters.VpVisitId = vpVisitId;
			return new JournalEventParameters(parameters);
		}

		public override JournalEventCategoryEnum EventRuleTrigger => JournalEventCategoryEnum.VpGuarantorUserAction;
		public override string[] EventRuleTags => new[] { JournalEventTags.Payments };
		public override string EventRuleName => "GuarantorMadeAPromptPayment";
		public override JournalEventTypeEnum AuditLogId => JournalEventTypeEnum.GuarantorMadeAPromptPayment;
		public override string Description => "Generated when a VPG makes a prompt payment (right now)";
		public override bool TriggeredByEventUserId => true;
		public override bool AppliedToHsGId => true;
		public override bool AppliedToVpGId => true;
		public override bool AppliedToVpUserIdClientUserId => true;
		public override bool AppliedToFpId => false;
		public override bool AppliedToVisitId => false;
		public override bool AppliedToTransId => false;
		public override bool AppliedToPaymentId => true;
		public override string[] AdditionalInfo => new string[] { "PaymentDate", "PaymentMethod", "PaymentAmount", "PaymentOption", "VpVisitId", "FinancePlanId" };
		public override bool Audit => false;
		public override string HumanReadableDescription => "VP User [VpUserName] made a prompt payment of [PaymentAmount]";
	}
}
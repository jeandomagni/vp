namespace Ivh.Common.EventJournal.Templates.Guarantor
{
	using System.Dynamic;
	using Base.Enums;
	using EventJournal;
	using Constants;
	using VisitPay.Enums;


    public class EventGuarantorDeletedPhoneNumber : JournalEventTemplate<JournalEventParameters>
	{
		public static JournalEventParameters GetParameters( string phoneType, string previousPhoneNumber, string vpUserName)
		{
			//Property names are case sensitive and must match AdditionalInfo and HumanReadableDescription [tags] exactly
			dynamic parameters = new ExpandoObject();
			parameters.PhoneType = phoneType;
			parameters.PreviousPhoneNumber = previousPhoneNumber;
			parameters.VpUserName = vpUserName;
			return new JournalEventParameters(parameters);
		}

		public override JournalEventCategoryEnum EventRuleTrigger => JournalEventCategoryEnum.VpGuarantorUserAction;
		public override string[] EventRuleTags => new[] { JournalEventTags.AcctMngmnt };
		public override string EventRuleName => "GuarantorDeletedPhoneNumber";
		public override JournalEventTypeEnum AuditLogId => JournalEventTypeEnum.GuarantorDeletedPhoneNumber;
		public override string Description => "Generated when a VPG deletes a VPG phone number";
		public override bool TriggeredByEventUserId => true;
		public override bool AppliedToHsGId => true;
		public override bool AppliedToVpGId => true;
		public override bool AppliedToVpUserIdClientUserId => true;
		public override bool AppliedToFpId => false;
		public override bool AppliedToVisitId => false;
		public override bool AppliedToTransId => false;
		public override bool AppliedToPaymentId => false;
		public override string[] AdditionalInfo => new string[] { "PhoneType", "PreviousPhoneNumber" };
		public override bool Audit => false;
		public override string HumanReadableDescription => "VP User [VpUserName] deleted [PhoneType] phone [PreviousPhoneNumber]";
	}
}
namespace Ivh.Common.EventJournal.Templates.Guarantor
{
	using System.Dynamic;
	using Base.Enums;
	using EventJournal;
	using Constants;
	using VisitPay.Enums;


    public class EventGuarantorRescheduledAFinancePlanPayment : JournalEventTemplate<JournalEventParameters>
	{
		public static JournalEventParameters GetParameters( string financePlanId, string newDate, string previousDate, string rescheduleReason, string vpUserName)
		{
			//Property names are case sensitive and must match AdditionalInfo and HumanReadableDescription [tags] exactly
			dynamic parameters = new ExpandoObject();
			parameters.FinancePlanId = financePlanId;
			parameters.NewDate = newDate;
			parameters.PreviousDate = previousDate;
			parameters.RescheduleReason = rescheduleReason;
			parameters.VpUserName = vpUserName;
			return new JournalEventParameters(parameters);
		}

		public override JournalEventCategoryEnum EventRuleTrigger => JournalEventCategoryEnum.VpGuarantorUserAction;
		public override string[] EventRuleTags => new[] { JournalEventTags.FinancePlans };
		public override string EventRuleName => "GuarantorRescheduledAFinancePlanPayment";
		public override JournalEventTypeEnum AuditLogId => JournalEventTypeEnum.GuarantorRescheduledAFinancePlanPayment;
		public override string Description => "Generated when a VPG recheduled a FP payment";
		public override bool TriggeredByEventUserId => true;
		public override bool AppliedToHsGId => true;
		public override bool AppliedToVpGId => true;
		public override bool AppliedToVpUserIdClientUserId => true;
		public override bool AppliedToFpId => true;
		public override bool AppliedToVisitId => false;
		public override bool AppliedToTransId => false;
		public override bool AppliedToPaymentId => true;
		public override string[] AdditionalInfo => new string[] { "NewDate", "RescheduleReason" };
		public override bool Audit => false;
		public override string HumanReadableDescription => "VP User [VpUserName] rescheduled Finance Plan [FinancePlanId] payment from [PreviousDate] to [NewDate] for [RescheduleReason]";
	}
}
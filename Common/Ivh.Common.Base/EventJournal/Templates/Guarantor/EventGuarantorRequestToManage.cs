namespace Ivh.Common.EventJournal.Templates.Guarantor
{
	using System.Dynamic;
	using Base.Enums;
	using EventJournal;
	using Constants;
	using VisitPay.Enums;


    public class EventGuarantorRequestToManage : JournalEventTemplate<JournalEventParameters>
	{
		public static JournalEventParameters GetParameters( string managedVpGuarantorId, string managedVPUserName, string vpUserName)
		{
			//Property names are case sensitive and must match AdditionalInfo and HumanReadableDescription [tags] exactly
			dynamic parameters = new ExpandoObject();
			parameters.ManagedVpGuarantorId = managedVpGuarantorId;
			parameters.ManagedVPUserName = managedVPUserName;
			parameters.VpUserName = vpUserName;
			return new JournalEventParameters(parameters);
		}

		public override JournalEventCategoryEnum EventRuleTrigger => JournalEventCategoryEnum.VpGuarantorUserAction;
		public override string[] EventRuleTags => new[] { JournalEventTags.Consolidation };
		public override string EventRuleName => "GuarantorRequestToManage";
		public override JournalEventTypeEnum AuditLogId => JournalEventTypeEnum.GuarantorRequestToManage;
		public override string Description => "Generated when a VPG requests to manage another VPG";
		public override bool TriggeredByEventUserId => true;
		public override bool AppliedToHsGId => true;
		public override bool AppliedToVpGId => true;
		public override bool AppliedToVpUserIdClientUserId => true;
		public override bool AppliedToFpId => false;
		public override bool AppliedToVisitId => false;
		public override bool AppliedToTransId => false;
		public override bool AppliedToPaymentId => false;
		public override string[] AdditionalInfo => new string[] { "ManagedVpGuarantorId" };
		public override bool Audit => false;
		public override string HumanReadableDescription => "VP User [VpUserName] requested to manage VP User [ManagedVPUserName]";
	}
}
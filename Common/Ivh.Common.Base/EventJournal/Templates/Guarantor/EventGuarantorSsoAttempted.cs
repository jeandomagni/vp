namespace Ivh.Common.EventJournal.Templates.Guarantor
{
	using System.Dynamic;
	using Base.Enums;
	using EventJournal;
	using Constants;
	using VisitPay.Enums;


    public class EventGuarantorSsoAttempted : JournalEventTemplate<JournalEventParameters>
	{
		public static JournalEventParameters GetParameters( string ssoProviderId, string ssoProviderName, string vpUserName)
		{
			//Property names are case sensitive and must match AdditionalInfo and HumanReadableDescription [tags] exactly
			dynamic parameters = new ExpandoObject();
			parameters.SsoProviderId = ssoProviderId;
			parameters.SsoProviderName = ssoProviderName;
			parameters.VpUserName = vpUserName;
			return new JournalEventParameters(parameters);
		}

		public override JournalEventCategoryEnum EventRuleTrigger => JournalEventCategoryEnum.VpGuarantorUserAction;
		public override string[] EventRuleTags => new[] { JournalEventTags.Sso };
		public override string EventRuleName => "GuarantorSsoAttempted";
		public override JournalEventTypeEnum AuditLogId => JournalEventTypeEnum.GuarantorSsoAttempted;
		public override string Description => "Generated when a user tries to SSO to a partner site from VP VPNG-16328 (HQY)";
		public override bool TriggeredByEventUserId => true;
		public override bool AppliedToHsGId => true;
		public override bool AppliedToVpGId => true;
		public override bool AppliedToVpUserIdClientUserId => true;
		public override bool AppliedToFpId => false;
		public override bool AppliedToVisitId => false;
		public override bool AppliedToTransId => false;
		public override bool AppliedToPaymentId => false;
		public override string[] AdditionalInfo => new string[] { "SsoProviderId" };
		public override bool Audit => false;
		public override string HumanReadableDescription => "VP User [VpUserName] attempted to SSO to [SsoProviderName]";
	}
}
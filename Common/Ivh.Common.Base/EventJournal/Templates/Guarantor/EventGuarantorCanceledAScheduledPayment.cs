namespace Ivh.Common.EventJournal.Templates.Guarantor
{
	using System.Dynamic;
	using Base.Enums;
	using EventJournal;
	using Constants;
	using VisitPay.Enums;


    public class EventGuarantorCanceledAScheduledPayment : JournalEventTemplate<JournalEventParameters>
	{
		public static JournalEventParameters GetParameters( string cancelReason, string paymentMethod, string scheduledAmount, string scheduledDate, string vpUserName)
		{
			//Property names are case sensitive and must match AdditionalInfo and HumanReadableDescription [tags] exactly
			dynamic parameters = new ExpandoObject();
			parameters.CancelReason = cancelReason;
			parameters.PaymentMethod = paymentMethod;
			parameters.ScheduledAmount = scheduledAmount;
			parameters.ScheduledDate = scheduledDate;
			parameters.VpUserName = vpUserName;
			return new JournalEventParameters(parameters);
		}

		public override JournalEventCategoryEnum EventRuleTrigger => JournalEventCategoryEnum.VpGuarantorUserAction;
		public override string[] EventRuleTags => new[] { JournalEventTags.Payments };
		public override string EventRuleName => "GuarantorCanceledAScheduledPayment";
		public override JournalEventTypeEnum AuditLogId => JournalEventTypeEnum.GuarantorCanceledAScheduledPayment;
		public override string Description => "Generated when a VPG cancels a scheduled, manual payment";
		public override bool TriggeredByEventUserId => true;
		public override bool AppliedToHsGId => true;
		public override bool AppliedToVpGId => true;
		public override bool AppliedToVpUserIdClientUserId => true;
		public override bool AppliedToFpId => false;
		public override bool AppliedToVisitId => false;
		public override bool AppliedToTransId => false;
		public override bool AppliedToPaymentId => true;
		public override string[] AdditionalInfo => new string[] { "ScheduledDate", "PaymentMethod", "ScheduledAmount", "CancelReason" };
		public override bool Audit => false;
		public override string HumanReadableDescription => "VP User [VpUserName] cancelled a payment of [ScheduledAmount] scheduled for [ScheduledDate] because [CancelReason]";
	}
}
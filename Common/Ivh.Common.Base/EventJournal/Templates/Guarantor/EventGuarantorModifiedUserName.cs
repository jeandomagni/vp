namespace Ivh.Common.EventJournal.Templates.Guarantor
{
	using System.Dynamic;
	using Base.Enums;
	using EventJournal;
	using Constants;
	using VisitPay.Enums;


    public class EventGuarantorModifiedUserName : JournalEventTemplate<JournalEventParameters>
	{
		public static JournalEventParameters GetParameters( string newUserName, string previousUserName, string vpUserName)
		{
			//Property names are case sensitive and must match AdditionalInfo and HumanReadableDescription [tags] exactly
			dynamic parameters = new ExpandoObject();
			parameters.NewUserName = newUserName;
			parameters.PreviousUserName = previousUserName;
			parameters.VpUserName = vpUserName;
			return new JournalEventParameters(parameters);
		}

		public override JournalEventCategoryEnum EventRuleTrigger => JournalEventCategoryEnum.VpGuarantorUserAction;
		public override string[] EventRuleTags => new[] { JournalEventTags.AcctMngmnt };
		public override string EventRuleName => "GuarantorModifiedUserName";
		public override JournalEventTypeEnum AuditLogId => JournalEventTypeEnum.GuarantorModifiedUserName;
		public override string Description => "Generated when a VPG modifies their user name";
		public override bool TriggeredByEventUserId => true;
		public override bool AppliedToHsGId => true;
		public override bool AppliedToVpGId => true;
		public override bool AppliedToVpUserIdClientUserId => true;
		public override bool AppliedToFpId => false;
		public override bool AppliedToVisitId => false;
		public override bool AppliedToTransId => false;
		public override bool AppliedToPaymentId => false;
		public override string[] AdditionalInfo => new string[] { "PreviousUserName", "NewUserName" };
		public override bool Audit => false;
		public override string HumanReadableDescription => "VP User [VpUserName] changed their UserName from [PreviousUserName] to [NewUserName]";
	}
}
﻿namespace Ivh.Common.EventJournal.Templates.Guarantor
{
    using System.Dynamic;
    using Base.Enums;
    using EventJournal;
    using Constants;
    using VisitPay.Enums;

    public class EventGuarantorGuestPayReceiptDownload : JournalEventTemplate<JournalEventParameters>
    {
        public static JournalEventParameters GetParameters( string hsGuarantorId, string receiptId)
        {
            //Property names are case sensitive and must match AdditionalInfo and HumanReadableDescription [tags] exactly
            dynamic parameters = new ExpandoObject();
            parameters.HsGuarantorId = hsGuarantorId;
            parameters.ReceiptId = receiptId;
            return new JournalEventParameters(parameters);
        }

        public override JournalEventCategoryEnum EventRuleTrigger => JournalEventCategoryEnum.VpGuarantorUserAction;
        public override string[] EventRuleTags => new[] { JournalEventTags.Downloads };
        public override string EventRuleName => "GuarantorGuestPayReceiptDownload";
        public override JournalEventTypeEnum AuditLogId => JournalEventTypeEnum.GuarantorGuestPayReceiptDownload;
        public override string Description => "Generated when a HsGuarantor downloads their Guestpay receipt ";
        public override bool TriggeredByEventUserId => true;
        public override bool AppliedToHsGId => true;
        public override bool AppliedToVpGId => false;
        public override bool AppliedToVpUserIdClientUserId => false;
        public override bool AppliedToFpId => false;
        public override bool AppliedToVisitId => false;
        public override bool AppliedToTransId => false;
        public override bool AppliedToPaymentId => false;
        public override string[] AdditionalInfo => new string[] {"ReceiptId"};
        public override bool Audit => false;
        public override string HumanReadableDescription => "HS Guarantor [HsGuarantorId] downloaded their payment reciept [ReceiptId]";
    }
}

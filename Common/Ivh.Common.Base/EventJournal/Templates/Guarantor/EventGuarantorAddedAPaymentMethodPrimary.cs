namespace Ivh.Common.EventJournal.Templates.Guarantor
{
	using System.Dynamic;
	using Base.Enums;
	using EventJournal;
	using Constants;
	using VisitPay.Enums;


    public class EventGuarantorAddedAPaymentMethodPrimary : JournalEventTemplate<JournalEventParameters>
	{
		public static JournalEventParameters GetParameters( string accountNumber, string primaryFlag, string vpUserName)
		{
			//Property names are case sensitive and must match AdditionalInfo and HumanReadableDescription [tags] exactly
			dynamic parameters = new ExpandoObject();
			parameters.AccountNumber = accountNumber;
			parameters.PrimaryFlag = primaryFlag;
			parameters.VpUserName = vpUserName;
			return new JournalEventParameters(parameters);
		}

		public override JournalEventCategoryEnum EventRuleTrigger => JournalEventCategoryEnum.VpGuarantorUserAction;
		public override string[] EventRuleTags => new[] { JournalEventTags.PaymentMethods };
		public override string EventRuleName => "GuarantorAddedAPaymentMethodPrimary";
		public override JournalEventTypeEnum AuditLogId => JournalEventTypeEnum.GuarantorAddedAPaymentMethodPrimary;
		public override string Description => "Generated when a payment method is added";
		public override bool TriggeredByEventUserId => true;
		public override bool AppliedToHsGId => true;
		public override bool AppliedToVpGId => true;
		public override bool AppliedToVpUserIdClientUserId => true;
		public override bool AppliedToFpId => false;
		public override bool AppliedToVisitId => false;
		public override bool AppliedToTransId => false;
		public override bool AppliedToPaymentId => false;
		public override string[] AdditionalInfo => new string[] { "AccountNumber", "PrimaryFlag" };
		public override bool Audit => false;
		public override string HumanReadableDescription => "VP User [VpUserName]  added payment method [AccountNumber] and set it as the primary payment method";
	}
}
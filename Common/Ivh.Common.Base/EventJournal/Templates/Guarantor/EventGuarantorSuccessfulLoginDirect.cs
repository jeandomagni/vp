namespace Ivh.Common.EventJournal.Templates.Guarantor
{
	using System.Dynamic;
	using Base.Enums;
	using EventJournal;
	using Constants;
	using VisitPay.Enums;


    public class EventGuarantorSuccessfulLoginDirect : JournalEventTemplate<JournalEventParameters>
	{
		public static JournalEventParameters GetParameters( string applicationId, string deviceType, string url, string vpUserName)
		{
			//Property names are case sensitive and must match AdditionalInfo and HumanReadableDescription [tags] exactly
			dynamic parameters = new ExpandoObject();
			parameters.ApplicationID = applicationId;
			parameters.DeviceType = deviceType;
			parameters.URL = url;
			parameters.VpUserName = vpUserName;
			return new JournalEventParameters(parameters);
		}

		public override JournalEventCategoryEnum EventRuleTrigger => JournalEventCategoryEnum.VpGuarantorUserAction;
		public override string[] EventRuleTags => new[] { JournalEventTags.AppAccess };
		public override string EventRuleName => "GuarantorSuccessfulLoginDirect";
		public override JournalEventTypeEnum AuditLogId => JournalEventTypeEnum.GuarantorSuccessfulLoginDirect;
		public override string Description => "Generated when a user logs into VP (Patient portal)";
		public override bool TriggeredByEventUserId => true;
		public override bool AppliedToHsGId => true;
		public override bool AppliedToVpGId => true;
		public override bool AppliedToVpUserIdClientUserId => true;
		public override bool AppliedToFpId => false;
		public override bool AppliedToVisitId => false;
		public override bool AppliedToTransId => false;
		public override bool AppliedToPaymentId => false;
		public override string[] AdditionalInfo => new string[] { "ApplicationID", "DeviceType", "URL" };
		public override bool Audit => false;
		public override string HumanReadableDescription => "VP User [VpUserName] logged into VisitPay";
	}
}
namespace Ivh.Common.EventJournal.Templates.Guarantor
{
	using System.Dynamic;
	using Base.Enums;
	using EventJournal;
	using Constants;
	using VisitPay.Enums;


    public class EventGuarantorAcceptedReconfiguredTerms : JournalEventTemplate<JournalEventParameters>
	{
		public static JournalEventParameters GetParameters( string financePlanId, string newTerms, string vpUserName)
		{
			//Property names are case sensitive and must match AdditionalInfo and HumanReadableDescription [tags] exactly
			dynamic parameters = new ExpandoObject();
			parameters.FinancePlanId = financePlanId;
			parameters.NewTerms = newTerms;
			parameters.VpUserName = vpUserName;
			return new JournalEventParameters(parameters);
		}

		public override JournalEventCategoryEnum EventRuleTrigger => JournalEventCategoryEnum.VpGuarantorUserAction;
		public override string[] EventRuleTags => new[] { JournalEventTags.FinancePlans };
		public override string EventRuleName => "GuarantorAcceptedReconfiguredTerms";
		public override JournalEventTypeEnum AuditLogId => JournalEventTypeEnum.GuarantorAcceptedReconfiguredTerms;
		public override string Description => "Generated when the VPG accepts a reconfigured FP";
		public override bool TriggeredByEventUserId => true;
		public override bool AppliedToHsGId => true;
		public override bool AppliedToVpGId => true;
		public override bool AppliedToVpUserIdClientUserId => true;
		public override bool AppliedToFpId => true;
		public override bool AppliedToVisitId => false;
		public override bool AppliedToTransId => false;
		public override bool AppliedToPaymentId => false;
		public override string[] AdditionalInfo => new string[] { "NewTerms" };
		public override bool Audit => false;
		public override string HumanReadableDescription => "VP User [VpUserName] accepts terms on reconfigured Finance Plan [FinancePlanId]";
	}
}
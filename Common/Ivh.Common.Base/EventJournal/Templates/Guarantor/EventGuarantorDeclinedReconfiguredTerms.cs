namespace Ivh.Common.EventJournal.Templates.Guarantor
{
	using System.Dynamic;
	using Base.Enums;
	using EventJournal;
	using Constants;
	using VisitPay.Enums;


    public class EventGuarantorDeclinedReconfiguredTerms : JournalEventTemplate<JournalEventParameters>
	{
		public static JournalEventParameters GetParameters( string declinedTerms, string financePlanId, string vpUserName)
		{
			//Property names are case sensitive and must match AdditionalInfo and HumanReadableDescription [tags] exactly
			dynamic parameters = new ExpandoObject();
			parameters.DeclinedTerms = declinedTerms;
			parameters.FinancePlanId = financePlanId;
			parameters.VpUserName = vpUserName;
			return new JournalEventParameters(parameters);
		}

		public override JournalEventCategoryEnum EventRuleTrigger => JournalEventCategoryEnum.VpGuarantorUserAction;
		public override string[] EventRuleTags => new[] { JournalEventTags.FinancePlans };
		public override string EventRuleName => "GuarantorDeclinedReconfiguredTerms";
		public override JournalEventTypeEnum AuditLogId => JournalEventTypeEnum.GuarantorDeclinedReconfiguredTerms;
		public override string Description => "Generated when the VPG declines a reconfigured FP";
		public override bool TriggeredByEventUserId => true;
		public override bool AppliedToHsGId => true;
		public override bool AppliedToVpGId => true;
		public override bool AppliedToVpUserIdClientUserId => true;
		public override bool AppliedToFpId => true;
		public override bool AppliedToVisitId => false;
		public override bool AppliedToTransId => false;
		public override bool AppliedToPaymentId => false;
		public override string[] AdditionalInfo => new string[] { "DeclinedTerms" };
		public override bool Audit => false;
		public override string HumanReadableDescription => "VP User [VpUserName] declined the terms on reconfigured Finance Plan [FinancePlanId]";
	}
}
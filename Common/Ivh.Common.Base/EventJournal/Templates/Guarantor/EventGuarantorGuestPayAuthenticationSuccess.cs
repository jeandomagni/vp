﻿namespace Ivh.Common.EventJournal.Templates.Guarantor
{
    using System.Dynamic;
    using Base.Enums;
    using EventJournal;
    using Constants;
    using VisitPay.Enums;

    public class EventGuarantorGuestPayAuthenticationSuccess : JournalEventTemplate<JournalEventParameters>
    {
        public static JournalEventParameters GetParameters( string hsGuarantorId)
        {
            //Property names are case sensitive and must match AdditionalInfo and HumanReadableDescription [tags] exactly
            dynamic parameters = new ExpandoObject();
            parameters.HsGuarantorId = hsGuarantorId;
            return new JournalEventParameters(parameters);
        }

        public override JournalEventCategoryEnum EventRuleTrigger => JournalEventCategoryEnum.VpGuarantorUserAction;
        public override string[] EventRuleTags => new[] { JournalEventTags.AppAccess };
        public override string EventRuleName => "GuarantorGuestPayAuthenticationSuccess";
        public override JournalEventTypeEnum AuditLogId => JournalEventTypeEnum.GuarantorGuestPayAuthenticationSuccess;
        public override string Description => "Generated when a VPGuarantor successfully gets past step 1 of Guestpay";
        public override bool TriggeredByEventUserId => true;
        public override bool AppliedToHsGId => true;
        public override bool AppliedToVpGId => false;
        public override bool AppliedToVpUserIdClientUserId => false;
        public override bool AppliedToFpId => false;
        public override bool AppliedToVisitId => false;
        public override bool AppliedToTransId => false;
        public override bool AppliedToPaymentId => false;
        public override string[] AdditionalInfo => new string[] {};
        public override bool Audit => false;
        public override string HumanReadableDescription => "HS Guarantor [HsGuarantorId] successfully authenticated in GuestPay";
    }
}

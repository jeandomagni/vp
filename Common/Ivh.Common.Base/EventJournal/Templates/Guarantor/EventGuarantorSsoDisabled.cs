namespace Ivh.Common.EventJournal.Templates.Guarantor
{
	using System.Dynamic;
	using Base.Enums;
	using EventJournal;
	using Constants;
	using VisitPay.Enums;


    public class EventGuarantorSsoDisabled : JournalEventTemplate<JournalEventParameters>
	{
		public static JournalEventParameters GetParameters( string ssoProviderId, string ssoProviderName, string vpUserName)
		{
			//Property names are case sensitive and must match AdditionalInfo and HumanReadableDescription [tags] exactly
			dynamic parameters = new ExpandoObject();
			parameters.SsoProviderId = ssoProviderId;
			parameters.SsoProviderName = ssoProviderName;
			parameters.VpUserName = vpUserName;
			return new JournalEventParameters(parameters);
		}

		public override JournalEventCategoryEnum EventRuleTrigger => JournalEventCategoryEnum.VpGuarantorUserAction;
		public override string[] EventRuleTags => new[] { JournalEventTags.Sso };
		public override string EventRuleName => "GuarantorSsoDisabled";
		public override JournalEventTypeEnum AuditLogId => JournalEventTypeEnum.GuarantorSsoDisabled;
		public override string Description => "Generated when a user disables SSO after terms have been accepted VPNG-16328 (HQY), VPNG-16526";
		public override bool TriggeredByEventUserId => true;
		public override bool AppliedToHsGId => true;
		public override bool AppliedToVpGId => true;
		public override bool AppliedToVpUserIdClientUserId => true;
		public override bool AppliedToFpId => false;
		public override bool AppliedToVisitId => false;
		public override bool AppliedToTransId => false;
		public override bool AppliedToPaymentId => false;
		public override string[] AdditionalInfo => new string[] { "SsoProviderId" };
		public override bool Audit => false;
		public override string HumanReadableDescription => "VP User [VpUserName] disabled SSO with [SsoProviderName]";
	}
}
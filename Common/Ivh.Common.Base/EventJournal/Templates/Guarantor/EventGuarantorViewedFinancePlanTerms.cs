namespace Ivh.Common.EventJournal.Templates.Guarantor
{
	using System.Dynamic;
	using Base.Enums;
	using EventJournal;
	using Constants;
	using VisitPay.Enums;


    public class EventGuarantorViewedFinancePlanTerms : JournalEventTemplate<JournalEventParameters>
	{
		public static JournalEventParameters GetParameters( string annualInterestRate, string effectiveDate, string finalPaymentDate, string financedAmount, string firstPaymentDueDate, string newMonthlyPayment, string numberOfPayments, string paymentAmount, string previousMonthlyPayment, string totalInterestOwed, string vpUserName)
		{
			//Property names are case sensitive and must match AdditionalInfo and HumanReadableDescription [tags] exactly
			dynamic parameters = new ExpandoObject();
			parameters.AnnualInterestRate = annualInterestRate;
			parameters.EffectiveDate = effectiveDate;
			parameters.FinalPaymentDate = finalPaymentDate;
			parameters.FinancedAmount = financedAmount;
			parameters.FirstPaymentDueDate = firstPaymentDueDate;
			parameters.NewMonthlyPayment = newMonthlyPayment;
			parameters.NumberOfPayments = numberOfPayments;
			parameters.PaymentAmount = paymentAmount;
			parameters.PreviousMonthlyPayment = previousMonthlyPayment;
			parameters.TotalInterestOwed = totalInterestOwed;
			parameters.VpUserName = vpUserName;
			return new JournalEventParameters(parameters);
		}

		public override JournalEventCategoryEnum EventRuleTrigger => JournalEventCategoryEnum.VpGuarantorUserAction;
		public override string[] EventRuleTags => new[] { JournalEventTags.FinancePlans };
		public override string EventRuleName => "GuarantorViewedFinancePlanTermsPaymentAmount";
		public override JournalEventTypeEnum AuditLogId => JournalEventTypeEnum.GuarantorViewedFinancePlanTerms;
		public override string Description => "Generated when a VPG sets a payment amt or # of payments and gets terms";
		public override bool TriggeredByEventUserId => true;
		public override bool AppliedToHsGId => true;
		public override bool AppliedToVpGId => true;
		public override bool AppliedToVpUserIdClientUserId => true;
		public override bool AppliedToFpId => false;
		public override bool AppliedToVisitId => false;
		public override bool AppliedToTransId => false;
		public override bool AppliedToPaymentId => false;
		public override string[] AdditionalInfo => new string[] { "FinancedAmount", "AnnualInterestRate", "TotalInterestOwed", "NumberOfPayments", "FinalPaymentDate", "PreviousMonthlyPayment", "NewMonthlyPayment", "EffectiveDate", "FirstPaymentDueDate" };
		public override bool Audit => false;
		public override string HumanReadableDescription => "VP User [VpUserName] requested terms to finance [FinancedAmount] with a payment of [PaymentAmount] and [NumberOfPayments] payments";
	}
}
namespace Ivh.Common.EventJournal.Templates.Guarantor
{
	using System.Dynamic;
	using Base.Enums;
	using EventJournal;
	using Constants;
	using VisitPay.Enums;


    public class EventGuarantorSmsDeactivated : JournalEventTemplate<JournalEventParameters>
	{
		public static JournalEventParameters GetParameters( string phoneNumber, string vpUserName)
		{
			//Property names are case sensitive and must match AdditionalInfo and HumanReadableDescription [tags] exactly
			dynamic parameters = new ExpandoObject();
			parameters.PhoneNumber = phoneNumber;
			parameters.VpUserName = vpUserName;
			return new JournalEventParameters(parameters);
		}

		public override JournalEventCategoryEnum EventRuleTrigger => JournalEventCategoryEnum.VpGuarantorUserAction;
		public override string[] EventRuleTags => new[] { JournalEventTags.Sms };
		public override string EventRuleName => "GuarantorSmsDeactivated";
		public override JournalEventTypeEnum AuditLogId => JournalEventTypeEnum.GuarantorSmsDeactivated;
		public override string Description => "Generated when a phone number is deactivated for SMS. This can happen 1) when a different number if validated and 2) when the disable texting button is selected VPNG-15232";
		public override bool TriggeredByEventUserId => true;
		public override bool AppliedToHsGId => true;
		public override bool AppliedToVpGId => true;
		public override bool AppliedToVpUserIdClientUserId => true;
		public override bool AppliedToFpId => false;
		public override bool AppliedToVisitId => false;
		public override bool AppliedToTransId => false;
		public override bool AppliedToPaymentId => false;
		public override string[] AdditionalInfo => new string[] { "PhoneNumber" };
		public override bool Audit => false;
		public override string HumanReadableDescription => "VP User [VpUserName] deactivated phone number [PhoneNumber] as an SMS number";
	}
}
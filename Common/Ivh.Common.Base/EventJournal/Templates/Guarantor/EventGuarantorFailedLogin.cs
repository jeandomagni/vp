namespace Ivh.Common.EventJournal.Templates.Guarantor
{
	using System.Dynamic;
	using Base.Enums;
	using EventJournal;
	using Constants;
	using VisitPay.Enums;


    public class EventGuarantorFailedLogin : JournalEventTemplate<JournalEventParameters>
	{
		public static JournalEventParameters GetParameters( string accountDisabled, string accountLocked, string attemptedLoginName, string loginName, string passwordHashed)
		{
			//Property names are case sensitive and must match AdditionalInfo and HumanReadableDescription [tags] exactly
			dynamic parameters = new ExpandoObject();
			parameters.AccountDisabled = accountDisabled;
			parameters.AccountLocked = accountLocked;
			parameters.AttemptedLoginName = attemptedLoginName;
			parameters.LoginName = loginName;
			parameters.PasswordHashed = passwordHashed;
			return new JournalEventParameters(parameters);
		}

		public override JournalEventCategoryEnum EventRuleTrigger => JournalEventCategoryEnum.VpGuarantorUserAction;
		public override string[] EventRuleTags => new[] { JournalEventTags.AppAccess };
		public override string EventRuleName => "GuarantorFailedLogin";
		public override JournalEventTypeEnum AuditLogId => JournalEventTypeEnum.GuarantorFailedLogin;
		public override string Description => "Generated when a user attempts to login and fails Added LoginName to Additional Data field in VPNG-18649";
		public override bool TriggeredByEventUserId => true;
		public override bool AppliedToHsGId => false;
		public override bool AppliedToVpGId => false;
		public override bool AppliedToVpUserIdClientUserId => false;
		public override bool AppliedToFpId => false;
		public override bool AppliedToVisitId => false;
		public override bool AppliedToTransId => false;
		public override bool AppliedToPaymentId => false;
		public override string[] AdditionalInfo => new string[] { "PasswordHashed", "AccountLocked", "AccountDisabled", "LoginName" };
		public override bool Audit => false;
		public override string HumanReadableDescription => "User [AttemptedLoginName] failed trying to login";
	}
}
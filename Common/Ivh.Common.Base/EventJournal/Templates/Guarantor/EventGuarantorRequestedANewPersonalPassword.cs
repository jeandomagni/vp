namespace Ivh.Common.EventJournal.Templates.Guarantor
{
	using System.Dynamic;
	using Base.Enums;
	using EventJournal;
	using Constants;
	using VisitPay.Enums;


    public class EventGuarantorRequestedANewPersonalPassword : JournalEventTemplate<JournalEventParameters>
	{
		public static JournalEventParameters GetParameters( string dateOfBirth, string foundMatchFlag, string lastName, string sSN4, string submittedUserName, string vpUserName)
		{
			//Property names are case sensitive and must match AdditionalInfo and HumanReadableDescription [tags] exactly
			dynamic parameters = new ExpandoObject();
			parameters.DateOfBirth = dateOfBirth;
			parameters.FoundMatchFlag = foundMatchFlag;
			parameters.LastName = lastName;
			parameters.SSN4 = sSN4;
			parameters.SubmittedUserName = submittedUserName;
			parameters.VpUserName = vpUserName;
			return new JournalEventParameters(parameters);
		}

		public override JournalEventCategoryEnum EventRuleTrigger => JournalEventCategoryEnum.VpGuarantorUserAction;
		public override string[] EventRuleTags => new[] { JournalEventTags.AppAccess };
		public override string EventRuleName => "GuarantorRequestedANewPersonalPassword";
		public override JournalEventTypeEnum AuditLogId => JournalEventTypeEnum.GuarantorRequestedANewPersonalPassword;
		public override string Description => "Generated when the VPG requested a new Password";
		public override bool TriggeredByEventUserId => true;
		public override bool AppliedToHsGId => true;
		public override bool AppliedToVpGId => true;
		public override bool AppliedToVpUserIdClientUserId => true;
		public override bool AppliedToFpId => false;
		public override bool AppliedToVisitId => false;
		public override bool AppliedToTransId => false;
		public override bool AppliedToPaymentId => false;
		public override string[] AdditionalInfo => new string[] { "SubmittedUserName", "LastName", "DateOfBirth", "SSN4", "FoundMatchFlag" };
		public override bool Audit => false;
		public override string HumanReadableDescription => "VP User [VpUserName] requested a new password";
	}
}
namespace Ivh.Common.EventJournal.Templates.Guarantor
{
	using System.Dynamic;
	using Base.Enums;
	using EventJournal;
	using Constants;
	using VisitPay.Enums;


    public class EventGuarantorRequestedUserName : JournalEventTemplate<JournalEventParameters>
	{
		public static JournalEventParameters GetParameters( string dateOfBirth, string foundMatchFlag, string sSN4, string submittedLastName, string vpUserName)
		{
			//Property names are case sensitive and must match AdditionalInfo and HumanReadableDescription [tags] exactly
			dynamic parameters = new ExpandoObject();
			parameters.DateOfBirth = dateOfBirth;
			parameters.FoundMatchFlag = foundMatchFlag;
			parameters.SSN4 = sSN4;
			parameters.SubmittedLastName = submittedLastName;
			parameters.VpUserName = vpUserName;
			return new JournalEventParameters(parameters);
		}

		public override JournalEventCategoryEnum EventRuleTrigger => JournalEventCategoryEnum.VpGuarantorUserAction;
		public override string[] EventRuleTags => new[] { JournalEventTags.AppAccess };
		public override string EventRuleName => "GuarantorRequestedUserName";
		public override JournalEventTypeEnum AuditLogId => JournalEventTypeEnum.GuarantorRequestedUserName;
		public override string Description => "Generated when the VPG requested a their user name";
		public override bool TriggeredByEventUserId => true;
		public override bool AppliedToHsGId => true;
		public override bool AppliedToVpGId => true;
		public override bool AppliedToVpUserIdClientUserId => true;
		public override bool AppliedToFpId => false;
		public override bool AppliedToVisitId => false;
		public override bool AppliedToTransId => false;
		public override bool AppliedToPaymentId => false;
		public override string[] AdditionalInfo => new string[] { "SubmittedLastName", "DateOfBirth", "SSN4", "FoundMatchFlag" };
		public override bool Audit => false;
		public override string HumanReadableDescription => "VP User [VpUserName] requested their user name";
	}
}
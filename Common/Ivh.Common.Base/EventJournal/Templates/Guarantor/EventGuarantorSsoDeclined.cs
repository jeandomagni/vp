namespace Ivh.Common.EventJournal.Templates.Guarantor
{
	using System.Dynamic;
	using Base.Enums;
	using EventJournal;
	using Constants;
	using VisitPay.Enums;


    public class EventGuarantorSsoDeclined : JournalEventTemplate<JournalEventParameters>
	{
		public static JournalEventParameters GetParameters( string dateOfBirth, string ssoProviderId, string ssoProviderName, string ssoSystemSourceKey, string vpUserName)
		{
			//Property names are case sensitive and must match AdditionalInfo and HumanReadableDescription [tags] exactly
			dynamic parameters = new ExpandoObject();
			parameters.DateOfBirth = dateOfBirth;
			parameters.SsoProviderId = ssoProviderId;
			parameters.SsoProviderName = ssoProviderName;
			parameters.SsoSystemSourceKey = ssoSystemSourceKey;
			parameters.VpUserName = vpUserName;
			return new JournalEventParameters(parameters);
		}

		public override JournalEventCategoryEnum EventRuleTrigger => JournalEventCategoryEnum.VpGuarantorUserAction;
		public override string[] EventRuleTags => new[] { JournalEventTags.Sso };
		public override string EventRuleName => "GuarantorSsoDeclined";
		public override JournalEventTypeEnum AuditLogId => JournalEventTypeEnum.GuarantorSsoDeclined;
		public override string Description => "Generated after a user declines the SSO terms during setup. ";
		public override bool TriggeredByEventUserId => true;
		public override bool AppliedToHsGId => true;
		public override bool AppliedToVpGId => true;
		public override bool AppliedToVpUserIdClientUserId => true;
		public override bool AppliedToFpId => false;
		public override bool AppliedToVisitId => false;
		public override bool AppliedToTransId => false;
		public override bool AppliedToPaymentId => false;
		public override string[] AdditionalInfo => new string[] { "SsoProviderId", "DateOfBirth", "SsoSystemSourceKey" };
		public override bool Audit => false;
		public override string HumanReadableDescription => "VP User [VpUserName] declined Sso terms with [SsoProviderName]";
	}
}
namespace Ivh.Common.EventJournal.Templates.Guarantor
{
	using System.Dynamic;
	using Base.Enums;
	using EventJournal;
	using Constants;
	using VisitPay.Enums;


    public class EventGuarantorConsolidationRequestAccepted : JournalEventTemplate<JournalEventParameters>
	{
		public static JournalEventParameters GetParameters( string requestingUserName, string requestingVpGuarantorId, string vpUserName)
		{
			//Property names are case sensitive and must match AdditionalInfo and HumanReadableDescription [tags] exactly
			dynamic parameters = new ExpandoObject();
			parameters.RequestingUserName = requestingUserName;
			parameters.RequestingVpGuarantorId = requestingVpGuarantorId;
			parameters.VpUserName = vpUserName;
			return new JournalEventParameters(parameters);
		}

		public override JournalEventCategoryEnum EventRuleTrigger => JournalEventCategoryEnum.VpGuarantorUserAction;
		public override string[] EventRuleTags => new[] { JournalEventTags.Consolidation };
		public override string EventRuleName => "GuarantorConsolidationRequestAccepted";
		public override JournalEventTypeEnum AuditLogId => JournalEventTypeEnum.GuarantorConsolidationRequestAccepted;
		public override string Description => "Generated when the VPG User accepts a Consolidation Request VPNG-11222";
		public override bool TriggeredByEventUserId => true;
		public override bool AppliedToHsGId => true;
		public override bool AppliedToVpGId => true;
		public override bool AppliedToVpUserIdClientUserId => true;
		public override bool AppliedToFpId => false;
		public override bool AppliedToVisitId => false;
		public override bool AppliedToTransId => false;
		public override bool AppliedToPaymentId => false;
		public override string[] AdditionalInfo => new string[] { "RequestingVpGuarantorId" };
		public override bool Audit => false;
		public override string HumanReadableDescription => "VP User [VpUserName] accepted the request to consolidation with VP User [RequestingUserName]";
	}
}
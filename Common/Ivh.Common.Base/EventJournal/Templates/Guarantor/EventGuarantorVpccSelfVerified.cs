﻿namespace Ivh.Common.EventJournal.Templates.Guarantor
{
    using System.Dynamic;
    using Constants;
    using VisitPay.Constants;
    using VisitPay.Enums;

    public class EventGuarantorVpccSelfVerified : JournalEventTemplate<JournalEventParameters>
    {
        public static JournalEventParameters GetParameters(string vpUserName)
        {
            //Property names are case sensitive and must match AdditionalInfo and HumanReadableDescription [tags] exactly
            dynamic parameters = new ExpandoObject();
            parameters.VpUserName = vpUserName;
            return new JournalEventParameters(parameters);
        }

        public override JournalEventCategoryEnum EventRuleTrigger => JournalEventCategoryEnum.VpGuarantorUserAction;
        public override string[] EventRuleTags => new[] { JournalEventTags.AppAccess };
        public override string EventRuleName => "GuarantorVpccSelfVerified";
        public override JournalEventTypeEnum AuditLogId => JournalEventTypeEnum.GuarantorVpccSelfVerified;
        public override string Description => $"Generated when a user logs into VPCC (VPCC Patient portal), having checked '{TextRegionConstants.GuarantorSelfVerification}'";
        public override bool TriggeredByEventUserId => true;
        public override bool AppliedToHsGId => true;
        public override bool AppliedToVpGId => true;
        public override bool AppliedToVpUserIdClientUserId => false;
        public override bool AppliedToFpId => false;
        public override bool AppliedToVisitId => false;
        public override bool AppliedToTransId => false;
        public override bool AppliedToPaymentId => false;
        public override string[] AdditionalInfo => new string[] { };
        public override bool Audit => false;
        public override string HumanReadableDescription => "VPCC User [VpUserName] verified their identity";
    }
}
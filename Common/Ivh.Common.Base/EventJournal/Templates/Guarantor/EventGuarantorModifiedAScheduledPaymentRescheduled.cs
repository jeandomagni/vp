namespace Ivh.Common.EventJournal.Templates.Guarantor
{
	using System.Dynamic;
	using Base.Enums;
	using EventJournal;
	using Constants;
	using VisitPay.Enums;


    public class EventGuarantorModifiedAScheduledPaymentRescheduled : JournalEventTemplate<JournalEventParameters>
	{
		public static JournalEventParameters GetParameters( string changeReason, string newScheduledDate, string paymentMethod, string previousScheduledDate, string scheduledAmount, string vpUserName)
		{
			//Property names are case sensitive and must match AdditionalInfo and HumanReadableDescription [tags] exactly
			dynamic parameters = new ExpandoObject();
			parameters.ChangeReason = changeReason;
			parameters.NewScheduledDate = newScheduledDate;
			parameters.PaymentMethod = paymentMethod;
			parameters.PreviousScheduledDate = previousScheduledDate;
			parameters.ScheduledAmount = scheduledAmount;
			parameters.VpUserName = vpUserName;
			return new JournalEventParameters(parameters);
		}

		public override JournalEventCategoryEnum EventRuleTrigger => JournalEventCategoryEnum.VpGuarantorUserAction;
		public override string[] EventRuleTags => new[] { JournalEventTags.Payments };
		public override string EventRuleName => "GuarantorModifiedAScheduledPaymentRescheduled";
		public override JournalEventTypeEnum AuditLogId => JournalEventTypeEnum.GuarantorModifiedAScheduledPaymentRescheduled;
		public override string Description => "Generated when a VPG changes the scheduled date on a scheduled, manual payment";
		public override bool TriggeredByEventUserId => true;
		public override bool AppliedToHsGId => true;
		public override bool AppliedToVpGId => true;
		public override bool AppliedToVpUserIdClientUserId => true;
		public override bool AppliedToFpId => false;
		public override bool AppliedToVisitId => false;
		public override bool AppliedToTransId => false;
		public override bool AppliedToPaymentId => true;
		public override string[] AdditionalInfo => new string[] { "NewScheduledDate", "PaymentMethod", "ScheduledAmount", "ChangeReason" };
		public override bool Audit => false;
		public override string HumanReadableDescription => "VP User [VpUserName] changed the scheduled date on the payment of [ScheduledAmount] from [PreviousScheduledDate] to [NewScheduledDate]";
	}
}
namespace Ivh.Common.EventJournal.Templates.Admin
{
	using System.Dynamic;
	using Base.Enums;
	using EventJournal;
	using Constants;
	using VisitPay.Enums;

    public class EventAdminVpTransactionUpdate : JournalEventTemplate<JournalEventParameters>
	{
		public static JournalEventParameters GetParameters( string amountOfAdjustment, string eventUserName, string notes)
		{
			//Property names are case sensitive and must match AdditionalInfo and HumanReadableDescription [tags] exactly
			dynamic parameters = new ExpandoObject();
			parameters.AmountOfAdjustment = amountOfAdjustment;
			parameters.EventUserName = eventUserName;
			parameters.Notes = notes;
			return new JournalEventParameters(parameters);
		}

		public override JournalEventCategoryEnum EventRuleTrigger => JournalEventCategoryEnum.VpAdminAction;
		public override string[] EventRuleTags => new[] { JournalEventTags.Transactions };
		public override string EventRuleName => "AdminVpTransactionUpdate";
		public override JournalEventTypeEnum AuditLogId => JournalEventTypeEnum.AdminVpTransactionUpdate;
		public override string Description => "Generated when a transactional level fix is made via the console VPNG-10849";
		public override bool TriggeredByEventUserId => true;
		public override bool AppliedToHsGId => true;
		public override bool AppliedToVpGId => true;
		public override bool AppliedToVpUserIdClientUserId => true;
		public override bool AppliedToFpId => false;
		public override bool AppliedToVisitId => true;
		public override bool AppliedToTransId => true;
		public override bool AppliedToPaymentId => false;
		public override string[] AdditionalInfo => new string[] { "AmountOfAdjustment", "Notes" };
		public override bool Audit => false;
		public override string HumanReadableDescription => "Admin User [EventUserName] applied a transactional fix";
	}
}
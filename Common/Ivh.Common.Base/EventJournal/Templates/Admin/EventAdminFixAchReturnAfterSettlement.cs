namespace Ivh.Common.EventJournal.Templates.Admin
{
	using System.Dynamic;
	using Base.Enums;
	using EventJournal;
	using Constants;
	using VisitPay.Enums;

    public class EventAdminFixAchReturnAfterSettlement : JournalEventTemplate<JournalEventParameters>
	{
		public static JournalEventParameters GetParameters( string eventUserName)
		{
			//Property names are case sensitive and must match AdditionalInfo and HumanReadableDescription [tags] exactly
			dynamic parameters = new ExpandoObject();
			parameters.EventUserName = eventUserName;
			return new JournalEventParameters(parameters);
		}

		public override JournalEventCategoryEnum EventRuleTrigger => JournalEventCategoryEnum.VpAdminAction;
		public override string[] EventRuleTags => new[] { JournalEventTags.Exceptions };
		public override string EventRuleName => "AdminFixAchReturnAfterSettlement";
		public override JournalEventTypeEnum AuditLogId => JournalEventTypeEnum.AdminFixAchReturnAfterSettlement;
		public override string Description => "Generated when a ACH Return Fix is implemented";
		public override bool TriggeredByEventUserId => true;
		public override bool AppliedToHsGId => true;
		public override bool AppliedToVpGId => true;
		public override bool AppliedToVpUserIdClientUserId => false;
		public override bool AppliedToFpId => false;
		public override bool AppliedToVisitId => false;
		public override bool AppliedToTransId => true;
		public override bool AppliedToPaymentId => true;
		public override string[] AdditionalInfo => new string[] {};
		public override bool Audit => false;
		public override string HumanReadableDescription => "Admin User [EventUserName] applied an ACH Return after Settlement Fix";
	}
}
﻿namespace Ivh.Common.EventJournal
{
    using System;
    using System.Collections.Generic;
    using System.Dynamic;
    using System.Text.RegularExpressions;
    using Base.Enums;
    using Base.Logging;
    using Base.Utilities.Extensions;
    using Exceptions;
    using Interfaces;
    using Newtonsoft.Json;
    using VisitPay.Enums;

    public abstract class JournalEventTemplate : IJournalEventRule
    {

        public abstract JournalEventCategoryEnum EventRuleTrigger { get; }
        public abstract string[] EventRuleTags { get; }
        public abstract string EventRuleName { get; }
        public abstract JournalEventTypeEnum AuditLogId { get; }
        public abstract string Description { get; }
        public abstract bool TriggeredByEventUserId { get; }
        public abstract bool AppliedToHsGId { get; }
        public abstract bool AppliedToVpGId { get; }
        public abstract bool AppliedToVpUserIdClientUserId { get; }
        public abstract bool AppliedToFpId { get; }
        public abstract bool AppliedToVisitId { get; }
        public abstract bool AppliedToTransId { get; }
        public abstract bool AppliedToPaymentId { get; }
        public abstract string[] AdditionalInfo { get; }
        public abstract bool Audit { get; }
        public abstract string HumanReadableDescription { get; }

        public JournalEventContext JournalEventContext { get; set; }
        public JournalEventUserContext JournalEventUserContext { get; set; }
        public JournalEventTypeEnum JournalEventType => this.AuditLogId;
        public JournalEventCategoryEnum JournalEventCategory => this.EventRuleTrigger;
        public string EventTags => this.EventRuleTags.IsNotNullOrEmpty() ? string.Join(",", this.EventRuleTags) : null;

        protected JournalEventTemplate()
        {
        }

        public virtual string BuildMessage()
        {
            return null;
        }

        public virtual dynamic BuildAdditionalData()
        {
            return null;
        }

        public string SerializeAdditionalData(dynamic additionalData)
        {
            return additionalData != null ? JsonConvert.SerializeObject(additionalData) : null;
        }

        public JournalEvent SerializeJournalEventUserContext(JournalEvent journalEvent)
        {
            journalEvent.JournalUserEventContext = this.JournalEventUserContext != null ? JsonConvert.SerializeObject(this.JournalEventUserContext) : null;
            return journalEvent;
        }

        public JournalEvent SerializeJournalEventContext(JournalEvent journalEvent)
        {
            journalEvent.JournalEventContext = this.JournalEventContext != null ? JsonConvert.SerializeObject(this.JournalEventContext) : null;
            return journalEvent;
        }

        public JournalEvent BuildJournalEvent()
        {
            JournalEvent journalEvent = this.CreateEvent();
            journalEvent.Message = this.BuildMessage();
            journalEvent.AdditionalData = this.SerializeAdditionalData(this.BuildAdditionalData());
            return journalEvent;
        }

        public JournalEvent CreateEvent()
        {
            JournalEvent journalEvent = new JournalEvent
            {
                EventDateTime = DateTime.UtcNow,
                JournalEventType = this.JournalEventType,
                JournalEventCategory = this.JournalEventCategory,
                EventTags = this.EventTags,
                CorrelationId = CorrelationManager.CorrelationId
            };

            journalEvent = this.SerializeJournalEventContext(journalEvent);
            journalEvent = this.SerializeJournalEventUserContext(journalEvent);
            journalEvent = this.SupplementUserEventDetails(journalEvent);
            return journalEvent;
        }

        public JournalEvent SupplementUserEventDetails(JournalEvent journalEvent)
        {
            if (this.JournalEventUserContext != null)
            {
                journalEvent.EventVisitPayUserId = this.JournalEventUserContext.EventVisitPayUserId;
                journalEvent.UserHostAddress = this.JournalEventUserContext.UserHostAddress;
                journalEvent.DeviceType = this.JournalEventUserContext.DeviceType;
                journalEvent.UserAgentString = this.JournalEventUserContext.UserAgent;
            }
            if (this.JournalEventContext != null)
            {
                journalEvent.VisitPayUserId = this.JournalEventContext.VisitPayUserId;
                journalEvent.VpGuarantorId = this.JournalEventContext.VpGuarantorId;
                journalEvent.HsGuarantorId = this.JournalEventContext.HsGuarantorId;
                journalEvent.VisitId = this.JournalEventContext.VisitId;
                journalEvent.VisitTransactionId = this.JournalEventContext.VisitTransactionId;
                journalEvent.PaymentId = this.JournalEventContext.PaymentId;
                journalEvent.FinancePlanId = this.JournalEventContext.FinancePlanId;
            }

            if (!journalEvent.VisitPayUserId.HasValue)
            {
                if (this.JournalEventCategory == JournalEventCategoryEnum.VpGuarantorUserAction)
                {
                    journalEvent.VisitPayUserId = journalEvent.EventVisitPayUserId;
                }
                else
                {
                    throw new JournalEventContextValueMissingException($"Journal Events with JournalEventCategoryEnum.{this.JournalEventCategory} must specify a value for {nameof(EventJournal.JournalEventContext)}.{nameof(EventJournal.JournalEventContext.VisitPayUserId)}.");
                }
            }

            return journalEvent;
        }
    }

    public abstract class JournalEventTemplate<T> : JournalEventTemplate
        where T : IJournalEventParameters
    {       
        public string BuildMessage(T journalEventParameter)
        {
            IDictionary<string, object> parameterDict = journalEventParameter.Parameters as IDictionary<string, object>;
            string message = this.HumanReadableDescription;
            MatchCollection matches = Regex.Matches(message, "\\[.*?\\]");
            foreach (Match match in matches)
            {
                string tag = match.Value;
                string propName = tag.Replace("[", "").Replace("]", "");
                object replacementValue = null;
                if (parameterDict?.TryGetValue(propName, out replacementValue) ?? false)
                {
                    message = message.Replace(tag, replacementValue?.ToString() ?? string.Empty);
                }
                else
                {
                    string parameterDictText = JsonConvert.SerializeObject(parameterDict);
                    throw new JournalEventTagMissingException($"JournalEvent::BuildMessage is missing a tag replacement value for \"{tag}\".\nCheck case and spelling.\n{this.HumanReadableDescription}\n{parameterDictText}");
                }
            }
            return message;
        }

        public dynamic BuildAdditionalData(T journalEventParameter)
        {
            IDictionary<string, object> parameterDict = journalEventParameter.Parameters as IDictionary<string, object>;
            dynamic additionalData = new ExpandoObject();
            IDictionary<string, object> additionalDataDict = additionalData as IDictionary<string, object>;
            foreach (string propName in this.AdditionalInfo)
            {
                object propValue = null;
                if (parameterDict.TryGetValue(propName, out propValue))
                {
                    additionalDataDict.Add(propName, propValue);
                }
                else
                {
                    string parameterDictText = JsonConvert.SerializeObject(parameterDict);
                    throw new JournalEventAdditionalInfoMissingException($"JournalEvent::BuildAdditionalData is missing a value for \"{propName}\".\nCheck case, spelling, and the assigned value.\n{string.Join(",",this.AdditionalInfo)}\nparameterDictText");
                }               
            }
            return additionalData;
        }

        public JournalEvent BuildJournalEvent(T journalEventParameter)
        {
            JournalEvent journalEvent = this.CreateEvent();
            journalEvent.Message = this.BuildMessage(journalEventParameter);
            journalEvent.AdditionalData = this.SerializeAdditionalData(this.BuildAdditionalData(journalEventParameter));
            return journalEvent;
        }
    }

}
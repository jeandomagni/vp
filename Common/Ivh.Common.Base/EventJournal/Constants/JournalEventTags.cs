﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ivh.Common.EventJournal.Constants
{
    public static class JournalEventTags
    {
        public const string AcctMngmnt = "Acct Mngmnt";
        public const string AppAccess = "App Access";
        public const string AuditLogs = "Audit Logs";
        public const string Chat = "Chat";
        public const string Communication = "Communication";
        public const string Consolidation = "Consolidation";
        public const string Downloads = "Downloads";
        public const string EventLog = "Event Log";
        public const string Exceptions = "Exceptions";
        public const string FinancePlans = "Finance Plans";
        public const string Guarantor = "Guarantor";
        public const string MatchUnmatch = "MatchUnmatch";
        public const string PaymentMethods = "Payment Methods";
        public const string Payments = "Payments";
        public const string PhiViews = "PHI Views";
        public const string Registration = "Registration";
        public const string Sms = "SMS";       
        public const string Sso = "SSO";
        public const string Transactions = "Transactions";
        public const string Visits = "Visits";
            
    }
}

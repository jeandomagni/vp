﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ivh.Common.EventJournal.Constants
{
    public static class JournalEventValue
    {
        public const string Unknown = "??";
        public const int UnknownUserId = -1;
    }
}

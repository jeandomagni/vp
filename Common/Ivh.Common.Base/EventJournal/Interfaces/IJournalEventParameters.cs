﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ivh.Common.EventJournal.Interfaces
{
    public interface IJournalEventParameters
    {
        dynamic Parameters { get; set; }
    }
}

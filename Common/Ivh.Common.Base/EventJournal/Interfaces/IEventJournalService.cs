﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ivh.Common.EventJournal.Interfaces
{
    public interface IEventJournalService
    {
        JournalEvent AddEvent<TTemplate>(
            JournalEventUserContext userEventDetails = null,
            JournalEventContext journalEventContext = null) where TTemplate : JournalEventTemplate, new();

        JournalEvent AddEvent<TTemplate, T1>(
            T1 journalEventParameter,
            JournalEventUserContext userEventDetails = null,
            JournalEventContext journalEventContext = null)
            where TTemplate : JournalEventTemplate<T1>, new()
            where T1 : IJournalEventParameters;

        JournalEvent AddEvent<TTemplate>(JournalEventContext journalEventContext = null) where TTemplate : SystemJournalTemplate, new();

        JournalEvent AddEvent<TTemplate, T1>(T1 entity, JournalEventContext journalEventContext = null)
            where TTemplate : SystemJournalTemplate<T1>, new()
            where T1 : IJournalEventParameters;

    }
}

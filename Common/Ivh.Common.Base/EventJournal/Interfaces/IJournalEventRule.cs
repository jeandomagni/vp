﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ivh.Common.EventJournal.Interfaces
{
    using Base.Enums;
    using VisitPay.Enums;

    public interface IJournalEventRule
    {
        JournalEventCategoryEnum EventRuleTrigger { get; }
        string[] EventRuleTags { get; }
        string EventRuleName { get; }
        JournalEventTypeEnum AuditLogId { get; }
        string Description { get; }
        bool TriggeredByEventUserId { get; }
        bool AppliedToHsGId { get; }
        bool AppliedToVpGId { get; }
        bool AppliedToVpUserIdClientUserId { get; }
        bool AppliedToFpId { get; }
        bool AppliedToVisitId { get; }
        bool AppliedToTransId { get; }
        bool AppliedToPaymentId { get; }
        string[] AdditionalInfo { get; }
        bool Audit { get; }
        string HumanReadableDescription { get; }
    }
}

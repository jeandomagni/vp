﻿namespace Ivh.Common.EventJournal
{
    using System.Web;
    using Base.Utilities.Helpers;

    public class JournalEventContext
    {
        public virtual int? VisitPayUserId { get; set; }
        public virtual int? VpGuarantorId { get; set; }
        public virtual int? HsGuarantorId { get; set; }
        public virtual int? VisitId { get; set; }
        public virtual int? VisitTransactionId { get; set; }
        public virtual int? PaymentId { get; set; }
        public virtual int? FinancePlanId { get; set; }

        public static JournalEventContext BuildEventContext(
            int? visitPayUserId = null,
            int? vpGuarantorId = null,
            int? hsGuarantorId = null,
            int? visitId = null,
            int? visitTransactionId = null,
            int? paymentId = null,
            int? financePlanId = null)
        {
            return new JournalEventContext()
            {
                VisitPayUserId = visitPayUserId,
                VpGuarantorId = vpGuarantorId,
                HsGuarantorId = hsGuarantorId,
                VisitId = visitId,
                VisitTransactionId = visitTransactionId,
                PaymentId = paymentId,
                FinancePlanId = financePlanId,
            };
        }
    }
}

﻿namespace Ivh.Common.EventJournal
{
    using System.Web;
    using Base.Constants;
    using Base.Enums;
    using Base.Utilities.Helpers;
    using VisitPay.Constants;
    using VisitPay.Enums;

    public class JournalEventUserContext
    {
        public int EventVisitPayUserId
        {
            get;
            set;
        }

        public virtual JournalEventHttpContextDto JournalEventHttpContextDto { get; set; }

        public string UserName { get; set; }
        public bool IsIvinciUser { get; set; }
        public bool IsClientUser { get; set; }
        public bool IsSystemUser { get; set; }
        public string Email { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }

        public virtual DeviceTypeEnum? DeviceType => this.JournalEventHttpContextDto?.DeviceType;
        public virtual string UserAgent => this.JournalEventHttpContextDto?.UserAgent;
        public virtual string UserHostAddress => this.JournalEventHttpContextDto?.UserHostAddress;
        public virtual string Url => this.JournalEventHttpContextDto?.Url;

        public static JournalEventUserContext BuildSystemUserContext(JournalEventHttpContextDto journalEventHttpContextDto)
        {
            return new JournalEventUserContext()
            {
                EventVisitPayUserId = SystemUsers.SystemUserId,
                JournalEventHttpContextDto = journalEventHttpContextDto
            };
        }

        internal static JournalEventUserContext BuildSystemContext()
        {
            return new JournalEventUserContext()
            {
                EventVisitPayUserId = SystemUsers.SystemUserId,
            };
        }
    }
}

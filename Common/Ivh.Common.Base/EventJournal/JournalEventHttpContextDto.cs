﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ivh.Common.EventJournal
{
    using Base.Enums;
    using VisitPay.Enums;

    public class JournalEventHttpContextDto
    {
        public string Url { get; set; }
        public string UserHostAddress { get; set; }
        public string UserAgent { get; set; }
        public DeviceTypeEnum? DeviceType { get; set; }
    }
}

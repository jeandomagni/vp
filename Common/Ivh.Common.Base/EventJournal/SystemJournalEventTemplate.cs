﻿namespace Ivh.Common.EventJournal
{
    using Interfaces;

    public abstract class SystemJournalTemplate : JournalEventTemplate
    {
        protected SystemJournalTemplate()
        {
            this.JournalEventUserContext = JournalEventUserContext.BuildSystemContext();
        }
    }

    public abstract class SystemJournalTemplate<T> : JournalEventTemplate<T> where T : IJournalEventParameters
    {
        protected SystemJournalTemplate()
        {
            this.JournalEventUserContext = JournalEventUserContext.BuildSystemContext();
        }
    }

    //public abstract class SystemJournalTemplate<T> : JournalEventTemplate<T> where T : class
    //{
    //    protected SystemJournalTemplate()
    //    {
    //        this.JournalEventUserContext = JournalEventUserContext.BuildSystemContext();
    //    }
    //}

    //public abstract class SystemJournalTemplate<T1, T2> : JournalEventTemplate<T1, T2>
    //    where T1 : class
    //    where T2 : class
    //{
    //    protected SystemJournalTemplate()
    //    {
    //        this.JournalEventUserContext = JournalEventUserContext.BuildSystemContext();
    //    }
    //}

    //public abstract class SystemJournalTemplate<T1, T2, T3> : JournalEventTemplate<T1, T2, T3>
    //    where T1 : class
    //    where T2 : class
    //    where T3 : class
    //{
    //    protected SystemJournalTemplate()
    //    {
    //        this.JournalEventUserContext = JournalEventUserContext.BuildSystemContext();
    //    }
    //}

    //public abstract class SystemJournalTemplate<T1, T2, T3, T4> : JournalEventTemplate<T1, T2, T3, T4>
    //    where T1 : class
    //    where T2 : class
    //    where T3 : class
    //    where T4 : class
    //{
    //    protected SystemJournalTemplate()
    //    {
    //        this.JournalEventUserContext = JournalEventUserContext.BuildSystemContext();
    //    }
    //}
}

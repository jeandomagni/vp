﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ivh.Common.EventJournal
{
    using Interfaces;

    public class JournalEventParameters : IJournalEventParameters
    {
        public dynamic Parameters { get; set; }

        public JournalEventParameters(dynamic parameters)
        {
            this.Parameters = parameters;
        }        
    }
}

﻿namespace Ivh.Common.Cache
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Runtime.InteropServices;
    using System.Text;

    public class IvinciHash
    {
        public static uint Hash(byte[] data)
        {
            return Hash(data, 0xc58f1a7b);
        }

        private const uint M = 0x5bd1e995;
        private const int R = 24;

        [StructLayout(LayoutKind.Explicit)]
        private struct BytetoUInt32Converter
        {
            [FieldOffset(0)] public byte[] Bytes;

            [FieldOffset(0)] public readonly uint[] UInts;
        }

        public static uint Hash(byte[] data, uint seed)
        {
            int length = data.Length;
            if (length == 0)
                return 0;
            uint h = seed ^ (uint) length;
            int currentIndex = 0;
            // array will be length of Bytes but contains Uints
            // therefore the currentIndex will jump with +1 while length will jump with +4
            uint[] hackArray = new BytetoUInt32Converter {Bytes = data}.UInts;
            while (length >= 4)
            {
                uint k = hackArray[currentIndex++];
                k *= M;
                k ^= k >> R;
                k *= M;

                h *= M;
                h ^= k;
                length -= 4;
            }
            currentIndex *= 4; // fix the length
            switch (length)
            {
                case 3:
                    h ^= (ushort) (data[currentIndex++] | data[currentIndex++] << 8);
                    h ^= (uint) data[currentIndex] << 16;
                    h *= M;
                    break;
                case 2:
                    h ^= (ushort) (data[currentIndex++] | data[currentIndex] << 8);
                    h *= M;
                    break;
                case 1:
                    h ^= data[currentIndex];
                    h *= M;
                    break;
                default:
                    break;
            }

            // Do a few final mixes of the hash to ensure the last few
            // bytes are well-incorporated.

            h ^= h >> 13;
            h *= M;
            h ^= h >> 15;

            return h;
        }
    }

    internal class ConsistentHash<T>
    {
        private readonly SortedDictionary<int, T> _circle = new SortedDictionary<int, T>();
        private int _replicate = 100; //default _replicate count
        private int[] _ayKeys; //cache the ordered keys for better performance

        //it's better you override the GetHashCode() of T.
        //we will use GetHashCode() to identify different node.
        public void Init(IEnumerable<T> nodes)
        {
            this.Init(nodes, this._replicate);
        }

        public void Init(IEnumerable<T> nodes, int replicate)
        {
            this._replicate = replicate;

            foreach (T node in nodes)
            {
                this.Add(node, false);
            }
            this._ayKeys = this._circle.Keys.ToArray();
        }

        public void Add(T node)
        {
            this.Add(node, true);
        }

        private void Add(T node, bool updateKeyArray)
        {
            for (int i = 0; i < this._replicate; i++)
            {
                int hash = BetterHash(node.GetHashCode().ToString() + i);
                this._circle[hash] = node;
            }

            if (updateKeyArray)
            {
                this._ayKeys = this._circle.Keys.ToArray();
            }
        }

        public void Remove(T node)
        {
            for (int i = 0; i < this._replicate; i++)
            {
                int hash = BetterHash(node.GetHashCode().ToString() + i);
                if (!this._circle.Remove(hash))
                {
                    throw new Exception("can not remove a node that not added");
                }
            }
            this._ayKeys = this._circle.Keys.ToArray();
        }

        //we keep this function just for performance compare
        private T GetNode_slow(string key)
        {
            int hash = BetterHash(key);
            if (this._circle.ContainsKey(hash))
            {
                return this._circle[hash];
            }

            int first = this._circle.Keys.FirstOrDefault(h => h >= hash);
            if (first == new int())
            {
                first = this._ayKeys[0];
            }
            T node = this._circle[first];
            return node;
        }

        //return the index of first item that >= val.
        //if not exist, return 0;
        //ay should be ordered array.
        private int First_ge(int[] ay, int val)
        {
            int begin = 0;
            int end = ay.Length - 1;

            if (ay[end] < val || ay[0] > val)
            {
                return 0;
            }

            int mid = begin;
            while (end - begin > 1)
            {
                mid = (end + begin)/2;
                if (ay[mid] >= val)
                {
                    end = mid;
                }
                else
                {
                    begin = mid;
                }
            }

            if (ay[begin] > val || ay[end] < val)
            {
                throw new Exception("should not happen");
            }

            return end;
        }

        public T GetNode(string key)
        {
            //return GetNode_slow(key);

            int hash = BetterHash(key);

            int first = this.First_ge(this._ayKeys, hash);

            //int diff = circle.Keys[first] - hash;

            return this._circle[this._ayKeys[first]];
        }

        //default String.GetHashCode() can't well spread strings like "1", "2", "3"
        public static int BetterHash(string key)
        {
            uint hash = IvinciHash.Hash(Encoding.ASCII.GetBytes(key));
            return (int) hash;
        }
    }
}
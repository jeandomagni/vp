﻿namespace Ivh.Common.Cache
{
    using System;
    using System.Configuration;
    using System.IO;
    using System.Text;
    using System.Threading;
    using System.Threading.Tasks;
    using Base.Enums;
    using Configuration;
    using Encryption;
    using ProtoBuf;
    using StackExchange.Redis;

    public class RedisCache : IDistributedCache
    {
        /// <summary>
        ///     This should not be directly referenced.  Use the this.GetInstance(string) to access.
        /// </summary>
        private static volatile ConnectionMultiplexer[] _cacheMultiplexer;

        private static ConsistentHash<int> _cacheHash;
        private static readonly object SyncRoot = new object();

        private static readonly string MachineGuid = new Guid().ToString();
        private readonly string _authKeystring;
        private readonly int _cacheInstances;
        private readonly string _encryptionKey;
        private readonly string _distributedCacheAuthKeyString;
        private readonly string _client;

        private const string LockPrefix = "Lock:";
        private const int LockMaxRetry = 200;
        private static readonly TimeSpan LockExpiry = TimeSpan.FromSeconds(60);
        private static readonly TimeSpan LockRetryWait = TimeSpan.FromMilliseconds(10);

        //Default constructor
        public RedisCache(
            string encryptionKey,
            string authKeystring,
            int cacheInstances,
            string distributedCacheAuthKeyString,
            string client)
        {
            this._encryptionKey = encryptionKey;
            this._authKeystring = authKeystring;
            this._cacheInstances = cacheInstances;
            this._distributedCacheAuthKeyString = distributedCacheAuthKeyString;
            this._client = client;
        }

        // https://github.com/StackExchange/StackExchange.Redis/issues/144
        // "It is expected that the lock key does not exist prior to this operation. The lock key should be distinct from from the key of the data."
        public bool GetLock(string key)
        {
            string keyLock = this.GetLockKey(key);
            
            IDatabase db = this.GetInstance(keyLock).GetDatabase();
            
            int count = 0;
            while (count < LockMaxRetry)
            {
                if (db.LockTake(keyLock, MachineGuid, LockExpiry))
                {
                    return true;
                }

                Thread.Sleep(LockRetryWait);
                count++;
            }

            return false;
        }

        public bool GetLock(string key, int maxRetryCount, int retryWait)
        {
            string keyLock = this.GetLockKey(key);

            IDatabase db = this.GetInstance(keyLock).GetDatabase();

            int count = 0;
            while (count < maxRetryCount)
            {
                if (db.LockTake(keyLock, MachineGuid, LockExpiry))
                {
                    return true;
                }

                Thread.Sleep(retryWait);
                count++;
            }

            return false;
        }

        public bool ReleaseLock(string key)
        {
            string keyLock = this.GetLockKey(key);

            IDatabase db = this.GetInstance(keyLock).GetDatabase();
            return db.LockRelease(keyLock, MachineGuid);
        }

        /// <summary>
        ///     This uses serialization, if the key is only a string use GetString.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="key"></param>
        /// <returns></returns>
        public T GetObject<T>(string key)
        {
            IDatabase db = this.GetInstance(key).GetDatabase();
            Task<RedisValue> task = db.StringGetAsync(key);

            RedisValue value = db.Wait(task);

            if (value.HasValue)
            {
                return this.Deserialize<T>(AESThenHMAC.SimpleDecrypt(value, this._encryptionKey, this._authKeystring, this._distributedCacheAuthKeyString.Length));
            }

            return default(T);
        }

        /// <summary>
        ///     This uses serialization, if the key is only a string use GetString.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="key"></param>
        /// <returns></returns>
        public async Task<T> GetObjectAsync<T>(string key)
        {
            IDatabase db = this.GetInstance(key).GetDatabase();
            RedisValue value = await db.StringGetAsync(key).ConfigureAwait(true);
            if (value.HasValue)
            {
                return this.Deserialize<T>(AESThenHMAC.SimpleDecrypt(value, this._encryptionKey, this._authKeystring, this._distributedCacheAuthKeyString.Length));
            }

            return default(T);
        }

        public bool SetObject(string key, object obj, int expiration = 3600)
        {
            IDatabase db = this.GetInstance(key).GetDatabase();
            string serializedString = this.SerializedString(obj);

            Task<bool> task = db.StringSetAsync(key, AESThenHMAC.SimpleEncrypt(serializedString, this._encryptionKey, this._authKeystring, Encoding.UTF8.GetBytes(this._distributedCacheAuthKeyString)), new TimeSpan(0, 0, 0, expiration));
            db.Wait(task);
            return task.Result;
        }

        public async Task<bool> SetObjectAsync(string key, object obj, int expiration = 3600)
        {
            IDatabase db = this.GetInstance(key).GetDatabase();
            string serializedString = this.SerializedString(obj);
            return await db.StringSetAsync(key, AESThenHMAC.SimpleEncrypt(serializedString, this._encryptionKey, this._authKeystring, Encoding.UTF8.GetBytes(this._distributedCacheAuthKeyString)), new TimeSpan(0, 0, 0, expiration)).ConfigureAwait(true);
        }

        /// <summary>
        ///     This only returns basic datatypes.  Use GetObject(T) if you need serialization.
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public string GetString(string key)
        {
            IDatabase db = this.GetInstance(key).GetDatabase();
            Task<RedisValue> task = db.StringGetAsync(key);

            RedisValue value = db.Wait(task);

            return value.HasValue ? AESThenHMAC.SimpleDecrypt(value, this._encryptionKey, this._authKeystring, this._distributedCacheAuthKeyString.Length) : null;
        }

        public async Task<string> GetStringAsync(string key)
        {
            IDatabase db = this.GetInstance(key).GetDatabase();
            RedisValue value = await db.StringGetAsync(key).ConfigureAwait(true);

            return value.HasValue ? AESThenHMAC.SimpleDecrypt(value, this._encryptionKey, this._authKeystring, this._distributedCacheAuthKeyString.Length) : null;
        }

        public bool SetString(string key, string obj, int expiration = 3600)
        {
            IDatabase db = this.GetInstance(key).GetDatabase();
            Task<bool> task = db.StringSetAsync(key, AESThenHMAC.SimpleEncrypt(obj, this._encryptionKey, this._authKeystring, Encoding.UTF8.GetBytes(this._distributedCacheAuthKeyString)), new TimeSpan(0, 0, 0, expiration));
            db.Wait(task);
            return task.Result;
        }

        public async Task<bool> SetStringAsync(string key, string obj, int expiration = 3600)
        {
            IDatabase db = this.GetInstance(key).GetDatabase();
            return await db.StringSetAsync(key, AESThenHMAC.SimpleEncrypt(obj, this._encryptionKey, this._authKeystring, Encoding.UTF8.GetBytes(this._distributedCacheAuthKeyString)), new TimeSpan(0, 0, 0, expiration));
        }

        public bool Remove(string key)
        {
            IDatabase db = this.GetInstance(key).GetDatabase();
            Task<bool> task = db.KeyDeleteAsync(key);
            db.Wait(task);
            return task.Result;
        }

        public async Task<bool> RemoveAsync(string key)
        {
            IDatabase db = this.GetInstance(key).GetDatabase();
            return await db.KeyDeleteAsync(key);
        }

        public static byte[] ReadFully(Stream input)
        {
            using (MemoryStream ms = new MemoryStream())
            {
                input.CopyTo(ms);
                return ms.ToArray();
            }
        }
        
        public void PublishMessage(string channel, string message)
        {
            ConnectionMultiplexer instance = this.GetInstance(channel);
            ISubscriber subscriber = instance.GetSubscriber();
            subscriber.Publish(channel, message, CommandFlags.FireAndForget);
        }

        public void Subscribe(string channel, Action<string> action)
        {
            ConnectionMultiplexer instance = this.GetInstance(channel);
            ISubscriber subscriber = instance.GetSubscriber();
            subscriber.Subscribe(channel, (c, m) =>
            {
                action(m);
            }, CommandFlags.FireAndForget);
        }

        private string SerializedString(object obj)
        {
            string serializedString;
            using (MemoryStream myMemoryStream = new MemoryStream())
            {
                //Did the code throw here?
                //If so:  Add '[DataContract]' to the class
                //and '[DataMember(Order = 1)]' to the members that should be serialized.
                //It's important to increment the order number.
                Serializer.Serialize(myMemoryStream, obj);
                myMemoryStream.Position = 0;
                byte[] byteData = ReadFully(myMemoryStream);
                serializedString = Convert.ToBase64String(byteData);
            }
            return serializedString;
        }

        public T Deserialize<T>(string value)
        {
            return this.Deserialize<T>((RedisValue)value);
        }

        private T Deserialize<T>(RedisValue value)
        {
            if (!value.HasValue)
            {
                return default(T);
            }

            return Serializer.Deserialize<T>(new MemoryStream(Convert.FromBase64String(value)));
        }

        private ConnectionMultiplexer GetInstance(string key)
        {
            if (_cacheMultiplexer == null)
            {
                lock (SyncRoot)
                {
                    if (_cacheMultiplexer == null)
                    {
                        ConnectionMultiplexer[] connectionMultis = new ConnectionMultiplexer[this._cacheInstances];
                        int[] instances = new int[this._cacheInstances];
                        for (int i = 0; i < this._cacheInstances; i++)
                        {
                            string name = ConnectionStringsProvider.Instance.GetConnectionString($"RedisCache{i}");

                            connectionMultis[i] = ConnectionMultiplexer.Connect(name);
                            instances[i] = i;
                        }
                        ConsistentHash<int> hash = new ConsistentHash<int>();
                        hash.Init(instances);
                        _cacheHash = hash;
                        _cacheMultiplexer = connectionMultis;
                    }
                }
            }

            key = this._client + "::" + key;

            return _cacheMultiplexer[_cacheHash.GetNode(key)];
        }

        private string GetLockKey(string key)
        {
            return $"{LockPrefix}:{key}";
        }
        
        /* Per IDistributedCache: This isnt helpful because you cant await in a finally block */
        /*
        public async Task<bool> GetLockAsync(string key)
        {
            string keyLock = this.GetLockKey(key);
            
            IDatabase db = this.GetInstance(keyLock).GetDatabase();
            
            int count = 0;
            while (count < LockMaxRetry)
            {
                if (await db.LockTakeAsync(keyLock, MachineGuid, LockExpiry))
                {
                    return true;
                }

                Thread.Sleep(LockRetryWait);
                count++;
            }

            return false;
        }
        
        public async Task<bool> ReleaseLockAsync(string key)
        {
            string keyLock = this.GetLockKey(key);

            IDatabase db = this.GetInstance(keyLock).GetDatabase();
            return await db.LockReleaseAsync(keyLock, MachineGuid);
        }
        */
    }
}
 
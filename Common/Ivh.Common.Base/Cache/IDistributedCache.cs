﻿namespace Ivh.Common.Cache
{
    using System;

    /// <summary>
    /// This is an empty interface so that it's easy to pick between distributed cache and memory
    /// </summary>
    public interface IDistributedCache : ICache
    {
        bool GetLock(string key);

        bool GetLock(string key, int retries, int delay);

        //This isnt helpful because you cant await in a finally block
        //Task<bool> GetLockAsync(string key);
        //Task<bool> ReleaseLockAsync(string key);
        bool ReleaseLock(string key);

        void PublishMessage(string channel, string message);

        void Subscribe(string channel, Action<string> func);

        T Deserialize<T>(string value);
    }
}
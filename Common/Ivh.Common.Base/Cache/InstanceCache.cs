﻿namespace Ivh.Common.Cache
{
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;

    public class InstanceCache : IInstanceCache
    {
        private readonly Lazy<IDictionary<int, object>> _instanceCache = new Lazy<IDictionary<int, object>>(() => new Dictionary<int, object>());
        public TValue CacheValue<TValue>(Func<int> callerParameterValuesHash, Func<TValue> valueFactory, [CallerMemberName] string callerMemberName = "")
        {
            int key = callerMemberName.GetHashCode();
            unchecked
            {
                key += callerParameterValuesHash();
            }

            if (this._instanceCache.Value.ContainsKey(key))
            {
                return (TValue)this._instanceCache.Value[key];
            }
            else
            {
                TValue value = valueFactory();
                this._instanceCache.Value[key] = value;
                return value;
            }
        }
    }
}

﻿namespace Ivh.Common.Cache
{
    using System;
    using System.IO;
    using System.Runtime.Caching;
    using System.Threading.Tasks;
    using ProtoBuf;

    public class IvinciMemoryCache : IMemoryCache
    {
        protected static readonly ObjectCache Cache = MemoryCache.Default;
        public Task<T> GetObjectAsync<T>(string key)
        {
            return Task.Run(() => this.GetObject<T>(key));
        }

        public T GetObject<T>(string key)
        {
            byte[] checkBytes =(byte[]) Cache.Get(key);
            if (checkBytes != null)
            {
                T outObject;
                using (MemoryStream myMemoryStream = new MemoryStream(checkBytes))
                {
                    outObject = Serializer.Deserialize<T>(myMemoryStream);
                }
                return outObject;
            }
            return default(T);
        }

        public Task<string> GetStringAsync(string key)
        {
            return Task.Run(() => this.GetString(key));
        }

        protected CacheItemPolicy GetCacheItemPolicy(int expiration)
        {
            CacheItemPolicy policy = new CacheItemPolicy {AbsoluteExpiration = DateTime.UtcNow.AddSeconds(expiration)};
            return policy;
        }

        public string GetString(string key)
        {
            return this.GetObject<string>(key);
        }

        public Task<bool> SetStringAsync(string key, string obj, int expiration = 3600)
        {
            return Task.Run(() =>
            {
                this.SetString(key, obj, expiration);
                return true;
            });
        }

        public bool SetString(string key, string obj, int expiration = 3600)
        {
            Cache.Set(new CacheItem(key, this.SerializedObject(obj)), this.GetCacheItemPolicy(expiration));
            return true;
        }

        public Task<bool> SetObjectAsync(string key, object obj, int expiration = 3600)
        {
            return Task.Run(() =>
            {
                this.SetObject(key, obj, expiration);
                return true;
            });
        }

        public bool SetObject(string key, object obj, int expiration = 3600)
        {
            Cache.Set(new CacheItem(key, this.SerializedObject(obj)), this.GetCacheItemPolicy(expiration));
            return true;
        }

        public bool Remove(string key)
        {
            Cache.Remove(key);
            return true;
        }

        public Task<bool> RemoveAsync(string key)
        {
            return Task.Run(() =>
            {
                this.Remove(key);
                return true;
            });
        }

        private byte[] SerializedObject(object obj)
        {
            byte[] byteData;
            using (MemoryStream myMemoryStream = new MemoryStream())
            {
                //Did the code throw here?
                //If so:  Add '[ProtoContract]' to the class
                //and '[ProtoMember(n)]' to the members that should be serialized.
                //It's important to increment the order number.
                Serializer.Serialize(myMemoryStream, obj);
                byteData = myMemoryStream.ToArray();
            }
            return byteData;
        }
    }
}
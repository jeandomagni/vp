﻿namespace Ivh.Common.Cache
{
    using System;
    using System.Runtime.CompilerServices;

    public interface IInstanceCache
    {
        TValue CacheValue<TValue>(Func<int> callerParameterValuesHash, Func<TValue> valueFactory, [CallerMemberName] string callerMemberName = "");
    }
}
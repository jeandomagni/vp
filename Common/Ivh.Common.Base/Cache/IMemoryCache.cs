﻿namespace Ivh.Common.Cache
{
    using System;

    /// <summary>
    /// This is an empty interface so that it's easy to pick between distributed cache and memory
    /// </summary>
    [Obsolete("Prefer using DistributedCache")]
    public interface IMemoryCache : ICache
    {
    }
}
﻿namespace Ivh.Common.Cache
{
    using System;
    using System.Threading.Tasks;

    public interface ICache
    {
        Task<T> GetObjectAsync<T>(string key);
        [Obsolete("Use GetObjectAsync")]
        T GetObject<T>(string key);
        Task<string> GetStringAsync(string key);
        [Obsolete("Use GetStringAsync")]
        string GetString(string key);
        Task<bool> SetStringAsync(string key, string obj, int expiration = 3600);
        [Obsolete("Use SetStringAsync")]
        bool SetString(string key, string obj, int expiration = 3600);
        Task<bool> SetObjectAsync(string key, object obj, int expiration = 3600);
        [Obsolete("Use SetObjectAsync")]
        bool SetObject(string key, object obj, int expiration = 3600);

        [Obsolete("Use RemoveAsync")]
        bool Remove(string key);
        Task<bool> RemoveAsync(string key);
    }
}
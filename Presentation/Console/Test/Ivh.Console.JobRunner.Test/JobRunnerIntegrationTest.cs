﻿//﻿namespace Ivh.Console.JobRunner.Test
//{
//    using Application.Monitoring.Common.Interfaces;
//    using Moq;
//    using NUnit.Framework;
//    using System;
//    using System.Threading.Tasks;
//    using Application.Logging.Common.Interfaces;
//    using Domain.Settings.Interfaces;
//    using Ivh.Provider.Logging.Database.Interfaces;

//    [TestFixture]
//    public class JobRunnerAcceptanceTest
//    {
//        private Lazy<ILoggingApplicationService> _lazyLogger;
//        private Lazy<IApplicationSettingsService> _lazySettingsService;
//        private Lazy<IMonitoringApplicationService> _lazyMonitoringService;
//        private Lazy<Application.Enterprise.Common.Monitoring.Interfaces.IMonitoringApplicationService> _lazyEnterpriseMonitoringService;
//        private TestJobRunner _jobRunner;

//        [SetUp]
//        public void InitializeTest()
//        {
//            this._lazyLogger = new Lazy<ILoggingApplicationService>(this.CreateLoggerService);
//            this._lazySettingsService = new Lazy<IApplicationSettingsService>(this.CreateApplicationSettingsService);
//            this._lazyMonitoringService = new Lazy<IMonitoringApplicationService>(this.CreateMonitoringService);
//            this._lazyEnterpriseMonitoringService = new Lazy<Application.Enterprise.Common.Monitoring.Interfaces.IMonitoringApplicationService>(this.CreateEnterpriseMonitoringService);

//            this._jobRunner = new TestJobRunner(this._lazyMonitoringService, this._lazyEnterpriseMonitoringService, this._lazyLogger);
//        }

//        [Test]
//        public async Task TestActual1Expected3()
//        {
//            await Task.Run(() =>
//            {
//                this._jobRunner.SetTestValues(5, 1);
//                this._jobRunner.Execute();

//                Assert.IsTrue(this._jobRunner.ResultsActualValue == this._jobRunner.ResultsExpectedValue);
//            });
//        }

//        private void MapperConfig()
//        {
//        }

//        #region Test Helpers

//        public IApplicationSettingsService CreateApplicationSettingsService()
//        {
//            IApplicationSettingsService mockSettingService = new Mock<IApplicationSettingsService>().Object;
//            return mockSettingService;
//        }

//        public IMonitoringApplicationService CreateMonitoringService()
//        {
//            IMonitoringApplicationService mockMonitoringService = new Mock<IMonitoringApplicationService>().Object;
//            return mockMonitoringService;
//        }

//        public Application.Enterprise.Common.Monitoring.Interfaces.IMonitoringApplicationService CreateEnterpriseMonitoringService()
//        {
//            Application.Enterprise.Common.Monitoring.Interfaces.IMonitoringApplicationService mockMonitoringService = new Mock<Application.Enterprise.Common.Monitoring.Interfaces.IMonitoringApplicationService>().Object;
//            return mockMonitoringService;
//        }
//        public ILoggingApplicationService CreateLoggerService()
//        {
//            ILoggingApplicationService mockLogger = new Mock<ILoggingApplicationService>().Object;
//            return mockLogger;
//        }
//        #endregion
//    }
//}
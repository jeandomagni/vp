﻿//namespace Ivh.Console.JobRunner.Test
//{
//    using System;
//    using Application.Logging.Common.Interfaces;
//    using Application.Monitoring.Common.Interfaces;
//    using Common.VisitPay.Enums;
//    using Common.VisitPay.Jobs.Attributes;

//    [JobRunner(JobRunnerTypeEnum.FileStorageImport)]
//    internal class TestJobRunner : IJob
//    {
//        public TestJobRunner(
//            Lazy<IMonitoringApplicationService> monitoringApplicationService,
//            Lazy<Application.Enterprise.Common.Monitoring.Interfaces.IMonitoringApplicationService> enterpriseMonitoringApplicationService,
//            Lazy<ILoggingApplicationService> logger)
//            : base(monitoringApplicationService, enterpriseMonitoringApplicationService, logger)
//        {
//        }

//        public void SetTestValues(decimal? expected, decimal? actual)
//        {
//            this.ResultsActualValue = actual;
//            this.ResultsExpectedValue = expected;
//        }

//        protected override void ExecuteAction(DateTime startDateTime, DateTime endDateTime, string[] args)
//        {
//            // do some simulated work
//        }

//        /// <summary>
//        /// Return expected
//        /// </summary>
//        /// <returns></returns>
//        protected override decimal? ResultsExpectedValueFunc()
//        {
//            return this.ResultsExpectedValue;
//        }

//        /// <summary>
//        /// Count the number of files to be processed that end up in the archive Folder after processing
//        /// and return as the Actual count.
//        /// </summary>
//        /// <returns></returns>
//        protected override decimal? ResultsActualValueFunc()
//        {
//            // custom implementation to simulate actual results increasing with time
//            this.ResultsActualValue++;

//            return this.ResultsActualValue;
//        }

//        protected override bool ExitWhenTrue()
//        {
//            bool moreToDo = this.ResultsExpectedValue <= this.ResultsActualValue;
//            return moreToDo;
//        }

//    }
//}
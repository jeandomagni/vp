﻿namespace Ivh.Console.MigrationTool
{
    using System;
    using System.Configuration;
    using System.Text;
    using Autofac;
    using Common.DependencyInjection;
    using Common.Encryption;
    using Domain.Settings.Interfaces;

    internal class Program
    {
        private const string IVinciAuthString = "iVinciIsTheBest";
        private static byte[] IVinciAuthBytes { get { return Encoding.UTF8.GetBytes(IVinciAuthString); } }
        private static string CryptKeyProd { get { return IvinciContainer.Instance.Container().Resolve<IApplicationSettingsService>().RedisEncryptionMaster.Value; } }
        private static string AuthKeyProd { get { return IvinciContainer.Instance.Container().Resolve<IApplicationSettingsService>().RedisEncryptionAuth.Value; } }

        private static string CryptKeyOld { get { return IvinciContainer.Instance.Container().Resolve<IApplicationSettingsService>().RedisEncryptionMasterOld.Value; } }
        private static string AuthKeyOld { get { return IvinciContainer.Instance.Container().Resolve<IApplicationSettingsService>().RedisEncryptionAuthOld.Value; } }
        private static void Main(string[] args)
        {
            MigrationToolEnum migrationToolEnum = MigrationToolEnum.Unknown;
            //MappingVisitPayBase.ConfigureMappings();
            //MappingHospitalDataBase.ConfigureMappings();
            //ServiceBusMessageSerializationMappingBase.ConfigureMappings();
            //IvinciContainer.Instance.RegisterComponent(new RegisterVisitPayProcessorConsole());
            //IContainer container = IvinciContainer.Instance.Container();
            if (args.Length == 0 || args.Length > 2)
            {
                System.Console.WriteLine("Pre:  stringtoconvert is unencrypted and should be encrypted using the default(production) keys");
                System.Console.WriteLine("usage: Ivh.Console.MigrationTool.exe [stringtoconvert]");
                Console.WriteLine("");
                System.Console.WriteLine("Pre:  stringtoconvert is encrypted using old or development keys and should be encrypted using the default(production) keys");
                System.Console.WriteLine("usage: Ivh.Console.MigrationTool.exe [stringtoconvert] DecryptWithOldReEncryptNew");
                return;
            }

            if (args.Length > 1)
            {
                migrationToolEnum = Enum.TryParse(args[1], true, out migrationToolEnum) ? migrationToolEnum : MigrationToolEnum.Unknown;
            }
            string encrypted;
            if (migrationToolEnum == MigrationToolEnum.DecryptWithOldReEncryptNew)
            {
                //Test parameters.  Should be "test"  Both of these are the same:
                //"aVZpbmNpSXNUaGVCZXN01bRpytDYPpFERNNpH5PRZtGWw9xSReG/6LxZvWqVGVda0768hu58cJLxD5bUxv0DpuEGGa0WUhGOT4cg7bK6cA==" DecryptWithOldReEncryptNew
                //"aVZpbmNpSXNUaGVCZXN0gtecxJKVJL8hhlmeZBlDMkMgV2UN2jo+9m7Z+gDQbZ/+93BcLo5gHobzFqJ2OCZ9uz1TFkvcw0L7fIl+QidVlg==" DecryptWithOldReEncryptNew

                string decrypted = AESThenHMAC.SimpleDecrypt(args[0], CryptKeyOld, AuthKeyOld, IVinciAuthBytes.Length);
                encrypted = AESThenHMAC.SimpleEncrypt(decrypted, CryptKeyProd, AuthKeyProd, IVinciAuthBytes);                
            }
            else 
            {
                encrypted = AESThenHMAC.SimpleEncrypt(args[0], CryptKeyProd, AuthKeyProd, IVinciAuthBytes);
            }
            Console.WriteLine(encrypted);
        }
    }

    public enum MigrationToolEnum
    {
        Unknown,
        DecryptWithOldReEncryptNew,
    }
}
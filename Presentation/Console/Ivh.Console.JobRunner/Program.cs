﻿namespace Ivh.Console.JobRunner
{
    using System;
    using System.Threading.Tasks;
    using Autofac;
    using Common.DependencyInjection;
    using Common.DependencyInjection.Registration;
    using Common.JobRunner.Common.Interfaces;
    using Common.ServiceBus.Constants;
    using Common.VisitPay.Enums;
    using MassTransit;
    using SetupIoc;

    internal class Program
    {
        private static void Main(string[] args)
        {
            //This allows this to actually run more than once per machine if we're hosting many environments on a single machine.
            if (args.Length == 0)
            {
                PrintWelcomeMessage();
            }
            else
            {
                JobRunnerTypeEnum jobRunnerOption = JobRunnerTypeEnum.Unknown;
                jobRunnerOption = Enum.TryParse(args[0], true, out jobRunnerOption) ? jobRunnerOption : JobRunnerTypeEnum.Unknown;
                Console.WriteLine("Parsed: {0}", args[0]);
                ExecuteAction(jobRunnerOption, args);
            }
        }

        private static void ExecuteAction(JobRunnerTypeEnum jobRunnerType, string[] args)
        {
            if (jobRunnerType != JobRunnerTypeEnum.Unknown)
            {
                using (ILifetimeScope lifetimeScope = SetupIoc(jobRunnerType).BeginLifetimeScope())
                {


                    using (IJobRunner jobRunner = lifetimeScope.Resolve<IJobRunner>())
                    {
                        IBusControl busControl = lifetimeScope.Resolve<IBusControl>();
                        try
                        {
                            busControl.Start();
                            jobRunner.Execute(args);
                        }
                        catch (Exception ex)
                        {
                            Environment.ExitCode = ex.HResult != 0 ? ex.HResult : -1;
                            throw;
                        }
                        finally
                        {
                            busControl?.Stop(TimeSpan.FromSeconds(ServiceBus.StopTimeoutSeconds));
                        }
                    }
                }
            }
            else
            {
                PrintInvalidSelection();
            }
        }
        private static IContainer SetupIoc(JobRunnerTypeEnum jobRunnerType)
        {
            Task mappingTasks = Task.Run(() =>
            {
                ServiceBusMessageSerializationMappingBase.ConfigureMappings();
                using (MappingBuilder mappingBuilder = new MappingBuilder())
                {
                    mappingBuilder.WithBase();
                }
            });
            IvinciContainer.Instance.RegisterComponent(new RegisterJobRunnerConsole(jobRunnerType));
            mappingTasks.Wait();

            IContainer container = IvinciContainer.Instance.Container();
#if DEBUG
            RegisterJobRunnerConsole.DebugRegistrationHelper(container);
#endif
            return container;
        }

        private static void PrintInvalidSelection()
        {
            Console.WriteLine("That is not a valid option.  Please select enter a valid option.");
            Console.WriteLine("---");
            PrintWelcomeMessage();
        }

        private static void PrintWelcomeMessage()
        {
            Console.WriteLine("Ivh.Console.JobRunner --  A runner for all your needs!");
            Console.WriteLine("Ivh.Console.JobRunner.exe <option>");
            Console.WriteLine("Options:");
            foreach (JobRunnerTypeEnum jobRunnerOption in Enum.GetValues(typeof(JobRunnerTypeEnum)))
            {
                Console.WriteLine("{0}", jobRunnerOption);
            }
            Console.WriteLine("---");
            Console.WriteLine();
        }
    }
}
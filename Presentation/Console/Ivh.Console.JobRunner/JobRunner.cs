﻿namespace Ivh.Console.JobRunner
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.Linq;
    using Application.Logging.Common.Interfaces;
    using Application.Monitoring.Common.Interfaces;
    using Common.Base.Utilities.Extensions;
    using Common.Base.Utilities.Helpers;
    using Common.JobRunner.Common.Interfaces;
    using Common.VisitPay.Enums;
    using Common.VisitPay.Jobs.Attributes;
    using Common.VisitPay.Strings;
    using Common.VisitPay.Utilities.Extensions;
    using Domain.Monitoring.Entities;
    using Domain.Monitoring.Exceptions;
    using Domain.Monitoring.Interfaces;
    using IEnterpriseMonitoringApplicationService = Application.Enterprise.Common.Monitoring.Interfaces.IMonitoringApplicationService;

    public class JobRunner<TJob> : IJobRunner where TJob : IJob
    {

        // #define to turn on the new progressive timeout functionality for job runners
        //#define JOBPROGRESS

        private readonly TJob _job;
        private readonly IJobRunnerService<TJob> _jobRunnerService;
        private readonly IMonitoringApplicationService _monitoringApplicationService;
        private readonly IEnterpriseMonitoringApplicationService _enterpriseMonitoringApplicationService;
        protected readonly ILoggingApplicationService Logger;

        private readonly int _dateTimeBeginOffset;
        private readonly int _dateTimeEndOffset;
        private readonly TimeSpan _exitTimeout;
        private readonly TimeSpan _exitCheckInterval = TimeSpan.FromSeconds(15);

        private DateTime _dateTimeOffsetBegin;
        private DateTime _dateTimeOffsetEnd;
        private readonly string _jobName;
        protected readonly DatumEnum? ExpectedDatumEnum = DatumEnum.None;
        protected readonly DatumEnum? ActualDatumEnum = DatumEnum.None;

        public JobRunner(
            TJob job,
            IJobRunnerService<TJob> jobRunnerService,
            IMonitoringApplicationService monitoringApplicationService,
            IEnterpriseMonitoringApplicationService enterpriseMonitoringApplicationService,
            ILoggingApplicationService logger)
        {
            this._job = job;
            this._jobRunnerService = jobRunnerService;
            this._monitoringApplicationService = monitoringApplicationService;
            this._enterpriseMonitoringApplicationService = enterpriseMonitoringApplicationService;
            this.Logger = logger;
            this._exitTimeout = this.GetJobTimeoutDuration();
            JobRunnerAttribute jobRunnerAttribute;
            if (typeof(TJob).TryGetAttribute(out jobRunnerAttribute))
            {
                this._jobName = jobRunnerAttribute.JobName.IsNotNullOrEmpty() ? jobRunnerAttribute.JobName : this.GetType().Name;
                this._dateTimeBeginOffset = jobRunnerAttribute.DateTimeBeginOffset ?? 0;
                this._dateTimeEndOffset = jobRunnerAttribute.DateTimeEndOffset ?? 0;
                this._exitCheckInterval = TimeSpan.FromSeconds(jobRunnerAttribute.ExitCheckIntervalSeconds ?? 15);
                this._exitTimeout = TimeSpan.FromSeconds(jobRunnerAttribute.ExitTimeoutSeconds ?? (15 * 60));
            }
        }

        public void Execute(string[] args = null)
        {
            try
            {
                //todo: should this be UTC? probably.
                Func<int, int, DateTime> getDate = (argsIndex, offset) => args.TryParseDateArg(argsIndex) ?? DateTime.Today.AddMilliseconds(offset);

                this._dateTimeOffsetBegin = getDate(1, this._dateTimeBeginOffset);
                this._dateTimeOffsetEnd = getDate(2, this._dateTimeEndOffset);

                this.PublishGaugeSetsBefore(args);

                this.OnBeforeExecuteSuccess(args);
            }
            catch (Exception e)
            {
                this.LogMessage(() => $"{this._jobName}::Execute - failed for DateTime = {this._dateTimeOffsetBegin}, exception = {ExceptionHelper.AggregateExceptionToString(e)}");
                throw;
            }
        }

        protected virtual bool ExitWhenTrue()
        {
            return (!this.ExpectedDatumEnum.HasValue || !this.ActualDatumEnum.HasValue || this.ExpectedDatumEnum == DatumEnum.None || this.ActualDatumEnum == DatumEnum.None)//cannot compare
                    || (
                            (this.ResultsExpectedValue.HasValue && this.ResultsActualValue.HasValue) &&
                            (this.ResultsExpectedValue.Value <= this.ResultsActualValue.Value)
                        );

        }

        private bool ExitWhenTrueInternal()
        {
            //get the latest value
            this.ResultsActualValue = this.ResultsActualValueFunc();

            bool exit = this.ExitWhenTrue();

            if (!exit)
            {
                this.LogMessage(() => $"{this._jobName}::Executing Actual Value - DateTime = {this._dateTimeOffsetBegin} - Expected Value = {this.ResultFormatter(this.ResultsExpectedValue)} - Actual Value = {this.ResultFormatter(this.ResultsActualValue)}");
            }
            return exit;
        }

        /// <summary>
        /// Evaluates the change between the current actual value and the previous actual value
        /// to determine if the job is still making progress towards meeting its expected value.
        /// Job progress may be made by either incrementing or decrementing the actual value.
        /// </summary>
        /// <returns>
        ///  true if there has been progress; false when the expected value has been met
        ///  (the anticipated work is presumed to be complete)
        /// </returns>
        protected virtual bool JobProgressInternal()
        {
            bool progress = this.ResultsActualValue != this.ResultsPreviousValue;

            this.ResultsPreviousValue = this.ResultsActualValue;
            this.ResultsActualValue = this._job.ResultsActualValue() ?? this.ResultsActualValueFunc();

            return progress;
        }


        public decimal? ResultsExpectedValue { get; set; }
        private decimal? ResultsExpectedValueFunc()
        {
            if (this.ExpectedDatumEnum.HasValue && this.ExpectedDatumEnum != DatumEnum.None)
            {
                return this.GetDatum<decimal?>(this.ExpectedDatumEnum.Value);
            }
            return default(decimal?);
        }

        public decimal? ResultsPreviousValue { get; set; }
        public decimal? ResultsActualValue { get; set; }

        private decimal? ResultsActualValueFunc()
        {
            if (this.ActualDatumEnum.HasValue && this.ActualDatumEnum != DatumEnum.None)
            {
                return this.GetDatum<decimal?>(this.ActualDatumEnum.Value);
            }
            return default(decimal?);
        }

        protected virtual void OnBeforeExecuteSuccess(string[] args)
        {
            this.ResultsExpectedValue = this._job.ResultsExpectedValue() ?? this.ResultsExpectedValueFunc();
            this.LogMessage(() => $"{this._jobName}::Execute Start - DateTime = {this._dateTimeOffsetBegin} - Expected Value = {this.ResultFormatter(this.ResultsExpectedValue)}");
            this._jobRunnerService.Execute(this._dateTimeOffsetBegin, this._dateTimeOffsetEnd, args);

            this.PublishGaugeSetsAfter(args);

            this.OnAfterExecuteSuccess();
        }

        protected virtual void OnAfterExecuteSuccess()
        {
            // TODO: remove the conditional #define after thorough testing when ready to migrate to the progressive timeout code
#if JOBPROGRESS
            if (ConditionalWaiter.Wait(this.ExitWhenTrueInternal, this.JobProgressInternal, this._exitTimeout, this._exitCheckInterval))
#else
            if (ConditionalWaiter.Wait(this.ExitWhenTrueInternal, this._exitTimeout, this._exitCheckInterval))
#endif
            {
                this.LogMessage(() => $"{this._jobName}::Execute Stop - DateTime = {this._dateTimeOffsetBegin} - Expected Value = {this.ResultFormatter(this.ResultsExpectedValue)} - Actual Value = {this.ResultFormatter(this.ResultsActualValue)}");
                if (this.ResultsExpectedValue.HasValue && this.ResultsActualValue.HasValue && (this.ResultsExpectedValue.Value != this.ResultsActualValue.Value))
                {
                    this.LogWarning(() => $"{this._jobName}::Expected <> Actual - DateTime = {this._dateTimeOffsetBegin} - Expected Value = {this.ResultFormatter(this.ResultsExpectedValue)} - Actual Value = {this.ResultFormatter(this.ResultsActualValue)}");
                }
            }
            else
            {
                this.LogWarning(() => $"{this._jobName}::Timeout - DateTime = {this._dateTimeOffsetBegin} - Expected Value = {this.ResultFormatter(this.ResultsExpectedValue)} - Actual Value = {this.ResultFormatter(this.ResultsActualValue)}");
                throw new TimeoutException();
            }
        }

        private void LogMessage(Func<string> message, params object[] parameters)
        {
            this.Logger.Info(message, parameters);
            Console.Out.WriteLine(message(), parameters);
        }

        private void LogWarning(Func<string> message, params object[] parameters)
        {
            this.Logger.Warn(message, parameters);
            Console.Out.WriteLine(message(), parameters);
        }

        protected T GetDatum<T>(DatumEnum datumEnum)
        {
            return this._monitoringApplicationService.GetDatum<T>(datumEnum, this._dateTimeOffsetBegin, this._dateTimeOffsetEnd, true);
        }

        private void PublishGaugeSets(string gaugeSetName)
        {
            this._enterpriseMonitoringApplicationService.PublishGaugeSets(new[] { gaugeSetName });
        }
        private void PublishGaugeSetsBefore(string[] args)
        {
            if (args.IsNotNullOrEmpty())
            {
                string jobname = args[0];
                this.PublishGaugeSets($"{jobname}Before");
            }
        }
        private void PublishGaugeSetsAfter(string[] args)
        {
            if (args.IsNotNullOrEmpty())
            {
                string jobname = args[0];
                this.PublishGaugeSets($"{jobname}After");
            }
        }

        private string MonitorInformation(IEnumerable<IMonitor> monitors)
        {
            return string.Join("", monitors.Select(x => x.Information));
        }

        private string MonitorNames(IEnumerable<IMonitor> monitors)
        {
            return string.Join(";", monitors.Select(x => x.Name));
        }

        private string ResultFormatter(decimal? value)
        {
            return value.HasValue ? value.Value.ToString() : "unknown";
        }

        private TimeSpan GetJobTimeoutDuration()
        {
            return TimeSpan.FromSeconds(15 * 60);//SECONDS
        }

        private void UpdateJobTimeoutDurationStatistics()
        {
            //TODO: IMPLEMENT
        }

        public void Dispose()
        {
            this.UpdateJobTimeoutDurationStatistics();
        }
    }
}
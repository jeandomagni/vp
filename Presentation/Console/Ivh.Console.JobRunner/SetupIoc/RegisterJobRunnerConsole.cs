﻿namespace Ivh.Console.JobRunner.SetupIoc
{
    using System;
    using System.Collections.Generic;
    using System.Configuration;
    using System.Linq;
    using Application.Logging.Common.Interfaces;
    using Application.Monitoring.Common.Interfaces;
    using Autofac;
    using Autofac.Core;
    using Common.Base.Utilities.Extensions;
    using Common.Base.Utilities.Helpers;
    using Common.DependencyInjection;
    using Common.DependencyInjection.Registration;
    using Common.DependencyInjection.Registration.AutofacModules;
    using Common.DependencyInjection.Registration.Register;
    using Common.JobRunner.Common.Interfaces;
    using Common.VisitPay.Enums;
    using Common.VisitPay.Jobs.Attributes;
    using Common.VisitPay.Jobs.Utilities;
    using Domain.User.Entities;
    using Domain.User.Interfaces;
    using Domain.User.Services;
    using Microsoft.AspNet.Identity;
    using Provider.User;
    using IEnterpriseMonitoringApplicationService = Application.Enterprise.Common.Monitoring.Interfaces.IMonitoringApplicationService;

    public class RegisterJobRunnerConsole : IRegistrationModule
    {
        private readonly JobRunnerTypeEnum _jobRunnerType;

        public RegisterJobRunnerConsole(JobRunnerTypeEnum jobRunnerType)
        {
            this._jobRunnerType = jobRunnerType;
        }

        public void Register(ContainerBuilder builder)
        {
            RegistrationSettings registrationSettings = RegistrationSettingsService.GetRegistrationSettings();
            Base.Register(builder, registrationSettings);
            VisitPay.Register(builder, registrationSettings, true);
            Data.Register(builder, registrationSettings)
                .AddVisitPayDatabase()
                .AddCdiEtlDatabase()
                .AddGuestPayDatabase()
                .AddEnterpriseDatabase()
                .AddStorageDatabase();

            Type JobType = typeof(TypeReference).Assembly.GetTypes().FirstOrDefault(y => typeof(IJob).IsAssignableFrom(y)
                                                                                               && y.IsClass
                                                                                               && !y.IsAbstract
                                                                                               && y.TryGetAttribute(out JobRunnerAttribute jobRunnerAttribute)
                                                                                               && jobRunnerAttribute.JobRunnerType == this._jobRunnerType);

            if (JobType != null)
            {
                builder.Register(c =>
                {
                    Type jobRunnerServiceType = typeof(IJobRunnerService<>).MakeGenericType(JobType);
                    Type jobRunnerType = typeof(JobRunner<>).MakeGenericType(JobType);

                    IServiceWithType serviceWithType = c.ComponentRegistry.Registrations.SelectMany(x => x.Services.Where(y => y is IServiceWithType).Cast<IServiceWithType>().Where(z => jobRunnerServiceType.IsAssignableFrom(z.ServiceType))).FirstOrDefault();

                    if (serviceWithType != null)
                    {
                        IJob job = (IJob)Activator.CreateInstance(JobType, c);
                        IJobRunnerService jobRunnerService = c.Resolve(serviceWithType.ServiceType) as IJobRunnerService;
                        IMonitoringApplicationService monitoringApplicationService = c.Resolve<IMonitoringApplicationService>();
                        IEnterpriseMonitoringApplicationService enterpriseMonitoringApplicationService = c.Resolve<IEnterpriseMonitoringApplicationService>();
                        ILoggingApplicationService loggingApplicationService = c.Resolve<ILoggingApplicationService>();

                        return Activator.CreateInstance(jobRunnerType,
                            job,
                            jobRunnerService,
                            monitoringApplicationService,
                            enterpriseMonitoringApplicationService,
                            loggingApplicationService);
                    }
                    throw new InvalidOperationException($"No service found in the component registry for {jobRunnerServiceType.Name}");
                }).As<IJobRunner>().InstancePerLifetimeScope();
            }
            else
            {
                throw new ConfigurationErrorsException($"Job Runner of type {this._jobRunnerType.ToString()} cannot be registered.");
            }

            builder.RegisterType<Common.ServiceBus.Bus>().As<Common.ServiceBus.Interfaces.IBus>();
            // register VisitPayUserService and dependencies
            builder.RegisterType<VisitPayUserRepository<VisitPayUser>>().As<IVisitPayUserRepository>();
            builder.RegisterType<VisitPayUserRepository<VisitPayUser>>().As<IUserStore<VisitPayUser>>();
            builder.RegisterType<VisitPayUserRepository<VisitPayUser>>().As<IUserTokenProvider<VisitPayUser, string>>();
            builder.RegisterType<VisitPayUserRepository<VisitPayUser>>().As<IVisitPayUserRepository>();
            builder.RegisterType<VisitPayUserService>().AsSelf();
            //IAuthenticationManager
            builder.RegisterType<VisitPaySignInService>().AsSelf();
            builder.RegisterType<IdentityEmailService>().As<IIdentityEmailService>();
            builder.RegisterType<IdentitySmsService>().As<IIdentitySmsService>();
            builder.RegisterModule(new AgingModule());

        }

        public static void DebugRegistrationHelper(IContainer container)
        {
#if DEBUG
            IEnumerable<Type> jobRunnerTypes = typeof(TypeReference).Assembly.GetTypes().Where(y => typeof(IJob).IsAssignableFrom(y)
                                                                                         && y.IsClass
                                                                                         && !y.IsAbstract
                                                                                         && y.TryGetAttribute(out JobRunnerAttribute jobRunnerAttribute)).ToList();

            foreach (JobRunnerTypeEnum jobRunnerTypeEnum in EnumExt.GetAllItems<JobRunnerTypeEnum>().Where(x => x != JobRunnerTypeEnum.Unknown).OrderBy(x => x.ToString()))
            {

                Type jobType = jobRunnerTypes.FirstOrDefault(y => y.TryGetAttribute(out JobRunnerAttribute jobRunnerAttribute) && jobRunnerAttribute.JobRunnerType == jobRunnerTypeEnum);

                if (jobType != null)
                {
                    Type jobRunnerServiceType = typeof(IJobRunnerService<>).MakeGenericType(jobType);
                    Type jobRunnerType = typeof(JobRunner<>).MakeGenericType(jobType);
                    IServiceWithType serviceWithType = container.ComponentRegistry.Registrations.SelectMany(x => x.Services.Where(y => y is IServiceWithType).Cast<IServiceWithType>().Where(z => jobRunnerServiceType.IsAssignableFrom(z.ServiceType))).FirstOrDefault();

                    if (serviceWithType != null)
                    {
                        using (ConsoleColorKeeper.KeepForeground(ConsoleColor.Green))
                        {
                            Console.WriteLine($"{jobRunnerTypeEnum}\tIJobRunnerService<{jobType.Name}>.Execute()\t{serviceWithType.ServiceType.Name}");
                        }
                    }
                    else
                    {
                        using (ConsoleColorKeeper.KeepForeground(ConsoleColor.Red))
                        {
                            Console.WriteLine($"{jobRunnerTypeEnum}\t{jobType.Name}\t");
                        }
                    }
                }
                else
                {
                    using (ConsoleColorKeeper.KeepForeground(ConsoleColor.Red))
                    {
                        Console.WriteLine($"{jobRunnerTypeEnum}\t\t\t");
                    }
                }

            }

#endif
        }
    }
}
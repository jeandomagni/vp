﻿namespace Ivh.Console.JobRunner.Tests
{
    using System.Collections.Generic;
    using System.Reflection;
    using NUnit.Framework;

    [TestFixture]
    public class DependencyCheckerTest : Ivh.Common.Testing.DependencyCheckerTest
    {
        [Test]
        public void CheckDependencies()
        {
            IList<KeyValuePair<string, string>> ignoreList = new List<KeyValuePair<string, string>>
            {
                new KeyValuePair<string, string>("NSass.Wrapper.proxy", "NSass.Core")
            };
            base.CheckDependencies(Assembly.GetExecutingAssembly().GetName(), ignoreList);
        }
    }
}

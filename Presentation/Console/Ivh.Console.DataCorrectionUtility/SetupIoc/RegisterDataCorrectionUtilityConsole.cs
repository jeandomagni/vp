﻿namespace Ivh.Console.DataCorrectionUtility.SetupIoc
{
    using Application.DataCorrectionUtility.Common.Interfaces;
    using Application.DataCorrectionUtility.Services;
    using Autofac;
    using Common.DependencyInjection;
    using Common.DependencyInjection.Registration;
    using Common.DependencyInjection.Registration.Register;
    using Domain.User.Entities;
    using Domain.User.Interfaces;
    using Domain.User.Services;
    using Ivh.Domain.HospitalData.Eob.Interfaces;
    using Ivh.Domain.HospitalData.Eob.Services;
    using Microsoft.AspNet.Identity;
    using Provider.User;
    using Module = Application.DataCorrectionUtility.Modules.Module;

    public class RegisterDataCorrectionUtilityConsole : IRegistrationModule
    {
        public void Register(ContainerBuilder builder)
        {
            RegistrationSettings registrationSettings = RegistrationSettingsService.GetRegistrationSettings();
            Base.Register(builder, registrationSettings);
            VisitPay.Register(builder, registrationSettings, true);
            Data.Register(builder, registrationSettings)
                .AddVisitPayDatabase()
                .AddCdiEtlDatabase()
                .AddEnterpriseDatabase()
                .AddGuestPayDatabase()
                .AddStorageDatabase();

            builder.RegisterType<DataCorrectionApplicationService>().As<IDataCorrectionApplicationService>();
            builder.RegisterAssemblyModules(typeof(Module).Assembly);
            builder.RegisterType<VisitPayUserRepository<VisitPayUser>>().As<IVisitPayUserRepository>();
            builder.RegisterType<VisitPayUserRepository<VisitPayUser>>().As<IUserStore<VisitPayUser>>();
            builder.RegisterType<VisitPayUserRepository<VisitPayUser>>().As<IUserTokenProvider<VisitPayUser, string>>();
            builder.RegisterType<VisitPayUserRepository<VisitPayUser>>().As<IVisitPayUserRepository>();
            builder.RegisterType<VisitPayUserService>().AsSelf();
            builder.RegisterType<IdentityEmailService>().As<IIdentityEmailService>();
            builder.RegisterType<IdentitySmsService>().As<IIdentitySmsService>();
            builder.RegisterType<Eob835Service>().As<IEob835Service>();
        }
    }
}
﻿namespace Ivh.Console.DataCorrectionUtility
{
    using System;
    using System.Linq;
    using System.Threading.Tasks;
    using Application.DataCorrectionUtility.Common.Interfaces;
    using Autofac;
    using Common.Base.Constants;
    using Common.Base.Enums;
    using Common.Base.Utilities.Helpers;
    using Common.DependencyInjection;
    using Common.DependencyInjection.Registration;
    using Common.ServiceBus.Constants;
    using Common.VisitPay.Enums;
    using MassTransit;
    using SetupIoc;

    internal class Program
    {
        private static IContainer _iocContainer;

        private static void Main(string[] args)
        {
            DataCorrectionTypeEnum dataCorrectionType = DataCorrectionTypeEnum.Unknown;
            dataCorrectionType = Enum.TryParse(args[0], true, out dataCorrectionType) ? dataCorrectionType : DataCorrectionTypeEnum.Unknown;
            Console.WriteLine("Parsed: {0}", args[0]);
            if (dataCorrectionType == DataCorrectionTypeEnum.Unknown)
            {
                PrintWelcomeMessage();
            }
            else
            {
                _iocContainer = SetupIoc();
                IBusControl busControl =  _iocContainer.Resolve<IBusControl>();
                try
                {
                    busControl.Start();
                    IDataCorrectionApplicationService dataCorrectionApplicationService = _iocContainer.Resolve<IDataCorrectionApplicationService>();
                    dataCorrectionApplicationService.RunDataCorrection(dataCorrectionType, args.Skip(1));
                }
                catch (Exception ex)
                {
                    Environment.ExitCode = ex.HResult != 0 ? ex.HResult : -1;
                    throw;
                }
                finally
                {
                    busControl?.Stop(TimeSpan.FromSeconds(ServiceBus.StopTimeoutSeconds));
                }
            }
        }

        private static IContainer SetupIoc()
        {
            Task mappingTasks = Task.Run(() =>
            {
                ServiceBusMessageSerializationMappingBase.ConfigureMappings();
                using (MappingBuilder mappingBuilder = new MappingBuilder())
                {
                    mappingBuilder
                        .WithBase()
                        .WithProfile<Application.DataCorrectionUtility.Mappings.AutomapperProfile>();
                }
            });

            IvinciContainer.Instance.RegisterComponent(new RegisterDataCorrectionUtilityConsole());

            mappingTasks.Wait();

            return IvinciContainer.Instance.Container();
        }
        
        private static void PrintWelcomeMessage()
        {
            Console.WriteLine("Ivh.Console.Utility");
            Console.WriteLine("Ivh.Console.JobRunner.exe <option>");
            Console.WriteLine("Options:");
            foreach (DataCorrectionTypeEnum dataCorrectionTypeEnum in EnumHelper<DataCorrectionTypeEnum>.GetValues())
            {
                Console.WriteLine("{0}", dataCorrectionTypeEnum);
            }
            Console.WriteLine("---");
            Console.WriteLine();
        }
    }
}
﻿-- -9223372036854775808 + (rand()  * 18446744073709551615)
declare @count int = 5000, @start int = 1, @index int = 0 --change start to avoid collisions with previous IDs, if you wanted to create 1000, @start = 0, @count = 1000.  Then you want to create 1000 more, @start = 1000, @count = 1000
declare @guar int = -5


IF OBJECT_ID('tempdb..#tempIDs') IS NOT NULL DROP TABLE #tempIDs
create table #tempIDs (ID varchar(100))
set @index = @start
while (@index < (@count + @start))
begin
	insert into #tempids
	select cast(@index as varchar(100))
	set @index = @index + 1
END

	insert into base.HsGuarantor
	( [HsBillingSystemID]
		  ,[SourceSystemKey]
		  ,[FirstName]
		  ,[LastName]
		  ,[DOB]
		  ,[SSN]
		  ,[SSN4]
		  ,[AddressLine1]
		  ,[AddressLine2]
		  ,[City]
		  ,[StateProvince]
		  ,[PostalCode]
		  ,[Country]
		  ,[ChangeEventID])
	select [HsBillingSystemID]
		  ,'fake-' + cast(i.ID as varchar)
		  ,'test'
		  ,'test' + cast(i.id as varchar)
		  ,'1/1/1990'
		  ,'1111-11-1111'
		  ,'1111'
		  ,'1 main st'
		  ,null
		  ,'boise'
		  ,'ID'
		  ,'11111'
		  ,null
		  ,[ChangeEventID]
	from base.HsGuarantor g cross join #tempids i
	where HsGuarantorID = @guar

select min(HsGuarantorID), max(HsGuarantorID)
from base.HsGuarantor
where SourceSystemKey like 'fake%'

﻿namespace Ivh.Console.JunkDataTool
{
    using System;
    using System.Collections.Generic;
    using System.Collections.Specialized;
    using System.Diagnostics;
    using System.IO;
    using System.Linq;
    using System.Net;
    using System.Text;
    using Amib.Threading;
    using Newtonsoft.Json;

    internal class Program
    {
        private static int _numberOfGuarantors;
        private static int _numberOfVisits;
        private static int _numberOfTransactionsPerVisit;
        private static readonly string QaToolsUrl = "https://qatools-loadtest-stlukes.visitpaytest.com/JunkData";
        //private static readonly string QaToolsUrl = "http://localhost:1873/JunkData";
        //private static readonly string QaToolsUrl = "http://qatools-dev.visitpaytest.com/JunkData";

        private static void Main(string[] args)
        {
            CreateGuarantors();
        }

        private static void CreateGuarantors()
        {
            Console.WriteLine("Environment: {0} <press any key>", QaToolsUrl);
            Console.ReadKey();

            string line;

            Console.WriteLine("");
            Console.WriteLine("how many guarantors? <100>");
            line = Console.ReadLine();
            if (!int.TryParse(line, out _numberOfGuarantors))
            {
                _numberOfGuarantors = 1000;
            }

            Console.WriteLine("");
            Console.WriteLine("how many visits per guarantor? <7>");
            line = Console.ReadLine();
            if (!int.TryParse(line, out _numberOfVisits))
            {
                _numberOfVisits = 7; // from Jake
            }

            Console.WriteLine("");
            Console.WriteLine("how many transactions per visit? <23>");
            line = Console.ReadLine();
            if (!int.TryParse(line, out _numberOfTransactionsPerVisit))
            {
                _numberOfTransactionsPerVisit = 23; // from Jake
            }

            Console.WriteLine("");
            Console.WriteLine("{0} guarantors, {1} visits per guarantor, {2} transactions per visit", _numberOfGuarantors, _numberOfVisits, _numberOfTransactionsPerVisit);

            Console.WriteLine("");
            Console.WriteLine("create hs data? <press any key>");
            Console.ReadKey();
            Console.WriteLine("...working...");
            IList<int> hsGuarantorIds = CreateHsData();
            Console.WriteLine("{0} HsGuarantors created", hsGuarantorIds.Count);

            if (!hsGuarantorIds.Any())
            {
                return;
            }

            Console.WriteLine("");
            Console.WriteLine("create vp data? <press any key>");
            Console.ReadKey();
            Console.WriteLine("...working...");
            CreatePaymentMethod();
            CreateVpData(hsGuarantorIds);

            Console.WriteLine("");
            Console.WriteLine("queue change sets? <press any key>");
            Console.ReadKey();
            Console.WriteLine("...working...");
            QueueChangeSets();

            Console.WriteLine("");
            Console.WriteLine("change sets queued!");
            Console.WriteLine("");
            Console.WriteLine("you're done.  the change sets will take some time to process.");
            Console.ReadKey();

            Environment.Exit(0);
        }

        private static string GetResponse(string url, string parameters)
        {
            ServicePointManager.ServerCertificateValidationCallback += (sender, cert, chain, sslPolicyErrors) => true;
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
            
            HttpWebRequest request = (HttpWebRequest) WebRequest.Create(string.Format("{0}/{1}", QaToolsUrl, url));
            request.Timeout = 5*60*1000;
            request.Method = "POST";
            request.ContentType = "application/x-www-form-urlencoded";
            request.ContentLength = parameters.Length;

            using (Stream requestStream = request.GetRequestStream())
            {
                requestStream.Write(Encoding.ASCII.GetBytes(parameters), 0, parameters.Length);
            }

            HttpWebResponse response = (HttpWebResponse) request.GetResponse();
            Stream responseStream = response.GetResponseStream();

            return responseStream == null ? null : new StreamReader(responseStream).ReadToEnd();
        }

        private static IList<int> CreateHsData()
        {
            string response = GetResponse("CreateHsData", string.Format("numberOfGuarantors={0}", _numberOfGuarantors));

            if (response == null)
            {
                Console.WriteLine("create hs data returned 0 guarantors.");
                return new List<int>();
            }

            if (response.Contains("exception:"))
            {
                Console.WriteLine(response);
                return new List<int>();
            }

            return JsonConvert.DeserializeObject<IList<int>>(response);
        }

        private static void CreatePaymentMethod()
        {
            string response = GetResponse("CreatePaymentMethod", "");
        }

        private static void CreateVpData(IList<int> hsGuarantorIds)
        {
            int currentCmsVersionId = GetCurrentCmsVersionAgreement();

            //
            STPStartInfo startInfo = new STPStartInfo
            {
                MaxWorkerThreads = 1,
                MinWorkerThreads = 1
            };

            SmartThreadPool threadPool = new SmartThreadPool(startInfo)
            {
                Concurrency = 1
            };

            for (int i = 0; i < hsGuarantorIds.Count; i++)
            {
                int hsGuarantorId = hsGuarantorIds[i];
                threadPool.QueueWorkItem(new WorkItemCallback(SendGuarantorRequest), new object[] {hsGuarantorId, currentCmsVersionId, i});
            }

            threadPool.Start();
            threadPool.WaitForIdle();
        }

        private static object SendGuarantorRequest(object p)
        {
            object[] o = (object[]) p;
            object hsGuarantorId = o[0];
            object currentCmsVersionId = o[1];
            object i = o[2];

            string response = GetResponse("CreateVpData", string.Format("hsGuarantorId={0}&currentCmsVersionId={1}&numberOfVisits={2}&numberOfTxnPerVisit={3}", hsGuarantorId, currentCmsVersionId, _numberOfVisits, _numberOfTransactionsPerVisit));

            Console.WriteLine(i + " :: " + response);

            return hsGuarantorId;
        }

        private static int GetCurrentCmsVersionAgreement()
        {
            try
            {
                using (WebClient client = new WebClient())
                {
                    NameValueCollection values = new NameValueCollection();

                    byte[] response = client.UploadValues(string.Format("{0}/GetCurrentCmsVersionAgreement", QaToolsUrl), values);

                    string responseString = Encoding.Default.GetString(response);

                    return Convert.ToInt32(JsonConvert.DeserializeObject(responseString));
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                return 0;
            }
        }

        private static void QueueChangeSets()
        {
            try
            {
                using (WebClient client = new WebClient())
                {
                    NameValueCollection values = new NameValueCollection();

                    byte[] response = client.UploadValues(string.Format("{0}/QueueChangeSets", QaToolsUrl), values);

                    string responseString = Encoding.Default.GetString(response);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }
    }
}
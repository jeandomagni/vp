﻿namespace Ivh.Console.UniversalProcessor.Interfaces
{
    using Common.Base.Interfaces;

    public interface IUniversalProcessorWindowsService : ITopShelfService
    {
        void Start(bool startScheduler);
    }
}

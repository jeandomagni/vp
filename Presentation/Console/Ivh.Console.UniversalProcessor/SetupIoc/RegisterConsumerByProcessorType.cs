﻿namespace Ivh.Console.UniversalProcessor.SetupIoc
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using Application.Core.Common.Interfaces;
    using Autofac;
    using Common.DependencyInjection;
    using Common.DependencyInjection.Registration;
    using Common.DependencyInjection.Registration.AutofacModules;
    using Common.DependencyInjection.Registration.Register;
    using Common.VisitPay.Enums;
    using Domain.User.Entities;
    using Domain.User.Interfaces;
    using Domain.User.Services;
    using Interfaces;
    using MassTransit;
    using Microsoft.AspNet.Identity;
    using Provider.User;
    using Quartz;
    using Quartz.Impl;

    public class RegisterConsumerByProcessorType : IRegistrationModule
    {
        private const string NameSpace = "Ivh.Console.UniversalProcessor.Consumers";
        private readonly IList<string> _processorTypes;
        private IList<string> _universalConsumerAllowedQueues;

        public RegisterConsumerByProcessorType(IList<string> processorTypes, IList<string> universalConsumerAllowedQueues)
        {
            this._processorTypes = processorTypes;
            this._universalConsumerAllowedQueues = universalConsumerAllowedQueues;
        }

        public void Register(ContainerBuilder builder)
        {
            builder.RegisterAssemblyTypes(Assembly.GetExecutingAssembly())
                .Where(t => t.IsInNamespace(NameSpace) && t.IsAssignableTo<IConsumer>())
                .AsImplementedInterfaces();

            IEnumerable<Tuple<string,IList<Type>>> GetConsumersForProcessorTypes()
            {
                foreach (string processorType in this._processorTypes)
                {
                    IList<Type> consumerTypes = Assembly.GetExecutingAssembly().GetTypes().Where(t => t.IsInNamespace($"{NameSpace}.{processorType}") && t.IsAssignableTo<IConsumer>()).ToList();
                    yield return new Tuple<string, IList<Type>>($"VisitPay::Ivh.Console.{processorType}", consumerTypes);
                }
            }

            IList<Tuple<string,IList<Type>>> queueConsumerList = GetConsumersForProcessorTypes().ToList();

            RegistrationSettings registrationSettings = RegistrationSettingsService.GetRegistrationSettings();
            Base.Register(builder, registrationSettings);
            VisitPay.Register(builder, registrationSettings, true);
            Data.Register(builder, registrationSettings)
                .AddVisitPayDatabase()
                .AddCdiEtlDatabase()
                .AddEnterpriseDatabase()
                .AddGuestPayDatabase()
                .AddStorageDatabase();

            builder.RegisterType<UniversalProcessorWindowsService>().As<IUniversalProcessorWindowsService>();
            builder.Register(x => new StdSchedulerFactory().GetScheduler()).SingleInstance().As<IScheduler>();

            //register VisitPayUserService and dependencies
            builder.RegisterType<VisitPayUserRepository<VisitPayUser>>()
                .As<IVisitPayUserRepository>()
                .As<IUserStore<VisitPayUser>>()
                .As<IUserTokenProvider<VisitPayUser, string>>()
                .As<IVisitPayUserRepository>();

            builder.RegisterType<VisitPaySignInService>().AsSelf();
            builder.RegisterType<VisitPayUserService>().AsSelf();
            builder.RegisterType<IdentityEmailService>().As<IIdentityEmailService>();
            builder.RegisterType<IdentitySmsService>().As<IIdentitySmsService>();
            builder.RegisterModule(new AgingModule());

            builder.Register(c =>
                {
                    //need to capture context in this closure to ensure it is available to resolve consumers later
                    IComponentContext context = c.Resolve<IComponentContext>();

                    //dont register agingProcessor queues if it is disabled for this client
                    bool agingEnabled = c.Resolve<IFeatureApplicationService>().IsFeatureEnabled(VisitPayFeatureEnum.FeatureVisitPayAgingIsEnabled);
                    if (!agingEnabled)
                    {
                        this._universalConsumerAllowedQueues = this._universalConsumerAllowedQueues.Where(x => !x.Contains("AgingProcessor")).ToList();
                    }

                    IBusControl bus = ServiceBus.Register(builder, registrationSettings, queueConsumerList, this._universalConsumerAllowedQueues, context, 2);
#if DEBUG
                    //MessageConsumerValidator.Validate(IvinciContainer.Instance.Container());
#endif
                    return bus;
                }
            ).SingleInstance().As<IBusControl>().As<IBus>();
            builder.RegisterType<Common.ServiceBus.Bus>().As<Common.ServiceBus.Interfaces.IBus>();
        }
    }
}
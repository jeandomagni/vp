#----------[Init]----------
$ErrorActionPreference = "Stop"

$localDirectory = $OctopusParameters["OctopusOriginalPackageDirectoryPath"]
$localDirectory

$PowerShellModules = Get-ChildItem -Path "$localDirectory" | Where-Object { $_.Name -like 'DeployModule.psm1' }
foreach ($Module in $PowerShellModules) {
	Write-Host "Import Module $($Module.FullName)"
	Import-Module $Module.FullName -Force -Verbose:$False
}

#----------[Declarations]----------
$processorName = "Ivh.Console.UniversalProcessor"
$processorConfig = "$processorName.exe.config"
$processorDirectory = $OctopusParameters["EXE_Directory_UniversalProcessor"]
$clientName = $OctopusParameters["Client"]
$serviceName = "Ivh.Console.UniversalProcessor.All.$clientName"
$serviceName

#----------[Main]----------
Write-Host "Installing Service"
Install-TopShelfService -Exe_Directory_Processor $processorDirectory

Write-Host "Setting Recovery Options"
Set-Recovery -ServiceName $serviceName

Write-Host "Encrypting ConnectionStrings"
Protect-EncryptConfig -websiteDirectory $processorDirectory -sectionToEncrypt connectionStrings -configFile $processorConfig -otherFiles "ConnectionStrings.Config"
Write-Host "Encrypting AppSettings"
Protect-EncryptConfig -websiteDirectory $processorDirectory -sectionToEncrypt appSettings -configFile $processorConfig -otherFiles "AppSettings.Config"



#This is a dirty little hack as it wont let us set our retention policy to 0
try{
	$path = $OctopusParameters["Octopus.Tentacle.PreviousInstallation.OriginalInstalledPath"]
	Write-Host "Path to look at = $($path)\.."

	if($path){
		$path = $path + "\.."
		$items = Get-Childitem $path -include *.* -recurse 
		foreach ($item in $items) 
		{
    		Remove-Item $item.fullname -Force -Recurse -EA SilentlyContinue
    		#Write-Host $items.FullName
		}
	}
	else{
		Write-Host "Nothing to remove"
	}
}
Catch
{}
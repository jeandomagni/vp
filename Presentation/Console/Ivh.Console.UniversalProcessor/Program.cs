﻿namespace Ivh.Console.UniversalProcessor
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Autofac;
    using Common.Console;
    using Common.DependencyInjection;
    using Common.DependencyInjection.Registration;
    using Interfaces;
    using SetupIoc;

    class Program
    {
        static void Main(string[] args)
        {
            string priority = ConsoleSetup.GetAppSettingValue("Priority");

            string processorTypesSettingValue = ConsoleSetup.GetAppSettingValue("ProcessorTypeNames");
            IList<string> processorTypes = processorTypesSettingValue.Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries).Select(x => x.Trim()).ToList();

            string universalConsumerAllowedQueuesValue = ConsoleSetup.GetAppSettingValue("UniversalConsumerAllowedQueues");
            IList<string> universalConsumerAllowedQueues = universalConsumerAllowedQueuesValue.Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries).Select(x => x.Trim()).ToList();

            IContainer container = null;
            if (!args.Contains("install", StringComparer.InvariantCultureIgnoreCase)
                && !args.Contains("uninstall", StringComparer.InvariantCultureIgnoreCase)
                && !args.Contains("stop", StringComparer.InvariantCultureIgnoreCase)
            )
            {
                Task mappingTasks = Task.Run(() =>
                {
                    ServiceBusMessageSerializationMappingBase.ConfigureMappings();

                    using (MappingBuilder mappingBuilder = new MappingBuilder())
                    {
                        mappingBuilder.WithBase();
                    }
                });

                IvinciContainer.Instance.RegisterComponent(new RegisterConsumerByProcessorType(processorTypes, universalConsumerAllowedQueues));

                container = IvinciContainer.Instance.Container();

                mappingTasks.Wait();
            }
            
            bool shouldStartScheduler = processorTypes.Contains("ScheduleService", StringComparer.InvariantCultureIgnoreCase);

            ConsoleSetup.Run<IUniversalProcessorWindowsService>(container, priority, "Ivh.Console.UniversalProcessor", service => service.Start(shouldStartScheduler));
        }
    }
}
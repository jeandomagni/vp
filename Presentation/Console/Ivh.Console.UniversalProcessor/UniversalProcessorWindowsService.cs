﻿namespace Ivh.Console.UniversalProcessor
{
    using System;
    using Autofac;
    using Common.DependencyInjection;
    using Common.ServiceBus.Constants;
    using Interfaces;
    using MassTransit;
    using Quartz;

    public class UniversalProcessorWindowsService : IUniversalProcessorWindowsService
    {
        private IBusControl _bus;
        private IScheduler _scheduler;

        public void Start(bool startScheduler)
        {
            this._bus = IvinciContainer.Instance.Container().Resolve<IBusControl>();
            this._bus.Start();

            if (startScheduler)
            {
                this._scheduler = IvinciContainer.Instance.Container().Resolve<IScheduler>();
                this._scheduler.Start();
            }
        }

        public void Start()
        {
            this.Start(false);
        }

        public void Stop()
        {
            this._bus?.Stop(TimeSpan.FromSeconds(ServiceBus.StopTimeoutSeconds));
            this._bus = null;

            if (this._scheduler != null)
            {
                this._scheduler.Standby();
                this._scheduler.Shutdown();
                this._scheduler = null;
            }

            IvinciContainer.Instance.Container().Dispose();
        }
    }
}
﻿namespace Ivh.Console.UniversalProcessor.Consumers.ScheduleService
{
    using System;
    using System.Threading.Tasks;
    using Application.Logging.Common.Interfaces;
    using Application.Scheduling.Common.Interfaces;
    using Autofac;
    using Common.DependencyInjection;
    using MassTransit;
    using MassTransit.Scheduling;
    
    public class ScheduleMessageConsumer : IConsumer<ScheduleMessage>, IConsumer<ScheduleRecurringMessage>, IDisposable
    {
        public Task Consume(ConsumeContext<ScheduleMessage> context)
        {
            if (context.Message != null)
            {
                using (ILifetimeScope scope = IvinciContainer.Instance.Container().BeginLifetimeScope())
                {
                    ISchedulingApplicationService schedulingApplicationService = scope.Resolve<ISchedulingApplicationService>();
                    ILoggingApplicationService logger = scope.Resolve<ILoggingApplicationService>();

                    logger.Info(() => "ScheduleMessageConsumer::Consume<ConsumeContext<ScheduleMessage>> - Start");
                    schedulingApplicationService.ScheduleMessage(context);
                    logger.Info(() => "ScheduleMessageConsumer::Consume<ConsumeContext<ScheduleMessage>> - End");
                }
            }

            return Task.CompletedTask;
        }

        public Task Consume(ConsumeContext<ScheduleRecurringMessage> context)
        {
            if (context.Message != null)
            {
                using (ILifetimeScope scope = IvinciContainer.Instance.Container().BeginLifetimeScope())
                {
                    ISchedulingApplicationService schedulingApplicationService = scope.Resolve<ISchedulingApplicationService>();
                    ILoggingApplicationService logger = scope.Resolve<ILoggingApplicationService>();

                    logger.Info(() => "ScheduleMessageConsumer::Consume<ConsumeContext<ScheduleRecurringMessage>> - Start");
                    schedulingApplicationService.ScheduleRecurringMessage(context);
                    logger.Info(() => "ScheduleMessageConsumer::Consume<ConsumeContext<ScheduleRecurringMessage>> - End");
                }
            }

            return Task.CompletedTask;
        }
        public void Dispose()
        {
        }
    }
}

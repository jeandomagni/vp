#----------[Init]----------
$ErrorActionPreference = "Stop"

$localDirectory = $OctopusParameters["OctopusOriginalPackageDirectoryPath"]
$localDirectory

$PowerShellModules = Get-ChildItem -Path "$localDirectory" | Where-Object { $_.Name -like 'DeployModule.psm1' }
foreach ($Module in $PowerShellModules) {
	Write-Host "Import Module $($Module.FullName)"
	Import-Module $Module.FullName -Force -Verbose:$False
}

#----------[Declarations]----------
$clientName = $OctopusParameters["Client"]
$serviceName = "Ivh.Console.UniversalProcessor.All.$clientName"
$serviceName

#----------[Main]----------
Write-Host "Stopping $serviceName"
Stop-WindowsServices -ServiceNames $serviceName

Write-Host "Removing $serviceName"
Remove-WindowsServices -ServiceNames $serviceName
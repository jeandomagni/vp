
###                                                       ###
###                                                       ###
### This is sample code to show the typical R code        ###
###   used to implement bad debt segmantation. None       ###
###   of the code is real or reflects what goes on in     ###
###   production today.                                   ###
###                                                       ###
###                                                       ###

args = commandArgs() 

# Load in needed libraries
library(jsonlite)


# Read in the data used in the segmentation  
# rdd <- fromJSON("C:/home/data/BadDebtSegmentation2.json")
ixx <- inp

# create a new variable "SegScore" that stores a new score; default the initial value to 0
ixx$RawSegmentationScore <- 0
ixx$ScriptVersion <- "BD Version 1"

sample.rawscores <- c(-1L,0L,50L,100L,101L,150L,173L,174L,250L,263L,264L,350L,460L,461L)

# Calculate the raw segmentation score
ixx$RawSegmentationScore <- ifelse(is.na(ixx$PtpScore)==TRUE, 
                                   "N",
                                   sample(sample.rawscores, nrow(ixx), replace=TRUE))

# Output the data as a JSON object
output <- jsonlite::toJSON(ixx, dataframe = "rows")
print(paste("Output: ", output))
# print(ixx)

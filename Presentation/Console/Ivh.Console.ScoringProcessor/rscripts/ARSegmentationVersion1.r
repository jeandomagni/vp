
args = commandArgs() 

# Load in needed libraries
library(jsonlite)

# Read in the data used in the segmentation  
ths <- inp$AccountsReceivableSegmentationThresholds
ixx <- inp$SegmentationServiceAccountsReceivableData

ixx$ScriptVersion <- "AR Version 1"


# calculate the segmentation value
for(k in 1:nrow(ixx)) {
  
  #The decision to put an HSG into the Very Low OR Very High Balance segment should happen independent of Ptp Score
  if(ixx$HsCurrentBalance[k] >= ths$MinHsGuarantorBalance[1] && ixx$HsCurrentBalance[k] <= ths$MaxHsGuarantorBalance[1]) {
    ixx$RawSegmentationScore[k] <- ths$SegmentationValue[1]
  } 
  
  else if(ixx$HsCurrentBalance[k] >= ths$MinHsGuarantorBalance[15] && ixx$HsCurrentBalance[k] <= ths$MaxHsGuarantorBalance[15]) {
    ixx$RawSegmentationScore[k] <- ths$SegmentationValue[15]
  } 

  else if(is.na(ixx$PtpScore[k])==TRUE || ixx$PtpScore[k]==0.00) {
    ixx$RawSegmentationScore[k] <- 'N'
  } 
  
  else if(ixx$PtpScore[k] >= ths$MinPtpScore[2] & ixx$PtpScore[k] <= ths$MaxPtpScore[2] & 
          ixx$HsCurrentBalance[k] >= ths$MinHsGuarantorBalance[2] && ixx$HsCurrentBalance[k] <= ths$MaxHsGuarantorBalance[2]) {
    ixx$RawSegmentationScore[k] <- ths$SegmentationValue[2]
  } 
  
  else if(ixx$PtpScore[k] >= ths$MinPtpScore[3] & ixx$PtpScore[k] <= ths$MaxPtpScore[3] & 
          ixx$HsCurrentBalance[k] >= ths$MinHsGuarantorBalance[3] && ixx$HsCurrentBalance[k] <= ths$MaxHsGuarantorBalance[3]) {
    ixx$RawSegmentationScore[k] <- ths$SegmentationValue[3]
  } 
  
  else if(ixx$PtpScore[k] >= ths$MinPtpScore[4] & ixx$PtpScore[k] <= ths$MaxPtpScore[4] & 
          ixx$HsCurrentBalance[k] >= ths$MinHsGuarantorBalance[4] && ixx$HsCurrentBalance[k] <= ths$MaxHsGuarantorBalance[4]) {
    ixx$RawSegmentationScore[k] <- ths$SegmentationValue[4]
  } 
  
  else if(ixx$PtpScore[k] >= ths$MinPtpScore[5] & ixx$PtpScore[k] <= ths$MaxPtpScore[5] & 
          ixx$HsCurrentBalance[k] >= ths$MinHsGuarantorBalance[5] && ixx$HsCurrentBalance[k] <= ths$MaxHsGuarantorBalance[5]) {
    ixx$RawSegmentationScore[k] <- ths$SegmentationValue[5]
  } 
  
  else if(ixx$PtpScore[k] >= ths$MinPtpScore[6] & ixx$PtpScore[k] <= ths$MaxPtpScore[6] & 
          ixx$HsCurrentBalance[k] >= ths$MinHsGuarantorBalance[6] && ixx$HsCurrentBalance[k] <= ths$MaxHsGuarantorBalance[6]) {
    ixx$RawSegmentationScore[k] <- ths$SegmentationValue[6]
  } 
  
  else if(ixx$PtpScore[k] >= ths$MinPtpScore[7] & ixx$PtpScore[k] <= ths$MaxPtpScore[7] & 
          ixx$HsCurrentBalance[k] >= ths$MinHsGuarantorBalance[7] && ixx$HsCurrentBalance[k] <= ths$MaxHsGuarantorBalance[7]) {
    ixx$RawSegmentationScore[k] <- ths$SegmentationValue[7]
  } 
  
  else if(ixx$PtpScore[k] >= ths$MinPtpScore[8] & ixx$PtpScore[k] <= ths$MaxPtpScore[8] & 
          ixx$HsCurrentBalance[k] >= ths$MinHsGuarantorBalance[8] && ixx$HsCurrentBalance[k] <= ths$MaxHsGuarantorBalance[8]) {
    ixx$RawSegmentationScore[k] <- ths$SegmentationValue[8]
  } 
  
  else if(ixx$PtpScore[k] >= ths$MinPtpScore[9] & ixx$PtpScore[k] <= ths$MaxPtpScore[9] & 
          ixx$HsCurrentBalance[k] >= ths$MinHsGuarantorBalance[9] && ixx$HsCurrentBalance[k] <= ths$MaxHsGuarantorBalance[9]) {
    ixx$RawSegmentationScore[k] <- ths$SegmentationValue[9]
  } 
  
  else if(ixx$PtpScore[k] >= ths$MinPtpScore[10] & ixx$PtpScore[k] <= ths$MaxPtpScore[10] & 
          ixx$HsCurrentBalance[k] >= ths$MinHsGuarantorBalance[10] && ixx$HsCurrentBalance[k] <= ths$MaxHsGuarantorBalance[10]) {
    ixx$RawSegmentationScore[k] <- ths$SegmentationValue[10]
  } 
  
  else if(ixx$PtpScore[k] >= ths$MinPtpScore[11] & ixx$PtpScore[k] <= ths$MaxPtpScore[11] & 
          ixx$HsCurrentBalance[k] >= ths$MinHsGuarantorBalance[11] && ixx$HsCurrentBalance[k] <= ths$MaxHsGuarantorBalance[11]) {
    ixx$RawSegmentationScore[k] <- ths$SegmentationValue[11]
  } 
  
  else if(ixx$PtpScore[k] >= ths$MinPtpScore[12] & ixx$PtpScore[k] <= ths$MaxPtpScore[12] & 
          ixx$HsCurrentBalance[k] >= ths$MinHsGuarantorBalance[12] && ixx$HsCurrentBalance[k] <= ths$MaxHsGuarantorBalance[12]) {
    ixx$RawSegmentationScore[k] <- ths$SegmentationValue[12]
  } 
  
  else if(ixx$PtpScore[k] >= ths$MinPtpScore[13] & ixx$PtpScore[k] <= ths$MaxPtpScore[13] & 
          ixx$HsCurrentBalance[k] >= ths$MinHsGuarantorBalance[13] && ixx$HsCurrentBalance[k] <= ths$MaxHsGuarantorBalance[13]) {
    ixx$RawSegmentationScore[k] <- ths$SegmentationValue[13]
  } 
  
  else if(ixx$PtpScore[k] >= ths$MinPtpScore[14] & ixx$PtpScore[k] <= ths$MaxPtpScore[14] & 
          ixx$HsCurrentBalance[k] >= ths$MinHsGuarantorBalance[14] && ixx$HsCurrentBalance[k] <= ths$MaxHsGuarantorBalance[14]) {
    ixx$RawSegmentationScore[k] <- ths$SegmentationValue[14]
  } 
  
  else {
    ixx$RawSegmentationScore[k] <- 'E'
  }
}

# Output the data as a JSON object
output <- jsonlite::toJSON(ixx, dataframe = "rows")
print(paste("Output: ", output))

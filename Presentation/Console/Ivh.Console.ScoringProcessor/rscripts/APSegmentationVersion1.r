
###                                    ###
###  Active passive segmentation.      ###
###                                    ###


args = commandArgs() 

# Load in needed libraries
library(jsonlite)


# Read in the data used in the segmentation  
ixx <- inp$SegmentationServiceActivePassiveData
ths <- inp$ActivePassiveSegmentationThreshold

ixx$ScriptVersion <- "AP Version 1"

# calculated the Active/Passive flag
ixx$IsActive <- ifelse(is.na(ixx$PtpScore)==TRUE & #If PTP is null & Guarantor Balance <= $1,000
                       ixx$HsCurrentBalance <= ths$HsGuarantorBalanceNoPtpLimit, FALSE,
                       
                       ifelse(is.na(ixx$PtpScore)==FALSE & ixx$PtpScore >= ths$Ptp &  #If PTP >= 220 & Guarantor Balance >= $10,000 - Special Passive
                              ixx$HsCurrentBalance >= ths$HsGuarantorBalanceUpperLimit, FALSE,
                       
                             ifelse(is.na(ixx$PtpScore)==FALSE & ixx$PtpScore >= ths$Ptp &  #If PTP >= 220 & Guarantor Balance <= $5000 & All accounts have SelfPayDate < 60 days
                                    ixx$HsCurrentBalance <= ths$HsGuarantorBalanceLowerLimit & 
                                    ixx$SelfPayDatesWithinBounds == 1, FALSE,
                                    
                                    ifelse(is.na(ixx$PtpScore)==FALSE & ixx$PtpScore >= ths$Ptp &  #If PTP >= 220 & Guarantor Balance <= $5000 & If any accounts SelfPayDate >= 60 days, must have a Payment within the last 60 days
                                           ixx$HsCurrentBalance <= ths$HsGuarantorBalanceLowerLimit & 
                                           ixx$SelfPayDatesWithinBounds == 0 &
                                           ixx$AllOldVisitsHavePriorPayment == 1, FALSE,
      
                                           TRUE))))


# Output the data as a JSON object
output <- jsonlite::toJSON(ixx, dataframe = "rows")
print(paste("Output: ", output))

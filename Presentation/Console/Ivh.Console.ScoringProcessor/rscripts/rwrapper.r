args = commandArgs() 
if (!require("jsonlite"))
{
  install.packages("jsonlite",repos = "http://cran.us.r-project.org")
}
if (!require("gbm"))
{
  install.packages("gbm",repos = "http://cran.us.r-project.org")
}
if (!require("dplyr"))
{
  install.packages("dplyr",repos = "http://cran.us.r-project.org")
}
if (!require("xgboost"))
{
  install.packages("xgboost",repos = "http://cran.us.r-project.org")
}
library(jsonlite)
inp <- fromJSON(args[4])
dataUrls = strsplit(args[3], ",")[[1]]
for(dataUrl in dataUrls){
  load(file=dataUrl)
}
source(args[2])
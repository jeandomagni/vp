﻿namespace Ivh.Console.ScoringProcessor.Consumers
{
    using System.Threading.Tasks;
    using Autofac;
    using Common.Base.Enums;
    using Common.Cache;
    using Common.DependencyInjection;
    using Common.VisitPay.Enums;
    using Common.VisitPay.Messages.Monitoring.SystemHealth.ProcessorHealth;
    using MassTransit;
    public class ProcessorHealthConsumer : IConsumer<ProcessorHealthMessage>
    {

        public Task Consume(ConsumeContext<ProcessorHealthMessage> context)
        {
            if (context.Message.Application == ApplicationEnum.ScoringProcessor)
            {
                using (ILifetimeScope lifetimeScope = IvinciContainer.Instance.Container().BeginLifetimeScope())
                {
                    lifetimeScope.Resolve<IDistributedCache>().ReleaseLock(context.Message.RoundTripToken.ToString());
                }
            }
            return Task.CompletedTask;
        }
    }
}

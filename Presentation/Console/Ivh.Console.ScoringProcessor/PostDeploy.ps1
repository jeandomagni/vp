﻿#This is a dirty little hack as it wont let us set our retention policy to 0
try{
	$path = $OctopusParameters["Octopus.Tentacle.PreviousInstallation.OriginalInstalledPath"]
	Write-Host "Path to look at = $($path)\.."

	if($path){
		$path = $path + "\.."
		$items = Get-Childitem $path -include *.* -recurse 
		foreach ($item in $items) 
		{
    		Remove-Item $item.fullname -Force -Recurse -EA SilentlyContinue
    		#Write-Host $items.FullName
		}
	}
	else{
		Write-Host "Nothing to remove"
	}
}
Catch
{}

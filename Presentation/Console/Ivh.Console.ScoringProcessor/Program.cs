﻿namespace Ivh.Console.ScoringProcessor
{
    using System;
    using System.Linq;
    using System.Threading.Tasks;
    using Autofac;
    using Common.Base.Enums;
    using Common.Base.Interfaces;
    using Common.Console;
    using Common.DependencyInjection;
    using Common.DependencyInjection.Registration;
    using Common.ServiceBus;
    using Common.VisitPay.Enums;
    using SetupIoc;

    class Program
    {
        static void Main(string[] args)
        {
            string priority = ConsoleSetup.GetAppSettingValue("Priority");

            IContainer container = null;
            if (!args.Contains("install", StringComparer.InvariantCultureIgnoreCase)
                && !args.Contains("uninstall", StringComparer.InvariantCultureIgnoreCase)
                && !args.Contains("stop", StringComparer.InvariantCultureIgnoreCase)
            )
            {
                Task mappingTasks = Task.Run(() =>
                {
                    ServiceBusMessageSerializationMappingBase.ConfigureMappings();
                    using (MappingBuilder mappingBuilder = new MappingBuilder())
                    {
                        mappingBuilder.WithBase();
                    }
                });

                ConsumerMessagePriorityEnum priorityEnum = ConsoleSetup.ParseConsumerMessagePriorityFromAppConfigParameter(priority);
                IvinciContainer.Instance.RegisterComponent(new RegisterConsumerByPriority(priorityEnum));
                container = IvinciContainer.Instance.Container();

                mappingTasks.Wait();
            }

            ConsoleSetup.Run<ITopShelfService>(container, priority, "Ivh.Console.ScoringProcessor");
        }
    }
}

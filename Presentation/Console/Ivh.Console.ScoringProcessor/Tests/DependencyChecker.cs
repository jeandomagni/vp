﻿namespace Ivh.Console.ScoringProcessor.Tests
{
    using System.Collections.Generic;
    using System.Reflection;
    using NUnit.Framework;

    [TestFixture]
    public class DependencyChecker : Common.Testing.DependencyCheckerTest
    {
        [Test]
        public void CheckDependencies()
        {
            IList<KeyValuePair<string, string>> ignoreList = new List<KeyValuePair<string, string>>
            {
                new KeyValuePair<string, string>("NSass.Wrapper.proxy", "NSass.Core")
            };
            base.CheckDependencies(Assembly.GetExecutingAssembly().GetName(), ignoreList);
        }
    }
}
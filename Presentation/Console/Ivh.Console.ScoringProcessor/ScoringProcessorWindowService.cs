﻿namespace Ivh.Console.ScoringProcessor
{
    using System;
    using Autofac;
    using Common.Base.Constants;
    using Common.Base.Interfaces;
    using Common.DependencyInjection;
    using Ivh.Common.ServiceBus.Constants;
    using MassTransit;

    public class ScoringProcessorWindowService : ITopShelfService
    {
        private IBusControl _bus;
        public void Start()
        {
            this._bus = IvinciContainer.Instance.Container().Resolve<IBusControl>();
            this._bus.Start();
        }

        public void Stop()
        {
            this._bus?.Stop(TimeSpan.FromSeconds(ServiceBus.StopTimeoutSeconds));
            this._bus = null;
            IvinciContainer.Instance.Container().Dispose();
        }
    }
}
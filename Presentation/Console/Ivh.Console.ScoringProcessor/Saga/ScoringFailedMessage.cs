﻿namespace Ivh.Console.ScoringProcessor.Saga
{
    using System;
    using System.Runtime.Serialization;
    using MassTransit;

    [DataContract]
    [Serializable]
    public class ScoringFailedMessage : CorrelatedBy<Guid>
    {
        [DataMember(Order = 1)]
        public Guid CorrelationId { get; set; }
        [DataMember(Order = 2)]
        public string ErrorMessage { get; set; }
        [DataMember(Order = 3)]
        public Guid ProcessId { get; set; }
    }
}

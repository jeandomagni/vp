﻿namespace Ivh.Console.ScoringProcessor.Saga
{
    using System;
    using System.Runtime.Serialization;
    using Common.VisitPay.Messages.Scoring;
    using MassTransit;

    [DataContract]
    [Serializable]
    public class ScoringFinishedMessage : CorrelatedBy<Guid>
    {
        [DataMember(Order = 1)]
        public Guid CorrelationId { get; set; }
        [DataMember(Order = 2)]
        public Guid ProcessId { get; set; }
        [DataMember(Order = 3)]
        public SegmentationEnable SegmentationEnable { get; set; }
        [DataMember(Order = 4)]
        public bool IsProcessed { get; set; }
    }
}

﻿namespace Ivh.Console.ScoringProcessor.Saga
{
    using MassTransit.NHibernateIntegration;

    public class ScoringAndSegmentationSagaMap : SagaClassMapping<ScoringAndSegmentationSaga>
    {
        public ScoringAndSegmentationSagaMap()
        {
            this.Lazy(false);
            this.Schema("scoring");
            this.Table("ScoringAndSegmentationSaga");
            this.Property(x => x.CurrentState);
            this.Property(x => x.ScoringBatchId);
            this.Property(x => x.BadDebtSegmentationBatchId);
            this.Property(x => x.AccountsReceivableSegmentationBatchId);
            this.Property(x => x.ActivePassiveSegmentationBatchId);
            this.Property(x => x.PresumptiveCharitySegmentationBatchId);
            this.Property(x => x.Version);
            this.Property(x => x.Timestamp);
            this.Property(x => x.ScoringEventFailures);
            this.Property(x => x.ScoringEventSuccesses);
            this.Property(x => x.ScoringEventTotal);
            this.Property(x => x.AccountReceivableSegmentationEventFailures);
            this.Property(x => x.AccountReceivableSegmentationEventSuccesses);
            this.Property(x => x.AccountReceivableSegmentationEventTotal);
            this.Property(x => x.BadDebtSegmentationEventFailures);
            this.Property(x => x.BadDebtSegmentationEventSuccesses);
            this.Property(x => x.BadDebtSegmentationEventTotal);
            this.Property(x => x.ActivePassiveSegmentationEventSuccesses);
            this.Property(x => x.ActivePassiveSegmentationEventFailures);
            this.Property(x => x.ActivePassiveSegmentationEventTotal);
            this.Property(x => x.PresumptiveCharitySegmentationEventSuccesses);
            this.Property(x => x.PresumptiveCharitySegmentationEventFailures);
            this.Property(x => x.PresumptiveCharitySegmentationEventTotal);

        }
    }
}

﻿namespace Ivh.Console.ScoringProcessor.Saga
{
    using System;
    using System.Runtime.Serialization;
    using MassTransit;

    [DataContract]
    [Serializable]
    public class SegmentationStartMessage : CorrelatedBy<Guid>
    {
        [DataMember(Order = 1)]
        public Guid CorrelationId { get; set; }
    }
}

﻿namespace Ivh.Console.ScoringProcessor.Saga
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading;
    using System.Threading.Tasks;
    using Application.HospitalData.Common.Interfaces;
    using Autofac;
    using Automatonymous;
    using Common.Base.Utilities.Extensions;
    using Common.Base.Utilities.Helpers;
    using Common.DependencyInjection;
    using Common.VisitPay.Messages.HospitalData.Dtos;
    using Common.VisitPay.Messages.Scoring;
    using Domain.HospitalData.Scoring.Interfaces;
    using Domain.HospitalData.Segmentation.Enums;
    using Domain.HospitalData.Segmentation.Interfaces;
    using Domain.Logging.Interfaces;
    using Ivh.Common.VisitPay.Constants;
    using MassTransit;

    public class ScoringAndSegmentationStateMachine : MassTransitStateMachine<ScoringAndSegmentationSaga>
    {
        private readonly Lazy<ILoggingService> _loggingService;
        private readonly Lazy<IMetricsProvider> _metricsProvider;
        private readonly Lazy<ISegmentationBadDebtService> _segmentationBadDebtService;
        private readonly Lazy<ISegmentationAccountsReceivableService> _segmentationAccountsReceivableService;
        private readonly Lazy<ISegmentationActivePassiveService> _segmentationActivePassiveService;
        private readonly Lazy<ISegmentationPresumptiveCharityService> _segmentationPresumptiveCharityService;

        // States
        public State ProcessingScoring { get; set; }
        public State ProcessingBadDebtSegmentation { get; set; }
        public State ProcessingAccountsReceivableSegmentation { get; set; }
        public State ProcessingActivePassiveSegmentation { get; set; }
        public State ProcessingPresumptiveCharitySegmentation { get; set; }
        public State Failed { get; set; }
        public State Complete { get; set; }

        // Scoring eventsScoringPtpScoreRequestEvent
        public Event<ScoringAndSegmentationStartMessage> ScoringAndSegmentationStarting { get; set; }

        public Event<ScoringScoreRequestMessage> ScoringScoreRequestEvent { get; set; }
        public Event<ScoringStatisticalMonitoringMessage> ScoringStatisticalMonitoring { get; set; }
        public Event<ScoringFinishedMessage> ScoringFinished { get; set; }
        public Event<ScoringFailedMessage> ScoringFailed { get; set; }
        public Event<ScoringStatisticalMonitoringFinishedMessage> ScoringStatisticalMonitoringFinished { get; set; }


        // Segmentation Events
        public Event<SegmentationBadDebtStartingMessage> SegmentationBadDebtStartingEvent { get; set; }
        public Event<SegmentationBadDebtRequestMessage> SegmentationBadDebtRequestEvent { get; set; }
        public Event<SegmentationAccountsReceivableStartingMessage> SegmentationAccountsReceivableStartingEvent { get; set; }
        public Event<SegmentationAccountsReceivableRequestMessage> SegmentationAccountReceivableRequestEvent { get; set; }
        public Event<SegmentationActivePassiveStartingMessage> SegmentationActivePassiveStartingEvent { get; set; }
        public Event<SegmentationActivePassiveRequestMessage> SegmentationActivePassiveRequestEvent { get; set; }
        public Event<SegmentationPresumptiveCharityStartingMessage> SegmentationPresumptiveCharityStartingEvent { get; set; }
        public Event<SegmentationPresumptiveCharityRequestMessage> SegmentationPresumptiveCharityRequestEvent { get; set; }
        public Event<SegmentationBadDebtFinishedMessage> BadDebtSegmentationFinished { get; set; }
        public Event<SegmentationAccountsReceivableFinishedMessage> AccountsReceivableSegmentationFinished { get; set; }
        public Event<SegmentationActivePassiveFinishedMessage> ActivePassiveSegmentationFinished { get; set; }
        public Event<SegmentationPresumptiveCharityFinishedMessage> PresumptiveCharitySegmentationFinished { get; set; }

        private int _scoringSuccesses;
        private int _scoringFailures;
        private int _scoringTotal;
        private int _scoringLastMessage;

        public ScoringAndSegmentationStateMachine(
            Lazy<ILoggingService> loggingService,
            Lazy<IMetricsProvider> metricsProvider,
            Lazy<ISegmentationBadDebtService> segmentationBadDebtService,
            Lazy<ISegmentationAccountsReceivableService> segmentationAccountsReceivableService,
            Lazy<ISegmentationActivePassiveService> segmentationActivePassiveService,
            Lazy<ISegmentationPresumptiveCharityService> segmentationPresumptiveCharityService
            )
        {
            this._loggingService = loggingService;
            this._metricsProvider = metricsProvider;
            this._segmentationBadDebtService = segmentationBadDebtService;
            this._segmentationAccountsReceivableService = segmentationAccountsReceivableService;
            this._segmentationActivePassiveService = segmentationActivePassiveService;
            this._segmentationPresumptiveCharityService = segmentationPresumptiveCharityService;

            this.InstanceState(x => x.CurrentState);
            this.State(() => this.ProcessingScoring);
            this.State(() => this.ProcessingBadDebtSegmentation);
            this.State(() => this.ProcessingAccountsReceivableSegmentation);
            this.State(() => this.ProcessingActivePassiveSegmentation);
            this.State(() => this.ProcessingPresumptiveCharitySegmentation);
            this.State(() => this.Complete);
            this.State(() => this.Failed);

            this.Event(() => this.ScoringAndSegmentationStarting);
            this.Event(() => this.ScoringScoreRequestEvent, x => x.CorrelateById(data => data.CorrelationId, context => context.Message.CorrelationId));
            this.Event(() => this.ScoringStatisticalMonitoring, x => x.CorrelateById(data => data.CorrelationId, context => context.Message.CorrelationId));
            this.Event(() => this.ScoringFinished, x => x.CorrelateById(data => data.CorrelationId, context => context.Message.CorrelationId));
            this.Event(() => this.ScoringFailed, x => x.CorrelateById(data => data.CorrelationId, context => context.Message.CorrelationId));
            this.Event(() => this.SegmentationBadDebtStartingEvent, x => x.CorrelateById(data => data.CorrelationId, context => context.Message.CorrelationId));
            this.Event(() => this.SegmentationBadDebtRequestEvent, x => x.CorrelateById(data => data.CorrelationId, context => context.Message.CorrelationId));
            this.Event(() => this.SegmentationAccountsReceivableStartingEvent, x => x.CorrelateById(data => data.CorrelationId, context => context.Message.CorrelationId));
            this.Event(() => this.SegmentationAccountReceivableRequestEvent, x => x.CorrelateById(data => data.CorrelationId, context => context.Message.CorrelationId));
            this.Event(() => this.SegmentationActivePassiveStartingEvent, x => x.CorrelateById(data => data.CorrelationId, context => context.Message.CorrelationId));
            this.Event(() => this.SegmentationActivePassiveRequestEvent, x => x.CorrelateById(data => data.CorrelationId, context => context.Message.CorrelationId));
            this.Event(() => this.BadDebtSegmentationFinished, x => x.CorrelateById(data => data.CorrelationId, context => context.Message.CorrelationId));
            this.Event(() => this.AccountsReceivableSegmentationFinished, x => x.CorrelateById(data => data.CorrelationId, context => context.Message.CorrelationId));
            this.Event(() => this.ActivePassiveSegmentationFinished, x => x.CorrelateById(data => data.CorrelationId, context => context.Message.CorrelationId));
            this.Event(() => this.SegmentationPresumptiveCharityStartingEvent, x => x.CorrelateById(data => data.CorrelationId, context => context.Message.CorrelationId));
            this.Event(() => this.PresumptiveCharitySegmentationFinished, x => x.CorrelateById(data => data.CorrelationId, context => context.Message.CorrelationId));

            this.Initially(
                this.When(this.ScoringAndSegmentationStarting)
                    .ThenAsync(async context =>
                    {
                        context.Instance.CorrelationId = new Guid(NewId.Next().ToByteArray());
                        context.Instance.ScoringBatchId = context.Data.ScoringBatchId;
                        context.Instance.Timestamp = context.Data.StartTime;
                        context.Instance.ScoringEventTotal = context.Data.ScoringGuarantorDtos.Count;
                        this._scoringTotal = context.Instance.ScoringEventTotal;
                        this.InitializeScoring();
                        this._metricsProvider.Value.Increment(Metrics.Increment.Scoring.ScoringBatchId, context.Instance.ScoringBatchId);
                        this._metricsProvider.Value.Increment(Metrics.Increment.Scoring.ScoringTotal, this._scoringTotal);

                        try
                        {
                            if (this._scoringTotal == 0)
                            {
                                await context.Publish(new ScoringFinishedMessage
                                {
                                    CorrelationId = context.Instance.CorrelationId,
                                    SegmentationEnable = context.Data.SegmentationEnable
                                });

                            }
                            else
                            {
                                foreach (List<ScoringGuarantorDto> dataScoringGuarantorDto in context.Data.ScoringGuarantorDtos)
                                {
                                    await context.Publish(new ScoringScoreRequestMessage
                                    {
                                        ScoringGuarantorDtos = dataScoringGuarantorDto,
                                        CorrelationId = context.Instance.CorrelationId,
                                        ProcessId = new Guid(NewId.Next().ToByteArray()),
                                        SegmentationEnable = context.Data.SegmentationEnable
                                    });
                                }
                            }
                        }
                        catch (Exception e)
                        {
                            this._loggingService.Value.Fatal(() => ExceptionHelper.AggregateExceptionToString(e));
                            context.Instance.ScoringEventFailures++;
                        }
                    })
                    .TransitionTo(this.ProcessingScoring)
                    .Catch<Exception>(ex => ex.Then(ctx =>
                    {
                        this._loggingService.Value.Fatal(() => ExceptionHelper.AggregateExceptionToString(ctx.Exception));
                    }))
            );
            this.During(this.ProcessingScoring,
                this.When(this.ScoringScoreRequestEvent).ThenAsync(async context =>
                {
                    if (context.Data.IsProcessed)
                    {
                        context.Instance.ScoringEventSuccesses = this._scoringSuccesses;
                        context.Instance.ScoringEventFailures = this._scoringFailures;
                        return;
                    }
                    using (ILifetimeScope lifetimeScope = IvinciContainer.Instance.Container().BeginLifetimeScope())
                    {
                        IScoringSagaLogger scoringSagaLogger = lifetimeScope.Resolve<IScoringSagaLogger>();

                        try
                        {
                            this.LogStartTime(scoringSagaLogger, context.Instance.ScoringBatchId, context.Instance.CurrentState, context.Event.Name, context.Instance.CorrelationId, context.Data.ProcessId);
                            IHsGuarantorScoringApplicationService hsGuarantorScoringApplicationService = lifetimeScope.Resolve<IHsGuarantorScoringApplicationService>();
                            hsGuarantorScoringApplicationService.ProcessScoreRequest(context.Data, context.Instance.ScoringBatchId);
                            Interlocked.Increment(ref this._scoringSuccesses);
                            context.Instance.ScoringEventSuccesses = this._scoringSuccesses;
                            this._metricsProvider.Value.Increment(Metrics.Increment.Scoring.ScoringSuccesses);
                        }
                        catch (Exception e)
                        {
                            this._loggingService.Value.Fatal(() => ExceptionHelper.AggregateExceptionToString(e));
                            Interlocked.Increment(ref this._scoringFailures);
                            context.Instance.ScoringEventFailures = this._scoringFailures;
                            this._metricsProvider.Value.Increment(Metrics.Increment.Scoring.ScoringFailures);
                        }
                        finally
                        {
                            this.LogEndTime(scoringSagaLogger, context.Instance.ScoringBatchId, context.Instance.CurrentState, context.Event.Name, context.Instance.CorrelationId, context.Data.ProcessId);
                        }
                        context.Data.IsProcessed = true;
                        if (!this.IsScoringComplete())
                        {
                            return;
                        }

                        if (Interlocked.Exchange(ref this._scoringLastMessage, 1) == 0)
                        {
                            // This is the last scoring message
                            try
                            {
                                IScoringMatchedGuarantorScoreHistoryService scoringMatchedGuarantorScoreHistoryService = lifetimeScope.Resolve<IScoringMatchedGuarantorScoreHistoryService>();
                                scoringMatchedGuarantorScoreHistoryService.GenerateMatchedGuarantorScoreHistory(context.Instance.ScoringBatchId);
                                if (context.Instance.IsScoringSuccessful())
                                {
                                    await context.Publish(new ScoringFinishedMessage
                                    {
                                        CorrelationId = context.Instance.CorrelationId,
                                        ProcessId = context.Data.ProcessId,
                                        SegmentationEnable = context.Data.SegmentationEnable
                                    });
                                }
                                else
                                {
                                    await this.TransitionToState(context.Instance, this.Failed);
                                    await context.Publish(new ScoringFailedMessage { CorrelationId = context.Instance.CorrelationId, ProcessId = context.Data.ProcessId });
                                }
                            }
                            catch (Exception e)
                            {
                                this._loggingService.Value.Fatal(() => ExceptionHelper.AggregateExceptionToString(e));
                                await this.TransitionToState(context.Instance, this.Failed);
                                await context.Publish(new ScoringFailedMessage { CorrelationId = context.Instance.CorrelationId, ProcessId = context.Data.ProcessId });
                            }
                        }
                    }
                })
                    .Catch<Exception>(ex => ex.Then(ctx =>
                    {
                        using (ILifetimeScope lifetimeScope = IvinciContainer.Instance.Container().BeginLifetimeScope())
                        {
                            IScoringMatchedGuarantorScoreHistoryService scoringMatchedGuarantorScoreHistoryService = lifetimeScope.Resolve<IScoringMatchedGuarantorScoreHistoryService>();
                            scoringMatchedGuarantorScoreHistoryService.GenerateMatchedGuarantorScoreHistory(ctx.Instance.ScoringBatchId);
                        }
                        this._loggingService.Value.Fatal(() => ExceptionHelper.AggregateExceptionToString(ctx.Exception));
                    }))
                ,
                this.When(this.ScoringFinished)
                    .ThenAsync(async context =>
                    {
                        if (context.Data.IsProcessed)
                        {
                            return;
                        }

                        context.Data.IsProcessed = true;
                        int totalMessagesPublished = 0;
                        if (context.Data.SegmentationEnable.EnableBadDebtSegmentation)
                        {
                            totalMessagesPublished = this.GenerateBadDebtMessages(context);
                            this._segmentationBadDebtService.Value.SegmentationTotal = totalMessagesPublished;
                            context.Instance.BadDebtSegmentationEventTotal = totalMessagesPublished;
                            this._segmentationBadDebtService.Value.SegmentationBatchId = context.Instance.BadDebtSegmentationBatchId;
                        }

                        this._metricsProvider.Value.Increment(Metrics.Increment.Scoring.SegmentationBadDebtTotal, totalMessagesPublished);
                        if (totalMessagesPublished == 0)
                        {
                            await context.Publish(new SegmentationBadDebtFinishedMessage
                            {
                                CorrelationId = context.Instance.CorrelationId,
                                ProcessId = new Guid(NewId.Next().ToByteArray()),
                                SegmentationEnable = context.Data.SegmentationEnable
                            });
                        }
                    })
                    .TransitionTo(this.ProcessingBadDebtSegmentation)
                    .Catch<Exception>(ex => ex.Then(ctx =>
                    {
                        this._loggingService.Value.Fatal(() => ExceptionHelper.AggregateExceptionToString(ctx.Exception));
                    }))
            );

            this.During(this.ProcessingBadDebtSegmentation,
                this.When(this.SegmentationBadDebtRequestEvent)
                    .ThenAsync(async context =>
                    {
                        context.Instance.BadDebtSegmentationBatchId = this._segmentationBadDebtService.Value.SegmentationBatchId;
                        context.Instance.BadDebtSegmentationEventTotal = this._segmentationBadDebtService.Value.SegmentationTotal;
                        if (context.Data.IsProcessed)
                        {
                            context.Instance.BadDebtSegmentationEventSuccesses = this._segmentationBadDebtService.Value.SegmentationSuccesses();
                            context.Instance.BadDebtSegmentationEventFailures = this._segmentationBadDebtService.Value.SegmentationFailures();
                            return;
                        }
                        bool isLastMessage = this._segmentationBadDebtService.Value.ProcessBadDebtSegmentation(context.Data.SegmentationBadDebtDtos, context.Instance.ScoringBatchId,
                            context.Instance.CurrentState,  context.Event.Name, 
                            context.Instance.CorrelationId, context.Data.ProcessId);
                        context.Instance.BadDebtSegmentationEventSuccesses = this._segmentationBadDebtService.Value.SegmentationSuccesses();
                        context.Instance.BadDebtSegmentationEventFailures = this._segmentationBadDebtService.Value.SegmentationFailures();
                        context.Data.IsProcessed = true;
                        if (isLastMessage)
                        {
                            await context.Publish(new SegmentationBadDebtFinishedMessage
                            {
                                CorrelationId = context.Instance.CorrelationId,
                                ProcessId = context.Data.ProcessId,
                                SegmentationEnable = context.Data.SegmentationEnable
                            });
                        }
                    })
                    .Catch<Exception>(ex => ex.Then(ctx =>
                    {
                        this._loggingService.Value.Fatal(() => ExceptionHelper.AggregateExceptionToString(ctx.Exception));
                    }))
                ,
                this.When(this.BadDebtSegmentationFinished)
                    .ThenAsync(async context =>
                    {
                        if (context.Data.IsProcessed)
                        {
                            return;
                        }

                        context.Data.IsProcessed = true;
                        int totalMessagesPublished = 0;
                        if (context.Data.SegmentationEnable.EnableAccountsReceivableSegmentation)
                        {
                            totalMessagesPublished = this.GenerateAccountsReceivableMessages(context);
                            this._segmentationAccountsReceivableService.Value.SegmentationTotal = totalMessagesPublished;
                            context.Instance.AccountReceivableSegmentationEventTotal = totalMessagesPublished;
                            this._segmentationAccountsReceivableService.Value.SegmentationBatchId = context.Instance.AccountsReceivableSegmentationBatchId;
                        }
						
                        this._metricsProvider.Value.Increment(Metrics.Increment.Scoring.SegmentationAccountsReceivableTotal, totalMessagesPublished);
                        if (totalMessagesPublished == 0)
                        {
                            await context.Publish(new SegmentationAccountsReceivableFinishedMessage
                            {
                                CorrelationId = context.Instance.CorrelationId,
                                ProcessId = context.Data.ProcessId,
                                SegmentationEnable = context.Data.SegmentationEnable
                            });
                        }
                    })
                    .TransitionTo(this.ProcessingAccountsReceivableSegmentation)
                    .Catch<Exception>(ex => ex.Then(ctx =>
                    {
                        this._loggingService.Value.Fatal(() => ExceptionHelper.AggregateExceptionToString(ctx.Exception));
                    })));
            this.During(this.ProcessingAccountsReceivableSegmentation,
                this.When(this.SegmentationAccountReceivableRequestEvent)
                    .ThenAsync(async context =>
                    {
                        context.Instance.AccountsReceivableSegmentationBatchId = this._segmentationAccountsReceivableService.Value.SegmentationBatchId;
                        context.Instance.AccountReceivableSegmentationEventTotal = this._segmentationAccountsReceivableService.Value.SegmentationTotal;
                        if (context.Data.IsProcessed)
                        {
                            context.Instance.AccountReceivableSegmentationEventSuccesses = this._segmentationAccountsReceivableService.Value.SegmentationSuccesses();
                            context.Instance.AccountReceivableSegmentationEventFailures = this._segmentationAccountsReceivableService.Value.SegmentationFailures();
                            return;
                        }
                        bool isLastMessage = this._segmentationAccountsReceivableService.Value.ProcessAccountsReceivableSegmentation(context.Data.AccountsReceivableDtos, context.Instance.ScoringBatchId,
                            context.Instance.CurrentState, context.Event.Name,
                            context.Instance.CorrelationId, context.Data.ProcessId);
                        context.Instance.AccountReceivableSegmentationEventSuccesses = this._segmentationAccountsReceivableService.Value.SegmentationSuccesses();
                        context.Instance.AccountReceivableSegmentationEventFailures = this._segmentationAccountsReceivableService.Value.SegmentationFailures();
                        context.Data.IsProcessed = true;
                        if (isLastMessage)
                        {
                            await context.Publish(new SegmentationAccountsReceivableFinishedMessage
                            {
                                CorrelationId = context.Instance.CorrelationId,
                                ProcessId = context.Data.ProcessId,
                                SegmentationEnable = context.Data.SegmentationEnable
                            });
                        }
                    }),
                this.When(this.AccountsReceivableSegmentationFinished)
                    .ThenAsync(async context =>
                    {
                        if (context.Data.IsProcessed)
                        {
                            return;
                        }

                        context.Data.IsProcessed = true;
                        int totalMessagesPublished = 0;
                        if (context.Data.SegmentationEnable.EnableActivePassiveSegmentation)
                        {
                            totalMessagesPublished = this.GenerateActivePassiveMessages(context);
                            this._segmentationActivePassiveService.Value.SegmentationTotal = totalMessagesPublished;
                            context.Instance.ActivePassiveSegmentationEventTotal = totalMessagesPublished;
                            this._segmentationActivePassiveService.Value.SegmentationBatchId = context.Instance.ActivePassiveSegmentationBatchId;
                        }

                        this._metricsProvider.Value.Increment(Metrics.Increment.Scoring.SegmentationActivePassiveTotal, totalMessagesPublished);
                        if (totalMessagesPublished == 0)
                        {
                            await context.Publish(new SegmentationActivePassiveFinishedMessage
                            {
                                CorrelationId = context.Instance.CorrelationId,
                                ProcessId = context.Data.ProcessId,
                                SegmentationEnable = context.Data.SegmentationEnable
                            });
                        }
                    })
                    .TransitionTo(this.ProcessingActivePassiveSegmentation)
                    .Catch<Exception>(ex => ex.Then(ctx => { this._loggingService.Value.Fatal(() => ExceptionHelper.AggregateExceptionToString(ctx.Exception)); })));
                this.During(this.ProcessingActivePassiveSegmentation,
                this.When(this.SegmentationActivePassiveRequestEvent)
                    .ThenAsync(async context =>
                    {
                        context.Instance.ActivePassiveSegmentationBatchId = this._segmentationActivePassiveService.Value.SegmentationBatchId;
                        context.Instance.ActivePassiveSegmentationEventTotal = this._segmentationActivePassiveService.Value.SegmentationTotal;
                        if (context.Data.IsProcessed)
                        {
                            context.Instance.ActivePassiveSegmentationEventSuccesses = this._segmentationActivePassiveService.Value.SegmentationSuccesses();
                            context.Instance.ActivePassiveSegmentationEventFailures = this._segmentationActivePassiveService.Value.SegmentationSuccesses();
                            return;
                        }
                        bool isLastMessage = this._segmentationActivePassiveService.Value.ProcessActivePassiveSegmentation(context.Data.SegmentationActivePassiveDtos, context.Instance.ScoringBatchId,
                            context.Instance.CurrentState, context.Event.Name,
                            context.Instance.CorrelationId, context.Data.ProcessId);
                        context.Instance.ActivePassiveSegmentationEventSuccesses = this._segmentationActivePassiveService.Value.SegmentationSuccesses();
                        context.Instance.ActivePassiveSegmentationEventFailures = this._segmentationActivePassiveService.Value.SegmentationFailures();
                        context.Data.IsProcessed = true;
                        if (isLastMessage)
                        {
                            await context.Publish(new SegmentationActivePassiveFinishedMessage
                            {
                                CorrelationId = context.Instance.CorrelationId,
                                ProcessId = context.Data.ProcessId,
                                SegmentationEnable = context.Data.SegmentationEnable
                            });
                        }
                    }),
                this.When(this.ActivePassiveSegmentationFinished)
                    .ThenAsync(async context =>
                    {
                        await context.Publish(new SegmentationPresumptiveCharityStartingMessage
                        {
                            CorrelationId = context.Instance.CorrelationId,
                            ProcessId = context.Data.ProcessId,
                            SegmentationEnable = context.Data.SegmentationEnable
                        });
                    })
                    .TransitionTo(this.ProcessingPresumptiveCharitySegmentation)
                    .Catch<Exception>(ex => ex.Then(ctx =>
                    {
                        this._loggingService.Value.Fatal(() => ExceptionHelper.AggregateExceptionToString(ctx.Exception));
                    })));
                this.During(this.ProcessingPresumptiveCharitySegmentation,
                    this.When(this.SegmentationPresumptiveCharityStartingEvent)
                    .ThenAsync(async context =>
                    {
                        if (context.Data.SegmentationEnable.EnablePresumptiveCharitySegmentation)
                        {
                            await this.PublishPresumptiveCharityMessages(context);
                        }
                        else
                        {
                            await context.Publish(new SegmentationPresumptiveCharityFinishedMessage { CorrelationId = context.Instance.CorrelationId });
                        }
                    }),
                this.When(this.SegmentationPresumptiveCharityRequestEvent)
                    .ThenAsync(async context =>
                    {
                        context.Instance.PresumptiveCharitySegmentationBatchId = this._segmentationPresumptiveCharityService.Value.SegmentationBatchId;
                        context.Instance.PresumptiveCharitySegmentationEventTotal = this._segmentationPresumptiveCharityService.Value.SegmentationTotal;
                        if (context.Data.IsProcessed)
                        {
                            context.Instance.PresumptiveCharitySegmentationEventSuccesses = this._segmentationPresumptiveCharityService.Value.SegmentationSuccesses();
                            context.Instance.PresumptiveCharitySegmentationEventFailures = this._segmentationPresumptiveCharityService.Value.SegmentationSuccesses();
                            return;
                        }
                        bool isLastMessage = this._segmentationPresumptiveCharityService.Value.ProcessPresumptiveCharitySegmentation(context.Instance.ScoringBatchId,
                            context.Instance.CurrentState, context.Event.Name,
                            context.Instance.CorrelationId, context.Data.ProcessId);
                        context.Instance.PresumptiveCharitySegmentationEventSuccesses = this._segmentationPresumptiveCharityService.Value.SegmentationSuccesses();
                        context.Instance.PresumptiveCharitySegmentationEventFailures = this._segmentationPresumptiveCharityService.Value.SegmentationFailures();
                        context.Data.IsProcessed = true;
                        if (isLastMessage)
                        {
                            await context.Publish(new SegmentationPresumptiveCharityFinishedMessage
                            {
                                CorrelationId = context.Instance.CorrelationId
                            });
                        }
                    })
                    .Catch<Exception>(ex => ex.Then(ctx =>
                    {
                        this._loggingService.Value.Fatal(() => ExceptionHelper.AggregateExceptionToString(ctx.Exception));
                    })),
                this.When(this.PresumptiveCharitySegmentationFinished)
                    .ThenAsync(async context =>
                    {
                        if (context.Instance.ScoringEventTotal <= 0)
                        {
                            await context.Publish(new ScoringStatisticalMonitoringFinishedMessage
                            {
                                CorrelationId = context.Instance.CorrelationId
                            });
                        }

                        await context.Publish(new ScoringStatisticalMonitoringMessage
                        {
                            CorrelationId = context.Instance.CorrelationId
                        });
                    })
                    .Catch<Exception>(ex => ex.Then(ctx =>
                    {
                        this._loggingService.Value.Fatal(() => ExceptionHelper.AggregateExceptionToString(ctx.Exception));
                    })),
                this.When(this.ScoringStatisticalMonitoring)
                    .ThenAsync(async context =>
                    {
                        using (ILifetimeScope lifetimeScope = IvinciContainer.Instance.Container().BeginLifetimeScope())
                        {
                            IScoringApplicationService scoringApplicationService = lifetimeScope.Resolve<IScoringApplicationService>();
                            scoringApplicationService.ValidateDailyScoringBatchInputVariables();
                            scoringApplicationService.ValidateWeeklyScoringBatchInputVariables();
                        }
                        await context.Publish(new ScoringStatisticalMonitoringFinishedMessage
                        {
                            CorrelationId = context.Instance.CorrelationId
                        });
                    })
                    .Catch<Exception>(ex => ex.Then(ctx =>
                    {
                        this._loggingService.Value.Fatal(() => ExceptionHelper.AggregateExceptionToString(ctx.Exception));
                    })),
                this.When(this.ScoringStatisticalMonitoringFinished)
                    .TransitionTo(this.Complete)
                    .Finalize()
                    .Catch<Exception>(ex => ex.Then(ctx =>
                    {
                        this._loggingService.Value.Fatal(() => ExceptionHelper.AggregateExceptionToString(ctx.Exception));
                    }))

            );
            this.During(this.Failed,
                this.When(this.ScoringFailed)
                    .Then(context =>
                    {
                        using (ILifetimeScope lifetimeScope = IvinciContainer.Instance.Container().BeginLifetimeScope())
                        {
                            IScoringMatchedGuarantorScoreHistoryService scoringMatchedGuarantorScoreHistoryService = lifetimeScope.Resolve<IScoringMatchedGuarantorScoreHistoryService>();
                            scoringMatchedGuarantorScoreHistoryService.GenerateMatchedGuarantorScoreHistory(context.Instance.ScoringBatchId);
                        }
                        this._loggingService.Value.Fatal(() => context.Data.ErrorMessage);
                    })
                    .Catch<Exception>(ex => ex.Then(ctx =>
                    {
                        this._loggingService.Value.Fatal(() => ExceptionHelper.AggregateExceptionToString(ctx.Exception));
                    }))

            );

        }

        private void InitializeScoring()
        {
            this._scoringFailures = 0;
            this._scoringSuccesses = 0;
            this._scoringLastMessage = 0;
            this._segmentationBadDebtService.Value.InitializeService();
            this._segmentationAccountsReceivableService.Value.InitializeService();
            this._segmentationActivePassiveService.Value.InitializeService();
            this._segmentationPresumptiveCharityService.Value.InitializeService();
        }

        private bool IsScoringComplete()
        {
            return this._scoringSuccesses + this._scoringFailures >= this._scoringTotal;
        }

        private Guid LogStartTime(IScoringSagaLogger scoringSagaLogger, int scoringBatchId, string state, string eventName, Guid correlationId, Guid? processId = null)
        {
            Guid id = processId ?? new Guid(NewId.Next().ToByteArray());
            scoringSagaLogger.LogStartTime(scoringBatchId, state, eventName, correlationId, id);
            return id;
        }

        private void LogEndTime(IScoringSagaLogger scoringSagaLogger, int scoringBatchId, string state, string eventName, Guid correlationId, Guid processId)
        {
            scoringSagaLogger.LogEndTime(scoringBatchId, state, eventName, correlationId, processId);
        }

        private int GenerateBadDebtMessages(BehaviorContext<ScoringAndSegmentationSaga, ScoringFinishedMessage> context)
        {
            using (ILifetimeScope lifetimeScope = IvinciContainer.Instance.Container().BeginLifetimeScope())
            {
                ISegmentationDailyProcessService segmentationDailyProcessService = lifetimeScope.Resolve<ISegmentationDailyProcessService>();
                IScoringSagaLogger scoringSagaLogger = lifetimeScope.Resolve<IScoringSagaLogger>();

                // Create bad debt messages
                context.Instance.BadDebtSegmentationBatchId = segmentationDailyProcessService.GenerateSegmentationBatchId(SegmentationTypeEnum.BadDebt);
                this._metricsProvider.Value.Increment(Metrics.Increment.Scoring.SegmentationBadDebtBatchId, context.Instance.BadDebtSegmentationBatchId);
                this.LogStartTime(scoringSagaLogger, context.Instance.ScoringBatchId, context.Instance.CurrentState, $"{context.Event.Name}-ExecuteBadDebtSegmentation", context.Instance.CorrelationId, context.Data.ProcessId);
                segmentationDailyProcessService.ExecuteBadDebtSegmentation(context.Instance.BadDebtSegmentationBatchId);
                this.LogEndTime(scoringSagaLogger, context.Instance.ScoringBatchId, context.Instance.CurrentState, $"{context.Event.Name}-ExecuteBadDebtSegmentation", context.Instance.CorrelationId, context.Data.ProcessId);
                this.LogStartTime(scoringSagaLogger, context.Instance.ScoringBatchId, context.Instance.CurrentState, $"{context.Event.Name}-ProcessBadDebtVisitDaily", context.Instance.CorrelationId, context.Data.ProcessId);
                IList<SegmentationBadDebtDto> dailyBadDebtVisits;
                int lastHsGuarantorId = 0;
                int count = 0;
                do
                {
                    this.LogStartTime(scoringSagaLogger, context.Instance.ScoringBatchId, context.Instance.CurrentState, $"{context.Event.Name}-ProcessBadDebtVisitDaily-{count}", context.Instance.CorrelationId, context.Data.ProcessId);
                    dailyBadDebtVisits = segmentationDailyProcessService.ProcessBadDebtVisitDaily(context.Instance.BadDebtSegmentationBatchId, lastHsGuarantorId);
                    this.LogEndTime(scoringSagaLogger, context.Instance.ScoringBatchId, context.Instance.CurrentState, $"{context.Event.Name}-ProcessBadDebtVisitDaily-{count}", context.Instance.CorrelationId, context.Data.ProcessId);
                    if (dailyBadDebtVisits.IsNotNullOrEmpty())
                    {
                        context.Publish(new SegmentationBadDebtRequestMessage
                        {
                            SegmentationBadDebtDtos = dailyBadDebtVisits.ToList(),
                            CorrelationId = context.Instance.CorrelationId,
                            ProcessId = new Guid(NewId.Next().ToByteArray()),
                            SegmentationEnable = context.Data.SegmentationEnable,
                            BatchId = context.Instance.BadDebtSegmentationBatchId
                        });
                        count += 1;
                        lastHsGuarantorId = dailyBadDebtVisits.Max(x => x.HsGuarantorId);
                    }
                } while (dailyBadDebtVisits.IsNotNullOrEmpty());
                this.LogEndTime(scoringSagaLogger, context.Instance.ScoringBatchId, context.Instance.CurrentState, $"{context.Event.Name}-ProcessBadDebtVisitDaily", context.Instance.CorrelationId, context.Data.ProcessId);

                return count;
            }
        }

        private int GenerateAccountsReceivableMessages(BehaviorContext<ScoringAndSegmentationSaga, SegmentationBadDebtFinishedMessage> context)
        {
            using (ILifetimeScope lifetimeScope = IvinciContainer.Instance.Container().BeginLifetimeScope())
            {
                ISegmentationDailyProcessService segmentationDailyProcessService = lifetimeScope.Resolve<ISegmentationDailyProcessService>();
                IScoringSagaLogger scoringSagaLogger = lifetimeScope.Resolve<IScoringSagaLogger>();

                // Create Accounts Receivable messages
                context.Instance.AccountsReceivableSegmentationBatchId = segmentationDailyProcessService.GenerateSegmentationBatchId(SegmentationTypeEnum.AccountsReceivable);
                this._metricsProvider.Value.Increment(Metrics.Increment.Scoring.SegmentationAccountsReceivableBatchId, context.Instance.AccountsReceivableSegmentationBatchId);
                this.LogStartTime(scoringSagaLogger, context.Instance.ScoringBatchId, context.Instance.CurrentState, $"{context.Event.Name}-ExecuteAccountsReceivableSegmentation", context.Instance.CorrelationId, context.Data.ProcessId);
                segmentationDailyProcessService.ExecuteAccountsReceivableSegmentation(context.Instance.AccountsReceivableSegmentationBatchId);
                this.LogEndTime(scoringSagaLogger, context.Instance.ScoringBatchId, context.Instance.CurrentState, $"{context.Event.Name}-ExecuteAccountsReceivableSegmentation", context.Instance.CorrelationId, context.Data.ProcessId);
                this.LogStartTime(scoringSagaLogger, context.Instance.ScoringBatchId, context.Instance.CurrentState, $"{context.Event.Name}-ProcessAccountsReceivableVisitDaily", context.Instance.CorrelationId, context.Data.ProcessId);
                IList<SegmentationAccountsReceivableDto> accountsReceivableDailyVisits;
                int lastHsGuarantorId = 0;
                int count = 0;
                do
                {
                    this.LogStartTime(scoringSagaLogger, context.Instance.ScoringBatchId, context.Instance.CurrentState, $"{context.Event.Name}-ProcessAccountsReceivableVisitDaily-{count}", context.Instance.CorrelationId, context.Data.ProcessId);
                    accountsReceivableDailyVisits = segmentationDailyProcessService.ProcessAccountsReceivableVisitDaily(context.Instance.AccountsReceivableSegmentationBatchId,lastHsGuarantorId);
                    this.LogEndTime(scoringSagaLogger, context.Instance.ScoringBatchId, context.Instance.CurrentState, $"{context.Event.Name}-ProcessAccountsReceivableVisitDaily-{count}", context.Instance.CorrelationId, context.Data.ProcessId);
                    if (accountsReceivableDailyVisits.IsNotNullOrEmpty())
                    {
                        context.Publish(new SegmentationAccountsReceivableRequestMessage
                        {
                            AccountsReceivableDtos = accountsReceivableDailyVisits.ToList(),
                            CorrelationId = context.Instance.CorrelationId,
                            ProcessId = new Guid(NewId.Next().ToByteArray()),
                            SegmentationEnable = context.Data.SegmentationEnable,
                            BatchId = context.Instance.AccountsReceivableSegmentationBatchId
                        });
                        count += 1;
                        lastHsGuarantorId = accountsReceivableDailyVisits.Max(x => x.HsGuarantorId);
                    }
                } while (accountsReceivableDailyVisits.IsNotNullOrEmpty());
                this.LogEndTime(scoringSagaLogger, context.Instance.ScoringBatchId, context.Instance.CurrentState, $"{context.Event.Name}-ProcessAccountsReceivableVisitDaily", context.Instance.CorrelationId, context.Data.ProcessId);

                return count;
            }
        }

        private int GenerateActivePassiveMessages(BehaviorContext<ScoringAndSegmentationSaga, SegmentationAccountsReceivableFinishedMessage> context)
        {
            using (ILifetimeScope lifetimeScope = IvinciContainer.Instance.Container().BeginLifetimeScope())
            {
                ISegmentationDailyProcessService segmentationDailyProcessService = lifetimeScope.Resolve<ISegmentationDailyProcessService>();
                IScoringSagaLogger scoringSagaLogger = lifetimeScope.Resolve<IScoringSagaLogger>();

                // Create Active Passive messages
                context.Instance.ActivePassiveSegmentationBatchId = segmentationDailyProcessService.GenerateSegmentationBatchId(SegmentationTypeEnum.ActivePassive);
                this._metricsProvider.Value.Increment(Metrics.Increment.Scoring.SegmentationActivePassiveBatchId, context.Instance.ActivePassiveSegmentationBatchId);
                this.LogStartTime(scoringSagaLogger, context.Instance.ScoringBatchId, context.Instance.CurrentState, $"{context.Event.Name}-ExecuteActivePassiveSegmentation", context.Instance.CorrelationId, context.Data.ProcessId);
                segmentationDailyProcessService.ExecuteActivePassiveSegmentation(context.Instance.ActivePassiveSegmentationBatchId);
                this.LogEndTime(scoringSagaLogger, context.Instance.ScoringBatchId, context.Instance.CurrentState, $"{context.Event.Name}-ExecuteActivePassiveSegmentation", context.Instance.CorrelationId, context.Data.ProcessId);
                this.LogStartTime(scoringSagaLogger, context.Instance.ScoringBatchId, context.Instance.CurrentState, $"{context.Event.Name}-ProcessActivePassiveVisitDaily", context.Instance.CorrelationId, context.Data.ProcessId);
                IList<SegmentationActivePassiveDto> activePassiveDailyVisits;
                int lastHsGuarantorId = 0;
                int count = 0;
                do
                {
                    this.LogStartTime(scoringSagaLogger, context.Instance.ScoringBatchId, context.Instance.CurrentState, $"{context.Event.Name}-ProcessActivePassiveVisitDaily-{count}", context.Instance.CorrelationId, context.Data.ProcessId);
                    activePassiveDailyVisits = segmentationDailyProcessService.ProcessActivePassiveVisitDaily(context.Instance.ActivePassiveSegmentationBatchId, lastHsGuarantorId);
                    this.LogEndTime(scoringSagaLogger, context.Instance.ScoringBatchId, context.Instance.CurrentState, $"{context.Event.Name}-ProcessActivePassiveVisitDaily-{count}", context.Instance.CorrelationId, context.Data.ProcessId);
                    if (activePassiveDailyVisits.IsNotNullOrEmpty())
                    {
                        context.Publish(new SegmentationActivePassiveRequestMessage
                        {
                            SegmentationActivePassiveDtos = activePassiveDailyVisits.ToList(),
                            CorrelationId = context.Instance.CorrelationId,
                            ProcessId = new Guid(NewId.Next().ToByteArray()),
                            SegmentationEnable = context.Data.SegmentationEnable,
                            BatchId = context.Instance.ActivePassiveSegmentationBatchId
                        });
                        count += 1;
                        lastHsGuarantorId = activePassiveDailyVisits.Max(x => x.HsGuarantorId);
                    }
                } while (activePassiveDailyVisits.IsNotNullOrEmpty());
                this.LogEndTime(scoringSagaLogger, context.Instance.ScoringBatchId, context.Instance.CurrentState, $"{context.Event.Name}-ProcessActivePassiveVisitDaily", context.Instance.CorrelationId, context.Data.ProcessId);

                return count;
            }
        }

        private async Task PublishPresumptiveCharityMessages(BehaviorContext<ScoringAndSegmentationSaga, SegmentationPresumptiveCharityStartingMessage> context)
        {
            using (ILifetimeScope lifetimeScope = IvinciContainer.Instance.Container().BeginLifetimeScope())
            {
                ISegmentationDailyProcessService segmentationDailyProcessService = lifetimeScope.Resolve<ISegmentationDailyProcessService>();
                context.Instance.PresumptiveCharitySegmentationEventTotal = 1;
                this._metricsProvider.Value.Increment(Metrics.Increment.Scoring.SegmentationPresumptiveCharityTotal);
                context.Instance.PresumptiveCharitySegmentationBatchId = segmentationDailyProcessService.GenerateSegmentationBatchId(SegmentationTypeEnum.PresumptiveCharity);
                this._metricsProvider.Value.Increment(Metrics.Increment.Scoring.SegmentationPresumptiveCharityBatchId, context.Instance.PresumptiveCharitySegmentationBatchId);
                this._segmentationPresumptiveCharityService.Value.SegmentationTotal = context.Instance.PresumptiveCharitySegmentationEventTotal;
                this._segmentationPresumptiveCharityService.Value.SegmentationBatchId = context.Instance.PresumptiveCharitySegmentationBatchId;
                await context.Publish(new SegmentationPresumptiveCharityRequestMessage
                {
                    CorrelationId = context.Instance.CorrelationId,
                    ProcessId = new Guid(NewId.Next().ToByteArray()),
                    SegmentationEnable = context.Data.SegmentationEnable,
                    BatchId = context.Instance.PresumptiveCharitySegmentationBatchId
                });
            }
        }

    }

}

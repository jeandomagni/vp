﻿namespace Ivh.Console.ScoringProcessor.Saga
{
    using System;
    using Autofac;
    using Automatonymous;
    using Common.DependencyInjection;
    using Domain.HospitalData.Scoring.Entities;
    using Domain.HospitalData.Scoring.Interfaces;

    public class ScoringAndSegmentationSaga : SagaStateMachineInstance
    {
        public Guid CorrelationId { get; set; }
        public int Version { get; set; }
        public int ScoringBatchId { get; set; }
        public int BadDebtSegmentationBatchId { get; set; }
        public int AccountsReceivableSegmentationBatchId { get; set; }
        public int ActivePassiveSegmentationBatchId { get; set; }
        public int PresumptiveCharitySegmentationBatchId { get; set; }
        public DateTime Timestamp { get; set; }
        public int ScoringEventTotal { get; set; }
        public int ScoringEventSuccesses { get; set; }
        public int ScoringEventFailures { get; set; }
        public int BadDebtSegmentationEventTotal { get; set; }
        public int BadDebtSegmentationEventSuccesses { get; set; }
        public int BadDebtSegmentationEventFailures { get; set; }
        public int AccountReceivableSegmentationEventTotal { get; set; }
        public int AccountReceivableSegmentationEventSuccesses { get; set; }
        public int AccountReceivableSegmentationEventFailures { get; set; }
        public int ActivePassiveSegmentationEventTotal { get; set; }
        public int ActivePassiveSegmentationEventSuccesses { get; set; }
        public int PresumptiveCharitySegmentationEventFailures { get; set; }
        public int PresumptiveCharitySegmentationEventTotal { get; set; }
        public int PresumptiveCharitySegmentationEventSuccesses { get; set; }
        public int ActivePassiveSegmentationEventFailures { get; set; }
        public string CurrentState { get; set; }

        public bool IsScoringComplete()
        {
            return (this.ScoringEventFailures + this.ScoringEventSuccesses) == this.ScoringEventTotal;
        }

        public bool IsScoringSuccessful()
        {
            return this.ScoringEventSuccesses > this.ScoringEventFailures;
        }

        public bool IsBadDebtSegmentationComplete()
        {
            return (this.BadDebtSegmentationEventFailures + this.BadDebtSegmentationEventSuccesses) == this.BadDebtSegmentationEventTotal;
        }
        public bool IsAccountReceivableSegmentationComplete()
        {
            return (this.AccountReceivableSegmentationEventFailures + this.AccountReceivableSegmentationEventSuccesses) == this.AccountReceivableSegmentationEventTotal;
        }

        public bool IsActivePassiveSegmentationComplete()
        {
            return (this.ActivePassiveSegmentationEventFailures + this.ActivePassiveSegmentationEventSuccesses) == this.ActivePassiveSegmentationEventTotal;
        }

        public bool IsValidEvent(int scoringBatchId, string currentState, string eventName, Guid correlationId, Guid processId)
        {
            using (ILifetimeScope lifetimeScope = IvinciContainer.Instance.Container().BeginLifetimeScope())
            {
                IScoringSagaLogger scoringSagaLogger = lifetimeScope.Resolve<IScoringSagaLogger>();
                ScoringSagaLog scoringSagaLog = scoringSagaLogger.GetScoringSagaLog(scoringBatchId, currentState, eventName, correlationId, processId);
                return scoringSagaLog == null;
            }
        }

    }
}

﻿namespace Ivh.Console.ScoringProcessor.SetupIoc
{
    using Autofac;
    using Common.Base.Interfaces;
    using Common.DependencyInjection;
    public class RegisterProcessorConsole : IRegistrationModule
    {
        public virtual void Register(ContainerBuilder builder)
        {
            builder.RegisterType<ScoringProcessorWindowService>().As<ITopShelfService>();
        }
    }
}
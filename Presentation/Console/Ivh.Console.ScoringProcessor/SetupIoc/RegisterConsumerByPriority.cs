﻿namespace Ivh.Console.ScoringProcessor.SetupIoc
{
    using System;
    using System.Linq;
    using System.Reflection;
    using Autofac;
    using Automatonymous;
    using Common.Configuration;
    using Common.Data.Services;
    using Common.DependencyInjection;
    using Common.DependencyInjection.Registration;
    using Common.DependencyInjection.Registration.Register;
    using Common.VisitPay.Enums;
    using Consumers;
    using GreenPipes;
    using MassTransit;
    using MassTransit.AutomatonymousAutofacIntegration;
    using MassTransit.NHibernateIntegration;
    using MassTransit.NHibernateIntegration.Saga;
    using MassTransit.Saga;
    using NHibernate;
    using NHibernate.Mapping.ByCode.Conformist;
    using Saga;

    public class RegisterConsumerByPriority : RegisterProcessorConsole, IRegistrationModule
    {
        private const string NameSpace = "Ivh.Console.ScoringProcessor.Consumers";
        private string _priority { get; }
        private readonly string _fullNameSpace = $"{NameSpace}";

        public RegisterConsumerByPriority(ConsumerMessagePriorityEnum priority)
        {
            this._priority = priority.ToString();
            if (priority != ConsumerMessagePriorityEnum.All)
            {
                this._fullNameSpace = $"{this._fullNameSpace}.{this._priority}";
            }
        }

        public override void Register(ContainerBuilder builder)
        {
            builder.RegisterAssemblyTypes(Assembly.GetExecutingAssembly())
                .Where(t => t.IsInNamespace(this._fullNameSpace) && t.IsAssignableTo<IConsumer>())
                .AsImplementedInterfaces();

            RegistrationSettings registrationSettings = RegistrationSettingsService.GetRegistrationSettings();
            Base.Register(builder, registrationSettings, false);
            VisitPay.Register(builder, registrationSettings, true);
            Data.Register(builder, registrationSettings)
                .AddVisitPayDatabase()
                .AddCdiEtlDatabase()
                .AddEnterpriseDatabase()
                .AddStorageDatabase();

            builder.RegisterType<ScoringAndSegmentationStateMachine>();
            builder.RegisterType<NHibernateSagaRepository<ScoringAndSegmentationSaga>>().As<ISagaRepository<ScoringAndSegmentationSaga>>();
            builder.RegisterType<AutofacStateMachineActivityFactory>().As<IStateMachineActivityFactory>().InstancePerLifetimeScope();
            builder.RegisterGeneric(typeof(NHibernateSagaRepository<>)).As(typeof(ISagaRepository<>));
            Assembly mappingsAssembly = Assembly.GetExecutingAssembly();
            builder.RegisterStateMachineSagas(mappingsAssembly);
            Type[] mappings = mappingsAssembly
                .GetTypes()
                .Where(t => t.BaseType != null && t.BaseType.IsGenericType &&
                            (t.BaseType.GetGenericTypeDefinition() == typeof(SagaClassMapping<>) ||
                             t.BaseType.GetGenericTypeDefinition() == typeof(ClassMapping<>)))
                .ToArray();
            builder.Register(c => new SqlServerSessionFactoryProvider(new ConnectionStringService().CdiEtlConnection.Value, mappings).GetSessionFactory())
                .As<ISessionFactory>()
                .SingleInstance();

            int concurrencyLimit = int.Parse(AppSettingsProvider.Instance.Get("Scoring.ConcurrencyLimit") ?? "1");
            builder.Register(c => ServiceBus.Register(builder, registrationSettings, this._priority, c, concurrencyLimit, (configurator, host, queue) =>
            {
                configurator.ReceiveEndpoint(host, $"{queue}-Scoring", e =>
                {
                    e.UseInMemoryOutbox();
                    e.PrefetchCount = 8;
                    e.UseRetry(r => r.Immediate(3));
                    e.UseConcurrencyLimit(concurrencyLimit);
                    e.StateMachineSaga(c.Resolve<ScoringAndSegmentationStateMachine>(), c.Resolve<ISagaRepository<ScoringAndSegmentationSaga>>());

                    // This is for the systemhealthmonitoringpage
                    e.Consumer<ProcessorHealthConsumer>();
                });
                return true;
            })).SingleInstance().As<IBusControl>().As<IBus>();
            builder.RegisterType<Common.ServiceBus.Bus>().As<Common.ServiceBus.Interfaces.IBus>();

            base.Register(builder);
        }
    }
}
﻿namespace Ivh.Console.MetricQuery
{
    using System;
    using System.Threading;
    using System.Threading.Tasks;
    using Autofac;
    using Common.Base.Enums;
    using Common.DependencyInjection;
    using Common.DependencyInjection.Registration;
    using Common.JobRunner.Common.Interfaces;
    using Common.ServiceBus.Constants;
    using Common.VisitPay.Enums;
    using Domain.Settings.Interfaces;
    using MassTransit;
    using SetupIoc;
    using Console = System.Console;

    class Program
    {
        private static IContainer _iocContainer;
        private static string _jobRunnerGuid;

        static void Main(string[] args)
        {
            if (args.Length == 0)
            {
                PrintWelcomeMessage();
            }
            else
            {
                MetricQueryEnum option = MetricQueryEnum.Unknown;
                option = Enum.TryParse(args[0], true, out option) ? option : MetricQueryEnum.Unknown;
                Console.WriteLine("Parsed: {0}", args[0]);
                ExecuteAction(option, args);
            }
        }

        private static void ExecuteAction(MetricQueryEnum type, string[] args)
        {
            _iocContainer = SetupIoc();
            _jobRunnerGuid = _iocContainer.Resolve<IApplicationSettingsService>().ProgramUniqueGuid.Value.ToString();
            if (type != MetricQueryEnum.Unknown)
            {
                bool ownsMutex;
                using (Mutex mutex = new Mutex(true, _jobRunnerGuid + type.ToString(), out ownsMutex))
                {
                    if (ownsMutex)
                    {
                        using (IJobRunner jobRunner = _iocContainer.ResolveKeyed<IJobRunner>(type))
                        {
                            IBusControl busControl = _iocContainer.Resolve<IBusControl>();
                            try
                            {
                                busControl.Start();
                                jobRunner.Execute(args);
                            }
                            catch (Exception ex)
                            {
                                Environment.ExitCode = ex.HResult != 0 ? ex.HResult : -1;
                                throw;
                            }
                            finally
                            {
                                busControl?.Stop(TimeSpan.FromSeconds(ServiceBus.StopTimeoutSeconds));
                            }
                        }
                    }
                    else
                    {
                        Console.WriteLine("Couldn't acquire mutex, is the program already running?");
                    }
                }
            }
            else
            {
                PrintWelcomeMessage();
            }

        }


        private static IContainer SetupIoc()
        {
            Task mappingTasks = Task.Run(() =>
            {
                ServiceBusMessageSerializationMappingBase.ConfigureMappings();
                using (MappingBuilder mappingBuilder = new MappingBuilder())
                {
                    mappingBuilder.WithBase();
                }
            });

            IvinciContainer.Instance.RegisterComponent(new RegisterMetricQueryConsole());
            mappingTasks.Wait();
            return IvinciContainer.Instance.Container();
        }


        private static void PrintWelcomeMessage()
        {
            Console.WriteLine("Ivh.Console.MetricQuery --  Pushes data into Metric!");
            Console.WriteLine("Ivh.Console.MetricQuery.exe <option>");

            Console.WriteLine("Options:");
            foreach (MetricQueryEnum option in Enum.GetValues(typeof(MetricQueryEnum)))
            {
                Console.WriteLine("{0}", option);
            }
            Console.WriteLine("      _ ");
            Console.WriteLine("     /  \\");
            Console.WriteLine("    /|oo \\");
            Console.WriteLine("   (_|  /_) ");
            Console.WriteLine("    `@/  \\  ");
            Console.WriteLine("    |     \\   \\\\");
            Console.WriteLine("     \\||   \\   ))");
            Console.WriteLine("     |||\\ /  \\//");
            Console.WriteLine("   _//|| _\\   / ");
            Console.WriteLine("  (_/(_|(____/");
            Console.WriteLine("---");

            Console.WriteLine();
        }
    }
}

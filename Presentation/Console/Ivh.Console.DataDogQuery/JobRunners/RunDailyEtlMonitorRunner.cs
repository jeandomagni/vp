﻿﻿namespace Ivh.Console.MetricQuery.JobRunners
 {
     using System;
     using Application.Logging.Common.Interfaces;
     using Common.Base.Utilities.Helpers;
     using Common.JobRunner.Common.Interfaces;

     public class RunDailyEtlMonitorRunner : IJobRunner
     {
         private readonly Lazy<ILoggingApplicationService> _logger;

         public RunDailyEtlMonitorRunner(
             Lazy<ILoggingApplicationService> logger)
         {
             this._logger = logger;
         }

         public void Execute(string[] args = null)
         {
             try
             {
                 this._logger.Value.Info(() => "RunDailyEtlMonitorRunner::Execute Start");
                 this._logger.Value.Info(() => "RunDailyEtlMonitorRunner::Execute End");
             }
             catch (Exception e)
             {
                 this._logger.Value.Fatal(() => $"RunDailyEtlMonitorRunner::Execute - exception = {ExceptionHelper.AggregateExceptionToString(e)}");
                 throw;
             }
         }


         public void Dispose()
         {
         }
     }
 }
﻿namespace Ivh.Console.MetricQuery.JobRunners
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Application.Logging.Common.Interfaces;
    using Application.Monitoring.Common.Interfaces;
    using Common.Base.Utilities.Helpers;
    using Common.JobRunner.Common.Interfaces;
    using Common.VisitPay.Enums;
    using Domain.Monitoring.Entities;

    public class RunEvery10AppMonitorRunner : IJobRunner
    {
        private readonly Lazy<IPaymentMonitoringApplicationService> _paymentMonitoringApplicationService;
        private readonly Lazy<IEmailMonitoringApplicationService> _emailMonitoringApplicationService;
        private readonly Lazy<IClientDataIntegrationMonitoringApplicationService> _clientDataIntegrationMonitoringApplicationService;
        private readonly Lazy<IMonitoringApplicationService> _monitoringApplicationService;
        private readonly Lazy<ILoggingApplicationService> _logger;

        private readonly IList<Exception> _exceptions = new List<Exception>();

        public RunEvery10AppMonitorRunner(
            Lazy<IPaymentMonitoringApplicationService> paymentMonitoringApplicationService,
            Lazy<IEmailMonitoringApplicationService> emailMonitoringApplicationService,
            Lazy<IClientDataIntegrationMonitoringApplicationService> clientDataIntegrationMonitoringApplicationService,
            Lazy<IMonitoringApplicationService> monitoringApplicationService,
            Lazy<ILoggingApplicationService> logger)
        {
            this._paymentMonitoringApplicationService = paymentMonitoringApplicationService;
            this._emailMonitoringApplicationService = emailMonitoringApplicationService;
            this._clientDataIntegrationMonitoringApplicationService = clientDataIntegrationMonitoringApplicationService;
            this._logger = logger;
            this._monitoringApplicationService = monitoringApplicationService;
        }

        public void Execute(string[] args = null)
        {
            try
            {
                this._logger.Value.Info(() => "RunEvery10AppMonitorRunner::Execute Start");
                this.DataLoadPayments();
                this.DataLoadEmails();
                this.DataLoadClientDataIntegration();
                this.DataLoadMiscellaneous();
                this._logger.Value.Info(() => "RunEvery10AppMonitorRunner::Execute End");

                if (this._exceptions.Any())
                {
                    throw new AggregateException(this._exceptions);
                }
            }
            catch (Exception e)
            {
                this._logger.Value.Fatal(() => string.Format("RunEvery10AppMonitorRunner::Execute - exception = {0}", ExceptionHelper.AggregateExceptionToString(e)));
                throw;
            }
        }

        private void DataLoadEmails()
        {
            this.Run(() => this._emailMonitoringApplicationService.Value.SendEmailPastDueExpectedVsActual());
            this.Run(() => this._emailMonitoringApplicationService.Value.SendEmailFinalPastDueExpectedVsActual());
            this.Run(() => this._emailMonitoringApplicationService.Value.SendEmailPaymentConfirmationExpectedVsActual());
            this.Run(() => this._emailMonitoringApplicationService.Value.SendEmailPaymentDueDateChangeExpectedVsActual());
            this.Run(() => this._emailMonitoringApplicationService.Value.SendEmailStatementNotificationExpectedVsActual());
            this.Run(() => this._emailMonitoringApplicationService.Value.SendEmailUncollectableExpectedVsActual());
            this.Run(() => this._emailMonitoringApplicationService.Value.SendEmailFailedPaymentsExpectedVsActual());
            this.Run(() => this._emailMonitoringApplicationService.Value.SendEmailAccountCancellation());
            this.Run(() => this._emailMonitoringApplicationService.Value.SendEmailFinancePlanDurationIncrease());
        }

        private void DataLoadPayments()
        {
            this.Run(() => this._paymentMonitoringApplicationService.Value.SendACHClosedFailedActualVsExpected());
            this.Run(() => this._paymentMonitoringApplicationService.Value.SendACHClosedPaidActualVsExpected());
            this.Run(() => this._paymentMonitoringApplicationService.Value.SendACHReturnReportCount());
            this.Run(() => this._paymentMonitoringApplicationService.Value.SendACHSettlementReportCount());
        }

        private void DataLoadClientDataIntegration()
        {
            this.Run(() => this._clientDataIntegrationMonitoringApplicationService.Value.SendPlacementActualVsExpected());
        }

        private void DataLoadMiscellaneous()
        {
            this.Run(() =>
            {
                IList<DatumEnum> data = new List<DatumEnum>()
                    {
                        DatumEnum.HSAccountDeltasRecordCount,
                        DatumEnum.VisitPayTransactionsWithUnmappedTransactionCount,
                        DatumEnum.UnmappedTransactionCodesCount,
                        DatumEnum.GuarantorChangeSetRecordCount,
                        DatumEnum.VisitChangeSetRecordCount,
                        DatumEnum.VisitTransactionChangeSetRecordCount,
                        DatumEnum.ChangeEventRecordType2Count,
                        DatumEnum.ChangeEventRecordType3Count,
                        DatumEnum.ChangeEventRecordType4Count,
                        DatumEnum.ChangeEventRecordType5Count,
                        DatumEnum.ChangeEventRecordType6Count,
                        DatumEnum.ChangeEventRecordType7Count,
                        DatumEnum.ActiveUncollectableVisitsExpected,
                        DatumEnum.ActiveUncollectableVisitsActual,
                        DatumEnum.ActiveUncollectableFinancePlansExpected,
                        DatumEnum.ActiveUncollectableFinancePlansActual,
                        DatumEnum.ClosedUncollectableVisitsActual,
                        DatumEnum.ClosedUncollectableVisitsExpected,
                        DatumEnum.ScheduledPaymentsExpected,
                        DatumEnum.ScheduledPaymentsActual,
                        DatumEnum.StatementLineMismatchCount,
                        DatumEnum.StatementFPLineVsFPBalanceMismatchCount,
                        DatumEnum.PaymentActual,
                        DatumEnum.VpGuarantorIdDifferencesCount,
                        DatumEnum.StatementCountActual,
                        DatumEnum.FinancePlansOriginatedExpected,
                        DatumEnum.FinancePlansOriginatedActual
                    };
                this._monitoringApplicationService.Value.PublishData(data, DateTime.UtcNow, DateTime.UtcNow);
            });
        }

        private void Run(Action action)
        {
            try
            {
                action();
            }
            catch (Exception ex)
            {
                this._exceptions.Add(ex);
            }
        }

        public void Dispose()
        {
        }
    }
}
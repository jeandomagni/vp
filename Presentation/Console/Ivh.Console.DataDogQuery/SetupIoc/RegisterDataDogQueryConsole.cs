﻿namespace Ivh.Console.MetricQuery.SetupIoc
{
    using Autofac;
    using Common.DependencyInjection;
    using Common.DependencyInjection.Registration;
    using Common.DependencyInjection.Registration.Register;
    using Common.JobRunner.Common.Interfaces;
    using Common.VisitPay.Enums;
    using JobRunners;

    public class RegisterMetricQueryConsole : IRegistrationModule
    {
        public void Register(ContainerBuilder builder)
        {
            RegistrationSettings registrationSettings = RegistrationSettingsService.GetRegistrationSettings();
            Base.Register(builder, registrationSettings);
            VisitPay.Register(builder, registrationSettings, true);
            Data.Register(builder, registrationSettings)
                .AddEnterpriseDatabase();
            
            builder.RegisterType<RunDailyEtlMonitorRunner>().Keyed<IJobRunner>(MetricQueryEnum.RunDailyEtl);
            builder.RegisterType<RunEvery10AppMonitorRunner>().Keyed<IJobRunner>(MetricQueryEnum.RunEvery10App);
        }
    }
}
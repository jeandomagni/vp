args = commandArgs(TRUE) 
if (!require("jsonlite"))
{
	install.packages("jsonlite",repos = "http://cran.us.r-project.org")
}
if (!require("gbm"))
{
	install.packages("gbm",repos = "http://cran.us.r-project.org")
}
library(jsonlite)
jsoncon <- url(args[3])
inp <- fromJSON(jsoncon)
dataUrls = strsplit(args[2], ",")[[1]]
for(dataUrl in dataUrls){
	finalUrl <- paste(dataUrl,args[4],sep="")
	con <- url(finalUrl)	
	load(file=con)
}
source(args[1])

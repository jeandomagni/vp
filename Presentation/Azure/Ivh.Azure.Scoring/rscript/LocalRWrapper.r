args = commandArgs() 
if (!require("jsonlite"))
{
	install.packages("jsonlite",repos = "http://cran.us.r-project.org")
}
if (!require("gbm"))
{
	install.packages("gbm",repos = "http://cran.us.r-project.org")
}
library(jsonlite)
inp <- fromJSON(args[5])
sasKey <- args[4]
dataUrls = strsplit(args[3], ",")[[1]]
for(dataUrl in dataUrls){
	if(is.na(sasKey) || sasKey == '')
	{
		load(file=dataUrl)
	}
	else
	{
		finalUrl <- paste(dataUrl,args[4],sep="")
		con <- url(finalUrl)	
		load(file=con)
	}
}
source(args[2])

namespace Ivh.Azure.Scoring
{
    using System;
    using System.IO;
    using Newtonsoft.Json;
    using System.Linq;
    using System.Net;
    using System.Net.Http;
    using System.Threading.Tasks;
    using Application.HospitalData.Common.Dtos;
    using Common.Azure;
    using Microsoft.Azure.WebJobs;
    using Microsoft.Azure.WebJobs.Extensions.Http;
    using Microsoft.Azure.WebJobs.Host;

    public class GuarantorPtpScore : BaseFunction
    {
        [FunctionName("GuarantorPtpScore")]
        public static async Task<HttpResponseMessage> Run([HttpTrigger(AuthorizationLevel.Function, "get", "post", Route = null)]HttpRequestMessage req, ExecutionContext context, TraceWriter log)
        {
            log.Info("GuarantorPtpScore Function processed a request");

            // parse query parameter
            string endPointJson = req.GetQueryNameValuePairs().FirstOrDefault(q => String.Compare(q.Key, "EndPoint", StringComparison.OrdinalIgnoreCase) == 0).Value;
            string rawInputJson = req.GetQueryNameValuePairs().FirstOrDefault(q => String.Compare(q.Key, "Input", StringComparison.OrdinalIgnoreCase) == 0).Value;
            string rscriptJson = req.GetQueryNameValuePairs().FirstOrDefault(q => String.Compare(q.Key, "Rscript", StringComparison.OrdinalIgnoreCase) == 0).Value;

            // Get request body
            dynamic input = await req.Content.ReadAsAsync<object>();
            dynamic data = JsonConvert.DeserializeObject(input.ToString());

            try
            {
                // Set name to query string or body data
                dynamic endPoint = string.IsNullOrEmpty(endPointJson) ? data?.EndPoint : JsonConvert.DeserializeObject(endPointJson);
                dynamic rawInput = string.IsNullOrEmpty(rawInputJson) ? data?.Input : JsonConvert.DeserializeObject(rawInputJson);
                dynamic rscript = string.IsNullOrEmpty(rscriptJson) ? data?.Rscript : JsonConvert.DeserializeObject(rscriptJson);
                if (endPoint == null || rawInput == null || rscript == null)
                {
                    log.Info("Invalid parameters");
                    return req.CreateErrorResponse(HttpStatusCode.BadRequest, "Invalid parameters. Endpoint, input and/or Rscript information are missing.");
                }
                ResolveServices(endPoint);
                string rwrapperPath = Path.Combine(context.FunctionAppDirectory, @"rscript\rwrapper.r");
                LoggingService.Info($"RWrapper: {rwrapperPath}");
                ScoringApplicationService.GeneratePtpScore(rawInput.ToString(), new ScoringScriptStorage
                {
                    SourceUrl = (string)rscript.SourceUrl,
                    AuthorizationCode = (string)rscript.AuthorizationCode,
                    RdataUrls = (string)rscript.DataUrls,
                    StorageAccount = (string)rscript.StorageAccount,
                    ContainerName = (string)rscript.ContainerName,
                    SasKey = (string)rscript.SasKey,
                    RwrapperPath = rwrapperPath
                });
            }
            catch (Exception e)
            {
                log.Info(e.Message);
                LoggingService.Fatal(e.Message);
                return req.CreateErrorResponse(HttpStatusCode.BadRequest, $"Error encountered: {e.Message}");
            }

            log.Info("GuarantorPtpScore Function successfully processed a request.");
            return req.CreateResponse(HttpStatusCode.OK);
        }
    }
}

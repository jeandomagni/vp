﻿using Ivh.Web.Client;
using Microsoft.Owin;

[assembly: OwinStartup(typeof (Startup))]

namespace Ivh.Web.Client
{
    using Owin;

    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            this.ConfigureIoc(app);
            this.ConfigureAuth(app);
            this.ConfigureMisc(app);
        }
    }
}
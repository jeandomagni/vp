﻿namespace Ivh.Web.Client.Services
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web;
    using System.Web.Mvc;
    using Application.Base.Common.Dtos;
    using Application.Core.Common.Dtos;
    using Application.Core.Common.Interfaces;
    using Application.FinanceManagement.Common.Interfaces;
    using Application.User.Common.Dtos;
    using AutoMapper;
    using Common.Base.Enums;
    using Common.EventJournal;
    using Common.Session;
    using Common.VisitPay.Enums;

    public class GuarantorFilterService
    {
        private readonly Lazy<IWebClientSessionFacade> _sessionFacade;
        private readonly Lazy<IConsolidationGuarantorApplicationService> _consolidationGuarantorApplicationService;
        private readonly Lazy<IGuarantorApplicationService> _guarantorApplicationService;
        private readonly Lazy<IVisitPayUserApplicationService> _visitPayUserApplicationService;
        private readonly Lazy<IVisitPayUserJournalEventApplicationService> _visitPayUserJournalEventApplicationService;

        // <CurrentVisitPayUserId, FilteredVpGuarantorId>
        private IDictionary<int, int> _vpGuarantorId;

        public GuarantorFilterService(
            Lazy<IWebClientSessionFacade> sessionFacade,
            Lazy<IConsolidationGuarantorApplicationService> consolidationGuarantorApplicationService,
            Lazy<IGuarantorApplicationService> guarantorApplicationService,
            Lazy<IVisitPayUserApplicationService> visitPayUserApplicationService,
            Lazy<IVisitPayUserJournalEventApplicationService> visitPayUserJournalEventApplicationService)
        {
            this._sessionFacade = sessionFacade;
            this._consolidationGuarantorApplicationService = consolidationGuarantorApplicationService;
            this._guarantorApplicationService = guarantorApplicationService;
            this._visitPayUserApplicationService = visitPayUserApplicationService;
            this._visitPayUserJournalEventApplicationService = visitPayUserJournalEventApplicationService;
        }

        public KeyValuePair<int, int> VpGuarantorId(int currentVisitPayUserId)
        {
            if (this._vpGuarantorId != null && this._vpGuarantorId.ContainsKey(currentVisitPayUserId))
                return this._vpGuarantorId.First(x => x.Key == currentVisitPayUserId);

            if (this._vpGuarantorId == null)
                this._vpGuarantorId = this._sessionFacade.Value.FilteredVpGuarantorId;

            return this._vpGuarantorId.ContainsKey(currentVisitPayUserId) ?
                this._vpGuarantorId.First(x => x.Key == currentVisitPayUserId) :
                new KeyValuePair<int, int>(currentVisitPayUserId, this._visitPayUserApplicationService.Value.GetGuarantorIdFromVisitPayUserId(currentVisitPayUserId));
        }

        public bool IsManagedViewingManaged(int currentVisitPayUserId, int filteredVisitPayUserId)
        {
            int filteredVpGuarantorId = this._visitPayUserApplicationService.Value.GetGuarantorIdFromVisitPayUserId(filteredVisitPayUserId);

            if (currentVisitPayUserId != filteredVisitPayUserId) 
                return false;

            GuarantorDto guarantor = this._guarantorApplicationService.Value.GetGuarantor(filteredVpGuarantorId);
            return guarantor.ConsolidationStatus == GuarantorConsolidationStatusEnum.Managed;
        }

        public void SetGuarantor(int currentVisitPayUserId, int filteredVpGuarantorId, int clientVisitPayUserId)
        {
            this._sessionFacade.Value.SetFilteredVpGuarantorId(currentVisitPayUserId, filteredVpGuarantorId);
            this._vpGuarantorId = null;

            int currentVpGuarantorId = this._visitPayUserApplicationService.Value.GetGuarantorIdFromVisitPayUserId(currentVisitPayUserId);
            if (currentVpGuarantorId != filteredVpGuarantorId)
                this._visitPayUserJournalEventApplicationService.Value.LogClientViewManaged(clientVisitPayUserId, filteredVpGuarantorId, currentVpGuarantorId, Mapper.Map<JournalEventHttpContextDto>(HttpContext.Current));
        }

        public IList<SelectListItem> GetFilterGuarantors(int currentVisitPayUserId)
        {
            GuarantorDto currentGuarantor = this._visitPayUserApplicationService.Value.GetGuarantorFromVisitPayUserId(currentVisitPayUserId);
            IList<GuarantorDto> guarantors = this.GetManagedGuarantors(currentGuarantor);
            if (!guarantors.Any())
                return new List<SelectListItem>();

            List<SelectListItem> items = new List<SelectListItem>();

            guarantors.Insert(0, currentGuarantor);

            guarantors.ToList().ForEach(guarantor =>
            {
                items.Add(new SelectListItem
                {
                    Selected = guarantor.VpGuarantorId == this.VpGuarantorId(currentVisitPayUserId).Value,
                    Text = guarantor.User.DisplayFirstNameLastName,
                    Value = string.Format("{0},{1},{2}", currentVisitPayUserId, guarantor.User.VisitPayUserId, guarantor.VpGuarantorId.ToString())
                });
            });

            return items;
        }

        public void ClearSelection(int currentVisitPayUserId)
        {
            this._sessionFacade.Value.SetFilteredVpGuarantorId(currentVisitPayUserId, null);
        }

        private IList<GuarantorDto> GetManagedGuarantors(GuarantorDto currentGuarantor)
        {
            if (currentGuarantor.ConsolidationStatus != GuarantorConsolidationStatusEnum.Managing || !currentGuarantor.ManagedGuarantorIds.Any())
                return new List<GuarantorDto>();

            return this._consolidationGuarantorApplicationService.Value.GetAllManagedGuarantors(currentGuarantor.User.VisitPayUserId)
                .Where(x => x.ConsolidationGuarantorStatus == ConsolidationGuarantorStatusEnum.Accepted)
                .Select(x => x.ManagedGuarantor)
                .OrderBy(x => x.User.DisplayFirstNameLastName)
                .ToList();
        }
    }
}
﻿namespace Ivh.Web.Client.Services
{
    using System.Security.Principal;
    using System.Web.Mvc;
    using Common.VisitPay.Strings;
    using Common.Web.Models.Payment;

    public class ModelService
    {
        public static void CreateArrangePaymentViewModelContext(ControllerBase controller, IPrincipal user, ArrangePaymentViewModel model, int vpGuarantorId, int visitPayUserId)
        {
            UrlHelper url = new UrlHelper(controller.ControllerContext.RequestContext);
            model.DataUrl = url.Action("ArrangePaymentData", "Payment");
            model.OptionUrl = url.Action("ArrangePaymentFinancePlanData", "Payment");
            model.FinancePlanSubmitUrl = url.Action("FinancePlanSubmit", "FinancePlan");
            model.FinancePlanValidateUrl = url.Action("FinancePlanValidate", "FinancePlan");
            model.PaymentSubmitUrl = url.Action("ArrangePaymentSubmit", "Payment");
            model.PaymentValidateUrl = url.Action("ArrangePaymentValidate", "Payment");
            model.HomeUrl = url.Action("GuarantorAccount", "Search", new {id = vpGuarantorId});
            model.HomeButtonText = "Return to Guarantor Details";
            model.VisitPayUserId = visitPayUserId;

            if (!user.IsInRole(VisitPayRoleStrings.Csr.ChangePaymentDueDay))
            {
                foreach (PaymentOptionViewModel paymentOption in model.PaymentOptions)
                {
                    paymentOption.CanSetPaymentDueDay = false;
                }
            }
        }
    }
}
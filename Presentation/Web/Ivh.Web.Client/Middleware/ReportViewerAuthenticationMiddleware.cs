﻿namespace Ivh.Web.Client.Middleware
{
    using System.Threading.Tasks;
    using System.Web;
    using Common.VisitPay.Strings;
    using Common.Web.Extensions;
    using Microsoft.Owin;

    public class ReportViewerAuthenticationMiddleware : OwinMiddleware
    {
        public ReportViewerAuthenticationMiddleware(OwinMiddleware next) : base(next)
        {
        }

        public override Task Invoke(IOwinContext context)
        {
            if (HttpContext.Current.Request.Url.AbsolutePath.EndsWith(".aspx")
                    && (!context.Authentication.User.Identity.IsAuthenticated
                    || !context.Authentication.User.IsInARole(VisitPayRoleStrings.Miscellaneous.IvhAdmin, VisitPayRoleStrings.Restricted.VisitPayReports)))
            {
                HttpContext.Current.Session.Clear();
                HttpContext.Current.Session.Abandon();
                HttpContext.Current.Response.StatusCode = 404; //changing this to a 401 will cause an infinite loop
                HttpContext.Current.Response.End();
            }
            return this.Next.Invoke(context);
        }

    }
}
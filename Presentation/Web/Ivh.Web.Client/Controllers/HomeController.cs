﻿namespace Ivh.Web.Client.Controllers
{
    using Application.Content.Common.Dtos;
    using Application.Content.Common.Interfaces;
    using Application.Core.Common.Dtos;
    using Application.Core.Common.Interfaces;
    using Application.SecureCommunication.Common.Interfaces;
    using Application.User.Common.Dtos;
    using Attributes;
    using AutoMapper;
    using Common.Web.Interfaces;
    using Common.Web.Models.Faq;
    using Domain.Settings.Interfaces;
    using Models;
    using Models.Home;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Security.Claims;
    using System.Threading.Tasks;
    using System.Web.Mvc;
    using Common.VisitPay.Enums;
    using Common.VisitPay.Strings;
    using Common.Web.Cookies;
    using Ivh.Domain.Core.ThirdPartyKey.Interfaces;
    using System.Web.SessionState;
    using Ivh.Common.Session;

    [VpAuthorize(Roles = VisitPayRoleStrings.System.Client + "," + VisitPayRoleStrings.Admin.Administrator)]
    public class HomeController : BaseController
    {
        private readonly Lazy<IContentApplicationService> _contentApplicationService;
        private readonly Lazy<IClientSupportRequestApplicationService> _clientSupportRequestApplicationService;
        private readonly Lazy<ISupportRequestApplicationService> _supportRequestApplicationService;
        private readonly Lazy<IVisitPayUserApplicationService> _visitPayUserApplicationService;
        private readonly Lazy<IApplicationSettingsService> _applicationSettingsService;
        private readonly Lazy<IKnowledgeBaseApplicationService> _knowledgeBaseApplicationService;
        private readonly Lazy<IThirdPartyValueApplicationService> _thirdPartyValueApplicationService;
        private readonly Lazy<IWebClientSessionFacade> _webClientSessionFacade;

        public HomeController(
            Lazy<IBaseControllerService> baseControllerService,
            Lazy<IContentApplicationService> contentApplicationService,
            Lazy<IClientSupportRequestApplicationService> clientSupportRequestApplicationService,
            Lazy<ISupportRequestApplicationService> supportRequestApplicationService,
            Lazy<IApplicationSettingsService> applicationSettingsService,
            Lazy<IVisitPayUserApplicationService> visitPayUserApplicationService,
            Lazy<IKnowledgeBaseApplicationService> knowledgeBaseApplicationService,
            Lazy<IThirdPartyValueApplicationService> thirdPartyValueApplicationService,
            Lazy<IWebClientSessionFacade> webClientSessionFacade) 
            : base(baseControllerService)
        {
            this._contentApplicationService = contentApplicationService;
            this._clientSupportRequestApplicationService = clientSupportRequestApplicationService;
            this._supportRequestApplicationService = supportRequestApplicationService;
            this._visitPayUserApplicationService = visitPayUserApplicationService;
            this._applicationSettingsService = applicationSettingsService;
            this._knowledgeBaseApplicationService = knowledgeBaseApplicationService;
            this._thirdPartyValueApplicationService = thirdPartyValueApplicationService;
            this._webClientSessionFacade = webClientSessionFacade;
        }

        const string HideBuildBannerKey = "HideBuildBanner";

        public async Task<ActionResult> Index()
        {
            CmsVersionDto cmsVersionTop = await this._contentApplicationService.Value.GetCurrentVersionAsync(CmsRegionEnum.ClientHomeTopText);
            CmsVersionDto cmsVersionRoles = await this._contentApplicationService.Value.GetCurrentVersionAsync(CmsRegionEnum.ClientHomeRoleText);

            IList<VisitPayRoleDto> roles = this._visitPayUserApplicationService.Value.GetClientUserRoles(this.User.IsInRole(VisitPayRoleStrings.Miscellaneous.IvhAdmin));

            bool isFeatureEnabled= this.IsFeatureEnabled(VisitPayFeatureEnum.FeatureCardReaderIsEnabled);
            bool userHasCardReaderRole = this.User.IsInRole(VisitPayRoleStrings.Payment.CardReader);
            bool shouldShowCardDeviceKeyModal = this._thirdPartyValueApplicationService.Value.CardDeviceKeyNeedsToBeSet(this.CurrentUserId, isFeatureEnabled, userHasCardReaderRole);

            HomeViewModel model = new HomeViewModel
            {
                TextRoles = cmsVersionRoles.ContentBody,
                TextTop = cmsVersionTop.ContentBody,
                UserRoles = (roles?.Select(r => new UserRolesViewModel {Name = r.Name, VisitPayRoleDescription = r.VisitPayRoleDescription}).ToList() ?? new List<UserRolesViewModel>()).OrderBy(x => x.Name).ToList(),
                ShowCardReaderDeviceKeyModal = shouldShowCardDeviceKeyModal
                
            };

            return this.View(model);
        }

        [HttpGet]
        [AllowAnonymous]
        public async Task<PartialViewResult> FaqsPartial()
        {
            IList<KnowledgeBaseCategoryDto> categoryDtos = await this._knowledgeBaseApplicationService.Value.GetKnowledgeBaseCategoriesAsync(KnowledgeBaseApplicationTypeEnum.Client);

            List<KnowledgeBaseCategoryViewModel> categoryViewModels = Mapper.Map<List<KnowledgeBaseCategoryViewModel>>(categoryDtos);
            return this.PartialView("_Faqs", categoryViewModels);
        }

        [HttpGet]
        public ActionResult NotAuthorized()
        {
            return this.View();
        }

        public new PartialViewResult CurrentUser()
        {
            VisitPayUserDto visitPayUserDto = this._visitPayUserApplicationService.Value.FindById(this.CurrentUserIdString);

            ClaimsIdentity identity = this.HttpContext.User.Identity as ClaimsIdentity;
            DateTime? dateTime = null;

            if (identity != null && identity.HasClaim(c => c.Type.Equals(ClaimTypeEnum.LastLoginDate.ToString(), StringComparison.OrdinalIgnoreCase)))
            {
                dateTime = DateTime.Parse(identity.Claims.First(c => c.Type.Equals(ClaimTypeEnum.LastLoginDate.ToString(), StringComparison.OrdinalIgnoreCase)).Value);
            }

            string lastLoginDate = this.TimeZoneHelper.Client.ToLocalDateTimeString(dateTime ?? visitPayUserDto.LastLoginDate ?? DateTime.UtcNow);

            int? unreadMessagesFromClient = this.User.IsInRole(VisitPayRoleStrings.Miscellaneous.VpSupportTicketAdmin) ? this._clientSupportRequestApplicationService.Value.GetUnreadMessagesCountFromClient(this.CurrentUserId) : (int?) null;
            int? unreadMessagesToClient = this.User.IsInRole(VisitPayRoleStrings.Miscellaneous.VpSupportTickets) ? this._clientSupportRequestApplicationService.Value.GetUnreadMessagesCountToClient(this.CurrentUserId) : (int?) null;
            int? unreadMessagesToClientAll = this.User.IsInRole(VisitPayRoleStrings.Miscellaneous.VpSupportTicketsManage) ? this._clientSupportRequestApplicationService.Value.GetUnreadMessagesCountToClient(null) : (int?) null;

            int? unreadMessagesGuarantor = null;
            if (this.User.IsInRole(VisitPayRoleStrings.Csr.SupportView) || this.User.IsInRole(VisitPayRoleStrings.Csr.SupportAdmin))
            {
                unreadMessagesGuarantor = this._supportRequestApplicationService.Value.GetAllUnreadMessagesFromGuarantorsCount();
            }

            return this.PartialView("_CurrentUser", new ClientCurrentUserViewModel
            {
                FirstName = visitPayUserDto.FirstName,
                LastName = visitPayUserDto.LastName,
                LastLoginDate = lastLoginDate,
                UserName = visitPayUserDto.UserName,
                UnreadMessagesFromClient = unreadMessagesFromClient,
                UnreadMessagesToClient = unreadMessagesToClient,
                UnreadMessagesToClientAll = unreadMessagesToClientAll,
                UnreadMessagesGuarantor = unreadMessagesGuarantor,
            });
        }

        [AllowAnonymous]
        [HttpPost]
        [Common.Web.Attributes.SessionState(SessionStateBehavior.Required)]
        public void HideBuildBanner()
        {
            if (this.ShowBuildBanner)
            {
                this._webClientSessionFacade.Value.SetSessionValue(HideBuildBannerKey, true);
            }
        }
    }
}
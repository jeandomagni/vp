﻿namespace Ivh.Web.Client.Controllers
{
    using Application.Core.Common.Dtos;
    using Application.FinanceManagement.Common.Dtos;
    using Application.FinanceManagement.Common.Interfaces;
    using Attributes;
    using AutoMapper;
    using Common.Base.Utilities.Extensions;
    using Common.VisitPay.Strings;
    using Common.Web.Controllers;
    using Common.Web.Interfaces;
    using Models;
    using Models.PaymentQueue;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using System.Web.Mvc;
    using System.Web.SessionState;

    [VpAuthorize(Roles = VisitPayRoleStrings.Csr.PaymentQueue)]
    public class PaymentQueueController : BaseWebController
    {
        private readonly Lazy<IStatementApplicationService> _statementApplicationService;
        private readonly Lazy<IPaymentQueueApplicationService> _paymentQueueApplicationService;
        private readonly Lazy<IPaymentSubmissionApplicationService> _paymentSubmissionApplicationService;

        public PaymentQueueController(
            Lazy<IBaseControllerService> baseControllerService,
            Lazy<IStatementApplicationService> statementApplicationService,
            Lazy<IPaymentQueueApplicationService> paymentQueueApplicationService,
            Lazy<IPaymentSubmissionApplicationService> paymentSubmissionApplicationService
        ) : base(baseControllerService)
        {
            this._statementApplicationService = statementApplicationService;
            this._paymentQueueApplicationService = paymentQueueApplicationService;
            this._paymentSubmissionApplicationService = paymentSubmissionApplicationService;
        }

        const string PaymentQueueExportKey = "PaymentQueueExport";

        // GET: PaymentQueue
        public ActionResult Index()
        {
            PaymentQueueFilterViewModel model = new PaymentQueueFilterViewModel();

            //Populate available lockbox batch numbers in model
            IList<string> batchNumbers = this._paymentQueueApplicationService.Value.GetAllPaymentImportBatchNumbers();
            IList<SelectListItem> batchNumberSelections = new List<SelectListItem>()
            {
                new SelectListItem()
                {
                    Text = "All", Value = "", Selected = true
                }
            };
            batchNumberSelections.AddRange(batchNumbers.Select(b => new SelectListItem()
            {
                Text = b,
                Value = b
            }));
            model.BatchNumbers = batchNumberSelections;


            return View(model);
        }

        public PartialViewResult GuarantorSearchModal(PaymentQueueGuarantorSearchFilterViewModel model)
        {
            return this.PartialView("_GuarantorSearchModal", model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult LoadPaymentQueue(PaymentQueueFilterViewModel model, int page, int rows, string sidx, string sord)
        {
            //Convert filter
            PaymentImportFilterDto filterDto = Mapper.Map<PaymentImportFilterDto>(model);
            PaymentImportQueueDto resultDto = this._paymentQueueApplicationService.Value.LoadPaymentImportQueue(filterDto);

            //Convert to view model
            PaymentQueueViewModel viewModel = Mapper.Map<PaymentQueueViewModel>(resultDto);

            //TODO: Not paging for now as it would cause issues with inline editing. May implement in future but would require some thinking
            /*
            int totalRecords = viewModel.PaymentQueueItems.Count;
            int recordsToSkip = rows * (page - 1);
            viewModel.PaymentQueueItems = viewModel.PaymentQueueItems.Skip(recordsToSkip).Take(rows).ToList();
            viewModel.Records = totalRecords;
            viewModel.Page = page;
            viewModel.Total = Math.Ceiling((decimal) totalRecords / rows);
            */

            return this.Json(viewModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Common.Web.Attributes.SessionState(SessionStateBehavior.Required)]
        public async Task<ActionResult> PaymentQueueExport(PaymentQueueFilterViewModel model, string sidx, string sord)
        {
            PaymentImportFilterDto filterDto = Mapper.Map<PaymentImportFilterDto>(model);

            byte[] bytes = await this._paymentQueueApplicationService.Value.ExportPaymentImportQueueAsync(filterDto, this.CurrentUser.FullNameDisplay);
            this.SetTempData(PaymentQueueExportKey, bytes);

            return this.Json(new ResultMessage(true, this.Url.Action("PaymentQueueExportDownload")));

        }
        [HttpGet]
        public ActionResult PaymentQueueExportDownload()
        {
            object tempData = this.TempData[PaymentQueueExportKey];
            if (tempData != null)
            {
                this.WriteExcelInline((byte[])tempData, "PaymentQueue.xlsx");
            }

            return this.Json(new { }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult UpdatePaymentQueueItems(List<PaymentQueueItemViewModel> itemsToUpdate)
        {
            //Update records
            IList<PaymentImportDto> paymentImportsToUpdate = itemsToUpdate.Select(i => Mapper.Map<PaymentImportDto>(i)).ToList();
            this._paymentSubmissionApplicationService.Value.ReprocessPaymentImports(paymentImportsToUpdate);

            return this.Json(new
            {
                Success = true,
                Message =  "Records updated successfully"
            });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult SearchGuarantors(PaymentQueueGuarantorSearchFilterViewModel model, int page, int rows, string sidx, string sord)
        {
            if (!model.IsValidSearch)
            {
                model.HsGuarantorId = "-99999";
            }

            //
            GuarantorFilterDto guarantorFilterDto = new GuarantorFilterDto
            {
                SortField = sidx,
                SortOrder = sord
            };
            Mapper.Map(model, guarantorFilterDto);

            
            GuarantorWithLastFinancePlanStatementResultsDto guarantorsResultsDto = this._statementApplicationService.Value.GetGuarantorsWithLastFinancePlanStatement(guarantorFilterDto, page, rows);
            List<PaymentQueueGuarantorSearchResultViewModel> results = new List<PaymentQueueGuarantorSearchResultViewModel>();
            foreach (GuarantorWithLastFinancePlanStatementDto guarantorWithLastStatementDto in guarantorsResultsDto.Guarantors)
            {
                PaymentQueueGuarantorSearchResultViewModel searchResult = Mapper.Map<PaymentQueueGuarantorSearchResultViewModel>(guarantorWithLastStatementDto);
                results.Add(searchResult);
            }

            int totalRecordCount = guarantorsResultsDto.TotalRecords;
            
            return this.Json(new
            {
                Guarantors = results,
                page,
                total = Math.Ceiling((decimal)totalRecordCount / rows),
                records = totalRecordCount
            });
        }
    }
}
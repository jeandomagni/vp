﻿namespace Ivh.Web.Client.Controllers
{
    using System;
    using System.Web.Mvc;
    using Application.Core.Common.Dtos;
    using Application.Core.Common.Interfaces;
    using Attributes;
    using AutoMapper;
    using Common.Base.Enums;
    using Common.VisitPay.Enums;
    using Common.VisitPay.Strings;
    using Common.Web.Interfaces;
    using Microsoft.AspNet.Identity;
    using Models.Unmatch;

    [VpAuthorize(Roles = VisitPayRoleStrings.System.Client + "," + VisitPayRoleStrings.Admin.Administrator + ",")]
    [VpAuthorize(Roles = VisitPayRoleStrings.Miscellaneous.Unmatch + "," + VisitPayRoleStrings.Miscellaneous.IvhAdmin)]
    public class UnmatchController : BaseController
    {
        private readonly Lazy<IGuarantorApplicationService> _guarantorApplicationService;
        private readonly Lazy<IRedactionApplicationService> _redactionApplicationService;
        private readonly Lazy<IVisitApplicationService> _visitApplicationService;
        private readonly Lazy<IVisitPayUserApplicationService> _visitPayUserApplicationService;

        public UnmatchController(
            Lazy<IBaseControllerService> baseControllerService,
            Lazy<IGuarantorApplicationService> guarantorApplicationService,
            Lazy<IRedactionApplicationService> redactionApplicationService,
            Lazy<IVisitApplicationService> visitApplicationService,
            Lazy<IVisitPayUserApplicationService> visitPayUserApplicationService)
            : base(baseControllerService)
        {
            this._guarantorApplicationService = guarantorApplicationService;
            this._redactionApplicationService = redactionApplicationService;
            this._visitApplicationService = visitApplicationService;
            this._visitPayUserApplicationService = visitPayUserApplicationService;
        }

        [HttpGet]
        [VpAuthorize(Roles = VisitPayRoleStrings.Miscellaneous.Unmatch)]
        public PartialViewResult SelectHsGuarantors(int vpGuarantorId)
        {
            GuarantorDto guarantor = this._guarantorApplicationService.Value.GetGuarantor(vpGuarantorId);

            SelectHsGuarantorsViewModel model = new SelectHsGuarantorsViewModel
            {
                HsGuarantorSourceSystemKeys = guarantor.HsGuarantorsSourceSystemKeys,
                VpGuarantorId = vpGuarantorId
            };

            return this.PartialView("_SelectHsGuarantors", model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [VpAuthorize(Roles = VisitPayRoleStrings.Miscellaneous.Unmatch)]
        public PartialViewResult UnmatchGuarantors(SelectHsGuarantorsViewModel model)
        {
            GuarantorDto guarantor = this._guarantorApplicationService.Value.GetGuarantor(model.VpGuarantorId);

            return this.PartialView("_UnmatchGuarantors", new UnmatchGuarantorsViewModel
            {
                Source = model.UnmatchSource,
                ActionType = model.ActionType,
                VpGuarantorId = model.VpGuarantorId,
                VpGuarantorFirstName = guarantor.User.FirstName,
                VpGuarantorLastName = guarantor.User.LastName
            });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [VpAuthorize(Roles = VisitPayRoleStrings.Miscellaneous.Unmatch)]
        public JsonResult ConfirmUnmatchGuarantors(UnmatchGuarantorsViewModel model)
        {
            if (!this.ModelState.IsValid)
            {
                return this.Json(false);
            }

            HsGuarantorUnmatchRequestDto request = new HsGuarantorUnmatchRequestDto
            {
                ActionVisitPayUser = this._visitPayUserApplicationService.Value.FindById(this.CurrentUserIdString),
                HsGuarantorMatchDiscrepancyId = model.HsGuarantorMatchDiscrepancyId,
                HsGuarantorSourceSystemKeys = model.HsGuarantorSourceSystemKeys,
                Notes = model.Notes,
                UnmatchActionType = model.ActionType,
                VpGuarantorId = model.VpGuarantorId
            };

            bool result = this._redactionApplicationService.Value.ProcessGuarantorUnmatchRequest(request, model.Source);

            return this.Json(result);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [VpAuthorize(Roles = VisitPayRoleStrings.Miscellaneous.IvhAdmin)]
        public PartialViewResult UnmatchVisit(int visitId, int visitPayUserId)
        {
            VisitDto visit = this._visitApplicationService.Value.GetVisit(visitPayUserId, visitId);
            UnmatchVisitViewModel model = Mapper.Map<UnmatchVisitViewModel>(visit);
            return this.PartialView("_UnmatchVisit", model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [VpAuthorize(Roles = VisitPayRoleStrings.Miscellaneous.IvhAdmin)]
        public JsonResult ConfirmUnmatchVisit(UnmatchVisitViewModel model)
        {
            if (!this.ModelState.IsValid)
            {
                return this.Json(false);
            }

            VisitUnmatchRequestDto request = new VisitUnmatchRequestDto
            {
                ActionVisitPayUser = this._visitPayUserApplicationService.Value.FindById(this.CurrentUserIdString),
                Notes = model.Notes,
                SourceSystemKey = model.SourceSystemKey,
                VisitId = model.VisitId,
                VpGuarantorId = model.VpGuarantorId,
                MatchStatus = model.MatchStatus,
                UnmatchReason = HsGuarantorUnmatchReasonEnum.ClientUnmatchedVisitManual
            };

            bool result = this._redactionApplicationService.Value.ProcessVisitUnmatchRequest(request);

            return this.Json(result);
        }
    }
}
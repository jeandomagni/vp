﻿namespace Ivh.Web.Client.Controllers
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Web.Mvc;
    using System.Web.SessionState;
    using Application.Core.Common.Dtos;
    using Application.Core.Common.Interfaces;
    using Attributes;
    using AutoMapper;
    using Common.Base.Utilities.Extensions;
    using Common.VisitPay.Enums;
    using Common.VisitPay.Strings;
    using Common.Web.Extensions;
    using Common.Web.Filters;
    using Common.Web.Interfaces;
    using Common.Web.Models;
    using Models.VisitPayDocument;
    using NUnit.Framework;
    
    [RequireFeature(VisitPayFeatureEnum.FeatureVisitPayDocumentIsEnabled)]
    public class VisitPayDocumentController : BaseController
    {
        private readonly Lazy<IVisitPayDocumentApplicationService> _visitPayDocumentApplicationService;

        public VisitPayDocumentController(
            Lazy<IBaseControllerService> baseControllerService,
            Lazy<IVisitPayDocumentApplicationService> visitPayDocumentApplicationService
        ) : base(baseControllerService)
        {
            this._visitPayDocumentApplicationService = visitPayDocumentApplicationService;
        }


        [VpAuthorize(Roles = VisitPayRoleStrings.Miscellaneous.VisitPayDocument)]
        public ActionResult Index()
        {
            VisitPayDocumentFilterViewModel visitPayDocumentFilterViewModel = new VisitPayDocumentFilterViewModel();
            return this.View(visitPayDocumentFilterViewModel);
        }

        [VpAuthorize(Roles = VisitPayRoleStrings.Miscellaneous.VisitPayDocument)]
        public ActionResult UploadVisitPayDocument(VisitPayDocumentUploadViewModel visitPayDocumentUploadViewModel)
        {

            if (!visitPayDocumentUploadViewModel.IsValidContentType())
            {
                return this.Json(new
                {
                    errorProperty=nameof(VisitPayDocumentUploadViewModel.FileContents),
                    errorMessage=visitPayDocumentUploadViewModel.AcceptableFileExtensionsMessage
                }, JsonRequestBehavior.AllowGet);
            }

            if (!visitPayDocumentUploadViewModel.IsSupportedFileSize())
            {
                return this.Json(new
                {
                    errorProperty = nameof(VisitPayDocumentUploadViewModel.FileContents),
                    errorMessage = visitPayDocumentUploadViewModel.FileSizeMessage
                }, JsonRequestBehavior.AllowGet);
            }

            byte[] fileInBytes = new byte[visitPayDocumentUploadViewModel.FileContents.ContentLength];
            using (BinaryReader theReader = new BinaryReader(visitPayDocumentUploadViewModel.FileContents.InputStream))
            {
                fileInBytes = theReader.ReadBytes(visitPayDocumentUploadViewModel.FileContents.ContentLength);
            }

            VisitPayDocumentDto dto = new VisitPayDocumentDto
            {
                AttachmentFileName = visitPayDocumentUploadViewModel.FileContents.FileName,
                FileContent = fileInBytes,
                FileSize = fileInBytes.Length,
                MimeType = visitPayDocumentUploadViewModel.FileContents.ContentType,
                UploadedByVpUserId = this.CurrentUserId,
                InsertDate = DateTime.UtcNow,
                DisplayTitle= visitPayDocumentUploadViewModel.DisplayTitle,
                ActiveDate = visitPayDocumentUploadViewModel.ActiveDate,
                VisitPayDocumentType = (VisitPayDocumentTypeEnum) visitPayDocumentUploadViewModel.VisitPayDocumentTypeId.Value
            };
            this._visitPayDocumentApplicationService.Value.AddVisitPayDocument(dto);

            return this.Json(new { success = true }, JsonRequestBehavior.AllowGet);

        }

        [HttpGet]
        public ActionResult DownloadAttachment(int visitPayDocumentId)
        {
            VisitPayDocumentDto attachmentDownloadDto = this._visitPayDocumentApplicationService.Value.GetVisitPayDocumentDtoById(visitPayDocumentId);
            return attachmentDownloadDto != null ? this.File(attachmentDownloadDto.FileContent, attachmentDownloadDto.MimeType, attachmentDownloadDto.AttachmentFileName) : null;
        }


        [HttpPost]
        [VpAuthorize(Roles = VisitPayRoleStrings.Miscellaneous.VisitPayDocument)]
        public JsonResult VisitPayDocuments(VisitPayDocumentFilterViewModel model, int page, int rows, string sidx, string sord)
        {
            sidx = string.IsNullOrEmpty(sidx) ? nameof(VisitPayDocumentResultDto.VisitPayDocumentId) : sidx;
            VisitPayDocumentFilter trainingDocummentFilterDto = new VisitPayDocumentFilter
            {
                Page = page,
                Rows = rows,
                SortField = this.TransformSortField(sidx),
                SortOrder = sord
            };
            Mapper.Map(model, trainingDocummentFilterDto);

            IList<VisitPayDocumentResultDto> documentAttachmentResultDtos =
                this._visitPayDocumentApplicationService.Value.GetVisitPayDocumentResults(trainingDocummentFilterDto);

            List<VisitPayDocumentResultViewModel> viewModel = Mapper.Map<List<VisitPayDocumentResultViewModel>>(documentAttachmentResultDtos);

            //
            return this.Json(new
            {
                VisitPayDocuments = viewModel,
                page,
                total = Math.Ceiling((decimal) documentAttachmentResultDtos.Count() / rows),
                records = documentAttachmentResultDtos.Count()
            });
        }

        private string TransformSortField(string sortField)
        {
            Dictionary<string, string> sortFieldLookup = new Dictionary<string, string>
            {
                ["ActiveDateString"] = "ActiveDate"
            };

            string fieldName;
            if (sortFieldLookup.TryGetValue(sortField,out fieldName))
            {
                return fieldName;
            }
            return sortField;
        }
        
        
        [HttpGet]
        [VpAuthorize(Roles = VisitPayRoleStrings.Miscellaneous.VisitPayDocument)]
        public PartialViewResult EditVisitPayDocument(int visitPayDocumentId)
        {
           
            VisitPayDocumentResultDto visitPayDocumentResultDtoById = 
                this._visitPayDocumentApplicationService.Value.GetVisitPayDocumentResultDtoById(visitPayDocumentId);
            VisitPayDocumentEditViewModel vm = Mapper.Map<VisitPayDocumentEditViewModel>(visitPayDocumentResultDtoById);
            int currentUserId = this.CurrentUserId;
            vm.CanApprove = (currentUserId != vm.UploadedByVpUserId);
            return this.PartialView("_EditVisitPayDocumentModal", vm);
        }

        [HttpPost]
        [VpAuthorize(Roles = VisitPayRoleStrings.Miscellaneous.VisitPayDocument)]
        public JsonResult EditVisitPayDocument(VisitPayDocumentEditViewModel visitPayDocumentEditViewModel)
        {

            int? approvedByUser = visitPayDocumentEditViewModel.ApprovedByVpUserId;
            if (visitPayDocumentEditViewModel.WasApproved)
            {
                approvedByUser = this.CurrentUserId;
            }
            if (visitPayDocumentEditViewModel.WasUnApproved)
            {
                approvedByUser = null;
            }

            VisitPayDocumentEditDto trainingDocumentAttachmentEditDto = new VisitPayDocumentEditDto
            {
                VisitPayDocumentId =  visitPayDocumentEditViewModel.VisitPayDocumentId,
                ActiveDate = visitPayDocumentEditViewModel.ActiveDate,
                ApprovedDate = visitPayDocumentEditViewModel.ApprovedDate,
                ApprovedByVpUserId = approvedByUser,
                DisplayTitle = visitPayDocumentEditViewModel.DisplayTitle,
                VisitPayDocumentType = (VisitPayDocumentTypeEnum)visitPayDocumentEditViewModel.VisitPayDocumentTypeId.Value
            };

            this._visitPayDocumentApplicationService.Value.SaveVisitPayDocument(trainingDocumentAttachmentEditDto);

            return this.Json(new { Success = true }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [VpAuthorize(Roles = VisitPayRoleStrings.Miscellaneous.VisitPayDocument)]
        public JsonResult DeleteVisitPayDocument(int visitPayDocumentId)
        {
            this._visitPayDocumentApplicationService.Value.DeleteVisitPayDocument(visitPayDocumentId);
            return this.Json(new {Success=true}, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        [VpAuthorize(Roles = VisitPayRoleStrings.Miscellaneous.VisitPayDocument)]
        public PartialViewResult UploadVisitPayDocumentModal()
        {
            return this.PartialView("_UploadVisitPayDocument", new VisitPayDocumentUploadViewModel());
        }

        [HttpGet]
        public ActionResult VisitPayDocumentClient()
        {

            VisitPayDocumentFilter filter = new VisitPayDocumentFilter { IsApproved = true };
            IList<VisitPayDocumentResultDto> documentAttachmentResultDtos =
                this._visitPayDocumentApplicationService.Value.GetVisitPayDocumentResults(filter);
            
            Dictionary<VisitPayDocumentTypeEnum, List<VisitPayDocumentResultViewModel>> dictionary = documentAttachmentResultDtos.GroupBy(x => x.VisitPayDocumentType).ToDictionary(g => g.Key, g => Mapper.Map<List<VisitPayDocumentResultViewModel>>(g).ToList());
            VisitPayDocumentClientViewModel vm = new VisitPayDocumentClientViewModel { VisitPayDocumentResultDictionary = dictionary };
            return this.View(vm);
        }


    }
}
﻿namespace Ivh.Web.Client.Controllers
{
    using System;
    using System.Web.Mvc;
    using Attributes;
    using Common.Base.Enums;
    using Common.VisitPay.Enums;
    using Common.VisitPay.Strings;
    using Common.Web.Filters;
    using Common.Web.Interfaces;

    public class DemoController : BaseController
    {
        public DemoController(Lazy<IBaseControllerService> baseControllerService) : base(baseControllerService)
        {
        }

        [RequireFeature(VisitPayFeatureEnum.DemoClientReportDashboardIsEnabled)]
        [VpAuthorize(Roles = VisitPayRoleStrings.Miscellaneous.IvhAdmin)]
        public ActionResult ReportDashboard()
        {
            return this.View();
        }
    }
}
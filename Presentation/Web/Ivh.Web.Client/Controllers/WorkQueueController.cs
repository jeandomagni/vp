﻿namespace Ivh.Web.Client.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using System.Web.Mvc;
    using System.Web.SessionState;
    using Application.Core.Common.Dtos;
    using Application.Core.Common.Interfaces;
    using Application.SecureCommunication.Common.Dtos;
    using Application.SecureCommunication.Common.Interfaces;
    using Attributes;
    using AutoMapper;
    using Common.Base.Utilities.Extensions;
    using Common.VisitPay.Enums;
    using Common.VisitPay.Strings;
    using Common.Web.Interfaces;
    using Common.Web.Models;
    using Microsoft.AspNet.Identity;
    using Models.ClientSupport;
    using Models.WorkQueue;

    [VpAuthorize(Roles = VisitPayRoleStrings.System.Client + "," + VisitPayRoleStrings.Admin.Administrator)]
    public class WorkQueueController : BaseController
    {
        private readonly Lazy<IClientSupportRequestApplicationService> _clientSupportRequestApplicationService;
        private readonly Lazy<ISupportRequestApplicationService> _supportRequestApplicationService;
        private readonly Lazy<IRedactionApplicationService> _redactionApplicationService;
        private readonly Lazy<IFacilityApplicationService> _facilityApplicationService;

        public WorkQueueController(
            Lazy<IBaseControllerService> baseControllerService,
            Lazy<IClientSupportRequestApplicationService> clientSupportRequestApplicationService,
            Lazy<ISupportRequestApplicationService> supportRequestApplicationService,
            Lazy<IRedactionApplicationService> redactionApplicationService,
            Lazy<IFacilityApplicationService> facilityApplicationService)
            : base(baseControllerService)
        {
            this._clientSupportRequestApplicationService = clientSupportRequestApplicationService;
            this._supportRequestApplicationService = supportRequestApplicationService;
            this._redactionApplicationService = redactionApplicationService;
            this._facilityApplicationService = facilityApplicationService;
        }

        const string SupportRequestsExportKey = "SupportRequestsExport";
        const string GuarantorAttributeChangesExportKey = "GuarantorAttributeChangesExport";

        #region support requests

        private const string UnassignedAdminUser = "<Unassigned>";
        private const int UnassignedAdminUserId = -1;

        [VpAuthorize(Roles = VisitPayRoleStrings.Csr.SupportAdmin + "," + VisitPayRoleStrings.Csr.SupportCreate + "," + VisitPayRoleStrings.Csr.SupportView)]
        [HttpGet]
        public ActionResult SupportRequests(bool? readMessages, bool? unreadMessages, int? dateRange)
        {
            SupportRequestSearchFilterViewModel model = new SupportRequestSearchFilterViewModel();
            model.SupportAdminUsers = this.GetSupportAdminUsers();
            model.ReadMessages = readMessages.GetValueOrDefault(true);
            model.UnreadMessages = unreadMessages.GetValueOrDefault(true);
            model.Facilities = this.GetFacilities();

            if (dateRange.HasValue)
            {
                model.DateRanges[dateRange.Value].Selected = true;
            }
            else
            {
                model.DateRanges[0].Selected = true;
            }
            return this.View(model);
        }

        [VpAuthorize(Roles = VisitPayRoleStrings.Csr.SupportAdmin + "," + VisitPayRoleStrings.Csr.SupportCreate + "," + VisitPayRoleStrings.Csr.SupportView)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult SupportAdminUsers()
        {
            IList<SelectListItem> users = this.GetSupportAdminUsers();
            var result = users.Select(x => new { x.Text, x.Value });
            return this.Json(result);
        }

        private IList<SelectListItem> GetFacilities()
        {
            IReadOnlyList<FacilityDto> facilities = this._facilityApplicationService.Value.GetAllFacilities();

            List<SelectListItem> facilitySelectListItems =
                facilities
                    .Where(x => !string.IsNullOrWhiteSpace(x.SourceSystemKey))
                    .OrderBy(f => f.SourceSystemKey)
                    .Select(f => new SelectListItem { Text = $"{f.SourceSystemKey} - {f.FacilityDescription}", Value = f.SourceSystemKey })
                    .ToList();

            facilitySelectListItems.Insert(0, new SelectListItem { Text = "All", Value = string.Empty });

            return facilitySelectListItems;
        }

        private IList<SelectListItem> GetSupportAdminUsers()
        {
            IDictionary<int, string> vpSupportAdminUsers = this._supportRequestApplicationService.Value.GetAssignableUsers(
                assignedOnly: true,  
                assignedVisitPayUserId: null);
            List<SelectListItem> supportAdminUsers = vpSupportAdminUsers.Select(x => new SelectListItem { Text = x.Value, Value = x.Key.ToString() }).ToList();
            supportAdminUsers.Insert(0, new SelectListItem { Text = UnassignedAdminUser, Value = UnassignedAdminUserId.ToString() });
            return supportAdminUsers;
        }

        [VpAuthorize(Roles = VisitPayRoleStrings.Csr.SupportAdmin + "," + VisitPayRoleStrings.Csr.SupportCreate + "," + VisitPayRoleStrings.Csr.SupportView)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult SupportRequests(SupportRequestSearchFilterViewModel model, int page, int rows, string sidx, string sord)
        {
            //
            SupportRequestFilterDto supportRequestFilterDto = new SupportRequestFilterDto
            {
                SortField = sidx,
                SortOrder = sord
            };
            Mapper.Map(model, supportRequestFilterDto);

            //
            SupportRequestResultsDto supportRequestResultsDto = this._supportRequestApplicationService.Value.GetSupportRequests(supportRequestFilterDto, page, rows, null);

            foreach (SupportRequestDto sr in supportRequestResultsDto.SupportRequests)
            {
                sr.SupportRequestMessages = sr.SupportRequestMessages.Where(srm => srm.SupportRequestMessageType != SupportRequestMessageTypeEnum.DraftMessage || srm.CreatedByVpUserId == this.CurrentUserId).ToList();
            }
            List<SupportRequestSearchResultViewModel> supportRequests = Mapper.Map<List<SupportRequestSearchResultViewModel>>(supportRequestResultsDto.SupportRequests);
            supportRequests.ForEach(supportRequest => supportRequest.CanTakeAction = this.User.IsInRole(VisitPayRoleStrings.Csr.SupportAdmin));

            return this.Json(new
            {
                SupportRequests = supportRequests,
                page,
                total = Math.Ceiling((decimal)supportRequestResultsDto.TotalRecords / rows),
                records = supportRequestResultsDto.TotalRecords
            });
        }

        [VpAuthorize(Roles = VisitPayRoleStrings.Csr.SupportAdmin + "," + VisitPayRoleStrings.Csr.SupportCreate + "," + VisitPayRoleStrings.Csr.SupportView)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Common.Web.Attributes.SessionState(SessionStateBehavior.Required)]
        public async Task<JsonResult> SupportRequestsExport(SupportRequestSearchFilterViewModel model, string sidx, string sord)
        {
            SupportRequestFilterDto filter = Mapper.Map<SupportRequestFilterDto>(model);
            filter.SortField = sidx;
            filter.SortOrder = sord;

            //
            byte[] bytes = await this._supportRequestApplicationService.Value.ExportSupportRequestsClientAsync(filter, this.User.Identity.GetUserName());
            this.SetTempData(SupportRequestsExportKey, bytes);

            //
            return this.Json(new ResultMessage(true, this.Url.Action("SupportRequestsExportDownload")));
        }

        [VpAuthorize(Roles = VisitPayRoleStrings.Csr.SupportAdmin + "," + VisitPayRoleStrings.Csr.SupportCreate + "," + VisitPayRoleStrings.Csr.SupportView)]
        [HttpGet]
        public ActionResult SupportRequestsExportDownload()
        {
            object tempData = this.TempData[SupportRequestsExportKey];
            if (tempData != null)
            {
                this.WriteExcelInline((byte[])tempData, "SupportRequests.xlsx");
            }

            return this.Json(new { }, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region manage client support requests

        [VpAuthorize(Roles = VisitPayRoleStrings.Miscellaneous.VpSupportTicketsManage)]
        [HttpGet]
        public ActionResult ClientSupportRequests()
        {
            ClientSupportRequestFilterViewModel model = new ClientSupportRequestFilterViewModel
            {
                DaysOffset = -30
            };

            model.DayOffsets.Insert(0, new SelectListItem { Text = "Previous Week", Value = "-7" });
            model.DayOffsets.Insert(1, new SelectListItem { Text = "Previous 2 Weeks", Value = "-14" });

            this._clientSupportRequestApplicationService.Value.GetAllClientUsersWithSupportRequest().ForEach(x =>
            {
                model.RequestedByUsers.Add(new SelectListItem {Text = x.Value, Value = x.Key.ToString()});
            });

            return this.View(model);
        }

        [VpAuthorize(Roles = VisitPayRoleStrings.Miscellaneous.VpSupportTicketsManage)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult ClientSupportRequests(GridSearchModel<ClientSupportRequestFilterViewModel> filterModel)
        {
            ClientSupportRequestFilterDto filterDto = Mapper.Map<ClientSupportRequestFilterDto>(filterModel);
            ClientSupportRequestResultsDto resultsDto = this._clientSupportRequestApplicationService.Value.GetClientSupportRequests(filterDto);

            GridResultsModel<ClientSupportRequestSearchResultViewModel> model = new GridResultsModel<ClientSupportRequestSearchResultViewModel>(filterModel.Page, filterModel.Rows, resultsDto.TotalRecords)
            {
                Results = Mapper.Map<List<ClientSupportRequestSearchResultViewModel>>(resultsDto.ClientSupportRequests)
            };

            return this.Json(model);
        }

        #endregion

        #region manage guarantor attribute changes

        [VpAuthorize(Roles = VisitPayRoleStrings.Miscellaneous.Unmatch)]
        [HttpGet]
        public ActionResult GuarantorAttributeChanges()
        {
            GuarantorAttributeChangesFilterViewModel filter = new GuarantorAttributeChangesFilterViewModel
            {
                HsGuarantorMatchDiscrepancyStatuses = this._redactionApplicationService.Value.GetAllHsGuarantorMatchDiscrepancyStatuses().Select(x => new SelectListItem
                {
                    Text = x.HsGuarantorMatchDiscrepancyStatusName,
                    Value = x.HsGuarantorMatchDiscrepancyStatusId.ToString()
                }).OrderBy(x => x.Text).ToList()
            };
            filter.HsGuarantorMatchDiscrepancyStatuses.Add(new SelectListItem {Text = "All", Value = ""});

            List<KeyValuePair<string, string>> matchFields = this._redactionApplicationService.Value.GetMatchFieldsForDisplay().ToList();

            GuarantorAttributeChangesViewModel model = new GuarantorAttributeChangesViewModel
            {
                Filter = filter,
                MatchFields = matchFields
            };

            return this.View(model);
        }

        [VpAuthorize(Roles = VisitPayRoleStrings.Miscellaneous.Unmatch)]
        [HttpPost]
        public ActionResult GuarantorAttributeChanges(GuarantorAttributeChangesFilterViewModel model, int page, int rows, string sidx, string sord)
        {
            HsGuarantorMatchDiscrepancyFilterDto filter = Mapper.Map<HsGuarantorMatchDiscrepancyFilterDto>(model);
            filter.SortField = sidx;
            filter.SortOrder = sord;

            HsGuarantorMatchDiscrepancyResultsDto results = this._redactionApplicationService.Value.GetAllHsGuarantorAttributeChanges(filter, page, rows);

            return this.Json(new
            {
                GuarantorAttributeChanges = Mapper.Map<IList<HsGuarantorMatchDiscrepancyViewModel>>(results.HsGuarantorMatchDiscrepancies),
                page,
                total = Math.Ceiling((decimal) results.TotalRecords/rows),
                records = results.TotalRecords
            });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Common.Web.Attributes.SessionState(SessionStateBehavior.Required)]
        public async Task<ActionResult> GuarantorAttributeChangesExport(GuarantorAttributeChangesFilterViewModel model, string sidx, string sord)
        {
            HsGuarantorMatchDiscrepancyFilterDto filter = Mapper.Map<HsGuarantorMatchDiscrepancyFilterDto>(model);
            filter.SortField = sidx;
            filter.SortOrder = sord;

            //
            byte[] bytes = await this._redactionApplicationService.Value.ExportAllHsGuarantorMatchDiscrepancyAsync(filter, this.User.Identity.GetUserName());
            this.SetTempData(GuarantorAttributeChangesExportKey, bytes);

            //
            return this.Json(new ResultMessage(true, this.Url.Action("GuarantorAttributeChangesExportDownload")));
        }

        [HttpGet]
        public ActionResult GuarantorAttributeChangesExportDownload()
        {
            object tempData = this.TempData[GuarantorAttributeChangesExportKey];
            if (tempData != null)
            {
                this.WriteExcelInline((byte[]) tempData, "GuarantorAttributeChanges.xlsx");
            }

            return this.Json(new { }, JsonRequestBehavior.AllowGet);
        }

        #endregion
    }
}
﻿namespace Ivh.Web.Client.Controllers
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Linq;
    using System.Net;
    using System.Net.Http;
    using System.Threading.Tasks;
    using System.Web.Mvc;
    using System.Web.SessionState;
    using Application.Base.Common.Dtos;
    using Application.Content.Common.Dtos;
    using Application.Content.Common.Interfaces;
    using Application.Core.Common.Dtos;
    using Application.Core.Common.Interfaces;
    using Application.FinanceManagement.Common.Dtos;
    using Application.FinanceManagement.Common.Interfaces;
    using Application.User.Common.Dtos;
    using Application.User.Common.Interfaces;
    using Attributes;
    using AutoMapper;
    using Common.Base.Utilities.Extensions;
    using Common.Base.Utilities.Helpers;
    using Common.Base.Utilities.Lookup;
    using Common.EventJournal;
    using Common.VisitPay.Enums;
    using Common.VisitPay.Messages.Matching.Dtos;
    using Common.VisitPay.Strings;
    using Common.Web.Controllers;
    using Common.Web.Extensions;
    using Common.Web.Filters;
    using Common.Web.Interfaces;
    using Common.Web.Models;
    using Common.Web.Models.Account;
    using Common.Web.Models.Payment;
    using Common.Web.Models.Shared;
    using Models.Offline;
    using Services;

    [VpAuthorize(Roles = VisitPayRoleStrings.Csr.CallCenter)]
    [RequireFeature(VisitPayFeatureEnum.OfflineVisitPay)]
    public class OfflineController : BaseWebController
    {
        private readonly Lazy<IContentApplicationService> _contentApplicationService;
        private readonly Lazy<IGuarantorApplicationService> _guarantorApplicationService;
        private readonly Lazy<IOfflineUserApplicationService> _offlineUserApplicationService;
        private readonly Lazy<IPaymentOptionApplicationService> _paymentOptionApplicationService;
        private readonly Lazy<IVisitPayUserApplicationService> _visitPayUserApplicationService;
        private readonly Lazy<IVisitPayUserAddressApplicationService> _visitPayUserAddressApplicationService;
        private readonly Lazy<IRegistrationService> _registrationService;

        public OfflineController(
            Lazy<IBaseControllerService> baseControllerService,
            Lazy<IContentApplicationService> contentApplicationService,
            Lazy<IGuarantorApplicationService> guarantorApplicationService,
            Lazy<IOfflineUserApplicationService> offlineUserApplicationService,
            Lazy<IPaymentOptionApplicationService> paymentOptionApplicationService,
            Lazy<IVisitPayUserApplicationService> visitPayUserApplicationService,
            Lazy<IVisitPayUserAddressApplicationService> visitPayUserAddressApplicationService,
            Lazy<IRegistrationService> registrationService
        ) : base(baseControllerService)
        {
            this._contentApplicationService = contentApplicationService;
            this._guarantorApplicationService = guarantorApplicationService;
            this._offlineUserApplicationService = offlineUserApplicationService;
            this._paymentOptionApplicationService = paymentOptionApplicationService;
            this._visitPayUserApplicationService = visitPayUserApplicationService;
            this._visitPayUserAddressApplicationService = visitPayUserAddressApplicationService;
            this._registrationService = registrationService;
        }
        
        #region find / create guarantor

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult FindGuarantor(string sourceSystemKey, string lastName)
        {
            OfflineGuarantorSearchResultsDto results = this._offlineUserApplicationService.Value.GetHsGuarantors(sourceSystemKey, lastName);
            RegisterPersonalInformationViewModel model = null;

            if (results.Status == OfflineGuarantorSearchResultStatusEnum.Found)
            {
                OfflineGuarantorSearchResultDto result = results.Result;
                model = this._registrationService.Value.GetRegisterPersonalInformationViewModel(false);
                model.AddressStreet1 = result.AddressLine1;
                model.AddressStreet2 = result.AddressLine2;
                model.City = result.City;
                model.DateOfBirthDay = result.DOB?.Day.ToString();
                model.DateOfBirthMonth = result.DOB?.Month.ToString();
                model.DateOfBirthYear = result.DOB?.Year.ToString();
                model.GuarantorId = new[] {result.SourceSystemKey};
                model.FirstName = result.FirstName;
                model.LastName = result.LastName;
                if (model.ConfigSsn.Required || model.ConfigSsn.Visible)
                {
                    model.Ssn4 = result.SSN4;
                }
                model.Zip = result.PostalCode;

                if (!string.IsNullOrWhiteSpace(result.StateProvince))
                {
                    KeyValuePair<string, string>[] foundState = Geo.GetStates().Where(x => string.Equals(x.Key, result.StateProvince, StringComparison.InvariantCultureIgnoreCase) ||
                                                                                           string.Equals(x.Value, result.StateProvince, StringComparison.InvariantCultureIgnoreCase)).ToArray();

                    if (foundState.Any())
                    {
                        model.State = foundState.First().Key;
                    }
                }
            }
            
            return this.Json(new 
            {
                Status = results.Status,
                Data = model
            });
        }

        [HttpGet]
        public ActionResult CreateVpGuarantor()
        {
            RegisterPersonalInformationViewModel guarantor = this._registrationService.Value.GetRegisterPersonalInformationViewModel(isSso:false);
            CreateVpGuarantorViewModel vm = Mapper.Map<CreateVpGuarantorViewModel>(guarantor);
            return this.View(vm);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CreateVpGuarantor(CreateVpGuarantorViewModel model)
        {
            this.TryValidateModel(model);
            this._registrationService.Value.ValidateRecords(this.ModelState, model);

            if (!this.ModelState.IsValid)
            {
                return this.Json(new ResultMessage<Hashtable>(false, this.ModelState.GetErrors()));
            }
            
            //the following is copy pasted from BaseRegistrationController (patient)
            string guarantorId = string.Join("_", model.GuarantorId).TrimNullSafe();
            string lastName = model.LastName.TrimNullSafe();
            string ssn4 = model.Ssn4.TrimNullSafe();
            string zip = model.Zip.TrimNullSafe();
            DateTime dateOfBirth = DateTimeHelper.FromValues(model.DateOfBirthMonth, model.DateOfBirthDay, model.DateOfBirthYear);
            DateTime? patientDateOfBirth = this._registrationService.Value.GetPatientDateOfBirth(model, dateOfBirth);

            MatchGuarantorDto guarantor = new MatchGuarantorDto
            {
                SourceSystemKey = guarantorId,
                SSN4 = ssn4,
                FirstName = model.FirstName.TrimNullSafe(),
                LastName = lastName,
                DOB = dateOfBirth,
                AddressLine1 = model.AddressStreet1.TrimNullSafe(),
                City = model.City.TrimNullSafe(),
                StateProvince = model.State.TrimNullSafe(),
                PostalCode = zip,
            };
            
            MatchPatientDto patient = new MatchPatientDto
            {
                PatientDOB = patientDateOfBirth
            };

            MatchDataDto matchData = new MatchDataDto
            {
                Guarantor = guarantor,
                Patient = patient,
                ApplicationEnum = ApplicationEnum.VisitPay
            };

            // try to match
            GuarantorMatchResultEnum matchStatus = this._guarantorApplicationService.Value.IsGuarantorMatch(matchData);

            string message = "";
            string guarantorMatchResultAlreadyRegistered = $"This account already exists in our system, try logging in.";
            string guarantorMatchResultNoMatchFound = "We're sorry, we can't find your records. Make sure the information entered is correct and from a recent statement.";
            string guarantorMatchResultCanceledAccount = "This account has been canceled.";

            switch (matchStatus)
            {
                case GuarantorMatchResultEnum.AlreadyRegistered:
                    message = guarantorMatchResultAlreadyRegistered;
                    break;
                case GuarantorMatchResultEnum.NoMatchFound:
                    message = guarantorMatchResultNoMatchFound;
                    break;
                case GuarantorMatchResultEnum.Matched:
                    message = null;
                    break;
            }

            if (matchStatus == GuarantorMatchResultEnum.AlreadyRegistered || matchStatus == GuarantorMatchResultEnum.Matched)
            {
                bool credentialMatchCanceledAccount = this._guarantorApplicationService.Value.CredentialsMatchCanceledAccount(matchData);
                if (credentialMatchCanceledAccount)
                {
                    message = guarantorMatchResultCanceledAccount;
                }
            }

            bool success = string.IsNullOrEmpty(message) && matchStatus == GuarantorMatchResultEnum.Matched;
            if (!success)
            {
                this.ModelState.AddModelError(string.Empty, message);
                return this.Json(new ResultMessage<Hashtable>(false, this.ModelState.GetErrors()));
            }

            VisitPayUserDto userDto = new VisitPayUserDto
            {
                AddressStreet1 = model.AddressStreet1,
                AddressStreet2 = string.IsNullOrWhiteSpace(model.AddressStreet2) ? null : model.AddressStreet2,
                City = model.City,
                DateOfBirth = dateOfBirth,
                FirstName = model.FirstName,
                LastName = model.LastName,
                SSN4 = Convert.ToInt32(ssn4),
                State = Geo.GetStates().Select(x => x.Key).FirstOrDefault(x => string.Equals(x, model.State, StringComparison.InvariantCultureIgnoreCase)),
                UserName = Guid.NewGuid().ToString(),
                Zip = model.Zip
            };

            GuarantorDto guarantorDto = this._visitPayUserApplicationService.Value.CreateOfflinePatient(userDto, guarantorId, patientDateOfBirth);

            return this.Json(new ResultMessage(true, $"{guarantorDto.VpGuarantorId}"));
        }
        
        [HttpGet]
        [VpAuthorize(Roles = VisitPayRoleStrings.Csr.CallCenter)]
        public ActionResult VpGuarantorHasStatement(int vpGuarantorId)
        {
            int visitPayUserId = this._visitPayUserApplicationService.Value.GetVisitPayUserId(vpGuarantorId);
            bool hasStatement = this._registrationService.Value.HasStatement(visitPayUserId);
            return new HttpStatusCodeResult(hasStatement ? HttpStatusCode.OK : HttpStatusCode.NoContent);
        }
        
        #endregion
        
        #region finance plan
        
        [HttpGet]
        public async Task<ActionResult> CreateFinancePlan(int vpGuarantorId)
        {
            VisitPayUserDto visitPayUserDto = this._offlineUserApplicationService.Value.FindByGuarantorId(vpGuarantorId);
            UserPaymentOptionDto option = await this._paymentOptionApplicationService.Value.GetFinancePlanPaymentOptionAsync(vpGuarantorId, visitPayUserDto.VisitPayUserId);
            
            ArrangePaymentViewModel model = new ArrangePaymentViewModel();
            ModelService.CreateArrangePaymentViewModelContext(this, this.User, model, vpGuarantorId, visitPayUserDto.VisitPayUserId);

            model.IsOfflineGuarantor = true;
            model.PaymentOptions = new List<PaymentOptionViewModel>
            {
                Mapper.Map<PaymentOptionViewModel>(option)
            };
            
            return this.View(model);
        }

        #endregion

        #region mailing address

        [HttpGet]
        public async Task<JsonResult> MailingAddress(int visitPayUserId)
        {
            // get current mailing address, if any
            AddressDto addressDto = 
                await this._visitPayUserAddressApplicationService.Value
                    .GetAddressAsync(visitPayUserId, VisitPayUserAddressTypeEnum.Mailing);

            if (addressDto == null)
            {
                // get physical address, if any, to pre-populate form
                addressDto = 
                    await this._visitPayUserAddressApplicationService.Value
                        .GetAddressAsync(visitPayUserId, VisitPayUserAddressTypeEnum.Physical);
            }

            AddressViewModel model = Mapper.Map<AddressViewModel>(addressDto);
            bool userHasEmailCommunication = this._visitPayUserApplicationService.Value.UserHasCommunicationPreference(visitPayUserId,CommunicationMethodEnum.Email);
            model.ShowRemoveEmailPreferences = userHasEmailCommunication;
            return this.Json(model, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public PartialViewResult MailingAddressPartial()
        {
            return this.PartialView("~/Views/Offline/_ConfirmMailingAddressModal.cshtml", new AddressViewModel());
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<JsonResult> MailingAddressConfirmAsync(int visitPayUserId, AddressViewModel address, bool validate, bool addPreferences)
        {
            if (!this.ModelState.IsValid)
            {
                return this.Json(new DataResult<string, bool>(string.Empty, false));
            }

            AddressDto addressDto = Mapper.Map<AddressDto>(address);

            AddressUpdateResultDto result = 
                await this._visitPayUserAddressApplicationService.Value
                    .UpdateAddressAsync(visitPayUserId, addressDto, VisitPayUserAddressTypeEnum.Mailing, validate);

            if (result.Result && addPreferences)
            {
                this._visitPayUserApplicationService.Value.AddMailCommunicationPreferences(Mapper.Map<JournalEventHttpContextDto>(System.Web.HttpContext.Current), visitPayUserId,this.CurrentUserId);
                if (address.RemoveEmailPreferences)
                {
                    this._visitPayUserApplicationService.Value.RemoveEmailCommunicationPreferences(Mapper.Map<JournalEventHttpContextDto>(System.Web.HttpContext.Current), visitPayUserId, this.CurrentUserId);
                }
            }
            
            return this.Json(result);
        }

        #endregion
        
        #region send token
        
        [HttpGet]
        [OverrideAuthorization]
        public async Task<ActionResult> SendToken(int vpGuarantorId, bool sendFinancePlanConfirmation = false)
        {
            VisitPayUserDto visitPayUserDto = this._offlineUserApplicationService.Value.FindByGuarantorId(vpGuarantorId);
            if (visitPayUserDto == null)
            {
                this.Logger.Value.Warn(() =>  $"{nameof(OfflineController)}:{nameof(this.SendToken)} - couldn't find offline visitpayuser for vpguarantor = {vpGuarantorId}");
                return this.Content(string.Empty);
            }

            CmsRegionEnum cmsRegionEnum = sendFinancePlanConfirmation ? CmsRegionEnum.OfflineFinancePlanConfirmEmailAddress : CmsRegionEnum.OfflineResendLoginToken;
            CmsVersionDto cmsVersionDto = await this._contentApplicationService.Value.GetCurrentVersionAsync(cmsRegionEnum);


            bool userHasPaperCommunicationPreference = this._visitPayUserApplicationService.Value.UserHasCommunicationPreference(visitPayUserDto.VisitPayUserId, CommunicationMethodEnum.Mail);

            SendOfflineTokenViewModel model = new SendOfflineTokenViewModel
            {
                EmailAddress = visitPayUserDto.Email,
                SendFinancePlanNotification = sendFinancePlanConfirmation,
                Content = Mapper.Map<CmsViewModel>(cmsVersionDto),
                ShowRemovePaperCommunicationPreferences = userHasPaperCommunicationPreference
            };
            
            return this.PartialView("~/Views/Offline/_SendOfflineToken.cshtml", model);
        }

        [HttpPost]
        [OverrideAuthorization]
        [ValidateAntiForgeryToken]
        public JsonResult SendToken(SendOfflineTokenViewModel model)
        {
            SendOfflineTokenDto tokenDto = new SendOfflineTokenDto(model.VpGuarantorId, model.EmailAddress)
            {
                PhoneNumber = model.PhoneNumber,
                SendSms = model.SendSms
            };

            VisitPayUserDto visitPayUserDto = this._offlineUserApplicationService.Value.FindByGuarantorId(model.VpGuarantorId);
            bool result;

            if (model.SendFinancePlanNotification)
            {
                result = this._offlineUserApplicationService.Value.SendFinancePlanNotification(tokenDto);
            }
            else
            {
                result = this._offlineUserApplicationService.Value.SendLoginToken(tokenDto);
            }

            if (visitPayUserDto.Email.IsNullOrEmpty() && model.EmailAddress.IsNotNullOrEmpty()) //User was not subscribed to electronic communications, but now is.
            {
                this._visitPayUserApplicationService.Value.AddEmailCommunicationPreferences(Mapper.Map<JournalEventHttpContextDto>(System.Web.HttpContext.Current), visitPayUserDto.VisitPayUserId, this.CurrentUserId);
            }

            if (model.RemovePaperCommunicationPreferences)
            {
                visitPayUserDto = this._offlineUserApplicationService.Value.FindByGuarantorId(model.VpGuarantorId);
                this._visitPayUserApplicationService.Value.RemoveMailCommunicationPreferences(Mapper.Map<JournalEventHttpContextDto>(System.Web.HttpContext.Current),  visitPayUserDto.VisitPayUserId, this.CurrentUserId);
            }

            return this.Json(result);
        }

        #endregion
        
        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult SetGuarantorAutoPay(int vpGuarantorId, bool isAutoPay)
        {
            bool result = this._guarantorApplicationService.Value.SetGuarantorAutoPay(vpGuarantorId, isAutoPay);
            return this.Json(result);
        }
    }
}
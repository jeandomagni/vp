﻿namespace Ivh.Web.Client.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web;
    using System.Web.Mvc;
    using Application.Content.Common.Interfaces;
    using Application.Core.Common.Dtos;
    using Application.Core.Common.Interfaces;
    using Application.FinanceManagement.Common.Dtos;
    using Application.FinanceManagement.Common.Interfaces;
    using Application.SecureCommunication.Common.Dtos;
    using Application.SecureCommunication.Common.Interfaces;
    using Application.User.Common.Dtos;
    using Attributes;
    using AutoMapper;
    using Common.Base.Enums;
    using Common.EventJournal;
    using Common.VisitPay.Enums;
    using Common.VisitPay.Strings;
    using Common.Web.Controllers;
    using Common.Web.Extensions;
    using Common.Web.Interfaces;
    using Common.Web.Models;
    using Common.Web.Models.Support;
    using Models.Support;

    [VpAuthorize(Roles = VisitPayRoleStrings.System.Client + "," + VisitPayRoleStrings.Admin.Administrator + ",")]
    [VpAuthorize(Roles = VisitPayRoleStrings.Csr.CSR + "," + VisitPayRoleStrings.Payment.PaymentRefund + "," + VisitPayRoleStrings.Payment.PaymentVoid + "," + VisitPayRoleStrings.Csr.SupportAdmin + "," + VisitPayRoleStrings.Csr.SupportCreate + "," + VisitPayRoleStrings.Csr.SupportView + "," + VisitPayRoleStrings.Csr.UserAdmin)]
    public class SupportController : BaseSupportController
    {
        private readonly Lazy<ISupportRequestApplicationService> _supportRequestApplicationService;
        private readonly Lazy<ISupportTopicApplicationService> _supportTopicApplicationService;
        private readonly Lazy<ISupportTemplateApplicationService> _supportTemplateApplicationService;

        public SupportController(
            Lazy<IBaseControllerService> baseControllerService,
            Lazy<IContentApplicationService> contentApplicationService,
            Lazy<IConsolidationGuarantorApplicationService> consolidationGuarantorApplicationService,
            Lazy<ISupportRequestApplicationService> supportRequestApplicationService,
            Lazy<ISupportTopicApplicationService> supportTopicApplicationService,
            Lazy<ITreatmentLocationApplicationService> treatmentLocationApplicationService,
            Lazy<IVisitApplicationService> visitApplicationService,
            Lazy<IVisitPayUserApplicationService> visitPayUserApplicationService,
            Lazy<IVisitPayUserJournalEventApplicationService> visitPayUserJournalEventApplicationService,
            Lazy<ISupportTemplateApplicationService> supportTemplateApplicationService)
            : base(
            baseControllerService, 
            contentApplicationService,
            consolidationGuarantorApplicationService, 
            supportRequestApplicationService, 
            supportTopicApplicationService, 
            treatmentLocationApplicationService, 
            visitApplicationService, 
            visitPayUserApplicationService, 
            visitPayUserJournalEventApplicationService)
        {
            this._supportRequestApplicationService = supportRequestApplicationService;
            this._supportTopicApplicationService = supportTopicApplicationService;
            this._supportTemplateApplicationService = supportTemplateApplicationService;
        }

        #region create

        [HttpGet]
        public PartialViewResult CreateSupportRequest(int? visitId, int visitPayUserId)
        {
            GuarantorDto guarantor = this.VisitPayUserApplicationService.Value.GetGuarantorFromVisitPayUserId(visitPayUserId);
            if (guarantor == null || guarantor.AccountClosedDate.HasValue)
            {
                return this.PartialView("Error");
            }

            Models.Support.CreateSupportRequestViewModel model = Mapper.Map<Models.Support.CreateSupportRequestViewModel>(this.CreateSupportRequestModel(visitPayUserId, guarantor.VpGuarantorId, visitId,false));
            
            return this.PartialView("_CreateSupportRequest", model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult CreateSupportRequest(Models.Support.CreateSupportRequestViewModel model)
        {
            if (!this.ModelState.IsValid)
            {
                return this.Json(new ResultMessage(false, ""));
            }

            GuarantorDto guarantor = this.VisitPayUserApplicationService.Value.GetGuarantorFromVisitPayUserId(model.VisitPayUserId);
            if (guarantor == null || guarantor.AccountClosedDate.HasValue)
            {
                return this.Json(new ResultMessage(false, "Invalid or closed account"));
            }

            SupportRequestCreateDto supportRequestCreateDto = new SupportRequestCreateDto
            {
                AssignedVisitPayUserId = model.AssignedVisitPayUserId,
                CreatedByVisitPayUserId = this.CurrentUserId,
                MessageBody = model.MessageBody,
                SubmittedForVisitPayUserId = model.SubmittedForVisitPayUserId,
                SupportTopicId = model.SupportSubTopicId,
                VisitId = model.VisitId,
                VisitPayUserId = model.VisitPayUserId
            };

            SupportRequestDto supportRequestDto = this.SupportRequestApplicationService.Value.CreateSupportRequest(supportRequestCreateDto);

            return this.Json(new ResultMessage<dynamic>(true, new
            {
                SupportRequestId = supportRequestDto.SupportRequestId,
                SupportRequestMessageId = supportRequestDto.SupportRequestMessages.First().SupportRequestMessageId
            }));
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult SupportRequestData(int? visitId, int visitPayUserId)
        {
            List<VisitPayUserDto> allUsers = new List<VisitPayUserDto>
            {
                this.VisitPayUserApplicationService.Value.FindById(visitPayUserId.ToString())
            };

            List<ConsolidationGuarantorDto> managedGuarantors = this.ConsolidationGuarantorApplicationService.Value.GetAllManagedGuarantors(visitPayUserId).Where(x => x.IsActive).ToList();
            if (managedGuarantors.Any())
            {
                allUsers.AddRange(managedGuarantors.Select(x => x.ManagedGuarantor.User));
            }

            List<VisitDto> allVisits = new List<VisitDto>();

            allUsers.ForEach(user =>
            {
                List<VisitDto> visits = this.VisitApplicationService.Value.GetVisits(user.VisitPayUserId, new VisitFilterDto
                {
                    SortField = "DischargeDate",
                    SortOrder = "desc",
                    IsUnmatched = false
                }, 1, 50).Visits.ToList();

                if (visits.Any())
                {
                    allVisits.AddRange(visits);
                }
            });
            
            VisitDto currentVisit = null;
            string currentVisitFirstName = null;
            string currentVisitLastName = null;

            if (visitId.HasValue)
            {
                currentVisit = allVisits.FirstOrDefault(x => x.VisitId == visitId.Value);
                if (currentVisit != null)
                {
                    currentVisitFirstName = currentVisit.PatientFirstName;
                    currentVisitLastName = currentVisit.PatientLastName;
                }
            }

            List<SelectListItem> supportAdminUsers = new List<SelectListItem>();
            if (this.User.IsInRole(VisitPayRoleStrings.Csr.SupportAdmin))
            {
                supportAdminUsers = this._supportRequestApplicationService.Value.GetAssignableUsers(
                    assignedOnly: false,  
                    assignedVisitPayUserId: null)
                    .Select(x => new SelectListItem { Text = x.Value, Value = x.Key.ToString() }).ToList();
            }

            return this.Json(new
            {
                TreatmentDate = currentVisit != null ? currentVisit.VisitDisplayDate : "",
                PatientFirstName = currentVisitFirstName,
                PatientLastName = currentVisitLastName,
                SelectedVisitPayUserId = currentVisit?.VPGuarantor.User.VisitPayUserId ?? visitPayUserId,
                SelectedVisitId = currentVisit?.VisitId,
                SupportAdminUsers = supportAdminUsers,
                Users = allUsers.Select(user => new
                {
                    Name = user.DisplayFirstNameLastName,
                    VisitPayUserId = user.VisitPayUserId,
                    Visits = allVisits.Where(x => x.VPGuarantor.User.VisitPayUserId == user.VisitPayUserId).Select(visit => new
                    {
                        Value = visit.VisitId,
                        Text = $"{visit.VisitDisplayDate}, {visit.SourceSystemKeyDisplay}, {visit.PatientName}, {(visit.BillingApplication == "HB" ? "Hospital" : "Physician")}"
                    })
                })
            });
        }

        #endregion

        #region details

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult SupportRequest(int supportRequestId, int visitPayUserId)
        {
            this._supportRequestApplicationService.Value.SetRead(supportRequestId, visitPayUserId, SupportRequestMessageTypeEnum.FromGuarantor);

            SupportRequestViewModel model = this.GetSupportRequest(supportRequestId, visitPayUserId, true);
            model.SupportAdminUsers = this._supportRequestApplicationService.Value.GetAssignableUsers(
                assignedOnly: false,  
                assignedVisitPayUserId: model.AssignedVisitPayUserId)
                .Select(x => new SelectListItem { Text = x.Value, Value = x.Key.ToString() }).ToList();

            int guarantorId = this.VisitPayUserApplicationService.Value.GetGuarantorIdFromVisitPayUserId(visitPayUserId);
            this.VisitPayUserJournalEventApplicationService.Value.LogClientSupportRequestView(this.CurrentUserId, guarantorId, supportRequestId, Mapper.Map<JournalEventHttpContextDto>(System.Web.HttpContext.Current));

            return this.Json(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult SaveInternalNote(SupportRequestGenericMessageViewModel model, int visitPayUserId)
        {
            GuarantorDto guarantor = this.VisitPayUserApplicationService.Value.GetGuarantorFromVisitPayUserId(visitPayUserId);
            if (guarantor == null || !guarantor.AccountClosedDate.HasValue)
            {
                SupportRequestMessageDto supportRequestMessageDto = this._supportRequestApplicationService.Value.SaveInternalNote(model.SupportRequestId, visitPayUserId, model.MessageBody, this.CurrentUserId);

                return this.Json(new ResultMessage(true, supportRequestMessageDto.SupportRequestMessageId.ToString()));
            }
            return this.Json(new ResultMessage(false, "Invalid or closed account"));
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult SaveDetails(int supportRequestId, int visitPayUserId, int? assignedToVisitPayUserId)
        {
            this._supportRequestApplicationService.Value.SaveDetails(supportRequestId, visitPayUserId, assignedToVisitPayUserId, this.CurrentUserId);

            return this.Json(true);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public PartialViewResult Attachments(int supportRequestId, int? supportRequestMessageId, int visitPayUserId)
        {
            return this.GetAttachments(supportRequestId, supportRequestMessageId, visitPayUserId, true);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [VpAuthorize(Roles = VisitPayRoleStrings.Miscellaneous.IvhAdmin)]
        public JsonResult DeleteAttachment(int supportRequestId, int supportRequestMessageAttachmentId, int visitPayUserId, int vpGuarantorId)
        {
            this._supportRequestApplicationService.Value.DeleteAttachment(supportRequestId, supportRequestMessageAttachmentId, visitPayUserId);

            this.VisitPayUserJournalEventApplicationService.Value.LogDeleteAttachmentSupportRequest(this.CurrentUserId, vpGuarantorId, visitPayUserId, supportRequestId, Mapper.Map<JournalEventHttpContextDto>(System.Web.HttpContext.Current));

            return this.Json(true);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult PrepareDownload(int supportRequestId, int? supportRequestMessageAttachmentId, int visitPayUserId)
        {
            return this.AttachmentPrepareDownload(supportRequestId, supportRequestMessageAttachmentId, visitPayUserId, true);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult SetSupportTopic(int supportRequestId, int? supportTopicId, int visitPayUserId)
        {
            this._supportRequestApplicationService.Value.SetSupportTopic(supportRequestId, supportTopicId, visitPayUserId);
            return this.Json(true);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public void SaveDraft(int supportRequestId, int visitPayUserId, string messageBody)
        {
            GuarantorDto guarantor = this.VisitPayUserApplicationService.Value.GetGuarantorFromVisitPayUserId(visitPayUserId);
            this._supportRequestApplicationService.Value.SaveDraft(supportRequestId, visitPayUserId, messageBody, this.User.CurrentUserId());
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public void FlagForFollowUp(int supportRequestId, int visitPayUserId, bool flagForFollowUp)
        {
            GuarantorDto guarantor = this.VisitPayUserApplicationService.Value.GetGuarantorFromVisitPayUserId(visitPayUserId);
            this._supportRequestApplicationService.Value.FlagForFollowUp(supportRequestId, visitPayUserId, flagForFollowUp, this.User.CurrentUserId());
        }

        #endregion

        #region reply

        [HttpGet]
        public PartialViewResult SupportRequestReply()
        {
            return this.PartialView("_SupportRequestReply", new SupportRequestGenericMessageViewModel());
        }

        [HttpGet]
        public PartialViewResult SupportRequestReplyToGuarantor()
        {
            return this.PartialView("_SupportRequestReplyToGuarantor", new SupportRequestGenericMessageViewModel());
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult SaveSupportRequestReply(int supportRequestId, int visitPayUserId, string messageBody)
        {
            GuarantorDto guarantor = this.VisitPayUserApplicationService.Value.GetGuarantorFromVisitPayUserId(visitPayUserId);
            if (guarantor == null || !guarantor.AccountClosedDate.HasValue)
            {
                SupportRequestMessageDto supportRequestMessageDto = this._supportRequestApplicationService.Value.SaveClientMessage(supportRequestId, visitPayUserId, messageBody, this.CurrentUserId);

                return this.Json(new ResultMessage(true, supportRequestMessageDto.SupportRequestMessageId.ToString()));
            }
            return this.Json(new ResultMessage(false, "Invalid or closed account"));
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult Topics(int visitPayUserId)
        {
            return this.Json(this._supportTopicApplicationService.Value.GetSupportTopicsGroupedTemplates(visitPayUserId, this.CurrentUserId));
        }

        #endregion

        #region actions: close/reopen

        [HttpPost]
        [ValidateAntiForgeryToken]
        public PartialViewResult SupportRequestAction(SupportRequestStatusEnum supportRequestStatus, int supportRequestId, string supportRequestDisplayId, int visitPayUserId)
        {
            GuarantorDto guarantor = this.VisitPayUserApplicationService.Value.GetGuarantorFromVisitPayUserId(visitPayUserId);
            if (guarantor == null || !guarantor.AccountClosedDate.HasValue)
            {
                SupportRequestActionViewModel model = new SupportRequestActionViewModel
                {
                    VisitPayUserId = visitPayUserId
                };

                return base.SupportRequestAction(supportRequestStatus, supportRequestId, supportRequestDisplayId, model);
            }
            return this.PartialView("Error");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult CompleteSupportRequestAction(SupportRequestActionViewModel model)
        {
            GuarantorDto guarantor = this.VisitPayUserApplicationService.Value.GetGuarantorFromVisitPayUserId(model.VisitPayUserId);
            if (guarantor == null || !guarantor.AccountClosedDate.HasValue)
            {
                return base.CompleteSupportRequestAction(model, model.VisitPayUserId);
            }
            return this.Json(new ResultMessage(false, "Invalid or closed account"));
        }

        #endregion

        #region shared

        [HttpGet]
        public JsonResult SupportTopics()
        {
            IList<SupportTopicGroupItemDto> supportTopicGroupItemsDto = this.SupportTopicApplicationService.Value.GetSupportTopicsGrouped();

            return this.Json(new
            {
                SupportTopics = supportTopicGroupItemsDto
            }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult UploadSupportRequestMessageAttachment()
        {
            HttpRequestBase httpRequest = this.HttpContext.Request;

            int visitPayUserId;
            if (int.TryParse(httpRequest.Form["VisitPayUserId"], out visitPayUserId))
            {
                GuarantorDto guarantor = this.VisitPayUserApplicationService.Value.GetGuarantorFromVisitPayUserId(visitPayUserId);
                if (guarantor == null || !guarantor.AccountClosedDate.HasValue)
                {
                    return this.UploadSupportRequestMessageAttachment(httpRequest, visitPayUserId);
                }
            }
            return this.Json(new ResultMessage(false, "Invalid or closed account"));
        }

        #endregion

        #region print

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult PreparePrintSupportRequest(int supportRequestId, int visitPayUserId, string sortOrderGuarantor, string sortOrderInternal)
        {
            return base.PrintSupportRequest(supportRequestId, visitPayUserId, sortOrderGuarantor, sortOrderInternal);
        }

        [HttpGet]
        public new ActionResult PrintSupportRequest()
        {
            PrintSupportRequestViewModel model = base.PrintSupportRequest(true);

            return this.View("~/Views/Support/PrintSupportRequest.cshtml", model);
        }

        #endregion

        #region SupportTemplates

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult SaveSupportTemplate(SupportTemplateDto template, bool globalTemplate)
        {
            if (globalTemplate && this.UserCanManageGlobalTemplates())
            {
                //Saving as global template
                template.VisitPayUserId = null;
            }
            else
            {
                //Non-global template, set user ID
                template.VisitPayUserId = this.CurrentUserId;
            }

            SupportTemplateDto savedTemplate = this._supportTemplateApplicationService.Value.SaveSupportTemplate(template, base.CurrentUserId, this.UserCanManageGlobalTemplates());
            return this.Json(savedTemplate);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult SupportTemplates(int guarantorVisitPayUserId)
        {
            IList<SupportTemplateDto> results = this._supportTemplateApplicationService.Value.GetSupportTemplates(base.CurrentUserId, guarantorVisitPayUserId);
            return this.Json(results);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult GetSupportTemplate(int supportTemplateId)
        {
            SupportTemplateDto template = this._supportTemplateApplicationService.Value.GetSupportTemplate(supportTemplateId, this.UserCanManageGlobalTemplates());
            return this.Json(template);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public void RemoveSupportTemplate(int supportTemplateId)
        {
            this._supportTemplateApplicationService.Value.DeleteSupportTemplate(supportTemplateId, base.CurrentUserId, this.UserCanManageGlobalTemplates());
        }

        private bool UserCanManageGlobalTemplates()
        {
            return base.User.IsInRole(VisitPayRoleStrings.Csr.SupportTemplateAdmin);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult SupportTemplateReplacementTags()
        {
            IList<string> replacementTags = this._supportTemplateApplicationService.Value.GetCommonTemplateReplacementTags();
            return this.Json(replacementTags);
        }

        #endregion SupportTemplates
    }
}
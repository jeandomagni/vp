﻿namespace Ivh.Web.Client.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using System.Web.Mvc;
    using Application.Content.Common.Interfaces;
    using Application.Core.Common.Dtos;
    using Application.Core.Common.Interfaces;
    using Attributes;
    using AutoMapper;
    using Common.Base.Enums;
    using Common.Cache;
    using Common.VisitPay.Enums;
    using Common.VisitPay.Strings;
    using Common.Web.Controllers;
    using Common.Web.Filters;
    using Common.Web.Interfaces;
    using Common.Web.Models.Shared;
    using Microsoft.AspNet.Identity;
    using Models.Sso;

    [Authorize(Roles = VisitPayRoleStrings.System.Client + "," + VisitPayRoleStrings.Admin.Administrator)]
    [VpAuthorize(Roles = VisitPayRoleStrings.Csr.CSR + "," + VisitPayRoleStrings.Csr.UserAdmin + "," + VisitPayRoleStrings.Csr.SupportView + "," + VisitPayRoleStrings.Csr.SupportAdmin)]
    [RequireFeatureAny(VisitPayFeatureEnum.MyChartSso, VisitPayFeatureEnum.HealthEquityFeature)]
    public class SsoController : BaseSsoController
    {
        private readonly Lazy<IVisitPayUserApplicationService> _visitPayUserApplicationService;

        public SsoController(Lazy<IBaseControllerService> baseControllerService,
            Lazy<ISsoApplicationService> ssoApplicationService,
            Lazy<IContentApplicationService> contentApplicationService,
            Lazy<IGuarantorApplicationService> guarantorApplicationService,
            Lazy<IVisitPayUserApplicationService> visitPayUserApplicationService,
            Lazy<IDistributedCache> distributedCache
            )
            : base(
                baseControllerService,
                ssoApplicationService,
                contentApplicationService,
                guarantorApplicationService,
                distributedCache)
        {
            this._visitPayUserApplicationService = visitPayUserApplicationService;
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Settings(int visitPayUserId)
        {
            return this.PartialView("_SettingsModal", this.GetSettingsModel(visitPayUserId));
        }
        
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<JsonResult> DeclineConfirmation()
        {
            return this.Json(Mapper.Map<CmsViewModel>(await this.ContentApplicationService.Value.GetCurrentVersionAsync(CmsRegionEnum.SsoDeclineConfirmation)));
        }
        
        [HttpPost]
        [ValidateAntiForgeryToken]
        public PartialViewResult Decline(SsoProviderEnum ssoProvider, int visitPayUserId)
        {
            this.Decline(visitPayUserId, ssoProvider, this.CurrentUserId);

            return this.PartialView("_Settings", this.GetSettingsModel(visitPayUserId).FirstOrDefault(x => x.SsoProviderId == (int) ssoProvider));
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public PartialViewResult Ignore(SsoProviderEnum ssoProvider, bool isIgnored, int visitPayUserId)
        {
            this.Ignore(visitPayUserId, ssoProvider, isIgnored, this.CurrentUserId);

            return this.PartialView("_Settings", this.GetSettingsModel(visitPayUserId).FirstOrDefault(x => x.SsoProviderId == (int) ssoProvider));
        }

        private List<ClientSsoProviderWithUserSettingsViewModel> GetSettingsModel(int visitPayUserId)
        {
            int vpGuarantorId = this._visitPayUserApplicationService.Value.GetGuarantorIdFromVisitPayUserId(visitPayUserId);

            IList<SsoProviderWithUserSettingsDto> providersWithSettings = this.SsoApplicationService.Value.GetAllProvidersWithUserSettings(visitPayUserId);

            List<ClientSsoProviderWithUserSettingsViewModel> model = Mapper.Map<List<ClientSsoProviderWithUserSettingsViewModel>>(providersWithSettings);

            model.ForEach(setting =>
            {
                setting.IsUserEligible = (SsoProviderEnum) setting.SsoProviderId != SsoProviderEnum.HealthEquityOutbound || this._visitPayUserApplicationService.Value.IsUserEligibleForHealthEquityOutboundSso(vpGuarantorId).GetValueOrDefault(false);
                setting.Body = this.CmsView(setting, true);
                setting.VisitPayUserId = visitPayUserId;
            });

            return model;
        }
    }
}
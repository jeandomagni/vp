﻿namespace Ivh.Web.Client.Controllers
{
    using System;
    using System.Web.Mvc;
    using Common.VisitPay.Strings;
    using Common.Web.Controllers;
    using Common.Web.Filters;
    using Common.Web.Interfaces;

    [IpAccessCheck]
    [Authorize(Roles = VisitPayRoleStrings.System.Client + "," + VisitPayRoleStrings.Admin.Administrator)]
    public class BaseController : BaseWebController
    {
        public BaseController(Lazy<IBaseControllerService> baseControllerService)
            : base(baseControllerService)
        {
        }
    }
}
﻿
namespace Ivh.Web.Client.Controllers
{
    using Application.Core.Common.Dtos;
    using Application.Core.Common.Interfaces;
    using Attributes;
    using AutoMapper;
    using Common.VisitPay.Enums;
    using Common.VisitPay.Strings;
    using Common.Web.Filters;
    using Common.Web.Interfaces;
    using Models.Chat;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web.Mvc;
    using Common.Web.Controllers;

    [RequireFeature(VisitPayFeatureEnum.FeatureChatIsEnabled)]
    [VpAuthorize(Roles = VisitPayRoleStrings.Csr.ChatOperator + "," + VisitPayRoleStrings.Csr.ChatOperatorAdmin)]
    public class ChatController : BaseChatController
    {
        public ChatController(
            Lazy<IBaseControllerService> baseControllerService,
            Lazy<IChatApplicationService> chatApplicationService
            ) : base(baseControllerService, chatApplicationService)
        {
            
        }

        // GET: Chat
        public ActionResult Index()
        {
            return this.View();
        }

        [HttpGet]
        public PartialViewResult ChatDialog()
        {
            return this.PartialView("_ChatDialog");
        }

        [HttpPost]
        public JsonResult GoToAccount(Guid guarantorChatToken)
        {
            int guarantorId = this.ChatApplicationService.Value.GetGuarantorIdFromToken(guarantorChatToken);
            if (guarantorId > 0)
            {
                string url = this.Url.Action("GuarantorAccount", "Search", new { id = guarantorId });
                return this.Json(url);
            }

            return null;
        }

        //Admin Only
        [HttpGet]
        [VpAuthorize(Roles = VisitPayRoleStrings.Csr.ChatOperatorAdmin)]
        public ActionResult ChatAdmin()
        {
            IList<ChatAvailabilityDto> availabilities = base.ChatApplicationService.Value.GetChatAvailabilities();

            IList<ChatAvailabilityViewModel> model = Mapper.Map<IList<ChatAvailabilityViewModel>>(availabilities);
            return this.View(model);
        }

        //Admin Only
        [HttpPost]
        [VpAuthorize(Roles = VisitPayRoleStrings.Csr.ChatOperatorAdmin)]
        public JsonResult UpdateChatAvailability(ChatAvailabilityViewModel[] availabilities)
        {
            if (!this.ModelState.IsValid)
            {
                return this.Json(new
                {
                    Success = false,
                    Errors = ModelState.Values.SelectMany(v => v.Errors).Select(v => v.ErrorMessage).ToList()
                });
            }
            
            if (availabilities != null)
            {
                //Validate that there are no duplicate dates
                List<DateTime> duplicates = availabilities.Where(a => a.Date.HasValue)
                                                          .GroupBy(a => a.Date.Value.Date)
                                                          .Where(g => g.Count() > 1)
                                                          .Select(g => g.Key)
                                                          .ToList();
                if (duplicates.Count > 0)
                {
                    //Have duplicate dates, invalid
                    List<string> errorMessages = duplicates.Select(d => $"Duplicate date '{d.Date.ToString("MM/dd/yyyy")}'").ToList();
                    return this.Json(new
                    {
                        Success = false,
                        Errors = errorMessages
                    });
                }

                IList<ChatAvailabilityDto> dtos = Mapper.Map<IList<ChatAvailabilityDto>>(availabilities.ToList());
                foreach (ChatAvailabilityDto dto in dtos)
                {
                    base.ChatApplicationService.Value.EditChatAvailability(dto);
                }
            }
            
            return this.Json(new
            {
                Success = true
            });
        }

        //Admin Only
        [HttpPost]
        [VpAuthorize(Roles = VisitPayRoleStrings.Csr.ChatOperatorAdmin)]
        public JsonResult DeleteChatAvailability(int chatAvailabilityId)
        {
            base.ChatApplicationService.Value.DeleteChatAvailability(chatAvailabilityId);
            
            return this.Json(true);
        }

        [HttpPost]
        public JsonResult ConnectToThread(string chatThreadId, Guid guarantorChatToken)
        {
            int vpGuarantorId = this.ChatApplicationService.Value.GetGuarantorIdFromToken(guarantorChatToken);
            base.ChatApplicationService.Value.ConnectToChatThread(this.CurrentUserId, vpGuarantorId, chatThreadId);

            return this.Json(true);
        }

        [HttpPost]
        public JsonResult DisconnectFromThread(string chatThreadId, Guid guarantorChatToken)
        {
            int vpGuarantorId = this.ChatApplicationService.Value.GetGuarantorIdFromToken(guarantorChatToken);
            base.ChatApplicationService.Value.DisconnectFromChatThread(this.CurrentUserId, vpGuarantorId, chatThreadId);

            return this.Json(true);
        }

        [HttpPost]
        public JsonResult EndThread(string chatThreadId, Guid guarantorChatToken)
        {
            int vpGuarantorId = this.ChatApplicationService.Value.GetGuarantorIdFromToken(guarantorChatToken);
            base.ChatApplicationService.Value.EndChatThread(this.CurrentUserId, vpGuarantorId, chatThreadId);

            return this.Json(true);
        }
    }
}
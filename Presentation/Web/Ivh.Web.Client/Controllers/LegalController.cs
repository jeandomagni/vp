﻿namespace Ivh.Web.Client.Controllers
{
    using System;
    using System.Threading.Tasks;
    using System.Web.Mvc;
    using System.Web.SessionState;
    using Application.Content.Common.Dtos;
    using Application.Content.Common.Interfaces;
    using Application.Core.Common.Interfaces;
    using Application.FinanceManagement.Common.Interfaces;
    using AutoMapper;
    using Common.Base.Utilities.Helpers;
    using Common.VisitPay.Enums;
    using Common.Web.Controllers;
    using Common.Web.Interfaces;
    using Common.Web.Models.Legal;
    using Common.Web.Models.Shared;
    using Provider.Pdf;

    [AllowAnonymous]
    public class LegalController : BaseLegalController
    {
        public LegalController(
            Lazy<IBaseControllerService> baseControllerService,
            Lazy<IGuarantorApplicationService> guarantorApplicationService,
            Lazy<IContentApplicationService> contentApplicationService,
            Lazy<ICreditAgreementApplicationService> creditAgreementApplicationService,
            Lazy<IPdfConverter> pdfConverter)
            : base(baseControllerService, guarantorApplicationService, contentApplicationService, creditAgreementApplicationService, pdfConverter)
        {
        }

        [HttpGet]
        public override async Task<ActionResult> Index()
        {
            string view = this.User.Identity.IsAuthenticated ? "IndexAuth" : "IndexPublic";

            ContentMenuDto contentMenuDto = await this.ContentApplicationService.Value.GetContentMenuAsync(ContentMenuEnum.LegalAgreementsClient, this.User.Identity.IsAuthenticated);

            return this.View(view, Mapper.Map<ContentMenuViewModel>(contentMenuDto));
        }

        [HttpGet]
        public ActionResult LegalPartial()
        {
            CmsVersionDto cmsVersion = this.ContentApplicationService.Value.GetCurrentVersionAsync(CmsRegionEnum.Legal).Result;

            TextViewModel model = new TextViewModel
            {
                Text = cmsVersion.ContentBody,
                IsExport = false
            };

            return this.PartialView("_Legal", model);
        }

        [HttpGet]
        public ActionResult LegalPdf()
        {
            CmsVersionDto cmsVersion = this.ContentApplicationService.Value.GetCurrentVersionAsync(CmsRegionEnum.Legal).Result;

            this.ViewData.Model = new TextViewModel
            {
                Text = cmsVersion.ContentBody,
                IsExport = true
            };

            string fileName = ReplacementUtility.ReplaceWhiteSpace(ReplacementUtility.RemoveNonAlphaNumeric(this.ClientDto.ClientName), "_") + "_Legal.pdf";
            this.WritePdfInline(this.PdfConverter.Value.CreatePdfFromHtml(this.ClientDto.ClientName + " Legal", this.RenderViewToString("~/Views/Legal/_Legal.cshtml", "~/Views/Shared/_LayoutPrint.cshtml")), fileName);

            return this.Json(new { }, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult PrivacyPartial()
        {
            CmsVersionDto cmsVersion = this.ContentApplicationService.Value.GetCurrentVersionAsync(CmsRegionEnum.Privacy).Result;

            TextViewModel model = new TextViewModel
            {
                Text = cmsVersion.ContentBody,
                IsExport = false
            };

            return this.PartialView("_Privacy", model);
        }

        [HttpGet]
        public ActionResult PrivacyPdf()
        {
            CmsVersionDto cmsVersion = this.ContentApplicationService.Value.GetCurrentVersionAsync(CmsRegionEnum.Privacy).Result;

            this.ViewData.Model = new TextViewModel
            {
                Text = cmsVersion.ContentBody,
                IsExport = true
            };

            string fileName = ReplacementUtility.ReplaceWhiteSpace(ReplacementUtility.RemoveNonAlphaNumeric(this.ClientDto.ClientName), "_") + "_PrivacyPolicy.pdf";
            this.WritePdfInline(this.PdfConverter.Value.CreatePdfFromHtml(this.ClientDto.ClientName + " Privacy Policy", this.RenderViewToString("~/Views/Legal/_Privacy.cshtml", "~/Views/Shared/_LayoutPrint.cshtml")), fileName);

            return this.Json(new { }, JsonRequestBehavior.AllowGet);
        }
        
        [HttpGet]
        [Common.Web.Attributes.SessionState(SessionStateBehavior.ReadOnly)]
        public async Task<ActionResult> CreditAgreementPartial(int o)
        {
            ContentDto content = await this.CreditAgreementApplicationService.Value.GetCreditAgreementContentAsync(o, true);

            CreditAgreementViewModel model = new CreditAgreementViewModel
            {
                Text = content.ContentBody,
                Title = content.ContentTitle,
                IsExport = false,
                FinancePlanOfferId = o
            };
            
            return this.PartialView("_CreditAgreement", model);
        }

        [HttpGet]
        [Common.Web.Attributes.SessionState(SessionStateBehavior.ReadOnly)]
        public async Task<ActionResult> CreditAgreementPdf(int o, int? v)
        {
            ContentDto content = await this.CreditAgreementApplicationService.Value.GetCreditAgreementContentAsync(o, true);

            this.ViewData.Model = new CreditAgreementViewModel
            {
                Text = content.ContentBody,
                Title = content.ContentTitle,
                IsExport = true,
                FinancePlanOfferId = o
            };
            
            string clientName = ReplacementUtility.ReplaceWhiteSpace(ReplacementUtility.RemoveNonAlphaNumeric(this.ClientDto.ClientName), "_");
            string agreementName = ReplacementUtility.ReplaceWhiteSpace(ReplacementUtility.RemoveNonAlphaNumeric(content.ContentTitle), "_");
            string fileName = $"{clientName}_{agreementName}.pdf";

            this.WritePdfInline(this.PdfConverter.Value.CreatePdfFromHtml($"{this.ClientDto.ClientName} {agreementName}", this.RenderViewToString("~/Views/Legal/_CreditAgreementPdf.cshtml", "~/Views/Shared/_LayoutPrint.cshtml")), fileName);

            return this.Json(new { }, JsonRequestBehavior.AllowGet);
        }
    }
}
﻿namespace Ivh.Web.Client.Controllers
{
    using System;
    using System.Web.Mvc;
    using Application.Content.Common.Interfaces;
    using Application.Core.Common.Interfaces;
    using Application.FinanceManagement.Common.Interfaces;
    using Common.Web.Controllers;
    using Common.Web.Interfaces;
    using Microsoft.AspNet.Identity;
    using Provider.Pdf;

    public class StatementController : BaseStatementController
    {
        public StatementController(
            Lazy<IBaseControllerService> baseControllerService,
            Lazy<IContentApplicationService> contentApplicationService,
            Lazy<IStatementApplicationService> statementApplicationService,
            Lazy<IVisitPayUserApplicationService> visitPayUserApplicationService,
            Lazy<IVisitPayUserJournalEventApplicationService> visitPayUserJournalEventApplicationService,
            Lazy<IPdfConverter> pdfConverter,
            Lazy<IVisitApplicationService> visitApplicationService)
            : base(
            baseControllerService,
            contentApplicationService,
            statementApplicationService,
            visitPayUserApplicationService,
            visitPayUserJournalEventApplicationService,
            pdfConverter,
            visitApplicationService)
        {
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult Statement(int statementId, int guarantorVisitPayUserId)
        {
            return this.Json(this.GetStatement(statementId, guarantorVisitPayUserId));
        }

        [HttpGet]
        public ActionResult FinancePlanStatementDownload(int id, int guarantorVisitPayUserId)
        {
            this.WritePdfInline(this.FinancePlanStatementDownload(id, guarantorVisitPayUserId, this.CurrentUserId), "FinancePlanStatement.pdf");

            return null;
        }

        [HttpGet]
        public ActionResult VisitBalanceSummaryDownload(int id, int guarantorVisitPayUserId)
        {
            this.WritePdfInline(this.StatementDownload(id, guarantorVisitPayUserId, this.CurrentUserId), "VisitBalanceSummary.pdf");

            return null;
        }
    }
}
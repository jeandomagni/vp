﻿namespace Ivh.Web.Client.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using System.Web.Mvc;
    using System.Web.SessionState;
    using Application.Content.Common.Dtos;
    using Application.Content.Common.Interfaces;
    using Application.Core.Common.Dtos;
    using Application.Core.Common.Interfaces;
    using Application.FinanceManagement.Common.Dtos;
    using Application.FinanceManagement.Common.Interfaces;
    using Application.User.Common.Dtos;
    using Attributes;
    using AutoMapper;
    using Common.EventJournal;
    using Common.VisitPay.Constants;
    using Common.VisitPay.Enums;
    using Common.VisitPay.Strings;
    using Common.Web.Controllers;
    using Common.Web.Interfaces;
    using Common.Web.Models;
    using Common.Web.Models.Account;
    using Common.Web.Models.Payment;
    using Common.Web.Models.Shared;
    using Domain.Logging.Interfaces;
    using Domain.Settings.Interfaces;
    using Models.Payment;
    using Services;

    [Authorize(Roles = VisitPayRoleStrings.System.Client + "," + VisitPayRoleStrings.Admin.Administrator)]
    public class PaymentController : BasePaymentController
    {
        private readonly Lazy<GuarantorFilterService> _guarantorFilterService;

        public PaymentController(
            Lazy<IBaseControllerService> baseControllerService,
            Lazy<IContentApplicationService> contentApplicationService,
            Lazy<IFinancePlanApplicationService> financePlanApplicationService,
            Lazy<IGuarantorApplicationService> guarantorApplicationService,
            Lazy<IPaymentActionApplicationService> paymentActionApplicationService,
            Lazy<IPaymentConfigurationService> paymentConfigurationService,
            Lazy<IPaymentDetailApplicationService> paymentDetailApplicationService,
            Lazy<IPaymentMethodsApplicationService> paymentMethodsApplicationService,
            Lazy<IPaymentMethodAccountTypeApplicationService> paymentMethodAccountTypeApplicationService,
            Lazy<IPaymentMethodProviderTypeApplicationService> paymentMethodProviderTypeApplicationService,
            Lazy<IPaymentOptionApplicationService> paymentOptionApplicationService,
            Lazy<IPaymentReversalApplicationService> paymentReversalApplicationService,
            Lazy<IPaymentSubmissionApplicationService> paymentSubmissionApplicationService,
            Lazy<IVisitApplicationService> visitApplicationService,
            Lazy<IVisitPayUserApplicationService> visitPayUserApplicationService,
            Lazy<GuarantorFilterService> guarantorFilterService)
            : base(
                baseControllerService,
                contentApplicationService,
                financePlanApplicationService,
                guarantorApplicationService,
                paymentActionApplicationService,
                paymentConfigurationService,
                paymentDetailApplicationService,
                paymentMethodsApplicationService,
                paymentMethodAccountTypeApplicationService,
                paymentMethodProviderTypeApplicationService,
                paymentOptionApplicationService,
                paymentReversalApplicationService,
                paymentSubmissionApplicationService,
                visitApplicationService,
                visitPayUserApplicationService)
        {
            this._guarantorFilterService = guarantorFilterService;
        }

        #region payments (pending/history)
        
        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult PaymentPending(int vpGuarantorId, int currentVisitPayUserId)
        {
            IList<PaymentPendingSearchResultViewModel> scheduledPayments = this.GetScheduledPayments(vpGuarantorId, currentVisitPayUserId);
            return this.Json(new PaymentPendingResultsModel(1, scheduledPayments.Count, scheduledPayments.Count)
            {
                Results = scheduledPayments
            });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<JsonResult> PaymentPendingExport(int vpGuarantorId)
        {
            return await this.GetPendingPaymentsExportAsync(vpGuarantorId);
        }
        
        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult PaymentHistory(PaymentHistorySearchFilterViewModel model, int vpGuarantorId, int currentVisitPayUserId, int page, int rows, string sidx, string sord)
        {
            PaymentHistorySearchResultsViewModel paymentHistorySearchResultsViewModel = this.GetPaymentHistory(vpGuarantorId, model, page, rows, sidx, sord, currentVisitPayUserId);
            return this.Json(paymentHistorySearchResultsViewModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<JsonResult> PaymentHistoryExport(int vpGuarantorId, int currentVisitPayUserId, PaymentHistorySearchFilterViewModel model, string sidx, string sord)
        {
            return await this.GetPaymentHistoryExportAsync(vpGuarantorId, model, sidx, sord, currentVisitPayUserId);
        }

        #endregion

        #region payment edit/reschedule/cancel

        [HttpGet]
        public PartialViewResult PaymentEdit()
        {
            return this.PartialView("_PaymentEdit");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Common.Web.Attributes.SessionState(SessionStateBehavior.Required)]
        public async Task<JsonResult> PaymentEdit(bool isRecurring, int vpGuarantorId, int? paymentId)
        {
            this.SessionFacade.Value.VpGuarantorId = vpGuarantorId; //todo adding for payment method as it expects this but really should be passed from javascript
            return this.Json(await this.GetPaymentEditModalAsync(isRecurring, paymentId, vpGuarantorId).ConfigureAwait(true));
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult PaymentEditValidate(PaymentRescheduleDto paymentRescheduleDto, int vpGuarantorId)
        {
            return this.ValidatePaymentEdit(paymentRescheduleDto, vpGuarantorId);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult PaymentEditSave(PaymentRescheduleDto paymentRescheduleDto, int vpGuarantorId)
        {
            if (this.UserHasReschedulePaymentPermissions(paymentRescheduleDto.PaymentType))
            {
                bool result = this.PaymentActionApplicationService.Value.ReschedulePayment(vpGuarantorId, this.CurrentUserId, paymentRescheduleDto);
                return this.Json(result);
            }

            return this.Json(false);
        }

        [HttpGet]
        public PartialViewResult PaymentCancel()
        {
            return this.PartialView("_PaymentCancel");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<JsonResult> PaymentCancel(bool isRecurringPayment, int vpGuarantorId, int? paymentId = null)
        {
            PaymentCancelViewModel model = await this.GetPaymentCancelModelAsync(isRecurringPayment, paymentId, vpGuarantorId).ConfigureAwait(true);

            return this.Json(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult PaymentCancelSubmit(PaymentCancelDto paymentCancelDto, int vpGuarantorId)
        {
            if (this.UserHasCancelPaymentPermissions(paymentCancelDto.PaymentType))
            {
                bool result = this.PaymentActionApplicationService.Value.CancelPayment(vpGuarantorId, this.CurrentUserId, paymentCancelDto);
                return this.Json(result);
            }

            return this.Json(false);
        }

        #endregion

        #region void/refund

        [HttpPost]
        [ValidateAntiForgeryToken]
        [VpAuthorize(Roles = VisitPayRoleStrings.Payment.PaymentVoid)]
        public async Task<JsonResult> PaymentVoid(int vpGuarantorId, int paymentProcessorResponseId)
        {
            VoidPaymentResponseDto response = await this.PaymentReversalApplicationService.Value.VoidPaymentAsync(vpGuarantorId, paymentProcessorResponseId, this.CurrentUserId);

            return this.Json(new
            {
                Success = !response.IsError,
                Message = response.IsError ? response.ErrorMessage : response.TransactionId
            });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [VpAuthorize(Roles = VisitPayRoleStrings.Payment.PaymentRefund + "," + VisitPayRoleStrings.Payment.PaymentRefundReturnedCheck)]
        public async Task<JsonResult> PaymentRefund(int vpGuarantorId, int paymentProcessorResponseId, decimal amount)
        {
            RefundPaymentResponseDto response = await this.PaymentReversalApplicationService.Value.RefundPaymentAsync(Mapper.Map<JournalEventHttpContextDto>(System.Web.HttpContext.Current), vpGuarantorId, paymentProcessorResponseId, this.CurrentUserId, amount);

            return this.Json(new
            {
                Success = !response.IsError,
                Message = response.IsError ? response.ErrorMessage : response.TransactionId
            });
        }

        #endregion

        #region payment details

        [HttpPost]
        [ValidateAntiForgeryToken]
        public PartialViewResult PaymentDetail(int paymentProcessorResponseId, int vpGuarantorId, int currentVisitPayUserId)
        {
            return this.GetPaymentDetail(paymentProcessorResponseId, vpGuarantorId, currentVisitPayUserId);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<JsonResult> PaymentDetailExport(int paymentProcessorResponseId, int vpGuarantorId, int currentVisitPayUserId)
        {
            return await this.GetPaymentDetailExportAsync(paymentProcessorResponseId, vpGuarantorId, currentVisitPayUserId);
        }
        
        #endregion

        #region payment methods

        private const string PaymentMethodRoles = VisitPayRoleStrings.Miscellaneous.IvhAdmin + "," + VisitPayRoleStrings.Csr.CSR + "," + VisitPayRoleStrings.Csr.UserAdmin;

        [HttpGet]
        [VpAuthorize(Roles = PaymentMethodRoles)]
        public ActionResult PaymentMethods(int currentVisitPayUserId)
        {
            PaymentMethodsListViewModel model = this.CreatePaymentMethodsListViewModel(currentVisitPayUserId, false);

            return this.View(model);
        }

        [HttpGet]
        [VpAuthorize(Roles = PaymentMethodRoles)]
        public PartialViewResult PaymentMethodsListPartial(int currentVisitPayUserId, bool allowSelection)
        {
            PaymentMethodsListViewModel model = this.CreatePaymentMethodsListViewModel(currentVisitPayUserId, allowSelection);
            
            return this.PartialView("_PaymentMethodsList", model);
        }

        private PaymentMethodsListViewModel CreatePaymentMethodsListViewModel(int currentVisitPayUserId, bool allowSelection)
        {
            bool isAchEnabled = this.IsFeatureEnabled(VisitPayFeatureEnum.PaymentsAch);
            bool isCardEnabled = this.IsFeatureEnabled(VisitPayFeatureEnum.PaymentsCreditCard);
            
            PaymentMethodSelectType selectType = allowSelection ? PaymentMethodSelectType.Select : PaymentMethodSelectType.SetPrimary;

            return new PaymentMethodsListViewModel(currentVisitPayUserId, selectType, isAchEnabled, isCardEnabled);
        }

        [HttpGet]
        [VpAuthorize(Roles = PaymentMethodRoles)]
        public PartialViewResult PaymentMethodBank()
        {
            return this.PartialView("_PaymentMethodBankAccount", new BankAccountViewModel());
        }

        [HttpGet]
        public PartialViewResult PaymentMethodAccountProvider(int paymentMethodId)
        {
            PaymentMethodDto paymentMethod = this.PaymentMethodsApplicationService.Value.GetPaymentMethodById(paymentMethodId);
            IList<PaymentMethodAccountTypeDto> paymentMethodAccountTypes = this.PaymentMethodAccountTypeApplicationService.Value.GetPaymentMethodAccountTypes();
            IList<PaymentMethodProviderTypeDto> paymentMethodProviderTypes = this.PaymentMethodProviderTypeApplicationService.Value.GetPaymentMethodProviderTypes();
            return this.PartialView("_PaymentMethodAccountProvider", new PaymentMethodAccountProviderViewModel
            {
                PaymentMethodAccountTypes = Mapper.Map<IList<PaymentMethodAccountTypeViewModel>>(paymentMethodAccountTypes),
                PaymentMethodProviderTypes = Mapper.Map<IList<PaymentMethodProviderTypeViewModel>>(paymentMethodProviderTypes),
                SelectedAccountTypeId = paymentMethod.PaymentMethodAccountType.PaymentMethodAccountTypeId,
                SelectedProviderTypeId = paymentMethod.PaymentMethodProviderType.PaymentMethodProviderTypeId
            });
        }

        [HttpGet]
        [VpAuthorize(Roles = PaymentMethodRoles)]
        public PartialViewResult PaymentMethodCard()
        {
            IList<PaymentMethodAccountTypeDto> paymentMethodAccountTypes = this.PaymentMethodAccountTypeApplicationService.Value.GetPaymentMethodAccountTypes();
            IList<PaymentMethodProviderTypeDto> paymentMethodProviderTypes = this.PaymentMethodProviderTypeApplicationService.Value.GetPaymentMethodProviderTypes();
            return this.PartialView("_PaymentMethodCardAccount", new CardAccountViewModel
            {
                PaymentMethodAccountTypes = Mapper.Map<IList<PaymentMethodAccountTypeViewModel>>(paymentMethodAccountTypes),
                PaymentMethodProviderTypes = Mapper.Map<IList<PaymentMethodProviderTypeViewModel>>(paymentMethodProviderTypes)
            });
        }
        
        [HttpPost]
        [VpAuthorize(Roles = PaymentMethodRoles)]
        [ValidateAntiForgeryToken]
        public async Task<JsonResult> GetPaymentMethodsList(int currentVisitPayUserId)
        {
            int currentVpGuarantorId = this.VisitPayUserApplicationService.Value.GetGuarantorIdFromVisitPayUserId(currentVisitPayUserId);

            IList<PaymentMethodDto> paymentMethodsDto = this.PaymentMethodsApplicationService.Value.GetPaymentMethods(currentVpGuarantorId, this.CurrentUserId);
            CmsVersionDto hsaInterestDisclaimerCms = await this.ContentApplicationService.Value.GetCurrentVersionAsync(CmsRegionEnum.HsaInterestDisclaimer).ConfigureAwait(true);
            IList<PaymentMethodAccountTypeDto> paymentMethodAccountTypes = this.PaymentMethodAccountTypeApplicationService.Value.GetPaymentMethodAccountTypes();
            IList<PaymentMethodProviderTypeDto> paymentMethodProviderTypes = this.PaymentMethodProviderTypeApplicationService.Value.GetPaymentMethodProviderTypes();

            PaymentMethodsViewModel model = new PaymentMethodsViewModel
            {
                AllAccounts = Mapper.Map<IList<PaymentMethodListItemViewModel>>(paymentMethodsDto.OrderBy(x => x.IsPrimary ? 0 : 1)),
                HsaInterestDisclaimerCms = Mapper.Map<CmsViewModel>(hsaInterestDisclaimerCms),
                PaymentMethodAccountTypes = Mapper.Map<IList<PaymentMethodAccountTypeViewModel>>(paymentMethodAccountTypes),
                PaymentMethodProviderTypes = Mapper.Map<IList<PaymentMethodProviderTypeViewModel>>(paymentMethodProviderTypes)
            };

            return this.Json(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult PaymentMethod(int paymentMethodId, int currentVisitPayUserId, bool isNewAch = false)
        {
            PaymentMethodDto paymentMethodDto = new PaymentMethodDto
            {
                IsActive = true
            };

            bool hasExistingPrimary = true;

            int currentVpGuarantorId = this.VisitPayUserApplicationService.Value.GetGuarantorIdFromVisitPayUserId(currentVisitPayUserId);
            if (paymentMethodId > 0)
            {
                paymentMethodDto = this.PaymentMethodsApplicationService.Value.GetPaymentMethodById(paymentMethodId, currentVpGuarantorId);
            }
            else
            {
                PaymentMethodDto primaryPaymentMethod = this.PaymentMethodsApplicationService.Value.GetPrimaryPaymentMethod(currentVpGuarantorId);
                hasExistingPrimary = primaryPaymentMethod != null;
            }

            if (paymentMethodDto.IsAchType || (paymentMethodId == 0 && isNewAch))
            {
                BankAccountViewModel bankAccountViewModel = Mapper.Map<BankAccountViewModel>(paymentMethodDto);
                bankAccountViewModel.HasExistingPrimary = hasExistingPrimary;
                bankAccountViewModel.IsPrimary = paymentMethodDto.IsPrimary || !hasExistingPrimary;

                return this.Json(bankAccountViewModel);
            }

            IList<PaymentMethodAccountTypeDto> paymentMethodAccountTypes = this.PaymentMethodAccountTypeApplicationService.Value.GetPaymentMethodAccountTypes();
            IList<PaymentMethodProviderTypeDto> paymentMethodProviderTypes = this.PaymentMethodProviderTypeApplicationService.Value.GetPaymentMethodProviderTypes();

            CardAccountViewModel cardAccountViewModel = Mapper.Map<CardAccountViewModel>(paymentMethodDto);
            cardAccountViewModel.HasExistingPrimary = hasExistingPrimary;
            cardAccountViewModel.IsPrimary = paymentMethodDto.IsPrimary || !hasExistingPrimary;
            cardAccountViewModel.PaymentMethodAccountTypes = Mapper.Map<IList<PaymentMethodAccountTypeViewModel>>(paymentMethodAccountTypes);
            cardAccountViewModel.PaymentMethodProviderTypes = Mapper.Map<IList<PaymentMethodProviderTypeViewModel>>(paymentMethodProviderTypes);

            if (paymentMethodDto.PaymentMethodAccountType != null)
            {
                cardAccountViewModel.AccountTypeId = paymentMethodDto.PaymentMethodAccountType.PaymentMethodAccountTypeId;
            }
            if (paymentMethodDto.PaymentMethodProviderType != null)
            {
                cardAccountViewModel.ProviderTypeId = paymentMethodDto.PaymentMethodProviderType.PaymentMethodProviderTypeId;
            }

            return this.Json(cardAccountViewModel);

        }
        
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<JsonResult> SavePaymentMethodBank(int currentVisitPayUserId, BankAccountViewModel model)
        {
            if (!this.ValidateModelState("BankAccountViewModel Failed Validation"))
            {
                return this.Json(new PaymentMethodResultViewModel(false, this.GenericErrorMessage));
            }

            int currentVpGuarantorId = this.VisitPayUserApplicationService.Value.GetGuarantorIdFromVisitPayUserId(currentVisitPayUserId);

            PaymentMethodResultDto resultDto = await this.SavePaymentMethodBank(model, currentVpGuarantorId, this.CurrentUserId).ConfigureAwait(true);

            return this.Json(Mapper.Map<PaymentMethodResultViewModel>(resultDto));
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult SavePaymentMethodCard(int currentVisitPayUserId, CardAccountViewModel model)
        {
            if (!this.ValidateModelState("CardAccountViewModel Failed Validation"))
            {
                return this.Json(new PaymentMethodResultViewModel(false, this.GenericErrorMessage));
            }

            int currentVpGuarantorId = this.VisitPayUserApplicationService.Value.GetGuarantorIdFromVisitPayUserId(currentVisitPayUserId);
            PaymentMethodResultDto resultDto = this.SavePaymentMethodCard(model, currentVpGuarantorId, this.CurrentUserId);
            this.MetricsProvider.Value.Increment(Metrics.Increment.SecurePan.PaymentMethod.Success);

            return this.Json(Mapper.Map<PaymentMethodResultViewModel>(resultDto));
        }
        
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<JsonResult> RemovePaymentMethod(int currentVisitPayUserId, int paymentMethodId)
        {
            int currentVpGuarantorId = this.VisitPayUserApplicationService.Value.GetGuarantorIdFromVisitPayUserId(currentVisitPayUserId);
            DeletePaymentMethodResponseDto response = await this.PaymentMethodsApplicationService.Value.DeactivatePaymentMethodAsync(paymentMethodId, currentVpGuarantorId, this.CurrentUserId);

            return this.Json(new PaymentMethodResultViewModel(!response.IsError, response.ErrorMessage));
        }
        
        [HttpPost]
        [ValidateAntiForgeryToken]
        [VpAuthorize(Roles = PaymentMethodRoles)]
        public JsonResult SetPrimaryPaymentMethod(int currentVisitPayUserId, int paymentMethodId)
        {
            int currentVpGuarantorId = this.VisitPayUserApplicationService.Value.GetGuarantorIdFromVisitPayUserId(currentVisitPayUserId);

            PaymentMethodResultDto resultDto = this.PaymentMethodsApplicationService.Value.SetPrimaryPaymentMethod(Mapper.Map<JournalEventHttpContextDto>(System.Web.HttpContext.Current), paymentMethodId, currentVpGuarantorId, this.CurrentUserId);

            return this.Json(Mapper.Map<PaymentMethodResultViewModel>(resultDto));
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult SetPaymentMethodAccountProviderType(int currentVisitPayUserId, int paymentMethodId, int paymentMethodAccountTypeId, int paymentMethodProviderTypeId)
        {
            int vpGuarantorId = this.VisitPayUserApplicationService.Value.GetGuarantorIdFromVisitPayUserId(currentVisitPayUserId);

            PaymentMethodResultDto resultDto = this.PaymentMethodsApplicationService.Value.SetPaymentMethodAccountProviderType(paymentMethodId, paymentMethodAccountTypeId, paymentMethodProviderTypeId, vpGuarantorId, currentVisitPayUserId);

            return this.Json(Mapper.Map<PaymentMethodResultViewModel>(resultDto));
        }

        #endregion

        #region arrange payment

        [HttpGet]
        [OverrideAuthorization]
        public PartialViewResult GuarantorContext(int visitPayUserId)
        {
            VisitPayUserDto currentVisitPayUser = this.VisitPayUserApplicationService.Value.FindById(visitPayUserId.ToString());
            GuarantorContextViewModel model = new GuarantorContextViewModel
            {
                VpGuarantorId = this.VisitPayUserApplicationService.Value.GetGuarantorIdFromVisitPayUserId(visitPayUserId),
                Email = currentVisitPayUser.Email,
                UserFirstName = currentVisitPayUser.FirstName,
                UserMiddleName = currentVisitPayUser.MiddleName,
                UserLastName = currentVisitPayUser.LastName
            };

            return this.PartialView("~/Views/Shared/_GuarantorContext.cshtml", model);
        }

        [HttpGet]
        [OverrideAuthorization]
        [VpAuthorize(Roles = VisitPayRoleStrings.Csr.ArrangePayment)]
        [Common.Web.Attributes.SessionState(SessionStateBehavior.Required)]
        public async Task<ActionResult> ArrangePayment(int vpGuarantorId, int? currentVisitPayUserId, PaymentOptionEnum? paymentOption)
        {
            // the managing or current guarantor, not the managed guarantor
            GuarantorDto currentGuarantor = this.GuarantorApplicationService.Value.GetGuarantor(vpGuarantorId);
            if (currentGuarantor == null || currentGuarantor.AccountClosedDate.HasValue)
            {
                return this.Json(new ResultMessage(false, "Invalid guarantor or closed account"));
            }

            VisitPayUserDto currentVisitPayUser = currentVisitPayUserId.HasValue ? this.VisitPayUserApplicationService.Value.FindById(currentVisitPayUserId.ToString()) : currentGuarantor.User;
            int filteredVpGuarantorId = this._guarantorFilterService.Value.VpGuarantorId(currentVisitPayUser.VisitPayUserId).Value;

            return this.View(await this.GetArrangePaymentModelAsync(paymentOption, filteredVpGuarantorId, currentVisitPayUser.VisitPayUserId));
        }

        [HttpPost]
        [VpAuthorize(Roles = VisitPayRoleStrings.Miscellaneous.IvhAdmin + "," + VisitPayRoleStrings.Csr.CSR + "," + VisitPayRoleStrings.Csr.UserAdmin)]
        [ValidateAntiForgeryToken]
        [Common.Web.Attributes.SessionState(SessionStateBehavior.Required)]
        public async Task<JsonResult> ArrangePaymentData(int currentVisitPayUserId)
        {
            int vpGuarantorId = this._guarantorFilterService.Value.VpGuarantorId(currentVisitPayUserId).Value;
            GuarantorDto guarantor = this.GuarantorApplicationService.Value.GetGuarantor(vpGuarantorId);
            if (guarantor == null || guarantor.AccountClosedDate.HasValue)
            {
                return this.Json(new ResultMessage(false, "Invalid guarantor or closed account"));
            }

            VisitPayUserDto currentVisitPayUser = this.VisitPayUserApplicationService.Value.FindById(currentVisitPayUserId.ToString());

            return this.Json(await this.GetArrangePaymentModelAsync(null, guarantor.VpGuarantorId, currentVisitPayUser.VisitPayUserId));
        }

        [HttpPost]
        [OverrideAuthorization]
        [ValidateAntiForgeryToken]
        public async Task<JsonResult> ArrangePaymentFinancePlanData(int currentVisitPayUserId, int? financePlanOfferSetTypeId = null, string financePlanOptionStateCode = null, bool otherFinancePlanStatesAreAvailable = false)
        {
            int vpGuarantorId = this._guarantorFilterService.Value.VpGuarantorId(currentVisitPayUserId).Value;
            UserPaymentOptionDto option = await this.PaymentOptionApplicationService.Value.GetFinancePlanPaymentOptionAsync(vpGuarantorId, currentVisitPayUserId, financePlanOfferSetTypeId, financePlanOptionStateCode, otherFinancePlanStatesAreAvailable);
            return this.Json(Mapper.Map<PaymentOptionViewModel>(option));
        }

        [HttpPost]
        [VpAuthorize(Roles = VisitPayRoleStrings.Miscellaneous.IvhAdmin + "," + VisitPayRoleStrings.Csr.CSR + "," + VisitPayRoleStrings.Csr.UserAdmin)]
        [ValidateAntiForgeryToken]
        public JsonResult ArrangePaymentSubmit(int currentVisitPayUserId, ArrangePaymentSubmitViewModel model)
        {
            int vpGuarantorId = this._guarantorFilterService.Value.VpGuarantorId(currentVisitPayUserId).Value;
            GuarantorDto guarantor = this.GuarantorApplicationService.Value.GetGuarantor(vpGuarantorId);
            if (guarantor == null || guarantor.AccountClosedDate.HasValue)
            {
                return this.Json(new ResultMessage(false, "Invalid guarantor or closed account"));
            }

            return this.Json(this.SubmitArrangePayment(model, currentVisitPayUserId, guarantor.VpGuarantorId, "~/Views/Payment/_ArrangePaymentReceipt.cshtml", this.CurrentUserId));
        }

        [HttpPost]
        [VpAuthorize(Roles = VisitPayRoleStrings.Miscellaneous.IvhAdmin + "," + VisitPayRoleStrings.Csr.CSR + "," + VisitPayRoleStrings.Csr.UserAdmin)]
        [ValidateAntiForgeryToken]
        public JsonResult ArrangePaymentValidate(int currentVisitPayUserId, ArrangePaymentSubmitViewModel model)
        {
            int vpGuarantorId = this._guarantorFilterService.Value.VpGuarantorId(currentVisitPayUserId).Value;
            GuarantorDto guarantor = this.GuarantorApplicationService.Value.GetGuarantor(vpGuarantorId);
            if (guarantor == null || guarantor.AccountClosedDate.HasValue)
            {
                return this.Json(new ResultMessage(false, "Invalid guarantor or closed account"));
            }

            ValidatePaymentResponse validatePaymentResponse = this.ValidateArrangePayment(model, currentVisitPayUserId, guarantor.VpGuarantorId);

            return validatePaymentResponse == null ? this.Json(true) : this.Json(validatePaymentResponse);
        }

        [Common.Web.Attributes.SessionState(SessionStateBehavior.Required)]
        protected override async Task<ArrangePaymentViewModel> GetArrangePaymentModelAsync(PaymentOptionEnum? selectedPaymentOption, int vpGuarantorId, int currentVisitPayUserId)
        {
            int currentUserVpGuarantorId = this.VisitPayUserApplicationService.Value.GetGuarantorIdFromVisitPayUserId(currentVisitPayUserId);
            ArrangePaymentViewModel model = await base.GetArrangePaymentModelAsync(selectedPaymentOption, vpGuarantorId, currentVisitPayUserId);
            ModelService.CreateArrangePaymentViewModelContext(this, this.User, model, currentUserVpGuarantorId, currentVisitPayUserId);
            model.IsOfflineGuarantor = this.GuarantorApplicationService.Value.GetGuarantor(vpGuarantorId).IsOfflineGuarantor;

            return model;
        }
        
        #endregion
        
        [HttpGet]
        public async Task<JsonResult> PreviousConsolidationModal()
        {
            CmsVersionDto cmsVersion = await this.ContentApplicationService.Value.GetCurrentVersionAsync(CmsRegionEnum.PreviousConsolidationMessage);
            return this.Json(new
            {
                ContentTitle = cmsVersion.ContentTitle,
                ContentBody = cmsVersion.ContentBody
            }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public void InvalidCreditCard(string errorMessage, bool isTimeout, int currentVisitPayUserId)
        {
            int vpGuarantorId = this.VisitPayUserApplicationService.Value.GetGuarantorIdFromVisitPayUserId(currentVisitPayUserId);
            this.LogInvalidCreditCard(currentVisitPayUserId, vpGuarantorId, errorMessage, isTimeout);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult DeviceError(string statusCode, string rawResponse, int currentVisitPayUserId)
        {
            int vpGuarantorId = this.VisitPayUserApplicationService.Value.GetGuarantorIdFromVisitPayUserId(currentVisitPayUserId);

            IDictionary<string, string> d = new Dictionary<string, string>();
            d.Add("StatusCode", statusCode ?? string.Empty);
            d.Add("VpGuarantorId", vpGuarantorId.ToString());
            d.Add("ActionVisitPayUserId", this.CurrentUserId.ToString());
            d.Add("RawResponse", rawResponse);

            this.Logger.Value.Warn(() => $"Device Error: {string.Join(", ", d.Select(x => string.Concat(x.Key, " = ", x.Value)))}");

            return this.Json(true);
        }
    }
}
﻿namespace Ivh.Web.Client.Controllers
{
    using System;
    using System.Web.Mvc;
    using System.Web.SessionState;
    using Common.Web.Interfaces;
    using Common.Web.Models.Consolidate;
    using Services;

    public class GuarantorFilterController : BaseController
    {
        private readonly Lazy<GuarantorFilterService> _guarantorFilterService;

        public GuarantorFilterController(
            Lazy<IBaseControllerService> baseControllerService,
            Lazy<GuarantorFilterService> guarantorFilterService)
            : base(baseControllerService)
        {
            this._guarantorFilterService = guarantorFilterService;
        }

        [HttpGet]
        public PartialViewResult GetFilter(int currentVisitPayUserId, string title = "Select Guarantor")
        {
            GuarantorFilterViewModel model = new GuarantorFilterViewModel
            {
                Title = title,
                Items = this._guarantorFilterService.Value.GetFilterGuarantors(currentVisitPayUserId)
            };

            return this.PartialView("~/Views/Shared/_GuarantorFilter.cshtml", model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Common.Web.Attributes.SessionState(SessionStateBehavior.Required)]
        public void SetGuarantor(int currentVisitPayUserId, int filteredVpGuarantorId)
        {
            this._guarantorFilterService.Value.SetGuarantor(currentVisitPayUserId, filteredVpGuarantorId, this.CurrentUserId);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Common.Web.Attributes.SessionState(SessionStateBehavior.Required)]
        public void ClearSelection(int currentVisitPayUserId)
        {
            this._guarantorFilterService.Value.ClearSelection(currentVisitPayUserId);
        }
    }
}
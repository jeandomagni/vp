﻿namespace Ivh.Web.Client.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using System.Web;
    using System.Web.Mvc;
    using Application.Core.Common.Interfaces;
    using Application.User.Common.Dtos;
    using AutoMapper;
    using Common.VisitPay.Strings;
    using Common.Web.Extensions;
    using Common.Web.Interfaces;
    using Microsoft.AspNet.Identity;
    using Microsoft.AspNet.Identity.Owin;
    using Microsoft.Owin.Security;
    using Models.Manage;

    [Authorize(Roles = VisitPayRoleStrings.System.Client + "," + VisitPayRoleStrings.Admin.Administrator)]
    public class ManageController : BaseController
    {
        private readonly Lazy<IVisitPayUserApplicationService> _visitPayUserApplicationService;

        public ManageController(
            Lazy<IBaseControllerService> baseControllerService, 
            Lazy<IVisitPayUserApplicationService> visitPayUserApplicationService)
            : base(baseControllerService)
        {
            this._visitPayUserApplicationService = visitPayUserApplicationService;
        }

        //
        // GET: /Manage/Index
        public async Task<ActionResult> Index(ManageMessageId? message)
        {
            this.ViewBag.StatusMessage =
                message == ManageMessageId.ChangePasswordSuccess
                    ? "Your password has been changed."
                    : message == ManageMessageId.SetPasswordSuccess
                        ? "Your password has been set."
                        : message == ManageMessageId.SetTwoFactorSuccess
                            ? "Your two-factor authentication provider has been set."
                            : message == ManageMessageId.Error
                                ? "An error has occurred."
                                : message == ManageMessageId.AddPhoneSuccess
                                    ? "Your phone number was added."
                                    : message == ManageMessageId.RemovePhoneSuccess
                                        ? "Your phone number was removed."
                                        : "";

            string userId = this.CurrentUserIdString;
            IndexViewModel model = new IndexViewModel
            {
                HasPassword = await this.HasPassword(),
                PhoneNumber = await this._visitPayUserApplicationService.Value.GetPhoneNumberAsync(userId),
                TwoFactor = await this._visitPayUserApplicationService.Value.GetTwoFactorEnabledAsync(userId),
                Logins = await this._visitPayUserApplicationService.Value.GetLoginsAsync(userId),
                BrowserRemembered = await this.AuthenticationManager.TwoFactorBrowserRememberedAsync(userId)
            };
            return View(model);
        }

        //
        // POST: /Manage/RemoveLogin
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> RemoveLogin(string loginProvider, string providerKey)
        {
            ManageMessageId? message;
            IdentityResult result =
                await
                   this._visitPayUserApplicationService.Value.RemoveLoginAsync(this.CurrentUserIdString,
                        new UserLoginInfo(loginProvider, providerKey)).ConfigureAwait(true);
            if (result.Succeeded)
            {
                await this._visitPayUserApplicationService.Value.ReLoginAsync(this.CurrentUserIdString, Mapper.Map<HttpContextDto>(System.Web.HttpContext.Current)).ConfigureAwait(true);
                message = ManageMessageId.RemoveLoginSuccess;
            }
            else
            {
                message = ManageMessageId.Error;
            }
            return this.RedirectToAction("ManageLogins", new { Message = message });
        }

        //
        // GET: /Manage/AddPhoneNumber
        public ActionResult AddPhoneNumber()
        {
            return View();
        }

        //
        // POST: /Manage/AddPhoneNumber
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> AddPhoneNumber(AddPhoneNumberViewModel model)
        {
            if (!this.ModelState.IsValid)
            {
                return View(model);
            }

            // Generate the token and send it
            string code =
                await
                    this._visitPayUserApplicationService.Value.GenerateChangePhoneNumberTokenAsync(this.CurrentUserIdString,
                        model.Number).ConfigureAwait(true);
            await this._visitPayUserApplicationService.Value.SendSmsMessageForCode(model.Number, code).ConfigureAwait(true);

            return this.RedirectToAction("VerifyPhoneNumber", new { PhoneNumber = model.Number });
        }

        //
        // GET: /Manage/VerifyPhoneNumber
        public async Task<ActionResult> VerifyPhoneNumber(string phoneNumber)
        {
            string code =
                await
                    this._visitPayUserApplicationService.Value.GenerateChangePhoneNumberTokenAsync(this.CurrentUserIdString,
                        phoneNumber).ConfigureAwait(true);
            // Send an SMS through the SMS provider to verify the phone number
            return phoneNumber == null
                ? this.View("Error")
                : this.View(new VerifyPhoneNumberViewModel { PhoneNumber = phoneNumber });
        }

        //
        // POST: /Manage/VerifyPhoneNumber
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> VerifyPhoneNumber(VerifyPhoneNumberViewModel model)
        {
            if (!this.ModelState.IsValid)
            {
                return View(model);
            }
            IdentityResult result =
                await
                    this._visitPayUserApplicationService.Value.ChangePhoneNumberAsync(this.CurrentUserIdString,
                        model.PhoneNumber, model.Code).ConfigureAwait(true);
            if (result.Succeeded)
            {
                await this._visitPayUserApplicationService.Value.ReLoginAsync(this.CurrentUserIdString, Mapper.Map<HttpContextDto>(System.Web.HttpContext.Current)).ConfigureAwait(true);
                return this.RedirectToAction("Index", new { Message = ManageMessageId.AddPhoneSuccess });
            }

            // If we got this far, something failed, redisplay form
            this.ModelState.AddModelError("", "Failed to verify phone");
            return View(model);
        }

        //
        // GET: /Manage/RemovePhoneNumber
        public async Task<ActionResult> RemovePhoneNumber()
        {
            IdentityResult result = await this._visitPayUserApplicationService.Value.SetPhoneNumberAsync(this.CurrentUserIdString, null).ConfigureAwait(true);
            if (!result.Succeeded)
            {
                return this.RedirectToAction("Index", new { Message = ManageMessageId.Error });
            }
            HttpContext context = System.Web.HttpContext.Current;
            context.SetSessionCookie();
            await this._visitPayUserApplicationService.Value.ReLoginAsync(this.CurrentUserIdString, Mapper.Map<HttpContextDto>(context)).ConfigureAwait(true);
            return this.RedirectToAction("Index", new { Message = ManageMessageId.RemovePhoneSuccess });
        }

        //
        // GET: /Manage/ChangePassword
        public ActionResult ChangePassword()
        {
            return View();
        }

        //
        // POST: /Manage/ChangePassword
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ChangePassword(ChangePasswordViewModel model)
        {
            if (!this.ModelState.IsValid)
            {
                return View(model);
            }
            IdentityResult result =
                await
                    this._visitPayUserApplicationService.Value.ChangePasswordAsync(this.CurrentUserIdString,
                        model.OldPassword, model.NewPassword, this.ClientDto.GuarantorPasswordExpLimitInDays, this.ClientDto.GuarantorUnrepeatablePasswordCount).ConfigureAwait(true);
            if (result.Succeeded)
            {
                await this._visitPayUserApplicationService.Value.ReLoginAsync(this.CurrentUserIdString, Mapper.Map<HttpContextDto>(System.Web.HttpContext.Current)).ConfigureAwait(true);
                return this.RedirectToAction("Index", new { Message = ManageMessageId.ChangePasswordSuccess });
            }
            this.AddErrors(result);
            return this.View(model);
        }

        //
        // GET: /Manage/SetPassword
        public ActionResult SetPassword()
        {
            return this.View();
        }

        //
        // POST: /Manage/SetPassword
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> SetPassword(SetPasswordViewModel model)
        {
            if (this.ModelState.IsValid)
            {
                IdentityResult result =
                    await
                        this._visitPayUserApplicationService.Value.AddPasswordAsync(this.CurrentUserIdString,
                            model.NewPassword).ConfigureAwait(true);
                if (result.Succeeded)
                {
                    await this._visitPayUserApplicationService.Value.ReLoginAsync(this.CurrentUserIdString, Mapper.Map<HttpContextDto>(System.Web.HttpContext.Current)).ConfigureAwait(true);
                    return this.RedirectToAction("Index", new { Message = ManageMessageId.SetPasswordSuccess });
                }
                this.AddErrors(result);
            }

            // If we got this far, something failed, redisplay form
            return View(model);
        }

        //
        // GET: /Manage/ManageLogins
        public async Task<ActionResult> ManageLogins(ManageMessageId? message)
        {
            this.ViewBag.StatusMessage =
                message == ManageMessageId.RemoveLoginSuccess
                    ? "The external login was removed."
                    : message == ManageMessageId.Error
                        ? "An error has occurred."
                        : "";
            VisitPayUserDto user = await this._visitPayUserApplicationService.Value.FindByIdAsync(this.CurrentUserIdString).ConfigureAwait(true);
            if (user == null)
            {
                return View("Error");
            }
            IList<UserLoginInfo> userLogins = await this._visitPayUserApplicationService.Value.GetLoginsAsync(this.CurrentUserIdString).ConfigureAwait(true);
            List<AuthenticationDescription> otherLogins =
                this.AuthenticationManager.GetExternalAuthenticationTypes()
                    .Where(auth => userLogins.All(ul => auth.AuthenticationType != ul.LoginProvider))
                    .ToList();
            this.ViewBag.ShowRemoveButton = user.PasswordHash != null || userLogins.Count > 1;
            return View(new ManageLoginsViewModel
            {
                CurrentLogins = userLogins,
                OtherLogins = otherLogins
            });
        }

        //
        // POST: /Manage/LinkLogin
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult LinkLogin(string provider)
        {
            // Request a redirect to the external login provider to link a login for the current user
            return new AccountController.ChallengeResult(provider, this.Url.Action("LinkLoginCallback", "Manage"),
                this.CurrentUserIdString);
        }

        //
        // GET: /Manage/LinkLoginCallback
        public async Task<ActionResult> LinkLoginCallback()
        {
            ExternalLoginInfo loginInfo =
                await this.AuthenticationManager.GetExternalLoginInfoAsync(XsrfKey, this.CurrentUserIdString).ConfigureAwait(true);
            if (loginInfo == null)
            {
                return this.RedirectToAction("ManageLogins", new { Message = ManageMessageId.Error });
            }
            IdentityResult result =
                await this._visitPayUserApplicationService.Value.AddLoginAsync(this.CurrentUserIdString, loginInfo.Login).ConfigureAwait(true);
            return result.Succeeded
                ? this.RedirectToAction("ManageLogins")
                : this.RedirectToAction("ManageLogins", new { Message = ManageMessageId.Error });
        }

        #region Helpers

        // Used for XSRF protection when adding external logins
        private const string XsrfKey = "XsrfId";

        private IAuthenticationManager AuthenticationManager
        {
            get { return this.HttpContext.GetOwinContext().Authentication; }
        }

        private void AddErrors(IdentityResult result)
        {
            foreach (string error in result.Errors)
            {
                this.ModelState.AddModelError("", error);
            }
        }

        private async Task<bool> HasPassword()
        {
            VisitPayUserDto user = await this._visitPayUserApplicationService.Value.FindByIdAsync(this.CurrentUserIdString);
            if (user != null)
            {
                return user.PasswordHash != null;
            }
            return false;
        }

        public enum ManageMessageId
        {
            AddPhoneSuccess,
            ChangePasswordSuccess,
            SetTwoFactorSuccess,
            SetPasswordSuccess,
            RemoveLoginSuccess,
            RemovePhoneSuccess,
            Error
        }

        #endregion
    }
}
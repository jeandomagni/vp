﻿namespace Ivh.Web.Client.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using System.Web;
    using System.Web.Mvc;
    using System.Web.SessionState;
    using Application.Content.Common.Interfaces;
    using Application.Core.Common.Dtos;
    using Application.Core.Common.Interfaces;
    using Application.FinanceManagement.Common.Dtos;
    using Application.FinanceManagement.Common.Interfaces;
    using Attributes;
    using AutoMapper;
    using Common.Base.Utilities.Extensions;
    using Common.EventJournal;
    using Common.VisitPay.Enums;
    using Common.VisitPay.Strings;
    using Common.Web.Controllers;
    using Common.Web.Interfaces;
    using Common.Web.Models;
    using Common.Web.Models.Account;
    using Common.Web.Models.FinancePlan;
    using Common.Web.Models.Shared;
    using Models.FinancePlan;
    using Provider.Pdf;
    using Services;

    [VpAuthorize(Roles = VisitPayRoleStrings.System.Client + "," + VisitPayRoleStrings.Admin.Administrator + ",")]
    [VpAuthorize(Roles = VisitPayRoleStrings.Csr.CSR + "," + VisitPayRoleStrings.Payment.PaymentRefund + "," + VisitPayRoleStrings.Payment.PaymentVoid + "," + VisitPayRoleStrings.Csr.ReconfigureFP + "," + VisitPayRoleStrings.Csr.SupportAdmin + "," + VisitPayRoleStrings.Csr.SupportView + "," + VisitPayRoleStrings.Csr.UserAdmin)]
    public class FinancePlanController : BaseFinancePlanController
    {
        private readonly Lazy<GuarantorFilterService> _guarantorFilterService;

        public FinancePlanController(
            Lazy<IBaseControllerService> baseControllerService,
            Lazy<IContentApplicationService> contentApplicationService,
            Lazy<IPdfConverter> pdfConverter,
            Lazy<IFinancePlanApplicationService> financePlanApplicationService,
            Lazy<IVisitApplicationService> visitApplicationService,
            Lazy<IFinancialDataSummaryApplicationService> financeDataSummaryApplicationService,
            Lazy<IGuarantorApplicationService> guarantorApplicationService,
            Lazy<IReconfigureFinancePlanApplicationService> reconfigureFinancePlanApplicationService,
            Lazy<IPaymentDetailApplicationService> paymentDetailApplicationService,
            Lazy<IPaymentMethodsApplicationService> paymentMethodsApplicationService,
            Lazy<IVisitPayUserApplicationService> visitPayUserApplicationService,
            Lazy<GuarantorFilterService> guarantorFilterService)
            : base(baseControllerService,
                contentApplicationService,
                financeDataSummaryApplicationService,
                financePlanApplicationService,
                guarantorApplicationService,
                paymentDetailApplicationService,
                paymentMethodsApplicationService,
                reconfigureFinancePlanApplicationService,
                visitApplicationService,
                visitPayUserApplicationService,
                pdfConverter)
        {
            this._guarantorFilterService = guarantorFilterService;
        }

        #region finance plan setup

        private const string FinancePlanReceiptView = "~/Views/Payment/_ArrangePaymentFpCustomized.cshtml";

        [HttpPost]
        [OverrideAuthorization]
        [ValidateAntiForgeryToken]
        public JsonResult VisitSelectionForFinancePlan(int vpGuarantorId, int currentVisitPayUserId)
        {
            VisitFilterDto visitFilter = new VisitFilterDto
            {
                VisitStateIds = ((int)VisitStateEnum.Active).ToListOfOne(),
                IsOnActiveFinancePlan = false,
                BillingHold = false
            };
            List<VisitDto> visitDto = this.VisitApplicationService.Value.GetVisits(currentVisitPayUserId, visitFilter, 1, 100, vpGuarantorId).Visits.ToList();
            IList<VisitsSearchResultViewModel> visits = Mapper.Map<IList<VisitsSearchResultViewModel>>(visitDto);

            return this.Json(new { visits });
        }

        [HttpPost]
        [OverrideAuthorization]
        [ValidateAntiForgeryToken]
        public JsonResult GetFinancePlanOfferBoundary(int vpGuarantorId, int statementId, int financePlanOfferSetTypeId, bool combineFinancePlans, IList<int> selectedVisits)
        {
            FinancePlanCalculationParametersDto financePlanCalculationParametersDto = FinancePlanCalculationParametersDto.CreateForOfferBoundary(vpGuarantorId, statementId, financePlanOfferSetTypeId, combineFinancePlans, selectedVisits?.ToList());
            FinancePlanBoundaryDto financePlanBoundaryDto = this.FinancePlanApplicationService.Value.GetMinimumPaymentAmount(financePlanCalculationParametersDto);
            IList<InterestRateDto> interestRatesDto = this.FinancePlanApplicationService.Value.GetInterestRates(financePlanCalculationParametersDto);
            IList<InterestRateViewModel> interestRates = Mapper.Map<IList<InterestRateViewModel>>(interestRatesDto);

            return this.Json(new { financePlanBoundaryDto, interestRates });
        }

        [HttpPost]
        [OverrideAuthorization]
        [ValidateAntiForgeryToken]
        public async Task<JsonResult> FinancePlanValidate(int currentVisitPayUserId, int statementId, decimal? monthlyPaymentAmount, int? numberMonthlyPayments, int? financePlanOfferSetTypeId, bool combineFinancePlans, int? paymentDueDay, IList<int> selectedVisits, string financePlanOptionStateCode)
        {
            int vpGuarantorId = this._guarantorFilterService.Value.VpGuarantorId(currentVisitPayUserId).Value;
            
            FinancePlanTermsResponseViewModel response = await this.ValidateFinancePlanAsync(
                vpGuarantorId, 
                statementId, 
                monthlyPaymentAmount, 
                numberMonthlyPayments, 
                financePlanOfferSetTypeId, 
                combineFinancePlans, 
                paymentDueDay, 
                selectedVisits?.ToList(), 
                financePlanOptionStateCode)
                .ConfigureAwait(false);
            return this.Json(response);
        }
        
        [HttpPost]
        [OverrideAuthorization]
        [ValidateAntiForgeryToken]
        public async Task<JsonResult> FinancePlanSubmit(int currentVisitPayUserId, ConfirmFinancePlanViewModel model)
        {
            int clientVisitPayUserId = this.CurrentUserId;
            HttpContext httpContext = System.Web.HttpContext.Current;
            int vpGuarantorId = this._guarantorFilterService.Value.VpGuarantorId(currentVisitPayUserId).Value;
            GuarantorDto guarantorDto = this.GuarantorApplicationService.Value.GetGuarantor(vpGuarantorId);

            if (model.PaymentDueDay.HasValue)
            {
                int managingVisitPayUserId = currentVisitPayUserId;
                if (guarantorDto.IsManaged)
                {
                    managingVisitPayUserId = guarantorDto.ManagingGuarantorId.GetValueOrDefault(currentVisitPayUserId);
                }

                // update payment due day before creating finance plan
                // should pass managing guarantor vp user id, even if setting up a managed fp
                this.GuarantorApplicationService.Value.ChangePaymentDueDay(managingVisitPayUserId, model.PaymentDueDay.Value, true, clientVisitPayUserId);
            }

            CreateFinancePlanResponse response;
            
            FinancePlanCalculationParametersDto financePlanCalculationParametersDto = FinancePlanCalculationParametersDto.CreateForSubmit(
                vpGuarantorId,
                model.StatementId,
                model.MonthlyPaymentAmount,
                model.FinancePlanOfferSetTypeId,
                model.OfferCalculationStrategy,
                model.CombineFinancePlans,
                model.FinancePlanOptionStateCode);

            financePlanCalculationParametersDto.SelectedVisits = model.SelectedVisits?.ToList();

            if (model.FinancePlanId.HasValue)
            {
                response = await this.FinancePlanApplicationService.Value.UpdatePendingFinancePlanAsync(
                    financePlanCalculationParametersDto,
                    model.FinancePlanId.Value,
                    model.FinancePlanOfferId,
                    clientVisitPayUserId, 
                    null, 
                    clientVisitPayUserId,
                    Mapper.Map<JournalEventHttpContextDto>(httpContext)).ConfigureAwait(true);
            }
            else
            {
                // todo: VP-5702 -should also check the FP type or something
                if (this.IsFeatureEnabled(VisitPayFeatureEnum.OfflineVisitPay) &&
                    guarantorDto.IsOfflineGuarantor &&
                    model.TermsCmsVersionId != default(int) && // todo: this should probably be nullable
                    model.EsignCmsVersionId.HasValue)
                {
                    financePlanCalculationParametersDto.FinancePlanTypeId = model.FinancePlanTypeId;

                    response = await this.FinancePlanApplicationService.Value.CreateFinancePlanAsync(
                        currentVisitPayUserId,
                        currentVisitPayUserId,
                        financePlanCalculationParametersDto,
                        model.TermsCmsVersionId,
                        model.FinancePlanOfferId,
                        clientVisitPayUserId,
                        Mapper.Map<JournalEventHttpContextDto>(httpContext));
                }
                else
                {
                    response = this.FinancePlanApplicationService.Value.CreateFinancePlanPendingAcceptance(financePlanCalculationParametersDto, clientVisitPayUserId, Mapper.Map<JournalEventHttpContextDto>(httpContext));
                }
            }

            if (response.IsError)
            {
                return this.Json(new FinancePlanResultsModel(false, response.ErrorMessage));
            }

            // offline guarantor - just return
            if (this.IsFeatureEnabled(VisitPayFeatureEnum.OfflineVisitPay) && guarantorDto.IsOfflineGuarantor)
            {
                if (model.EsignCmsVersionId.HasValue)
                {
                    this.VisitPayUserApplicationService.Value.AcknowledgeEsign(currentVisitPayUserId, model.EsignCmsVersionId.Value);
                }

                return this.Json(new FinancePlanResultsModel(true, string.Empty));
            }

            // normal guarantor
            this.ViewData.Model = new FinancePlanCustomizedReceipt
            {
                VpGuarantorId = vpGuarantorId,
                UserDateSummary = Mapper.Map<UserDateSummaryViewModel>(this.FinancialDataSummaryApplicationService.Value.GetFinancialDataSummaryByGuarantor(vpGuarantorId)),
                IsPatientOffer = (response.FinancePlan?.IsFinancePlanOfferTypePatient).GetValueOrDefault(true),
                FinancePlanOfferId = (response.FinancePlan?.FinancePlanOffer.FinancePlanOfferId).GetValueOrDefault(0)
            }; 
            
            string partialViewName = FinancePlanReceiptView;
            string confirmHtml = this.RenderPartialViewToString(partialViewName);

            int? financePlanId = response.FinancePlan?.FinancePlanId;
            int? paymentId = (response.Payments != null && response.Payments.Any(x => !x.IsError && x.Payment != null)) ? (int?) response.Payments.First(x => !x.IsError && x.Payment != null).Payment.PaymentId : null;
            
            FinancePlanResultsModel financePlanResults = new FinancePlanResultsModel(true, confirmHtml)
            {
                FinancePlanId = financePlanId,
                PaymentId = paymentId,
                VpGuarantorId = vpGuarantorId
            };

            return this.Json(financePlanResults);
        }
        
        #endregion

        #region reconfigure finance plans

        private const string RolesReconfigure = VisitPayRoleStrings.Csr.CSR + "," + VisitPayRoleStrings.Csr.UserAdmin + "," + VisitPayRoleStrings.Csr.ReconfigureFP;

        [HttpGet]
        [OverrideAuthorization]
        public ActionResult FinancePlanLog(int visitPayUserId, int financePlanId)
        {
            FinancePlanDto financePlanDto = this.FinancePlanApplicationService.Value.GetFinancePlan(visitPayUserId, financePlanId);
            List<string> sourceSystemKeys = financePlanDto.FinancePlanVisits.Select(x => x.SourceSystemKey).Distinct().ToList();

            FinancePlanLogFilterViewModel financePlanLogFilter = new FinancePlanLogFilterViewModel
            {
                VisitPayUserId = visitPayUserId,
                FinancePlanId = financePlanId,
                SourceSystemKeys = sourceSystemKeys
            };
            return this.PartialView("/Views/Search/_GuarantorFinancePlanLog.cshtml",financePlanLogFilter);
        }

       
        [HttpPost]
        [OverrideAuthorization]
        [ValidateAntiForgeryToken]
        public JsonResult FinancePlanLog(GridSearchModel<FinancePlanLogFilterViewModel> filterModel)
        {
            FinancePlanLogFilterDto financePlanLogFilterDto = Mapper.Map<FinancePlanLogFilterDto>(filterModel);
            FinancePlanLogResultsDto financePlanLogDto = this.FinancePlanApplicationService.Value.GetFinancePlanLog(financePlanLogFilterDto);
            List<FinancePlanLogViewModel> financePlanLogViewModels = Mapper.Map<List<FinancePlanLogViewModel>>(financePlanLogDto.FinancePlanLogs);

            GridResultsModel<FinancePlanLogViewModel> model = new GridResultsModel<FinancePlanLogViewModel>(filterModel.Page, filterModel.Rows, financePlanLogDto.TotalRecords)
            {
                Results = financePlanLogViewModels
            };

            return this.Json(model);

        }


        [HttpGet]
        [OverrideAuthorization]
        [VpAuthorize(Roles = RolesReconfigure)]
        public async Task<ActionResult> ReconfigureTerms(int id, int vpGuarantorId)
        {
            ReconfigureFinancePlanViewModel model = await this.GetReconfigureTermsModelAsync(id, vpGuarantorId, null);
            model.ReconfigureCancelUrl = this.Url.Action("ReconfigureCancel", "FinancePlan");
            model.ReconfigureModelUrl = this.Url.Action("ReconfigureTermsModel", "FinancePlan");
            model.ReconfigureSubmitUrl = this.Url.Action("ReconfigureSubmit", "FinancePlan");
            model.ReconfigureValidateUrl = this.Url.Action("ReconfigureValidate", "FinancePlan");
            model.IsEditable = true;

            return this.View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [OverrideAuthorization]
        [VpAuthorize(Roles = RolesReconfigure)]
        public async Task<JsonResult> ReconfigureTermsModel(int financePlanIdToReconfigure, int vpGuarantorId, int financePlanOfferSetTypeId)
        {
            ReconfigureFinancePlanViewModel model = await this.GetReconfigureTermsModelAsync(financePlanIdToReconfigure, vpGuarantorId, financePlanOfferSetTypeId);
            if (model == null)
            {
                return this.Json(null);
            }

            return this.Json(model);
        }
        
        [HttpPost]
        [OverrideAuthorization]
        [VpAuthorize(Roles = RolesReconfigure)]
        [ValidateAntiForgeryToken]
        public JsonResult ReconfigureCancel(int financePlanIdToReconfigure, int vpGuarantorId)
        {
            JournalEventHttpContextDto journalContext = Mapper.Map<JournalEventHttpContextDto>(System.Web.HttpContext.Current);
            ReconfigurationActionResponseDto responseDto = this.ReconfigureFinancePlanApplicationService.Value.CancelReconfiguration(financePlanIdToReconfigure, vpGuarantorId, this.CurrentUserId, "Client has canceled reconfiguration", journalContext);
            ReconfigurationActionResponseViewModel model = new ReconfigurationActionResponseViewModel(responseDto, $"{this.Url.Action("GuarantorAccount", "Search", new {id = vpGuarantorId})}#tabFinancePlans");

            return this.Json(model);
        }
        
        [HttpPost]
        [OverrideAuthorization]
        [VpAuthorize(Roles = RolesReconfigure)]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ReconfigureSubmit(int financePlanIdToReconfigure, int vpGuarantorId, decimal monthlyPaymentAmount, int? financePlanOfferSetTypeId)
        {
            ReconfigurationActionResponseDto responseDto = await this.SubmitReconfiguredFinancePlanAsync(vpGuarantorId, financePlanIdToReconfigure, monthlyPaymentAmount, financePlanOfferSetTypeId);
            ReconfigurationActionResponseViewModel model = new ReconfigurationActionResponseViewModel(responseDto, $"{this.Url.Action("GuarantorAccount", "Search", new {id = vpGuarantorId})}#tabFinancePlans");

            return this.Json(model);
        }

        [HttpPost]
        [OverrideAuthorization]
        [VpAuthorize(Roles = RolesReconfigure)]
        [ValidateAntiForgeryToken]
        public async Task<JsonResult> ReconfigureValidate(int vpGuarantorId, int financePlanIdToReconfigure, decimal? monthlyPaymentAmount, int? numberMonthlyPayments, int? financePlanOfferSetTypeId)
        {
            FinancePlanTermsResponseViewModel model = await this.ValidateReconfiguredFinancePlanAsync(vpGuarantorId, financePlanIdToReconfigure, monthlyPaymentAmount, numberMonthlyPayments, financePlanOfferSetTypeId);
            
            return this.Json(model);
        }

        #endregion

        #region cancel finance plan

        [HttpGet]
        [OverrideAuthorization]
        public PartialViewResult FinancePlanCancel()
        {
            return this.PartialView("_FinancePlanCancel", new FinancePlanCancelViewModel());
        }

        [HttpPost]
        [OverrideAuthorization]
        public JsonResult FinancePlanCancel(int guarantorVisitPayUserId, int financePlanId)
        {
            FinancePlanDto financePlanDto = this.FinancePlanApplicationService.Value.GetFinancePlan(guarantorVisitPayUserId, financePlanId);
            FinancePlanCancelViewModel model = new FinancePlanCancelViewModel
            {
                FinancePlan = Mapper.Map<FinancePlanDetailsViewModel>(financePlanDto),
                PendingVisitStateFullDisplayName = string.Empty
            };

            return this.Json(model);
        }

        [HttpPost]
        [OverrideAuthorization]
        [ValidateAntiForgeryToken]
        public JsonResult CancelFinancePlan(int guarantorVisitPayUserId, int financePlanId)
        {
            this.FinancePlanApplicationService.Value.CancelFinancePlan(guarantorVisitPayUserId, financePlanId, "Client Canceled Finance Plan", this.CurrentUserId);

            return this.Json(true);
        }

        #endregion
        
        #region finance plan interest rate history

        [HttpGet]
        [OverrideAuthorization]
        public ActionResult FinancePlanInterestRateHistory()
        {
            return this.PartialView("/Views/Search/_GuarantorFinancePlanInterestRateHistory.cshtml");
        }

        [HttpPost]
        [OverrideAuthorization]
        [ValidateAntiForgeryToken]
        public JsonResult FinancePlanInterestRateHistory(int guarantorVisitPayUserId, int financePlanId, int page, int rows, string sidx, string sord)
        {
            return this.FinancePlanInterestRateHistoryData(financePlanId, guarantorVisitPayUserId, page, rows, sidx, sord);
        }

        #endregion

        #region finance plan bucket history

        [HttpGet]
        [OverrideAuthorization]
        [VpAuthorize(Roles = VisitPayRoleStrings.Csr.CSR + "," + VisitPayRoleStrings.Csr.UserAdmin + "," + VisitPayRoleStrings.Csr.ReconfigureFP + "," + VisitPayRoleStrings.Csr.SupportAdmin + "," + VisitPayRoleStrings.Csr.SupportView)]
        public PartialViewResult FinancePlanBucketHistory()
        {
            return this.PartialView("_FinancePlanBucketHistory");
        }

        [HttpPost]
        [OverrideAuthorization]
        [VpAuthorize(Roles = VisitPayRoleStrings.Csr.CSR + "," + VisitPayRoleStrings.Csr.UserAdmin + "," + VisitPayRoleStrings.Csr.ReconfigureFP + "," + VisitPayRoleStrings.Csr.SupportAdmin + "," + VisitPayRoleStrings.Csr.SupportView)]
        [ValidateAntiForgeryToken]
        public JsonResult FinancePlanBuckets(GridSearchModel<FinancePlanBucketHistoryFilterViewModel> filterModel)
        {
            FinancePlanBucketHistoryResultsDto resultsDto = this.FinancePlanApplicationService.Value.GetBucketHistory(Mapper.Map<FinancePlanBucketHistoryFilterDto>(filterModel));
            
            GridResultsModel<FinancePlanBucketHistorySearchResultViewModel> model = new GridResultsModel<FinancePlanBucketHistorySearchResultViewModel>(filterModel.Page, filterModel.Rows, resultsDto.TotalRecords)
            {
                Results = Mapper.Map<List<FinancePlanBucketHistorySearchResultViewModel>>(resultsDto.FinancePlanBucketHistories)
            };

            return this.Json(model);
        }
        
        #endregion

        #region interest reallocate

        [HttpGet]
        [OverrideAuthorization]
        public PartialViewResult FinancePlanInterestReallocate()
        {
            return this.PartialView("_FinancePlanInterestReallocate");
        }

        [HttpPost]
        [OverrideAuthorization]
        [ValidateAntiForgeryToken]
        public JsonResult FinancePlanInterestReallocate(int vpGuarantorId, int financePlanId)
        {
            ReallocateInterestToPrincipalModel model = new ReallocateInterestToPrincipalModel();
            FinancePlanDto fp = this.FinancePlanApplicationService.Value.GetFinancePlan(this.CurrentUserId, financePlanId, vpGuarantorId);
            //should probably just map this
            model.FinancePlanId = financePlanId;
            model.MaxReallocationAmount = fp.InterestAssessed ?? 0m;
            model.AmountToReallocate = fp.InterestAssessed ?? 0m;
            model.InterestPaid = Math.Abs(fp.InterestPaidToDate);

            return this.Json(model);
        }

        [HttpPost]
        [OverrideAuthorization]
        [ValidateAntiForgeryToken]
        [VpAuthorize(Roles = VisitPayRoleStrings.Payment.PaymentRefund  + "," + VisitPayRoleStrings.Payment.PaymentVoid)]
        public JsonResult ReallocateInterestToPrincipalFinancePlan(int vpGuarantorId, int financePlanId, decimal amount)
        {
            ReallocateInterestToPrincipalDto response = this.FinancePlanApplicationService.Value.ReallocateInterestToPrincipal(this.CurrentUserId, vpGuarantorId, financePlanId, amount);

            return this.Json(new
            {
                Success = response.IsSuccessful,
                Message = !response.IsSuccessful ? response.ErrorMessage : response.PaymentId.ToString()
            });
        }

        #endregion
    }
}
﻿namespace Ivh.Web.Client.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using System.Web.Mvc;
    using Application.Base.Common.Dtos;
    using Application.Core.Common.Dtos;
    using Application.Core.Common.Interfaces;
    using Application.FinanceManagement.Common.Dtos;
    using Application.FinanceManagement.Common.Interfaces;
    using Application.User.Common.Dtos;
    using Attributes;
    using AutoMapper;
    using Common.Base.Enums;
    using Common.EventJournal;
    using Common.VisitPay.Enums;
    using Common.VisitPay.Strings;
    using Common.Web.Filters;
    using Common.Web.Interfaces;
    using Common.Web.Models;
    using Domain.Settings.Interfaces;
    using Microsoft.AspNet.Identity;
    using Microsoft.Reporting.WebForms;
    using Models.Reports;
    using Microsoft.IdentityModel.Clients.ActiveDirectory;
    using Microsoft.Rest;
    using Microsoft.PowerBI.Api.V2;
    using Microsoft.PowerBI.Api.V2.Models;
    using AnalyticReport = Microsoft.PowerBI.Api.V2.Models.Report;
    using System.Web.SessionState;

    public class ReportingController : BaseController
    {
        private readonly Lazy<IApplicationSettingsService> _applicationSettingsService;
        private readonly Lazy<IRedactionApplicationService> _redactionApplicationService;
        private readonly Lazy<IPaymentDetailApplicationService> _paymentDetailApplicationService;
        private readonly Lazy<IVisitPayUserJournalEventApplicationService> _visitPayUserJournalEventApplicationService;
        private readonly Lazy<IAnalyticReportApplicationService> _analyticReportApplicationService;

        #region Analytic report properties
        private static TokenCredentials _tokenCredentials;
        private static EmbedToken _reportTokenResponse;
        #endregion


        public ReportingController(
            Lazy<IApplicationSettingsService> applicationSettingsService,
            Lazy<IBaseControllerService> baseControllerService,
            Lazy<IRedactionApplicationService> redactionApplicationService,
            Lazy<IPaymentDetailApplicationService> paymentDetailApplicationService,
            Lazy<IVisitPayUserJournalEventApplicationService> visitPayUserJournalEventApplicationService,
            Lazy<IAnalyticReportApplicationService> analyticReportApplicationService)
            : base(baseControllerService)
        {
            this._applicationSettingsService = applicationSettingsService;
            this._redactionApplicationService = redactionApplicationService;
            this._paymentDetailApplicationService = paymentDetailApplicationService;
            this._visitPayUserJournalEventApplicationService = visitPayUserJournalEventApplicationService;
            this._analyticReportApplicationService = analyticReportApplicationService;
        }

        const string GuarantorSummaryReportExportKey = "GuarantorSummaryReportExport";
        const string VisitUnmatchReportExportKey = "VisitUnmatchReportExport";

        [HttpGet]
        [VpAuthorize(Roles = VisitPayRoleStrings.Miscellaneous.Reports)]
        public ActionResult PaymentReports()
        {
            return this.View();
        }

        #region guarantor summary report

        [HttpGet]
        [VpAuthorize(Roles = VisitPayRoleStrings.Miscellaneous.Unmatch)]
        public ActionResult GuarantorSummaryReport()
        {
            return this.View(new GuarantorSummaryFilterViewModel());
        }

        [HttpPost]
        [VpAuthorize(Roles = VisitPayRoleStrings.Miscellaneous.Unmatch)]
        public JsonResult GuarantorSummaryReport(GuarantorSummaryFilterViewModel model, int page, int rows, string sidx, string sord)
        {
            HsGuarantorMatchDiscrepancyFilterDto filter = Mapper.Map<HsGuarantorMatchDiscrepancyFilterDto>(model);
            filter.SortField = sidx;
            filter.SortOrder = sord;

            HsGuarantorMatchDiscrepancyResultsDto results = this._redactionApplicationService.Value.GetAllHsGuarantorMatchDiscrepancy(filter, page, rows);

            return this.Json(new
            {
                Data = Mapper.Map<IList<GuarantorSummaryViewModel>>(results.HsGuarantorMatchDiscrepancies),
                page,
                total = Math.Ceiling((decimal) results.TotalRecords/rows),
                records = results.TotalRecords
            });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [VpAuthorize(Roles = VisitPayRoleStrings.Miscellaneous.Unmatch)]
        [Common.Web.Attributes.SessionState(SessionStateBehavior.Required)]
        public async Task<ActionResult> GuarantorSummaryReportExport(GuarantorSummaryFilterViewModel model, string sidx, string sord)
        {
            HsGuarantorMatchDiscrepancyFilterDto filter = Mapper.Map<HsGuarantorMatchDiscrepancyFilterDto>(model);
            filter.SortField = sidx;
            filter.SortOrder = sord;

            //
            byte[] bytes = await this._redactionApplicationService.Value.ExportGuarantorSummaryReportAsync(filter, this.User.Identity.GetUserName());
            this.SetTempData(GuarantorSummaryReportExportKey, bytes);

            //
            return this.Json(new ResultMessage(true, this.Url.Action("GuarantorSummaryReportExportDownload")));
        }

        [HttpGet]
        [VpAuthorize(Roles = VisitPayRoleStrings.Miscellaneous.Unmatch)]
        public ActionResult GuarantorSummaryReportExportDownload()
        {
            object tempData = this.TempData[GuarantorSummaryReportExportKey];
            if (tempData != null)
            {
                this.WriteExcelInline((byte[])tempData, "GuarantorSummaryReport.xlsx");
            }

            return this.Json(new { }, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region visit unmatch report

        [HttpGet]
        [VpAuthorize(Roles = VisitPayRoleStrings.Miscellaneous.Unmatch)]
        public ActionResult VisitUnmatchReport()
        {
            return this.View(new VisitUnmatchFilterViewModel());
        }

        [HttpPost]
        [VpAuthorize(Roles = VisitPayRoleStrings.Miscellaneous.Unmatch)]
        public ActionResult VisitUnmatchReport(VisitUnmatchFilterViewModel model, int page, int rows, string sidx, string sord)
        {
            VisitUnmatchFilterDto filter = Mapper.Map<VisitUnmatchFilterDto>(model);
            filter.SortField = sidx;
            filter.SortOrder = sord;

            VisitUnmatchResultsDto results = this._redactionApplicationService.Value.GetAllVisitUnmatches(filter, page, rows);

            return this.Json(new
            {
                Data = Mapper.Map<IList<VisitUnmatchViewModel>>(results.VisitUnmatches),
                page,
                total = Math.Ceiling((decimal) results.TotalRecords / rows),
                records = results.TotalRecords
            });
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        [VpAuthorize(Roles = VisitPayRoleStrings.Miscellaneous.Reports + "," + VisitPayRoleStrings.Miscellaneous.Unmatch)]
        [Common.Web.Attributes.SessionState(SessionStateBehavior.Required)]
        public async Task<ActionResult> VisitUnmatchReportExport(VisitUnmatchFilterViewModel model, string sidx, string sord)
        {
            VisitUnmatchFilterDto filter = Mapper.Map<VisitUnmatchFilterDto>(model);
            filter.SortField = sidx;
            filter.SortOrder = sord;

            //
            byte[] bytes = await this._redactionApplicationService.Value.ExportVisitUnmatchReportAsync(filter, this.User.Identity.GetUserName());
            this.SetTempData(VisitUnmatchReportExportKey, bytes);

            //
            return this.Json(new ResultMessage(true, this.Url.Action("VisitUnmatchReportExportDownload")));
        }

        [HttpGet]
        [VpAuthorize(Roles = VisitPayRoleStrings.Miscellaneous.Reports + "," + VisitPayRoleStrings.Miscellaneous.Unmatch)]
        public ActionResult VisitUnmatchReportExportDownload()
        {
            object tempData = this.TempData[VisitUnmatchReportExportKey];
            if (tempData != null)
            {
                this.WriteExcelInline((byte[])tempData, "VisitUnmatchReport.xlsx");
            }

            return this.Json(new { }, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region ssrs report

        private const string ReportOfReport = "ReportOfReports";

        [HttpGet]
        [VpAuthorize(Roles = VisitPayRoleStrings.Miscellaneous.IvhAdmin + "," + VisitPayRoleStrings.Restricted.VisitPayReports)]
        [RequireFeature(VisitPayFeatureEnum.VisitPayReportsIsEnabled)]
        public ActionResult SsrsReport(string reportName = ReportOfReport)
        {

            string reportPath = this._applicationSettingsService.Value.SsrsReportPath.Value;
            Uri reportServerUri = this._applicationSettingsService.Value.SsrsReportServerUri.Value;

            ReportViewer reportViewer = new ReportViewer
            {
                ProcessingMode = ProcessingMode.Remote,
                SizeToReportContent = true,
                AsyncRendering = true,
                ShowBackButton = false,
                ShowParameterPrompts = true,
                KeepSessionAlive = false
            };
            
            string reportUserName = this._applicationSettingsService.Value.SsrsReportServerUserName.Value;
            string reportPassword = this._applicationSettingsService.Value.SsrsReportServerPassword.Value;
            string reportDomain = this._applicationSettingsService.Value.SsrsReportServerDomain.Value;
            if (!string.IsNullOrWhiteSpace(reportUserName))
            {
                ReportServerCredentials reportServerCredentials = new ReportServerCredentials(reportUserName, reportPassword, reportDomain);
                reportViewer.ServerReport.ReportServerCredentials = reportServerCredentials; 
            }

            reportViewer.ServerReport.ReportServerUrl = reportServerUri;
            reportViewer.ServerReport.ReportPath = $"/{reportPath}/{reportName}";

            this.ViewBag.ReportViewer = reportViewer;

            this._visitPayUserJournalEventApplicationService.Value.LogVisitPayReportRun(this.CurrentUserId, Mapper.Map<JournalEventHttpContextDto>(System.Web.HttpContext.Current), reportName);

            return this.View();
        }

        #endregion

        #region Analytic Reports

        [HttpGet]
        [VpAuthorize(Roles = VisitPayRoleStrings.Restricted.AnalyticReports)]
        public ActionResult AnalyticReport(string reportId, string reportName, string groupId, string vpReportEmbedUrl)
        {
            if (string.IsNullOrEmpty(reportId) || string.IsNullOrEmpty(reportName) || string.IsNullOrEmpty(vpReportEmbedUrl))
            {
                return this.View(new AnalyticReportConfig()
                {
                    ErrorMessage = "Analytic report parameters are empty"
                });
            }
            _tokenCredentials = this._analyticReportApplicationService.Value.ValidateToken(_reportTokenResponse, _tokenCredentials);
            string apiUrl = this.ApplicationSettingsService.Value.PowerBiApiUrl.Value;
            using (PowerBIClient client = new PowerBIClient(new Uri(apiUrl), _tokenCredentials))
            {
                GenerateTokenRequest generateTokenRequestParameters = new GenerateTokenRequest(accessLevel: "view");
                _reportTokenResponse = client.Reports.GenerateTokenInGroup(groupId,
                    reportId, generateTokenRequestParameters);
                if (_reportTokenResponse == null)
                {
                    return this.View(new AnalyticReportConfig
                    {
                        ErrorMessage = "Failed to generate request token."
                    });
                }
            }
            AnalyticReportConfig embedConfig = new AnalyticReportConfig
            {
                EmbedToken = _reportTokenResponse,
                EmbedUrl = vpReportEmbedUrl,
                Id = reportId,
                Name = reportName,
                Token = _reportTokenResponse.Token
            };

            return this.View(embedConfig);
        }

        #endregion
    }
}
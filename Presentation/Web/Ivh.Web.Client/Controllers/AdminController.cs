﻿namespace Ivh.Web.Client.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using System.Web.Mvc;
    using Application.Core.Common.Dtos;
    using System.Web.Script.Serialization;
    using Application.Content.Common.Interfaces;
    using Application.Core.Common.Interfaces;
    using Application.FinanceManagement.Common.Interfaces;
    using AutoMapper;
    using Attributes;
    using Common.EventJournal;
    using Common.VisitPay.Enums;
    using Common.VisitPay.Strings;
    using Common.Web.Interfaces;
    using Common.Web.Models;
    using Microsoft.Ajax.Utilities;
    using Microsoft.AspNet.Identity;
    using Models.Admin;
    using Ivh.Application.User.Common.Dtos;
    using Ivh.Application.EventJournal.Common.Dtos;
    using Ivh.Application.EventJournal.Common.Interfaces;
    using System.Web.SessionState;

    public class AdminController : BaseController
    {
        private readonly Lazy<IVisitPayUserApplicationService> _visitPayUserApplicationService;
        private readonly Lazy<IVisitPayUserJournalEventApplicationService> _visitPayUserJournalEventApplicationService;
        private readonly Lazy<IAchApplicationService> _achApplicationService;
        private readonly Lazy<IAuditApplicationService> _auditApplicationService;
        private readonly Lazy<IContentApplicationService> _contentApplicationService;
        private readonly Lazy<IFeatureApplicationService> _featureApplicationServiceLazy;
        private readonly Lazy<IJournalEventQueryApplicationService> _journalEventQueryApplicationService;

        public AdminController(
            Lazy<IBaseControllerService> baseControllerService,
            Lazy<IAchApplicationService> achApplicationService,
            Lazy<IAuditApplicationService> auditApplicationService,
            Lazy<IContentApplicationService> contentApplicationService,
            Lazy<IFeatureApplicationService> featureApplicationServiceLazy,
            Lazy<IVisitPayUserApplicationService> visitPayUserApplicationService,
            Lazy<IVisitPayUserJournalEventApplicationService> visitPayUserJournalEventApplicationService,
            Lazy<IJournalEventQueryApplicationService> journalEventQueryApplicationService)
            : base(baseControllerService)
        {
            this._achApplicationService = achApplicationService;
            this._auditApplicationService = auditApplicationService;
            this._contentApplicationService = contentApplicationService;
            this._featureApplicationServiceLazy = featureApplicationServiceLazy;
            this._visitPayUserApplicationService = visitPayUserApplicationService;
            this._visitPayUserJournalEventApplicationService = visitPayUserJournalEventApplicationService;
            this._journalEventQueryApplicationService = journalEventQueryApplicationService;
        }

        const string AuditEventLogExportKey = "AuditEventLogExport";
        const string AuditLogsExportKey = "AuditLogsExport";
        const string ClientUserManagementExportKey = "ClientUserManagementExport";
        const string PciUserAccessExportKey = "PciUserAccessExport";
        const string PciUserChangeExportKey = "PciUserChangeExport";
        const string PciUserChangeDetailsExportKey = "PciUserChangeDetailsExport";
        const string PciRoleChangeExportKey = "PciRoleChangeExport";

        #region client user management (list)

        [VpAuthorize(Roles = VisitPayRoleStrings.Miscellaneous.IvhAdmin + "," + VisitPayRoleStrings.Miscellaneous.ClientUserManagement + "," + VisitPayRoleStrings.Restricted.IvhUserAuditor + "," + VisitPayRoleStrings.Restricted.ClientUserAuditor)]
        [HttpGet]
        public ActionResult ClientUserManagement()
        {
            ClientUserSearchFilterViewModel model = new ClientUserSearchFilterViewModel
            {
                VisitPayRoles = this.GetVisitPayRoleStringSelectItems(true),
                LastIvhUserAudit = this._auditApplicationService.Value.LastIvhUserAudit(),
                LastClientUserAudit = this._auditApplicationService.Value.LastClientUserAudit()
            };

            this.ViewBag.ConfirmationLabel = this._contentApplicationService.Value.GetLabelAsync("AdminPciUserAuditConfirmationLabel", "Admin PCI User Audit Confirmation Label").Result.ContentBody;

            return this.View(model);
        }

        [VpAuthorize(Roles = VisitPayRoleStrings.Miscellaneous.IvhAdmin + "," + VisitPayRoleStrings.Miscellaneous.ClientUserManagement + "," + VisitPayRoleStrings.Restricted.IvhUserAuditor + "," + VisitPayRoleStrings.Restricted.ClientUserAuditor)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult ClientUserManagement(GridSearchModel<ClientUserSearchFilterViewModel> filterModel, int page, int rows, string sidx, string sord)
        {
            VisitPayUserFilterDto visitPayUserFilterDto = new VisitPayUserFilterDto
            {
                SortField = sidx,
                SortOrder = sord
            };
            Mapper.Map(filterModel.Model, visitPayUserFilterDto);

            visitPayUserFilterDto.IsNotIvhAdminOrIvhUserAuditor = (!this.User.IsInRole(VisitPayRoleStrings.Miscellaneous.IvhAdmin) && !this.User.IsInRole(VisitPayRoleStrings.Restricted.IvhUserAuditor));
            visitPayUserFilterDto.IsNotClientUserManagementOrClientUserAuditor = (!this.User.IsInRole(VisitPayRoleStrings.Miscellaneous.ClientUserManagement) && !this.User.IsInRole(VisitPayRoleStrings.Restricted.ClientUserAuditor));

            VisitPayUserResultsDto visitPayUserResultsDto = this._visitPayUserApplicationService.Value.GetVisitPayClientUsers(visitPayUserFilterDto, this.CurrentUserId, page, rows);
            return this.Json(new
            {
                ClientUsers = Mapper.Map<List<ClientUserSearchResultViewModel>>(visitPayUserResultsDto.VisitPayUsers),
                page = page,
                total = Math.Ceiling((decimal)visitPayUserResultsDto.TotalRecords / rows),
                records = visitPayUserResultsDto.TotalRecords
            });
        }

        #endregion

        #region client user (add/edit)

        [VpAuthorize(Roles = VisitPayRoleStrings.Miscellaneous.IvhAdmin + "," + VisitPayRoleStrings.Miscellaneous.ClientUserManagement)]
        [HttpGet]
        public ActionResult ClientUser(int? id)
        {
            ClientUserViewModel model = new ClientUserViewModel
            {
                VisitPayRoles = this.GetVisitPayRoleStringSelectItems(false),
                //IsSecureCodeRequired = true
            };

            if (!id.HasValue)
            {
                //model.IsSecureCodeRequired = true;
                return this.View(model);
            }

            VisitPayUserDto visitPayUserDto = this._visitPayUserApplicationService.Value.FindById(id.Value.ToString());
            if (visitPayUserDto == null)
            {
                return this.RedirectToAction("ClientUserManagement");
            }

            if (!this.User.IsInRole(VisitPayRoleStrings.Miscellaneous.IvhAdmin) &&
                visitPayUserDto.Roles.Any(x => string.Equals(x.Name, VisitPayRoleStrings.Miscellaneous.IvhAdmin, StringComparison.CurrentCultureIgnoreCase)))
            {
                return this.RedirectToAction("NotAuthorized", "Home");
            }

            Mapper.Map(visitPayUserDto, model);
            //model.IsSecureCodeRequired = (model.VisitPayUserId == 0);
            model.IsCurrentUser = visitPayUserDto.VisitPayUserId == this.CurrentUserId;

            return this.View(model);
        }

        [VpAuthorize(Roles = VisitPayRoleStrings.Miscellaneous.IvhAdmin + "," + VisitPayRoleStrings.Miscellaneous.ClientUserManagement)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ClientUser(ClientUserViewModel model)
        {
            model.IsCurrentUser = model.VisitPayUserId == this.CurrentUserId;

            if (model.IsCurrentUser)
            {
                // This mean the user is editing himself/herself
                // If they are removing the IvhAdmin role we need to force the Indentity framework to reflect that
                // Otherwise this.User.IsInRole(VisitPayRoleStrings.Miscellaneous.IvhAdmin) will continue to return true

                // First, is IvhAdmin being removed
                bool ivhAdminHasBeenRemoved = !model.VisitPayRoleStrings.Select(x => string.Equals(x, VisitPayRoleStrings.Miscellaneous.IvhAdmin, StringComparison.CurrentCultureIgnoreCase)).Any();

                // If IvhAdmin is removed then remove the role from  the claims and force a refresh. 
                if (ivhAdminHasBeenRemoved)
                {
                    this._visitPayUserApplicationService.Value.RemoveClaimWithValue(VisitPayRoleStrings.Miscellaneous.IvhAdmin);
                }
            }
            
            if (!this.ModelState.IsValid)
            {
                model.VisitPayRoles = this.GetVisitPayRoleStringSelectItems(false);
                return this.View(model);
            }

            IdentityResult result;

            if (model.VisitPayUserId.HasValue)
            {
                // update an existing user
                VisitPayUserDto visitPayUserDto = this._visitPayUserApplicationService.Value.FindById(model.VisitPayUserId.Value.ToString());
                visitPayUserDto.Email = model.Email;
                visitPayUserDto.FirstName = model.FirstName;
                visitPayUserDto.LastName = model.LastName;
                visitPayUserDto.SecurityValidationValue = model.SecurityValidationValue;

                bool isCurrentUser = model.VisitPayUserId == this.CurrentUserId;
                bool isIvhAdmin = this.User.IsInRole(VisitPayRoleStrings.Miscellaneous.IvhAdmin);

                IList<string> roles = isCurrentUser && !isIvhAdmin ? visitPayUserDto.Roles.Select(x => x.Name).ToList() : model.VisitPayRoleStrings;
                bool isLocked = isCurrentUser ? visitPayUserDto.IsLocked : model.IsLocked;
                bool isDisabled = isCurrentUser ? visitPayUserDto.IsDisabled : model.IsDisabled;

                // handle account reset (if requested)
                if (model.IsAccountReset)
                {
                    bool success = await this._visitPayUserApplicationService.Value.ClientResetAccount(visitPayUserDto, model.SecurityValidationValue, this.CurrentUserId, this.GetJournalEventHttpContextDto());
                    if (!success)
                    {
                        this.ModelState.AddModelError("", "Error resetting this user's account.");
                        this.View(model);
                    }
                }

                // save any account updates after handling account reset (if requested)
                result = this._visitPayUserApplicationService.Value.UpdateClientUser(visitPayUserDto, roles, isLocked, isDisabled, this.User.IsInRole(VisitPayRoleStrings.Miscellaneous.IvhAdmin));
                this._visitPayUserJournalEventApplicationService.Value.LogClientUserModifyClientAccount(visitPayUserDto.VisitPayUserId, this.CurrentUserId, roles, this.GetJournalEventHttpContextDto());
                
                if (isCurrentUser)
                {
                    this._visitPayUserApplicationService.Value.RefreshRoles(this.User.Identity);
                }
            }
            else
            {
                // create a new user
                VisitPayUserDto visitPayUserDto = new VisitPayUserDto
                {
                    Email = model.Email,
                    EmailConfirmed = false,
                    FirstName = model.FirstName,
                    LastName = model.LastName,
                    LockoutEnabled = true,
                    UserName = model.UserName,
                    SecurityValidationValue = model.SecurityValidationValue
                };

                result = await this._visitPayUserApplicationService.Value.CreateClientUser(visitPayUserDto,
                    model.VisitPayRoleStrings,
                    this.ClientDto.ClientTempPasswordExpLimitInHours,
                    this.ClientDto.ClientPasswordExpLimitInDays);

                if (result.Succeeded)
                {
                    VisitPayUserDto newClientUser = this._visitPayUserApplicationService.Value.FindByName(model.UserName);

                    // log create account
                    this._visitPayUserJournalEventApplicationService.Value.LogClientUserCreateClientAccount(newClientUser.VisitPayUserId, this.CurrentUserId, newClientUser.Roles.Select(x => x.Name).ToList(), this.GetJournalEventHttpContextDto());

                    // log request password
                    this._visitPayUserJournalEventApplicationService.Value.LogClientRequestPassword(newClientUser.VisitPayUserId, this.CurrentUserId, this.GetJournalEventHttpContextDto());
                }
            }

            if (result.Succeeded)
            {
                return this.RedirectToAction("ClientUserManagement");
            }

            result.Errors.ToList().ForEach(error => this.ModelState.AddModelError("", error));
            model.VisitPayRoles = this.GetVisitPayRoleStringSelectItems(false);

            return this.View(model);
        }

        [VpAuthorize(Roles = VisitPayRoleStrings.Miscellaneous.IvhAdmin + "," + VisitPayRoleStrings.Miscellaneous.ClientUserManagement)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<JsonResult> ClientUserResetPassword(int visitPayUserId)
        {
            bool success = await this._visitPayUserApplicationService.Value.ClientResetClientPassword(visitPayUserId, this.ClientDto.ClientTempPasswordExpLimitInHours, this.CurrentUserId, this.GetJournalEventHttpContextDto()).ConfigureAwait(true);

            return this.Json(success);
        }

        [VpAuthorize(Roles = VisitPayRoleStrings.Miscellaneous.IvhAdmin + "," + VisitPayRoleStrings.Miscellaneous.ClientUserManagement)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult ClientUserGenerateSecureCode()
        {
            return this.Json(this.GenerateSecureCodeImpl());
        }

        //[VpAuthorize(Roles = VisitPayRoleStrings.Miscellaneous.IvhAdmin + "," + VisitPayRoleStrings.Miscellaneous.ClientUserManagement)]
        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public async Task<JsonResult> ClientUserResetAccount(int visitPayUserId, string secureCode)
        //{
        //    VisitPayUserDto visitPayUserDto = this._visitPayUserApplicationService.Value.FindById(visitPayUserId.ToString());
        //    if (visitPayUserDto == null)
        //    {
        //        return this.Json(false);
        //    }

        //    bool success = await this._visitPayUserApplicationService.Value.ClientResetAccount(visitPayUserDto, secureCode, this.CurrentUserId);

        //    return this.Json(success);
        //}

        [VpAuthorize(Roles = VisitPayRoleStrings.Miscellaneous.IvhAdmin + "," + VisitPayRoleStrings.Miscellaneous.ClientUserManagement)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult GenerateSecureCode()
        {
            return this.Json(this.GenerateSecureCodeImpl());
        }

        private List<SelectListItem> GetVisitPayRoleStringSelectItems(bool valueIsId)
        {
            List<SelectListItem> items = new List<SelectListItem>();
            IList<VisitPayRoleDto> roles = this._visitPayUserApplicationService.Value.GetClientUserRoles(this.User.IsInRole(VisitPayRoleStrings.Miscellaneous.IvhAdmin));

            if (!this.User.IsInRole(VisitPayRoleStrings.Miscellaneous.IvhAdmin))
            {
                roles = roles.Where(x => !string.Equals(x.Name, VisitPayRoleStrings.Miscellaneous.IvhAdmin, StringComparison.CurrentCultureIgnoreCase)).ToList();
            }

            if (!this._featureApplicationServiceLazy.Value.IsFeatureEnabled(VisitPayFeatureEnum.VisitPayReportsIsEnabled))
            {
                roles = roles.Where(x => !string.Equals(x.Name, VisitPayRoleStrings.Restricted.VisitPayReports, StringComparison.CurrentCultureIgnoreCase)).ToList();
            }

            if (!this._featureApplicationServiceLazy.Value.IsFeatureEnabled(VisitPayFeatureEnum.FeatureChatIsEnabled))
            {
                roles = roles.Where(x => !string.Equals(x.Name, VisitPayRoleStrings.Csr.ChatOperator, StringComparison.CurrentCultureIgnoreCase)).ToList();
                roles = roles.Where(x => !string.Equals(x.Name, VisitPayRoleStrings.Csr.ChatOperatorAdmin, StringComparison.CurrentCultureIgnoreCase)).ToList();
            }

            if (!this.ApplicationSettingsService.Value.IsLockBoxEnabled.Value)
            {
                roles = roles.Where(x => !string.Equals(x.Name, VisitPayRoleStrings.Csr.PaymentQueue, StringComparison.CurrentCultureIgnoreCase)).ToList();
            }

            roles.ForEach(role => items.Add(new SelectListItem
            {
                Text = role.Name,
                Value = (valueIsId) ? role.VisitPayRoleId.ToString() : role.Name
            }));

            return items.OrderBy(x => x.Text).ToList();
        }

        #endregion

        #region payment admin

        [VpAuthorize(Roles = VisitPayRoleStrings.Miscellaneous.IvhAdmin)]
        [HttpGet]
        public ActionResult PaymentAdmin()
        {
            return this.View();
        }

        #endregion

        #region cms admin

        [VpAuthorize(Roles = VisitPayRoleStrings.Miscellaneous.IvhAdmin)]
        [HttpGet]
        public ActionResult CmsAdmin()
        {
            return this.View();
        }

        #endregion

        #region auditing

        [VpAuthorize(Roles = VisitPayRoleStrings.Restricted.AuditReviewer)]
        [HttpGet]
        public ActionResult AuditLogReview()
        {
            return this.View(new AuditLogReviewFilterViewModel());
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [VpAuthorize(Roles = VisitPayRoleStrings.Restricted.AuditReviewer)]
        public JsonResult AuditLogs(GridSearchModel<AuditLogReviewFilterViewModel> filterModel, int page, int rows, string sidx, string sord)
        {
            IList<AuditLogDto> results = this._auditApplicationService.Value.GetAuditLogs(new AuditLogFilterDto
            {
                StartDate = filterModel.Filter.BeginDate ?? DateTime.UtcNow.AddDays(-30),
                EndDate = filterModel.Filter.EndDate ?? DateTime.UtcNow,
                SortField = sidx,
                SortOrder = sord
            });

            return this.Json(new GridResultsModel<AuditLogViewModel>(filterModel.Page, filterModel.Rows, results.Count)
            {
                Results = Mapper.Map<IList<AuditLogViewModel>>(results).Skip((filterModel.Page - 1) * filterModel.Rows).Take(filterModel.Rows).ToList()
            });
        }

        [VpAuthorize(Roles = VisitPayRoleStrings.Restricted.AuditEventLogViewer)]
        [HttpGet]
        public ActionResult AuditEventLog()
        {
            return this.View(new JournalEventFilterViewModel
            {
                JournalEventTypes = Enum.GetValues(typeof(JournalEventTypeEnum)).Cast<JournalEventTypeEnum>().Select(s => new SelectListItem { Value = ((int)s).ToString(), Text = $"{(int)s}: {s}" }).ToList(),
                JournalEventCategories = Enum.GetValues(typeof(JournalEventCategoryEnum)).Cast<JournalEventCategoryEnum>().Select(s => new SelectListItem { Value = ((int)s).ToString(), Text = $"{(int)s}: {s}" }).ToList()
            });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [VpAuthorize(Roles = VisitPayRoleStrings.Restricted.AuditEventLogViewer)]
        public ActionResult AuditEventLog(GridSearchModel<JournalEventFilterViewModel> filterModel, int page, int rows, string sidx, string sord)
        {
            this._visitPayUserJournalEventApplicationService.Value.LogViewAuditLogList(
                this.CurrentUserId,
                this.GetJournalEventHttpContextDto(),
                filterModel.Filter.JournalEventType,
                filterModel.Filter.BeginDate,
                filterModel.Filter.EndDate,
                filterModel.Filter.UserNameFilter,
                filterModel.Filter.VpGuarantorId);

            JournalEventResultsDto results = this._journalEventQueryApplicationService.Value.GetJournalEvents(new JournalEventFilterDto
            {
                JournalEventTypes = filterModel.Filter.JournalEventType.HasValue ? new List<JournalEventTypeEnum> { filterModel.Filter.JournalEventType.Value } : null,
                JournalEventCategories = filterModel.Filter.JournalEventCategory.HasValue ? new List<JournalEventCategoryEnum> { filterModel.Filter.JournalEventCategory.Value } : null,
                StartDate = filterModel.Filter.BeginDate ?? DateTime.MinValue,
                EndDate = filterModel.Filter.EndDate ?? DateTime.UtcNow,
                UserName = filterModel.Filter.UserNameFilter,
                VpGuarantorId = filterModel.Filter.VpGuarantorId,
                SortField = sidx,
                SortOrder = sord,
                Page = page,
                Rows = rows
            });

            return this.Json(new GridResultsModel<JournalEventResultsViewModel>(filterModel.Page, filterModel.Rows, results.TotalRecords)
            {
                Results = Mapper.Map<IList<JournalEventResultsViewModel>>(results.JournalEvents)
            });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [VpAuthorize(Roles = VisitPayRoleStrings.Restricted.AuditEventLogViewer)]
        [Common.Web.Attributes.SessionState(SessionStateBehavior.Required)]
        public ActionResult AuditEventLogExport(GridSearchModel<JournalEventFilterViewModel> filterModel, int page, int rows, string sidx, string sord)
        {
            byte[] bytes = this._journalEventQueryApplicationService.Value.ExportJournalEvents(new JournalEventFilterDto
            {
                JournalEventTypes = filterModel.Filter.JournalEventType.HasValue ? new List<JournalEventTypeEnum> { filterModel.Filter.JournalEventType.Value } : null,
                JournalEventCategories = filterModel.Filter.JournalEventCategory.HasValue ? new List<JournalEventCategoryEnum> { filterModel.Filter.JournalEventCategory.Value } : null,
                StartDate = filterModel.Filter.BeginDate ?? DateTime.MinValue,
                EndDate = filterModel.Filter.EndDate ?? DateTime.UtcNow,
                UserName = filterModel.Filter.UserNameFilter,
                VpGuarantorId = filterModel.Filter.VpGuarantorId,
                SortField = sidx,
                SortOrder = sord
            });
            this.SetTempData(AuditEventLogExportKey, bytes);

            return this.Json(new ResultMessage(true, this.Url.Action("AuditEventLogExportDownload")));
        }

        [HttpGet]
        [VpAuthorize(Roles = VisitPayRoleStrings.Restricted.AuditEventLogViewer)]
        public ActionResult AuditEventLogExportDownload()
        {
            object tempData = this.TempData[AuditEventLogExportKey];
            if (tempData != null)
                this.WriteExcelInline((byte[])tempData, "AuditEventLog.xlsx");

            return this.Json(new { }, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        [VpAuthorize(Roles = VisitPayRoleStrings.Restricted.AuditEventLogViewer)]
        public PartialViewResult AuditEventLogDetails(int eventJournalId)
        {
            this._visitPayUserJournalEventApplicationService.Value.LogViewAuditLogDetail(this.CurrentUserId, this.GetJournalEventHttpContextDto(), eventJournalId);

            JournalEventDto result = this._journalEventQueryApplicationService.Value.GetJournalEventDetail(eventJournalId);

            return this.PartialView("_AuditLogEventDetail", Mapper.Map<JournalEventResultsViewModel>(result));
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [VpAuthorize(Roles = VisitPayRoleStrings.Restricted.AuditReviewer)]
        [Common.Web.Attributes.SessionState(SessionStateBehavior.Required)]
        public ActionResult AuditLogsExport(GridSearchModel<AuditLogReviewFilterViewModel> filterModel, string sidx, string sord)
        {
            byte[] bytes = this._auditApplicationService.Value.ExportAuditLogs(new AuditLogFilterDto
            {
                StartDate = filterModel.Filter.BeginDate ?? DateTime.UtcNow.AddDays(-30),
                EndDate = filterModel.Filter.EndDate ?? DateTime.UtcNow,
                SortField = sidx,
                SortOrder = sord
            });
            this.SetTempData(AuditLogsExportKey, bytes);

            return this.Json(new ResultMessage(true, this.Url.Action("AuditLogsExportDownload")));
        }

        [HttpGet]
        [VpAuthorize(Roles = VisitPayRoleStrings.Restricted.AuditReviewer)]
        public ActionResult AuditLogsExportDownload()
        {
            object tempData = this.TempData[AuditLogsExportKey];
            if (tempData != null)
                this.WriteExcelInline((byte[])tempData, "AuditLogs.xlsx");

            return this.Json(new { }, JsonRequestBehavior.AllowGet);
        }

        [VpAuthorize(Roles = VisitPayRoleStrings.Restricted.IvhPciAuditor + "," + VisitPayRoleStrings.Restricted.ClientPciAuditor)]
        [HttpGet]
        public ActionResult PciAudit()
        {
            this._auditApplicationService.Value.LogViewPciLog(this.CurrentUserId, this.GetJournalEventHttpContextDto());
            DateTime? lastIvhAudit = this._auditApplicationService.Value.LastIvhPciAudit();
            DateTime? lastClientAudit = this._auditApplicationService.Value.LastClientPciAudit();
            DateTime lastAuditDate = lastIvhAudit ?? lastClientAudit ?? DateTime.UtcNow.AddDays(-30);
            this.ViewBag.PageLabel = this._contentApplicationService.Value.GetLabelAsync("AdminPciAuditPageLabel", "View PCI Audit Logs").Result.ContentBody;
            this.ViewBag.ConfirmationLabel = this._contentApplicationService.Value.GetLabelAsync("AdminPciAuditConfirmationLabel", "Admin PCI Audit Confirmation Label").Result.ContentBody;
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            this.ViewBag.InitialData = serializer.Serialize(new
            {
                StartTime = this.TimeZoneHelper.Client.ToLocalDateTime(lastAuditDate).ToString("G"),
                StopTime = DateTime.UtcNow.ToString("G")
            });

            return this.View(new PciAuditFilterViewModel
            {
                BeginDate = this.TimeZoneHelper.Client.ToLocalDateTime(lastAuditDate),
                BeginTime = this.TimeZoneHelper.Client.ToLocalDateTime(lastAuditDate),
                EndDate = DateTime.UtcNow,
                EndTime = DateTime.UtcNow,
                LastIvhAudit = lastIvhAudit,
                LastClientAudit = lastClientAudit
            });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [VpAuthorize(Roles = VisitPayRoleStrings.Restricted.IvhPciAuditor + "," + VisitPayRoleStrings.Restricted.ClientPciAuditor)]
        public JsonResult ConfirmAudit(PciAuditFilterViewModel filterModel, string notes)
        {
            this._auditApplicationService.Value.LogConfirmAudit(
                this.CurrentUserId,
                this.User.IsInRole(VisitPayRoleStrings.Restricted.IvhPciAuditor),
                this.User.IsInRole(VisitPayRoleStrings.Restricted.ClientPciAuditor),
                filterModel.BeginDateTime,
                filterModel.EndDateTime,
                notes,
                this.GetJournalEventHttpContextDto());
            return this.Json(new ResultMessage(true, this.Url.Action("PciAudit")));
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [VpAuthorize(Roles = VisitPayRoleStrings.Restricted.IvhUserAuditor + "," + VisitPayRoleStrings.Restricted.ClientUserAuditor)]
        public JsonResult CertifyAudit(string notes)
        {
            this._auditApplicationService.Value.LogUserManagementAudit(
                this.CurrentUserId,
                this.User.IsInRole(VisitPayRoleStrings.Restricted.IvhUserAuditor),
                this.User.IsInRole(VisitPayRoleStrings.Restricted.ClientUserAuditor),
                notes,
                this.GetJournalEventHttpContextDto());
            return this.Json(new ResultMessage(true, this.Url.Action("ClientUserManagement")));
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [VpAuthorize(Roles = VisitPayRoleStrings.Miscellaneous.IvhAdmin + "," + VisitPayRoleStrings.Miscellaneous.ClientUserManagement + "," + VisitPayRoleStrings.Restricted.IvhUserAuditor + "," + VisitPayRoleStrings.Restricted.ClientUserAuditor)]
        [Common.Web.Attributes.SessionState(SessionStateBehavior.Required)]
        public async Task<ActionResult> ClientUserManagementExport(GridSearchModel<ClientUserSearchFilterViewModel> filterModel, string sidx, string sord)

        {
            VisitPayUserFilterDto visitPayUserFilterDto = new VisitPayUserFilterDto
            {
                SortField = sidx,
                SortOrder = sord
            };
            Mapper.Map(filterModel.Model, visitPayUserFilterDto);
            byte[] bytes = await this._visitPayUserApplicationService.Value.ExportVisitPayClientUsersAsync(
                visitPayUserFilterDto,
                this.CurrentUserId,
                this.User.IsInRole(VisitPayRoleStrings.Restricted.IvhUserAuditor) || this.User.IsInRole(VisitPayRoleStrings.Miscellaneous.IvhAdmin),
                this.User.IsInRole(VisitPayRoleStrings.Restricted.ClientUserAuditor) || this.User.IsInRole(VisitPayRoleStrings.Miscellaneous.ClientUserManagement));
            this.SetTempData(ClientUserManagementExportKey, bytes);

            return this.Json(new ResultMessage(true, this.Url.Action("ClientUserManagementExportDownload")));
        }

        [HttpGet]
        [VpAuthorize(Roles = VisitPayRoleStrings.Miscellaneous.IvhAdmin + "," + VisitPayRoleStrings.Miscellaneous.ClientUserManagement + "," + VisitPayRoleStrings.Restricted.IvhUserAuditor + "," + VisitPayRoleStrings.Restricted.ClientUserAuditor)]
        public ActionResult ClientUserManagementExportDownload()
        {
            object tempData = this.TempData[ClientUserManagementExportKey];
            if (tempData != null)
                this.WriteExcelInline((byte[])tempData, "ClientUserManagement.xlsx");

            return this.Json(new { }, JsonRequestBehavior.AllowGet);
        }

        #region PCI User Access
        [HttpPost]
        [ValidateAntiForgeryToken]
        [VpAuthorize(Roles = VisitPayRoleStrings.Restricted.IvhPciAuditor + "," + VisitPayRoleStrings.Restricted.ClientPciAuditor)]
        public JsonResult PciUserAccess(GridSearchModel<PciAuditFilterViewModel> filterModel, int page, int rows, string sidx, string sord)
        {
            IList<AuditUserAccessDto> results = this._auditApplicationService.Value.GetUserAccessLogsSince(new AuditFilterDto
            {
                StartDate = filterModel.Filter.BeginDateTime,
                EndDate = filterModel.Filter.EndDateTime,
                IncludeClient = this.User.IsInRole(VisitPayRoleStrings.Restricted.ClientPciAuditor),
                IncludeIvinci = this.User.IsInRole(VisitPayRoleStrings.Restricted.IvhPciAuditor),
                SortField = sidx,
                SortOrder = sord
            });

            return this.Json(new GridResultsModel<AuditUserAccessViewModel>(filterModel.Page, filterModel.Rows, results.Count)
            {
                Results = Mapper.Map<IList<AuditUserAccessViewModel>>(results).Skip((filterModel.Page - 1) * filterModel.Rows).Take(filterModel.Rows).ToList()
            });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [VpAuthorize(Roles = VisitPayRoleStrings.Restricted.IvhPciAuditor + "," + VisitPayRoleStrings.Restricted.ClientPciAuditor)]
        [Common.Web.Attributes.SessionState(SessionStateBehavior.Required)]
        public ActionResult PciUserAccessExport(GridSearchModel<PciAuditFilterViewModel> filterModel, string sidx, string sord)
        {
            byte[] bytes = this._auditApplicationService.Value.ExportUserAccessLogs(new AuditFilterDto
            {
                StartDate = filterModel.Filter.BeginDateTime,
                EndDate = filterModel.Filter.EndDateTime,
                IncludeClient = this.User.IsInRole(VisitPayRoleStrings.Restricted.ClientPciAuditor),
                IncludeIvinci = this.User.IsInRole(VisitPayRoleStrings.Restricted.IvhPciAuditor),
                SortField = sidx,
                SortOrder = sord
            });
            this.SetTempData(PciUserAccessExportKey, bytes);

            return this.Json(new ResultMessage(true, this.Url.Action("PciUserAccessExportDownload")));
        }

        [HttpGet]
        [VpAuthorize(Roles = VisitPayRoleStrings.Restricted.IvhPciAuditor + "," + VisitPayRoleStrings.Restricted.ClientPciAuditor)]
        public ActionResult PciUserAccessExportDownload()
        {
            object tempData = this.TempData[PciUserAccessExportKey];
            if (tempData != null)
                this.WriteExcelInline((byte[])tempData, "PciUserAccess.xlsx");

            return this.Json(new { }, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region PCI User Change
        [HttpPost]
        [ValidateAntiForgeryToken]
        [VpAuthorize(Roles = VisitPayRoleStrings.Restricted.IvhPciAuditor + "," + VisitPayRoleStrings.Restricted.ClientPciAuditor)]
        public JsonResult PciUserChange(GridSearchModel<PciAuditFilterViewModel> filterModel, int page, int rows, string sidx, string sord)
        {
            IList<AuditUserChangeDto> results = this._auditApplicationService.Value.GetUserChangeLogsSince(new AuditFilterDto
            {
                StartDate = filterModel.Filter.BeginDateTime,
                EndDate = filterModel.Filter.EndDateTime,
                IncludeClient = this.User.IsInRole(VisitPayRoleStrings.Restricted.ClientPciAuditor),
                IncludeIvinci = this.User.IsInRole(VisitPayRoleStrings.Restricted.IvhPciAuditor),
                SortField = sidx,
                SortOrder = sord
            });

            return this.Json(new GridResultsModel<AuditUserChangeViewModel>(filterModel.Page, filterModel.Rows, results.Count)
            {
                Results = Mapper.Map<IList<AuditUserChangeViewModel>>(results).Skip((filterModel.Page - 1) * filterModel.Rows).Take(filterModel.Rows).ToList()
            });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [VpAuthorize(Roles = VisitPayRoleStrings.Restricted.IvhPciAuditor + "," + VisitPayRoleStrings.Restricted.ClientPciAuditor)]
        [Common.Web.Attributes.SessionState(SessionStateBehavior.Required)]
        public ActionResult PciUserChangeExport(GridSearchModel<PciAuditFilterViewModel> filterModel, string sidx, string sord)
        {
            byte[] bytes = this._auditApplicationService.Value.ExportUserChangeLogs(new AuditFilterDto
            {
                StartDate = filterModel.Filter.BeginDateTime,
                EndDate = filterModel.Filter.EndDateTime,
                IncludeClient = this.User.IsInRole(VisitPayRoleStrings.Restricted.ClientPciAuditor),
                IncludeIvinci = this.User.IsInRole(VisitPayRoleStrings.Restricted.IvhPciAuditor),
                SortField = sidx,
                SortOrder = sord
            });
            this.SetTempData(PciUserChangeExportKey, bytes);

            return this.Json(new ResultMessage(true, this.Url.Action("PciUserChangeExportDownload")));
        }

        [HttpGet]
        [VpAuthorize(Roles = VisitPayRoleStrings.Restricted.IvhPciAuditor + "," + VisitPayRoleStrings.Restricted.ClientPciAuditor)]
        public ActionResult PciUserChangeExportDownload()
        {
            object tempData = this.TempData[PciUserChangeExportKey];
            if (tempData != null)
                this.WriteExcelInline((byte[])tempData, "PciUserChange.xlsx");

            return this.Json(new { }, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region PCI User Change Details
        [HttpPost]
        [ValidateAntiForgeryToken]
        [VpAuthorize(Roles = VisitPayRoleStrings.Restricted.IvhPciAuditor + "," + VisitPayRoleStrings.Restricted.ClientPciAuditor)]
        public JsonResult PciUserChangeDetails(GridSearchModel<PciAuditFilterViewModel> filterModel, int page, int rows, string sidx, string sord)
        {
            IList<AuditUserChangeDetailDto> results = this._auditApplicationService.Value.GetUserChangeDetailLogsSince(new AuditFilterDto
            {
                StartDate = filterModel.Filter.BeginDateTime,
                EndDate = filterModel.Filter.EndDateTime,
                IncludeClient = this.User.IsInRole(VisitPayRoleStrings.Restricted.ClientPciAuditor),
                IncludeIvinci = this.User.IsInRole(VisitPayRoleStrings.Restricted.IvhPciAuditor),
                SortField = sidx,
                SortOrder = sord
            });

            return this.Json(new GridResultsModel<AuditUserChangeDetailViewModel>(filterModel.Page, filterModel.Rows, results.Count)
            {
                Results = Mapper.Map<IList<AuditUserChangeDetailViewModel>>(results).Skip((filterModel.Page - 1) * filterModel.Rows).Take(filterModel.Rows).ToList()
            });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [VpAuthorize(Roles = VisitPayRoleStrings.Restricted.IvhPciAuditor + "," + VisitPayRoleStrings.Restricted.ClientPciAuditor)]
        [Common.Web.Attributes.SessionState(SessionStateBehavior.Required)]
        public ActionResult PciUserChangeDetailsExport(GridSearchModel<PciAuditFilterViewModel> filterModel, string sidx, string sord)
        {
            byte[] bytes = this._auditApplicationService.Value.ExportUserChangeDetailLogs(new AuditFilterDto
            {
                StartDate = filterModel.Filter.BeginDateTime,
                EndDate = filterModel.Filter.EndDateTime,
                IncludeClient = this.User.IsInRole(VisitPayRoleStrings.Restricted.ClientPciAuditor),
                IncludeIvinci = this.User.IsInRole(VisitPayRoleStrings.Restricted.IvhPciAuditor),
                SortField = sidx,
                SortOrder = sord
            });
            this.SetTempData(PciUserChangeDetailsExportKey, bytes);

            return this.Json(new ResultMessage(true, this.Url.Action("PciUserChangeDetailsExportDownload")));
        }

        [HttpGet]
        [VpAuthorize(Roles = VisitPayRoleStrings.Restricted.IvhPciAuditor + "," + VisitPayRoleStrings.Restricted.ClientPciAuditor)]
        public ActionResult PciUserChangeDetailsExportDownload()
        {
            object tempData = this.TempData[PciUserChangeDetailsExportKey];
            if (tempData != null)
                this.WriteExcelInline((byte[])tempData, "PciUserChangeDetails.xlsx");

            return this.Json(new { }, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region PCI Role Change
        [HttpPost]
        [ValidateAntiForgeryToken]
        [VpAuthorize(Roles = VisitPayRoleStrings.Restricted.IvhPciAuditor + "," + VisitPayRoleStrings.Restricted.ClientPciAuditor)]
        public JsonResult PciRoleChange(GridSearchModel<PciAuditFilterViewModel> filterModel, int page, int rows, string sidx, string sord)
        {
            IList<AuditRoleChangeDto> results = this._auditApplicationService.Value.GetRoleChangeLogsSince(new AuditFilterDto
            {
                StartDate = filterModel.Filter.BeginDateTime,
                EndDate = filterModel.Filter.EndDateTime,
                IncludeClient = this.User.IsInRole(VisitPayRoleStrings.Restricted.ClientPciAuditor),
                IncludeIvinci = this.User.IsInRole(VisitPayRoleStrings.Restricted.IvhPciAuditor),
                SortField = sidx,
                SortOrder = sord
            });

            return this.Json(new GridResultsModel<AuditRoleChangeViewModel>(filterModel.Page, filterModel.Rows, results.Count)
            {
                Results = Mapper.Map<IList<AuditRoleChangeViewModel>>(results).Skip((filterModel.Page - 1) * filterModel.Rows).Take(filterModel.Rows).ToList()
            });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [VpAuthorize(Roles = VisitPayRoleStrings.Restricted.IvhPciAuditor + "," + VisitPayRoleStrings.Restricted.ClientPciAuditor)]
        [Common.Web.Attributes.SessionState(SessionStateBehavior.Required)]
        public ActionResult PciRoleChangeExport(GridSearchModel<PciAuditFilterViewModel> filterModel, string sidx, string sord)
        {
            byte[] bytes = this._auditApplicationService.Value.ExportRoleChangeLogs(new AuditFilterDto
            {
                StartDate = filterModel.Filter.BeginDateTime,
                EndDate = filterModel.Filter.EndDateTime,
                IncludeClient = this.User.IsInRole(VisitPayRoleStrings.Restricted.ClientPciAuditor),
                IncludeIvinci = this.User.IsInRole(VisitPayRoleStrings.Restricted.IvhPciAuditor),
                SortField = sidx,
                SortOrder = sord
            });
            this.SetTempData(PciRoleChangeExportKey, bytes);

            return this.Json(new ResultMessage(true, this.Url.Action("PciRoleChangeExportDownload")));
        }

        [HttpGet]
        [VpAuthorize(Roles = VisitPayRoleStrings.Restricted.IvhPciAuditor + "," + VisitPayRoleStrings.Restricted.ClientPciAuditor)]
        public ActionResult PciRoleChangeExportDownload()
        {
            object tempData = this.TempData[PciRoleChangeExportKey];
            if (tempData != null)
                this.WriteExcelInline((byte[])tempData, "PciRoleChange.xlsx");

            return this.Json(new { }, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #endregion

        #region system exceptions

        [VpAuthorize(Roles = VisitPayRoleStrings.Miscellaneous.IvhAdmin)]
        [HttpGet]
        public ActionResult ResolveSystemExceptions()
        {
            return this.View();
        }

        [VpAuthorize(Roles = VisitPayRoleStrings.Miscellaneous.IvhAdmin)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ResolveSystemExceptions(string transactionId, int vpGuarantorId)
        {
            if (!string.IsNullOrEmpty(transactionId) && vpGuarantorId > 0)
            {
                this._achApplicationService.Value.ProcessAchReturnAfterSettlement(transactionId, vpGuarantorId);
            }
            return this.RedirectToAction("ResolveSystemExceptions");
        }

        #endregion

        #region helper methods

        private string GenerateSecureCodeImpl()
        {
            Random rnd = new Random();
            const int len = 6;
            string secureCode = "";
            //var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789".ToCharArray();
            char[] possible = "0123456789".ToCharArray();

            for (int i = 0; i < len; i++)
            {
                int cn = rnd.Next(0, possible.Length);
                secureCode += possible[cn];
            }

            return secureCode;
        }

        private JournalEventHttpContextDto GetJournalEventHttpContextDto()
        {
            return Mapper.Map<JournalEventHttpContextDto>(System.Web.HttpContext.Current);
        }

        #endregion

    }
}
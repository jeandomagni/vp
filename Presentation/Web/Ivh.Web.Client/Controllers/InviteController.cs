﻿using System;
using System.Web.Mvc;

namespace Ivh.Web.Client.Controllers
{
    using Application.Core.Common.Dtos;
    using Application.Core.Common.Interfaces;
    using Attributes;
    using AutoMapper;
    using Common.VisitPay.Strings;
    using Common.Web.Interfaces;
    using Microsoft.AspNet.Identity;
    using Models;

    [VpAuthorize(Roles = VisitPayRoleStrings.System.Client + "," + VisitPayRoleStrings.Admin.Administrator + ",")]
    [VpAuthorize(Roles = VisitPayRoleStrings.Csr.CSR + "," + VisitPayRoleStrings.Csr.SupportAdmin + "," + VisitPayRoleStrings.Csr.SupportView + "," + VisitPayRoleStrings.Csr.UserAdmin)]
    public class InviteController : BaseController
    {
        private readonly Lazy<IGuarantorApplicationService> _guarantorApplicationService;

        public InviteController(
            Lazy<IBaseControllerService> baseControllerService,
            Lazy<IGuarantorApplicationService> guarantorApplicationService)
            : base(baseControllerService)
        {
            this._guarantorApplicationService = guarantorApplicationService;
        }

        [HttpGet]
        public ActionResult InviteGuarantor()
        {
            return this.View(new GuarantorInvitationViewModel() { ClientBrandName = this.ClientDto.ClientBrandName });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult InviteGuarantor(GuarantorInvitationViewModel model)
        {
            GuarantorInvitationDto guarantorInvitationDto = Mapper.Map<GuarantorInvitationDto>(model);
            guarantorInvitationDto.VisitPayUserId = this.CurrentUserId;
            this._guarantorApplicationService.Value.SendGuarantorInvitation(guarantorInvitationDto);
            ResultMessage resultMessage = new ResultMessage(true, this.Url.Action("InviteGuarantor", "Invite"));
            return this.Json(resultMessage);
        }
    }
}
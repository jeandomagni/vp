﻿namespace Ivh.Web.Client.Controllers
{
    using System;
    using System.Web.Mvc;
    using Attributes;
    using Common.VisitPay.Enums;
    using Common.VisitPay.Strings;
    using Common.Web.Controllers;
    using Common.Web.Filters;
    using Common.Web.Interfaces;

    [VpAuthorize(Roles = VisitPayRoleStrings.Csr.CallCenter + "," + VisitPayRoleStrings.Admin.Administrator)]
    [RequireFeature(VisitPayFeatureEnum.OfflineVisitPay)]
    public class TraceController : BaseWebController
    {
        public TraceController(Lazy<IBaseControllerService> baseControllerService) : base(baseControllerService)
        {
        }

        [HttpPost]
        [RequireFeature(VisitPayFeatureEnum.FeatureCardReaderIsEnabled)]
        public ActionResult BlackoutStart(string userAccount, string clientName)
        {
            this.Logger.Value.Debug(() => $"ClientApi:{nameof(MonitoringController)}::{nameof(this.BlackoutStart)} - User Account: {userAccount} - Client Name: {clientName}");
            return this.Json(true);
        }

        [HttpPost]
        [RequireFeature(VisitPayFeatureEnum.FeatureCardReaderIsEnabled)]
        public ActionResult BlackoutStop(string userAccount, string clientName)
        {
            this.Logger.Value.Debug(() => $"ClientApi:{nameof(MonitoringController)}::{nameof(this.BlackoutStop)} - User Account: {userAccount} - Client Name: {clientName}");
            return this.Json(true);
        }

    }
}
﻿using System;
using System.Web.Mvc;

namespace Ivh.Web.Client.Controllers
{
    using System.Threading.Tasks;
    using System.Web.SessionState;
    using Application.Content.Common.Interfaces;
    using Application.Core.Common.Dtos;
    using Application.Core.Common.Interfaces;
    using Application.FileStorage.Common.Interfaces;
    using Attributes;
    using AutoMapper;
    using Common.Base.Enums;
    using Common.VisitPay.Enums;
    using Common.VisitPay.Strings;
    using Common.Web.Controllers;
    using Common.Web.Filters;
    using Common.Web.Interfaces;
    using Common.Web.Models.Itemization;
    using Microsoft.AspNet.Identity;

    [VpAuthorize(Roles = VisitPayRoleStrings.System.Client + "," + VisitPayRoleStrings.Admin.Administrator + ",")]
    [VpAuthorize(Roles = VisitPayRoleStrings.Csr.CSR)]
    [RequireFeature(VisitPayFeatureEnum.Itemization)]
    public class ItemizationController : BaseItemizationController
    {
        private readonly Lazy<IVisitItemizationStorageApplicationService> _visitItemizationStorageApplicationService;
        private readonly Lazy<IGuarantorApplicationService> _guarantorApplicationService;
        private readonly Lazy<IVisitPayUserApplicationService> _visitPayUserApplicationService;

        public ItemizationController(
            Lazy<IBaseControllerService> baseControllerService,
            Lazy<IContentApplicationService> contentApplicationService,
            Lazy<IFileStorageApplicationService> fileStorageApplicationService,
            Lazy<IVisitItemizationStorageApplicationService> visitItemizationStorageApplicationService,
            Lazy<IGuarantorApplicationService> guarantorApplicationService,
            Lazy<IVisitPayUserApplicationService> visitPayUserApplicationService)
            : base(baseControllerService,
            contentApplicationService,
            fileStorageApplicationService,
            visitItemizationStorageApplicationService)
        {
            this._visitItemizationStorageApplicationService = visitItemizationStorageApplicationService;
            this._guarantorApplicationService = guarantorApplicationService;
            this._visitPayUserApplicationService = visitPayUserApplicationService;
        }


        [HttpGet]
        public PartialViewResult ItemizationsFilter(int visitPayUserId)
        {
            int vpGuarantorId = this._visitPayUserApplicationService.Value.GetGuarantorIdFromVisitPayUserId(visitPayUserId);
            ItemizationSearchFilterViewModel model = this.GetItemizationSearchFilter(vpGuarantorId);

            return this.PartialView("_ItemizationsFilter", model);
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        [Common.Web.Attributes.SessionState(SessionStateBehavior.Required)]
        public JsonResult Itemizations(ItemizationSearchFilterViewModel model, int page, int rows, string sidx, string sord)
        {
            int vpGuarantorId = this._visitPayUserApplicationService.Value.GetGuarantorIdFromVisitPayUserId(model.VisitPayUserId);
            ItemizationSearchResultsViewModel results = this.GetItemizations(vpGuarantorId, model, page, rows, sidx, sord);

            return this.Json(results);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult ItemizationTotals(ItemizationSearchFilterViewModel model)
        {
            int vpGuarantorId = this._visitPayUserApplicationService.Value.GetGuarantorIdFromVisitPayUserId(model.VisitPayUserId);
            VisitItemizationStorageFilterDto visitItemizationStorageFilterDto = Mapper.Map<VisitItemizationStorageFilterDto>(model);
            visitItemizationStorageFilterDto.CurrentVisitPayUserId = this.CurrentUserId;
            return this.Json(this._visitItemizationStorageApplicationService.Value.GetItemizationTotals(vpGuarantorId, visitItemizationStorageFilterDto));
        }


        [HttpGet]
        public async Task<ActionResult> ItemizationDownload(Guid id, int visitPayUserId)
        {
            int vpGuarantorId = this._visitPayUserApplicationService.Value.GetGuarantorIdFromVisitPayUserId(visitPayUserId);
            return await base.ItemizationDownload(id, vpGuarantorId, this.CurrentUserId);
        }
    }
}
﻿namespace Ivh.Web.Client.Controllers
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Threading.Tasks;
    using System.Web;
    using System.Web.Mvc;
    using System.Web.SessionState;
    using Application.Base.Common.Dtos;
    using Application.Core.Common.Dtos;
    using Application.Core.Common.Interfaces;
    using Application.SecureCommunication.Common.Dtos;
    using Application.SecureCommunication.Common.Interfaces;
    using Application.User.Common.Dtos;
    using Attributes;
    using AutoMapper;
    using Common.Base.Enums;
    using Common.Base.Utilities.Extensions;
    using Common.EventJournal;
    using Common.VisitPay.Enums;
    using Common.VisitPay.Strings;
    using Common.Web.Interfaces;
    using Common.Web.Models;
    using Microsoft.AspNet.Identity;
    using Models.ClientSupport;

    [VpAuthorize(Roles = VisitPayRoleStrings.System.Client + "," + VisitPayRoleStrings.Admin.Administrator + ",")]
    [VpAuthorize(Roles = VisitPayRoleStrings.Miscellaneous.VpSupportTickets + "," + VisitPayRoleStrings.Miscellaneous.VpSupportTicketAdmin + "," + VisitPayRoleStrings.Miscellaneous.VpSupportTicketsManage)]
    public class ClientSupportController : BaseController
    {
        private readonly Lazy<IClientSupportRequestApplicationService> _clientSupportRequestApplicationService;
        private readonly Lazy<IVisitPayUserApplicationService> _visitPayUserApplicationService;
        private readonly Lazy<IVisitPayUserJournalEventApplicationService> _visitPayUserJournalEventApplicationService;

        public ClientSupportController(
            Lazy<IBaseControllerService> baseControllerService,
            Lazy<IClientSupportRequestApplicationService> clientSupportRequestApplicationService,
            Lazy<IVisitPayUserApplicationService> visitPayUserApplicationService,
            Lazy<IVisitPayUserJournalEventApplicationService> visitPayUserJournalEventApplicationService)
            : base(baseControllerService)
        {
            this._clientSupportRequestApplicationService = clientSupportRequestApplicationService;
            this._visitPayUserApplicationService = visitPayUserApplicationService;
            this._visitPayUserJournalEventApplicationService = visitPayUserJournalEventApplicationService;
        }

        #region admin grid

        private const string UnassignedAdminUser = "<Unassigned>";
        private const int UnassignedAdminUserId = -1;
        private const int AdminDaysOffset = -30;

        const string ClientAdminSupportRequestsExportKey = "ClientAdminSupportRequestsExport";
        const string SupportRequestAttachmentDownloadKey = "SupportRequestAttachmentDownload";

        [VpAuthorize(Roles = VisitPayRoleStrings.Miscellaneous.VpSupportTicketAdmin)]
        [HttpGet]
        public ActionResult ClientAdminSupportRequests(int? u)
        {
            ClientSupportRequestFilterViewModel model = new ClientSupportRequestFilterViewModel
            {
                DaysOffset = AdminDaysOffset
            };

            model.AdminVisitPayUsers = GetAdminVisitPayUsers();

            string uText = u.GetValueOrDefault(UnassignedAdminUserId).ToString();
            bool foundInAdminUsers = model.AdminVisitPayUsers.Where(x => x.Value == uText).Any();

            if (foundInAdminUsers)
            {
                model.AssignedToVisitPayUserId = u.GetValueOrDefault(UnassignedAdminUserId);
            }
            else
            {
                model.AssignedToVisitPayUserId = UnassignedAdminUserId;
            }

            return this.View(model);
        }

        [VpAuthorize(Roles = VisitPayRoleStrings.Miscellaneous.VpSupportTicketAdmin)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult ClientAdminSupportUsers()
        {
            IList<SelectListItem> users = this.GetAdminVisitPayUsers();
            var result = users.Select(x => new { x.Text, x.Value });
            return this.Json(result);
        }

        private IList<SelectListItem> GetAdminVisitPayUsers()
        {
            IDictionary<int, string> vpSupportAdminUsers = this._clientSupportRequestApplicationService.Value.GetAssignableUsers(
                assignedOnly: true, 
                assignedVisitPayUserId: null);
            List<SelectListItem> adminVisitPayUsers = vpSupportAdminUsers.Select(x => new SelectListItem { Text = x.Value, Value = x.Key.ToString() }).ToList();
            adminVisitPayUsers.Insert(0, new SelectListItem { Text = UnassignedAdminUser, Value = UnassignedAdminUserId.ToString() });
            return adminVisitPayUsers;
        }

        [VpAuthorize(Roles = VisitPayRoleStrings.Miscellaneous.VpSupportTicketAdmin)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult ClientAdminSupportRequests(GridSearchModel<ClientSupportRequestFilterViewModel> filterModel)
        {
            ClientSupportRequestResultsDto resultsDto = this._clientSupportRequestApplicationService.Value.GetClientSupportRequests(Mapper.Map<ClientSupportRequestFilterDto>(filterModel));

            GridResultsModel<ClientSupportRequestSearchResultViewModel> model = new GridResultsModel<ClientSupportRequestSearchResultViewModel>(filterModel.Page, filterModel.Rows, resultsDto.TotalRecords)
            {
                Results = Mapper.Map<List<ClientSupportRequestSearchResultViewModel>>(resultsDto.ClientSupportRequests)
            };

            return this.Json(model);
        }

        [VpAuthorize(Roles = VisitPayRoleStrings.Miscellaneous.VpSupportTicketAdmin)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Common.Web.Attributes.SessionState(SessionStateBehavior.Required)]
        public async Task<ActionResult> ClientAdminSupportRequestsExport(GridSearchModel<ClientSupportRequestFilterViewModel> filterModel)
        {
            ClientSupportRequestFilterDto filter = Mapper.Map<ClientSupportRequestFilterDto>(filterModel);

            byte[] bytes = await this._clientSupportRequestApplicationService.Value.ExportClientAdminSupportRequestsAsync(filter, this.User.Identity.GetUserName());
            this.SetTempData(ClientAdminSupportRequestsExportKey, bytes);

            return this.Json(new ResultMessage(true, this.Url.Action("ClientAdminSupportRequestsExportDownload")));
        }

        [VpAuthorize(Roles = VisitPayRoleStrings.Miscellaneous.VpSupportTicketAdmin)]
        [HttpGet]
        public ActionResult ClientAdminSupportRequestsExportDownload()
        {
            object tempData = this.TempData[ClientAdminSupportRequestsExportKey];
            if (tempData != null)
            {
                this.WriteExcelInline((byte[])tempData, "SupportTickets.xlsx");
            }

            return this.Json(new { }, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region client user grid

        [VpAuthorize(Roles = VisitPayRoleStrings.Miscellaneous.VpSupportTickets)]
        [HttpGet]
        public ActionResult ClientUserSupportRequests()
        {
            ClientSupportRequestFilterViewModel model = new ClientSupportRequestFilterViewModel
            {
                DaysOffset = -30
            };

            model.DayOffsets.Insert(0, new SelectListItem { Text = "Previous Week", Value = "-7" });
            model.DayOffsets.Insert(1, new SelectListItem { Text = "Previous 2 Weeks", Value = "-14" });

            return this.View(model);
        }

        [VpAuthorize(Roles = VisitPayRoleStrings.Miscellaneous.VpSupportTickets)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult ClientUserSupportRequests(GridSearchModel<ClientSupportRequestFilterViewModel> filterModel)
        {
            ClientSupportRequestFilterDto filterDto = Mapper.Map<ClientSupportRequestFilterDto>(filterModel);
            filterDto.CreatedByVisitPayUserId = this.CurrentUserId;

            ClientSupportRequestResultsDto resultsDto = this._clientSupportRequestApplicationService.Value.GetClientSupportRequests(filterDto);

            GridResultsModel<ClientSupportRequestSearchResultViewModel> model = new GridResultsModel<ClientSupportRequestSearchResultViewModel>(filterModel.Page, filterModel.Rows, resultsDto.TotalRecords)
            {
                Results = Mapper.Map<List<ClientSupportRequestSearchResultViewModel>>(resultsDto.ClientSupportRequests)
            };

            return this.Json(model);
        }

        #endregion

        #region create

        [VpAuthorize(Roles = VisitPayRoleStrings.Miscellaneous.VpSupportTickets + "," + VisitPayRoleStrings.Miscellaneous.VpSupportTicketAdmin)]
        [HttpGet]
        public PartialViewResult ClientSupportRequestCreate(bool? onBehalfOfClient)
        {
            VisitPayUserDto visitPayUser = this._visitPayUserApplicationService.Value.FindById(this.CurrentUserIdString);
            IList<VisitPayUserDto> visitPayUsers = this._visitPayUserApplicationService.Value.GetVisitPayClientUsersByRole(
                roles: new List<int> { (int)VisitPayRoleEnum.VpSupportTicketAdmin, (int)VisitPayRoleEnum.VpSupportTickets }, 
                isDisabled: null, 
                excludedRoles: null);
            
            ClientSupportRequestCreateViewModel model = new ClientSupportRequestCreateViewModel
            {
                IsCreatedOnBehalfOfClient = onBehalfOfClient ?? false,
                AdminVisitPayUsers = visitPayUsers.Select(x => new SelectListItem { Text = x.DisplayFullName, Value = x.VisitPayUserId.ToString() }).ToList(),
                ClientUserFullName = visitPayUser.DisplayFirstNameLastName,
                ClientUserEmail = visitPayUser.Email
            };

            return this.PartialView("_ClientSupportRequestCreate", model);
        }

        [VpAuthorize(Roles = VisitPayRoleStrings.Miscellaneous.VpSupportTickets + "," + VisitPayRoleStrings.Miscellaneous.VpSupportTicketAdmin)]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult ClientSupportRequestCreate(ClientSupportRequestCreateViewModel model)
        {
            int loggedInUser = this.CurrentUserId;
            int requestorUserId = (model.IsCreatedOnBehalfOfClient) ? model.RequestorUserId : loggedInUser;

            CreateClientSupportRequestParameterDto createClientSupportRequestParameter = new CreateClientSupportRequestParameterDto
            {
                ClientSupportTopicId = model.ClientSupportTopicId,
                ContactPhone = model.ContactPhone,
                MessageBody = model.MessageBody,
                IsCreatedOnBehalfOfClient = model.IsCreatedOnBehalfOfClient,
                IvinciAdminUserId = (model.IsCreatedOnBehalfOfClient) ? loggedInUser : -1,
                RequestorUserId = requestorUserId,
                CreatedOnBefalfOfClientInternalNote = model.CreatedOnBefalfOfClientInternalNote,
                ReferenceGuarantorSupportRequestId = model.ReferenceGuarantorSupportRequestId
            };

            ClientSupportRequestDto clientSupportRequestDto = this._clientSupportRequestApplicationService.Value.CreateClientSupportRequest(createClientSupportRequestParameter);

            return this.Json(new
            {
                ClientSupportRequestId = clientSupportRequestDto.ClientSupportRequestId,
                ClientSupportRequestMessageId = clientSupportRequestDto.ClientSupportRequestMessages.OrderBy(x => x.InsertDate).Select(x => x.ClientSupportRequestMessageId).FirstOrDefault()
            });
        }

        #endregion

        #region edit

        [HttpGet]
        public PartialViewResult ClientSupportRequest(bool isAdmin, int clientSupportRequestId, bool markAsRead)
        {
            if (markAsRead)
            {
                this._clientSupportRequestApplicationService.Value.SetMessageRead(clientSupportRequestId, isAdmin ? ClientSupportRequestMessageTypeEnum.FromClient : ClientSupportRequestMessageTypeEnum.ToClient);
            }

            return this.PartialView("_ClientSupportRequest");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult ClientSupportRequestData(bool isAdmin, int clientSupportRequestId)
        {
            return this.Json(this.GetSupportRequestViewModel(isAdmin, clientSupportRequestId));
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult ClientSupportRequestSaveDetails(int clientSupportRequestId, int? assignedToVisitPayUserId, int? targetVpGuarantorId, string jiraId)
        {
            IList<string> errors = this._clientSupportRequestApplicationService.Value.SaveDetails(clientSupportRequestId, assignedToVisitPayUserId, targetVpGuarantorId, jiraId, this.CurrentUserId);
            if (errors.Any())
            {
                return this.Json(errors);
            }

            return this.Json("");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult ClientSupportRequestSaveMessage(int clientSupportRequestId, string messageBody, ClientSupportRequestMessageTypeEnum messageType)
        {
            ClientSupportRequestMessageDto clientSupportRequestMessageDto = this._clientSupportRequestApplicationService.Value.SaveMessage(clientSupportRequestId, messageBody, messageType, this.CurrentUserId);

            return this.Json(new
            {
                ClientSupportRequestMessageId = clientSupportRequestMessageDto.ClientSupportRequestMessageId
            });
        }

        [VpAuthorize(Roles = VisitPayRoleStrings.Miscellaneous.VpSupportTicketAdmin)]
        [HttpGet]
        public PartialViewResult ClientSupportRequestReply()
        {
            return this.PartialView("_ClientSupportRequestReply");
        }

        [HttpGet]
        public ActionResult Print(bool i, int clientSupportRequestId)
        {
            return this.View("ClientPrintSupportRequest", new ClientSupportRequestPrintViewModel
            {
                IsAdmin = i,
                ClientSupportRequest = this.GetSupportRequestViewModel(i, clientSupportRequestId)
            });
        }

        private ClientSupportRequestViewModel GetSupportRequestViewModel(bool isAdmin, int clientSupportRequestId)
        {
            ClientSupportRequestDto clientSupportRequestDto = this._clientSupportRequestApplicationService.Value.GetClientSupportRequest(clientSupportRequestId);

            ClientSupportRequestViewModel model = Mapper.Map<ClientSupportRequestViewModel>(clientSupportRequestDto);

            if (isAdmin)
            {
                model.Attachments = Mapper.Map<IList<ClientSupportRequestMessageAttachmentViewModel>>(clientSupportRequestDto.ClientSupportRequestMessages.SelectMany(x => x.ActiveClientSupportRequestMessageAttachments).OrderBy(x => x.AttachmentFileName));

                IDictionary<int, string> vpSupportAdminUsers = this._clientSupportRequestApplicationService.Value.GetAssignableUsers(false, clientSupportRequestDto.AssignedVisitPayUser?.VisitPayUserId);

                model.AdminVisitPayUsers = vpSupportAdminUsers.Select(x => new SelectListItem { Text = x.Value, Value = x.Key.ToString() }).ToList();
            }
            else
            {
                model.Attachments = Mapper.Map<IList<ClientSupportRequestMessageAttachmentViewModel>>(clientSupportRequestDto.MessagesClient.SelectMany(x => x.ActiveClientSupportRequestMessageAttachments).OrderBy(x => x.AttachmentFileName));

                // VP-3139 - show open/close notes to the client user
                IEnumerable<ClientSupportRequestMessageViewModel> applicableMessages = model.MessagesInternal.Where(x => x.ClientSupportRequestMessageType == ClientSupportRequestMessageTypeEnum.OpenMessage ||
                                                                                                                         x.ClientSupportRequestMessageType == ClientSupportRequestMessageTypeEnum.CloseMessage);
                model.MessagesClient.AddRange(applicableMessages);
            }

            Action<ClientSupportRequestMessageViewModel> setUser = x =>
            {
                if (x.CreatedByVisitPayUserId == this.CurrentUserId)
                {
                    x.CreatedByVisitPayUserName = "Me";
                }
            };

            model.MessagesClient.ToList().ForEach(setUser);
            model.MessagesInternal.ToList().ForEach(setUser);
            model.IsAdmin = isAdmin;

            return model;
        }

        #endregion

        #region actions: close/reopen

        [HttpGet]
        public PartialViewResult ClientSupportRequestAction(int clientSupportRequestId)
        {
            ClientSupportRequestDto clientSupportRequestDto = this._clientSupportRequestApplicationService.Value.GetClientSupportRequest(clientSupportRequestId);
            ClientSupportRequestActionViewModel model = new ClientSupportRequestActionViewModel
            {
                ClientSupportRequestId = clientSupportRequestDto.ClientSupportRequestId,
                ClientSupportRequestDisplayId = clientSupportRequestDto.ClientSupportRequestDisplayId,
                ClientSupportRequestStatus = clientSupportRequestDto.ClientSupportRequestStatus == ClientSupportRequestStatusEnum.Open ? ClientSupportRequestStatusEnum.Closed : ClientSupportRequestStatusEnum.Open
            };

            return this.PartialView("_ClientSupportRequestAction", model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult ClientSupportRequestAction(ClientSupportRequestActionViewModel model)
        {
            this._clientSupportRequestApplicationService.Value.SetSupportRequestStatus(model.ClientSupportRequestId, this.CurrentUserId, model.ClientSupportRequestStatus, model.Message);

            return this.Json(model.ClientSupportRequestStatus);
        }

        #endregion

        #region attachments

        [HttpGet]
        public PartialViewResult ClientSupportRequestAttachments()
        {
            return this.PartialView("_ClientSupportRequestAttachments");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Common.Web.Attributes.SessionState(SessionStateBehavior.Required)]
        public JsonResult PrepareDownload(int clientSupportRequestId, int? clientSupportRequestMessageId, int? clientSupportRequestMessageAttachmentId, bool isAdmin)
        {
            AttachmentDownloadDto attachmentDownloadDto = this._clientSupportRequestApplicationService.Value.PrepareDownload(clientSupportRequestId, clientSupportRequestMessageId, clientSupportRequestMessageAttachmentId, isAdmin);

            if (attachmentDownloadDto == null)
            {
                return this.Json(new ResultMessage(false, "No attachments available for download."));
            }

            this.SetTempData(SupportRequestAttachmentDownloadKey, attachmentDownloadDto);

            return this.Json(new ResultMessage(true, this.Url.Action("DownloadAttachment", "ClientSupport")));
        }

        [HttpGet]
        public ActionResult DownloadAttachment()
        {
            AttachmentDownloadDto attachmentDownloadDto = this.TempData[SupportRequestAttachmentDownloadKey] as AttachmentDownloadDto;
            return attachmentDownloadDto != null ? this.File(attachmentDownloadDto.Bytes, attachmentDownloadDto.MimeType, attachmentDownloadDto.FileName) : null;
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult UploadAttachment()
        {
            HttpRequestBase httpRequest = this.HttpContext.Request;

            IList<string> acceptedTypes = new List<string>
            {
                "application/msword", // .doc
                "application/vnd.openxmlformats-officedocument.wordprocessingml.document", // .docx
                "application/vnd.ms-excel", // .xls
                "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", // .xlsx
                "application/vnd.ms-powerpoint", // .ppt
                "application/vnd.openxmlformats-officedocument.presentationml.presentation", // .pptx
                "application/pdf",
                "application/x-zip-compressed",
                "image/gif",
                "image/jpg",
                "image/jpeg",
                "image/png",
                "text/plain"
            };

            int clientSupportRequestId;
            int clientSupportRequestMessageId;

            if (!int.TryParse(httpRequest.Form["ClientSupportRequestId"], out clientSupportRequestId) ||
                !int.TryParse(httpRequest.Form["ClientSupportRequestMessageId"], out clientSupportRequestMessageId) ||
                httpRequest.Files.Count < 1 ||
                httpRequest.Files[0] == null ||
                !acceptedTypes.Contains(httpRequest.Files[0].ContentType))
            {
                return this.Json(false);
            }

            HttpPostedFileBase postedFile = httpRequest.Files[0];

            byte[] bytes;
            Stream stream = postedFile.InputStream;
            using (BinaryReader reader = new BinaryReader(stream))
            {
                bytes = reader.ReadBytes((int)stream.Length);
            }

            if (bytes.Length <= 0 || bytes.Length > 10000000)
            {
                return this.Json(false);
            }

            ClientSupportRequestMessageAttachmentDto attachmentDto = new ClientSupportRequestMessageAttachmentDto
            {
                AttachmentFileName = postedFile.FileName,
                FileSize = bytes.Length,
                MimeType = postedFile.ContentType,
                QuarantinedBytes = bytes
            };

            this._clientSupportRequestApplicationService.Value.SaveAttachment(attachmentDto, clientSupportRequestMessageId, clientSupportRequestId);

            return this.Json(true);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [VpAuthorize(Roles = VisitPayRoleStrings.Miscellaneous.IvhAdmin)]
        public JsonResult DeleteAttachment(int clientSupportRequestId, int clientSupportRequestMessageAttachmentId, int visitPayUserId)
        {
            this._clientSupportRequestApplicationService.Value.DeleteAttachment(clientSupportRequestId, clientSupportRequestMessageAttachmentId);

            this._visitPayUserJournalEventApplicationService.Value.LogDeleteAttachmentSupportTicket(this.CurrentUserId, visitPayUserId, clientSupportRequestId, Mapper.Map<JournalEventHttpContextDto>(System.Web.HttpContext.Current));

            return this.Json(true);
        }

        #endregion

        #region topics

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult ClientSupportTopics(bool? withTemplates)
        {
            IList<ClientSupportTopicDto> topics = this._clientSupportRequestApplicationService.Value.GetClientSupportTopics(withTemplates.GetValueOrDefault(false));
            List<ClientSupportTopicDto> parentTopics = topics.Where(x => x.ParentClientSupportTopic == null).Distinct().OrderBy(x => x.SortOrder).ToList();

            IList<ClientSupportTopicViewModel> model = new List<ClientSupportTopicViewModel>();
            parentTopics.ForEach(topic =>
            {
                ClientSupportTopicViewModel m = Mapper.Map<ClientSupportTopicViewModel>(topic);
                m.Topics = Mapper.Map<IList<ClientSupportTopicViewModel>>(topics.Where(x => x.ParentClientSupportTopic != null && x.ParentClientSupportTopic.ClientSupportTopicId == topic.ClientSupportTopicId));
                if (m.Topics.Any())
                {
                    model.Add(m);
                }
            });

            return this.Json(model);
        }

        #endregion
    }
}
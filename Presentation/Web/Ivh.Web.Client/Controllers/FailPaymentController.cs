﻿using System.Web.Mvc;

namespace Ivh.Web.Client.Controllers
{
    using Application.Core.Common.Interfaces;
    using Common.Session;
    using Common.Web.Controllers;
    
    public class FailPaymentController : BaseFailPaymentController
    {
        public FailPaymentController(ISessionFacade sessionFacade, IClientApplicationService clientApplicationService) 
            : base(sessionFacade, clientApplicationService)
        {
        }

        public ActionResult Index(bool? failPayment)
        {
            return this.View(base.Index(failPayment));
        }
    }
}
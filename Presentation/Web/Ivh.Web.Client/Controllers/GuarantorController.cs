﻿namespace Ivh.Web.Client.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using System.Web;
    using System.Web.Mvc;
    using Application.Content.Common.Dtos;
    using Application.Content.Common.Interfaces;
    using Application.Core.Common.Dtos;
    using Application.Core.Common.Interfaces;
    using Application.Email.Common.Dtos;
    using Application.Email.Common.Interfaces;
    using Application.User.Common.Dtos;
    using AutoMapper;
    using Common.EventJournal;
    using Common.VisitPay.Enums;
    using Common.VisitPay.Strings;
    using Common.Web.Extensions;
    using Common.Web.Filters;
    using Common.Web.Interfaces;
    using Common.Web.Models;
    using Common.Web.Models.Guarantor;
    using Common.Web.Models.Shared;
    using Microsoft.AspNet.Identity;
    using Models.Manage;

    public class GuarantorController : BaseController
    {
        private readonly Lazy<ICommunicationApplicationService> _communicationApplciationService;
        private readonly Lazy<IContentApplicationService> _contentApplicationService;
        private readonly Lazy<IVisitPayUserApplicationService> _visitPayUserApplicationService;

        public GuarantorController(
            Lazy<IBaseControllerService> baseControllerService,
            Lazy<IContentApplicationService> contentApplicationService,
            Lazy<ICommunicationApplicationService> communicationApplicationService,
            Lazy<IVisitPayUserApplicationService> visitPayUserApplicationService
        )
            : base(baseControllerService)
        {
            this._visitPayUserApplicationService = visitPayUserApplicationService;
            this._contentApplicationService = contentApplicationService;
            this._communicationApplciationService = communicationApplicationService;
        }

        [HttpGet]
        public async Task<ActionResult> PhoneTextSettings(int vpGuarantorId)
        {
            VisitPayUserDto visitPayUser = this._visitPayUserApplicationService.Value.FindByGuarantorId(vpGuarantorId);

            PhoneTextInformationViewModel model = await this.CreatePhoneTextInformationViewModel(visitPayUser.VisitPayUserId.ToString());
            return this.View(model);
        }

        [HttpGet]
        [OverrideAuthorization]
        public async Task<ActionResult> PhoneTextInformation(string visitPayUserId)
        {
            PhoneTextInformationViewModel model = await this.CreatePhoneTextInformationViewModel(visitPayUserId);
            return this.PartialView("_PhoneTextInformation", model);
        }

        private async Task<PhoneTextInformationViewModel> CreatePhoneTextInformationViewModel(string visitPayUserId)
        {
            VisitPayUserDto visitPayUserDto = this._visitPayUserApplicationService.Value.FindById(visitPayUserId);
            SmsPhoneTypeEnum? smsPhoneType = this._visitPayUserApplicationService.Value.GetSmsAcknowledgement(visitPayUserId);
            int cmsVersionId = (await this._contentApplicationService.Value.GetCurrentVersionAsync(CmsRegionEnum.SmsAcknowledgement, false)).CmsVersionId;

            IList<CommunicationGroupSelectionViewModel> preferences = null;
            if (this.IsFeatureEnabled(VisitPayFeatureEnum.Sms))
            {
                IList<CommunicationGroupDto> communicationGroupDtos = await this._communicationApplciationService.Value.GetCommunicationGroupsAsync();
                preferences = Mapper.Map<IList<CommunicationGroupDto>, IList<CommunicationGroupSelectionViewModel>>(communicationGroupDtos);
                if (smsPhoneType.HasValue)
                {
                    foreach (VisitPayUserCommunicationPreferenceDto communicationPreference in this._communicationApplciationService.Value.CommunicationPreferences(visitPayUserDto.VisitPayUserId))
                    {
                        preferences.First(g => g.CommunicationGroupId == communicationPreference.CommunicationGroupId).Enabled = true;
                    }
                }
            }
            return new PhoneTextInformationViewModel
            {
                VisitPayUserId = visitPayUserId,
                PhoneNumber = visitPayUserDto.PhoneNumber,
                PhoneNumberType = visitPayUserDto.PhoneNumberType,
                PhoneNumberTypeIsMobile = visitPayUserDto.PhoneNumberTypeIsMobile,
                PhoneNumberSecondary = visitPayUserDto.PhoneNumberSecondary,
                PhoneNumberSecondaryType = visitPayUserDto.PhoneNumberSecondaryType,
                PhoneNumberSecondaryTypeIsMobile = visitPayUserDto.PhoneNumberSecondaryTypeIsMobile,
                SmsModuleEnabled = this.IsFeatureEnabled(VisitPayFeatureEnum.Sms),
                SmsEnabledFor = smsPhoneType,
                CmsVersionId = cmsVersionId,
                Preferences = preferences
            };
        }

        private EditPhoneViewModel CreateEditPhoneViewModel(SmsPhoneTypeEnum phoneType, string visitPayUserId)
        {
            VisitPayUserDto visitPayUserDto = this._visitPayUserApplicationService.Value.FindById(visitPayUserId);
            SmsPhoneTypeEnum? smsPhoneType = this._visitPayUserApplicationService.Value.GetSmsAcknowledgement(visitPayUserId);

            EditPhoneViewModel model = new EditPhoneViewModel
            {
                VisitPayUserId = visitPayUserId,
                PhoneNumber = phoneType == SmsPhoneTypeEnum.Primary ? visitPayUserDto.PhoneNumber : visitPayUserDto.PhoneNumberSecondary,
                PhoneNumberType = phoneType == SmsPhoneTypeEnum.Primary ? visitPayUserDto.PhoneNumberType : visitPayUserDto.PhoneNumberSecondaryType,
                IsSmsEnabled = smsPhoneType != null && smsPhoneType.Value == phoneType,
                SmsPhoneType = phoneType
            };

            return model;
        }
        
        [HttpGet]
        [RequireFeature(VisitPayFeatureEnum.OfflineVisitPay)]
        public ActionResult UserProfileEdit(string id)
        {
            VisitPayUserDto visitPayUserDto = this._visitPayUserApplicationService.Value.FindById(id);

            UpgradeGuarantorViewModel model = new UpgradeGuarantorViewModel
            {
                VisitPayUserId = visitPayUserDto.VisitPayUserId,
                AddressStreet1 = visitPayUserDto.AddressStreet1,
                AddressStreet2 = visitPayUserDto.AddressStreet2,
                City = visitPayUserDto.City,
                State = visitPayUserDto.State,
                Zip = visitPayUserDto.Zip,
                MailingAddressStreet1 = visitPayUserDto.MailingAddressStreet1,
                MailingAddressStreet2 = visitPayUserDto.MailingAddressStreet2,
                MailingCity = visitPayUserDto.MailingCity,
                MailingState = visitPayUserDto.MailingState,
                MailingZip = visitPayUserDto.MailingZip,
                Email = visitPayUserDto.Email,
                EmailConfirm = visitPayUserDto.Email,
                FirstName = visitPayUserDto.FirstName,
                LastName = visitPayUserDto.LastName,
                MiddleName = visitPayUserDto.MiddleName,
                DateOfBirth = visitPayUserDto.DateOfBirth?.ToString(Format.DateFormat) ?? string.Empty,
                Ssn4 = visitPayUserDto.SSN4.HasValue ? visitPayUserDto.SSN4.Value.ToString("D4") : ""
            };

            return this.View("UpgradeAccount", model);
        }
        
        [HttpPost]
        [ValidateAntiForgeryToken]
        [RequireFeature(VisitPayFeatureEnum.OfflineVisitPay)]
        public ActionResult UpgradeAccount(UpgradeGuarantorViewModel model)
        {
            if (!this.ModelState.IsValid)
                return this.View(model);

            bool isUsernameAvailable = this._visitPayUserApplicationService.Value.IsUsernameAvailable(model.Username, model.VisitPayUserId);
            if (!isUsernameAvailable)
            {
                this.ModelState.AddModelError("Error", "Username already exists for another user.");
                return this.View(model);
            }

            VisitPayUserDto visitPayUserDto = this._visitPayUserApplicationService.Value.FindById(model.VisitPayUserId.ToString());
            if (visitPayUserDto != null)
            {               
                visitPayUserDto.AddressStreet1 = model.AddressStreet1;
                visitPayUserDto.AddressStreet2 = model.AddressStreet2;
                visitPayUserDto.City = model.City;
                visitPayUserDto.State = model.State;
                visitPayUserDto.Zip = model.Zip;
                visitPayUserDto.MailingAddressStreet1 = model.MailingAddressStreet1;
                visitPayUserDto.MailingAddressStreet2 = model.MailingAddressStreet2;
                visitPayUserDto.MailingCity = model.MailingCity;
                visitPayUserDto.MailingState = model.MailingState;
                visitPayUserDto.MailingZip = model.MailingZip;
                visitPayUserDto.Email = model.Email;
                visitPayUserDto.UserName = model.Username;
                visitPayUserDto.FirstName = model.FirstName;
                visitPayUserDto.LastName = model.LastName;
                visitPayUserDto.MiddleName = model.MiddleName;
                visitPayUserDto.PhoneNumber = model.PhoneNumber;
                visitPayUserDto.PhoneNumberType = model.PhoneNumberType;
                visitPayUserDto.PhoneNumberSecondary = model.PhoneNumberSecondary;
                visitPayUserDto.PhoneNumberSecondaryType = model.PhoneNumberSecondaryType;
              
                IdentityResult identityResult = this._visitPayUserApplicationService.Value.UpdateUserAndGuarantorType(visitPayUserDto, GuarantorTypeEnum.Online);

                if (identityResult.Succeeded)
                {
                    this._visitPayUserApplicationService.Value.RequestPassword(visitPayUserDto.VisitPayUserId, this.ClientDto.GuarantorPasswordExpLimitInDays);
                    int vpGuarantorId = this._visitPayUserApplicationService.Value.GetGuarantorIdFromVisitPayUserId(visitPayUserDto.VisitPayUserId);
                    return this.RedirectToAction("GuarantorAccount", "Search", new {id = vpGuarantorId});
                }
            }

            this.ModelState.AddModelError("Error", this.GenericErrorMessage);
            return this.View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult AddSmsPreference(int id, string visitPayUserId)
        {
            VisitPayUserDto visitPayUserDto = this._visitPayUserApplicationService.Value.FindById(visitPayUserId);
            this._communicationApplciationService.Value.AddPreference(CommunicationMethodEnum.Sms, visitPayUserDto.VisitPayUserId, id, this.GetVpGuarantorId(visitPayUserDto.VisitPayUserId), this.User.Identity.CurrentUserId(), Mapper.Map<JournalEventHttpContextDto>(System.Web.HttpContext.Current));
            return this.Json(true);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult RemoveSmsPreference(int id, string visitPayUserId)
        {
            VisitPayUserDto visitPayUserDto = this._visitPayUserApplicationService.Value.FindById(visitPayUserId);
            this._communicationApplciationService.Value.RemovePreference(CommunicationMethodEnum.Sms, visitPayUserDto.VisitPayUserId, id, this.GetVpGuarantorId(visitPayUserDto.VisitPayUserId), this.User.Identity.CurrentUserId(), Mapper.Map<JournalEventHttpContextDto>(System.Web.HttpContext.Current));
            return this.Json(true);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<JsonResult> GetDisableText(string visitPayUserId)
        {
            VisitPayUserDto visitPayUserDto = this._visitPayUserApplicationService.Value.FindById(visitPayUserId);
            SmsPhoneTypeEnum? smsPhoneType = this._visitPayUserApplicationService.Value.GetSmsAcknowledgement(visitPayUserId);

            Dictionary<string, string> replacementValues = new Dictionary<string, string>
            {
                {"[[PhoneNumber]]", !smsPhoneType.HasValue || smsPhoneType.Value == SmsPhoneTypeEnum.Primary ? visitPayUserDto.PhoneNumber : visitPayUserDto.PhoneNumberSecondary}
            };
            CmsVersionDto version = await this._contentApplicationService.Value.GetCurrentVersionAsync(CmsRegionEnum.SmsDisableMessage, true, replacementValues);
            return this.Json(Mapper.Map<CmsVersionDto, CmsViewModel>(version));
        }

        [HttpGet]
        public ActionResult PhoneEdit(SmsPhoneTypeEnum phoneType, string visitPayUserId)
        {
            return this.PartialView("_EditPhone", this.CreateEditPhoneViewModel(phoneType, visitPayUserId));
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult PhoneEdit(EditPhoneViewModel model, string visitPayUserId)
        {
            if (!this.ModelState.IsValid)
                return this.Json(new ResultMessage(false, this.GenericErrorMessage));

            bool isPrimary = model.SmsPhoneType == SmsPhoneTypeEnum.Primary;

            IdentityResult identityResult = this._visitPayUserApplicationService.Value.UpdatePhone(
                visitPayUserId: model.VisitPayUserId, 
                phoneNumber: model.PhoneNumber, 
                phoneNumberType: model.PhoneNumberType, 
                isPrimary: isPrimary,
                context: Mapper.Map<HttpContext, JournalEventHttpContextDto>(System.Web.HttpContext.Current),
                clientVisitPayUserId: this.User.Identity.CurrentUserId().ToString());
            return this.Json(new ResultMessage(identityResult.Succeeded, identityResult.Succeeded ? $"This Guarantor's {(isPrimary ? "primary" : "secondary")} phone number has been updated successfully." : this.GenericErrorMessage));
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public void DisableSms(string visitPayUserId)
        {
            this._visitPayUserApplicationService.Value.RemoveSmsAcknowledgement(
                visitPayUserId: visitPayUserId,
                fromSmsReply: false,
                clientVisitPayUserId: this.User.Identity.GetUserId(),
                context: Mapper.Map<HttpContext, JournalEventHttpContextDto>(System.Web.HttpContext.Current)
                );
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult SecondaryPhoneDelete(string visitPayUserId)
        {
            IdentityResult identityResult = this._visitPayUserApplicationService.Value.SecondaryPhoneDelete(
                visitPayUserId: visitPayUserId,
                context: Mapper.Map<HttpContext, JournalEventHttpContextDto>(System.Web.HttpContext.Current),
                clientVisitPayUserId: this.User.Identity.CurrentUserId().ToString());
            return this.Json(new ResultMessage(identityResult.Succeeded, identityResult.Succeeded ? "This Guarantor's secondary phone number has been deleted successfully." : this.GenericErrorMessage));
        }
    }
}
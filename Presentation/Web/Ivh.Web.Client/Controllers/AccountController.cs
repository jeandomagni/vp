﻿namespace Ivh.Web.Client.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.Linq;
    using System.Threading.Tasks;
    using System.Web;
    using System.Web.Helpers;
    using System.Web.Mvc;
    using System.Web.Routing;
    using System.Web.SessionState;
    using Application.Base.Common.Dtos;
    using Application.Core.Common.Dtos;
    using Application.Core.Common.Interfaces;
    using Application.User.Common.Dtos;
    using AutoMapper;
    using Common.Base.Enums;
    using Common.Base.Utilities.Extensions;
    using Common.EventJournal;
    using Common.VisitPay.Enums;
    using Common.VisitPay.Strings;
    using Common.Web.Interfaces;
    using Common.Web.Models;
    using Common.Web.Models.Account;
    using Microsoft.AspNet.Identity;
    using Microsoft.Owin.Security;
    using Models.Account;

    [Authorize(Roles = VisitPayRoleStrings.System.Client + "," + VisitPayRoleStrings.Admin.Administrator)]
    public class AccountController : BaseController
    {
        private readonly Lazy<IVisitPayUserApplicationService> _visitPayUserApplicationService;
        private readonly Lazy<ISecurityQuestionApplicationService> _securityQuestionApplicationService;

        public AccountController(
            Lazy<IBaseControllerService> baseControllerService,
            Lazy<IVisitPayUserApplicationService> visitPayUserApplicationService,
            Lazy<ISecurityQuestionApplicationService> securityQuestionApplicationService)
            : base(baseControllerService)
        {
            this._visitPayUserApplicationService = visitPayUserApplicationService;
            this._securityQuestionApplicationService = securityQuestionApplicationService;
        }

        const string IsPasswordExpiredKey = "IsPasswordExpired";
        const string UsernameKey = "Username";
        const string PasswordKey = "Password";
        const string TempPasswordKey = "TempPassword";
        const string IsSecureCodeRequiredKey = "IsSecureCodeRequired";
        const string IsSecurityQuestionRequiredKey = "IsSecurityQuestionRequired";
        const string SecurityQuestionIdKey = "SecurityQuestionId";
        const string VisitPayUserIdKey = "VisitPayUserId";

        //
        // GET: /Account/Login
        [AllowAnonymous]
        public ActionResult Index(string returnUrl, int? sm)
        {
            if (this.User.Identity.IsAuthenticated)
            {
                return this.KillExistingSession(returnUrl);
            }

            LoginViewModel model = new LoginViewModel { ReturnUrl = returnUrl };

            if (sm.HasValue)
            {
                this.ModelState.AddModelError("", "A session error has occurred, please try again.");
            }

            return !this.IsBrowserSupported ? this.View("~/Views/Shared/BrowserNotSupported.cshtml") : this.View(model);
        }

        //
        // POST: /Account/Login
        [HttpPost]
        [AllowAnonymous]
        [Common.Web.Attributes.SessionState(SessionStateBehavior.Required)]
        public async Task<ActionResult> Index(LoginViewModel model)
        {
            HttpContext httpContext = System.Web.HttpContext.Current;
            if (this.User.Identity.IsAuthenticated)
            {
                return this.KillExistingSession(model.ReturnUrl, true);
            }

            AntiForgery.Validate();

            if (!this.ModelState.IsValid)
            {
                return this.View(model);
            }

            SignInStatusExEnum result = await this._visitPayUserApplicationService.Value.PasswordSignInAsync(Mapper.Map<HttpContextDto>(httpContext), model.Username, model.Password, false, true, VisitPayRoleStrings.System.Client);
            await this.BruteForceDelayAsync(httpContext.Request.UserHostAddress, () => result == SignInStatusExEnum.Failure, () => result == SignInStatusExEnum.Success);
            VisitPayUserDto visitPayUserDto;
            switch (result)
            {
                case SignInStatusExEnum.Success:
                    return this.RedirectToLocal(model.ReturnUrl);
                case SignInStatusExEnum.LockedOut:
                    visitPayUserDto = this._visitPayUserApplicationService.Value.FindByName(model.Username);
                    return this.View("Lockout", visitPayUserDto.IsDisabled);
                case SignInStatusExEnum.RequiresVerification:
                    visitPayUserDto = this._visitPayUserApplicationService.Value.FindByName(model.Username);
                    this.SetTempData(IsPasswordExpiredKey, this._visitPayUserApplicationService.Value.IsPasswordExpired(visitPayUserDto));
                    this.SetTempData(UsernameKey, model.Username);
                    this.SetTempData(PasswordKey, model.Password);
                    this.SetTempData(IsSecureCodeRequiredKey, !string.IsNullOrEmpty(visitPayUserDto.SecurityValidationValue));
                    this.SetTempData(IsSecurityQuestionRequiredKey, (visitPayUserDto.SecurityQuestionAnswers.Count > 0));
                    return this.RedirectToAction("ResetPassword");
                case SignInStatusExEnum.PasswordExpired:
                    visitPayUserDto = this._visitPayUserApplicationService.Value.FindByName(model.Username);
                    this.SetTempData(UsernameKey, model.Username);
                    this.SetTempData(TempPasswordKey, model.Password);
                    this.SetTempData(IsPasswordExpiredKey, true);
                    this.SetTempData(IsSecureCodeRequiredKey, false);
                    this.SetTempData(IsSecurityQuestionRequiredKey, (visitPayUserDto.SecurityQuestionAnswers.Count > 0));
                    return this.RedirectToAction("ResetPassword");
                case SignInStatusExEnum.RequiresSecurityQuestions:
                    this.SetTempData(UsernameKey, model.Username);
                    return this.RedirectToAction("SecuritySetup");
                case SignInStatusExEnum.AccountClosed:
                    this.ModelState.AddModelError("", string.Format("This account has been canceled. Please call {0} for assistance.", this.ClientDto.SupportPhone));
                    return this.View(model);
                default:
                    this.ModelState.AddModelError("", "Invalid login attempt.");
                    return this.View(model);
            }
        }

        [AllowAnonymous]
        public ActionResult ForgotPassword()
        {
            return this.View();
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        [Common.Web.Attributes.SessionState(SessionStateBehavior.Required)]
        public async Task<JsonResult> ForgotPassword(ForgotPasswordViewModel model)
        {
            if (!this.ModelState.IsValid)
            {
                return this.Json(new ResultMessage(false, this.GenericErrorMessage));
            }

            VisitPayUserDto user = await this._visitPayUserApplicationService.Value.FindByNameAsync(model.UserName).ConfigureAwait(true);

            if (user == null)
            {
                this._visitPayUserApplicationService.Value.LogClientRequestPasswordVerificationFailed(model.UserName, null, this.GetJournalEventHttpContextDto());
                return this.Json(new ResultMessage(true, ""));
            }

            if (user.IsLocked || user.IsDisabled)
            {
                this._visitPayUserApplicationService.Value.LogClientRequestPasswordVerificationFailed(model.UserName, user, this.GetJournalEventHttpContextDto());
                return this.Json(new ResultMessage(false, "The account is locked out."));
            }

            bool result = await this._visitPayUserApplicationService.Value.BeginResetPasswordClientAsync(user.VisitPayUserId, this.ClientDto.ClientTempPasswordExpLimitInHours).ConfigureAwait(true);

            this.SetTempData(VisitPayUserIdKey, user.VisitPayUserId);

            return this.Json(new ResultMessage(true, ""));
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<JsonResult> GetRandomQuestionAsJson(string userName, int? currentQuestionId)
        {
            //var userName = this.GetCurrentUserName();
            SecurityQuestionDto result = await this.GetRandomQuestionAsync(userName, currentQuestionId);
            return this.Json(result);
        }

        /// <summary>
        /// Reset the user's password using a required security code as validation
        /// </summary>
        /// <returns></returns>
        [AllowAnonymous]
        public async Task<ActionResult> ResetPassword()
        {
            string password = this.TempData[TempPasswordKey] as string ?? this.TempData[PasswordKey] as string;
            string queryStringPassword = this.Request.QueryString["p"];
            password = queryStringPassword ?? password;

            bool isSecureCodeRequired = Convert.ToBoolean(this.TempData[IsSecureCodeRequiredKey]);
            bool isSecurityQuestionRequired = Convert.ToBoolean(this.TempData[IsSecurityQuestionRequiredKey]);

            bool hasQueryStringPassword = !string.IsNullOrWhiteSpace(queryStringPassword);

            if (hasQueryStringPassword)
            {
                isSecurityQuestionRequired = true;
            }

            string userName = this.GetCurrentUserName();
            int? currentQuestionId = this.GetCurrentSecurityQuestionId();

            bool isAuthenticated = userName != null && password != null && !hasQueryStringPassword;

            string questionText = default(string);
            int questionId = default(int);

            SecurityQuestionDto question = await this.GetRandomQuestionAsync(userName, currentQuestionId);
            if (question != null)
            {
                questionText = question.Question;
                questionId = question.SecurityQuestionId;
            }


            if (password == null || (userName == null && !hasQueryStringPassword))
            {
                return this.RedirectToAction("Index");
            }

            object isPasswordExpired = this.TempData[IsPasswordExpiredKey];

            return this.View(new ResetPasswordViewModel
            {
                IsPasswordExpired = isPasswordExpired != null && Convert.ToBoolean(isPasswordExpired),
                TempPassword = (string)password,
                UserName = userName,
                SecurityQuestionId = questionId,
                SecurityQuestion = questionText,
                SecurityAnswer = "",
                IsSecureCodeRequired = isSecureCodeRequired,
                IsSecurityQuestionRequired = isSecurityQuestionRequired,
                HideUserName = isAuthenticated
            });
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        [Common.Web.Attributes.SessionState(SessionStateBehavior.Required)]
        public async Task<ActionResult> ResetPassword(ResetPasswordViewModel model)
        {
            // repopulate security question text on model
            if (model.SecurityQuestionId != default(int))
            {
                SecurityQuestionDto securityQuestion = this._securityQuestionApplicationService.Value.GetAllSecurityQuestions().FirstOrDefault(x => x.SecurityQuestionId == model.SecurityQuestionId);
                if (securityQuestion != null)
                {
                    model.SecurityQuestion = securityQuestion.Question;
                }
            }

            // clear security answer
            string securityAnswer = model.SecurityAnswer;
            this.ModelState.SetModelValue("SecurityAnswer", new ValueProviderResult(null, string.Empty, CultureInfo.InvariantCulture));
            model.SecurityAnswer = string.Empty;

            if (!this.ModelState.IsValid)
            {
                return this.View(model);
            }

            VisitPayUserDto user = await this._visitPayUserApplicationService.Value.FindByNameAsync(model.UserName).ConfigureAwait(true);
            if (user == null)
            {
                return this.View(model);
            }

            IdentityResult result = IdentityResult.Success;

            if (user.IsDisabled || user.IsLocked)
            {
                return this.View("Lockout", user.IsDisabled);
            }

            bool isSecureCodeValid = await this._visitPayUserApplicationService.Value.ValidateSecureCodeAsync(user.VisitPayUserId, model.SecureCode, this.GetJournalEventHttpContextDto());
            if (model.IsSecureCodeRequired && !isSecureCodeValid)
            {
                result = new IdentityResult("Your Secure Code is not correct.");
            }

            if (model.IsSecurityQuestionRequired && !this._visitPayUserApplicationService.Value.ForgotPasswordValidateSecurityQuestion(user.VisitPayUserId, model.SecurityQuestionId, securityAnswer, this.GetJournalEventHttpContextDto()))
            {
                result = new IdentityResult("Your answer to the security question is incorrect.");
            }

            // temp password invalid
            bool isPasswordExpired = this._visitPayUserApplicationService.Value.IsPasswordExpired(user);
            if (!isPasswordExpired)
            {
                bool isTempPasswordExpired = this._visitPayUserApplicationService.Value.IsTempPasswordExpired(user);
                bool isTempPasswordValid = this._visitPayUserApplicationService.Value.VerifyTempPassword(user.TempPassword, model.TempPassword.TrimNullSafe());
                if (isTempPasswordExpired || !isTempPasswordValid)
                {
                    await this._visitPayUserApplicationService.Value.LogVerificationResultAsync(model.UserName, user.VisitPayUserId, false, this.GetJournalEventHttpContextDto());
                    this.Logger.Value.Info(() => $"{nameof(AccountController)}::{nameof(ResetPassword)} - attempt to reset with invalid temp password, expired: {isTempPasswordExpired}, valid: {isTempPasswordValid}");
                    result = new IdentityResult("Your password reset token is invalid.");
                }
            }

            if (result.Succeeded)
            {               
                if (model.IsPasswordExpired && string.IsNullOrEmpty(user.TempPassword))
                {
                    // update password
                    result = await this._visitPayUserApplicationService.Value.ChangePasswordClientAsync(user.Id,
                        model.TempPassword,
                        model.NewPassword,
                        this.ClientDto.ClientPasswordExpLimitInDays,
                        this.ClientDto.ClientPasswordReuseLimit).ConfigureAwait(true);
                }
                else
                {
                    // reset password
                    result = await this._visitPayUserApplicationService.Value.CompleteResetPasswordClientAsync(user.Id,
                        model.TempPassword,
                        model.NewPassword,
                        this.ClientDto.ClientPasswordExpLimitInDays,
                        this.ClientDto.ClientPasswordReuseLimit).ConfigureAwait(true);
                }

                // capture error conditions when resetting the password like the password being used too many times
                if (result.Succeeded)
                {
                    return await this.ReLogin(user).ConfigureAwait(true);
                }
            }

            this.AddErrors(result);
            return this.View(model);
        }

        [HttpGet]
        [AllowAnonymous]
        public ActionResult SecuritySetup()
        {
            object username = this.TempData[UsernameKey];

            if (username == null)
            {
                return this.RedirectToAction("Index");
            }

            SecuritySetupViewModel model = new SecuritySetupViewModel
            {
                UserName = username.ToString(),
                SecurityQuestions = new List<SecurityQuestionViewModel>()
            };

            // security questions
            List<SecurityQuestionDto> allQuestions = this._securityQuestionApplicationService.Value.GetAllSecurityQuestions().ToList();
            for (int i = 0; i < this.ClientDto.ClientSecurityQuestionsCount; i++)
            {
                SecurityQuestionViewModel securityQuestionViewModel = new SecurityQuestionViewModel
                {
                    Answer = null,
                    SecurityQuestionId = default(int),
                    SecurityQuestions = new List<SelectListItem>()
                };

                allQuestions.ForEach(x => securityQuestionViewModel.SecurityQuestions.Add(new SelectListItem { Text = x.Question, Value = x.SecurityQuestionId.ToString() }));

                model.SecurityQuestions.Add(securityQuestionViewModel);
            }

            return this.View(model);
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        [Common.Web.Attributes.SessionState(SessionStateBehavior.Required)]
        public async Task<ActionResult> SecuritySetup(SecuritySetupViewModel model)
        {
            if (!this.ModelState.IsValid)
            {
                this.View(model);
            }

            VisitPayUserDto user = this._visitPayUserApplicationService.Value.FindByName(model.UserName);
            if (user == null || user.Roles.All(x => x.Name != VisitPayRoleStrings.System.Client))
            {
                return this.View(model);
            }

            user.SecurityQuestionAnswers = new List<SecurityQuestionAnswerDto>();
            foreach (SecurityQuestionViewModel question in model.SecurityQuestions)
            {
                SecurityQuestionAnswerDto mapped = Mapper.Map<SecurityQuestionAnswerDto>(question);
                mapped.SecurityQuestion = new SecurityQuestionDto { SecurityQuestionId = question.SecurityQuestionId };
                user.SecurityQuestionAnswers.Add(mapped);
            }

            IdentityResult result = this._visitPayUserApplicationService.Value.AddSecurityQuestionsClientUser(user, true, this.GetJournalEventHttpContextDto());
            if (result.Succeeded)
            {
                return await this.ReLogin(user).ConfigureAwait(true);
            }

            this.AddErrors(result);
            return this.View(model);
        }

        [AllowAnonymous]
        public ActionResult LogOff(bool timeout = false)
        {
            this.LogOffPrivate(timeout);
            return this.RedirectToAction("Index", "Home");
        }

        private void LogOffPrivate(bool timeout = false)
        {
            if (this.User.Identity.IsAuthenticated)
            {
                this._visitPayUserApplicationService.Value.SignOut(this.User.Identity.Name, timeout);
                this.AuthenticationManager.SignOut();
            }
            this.SessionFacade.Value.ClearSession();
        }

        public ActionResult UpdateSession()
        {
            //Using this to touch the session to keep it alive.
            return this.Json(new { }, JsonRequestBehavior.AllowGet);
        }

        #region security questions

        [HttpGet]
        public ActionResult SecurityQuestions()
        {
            List<SecurityQuestionViewModel> model = new List<SecurityQuestionViewModel>();
            VisitPayUserDto visitPayUserDto = this._visitPayUserApplicationService.Value.FindById(this.CurrentUserIdString);

            if (visitPayUserDto == null)
            {
                return this.RedirectToAction("Index", "Home");
            }

            // security questions
            List<SecurityQuestionDto> allQuestions = this._securityQuestionApplicationService.Value.GetAllSecurityQuestions().ToList();

            // maximum of client setting or # of current answers
            int minimumNumberOfQuestions = Math.Max(this.ClientDto.ClientSecurityQuestionsCount, visitPayUserDto.SecurityQuestionAnswers.Count());

            // existing questions
            foreach (SecurityQuestionAnswerDto securityQuestion in visitPayUserDto.SecurityQuestionAnswers)
            {
                SecurityQuestionViewModel securityQuestionViewModel = new SecurityQuestionViewModel
                {
                    Answer = null,
                    SecurityQuestionId = securityQuestion.SecurityQuestion.SecurityQuestionId,
                    SecurityQuestions = new List<SelectListItem>()
                };

                allQuestions.ForEach(x => securityQuestionViewModel.SecurityQuestions.Add(new SelectListItem { Text = x.Question, Value = x.SecurityQuestionId.ToString() }));

                SelectListItem selectedQuestion = securityQuestionViewModel.SecurityQuestions.FirstOrDefault(x => Convert.ToInt32(x.Value) == securityQuestion.SecurityQuestion.SecurityQuestionId);
                if (selectedQuestion != null)
                {
                    selectedQuestion.Selected = true;
                }

                model.Add(securityQuestionViewModel);
            }

            // any questions needed to reach the minimum required number
            // scenario: client setting changing after a user has been created
            // example: client setting is now 3, but was 2 at time of creation, should be additional question shown here
            // note: if client setting < current answers, continue showing all current answers
            int difference = minimumNumberOfQuestions - model.Count();
            if (difference > 0)
            {
                for (int i = 0; i < difference; i++)
                {
                    SecurityQuestionViewModel securityQuestionViewModel = new SecurityQuestionViewModel
                    {
                        Answer = null,
                        SecurityQuestionId = default(int),
                        SecurityQuestions = new List<SelectListItem>()
                    };

                    allQuestions.ForEach(x => securityQuestionViewModel.SecurityQuestions.Add(new SelectListItem { Text = x.Question, Value = x.SecurityQuestionId.ToString() }));

                    model.Add(securityQuestionViewModel);
                }
            }

            return this.View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult SecurityQuestions(IList<SecurityQuestionViewModel> model)
        {
            if (!this.ModelState.IsValid)
            {
                return this.Json(new ResultMessage(false, this.GenericErrorMessage));
            }

            VisitPayUserDto visitPayUserDto = this._visitPayUserApplicationService.Value.FindById(this.CurrentUserIdString);
            if (visitPayUserDto == null)
            {
                return this.Json(new ResultMessage(false, this.GenericErrorMessage));
            }

            visitPayUserDto.SecurityQuestionAnswers = new List<SecurityQuestionAnswerDto>();
            foreach (SecurityQuestionViewModel question in model)
            {
                SecurityQuestionAnswerDto mapped = Mapper.Map<SecurityQuestionAnswerDto>(question);
                mapped.SecurityQuestion = new SecurityQuestionDto { SecurityQuestionId = question.SecurityQuestionId };
                visitPayUserDto.SecurityQuestionAnswers.Add(mapped);
            }

            IdentityResult identityResult = this._visitPayUserApplicationService.Value.AddSecurityQuestionsClientUser(visitPayUserDto, false, this.GetJournalEventHttpContextDto());
            if (!identityResult.Succeeded)
            {
                return this.Json(new ResultMessage(false, this.GenericErrorMessage));
            }

            this._visitPayUserApplicationService.Value.SendClientProfileChangeNotification(this.CurrentUserId);

            return this.Json(new ResultMessage(true, "Your security questions have been updated successfully."));
        }

        private SecurityQuestionDto GetRandomQuestion(int visitPayUserId, int? securityQuestionid)
        {
            SecurityQuestionDto result = this._visitPayUserApplicationService.Value.GetRandomSecurityQuestion(visitPayUserId, securityQuestionid);
            return result;
        }

        private async Task<SecurityQuestionDto> GetRandomQuestionAsync(string userName, int? currentQuestionId)
        {
            SecurityQuestionDto question = default(SecurityQuestionDto);

            if (currentQuestionId == null)
            {
                // use the one stored in TempData, if it exists
                currentQuestionId = this.GetCurrentSecurityQuestionId();
            }

            VisitPayUserDto user = await this.GetVisitPayUserByUsernameAsync(userName);
            if (user != null)
            {
                question = this.GetRandomQuestion(user.VisitPayUserId, currentQuestionId);
            }
            return question;
        }

        #endregion

        #region edit password

        [HttpGet]
        public ActionResult EditPassword()
        {
            return this.View(new PasswordEditViewModel());
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> EditPassword(PasswordEditViewModel model)
        {
            if (!this.ModelState.IsValid)
            {
                return this.Json(new ResultMessage(false, this.GenericErrorMessage));
            }

            if (!this._visitPayUserApplicationService.Value.IsPasswordStrongEnough(model.NewPassword).Result.Succeeded)
            {
                return this.Json(new ResultMessage(false, "This password is invalid. Please try again."));
            }

            IdentityResult identityResult = await this._visitPayUserApplicationService.Value.ChangePasswordClientAsync(this.CurrentUserIdString,
                model.OldPassword,
                model.NewPassword,
                this.ClientDto.ClientPasswordExpLimitInDays,
                this.ClientDto.ClientPasswordReuseLimit).ConfigureAwait(true);

            if (!identityResult.Succeeded)
            {
                return this.Json(new ResultMessage(false, string.Join(".  ", identityResult.Errors)));
            }

            this._visitPayUserApplicationService.Value.SendClientProfileChangeNotification(this.CurrentUserId);

            return this.Json(new ResultMessage(true, "Your password has been updated successfully."));
        }

        #endregion

        #region Helpers

        private JournalEventHttpContextDto GetJournalEventHttpContextDto()
        {
            return Mapper.Map<JournalEventHttpContextDto>(System.Web.HttpContext.Current);
        }

        // Used for XSRF protection when adding external logins
        private const string XsrfKey = "XsrfId";

        private IAuthenticationManager AuthenticationManager
        {
            get { return this.HttpContext.GetOwinContext().Authentication; }
        }

        private void AddErrors(IdentityResult result)
        {
            foreach (string error in result.Errors)
            {
                this.ModelState.AddModelError("", error);
            }
        }

        private ActionResult RedirectToLocal(string returnUrl)
        {
            if (this.Url.IsLocalUrl(returnUrl))
            {
                return this.Redirect(returnUrl);
            }
            return this.RedirectToAction("Index", "Home");
        }

        private ActionResult KillExistingSession(string returnUrl, bool showSystemMessage = false)
        {
            this._visitPayUserApplicationService.Value.SignOut(this.User.Identity.Name, false);
            this.AuthenticationManager.SignOut();
            this.SessionFacade.Value.ClearSession();

            RouteValueDictionary routeValues = new RouteValueDictionary { { "returnUrl", returnUrl } };

            if (showSystemMessage)
                routeValues.Add("sm", 1);

            return this.RedirectToAction("Index", "Account", routeValues);
        }

        private async Task<ActionResult> ReLogin(VisitPayUserDto user)
        {
            SignInStatusExEnum signInStatus = await this._visitPayUserApplicationService.Value.ClientReLoginAsync(user, Mapper.Map<HttpContextDto>(System.Web.HttpContext.Current)).ConfigureAwait(true);
            if (signInStatus == SignInStatusExEnum.RequiresSecurityQuestions)
            {
                this.SetTempData(UsernameKey, user.UserName);
                return this.RedirectToAction("SecuritySetup");
            }

            return this.RedirectToLocal("");
        }

        internal class ChallengeResult : HttpUnauthorizedResult
        {
            public ChallengeResult(string provider, string redirectUri)
                : this(provider, redirectUri, null)
            {
            }

            public ChallengeResult(string provider, string redirectUri, string userId)
            {
                this.LoginProvider = provider;
                this.RedirectUri = redirectUri;
                this.UserId = userId;
            }

            public string LoginProvider { get; set; }
            public string RedirectUri { get; set; }
            public string UserId { get; set; }

            public override void ExecuteResult(ControllerContext context)
            {
                AuthenticationProperties properties = new AuthenticationProperties { RedirectUri = this.RedirectUri };
                if (this.UserId != null)
                {
                    properties.Dictionary[XsrfKey] = this.UserId;
                }
                context.HttpContext.GetOwinContext().Authentication.Challenge(properties, this.LoginProvider);
            }
        }

        private async Task<VisitPayUserDto> GetVisitPayUserByUsernameAsync(string userName)
        {
            VisitPayUserDto user = await this._visitPayUserApplicationService.Value.FindByNameAsync(userName).ConfigureAwait(true);
            return user;
        }

        private int? GetCurrentSecurityQuestionId()
        {
            int? securityQuestionid = null;

            string questionIdStr = this.TempData[SecurityQuestionIdKey] as string;
            if (questionIdStr != null)
            {
                int tempId;
                if (Int32.TryParse(questionIdStr, out tempId))
                {
                    securityQuestionid = tempId;
                }
            }
            return securityQuestionid;
        }

        private string GetCurrentUserName()
        {
            return this.TempData[UsernameKey] as string;
        }
        #endregion

    }
}
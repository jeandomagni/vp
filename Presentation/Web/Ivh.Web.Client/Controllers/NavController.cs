﻿namespace Ivh.Web.Client.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Web.Mvc;
    using System.Web.SessionState;
    using Application.Core.Common.Dtos;
    using Application.Core.Common.Interfaces;
    using Application.SecureCommunication.Common.Interfaces;
    using Common.Base.Enums;
    using Common.VisitPay.Enums;
    using Common.Web.Interfaces;
    using Ivh.Common.Session;
    using Models;

    public class NavController : BaseController
    {
        private readonly Lazy<IClientSupportRequestApplicationService> _clientSupportRequestApplicationService;
        private readonly Lazy<IFeatureApplicationService> _featureApplicationService;
        private readonly Lazy<IAnalyticReportApplicationService> _analyticReportApplicationService;
        private readonly Lazy<IWebClientSessionFacade> _webClientSessionFacade;

        public NavController(
            Lazy<IBaseControllerService> baseControllerService,
            Lazy<IClientSupportRequestApplicationService> clientSupportRequestApplicationService,
            Lazy<IFeatureApplicationService> featureApplicationService,
            Lazy<IAnalyticReportApplicationService> analyticReportApplicationService,
            Lazy<IWebClientSessionFacade> webClientSessionFacade)
            : base(baseControllerService)
        {
            this._clientSupportRequestApplicationService = clientSupportRequestApplicationService;
            this._featureApplicationService = featureApplicationService;
            this._analyticReportApplicationService = analyticReportApplicationService;
            this._webClientSessionFacade = webClientSessionFacade;
        }

        const string AnalyticReportsKey = "AnalyticReports";

        [Common.Web.Attributes.SessionState(SessionStateBehavior.Required)]
        public PartialViewResult NavClient()
        {
            int unreadMessageCountFromClient = this._clientSupportRequestApplicationService.Value.GetUnreadMessagesCountFromClient(null);
            int unreadMessageCountToClient = this._clientSupportRequestApplicationService.Value.GetUnreadMessagesCountToClient(this.CurrentUserId);
            bool visitPayReportsIsEnabled = this._featureApplicationService.Value.IsFeatureEnabled(VisitPayFeatureEnum.VisitPayReportsIsEnabled);
            bool demoClientReportsDashboardIsEnabled = this._featureApplicationService.Value.IsFeatureEnabled(VisitPayFeatureEnum.DemoClientReportDashboardIsEnabled);
            bool offlineVisitPayIsEnabled = this._featureApplicationService.Value.IsFeatureEnabled(VisitPayFeatureEnum.OfflineVisitPay);
            bool visitPayDocumentIsEnabled = this._featureApplicationService.Value.IsFeatureEnabled(VisitPayFeatureEnum.FeatureVisitPayDocumentIsEnabled);
            bool analyticReportsIsEnabled = this._featureApplicationService.Value.IsFeatureEnabled(VisitPayFeatureEnum.FeatureAnalyticReportsIsEnabled);
            bool cardReaderIsEnabled = this._featureApplicationService.Value.IsFeatureEnabled(VisitPayFeatureEnum.FeatureCardReaderIsEnabled);
            IList<AnalyticReportDto> analyticReportList = null;
            if (analyticReportsIsEnabled)
            {
                analyticReportList = (IList<AnalyticReportDto>) this.Session[AnalyticReportsKey];
                if (analyticReportList == null)
                {
                    this._analyticReportApplicationService.Value.ValidateToken(null, null);
                    analyticReportList = this._analyticReportApplicationService.Value.GetAnalyticReportList();
                    this._webClientSessionFacade.Value.SetSessionValue(AnalyticReportsKey, analyticReportList);
                }
            }
            bool chatIsEnabled = this._featureApplicationService.Value.IsFeatureEnabled(VisitPayFeatureEnum.FeatureChatIsEnabled);
            bool lockboxIsEnabled = this.ApplicationSettingsService.Value.IsLockBoxEnabled.Value;

            return this.PartialView("_NavClient", new NavViewModel(
                this.ClientDto,
                unreadMessageCountFromClient,
                unreadMessageCountToClient,
                visitPayReportsIsEnabled,
                demoClientReportsDashboardIsEnabled,
                offlineVisitPayIsEnabled,
                visitPayDocumentIsEnabled,
                analyticReportsIsEnabled,
                analyticReportList,
                chatIsEnabled,
                lockboxIsEnabled,
                cardReaderIsEnabled));
        }
    }
}
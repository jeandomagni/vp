﻿namespace Ivh.Web.Client.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.Linq;
    using System.Security.Claims;
    using System.Threading.Tasks;
    using System.Web;
    using System.Web.Mvc;
    using System.Web.SessionState;
    using Application.Base.Common.Dtos;
    using Application.Base.Common.Interfaces;
    using Application.Content.Common.Dtos;
    using Application.Content.Common.Interfaces;
    using Application.Core.Common.Dtos;
    using Application.Core.Common.Interfaces;
    using Application.Email.Common.Dtos;
    using Application.Email.Common.Interfaces;
    using Application.FinanceManagement.Common.Dtos;
    using Application.FinanceManagement.Common.Interfaces;
    using Application.SecureCommunication.Common.Dtos;
    using Application.SecureCommunication.Common.Interfaces;
    using Application.User.Common.Dtos;
    using Attributes;
    using AutoMapper;
    using Common.Base.Utilities.Extensions;
    using Common.Base.Utilities.Helpers;
    using Common.EventJournal;
    using Common.VisitPay.Constants;
    using Common.VisitPay.Enums;
    using Common.VisitPay.Strings;
    using Common.Web.Filters;
    using Common.Web.Interfaces;
    using Common.Web.Mapping;
    using Common.Web.Models;
    using Common.Web.Models.Alerts;
    using Common.Web.Models.Consolidate;
    using Common.Web.Models.FinancePlan;
    using Common.Web.Models.Payment;
    using Common.Web.Models.Statement;
    using Microsoft.AspNet.Identity;
    using Microsoft.Owin.Security;
    using Models.Account;
    using Models.Consolidate;
    using Models.Search;
    using Models.Sso;
    using Models.WorkQueue;
    using Services;
    using SessionStateAttribute = Common.Web.Attributes.SessionStateAttribute;
    using ResultMessage = Models.ResultMessage;

    [VpAuthorize(Roles = VisitPayRoleStrings.System.Client + "," + VisitPayRoleStrings.Admin.Administrator + ",")]
    [VpAuthorize(Roles = VisitPayRoleStrings.Csr.CSR + "," + VisitPayRoleStrings.Payment.PaymentRefund + "," + VisitPayRoleStrings.Payment.PaymentRefundReturnedCheck + "," + VisitPayRoleStrings.Payment.PaymentVoid + "," + VisitPayRoleStrings.Csr.ReconfigureFP + "," + VisitPayRoleStrings.Csr.SupportAdmin + "," + VisitPayRoleStrings.Csr.SupportView + "," + VisitPayRoleStrings.Csr.UserAdmin)]
    public class SearchController : BaseController
    {
        private const string ViewAccountMetricController = "SearchController.ViewAccount";

        private readonly Lazy<IAlertingApplicationService> _alertingApplicationService;
        private readonly Lazy<IConsolidationGuarantorApplicationService> _consolidationGuarantorApplicationService;
        private readonly Lazy<IContentApplicationService> _contentApplicationService;
        private readonly Lazy<IFinancePlanApplicationService> _financePlanApplicationService;
        private readonly Lazy<IGuarantorApplicationService> _guarantorApplicationService;
        private readonly Lazy<IPaymentMethodsApplicationService> _paymentMethodsApplicationService;
        private readonly Lazy<IStatementApplicationService> _statementApplicationService;
        private readonly Lazy<ISupportRequestApplicationService> _supportRequestApplicationService;
        private readonly Lazy<IUserEmailApplicationService> _userEmailApplicationService;
        private readonly Lazy<IVisitAgingHistoryApplicationService> _visitAgingHistoryApplicationService;
        private readonly Lazy<IVisitApplicationService> _visitApplicationService;
        private readonly Lazy<IVisitPayUserApplicationService> _visitPayUserApplicationService;
        private readonly Lazy<IVisitPayUserJournalEventApplicationService> _visitPayUserJournalEventApplicationService;
        private readonly Lazy<IVisitStateHistoryApplicationService> _visitStateHistoryApplicationService;
        private readonly Lazy<IVisitTransactionApplicationService> _visitTransactionApplicationService;
        private readonly Lazy<IPaymentDetailApplicationService> _paymentDetailApplicationService;
        private readonly Lazy<ISecurityQuestionApplicationService> _securityQuestionApplicationService;
        private readonly Lazy<GuarantorFilterService> _guarantorFilterService;
        private readonly Lazy<ISsoApplicationService> _ssoApplicationService;
        private readonly Lazy<IBalanceTransferApplicationService> _balanceTransferApplicationService;
        private readonly Lazy<IVisitBalanceBaseApplicationService> _visitBalanceBaseApplicationService;
        private readonly Lazy<ICommunicationApplicationService> _communicationApplicationService;
        private readonly Lazy<IChatHistoryExportApplicationService> _chatHistoryExportApplicationService;

        public SearchController(
            Lazy<IBaseControllerService> baseControllerService,
            Lazy<IConsolidationGuarantorApplicationService> consolidationGuarantorApplicationService,
            Lazy<IContentApplicationService> contentApplicationService,
            Lazy<IFinancePlanApplicationService> financePlanApplicationService,
            Lazy<IGuarantorApplicationService> guarantorApplicationService,
            Lazy<IPaymentMethodsApplicationService> paymentMethodsApplicationService,
            Lazy<ISupportRequestApplicationService> supportRequestApplicationService,
            Lazy<IStatementApplicationService> statementApplicationService,
            Lazy<IVisitApplicationService> visitApplicationService,
            Lazy<IVisitTransactionApplicationService> visitTransactionApplicationService,
            Lazy<IVisitPayUserApplicationService> visitPayUserApplicationService,
            Lazy<IVisitPayUserJournalEventApplicationService> visitPayUserJournalEventApplicationService,
            Lazy<IVisitStateHistoryApplicationService> visitStateHistoryApplicationService,
            Lazy<IVisitAgingHistoryApplicationService> visitAgingHistoryApplicationService,
            Lazy<IUserEmailApplicationService> userEmailApplicationService,
            Lazy<IAlertingApplicationService> alertingApplicationService,
            Lazy<IPaymentDetailApplicationService> paymentDetailApplicationService,
            Lazy<ISecurityQuestionApplicationService> securityQuestionApplicationService,
            Lazy<GuarantorFilterService> guarantorFilterService,
            Lazy<ISsoApplicationService> ssoApplicationService,
            Lazy<IBalanceTransferApplicationService> balanceTransferApplicationService,
            Lazy<IVisitBalanceBaseApplicationService> visitBalanceBaseApplicationService,
            Lazy<ICommunicationApplicationService> communicationApplicationService,
            Lazy<IChatHistoryExportApplicationService> chatHistoryExportApplicationService)
            : base(baseControllerService)
        {
            this._consolidationGuarantorApplicationService = consolidationGuarantorApplicationService;
            this._contentApplicationService = contentApplicationService;
            this._financePlanApplicationService = financePlanApplicationService;
            this._guarantorApplicationService = guarantorApplicationService;
            this._paymentMethodsApplicationService = paymentMethodsApplicationService;
            this._supportRequestApplicationService = supportRequestApplicationService;
            this._statementApplicationService = statementApplicationService;
            this._visitApplicationService = visitApplicationService;
            this._visitTransactionApplicationService = visitTransactionApplicationService;
            this._visitPayUserApplicationService = visitPayUserApplicationService;
            this._visitPayUserJournalEventApplicationService = visitPayUserJournalEventApplicationService;
            this._visitStateHistoryApplicationService = visitStateHistoryApplicationService;
            this._visitAgingHistoryApplicationService = visitAgingHistoryApplicationService;
            this._userEmailApplicationService = userEmailApplicationService;
            this._alertingApplicationService = alertingApplicationService;
            this._paymentDetailApplicationService = paymentDetailApplicationService;
            this._securityQuestionApplicationService = securityQuestionApplicationService;
            this._guarantorFilterService = guarantorFilterService;
            this._ssoApplicationService = ssoApplicationService;
            this._balanceTransferApplicationService = balanceTransferApplicationService;
            this._visitBalanceBaseApplicationService = visitBalanceBaseApplicationService;
            this._communicationApplicationService = communicationApplicationService;
            this._chatHistoryExportApplicationService = chatHistoryExportApplicationService;
        }

        const string SearchGuarantorsExportKey = "SearchGuarantorsExport";
        const string EmulateVpGuarantorIdKey = "EmulateVpGuarantorId";
        const string GuarantorVisitsExportKey = "GuarantorVisitsExport";
        const string GuarantorVisitTransactionsExportKey = "GuarantorVisitTransactionsExport";
        const string GuarantorTransactionsExportKey = "GuarantorTransactionsExport";
        const string GuarantorFinancePlansExportKey = "GuarantorFinancePlansExport";
        const string CommunicationEventsExportKey = "CommunicationEventsExport";
        const string CommunicationsExportKey = "CommunicationsExport";
        const string ChatHistoryOverviewExportKey = "ChatHistoryOverviewExport";
        const string ChatThreadHistoryExportKey = "ChatThreadHistoryExport";

        private List<SelectListItem> GetDerivedVisitStates()
        {
            List<SelectListItem> items = new List<SelectListItem>();

            IEnumerable<VisitStateDerivedEnum> visitStateFilters = this._visitApplicationService.Value.GetDerivedVisitStates();
            foreach (VisitStateDerivedEnum visitStateFilter in visitStateFilters)
            {
                //SelectListGroup selectListGroup = new SelectListGroup { Name = visitState.ToString() };
                items.Add(new SelectListItem
                {
                    //Group = selectListGroup,
                    Text = visitStateFilter.ToString().InsertSpaceBetweenLowerAndCapitalLetter(),
                    Value = $"{(int)visitStateFilter}"
                });
            }

            return items;
        }

        private List<SelectListItem> GetVisitStates()
        {
            List<SelectListItem> items = new List<SelectListItem>();

            IEnumerable<VisitStateEnum> visitStateFilters = this._visitApplicationService.Value.GetVisitStates().Where(x => x != VisitStateEnum.NotSet);
            foreach (VisitStateEnum visitStateFilter in visitStateFilters)
            {
                //SelectListGroup selectListGroup = new SelectListGroup { Name = visitState.ToString() };
                items.Add(new SelectListItem
                {
                    //Group = selectListGroup,
                    Text = visitStateFilter.ToString().InsertSpaceBetweenLowerAndCapitalLetter(),
                    Value = $"{(int)visitStateFilter}"
                });
            }

            return items;
        }

        private List<SelectListItem> GetGroupedFinancePlanStatuses()
        {
            List<SelectListItem> items = new List<SelectListItem>();

            IEnumerable<IGrouping<string, FinancePlanStatusDto>> groupedFinancePlanStatuses = this._financePlanApplicationService.Value.GetFinancePlanStatuses().GroupBy(x => x.FinancePlanStatusGroupName);
            foreach (IGrouping<string, FinancePlanStatusDto> groupedFinancePlanStatus in groupedFinancePlanStatuses)
            {
                SelectListGroup selectListGroup = new SelectListGroup { Name = groupedFinancePlanStatus.Key };
                groupedFinancePlanStatus.ToList().ForEach(x => items.Add(new SelectListItem
                {
                    Group = selectListGroup,
                    Text = x.FinancePlanStatusDisplayName,
                    Value = x.FinancePlanStatusId.ToString()
                }));
            }

            return items;
        }

        #region search guarantors

        [HttpGet]
        public ActionResult SearchGuarantors()
        {
            return this.View(new SearchGuarantorsSearchFilterViewModel());
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult SearchGuarantors(SearchGuarantorsSearchFilterViewModel model, int page, int rows, string sidx, string sord)
        {
            if (!model.IsValidSearch)
            {
                model.VpGuarantorId = -99999;
            }

            //
            GuarantorFilterDto guarantorFilterDto = new GuarantorFilterDto
            {
                SortField = sidx,
                SortOrder = sord
            };
            Mapper.Map(model, guarantorFilterDto);

            GuarantorWithLastStatementResultsDto guarantorsResultsDto = this._statementApplicationService.Value.GetGuarantorsWithLastStatement(guarantorFilterDto, page, rows);

            this._visitPayUserJournalEventApplicationService.Value.LogClientAccountSearch(this.CurrentUserId, guarantorsResultsDto.Guarantors.Select(x => x.Guarantor.VpGuarantorId).ToList(), this.GetJournalEventHttpContextDto());

            List<SearchGuarantorsSearchResultViewModel> searchGuarantorsSearchResultViewModels = new List<SearchGuarantorsSearchResultViewModel>();
            foreach (GuarantorWithLastStatementDto guarantorWithLastStatementDto in guarantorsResultsDto.Guarantors)
            {
                SearchGuarantorsSearchResultViewModel searchGuarantorsSearchResultViewModel = Mapper.Map<SearchGuarantorsSearchResultViewModel>(guarantorWithLastStatementDto);
                searchGuarantorsSearchResultViewModel.SupportsOffline = this.IsFeatureEnabled(VisitPayFeatureEnum.OfflineVisitPay);
                searchGuarantorsSearchResultViewModels.Add(searchGuarantorsSearchResultViewModel);
            }

            int totalRecordCount = guarantorsResultsDto.TotalRecords;

            return this.Json(new
            {
                Guarantors = searchGuarantorsSearchResultViewModels,
                page,
                total = Math.Ceiling((decimal)totalRecordCount / rows),
                records = totalRecordCount
            });
        }
        
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Common.Web.Attributes.SessionState(SessionStateBehavior.Required)]
        public ActionResult SearchGuarantorsExport(SearchGuarantorsSearchFilterViewModel model, string sidx, string sord)
        {
            if (!model.IsValidSearch)
            {
                model.VpGuarantorId = -99999;
            }

            //
            GuarantorFilterDto guarantorFilterDto = new GuarantorFilterDto
            {
                SortField = sidx,
                SortOrder = sord
            };
            Mapper.Map(model, guarantorFilterDto);

            //
            byte[] bytes = this._statementApplicationService.Value.ExportGuarantorsWithLastStatement(guarantorFilterDto, this.User.Identity.GetUserName());
            this.SetTempData(SearchGuarantorsExportKey, bytes);

            //
            return this.Json(new ResultMessage(true, this.Url.Action("SearchGuarantorsExportDownload")));
        }

        [HttpGet]
        public ActionResult SearchGuarantorsExportDownload()
        {
            object tempData = this.TempData[SearchGuarantorsExportKey];
            if (tempData != null)
            {
                this.WriteExcelInline((byte[])tempData, "AccountList.xlsx");
            }

            return this.Json(new { }, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region guarantor account

        [HttpGet]
        public ActionResult GuarantorAccount(int id)
        {
            GuarantorAccountViewModel model = new GuarantorAccountViewModel { GracePeriod = this.ClientDto.GracePeriodLength };

            //
            GuarantorDto guarantorDto = this._guarantorApplicationService.Value.GetGuarantor(id);
            if (guarantorDto == null)
            {
                return this.View(model);
            }

            Mapper.Map(guarantorDto, model);

            // payment methods
            IList<PaymentMethodDto> paymentMethodDtos = this._paymentMethodsApplicationService.Value.GetPaymentMethods(id, this.CurrentUserId);
            Mapper.Map(paymentMethodDtos, model.PaymentMethods);

            // payments
            model.HasPendingPayments = this._paymentDetailApplicationService.Value.HasScheduledPayments(model.VpGuarantorId);

            PaymentProcessorResponseFilterDto paymentProcessorResponseFilterDto = new PaymentProcessorResponseFilterDto()
            {
                PaymentStatus = new List<PaymentStatusEnum>() { PaymentStatusEnum.ClosedPaid, PaymentStatusEnum.ActivePendingACHApproval },
                PaymentType = EnumHelper<PaymentTypeEnum>.GetValues(PaymentTypeEnumCategory.Payment).ToList(),
                SortField = "InsertDate",
                SortOrder = "desc"
            };

            PaymentHistoryResultsDto paymentResultsDto = this._paymentDetailApplicationService.Value.GetPaymentHistory(model.VpGuarantorId, model.VpGuarantorId, paymentProcessorResponseFilterDto, 1, 1);
            if (paymentResultsDto.Payments.Any())
            {
                model.LastPaymentAmount = paymentResultsDto.Payments.First().NetPaymentAmount;
                model.LastPaymentDate = MappingHelper.MapToClientDateTime(paymentResultsDto.Payments.First().InsertDate);
                
            }

            // communication preference 
            IList<VisitPayUserCommunicationPreferenceDto> communicationPreferences = this._communicationApplicationService.Value.CommunicationPreferences(guarantorDto.User.VisitPayUserId);
            Mapper.Map(communicationPreferences, model.VisitPayUserCommunicationPreferences);

            // consolidation
            model.GuarantorConsolidationStatus = guarantorDto.ConsolidationStatus;

            // last login
            DateTime? lastLoginDate = this._visitPayUserApplicationService.Value.GetLastLoginDate(guarantorDto.User.VisitPayUserId);
            model.LastLoginDate = MappingHelper.MapToClientDateTime(lastLoginDate);

            // locale
            string locale = this._visitPayUserApplicationService.Value.GetUserLocale(guarantorDto.User.VisitPayUserId.ToString());
            model.Locale = locale;

            // last statement
            StatementDto lastStatementDto = this._statementApplicationService.Value.GetLastStatement(guarantorDto);
            model.LastStatementDate = lastStatementDto?.StatementDate; //Needs to stay in UTC since it is supposed to be a Date value with no relevant time value

            // VPNG-19339 - PaymentDueDate can never be in the past 
            DateTime? paymentDueDate = this._statementApplicationService.Value.GetNextPaymentDate(guarantorDto.VpGuarantorId);
            model.PaymentDueDate = paymentDueDate;

            //
            model.TotalBalance = this._visitBalanceBaseApplicationService.Value.GetTotalBalance(guarantorDto.VpGuarantorId);

            // search filters
            model.GuarantorVisitsSearchFilter = this.GetGuarantorVisitsSearchFilter(guarantorDto);
            model.GuarantorTransactionsSearchFilter = this.GetGuarantorTransactionsSearchFilter(guarantorDto);
            model.GuarantorFinancePlansSearchFilter = this.GetGuarantorFinancePlansSearchFilter();
            model.GuarantorSupportHistoryFilter = new GuarantorSupportHistorySearchFilterViewModel();
            model.GuarantorChatHistoryFilter = new GuarantorChatHistorySearchFilterViewModel();
            model.GuarantorCommunicationsFilter = new GuarantorCommunicationsSearchFilterViewModel();
            model.GuarantorPaymentsViewModel = new MyPaymentsViewModel
            {
                PaymentHistorySearchFilter = new PaymentHistorySearchFilterViewModel
                {
                    PaymentMethods = Mapper.Map<List<SelectListItem>>(this._paymentMethodsApplicationService.Value.GetPaymentMethods(guarantorDto.VpGuarantorId, this.CurrentUserId, true)),
                    PaymentStatuses = Mapper.Map<List<SelectListItem>>(this._paymentDetailApplicationService.Value.GetPaymentStatuses("Search"))
                },
                PaymentPending = new PaymentPendingViewModel()
            };

            // log
            this._visitPayUserJournalEventApplicationService.Value.LogClientAccountView(this.CurrentUserId, guarantorDto.VpGuarantorId, this.GetJournalEventHttpContextDto());

            this.MetricsProvider.Value.Increment(ViewAccountMetricController);

            if (this.ClientDto.MyChartSsoIsEnabled)
            {
                IList<SsoProviderWithUserSettingsDto> providersWithSettings = this._ssoApplicationService.Value.GetAllProvidersWithUserSettings(model.UserVisitPayUserId);
                IList<ClientSsoProviderWithUserSettingsViewModel> ssoModels = Mapper.Map<IList<ClientSsoProviderWithUserSettingsViewModel>>(providersWithSettings);

                model.ManagedSso = new Dictionary<string, string>();
                foreach (ClientSsoProviderWithUserSettingsViewModel ssoModel in ssoModels.Where(x => !string.IsNullOrWhiteSpace(x.SourceSystemKeyLabel)))
                {
                    model.ManagedSso.Add(ssoModel.SourceSystemKeyLabel, ssoModel.SourceSystemKey);
                }
            }

            return this.View(model);
        }

        [HttpGet]
        [Common.Web.Attributes.SessionState(SessionStateBehavior.Required)]
        public PartialViewResult GuarantorAlerts(int id)
        {
            IList<AlertDto> alerts = this._alertingApplicationService.Value.GetAlertsForGuarantor(id, this.SessionFacade.Value, new List<int>());
            return this.PartialView("~/Views/Search/_GuarantorAlerts.cshtml", alerts.Count());
        }

        [HttpGet]
        [Common.Web.Attributes.SessionState(SessionStateBehavior.Required)]
        public PartialViewResult GuarantorAlertsModal(int id)
        {
            IList<AlertDto> alerts = this._alertingApplicationService.Value.GetAlertsForGuarantor(id, this.SessionFacade.Value, new List<int>());
            return this.PartialView("~/Views/Search/_GuarantorAlertsModal.cshtml", Mapper.Map<IList<AlertViewModel>>(alerts));
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [OverrideAuthorization]
        [VpAuthorize(Roles = VisitPayRoleStrings.Csr.UserAdmin)]
        public JsonResult GuarantorCancel(int guarantorId, int reasonId, string note)
        {
            GuarantorDto guarantorDto = this._guarantorApplicationService.Value.GetGuarantor(guarantorId);
            if (guarantorDto != null && !guarantorDto.AccountClosedDate.HasValue)
            {
                DataResult<string, bool> result = this._guarantorApplicationService.Value.CancelAccount(guarantorDto.User.VisitPayUserId, this.CurrentUserId, reasonId, note);
                return this.Json(result, JsonRequestBehavior.AllowGet);
            }
            return this.Json(new DataResult<string, bool> { Result = false, Data = "Unable to find guarantor" });
        }

        [HttpGet]
        [OverrideAuthorization]
        [VpAuthorize(Roles = VisitPayRoleStrings.Csr.UserAdmin)]
        public PartialViewResult GuarantorCancelAccount()
        {
            IList<GuarantorCancellationReasonDto> reasons = this._guarantorApplicationService.Value.GetAllCancellationReasons();
            return this.PartialView("_GuarantorCancelAccount", Mapper.Map<IList<GuarantorCancellationReasonViewModel>>(reasons));
        }

        [HttpGet]
        public ActionResult ConfirmReactivation()
        {
            CmsVersionDto cmsVersion = this._contentApplicationService.Value.GetCurrentVersionAsync(CmsRegionEnum.WaitPeriodToReactivateAccount).Result;

            return this.Json(new
            {
                cmsVersion.ContentTitle,
                cmsVersion.ContentBody
            }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [OverrideAuthorization]
        [VpAuthorize(Roles = VisitPayRoleStrings.Csr.UserAdmin)]
        public JsonResult GuarantorReactivate(int guarantorId)
        {
            GuarantorDto guarantorDto = this._guarantorApplicationService.Value.GetGuarantor(guarantorId);
            if (guarantorDto != null && (guarantorDto.AccountClosedDate.HasValue || guarantorDto.AccountCancellationDate.HasValue))
            {
                DateTime reactivationBaseDate = guarantorDto.AccountCancellationDate ?? DateTime.UtcNow;
                DateTime reactivationDate = reactivationBaseDate.AddDays(this.ClientDto.WaitPeriodToReactivateAccount);
                if (DateTime.UtcNow.CompareTo(reactivationDate) > 0)
                {
                    reactivationDate = DateTime.UtcNow;
                }
                DataResult<string, bool> result = this._guarantorApplicationService.Value.ReactivateAccount(
                    guarantorUserId: guarantorDto.User.VisitPayUserId,
                    clientUserId: this.CurrentUserId,
                    reactivateAccountOn: reactivationDate,
                    context: this.GetJournalEventHttpContextDto()
                    );

                return this.Json(result, JsonRequestBehavior.AllowGet);
            }
            return this.Json(new DataResult<string, bool> { Result = false, Data = "Unable to find guarantor" });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [OverrideAuthorization]
        [VpAuthorize(Roles = VisitPayRoleStrings.Miscellaneous.IvhAdmin)]
        public JsonResult GuarantorDisable(int guarantorId, bool disable)
        {
            bool result = false;
            GuarantorDto guarantorDto = this._guarantorApplicationService.Value.GetGuarantor(guarantorId);
            if (guarantorDto != null)
            {
                result = this._visitPayUserApplicationService.Value.DisableEnableVisitPayUser(guarantorDto.User.VisitPayUserId, disable, this.CurrentUserId, this.GetJournalEventHttpContextDto());
            }
            return this.Json(new { Result = result });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [OverrideAuthorization]
        public JsonResult RequestPassword(int guarantorId)
        {
            GuarantorDto guarantorDto = this._guarantorApplicationService.Value.GetGuarantor(guarantorId);
            if (guarantorDto != null && !guarantorDto.AccountClosedDate.HasValue)
            {
                bool result = this._visitPayUserApplicationService.Value.RequestPasswordForGuarantor(guarantorId, this.ClientDto.GuarantorTempPasswordExpLimitInHours);
                return this.Json(result, JsonRequestBehavior.AllowGet);
            }
            return this.Json(new DataResult<string, bool> { Result = false, Data = "Unable to find guarantor" });
        }

        [VpAuthorize(Roles = VisitPayRoleStrings.Csr.ChangePaymentDueDay)]
        public ActionResult ChangePaymentDueDay()
        {
            return this.PartialView("_GuarantorChangePaymentDueDay");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [OverrideAuthorization]
        [VpAuthorize(Roles = VisitPayRoleStrings.Csr.ChangePaymentDueDay)]
        public JsonResult ChangePaymentDueDay(int? guarantorId, int dueDay, int? currentVisitPayUserId)
        {
            GuarantorDto guarantorDto = null;
            if (guarantorId.HasValue)
            {
                guarantorDto = this._guarantorApplicationService.Value.GetGuarantor(guarantorId.Value);
            }
            else if(currentVisitPayUserId.HasValue)
            {
                guarantorDto = this._visitPayUserApplicationService.Value.GetGuarantorFromVisitPayUserId(currentVisitPayUserId.Value);
            }
            
            if (guarantorDto != null)
            {
                DataResult<string, bool> result = this._guarantorApplicationService.Value.ChangePaymentDueDay(guarantorDto.User.VisitPayUserId, dueDay, false, this.CurrentUserId);
                return this.Json(result, JsonRequestBehavior.AllowGet);
            }

            return this.Json(new DataResult<string, bool> { Result = false, Data = "Unable to find guarantor" });
        }

        [HttpGet]
        [VpAuthorize(Roles = VisitPayRoleStrings.Csr.UserAdmin + "," + VisitPayRoleStrings.Csr.CSR + "," + VisitPayRoleStrings.Csr.SupportAdmin + "," + VisitPayRoleStrings.Csr.SupportView)]
        public ActionResult GuarantorEditAccountSettings(int vpGuarantorId)
        {
            HttpContext context = System.Web.HttpContext.Current;
            this._visitPayUserJournalEventApplicationService.Value.LogClientAccountView(this.CurrentUserId, vpGuarantorId, Mapper.Map<JournalEventHttpContextDto>(context));

            GuarantorDto guarantorDto = this._guarantorApplicationService.Value.GetGuarantor(vpGuarantorId);

            VisitPayUserDto visitPayUserDto = this._visitPayUserApplicationService.Value.FindByGuarantorId(vpGuarantorId);

            IList<VisitPayUserCommunicationPreferenceDto> communicationPreferences = this._communicationApplicationService.Value.CommunicationPreferences(guarantorDto.User.VisitPayUserId);

            ProfileEditClientViewModel model = new ProfileEditClientViewModel
            {
                AddressStreet1 = visitPayUserDto.AddressStreet1,
                AddressStreet2 = visitPayUserDto.AddressStreet2,
                City = visitPayUserDto.City,
                State = visitPayUserDto.State,
                Zip = visitPayUserDto.Zip,
                MailingAddressStreet1 = visitPayUserDto.MailingAddressStreet1,
                MailingAddressStreet2 = visitPayUserDto.MailingAddressStreet2,
                MailingCity = visitPayUserDto.MailingCity,
                MailingState = visitPayUserDto.MailingState,
                MailingZip = visitPayUserDto.MailingZip,
                Email = visitPayUserDto.Email,
                EmailOriginal = visitPayUserDto.Email,
                FirstName = visitPayUserDto.FirstName,
                LastName = visitPayUserDto.LastName,
                MiddleName = visitPayUserDto.MiddleName,               
                UserName = visitPayUserDto.UserName,
                UserNameConfirm = visitPayUserDto.UserName,
                UserNameOriginal = visitPayUserDto.UserName,
                CanEditMailingAddress = this.IsFeatureEnabled(VisitPayFeatureEnum.FeatureMailIsEnabled) && communicationPreferences.Any(x => x.CommunicationMethodId == (int)CommunicationMethodEnum.Mail),

                IsOfflineUser = guarantorDto.IsOfflineGuarantor
            };

            return this.PartialView("_GuarantorEditAccountSettings", model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [OverrideAuthorization]
        [VpAuthorize(Roles = VisitPayRoleStrings.Csr.UserAdmin + "," + VisitPayRoleStrings.Csr.CSR + "," + VisitPayRoleStrings.Csr.SupportAdmin + "," + VisitPayRoleStrings.Csr.SupportView)]
        public JsonResult GuarantorEditAccountSettings(int vpGuarantorId, ProfileEditClientViewModel model)
        {
            HttpContext context = System.Web.HttpContext.Current;
            if (!this.ModelState.IsValid)
            {
                return this.Json(new Common.Web.Models.ResultMessage(false, this.GenericErrorMessage));
            }

            VisitPayUserDto visitPayUserDto = this._visitPayUserApplicationService.Value.FindByGuarantorId(vpGuarantorId);

            if (visitPayUserDto != null)
            {
                bool isUsernameAvailable = this._visitPayUserApplicationService.Value.IsUsernameAvailable(model.UserName, visitPayUserDto.VisitPayUserId);
                if (!isUsernameAvailable)
                {
                    return this.Json(new ResultMessage(false, "Username already exists for another user."));
                }

                bool update = false;
                Func<string, string, string> setValue = (a, b) =>
                {
                    if (!string.Equals(a, b))
                    {
                        update = true;
                        return b;
                    }
                    return a;
                };

                visitPayUserDto.UserName = setValue(visitPayUserDto.UserName, model.UserName);
                visitPayUserDto.AddressStreet1 = setValue(visitPayUserDto.AddressStreet1, model.AddressStreet1);
                visitPayUserDto.AddressStreet2 = setValue(visitPayUserDto.AddressStreet2, model.AddressStreet2);
                visitPayUserDto.City = setValue(visitPayUserDto.City, model.City);
                visitPayUserDto.State = setValue(visitPayUserDto.State, model.State);
                visitPayUserDto.Zip = setValue(visitPayUserDto.Zip, model.Zip);
                visitPayUserDto.MailingAddressStreet1 = setValue(visitPayUserDto.MailingAddressStreet1, model.MailingAddressStreet1);
                visitPayUserDto.MailingAddressStreet2 = setValue(visitPayUserDto.MailingAddressStreet2, model.MailingAddressStreet2);
                visitPayUserDto.MailingCity = setValue(visitPayUserDto.MailingCity, model.MailingCity);
                visitPayUserDto.MailingState = setValue(visitPayUserDto.MailingState, model.MailingState);
                visitPayUserDto.MailingZip = setValue(visitPayUserDto.MailingZip, model.MailingZip);
                visitPayUserDto.Email = setValue(visitPayUserDto.Email, model.Email);
                visitPayUserDto.FirstName = setValue(visitPayUserDto.FirstName, model.FirstName);
                visitPayUserDto.LastName = setValue(visitPayUserDto.LastName, model.LastName);
                visitPayUserDto.MiddleName = setValue(visitPayUserDto.MiddleName, model.MiddleName);

                if (update)
                {
                    this._visitPayUserJournalEventApplicationService.Value.LogClientAccountModify(this.CurrentUserId, vpGuarantorId, Mapper.Map<JournalEventHttpContextDto>(context));
                    IdentityResult identityResult = this._visitPayUserApplicationService.Value.UpdateUser(visitPayUserDto, model.CanEditMailingAddress);
                    string customError = string.Join(@"\n", identityResult.Errors);
                    return this.Json(new Common.Web.Models.ResultMessage(identityResult.Succeeded, (identityResult.Succeeded) ? "Guarantor's personal information has been updated successfully." : customError.Contains("Mailing Address:") ? customError : this.GenericErrorMessage));
                }
                return this.Json(new Common.Web.Models.ResultMessage(false, "Nothing was changed."));
            }

            return this.Json(new Common.Web.Models.ResultMessage(false, this.GenericErrorMessage));
        }

        [HttpGet]
        [OverrideAuthorization]
        public ActionResult GetPaymentDueDayHistory()
        {
            return this.PartialView("_GuarantorPaymentDueDayHistory");
        }

        [HttpPost]
        [OverrideAuthorization]
        [ValidateAntiForgeryToken]
        public JsonResult GetPaymentDueDayHistory(int guarantorId, int page, int rows, string sidx, string sord)
        {
            GuarantorDto guarantorDto = this._guarantorApplicationService.Value.GetGuarantor(guarantorId);
            if (guarantorDto != null)
            {
                IList<GuarantorPaymentDueDayHistoryDto> paymentDueDayHistory = this._guarantorApplicationService.Value.GetGuarantorPaymentDueDayHistory(guarantorDto.User.VisitPayUserId);
                decimal total = Math.Ceiling((decimal)paymentDueDayHistory.Count / rows);
                int records = paymentDueDayHistory.Count;

                #region sort

                Func<GuarantorPaymentDueDayHistoryDto, object> orderByFunc = history => history.InsertDate;

                switch (sidx ?? string.Empty)
                {
                    case "InsertDate":
                        {
                            orderByFunc = history => history.InsertDate;
                            break;
                        }
                }

                bool isAscendingOrder = "asc".Equals(sord, StringComparison.InvariantCultureIgnoreCase);
                paymentDueDayHistory = ((isAscendingOrder) ? paymentDueDayHistory.OrderBy(orderByFunc) : paymentDueDayHistory.OrderByDescending(orderByFunc)).ToList();

                #endregion

                return this.Json(new
                {
                    PaymentDueDayHistory = Mapper.Map<List<GuarantorPaymentDueDayHistoryViewModel>>(paymentDueDayHistory).Skip((page - 1) * rows).Take(rows),
                    page,
                    total,
                    records
                });
            }
            return this.Json(new DataResult<string, bool> { Result = false, Data = "Unable to find guarantor payment due day change history" });
        }

        [HttpPost]
        public ActionResult ViewSecurityQuestions(int vpUserId, int vpGuarantorId)
        {
            IList<SecurityQuestionAnswerDto> securityQuestionAnswers = this._securityQuestionApplicationService.Value.GetUserSecurityQuestionAnswer(vpUserId);
            this._visitPayUserJournalEventApplicationService.Value.LogViewSecurityQuestionsEvent(this.CurrentUserId, vpGuarantorId, this.GetJournalEventHttpContextDto());
            IList<SecurityQuestionAnswerViewModel> securityQuestionAnswerViewModels = Mapper.Map<IList<SecurityQuestionAnswerViewModel>>(securityQuestionAnswers);
            foreach (SecurityQuestionAnswerViewModel securityQuestionAnswerViewModel in securityQuestionAnswerViewModels)
            {
                securityQuestionAnswerViewModel.VisitPayUserId = vpUserId;
            }
            return this.PartialView("_GuarantorSecurityQuestions", securityQuestionAnswerViewModels);
        }

        #region emulate user

        [HttpPost]
        [OverrideAuthorization]
        [VpAuthorize(Roles = VisitPayRoleStrings.Csr.CSR + "," + VisitPayRoleStrings.Csr.SupportAdmin + "," + VisitPayRoleStrings.Csr.SupportView + "," + VisitPayRoleStrings.Csr.UserAdmin)]
        [Common.Web.Attributes.SessionState(SessionStateBehavior.Required)]
        public JsonResult BeginEmulateUser(int vpGuarantorId)
        {
            this.SetTempData(EmulateVpGuarantorIdKey, vpGuarantorId);

            return this.Json(this.Url.Action("EmulateUser"));
        }
        
        [HttpGet]
        [OverrideAuthorization]
        [VpAuthorize(Roles = VisitPayRoleStrings.Csr.CSR + "," + VisitPayRoleStrings.Csr.SupportAdmin + "," + VisitPayRoleStrings.Csr.SupportView + "," + VisitPayRoleStrings.Csr.UserAdmin)]
        public ActionResult EmulateUser()
        {
            int id;

            object tempData = this.TempData[EmulateVpGuarantorIdKey];

            if (tempData == null || !int.TryParse(tempData.ToString(), out id))
            {
                return this.RedirectToAction("Index", "Home");
            }

            GuarantorDto guarantorDto = this._guarantorApplicationService.Value.GetGuarantor(id);
            if (guarantorDto != null && !guarantorDto.AccountClosedDate.HasValue)
            {
                string token = this._visitPayUserApplicationService.Value.GenerateEmulateToken(this.CurrentUserIdString, guarantorDto.User.Id, this.GetJournalEventHttpContextDto());
                if (!string.IsNullOrWhiteSpace(token))
                {
                    ClaimsIdentity identity = this.User.Identity as ClaimsIdentity;
                    if (identity != null)
                    {
                        foreach (Claim claim in identity.FindAll(c => c.Type.Equals(ClaimTypeEnum.EmulationToken.ToString(), StringComparison.OrdinalIgnoreCase)))
                        {
                            identity.RemoveClaim(claim);
                        }
                        identity.AddClaim(new Claim(ClaimTypeEnum.EmulationToken.ToString(), token));
                        IAuthenticationManager authenticationManager = this.HttpContext.GetOwinContext().Authentication;
                        authenticationManager.AuthenticationResponseGrant = new AuthenticationResponseGrant(new ClaimsPrincipal(identity), new AuthenticationProperties { IsPersistent = true });
                    }
                    return this.View(new EmulateUserViewModel
                    {
                        EmulateGuarantorId = id,
                        EmulateUserName = guarantorDto.User.DisplayFirstNameLastName,
                        Url = $"{this.ClientDto.AppGuarantorUrlHost}account/loginwithtoken?token={token}",
                        EmulateToken = token
                    });
                }
            }

            return this.RedirectToAction("Index", "Home");
        }

        #endregion

        #region visits

        private GuarantorVisitsSearchFilterViewModel GetGuarantorVisitsSearchFilter(GuarantorDto guarantorDto)
        {
            List<string> billingApplications = new List<string>
            {
                BillingApplicationConstants.HB,
                BillingApplicationConstants.PB
            };

            GuarantorVisitsSearchFilterViewModel model = new GuarantorVisitsSearchFilterViewModel
            {
                BillingApplications = billingApplications.Select(x => new SelectListItem
                {
                    Text = x,
                    Value = x
                }).ToList(),
                VisitPayUserId = guarantorDto.User.VisitPayUserId,
                VisitStates = this.GetDerivedVisitStates()
            };

            return model;
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult GuarantorVisits(GuarantorVisitsSearchFilterViewModel model, int page, int rows, string sidx, string sord)
        {
            //
            VisitFilterDto visitFilterDto = new VisitFilterDto
            {
                SortField = sidx,
                SortOrder = sord
            };
            Mapper.Map(model, visitFilterDto);

            // VisitStateIDs will not be filtered by this call since they are not VisitStateEnum values
            VisitResultsDto visitResultsDto = this._visitApplicationService.Value.GetVisits(model.VisitPayUserId, visitFilterDto, page, null);

            // Filter results by VisitStateIDs which are VisitStateDerivedEnum values
            IEnumerable<VisitDto> visits = visitResultsDto.Visits.Where(x =>
                visitFilterDto.VisitStateIds == null
                || visitFilterDto.VisitStateIds.Count == 0
                || visitFilterDto.VisitStateIds.Contains((int)x.CurrentVisitStateDerivedEnum)
                );

            //late paging required due to dto-level filter
            visits = visits
                .Skip(Math.Max(page - 1, 0) * rows)
                .Take(rows);

            List<GuarantorVisitsSearchResultViewModel> viewModel = Mapper.Map<List<GuarantorVisitsSearchResultViewModel>>(visits);

            bool isIvhAdmin = this.User.IsInRole(VisitPayRoleStrings.Miscellaneous.IvhAdmin);
            bool isBalanceTransferEnabled = this.IsFeatureEnabled(VisitPayFeatureEnum.BalanceTransfer);

            viewModel.ForEach(visit =>
            {
                visit.UnmatchEnabled = isIvhAdmin;
                visit.BalanceTransferEnabled = isBalanceTransferEnabled && isIvhAdmin;
            });

            //
            return this.Json(new
            {
                Visits = viewModel,
                page,
                total = Math.Ceiling((decimal)visitResultsDto.TotalRecords / rows),
                records = visitResultsDto.TotalRecords
            });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Common.Web.Attributes.SessionState(SessionStateBehavior.Required)]
        public async Task<ActionResult> GuarantorVisitsExport(GuarantorVisitsSearchFilterViewModel model, string sidx, string sord)
        {
            //
            VisitFilterDto visitFilterDto = new VisitFilterDto
            {
                SortField = sidx,
                SortOrder = sord
            };
            Mapper.Map(model, visitFilterDto);

            //
            byte[] bytes = await this._visitApplicationService.Value.ExportVisitsClientAsync(model.VisitPayUserId, visitFilterDto, this.User.Identity.GetUserName());
            this.SetTempData(GuarantorVisitsExportKey, bytes);

            //
            return this.Json(new ResultMessage(true, this.Url.Action("GuarantorVisitsExportDownload")));
        }

        [HttpGet]
        public ActionResult GuarantorVisitsExportDownload()
        {
            object tempData = this.TempData[GuarantorVisitsExportKey];
            if (tempData != null)
            {
                this.WriteExcelInline((byte[])tempData, "GuarantorVisits.xlsx");
            }

            return this.Json(new { }, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region visit transactions

        [HttpGet]
        public ActionResult GuarantorVisitTransactions(int id, int visitPayUserId)
        {
            VisitDto visitDto = this._visitApplicationService.Value.GetVisit(visitPayUserId, id);
            List<SelectListItem> transactionTypes = Enum.GetValues(typeof(VpTransactionTypeFilterEnum)).Cast<VpTransactionTypeFilterEnum>().Select(x => new SelectListItem
            {
                Text = x.ToString(),
                Value = ((int)x).ToString()
            }).ToList();

            GuarantorVisitTransactionsViewModel model = new GuarantorVisitTransactionsViewModel
            {
                SearchFilter = new GuarantorVisitTransactionsSearchFilterViewModel
                {
                    TransactionTypes = transactionTypes,
                    VisitId = visitDto.VisitId
                },
                SourceSystemKeyDisplay = visitDto.SourceSystemKeyDisplay,
                VisitBalance = visitDto.UnclearedBalance,
                VisitDescription = visitDto.VisitDescription,
                VisitBillingApplication = visitDto.BillingApplication
            };

            return this.PartialView("_GuarantorVisitTransactions", model);
        }

        [HttpGet]
        public ActionResult GuarantorVisitBalances()
        {
            return this.PartialView("_GuarantorVisitBalances");
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult GuarantorVisitTransactions(GuarantorVisitTransactionsSearchFilterViewModel model, int page, int rows, string sidx, string sord)
        {
            //
            VisitTransactionFilterDto visitTransactionFilterDto = new VisitTransactionFilterDto
            {
                SortField = sidx,
                SortOrder = sord
            };
            Mapper.Map(model, visitTransactionFilterDto);

            //
            VisitTransactionResultsDto visitTransactionResultsDto = this._visitTransactionApplicationService.Value.GetTransactions(model.VisitPayUserId, visitTransactionFilterDto, page, rows);

            return this.Json(new
            {
                Transactions = Mapper.Map<List<GuarantorVisitTransactionsSearchResultViewModel>>(visitTransactionResultsDto.VisitTransactions),
                page,
                total = Math.Ceiling((decimal)visitTransactionResultsDto.TotalRecords / rows),
                records = visitTransactionResultsDto.TotalRecords
            });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Common.Web.Attributes.SessionState(SessionStateBehavior.Required)]
        public async Task<ActionResult> GuarantorVisitTransactionsExport(GuarantorVisitTransactionsSearchFilterViewModel model, string sidx, string sord)
        {
            //
            VisitTransactionFilterDto visitTransactionFilterDto = new VisitTransactionFilterDto
            {
                SortField = sidx,
                SortOrder = sord
            };
            Mapper.Map(model, visitTransactionFilterDto);

            //
            byte[] bytes = await this._visitTransactionApplicationService.Value.ExportVisitTransactionsClientAsync(model.VisitPayUserId, visitTransactionFilterDto, this.User.Identity.GetUserName());
            this.SetTempData(GuarantorVisitTransactionsExportKey, bytes);

            //
            return this.Json(new ResultMessage(true, this.Url.Action("GuarantorVisitTransactionsExportDownload")));
        }

        [HttpGet]
        public ActionResult GuarantorVisitTransactionsExportDownload()
        {
            object tempData = this.TempData[GuarantorVisitTransactionsExportKey];
            if (tempData != null)
            {
                this.WriteExcelInline((byte[])tempData, "GuarantorVisitTransactions.xlsx");
            }

            return this.Json(new { }, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region visit status history

        [HttpGet]
        public ActionResult GuarantorVisitStateHistory(string sourceSystemKey)
        {
            return this.PartialView("_GuarantorVisitStateHistory", new GuarantorVisitStateHistorySearchFilterViewModel
            {
                VisitStates = this.GetVisitStates()
            });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult GuarantorVisitStateHistoryData(GuarantorVisitStateHistorySearchFilterViewModel model, int page, int rows, string sidx, string sord)
        {
            VisitStateHistoryFilterDto filterDto = new VisitStateHistoryFilterDto
            {
                SortField = sidx,
                SortOrder = sord
            };
            Mapper.Map(model, filterDto);

            VisitStateHistoryResultsDto resultsDto = this._visitStateHistoryApplicationService.Value.GetAllVisitStateHistory(model.VisitPayUserId, filterDto, page, rows, onlyChanges: true);

            return this.Json(new
            {
                VisitStateHistory = Mapper.Map<List<GuarantorVisitStateHistorySearchResultViewModel>>(resultsDto.VisitStateHistories),
                page,
                total = Math.Ceiling((decimal)resultsDto.TotalRecords / rows),
                records = resultsDto.TotalRecords
            });
        }

        #endregion

        #region visit aging history

        [HttpGet]
        public ActionResult GuarantorVisitAgingHistory()
        {
            return this.PartialView("_GuarantorVisitAgingHistory", new GuarantorVisitAgingUpdateViewModel());
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult GuarantorVisitAgingHistory(int visitId, int visitPayUserId)
        {
            VisitDto visitDto = this._visitApplicationService.Value.GetVisit(visitPayUserId, visitId);
            GuarantorDto guarantorDto = this._visitPayUserApplicationService.Value.GetGuarantorFromVisitPayUserId(visitPayUserId);

            GuarantorVisitAgingUpdateViewModel model = new GuarantorVisitAgingUpdateViewModel
            {
                CurrentAgingTier = visitDto.AgingCount,
                IsAccountClosed = guarantorDto == null || guarantorDto.AccountClosedDate.HasValue,
                SourceSystemKeyDisplay = visitDto.SourceSystemKeyDisplay,
                VisitId = visitDto.VisitId,
                VisitPayUserId = visitPayUserId
            };

            //TODO: add this confirm dialog
            // cms for confirm dialog on setting to max aging tier
            //CmsVersionDto cms = await this._contentApplicationService.Value.GetCurrentVersionAsync(CmsRegionEnum.AgeVisitToUncollectableConfirmation, true, additionalValues).ConfigureAwait(true);
            //model.MaxAgingTierContentBody = cms.ContentBody;
            //model.MasAgingTierContentTitle = cms.ContentTitle;

            //only age backwards
            //Business decision not to allow NotStatemented when the user has had communications VP-1408 (AgingTierEnum.Good in most cases implies a statement)
            model.AvailableAgingTiers.AddRange(Enum.GetValues(typeof(AgingTierEnum)).Cast<AgingTierEnum>().Where(x => visitDto.AgingCount > (int)x && x != AgingTierEnum.NotStatemented)
            .Select(x => new SelectListItem
            {
                Text = x.ToString().InsertSpaceBetweenLowerAndCapitalLetter(),
                Value = ((int)x).ToString()
            }).ToList());
            

            model.NewAgingTier = model.AvailableAgingTiers.Any() ? Convert.ToInt32(model.AvailableAgingTiers.First().Value) : 0;

            return this.Json(model);
        }

        [HttpPost]
        [RequireFeature(VisitPayFeatureEnum.FeatureVisitPayAgingIsEnabled)]
        [VpAuthorize(Roles = VisitPayRoleStrings.Csr.ResetVisitAging)]
        [ValidateAntiForgeryToken]
        public JsonResult GuarantorVisitAgingHistoryUpdate(GuarantorVisitAgingUpdateViewModel model)
        {
            VisitDto visit = this._visitApplicationService.Value.GetVisit(model.VisitPayUserId, model.VisitId);
            if (visit == null || visit.VPGuarantor.AccountClosedDate.HasValue)
            {
                return this.Json(new ResultMessage(false, "Can't find visit or account is closed"));
            }

            VisitAgingHistoryDto visitAgingHistoryDto = new VisitAgingHistoryDto
            {
                AgingTier = model.NewAgingTier,
                Visit = visit,
                VpGuarantorId = visit.VPGuarantor.VpGuarantorId,
                VisitPayUser = new VisitPayUserDto { VisitPayUserId = this.CurrentUserId },
                ChangeDescription = model.ChangeDescription
            };

            this._visitApplicationService.Value.SetVisitAgingCount(visitAgingHistoryDto);

            return this.Json(new ResultMessage(true, string.Empty));
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult GuarantorVisitAgingHistoryData(GuarantorVisitAgingHistorySearchFilterViewModel model, int page, int rows, string sidx, string sord)
        {
            VisitAgingHistoryFilterDto filterDto = Mapper.Map<VisitAgingHistoryFilterDto>(model);

            filterDto.SortField = sidx;
            filterDto.SortOrder = sord;

            VisitAgingHistoryResultsDto resultsDto = this._visitAgingHistoryApplicationService.Value.GetAllVisitAgingHistory(model.VisitPayUserId, filterDto, page, rows);

            return this.Json(new
            {
                VisitAgingHistory = Mapper.Map<List<GuarantorVisitAgingHistorySearchResultViewModel>>(resultsDto.VisitAgingHistories),
                page,
                total = Math.Ceiling((decimal)resultsDto.TotalRecords / rows),
                records = resultsDto.TotalRecords
            });
        }

        #endregion

        #region all transactions for guarantor

        private GuarantorTransactionsSearchFilterViewModel GetGuarantorTransactionsSearchFilter(GuarantorDto guarantorDto)
        {
            GuarantorTransactionsSearchFilterViewModel model = new GuarantorTransactionsSearchFilterViewModel
            {
                VisitPayUserId = guarantorDto.User.VisitPayUserId
            };

            this._visitTransactionApplicationService.Value.GetTransactionTypes().ToList().ForEach(x => model.TransactionTypes.Add(new SelectListItem { Text = x, Value = x }));

            return model;
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult GuarantorTransactions(GuarantorTransactionsSearchFilterViewModel model, int page, int rows, string sidx, string sord)
        {
            VisitTransactionFilterDto visitTransactionFilterDto = new VisitTransactionFilterDto
            {
                SortField = sidx,
                SortOrder = sord
            };
            Mapper.Map(model, visitTransactionFilterDto);

            //
            VisitTransactionResultsDto visitTransactionResultsDto = this._visitTransactionApplicationService.Value.GetTransactions(model.VisitPayUserId, visitTransactionFilterDto, page, rows);

            return this.Json(new
            {
                Transactions = Mapper.Map<List<GuarantorTransactionsSearchResultViewModel>>(visitTransactionResultsDto.VisitTransactions),
                page,
                total = Math.Ceiling((decimal)visitTransactionResultsDto.TotalRecords / rows),
                records = visitTransactionResultsDto.TotalRecords
            });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Common.Web.Attributes.SessionState(SessionStateBehavior.Required)]
        public async Task<ActionResult> GuarantorTransactionsExport(GuarantorTransactionsSearchFilterViewModel model, string sidx, string sord)
        {
            //
            VisitTransactionFilterDto visitTransactionFilterDto = new VisitTransactionFilterDto
            {
                SortField = sidx,
                SortOrder = sord
            };
            Mapper.Map(model, visitTransactionFilterDto);

            //
            byte[] bytes = await this._visitTransactionApplicationService.Value.ExportTransactionsClientAsync(model.VisitPayUserId, visitTransactionFilterDto, this.User.Identity.GetUserName());
            this.SetTempData(GuarantorTransactionsExportKey, bytes);

            //
            return this.Json(new ResultMessage(true, this.Url.Action("GuarantorTransactionsExportDownload")));
        }

        [HttpGet]
        public ActionResult GuarantorTransactionsExportDownload()
        {
            object tempData = this.TempData[GuarantorTransactionsExportKey];
            if (tempData != null)
            {
                this.WriteExcelInline((byte[])tempData, "GuarantorTransactions.xlsx");
            }

            return this.Json(new { }, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region finance plans

        private GuarantorFinancePlansSearchFilterViewModel GetGuarantorFinancePlansSearchFilter()
        {
            GuarantorFinancePlansSearchFilterViewModel model = new GuarantorFinancePlansSearchFilterViewModel
            {
                FinancePlanStatuses = this.GetGroupedFinancePlanStatuses()
            };

            return model;
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult GuarantorFinancePlans(GuarantorFinancePlansSearchFilterViewModel model, int page, int rows, string sidx, string sord)
        {
            FinancePlanFilterDto financePlanFilterDto = new FinancePlanFilterDto
            {
                SortField = sidx,
                SortOrder = sord
            };
            Mapper.Map(model, financePlanFilterDto);

            FinancePlanResultsDto financePlanResultsDto = this._financePlanApplicationService.Value.GetFinancePlans(model.VisitPayUserId, financePlanFilterDto, page, rows);

            List<GuarantorFinancePlansSearchResultViewModel> results = Mapper.Map<List<GuarantorFinancePlansSearchResultViewModel>>(financePlanResultsDto.FinancePlans);

            bool isManaged = this._guarantorFilterService.Value.IsManagedViewingManaged(model.CurrentVisitPayUserId, model.VisitPayUserId);

            bool isGuarantorCanceled = false;
            bool isOfflineGuarantor = false;
            FinancePlanDto firstFp = financePlanResultsDto.FinancePlans.FirstOrDefault();
            if (firstFp != null)
            {
                isGuarantorCanceled = firstFp.VpGuarantor.AccountClosedDate != null;
                isOfflineGuarantor = firstFp.VpGuarantor.IsOfflineGuarantor;
            }

            bool isClientUserInReconfigureRole = this.User.IsInRole(VisitPayRoleStrings.Csr.ReconfigureFP);
            bool isClientUserInCancelFpRole = this.User.IsInRole(VisitPayRoleStrings.Csr.CancelFP);

            foreach (GuarantorFinancePlansSearchResultViewModel x in results)
            {
                //Dont allow them to reconfigure a finance plan that is uncollectable.
                x.CanReconfigure = isClientUserInReconfigureRole && x.FinancePlanStatus != FinancePlanStatusEnum.UncollectableClosed;
                x.IsManaged = isManaged;
                x.IsGuarantorCanceled = isGuarantorCanceled;
                x.IsOfflineGuarantor = isOfflineGuarantor;
                x.CanCancelOriginatedFinancePlan = isClientUserInCancelFpRole;
            }

            return this.Json(new
            {
                FinancePlans = results,
                TotalCurrentBalance = financePlanResultsDto.TotalCurrentBalance.ToString("C", new NumberFormatInfo { CurrencySymbol = "$", CurrencyNegativePattern = 1 }),
                page,
                total = Math.Ceiling((decimal)financePlanResultsDto.TotalRecords / rows),
                records = financePlanResultsDto.TotalRecords
            });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Common.Web.Attributes.SessionState(SessionStateBehavior.Required)]
        public async Task<ActionResult> GuarantorFinancePlansExport(GuarantorFinancePlansSearchFilterViewModel model, string sidx, string sord)
        {
            //
            FinancePlanFilterDto financePlanFilterDto = new FinancePlanFilterDto
            {
                SortField = sidx,
                SortOrder = sord
            };
            Mapper.Map(model, financePlanFilterDto);

            //
            byte[] bytes = await this._financePlanApplicationService.Value.ExportFinancePlansClientAsync(model.VisitPayUserId, financePlanFilterDto, this.User.Identity.GetUserName());
            this.SetTempData(GuarantorFinancePlansExportKey, bytes);

            //
            return this.Json(new ResultMessage(true, this.Url.Action("GuarantorFinancePlansExportDownload")));
        }

        [HttpGet]
        public ActionResult GuarantorFinancePlansExportDownload()
        {
            object tempData = this.TempData[GuarantorFinancePlansExportKey];
            if (tempData != null)
            {
                this.WriteExcelInline((byte[])tempData, "GuarantorFinancePlans.xlsx");
            }

            return this.Json(new { }, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region finance plans visits

        [HttpGet]
        public ActionResult GuarantorFinancePlanVisits(int visitPayUserId, int financePlanId)
        {
            FinancePlanDto financePlanDto = this._financePlanApplicationService.Value.GetFinancePlan(visitPayUserId, financePlanId);
            FinancePlanDetailsViewModel model = Mapper.Map<FinancePlanDetailsViewModel>(financePlanDto);

            if (financePlanDto.TermsCmsVersionId.HasValue)
            {
                model.FinancePlanOfferId = financePlanDto.FinancePlanOffer.FinancePlanOfferId;
            }

            return this.PartialView("_GuarantorFinancePlanVisits", model);
        }

        #endregion

        #region support history

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult GuarantorSupportHistory(GuarantorSupportHistorySearchFilterViewModel model, int page, int rows, string sidx, string sord)
        {
            //
            SupportRequestFilterDto supportRequestFilterDto = new SupportRequestFilterDto
            {
                SortField = sidx,
                SortOrder = sord
            };
            Mapper.Map(model, supportRequestFilterDto);

            //
            SupportRequestResultsDto supportRequestResultsDto = this._supportRequestApplicationService.Value.GetSupportRequests(supportRequestFilterDto, page, rows, model.VisitPayUserId);

            List<SupportRequestSearchResultViewModel> supportRequests = Mapper.Map<List<SupportRequestSearchResultViewModel>>(supportRequestResultsDto.SupportRequests);
            supportRequests.ForEach(supportRequest => supportRequest.CanTakeAction = this.User.IsInRole(VisitPayRoleStrings.Csr.SupportAdmin));

            return this.Json(new
            {
                SupportRequests = supportRequests,
                page,
                total = Math.Ceiling((decimal)supportRequestResultsDto.TotalRecords / rows),
                records = supportRequestResultsDto.TotalRecords
            });
        }

        #endregion

        #region communications

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult GuarantorCommunications(GuarantorCommunicationsSearchFilterViewModel model, int page, int rows, string sidx, string sord)
        {
            CommunicationsFilterDto communicationsFilterDto = new CommunicationsFilterDto
            {
                SortField = sidx,
                SortOrder = sord
            };
            Mapper.Map(model, communicationsFilterDto);

            //
            CommunicationsResultsDto communicationsResultsDto = this._userEmailApplicationService.Value.GetCommunications(communicationsFilterDto, page, rows, model.VisitPayUserId);

            List<GuarantorCommunicationSearchResultViewModel> communications = Mapper.Map<List<GuarantorCommunicationSearchResultViewModel>>(communicationsResultsDto.Communications);

            return this.Json(new
            {
                Communications = communications,
                page,
                total = Math.Ceiling((decimal)communicationsResultsDto.TotalRecords / rows),
                records = communicationsResultsDto.TotalRecords
            });
        }

        [HttpPost]
        [Common.Web.Attributes.SessionState(SessionStateBehavior.Required)]
        public ActionResult GuarantorCommunicationEventsExport(int emailId, string sidx, string sord)
        {
            byte[] bytes = this._userEmailApplicationService.Value.ExportEmailEventsForEmailId(emailId, sidx, sord, this.User.Identity.GetUserName());
            this.SetTempData(CommunicationEventsExportKey, bytes);

            return this.Json(new ResultMessage(true, this.Url.Action("GuarantorCommunicationEventsExportDownload")));
        }

        [HttpGet]
        public ActionResult GuarantorCommunicationEventsExportDownload()
        {
            object tempData = this.TempData[CommunicationEventsExportKey];
            if (tempData != null)
            {
                this.WriteExcelInline((byte[])tempData, "CommunicationEvents.xlsx");
            }

            return this.Json(new { }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Common.Web.Attributes.SessionState(SessionStateBehavior.Required)]
        public async Task<ActionResult> GuarantorCommunicationsExport(GuarantorCommunicationsSearchFilterViewModel model, string sidx, string sord)
        {
            CommunicationsFilterDto communicationsFilterDto = new CommunicationsFilterDto
            {
                SortField = sidx,
                SortOrder = sord
            };
            Mapper.Map(model, communicationsFilterDto);

            //
            byte[] bytes = await this._userEmailApplicationService.Value.ExportCommunicationsAsync(communicationsFilterDto, model.VisitPayUserId, this.User.Identity.GetUserName());
            this.SetTempData(CommunicationsExportKey, bytes);

            return this.Json(new ResultMessage(true, this.Url.Action("GuarantorCommunicationsExportDownload")));
        }

        [HttpGet]
        public ActionResult GuarantorCommunicationsExportDownload()
        {
            object tempData = this.TempData[CommunicationsExportKey];
            if (tempData != null)
            {
                this.WriteExcelInline((byte[])tempData, "Communications.xlsx");
            }

            return this.Json(new { }, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult GuarantorCommunicationContent()
        {
            return this.PartialView("_GuarantorCommunicationContent");
        }

        [HttpGet]
        public ActionResult GuarantorCommunicationEvents()
        {
            return this.PartialView("_GuarantorCommunicationEvents");
        }

        [HttpGet]
        public ActionResult GuarantorChatHistoryDetails()
        {
            return this.PartialView("_GuarantorChatHistoryDetails");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Common.Web.Attributes.SessionState(SessionStateBehavior.Required)]
        public async Task<ActionResult> GuarantorChatHistoryOverviewExport(ChatThreadHistoryOverviewsDto chatHistoryOverviews, Guid guarantorVisitPayUserGuid)
        {
            //Export
            byte[] bytes = await this._chatHistoryExportApplicationService.Value.ExportChatHistoryOverviewAsync(chatHistoryOverviews, this.User.Identity.GetUserName(), guarantorVisitPayUserGuid);
            this.SetTempData(ChatHistoryOverviewExportKey, bytes);

            return this.Json(new ResultMessage(true, this.Url.Action("GuarantorChatHistoryOverviewExportDownload")));
        }

        [HttpGet]
        public ActionResult GuarantorChatHistoryOverviewExportDownload()
        {
            object tempData = this.TempData[ChatHistoryOverviewExportKey];
            if (tempData != null)
            {
                this.WriteExcelInline((byte[])tempData, "ChatHistory.xlsx");
            }

            return this.Json(new { }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Common.Web.Attributes.SessionState(SessionStateBehavior.Required)]
        public async Task<ActionResult> GuarantorChatThreadHistoryExport(ChatThreadHistoryDto chatThreadHistory, Guid guarantorVisitPayUserGuid)
        {
            //Export
            byte[] bytes = await this._chatHistoryExportApplicationService.Value.ExportChatThreadHistoryAsync(chatThreadHistory, this.User.Identity.GetUserName(), guarantorVisitPayUserGuid);
            this.SetTempData(ChatThreadHistoryExportKey, bytes);

            return this.Json(new ResultMessage(true, this.Url.Action("GuarantorChatThreadHistoryExportDownload")));
        }

        [HttpGet]
        public ActionResult GuarantorChatThreadHistoryExportDownload()
        {
            object tempData = this.TempData[ChatThreadHistoryExportKey];
            if (tempData != null)
            {
                this.WriteExcelInline((byte[])tempData, "ChatThreadHistory.xlsx");
            }

            return this.Json(new { }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult GuarantorCommunicationEvents(int emailId, int page, int rows, string sidx, string sord)
        {
            CommunicationEventResultsDto communicationEventResultsDto = this._userEmailApplicationService.Value.EmailEventsForEmailId(emailId, page, rows, sidx, sord);

            List<GuarantorEmailEventResultViewModel> communications = Mapper.Map<List<GuarantorEmailEventResultViewModel>>(communicationEventResultsDto.Events);

            return this.Json(new
            {
                communications = communications,
                page,
                total = Math.Ceiling((decimal)communicationEventResultsDto.TotalRecords / rows),
                records = communicationEventResultsDto.TotalRecords
            });
        }

        [HttpGet]
        public ActionResult GuarantorCommunicationResendModal(int emailId, int visitPayUserId, string subject, string to, bool isSms)
        {
            VisitPayUserDto visitPayUserDto = this._visitPayUserApplicationService.Value.FindById(visitPayUserId.ToString());

            GuarantorResendEmailViewModel model = new GuarantorResendEmailViewModel
            {
                VisitPayUserId = visitPayUserId,
                EmailId = emailId,
                ToEmail = isSms ? string.Empty : visitPayUserDto.Email,
                ToPhone = isSms ? to : string.Empty,
                Subject = subject,
                IsSms = isSms
            };

            return this.PartialView("_GuarantorEmailResendModal", model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult GuarantorCommunicationResend(GuarantorResendEmailViewModel model)
        {
            GuarantorDto guarantor = this._visitPayUserApplicationService.Value.GetGuarantorFromVisitPayUserId(model.VisitPayUserId);
            if (guarantor == null || guarantor.AccountClosedDate.HasValue)
            {
                return this.Json(new ResultMessage(false, "Invalid or closed account"));
            }

            string toAddress = model.IsSms ? model.ToPhone : model.ToEmail;
            EmailActionResultDto result = this._userEmailApplicationService.Value.ResendCommunication(model.EmailId, toAddress, guarantor.VpGuarantorId, this.CurrentUserId);
            return this.Json(result);
        }

        #endregion

        #region statements

        private StatementListViewModel GetStatements(int guarantorVisitPayUserId)
        {
            // statements for guarantor visit pay user
            List<StatementDto> statementsDto = this._statementApplicationService.Value.GetStatements(guarantorVisitPayUserId)
                .OrderByDescending(x => x.StatementDate).ToList();
            List<FinancePlanStatementDto> financePlanStatementDtos = this._statementApplicationService.Value.GetFinancePlanStatements(guarantorVisitPayUserId)
                .Where(x => x.StatementVersion != VpStatementVersionEnum.Vp2)
                .OrderByDescending(x => x.StatementDate).ToList();

            // model
            StatementListViewModel model = new StatementListViewModel
            {
                FinancePlanStatements = Mapper.Map<List<StatementListItemViewModel>>(financePlanStatementDtos),
                VisitBalanceSummaryStatements = Mapper.Map<List<StatementListItemViewModel>>(statementsDto)
            };

            // no statements
            if (!model.HasStatements)
            {
                GuarantorDto guarantorDto = this._visitPayUserApplicationService.Value.GetGuarantorFromVisitPayUserId(guarantorVisitPayUserId);
                model.NextStatementDate = guarantorDto.NextStatementDate.HasValue ? guarantorDto.NextStatementDate.Value.ToString(Format.MonthDayYearFormat) : string.Empty;
            }

            return model;
        }

        [HttpGet]
        public PartialViewResult Statements(int guarantorVisitPayUserId)
        {
            return this.PartialView("_GuarantorStatements", this.GetStatements(guarantorVisitPayUserId));
        }

        #endregion

        #region consolidation

        [HttpPost]
        [ValidateAntiForgeryToken]
        [RequireFeature(VisitPayFeatureEnum.Consolidation)]
        public PartialViewResult GuarantorConsolidations(int guarantorVisitPayUserId)
        {
            IEnumerable<ConsolidationGuarantorDto> managed = this._consolidationGuarantorApplicationService.Value.GetAllManagedGuarantors(guarantorVisitPayUserId);
            IEnumerable<ConsolidationGuarantorDto> managing = this._consolidationGuarantorApplicationService.Value.GetAllManagingGuarantors(guarantorVisitPayUserId);

            GuarantorConsolidationsViewModel model = new GuarantorConsolidationsViewModel
            {
                ManagedGuarantors = new List<ConsolidationGuarantorViewModel>(),
                ManagingGuarantors = new List<ConsolidationGuarantorViewModel>()
            };

            managed.ToList().ForEach(guarantor =>
            {
                ConsolidationGuarantorViewModel vm = new ConsolidationGuarantorViewModel(this.ClientDto);
                Mapper.Map(guarantor, vm);
                model.ManagedGuarantors.Add(vm);
            });

            managing.ToList().ForEach(guarantor =>
            {
                ConsolidationGuarantorViewModel vm = new ConsolidationGuarantorViewModel(this.ClientDto);
                Mapper.Map(guarantor, vm);
                model.ManagingGuarantors.Add(vm);
            });

            return this.PartialView("_GuarantorConsolidations", model);

        }

        #endregion

        #region balance transfer

        [HttpGet]
        [OverrideAuthorization]
        [VpAuthorize(Roles = VisitPayRoleStrings.Miscellaneous.IvhAdmin)]
        [RequireFeature(VisitPayFeatureEnum.BalanceTransfer)]
        public PartialViewResult BalanceTransferStatusHistory()
        {
            return this.PartialView("_GuarantorVisitBalanceTransferStatusHistory", new GuarantorVisitBalanceTransferStatusUpdateViewModel());
        }

        [HttpPost]
        [OverrideAuthorization]
        [VpAuthorize(Roles = VisitPayRoleStrings.Miscellaneous.IvhAdmin)]
        [RequireFeature(VisitPayFeatureEnum.BalanceTransfer)]
        public JsonResult BalanceTransferStatusHistory(int guarantorVisitPayUserId, int visitId)
        {
            VisitDto visitDto = this._visitApplicationService.Value.GetVisit(guarantorVisitPayUserId, visitId);
            GuarantorVisitBalanceTransferStatusUpdateViewModel model = new GuarantorVisitBalanceTransferStatusUpdateViewModel
            {
                CanSetBalanceTransferStatus = this.User.IsInRole(VisitPayRoleStrings.Miscellaneous.IvhAdmin),
                CanSetEligible = visitDto.BalanceTransferStatus != null && (BalanceTransferStatusEnum)visitDto.BalanceTransferStatus.BalanceTransferStatusId == BalanceTransferStatusEnum.Ineligible,
                CanSetIneligible = true,
                VisitSourceSystemKey = visitDto.SourceSystemKeyDisplay
            };

            return this.Json(model);
        }

        [HttpPost]
        [OverrideAuthorization]
        [VpAuthorize(Roles = VisitPayRoleStrings.Miscellaneous.IvhAdmin)]
        [RequireFeature(VisitPayFeatureEnum.BalanceTransfer)]
        [ValidateAntiForgeryToken]
        public JsonResult BalanceTransferStatusHistoryGrid(GridSearchModel<GuarantorVisitBalanceTransferStatusSearchFilterViewModel> filterModel)
        {
            BalanceTransferStatusHistoryResultsDto resultsDto = this._balanceTransferApplicationService.Value.GetBalanceTransferStatusHistory(Mapper.Map<BalanceTransferStatusHistoryFilterDto>(filterModel));

            GridResultsModel<GuarantorVisitBalanceTransferStatusSearchResultViewModel> model = new GridResultsModel<GuarantorVisitBalanceTransferStatusSearchResultViewModel>(filterModel.Page, filterModel.Rows, resultsDto.TotalRecords)
            {
                Results = Mapper.Map<List<GuarantorVisitBalanceTransferStatusSearchResultViewModel>>(resultsDto.BalanceTransferStatusHistories)
            };

            return this.Json(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [OverrideAuthorization]
        [VpAuthorize(Roles = VisitPayRoleStrings.Miscellaneous.IvhAdmin)]
        [RequireFeature(VisitPayFeatureEnum.BalanceTransfer)]
        public ActionResult SetBalanceTransferStatusIneligible(int visitId, int visitPayUserId, string notes)
        {
            return this.SetBalanceTransferStatus(visitId, visitPayUserId, BalanceTransferStatusEnum.Ineligible, notes);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [OverrideAuthorization]
        [VpAuthorize(Roles = VisitPayRoleStrings.Miscellaneous.IvhAdmin)]
        [RequireFeature(VisitPayFeatureEnum.BalanceTransfer)]
        public ActionResult SetBalanceTransferStatusEligible(int visitId, int visitPayUserId, string notes)
        {
            return this.SetBalanceTransferStatus(visitId, visitPayUserId, BalanceTransferStatusEnum.Eligible, notes);
        }

        private ActionResult SetBalanceTransferStatus(int visitId, int visitPayUserId, BalanceTransferStatusEnum balanceTransferStatus, string notes)
        {
            GuarantorDto targetGuarantorDto = this._guarantorApplicationService.Value.GetGuarantor(this._guarantorApplicationService.Value.GetGuarantorId(visitPayUserId));
            VisitDto visitDto = this._visitApplicationService.Value.GetVisit(visitPayUserId, visitId);
            FinancePlanDto financePlanDto = this._financePlanApplicationService.Value.GetFinancePlanWithVisitId(visitId);

            if (targetGuarantorDto == null)
            {
                return this.Json(new ResultMessage(false, ""));
            }

            this._balanceTransferApplicationService.Value.SetVisitBalanceTransferStatus(visitId, visitPayUserId, this.CurrentUserId, notes, (int)balanceTransferStatus);

            if (balanceTransferStatus == BalanceTransferStatusEnum.Eligible)
            {
                this._visitPayUserJournalEventApplicationService.Value.LogClientSetBalanceTransferStatus(
                    targetGuarantorDto.VpGuarantorId,
                    visitPayUserId,
                    this.CurrentUserId,
                    financePlanDto?.FinancePlanId,
                    financePlanDto?.CustomizedVisitPayUserId,
                    visitDto.VisitId,
                    visitDto.HsCurrentBalance,
                    notes,
                    Mapper.Map<JournalEventHttpContextDto>(System.Web.HttpContext.Current));
            }
            else if (balanceTransferStatus == BalanceTransferStatusEnum.Ineligible)
            {
                this._visitPayUserJournalEventApplicationService.Value.LogClientRemovedBalanceTransferStatus(
                    targetGuarantorDto.VpGuarantorId,
                    visitPayUserId,
                    this.CurrentUserId,
                    financePlanDto?.FinancePlanId,
                    financePlanDto?.CustomizedVisitPayUserId,
                    visitDto.VisitId,
                    visitDto.HsCurrentBalance,
                    notes,
                    Mapper.Map<JournalEventHttpContextDto>(System.Web.HttpContext.Current));
            }

            return this.Json(new ResultMessage(true, ""));
        }

        #endregion

        #endregion

        #region guarantor account record counts

        public JsonResult CommunicationsTotals(GuarantorCommunicationsSearchFilterViewModel model)
        {
            return this.Json(this._userEmailApplicationService.Value.GetCommunicationsTotals(model.VisitPayUserId, Mapper.Map<CommunicationsFilterDto>(model)));
        }

        public JsonResult FinancePlanTotals(GuarantorFinancePlansSearchFilterViewModel model)
        {
            return this.Json(this._financePlanApplicationService.Value.GetFinancePlanTotals(model.VisitPayUserId, Mapper.Map<FinancePlanFilterDto>(model)));
        }

        public JsonResult StatementTotals(int guarantorVisitPayUserId)
        {
            return this.Json(this._statementApplicationService.Value.GetStatementTotals(guarantorVisitPayUserId));
        }

        public JsonResult SupportRequestTotals(GuarantorSupportHistorySearchFilterViewModel model)
        {
            return this.Json(this._supportRequestApplicationService.Value.GetSupportRequestTotals(model.VisitPayUserId, Mapper.Map<SupportRequestFilterDto>(model)));
        }

        public JsonResult TransactionTotals(GuarantorTransactionsSearchFilterViewModel model)
        {
            return this.Json(this._visitTransactionApplicationService.Value.GetTransactionTotals(model.VisitPayUserId, Mapper.Map<VisitTransactionFilterDto>(model)));
        }

        public JsonResult VisitTotals(GuarantorVisitsSearchFilterViewModel model)
        {
            return this.Json(this._visitApplicationService.Value.GetVisitTotals(model.VisitPayUserId, Mapper.Map<VisitFilterDto>(model)));
        }

        #endregion

        #region Helpers

        private JournalEventHttpContextDto GetJournalEventHttpContextDto()
        {
            return Mapper.Map<JournalEventHttpContextDto>(System.Web.HttpContext.Current);
        }

        #endregion

    }
}
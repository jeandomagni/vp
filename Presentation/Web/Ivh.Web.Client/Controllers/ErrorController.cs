﻿namespace Ivh.Web.Client.Controllers
{
    using System;
    using System.Web.Mvc;
    using Common.Web.Controllers;
    using Common.Web.Interfaces;

    public class ErrorController : BaseErrorController
    {
        private const string ErrorFormatString = "event: {0}, jqxhr: {1}, settings: {2}, thrownError: {3}";

        public ErrorController(
            Lazy<IBaseControllerService> baseControllerService)
            : base(baseControllerService)
        {
        }

        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult LogError(string ajaxEvent, string jqxhr, string settings, string thrownError)
        {
            try
            {
                this.Logger.Value.Fatal(() => string.Format(ErrorFormatString, ajaxEvent, jqxhr, settings, thrownError));
            }
            catch
            {
                // just in case
            }
            return new EmptyResult();
        }
    }
}
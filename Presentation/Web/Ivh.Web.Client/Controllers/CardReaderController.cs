﻿namespace Ivh.Web.Client.Controllers
{
    using System;
    using System.Web.Mvc;
    using Attributes;
    using AutoMapper;
    using Common.EventJournal;
    using Common.VisitPay.Constants;
    using Common.VisitPay.Enums;
    using Common.VisitPay.Strings;
    using Common.Web.Cookies;
    using Common.Web.HtmlHelpers;
    using Common.Web.Interfaces;
    using Domain.Settings.Enums;
    using Ivh.Application.Core.Common.Interfaces;
    using Ivh.Application.Monitoring.Common.Interfaces;
    using Ivh.Common.Web.Filters;
    using Models.CardReader;
    
    [VpAuthorize(Roles = VisitPayRoleStrings.System.Client + "," + VisitPayRoleStrings.Admin.Administrator)]
    public class CardReaderController : BaseController
    {
        private readonly Lazy<IThirdPartyKeyApplicationService> _thirdPartyKeyApplicationService;
        private readonly Lazy<IThirdPartyValueApplicationService> _thirdPartyValueApplicationService;

        public CardReaderController(
            Lazy<IBaseControllerService> baseControllerService,
            Lazy<IThirdPartyKeyApplicationService> thirdPartyKeyApplicationService,
            Lazy<IThirdPartyValueApplicationService> thirdPartyValueApplicationService) : base(baseControllerService)
        {
            this._thirdPartyKeyApplicationService = thirdPartyKeyApplicationService;
            this._thirdPartyValueApplicationService = thirdPartyValueApplicationService;
        }
        
        [HttpGet]
        public ActionResult GetCardReaderDeviceKeyModal()
        {
            return this.PartialView("_CardReaderDeviceKeyModal", new CardReaderDeviceKeyViewModel());
        }
        
        [HttpPost]
        [RequireFeature(VisitPayFeatureEnum.FeatureCardReaderIsEnabled)]
        public ActionResult SetCardReaderDeviceKey(CardReaderDeviceKeyViewModel model)
        {
            if (this.IsDeviceKeyAllowed(model.CardReaderDeviceKey) && this.IsUserAccoutAllowed(model.CardReaderUserAccount))
            {
                this._thirdPartyValueApplicationService.Value.SetCardReaderDeviceKey(model.CardReaderDeviceKey, this.CurrentUserId, Mapper.Map<JournalEventHttpContextDto>(System.Web.HttpContext.Current));
                this._thirdPartyValueApplicationService.Value.SetCardReaderUserAccount(model.CardReaderUserAccount, this.CurrentUserId, Mapper.Map<JournalEventHttpContextDto>(System.Web.HttpContext.Current));
            }

            if (Request.IsAjaxRequest())
            {
                return this.Json(new
                {
                    Success = true,
                    DeviceKey = model.CardReaderDeviceKey,
                    Message = "Something went wrong"
                });
            }

            return RedirectToAction(nameof(ManageCardReaderDevice));
        }

        [HttpPost]
        [RequireFeature(VisitPayFeatureEnum.FeatureCardReaderIsEnabled)]
        public JsonResult IsCardReaderDeviceKeyAllowed(string cardReaderDeviceKey)
        {
            return this.Json(this.IsDeviceKeyAllowed(cardReaderDeviceKey));
        }

        private bool IsDeviceKeyAllowed(string cardReaderDeviceKey)
        {
            return this._thirdPartyKeyApplicationService.Value.IsCardReaderDeviceKeyAllowed(cardReaderDeviceKey);
        }

        private bool IsUserAccoutAllowed(string cardReaderUserAccount)
        {
            return this._thirdPartyKeyApplicationService.Value.IsCardReaderUserAccountAllowed(cardReaderUserAccount);
        }

        [HttpPost]
        [RequireFeature(VisitPayFeatureEnum.FeatureCardReaderIsEnabled)]
        public JsonResult IsCardReaderUserAccountAllowed(string cardReaderUserAccount)
        {
            return this.Json(this.IsUserAccoutAllowed(cardReaderUserAccount));
        }

        [HttpPost]
        public JsonResult GetCardReaderDeviceParameters()
        {
            if (!this.IsFeatureEnabled(VisitPayFeatureEnum.FeatureCardReaderIsEnabled))
            {
                return this.Json(false);
            }

            string deviceKey = this._thirdPartyValueApplicationService.Value.GetCardReaderDeviceValue(this.CurrentUserId);
            string userAccount = this._thirdPartyValueApplicationService.Value.GetCardReaderUserAccountValue(this.CurrentUserId);
            string blackoutStartUrl = string.IsNullOrEmpty(this.ApplicationSettingsService.Value.TraceApiBlackoutStartUrl.Value) ? $"{this.Request.Url?.Scheme}://{this.Request.Url?.Authority}/Trace/BlackoutStart" : this.ApplicationSettingsService.Value.TraceApiBlackoutStartUrl.Value;
            string blackoutStopUrl = string.IsNullOrEmpty(this.ApplicationSettingsService.Value.TraceApiBlackoutStopUrl.Value) ? $"{this.Request.Url?.Scheme}://{this.Request.Url?.Authority}/Trace/BlackoutStop" : this.ApplicationSettingsService.Value.TraceApiBlackoutStopUrl.Value;
            string getDeviceStateUrl = string.IsNullOrEmpty(this.ApplicationSettingsService.Value.TraceApiGetDeviceStateUrl.Value) ? $"{this.Request.Url?.Scheme}://{this.Request.Url?.Authority}/Trace/GetDeviceState" : this.ApplicationSettingsService.Value.TraceApiGetDeviceStateUrl.Value;
            string traceClientName = this.ClientDto.TraceApiClientName;
            string securePanApiUrl = string.Join("/", this.ApplicationSettingsService.Value.GetUrl(UrlEnum.SecurePanApiUrl).AbsoluteUri.TrimEnd('/'), "api/v1/Device");
            string apiKey = string.Empty;

            // todo: I think this is closer to prod version, but securepan side isn't currently handling it.
            //string apiKey = $"Digest: {SecurePanAuthorization.GenerateAuthToken(this.ApplicationSettingsService.Value)}";

            // todo: TEST ONLY - this needs to get removed before release, waiting on VP-5741 but unblocking basic testing
            apiKey = "PaymentAPI-THR-SecurePan-ApiKey";

            return this.Json(new
            {
                DeviceKey = string.IsNullOrWhiteSpace(deviceKey) ? null : deviceKey,
                ApiUrl = securePanApiUrl,
                ApiKey = apiKey,
                UserAccount = userAccount,
                BlackoutStartUrl = blackoutStartUrl,
                BlackoutStopUrl = blackoutStopUrl,
                GetDeviceStateUrl = getDeviceStateUrl,
                TraceClientName = traceClientName
            });

        }

        [HttpPost]
        [RequireFeature(VisitPayFeatureEnum.FeatureCardReaderIsEnabled)]
        public void BlackoutStart(string userAccount, int responseTimeMs, bool success = true)
        {
            this.MetricsProvider.Value.Gauge(success ? Metrics.Increment.Trace.BlackoutStartSuccessResponseTime : Metrics.Increment.Trace.BlackoutStartFailureResponseTime, responseTimeMs);
            this.MetricsProvider.Value.Increment(success ? Metrics.Increment.Trace.BlackoutStartSuccess : Metrics.Increment.Trace.BlackoutStartFailure);
            if (!success)
            {
                this.Logger.Value.Info(() => $"Blackout START failed for user account {userAccount} - response time: {responseTimeMs} ms");
            }
        }

        [HttpPost]
        [RequireFeature(VisitPayFeatureEnum.FeatureCardReaderIsEnabled)]
        public void BlackoutStop(string userAccount, int responseTimeMs, bool success = true)
        {
            this.MetricsProvider.Value.Gauge(success ? Metrics.Increment.Trace.BlackoutStopSuccessResponseTime: Metrics.Increment.Trace.BlackoutStopFailureResponseTime, responseTimeMs);
            this.MetricsProvider.Value.Increment(success ? Metrics.Increment.Trace.BlackoutStopSuccess : Metrics.Increment.Trace.BlackoutStopFailure);
            if (!success)
            {
                this.Logger.Value.Info(() => $"Blackout STOP failed for user account {userAccount} - response time: {responseTimeMs} ms");
            }
        }

        [RequireFeature(VisitPayFeatureEnum.FeatureCardReaderIsEnabled)]
        public ActionResult ManageCardReaderDevice()
        {
            CardReaderDeviceKeyViewModel model = new CardReaderDeviceKeyViewModel
            {
                CardReaderUserAccount = this._thirdPartyValueApplicationService.Value.GetCardReaderUserAccountValue(this.CurrentUserId),
                CardReaderDeviceKey = this._thirdPartyValueApplicationService.Value.GetCardReaderDeviceValue(this.CurrentUserId)
            };
            return View(model);
        }

    }
}
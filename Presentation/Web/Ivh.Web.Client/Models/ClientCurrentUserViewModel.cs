﻿namespace Ivh.Web.Client.Models
{
    using System.Linq;
    using Common.Web.Models.Shared;

    public class ClientCurrentUserViewModel : CurrentUserViewModel
    {
        public int? UnreadMessagesFromClient { get; set; }
        public int? UnreadMessagesToClient { get; set; }
        public int? UnreadMessagesToClientAll { get; set; }
        public int? UnreadMessagesGuarantor { get; set; }

        public int UnreadTotal
        {
            get
            {
                return new[]
                {
                    this.UnreadMessagesFromClient,
                    this.UnreadMessagesToClientAll,
                    this.UnreadMessagesToClient,
                    this.UnreadMessagesGuarantor
                }.Sum(x => x.GetValueOrDefault(0));
            }
        }

        public bool MessagesIsVisible
        {
            get
            {
                return new[]
                {
                    this.UnreadMessagesFromClient,
                    this.UnreadMessagesToClientAll,
                    this.UnreadMessagesToClient,
                    this.UnreadMessagesGuarantor
                }.Any(x => x.HasValue);
            }
        }
    }
}
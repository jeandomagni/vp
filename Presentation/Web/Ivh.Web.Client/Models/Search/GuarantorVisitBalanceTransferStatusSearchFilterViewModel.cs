﻿namespace Ivh.Web.Client.Models.Search
{
    public class GuarantorVisitBalanceTransferStatusSearchFilterViewModel
    {
        public int GuarantorVisitPayUserId { get; set; }
        public int VisitId { get; set; }
    }
}
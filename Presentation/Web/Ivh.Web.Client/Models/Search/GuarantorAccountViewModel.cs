namespace Ivh.Web.Client.Models.Search
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.Linq;
    using System.Web.Mvc;
    using Common.Web.Attributes;
    using Common.Web.Models.Payment;
    using Common.Web.Models.Statement;
    using Common.Base.Utilities.Helpers;
    using Common.VisitPay.Constants;
    using Common.VisitPay.Enums;
    using Common.VisitPay.Strings;
    using Common.Web.Models.Communication;
    using Ivh.Common.Base.Utilities.Extensions;

    public class GuarantorAccountViewModel
    {
        public GuarantorAccountViewModel()
        {
            this.PaymentMethods = new List<GuarantorAccountPaymentMethodViewModel>();
            this.GuarantorVisitsSearchFilter = new GuarantorVisitsSearchFilterViewModel();
            this.GuarantorTransactionsSearchFilter = new GuarantorTransactionsSearchFilterViewModel();
            this.Statements = new StatementListViewModel();
            this.VisitPayUserCommunicationPreferences = new List<VisitPayUserCommunicationPreferenceViewModel>();
        }

        // from guarantor
        public int VpGuarantorId { get; set; }

        public int VpGuarantorTypeId { get; set; }

        public bool ClientSupportsOffline { get; set; }

        public DateTime? NextStatementDate { get; set; }

        public DateTime RegistrationDate { get; set; }

        public DateTime? AccountClosedDate { get; set; }

        public DateTime? AccountCancellationDate { get; set; }

        public DateTime? AccountReactivationDate { get; set; }

        public bool UseAutoPay { get; set; }

        // from guarantor.user
        [Display(Name = "Address", Prompt = "Street or PO Box")]
        [Required(ErrorMessage = "Street or PO Box is required.")]
        public string UserAddressStreet1 { get; set; }

        [Display(Prompt = "Apt, Suite, or Building #")]
        public string UserAddressStreet2 { get; set; }

        public string UserAddress
        {
            get
            {
                List<string> address = new List<string>();
                if (!string.IsNullOrEmpty(this.UserAddressStreet1))
                {
                    address.Add(this.UserAddressStreet1);
                }

                if (!string.IsNullOrEmpty(this.UserAddressStreet2))
                {
                    address.Add(this.UserAddressStreet2);
                }

                return string.Join(", ", address);
            }
        }

        [Display(Name = "City", Prompt = "City")]
        [Required(ErrorMessage = "City is required.")]
        public string UserCity { get; set; }

        [Display(Name = "Notification Email", Prompt = "Notification Email")]
        [Required(ErrorMessage = "Notification Email is required."), RegularExpressionEmailAddress(TextRegionConstants.NotificationEmailInvalid)]
        [DataType(DataType.EmailAddress)]
        public string UserEmail { get; set; }

        public string UserEmailOriginal { get; set; }

        [Display(Name = "Re-enter Notification Email", Prompt = "Re-enter Notification Email")]
        [DataType(DataType.EmailAddress)]
        [CompareToIfNotEqual("UserEmail", "UserEmailOriginal", "UserEmail", false, TextRegionConstants.NotificationEmailsDoNotMatch)]
        public string UserEmailConfirm { get; set; }

        [Display(Name = "Name", Prompt = "First Name")]
        [Required(ErrorMessage = "First Name is required.")]
        public string UserFirstName { get; set; }

        [Display(Prompt = "Last Name")]
        [Required(ErrorMessage = "Last Name is required.")]
        public string UserLastName { get; set; }

        [Display(Prompt = "Middle Name")]
        public string UserMiddleName { get; set; }

        [Display(Name = "Phone (Primary)", Prompt = "Phone (Primary)")]
        [Required(ErrorMessage = "Primary Phone is required.")]
        public string UserPhoneNumber { get; set; }

        [Required(ErrorMessage = "Primary Phone Type is required.")]
        public string UserPhoneNumberType { get; set; }

        [Display(Name = "Phone (Secondary)", Prompt = "Phone (Secondary)")]
        [DataType(DataType.PhoneNumber)]
        public string UserPhoneNumberSecondary { get; set; }

        [ConditionalRequired("PersonalInformation", "PhoneNumberSecondary", ErrorMessage = "Secondary Phone Type is required.")]
        public string UserPhoneNumberSecondaryType { get; set; }

        [Display(Name = "User Display Language", Prompt = "User Display Language")]
        public string Locale { get; set; }

        public string UserState { get; set; }

        public string UserUserName { get; set; }

        public string UserNameDisplay => this.IsOfflineUser ? "N/A" : this.UserUserName;

        public string UserZip { get; set; }

        public int UserVisitPayUserId { get; set; }

        public Guid? UserVisitPayUserGuid { get; set; }

        public int GracePeriod { get; set; }

        public int PaymentDueDayChanges { get; set; }

        // payment methods
        public IList<GuarantorAccountPaymentMethodViewModel> PaymentMethods { get; set; }

        // statement
        public DateTime? LastStatementDate { get; set; }

        public DateTime? PaymentDueDate { get; set; }

        // login summary
        public DateTime? LastLoginDate { get; set; }

        // consolidation
        public GuarantorConsolidationStatusEnum GuarantorConsolidationStatus { get; set; }

        // display
        public bool IsConsolidated => this.GuarantorConsolidationStatus != GuarantorConsolidationStatusEnum.NotConsolidated;

        public string GuarantorConsolidationStatusDisplay
        {
            get
            {
                //per VPNG-19038 Consolidation Status - will always be N/A
                if (this.IsOfflineUser)
                {
                    return "N/A";
                }

                switch (this.GuarantorConsolidationStatus)
                {
                    case GuarantorConsolidationStatusEnum.Managed:
                    case GuarantorConsolidationStatusEnum.PendingManaged:
                        return "Managed";
                    case GuarantorConsolidationStatusEnum.Managing:
                    case GuarantorConsolidationStatusEnum.PendingManaging:
                        return "Managing";
                    default:
                        return "N/A";
                }
            }
        }

        public string LastLoginDateDisplay => this.LastLoginDate?.ToString(Format.DateTimeFormat) ?? "N/A";

        public string LastStatementDateDisplay => this.LastStatementDate?.ToString(Format.DateFormat) ?? "N/A";

        public string NextStatementDateDisplay => this.NextStatementDate?.ToString(Format.DateFormat) ?? "N/A";

        public string PaymentDueDateDisplay => this.PaymentDueDate?.ToString(Format.DateFormat) ?? "N/A";

        public string PrimaryPaymentMethodDisplay
        {
            get
            {
                GuarantorAccountPaymentMethodViewModel primaryMethod = this.PaymentMethods.FirstOrDefault(x => x.IsPrimary);
                return primaryMethod == null ? "N/A" : primaryMethod.DisplayText;
            }
        }

        public string RegistrationDateDisplay => this.RegistrationDate.ToString(Format.DateFormat);

        public string UserFullNameDisplay => FormatHelper.FullNameMiddleInitial(this.UserFirstName, this.UserMiddleName, this.UserLastName);

        public string UserPhoneNumberSecondaryDisplay => !string.IsNullOrWhiteSpace(this.UserPhoneNumberSecondary) ? this.UserPhoneNumberSecondary : "N/A";

        public bool IsOfflineUser { get; set; }

        // search filters
        public GuarantorFinancePlansSearchFilterViewModel GuarantorFinancePlansSearchFilter { get; set; }

        public GuarantorTransactionsSearchFilterViewModel GuarantorTransactionsSearchFilter { get; set; }

        public GuarantorVisitsSearchFilterViewModel GuarantorVisitsSearchFilter { get; set; }

        public GuarantorSupportHistorySearchFilterViewModel GuarantorSupportHistoryFilter { get; set; }

        public GuarantorChatHistorySearchFilterViewModel GuarantorChatHistoryFilter { get; set; }

        public GuarantorCommunicationsSearchFilterViewModel GuarantorCommunicationsFilter { get; set; }

        public MyPaymentsViewModel GuarantorPaymentsViewModel { get; set; }

        public StatementListViewModel Statements { get; set; }

        public IList<VisitPayUserCommunicationPreferenceViewModel> VisitPayUserCommunicationPreferences { get; set; }


        public string GetCommunicationPreferences 
        {
            get {

                List<string> communicationPreferences = this.VisitPayUserCommunicationPreferences?.Select(x => x.CommunicationMethodId)
                    .Distinct()
                    .Select(x => ((CommunicationMethodEnum) x).GetDescription())
                    .ToList();

                if (!string.IsNullOrWhiteSpace(this.UserEmail) && !communicationPreferences.Contains(CommunicationMethodEnumDescription.Email))
                {
                    communicationPreferences.Add(CommunicationMethodEnumDescription.Email);
                }

                if (communicationPreferences.Any())
                {
                    return string.Join(" / ", communicationPreferences);
                }

                return CommunicationMethodEnumDescription.None;
            }
        }

        public string GetAutoPayPreference => (this.UseAutoPay) ? TextRegionConstants.AutoPay :TextRegionConstants.NonAutoPay;
        
        public decimal TotalBalance { get; set; }

        public bool HasPendingPayments { get; set; }

        public DateTime? LastPaymentDate { get; set; }

        public decimal? LastPaymentAmount { get; set; }

        public string HsGuarantorIds { get; set; }

        public int HsGuarantorIdsCount { get; set; }

        public bool IsDisabled { get; set; }

        public IList<SelectListItem> PhoneNumberTypes => new List<SelectListItem>
        {
            new SelectListItem {Text = "Mobile", Value = "Mobile"},
            new SelectListItem {Text = "Other", Value = "Other"}
        };

        public IList<SelectListItem> States
        {
            get { return Common.Base.Utilities.Lookup.Geo.GetStates().Select(x => new SelectListItem {Text = x.Value.ToString(), Value = x.Key.ToString()}).ToList(); }
        }

        public Dictionary<string, string> ManagedSso { get; set; }

        public bool IsLocked { get; set; }

        public LockoutReasonEnum? LockoutReasonEnum { get; set; }

        public string LockoutMessage { get; set; }

        // roles
        private static IList<string> BaseRoles => new List<string>
        {
            VisitPayRoleStrings.Csr.CSR,
            VisitPayRoleStrings.Csr.ReconfigureFP,
            VisitPayRoleStrings.Csr.UserAdmin,
            VisitPayRoleStrings.Csr.SupportAdmin,
            VisitPayRoleStrings.Csr.SupportView
        };

        public string[] RolesItemizations => new[]
        {
            VisitPayRoleStrings.Csr.CSR
        };

        public string[] RolesAccountHistory => new[]
        {
            VisitPayRoleStrings.Restricted.AuditEventLogViewer
        };

        public string[] RolesPayments => new List<string>(BaseRoles)
        {
            VisitPayRoleStrings.Payment.PaymentRefund,
            VisitPayRoleStrings.Payment.PaymentVoid,
            VisitPayRoleStrings.Payment.PaymentRefundReturnedCheck
        }.ToArray();

        public string[] RolesVisits => BaseRoles.ToArray();

        public bool ShowAccountClosedText => this.AccountClosedText.Length > 0;

        public string AccountClosedText => this.GetAccountClosedMessage();

        private string GetAccountClosedMessage()
        {
            string message = "";

            if (this.LockoutReasonEnum == null && this.IsOfflineUser)
            {
                return "Offline user";
            }


            switch (this.LockoutReasonEnum)
            {
                case Common.VisitPay.Enums.LockoutReasonEnum.AccountPendingCancellation:
                    message = $"Account pending cancellation {this.AccountClosedDate} <br/>Balances may not be up to date";
                    break;
                case Common.VisitPay.Enums.LockoutReasonEnum.AccountCancellation:
                    message = $"Account canceled {this.AccountClosedDate} <br/>Balances may not be up to date";
                    break;
                case Common.VisitPay.Enums.LockoutReasonEnum.AccountPendingReactivation:
                    message = (this.AccountClosedDate.HasValue) ? $"Pending reactivation on {this.AccountReactivationDate}" : "Pending reactivation";
                    message = $"{message} <br/>Balances may not be up to date";
                    break;
                case Common.VisitPay.Enums.LockoutReasonEnum.Offline:
                    message = "Offline user";
                    break;
                case Common.VisitPay.Enums.LockoutReasonEnum.OfflinePending:
                    message = "Offline user (pending)";
                    break;
            }

            return message;
        }

        public bool IsClosedOrCancelled => this.AccountClosedDate.HasValue || this.AccountCancellationDate.HasValue;
    }
}
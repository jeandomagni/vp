﻿namespace Ivh.Web.Client.Models.Search
{
    using System.ComponentModel.DataAnnotations;

    public class GuarantorVisitBalanceTransferStatusUpdateViewModel
    {
        public string VisitSourceSystemKey { get; set; }
        public bool CanSetBalanceTransferStatus { get; set; }
        public bool CanSetIneligible { get; set; }
        public bool CanSetEligible { get; set; }

        [Required(ErrorMessage = "A change reason is required to change the balance transfer status.")]
        [Display(Name = "Change Reason")]
        public string Notes { get; set; }

        
    }
}
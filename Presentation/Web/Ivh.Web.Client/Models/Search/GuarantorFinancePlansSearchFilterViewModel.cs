namespace Ivh.Web.Client.Models.Search
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.Web.Mvc;
    using Common.Web.Attributes;

    public class GuarantorFinancePlansSearchFilterViewModel
    {
        public GuarantorFinancePlansSearchFilterViewModel()
        {
            this.FinancePlanStatuses = new List<SelectListItem>();
        }

        public IList<int> FinancePlanStatusIds { get; set; }

        public string LastXRange { get; set; }

        [Display(Prompt = "Start Date")]
        [DateRangeCompare(false, "*.OriginationDateRangeFrom", "*.OriginationDateRangeTo", DateRangeCompareType.LessThanOrEqual, ErrorMessage = "Invalid Date Range")]
        [DataType(DataType.Date)]
        public DateTime? OriginationDateRangeFrom { get; set; }

        [Display(Prompt = "End Date")]
        [DateRangeCompare(false, "*.OriginationDateRangeTo", "*.OriginationDateRangeFrom", DateRangeCompareType.GreaterThanOrEqual, ErrorMessage = "Invalid Date Range")]
        [DataType(DataType.Date)]
        public DateTime? OriginationDateRangeTo { get; set; }

        public int VisitPayUserId { get; set; }

        public int CurrentVisitPayUserId { get; set; }

        public IList<SelectListItem> LastXRanges
        {
            get
            {
                return new List<SelectListItem>
                {
                    new SelectListItem { Text = "90 Days", Value = "90d" },
                    new SelectListItem { Text = "6 Months", Value = "6m" },
                    new SelectListItem { Text = "1 Year", Value = "1y" },
                    new SelectListItem { Text = "All", Value = "" }
                };
            }
        }
        public IList<SelectListItem> FinancePlanStatuses { get; set; }
    }
}
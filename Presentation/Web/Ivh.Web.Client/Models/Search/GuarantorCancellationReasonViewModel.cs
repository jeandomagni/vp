﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Ivh.Web.Client.Models.Search
{
    public class GuarantorCancellationReasonViewModel
    {
        public int VpGuarantorCancellationReasonId { get; set; }
        public string Name { get; set; }
        public string DisplayName { get; set; }
        public int DisplaySortOrder { get; set; }
        public bool Selectable { get; set; }
    }
}
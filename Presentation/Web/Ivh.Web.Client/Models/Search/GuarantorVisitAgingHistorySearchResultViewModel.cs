namespace Ivh.Web.Client.Models.Search
{
    public class GuarantorVisitAgingHistorySearchResultViewModel
    {
        public int VisitAgingHistoryId { get; set; }
        public virtual int VisitId { get; set; }
        public string InsertDate { get; set; }
        public int AgingTier { get; set; }
        public string VisitPayUserName { get; set; }
    }
}
namespace Ivh.Web.Client.Models.Search
{
    using Common.Base.Enums;
    using Common.VisitPay.Enums;

    public class GuarantorVisitsSearchResultViewModel
    {
        public int AgingCount { get; set; }

        public bool BillingHold { get; set; }

        public string BillingApplication { get; set; }

        public string CurrentBalance { get; set; }

        public decimal CurrentBalanceDecimal { get; set; }

        public string DischargeDate { get; set; }

        public int VisitPayUserId { get; set; }

        public string PatientName { get; set; }

        public string SourceSystemKey { get; set; }

        public string SourceSystemKeyDisplay { get; set; }

        public string TotalCharges { get; set; }

        public int VisitId { get; set; }

        public int VisitStateId { get; set; }

        public string VisitStateFullDisplayName { get; set; }

        public string VisitStateDerivedFullDisplayName { get; set; }

        public string MatchedSourceSystemKey { get; set; }

        public bool UnmatchEnabled { get; set; }

        public bool BalanceTransferEnabled { get; set; }

        public int BalanceTransferStatus { get; set; }

        public string UnclearedPaymentsSum { get; set; }

        public string UnclearedBalance { get; set; }

        public string InterestAssessed { get; set; }

        public string InterestDue { get; set; }

        public string InterestPaid { get; set; }

        public bool IsBalanceTransferEligible => (BalanceTransferStatusEnum)this.BalanceTransferStatus == BalanceTransferStatusEnum.Eligible;

        public bool IsBalanceTransferActive => (BalanceTransferStatusEnum)this.BalanceTransferStatus == BalanceTransferStatusEnum.ActiveBalanceTransfer;

        public bool IsBalanceTransferIneligible => (BalanceTransferStatusEnum)this.BalanceTransferStatus == BalanceTransferStatusEnum.Ineligible;

        public bool IsRemoved { get; set; }

        public bool IsPastDue { get; set; }
    }
}
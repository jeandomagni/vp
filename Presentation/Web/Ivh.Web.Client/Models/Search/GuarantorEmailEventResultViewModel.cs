namespace Ivh.Web.Client.Models.Search
{
    public class GuarantorEmailEventResultViewModel
    {
        public int EmailEventId { get; set; }
        public string EmailAddress { get; set; }
        public string EventName { get; set; }
        public string Response { get; set; }
        public string Reason { get; set; }
        public string EventDateTime { get; set; }
    }
}
﻿namespace Ivh.Web.Client.Models.Search
{
    public class ChangePaymentDueDateViewModel
    {
        public int VpGuarantorId { get; set; }
        public string PaymentDueDate { get; set; }
        public int DueDay { get; set; }
    }
}
namespace Ivh.Web.Client.Models.Search
{
    using System;
    using System.Collections.Generic;
    using System.Web.Mvc;
    using Common.Base.Enums;
    using Common.Base.Utilities.Extensions;
    using Common.VisitPay.Enums;

    public class GuarantorSupportHistorySearchFilterViewModel
    {
        public GuarantorSupportHistorySearchFilterViewModel()
        {
            this.SupportRequestStatuses = new List<SelectListItem>
            {
                new SelectListItem {Text = "All", Value = "", Selected = true},
                new SelectListItem {Text = SupportRequestStatusEnum.Open.GetDescription(), Value = ((int)SupportRequestStatusEnum.Open).ToString()},
                new SelectListItem {Text = SupportRequestStatusEnum.Closed.GetDescription(), Value = ((int)SupportRequestStatusEnum.Closed).ToString()}
            };
            this.DateRanges = new List<SelectListItem>
            {
                new SelectListItem {Text = "Last 30 Days", Value = DateTime.UtcNow.AddDays(-30).ToShortDateString(), Selected = true},
                new SelectListItem {Text = "Last 60 Days", Value = DateTime.UtcNow.AddDays(-60).ToShortDateString()},
                new SelectListItem {Text = "Last 90 Days", Value = DateTime.UtcNow.AddDays(-90).ToShortDateString()},
                new SelectListItem {Text = "All", Value = ""}
            };
        }

        public int VisitPayUserId { get; set; }
        public SupportRequestStatusEnum? SupportRequestStatus { get; set; }
        public DateTime? DateRangeFrom { get; set; }

        public IList<SelectListItem> SupportRequestStatuses { get; set; }
        public IList<SelectListItem> DateRanges { get; set; }
    }
}
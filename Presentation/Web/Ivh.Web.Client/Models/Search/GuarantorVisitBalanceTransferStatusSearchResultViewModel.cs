﻿namespace Ivh.Web.Client.Models.Search
{
    public class GuarantorVisitBalanceTransferStatusSearchResultViewModel
    {
        public string BalanceTransferStatusDisplayName { get; set; }
        public string InsertDate { get; set; }
        public string VisitPayUserName { get; set; }
        public string Notes { get; set; }
    }
}
﻿namespace Ivh.Web.Client.Models.Search
{
    public class GuarantorPaymentDueDayHistoryViewModel
    {
        public string PaymentDueDay { get; set; }
        public string OldPaymentDueDay { get; set; }
        public string InsertedBy { get; set; }
        public string InsertDate { get; set; }
    }
}
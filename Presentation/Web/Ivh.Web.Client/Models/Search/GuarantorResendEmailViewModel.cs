﻿using Ivh.Common.Web.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Ivh.Web.Client.Models.Search
{
    using Common.Base.Constants;
    using Common.VisitPay.Constants;

    public class GuarantorResendEmailViewModel
    {
        public int EmailId { get; set; }

        public int VisitPayUserId { get; set; }

        public string Subject { get; set; }

        [Display(Name = "To")]
        [Required(ErrorMessage = "To field is required."), RegularExpressionEmailAddress(TextRegionConstants.EmailInvalidLongMessage)]
        [DataType(DataType.EmailAddress)]
        public string ToEmail { get; set; }

        [Display(Name = "To")]
        [DataType(DataType.PhoneNumber)]
        public string ToPhone { get; set; }

        public bool IsSms { get; set; }
    }
}
namespace Ivh.Web.Client.Models.Search
{
    using System;
    using Common.Base.Enums;
    using Common.VisitPay.Enums;
    using Common.VisitPay.Strings;

    public class SearchGuarantorsSearchResultViewModel
    {
        public string Name { get; set; }
        public VpGuarantorStatusEnum VpGuarantorStatus { get; set; }
        public string SourceSystemKeys { get; set; }
        public int HsGuarantorId { get; set; }
        public int VpGuarantorId { get; set; }
        //public string VpGuarantorIdDisplay => this.VisitPayUserId == 0 ? "" : this.VisitPayUserId.ToString();
        public int VisitPayUserId { get; set; }
        public string Ssn4 { get; set; }
        public string DateOfBirth { get; set; }
        public string UserNameAndEmailAddress { get; set; }
        public string LastStatementBalance { get; set; }
        public DateTime? LastStatementPaymentDueDate { get; set; }
        public DateTime? LastStatementDate { get; set; }
        public bool LastStatementIsGracePeriod { get; set; }

        public string LastStatementDateDisplay => this.LastStatementDate.HasValue ? this.LastStatementDate.Value.ToString(Format.DateFormat) : "";

        public bool IsAwaitingStatement { get; set; }

        public bool IsStatemented => this.LastStatementDate.HasValue;

        public string VpGuarantorStatusDisplay => this.GetVpGuarantorStatusDisplay();

        public string LockoutMessage => this.GetLockoutMessage();

        public LockoutReasonEnum? LockoutReasonEnum { get; set; }
        public DateTime? LockoutEndDateUtc { get; set; }
        public bool IsLocked { get; set; }
        public DateTime? AccountClosedDate { get; set; }
        public GuarantorTypeEnum? VpGuarantorTypeEnum { get; set; }

        public bool SupportsOffline { get; set; }

        public bool IsHsGuarantor => this.VisitPayUserId == 0;

        private string GetVpGuarantorStatusDisplay()
        {
            if (!this.VpGuarantorTypeEnum.HasValue)
                return string.Empty;

            string status = "";
            if (!this.LockoutReasonEnum.HasValue)
                status = "Active";
            else
                switch (this.LockoutReasonEnum.Value)
                {
                    case Common.VisitPay.Enums.LockoutReasonEnum.AccountPendingCancellation:
                        status = "Pending Cancellation";
                        break;
                    case Common.VisitPay.Enums.LockoutReasonEnum.AccountCancellation:
                        status = "Canceled";
                        break;
                    case Common.VisitPay.Enums.LockoutReasonEnum.AccountPendingReactivation:
                        status = "Pending Reactivation";
                        break;
                    default:
                        ;
                        status = "Active";
                        break;
                }
            return this.FormatVpGuarantorStatusDisplay(status);
        }

        private string GetLockoutMessage()
        {
            if (this.LockoutEndDateUtc.HasValue && this.IsLocked && !this.LockoutReasonEnum.HasValue)
                return "Failed Login. Clears on " + this.LockoutEndDateUtc.Value.ToShortDateString();

            if (this.LockoutReasonEnum.HasValue)
                switch (this.LockoutReasonEnum.Value)
                {
                    case Common.VisitPay.Enums.LockoutReasonEnum.AccountDisabled:
                        return "Account Disabled from Client Portal";
                    case Common.VisitPay.Enums.LockoutReasonEnum.AccountPendingCancellation:
                    case Common.VisitPay.Enums.LockoutReasonEnum.AccountCancellation:
                    case Common.VisitPay.Enums.LockoutReasonEnum.AccountPendingReactivation:
                        if (this.AccountClosedDate.HasValue)
                            return "Locked due to Cancellation on " + this.AccountClosedDate.Value.ToString(Format.DateFormat) + " at " + this.AccountClosedDate.Value.ToString(Format.TimeFormat);
                        return "Locked due to Cancellation";
                    default:
                        return string.Empty;
                }

            return string.Empty;
        }

        private string FormatVpGuarantorStatusDisplay(string status)
        {
            if (!this.SupportsOffline) return status;
            if (!this.VpGuarantorTypeEnum.HasValue) return status;
            return $"{status} - {this.VpGuarantorTypeEnum.Value}";
        }
    }
}
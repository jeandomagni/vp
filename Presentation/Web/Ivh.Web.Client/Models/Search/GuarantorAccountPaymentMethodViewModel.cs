namespace Ivh.Web.Client.Models.Search
{
    using Common.Base.Enums;
    using Common.VisitPay.Enums;

    public class GuarantorAccountPaymentMethodViewModel
    {
        public PaymentMethodTypeEnum PaymentMethodType { get; set; }
        public string LastFour { get; set; }
        public bool IsPrimary { get; set; }
        public string DisplayText => $"{this.PaymentMethodType} (X-{this.LastFour})";
    }
}
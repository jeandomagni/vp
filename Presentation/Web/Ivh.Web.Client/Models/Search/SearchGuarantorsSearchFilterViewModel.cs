namespace Ivh.Web.Client.Models.Search
{
    using System;
    using Ivh.Common.Base.Utilities.Helpers;
    using System.ComponentModel.DataAnnotations;
    using Common.Web.Attributes;

    public class SearchGuarantorsSearchFilterViewModel
    {
        public string HsGuarantorId { get; set; }

        public int? VpGuarantorId { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string Ssn4 { get; set; }

        public string UserNameOrEmailAddress { get; set; }

        [AgeDateRange(MaximumAge = 120, MinimumAge = 0)]
        [DataType(DataType.Date)]
        public DateTime? DateOfBirth { get; set; }

        public bool IsValidSearch
        {
            get
            {
                return !string.IsNullOrWhiteSpace(this.HsGuarantorId) ||
                       this.VpGuarantorId.HasValue ||
                       !string.IsNullOrWhiteSpace(this.FirstName) ||
                       !string.IsNullOrWhiteSpace(this.LastName) ||
                       !string.IsNullOrWhiteSpace(this.Ssn4) ||
                       !string.IsNullOrWhiteSpace(this.UserNameOrEmailAddress)||
                       this.DateOfBirth.HasValue;
            }
        }

        public string LastNameFirstName => FormatHelper.LastNameFirstName(this.LastName, this.FirstName);
        public string FirstNameLastName => FormatHelper.FirstNameLastName(this.FirstName, this.LastName);
    }
}
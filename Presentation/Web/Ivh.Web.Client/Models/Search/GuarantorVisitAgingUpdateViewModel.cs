namespace Ivh.Web.Client.Models.Search
{
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.Web.Mvc;

    public class GuarantorVisitAgingUpdateViewModel
    {
        public GuarantorVisitAgingUpdateViewModel()
        {
            this.AvailableAgingTiers = new List<SelectListItem>();
        }

        [Required]
        [Display(Name = "Change Aging Tier To:")]
        public int NewAgingTier { get; set; }

        [Required(ErrorMessage = "A reason is required to change the aging tier.")]
        [Display(Name = "Reason:")]
        public string ChangeDescription { get; set; }
        public bool IsAccountClosed { get; set; }
        public string MasAgingTierContentTitle { get; set; }
        public string MaxAgingTierContentBody { get; set; }
        public int CurrentAgingTier { get; set; }
        public string SourceSystemKeyDisplay { get; set; }
        public int VisitId { get; set; }
        public int VisitPayUserId { get; set; }
        public IList<SelectListItem> AvailableAgingTiers { get; set; }
    }
}
namespace Ivh.Web.Client.Models.Search
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.Web.Mvc;
    using Common.Web.Attributes;

    public class GuarantorVisitsSearchFilterViewModel
    {
        public GuarantorVisitsSearchFilterViewModel()
        {
            this.BillingApplications = new List<SelectListItem>();
            this.VisitStates = new List<SelectListItem>();
        }

        public string BillingApplication { get; set; }

        [Display(Prompt = "Start Date")]
        [DateRangeCompare(false, "*.VisitDateRangeFrom", "*.VisitDateRangeTo", DateRangeCompareType.LessThanOrEqual, ErrorMessage = "Invalid Date Range")]
        [DataType(DataType.Date)]
        public DateTime? VisitDateRangeFrom { get; set; }

        [Display(Prompt = "End Date")]
        [DateRangeCompare(false, "*.VisitDateRangeTo", "*.VisitDateRangeFrom", DateRangeCompareType.GreaterThanOrEqual, ErrorMessage = "Invalid Date Range")]
        [DataType(DataType.Date)]
        public DateTime? VisitDateRangeTo { get; set; }

        public int VisitPayUserId { get; set; }

        public IList<int> VisitStateIds { get; set; }
        
        public IList<SelectListItem> BillingApplications { get; set; }

        public IList<SelectListItem> VisitStates { get; set; }

        public int? FinancePlanId { get; set; }
    }
}
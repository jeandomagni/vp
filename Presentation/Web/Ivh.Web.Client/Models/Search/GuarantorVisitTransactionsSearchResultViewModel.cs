namespace Ivh.Web.Client.Models.Search
{
    public class GuarantorVisitTransactionsSearchResultViewModel
    {
        public string DisplayDate { get; set; }
        public string TransactionType { get; set; }
        public string TransactionDescription { get; set; }
        public string TransactionAmount { get; set; }
        public bool CanIgnore { get; set; }
    }
}
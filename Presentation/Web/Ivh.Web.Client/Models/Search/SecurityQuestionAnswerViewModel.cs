﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Ivh.Web.Client.Models.Search
{
    public class SecurityQuestionAnswerViewModel
    {
        public SecurityQuestionReadOnlyViewModel SecurityQuestion { get; set; }
        public string Answer { get; set; }
        public int VisitPayUserId { get; set; }
    }
}
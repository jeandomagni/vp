namespace Ivh.Web.Client.Models.Search
{
    using System.Collections.Generic;
    using System.Web.Mvc;

    public class GuarantorVisitStateHistorySearchFilterViewModel
    {
        public GuarantorVisitStateHistorySearchFilterViewModel()
        {
            this.VisitStates = new List<SelectListItem>();
        }

        public int VisitPayUserId { get; set; }
        public int VisitId { get; set; }
        public IList<int> VisitStateIds { get; set; }

        public IList<SelectListItem> VisitStates { get; set; }
    }
}
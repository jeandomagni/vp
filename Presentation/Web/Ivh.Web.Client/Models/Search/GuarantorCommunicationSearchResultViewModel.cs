﻿namespace Ivh.Web.Client.Models.Search
{
    public class GuarantorCommunicationSearchResultViewModel
    {
        public int EmailId { get; set; }
        public string Subject { get; set; }
        public string EmailBody { get; set; }
        public string CommunicationMethod { get; set; }
        public string To { get; set; }
        public string EmailStatus { get; set; }
        public string CreatedDate { get; set; }
        public string OutboundServerAccepted { get; set; }
        public string ProcessedDate { get; set; }
        public string DeliveredDate { get; set; }
        public string FirstOpenDate { get; set; }
        public string FailedDate { get; set; }
        public bool WasDelivered { get; set; }
        public bool WasOpened { get; set; }
    }
}
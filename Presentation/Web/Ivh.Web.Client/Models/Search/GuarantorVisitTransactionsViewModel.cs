namespace Ivh.Web.Client.Models.Search
{
    using System.Globalization;

    public class GuarantorVisitTransactionsViewModel
    {
        public GuarantorVisitTransactionsSearchFilterViewModel SearchFilter { get; set; }

        public decimal VisitBalance { get; set; }

        public string VisitBillingApplication { get; set; }

        public string VisitDescription { get; set; }

        public string SourceSystemKeyDisplay { get; set; }

        public string VisitBalanceDisplayText => this.VisitBalance.ToString("C", new NumberFormatInfo { CurrencySymbol = "$", CurrencyNegativePattern = 1 });
    }
}
namespace Ivh.Web.Client.Models.Search
{
    using System.Collections.Generic;
    using Common.Base.Enums;
    using Common.VisitPay.Enums;

    public class GuarantorFinancePlansSearchResultViewModel
    {
        public GuarantorFinancePlansSearchResultViewModel()
        {
            this.VisitIds = new List<int>();
        }
        
        public string CurrentFinancePlanBalance { get; set; }
        public decimal CurrentFinancePlanBalanceDecimal { get; set; }
        public int FinancePlanId { get; set; }
        public string FinancePlanStatusFullDisplayName { get; set; }
        public string InterestAssessed { get; set; }
        public string InterestPaidToDate { get; set; }
        public string InterestRate { get; set; }
        public string OriginatedFinancePlanBalance { get; set; }
        public string OriginationDate { get; set; }
        public string PaymentAmount { get; set; }
        public string PrincipalPaidToDate { get; set; }
        public int ReconfiguredFinancePlanId { get; set; }
        public string Terms { get; set; }
        public int CurrentBucket { get; set; }
        public IList<int> VisitIds { get; set; }
        public FinancePlanStatusEnum FinancePlanStatus { get; set; }

        public bool IsPending
        {
            get { return this.FinancePlanStatus == FinancePlanStatusEnum.PendingAcceptance || this.FinancePlanStatus == FinancePlanStatusEnum.TermsAccepted; }
        }

        public bool IsPastDue
        {
            get { return this.FinancePlanStatus == FinancePlanStatusEnum.PastDue; }
        }

        public bool IsUncollectable
        {
            get { return this.FinancePlanStatus == FinancePlanStatusEnum.UncollectableClosed || this.FinancePlanStatus == FinancePlanStatusEnum.UncollectableActive; }
        }

        public bool CanReconfigure { get; set; }
        public bool IsManaged { get; set; }
        public bool IsGuarantorCanceled { get; set; }
        public bool CanCancelOriginatedFinancePlan { get; set; }
        public bool IsOfflineGuarantor { get; set; }
    }
}
namespace Ivh.Web.Client.Models.Search
{
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.Web.Mvc;
    using Common.Base.Enums;
    using Common.VisitPay.Enums;

    public class GuarantorVisitTransactionsSearchFilterViewModel
    {
        public GuarantorVisitTransactionsSearchFilterViewModel()
        {
            this.TransactionTypes = new List<SelectListItem>();
        }

        public int VisitId { get; set; }

        public int VisitPayUserId { get; set; }

        [Display(Name = "Transaction Type")]
        public IList<VpTransactionTypeFilterEnum> TransactionType { get; set; }

        public IList<SelectListItem> TransactionTypes { get; set; }
    }
}
﻿
namespace Ivh.Web.Client.Models.Search
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.Web.Mvc;
    using Common.Web.Attributes;

    public class GuarantorChatHistorySearchFilterViewModel
    {
        public GuarantorChatHistorySearchFilterViewModel()
        {
            this.DateRanges = new List<SelectListItem>
            {
                new SelectListItem {Text = "Last 30 Days", Value = DateTime.UtcNow.AddDays(-30).ToShortDateString()},
                new SelectListItem {Text = "Last 60 Days", Value = DateTime.UtcNow.AddDays(-60).ToShortDateString(), Selected = true},
                new SelectListItem {Text = "Last 90 Days", Value = DateTime.UtcNow.AddDays(-90).ToShortDateString()},
                new SelectListItem {Text = "Last 120 Days", Value = DateTime.UtcNow.AddDays(-120).ToShortDateString()},
                new SelectListItem {Text = "All", Value = ""}
            };
        }

        [Display(Prompt = "Start Date")]
        [DateRangeCompare(false, "*.ChatHistoryDateRangeFrom", "*.ChatHistoryDateRangeTo", DateRangeCompareType.LessThanOrEqual, ErrorMessage = "Invalid Date Range")]
        [DataType(DataType.Date)]
        public DateTime? ChatHistoryDateRangeFrom { get; set; }

        [Display(Prompt = "End Date")]
        [DateRangeCompare(false, "*.ChatHistoryDateRangeTo", "*.ChatHistoryDateRangeFrom", DateRangeCompareType.GreaterThanOrEqual, ErrorMessage = "Invalid Date Range")]
        [DataType(DataType.Date)]
        public DateTime? ChatHistoryDateRangeTo { get; set; }

        public int VisitPayUserId { get; set; }
        public DateTime? DateRangeFrom { get; set; }

        public IList<SelectListItem> DateRanges { get; set; }
    }
}
namespace Ivh.Web.Client.Models.Search
{
    using Common.Base.Enums;

    public class GuarantorTransactionsSearchResultViewModel
    {
        public string DisplayDate { get; set; }
        public string GuarantorName { get; set; }
        public string PatientName { get; set; }
        public string TransactionType { get; set; }
        public string TransactionDescription { get; set; }
        public string TransactionAmount { get; set; }
        public string VisitSourceSystemKey { get; set; }
        public string VisitSourceSystemKeyDisplay { get; set; }
        public string VisitMatchedSourceSystemKey { get; set; }
        public int VisitStateId { get; set; }
        public string VisitStateFullDisplayName { get; set; }
        public int VisitId { get; set; }
        public bool CanIgnore { get; set; }

        public bool IsVisitRemoved { get; set; }
    }
}
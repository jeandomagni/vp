namespace Ivh.Web.Client.Models.Search
{
    public class EmulateUserViewModel
    {
        public int EmulateGuarantorId { get; set; }
        public string EmulateUserName { get; set; }
        public string Url { get; set; }
        public string EmulateToken { get; set; }
    }
}
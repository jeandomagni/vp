namespace Ivh.Web.Client.Models.Search
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.Web.Mvc;
    using Common.Web.Attributes;

    public class GuarantorTransactionsSearchFilterViewModel
    {
        public GuarantorTransactionsSearchFilterViewModel()
        {
            this.TransactionTypes = new List<SelectListItem>();
        }

        public int VisitPayUserId { get; set; }

        [Display(Prompt = "Start Date")]
        [DateRangeCompare(false, "*.TransactionDateRangeFrom", "*.TransactionDateRangeTo", DateRangeCompareType.LessThanOrEqual, ErrorMessage = "Invalid Date Range")]
        [DataType(DataType.Date)]
        public DateTime? TransactionDateRangeFrom { get; set; }

        [Display(Prompt = "End Date")]
        [DateRangeCompare(false, "*.TransactionDateRangeTo", "*.TransactionDateRangeFrom", DateRangeCompareType.GreaterThanOrEqual, ErrorMessage = "Invalid Date Range")]
        [DataType(DataType.Date)]
        public DateTime? TransactionDateRangeTo { get; set; }

        public string TransactionType { get; set; }

        public int? VpGuarantorId { get; set; }
        
        public IList<SelectListItem> TransactionTypes { get; set; }
    }
}
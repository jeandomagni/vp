namespace Ivh.Web.Client.Models.Search
{
    public class GuarantorVisitAgingHistorySearchFilterViewModel
    {
        public int VisitPayUserId { get; set; }
        public int VisitId { get; set; }
    }
}
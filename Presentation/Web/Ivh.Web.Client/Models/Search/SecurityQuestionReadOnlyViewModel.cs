﻿namespace Ivh.Web.Client.Models.Search
{
    public class SecurityQuestionReadOnlyViewModel
    {
        public int SecurityQuestionId { get; set; }
        public string Question { get; set; }
        public bool IsActive { get; set; }
    }
}
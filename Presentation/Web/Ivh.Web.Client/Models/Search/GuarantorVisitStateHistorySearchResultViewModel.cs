namespace Ivh.Web.Client.Models.Search
{
    public class GuarantorVisitStateHistorySearchResultViewModel
    {
        public string InsertDate { get; set; }
        public int VisitStateId { get; set; }
        public string VisitStateFullDisplayName { get; set; }
        public string ChangeDescription { get; set; }
    }
}
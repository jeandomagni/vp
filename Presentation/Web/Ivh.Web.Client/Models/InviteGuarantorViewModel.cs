﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Ivh.Web.Client.Models
{
    using System.ComponentModel.DataAnnotations;
    using Common.Base.Constants;
    using Common.VisitPay.Constants;
    using Common.Web.Attributes;

    public class GuarantorInvitationViewModel
    {
        public string ClientBrandName { get; set; }

        [Display(Name = "Name", Prompt = "First Name")]
        [Required(ErrorMessage = "First Name is required.")]
        public string FirstName { get; set; }

        [Display(Prompt = "Middle Name")]
        public string MiddleName { get; set; }

        [Display(Prompt = "Last Name")]
        [Required(ErrorMessage = "Last Name is required.")]
        public string LastName { get; set; }

        [Display(Name = "Email Address", Prompt = "Email Address")]
        [Required(ErrorMessage = "Email is required."), RegularExpressionEmailAddress(TextRegionConstants.EmailInvalidLongMessage)]
        [DataType(DataType.EmailAddress)]
        public string Email { get; set; }
    }
}
﻿namespace Ivh.Web.Client.Models.Consolidate
{
    using System.Collections.Generic;
    using Common.Web.Models.Consolidate;

    public class GuarantorConsolidationsViewModel
    {
        public GuarantorConsolidationsViewModel()
        {
            this.ManagedGuarantors = new List<ConsolidationGuarantorViewModel>();
            this.ManagingGuarantors = new List<ConsolidationGuarantorViewModel>();
        }

        public List<ConsolidationGuarantorViewModel> ManagedGuarantors { get; set; }
        public List<ConsolidationGuarantorViewModel> ManagingGuarantors { get; set; }
    }
}
﻿namespace Ivh.Web.Client.Models.ClientSupport
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Web.Mvc;
    using Common.Base.Enums;
    using Common.VisitPay.Enums;

    public class ClientSupportRequestViewModel
    {
        public ClientSupportRequestViewModel()
        {
            this.Attachments = new List<ClientSupportRequestMessageAttachmentViewModel>();
            this.AdminVisitPayUsers = new List<SelectListItem>();
        }

        public bool IsAdmin { get; set; }
        public int? AssignedVisitPayUserId { get; set; }
        public int? TargetVpGuarantorId { get; set; }
        public string ClientSupportRequestDisplayId { get; set; }
        public ClientSupportRequestStatusEnum ClientSupportRequestStatus { get; set; }
        public string ContactPhone { get; set; }
        public string CreatedByVisitPayUserEmail { get; set; }
        public string CreatedByVisitPayUserName { get; set; }
        public int CreatedByVisitPayUserId { get; set; }
        public string InsertDate { get; set; }
        public string ClosedDate { get; set; }
        public string JiraId { get; set; }
        public string TopicDisplay { get; set; }
        public IList<ClientSupportRequestMessageViewModel> MessagesClient { get; set; }
        public IList<ClientSupportRequestMessageViewModel> MessagesInternal { get; set; }
        public IList<ClientSupportRequestMessageAttachmentViewModel> Attachments { get; set; }
        public IList<SelectListItem> AdminVisitPayUsers { get; set; }

        public string AssignedVisitPayUserName
        {
            get
            {
                if (!this.AssignedVisitPayUserId.HasValue)
                {
                    return string.Empty;
                }

                SelectListItem user = this.AdminVisitPayUsers.FirstOrDefault(x => x.Value == this.AssignedVisitPayUserId.Value.ToString());
                if (user == null)
                {
                    return string.Empty;
                }

                return user.Text;
            }
        }
    }

    public class ClientSupportRequestMessageViewModel
    {
        public int ClientSupportRequestMessageId { get; set; }
        public string InsertDate { get; set; }
        public string MessageBody { get; set; }
        public ClientSupportRequestMessageTypeEnum ClientSupportRequestMessageType { get; set; }
        public int AttachmentCount { get; set; }
        public int CreatedByVisitPayUserId { get; set; }
        public string CreatedByVisitPayUserName { get; set; }
    }
}
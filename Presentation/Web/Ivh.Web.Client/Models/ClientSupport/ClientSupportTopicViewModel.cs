﻿namespace Ivh.Web.Client.Models.ClientSupport
{
    using System.Collections.Generic;
    using Common.Web.Models.Shared;

    public class ClientSupportTopicViewModel
    {
        public ClientSupportTopicViewModel()
        {
            this.Templates = new List<CmsViewModel>();
            this.Topics = new List<ClientSupportTopicViewModel>();
        }

        public string ClientSupportTopicId { get; set; }
        public string ClientSupportTopicName { get; set; }
        public IList<ClientSupportTopicViewModel> Topics { get; set; }
        public IList<CmsViewModel> Templates { get; set; }
    }
}
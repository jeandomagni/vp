﻿namespace Ivh.Web.Client.Models.ClientSupport
{
    using System.ComponentModel.DataAnnotations;
    using Common.Base.Enums;
    using Common.VisitPay.Enums;

    public class ClientSupportRequestActionViewModel
    {
        public int ClientSupportRequestId { get; set; }

        public string ClientSupportRequestDisplayId { get; set; }

        public ClientSupportRequestStatusEnum ClientSupportRequestStatus { get; set; }

        [Required(ErrorMessage = "Note is required")]
        public string Message { get; set; }

        public string ActionText
        {
            get { return this.ClientSupportRequestStatus == ClientSupportRequestStatusEnum.Open ? "Re-Open" : "Close"; }
        }
    }
}
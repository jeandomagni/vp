﻿namespace Ivh.Web.Client.Models.ClientSupport
{
    using Common.VisitPay.Enums;

    public class ClientSupportRequestMessageAttachmentViewModel
    {
        public int ClientSupportRequestMessageAttachmentId { get; set; }
        public int ClientSupportRequestMessageId { get; set; }
        public string AttachmentFileName { get; set; }
        public string MimeType { get; set; }
        public int FileSize { get; set; }
        public string InsertDate { get; set; }
        public MalwareScanStatusEnum MalwareScanStatus { get; set; }
    }
}
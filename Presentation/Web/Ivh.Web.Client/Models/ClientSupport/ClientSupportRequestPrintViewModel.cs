﻿namespace Ivh.Web.Client.Models.ClientSupport
{
    public class ClientSupportRequestPrintViewModel
    {
        public bool IsAdmin { get; set; }
        public ClientSupportRequestViewModel ClientSupportRequest { get; set; }
    }
}
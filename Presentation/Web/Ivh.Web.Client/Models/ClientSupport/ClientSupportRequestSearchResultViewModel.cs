﻿namespace Ivh.Web.Client.Models.ClientSupport
{
    using System;

    public class ClientSupportRequestSearchResultViewModel
    {
        public int ClientSupportRequestId { get; set; }
        public string ClientSupportRequestDisplayId { get; set; }
        public string JiraId { get; set; }
        public string StatusDisplay { get; set; }
        public string TopicDisplay { get; set; }
        public string InsertDate { get; set; }
        public string LastModifiedDate { get; set; }
        public string AssignedToVisitPayUserName { get; set; }
        public string CreatedByVisitPayUserName { get; set; }
        public string LastModifiedByVisitPayUserName { get; set; }
        public string TargetVpGuarantorId { get; set; }
        public bool IsClosed { get; set; }
        public int UnreadMessageCountFromClient { get; set; }
        public int UnreadMessageCountToClient { get; set; }
        public int AttachmentCountClient { get; set; }
        public int AttachmentCountInternal { get; set; }
        public string MessagePreview { get; set; }
    }
}
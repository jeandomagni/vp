﻿namespace Ivh.Web.Client.Models.ClientSupport
{
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.Web.Mvc;
    using Common.Base.Enums;
    using Common.Base.Utilities.Extensions;
    using Common.VisitPay.Enums;

    public class ClientSupportRequestFilterViewModel
    {
        private IList<SelectListItem> _defaultDayOffsets = new List<SelectListItem>
        {
            new SelectListItem {Text = "Last 30 Days", Value = (-30).ToString()},
            new SelectListItem {Text = "Last 60 Days", Value = (-60).ToString()},
            new SelectListItem {Text = "Last 90 Days", Value = (-90).ToString()}
        };

        public ClientSupportRequestFilterViewModel()
        {
            this.AdminVisitPayUsers = new List<SelectListItem>();
            this.RequestedByUsers = new List<SelectListItem>();
        }

        [Display(Name = "Case ID", Prompt = "Case ID")]
        public string ClientSupportRequestDisplayId { get; set; }

        [Display(Name = "Jira ID", Prompt = "Jira ID")]
        public string JiraId { get; set; }

        [Display(Name = "Status")]
        public ClientSupportRequestStatusEnum? ClientSupportRequestStatus { get; set; }

        [Display(Name = "Assigned to")]
        public int? AssignedToVisitPayUserId { get; set; }

        [Display(Name = "Requested by")]
        public int? CreatedByVisitPayUserId { get; set; }

        [Display(Name = "")] // set in view because it uses a client setting
        public int? TargetVpGuarantorId { get; set; }

        [Display(Name = "Date Range")]
        public int? DaysOffset { get; set; }

        public IList<SelectListItem> AdminVisitPayUsers { get; set; }

        public IList<SelectListItem> ClientSupportRequestStatuses
        {
            get
            {
                return new List<SelectListItem>
                {
                    new SelectListItem {Text = ClientSupportRequestStatusEnum.Open.GetDescription(), Value = ((int) ClientSupportRequestStatusEnum.Open).ToString()},
                    new SelectListItem {Text = ClientSupportRequestStatusEnum.Closed.GetDescription(), Value = ((int) ClientSupportRequestStatusEnum.Closed).ToString()}
                };
            }
        }

        public IList<SelectListItem> RequestedByUsers { get; set; }

        public IList<SelectListItem> DayOffsets
        {
            get { return this._defaultDayOffsets; }
            set { this._defaultDayOffsets = value; }
        }
    }
}
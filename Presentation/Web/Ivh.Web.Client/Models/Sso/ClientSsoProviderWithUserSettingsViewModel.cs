﻿namespace Ivh.Web.Client.Models.Sso
{
    using Common.Web.Models.Sso;

    public class ClientSsoProviderWithUserSettingsViewModel : SsoProviderWithUserSettingsViewModel
    {
        public int VisitPayUserId { get; set; }
        public string SourceSystemKey { get; set; }
        public bool IsUserEligible { get; set; }
    }
}
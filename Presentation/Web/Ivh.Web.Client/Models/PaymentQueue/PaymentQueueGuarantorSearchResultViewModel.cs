﻿namespace Ivh.Web.Client.Models.PaymentQueue
{
    using System.Collections.Generic;
    
    public class PaymentQueueGuarantorSearchResultViewModel
    {
        public string Name { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string HsGuarantorId { get; set; }
        public int VpGuarantorId { get; set; }
        public string LastAmountDue { get; set; }
        public List<string> PhoneNumbers { get; set; }
        public List<string> Addresses { get; set; }
    }
}
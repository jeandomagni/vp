﻿
namespace Ivh.Web.Client.Models.PaymentQueue
{
    using System;
    using Ivh.Common.VisitPay.Enums;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.Linq;
    using System.Web.Mvc;
    using Common.Base.Utilities.Extensions;
    using Common.Web.Attributes;

    public class PaymentQueueFilterViewModel
    {
        public PaymentQueueFilterViewModel()
        {
            this.DateRanges = new List<SelectListItem>
            {
                new SelectListItem {Text = "Last 30 Days", Value = DateTime.UtcNow.AddDays(-30).ToShortDateString(), Selected = true},
                new SelectListItem {Text = "Last 60 Days", Value = DateTime.UtcNow.AddDays(-60).ToShortDateString()},
                new SelectListItem {Text = "Last 90 Days", Value = DateTime.UtcNow.AddDays(-90).ToShortDateString()},
                new SelectListItem {Text = "All", Value = ""}
            };

            this.PaymentStates = new List<SelectListItem>()
            {
                new SelectListItem
                {
                    Text = "All",
                    Value = "",
                    Selected = true
                }
            };
            this.PaymentStates.AddRange(Enum.GetValues(typeof(PaymentImportStateEnum))
                                                 .Cast<PaymentImportStateEnum>()
                                                 .Select(x => new SelectListItem
                                                 {
                                                     Text = x.ToString(),
                                                     Value = ((int)x).ToString()
                                                 })
                                                 .ToList());

            
        }

        [Display(Name = "Date Range")]
        public DateTime? DateRangeFrom { get; set; }

        [Display(Name = "Status")]
        public PaymentImportStateEnum? PaymentState { get; set; }

        [Display(Prompt = "Start Date")]
        [DateRangeCompare(false, "*.StartDate", "*.EndDate", DateRangeCompareType.LessThanOrEqual, ErrorMessage = "Invalid Date Range")]
        [DataType(DataType.Date)]
        public DateTime? StartDate { get; set; }

        [Display(Prompt = "End Date")]
        [DateRangeCompare(false, "*.EndDate", "*.StartDate", DateRangeCompareType.GreaterThanOrEqual, ErrorMessage = "Invalid Date Range")]
        [DataType(DataType.Date)]
        public DateTime? EndDate { get; set; }

        public IList<SelectListItem> DateRanges { get; set; }

        public IList<SelectListItem> PaymentStates { get; set; }

        [Display(Name = "Batch Number")]
        public string BatchNumber { get; set; }

        public IList<SelectListItem> BatchNumbers { get; set; }
    }
}
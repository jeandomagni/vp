﻿namespace Ivh.Web.Client.Models.PaymentQueue
{
    public class PaymentQueueGuarantorSearchFilterViewModel
    {
        public string HsGuarantorId { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string PhoneNumber { get; set; }

        public string AddressStreet1 { get; set; }

        public string AddressStreet2 { get; set; }

        public string City { get; set; }

        public string StateProvince { get; set; }

        public string PostalCode { get; set; }

        public decimal? AmountDue { get; set; }

        public bool IsValidSearch
        {
            get
            {
                return !string.IsNullOrWhiteSpace(this.HsGuarantorId) ||
                       !string.IsNullOrWhiteSpace(this.FirstName) ||
                       !string.IsNullOrWhiteSpace(this.LastName) ||
                       !string.IsNullOrEmpty(this.PhoneNumber) ||
                       !string.IsNullOrEmpty(this.AddressStreet1) ||
                       !string.IsNullOrEmpty(this.AddressStreet2) ||
                       !string.IsNullOrEmpty(this.City) ||
                       !string.IsNullOrEmpty(this.StateProvince) ||
                       !string.IsNullOrEmpty(this.PostalCode) ||
                       this.AmountDue.HasValue;
            }
        }
    }
}
﻿
namespace Ivh.Web.Client.Models.PaymentQueue
{
    using System.Collections.Generic;

    public class PaymentQueueViewModel
    {
        public IList<PaymentQueueItemViewModel> PaymentQueueItems { get; set; }
        public string AllPaymentsTotal { get; set; }
        public string PostedPaymentsTotal { get; set; }
        public string UnpostedPaymentsTotal { get; set; }
        public string NonVisitPayPaymentsTotal { get; set; }
        public int Page { get; set; }
        public decimal Total { get; set; }
        public int Records { get; set; }
    }
}
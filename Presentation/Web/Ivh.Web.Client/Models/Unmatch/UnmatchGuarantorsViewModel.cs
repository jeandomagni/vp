﻿namespace Ivh.Web.Client.Models.Unmatch
{
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using Common.Base.Enums;
    using Common.VisitPay.Enums;
    using Ivh.Common.Base.Utilities.Helpers;

    public class UnmatchGuarantorsViewModel
    {
        public int? HsGuarantorMatchDiscrepancyId { get; set; }

        [Required]
        public int VpGuarantorId { get; set; }

        public string VpGuarantorFirstName { get; set; }

        public string VpGuarantorLastName { get; set; }

        public string VpGuarantorLastNameFirstName => FormatHelper.LastNameFirstName(this.VpGuarantorLastName, this.VpGuarantorFirstName);
        public string VpGuarantorFirstNameLastName => FormatHelper.FirstNameLastName(this.VpGuarantorFirstName, this.VpGuarantorLastName);

        [Required]
        public IList<string> HsGuarantorSourceSystemKeys { get; set; }

        [Required(ErrorMessage = "Select at least one guarantor.")]
        public string HsGuarantorSourceSystemKeysValidationString { get; set; }

        [Required]
        public string Notes { get; set; }

        public UnmatchActionTypeEnum ActionType { get; set; }

        public UnmatchSourceEnum Source { get; set; }
    }
}
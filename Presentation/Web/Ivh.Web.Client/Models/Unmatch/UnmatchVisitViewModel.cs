﻿namespace Ivh.Web.Client.Models.Unmatch
{
    using Ivh.Common.Base.Utilities.Helpers;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using Common.Base.Enums;
    using Common.VisitPay.Enums;

    public class UnmatchVisitViewModel
    {
        public string SourceSystemKey { get; set; }
        public string SourceSystemKeyDisplay { get; set; }
        public int VpGuarantorId { get; set; }
        public int VisitId { get; set; }
        public string VisitStateFullDisplayName { get; set; }
        public string GuarantorName { get; set; }
        public string PatientLastName { get; set; }
        public string PatientFirstName { get; set; }
        public string PatientLastNameFirstName => FormatHelper.LastNameFirstName(this.PatientLastName, this.PatientFirstName);
        public string PatientFirstNameLastName => FormatHelper.FirstNameLastName(this.PatientFirstName, this.PatientLastName);
        public DateTime DischargeDate { get; set; }
        public decimal CurrentBalance { get; set; }

        public HsGuarantorMatchStatusEnum MatchStatus { get; set; }

        public IDictionary<KeyValuePair<string, string>, HsGuarantorMatchStatusEnum> AvailableMatchStatuses => new Dictionary<KeyValuePair<string, string>, HsGuarantorMatchStatusEnum>()
        {
            {new KeyValuePair<string,string>("Permanent","The visit will never be allowed to match with the Guarantor"),HsGuarantorMatchStatusEnum.UnmatchedHard},
            {new KeyValuePair<string,string>("Temporary","The visit be unmatched from the Guarantor but is eligible to match the Guarantor again"),HsGuarantorMatchStatusEnum.UnmatchedSoft}
        };

        [Required(ErrorMessage = "Visit # is required"), Compare("SourceSystemKeyDisplay", ErrorMessage = "Invalid Visit #")]
        [Display(Prompt = "Visit # is required")]
        public string SourceSystemKeyValidationString { get; set; }

        [Required]
        public string Notes { get; set; }
    }
}
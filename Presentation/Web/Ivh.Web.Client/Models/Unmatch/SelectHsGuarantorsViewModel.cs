﻿namespace Ivh.Web.Client.Models.Unmatch
{
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using Common.Base.Enums;
    using Common.VisitPay.Enums;

    public class SelectHsGuarantorsViewModel
    {
        [Required]
        public int VpGuarantorId { get; set; }

        [Required]
        public IList<string> HsGuarantorSourceSystemKeys { get; set; }

        public UnmatchActionTypeEnum ActionType { get; set; }
        public UnmatchSourceEnum UnmatchSource { get; set; }
    }
}
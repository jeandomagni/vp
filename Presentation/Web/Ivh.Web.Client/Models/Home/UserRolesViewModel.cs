﻿namespace Ivh.Web.Client.Models.Home
{
    public class UserRolesViewModel
    {
        public string VisitPayRoleDescription { get; set; }

        public string Name { get; set; }
    }
}
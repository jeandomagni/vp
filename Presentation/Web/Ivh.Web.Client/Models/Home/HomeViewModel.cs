﻿namespace Ivh.Web.Client.Models.Home
{
    using System.Collections.Generic;

    public class HomeViewModel
    {
        public HomeViewModel()
        {
            this.UserRoles = new List<UserRolesViewModel>();
        }

        public string TextRoles { get; set; }
        public string TextTop { get; set; }
        public IList<UserRolesViewModel> UserRoles { get; set; }
        public bool ShowCardReaderDeviceKeyModal { get; set; }
    }
}
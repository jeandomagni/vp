﻿namespace Ivh.Web.Client.Models.Manage
{
    using System;
    using System.ComponentModel.DataAnnotations;
    using Common.Base.Constants;
    using Common.VisitPay.Constants;
    using Common.Web.Attributes;

    public class UpgradeGuarantorViewModel
    {
        public int VisitPayUserId { get; set; }

        [Display(Name = "Username")]
        [Required(ErrorMessage = "Username is required.")]
        [MinLength(6, ErrorMessage = "This is an invalid username.  Please try again.")]
        public string Username { get; set; }

        [Display(Name = "Username Confirm")]
        [Required(ErrorMessage = "Username Confirm is required."), System.ComponentModel.DataAnnotations.Compare("Username", ErrorMessage = "The usernames do not match. Please try again.")]
        public string UsernameConfirm { get; set; }

        [Display(Name = "Email")]
        [Required(ErrorMessage = "Email is required."), RegularExpressionEmailAddress(TextRegionConstants.EmailInvalidLongMessage)]
        [DataType(DataType.EmailAddress)]
        public string Email { get; set; }

        [Display(Name = "Email Confirm")]
        [DataType(DataType.EmailAddress)]
        [Required(ErrorMessage = "Email Confirm is required."), System.ComponentModel.DataAnnotations.Compare("Email", ErrorMessage = "The email addresses do not match. Please try again.")]
        public string EmailConfirm { get; set; }

        [Display(Name = "Name", Prompt = "First Name")]
        [Required(ErrorMessage = "First Name is required.")]
        public string FirstName { get; set; }

        [Display(Prompt = "Middle Name")]
        public string MiddleName { get; set; }

        [Display(Prompt = "Last Name")]
        [Required(ErrorMessage = "Last Name is required.")]
        public string LastName { get; set; }

        [Display(Name = "Address", Prompt = "Street or PO Box")]
        [Required(ErrorMessage = "Street or PO Box is required.")]
        public string AddressStreet1 { get; set; }

        [Display(Prompt = "Apt, Suite, or Building #")]
        public string AddressStreet2 { get; set; }

        [Display(Prompt = "City")]
        [Required(ErrorMessage = "City is required.")]
        public string City { get; set; }

        [Required]
        public string State { get; set; }

        [Display(Prompt = "Zip")]
        [Required(ErrorMessage = "Zip is required.")]
        [DataType("Zip")]
        public string Zip { get; set; }

        [Display(Name = "Address", Prompt = "Street or PO Box")]
        //[Required(ErrorMessage = "Street or PO Box is required.")]
        public string MailingAddressStreet1 { get; set; }

        [Display(Prompt = "Apt, Suite, or Building #")]
        public string MailingAddressStreet2 { get; set; }

        [Display(Prompt = "City")]
        //[Required(ErrorMessage = "City is required.")]
        public string MailingCity { get; set; }

        //[Required]
        public string MailingState { get; set; }

        [Display(Prompt = "Zip")]
        //[Required(ErrorMessage = "Zip is required.")]
        //[DataType("Zip")]
        public string MailingZip { get; set; }

        [Display(Name = "Phone (Primary)", Prompt = "Phone Primary")]
        [Required(ErrorMessage = "Phone (Primary) is required.")]
        [DataType(DataType.PhoneNumber)]
        public string PhoneNumber { get; set; }

        [Required(ErrorMessage = "Phone (Primary) Type is required.")]
        public string PhoneNumberType { get; set; }

        [Display(Name = "Phone (Secondary)", Prompt = "Phone Secondary")]
        [DataType(DataType.PhoneNumber)]
        [NotEqual("*.PhoneNumber", TextRegionConstants.PhoneNumbersShouldNotMatch)]
        public string PhoneNumberSecondary { get; set; }

        [ConditionalRequired("PersonalInformation", "PhoneNumberSecondary")]
        public string PhoneNumberSecondaryType { get; set; }

        [Display(Name = "Last Four SSN", Prompt = "SSN")]
        [Required(ErrorMessage = "Last Four SSN is required."), RegularExpressionSsn4(ErrorMessage = "Invalid Social Security Number.")]
        [DataType("Ssn4")]
        public string Ssn4 { get; set; }

        [Display(Name = "Date of Birth", Prompt = "Date of Birth")]
        [Required(ErrorMessage = "Date of Birth is required.")]
        public string DateOfBirth { get; set; }
    }
}
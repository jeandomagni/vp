﻿namespace Ivh.Web.Client.Models.Guarantor
{
    public class PhoneTextSettingsViewModel
    {
        public string VisitPayUserId { get; set; }
        public int VpGuarantorId { get; set; }
        public string GuarantorName { get; set; }
    }
}
﻿namespace Ivh.Web.Client.Models.Support
{
    using Common.Web.Models.Support;

    public class SupportRequestActionViewModel : SupportRequestActionBaseModel
    {
        public int VisitPayUserId { get; set; }
    }
}
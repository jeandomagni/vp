﻿namespace Ivh.Web.Client.Models.Support
{
    using System.ComponentModel.DataAnnotations;

    public class CreateSupportRequestViewModel : Common.Web.Models.Support.CreateSupportRequestViewModel
    {

        public int VisitPayUserId { get; set; }

        [Display(Name = "Select a topic for your request:")]
        public int? SupportTopicId { get; set; }

        [Display(Name = "Select a sub topic:")]
        public int? SupportSubTopicId { get; set; }

        [Display(Name = "Assigned to:")]
        public int? AssignedVisitPayUserId { get; set; }

        
    }
}
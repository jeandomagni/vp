﻿namespace Ivh.Web.Client.Models.FinancePlan
{
    public class ReallocateInterestToPrincipalModel
    {
        public int FinancePlanId { get; set; }
        public decimal MaxReallocationAmount { get; set; }
        public decimal AmountToReallocate { get; set; }
        public decimal InterestPaid { get; set; }
    }
}
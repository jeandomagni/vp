﻿namespace Ivh.Web.Client.Models.FinancePlan
{
    using Common.VisitPay.Strings;
    using Common.Web.Models.Shared;
    
    public class FinancePlanCustomizedReceipt
    {
        public int VpGuarantorId { get; set; }
        public UserDateSummaryViewModel UserDateSummary { get; set; }
        public bool IsPatientOffer { get; set; }
        public int FinancePlanOfferId { get; set; }
        public string ApplicableDate => this.UserDateSummary.IsGracePeriod && this.UserDateSummary.PaymentDueDate.HasValue ? this.UserDateSummary.PaymentDueDate.Value.ToString(Format.DateFormat) : this.UserDateSummary.NextStatementDate.ToString(Format.DateFormat);
    }
}
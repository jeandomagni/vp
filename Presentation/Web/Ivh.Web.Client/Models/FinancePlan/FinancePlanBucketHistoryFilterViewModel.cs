﻿namespace Ivh.Web.Client.Models.FinancePlan
{
    public class FinancePlanBucketHistoryFilterViewModel
    {
        public int GuarantorVisitPayUserId { get; set; }
        public int FinancePlanId { get; set; }
    }
}
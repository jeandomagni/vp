﻿namespace Ivh.Web.Client.Models.FinancePlan
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Web.Mvc;

    public class FinancePlanLogFilterViewModel
    {
        public int VisitPayUserId { get; set; }
        public int FinancePlanId { get; set; }
        public List<string> SourceSystemKeys { get; set; } = new List<string>();

        public IList<SelectListItem> SourceSystemKeySelectList => this.SourceSystemKeys.Select(x =>
            new SelectListItem
            {
                Text = x,
                Value = x
            }).ToList();
    }
}
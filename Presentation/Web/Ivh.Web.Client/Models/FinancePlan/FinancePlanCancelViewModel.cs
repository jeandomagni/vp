﻿namespace Ivh.Web.Client.Models.FinancePlan
{
    using System.Collections.Generic;
    using System.Web.Mvc;
    using Common.Web.Models.FinancePlan;

    public class FinancePlanCancelViewModel
    {
        public FinancePlanCancelViewModel()
        {
            this.AvailableAgingCounts = new List<SelectListItem>();
        }

        public bool CanUserReageVisits { get; set; }
        public string PendingVisitStateFullDisplayName { get; set; }
        public FinancePlanDetailsViewModel FinancePlan { get; set; }

        public int ActiveUncollectableAgingCount { get; set; }
        public IList<SelectListItem> AvailableAgingCounts { get; set; }
    }
}
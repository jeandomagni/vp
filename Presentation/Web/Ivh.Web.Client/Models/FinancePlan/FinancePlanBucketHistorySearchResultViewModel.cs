﻿namespace Ivh.Web.Client.Models.FinancePlan
{
    public class FinancePlanBucketHistorySearchResultViewModel
    {
        public string Bucket { get; set; }
        public string Comment { get; set; }
        public string InsertDate { get; set; }
        public string VisitPayUserFullName { get; set; }
    }
}
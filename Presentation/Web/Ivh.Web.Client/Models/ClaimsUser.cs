﻿namespace Ivh.Web.Client.Models
{
    using System.Security.Claims;
    using Common.Base.Enums;
    using Common.VisitPay.Enums;

    public class ClaimsUser : ClaimsPrincipal
    {
        public ClaimsUser(ClaimsPrincipal principal) : base(principal)
        {
        }

        public string EmulationToken
        {
            get
            {
                Claim claim = this.FindFirst(ClaimTypeEnum.EmulationToken.ToString());
                return claim == null ? string.Empty : claim.Value;
            }
        }
    }
}
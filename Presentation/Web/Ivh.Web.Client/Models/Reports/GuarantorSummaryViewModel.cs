﻿namespace Ivh.Web.Client.Models.Reports
{
    public class GuarantorSummaryViewModel
    {

        public string ActionDate { get; set; }

        public string ActionNotes { get; set; }

        public string ActionUserName { get; set; }

        public string HsGuarantorMatchDiscrepancyStatusName { get; set; }

        public string HsGuarantorSourceSystemKey { get; set; }

        //keeps the grid happy to have this as it's own property
        //requirements say it's always the same as "previous"
        public string HsGuarantorSourceSystemKeyCurrent
        {
            get { return this.HsGuarantorSourceSystemKey; }
        }

        public string HsGuarantorUnmatchReasonDisplayName { get; set; }

        public string InsertDate { get; set; }

        public string VpGuarantorId { get; set; }

        public string VpGuarantorIdCurrent { get; set; }
    }
}
﻿namespace Ivh.Web.Client.Models.Reports
{
    using Application.Core.Common.Dtos;

    public class VisitUnmatchViewModel
    {
        public string HsGuarantorSourceSystemKey { get; set; }
        public string HsGuarantorSourceSystemKeyCurrent { get; set; }
        public string HsGuarantorUnmatchReasonDisplayName { get; set; }
        public string MatchedSourceSystemKey { get; set; }
        public string UnmatchActionDate { get; set; }
        public string UnmatchActionNotes { get; set; }
        public string UnmatchActionUserName { get; set; }
        public string VpGuarantorId { get; set; }
        public string VpGuarantorIdCurrent { get; set; }
    }
}
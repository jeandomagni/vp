﻿namespace Ivh.Web.Client.Models.Reports
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.Web.Mvc;

    public class VisitUnmatchFilterViewModel
    {
        public VisitUnmatchFilterViewModel()
        {
            this.DateRanges = new List<SelectListItem>
            {
                new SelectListItem {Text = "Last 30 Days", Value = (-30).ToString()},
                new SelectListItem {Text = "Last 60 Days", Value = (-60).ToString()},
                new SelectListItem {Text = "Last 90 Days", Value = (-90).ToString()},
                new SelectListItem {Text = "All", Value = ""}
            };
        }

        [Display(Name = "Date Range")]
        public int? DateRangeFrom { get; set; }

        [Display(Prompt = "Start Date")]
        public DateTime? SpecificDateFrom { get; set; }

        [Display(Prompt = "End Date")]
        public DateTime? SpecificDateTo { get; set; }

        [Display(Name = "HS Visit ID", Prompt = "HS Visit ID")]
        public string MatchedSourceSystemKey { get; set; }

        [Display(Name = "Previous [[HsGuarantorPatientIdentifier]]", Prompt = "Previous [[HsGuarantorPatientIdentifier]]")]
        public string HsGuarantorSourceSystemKey { get; set; }

        [Display(Name = "Previous [[VpGuarantorPatientIdentifier]]", Prompt = "Previous [[VpGuarantorPatientIdentifier]]")]
        public int? VpGuarantorId { get; set; }

        public IList<SelectListItem> DateRanges { get; set; }
    }
}
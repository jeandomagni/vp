﻿namespace Ivh.Web.Client.Models.Reports
{
    using System.Collections.Generic;

    public class AnalyticReportListViewModel
    {
        public List<AnalyticReportViewModel> AnalyticReports { get; set; }
        public List<string> Classifications { get; set; }
    }
}
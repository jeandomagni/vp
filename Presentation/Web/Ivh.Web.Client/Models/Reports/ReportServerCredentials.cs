﻿namespace Ivh.Web.Client.Models.Reports
{
    using System;
    using System.Net;
    using System.Security.Principal;
    using Microsoft.Reporting.WebForms;

    [Serializable]
    public class ReportServerCredentials : IReportServerCredentials
    {
        private readonly string _userName;
        private readonly string _password;
        private readonly string _domain;

        public ReportServerCredentials(string userName, string password, string domain)
        {
            this._userName = userName;
            this._password = password;
            this._domain = domain;
        }

        public WindowsIdentity ImpersonationUser
        {
            get
            {
                // Use default identity.
                return null;
            }
        }

        public ICredentials NetworkCredentials
        {
            get
            {
                // Use default identity.
                return new NetworkCredential(this._userName, this._password, this._domain);
            }
        }

        public bool GetFormsCredentials(out Cookie authCookie, out string user, out string password, out string authority)
        {
            user = null;
            password = null;
            authority = null;
            authCookie = null;
            return false;
        }
    }
}
﻿namespace Ivh.Web.Client.Models.Reports
{
    public class AnalyticReportViewModel
    {
        public string ReportName { get; set; }
        public string ReportId { get; set; }
        public string DatasetId { get; set; }
        public string EmbedUrl { get; set; }
        public string WebUrl { get; set; }
        public string GroupId { get; set; }
        public string Classification { get; set; }
    }
}
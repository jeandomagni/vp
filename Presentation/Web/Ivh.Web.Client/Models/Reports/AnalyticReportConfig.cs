﻿namespace Ivh.Web.Client.Models.Reports
{
    using Microsoft.PowerBI.Api.V2.Models;

    public class AnalyticReportConfig
    {
        public string Id { get; set; }

        public string Name { get; set; }

        public string EmbedUrl { get; set; }

        public string Token { get; set; }

        public EmbedToken EmbedToken { get; set; }

        public string ErrorMessage { get; internal set; }
    }
}
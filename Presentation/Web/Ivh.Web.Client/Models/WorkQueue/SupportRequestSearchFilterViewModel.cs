﻿namespace Ivh.Web.Client.Models.WorkQueue
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.Web.Mvc;
    using Common.Base.Utilities.Extensions;
    using Common.VisitPay.Enums;

    public class SupportRequestSearchFilterViewModel
    {
        public SupportRequestSearchFilterViewModel()
        {
            this.SupportRequestStatuses = new List<SelectListItem>
            {
                new SelectListItem {Text = "All", Value = "", Selected = true},
                new SelectListItem {Text = SupportRequestStatusEnum.Open.GetDescription(), Value = ((int)SupportRequestStatusEnum.Open).ToString()},
                new SelectListItem {Text = SupportRequestStatusEnum.Closed.GetDescription(), Value = ((int)SupportRequestStatusEnum.Closed).ToString()}
            };
            this.DateRanges = new List<SelectListItem>
            {
                new SelectListItem {Text = "Last 30 Days", Value = DateTime.UtcNow.AddDays(-30).ToShortDateString()},
                new SelectListItem {Text = "Last 60 Days", Value = DateTime.UtcNow.AddDays(-60).ToShortDateString()},
                new SelectListItem {Text = "Last 90 Days", Value = DateTime.UtcNow.AddDays(-90).ToShortDateString()},
                new SelectListItem {Text = "All", Value = ""}
            };
            this.SupportAdminUsers = new List<SelectListItem>();
        }

        public SupportRequestStatusEnum? SupportRequestStatus { get; set; }
        public DateTime? DateRangeFrom { get; set; }

        [Display(Prompt = "Case ID")]
        public string SupportRequestDisplayId { get; set; }

        [Display(Name = "Assigned To")]
        public int? AssignedToVisitPayUserId { get; set; }

        [Display(Name = "Read")]
        public bool ReadMessages { get; set; }

        [Display(Name = "Unread")]
        public bool UnreadMessages { get; set; }

        public string FacilitySourceSystemKey { get; set; }

        public IList<SelectListItem> SupportRequestStatuses { get; set; }
        public IList<SelectListItem> DateRanges { get; set; }
        public IList<SelectListItem> SupportAdminUsers { get; set; }
        public IList<SelectListItem> Facilities { get; set; }
    }
}
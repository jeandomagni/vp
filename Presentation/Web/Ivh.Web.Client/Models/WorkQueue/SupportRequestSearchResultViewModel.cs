﻿namespace Ivh.Web.Client.Models.WorkQueue
{
    using Common.Web.Models.Support;

    public class SupportRequestSearchResultViewModel : SupportRequestResultViewModel
    {
        public string AssignedToVisitPayUserName { get; set; }

        public bool CanTakeAction { get; set; }

        public string[] FacilitySourceSystemKeys { get; set; }

        public string GuarantorFullName { get; set; }

        public bool HasDraftMessage { get; set; }

        public bool IsAccountClosed { get; set; }

        public bool IsClosed { get; set; }

        public bool IsFlaggedForFollowUp { get; set; }

        public string LastMessageBy { get; set; }

        public string LastMessageDate { get; set; }

        public int SupportRequestStatusId { get; set; }
        
        public int VisitPayUserId { get; set; }

        public int VpGuarantorId { get; set; }
    }
}
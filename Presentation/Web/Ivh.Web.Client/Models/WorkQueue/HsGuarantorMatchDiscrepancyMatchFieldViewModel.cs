﻿namespace Ivh.Web.Client.Models.WorkQueue
{
    public class HsGuarantorMatchDiscrepancyMatchFieldViewModel
    {
        public string HsGuarantorMatchDiscrepancyMatchFieldName { get; set; }
        public string HsGuarantorMatchDiscrepancyMatchFieldCurrentValue { get; set; }
        public string HsGuarantorMatchDiscrepancyMatchFieldNewValue { get; set; }
        public bool IsDiscrepancy { get; set; }
    }
}
﻿namespace Ivh.Web.Client.Models.WorkQueue
{
    using System.Collections.Generic;
    using Common.Base.Enums;
    using Common.VisitPay.Enums;

    public class HsGuarantorMatchDiscrepancyViewModel
    {
        public string ActionDate { get; set; }
        public string ActionNotes { get; set; }
        public string ActionUserName { get; set; }
        public HsGuarantorMatchDiscrepancyStatusEnum HsGuarantorMatchDiscrepancyStatus { get; set; }
        public int HsGuarantorMatchDiscrepancyId { get; set; }
        public string HsGuarantorMatchDiscrepancyStatusName { get; set; }
        public string HsGuarantorSourceSystemKey { get; set; }
        public string InsertDate { get; set; }
        public string VpGuarantorFullName { get; set; }
        public int VpGuarantorId { get; set; }

        public IList<HsGuarantorMatchDiscrepancyMatchFieldViewModel> HsGuarantorMatchDiscrepancyMatchFields { get; set; } 
    }
}
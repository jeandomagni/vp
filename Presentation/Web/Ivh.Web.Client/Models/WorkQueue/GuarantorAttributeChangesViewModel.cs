﻿namespace Ivh.Web.Client.Models.WorkQueue
{
    using System.Collections.Generic;

    public class GuarantorAttributeChangesViewModel
    {
        public IList<KeyValuePair<string, string>> MatchFields { get; set; }
        public GuarantorAttributeChangesFilterViewModel Filter { get; set; }
    }
}
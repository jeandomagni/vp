﻿namespace Ivh.Web.Client.Models.WorkQueue
{
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.Web.Mvc;
    using Common.Base.Enums;
    using Common.VisitPay.Enums;

    public class GuarantorAttributeChangesFilterViewModel
    {
        public GuarantorAttributeChangesFilterViewModel()
        {
            this.DateRanges = new List<SelectListItem>
            {
                new SelectListItem {Text = "Last 30 Days", Value = (-30).ToString()},
                new SelectListItem {Text = "Last 60 Days", Value = (-60).ToString()},
                new SelectListItem {Text = "Last 90 Days", Value = (-90).ToString()},
                new SelectListItem {Text = "All", Value = ""}
            };
        }

        [Display(Name = "Status")]
        public HsGuarantorMatchDiscrepancyStatusEnum? HsGuarantorMatchDiscrepancyStatus { get; set; }

        [Display(Name = "Date Range")]
        public int? DateRangeFrom { get; set; }

        [Display(Name = "[[HsGuarantorPatientIdentifier]]", Prompt = "[[HsGuarantorPatientIdentifier]]")]
        public string HsGuarantorSourceSystemKey { get; set; }

        [Display(Name = "[[VpGuarantorPatientIdentifier]]", Prompt = "[[VpGuarantorPatientIdentifier]]")]
        public int? VpGuarantorId { get; set; }

        public IList<SelectListItem> HsGuarantorMatchDiscrepancyStatuses { get; set; }

        public IList<SelectListItem> DateRanges { get; set; }
    }
}
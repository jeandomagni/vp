﻿namespace Ivh.Web.Client.Models.Payment
{
    public class PaymentMethodsListViewModel
    {
        private readonly PaymentMethodSelectType _selectType;

        public PaymentMethodsListViewModel(int currentVisitPayUserId, PaymentMethodSelectType selectType, bool isAchEnabled, bool isCardEnabled)
        {
            this._selectType = selectType;
            
            this.IsAchEnabled = isAchEnabled;
            this.IsCardEnabled = isCardEnabled;
            this.CurrentVisitPayUserId = currentVisitPayUserId;
        }

        public int CurrentVisitPayUserId { get; }
        
        public bool IsAchEnabled { get; }

        public bool IsCardEnabled { get; }

        public bool IsSetPrimaryEnabled => this._selectType == PaymentMethodSelectType.SetPrimary;

        public bool IsSelectEnabled => this._selectType == PaymentMethodSelectType.Select;
        public bool ShouldShowCardDeviceKeyModal { get; set; }
    }

    public enum PaymentMethodSelectType
    {
        SetPrimary = 1,
        Select = 2
    }
}
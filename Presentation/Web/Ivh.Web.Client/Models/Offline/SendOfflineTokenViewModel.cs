﻿namespace Ivh.Web.Client.Models.Offline
{
    using System.ComponentModel.DataAnnotations;
    using Common.VisitPay.Constants;
    using Common.Web.Attributes;
    using Common.Web.Models.Shared;

    public class SendOfflineTokenViewModel
    {
        public int VpGuarantorId { get; set; }
        
        [VpRequired]
        [Display(Name = TextRegionConstants.EmailAddress, Prompt = TextRegionConstants.EmailAddress)]
        [RegularExpressionEmailAddress(TextRegionConstants.EmailInvalid)]
        public string EmailAddress { get; set; }
        
        [RequiredIf(nameof(SendSms), true)]
        [Display(Name = TextRegionConstants.PhoneNumber, Prompt = TextRegionConstants.PhoneNumber)]
        [DataType(DataType.PhoneNumber)]
        public string PhoneNumber { get; set; }
        
        [Display(Name = "Send a Text Message")]
        public bool SendSms { get; set; }

        public bool SendFinancePlanNotification { get; set; }

        public CmsViewModel Content { get; set; }

        public bool ShowRemovePaperCommunicationPreferences { get; set; }

        [Display(Name = TextRegionConstants.RemovePaperCommunicationPreferences, Prompt = TextRegionConstants.RemovePaperCommunicationPreferences)]
        public bool RemovePaperCommunicationPreferences { get; set; }
    }
}
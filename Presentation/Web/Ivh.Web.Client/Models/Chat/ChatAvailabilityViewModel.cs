﻿
namespace Ivh.Web.Client.Models.Chat
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web;
    using Common.Web.Attributes;

    public class ChatAvailabilityViewModel
    {
        public int? ChatAvailabilityId { get; set; }

        
        public int? DayOfWeek { get; set; }

        public string DayOfWeekDisplay { get; set; }

        [RequiredIf("DayOfWeek", null, ErrorMessage = "Date is Required")]
        public DateTime? Date { get; set; }

        [TimeSpanCompare(false, "TimeStart", "TimeEnd", TimeSpanCompareType.LessThan, ErrorMessage = "Chat Start Time must be before Chat End Time")]
        public TimeSpan? TimeStart { get; set; }

        [TimeSpanCompare(false, "TimeEnd", "TimeStart", TimeSpanCompareType.GreaterThan, ErrorMessage = "Chat End Time must be after Chat Start Time")]
        public TimeSpan? TimeEnd { get; set; }

        public bool Offline => (!this.TimeStart.HasValue && !this.TimeEnd.HasValue);
    }
}
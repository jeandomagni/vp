﻿namespace Ivh.Web.Client.Models.Account
{
    using System.Collections.Generic;
    using Common.Web.Models.Account;

    public class SecuritySetupViewModel
    {
        public string UserName { get; set; }

        public IList<SecurityQuestionViewModel> SecurityQuestions { get; set; }
    }
}
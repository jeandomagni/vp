﻿namespace Ivh.Web.Client.Models.Account
{
    using System.ComponentModel.DataAnnotations;
    using Common.Base.Constants;
    using Common.VisitPay.Constants;
    using Common.Web.Attributes;

    public class ProfileEditClientViewModel
    {
        [Display(Name = "Name", Prompt = "First Name")]
        [Required(ErrorMessage = "First Name is required.")]
        public string FirstName { get; set; }

        [Display(Name = "Middle", Prompt = "Middle Name")]
        public string MiddleName { get; set; }

        [Display(Name = "Last", Prompt = "Last Name")]
        [Required(ErrorMessage = "Last Name is required.")]
        public string LastName { get; set; }

        [Display(Name = "Username", Prompt = "Username")]
        [Required(ErrorMessage = "Username is required.")]
        [MinLength(6, ErrorMessage = "This is an invalid username.  Please try again.")]
        public string UserName { get; set; }

        public string UserNameOriginal { get; set; }

        [Display(Name = "Re-enter Username", Prompt = "Re-enter Username")]
        [CompareToIfNotEqual("UserName", "UserNameOriginal", "UserName", false, TextRegionConstants.UsernamesDoNotMatch)]
        public string UserNameConfirm { get; set; }

        [Display(Name = "Notification Email", Prompt = "Notification Email")]
        [RegularExpressionEmailAddress(TextRegionConstants.NotificationEmailInvalid)]
        [RequiredIf("IsOfflineUser", false)]
        [DataType(DataType.EmailAddress)]
        public string Email { get; set; }

        public string EmailOriginal { get; set; }

        [Display(Name = "Re-enter Notification Email", Prompt = "Re-enter Notification Email")]
        [DataType(DataType.EmailAddress)]
        [CompareToIfNotEqual("Email", "EmailOriginal", "Email", false, TextRegionConstants.NotificationEmailsDoNotMatch)]
        public string EmailConfirm { get; set; }

        [Display(Name = "Address", Prompt = "Street or PO Box")]
        [Required(ErrorMessage = "Street or PO Box is required.")]
        public string AddressStreet1 { get; set; }

        [Display(Prompt = "Apt, Suite, or Building #")]
        public string AddressStreet2 { get; set; }

        [Display(Name = "City", Prompt = "City")]
        [Required(ErrorMessage = "City is required.")]
        public string City { get; set; }

        [Display(Name = "State")]
        [Required(ErrorMessage = "State is required.")]
        public string State { get; set; }

        [Display(Name = "Zip", Prompt = "Zip")]
        [Required(ErrorMessage = "Zip is required.")]
        [DataType("Zip")]
        public string Zip { get; set; }

        [Display(Name = "Mailing Address", Prompt = "Street or PO Box")]
        //[Required(ErrorMessage = "Street or PO Box is required.")]
        public string MailingAddressStreet1 { get; set; }

        [Display(Prompt = "Apt, Suite, or Building #")]
        public string MailingAddressStreet2 { get; set; }

        [Display(Name = "City", Prompt = "City")]
        //[Required(ErrorMessage = "City is required.")]
        public string MailingCity { get; set; }

        [Display(Name = "State")]
        //[Required(ErrorMessage = "State is required.")]
        public string MailingState { get; set; }

        [Display(Name = "Zip", Prompt = "Zip")]
        //[Required(ErrorMessage = "Zip is required.")]
        //[DataType("Zip")]
        public string MailingZip { get; set; }

        public bool IsOfflineUser { get; set; }
        public bool CanEditMailingAddress { get; set; }
    }
}
namespace Ivh.Web.Client.Models.Account
{
    using Ivh.Common.Web.Attributes;
    using System.ComponentModel.DataAnnotations;

    public class ResetPasswordViewModel
    {
        public string UserName { get; set; }
        public string TempPassword { get; set; }
        public bool IsPasswordExpired { get; set; }
        public bool IsSecureCodeRequired { get; set; }
        public bool IsSecurityQuestionRequired { get; set; }
        public int SecurityQuestionId { get; set; }
        public bool HideUserName { get; set; }

        [RequiredIf("IsSecureCodeRequired", true, ErrorMessage="Secure Code is required.")]
        [DataType(DataType.Text)]
        [Display(Name = "Secure Code:", Prompt = "Secure Code")]
        public string SecureCode { get; set; }

        [Required(ErrorMessage = "New Password is required.")]
        [DataType(DataType.Password)]
        [Display(Name = "New Password:", Prompt = "New Password")]
        public string NewPassword { get; set; }

        [Required(ErrorMessage = "Confirm Password is required.")]
        [DataType(DataType.Password)]
        [Display(Name = "Confirm Password:", Prompt = "Confirm Password")]
        [Compare("NewPassword", ErrorMessage = "Your passwords do not match.")]
        public string ConfirmPassword { get; set; }

        [DataType(DataType.Text)]
        [Display(Name = "Security Question:")]
        public string SecurityQuestion { get; set; }

        [RequiredIf("IsSecurityQuestionRequired", true, ErrorMessage = "Security answer is required.")]
        [DataType(DataType.Text)]
        [Display(Name = "Answer:", Prompt = "Security Question Answer")]
        public string SecurityAnswer { get; set; }
    }
}
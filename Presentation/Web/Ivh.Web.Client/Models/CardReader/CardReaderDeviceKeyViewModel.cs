﻿namespace Ivh.Web.Client.Models.CardReader
{
    using System.ComponentModel.DataAnnotations;
    using System.Web.Mvc;

    public class CardReaderDeviceKeyViewModel
    {
        [Display(Name = "Enter Your Card Reader Device Key",Prompt = "Card Reader Device Key")]
        [Required(ErrorMessage = "Device key is required")]
        [Remote("IsCardReaderDeviceKeyAllowed", "CardReader", HttpMethod = "POST", ErrorMessage = "Enter Valid Card Reader Device Key")]
        public string CardReaderDeviceKey { get; set; }

        [Display(Name = "Enter Your Call Pause Credentials",Prompt = "Call Pause Credentials")]
        [Required(ErrorMessage = "User Account is required")]
        [Remote("IsCardReaderUserAccountAllowed", "CardReader", HttpMethod = "POST", ErrorMessage = "Enter Valid Card Reader User Account")]
        public string CardReaderUserAccount { get; set; }
    }
}
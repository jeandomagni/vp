﻿
namespace Ivh.Web.Client.Models
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Security.Principal;
    using System.Web;
    using System.Web.Mvc;
    using Application.Core.Common.Dtos;
    using Common.VisitPay.Strings;
    using Microsoft.Ajax.Utilities;

    public class NavViewModel
    {
        private IPrincipal CurrentUser => HttpContext.Current.User;

        public IList<NavItemViewModel> NavItems { get; set; }

        public NavViewModel(
            ClientDto clientDto, 
            int unreadMessageCountFromClient, 
            int unreadMessageCountToClient, 
            bool visitPayReportsIsEnabled, 
            bool demoClientReportsDashboardIsEnabled,
            bool offlineVisitPayIsEnabled,
            bool visitPayDocumentIsEnabled,
            bool analyticReportsIsEnabled,
            IList<AnalyticReportDto> analyticReportList,
            bool chatIsEnabled,
            bool lockboxIsEnabled,
            bool cardReaderIsEnabled)
        {
            UrlHelper urlHelper = new UrlHelper(HttpContext.Current.Request.RequestContext);

            IList<NavItemViewModel> allNavItems = new List<NavItemViewModel>();

            if (offlineVisitPayIsEnabled)
            {
                allNavItems.Add(new NavItemViewModel("Create", urlHelper.Action("CreateVpGuarantor", "Offline"),
                false, VisitPayRoleStrings.Csr.CallCenter));
            }

            allNavItems.Add(new NavItemViewModel("Search", urlHelper.Action("SearchGuarantors", "Search"), false,
                VisitPayRoleStrings.Csr.CSR,
                VisitPayRoleStrings.Csr.ReconfigureFP,
                VisitPayRoleStrings.Csr.SupportAdmin,
                VisitPayRoleStrings.Csr.SupportView,
                VisitPayRoleStrings.Csr.UserAdmin,
                VisitPayRoleStrings.Payment.PaymentRefund,
                VisitPayRoleStrings.Payment.PaymentRefundReturnedCheck,
                VisitPayRoleStrings.Payment.PaymentVoid)
            );

            NavItemViewModel workQueues = new NavItemViewModel("Work Queues", "javascript:;", false)
            {
                Children = new List<NavItemViewModel>
                {                  
                    new NavItemViewModel("Guarantor Support Requests", urlHelper.Action("SupportRequests", "WorkQueue"), false, VisitPayRoleStrings.Csr.SupportAdmin, VisitPayRoleStrings.Csr.SupportView, VisitPayRoleStrings.Csr.SupportCreate),
                    new NavItemViewModel($"{clientDto.AppSupportRequestNamePrefix} Support Tickets", urlHelper.Action("ClientSupportRequests", "WorkQueue"), false, VisitPayRoleStrings.Miscellaneous.VpSupportTicketsManage),
                    new NavItemViewModel("Manage Guarantor Attribute Changes", urlHelper.Action("GuarantorAttributeChanges", "WorkQueue"), false, VisitPayRoleStrings.Miscellaneous.Unmatch),
                    new NavItemViewModel("Invite New Guarantor", urlHelper.Action("InviteGuarantor", "Invite"), false,
                        VisitPayRoleStrings.Csr.CSR,
                        VisitPayRoleStrings.Csr.SupportAdmin,
                        VisitPayRoleStrings.Csr.SupportView,
                        VisitPayRoleStrings.Csr.UserAdmin,
                        VisitPayRoleStrings.Payment.PaymentRefund,
                        VisitPayRoleStrings.Payment.PaymentRefundReturnedCheck,
                        VisitPayRoleStrings.Payment.PaymentVoid)
                }
            };

            if (chatIsEnabled)
            {
                NavItemViewModel chatNavItem = new NavItemViewModel("Guarantor Chat Requests", urlHelper.Action("Index", "Chat"), false, VisitPayRoleStrings.Csr.ChatOperator, VisitPayRoleStrings.Csr.ChatOperatorAdmin);
                workQueues.Children.Insert(0, chatNavItem);
            }

            if (lockboxIsEnabled)
            {
                NavItemViewModel paymentQueueNavItem = new NavItemViewModel("Payment Queue", urlHelper.Action("Index", "PaymentQueue"), false, VisitPayRoleStrings.Csr.PaymentQueue);
                workQueues.Children.Add(paymentQueueNavItem);
            }

            allNavItems.Add(workQueues);

            allNavItems.Add(new NavItemViewModel("Admin Tools", "javascript:;", false)
            {
                Children = new List<NavItemViewModel>
                {
                    new NavItemViewModel("Client User Management", urlHelper.Action("ClientUserManagement", "Admin"), false, VisitPayRoleStrings.Miscellaneous.IvhAdmin, VisitPayRoleStrings.Miscellaneous.ClientUserManagement, VisitPayRoleStrings.Restricted.IvhUserAuditor, VisitPayRoleStrings.Restricted.ClientUserAuditor),
                    new NavItemViewModel("Payment Admin", "https://vault.trustcommerce.com", true, VisitPayRoleStrings.Miscellaneous.IvhAdmin),
                    new NavItemViewModel("CMS Admin", urlHelper.Action("CmsAdmin", "Admin"), false, VisitPayRoleStrings.Miscellaneous.IvhAdmin),
                    new NavItemViewModel("Resolve System Exceptions", urlHelper.Action("ResolveSystemExceptions", "Admin"), false, VisitPayRoleStrings.Miscellaneous.IvhAdmin),
                    new NavItemViewModel($"{clientDto.AppSupportRequestNamePrefix} Support Tickets - Admin ({unreadMessageCountFromClient})", urlHelper.Action("ClientAdminSupportRequests", "ClientSupport"), false, VisitPayRoleStrings.Miscellaneous.VpSupportTicketAdmin),
                    new NavItemViewModel("PCI Audit", urlHelper.Action("PciAudit", "Admin"), false, VisitPayRoleStrings.Restricted.IvhPciAuditor, VisitPayRoleStrings.Restricted.ClientPciAuditor),
                    new NavItemViewModel("Audit Log Review", urlHelper.Action("AuditLogReview", "Admin"), false, VisitPayRoleStrings.Restricted.AuditReviewer),
                    new NavItemViewModel("View Journal Event Logs", urlHelper.Action("AuditEventLog", "Admin"), false, VisitPayRoleStrings.Restricted.AuditEventLogViewer),
                }
            });

            if (chatIsEnabled)
            {
                NavItemViewModel adminToolsNavItems = allNavItems.FirstOrDefault(x => x.Text == "Admin Tools");
                if (adminToolsNavItems != null)
                {
                    NavItemViewModel chatAdminNavItem = new NavItemViewModel("Chat Admin", urlHelper.Action("ChatAdmin", "Chat"), false, VisitPayRoleStrings.Csr.ChatOperatorAdmin);
                    adminToolsNavItems.Children.Add(chatAdminNavItem);
                }
            }

            if (visitPayDocumentIsEnabled)
            {
                NavItemViewModel adminToolsNavItmens = allNavItems.FirstOrDefault(x => x.Text == "Admin Tools");
                adminToolsNavItmens?.Children.Add(new NavItemViewModel("VisitPay Documents", urlHelper.Action("Index", "VisitPayDocument"), false, VisitPayRoleStrings.Miscellaneous.VisitPayDocument));
            }
            
            List<NavItemViewModel> reportNavItems = new List<NavItemViewModel>
            {
                new NavItemViewModel("Guarantor Summary Report", urlHelper.Action("GuarantorSummaryReport", "Reporting"), false, VisitPayRoleStrings.Miscellaneous.Unmatch),
                new NavItemViewModel("Visit Unmatch Report", urlHelper.Action("VisitUnmatchReport", "Reporting"), false, VisitPayRoleStrings.Miscellaneous.Unmatch),
            };
            if (visitPayReportsIsEnabled)
            {
                reportNavItems.Add(new NavItemViewModel("VisitPay Reports", urlHelper.Action("SsrsReport", "Reporting"), false, VisitPayRoleStrings.Restricted.VisitPayReports, VisitPayRoleStrings.Miscellaneous.IvhAdmin));
            }
            
            if (demoClientReportsDashboardIsEnabled)
            {
                reportNavItems.Add(new NavItemViewModel("Report Dashboard", urlHelper.Action("ReportDashboard", "Demo"), false, VisitPayRoleStrings.Miscellaneous.IvhAdmin));
            }

            allNavItems.Add(new NavItemViewModel("Reports", "javascript:;", false)
            {
                Children = reportNavItems
            });

            if (analyticReportsIsEnabled && analyticReportList != null)
            {
                List<NavItemViewModel> analyticReportNavItems = new List<NavItemViewModel>();
                List<string> classifications = analyticReportList.Select(x => x.Classification).Distinct().ToList();
                foreach (string classification in classifications)
                {
                    analyticReportNavItems.Add(new NavItemViewModel(classification, null, false, VisitPayRoleStrings.Restricted.VisitPayReports, VisitPayRoleStrings.Miscellaneous.IvhAdmin));
                    foreach (AnalyticReportDto analyticReport in analyticReportList.Where(y => y.Classification.Equals(classification, StringComparison.OrdinalIgnoreCase)).ToList())
                    {
                        NavItemViewModel item = new NavItemViewModel(analyticReport.DisplayName,
                            urlHelper.Action("AnalyticReport", "Reporting", new {reportId = analyticReport.ReportId, reportName = analyticReport.ReportName, groupId = analyticReport.GroupId, vpReportEmbedUrl = analyticReport.EmbedUrl}),
                            false, VisitPayRoleStrings.Miscellaneous.IvhAdmin);
                        item.Id = analyticReport.ReportId;
                        analyticReportNavItems.Add(item);
                    }
                }

                allNavItems.Add(new NavItemViewModel("Analytic Reports", "javascript:;", false)
                {
                    Children = analyticReportNavItems
                });
            }

            NavItemViewModel myAccount = new NavItemViewModel("My Account", "javascript:;", false)
            {
                Children = new List<NavItemViewModel>
                {
                    new NavItemViewModel("Edit Security Questions", urlHelper.Action("SecurityQuestions", "Account"), false, VisitPayRoleStrings.System.Client),
                    new NavItemViewModel("Edit Password", urlHelper.Action("EditPassword", "Account"), false, VisitPayRoleStrings.System.Client),
                    new NavItemViewModel($"My {clientDto.AppSupportRequestNamePrefix} Support Tickets ({unreadMessageCountToClient})", urlHelper.Action("ClientUserSupportRequests", "ClientSupport"), false, VisitPayRoleStrings.Miscellaneous.VpSupportTickets)
                }
            };

            if (cardReaderIsEnabled)
            {
                myAccount?.Children.Add(new NavItemViewModel("Manage Card Reader Device", urlHelper.Action("ManageCardReaderDevice", "CardReader"), false, VisitPayRoleStrings.Payment.CardReader));
            }

            allNavItems.Add(myAccount);

            IList<NavItemViewModel> withAccess = allNavItems.Where(
                x => x.Children.Any(y => y.Roles.Any(z => this.CurrentUser.IsInRole(z))) //NavItemViewModels with children
                || x.Roles.Any(y => this.CurrentUser.IsInRole(y))) //NavItemViewModels without children
                .ToList();

            //assign children collection with subset of children that contain the user's roles
            withAccess.ForEach(x => x.Children = x.Children.Where(y => y.Roles.Any(z => this.CurrentUser.IsInRole(z))).ToList());

            this.NavItems = withAccess;
        }
    }

    public class NavItemViewModel
    {
        public NavItemViewModel(string text, string url, bool newWindowOrTab, params string[] roles)
        {
            this.Children = new List<NavItemViewModel>();
            this.Roles = roles?.ToList() ?? new List<string>();
            this.NewWindowOrTab = newWindowOrTab;
            this.Text = text;
            this.Url = url;
            this.Id = this.Text.Replace(" ", "");
        }

        public string Url { get; set; }
        public string Text { get; set; }
        public bool NewWindowOrTab  { get; set; }
        public IList<string> Roles { get; set; }
        public IList<NavItemViewModel> Children { get; set; }
        public string Id { get; set; }
    }
}
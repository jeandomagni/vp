﻿namespace Ivh.Web.Client.Models
{
    using System.Collections.Generic;
    using System.Web.Mvc;

    public class VisitStatesViewModel
    {
        public IList<SelectListItem> VisitStates { get; set; }
    }
}
﻿using Ivh.Common.Base.Utilities.Helpers;

namespace Ivh.Web.Client.Models.Admin
{
    public class ClientUserSearchResultViewModel
    {
        public int VisitPayUserId { get; set; }
        public string LastName { get; set; }
        public string FirstName { get; set; }
        public string UserName { get; set; }
        public string Roles { get; set; }
        public string InsertDate { get; set; }
        public string LastLoginDate { get; set; }
        public string IsLocked { get; set; }
        public string IsDisabled { get; set; }
        public string LastNameFirstName => FormatHelper.LastNameFirstName(this.LastName, this.FirstName);
        public string FirstNameLastName => FormatHelper.FirstNameLastName(this.FirstName, this.LastName);
    }
}
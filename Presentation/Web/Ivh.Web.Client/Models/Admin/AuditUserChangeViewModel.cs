﻿using Ivh.Common.Base.Utilities.Helpers;

namespace Ivh.Web.Client.Models.Admin
{
    public class AuditUserChangeViewModel
    {
        public string UserName { get; set; }
        public string Email { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string EventDate { get; set; }
        public string EventType { get; set; }
        public string IpAddress { get; set; }
        public string TargetUserName { get; set; }
        public string TargetVisitPayUserId { get; set; }
        public string LastNameFirstName => FormatHelper.LastNameFirstName(this.LastName, this.FirstName);
        public string FirstNameLastName => FormatHelper.FirstNameLastName(this.FirstName, this.LastName);
    }
}
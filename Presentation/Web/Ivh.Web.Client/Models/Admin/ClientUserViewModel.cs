﻿namespace Ivh.Web.Client.Models.Admin
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.Web.Mvc;
    using Common.VisitPay.Constants;
    using Common.Web.Attributes;
    using Ivh.Common.Base.Utilities.Helpers;

    public class ClientUserViewModel
    {
        public ClientUserViewModel()
        {
            this.VisitPayRoles = new List<SelectListItem>();
        }

        public int? VisitPayUserId { get; set; }

        [Display(Name = "Email Address:", Prompt = "Email Address")]
        [Required(ErrorMessage = "Email Address is required."), RegularExpressionEmailAddress(TextRegionConstants.EmailInvalidLongMessage)]
        [DataType(DataType.EmailAddress)]
        public string Email { get; set; }

        [Display(Name = "First Name:", Prompt = "First Name")]
        [Required(ErrorMessage = "First Name is required.")]
        public string FirstName { get; set; }

        [Display(Name = "Last Name:", Prompt = "Last Name")]
        [Required(ErrorMessage = "Last Name is required.")]
        public string LastName { get;set;}

        [Display(Name = "Username:", Prompt = "Username")]
        [Required(ErrorMessage = "Username is required.")]
        [MinLength(6, ErrorMessage = "Username must be at least 6 characters.")]
        public string UserName { get; set; }

        [DataType(DataType.Text)]
        [StringLength(6, ErrorMessage="Secure Code must be six characters.", MinimumLength=6)]
        [RegularExpression(@"\d\d\d\d\d\d", ErrorMessage = "Secure code must be six digits.")]
        [Display(Name = "Secure Code:", Prompt = "Secure Code")]
        [RequiredIf("IsSecureCodeRequired", true, ErrorMessage = "Secure Code is required.")]
        public string SecurityValidationValue { get; set; }

        [Display(Name = "Roles:")]
        [Required(ErrorMessage = "At least one role is required.")]
        public IList<string> VisitPayRoleStrings { get; set; }

        [Display(Name = "Account Locked:")]
        public bool IsLocked { get; set; }

        public DateTime? LockoutEndDateUtc { get; set; }

        [Display(Name = "Account Disabled:")]
        public bool IsDisabled { get; set; }

        [Display(Name = "Account Reset:")]
        public bool IsAccountReset { get; set; }

        public string LastLoginDate { get; set; }

        public bool IsCurrentUser { get; set; }

        public bool IsSecureCodeRequired => !this.VisitPayUserId.HasValue || this.IsAccountReset;

        public IList<SelectListItem> VisitPayRoles { get; set; }

        public string LastNameFirstName => FormatHelper.LastNameFirstName(this.LastName, this.FirstName);

        public string FirstNameLastName => FormatHelper.FirstNameLastName(this.FirstName, this.LastName);
    }
}
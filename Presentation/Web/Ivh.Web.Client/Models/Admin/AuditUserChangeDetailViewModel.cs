﻿using Ivh.Common.Base.Utilities.Helpers;

namespace Ivh.Web.Client.Models.Admin
{
    public class AuditUserChangeDetailViewModel
    {
        public string ChangeTime { get; set; }
        public string ExpiredTime { get; set; }
        public string VisitPayUserId { get; set; }
        public string PriorUserName { get; set; }
        public string UserName { get; set; }
        public string PriorEmail { get; set; }
        public string Email { get; set; }
        public string PriorFirstName { get; set; }
        public string FirstName { get; set; }
        public string PriorLastName { get; set; }
        public string LastName { get; set; }
        public string PriorLockoutEnabled { get; set; }
        public string LockoutEnabled { get; set; }
        public string PriorLockoutEndDateUtc { get; set; }
        public string LockoutEndDateUtc { get; set; }
        public string LastNameFirstName => FormatHelper.LastNameFirstName(this.LastName, this.FirstName);
        public string FirstNameLastName => FormatHelper.FirstNameLastName(this.FirstName, this.LastName);
        public string PriorLastNameFirstName => FormatHelper.LastNameFirstName(this.PriorLastName, this.PriorFirstName);
        public string PriorFirstNameLastName => FormatHelper.FirstNameLastName(this.PriorFirstName, this.PriorLastName);
    }
}
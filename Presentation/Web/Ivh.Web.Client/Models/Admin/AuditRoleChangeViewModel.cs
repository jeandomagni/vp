﻿namespace Ivh.Web.Client.Models.Admin
{
    public class AuditRoleChangeViewModel
    {
        public string VisitPayUserId { get; set; }
        public string UserName { get; set; }
        public string VisitPayRoleId { get; set; }
        public string ChangeEventId { get; set; }
        public string ExpiredChangeEventId { get; set; }
        public string ChangeTime { get; set; }
        public string ExpiredTime { get; set; }
        public string PriorDelete { get; set; }
    }
}
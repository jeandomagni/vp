﻿namespace Ivh.Web.Client.Models.Admin
{
    public class AuditLogViewModel
    {
        public string AuditDate { get; set; }
        public string VisitPayUserId { get; set; }
        public string UserName { get; set; }
        public string EventType { get; set; }
        public string AuditType { get; set; }
        public string AuditPeriod { get; set; }
        public string Notes { get; set; }
    }
}
﻿namespace Ivh.Web.Client.Models.Admin
{
    using Ivh.Common.Base.Utilities.Helpers;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.Web.Mvc;

    public class ClientUserSearchFilterViewModel
    {
        public ClientUserSearchFilterViewModel()
        {
            this.VisitPayRoles = new List<SelectListItem>();
        }

        [Display(Prompt = "Last Name")]
        public string LastName { get; set; }

        [Display(Prompt = "First Name")]
        public string FirstName { get; set; }

        [Display(Prompt = "Username")]
        public string Username { get; set; }

        public int? VisitPayRoleId { get; set; }

        public IList<SelectListItem> VisitPayRoles { get; set; }

        public DateTime? LastIvhUserAudit { get; set; }

        public DateTime? LastClientUserAudit { get; set; }

        public string LastNameFirstName => FormatHelper.LastNameFirstName(this.LastName, this.FirstName);

        public string FirstNameLastName => FormatHelper.FirstNameLastName(this.FirstName, this.LastName);
    }
}
﻿namespace Ivh.Web.Client.Models.Admin
{
    using System;
    using System.ComponentModel.DataAnnotations;

    public class PciAuditFilterViewModel
    {
        [Display(Prompt = "Begin Date", Name = "Begin Date")]
        public DateTime BeginDate { get; set; }

        public DateTime BeginTime { get; set; }

        [Display(Prompt = "End Date", Name = "End Date")]
        public DateTime EndDate { get; set; }

        public DateTime EndTime { get; set; }

        public DateTime BeginDateTime { get { return new DateTime(BeginDate.Year, BeginDate.Month, BeginDate.Day, BeginTime.Hour, BeginTime.Minute, BeginTime.Second, BeginTime.Millisecond);} }
        public DateTime EndDateTime { get { return new DateTime(EndDate.Year, EndDate.Month, EndDate.Day, EndTime.Hour, EndTime.Minute, EndTime.Second, EndTime.Millisecond); } }
        public string SortField { get; set; }
        public string SortOrder { get; set; }
        public DateTime? LastIvhAudit { get; set; }
        public DateTime? LastClientAudit { get; set; }
    }
}
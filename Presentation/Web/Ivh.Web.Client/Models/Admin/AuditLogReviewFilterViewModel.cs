﻿namespace Ivh.Web.Client.Models.Admin
{
    using System;
    using System.ComponentModel.DataAnnotations;

    public class AuditLogReviewFilterViewModel
    {
        [Display(Prompt = "Begin Date", Name = "Begin Date")]
        public DateTime? BeginDate { get; set; }

        [Display(Prompt = "End Date", Name = "End Date")]
        public DateTime? EndDate { get; set; }
    }
}
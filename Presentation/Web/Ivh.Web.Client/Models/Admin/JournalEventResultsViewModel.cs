﻿namespace Ivh.Web.Client.Models.Admin
{
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.Dynamic;
    using System.Linq;
    using Common.Base.Utilities.Extensions;
    using Newtonsoft.Json;

    public class JournalEventResultsViewModel
    {
        [Display(Name = "Message")]
        public string Message { get; set; }
        [Display(Name = "Event Type")]
        public string EventType { get; set; }
        [Display(Name = "Event Date / Time")]
        public string EventDate { get; set; }
        [Display(Name = "User Name")]
        public string UserName { get; set; }
        [Display(Name = "VP Guarantor ID")]
        public string VpGuarantorId { get; set; }

        public IDictionary<string,string> AdditionalDataDictionary { get; set; }
        public string EventHistoryId { get; set; }

        [Display(Name = "IP Address")]
        public string IpAddress { get; set; }
        [Display(Name = "User ID")]
        public string EventVisitPayUserId { get; set; }
        [Display(Name = "User Agent")]
        public string UserAgent { get; set; }
        [Display(Name = "Target User ID")]
        public string VisitPayUserId { get; set; }
        [Display(Name = "Application Name")]
        public string Application { get; set; }
        [Display(Name = "Device Type")]
        public string DeviceType { get; set; }

        public int VpGuarantorIdUserName { get; set; }
        public int VpGuarantorIdTargetUserId { get; set; }
    }
}
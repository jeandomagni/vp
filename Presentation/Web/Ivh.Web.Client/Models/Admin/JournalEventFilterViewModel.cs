﻿namespace Ivh.Web.Client.Models.Admin
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.Web.Mvc;
    using Common.Base.Enums;
    using Common.VisitPay.Enums;
    using Common.Web.Attributes;

    public class JournalEventFilterViewModel
    {
        [Display(Prompt = "Begin Date", Name = "Begin Date")]
        [DateRangeCompare(false, "*.BeginDate", "*.EndDate", DateRangeCompareType.LessThanOrEqual, ErrorMessage = "Invalid Date Range")]
        [DataType(DataType.DateTime)]
        public DateTime? BeginDate { get; set; }

        [Display(Prompt = "End Date", Name = "End Date")]
        [DateRangeCompare(false, "*.EndDate", "*.BeginDate", DateRangeCompareType.GreaterThanOrEqual, ErrorMessage = "Invalid Date Range")]
        [DataType(DataType.DateTime)]
        public DateTime? EndDate { get; set; }

        [Display(Name = "Event Type")]
        public JournalEventTypeEnum? JournalEventType { get; set; }

        [Display(Name = "Event Trigger")]
        public JournalEventCategoryEnum? JournalEventCategory { get; set; }

        [Display(Name = "VP Guarantor ID")]
        public int? VpGuarantorId { get; set; }

        [Display(Name = "User Name")]
        public string UserNameFilter { get; set; }

        public bool HideVpGuarantorFilter { get; set; }

        public IList<SelectListItem> JournalEventTypes { get; set; }
        public IList<SelectListItem> JournalEventCategories { get; set; }
    }
}
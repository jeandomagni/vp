﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Ivh.Web.Client.Models.VisitPayDocument
{
    using Common.VisitPay.Enums;

    public class VisitPayDocumentClientViewModel
    {
        public Dictionary<VisitPayDocumentTypeEnum, List<VisitPayDocumentResultViewModel>> VisitPayDocumentResultDictionary { get; set; }
    }
}
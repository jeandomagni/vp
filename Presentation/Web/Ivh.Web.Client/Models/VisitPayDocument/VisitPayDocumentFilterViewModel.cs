﻿namespace Ivh.Web.Client.Models.VisitPayDocument
{
    using System;
    using System.Collections.Generic;
    using System.Web.Mvc;

    public class VisitPayDocumentFilterViewModel
    {

        public VisitPayDocumentFilterViewModel()
        {
            this.DateRanges = new List<SelectListItem>
            {
                new SelectListItem {Text = "Last 30 Days", Value = DateTime.UtcNow.AddDays(-30).ToShortDateString()},
                new SelectListItem {Text = "Last 60 Days", Value = DateTime.UtcNow.AddDays(-60).ToShortDateString()},
                new SelectListItem {Text = "Last 90 Days", Value = DateTime.UtcNow.AddDays(-90).ToShortDateString()},
                new SelectListItem {Text = "All", Value = ""}
            };

            this.VisitPayDocumentStatuses = new List<SelectListItem>
            {
                //TODO- need to figure out something here (constants?)
                // this really should be a checkbox 
                new SelectListItem {Text = "Approved", Value = "Approved"},
                new SelectListItem {Text = "Not Approved", Value = "Not Approved"},
                new SelectListItem {Text = "All", Value = ""}
            };
        }

        public IList<SelectListItem> VisitPayDocumentStatuses { get; set; }
        public string VisitPayDocumentStatus { get; set; }

        public IList<SelectListItem> DateRanges { get; set; }
        public DateTime? DateRangeFrom { get; set; }

    }
}
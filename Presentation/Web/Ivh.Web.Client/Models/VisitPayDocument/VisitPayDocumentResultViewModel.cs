﻿namespace Ivh.Web.Client.Models.VisitPayDocument
{
    using System;
    using Common.VisitPay.Enums;

    public class VisitPayDocumentResultViewModel
    {
        public int VisitPayDocumentId { get; set; }
        public int UploadedByVpUserId { get; set; }
        public int? ApprovedByVpUserId { get; set; }
        public DateTime InsertDate { get; set; }
        public string ActiveDateString { get; set; }
        public string ApprovedDateString { get; set; }
        public string AttachmentFileName { get; set; }
        public string DisplayTitle { get; set; }
        public string MimeType { get; set; }
        public string UploadedByUserName { get; set; }
        public string ApprovedByUserName { get; set; }
        public string VisitPayDocumentType { get; set; }

    }
}
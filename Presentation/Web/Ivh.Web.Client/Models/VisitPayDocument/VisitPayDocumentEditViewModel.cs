﻿namespace Ivh.Web.Client.Models.VisitPayDocument
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.Linq;
    using System.Web.Mvc;
    using Common.VisitPay.Enums;

    public class VisitPayDocumentEditViewModel
    {
        public int VisitPayDocumentId { get; set; }

        [Display(Name = "Approve")]
        public bool Approved { get; set; }

        [Display(Name = "Activation Date")]
        public string ActiveDateString { get; set; }

        [Display(Name = "Display Title")]
        public string DisplayTitle { get; set; }

        //Do allow the user to edit this
        public string ApprovedDateString { get; set; }

        public int? ApprovedByVpUserId { get; set; }

        [Required]
        [Range(1, int.MaxValue)]
        [Display(Name = "Category")]
        public int? VisitPayDocumentTypeId { get; set; }

        public List<SelectListItem> VisitPayDocumentTypes => Enum.GetValues(typeof(VisitPayDocumentTypeEnum)).Cast<VisitPayDocumentTypeEnum>().Select(x => new SelectListItem
        {
            Text = x.ToString(),
            Value = ((int)x).ToString()
        }).ToList();


        //The following properties to be used in the controller
        public bool WasApproved => this.Approved && string.IsNullOrWhiteSpace(this.ApprovedDateString);

        public bool WasUnApproved => !this.Approved && string.IsNullOrWhiteSpace(this.ApprovedDateString);

        public int? UploadedByVpUserId { get; set; }
        public bool CanApprove { get; set; }

        public DateTime? ApprovedDate
        {
            get
            {
                if (this.WasApproved)
                {
                    return DateTime.UtcNow;
                }
                if (this.WasUnApproved)
                {
                    return null;
                }
                DateTime parsedDate;
                if (DateTime.TryParse(this.ActiveDateString, out parsedDate))
                {
                    return parsedDate;
                }
                return null;
            }
        }

        public DateTime? ActiveDate
        {
            get
            {
                bool hasDate = !string.IsNullOrEmpty(this.ActiveDateString?.Trim());
                if (hasDate)
                {
                    DateTime parsedDate;
                    if (DateTime.TryParse(this.ActiveDateString, out parsedDate))
                    {
                        return parsedDate;
                    }
                }
                return null;
            }
        }
    }
}
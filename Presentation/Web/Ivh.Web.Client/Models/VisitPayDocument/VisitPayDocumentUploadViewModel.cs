﻿namespace Ivh.Web.Client.Models.VisitPayDocument
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.Linq;
    using System.Web;
    using System.Web.Mvc;
    using Common.VisitPay.Enums;

    public class VisitPayDocumentUploadViewModel
    {
        private const int _fileLength = 4096000; // 4000 KB

        [Required]
        [Display(Name = "File Name")]
        public HttpPostedFileBase FileContents { get; set; }

        [Display(Name = "Activation Date")]
        public string ActiveDateString { get; set; }

        [Display(Name = "Display Title")]
        [MaxLength(200)]
        public string DisplayTitle { get; set; }

        public DateTime? ActiveDate
        {
            get
            {
                bool hasDate = !string.IsNullOrEmpty(this.ActiveDateString?.Trim());
                if (hasDate)
                {
                    DateTime parsedDate;
                    if (DateTime.TryParse(this.ActiveDateString, out parsedDate))
                    {
                        return parsedDate;
                    }
                }
                return null;
            }
        }

        [Required]
        [Range(1, int.MaxValue)]
        [Display(Name = "Category")]
        public int? VisitPayDocumentTypeId { get; set; }

        public List<SelectListItem> VisitPayDocumentTypes => Enum.GetValues(typeof(VisitPayDocumentTypeEnum)).Cast<VisitPayDocumentTypeEnum>().Select(x => new SelectListItem
        {
            Text = x.ToString(),
            Value = ((int) x).ToString()
        }).ToList();

        public string AcceptableFileExtensions => string.Join(",", this.AcceptedFileTypesDictionary.Keys);
        public string AcceptableFileExtensionsMessage => $"Accepted file types:{this.AcceptableFileExtensions}";

        public string FileSizeMessage => "File size too large. Must be 4000 KB or smaller";

        private Dictionary<string, string> AcceptedFileTypesDictionary => new Dictionary<string, string>
        {
            [".doc"] = "application/msword",
            [".docx"] = "application/vnd.openxmlformats-officedocument.wordprocessingml.document",
            [".xls"] = "application/vnd.ms-excel",
            [".xlsx"] = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
            [".ppt"] = "application/vnd.ms-powerpoint",
            [".pptx"] = "application/vnd.openxmlformats-officedocument.presentationml.presentation",
            [".pdf"] = "application/pdf",
            [".gif"] = "image/gif",
            [".jpg"] = "image/jpg",
            [".jpeg"] = "image/jpeg",
            [".png"] = "image/png"
        };

        public bool IsValidContentType()
        {
            if (this.FileContents == null)
            {
                return false;
            }
            if (this.AcceptedFileTypesDictionary.Values.Contains(this.FileContents.ContentType))
            {
                return true;
            }
            return false;
        }


        public bool IsSupportedFileSize()
        {
            if (this.FileContents == null)
            {
                return false;
            }
            return this.FileContents.ContentLength < _fileLength;
        }
    }
}
﻿window.VisitPay.PaymentQueueGuarantorLookup = (function ($, ko, clientSettings, common) {
    var $scope;
    var $grid;
    var model;
    var rowToPopulate;
    var searchModal;
    var wasSaved;
    
    var gridDefaultSortOrder = 'asc';
    var gridDefaultSortName = 'LastName';
    var maxRowsToAllowedToReturn = 200;
    var rowsPerPage = 10;
    var moreThanMaxWarning = 'More than ' + maxRowsToAllowedToReturn + ' records returned. Please refine your search';

    var guarantorSearchFilterViewModel = function (data) {
        var self = this;

        self.HsGuarantorId = ko.observable('');
        self.FirstName = ko.observable('');
        self.LastName = ko.observable('');
        self.PhoneNumber = ko.observable('');
        self.AddressStreet1 = ko.observable('');
        self.AddressStreet2 = ko.observable('');
        self.City = ko.observable('');
        self.StateProvince = ko.observable('');
        self.PostalCode = ko.observable('');
        self.AmountDue = ko.observable('');
        
        self.RunResults = ko.observable(false);

        self.Reset = function () {
            self.HsGuarantorId('');
            self.FirstName('');
            self.LastName('');
            self.PhoneNumber('');
            self.AddressStreet1('');
            self.AddressStreet2('');
            self.City('');
            self.StateProvince('');
            self.PostalCode('');
            self.AmountDue('');
            self.RunResults(false);

            common.ResetUnobtrusiveValidation($('#guarantorSearchJqGridFilter'));
        };

        //Needs to match filter view model IsValid
        self.IsValid = ko.computed(function () {
            return ko.unwrap(self.HsGuarantorId).length > 0 ||
                ko.unwrap(self.FirstName).length > 0 ||
                ko.unwrap(self.LastName).length > 0 ||
                ko.unwrap(self.PhoneNumber).length > 0 ||
                ko.unwrap(self.AddressStreet1).length > 0 ||
                ko.unwrap(self.AddressStreet2).length > 0 ||
                ko.unwrap(self.City).length > 0 ||
                ko.unwrap(self.StateProvince).length > 0 ||
                ko.unwrap(self.PostalCode).length > 0 ||
                ko.unwrap(self.AmountDue).length > 0;
        });
        
        self.ChangeNotifications = function () {
            var setRun = function () {
                self.RunResults(true);
            };

            self.FirstName.subscribe(setRun);
            self.LastName.subscribe(setRun);
            self.AddressStreet1.subscribe(setRun);
            self.AddressStreet2.subscribe(setRun);
            self.City.subscribe(setRun);
            self.StateProvince.subscribe(setRun);
            self.PostalCode.subscribe(setRun);
            self.PhoneNumber.subscribe(setRun);
            self.AmountDue.subscribe(setRun);
            self.HsGuarantorId.subscribe(setRun);
        };

        self.PostModel = function () {
            return ko.mapping.toJS(self, {
                'ignore': ['ChangeNotifications', 'Reset', 'RunResults', 'PostModel', 'IsValid']
            });
        };

        //Map any incoming data. Only a subset of fields are passed in from the queue.
        if (data) {
            self.FirstName(data.FirstName);
            self.LastName(data.LastName);
            self.AmountDue(data.AmountDue);
            self.HsGuarantorId(data.HsGuarantorId);
        }
    };

    function bindGrid(json) {
        $grid.jqGrid({
            datatype: 'local',
            data: json,
            pager: 'guarantorSearchJqGridPager',
            sortname: gridDefaultSortName,
            sortorder: gridDefaultSortOrder,
            onSelectRow: function(rowId, e) {
                var rowData = $grid.getRowData(rowId);
                window.VisitPay.PaymentQueue.PopulateRowData(rowToPopulate, rowData);
                wasSaved = true;
                searchModal.modal('hide');
            },
            loadonce: true,
            rowNum: rowsPerPage,
            rowList: [10, 20, 30],
            loadComplete: function (data) {
                var gridContainer = $grid.parents('.ui-jqgrid:eq(0)');
                var gridNoRecords = $('#guarantorSearchJqGridNoRecords');
                if (data != null && data.rows.length > 0) {
                    gridContainer.show();
                    gridNoRecords.removeClass('in');
                } else {
                    gridContainer.hide();
                    gridNoRecords.addClass('in');
                }
            },
            gridComplete: function () {
                $grid.find('[data-toggle="tooltip"]').tooltip({ animation: false, container: 'body' });

                $('#guarantorSearchJqGridPager').find('#input_jqGridPager').find('input').off('change').on('change', function () {
                    $grid.jqGrid().trigger('reloadGrid', [{ page: $(this).val() }]);
                });

                var gridContainer = $grid.parents('.ui-jqgrid:eq(0)');
                gridContainer.addClass('visible');
            },
            colModel: [
                {
                    label: 'LAST NAME',
                    name: 'LastName',
                    width: 65,
                    resizable: false,
                    title: false
                },
                {
                    label: 'FIRST NAME',
                    name: 'FirstName',
                    width: 65,
                    resizable: false,
                    title: false
                },
                {
                    label: clientSettings.HsGuarantorPatientIdentifier.toUpperCase(),
                    name: 'HsGuarantorId',
                    width: 45,
                    resizable: false,
                    title: false
                },
                {
                    label: clientSettings.VpGuarantorPatientIdentifier.toUpperCase(),
                    name: 'VpGuarantorId',
                    width: 40,
                    resizable: false
                },
                {
                    label: 'ADDRESSES',
                    name: 'Addresses',
                    width: 112,
                    resizable: false,
                    formatter: function (cellValue, options, rowObject) {
                        if (rowObject.Addresses) {
                            var addresses = '';
                            $.each(rowObject.Addresses, function (idx, address) {
                                addresses += '<p>' + address + '</p>';
                            });

                            return addresses;
                        } else {
                            return '';
                        }
                    }
                },
                {
                    label: 'PHONE NUMBERS',
                    name: 'PhoneNumbers',
                    width: 50,
                    resizable: false,
                    formatter: function (cellValue, options, rowObject) {
                        if (rowObject.PhoneNumbers) {
                            var phones = '';
                            $.each(rowObject.PhoneNumbers, function (idx, phone) {
                                phones += '<p>' + phone + '</p>';
                            });

                            return phones;
                        } else {
                            return '';
                        }
                    }
                },
                {
                    label: 'AMOUNT DUE',
                    name: 'LastAmountDue',
                    width: 40,
                    resizable: false,
                    align: 'right'
                }
            ]
        });
        
        $grid.jqGrid('clearGridData');
        $grid.jqGrid('setGridParam', { data: json });
        $grid.trigger('reloadGrid');
    }

    function showMoreThanMaxWarning(refineSearchMessageContainer, show) {
        if (show === true) {
            $(refineSearchMessageContainer).html(moreThanMaxWarning);
            $(refineSearchMessageContainer).show();
        } else {
            $(refineSearchMessageContainer).hide();
        }
    }

    function executeSearch() {
        if (model.IsValid()) {
            $('#guarantorSearchJqGridInitialMessage').addClass('collapse');
            $.blockUI();
            $.post('/PaymentQueue/SearchGuarantors', {
                model: model.PostModel(),
                page: 1,
                rows: maxRowsToAllowedToReturn,
                sidx: $grid.jqGrid('getGridParam', 'sortname'),
                sord: $grid.jqGrid('getGridParam', 'sortorder')
            }, function (result) {
                $.unblockUI();
                
                var moreThanMax = result.records > maxRowsToAllowedToReturn;
                if (moreThanMax) {
                    showMoreThanMaxWarning($('#refineSearchMessage'), moreThanMax);
                } else {
                    bindGrid(result.Guarantors);
                }
                
                model.RunResults(false);
            }, 'json');

        } else {
            $('#guarantorSearchJqGridNoRecords').removeClass('in');
            $('#guarantorSearchJqGridInitialMessage').removeClass('collapse');
            $grid.parents('.ui-jqgrid:eq(0)').hide();
        }
    }

    function focusInitialSearchField() {
        //Focus initial search field. Have to clear out and reset the input value after focus so that cursor goes to end.
        var initialSearchField = searchModal.find('#FirstName');
        var tempStr = initialSearchField.val();
        initialSearchField.val('').focus().val(tempStr);
    }

    function init(modelData) {
        $scope = $('#guarantorSearchModal');
        $grid = $scope.find('#guarantorSearchJqGrid');

        model = new guarantorSearchFilterViewModel(modelData);
        ko.applyBindings(model, $('#guarantorSearchJqGridFilter')[0]);
        model.ChangeNotifications();

        $('#guarantorSearchJqGridFilter').on('submit', function (e) {
            e.preventDefault();

            if ($('#guarantorSearchJqGridFilter').valid()) {
                executeSearch();
            }
        });

        $('#btnGuarantorSearchResetFilter').on('click', function () {
            model.Reset();
            executeSearch();
            focusInitialSearchField();
        });

        //Mask phone input
        $scope.find('#PhoneNumber').mask('(999) 999-9999');
    }

    return {
        Initialize: function (data, referenceRowIndex) {
            $.blockUI();

            wasSaved = false;

            rowToPopulate = referenceRowIndex;

            var modalData = {
                FirstName: data.GuarantorFirstName,
                LastName: data.GuarantorLastName,
                HsGuarantorId: data.GuarantorAccountNumber,
                AmountDue: data.InvoiceAmount
            };
            var modalPromise = VisitPay.Common.ModalNoContainerAsync('guarantorSearchModal', '/PaymentQueue/GuarantorSearchModal', 'GET', modalData, true);
            modalPromise.done(function (modal) {
                searchModal = modal;

                searchModal.on('shown.bs.modal', function () {
                    focusInitialSearchField();
                    init(modalData);
                });

                searchModal.on('hidden.bs.modal', function () {
                    if (!wasSaved) {
                        window.VisitPay.PaymentQueue.SearchModalCancelled(rowToPopulate);
                    } 
                });

                $.unblockUI();
            });
        }
    };
})(jQuery,
    window.ko,
    window.VisitPay.ClientSettings = window.VisitPay.ClientSettings || {},
    window.VisitPay.Common = window.VisitPay.Common || {});

window.VisitPay.PaymentQueue = (function ($, ko, clientSettings, guarantorLookup, common) {
    var $scope = $('#section-payment-queue');
    var $grid = $scope.find('#jqGrid');

    var model;
    var summaryModel;
    var lastSelectedRow; //Track selected row in grid
    var initialDateRange;

    var summaryViewModel = function () {
        var self = this;

        self.AllPaymentsTotal = ko.observable(null);
        self.PostedPaymentsTotal = ko.observable(null);
        self.UnpostedPaymentsTotal = ko.observable(null);
        self.NonVisitPayPaymentsTotal = ko.observable(null);
    };

    var filterViewModel = function () {
        var self = this;

        self.DateRangeFrom = ko.observable(null);
        self.PaymentState = ko.observable(null);
        self.StartDate = ko.observable(null);
        self.EndDate = ko.observable(null);
        self.BatchNumber = ko.observable(null);
        self.RunResults = ko.observable(false);

        self.ChangeNotifications = function () {
            var setRun = function () {
                self.RunResults(true);
            };

            self.DateRangeFrom.subscribe(setRun);
            self.PaymentState.subscribe(setRun);
            self.StartDate.subscribe(setRun);
            self.EndDate.subscribe(setRun);
            self.BatchNumber.subscribe(setRun);
        };
    };

    function formatActionColumn(cellValue, options, rowObject) {
        var $div = $('<div/>');

        //Only allow guarantor search on rows that are editable
        if (isEditableStatus(rowObject.PaymentImportState)) {
            var $btn = $('<button/>');
            $btn.addClass('btn btn-icon search-guarantor-btn');
            $btn.attr('title', 'Find Guarantor');
            $btn.attr('data-row-id', options.rowId);
            $btn.html('<i class="glyphicon glyphicon-search"></i>');
            $div.append($btn);
        }

        return $div.html();
    }

    function buildColumnModel() {

        //Build column model
        var colModel = [];
        colModel.push({
            label: 'Selected',
            name: 'Selected',
            width: 58,
            resizable: false,
            title: false,
            sortable: false,
            classes: 'text-center',
            formatter: 'checkbox',
            formatoptions: { disabled: false }
        });
        colModel.push({ width: 80, label: 'Date', name: 'BatchCreditDateDisplay', title: true, sortable: false });
        colModel.push({
            width: 90,
            label: 'Status',
            name: 'PaymentImportState',
            title: true,
            sortable: false,
            editable: false, //Not editable by default
            formatter: function (cellValue, options, rowData) {
                //If in Failed or Cancelled status, allow changing the status
                if (isEditableStatus(cellValue)) {
                    options.colModel.editable = true;
                    options.colModel.edittype = 'select';
                    options.colModel.editoptions = {
                        value: "Cancelled:Cancelled; Failed:Failed"
                    };
                }
                return cellValue;
            }
        });
        colModel.push({ width: 70, label: 'Lockbox #', name: 'LockBoxNumber', title: true, sortable: false });
        colModel.push({ width: 55, label: 'Batch #', name: 'BatchNumber', title: true, sortable: false });
        colModel.push({ width: 50, label: 'Item #', name: 'PaymentSequenceNumber', title: true, sortable: false });
        colModel.push({
            label: clientSettings.VpGuarantorPatientIdentifier,
            name: 'VpGuarantorId',
            width: 100,
            resizable: false,
            sortable: false
        });
        colModel.push({ width: 140, label: 'Last Name', name: 'GuarantorLastName', editable: true, title: false, sortable: false });
        colModel.push({ width: 140, label: 'First Name', name: 'GuarantorFirstName', editable: true, title: false, sortable: false });
        colModel.push({
            width: 100,
            label: clientSettings.HsGuarantorPatientIdentifier,
            name: 'GuarantorAccountNumber',
            editable: true,
            title: false,
            sortable: true,
            editoptions: {
                 'data-lpignore': true
            }
        });
        colModel.push({
            width: 95, 
            label: 'Check Amount',
            name: 'InvoiceAmount',
            editable: false,
            title: false,
            sortable: false,
            align: 'right',
            formatter: 'currency',
            formatoptions: {
                defaultValue: ''
            }
        });
        colModel.push({
            width: 140,
            label: 'Exceptions',
            name: 'PaymentImportStateReason',
            title: true,
            sortable: false,
            classes: 'payment-exception-text text-danger'
        });
        colModel.push({ width: 55, label: 'Actions', name: 'Actions', sortable: false, classes: 'text-center', formatter: formatActionColumn });

        //Hidden fields
        colModel.push({
            name: 'PaymentImportId',
            hidden: true
        });

        //No resizable columns
        for (var z = 0; z < colModel.length; z++) {
            colModel[z].resizable = false;
        }

        return colModel;
    }

    function bindSearchGuarantorButtons() {
        $('.search-guarantor-btn').off('click');

        //Add listeners for search guarantors action buttons
        $('.search-guarantor-btn').click(function (e) {
            var btn;
            if (e.target.tagName.toUpperCase() != 'BUTTON') {
                btn = $(e.target).closest('.search-guarantor-btn');
            } else {
                btn = $(e.target);
            }

            var btnRow = $(btn).attr('data-row-id');

            //Save and clear out the row
            $grid.saveRow(lastSelectedRow);
            lastSelectedRow = null;
            var rowData = $grid.getRowData(btnRow);
            guarantorLookup.Initialize(rowData, btnRow);
        });
    }

    function removeSelectCheckboxesForNonEditableRows() {
        var rows = $grid.getRowData();
        for (var i = 0; i < rows.length; i++) {
            var row = rows[i];
            if (!isEditableStatus(row.PaymentImportState)) {
                var $row = $('#' + (i + 1) + '.jqgrow ');
                var $checkbox = $row.find('input[type="checkbox"]');
                $checkbox.remove();
            }
        }
    }

    function gridGridComplete() {
        //No pager at this time. Can add later if we need paging on queue
        //$scope.find('#jqGridPager').find('#input_jqGridPager').find('input').off('change').on('change', function () {
        //    $grid.jqGrid().trigger('reloadGrid', [{ page: $(this).val() }]);
        //});

        $grid.parents('.ui-jqgrid:eq(0)').addClass('visible');

        bindSearchGuarantorButtons();
        removeSelectCheckboxesForNonEditableRows();
    }

    function gridLoadComplete(data) {
        //Save summary info
        summaryModel.AllPaymentsTotal(data.AllPaymentsTotal);
        summaryModel.PostedPaymentsTotal(data.PostedPaymentsTotal);
        summaryModel.UnpostedPaymentsTotal(data.UnpostedPaymentsTotal);
        summaryModel.NonVisitPayPaymentsTotal(data.NonVisitPayPaymentsTotal);

        if (data.PaymentQueueItems && data.PaymentQueueItems.length > 0) {
            //Have table data
            $grid.parents('.ui-jqgrid:eq(0)').show();
            $('#jqGridNoRecords').removeClass('in');
            $('.selection-action-buttons').show();
            $('#payment-queue-summary').show();
        } else {
            //Have no table data
            $grid.parents('.ui-jqgrid:eq(0)').hide();
            $('#jqGridNoRecords').addClass('in');
            $('.selection-action-buttons').hide();
            $('#payment-queue-summary').hide();
        }

        model.RunResults(false);
        $.unblockUI();
    }

    //This is called when Enter key is pressed on a selected row
    function rowSaved(rowId) {
        //Mark row selected since changes have been made
        $grid.jqGrid('setCell', rowId, 'Selected', 'Yes');

        //Select the next EDITABLE row
        var nextEditableRowId = getIdOfNextEditableRow(rowId);
        if (nextEditableRowId) {
            $grid.setSelection(nextEditableRowId);
            lastSelectedRow = nextEditableRowId;
        }

        //Have to rebind the search buttons since saving the row recreates the action column that contains the search button
        bindSearchGuarantorButtons();
    }

    function getIdOfNextEditableRow(afterRowId) {
        var nextRowIndex = parseInt(afterRowId) + 1;
        var numRecords = $grid.jqGrid('getGridParam', 'records');
        var nextEditableRowId = null;
        for (var i = nextRowIndex; i <= numRecords; i++) {
            if (isRowEditable(i)) {
                log.debug('found next editable row with index: ', i);
                nextEditableRowId = i;
                break;
            } else {
                nextEditableRowId = null;
            }
        }
        return nextEditableRowId;
    }

    function getIdOfPreviousEditableRow(beforeRowId) {
        var previousRowIndex = parseInt(beforeRowId) - 1;
        var previousEditableRowId = null;
        for (var i = previousRowIndex; i > 0; i--) {
            if (isRowEditable(i)) {
                log.debug('found previous editable row with index: ', i);
                previousEditableRowId = i;
                break;
            } else {
                previousEditableRowId = null;
            }
        }
        return previousEditableRowId;
    }

    function findHotkeyPressed() {
        if (lastSelectedRow && lastSelectedRow > 0) {
            $grid.saveRow(lastSelectedRow);
            var rowData = $grid.getRowData(lastSelectedRow);
            guarantorLookup.Initialize(rowData, lastSelectedRow);
            lastSelectedRow = null;
        }
    }

    function isEditableStatus(status) {
        //Only allow editing of Failed or Cancelled records for now.
        //Need to also check for "select" since the status column could be in an edit state in which case it's a select element
        return status === 'Failed' || status === 'Cancelled' || status.includes('select');
    }

    function isRowEditable(rowId) {
        //Only allow editing of Pending or Failed records
        var rowData = $grid.jqGrid('getRowData', rowId);
        return isEditableStatus(rowData.PaymentImportState);
    }

    function buildGrid() {

        //Build the column model
        var colModel = buildColumnModel();
        $grid.jqGrid({
            url: '/PaymentQueue/LoadPaymentQueue',
            postData: {
                model: ko.toJS(model)
            },
            jsonReader: { root: 'PaymentQueueItems' },
            autowidth: true,
            //pager: 'jqGridPager', //No pager at this time, would interfere with saving edited rows. Can add later if queue needs paging.
            loadComplete: gridLoadComplete,
            gridComplete: gridGridComplete,
            colModel: colModel,
            onSelectRow: function (id, status, e) {
                //Handle arrow key navigation for inline editing
                if (e && e.type && e.type === 'keydown') {
                    if (e.keyCode === 38) {
                        //Arrow up key was pressed (keyCode 38), get ID of previous editable row
                        id = getIdOfPreviousEditableRow(lastSelectedRow);
                    } else if (e.keyCode === 40) {
                        //Arrow down key was pressed (keyCode 40), get ID of previous editable row
                        id = getIdOfNextEditableRow(lastSelectedRow);
                    }
                }

                if (id && id !== lastSelectedRow) {
                    $grid.jqGrid('saveRow', lastSelectedRow);

                    if (isRowEditable(id)) {
                        $grid.jqGrid('editRow', id, {
                            keys: true,
                            aftersavefunc: rowSaved //Will be called when Enter key is pressed on the selected row
                        });
                        lastSelectedRow = id;
                    } else {
                        //Row isn't editable
                        lastSelectedRow = null;
                        log.debug('selected row is not editable');
                    }

                    //Add hotkey listeners to editable fields
                    $('input:text, select').bind('keydown', 'ctrl+f', function(e) {
                        findHotkeyPressed();

                        //Returning false prevents default browser hotkey action
                        return false;
                    });
                } else if (id == lastSelectedRow && isRowEditable(id)) {
                    //Re-enable row editing
                    $grid.jqGrid('editRow', id, {
                        keys: true,
                        aftersavefunc: rowSaved //Will be called when Enter key is pressed on the selected row
                    });
                }
            }
        });

        //Allow arrow keys and enter keys for navigating grid
        $grid.jqGrid('bindKeys');
    }

    function reloadGrid() {
        $.extend($grid.jqGrid('getGridParam', 'postData'), { model: ko.toJS(model) });
        $grid.trigger('reloadGrid');
    }

    function resetModel() {
        model.DateRangeFrom(initialDateRange);
        model.PaymentState(null);
        model.StartDate(null);
        model.EndDate(null);
        model.BatchNumber(null);

        model.RunResults(false);
    }

    function stopEditingSelectedRow() {
        if (lastSelectedRow && lastSelectedRow > 0) {
            $grid.saveRow(lastSelectedRow);
            lastSelectedRow = null;
        }
    }

    function bindFilters() {
        // prevent focus on validate failure to prevent calendar from opening
        $('#jqGridFilter').on('invalid-form.validate', function (form, validator) {
            validator.settings && (validator.settings.focusInvalid = false);
        });

        //Bind date pickers
        $('#section-payment-queue .input-group.date').each(function () {
            var element = $(this);
            element.datepicker({
                clearBtn: true,
                format: 'mm/dd/yyyy',
                orientation: 'bottom left',
                autoclose: true
            }).on('hide', function () {
                if ($('#jqGridFilter').valid())
                    common.ResetUnobtrusiveValidation($('#jqGridFilter'));
            });
        });

    };

    return {
        Initialize: function (data) {
            //Save initial date range
            initialDateRange = $('#DateRangeFrom').val();

            ko.cleanNode($('#jqGridFilter')[0]);
            model = new filterViewModel();
            summaryModel = new summaryViewModel();
            model.ChangeNotifications();
            ko.applyBindings(model, $('#jqGridFilter')[0]);
            ko.applyBindings(summaryModel, $('#payment-queue-summary')[0]);

            //Bind search filters
            bindFilters();

            resetModel();

            $('#jqGridFilter').on('submit', function (e) {
                e.preventDefault();
                if ($(this).valid()) {
                    $.blockUI();
                    reloadGrid();
                }
            });

            $('#btnGridFilterReset').on('click', function () {
                $.blockUI();
                resetModel();
                reloadGrid();
            });

            $('#select-all-btn').click(function (e) {
                //Stop editing row if necessary
                stopEditingSelectedRow();

                var rows = $grid.getRowData();
                for (var i = 0; i < rows.length; i++) {
                    var row = rows[i];

                    //Select row if it is editable
                    if (isEditableStatus(row.PaymentImportState)) {
                        $grid.jqGrid('setCell', i + 1, 'Selected', 'Yes');
                    }
                }
            });

            $('#deselect-all-btn').click(function (e) {
                //Stop editing row if necessary
                stopEditingSelectedRow();

                var rows = $grid.getRowData();
                for (var i = 0; i < rows.length; i++) {
                    var row = rows[i];

                    //Deselect row if it is editable
                    if (isEditableStatus(row.PaymentImportState)) {
                        $grid.jqGrid('setCell', i + 1, 'Selected', 'No');
                    }
                }
            });

            $('.save-selected-btn').click(function (e) {
                //Stop editing row if necessary
                stopEditingSelectedRow();

                var rows = $grid.getRowData();
                var rowsToSave = [];
                for (var i = 0; i < rows.length; i++) {
                    var row = rows[i];
                    if (row.Selected === 'Yes') {
                        //Add the row
                        rowsToSave.push({
                            //Only post editable fields
                            'GuarantorAccountNumber': row.GuarantorAccountNumber,
                            'GuarantorFirstName': row.GuarantorFirstName,
                            'GuarantorLastName': row.GuarantorLastName,
                            'PaymentImportId': row.PaymentImportId,
                            'PaymentImportState': row.PaymentImportState
                        });
                    }
                }
                log.debug('saving rows: ', rowsToSave);
                if (rowsToSave.length > 0) {
                    $.blockUI();
                    $.post('/PaymentQueue/UpdatePaymentQueueItems', { itemsToUpdate: rowsToSave }, function(response) {
                        if (response) {
                            if (response.Success) {
                                //Reload the grid to get new data
                                $grid.trigger('reloadGrid');
                            }

                            //Show response
                            VisitPay.Common.ModalGenericAlert('Save Result', response.Message, 'Ok');

                            $.unblockUI();
                        }
                    }, 'json');
                } else {
                    //Nothing to save
                    VisitPay.Common.ModalGenericAlert('Save Result', 'No rows selected', 'Ok');
                }
            });

            //Listen for ctrl+f anywhere on document for this page
            $(document).bind('keydown', 'ctrl+f', function (e) {
                findHotkeyPressed();

                //Returning false prevents default browser hotkey action
                return false;
            });


            $('#btnPaymentQueueExport').on('click', function (e) {

                e.preventDefault();
                $.post('/PaymentQueue/PaymentQueueExport', {
                    model: ko.toJS(model),
                    sidx: "", //grid.jqGrid('getGridParam', 'sortname'),
                    sord: ""  //grid.jqGrid('getGridParam', 'sortorder')
                }, function (result) {
                    if (result.Result === true)
                        window.location.href = result.Message;
                }, 'json');

            });
            
            buildGrid();
        },
        PopulateRowData(rowIndex, data) {
            if (rowIndex && rowIndex > 0 && data) {
                var rowData = $grid.getRowData(rowIndex);
                rowData.GuarantorLastName = data.LastName;
                rowData.GuarantorFirstName = data.FirstName;
                rowData.GuarantorAccountNumber = data.HsGuarantorId;
                rowData.VpGuarantorId = data.VpGuarantorId;

                $grid.setRowData(rowIndex, rowData);
                $grid.jqGrid('setCell', rowIndex, 'Selected', 'Yes');

                //Select the next EDITABLE row
                var nextEditableRowId = getIdOfNextEditableRow(rowIndex);
                if (nextEditableRowId) {
                    $grid.setSelection(nextEditableRowId);
                    lastSelectedRow = nextEditableRowId;
                }
            }
        },
        SearchModalCancelled(rowIndex) {
            //Reselect the row if it is editable
            if (rowIndex && rowIndex > 0 && isRowEditable(rowIndex)) {
                //Reselect the row
                $grid.setSelection(rowIndex);
                lastSelectedRow = rowIndex;
            }
        }
    };
})(jQuery,
    window.ko,
    window.VisitPay.ClientSettings = window.VisitPay.ClientSettings || {},
    window.VisitPay.PaymentQueueGuarantorLookup = window.VisitPay.PaymentQueueGuarantorLookup || {},
    window.VisitPay.Common = window.VisitPay.Common || {});
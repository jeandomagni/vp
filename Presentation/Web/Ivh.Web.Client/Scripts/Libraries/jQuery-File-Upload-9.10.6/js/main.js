$(function() {

    $('#fileupload').fileupload({
        autoUpload: false,
        dataType: 'json',
        maxFileSize: 999000,
        acceptFileTypes: /(\.|\/)(gif|jpe?g|png)$/i,
        formData: {
            supportRequestId: 9,
            supportRequestMessageId: 7
        },
        url: '/Support/UploadSupportRequestMessageAttachment'

    }).on('fileuploaddone', function(e, data) {

        /*$.each(data.result.files, function (index, file) {
                if (file.url) {
                    var link = $('<a>')
                        .attr('target', '_blank')
                        .prop('href', file.url);
                    $(data.context.children()[index])
                        .wrap(link);
                } else if (file.error) {
                    var error = $('<span class="text-danger"/>').text(file.error);
                    $(data.context.children()[index])
                        .append('<br>')
                        .append(error);
                }
            });*/

    }).on('fileuploadfail', function(e, data) {

        alert('fail');
        $.each(data.files, function(index) {
            var error = $('<span class="text-danger"/>').text('File upload failed.');
            $(data.context.children()[index])
                .append('<br>')
                .append(error);
        });

    }).on('fileuploadprogressall', function(e, data) {
        var progress = parseInt(data.loaded / data.total * 100, 10);
        console.log(progress);

        if (progress === 100) {
            console.log('done');
        }
    });

});

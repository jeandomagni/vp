﻿(function() {

    window.VisitPay.Grids.ClientAdminSupportRequests = (function($gridElement, filterModel, handlers) {

        var gridOpts = {
            autowidth: false,
            url: '/ClientSupport/ClientAdminSupportRequests',
            sortname: 'LastModifiedDate',
            colModel: [
                {
                    label: 'Closed',
                    name: 'ClientSupportRequestId',
                    width: 52,
                    formatter: function(cellValue, options, rowObject) {
                        return '<input type="checkbox" name="openclose" class="form-control" />';
                    }
                },
                {
                    label: 'Assigned To',
                    name: 'AssignedToVisitPayUserName',
                    width: 100
                },
                {
                    label: 'Case ID',
                    name: 'ClientSupportRequestDisplayId',
                    width: 100,
                    formatter: function(cellValue, options, rowObject) {

                        var $temp = $('<div/>');

                        var $unread = $('<em/>').addClass('glyphicon glyphicon-envelope pull-right').attr('title', rowObject.UnreadMessageCountFromClient + ' unread message(s)').attr('data-toggle', 'tooltip').attr('data-placement', 'right');
                        if (rowObject.UnreadMessageCountFromClient > 0) {
                            $unread.addClass('unread');
                        }
                        $temp.append($unread);

                        var attachmentCount = rowObject.AttachmentCountClient + rowObject.AttachmentCountInternal;
                        if (attachmentCount > 0) {
                            $temp.append($('<em/>').addClass('glyphicon glyphicon-paperclip pull-right').attr('title', 'There are files attached to this ticket.').attr('data-toggle', 'tooltip').attr('data-placement', 'right'));
                        }

                        $temp.append($('<a/>').attr('href', 'javascript:;').attr('name', 'case').attr('title', 'View Support Ticket History').attr('data-toggle', 'tooltip').attr('data-placement', 'right').text(cellValue));

                        return $temp.html();
                    }
                },
                {
                    label: 'Created',
                    name: 'InsertDate',
                    width: 80
                },
                {
                    label: 'Status',
                    name: 'StatusDisplay',
                    width: 64
                },
                {
                    label: 'Requestor',
                    name: 'CreatedByVisitPayUserName',
                    width: 120
                },
                {
                    label: 'Topic - Sub Topic',
                    name: 'TopicDisplay',
                    width: 127
                },
                {
                    label: 'Jira ID',
                    name: 'JiraId',
                    width: 90,
                    formatter: function (cellValue, options, rowObject) {
                        if (!cellValue || cellValue.length === 0) {
                            return '';
                        }

                        var $temp = $('<div/>');
                        $temp.append($('<a/>').attr('href', 'https://ivinci.atlassian.net/browse/' + cellValue).attr('target', '_blank').attr('title', cellValue).text(cellValue));

                        return $temp.html();
                    }
                },
                {
                    label: window.VisitPay.ClientSettings.AppSupportRequestNamePrefix + ' ID',
                    name: 'TargetVpGuarantorId',
                    width: 115,
                    formatter: function (cellValue, options, rowObject) {
                        if (!cellValue || cellValue.length === 0) {
                            return '';
                        }

                        var $temp = $('<div/>');
                        $temp.append($('<a/>').attr('href', '/Search/GuarantorAccount/' + cellValue).attr('title', cellValue).text(cellValue));

                        return $temp.html();
                    }
                },
                {
                    label: 'Modified',
                    name: 'LastModifiedDate',
                    width: 142,
                    formatter: function(cellValue, options, rowObject) {
                        if (!cellValue || cellValue.length === 0) {
                            return '';
                        }

                        var $temp = $('<div/>');
                        $temp.append($('<span/>').attr('title', cellValue + ' by ' + rowObject.LastModifiedByVisitPayUserName).text(cellValue));
                        return $temp.html();
                    }
                }
            ]
        };

        return new window.VisitPay.Grids.BaseGrid($gridElement, filterModel, $(), gridOpts, { noRecordsText: 'There are no support tickets to display at this time.' }, { loadComplete: handlers.loadComplete });

    });

    var ViewModel = function() {

        var self = this;

        self.Filter = {
            AssignedToVisitPayUserId: ko.observable(),
            ClientSupportRequestDisplayId: ko.observable(),
            ClientSupportRequestStatus: ko.observable(),
            DaysOffset: ko.observable(),
            JiraId: ko.observable(),
            TargetVpGuarantorId: ko.observable()
        };

        self.RunResults = ko.observable(false);
        self.ChangeNotifications = function() {

            var setRun = function() {
                self.RunResults(true);
            };

            self.Filter.AssignedToVisitPayUserId.subscribe(setRun);
            self.Filter.ClientSupportRequestDisplayId.subscribe(setRun);
            self.Filter.ClientSupportRequestStatus.subscribe(setRun);
            self.Filter.DaysOffset.subscribe(setRun);
            self.Filter.JiraId.subscribe(setRun);
            self.Filter.TargetVpGuarantorId.subscribe(setRun);

        };

        return self;

    };

    var vm;
    var grid;

    function reloadControls(e) {
        reloadGrid(e);
        reloadAssignedUserDropDown(e);
    }

    function reloadGrid(e) {
        if (e) {
            e.preventDefault();
        }
        if (!$('.blockPage').is(':visible')) {
            $.blockUI();
        }
        grid.reload();
        vm.RunResults(false);
    };

    function resetGrid(e) {
        if (e) {
            e.preventDefault();
        }
        $.blockUI();
        grid.reset();
        vm.RunResults(false);
    }

    function reloadAssignedUserDropDown(e) {

        var controlID = '#AssignedToVisitPayUserId';
        var apiURL = '/ClientSupport/ClientAdminSupportUsers';
        var allOptionText = 'All';
        var allValueText = '';
        var defaultSelectedVal = '-1'; // <Unassigned>
        var foundSelectedVal = false;

        //get unique filter options from ClientSupportController ASYNC
        $.post(apiURL, {}, function (data) {

            //get current selected filter value
            var selectedVal = ko.unwrap(vm.Filter.AssignedToVisitPayUserId);

            //clear all existing options
            $(controlID).empty();

            //add "All" option
            $(controlID).append($('<option>', { value: allValueText, text: allOptionText }));

            var i;
            for (i = 0; i < data.length; i++) {
                //add option from JSON array results (Value,Text)
                $(controlID).append($('<option>', { value: data[i].Value, text: data[i].Text }));
                if (String(data[i].Value) == String(selectedVal)) //JSON object value needs conversion to string to succeed with "loose equality"
                {
                    //indicate that the previous selected value is still present
                    foundSelectedVal = true;
                }
            }

            if (selectedVal == allValueText)
            {
                //indicate that the previous selected value is still present
                foundSelectedVal = true;
            }

            // use default selected value if the new filter options don't contain the previous selected value
            if (!foundSelectedVal) {
                selectedVal = defaultSelectedVal;
                //forces bound UI control to display message about "Run button needs to be clicked"
                vm.RunResults(true);
            }

            //set current selected filter value
            vm.Filter.AssignedToVisitPayUserId(selectedVal);
            //trigger change event since same value assignment might not notify (maybe use always notify extender on observable?)
            $(controlID).val(selectedVal).change(); 
        });

    }

    function loadComplete(data, root) {
        grid.jqg.find('.jqgrow').each(function() {

            var row = root[$(this).index() - 1];

            $(this).find('input[name=openclose]').prop('checked', row.IsClosed);

            $(this).off('click');

            $(this).on('click', 'input[name="openclose"]', function(e) {
                e.preventDefault();
                window.VisitPay.ClientSupport.ClientSupportRequestAction.show(row.ClientSupportRequestId).done(function() {
                    reloadControls();
                });
            });

            $(this).on('click', 'a[name="case"]', function(e) {
                e.preventDefault();
                var edit = new window.VisitPay.ClientSupport.ClientSupportRequestEdit(true, row.ClientSupportRequestId);
                edit.show().done(function () {
                    reloadControls();
                });
            });

        });

        $.unblockUI();
    }

    $(document).ready(function() {

        vm = new ViewModel();

        ko.applyBindings(vm, $('#section-clientadminsupportrequests form')[0]);
        vm.ChangeNotifications();

        grid = new window.VisitPay.Grids.ClientAdminSupportRequests($('#gridClientAdminSupportRequests'), vm.Filter, { loadComplete: loadComplete });

        $('#section-clientadminsupportrequests')
            .on('submit', 'form', reloadGrid)
            .on('click', 'form button[data-reset=true]', resetGrid)
            .on('click', '#btnCreateSupportRequest', function () {
                var create = window.VisitPay.ClientSupport.ClientSupportRequestCreate();
                create.show({ "onBehalfOfClient" : true}).done(reloadGrid);
              });
        
        $('#section-clientadminsupportrequests').find('#TargetVpGuarantorId').makeNumericInput({ allowDecimal: false, allowNegative: true, addClass: false });

        $('body').on('click', '#btnExport', function(e) {
            e.preventDefault();
            grid.exportData();
        });

    });

})();
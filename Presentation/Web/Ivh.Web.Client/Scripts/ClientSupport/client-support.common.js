﻿(function(clientSupport) {

    clientSupport.ClientSupportFileUploader = (function ($form) {

        var fileCount = 0;

        function addError(error) {

            if (error.indexOf('<br') > -1) {
                error = '<span class="multiline">' + error + '</span>';
            }

            $form.find('.error').html(error);
            if ($form.parents('.modal').eq(0).length > 0) {
                $form.parents('.modal').eq(0).find('.error-upload').html(error);
            }
        }

        function change(e, data) {
            addError('');
        };

        function processFail(e, data) {
            var error = data.files[data.index].error;
            var errorMessage = error;
            if (error.indexOf('too large') > -1) {
                errorMessage = 'We cannot accept files over 10MB.';
            } else if (error.indexOf('not allowed') > -1) {
                errorMessage = 'This is an unsupported file type.<br/>Supported file types are: doc, docx, gif, jpg, jpeg, pdf, png, ppt, pptx, txt, xls, xlsx, zip.';
            }
            addError(errorMessage);
        };

        function fileUploadAdded() {
            fileCount++;
            $form.trigger('fileUploadCountChange', fileCount);
            $form.find('button.cancel').off('click').on('click', function () {
                fileCount--;
                addError('');
                $form.trigger('fileUploadCountChange', fileCount);
            });
        };

        function fileUploadAlways(promise) {
            fileCount--;
            if (fileCount <= 0) {
                promise.resolve();
            }
        };

        function clear() {
            $form.find('tbody.files tr').each(function() {
                $(this).find('button.cancel').click();
            });
        }

        function init() {

            var opts = {
                acceptFileTypes: /(\.|\/)(doc|docx|gif|jpe?g|pdf|png|ppt|pptx|txt|xls|xlsx|zip)$/i,
                autoUpload: false,
                dataType: 'json',
                maxFileSize: 10000000, //10mb in bytes
                url: '/ClientSupport/UploadAttachment',
                change: change,
                processfail: processFail
            };
            $form.fileupload(opts).on('fileuploadadded', fileUploadAdded);
        };

        function upload(clientSupportRequestId, clientSupportRequestMessageId) {

            var promise = $.Deferred();

            var data = {
                __RequestVerificationToken: $('#__AjaxAntiForgeryForm input[name=__RequestVerificationToken]').val(),
                ClientSupportRequestId: clientSupportRequestId,
                ClientSupportRequestMessageId: clientSupportRequestMessageId
            };

            if ($form.find('tbody.files > tr').length > 0) {

                $form.fileupload('option', {
                    formData: data
                }).off('fileuploadalways').on('fileuploadalways', function() {
                    fileUploadAlways(promise);
                });

                $form.find('button.start').click();

            } else {
                promise.resolve();
            }

            return promise;

        };

        return {
            init: init,
            clear: clear,
            upload: upload
        }

    });

    clientSupport.ClientSupportRequestAction = (function() {

        function show(clientSupportRequestId, opts) {

            opts = opts || {};

            $.blockUI();

            var promise = $.Deferred();

            window.VisitPay.Common.ModalNoContainerAsync('modalClientSupportRequestAction', '/ClientSupport/ClientSupportRequestAction?clientSupportRequestId=' + clientSupportRequestId, 'GET', null, true).done(function($modal) {

                $.unblockUI();

                $modal.on('click', '[data-dismiss="modal"]', function () {
                    promise.reject();
                });

                var $form = $modal.find('form');
                $form.parseValidation();
                $form.on('submit', function(e) {

                    e.preventDefault();
                    if (!$(this).valid()) {
                        return;
                    }

                    var action = $(this).attr('action');
                    var data = $(this).serializeToObject();

                    $modal.modal('hide');

                    $.blockUI();
                    $.post(action, data, function(newStatus) {
                        promise.resolve(newStatus);
                        if (opts.unblockOnSubmit === true) {
                            $.unblockUI();
                        }
                    });

                });

            });

            return promise;
        };

        return {
            show: show
        }

    })();

    clientSupport.ClientSupportRequestCreate = (function() {

        var ViewModel = function(data) {

            var self = this;

            self.ParentTopics = ko.observableArray([]).extend({ extendedArray: {} });
            self.ParentTopics.SelectedId = ko.observable();
            self.ParentTopics.Selected = ko.computed(function() {
                return ko.utils.arrayFirst(self.ParentTopics(), function(topic) {
                    return ko.unwrap(topic.ClientSupportTopicId) === ko.unwrap(self.ParentTopics.SelectedId);
                });
            });

            self.Topics = ko.computed(function() {
                if (!ko.unwrap(self.ParentTopics.Selected())) {
                    return [];
                }
                return self.ParentTopics.Selected().Topics();
            });
            self.Topics.Any = ko.computed(function() {
                return self.Topics && ko.unwrap(self.Topics).length > 0;
            });
            self.Topics.SelectedId = ko.observable();

            ko.mapping.fromJS(data, {}, self.ParentTopics);

            return self;

        };

        function show(opts, referenceGuarantorSupportRequest) {

            opts = opts || {};
            var createClientRequestUrl ='/ClientSupport/ClientSupportRequestCreate';
            if
            (opts.hasOwnProperty('onBehalfOfClient') ===true) {
                if(opts.onBehalfOfClient ===true) {
                    createClientRequestUrl = createClientRequestUrl + '?onBehalfOfClient=true';
                }
            
            }
            $.blockUI();

            var promise = $.Deferred();

            var promises = [];
            var promiseTopics = $.Deferred();
            promises.push(window.VisitPay.Common.ModalNoContainerAsync('modalClientSupportRequestCreate', createClientRequestUrl, 'GET', null, false));
            promises.push(promiseTopics);

            $.when.apply($, promises).done(function($modal, topics) {

                var vm = new ViewModel(topics);

                if (referenceGuarantorSupportRequest !== undefined &&
                    referenceGuarantorSupportRequest !== null &&
                    referenceGuarantorSupportRequest.GuarantorMessages !== undefined &&
                    referenceGuarantorSupportRequest.GuarantorMessages !== null &&
                    referenceGuarantorSupportRequest.GuarantorMessages.length > 0) {
                    $modal.find('#MessageBody').val('Original Message: ' + referenceGuarantorSupportRequest.GuarantorMessages[referenceGuarantorSupportRequest.GuarantorMessages.length - 1].MessageBody);
                }

                ko.applyBindings(vm, $modal[0]);

                $modal.modal('show');
                $.unblockUI();

                $modal.on('click', '[data-dismiss="modal"]', function () {
                    promise.reject();
                });

                var uploader = new clientSupport.ClientSupportFileUploader($modal.find('#frmClientSupportAttachments'));
                uploader.init();

                var $form = $modal.find('form:eq(0)');
                $form.parseValidation();
                $form.off('submit').on('submit', function(e) {
                    e.preventDefault();
                });

                $modal.find('#btnCreateClientSupportRequest').off('click').on('click', function(e) {

                    e.preventDefault();
                    if (!$form.valid()) {
                        return;
                    }

                    // remove html from message body
                    var $messageBody = $form.find('#MessageBody');
                    var $temp = $('<div>');
                    $temp.html($messageBody.val());
                    $messageBody.val($temp.text());

                    var action = $form.attr('action');
                    var data = $form.serializeToObject();

                    if (referenceGuarantorSupportRequest !== undefined && referenceGuarantorSupportRequest !== null) {
                        $.extend(data, { 'referenceGuarantorSupportRequestId': ko.unwrap(referenceGuarantorSupportRequest.SupportRequestId) });
                    }

                    $.blockUI();
                    $modal.addClass('hidden');
                    $('.modal-backdrop').addClass('hidden');

                    $.post(action, data, function(result) {
                        uploader.upload(result.ClientSupportRequestId, result.ClientSupportRequestMessageId).done(function() {
                            $modal.modal('hide');
                            promise.resolve();

                            // trigger menu update for unread counts
                            $(document).trigger('clientSupportRequestChanged');

                            if (opts.unblockOnSubmit === true) {
                                $.unblockUI();
                            }
                        });
                    });
                });

            });

            $.post('/ClientSupport/ClientSupportTopics', {}, function(data) {
                promiseTopics.resolve(data);
            });

            return promise;

        };

        return {
            show: show
        }

    });

    clientSupport.ClientSupportRequestEdit = (function (isAdmin, clientSupportRequestId, markAsRead) {

        var ViewModel = function(data) {

            var self = this;

            //
            self.AssignedVisitPayUserId = ko.observable();
            self.ClientSupportRequestDisplayId = ko.observable();
            self.ClientSupportRequestStatus = ko.observable();
            self.ClientSupportRequestStatus.HeaderText = ko.computed(function() {
                var status = ko.unwrap(self.ClientSupportRequestStatus);
                if (!status) {
                    return '';
                }
                return parseInt(status) === 1 ? 'Opened on ' + ko.unwrap(self.InsertDate) : 'Closed on ' + ko.unwrap(self.ClosedDate);
            });
            self.ClientSupportRequestStatus.IsClosed = ko.computed(function () {
                var status = ko.unwrap(self.ClientSupportRequestStatus);
                if (!status) {
                    return false;
                }
                return parseInt(status) === 2;
            });
            self.ClientSupportRequestStatus.IsOpen = ko.computed(function () {
                return !self.ClientSupportRequestStatus.IsClosed();
            });
            self.ContactPhone = ko.observable();
            self.CreatedByVisitPayUserEmail = ko.observable();
            self.CreatedByVisitPayUserName = ko.observable();
            self.CreatedByVisitPayUserId = ko.observable();
            self.InsertDate = ko.observable();
            self.ClosedDate = ko.observable();
            self.IsAdmin = ko.observable();
            self.JiraId = ko.observable();
            self.TargetVpGuarantorId = ko.observable();
            self.TopicDisplay = ko.observable();
            self.Attachments = ko.observableArray([]).extend({ extendedArray: {} });
            self.Attachments.Applicable = ko.observableArray([]).extend({ extendedArray: {} });
            self.Attachments.Applicable.Set = function(clientSupportRequestMessageId) {
                self.Attachments.Applicable([]);
                if (clientSupportRequestMessageId && clientSupportRequestMessageId > 0) {
                    ko.utils.arrayForEach(self.Attachments(), function(attachment) {
                        if (ko.unwrap(attachment.ClientSupportRequestMessageId) === clientSupportRequestMessageId) {
                            self.Attachments.Applicable.push(attachment);
                        }
                    });
                } else {
                    self.Attachments.Applicable(self.Attachments());
                }
            };
            self.AttachmentMalwareScanStatusDisplay = function (status) {
                var unwrappedStatus = ko.unwrap(status);
                switch (unwrappedStatus) {
                    case 1:
                        return 'Not Scanned';
                    case 2:
                        return 'No Malware Detected';
                    case 3:
                        return 'Malware Detected';
                }
            };
            self.AdminVisitPayUsers = ko.observableArray([]);

            // this is only set on refresh()
            self.JiraIdDisplay = ko.observable();
            self.JiraIdDisplay.Any = ko.computed(function() {
                return ko.unwrap(self.JiraIdDisplay) && ko.unwrap(self.JiraIdDisplay).length > 0;
            });
            self.JiraIdDisplay.Href = ko.computed(function () {
                return self.JiraIdDisplay.Any() ? 'https://ivinci.atlassian.net/browse/' + ko.unwrap(self.JiraIdDisplay) : '';
            });

            // this is only set on refresh()
            self.TargetVpGuarantorIdDisplay = ko.observable();
            self.TargetVpGuarantorIdDisplay.Any = ko.computed(function () {
                return ko.unwrap(self.TargetVpGuarantorIdDisplay) && ko.unwrap(self.TargetVpGuarantorIdDisplay).toString().length > 0;
            });
            self.TargetVpGuarantorIdDisplay.Href = ko.computed(function () {
                return self.TargetVpGuarantorIdDisplay.Any() ? '/Search/GuarantorAccount/' + ko.unwrap(self.TargetVpGuarantorIdDisplay) : '';
            });

            //
            self.MessagesClient = ko.observableArray([]);
            self.MessagesClient.SortOrder = ko.observable(1);
            self.MessagesClient.SortOrder.IsAsc = ko.computed(function() {
                return ko.unwrap(self.MessagesClient.SortOrder) === 0;
            });
            self.MessagesClient.SortOrder.IsDesc = ko.computed(function () {
                return ko.unwrap(self.MessagesClient.SortOrder) === 1;
            });
            self.MessagesClient.SortOrder.Text = ko.computed(function() {
                return ko.unwrap(self.MessagesClient.SortOrder.IsAsc) ? 'This will sort the messages from newest to oldest' : 'This will sort the messages from oldest to newest';
            });
            self.MessagesClient.Sorted = ko.computed(function () {
                return self.MessagesClient().sort(function (l, r) {
                    if (self.MessagesClient.SortOrder() === 0) {
                        return l.ClientSupportRequestMessageId() === r.ClientSupportRequestMessageId() ? 0 : (l.ClientSupportRequestMessageId() < r.ClientSupportRequestMessageId() ? -1 : 1);
                    } else {
                        return l.ClientSupportRequestMessageId() === r.ClientSupportRequestMessageId() ? 0 : (l.ClientSupportRequestMessageId() < r.ClientSupportRequestMessageId() ? 1 : -1);
                    }
                });
            });
            self.MessagesClient.ToggleSortOrder = function () {
                self.MessagesClient.SortOrder() === 0 ? self.MessagesClient.SortOrder(1) : self.MessagesClient.SortOrder(0);
            };

            //
            self.MessagesInternal = ko.observableArray([]);
            self.MessagesInternal.SortOrder = ko.observable(1);
            self.MessagesInternal.SortOrder.IsAsc = ko.computed(function () {
                return ko.unwrap(self.MessagesInternal.SortOrder) === 0;
            });
            self.MessagesInternal.SortOrder.IsDesc = ko.computed(function () {
                return ko.unwrap(self.MessagesInternal.SortOrder) === 1;
            });
            self.MessagesInternal.SortOrder.Text = ko.computed(function () {
                return ko.unwrap(self.MessagesInternal.SortOrder.IsAsc) ? 'This will sort the messages from newest to oldest' : 'This will sort the messages from oldest to newest';
            });
            self.MessagesInternal.Sorted = ko.computed(function () {
                return self.MessagesInternal().sort(function (l, r) {
                    if (self.MessagesInternal.SortOrder() === 0) {
                        return l.ClientSupportRequestMessageId() === r.ClientSupportRequestMessageId() ? 0 : (l.ClientSupportRequestMessageId() < r.ClientSupportRequestMessageId() ? -1 : 1);
                    } else {
                        return l.ClientSupportRequestMessageId() === r.ClientSupportRequestMessageId() ? 0 : (l.ClientSupportRequestMessageId() < r.ClientSupportRequestMessageId() ? 1 : -1);
                    }
                });
            });
            self.MessagesInternal.ToggleSortOrder = function () {
                self.MessagesInternal.SortOrder() === 0 ? self.MessagesInternal.SortOrder(1) : self.MessagesInternal.SortOrder(0);
            };

            //
            self.MessageBody = ko.observable();
            self.MessageBody.Placeholder = ko.computed(function() {
                return ko.unwrap(self.IsAdmin) ? 'Click to add a new note (you can send attachments, too)' : 'Click here to reply (you can send attachments, too)';
            });
            self.MessageBody.IsVisible = ko.computed(function() {
                return ko.unwrap(self.IsAddMessage) || !ko.unwrap(self.IsAdmin);
            });
            self.IsAddMessage = ko.observable(false);
            self.Discard = function() {
                self.IsAddMessage(false);
                self.MessageBody('');
            };

            //Error message
            self.ErrorMessages = ko.observable();
            self.ErrorMessages.Any = ko.computed(function () {
                return (ko.unwrap(self.ErrorMessages) && ko.unwrap(self.ErrorMessages).length > 0);
            });

            self.Refresh = function (newData) {
                ko.mapping.fromJS(newData, {}, self);
                self.JiraIdDisplay(newData.JiraId);
                self.TargetVpGuarantorIdDisplay(newData.TargetVpGuarantorId);

                setTimeout(function() {
                    $('.messages div.body').each(function () {
                        if ($(this).find('p:eq(0)').height() > 36) {
                            $(this).find('.more').show();
                        }
                    });
                    $('.messages div.body .more, .messages div.body .less').off('click').on('click', function(e) {

                        e.preventDefault();

                        var $body = $(this).parents('.body:eq(0)');
                        var $less = $body.find('.less');
                        var $more = $body.find('.more');

                        if ($more.is(':visible')) {
                            $less.show();
                            $more.hide();
                            $body.css('height', 'auto');
                        } else if ($less.is(':visible')) {
                            $less.hide();
                            $more.show();
                            $body.css('height', '36px');
                        }

                    });
                }, 500);

            };
            self.Refresh(data);

            return self;

        };

        // global
        var vm;
        var $modal;
        var uploader;
        var promise = $.Deferred();
        markAsRead = markAsRead === undefined || markAsRead === null ? true : false;

        // utility

        function loadData() {
            var promise = $.Deferred();
            $.post('/ClientSupport/ClientSupportRequestData', { isAdmin: isAdmin, clientSupportRequestId: clientSupportRequestId }, function (data) {
                promise.resolve(data);
            });
            return promise;
        };

        function hideModal(b) {
            if (b) {
                $modal.removeClass('in');
                $('.modal-backdrop').removeClass('in');
            } else {
                $modal.addClass('in');
                $('.modal-backdrop').addClass('in');
            }
        };

        // status, action dropdown

        function changeStatus(e) {

            if (e) {
                e.preventDefault();
            }

            hideModal(true);

            clientSupport.ClientSupportRequestAction.show(clientSupportRequestId).done(function(newStatus) {

                // closed
                if (parseInt(newStatus) === 2) {
                    $modal.modal('hide');
                    promise.resolve();
                    return;
                }

                // re-open
                $.unblockUI();
                loadData().done(function(newData) {
                    vm.Refresh(newData);
                    hideModal(false);
                });

            }).fail(function() {

                hideModal(false);

            });
        };

        function doAction(e) {
            e.preventDefault();
            switch ($(this).val()) {
                case 'status':
                    changeStatus();
                    break;
                case 'print':
                    window.open('/ClientSupport/Print?i=' + isAdmin + '&clientSupportRequestId=' + clientSupportRequestId);
                    break;
            }
            $(this).val('');
        };

        // attachments

        function downloadAttachments(clientSupportRequestMessageId, attachmentId) {

            $.blockUI();
            $.post('/ClientSupport/PrepareDownload', {
                clientSupportRequestId: clientSupportRequestId,
                clientSupportRequestMessageId: clientSupportRequestMessageId || null,
                clientSupportRequestMessageAttachmentId: attachmentId || null,
                isAdmin: isAdmin
            }, function (result) {
                $.unblockUI();
                if (result.Result === true) {
                    window.location.href = result.Message;
                } else {
                    VisitPay.Common.ModalGenericAlert("Unable to Download", result.Message, "Ok");
                }
            });

        };

        var deleteAttachment = function(attachmentId, supportRequestMessageAttachmentName, $supportAttachmentsModal) {
    
            $supportAttachmentsModal.hide();

            var promise = window.VisitPay.Common.ModalGenericConfirmation('Confirm Delete', 'Are you sure you want to delete ' + supportRequestMessageAttachmentName + '?', 'Yes', 'No','You action will be logged for auditing purposes', 'vp-primary');

            promise.done(function () {
            
                $.post('/ClientSupport/DeleteAttachment', {
                    clientSupportRequestId: clientSupportRequestId,
                    clientSupportRequestMessageAttachmentId: attachmentId,
                    visitPayUserId: ko.unwrap(vm.CreatedByVisitPayUserId)
                }, function(result) {
                    window.location.reload();
                }, 'json');
            });

            promise.fail(function() {
                $supportAttachmentsModal.show();
            });

        };

        function showAttachments(e, clientSupportRequestMessageId) {

            e.preventDefault();
            $.blockUI();
            vm.Attachments.Applicable.Set(clientSupportRequestMessageId);
            window.VisitPay.Common.ModalNoContainerAsync('modalClientSupportRequestAttachments', '/ClientSupport/ClientSupportRequestAttachments', 'GET', {}, false).done(function($m) {
                $.unblockUI();
                ko.applyBindings(vm, $m[0]);

                $m.find('.icon-file-type').each(function () {
                    $(this).addClass(getFileType(ko.unwrap(ko.dataFor($(this)[0]).MimeType)));
                });

                $m.find('a').click(function (e) {
                    e.preventDefault();
                    downloadAttachments(clientSupportRequestMessageId, ko.unwrap(ko.dataFor($(this)[0]).ClientSupportRequestMessageAttachmentId));
                });

                $m.find('button.srma-delete').click(function (e) {
                    e.preventDefault();
                    var attachmentId = ko.unwrap(ko.dataFor($(this)[0]).ClientSupportRequestMessageAttachmentId);
                    var supportRequestMessageAttachmentName = ko.unwrap(ko.dataFor($(this)[0]).AttachmentFileName);

                    deleteAttachment(attachmentId, supportRequestMessageAttachmentName, $m);
                });

                $m.find('button[type=submit]').click(function (e) {
                    e.preventDefault();
                    downloadAttachments(clientSupportRequestMessageId);
                });

                $m.modal('show');
            });

        };

        // details
        function saveDetails(e) {
            e.preventDefault();
            $.blockUI();
            $.post($(this).attr('action'), { clientSupportRequestId: clientSupportRequestId, assignedToVisitPayUserId: ko.unwrap(vm.AssignedVisitPayUserId), targetVpGuarantorId: ko.unwrap(vm.TargetVpGuarantorId), jiraId: ko.unwrap(vm.JiraId) }, function(errors) {

                vm.ErrorMessages(errors);

                loadData().done(function(newData) {
                    vm.Refresh(newData);
                    $.unblockUI();
                });

            });
        }

        // message

        function saveMessage(e) {

            e.preventDefault();
            if (!$(this).valid()) {
                return;
            }

            var messageType = ko.unwrap(vm.IsAdmin) ? 3 : 1; // internal : fromclient

            $.blockUI();
            $.post($(this).attr('action'), { clientSupportRequestId: clientSupportRequestId, messageBody: ko.unwrap(vm.MessageBody), messageType: messageType }, function(result) {

                // upload
                uploader.upload(clientSupportRequestId, result.ClientSupportRequestMessageId).done(function() {

                    // reload
                    loadData().done(function (newData) {

                        // clear textbox, refresh ui
                        vm.Discard();
                        vm.Refresh(newData);

                        // trigger menu update for unread counts
                        $(document).trigger('clientSupportRequestChanged');

                        //
                        $.unblockUI();

                    });

                });

            });
        };

        //
        function show() {

            $.blockUI();

            var promises = [];
            promises.push(window.VisitPay.Common.ModalNoContainerAsync('modalClientSupportRequest', '/ClientSupport/ClientSupportRequest', 'GET', { isAdmin: isAdmin, clientSupportRequestId: clientSupportRequestId, markAsRead: markAsRead }, false));
            promises.push(loadData());

            $.when.apply($, promises).done(function($m, data) {

                $modal = $m;

                vm = new ViewModel(data);
                ko.applyBindings(vm, $modal[0]);

                $.unblockUI();
                $modal.modal('show');
                $modal.find('#frmMessage').parseValidation();

                // uploader
                var $formUpload = $modal.find('#frmClientSupportAttachments');
                uploader = new clientSupport.ClientSupportFileUploader($formUpload);
                uploader.init();

                $formUpload.on('fileUploadCountChange', function (e, count) {
                    if (count > 0) {
                        $formUpload.show();
                    } else {
                        $formUpload.hide();
                        $modal.find('#txtMessage').focus();
                    }
                });

                // details
                $modal.find('#txtTargetVpGuarantorId').makeNumericInput({ allowDecimal: false, allowNegative: true, addClass: false });

                // close events
                $modal.on('click', '[data-dismiss="modal"]', function() {
                    $.blockUI();
                    promise.resolve();
                });

                // save assigned and jira id
                $modal.on('submit', '#frmSaveDetails', saveDetails);

                // actions dropdown
                $modal.on('click', '#ddAction', doAction);

                // attachments
                $modal.on('click', '[data-action="showattachments"]', showAttachments);
                $modal.on('click', '.messages .glyphicon-paperclip', function(e) {
                    showAttachments(e, ko.unwrap(ko.dataFor($(this)[0]).ClientSupportRequestMessageId));
                });

                // status button
                $modal.on('click', '[data-action="status"]', changeStatus);

                // message
                $modal.on('focus', '#txtMessage', function () {
                    vm.IsAddMessage(true);
                });
                $modal.on('submit', '#frmMessage', saveMessage);
                $modal.on('click', '#btnMessageAttach', function (e) {
                    e.preventDefault();
                    $modal.find('#frmClientSupportAttachments input[type=file]').click();
                });
                $modal.on('click', '[name="btnMessageSave"]', function(e) {
                    e.preventDefault();
                    $modal.find('#frmMessage').submit();
                });
                $modal.on('click', '#btnMessageDiscard', function() {
                    window.VisitPay.Common.ModalGenericConfirmation('Confirm Discard', 'This will not be saved as a draft.  Do you want to discard?', 'Yes', 'No', null, 'vp-primary').done(function() {
                        vm.Discard();
                        $modal.find('#frmMessage').parseValidation();
                        uploader.clear();
                    }).fail(function() {
                        $modal.find('#txtMessage').focus();
                    });
                });

                // reply
                $modal.on('click', '#btnReplyRequestor', function(e) {
                    e.preventDefault();

                    // prevent modal from being removed from dom
                    $modal.off('hidden.bs.modal').modal('hide');

                    var reply = new clientSupport.ClientSupportRequestReply(clientSupportRequestId);
                    reply.show().done(function () {
                        loadData().done(function (newData) {
                            vm.Refresh(newData);
                            $modal.modal('show');
                            $modal.on('hidden.bs.modal', function () { $modal.remove(); }); // restore default functionality
                        });
                    }).fail(function() {
                        $modal.modal('show');
                        $modal.on('hidden.bs.modal', function () { $modal.remove(); }); // restore default functionality
                    });
                });

                // internal notes
                $modal.on('click', '#btnInternalNoteAdd', function (e) {
                    vm.IsAddMessage(true);
                });


                // subscribe
                vm.IsAddMessage.subscribe(function(newValue) {
                    $formUpload.hide();
                });

                // trigger menu update for unread counts
                $(document).trigger('clientSupportRequestChanged');

            });

            return promise;

        };

        //
        return {
            show: show
        }

    });

    clientSupport.ClientSupportRequestReply = (function(clientSupportRequestId) {

        var ViewModel = function (data) {

            var self = this;

            self.Topics = ko.observableArray([]).extend({ extendedArray: {} });
            
            ko.mapping.fromJS(data, {}, self.Topics);

            return self;

        };

        // global
        var $modal;
        var promise = $.Deferred();
        var vm;
        var uploader;

        // utility

        function loadTopics() {

            var promise = $.Deferred();

            $.post('/ClientSupport/ClientSupportTopics', { withTemplates: true }, function (data) {
                promise.resolve(data);
            });

            return promise;
        };

        // tree control
        function bindTree() {
            $modal.find('.topics').jstree()
                .delegate('.jstree-open>a', 'click.jstree', function(event) {
                    $.jstree.reference(this).close_node(this, false, false);
                })
                .delegate('.jstree-closed>a', 'click.jstree', function(event) {
                    $.jstree.reference(this).open_node(this, false, false);
                });

            $modal.find('.topics li:eq(0)').children('a').addClass('jstree-clicked');
        };

        function selectTemplate(e) {

            // a little bit of a mess because the tree eats knockout bindings
            var $li = $(this).parents('li:eq(0)');
            var topicId = parseInt($li.attr('data-topic') || 0);
            var templateIndex = parseInt($li.attr('data-template') || 0);

            var template;

            if (topicId > 0) {
                var topic;
                ko.utils.arrayForEach(vm.Topics(), function(t1) {
                    if (parseInt(ko.unwrap(t1.ClientSupportTopicId)) === topicId) {
                        topic = t1;
                    } else {
                        ko.utils.arrayForEach(t1.Topics(), function(t2) {
                            if (parseInt(ko.unwrap(t2.ClientSupportTopicId)) === topicId) {
                                topic = t2;
                            }
                        });
                    }
                });

                if (topic) {
                    template = ko.unwrap(topic.Templates)[templateIndex];
                }
            }

            if (template) {
                $modal.find('#txtReply').val(ko.unwrap(template.ContentBody));
            } else {
                $modal.find('#txtReply').val('');
            }

        };

        // save
        function sendReply(e) {

            e.preventDefault();

            if (!$(this).valid()) {
                return;
            }

            $.blockUI();
            $.post($(this).attr('action'), { clientSupportRequestId: clientSupportRequestId, messageBody: $(this).find('#txtReply').val(), messageType: 2 }, function (result) {

                // upload
                uploader.upload(clientSupportRequestId, result.ClientSupportRequestMessageId).done(function() {

                    $modal.modal('hide');
                    $.unblockUI();
                    promise.resolve();

                    // trigger menu update for unread counts
                    $(document).trigger('clientSupportRequestChanged');

                });

            });
        };

        //
        function show() {

            $.blockUI();

            var promises = [];
            promises.push(window.VisitPay.Common.ModalNoContainerAsync('modalClientSupportRequestReply', '/ClientSupport/ClientSupportRequestReply', 'GET', { clientSupportRequestId: clientSupportRequestId }, false));
            promises.push(loadTopics());

            $.when.apply($, promises).done(function($m, topics) {

                $modal = $m;

                vm = new ViewModel(topics);
                ko.applyBindings(vm, $modal[0]);

                $.unblockUI();
                $modal.modal('show');
                $modal.find('#frmReply').parseValidation();

                // tree
                bindTree();

                // close events
                $modal.on('click', '[data-dismiss="modal"]', function() {
                    promise.reject();
                });

                // uploader
                var $formUpload = $modal.find('#frmClientSupportAttachments');
                uploader = new clientSupport.ClientSupportFileUploader($formUpload);
                uploader.init();

                $formUpload.on('fileUploadCountChange', function (e, count) {
                    if (count > 0) {
                        $modal.find('.topics').height(366);
                        $formUpload.show();
                    } else {
                        $modal.find('.topics').height(493);
                        $formUpload.hide();
                        $modal.find('#txtReply').focus();
                    }
                });

                // select
                $modal.on('click', '.topics .jstree-leaf > a', selectTemplate);

                // message
                $modal.on('submit', '#frmReply', sendReply);
                $modal.on('click', '#btnReply', function (e) {
                    e.preventDefault();
                    $modal.find('#frmReply').submit();
                });
                $modal.on('click', '#btnReplyAttach', function (e) {
                    e.preventDefault();
                    $modal.find('#frmClientSupportAttachments input[type=file]').click();
                });
                $modal.on('click', '#btnReplyDiscard', function () {
                    window.VisitPay.Common.ModalGenericConfirmation('Confirm Discard', 'This will not be saved as a draft.  Do you want to discard?', 'Yes', 'No', null, 'vp-primary').done(function () {
                        $modal.modal('hide');
                        uploader.clear();
                        promise.reject();
                    }).fail(function () {
                        $modal.find('#txtMessage').focus();
                    });
                });

            });

            return promise;
        };

        //
        return {
            show: show
        };

    });

})(window.VisitPay.ClientSupport = window.VisitPay.ClientSupport || {});
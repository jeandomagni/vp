﻿window.VisitPay.SupportRequestAction = (function(common, $, ko) {

    //
    var supportRequestAction = {};

    var SupportRequestActionViewModel = function() {

        var self = this;

        self.SupportRequestId = ko.observable();

        self.SupportTopicId = ko.observable();
        self.SupportTopicId.Data = ko.observableArray([]);
        self.SupportTopicId.IsEnabled = ko.computed(function () {
            return ko.unwrap(self.SupportTopicId.Data).length > 0;
        });

        self.SupportSubTopicId = ko.observable();
        self.SupportSubTopicId.Data = ko.observableArray([]);
        self.SupportSubTopicId.IsEnabled = ko.computed(function () {
            return ko.unwrap(self.SupportSubTopicId.Data).length > 0;
        });

    };

    var model;

    var loadTopics = function (supportRequestId) {

        var deferred = $.Deferred();

        $.get('/Support/SupportTopics?supportRequestId=' + supportRequestId, {}, function (data) {
            deferred.resolve(data.SupportTopics);

        });

        return deferred;

    }

    var loadData = function (supportRequestId, visitPayUserId) {

        var promiseData = $.Deferred();
        $.post('/Support/SupportRequest', { supportRequestId: supportRequestId, visitPayUserId: visitPayUserId }, function (data) {
            promiseData.resolve(data);
        }, 'json');

        return promiseData;

    };

    var setSubTopics = function (supportTopicId) {

        var items = [];
        ko.utils.arrayFirst(ko.unwrap(model.SupportTopicId.Data), function(topic) {

            if (ko.unwrap(topic.SupportTopicId) === supportTopicId) {
                items = ko.unwrap(topic.Items);
            }

        });

        model.SupportSubTopicId.Data(items);

    };

    supportRequestAction.Initialize = function (modalContainer, supportRequestStatus, supportRequestId, supportRequestDisplayId, visitPayUserId) {

        var promise = window.VisitPay.Common.ModalContainerAsync(modalContainer, '/Support/SupportRequestAction', 'POST', {
            supportRequestStatus: supportRequestStatus,
            supportRequestId: supportRequestId,
            supportRequestDisplayId: supportRequestDisplayId,
            visitPayUserId: visitPayUserId
        }, false);

        var promiseTopics = loadTopics(supportRequestId);
        var promiseData = loadData(supportRequestId, visitPayUserId);

        $.when(promise, promiseTopics, promiseData).done(function (modal, topics, data) {
            
            modal.modal('show');

            var $scope = modal;
            //
            model = new SupportRequestActionViewModel();
            ko.mapping.fromJS(topics, null, model.SupportTopicId.Data);
            ko.mapping.fromJS(data, null, model);

            setSubTopics(ko.unwrap(model.SupportTopicId));
            model.SupportSubTopicId(ko.unwrap(model.SupportSubTopicId));

            ko.applyBindings(model, $scope[0]);

            model.SupportTopicId.subscribe(setSubTopics);

            var $form = modal.find('form');

            common.ResetUnobtrusiveValidation($form);

            setTimeout(function() {
                $form.find('textarea').focus();
            }, 500);

            modal.on('click', '#btnCloseSupportRequestSubmit', function(e) {
                e.preventDefault();

                if ($form.valid()) {
                    modal.modal('hide');
                    $.blockUI();

                    var topicPromise = $.Deferred();
                    var closePromise = $.Deferred();

                    $.post('/Support/SetSupportTopic', { supportRequestId: supportRequestId, supportTopicId: ko.unwrap(model.SupportSubTopicId), visitPayUserId: visitPayUserId }, function () {
                        topicPromise.resolve();
                    });

                    $.post('/Support/CompleteSupportRequestAction', $form.serialize() + '&__RequestVerificationToken=' + $('#__AjaxAntiForgeryForm input[name=__RequestVerificationToken]').val(), function () {
                        closePromise.resolve();
                    });

                    $.when(topicPromise, closePromise).done(function () {
                        $.unblockUI();

                        //Make sure to always trigger this here, since supportRequestAction.OnCompleted() can be overridden on certain pages
                        $(document).trigger('supportRequestActionCompleted');
                    });

                }

            });

            $.unblockUI();
        });

        return promise;

    };

    supportRequestAction.OnCompleted = function () {
        $(document).trigger('supportRequestActionCompleted');
    };

    return supportRequestAction;

})(window.VisitPay.Common, jQuery, window.ko);
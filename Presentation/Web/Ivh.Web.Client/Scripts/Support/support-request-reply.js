﻿window.VisitPay.SupportRequestReply = (function (common, $, ko) {
    var lastFocus = null; //Keeps track of where in the text area we were last focused, for tracking position to insert template tags
    function insertTextAtCursor(field, text) {
        //IE support
        if (document.selection) {
            field.focus();
            var sel = document.selection.createRange();
            sel.text = text;
        }
        //MOZILLA and others
        else if (field.selectionStart || field.selectionStart == '0') {
            var startPos = field.selectionStart;
            var endPos = field.selectionEnd;
            field.value = field.value.substring(0, startPos)
                + text
                + field.value.substring(endPos, field.value.length);
        } else {
            field.value += text;
        }
        // trigger change to force observable update
        $(field).trigger('change');
    }

    var supportRequestReplyViewModel = function() {
        
        var self = this;

        self.Topics = ko.observableArray([]);
        self.LastContent = ko.observable(null);
        self.LastTemplate = ko.observable(null);
        self.ReplyMessage = ko.observable();
        self.ReplyMessage.IsEmpty = ko.computed(function() {
            var contents = self.ReplyMessage();
            if (contents) {
                return false;
            }
            return true;
        });

        //Custom template creation
        self.CustomTemplates = ko.observableArray([]);
        self.CustomTemplates.Update = function (jsonTemplates) {
            //Clear
            self.CustomTemplates.removeAll();

            //Populate
            ko.mapping.fromJS(jsonTemplates, null, self.CustomTemplates);
        };

        self.CreatingCustomTemplate = ko.observable(false); //Tracks if we are currently creating a custom template
        self.TemplateBeingShown = ko.observable(null);
        self.TemplateBeingShown.CanBeManaged = function(isAdmin) {
            if (self.TemplateBeingShown() != null) {
                if (isAdmin === true) {
                    //Admins can manage any templates in their list
                    return true;
                } else if (ko.unwrap(self.TemplateBeingShown().VisitPayUserId) == null) {
                    //Global template but user isn't admin, can't manage
                    return false;
                } else {
                    //Template is for the user
                    return true;
                }
            }
            
            //Not showing a custom template
            return false;
        };
        self.ShowingCustomTemplate = ko.computed(function () {
            var templateShown = self.TemplateBeingShown();
            if (templateShown) {
                return true;
            }
            return false;
        });

        self.CustomTemplateName = ko.observable(null);
        self.CustomTemplateName.IsEmpty = ko.computed(function () {
            var contents = self.CustomTemplateName();
            if (contents) {
                return false;
            }
            return true;
        });
        
        self.TemplateReplacementTags = ko.observableArray(null);

        self.InsertTag = function (tag) {
            if (lastFocus) {
                lastFocus.focus();
                insertTextAtCursor(lastFocus, tag);
                lastFocus = null;
            }
        };

        self.EditTemplate = function () {
            if (self.TemplateBeingShown() !== null) {
                var templateId = ko.unwrap(self.TemplateBeingShown().SupportTemplateId);

                //Load the template from the server so we get it with the original tags
                $.post('/Support/GetSupportTemplate', { supportTemplateId: templateId }, function (data) {
                    self.StartEditingTemplate(false);
                    self.CustomTemplateName(data.Title);
                    self.ReplyMessage(data.Content);
                });
            }
        };

        self.StartEditingTemplate = function (isCreate) {
            self.CreatingCustomTemplate(true);
            self.ClearTemplateFields();

            //Don't clear template being shown when editing a template, so that we keep track of the TemplateId
            if (isCreate) {
                self.TemplateBeingShown(null);
            }
            
            $('#frmReply').find('#MessageBody').focus();
        }

        self.FinishEditingTemplate = function() {
            self.CreatingCustomTemplate(false);
            self.ClearTemplateFields();
            self.TemplateBeingShown(null);
            lastFocus = null;
        }

        self.ClearTemplateFields = function() {
            self.CustomTemplateName(null);
            self.ReplyMessage(null);
            self.LastContent(null);
            self.LastTemplate(null);
        }

        self.GetIconForCustomTemplate = function(template) {
            var templateUserId = ko.unwrap(template.VisitPayUserId);
            if (templateUserId) {
                //Private template
                return JSON.stringify({ "icon": "glyphicon glyphicon-user" });
            } else {
                //Global template
                return JSON.stringify({ "icon": "glyphicon glyphicon-globe" });
            }
        }
    };
    
    //
    var $scope;
    var model;
    var internal = {
        supportRequestId: null,
        visitPayUserId: null
    };

    //
    var bindTree = function () {
        $scope.find('.topics').jstree()
            .delegate('.jstree-open>a', 'click.jstree', function(event) {
                $.jstree.reference(this).close_node(this, false, false);
            })
            .delegate('.jstree-closed>a', 'click.jstree', function(event) {
                $.jstree.reference(this).open_node(this, false, false);
            });
    };

    var getTextArea = function() {
        return $scope.find('textarea.message');
    };

    var selectLastTemplate = function () {
        //Clear out selection
        $scope.find('.topics a').removeClass('jstree-clicked');

        if (!model.TemplateBeingShown()) {
            //Non-custom template
            $scope.find('.topics a:eq(' + model.LastTemplate() + ')').addClass('jstree-clicked');
        } else {
            //Custom template
            var lastCustom = $scope.find('.topics').find('[data-custom-template-index=' + model.LastTemplate() + ']');
            lastCustom.addClass('jstree-clicked');
        }
    }

    var selectTemplateWithPrompt = function (selectActionFunction) {
        if (model.LastContent() != null && model.ReplyMessage() != model.LastContent()) {
            var promise = common.ModalGenericConfirmation('Confirm Discard', 'Your changes will not be saved.<br /><br />Continue?');
            promise.done(function () {
                selectActionFunction();
            });
            promise.fail(function () {
                selectLastTemplate();
            });
        } else {
            selectActionFunction();
        }
    }

    var selectTemplate = function($element) {
        var action = function () {
            var index = $element.index('.topics a');

            model.ReplyMessage(null);

            if (index > 0) {
                var $elementTemplate = $element.parent('li');
                var $elementSubtopic = $elementTemplate.parents('li:eq(0)');
                var $elementTopic = $elementSubtopic.parents('li:eq(0)');
                
                var topic = model.Topics()[$elementTopic.index() - 1]; // -1 to account for blank at the top
                var subtopic = topic.Items()[$elementSubtopic.index()];
                var template = subtopic.Templates()[$elementTemplate.index()];

                model.ReplyMessage(template.Content());
            }

            model.LastContent(ko.unwrap(model.ReplyMessage()));
            model.LastTemplate(index);

            common.ResetUnobtrusiveValidation($scope.find('#frmReply'));

            //Clear out custom template in case we were showing one
            model.TemplateBeingShown(null);
        }

        selectTemplateWithPrompt(action);
    };

    var selectCustomTemplate = function (templateIndex) {
        var action = function () {
            model.ReplyMessage(null);
            
            if (templateIndex >= 0 && templateIndex < model.CustomTemplates().length) {
                var template = model.CustomTemplates()[templateIndex];
                model.ReplyMessage(template.Content());

                model.TemplateBeingShown(template);
            }
            
            model.LastContent(ko.unwrap(model.ReplyMessage()));
            model.LastTemplate(templateIndex);

            common.ResetUnobtrusiveValidation($scope.find('#frmReply'));
        }
        
        selectTemplateWithPrompt(action);
    };

    var performTemplateSave = function(saveAsGlobal) {
        var templateToSave = {
            Title: ko.unwrap(model.CustomTemplateName),
            Content: ko.unwrap(model.ReplyMessage),
            SupportTemplateId: model.TemplateBeingShown() !== null ? ko.unwrap(model.TemplateBeingShown().SupportTemplateId) : null
        };

        $.post('/Support/SaveSupportTemplate', { template: templateToSave, globalTemplate: saveAsGlobal }, function () {
            //Reload all custom templates
            $.post('/Support/SupportTemplates', { guarantorVisitPayUserId: internal.visitPayUserId }, function (data) {
                model.CustomTemplates.Update(data);
                model.FinishEditingTemplate();
                bindTree(); //Setup tree again
            });
        });
    }

    var saveTemplate = function (savingAsGlobal) {
        var promptMessage = null;
        if (model.ShowingCustomTemplate()) {
            //We are saving an existing template, so check if we are changing template type
            var templateUserId = ko.unwrap(model.TemplateBeingShown().VisitPayUserId);
            if (templateUserId && savingAsGlobal) {
                //Saving non-global template as global
                promptMessage = 'This template is not currently a global template. Are you sure you want to make it a global template?';
            } else if (!templateUserId && !savingAsGlobal) {
                //Saving global template as non-global
                promptMessage = 'This template is currently a global template. Are you sure you want to make it a private template?';
            }
        }

        if (promptMessage) {
            //Need to prompt before saving
            var promise = common.ModalGenericConfirmation('Confirm Save', promptMessage);
            promise.done(function () {
                performTemplateSave(savingAsGlobal);
            });
        } else {
            //No need to prompt
            performTemplateSave(savingAsGlobal);
        }
    }

    var deleteTemplate = function() {
        if (model.TemplateBeingShown() !== null) {
            //Get ID of the template to delete
            var templateId = ko.unwrap(model.TemplateBeingShown().SupportTemplateId);

            //Load the template from the server so we get it with the original tags
            $.post('/Support/RemoveSupportTemplate', { supportTemplateId: templateId }, function () {
                //Reload all custom templates
                $.post('/Support/SupportTemplates', { guarantorVisitPayUserId: internal.visitPayUserId }, function (data) {
                    model.CustomTemplates.Update(data);
                    model.FinishEditingTemplate();

                    /*
                     IMPORTANT: Toggle CreatingCustomTemplate so that the jsTree section of the page gets rebuilt with the KO bindings...otherwise
                     the custom template section of the tree won't get updated with the new indexes from updating the template list. We have to do
                     this because jsTree overwrites all the KO bindings when it initializes the tree, preventing things from updating automatically.
                     */
                    model.CreatingCustomTemplate(true);
                    model.CreatingCustomTemplate(false);
                    bindTree(); //Setup tree again
                });
            });
        }
    }

    var saveComplete = function () {
        $scope.modal('hide');
        $.unblockUI();
        $(document).trigger('supportRequestReplyCompleted');
    }
    var saveAttachments = function (supportRequestMessageId) {

        var $form = $scope.find('#frmReplyAttachments');

        if ($form.find('tbody.files > tr').length > 0) {

            $form.fileupload('option', {
                formData: {
                    SupportRequestId: internal.supportRequestId,
                    SupportRequestMessageId: supportRequestMessageId,
                    VisitPayUserId: internal.visitPayUserId,
                    __RequestVerificationToken: $('#__AjaxAntiForgeryForm input[name=__RequestVerificationToken]').val()
                }
            });

            $form.find('button.start').click();

        } else {

            saveComplete();
        }

    };

    //
    var actions = {};
    actions.bindFileUploader = function () {

        var $form = $('#frmReplyAttachments');
        var $error = $form.find('.error');
        if ($error.length === 0)
            $error = $('.primary-buttons').find('.error');

        var fileCount = 0;

        $form.find('.files').empty();

        $form.fileupload({
            acceptFileTypes: /(\.|\/)(doc|docx|gif|jpe?g|pdf|png|ppt|pptx|txt|xls|xlsx|zip)$/i,
            autoUpload: false,
            dataType: 'json',
            maxFileSize: 5000000, //5mb in bytes
            url: '/Support/UploadSupportRequestMessageAttachment',
            processfail: function (e, data) {
                var error = data.files[data.index].error;
                var errorMessage = error;
                if (error.indexOf('too large') > -1)
                    errorMessage = 'We cannot accept files over 5MB.';
                else if (error.indexOf('not allowed') > -1)
                    errorMessage = 'This is an unsupported file type.';

                $error.text(errorMessage);
            }
        }).off('fileuploadchange').on('fileuploadchange', function () {

            $error.text('');

        }).off('fileuploadadded').on('fileuploadadded', function () {

            fileCount++;
            actions.showUpload(fileCount);

            $form.find('button.cancel').off('click').on('click', function () {
                fileCount--;
                $error.text('');
                actions.showUpload(fileCount);
            });

        }).off('fileuploadalways').on('fileuploadalways', function (e) {
            fileCount--;
            if (fileCount <= 0)
                saveComplete();
        });

    };
    actions.sendReply = function () {

        var $form = $scope.find('#frmReply');

        if ($form.valid()) {
            $.blockUI();

            $.post('/Support/SaveSupportRequestReply', {
                supportRequestId: internal.supportRequestId,
                visitPayUserId: internal.visitPayUserId,
                messageBody: getTextArea().val()
            }, function(result) {
                if (result.Result) {
                    saveAttachments(result.Message);
                }
            }, 'json');
        }

    }
    actions.showUpload = function (fileCount) {

        var $form = $scope.find('#frmReplyAttachments');
        var $topics = $scope.find('.topics');

        if (fileCount > 0 && !$form.is(':visible')) {
            $topics.animate({ height: '378px' }, 150);
            $form.css('height', '0px').show().animate({ height: '119px' }, 150);
        } else if (fileCount === 0 && $form.is(':visible')) {
            $topics.animate({ height: '512px' }, 150);
            $form.animate({ height: '0' }, 150, function () {
                $(this).hide();
            });
        }

    };

    //
    var initialize = function (modal, data, supportTemplateData, templateReplacementTags) {

        //
        $scope = modal;

        //
        model = new supportRequestReplyViewModel();
        ko.mapping.fromJS(data, null, model.Topics);
        model.CustomTemplates.Update(supportTemplateData);
        ko.mapping.fromJS(templateReplacementTags, null, model.TemplateReplacementTags);
        ko.applyBindings(model, $scope[0]);

        //
        var $replyForm = $scope.find('#frmReply');
        if ($replyForm.length > 0) {
            common.ResetUnobtrusiveValidation($replyForm);    
        }
        bindTree();
        actions.bindFileUploader();

        //
        $scope.on('click', '.topics > ul:eq(0) > li:eq(0) > a', function () {
            selectTemplate($(this));
        });
        $scope.on('click', '.topics > ul ul ul a', function () {
            selectTemplate($(this));
        });
        //Custom templates jstree click handler
        $scope.on('click', '.topics > ul li ul a', function () {
            var templateIndex = $(this).find('.template-title').data('customTemplateIndex');
            if (templateIndex !== undefined) {
                selectCustomTemplate(templateIndex);
            }
        });

        $scope.on('click', '#btnSendReply', actions.sendReply);
        $scope.on('click', '#btnReplyAttach', function() {
            $('#frmReplyAttachments').find('input[type=file]').click();
        });
        $scope.on('click', '#btnAddReply', function() {
            var replyMsg = $('#frmReply').find('textarea.message').val();
            $('#frmReplyToGuarantor').find('textarea.message').val(replyMsg);
            $('#btnReplyGuarantor').attr('disabled', false);
            $('#btnSaveDraft').attr('disabled', false);
            var scrollHeight = $('#frmReplyToGuarantor').find('textarea#MessageBody.form-control.message')[0].scrollHeight;
            $('#frmReplyToGuarantor').find('textarea.message').css('height', scrollHeight);
            $scope.modal('hide');
        });
        
        $scope.on('click', '#btnReplyDiscard', function () {

            var promise = common.ModalGenericConfirmation('Confirm Discard', 'Are you sure you want to discard this message?');
            promise.done(function () {
                $scope.modal('hide');
            });

        });

        $scope.on('click', '#btnCreateTemplate', function () {
            model.StartEditingTemplate(true);
        });

        $scope.on('click', '#btnDiscardTemplate', function () {
            var promise = common.ModalGenericConfirmation('Confirm Discard', 'Are you sure you want to discard the changes to this template?');
            promise.done(function () {
                model.FinishEditingTemplate();
                bindTree(); //Setup tree again
            });
        });

        $scope.on('click', '#btnSaveTemplate', function () {
            saveTemplate(false);
        });

        $scope.on('click', '#btnSaveGlobalTemplate', function () {
            saveTemplate(true);
        });

        $scope.on('click', '#btnDeleteTemplate', function () {
            var promise = common.ModalGenericConfirmation('Confirm Delete', 'Are you sure you want to delete this template?');
            promise.done(function () {
                deleteTemplate();
            });
        });

        $scope.on('blur', '#MessageBody', function () {
            //If we are on a custom template, save the last place we were focused so we can add tags there if needed
            if (model.CreatingCustomTemplate()) {
                lastFocus = this;
            }
        });
        //
        $scope.modal('show');
        $.unblockUI();
    };

    //
    var reply = {};
    reply.Initialize = function(supportRequestId, visitPayUserId) {

        internal.supportRequestId = supportRequestId;
        internal.visitPayUserId = visitPayUserId;

        var promiseModal = common.ModalNoContainerAsync('modalSupportRequestReply', '/Support/SupportRequestReply', 'GET', {}, false);
        var promiseData = $.Deferred();
        var promiseTemplateData = $.Deferred();
        var promiseTemplateReplacementTags = $.Deferred();

        $.post('/Support/Topics', { visitPayUserId: visitPayUserId }, function (data) {
            promiseData.resolve(data);
        });

        $.post('/Support/SupportTemplates', { guarantorVisitPayUserId: visitPayUserId }, function (data) {
            promiseTemplateData.resolve(data);
        });

        $.post('/Support/SupportTemplateReplacementTags', {}, function (data) {
            promiseTemplateReplacementTags.resolve(data);
        });
        
        $.when(promiseModal, promiseData, promiseTemplateData, promiseTemplateReplacementTags).done(initialize);

    };

    reply.InitializeReplyToGuarantor = function(supportRequestId, visitPayUserId, message) {
        internal.supportRequestId = supportRequestId;
        internal.visitPayUserId = visitPayUserId;

        var promiseModal = common.ModalNoContainerAsync('modalSupportRequestReplyToGuarantor', '/Support/SupportRequestReplyToGuarantor', 'GET', {}, false);
        var promiseReply = $.Deferred();

        $.when(promiseModal).done(function(modal) {
            initialize(modal);
            model.ReplyMessage(message);
            $scope.on('click', '#btnSendReply', function(){ promiseReply.resolve(); });
            $scope.off('click', '#btnReplyDiscard').on('click', '#btnReplyDiscard', function(){ promiseReply.reject(); });
            $scope.on('click', 'button.close', function(){ promiseReply.reject(); });
        });

        return promiseReply;

    };

    return reply;

})(window.VisitPay.Common, jQuery, window.ko);
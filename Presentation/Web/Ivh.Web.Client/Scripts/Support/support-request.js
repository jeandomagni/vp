﻿window.VisitPay.SupportRequest = (function(supportRequestAction, supportRequestReply, clientSupport, common, $, ko) {

    //
    var supportRequestViewModel = function() {

        var self = this;

        self.AssignedVisitPayUserId = ko.observable();
        self.ClosedBy = ko.observable();
        self.ClosedDate = ko.observable();
        self.CreatedDate = ko.observable();
        self.IsPreviousConsolidation = ko.observable();
        self.GuarantorFullName = ko.observable();
        self.PatientFullName = ko.observable();
        self.SubmittedFor = ko.observable();
        self.SupportRequestId = ko.observable();
        self.SupportRequestDisplayId = ko.observable();
        self.SupportRequestStatusId = ko.observable();
        self.SupportRequestStatusId.IsOpen = ko.computed(function() {
            return ko.unwrap(self.SupportRequestStatusId) === 1;
        });
        self.SupportRequestStatusId.IsClosed = ko.computed(function () {
            return ko.unwrap(self.SupportRequestStatusId) === 2;
        });
        self.SupportRequestStatusName = ko.observable();
        self.TreatmentDate = ko.observable();
        self.TreatmentLocation = ko.observable();
        self.TreatmentLocation.Any = ko.computed(function () {
            return ko.unwrap(self.TreatmentLocation) && ko.unwrap(self.TreatmentLocation).length > 0;
        });
        self.VisitSourceSystemKeyDisplay = ko.observable();
        self.VisitMatchedSourceSystemKey = ko.observable();
        self.IsVisitRemoved = ko.observable();
        self.IsFlaggedForFollowUp = ko.observable();
        self.IsFlaggedForFollowUp.IsEnabled = ko.computed(function () {
            return ko.unwrap(self.IsFlaggedForFollowUp) === true;
        });

        self.CommunicationWindow = {
            IsExpanded: ko.observable(true)
        };
        self.CommunicationWindow.ToggleExpanded = function () {
            self.CommunicationWindow.IsExpanded(!ko.unwrap(self.CommunicationWindow.IsExpanded));
            var action = ko.unwrap(self.CommunicationWindow.IsExpanded) ? 'Show' : 'Hide';
            window._paq.push(['trackEvent', 'SupportRequest', 'Click', 'CommunicationWindow - ' + action]);
        }
        self.ReplyMessage = ko.observable();
        self.ReplyMessage.IsEmpty = ko.computed(function() {
            var contents = self.ReplyMessage();
            if (contents) {
                return false;
            }
            return true;
        });
        self.InternalNoteWindow = {
            IsExpanded: ko.observable(false)
        };
        self.InternalNoteWindow.ToggleExpanded = function () {
            self.InternalNoteWindow.IsExpanded(!ko.unwrap(self.InternalNoteWindow.IsExpanded));
            var action = ko.unwrap(self.InternalNoteWindow.IsExpanded) ? 'Show' : 'Hide';
            window._paq.push(['trackEvent', 'SupportRequest', 'Click', 'InternalNote - ' + action]);
        }
        self.InternalNote = ko.observable();
        self.InternalNote.IsEmpty = ko.computed(function() {
            var contents = self.InternalNote();
            if (contents) {
                return false;
            }
            return true;
        });

        self.GuarantorMessages = ko.observableArray([]);
        self.GuarantorMessages.SortOrder = ko.observable(1);
        self.GuarantorMessages.Sorted = ko.computed(function() {
            return self.GuarantorMessages().sort(function (l, r) {
                if (self.GuarantorMessages.SortOrder() === 0)
                    return l.SupportRequestMessageId() === r.SupportRequestMessageId() ? 0 : (l.SupportRequestMessageId() < r.SupportRequestMessageId() ? -1 : 1);
                else
                    return l.SupportRequestMessageId() === r.SupportRequestMessageId() ? 0 : (l.SupportRequestMessageId() < r.SupportRequestMessageId() ? 1 : -1);
            });
        });
        self.GuarantorMessages.ToggleSortOrder = function () {
            self.GuarantorMessages.SortOrder() === 0 ? self.GuarantorMessages.SortOrder(1) : self.GuarantorMessages.SortOrder(0);
        };
        
        self.InternalMessages = ko.observableArray([]);
        self.InternalMessages.SortOrder = ko.observable(1);
        self.InternalMessages.Sorted = ko.computed(function () {
            return self.InternalMessages().sort(function (l, r) {
                if (self.InternalMessages.SortOrder() === 0)
                    return l.SupportRequestMessageId() === r.SupportRequestMessageId() ? 0 : (l.SupportRequestMessageId() < r.SupportRequestMessageId() ? -1 : 1);
                else
                    return l.SupportRequestMessageId() === r.SupportRequestMessageId() ? 0 : (l.SupportRequestMessageId() < r.SupportRequestMessageId() ? 1 : -1);
            });
        });
        self.InternalMessages.ToggleSortOrder = function () {
            self.InternalMessages.SortOrder() === 0 ? self.InternalMessages.SortOrder(1) : self.InternalMessages.SortOrder(0);
        };

        self.SupportAdminUsers = ko.observableArray([]);

        self.Attachments = ko.observableArray([]);
        self.Attachments.Count = ko.computed(function () {
            return self.Attachments().length;
        });

        self.SupportTopicId = ko.observable();
        self.SupportTopicId.Data = ko.observableArray([]);
        self.SupportTopicId.IsEnabled = ko.computed(function () {
            return ko.unwrap(self.SupportTopicId.Data).length > 0;
        });

        self.SupportSubTopicId = ko.observable();
        self.SupportSubTopicId.Data = ko.observableArray([]);
        self.SupportSubTopicId.IsEnabled = ko.computed(function () {
            return ko.unwrap(self.SupportSubTopicId.Data).length > 0;
        });

        self.SubmittedForVisitPayUserId = ko.observable();
        self.SubmittedForVpGuarantorId = ko.observable();
        self.SubmittedForVpGuarantorId.Any = ko.computed(function() {
            var id = ko.unwrap(self.SubmittedForVpGuarantorId);
            return id !== undefined && id !== null;
        });
        self.FromVpGuarantorId = ko.observable();
        self.FromVpGuarantorId.Any = ko.computed(function() {
            var id = ko.unwrap(self.FromVpGuarantorId);
            return id !== undefined && id !== null;
        });

        self.VisitId = ko.observable();
        self.IsSavingDraft = ko.observable();
    };

    //
    var model;
    var $scope;
    var $timeoutId;

    //
    var supportRequest = {};
    supportRequest.ModalIsOpen = false;

    //
    var internal = {};
    var actions = {};

    //
    var bindLessMore = function () {
        var messageHeight = 74;
        var allMessagesHeight = 0;
        $scope.find('.messages .body').each(function () {
            var height = $(this).find('p:eq(0)').height();
            allMessagesHeight += messageHeight;
            if (height > 36) {
                $(this).find('.more').show();
            }
        });
        var clickableHeight = $('#messagesDiv').height() - allMessagesHeight;
        if (clickableHeight < messageHeight) {
            clickableHeight = messageHeight;
        }
        $('#MessageBody').css('height', clickableHeight);
        actions.refreshReplyMessage();
    };
    var loadData = function () {

        var promiseData = $.Deferred();
        $.post('/Support/SupportRequest', { supportRequestId: internal.supportRequestId, visitPayUserId: internal.visitPayUserId }, function (data) {
            promiseData.resolve(data);
        }, 'json');

        return promiseData;

    };
    var loadTopics = function (supportRequestId) {

        var deferred = $.Deferred();

        $.get('/Support/SupportTopics?supportRequestId=' + supportRequestId, {}, function (data) {

            deferred.resolve(data.SupportTopics);

        });

        return deferred;

    };
    var downloadAttachment = function (supportRequestMessageAttachmentId) {

        supportRequestMessageAttachmentId = supportRequestMessageAttachmentId || null;

        $.post('/Support/PrepareDownload', {
            supportRequestId: internal.supportRequestId,
            supportRequestMessageAttachmentId: supportRequestMessageAttachmentId,
            visitPayUserId: internal.visitPayUserId
        }, function (result) {
            if (result.Result === true) {
                window.location.href = result.Message;
            } else {
                VisitPay.Common.ModalGenericAlert("Unable to Download", result.Message, "Ok");
            }
        }, 'json');

    };

    var deleteAttachment = function(supportRequestMessageAttachmentId, supportRequestMessageAttachmentName, $supportAttachmentsModal) {

        $supportAttachmentsModal.modal('hide');

        var promise = common.ModalGenericConfirmation('Confirm Delete', 'Are you sure you want to delete ' + supportRequestMessageAttachmentName + '?', 'Yes', 'No','You action will be logged for auditing purposes');

        promise.done(function () {
            
            $.post('/Support/DeleteAttachment', {
                supportRequestId: internal.supportRequestId,
                supportRequestMessageAttachmentId: supportRequestMessageAttachmentId,
                visitPayUserId: internal.visitPayUserId,
                vpGuarantorId: internal.VpGuarantorId
            }, function() {
                loadData().done(function (data) {
                    ko.mapping.fromJS(data, null, model);
                    bindLessMore();
                    $.unblockUI();
                    $(document).trigger('supportRequestChanged');
                });
            }, 'json');
        });

        promise.fail(function() {
            $.unblockUI();
        });

    };

    var openVisitDetails = function (e) {
        e.preventDefault();
        $.blockUI();

        $.get('/Search/GuarantorVisitTransactions/' + ko.unwrap(model.VisitId) + '?visitPayUserId=' + ko.unwrap(model.SubmittedForVisitPayUserId), {}, function(html) {
            if ($('#modelVisitTransactions').length === 0) {
                $('body').append('<div id="modalVisitTransactionsContainer"></div>');
            }
            $('#modalVisitTransactionsContainer').html(html);
            $('#modalVisitTransactionsContainer').find('.modal').modal();
            window.VisitPay.VisitTransactions.Initialize(ko.unwrap(model.SubmittedForVisitPayUserId));
        });
    }
    var saveComplete = function () {

        loadData().done(function (data) {
            ko.mapping.fromJS(data, null, model);
            actions.showInternalEditor();
            bindLessMore();
            $.unblockUI();
            $(document).trigger('supportRequestChanged');
        });

    };
    var saveAttachments = function (supportRequestMessageId) {

        var $form = $scope.find('#frmEditorInternalAttachments');

        if ($form.find('tbody.files > tr').length > 0) {
            $form.fileupload('option', {
                formData: {
                    SupportRequestId: internal.supportRequestId,
                    SupportRequestMessageId: supportRequestMessageId,
                    VisitPayUserId: internal.visitPayUserId,
                    __RequestVerificationToken: $('#__AjaxAntiForgeryForm input[name=__RequestVerificationToken]').val()
                }
            });

            $form.find('button.start').click();
        } else {
            saveComplete();
        }

    };
    var saveInternalNote = function (messageBody) {
        $.post('/Support/SaveInternalNote', { model: { SupportRequestId: internal.supportRequestId, MessageBody: messageBody }, VisitPayUserId: internal.visitPayUserId }, function (result) {

            if (result.Result) {
                saveAttachments(result.Message);
            }

        }, 'json');
    };
    var saveDetails = function () {
        $.blockUI();
        $scope.off('change.assigned', '#ddAssignedVisitPayUser');
        $.post('/Support/SaveDetails', { supportRequestId: internal.supportRequestId, visitPayUserId: internal.visitPayUserId, assignedToVisitPayUserId: $(this).val() }, function() {
            loadData().done(function(data) {
                ko.mapping.fromJS(data, null, model);
                bindLessMore();
                $.unblockUI();
                $(document).trigger('supportRequestChanged');
                $scope.on('change.assigned', '#ddAssignedVisitPayUser', saveDetails);
            });
        });
    };
    var setSubTopics = function (supportTopicId) {

        var items = [];
        ko.utils.arrayFirst(ko.unwrap(model.SupportTopicId.Data), function(topic) {

            if (ko.unwrap(topic.SupportTopicId) === supportTopicId)
                items = ko.unwrap(topic.Items);

        });

        model.SupportSubTopicId.Data(items);

    };
    var setSupportTopic = function(e) {
        e.preventDefault();
        var $form = $scope.find('#frmSubmitTopic');
        $form.parseValidation();
        if (!$form.valid()) {
            return;
        }
        
        $.blockUI();
        $.post('/Support/SetSupportTopic', { supportRequestId: ko.unwrap(model.SupportRequestId), supportTopicId: ko.unwrap(model.SupportSubTopicId), visitPayUserId: internal.visitPayUserId }, function() {
            $.unblockUI();
        });
    };
    var getTextArea = function() {
        return $scope.find('textarea.message');
    };

    var savingDraft = function() {
        if (internal.IsSavingDraft) return;
        $('#saveDraftStatus').html('<span></span>');
        internal.IsSavingDraft = true;
        $timeoutId = setTimeout(function() {
            actions.saveDraft();
        }, 15000);
    }

    var loadSupportRequestUI = function (element, url, method, data) {

        var promise = $.Deferred();

        $.ajax({
            method: method,
            url: url,
            data: data
        }).done(function (html) {
            element.html(html);
            
            promise.resolve(element);
        });

        return promise;

    };

    var getTitle = function () {
        var title = 'Case ' + ko.unwrap(model.SupportRequestDisplayId()) + ' ';
        if (model.SupportRequestStatusId.IsOpen()) {
            //Opened on
            title += 'Opened on ' + ko.unwrap(model.CreatedDate());
        } else {
            //Closed on
            title += 'Closed on ' + ko.unwrap(model.ClosedDate()) + ' by ' + ko.unwrap(model.ClosedBy());
        }

        return title;
    }

    //
    internal.supportRequestId = null;
    internal.visitPayUserId = null;

    //
    actions.bindFileUploader = function () {

        var $form = $('#frmEditorInternalAttachments');
        var $error = $form.find('.error');
        if ($error.length === 0)
            $error = $('.editor-internal-buttons').find('.error');

        var fileCount = 0;

        $form.find('.files').empty();

        $form.fileupload({
            acceptFileTypes: /(\.|\/)(doc|docx|gif|jpe?g|pdf|png|ppt|pptx|txt|xls|xlsx|zip)$/i,
            autoUpload: false,
            dataType: 'json',
            maxFileSize: 5000000, //5mb in bytes
            url: '/Support/UploadSupportRequestMessageAttachment',
            processfail: function (e, data) {
                var error = data.files[data.index].error;
                var errorMessage = error;
                if (error.indexOf('too large') > -1)
                    errorMessage = 'We cannot accept files over 5MB.';
                else if (error.indexOf('not allowed') > -1)
                    errorMessage = 'This is an unsupported file type.';

                $error.text(errorMessage);
            }
        }).off('fileuploadchange').on('fileuploadchange', function () {

            $error.text('');

        }).off('fileuploadadded').on('fileuploadadded', function () {

            fileCount++;
            actions.showInternalUpload(fileCount);

            $form.find('button.cancel').off('click').on('click', function () {
                fileCount--;
                $error.text('');
                actions.showInternalUpload(fileCount);
            });

        }).off('fileuploadalways').on('fileuploadalways', function (e) {
            fileCount--;
            if (fileCount <= 0)
                saveComplete();
        });

    };
    actions.createSupportTicket = function () {
        $.blockUI();
        var clientSupportRequestCreate = new clientSupport.ClientSupportRequestCreate();
        clientSupportRequestCreate.show({ unblockOnSubmit: false }, ko.mapping.toJS(model)).done(function () {
            loadData().done(function (data) {
                ko.mapping.fromJS(data, { 'ignore': ['supportTopicId', 'AssignedVisitPayUserId'] }, model);
                $.unblockUI();
            });
        }).fail(function () {
            $.unblockUI();
        });
    };
    actions.printRequest = function () {
        $.blockUI();
        $.post('/Support/PreparePrintSupportRequest', {
            supportRequestId: internal.supportRequestId,
            visitPayUserId: internal.visitPayUserId,
            sortOrderGuarantor: model.GuarantorMessages.SortOrder() === 1 ? 'desc' : 'asc',
            sortOrderInternal: model.InternalMessages.SortOrder() === 1 ? 'desc' : 'asc'
        }, function (url) {
            $.unblockUI();
            window.open(url);
        }, 'json');
    };
    actions.refreshSupportRequest = function () {
        if (supportRequest.ModalIsOpen) {
            $.blockUI();

            loadData().done(function (data) {
                $.unblockUI();
                ko.mapping.fromJS(data, null, model);
                bindLessMore();

                //Update title
                $('#supportRequestDialog').dialog("option", "title", getTitle());
            });
        }
    };
    actions.flagForFollowUpSupportTicket = function () {
        var flagForFollowUp = !(ko.unwrap(model.IsFlaggedForFollowUp));
        var options = {
            type: 'POST',
            url: '/Support/FlagForFollowUp',
            data: {
                supportRequestId: internal.supportRequestId,
                visitPayUserId: internal.visitPayUserId,
                flagForFollowUp: flagForFollowUp
            },
            success: function (response) {
                $(document).trigger('supportRequestChanged');
                actions.refreshSupportRequest();
            }
        };
        $.ajax(options);
    }

    actions.refreshReplyMessage = function () {
        var scrollHeight = $('#frmReplyToGuarantor').find('textarea#MessageBody.form-control.message')[0].scrollHeight;
        $('#frmReplyToGuarantor').find('textarea.message').css('height', scrollHeight);
    };
    actions.reply = function () {

        var $form = $scope.find('#frmReplyToGuarantor');
        $('#saveDraftStatus').html('<span></span>');

        if ($form.valid()) {
            $.blockUI();
            var promiseReply = supportRequestReply.InitializeReplyToGuarantor(internal.supportRequestId, internal.visitPayUserId, getTextArea().val());
            promiseReply.done(function() {
                model.ReplyMessage('');
                getTextArea().val('');
            });
        }

    }
    actions.selectTemplate = function () {
        $.blockUI();
        $('#saveDraftStatus').html('<span></span>');

        supportRequestReply.Initialize(internal.supportRequestId, internal.visitPayUserId);
    };
    actions.showAccountDetails = function () {
        window.location.href = '/Search/GuarantorAccount/' + internal.VpGuarantorId;
    };
    actions.showAttachments = function (supportRequestMessageId) {
        supportRequestMessageId = supportRequestMessageId || null;

        $.blockUI();

        var promise = common.ModalNoContainerAsync('modalSupportAttachments', '/Support/Attachments', 'POST', {
            supportRequestId: internal.supportRequestId,
            supportRequestMessageId: supportRequestMessageId,
            visitPayUserId: internal.visitPayUserId
        }, false);

        promise.done(function ($modal) {

            $modal.find('.icon-file-type').each(function () {

                $(this).addClass(getFileType($(this).prev('input[type=hidden]').val()));

            });

            $modal.find('a').click(function (e) {

                e.preventDefault();
                downloadAttachment($(this).prev('input[type=hidden].srma-id').val());

            });

            $modal.find('button.srma-delete').click(function (e) {

                e.preventDefault();
                var $container = $(this).parent().parent('.srma-container');
                deleteAttachment($container.find('input[type=hidden].srma-id').val(), $container.find('.srma-download').text(), $modal);

            });

            $modal.find('#btnDownloadAttachmentsAll').click(function (e) {

                e.preventDefault();
                downloadAttachment();

            });

            $modal.modal('show');

            $.unblockUI();
        });
    }
    actions.showRequestAction = function () {
        supportRequest.ModalIsOpen = true;

        var promise = supportRequestAction.Initialize($('#modalSupportRequestActionContainer'), model.SupportRequestStatusId, model.SupportRequestId, model.SupportRequestDisplayId, internal.visitPayUserId);

        promise.done(function ($modal) {
            var dismiss = $modal.find('[data-dismiss="modal"]');
            dismiss.on('click', function () {
                dismiss.off('click');
            });
            $modal.on('hidden.bs.modal', function () {
                dismiss.off('click');
            });
        });

    };
    actions.showInternalEditor = function () {

        //
        var $modalBody = $scope.find('.modal-body');
        var $top = $scope.find('.top');
        var $editorInternal = $scope.find('.editor-internal');
        var $form = $editorInternal.find('#frmEditorInternal');
        var $textarea = $editorInternal.find('textarea');

        //
        actions.showInternalUpload(0);
        actions.bindFileUploader();

        //
        var isVisible = $editorInternal.is(':visible');

        //
        var setHeightAuto = function () {
            $modalBody.css('height', 'auto');
        };
        var setState = function (editorVisible) {

            var $buttonReply = $scope.find('#btnReplyGuarantor');
            var $buttonSelectTemplate = $scope.find('#btnSelectTemplate');
            var $buttonInternal = $scope.find('#btnAddInternalNote');
            var $buttonsPrimary = $scope.find('.modal-footer .primary-buttons');
            var $buttonsInternal = $scope.find('.modal-footer .editor-internal-buttons');
            var $saveDraft = $scope.find('#btnSaveDraft');

            if (editorVisible) {

                $textarea.focus();
                $buttonReply.prop('disabled', true);
                $buttonInternal.prop('disabled', true);

                $buttonsPrimary.hide();
                $buttonsInternal.fadeIn(500);


            } else {

                $buttonReply.prop('disabled', false);
                $buttonInternal.prop('disabled', false);

                $buttonsInternal.hide();
                $buttonsPrimary.fadeIn(500);
            }

        };

    };
    actions.saveInternalNote = function () {

        var $form = $scope.find('#frmEditorInternal');
        var $textarea = $form.find('textarea');

        if ($form.valid()) {
            $.blockUI();
            saveInternalNote($textarea.val());
            model.InternalNote('');
        }
    };
    actions.showInternalUpload = function(fileCount) {

        var $form = $('#frmEditorInternalAttachments');

        if (fileCount > 0 && !$form.is(':visible')) {
            $('#frmEditorInternal').animate({ width: '560px' }, 150);
            $('#frmEditorInternalAttachments').show().animate({ width: '293px' }, 150);
        } else if (fileCount === 0 && $form.is(':visible')) {
            $('#frmEditorInternal').animate({ width: '867px' }, 150);
            $('#frmEditorInternalAttachments').animate({ width: '0' }, 150, function () {
                $(this).hide();
                $('#frmEditorInternal').find('textarea').focus();
            });
        }

    };
    actions.saveDraft = function () {
        var options = {
            type: 'POST',
            url: '/Support/SaveDraft',
            data: {
                supportRequestId: internal.supportRequestId,
                visitPayUserId: internal.visitPayUserId,
                messageBody: getTextArea().val()
            },
            beforeSend: function(xhr) {
                $('#saveDraftStatus').html('<span class="text-italic">Saving...</span>');
            },
            success: function(response) {
                internal.IsSavingDraft = false;
                $('#saveDraftStatus').html('<span>Saved</span>');
            }
        };
        $.ajax(options);
    }
    actions.closeSRModal = function () {    
        clearTimeout($timeoutId);
        actions.saveDraft();
        dialogClosed();
    }

    var dialogClosed = function () {
        supportRequest.ModalIsOpen = false;
        $(document).trigger('supportRequestChanged');
        $(document).trigger('supportRequestAssignedUserChanged');
    }

    var dialogHidden = function() {
        supportRequest.ModalIsOpen = false;
        actions.saveDraft();
    }

    var dialogRestored = function() {
        supportRequest.ModalIsOpen = true;
    }

    var initializeData = function (data, topics) {
        model = new supportRequestViewModel();
        ko.mapping.fromJS(topics, null, model.SupportTopicId.Data);
        ko.mapping.fromJS(data, null, model);
        model.ReplyMessage(data.DraftMessage);

        setSubTopics(ko.unwrap(model.SupportTopicId));
        model.SupportSubTopicId(ko.unwrap(model.SupportSubTopicId));

        model.SupportTopicId.subscribe(setSubTopics);
    };

    //
    var initializeDialog = function ($dialog) {
        $scope = $('#supportRequest');

        //
        ko.cleanNode($scope[0]); //Clean before applying bindings
        ko.applyBindings(model, $scope[0]);
        
        //
        $.unblockUI();

        //Open the dialog
        $dialog.dialog('open');
        
        $scope.on('change', '#ddSupportRequestAction', function (e) {

            e.preventDefault();

            switch (parseInt($(this).val())) {
                case 1:
                    actions.printRequest();
                    break;
                case 2:
                    actions.showAccountDetails();
                    break;
                case 3:
                    actions.showRequestAction();
                    break;
                case 4:
                    actions.createSupportTicket();
                    break;
                case 5:
                    actions.flagForFollowUpSupportTicket();
                    break;
            }

            $(this).val('');

        });
        $scope.on('change.assigned', '#ddAssignedVisitPayUser', saveDetails);

        $scope.on('click', '.messages .title a', function (e) {

            e.preventDefault();
            var context = ko.contextFor(this);
            actions.showAttachments(context.$data.SupportRequestMessageId());

        });

        $scope.on('click', '.messages .body .more a', function(e) {

            e.preventDefault();
            $(this).parent().hide();
            $(this).parents('.body').find('.less').show();
            $(this).parents('.body').css('height', 'auto');

        });

        $scope.on('click', '.messages .body .less a', function(e) {

            e.preventDefault();
            $(this).parent().hide();
            $(this).parents('.body').css('height', '36px');
            $(this).parents('.body').find('.more').show();

        });

        if (internal.accountClosed) {
            $scope.find('#btnAddInternalNote').prop('disabled', true);
            $scope.find('#btnSaveInternalNote').prop('disabled', true);
            $scope.find('#btnInternalEditorAttach').prop('disabled', true);
            $scope.find('#btnInternalEditorDiscard').prop('disabled', true);
            $scope.find('#btnReplyGuarantor').prop('disabled', true);
            $scope.find('#btnSelectTemplate').prop('disabled', true);
            $scope.find('#btnSupportRequestDoAction').prop('disabled', true);
            $scope.find('#ddSupportRequestAction option[value=3]').remove();
            $scope.find('#btnSaveDraft').prop('disabled', true);
        } else {
            $scope.on('click', '#btnSupportRequestDoAction', actions.showRequestAction);
            $scope.on('click', '#btnAddInternalNote', actions.showInternalEditor);
            $scope.on('click', '#btnSaveInternalNote', actions.saveInternalNote);
            $scope.on('click', '#btnInternalEditorAttach', function () {
                $('#frmEditorInternalAttachments').find('input[type=file]').click();
            });
            $scope.on('click', '#btnInternalEditorDiscard', function () {

                var promise = common.ModalGenericConfirmation('Confirm Discard', 'Are you sure you want to discard this note?');
                promise.done(function () {
                    actions.showInternalEditor();
                });

            });

            $scope.on('click', '#btnReplyGuarantor', actions.reply);
            $scope.on('click', '#btnSelectTemplate', actions.selectTemplate);
            $scope.on('change', '#ddSupportSubTopic', setSupportTopic);
        }

        $scope.on('click', '#btnSupportRequestAttachments', function() {

            actions.showAttachments();

        });

        $scope.find('[data-toggle="tooltip"]').tooltip({ animation: false, container: 'body' });

        $scope.on('submit', '#frmSubmitTopic', setSupportTopic);
        $scope.on('click', '#aOpenVisitDetails', openVisitDetails);
        $scope.on('paste', '#frmReplyToGuarantor', savingDraft);
        $scope.on('keydown', '#frmReplyToGuarantor', savingDraft);
        $.each($('textarea[data-autoresize]'), function() {
            $(this).on('keyup input', function() { $(this).css('height', this.scrollHeight); });
        });

    };
    supportRequest.Initialize = function (supportRequestId, visitPayUserId, vpGuarantorId, accountClosed) {

        $.blockUI();
        
        internal.supportRequestId = supportRequestId;
        internal.visitPayUserId = visitPayUserId;
        internal.VpGuarantorId = vpGuarantorId;
        internal.accountClosed = accountClosed;
        
        var promiseData = loadData(supportRequestId, visitPayUserId);
        var promiseTopics = loadTopics(supportRequestId);
        
        $.when(promiseData, promiseTopics).done(function (data, topics) {
            supportRequest.ModalIsOpen = true;
            $(document).trigger('supportRequestChanged');

            //Read in the data and topics before loading the dialog
            initializeData(data, topics);

            //Load the dialog
            var createDialogPromise = createDialog(supportRequestId, visitPayUserId, vpGuarantorId, accountClosed);
            createDialogPromise.done(function (dialogContainer) {
                initializeDialog(dialogContainer);
            });
        });

    };

    function createDialog(supportRequestId, visitPayUserId, vpGuarantorId, accountClosed) {
        var model = new VisitPay.Dialog.Model();
        model.Height = 714;
        model.MinHeight = 714;
        model.Width = 900;
        model.MinWidth = 900;
        model.Data = {
            SupportRequestId: supportRequestId,
            VisitPayUserId: visitPayUserId,
            VpGuarantorId: vpGuarantorId,
            AccountClosed: accountClosed
        };
        return VisitPay.Dialog.CreateAsync('#supportRequestDialog',
            model,
            getTitle(), //Dialog title
            '/Support/SupportRequest',
            'GET',
            false, //AutoOpen
            bindLessMore, //OnOpen
            actions.closeSRModal, //OnClose
            dialogHidden, //OnMinimize
            dialogRestored); //OnRestore
    }

    //
    $(document).on('supportRequestActionCompleted', actions.refreshSupportRequest);
    $(document).on('supportRequestReplyCompleted', actions.refreshSupportRequest);
    $(document).on('refreshReplyTextMessage', actions.refreshReplyMessage);

    //
    return supportRequest;

})(window.VisitPay.SupportRequestAction, window.VisitPay.SupportRequestReply, window.VisitPay.ClientSupport, window.VisitPay.Common, jQuery, window.ko);
﻿window.VisitPay.CreateSupportRequest = (function (common, $, ko) {

    //
    var viewModel = function () {

        var self = this;

        self.SupportTopicId = ko.observable();
        self.SupportTopicId.Data = ko.observableArray([]);
        self.SupportTopicId.IsEnabled = ko.computed(function () {
            return ko.unwrap(self.SupportTopicId.Data).length > 0;
        });

        self.SupportSubTopicId = ko.observable();
        self.SupportSubTopicId.Data = ko.observableArray([]);
        self.SupportSubTopicId.IsEnabled = ko.computed(function () {
            return ko.unwrap(self.SupportSubTopicId.Data).length > 0;
        });

        self.TreatmentDate = ko.observable();
        self.PatientFirstName = ko.observable();
        self.PatientLastName = ko.observable();
        self.SelectedVisitId = ko.observable();

        self.Users = ko.observableArray([]);
        self.Users.MoreThanOne = ko.computed(function () {
            var users = ko.unwrap(self.Users);
            return users !== undefined && users !== null && users.length > 1;
        });
        self.Users.SelectedVisitPayUserId = ko.observable();
        self.Users.SelectedUser = ko.computed(function () {
            return ko.utils.arrayFirst(self.Users(), function (child) {
                return child.VisitPayUserId() === self.Users.SelectedVisitPayUserId();
            });
        });

        self.AssignedVisitPayUserId = ko.observable();
        self.SupportAdminUsers = ko.observableArray([]);

        return self;
    };

    //
    var createSupportRequest = {};

    //
    var model;
    var visitPayUserId;

    var internal = {};
    internal.submitComplete = function () {

        $.unblockUI();
        createSupportRequest.OnCreated();

    };
    internal.submitForm = function (e) {

        e.preventDefault();

        if ($(this).valid()) {
            $(this).parents('.modal').modal('hide');
            $(this).find('#MessageBody').val($(this).find('#MessageBody').val().replace(/>/g, '').replace(/</g, ''));
            $.blockUI();
            $.post('/Support/CreateSupportRequest', $(this).serialize() + '&visitPayUserId=' + visitPayUserId + '&__RequestVerificationToken=' + $('#__AjaxAntiForgeryForm input[name=__RequestVerificationToken]').val(), function(result) {

                if (result.Result) {
                    internal.uploadFiles(result.Message);
                } else {
                    $.unblockUI();
                }

            });

        } else {
            $.unblockUI();
        }

    };
    internal.uploadFiles = function (data) {

        data.VisitPayUserId = visitPayUserId;
        data.__RequestVerificationToken = $('#__AjaxAntiForgeryForm input[name=__RequestVerificationToken]').val();

        var $form = $('#frmCreateSupportRequestAttachments');

        if ($form.find('tbody.files > tr').length > 0) {

            $form.fileupload('option', {
                formData: data
            });

            $form.find('button.start').click();

        } else {

            internal.submitComplete();

        }

    };
    
    //
    var loadData = function () {

        var deferred = $.Deferred();

        $.post('/Support/SupportRequestData', {
             visitId: $('#section-createsupportrequest').find('#VisitId').val(),
             visitPayUserId: visitPayUserId
        }, function (data) {

            ko.mapping.fromJS(data.Users, null, model.Users);
            model.TreatmentDate(data.TreatmentDate);
            model.PatientFirstName(data.PatientFirstName);
            model.PatientLastName(data.PatientLastName);
            model.SelectedVisitId(data.SelectedVisitId);
            model.Users.SelectedVisitPayUserId(data.SelectedVisitPayUserId);
            model.SupportAdminUsers(data.SupportAdminUsers);
            deferred.resolve();

        });

        return deferred;

    };
    var loadTopics = function () {

        var deferred = $.Deferred();

        $.get('/Support/SupportTopics', {}, function (data) {

            ko.mapping.fromJS(data.SupportTopics, null, model.SupportTopicId.Data);
            deferred.resolve();

        });

        return deferred;

    };
    var setSubTopics = function (supportTopicId) {

        var items = [];
        ko.utils.arrayFirst(ko.unwrap(model.SupportTopicId.Data), function (topic) {

            if (ko.unwrap(topic.SupportTopicId) == supportTopicId)
                items = ko.unwrap(topic.Items);

        });

        model.SupportSubTopicId.Data(items);

    };

    //
    var bindFileUploader = function () {

        var $form = $('#frmCreateSupportRequestAttachments');
        var fileCount = 0;

        $form.fileupload({
            acceptFileTypes: /(\.|\/)(doc|docx|gif|jpe?g|pdf|png|ppt|pptx|txt|xls|xlsx|zip)$/i,
            autoUpload: false,
            dataType: 'json',
            maxFileSize: 5000000, //5mb in bytes
            url: '/Support/UploadSupportRequestMessageAttachment',
            change: function (e, data) {
                $form.find('.error').text('');
            },
            processfail: function (e, data) {
                var error = data.files[data.index].error;
                var errorMessage = error;
                if (error.indexOf('too large') > -1)
                    errorMessage = 'We cannot accept files over 5MB.';
                else if (error.indexOf('not allowed') > -1)
                    errorMessage = 'This is an unsupported file type.';

                $form.find('.error').text(errorMessage);
            }
        }).on('fileuploadadded', function () {

            fileCount++;

            $form.find('button.cancel').off('click').on('click', function () {
                fileCount--;
                $form.find('.error').text('');
            });
        })
            .on('fileuploadalways', function () {
                fileCount--;
                if (fileCount <= 0)
                    internal.submitComplete();
            });
    };

    //
    var initialize = function ($scope) {

        //
        model = new viewModel();
        loadData().done(function() {

            ko.applyBindings(model, $scope[0]);
            model.SupportTopicId.subscribe(setSubTopics);

            //
            common.ResetUnobtrusiveValidation($('#frmCreateSupportRequest'));
            common.InitTooltips($scope);

            //
            bindFileUploader();

            //
            $scope.on('submit', '#frmCreateSupportRequest', internal.submitForm);
            $scope.on('click', '#btnCreateSupportRequestSubmit', function(e) {

                e.preventDefault();
                $scope.find('#frmCreateSupportRequest').submit();

            });

            //
            $scope.find('#TreatmentDate').datepicker({
                dateFormat: 'MM/DD/YYYY',
                defaultDate: new Date(),
                endDate: '+0d',
                autoclose: true
            });

            // show modal after all data loaded
            loadTopics().done(function() {
                $scope.modal('show');
                $.unblockUI();
            });

        });
    }

    createSupportRequest.Initialize = function (modalContainer, visitId, vpUserId) {

        visitPayUserId = vpUserId;

        var promise = window.VisitPay.Common.ModalContainerAsync(modalContainer, '/Support/CreateSupportRequest', 'GET', { visitId: visitId, visitPayUserId: vpUserId }, false);
        promise.done(function (modal) {
            initialize(modal);
        });

    };
    createSupportRequest.OnCreated = function() {};

    return createSupportRequest;

})(window.VisitPay.Common, $, ko);
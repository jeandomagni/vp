﻿function UpdateMatchRegion() {
    var permUnmatchMsgDisplay = $('#MatchStatus').val() === 'UnmatchedHard' ? 'inline-block' : 'none';
    var tempUnmatchMsgDisplay = $('#MatchStatus').val() === 'UnmatchedSoft' ? 'inline-block' : 'none';
    $('#ConfirmPermanentUnmatch').css('display', permUnmatchMsgDisplay);
    $('#ConfirmTemporaryUnmatch').css('display', tempUnmatchMsgDisplay);
}

window.VisitPay.VisitUnmatch = (function (common, $) {

    var visitUnmatch = {};

    function submitForm(e) {

        e.preventDefault();

        var $form = $(this);
        $form.parents('.modal').modal('hide');

        $.blockUI();

        $.post($form.attr('action'), $form.serialize() + '&__RequestVerificationToken=' + $('#__AjaxAntiForgeryForm input[name=__RequestVerificationToken]').val(), function (result) {

            $.unblockUI();

            $(document).trigger('visitUnmatchActionComplete', result);

        });

    };

    visitUnmatch.OpenModal = function(visitId, visitPayUserId) {

        $.blockUI();

        common.ModalNoContainerAsync('modalUnmatchVisit', '/Unmatch/UnmatchVisit', 'POST', {
            visitId: visitId,
            visitPayUserId: visitPayUserId
        }).done(function ($modal) {

            $.unblockUI();
            $modal.modal('show');

            var $form = $modal.find('form');
            common.ResetUnobtrusiveValidation($form);

            var validationString = $form.find('#SourceSystemKeyValidationString');
            common.DisablePaste(validationString, validationString);

            $modal.on('submit', 'form', submitForm);

        });

    };

    return visitUnmatch;

})(window.VisitPay.Common = window.VisitPay.Common || {}, jQuery);
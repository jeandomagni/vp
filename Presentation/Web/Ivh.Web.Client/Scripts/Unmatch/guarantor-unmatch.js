﻿window.VisitPay.GuarantorUnmatch = (function (common, $) {

    var confirmViewModel = (function (vpGuarantorId, hsGuarantorSourceSystemKeys, hsGuarantorMatchDiscrepancyId, unmatchActionType, unmatchSource, hsGuarantorMatchDiscrepancyDetails) {

        var self = this;

        self.ActionType = ko.observable(unmatchActionType);
        self.Source = ko.observable(unmatchSource);
        self.HsGuarantorMatchDiscrepancyId = ko.observable(hsGuarantorMatchDiscrepancyId);
        self.HsGuarantorSourceSystemKeys = ko.observableArray();
        self.HsGuarantorSourceSystemKeys.AllValues = hsGuarantorSourceSystemKeys;
        self.HsGuarantorSourceSystemKeys.Select = function (hsGuarantorSourceSystemKey, elem) {

            var $checkbox = $(elem.srcElement);
            var checked = $checkbox.prop('checked');

            if (checked && self.HsGuarantorSourceSystemKeys.indexOf(hsGuarantorSourceSystemKey) < 0) {
                self.HsGuarantorSourceSystemKeys.push(hsGuarantorSourceSystemKey);
            } else if (!checked && self.HsGuarantorSourceSystemKeys.indexOf(hsGuarantorSourceSystemKey) > -1) {
                self.HsGuarantorSourceSystemKeys.remove(hsGuarantorSourceSystemKey);
            }

            return true;

        };
        self.HsGuarantorSourceSystemKeysValidationString = ko.computed(function() {
            return self.HsGuarantorSourceSystemKeys().join('');
        });
        self.Notes = ko.observable();
        self.VpGuarantorId = ko.observable(vpGuarantorId);

        self.HsGuarantorMatchDiscrepancyDetails = ko.observableArray(hsGuarantorMatchDiscrepancyDetails).extend({ extendedArray: {} });

        return self;

    });

    var guarantorUnmatch = {};

    // confirm

    var confirmModel;

    function submitConfirm(e) {

        e.preventDefault();

        var $form = $(this);
        $form.parents('.modal').modal('hide');

        $.blockUI();

        $.post($form.attr('action'), ko.toJS(confirmModel), function(result) {

            $(document).trigger('guarantorUnmatchActionComplete', result);

        });

    };

    function openConfirmModal(vpGuarantorId, hsGuarantorSourceSystemKeys, hsGuarantorMatchDiscrepancyId, unmatchActionType, unmatchSource, hsGuarantorMatchDiscrepancyDetails) {

        $.blockUI();

        common.ModalNoContainerAsync('modalUnmatchGuarantors', '/Unmatch/UnmatchGuarantors', 'POST', {
            vpGuarantorId: vpGuarantorId,
            actionType: unmatchActionType,
            unmatchSource: unmatchSource
        }).done(function ($modal) {

            confirmModel = new confirmViewModel(vpGuarantorId, hsGuarantorSourceSystemKeys, hsGuarantorMatchDiscrepancyId, unmatchActionType, unmatchSource, hsGuarantorMatchDiscrepancyDetails);
            ko.cleanNode($modal);
            ko.applyBindings(confirmModel, $modal[0]);

            $.unblockUI();
            $modal.modal('show');

            var $form = $modal.find('form');
            common.ResetUnobtrusiveValidation($form);
            $form.data('validator').settings.ignore = '';

            $modal.on('submit', 'form', submitConfirm);

        });

    }

    guarantorUnmatch.OpenUnmatchConfirmModal = function (vpGuarantorId, hsGuarantorSourceSystemKeys, hsGuarantorMatchDiscrepancyId, unmatchSource, hsGuarantorMatchDiscrepancyDetails) {

        openConfirmModal(vpGuarantorId, hsGuarantorSourceSystemKeys, hsGuarantorMatchDiscrepancyId, 1, unmatchSource, hsGuarantorMatchDiscrepancyDetails);

    };

    guarantorUnmatch.OpenValidateMatchConfirmModal = function(vpGuarantorId, hsGuarantorSourceSystemKeys, hsGuarantorMatchDiscrepancyId, unmatchSource, hsGuarantorMatchDiscrepancyDetails) {

        openConfirmModal(vpGuarantorId, hsGuarantorSourceSystemKeys, hsGuarantorMatchDiscrepancyId, 2, unmatchSource, hsGuarantorMatchDiscrepancyDetails);

    };

    // select

    var submitSelect = function(e, $form, vpGuarantorId) {

        e.preventDefault();

        var hsGuarantorSourceSystemKeys = [];

        $form.find('input[name=HsGuarantorSourceSystemKeys]:checked').each(function () {
            hsGuarantorSourceSystemKeys.push($(this).val());
        });

        guarantorUnmatch.OpenUnmatchConfirmModal(vpGuarantorId, hsGuarantorSourceSystemKeys, null, 2);

    };

    guarantorUnmatch.OpenSelectModal = function(vpGuarantorId) {

        $.blockUI();

        common.ModalNoContainerAsync('modalSelectHsGuarantors', '/Unmatch/SelectHsGuarantors/?vpGuarantorId=' + vpGuarantorId, 'GET').done(function ($modal) {

            $.unblockUI();

            $modal.on('change', 'input[type=checkbox]', function () {
                var $submit = $modal.find('button[type=submit]');
                $submit.prop('disabled', $modal.find('input[type=checkbox]:checked').length === 0);
            });

            $modal.modal('show');

            common.ResetUnobtrusiveValidation($modal.find('form'));

            $modal.on('submit', 'form', function (e) {
                $modal.modal('hide');
                submitSelect(e, $(this), vpGuarantorId);
            });

        });

    };

    //

    return guarantorUnmatch;

})(window.VisitPay.Common = window.VisitPay.Common || {}, jQuery);
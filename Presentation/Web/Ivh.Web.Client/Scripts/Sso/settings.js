﻿window.VisitPay.ManageSso = (function (visitPay) {

    function submitForm(sender) {
        $.blockUI();
        var $form = sender;
        var $container = $form.parents('div.provider').eq(0).parent();
        var promise = $.Deferred();
        $.post($form.attr('action'), $form.serialize(), function (html) {
            $container.html(html);
            promise.resolve();
        });
        return promise;
    }

    var self = {};

    self.Initialize = function () {

        $('#modalManageSso').on('change', 'input[type=checkbox]', function () {
            var $form = $(this).parents('form').eq(0);
            submitForm($form).done(function () {
                setTimeout(function () {
                    $.unblockUI();
                }, 1000);
            });
        });

        $('#modalManageSso').on('click', '#btnOff', function () {
            var $form = $(this).parents('form').eq(0);
            $.post('/Sso/DeclineConfirmation', function(cmsVersion) {
                visitPay.Common.ModalGenericConfirmation(cmsVersion.ContentTitle, cmsVersion.ContentBody, 'Yes', 'No').done(function () {
                    submitForm($form).done($.unblockUI());
                });
            });
        });

    };

    return self;

})(window.VisitPay);
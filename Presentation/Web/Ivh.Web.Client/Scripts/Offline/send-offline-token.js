﻿(function () {
    
    window.VisitPay.SendToken = function () {

        function getContent(vpGuarantorId, sendFinancePlanConfirmation) {

            var promise = $.Deferred();

            var params = $.param({ vpGuarantorId: vpGuarantorId, sendFinancePlanConfirmation: sendFinancePlanConfirmation });
            $.get('/Offline/SendToken?' + params, function(html) {
                promise.resolve(html);
            });

            return promise;
        }

        function init(vpGuarantorId, sendFinancePlanConfirmation) {

            $.blockUI();
            var promise = $.Deferred();

            getContent(vpGuarantorId, sendFinancePlanConfirmation).done(function(html) {

                var $modal = window.VisitPay.Common.ModalFromHtml('#modalSendToken', html);

                $modal.find('[data-mask]').each(function() {
                    $(this).mask($(this).data('mask').toString());
                });

                $modal.on('change.send', '#SendSms', function(e) {
                    var $phone = $('#divPhoneNumber');
                    if ($(this).is(':checked')) {
                        $phone.show();
                    } else {
                        $phone.find('input').val('');
                        $phone.hide();
                    }
                });

                $modal.on('submit.send', 'form', function(e) {
                    e.preventDefault();
                    $(this).parseValidation();
                    if ($(this).valid()) {
                        $.blockUI();
                        $.post($(this).attr('action'), $(this).serialize(), function(result) {
                            promise.resolve();
                        });
                    }
                });

                $modal.modal('show');
                $.unblockUI();

                promise.done(function() {
                    if (!sendFinancePlanConfirmation) {
                        $.unblockUI();
                    }
                    $modal.modal('hide');
                });

            });

            return promise;

        }

        return {
            init: init
        };
    };

})();
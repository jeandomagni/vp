﻿window.VisitPay.CreateOfflineUserVm = (function () {
   
    var offlineGuarantorSearchResultStatusEnum = {
        Found: 0,
        NotFound: 1,
        Ambiguous: 2
    };

    var self = {};

    self.Step = ko.observable(1);
    self.Step.Is = function (step) {
        return parseInt(ko.unwrap(self.Step)) === parseInt(step);
    };
    self.Step.IsNot = function (step) {
        return !self.Step.Is(step);
    };

    self.Error = ko.observable();
    self.Error.Any = ko.computed(function () {
        return (ko.unwrap(self.Error) || '').length > 0;
    });

    self.Warning = ko.observable();
    self.Warning.Any = ko.computed(function () {
        return (ko.unwrap(self.Warning) || '').length > 0;
    });
    
    // fields
    self.AddressStreet1 = ko.observable();
    self.AddressStreet2 = ko.observable();
    self.City = ko.observable();
    self.DateOfBirthDay = ko.observable();
    self.DateOfBirthMonth = ko.observable();
    self.DateOfBirthYear = ko.observable();
    self.FirstName = ko.observable();
    self.GuarantorAcknowledged = ko.observable();
    self.GuarantorId = ko.observable();
    self.LastName = ko.observable();
    self.ShowPatientDateOfBirth = ko.observable(false);
    self.Ssn4 = ko.observable();
    self.State = ko.observable();
    self.Zip = ko.observable();

    // config values
    self.RequirePatientDateOfBirthDefault = ko.observable();
    self.RequireSsn = ko.observable();
    self.RequireZip = ko.observable();

    self.FromResult = function (result) {
        
        self.Error('');
        self.Warning('');

        var data = {};
        if (result.Status === offlineGuarantorSearchResultStatusEnum.Found) {
            data = $.extend(data, result.Data);
            self.Step(2);
        } else if (result.Status === offlineGuarantorSearchResultStatusEnum.NotFound) {
            self.Step(1);
            self.Error('Record not found.  Verify that the information provided matches the Guarantor\'s statement.');
        } else if (result.Status === offlineGuarantorSearchResultStatusEnum.Ambiguous) {
            self.Step(2);
            self.Warning('Multiple records were found.  Fill out the form using the information from the Guarantor\'s statement.');
        }

        // fields
        self.AddressStreet1(data.AddressStreet1);
        self.AddressStreet2(data.AddressStreet2);
        self.City(data.City);
        self.DateOfBirthDay(data.DateOfBirthDay);
        self.DateOfBirthMonth(data.DateOfBirthMonth);
        self.DateOfBirthYear(data.DateOfBirthYear);
        self.FirstName(data.FirstName);
        self.GuarantorAcknowledged(data.GuarantorAcknowledged || false);
        self.Ssn4(data.Ssn4);
        self.State(data.State);
        self.Zip(data.Zip);

        // config values
        self.RequirePatientDateOfBirthDefault(data.RequirePatientDateOfBirthDefault);
        self.RequireSsn(data.RequireSsn);
        self.RequireZip(data.RequireZip);
    };
    self.FindModel = function () {
        return {
            SourceSystemKey: ko.unwrap(self.GuarantorId),
            LastName: ko.unwrap(self.LastName)
        };
    };
    self.SubmitModel = function () {
        return ko.mapping.toJS(self, { 'ignore': ['Error', 'Warning', 'FromResult', 'FindModel', 'SubmitModel', 'Step'] });
    };
    
    return self;

});

(function (urls, clientSettings) {
    
    var model = new window.VisitPay.CreateOfflineUserVm();
    var $scope = $('#section-createvpguarantor');
    var $findForm = $scope.find('#frmFindGuarantor');
    var $createForm = $scope.find('#frmCreateGuarantor');

    function goToOfflineCreateFinancePlan(vpGuarantorId) {
        window.location.href = '/Offline/CreateFinancePlan/?vpGuarantorId=' + vpGuarantorId;
    }

    function goToGuarantorDetails(vpGuarantorId) {
        window.location.href = '/Search/GuarantorAccount/' + vpGuarantorId;
    }
    
    function findGuarantor(e) {
        e.preventDefault();

        var $form = $(this);
        $form.parseValidation();
        if (!$form.valid()) {
            return;
        }

        $.blockUI();

        $.post($form.attr('action'), model.FindModel(), function(result) {
            $createForm.parseValidation();
            $createForm.find('[data-valmsg-summary-custom="true"]').find('ul').empty();
            model.FromResult(result);
            $.unblockUI();
        });
    };

    function submitGuarantor(e) {
        e.preventDefault();

        var $form = $(this);
        $form.parseValidation();
        if (!$form.valid()) {
            return;
        }

        $.blockUI();

        $.post($form.attr('action'), { model: model.SubmitModel() }, function (result) {

            if (result.Result === true) {

                var vpGuarantorId = result.Message;
                var statementUrl = urls.VpGuarantorHasStatement + '?vpGuarantorId=' + vpGuarantorId;
                var retryTimes = clientSettings.GuarantorEnrollmentProviderDelaySeconds * 2;
                var retryInterval = 2000;
                var retryCodes = [204];

                $.get(statementUrl)
                    .retry({ times: retryTimes, interval: retryInterval, statusCodes: retryCodes }) // retry if NoContent returned
                    .done(function() { 
                        goToOfflineCreateFinancePlan(vpGuarantorId); 
                    })
                    .fail(function() { 
                        goToGuarantorDetails(vpGuarantorId); 
                    });

            } else {
                $form.addCustomErrors(result.Message);
                $.unblockUI();
            }
                
        });
    };
    
    $(document).ready(function () {
        
        ko.applyBindings(model, $scope[0]);
        
        $findForm.off('submit').on('submit', findGuarantor);
        $createForm.off('submit').on('submit', submitGuarantor);

        $scope.find('#cbUsePatientDateOfBirth').off('click').on('click', function () {
            $scope.find('#patient-dob-container').toggle(this.checked);
        });

        $scope.find('#Zip').zipCode();
        $scope.find('#DateOfBirthDay').numbersOnly();
        $scope.find('#DateOfBirthYear').numbersOnly();
        $scope.find('#PatientDateOfBirthDay').numbersOnly();
        $scope.find('#PatientDateOfBirthYear').numbersOnly();

        $scope.show();
        
    });

})(window.VisitPay.Urls, window.VisitPay.ClientSettings);


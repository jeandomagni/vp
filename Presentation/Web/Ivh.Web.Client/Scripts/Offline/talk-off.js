﻿(function() {
    
    $(document).ready(function() {

        var $scope = $('.talk-off-content');
        var expandedClassName = 'expanded';
        var sessionStorageKey = 'talk-off:isexpanded';
        var isExpanded = (sessionStorage.getItem(sessionStorageKey) || true).toString().toLowerCase() === 'true' ? true : false;

        if (isExpanded === true) {
            $scope.addClass(expandedClassName);
        } else {
            $scope.removeClass(expandedClassName);
        }

        $scope.find('.action, .header').on('click', function() {
            var isExpanded = $scope.hasClass(expandedClassName);
            if ($scope.hasClass(expandedClassName)) {
                $scope.removeClass(expandedClassName);
            } else {
                $scope.addClass(expandedClassName);
            }
            sessionStorage.setItem(sessionStorageKey, !isExpanded);
        });

        $scope.find('.talking-point').on('click', 'h4', function() {
            var $parent = $(this).parents('.talking-point').eq(0);
            var $content = $parent.find('.talking-point-content');
            if ($content.is(':visible')) {
                $content.slideUp(250);
            } else {
                $content.slideDown(250);
            }
        });
    });

})();
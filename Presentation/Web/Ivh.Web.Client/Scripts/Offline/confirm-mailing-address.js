﻿(function () {

    window.VisitPay.MailingAddressVm = function () {

        var self = {};

        // AddressViewModel.cs
        self.AddressStreet1 = ko.observable();
        self.AddressStreet2 = ko.observable();
        self.City = ko.observable();
        self.StateProvince = ko.observable();
        self.PostalCode = ko.observable();
        self.UspsAddressStreet1 = ko.observable();
        self.UspsAddressStreet2 = ko.observable();
        self.UspsCity = ko.observable();
        self.UspsStateProvince = ko.observable();
        self.UspsPostalCode = ko.observable();
        self.IncludesUspsStandardizedAddress = ko.observable();
        self.IsUspsFormatSelected = ko.observable(false);
        self.RemoveEmailPreferences = ko.observable();
        self.ShowRemoveEmailPreferences = ko.observable();

        self.Validation = {
            HasErrors: ko.observable(false),
            Skip: ko.observable(false),
            Failed: ko.observable(false),
            Messages: ko.observableArray([])
        };
        self.Validation.Reset = function () {
            self.Validation.Failed(false);
            self.Validation.HasErrors(false);
            self.Validation.Messages.removeAll();
        };
        self.Validation.Reset();

        self.Map = function (model) {
            self.AddressStreet1(model.AddressStreet1);
            self.AddressStreet2(model.AddressStreet2);
            self.City(model.City);
            self.StateProvince(model.StateProvince);
            self.PostalCode(model.PostalCode);
            self.UspsAddressStreet1(model.UspsAddressStreet1);
            self.UspsAddressStreet2(model.UspsAddressStreet2);
            self.UspsCity(model.UspsCity);
            self.UspsStateProvince(model.UspsStateProvince);
            self.UspsPostalCode(model.UspsPostalCode);
            self.IncludesUspsStandardizedAddress(model.IncludesUspsStandardizedAddress);
            self.IsUspsFormatSelected(model.IsUspsFormatSelected);
            self.RemoveEmailPreferences(model.RemoveEmailPreferences);
            self.ShowRemoveEmailPreferences(model.ShowRemoveEmailPreferences);
        };

        self.PostModel = function () {
            return ko.mapping.toJS(self, { 'ignore': ['IsSubmitEnabled', 'Map', 'PostModel', 'Validation'] });
        };

        return self;
    }

    window.VisitPay.ConfirmMailingAddress = function () {

        function getContent() {

            var promise = $.Deferred();

            $.get('/Offline/MailingAddressPartial', function (html) {
                promise.resolve(html);
            });

            return promise;
        }

        function init(currentVisitPayUserId) {

            var promise = $.Deferred();

            getContent().done(function (html) {

                var $modal = window.VisitPay.Common.ModalFromHtml('#modalMailingAddressConfirm', html);

                var params = $.param({ visitPayUserId: currentVisitPayUserId });
                $.get('/Offline/MailingAddress?' + params, function (addressVm) {

                    // map to knockout model
                    var vm = new window.VisitPay.MailingAddressVm();
                    vm.Map(addressVm);
                    ko.applyBindings(vm, $modal[0]);

                    // submit binding
                    $modal.off('submit').on('submit', 'form', function (e) {

                        e.preventDefault();

                        $(this).parseValidation();
                        if (!$(this).valid()) {
                            return;
                        }

                        var postModel = {
                            visitPayUserId: currentVisitPayUserId,
                            address: vm.PostModel(),
                            validate: !ko.unwrap(vm.Validation.Skip),
                            addPreferences: true
                        };

                        $.post($(this).attr('action'), postModel, function (result) {
                            vm.Validation.Reset();
                            if (result.Result === true) {
                                promise.resolve();
                                $modal.modal('hide');
                            } else {
                                vm.Validation.HasErrors(true);
                                if (result.AddressStatus === 3) { // address rejected
                                    vm.Validation.Failed(true);
                                }

                                ko.utils.arrayPushAll(vm.Validation.Messages, result.Data);
                            }
                        });

                    });

                    //
                    $modal.find('#PostalCode').zipCode();

                    // show it
                    $modal.modal('show');
                    $.unblockUI();

                });

            });

            return promise;
        }

        return {
            init: init
        };

    };

})();
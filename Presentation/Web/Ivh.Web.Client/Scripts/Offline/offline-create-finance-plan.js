﻿(function() {

    window.VisitPay.OfflineCreateFinancePlan = function(data) {
        
        function onPreProcess(model) {
            
            var promise = $.Deferred();

            if (model.FinancePlanType.RequireEmailCommunication === true) {

                new window.VisitPay.SendToken().init(model.VpGuarantorId, true).done(function() {
                    promise.resolve();
                });

            } else if (model.FinancePlanType.RequirePaperCommunication === true) {

                new window.VisitPay.ConfirmMailingAddress().init(model.CurrentVisitPayUserId).done(function() {
                    promise.resolve();
                });

            }
            else {

                promise.resolve();

            }

            var autoPayPromise = $.Deferred();
            promise.done(function () {

                setGuarantorAutoPayAndPaper(model.VpGuarantorId, model.FinancePlanType.IsAutoPay).done(function () {
                    autoPayPromise.resolve();
                });
             
            });
            
            return autoPayPromise;

        };
        
        function onPostProcess() {
            
            $.blockUI();
            window.location.href = data.HomeUrl;

            // don't need the downstream callback to actually fire, as we're just redirecting
            return $.Deferred().reject();
            
        };

        function setGuarantorAutoPayAndPaper(vpGuarantorId, isAutoPay) {
            var promise = $.Deferred();

            $.post('/Offline/SetGuarantorAutoPay', { vpGuarantorId: vpGuarantorId, isAutoPay: isAutoPay }, function (result) {
                promise.resolve();
            });

            return promise;
        }

        function init() {

            $('.arrange-payment-module').hide();

            if (data.HasCombinedOption || data.HasDefaultOption) {

                var financePlanModule = new window.VisitPay.ArrangePayment.FinancePlanModule({
                    optionUrl: data.OptionUrl,
                    submitUrl: data.FinancePlanSubmitUrl,
                    validateUrl: data.FinancePlanValidateUrl,
                    homeUrl: data.HomeUrl,
                    additionalParameters: { currentVisitPayUserId: data.VisitPayUserId },
                    enablePaymentMethods: true,
                    preProcess: onPreProcess,
                    onPaymentComplete: onPostProcess
                });

                $('#arrange-payment').css('visibility', 'visible');
                
                $('#finance-plan').show();
                financePlanModule.init(data.PaymentOptions[0]);

                $('[data-toggle="tooltip"]').tooltip({ animation: false, container: 'body' });
                $('[data-toggle="tooltip-html"]').tooltipHtml();

            } else {

                $('#arrange-payment').css('visibility', 'visible');
                $('#noOptions').show();

            }

        };

        return {
            init: init
        };

    };

})();
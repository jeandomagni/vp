﻿// models
(function () {

    window.VisitPay.PaymentMethodListItemViewModel = function() {

        var self = {};

        self.PaymentMethodId = ko.observable();
        self.PaymentMethodType = ko.observable();

        self.AccountNickName = ko.observable();
        self.AccountNickName.Any = ko.computed(function () {
            return ko.unwrap(self.AccountNickName) && ko.unwrap(self.AccountNickName).length > 0;
        });
        self.DisplayAccountTypeText = ko.observable();
        self.DisplayProviderTypeText = ko.observable();
        self.DisplayProviderTypeImage = ko.observable();
        self.DisplayProviderTypeImage.Any = ko.computed(function () {
            return ko.unwrap(self.DisplayProviderTypeImage) !== undefined && ko.unwrap(self.DisplayProviderTypeImage) !== null && ko.unwrap(self.DisplayProviderTypeImage).length > 0;
        });
        self.DisplayAccountTypeText.WithProvider = ko.computed(function () {

            var account = ko.unwrap(self.DisplayAccountTypeText);
            var accountText = account !== undefined && account !== null && account.length > 0 ? account : '';

            var provider = ko.unwrap(self.DisplayProviderTypeText);
            var providerText = provider !== undefined && provider !== null && provider.length > 0 ? provider : '';

            if (accountText.length > 0 && providerText.length > 0) {
                return providerText + ' ' + accountText;
            }

            return '';

        });
        self.DisplayImage = ko.observable();
        self.DisplayName = ko.observable();
        self.DisplayName.Any = ko.computed(function () {
            return ko.unwrap(self.DisplayName) && ko.unwrap(self.DisplayName).length > 0;
        });
        self.ExpDateForDisplay = ko.observable();
        self.ExpDateForDisplay.Display = ko.computed(function () {
            return 'exp. ' + ko.unwrap(self.ExpDateForDisplay);
        });
        self.ExpirationMessage = ko.observable();
        self.ExpirationMessage.Any = ko.computed(function () {
            return ko.unwrap(self.ExpirationMessage) && ko.unwrap(self.ExpirationMessage).length > 0;
        });
        self.IsAchType = ko.observable();
        self.IsCardType = ko.observable();
        self.IsActive = ko.observable();
        self.IsRemoveable = ko.observable();
        self.LastFour = ko.observable();
        self.LastFour.Display = ko.computed(function () {
            var lastFour = ko.unwrap(self.LastFour);
            if (lastFour && lastFour.length > 0) {
                return 'X-' + ko.unwrap(self.LastFour);
            }
            return '';
        });
        self.RemoveableMessage = ko.observable();
        self.RemoveableMessage.Display = ko.computed(function () {
            return ko.unwrap(self.IsRemoveable) ? 'Remove Payment Method' : ko.unwrap(self.RemoveableMessage);
        });

        // primary
        self.CanSetPrimary = ko.observable(false);
        self.IsPrimary = ko.observable();
        
        return self;
    };

    window.VisitPay.PaymentMethodsViewModel = function (currentVisitPayUserId) {

        var self = {};

        //
        self.AllAccounts = ko.observableArray([]);
        self.AllAccounts.Count = ko.computed(function () {
            return ko.unwrap(self.AllAccounts).length;
        });

        //
        self.AllowSingleUse = ko.observable(false);

        //
        self.SelectedPaymentMethod = ko.observable();
        self.SelectedPaymentMethod.Set = function (paymentMethodId, previousSelection, singleUsePaymentMethod) {

            var selected;

            if (paymentMethodId && parseInt(paymentMethodId) > 0) {
                selected = ko.utils.arrayFirst(ko.unwrap(self.AllAccounts), function (paymentMethod) {
                    return ko.unwrap(paymentMethod.PaymentMethodId) === paymentMethodId;
                });
            }
            // is it a new single use payment method?
            else if (singleUsePaymentMethod !== undefined && singleUsePaymentMethod !== null) {
                selected = ko.mapping.fromJS(singleUsePaymentMethod, null, new window.VisitPay.PaymentMethodListItemViewModel());
            }
            // is previously selected method still available?
            else if (previousSelection !== undefined && previousSelection !== null) {

                if (ko.unwrap(previousSelection.IsActive)) {

                    selected = ko.utils.arrayFirst(ko.unwrap(self.AllAccounts), function (paymentMethod) {
                        return ko.unwrap(paymentMethod.PaymentMethodId) === ko.unwrap(previousSelection.PaymentMethodId);
                    });

                } else {

                    selected = previousSelection;

                }
            }

            // nothing found, get primary
            if (selected === undefined || selected === null) {
                selected = ko.unwrap(self.PrimaryPaymentMethod);
            }

            // set value
            if (selected === undefined || selected === null) {
                self.SelectedPaymentMethod(null);
            } else {
                self.SelectedPaymentMethod(ko.unwrap(selected));
            }

        };

        //
        self.PrimaryPaymentMethod = ko.observable();
        self.PrimaryPaymentMethod.Set = function () {

            var primary = ko.utils.arrayFirst(ko.unwrap(self.AllAccounts), function (paymentMethod) {
                return ko.unwrap(paymentMethod.IsPrimary);
            });

            self.PrimaryPaymentMethod(primary);

        };

        //
        self.CurrentVisitPayUserId = ko.observable(currentVisitPayUserId);

        var deviceUserAccount;
        var deviceBlackoutStopUrl;
        var deviceBlackoutStartUrl;
        var deviceBlackoutStarted;
        var traceClientName;

        return self;

    };

    window.VisitPay.PaymentMethodViewModel = function (allowSingleUse) {

        var self = {};

        //
        self.AllowSingleUse = ko.observable(allowSingleUse);

        // shared
        self.AccountNickName = ko.observable();
        self.BillingId = ko.observable();
        self.AvsCode = ko.observable();
        self.GatewayToken = ko.observable();
        self.IsActive = ko.observable(true);
        self.LastFour = ko.observable();
        self.PaymentMethodId = ko.observable(0);
        self.PaymentMethodId.IsNew = ko.computed(function () {
            return ko.unwrap(self.PaymentMethodId) === 0;
        });
        self.PaymentMethodTypeId = ko.observable();
        self.PaymentMethodTypeId.IsAchType = ko.computed(function () {
            var paymentMethodTypeId = ko.unwrap(self.PaymentMethodTypeId);
            return paymentMethodTypeId >= 5 && paymentMethodTypeId <= 6;
        });
        self.PaymentMethodTypeId.IsCardType = ko.computed(function () {
            var paymentMethodTypeId = ko.unwrap(self.PaymentMethodTypeId);
            return paymentMethodTypeId >= 1 && paymentMethodTypeId <= 4;
        });
        self.PaymentMethodTypeId.IsAmex = ko.computed(function () {
            return ko.unwrap(self.PaymentMethodTypeId) === 3;
        });
        self.PaymentMethodTypeId.IsDiscover = ko.computed(function () {
            return ko.unwrap(self.PaymentMethodTypeId) === 4;
        });
        self.PaymentMethodTypeId.IsMastercard = ko.computed(function () {
            return ko.unwrap(self.PaymentMethodTypeId) === 2;
        });
        self.PaymentMethodTypeId.IsVisa = ko.computed(function () {
            return ko.unwrap(self.PaymentMethodTypeId) === 1;
        });

        // ach specific
        self.AccountNumber = ko.observable();
        self.AccountNumberConfirm = ko.observable();
        self.AchTypeString = ko.observable();
        self.BankName = ko.observable();
        self.FirstName = ko.observable();
        self.LastName = ko.observable();
        self.RoutingNumber = ko.observable();

        // card specific
        self.AddressLine1 = ko.observable();
        self.AddressLine2 = ko.observable();
        self.CardCode = ko.observable();
        self.CardCodeRequired = ko.observable(true);
        self.City = ko.observable();
        self.ExpirationMonth = ko.observable();
        self.ExpirationYear = ko.observable();
        self.NameOnCard = ko.observable();
        self.AccountTypeId = ko.observable();
        self.ProviderTypeId = ko.observable();
        self.PaymentMethodAccountTypes = ko.observable();
        self.PaymentMethodProviderTypes = ko.observable();
        self.State = ko.observable();
        self.Zip = ko.observable();

        //
        self.FromDeviceResponse = function (response) {

            if (response == null) {
                return;
            }
            
            // card code is not required for a device payment method
            self.CardCode(null);
            self.CardCodeRequired(false);

            self.BillingId(response['billing-id'] || null);
            self.GatewayToken(response['transaction-id'] || null);
            self.LastFour(response['last-four'] || null);
            self.NameOnCard(response['card-holder-name'] || null);
            self.ExpirationMonth(response['card-expiration-month'] || null);
            self.ExpirationYear(response['card-expiration-year'] || null);

            switch ((response['card-brand'] || '').toLowerCase().replace(/ /g, '')) {
                case 'visa':
                    self.PaymentMethodTypeId(1);
                    break;
                case 'mastercard':
                    self.PaymentMethodTypeId(2);
                    break;
                case 'americanexpress':
                    self.PaymentMethodTypeId(3);
                    break;
                case 'discover':
                    self.PaymentMethodTypeId(4);
                    break;
            }

            var isHsa = response['is-hsa'] || false;
            if (isHsa === true || isHsa.toString() === '1') {
                self.AccountTypeId(1);
            }

            var isFsa = response['is-fsa'] || false;
            if (isFsa === true || isFsa.toString() === '1') {
                self.AccountTypeId(2);
            }
        }

        return self;

    };

})();

// payment method list
(function () {

    var opts = {
        urlPaymentMethod: '/Payment/PaymentMethod',
        urlPaymentMethodBank: '/Payment/PaymentMethodBank',
        urlPaymentMethodCard: '/Payment/PaymentMethodCard',
        urlPaymentMethodAccountProvider: '/Payment/PaymentMethodAccountProvider',
        urlPaymentMethodsList: '/Payment/GetPaymentMethodsList',
        urlRemovePaymentMethod: '/Payment/RemovePaymentMethod',
        urlSavePaymentMethodBank: '/Payment/SavePaymentMethodBank',
        urlSavePaymentMethodCard: '/Payment/SavePaymentMethodCard',
        urlSetAccountProviderType: '/Payment/SetPaymentMethodAccountProviderType',
        urlSetPrimary: '/Payment/SetPrimaryPaymentMethod',
        urlCardReaderParameters: '/CardReader/GetCardReaderDeviceParameters',
        urlBlackoutStart: '/CardReader/BlackoutStart',
        urlBlackoutStop: '/CardReader/BlackoutStop'
    };

    window.VisitPay.PaymentMethods = function(model, $scopeElement, options) {
        
        opts = $.extend({}, opts, options);

        //
        var paymentMethodInstance = new window.VisitPay.PaymentMethod(opts);
        var cms = {
            AccountRemoveCms: {
                // this one is not actually cms
                ContentBody: '<p>Are you sure you want to remove this payment method?</p>',
                ContentTitle: 'Remove Payment Method'
            },
            HsaInterestDisclaimerCms: {}
        };
        
        //
        function showAlert(title, message, className) {

            className = 'alert-' + (className || 'success');

            var $alerts = $scopeElement.find('#paymentmethod-alerts');
            if ($alerts.length > 0) {
                $alerts.html('');

                var $alert = $('<div/>', { 'class': 'alert ' + className + ' alert-dismissible' });
                var $button = $('<button/>', { type: 'button', 'class': 'close', 'data-dismiss': 'alert' }).html('&times;');

                $alert.append($button);

                if (title && title.length > 0) {
                    $alert.append('<p><strong>' + title + '</p>');
                }
                if (message && message.length > 0) {
                    $alert.append(message);
                }

                $alerts.append($alert);
                $alerts.alert();
            }
        }
        
        function showErrorMessage(message) {
            showAlert('An Error Has Occurred', '<p>' + message + '</p>', 'danger');
        };

        function showPrimaryNotification() {
            
            if (opts.suppressPrimaryNotification != null && opts.suppressPrimaryNotification === true) {
                return;
            }

            var text = '<p>All finance plan payments will be applied to this payment method.</p>';
            if (cms.HsaInterestDisclaimerCms !== undefined && cms.HsaInterestDisclaimerCms !== null) {
                var hsa = (cms.HsaInterestDisclaimerCms.ContentBody || '');
                if (hsa.length > 0) {
                    text += '<hr/><div class="text-small text-left"><p>' + hsa + '</p></div>';
                }
            }
            showAlert('You have selected a new primary payment method', text);

        };

        //
        function loadPaymentMethods(singleUsePaymentMethod) {
            
            var promise = $.Deferred();

            // get selected payment method
            var previousSelection = ko.unwrap(model.PaymentMethods.SelectedPaymentMethod);
            
            // post
            $.post(opts.urlPaymentMethodsList, { currentVisitPayUserId: ko.unwrap(model.PaymentMethods.CurrentVisitPayUserId) }, function(data) {
                
                // map
                ko.mapping.fromJS(data.AllAccounts, {
                    create: function(options) { return ko.mapping.fromJS(options.data, {}, new window.VisitPay.PaymentMethodListItemViewModel()); }
                }, model.PaymentMethods.AllAccounts);

                ko.mapping.fromJS(data.PaymentMethodAccountTypes, {
                    create: function(o) { return ko.mapping.fromJS(o.data, {}, new window.VisitPay.PaymentMethodAccountTypeVm()); }
                }, model.PaymentMethods.PaymentMethodAccountTypes);

                ko.mapping.fromJS(data.PaymentMethodProviderTypes, {
                    create: function(o) { return ko.mapping.fromJS(o.data, {}, new window.VisitPay.PaymentMethodProviderTypeVm()); }
                }, model.PaymentMethods.PaymentMethodProviderTypes);

                // cms
                cms.HsaInterestDisclaimerCms = data.HsaInterestDisclaimerCms;

                // set primary
                model.PaymentMethods.PrimaryPaymentMethod.Set();

                // set selected
                model.PaymentMethods.SelectedPaymentMethod.Set(null, previousSelection, singleUsePaymentMethod);

                promise.resolve();

            });

            return promise;
        }
        
        //
        function setPrimary(e) {
            
            var paymentMethod = ko.dataFor(this);
            if (!ko.unwrap(paymentMethod.CanSetPrimary)) {
                return;
            }
            
            var paymentMethodId = ko.unwrap(paymentMethod.PaymentMethodId);
            var currentVisitPayUserId = ko.unwrap(model.PaymentMethods.CurrentVisitPayUserId);
            
            $.blockUI();
            $.post(opts.urlSetPrimary, { paymentMethodId: paymentMethodId, currentVisitPayUserId: currentVisitPayUserId }, function(result) {

                loadPaymentMethods().done(function() {

                    $.unblockUI();

                    if (result.IsError) {
                        showErrorMessage(result.ErrorMessage);
                    } else if (result.PrimaryChanged) {
                        showPrimaryNotification();
                    }

                });

            });

        };

        //
        function remove(e) {

            if (e) {
                e.preventDefault();
            }

            var paymentMethod = ko.dataFor(this);
            if (!ko.unwrap(paymentMethod.IsRemoveable)) {
                return;
            }

            var paymentMethodId = ko.unwrap(paymentMethod.PaymentMethodId);
            var currentVisitPayUserId = ko.unwrap(model.PaymentMethods.CurrentVisitPayUserId);
            
            window.VisitPay.Common.ModalGenericConfirmation(cms.AccountRemoveCms.ContentTitle, cms.AccountRemoveCms.ContentBody, 'Yes', 'No').done(function() {

                $.blockUI();
                $.post(opts.urlRemovePaymentMethod, { paymentMethodId: paymentMethodId, currentVisitPayUserId: currentVisitPayUserId }, function(result) {

                    loadPaymentMethods().done(function() {

                        $.unblockUI();
                        if (result.IsError) {
                            showErrorMessage(result.ErrorMessage);
                        }

                    });

                });

            });

        };

        function select(e) {
            model.PaymentMethods.SelectedPaymentMethod.Set(ko.unwrap(ko.dataFor(this).PaymentMethodId));
        };
        
        // callback after add or edit
        function paymentMethodChanged(result) {

            // single use payment method, saved as inactive
            var singleUsePaymentMethod = null;
            if (!ko.unwrap(result.PaymentMethod.IsActive)) {
                singleUsePaymentMethod = ko.mapping.fromJS(result.PaymentMethod, {}, new window.VisitPay.PaymentMethodListItemViewModel());
            }

            loadPaymentMethods(singleUsePaymentMethod).done(function() {

                if (result.PrimaryChanged && ko.unwrap(model.PaymentMethods.AllAccounts.Count) > 1) {
                    showPrimaryNotification();
                }

            });

        };
        
        // load
        var deferred = $.Deferred();
        loadPaymentMethods().done(function() {
            deferred.resolve();
        });

        // event bindings
        $scopeElement.off('click');
        $scopeElement.off('change');
        
        // account events
        $scopeElement.on('click', '[id^="btnBankAccountAdd"]', function(e) {
            e.preventDefault();
            paymentMethodInstance.LoadBankModal(0, ko.unwrap(model.PaymentMethods.CurrentVisitPayUserId), ko.unwrap(model.PaymentMethods.AllowSingleUse()), true).then(paymentMethodChanged);
        });
        $scopeElement.on('click', '[id^="btnCardAccountAdd"]', function(e) {
            e.preventDefault();
            paymentMethodInstance.LoadCardModal(0, ko.unwrap(model.PaymentMethods.CurrentVisitPayUserId), ko.unwrap(model.PaymentMethods.AllowSingleUse()), false)
                .then(paymentMethodChanged)
                .always(function () {
                    if (!self.deviceBlackoutStopUrl || !self.deviceBlackoutStarted) {
                        // either no device or blackout has not started
                        return null;
                    }
                    // Stop blackout
                    var blackoutStopStartTime = Date.now();
                    return $.post(self.deviceBlackoutStopUrl, { userAccount: self.deviceUserAccount, clientName: self.traceClientName })
                        .always(function (response) {
                            var blackoutStopElapsedTimeMs = Date.now() - blackoutStopStartTime;
                            if (response) {
                                // Call blackout stop success logger from CardReader controller
                                self.deviceBlackoutStarted = false;
                                $.post(opts.urlBlackoutStop, { userAccount: self.deviceUserAccount, responseTimeMs: blackoutStopElapsedTimeMs});
                            } else {
                                // Call blackout stop failure logger from CardReader controller
                                $.post(opts.urlBlackoutStop, { userAccount: self.deviceUserAccount, responseTimeMs: blackoutStopElapsedTimeMs, success: false });
                                showAlert('Warning', 'Warning: we received an error while restarting the call recording. Agreements read over the phone may not be recorded. Please advise user to agree to terms with a link', 'danger');
                            }
                        });
                });

        });
        $scopeElement.on('click', '[name="paymentmethod-edit"]', function(e) {

            e.preventDefault();
            var data = ko.dataFor(this);

            if (ko.unwrap(data.IsCardType)) {
                paymentMethodInstance.LoadCardModal(ko.unwrap(data.PaymentMethodId), ko.unwrap(model.PaymentMethods.CurrentVisitPayUserId), false).then(paymentMethodChanged);

            } else {
                paymentMethodInstance.LoadBankModal(ko.unwrap(data.PaymentMethodId), ko.unwrap(model.PaymentMethods.CurrentVisitPayUserId), false).then(paymentMethodChanged);

            }

        });
        $scopeElement.on('click', '[name="paymentmethod-remove"]', remove);
        $scopeElement.on('click', '[name="paymentmethod-select"]', select);
        $scopeElement.on('change', '[name="paymentmethod-setprimary"]', setPrimary);

        // account/provider action.  
        $scopeElement.on('click', 'a.hsaType', function(e) {
            var data = ko.dataFor(this);
            e.preventDefault();
            paymentMethodInstance.LoadAccountProviderModal(ko.unwrap(data.PaymentMethodId), ko.unwrap(model.PaymentMethods.CurrentVisitPayUserId), false).then(paymentMethodChanged);
        });
        
        // subscriptions
        model.PaymentMethods.SelectedPaymentMethod.subscribe(function(previousValue) {

            if (previousValue === undefined || previousValue === null) {
                return;
            }

            // when switching away from a "single use" payment method, deactivate it.
            // it's already inactive, but this will also remove the billing id.
            // doing this b/c there's no way to switch back to it.
            if (!ko.unwrap(previousValue.IsActive)) {
                $.post(opts.urlRemovePaymentMethod, { paymentMethodId: ko.unwrap(previousValue.PaymentMethodId), currentVisitPayUserId: ko.unwrap(model.PaymentMethods.CurrentVisitPayUserId) });
            }

        }, self, 'beforeChange');
        model.PaymentMethods.SelectedPaymentMethod.subscribe(function(newValue) {

            var paymentMethod = ko.unwrap(newValue);
            var paymentMethodId = paymentMethod ? ko.unwrap(paymentMethod.PaymentMethodId) : -1;

            $('input[name="paymentmethod-select"]').each(function() {
                if (ko.dataFor(this)) {
                    $(this).prop('checked', ko.unwrap(ko.dataFor(this).PaymentMethodId) === paymentMethodId);
                }
            });

        });

        // deselect single use after switching to an option that does not allow it
        model.PaymentMethods.AllowSingleUse.subscribe(function(newValue) {
            if (!newValue && model.PaymentMethods.SelectedPaymentMethod && ko.unwrap(model.PaymentMethods.SelectedPaymentMethod) && !model.PaymentMethods.SelectedPaymentMethod().IsActive()) {
                model.PaymentMethods.SelectedPaymentMethod.Set(null, null, null);
            }
        });
        
        return deferred;

    };

})();

// payment method dialog
(function () {

    window.VisitPay.PaymentMethod = function (opts) {
        
        //
        var model;
        var modalPromise;
        var $securepanframe;
        var currentVisitPayUserId;
        var traceApiRetryTimes = 0;
        var traceApiMaxRetry = 3;

        function isSecurePanEnabled() {
            return $('.secure-pan-frame').length > 0;
        }

        // todo: append to validation summary instead
        function showError($form, message) {

            var parentModal = $($form.parents('.modal-paymentmethod')[0]);
            parentModal.addClass('disable-trigger');
            parentModal.modal('hide');

            window.VisitPay.Common.ModalGenericAlertMd('An Error Has Occurred', message, 'Ok').always(function () {
                parentModal.removeClass('disable-trigger');
                parentModal.modal('show');
            });

        };

        function showTraceApiMessage(title, message, traceApiBlackoutStartUrl, userAccount, traceClientName) {

            var $alerts = $('#traceApi-alerts');
            if ($alerts.length > 0) {
                $alerts.html('');

                var $alert = $('<div/>', { 'class': 'alert alert-danger alert-dismissible' });
                var $button = $('<button/>', { type: 'button', 'class': 'close', 'data-dismiss': 'alert' }).html('&times;');

                $alert.append($button);

                if (title && title.length > 0) {
                    $alert.append('<p><strong>' + title + '</p>');
                }
                if (message && message.length > 0) {
                    $alert.append(message);
                }
                if (traceApiRetryTimes < traceApiMaxRetry) {
                    $alert.append('<br/><br/>');
                    var $retryButton = $('<button/>', { type: 'button', 'class': 'btn btn-warning', 'data-dismiss': 'alert', id: 'traceRetryBtn' }).html('Retry');
                    $alert.append($retryButton);
                    traceApiRetryTimes += 1;
                }
                $alerts.append($alert);
                $alerts.alert();
            }
            $('#traceRetryBtn').click({
                TraceApiBlackoutStartUrl: traceApiBlackoutStartUrl,
                UserAccount: userAccount,
                TraceClientName: traceClientName
             }, callTraceApiBlackoutStart);
        };

        function callTraceApiBlackoutStart(event) {
            // Start blackout
            var retryTimes = 3;
            var retryInterval = 1000;
            var retryCodes = [204, 404, 500]; // No content, Not found, Internal server error
            var blackoutStartTime = Date.now();
            $.post(event.data.TraceApiBlackoutStartUrl, { userAccount: event.data.UserAccount, clientName: event.data.TraceClientName })
                .retry({ times: retryTimes, interval: retryInterval, statusCodes: retryCodes })
                .done(function (response) {
                    var blackoutElapsedTimeMs = Date.now() - blackoutStartTime;
                    self.deviceBlackoutStarted = true;
                    if (response) {
                        // Call blackout start success logger from CardReader controller
                        $.post(opts.urlBlackoutStart, { userAccount: event.data.UserAccount, responseTimeMs: blackoutElapsedTimeMs });
                    } else {
                        // Call blackout start failure logger from CardReader controller
                        $.post(opts.urlBlackoutStart, { userAccount: event.data.userAccount, responseTimeMs: blackoutElapsedTimeMs, success: false });
                        showTraceApiMessage('Warning', 'Warning: we received an error while pausing call recording. Card data read over the phone may be recorded. Please advise user to add card data with a link',
                            event.data.TraceApiBlackoutStartUrl, event.data.UserAccount, event.data.TraceClientName);
                    }
                })
                .fail(function () {
                    var blackoutElapsedTimeMs = Date.now() - blackoutStartTime;
                    self.deviceBlackoutStarted = false;
                    // Call blackout start failure logger from CardReader controller
                    $.post(opts.urlBlackoutStart, { userAccount: event.data.userAccount, responseTimeMs: blackoutElapsedTimeMs, success: false });
                    showTraceApiMessage('Warning', 'Warning: we received an error while pausing call recording. Card data read over the phone may be recorded. Please advise user to add card data with a link',
                        event.data.TraceApiBlackoutStartUrl, event.data.UserAccount, event.data.TraceClientName);
                });
        }

        function loadData(paymentMethodId, allowSingleUse, isNewAch) {

            var promise = $.Deferred();

            model = new window.VisitPay.PaymentMethodViewModel(allowSingleUse);

            $.post(opts.urlPaymentMethod, { paymentMethodId: paymentMethodId, currentVisitPayUserId: currentVisitPayUserId, isNewAch: isNewAch }, function(data) {

                var mapping = isNewAch ? {} : {
                    'PaymentMethodAccountTypes': {
                        create: function(o) {
                            return ko.mapping.fromJS(o.data, {}, new window.VisitPay.PaymentMethodAccountTypeVm());
                        }
                    }
                };

                ko.mapping.fromJS(data, mapping, model);
                promise.resolve();
            });

            return promise;

        };

        function loadModal(url) {

            var promise = $.Deferred();

            window.VisitPay.Common.ModalNoContainerAsync('modalPaymentMethod', url, 'GET', {}, false).done(function ($modal) {
                promise.resolve($modal);
            });

            return promise;
        };

        function loadAll(url, paymentMethodId, allowSingleUse, isNewAch) {

            $.blockUI();

            var promise = $.Deferred();

            loadData(paymentMethodId, allowSingleUse, isNewAch).done(function () {
                loadModal(url).done(function ($modal) {
                    promise.resolve($modal);
                    ko.applyBindings(model, $modal[0]);
                });
            });

            return promise;

        };

        function saveDevice(postModel, promise) {
            // nothing to do here at this point - just proceed
            promise.resolve();
        }

        function saveSecurePan(postModel, promise) {
            // on securepan saved
            $securepanframe.off('securepan.onsaved').on('securepan.onsaved', function(e, data) {

                postModel.BillingId = data.BillingId;
                postModel.GatewayToken = data.TransactionId;
                postModel.LastFour = data.LastFour;
                postModel.AvsCode = data.AvsCode;

                promise.resolve();
                $securepanframe.off('securepan.onsaved');

            });

            // trigger securepan save (this will trigger .onsaved or an error in the callback)
            $securepanframe.trigger('securepan.save', {
                cvv: postModel.CardCode,
                expM: postModel.ExpirationMonth,
                expY: postModel.ExpirationYear,
                name: postModel.NameOnCard,
                address1: postModel.AddressLine1,
                address2: postModel.AddressLine2,
                city: postModel.City,
                state: postModel.State,
                zip: postModel.Zip
            });
        }

        function save($form) {

            var promise = $.Deferred();
            var savePromise = $.Deferred();

            var isCardType = ko.unwrap(model.PaymentMethodTypeId.IsCardType);
            var postUrl = isCardType ? opts.urlSavePaymentMethodCard : opts.urlSavePaymentMethodBank;
            var postModel = ko.mapping.toJS(model, { 'ignore': ['AchTypes', 'Months', 'Years', 'PaymentMethodAccountTypes', 'PaymentMethodProviderTypes', 'AccountTypes', 'ProviderTypes', 'FromDeviceResponse'] });
            
            if (isCardType) {

                postModel.AccountTypeId = $('#AccountTypeId').val();
                if ($form.find('#ProviderTypeId').prop('disabled')) {
                    postModel.ProviderTypeId = 0;
                } else {
                    postModel.ProviderTypeId = $('#ProviderTypeId').val();
                }

                if (ko.unwrap(model.PaymentMethodId.IsNew)) {
                    // it's a new payment method
                    if (isSecurePanEnabled()) {
                        // save via securepan
                        saveSecurePan(postModel, savePromise);
                    } else {
                        // save via device
                        saveDevice(postModel, savePromise);
                    }
                } else {
                    savePromise.resolve();
                }

            } else {

                if (ko.unwrap(model.PaymentMethodId.IsNew)) {
                    postModel.RoutingNumber = $form.find('#RoutingNumber').val();
                    postModel.AccountNumber = $form.find('#AccountNumber').val();
                    postModel.AccountNumberConfirm = $form.find('#AccountNumberConfirm').val();
                }
                savePromise.resolve();

            }

            savePromise.done(function() {

                $.post(postUrl, { model: postModel, currentVisitPayUserId: currentVisitPayUserId, __RequestVerificationToken: $form.find('[name="__RequestVerificationToken"]').val() }, function(result) {

                    $.unblockUI();

                    if (result.IsSuccess) {

                        promise.resolve(result);

                    } else {

                        promise.reject();
                        showError($form, result.ErrorMessage);

                    }

                });

            });

            return promise;

        };

        function validate($form) {

            var promise = $.Deferred();

            $form.parseValidation();
            var formIsValid = $form.valid();

            if ($securepanframe && $securepanframe.length > 0 && ko.unwrap(model.PaymentMethodId.IsNew)) {
                
                // on securepan clientside validation complete
                $securepanframe.off('securepan.onvalidate').on('securepan.onvalidate', function(e, result) {

                    if (formIsValid && result.isValid) {
                        // form + securepan valid
                        promise.resolve();
                    } else {
                        // one or both invalid
                        promise.reject();
                    }

                    $securepanframe.off('securepan.onvalidate');
                });

                // trigger securepan to run clientside validation  (this will trigger .onvalidate or an error in the callback)
                $securepanframe.trigger('securepan.validate');

            } else {

                if (formIsValid) {
                    promise.resolve();
                } else {
                    promise.reject();
                }

            }

            return promise;

        };

        function submit(e) {

            var $form = $(this);

            e.preventDefault();

            validate($form).done(function () {

                $.blockUI();
                save($form).done(function (result) {

                    $($form.parents('.modal-paymentmethod')[0]).modal('hide');
                    modalPromise.resolve(result);

                });

            });

        };

        function showAccountProviderModel($modal) {
            var $form = $modal.find('form');
            $form.parseValidation();
            var $selectedAccountTypeId = $form.find('#SelectedAccountTypeId');
            var $selectedProviderTypeId = $form.find('#SelectedProviderTypeId');

            // save
            $form.on('submit',
                function (e) {
                    e.preventDefault();
                    if (!$form.valid()) {
                        return;
                    }
                    var data = ko.dataFor($form[0]);
                    var paymentMethodId = ko.unwrap(data.PaymentMethodId);
                    var accountTypeId = $selectedAccountTypeId.val();
                    var providerTypeId = $selectedProviderTypeId.val();

                    if ($selectedProviderTypeId.prop('disabled'))
                        providerTypeId = 0;

                    $.blockUI();
                    $.post(opts.urlSetAccountProviderType,
                        {
                            currentVisitPayUserId: currentVisitPayUserId,
                            paymentMethodId: paymentMethodId,
                            paymentMethodAccountTypeId: accountTypeId,
                            paymentMethodProviderTypeId: providerTypeId
                        },
                        function (result) {
                            $($form.parents('.modal-paymentmethod')[0]).modal('hide');
                            $.unblockUI();
                            modalPromise.resolve(result);
                        });
                });

            $selectedAccountTypeId.on('change', function () {
                $form.parseValidation();
                window.VisitPay.PaymentAccountProviderHelpers.accountTypeChanged($selectedAccountTypeId, $selectedProviderTypeId);
            });
            window.VisitPay.PaymentAccountProviderHelpers.accountTypeChanged($selectedAccountTypeId, $selectedProviderTypeId);

            // on close
            $modal.find('[data-dismiss="modal"]').on('click.pm', function () {
                modalPromise.reject();
            });

            // show modal
            $modal.modal('show');
            $.unblockUI();

        };

        function showModal($modal) {
            
            var $form = $modal.find('form');
            $form.parseValidation();

            // bank masks
            var accountMask = '9999[9999999999999]';
            $form.on('focus', '#AccountNumber', function () {
                $(this).inputmask('remove').inputmask(accountMask);
            });
            $form.on('focus', '#AccountNumberConfirm', function () {
                $(this).inputmask('remove').inputmask(accountMask);
            }); 
            $form.on('blur', '#AccountNumberConfirm', function () {
                // remove mask values (underscore) and revalidate so compare works
                $(this).inputmask(accountMask);
                $form.validate().element($(this));
            });
            $form.on('focus', '#RoutingNumber', function () {
                $(this).inputmask('remove').inputmask('999999999');
            });

            //
            $form.find('#CardCode').numbersOnly();
            $form.find('#Zip').zipCode();

            // image tooltips
            $modal.on('shown.bs.modal', function () {

                $form.find('.aRoutingAccountHelp').popover({
                    html: true,
                    trigger: 'hover',
                    placement: 'right',
                    container: 'body',
                    content: function () {
                        return '<img src="/Content/Images/check-routing-number.jpg" alt="" style="width: 450px; height: 213px;"/>';
                    }
                }).on('show.bs.popover', function () {
                    $(this).data('bs.popover').tip().css('max-width', '100%');
                });

                $form.find('#aCardCodeHelp').popover({
                    html: true,
                    trigger: 'hover',
                    placement: 'right',
                    container: 'body',
                    content: function () {
                        return '<img src="/Content/Images/cvv.jpg" alt="" style="width: 251px; height: 266px;" />';
                    }
                }).on('show.bs.popover', function () {
                    $(this).data('bs.popover').tip().css('max-width', '100%');
                });;

            });

            // set primary tooltips
            $form.find('a.checkbox-tip').each(function (e) {
                var $element = $(this);
                var title = $element.attr('title') || $element.attr('title-old');
                $element.attr('title-old', title).removeAttr('title');
                $element.tooltip({
                    animation: false,
                    container: 'body',
                    html: true,
                    title: '<span style="display: block; width: 320px;">' + title + '</span>'
                });
            });

            // save
            $form.off('submit').on('submit', submit);

            // on close
            $modal.find('[data-dismiss="modal"]').on('click.pm', function () {
                modalPromise.reject();
            });

            // show modal
            $modal.modal('show');
            $.unblockUI();

        };
        
        function onDeviceError($element, response, statusCode) {
            
            response = response || {};
            console.error('onError', response);

            // log
            var rawResponse = response['raw-ts-ipa-link-raw-response'];
            $.post('/Payment/DeviceError', { statusCode: statusCode, rawResponse: rawResponse, currentVisitPayUserId: currentVisitPayUserId });

            $element.find('#device-error-text').text(response['error']);
            
            var $detail = $element.find($('#device-errors'));
            $detail.html('');

            var messages = response['error-messages'];
            for (var key in messages) {
                if (messages.hasOwnProperty(key)) {
                    $detail.append('<div>' + key + ': ' + messages[key]);
                }
            }

            if ($detail.html().length === 0) {
                $detail.append('<div>An Error Has Occurred</div>');
            }
            
            $element.show();

        };

        function initializeDevice($modal, $form) {
            
            var keyPromise = $.Deferred();
            
            // ensure they've entered their key
            $.post(opts.urlCardReaderParameters, function (response) {

                var apiUrl = response.ApiUrl;
                var apiKey = response.ApiKey;
                var userAccount = response.UserAccount;
                var blackoutStartUrl = response.BlackoutStartUrl;
                var blackoutStopUrl = response.BlackoutStopUrl;
                var traceClientName = response.TraceClientName;

                if (response.DeviceKey == null) {
                    window.VisitPay.Common.ShowCardReaderModal().fail(keyPromise.reject).done(function (deviceKey) {
                        keyPromise.resolve({
                            DeviceKey: deviceKey,
                            ApiUrl: apiUrl,
                            ApiKey: apiKey,
                            UserAccount: userAccount,
                            BlackoutStartUrl: blackoutStartUrl,
                            BlackoutStopUrl: blackoutStopUrl,
                            TraceClientName: traceClientName
                        });
                    });
                } else {
                    keyPromise.resolve({
                        DeviceKey: response.DeviceKey,
                        ApiUrl: apiUrl,
                        ApiKey: apiKey,
                        UserAccount: userAccount,
                        BlackoutStartUrl: blackoutStartUrl,
                        BlackoutStopUrl: blackoutStopUrl,
                        TraceClientName: traceClientName
                });
                }

            });

            // exited dialog without entering a key
            keyPromise.fail(function () {
                console.info('deviceKey', 'failed');
                // the modal was never shown, so we just remove instead of hide.
                $modal.remove();
            });

            // we have a key, start the device communication
            keyPromise.done(function(deviceParameters) {
                
                var promise = $.Deferred();
                promise
                    .then(function() {
                        if (!deviceParameters.BlackoutStartUrl) {
                            return;
                        }
                        traceApiRetryTimes = 0;
                        callTraceApiBlackoutStart({data:
                            {
                                TraceApiBlackoutStartUrl: deviceParameters.BlackoutStartUrl,
                                UserAccount: deviceParameters.UserAccount,
                                TraceClientName: deviceParameters.TraceClientName
                            }
                        });
                        self.deviceUserAccount = deviceParameters.UserAccount;
                        self.deviceBlackoutStartUrl = deviceParameters.BlackoutStartUrl;
                        self.deviceBlackoutStopUrl = deviceParameters.BlackoutStopUrl;
                        self.traceClientName = deviceParameters.TraceClientName;
                    });
                promise.resolve(false);


                console.info('deviceParams', deviceParameters);
                
                var $deviceError = $modal.find('#device-error');
                var $devicePrompt = $modal.find('#device-prompt');

                $deviceError.hide();
                $devicePrompt.show();

                $modal.off('click.retry').on('click.retry', '#btnDeviceRetry', function (e) {
                    e.preventDefault();
                    initializeDevice($modal, $form);
                });
                
                showModal($modal);
                $modal.find('#btnAccountSave').attr('disabled', 'disabled');

                $.ajax({
                    global: false,
                    method: 'POST',
                    timeout: ((2 * 60) + 30) * 1000, // 2min30sec
                    headers: {
                        'Authorization': deviceParameters.ApiKey
                    },
                    dataType: 'json',
                    data: JSON.stringify({ 'card-reader-device-key': deviceParameters.DeviceKey }),
                    url: deviceParameters.ApiUrl,
                    contentType: 'application/json; charset=utf-8',
                    success: function(response) {
                        if ($('#traceRetryBtn').length) {
                            $('#traceRetryBtn').remove();
                        }
                        var result = response['result'].toString();
                        if (result.toLowerCase() === 'success' || result === '1') {
                            model.FromDeviceResponse(response);
                            $modal.find('#btnAccountSave').removeAttr('disabled');
                        } else {
                            onDeviceError($deviceError, response, null);
                        }
                        $devicePrompt.hide();
                    },
                    error: function (xhr) {
                        if (xhr.status.toString() === '0') {
                            // we could do a retry here - 0 means api is down i think
                        }
                        onDeviceError($deviceError, xhr.responseJSON, xhr.status);
                        $devicePrompt.hide();
                    }
                });
            });
        };

        function initializeSecurePan($modal, $form, visitPayUserId, callback) {

            window.VisitPay.SecurePan.Initialize().done(function($iframe) {

                $securepanframe = $iframe;

                // on card type changed
                $securepanframe.off('securepan.oncardtypechanged').on('securepan.oncardtypechanged', function(e, paymentMethodTypeId) {
                    model.PaymentMethodTypeId(paymentMethodTypeId);
                });

                // on error
                $securepanframe.off('securepan.onerror').on('securepan.onerror', function(e, result) {
                    $.unblockUI();
                    var $li = $form.find('.securepan-validationmessage').parents('li:eq(0)');
                    var $summary = $li.parents('.validation-summary-errors:eq(0)');

                    $li.remove();
                    if ($summary.find('li').length === 0) {
                        $summary.removeClass('validation-summary-errors').addClass('validation-summary-valid');
                    }

                    if (result.errorMessage && result.errorMessage.length > 0) {
                        window.VisitPay.Common.PrependMessageToValidationSummary($form, '<span class="securepan-validationmessage">' + result.errorMessage + '</span>');

                        // audit log
                        try {
                            $.post('/payment/InvalidCreditCard', {
                                errorMessage: result.errorMessage,
                                isTimeout: result.isTimeout,
                                currentVisitPayUserId: visitPayUserId,
                                __RequestVerificationToken: $('body').find('input[name=__RequestVerificationToken]').eq(0).val()
                            });
                        } catch (ex) { }
                    }
                });

                showModal($modal);

            }).fail(function(retry) {

                // securepan did not load successfully
                $modal.remove();

                if (retry) {
                    // call same function we're in again
                    callback();
                }

            });

        };
        
        function loadCardModal(paymentMethodId, visitPayUserId, allowSingleUse) {

            $securepanframe = null;
            currentVisitPayUserId = visitPayUserId;

            loadAll(opts.urlPaymentMethodCard, paymentMethodId, allowSingleUse, false).done(function ($modal) {

                var $form = $modal.find('form');

                // hsa account/provider
                var $selectedAccountTypeId = $form.find('#AccountTypeId');
                var $selectedProviderTypeId = $form.find('#ProviderTypeId');
                $selectedAccountTypeId.on('change', function () {
                    $form.parseValidation();
                    window.VisitPay.PaymentAccountProviderHelpers.accountTypeChanged($selectedAccountTypeId, $selectedProviderTypeId);
                });
                window.VisitPay.PaymentAccountProviderHelpers.accountTypeChanged($selectedAccountTypeId, $selectedProviderTypeId);

                // if new payment method, setup securepan
                if (ko.unwrap(model.PaymentMethodId.IsNew)) {
                    
                    if (isSecurePanEnabled()) {
                        initializeSecurePan($modal, $form, visitPayUserId, function() { loadCardModal(paymentMethodId, visitPayUserId, allowSingleUse); });
                    } else {
                        initializeDevice($modal, $form);
                    }

                } else {
                    showModal($modal);
                }
                
            });

        };

        return {
            LoadAccountProviderModal: function(paymentMethodId, visitPayUserId, allowSingleUse) {

                modalPromise = $.Deferred();
                $securepanframe = null;
                currentVisitPayUserId = visitPayUserId;

                loadAll(opts.urlPaymentMethodAccountProvider + '?paymentMethodId=' + paymentMethodId, paymentMethodId, allowSingleUse, false).done(function($modal) {

                    showAccountProviderModel($modal);

                });

                return modalPromise;

            },
            LoadBankModal: function(paymentMethodId, visitPayUserId, allowSingleUse, isNewAch) {

                modalPromise = $.Deferred();
                $securepanframe = null;
                currentVisitPayUserId = visitPayUserId;

                loadAll(opts.urlPaymentMethodBank, paymentMethodId, allowSingleUse, isNewAch).done(function($modal) {

                    showModal($modal);

                });

                return modalPromise;

            },
            LoadCardModal: function (paymentMethodId, visitPayUserId, allowSingleUse) {
                modalPromise = $.Deferred();
                loadCardModal(paymentMethodId, visitPayUserId, allowSingleUse);
                return modalPromise;
            }
        };

    };

})();

window.VisitPay.PaymentMethodAccountTypeVm = (function () {
    var self = this;

    self.PaymentMethodAccountTypeId = ko.observable();
    self.PaymentMethodAccountTypeListDisplayText = ko.observable();
    self.PaymentMethodAccountTypeSelectedDisplayText = ko.observable();
    self.DisplayOrder = ko.observable();
    return self;
});

window.VisitPay.PaymentMethodProviderTypeVm = (function () {
    var self = this;

    self.PaymentMethodProviderTypeId = ko.observable();
    self.PaymentMethodProviderTypeText = ko.observable();
    self.PaymentMethodProviderTypeDisplayText = ko.observable();
    self.ImageName = ko.observable();
    self.DisplayImage = ko.observable();
    self.DisplayOrder = ko.observable();

    self.DisplayImage.Any = ko.computed(function () {
        return ko.unwrap(self.DisplayImage).length > 0;
    });

    return self;
});

window.VisitPay.PaymentAccountProviderHelpers = (function () {

    function accountTypeChanged($selectedAccountTypeId, $selectedProviderTypeId) {
        var isNonOfTheAbove = $selectedAccountTypeId.find('option').first().prop('selected') || $selectedAccountTypeId.find('option').last().prop('selected');
        if (isNonOfTheAbove) {
            $selectedProviderTypeId.find('option:eq(0)').prop('selected', true);
            $selectedProviderTypeId.prop('disabled', true);
        } else {
            $selectedProviderTypeId.prop('disabled', false);
        }
    }

    return {
        accountTypeChanged: accountTypeChanged
    };

})();
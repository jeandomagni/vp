﻿window.VisitPay.DiscountsVm = function (guarantorOffer, managedOffers) {

    var self = this;

    guarantorOffer = guarantorOffer || {};
    managedOffers = managedOffers || [];

    self.DiscountGuarantorOffer = {
        DiscountTotal: ko.observable(ko.unwrap(guarantorOffer.DiscountTotal)),
        FirstNameLastName: ko.observable(ko.unwrap(guarantorOffer.FirstNameLastName)),
        VisitOffers: ko.observableArray(ko.unwrap(guarantorOffer.VisitOffers)).extend({ extendedArray: {} })
    };
    self.DiscountGuarantorOffer.Any = ko.computed(function () {
        return ko.unwrap(self.DiscountGuarantorOffer) !== undefined && ko.unwrap(self.DiscountGuarantorOffer) !== null && self.DiscountGuarantorOffer.VisitOffers.Any();
    });

    self.DiscountManagedOffers = ko.observableArray(managedOffers).extend({ extendedArray: {} });

    return self;

};
﻿(function (arrangePayment) {
    var FinancePlanStateViewModel = function(messenger) {
        var self = this;

        self.PaymentOptionId = ko.observable(null);
        self.FinancePlanOptionStateCode = ko.observable(null);
        self.FinancePlanOptionStateName = ko.observable(null);
        self.OtherFinancePlanStatesAreAvailable = ko.observable(false);
        self.FinancePlanVisits = ko.observableArray([]);

        self.StateSelected = function() {
            log.debug('state was selected: ', self.FinancePlanOptionStateCode());
            messenger.notifySubscribers(ko.toJS(self), 'financePlanStateSelected');
        };

        self.Map = function(data) {
            self.PaymentOptionId(data.PaymentOptionId);
            self.FinancePlanOptionStateCode(data.FinancePlanOptionStateCode);
            self.FinancePlanOptionStateName(data.FinancePlanOptionStateName);
            self.OtherFinancePlanStatesAreAvailable(data.OtherFinancePlanStatesAreAvailable);
            //Map visits
            ko.mapping.fromJS(data.FinancePlanVisits, {}, self.FinancePlanVisits);
        };
    };

    var FinancePlanStateSelectionViewModel = function(messenger) {
        var self = this;

        self.IsVisible = ko.observable(false);

        self.States = ko.observableArray([]);

        self.SortedStates = ko.computed(function() {
            return self.States.sort(function(l, r) {
                return l.FinancePlanVisits.length > r.FinancePlanVisits.length;
            });
        });

        self.Map = function(data) {
            //Loop through and map all state options
            for (var i = 0; i < data.length; i++) {
                var state = new FinancePlanStateViewModel(messenger);
                state.Map(data[i]);
                self.States.push(state);
            }
        };
    };

    arrangePayment.FinancePlanStateSelectionModule = (function () {
        var messenger = new ko.subscribable();

        var vm = new FinancePlanStateSelectionViewModel(messenger);

        $.each($('[data-context="financeplanstateselection"]'), function () {
            ko.applyBindings(vm, $(this)[0]);
        });
        
        //
        return {
            init: function (paymentOptions, financePlanOfferDetails) {
                var $scope = $('#finance-plan-state-selection');

                if (paymentOptions == null) {
                    vm.IsVisible(false);
                    return;
                }

                //Reset list
                vm.States([]);

                //Hide during load
                $scope.css('visibility', 'hidden');
                
                //Map options
                vm.Map(paymentOptions);

                log.debug('FP state selection module init with mapped VM: ', ko.unwrap(vm));

                // show
                $scope.css('visibility', 'visible');

                //Clear visit row tooltips
                $('.grid-row').attr('title', '');
            },
            messenger: messenger
        };

    });

})(window.VisitPay.ArrangePayment = window.VisitPay.ArrangePayment || {});
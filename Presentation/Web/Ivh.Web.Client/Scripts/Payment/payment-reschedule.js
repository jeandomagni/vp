﻿window.VisitPay.PaymentReschedule = (function ($) {

    var self = {};

    self.OnPaymentRescheduled = function () { };
    self.Initialize = function (paymentIds, guarantorId) {

        var modal = $('#modalPaymentRescheduleContainer').find('.modal');
        modal.modal('show');

        $('#btnPaymentRescheduleConfirm').off('click').on('click', function (e) {

            e.preventDefault();
            modal.modal('hide');

            $.blockUI();

            $.post('/Payment/PaymentReschedule', { paymentIds: paymentIds, guarantorId: guarantorId }, function () {

                $.unblockUI();
                self.OnPaymentRescheduled();

            }, 'json');

        });

    };

    return self;

})(jQuery);
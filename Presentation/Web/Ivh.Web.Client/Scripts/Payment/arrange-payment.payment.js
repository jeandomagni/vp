﻿(function(arrangePayment) {

    var paymentOptionEnum = {
        Resubmit: 1,
        FinancePlan: 4,
        SpecificAmount: 5,
        SpecificVisits: 6,
        SpecificFinancePlans: 7,
        AccountBalance: 8,
        CurrentNonFinancedBalance: 9,
        HouseholdCurrentNonFinancedBalance: 10
    };

    var dateFormat = 'MM/DD/YYYY';
    var PaymentOptionViewModel = function(data) {

        var self = this;
        
        // properties from model
        self.DiscountAmount = ko.observable();
        self.IsDiscountEligible = ko.observable();
        self.IsDiscountPromptPayOnly = ko.observable();
        self.DateBoundary = ko.observable();
        self.PaymentDate = ko.observable();
        self.PaymentDateDefault = "";
        self.IsDateReadonly = ko.observable();
        self.IsAmountReadonly = ko.observable();
        self.PaymentOptionId = ko.observable();
        self.PaymentOptionId.IsAccountBalance = ko.computed(function () {
            return ko.unwrap(self.PaymentOptionId) === paymentOptionEnum.AccountBalance;
        });
        self.PaymentOptionId.IsHouseholdCurrentNonFinancedBalance = ko.computed(function () {
            return ko.unwrap(self.PaymentOptionId) === paymentOptionEnum.HouseholdCurrentNonFinancedBalance;
        });
        self.PaymentOptionId.IsResubmit = ko.computed(function () {
            return ko.unwrap(self.PaymentOptionId) === paymentOptionEnum.Resubmit;
        });
        self.PaymentOptionId.IsSpecificFinancePlans = ko.computed(function () {
            return ko.unwrap(self.PaymentOptionId) === paymentOptionEnum.SpecificFinancePlans;
        });
        self.PaymentOptionId.IsSpecificVisits = ko.computed(function () {
            return ko.unwrap(self.PaymentOptionId) === paymentOptionEnum.SpecificVisits;
        });
        self.UserApplicableBalance = ko.observable();
        self.MaximumPaymentAmount = ko.observable();
        self.PaymentAmount = ko.observable().extend({ money: { minValue: 0, maxValue: self.MaximumPaymentAmount } });

        // lists from model
        self.FinancePlans = ko.observableArray([]).extend({ selectablePaymentGrid: { idKey: 'FinancePlanId', amountKey: 'PaymentAmount', defaultKey: 'CurrentFinancePlanBalanceDecimal' } });
        self.FinancePlans.AllowInput = ko.computed(function () {
            return ko.unwrap(self.PaymentOptionId.IsSpecificFinancePlans);
        });
        self.HouseholdBalances = ko.observableArray([]).extend({ selectablePaymentGrid: { idKey: 'VpStatementId', amountKey: 'TotalBalanceDecimal', defaultKey: 'TotalBalanceDecimal', setPaymentAmount: true } });
        self.HouseholdBalances.WithBalances = ko.computed(function() {
            return ko.utils.arrayFilter(self.HouseholdBalances(), function(balance) {
                return ko.unwrap(balance.TotalBalanceDecimal) > 0;
            });
        });
        self.HouseholdBalances.WithoutBalances = ko.computed(function() {
            return ko.utils.arrayFilter(self.HouseholdBalances(), function(balance) {
                return ko.unwrap(balance.TotalBalanceDecimal) <= 0;
            });
        });
        self.Payments = ko.observableArray([]).extend({ selectablePaymentGrid: { idKey: 'FinancePlanId', amountKey: 'PaymentAmount', defaultKey: 'ScheduledPaymentAmountDecimal' } });
        self.Visits = ko.observableArray([]).extend({ selectablePaymentGrid: { idKey: 'VisitId', amountKey: 'PaymentAmount', defaultKey: 'DisplayBalanceDecimal' } });
        self.Visits.AllowInput = ko.computed(function () {
            return ko.unwrap(self.PaymentOptionId.IsSpecificVisits);
        });
        
        // bucket zero
        self.AmountDueTotal = ko.pureComputed(function() {
            var sum = 0;
            ko.utils.arrayForEach(self.FinancePlans(), function(fp) {
                sum += parseFloat(ko.unwrap(fp.AmountDueDecimal));
            });
            return sum;
        });
        self.ShowPayBucketZero = ko.pureComputed(function() {
            return ko.unwrap(self.PaymentOptionId) === paymentOptionEnum.SpecificFinancePlans
                && ko.unwrap(self.HasBucketZero) === true;
        });
        self.HasBucketZero = ko.observable(false);
        self.PayBucketZero = ko.observable(false);
        self.PayBucketZero.subscribe(function(newValue) {
            if (newValue) {
                //reset to default date when it's turned on
                self.PaymentDate(self.PaymentDateDefault);
            }
        });
        self.CanSchedulePayment = function() {
            if (ko.unwrap(self.PaymentOptionId) === paymentOptionEnum.SpecificFinancePlans) {
                return ko.unwrap(self.PayBucketZero) !== undefined
                    && ko.unwrap(self.PayBucketZero) !== null
                    && ko.unwrap(self.PayBucketZero) !== true;
            }
            return true;
        }
        self.IsDateReadonlyOverride = ko.pureComputed(function() {
            if (ko.unwrap(self.PaymentOptionId) === paymentOptionEnum.SpecificFinancePlans) {
                return !self.CanSchedulePayment();
            }
            return ko.unwrap(self.IsDateReadonly);
        });

        // computed properties
        self.PaymentDate.IsPromptPay = ko.computed(function () {
            var isPromptPay = moment().isSame(moment(ko.unwrap(self.PaymentDate), dateFormat), 'day');
            return isPromptPay;
        });
        self.DiscountAmount.Calculated = ko.computed(function() {
            
            if (ko.unwrap(self.IsDiscountPromptPayOnly) === true) {
                if (ko.unwrap(self.PaymentDate.IsPromptPay) === false) {
                    return 0;
                }
            }

            if (ko.unwrap(self.PaymentOptionId) === paymentOptionEnum.HouseholdCurrentNonFinancedBalance) {
                // household
                var discountAmount = 0;
                ko.utils.arrayForEach(self.HouseholdBalances.Selected(), function (balance) {
                    if (ko.unwrap(balance.IsDiscountEligible)) {
                        discountAmount += parseFloat(ko.unwrap(balance.DiscountAmount));
                    }
                });
                return discountAmount;
            }

            // not household
            return self.DiscountAmount();

        });
        self.PaymentAmount.Calculated = ko.computed(function() {

            if (!self.IsAmountReadonly()) {
                return self.PaymentAmount();
            }

            if (ko.unwrap(self.PaymentOptionId.IsHouseholdCurrentNonFinancedBalance)) {
                var paymentAmount = 0;
                ko.utils.arrayForEach(self.HouseholdBalances.Selected(), function(balance) {
                    paymentAmount += parseFloat(balance.TotalBalanceDecimal());
                });
                paymentAmount -= self.DiscountAmount.Calculated();
                return paymentAmount;
            }

            if (ko.unwrap(self.IsDiscountEligible)) {
                var amount = parseFloat(self.UserApplicableBalance.Calculated()) - parseFloat(self.DiscountAmount.Calculated());
                return amount;
            }

            if (ko.unwrap(self.PaymentOptionId.IsSpecificFinancePlans)) {
                return self.FinancePlans.Sum();
            }

            if (ko.unwrap(self.PaymentOptionId.IsResubmit)) {
                return self.Payments.Sum();
            }

            if (ko.unwrap(self.PaymentOptionId.IsSpecificVisits)) {
                return self.Visits.Sum();
            }

            return self.UserApplicableBalance.Calculated();
        });
        self.PaymentAmount.Calculated.IsValid = ko.computed(function() {
            return !self.IsAmountReadonly() || self.PaymentAmount.Calculated() > 0;
        });
        self.UserApplicableBalance.Calculated = ko.computed(function() {
            if (ko.unwrap(self.PaymentOptionId) === paymentOptionEnum.HouseholdCurrentNonFinancedBalance) {
                // household
                return self.HouseholdBalances.Selected.Sum();
            }

            // not household
            return ko.unwrap(self.UserApplicableBalance);
        });

        // map
        var mapping = {
            'DateBoundary': {
                update: function(options) {
                    return moment.utc(options.data, dateFormat).format(dateFormat);
                }
            },
            'HouseholdBalances': {
                update: function(options) {
                    var model = ko.mapping.fromJS(options.data, {});
                    return model;
                }
            },
            'PaymentDate': {
                update: function(options) {
                    self.PaymentDateDefault = moment.utc(options.data, dateFormat).format(dateFormat);
                    return moment.utc(options.data, dateFormat).format(dateFormat);
                }
            },
            'FinancePlans': {
                create: function(options) {
                    var model = ko.mapping.fromJS(options.data);
                    model.PaymentAmount = ko.observable(0).extend({ minmaxMoney: { minValue: 0, maxValue: model.CurrentFinancePlanBalanceDecimal } });
                    return model;
                }
            },
            'Visits': {
                create: function(options) {
                    var model = ko.mapping.fromJS(options.data);
                    model.PaymentAmount = ko.observable(0).extend({ minmaxMoney: { minValue: 0, maxValue: model.DisplayBalanceDecimal } });
                    return model;
                }
            },
            'Payments': {
                create: function(options) {
                    var model = ko.mapping.fromJS(options.data);
                    var defaultPaymentAmount = ko.unwrap(model.ScheduledPaymentAmountDecimal).toFixed(2);
                    model.PaymentAmount = ko.observable(defaultPaymentAmount).extend({ minmaxMoney: { minValue: 0, maxValue: model.ScheduledPaymentAmountDecimal } });
                    return model;
                }
            }
        };
        ko.mapping.fromJS(data, mapping, self);

        //
        return self;

    };

    var PaymentViewModel = function(messenger, paymentMethods, discountsVm) {

        var self = this;

        //
        self.ActivePaymentOption = ko.observable();
        self.ActivePaymentOption.Any = ko.computed(function() {
            return ko.unwrap(self.ActivePaymentOption) && ko.unwrap(self.ActivePaymentOption().PaymentOptionId) > 0;
        });
        self.ActivePaymentOption.PostModel = function() {
            var o = self.ActivePaymentOption();

            if (ko.unwrap(o.PaymentOptionId.IsAccountBalance)) {
                o.FinancePlans.EnsureAll();
                o.Visits.EnsureAll();
            }

            return {
                PaymentOptionId: o.PaymentOptionId(),
                PaymentAmount: o.PaymentAmount.Calculated(),
                PaymentDate: o.PaymentDate(),
                PaymentMethodId: ko.unwrap(self.PaymentMethods.SelectedPaymentMethod().PaymentMethodId),
                Payments: o.Payments.AsDictionary(),
                HouseholdBalances: o.HouseholdBalances.Selected.AsDictionary(),
                FinancePlans: o.FinancePlans.AsDictionary(),
                Visits: o.Visits.AsDictionary(),
                PayBucketZero: o.PayBucketZero()
            };
        };

        //
        self.PaymentMethods = paymentMethods;
        
        //
        self.Messages = {
            Errors: ko.observableArray().extend({ extendedArray: {} })
        };

        //
        self.SetSpecificFinancePlans = function() {
            messenger.notifySubscribers(paymentOptionEnum.SpecificFinancePlans, 'setPaymentOption');
        };
        self.ShowDiscounts = function() {
            discountsVm(new window.VisitPay.DiscountsVm(ko.unwrap(self.ActivePaymentOption().DiscountGuarantorOffer), ko.unwrap(self.ActivePaymentOption().DiscountManagedOffers)));
            $('#modalDiscounts').modal('show');
        };
        self.ShowDiscountsFor = function(obj) {
            if (ko.unwrap(obj.IsManaged)) {
                discountsVm(new window.VisitPay.DiscountsVm(null, [obj.DiscountOffer]));
            } else {
                discountsVm(new window.VisitPay.DiscountsVm(obj.DiscountOffer));
            }
            $('#modalDiscounts').modal('show');
        };

        //
        self.IsValid = ko.computed(function() {
            if (!ko.unwrap(self.ActivePaymentOption) || !ko.unwrap(self.PaymentMethods.SelectedPaymentMethod)) {
                return false;
            }
            return ko.unwrap(self.PaymentMethods.SelectedPaymentMethod().PaymentMethodId) > 0 && self.ActivePaymentOption().PaymentAmount.Calculated.IsValid();
        });

        return self;

    };

    arrangePayment.PaymentModule = (function(opts) {
        

        //
        var messenger = new ko.subscribable();
        var paymentMethods = new window.VisitPay.ArrangePayment.PaymentMethods($('#section-paymentmethods'), opts.additionalParameters.currentVisitPayUserId);
        var discountsVm = ko.observable(new window.VisitPay.DiscountsVm());
        var vm = new PaymentViewModel(messenger, paymentMethods, discountsVm);

        $.each($('[data-context="payment"]'), function() {
            ko.applyBindings(vm, $(this)[0]);
        });

        $.each($('[data-context="discounts"]'), function() {
            ko.applyBindings(discountsVm, $(this)[0]);
        });

        function submitPayment() {

            var $modal = $('#modalArrangePaymentSubmit');
            $modal.modal('show');

            $modal.find('button[type=submit]').off('click').on('click', function(e) {

                e.preventDefault();
                $modal.modal('hide');
                $.blockUI();

                var postModel = { model: vm.ActivePaymentOption.PostModel() };
                $.extend(postModel, opts.additionalParameters);

                $.post(opts.submitUrl, postModel, function(result) {

                    if (result.Result === true) {
                        opts.onPaymentComplete(result.Message).done(function() {
                            $.unblockUI();
                        });
                    } else {
                        vm.Messages.Errors.push(result.Message);
                        $.unblockUI();
                    }

                });

            });

        }

        function validatePayment(e) {

            e.preventDefault();
            vm.Messages.Errors.Clear();

            if (!$(this).valid()) {
                return;
            }

            paymentMethods.Toggle(false);
            $.blockUI();

            var postModel = { model: vm.ActivePaymentOption.PostModel() };
            $.extend(postModel, opts.additionalParameters);

            $.post(opts.validateUrl, postModel, function(result) {

                $.unblockUI();

                if (result === undefined || result === null || result === true) {

                    submitPayment();

                } else {

                    if (result.Prompt === true) {
                        window.VisitPay.Common.ModalGenericConfirmationMd('Payment Alert', result.Message, 'Continue', 'Make Changes', false, { resolveAfterHidden: true }).done(submitPayment);
                    } else {
                        window.VisitPay.Common.ModalGenericAlertMd('Error', result.Message, 'Ok');
                    }

                }

            });

        }

        return {
            init: function(paymentOption) {

                vm.Messages.Errors.Clear();

                if (!paymentOption) {
                    vm.ActivePaymentOption(new PaymentOptionViewModel());
                    return;
                }
                
                var paymentOptionVm = new PaymentOptionViewModel(paymentOption);
                
                // reload payment methods on each init
                var clearSelection = paymentOption.PaymentOptionId === paymentOptionEnum.Resubmit;
                paymentMethods.ForPayment(clearSelection, paymentOptionVm.PaymentDate);
                
                // setup payment option
                vm.ActivePaymentOption(paymentOptionVm);

                if (paymentOption.PaymentOptionId === paymentOptionEnum.Resubmit) {
                    vm.ActivePaymentOption().Payments.SelectAll();
                } else if (paymentOption.PaymentOptionId === paymentOptionEnum.HouseholdCurrentNonFinancedBalance) {
                    vm.ActivePaymentOption().HouseholdBalances.SelectAll();
                }

                // bind events
                var $scope = $('#payment-submit form');
                $scope.find('#PaymentDate').makeDatepicker(moment.utc(paymentOption.DateBoundary, dateFormat));
                $scope.find('.numeric').makeNumericInput();
                $scope.parseValidation();
                $scope.off('submit').on('submit', validatePayment);

            },
            messenger: messenger
        }

    });

})(window.VisitPay.ArrangePayment = window.VisitPay.ArrangePayment || {});
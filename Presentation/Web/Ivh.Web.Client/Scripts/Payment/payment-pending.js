﻿window.VisitPay.PaymentsPending = (function() {

    var self = {};

    var hasInitialized = false;
    var grid = null;
    var model = {
        CurrentVisitPayUserId: null,
        VisitPayUserId: null,
        VpGuarantorId: null
    };

    function refreshPendingAndHistory() {
        self.RefreshGrid();
        var historyGrid = $('#jqGridHistory');
        historyGrid.sortGrid(historyGrid.jqGrid('getGridParam', 'sortname'), true, historyGrid.jqGrid('getGridParam', 'sortorder'));
    };

    function getColumnModel() {
        var colModel = [
                { label: 'Date', name: 'ScheduledPaymentDate', width: 76 },
                { label: 'Payment Made By', name: 'MadeByGuarantorName', width: 174 },
                { label: 'Payment Made For', name: 'GuarantorName', width: 174 },
                { label: 'Payment Type', name: 'PaymentType', width: 140 },
                {
                    label: 'Payment Method', name: 'PaymentMethod', width: 207, formatter: function (cellValue, options, rowObject) {
                        if (rowObject.PaymentMethod != null) {
                            return rowObject.PaymentMethod.DisplayName;
                        }
                        return '';
                    }
                },
                { label: 'Amount', name: 'ScheduledPaymentAmount', width: 80, classes: 'text-right' },
                {
                    label: 'Take Action',
                    name: '',
                    width: 140,
                    classes: 'text-center',
                    formatter: function (cellValue, options, rowObject) {
                        return '<a name="payment-edit" title="Edit Payment" href="javascript:;">Edit Payment</a>';
                    }
                }
            ];

        for (var i = 0; i < colModel.length; i++) {
            var col = colModel[i];
            col.resizable = false;
            col.sortable = false;
        }

        return colModel;
    };

    function loadComplete(data) {
        var gridContainer = grid.parents('.ui-jqgrid:eq(0)');
        var gridNoRecords = $('#jqGridNoRecordsPending');

        if (data.Results != null && data.Results.length > 0) {

            gridContainer.show();
            gridNoRecords.removeClass('in');

            grid.find('.jqgrow').each(function() {

                var rowData = data.Results[$(this).index() - 1];

                $(this).find('a[name="payment-edit"]').off('click').on('click', function(e) {
                    e.preventDefault();
                    window.VisitPay.PaymentEdit.Initialize(rowData.PaymentId, ko.unwrap(model.VpGuarantorId), ko.unwrap(model.CurrentVisitPayUserId)).done(function (hasChanged) {
                        if (hasChanged) {
                            refreshPendingAndHistory();
                            $(document).trigger('financeplans.reload');
                        }
                    });
                });

            });

        } else {
            gridContainer.hide();
            gridNoRecords.addClass('in');
        }

        $.unblockUI();
    };

    function gridComplete() {
        grid.find('[data-toggle="tooltip"]').tooltip({ animation: false, container: 'body' });  
        var gridContainer = grid.parents('.ui-jqgrid:eq(0)');
        gridContainer.addClass('visible');
    };

    function onExport(e) {
        e.preventDefault();
        $.post('/Payment/PaymentPendingExport', {
            CurrentVisitPayUserId: model.CurrentVisitPayUserId,
            VpGuarantorId: model.VpGuarantorId
        }, function(result) {
            if (result.Result === true)
                window.location.href = result.Message;
        }, 'json');
    };

    self.Setup = function(currentVisitPayUserId, filteredVisitPayUserId, vpGuarantorId, accountClosed) {

        model.CurrentVisitPayUserId = currentVisitPayUserId;
        model.VisitPayUserId = filteredVisitPayUserId || currentVisitPayUserId;
        model.VpGuarantorId = vpGuarantorId;
        model.accountClosed = accountClosed;

        $.jgrid.gridUnload('#jqGridPending');

        hasInitialized = false;

    };
    
    self.RefreshGrid = function() {
        grid.trigger('reloadGrid');
    };

    self.Initialize = function() {

        if (hasInitialized)
            return;
        else {
            $.blockUI();
            hasInitialized = true;
        }

        //
        grid = $('#jqGridPending');
        
        //
        $(document).off('paymentsPending.reload').on('paymentsPending.reload', self.RefreshGrid);
        $('#btnPaymentPendingExport').off('click').on('click', onExport);
        
        //
        grid.jqGrid({
            autowidth: false,
            url: '/Payment/PaymentPending',
            jsonReader: { root: 'Results' },
            pager: 'jqGridPagerPending',
            postData: {
                CurrentVisitPayUserId: model.CurrentVisitPayUserId,
                VpGuarantorId: model.VpGuarantorId
            },
            pgbuttons : false,
            pginput : false,
            pgtext : '',
            rowList: false,
            loadComplete: loadComplete,
            gridComplete: gridComplete,
            sortname: '',
            sortorder: '',
            colModel: getColumnModel()
        });
    };

    return self;

})();
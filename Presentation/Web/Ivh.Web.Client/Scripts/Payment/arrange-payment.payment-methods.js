﻿(function () {
    window.VisitPay.ArrangePayment = window.VisitPay.ArrangePayment || {};
    window.VisitPay.ArrangePayment.PaymentMethods = (function ($scope, currentVisitPayUserId) {

        var self = {};
        
        function formatDisplay(target, withImage) {

            var str = '<strong>' + ko.unwrap(target.DisplayName) + '</strong>';

            withImage = withImage === undefined || withImage === null ? true : withImage;
            if (!withImage) {
                return str;
            }

            var html = '<img src="' + ko.unwrap(target.DisplayImage) + '" alt=""/>' + str;

            if (target.PaymentMethodProviderType &&
                target.PaymentMethodProviderType.DisplayImage &&
                ko.unwrap(target.PaymentMethodProviderType.DisplayImage).length > 0) {
                return '<img class="logo-provider" src="' + ko.unwrap(target.PaymentMethodProviderType.DisplayImage) + '" alt=""/>' + html;
            }

            return html;
        }

        function load(allowSelection) {

            var promise = $.Deferred();
            
            $scope.off();
            $scope.hide();
            $scope.html('');

            // the view will return with different functionality based on parameters
            $.get('/Payment/PaymentMethodsListPartial?allowSelection=' + allowSelection + '&currentVisitPayUserId=' + currentVisitPayUserId, function (html) {
                
                ko.cleanNode($scope[0]);
                $scope.html(html);
                ko.applyBindings(self, $scope[0]);
                
                var opts = {
                    suppressPrimaryNotification: true
                };

                window.VisitPay.PaymentMethods(self, $scope, opts).done(function() {
                    promise.resolve();
                });
            });

            return promise;
        }

        function setSingleUse(date) {
            if (!date || !ko.unwrap(date)) {
                self.PaymentMethods.AllowSingleUse(false);
                return;
            }

            var isPromptPay = moment().isSame(moment(ko.unwrap(date), 'MM/DD/YYYY'), 'day');
            self.PaymentMethods.AllowSingleUse(isPromptPay);
        }
        
        self.PaymentMethods = new window.VisitPay.PaymentMethodsViewModel(currentVisitPayUserId);

        // selected
        self.SelectedPaymentMethod = ko.computed(function() {
            return ko.unwrap(self.PaymentMethods.SelectedPaymentMethod);
        });
        self.SelectedPaymentMethod.Display = ko.computed(function() {
            var selected = ko.unwrap(self.SelectedPaymentMethod);
            if (!selected) {
                return 'Select a Payment Method';
            }
            return formatDisplay(selected);
        });
        
        // primary
        self.PrimaryPaymentMethod = ko.computed(function() {
            return ko.unwrap(self.PaymentMethods.PrimaryPaymentMethod());
        });
        self.PrimaryPaymentMethod.Any = ko.computed(function() {
            var primary = ko.unwrap(self.PrimaryPaymentMethod);
            return primary && ko.unwrap(primary.PaymentMethodId) > 0;
        });
        self.PrimaryPaymentMethod.Display = ko.computed(function() {
            var primary = ko.unwrap(self.PrimaryPaymentMethod);
            if (!primary) {
                return 'Select a Payment Method';
            }
            return formatDisplay(primary);
        });
        self.PrimaryPaymentMethod.DisplayShort = ko.computed(function() {
            var primary = ko.unwrap(self.PrimaryPaymentMethod);
            if (!primary) {
                return 'Select a Payment Method';
            }
            return formatDisplay(primary, false);
        });
        
        //
        self.ForFinancePlan = function () {
            
            var promise = $.Deferred();
            
            load(false).done(function () {
                promise.resolve();

                // watch primary
                self.PrimaryPaymentMethod.subscribeChanged(function (newValue, oldValue) {
                    if (oldValue && newValue && parseInt(ko.unwrap(oldValue.PaymentMethodId)) !== parseInt(ko.unwrap(newValue.PaymentMethodId))) {
                        self.Toggle(false);
                    }
                });

            });

            return promise;
        };
        self.ForPayment = function (clearSelection, paymentDateObservable) {

            var promise = $.Deferred();

            load(true).done(function() {

                if (clearSelection) {
                    self.PaymentMethods.SelectedPaymentMethod(null);
                }

                // if nothing selected, default to primary
                if (!ko.unwrap(self.PaymentMethods.SelectedPaymentMethod)) {
                    self.PaymentMethods.SelectedPaymentMethod(ko.unwrap(self.PrimaryPaymentMethod));
                }

                // set single use
                if (paymentDateObservable) {
                    setSingleUse(ko.unwrap(paymentDateObservable));

                    if (ko.isObservable(paymentDateObservable)) {
                        paymentDateObservable.subscribe(setSingleUse);
                    }
                }

                // watch selected
                self.SelectedPaymentMethod.subscribeChanged(function (newValue, oldValue) {
                    if (oldValue && newValue && parseInt(ko.unwrap(oldValue.PaymentMethodId)) !== parseInt(ko.unwrap(newValue.PaymentMethodId))) {
                        self.Toggle(false);
                    }
                });

                // click handler for selecting already selected
                $scope.off('click.account-selected').on('click.account-select', '.account-item label', function () {
                    self.Toggle(false);
                });

                promise.resolve();
            });

            return promise;
            
        };

        //
        self.Toggle = function (show) {
            if (show === true) {
                $scope.slideDown(200, 'swing');
            } else if (show === false) {
                $scope.slideUp(200, 'swing');
            } else {
                $scope.slideToggle(200, 'swing');
            }

            setTimeout(function () {
                $('html, body').animate({scrollTop:$(document).height()});
            }, 200);
        };

        //
        return self;

    });

})();
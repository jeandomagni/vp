﻿(function (arrangePayment) {

    var paymentOptionEnum = {
        Resubmit: 1,
        FinancePlan: 4,
        SpecificAmount: 5,
        SpecificVisits: 6,
        SpecificFinancePlans: 7,
        AccountBalance: 8,
        CurrentNonFinancedBalance: 9,
        HouseholdCurrentNonFinancedBalance: 10
    };

    var PaymentMenuViewModel = function () {

        var self = this;
        var allMenuItems = [];

        //
        function map(source, setSelected) {

            allMenuItems = [];
            ko.utils.arrayForEach(source.PaymentMenuItems, function (item) {
                allMenuItems.push(item);
                if (item.PaymentMenuItems && item.PaymentMenuItems.length > 0) {
                    allMenuItems.push.apply(allMenuItems, item.PaymentMenuItems);
                }
            });

            //
            ko.mapping.fromJS(source.PaymentMenuItems, {}, self.PaymentMenuItems);
            if (setSelected) {
                self.SelectedPaymentMenuItemId(source.SelectedPaymentMenuItemId);
            } else {
                self.SelectedPaymentMenuItemId(null);
            }

            //
            self.HasOptionsEnabled(ko.utils.arrayFirst(allMenuItems, function (item) {
                return item.IsEnabled === true;
            }));

        };

        // selected
        self.SelectedPaymentMenuItemId = ko.observable();
        self.SelectedPaymentMenuItemId.subscribe(function(newValue) {

            var selectedId = ko.unwrap(newValue);
            var foundItem = ko.utils.arrayFirst(allMenuItems, function(item) {
                return item.PaymentMenuItemId === selectedId;
            });

            if (!foundItem) {
                self.PaymentMenuItems.Selected(null);
                return;
            }

            var paymentMenuItems = foundItem.PaymentMenuItems;

            if (paymentMenuItems && paymentMenuItems.length > 0) {
                var firstEnabledItem = ko.utils.arrayFirst(ko.unwrap(paymentMenuItems), function (item) { return ko.unwrap(item.IsEnabled) === true; });
                if (firstEnabledItem) {
                    self.SelectedPaymentMenuItemId(ko.unwrap(firstEnabledItem.PaymentMenuItemId));
                }
            } else {
                self.PaymentMenuItems.Selected(ko.utils.arrayFirst(allMenuItems, function(item) {
                    return item.PaymentMenuItemId === selectedId;
                }));
            }

        });

        // menu items
        self.PaymentMenuItems = ko.observableArray([]).extend({ extendedArray: {} });
        self.PaymentMenuItems.Selected = ko.observable();
        self.PaymentMenuItems.Selected.Any = ko.computed(function () {
            var selected = ko.unwrap(self.PaymentMenuItems.Selected);
            return selected !== undefined && selected !== null;
        });
        self.PaymentMenuItems.Selected.Is = function (paymentMenuItemId) {
            if (!self.PaymentMenuItems.Selected.Any()) {
                return false;
            }

            var checkValue = ko.unwrap(paymentMenuItemId);
            var selected = self.PaymentMenuItems.Selected();

            return checkValue === ko.unwrap(selected.PaymentMenuItemId) || checkValue === ko.unwrap(selected.ParentPaymentMenuItemId);
        };
        self.PaymentMenuItems.Selected.Parent = ko.computed(function () {

            var selected = ko.unwrap(self.PaymentMenuItems.Selected);
            if (!selected) {
                return {};
            }

            var parentId = ko.unwrap(selected.ParentPaymentMenuItemId);
            if (!$.isNumeric(parentId)) {
                return selected;
            }

            var parentItem = ko.utils.arrayFirst(allMenuItems, function (item) {
                return item.PaymentMenuItemId === parentId;
            });
            return parentItem || {};

        });
        self.HasOptionsEnabled = ko.observable(false);

        //
        self.Init = function (data, setSelected) {
            map(data, setSelected);
            return $.Deferred().resolve();
        };
        self.SetPaymentOption = function (paymentOptionId) {
            if (paymentOptionId === undefined || paymentOptionId === null) {
                // select first enabled
                ko.utils.arrayFirst(allMenuItems, function (item) {
                    if (item.IsEnabled === true) {
                        self.SelectedPaymentMenuItemId(item.PaymentMenuItemId);
                        return true;
                    }
                    return false;
                });
            } else {
                // select specified
                ko.utils.arrayFirst(allMenuItems, function (item) {
                    if (item.IsEnabled === true && item.PaymentOptionId === ko.unwrap(paymentOptionId)) {
                        self.SelectedPaymentMenuItemId(item.PaymentMenuItemId);
                        return true;
                    }
                    return false;
                });
            }

        };

        //
        return self;

    };

    var ReceiptViewModel = function() {

        var self = this;
        self.messenger = new ko.subscribable();

        self.HomeButtonText = ko.observable();
        self.ReceiptText = ko.observable();
        self.ShowMakeAnotherPayment = ko.observable();
        self.IsVisible = ko.computed(function() {
            return ko.unwrap(self.ReceiptText) != undefined && ko.unwrap(self.ReceiptText).length > 0;
        });

        self.GoHome = function () {
            self.messenger.notifySubscribers(null, 'goHome');
        };
        self.SetDefaultPaymentOption = function () {
            self.messenger.notifySubscribers(null, 'setPaymentOption');
        };

        self.Clear = function() {
            self.ReceiptText('');
            self.ShowMakeAnotherPayment(true);
        }
        self.Clear();

        return self;

    };

    arrangePayment.Main = (function (data) {

        function setAdditionalParameters(sourceData) {
            var p = {};
            if (sourceData.VisitPayUserId !== undefined && sourceData.VisitPayUserId !== null) {
                p['currentVisitPayUserId'] = data.VisitPayUserId;
            }
            return p;
        };

        var paymentMenuVm = new PaymentMenuViewModel();
        var receiptVm = new ReceiptViewModel();
        var additionalParameters = setAdditionalParameters(data);

        function showReceipt(receiptText, hasEnabledOptions) {
            $('.arrange-payment-module').hide();
            $('#receipt').show();
            receiptVm.Clear();
            receiptVm.ReceiptText(receiptText);
            receiptVm.HomeButtonText(data.HomeButtonText);
            receiptVm.ShowMakeAnotherPayment(ko.unwrap(hasEnabledOptions));
        };

        function goHome() {
            window.location.href = data.HomeUrl;
        };

        function refreshData() {

            var promise = $.Deferred();

            $.post(data.DataUrl, additionalParameters, function (newData) {

                data = newData;
                additionalParameters = setAdditionalParameters(data);

                var promiseAlerts = $.Deferred();
                promiseAlerts.done(function () {
                    paymentMenuVm.Init(newData.PaymentMenu, false);
                    promise.resolve();
                });

                if ($.isFunction(window.VisitPay.Common.RefreshAlerts)) {
                    window.VisitPay.Common.RefreshAlerts().done(function () { promiseAlerts.resolve(); });
                } else {
                    promiseAlerts.resolve();
                }

            });

            return promise;

        };

        function paymentComplete(receiptText) {

            var promise = $.Deferred();
            refreshData().done(function() {
                showReceipt(receiptText, paymentMenuVm.HasOptionsEnabled());
                promise.resolve();
            });

            return promise;

        };

        $(document).ready(function () {

            // payment menu
            $.each($('[data-context="menu"]'), function () {
                ko.applyBindings(paymentMenuVm, $(this)[0]);
            });

            // receipt
            $.each($('[data-context="receipt"]'), function () {
                ko.applyBindings(receiptVm, $(this)[0]);
            });
            
            // payment module
            var paymentModule = new arrangePayment.PaymentModule({
                submitUrl: data.PaymentSubmitUrl,
                validateUrl: data.PaymentValidateUrl,
                onPaymentComplete: paymentComplete,
                additionalParameters: additionalParameters
            });

            // finance plan module
            var financePlanModule = new arrangePayment.FinancePlanModule({
                optionUrl: data.OptionUrl,
                submitUrl: data.FinancePlanSubmitUrl,
                validateUrl: data.FinancePlanValidateUrl,
                homeUrl: data.HomeUrl,
                onPaymentComplete: paymentComplete,
                additionalParameters: additionalParameters,
                isOfflineGuarantor: data.IsOfflineGuarantor
            });

            //finance plan state selection module
            var financePlanStateSelectionModule = new arrangePayment.FinancePlanStateSelectionModule({});

            var selectPaymentOption = function(newValue) {
                // reset all modules
                paymentModule.init();
                financePlanModule.init();
                receiptVm.Clear();
                financePlanStateSelectionModule.init();

                if (!newValue) {
                    return;
                }

                var selectedPaymentOptionId = ko.unwrap(newValue.PaymentOptionId);
                if (!selectedPaymentOptionId) {
                    return;
                }

                if (selectedPaymentOptionId !== paymentOptionEnum.FinancePlan) {
                    //Non-FP option selected
                    var selectedPaymentOption = ko.utils.arrayFirst(data.PaymentOptions, function(item) {
                        return item.PaymentOptionId === selectedPaymentOptionId;
                    });

                    if (selectedPaymentOption) {
                        // hide modules
                        $('.arrange-payment-module').hide();

                        // init payment module
                        $('#payment-submit-container').show();
                        paymentModule.init(selectedPaymentOption);

                        $('[data-toggle="tooltip"]').tooltip({ animation: false, container: 'body' });
                        $('[data-toggle="tooltip-html"]').tooltipHtml();

                    }
                } else {
                    //FP option selected
                    var financePlanPaymentOptions = [];

                    //If a specific state code is provided, choose that. Otherwise get all FP options
                    if (newValue.FinancePlanOptionStateCode) {
                        //Specific state selected
                        var fpOptionForState = ko.utils.arrayFirst(data.PaymentOptions, function(item) {
                            return item.PaymentOptionId === selectedPaymentOptionId && item.FinancePlanOptionStateCode === newValue.FinancePlanOptionStateCode;
                        });
                        financePlanPaymentOptions.push(fpOptionForState);
                    } else {
                        ko.utils.arrayForEach(data.PaymentOptions, function(item) {
                            if (item.PaymentOptionId === selectedPaymentOptionId) {
                                financePlanPaymentOptions.push(item);
                            }
                        });
                    }

                    //If more than one FP option, load state selection module. Otherwise load regular FP module
                    if (financePlanPaymentOptions.length > 1) {
                        //Hide modules
                        $('.arrange-payment-module').hide();

                        //Have multiple state options, load state selection module
                        $('#finance-plan-state-selection').show();
                        financePlanStateSelectionModule.init(financePlanPaymentOptions, ko.unwrap(paymentMenuVm.financePlanOffer));
                    } else {
                        // hide modules
                        $('.arrange-payment-module').hide();

                        //Only have FPs for a single state, load FP module
                        $('#finance-plan').show();
                        financePlanModule.init(financePlanPaymentOptions[0]);
                    }
                }
            };

            var setPaymentOptionWithReset = function(paymentOptionId) {
                // reset all modules
                paymentModule.init();
                financePlanModule.init();
                receiptVm.Clear();
                financePlanStateSelectionModule.init();

                paymentMenuVm.SetPaymentOption(paymentOptionId);
            };

            // subscribe to things
            paymentModule.messenger.subscribe(setPaymentOptionWithReset, null, 'setPaymentOption');
            receiptVm.messenger.subscribe(setPaymentOptionWithReset, null, 'setPaymentOption');
            receiptVm.messenger.subscribe(goHome, null, 'goHome');
            financePlanStateSelectionModule.messenger.subscribe(selectPaymentOption, null, 'financePlanStateSelected');
            financePlanModule.messenger.subscribe(selectPaymentOption, null, 'financePlanChangeState');

            // subscribe to menu change
            paymentMenuVm.PaymentMenuItems.Selected.subscribe(selectPaymentOption);

            // init
            $('.arrange-payment-module').hide();

            paymentMenuVm.Init(data.PaymentMenu, true).done(function() {
                if (!paymentMenuVm.HasOptionsEnabled()) {
                    $('#noOptions').show();
                }
                $('#arrange-payment').css('visibility', 'visible');
            });

            function guarantorFilterChanged() {
                $.blockUI();
                window.location.reload();
            };

            // on guarantor filter change
            if (window.VisitPay.GuarantorFilter && $.isFunction(window.VisitPay.GuarantorFilter.OnFilterChanged)) {
                window.VisitPay.GuarantorFilter.OnFilterChanged = guarantorFilterChanged;
            }

        });

    });

})(window.VisitPay.ArrangePayment = window.VisitPay.ArrangePayment || {});
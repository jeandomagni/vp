﻿(function (arrangePayment) {
    
    function formatDecimal(value) {
        if (value === undefined || value === null || isNaN(value)) {
            return value;
        }
        return parseFloat(value).toFixed(2);
    }

    var FinancePlanViewModel = function(initialData, paymentMethods, visitPayUserId, vpGuarantorId, messenger) {
        
        var self = new window.VisitPay.FinancePlanSetupBaseViewModel(initialData);
        var initialSelectedVisits = [];
        var initialCommitedVisits = [];
        var initialUserApplicableBalance;
        var initialAmountToFinance;
        
        //
        self.IsVisible = ko.observable(false);
        
        // user
        self.VpGuarantorId = ko.observable(getUrlVar('VpGuarantorId'));
        self.CurrentVisitPayUserId = ko.observable(visitPayUserId);
        if (vpGuarantorId != null) {
            self.VpGuarantorId(vpGuarantorId);
        }

        // basics
        self.AmountToFinance = ko.observable();
        self.FinancePlanId = ko.observable();
        self.FinancePlanTotalMonthlyPaymentAmount = ko.observable();
        self.HasPendingResubmitPayments = ko.observable();
        self.StatementId = ko.observable();
        self.UserApplicableBalance = ko.observable();
        self.FinancePlanOptionStateCode = ko.observable(null);
        self.FinancePlanOptionStateName = ko.observable(null);
        self.OtherFinancePlanStatesAreAvailable = ko.observable(false);
        self.PaymentOptionId = ko.observable(null);
        
        // visit selections
        self.Visits = ko.observableArray();
        self.Visits.SelectAll = function () {
            ko.utils.arrayForEach(ko.unwrap(self.Visits), function (visit) {
                self.SelectedVisits.push(ko.unwrap(visit.VisitId));
            });
        };
        self.SelectedVisits = ko.observableArray();
        self.CommitedVisits = ko.observableArray();
        
        self.ShowVisitSelection = function () {
            var vpGuarantorId = self.VpGuarantorId();
            var currentVisitPayUserId = self.CurrentVisitPayUserId();
            $.post("/FinancePlan/VisitSelectionForFinancePlan", { vpGuarantorId: vpGuarantorId, currentVisitPayUserId: currentVisitPayUserId }, function (result) {
                if (result.IsError === true) {
                    showErrorMessage(result.ErrorMessage);
                } else {
                    self.Visits(result.visits);
                    if (ko.unwrap(self.SelectedVisits).length === 0) {
                        self.Visits.SelectAll();
                    }
                    $('#modalVisitSelection').modal('show');
                }
            });
        };
        self.CancelVisitSelection = function () {
            self.SelectedVisits([]);
            self.SelectedVisits(self.CommitedVisits.slice(0));
            $('#modalVisitSelection').modal('hide');
        };
        self.ApplyVisitSelection = function () {
            var amtToFinance = computeAmtToFinance(0);

            self.AmountToFinance(amtToFinance.toFixed(2).toString());
            self.CommitedVisits([]);
            self.CommitedVisits(self.SelectedVisits.slice(0));
            self.ResetValues();
            var vpGuarantorId = self.VpGuarantorId();
            var selectedVisits = self.SelectedVisits();
            var statementId = self.StatementId();
            var financePlanOfferSetTypeId = self.FinancePlanOfferSetTypeId();
            var combineFinancePlans = self.Option.CombineFinancePlans();
            $.post("/FinancePlan/GetFinancePlanOfferBoundary", { vpGuarantorId: vpGuarantorId, statementId: statementId, financePlanOfferSetTypeId: financePlanOfferSetTypeId, combineFinancePlans:combineFinancePlans, selectedVisits:selectedVisits }, function (result) {
                if (result.IsError === true) {
                    showErrorMessage(result.ErrorMessage);
                } else {
                    self.MaximumMonthlyPayments(result.financePlanBoundaryDto.MaximumNumberOfPayments);
                    self.MinimumPaymentAmount(result.financePlanBoundaryDto.MinimumPaymentAmount);
                    self.InterestRates(result.interestRates);
                }
            });
            self.Terms().ErrorMessage([]);
            $('#modalVisitSelection').modal('hide');
        }

        function computeAmtToFinance(defaultAmount) {
            if (self.Visits().length <= 0) {
                return defaultAmount;
            }
            var curBal = self.Option.ActiveFinancePlanBalance();
            var visitsBal = 0;
            ko.utils.arrayForEach(self.Visits(), function (visit) {
                if (self.SelectedVisits.indexOf(visit.VisitId) >= 0) {
                    visitsBal = visitsBal + parseFloat(visit.DisplayBalanceDecimal);
                }
            });
            self.UserApplicableBalance(visitsBal.toString());
            var amtToFinance = visitsBal + (self.Option.CombineFinancePlans() ? parseFloat(curBal) : 0);
            return amtToFinance;
        };

        function resetVisitSelections() {
            self.SelectedVisits(initialSelectedVisits.slice(0));
            self.CommitedVisits(initialCommitedVisits.slice(0));
            self.UserApplicableBalance(initialUserApplicableBalance);
            self.AmountToFinance(initialAmountToFinance);
        }

        function getUrlVar(key) {
            var vars = {};
            window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi, function (m, k, value) {
                vars[k.toUpperCase()] = value;
            });
            return vars[key.toUpperCase()];
        }

        function showErrorMessage(message) {
            window.VisitPay.Common.ModalGenericAlertMd('An Error Has Occurred', message, 'Ok').always(true);

        };

        // payment due day
        self.PaymentDueDay = ko.observable();
        self.PaymentDueDay.IsActive = ko.observable(false);
        self.PaymentDueDay.IsEnabled = ko.observable(false);
        self.PaymentDueDay.Toggle = function() {
            self.PaymentDueDay.IsActive(ko.unwrap(self.PaymentDueDay.IsActive) ^ true);
        };
        
        // esign
        self.EsignCmsVersionId = ko.observable();
        self.EsignCmsVersionId.IsChecked = ko.observable(false);

        // type - required for offline finance plans, null otherwise
        self.FinancePlanTypes = ko.observableArray([]).extend({ extendedArray: {} });
        self.FinancePlanType = ko.observable();
        self.FinancePlanType.RequireAgreement = ko.computed(function () {
            var obj = ko.unwrap(self.FinancePlanType);
            return obj != null && (obj.RequireCreditAgreement || obj.RequireEsign);
        });
        self.FinancePlanType.subscribe(function (newValue) {
            // clear any accepted agreements when this value changes
            self.EsignCmsVersionId.IsChecked(false);
            self.TermsCmsVersionId.IsChecked(false);
        });

        // payment methods
        self.PaymentMethods = paymentMethods;
        self.PaymentMethodsIsEnabled = ko.computed(function () {
            if (self.PaymentMethods == null) {
                return false;
            }

            var fpType = ko.unwrap(self.FinancePlanType);

            if (fpType == null) {
                return false;
            }

            return fpType.IsPaymentMethodEditable;
        });
        
        // options
        self.Options = ko.observableArray([]);
        self.Option = ko.observable();
        self.Option.Is = function (s) {
            var option = ko.unwrap(self.Option);
            if (option === undefined || option === null) {
                return false;
            }
            return parseInt(option) === parseInt(s);
        };
        self.Option.HasOptions = ko.computed(function () {
            return ko.unwrap(self.Options).length > 1;
        });
        self.Option.CombineFinancePlans = ko.computed(function () {
            return self.Option.Is(1);
        });
        self.Option.ShowOptions = ko.computed(function () {
            if (ko.unwrap(self.Option.HasOptions)) {
                return true;
            }
            return false;
        });
        self.Option.ActiveFinancePlanBalance = ko.computed(function () {
            var combined = ko.unwrap(self.Option.CombineFinancePlans);
            var active = ko.utils.arrayFirst(ko.unwrap(self.Options), function (o) {
                return ko.unwrap(o.IsCombined) === combined;
            });
            if (active !== undefined && active !== null) {
                return ko.unwrap(active.ActiveFinancePlanBalance);
            }
            return 0;
        });
        
        //
        self.IsMenuVisible = ko.computed(function () {
            return true;
        });
        self.IsSubmitEnabled = ko.computed(function () {
            var financePlanType = ko.unwrap(self.FinancePlanType);
            if (financePlanType == null) {
                return true;
            }
            var validate = [];
            if (financePlanType.RequireCreditAgreement === true) {
                validate.push(ko.unwrap(self.TermsCmsVersionId.IsChecked));
            }
            if (financePlanType.RequireEsign === true) {
                validate.push(ko.unwrap(self.EsignCmsVersionId.IsChecked));
            }
            return validate.every(function(b) { return b === true; });
        });

        //
        self.Messages = {
            Errors: ko.observableArray().extend({ extendedArray: {} })
        };

        //
        self.SubmitModel = function() {
            var model = $.extend({}, self.SubmitModelBase(), {
                CombineFinancePlans: ko.unwrap(self.Option.CombineFinancePlans),
                FinancePlanId: ko.unwrap(self.FinancePlanId),
                StatementId: ko.unwrap(self.StatementId),
                SelectedVisits: ko.unwrap(self.SelectedVisits),
                FinancePlanOptionStateCode: ko.unwrap(self.FinancePlanOptionStateCode)
            });

            var financePlanType = ko.unwrap(self.FinancePlanType);
            if (financePlanType != null) {

                $.extend(model, { FinancePlanTypeId: ko.unwrap(financePlanType.FinancePlanTypeId) });

                if (ko.unwrap(financePlanType.RequireCreditAgreement) === true) {
                    $.extend(model, { TermsCmsVersionId: ko.unwrap(self.TermsCmsVersionId) });
                }

                if (ko.unwrap(financePlanType.RequireEsign) === true) {
                    $.extend(model, { EsignCmsVersionId: ko.unwrap(self.EsignCmsVersionId) });
                }
            }

            if (ko.unwrap(self.PaymentDueDay.IsEnabled)) {
                $.extend(model, { PaymentDueDay: ko.unwrap(self.PaymentDueDay) });
            }

            return model;
        };
        self.ValidateModel = function() {
            var model = $.extend({}, self.ValidateModelBase(), {
                StatementId: ko.unwrap(self.StatementId),
                CombineFinancePlans: ko.unwrap(self.Option.CombineFinancePlans),
                PaymentDueDay: ko.unwrap(self.PaymentDueDay),
                SelectedVisits: ko.unwrap(self.SelectedVisits),
                FinancePlanOptionStateCode: ko.unwrap(self.FinancePlanOptionStateCode),
                OtherFinancePlanStatesAreAvailable: ko.unwrap(self.OtherFinancePlanStatesAreAvailable)
        });
            return model;
        };
        
        //
        var baseMapFunction = self.Map;
        self.Map = function(data) {
            
            self.SetOptions(data);
            
            var isCombined = ko.unwrap(self.Option.CombineFinancePlans);
            var activeOption = ko.utils.arrayFirst(ko.unwrap(self.Options), function (o) {
                return ko.unwrap(o.IsCombined) === isCombined;
            });
            baseMapFunction(activeOption);
            
            self.HasPendingResubmitPayments(data.HasPendingResubmitPayments);
            self.FinancePlanTotalMonthlyPaymentAmount(data.FinancePlanTotalMonthlyPaymentAmount);
            self.IsVisible(!data.HasPendingResubmitPayments);
            self.UserApplicableBalance(data.UserApplicableBalance);
            
            initialSelectedVisits = [];
            initialCommitedVisits = [];

            data.SelectedVisits.forEach(function(visit) {
                initialSelectedVisits.push(visit.VisitId);
                initialCommitedVisits.push(visit.VisitId);
            });
            initialUserApplicableBalance = data.UserApplicableBalance;
            var amountToFinance = data.AmountToFinance;
            if (!isCombined) {
                amountToFinance += data.UserApplicableBalance;
            }
            initialAmountToFinance = amountToFinance;
            resetVisitSelections();
            
            if (activeOption != null) {
                var o = ko.unwrap(activeOption);
                if (o != null) {
                    self.EsignCmsVersionId(ko.unwrap(o.EsignCmsVersionId));
                    if (ko.unwrap(o.IsEditable) === false) {
                        self.OfferCalculationStrategy(ko.unwrap(o.OfferCalculationStrategy));
                    }
                }
            }

            self.SetOption();
            
            // pdd
            self.PaymentDueDay(data.SuggestedPaymentDueDay);
            self.PaymentDueDay.IsActive(false);
            self.PaymentDueDay.IsEnabled(data.CanSetPaymentDueDay);

            //FP State
            self.FinancePlanOptionStateCode(data.FinancePlanOptionStateCode);
            self.FinancePlanOptionStateName(data.FinancePlanOptionStateName);
            self.OtherFinancePlanStatesAreAvailable(data.OtherFinancePlanStatesAreAvailable);
            self.PaymentOptionId(data.PaymentOptionId);
        };
        self.SetOptions = function(data) {
            // combo/stack options
            var hasCombinedOption = data.FinancePlanCombinedConfiguration != null;
            var hasStackedOption = data.FinancePlanDefaultConfiguration != null;

            var options = [];
            if (hasStackedOption) {
                var stackedOption = data.FinancePlanDefaultConfiguration;
                stackedOption.IsCombined = false;
                stackedOption.Value = 0;
                options.push(stackedOption);
            }

            if (hasCombinedOption) {
                var combinedOption = data.FinancePlanCombinedConfiguration;
                combinedOption.IsCombined = true;
                combinedOption.Value = 1;
                options.push(combinedOption);
            }

            options.sort(function(left, right) {
                return parseInt(left.DisplayOrder) > parseInt(right.DisplayOrder) ? 1 : -1;
            });

            self.Options(options);
            
            if (ko.unwrap(self.Option.HasOptions)) {

                var hasCombinedPendingFinancePlan = hasCombinedOption && data.FinancePlanCombinedConfiguration.Terms.FinancePlanOfferId !== undefined && parseInt(data.FinancePlanCombinedConfiguration.Terms.FinancePlanOfferId) > 0;
                var hasDefaultPendingFinancePlan = hasStackedOption && data.FinancePlanDefaultConfiguration.Terms.FinancePlanOfferId !== undefined && parseInt(data.FinancePlanDefaultConfiguration.Terms.FinancePlanOfferId) > 0;

                if (hasDefaultPendingFinancePlan) {
                    self.Option(0);
                    return;
                } else if (hasCombinedPendingFinancePlan) {
                    self.Option(1);
                    return;
                }
            }

            self.Option(hasCombinedOption ? 1 : 0);
        };
        self.SetOption = function() {
            var isCombined = self.Option.CombineFinancePlans();
            var found = ko.utils.arrayFirst(ko.unwrap(self.Options), function(o) {
                return ko.unwrap(o.IsCombined) === isCombined;
            });
            var config = ko.mapping.toJS(found);
            if (config == null) {
                config = {};
            }
            
            // config data
            resetVisitSelections();
            self.AmountToFinance(config.AmountToFinance);
            self.FinancePlanId(config.FinancePlanId);
            if (parseInt(config.FinancePlanOfferSetTypeId) !== parseInt(ko.unwrap(self.FinancePlanOfferSetTypeId))) {
                self.FinancePlanOfferSetTypeId(config.FinancePlanOfferSetTypeId);
            }
            self.FinancePlanOfferSetTypes(config.FinancePlanOfferSetTypes);
            self.FinancePlanTypes(config.FinancePlanTypes);
            self.InterestRates(config.InterestRates);
            self.IsEditable(config.IsEditable);
            self.MaximumMonthlyPayments(config.MaximumMonthlyPayments);
            self.MinimumPaymentAmount(config.MinimumPaymentAmount);
            self.StatementId(config.StatementId);
            self.TermsCmsVersionId(config.TermsCmsVersionId);
            self.UpdateFromTerms(config);

            if (!self.Terms().IsValid()) {
                var monthlyPaymentAmount = config.SuggestedMonthlyPaymentAmount;
                var numberMonthlyPayments = config.SuggestedNumberMonthlyPayments;

                if (monthlyPaymentAmount && numberMonthlyPayments) {
                    self.UserMonthlyPaymentAmount(formatDecimal(monthlyPaymentAmount));
                    self.UserNumberMonthlyPayments(numberMonthlyPayments);
                }
            }

            //Reset terms alert timeout
            self.TermsAlertTimeoutMet(false);
        };    
        self.SetSubscriptions = function() {
            self.Option.subscribe(function(newValue) {
                if (newValue == null) {
                    return;
                }
                self.SetOption();
                self.ResetValues();
            });
            self.OfferCalculationStrategy.subscribe(self.ResetValues);
        };

        self.ChangeStateClicked = function() {
            log.debug('Change state clicked');
            self.FinancePlanOptionStateCode(null);
            messenger.notifySubscribers(ko.toJS(self), 'financePlanChangeState');
        };
        
        return self;

    };

    arrangePayment.FinancePlanModule = (function(opts) {
        
        var messenger = new ko.subscribable();

        // payment methods
        var $paymentMethods = $('#section-paymentmethods');
        var paymentMethodsModule = null;
        if (opts.enablePaymentMethods === true) {
            paymentMethodsModule = new window.VisitPay.ArrangePayment.PaymentMethods($paymentMethods, opts.additionalParameters.currentVisitPayUserId);
        }

        // model
        var vm = new FinancePlanViewModel({}, paymentMethodsModule, opts.additionalParameters.currentVisitPayUserId, self.FilterValue.FilteredVpGuarantorId, messenger);
        
        $.each($('[data-context="financeplan"]'), function() {
            ko.applyBindings(vm, $(this)[0]);
        });

        var offerTypeSubscription = null;
        
        function onOfferTypeChange() {

            if (ko.unwrap(vm.PaymentMethodsIsEnabled) === true) {
                vm.PaymentMethods.Toggle(false);
            }

            offerTypeSubscription.dispose();
            $.blockUI();

            var validateModel = vm.ValidateModel();
            $.extend(validateModel, opts.additionalParameters);
            $.post(opts.optionUrl, validateModel, function(data) {
                vm.Map(data);
                vm.ResetValues();
                offerTypeSubscription = vm.FinancePlanOfferSetTypeId.subscribe(onOfferTypeChange);
                $.unblockUI();
            });

        };
        
        function onPaymentDueDayChange() {
            vm.PaymentDueDay.Toggle();
            if (!ko.unwrap(vm.HasPaymentParameters)) {
                // don't submit if the rest of the form isn't filled out
                return;
            }
            var validateModel = vm.ValidateModel();
            $.extend(validateModel, opts.additionalParameters);
            $.post(opts.validateUrl, validateModel, function(result) {
                if (result.IsError === true) {
                    // if changing the pdd results in invalid terms, clear the form
                    vm.ResetValues();
                    vm.UpdateBoundaries(result);
                } else {
                    // terms are valid, just update the terms
                    vm.UpdateFromTerms(result);
                }
                window.VisitPay.FinancePlanSetupCommon.SetTooltips($('#finance-plan'));
            });
        };
       
        function handleSubmitResult(result) {
            if (result.Result === true) {
                opts.onPaymentComplete(result.Message).done(function() {
                    $.unblockUI();
                });
            } else {
                vm.Messages.Errors.push(result.Message);
                $.unblockUI();
            }
        }

        function submitFinancePlan(e) {

            e.preventDefault();

            if (!ko.unwrap(vm.IsSubmitEnabled)) {
                return;
            }

            $.blockUI();

            var submitModel = { model: vm.SubmitModel() };
            $.extend(submitModel, opts.additionalParameters);
            
            var preProcess;
            if (opts.preProcess) {
                // map to json, from json, to js, to get a plain JS object without observables
                var dto = ko.mapping.toJS(ko.mapping.fromJSON(ko.mapping.toJSON(vm)));
                preProcess = opts.preProcess(dto);
            } else {
                preProcess = $.Deferred().resolve();
            }

            preProcess.done(function () {
                $.post(opts.submitUrl, submitModel, handleSubmitResult);
            }).fail(function () {
                $.unblockUI();
            });

        };
        
        //
        return {
            init: function(paymentOption) {
                
                if (offerTypeSubscription != null) {
                    offerTypeSubscription.dispose();
                }

                var $scope = $('#finance-plan');

                vm.Messages.Errors.Clear();
                vm.Option(null);
                
                if (paymentOption == null) {
                    vm.IsVisible(false);
                    return;
                }
                
                //
                $scope.css('visibility', 'hidden');

                //
                if (paymentMethodsModule != null) {
                    // reload payment methods on each init
                    paymentMethodsModule.ForFinancePlan();
                }
                
                // setup
                vm.Map(paymentOption);
                vm.SetSubscriptions();

                // offer type
                offerTypeSubscription = vm.FinancePlanOfferSetTypeId.subscribe(onOfferTypeChange);
                
                // common
                window.VisitPay.FinancePlanSetupCommon.BindEvents($scope, vm, opts.validateUrl, opts.additionalParameters);
                
                // click events
                $scope.off('click.confirm').on('click.confirm', '#btnConfirmFinancePlan', submitFinancePlan);
                $scope.off('change.pdd').on('change.pdd', '#PaymentDueDay', onPaymentDueDayChange);

                // show
                $('#finance-plan').css('visibility', 'visible');

            },
            messenger: messenger
        };

    });

})(window.VisitPay.ArrangePayment = window.VisitPay.ArrangePayment || {});
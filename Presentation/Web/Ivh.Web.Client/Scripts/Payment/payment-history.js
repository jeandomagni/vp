﻿window.VisitPay.PaymentHistory = (function (paymentDetail, common, $, ko) {

    var paymentHistory = {};

    var filterViewModel = function () {

        var self = this;

        self.PaymentId = ko.observable('');
        self.PaymentDateRangeFrom = ko.observable('');
        self.PaymentDateRangeTo = ko.observable('');
        self.PaymentMethodId = ko.observable('');
        self.PaymentStatusId = ko.observable('');
        self.PaymentTypeId = ko.observable('');
        self.VpGuarantorId = ko.observable();
        self.CurrentVisitPayUserId = ko.observable();
        self.VisitPayUserId = ko.observable();
        self.TransactionRange = ko.observable('');
        self.TransactionId = ko.observableArray('');

        self.RunResults = ko.observable(false);
        self.ChangeNotifications = function () {

            var setRun = function () {
                self.RunResults(true);
            };

            self.PaymentId.subscribe(setRun);
            self.PaymentDateRangeFrom.subscribe(setRun);
            self.PaymentDateRangeTo.subscribe(setRun);
            self.PaymentMethodId.subscribe(setRun);
            self.PaymentStatusId.subscribe(setRun);
            self.PaymentTypeId.subscribe(setRun);
            self.TransactionRange.subscribe(setRun);
            self.TransactionId.subscribe(setRun);

        };

    };

    var hasInitialized = false;
    var model;

    function bindFilters() {

        // prevent focus on validate failure to prevent calendar from opening
        $('#jqGridFilterHistory').on('invalid-form.validate', function (form, validator) {
            validator.settings && (validator.settings.focusInvalid = false);
        });

        $('#section-paymenthistory .input-group.date').each(function () {
            var element = $(this);
            element.datepicker({
                clearBtn: true,
                format: 'mm/dd/yyyy',
                orientation: 'top left',
                autoclose: true
            }).on('hide', function () {
                if ($('#jqGridFilterHistory').valid()) {
                    common.ResetUnobtrusiveValidation($('#jqGridFilterHistory'));
                }
            });
        });
    }
    function resetModel() {
        // reset viewmodel
        model.PaymentId('');
        model.PaymentDateRangeFrom('');
        model.PaymentDateRangeTo('');
        model.PaymentMethodId('');
        model.PaymentStatusId('');
        model.PaymentTypeId('');
        model.TransactionRange('');
        model.TransactionId('');
        model.RunResults(false);
    }
    function resetFilters() {
        resetModel();
        common.ResetUnobtrusiveValidation($('#jqGridFilterHistory'));
    }
    function setNumericInput($element) {
        $element.autoNumeric('init', { aSep: '', lZero: 'deny' });
        $element.addClass('numeric');
        $element.on('keydown', function (e) {
            if (e.keyCode === 188)
                e.preventDefault();
        });
        $element.off('focus').focus(function () { $(this).select(); });
    }

    paymentHistory.Setup = function (currentVisitPayUserId, filteredVisitPayUserId, vpGuarantorId, accountClosed) {

        if (!model) {
            model = new filterViewModel();
            ko.applyBindings(model, $('#jqGridFilterHistory')[0]);
            model.ChangeNotifications();
        }

        model.CurrentVisitPayUserId(currentVisitPayUserId);
        model.VisitPayUserId(filteredVisitPayUserId || currentVisitPayUserId);
        model.VpGuarantorId(vpGuarantorId);
        model.accountClosed = accountClosed;

        bindFilters();
        resetModel();
        resetFilters();

        $.jgrid.gridUnload('#jqGridHistory');

        $(document).on('visitUnmatchActionComplete', function () {
            if (hasInitialized) {

                var grid = $('#jqGridHistory');
                $.extend(grid.jqGrid('getGridParam', 'postData'), { model: ko.toJS(model) });
                grid.setGridParam({ page: 1 });
                grid.sortGrid(grid.jqGrid('getGridParam', 'sortname'), true, grid.jqGrid('getGridParam', 'sortorder'));

            }
        });

        hasInitialized = false;

    };
    paymentHistory.Initialize = function() {

        if (hasInitialized)
            return;
        else {
            $.blockUI();
            hasInitialized = true;
        }

        //
        var grid = $('#jqGridHistory');
        var gridDefaultSortOrder = 'desc';
        var gridDefaultSortName = 'InsertDate';

        //
        var reloadGrid = function() {
            $.extend(grid.jqGrid('getGridParam', 'postData'), {
                model: ko.toJS(model),
                currentVisitPayUserId: model.CurrentVisitPayUserId(),
                vpGuarantorId: model.VpGuarantorId()
            });
            grid.setGridParam({ page: 1 });
            grid.sortGrid(gridDefaultSortName, true, gridDefaultSortOrder);
        };
        
        //
        $('#jqGridFilterHistory').off('submit').on('submit', function(e) {
            e.preventDefault();
            if ($(this).valid()) {
                $.blockUI();
                reloadGrid();
            };
        });
        $('#btnGridFilterResetHistory').off('click').on('click', function (e) {
            $.blockUI();
            resetFilters();
            reloadGrid();
        });
        $('#btnPaymentHistoryExport').off('click').on('click', function (e) {

            e.preventDefault();
            $.post('/Payment/PaymentHistoryExport', {
                model: ko.toJS(model),
                CurrentVisitPayUserId: model.CurrentVisitPayUserId(),
                vpGuarantorId: model.VpGuarantorId(),
                sidx: grid.jqGrid('getGridParam', 'sortname'),
                sord: grid.jqGrid('getGridParam', 'sortorder')
            }, function(result) {
                if (result.Result === true)
                    window.location.href = result.Message;
            }, 'json');

        });

        $(document).off('paymentHistory.reload').on('paymentHistory.reload', reloadGrid);

        //
        resetFilters();

        //
        var showPreviousConsolidationModal = function () {
            $.blockUI();
            $.get('/Payment/PreviousConsolidationModal', function (cmsVersion) {
                $.unblockUI();
                common.ModalGenericAlertMd(cmsVersion.ContentTitle, cmsVersion.ContentBody, 'OK');
            });
        };
        var showReversalResult = function(result) {
            if (result.Success) {
                if (window.VisitPay.PaymentsPending != undefined && window.VisitPay.PaymentsPending.RefreshGrid != undefined && typeof window.VisitPay.PaymentsPending.RefreshGrid === 'function') {
                    window.VisitPay.PaymentsPending.RefreshGrid();
                }
                common.ModalGenericAlertMd('Message', 'The confirmation number for this transaction is ' + (result.Message ? result.Message : 'N/A') + '.<br /><br />A receipt has been emailed to the Guarantor.', 'Ok');
            } else {
                common.ModalGenericAlertMd('Message', 'Your request cannot be processed at this time.<br /><br />Please try again or contact your service provider for more information', 'Ok');
            }
        };
        var showRefundModal = function (rowData, vpGuarantorId) {
            if (model.accountClosed) {
                common.ModalGenericAlertMd('Message', 'This account has been canceled. To void or refund this payment, please go to the hospital billing system of record.', 'Ok');
                return;
            }
            var $modal = $('#modalPaymentRefund');
            $modal.off('click');
            var $alert = $modal.find('.alert');
            var $input = $modal.find('#refundAmount');

            $modal.find('#refundOriginalAmount').text(rowData.SnapshotTotalPaymentAmount);
            $modal.find('#refundNetAmount').text(rowData.NetPaymentAmount);
            $alert.addClass('hidden').text('');
            $input.removeClass('input-validation-error');

            var netPaymentAmount = parseFloat(rowData.NetPaymentAmount.toString().replace(/\$/g, '').replace(/-/g, '').replace(/,/g, '')).toFixed(2);

            setNumericInput($modal.find('#refundAmount'));
            $input.val(netPaymentAmount);
            $input.off('keyup').on('keyup', function() {
                var val = parseFloat($(this).val().toString().replace(/\$/g, '').replace(/-/g, '').replace(/,/g, ''));
                if (val > parseFloat(netPaymentAmount))
                    $(this).val(netPaymentAmount);
            });

            var isValid = function () {

                var amount = parseFloat($modal.find('#refundAmount').val());
                var error = '';

                if (isNaN(amount))
                    error = 'Please enter an amount to be refunded';
                else if (amount > netPaymentAmount)
                    error = 'The amount entered exceeds the payment amount that can be refunded.';
                else if (amount < 0.01)
                    error = 'The amount entered is not valid.';

                if (error.length > 0) {
                    $alert.text(error);
                    $alert.removeClass('hidden');
                    $input.addClass('input-validation-error');
                    return false;
                } else {
                    $input.removeClass('input-validation-error');
                    return true;
                }
            };

            $modal.on('click', 'button[type=submit]', function(e) {
                e.preventDefault();

                if (isValid()) {
                    $modal.modal('hide');
                    $.blockUI();
                    $.post('/Payment/PaymentRefund', { VpGuarantorId: vpGuarantorId, paymentProcessorResponseId: rowData.PaymentProcessorResponseId, amount: $input.val() }, function (result) {
                        $.unblockUI();
                        reloadGrid();
                        showReversalResult(result);
                    });
                }
            });
            $modal.modal('show');

        };
        var showVoidModal = function (rowData, vpGuarantorId) {
            if (model.accountClosed) {
                common.ModalGenericAlertMd('Message', 'This account has been canceled. To void or refund this payment, please go to the hospital billing system of record.', 'Ok');
                return;
            }
            common.ModalGenericConfirmation('Void Payment', 'Confirm payment void of <strong>' + rowData.SnapshotTotalPaymentAmount + '</strong>.', 'Confirm', 'Cancel').done(function () {
                $.blockUI();
                $.post('/Payment/PaymentVoid', { VpGuarantorId: vpGuarantorId, paymentProcessorResponseId: rowData.PaymentProcessorResponseId }, function (result) {
                    $.unblockUI();
                    reloadGrid();
                    showReversalResult(result);
                });
            });

        };
        var showUnallocatedModal = function (rowData, vpGuarantorId) {
            common.ModalGenericAlertMd('Payment has unallocated funds', 'There are unallocated funds associated with this payment. Unallocated funds are not available for refund in ' + window.VisitPay.ClientSettings.ClientBrandName + '. To refund this payment, including any and all unallocated funds, please go to the hospital billing system of record.', 'Ok').done(function () {
            });
        };
        var showUnmatchReversalModal = function(rowData) {

            var message = 'A visit has been unmatched from this account since this payment was made. ' +
                'These funds are not available for void or refund in ' + window.VisitPay.ClientSettings.ClientBrandName + '. ' +
                'To void or refund this payment, please go to the hospital billing system of record.';

            common.ModalGenericAlertMd('Action Unavailable', message, 'Ok');

        };

        //
        grid.jqGrid({
            autowidth: false,
            url: '/Payment/PaymentHistory',
            jsonReader: { root: 'Payments' },
            postData: {
                model: ko.toJS(model),
                currentVisitPayUserId: model.CurrentVisitPayUserId(),
                vpGuarantorId: model.VpGuarantorId()
            },
            pager: 'jqGridPagerHistory',
            sortname: gridDefaultSortName,
            sortorder: gridDefaultSortOrder,
            loadComplete: function(data) {

                var gridContainer = grid.parents('.ui-jqgrid:eq(0)');
                var gridNoRecords = $('#jqGridNoRecordsHistory');

                if (data.Payments != null && data.Payments.length > 0) {

                    gridContainer.show();
                    gridNoRecords.removeClass('in');
                    
                    grid.find('.jqgrow a.details').on('click', function(e) {

                        e.preventDefault();

                        var row = $(this).parents('tr');
                        var rowIndex = row.index('#jqGridHistory tr') - 1;
                        var rowData = data.Payments[rowIndex];

                        $.post('/Payment/PaymentDetail', { paymentProcessorResponseId: rowData.PaymentProcessorResponseId, vpGuarantorId: model.VpGuarantorId(), currentVisitPayUserId: model.CurrentVisitPayUserId() }, function(html) {

                            $('#modalPaymentDetailContainer').html(html);
                            $('#modalPaymentDetailContainer').find('.modal').modal();
                            paymentDetail.Initialize(rowData.PaymentProcessorResponseId, model.VpGuarantorId(), model.CurrentVisitPayUserId());

                            $.unblockUI();

                        });

                    });
                    grid.find('.jqgrow select').on('change', function (e) {

                        e.preventDefault();
                        
                        var row = $(this).parents('tr');
                        var rowIndex = row.index('#jqGridHistory tr') - 1;
                        var rowData = data.Payments[rowIndex];

                        if ($(this).val() === 'resubmit') {
                            window.location.href = '/Payment/ArrangePayment?VpGuarantorId=' + model.VpGuarantorId() + '&paymentOption=Resubmit';

                            // void/refund
                        } else if (rowData.HasRemovedVisits) {

                            showUnmatchReversalModal(rowData);

                        } else if (rowData.UnallocatedAmountSum > 0) {

                            showUnallocatedModal(rowData, model.VpGuarantorId());

                        }
                        else if ($(this).val() === 'void')
                            if (rowData.PreviousConsolidation)
                                showPreviousConsolidationModal();
                            else
                                showVoidModal(rowData, model.VpGuarantorId());
                        else if ($(this).val() === 'refund')
                            if (rowData.PreviousConsolidation)
                                showPreviousConsolidationModal();
                            else
                                showRefundModal(rowData, model.VpGuarantorId());
                        else if ($(this).val() === 'refund-returned-mailed-check') {
                            if (rowData.PreviousConsolidation)
                                showPreviousConsolidationModal();
                            else
                                showRefundModal(rowData, model.VpGuarantorId());
                        }

                        $(this).val('');

                    });

                } else {
                    gridContainer.hide();
                    gridNoRecords.addClass('in');
                }

                model.RunResults(false);
                $.unblockUI();

            },
            gridComplete: function() {

                grid.find('[data-toggle="tooltip"]').tooltip({ animation: false, container: 'body' });

                $('#jqGridPagerHistory').find('#input_jqGridPager').find('input').on('change', function() {
                    grid.jqGrid().trigger('reloadGrid', [{ page: $(this).val() }]);
                });

                var gridContainer = grid.parents('.ui-jqgrid:eq(0)');
                gridContainer.addClass('visible');

            },
            colModel: [
                {
                    label: 'ID',
                    name: 'IdDisplay',
                    width: 62,
                    resizable: false,
                    formatter: function(cellValue, options, rowObject) {

                        if (rowObject.PaymentStatusId === 6)
                            return '<a class="details" href="javascript:;" title="View Details">' + cellValue + '</a>';
                        else
                            return cellValue;
                    }
                },
                { label: 'Date', name: 'InsertDate', width: 76, resizable: false },
                { label: 'Payment Made By', name: 'MadeByGuarantorName', width: 124, sortable: false },
                { label: 'Payment Made For', name: 'GuarantorName', width: 128 },
                {
                    label: 'Type',
                    name: 'PaymentType',
                    width: 144,
                    resizable: false,
                    formatter: function(cellValue, options, rowObject) {
                        var html = '';
                        if (rowObject.ActionTakenBy != null && rowObject.ActionTakenBy.length > 0) {
                            html += '<a class="glyphicon glyphicon-info-sign" href="javascript:;" title="Action Taken By: ' + rowObject.ActionTakenBy + '" data-placement="right" data-toggle="tooltip" style="float: right; margin-top: 3px;"></a>';
                        }
                        return html + cellValue;
                    }
                },
                { label: 'Payment Method', name: 'PaymentMethod', width: 116, resizable: false, formatter: function(cellValue, options, rowObject) {
                    if (rowObject.PaymentMethod != null) {
                        return rowObject.PaymentMethod.DisplayName;
                    }
                    return '';
                } },
                { label: 'Transaction<br />Amount', name: 'SnapshotTotalPaymentAmount', resizable: false, width: 82, classes: 'text-right' },
                { label: 'Net Payment<br />Amount', name: 'NetPaymentAmount', resizable: false, width: 82, classes: 'text-right' },
                {
                    label: 'Status', name: 'PaymentStatusMessage', width: 95, resizable: false, formatter: function (cellValue, options, rowObject) {
                        if (rowObject.IsCancelled) { // cancelled
                            return '<span class="cancelled">' + cellValue + '</span>';
                        }
                        return cellValue;
                    }
                },
                {
                    label: 'Take Action',
                    name: '',
                    width: 80,
                    resizable: false,
                    sortable: false,
                    classes: 'text-center',
                    formatter: function (cellValue, options, rowObject) {

                        var opts = [];

                        if (!rowObject.CanTakeAction)
                            return 'N/A';

                        if (rowObject.PaymentReversalStatus === 2 && rowObject.CanVoid)
                            opts.push({ value: 'void', text: 'Void' });
                        if (rowObject.PaymentReversalStatus === 3) {
                            if (rowObject.PaymentRefundableType === 2 && rowObject.CanRefundReturnedCheck) {
                                opts.push({ value: 'refund-returned-mailed-check', text: 'Returned Mailed-in Check' });    
                            }
                            else if (rowObject.PaymentRefundableType === 1 && rowObject.CanRefund) {
                                opts.push({ value: 'refund', text: 'Refund' });
                            }
                        }

                        if (opts.length > 0) {
                            var html = '';
                            for (var i = 0; i < opts.length; i++) {
                                html += '<option value="' + opts[i].value + '">' + opts[i].text + '</option>';
                            }
                            return '<select class="form-control"><option value="">-</option>' + html + '</select>';
                        }

                        return 'N/A';
                    }
                }
            ]
        });
    };

    return paymentHistory;

})(window.PaymentDetail = window.PaymentDetail || {}, window.VisitPay.Common = window.VisitPay.Common || {}, jQuery, window.ko);
﻿window.VisitPay.PaymentCancel = (function () {

    function viewModel() {

        var self = this;

        //
        self.CmsCancel = ko.observable();
        self.PaymentCancelReasonId = ko.observable();
        self.PaymentCancelDescription = ko.observable();
        self.PaymentCancelReasons = ko.observable();
        self.PaymentCancelReasons.IsRequireInput = ko.computed(function () {

            if (!ko.unwrap(self.PaymentCancelReasonId)) {
                self.PaymentCancelDescription('');
                return false;
            }

            var selectedItem = ko.utils.arrayFirst(ko.unwrap(self.PaymentCancelReasons), function (item) {
                return parseInt(ko.unwrap(item.PaymentActionReasonId)) === parseInt(ko.unwrap(self.PaymentCancelReasonId));
            });

            if (!selectedItem) {
                self.PaymentCancelDescription('');
                return false;
            }

            if (!ko.unwrap(selectedItem.RequireInput)) {
                self.PaymentCancelDescription('');
                return false;
            }

            return true;

        });

        //
        self.IsRecurringPayment = ko.observable(false);
        self.PaymentType = ko.observable();
        self.PaymentDueDate = ko.observable();
        self.AmountDuePrior = ko.observable();
        self.AmountDueAfter = ko.observable();

        return self;

    };

    function submit($form, model, paymentId, vpGuarantorId) {

        var promise = $.Deferred();

        $form.parseValidation();

        if (!$form.valid()) {
            promise.reject();
            return promise;
        }

        $.blockUI();

        var postModel = {
            PaymentId: paymentId,
            PaymentCancelReasonId: ko.unwrap(model.PaymentCancelReasonId),
            PaymentCancelDescription: ko.unwrap(model.PaymentCancelDescription),
            PaymentType: ko.unwrap(model.PaymentType),
            VpGuarantorId: vpGuarantorId
        };

        $.post('/Payment/PaymentCancelSubmit', postModel, function (result) {
            promise.resolve();
            $.unblockUI();
        });

        return promise;

    }

    function loadData(isRecurringPayment, model, paymentId, vpGuarantorId) {

        var promise = $.Deferred();

        var postModel = {
            IsRecurringPayment: isRecurringPayment,
            VpGuarantorId: vpGuarantorId
        };

        if (paymentId != null) {
            postModel.PaymentId = paymentId;
        }

        $.post('/Payment/PaymentCancel', postModel, function (data) {
            ko.mapping.fromJS(data, {}, model);
            promise.resolve();
        });

        return promise;

    };

    function loadModal() {

        var promise = $.Deferred();

        window.VisitPay.Common.ModalNoContainerAsync('modalPaymentCancel', '/Payment/PaymentCancel', 'GET', null, false).done(function ($modal) {

            promise.resolve($modal);

        });

        return promise;
    };

    function initialize(isRecurringPayment, paymentId, vpGuarantorId) {
        $.blockUI();

        var model = new viewModel();
        var promise = $.Deferred();
        var promiseData = loadData(isRecurringPayment, model, paymentId, vpGuarantorId);
        var promiseModal = loadModal();

        $.when(promiseData, promiseModal).done(function(data, $modal) {

            $.unblockUI();
            $modal.modal('show');

            //
            ko.applyBindings(model, $modal[0]);

            var $form = $modal.find('form');
            $form.parseValidation();
            $form.on('submit', function (e) {
                e.preventDefault();
            });

            //
            $modal.on('click', '.modal-footer button[type=submit]', function (e) {
                e.preventDefault();
                submit($form, model, paymentId, vpGuarantorId).done(function () {
                    $modal.modal('hide');
                    promise.resolve();
                });
            });

            $modal.on('click', '[data-dismiss="modal"]', function (e) {
                e.preventDefault();
                $modal.modal('hide');
                promise.reject();
            });

        });

        return promise;
    }

    return {
        Initialize: function (isRecurringPayment, paymentId, vpGuarantorId) {
            return initialize(isRecurringPayment, paymentId, vpGuarantorId);
        }
    };

})();
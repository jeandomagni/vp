﻿window.PaymentDetail = (function(common, $, ko) {

    var self = {};

    self.Initialize = function(paymentProcessorResponseId, guarantorId, currentVisitPayUserId) {

        //
        var scope = $('#modalPaymentDetail');
        scope.find('[data-toggle="tooltip"]').tooltip({ animation: false, container: 'body' });
        scope.find('.modal').css('position', 'absolute');
        scope.find('#btnPaymentDetailExport').on('click', function(e) {
            e.preventDefault();
            $.post('/Payment/PaymentDetailExport', {
                paymentProcessorResponseId: paymentProcessorResponseId,
                currentVisitPayUserId: currentVisitPayUserId,
                vpGuarantorId: guarantorId
            }, function(result) {
                if (result.Result === true)
                    window.location.href = result.Message;
            }, 'json');
        });

    };

    return self;

})(window.VisitPay.Common = window.VisitPay.Common || {}, jQuery, window.ko);
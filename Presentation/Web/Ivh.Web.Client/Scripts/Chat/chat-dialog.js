﻿window.VisitPay = window.VisitPay || {};

VisitPay.ChatDialog = (function ($) {
    var self = {};
    var dialogContainerId = '#chatDialog';
    var guarantorEndedChat = false;

    self.SetupDialog = function (dialogData) {
        var model = new VisitPay.Dialog.Model();
        model.Height = 500;
        model.Width = 400;
        model.MinWidth = 350; //Should be the min width for the chat-panel
        model.MinHeight = 350;
        model.Resizable = false;
        model.Data = dialogData;

        //Load up the dialog, but don't show it
        return loadDialog(model, false);
    };

    self.ShowActiveDialog = function () {
        //Don't want to show dialog on the chat queue page.
        if (window.location.href.indexOf('Chat') > -1 && window.location.href.indexOf('ChatAdmin') === -1) {
            //On the chat page
            return;
        }

        //Not on chat page, show dialog
        var activeDialogModel = self.ActiveDialogModel();
        if (activeDialogModel != null) {
            log.debug('have active chat dialog, reloading it');
            
            loadDialog(activeDialogModel, true).done(function (dialogContainer) {
                //Setup iframe event listeners
                setupIframeListeners();

                //Setup iframe
                $('#chatDialogIframe').attr('src', activeDialogModel.Data.iframeSrc);

                //Need this to give chat dialog focus when iframe is clicked
                addEventListener('blur', function () {
                    if (document.activeElement === document.getElementById('chatDialogIframe')) {
                        $(dialogContainerId).dialog('moveToTop');
                    }
                });
            });
        }
    };

    self.DialogIsActive = function () {
        return VisitPay.Dialog.DialogIsActive(dialogContainerId);
    };

    self.ActiveDialogModel = function () {
        return VisitPay.Dialog.GetActiveDialogModel(dialogContainerId);
    };

    self.CloseActiveDialog = function () {
        VisitPay.Dialog.CloseActiveDialog(dialogContainerId);

        if ($(dialogContainerId).dialog('instance')) {
            $(dialogContainerId).dialog('close');
        }
    };

    function setupIframeListeners() {
        //Listen for events from chat iframe
        window.addEventListener('message', function (event) {
            if (event) {
                if (event.data === 'chatClosed') {
                    //Tell chat indicator to update
                    $(window).trigger('UpdateChatIndicator');
                } else if (event.data === 'chatThreadClosed') {
                    log.debug('chat thread closed event');
                    guarantorEndedThread();
                }
            }
        });
    }

    function postMessageToIframe(message) {
        $('#chatDialogIframe')[0].contentWindow.postMessage(message, '*');
    }

    function guarantorEndedThread() {
        //Track that guarantor ended so we don't prompt when closing the dialog
        guarantorEndedChat = true;
    }

    var beforeClose = function () {
        var closePromise = $.Deferred();

        if (!guarantorEndedChat) {
            var confirmationPromise = VisitPay.Common.ModalGenericConfirmation('End Thread', 'Are you sure you want to end this chat thread?', 'End Thread', 'Cancel');
            confirmationPromise.done(function() {
                //Get the thread data from the dialog
                var dialogModel = VisitPay.ChatDialog.ActiveDialogModel();
                if (dialogModel) {
                    var threadId = dialogModel.Data.threadId;
                    var guarantorToken = dialogModel.Data.guarantorChatToken;

                    //Log end thread
                    var postData = {
                        chatThreadId: threadId,
                        guarantorChatToken: guarantorToken
                    };
                    $.post('/Chat/EndThread', postData)
                        .done(function(data) {
                            log.debug('Called END THREAD with response: ', data);
                        });

                    //End the thread
                    postMessageToIframe('endChatRequest');
                }

                //Let dialog finish cleanup
                closePromise.resolve();
            }).fail(function() {
                //Didn't want to end thread, show dialog
                self.ShowActiveDialog();
                closePromise.reject();
            });
        } else {
            //End the thread
            postMessageToIframe('endChatRequest');

            closePromise.resolve();
        }

        return closePromise;
    };

    function loadDialog(dialogModel, showAfterLoad) {
        var fullTitle = 'Chatting With: ' + dialogModel.Data.title;

        var dialogCreatePromise = VisitPay.Dialog.CreateAsync(dialogContainerId, dialogModel, fullTitle, '/Chat/ChatDialog', 'GET', showAfterLoad, null, null, null, null, beforeClose);
        return dialogCreatePromise;
    }

    return self;

})(jQuery);
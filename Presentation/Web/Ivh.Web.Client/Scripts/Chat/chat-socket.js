﻿
window.VisitPay.ChatSocket = (function ($) {
    var chatSocket;
    var operatorAvailabilityTimer; //Timer for pinging socket with availability
    var availabilityPingDelay = 20000; //millisecond delay for pinging the socket to tell it we are online
    var operatorAvailableStatusKey = 'operatorAvailableStatus'; //Key in session storage to store whether the operator has set themselves available
    var socketReconnectDelay = 5000; //Delay in ms that should be used when attempting to reconnect a failed socket connection

    var urls = {
        ChatSocket: VisitPay.ChatSettings.ChatSocketUrl,
        IsChatAvailable: '/Chat/IsChatAvailable'
    };

    var socketMessageReceived = function (message) {
        log.debug('Socket received message from server ', message.data);

        var messageJson = JSON.parse(message.data);
        var messageBody = JSON.parse(messageJson.Message);
        switch (messageJson.MessageType) {
            case VisitPay.ChatSettings.SocketMessageTypes.ThreadClosed:
                log.debug('received close thread message from socket!', messageBody);
                //Send event to listeners
                $(window).trigger('ChatThreadClosed', [messageBody]);
                break;

            case VisitPay.ChatSettings.SocketMessageTypes.ThreadDetails:
                log.debug('received thread details message from socket!', messageBody);
                //Send event to listeners
                $(window).trigger('ChatThreadReceived', [messageBody]);
                break;

            case VisitPay.ChatSettings.SocketMessageTypes.OperatorPickedUpThread:
                log.debug('received operator picked up thread message from socket!', messageBody);
                //Send event to listeners
                $(window).trigger('OperatorPickedUpThread', [messageBody]);
                break;
        }
    };

    function isChatAvailable() {
        //Check if chat is available
        var availablePromise = $.Deferred();
        $.post(urls.IsChatAvailable, function (response) {
            log.debug('Chat Available Response: ', response);
            availablePromise.resolve(response.IsAvailable);
        }).fail(function() {
            availablePromise.fail();
        });

        return availablePromise;
    }

    function startChatSocket() {
        var chatSocketStartedPromise = $.Deferred();

        if (!isSocketOpen()) {
            chatSocket = new WebSocket(urls.ChatSocket);

            //Connection opened
            chatSocket.addEventListener('open', function (event) {
                log.debug('chat socket open');
                //Start polling socket to let it know we are online, if operator is available
                if (isOperatorStatusAvailable()) {
                    startOperatorAvailablePinging();
                }

                //Successfully started socket
                chatSocketStartedPromise.resolve();

                //Listen for logout
                listenForLogout();
            });

            //Connection Closed
            chatSocket.addEventListener('close', function (event) {
                log.debug('chat socket close', event);
                stopOperatorAvailablePinging();

                //Attempt to reconnect if socket had an unclean close
                if (event && !event.wasClean) {
                    log.error('chat socket had an unclean close. start reconnect attempts');
                    setTimeout(function () {
                        log.warn('attempting to reconnect to chat socket...');
                        startChatSocket();
                    }, socketReconnectDelay);
                }
            });

            chatSocket.onerror = function (event) {
                log.error("WebSocket error observed:", event);
            };

            //Listen for messages
            chatSocket.addEventListener('message', socketMessageReceived);
        }

        return chatSocketStartedPromise;
    }

    function isSocketOpen() {
        return chatSocket && chatSocket.readyState === 1;
    }

    var pingSocketWithAvailability = function (availableOverride) {
        if (isSocketOpen()) {
            //Use override, if provided. Else use the operator's stored availability status.
            var availabilityToUse = availableOverride != null ? availableOverride : isOperatorStatusAvailable();
            var payload = {
                MessageType: VisitPay.ChatSettings.SocketMessageTypes.OperatorConnect,
                Message: JSON.stringify({
                    UserToken: VisitPay.ChatSettings.OperatorChatToken,
                    IsConnected: availabilityToUse,
                    FirstName: VisitPay.ChatSettings.OperatorFirstName,
                    LastName: VisitPay.ChatSettings.OperatorLastName
                })
            };
            chatSocket.send(JSON.stringify(payload));
        }
    };

    function startOperatorAvailablePinging() {
        if (isSocketOpen() && !operatorAvailabilityTimer) {
            //Do initial online call, then ping on timer
            pingSocketWithAvailability();
            operatorAvailabilityTimer = setInterval(pingSocketWithAvailability, availabilityPingDelay);
        }
    }

    function stopOperatorAvailablePinging() {
        //Stop pinging
        log.debug('stopping operator available pinging');
        if (operatorAvailabilityTimer) {
            clearTimeout(operatorAvailabilityTimer);
            operatorAvailabilityTimer = null;
        }
    }

    function isOperatorStatusAvailable() {
        var status = sessionStorage.getItem(operatorAvailableStatusKey);
        if (status) {
            return JSON.parse(status);
        }

        return true; //Operator available by default
    }

    function setOperatorAvailableStatus(available) {
        sessionStorage.setItem(operatorAvailableStatusKey, JSON.stringify(available));
    }

    function updateOperatorStatus(available) {
        //Update operator status in session storage
        setOperatorAvailableStatus(available);

        var socketOpen = isSocketOpen();
        if (available && !socketOpen) {
            log.debug('update operator status available, socket is not open, so open it');
            startChatSocket();
        } else if (available && socketOpen) {
            startOperatorAvailablePinging();
        } else if (!available && socketOpen) {
            log.debug('update operator status NOT available, socket is open, so close it');
            pingSocketWithAvailability();
            stopOperatorAvailablePinging();
        }

        //Send event to listeners
        $(window).trigger('OperatorAvailableStatusChanged', [available]);
    }

    function sendThreadClosedMessage(threadId) {
        if (isSocketOpen()) {
            var payload = {
                MessageType: VisitPay.ChatSettings.SocketMessageTypes.ThreadClosed,
                Message: JSON.stringify({ ThreadId: threadId })
            };
            log.debug('sending thread closed message over socket: ', JSON.stringify(payload));
            chatSocket.send(JSON.stringify(payload));
        }
    }

    function listenForLogout() {
        $('#btnNavLogout').click(function (e) {
            e.preventDefault();
            log.debug('LOG OUT BUTTON CLICKED: ', $(e.target).attr('href'));
            log.debug('telling socket that operator is logging out');
            
            $.blockUI();

            //Tell socket operator is disconnecting
            pingSocketWithAvailability(false);
            stopOperatorAvailablePinging();

            //Logout
            window.location.href = $(e.target).attr('href');
        });
    }

    return {
        Initialize: function () {
            var initPromise = $.Deferred();

            //Check if chat available
            isChatAvailable().done(function (isAvailable) {
                log.debug('available promise completed with answer: ', isAvailable);
                if (isAvailable === true) {
                    //Chat available, start the socket
                    log.debug('operator status is: ', isOperatorStatusAvailable());
                    startChatSocket().done(function () {
                        initPromise.resolve();
                    }).fail(function () {
                        log.debug('Socket start promise failed. Init promise will fail.');
                        //Socket failed to start
                        initPromise.fail();
                    });
                } else {
                    log.debug('Chat not available. Socket init promise will fail and socket will not connect.');
                    //Chat is unavailable
                    initPromise.fail();
                }
            }).fail(function () {
                log.error('Unable to check chat availability. Socket init promise will fail.');
                //Issue checking chat availability
                initPromise.fail();
            });

            return initPromise;
        },
        UpdateOperatorStatus: function(available) {
            log.debug('update operator status called with: ', available);
            updateOperatorStatus(available);
        },
        IsOperatorStatusAvailable: function() {
            return isOperatorStatusAvailable();
        },
        SendThreadClosedMessage: function(threadId) {
            sendThreadClosedMessage(threadId);
        }
    };
})(jQuery);
﻿(function(chatIndicator, $, ko) {
    var chatIndicatorModel;

    var urls = {
        ThreadQueueCount: VisitPay.ChatSettings.ChatApiUrl + 'thread/queue/count?operatorToken=' + VisitPay.ChatSettings.OperatorChatToken,
        IsChatAvailable: '/Chat/IsChatAvailable'
    };

    var ChatIndicatorViewModel = function () {
        var self = this;

        self.ThreadCount = ko.observable(0);
        self.OperatorIsAvailable = ko.observable(true);
        self.ChatIsAvailable = ko.observable(true);

        self.IsOff = ko.computed(function () {
            //Only show OFF if operator or chat is offline and they have no current active threads
            return (!self.OperatorIsAvailable() || !self.ChatIsAvailable()) && self.ThreadCount() == 0;
        });

        //The text that should show on the indicator badge
        self.ChatIndicatorBadge = ko.computed(function () {
            if (self.IsOff()) {
                return 'OFF';
            }

            return self.ThreadCount();
        });
    };

    function loadThreadCount() {
        VisitPay.ChatSettings.ChatApiToken().done(function (apiToken) {
            $.ajax({
                url: urls.ThreadQueueCount,
                type: 'GET',
                cache: false,
                beforeSend: function (xhr) {
                    xhr.setRequestHeader('Authorization', 'Bearer ' + apiToken);
                },
                success: function (count) {
                    log.debug('CHAT INDICATOR load thread count response: ', count);
                    chatIndicatorModel.ThreadCount(count);
                }
            });
        });
    }

    function setupSocketListeners() {
        log.debug('Chat Indicator: starting to listen for socket events');
        //Listen for socket messages
        $(window).on('ChatThreadReceived', function (event, threadJson) {
            log.debug('CHAT INDICATOR received a chat socket thread event: ', threadJson);
            //Received a new message from the socket. Reload the thread count.
            loadThreadCount();
        });

        $(window).on('ChatThreadClosed', function (event, threadClosedJson) {
            log.debug('CHAT INDICATOR received a chat socket thread CLOSED event: ', threadClosedJson);
            //A thread was closed. Reload the thread count.
            loadThreadCount();
        });

        $(window).on('UpdateChatIndicator', function(event) {
            log.debug('CHAT INDICATOR received an UpdateChatIndicator event');
            //Someone told us to update
            loadThreadCount();
        });
    }

    function setupOperatorStatusListener() {
        $(window).on('OperatorAvailableStatusChanged', function (event, available) {
            log.debug('CHAT INDICATOR received an operator status update event: ', available);
            chatIndicatorModel.OperatorIsAvailable(available);
            if (available) {
                loadThreadCount();
            }
        });
    }

    function init() {
        //Check if chat is available
        var availablePromise = $.Deferred();
        
        $.post(urls.IsChatAvailable, function (response) {
            log.debug('CHAT INDICATOR Chat Available Response: ', response);
            chatIndicatorModel.ChatIsAvailable(response.IsAvailable);

            //Check operator status
            chatIndicatorModel.OperatorIsAvailable(VisitPay.ChatSocket.IsOperatorStatusAvailable());

            availablePromise.resolve();
        });

        availablePromise.done(function () {
            //Get count of threads
            loadThreadCount();

            //Setup listeners
            setupSocketListeners();
            setupOperatorStatusListener();
        });
    }

    $(document).ready(function () {
        chatIndicatorModel = new ChatIndicatorViewModel();
        var $scope = $('#chat-indicator');

        //Apply bindings
        ko.applyBindings(chatIndicatorModel, $scope[0]);

        //Initialize
        init();
    });

})(window.VisitPay.ChatIndicator = window.VisitPay.ChatIndicator || {},
    jQuery,
    window.ko);
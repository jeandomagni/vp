﻿
window.VisitPay.ChatAvailabilityViewModel = function (data) {
    var self = this;

    self.ChatAvailabilityId = ko.observable(null);
    self.DayOfWeek = ko.observable(null);
    self.DayOfWeekDisplay = ko.observable(null);
    self.Date = ko.observable(null);
    self.TimeStart = ko.observable(null);
    self.TimeEnd = ko.observable(null);
    self.Offline = ko.observable(true);

    self.OfflineChanged = function (data) {
        if (data.Offline()) {
            //Clear out times
            self.TimeStart(null);
            self.TimeEnd(null);
        } else {
            //Set defaults
            self.TimeStart(self.DefaultStartTime());
            self.TimeEnd(self.DefaultEndTime());
        }

        return true; //Allow default event so checkbox changes
    };

    self.IsCustomAvailability = ko.computed(function () {
        //Custom availabilities will not have a DayOfWeek
        return self.DayOfWeek() == null;
    });
    
    self.DefaultStartTime = ko.computed(function() {
        return new moment('08:00:00', 'hh:mm A');
    });
    
    self.DefaultEndTime = ko.computed(function () {
        return new moment('17:00:00', 'hh:mm A');
    });

    self.PostModel = function () {
        return {
            ChatAvailabilityId: ko.unwrap(self.ChatAvailabilityId),
            DayOfWeek: ko.unwrap(self.DayOfWeek),
            Date: convertToServerDate(self.Date, 'MM/DD/YYYY'),
            TimeStart: convertToServerDate(self.TimeStart, 'hh:mm A', 'HH:mm:ss'),
            TimeEnd: convertToServerDate(self.TimeEnd, 'hh:mm A', 'HH:mm:ss')
        };
    };

    function convertToServerDate(koDate, webFormat, serverFormat) {
        var date = moment(ko.unwrap(koDate), webFormat);
        if (!date.isValid()) {
            return null;
        } else {
            if (serverFormat) {
                return date.format(serverFormat);
            } else {
                return date.format();
            }
        }
    }

    //Map from incoming JSON
    ko.mapping.fromJS(data, {}, self);
};

window.VisitPay.ChatAvailabilityMapping = {
    create: function (options) {
        var chatAvailabilityModel = new window.VisitPay.ChatAvailabilityViewModel(options.data);
        return chatAvailabilityModel;
    }
};

window.VisitPay.ChatAdminViewModel = (function (initialData) {
    var self = this;

    var urls = {
        SetChatEnabled: VisitPay.ChatSettings.ChatApiUrl + 'manage/enable',
        CheckChatStatus: VisitPay.ChatSettings.ChatApiUrl + 'manage/status',
        DeleteChatAvailability: '/Chat/DeleteChatAvailability',
        UpdateChatAvailability: '/Chat/UpdateChatAvailability'
    };

    //Listen for changes
    self.HasDefaultChanges = ko.observable(false);
    self.HasCustomChanges = ko.observable(false);

    self.ChatEnabled = ko.observable(true); //Kill switch, load from API
    self.ChatStatus = ko.observable('');
    self.ChatAvailabilities = ko.observableArray([]).extend({ extendedArray: {} }); //Full list of availabilities
    self.DefaultAvailabilities = ko.computed(function () {
        if (self.ChatAvailabilities.Any()) {
            //Only return the defaults, which have a "DayOfWeek" set
            return ko.utils.arrayFilter(self.ChatAvailabilities(), function (availability) {
                return ko.unwrap(availability.DayOfWeek) !== null;
            });
        }

        return ko.observableArray([]); //No availabilities, return empty array
    });

    self.CustomAvailabilities = ko.computed(function () {
        if (self.ChatAvailabilities.Any()) {
            //Only return the custom entries, which do not have a "DayOfWeek" set
            return ko.utils.arrayFilter(self.ChatAvailabilities(), function (availability) {
                return ko.unwrap(availability.DayOfWeek) == null;
            });
        }

        return ko.observableArray([]); //No availabilities, return empty array
    });

    self.AddCustomAvailability = function () {
        self.ChatAvailabilities.push(new window.VisitPay.ChatAvailabilityViewModel());
    };

    self.RemoveCustomAvailability = function (availability) {
        if (ko.unwrap(availability.ChatAvailabilityId) > 0) {
            var confirmPromise = window.VisitPay.Common.ModalGenericConfirmation('Remove Override', 'Are you sure you want to delete this chat availability override?', 'Delete', 'Cancel');
            confirmPromise.done(function() {
                $.post(urls.DeleteChatAvailability, { chatAvailabilityId: ko.unwrap(availability.ChatAvailabilityId) }, function() {
                    self.ChatAvailabilities.remove(availability);
                    var promise = window.VisitPay.Common.ModalGenericAlert('Delete Successful', 'The chat availability override has been deleted', 'Ok');
                    promise.done(function () {
                        self.Reload(); //Reload availabilities
                    });
                });
            });
        } else {
            //Hasn't been saved yet, remove from the list
            self.ChatAvailabilities.remove(availability);
        }
    };

    self.ToggleChatEnabled = function (e) {
        if (self.ChatEnabled()) {
            //Want to turn off chat
            var turnOffPromise = window.VisitPay.Common.ModalGenericConfirmation('Disable Chat', 'Are you sure you want to turn chat off?', 'Disable', 'Cancel');
            turnOffPromise.done(function () {
                setChatEnabled(false);
            });
        } else {
            //Want to turn on chat
            var turnOnPromise = window.VisitPay.Common.ModalGenericConfirmation('Enable Chat', 'Are you sure you want to turn chat on?', 'Enable', 'Cancel');
            turnOnPromise.done(function () {
                setChatEnabled(true);
            });
        }
    };

    function setChatEnabled(enabled) {
        VisitPay.ChatSettings.ChatApiToken().done(function (apiToken) {
            $.ajax({
                url: urls.SetChatEnabled,
                type: 'POST',
                cache: false,
                contentType: 'application/json',
                data: JSON.stringify(enabled),
                crossDomain: true,
                beforeSend: function (xhr) {
                    xhr.setRequestHeader('Authorization', 'Bearer ' + apiToken);
                },
                success: function (result) {
                    log.debug('chat enabled response: ', result);
                    if (result === true) {
                        self.UpdateChatStatus();
                    }
                }
            });
        });
    }

    self.UpdateChatStatus = function () {
        var promise = $.Deferred();

        //Check API to see if chat is enabled
        VisitPay.ChatSettings.ChatApiToken().done(function (apiToken) {
            $.ajax({
                url: urls.CheckChatStatus,
                type: 'GET',
                cache: false,
                crossDomain: true,
                beforeSend: function (xhr) {
                    xhr.setRequestHeader('Authorization', 'Bearer ' + apiToken);
                },
                success: function (response) {
                    log.debug('CHAT ADMIN chat status response: ', response);
                    //Set the status on the model
                    self.ChatStatus(response);

                    //Set chat enabled flag
                    if (response === 'Running') {
                        self.ChatEnabled(true);
                    } else if (response === 'Stopped') {
                        self.ChatEnabled(false);
                    }

                    promise.resolve();
                }
            });
        });

        return promise;
    };

    self.SaveDefaults = function () {
        saveAvailabilities(self.DefaultAvailabilities(), 'Default Availabilities Saved', $('#defaultAvailabilitiesForm'));
    };

    self.SaveCustoms = function () {
        saveAvailabilities(self.CustomAvailabilities(), 'Availability Overrides Saved', $('#customAvailabilitiesForm'));
    };

    function saveAvailabilities(listToSave, successMessage, $form) {
        //Reset validation
        window.VisitPay.Common.ResetValidationSummary($form);

        //Build list to save
        var toSave = new Array();
        $.each(listToSave, function (index, availability) {
            toSave.push(availability.PostModel());
        });
        
        $.ajax({
            url: urls.UpdateChatAvailability,
            type: 'POST',
            cache: false,
            contentType: 'application/json',
            data: JSON.stringify({ availabilities: toSave }),
            success: function (result) {
                if (!result.Success) {
                    //Save failed, show errors
                    $.each(result.Errors, function (index, error) {
                        window.VisitPay.Common.AppendMessageToValidationSummary($form, error);
                    });
                    window.VisitPay.Common.ModalGenericAlert('Save Failed', 'There are one or more invalid entries. Please correct the errors and try again.', 'Ok');
                } else {
                    //Save successful
                    var promise = window.VisitPay.Common.ModalGenericAlert('Save Successful', successMessage, 'Ok');
                    promise.done(function () {
                        self.Reload(); //Reload availabilities
                    });
                }
            }
        });
    }

    self.Reload = function () {
        $.blockUI();
        location.reload();
    };
    
    self.Map = function (data) {
        if (data == null) {
            return;
        }

        var availabilityArray = ko.mapping.fromJS(data, window.VisitPay.ChatAvailabilityMapping);
        self.ChatAvailabilities(ko.unwrap(availabilityArray));
    };
    self.Map(initialData);

    return self;
});

window.VisitPay.ChatAdmin = (function ($, ko) {
    var $scope = $('#section-chat-admin');
    var model;

    function initKillSwitch() {
        model.UpdateChatStatus().done(function () {
            //Show status section
            $('.chat-status-section').css('visibility', 'visible');
        });
    }

    return {
        Initialize: function (data) {
            model = new window.VisitPay.ChatAdminViewModel(data);
            ko.applyBindings(model, $scope[0]);
            
            //Set up kill switch
            initKillSwitch();

            //Show UI
            $scope.css('visibility', 'visible');
        }

    };
})(jQuery, window.ko);
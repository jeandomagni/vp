﻿(function (chatQueue, clientSettings, common, $, ko) {
    var chatQueueModel;

    var urls = {
        QueueThreads: VisitPay.ChatSettings.ChatApiUrl + 'thread/queue?operatorToken=' + VisitPay.ChatSettings.OperatorChatToken,
        RequestThreadConnect: VisitPay.ChatSettings.ChatApiUrl + 'thread/requestThreadConnect',
        ChatIframeUrl: VisitPay.ChatSettings.ChatIframeUrl,
        ChatSocket: VisitPay.ChatSettings.ChatSocketUrl,
        ConnectToThread: '/Chat/ConnectToThread',
        DisconnectFromThread: '/Chat/DisconnectFromThread',
        EndThread: '/Chat/EndThread',
        GoToAccount: '/Chat/GoToAccount',
        IsChatAvailable: '/Chat/IsChatAvailable'
    };

    var threadStatus = {
        New: 'New',
        Pending: 'Pending',
        Active: 'Active',
        EndedByGuarantor: 'EndedByGuarantor',
        Closed: 'Closed'
    };

    //This is updating the message time display in the queue every 30 seconds
    ko.bindingHandlers.messageSentDateTimer = {
        update: function (element) {
            var time = $(element).data('messageTime');
            var timePrefix = $(element).data('timePrefix');
            var timer = setInterval(function () {
                var display = timePrefix + jQuery.timeago(time);
                $(element).text(display);
            }, 30000);
        }
    };

    //These 3 models represent the data we are getting from ChatGateway
    //This mapping is used to map chat threads from the API/Socket into KO observable view models
    var ChatThreadViewModelMapping = {
        create: function(options) {
            var chatThread = new ChatThreadViewModel(options.data);
            return chatThread;
        },
        'lastMessage': {
            create: function(options) {
                var chatThreadMessage = new ChatThreadMessageViewModel(options.data);
                return chatThreadMessage;
            }
        }
    };

    var ChatThreadViewModel = function (data) {
        var self = this;
        
        self.id = ko.observable(null);
        self.status = ko.observable(null);
        self.dateCreated = ko.observable(null);
        self.vpUserToken = ko.observable(null);
        self.title = ko.observable(null);
        self.totalMessages = ko.observable(null);
        self.unreadMessages = ko.observable(null);
        self.lastMessage = ko.observable(null); //ChatThreadMessageViewModel

        self.badgeDisplayText = ko.computed(function () {
            if (self.status() === threadStatus.New) {
                //No operator connected
                return 'NEW';
            } else {
                //Operator connected, show unread count
                return self.unreadMessages();
            }
        });

        self.isThreadClosed = ko.computed(function () {
            return self.status() === threadStatus.Closed || self.status() === threadStatus.EndedByGuarantor;
        });

        //Map from incoming JSON
        ko.mapping.fromJS(data, {}, self);
    };

    var ChatThreadMessageViewModel = function (data) {
        var self = this;
        
        self.id = ko.observable(null);
        self.dateSent = ko.observable(null);
        self.senderId = ko.observable(null);
        self.threadId = ko.observable(null);
        self.body = ko.observable(null);

        //Map from incoming JSON
        ko.mapping.fromJS(data, {}, self);
    };

    var ChatQueueViewModel = function () {
        var self = this;

        self.ChatIsAvailable = ko.observable(false);
        self.OperatorIsAvailable = ko.observable(true);

        self.ToggleOperatorStatus = function () {
            var isAvailable = VisitPay.ChatSocket.IsOperatorStatusAvailable();
            var confirmTitle;
            var confirmMessage;
            var confirmBtn;
            if (isAvailable) {
                confirmTitle = 'Set Self Unavailable';
                confirmMessage = 'Are you sure you want to mark yourself as unavailable for chat?';
                confirmBtn = 'Set Unavailable';
            } else {
                confirmTitle = 'Set Self Available';
                confirmMessage = 'Are you sure you want to mark yourself as available for chat?';
                confirmBtn = 'Set Available';
            }

            var togglePromise = window.VisitPay.Common.ModalGenericConfirmation(confirmTitle, confirmMessage, confirmBtn, 'Cancel');
            togglePromise.done(function () {
                //User confirmed toggle, update
                VisitPay.ChatSocket.UpdateOperatorStatus(!isAvailable);
                self.OperatorIsAvailable(!isAvailable);

                //If we turned queue on, reload chats
                if (self.OperatorIsAvailable()) {
                    log.debug('operator went available, reload chats');
                    loadChats();
                }
            });
        };

        //Chat Threads
        self.ChatThreads = ko.observableArray([]); //Array of ChatThreadViewModel

        self.ThreadWithId = function (id) {
            if (!id) {
                return null;
            } else {
                return ko.utils.arrayFirst(this.ChatThreads(), function (item) {
                    return item.id() == id;
                });
            }
        };

        //Sorts threads by date created
        self.SortedChatThreads = ko.computed(function () {
            return self.ChatThreads().sort(function (left, right) {
                var leftDateCreated = left.dateCreated();
                var rightDateCreated = right.dateCreated();
                if (leftDateCreated == rightDateCreated)
                    return 0;

                //Sort by time, oldest first
                return leftDateCreated < rightDateCreated ? -1 : 1;
            });
        });

        self.ChatThreadWithId = function (id) {
            //Gets the chat thread with the given id, if one exists
            if (!id) {
                return null;
            } else {
                return ko.utils.arrayFirst(this.ChatThreads(), function (item) {
                    return item.id() == id;
                });
            }
        };

        self.RemoveThreadWithId = function (id) {
            log.debug('removing thread from queue with threadId: ', id);
            self.ChatThreads.remove(function (thread) {
                return thread.id() == id;
            });
        };

        self.ChatThreadMessageReceived = function (chatThread) {
            log.debug('received thread message: ', ko.unwrap(chatThread));
            var existingThread = self.ChatThreadWithId(chatThread.id());
            if (existingThread) {
                log.debug('updating existing thread');
                existingThread.title(chatThread.title());
                existingThread.status(chatThread.status());
                existingThread.totalMessages(chatThread.totalMessages());
                existingThread.unreadMessages(chatThread.unreadMessages());
                existingThread.lastMessage(chatThread.lastMessage());
            } else {
                log.debug('received message for new thread. adding');
                self.ChatThreads.push(chatThread);
            }
        };

        self.ActiveThread = ko.observable(null);
        self.IsActiveThread = function (chatThread) {
            if (!chatThread || !self.ActiveThread()) {
                return false;
            }

            return chatThread.id() === self.ActiveThread().id();
        };
        self.HasActiveThread = ko.computed(function () {
            return self.ActiveThread() != null;
        });

        self.ThreadClicked = function (chatThread) {
            var requestData = {
                ThreadId: ko.unwrap(chatThread.id()),
                UserToken: VisitPay.ChatSettings.OperatorChatToken,
                FirstName: VisitPay.ChatSettings.OperatorFirstName,
                LastName: VisitPay.ChatSettings.OperatorLastName
            };
            log.debug('checking if operator can pick up thread: ', requestData);

            VisitPay.ChatSettings.ChatApiToken().done(function (apiToken) {
                $.ajax({
                    url: urls.RequestThreadConnect,
                    type: 'PUT',
                    cache: false,
                    contentType: 'application/json',
                    data: JSON.stringify(requestData),
                    crossDomain: true,
                    beforeSend: function (xhr) {
                        xhr.setRequestHeader('Authorization', 'Bearer ' + apiToken);
                    },
                    success: function (response) {
                        log.debug('got response from thread pickup request: ', response);
                        if (response && response.canConnect) {
                            //Operator is allowed to connect, show the thread
                            self.ShowThread(chatThread);
                        } else {
                            //Another operator has picked up the thread
                            var confirmationPromise = VisitPay.Common.ModalGenericAlert('Cannot Connect to Thread', response.message, 'Ok');
                            confirmationPromise.done(function () {
                                //Remove the thread from the queue
                                self.RemoveThreadWithId(chatThread.id());
                            });
                        }
                    }
                });
            });
        };

        self.ShowThread = function (chatThread) {
            //If it's the same thread, don't do anything
            if (self.IsActiveThread(chatThread)) {
                return;
            }

            //If we are showing a different thread than the current active one, log a disconnect event
            if (self.HasActiveThread() && !self.IsActiveThread(chatThread)) {
                log.debug('user opened a thread while a current thread was active. log DISCONNECT event');
                disconnectFromChatThread(self.ActiveThread());
            }

            //Set the thread as active
            self.ActiveThread(chatThread);

            //If it's a new thread, go ahead and mark the thread as pending locally to update the unreadBadge
            if (chatThread.status() === threadStatus.New) {
                chatThread.status(threadStatus.Pending);
            }

            loadChatThread(chatThread);

            //Save chat in dialog in case we leave page
            setupActiveThreadInDialog();
        };

        self.EndThread = function () {
            log.debug('end thread called');
            operatorEndThread();
        };

        self.ChatMessageTime = function (date) {
            return jQuery.timeago(ko.unwrap(date));
        };
        
        self.ShowThreadById = function (threadId, iframeSrc) {
            var thread = self.ThreadWithId(threadId);
            if (thread) {
                self.ShowThread(thread, iframeSrc);
            }
        };

        self.GoToAccount = function () {
            $.blockUI();

            var guarantorChatToken = ko.unwrap(self.ActiveThread().vpUserToken());
            $.post(urls.GoToAccount, { guarantorChatToken: guarantorChatToken }, function (result) {
                if (result != null) {
                    window.location = result;
                }
            });
        };
    };

    function threadAuditingPostData(chatThread) {  
        if (chatThread) {
            var postData =  {
                chatThreadId: ko.unwrap(chatThread.id()),
                guarantorChatToken: ko.unwrap(chatThread.vpUserToken())
            };
            return postData;
        }

        return null;
    }

    function postMessageToIframe(message) {
        $('#chatIframe')[0].contentWindow.postMessage(message, '*');
    }

    function operatorEndThread() {

        //Check if we have an active thread
        if (chatQueueModel.HasActiveThread()) {
            var activeThread = chatQueueModel.ActiveThread();

            var confirmationPromise = $.Deferred();
            //Check if thread is already closed
            if (activeThread.isThreadClosed()) {
                //Thread was closed by guarantor, no need to prompt
                confirmationPromise.resolve();
            } else {
                //Thread isn't closed yet, so verify they want to end it
                confirmationPromise = VisitPay.Common.ModalGenericConfirmation('End Thread', 'Are you sure you want to end this chat thread?', 'End Thread', 'Cancel');
            }

            confirmationPromise.done(function () {
                //Send end chat request to iframe...will get response from iframe listener once thread is closed
                log.warn('sending endChatRequest to iframe');
                postMessageToIframe('endChatRequest');
            });
        }
    }

    function operatorPickedUpThread(operatorPickedUpThreadMessage) {
        //Make sure it's not this operator picking it up
        if (operatorPickedUpThreadMessage.OperatorToken != VisitPay.ChatSettings.OperatorChatToken) {
            var threadId = operatorPickedUpThreadMessage.ThreadId;

            //Remove thread from the queue
            chatQueueModel.RemoveThreadWithId(threadId);
        }
    }

    function guarantorEndedThread(threadClosedMessage) {
        var threadId = threadClosedMessage.ThreadId;
        //Check if the thread that ended is the current thread
        var needToWarnPromise = $.Deferred();
        if (chatQueueModel.IsActiveThread(chatQueueModel.ThreadWithId(threadId))) {
            //It is the active thread, show confirmation
            var confirmationPromise = VisitPay.Common.ModalGenericAlert('Guarantor Ended Thread', 'The guarantor has ended the thread.', 'Ok');
            confirmationPromise.done(function () {
                //Log end thread
                $.post(urls.EndThread, threadAuditingPostData(chatQueueModel.ActiveThread()));

                //Clear out the active session
                chatQueueModel.ActiveThread(null);

                //Clear chat from dialog
                VisitPay.ChatDialog.CloseActiveDialog();

                needToWarnPromise.resolve();
            });
        } else {
            //It's not the active thread, no need to warn
            needToWarnPromise.resolve();
        }

        needToWarnPromise.done(function () {
            //Remove thread from the queue
            chatQueueModel.RemoveThreadWithId(threadId);
        });
    }

    function disconnectFromChatThread(chatThread) {
        log.debug('disconnecting from chat thread: ', ko.unwrap(chatThread));

        //Audit disconnect event
        $.post(urls.DisconnectFromThread, threadAuditingPostData(chatThread))
            .done(function (data) {
                log.debug('Called DISCONNECT FROM THREAD with response: ', data);
            });
    }

    function buildBaseIframeUrl(chatThread) {
        var urlPromise = $.Deferred();

        VisitPay.ChatSettings.ChatApiToken().done(function (apiToken) {
            var url = urls.ChatIframeUrl;
            url += '?userToken=' + VisitPay.ChatSettings.OperatorChatToken;
            url += '&apiToken=' + apiToken;
            url += '&threadId=' + chatThread.id();
            url += '&firstName=' + VisitPay.ChatSettings.OperatorFirstName;
            url += '&lastName=' + VisitPay.ChatSettings.OperatorLastName;

            urlPromise.resolve(url);
        });

        return urlPromise;
    }

    function loadChatThread(chatThread) {
        var iframe = $('#chatIframe');
        buildBaseIframeUrl(chatThread).done(function (url) {
            //Provide styling in URL
            var styles = {
                header: {
                    visible: false
                },
                launcher: {
                    visible: false
                },
                window: {
                    width: iframe.width() + 'px',
                    height: iframe.height() + 'px',
                    bottom: '0px',
                    right: '0px'
                },
                theme: VisitPay.ChatSettings.ChatThemeColor
            };

            //Add context param
            var context = {
                styles: styles
            };
            url += '&context=' + JSON.stringify(context);

            log.debug('connecting iframe with url: ', url);
            iframe.attr('src', url);

            //Audit connect event
            $.post('/Chat/ConnectToThread', threadAuditingPostData(chatThread))
                .done(function (data) {
                    log.debug('Called CONNECT TO THREAD with response: ', data);
                }); 
        });
    }
    
    function loadChats() {
        var promise = $.Deferred();
        log.debug('loading chats...');
        VisitPay.ChatSettings.ChatApiToken().done(function (apiToken) {
            $.ajax({
                url: urls.QueueThreads,
                type: 'GET',
                cache: false,
                beforeSend: function (xhr) {
                    xhr.setRequestHeader('Authorization', 'Bearer ' + apiToken);
                },
                success: function (data) {
                    chatQueueModel.ChatThreads([]); //Clear

                    var threadArray = ko.mapping.fromJS(data, ChatThreadViewModelMapping);
                    chatQueueModel.ChatThreads(ko.unwrap(threadArray));

                    promise.resolve();
                },
                error: function (e) {
                    log.error('Error occurred calling VisitPay.ChatGateway: ', e);
                }
            });
        });
        
        return promise;
    }

    function setupActiveThreadInDialog() {
        log.debug('setupActiveThreadInDialog');
        buildBaseIframeUrl(chatQueueModel.ActiveThread()).done(function (url) {
            var styles = {
                header: {
                    visible: false
                },
                launcher: {
                    visible: false
                },
                window: {
                    width: '400px',
                    height: '457px',
                    bottom: '0px',
                    right: '0px'
                },
                theme: VisitPay.ChatSettings.ChatThemeColor
            };

            //Add context param
            var context = {
                styles: styles
            };
            url += '&context=' + JSON.stringify(context);

            log.debug('built iframe URL for dialog: ', url);

            var dialogData = {
                iframeSrc: url,
                title: chatQueueModel.ActiveThread().title(),
                threadId: chatQueueModel.ActiveThread().id(),
                guarantorChatToken: chatQueueModel.ActiveThread().vpUserToken()
            };
            return VisitPay.ChatDialog.SetupDialog(dialogData);
        });
    }
    
    function setupSocketListeners() {
        log.debug('starting to listen for socket events');
        //Listen for socket messages
        $(window).on('ChatThreadReceived', function (event, threadJson) {
            log.debug('CHAT QUEUE received a chat socket THREAD RECEIVED event: ', threadJson);
            if (threadJson) {
                var koThread = ko.mapping.fromJS(threadJson, ChatThreadViewModelMapping);
                chatQueueModel.ChatThreadMessageReceived(koThread);
            }
        });

        $(window).on('ChatThreadClosed', function (event, threadClosedJson) {
            log.debug('CHAT QUEUE received a chat socket THREAD CLOSED event: ', threadClosedJson);
            if (threadClosedJson) {
                guarantorEndedThread(threadClosedJson);
            }
        });

        $(window).on('OperatorPickedUpThread', function (event, operatorPickedUpThreadJson) {
            log.debug('CHAT QUEUE received a chat socket OPERATOR PICKED UP THREAD event: ', operatorPickedUpThreadJson);
            if (operatorPickedUpThreadJson) {
                log.debug('removing thread from queue');
                operatorPickedUpThread(operatorPickedUpThreadJson);
            }
        });
    }

    function setupIframeListeners() {
        //Listen for events from chat iframe
        window.addEventListener('message', function (event) {
            if (event) {
                if (event.data === 'chatClosed') {
                    log.debug('chat closed event');

                    var activeThread = chatQueueModel.ActiveThread();

                    //Save threadId to send over socket
                    var threadId = ko.unwrap(activeThread.id());

                    //Log end thread
                    $.post(urls.EndThread, threadAuditingPostData(activeThread))
                        .done(function (data) {
                            log.debug('Called END THREAD with response: ', data);
                        });

                    //Clear chat from dialog
                    VisitPay.ChatDialog.CloseActiveDialog();

                    //Clear out the active session
                    chatQueueModel.ActiveThread(null);

                    //Remove thread from the queue
                    chatQueueModel.RemoveThreadWithId(threadId);

                    //Tell chat indicator to update
                    $(window).trigger('UpdateChatIndicator');

                    //TODO: Save resolution information when that functionality exists (VP-4980)
                }
            }
        });
    }

    function init() {
        var initPromise = $.Deferred();

        //Check if chat is available
        var availablePromise = $.Deferred();
        $.post(urls.IsChatAvailable, function (response) {
            log.debug('Chat Available Response: ', response);
            chatQueueModel.ChatIsAvailable(response.IsAvailable);

            //Set operator status
            chatQueueModel.OperatorIsAvailable(VisitPay.ChatSocket.IsOperatorStatusAvailable());

            availablePromise.resolve();
        });

        availablePromise.done(function () {
            if (chatQueueModel.ChatIsAvailable()) {
                //Chat is available, so setup the page
                //Load chats
                var chatsLoadedPromise = loadChats();
                chatsLoadedPromise.done(function () {
                    //Setup socket listeners
                    setupSocketListeners();

                    //Setup iframe listeners
                    setupIframeListeners();

                    //Had chat open in a dialog, show that chat in the queue
                    if (VisitPay.ChatDialog.DialogIsActive()) {
                        log.debug('HAD ACTIVE DIALOG ON CHAT PAGE, LOAD ACTIVE CHAT');
                        var dialogModel = VisitPay.ChatDialog.ActiveDialogModel();
                        if (dialogModel) {
                            var threadId = dialogModel.Data.threadId;
                            chatQueueModel.ShowThreadById(threadId);

                            //Close the chat dialog since we are now handling it on this page
                            VisitPay.ChatDialog.CloseActiveDialog();
                        }
                    }
                });
            }

            //Resolve
            initPromise.resolve();
        });

        return initPromise;
    }

    $(document).ready(function () {
        chatQueueModel = new ChatQueueViewModel();
        var $scope = $('#section-chat-queue');
        
        //Apply bindings
        ko.applyBindings(chatQueueModel, $scope[0]);

        init().done(function () {
            //Show the UI
            $scope.css('visibility', 'visible');
        });
    });
})(window.VisitPay.ChatQueue = window.VisitPay.ChatQueue || {}, window.VisitPay.ClientSettings = window.VisitPay.ClientSettings || {}, window.VisitPay.Common = window.VisitPay.Common || {}, jQuery, window.ko);
﻿/*
 * keep in sync with same file in patient
 * used in both reconfig and normal fp flows
 */

(function() {

    var offerCalculationStrategyEnum = {
        MonthlyPayment: 1,
        NumberOfPayments: 2
    };

    function formatDecimal(value) {
        if (value === undefined || value === null || isNaN(value)) {
            return value;
        }
        return parseFloat(value).toFixed(2);
    }
    
    window.VisitPay.FinancePlanTermsViewModel = function(initialTerms) {

        var self = {};

        self.EffectiveDate = ko.observable();
        self.FinalPaymentDate = ko.observable();
        self.FinancePlanOfferId = ko.observable();
        self.FinancePlanOfferSetTypeId = ko.observable();
        self.GuarantorPaymentDueDay = ko.observable();
        self.HasInterest = ko.observable(false);
        self.InterestRate = ko.observable();
        self.LastPaymentAmountIsDifferentThanMonthlyAmount = ko.observable();
        self.MonthlyPaymentAmount = ko.observable();
        self.MonthlyPaymentAmountDecimal = ko.observable();
        self.MonthlyPaymentAmountLast = ko.observable();
        self.MonthlyPaymentAmountOtherPlans = ko.observable();
        self.MonthlyPaymentAmountPrevious = ko.observable();
        self.MonthlyPaymentAmountTotal = ko.observable();
        self.NextPaymentDueDate = ko.observable();
        self.NumberMonthlyPayments = ko.observable();
        self.NumberOfEqualPayments = ko.observable();
        self.NumberOfEqualPayments.IsVisible = ko.computed(function() {
            var num = ko.unwrap(self.NumberOfEqualPayments);
            if (!num || isNaN(num) || parseInt(num) === 0) {
                return false;
            }
            return true;
        });
        self.PaymentDueDayOrdinal = ko.observable();
        self.RequireCreditAgreement = ko.observable(true);
        self.TotalInterest = ko.observable();

        self.ErrorMessage = ko.observable('');
        self.ErrorMessage.Any = ko.computed(function() {
            return ko.unwrap(self.ErrorMessage) && ko.unwrap(self.ErrorMessage).length > 0;
        });
        self.ErrorMessage.Clear = function() {
            self.ErrorMessage('');
        };
        
        // helpers
        self.IsValid = ko.computed(function () {
            return !self.ErrorMessage.Any() && ko.unwrap(self.MonthlyPaymentAmount) > 0;
        });
        self.ResetValues = function () {
            self.ErrorMessage.Clear();
            self.Map();
        };

        // map
        self.Map = function(terms) {
            if (terms == null) {
                terms = {};
            };
            self.EffectiveDate(terms.EffectiveDate);
            self.FinalPaymentDate(terms.FinalPaymentDate);
            self.FinancePlanOfferId(terms.FinancePlanOfferId);
            self.FinancePlanOfferSetTypeId(terms.FinancePlanOfferSetTypeId);
            self.GuarantorPaymentDueDay(terms.GuarantorPaymentDueDay);
            self.HasInterest(terms.HasInterest);
            self.InterestRate(terms.InterestRate);
            self.LastPaymentAmountIsDifferentThanMonthlyAmount(terms.LastPaymentAmountIsDifferentThanMonthlyAmount);
            self.MonthlyPaymentAmount(terms.MonthlyPaymentAmount);
            self.MonthlyPaymentAmountLast(terms.MonthlyPaymentAmountLast);
            self.MonthlyPaymentAmountOtherPlans(terms.MonthlyPaymentAmountOtherPlans);
            self.MonthlyPaymentAmountPrevious(terms.MonthlyPaymentAmountPrevious);
            self.MonthlyPaymentAmountTotal(terms.MonthlyPaymentAmountTotal);
            self.NextPaymentDueDate(terms.NextPaymentDueDate);
            self.NumberMonthlyPayments(terms.NumberMonthlyPayments);
            self.NumberOfEqualPayments(terms.NumberOfEqualPayments);
            self.PaymentDueDayOrdinal(terms.PaymentDueDayOrdinal);
            self.RequireCreditAgreement(terms.RequireCreditAgreement);
            self.TotalInterest(terms.TotalInterest);
        }
        self.Map(initialTerms);

        return self;

    };
    
    window.VisitPay.FinancePlanSetupBaseViewModel = function(initialData) {

        var self = {};

        self.FinancePlanOfferSetTypeId = ko.observable();
        self.FinancePlanOfferSetTypes = ko.observableArray([]).extend({ extendedArray: {} });
        self.InterestRates = ko.observableArray([]).extend({ extendedArray: {} });
        self.IsEditable = ko.observable(true);
        self.MaximumMonthlyPayments = ko.observable();
        self.MinimumPaymentAmount = ko.observable();
        self.OfferCalculationStrategy = ko.observable(1);
        self.OfferCalculationStrategy.IsDuration = ko.computed(function() {
            return ko.unwrap(self.OfferCalculationStrategy) === offerCalculationStrategyEnum.NumberOfPayments;
        });
        self.OfferCalculationStrategy.IsMonthly = ko.computed(function() {
            return ko.unwrap(self.OfferCalculationStrategy) === offerCalculationStrategyEnum.MonthlyPayment;
        });
        self.UserMonthlyPaymentAmount = ko.observable();
        self.UserMonthlyPaymentAmount.Any = ko.computed(function () {
            return ko.unwrap(self.UserMonthlyPaymentAmount) > 0;
        });
        self.UserMonthlyPaymentAmount.Clear = function () {
            self.UserMonthlyPaymentAmount('');
        };
        self.UserNumberMonthlyPayments = ko.observable();
        self.UserNumberMonthlyPayments.Any = ko.computed(function () {
            return ko.unwrap(self.UserNumberMonthlyPayments) > 0;
        });
        self.UserNumberMonthlyPayments.Clear = function () {
            self.UserNumberMonthlyPayments('');
        };
        self.Terms = ko.observable(new window.VisitPay.FinancePlanTermsViewModel());
        self.TermsCmsVersionId = ko.observable();
        self.TermsCmsVersionId.IsChecked = ko.observable(false);
        self.TermsCmsVersionId.TermsAgreementTitle = ko.observable('');
        self.TermsCmsVersionId.IsRetailInstallmentContract = ko.observable(true);
        self.TermsAlertTimeoutMet = ko.observable(false);
        self.TermsMeetThreshold = ko.computed(function () {
            if (self.OfferCalculationStrategy.IsMonthly()) {
                return self.UserMonthlyPaymentAmount.Any() && ko.unwrap(self.UserMonthlyPaymentAmount) >= ko.unwrap(self.MinimumPaymentAmount);
            }

            if (self.OfferCalculationStrategy.IsDuration()) {
                return self.UserNumberMonthlyPayments.Any() && ko.unwrap(self.UserNumberMonthlyPayments) <= ko.unwrap(self.MaximumMonthlyPayments);
            }

            return false;
        });
        self.TermsCalculatedSinceLastChange = ko.observable(false);
        self.ShowHsaInterestDisclaimer = ko.computed(function () {

            if (self.Terms === undefined || self.Terms === null) {
                return false;
            }

            var terms = ko.unwrap(self.Terms);
            if (terms === undefined || terms === null) {
                return false;
            }

            return ko.unwrap(terms.IsValid) && ko.unwrap(terms.HasInterest);

        });

        // helpers
        self.HasPaymentParameters = ko.computed(function () {
            return ko.unwrap(self.UserMonthlyPaymentAmount.Any()) || ko.unwrap(self.UserNumberMonthlyPayments.Any());
        });
        self.CanValidate = ko.computed(function () {
            //Can validate if we have one of the term fields filled out, and we haven't calculated since they changed
            return !ko.unwrap(self.TermsCalculatedSinceLastChange()) && ko.unwrap(self.HasPaymentParameters);
        });
        self.IsValid = ko.computed(function() {
            return ko.unwrap(self.Terms).IsValid() && self.TermsCmsVersionId.IsChecked();
        });
        self.ResetValues = function() {
            ko.unwrap(self.Terms).ResetValues();
            self.UserMonthlyPaymentAmount('');
            self.UserNumberMonthlyPayments('');
        };
        self.UpdateFromTerms = function(result) {
            self.TermsCmsVersionId.IsChecked(false);
            self.UpdateBoundaries(result);
            if (result.IsError === true) {
                self.ResetValues();
                self.Terms().ErrorMessage(result.ErrorMessage);
                return;
            }
            
            self.TermsCmsVersionId(result.Terms.TermsCmsVersionId || null);
            self.TermsCmsVersionId.TermsAgreementTitle(result.Terms.TermsAgreementTitle || '');
            self.TermsCmsVersionId.IsRetailInstallmentContract(result.Terms.IsRetailInstallmentContract);

            var newTerms = ko.mapping.fromJS(result.Terms, {}, new window.VisitPay.FinancePlanTermsViewModel());
            self.Terms(newTerms);
            self.UserMonthlyPaymentAmount(formatDecimal(ko.unwrap(newTerms.MonthlyPaymentAmount)));
            self.UserNumberMonthlyPayments(ko.unwrap(newTerms.NumberMonthlyPayments));
        };
        self.UpdateBoundaries = function(result) {
            if (result == null) {
                return;
            }
            if (result.MaximumMonthlyPayments != null) {
                self.MaximumMonthlyPayments(result.MaximumMonthlyPayments);
            }
            if (result.MinimumPaymentAmount != null) {
                self.MinimumPaymentAmount(result.MinimumPaymentAmount);
            }
        };
        self.ShouldShowTermsAlert = ko.computed(function () {
            /*
             Only show terms alert if there are errors on the terms, or if we
             have entered terms that don't meet the thresholds after a certain amount
             of time.
             */
            return (self.TermsAlertTimeoutMet() && !self.TermsMeetThreshold()) || self.Terms().ErrorMessage.Any();
        });

        // subscribe
        self.TermsCalculatedSinceLastChange.subscribe(function (newValue) {
            // clear terms when inputs changed
            if (ko.unwrap(newValue) === false) {
                var terms = ko.unwrap(self.Terms);
                if (terms) {
                    terms.ResetValues();
                }
            }
        });

        // post models
        self.SubmitModelBase = function() {
            var terms = ko.unwrap(self.Terms);
            return {
                OfferCalculationStrategy: ko.unwrap(self.OfferCalculationStrategy),
                FinancePlanOfferId: ko.unwrap(terms.FinancePlanOfferId),
                FinancePlanOfferSetTypeId: ko.unwrap(terms.FinancePlanOfferSetTypeId),
                MonthlyPaymentAmount: ko.unwrap(terms.MonthlyPaymentAmount)
            };
        };
        self.ValidateModelBase = function() {
            var model = {
                FinancePlanOfferSetTypeId: ko.unwrap(self.FinancePlanOfferSetTypeId)
            };
            if (ko.unwrap(self.OfferCalculationStrategy.IsDuration)) {
                model.NumberMonthlyPayments = ko.unwrap(self.UserNumberMonthlyPayments);
            } else {
                model.MonthlyPaymentAmount = ko.unwrap(self.UserMonthlyPaymentAmount);
            }
            return model;
        };

        // map
        self.Map = function(data) {
            if (data == null) {
                return;
            }

            if (data.Terms) {
                self.TermsCmsVersionId.TermsAgreementTitle(data.Terms.TermsAgreementTitle);
                self.TermsCmsVersionId.IsRetailInstallmentContract(data.Terms.IsRetailInstallmentContract);
            }

            var terms = ko.unwrap(self.Terms);
            terms.Map(data.Terms);

            self.FinancePlanOfferSetTypeId(data.FinancePlanOfferSetTypeId);
            self.FinancePlanOfferSetTypes(data.FinancePlanOfferSetTypes);
            self.InterestRates(data.InterestRates);
            self.IsEditable(data.IsEditable);
            self.MaximumMonthlyPayments(data.MaximumMonthlyPayments);
            self.MinimumPaymentAmount(data.MinimumPaymentAmount);
            self.TermsCmsVersionId(data.TermsCmsVersionId);
            self.UserMonthlyPaymentAmount(ko.unwrap(terms.MonthlyPaymentAmount));
            self.UserNumberMonthlyPayments(ko.unwrap(terms.NumberMonthlyPayments));
        }
        self.Map(initialData);

        return self;

    };

})();
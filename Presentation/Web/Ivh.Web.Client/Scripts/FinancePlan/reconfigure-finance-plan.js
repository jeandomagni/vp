﻿window.VisitPay.ReconfigureFinancePlanViewModel = (function(data) {

    var self = new window.VisitPay.FinancePlanSetupBaseViewModel(data);
    self.FinancePlanIdToReconfigure = ko.observable(data.FinancePlanIdToReconfigure);
    self.IsError = ko.observable(false);
    self.VpGuarantorId = ko.observable(data.VpGuarantorId);

    // post models
    self.CancelModel = function() {
        return {
            FinancePlanIdToReconfigure: ko.unwrap(self.FinancePlanIdToReconfigure),
            VpGuarantorId: ko.unwrap(self.VpGuarantorId)
        };
    };
    self.SubmitModel = function() {
        return $.extend({}, self.SubmitModelBase(), {
            FinancePlanIdToReconfigure: ko.unwrap(self.FinancePlanIdToReconfigure),
            VpGuarantorId: ko.unwrap(self.VpGuarantorId)
        });
    };
    self.ValidateModel = function() {
        return $.extend({}, self.ValidateModelBase(), {
            FinancePlanIdToReconfigure: ko.unwrap(self.FinancePlanIdToReconfigure),
            VpGuarantorId: ko.unwrap(self.VpGuarantorId)
        });
    };

    return self;

});

window.VisitPay.ReconfigureFinancePlan = (function() {
    
    var $scope = $('#section-reconfigure');
    var model;
    var urls = {};
    var cms = {
        Cancel: {
            ContentTitle: 'Cancel Reconfigured Terms',
            ContentBody: '<p><strong>Please confirm your cancelation request.</strong></p><p style="text-align: left;">Canceling this reconfiguration will remove the alert on the Guarantor\'s account.  The Guarantor will no longer have access to review or accept the previously reconfigurated terms, and will continue to be responsble for this finance plan under its current terms.</p>'
        },
        Confirm: {
            ContentTitle: 'Confirm Reconfigured Terms',
            ContentBody: '<p><strong>Confirm the reconfigured terms and save for Guarantor review.</strong></p><hr /><p>An email confirmation will be sent to the Guarantor.</p><p style="text-align: left;">Please remind the Guarantor that she/he must log in and accept the reconfigured terms in order for the terms to be in effect. The Guarantor is responsible for this finance plan under its current terms up until the new, reconfigured terms are accepted.</p>'
        }
    };
    
    function postAction(url, data) {
        $.blockUI();
        $.post(url, data, function(result) {
            if (result.IsError === true) {
                model.IsError(result.IsError);
                $.unblockUI();
            } else {
                window.location.href = result.RedirectUrl;
            }
        });
    }
    
    function onOfferTypeChange(newValue) {
        $.blockUI();
        $.post(urls.ReconfigureModelUrl, model.ValidateModel(), function(data) {
            model.ResetValues();
            model.MinimumPaymentAmount(data.MinimumPaymentAmount);
            model.InterestRates(data.InterestRates);
            $.unblockUI();
        });
    };

    function onCancelReconfiguration(e) {
        e.preventDefault();
        model.IsError(false);
        window.VisitPay.Common.ModalGenericConfirmationMd(cms.Cancel.ContentTitle, cms.Cancel.ContentBody, 'Confirm', 'Exit').done(function() {
            postAction(urls.ReconfigureCancelUrl, model.CancelModel());
        });
    };

    function onSubmitReconfiguration(e) {
        e.preventDefault();
        model.IsError(false);
        window.VisitPay.Common.ModalGenericConfirmationMd(cms.Confirm.ContentTitle, cms.Confirm.ContentBody, 'Confirm', 'Cancel').done(function() {
            postAction(urls.ReconfigureSubmitUrl, model.SubmitModel());
        });
    };
    
    return {
        Initialize: function(data) {

            urls = {
                ReconfigureCancelUrl: data.ReconfigureCancelUrl,
                ReconfigureModelUrl: data.ReconfigureModelUrl,
                ReconfigureSubmitUrl: data.ReconfigureSubmitUrl,
                ReconfigureValidateUrl: data.ReconfigureValidateUrl
            };

            model = new window.VisitPay.ReconfigureFinancePlanViewModel(data);
            ko.applyBindings(model, $scope[0]);

            // common
            window.VisitPay.FinancePlanSetupCommon.BindEvents($scope, model, urls.ReconfigureValidateUrl);

            // events
            $scope.on('click', '#btnReconfigureCancel', onCancelReconfiguration);
            $scope.on('click', '#btnReconfigureSubmit', onSubmitReconfiguration);

            // subscriptions
            model.FinancePlanOfferSetTypeId.subscribe(onOfferTypeChange);

            // show the UI
            $scope.css('visibility', 'visible');
        }

    };

})();

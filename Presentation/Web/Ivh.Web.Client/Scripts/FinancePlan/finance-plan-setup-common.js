﻿/*
 * keep in sync with same file in patient
 * used in both reconfig and normal fp flows
 */

window.VisitPay.FinancePlanSetupCommon = (function() {
    
    function onValidateTerms(e, model, validateUrl, additionalParameters) {
        if (e) {
            e.preventDefault();
        }
        var promise = $.Deferred();
        $.blockUI();
        setTimeout(function() {
            var validateModel = model.ValidateModel();
            if (additionalParameters != null) {
                $.extend(validateModel, additionalParameters);
            }
            // there's something with the numeric plugin and knockout that causes the value to have a slight delay updating occassionally
            $.post(validateUrl, validateModel, function (result) {
                model.UpdateFromTerms(result);
                model.TermsCalculatedSinceLastChange(true); //We have calculated terms
                $.unblockUI();
                promise.resolve();
            });
        }, 500);
        return promise;
    };

    function setTooltips($scope) {
        $scope.find('[data-toggle="tooltip-html"]').tooltipHtml(true);
        $scope.tooltip({
            selector: '[data-toggle="tooltip"]',
            animation: false,
            container: 'body'
        });
    };

    var termsInputTimeout = null;
    function termsInputChanged(model) {
        //We need to recalculate
        model.TermsCalculatedSinceLastChange(false);

        //Wait 1 second after user stops typing in terms before showing terms alert
        clearTimeout(termsInputTimeout);
        termsInputTimeout = setTimeout(function () {
            //If the terms threshold isn't met after user has stopped typing, allow alert to be shown
            if (!model.TermsMeetThreshold()) {
                model.TermsAlertTimeoutMet(true);
            }
        }, 1000);
    }

    function bindEvents($scope, model, validateUrl, additionalParameters) {

        // focus/blur/input events for selector inputs
        $scope.find('#UserMonthlyPaymentAmount').on('focus', function() {
            $(this).attr('placeholder', '');
            model.OfferCalculationStrategy(1);
        });
        $scope.find('#UserMonthlyPaymentAmount').on('focusout', function() {
            var placeholder = $(this).attr('data-placeholder');
            $(this).attr('placeholder', placeholder);
        });
        $scope.find('#UserMonthlyPaymentAmount').on('input', function () {
            termsInputChanged(model);

            //Clear number payments, if applicable
            if (model.UserNumberMonthlyPayments.Any()) {
                model.UserNumberMonthlyPayments.Clear();
            }
        });
        $scope.find('#UserNumberMonthlyPayments').on('focus', function() {
            $(this).attr('placeholder', '');
            model.OfferCalculationStrategy(2);
        });
        $scope.find('#UserNumberMonthlyPayments').on('focusout', function() {
            var placeholder = $(this).attr('data-placeholder');
            $(this).attr('placeholder', placeholder);
        });
        $scope.find('#UserNumberMonthlyPayments').on('input', function () {
            termsInputChanged(model);

            //Clear payment amount, if applicable
            if (model.UserMonthlyPaymentAmount.Any()) {
                model.UserMonthlyPaymentAmount.Clear();
            }
        });
        $scope.find('.selector-inner label').on('click', function() {
            $(this).parent().find('input[type=text]').focus();
        });

        // validate terms
        var $calculateForm = $scope.find('#frmTerms');
        $calculateForm.find('#UserMonthlyPaymentAmount').makeNumericInput({ addClass: false });
        $calculateForm.find('#UserNumberMonthlyPayments').makeNumericInput({ allowDecimal: false, addClass: false });
        $calculateForm.off('submit').on('submit', function(e) {
            if (model.CanValidate()) {
                onValidateTerms(e, model, validateUrl, additionalParameters).done(function () {
                    setTooltips($scope);
                });
            } else {
                e.preventDefault();
            }
        });
        setTooltips($scope);
    };

    return {
        BindEvents: bindEvents,
        OnValidateTerms: onValidateTerms,
        SetTooltips: setTooltips
    };
})();
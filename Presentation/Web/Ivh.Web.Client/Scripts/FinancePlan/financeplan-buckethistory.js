﻿window.VisitPay.FinancePlanBucketHistory = (function (common, $) {

    window.VisitPay.Grids.FinancePlanBucketHistory = (function ($gridElement, filterModel, handlers) {

        var gridOpts = {
            autowidth: false,
            url: '/FinancePlan/FinancePlanBuckets',
            sortname: 'InsertDate',
            colModel: [
                {
                    label: 'Date Changed',
                    name: 'InsertDate',
                    sortable: false,
                    width: 200
                },
                {
                    label: 'Bucket',
                    name: 'Bucket',
                    sortable: false,
                    width: 100
                },
                {
                    label: 'Reason',
                    name: 'Comment',
                    sortable: false,
                    width: 450
                },
                {
                    label: 'Changed By',
                    name: 'VisitPayUserFullName',
                    sortable: false,
                    width: 115
                }
            ]
        };

        return new window.VisitPay.Grids.BaseGrid($gridElement, filterModel, $(), gridOpts, { noRecordsText: 'There is no finance plan aging history to display at this time.' }, { loadComplete: handlers.loadComplete });

    });
    
    function loadModal() {

        var promise = $.Deferred();

        common.ModalNoContainerAsync('modalFinancePlanBucketHistory', '/FinancePlan/FinancePlanBucketHistory', 'GET', null, false).done(function ($modal) {

            promise.resolve($modal);

        });

        return promise;
    }

    function bindGrid(visitPayUserId, financePlanId) {

        var filterModel = {
            GuarantorVisitPayUserId: ko.observable(visitPayUserId),
            FinancePlanId: ko.observable(financePlanId)
        };

        return new window.VisitPay.Grids.FinancePlanBucketHistory($('#gridFinancePlanBucketHistory'), filterModel, {
            loadComplete: function() {
                $.unblockUI();
            }
        });

    };
    
    return {
        Initialize: function (visitPayUserId, financePlanId) {
            
            $.blockUI();

            loadModal().done(function($modal) {

                bindGrid(visitPayUserId, financePlanId);
                $.unblockUI();
                $modal.modal('show');

            });

        }
    };

})(window.VisitPay.Common = window.VisitPay.Common || {}, jQuery);
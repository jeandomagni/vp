﻿window.VisitPay.FinancePlanInterestReallocate = (function(common, $) {

    function ViewModel() {

        var self = this;
        self.MaxReallocationAmount = ko.observable();
        self.InterestPaid = ko.observable();
        self.AmountToReallocate = ko.observable().extend({ minmaxMoney: { minValue: 0, maxValue: self.MaxReallocationAmount } });
        self.IsEnabled = ko.computed(function() {
            return ko.unwrap(self.AmountToReallocate) > 0;
        });
        return self;

    };

    function loadData(vpGuarantorId, financePlanId, model) {

        var promise = $.Deferred();

        $.post('/FinancePlan/FinancePlanInterestReallocate', { vpGuarantorId: vpGuarantorId, financePlanId: financePlanId }, function (data) {
            ko.mapping.fromJS(data, {}, model);
            promise.resolve();
        });

        return promise;

    };

    function loadModal() {

        var promise = $.Deferred();

        common.ModalNoContainerAsync('modalFinancePlanInterestReallocate', '/FinancePlan/FinancePlanInterestReallocate', 'GET', null, false).done(function ($modal) {

            promise.resolve($modal);

        });

        return promise;
    }

    function submit(vpGuarantorId, financePlanId, model) {

        var promise = $.Deferred();

        $.blockUI();
        $.post('/FinancePlan/ReallocateInterestToPrincipalFinancePlan', { vpGuarantorId: vpGuarantorId, financePlanId: financePlanId, amount: ko.unwrap(model.AmountToReallocate)}, function () {

            $.unblockUI();
            promise.resolve();

        });

        return promise;

    }

    return {
        Initialize: function(vpGuarantorId, financePlanId, functionToCallAfterSuccess) {

            var model = new ViewModel();
            var promise = $.Deferred();

            $.blockUI();
            loadData(vpGuarantorId, financePlanId, model).done(function(){

                loadModal().done(function($modal) {
                    
                    ko.applyBindings(model, $modal[0]);

                    $modal.on('click', 'button[type=submit]', function(e) {

                        e.preventDefault();
                        submit(vpGuarantorId, financePlanId, model).done(function() {
                            if (functionToCallAfterSuccess !== null && functionToCallAfterSuccess !== undefined && typeof (functionToCallAfterSuccess) === 'function') {
                                functionToCallAfterSuccess();
                            }
                            $modal.modal('hide');
                        });

                    });

                    //
                    $.unblockUI();
                    $modal.modal('show');
                });
            });
            return promise;

        }
    }

})(window.VisitPay.Common = window.VisitPay.Common || {}, jQuery);
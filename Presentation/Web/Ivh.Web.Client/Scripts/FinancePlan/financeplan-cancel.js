﻿window.VisitPay.FinancePlanCancel = (function(common, $) {

    function ViewModel() {

        var self = this;
        self.PendingVisitStateFullDisplayName = ko.observable();
        self.FinancePlan = {};

        return self;

    };

    function loadData(model, visitPayUserId, financePlanId) {

        var promise = $.Deferred();

        $.post('/FinancePlan/FinancePlanCancel', { guarantorVisitPayUserId: visitPayUserId, financePlanId: financePlanId }, function (data) {
            ko.mapping.fromJS(data, {}, model);
            promise.resolve();

        });

        return promise;

    };

    function loadModal() {

        var promise = $.Deferred();

        common.ModalNoContainerAsync('modalFinancePlanCancel', '/FinancePlan/FinancePlanCancel', 'GET', null, false).done(function ($modal) {

            promise.resolve($modal);

        });

        return promise;
    }

    function submit(visitPayUserId, financePlanId) {

        var promise = $.Deferred();

        $.blockUI();
        $.post('/FinancePlan/CancelFinancePlan', { guarantorVisitPayUserId: visitPayUserId, financePlanId: financePlanId}, function () {

            $.unblockUI();
            promise.resolve();

        });

        return promise;

    }

    return {
        Initialize: function(visitPayUserId, financePlanId) {

            var model = new ViewModel();

            var promise = $.Deferred();
            var hasChanged = false;

            $.blockUI();

            loadData(model, visitPayUserId, financePlanId).done(function() {

                loadModal(visitPayUserId, financePlanId).done(function($modal) {

                    //
                    $modal.on('hide.bs.modal.fpcancel', function() { promise.resolve(hasChanged); });

                    //
                    ko.applyBindings(model, $modal[0]);

                    $modal.on('click', 'button[type=submit]', function(e) {

                        e.preventDefault();
                        submit(visitPayUserId, financePlanId).done(function() {

                            hasChanged = true;
                            $modal.modal('hide');

                        });

                    });

                    //
                    $.unblockUI();
                    $modal.modal('show');

                });

            });

            return promise;

        }
    }

})(window.VisitPay.Common = window.VisitPay.Common || {}, jQuery);
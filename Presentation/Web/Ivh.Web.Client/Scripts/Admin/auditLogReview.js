﻿(function (common, $, ko) {

    var filterViewModel = function () {

        var self = this;

        self.BeginDate = ko.observable('');
        self.EndDate = ko.observable('');

        self.RunResults = ko.observable(false);
        self.ChangeNotifications = function () {

            var setRun = function () {
                self.RunResults(true);
            };

            self.BeginDate.subscribe(function () {
                setRun();
            });

            self.EndDate.subscribe(function () {
                setRun();
            });
        };
    };

    var model;

    function bindFilters() {

        // prevent focus on validate failure to prevent calendar from opening
        $('#jqGridFilter').on('invalid-form.validate', function (form, validator) {
            validator.settings && (validator.settings.focusInvalid = false);
        });

        //
        $('#jqGridFilter').find('.input-group.date').each(function () {
            var element = $(this);
            element.datepicker({
                clearBtn: true,
                format: 'mm/dd/yyyy',
                orientation: 'bottom left',
                autoclose: true
            }).on('hide', function () {
                if ($('#jqGridFilter').valid())
                    VisitPay.Common.ResetUnobtrusiveValidation($('#jqGridFilter'));
            });
        });

        

    }
    
    $(document).ready(function () {

        ko.cleanNode($('#jqGridFilter')[0]);
        model = new filterViewModel();
        model.ChangeNotifications();
        ko.applyBindings(model, $('#jqGridFilter')[0]);

        bindFilters();

        var gridAuditLogs = new window.VisitPay.Grids.BaseGrid($('#gridAuditLogs'), model, null,
        {
            url: '/Admin/AuditLogs',
            sortname: 'AuditDate',
            sortorder: 'desc',
            footerrow: false,
            rowList: [25, 50, 75, 100],
            rowNum: 25,
            colModel: [
                { width: 142, label: 'Audit Date', name: 'AuditDate' },
                { width: 60, label: 'User ID', name: 'VisitPayUserId' },
                { width: 120, label: 'User Name', name: 'UserName' },
                { width: 120, label: 'Event Type', name: 'EventType' },
                { width: 80, label: 'Audit Type', name: 'AuditType' },
                { width: 208, label: 'Audit Period', name: 'AuditPeriod' },
                { width: 264, label: 'Notes', name: 'Notes', sortable: false }
            ]
        }, { noRecordsText: 'There are no audit log events to display at this time' }, { loadComplete: function () { $.unblockUI(); } });

        $('#btnAuditLogsExport').on('click', function (e) {

            e.preventDefault();
            gridAuditLogs.exportData();
        });

        var grids = [gridAuditLogs];

        $('#jqGridFilter').on('submit', function (e) {
            e.preventDefault();
            if ($(this).valid()) {

                $.blockUI();

                for (var i = 0; i < grids.length; i++) {
                    grids[i].reload();
                }

            };
        });

        $('#btnGridFilterReset').on('click', function (e) {
            e.preventDefault();

            $.blockUI();

            for (var i = 0; i < grids.length; i++) {
                grids[i].reset();
            }

            window.VisitPay.Common.ResetUnobtrusiveValidation($(this).parents('form').eq(0));
        });
    });

})(window.VisitPay.Common, jQuery, ko);
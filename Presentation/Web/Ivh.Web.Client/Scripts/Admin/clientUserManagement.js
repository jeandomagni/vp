﻿(function (common, $, ko) {

    var filterViewModel = function () {

        var self = this;

        self.LastName = ko.observable('');
        self.FirstName = ko.observable();
        self.UserName = ko.observable('');
        self.VisitPayRoleId = ko.observable('');
        self.LastIvhUserAudit = ko.observable('');
        self.LastClientUserAudit = ko.observable('');

        self.RunResults = ko.observable(false);
        self.ChangeNotifications = function () {

            var setRun = function () {
                self.RunResults(true);
            };

            self.LastName.subscribe(setRun);
            self.FirstName.subscribe(setRun);
            self.UserName.subscribe(setRun);
            self.VisitPayRoleId.subscribe(setRun);

        };
    };

    $(document).ready(function () {

        //
        var model = new filterViewModel();
        ko.applyBindings(model, $('#jqGridFilter')[0]);
        model.ChangeNotifications();

        $('#btnCertifyAudit').on('click', function (e) {
            e.preventDefault();
            $('#modalCertifyAudit').modal();
        });

        $('#btnComplete').on('click', function (e) {
            e.preventDefault();
            $.blockUI();

            $.post('/Admin/CertifyAudit', {
                notes: $('#txtNotes').val()
            }, function (result) {
                if (result.Result === true) {
                    window.location.href = result.Message;
                }
            });
        });

        $('#btnCancel').on('click', function () {
            $('#txtNotes').val('');
        });

        var gridUser = new window.VisitPay.Grids.BaseGrid($('#gridUsers'), model, null,
        {
            url: '/Admin/ClientUserManagement',
            jsonReader: { root: 'ClientUsers' },
            sortname: 'LastName',
            sortorder: 'asc',
            footerrow: false,
            rowList: [25, 50, 75, 100],
            rowNum: 25,
            autowidth:false,
            colModel: [
                { width: 100, label: 'Last Name', name: 'LastName', resizable: false,formatter: function (cellValue, options, rowObject) {
                        return '<a href="/Admin/ClientUser/' + rowObject.VisitPayUserId + '" title="' + cellValue + '">' + cellValue + '</a>';
                    }
                },
                { width: 100, label: 'First Name', name: 'FirstName', resizable: false },
                { width: 192, label: 'Username', name: 'UserName', resizable: false,
                    formatter: function (cellValue, options, rowObject) {
                        return '<a href="/Admin/ClientUser/' + rowObject.VisitPayUserId + '" title="' + cellValue + '">' + cellValue + '</a>';
                    }
                },
                { width: 190, label: 'Role(s)', name: 'Roles', resizable: false, sortable: false },
                { width: 140, label: 'Date Added', name: 'InsertDate', resizable: false, classes: 'text-right' },
                { width: 140, label: 'Last Login', name: 'LastLoginDate', resizable: false, classes: 'text-right' },
                { width: 60, label: 'Locked', name: 'IsLocked', resizable: false, classes: 'text-center' },
                { width: 70, label: 'Disabled', name: 'IsDisabled', resizable: false, classes: 'text-center' }
            ]
        }, { noRecordsText: 'There are no client users to display at this time.' }, { loadComplete: function () { $.unblockUI(); } });

        //
        $('#jqGridFilter').on('submit', function (e) {
            e.preventDefault();
            if ($(this).valid()) {
                $.blockUI();
                gridUser.reload();
                model.RunResults(false);
            }
        });
        $('#btnGridFilterReset').on('click', function (e) {
            e.preventDefault();
            $.blockUI();
            gridUser.reset();

            window.VisitPay.Common.ResetUnobtrusiveValidation($(this).parents('form').eq(0));
        });

        $('#btnUserExport').on('click', function (e) {

            e.preventDefault();
            gridUser.exportData();
        });
    });

})(window.VisitPay.Common, jQuery, ko);
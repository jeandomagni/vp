﻿(function (common, $, ko) {

    var filterViewModel = function () {

        var self = this;

        self.BeginDate = ko.observable(initialData.StartTime);
        self.BeginTime = ko.observable(initialData.StartTime);
        self.EndDate = ko.observable(initialData.StopTime);
        self.EndTime = ko.observable(initialData.StopTime);

        self.RunResults = ko.observable(false);
        self.ChangeNotifications = function () {

            var setRun = function () {
                self.RunResults(true);
            };

            self.BeginDate.subscribe(function () {
                self.BeginTime(self.BeginDate() + ' 12:00:00 AM');
                setRun();
            });

            self.EndDate.subscribe(function () {
                self.EndTime(self.EndDate() + ' 11:59:59 PM');
                setRun();
            });
        };
    };

    var model;

    function bindFilters() {

        // prevent focus on validate failure to prevent calendar from opening
        $('#jqGridFilter').on('invalid-form.validate', function (form, validator) {
            validator.settings && (validator.settings.focusInvalid = false);
        });

        //
        $('#jqGridFilter').find('.input-group.date').each(function () {
            var element = $(this);
            element.datepicker({
                clearBtn: true,
                format: 'mm/dd/yyyy',
                orientation: 'bottom left',
                autoclose: true
            }).on('hide', function () {
                if ($('#jqGridFilter').valid())
                    VisitPay.Common.ResetUnobtrusiveValidation($('#jqGridFilter'));
            });
        });

        

    }
    
    $(document).ready(function () {

        ko.cleanNode($('#jqGridFilter')[0]);
        model = new filterViewModel();
        model.ChangeNotifications();
        ko.applyBindings(model, $('#jqGridFilter')[0]);

        bindFilters();

        var gridUserAccess = new window.VisitPay.Grids.BaseGrid($('#gridUserAccess'), model, null,
        {
            url: '/Admin/PciUserAccess',
            sortname: 'EventDate',
            sortorder: 'asc',
            footerrow: false,
            rowList: [25, 50, 75, 100],
            rowNum: 25,
            colModel: [
                { width: 120, label: 'User Name', name: 'UserName' },
                { width: 204, label: 'Email', name: 'Email' },
                { width: 120, label: 'First Name', name: 'FirstName' },
                { width: 120, label: 'Last Name', name: 'LastName' },
                { width: 146, label: 'Event Date', name: 'EventDate' },
                { width: 142, label: 'Event Type', name: 'EventType' },
                { width: 142, label: 'IP Address', name: 'IpAddress' }
            ]
        }, { noRecordsText: 'There are no user access events to display at this time' }, { loadComplete: function () { $.unblockUI(); } });

        $('#btnUserActiveExport').on('click', function (e) {

            e.preventDefault();
            gridUserAccess.exportData();
        });

        var gridUserChange = new window.VisitPay.Grids.BaseGrid($('#gridUserChange'), model, null,
        {
            url: '/Admin/PciUserChange',
            sortname: 'EventDate',
            sortorder: 'asc',
            footerrow: false,
            rowList: [25, 50, 75, 100],
            rowNum: 25,
            colModel: [
                { width: 110, label: 'User Name', name: 'UserName' },
                { width: 110, label: 'Email', name: 'Email' },
                { width: 110, label: 'First Name', name: 'FirstName' },
                { width: 110, label: 'Last Name', name: 'LastName' },
                { width: 110, label: 'Event Date', name: 'EventDate' },
                { width: 110, label: 'Event Type', name: 'EventType' },
                { width: 110, label: 'IP Address', name: 'IpAddress' },
                { width: 110, label: 'Target User Name', name: 'TargetUserName' },
                { width: 110, label: 'Target VP User ID', name: 'TargetVisitPayUserId' }
            ]
        }, { noRecordsText: 'There are no user change events to display at this time' }, { loadComplete: function () { $.unblockUI(); } });

        $('#btnUserChangeExport').on('click', function (e) {

            e.preventDefault();
            gridUserChange.exportData();
        });

        var gridUserChangeDetail = new window.VisitPay.Grids.BaseGrid($('#gridUserChangeDetails'), model, null,
        {
            url: '/Admin/PciUserChangeDetails',
            sortname: 'ChangeTime',
            sortorder: 'asc',
            footerrow: false,
            rowList: [25, 50, 75, 100],
            rowNum: 25,
            colModel: [
                { width: 66, label: 'Change Time', name: 'ChangeTime' },
                { width: 66, label: 'Expired Time', name: 'ExpiredTime' },
                { width: 66, label: 'VP User ID', name: 'VisitPayUserId' },
                { width: 66, label: 'Prior User Name', name: 'PriorUserName' },
                { width: 66, label: 'User Name', name: 'UserName' },
                { width: 66, label: 'Prior Email', name: 'PriorEmail' },
                { width: 66, label: 'Email', name: 'Email' },
                { width: 66, label: 'Prior First Name', name: 'PriorFirstName' },
                { width: 66, label: 'First Name', name: 'FirstName' },
                { width: 66, label: 'Prior Last Name', name: 'PriorLastName' },
                { width: 66, label: 'Last Name', name: 'LastName' },
                { width: 66, label: 'Prior Lockout Enabled', name: 'PriorLockoutEnabled' },
                { width: 66, label: 'Lockout Enabled', name: 'LockoutEnabled' },
                { width: 66, label: 'Prior Lockout End Date UTC', name: 'PriorLockoutEndDateUtc' },
                { width: 66, label: 'Lockout End Date UTC', name: 'LockoutEndDateUtc' }
            ]
        }, { noRecordsText: 'There are no user change detail events to display at this time' }, { loadComplete: function () { $.unblockUI(); } });

        $('#btnUserChangeDetailExport').on('click', function (e) {

            e.preventDefault();
            gridUserChangeDetail.exportData();
        });

        var gridRoleChange = new window.VisitPay.Grids.BaseGrid($('#gridRoleChange'), model, null,
        {
            url: '/Admin/PciRoleChange',
            sortname: 'ChangeTime',
            sortorder: 'asc',
            footerrow: false,
            rowList: [25, 50, 75, 100],
            rowNum: 25,
            colModel: [
                { width: 142, label: 'VP User ID', name: 'VisitPayUserId' },
                { width: 142, label: 'User Name', name: 'UserName' },
                { width: 142, label: 'Visit Pay Role ID', name: 'VisitPayRoleId' },
                { width: 142, label: 'Change Event ID', name: 'ChangeEventId' },
                { width: 142, label: 'Change Time', name: 'ChangeTime' },
                { width: 142, label: 'Expired Time', name: 'ExpiredTime' },
                { width: 142, label: 'Prior Delete', name: 'PriorDelete' }
            ]
        }, { noRecordsText: 'There are no role change events to display at this time' }, { loadComplete: function () { $.unblockUI(); } });

        $('#btnRoleChangeExport').on('click', function (e) {
            e.preventDefault();
            gridRoleChange.exportData();
        });

        var grids = [
            gridUserAccess,
            gridUserChange,
            gridUserChangeDetail,
            gridRoleChange
        ];

        $('#jqGridFilter').on('submit', function (e) {
            e.preventDefault();
            if ($(this).valid()) {

                $.blockUI();

                for (var i = 0; i < grids.length; i++) {
                    grids[i].reload();
                }

            };
        });

        $('#btnGridFilterReset').on('click', function (e) {
            e.preventDefault();

            $.blockUI();

            for (var i = 0; i < grids.length; i++) {
                grids[i].reset();
            }

            window.VisitPay.Common.ResetUnobtrusiveValidation($(this).parents('form').eq(0));
        });

        $('#btnConfirmAudit').on('click', function(e) {
            e.preventDefault();
            $('#modalConfirmAudit').modal();
        });

        $('#btnComplete').on('click', function(e) {
            e.preventDefault();
            $.blockUI();

            $.post('/Admin/ConfirmAudit', {
                filterModel: ko.toJS(model),
                notes: $('#txtNotes').val()
                }, function (result) {
                if (result.Result === true) {
                    window.location.href = result.Message;
                }
                });
        });

        $('#btnCancel').on('click', function() {
            $('#txtNotes').val('');
        });

    });

})(window.VisitPay.Common, jQuery, ko);
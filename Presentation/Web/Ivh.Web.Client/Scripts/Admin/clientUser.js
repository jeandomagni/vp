﻿(function($, common) {

    $(document).ready(function () {

        var secureCodeAddUserText = "The secure code, along with a temporary password that will be sent automatically via email, is required for new users to login the first time.<br><br>Please provide the secure code to the new user prior to his or her first login attempt. For security purposes, since the user's temporary password will be emailed, please avoid sending this code to the user via email.";
        var secureCodeEditUserText = "A secure code, along with a temporary password that will be sent automatically via email, is required for login following an account reset. Once the user is logged in, he or she will be required to setup a new password and security questions.<br><br>Please provide the secure code to the user prior to his or her next login attempt.<br><br>For security purposes, since the user's temporary password will be emailed, please avoid sending this code to the user via email.";
        
        var visitPayUserId = $('#section-clientuser').find('#VisitPayUserId').val();
        var addUser = visitPayUserId == 0;
        var isSecureCodeRequired = $('#IsSecureCodeRequired').Value;

        // Enabled Secure Code controls if adding a new account / disabled if editing an existing account
        $('#SecureCodeLabel').prop('disabled', !addUser);
        $('#btnGenerateSecureCode').prop('disabled', !addUser);

        if (addUser) {
            $('#secureCodeHelp').html(secureCodeAddUserText);
        } else {
            $('#secureCodeHelp').html('');
        }


        $('#IsLocked').on('change', function(e) {

            if ($(this).prop('checked'))
                $('#IsDisabled').prop('checked', false);

        });

        $('#IsDisabled').on('change', function(e) {

            if ($(this).prop('checked'))
                $('#IsLocked').prop('checked', false);

        });

        // reset account checkmark makes Secure Code controls enabled/disabled
        $('#IsReset').on('change', function (e) {

            var chkd = $(this).prop('checked');

            // if account reset is requested, then secure code is required on user edit
            $('#IsSecureCodeRequired').val(chkd);
            $('#SecureCodeLabel').prop('disabled', !chkd);
            $('#btnGenerateSecureCode').prop('disabled', !chkd);
            $('#SecurityValidationValue').val(null);
            $('#SecurityValidationValue').prop('disabled', !chkd);
            $('#btnResetPassword').prop('disabled', chkd);
            if (chkd) {
                $('#secureCodeHelp').html(secureCodeEditUserText);
                generateSecureCode($('#SecurityValidationValue'));
            } else {
                $('#secureCodeHelp').html('');
                $('#SecurityValidationValue').val('');
            }
        });

        // secure code
        function generateSecureCode($target) {

            $.blockUI();
            $.post('/Admin/ClientUserGenerateSecureCode', function (result) {
                $.unblockUI();
                $target.val(result);
            });

        }

        // reset password
        $(document).on('click', '#section-clientuser #btnResetPassword', function (e) {

            e.preventDefault();
            $.blockUI();
            $.post('/Admin/ClientUserResetPassword', { visitPayUserId: visitPayUserId }, function(success) {
                $.unblockUI();
                var text = success ? 'Password reset successfully.' : 'Password reset failed';
                common.ModalGenericAlert('Reset Password', text, 'Ok');
            });

        });

        // generate secure code for non-modal Secure Code textbox
        $(document).on('click', '#section-clientuser #btnGenerateSecureCode', function (e) {

            e.preventDefault();
            $.blockUI();
            var txtSecureCode = $('#section-clientuser').find('#SecurityValidationValue');
            generateSecureCode(txtSecureCode);
        });

        // reset account
        function resetAccount(e) {

            e.preventDefault();
            var $form = $(this);

            if ($form.valid()) {
                $.blockUI();

                var modal = $form.parents('.modal')[0];
                $(modal).modal('hide');

                $.post('/Admin/ClientUserResetAccount', { visitPayUserId: visitPayUserId, secureCode: $form.find('#txtSecureCode').val() }, function (success) {
                    $.unblockUI();
                    var text = success ? 'Account reset successfully.' : 'Account reset failed';
                    common.ModalGenericAlert('Reset Account', text, 'Ok');
                });
            }
        }

        $('#modalResetAccount').on('submit', 'form', resetAccount);

        $('#modalResetAccount').on('click', '#btnGenerateSecureCodeModal', function () {
            generateSecureCode($('#txtSecureCode'));
        });

        $(document).on('click', '#section-clientuser #btnResetAccount', function(e) {

            e.preventDefault();
            common.ResetUnobtrusiveValidation($('#modalResetAccount').find('form'));
            $('#modalResetAccount').find('input').val('');
            $('#modalResetAccount').modal('show');
            setTimeout(function() {
                $('#modalResetAccount').find('input:eq(0)').focus();
            }, 500);


        });

    });

})(jQuery, window.VisitPay.Common);
﻿(function (common, $, ko, journalEventLogFilter) {
    
    journalEventLogFilter.Main = (function(data) {

    var filterViewModel = function () {

            var self = this;

            if (data) {
                self.BeginDate = ko.observable(data.BeginDate || '');
                self.EndDate = ko.observable(data.EndDate || '');
                self.UserNameFilter = ko.observable(data.UserNameFilter || '');
                self.VpGuarantorId = ko.observable(data.VpGuarantorId || '');

                //Event Type
                self.JournalEventType = ko.observable(data.JournalEventType || '');

                //Event Trigger
                self.JournalEventCategory = ko.observable(data.JournalEventCategory || '');
            }
            else {
                self.BeginDate = ko.observable('');
                self.EndDate = ko.observable('');
                self.UserNameFilter = ko.observable('');
                self.VpGuarantorId = ko.observable('');

                //Event Type
                self.JournalEventType = ko.observable('');

                //Event Trigger
                self.JournalEventCategory = ko.observable('');
            }


            self.RunResults = ko.observable(false);
            self.ChangeNotifications = function () {

                var setRun = function () {
                    self.RunResults(true);
                };

                self.BeginDate.subscribe(setRun);
                self.EndDate.subscribe(setRun);
                self.UserNameFilter.subscribe(setRun);
                self.VpGuarantorId.subscribe(setRun);
                self.JournalEventType.subscribe(setRun);
                self.JournalEventCategory.subscribe(setRun);
            };

            self.PostModel = {
                JournalEventCategory: ko.unwrap(self.JournalEventCategory),
                JournalEventType: ko.unwrap(self.JournalEventType),
                BeginDate: ko.unwrap(self.BeginDate),
                EndDate: ko.unwrap(self.EndDate),
                UserNameFilter: ko.unwrap(self.UserNameFilter),
                VpGuarantorId: ko.unwrap(self.VpGuarantorId)
            };
        };

        var model;

        function bindFilters() {

            // prevent focus on validate failure to prevent calendar from opening
            $('#jqGridFilterJournalEvents').on('invalid-form.validate', function (form, validator) {
                validator.settings && (validator.settings.focusInvalid = false);
            });

        }

        $(document).ready(function () {

            ko.cleanNode($('#jqGridFilterJournalEvents')[0]);
            model = new filterViewModel();
            model.ChangeNotifications();
            ko.applyBindings(model, $('#jqGridFilterJournalEvents')[0]);
            setUiComponents();
            bindFilters();
            common.ResetUnobtrusiveValidation($('#jqGridFilterJournalEvents'));

            var gridJournalEventLogs = new window.VisitPay.Grids.BaseGrid($('#gridJournalEventLogs'), model, null,
                {
                    url: '/Admin/AuditEventLog',
                    sortname: 'EventDate',
                    sortorder: 'desc',
                    footerrow: false,
                    rowList: [25, 50, 75, 100],
                    rowNum: 25,
                    colModel: [
                        {
                            width: 547,
                            label: 'Event Description',
                            name: 'Message',
                            formatter: function (cellValue, options, rowObject) {

                                var dict = rowObject.AdditionalDataDictionary;

                                if (Object.keys(dict).length > 0) {

                                    var tooltip = '<div>';
                                    for (var key in dict) {
                                        if (dict.hasOwnProperty(key)) {
                                            tooltip += '<div><span>' + key + ': </span><span>' + dict[key] + '</span></div>';
                                        }
                                    }
                                    tooltip += '</div>';

                                    return '<a href="javascript:;" data-toggle="tooltip" data-placement="right" data-html="true" title data-original-title="' + tooltip + '">' + cellValue + '</a>';

                                } else {

                                    return '<a href="javascript:;">' + cellValue + '</a>';
                                }

                            }
                        },
                        {
                            width: 171,
                            label: 'Date/Time',
                            name: 'EventDate'
                        },
                        {
                            width: 137,
                            label: 'User Name',
                            name: 'UserName',
                            formatter: function (cellValue, options, rowObject) {
                                if (rowObject.VpGuarantorIdUserName !== 0) {
                                    return '<a href="/Search/GuarantorAccount/' + rowObject.VpGuarantorIdUserName + '">' + cellValue + '</a>';
                                } else {
                                    return cellValue;
                                }
                            }
                        },
                        {
                            width: 137,
                            label: 'VP Guarantor ID',
                            name: 'VpGuarantorId',
                            formatter: function (cellValue, options, rowObject) {
                                if (rowObject.VpGuarantorId !== 0) {
                                    return '<a href="/Search/GuarantorAccount/' + rowObject.VpGuarantorId + '">' + cellValue + '</a>';
                                } else {
                                    return cellValue;
                                }
                            }
                        }
                    ]
                }, { noRecordsText: 'There are no journal event logs to display at this time' }
                , {
                    loadComplete: function (data) {

                        if (data.Results != null && data.Results.length > 0) {
                            gridJournalEventLogs.jqg.find('.jqgrow').each(function () {
                                $(this).on('click', 'td:eq(0) > a', function () {
                                    var rowData = data.Results[$(this).parents('tr').index() - 1];
                                    $.blockUI();

                                    common.ModalContainerAsync($('#modalJournalEventLogDetailsContainer'), '/Admin/AuditEventLogDetails?eventJournalId=' + rowData.EventHistoryId, 'GET', null, true).done(function ($modal) {
                                        $.unblockUI();
                                    });
                                });
                            });
                        }

                        $.unblockUI();

                    }
                });

            $('#btnJournalEventLogExport').on('click', function (e) {
                e.preventDefault();
                gridJournalEventLogs.exportData();
            });

            var grids = [gridJournalEventLogs];

            $('#jqGridFilterJournalEvents').on('submit', function (e) {
                e.preventDefault();
                if ($(this).valid()) {

                    model.RunResults(false);

                    $.blockUI();

                    for (var i = 0; i < grids.length; i++) {
                        grids[i].reload();
                    }
                }

            });

            $('#btnGridFilterJournalEventsReset').on('click', function (e) {
                e.preventDefault();

                $.blockUI();

                for (var i = 0; i < grids.length; i++) {
                    grids[i].reset();
                }
                common.ResetUnobtrusiveValidation($('#jqGridFilterJournalEvents'));
            });

        });

    });
    
})(window.VisitPay.Common, jQuery, ko, window.VisitPay.JournalEventLogFilter = window.VisitPay.JournalEventLogFilter || {});
﻿(function(visitPay, $, ko) {

    var filterViewModel = function () {

        var self = this;

        self.DateRangeFrom = ko.observable('');
        self.HsGuarantorSourceSystemKey = ko.observable('');
        self.SpecificDateFrom = ko.observable('');
        self.SpecificDateTo = ko.observable('');
        self.VpGuarantorId = ko.observable('');

        self.RunResults = ko.observable(false);
        self.ChangeNotifications = function () {

            var setRun = function () {
                self.RunResults(true);
            };

            self.DateRangeFrom.subscribe(function(value) {
                setRun();
                if (value && value.length > 0) {
                    self.SpecificDateFrom('');
                    self.SpecificDateTo('');
                }
            });
            self.HsGuarantorSourceSystemKey.subscribe(setRun);
            self.SpecificDateFrom.subscribe(function(value) {
                setRun();
                if (value && value.length > 0) {
                    self.DateRangeFrom('');
                }
            });
            self.SpecificDateTo.subscribe(function(value) {
                setRun();
                if (value && value.length > 0) {
                    self.DateRangeFrom('');
                }
            });
            self.VpGuarantorId.subscribe(setRun);

        };

    };

    var $scope = $('#section-guarantorsummaryreport');
    var $grid = $('table#jqGrid');

    var gridDefaultSortName = 'InsertDate';
    var gridDefaultSortOrder = 'asc';

    var model;

    function bindFilters() {

        // prevent focus on validate failure to prevent calendar from opening
        $('#jqGridFilter').on('invalid-form.validate', function (form, validator) {
            validator.settings && (validator.settings.focusInvalid = false);
        });

        //
        $('#jqGridFilter').find('.input-group.date').each(function () {
            var element = $(this);
            element.datepicker({
                clearBtn: true,
                format: 'mm/dd/yyyy',
                orientation: 'bottom left',
                autoclose: true
            }).on('hide', function () {
                if ($('#jqGridFilter').valid())
                    VisitPay.Common.ResetUnobtrusiveValidation($('#jqGridFilter'));
            });
        });

    }

    function gridGridComplete() {

        $scope.find('#jqGridPager').find('#input_jqGridPager').find('input').off('change').on('change', function() {
            $grid.jqGrid().trigger('reloadGrid', [{ page: $(this).val() }]);
        });

        $grid.find('[data-toggle="tooltip"]').each(function () {
            $(this).tooltip({
                animation: false,
                html: true,
                title: '<div style="max-width: 350px; white-space: normal;">' + $(this).data('tip') + '</div>',
                container: 'body'
            });
        });

        $grid.parents('.ui-jqgrid:eq(0)').addClass('visible');

    };

    function gridLoadComplete(data) {

        if (data.Data && data.Data.length > 0) {

            $grid.parents('.ui-jqgrid:eq(0)').show();
            $('#jqGridNoRecords').removeClass('in');

        } else {

            $grid.parents('.ui-jqgrid:eq(0)').hide();
            $('#jqGridNoRecords').addClass('in');
        }

        model.RunResults(false);
        $.unblockUI();

    };

    function unmatchReasonFormatter(cellValue, options, rowObject) {

        var $status = $('<div/>').text(cellValue);

        if (rowObject.ActionNotes && rowObject.ActionNotes.length > 0) {
            $status.prepend($('<em/>').addClass('pull-right glyphicon glyphicon-comment'));
            $status.attr('data-tip', rowObject.ActionNotes);
            $status.attr('data-toggle', 'tooltip');
            $status.attr('data-placement', 'top');
        }

        return $('<div/>').append($status).html();
    };

    function buildGrid() {

        var colModel = [
            { width: 96, label: 'Insert Date', name: 'InsertDate' },
            { width: 96, label: 'Change Date', name: 'ActionDate' },
            { width: 76, label: 'Status', name: 'HsGuarantorMatchDiscrepancyStatusName' },
            { width: 93, label: visitPay.ClientSettings.HsGuarantorPatientIdentifier, name: 'HsGuarantorSourceSystemKey' },
            { width: 100, label: visitPay.ClientSettings.VpGuarantorPatientIdentifier, name: 'VpGuarantorId' },
            { width: 93, label: visitPay.ClientSettings.HsGuarantorPatientIdentifier, name: 'HsGuarantorSourceSystemKeyCurrent' },
            { width: 100, label: visitPay.ClientSettings.VpGuarantorPatientIdentifier, name: 'VpGuarantorIdCurrent' },
            { width: 242, label: 'Status Reason', name: 'HsGuarantorUnmatchReasonDisplayName', formatter: unmatchReasonFormatter },
            { width: 95, label: 'Username', name: 'ActionUserName' }
        ];

        for (var i = 0; i < colModel.length; i++) {
            colModel[i].resizable = false;
        }

        $grid.jqGrid({
            url: '/Reporting/GuarantorSummaryReport',
            postData: { model: ko.toJS(model) },
            jsonReader: { root: 'Data' },
            autowidth: false,
            pager: 'jqGridPager',
            sortname: gridDefaultSortName,
            sortorder: gridDefaultSortOrder,
            rowList: [25, 50, 75, 100],
            loadComplete: gridLoadComplete,
            gridComplete: gridGridComplete,
            colModel: colModel
        });

        $grid.jqGrid('setGroupHeaders', {
            useColSpanStyle: false,
            groupHeaders: [
                { startColumnName: 'InsertDate', numberOfColumns: 3, titleText: '' },
                { startColumnName: 'HsGuarantorSourceSystemKey', numberOfColumns: 2, titleText: 'Previous Match' },
                { startColumnName: 'HsGuarantorSourceSystemKeyCurrent', numberOfColumns: 2, titleText: 'Current Match' },
                { startColumnName: 'HsGuarantorUnmatchReasonDisplayName', numberOfColumns: 2, titleText: '' }
            ]
        });

    }

    function reloadGrid() {
        $.extend($grid.jqGrid('getGridParam', 'postData'), { model: ko.toJS(model) });
        $grid.setGridParam({ page: 1 });
        $grid.sortGrid(gridDefaultSortName, true, gridDefaultSortOrder);
    };

    function resetModel() {

        model.DateRangeFrom('-30');
        model.HsGuarantorSourceSystemKey('');
        model.SpecificDateFrom('');
        model.SpecificDateTo('');
        model.VpGuarantorId('');
        model.RunResults(false);

    };

    $(document).ready(function() {

        ko.cleanNode($('#jqGridFilter')[0]);
        model = new filterViewModel();
        model.ChangeNotifications();
        ko.applyBindings(model, $('#jqGridFilter')[0]);
        resetModel();

        bindFilters();

        buildGrid();

        $('#jqGridFilter').on('submit', function (e) {
            e.preventDefault();
            if ($(this).valid()) {
                $.blockUI();
                reloadGrid();
            }
        });

        $('#btnGridFilterReset').on('click', function () {
            $.blockUI();
            resetModel();
            reloadGrid();
        });

        $('#btnExport').on('click', function (e) {

            e.preventDefault();

            $.blockUI();

            $.post('/Reporting/GuarantorSummaryReportExport', {
                model: ko.toJS(model),
                sidx: $grid.jqGrid('getGridParam', 'sortname'),
                sord: $grid.jqGrid('getGridParam', 'sortorder')
            }, function (result) {
                $.unblockUI();
                if (result.Result === true) {
                    window.location.href = result.Message;
                }
            }, 'json');

        });

    });

})(window.VisitPay || {}, window.jQuery, window.ko);
﻿(function ($, common) {

    $(document).ready(function () {

        var ensureUniqueSelection = function () {
            var selects = $('select[name$=".SecurityQuestionId"]');
            selects.each(function () {

                var self = $(this);
                self.find('option').prop('disabled', false);

                selects.not(self).each(function () {
                    if ($(this).val().length > 0) {
                        self.find('option[value="' + $(this).val() + '"]').attr('disabled', 'disabled');
                    }
                });

            });
        };

        $('select[name$=".SecurityQuestionId"]').each(ensureUniqueSelection);
        $('select[name$=".SecurityQuestionId"]').on('change', function () {
            ensureUniqueSelection();
            $(this).parents('.row:eq(0)').find('input[type=text][name$=".Answer"]').val('');
        });

        $('#frmSecurityQuestions').on('submit', function (e) {
            e.preventDefault();
            var $form = $(this);
            if ($form.valid()) {
                $.blockUI();
                $.post($form.attr('action'), $form.serialize(), function(result) {
                    $.unblockUI();
                    common.ModalGenericAlert('Security Questions', result ? 'Security Questions Updated' : 'Security Questions update failed.', 'Ok').done(function () {
                        if (result) {
                            window.location.href = '/Home';
                        } else {
                            window.location.reload();
                        }
                    });
                });
            }
        });

    });

})(jQuery, window.VisitPay.Common);
﻿(function (visitPay, $) {

    $(document).ready(function () {

        var $scope = $('#section-forgotpassword');
        var $form = $scope.find('form');

        visitPay.Common.ResetUnobtrusiveValidation($form);

        $form.off('submit').on('submit', function(e) {

            var $this = $(this);

            e.preventDefault();
            visitPay.Common.ResetValidationSummary($this);

            if (!$form.valid()) {
                return;
            }

            $.blockUI();

            $.post($form.attr('action'), $form.serialize() + '', function(data) {

                $.unblockUI();
                
                if (data.Result) {

                    $('#modalSuccess').modal('show');

                } else {

                    visitPay.Common.AppendMessageToValidationSummary($this, data.Message);

                }

            });

        });

    });

})(window.VisitPay, jQuery);
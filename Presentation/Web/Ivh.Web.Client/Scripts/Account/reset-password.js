﻿(function (visitPay, $) {
    $(document).ready(function () {

        visitPay.Common.DisablePaste($('#section-resetpassword input[type=password]'));
        visitPay.Common.DisablePaste($('#section-resetpassword').find('#UserName'));

        // tooltip
        $('#section-resetpassword a.glyphicon-info-sign').each(function (e) {
            var $element = $(this);
            var title = $element.attr('title');
            $element.removeAttr('title');
            $element.tooltip({
                animation: false,
                container: 'body',
                html: true,
                title: '<span style="display: block; width: 480px;">' + title + '</span>'
            });
        });

        // clear security answer
        function clearSecurityAnswer() {
            var $securityAnswer = $('#section-resetpassword').find('#SecurityAnswer');
            $securityAnswer.val('');
        }

        // clear security question
        function clearSecurityQuestion() {
            var $txtQuestionId = $('#section-resetpassword').find('#SecurityQuestionId');
            $txtQuestionId.val('0');
            var $securityQuestion = $('#section-resetpassword').find('#securityQuestionText');
            $securityQuestion.html('Security question was not found for the user.');
            $securityQuestion.addClass('validation-summary-errors');
        }

        function enableSecurityAnswer(enable) {
            var $securityAnswer = $('#section-resetpassword').find('#SecurityAnswer');
            $securityAnswer.prop('disabled', !enable);
        }

        function loadSecurityQuestion(e) {
            clearSecurityQuestion();
            clearSecurityAnswer();
            changeSecurityQuestion(e);
        }

        // change security question
        function changeSecurityQuestion(e) {

            e.preventDefault();

            var $txtQuestionId = $('#section-resetpassword').find('#SecurityQuestionId');
            var $txtUserName = $('#section-resetpassword').find('#UserName');
            var $securityQuestionText = $('#section-resetpassword').find('#securityQuestionText');           

            visitPay.Common.ResetUnobtrusiveValidation($('#section-resetpassword').find('form'));

            clearSecurityAnswer();

            $.blockUI();
            $.ajax({
                type: "POST",
                url: '/Account/GetRandomQuestionAsJson',
                data: { userName: $txtUserName.val(), currentQuestionId: $txtQuestionId.val() },
                success: function (result) {
                    $.unblockUI();
                    $txtQuestionId.val(result.SecurityQuestionId);
                    $securityQuestionText.html(result.Question);
                    $securityQuestionText.removeClass('validation-summary-errors');
                    enableSecurityAnswer(true);
                },
                global: false //avoid global error handler "$(document).ajaxError" in common.js
            })
                .fail(function () {
                    $.unblockUI();
                    clearSecurityQuestion(); 
                    enableSecurityAnswer(false);
                });
        }

        // change security question for non-modal question textbox
        $(document).on('click', '#section-resetpassword #btnChangeQuestion', changeSecurityQuestion);
        $(document).on('change', '#section-resetpassword #UserName', loadSecurityQuestion);

        var $hiddenHideUserName = $('#section-resetpassword').find('#HideUserName');
        var $txtQuestionId = $('#section-resetpassword').find('#SecurityQuestionId');
        if ($hiddenHideUserName.val() === 'False' && $txtQuestionId.val() === '0') {
            clearSecurityQuestion();
            enableSecurityAnswer(false);
        }

    });

})(window.VisitPay, jQuery);
﻿(function ($, common) {

    $(document).ready(function () {

        $('#frmEditPassword').on('submit', function (e) {
            e.preventDefault();
            var $form = $(this);
            if (!$form.valid()) {
                return;
            }

            $.blockUI();
            $.post($form.attr('action'), $form.serialize(), function (result) {
                $.unblockUI();

                if (result.Result) {
                    common.ModalGenericAlert('Edit Password', result.Message, 'Ok').done(function () {
                        if (result.Result) {
                            window.location.href = '/Home';
                        }
                    });
                } else {
                    common.ResetUnobtrusiveValidation($form);
                    common.AppendMessageToValidationSummary($form, result.Message);
                    $form.find('input[type=password]').val('');
                }
            });
        });

    });

})(jQuery, window.VisitPay.Common);
﻿var setUiComponents = function () {

    $('body.modal-open').css('padding-right', '0');
    $('[placeholder]').watermark({ textAttr: 'placeholder' });
    $('[data-mask]').each(function () {
        $(this).mask($(this).data('mask').toString());
    });
    $('[data-toggle="tooltip"]').tooltip({ animation: false, container: 'body' });
    $('[data-numeric]').forceNumeric();
};

(function ($) {

    $.fn.forceNumeric = function () {

        return this.each(function () {
            $(this).keydown(function (e) {
                var key = e.which || e.keyCode;

                if (!e.shiftKey && !e.altKey &&
                    // numbers   
                    key >= 48 && key <= 57 ||
                    // Numeric keypad
                    key >= 96 && key <= 105 ||
                    // comma, period and minus, . on keypad
                   key == 190 || key == 188 || key == 109 || key == 110 ||
                    // Backspace and Tab and Enter
                   key == 8 || key == 9 || key == 13 ||
                    // Home and End
                   key == 35 || key == 36 ||
                    // left and right arrows
                   key == 37 || key == 39 ||
                    // Del and Ins
                   key == 46 || key == 45 ||
                    // Cntl-v
                    (key == 86 && e.ctrlKey))
                    return true;

                return false;
            });
        });
    }

    $.blockUI.defaults.baseZ = 99999;
    $.blockUI.defaults.css.padding = 18;
    $.blockUI.defaults.message = '<h1 style="line-height: 32px;"><img src="/Content/Images/loading.gif" alt="" /> &nbsp; Please wait...</h1>';
    $.blockUI.defaults.ignoreIfBlocked = true;

    String.prototype.cleanUrl = function () {
        var pathArray = this.split('/');
        if (pathArray == undefined || pathArray.length < 3) {
            //http://blah/ should have at least 3
            return this;
        }
        var protocol = pathArray[0];
        if (protocol.toLowerCase().indexOf('http') < 0) {
            //Seems like this isnt url...
            return this;
        }
        var host = pathArray[2];
        return host;
    };

    var modalVerticalCenter = function($modal) {

        var offset = 0;

        $modal.css('display', 'block');

        var $dialog = $modal.find('.modal-dialog');
        if ($dialog)
            offset = ($(window).height() - $dialog.height() - 100) / 2;

        $dialog.css('margin-top', Math.max(offset, 0));

    };
    
    function supportRequestChanged() {

        $.get('/Home/CurrentUser', function(html) {

            var $temp = $('<div/>').html(html);
            $('#header .messages:not(#chat-indicator)').html($temp.find('.messages').html());

        });

        $.get('/Nav/NavClient', function(html) {

            var $temp = $('<div/>').html(html);
            $('#header .navbar').html($temp.find('.navbar').html());

        });

    }

    $(document).ready(function() {

        setUiComponents();

        $.ajaxSetup({ data: { __RequestVerificationToken: $('#__AjaxAntiForgeryForm input[name=__RequestVerificationToken]').val() } });

        $('.modal').on('shown.bs.modal', setUiComponents);

        $(window).on('show.bs.modal', function (e) {

            var $modal = $(e.target);
            if ($modal.hasClass('modal-vcenter')) {

                modalVerticalCenter($modal);

            }

        });

        // global modals
        $(document).on('click', 'a[href="#Privacy"]', function (e) {
            e.preventDefault();
            window.VisitPay.Common.ModalAsync($('#modalFooter'), 'Privacy Policy', '/Legal/PrivacyPartial');
        });

        $(document).on('click', 'a[href="#Legal"]', function (e) {
            e.preventDefault();
            window.VisitPay.Common.ModalAsync($('#modalFooter'), 'Legal', '/Legal/LegalPartial');
        });

        $(document).on('click', 'a[href="#Faqs"]', function (e) {
            e.preventDefault();
            window.VisitPay.Common.ModalAsync($('#modalFooter'), 'FAQs', '/Home/FaqsPartial', null, null, function () {
                $('#modalFooter .faqs').find('a[href*="/KnowledgeBase"]').each(function () {
                    $(this).replaceWith($(this).text());
                });
                window.VisitPay.Common.BsCollapseUl($('#modalFooter .faqs > ul').eq(0), 'faq-accordion');
            });
        });

        $(document).on('click', 'a[href="#TermsOfUse"]', function (e) {
            e.preventDefault();
            window.VisitPay.Common.ModalAsync($('#modalFooter'), 'Terms of Use', '/Legal/TermsPartial');
        });
        
        $(document).on('click', 'a[href="#CreditAgreement"]', function (e) {
            e.preventDefault();

            var url = '/Legal/CreditAgreementPartial';
            var params = [];
            
            var offerid = $(this).attr('data-offerid');
            if (offerid && offerid.length > 0) {
                params.push('o=' + offerid);
            }

            if (params.length > 0) {
                url += '?' + params.join('&');
            }
            
            window.VisitPay.Common.ModalNoContainerAsync('modalCreditAgreement', url, 'GET', {}, true);
        });

        $(document).on('click', 'a[href="#EsignAct"], [data-href="#EsignAct"]', function (e) {
            e.preventDefault();
            window.VisitPay.Common.ModalAsync($('#modalFooter'), 'E-Sign Act Terms', '/Legal/EsignPartial');
        });

        // this fixes bootstrap tooltip behavior (intermittent) in IE
        // when the tooltip is open and a modal windows opens but doesn't cover the tooltip, the tooltip never hides
        $(document).on('click', '[data-toggle="tooltip"]', function () {
            $(this).blur();
        });

        $(document).on('click', '#header .messages > ul > li', function (e) {
            
            if ($(this).hasClass('open')) {
                $(this).removeClass('open');
            } else {

                $(this).addClass('open');
                
                $('body').on('click.headermessagehide', function (ev) {

                    var $element = $('#header .messages > ul > li');
                    var $target = $(ev.target);

                    if ($target === $element || $target.parents('#header .messages > ul > li').length > 0) {
                    } else {
                        $('#header .messages > ul > li').removeClass('open');
                    }

                    $('body').off('click.headermessagehide');

                });

            }
        });

        $(document).on('supportRequestChanged', supportRequestChanged);
        $(document).on('clientSupportRequestChanged', supportRequestChanged);

        $(document).on('shown.bs.modal.top', function (e) {
            $(e.target).find('.modal-body').scrollTop(0);
        });

    });

})(jQuery);

var getFileType = function (fileType) {

    switch (fileType) {
        case 'application/msword':
        case 'application/vnd.openxmlformats-officedocument.wordprocessingml.document':
            return 'file-type-word';
        case 'application/vnd.ms-excel':
        case 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet':
            return 'file-type-excel';
        case 'application/vnd.ms-powerpoint':
        case 'application/vnd.openxmlformats-officedocument.presentationml.presentation':
            return 'file-type-ppt';
        case 'application/pdf':
            return 'file-type-pdf';
        case 'application/x-zip-compressed':
            return 'file-type-zip';
        case 'image/gif':
        case 'image/png':
        case 'image/jpg':
        case 'image/jpeg':
            return 'file-type-image';
        default:
            return 'file-type-text';

    }
};

window.onerror = function (message, file, line, col, error) {
    try {
        $.ajax({
            global: false,
            url: window.VisitPay.Urls.WindowOnError,
            type: 'POST',
            data: { 'message': message, 'file': file, 'line': line, 'col': col, 'error': error }
        }).done(function () {
            // nothing
        }).fail(function () {
            // nothing
        });
    } catch (x) {
        // nothing
    }
    return false;
};
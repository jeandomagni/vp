﻿(function () {
    if (VisitPay.State.IsUserLoggedIn) {
        var userIdleTime = 0;
        var baseIncrement = 10000; //10sec
        var incrementsPerMinute = Math.ceil(60000 / baseIncrement);
        //30 seconds before logout
        var incrementWarning = (window.VisitPay.ClientSettings.SessionTimeoutInMinutes * incrementsPerMinute) - Math.ceil((incrementsPerMinute / 2));
        var incrementLogout = (window.VisitPay.ClientSettings.SessionTimeoutInMinutes * incrementsPerMinute);
        var dialogBox = null;
        var idleInterval = null;
        var countdownInterval = null;

        function countdownIncrement() {

            var secondsRemaining = (incrementLogout * 60 / incrementsPerMinute) - (incrementWarning * 60 / incrementsPerMinute);
            var element = dialogBox.find('#timeRemaining');
            element.text(secondsRemaining);

            countdownInterval = window.setInterval(function () {

                if (secondsRemaining === 1) {
                    window.clearTimeout(idleInterval);
                    window.clearInterval(countdownInterval);
                    window.location.href = window.VisitPay.Urls.LogoffUrl + '?timeout=true';
                }

                secondsRemaining--;
                element.text(secondsRemaining);

            }, 1000);

        }

        function timerIncrement() {
            userIdleTime = userIdleTime + 1;

            if (userIdleTime >= incrementWarning) {
                if (dialogBox !== null && !dialogBox.data('bs.modal').isShown) {
                    dialogBox.modal('show');
                    countdownIncrement();
                }
            }

            //Make sure the session doesnt expire if the user is doing things. If 401 then session is dead, redirect to logoff
            $.ajax(VisitPay.Urls.UpdateSession, { cache: false }).fail(function(jqxhr) {
                if (jqxhr.status === 401) {
                    window.location.href = '/account/logoff';
                }
            }).done(function(result, status, request) {
                var response = JSON.parse(request.getResponseHeader('X-Responded-JSON'));
                if (response && response.status && response.status === 401) {
                    window.location.href = '/account/logoff';
                }
            });
        }

        function timerReset() {
            userIdleTime = 0;
            if (dialogBox !== null) {
                dialogBox.modal('hide');
                window.clearInterval(countdownInterval);
            }
        }

        function continueClicked() {
            timerReset();
        }

        $(document).ready(function () {
            //Increment the idle time counter every minute.
            idleInterval = setInterval(timerIncrement, baseIncrement);
            dialogBox = $('#TimeoutWarning');
            dialogBox.modal('hide');
            dialogBox.find('.btn-primary').click(continueClicked);

            //Zero the idle timer on mouse movement.
            $(this).mousemove(function (e) {
                if (dialogBox !== null && !dialogBox.data('bs.modal').isShown) {
                    timerReset();
                }
            });
            $(this).keypress(function (e) {
                if (dialogBox !== null && !dialogBox.data('bs.modal').isShown) {
                    timerReset();
                }
            });
        });
    }
})();
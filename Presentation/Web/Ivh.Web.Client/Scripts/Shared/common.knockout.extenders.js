﻿(function (ko) {

    ko.extenders.minmaxMoney = function(target, opts) {
        target.subscribe(function(newValue) {

            var minValue = ko.unwrap(opts.minValue) || 0;
            if (!$.isNumeric(minValue)) {
                minValue = 0;
            }
            minValue = parseFloat(minValue);

            var maxValue = ko.unwrap(opts.maxValue);
            if ($.isNumeric(maxValue)) {
                maxValue = parseFloat(maxValue);
            }

            if (!newValue || !$.isNumeric(newValue)) {
                return;
            }

            if (newValue < minValue) {
                target(minValue.toFixed(2));
            }

            if (newValue > maxValue) {
                target(maxValue.toFixed(2));
                return;
            }

        });
    }

    ko.extenders.money = function (target, opts) {

        var minValue = opts.minValue;
        var maxValue = opts.maxValue;

        var result = ko.pureComputed({
            read: target,
            write: function (newValue) {

                target(0.00);

                if (newValue === undefined || newValue === null) {
                    target(null);
                    target.notifySubscribers(null);
                    return;
                }

                var valueToWrite = parseFloat(newValue.toString().replace('$', '').replace(',', ''));
                if (isNaN(valueToWrite)) {
                    valueToWrite = 0;
                }
                
                if (minValue !== undefined && minValue !== null) {
                    valueToWrite = Math.max(valueToWrite, parseFloat(ko.unwrap(minValue)));
                }

                if (maxValue !== undefined && maxValue !== null) {
                    valueToWrite = Math.min(valueToWrite, parseFloat(ko.unwrap(maxValue)));
                }

                valueToWrite = valueToWrite.toFixed(2);

                target(valueToWrite);
                target.notifySubscribers(valueToWrite);

            }

        }).extend({ notify: 'always' });

        result(target());

        return result;

    }

    ko.extenders.extendedArray = function (target, opts) {

        opts = opts || {};

        target.Any = ko.computed(function () {
            return ko.unwrap(target) && ko.unwrap(target).length > 0;
        });
        target.Clear = function() {
            if (target.Any()) {
                if ($.isFunction(target)) {
                    target.removeAll();
                } else {
                    target = [];
                }
            }
        };
        target.Count = ko.computed(function () {
            return target.Any() ? ko.unwrap(target).length : 0;
        });
        target.Find = function (item) {
            return ko.utils.arrayFirst(ko.unwrap(target), function (source) {
                if (opts.idKey === undefined || opts.idKey === null || opts.idKey.length === 0) {
                    return ko.unwrap(source) === ko.unwrap(item);
                } else {
                    return ko.unwrap(source[opts.idKey]) === ko.unwrap(item[opts.idKey]);
                }
            });
        };

    };

    ko.extenders.selectablePaymentGrid = function (target, opts) {

        opts = opts || {};

        target.extend({ extendedArray: { idKey: opts.idKey } });
        target.Selected = ko.observableArray([]).extend({ extendedArray: { idKey: opts.idKey } });
        target.Selected.Add = function (item, noCheck) {
            var existing = target.Selected.Find(item);
            var amount = ko.unwrap(item[opts.amountKey]);

            if (!existing && (noCheck || amount > 0)) {
                target.Selected.push(item);
            } else if (existing && (noCheck || amount <= 0)) {
                target.Selected.remove(existing);
            }
        };
        target.Selected.IsAll = ko.computed(function () {
            return target.Selected.Count() === target.Count() && target.Count() > 0;
        });
        target.Selected.IsSelected = function (item) {
            var i = target.Selected.Find(item);
            return i !== undefined && i !== null;
        };
        target.Selected.Sum = ko.computed(function () {
            if (!target.Selected.Any()) {
                return 0;
            }

            var sum = 0;
            ko.utils.arrayForEach(target.Selected(), function (item) {
                sum += parseFloat(ko.unwrap(item[opts.amountKey]));
            });
            return sum;

        });
        target.Selected.AsDictionary = ko.computed(function () {

            if (!target.Selected.Any()) {
                return [];
            }

            var dict = [];
            ko.utils.arrayForEach(target.Selected(), function (item) {
                if (item[opts.amountKey]() > 0) {
                    dict.push({ Key: item[opts.idKey](), Value: item[opts.amountKey]() });
                }
            });

            return dict;

        });

        //Sum without caring about whether it is selected or not 
        target.Sum = ko.computed(function () {

            var sum = 0.0;
            ko.utils.arrayForEach(target(), function (item) {
                var amount = ko.unwrap(item[opts.amountKey]);
                sum += Math.max(0.0, parseFloat(amount));
            });
            return sum;

        });

        //Convert to dictionary without caring about whether it is selected or not 
        target.AsDictionary = ko.computed(function () {

            var dict = [];
            ko.utils.arrayForEach(target(), function (item) {
                var amount = ko.unwrap(item[opts.amountKey]);
                amount = Math.max(0.0, parseFloat(amount));
                dict.push({ Key: item[opts.idKey](), Value: amount });
            });
            return dict;

        });

        target.Click = function (item) {
            if (opts.defaultKey !== opts.amountKey) {
                if (opts.setPaymentAmount) {
                    item[opts.amountKey](target.Selected.Find(item) ? ko.unwrap(item[opts.defaultKey]) : 0);
                }               
            }
            return true;
        };
        target.ClickRow = function(item) {
            if (opts.defaultKey !== opts.amountKey) {
                if (!target.Selected.Find(item)) {
                    if (opts.setPaymentAmount) {
                        item[opts.amountKey](ko.unwrap(item[opts.defaultKey]));
                    }                   
                    target.Selected.Add(item);
                } else {
                    if (opts.setPaymentAmount) {
                        item[opts.amountKey](0);
                    }                  
                    target.Selected.Add(item);
                }
            } else {
                target.Selected.Add(item, true);
            }
            return true;
        };
        target.Change = function (item) {
            target.Selected.Add(item);
        };
        target.EnsureAll = function () {
            target.Selected.removeAll();
            ko.utils.arrayForEach(ko.unwrap(target), function (item) {
                item[opts.amountKey](ko.unwrap(item[opts.defaultKey]));
                target.Selected.Add(item, true);
            });
            return true;
        };
        target.SelectAll = function () {

            var noCheck = opts.amountKey === opts.defaultKey;

            if (target.Selected.IsAll()) {
                ko.utils.arrayForEach(ko.unwrap(target), function (item) {
                    if (!noCheck) {
                        item[opts.amountKey](parseFloat(0).toFixed(2));
                    }
                    target.Selected.Add(item, noCheck);
                });
            } else {
                target.Selected.removeAll();
                ko.utils.arrayForEach(ko.unwrap(target), function (item) {
                    if (!noCheck) {
                        item[opts.amountKey](ko.unwrap(item[opts.defaultKey]));
                    }
                    target.Selected.Add(item, noCheck);
                });
            }
            return true;
        };

        return target;

    };

})(window.ko);
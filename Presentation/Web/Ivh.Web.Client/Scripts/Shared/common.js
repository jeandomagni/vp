﻿window.VisitPay = window.VisitPay || {};

VisitPay.Common = (function ($) {

    var self = {};

    self.BsCollapseUl = function (ul, className) {

        ul.hide();

        var classNameSelector = '.' + className;

        ul.attr('role', 'tablist').addClass(className);
        ul.attr('id', className + $('body').find(classNameSelector).index(ul).toString() + '_root');

        ul.children('li').addClass('panel');
        ul.children('li').each(function () {

            var index = $(classNameSelector).find('li').index($(this)).toString();
            var id = className + index;
            var text = $(this).contents().get(0).nodeValue.trim();
            var html = $(this).html().replace(text, '<a class="collapsed title" data-toggle="collapse" data-parent="#' + ul.attr('id') + '" href="#' + id + '">' + text + '</a>');

            $(this).html(html);

            $(this).children('ul').attr('id', id).addClass('collapse').attr('role', 'tabpanel');
            $(this).children('ul').each(function () {

                $(this).children('li').addClass('panel');
                $(this).children('li').each(function () {

                    var index = $(classNameSelector).find('li').index($(this)).toString();
                    var thisId = className + index;

                    var text = $(this).contents().get(0).nodeValue.trim();
                    var html = $(this).html().replace(text, '<a class="collapsed title" data-toggle="collapse" data-parent="#' + id + '" href="#' + thisId + '">' + text + '</a>');

                    $(this).html(html);

                    $(this).children('ul').attr('id', thisId).addClass('collapse').attr('role', 'tabpanel');
                });
            });

        });

        ul.on('click', '> li > a.title', function () {
            $(this).parent().parent().children('li').children('a.title').not('.collapsed').each(function () {
                $(this).siblings('ul[id^="' + className + '"]').children('li').each(function () {
                    $(this).find('ul[id^="' + className + '"].in').collapse('hide');
                });
            });
        });

        ul.collapse();
        ul.show();

    };

    var createAlertModal = function (title, content, confirmText, modalClass) {

        var promise = $.Deferred();

        var html = '<div id="modalGenericAlert" class="modal modal-nested modal-vcenter fade" tabindex="-1" role="dialog" data-backdrop="static"><div class="modal-dialog ' + modalClass + '"><div class="modal-content"><div class="modal-header"><button type="button" class="close" data-dismiss="modal"><span>&times;</span></button><h4 class="modal-title"></h4></div><div class="modal-body text-center"></div><div class="modal-footer text-center"><button type="submit" class="btn btn-primary" title="OK"OK</button></div></div></div></div>';
        $('body').append(html);

        var $modal = $('#modalGenericAlert');

        $modal.find('.modal-title').html(title);
        $modal.find('.modal-body').html(content);
        
        if (confirmText) {
            $modal.find('.modal-footer button[type=submit]').text(confirmText);
            $modal.find('.modal-footer button[type=submit]').attr('title', confirmText);
        }

        $modal.modal('show');

        $modal.on('hidden.bs.modal', function () {
            $modal.remove();
        });

        $modal.on('click', 'button[type=submit]', function () {
            $modal.modal('hide');
            promise.resolve();
        });

        $modal.on('click', '[data-dismiss="modal"]', function () {
            promise.reject();
        });

        return promise;

    };

    var createConfirmationModal = function (title, content, confirmText, cancelText, modalClass, footerText, headerClass) {

        var promise = $.Deferred();

        headerClass = headerClass || '';
        var html = '<div id="modalGenericConfirmation" class="modal modal-nested modal-vcenter fade" tabindex="-1" role="dialog" data-backdrop="static"><div class="modal-dialog ' + modalClass + '"><div class="modal-content"><div class="modal-header ' + headerClass + '"><button type="button" class="close" data-dismiss="modal"><span>&times;</span></button><h4 class="modal-title"></h4></div><div class="modal-body text-center"></div><div class="modal-footer text-center"><button type="submit" class="btn btn-primary" title="Yes">Yes</button><button type="button" class="btn btn-primary" data-dismiss="modal" title="No">No</button><div class="footer-text" style="font-size:10px; font-weight:bold; margin: 10px 0 0 0"></div></div></div></div></div>';
        $('body').append(html);

        var $modal = $('#modalGenericConfirmation');

        $modal.find('.modal-title').html(title);
        $modal.find('.modal-body').html(content);

        if (confirmText) {
            $modal.find('.modal-footer button[type=submit]').text(confirmText);
            $modal.find('.modal-footer button[type=submit]').attr('title', confirmText);
        }
        if (cancelText) {
            $modal.find('.modal-footer button[type=button]').text(cancelText);
            $modal.find('.modal-footer button[type=button]').attr('title', cancelText);
        }
        if (footerText) {
            $modal.find('.modal-footer .footer-text').text(footerText);
        }

        $modal.modal('show');

        $modal.on('hidden.bs.modal', function () {
            $modal.remove();
        });

        $modal.on('click', 'button[type=submit]', function () {
            $modal.modal('hide');
            promise.resolve();
        });

        $modal.on('click', '[data-dismiss="modal"]', function () {
            promise.reject();
        });

        return promise;

    };

    self.ModalAsync = function (element, title, url, method, data, fn) {

        method = method || 'GET';

        $.ajax({
            method: method,
            url: url,
            data: data
        }).done(function(html) {
            element.find('.modal-body').html(html).scrollTop(0);
            element.find('.modal-title').text(title);
            element.modal();

            if (fn != null)
                fn();
        });

    };
    
    self.ModalContainerAsync = function (element, url, method, data, show) {

        var promise = $.Deferred();

        $.ajax({
            method: method,
            url: url,
            data: data
        }).done(function (html) {

            element.html(html);

            var modal = element.find('.modal');

            if (show)
                modal.modal();

            promise.resolve(modal);
        });

        return promise;

    };

    self.ModalNoContainerAsync = function (modalId, url, method, data, show) {

        var promise = $.Deferred();

        $.ajax({
            method: method,
            url: url,
            data: data
        }).done(function(html) {

            $('body').append(html);

            var $modal = $('body').find('#' + modalId + '.modal');

            $modal.on('hidden.bs.modal', function() {
                if (!$modal.hasClass('disable-trigger')) {
                    $modal.remove();
                }
            });

            if (show)
                $modal.modal();

            promise.resolve($modal);
        });

        return promise;

    };
    
    self.ModalFromHtml = function (selector, html) {
        // selector should match the modal element in the html
        // ex: #modalForSomething <div id="modalForSomething"....
        $(selector).remove();
        $('body').append(html);

        var $modal = $('body').find(selector);

        return $modal;
    };

    self.ModalGeneric = function (title, content, modalClass) {

        modalClass = modalClass || 'modal-md';
        var html = '<div id="modalGeneric" class="modal modal-nested modal-vcenter fade" tabindex="-1" role="dialog" data-backdrop="static"><div class="modal-dialog ' + modalClass + '"><div class="modal-content"><div class="modal-header"><button type="button" class="close" data-dismiss="modal"><span>&times;</span></button><h4 class="modal-title"></h4></div><div class="modal-body"></div><div class="modal-footer text-center"><button type="submit" class="btn btn-primary" title="OK"OK</button></div></div></div></div>';
        $('body').append(html);

        var $modal = $('#modalGeneric');

        $modal.find('.modal-title').html(title);
        $modal.find('.modal-body').html(content);
        $modal.find('.modal-footer').hide();

        $modal.on('hidden.bs.modal', function () {
            $modal.remove();
        });
        
        return $modal;
    };

    self.ModalGenericAlert = function(title, content, confirmText) {
        return createAlertModal(title, content, confirmText, 'modal-sm');
    };

    self.ModalGenericAlertMd = function(title, content, confirmText) {
        return createAlertModal(title, content, confirmText, 'modal-md');
    };

    self.ModalGenericConfirmation = function (title, content, confirmText, cancelText, footerText, headerClass) {
        return createConfirmationModal(title, content, confirmText, cancelText, 'modal-sm', footerText, headerClass);
    }

    self.ModalGenericConfirmationMd = function (title, content, confirmText, cancelText, footerText, headerClass) {
        return createConfirmationModal(title, content, confirmText, cancelText, '', footerText, headerClass);
    }

    self.ModalGenericConfirmationLg = function (title, content, confirmText, cancelText, footerText, headerClass) {
        return createConfirmationModal(title, content, confirmText, cancelText, 'modal-lg', footerText, headerClass);
    }

    self.DisablePaste = function (selector, scopeElement) {
        scopeElement = scopeElement || $(document);
        scopeElement.on('paste', selector, function (e) {
            e.preventDefault();
        });
        scopeElement.on('contextmenu', selector, function (e) {
            e.preventDefault();
        });
    };

    self.AppendMessageToValidationSummary = function (form, message) {

        var container = form.find('[data-valmsg-summary="true"]');
        if (container && container.length) {
            container.removeClass('validation-summary-valid').addClass('validation-summary-errors');
            var list = container.find('ul');
            if (list && list.length && !list.find('li:contains("' + message + '")').length) {
                $('<li />').html(message).appendTo(list);
            }
        }

    };

    self.PrependMessageToValidationSummary = function (form, message, isMobileSummary) {

        isMobileSummary = isMobileSummary || false;

        var container = isMobileSummary ? form.find('[data-mob-valmsg-summary="true"]') : form.find('[data-valmsg-summary="true"]');
        if (container && container.length) {
            container.removeClass('validation-summary-valid').addClass('validation-summary-errors');
            var list = container.find('ul');
            if (list && list.length && !$('li:contains("' + message + '")').length) {
                $('<li />').html(message).prependTo(list);
            }
        }

    };

    self.ResetUnobtrusiveValidation = function (form) {
        form.removeData('validator').removeData('unobtrusiveValidation');
        $.validator.unobtrusive.parse(form);
        form.validate().resetForm();
        form.find('[data-valmsg-summary=true]').removeClass('validation-summary-errors').addClass('validation-summary-valid').find('ul').empty();
        form.find('.field-validation-error').removeClass('field-validation-error').addClass('field-validation-valid');
        form.find('.input-validation-error').removeClass('input-validation-error').addClass('valid');
    };

    self.ResetValidationSummary = function (form) {

        var container = form.find('[data-valmsg-summary="true"]');
        if (container && container.length) {
            var list = container.find('ul');

            if (list && list.length) {
                list.empty();
                container.addClass('validation-summary-valid').removeClass('validation-summary-errors');
            }
        }

    };

    // hack to remove "...is required" from validation summarys
    self.SuppressRequiredErrorsFromValidationSummary = function (forms) {

        forms.each(function () {
            var messages = $(this).data('validator').settings.messages;
            for (var propertyName in messages) {
                if (messages.hasOwnProperty(propertyName)) {
                    messages[propertyName].conditionalrequired = '';
                    messages[propertyName].required = '';
                }
            }
        });
        $.validator.unobtrusive.parse(document);

    };

    self.TurnOffValidation = function (form) {
        var settings = form.validate().settings;
        for (var ruleIndex in settings.rules) {
            if (settings.rules.hasOwnProperty(ruleIndex)) {
                delete settings.rules[ruleIndex];
            }
        }
    };

    self.AddAntiForgeryToken = function (data) {
        data.__RequestVerificationToken = $('#__AjaxAntiForgeryForm input[name=__RequestVerificationToken]').val();
        return data;
    };

    self.InitTooltips = function ($container) {
        $container.find('[data-toggle="tooltip"]').tooltip({ animation: false, container: 'body' });
    }

    self.getQuery = function (query) {
        query = query.replace(/[\[]/, '\\\[').replace(/[\]]/, '\\\]');
        var expr = '[\\?&]' + query + '=([^&#]*)';
        var regex = new RegExp(expr);
        var results = regex.exec(window.location.href);
        if (results !== null) {
            return results[1];
        } else {
            return false;
        }
    };

    self.ToDecimal = function(v) {

        if (v === undefined || v === null)
            return 0;

        return parseFloat(v.toString().replace(/,/g, '').replace('$', ''));

    };

    self.ShowCardReaderModal = function() {

        var promise = $.Deferred();

        $.blockUI();

        self.ModalNoContainerAsync('modalCardReaderDeviceKey', '/CardReader/GetCardReaderDeviceKeyModal', 'GET', {}, true).done(function ($modal) {
            
            $modal.off('click.dismiss').on('click.dismiss', '[data-dismiss="modal"]', function () {
                $modal.modal('hide');
                promise.reject();
            });

            var $form = $modal.find('form');
            $form.parseValidation();
            $form.off('submit').on('submit', function (e) {
                e.preventDefault();

                if (!$(this).valid()) {
                    return;
                }

                $.blockUI();

                $.post($form.attr('action'), $form.serialize(), function (data) {
                    $.unblockUI();
                    if (data.Success) {
                        $modal.modal('hide');
                        promise.resolve(data.DeviceKey);
                    } else {
                        self.AppendMessageToValidationSummary($form, data.Message);
                    }
                });
            });


            $.unblockUI();
        });

        return promise;
    };

    $(document).ajaxError(function (event, jqxhr, settings, thrownError) {

        $.unblockUI();

        // if either of these are true, then it's not a true error and we don't care
        // navigating before ajax completes for example
        if (jqxhr.status === 0 || jqxhr.readyState === 0) {
            return;
        }

        var url = settings && settings.url ? settings.url.split('?')[0].toUpperCase() : '';

        if (url !== window.VisitPay.Urls.ErrorLogging.toUpperCase()) {

            try {
                $.ajax({
                    global: false,
                    url: window.VisitPay.Urls.ErrorLogging,
                    type: 'POST',
                    data: { 'ajaxEvent': JSON.stringify(event), 'jqxhr': jqxhr.status, 'settings': JSON.stringify(settings), 'thrownError': thrownError }
                }).done(function() {
                    // nothing
                }).fail(function() {
                    // nothing
                });
            } catch (x) {
                // nothing
            }

            if (url !== window.VisitPay.Urls.UpdateSession.toUpperCase()) {
                if (jqxhr.status === 401) { // emulate error
                    $('#modalEmulate').modal('show');
                    $('#modalEmulate').find('.modal-body').html('<p>' + thrownError + '</p>');
                } else {
                    self.ModalGenericAlert('Processing Error', '<p>There was an error trying to process your request...</p>', 'OK');
                }
            }
        }

        return;

    });

    return self;

})(jQuery);

jQuery.fn.NumericOnly = function () {
    return this.each(function () {
        $(this).keypress(function (e) {
            e = e || window.event;
            if (e.keyCode != 8 && e.keyCode != 9 && e.keyCode != 13 && !e.ctrlKey && !e.metaKey && !e.altKey) {
                var charCode = (typeof e.which == 'undefined') ? e.keyCode : e.which;
                if (charCode && !/\d/.test(String.fromCharCode(charCode))) {
                    return false;
                }
            }
        });
    });
};
﻿(function ($) {
    var sideNavStateKey = 'sideNavState'; //Key in local storage
    var minWidthForOpenSideNav = 1475; //NOTE: This should match '$sidenav-wrapper-push-min-width' in _sidenav.scss

    function canWindowSupportOpenSideNav() {
        if (window.innerWidth < minWidthForOpenSideNav) {
            //Window isn't wide enough to support a stay-open sideNav
            return false;
        }
        
        //Window is wide enough to support a stay-open sideNav
        return true;
    }

    function isSideNavStateOpen() {
        var state = localStorage.getItem(sideNavStateKey);

        if (state != null && JSON.parse(state) === false) {
            //Menu closed
            return false;
        }
        
        //Default state of menu is open if closed is not explicitly set
        return true;
    }

    function setMenuState(open) {
        //Save state
        localStorage.setItem(sideNavStateKey, open);

        //Update CSS classes
        if (open) {
            //Open
            $('#sideNav').addClass('open');
            $('#wrapper').addClass('sidenav-open');
            $('#sidenav-toggle').addClass('active');
        } else {
            //Closed
            $('#sideNav').removeClass('open');
            $('#wrapper').removeClass('sidenav-open');
            $('#sidenav-toggle').removeClass('active');
        }
    }

    function restoreDropdownState() {
        //Close all dropdowns
        $('.sidenav-dropdown-btn.expanded').removeClass('expanded');
        $('.sidenav-dropdown-container.expanded').toggleClass('expanded');
        
        //Add expanded class back to active dropdown and active to its dropdown container
        var activeDropdownBtn = $('.sidenav-dropdown-btn.active')[0];
        if (activeDropdownBtn) {
            $(activeDropdownBtn).addClass('expanded');
            $(activeDropdownBtn.nextElementSibling).addClass('expanded');
        }
    }

    $(document).ready(function () {
        //Init nano scrolling for sideNav
        $('#sideNavMenu').overlayScrollbars({
            overflowBehavior: {
                x: 'hidden'
            }
        });

        //Init menu as open or closed, if the page can handle it
        if (canWindowSupportOpenSideNav()) {
            setMenuState(isSideNavStateOpen());
        } else {
            setMenuState(false);
        }

        //Listen for sidenav button clicks
        $('#sidenav-toggle').click(function (e) {
            if (isSideNavStateOpen()) {
                restoreDropdownState(); //Put dropdown menus back in the state they were in
            }

            setMenuState(!isSideNavStateOpen());
        });

        //Listen for dropdown clicks in sidenav
        $('.sidenav-dropdown-btn').click(function (e) {
            //Close any other open dropdowns
            if (!$('.sidenav-dropdown-btn.expanded').is($(this))) {
                $('.sidenav-dropdown-btn.expanded').toggleClass('expanded');
                $('.sidenav-dropdown-container.expanded').toggleClass('expanded');
            }

            //Open this dropdown
            $(this).toggleClass('expanded'); //Dropdown button
            $(this.nextElementSibling).toggleClass('expanded'); //Dropdown container
        });

        //Listen for clicks outside of menu
        $('body').click(function (e) {
            //Check if we clicked on something in the sideNav, or the sideNav toggle
            if (e.target.id == "sideNav" || $(e.target).parents("#sideNav").length
                || e.target.id == "sidenav-toggle" || $(e.target).parents("#sidenav-toggle").length) {
                //Do nothing
            } else {
                //If window isn't large enough to keep the menu open and it is currently open, dismiss it
                if (!canWindowSupportOpenSideNav() && isSideNavStateOpen()) {
                    setMenuState(false);
                    restoreDropdownState(); //Put dropdown menus back in the state they were in
                }
            }
        });

        $(window).resize(function () {
            if (!canWindowSupportOpenSideNav() && isSideNavStateOpen()) {
                setMenuState(false);
                restoreDropdownState(); //Put dropdown menus back in the state they were in
            } else if (canWindowSupportOpenSideNav() && !isSideNavStateOpen()) {
                setMenuState(true);
            }
        });
    });
})(jQuery);
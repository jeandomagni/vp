﻿window.VisitPay.GuarantorFilter = (function ($) {

    var self = this;
    self.FilterValue = {};
    self.OnFilterChanged = function() {};

    var $filter = $('.guarantor-filter');
    var $select = $filter.find('select');

    function parseSelect() {

        if ($filter.length > 0 && $select.length > 0) {

            var value = $select.val();
            var split = value.split(',');

            self.FilterValue = {
                CurrentVisitPayUserId: split[0],
                FilteredVisitPayUserId: split[1],
                FilteredVpGuarantorId: split[2]
            };

        } else {

            self.FilterValue = {};

        }

    };

    function disable() {
        $filter.addClass('disabled');
        $select.prop('disabled', true);
    };

    function enable() {
        $filter.removeClass('disabled');
        $select.prop('disabled', false);
    }

    $(document).ready(function() {
        parseSelect();
    });

    $(document).on('change', '.guarantor-filter select', function () {

        $.blockUI();
        parseSelect();

        $.post('/GuarantorFilter/SetGuarantor', {
            currentVisitPayUserId: self.FilterValue.CurrentVisitPayUserId,
            filteredVpGuarantorId: self.FilterValue.FilteredVpGuarantorId
        }).then(function () {
            $.unblockUI();
            self.OnFilterChanged(self.FilterValue);
        });

    });

    self.Disable = disable;
    self.Enable = enable;

    return self;

})(jQuery);
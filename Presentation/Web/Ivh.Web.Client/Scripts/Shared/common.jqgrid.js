﻿(function ($) {

    $.fn.extend($.jgrid.defaults, {
        autowidth: true,
        datatype: 'json',
        cellLayout: 9, // padding-left + padding-right + border-left + border-right ... needs to match css
        height: 'auto',
        loadui: 'disabled',
        mtype: 'POST',
        resizable: false,
        rowList: [25, 50, 75],
        rowNum: 25,
        viewrecords: true
    });

    $.fn.extend({
        makeJqGrid: function (initialModel, opts, vpOpts, handlers) {

            //
            var $wrapper = $(this);
            var $grid = null;
            var options = {};
            handlers = handlers || {};
            vpOpts = vpOpts || {};

            //
            var internalEvents = {
                loadComplete: function (data) {
                    
                    var root = options.jsonReader.root ? data[options.jsonReader.root] : data;
                    root = root === undefined || root === null ? [] : root;
                    var $container = $grid.parents('.ui-jqgrid:eq(0)');

                    if (root.length > 0) {

                        $container.show();
                        $wrapper.find('div[data-none=true]').remove();

                    } else {

                        $container.hide();
                        if (vpOpts.noRecordsText.length > 0 && $wrapper.find('div[data-none]').length === 0) {
                            $wrapper.append('<div data-none="true" class="alert alert-info text-center"><strong>' + vpOpts.noRecordsText + '</strong></div>');
                        }
                    }

                    if ($.isFunction(handlers.loadComplete)) {
                        handlers.loadComplete(data, root);
                    }

                    $container.tooltip({
                        selector: '[data-toggle="tooltip"]',
                        animation: false,
                        container: 'body'
                    });

                },
                gridComplete: function () {

                    var container = $grid.parents('.ui-jqgrid:eq(0)');
                    container.addClass('visible');

                    if ($.isFunction(handlers.gridComplete || {})) {
                        handlers.gridComplete();
                    }

                },
                reload: function (model) {
                    $.extend($grid.jqGrid('getGridParam', 'postData'), {
                        Filter: model
                    });
                    $grid.jqGrid('setGridParam', {
                        page: 1
                    });

                    var triggerSort = true;
                    if (options.sortname && options.sortname.length > 0) {
                        for (var i = 0; i < options.colModel.length; i++) {
                            if (options.colModel[i].name && options.colModel[i].name.toUpperCase() === options.sortname.toUpperCase()) {
                                if (options.colModel[i].sortable !== undefined && options.colModel[i].sortable === false) {
                                    triggerSort = false;
                                }
                            }
                        }
                    }
                    if (triggerSort) {
                        $grid.sortGrid(options.sortname, true, options.sortorder);
                    } else {
                        $grid.trigger('reloadGrid');
                    }
                }
            };

            // grid options
            $.extend(options,
                {
                    beforeRequest: function () {
                        var gridParameters = $grid.jqGrid('getGridParam');
                        if (gridParameters.postData.Filter == null) {
                            gridParameters.postData = {
                                Model: gridParameters.postData
                            };
                        }
                    },
                    jsonReader: {
                        root: 'Results'
                    },
                    gridvew: true,
                    loadComplete: internalEvents.loadComplete,
                    gridComplete: internalEvents.gridComplete,
                    pager: $(this).attr('id') + 'Pager',
                    rowList: [
                        25, 50, 75
                    ],
                    rowNum: 25,
                    postData: {
                        Filter: initialModel
                    },
                    sortorder: 'desc'
                },
                opts || {});

            // vp options
            vpOpts = $.extend({}, { noRecordsText: '' }, vpOpts);

            // add elements
            $wrapper.append('<table data-grid="true" />');
            if (options.pager) {
                $wrapper.append('<div data-pager="true" id="' + options.pager + '" />');
            }

            // prevent column resizing
            for (var i = 0; i < options.colModel.length; i++) {
                options.colModel[i].resizable = false;
            }

            // add pager
            var $pager = $(this).find('div[data-pager=true]');
            if ($pager.length > 0) {
                $.extend(options, {
                    pager: $pager.attr('id')
                });
            }

            // create grid
            $grid = $(this).find('table[data-grid=true]');
            var jqg = $grid.jqGrid(options);

            //
            return {
                jqg: jqg,
                reload: internalEvents.reload
            };

        }
    });

})(jQuery);

window.VisitPay.Grids = window.VisitPay.Grids || {};
window.VisitPay.Grids.BaseGrid = (function ($gridElement, filterModel, $formElement, gridOpts, vpOpts, handlers) {

    gridOpts = gridOpts || {};
    vpOpts = vpOpts || {};
    handlers = handlers || {};
    $formElement = $formElement || $();

    var grid = null;
    var defaultModelFilter = ko.mapping.toJS(filterModel);
    var defaultFormFilter = $formElement.serializeArray();

    function getFilter() {

        var formSerialized = {};
        if ($formElement.serializeToObject) {
            formSerialized = $formElement.serializeToObject(true);
        }

        return $.extend({}, ko.mapping.toJS(ko.mapping.fromJSON(ko.mapping.toJSON(filterModel))), formSerialized || {});
    };

    function exportData() {

        $.post(gridOpts.url + 'Export', {
            Filter: getFilter(),
            sidx: grid.jqg.jqGrid('getGridParam', 'sortname'),
            sord: grid.jqg.jqGrid('getGridParam', 'sortorder'),
            page: grid.jqg.jqGrid('getGridParam', 'page'),
            rows: grid.jqg.jqGrid('getGridParam', 'rowNum')
        }, function (result) {
            if (result.Result === true) {
                window.location.href = result.Message;
            }
        }, 'json');

    };

    function reload() {
        grid.reload(getFilter());
    };

    function reinit() {
        defaultModelFilter = ko.mapping.toJS(filterModel);
        reset();
    }

    function reset() {
        $formElement.deserializeArray(defaultFormFilter);
        ko.mapping.fromJS(defaultModelFilter, {}, filterModel);
        reload();
    };

    grid = $gridElement.makeJqGrid(getFilter(), gridOpts, vpOpts, {
        loadComplete: handlers.loadComplete
    });

    return {
        exportData: exportData,
        grid: grid,
        jqg: grid.jqg,
        reload: reload,
        reinit: reinit,
        reset: reset
    }
});
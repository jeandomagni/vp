﻿window.VisitPay = window.VisitPay || {};

VisitPay.Dialog = (function ($) {
    var self = {};

    //SessionStorageKeys
    var _sessionStorageActiveDialogs = 'ActiveDialogs';

    var DialogModel = function () {
        this.Height = 0;
        this.Width = 0;
        this.MinWidth = 0;
        this.MinHeight = 0;
        this.IsMinimized = false;
        this.Resizable = false;
        this.Position = null;
        this.Data = null;
    };

    self.Model = function () {
        return new DialogModel();
    };

    self.CreateAsync = function (dialogId, dialogModel, dialogTitle, url, method, autoOpen, onOpen, onClose, onMinimize, onRestore, beforeClose) {
        var promise = $.Deferred();

        var container = $(dialogId);

        //TODO: what to do if container doesn't exist?

        //If we already have an instance of the dialog, clear it out before creating
        var existingDialog = container.dialog('instance');
        if (existingDialog) {
            //We have an existing dialog, close it
            existingDialog.close();
        }

        //Load the dialog UI
        loadDialogUI(container, url, method).done(function () {
            //Create the dialog and save the dialog model
            initDialog(dialogId, container, dialogModel, dialogTitle);

            //Bind event listeners
            bindListeners(dialogId, container, onOpen, onClose, onMinimize, onRestore, beforeClose);

            //Open the dialog, if applicable
            // console.log('before auto-open: ', container.dialog('instance'));
            if (autoOpen) {
                container.dialog('open');
            }

            //Add the dialog to our stored array of active dialogs
            addActiveDialog(dialogId);

            promise.resolve(container);
        });

        return promise;
    };

    var resizeTimer;
    self.UpdateActiveDialogPositioning = function () {
        //Wait until resizing has finished so that we don't have to make resize calls a bunch of times
        clearTimeout(resizeTimer);
        resizeTimer = setTimeout(function () {
            // console.log('window was resized');

            var activeDialogs = getActiveDialogs();
            $.each(activeDialogs, function (idx, dialogId) {
                // console.log('need to resize: ', dialogId);
                var container = $(dialogId);
                if (container.dialog("instance")) {
                    var dialogModel = getSavedDialogModel(dialogId);
                    container.dialog('option', 'height', getSafeDialogHeight(dialogModel.Height));

                    container.dialog('option', 'position', getDialogPosition(dialogId));
                }
            });
        }, 50);
    };

    self.DialogIsActive = function (dialogId) {
        var activeDialogs = getActiveDialogs();
        if ($.inArray(dialogId, activeDialogs) > -1) {
            //Dialog is active
            return true;
        }

        //Dialog isn't active
        return false;
    };

    self.GetActiveDialogModel = function (dialogId) {
        if (self.DialogIsActive(dialogId)) {
            return getSavedDialogModel(dialogId);
        }

        return null;
    };

    self.CloseActiveDialog = function (dialogId) {
        if (self.DialogIsActive(dialogId)) {
            //Dialog is active, close it
            dialogWillClose(dialogId);
        }
    };

    //Used on page loads, to reshow any dialogs that were active on a previous page
    self.ShowActiveDialogs = function () {
        var activeDialogs = getActiveDialogs();
        $.each(activeDialogs, function (idx, dialogId) {
            // console.log('need to show dialog: ', dialogId);
            var dialogModel = getSavedDialogModel(dialogId);
        });
    };

    //Gets a height for the dialog that will fit within the window
    function getSafeDialogHeight(preferredHeight) {
        var windowHeight = $(window).height();
        if (preferredHeight > windowHeight) {
            return windowHeight - 50;
        }

        return preferredHeight;
    }

    function loadDialogUI (container, url, method) {
        var promise = $.Deferred();

        $.ajax({
            method: method,
            url: url
        }).done(function (html) {
            container.html(html);
            promise.resolve();
        });

        return promise;
    }

    //Loads the saved dialog model from sessionStorage
    function getSavedDialogModel (dialogId) {
        var dialogModel = JSON.parse(sessionStorage.getItem(dialogId));
        // console.log('got saved dialog model: ', dialogModel);
        return dialogModel;
    }

    //Saves the dialog model to sessionStorage
    function saveDialogModel (dialogId, dialogModel) {
        // console.log('attempting to save model: ', dialogModel);
        var modelJSON = JSON.stringify(dialogModel);
        sessionStorage.setItem(dialogId, modelJSON);
        // console.log('dialog model saved to session storage!');
    }

    function updateDialogPosition (dialogId, ui) {
        // console.log('update dialog position called: ', dialogId, ui);
        //Update dialog position
        var dialogModel = getSavedDialogModel(dialogId);
        dialogModel.Position = ui.position;

        //Save the updated model
        saveDialogModel(dialogId, dialogModel);
    }

    function dialogResized (dialogId, ui) {
        // console.log('dialog resized: ', dialogId, ui);
        //Update dialog size
        var dialogModel = getSavedDialogModel(dialogId);
        dialogModel.Width = ui.size.width;
        dialogModel.Height = ui.size.height;

        //Save the updated model
        saveDialogModel(dialogId, dialogModel);
    }

    function getActiveDialogs () {
        var activeDialogs = JSON.parse(sessionStorage.getItem(_sessionStorageActiveDialogs));
        if (activeDialogs == null) {
            //No active dialogs, return empty array
            activeDialogs = [];
        }
        // console.log('loaded active dialogs: ', activeDialogs);
        return activeDialogs;
    }

    function saveActiveDialogs (activeDialogs) {
        sessionStorage.setItem(_sessionStorageActiveDialogs, JSON.stringify(activeDialogs));
    }

    function addActiveDialog (dialogId) {
        var activeDialogs = getActiveDialogs();
        if ($.inArray(dialogId, activeDialogs) === -1) {
            //Dialog isn't in the list yet, add it
            activeDialogs.push(dialogId);

            //Save
            saveActiveDialogs(activeDialogs);
        }
    }

    function removeActiveDialog (dialogId) {
        var activeDialogs = getActiveDialogs();

        //Remove the dialog if it exists
        var dialogIndex = $.inArray(dialogId, activeDialogs);
        // console.log('remove dialog found one with IDX: ', dialogIndex);
        if (dialogIndex !== -1) {
            //Remove
            activeDialogs.splice(dialogIndex, 1);

            //Save
            saveActiveDialogs(activeDialogs);
        }
    }

    //Clears the data from the dialog saved in sessionStorage
    function dialogWillClose (dialogId) {
        // console.log('dialog closing with id: ', dialogId);
        //Clear dialog data, but remember the model so we can reuse where the user had it positioned/sized later
        var dialogModel = getSavedDialogModel(dialogId);
        dialogModel.Data = null;

        //Save the updated model
        saveDialogModel(dialogId, dialogModel);

        //Remove the active dialog
        removeActiveDialog(dialogId);
    }

    //Gets the stored dialog position if there is one, otherwise uses the default
    function getDialogPosition (dialogId) {
        //Default to center of window
        var dialogPosition = {
            my: 'center',
            at: 'center',
            of: window
        };

        //Load saved model
        var dialogModel = getSavedDialogModel(dialogId);
        // console.log('=============== model for position: ', dialogModel);
        if (dialogModel != null && dialogModel.Position != null) {
            //We have a position stored for the dialog. Make sure it still fits on screen.
            var windowWidth = $(window).width();
            var windowHeight = $(window).height();
            var dialogWidth = dialogModel.Width;
            //var dialogHeight = dialogModel.Height;
            var dialogHeight = getSafeDialogHeight(dialogModel.Height);
            var leftToUse = dialogModel.Position.left;
            var topToUse = dialogModel.Position.top;

            if ((leftToUse + dialogWidth) > windowWidth) {
                //Dialog left is offscreen, so update it
                leftToUse = windowWidth - dialogWidth - 15;
            }

            if ((topToUse + dialogHeight) > windowHeight) {
                //Dialog top is offscreen, so update it
                topToUse = windowHeight - dialogHeight;
            }

            dialogPosition = {
                my: 'left top',
                at: 'left+' + leftToUse + ' top+' + topToUse,
                of: window
            };
        }

        return dialogPosition;
    }

    /*
     Initializes the dialog, using details from the given model combined with
     previously saved model details (to restore user positioning). Then saves
     the model after the dialog is initialized.
     */
    function initDialog (dialogId, container, dialogModel, dialogTitle) {
        /*
         Check if we already have a model for the dialog. If we do, we want to
         keep it so we have the user's saved dialog position/sizing. Only replace
         the model data.
         */
        var existingDialogModel = getSavedDialogModel(dialogId);
        if (existingDialogModel) {
            existingDialogModel.Data = dialogModel.Data;
            dialogModel = existingDialogModel;
        }

        //Get the position we should use for the dialog
        var position = getDialogPosition(dialogId);

        //Init
        container.dialog({
            height: getSafeDialogHeight(dialogModel.Height),
            minHeight: dialogModel.MinHeight,
            width: dialogModel.Width,
            minWidth: dialogModel.MinWidth,
            title: dialogTitle,
            resizable: dialogModel.Resizable,
            position: position,
            classes: {
                'ui-dialog': 'dialog-shadow',
                'ui-dialog-titlebar': 'dialog-title-bar'
            },
            closeText: 'close',
            autoOpen: false
        }).dialogExtend({
            minimizable: true,
            closable: true,
            minimizeLocation: 'right',
            dblclick: 'minimize'
        });

        //Save the model to sessionStorage
        saveDialogModel(dialogId, dialogModel);
    }

    function setIsMinimized (dialogId, isMinimized) {
        //Load saved model
        var dialogModel = getSavedDialogModel(dialogId);
        dialogModel.IsMinimized = isMinimized;

        saveDialogModel(dialogId, dialogModel);
    }

    function bindListeners(dialogId, container, onOpen, onClose, onMinimize, onRestore, beforeClose) {
        container.on('dialogopen', function () {
            //Minimize, if applicable
            var dialogModel = getSavedDialogModel(dialogId);
            if (dialogModel.IsMinimized) {
                // console.log('dialog was MINIMIZED on init...');
                container.dialogExtend('minimize');
            }
        });
        container.on('dialogopen', onOpen);
        container.on('dialogclose', onClose);
        container.on('dialogextendminimize', onMinimize);
        container.on('dialogextendrestore', onRestore);

        //Below listeners are required
        container.on('dialogbeforeclose', function (e) {
            if (beforeClose) {
                var promise = beforeClose();
                promise.done(function () {
                    dialogWillClose(dialogId);
                });
            } else {
                dialogWillClose(dialogId);
            }
        });
        container.on('dialogdragstop', function (event, ui) {
            updateDialogPosition(dialogId, ui);
        });
        container.on('dialogresizestop', function (event, ui) {
            dialogResized(dialogId, ui);
        });
        container.on('dialogextendbeforeminimize', function () {
            setIsMinimized(dialogId, true);
        });
        container.on('dialogextendbeforerestore', function () {
            setIsMinimized(dialogId, false);
        });

        container.on('dialogresizestart', function () {
            /*
             Need to turn off window resize listener while dialog is being resized, since it can
             affect the window size
             */
            //TODO: Is this safe enough?
            // console.log('>>>>>>>>>>>>> TURNING RESIZE LISTENER OFF');
            $(window).off('resize');
        });
        container.on('dialogresizestop', function () {
            //Listen for window resize events now that dialog is finished being sized
            //TODO: Is this safe enough?
            // console.log('>>>>>>>>>>>>> TURNING RESIZE LISTENER BACK ON');
            $(window).resize(function() {
                self.UpdateActiveDialogPositioning();
            });
        });

        //TODO: do we need to listen for window resize and handle resizing this dialog??
    }

    return self;
})(jQuery);
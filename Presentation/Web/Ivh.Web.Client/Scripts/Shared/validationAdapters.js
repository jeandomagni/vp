﻿(function ($) {

    // copied from jquery.validate.unobtrusive.js
    function getModelPrefix(fieldName) {
        return fieldName.substr(0, fieldName.lastIndexOf('.') + 1);
    }
    function appendModelPrefix(value, prefix) {
        if (value.indexOf('*.') === 0) {
            value = value.replace('*.', prefix);
        }
        return value;
    }
    function escapeAttributeValue(value) {
        // As mentioned on http://api.jquery.com/category/selectors/
        return value.replace(/([!"#$%&'()*+,./:;<=>?@\[\\\]^`{|}~])/g, '\\$1');
    }

    // age date range
    $.validator.addMethod('agedaterange', function (value, element, params) {

        var maxDate = new Date();
        maxDate.setFullYear(new Date().getFullYear() - params.minimumage);

        var minDate = new Date();
        minDate.setFullYear(new Date().getFullYear() - params.maximumage);

        var parsedValue = Date.parse(value);
        var parsedMin = Date.parse(minDate);
        var parsedMax = Date.parse(maxDate);

        return parsedValue >= parsedMin && parsedValue <= parsedMax;
    });
    $.validator.unobtrusive.adapters.add('agedaterange', ['minimumage', 'maximumage'], function (options) {
        options.rules['agedaterange'] = options.params;
        options.messages['agedaterange'] = options.message;
    });

    // compare to if not equal
    $.validator.addMethod('comparetoifnotequal', function (value, element, params) {

        var valid = false;

        var parts = element.name.split('.');
        var prefix = '';
        if (parts.length > 1)
            prefix = parts[0] + '.';

        var compareToValue = $('input[name="' + prefix + params.comparetoproperty + '"]').val();
        var property1Value = $('input[name="' + prefix + params.property1 + '"]').val();
        var property2Value = $('input[name="' + prefix + params.property2 + '"]').val();

        if (!(params.casesensitive.toLowerCase() == 'true')) {
            compareToValue = compareToValue.toUpperCase();
            property1Value = property1Value.toUpperCase();
            property2Value = property2Value.toUpperCase();
            value = value.toUpperCase();
        }

        if ((property1Value == property2Value && value.length == 0) || value == compareToValue)
            valid = true;

        return valid;

    });
    $.validator.unobtrusive.adapters.add('comparetoifnotequal', ['comparetoproperty', 'property1', 'property2', 'casesensitive'], function (options) {
        options.rules['comparetoifnotequal'] = options.params;
        options.messages['comparetoifnotequal'] = options.message;
    });

    // conditional required
    $.validator.addMethod('conditionalrequired', function (value, element, params) {
        var requiredpropertyprefix = params['requiredpropertyprefix'];
        var requiredproperty = params['requiredproperty'];
        var field = $('#' + requiredproperty).length == 0 ? '#' + requiredpropertyprefix + '_' + requiredproperty : '#' + requiredproperty;
        return !($(field).val().length > 0 && value.length == 0);
    });
    $.validator.unobtrusive.adapters.add('conditionalrequired', ['requiredpropertyprefix', 'requiredproperty'], function (options) {
        options.rules['conditionalrequired'] = {
            requiredpropertyprefix: options.params['requiredpropertyprefix'],
            requiredproperty: options.params['requiredproperty']
        };
        options.messages['conditionalrequired'] = options.message;
    });

    // datebetween
    $.validator.addMethod('datebetween', function (value, element, params) {

        var result = false;
        var required = params['required'].toString().toUpperCase() === 'TRUE';

        if (!required && (value == null || value.length === 0 || value.trim().length === 0))
            return true;

        if (value && (params['date1'] != null && params['date2'] != null)) {

            var date1Value = $('input[id="' + params['date1'] + '"]').val().trim();
            var date2Value = $('input[id="' + params['date2'] + '"]').val().trim();

            if (moment(value, 'MM/DD/YYYY').isValid())
                result = Date.parse(value) >= Date.parse(date1Value) && Date.parse(value) <= Date.parse(date2Value);
            else
                result = false;
        }

        return result;

    });
    $.validator.unobtrusive.adapters.add('datebetween', ['required', 'date1', 'date2'], function (options) {

        var prefix = getModelPrefix(options.element.name);
        var date1 = options.params.date1;
        var date2 = options.params.date2;

        var date1FullId = $(options.form).find(':input').filter('[name="' + escapeAttributeValue(appendModelPrefix(date1, prefix)) + '"]').attr('id');
        var date2FullId = $(options.form).find(':input').filter('[name="' + escapeAttributeValue(appendModelPrefix(date2, prefix)) + '"]').attr('id');

        options.rules['datebetween'] = {
            required: options.params.required,
            date1: date1FullId,
            date2: date2FullId
        };
        options.messages['datebetween'] = options.message;
    });

    // daterangecompare
    $.validator.addMethod('daterangecompare', function (value, element, params) {

        var result = false;
        var required = params['required'].toString().toUpperCase() === 'TRUE';

        if (!required && (value == null || value.length === 0 || value.trim().length === 0))
            return true;

        if (value && (params['date1'] != null && params['date2'] != null)) {

            var date1Value = $('input[id="' + params['date1'] + '"]').val().trim();
            var date2Value = $('input[id="' + params['date2'] + '"]').val().trim();

            if (params['daterangecomparetype'] === 'EqualTo') {
                result = Date.parse(date1Value) === Date.parse(date2Value) || (!required && date2Value.length === 0);
            } else if (params['daterangecomparetype'] === 'GreaterThanOrEqual') {
                result = Date.parse(date1Value) >= Date.parse(date2Value) || (!required && date2Value.length === 0);
            } else if (params['daterangecomparetype'] === 'LessThanOrEqual') {
                result = Date.parse(date1Value) <= Date.parse(date2Value) || (!required && date2Value.length === 0);
            }
        }

        return result;

    });
    $.validator.unobtrusive.adapters.add('daterangecompare', ['required', 'date1', 'date2', 'daterangecomparetype'], function (options) {

        var prefix = getModelPrefix(options.element.name);
        var date1 = options.params.date1;
        var date2 = options.params.date2;

        var date1FullId = $(options.form).find(':input').filter('[name="' + escapeAttributeValue(appendModelPrefix(date1, prefix)) + '"]').attr('id');
        var date2FullId = $(options.form).find(':input').filter('[name="' + escapeAttributeValue(appendModelPrefix(date2, prefix)) + '"]').attr('id');

        options.rules['daterangecompare'] = {
            required: options.params.required,
            date1: date1FullId,
            date2: date2FullId,
            daterangecomparetype: options.params.daterangecomparetype
        };
        options.messages['daterangecompare'] = options.message;
    });

    // luhn check
    $.validator.addMethod('luhncheck', function (value, element, params) {

        var luhnChk = (function (arr) {
            return function (ccNum) {
                var
                    len = ccNum.length,
                    bit = 1,
                    sum = 0,
                    val;

                while (len) {
                    val = parseInt(ccNum.charAt(--len), 10);
                    sum += (bit ^= 1) ? arr[val] : val;
                }

                return sum && sum % 10 === 0;
            };
        }([0, 2, 4, 6, 8, 1, 3, 5, 7, 9]));

        value = value.replace(/[^0-9\.]+/g, '');
        if (value.length === 0)
            return true; // not required

        return luhnChk(value);

    });
    $.validator.unobtrusive.adapters.add('luhncheck', [], function (options) {
        options.rules['luhncheck'] = options.params;
        options.messages['luhncheck'] = options.message;
    });

    // not equal
    $.validator.addMethod('notEqualTo', function (value, element, param) {
        return this.optional(element) || value != $(param).val();
    }, '');
    $.validator.unobtrusive.adapters.add('notequalto', ['other'], function (options) {
        var prefix = getModelPrefix(options.element.name);
        var other = options.params.other;
        var fullOtherName = appendModelPrefix(other, prefix);
        var element = $(options.form).find(':input').filter('[name="' + escapeAttributeValue(fullOtherName) + '"]')[0];
        options.rules['notEqualTo'] = '#' + $(element).attr('id');
        if (options.message)
            options.messages['notEqualTo'] = options.message;
    });

    // require checked
    $.validator.addMethod('requirechecked', function (value, element) {
        return element.checked;
    });
    $.validator.unobtrusive.adapters.addBool('requirechecked');

    // required if
    $.validator.addMethod('requiredif', function (value, element, parameters) {
        var id = '#' + parameters['dependentproperty'];

        var targetvalue = parameters['targetvalue'];
        targetvalue = (targetvalue == null ? '' : targetvalue).toString();

        var control = $(id);
        var controltype = control.attr('type');
        var actualvalue = controltype === 'checkbox' ? control.is(':checked').toString() : control.val();
        
        // boolean
        if (actualvalue.toUpperCase() === 'TRUE') {
            actualvalue = 'true';
        } else if (actualvalue.toUpperCase() === 'FALSE') {
            actualvalue = 'false';
        }
        
        if (targetvalue === actualvalue)
            return $.validator.methods.required.call(
              this, value, element, parameters);

        return true;
    });
    $.validator.unobtrusive.adapters.add('requiredif', ['dependentproperty', 'targetvalue'], function (options) {
        options.rules['requiredif'] = {
            dependentproperty: options.params['dependentproperty'],
            targetvalue: options.params['targetvalue']
        };
        options.messages['requiredif'] = options.message;
    });

    // required ifnot
    $.validator.addMethod('requiredifnot', function (value, element, parameters) {
        var id = '#' + parameters['dependentproperty'];

        var targetvalue = parameters['targetvalue'];
        targetvalue = (targetvalue == null ? '' : targetvalue).toString();

        var control = $(id);
        var controltype = control.attr('type');
        var actualvalue = controltype === 'checkbox' ? control.attr('checked').toString() : control.val();

        if (targetvalue !== actualvalue)
            return $.validator.methods.required.call(this, value, element, parameters);

        return true;
    });
    $.validator.unobtrusive.adapters.add('requiredifnot', ['dependentproperty', 'targetvalue'], function (options) {
        options.rules['requiredifnot'] = {
            dependentproperty: options.params['dependentproperty'],
            targetvalue: options.params['targetvalue']
        };
        options.messages['requiredifnot'] = options.message;
    });

})(jQuery);
﻿window.log = window.log || {};

window.log = {
    log: window.console.log.bind(window.console, 'LOG >>'),
    error: window.console.error.bind(window.console, 'ERROR >>'),
    info: window.console.info.bind(window.console, 'INFO >>'),
    warn: window.console.warn.bind(window.console, 'WARN >>'),
    debug: window.console.debug.bind(window.console, 'DEBUG >>'),
    trace: window.console.debug.bind(window.console, 'TRACE >>')
};
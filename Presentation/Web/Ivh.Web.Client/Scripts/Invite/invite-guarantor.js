﻿(function (visitPay, $, ko) {
    // view model
    var inviteGuarantorViewModel = function () {
        //
        var self = this;
        self.FirstName = ko.observable();
        self.MiddleName = ko.observable();
        self.LastName = ko.observable();
        self.Email = ko.observable();
    };

    var resetForm = function (model) {
        // reset viewmodel
        model.FirstName('');
        model.MiddleName('');
        model.LastName('');
        model.Email('');
    };

    var resetInvitationSentMessage = function () {
        $('#InvitationSentMessage').addClass('hidden');
    };

    $('#FirstName').on('click', function () {
        resetInvitationSentMessage();
    });

    $('#MiddleName').on('click', function () {
        resetInvitationSentMessage();
    });

    $('#LastName').on('click', function () {
        resetInvitationSentMessage();
    });

    $('#Email').on('click', function () {
        resetInvitationSentMessage();
    });

    //
    $(document).ready(function () {

        var model = new inviteGuarantorViewModel();
        ko.applyBindings(model, $('#section-invite')[0]);

        $('#frmInviteGuarantor').on('submit', function (e) {
            e.preventDefault();
            visitPay.Common.ResetValidationSummary($(this));

            var self = $(this);
            if (self.valid()) {
                $.blockUI();
                $.ajax({
                    url: '/Invite/InviteGuarantor',
                    type: 'POST',
                    dataType: 'json',
                    data: ko.toJS(model)
                }).done(function(result) {
                    if (result.Result) {
                        $('#InvitationSentMessage').removeClass('hidden');
                        resetForm(model);
                    }
                    $.unblockUI();
                }).fail(function() {
                    $.unblockUI();
                });
            }
        });
    });

})(window.VisitPay, jQuery, window.ko || {});
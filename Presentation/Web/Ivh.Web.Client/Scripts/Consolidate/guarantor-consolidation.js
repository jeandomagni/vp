﻿window.VisitPay.GuarantorConsolidation = (function(common, $) {

    var self = this;

    var hasInitialized = false;
    self.Initialize = function(visitPayUserId) {

        if (hasInitialized)
            return;
        else {
            $.blockUI();
            hasInitialized = true;
        }

        $.post('/Search/GuarantorConsolidations', { guarantorVisitPayUserId: visitPayUserId }, function(html) {

            $('.tab-content > #tabConsolidation').html(html);
            $.unblockUI();

        });

    };

    return self;

}(window.VisitPay.Common || {}, jQuery));
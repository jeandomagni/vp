﻿window.VisitPay.VisitPayDocument = (function(common, $) {

    var model;
    var initialDateRange;
    var $scope;
    var filterViewModel = function() {
        var self = this;

        self.VisitPayDocumentStatus = ko.observable('');
        self.DateRangeFrom = ko.observable();

        self.RunResults = ko.observable(false);
        self.ChangeNotifications = function() {

            var setRun = function() {
                self.RunResults(true);
            };

            self.VisitPayDocumentStatus.subscribe(setRun);
            self.DateRangeFrom.subscribe(setRun);

        };
        return self;
    };

    var grid;
    var gridDefaultSortName = 'VisitPayDocumentId';
    var gridDefaultSortOrder = 'asc';
    
    var setDatePicker = function ($form, orientation) {

        if (orientation === undefined || orientation === null || orientation.length <= 0) {
            orientation = 'bottom left';
        }

        $('.input-group.date').each(function () {
            var element = $(this);
            element.datepicker({
                clearBtn: true,
                format: 'mm/dd/yyyy',
                orientation: orientation,
                autoclose: true
            }).on('hide', function () {
                if ($form.valid()) {
                     common.ResetUnobtrusiveValidation($form);
                }
            });
        });
    };
    
    var reloadGrid = function() {
        var postModel = ko.toJS(model);
        $.extend(grid.jqGrid('getGridParam', 'postData'), { model: postModel });
        grid.setGridParam({ page: 1 });
        grid.sortGrid(gridDefaultSortName, true, gridDefaultSortOrder);
    };

    var resetFilters = function() {
        //todo
        model.DateRangeFrom(initialDateRange);
        model.RunResults(false);
    };

    var editVisitPayDocument= function(id) {

        window.VisitPay.Common.ModalNoContainerAsync('modalEditVisitPayDocument', '/VisitPayDocument/EditVisitPayDocument?visitPayDocumentId=' + id, 'GET', {}, true)
            .done(function($modal) {

                $modal.find('.modal-title').text('Edit Document');
                var $form = $modal.find('form');

                setDatePicker($form);
                $form.parseValidation();
                $form.on('submit', function(e) {
                    e.preventDefault();
                    if (!$(this).valid()) {
                        return;
                    }
                    $modal.modal('hide');
                    
                    var action = $(this).attr('action');
                    var data = $(this).serializeToObject();

                    $.blockUI();
                    $.post(action, data, function(data) {
                        reloadGrid();
                    });
                });

                $modal.modal('show');
            });
    };

    var deleteAttachment = function (visitPayDocumentId, visitPayDocumentName) {

        var dialogParameters = {
            title: 'Confirm Delete',
            content: 'Are you sure you want to delete ' + visitPayDocumentName + '?',
            confirmText: 'Yes',
            cancelText: 'No',
            footerText: '',
            headerClass: ''
        };

        var promise = window.VisitPay.Common.ModalGenericConfirmation(
            dialogParameters.title,
            dialogParameters.content,
            dialogParameters.confirmText,
            dialogParameters.cancelText,
            dialogParameters.footerText,
            dialogParameters.headerClass
        );
        
        promise.done(function() {
            var data = { visitPayDocumentId: visitPayDocumentId};
            var deleteUrl = '/VisitPayDocument/DeleteVisitPayDocument';

            $.post(
                deleteUrl,
                data,
                function (result) {
                    reloadGrid();
                }
            );
        });

    };

    var bindGrid = function($scope) {

        grid.jqGrid({
            url: 'VisitPayDocument/VisitPayDocuments',
            jsonReader: { root: 'VisitPayDocuments' },
            postData: { model: ko.toJS(model) },
            pager: 'jqGridPagerVisitPayDocuments',
            loadComplete: function(data) {

                grid.find('.jqgrow').each(function() {

                    var row = data.VisitPayDocuments[$(this).index() - 1];

                    $(this).on('click', '.edit-visit-pay-document', function(e) {
                        e.preventDefault();
                        editVisitPayDocument(row.VisitPayDocumentId);
                    });

                    $(this).on('click', '.delete-visit-pay-document', function(e) {
                        e.preventDefault();
                        deleteAttachment(row.VisitPayDocumentId, row.AttachmentFileName);
                    });
                });

                var gridContainer = grid.parents('.ui-jqgrid:eq(0)');
                var gridNoRecords = $scope.find('#jqGridNoRecordsVisitPayDocuments');
                if (data.VisitPayDocuments != null && data.VisitPayDocuments.length > 0) {
                    gridContainer.show();
                    gridNoRecords.removeClass('in');
                } else {
                    gridContainer.hide();
                    gridNoRecords.addClass('in');
                }
            },

            gridComplete: function() {
                var gridContainer = grid.parents('.ui-jqgrid:eq(0)');
                gridContainer.addClass('visible');
                model.RunResults(false);
                $.unblockUI();
            },
            //VisitPayDocumentResultViewModel
            colModel: [
                {
                    label: 'Id',
                    name: 'VisitPayDocumentId',
                    width: 10,
                    resizable: false
                },
                {
                    label: 'Edit',
                    name: '',
                    width: 10,
                    sortable: false,
                    resizable: false,
                    formatter: function(cellValue, options, rowObject) {
                        return '<a href="javascript:;" class="edit-visit-pay-document" data-toggle="tooltip" data-placement="right" title="Edit Document" >Edit</a>';
                    }
                },
                {
                    label: 'Delete',
                    name: '',
                    width: 10,
                    resizable: false,
                    sortable: false,
                    formatter: function(cellValue, options, rowObject) {
                        return '<a href="javascript:;" class="delete-visit-pay-document" data-toggle="tooltip" data-placement="right" title="Delete Document" >Delete</a>';
                    }
                },
                {
                    label: 'Attachment File Name',
                    name: 'AttachmentFileName',
                    width: 30,
                    resizable: false,
                    formatter: function(cellValue, options, rowObject) {
                        return '<a href="VisitPayDocument/DownloadAttachment?visitPayDocumentId=' + rowObject.VisitPayDocumentId + '" data-toggle="tooltip" data-placement="right" title="Download Attachment" target="_blank">' + cellValue + '</a>';
                    }
                },
                {
                    label: 'Category',
                    name: 'VisitPayDocumentType',
                    width: 20,
                    resizable: false
                },
                {
                    label: 'Display Title',
                    name: 'DisplayTitle',
                    width: 30,
                    resizable: false
                },
                {
                    label: 'Uploaded By',
                    name: 'UploadedByUserName',
                    width: 15,
                    resizable: false,
                    sortable: false
                },
                {
                    label: 'Approved By',
                    name: 'ApprovedByUserName',
                    width: 20,
                    resizable: false,
                    sortable: false
                },
                {
                    label: 'Active Date',
                    name: 'ActiveDateString',
                    width: 20,
                    resizable: false
                }
            ]

            
        });

    };

    var bindFilter = function () {
        //
        $scope = $('#visit-pay-document-section');
        grid = $scope.find('#jqGridVisitPayDocuments');

        initialDateRange = $('#DateRangeFrom').val();

        model = new filterViewModel();
        ko.applyBindings(model, $('#jqGridFilterVisitPayDocuments')[0]);
        model.ChangeNotifications();

        resetFilters();

        $scope.on('submit', '#jqGridFilterVisitPayDocuments', function (e) {
            e.preventDefault();
            $.blockUI();
            reloadGrid();
        });
        $scope.on('click', '#btnGridFilterTrainingDocumentReset', 'click', function () {
            resetFilters();
            $.blockUI();
            reloadGrid();
        });
    };

    var serializeFiles = function ($form) {
        var form = $form;
        var formData = new FormData();
        var formParams = form.serializeArray();

        $.each(form.find('input[type="file"]'), function (i, tag) {
            $.each($(tag)[0].files, function (i, file) {
                formData.append(tag.name, file);
            });
        });

        $.each(formParams, function (i, val) {
            formData.append(val.name, val.value);
        });

        return formData;
    };

    $(document).ready(function () {
        
        $('#uploadButton').click(function () {
            window.VisitPay.Common.ModalNoContainerAsync(
                    'modalUploadVisitPayDocument', //_UploadTrainingDocument modal id
                    '/VisitPayDocument/UploadVisitPayDocumentModal',
                    'GET',
                    {},
                    true
                )
                .done(function ($modal) {

                    $modal.find('.modal-title').text('Upload Attachment');

                    var $form = $modal.find('form');
                    setDatePicker($form);
                    $form.parseValidation();

                    $form.off('submit').on('submit', function (e) {
                        e.preventDefault();

                        if (!$form.valid()) {
                            return;
                        }

                        var formData = serializeFiles($form);
                        $.ajax({
                            url: $form.attr('action'),
                            type: 'POST',
                            data: formData,
                            processData: false,  // tell jQuery not to process the data
                            contentType: false,  // tell jQuery not to set contentType
                            success: function (data) {

                                if (data.success == true) {
                                    $modal.modal('hide');
                                    reloadGrid();
                                } else {
                                    var $errorElement = $form.find('[data-valmsg-for="' + data.errorProperty + '"]');
                                    $errorElement.removeClass('field-validation-valid').addClass('field-validation-error');
                                    $errorElement.text(data.errorMessage);
                                    var $inputElement = $form.find('#' + data.errorProperty);
                                    $inputElement.removeClass('valid').addClass('input-validation-error');
              
                                }
                            }
                        });

                    });

                    $modal.modal('show');
                });
        });
    });

    bindFilter();
    bindGrid($scope);
    

})(
    window.VisitPay.Common = window.VisitPay.Common || {},
    jQuery
);
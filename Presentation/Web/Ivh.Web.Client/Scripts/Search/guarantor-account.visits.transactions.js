﻿window.VisitPay.VisitTransactions = (function(common, $, ko) {

    var filterViewModel = function() {

        var self = this;

        self.TransactionType = ko.observable('');
        self.VisitPayUserId = ko.observable();

        self.RunResults = ko.observable(false);
        self.ChangeNotifications = function() {

            var setRun = function() {
                self.RunResults(true);
            };

            self.TransactionType.subscribe(setRun);

        };
    };

    var visitTransactions = {};

    visitTransactions.Initialize = function(visitPayUserId) {

        //
        var scope = $('#modalVisitTransactions');
        scope.find('.modal').css('position', 'absolute');

        //
        var model = new filterViewModel();
        ko.applyBindings(model, $('#jqGridVisitTransactionsFilter')[0]);
        model.VisitPayUserId(visitPayUserId);
        model.ChangeNotifications();

        //
        var grid = scope.find('#jqGridVisitTransactions');
        var gridDefaultSortName = 'DisplayDate';
        var gridDefaultSortOrder = 'asc';

        //
        var reloadGrid = function() {
            var postModel = ko.toJS(model);
            if (postModel.TransactionType.length === 0) {
                delete postModel.TransactionType;
            }
            postModel.VisitId = scope.find('#SearchFilter_VisitId').val();
            $.extend(grid.jqGrid('getGridParam', 'postData'), { model: postModel });
            grid.setGridParam({ page: 1 });
            grid.sortGrid(gridDefaultSortName, true, gridDefaultSortOrder);
        };
        var resetFilters = function() {
            model.TransactionType('');
            model.RunResults(false);
        };

        //
        $('#jqGridVisitTransactionsFilter').on('submit', function(e) {
            e.preventDefault();
            $.blockUI();
            reloadGrid();
        });
        $('#btnGridVisitTransactionsFilterReset').on('click', function() {
            $.blockUI();
            resetFilters();
            reloadGrid();
        });
        $('#btnVisitTransactionsExport').on('click', function(e) {

            e.preventDefault();

            var postModel = ko.toJS(model);
            if (postModel.TransactionType.length === 0) {
                delete postModel.TransactionType;
            }
            postModel.VisitId = scope.find('#SearchFilter_VisitId').val();

            $.post('/Search/GuarantorVisitTransactionsExport', {
                model: postModel,
                sidx: grid.jqGrid('getGridParam', 'sortname'),
                sord: grid.jqGrid('getGridParam', 'sortorder')
            }, function(result) {
                if (result.Result === true)
                    window.location.href = result.Message;
            }, 'json');

        });

        //
        grid.jqGrid({
            url: '/Search/GuarantorVisitTransactions',
            postData: {
                model: {
                    visitId: scope.find('#SearchFilter_VisitId').val(),
                    VisitPayUserId: model.VisitPayUserId
                }
            },
            jsonReader: { root: 'Transactions' },
            pager: 'jqGridVisitTransactionsPager',
            sortname: gridDefaultSortName,
            sortorder: gridDefaultSortOrder,
            height: 1200, // overridden in css, this just makes the scrollbar appear correctly
            footerrow: false,
            loadComplete: function(data) {

                var gridContainer = grid.parents('.ui-jqgrid:eq(0)');
                var gridNoRecords = $('#jqGridVisitTransactionsNoRecords');

                if (data.Transactions != null && data.Transactions.length > 0) {

                    gridContainer.show();
                    gridNoRecords.removeClass('in');
                    grid.closest('.ui-jqgrid-bdiv').scrollTop(0);

                } else {

                    gridContainer.hide();
                    gridNoRecords.addClass('in');
                    grid.closest('.ui-jqgrid-bdiv').scrollTop(0);
                }

                model.RunResults(false);
                $.unblockUI();

            },
            gridComplete: function() {

                grid.find('[data-toggle="tooltip"]').tooltip({ animation: false, container: 'body' });

                $('#jqGridVisitTransactionsPager').find('#input_jqGridPager').find('input').off('change').on('change', function() {
                    grid.jqGrid().trigger('reloadGrid', [{ page: $(this).val() }]);
                });

                var gridContainer = grid.parents('.ui-jqgrid:eq(0)');
                gridContainer.addClass('visible');

            },
            colModel: [
                { label: 'Date', name: 'DisplayDate', width: 84, resizable: false },
                { label: 'Description', name: 'TransactionDescription', width: 675, resizable: false, sortable: false },
                {
                    label: 'Amount',
                    name: 'TransactionAmount',
                    width: 106,
                    resizable: false,
                    sortable: false,
                    formatter: function (cellValue, options, rowObject) {

                        var html = '<span style="display: block; position: relative;" class="text-right">';
                        
                        html += cellValue;

                        if (rowObject.CanIgnore )
                            html += '<span class="glyphicon glyphicon-eye-open" style="color: #ff7a32; padding-left: 5px" data-toggle="tooltip" data-placement="top" title="This transaction is filtered for guarantor"></span>';

                        html += '</span>';

                        return html;
                    }
                }
            ]
        });

    };

    return visitTransactions;

})(
    window.VisitPay.Common = window.VisitPay.Common || {},
    jQuery,
    window.ko);
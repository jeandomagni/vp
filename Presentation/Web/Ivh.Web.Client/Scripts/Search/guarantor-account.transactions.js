﻿window.VisitPay.GuarantorAccountTransactions = (function(common, visitTransactions, $, ko) {

    var guarantorAccountTransactions = {};
    var filterViewModel = function () {

        var self = this;
        
        self.TransactionDateRangeFrom = ko.observable('');
        self.TransactionDateRangeTo = ko.observable('');
        self.TransactionType = ko.observable('');
        self.VpGuarantorId = ko.observable('');
        self.VisitPayUserId = ko.observable();
        self.CurrentVisitPayUserId = ko.observable();

        self.RunResults = ko.observable(false);
        self.ChangeNotifications = function () {

            var setRun = function () {
                self.RunResults(true);
            };
            
            self.TransactionDateRangeFrom.subscribe(setRun);
            self.TransactionDateRangeTo.subscribe(setRun);
            self.TransactionType.subscribe(setRun);
            self.VpGuarantorId.subscribe(setRun);

        };

    };

    var hasInitialized = false;
    var model;

    var bindFilters = function () {
        
        $('#jqGridFilterTransactions').find('[data-toggle="tooltip"]').tooltip({ animation: false, container: 'body' });

        // prevent focus on validate failure to prevent calendar from opening
        $('#jqGridFilterTransactions').on('invalid-form.validate', function (form, validator) {
            validator.settings && (validator.settings.focusInvalid = false);
        });

        //
        $('#section-transactions .input-group.date').each(function () {
            var element = $(this);
            element.datepicker({
                clearBtn: true,
                format: 'mm/dd/yyyy',
                orientation: 'top left',
                autoclose: true
            }).on('hide', function () {
                if ($('#jqGridFilterTransactions').valid())
                    common.ResetUnobtrusiveValidation($('#jqGridFilterTransactions'));
            });
        });
        
    };

    var resetModel = function () {
        
        model.TransactionDateRangeFrom('');
        model.TransactionDateRangeTo('');
        model.TransactionType('');
        model.VpGuarantorId('');
        model.RunResults(false);

    };

    var resetFilters = function () {
        resetModel();
        common.ResetUnobtrusiveValidation($('#jqGridFilterTransactions'));
    };

    var setRecordCount = function(count) {
        guarantorAccountTransactions.OnDataLoaded(count);
    };

    guarantorAccountTransactions.Setup = function (currentVisitPayUserId, filteredVisitPayUserId) {

        var promise = $.Deferred();
        var uid = filteredVisitPayUserId || currentVisitPayUserId;
        
        if (!model) {
            model = new filterViewModel();
            model.ChangeNotifications();
            ko.applyBindings(model, $('#jqGridFilterTransactions')[0]);
        }

        model.CurrentVisitPayUserId(currentVisitPayUserId);
        model.VisitPayUserId(uid);

        bindFilters();
        resetModel();
        resetFilters();

        $.jgrid.gridUnload('#jqGridTransactions');

        hasInitialized = false;

        promise.resolve();

        $(document).on('visitUnmatchActionComplete', function() {
            if (hasInitialized) {

                var grid = $('#jqGridTransactions');
                $.extend(grid.jqGrid('getGridParam', 'postData'), { model: ko.toJS(model) });
                grid.setGridParam({ page: 1 });
                grid.sortGrid(grid.jqGrid('getGridParam', 'sortname'), true, grid.jqGrid('getGridParam', 'sortorder'));

            } else {

                guarantorAccountTransactions.GetInitialCount();

            }
        });

        return promise;
    };

    guarantorAccountTransactions.OnDataLoaded = function () { };

    guarantorAccountTransactions.GetInitialCount = function() {

        //
        var deferred = $.Deferred();

        //
        resetModel();

        //
        $.post('/Search/TransactionTotals', { model: ko.toJS(model) }, function (data) {
            setRecordCount(data);
            deferred.resolve();
        }, 'json');

        //
        return deferred;

    };

    guarantorAccountTransactions.Initialize = function() {

        if (hasInitialized)
            return;
        else {
            $.blockUI();
            hasInitialized = true;
        }

        //
        var grid = $('#jqGridTransactions');
        var gridDefaultSortOrder = 'asc';
        var gridDefaultSortName = 'DisplayDate';

        //
        var reloadGrid = function () {
            $.extend(grid.jqGrid('getGridParam', 'postData'), { model: ko.toJS(model) });
            grid.setGridParam({ page: 1 });
            grid.sortGrid(gridDefaultSortName, true, gridDefaultSortOrder);
        };

        //
        $('#jqGridFilterTransactions').on('submit', function (e) {
            e.preventDefault();
            if ($(this).valid()) {
                $.blockUI();
                reloadGrid();
            }
        });
        $('#btnGridFilterTransactionsReset').on('click', function () {
            $.blockUI();
            resetFilters();
            reloadGrid();
        });
        $('#btnTransactionsExport').on('click', function (e) {

            e.preventDefault();
            $.post('/Search/GuarantorTransactionsExport', {
                model: ko.toJS(model),
                sidx: grid.jqGrid('getGridParam', 'sortname'),
                sord: grid.jqGrid('getGridParam', 'sortorder')
            }, function (result) {
                if (result.Result === true)
                    window.location.href = result.Message;
            }, 'json');

        });

        //
        resetFilters();

    	//
        grid.jqGrid({
        	url: '/Search/GuarantorTransactions',
        	jsonReader: { root: 'Transactions' },
        	postData: { model: ko.toJS(model) },
        	pager: 'jqGridPagerTransactions',
        	sortname: gridDefaultSortName,
        	sortorder: gridDefaultSortOrder,
        	rowNum: 50,
        	footerrow: false,
        	loadComplete: function (data) {

        		var gridContainer = grid.parents('.ui-jqgrid:eq(0)');
        		var gridNoRecordsInitial = $('#jqGridNoRecordsInitialTransactions');
        		var gridNoRecords = $('#jqGridNoRecordsTransactions');

        		if (data.Transactions != null && data.Transactions.length > 0) {

        		    gridContainer.show();

		            gridNoRecords.removeClass('in');
		            gridNoRecordsInitial.removeClass('in');

        			grid.off('click').on('click', '.jqgrow > td > a', function () {

        				var rowData = data.Transactions[$(this).parents('tr').index() - 1];

        				$.blockUI();
			
						$.get('/Search/GuarantorVisitTransactions/' + rowData.VisitId + '?visitPayUserId=' + model.VisitPayUserId(), {}, function (html) {
			
							$('#modalVisitTransactionsContainer').html(html);
							$('#modalVisitTransactionsContainer').find('.modal').modal();
						    visitTransactions.Initialize(model.VisitPayUserId());
			
							$.unblockUI();
			
						});

        			});

        		} else {

        		    gridContainer.hide();
        		    gridNoRecords.removeClass('in');
        		    gridNoRecordsInitial.removeClass('in');
		            gridNoRecords.addClass('in');

		        }

	            setRecordCount(data.records);
        		model.RunResults(false);
        		$.unblockUI();
        	},
        	gridComplete: function () {

        	    grid.find('[data-toggle="tooltip"]').tooltip({ animation: false, container: 'body' });

        		$('#jqGridPagerTransactions').find('#input_jqGridPager').find('input').off('change').on('change', function () {
        			grid.jqGrid().trigger('reloadGrid', [{ page: $(this).val() }]);
        		});

        		var gridContainer = grid.parents('.ui-jqgrid:eq(0)');
        		gridContainer.addClass('visible');

        	},
        	colModel: [
                { label: 'Date', name: 'DisplayDate', width: 80, resizable: false },
                { label: 'Patient Name', name: 'PatientName', width: 132, resizable: false },
                { label: 'Visit #', name: 'VisitSourceSystemKeyDisplay', width: 97, resizable: false, formatter: function (cellValue, options, rowObject) {
                    if (rowObject.IsVisitRemoved) {
                        return '<div data-toggle="tooltip" data-placement="right" title="Removed Visit# - ' + rowObject.VisitMatchedSourceSystemKey + '">' + cellValue + '</div>';
                    }
                     return '<a href="javascript:;" data-toggle="tooltip" data-placement="right" title="' + cellValue + ' - View Transaction Details" class="view-transaction-details">' + cellValue + '</a>';
                } },
                { label: 'Description', name: 'TransactionDescription', resizable: false, width: 279 },
                { label: 'Transaction Type', name: 'TransactionType', width: 124, resizable: false },
                { label: 'Amount', name: 'TransactionAmount', width: 104, resizable: false, classes: 'text-right', formatter: function(cellValue, options, rowObject) {
                    if (rowObject.IsVisitRemoved) {
                        return '<div class="text-center">-</div>';
                    }
                    return cellValue;
                } },
        	    { label: 'Visit Status', name: 'VisitStateFullDisplayName', width: 175, resizable: false, formatter: function(cellValue, options, rowObject) {
	                 return (rowObject.IsPastDue) ? '<span style="color: red;" class="visitstatus3">' + cellValue + '</span>' : cellValue;
	            } }
        	]
        });
    };

    return guarantorAccountTransactions;

})(window.VisitPay.Common = window.VisitPay.Common || {},
    window.VisitPay.VisitTransactions = window.VisitPay.VisitTransactions || {},
    jQuery,
    window.ko);
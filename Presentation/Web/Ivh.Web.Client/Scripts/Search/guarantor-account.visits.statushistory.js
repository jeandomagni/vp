﻿window.VisitPay.VisitStateHistory = (function (common, $, ko) {

    var visitStateHistory = {};

    var filterViewModel = function () {
        var self = this;
        self.VisitId = ko.observable();
        self.VisitPayUserId = ko.observable();
        self.VisitStateIds = ko.observableArray([]);
        self.RunResults = ko.observable(false);
        self.ChangeNotifications = function () {

            var setRun = function () {
                self.RunResults(true);
            };

            self.VisitStateIds.subscribe(setRun);
        };
    };

    visitStateHistory.Initialize = function(visitPayUserId, visit) {

        if (visit != null) {
            var visitId = visit.VisitId;
            var sourceSystemKey = visit.SourceSystemKey;
            var isRemoved = visit.IsRemoved;

            var titleSourceSystemKey = isRemoved ? sourceSystemKey : 'Visit #' + sourceSystemKey;

            $('#spVisitStateHistorySourceSystemKey').text(titleSourceSystemKey);
            var model = new filterViewModel();
            ko.applyBindings(model, $('#jqGridFilterVisitStateHistory')[0]);
            model.VisitPayUserId(visitPayUserId);
            model.VisitId(visitId);
            model.ChangeNotifications();

            //
            var scope = $('#modalVisitStateHistoryContainer');
            scope.find('.modal').css('position', 'absolute');

            //
            var grid = scope.find('#jqGridVisitStateHistory');
            var gridDefaultSortName = 'InsertDate';
            var gridDefaultSortOrder = 'desc';

            //
            var reloadGrid = function() {
                $.extend(grid.jqGrid('getGridParam', 'postData'), { model: ko.toJS(model) });
                grid.setGridParam({ page: 1 });
                grid.sortGrid(gridDefaultSortName, true, gridDefaultSortOrder);
            };
            var resetFilters = function() {

                model.VisitStateIds([]);

                visitStateSelect.find('option').prop('selected', false);

                ko.utils.arrayForEach(model.VisitStateIds(),
                    function(visitStateId) {
                        $('select#VisitStateHistoryVisitStateIds option[value="' + visitStateId + '"]').prop('selected', true);
                    });

                visitStateSelect.multiselect('refresh');
            };

            var visitStateSelect = $('select#VisitStateHistoryVisitStateIds');
            visitStateSelect.multiselect({
                enableClickableOptGroups: true,
                includeSelectAllOption: true,
                allSelectedText: 'All Statuses',
                nonSelectedText: 'All Statuses',
                nSelectedText: 'Statuses',
                numberDisplayed: 1,
                selectAllNumber: false,
                selectAllText: 'All Statuses',
                onChange: function() {
                    model.VisitStateIds = new ko.observableArray([]);
                    visitStateSelect.find('option:selected').each(function() {
                        model.VisitStateIds.push($(this).val());
                    });
                    $.blockUI();
                    reloadGrid();
                }
            });

            //
            $('#jqGridFilterVisitStateHistory').on('submit',
                function(e) {
                    e.preventDefault();
                    $.blockUI();
                    reloadGrid();
                });
            $('#btnGridFilterVisitStateHistoryReset').on('click',
                function() {
                    $.blockUI();
                    resetFilters();
                    reloadGrid();
                });

            //
            resetFilters();

            //
            grid.jqGrid({
                url: '/Search/GuarantorVisitStateHistoryData',
                jsonReader: { root: 'VisitStateHistory' },
                postData: { model: ko.toJS(model) },
                pager: 'jqGridVisitStateHistoryPager',
                sortname: gridDefaultSortName,
                sortorder: gridDefaultSortOrder,
                height: 1200, // overridden in css, this just makes the scrollbar appear correctly
                loadComplete: function(data) {

                    var gridContainer = grid.parents('.ui-jqgrid:eq(0)');
                    var gridNoRecords = $('#jqGridVisitStateHistoryNoRecords');

                    if (data.VisitStateHistory != null && data.VisitStateHistory.length > 0) {

                        gridContainer.show();
                        gridNoRecords.removeClass('in');

                    } else {

                        gridContainer.hide();
                        gridNoRecords.addClass('in');

                    }

                    model.RunResults(false);
                    $.unblockUI();
                },
                gridComplete: function() {
                    $('#jqGridVisitStateHistoryPager').find('#input_jqGridPager').find('input').off('change').on('change',
                        function() {
                            grid.jqGrid().trigger('reloadGrid', [{ page: $(this).val() }]);
                        });
                    var gridContainer = grid.parents('.ui-jqgrid:eq(0)');
                    gridContainer.addClass('visible');
                },
                colModel: [
                    {
                        label: 'Date Changed',
                        formatter: function(cellValue, options, rowObject) {
                            return (rowObject.InsertDate === '') ? 'No' + cellValue : cellValue;
                        },
                        name: 'InsertDate',
                        width: 200,
                        resizable: false,
                        sortable: true
                    },
                    {
                        label: 'Status',
                        name: 'VisitStateFullDisplayName',
                        width: 250,
                        resizable: false,
                        key: true,
                        formatter: function(cellValue, options, rowObject) {
                            return (rowObject.VisitStateId === 3 || rowObject.VisitStateId === 11) ? '<span style="color: red;">' + cellValue + '</span>' : cellValue;
                        },
                        sortable: true
                    },
                    { label: 'Reason', name: 'ChangeDescription', width: 415, resizable: false, key: true, sortable: true }
                ]
            });
        }
    };

    return visitStateHistory;

})(window.VisitPay.Common = window.VisitPay.Common || {}, jQuery, window.ko);
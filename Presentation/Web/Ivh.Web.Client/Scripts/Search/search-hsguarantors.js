﻿(function (searchGuarantors, clientSettings, $, ko) {

    var searchHsGuarantorsVm = function () {

        var self = this;

        self.HsGuarantorId = ko.observable('');
        self.VpGuarantorId = ko.observable('');
        self.FirstName = ko.observable('');
        self.LastName = ko.observable('');
        self.Ssn4 = ko.observable('');
        self.DateOfBirth = ko.observable('');
        self.UserNameOrEmailAddress = ko.observableArray([]);

        self.IsValid = ko.computed(function () {
            return ko.unwrap(self.HsGuarantorId).length > 0 &&
                ko.unwrap(self.LastName).length > 0 &&
                ko.unwrap(self.DateOfBirth).length > 0;

        });
        self.RunResults = ko.observable(false);
        self.ChangeNotifications = function () {

            var setRun = function () {
                self.RunResults(true);
            };

            self.HsGuarantorId.subscribe(setRun);
            self.VpGuarantorId.subscribe(setRun);
            self.FirstName.subscribe(setRun);
            self.LastName.subscribe(setRun);
            self.Ssn4.subscribe(setRun);
            self.UserNameOrEmailAddress.subscribe(setRun);
            self.DateOfBirth.subscribe(setRun);

        };

        return self;
    };

    var columnModel = [
        {
            label: 'NAME',
            name: 'Name',
            width: 150,
            resizable: false,
            formatter: function (cellValue, options, rowObject) {
                return '<a class="bold-link" title="Create Account" href="/Search/CreateVpGuarantor/' + rowObject.HsGuarantorId + '">' + cellValue + '</a>';
            }
        },
        {
            label: clientSettings.HsGuarantorPatientIdentifier.toUpperCase(),
            name: 'SourceSystemKeys',
            width: 112,
            resizable: false,
            formatter: function (cellValue, options, rowObject) {
                return '<span title="' + rowObject.SourceSystemKey + '"><span>' + rowObject.SourceSystemKey + '</span>';
            }
        },
        { label: 'LAST 4 SSN', name: 'Ssn4', width: 104, resizable: false },
        {
            label: 'DOB',
            name: 'DateOfBirth',
            resizable: false,
            width: 120,
            sorttype: 'date',
            formatter: 'date',
            formatoptions: { srcformat: 'm/d/Y', newformat: 'm/d/Y' }
        },
        {
            label: '', //ACTION
            name: '',
            width: 150,
            resizable: false,
            sortable: false,
            classes: 'text-center',
            formatter: function (cellValue, options, rowObject) {
                var html = '<a class="bold-link" href="/Search/CreateVpGuarantor/' + rowObject.HsGuarantorId + '">Create Account</a>';

                return html;
            }
        }
    ];

    searchGuarantors.SearchHsGuarantors = (function (data) {

        $(document).ready(function () {

            var search = new window.VisitPay.SearchGuarantors.Main(searchHsGuarantorsVm, data.urls, columnModel);
            search.init();

        });

    });


})(window.VisitPay.SearchGuarantors = window.VisitPay.SearchGuarantors || {}, window.VisitPay.ClientSettings = window.VisitPay.ClientSettings || {}, jQuery, window.ko);
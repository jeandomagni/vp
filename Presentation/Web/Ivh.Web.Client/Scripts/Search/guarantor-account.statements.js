﻿window.VisitPay.GuarantorAccountStatements = (function (common, $, ko) {

    //
    var statements = {};
    var statementViewModel = function () {
        var self = this;

        self.StatementedSnapshotTotalBalance = ko.observable('');
        self.StatementDate = ko.observable('');
        self.StatementId = ko.observable(0);
        self.StatementIsSelected = ko.computed(function () {
            return ko.unwrap(self.StatementId) > 0;
        });
        return self;
    };

    //
    var hasInitialized = false;
    var visitPayUserId;
    var $scope;
    var statementModel;
    var visitBalanceSummaryModel;

    function bindStatement(e, innerScope) {
        var $innerScope = innerScope || $(this);
        var id = $innerScope.find('input[type=hidden][name=statementId]').val();
        var balance = $innerScope.find('input[type=hidden][name=statementBalance]').val();
        var date = $innerScope.find('input[type=hidden][name=statementDate]').val();

        e.data.model.StatementId(id);
        e.data.model.StatementDate(date);
        e.data.model.StatementedSnapshotTotalBalance(balance);
    };

    function downloadDocument(e) {

        e.preventDefault();

        if (!e.data.model.StatementIsSelected()) {
            bindStatement(e, $(this));
        }

        if (window.VisitPay.State.IsEmulation) {
            $('.modal').modal('hide');
            $('#modalEmulate').modal('show');
            $('#modalEmulate').find('.modal-body').html('<p>Emulated User Not Authorized for this action.</p>');
        } else {
            window.open(e.data.actionUrl + e.data.model.StatementId() + '?guarantorVisitPayUserId=' + visitPayUserId);
        }
    };

    function setRecordCount(count) {
        statements.OnDataLoaded(count);
    };

    function init() {
        statementModel = new statementViewModel();
        visitBalanceSummaryModel = new statementViewModel();

        $scope.on('click', '#ddStatement button.btn-download', { model: statementModel, actionUrl: '/Statement/FinancePlanStatementDownload/' }, downloadDocument);
        $scope.on('click', '#ddVisitBalanceSummary button.btn-download', { model: visitBalanceSummaryModel, actionUrl: '/Statement/VisitBalanceSummaryDownload/' }, downloadDocument);
        $scope.on('click', '#ddStatement li:not(.archived-separator)', { model: statementModel }, bindStatement);
        $scope.on('click', '#ddVisitBalanceSummary li:not(.archived-separator)', { model: visitBalanceSummaryModel }, bindStatement);

        var $fpStatements = $scope.find('#ddStatement')[0];
        var $visitBalanceStatements = $scope.find('#ddVisitBalanceSummary')[0];

        if ($fpStatements) {
            ko.applyBindings(statementModel, $fpStatements);
        }
        if ($visitBalanceStatements) {
            ko.applyBindings(visitBalanceSummaryModel, $visitBalanceStatements);
        }

    };

    statements.Setup = function (currentVisitPayUserId, filteredVisitPayUserId) {
        var promise = $.Deferred();
        visitPayUserId = filteredVisitPayUserId || currentVisitPayUserId;

        $.get('/Search/Statements?guarantorVisitPayUserId=' + visitPayUserId).then(function (html) {

            // need to reload because guarantor specific (list of statements)
            $scope = $('#section-statements');
            $scope.html(html);
            ko.cleanNode($scope[0]);

            init();

            hasInitialized = false;

            promise.resolve();

        });

        return promise;
    };
    statements.OnDataLoaded = function () { };
    statements.GetInitialCount = function () {

        //
        var deferred = $.Deferred();

        //
        $.post('/Search/StatementTotals', { guarantorVisitPayUserId: visitPayUserId }, function (data) {
            setRecordCount(data);
            deferred.resolve();
        }, 'json');

        //
        return deferred;

    };
    statements.Initialize = function () {

        if (hasInitialized)
            return;
        else {
            hasInitialized = true;
        }

        setRecordCount($scope.find('#ddStatement li:not(.archived-separator)').length);

    };

    return statements;

})(window.VisitPay.Common, window.jQuery, window.ko);
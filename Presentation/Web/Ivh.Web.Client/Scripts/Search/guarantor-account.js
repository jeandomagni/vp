﻿function validateGuarantorCancellation () {
    var reasonId = $('#vpgCancelReason').val();
    var note = $('#vpgCancelNote').val();
    var reasonOtherId = 5;
    var reasonNoneId = 0;
    var $modal = $('#modalGenericConfirmation');
    var submitButton = $modal.find('.modal-footer button[type=submit]');
    if (reasonId == reasonNoneId) {
        submitButton.prop('disabled', true);
        $('#vpgCancelNote').removeClass('input-validation-error');
    }
    else if(reasonId == reasonOtherId && !note)
    {
        submitButton.prop('disabled', true);
        $('#vpgCancelNote').addClass('input-validation-error');
    }
    else 
    {
        submitButton.prop('disabled', false);
        $('#vpgCancelNote').removeClass('input-validation-error');
    }
}

(function (guarantorAccountFinancePlans,
    guarantorAccountVisits,
    guarantorAccountTransactions,
    guarantorAccountSupportHistory,
    guarantorAccountChatHistory,
    guarantorAccountCommunications,
    guarantorAccountStatements,
    guarantorAccountItemizations,
    guarantorConsolidation,
    guarantorUnmatch,
    changePaymentDueDay,
    editAccountSettings,
    paymentDueDayHistory,
    paymentsPending,
    paymentHistory,
    common,
    clientSettings,
    guarantorFilter,
    manageSso,
    $) {

    var reactivateaccount = function (vpGuarantorId) {
        if (vpGuarantorId) {
            $.blockUI();

            $.get('/Search/ConfirmReactivation', function (cmsVersion) {

                $.unblockUI();
                common.ModalGenericConfirmationMd(cmsVersion.ContentTitle, cmsVersion.ContentBody,
                'Submit',
                'Exit').done(function () {
                    $.blockUI();
                    $.post('/Search/GuarantorReactivate/', { guarantorId: vpGuarantorId }, function (result) {
                        /* result contains message from server including success and failure.*/
                        $.unblockUI();
                        location.reload();
                    });
                });
            });
        }
    }

    var cancelAccount = function (vpGuarantorId) {
        if (vpGuarantorId) {
            $.get('/Search/GuarantorCancelAccount/', null, function (html) {
                common.ModalGenericConfirmationMd(
                    'Cancel Account',
                    html,
                    'Submit',
                    'Exit').done(function () {
                            $.blockUI();
                            var reasonId = $('#vpgCancelReason').val();
                            var note = $('#vpgCancelNote').val();
                            $.post('/Search/GuarantorCancel/', { guarantorId: vpGuarantorId, reasonId: reasonId, note: note }, function (result) {
                                /* result contains message from server including success and failure.*/
                                $.unblockUI();
                                location.reload();
                            });
                    });
                    validateGuarantorCancellation();
            });
        }
    }
    
    var unmatch = function (vpGuarantorId) {
        guarantorUnmatch.OpenSelectModal(vpGuarantorId);
        $(document).off('guarantorUnmatchActionComplete').on('guarantorUnmatchActionComplete', function (result) {
            window.location.reload();
        });
    };

    var emulateUser = function (vpGuarantorId) {
        if (vpGuarantorId) {
            $.post('/Search/BeginEmulateUser', { VpGuarantorId: vpGuarantorId }, function (url) {
                window.location.href = url;
            });
        }
    }

    var loadAccountSettings = function (vpGuarantorId) {
        if (vpGuarantorId) {
            $.blockUI();

            $.get('/Search/GuarantorEditAccountSettings/', { vpGuarantorId: vpGuarantorId }, function (html) {

                $('#modalEditAccountSettingsContainer').html(html);
                $('#modalEditAccountSettingsContainer').find('.modal').modal();
                setUiComponents();
                editAccountSettings.Initialize(vpGuarantorId);
                $.unblockUI();
            });
        }
    }

    var loadChangePaymentDueDay = function (vpGuarantorId) {
        var paymentDueDate = $('#PaymentDueDateDisplay').val();
        var gracePeriod = $('#GracePeriod').val();
        if (vpGuarantorId && paymentDueDate) {
            $.blockUI();

            $.get('/Search/ChangePaymentDueDay/', {}, function (html) {

                $('#modalChangePaymentDueDayContainer').html(html);
                $('#modalChangePaymentDueDayContainer').find('.modal').modal();
                setUiComponents();
                changePaymentDueDay.Initialize(vpGuarantorId, paymentDueDate, gracePeriod);

                $.unblockUI();
            });
        }
    }

    var loadSecurityQuestions = function (vpUserId, vpGuarantorId) {
        if (vpUserId) {
            $.blockUI();

            $.post('/Search/ViewSecurityQuestions/', { vpUserId: vpUserId, vpGuarantorId: vpGuarantorId }, function (html) {

                $('#modalViewSecurityQuestionsContainer').html(html);
                $('#modalViewSecurityQuestionsContainer').find('.modal').modal();

                $.unblockUI();

            });
        }
    }

    var loadManageSso = function(visitPayUserId) {
        $.blockUI();
        common.ModalNoContainerAsync('modalManageSso', '/Sso/Settings', 'POST', { visitPayUserId: visitPayUserId }, true).done(function () {
            manageSso.Initialize();
            $.unblockUI();
        });
    }
    
    var disableEnableAccountPost = function (vpGuarantorId, disable) {
        $.blockUI();
        $.post('/Search/GuarantorDisable',
            { guarantorId: vpGuarantorId, disable: disable },
            function (result) {
                window.location.reload();
            });
    }

    var disableEnableAccount = function (vpGuarantorId, disable) {
        if (disable) {
            common.ModalGenericConfirmationMd(
                    'Disable Account',
                    '<div class="disable-account"><p>' +
                    "You have selected to disable this Guarantor's account." +
                    '<br/>' +
                    '<br/>' +
                    'Upon submission of this request, the Guarantor will no longer be able to login.' +
                    '<br/>' +
                    'Your view/action on this content is logged for audit purposes.' +
                    '</p></div>',
                    'Submit',
                    'Exit')
                .done(function() {
                    disableEnableAccountPost(vpGuarantorId, disable);
                });
        } else {
            common.ModalGenericConfirmationMd(
                    'Enable Account',
                    '<div class="enable-account"><p>' +
                    "You have selected to enable this Guarantor's account." +
                    '<br/>' +
                    '<br/>' +
                    'Upon submission of this request, the Guarantor will be able to login.' +
                    '<br/>' +
                    'Your view/action on this content is logged for audit purposes.' +
                    '</p></div>',
                    'Submit',
                    'Exit')
                .done(function () {
                    disableEnableAccountPost(vpGuarantorId, disable);
                });
        }
    }
    
    var loadPaymentDueDayHistory = function (vpGuarantorId) {
        $.blockUI();

        $.get('/Search/GetPaymentDueDayHistory/', {}, function (html) {

            $('#modalPaymentDueDayHistoryContainer').html(html);
            $('#modalPaymentDueDayHistoryContainer').find('.modal').modal();
            setUiComponents();
            paymentDueDayHistory.Initialize(vpGuarantorId);

            $.unblockUI();
        });
    }

    var makePayment = function (vpGuarantorId, currentVisitPayUserId) {
        window.location.href = '/Payment/ArrangePayment?VpGuarantorId=' + vpGuarantorId + '&currentVisitPayUserId=' + currentVisitPayUserId;
    }

    var requestPassword = function (vpGuarantorId) {
        if (VpGuarantorId) {
            common.ModalGenericConfirmationMd(
                'Request Password',
                '<div class="request-password"><p>' +
                'Send a new, temporary password for this ' + clientSettings.ClientBrandName + ' account<br />to the notification email address on file.' +
                '<br />' +
                '<br />' +
                'The temporary password will expire after ' + clientSettings.GuarantorTempPasswordExpLimitInHours + ' hours.' +
                '</p></div>',
                'Submit',
                'Cancel',
                'Your action is logged for audit purposes.').done(function () {
                    $.blockUI();

                    $.post('/Search/RequestPassword/', { guarantorId: vpGuarantorId }, function (result) {
                        /* result contains message from server including success and failure.*/
                        $.unblockUI();
                    });
                });
        }
    }

    var phoneTextSettings = function (vpGuarantorId) {
        window.location.href = '/Guarantor/PhoneTextSettings?VpGuarantorId=' + vpGuarantorId;
    };

    var resendTemporaryLoginLink = function (vpGuarantorId) {
        var sendToken = new window.VisitPay.SendToken();
        sendToken.init(vpGuarantorId);
    };
    
    var initTab = function ($element, visitPayUserId, visitPayUserGuid) { // this is the VisitPayUserId of the account, not the filter.
        // tabs that will not support the consolidation filter can continue to work as-is, and need this id.
        // tabs that support the consolidation filter should be handled in the "Setup" of initTabs(), and simply Initialize() here, without any parameters.

        $element.tab('show');

        // need to enable or disable consolidation filter here, based on tab
        switch ($element.attr('href').replace('#', '')) {
            case 'tabVisits':
                guarantorFilter.Enable();
                guarantorAccountVisits.Initialize();
                break;
            case 'tabPayments':
                guarantorFilter.Enable();
                paymentsPending.Initialize();
                paymentHistory.Initialize();
                break;
            case 'tabFinancePlans':
                guarantorFilter.Enable();
                guarantorAccountFinancePlans.Initialize();
                break;
            /*case 'tabTransactions':
                guarantorFilter.Enable();
                guarantorAccountTransactions.Initialize();
                break;*/
            case 'tabSupportHistory':
                guarantorFilter.Disable();
                guarantorAccountSupportHistory.Initialize(visitPayUserId);
                break;
            case 'tabChatHistory':
                guarantorFilter.Disable();
                guarantorAccountChatHistory.Initialize(visitPayUserGuid);
                break;
            case 'tabStatements':
                guarantorFilter.Enable();
                guarantorAccountStatements.Initialize();
                break;
            case 'tabItemizations':
                guarantorFilter.Enable();
                guarantorAccountItemizations.Initialize();
                break;
            case 'tabCommunications':
                guarantorFilter.Disable();
                guarantorAccountCommunications.Initialize(visitPayUserId);
                break;
            case 'tabConsolidation':
                guarantorFilter.Disable();
                guarantorConsolidation.Initialize(visitPayUserId);
        };

    };

    var initTabs = function (selectedTab) {

        //
        var visitPayUserId = $('#section-guarantoraccount #UserVisitPayUserId').val();
        var visitPayUserGuid = $('#section-guarantoraccount #UserVisitPayUserGuid').val();
        var filteredVisitPayUserId = guarantorFilter.FilterValue.FilteredVisitPayUserId;
        var vpGuarantorId = guarantorFilter.FilterValue.FilteredVpGuarantorId || $('#section-guarantoraccount #VpGuarantorId').val();
        var vpGuarantorTypeId = $('#section-guarantoraccount #VpGuarantorTypeId').val();

        //
        $('#section-guarantordata .nav-tabs a').off('click').on('click', function(e) {
            e.preventDefault();
            initTab($(this), visitPayUserId, visitPayUserGuid);
        });

        //
        var tabVisits = $('#section-guarantordata #liTabVisits');
        var tabFinancePlans = $('#section-guarantordata #liTabFinancePlans');
        //var tabTransactions = $('#section-guarantordata #liTabTransactions');
        var tabSupportHistory = $('#section-guarantordata #liTabSupportHistory');
        var tabStatements = $('#section-guarantordata #liTabStatements');
        var tabItemizations = $('#section-guarantordata #liTabItemizations');
        var tabCommunications = $('#section-guarantordata #liTabCommunications');

        // setup for tabs supporting consolidation
        var p1 = $.Deferred();
        var p2 = $.Deferred();
        //var p3 = $.Deferred();
        var p4 = $.Deferred();

        // the filters for these tabs need to reload
        guarantorAccountVisits.Setup(visitPayUserId, filteredVisitPayUserId).done(function () { p1.resolve(); });
        guarantorAccountStatements.Setup(visitPayUserId, filteredVisitPayUserId).done(function () { p2.resolve(); });
        //guarantorAccountTransactions.Setup(visitPayUserId, filteredVisitPayUserId).done(function () { p3.resolve(); });
        if (tabItemizations.length > 0) {
            guarantorAccountItemizations.Setup(visitPayUserId, filteredVisitPayUserId).done(function() { p4.resolve(); });
        } else {
            p4.resolve();
        }

        // see if account closed
        var accountClosed = $('#AccountClosedDate').val();
        // these tabs are "normal"
        guarantorAccountFinancePlans.Setup(visitPayUserId, filteredVisitPayUserId, vpGuarantorId, vpGuarantorTypeId);
        paymentsPending.Setup(visitPayUserId, filteredVisitPayUserId, vpGuarantorId, accountClosed);
        paymentHistory.Setup(visitPayUserId, filteredVisitPayUserId, vpGuarantorId, accountClosed);

        $.when(p1, p2, /*p3,*/ p4).done(function() {

            // initialize
            // tabs that don't support the consolidation filter need the visitPayUserId parameter, others do not (handled in setup, above)
            if (selectedTab.length > 0)
                initTab(selectedTab, visitPayUserId, visitPayUserGuid);
        });

    };

    $(document).ready(function () {

        //payment method tooltip (because it's using an html element for the content)
        $('#aPaymentMethod').tooltip({
            placement: 'left',
            html: true,
            title: function () {
                return $('#tooltip-paymentMethods').html();
            }
        });

        $('.account-actions').on('change', function () {

            var vpGuarantorId = parseInt($('#section-guarantoraccount #VpGuarantorId').val());
            var visitPayUserId = parseInt($('#section-guarantoraccount #UserVisitPayUserId').val());

            if (this.value === 'reactivateaccount') {
                reactivateaccount(vpGuarantorId);
            } else if (this.value === 'cancelaccount') {
                cancelAccount(vpGuarantorId);
            } else if (this.value === 'unmatch') {
                unmatch(vpGuarantorId);
            } else if (this.value === 'emulate') {
                emulateUser(vpGuarantorId);
            } else if (this.value === 'payment') {
                makePayment(vpGuarantorId, visitPayUserId);
            } else if (this.value === 'accountsettings') {
                loadAccountSettings(vpGuarantorId);
            } else if (this.value === 'duedate') {
                loadChangePaymentDueDay(vpGuarantorId);
            } else if (this.value === 'requestPassword') {
                requestPassword(vpGuarantorId);
            } else if (this.value === 'viewsecurityquestions') {
                loadSecurityQuestions(visitPayUserId, vpGuarantorId);
            } else if (this.value === 'managesso') {
                loadManageSso(visitPayUserId);
            } else if (this.value === 'enableAccount') {
                disableEnableAccount(vpGuarantorId, false);
            } else if (this.value === 'disableAccount') {
                disableEnableAccount(vpGuarantorId, true);
            } else if (this.value === 'phoneTextSettings') {
                phoneTextSettings(vpGuarantorId);
            } else if (this.value === 'offlinefp') {
                window.location.href = '/Offline/CreateFinancePlan/?vpGuarantorId=' + vpGuarantorId;
            } else if (this.value === 'paymentMethods') {
                window.location.href = '/Payment/PaymentMethods?currentVisitPayUserId=' + visitPayUserId;
            } else if (this.value === 'resendtemporaryloginlink') {
                resendTemporaryLoginLink(vpGuarantorId);
            }
            if (this.value != 'none') {
                this.value = 'none';
            }
        });

        $('#paymentDueDayChanges').click(function () {
            loadPaymentDueDayHistory(parseInt($('#section-guarantoraccount #VpGuarantorId').val()));
        });

        $('#aConsolidationStatus').on('click', function(e) {
            e.preventDefault();
            initTab($('#liTabConsolidation').children('a'), $('#UserVisitPayUserId').val());
        });

        // alerts
        $.get('/Search/GuarantorAlerts/' + parseInt($('#section-guarantoraccount #VpGuarantorId').val()), function(html) {
            $('.action-required').replaceWith(html);

            $('#section-guarantoraccount').on('click', 'a.action-required', function (e) {
                e.preventDefault();
                common.ModalContainerAsync($('#modalAlertsContainer'), '/Search/GuarantorAlertsModal', 'GET', { id: parseInt($('#section-guarantoraccount #VpGuarantorId').val()) }, true).then(function() {
                    $('#modalAlertsContainer').find('a').each(function() {
                        $(this).replaceWith($(this).html());
                    });
                });
            });
        });

        //tabs
        if ($('#section-guarantordata .nav-tabs > li').length > 0) {

            // show first tab or tab from hash, lazy load others
            var selectedTab = window.location.hash.length > 0 ? $('#section-guarantordata .nav-tabs a[href$="' + window.location.hash + '"]:eq(0)') : $('#section-guarantordata .nav-tabs a:eq(0)');
            initTabs(selectedTab);

        } else {

            $('#section-guarantordata .nav-tabs').hide();

        }

        // consolidation filter
        guarantorFilter.OnFilterChanged = function() {

            var $activeTab = $('#section-guarantordata .nav-tabs li.active a:eq(0)');
            var selectedTab = $activeTab.length > 0 ? $activeTab : $('#section-guarantordata .nav-tabs a:eq(0)');
            initTabs(selectedTab);

        };

    });

})(window.VisitPay.GuarantorAccountFinancePlans,
    window.VisitPay.GuarantorAccountVisits,
    window.VisitPay.GuarantorAccountTransactions,
    window.VisitPay.GuarantorAccountSupportHistory,
    window.VisitPay.GuarantorAccountChatHistory,
    window.VisitPay.GuarantorAccountCommunications,
    window.VisitPay.GuarantorAccountStatements,
    window.VisitPay.GuarantorAccountItemizations,
    window.VisitPay.GuarantorConsolidation,
    window.VisitPay.GuarantorUnmatch,
    window.VisitPay.ChangePaymentDueDay,
    window.VisitPay.EditAccountSettings,
    window.VisitPay.PaymentDueDayHistory,
    window.VisitPay.PaymentsPending,
    window.VisitPay.PaymentHistory,
    window.VisitPay.Common,
    window.VisitPay.ClientSettings,
    window.VisitPay.GuarantorFilter,
    window.VisitPay.ManageSso,
    jQuery);

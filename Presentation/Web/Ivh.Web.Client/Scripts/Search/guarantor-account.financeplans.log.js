﻿window.VisitPay.FinancePlanLog = (function (common, $) {

    window.VisitPay.Grids.FinancePlanLog = (function ($gridElement, $formElement, filterModel, handlers) {

        var gridOpts = {
            autowidth: true,
            url: '/FinancePlan/FinancePlanLog',
            sortname: 'InsertDate',
            sortorder: 'desc',
            colModel: [
                { label: 'Date', name: 'InsertDate', width: 140, resizable: false },
                {
                    label: 'Source System Key', name: 'SourceSystemKey', width: 170, sortable: false, resizable: false,
                    formatter: function(cellValue, options, rowObject) {
                        return rowObject.SourceSystemKey + ' (' + rowObject.BillingApplication + ')';
                    }
                },
                { label: 'Amount', name: 'Amount', width: 75, sortable: false, resizable: false },
                { label: 'Balance At Time Of Assessement', name: 'VisitBalanceAtTimeOfAssessement', width: 200, sortable: false, resizable: false },
                { label: 'Interest Rate', name: 'InterestRateUsedForAssessement', width: 100, sortable: false, resizable: false },
                { label: 'Event', name: 'FinancePlanLogType', width: 183, sortable: false, resizable: false }
            ]
        };

        grid =  new window.VisitPay.Grids.BaseGrid($gridElement, filterModel, $formElement, gridOpts, { noRecordsText: 'There are no finance plan log records to display at this time.' }, { loadComplete: handlers.loadComplete });
        $formElement.submit(function (e) {
            e.preventDefault();
            $.blockUI();
            grid.reload();
        });

        $formElement.on('click', '#btnGridFilterFinanceLogReset', 'click', function () {
            var sourceSystemKeySelect = $('select#sourceSystemKeySelect');
            sourceSystemKeySelect.find('option').prop('selected', false);
            sourceSystemKeySelect.multiselect('refresh');
            
            grid.reset();
        });
        return grid;
    });

    function loadModal(visitPayUserId, financePlanId) {

        var promise = $.Deferred();
       
        var data = { visitPayUserId: visitPayUserId, financePlanId: financePlanId };
        common.ModalNoContainerAsync('modalFinancePlanLog', '/FinancePlan/FinancePlanLog', 'GET', data, false).done(function ($modal) {

            promise.resolve($modal);

        });

        return promise;
    }

    var ViewModel = function () {

        var self = this;

        self.Filter = {
            VisitPayUserId: ko.observable(),
            FinancePlanId: ko.observable(),
            SourceSystemKeys: ko.observable()
        };

        self.RunResults = ko.observable(false);
        self.ChangeNotifications = function () {

            var setRun = function () {
                self.RunResults(true);
            };

            self.Filter.SourceSystemKeys.subscribe(setRun);

        };

        return self;

    }; 
    var vm;
    var grid;

    function bindGrid(visitPayUserId, financePlanId) {

        vm = new ViewModel();
        vm.Filter.VisitPayUserId = visitPayUserId;
        vm.Filter.FinancePlanId = financePlanId;
        var sourceSystemKeySelect = $('select#sourceSystemKeySelect');

        sourceSystemKeySelect.multiselect({
            enableClickableOptGroups: true,
            includeSelectAllOption: true,
            allSelectedText: 'All Visits',
            nonSelectedText: 'All Visits',
            nSelectedText: ' Visits',
            numberDisplayed: 1,
            selectAllNumber: false,
            selectAllText: 'All Visits',
            onChange: function () {

                vm.Filter.SourceSystemKeys = new ko.observableArray([]);

                sourceSystemKeySelect.find('option:selected').each(function () {
                    vm.Filter.SourceSystemKeys.push($(this).val());
                });

                vm.RunResults(true);

            }
        });
        ko.applyBindings(vm, $('#jqGridFilterFinancePlanLog')[0]);
        vm.ChangeNotifications();

        return new window.VisitPay.Grids.FinancePlanLog($('#jqGridFinancePlanLog'), $('#jqGridFilterFinancePlanLog'), vm.Filter, {
            loadComplete: function() {
                $.unblockUI();
            }
        });
    };
    
    return {
        Initialize: function (visitPayUserId, financePlanId) {
            
            $.blockUI();

            loadModal(visitPayUserId, financePlanId).done(function($modal) {

                bindGrid(visitPayUserId, financePlanId);
                $.unblockUI();
                $modal.modal('show');

            });

        }
    };

})(window.VisitPay.Common = window.VisitPay.Common || {}, jQuery);
﻿window.VisitPay.VisitBalances = (function(common, $) {

    function loadModal() {

        var promise = $.Deferred();

        common.ModalNoContainerAsync('modalVisitBalances', '/Search/GuarantorVisitBalances', 'GET', null, false).done(function ($modal) {

            promise.resolve($modal);

        });

        return promise;
    }
    
    return {
        Initialize: function(visitId, unclearedBalance, interestDue, unclearedPaymentsSum, currentBalance) {

            var model = {
                UnclearedBalance: ko.observable(unclearedBalance),
                InterestDue: ko.observable(interestDue),
                UnclearedPaymentsSum: ko.observable(unclearedPaymentsSum),
                CurrentBalance: ko.observable(currentBalance),
                VisitId: ko.observable(visitId)
            };

            var hasChanged = false;
            var promise = $.Deferred();

            $.blockUI();
            loadModal().done(function($modal) {

                //
                $modal.on('hide.bs.modal.aging', function () { promise.resolve(hasChanged); });

                //
                ko.applyBindings(model, $modal[0]);

                //
                $.unblockUI();
                $modal.modal('show');

            });
            return promise;
        }
    };

})(window.VisitPay.Common = window.VisitPay.Common || {}, jQuery);
﻿window.VisitPay.GuarantorAccountFinancePlanVisits = (function (common, $, ko) {

    var viewModel = function (rowData) {

        var self = this;

        self.CurrentFinancePlanBalance = ko.observable(rowData.CurrentFinancePlanBalance);
        self.FinancePlanStatusFullDisplayName = ko.observable(rowData.FinancePlanStatusFullDisplayName);
        self.IsPastDue = ko.observable(rowData.IsPastDue || rowData.IsUncollectable);
        self.IsPending = ko.observable(rowData.IsPending);
        self.OriginatedFinancePlanBalance = ko.observable(rowData.OriginatedFinancePlanBalance);
        self.OriginationDate = ko.observable(rowData.OriginationDate);
        self.PaymentAmount = ko.observable(rowData.PaymentAmount);
        self.Terms = ko.observable(rowData.Terms);

    };

    var guarantorAccountFinancePlanVisits = {};

    guarantorAccountFinancePlanVisits.Initialize = function (visitPayUserId, rowData) {

        //
        var scope = $('#modalFinancePlanVisits');
        scope.find('.modal').css('position', 'absolute');

        //
        var model = new viewModel(rowData);
        ko.applyBindings(model, scope[0]);

        //
        var grid = scope.find('#jqGridFinancePlanVisits');
        var gridDefaultSortName = 'DischargeDate';
        var gridDefaultSortOrder = 'asc';

        //
        $('#btnFinancePlanVisitsExport').on('click', function (e) {

            e.preventDefault();
            $.post('/Search/GuarantorVisitsExport', {
                FinancePlanId: rowData.FinancePlanId,
                VisitPayUserId: visitPayUserId,
                sidx: grid.jqGrid('getGridParam', 'sortname'),
                sord: grid.jqGrid('getGridParam', 'sortorder')
            }, function (result) {
                if (result.Result === true)
                    window.location.href = result.Message;
            }, 'json');

        });

        //
        grid.jqGrid({
            url: '/Search/GuarantorVisits',
            postData: {
                FinancePlanId: rowData.FinancePlanId,
                VisitPayUserId: visitPayUserId
            },
            jsonReader: { root: 'Visits' },
            pager: 'jqGridFinancePlanVisitsPager',
            sortname: gridDefaultSortName,
            sortorder: gridDefaultSortOrder,
            height: 1200, // overridden in css, this just makes the scrollbar appear correctly
            loadComplete: function (data) {

                var gridContainer = grid.parents('.ui-jqgrid:eq(0)');
                var gridNoRecords = $('#jqGridFinancePlanVisitsNoRecords');

                if (data.Visits != null && data.Visits.length > 0) {

                    gridContainer.show();
                    gridNoRecords.removeClass('in');

                    grid.closest('.ui-jqgrid-bdiv').scrollTop(0);

                } else {

                    gridContainer.hide();
                    gridNoRecords.addClass('in');

                    grid.closest('.ui-jqgrid-bdiv').scrollTop(0);
                }

                $.unblockUI();

            },
            gridComplete: function () {

                grid.find('[data-toggle="tooltip"]').tooltip({ animation: false, container: 'body' });

                $('#jqGridFinancePlanVisitsPager').find('#input_jqGridPager').find('input').off('change').on('change', function () {
                    grid.jqGrid().trigger('reloadGrid', [{ page: $(this).val() }]);
                });

                var gridContainer = grid.parents('.ui-jqgrid:eq(0)');
                gridContainer.addClass('visible');

            },
            colModel: [
                { label: 'Visit #', name: 'SourceSystemKeyDisplay', width: 122, resizable: false },
                { label: 'Visit Date', name: 'DischargeDate', width: 114, resizable: false },
                { label: 'Patient Name', name: 'PatientName', width: 180, resizable: false },
                { label: 'Status', name: 'VisitStateFullDisplayName', width: 298, resizable: false, formatter: function (cellValue, options, rowObject) { return (rowObject.IsPastDue) ? '<span style="color: red;" class="visitstatus3">' + cellValue + '</span>' : cellValue; } },
                { label: 'Current Balance', name: 'UnclearedBalance', width: 150, resizable: false, sortable: false, classes: 'text-right' }
            ]
        });

    };

    return guarantorAccountFinancePlanVisits;

})(window.VisitPay.Common = window.VisitPay.Common || {},
    jQuery,
    window.ko);
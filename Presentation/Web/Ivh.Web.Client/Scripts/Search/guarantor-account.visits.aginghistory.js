﻿window.VisitPay.VisitAgingHistory = (function(common, $) {

    function loadData(model, visitPayUserId, visitId) {

        var promise = $.Deferred();

        $.post('/Search/GuarantorVisitAgingHistory', { visitPayUserId: visitPayUserId, visitId: visitId }, function(data) {
            ko.mapping.fromJS(data, {}, model);
            promise.resolve();

        });

        return promise;

    };

    function loadModal() {

        var promise = $.Deferred();

        common.ModalNoContainerAsync('modalVisitAgingHistory', '/Search/GuarantorVisitAgingHistory', 'GET', null, false).done(function ($modal) {

            promise.resolve($modal);

        });

        return promise;
    }

    function bindGrid(model, $modal) {

        //
        var grid = $modal.find('#jqGridVisitAgingHistory');
        var gridDefaultSortName = 'InsertDate';
        var gridDefaultSortOrder = 'desc';

        //
        grid.jqGrid({
            url: '/Search/GuarantorVisitAgingHistoryData',
            jsonReader: { root: 'VisitAgingHistory' },
            postData: { model: { VisitId: ko.unwrap(model.VisitId), VisitPayUserId: ko.unwrap(model.VisitPayUserId) } },
            pager: 'jqGridVisitAgingHistoryPager',
            sortname: gridDefaultSortName,
            sortorder: gridDefaultSortOrder,
            loadComplete: function (data) {

                var gridContainer = grid.parents('.ui-jqgrid:eq(0)');
                var gridNoRecords = $modal.find('#jqGridVisitAgingHistoryNoRecords');

                if (data.VisitAgingHistory != null && data.VisitAgingHistory.length > 0) {

                    gridContainer.show();
                    gridNoRecords.removeClass('in');

                } else {

                    gridContainer.hide();
                    gridNoRecords.addClass('in');

                }

                $.unblockUI();
            },
            gridComplete: function () {

                $modal.find('#input_jqGridPager').find('input').off('change').on('change', function() {
                    grid.jqGrid().trigger('reloadGrid', [{ page: $(this).val() }]);
                });
                var gridContainer = grid.parents('.ui-jqgrid:eq(0)');
                gridContainer.addClass('visible');

            },
            colModel: [
                { label: 'Date Changed', name: 'InsertDate', width: 250, resizable: false, sortable: false },
                { label: 'Aging Tier', name: 'AgingTier', width: 400, resizable: false, key: true, sortable: false },
                { label: 'Changed By', name: 'VisitPayUserName', width: 215, resizable: false, key: true, sortable: false }
            ]
        });

        return grid;

    };

    function post(model) {

        var promise = $.Deferred();

        $.blockUI();

        var postModel = ko.mapping.toJS(model);
        postModel.ActiveUncollectableContentBody = null;
        postModel.ActiveUncollectableContentTitle = null;

        $.post('/Search/GuarantorVisitAgingHistoryUpdate', { model: postModel }, function (result) {

            if (!result.Result) {
                promise.reject();
                $.unblockUI();
                return;
            }

            loadData(model, ko.unwrap(model.VisitPayUserId), ko.unwrap(model.VisitId)).done(function () {

                loadModal();

                setTimeout(function () {

                    $.unblockUI();
                    promise.resolve();
                }, 10000);

            });

        });

        return promise;

    }

    function submit($form, model) {

        var promise = $.Deferred();

        if (!$form.valid()) {
            promise.reject();
            return promise;
        }

        if (parseInt(ko.unwrap(model.NewAgingCount)) === parseInt(ko.unwrap(model.ActiveUncollectableAgingCount))) {

            common.ModalGenericConfirmationMd(ko.unwrap(model.ActiveUncollectableContentTitle), ko.unwrap(model.ActiveUncollectableContentBody), 'Confirm', 'Cancel').then(function () {

                post(model).done(function () {
                    promise.resolve();
                });

            }, function () {

                model.ChangeDescription('');

            });

        } else {

            post(model).done(function () {
                promise.resolve();
            });

        }

        return promise;

    }


    return {
        Initialize: function(visitPayUserId, visitId) {

            var model = {
                ChangeDescription: ko.observable(),
                CurrentAgingCount: ko.observable(),
                SourceSystemKeyDisplay: ko.observable(),
                VisitId: ko.observable(),
                VisitPayUserId: ko.observable(),
                AvailableAgingTiers: ko.observableArray().extend({ extendedArray: {} })
            };

            var hasChanged = false;
            var promise = $.Deferred();

            $.blockUI();
            loadData(model, visitPayUserId, visitId).done(function() {

                loadModal().done(function($modal) {

                    //
                    $modal.on('hide.bs.modal.aging', function () { promise.resolve(hasChanged); });

                    //
                    ko.applyBindings(model, $modal[0]);

                    //
                    var grid = bindGrid(model, $modal);

                    //
                    var $form = $modal.find('form');
                    $form.parseValidation();
                    $form.off('submit').on('submit', function (e) {

                        e.preventDefault();
                        submit($(this), model).done(function () {
                            
                            hasChanged = true;
                            grid.trigger('reloadGrid');

                        });

                    });

                    //
                    $.unblockUI();
                    $modal.modal('show');

                });

            });

            return promise;

        }
    };

})(window.VisitPay.Common = window.VisitPay.Common || {}, jQuery);
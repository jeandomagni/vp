﻿window.VisitPay.ChangePaymentDueDay = (function (common, $, ko) {

    var viewModel = function (guarantorId, paymentDueDate, gracePeriod) {

        var self = this;

        self.GuarantorId = ko.observable(guarantorId);
        self.PaymentDueDate = ko.observable(paymentDueDate);
        self.GracePeriod = ko.observable(gracePeriod);
        self.PaymentDueDay = ko.observable();
    };

    

    var changePaymentDueDay = {};

    changePaymentDueDay.Initialize = function (guarantorId, paymentDueDate, gracePeriod) {
        
        //
        var scope = $('#modalChangePaymentDueDay');
        scope.find('.modal').css('position', 'absolute');

        //
        var form = scope.find('#frmChangePaymentDueDay');
        common.ResetUnobtrusiveValidation(form);

        //
        var model = new viewModel(guarantorId, paymentDueDate, gracePeriod);
        ko.applyBindings(model, scope[0]);

        var submit = function () {
            if (form.valid()) {
                common.ResetUnobtrusiveValidation(form);
                $.post('/Search/ChangePaymentDueDay', { guarantorId: model.GuarantorId(), dueDay: model.PaymentDueDay() }).done(function() {
                    $('#modalChangePaymentDueDayContainer').find('.modal').modal('hide');
                    location.reload();
                });
            }
        };

        scope.on('click', '#btnChangePaymentDueDateSubmit', 'click', function (e) {
            e.preventDefault();
            submit();
        });

    };

    return changePaymentDueDay;

})(window.VisitPay.Common = window.VisitPay.Common || {},
    jQuery,
    window.ko);
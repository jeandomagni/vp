﻿window.VisitPay.GuarantorAccountSupportHistory = (function (supportRequestAction, supportRequest, createSupportRequest, common, $, ko) {

    var guarantorAccountSupportHistory = {};

    var filterViewModel = function () {

        var self = this;

        self.SupportRequestStatus = ko.observable('');
        self.DateRangeFrom = ko.observable();
        self.VisitPayUserId = ko.observable();

        self.RunResults = ko.observable(false);
        self.ChangeNotifications = function () {

            var setRun = function () {
                self.RunResults(true);
            };

            self.SupportRequestStatus.subscribe(setRun);
            self.DateRangeFrom.subscribe(setRun);

        };
    };

    var model;
    var $scope;
    var initialDateRange;
    
    var grid;
    var gridDefaultSortName = 'InsertDate';
    var gridDefaultSortOrder = 'desc';

    var bindFilters = function() {

        initialDateRange = $('#GuarantorSupportHistoryFilter_DateRangeFrom').val();

    };
    var refreshGrid = function() {
        if (grid) {
            $.blockUI();
            grid.sortGrid(grid.jqGrid('getGridParam', 'sortname'), true, grid.jqGrid('getGridParam', 'sortorder'));
        }

    };
    var reloadGrid = function () {
        var postModel = ko.toJS(model);
        $.extend(grid.jqGrid('getGridParam', 'postData'), { model: postModel });
        grid.setGridParam({ page: 1 });
        grid.sortGrid(gridDefaultSortName, true, gridDefaultSortOrder);
    };
    var resetFilters = function() {
        model.SupportRequestStatus('');
        model.DateRangeFrom(initialDateRange);
        model.RunResults(false);
    };
    var setRecordCount = function (count) {
        guarantorAccountSupportHistory.OnDataLoaded(count);
    };

    var bindGrid = function($scope) {

        var deferred = $.Deferred();

        //
        grid = $scope.find('#jqGridSupportRequests');

        //
        grid.jqGrid({
            url: '/Search/GuarantorSupportHistory',
            postData: { model: ko.toJS(model) },
            jsonReader: { root: 'SupportRequests' },
            autowidth: false,
            pager: 'jqGridSupportRequestsPager',
            sortname: gridDefaultSortName,
            sortorder: gridDefaultSortOrder,
            loadComplete: function(data) {

                var gridContainer = grid.parents('.ui-jqgrid:eq(0)');
                var gridNoRecords = $scope.find('#jqGridSupportRequestsNoRecords');

                if (data.SupportRequests != null && data.SupportRequests.length > 0) {

                    gridContainer.show();
                    gridNoRecords.removeClass('in');
                    grid.closest('.ui-jqgrid-bdiv').scrollTop(0);

                    grid.find('.jqgrow').each(function () {

                        var rowData = data.SupportRequests[$(this).index() - 1];

                        $(this).find('a:eq(0)').on('click', function(e) {

                            e.preventDefault();
                            supportRequest.Initialize(rowData.SupportRequestId, rowData.VisitPayUserId, rowData.VpGuarantorId, rowData.IsAccountClosed);

                        });

                        $(this).find('input[type=checkbox]').on('change', function(e) {

                            e.preventDefault();

                            $.blockUI();
                            supportRequestAction.Initialize($('#modalSupportRequestActionContainer'),
                                rowData.SupportRequestStatusId,
                                rowData.SupportRequestId,
                                rowData.SupportRequestDisplayId,
                                rowData.VisitPayUserId);

                            if ($(this).is(':checked')) {
                                $(this).prop('checked', false);
                            } else {
                                $(this).prop('checked', true);
                            }

                        });
                    });

                } else {

                    gridContainer.hide();
                    gridNoRecords.addClass('in');
                    grid.closest('.ui-jqgrid-bdiv').scrollTop(0);
                }

                common.InitTooltips(grid);

                setRecordCount(data.records);
                model.RunResults(false);
                $.unblockUI();

                deferred.resolve();

            },
            gridComplete: function() {

                $scope.find('#jqGridSupportRequestsPager').find('#input_jqGridPager').find('input').off('change').on('change', function() {
                    grid.jqGrid().trigger('reloadGrid', [{ page: $(this).val() }]);
                });

                var gridContainer = grid.parents('.ui-jqgrid:eq(0)');
                gridContainer.addClass('visible');

            },
            colModel: [
                {
                    label: 'Closed', name: '', width: 52, resizable: false, sortable: false, classes: 'text-center', formatter: function (cellValue, options, rowData) {
                        var cb = $('<input type="checkbox"/>').addClass('form-control');
                        if (rowData.IsClosed)
                            cb.attr('checked', 'checked');
                        if (!rowData.CanTakeAction || rowData.IsAccountClosed)
                            cb.prop('disabled', 'disabled');
                        return cb.wrap('<div></div>').parent().html();
                    }
                },
                {
                    label: 'Case ID',
                    name: 'SupportRequestDisplayId',
                    width: 120,
                    resizable: false,
                    formatter: function(cellValue, options, rowObject) {

                        var html = '<span style="display: block; position: relative;">';
                        html += '<a href="javascript:;" title="View Support Request History">' + cellValue + '</a>';

                        if (rowObject.HasMessages && rowObject.UnreadMessages > 0)
                            html += '<span class="glyphicon glyphicon-envelope" style="color: #ff7a32;" data-toggle="tooltip" data-placement="right" title="You have ' + rowObject.UnreadMessages + ' unread message(s)"></span>';
                        else if (rowObject.HasDraftMessage)
                            html += '<span class="glyphicon glyphicon-pencil" style="color: #000000;" data-toggle="tooltip" data-placement="right" title="You have an unsent draft message."></span>';
                        else if (rowObject.HasMessages)
                            html += '<span class="glyphicon glyphicon-envelope" style="color: #999999;" data-toggle="tooltip" data-placement="right" title="You have 0 unread messages."></span>';

                        if (rowObject.HasAttachments)
                            html += '<span class="glyphicon glyphicon-paperclip" data-toggle="tooltip" data-placement="right" title="There are files attached to this case."></span> ';


                        html += '</span>';

                        return html;
                    }
                },
                { label: 'Created', name: 'InsertDate', width: 80, resizable: false },
                { label: 'Submitted For', name: 'SubmittedFor', width: 120, resizable: false },
                { label: 'Status', name: 'SupportRequestStatus', width: 60, resizable: false },
                {
                    label: 'Request Details',
                    name: 'MessageBody',
                    width: 125,
                    resizable: false,
                    sortable: false,
                    formatter: function(cellValue) {
                        var html = '<a href="javascript:;" class="pull-right" data-toggle="tooltip" data-placement="top" title="' + cellValue + '"><em class="glyphicon glyphicon-comment"></em></a>';
                        html += '<span style="display: block; max-width: 160px !important; white-space: nowrap; overflow: hidden; text-overflow: ellipsis;" title="">' + cellValue + '</span>';

                        return html;
                    }
                },
                { label: 'Topic - Sub Topic', name: 'SupportTopic', width: 150, resizable: false },
                {
                    label: 'FollowUp',
                    name: 'IsFlaggedForFollowUp',
                    classes: 'text-center',
                    width: 60,
                    resizable: false,
                    formatter: function (cellValue, options, rowData) {
                        var html = '';
                        if (rowData.IsFlaggedForFollowUp)
                            html = '<span class="glyphicon glyphicon-flag" style="color: RED;" data-toggle="tooltip" data-placement="right" title="Flagged for follow-up"></span>';
                        return html;
                    }
                },
                { label: 'Last Modified Date', name: 'LastMessageDate', width: 115, resizable: false },
                { label: 'Last Modified By', name: 'LastMessageBy', width: 110, resizable: false }
            ]
        });

        return deferred;
    };

    var setModel = function(visitPayUserId) {

        if (!model) {
            model = new filterViewModel();
            model.VisitPayUserId(visitPayUserId);
            ko.applyBindings(model, $('#jqGridFilterSupportRequests')[0]);
            model.ChangeNotifications();
        }

    };

    guarantorAccountSupportHistory.OnDataLoaded = function () { };
    guarantorAccountSupportHistory.GetInitialCount = function (visitPayUserId) {

        //
        var deferred = $.Deferred();

        //
        bindFilters();
        setModel(visitPayUserId);
        resetFilters();

        //
        $.post('/Search/SupportRequestTotals', { model: model }, function (data) {
            setRecordCount(data, false);
            deferred.resolve();
        }, 'json');

        //
        return deferred;

    };

    var hasInitialized = false;
    guarantorAccountSupportHistory.Initialize = function (visitPayUserId) {

        if (hasInitialized)
            return null;
        else {
            $.blockUI();
            hasInitialized = true;
        }

        var deferred = $.Deferred();

        //
        $scope = $('#section-supporthistory');

        //
        bindFilters();
        setModel(visitPayUserId);
        resetFilters();

        //
        bindGrid($scope).done(function () {

            deferred.resolve();
            deferred = null;

        });

        $scope.on('click', '#btnCreateSupportRequest', function(e) {

            e.preventDefault();
            $.blockUI();
            createSupportRequest.Initialize($('#modalCreateSupportRequestContainer'), null, visitPayUserId);

        });
        $scope.on('submit', '#jqGridFilterSupportRequests', function (e) {
            e.preventDefault();
            $.blockUI();
            reloadGrid();
        });
        $scope.on('click', '#btnGridFilterSupportRequestsReset', 'click', function () {
            resetFilters();
            $.blockUI();
            reloadGrid();
        });

        return deferred;

    };

    createSupportRequest.OnCreated = refreshGrid;
    supportRequestAction.OnCompleted = refreshGrid;
    $(document).on('supportRequestChanged', refreshGrid);

    return guarantorAccountSupportHistory;


})(window.VisitPay.SupportRequestAction, window.VisitPay.SupportRequest, window.VisitPay.CreateSupportRequest, window.VisitPay.Common, jQuery, ko);
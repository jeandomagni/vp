﻿window.VisitPay.GuarantorAccountChatHistory = (function (guarantorAccountChatHistoryDetails, common, $, ko) {
    var guarantorAccountChatHistory = {};
    var guarantorVisitPayUserGuid;
    
    var urls = {
        HistoryOverview: VisitPay.ChatSettings.ChatApiUrl + 'thread/threadHistoryOverview?'
    };

    var filterViewModel = function () {

        var self = this;

        self.ChatHistoryDateRangeFrom = ko.observable();
        self.ChatHistoryDateRangeTo = ko.observable();
        self.DateRangeFrom = ko.observable();
        self.VisitPayUserGuid = ko.observable();

        self.RunResults = ko.observable(false);
        self.ChangeNotifications = function () {

            var setRun = function () {
                self.RunResults(true);
            };

            self.DateRangeFrom.subscribe(setRun);
            self.ChatHistoryDateRangeFrom.subscribe(setRun);
            self.ChatHistoryDateRangeTo.subscribe(setRun);
        };
    };

    var model;
    var $scope;
    var initialDateRange;
    var chatGatewayApiToken;

    var grid;
    var gridDefaultSortName = 'threadDateCreated';
    var gridDefaultSortOrder = 'desc';

    var buildGridSearchUrl = function() {
        var url = urls.HistoryOverview;
        url += 'userToken=' + model.VisitPayUserGuid();
        var fromDate;
        if (!model.ChatHistoryDateRangeFrom() || model.ChatHistoryDateRangeFrom().length == 0) {
            fromDate = model.DateRangeFrom();
        } else {
            fromDate = model.ChatHistoryDateRangeFrom();
        }
        url += '&fromDate=' + encodeURIComponent(fromDate);
        url += '&toDate=' + encodeURIComponent(model.ChatHistoryDateRangeTo());
        return url;
    };

    var bindFilters = function () {

        initialDateRange = $('#GuarantorChatHistoryFilter_DateRangeFrom').val();

        // prevent focus on validate failure to prevent calendar from opening
        $('#jqGridFilterChatHistory').on('invalid-form.validate', function (form, validator) {
            validator.settings && (validator.settings.focusInvalid = false);
        });

        //
        $('#section-chathistory .input-group.date').each(function () {
            var element = $(this);
            element.datepicker({
                clearBtn: true,
                format: 'mm/dd/yyyy',
                orientation: 'top left',
                autoclose: true
            }).on('hide', function () {
                if ($('#jqGridFilterChatHistory').valid())
                    common.ResetUnobtrusiveValidation($('#jqGridFilterChatHistory'));
            });
        });

    };

    var reloadGrid = function () {
        //Build the new URL based on filters
        var historyUrl = buildGridSearchUrl();
        log.debug('reloading chatHistory grid with url: ', historyUrl);
        grid.setGridParam({ page: 1 });
        grid.sortGrid(gridDefaultSortName, true, gridDefaultSortOrder);
        grid.jqGrid().setGridParam({ url: historyUrl }).trigger('reloadGrid');
    };

    var resetFilters = function () {
        model.DateRangeFrom(initialDateRange);
        model.ChatHistoryDateRangeFrom('');
        model.ChatHistoryDateRangeTo('');
        model.RunResults(false);
    };

    var bindGrid = function ($scope) {

        var deferred = $.Deferred();

        //Get grid
        grid = $scope.find('#jqGridChatHistory');

        //Setup grid
        var historyUrl = buildGridSearchUrl();
        log.debug('historyURL in bind grid: ', historyUrl);
        grid.jqGrid({
            url: historyUrl,
            loadBeforeSend: function (jqXHR) {
                //Add apiToken to request
                jqXHR.setRequestHeader('Authorization', 'Bearer ' + chatGatewayApiToken);
            },
            datatype: 'json',
            mtype: 'GET',
            ajaxGridOptions: { jsonp: true, contentType: 'application/json;' },
            jsonReader: { root: 'threadHistoryOverviews' },
            autowidth: true,
            pager: 'jqGridChatHistoryPager',
            sortname: gridDefaultSortName,
            sortorder: gridDefaultSortOrder,
            loadComplete: function (data) {
                log.debug('chatHistory grid load completed with data: ', data);
                var gridContainer = grid.parents('.ui-jqgrid:eq(0)');
                var gridNoRecords = $scope.find('#jqGridChatHistoryNoRecords');

                if (data != null && data.threadHistoryOverviews.length > 0) {
                    gridContainer.show();
                    gridNoRecords.removeClass('in');
                    grid.closest('.ui-jqgrid-bdiv').scrollTop(0);

                    //Add event listeners for grid rows
                    $('.viewThreadHistory').off('click');
                    $('.viewThreadHistory').click(function (e) {
                        e.preventDefault();
                        $.blockUI();

                        var threadGuid = $(e.target).data('threadGuid');
                        var userToken = $(e.target).data('userToken');
                        log.debug('viewThreadHistory clicked with threadGuid/userToken: ', threadGuid, userToken);

                        //Load and open the details modal
                        $.get('/Search/GuarantorChatHistoryDetails', {}, function(html) {
                            $('#modalChatHistoryDetailsContainer').html(html);
                            $('#modalChatHistoryDetailsContainer').find('.modal').modal();
                            guarantorAccountChatHistoryDetails.Initialize(threadGuid, userToken, guarantorVisitPayUserGuid);
                        });
                    });
                } else {
                    gridContainer.hide();
                    gridNoRecords.addClass('in');
                    grid.closest('.ui-jqgrid-bdiv').scrollTop(0);
                }

                common.InitTooltips(grid);

                model.RunResults(false);
                $.unblockUI();

                deferred.resolve();
            },
            gridComplete: function () {

                $scope.find('#jqGridChatHistoryPager').find('#input_jqGridPager').find('input').off('change').on('change', function () {
                    grid.jqGrid().trigger('reloadGrid', [{ page: $(this).val() }]);
                });

                var gridContainer = grid.parents('.ui-jqgrid:eq(0)');
                gridContainer.addClass('visible');

            },
            colModel: [
                {
                    label: 'Thread Start Date',
                    name: 'threadDateCreated',
                    resizable: false,
                    sortable: false,
                    formatter: function (cellValue, options, rowObject) {
                        //Convert UTC dateTime to local
                        var localDateTime = moment(cellValue).local().format('MM/DD/YYYY hh:mm:ss A');
                        var html = '<span style="display: block; position: relative;">' + localDateTime + '</span>';
                        return html;
                    }
                },
                {
                    label: 'Thread Closed Date',
                    name: 'threadDateClosed',
                    resizable: false,
                    sortable: false,
                    formatter: function (cellValue, options, rowObject) {
                        //Convert UTC dateTime to local
                        var localDateTime;
                        if (cellValue) {
                            localDateTime = moment(cellValue).local().format('MM/DD/YYYY hh:mm:ss A');
                        } else {
                            localDateTime = 'Ongoing';
                        }
                        var html = '<span style="display: block; position: relative;">' + localDateTime + '</span>';
                        return html;
                    }
                },
                {
                    label: 'Guarantor',
                    name: 'guarantorName',
                    resizable: false,
                    sortable: false,
                    formatter: function (cellValue, options, rowObject) {
                        var html = '<span style="display: block; position: relative;">';
                        html += '<a href="javascript:;" class="viewThreadHistory" data-thread-guid="' + rowObject.threadGuid + '" data-user-token="' + rowObject.guarantorUserToken + '" data-toggle="tooltip" data-placement="right" title="Click to see thread history for guarantor">' + cellValue + '</a>';
                        html += '</span>';

                        return html;
                    }
                },
                {
                    label: 'Chat Operator',
                    name: 'operatorName',
                    resizable: false,
                    sortable: false,
                    formatter: function (cellValue, options, rowObject) {
                        //Check if there was an operator
                        var html;
                        if (cellValue) {
                            html = '<span style="display: block; position: relative;">';
                            html += '<a href="javascript:;" class="viewThreadHistory" data-thread-guid="' + rowObject.threadGuid + '" data-user-token="' + rowObject.operatorUserToken + '" data-toggle="tooltip" data-placement="right" title="Click to see thread history for operator">' + cellValue + '</a>';
                            html += '</span>';
                        } else {
                            //No operator
                            html = '<span style="display: block; position: relative;">None</span>';
                        }

                        return html;
                    }
                },
                { label: 'Thread Status', name: 'threadStatus', resizable: false, sortable: false },
                {
                    label: 'Duration',
                    name: 'duration',
                    //width: 113,
                    resizable: false,
                    sortable: false,
                    formatter: function (cellValue, options, rowObject) {
                        //Get the different between start/end times and display in humanized form
                        var start = moment(rowObject.threadDateCreated);
                        var end = rowObject.threadDateClosed != null ? moment(rowObject.threadDateClosed) : moment.utc();
                        var duration = moment.duration(end.diff(start));

                        var html = '<span style="display: block; position: relative;">' + duration.humanize() + '</span>';
                        return html;
                    }
                }
            ]
        });

        return deferred;
    };

    var setModel = function (visitPayUserGuid) {
        if (!model) {
            model = new filterViewModel();
            model.VisitPayUserGuid(visitPayUserGuid);
            ko.applyBindings(model, $('#jqGridFilterChatHistory')[0]);
            model.ChangeNotifications();
        }
    };

    var hasInitialized = false;
    guarantorAccountChatHistory.Initialize = function(visitPayUserGuid) {
        log.debug('guarantorAccountChatHistory Initialize with VisitPayUserGuid: ', visitPayUserGuid);

        //Save guarantor user guid
        guarantorVisitPayUserGuid = visitPayUserGuid;

        if (hasInitialized)
            return null;
        else {
            $.blockUI();
            hasInitialized = true;
        }

        //Set scope
        $scope = $('#section-chathistory');

        //Show no records until loaded
        var jqGrid = $scope.find('#jqGridChatHistory');
        var gridContainer = jqGrid.parents('.ui-jqgrid:eq(0)');
        var gridNoRecords = $scope.find('#jqGridChatHistoryNoRecords');
        gridContainer.hide();
        gridNoRecords.addClass('in');

        //Must load api token before loading grid
        VisitPay.ChatSettings.ChatApiToken().done(function (apiToken) {
            chatGatewayApiToken = apiToken;

            var deferred = $.Deferred();

            //Setup filters
            bindFilters();
            setModel(visitPayUserGuid);
            resetFilters();

            //Bind grid
            bindGrid($scope).done(function () {
                deferred.resolve();
                deferred = null;
            });

            //Setup event listeners
            $scope.on('submit', '#jqGridFilterChatHistory', function (e) {
                e.preventDefault();
                $.blockUI();
                reloadGrid();
            });
            $scope.on('click', '#btnGridFilterChatHistoryReset', 'click', function () {
                resetFilters();
                $.blockUI();
                reloadGrid();
            });

            //Listen for export button click
            $scope.on('click', '#btnChatHistoryExport', 'click', function (e) {
                e.preventDefault();

                $.blockUI();

                var historyUrl = buildGridSearchUrl();
                historyUrl += '&page=1&rows=10000';
                //Load histories
                $.ajax({
                    url: historyUrl,
                    type: 'GET',
                    cache: false,
                    contentType: 'application/json',
                    crossDomain: true,
                    beforeSend: function (xhr) {
                        xhr.setRequestHeader('Authorization', 'Bearer ' + apiToken);
                    },
                    success: function (response) {
                        log.debug('got response from chatGateway: ', response);

                        //Call download
                        $.post('/Search/GuarantorChatHistoryOverviewExport', {
                            chatHistoryOverviews: response,
                            guarantorVisitPayUserGuid: visitPayUserGuid
                        },
                        function(result) {
                            if (result.Result === true) {
                                window.location.href = result.Message;
                            }
                        },
                        'json');
                    },
                    complete: function () {
                        $.unblockUI();
                    }
                });
            });

            return deferred;
        });
    };

    return guarantorAccountChatHistory;
})(window.VisitPay.GuarantorAccountChatHistoryDetails, window.VisitPay.Common, jQuery, ko);
﻿window.VisitPay.GuarantorAccountCommunications = (function (communicationEvents, communicationContent, common, $, ko) {

    var guarantorAccountCommunications = {};

    var filterViewModel = function () {

        var self = this;

        self.CommunicationDateRangeFrom = ko.observable();
        self.CommunicationDateRangeTo = ko.observable();
        self.DateRangeFrom = ko.observable();
        self.VisitPayUserId = ko.observable();

        self.RunResults = ko.observable(false);
        self.ChangeNotifications = function () {

            var setRun = function () {
                self.RunResults(true);
            };

            self.DateRangeFrom.subscribe(setRun);
            self.CommunicationDateRangeFrom.subscribe(setRun);
            self.CommunicationDateRangeTo.subscribe(setRun);

        };
    };

    var model;
    var $scope;
    var initialDateRange;
    var accountClosed = $('#AccountClosedDate').val();

    var grid;
    var gridDefaultSortName = 'CreatedDate';
    var gridDefaultSortOrder = 'desc';

    var bindFilters = function () {

        initialDateRange = $('#GuarantorCommunicationsFilter_DateRangeFrom').val();

        // prevent focus on validate failure to prevent calendar from opening
        $('#jqGridFilterCommunications').on('invalid-form.validate', function (form, validator) {
            validator.settings && (validator.settings.focusInvalid = false);
        });

        //
        $('#section-communications .input-group.date').each(function () {
            var element = $(this);
            element.datepicker({
                clearBtn: true,
                format: 'mm/dd/yyyy',
                orientation: 'top left',
                autoclose: true
            }).on('hide', function () {
                if ($('#jqGridFilterCommunications').valid())
                    common.ResetUnobtrusiveValidation($('#jqGridFilterCommunications'));
            });
        });

    };
    var reloadGrid = function () {
        var postModel = ko.toJS(model);
        $.extend(grid.jqGrid('getGridParam', 'postData'), { model: postModel });
        grid.setGridParam({ page: 1 });
        grid.sortGrid(gridDefaultSortName, true, gridDefaultSortOrder);
    };
    var resetFilters = function () {
        model.DateRangeFrom(initialDateRange);
        model.CommunicationDateRangeFrom('');
        model.CommunicationDateRangeTo('');
        model.RunResults(false);
    };
    var setRecordCount = function (count) {
        guarantorAccountCommunications.OnDataLoaded(count);
    };

    var bindGrid = function ($scope) {

        var deferred = $.Deferred();

        //
        grid = $scope.find('#jqGridCommunications');

        //
        grid.jqGrid({
            url: '/Search/GuarantorCommunications',
            postData: { model: ko.toJS(model) },
            jsonReader: { root: 'Communications' },
            autowidth: false,
            pager: 'jqGridCommunicationsPager',
            sortname: gridDefaultSortName,
            sortorder: gridDefaultSortOrder,
            loadComplete: function (data) {

                var gridContainer = grid.parents('.ui-jqgrid:eq(0)');
                var gridNoRecords = $scope.find('#jqGridCommunicationsNoRecords');

                if (data.Communications != null && data.Communications.length > 0) {

                    gridContainer.show();
                    gridNoRecords.removeClass('in');
                    grid.closest('.ui-jqgrid-bdiv').scrollTop(0);

                    grid.find('.jqgrow').each(function () {

                        var rowData = data.Communications[$(this).index() - 1];

                        $(this).find('a:eq(0)').on('click', function (e) {
                            e.preventDefault();

                            $.blockUI();

                            $.get('/Search/GuarantorCommunicationEvents/', {}, function (html) {

                                $('#modalCommunicationEventsContainer').html(html);
                                $('#modalCommunicationEventsContainer').find('.modal').modal();
                                communicationEvents.Initialize(rowData.EmailId);

                                $.unblockUI();

                            });
                        });
                        $(this).find('a:eq(1)').on('click', function (e) {
                            e.preventDefault();

                            $.blockUI();

                            $.get('/Search/GuarantorCommunicationContent/', {}, function (html) {

                                $('#modalCommunicationContentContainer').html(html);
                                $('#modalCommunicationContentContainer').find('.modal').modal();
                                communicationContent.Initialize(rowData.EmailBody, rowData.CommunicationMethod === 'Sms');

                                $.unblockUI();

                            });
                        });
                        $(this).find('a:eq(2)').on('click', function (e) {
                            e.preventDefault();
                            if (accountClosed) {
                                common.ModalGenericConfirmation(
                                    'Account Closed',
                                    '<p>' +
                                    '<b>Unable to resend message on a closed account</b>' +
                                    '</p>',
                                    'OK',
                                    'Cancel');
                            } else if(rowData.CommunicationMethod === 'Mail') {
                                common.ModalGenericConfirmation(
                                    'Not Supported',
                                    '<p>' +
                                    'Resending mail through this menu is not supported' +
                                    '</p>',
                                    'OK',
                                    'Cancel'
                                    );
                            } else {
                                $.blockUI();

                                $.get('/Search/GuarantorCommunicationResendModal/', { emailId: rowData.EmailId, visitPayUserId: model.VisitPayUserId(), subject: rowData.EmailSubject, to: rowData.To, isSms: rowData.CommunicationMethod === 'Sms' }, function (html) {

                                    $('#modalEmailResendContainer').html(html);
                                    $('#modalEmailResendContainer').find('.modal').modal();

                                    window.VisitPay.Common.ResetUnobtrusiveValidation($('#modalEmailResendContainer').find('form'));

                                    $.unblockUI();
                                });

                            }
                        });
                    });

                } else {

                    gridContainer.hide();
                    gridNoRecords.addClass('in');
                    grid.closest('.ui-jqgrid-bdiv').scrollTop(0);
                }

                common.InitTooltips(grid);
                
                setRecordCount(data.records);
                model.RunResults(false);
                $.unblockUI();

                deferred.resolve();

            },
            gridComplete: function () {

                $scope.find('#jqGridCommunicationsPager').find('#input_jqGridPager').find('input').off('change').on('change', function () {
                    grid.jqGrid().trigger('reloadGrid', [{ page: $(this).val() }]);
                });

                var gridContainer = grid.parents('.ui-jqgrid:eq(0)');
                gridContainer.addClass('visible');

            },
            colModel: [
                { label: 'Date first sent', name: 'CreatedDate', width: 155, resizable: false },
                {
                    label: 'Subject',
                    name: 'Subject',
                    width: 355,
                    resizable: false,
                    formatter: function (cellValue, options, rowObject) {

                        var html = '<span style="display: block; position: relative;">';
                        html += '<a href="javascript:;" data-toggle="tooltip" data-placement="right" title="Click to see activity detail history">' + cellValue + '</a>';
                        html += '</span>';
                        return html;
                    }
                },
                {
                    label: 'Method',
                    name: 'CommunicationMethod',
                    width: 130,
                    resizable: false,
                    formatter: function (cellValue, options, rowObject) {

                        var html = '<span style="display: block; position: relative;">' + cellValue + '</span>';
                        return html;
                    }
                },
                {
                    label: 'Content',
                    name: '',
                    width: 130,
                    resizable: false,
                    sortable: false,
                    formatter: function (cellValue, options, rowObject) {

                        var html = '<span style="display: block; position: relative;"><a href="javascript:;" data-toggle="tooltip" data-placement="right" title="Display the content of the message">Show Content</a></span>';
                        return html;
                    }
                },
                {
                    label: 'Take Action',
                    name: '',
                    width: 86,
                    resizable: false,
                    sortable: false,
                    formatter: function (cellValue, options, rowObject) {

                        var html = '<span style="display: block; position: relative;"><a href="javascript:;" data-toggle="tooltip" data-placement="right" title="Resend the message">Resend</a></span>';
                        return html;
                    }
                },
                {
                    label: 'First Delivered',
                    name: 'DeliveredDate',
                    width: 135,
                    resizable: false,
                    formatter: function (cellValue, options, rowObject) {

                        var html = '<span style="display: block; position: relative;">';
                        if (rowObject.WasDelivered) {
                            html += cellValue;
                        } else {
                            html += 'No';
                        }
                        html += '</span>';
                        return html;
                    }
                }
            ]
        });

        return deferred;
    };
    var setModel = function (visitPayUserId) {
        if (!model) {
            model = new filterViewModel();
            model.VisitPayUserId(visitPayUserId);
            ko.applyBindings(model, $('#jqGridFilterCommunications')[0]);
            model.ChangeNotifications();
        }
    };

    guarantorAccountCommunications.OnDataLoaded = function () { };
    guarantorAccountCommunications.GetInitialCount = function (visitPayUserId) {

        //
        var deferred = $.Deferred();

        bindFilters();

        //
        setModel(visitPayUserId);
        resetFilters();

        //
        $.post('/Search/CommunicationsTotals', { model: model }, function (data) {
            setRecordCount(data, false);
            deferred.resolve();
        }, 'json');

        //
        return deferred;

    };

    var hasInitialized = false;
    guarantorAccountCommunications.Initialize = function (visitPayUserId) {

        if (hasInitialized)
            return null;
        else {
            $.blockUI();
            hasInitialized = true;
        }

        var deferred = $.Deferred();

        //
        $scope = $('#section-communications');

        //
        bindFilters();
        setModel(visitPayUserId);
        resetFilters();

        //
        bindGrid($scope).done(function () {

            deferred.resolve();
            deferred = null;

        });

        $scope.on('submit', '#jqGridFilterCommunications', function (e) {
            e.preventDefault();
            $.blockUI();
            reloadGrid();
        });
        $scope.on('click', '#btnGridFilterCommunicationsReset', 'click', function () {
            resetFilters();
            $.blockUI();
            reloadGrid();
        });

        $scope.on('click', '#btnCommunicationsExport', 'click', function (e) {
            e.preventDefault();
            $.post('/Search/GuarantorCommunicationsExport', {
                model: ko.toJS(model),
                sidx: grid.jqGrid('getGridParam', 'sortname'),
                sord: grid.jqGrid('getGridParam', 'sortorder')
            }, function (result) {
                if (result.Result === true)
                    window.location.href = result.Message;
            }, 'json');

        });

        var resentEmailContainer = $('#modalEmailResendContainer');

        resentEmailContainer.on('submit','form','submit', function (e) {
            
            e.preventDefault();
            window.VisitPay.Common.ResetValidationSummary($(this));

            if ($(this).valid()) {
                $.blockUI();
                var model = $(this).serialize();

                $.post('/Search/GuarantorCommunicationResend', model, function (result) {
                    $('#modalEmailResendContainer').find('.modal').modal('hide');
                    $.unblockUI();
                    reloadGrid();
                }, 'json');
            }
        });

        return deferred;

    };

    return guarantorAccountCommunications;


})(window.VisitPay.GuarantorAccountCommunicationEvents, window.VisitPay.GuarantorAccountCommunicationContent, window.VisitPay.Common, jQuery, ko);
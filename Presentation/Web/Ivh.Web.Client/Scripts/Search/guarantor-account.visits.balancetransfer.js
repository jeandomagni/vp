﻿window.VisitPay.BalanceTransferStatusHistory = (function (common, $) {

	window.VisitPay.Grids.BalanceTransferStatusHistory = (function ($gridElement, filterModel, handlers) {

        var gridOpts = {
            autowidth: false,
            url: '/Search/BalanceTransferStatusHistoryGrid',
            sortname: 'InsertDate',
            colModel: [
                {
                    label: 'BT Flag Event',
                    name: 'BalanceTransferStatusDisplayName',
                    sortable: false,
                    width: 170
                },
                {
                    label: 'Date Changed',
                    name: 'InsertDate',
                    sortable: false,
                    width: 145
                },
                {
                    label: 'Changed By',
                    name: 'VisitPayUserName',
                    sortable: false,
                    width: 100
                },
                {
                    label: 'Change Reason',
                    name: 'Notes',
                    sortable: false,
                    width: 450
                }
            ]
        };

        return new window.VisitPay.Grids.BaseGrid($gridElement, filterModel, $(), gridOpts, { noRecordsText: 'There is no balance transfer status history to display at this time.' }, { loadComplete: handlers.loadComplete });

    });

    function loadData(model, visitPayUserId, visitId) {

        var promise = $.Deferred();

        $.post('/Search/BalanceTransferStatusHistory', { guarantorVisitPayUserId: visitPayUserId, visitId: visitId}, function (data) {
            ko.mapping.fromJS(data, {}, model);
            promise.resolve();

        });

        return promise;

    };

    function loadModal() {

        var promise = $.Deferred();

        common.ModalNoContainerAsync('modalBalanceTransferStatusHistory', '/Search/BalanceTransferStatusHistory', 'GET', null, false).done(function ($modal) {

            promise.resolve($modal);

        });

        return promise;
    }

    function bindGrid(visitPayUserId, visitId) {

        var filterModel = {
            GuarantorVisitPayUserId: ko.observable(visitPayUserId),
            VisitId: ko.observable(visitId)
        };

        return new window.VisitPay.Grids.BalanceTransferStatusHistory($('#gridBalanceTransferStatusHistory'), filterModel, {
            loadComplete: function () {
                $.unblockUI();
            }
        });

    };

    function post(model, visitPayUserId, visitId) {

        var promise = $.Deferred();

        $.blockUI();

        var postModel = {
            visitId: visitId,
            visitPayUserId: visitPayUserId,
            notes: ko.unwrap(model.Notes)
        };

        var postUrl;

		if (ko.unwrap(model.CanSetEligible)) {
		    postUrl = '/Search/SetBalanceTransferStatusEligible';
		} else if (ko.unwrap(model.CanSetIneligible)) {
		    postUrl = '/Search/SetBalanceTransferStatusIneligible';
		} else {
		    promise.reject();
		    return promise;
		}

        $.post(postUrl, postModel, function (result) {

            if (!result) {
                promise.reject();
                $.unblockUI();
                return;
            }

            loadData(model, visitPayUserId, visitId).done(function () {

                $.unblockUI();
                promise.resolve();

            });

        });

        return promise;

    }

    function submit($form, model, visitPayUserId, visitId) {

        var promise = $.Deferred();

        if (!$form.valid()) {
            promise.reject();
            return promise;
        }

        post(model, visitPayUserId, visitId).done(function () {
            promise.resolve();
        });

        return promise;

    }

    //GuarantorVisitBalanceTransferStatusUpdateViewModel
    var BalanceTransferViewModel = function () {
        var self = this;
        self.VisitSourceSystemKey = ko.observable('');
        self.CanSetIneligible = ko.observable();
        self.CanSetEligible = ko.observable();
        self.Notes = ko.observable('');
        self.ButtonText = ko.computed(function () {
            return ko.unwrap(self.CanSetEligible) ? 'Mark Eligible For BT' : ko.unwrap(self.CanSetIneligible) ? 'Mark Ineligible For BT' : '';
        });
        self.TitleText = ko.computed(function() {
            return 'Balance Transfer Status History for Visit #' + ko.unwrap(self.VisitSourceSystemKey);
        });

        return self;
    };

    return {
        Initialize: function (visitPayUserId, visitId) {

            var model = new BalanceTransferViewModel();

            var hasChanged = false;
            var promise = $.Deferred();

            $.blockUI();
            loadData(model, visitPayUserId, visitId).done(function () {

                loadModal().done(function ($modal) {

                    //
                    $modal.on('hide.bs.modal.btstatushistory', function () { promise.resolve(hasChanged); });

                    //
                    ko.applyBindings(model, $modal[0]);

                    //
                    var grid = bindGrid(visitPayUserId, visitId);

                    //
                    var $form = $modal.find('form');
                    $form.parseValidation();
                    $form.off('submit').on('submit', function (e) {

                        e.preventDefault();
                        submit($(this), model, visitPayUserId, visitId).done(function () {

                            hasChanged = true;
                            grid.reload();

                        });

                    });

                    //
                    $.unblockUI();
                    $modal.modal('show');

                });

            });

            return promise;

        }
    };

})(window.VisitPay.Common = window.VisitPay.Common || {}, jQuery);
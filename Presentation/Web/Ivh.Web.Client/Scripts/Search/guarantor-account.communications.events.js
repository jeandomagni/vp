﻿window.VisitPay.GuarantorAccountCommunicationEvents = (function (common, $, ko) {
    var guarantorAccountCommunicationEvents = {};

    guarantorAccountCommunicationEvents.Initialize = function (emailId) {

        //
        var scope = $('#modalCommunicationEvents');
        scope.find('.modal').css('position', 'absolute');

        //
        var grid = scope.find('#jqGridCommunicationEvents');
        var gridDefaultSortName = 'EventDateTime';
        var gridDefaultSortOrder = 'asc';

        //
        grid.jqGrid({
            url: '/Search/GuarantorCommunicationEvents',
            postData: {
                EmailId: emailId
            },
            jsonReader: { root: 'Events' },
            pager: 'jqGridCommunicationEventsPager',
            sortname: gridDefaultSortName,
            sortorder: gridDefaultSortOrder,
            loadComplete: function(data) {

                var gridContainer = grid.parents('.ui-jqgrid:eq(0)');
                var gridNoRecords = $('#jqGridCommunicationEventsNoRecords');

                if (data.Events != null && data.Events.length > 0) {

                    gridContainer.show();
                    gridNoRecords.removeClass('in');

                    grid.closest('.ui-jqgrid-bdiv').scrollTop(0);

                } else {

                    gridContainer.hide();
                    gridNoRecords.addClass('in');

                    grid.closest('.ui-jqgrid-bdiv').scrollTop(0);
                }

                $.unblockUI();

            },
            gridComplete: function() {

                grid.find('[data-toggle="tooltip"]').tooltip({ animation: false, container: 'body' });

                $('#jqGridCommunicationEventsPager').find('#input_jqGridPager').find('input').on('change', function() {
                    grid.jqGrid().trigger('reloadGrid', [{ page: $(this).val() }]);
                });

                var gridContainer = grid.parents('.ui-jqgrid:eq(0)');
                gridContainer.addClass('visible');

            },
            colModel: [
                { label: 'Sent To', name: 'EmailAddress', width: 222, resizable: false },
                { label: 'Event', name: 'EventName', width: 75, resizable: false },
                { label: 'Date', name: 'EventDateTime', width: 150, resizable: false },
                { label: 'Reason', name: 'Reason', width: 245, resizable: false },
                { label: 'Response', name: 'Response', width: 174, resizable: false }
            ]
        });

        var btnExport = scope.find('#btnCommunicationEventsExport');
        btnExport.on('click', function (e) {
            e.preventDefault();
            $.post('/Search/GuarantorCommunicationEventsExport', {
                EmailId: emailId,
                sidx: grid.jqGrid('getGridParam', 'sortname'),
                sord: grid.jqGrid('getGridParam', 'sortorder')
            }, function (result) {
                if (result.Result === true)
                    window.location.href = result.Message;
            }, 'json');

        });
    };

    return guarantorAccountCommunicationEvents;

})(window.VisitPay.Common = window.VisitPay.Common || {},
    jQuery,
    window.ko);
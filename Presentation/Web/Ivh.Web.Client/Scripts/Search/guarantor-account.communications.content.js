﻿window.VisitPay.GuarantorAccountCommunicationContent = (function (common, $, ko) {

    var viewModel = function (content) {

        var self = this;

        self.Content = ko.observable(content);
    };

    var guarantorAccountCommunicationContent = {};

    guarantorAccountCommunicationContent.Initialize = function (content, isSms) {

        //
        var scope = $('#modalCommunicationContent');
        scope.find('.modal').css('position', 'absolute');

        //

        var $temp = $('<div/>').html(content);
        if (!isSms) {
            $temp = $('<div/>').append($temp.children().not('head,title,style,script,link'));
        } else {
            scope.find('.modal-title').html('Text Message Content');
        }
        var html = $temp.html().replace(/(^\s+|\s+$)/g, '').replace(/\\r\\n/g, '<br/>').replace(/\\r/g, '<br/>').replace(/\\n/g, '<br/>');
        var $temp1 = $('<div/>').html(html);

        //
        var model = new viewModel($temp1[0].outerHTML);
        ko.applyBindings(model, scope[0]);

    };

    return guarantorAccountCommunicationContent;

})(window.VisitPay.Common = window.VisitPay.Common || {},
    jQuery,
    window.ko);
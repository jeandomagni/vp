﻿window.VisitPay.GuarantorAccountChatHistoryDetails = (function($, ko) {
    var guarantorAccountChatHistoryDetails = {};
    var model; //ChatThreadHistoryViewModel
    var $scope;
    var chatGatewayApiToken;
    var chatThreadHistoryJson; //Save for export so we don't have to load twice
    var guarantorVisitPayUserGuid;

    var urls = {
        ThreadHistory: VisitPay.ChatSettings.ChatApiUrl + 'thread/threadHistory?',
        AttachmentDownload: VisitPay.ChatSettings.ChatApiUrl + 'Message/DownloadFile?'
    };

    var formatTime = function(time) {
        if (time) {
            return moment(time).local().format('MM/DD/YYYY hh:mm:ss A');
        } else {
            return '';
        }

    };

    //User view model/mapping
    var ChatThreadUserViewModel = function (data) {
        var self = this;

        self.id = ko.observable(null);
        self.firstName = ko.observable(null);
        self.lastName = ko.observable(null);
        self.isGuarantor = ko.observable(false);
        self.isOperator = ko.observable(false);
        self.isSystem = ko.observable(false);
        self.userToken = ko.observable(null);

        self.fullName = ko.computed(function() {
            return self.firstName() + ' ' + self.lastName();
        });

        //Map from incoming JSON
        ko.mapping.fromJS(data, {}, self);
    };
    var ChatThreadUserViewModelMapping = {
        create: function (options) {
            var chatThreadUser = new ChatThreadUserViewModel(options.data);
            return chatThreadUser;
        }
    };

    //Message model/mapping
    var ChatThreadMessageViewModel = function (data) {
        var self = this;

        self.id = ko.observable(null);
        self.messageGuid = ko.observable(null);
        self.messageTimestamp = ko.observable(null);
        self.messageType = ko.observable(null); //System, Text, Emoji, File
        self.senderUserId = ko.observable(null); //Null if system message
        self.body = ko.observable(null); //Can be html
        self.content = ko.observable(null);
        self.sendingUser = ko.observable(null);

        self.messageTimestampFormatted = ko.computed(function() {
            return formatTime(self.messageTimestamp());
        });

        self.getFileName = function() {
            var parsedContent = JSON.parse(self.content());
            return parsedContent['FileName'];
        };

        self.downloadMessageAttachment = function () {
            log.debug('download message attachment clicked');

            //Get the attachment details
            var parsedContent = JSON.parse(self.content());
            var attachmentId = parsedContent['AttachmentId'];
            var threadId = parsedContent['ThreadId'];
            var userToken = parsedContent['UserToken'];
            var fileName = parsedContent['FileName'];

            //Build the download url
            var url = urls.AttachmentDownload;
            url += 'attachmentId=' + attachmentId;
            url += '&threadId=' + threadId;
            url += '&userToken=' + userToken;
            log.debug('downloading attachment with url: ', url);

            $.ajax({
                url: url,
                type: 'GET',
                cache: false,
                crossDomain: true,
                xhrFields: {
                    responseType: 'blob'
                },
                beforeSend: function (xhr) {
                    xhr.setRequestHeader('Authorization', 'Bearer ' + chatGatewayApiToken);
                },
                success: function (response) {
                    const blob = new Blob([response]);

                    if (navigator.msSaveOrOpenBlob) {
                        //IE
                        navigator.msSaveOrOpenBlob(blob, fileName);
                    } else {
                        //Real world
                        const url = window.URL.createObjectURL(blob);
                        const link = document.createElement('a');
                        link.href = url;
                        link.setAttribute('download', fileName);
                        document.body.appendChild(link);
                        link.click();
                        link.parentNode.removeChild(link);
                    }
                },
                complete: function() {
                    $.unblockUI();
                }
            });
        };

        //Map from incoming JSON
        ko.mapping.fromJS(data, { ignore: ['sendingUser']}, self);
    };
    var ChatThreadMessageViewModelMapping = {
        create: function (options) {
            var chatThreadMessage = new ChatThreadMessageViewModel(options.data);
            return chatThreadMessage;
        }
    };

    //The main view model for the page
    var ChatThreadHistoryViewModel = function(data) {
        var self = this;

        //Properties
        self.loaded = ko.observable(false);
        self.threadId = ko.observable(null); //ThreadGuid
        self.threadStatus = ko.observable(null);
        self.threadDateCreated = ko.observable(null);
        self.threadDateClosed = ko.observable(null);
        self.messageHistory = ko.observableArray([]); //Array of ChatThreadMessageViewModel
        self.threadUsers = ko.observableArray([]); //Array of ChatThreadUserViewModel

        //Methods
        self.getThreadTitle = function () {
            //Default title if not loaded
            if (!self.loaded()) {
                return 'Unable to load history';
            }

            var title = '';

            //Get names of all users on the thread
            var usersOnThread = [];
            ko.utils.arrayForEach(self.threadUsers(), function (user) {
                usersOnThread.push(user.fullName());
            });

            //Concatenate names
            title += usersOnThread.filter(function (val) { return val; }).join(' & ');

            //Add date
            title += ' on ' + formatTime(self.threadDateCreated());

            //Add status
            title += ' - ' + self.threadStatus();

            return title;
        };

        self.exportChatHistory = function() {
            log.debug('export chat history details button clicked: ', guarantorVisitPayUserGuid);
            $.blockUI();

            //Call download
            $.post('/Search/GuarantorChatThreadHistoryExport', {
                chatThreadHistory: chatThreadHistoryJson,
                guarantorVisitPayUserGuid: guarantorVisitPayUserGuid
            },
            function (result) {
                if (result.Result === true) {
                    window.location.href = result.Message;
                }
                $.unblockUI();
            },
            'json');
        };

        //Map from incoming JSON. Ignore properties that will be mapped by their own mappers.
        ko.mapping.fromJS(data, { ignore: ['messageHistory', 'threadUsers'] }, self);
    };
    var ChatThreadHistoryViewModelMapping = {
        create: function (options) {
            var chatThreadHistory = new ChatThreadHistoryViewModel(options.data);

            //Map messages
            var messageArray = ko.mapping.fromJS(options.data.messageHistory, ChatThreadMessageViewModelMapping);
            chatThreadHistory.messageHistory(ko.unwrap(messageArray));

            //Map users
            var userArray = ko.mapping.fromJS(options.data.threadUsers, ChatThreadUserViewModelMapping);
            chatThreadHistory.threadUsers(ko.unwrap(userArray));

            //Add sending user to all messages
            ko.utils.arrayForEach(chatThreadHistory.messageHistory(), function(message) {
                if (message.senderUserId() === null) {
                    //System user
                    var systemUser = new ChatThreadUserViewModel();
                    systemUser.firstName('System');
                    systemUser.lastName('');
                    systemUser.isSystem(true);
                    message.sendingUser(systemUser);
                } else {
                    //Get user for the message
                    var userForMessage = ko.utils.arrayFirst(chatThreadHistory.threadUsers(), function (user) {
                        return user.id() == message.senderUserId();
                    });
                    message.sendingUser(userForMessage);
                }
            });

            return chatThreadHistory;
        }
    };

    var loadChatHistory = function(threadGuid, userToken) {
        log.debug('loading chat history from chatGateway for threadGuid / userToken: ', threadGuid, userToken);
        VisitPay.ChatSettings.ChatApiToken().done(function (apiToken) {
            //Save API token for later use
            chatGatewayApiToken = apiToken;

            var historyUrl = urls.ThreadHistory + 'userToken=' + userToken + '&threadGuid=' + threadGuid;
            log.debug('calling chatGateway to get thread history with url: ', historyUrl);
            $.ajax({
                url: historyUrl,
                type: 'GET',
                cache: false,
                contentType: 'application/json',
                crossDomain: true,
                beforeSend: function (xhr) {
                    xhr.setRequestHeader('Authorization', 'Bearer ' + apiToken);
                },
                success: function (response) {
                    log.debug('got response from chatGateway: ', response);

                    if (response) {
                        //Save for export
                        chatThreadHistoryJson = response;

                        //Map response to model
                        model = ko.mapping.fromJS(response, ChatThreadHistoryViewModelMapping);
                        model.loaded(true);
                    }
                },
                complete: function() {
                    //Apply bindings
                    ko.applyBindings(model, $scope[0]);

                    $.unblockUI();
                }
            });
        });
    };

    guarantorAccountChatHistoryDetails.Initialize = function (threadGuid, userToken, guarantorUserGuid) {
        log.debug('guarantorAccountChatHistoryDetails Initialize with threadGuid / userToken / guarantorUserGuid: ', threadGuid, userToken, guarantorUserGuid);

        //Loading, blockUI
        $.blockUI();

        //Save guarantor user guid
        guarantorVisitPayUserGuid = guarantorUserGuid;

        //Set scope
        $scope = $('#modalChatHistoryDetails');

        //Init model
        model = new ChatThreadHistoryViewModel();

        //Load history
        loadChatHistory(threadGuid, userToken);
    };

    return guarantorAccountChatHistoryDetails;
})(jQuery, ko);
﻿window.VisitPay.EditAccountSettings = (function (common, $, ko) {

    var viewModel = function (vpGuarantorId) {
        var self = this;
        self.VpGuarantorId = ko.observable(vpGuarantorId);
    };

    var editAccountSettings = {};

    editAccountSettings.Initialize = function (vpGuarantorId) {
        var scope = $('#modalEditAccountSettings');
        scope.find('.modal').css('position', 'absolute');

        var form = scope.find('#frmEditAccountSettings');
        common.ResetUnobtrusiveValidation(form);

        var model = new viewModel(vpGuarantorId);
        ko.applyBindings(model, scope[0]);

        var submit = function () {
            if (form.valid()) {
                common.ResetUnobtrusiveValidation(form);
                $.post('/Search/GuarantorEditAccountSettings', form.serialize() + '&vpGuarantorId=' + ko.unwrap(model.VpGuarantorId) + '&__RequestVerificationToken=' + $('#__AjaxAntiForgeryForm input[name=__RequestVerificationToken]').val() ,function (result) {

                    if (result.Result) {
                        $('#modalEditAccountSettingsContainer').find('.modal').modal('hide');
                        location.reload();
                    } else
                        common.AppendMessageToValidationSummary(form, result.Message);
                   
                });
            }
        };

        scope.on('click', '#btnEditAccountSettingsSubmit', 'click', function (e) {
            e.preventDefault();
            submit();
        });

    };

    return editAccountSettings;

})(window.VisitPay.Common = window.VisitPay.Common || {},
    jQuery,
    window.ko);
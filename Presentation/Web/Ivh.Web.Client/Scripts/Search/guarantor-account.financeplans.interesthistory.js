﻿window.VisitPay.FinancePlanInterestRateHistory = (function (common, $) {

    var financePlanInterestRateHistory = {};

    financePlanInterestRateHistory.Initialize = function (guarantorVisitPayUserId, financePlanId) {

        //
        var scope = $('#modalFinancePlanInterestRateHistory');
        scope.find('.modal').css('position', 'absolute');

        //
        var grid = scope.find('#jqGridFinancePlanInterestRateHistory');
        var gridDefaultSortName = 'InsertDate';
        var gridDefaultSortOrder = 'desc';

        //
        grid.jqGrid({
            url: '/FinancePlan/FinancePlanInterestRateHistory',
            postData: {
                guarantorVisitPayUserId: guarantorVisitPayUserId,
                FinancePlanId: financePlanId
            },
            jsonReader: { root: 'FinancePlanInterestRateHistory' },
            pager: 'jqGridFinancePlanInterestRateHistoryPager',
            sortname: gridDefaultSortName,
            sortorder: gridDefaultSortOrder,
            autowidth: false,
            height: 1200, // overridden in css, this just makes the scrollbar appear correctly
            loadComplete: function (data) {

                var gridContainer = grid.parents('.ui-jqgrid:eq(0)');
                var gridNoRecords = $('#jqGridFinancePlanInterestRateHistoryNoRecords');

                if (data.FinancePlanInterestRateHistory != null && data.FinancePlanInterestRateHistory.length > 0) {

                    gridContainer.show();
                    gridNoRecords.removeClass('in');

                    grid.closest('.ui-jqgrid-bdiv').scrollTop(0);

                } else {

                    gridContainer.hide();
                    gridNoRecords.addClass('in');

                    grid.closest('.ui-jqgrid-bdiv').scrollTop(0);
                }

                $.unblockUI();

            },
            gridComplete: function () {

                $('#jqGridFinancePlanInterestRateHistoryPager').find('#input_jqGridPager').find('input').off('change').on('change', function () {
                    grid.jqGrid().trigger('reloadGrid', [{ page: $(this).val() }]);
                });

                var gridContainer = grid.parents('.ui-jqgrid:eq(0)');
                gridContainer.addClass('visible');

            },
            colModel: [
                { label: 'Date', name: 'InsertDate', width: 160, resizable: false },
                { label: 'Interest Rate', name: 'InterestRate', width: 110, classes: 'text-right', resizable: false },
                { label: 'Monthly Payment', name: 'PaymentAmount', width: 130, classes: 'text-right', resizable: false },
                { label: 'Description', name: 'ChangeDescription', width: 465, sortable: false, resizable: false }
            ]
        });

    };

    return financePlanInterestRateHistory;

})(window.VisitPay.Common = window.VisitPay.Common || {}, jQuery);
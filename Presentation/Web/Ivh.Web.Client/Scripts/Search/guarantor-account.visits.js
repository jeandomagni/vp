﻿window.VisitPay.GuarantorAccountVisits = (function (common, visitTransactions, visitStateHistory, visitAgingHistory, visitUnmatch, visitBalances, $, ko) {

    var guarantorAccountVisits = {};
    var filterViewModel = function () {

        var self = this;

        self.BillingApplication = ko.observable('');
        self.VisitDateRangeFrom = ko.observable('');
        self.VisitDateRangeTo = ko.observable('');
        self.VisitStateIds = ko.observableArray([]);
        self.VisitPayUserId = ko.observable();
        self.CurrentVisitPayUserId = ko.observable();

        self.RunResults = ko.observable(false);
        self.ChangeNotifications = function() {

            var setRun = function() {
                self.RunResults(true);
            };

            self.BillingApplication.subscribe(setRun);
            self.VisitDateRangeFrom.subscribe(setRun);
            self.VisitDateRangeTo.subscribe(setRun);

        };

    };

    var hasInitialized = false;
    var model;

    var bindFilters = function () {

        // visit status tooltip (because it's using an html element for the content)
        $('#aVisitState').tooltip({
            placement: 'right',
            html: true,
            title: function () {
                return $('#tooltip-visitStates').html();
            }
        });
        $('#jqGridFilterVisits').find('[data-toggle="tooltip"]').tooltip({ animation: false, container: 'body' });

        // prevent focus on validate failure to prevent calendar from opening
        $('#jqGridFilterVisits').on('invalid-form.validate', function (form, validator) {
            validator.settings && (validator.settings.focusInvalid = false);
        });

        //
        $('#section-visits .input-group.date').each(function () {
            var element = $(this);
            element.datepicker({
                clearBtn: true,
                format: 'mm/dd/yyyy',
                orientation: 'top left',
                autoclose: true
            }).on('hide', function () {
                if ($('#jqGridFilterVisits').valid())
                    common.ResetUnobtrusiveValidation($('#jqGridFilterVisits'));
            });
        });

        //
        var visitStateSelect = $('select#VisitStateIds');
        visitStateSelect.multiselect({
            enableClickableOptGroups: true,
            includeSelectAllOption: true,
            allSelectedText: 'All Statuses',
            nonSelectedText: 'All Statuses',
            nSelectedText: ' Statuses',
            numberDisplayed: 1,
            selectAllNumber: false,
            selectAllText: 'All Statuses',
            onChange: function () {

                model.VisitStateIds = new ko.observableArray([]);

                visitStateSelect.find('option:selected').each(function () {
                    model.VisitStateIds.push($(this).val());
                });

                model.RunResults(true);

            }
        });

    };

    var resetModel = function() {

        model.BillingApplication('');
        model.VisitDateRangeFrom('');
        model.VisitDateRangeTo('');
        model.VisitStateIds([1,2,3,4,5,6,7]); // WARNING: COUPLING Ivh.Common.Base.Enums.VisitStateDerivedEnum
        model.RunResults(false);

    };

    var resetFilters = function () {

        // reset viewmodel
        resetModel();

        // reset multiselect
        var visitStateSelect = $('select#VisitStateIds');
        visitStateSelect.find('option').prop('selected', false);

        ko.utils.arrayForEach(model.VisitStateIds(), function (visitStateId) {
            $('select#VisitStateIds option[value="' + visitStateId + '"]').prop('selected', true);
        });

        visitStateSelect.multiselect('refresh');

        common.ResetUnobtrusiveValidation($('#jqGridFilterVisits'));
    };

    var setRecordCount = function (count, containsPastDue) {
        guarantorAccountVisits.OnDataLoaded(count, containsPastDue);
    };

    guarantorAccountVisits.Setup = function(currentVisitPayUserId, filteredVisitPayUserId) {

        var promise = $.Deferred();
        var uid = filteredVisitPayUserId || currentVisitPayUserId;
        
        if (!model) {
            model = new filterViewModel();
            model.ChangeNotifications();
            ko.applyBindings(model, $('#jqGridFilterVisits')[0]);
        }
        
        model.CurrentVisitPayUserId(currentVisitPayUserId);
        model.VisitPayUserId(uid);

        bindFilters();
        resetModel();
        resetFilters();

        $.jgrid.gridUnload('#jqGridVisits');

        hasInitialized = false;

        promise.resolve();

        function reload() {
            var grid = $('#jqGridVisits');
            $.extend(grid.jqGrid('getGridParam', 'postData'), { model: ko.toJS(model) });
            grid.setGridParam({ page: 1 });
            grid.sortGrid(grid.jqGrid('getGridParam', 'sortname'), true, grid.jqGrid('getGridParam', 'sortorder'));
        }

        $(document).on('visitUnmatchActionComplete', reload);
        $(document).on('visits.reload', reload);

        return promise;

    };

    guarantorAccountVisits.OnDataLoaded = function () { };

    guarantorAccountVisits.GetInitialCount = function() {

        //
        var deferred = $.Deferred();

        //
        resetModel();

        //
        $.post('/Search/VisitTotals', { model: ko.toJS(model) }, function (data) {
            setRecordCount(data);
            deferred.resolve();
        }, 'json');

        //
        return deferred;

    };

    guarantorAccountVisits.Initialize = function () {

        if (hasInitialized) {
            return;
        } else {
            $.blockUI();
            hasInitialized = true;
        }

        //
        var grid = $('#jqGridVisits');
        var gridDefaultSortOrder = 'asc';
        var gridDefaultSortName = 'DischargeDate';
        var gridSetBalanceTransferOption = 'setBalanceTransfer';

        //
        var reloadGrid = function() {
            $.extend(grid.jqGrid('getGridParam', 'postData'), { model: ko.toJS(model) });
            grid.setGridParam({ page: 1 });
            grid.sortGrid(gridDefaultSortName, true, gridDefaultSortOrder);
        };
        
        //
        $('#jqGridFilterVisits').on('submit', function(e) {
            e.preventDefault();
            if ($(this).valid()) {
                $.blockUI();
                reloadGrid();
            }
        });

        $('#btnGridFilterVisitsReset').on('click', function() {
            $.blockUI();
            resetFilters();
            reloadGrid();
        });

        $('#btnVisitsExport').on('click', function(e) {

            e.preventDefault();
            $.post('/Search/GuarantorVisitsExport', {
                model: ko.toJS(model),
                sidx: grid.jqGrid('getGridParam', 'sortname'),
                sord: grid.jqGrid('getGridParam', 'sortorder')
            }, function(result) {
                if (result.Result === true)
                    window.location.href = result.Message;
            }, 'json');

        });

        //
        resetFilters();

        //
        grid.jqGrid({
            url: '/Search/GuarantorVisits',
            jsonReader: { root: 'Visits' },
            postData: { model: ko.toJS(model) },
            pager: 'jqGridPagerVisits',
            sortname: gridDefaultSortName,
            sortorder: gridDefaultSortOrder,
            footerrow: false,
            loadComplete: function(data) {
                var gridContainer = grid.parents('.ui-jqgrid:eq(0)');
                var gridNoRecords = $('#jqGridNoRecordsVisits');

                if (data.Visits != null && data.Visits.length > 0) {

                    gridContainer.show();
                    gridNoRecords.removeClass('in');

                    grid.off('click');
                    grid.find('.jqgrow').each(function() {
                        $(this).off('click');
                        $(this).on('click', 'td:eq(0) > a', function() {
                            var rowData = data.Visits[$(this).parents('tr').index() - 1];
                            $.blockUI();
                            $.get('/Search/GuarantorVisitTransactions/' + rowData.VisitId + '?visitPayUserId=' + model.VisitPayUserId(), {}, function(html) {
                                $('#modalVisitTransactionsContainer').html(html);
                                $('#modalVisitTransactionsContainer').find('.modal').modal();
                                visitTransactions.Initialize(model.VisitPayUserId());
                            });
                            $.unblockUI();
                        });
                        $(this).on('click', 'td:eq(4)', function() {
                            var rowData = data.Visits[$(this).parents('tr').index() - 1];
                            $.blockUI();
                            $.get('/Search/GuarantorVisitStateHistory', {}, function(html) {
                                $('#modalVisitStateHistoryContainer').html(html);
                                $('#modalVisitStateHistoryContainer').find('.modal').modal();
                                visitStateHistory.Initialize(model.VisitPayUserId(), rowData);
                            });
                            $.unblockUI();
                        });
                        $(this).on('click', 'td:eq(6)', function () {

                            if ($(this).text() === 'N/A') {
                                return;
                            }

                            var rowData = data.Visits[$(this).parents('tr').index() - 1];

                            visitAgingHistory.Initialize(model.VisitPayUserId(), rowData.VisitId).done(function(hasChanged) {
                                if (hasChanged) {
                                    grid.trigger('reloadGrid');
                                }
                            });

                        });
                        $(this).on('click', 'td:eq(9)', function () {

                            if ($(this).text() === '-') {
                                return;
                            }

                            var rowData = data.Visits[$(this).parents('tr').index() - 1];
                            visitBalances.Initialize(rowData.VisitId, rowData.UnclearedBalance, rowData.InterestDue, rowData.UnclearedPaymentsSum, rowData.CurrentBalance);
                        });
                        $(this).on('change', 'td:eq(10) select', function(e) {
                            e.preventDefault();
                            var rowData = data.Visits[$(this).parents('tr').index() - 1];
                            if ($(this).val() === '')
                                return;

                            if ($(this).val() === 'unmatch') {
                                visitUnmatch.OpenModal(rowData.VisitId, rowData.VisitPayUserId);
                            }
                            else if ($(this).val() === gridSetBalanceTransferOption) {
                                window.VisitPay.BalanceTransferStatusHistory.Initialize(rowData.VisitPayUserId, rowData.VisitId);
                            }

                            $(this).val('');
                        });
                    });

                } else {
                    gridContainer.hide();
                    gridNoRecords.addClass('in');
                }

                setRecordCount(data.records, grid.find('.visitstatus3').length > 0);
                model.RunResults(false);
                $.unblockUI();
            },
            gridComplete: function() {

                grid.find('[data-toggle="tooltip"]').tooltip({ animation: false, container: 'body' });

                $('#jqGridPagerVisits').find('#input_jqGridPager').find('input').off('change').on('change', function() {
                    grid.jqGrid().trigger('reloadGrid', [{ page: $(this).val() }]);
                });

                var gridContainer = grid.parents('.ui-jqgrid:eq(0)');
                gridContainer.addClass('visible');

            },
            colModel: [
                {
                    label: 'Visit#',
                    name: 'SourceSystemKeyDisplay',
                    width: 97,
                    resizable: false,
                    formatter: function (cellValue, options, rowObject) {
                        if (rowObject.IsRemoved) {
                            return '<div data-toggle="tooltip" data-placement="right" title="Removed Visit# - ' + rowObject.MatchedSourceSystemKey + '">' + cellValue + '</div>';
                        }

                        var html = '<a href="javascript:;" data-toggle="tooltip" data-placement="right" title="' + cellValue + ' - View Transaction Details" class="view-transaction-details">' + cellValue + '</a>';
                        if (rowObject.VisitStateId === 9)
                            html = '<a href="javascript:;" data-toggle="tooltip" data-placement="right" title="This Visit is on hold. Review the support request history page if necessary, if you have access." class="glyphicon glyphicon-flag pull-right"></a>' + html;

                        return html;
                    }
                },
                {
                    label: 'Patient',
                    name: 'PatientName',
                    width: 96 + 39,
                    resizable: false
                },
                {
                    label: 'Visit Date',
                    name: 'DischargeDate',
                    width: 62,
                    resizable: false,
                    classes: 'text-center'
                },
                {
                    label: 'Visit Type',
                    name: 'BillingApplication',
                    resizable: false,
                    width: 50
                },
                {
                    label: 'Status',
                    name: 'VisitStateDerivedFullDisplayName',
                    width: 80,
                    resizable: false,
                    formatter: function (cellValue, options, rowObject) {
                        var style = ((rowObject.IsPastDue) ? ' style="color: red;" class="visitstatus3"' : '');
                        return '<a href="javascript:;" data-toggle="tooltip" data-placement="right" title="View Status History"' + style + '>' + cellValue + '</a>';
                    }
                },
                {
                    label: 'On<br />Hold',
                    name: 'BillingHold',
                    width: 40,
                    resizable: false,
                    classes: 'text-center',
                    formatter: function (cellValue, options, rowObject) {
                        var displayText = (rowObject.BillingHold ? 'Yes' : 'No');
                        return displayText;
                    }
                },
                {
                    label: 'Aging<br />Count',
                    name: 'AgingCount',
                    width: 40,
                    resizable: false,
                    classes: 'text-center',
                    formatter: function (cellValue, options, rowObject) {
                        if (rowObject.IsRemoved) {
                            return 'N/A';
                        }
                        return '<a href="javascript:;" data-toggle="tooltip" data-placement="right" title="View Aging Count">' + cellValue + '</a>';
                    }
                },
                {
                    label: 'Interest<br />Charged',
                    name: 'InterestAssessed',
                    width: 45,
                    resizable: false,
                    classes: 'text-right'
                    //,
                    //formatter: function (cellValue, options, rowObject) {
                    //    if (rowObject.IsRemoved) {
                    //        return '<div class="text-center">-</div>';
                    //    }
                    //    return '<a href="javascript:;"' +  '>' + cellValue + '</a>';
                    //}
                },
                {
                    label: 'Interest<br />Paid',
                    name: 'InterestPaid',
                    width: 45,
                    resizable: false,
                    classes: 'text-right'
                    //,
                    //formatter: function (cellValue, options, rowObject) {
                    //    if (rowObject.IsRemoved) {
                    //        return '<div class="text-center">-</div>';
                    //    }
                    //    return '<a href="javascript:;"' +  '>' + cellValue + '</a>';
                    //}
                },
                {
                    label: 'Total<br />Balance',
                    name: 'UnclearedBalance',
                    width: 59,
                    resizable: false,
                    classes: 'text-right',
                    formatter: function (cellValue, options, rowObject) {
                        if (rowObject.IsRemoved) {
                            return '<div class="text-center">-</div>';
                        }
                        return '<a href="javascript:;"' +  '>' + cellValue + '</a>';
                    }
                },
                {
                    label: 'Take Action',
                    name: '',
                    width: 60,
                    resizable: false,
                    sortable: false,
                    classes: 'text-center',
                    formatter: function (cellValue, options, rowObject) {
                        if ((!rowObject.UnmatchEnabled || rowObject.IsRemoved) && !rowObject.BalanceTransferEnabled) {
                            return 'N/A';
                        }

                        var html = '<select class="form-control">';
                        html += '<option value="">-</option>';

                        if (rowObject.UnmatchEnabled) {
                            html += '<option value="unmatch">Unmatch Visit</option>';
                        }

                        if (rowObject.BalanceTransferEnabled) {
                            html += '<option value="' + gridSetBalanceTransferOption + '">Mark For Balance Transfer</option>';
                        }

                        html += '</select>';

                        return html;
                    }
                }
            ]
        });

    };

    return guarantorAccountVisits;

})(
window.VisitPay.Common = window.VisitPay.Common || {},
    window.VisitPay.VisitTransactions   =   window.VisitPay.VisitTransactions   || {},
    window.VisitPay.VisitStateHistory = window.VisitPay.VisitStateHistory || {},
    window.VisitPay.VisitAgingHistory = window.VisitPay.VisitAgingHistory || {},
    window.VisitPay.VisitUnmatch = window.VisitPay.VisitUnmatch || {},
    window.VisitPay.VisitBalances = window.VisitPay.VisitBalances || {},
    jQuery,
    window.ko);
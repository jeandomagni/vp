﻿(function (searchGuarantors, $, ko) {

    var searchVpGuarantorsVm = function () {

        var self = this;

        self.HsGuarantorId = ko.observable('');
        self.VpGuarantorId = ko.observable('');
        self.FirstName = ko.observable('');
        self.LastName = ko.observable('');
        self.Ssn4 = ko.observable('');
        self.DateOfBirth = ko.observable('');
        self.UserNameOrEmailAddress = ko.observableArray([]);

        self.IsValid = ko.computed(function () {
            return ko.unwrap(self.HsGuarantorId).length > 0 ||
                ko.unwrap(self.VpGuarantorId).length > 0 ||
                ko.unwrap(self.FirstName).length > 0 ||
                ko.unwrap(self.LastName).length > 0 ||
                ko.unwrap(self.Ssn4).length > 0 ||
                ko.unwrap(self.UserNameOrEmailAddress).length > 0 ||
                ko.unwrap(self.DateOfBirth).length > 0;

        });
        self.RunResults = ko.observable(false);
        self.ChangeNotifications = function () {

            var setRun = function () {
                self.RunResults(true);
            };

            self.HsGuarantorId.subscribe(setRun);
            self.VpGuarantorId.subscribe(setRun);
            self.FirstName.subscribe(setRun);
            self.LastName.subscribe(setRun);
            self.Ssn4.subscribe(setRun);
            self.UserNameOrEmailAddress.subscribe(setRun);
            self.DateOfBirth.subscribe(setRun);

        };

        return self;
    };

    searchGuarantors.SearchVpGuarantors = (function (data) {

        $(document).ready(function () {

            var search = new window.VisitPay.SearchGuarantors.Main(searchVpGuarantorsVm, data.urls);
            search.init();

        });

    });


})(window.VisitPay.SearchGuarantors = window.VisitPay.SearchGuarantors || {}, jQuery, window.ko);
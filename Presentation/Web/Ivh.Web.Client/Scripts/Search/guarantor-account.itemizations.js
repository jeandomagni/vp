﻿window.VisitPay.GuarantorAccountItemizations = (function (common, $, ko) {

    var guarantorAccountItemizations = {};
    var filterViewModel = function () {

        var self = this;

        self.DateRange = ko.observable('');
        self.DateRangeFrom = ko.observable('');
        self.DateRangeTo = ko.observable('');
        self.FacilityCode = ko.observable('');
        self.PatientName = ko.observable('');
        self.VisitPayUserId = ko.observable();
        self.CurrentVisitPayUserId = ko.observable();

        self.RunResults = ko.observable(false);
        self.ChangeNotifications = function () {

            var setRun = function () {
                self.RunResults(true);
            };

            self.DateRange.subscribe(setRun);
            self.DateRangeFrom.subscribe(setRun);
            self.DateRangeTo.subscribe(setRun);
            self.FacilityCode.subscribe(setRun);
            self.PatientName.subscribe(setRun);

        };

    };

    var hasInitialized = false;
    var model;

    var bindFilters = function () {

        $('#jqGridFilterItemizations').find('[data-toggle="tooltip"]').tooltip({ animation: false, container: 'body' });

        // prevent focus on validate failure to prevent calendar from opening
        $('#jqGridFilterItemizations').on('invalid-form.validate', function (form, validator) {
            validator.settings && (validator.settings.focusInvalid = false);
        });

        //
        $('#section-itemizations .input-group.date').each(function () {
            var element = $(this);
            element.datepicker({
                clearBtn: true,
                format: 'mm/dd/yyyy',
                orientation: 'top left',
                autoclose: true
            }).on('hide', function () {
                if ($('#jqGridFilterItemizations').valid())
                    common.ResetUnobtrusiveValidation($('#jqGridFilterItemizations'));
            });
        });

    };
    var resetModel = function () {

        // reset viewmodel
        model.DateRange('');
        model.DateRangeFrom('');
        model.DateRangeTo('');
        model.FacilityCode('');
        model.PatientName('');

        model.RunResults(false);

        common.ResetUnobtrusiveValidation($('#jqGridFilterItemizations'));
    };
    var setRecordCount = function (count) {
        guarantorAccountItemizations.OnDataLoaded(count);
    };

    guarantorAccountItemizations.Setup = function(currentVisitPayUserId, filteredVisitPayUserId) {

        var promise = $.Deferred();
        var uid = filteredVisitPayUserId || currentVisitPayUserId;

        $.get('/Itemization/ItemizationsFilter?visitPayUserId=' + uid).then(function(html) {

            $('#jqGridFilterItemizations').html(html);
            ko.cleanNode($('#jqGridFilterItemizations')[0]);

            if (!model) {
                model = new filterViewModel();
                model.ChangeNotifications();
            }

            ko.applyBindings(model, $('#jqGridFilterItemizations')[0]);

            model.CurrentVisitPayUserId(currentVisitPayUserId);
            model.VisitPayUserId(uid);

            bindFilters();
            resetModel();

            $.jgrid.gridUnload('#jqGridItemizations');

            hasInitialized = false;

            promise.resolve();
        });

        $(document).on('visitUnmatchActionComplete', function () {
            var grid = $('#jqGridItemizations');
            $.extend(grid.jqGrid('getGridParam', 'postData'), { model: ko.toJS(model) });
            grid.setGridParam({ page: 1 });
            grid.sortGrid(grid.jqGrid('getGridParam', 'sortname'), true, grid.jqGrid('getGridParam', 'sortorder'));
        });

        return promise;

    };
    guarantorAccountItemizations.OnDataLoaded = function () { };
    guarantorAccountItemizations.GetInitialCount = function () {

        //
        var deferred = $.Deferred();

        //
        resetModel();

        //
        $.post('/Itemization/ItemizationTotals', { model: ko.toJS(model) }, function (data) {
            setRecordCount(data);
            deferred.resolve();
        }, 'json');

        //
        return deferred;

    };
    guarantorAccountItemizations.Initialize = function () {

        if (hasInitialized)
            return;
        else {
            $.blockUI();
            hasInitialized = true;
        }

        //
        var grid = $('#jqGridItemizations');
        var gridDefaultSortOrder = 'desc';
        var gridDefaultSortName = 'DischargeDate';

        //
        var reloadGrid = function () {
            $.extend(grid.jqGrid('getGridParam', 'postData'), { model: ko.toJS(model) });
            grid.setGridParam({ page: 1 });
            grid.sortGrid(gridDefaultSortName, true, gridDefaultSortOrder);
        };

        //
        $('#jqGridFilterItemizations').on('submit', function (e) {
            e.preventDefault();
            if ($(this).valid()) {
                $.blockUI();
                reloadGrid();
            };
        });
        $('#btnGridFilterItemizationsReset').on('click', function () {
            $.blockUI();
            resetModel();
            reloadGrid();
        });
        
        //
        resetModel();

        //
        grid.jqGrid({
            url: '/Itemization/Itemizations',
            autowidth: false,
            jsonReader: { root: 'Results' },
            postData: { model: ko.toJS(model) },
            pager: 'jqGridPagerItemizations',
            sortname: gridDefaultSortName,
            sortorder: gridDefaultSortOrder,
            loadComplete: function (data) {

                var gridContainer = grid.parents('.ui-jqgrid:eq(0)');
                var gridNoRecords = $('#jqGridNoRecordsItemizations');

                if (data.Results != null && data.Results.length > 0) {

                    gridContainer.show();
                    gridNoRecords.removeClass('in');

                    grid.find('.jqgrow').each(function () {

                        var rowData = data.Results[$(this).index() - 1];

                        $(this).off('click');
                        $(this).on('click', 'td:last > a', function () {

                            $.blockUI();

                            $.get('/Itemization/ItemizationDownloadModal', function (cmsVersion) {

                                $.unblockUI();

                                common.ModalGenericConfirmation(cmsVersion.ContentTitle, cmsVersion.ContentBody, 'Continue Download', 'Cancel').then(function () {

                                    window.open('/Itemization/ItemizationDownload/' + rowData.VisitFileStoredId + '?visitPayUserId=' + model.VisitPayUserId());

                                });

                            });
                        });

                    });

                } else {

                    gridContainer.hide();
                    gridNoRecords.addClass('in');

                }

                setRecordCount(data.records);
                model.RunResults(false);
                $.unblockUI();

            },
            gridComplete: function () {

                grid.find('[data-toggle="tooltip"]').tooltip({ animation: false, container: 'body' });

                $('#jqGridPagerItemizations').find('#input_jqGridPager').find('input').off('change').on('change', function () {
                    grid.jqGrid().trigger('reloadGrid', [{ page: $(this).val() }]);
                });

                var gridContainer = grid.parents('.ui-jqgrid:eq(0)');
                gridContainer.addClass('visible');

            },
            colModel: [
                { label: 'Date', name: 'DischargeDate', width: 130, resizable: false },
                {
                    label: 'Encounter #', name: 'VisitSourceSystemKeyDisplay', width: 243, resizable: false, sortable: false, formatter: function (cellValue, options, rowObject) {
                        if (rowObject.IsRemoved) {
                            return '<div data-toggle="tooltip" data-placement="right" title="Removed Encounter# - ' + rowObject.MatchedVisitSourceSystemKeyDisplay + '">' + cellValue + '</div>';
                        }

                        return cellValue;
                    }
                },
                { label: 'Patient', name: 'PatientName', width: 244, resizable: false, sortable: false },
                { label: 'Facility', name: 'FacilityCode', resizable: false, sortable: false, width: 244 },
                {
                    label: 'Take Action', name: 'VisitFileStoredId', width: 130, resizable: false, sortable: false, classes: 'text-center', formatter: function (cellValue, options, rowObject) {
                        if (rowObject.IsRemoved) {
                            return 'N/A';
                        }
                        return '<a href="javascript:;" title="Download">download</a>';
                    }
                }
            ]
        });

    };

    return guarantorAccountItemizations;


})(
    window.VisitPay.Common = window.VisitPay.Common || {},
    jQuery,
    window.ko);
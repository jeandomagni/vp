﻿window.VisitPay.PaymentDueDayHistory = (function (common, $) {

    var paymentDueDayHistory = {};

    paymentDueDayHistory.Initialize = function (guarantorVisitPayUserId) {

        //
        var scope = $('#modalPaymentDueDayHistory');
        scope.find('.modal').css('position', 'absolute');

        //
        var grid = scope.find('#jqGridPaymentDueDayHistory');
        var gridDefaultSortName = 'InsertDate';
        var gridDefaultSortOrder = 'desc';

        //
        grid.jqGrid({
            url: '/Search/GetPaymentDueDayHistory',
            postData: {
                guarantorId: guarantorVisitPayUserId
            },
            jsonReader: { root: 'PaymentDueDayHistory' },
            pager: 'jqGridPaymentDueDayHistoryPager',
            sortname: gridDefaultSortName,
            sortorder: gridDefaultSortOrder,
            autowidth: false,
            height: 1200, // overridden in css, this just makes the scrollbar appear correctly
            loadComplete: function (data) {

                var gridContainer = grid.parents('.ui-jqgrid:eq(0)');
                var gridNoRecords = $('#jqGridPaymentDueDayHistoryNoRecords');

                if (data.PaymentDueDayHistory != null && data.PaymentDueDayHistory.length > 0) {

                    gridContainer.show();
                    gridNoRecords.removeClass('in');

                    grid.closest('.ui-jqgrid-bdiv').scrollTop(0);

                } else {

                    gridContainer.hide();
                    gridNoRecords.addClass('in');

                    grid.closest('.ui-jqgrid-bdiv').scrollTop(0);
                }

                $.unblockUI();

            },
            gridComplete: function () {

                $('#jqGridPaymentDueDayHistoryPager').find('#input_jqGridPager').find('input').off('change').on('change', function () {
                    grid.jqGrid().trigger('reloadGrid', [{ page: $(this).val() }]);
                });

                var gridContainer = grid.parents('.ui-jqgrid:eq(0)');
                gridContainer.addClass('visible');

            },
            colModel: [
                { label: 'Change Date', name: 'InsertDate', width: 265, resizable: false },
                { label: 'Changed By', name: 'InsertedBy', sortable: false, width: 200, resizable: false },
                { label: 'Changed From', name: 'OldPaymentDueDay', sortable: false, width: 200, resizable: false },
                { label: 'Changed To', name: 'PaymentDueDay', sortable: false, width: 200, resizable: false }
            ]
        });

    };

    return paymentDueDayHistory;

})(window.VisitPay.Common = window.VisitPay.Common || {}, jQuery);
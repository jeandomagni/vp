﻿(function (searchGuarantors, clientSettings, common, $, ko) {


    searchGuarantors.Main = (function (filterViewModel, urls, columnModel) {

        var emulateUser = function(vpGuarantorId) {
            if (vpGuarantorId) {
                $.post('/Search/BeginEmulateUser', { VpGuarantorId: vpGuarantorId }, function(url) {
                    window.location.href = url;
                });
            }
        };

        var makePayment = function(vpGuarantorId, currentVisitPayUserId) {
            window.location.href = '/Payment/ArrangePayment?VpGuarantorId=' + vpGuarantorId + '&currentVisitPayUserId=' + currentVisitPayUserId;
        };

        var requestPassword = function(vpGuarantorId) {
            if (vpGuarantorId) {
                common.ModalGenericConfirmationMd(
                    'Request Password',
                    '<div class="request-password"><p>' +
                    'Send a new, temporary password for this ' + clientSettings.ClientBrandName + ' account<br />to the notification email address on file.' +
                    '<br />' +
                    '<br />' +
                    'The temporary password will expire after ' + clientSettings.GuarantorTempPasswordExpLimitInHours + ' hours.' +
                    '</p></div>',
                    'Submit',
                    'Cancel',
                    'Your action is logged for audit purposes.').done(function() {
                    $.blockUI();

                    $.post('/Search/RequestPassword/', { guarantorId: vpGuarantorId }, function(result) {
                        /* result contains message from server including success and failure.*/
                        $.unblockUI();
                    });
                });
            }
        };

        var initDatePicker = function() {
            var d = new Date();
            var year = d.getFullYear();

            var dateOfBirthTextbox = $('#DateOfBirth');

            var maxYears = $(dateOfBirthTextbox).data('val-agedaterange-maximumage');
            var minYears = $(dateOfBirthTextbox).data('val-agedaterange-minimumage');

            var minYear = year - maxYears;
            var maxYear = year - minYears;

            $(dateOfBirthTextbox).combodate({
                minYear: minYear,
                maxYear: maxYear,
                firstItem: 'none',
                customClass: 'combodateDropdowns'
            });

            // firstItem: "name" option adds lower cased month, day, year- this is a hack for now
            $(".combodate .month option[label='Month']").remove();
            $(".combodate .day option[label='Day']").remove();
            $(".combodate .year option[label='Year']").remove();
            
            $('.combodate .month').prepend("<option selected='selected' label='Month'>Month</option>");
            $('.combodate .day').prepend("<option selected='selected' label='Day'>Day</option>");
            $('.combodate .year').prepend("<option selected='selected' label='Year'>Year</option>");
            
            //option selected='selected' not actually setting the selected index visually in IE VPNG-21792
            $('.combodate .month').prop("selectedIndex", 0);
            $('.combodate .day').prop("selectedIndex", 0);
            $('.combodate .year').prop("selectedIndex", 0);
        };

        //combodate doesnt play well with jquery validate. this should be roughly equivalent
        var validateDatePicker = function () {

            var datePicker = $('#DateOfBirth');

            if (datePicker.attr('aria-required')) {
                var date = datePicker.combodate('getValue');

                if (date == null || date.length < 1) {

                    $('.combodate').children('select').each(function () {
                        $(this).addClass('input-validation-error');
                    });

                    return false;
                }
            }

            return true;

        };

        var gridDefaultSortOrder = 'asc';

        var gridDefaultSortName = 'Name';

        var maxRowsToAllowedToReturn = 200;

        var moreThanMaxWarning = 'More than ' + maxRowsToAllowedToReturn + ' returned. Please refine your search';

        var showMoreThanMaxWarning = function(refineSearchMessageContainer, show) {
            if (show === true) {
                $(refineSearchMessageContainer).html(moreThanMaxWarning);
                $(refineSearchMessageContainer).show();
            } else {
                $(refineSearchMessageContainer).hide();
            }
        };

        var bindGrid = function(grid, json, columnModel) {
            grid.jqGrid({
                datatype: 'local',
                data: json,
                sortname: gridDefaultSortName,
                sortorder: gridDefaultSortOrder,
                beforeSelectRow: function (rowid, e) {
                    //Need to do this to prevent clicking on a row from changing hover state
                    return false;
                },
                onSortCol: function(index, idxcol, sortorder) {
                    var $icons = $(this.grid.headers[idxcol].el).find(">div.ui-jqgrid-sortable>span.s-ico");
                    if (this.p.sortorder === 'asc') {
                        //$icons.find('>span.ui-icon-asc').show();
                        $icons.find('>span.ui-icon-asc')[0].style.display = "";
                        $icons.find('>span.ui-icon-desc').hide();
                    } else {
                        //$icons.find('>span.ui-icon-desc').show();
                        $icons.find('>span.ui-icon-desc')[0].style.display = "";
                        $icons.find('>span.ui-icon-asc').hide();
                    }
                },
                loadonce: true,
                rowNum: maxRowsToAllowedToReturn,
                loadComplete: function(data) {

                    var gridContainer = grid.parents('.ui-jqgrid:eq(0)');
                    var gridNoRecords = $('#jqGridNoRecords');
                    if (data != null && data.rows.length > 0) {

                        gridContainer.show();
                        gridNoRecords.removeClass('in');

                        //// building block for modal actions
                        //// action links in popup
                        //grid.find('.jqgrow').each(function () {
                        //    $(this).on('click', 'td:eq(7) a', function (e) {
                        //        e.preventDefault();
                        //        var rowData = data.rows[$(this).parents('tr').index() - 1];

                        //        if ($(this).hasClass('payment')) {
                        //            makePayment(rowData.VpGuarantorId, rowData.VisitPayUserId);
                        //        }
                        //        else if ($(this).hasClass('requestPassword')) {
                        //            requestPassword(rowData.VpGuarantorId);
                        //        }
                        //        else if ($(this).hasClass('emulate')) {
                        //            emulateUser(rowData.VpGuarantorId);
                        //        }
                        //        return;
                        //    });
                        //});

                        // actions in select control
                        grid.find('.jqgrow').each(function() {

                            $(this).on('change', 'td:eq(7) select', function(e) {
                                e.preventDefault();
                                var rowData = data.rows[$(this).parents('tr').index() - 1];
                                if ($(this).val() === '')
                                    return;

                                if ($(this).val() === 'payment') {
                                    makePayment(rowData.VpGuarantorId, rowData.VisitPayUserId);
                                } else if ($(this).val() === 'requestPassword') {
                                    requestPassword(rowData.VpGuarantorId);
                                } else if ($(this).val() === 'emulate') {
                                    emulateUser(rowData.VpGuarantorId);
                                }

                                $(this).val('');
                            });
                        });

                    } else {

                        gridContainer.hide();
                        gridNoRecords.addClass('in');

                    }

                },
                gridComplete: function() {

                    grid.find('[data-toggle="tooltip"]').tooltip({ animation: false, container: 'body' });

                    $('#jqGridPager').find('#input_jqGridPager').find('input').off('change').on('change', function() {
                        grid.jqGrid().trigger('reloadGrid', [{ page: $(this).val() }]);
                    });

                    var gridContainer = grid.parents('.ui-jqgrid:eq(0)');
                    gridContainer.addClass('visible');

                },
                colModel: columnModel || [
                    {
                        label: 'NAME',
                        name: 'Name',
                        width: 150,
                        resizable: false,
                        title: false,
                        formatter: function(cellValue, options, rowObject) {
                            if (rowObject.LockoutMessage != null && rowObject.LockoutMessage != '') {
                                return '<a class="bold-link" href="/Search/GuarantorAccount/' + rowObject.VpGuarantorId + '" data-vpuid="' + rowObject.VisitPayUserId + '">' + cellValue + '</a>  <em title="' + rowObject.LockoutMessage + '" class="glyphicon glyphicon-lock"></em>';
                            } else {
                                return '<a class="bold-link" href="/Search/GuarantorAccount/' + rowObject.VpGuarantorId + '" data-vpuid="' + rowObject.VisitPayUserId + '">' + cellValue + '</a>';
                            }
                        }
                    },
                    {
                        label: clientSettings.HsGuarantorPatientIdentifier.toUpperCase(),
                        name: 'SourceSystemKeys',
                        width: 112,
                        resizable: false,
                        formatter: function(cellValue, options, rowObject) {
                            return '<span title="' + rowObject.SourceSystemKeys + '"><span>' + rowObject.SourceSystemKeys + '</span>';
                        }
                    },
                    {
                        label: clientSettings.VpGuarantorPatientIdentifier.toUpperCase(),
                        name: 'VpGuarantorId',
                        width: 103,
                        resizable: false,
                        formatter: function(cellValue, options, rowObject) {
                            if (rowObject.IsHsGuarantor === true) {
                                return '';
                            } else {
                                return rowObject.VpGuarantorId;
                            }
                        }
                    },
                    { label: 'LAST 4 SSN', name: 'Ssn4', width: 104, resizable: false },
                    {
                        label: 'STATUS',
                        name: 'VpGuarantorStatus',
                        width: 100,
                        resizable: false,
                        formatter: function(cellValue, options, rowObject) {
                            return rowObject.VpGuarantorStatusDisplay;
                        }
                    },
                    {
                        label: 'DOB',
                        name: 'DateOfBirth',
                        resizable: false,
                        width: 120,
                        sorttype: 'date',
                        formatter: 'date',
                        formatoptions: { srcformat: 'm/d/Y', newformat: 'm/d/Y' }
                    },
                    //{ label: 'USERNAME/EMAIL', name: 'UserNameAndEmailAddress', width: 191, resizable: false },
                    {
                        label: 'LAST STATEMENT',
                        name: 'LastStatementBalance',
                        width: 140,
                        resizable: false,
                        align: 'right',
                        formatter: function(cellValue, options, rowObject) {

                            if (rowObject.IsStatemented === true) {

                                //var className = ('last-statement ' + ((rowObject.LastStatementIsGracePeriod === true) ? 'grace-period' : (rowObject.IsAwaitingStatement === true) ? 'pending-statement' : '')).trim();
                                var className = '';
                                var title = rowObject.LastStatementBalance + ' - ' + rowObject.LastStatementDateDisplay + ((rowObject.IsAwaitingStatement === true) ? ': Pending Statement' : ': Payment Due');
                                return '<span class="' + className + '" title="' + title + '">' + rowObject.LastStatementBalance
                                    //+ ' ' + rowObject.LastStatementDateDisplay 
                                    + '</span>';

                            } else {
                                return 'Never Statemented';
                            }
                        }
                    },
                    {
                        label: '', //ACTION
                        name: '',
                        width: 150,
                        resizable: false,
                        sortable: false,
                        classes: 'text-center',
                        title: false,
                        formatter: function(cellValue, options, rowObject) {
                            
                            var html = '<select class="form-control grid-select">';
                            html += '<option value="">Take Action</option>';

                            if (rowObject.VpGuarantorTypeEnum !== 2) { // 2 = Offline
                                html += '<option value="payment">Make a Payment</option>';

                                html += '<option value="requestPassword">Reset Password</option>';

                                html += '<option value="emulate">Emulate User (view only)</option>';
                            }

                            html += '</select>';

                            //// building blocks for modal actions
                            //var html = '<div class="action-tab">';
                            //html += '<a href="#" class="payment">Make a Payment</a>';
                            //html += '<br/>' + '<a href="#" class="requestPassword">Reset Password</a>';
                            //html += '<br/>' + '<a href="#" class="emulate">Emulate User (view only)</a>';
                            //html += '</div>';

                            return html;
                        }
                    }
                ]
            });

            $('#gbox_' + $.jgrid.jqID(grid[0].id) +
                ' tr.ui-jqgrid-labels th.ui-th-column>div.ui-jqgrid-sortable>span.s-ico').each(function() {

                $(this).find('>span.ui-icon-' +
                    (grid.jqGrid('getGridParam', 'sortorder') === 'desc' ? 'asc' : 'desc')).hide();
            });
            grid.jqGrid('clearGridData');
            grid.jqGrid('setGridParam', { data: json });
            grid.trigger('reloadGrid');
        };

        return {
            init: function () {

                var model = new filterViewModel();
                ko.applyBindings(model, $('#jqGridFilter')[0]);
                model.ChangeNotifications();

                var grid = $('#jqGrid');

                var executeSearch = function () {

                    if (model.IsValid()) {
                        $('#jqGridInitialMessage').addClass('collapse');
                        $.blockUI();
                        $.post(urls.search || '/Search/SearchGuarantors', {
                            model: ko.toJS(model),
                            page: 1,
                            rows: maxRowsToAllowedToReturn,
                            sidx: grid.jqGrid('getGridParam', 'sortname'),
                            sord: grid.jqGrid('getGridParam', 'sortorder')
                        }, function (result) {
                            $.unblockUI();
                            bindGrid(grid, result.Guarantors, columnModel);
                            showMoreThanMaxWarning($('#refineSearchMessage'), result.records > maxRowsToAllowedToReturn);
                            //piwik
                            var fields = '# of Results: ' + result.Guarantors.length + ' | Fields: ' + piwikTrackFilterFields();
                            window._paq.push(['trackEvent', 'Guarantor Search', 'Search', fields]);
                            model.RunResults(false);
                        }, 'json');

                    } else {
                        $('#jqGridNoRecords').removeClass('in');
                        $('#jqGridInitialMessage').removeClass('collapse');
                        grid.parents('.ui-jqgrid:eq(0)').hide();
                    }

                };

                var piwikTrackFilterFields = function () {
                    var filterFields = [];
                    var fieldsToIgnore = ['IsValid', 'ChangeNotifications', 'RunResults'];
                    var jsModel = ko.toJS(model);
                    $.each(jsModel, function (key, value) {
                        if (typeof value != 'undefined' && value && fieldsToIgnore.indexOf(key) < 0) {
                            filterFields.push(key);
                        };
                    });
                    return filterFields.join();
                };

                var resetFilters = function () {
                    model.HsGuarantorId('');
                    model.VpGuarantorId('');
                    model.FirstName('');
                    model.LastName('');
                    model.Ssn4('');
                    model.UserNameOrEmailAddress('');
                    model.DateOfBirth('');
                    initDatePicker();
                    model.RunResults(false);
                    common.ResetUnobtrusiveValidation($('#jqGridFilter'));
                };

                //
                $('#jqGridFilter').on('submit', function (e) {
                    e.preventDefault();

                    if (validateDatePicker() && $('#jqGridFilter').valid()) {
                        executeSearch();
                    }
                });

                $('#btnGridFilterReset').on('click', function () {
                    resetFilters();
                    executeSearch();
                });

                $('#btnGuarantorsExport').on('click', function (e) {

                    e.preventDefault();

                    if ($('#jqGridFilter').valid()) {
                        $.post('/Search/SearchGuarantorsExport', {
                            model: ko.toJS(model),
                            sidx: grid.jqGrid('getGridParam', 'sortname'),
                            sord: grid.jqGrid('getGridParam', 'sortorder')
                        }, function (result) {
                            if (result.Result === true)
                                window.location.href = result.Message;
                        }, 'json');
                    }

                });

                //
                resetFilters();

                $('#section-searchguarantors').on('click', '.jqgrow a [data-vpuid]', function (e) {

                    e.preventDefault();
                    $.blockUI();

                    var href = $(this).attr('href');
                    $.post('/GuarantorFilter/ClearSelection', { currentVisitPayUserId: $(this).data('vpuid') }, function () {
                        window.location.href = href;
                    });

                });

                initDatePicker();

                //Set initial focus to guarantor ID field
                $('#HsGuarantorId').focus();
            }
        };

    });

})(window.VisitPay.SearchGuarantors = window.VisitPay.SearchGuarantors || {}, window.VisitPay.ClientSettings = window.VisitPay.ClientSettings || {}, window.VisitPay.Common = window.VisitPay.Common || {}, jQuery, window.ko);
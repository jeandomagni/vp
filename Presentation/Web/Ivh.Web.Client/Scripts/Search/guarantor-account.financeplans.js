﻿window.VisitPay.GuarantorAccountFinancePlans = (function (financePlanVisits, financePlanInterestRateHistory,financePlanLog, financePlanBucketHistory, financePlanCancel, common, guarantorFilter, interestReallocate, $, ko) {

    var guarantorAccountFinancePlans = {};

    var vpGuarantorTypeOffline = '2';
    
    var filterViewModel = function () {

        var self = this;

        self.FinancePlanStatusIds = ko.observableArray([]);
        self.LastXRange = ko.observable('');
        self.OriginationDateRangeFrom = ko.observable('');
        self.OriginationDateRangeTo = ko.observable('');
        self.VisitPayUserId = ko.observable();
        self.CurrentVisitPayUserId = ko.observable();
        self.VpGuarantorId = ko.observable();
        self.VpGuarantorTypeId = ko.observable();

        self.IsOfflineGuarantor = ko.computed(function() {
            return ko.unwrap(self.VpGuarantorTypeId) === vpGuarantorTypeOffline;
        });

        self.RunResults = ko.observable(false);
        self.ChangeNotifications = function () {

            var setRun = function () {
                self.RunResults(true);
            };

            self.FinancePlanStatusIds.subscribe(setRun);
            self.LastXRange.subscribe(setRun);
            self.OriginationDateRangeFrom.subscribe(setRun);
            self.OriginationDateRangeTo.subscribe(setRun);

        };

    };

    var hasInitialized = false;
    var model;
    var modelToJs = function () {
        var toJs = ko.toJS(model);
        return toJs;
    };
    
    var bindFilters = function () {

        // visit status tooltip (because it's using an html element for the content)
        $('#aFinancePlanStatus').tooltip({
            placement: 'right',
            html: true,
            title: function () {
                return $('#tooltip-financePlanStatuses').html();
            }
        });

        // prevent focus on validate failure to prevent calendar from opening
        $('#jqGridFilterFinancePlans').on('invalid-form.validate', function (form, validator) {
            validator.settings && (validator.settings.focusInvalid = false);
        });

        //
        $('#section-financeplans .input-group.date').each(function () {
            var element = $(this);
            element.datepicker({
                clearBtn: true,
                format: 'mm/dd/yyyy',
                orientation: 'top left',
                autoclose: true
            }).on('hide', function () {
                if ($('#jqGridFilterFinancePlans').valid())
                    common.ResetUnobtrusiveValidation($('#jqGridFilterFinancePlans'));
            });
        });

        var financePlanStatusSelect = $('select#FinancePlanStatusIds');
        financePlanStatusSelect.multiselect({
            enableClickableOptGroups: true,
            includeSelectAllOption: true,
            allSelectedText: 'All Statuses',
            nonSelectedText: 'All Statuses',
            nSelectedText: ' Statuses',
            numberDisplayed: 1,
            selectAllNumber: false,
            selectAllText: 'All Statuses',
            onChange: function () {

                model.FinancePlanStatusIds = new ko.observableArray([]);

                financePlanStatusSelect.find('option:selected').each(function () {
                    model.FinancePlanStatusIds.push($(this).val());
                });

                model.RunResults(true);

            }
        });

    };
    var resetModel = function() {

        model.FinancePlanStatusIds([1, 2, 3, 4, 9]);
        model.LastXRange('');
        model.OriginationDateRangeFrom('');
        model.OriginationDateRangeTo('');
        model.RunResults(false);

    };
    var resetFilters = function () {

        // reset viewmodel
        resetModel();

        // reset multiselect
        var financePlanStatusSelect = $('select#FinancePlanStatusIds');
        financePlanStatusSelect.find('option').prop('selected', false);

        ko.utils.arrayForEach(model.FinancePlanStatusIds(), function (financePlanStatusId) {
            $('select#FinancePlanStatusIds option[value="' + financePlanStatusId + '"]').prop('selected', true);
        });

        financePlanStatusSelect.multiselect('refresh');

        common.ResetUnobtrusiveValidation($('#jqGridFilterFinancePlans'));
    };
    var setRecordCount = function (count, containsPastDue) {
        guarantorAccountFinancePlans.OnDataLoaded(count, containsPastDue);
    };
    
    guarantorAccountFinancePlans.Setup = function (currentVisitPayUserId, filteredVisitPayUserId, vpGuarantorId, vpGuarantorTypeId) {
        
        if (!model) {
            model = new filterViewModel();
            ko.applyBindings(model, $('#jqGridFilterFinancePlans')[0]);
            model.ChangeNotifications();
        }
        
        model.CurrentVisitPayUserId(currentVisitPayUserId);
        model.VisitPayUserId(filteredVisitPayUserId || currentVisitPayUserId);
        model.VpGuarantorId(vpGuarantorId);
        model.VpGuarantorTypeId(vpGuarantorTypeId);

        bindFilters();
        resetModel();
        resetFilters();

        $.jgrid.gridUnload('#jqGridFinancePlans');

        $(document).on('visitUnmatchActionComplete', function () {
            setTimeout(function() {
                if (hasInitialized) {
                    var grid = $('#jqGridFinancePlans');
                    $.extend(grid.jqGrid('getGridParam', 'postData'), { model: ko.toJS(model) });
                    grid.setGridParam({ page: 1 });
                    grid.sortGrid(grid.jqGrid('getGridParam', 'sortname'), true, grid.jqGrid('getGridParam', 'sortorder'));
                }
            }, 1000);
        });

        hasInitialized = false;

    };
    guarantorAccountFinancePlans.OnDataLoaded = function() {};
    guarantorAccountFinancePlans.GetInitialCount = function () {

        //
        var deferred = $.Deferred();

        //
        resetModel();

        //
        $.post('/Search/FinancePlanTotals', { model: modelToJs() }, function (data) {
            setRecordCount(data.Total, data.PastDue > 0);
            deferred.resolve();
        }, 'json');

        //
        return deferred;

    };
    guarantorAccountFinancePlans.Initialize = function() {

        if (hasInitialized)
            return;
        else {
            $.blockUI();
            hasInitialized = true;
        }

        //
        var grid = $('#jqGridFinancePlans');
        var gridDefaultSortOrder = 'asc';
        var gridDefaultSortName = 'OriginationDate';

        //
        var reloadGrid = function () {
            $.extend(grid.jqGrid('getGridParam', 'postData'), { model: modelToJs() });
            grid.setGridParam({ page: 1 });
            grid.sortGrid(gridDefaultSortName, true, gridDefaultSortOrder);
        };

        var afterCancel = function (hasChanged) {

            if (hasChanged) {
                
                reloadGrid();

                $.get('/Search/GuarantorAlerts/' + parseInt($('#section-guarantoraccount #VpGuarantorId').val()), function(html) {
                    $('.action-required').replaceWith(html);
                });

                $(document).trigger('visits.reload');

            }

        };
        
        //
        $('#jqGridFilterFinancePlans').off('submit').on('submit', function (e) {
            e.preventDefault();
            if ($(this).valid()) {
                $.blockUI();
                reloadGrid();
            }
        });

        $('#btnGridFilterFinancePlansReset').off('click').on('click', function () {
            $.blockUI();
            resetFilters();
            reloadGrid();
        });

        $('#btnFinancePlansExport').off('click').on('click', function (e) {

            e.preventDefault();
            $.post('/Search/GuarantorFinancePlansExport', {
                model: modelToJs(),
                sidx: grid.jqGrid('getGridParam', 'sortname'),
                sord: grid.jqGrid('getGridParam', 'sortorder')
            }, function (result) {
                if (result.Result === true)
                    window.location.href = result.Message;
            }, 'json');

        });

        $(document).off('financeplans.reload').on('financeplans.reload', function () {
            reloadGrid();
        });

        //
        resetFilters();

        //
        grid.jqGrid({
            url: '/Search/GuarantorFinancePlans',
            jsonReader: { root: 'FinancePlans' },
            postData: { model: modelToJs() },
            pager: 'jqGridPagerFinancePlans',
            sortname: gridDefaultSortName,
            sortorder: gridDefaultSortOrder,
            footerrow: true,
            loadComplete: function (data) {

                var gridContainer = grid.parents('.ui-jqgrid:eq(0)');
                var gridNoRecordsInitial = $('#jqGridNoRecordsInitialFinancePlans');
                var gridNoRecords = $('#jqGridNoRecordsFinancePlans');

                if (data.FinancePlans != null && data.FinancePlans.length > 0) {

                    gridContainer.show();
                    gridNoRecords.removeClass('in');
                    gridNoRecordsInitial.removeClass('in');

                    // set footer
                    grid.jqGrid('footerData', 'set', { 'PrincipalPaidToDate': 'Current Balance Total<a href="javascript:;" class="glyphicon glyphicon-info-sign" data-toggle="tooltip" data-placement="top" title="This balance reflects active filter settings and reflects totals from all pages."></a>' }, false);
                    grid.jqGrid('footerData', 'set', { 'CurrentFinancePlanBalance': data.TotalCurrentBalance }, false);

                    var rowFooter = gridContainer.find('tr.footrow:eq(0)');
                    rowFooter.find('td:lt(6)').removeAttr('style').attr('class', 'cell-empty');
                    rowFooter.find('td:eq(6)').removeAttr('style').addClass('cell-label');
                    rowFooter.find('td:gt(7)').css('border-right-color', 'transparent');
                    rowFooter.find('[data-toggle="tooltip"]').tooltip({ animation: false, container: 'body' });

                    grid.off('click');
                    grid.find('.jqgrow').each(function() {

                        var rowData = data.FinancePlans[$(this).index() - 1];

                        $(this).off('click');

                        $(this).on('click', 'td:eq(0) > a', function() {

                            $.blockUI();

                            $.get('/Search/GuarantorFinancePlanVisits?visitPayUserId=' + model.VisitPayUserId() + '&financePlanId=' + rowData.FinancePlanId, {}, function(html) {

                                $('#modalFinancePlanVisitsContainer').html(html);
                                $('#modalFinancePlanVisitsContainer').find('.modal').modal();
                                financePlanVisits.Initialize(model.VisitPayUserId(), rowData);

                                $.unblockUI();

                            });

                        });

                        $(this).on('click', 'td:eq(1) > a', function () {

                            $.blockUI();

                            $.get('/FinancePlan/FinancePlanInterestRateHistory', {}, function (html) {

                                $('#modalFinancePlanInterestRateHistoryContainer').html(html);
                                $('#modalFinancePlanInterestRateHistoryContainer').find('.modal').modal();
                                financePlanInterestRateHistory.Initialize(model.VisitPayUserId(), rowData.FinancePlanId);

                                $.unblockUI();

                            });

                        });

                        $(this).on('click', 'td:eq(7) > a', function () {
                            financePlanLog.Initialize(model.VisitPayUserId(), rowData.FinancePlanId);
                            $.unblockUI();
                        });

                        $(this).on('change', 'td:eq(10) select', function (e) {
                            
                            var currentVisitPayUserId = guarantorFilter.FilterValue.CurrentVisitPayUserId;
                            var filterQs = currentVisitPayUserId != undefined ? '&currentVisitPayUserId=' + currentVisitPayUserId : '';
                            if ($(this).val() === '1') {
                                if (rowData.IsOfflineGuarantor === true) {
                                    window.location.href = '/Offline/CreateFinancePlan/?vpGuarantorId=' + model.VpGuarantorId();
                                } else {
                                    window.location.href = '/Payment/ArrangePayment?VpGuarantorId=' + model.VpGuarantorId() + '&PaymentOption=FinancePlan' + filterQs;
                                }
                            } else if ($(this).val() === '2') {
                                financePlanCancel.Initialize(ko.unwrap(model.VisitPayUserId), rowData.FinancePlanId).done(afterCancel);
                            } else if ($(this).val() === '3') {
                                window.location.href = '/FinancePlan/ReconfigureTerms/' + rowData.FinancePlanId + '?VpGuarantorId=' + model.VpGuarantorId() + filterQs;
                            } else if ($(this).val() === '5') {
                                $.blockUI();
                                interestReallocate.Initialize(model.VpGuarantorId(), rowData.FinancePlanId, function () {reloadGrid()}).done(function() {
                                    $.unblockUI();
                                });
                            }
                            $(this).val('');

                        });

                        $(this).on('click', 'td a[name=bucket]', function() {

                            financePlanBucketHistory.Initialize(ko.unwrap(model.VisitPayUserId), rowData.FinancePlanId);

                        });

                    });

                } else {

                    gridContainer.hide();
                    gridNoRecords.removeClass('in');
                    gridNoRecordsInitial.removeClass('in');
                    gridNoRecords.addClass('in');

                }

                setRecordCount(data.records, grid.find('.financeplan-pastdue').length > 0);
                model.RunResults(false);

                $.unblockUI();

            },
            gridComplete: function () {

                grid.find('[data-toggle="tooltip"]').tooltip({ animation: false, container: 'body' });

                $('#jqGridPagerFinancePlans').find('#input_jqGridPager').find('input').off('change').on('change', function () {
                    grid.jqGrid().trigger('reloadGrid', [{ page: $(this).val() }]);
                });

                var gridContainer = grid.parents('.ui-jqgrid:eq(0)');
                gridContainer.addClass('visible');

            },
            colModel: [
                {
                    label: 'Effective Date<br/>(view visits)',
                    name: 'OriginationDate',
                    width: 100,
                    resizable: false,
                    formatter: function (cellValue) {
                        return '<a href="javascript:;" title="View Finance Plan Visits" data-placement="right" data-toggle="tooltip">' + cellValue + '</a>';
                    }
                },
                {
                    label: 'Interest Rate<br/>(view history)', name: 'InterestRate', width: 87, resizable: false, sortable: false, classes: 'text-center', formatter: function (cellValue) {
                        return '<a href="javascript:;" name="history" title="View Finance Plan History" data-placement="right" data-toggle="tooltip">' + cellValue + '</a>';
                    }
                },
                {
                    label: 'Status',
                    name: 'FinancePlanStatusFullDisplayName',
                    width: 84,
                    resizable: false,
                    formatter: function(cellValue, options, rowObject) {
                        if (rowObject.IsPastDue === true || rowObject.IsUncollectable)
                            return '<span style="color: red;" class="financeplan-pastdue">' + cellValue + '</span>';
                        if (rowObject.IsPending === true)
                            return '<span style="color: green;" class="financeplan-pending">' + cellValue + '</span>';

                        return cellValue;
                    }
                },
                { label: 'Original<br />Balance', name: 'OriginatedFinancePlanBalance', width: 69, resizable: false, classes: 'text-right', formatter: function(cellValue, options, rowObject) {
                    if (cellValue.toUpperCase() === 'PENDING') {
                        return '<span style="display: block; text-align: center;">' + cellValue + '</span>';
                    }

                    return cellValue;
                }},
                { label: 'Interest<br />Charged', name: 'InterestAssessed', width: 72, resizable: false, classes: 'text-right' },
                { label: 'Interest<br />Paid', name: 'InterestPaidToDate', width: 60, resizable: false, classes: 'text-right' },
                { label: 'Principal<br />Paid', name: 'PrincipalPaidToDate', width: 60, resizable: false, classes: 'text-right' },
                // Finance Plan Log
                { 
                    label: 'Pay Off<br />Balance', 
                    name: 'CurrentFinancePlanBalance', 
                    width: 75, 
                    resizable: false, 
                    formatter: function (cellValue) {
                        return '<a href="javascript:;" title="View Finance Plan History" data-placement="right" data-toggle="tooltip">' + cellValue + '</a>';
                    },
                    classes: 'text-right' },
                { label: 'Monthly<br />Amount<br />Due', name: 'PaymentAmount', width: 64, resizable: false, classes: 'text-right' },
                { label: 'Current<br/>Bucket', name: 'CurrentBucket', width: 60, resizable: false, classes: 'text-center', formatter: function(cellValue, options, rowObject) {
                    return '<a name="bucket" href="javascript:;" title="View Finance Plan Bucket History" data-placement="right" data-toggle="tooltip">' + cellValue + '</a>';
                }},
                {
                    label: 'Take Action', name: '', width: 75, resizable: false, sortable: false, formatter: function (cellValue, options, rowObject) {

                        var na = '<span style="display: block; text-align: center;">N/A</span>';

                        if (rowObject.IsManaged === true || rowObject.IsGuarantorCanceled === true) {
                            return na;
                        }

                        var opts = [];
                        opts.push({ value: '', text: 'Select' });
                        //Probably should limit this to FPs that have interest assessed
                        opts.push({ value: '5', text: 'Refund Interest' });


                        if (rowObject.FinancePlanStatus === 1 || rowObject.FinancePlanStatus === 2) {

                            opts.push({ value: '1', text: 'Edit' });
                            opts.push({ value: '2', text: 'Cancel' });

                        } else if (rowObject.FinancePlanStatus === 3 || rowObject.FinancePlanStatus === 4 || rowObject.FinancePlanStatus === 9) {

                            if (rowObject.CanReconfigure) {
                                if (rowObject.ReconfiguredFinancePlanId && parseInt(rowObject.ReconfiguredFinancePlanId) > 0)
                                    opts.push({ value: '3', text: 'View/Edit Reconfigured Terms' });
                                else
                                    opts.push({ value: '3', text: 'Reconfigure Terms' });
                            }

                            if (rowObject.CanCancelOriginatedFinancePlan) {
                                opts.push({ value: '2', text: 'Cancel' });
                            }

                        }
                        
                        if (opts.length > 1) {
                            var html = '<select class="form-control">';

                            for (var i = 0; i < opts.length; i++) {
                                html += '<option value="' + opts[i].value + '">' + opts[i].text + '</option>';
                            }

                            return html + '</select>';
                        } else {
                            return na;
                        }
                    }
                }
            ]
        });

    };

    return guarantorAccountFinancePlans;

})(window.VisitPay.GuarantorAccountFinancePlanVisits = window.VisitPay.GuarantorAccountFinancePlanVisits || {},
    window.VisitPay.FinancePlanInterestRateHistory = window.VisitPay.FinancePlanInterestRateHistory || {},
    window.VisitPay.FinancePlanLog = window.VisitPay.FinancePlanLog || {},
    window.VisitPay.FinancePlanBucketHistory = window.VisitPay.FinancePlanBucketHistory || {},
    window.VisitPay.FinancePlanCancel = window.VisitPay.FinancePlanCancel || {},
    window.VisitPay.Common = window.VisitPay.Common || {},
    window.VisitPay.GuarantorFilter = window.VisitPay.GuarantorFilter || {},
    window.VisitPay.FinancePlanInterestReallocate = window.VisitPay.FinancePlanInterestReallocate || {},
    jQuery,
    window.ko);

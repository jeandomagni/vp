﻿window.VisitPay.ManageGuarantorAttributeChanges = (function(common, clientSettings, guarantorUnmatch, $) {

    var manageGuarantorAttributeChanges = this;
    var filterViewModel = function () {

        var self = this;

        self.HsGuarantorMatchDiscrepancyStatus = ko.observable('');
        self.DateRangeFrom = ko.observable('');
        self.HsGuarantorSourceSystemKey = ko.observable('');
        self.VpGuarantorId = ko.observable('');

        self.RunResults = ko.observable(false);
        self.ChangeNotifications = function () {

            var setRun = function () {
                self.RunResults(true);
            };

            self.HsGuarantorMatchDiscrepancyStatus.subscribe(setRun);
            self.DateRangeFrom.subscribe(setRun);
            self.HsGuarantorSourceSystemKey.subscribe(setRun);
            self.VpGuarantorId.subscribe(setRun);

        };

        self.HsGuarantorMatchFields = ko.observableArray();
        self.FilterMatchDiscrepancies = function (matchDiscrepancies) {
            if (!matchDiscrepancies) {
                return null;
            }

            // exclude discrepancies that are not related to guarantor matching
            return matchDiscrepancies.filter(function (matchDiscrepancy) {
                return ko.unwrap(self.HsGuarantorMatchFields).filter(function (matchField) {
                    return matchField.Key.toUpperCase() === matchDiscrepancy.HsGuarantorMatchDiscrepancyMatchFieldName.toUpperCase();
                }).length > 0;
            });
        };

    };

    var $scope = $('#section-guarantorattributechanges');
    var $grid = $scope.find('#jqGrid');

    var gridDefaultSortName = 'HsGuarantorMatchDiscrepancyStatusName';
    var gridDefaultSortOrder = 'asc';

    var model;

    function formatMatchFieldColumn(rowObject, matchFieldName) {

        if (!rowObject.HsGuarantorMatchDiscrepancyMatchFields)
            return '';

        var matchField = null;
        for (var i = 0; i < rowObject.HsGuarantorMatchDiscrepancyMatchFields.length; i++) {
            var item = rowObject.HsGuarantorMatchDiscrepancyMatchFields[i];
            if (item.HsGuarantorMatchDiscrepancyMatchFieldName.toUpperCase() === matchFieldName.toUpperCase()) {
                matchField = item;
                break;
            }
        }

        if (matchField == null)
            return '';

        var $container = $('<div/>');
        $container.addClass('matchField');
        if (matchField.IsDiscrepancy) {
            $container.addClass('discrepancy');
        }

        var $new = $('<div/>').append($('<strong/>').html(matchField.HsGuarantorMatchDiscrepancyMatchFieldNewValue));
        $new.attr('title', $new.text());
        var $current = $('<div/>').html(matchField.HsGuarantorMatchDiscrepancyMatchFieldCurrentValue);
        $current.attr('title', $current.text());

        $container.append($new).append($current);

        return $('<div/>').append($container).html();

    };

    function formatActionColumn(cellValue, options, rowObject) {
        if (rowObject.HsGuarantorMatchDiscrepancyStatus === 1) {

            var $unmatch = $('<a/>').text('Unmatch').attr('title', 'Unmatch').attr('href', 'javascript:;').addClass('unmatch');
            var $valid = $('<a/>').text('Valid Match').attr('title', 'Valid Match').attr('href', 'javascript:;').addClass('validmatch');

            return $('<div/>').append($valid).append('<span></span>').append($unmatch).html();

        }

        var $line1 = $('<div/>').text(rowObject.HsGuarantorMatchDiscrepancyStatusName + ' ' + rowObject.ActionDate);
        var $line2 = $('<div/>').text(rowObject.ActionUserName);

        var $div = $('<div/>');
        if (rowObject.ActionNotes && rowObject.ActionNotes.length > 0) {
            var $glyph = $('<div/>').append($('<em/>').addClass('glyphicon glyphicon-comment')).html();
            $line2.html($line2.text() + ' &nbsp; ' + $glyph);
            $div.attr('data-tip', rowObject.ActionNotes);
            $div.attr('data-toggle', 'tooltip');
            $div.attr('data-placement', 'top');
        }
        $div.append($line1).append($line2);

        return $('<div/>').append($div).html();

    };

    function formatVpGuarantorColumn(cellValue, options, rowObject) {

        var $a = $('<a/>');
        $a.attr('href', '/Search/GuarantorAccount/' + rowObject.VpGuarantorId);
        $a.text(cellValue);

        return $('<div/>').append($a).html();

    };

    function gridGridComplete() {

        $scope.find('#jqGridPager').find('#input_jqGridPager').find('input').off('change').on('change', function () {
            $grid.jqGrid().trigger('reloadGrid', [{ page: $(this).val() }]);
        });

        $grid.find('[data-toggle="tooltip"]').each(function () {
            $(this).tooltip({
                animation: false,
                html: true,
                title: '<div style="max-width: 350px; white-space: normal;">' + $(this).data('tip') + '</div>',
                container: 'body'
            });
        });

        $grid.parents('.ui-jqgrid:eq(0)').addClass('visible');

    };

    function gridLoadComplete(data) {

        if (data.GuarantorAttributeChanges && data.GuarantorAttributeChanges.length > 0) {

            $grid.parents('.ui-jqgrid:eq(0)').show();
            $('#jqGridNoRecords').removeClass('in');

            $grid.find('.jqgrow').each(function() {
                var $row = $(this);
                var rowData = data.GuarantorAttributeChanges[$row.index() - 1];
                var vpGuarantorId = rowData.VpGuarantorId;
                var hsGuarantorSourceSystemKey = rowData.HsGuarantorSourceSystemKey;
                var hsGuarantorMatchDiscrepancyId = rowData.HsGuarantorMatchDiscrepancyId;
                var matchDiscrepancies = model.FilterMatchDiscrepancies(rowData.HsGuarantorMatchDiscrepancyMatchFields);

                $row.find('a.unmatch').off('click').on('click', function(e) {
                    e.preventDefault();
                    guarantorUnmatch.OpenUnmatchConfirmModal(vpGuarantorId, [hsGuarantorSourceSystemKey], hsGuarantorMatchDiscrepancyId, 1, matchDiscrepancies);
                });

                $row.find('a.validmatch').off('click').on('click', function(e) {
                    e.preventDefault();
                    guarantorUnmatch.OpenValidateMatchConfirmModal(vpGuarantorId, [hsGuarantorSourceSystemKey], hsGuarantorMatchDiscrepancyId, 1, matchDiscrepancies);
                });
            });

        } else {
            $grid.parents('.ui-jqgrid:eq(0)').hide();
            $('#jqGridNoRecords').addClass('in');
        }

        model.RunResults(false);
        $.unblockUI();

    };

    function buildColumnModel(matchFields) {

        // jqgrid doesn't do %'s
        var outerWidth = $scope.width();
        var fixedFieldWidth = outerWidth * .68;
        var matchFieldWidth = outerWidth * .32;

        //
        var colModel = [];
        colModel.push({ width: fixedFieldWidth * .11, label: 'Status', name: 'HsGuarantorMatchDiscrepancyStatusName' });
        colModel.push({ width: fixedFieldWidth * .12, label: 'Date', name: 'InsertDate' });
        colModel.push({ width: fixedFieldWidth * .17, label: 'Guarantor', name: 'VpGuarantorFullName', formatter: formatVpGuarantorColumn });
        colModel.push({ width: fixedFieldWidth * .15, label: clientSettings.HsGuarantorPatientIdentifier, name: 'HsGuarantorSourceSystemKey' });
        colModel.push({ width: fixedFieldWidth * .15, label: clientSettings.VpGuarantorPatientIdentifier, name: 'VpGuarantorId' });

        for (var i = 0; i < matchFields.length; i++) {
            colModel.push({
                label: matchFields[i].Value,
                name: matchFields[i].Key,
                sortable: false,
                width: matchFieldWidth / matchFields.length,
                formatter: function (cellValue, options, rowObject) {
                    return formatMatchFieldColumn(rowObject, options.colModel.name);
                }
            });
        }

        colModel.push({ width: fixedFieldWidth * .24, label: 'Actions', name: 'Actions', sortable: false, classes: 'text-center', formatter: formatActionColumn });

        for (var z = 0; z < colModel.length; z++) {
            colModel[z].resizable = false;
        }

        return colModel;

    };

    function buildGrid(matchFields) {

        var colModel = buildColumnModel(matchFields);

        //
        $grid.jqGrid({
            url: '/WorkQueue/GuarantorAttributeChanges',
            postData: { model: ko.toJS(model) },
            jsonReader: { root: 'GuarantorAttributeChanges' },
            autowidth: true,
            pager: 'jqGridPager',
            sortname: gridDefaultSortName,
            sortorder: gridDefaultSortOrder,
            loadComplete: gridLoadComplete,
            gridComplete: gridGridComplete,
            colModel: colModel
        });

    };

    function reloadGrid() {
        $.extend($grid.jqGrid('getGridParam', 'postData'), { model: ko.toJS(model) });
        $grid.setGridParam({ page: 1 });
        $grid.sortGrid(gridDefaultSortName, true, gridDefaultSortOrder);
    };

    function refreshGrid() {
        $grid.sortGrid($grid.jqGrid('getGridParam', 'sortname'), true, $grid.jqGrid('getGridParam', 'sortorder'));
    };

    function resetModel() {

        model.HsGuarantorMatchDiscrepancyStatus('1');
        model.DateRangeFrom('-30');
        model.HsGuarantorSourceSystemKey('');
        model.VpGuarantorId('');
        model.RunResults(false);

    };

    manageGuarantorAttributeChanges.Init = function (matchFields) {

        ko.cleanNode($('#jqGridFilter')[0]);
        model = new filterViewModel();
        model.HsGuarantorMatchFields(matchFields);
        model.ChangeNotifications();
        ko.applyBindings(model, $('#jqGridFilter')[0]);
        resetModel();

        $('#jqGridFilter').on('submit', function (e) {
            e.preventDefault();
            if ($(this).valid()) {
                $.blockUI();
                reloadGrid();
            }
        });

        $('#btnGridFilterReset').on('click', function () {
            $.blockUI();
            resetModel();
            reloadGrid();
        });

        $('#btnExport').on('click', function (e) {

            e.preventDefault();

            $.blockUI();

            $.post('/WorkQueue/GuarantorAttributeChangesExport', {
                model: ko.toJS(model),
                sidx: $grid.jqGrid('getGridParam', 'sortname'),
                sord: $grid.jqGrid('getGridParam', 'sortorder')
            }, function (result) {
                $.unblockUI();
                if (result.Result === true) {
                    window.location.href = result.Message;
                }
            }, 'json');

        });

        buildGrid(matchFields);

        $(document).off('guarantorUnmatchActionComplete').on('guarantorUnmatchActionComplete', refreshGrid);

    };

    return manageGuarantorAttributeChanges;

})(window.VisitPay.Common || {}, window.VisitPay.ClientSettings || {}, window.VisitPay.GuarantorUnmatch || {}, jQuery);
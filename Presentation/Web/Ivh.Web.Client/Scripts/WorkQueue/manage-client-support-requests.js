﻿(function() {

    window.VisitPay.Grids.ManageClientSupportRequests = (function($gridElement, filterModel, handlers) {

        var gridOpts = {
            autowidth: false,
            url: '/WorkQueue/ClientSupportRequests',
            sortname: 'InsertDate',
            colModel: [
                {
                    label: 'Case ID',
                    name: 'ClientSupportRequestDisplayId',
                    width: 120,
                    formatter: function(cellValue, options, rowObject) {

                        var $temp = $('<div/>');

                        var $unread = $('<em/>').addClass('glyphicon glyphicon-envelope pull-right').attr('title', rowObject.UnreadMessageCountToClient + ' unread message(s)').attr('data-toggle', 'tooltip').attr('data-placement', 'right');
                        if (rowObject.UnreadMessageCountToClient > 0) {
                            $unread.addClass('unread');
                        }
                        $temp.append($unread);

                        if (rowObject.AttachmentCountClient > 0) {
                            $temp.append($('<em/>').addClass('glyphicon glyphicon-paperclip pull-right').attr('title', 'There are files attached to this ticket.').attr('data-toggle', 'tooltip').attr('data-placement', 'right'));
                        }

                        $temp.append($('<a/>').attr('href', 'javascript:;').attr('name', 'case').attr('title', 'View Support Ticket History').attr('data-toggle', 'tooltip').attr('data-placement', 'right').text(cellValue));

                        return $temp.html();
                    }
                },
                {
                    label: 'Created',
                    name: 'InsertDate',
                    width: 80
                },
                {
                    label: 'Requested By',
                    name: 'CreatedByVisitPayUserName',
                    width: 120
                },
                {
                    label: 'Status',
                    name: 'StatusDisplay',
                    width: 80
                },
                {
                    label: 'Topic - Sub Topic',
                    name: 'TopicDisplay',
                    width: 240
                },
                {
                    label: 'Comments',
                    name: 'MessagePreview',
                    sortable: false,
                    width: 351
                }
            ]
        };

        return new window.VisitPay.Grids.BaseGrid($gridElement, filterModel, $(), gridOpts, { noRecordsText: 'There are no support tickets to display at this time.' }, { loadComplete: handlers.loadComplete });

    });

    var ViewModel = function () {

        var self = this;

        self.Filter = {
            ClientSupportRequestStatus: ko.observable(),
            CreatedByVisitPayUserId: ko.observable(),
            DaysOffset: ko.observable()
        };

        self.RunResults = ko.observable(false);
        self.ChangeNotifications = function () {

            var setRun = function () {
                self.RunResults(true);
            };

            self.Filter.ClientSupportRequestStatus.subscribe(setRun);
            self.Filter.CreatedByVisitPayUserId.subscribe(setRun);
            self.Filter.DaysOffset.subscribe(setRun);

        };

        return self;

    };

    var vm;
    var grid;

    function reloadGrid(e) {
        if (e) {
            e.preventDefault();
        }
        if (!$('.blockPage').is(':visible')) {
            $.blockUI();
        }
        grid.reload();
        vm.RunResults(false);
    };

    function resetGrid(e) {
        if (e) {
            e.preventDefault();
        }
        $.blockUI();
        grid.reset();
        vm.RunResults(false);
    }

    function loadComplete(data, root) {
        grid.jqg.find('.jqgrow').each(function() {

            var row = root[$(this).index() - 1];

            $(this).off('click');

            $(this).on('click', 'a[name="case"]', function(e) {
                e.preventDefault();
                var edit = new window.VisitPay.ClientSupport.ClientSupportRequestEdit(false, row.ClientSupportRequestId, false);
                edit.show().done(function() {
                    reloadGrid();
                });
            });

        });

        $.unblockUI();
    }

    $(document).ready(function() {

        vm = new ViewModel();

        ko.applyBindings(vm, $('#section-manageclientsupportrequests form')[0]);
        vm.ChangeNotifications();

        grid = new window.VisitPay.Grids.ManageClientSupportRequests($('#gridManageClientSupportRequests'), vm.Filter, { loadComplete: loadComplete });

        $(document).on('submit', '#section-manageclientsupportrequests form', reloadGrid);
        $(document).on('click', '#section-manageclientsupportrequests form button[data-reset=true]', resetGrid);

        $(document).on('click', '#section-manageclientsupportrequests #btnCreateSupportRequest', function () {
            var create = window.VisitPay.ClientSupport.ClientSupportRequestCreate();
            create.show().done(reloadGrid);
        });

    });

})();
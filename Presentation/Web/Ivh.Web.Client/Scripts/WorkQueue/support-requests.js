﻿(function (supportRequest, supportRequestAction, common, createSupportRequest, $, ko) {

    var model;
    var $scope;
    var initialDateRange;
    var initialRead = true;
    var initialUnread = true;

    var filterViewModel = function () {

        var self = this;

        self.SupportRequestStatus = ko.observable('');
        self.DateRangeFrom = ko.observable();
        self.SupportRequestDisplayId = ko.observable('');
        self.AssignedToVisitPayUserId = ko.observable('');
        self.ReadMessages = ko.observable(initialRead);
        self.UnreadMessages = ko.observable(initialUnread);
        self.FacilitySourceSystemKey = ko.observable();

        self.RunResults = ko.observable(false);
        self.ChangeNotifications = function () {

            var setRun = function () {
                self.RunResults(true);
            };

            self.SupportRequestStatus.subscribe(setRun);
            self.DateRangeFrom.subscribe(setRun);
            self.SupportRequestDisplayId.subscribe(setRun);
            self.AssignedToVisitPayUserId.subscribe(setRun);
            self.ReadMessages.subscribe(setRun);
            self.UnreadMessages.subscribe(setRun);
            self.FacilitySourceSystemKey.subscribe(setRun);
        };
        return self;
    };


    var grid;
    var gridDefaultSortName = window.sessionStorage.getItem('gridDefaultSortName');
    var gridDefaultSortOrder = window.sessionStorage.getItem('gridDefaultSortOrder');
    if (!gridDefaultSortName) {
        gridDefaultSortName = 'AssignedToVisitPayUserName';
    }
    if (!gridDefaultSortOrder) {
        gridDefaultSortOrder = 'asc';
    }

    var refreshGrid = function () {
        if (!supportRequest.ModalIsOpen)
            $.blockUI();

        grid.sortGrid(grid.jqGrid('getGridParam', 'sortname'), true, grid.jqGrid('getGridParam', 'sortorder'));

    };

    var reloadGrid = function () {
        var postModel = ko.toJS(model);
        $.extend(grid.jqGrid('getGridParam', 'postData'), { model: postModel });
        grid.setGridParam({ page: 1 });
        grid.sortGrid(gridDefaultSortName, true, gridDefaultSortOrder);
    };

    var resetFilters = function () {
        model.SupportRequestStatus('');
        model.DateRangeFrom(initialDateRange);
        model.SupportRequestDisplayId('');
        model.AssignedToVisitPayUserId('');
        model.ReadMessages(initialRead);
        model.UnreadMessages(initialUnread);
        model.FacilitySourceSystemKey('');
        model.RunResults(false);
    };

    var reloadAssignedUserDropDown = function () {

        var controlID = '#AssignedToVisitPayUserId';
        var apiURL = '/WorkQueue/SupportAdminUsers';
        var allOptionText = 'All';
        var allValueText = '';
        var defaultSelectedVal = '-1'; // <Unassigned>
        var foundSelectedVal = false;

        //get knockout observable property that the drop down is databound to
        var myObservable = model.AssignedToVisitPayUserId;

        //get unique filter options from controller ASYNC
        $.post(apiURL, {}, function (data) {

            //get current selected filter value
            var selectedVal = ko.unwrap(myObservable);

            //clear all existing options
            $(controlID).empty();

            //add "All" option
            $(controlID).append($('<option>', { value: allValueText, text: allOptionText }));

            var i;
            for (i = 0; i < data.length; i++) {
                //add option from JSON array results (Value,Text)
                $(controlID).append($('<option>', { value: data[i].Value, text: data[i].Text }));
                if (String(data[i].Value) == String(selectedVal)) //JSON object value needs conversion to string to succeed with "loose equality"
                {
                    //indicate that the previous selected value is still present
                    foundSelectedVal = true;
                }
            }

            if (selectedVal == allValueText) {
                //indicate that the previous selected value is still present
                foundSelectedVal = true;
            }

            // use default selected value if the new filter options don't contain the previous selected value
            if (!foundSelectedVal) {
                selectedVal = defaultSelectedVal;
                //forces bound UI control to display message about "Run button needs to be clicked" (Other ASYNC events can still set it to false)
                model.RunResults(true);
            }

            //set current selected filter value
            myObservable(selectedVal);
            //trigger change event since same value assignment might not notify (maybe use always notify extender on observable?)
            $(controlID).val(selectedVal).change();
        });

    };

    var bindGrid = function ($scope) {

        var deferred = $.Deferred();

        //
        grid = $scope.find('#jqGridSupportRequests');

        //
        grid.jqGrid({
            url: '/WorkQueue/SupportRequests',
            postData: { model: ko.toJS(model) },
            jsonReader: { root: 'SupportRequests' },
            autowidth: false,
            pager: 'jqGridSupportRequestsPager',
            sortname: gridDefaultSortName,
            sortorder: gridDefaultSortOrder,
            loadComplete: function (data) {

                var gridContainer = grid.parents('.ui-jqgrid:eq(0)');
                var gridNoRecords = $scope.find('#jqGridSupportRequestsNoRecords');

                window.sessionStorage.setItem('gridDefaultSortName', grid.jqGrid('getGridParam', 'sortname'));
                window.sessionStorage.setItem('gridDefaultSortOrder', grid.jqGrid('getGridParam', 'sortorder'));

                if (data.SupportRequests != null && data.SupportRequests.length > 0) {

                    gridContainer.show();
                    gridNoRecords.removeClass('in');
                    grid.closest('.ui-jqgrid-bdiv').scrollTop(0);

                    grid.find('.jqgrow').each(function () {

                        var rowData = data.SupportRequests[$(this).index() - 1];

                        $(this).find('a:eq(0)').on('click', function (e) {
                            //Open support request
                            supportRequest.Initialize(rowData.SupportRequestId, rowData.VisitPayUserId, rowData.VpGuarantorId, rowData.IsAccountClosed);
                        });

                        $(this).find('input[type=checkbox]').on('change', function (e) {

                            e.preventDefault();

                            $.blockUI();
                            supportRequestAction.Initialize($('#modalSupportRequestActionContainer'),
                                rowData.SupportRequestStatusId,
                                rowData.SupportRequestId,
                                rowData.SupportRequestDisplayId,
                                rowData.VisitPayUserId);

                            if ($(this).is(':checked')) {
                                $(this).prop('checked', false);
                            } else {
                                $(this).prop('checked', true);
                            }

                        });
                    });

                } else {

                    gridContainer.hide();
                    gridNoRecords.addClass('in');
                    grid.closest('.ui-jqgrid-bdiv').scrollTop(0);
                }

                common.InitTooltips(grid);
                model.RunResults(false);

                if (!supportRequest.ModalIsOpen)
                    $.unblockUI();

                deferred.resolve();

            },
            gridComplete: function () {

                $scope.find('#jqGridSupportRequestsPager').find('#input_jqGridPager').find('input').off('change').on('change', function () {
                    grid.jqGrid().trigger('reloadGrid', [{ page: $(this).val() }]);
                });

                var gridContainer = grid.parents('.ui-jqgrid:eq(0)');
                gridContainer.addClass('visible');

            },
            colModel: [
                {
                    label: 'Closed', name: '', width: 43, resizable: false, sortable: false, classes: 'text-center', formatter: function (cellValue, options, rowData) {
                        var cb = $('<input type="checkbox"/>').addClass('form-control');
                        if (rowData.IsClosed)
                            cb.attr('checked', 'checked');
                        if (!rowData.CanTakeAction || rowData.IsAccountClosed)
                            cb.prop('disabled', 'disabled');
                        return cb.wrap('<div></div>').parent().html();
                    }
                },
                { label: 'Assigned To', name: 'AssignedToVisitPayUserName', width: 90, resizable: false },
                {
                    label: 'Case ID',
                    name: 'SupportRequestDisplayId',
                    width: 145,
                    resizable: false,
                    formatter: function (cellValue, options, rowObject) {

                        var html = '<span style="display: block; position: relative;">';
                        html += '<a href="javascript:;" title="View Support Request History">' + cellValue + '</a>';

                        if (rowObject.HasMessages && rowObject.UnreadMessages > 0)
                            html += '<span class="glyphicon glyphicon-envelope" style="color: #ff7a32;" data-toggle="tooltip" data-placement="right" title="You have ' + rowObject.UnreadMessages + ' unread message(s)"></span>';
                        else if (rowObject.HasDraftMessage)
                            html += '<span class="glyphicon glyphicon-pencil" style="color: #000000;" data-toggle="tooltip" data-placement="right" title="You have an unsent draft message."></span>';
                        else if (rowObject.HasMessages)
                            html += '<span class="glyphicon glyphicon-envelope" style="color: #999999;" data-toggle="tooltip" data-placement="right" title="You have 0 unread messages."></span>';

                        if (rowObject.IsFlaggedForFollowUp)
                            html += '<span class="glyphicon glyphicon-flag" style="color: RED;" data-toggle="tooltip" data-placement="right" title="Flagged for follow-up"></span>';

                        if (rowObject.HasAttachments)
                            html += '<span class="glyphicon glyphicon-paperclip" data-toggle="tooltip" data-placement="right" title="There are files attached to this case."></span> ';

                        html += '</span>';

                        return html;
                    }
                },
                { label: 'Created', name: 'InsertDate', width: 80, resizable: false, classes: 'text-center' },
                { label: 'Status', name: 'SupportRequestStatus', width: 60, resizable: false },
                {
                    label: 'Guarantor', name: 'GuarantorFullName', width: 98, resizable: false, formatter: function (cellValue, options, rowData) {
                        return '<a href="/Search/GuarantorAccount/' + rowData.VpGuarantorId + '#tabSupportHistory" title="' + cellValue + '">' + cellValue + '</a>';
                    }
                },
                { label: 'Submitted For', name: 'SubmittedFor', width: 102, resizable: false },
                { label: 'Topic - Sub Topic', name: 'SupportTopic', width: 113, resizable: false },
                { label: 'Facility', name: 'FacilitySourceSystemKeys', width: 57, resizable: false },
                {
                    label: 'Last Modified', name: 'LastMessageDate', width: 92, resizable: false, classes: 'text-center', formatter: function (cellValue) {
                        if (cellValue === undefined || cellValue === null || cellValue.length === 0) {
                            return '';
                        }
                        return '<span title="' + cellValue + '">' + cellValue.split(' ')[0] + '</span>';
                    }
                },
                { label: 'Last Modified By', name: 'LastMessageBy', width: 112, resizable: false }
            ]
        });

        return deferred;
    };

    $(document).ready(function () {

        var deferred = $.Deferred();

        //
        $scope = $('#section-supportrequests');

        //
        initialDateRange = $('#DateRangeFrom').val();
        initialRead = $('#ReadMessages').prop('checked');
        initialUnread = $('#UnreadMessages').prop('checked');

        //
        model = new filterViewModel();
        ko.applyBindings(model, $('#jqGridFilterSupportRequests')[0]);
        model.ChangeNotifications();

        //
        resetFilters();
        bindGrid($scope).done(function () {

            deferred.resolve();
            deferred = null;

        });

        $scope.on('submit', '#jqGridFilterSupportRequests', function (e) {
            e.preventDefault();
            $.blockUI();
            reloadGrid();
        });
        $scope.on('click', '#btnGridFilterSupportRequestsReset', 'click', function () {
            resetFilters();
            $.blockUI();
            reloadGrid();
        });

        $('#btnExport').on('click', function (e) {

            e.preventDefault();

            $.blockUI();

            $.post('/WorkQueue/SupportRequestsExport', {
                model: ko.toJS(model),
                sidx: grid.jqGrid('getGridParam', 'sortname'),
                sord: grid.jqGrid('getGridParam', 'sortorder')
            }, function (result) {
                $.unblockUI();
                if (result.Result === true) {
                    window.location.href = result.Message;
                }
            }, 'json');

        });

        return deferred;

    });

    createSupportRequest.OnCreated = refreshGrid;
    $(document).on('supportRequestActionCompleted', refreshGrid);
    $(document).on('supportRequestChanged', refreshGrid);
    $(document).on('supportRequestAssignedUserChanged', reloadAssignedUserDropDown);

})(window.VisitPay.SupportRequest, window.VisitPay.SupportRequestAction, window.VisitPay.Common, {}, jQuery, ko);
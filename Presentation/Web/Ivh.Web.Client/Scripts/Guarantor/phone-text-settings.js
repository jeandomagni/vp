﻿(function (visitPay, $) {

    var $scope = $('#section-phonetext');
    var visitPayUserId = $scope.find('#VisitPayUserId').val();

    function bindPreferences() {

        // these need to rebind each time the partial view reloads
        $scope.find('.preferences .preference input[type=checkbox]').bootstrapToggle({
            onstyle: 'vp-toggle',
            offstyle: 'vp-toggle',
            on: null,
            off: null,
            size: 'small'
        });
        $scope.find('.preferences .preference').css('visibility', 'visible');
    };

    function setPreference(e) {
        var postUrl = $(this).prop('checked') === true ? '/Guarantor/AddSmsPreference' : '/Guarantor/RemoveSmsPreference';
        $.post(postUrl, { id: $(this).data('id'), visitPayUserId: visitPayUserId }, function () { });
    };

    function reload() {

        var promise = $.Deferred();

        $.get('/Guarantor/PhoneTextInformation?visitPayUserId=' + visitPayUserId, {}, function (html) {

            $scope.html(html);
            bindPreferences();
            promise.resolve();

        });

        return promise;

    };

    function savePhone($form) {

        var promise = $.Deferred();
        $form.parseValidation();

        if (!$form.valid()) {
            promise.reject();
        }

        $.post('/Guarantor/PhoneEdit', $form.serialize(), function (result) {

            if (result.Result) {
                promise.resolve(result.Message);
            } else {
                promise.reject(result.Message);
            }

        });

        return promise;

    };

    function editPhone(e, isPrimary) {

        e.preventDefault();
        $.blockUI();

        visitPay.Common.ModalNoContainerAsync('modalPhoneEdit', '/Guarantor/PhoneEdit?phoneType=' + (isPrimary ? 'Primary' : 'Secondary') + '&visitPayUserId=' + visitPayUserId, 'GET', {}, true).done(function ($modal) {

            var title = isPrimary ? 'Primary' : 'Secondary';
            $modal.find('.modal-title').text('Edit ' + title + ' Phone');

            var $form = $modal.find('form');
            $form.parseValidation();

            $form.find('#PhoneNumber').mask('(999) 999-9999');

            $form.on('submit', function (e) {

                e.preventDefault();
                $.blockUI();

                savePhone($form).done(function (message) {

                    $modal.modal('hide');

                    reload().done(function () {
                        $.unblockUI();
                        visitPay.Common.ModalGenericAlert('Success!', message, 'Ok');
                    });

                }).fail(function (message) {

                    $.unblockUI();
                    if (message && message.length > 0) {
                        visitPay.Common.AppendMessageToValidationSummary($form, message);
                    }

                });
            });

            $.unblockUI();

        });

    };

    function removePhone(e) {

        var message = '<p>Are you sure you want to remove this Guarantor\'s secondary phone number?</p>';

        if ($(this).data('smsenabledfor').toString().toUpperCase() === 'TRUE') {
            message = '<p class="text-left">This number is used for text messaging.  If this number is removed, text messaging will be disabled.</p>' + message;
        }

        visitPay.Common.ModalGenericConfirmation('Remove Secondary Phone', message, 'Yes', 'No').done(function () {
            $.blockUI();
            $.post('/Guarantor/SecondaryPhoneDelete', { visitPayUserId: visitPayUserId }, function () {
                reload();
                $.unblockUI();
            });
        });
    };

    function disableSms(e) {

        $.blockUI();
        $.post('/Guarantor/GetDisableText', { visitPayUserId: visitPayUserId }, function (cms) {
            $.unblockUI();
            visitPay.Common.ModalGenericConfirmationMd(cms.ContentTitle, cms.ContentBody, 'Continue', 'Cancel').done(function () {
                $.blockUI();
                $.post('/Guarantor/DisableSms', { visitPayUserId: visitPayUserId }, function () {
                    reload();
                    $.unblockUI();
                });
            });
        });

    };

    $(document).ready(function () {

        bindPreferences();

        $scope.on('change', '.preferences input[type=checkbox]', setPreference);
        $scope.on('click', '#btnDisableSms', disableSms);
        $scope.on('click', '#btnEditPrimary', function (e) { editPhone(e, true); });
        $scope.on('click', '#btnEditSecondary', function (e) { editPhone(e, false); });
        $scope.on('click', '#btnDeleteSecondary', removePhone);

    });

})(window.VisitPay, jQuery);
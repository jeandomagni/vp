﻿(function (visitPay, $) {

    $(document).ready(function () {

        var form = $('#section-upgrade form');

        form.parseValidation();

        $('#cbUseEmailUsername').off('change').on('change', function (e) {
            if ($(this).is(':checked')) {
                var email = $('#Email').val();
                $('#Username').val(email);
                $('#UsernameConfirm').val(email);
            }
        });
    });

})(window.VisitPay, jQuery);
﻿namespace Ivh.Web.Client.Mappings
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.Linq;
    using System.Web;
    using System.Web.Mvc;
    using Application.Core.Common.Dtos;
    using Application.Email.Common.Dtos;
    using Application.EventJournal.Common.Dtos;
    using Application.FinanceManagement.Common.Dtos;
    using Application.SecureCommunication.Common.Dtos;
    using Application.User.Common.Dtos;
    using AutoMapper;
    using Common.Base.Utilities.Extensions;
    using Common.Base.Utilities.Helpers;
    using Common.EventJournal;
    using Common.VisitPay.Constants;
    using Common.VisitPay.Enums;
    using Common.VisitPay.Strings;
    using Common.Web.Extensions;
    using Common.Web.Mapping;
    using Common.Web.Models;
    using Common.Web.Models.Account;
    using Common.Web.Models.Alerts;
    using Common.Web.Models.Faq;
    using Common.Web.Models.Guarantor;
    using Common.Web.Models.Sso;
    using Ivh.Common.Web.Models.Communication;
    using Ivh.Web.Client.Models.PaymentQueue;
    using Models;
    using Models.Admin;
    using Models.Chat;
    using Models.ClientSupport;
    using Models.FinancePlan;
    using Models.Offline;
    using Models.Reports;
    using Models.Search;
    using Models.Sso;
    using Models.Unmatch;
    using Models.VisitPayDocument;
    using Models.WorkQueue;
    using Newtonsoft.Json;

    public class ClientWebMappings : CommonWebMappings
    {
        public ClientWebMappings()
        {
            this.CreateMap<HsGuarantorDto, SearchGuarantorsSearchResultViewModel>()
                .ForMember(x => x.DateOfBirth, opts => opts.MapFrom(x => x.DOB.HasValue ? x.DOB.Value.ToString(Format.DateFormat) : ""))
                .ForMember(x => x.Ssn4, opts => opts.MapFrom(x => x.SSN4.IsNullOrEmpty() ? "" : x.SSN4.PadLeft(4, '0')))
                .ForMember(dest => dest.Name, opts => opts.MapFrom(x => x.FirstNameLastName))
                .ForMember(x => x.SourceSystemKeys, opts => opts.MapFrom(x => x.SourceSystemKey))
                .ForMember(x => x.HsGuarantorId, opts => opts.MapFrom(x => x.HsGuarantorId));

            this.CreateMap<SearchGuarantorsSearchFilterViewModel, GuarantorFilterDto>()
                .ForMember(x => x.DOB, opts => opts.MapFrom(x => x.DateOfBirth));

            this.CreateMap<PaymentQueueGuarantorSearchFilterViewModel, GuarantorFilterDto>();

            this.CreateMap<GuarantorFilterDto, HsGuarantorFilterDto>()
                .ForMember(x => x.SourceSystemKey, opts => opts.MapFrom(x => x.HsGuarantorId))
                .ForMember(x => x.DOB, opts => opts.MapFrom(x => x.DOB.HasValue ? x.DOB.Value.ToString(Format.DateFormat) : ""));

            this.CreateMap<GuarantorWithLastStatementDto, SearchGuarantorsSearchResultViewModel>()
                .ForMember(x => x.DateOfBirth, opts => opts.MapFrom(x => x.Guarantor.User.DateOfBirth.HasValue ? x.Guarantor.User.DateOfBirth.Value.ToString(Format.DateFormat) : ""))
                .ForMember(x => x.SourceSystemKeys, opts => opts.MapFrom(x => string.Join(",", x.Guarantor.HsGuarantorsSourceSystemKeys.ToArray())))
                .ForMember(x => x.IsAwaitingStatement, opts => opts.MapFrom(x => x.IsAwaitingStatement))
                .ForMember(x => x.LastStatementBalance, opts => opts.MapFrom(x => x.LastStatement != null ? x.LastStatement.StatementedSnapshotTotalBalance.ToString("C") : ""))
                .ForMember(x => x.LastStatementDate, opts => opts.MapFrom(x => x.LastStatement != null ? x.LastStatement.StatementDate : (DateTime?) null))
                .ForMember(x => x.LastStatementIsGracePeriod, opts => opts.MapFrom(x => x.LastStatement != null && x.LastStatement.IsGracePeriod))
                .ForMember(x => x.LastStatementPaymentDueDate, opts => opts.MapFrom(x => x.LastStatement != null ? x.LastStatement.PaymentDueDate : (DateTime?) null))
                .ForMember(x => x.Name, opts => opts.MapFrom(x => x.Guarantor.User.DisplayFullName))
                .ForMember(x => x.Ssn4, opts => opts.MapFrom(x => x.Guarantor.User.SSN4.HasValue ? x.Guarantor.User.SSN4.Value.ToString().PadLeft(4, '0') : ""))
                .ForMember(x => x.VpGuarantorStatus, opts => opts.MapFrom(x => x.Guarantor.VpGuarantorStatus))
                .ForMember(x => x.UserNameAndEmailAddress, opts => opts.MapFrom(x => !string.Equals(x.Guarantor.User.UserName, x.Guarantor.User.Email, StringComparison.InvariantCultureIgnoreCase)
                    ? $"{x.Guarantor.User.UserName}; {x.Guarantor.User.Email}"
                    : x.Guarantor.User.UserName))
                .ForMember(x => x.VpGuarantorId, opts => opts.MapFrom(x => x.Guarantor.VpGuarantorId))
                .ForMember(x => x.VisitPayUserId, opts => opts.MapFrom(x => x.Guarantor.User.VisitPayUserId))
                .ForMember(x => x.LockoutReasonEnum, opts => opts.MapFrom(x => x.Guarantor.User.LockoutReasonEnum))
                .ForMember(x => x.VpGuarantorTypeEnum, opts => opts.MapFrom(x => x.Guarantor.VpGuarantorTypeEnum))
                .ForMember(x => x.LockoutEndDateUtc, opts => opts.MapFrom(x => x.Guarantor.User.LockoutEndDateUtc))
                .ForMember(x => x.IsLocked, opts => opts.MapFrom(x => x.Guarantor.User.IsLocked))
                .ForMember(x => x.AccountClosedDate, opts => opts.MapFrom(x => x.Guarantor.AccountClosedDate));

            this.CreateMap<GuarantorDto, GuarantorAccountViewModel>()
                .ForMember(x => x.HsGuarantorIds, opts => opts.MapFrom(x => x.HsGuarantorsSourceSystemKeys != null && x.HsGuarantorsSourceSystemKeys.Any() ? string.Join(",", x.HsGuarantorsSourceSystemKeys.ToArray()) : string.Empty))
                .ForMember(x => x.HsGuarantorIdsCount, opts => opts.MapFrom(x => x.HsGuarantorsSourceSystemKeys == null ? 0 : x.HsGuarantorsSourceSystemKeys.Count))
                .ForMember(x => x.IsDisabled, opts => opts.MapFrom(x => x.User != null && x.User.IsDisabled))
                .ForMember(x => x.IsLocked, opts => opts.MapFrom(x => x.User != null && x.User.IsLocked))
                .ForMember(x => x.LockoutReasonEnum, opts => opts.MapFrom(x => x.User == null ? null : x.User.LockoutReasonEnum))
                .ForMember(x => x.VpGuarantorTypeId, opts => opts.MapFrom(x => (int)x.VpGuarantorTypeEnum))
                .ForMember(dest => dest.NextStatementDate, opts => opts.MapFrom(x => x.NextStatementDate))
                .ForMember(dest => dest.RegistrationDate, opts => opts.MapFrom(x => MappingHelper.MapToClientDateTime(x.RegistrationDate)))
                .ForMember(dest => dest.AccountClosedDate, opts => opts.MapFrom(x => MappingHelper.MapToClientDateTime(x.AccountClosedDate)))
                .ForMember(dest => dest.AccountCancellationDate, opts => opts.MapFrom(x => MappingHelper.MapToClientDateTime(x.AccountCancellationDate)))
                .ForMember(dest => dest.AccountReactivationDate, opts => opts.MapFrom(x => MappingHelper.MapToClientDateTime(x.AccountReactivationDate)))
                .ForMember(dest => dest.IsOfflineUser, opts => opts.MapFrom(x => x.IsOfflineGuarantor))
                .ForMember(dest => dest.UserVisitPayUserGuid, opts => opts.MapFrom(x => x.User != null ? x.User.VisitPayUserGuid : new Nullable<Guid>()));
            //// TODO: these are getting assigned in the SearchController, might want to refactor where & how they get populated
            //.ForMember(dest => dest.LastStatementDate, opts => opts.MapFrom(x => CommonWebMappings.MapToClientDateTimeText(x.LastStatementDate)))
            //.ForMember(dest => dest.PaymentDueDate, opts => opts.MapFrom(x => CommonWebMappings.MapToDateText(x.PaymentDueDate)))
            //.ForMember(dest => dest.LastLoginDate, opts => opts.MapFrom(x => CommonWebMappings.MapToClientDateTimeText(x.LastLoginDate)))
            //.ForMember(dest => dest.LastPaymentDate, opts => opts.MapFrom(x => CommonWebMappings.MapToClientDateTimeText(x.LastPaymentDate)));

            this.CreateMap<VisitPayUserDto, ProfileEditViewModel>();

            this.CreateMap<AlertDto, AlertViewModel>();

            this.CreateMap<PaymentMethodDto, GuarantorAccountPaymentMethodViewModel>();

            this.CreateMap<VisitPayUserCommunicationPreferenceDto, VisitPayUserCommunicationPreferenceViewModel>();

            this.CreateMap<GuarantorVisitsSearchFilterViewModel, VisitFilterDto>()
                .ForMember(dest => dest.VisitDateRangeFrom, opts => opts.MapFrom(src => src.VisitDateRangeFrom.HasValue ? src.VisitDateRangeFrom.ToString() : null))
                .ForMember(dest => dest.VisitDateRangeTo, opts => opts.MapFrom(src => src.VisitDateRangeTo.HasValue ? src.VisitDateRangeTo.ToString() : null));

            this.CreateMap<VisitDto, GuarantorVisitsSearchResultViewModel>()
                .ForMember(dest => dest.BalanceTransferStatus, opts => opts.MapFrom(x => x.BalanceTransferStatus.BalanceTransferStatusId))
                .ForMember(dest => dest.CurrentBalance, opts => opts.MapFrom(x => x.CurrentBalance.FormatCurrency()))
                .ForMember(dest => dest.CurrentBalanceDecimal, opts => opts.MapFrom(x => x.CurrentBalance))
                .ForMember(dest => dest.DischargeDate, opts => opts.MapFrom(x => x.VisitDisplayDate))
                .ForMember(dest => dest.SourceSystemKey, opts => opts.MapFrom(x => MappingHelper.MapVisitSourceSystemKey(x)))
                .ForMember(dest => dest.TotalCharges, opts => opts.MapFrom(x => x.TotalCharges.FormatCurrency()))
                .ForMember(dest => dest.UnclearedBalance, opts => opts.MapFrom(x => x.UnclearedBalance.FormatCurrency()))
                .ForMember(dest => dest.InterestAssessed, opts => opts.MapFrom(x => x.InterestAssessed.FormatCurrency()))
                .ForMember(dest => dest.InterestDue, opts => opts.MapFrom(x => x.InterestDue.FormatCurrency()))
                .ForMember(dest => dest.InterestPaid, opts => opts.MapFrom(x => (x.InterestPaid * -1m).FormatCurrency()))
                .ForMember(dest => dest.UnclearedPaymentsSum, opts => opts.MapFrom(x => x.UnclearedPaymentsSum.FormatCurrency()))
                .ForMember(dest => dest.VisitStateId, opts => opts.MapFrom(x => x.CurrentVisitState))
                .ForMember(dest => dest.VisitStateFullDisplayName, opts => opts.MapFrom(x => x.CurrentVisitState.ToString()))
                .ForMember(dest => dest.VisitStateDerivedFullDisplayName, opts => opts.MapFrom(x => x.CurrentVisitStateDerivedEnum.ToString().InsertSpaceBetweenLowerAndCapitalLetter()))
                .ForMember(dest => dest.VisitPayUserId, opts => opts.MapFrom(x => x.VPGuarantor.User.VisitPayUserId))
                .ForMember(dest => dest.IsRemoved, opts => opts.MapFrom(x => x.IsUnmatched));

            this.CreateMap<GuarantorVisitTransactionsSearchFilterViewModel, VisitTransactionFilterDto>();

            this.CreateMap<VisitTransactionDto, GuarantorVisitTransactionsSearchResultViewModel>()
                .ForMember(dest => dest.DisplayDate, opts => opts.MapFrom(x => x.DisplayDate.ToDateText()))
                .ForMember(dest => dest.TransactionAmount, opts => opts.MapFrom(x => x.TransactionAmount.FormatCurrency()));

            this.CreateMap<GuarantorTransactionsSearchFilterViewModel, VisitTransactionFilterDto>()
                .ForMember(dest => dest.TransactionDateRangeFrom, opts => opts.MapFrom(src => src.TransactionDateRangeFrom.HasValue ? src.TransactionDateRangeFrom.ToString() : null))
                .ForMember(dest => dest.TransactionDateRangeTo, opts => opts.MapFrom(src => src.TransactionDateRangeFrom.HasValue ? src.TransactionDateRangeTo.ToString() : null))
                .ForMember(dest => dest.TransactionType, opts => opts.Ignore());

            this.CreateMap<VisitTransactionDto, GuarantorTransactionsSearchResultViewModel>()
                .ForMember(dest => dest.TransactionAmount, opts => opts.MapFrom(x => x.TransactionAmount.FormatCurrency()))
                .ForMember(dest => dest.GuarantorName, opts => opts.MapFrom(x => x.Visit.GuarantorName))
                .ForMember(dest => dest.PatientName, opts => opts.MapFrom(x => x.Visit.PatientName))
                .ForMember(dest => dest.DisplayDate, opts => opts.MapFrom(x => MappingHelper.MapToClientDateText(x.DisplayDate)))
                .ForMember(dest => dest.VisitSourceSystemKeyDisplay, opts => opts.MapFrom(x => x.Visit.SourceSystemKeyDisplay))
                .ForMember(dest => dest.VisitSourceSystemKey, opts => opts.MapFrom(x => MappingHelper.MapVisitSourceSystemKey(x.Visit)))
                .ForMember(dest => dest.VisitMatchedSourceSystemKey, opts => opts.MapFrom(x => x.Visit.MatchedSourceSystemKey))
                .ForMember(dest => dest.VisitId, opts => opts.MapFrom(x => x.Visit.VisitId))
                .ForMember(dest => dest.VisitStateId, opts => opts.MapFrom(x => x.Visit.CurrentVisitState))
                .ForMember(dest => dest.VisitStateFullDisplayName, opts => opts.MapFrom(x => x.Visit.CurrentVisitState.ToString()))
                .ForMember(dest => dest.IsVisitRemoved, opts => opts.MapFrom(x => x.Visit.IsUnmatched));

            this.CreateMap<FinancePlanDto, GuarantorFinancePlansSearchResultViewModel>()
                .ForMember(dest => dest.CurrentFinancePlanBalance, opts => opts.MapFrom(x => x.CurrentFinancePlanBalance.FormatCurrency()))
                .ForMember(dest => dest.CurrentFinancePlanBalanceDecimal, opts => opts.MapFrom(x => x.CurrentFinancePlanBalance))
                .ForMember(dest => dest.FinancePlanStatus, opts => opts.MapFrom(x => x.FinancePlanStatus.FinancePlanStatusEnum))
                .ForMember(dest => dest.FinancePlanStatusFullDisplayName, opts => opts.MapFrom(x => x.FinancePlanStatus.FinancePlanStatusDisplayName))
                .ForMember(dest => dest.InterestAssessed, opts => opts.MapFrom(x => x.InterestAssessed.FormatCurrency()))
                .ForMember(dest => dest.InterestPaidToDate, opts => opts.MapFrom(x => x.InterestPaidToDate.FormatCurrency()))
                .ForMember(dest => dest.InterestRate, opts => opts.MapFrom(x => x.CurrentInterestRate.FormatPercentage()))
                .ForMember(dest => dest.OriginatedFinancePlanBalance, opts => opts.MapFrom(x => MappingHelper.MapFinancePlanOriginatedBalance(x.FinancePlanStatus.FinancePlanStatusEnum, x.OriginatedFinancePlanBalance)))
                .ForMember(dest => dest.OriginationDate, opts => opts.MapFrom(x => x.OriginationDate.ToDateText()))
                .ForMember(dest => dest.PaymentAmount, opts => opts.MapFrom(x => x.PaymentAmount.FormatCurrency()))
                .ForMember(dest => dest.PrincipalPaidToDate, opts => opts.MapFrom(x => x.PrincipalPaidToDate.FormatCurrency()))
                .ForMember(dest => dest.Terms, opts => opts.MapFrom(x => $"{x.CurrentInterestRate.FormatPercentage()} - {MappingHelper.MapToFormattedClientDateTimeText(x.LastPaymentDate, Format.MonthYearFormat)}"))
                .ForMember(dest => dest.VisitIds, opts => opts.MapFrom(x => x.FinancePlanVisits.Select(z => z.VisitId)));

            this.CreateMap<GuarantorFinancePlansSearchFilterViewModel, FinancePlanFilterDto>()
                .ForMember(dest => dest.OriginationDateSince, opts => opts.MapFrom(x => x.LastXRange == "90d" ? DateTime.UtcNow.AddDays(-90) : x.LastXRange == "6m" ? DateTime.UtcNow.AddMonths(-6) : x.LastXRange == "1y" ? DateTime.UtcNow.AddYears(-1) : (DateTime?) null));

            this.CreateMap<GuarantorInvitationViewModel, GuarantorInvitationDto>();

            this.CreateMap<GuarantorVisitStateHistorySearchFilterViewModel, VisitStateHistoryFilterDto>()
                .ForMember(dest => dest.VisitStates, opts => opts.MapFrom(x => x.VisitStateIds != null ? x.VisitStateIds.Select(z => (VisitStateEnum) z) : new List<VisitStateEnum>()));

            this.CreateMap<GuarantorVisitAgingHistorySearchFilterViewModel, VisitAgingHistoryFilterDto>();

            this.CreateMap<VisitStateHistoryDto, GuarantorVisitStateHistorySearchResultViewModel>()
                .ForMember(dest => dest.InsertDate, opts => opts.MapFrom(x => MappingHelper.MapToClientDateText(x.DateTime)))
                .ForMember(dest => dest.VisitStateId, opts => opts.MapFrom(x => x.EvaluatedState))
                .ForMember(dest => dest.VisitStateFullDisplayName, opts => opts.MapFrom(x => x.EvaluatedState.ToString()));

            this.CreateMap<VisitAgingHistoryDto, GuarantorVisitAgingHistorySearchResultViewModel>()
                .ForMember(dest => dest.InsertDate, opts => opts.MapFrom(x => MappingHelper.MapToClientDateText(x.InsertDate)))
                .ForMember(dest => dest.VisitPayUserName, opts => opts.MapFrom(x => x.VisitPayUser.UserName))
                .ForMember(dest => dest.VisitId, opts => opts.MapFrom(x => x.Visit.VisitId));

            this.CreateMap<GuarantorSupportHistorySearchFilterViewModel, SupportRequestFilterDto>();

            this.CreateMap<GuarantorCommunicationsSearchFilterViewModel, CommunicationsFilterDto>();

            // Do not convert date properties to Client time zone
            // They are already converted in Ivh.Application.Email.Mappings.AutomapperProfile
            this.CreateMap<CommunicationDto, GuarantorCommunicationSearchResultViewModel>()
                .ForMember(dest => dest.WasDelivered, opts => opts.MapFrom(x => x.DeliveredDate.HasValue))
                .ForMember(dest => dest.WasOpened, opts => opts.MapFrom(x => x.FirstOpenDate.HasValue))
                .ForMember(dest => dest.EmailBody, opts => opts.MapFrom(x => x.Body))
                .ForMember(dest => dest.EmailId, opts => opts.MapFrom(x => x.CommunicationId))
                .ForMember(dest => dest.Subject, opts => opts.MapFrom(x => x.Subject))
                .ForMember(dest => dest.EmailStatus, opts => opts.MapFrom(x => x.CommunicationStatus.ToString()))
                .ForMember(dest => dest.To, opts => opts.MapFrom(x => x.ToAddress))
                .ForMember(dest => dest.CommunicationMethod, opts => opts.MapFrom(x => x.CommunicationType.CommunicationMethodId.ToString()))
                .ForMember(dest => dest.CreatedDate, opts => opts.MapFrom(src => src.CreatedDate.ToDateTimeText()))
                .ForMember(dest => dest.OutboundServerAccepted, opts => opts.MapFrom(src => src.OutboundServerAccepted.ToDateTimeText()))
                .ForMember(dest => dest.ProcessedDate, opts => opts.MapFrom(src => src.ProcessedDate.ToDateTimeText()))
                .ForMember(dest => dest.DeliveredDate, opts => opts.MapFrom(src => src.DeliveredDate.ToDateTimeText()))
                .ForMember(dest => dest.FirstOpenDate, opts => opts.MapFrom(src => src.FirstOpenDate.ToDateTimeText()))
                .ForMember(dest => dest.FailedDate, opts => opts.MapFrom(src => src.FailedDate.ToDateTimeText()));

            // Do not convert date properties to Client time zone
            // They are already converted in Ivh.Application.Email.Mappings.AutomapperProfile
            this.CreateMap<CommunicationEventDto, GuarantorEmailEventResultViewModel>()
                .ForMember(dest => dest.EventDateTime, opts => opts.MapFrom(src => src.EventDateTime.ToDateTimeText()));

            this.CreateMap<SupportRequestSearchFilterViewModel, SupportRequestFilterDto>();

            this.CreateMap<SupportRequestDto, SupportRequestSearchResultViewModel>()
                .ForMember(dest => dest.GuarantorFullName, opts => opts.MapFrom(x => x.VpGuarantor.User.DisplayFirstNameLastName))
                .ForMember(dest => dest.HasAttachments, opts => opts.MapFrom(x => x.GuarantorMessages.SelectMany(z => z.ActiveSupportRequestMessageAttachments).Any()))
                .ForMember(dest => dest.HasMessages, opts => opts.MapFrom(x => x.GuarantorMessages.Count() > 1))
                .ForMember(dest => dest.InsertDate, opts => opts.MapFrom(x => MappingHelper.MapToClientDateText(x.InsertDate)))
                .ForMember(dest => dest.IsClosed, opts => opts.MapFrom(x => x.SupportRequestStatus == SupportRequestStatusEnum.Closed))
                .ForMember(dest => dest.IsFlaggedForFollowUp, opts => opts.MapFrom(x => x.IsFlaggedForFollowUp))
                .ForMember(dest => dest.LastMessageBy, opts => opts.MapFrom(x => x.SupportRequestMessages.OrderByDescending(z => z.InsertDate).Select(z => z.VisitPayUser.DisplayFirstNameLastName).FirstOrDefault()))
                .ForMember(dest => dest.LastMessageDate, opts => opts.MapFrom(x => MappingHelper.MapToClientDateTimeText(x.SupportRequestMessages.OrderByDescending(z => z.InsertDate).Select(z => z.InsertDate).FirstOrDefault())))
                .ForMember(dest => dest.MessageBody, opts => opts.MapFrom(x => x.SupportRequestMessages.OrderBy(z => z.InsertDate).Select(z => z.MessageBody).FirstOrDefault()))
                .ForMember(dest => dest.HasDraftMessage, opts => opts.MapFrom(x => x.SupportRequestMessages.Any(sr => sr.SupportRequestMessageType == SupportRequestMessageTypeEnum.DraftMessage && !string.IsNullOrEmpty(sr.MessageBody))))
                .ForMember(dest => dest.SupportRequestStatus, opts => opts.MapFrom(x => x.SupportRequestStatusSource.SupportRequestStatusName))
                .ForMember(dest => dest.SupportRequestStatusId, opts => opts.MapFrom(x => (int) x.SupportRequestStatus))
                .ForMember(dest => dest.SupportTopic, opts => opts.MapFrom(x => x.SupportTopicDisplayString))
                .ForMember(dest => dest.UnreadMessages, opts => opts.MapFrom(x => x.GuarantorMessages.Where(z => z.SupportRequestMessageType == SupportRequestMessageTypeEnum.FromGuarantor).OrderBy(z => z.InsertDate).Skip(1).Count(z => !z.IsRead)))
                .ForMember(dest => dest.VpGuarantorId, opts => opts.MapFrom(x => x.VpGuarantor.VpGuarantorId))
                .ForMember(dest => dest.VisitPayUserId, opts => opts.MapFrom(x => x.VpGuarantor.User.VisitPayUserId))
                .ForMember(dest => dest.IsAccountClosed, opts => opts.MapFrom(x => x.VpGuarantor.AccountClosedDate.HasValue))
                .ForMember(dest => dest.SubmittedFor, opts => opts.MapFrom(x => MappingHelper.MapSupportRequestSubmittedFor(x, false)))
                .ForMember(dest => dest.FacilitySourceSystemKeys, opts => opts.MapFrom(src => src.FacilitySourceSystemKeys.ToList()))
                .ForMember(dest => dest.AssignedToVisitPayUserName, opts => opts.MapFrom(src => src.AssignedVisitPayUser == null ? "&lsaquo;Unassigned&rsaquo;" : MappingHelper.MapUserFullName(src.AssignedVisitPayUser)));

            this.CreateMap<Common.Web.Models.Support.CreateSupportRequestViewModel, Models.Support.CreateSupportRequestViewModel>();

            this.CreateMap<VisitPayRoleDto, SelectListItem>()
                .ForMember(dest => dest.Text, opts => opts.MapFrom(x => x.Name))
                .ForMember(dest => dest.Value, opts => opts.MapFrom(x => x.VisitPayRoleId));

            this.CreateMap<ClientUserSearchFilterViewModel, VisitPayUserFilterDto>()
                .ForMember(dest => dest.Roles, opts => opts.MapFrom(x => x.VisitPayRoleId.HasValue ? new List<int> {x.VisitPayRoleId.Value} : new List<int>()));

            this.CreateMap<VisitPayUserDto, ClientUserSearchResultViewModel>()
                .ForMember(dest => dest.InsertDate, opts => opts.MapFrom(x => MappingHelper.MapToClientDateTimeText(x.InsertDate)))
                .ForMember(dest => dest.LastLoginDate, opts => opts.MapFrom(x => MappingHelper.MapToClientDateTimeText(x.LastLoginDate)))
                .ForMember(dest => dest.IsLocked, opts => opts.MapFrom(x => x.IsLocked ? "Y" : "N"))
                .ForMember(dest => dest.IsDisabled, opts => opts.MapFrom(x => x.IsDisabled ? "Y" : "N"))
                .ForMember(dest => dest.Roles, opts => opts.MapFrom(x => string.Join(", ", x.Roles.Where(z => z.Name != VisitPayRoleStrings.System.Client).Select(z => z.Name).OrderBy(z => z))));

            this.CreateMap<VisitPayUserDto, ClientUserViewModel>()
                .ForMember(dest => dest.LastLoginDate, opts => opts.MapFrom(x => MappingHelper.MapToClientDateTimeText(x.LastLoginDate)))
                .ForMember(dest => dest.LockoutEndDateUtc, opts => opts.MapFrom(x => MappingHelper.MapToClientDateTime(x.LockoutEndDateUtc.HasValue ? x.LockoutEndDateUtc.Value.ToLocalTime() : x.LockoutEndDateUtc)))
                .ForMember(dest => dest.VisitPayRoleStrings, opts => opts.MapFrom(x => x.Roles.Select(z => z.Name)));

            this.CreateMap<GuarantorPaymentDueDayHistoryDto, GuarantorPaymentDueDayHistoryViewModel>()
                .ForMember(dest => dest.OldPaymentDueDay, opt => opt.MapFrom(src => src.OldPaymentDueDay == 31 ? "Last Day of Month" : src.OldPaymentDueDay.ToString(CultureInfo.InvariantCulture)))
                .ForMember(dest => dest.PaymentDueDay, opt => opt.MapFrom(src => src.PaymentDueDay == 31 ? "Last Day of Month" : src.PaymentDueDay.ToString(CultureInfo.InvariantCulture)))
                .ForMember(dest => dest.InsertDate, opt => opt.MapFrom(src => MappingHelper.MapToFormattedClientDateTimeText(src.InsertDate, Format.DateTimeFormat)))
                .ForMember(dest => dest.InsertedBy, opt => opt.MapFrom(src => MappingHelper.MapPaymentDueDayChangedBy(src)));

            this.CreateMap<SecurityQuestionDto, SecurityQuestionReadOnlyViewModel>();
            this.CreateMap<SecurityQuestionAnswerDto, SecurityQuestionAnswerViewModel>();

            this.CreateMap<VisitDto, UnmatchVisitViewModel>()
                .ForMember(dest => dest.SourceSystemKey, opts => opts.MapFrom(x => MappingHelper.MapVisitSourceSystemKey(x)))
                .ForMember(dest => dest.VisitStateFullDisplayName, opts => opts.MapFrom(x => x.CurrentVisitState.ToString()))
                .ForMember(dest => dest.VpGuarantorId, opts => opts.MapFrom(src => src.VPGuarantor.VpGuarantorId));

            this.CreateMap<HsGuarantorMatchDiscrepancyDto, HsGuarantorMatchDiscrepancyViewModel>()
                .ForMember(dest => dest.ActionDate, opts => opts.MapFrom(src => MappingHelper.MapToClientDateText(src.ActionDate)))
                .ForMember(dest => dest.ActionNotes, opts => opts.MapFrom(src => src.ActionNotes))
                .ForMember(dest => dest.ActionUserName, opts => opts.MapFrom(src => src.ActionVisitPayUser != null ? src.ActionVisitPayUser.UserName : ""))
                .ForMember(dest => dest.InsertDate, opts => opts.MapFrom(src => MappingHelper.MapToClientDateText(src.InsertDate)))
                .ForMember(dest => dest.VpGuarantorId, opts => opts.MapFrom(src => src.VpGuarantor.VpGuarantorId))
                .ForMember(dest => dest.VpGuarantorFullName, opts => opts.MapFrom(src => src.VpGuarantor.User.DisplayFirstNameLastName));

            this.CreateMap<HsGuarantorMatchDiscrepancyMatchFieldDto, HsGuarantorMatchDiscrepancyMatchFieldViewModel>();

            this.CreateMap<GuarantorAttributeChangesFilterViewModel, HsGuarantorMatchDiscrepancyFilterDto>();

            this.CreateMap<GuarantorSummaryFilterViewModel, HsGuarantorMatchDiscrepancyFilterDto>();

            this.CreateMap<HsGuarantorMatchDiscrepancyDto, GuarantorSummaryViewModel>()
                .ForMember(dest => dest.ActionDate, opts => opts.MapFrom(src => MappingHelper.MapToClientDateText(src.ActionDate)))
                .ForMember(dest => dest.ActionNotes, opts => opts.MapFrom(src => src.ActionNotes))
                .ForMember(dest => dest.ActionUserName, opts => opts.MapFrom(src => MappingHelper.MapClientUserName(src.ActionVisitPayUser)))
                .ForMember(dest => dest.InsertDate, opts => opts.MapFrom(src => MappingHelper.MapToClientDateText(src.InsertDate)))
                .ForMember(dest => dest.VpGuarantorId, opts => opts.MapFrom(src => src.VpGuarantor.VpGuarantorId))
                .ForMember(dest => dest.VpGuarantorIdCurrent, opts => opts.MapFrom(src => src.HsGuarantorVpGuarantorId.HasValue ? src.HsGuarantorVpGuarantorId.Value.ToString() : ""));

            this.CreateMap<VisitUnmatchFilterViewModel, VisitUnmatchFilterDto>();

            this.CreateMap<VisitUnmatchDto, VisitUnmatchViewModel>()
                .ForMember(dest => dest.UnmatchActionDate, opts => opts.MapFrom(src => MappingHelper.MapToClientDateText(src.UnmatchActionDate)))
                .ForMember(dest => dest.UnmatchActionUserName, opts => opts.MapFrom(src => MappingHelper.MapClientUserName(src.UnmatchActionVisitPayUser)))
                .ForMember(dest => dest.VpGuarantorId, opts => opts.MapFrom(src => src.VpGuarantor.VpGuarantorId))
                .ForMember(dest => dest.VpGuarantorIdCurrent, opts => opts.MapFrom(src => src.VpGuarantorCurrent != null ? src.VpGuarantorCurrent.VpGuarantorId.ToString() : ""));

            this.CreateMap<SsoProviderWithUserSettingsDto, ClientSsoProviderWithUserSettingsViewModel>()
                .IncludeBase<SsoProviderWithUserSettingsDto, SsoProviderWithUserSettingsViewModel>()
                .ForMember(dest => dest.SourceSystemKey, opts => opts.MapFrom(src => string.Join(", ", src.SourceSystemKey)));

            this.CreateMap<SsoVisitPayUserSettingDto, ClientSsoProviderWithUserSettingsViewModel>()
                .IncludeBase<SsoVisitPayUserSettingDto, SsoProviderWithUserSettingsViewModel>();

            this.CreateMap<AuditUserAccessDto, AuditUserAccessViewModel>()
                .ForMember(dest => dest.EventDate, mo => mo.MapFrom(src => MappingHelper.MapToFormattedClientDateTimeText(src.EventDate, "G")));

            this.CreateMap<AuditUserChangeDto, AuditUserChangeViewModel>()
                .ForMember(dest => dest.TargetVisitPayUserId, mo => mo.MapFrom(src => src.TargetVisitPayUserId.ToString()))
                .ForMember(dest => dest.EventDate, mo => mo.MapFrom(src => MappingHelper.MapToFormattedClientDateTimeText(src.EventDate, "G")));

            this.CreateMap<AuditUserChangeDetailDto, AuditUserChangeDetailViewModel>()
                .ForMember(dest => dest.ChangeTime, mo => mo.MapFrom(src => MappingHelper.MapToFormattedClientDateTimeText(src.ChangeTime, "G")))
                .ForMember(dest => dest.ExpiredTime, mo => mo.MapFrom(src => MappingHelper.MapToFormattedClientDateTimeText(src.ExpiredTime, "G")))
                .ForMember(dest => dest.PriorLockoutEnabled, mo => mo.MapFrom(src => src.PriorLockoutEnabled.ToString()))
                .ForMember(dest => dest.LockoutEnabled, mo => mo.MapFrom(src => src.LockoutEnabled.ToString()))
                .ForMember(dest => dest.VisitPayUserId, mo => mo.MapFrom(src => src.VisitPayUserId.ToString()))
                .ForMember(dest => dest.PriorLockoutEndDateUtc, mo => mo.MapFrom(src => MappingHelper.MapToFormattedClientDateTimeText(src.PriorLockoutEndDateUtc, "G")))
                .ForMember(dest => dest.LockoutEndDateUtc, mo => mo.MapFrom(src => MappingHelper.MapToFormattedClientDateTimeText(src.LockoutEndDateUtc, "G")));

            this.CreateMap<AuditRoleChangeDto, AuditRoleChangeViewModel>()
                .ForMember(dest => dest.ChangeTime, mo => mo.MapFrom(src => MappingHelper.MapToFormattedClientDateTimeText(src.ChangeTime, "G")))
                .ForMember(dest => dest.ExpiredTime, mo => mo.MapFrom(src => MappingHelper.MapToFormattedClientDateTimeText(src.ExpiredTime, "G")))
                .ForMember(dest => dest.PriorDelete, mo => mo.MapFrom(src => MappingHelper.MapToFormattedClientDateTimeText(src.PriorDelete, "G")))
                .ForMember(dest => dest.ChangeEventId, mo => mo.MapFrom(src => src.ChangeEventId.ToString()))
                .ForMember(dest => dest.ExpiredChangeEventId, mo => mo.MapFrom(src => src.ExpiredChangeEventId.ToString()))
                .ForMember(dest => dest.VisitPayUserId, mo => mo.MapFrom(src => src.VisitPayUserId.ToString()))
                .ForMember(dest => dest.VisitPayRoleId, mo => mo.MapFrom(src => src.VisitPayRoleId.ToString()));

            this.CreateMap<AuditLogDto, AuditLogViewModel>()
                .ForMember(dest => dest.AuditDate, mo => mo.MapFrom(src => MappingHelper.MapToFormattedClientDateTimeText(src.AuditDate, "G")))
                .ForMember(dest => dest.EventType, mo => mo.MapFrom(src => src.EventType.ToString()))
                .ForMember(dest => dest.VisitPayUserId, mo => mo.MapFrom(src => src.VisitPayUserId.ToString()));

            this.CreateMap<JournalEventDto, JournalEventResultsViewModel>()
                .ForMember(dest => dest.EventDate, mo => mo.MapFrom(src => MappingHelper.MapToFormattedClientDateTimeText(src.EventDateTime, "G")))
                .ForMember(dest => dest.VpGuarantorId, mo => mo.MapFrom(src => src.VpGuarantorId == 0 ? "" : src.VpGuarantorId.ToString()))
                .ForMember(dest => dest.EventType, mo => mo.MapFrom(src => src.JournalEventType.ToString()))
                .ForMember(dest => dest.EventHistoryId, mo => mo.MapFrom(src => src.JournalEventId.ToString()))
                .ForMember(dest => dest.IpAddress, mo => mo.MapFrom(src => src.UserHostAddress))
                .ForMember(dest => dest.VisitPayUserId, mo => mo.MapFrom(src => src.VisitPayUserId.ToString()))
                .ForMember(dest => dest.EventVisitPayUserId, mo => mo.MapFrom(src => src.EventVisitPayUserId.ToString()))
                .ForMember(dest => dest.UserAgent, mo => mo.MapFrom(src => src.UserAgent))
                .ForMember(dest => dest.UserName, mo => mo.MapFrom(src => src.EventVisitPayUserIdUserName))
                .ForMember(dest => dest.Application, mo => mo.MapFrom(src => src.Application.ToString()))
                .ForMember(dest => dest.DeviceType, mo => mo.MapFrom(src => src.DeviceType.HasValue ? src.DeviceType.ToString() : null))
                .ForMember(dest => dest.AdditionalDataDictionary, mo => mo.MapFrom(src => src.AdditionalData.IsNullOrEmpty() ? null : JsonConvert.DeserializeObject<IDictionary<string, string>>(src.AdditionalData)));

            // client support requests
            this.CreateMap<GridSearchModel<ClientSupportRequestFilterViewModel>, ClientSupportRequestFilterDto>()
                .ForMember(dest => dest.SortField, opts => opts.MapFrom(src => src.Sidx))
                .ForMember(dest => dest.SortOrder, opts => opts.MapFrom(src => src.Sord))
                .AfterMap((src, dest) => { Mapper.Map(src.Filter, dest); });

            this.CreateMap<ClientSupportRequestFilterViewModel, ClientSupportRequestFilterDto>();

            this.CreateMap<ClientSupportRequestDto, ClientSupportRequestSearchResultViewModel>()
                .ForMember(dest => dest.IsClosed, opts => opts.MapFrom(src => src.ClientSupportRequestStatus == ClientSupportRequestStatusEnum.Closed))
                .ForMember(dest => dest.StatusDisplay, opts => opts.MapFrom(src => src.ClientSupportRequestStatusSource.ClientSupportRequestStatusName))
                .ForMember(dest => dest.TopicDisplay, opts => opts.MapFrom(src => src.TopicDisplay))
                .ForMember(dest => dest.InsertDate, opts => opts.MapFrom(src => MappingHelper.MapToVisitPayDateTimeText(src.InsertDate)))
                .ForMember(dest => dest.LastModifiedDate, opts => opts.MapFrom(src => MappingHelper.MapToVisitPayDateTimeText(src.LastModifiedDate)))
                .ForMember(dest => dest.AssignedToVisitPayUserName, opts => opts.MapFrom(src => src.AssignedVisitPayUser == null ? "&lsaquo;Unassigned&rsaquo;" : MappingHelper.MapUserFullName(src.AssignedVisitPayUser)))
                .ForMember(dest => dest.CreatedByVisitPayUserName, opts => opts.MapFrom(src => MappingHelper.MapUserFullName(src.VisitPayUser)))
                .ForMember(dest => dest.LastModifiedByVisitPayUserName, opts => opts.MapFrom(src => MappingHelper.MapUserFullName(src.LastModifiedByVisitPayUser)))
                .ForMember(dest => dest.TargetVpGuarantorId, opts => opts.MapFrom(src => src.TargetVpGuarantorId.HasValue ? src.TargetVpGuarantorId.Value.ToString() : ""));

            this.CreateMap<ClientSupportTopicDto, ClientSupportTopicViewModel>()
                .ForMember(dest => dest.ClientSupportTopicId, opts => opts.MapFrom(src => src.ClientSupportTopicId))
                .ForMember(dest => dest.ClientSupportTopicName, opts => opts.MapFrom(src => src.ClientSupportTopicName));

            this.CreateMap<ClientSupportRequestDto, ClientSupportRequestViewModel>()
                .ForMember(dest => dest.AssignedVisitPayUserId, opts => opts.MapFrom(src => src.AssignedVisitPayUser == null ? (int?) null : src.AssignedVisitPayUser.VisitPayUserId))
                .ForMember(dest => dest.ClientSupportRequestDisplayId, opts => opts.MapFrom(src => src.ClientSupportRequestDisplayId))
                .ForMember(dest => dest.ClientSupportRequestStatus, opts => opts.MapFrom(src => src.ClientSupportRequestStatus))
                .ForMember(dest => dest.CreatedByVisitPayUserName, opts => opts.MapFrom(src => MappingHelper.MapUserFullName(src.VisitPayUser)))
                .ForMember(dest => dest.CreatedByVisitPayUserEmail, opts => opts.MapFrom(src => src.VisitPayUser.Email))
                .ForMember(dest => dest.CreatedByVisitPayUserId, opts => opts.MapFrom(src => src.VisitPayUser.VisitPayUserId))
                .ForMember(dest => dest.ContactPhone, opts => opts.MapFrom(src => !string.IsNullOrEmpty(src.ContactPhone) ? src.ContactPhone : "N/A"))
                .ForMember(dest => dest.InsertDate, opts => opts.MapFrom(src => MappingHelper.MapToVisitPayDateTimeText(src.InsertDate)))
                .ForMember(dest => dest.ClosedDate, opts => opts.MapFrom(src => MappingHelper.MapToVisitPayDateTimeText(src.ClosedDate)))
                .ForMember(dest => dest.TopicDisplay, opts => opts.MapFrom(src => src.TopicDisplay))
                .ForMember(dest => dest.Attachments, opts => opts.Ignore());

            this.CreateMap<ClientSupportRequestMessageDto, ClientSupportRequestMessageViewModel>()
                .ForMember(dest => dest.MessageBody, opts => opts.MapFrom(src => src.MessageBody.Replace(Environment.NewLine, "<br />").Replace("\n", "<br />").Replace("<br /><br />", "<br />")))
                .ForMember(dest => dest.CreatedByVisitPayUserName, opts => opts.MapFrom(src => MappingHelper.MapUserFullName(src.CreatedByVisitPayUser)))
                .ForMember(dest => dest.InsertDate, opts => opts.MapFrom(src => MappingHelper.MapToVisitPayDateTimeText(src.InsertDate)));

            this.CreateMap<ClientSupportRequestMessageAttachmentDto, ClientSupportRequestMessageAttachmentViewModel>()
                .ForMember(dest => dest.ClientSupportRequestMessageId, opts => opts.MapFrom(src => src.ClientSupportRequestMessage.ClientSupportRequestMessageId))
                .ForMember(dest => dest.InsertDate, opts => opts.MapFrom(src => MappingHelper.MapToVisitPayDateTimeText(src.ClientSupportRequestMessage.InsertDate)));

            // finance plan bucket history
            this.CreateMap<GridSearchModel<FinancePlanBucketHistoryFilterViewModel>, FinancePlanBucketHistoryFilterDto>()
                .AfterMap((src, dest) => { Mapper.Map(src.Filter, dest); });

            this.CreateMap<FinancePlanBucketHistoryFilterViewModel, FinancePlanBucketHistoryFilterDto>();

            this.CreateMap<GridSearchModel<FinancePlanLogFilterViewModel>, FinancePlanLogFilterDto>()
                .AfterMap((src, dest) => { Mapper.Map(src.Filter, dest); });

            this.CreateMap<FinancePlanLogFilterViewModel, FinancePlanLogFilterDto>();
            
            this.CreateMap<FinancePlanBucketHistoryDto, FinancePlanBucketHistorySearchResultViewModel>()
                .ForMember(dest => dest.Bucket, opts => opts.MapFrom(src => src.Bucket < 0 ? "-" : src.Bucket.ToString()))
                .ForMember(dest => dest.Comment, opts => opts.MapFrom(src => src.Comment))
                .ForMember(dest => dest.InsertDate, opts => opts.MapFrom(src => MappingHelper.MapToClientDateTimeText(src.InsertDate)))
                .ForMember(dest => dest.VisitPayUserFullName, opts => opts.MapFrom(src => src.VisitPayUser == null || src.VisitPayUser.VisitPayUserId == SystemUsers.SystemUserId ? SystemUsers.SystemUserName : MappingHelper.MapUserFullName(src.VisitPayUser)));

            this.CreateMap<CommunicationGroupDto, CommunicationGroupSelectionViewModel>();

            this.CreateMap<KnowledgeBaseCategoryDto, KnowledgeBaseCategoryViewModel>();
            this.CreateMap<KnowledgeBaseSubCategoryDto, KnowledgeBaseSubCategoryViewModel>();
            this.CreateMap<KnowledgeBaseQuestionAnswerDto, KnowledgeBaseQuestionAnswerViewModel>();

            this.CreateMap<GuarantorCancellationReasonDto, GuarantorCancellationReasonViewModel>();

            this.CreateMap<BalanceTransferStatusHistoryDto, GuarantorVisitBalanceTransferStatusSearchResultViewModel>()
                .ForMember(dest => dest.BalanceTransferStatusDisplayName, opts => opts.MapFrom(src => src.BalanceTransferStatus.BalanceTransferStatusDisplayName))
                .ForMember(dest => dest.InsertDate, opts => opts.MapFrom(src => MappingHelper.MapToClientDateTimeText(src.InsertDate)))
                .ForMember(dest => dest.VisitPayUserName, opts => opts.MapFrom(src => src.ChangedBy == null || src.ChangedBy.VisitPayUserId == SystemUsers.SystemUserId ? SystemUsers.SystemUserName : MappingHelper.MapUserFullName(src.ChangedBy)));

            this.CreateMap<GridSearchModel<GuarantorVisitBalanceTransferStatusSearchFilterViewModel>, BalanceTransferStatusHistoryFilterDto>()
                .AfterMap((src, dest) => { Mapper.Map(src.Filter, dest); });

            this.CreateMap<GuarantorVisitBalanceTransferStatusSearchFilterViewModel, BalanceTransferStatusHistoryFilterDto>();

            this.CreateMap<HttpContext, HttpContextDto>()
                .ForMember(dest => dest.Url, opts => opts.MapFrom(src => src.Request.Url.ToString()))
                .ForMember(dest => dest.Request, opts => opts.MapFrom(src => src.Request))
                .ForMember(dest => dest.Response, opts => opts.MapFrom(src => src.Response))
                .ForMember(dest => dest.User, opts => opts.MapFrom(src => src.User))
                .ForMember(dest => dest.Session, opts => opts.MapFrom(src => src.Session))
                .ForMember(dest => dest.UserHostAddress, opts => opts.MapFrom(src => src.Request.RequestUserHostAddressString()));

            this.CreateMap<HttpContext, JournalEventHttpContextDto>()
                .ForMember(dest => dest.Url, opts => opts.MapFrom(src => src.Request.Url.AbsolutePath))
                .ForMember(dest => dest.UserHostAddress, opts => opts.MapFrom(src => src.Request.UserHostAddress))
                .ForMember(dest => dest.UserAgent, opts => opts.MapFrom(src => src.Request.UserAgent))
                .ForMember(dest => dest.DeviceType, opts => opts.MapFrom(src => DeviceTypeHelper.GetDeviceType(src.Request.UserAgent)));

            //TODO: remove this temp map once UserEvents are entirely replaced
            this.CreateMap<HttpContextDto, JournalEventHttpContextDto>()
                .ForMember(dest => dest.Url, opts => opts.MapFrom(src => src.Request.Url.AbsolutePath))
                .ForMember(dest => dest.UserHostAddress, opts => opts.MapFrom(src => src.Request.UserHostAddress))
                .ForMember(dest => dest.UserAgent, opts => opts.MapFrom(src => src.Request.UserAgent))
                .ForMember(dest => dest.DeviceType, opts => opts.MapFrom(src => DeviceTypeHelper.GetDeviceType(src.Request.UserAgent)));

            this.CreateMap<VisitPayDocumentFilterViewModel, VisitPayDocumentFilter>();

            this.CreateMap<VisitPayDocumentBaseDto, VisitPayDocumentResultViewModel>()
                .ForMember(dest => dest.ActiveDateString, opts => opts.MapFrom(x => x.ActiveDate.ToDateText()))
                .ForMember(dest => dest.ApprovedDateString, opts => opts.MapFrom(x => x.ApprovedDate.ToDateText()))
                .ForMember(dest => dest.VisitPayDocumentType, opts => opts.MapFrom(src => src.VisitPayDocumentType.ToString()));

            this.CreateMap<VisitPayDocumentResultDto, VisitPayDocumentEditViewModel>()
                .ForMember(x => x.Approved, opts => opts.MapFrom(x => x.ApprovedDate != null))
                .ForMember(dest => dest.ActiveDateString, opts => opts.MapFrom(x => x.ActiveDate.ToDateText()))
                .ForMember(dest => dest.ApprovedDateString, opts => opts.MapFrom(x => x.ApprovedDate.ToDateText()))
                .ForMember(dest => dest.VisitPayDocumentTypeId, opts => opts.MapFrom(src => (int)src.VisitPayDocumentType));

            this.CreateMap<VisitPayDocumentResultDto, VisitPayDocumentResultViewModel>()
                .IncludeBase<VisitPayDocumentBaseDto, VisitPayDocumentResultViewModel>();

            this.CreateMap<AnalyticReportDto, AnalyticReportViewModel>()
                .ForMember(dest => dest.ReportName, opts => opts.MapFrom(x => string.IsNullOrEmpty(x.DisplayName) ? x.ReportName : x.DisplayName));

            //Chat
            this.CreateMap<ChatAvailabilityDto, ChatAvailabilityViewModel>()
                .ForMember(dest => dest.DayOfWeekDisplay, opts => opts.MapFrom(x => x.DayOfWeek.HasValue ? Enum.GetName(typeof(DayOfWeek), x.DayOfWeek) : null))
                .ForMember(dest => dest.Date, opts => opts.MapFrom(x => x.Date.HasValue ? x.Date.Value.Date : x.Date)); //Only care about date, not time

            this.CreateMap<ChatAvailabilityViewModel, ChatAvailabilityDto>()
                .ForMember(dest => dest.Date, opts => opts.MapFrom(x => x.Date.HasValue ? x.Date.Value.Date : x.Date)); //Only care about date, not time
            
            this.CreateMap<RegisterPersonalInformationViewModel, CreateVpGuarantorViewModel>();

            
            this.CreateMap<GuarantorWithLastFinancePlanStatementDto, PaymentQueueGuarantorSearchResultViewModel>()
                .ForMember(x => x.LastAmountDue, opts => opts.MapFrom(x => x.LastFinancePlanStatement != null ? x.LastFinancePlanStatement.StatementedSnapshotFinancePlanPaymentDueSum.HasValue ? x.LastFinancePlanStatement.StatementedSnapshotFinancePlanPaymentDueSum.Value.ToString("C") : "" : "" ))
                .ForMember(x => x.Name, opts => opts.MapFrom(x => x.Guarantor.User.DisplayFullName))
                .ForMember(x => x.FirstName, opts => opts.MapFrom(x => x.Guarantor.User.FirstName))
                .ForMember(x => x.LastName, opts => opts.MapFrom(x => x.Guarantor.User.LastName))
                .ForMember(x => x.VpGuarantorId, opts => opts.MapFrom(x => x.Guarantor.VpGuarantorId))
                .ForMember(x => x.Addresses, opts => opts.MapFrom(x => x.Guarantor.User.DisplayAddresses))
                .ForMember(x => x.PhoneNumbers, opts => opts.MapFrom(x => x.Guarantor.User.PhoneNumbers))
                .ForMember(x => x.HsGuarantorId, opts => opts.MapFrom(x => x.Guarantor.StatementAccountNumber));

            //Payment Queue
            this.CreateMap<PaymentQueueFilterViewModel, PaymentImportFilterDto>()
                .ForMember(dest => dest.DateRangeFrom, opts => opts.MapFrom(x => x.StartDate.HasValue ? x.StartDate : x.DateRangeFrom))
                .ForMember(dest => dest.DateRangeTo, opts => opts.MapFrom(x => x.EndDate))
                .ForMember(dest => dest.PaymentImportState, opts => opts.MapFrom(x => x.PaymentState))
                .ForMember(dest => dest.BatchNumber, opts => opts.MapFrom(x => x.BatchNumber));

            this.CreateMap<PaymentImportDto, PaymentQueueItemViewModel>()
                .ForMember(dest => dest.BatchCreditDateDisplay, opts => opts.MapFrom(x => x.BatchCreditDate.ToString(Format.DateFormat)))
                .ForMember(dest => dest.VpGuarantorId, opts => opts.MapFrom(x => x.Guarantor != null ? x.Guarantor.VpGuarantorId : new int?()))
                .ForMember(dest => dest.PaymentImportState, opts => opts.MapFrom(x => x.PaymentImportStateEnum.ToString()));

            this.CreateMap<PaymentQueueItemViewModel, PaymentImportDto>()
                .ForMember(dest => dest.PaymentImportStateEnum, opts => opts.MapFrom(x => Enum.Parse(typeof(PaymentImportStateEnum), x.PaymentImportState)));

            this.CreateMap<PaymentImportQueueDto, PaymentQueueViewModel>()
                .ForMember(dest => dest.PaymentQueueItems, opts => opts.MapFrom(x => x.PaymentImports.Select(y => Mapper.Map<PaymentQueueItemViewModel>(y)).ToList()))
                .ForMember(dest => dest.AllPaymentsTotal, opts => opts.MapFrom(x => x.AllPaymentsTotal.FormatCurrency()))
                .ForMember(dest => dest.PostedPaymentsTotal, opts => opts.MapFrom(x => x.PostedPaymentsTotal.FormatCurrency()))
                .ForMember(dest => dest.UnpostedPaymentsTotal, opts => opts.MapFrom(x => x.UnpostedPaymentsTotal.FormatCurrency()))
                .ForMember(dest => dest.NonVisitPayPaymentsTotal, opts => opts.MapFrom(x => x.NonVisitPayPaymentsTotal.FormatCurrency()));
        }
    }
}
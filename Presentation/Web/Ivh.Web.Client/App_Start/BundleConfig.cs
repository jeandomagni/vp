﻿using System.Web.Optimization;

namespace Ivh.Web.Client
{
    
    public static class BundleConstants
    {
        public const string ReconfigureFinancePlan = "~/bundles/scripts/financeplan/reconfigureterms";
        public const string CommonFinancePlanSetup = "~/bundles/scripts/common/financeplansetup";
        public const string OfflineCreateFinancePlan = "~/bundles/scripts/offline/createfinanceplan";
        public const string OfflineGuarantorAccount = "~/bundles/scripts/offlineguarantoraccount";
        public const string OfflineCreateVpGuarantor = "~/bundles/scripts/offlinecreatevpguarantor";
    }

    public class BundleConfig
    {
        public static void RegisterBundles(BundleCollection bundles)
        {
            #region javascript libraries and base/layouts

            bundles.Add(new ScriptBundle("~/bundles/scripts/jquery").Include(
                "~/Scripts/Libraries/jquery-3.2.1.js",
                "~/Scripts/Libraries/jquery.validate.js",
                "~/Scripts/Libraries/jquery.validate.unobtrusive.js",
                "~/Scripts/Libraries/jquery.maskedinput.js",
                "~/Scripts/Libraries/inputMask/inputmask.js",
                "~/Scripts/Libraries/inputMask/jquery.inputmask.js",
                "~/Scripts/Libraries/jquery.watermark.js",
                "~/Scripts/Libraries/jquery.blockUI.js",
                "~/Scripts/Libraries/autonumeric/autonumeric.js",
                "~/Scripts/Libraries/moment.js",
                "~/Scripts/Libraries/combodate.js"));

            bundles.Add(new ScriptBundle("~/bundles/scripts/knockout").Include(
                "~/Scripts/Libraries/knockout-3.4.2.js",
                "~/Scripts/Libraries/knockout.mapping-latest.js",
                "~/Scripts/Shared/common.knockout.js",
                "~/Scripts/Shared/common.knockout.extenders.js"));

            bundles.Add(new ScriptBundle("~/bundles/scripts/bootstrap").Include(
                "~/Scripts/Libraries/bootstrap.js",
                "~/Scripts/Libraries/respond.js",
                "~/Scripts/Libraries/bootstrap-datepicker.js",
                "~/Scripts/Libraries/bootstrap-datetimepicker.js",
                "~/Scripts/Libraries/bootstrap-multiselect.js",
                "~/Scripts/Libraries/bootstrap-multiselect-collapsible-groups.js",
                "~/Scripts/Libraries/bootstrap-toggle.js"));

            bundles.Add(new ScriptBundle("~/bundles/scripts/jqueryui").Include(
                "~/Scripts/Libraries/jquery-ui.js",
                "~/Scripts/Libraries/jquery.dialogextend.js"));

            bundles.Add(new StyleBundle("~/bundles/css/jqgrid").Include(
                "~/Scripts/Libraries/jqGrid-4.8.2/css/ui.jqgrid.css"));

            bundles.Add(new ScriptBundle("~/bundles/scripts/jqgrid").Include(
                "~/Scripts/Libraries/jqGrid-4.8.2/js/jquery.jqGrid.src.js",
                "~/Scripts/Libraries/jqGrid-4.8.2/js/i18n/grid.locale-en.js",
                "~/Scripts/Shared/common.jqgrid.js"));

            bundles.Add(new ScriptBundle("~/bundles/scripts/VisitPayBase").Include(
                "~/Scripts/Shared/common.js",
                "~/Scripts/Shared/common.logging.js",
                "~/Scripts/Shared/common.jquery.js",
                "~/Scripts/Shared/global.js",
                "~/Scripts/Shared/validationAdapters.js",
                "~/Scripts/Shared/IdleTimer.js",
                "~/Scripts/Shared/guarantorFilter.js"));

            bundles.Add(new StyleBundle("~/bundles/css/base").Include(
                "~/Content/bootstrap-datepicker3.css",
                "~/Content/bootstrap-datetimepicker.css",
                "~/Content/bootstrap-multiselect.css",
                "~/Content/bootstrap-toggle.css",
                "~/Content/jquery-ui.css",
                "~/Scripts/Libraries/jqGrid-4.8.2/css/jquery-ui.theme.css",
                "~/Scripts/Libraries/jQuery-File-Upload-9.10.6/css/jquery.fileupload.css",
                "~/Scripts/Libraries/jstree/themes/default/style.css"));

            bundles.Add(new ScriptBundle("~/bundles/scripts/sidenav").Include(
                "~/Scripts/Libraries/jquery.overlayScrollbars.js",
                "~/Scripts/Shared/sideNav.js"));

            bundles.Add(new StyleBundle("~/bundles/css/sidenav").Include(
                "~/Content/OverlayScrollbars.css"));

            bundles.Add(new ScriptBundle("~/bundles/scripts/chat").Include(
                "~/Scripts/Chat/chat-dialog.js",
                "~/Scripts/Chat/chat-socket.js",
                "~/Scripts/Chat/chat-indicator.js")); //NOTE: chat-indicator must render after chat-socket.js

            #endregion

            #region account/edit-password

            bundles.Add(new ScriptBundle("~/bundles/scripts/account/edit-password").Include("~/Scripts/Account/edit-password.js"));

            #endregion

            #region /account/reset-password

            bundles.Add(new ScriptBundle("~/bundles/scripts/account/reset-password").Include("~/Scripts/Account/reset-password.js"));

            #endregion

            #region /account/security-setup

            bundles.Add(new ScriptBundle("~/bundles/scripts/account/security-setup").Include("~/Scripts/Account/security-setup.js"));

            #endregion

            #region /account/security-questions

            bundles.Add(new ScriptBundle("~/bundles/scripts/account/security-questions").Include("~/Scripts/Account/security-questions.js"));

            #endregion

            #region /account/forgot-password

            bundles.Add(new ScriptBundle("~/bundles/scripts/account/forgot-password").Include("~/Scripts/Account/forgot-password.js"));

            #endregion

            #region /admin/auditlogreview

            bundles.Add(new ScriptBundle("~/bundles/scripts/admin/auditlogreview").Include("~/Scripts/Admin/auditLogReview.js"));

            #endregion

            #region /admin/auditeventlog

            bundles.Add(new ScriptBundle("~/bundles/scripts/admin/auditeventlog").Include("~/Scripts/Admin/auditEventLog.js"));

            #endregion

            #region /admin/clientusermanagement

            bundles.Add(new ScriptBundle("~/bundles/scripts/admin/clientusermanagement").Include("~/Scripts/Admin/clientUserManagement.js"));

            #endregion

            #region /admin/pciaudit

            bundles.Add(new ScriptBundle("~/bundles/scripts/admin/pciaudit").Include("~/Scripts/Admin/pciAudit.js"));

            #endregion

            #region /clientsupport (user)

            bundles.Add(new ScriptBundle("~/bundles/scripts/clientsupport/clientuser").Include(
                "~/Scripts/ClientSupport/client-support.common.js",
                "~/Scripts/ClientSupport/client-user-support-requests.js"));

            #endregion

            #region /clientsupport (admin)

            bundles.Add(new ScriptBundle("~/bundles/scripts/clientsupport/clientadmin").Include(
                "~/Scripts/Libraries/jstree/jstree.js",
                "~/Scripts/ClientSupport/client-support.common.js",
                "~/Scripts/ClientSupport/client-admin-support-requests.js"));

            #endregion

            #region financeplan/reconfigure

            bundles.Add(new ScriptBundle(BundleConstants.ReconfigureFinancePlan).Include("~/Scripts/FinancePlan/reconfigure-finance-plan.js"));

            #endregion

            #region /guarantor/phonetextsettings

            bundles.Add(new ScriptBundle("~/bundles/scripts/guarantor/phonetextsettings").Include("~/Scripts/Guarantor/phone-text-settings.js"));
            bundles.Add(new ScriptBundle("~/bundles/scripts/guarantor/upgradeguarantor").Include("~/Scripts/Guarantor/guarantor-upgrade.js"));

            #endregion

            #region /search/searchguarantors

            bundles.Add(new ScriptBundle("~/bundles/scripts/search/searchguarantors").Include("~/Scripts/Search/search-guarantors.js",
                "~/Scripts/Search/search-hsguarantors.js",
                "~/Scripts/Search/search-vpguarantors.js"));
            
            #endregion
            
            #region visitpaydocuments

            bundles.Add(new ScriptBundle("~/bundles/scripts/visitpaydocuments").Include("~/Scripts/VisitPayDocument/visit-pay-document.js"));

            #endregion

            #region /search/guarantoraccount

            bundles.Add(new ScriptBundle("~/bundles/scripts/search/guarantoraccount").Include("~/Scripts/Search/guarantor-account.visits.transactions.js",
                "~/Scripts/Search/guarantor-account.visits.statushistory.js",
                "~/Scripts/Search/guarantor-account.visits.aginghistory.js",
                "~/Scripts/Search/guarantor-account.visits.balancetransfer.js",
                "~/Scripts/Search/guarantor-account.visits.balances.js",
                "~/Scripts/Unmatch/visit-unmatch.js",
                "~/Scripts/FinancePlan/financeplan-buckethistory.js",
                "~/Scripts/FinancePlan/financeplan-cancel.js",
                "~/Scripts/FinancePlan/financeplan-interestreallocate.js",
                "~/Scripts/Search/guarantor-account.financeplans.interesthistory.js",
                "~/Scripts/Search/guarantor-account.financeplans.log.js",
                "~/Scripts/Search/guarantor-account.financeplans.visits.js",
                "~/Scripts/Search/guarantor-account.financeplans.js",
                "~/Scripts/Search/guarantor-account.visits.js",
                "~/Scripts/Search/guarantor-account.transactions.js",
                "~/Scripts/Search/guarantor-account.supporthistory.js",
                "~/Scripts/Search/guarantor-account.chathistory.details.js",
                "~/Scripts/Search/guarantor-account.chathistory.js",
                "~/Scripts/Search/guarantor-account.communications.events.js",
                "~/Scripts/Search/guarantor-account.communications.content.js",
                "~/Scripts/Search/guarantor-account.communications.js",
                "~/Scripts/Search/guarantor-account.statements.js",
                "~/Scripts/Search/guarantor-account.changepaymentdueday.js",
                "~/Scripts/Search/guarantor-account.editaccountsettings.js",
                "~/Scripts/Search/guarantor-account.paymentduedayhistory.js",
                "~/Scripts/Search/guarantor-account.itemizations.js",
                "~/Scripts/Consolidate/guarantor-consolidation.js",
                "~/Scripts/Unmatch/guarantor-unmatch.js",
                "~/Scripts/Sso/settings.js",
                "~/Scripts/Search/guarantor-account.js"));

            #endregion

            #region /Invite/inviteguarantor

            bundles.Add(new ScriptBundle("~/bundles/scripts/invite/inviteguarantor").Include("~/Scripts/invite/invite-guarantor.js"));

            #endregion

            #region /payment/arrangepayment

            bundles.Add(new ScriptBundle("~/bundles/scripts/payment/arrangepayment").Include(
                "~/Scripts/Payment/discounts-vm.js",
                "~/Scripts/Payment/secure-pan.js",
                "~/Scripts/Payment/payment-methods-common.js",
                "~/Scripts/Payment/arrange-payment.payment-methods.js",
                "~/Scripts/Payment/arrange-payment.payment.js",
                "~/Scripts/Payment/arrange-payment.finance-plan.js",
                "~/Scripts/Payment/arrange-payment.finance-plan-state-selection.js",
                "~/Scripts/Payment/arrange-payment.main.js"));

            #endregion

            #region /payment/mypayments

            bundles.Add(new ScriptBundle("~/bundles/scripts/payment/mypayments").Include(
                "~/Scripts/Payment/secure-pan.js",
                "~/Scripts/Payment/payment-methods-common.js",
                "~/Scripts/Payment/payment-cancel.js",
                "~/Scripts/Payment/payment-edit.js",
                "~/Scripts/Payment/payment-detail.js",
                "~/Scripts/Payment/payment-pending.js",
                "~/Scripts/Payment/payment-history.js"));

            #endregion

            #region /reports/guarantorsummaryreport

            bundles.Add(new ScriptBundle("~/bundles/scripts/reports/guarantorsummaryreport").Include("~/Scripts/reports/guarantor-summary-report.js"));

            #endregion

            #region /reports/visitunmatchreport

            bundles.Add(new ScriptBundle("~/bundles/scripts/reports/visitunmatchreport").Include("~/Scripts/reports/visit-unmatch-report.js"));

            #endregion

            #region /workqueue/supportrequests

            bundles.Add(new ScriptBundle("~/bundles/scripts/workqueue/supportrequests").Include(
                "~/Scripts/WorkQueue/support-requests.js"
            ));

            #endregion

            #region /workqueue/guarantorattributechanges

            bundles.Add(new ScriptBundle("~/bundles/scripts/workqueue/guarantorattributechanges").Include(
                "~/Scripts/Unmatch/guarantor-unmatch.js",
                "~/Scripts/WorkQueue/guarantor-attribute-changes.js"));

            #endregion

            #region /workqueue/clientsupportrequests

            bundles.Add(new ScriptBundle("~/bundles/scripts/workqueues/clientsupportrequests").Include(
                "~/Scripts/ClientSupport/client-support.common.js",
                "~/Scripts/WorkQueue/manage-client-support-requests.js"));

            #endregion

            #region support request shared

            bundles.Add(new ScriptBundle("~/bundles/scripts/support/supportupload").Include(
                "~/Scripts/Libraries/jQuery-File-Upload-9.10.6/js/tmpl.js",
                "~/Scripts/Libraries/jQuery-File-Upload-9.10.6/js/jquery.ui.widget.js",
                "~/Scripts/Libraries/jQuery-File-Upload-9.10.6/js/jquery.iframe-transport.js",
                "~/Scripts/Libraries/jQuery-File-Upload-9.10.6/js/jquery.fileupload.js",
                "~/Scripts/Libraries/jQuery-File-Upload-9.10.6/js/jquery.fileupload-ui.js",
                "~/Scripts/Libraries/jQuery-File-Upload-9.10.6/js/jquery.fileupload-process.js",
                "~/Scripts/Libraries/jQuery-File-Upload-9.10.6/js/jquery.fileupload-validate.js"));

            bundles.Add(new ScriptBundle("~/bundles/scripts/support/createsupportrequest").Include("~/Scripts/Support/create-support-request.js"));

            bundles.Add(new ScriptBundle("~/bundles/scripts/support/supportrequestaction").Include("~/Scripts/Support/support-request-action.js"));

            bundles.Add(new ScriptBundle("~/bundles/scripts/support/supportrequest").Include("~/Scripts/Libraries/jstree/jstree.js",
                "~/Scripts/Search/guarantor-account.visits.transactions.js",
                "~/Scripts/ClientSupport/client-support.common.js",
                "~/Scripts/Support/support-request-reply.js",
                "~/Scripts/Support/support-request.js"));

            #endregion
            
            #region Analytic Reports
            bundles.Add(new ScriptBundle("~/bundles/scripts/reports/analyticreport").Include(
                "~/Scripts/Libraries/jquery-3.2.1.js",
                "~/scripts/Libraries/powerbi.js"));
            #endregion

            bundles.Add(new ScriptBundle(BundleConstants.CommonFinancePlanSetup).Include(
                "~/Scripts/FinancePlan/finance-plan-setup-models.js",
                "~/Scripts/FinancePlan/finance-plan-setup-common.js"
            ));

            #region offline/createvpguarantor

            bundles.Add(new ScriptBundle(BundleConstants.OfflineCreateVpGuarantor).Include(
                "~/Scripts/Offline/talk-off.js",
                "~/Scripts/Offline/create-vpguarantor.js"));
            
            #endregion
            
            #region offline/createfinanceplan

            bundles.Add(new ScriptBundle(BundleConstants.OfflineCreateFinancePlan).Include(
                "~/Scripts/Payment/secure-pan.js",
                "~/Scripts/Payment/payment-methods-common.js",
                "~/Scripts/Payment/arrange-payment.payment-methods.js",
                "~/Scripts/Payment/arrange-payment.finance-plan.js",
                "~/Scripts/Offline/talk-off.js",
                "~/Scripts/Offline/send-offline-token.js",
                "~/Scripts/Offline/confirm-mailing-address.js",
                "~/Scripts/Offline/offline-create-finance-plan.js"
            ));

            #endregion

            #region offline guarantor account

            bundles.Add(new ScriptBundle(BundleConstants.OfflineGuarantorAccount).Include("~/Scripts/Offline/send-offline-token.js"));

            #endregion
        }
    }
}

﻿using System.Web;

[assembly: PreApplicationStartMethod(typeof(Ivh.Web.Client.ElmahConfig), "Configure")]
namespace Ivh.Web.Client
{
    using Common.Data.Services;

    public static class ElmahConfig
    {
        public static void Configure()
        {
            Common.Elmah.ElmahConfig.Configure(ConnectionStringService.Instance.LoggingConnection.Value);
        }
    }
}
﻿using System.Web.Mvc;
using System.Web.Routing;

namespace Ivh.Web.Client
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");
            routes.IgnoreRoute("{*scss}", new { scss = @".*\.scss(/.*)?" });
            routes.IgnoreRoute("content/client/{*image}", new { image = @".*\.(png|gif|jpg|jpeg|pdf)(/.)?" });

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Account", action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}

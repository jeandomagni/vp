﻿namespace Ivh.Web.Client
{
    using System;
    using System.Web;
    using System.Web.Mvc;
    using System.Web.Routing;
    using Application.Core.Common.Interfaces;
    using Autofac;
    using Common.DependencyInjection;
    using Common.Web.Cookies;
    using Common.Web.Extensions;
    using Domain.Logging.Interfaces;
    using Ivh.Application.Logging.Common.Interfaces;
    using Microsoft.AspNet.Identity;
    using Microsoft.Owin;
    using Microsoft.Owin.Security.Cookies;
    using Middleware;
    using Owin;

    public partial class Startup
    {
        // For more information on configuring authentication, please visit http://go.microsoft.com/fwlink/?LinkId=301864
        public void ConfigureAuth(IAppBuilder app)
        {
            ILoggingApplicationService logger = IvinciContainer.Instance.Container().Resolve<ILoggingApplicationService>();
            logger.Info(() => $"Startup.Auth::ConfigureAuth<IAppBuilder> - Start: {DateTime.UtcNow}");
            IClientApplicationService clientApplicationService = DependencyResolver.Current.GetService<IClientApplicationService>();
            int timeoutMinutes = clientApplicationService.GetClient().ClientSessionTimeOutInMinutes;
            ISecurityStampValidatorApplicationService securityStampValidatorApplicationService = DependencyResolver.Current.GetService<ISecurityStampValidatorApplicationService>();

            // Enable the application to use a cookie to store information for the signed in user
            // and to use a cookie to temporarily store information about a user logging in with a third party login provider
            // Configure the sign in cookie
            app.UseCookieAuthentication(CookieAuthenticationOptionsFactory.GetCookieAuthenticationOptions(
                cookieName: CookieNames.Client.Authentication,
                loginPath: "/Account",
                validateInterval: TimeSpan.FromMinutes(timeoutMinutes),
                onValidateIdentity: securityStampValidatorApplicationService.OnValidateIdentity(validateInterval: TimeSpan.FromMinutes(timeoutMinutes)),
                onApplyRedirect: ctx =>
                {
                    if (ctx.Request.IsAjaxRequest())
                    {
                        return;
                    }

                    ctx.Response.Redirect(ctx.RedirectUri); // web login (default)
                }
            ));
            app.UseExternalSignInCookie(DefaultAuthenticationTypes.ExternalCookie);
            app.Use(typeof(ReportViewerAuthenticationMiddleware));
            logger.Info(() => $"Startup.Auth::ConfigureAuth<IAppBuilder> - End: {DateTime.UtcNow}");
        }
    }
}
﻿namespace Ivh.Web.Client
{
    using System;
    using System.Web.Helpers;
    using Application.Logging.Common.Interfaces;
    using Autofac;
    using Common.Base.Constants;
    using Common.DependencyInjection;
    using Common.Web.Cookies;
    using Domain.Logging.Interfaces;
    using Owin;

    public partial class Startup
    {
        public void ConfigureMisc(IAppBuilder app)
        {
            // for future use
            ILoggingApplicationService logger = IvinciContainer.Instance.Container().Resolve<ILoggingApplicationService>();
            logger.Info(() => $"Startup.Misc::ConfigureMisc<IAppBuilder> - Start:  {DateTime.UtcNow}");
            AntiForgeryConfig.CookieName = CookieNames.Client.AntiForgeryString;
            logger.Info(() => $"Startup.Misc::ConfigureMisc<IAppBuilder> - End:  {DateTime.UtcNow}");

        }
    }
}
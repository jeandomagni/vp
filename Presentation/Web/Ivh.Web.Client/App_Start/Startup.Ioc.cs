﻿namespace Ivh.Web.Client
{
    using System;
    using System.Threading.Tasks;
    using System.Web.Mvc;
    using Application.Logging.Common.Interfaces;
    using Autofac;
    using Common.DependencyInjection;
    using Common.DependencyInjection.Registration;
    using Common.VisitPay.Enums;
    using Common.Web.Extensions;
    using Common.Web.Filters;
    using Domain.Settings.Interfaces;
    using IocSetup;
    using Mappings;
    using Owin;

    public partial class Startup
    {
        public void ConfigureIoc(IAppBuilder app)
        {
            Task mappingTasks = Task.Run(() =>
            {
                ServiceBusMessageSerializationMappingBase.ConfigureMappings();

                using (MappingBuilder mappingBuilder = new MappingBuilder())
                {
                    mappingBuilder
                        .WithBase()
                        .WithProfile<ClientWebMappings>();
                }
            });

            IvinciContainer.Instance.RegisterComponent(new RegisterClientVisitPayMvc(app));

            IDependencyResolver dependencyResolver = IvinciContainer.Instance.GetResolver();
            DependencyResolver.SetResolver(dependencyResolver);

            ILoggingApplicationService logger = IvinciContainer.Instance.Container().Resolve<ILoggingApplicationService>();
            logger.Info(() => $"Startup.Ioc::ConfigureIoc<IAppBuilder> - Start:  {DateTime.UtcNow}");

            app.UseAutofacMiddleware(IvinciContainer.Instance.Container());
            app.UseAutofacMvc();

            // add controller timer in QA
            IApplicationSettingsService applicationSettingsService = dependencyResolver.GetService<IApplicationSettingsService>();
            if (applicationSettingsService.EnvironmentType.Value == IvhEnvironmentTypeEnum.Qa)
            {
                GlobalFilters.Filters.Add(new ControllerTimingAttribute());
            }

            mappingTasks.Wait();

            logger.Info(() => $"Startup.Ioc::ConfigureIoc<IAppBuilder> - End:  {DateTime.UtcNow}");
        }
    }
}
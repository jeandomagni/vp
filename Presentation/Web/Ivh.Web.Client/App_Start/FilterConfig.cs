﻿namespace Ivh.Web.Client
{
    using System.Web.Mvc;
    using Common.Base.Enums;
    using Common.VisitPay.Enums;
    using Common.Web.Attributes;
    using Common.Web.Filters;

    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new LoggingCorrelationAttribute());
            filters.Add(new TransportSecurityPolicyFilterAttribute());
            filters.Add(new RedirectAttribute());
            filters.Add(new HandleErrorAttribute());
            filters.Add(new DisableCacheAttribute());
            filters.Add(new IvinciHandleErrorAttribute());
            filters.Add(new RestrictedCountryCheckAttribute(ApplicationEnum.Client));
        }
    }
}
﻿namespace Ivh.Web.Client.Filters
{
    using System.Web;
    using System.Web.Mvc;
    using Application.Base.Common.Dtos;
    using Application.Core.Common.Dtos;
    using Application.Core.Common.Interfaces;
    using Application.User.Common.Dtos;
    using AutoMapper;
    using Common.Base.Enums;
    using Common.EventJournal;
    using Common.VisitPay.Enums;
    using Common.Web.Extensions;
    using Microsoft.AspNet.Identity;

    public sealed class AccessLoggingFilterAttribute : ActionFilterAttribute
    {
        public JournalEventTypeEnum JournalEventType { get; set; }
        public string ParameterName { get; set; }

        /// <summary>
        /// This uses DependencyResolver to retrieve the services instead of IvinciContainer due to issues with lifecycle for attributes. 
        /// Similar to Ivh.Web.Patient.Attributes.GuarantorAuthorizeAttribute implementation."
        /// </summary>
        /// <param name="filterContext"></param>
        public override void OnActionExecuted(ActionExecutedContext filterContext)
        {
            switch (this.JournalEventType)
            {
                case JournalEventTypeEnum.ClientClientAccountView:

                    int visitPayUserId = 0;

                    if (int.TryParse(((string[]) filterContext.Controller.ValueProvider.GetValue(this.ParameterName).RawValue)[0], out visitPayUserId))
                    {
                        IVisitPayUserApplicationService visitPayUserApplicationService = DependencyResolver.Current.GetService<IVisitPayUserApplicationService>();
                        int vpGuarantorId = visitPayUserApplicationService.GetGuarantorIdFromVisitPayUserId(visitPayUserId);
                        IVisitPayUserJournalEventApplicationService visitPayUserJournalEventApplicationService = DependencyResolver.Current.GetService<IVisitPayUserJournalEventApplicationService>();
                        visitPayUserJournalEventApplicationService.LogClientAccountView(HttpContext.Current.User.CurrentUserId(), vpGuarantorId, Mapper.Map<JournalEventHttpContextDto>(HttpContext.Current));
                    }
                    break;
            }

            base.OnActionExecuted(filterContext);
        }
    }
}
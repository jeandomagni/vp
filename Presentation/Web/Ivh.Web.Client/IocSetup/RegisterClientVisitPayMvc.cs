﻿namespace Ivh.Web.Client.IocSetup
{
    using System.Web;
    using Autofac;
    using Autofac.Integration.Mvc;
    using Common.DependencyInjection;
    using Common.DependencyInjection.Registration;
    using Common.DependencyInjection.Registration.Register;
    using Common.Session;
    using Common.Web.Cookies;
    using Common.Web.Interfaces;
    using Common.Web.Services;
    using MassTransit;
    using Microsoft.Owin;
    using Microsoft.Owin.Security;
    using Owin;
    using Services;

    public class RegisterClientVisitPayMvc : IRegistrationModule
    {
        private readonly IAppBuilder _app;

        public RegisterClientVisitPayMvc(IAppBuilder app) 
        {
            this._app = app;
        }

        public void Register(ContainerBuilder builder)
        {
            RegistrationSettings registrationSettings = RegistrationSettingsService.GetRegistrationSettings();
            Base.Register(builder, registrationSettings);
            VisitPay.Register(builder, registrationSettings, true);
            Data.Register(builder, registrationSettings)
                .AddVisitPayDatabase()
                .AddCdiEtlDatabase()
                .AddEnterpriseDatabase()
                .AddStorageDatabase();
            Mvc.Register(builder, this._app);

            builder.RegisterType<BaseControllerService>().As<IBaseControllerService>().InstancePerRequest();
            builder.RegisterControllers(typeof(MvcApplication).Assembly);
            builder.RegisterType<WebClientSessionFacade>().As<IWebClientSessionFacade>().PreserveExistingDefaults();
            builder.Register<IAuthenticationManager>(c => HttpContext.Current.GetOwinContext().Authentication).InstancePerRequest();
            builder.Register(ctx => HttpContext.Current.GetOwinContext()).As<IOwinContext>();
            builder.RegisterType<GuarantorFilterService>().AsSelf();
            builder.RegisterType<RegistrationService>().As<IRegistrationService>().InstancePerRequest();

            IBusControl serviceBus = IvinciContainer.Instance.Container().Resolve<IBusControl>();
            serviceBus?.Start();
        }
    }
}
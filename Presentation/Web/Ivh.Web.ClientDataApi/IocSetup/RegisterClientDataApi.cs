﻿using Ivh.Common.Data.Connection.Connections;
using Ivh.Domain.Content.Modules;
using Ivh.Domain.Enterprise.Modules;
using Ivh.Domain.Logging.Modules;

namespace Ivh.Web.ClientDataApi.IocSetup
{
    using Autofac;
    using Autofac.Integration.WebApi;
    using Common.DependencyInjection;
    using Common.DependencyInjection.Extensions;
    using Common.DependencyInjection.Registration;
    using Common.DependencyInjection.Registration.Register;
    using Ivh.Domain.Core.Mappings;
    using Owin;

    public class RegisterClientDataApi : IRegistrationModule
    {
        private readonly IAppBuilder _app;

        public RegisterClientDataApi(IAppBuilder app)
        {
            this._app = app;
        }

        public void Register(ContainerBuilder builder)
        {
            RegistrationSettings registrationSettings = RegistrationSettingsService.GetRegistrationSettings(true);
            Base.Register(builder, registrationSettings);
            VisitPay.Register(builder, registrationSettings, true);
            Data.Register(builder, registrationSettings)
                //.AddClientDataApiFluentMappingModules() //TODO: VP-4424 - will supplant this implementation
                .AddVisitPayDatabase()
                .AddCdiEtlDatabase();
            Mvc.Register(builder, this._app);
            builder.RegisterApiControllers(typeof(WebApiApplication).Assembly);
        }
    }
}
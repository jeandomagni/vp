﻿namespace Ivh.Web.ClientDataApi.IocSetup
{
    using Autofac;
    using Common.Data;
    using Common.DependencyInjection.Registration;
    using Owin;
    using Common.Cache;
    using Common.Data.Connection.Connections;
    using Common.DependencyInjection;
    using Domain.Content.Modules;
    using Provider.Content.Modules;

    public class RegisterContentApi : IRegistrationModule
    {
        private readonly IAppBuilder _app;

        public RegisterContentApi(IAppBuilder app)
        {
            this._app = app;
        }

        public void Register(ContainerBuilder builder)
        {
            RegistrationSettings registrationSettings = RegistrationSettingsService.GetRegistrationSettings(true);

            //builder.RegisterType<ContentServiceFluentMappingModule>().As<IFluentMappingModule>();
            builder.RegisterSessionFactory<Ivh.Common.Data.Connection.Connections.VisitPay>();
            builder.RegisterSessionFactory<Logging>();

            builder.RegisterAssemblyModules(typeof(Application.Enterprise.Api.Modules.Module).Assembly);
            builder.RegisterAssemblyModules(typeof(Domain.Enterprise.Api.Modules.Module).Assembly);
            builder.RegisterAssemblyModules(typeof(Provider.Enterprise.Api.Modules.Module).Assembly);

            builder.RegisterAssemblyModules(typeof(Application.Content.Modules.Module).Assembly);
            builder.RegisterModule(new ContentModule(null));

            builder.RegisterModule(new ContentProviderModule(registrationSettings));

            builder.RegisterType<IvinciMemoryCache>().As<ICache>();
        }
    }
}
﻿namespace Ivh.Web.ClientDataApi.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Web.Http;
    using Application.Core.Common.Dtos;
    using Common.Api.Filters;
    using Common.VisitPay.Messages.Matching.Dtos;
    using Ivh.Application.Matching.Common.Interfaces;

    [HmacAuthentication]
    public class MatchingController : ApiController
    {
        private readonly Lazy<IMatchingApplicationService> _matchingApplicationService;

        public MatchingController(Lazy<IMatchingApplicationService> matchingApplicationService)
        {
            this._matchingApplicationService = matchingApplicationService;
        }

        [HttpPost]
        public IHttpActionResult IsGuarantorMatch(GuarantorMatchRequest request)
        {
            GuarantorMatchResult result = this._matchingApplicationService.Value.IsGuarantorMatch(request.MatchData);
            return this.Ok(result);
        }

        [HttpPost]
        public IHttpActionResult AddInitialMatches(GuarantorMatchAddRequest request)
        {
            IList<GuarantorMatchResult> result = this._matchingApplicationService.Value.AddInitialMatch(request.VpGuarantorId, request.MatchData);

            return this.Ok(result);
        }
    }
}
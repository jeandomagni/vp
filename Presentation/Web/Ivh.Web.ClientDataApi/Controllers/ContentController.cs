﻿namespace Ivh.Web.ClientDataApi.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using System.Web;
    using System.Web.Http;
    using Application.Content.Common.Dtos;
    using Application.Content.Common.Interfaces;
    using Autofac.Integration.WebApi;
    using Common.Api.Controllers;
    using Common.Api.Filters;
    using Common.VisitPay.Enums;
    using Ivh.Web.ClientDataApi.Content.Common.TextRegions;

    [AutofacControllerConfiguration]
    [HmacAuthentication]
    [LogAction]
    public class ContentController : BaseApiController
    {
        private readonly Lazy<IContentApplicationService> _contentApplicationService;
        private readonly string _englishLocale = "en-US";
        private readonly string _icelandicLocale = "is-IS";
        private readonly string _spanishLocale = "es-US";

        public ContentController(Lazy<IContentApplicationService> contentApplicationService)
            : base()
        {
            this._contentApplicationService = contentApplicationService;
        }

        [HttpGet]
        [ActionName("GetVersion")]
        public IHttpActionResult GetVersion(CmsRegionEnum cmsRegion, string locale, int? cmsVersionId = null)
        {
            string decodedLocale = !string.IsNullOrEmpty(locale) ? HttpUtility.UrlDecode(locale) : null;
            bool isEnglishLocale = string.Equals(decodedLocale, this._englishLocale, StringComparison.CurrentCultureIgnoreCase);
            CmsVersionDto cmsVersionDto;

            if (cmsVersionId.HasValue)
            {
                cmsVersionDto = this._contentApplicationService.Value.ApiGetSpecificVersion(cmsRegion, cmsVersionId.Value, decodedLocale, false);
                if (!isEnglishLocale && this.IsVersionEmpty(cmsVersionDto))
                {
                    cmsVersionDto = this._contentApplicationService.Value.ApiGetSpecificVersion(cmsRegion, cmsVersionId.Value, this._englishLocale, false);
                }
            }
            else
            {
                cmsVersionDto = this._contentApplicationService.Value.ApiGetCurrentVersion(cmsRegion, decodedLocale, false);
                if (!isEnglishLocale && this.IsVersionEmpty(cmsVersionDto))
                {
                    cmsVersionDto = this._contentApplicationService.Value.ApiGetCurrentVersion(cmsRegion, this._englishLocale, false);
                }
            }
            
            return this.Ok(cmsVersionDto);
        }
        
        [HttpGet]
        [ActionName("GetVersionByRegionNameAndType")]
        public IHttpActionResult GetVersionByRegionNameAndType(string cmsRegionName, CmsTypeEnum cmsType, string defaultLabel, string locale)
        {
            string decodedLocale = !string.IsNullOrEmpty(locale) ? HttpUtility.UrlDecode(locale) : null;
            bool isEnglishLocale = string.Equals(decodedLocale, this._englishLocale, StringComparison.CurrentCultureIgnoreCase);

            CmsVersionDto cmsVersionDto = this._contentApplicationService.Value.ApiGetVersionByRegionNameAndType(HttpUtility.UrlDecode(cmsRegionName), cmsType, HttpUtility.UrlDecode(defaultLabel), decodedLocale);
            if (!isEnglishLocale && this.IsVersionEmpty(cmsVersionDto))
            {
                cmsVersionDto = this._contentApplicationService.Value.ApiGetVersionByRegionNameAndType(HttpUtility.UrlDecode(cmsRegionName), cmsType, HttpUtility.UrlDecode(defaultLabel), this._englishLocale);
            }

            return this.Ok(cmsVersionDto);
        }

        [HttpGet]
        [ActionName("GetCmsVersionNumbers")]
        public IHttpActionResult GetCmsVersionNumbers(CmsRegionEnum cmsRegion, string locale)
        {
            string decodedLocale = !string.IsNullOrEmpty(locale) ? HttpUtility.UrlDecode(locale) : null;
            bool isEnglishLocale = string.Equals(decodedLocale, this._englishLocale, StringComparison.CurrentCultureIgnoreCase);

            IDictionary<int, int> versionNumbers = this._contentApplicationService.Value.ApiGetCmsVersionNumbers(cmsRegion, decodedLocale);
            if (!isEnglishLocale && (versionNumbers == null || versionNumbers.Count == 0))
            {
                versionNumbers = this._contentApplicationService.Value.ApiGetCmsVersionNumbers(cmsRegion, this._englishLocale);
            }
            
            return this.Ok((Dictionary<int, int>)versionNumbers);
        }

        [HttpGet]
        [ActionName("GetContentMenu")]
        public async Task<IHttpActionResult> GetContentMenu(ContentMenuEnum contentMenuEnum)
        {
            return this.Ok(await this._contentApplicationService.Value.GetContentMenuAsync(contentMenuEnum, true));//send everything. Client-side will filter.
        }
        
        [HttpGet]
        [ActionName("GetTextRegion")]
        public IHttpActionResult GetTextRegion(string textRegion, string locale, ApplicationEnum application)
        {
            locale = string.IsNullOrWhiteSpace(locale) ? this._englishLocale : locale;

            string decodedTextRegionQuery = HttpUtility.UrlDecode(textRegion);
            string decodedLocale = HttpUtility.UrlDecode(locale);
            
            /* 
             * GuestPay 
             */
            if (application == ApplicationEnum.GuestPay)
            {
                // es-US
                if (this._spanishLocale.Equals(decodedLocale, StringComparison.CurrentCultureIgnoreCase))
                {
                    return this.Ok(GuestPayTextRegion_es_US.GetTextRegion(decodedTextRegionQuery));
                }
                
                // is-IS
                if (this._icelandicLocale.Equals(decodedLocale, StringComparison.CurrentCultureIgnoreCase))
                {
                    return this.Ok(GuestPayTextRegion_is_IS.GetTextRegion(decodedTextRegionQuery));
                }

                // english or unset
                return this.Ok(GuestPayTextRegion_en_US.GetTextRegion(decodedTextRegionQuery));
            }

            /* 
             * Non-GuestPay 
             */

            // es-US
            if (this._spanishLocale.Equals(decodedLocale, StringComparison.CurrentCultureIgnoreCase))
            {
                return this.Ok(TextRegion_es_US.GetTextRegion(decodedTextRegionQuery));
            }

            // is-IS
            if (this._icelandicLocale.Equals(decodedLocale, StringComparison.CurrentCultureIgnoreCase))
            {
                return this.Ok(TextRegion_is_IS.GetTextRegion(decodedTextRegionQuery));
            }

            // english or unset
            return this.Ok(TextRegion_en_US.GetTextRegion(decodedTextRegionQuery));
        }

        [HttpGet]
        [ActionName("GetAllTextRegions")]
        public IHttpActionResult GetAllTextRegions(string locale, ApplicationEnum application)
        {
            string decodedLocale = HttpUtility.UrlDecode(locale);

            if (application == ApplicationEnum.GuestPay)
            {
                // es-US
                if (this._spanishLocale.Equals(decodedLocale, StringComparison.CurrentCultureIgnoreCase))
                {
                    return this.Ok(GuestPayTextRegion_es_US.ToDictionary());
                }

                // is-IS
                if (this._icelandicLocale.Equals(decodedLocale, StringComparison.CurrentCultureIgnoreCase))
                {
                    return this.Ok(GuestPayTextRegion_is_IS.ToDictionary());
                }

                // english
                if (this._englishLocale.Equals(decodedLocale, StringComparison.CurrentCultureIgnoreCase))
                {
                    return this.Ok(GuestPayTextRegion_en_US.ToDictionary());
                }

                // Language not found for GuestPay, return null
                return this.Ok();
            }

            /* 
            * Non-GuestPay 
            */

            // es-US
            if (this._spanishLocale.Equals(decodedLocale, StringComparison.CurrentCultureIgnoreCase))
            {
                return this.Ok(TextRegion_es_US.ToDictionary());
            }

            // is-IS
            if (this._icelandicLocale.Equals(decodedLocale, StringComparison.CurrentCultureIgnoreCase))
            {
                return this.Ok(TextRegion_is_IS.ToDictionary());
            }

            // english
            if (this._englishLocale.Equals(decodedLocale, StringComparison.CurrentCultureIgnoreCase))
            {
                return this.Ok(TextRegion_en_US.ToDictionary());
            }

            // Language not found, return null
            return this.Ok();
        }

        [HttpGet]
        [ActionName("InvalidateCache")]
        public async Task<IHttpActionResult> InvalidateCache()
        {
            return this.Ok(await this._contentApplicationService.Value.InvalidateCacheAsync());
        }

        private bool IsVersionEmpty(CmsVersionDto cmsVersionDto)
        {
            if (cmsVersionDto == null)
            {
                return true;
            }

            if (cmsVersionDto.CmsVersionId == 0)
            {
                return true;
            }
            
            if (string.IsNullOrWhiteSpace(cmsVersionDto.ContentTitle) && string.IsNullOrWhiteSpace(cmsVersionDto.ContentBody))
            {
                return true;
            }

            return false;
        }
    }
}
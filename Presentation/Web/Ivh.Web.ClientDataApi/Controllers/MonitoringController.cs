﻿namespace Ivh.Web.ClientDataApi.Controllers
{
    using System.Web.Http;
    using Common.Api.Controllers;
    using Common.Api.Filters;

    [HmacAuthentication]
    public class MonitoringController : BaseApiController
    {
        /*
            this action is called from SystemHealthApplicationService.GetAssemblyVersion
            don't remove it without updating the ClientDataApi constant in that file
        */
        [HmacAuthentication]
        [HttpGet]
        public override IHttpActionResult GetAssemblyVersion()
        {
            return base.GetAssemblyVersion();
        }
    }
}
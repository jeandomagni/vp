﻿namespace Ivh.Web.ClientDataApi.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using System.Web.Http;
    using Application.Settings.Common.Interfaces;
    using Autofac.Integration.WebApi;
    using Common.Api.Controllers;
    using Common.Api.Filters;
    using Models;

    [AutofacControllerConfiguration]
    [HmacAuthentication]
    [LogAction]
    public class SettingsController : BaseApiController
    {
        private readonly Lazy<ISettingsApplicationService> _applicationSettingsService;

        public SettingsController(Lazy<ISettingsApplicationService> applicationSettingsService)
            : base()
        {
            this._applicationSettingsService = applicationSettingsService;
        }

        [HttpGet]
        public async Task<IHttpActionResult> GetSettings()
        {
            IReadOnlyDictionary<string, string> result = await this._applicationSettingsService.Value.GetAllSettingsAsync();

            return result != null 
                ? this.Ok(result) 
                : (IHttpActionResult)this.NotFound();
        }

        [HttpGet]
        public async Task<IHttpActionResult> InvalidateCache()
        {
            return this.Ok(await this._applicationSettingsService.Value.InvalidateCacheAsync());
        }

        [HttpPost]
        public async Task<IHttpActionResult> OverrideManagedSetting(OverrideManagedSettingModel model)
        {
            if (model == null || string.IsNullOrWhiteSpace(model.Key))
            {
                return this.Ok(false);
            }

            return this.Ok(await this._applicationSettingsService.Value.OverrideManagedSettingAsync(model.Key, model.OverrideValue));
        }

        [HttpGet]
        public async Task<IHttpActionResult> GetManagedSettings()
        {
            IReadOnlyDictionary<string, string> result = await this._applicationSettingsService.Value.GetAllManagedSettingsAsync();
            return this.Ok(result);
        }

        [HttpGet]
        public async Task<IHttpActionResult> GetEditableManagedSettings()
        {
            IReadOnlyDictionary<string, string> result = await this._applicationSettingsService.Value.GetAllEditableManagedSettingsAsync();
            return this.Ok(result);
        }

        [HttpGet]
        public async Task<IHttpActionResult> ClearManagedSettingOverride(string key)
        {
            return this.Ok(await this._applicationSettingsService.Value.ClearManagedSettingOverrideAsync(key));
        }
        
        [HttpGet]
        public async Task<IHttpActionResult> ClearManagedSettingOverrides()
        {
            return this.Ok(await this._applicationSettingsService.Value.ClearManagedSettingOverridesAsync());
        }

        [HttpGet]
        public IHttpActionResult GetManagedSettingsHash()
        {
            string hash = this._applicationSettingsService.Value.GetManagedSettingsHash();
            return this.Ok(hash);
        }

        [HttpGet]
        public IHttpActionResult GetEditableManagedSettingsHash()
        {
            string hash = this._applicationSettingsService.Value.GetEditableManagedSettingsHash();
            return this.Ok(hash);
        }

    }
}

﻿namespace Ivh.Web.ClientDataApi.Models
{
    public class OverrideManagedSettingModel
    {
        public string Key { get; set; }
        public string OverrideValue { get; set; }
    }
}
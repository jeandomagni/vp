﻿namespace Ivh.Web.ClientDataApi.Models.PaymentSummary
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Runtime.Serialization;

    [Serializable]
    [DataContract]
    public class PaymentSummaryResponse
    {
        [DataMember(Order = 1)]
        public int VpGuarantorId { get; set; }
        [DataMember(Order = 2)]
        public DateTime PostDateBegin { get; set; }
        [DataMember(Order = 3)]
        public DateTime PostDateEnd { get; set; }
        [DataMember(Order = 4)]
        public List<PaymentSummaryTransactionResponse> Payments { get; set; }
        [DataMember(Order = 5)]
        public decimal TotalTransactionAmount
        {
            get { return this.Payments.Sum(x => x.TransactionAmount); }
        }
    }
}
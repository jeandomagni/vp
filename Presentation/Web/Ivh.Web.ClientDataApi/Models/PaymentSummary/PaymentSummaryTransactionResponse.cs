﻿namespace Ivh.Web.ClientDataApi.Models.PaymentSummary
{
    using System;
    using System.Runtime.Serialization;
    using Common.Base.Utilities.Helpers;
    using Common.VisitPay.Enums;

    [Serializable]
    [DataContract]
    public class PaymentSummaryTransactionResponse
    {
        [DataMember(Order = 1)]
        public DateTime PostDate { get; set; }

        [DataMember(Order = 2)]
        public decimal TransactionAmount { get; set; }

        [DataMember(Order = 3)]
        public string TransactionDescription { get; set; }

        [DataMember(Order = 4)]
        public string PatientFirstName { get; set; }

        [DataMember(Order = 5)]
        public string PatientLastName { get; set; }

        [DataMember(Order = 6)]
        public string VisitSourceSystemKeyDisplay { get; set; }
        
        [DataMember(Order = 7)]
        public DateTime? VisitDate { get; set; }

        [DataMember(Order = 8)]
        public string VisitDescription { get; set; }

        [DataMember(Order = 9)]
        public bool IsPatientGuarantor { get; set; }

        [DataMember(Order = 10)]
        public bool IsPatientMinor { get; set; }

        [DataMember(Order = 11)]
        public bool IsConfidential { get; set; }

        [DataMember(Order = 12)]
        public MatchOptionEnum? MatchOption { get; set; }
        
        [DataMember(Order = 13)]
        public int VisitBillingSystemId { get; set; }
        
        [DataMember(Order = 14)]
        public string VisitSourceSystemKey { get; set; }

        [IgnoreDataMember]
        public string PatientLastNameFirstName => FormatHelper.LastNameFirstName(this.PatientLastName, this.PatientFirstName);

        [IgnoreDataMember]
        public string PatientFirstNameLastName => FormatHelper.FirstNameLastName(this.PatientFirstName, this.PatientLastName);
    }
}
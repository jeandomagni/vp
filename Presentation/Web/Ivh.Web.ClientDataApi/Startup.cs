﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartup(typeof(Ivh.Web.ClientDataApi.Startup))]
namespace Ivh.Web.ClientDataApi
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            this.ConfigureIoc(app);
            this.ConfigureAuth(app);
            this.ConfigureMisc(app);
        }
    }
}
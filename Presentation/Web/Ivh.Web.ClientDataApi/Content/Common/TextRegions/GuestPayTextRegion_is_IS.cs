﻿namespace Ivh.Web.ClientDataApi.Content.Common.TextRegions
{
    using System.Collections.Generic;
    using System.Linq;
    using Ivh.Common.Base.Constants;
    using Ivh.Common.VisitPay.Constants;

    public class GuestPayTextRegion_is_IS
    {
        public static string GetTextRegion(string textRegion)
        {
            return TextRegions[textRegion];
        }

        public static Dictionary<int, string> ToDictionary()
        {
            Dictionary<int, string> dictionary = TextRegions.ToDictionary(k => k.Key, v => v.Value);
            return dictionary;
        }

        private static readonly LocalizationDictionary TextRegions = new LocalizationDictionary
        {
            { TextRegionConstants.BankAccount, "Bankareikning" },
            { TextRegionConstants.Checking, "Athuga" },
            { TextRegionConstants.ClickImageToEnlarge, "Smelltu á myndina til að stækka hana" },
            { TextRegionConstants.ClickToEdit, "Smelltu hér til að breyta" },
            { TextRegionConstants.CreditDebitCard, "Credit / debetkort" },
            { TextRegionConstants.EndingIn, "endar í" },
            { TextRegionConstants.EnrollToday, "Skráðu þig í dag" },
            { TextRegionConstants.Legal, "Löglegt" },
            { TextRegionConstants.OutsideTheUnitedStates, "Utan United States" },
            { TextRegionConstants.PleaseWait, "Vinsamlegast bíðið" },
            { TextRegionConstants.PrivacyPolicy, "Friðhelgisstefna"},
            { TextRegionConstants.PrivacyPolicyClientFullName, "[[ClientFullName]] Friðhelgisstefna" },
            { TextRegionConstants.Savings, "Sparnaður" },
            { TextRegionConstants.SessionExpiresIn, "Session þín rennur út í [[TimeRemaining]] sekúndur." },
            { TextRegionConstants.UnitedStates, "United States" },

            { TextRegionConstants.GuestPayAccountNumber, "Reikningsnúmer" },
            { TextRegionConstants.GuestPayAccountType, "Tegund reiknings" },
            { TextRegionConstants.GuestPayAddressCity, "Borg" },
            { TextRegionConstants.GuestPayAddressLine1, "Heimilisfang 1" },
            { TextRegionConstants.GuestPayAddressLine2, "Heimilisfang lína 2" },
            { TextRegionConstants.GuestPayAddressState, "Ríki" },
            { TextRegionConstants.GuestPayAddressZip, "Zip" },
            { TextRegionConstants.GuestPayAmount, "Magn" },
            { TextRegionConstants.GuestPayBankName, "Nafn banka" },
            { TextRegionConstants.GuestPayBillingAddress, "Innheimtu Heimilisfang" },
            { TextRegionConstants.GuestPayButtonAccountCreate, "Búðu til reikning" },
            { TextRegionConstants.GuestPayButtonContinue, "Haltu áfram" },
            { TextRegionConstants.GuestPayCancelSession, "Ef þú vilt hætta við þessa fundi skaltu smella hér" },
            { TextRegionConstants.GuestPayCardNumber, "Kortanúmer" },
            { TextRegionConstants.GuestPayConfirmAccountNumber, "Staðfestu reikningsnúmer" },
            { TextRegionConstants.GuestPayConfirmEmailAddress, "Staðfesta netfang" },
            { TextRegionConstants.GuestPayContactUs, "Hafðu samband við okkur" },
            { TextRegionConstants.GuestPayCreditCardNumber, "Kreditkortanúmer" },
            { TextRegionConstants.GuestPayCvv, TextRegionConstants.GuestPayCvv }, // CVV is CVV
            { TextRegionConstants.GuestPayDoYouWantToContinue, "Viltu halda áfram?" },
            { TextRegionConstants.GuestPayEmailAddress, "Netfang" },
            { TextRegionConstants.GuestPayEmailInvalid, "Ógilt netfang" },
            { TextRegionConstants.GuestPayEmailReceiptSent, "Tölvupóstur hefur verið sendur til staðfestingarbréfsins." },
            { TextRegionConstants.GuestPayEndSession, "Lok þingsins" },
            { TextRegionConstants.GuestPayExpirationDate, "Gildistökudagur" },
            { TextRegionConstants.GuestPayFAQs, "Algengar spurningar" },
            { TextRegionConstants.GuestPayInvalidSubmission, "Ógildur uppgjöf" },
            { TextRegionConstants.GuestPayInvalidZip, "Ógilt zip" },
            { TextRegionConstants.GuestPayIpAddressBlocked, "Þú hefur verið lokað vegna lokaðrar alþjóðlegu IP." },
            { TextRegionConstants.GuestPayHome, "Heim" },
            { TextRegionConstants.GuestPayLegalAgreements, "Lagalegir samningar" },
            { TextRegionConstants.GuestPayMakeAnotherPayment, "Gerðu aðra greiðslu" },
            { TextRegionConstants.GuestPayMenu, "Valmynd" },
            { TextRegionConstants.GuestPayNameFirst, "Fyrsta nafn" },
            { TextRegionConstants.GuestPayNameLast, "Eftirnafn" },
            { TextRegionConstants.GuestPayNameOnCard, "Nafn á korti" },
            { TextRegionConstants.GuestPayNotFound, "Ekki fundið" },
            { TextRegionConstants.GuestPayOneTimePayment, "Einstaklingsgreiðsla" },
            { TextRegionConstants.GuestPayPageNotFound, "Síða ekki fundin!" },
            { TextRegionConstants.GuestPayPaymentAmount, "Greiðslu upphæð" },
            { TextRegionConstants.GuestPayPaymentFailed, "Greiðsla þín mistókst. Vinsamlegast vertu viss um að upplýsingarnar sem þú sendir séu réttar og reyndu aftur." },
            { TextRegionConstants.GuestPayPaymentFailedPotentialCharge, "Þú gætir hafa verið rukkaðir. Vinsamlegast sendu ekki aftur inn og hringdu í stuðning við [[GuestPaySupportPhone]]." },
            { TextRegionConstants.GuestPayPaymentInformationEntered, "Greiðslur upplýsingar inn." },
            { TextRegionConstants.GuestPayPaymentMethod, "Greiðslumáti" },
            { TextRegionConstants.GuestPayPaymentMethodExpired, "Valin greiðslumáti er liðinn. Vinsamlegast veldu annan greiðslumáta." },
            { TextRegionConstants.GuestPayPaymentSummaryPageHeading, "Samantekt greiðslu" },
            { TextRegionConstants.GuestPayPaymentSystemNotResponding, "Greiðslukerfið svarar ekki. Vinsamlegast reyndu aftur eða hringdu í stuðning við [[GuestPaySupportPhone]]." },
            { TextRegionConstants.GuestPayPaymentType, "Greiðsla Tegund" },
            { TextRegionConstants.GuestPayPaymentValidationGeneralError, "Uppgjöf þín er ógild. Vinsamlegast athugaðu hvort upplýsingarnar sem þú slóst inn séu réttar og reyndu aftur." },
            { TextRegionConstants.GuestPayPostalCode, "póstnúmer" },
            { TextRegionConstants.GuestPayPrintCopyOfReceipt, "Prenta eða vista afrit af kvittun mína" },
            { TextRegionConstants.GuestPayPrintOrSaveAsPdf, "Prenta eða vista sem PDF" },
            { TextRegionConstants.GuestPayReturnHome, "Fara heim" },
            { TextRegionConstants.GuestPayRoutingNumber, "Leiðarnúmer" },
            { TextRegionConstants.GuestPaySecurityCodeCvv, "Öryggisnúmer (CVV)" },
            { TextRegionConstants.GuestPaySomethingWentWrong, "Eitthvað fór úrskeiðis!" },
            { TextRegionConstants.GuestPayStateProvinceRegion, "Ríki / hérað / svæði" },
            { TextRegionConstants.GuestPaySubmitPayment, "Sendu inn greiðsluna" },
            { TextRegionConstants.GuestPayTermsOfUse, "Notenda Skilmálar" },
            { TextRegionConstants.GuestPayTermsOfUseMustAccept, "Þú verður að samþykkja notkunarskilmálana." },
            { TextRegionConstants.GuestPayThankYouForYourPayment, "Þakka þér fyrir greiðslu þína." },
            { TextRegionConstants.GuestPayTransactionId, "Viðskiptareikning" },
            { TextRegionConstants.GuestPayType, "Gerð" },
            { TextRegionConstants.GuestPayUnableToFindMatchingGuarantor, "Ekki er hægt að finna samsvörun ábyrgðaraðila" },
            { TextRegionConstants.GuestPayYourAccountHasBeenLocated, "Reikningurinn þinn hefur verið staðsettur." },
            { TextRegionConstants.GuestPayYourSessionIsAboutToExpire, "Þingið er að renna út!" }
        };
    }
}
﻿namespace Ivh.Web.ClientDataApi.Content.Common.TextRegions
{
    using System.Collections.Generic;
    using System.Linq;
    using Ivh.Common.Base.Constants;
    using Ivh.Common.VisitPay.Constants;

    public class GuestPayTextRegion_es_US
    {
        public static string GetTextRegion(string textRegion)
        {
            return TextRegions[textRegion];
        }

        public static Dictionary<int, string> ToDictionary()
        {
            Dictionary<int, string> dictionary = TextRegions.ToDictionary(k => k.Key, v => v.Value);
            return dictionary;
        }

        private static readonly LocalizationDictionary TextRegions = new LocalizationDictionary
        {
            { TextRegionConstants.BankAccount, "Cuenta de Banco" },
            { TextRegionConstants.Checking, "Cuenta de Cheques" },
            { TextRegionConstants.ClickImageToEnlarge, "Haga clic en la Imagen para Ampliar " },
            { TextRegionConstants.ClickToEdit, "Haga clic aquí para editar" },
            { TextRegionConstants.CreditDebitCard, "Tarjeta de Débito/Crédito" },
            { TextRegionConstants.EndingIn, "Terminación de su tarjeta o cuanta de Banco" },
            { TextRegionConstants.EnrollToday, "Suscríbase Hoy" },
            { TextRegionConstants.Legal, "Marco Legal" },
            { TextRegionConstants.OutsideTheUnitedStates, "Fuera de los Estados Unidos" },
            { TextRegionConstants.PleaseWait, "Por favor espere" },
            { TextRegionConstants.PrivacyPolicy, "Política de Privacidad" },
            { TextRegionConstants.PrivacyPolicyClientFullName, "[[ClientFullName]] Política de Privacidad" },
            { TextRegionConstants.Savings, "Ahorros" },
            { TextRegionConstants.SessionExpiresIn, "Su sesión terminara en [[TimeRemaining]] segundos" },
            { TextRegionConstants.UnitedStates, "Estados Unidos" },

            { TextRegionConstants.GuestPayAccountNumber, "Número de Cuenta" },
            { TextRegionConstants.GuestPayAccountType, "Tipo de Cuenta" },
            { TextRegionConstants.GuestPayAddressCity, "Ciudad" },
            { TextRegionConstants.GuestPayAddressLine1, "Dirección 1" },
            { TextRegionConstants.GuestPayAddressLine2, "Dirección 2 (en caso de existir)" },
            { TextRegionConstants.GuestPayAddressState, "Estado" },
            { TextRegionConstants.GuestPayAddressZip, "Código Postal" },
            { TextRegionConstants.GuestPayAmount, "Cantidad a Pagar" },
            { TextRegionConstants.GuestPayBankName, "Nombre del Banco" },
            { TextRegionConstants.GuestPayBillingAddress, "Dirección" },
            { TextRegionConstants.GuestPayButtonAccountCreate, "Crear Cuenta" },
            { TextRegionConstants.GuestPayButtonContinue, "Continuar" },
            { TextRegionConstants.GuestPayCancelSession, "Desea cancelar la Sesión, presione aquí" },
            { TextRegionConstants.GuestPayCardNumber, "Número de Tarjeta" },
            { TextRegionConstants.GuestPayConfirmAccountNumber, "Confirme su Número de Cuenta" },
            { TextRegionConstants.GuestPayConfirmEmailAddress, "Confirme su Correo Electrónico" },
            { TextRegionConstants.GuestPayContactUs, "Contáctenos" },
            { TextRegionConstants.GuestPayCreditCardNumber, "Número de Tarjeta de Crédito" },
            { TextRegionConstants.GuestPayCvv, TextRegionConstants.GuestPayCvv }, // CVV is CVV
            { TextRegionConstants.GuestPayDoYouWantToContinue, "Desea Continuar?" },
            { TextRegionConstants.GuestPayEmailAddress, "Dirección de Correo Electrónico" },
            { TextRegionConstants.GuestPayEmailInvalid, "Correo Electrónico Incorrecto" },
            { TextRegionConstants.GuestPayEmailReceiptSent, "Su recibo de Pago ha sido enviado a su correo electrónico" },
            { TextRegionConstants.GuestPayEndSession, "Terminar Sesión" },
            { TextRegionConstants.GuestPayExpirationDate, "Fecha de Expiración" },
            { TextRegionConstants.GuestPayFAQs, "Preguntas más Frecuentes" },
            { TextRegionConstants.GuestPayInvalidSubmission, "Información Incorrecta" },
            { TextRegionConstants.GuestPayInvalidZip, "Código Postal Incorrecto" },
            { TextRegionConstants.GuestPayIpAddressBlocked, "Su Cuenta ha sido Bloqueada debido al IP Internacional Incorrect" },
            { TextRegionConstants.GuestPayHome, "Pagina Principal" },
            { TextRegionConstants.GuestPayLegalAgreements, "Marco Jurídico" },
            { TextRegionConstants.GuestPayMakeAnotherPayment, "Realizar otro Pago" },
            { TextRegionConstants.GuestPayMenu, "Menú" },
            { TextRegionConstants.GuestPayNameFirst, "Nombre" },
            { TextRegionConstants.GuestPayNameLast, "Apellido" },
            { TextRegionConstants.GuestPayNameOnCard, "Nombre que aparece en la Tarjeta de Débito/Crédito" },
            { TextRegionConstants.GuestPayNotFound, "Información no Encontrada" },
            { TextRegionConstants.GuestPayOneTimePayment, "Realizar Pago Único" },
            { TextRegionConstants.GuestPayPageNotFound, "!Página no encontrada!" },
            { TextRegionConstants.GuestPayPaymentAmount, "Cantidad a Pagar" },
            { TextRegionConstants.GuestPayPaymentFailed, "Su pago no paso. Asegúrese que todos los datos son correctos e intente nuevamente." },
            { TextRegionConstants.GuestPayPaymentFailedPotentialCharge, " Su pago ha sido enviado. No enviarlo otra vez, para cualquier duda comuníquese a [[GuestPaySupportPhone]]." },
            { TextRegionConstants.GuestPayPaymentInformationEntered, "Información del Pago" },
            { TextRegionConstants.GuestPayPaymentMethod, "Método de Pago" },
            { TextRegionConstants.GuestPayPaymentMethodExpired, "El Método de Pago ha Vencido. Elija otro Método de Pago" },
            { TextRegionConstants.GuestPayPaymentSummaryPageHeading, "Resumen de su Pago" },
            { TextRegionConstants.GuestPayPaymentSystemNotResponding, "El Sistema de Pago Electrónico no Funciona. Vuelva a Intentar o llame a Soporte Técnico a" },
            { TextRegionConstants.GuestPayPaymentType, "Forma de Pago" },
            { TextRegionConstants.GuestPayPaymentValidationGeneralError, "Información Incorrecta. Asegúrese que todos los datos son correctos e intente nuevamente." },
            { TextRegionConstants.GuestPayPostalCode, "Código Postal" },
            { TextRegionConstants.GuestPayPrintCopyOfReceipt, "Imprimir o Guardar una copia de su Recibo" },
            { TextRegionConstants.GuestPayPrintOrSaveAsPdf, "Imprimir o Guardar una copia como PDF" },
            { TextRegionConstants.GuestPayReturnHome, "Ir a la Página Principal" },
            { TextRegionConstants.GuestPayRoutingNumber, "Número de Ruta" },
            { TextRegionConstants.GuestPaySecurityCodeCvv, "Código Seguridad (CVV)" },
            { TextRegionConstants.GuestPaySomethingWentWrong, "!Algo Salió Mal!" },
            { TextRegionConstants.GuestPayStateProvinceRegion, "Estado/Provincia/Ciudad" },
            { TextRegionConstants.GuestPaySubmitPayment, "Enviar Pago" },
            { TextRegionConstants.GuestPayTermsOfUse, "Términos y Condiciones de Uso" },
            { TextRegionConstants.GuestPayTermsOfUseMustAccept, "Aceptar los Términos y Condiciones de Uso." },
            { TextRegionConstants.GuestPayThankYouForYourPayment, "Gracias por su Pago" },
            { TextRegionConstants.GuestPayTransactionId, "Número de Confirmación de su Transacción" },
            { TextRegionConstants.GuestPayType, "Tipo" },
            { TextRegionConstants.GuestPayUnableToFindMatchingGuarantor, "Datos del Fiador/Aval no encontrados" },
            { TextRegionConstants.GuestPayYourAccountHasBeenLocated, "Su cuenta ha sido localizada." },
            { TextRegionConstants.GuestPayYourSessionIsAboutToExpire, "Su Sesión ha Terminado." }
        };
    }
}
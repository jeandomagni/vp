﻿namespace Ivh.Web.ClientDataApi.Content.Common.TextRegions
{
    using Ivh.Common.Base.Utilities.Extensions;

    internal class LocalizationDictionary : ExtendedDictionary<int, string>
    {
        public void Add(string key, string value)
        {
            int? consistentHash = this.GetKey(key);
            if (consistentHash == null)
            {
                return;
            }

            this.Add(consistentHash.Value, value);
        }
        
        public string this[string key]
        {
            get
            {
                int? consistentHash = this.GetKey(key);
                if (consistentHash == null)
                {
                    return string.Empty;
                }

                if (this.Dictionary.ContainsKey(consistentHash.Value))
                {
                    return this.Dictionary[consistentHash.Value];
                }

                /*
                 recent changes will put the api in disaster mode if this throws more than once per 5 minutes.
                 it throws a lot when there's missing keys.
                #if DEBUG
                throw new Exception($"The given key \"{key}\" was not found in the dictionary");
                #endif
                */

                return key;
            }
        }

        private int? GetKey(string key)
        {
            if (string.IsNullOrWhiteSpace(key))
            {
                return null;
            }

            return key.GetConsistentHashCode();
        }
    }
}
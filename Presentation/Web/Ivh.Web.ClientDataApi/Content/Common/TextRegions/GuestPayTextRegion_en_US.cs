﻿namespace Ivh.Web.ClientDataApi.Content.Common.TextRegions
{
    using System.Collections.Generic;
    using System.Linq;
    using Ivh.Common.Base.Constants;
    using Ivh.Common.VisitPay.Constants;

    public class GuestPayTextRegion_en_US
    {
        public static string GetTextRegion(string textRegion)
        {
            return TextRegions[textRegion];
        }

        public static Dictionary<int, string> ToDictionary()
        {
            Dictionary<int, string> dictionary = TextRegions.ToDictionary(k => k.Key, v => v.Value);
            return dictionary;
        }

        private static readonly LocalizationDictionary TextRegions = new LocalizationDictionary
        {
            { TextRegionConstants.BankAccount, TextRegionConstants.BankAccount },
            { TextRegionConstants.Checking, TextRegionConstants.Checking },
            { TextRegionConstants.ClickImageToEnlarge, TextRegionConstants.ClickImageToEnlarge },
            { TextRegionConstants.ClickToEdit, TextRegionConstants.ClickToEdit },
            { TextRegionConstants.CreditDebitCard, TextRegionConstants.CreditDebitCard },
            { TextRegionConstants.EndingIn, TextRegionConstants.EndingIn },
            { TextRegionConstants.EnrollToday, TextRegionConstants.EnrollToday },
            { TextRegionConstants.Legal, TextRegionConstants.Legal },
            { TextRegionConstants.OutsideTheUnitedStates, TextRegionConstants.OutsideTheUnitedStates },
            { TextRegionConstants.PleaseWait, TextRegionConstants.PleaseWait },
            { TextRegionConstants.PrivacyPolicy, TextRegionConstants.PrivacyPolicy },
            { TextRegionConstants.PrivacyPolicyClientFullName, TextRegionConstants.PrivacyPolicyClientFullName },
            { TextRegionConstants.Savings, TextRegionConstants.Savings },
            { TextRegionConstants.SessionExpiresIn, TextRegionConstants.SessionExpiresIn },
            { TextRegionConstants.UnitedStates, TextRegionConstants.UnitedStates },
            
            { TextRegionConstants.GuestPayAccountNumber, TextRegionConstants.GuestPayAccountNumber },
            { TextRegionConstants.GuestPayAccountType, TextRegionConstants.GuestPayAccountType },
            { TextRegionConstants.GuestPayAddressCity, TextRegionConstants.GuestPayAddressCity },
            { TextRegionConstants.GuestPayAddressLine1, TextRegionConstants.GuestPayAddressLine1 },
            { TextRegionConstants.GuestPayAddressLine2, TextRegionConstants.GuestPayAddressLine2 },
            { TextRegionConstants.GuestPayAddressState, TextRegionConstants.GuestPayAddressState },
            { TextRegionConstants.GuestPayAddressZip, TextRegionConstants.GuestPayAddressZip },
            { TextRegionConstants.GuestPayAmount, TextRegionConstants.GuestPayAmount },
            { TextRegionConstants.GuestPayBankName, TextRegionConstants.GuestPayBankName },
            { TextRegionConstants.GuestPayBillingAddress, TextRegionConstants.GuestPayBillingAddress },
            { TextRegionConstants.GuestPayButtonAccountCreate, TextRegionConstants.GuestPayButtonAccountCreate },
            { TextRegionConstants.GuestPayButtonContinue, TextRegionConstants.GuestPayButtonContinue },
            { TextRegionConstants.GuestPayCancelSession, TextRegionConstants.GuestPayCancelSession },
            { TextRegionConstants.GuestPayCardNumber, TextRegionConstants.GuestPayCardNumber },
            { TextRegionConstants.GuestPayConfirmAccountNumber, TextRegionConstants.GuestPayConfirmAccountNumber },
            { TextRegionConstants.GuestPayConfirmEmailAddress, TextRegionConstants.GuestPayConfirmEmailAddress },
            { TextRegionConstants.GuestPayContactUs, TextRegionConstants.GuestPayContactUs },
            { TextRegionConstants.GuestPayCreditCardNumber, TextRegionConstants.GuestPayCreditCardNumber },
            { TextRegionConstants.GuestPayCvv, TextRegionConstants.GuestPayCvv },
            { TextRegionConstants.GuestPayDoYouWantToContinue, TextRegionConstants.GuestPayDoYouWantToContinue },
            { TextRegionConstants.GuestPayEmailAddress, TextRegionConstants.GuestPayEmailAddress },
            { TextRegionConstants.GuestPayEmailInvalid, TextRegionConstants.GuestPayEmailInvalid },
            { TextRegionConstants.GuestPayEmailReceiptSent, TextRegionConstants.GuestPayEmailReceiptSent },
            { TextRegionConstants.GuestPayEndSession, TextRegionConstants.GuestPayEndSession },
            { TextRegionConstants.GuestPayExpirationDate, TextRegionConstants.GuestPayExpirationDate },
            { TextRegionConstants.GuestPayFAQs, TextRegionConstants.GuestPayFAQs },
            { TextRegionConstants.GuestPayInvalidSubmission, TextRegionConstants.GuestPayInvalidSubmission },
            { TextRegionConstants.GuestPayInvalidZip, TextRegionConstants.GuestPayInvalidZip },
            { TextRegionConstants.GuestPayIpAddressBlocked, TextRegionConstants.GuestPayIpAddressBlocked },
            { TextRegionConstants.GuestPayHome, TextRegionConstants.GuestPayHome },
            { TextRegionConstants.GuestPayLegalAgreements, TextRegionConstants.GuestPayLegalAgreements },
            { TextRegionConstants.GuestPayMakeAnotherPayment, TextRegionConstants.GuestPayMakeAnotherPayment },
            { TextRegionConstants.GuestPayMenu, TextRegionConstants.GuestPayMenu },
            { TextRegionConstants.GuestPayNameFirst, TextRegionConstants.GuestPayNameFirst },
            { TextRegionConstants.GuestPayNameLast, TextRegionConstants.GuestPayNameLast },
            { TextRegionConstants.GuestPayNameOnCard, TextRegionConstants.GuestPayNameOnCard },
            { TextRegionConstants.GuestPayNotFound, TextRegionConstants.GuestPayNotFound },
            { TextRegionConstants.GuestPayOneTimePayment, TextRegionConstants.GuestPayOneTimePayment },
            { TextRegionConstants.GuestPayPageNotFound, TextRegionConstants.GuestPayPageNotFound },
            { TextRegionConstants.GuestPayPaymentAmount, TextRegionConstants.GuestPayPaymentAmount },
            { TextRegionConstants.GuestPayPaymentFailed, TextRegionConstants.GuestPayPaymentFailed },
            { TextRegionConstants.GuestPayPaymentFailedPotentialCharge, TextRegionConstants.GuestPayPaymentFailedPotentialCharge },
            { TextRegionConstants.GuestPayPaymentInformationEntered, TextRegionConstants.GuestPayPaymentInformationEntered },
            { TextRegionConstants.GuestPayPaymentMethod, TextRegionConstants.GuestPayPaymentMethod },
            { TextRegionConstants.GuestPayPaymentMethodExpired, TextRegionConstants.GuestPayPaymentMethodExpired },
            { TextRegionConstants.GuestPayPaymentSummaryPageHeading, TextRegionConstants.GuestPayPaymentSummaryPageHeading },
            { TextRegionConstants.GuestPayPaymentSystemNotResponding, TextRegionConstants.GuestPayPaymentSystemNotResponding },
            { TextRegionConstants.GuestPayPaymentType, TextRegionConstants.GuestPayPaymentType },
            { TextRegionConstants.GuestPayPaymentValidationGeneralError, TextRegionConstants.GuestPayPaymentValidationGeneralError },
            { TextRegionConstants.GuestPayPostalCode, TextRegionConstants.GuestPayPostalCode },
            { TextRegionConstants.GuestPayPrintCopyOfReceipt, TextRegionConstants.GuestPayPrintCopyOfReceipt },
            { TextRegionConstants.GuestPayPrintOrSaveAsPdf, TextRegionConstants.GuestPayPrintOrSaveAsPdf },
            { TextRegionConstants.GuestPayReturnHome, TextRegionConstants.GuestPayReturnHome },
            { TextRegionConstants.GuestPayRoutingNumber, TextRegionConstants.GuestPayRoutingNumber },
            { TextRegionConstants.GuestPaySecurityCodeCvv, TextRegionConstants.GuestPaySecurityCodeCvv },
            { TextRegionConstants.GuestPaySomethingWentWrong, TextRegionConstants.GuestPaySomethingWentWrong },
            { TextRegionConstants.GuestPayStateProvinceRegion, TextRegionConstants.GuestPayStateProvinceRegion },
            { TextRegionConstants.GuestPaySubmitPayment, TextRegionConstants.GuestPaySubmitPayment },
            { TextRegionConstants.GuestPayTermsOfUse, TextRegionConstants.GuestPayTermsOfUse },
            { TextRegionConstants.GuestPayTermsOfUseMustAccept, TextRegionConstants.GuestPayTermsOfUseMustAccept },
            { TextRegionConstants.GuestPayThankYouForYourPayment, TextRegionConstants.GuestPayThankYouForYourPayment },
            { TextRegionConstants.GuestPayTransactionId, TextRegionConstants.GuestPayTransactionId },
            { TextRegionConstants.GuestPayType, TextRegionConstants.GuestPayType },
            { TextRegionConstants.GuestPayUnableToFindMatchingGuarantor, TextRegionConstants.GuestPayUnableToFindMatchingGuarantor },
            { TextRegionConstants.GuestPayYourAccountHasBeenLocated, TextRegionConstants.GuestPayYourAccountHasBeenLocated },
            { TextRegionConstants.GuestPayYourSessionIsAboutToExpire, TextRegionConstants.GuestPayYourSessionIsAboutToExpire }
        };
    }
}
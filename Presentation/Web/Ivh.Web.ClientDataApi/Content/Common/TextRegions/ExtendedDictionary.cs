﻿namespace Ivh.Web.ClientDataApi.Content.Common.TextRegions
{
    using System;
    using System.Collections;
    using System.Collections.Generic;

    /// <summary>
    /// this throws a nicer error when a key is duplicated, very useful for localization
    /// </summary>
    /// <typeparam name="TKey"></typeparam>
    /// <typeparam name="TValue"></typeparam>
    internal class ExtendedDictionary<TKey, TValue> : IEnumerable<KeyValuePair<TKey, TValue>>
    {
        protected readonly Dictionary<TKey, TValue> Dictionary;

        public ExtendedDictionary()
        {
            this.Dictionary = new Dictionary<TKey, TValue>();
        }

        public void Add(TKey key, TValue value)
        {
            if (!this.Dictionary.ContainsKey(key))
            {
                this.Dictionary.Add(key, value);
            }
            else
            {
                throw new Exception($"The given key \"{key}\" with value \"{value}\" has already been added to the dictionary");
            }
        }

        public IEnumerable<TKey> Keys => this.Dictionary.Keys;

        public bool ContainsKey(TKey key)
        {
            return this.Dictionary.ContainsKey(key);
        }

        public TValue this[TKey key]
        {
            get
            {
                if (!this.Dictionary.ContainsKey(key))
                {
                    throw new KeyNotFoundException($"The given key \"{key}\" was not found in the dictionary.");
                }

                return this.Dictionary[key];
            }
        }

        public IEnumerator<KeyValuePair<TKey, TValue>> GetEnumerator()
        {
            return this.Dictionary.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return this.GetEnumerator();
        }
    }
}
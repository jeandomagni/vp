﻿namespace Ivh.Web.ClientDataApi.Mappings
{
    using AutoMapper;
    using Common.VisitPay.Messages.HospitalData.Dtos;
    using Models.PaymentSummary;

    public class ClientDataApiMappingProfile : Profile
    {
        public ClientDataApiMappingProfile()
        {
            this.CreateMap<PaymentSummaryTransactionDto, PaymentSummaryTransactionResponse>();
            this.CreateMap<PaymentSummaryDto, PaymentSummaryResponse>();
        }
    }
}
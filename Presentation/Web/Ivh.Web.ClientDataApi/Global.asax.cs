﻿using System;
using System.Web.Routing;

namespace Ivh.Web.ClientDataApi
{
    using System.Web.Http;
    using System.Web.Mvc;
    using Application.Logging.Common.Interfaces;
    using Autofac;
    using Common.Base.Utilities.Helpers;
    using Common.DependencyInjection;
    using Common.Web.Administration;
    using Common.Web.Application;
    
    public class WebApiApplication : HttpApplicationBase
    {
        protected override void Application_Start()
        {
            ViewEngines.Engines.Clear();
            ViewEngines.Engines.Add(new RazorViewEngine());
            /*
            var ve = new RazorViewEngine();
            ve.ViewLocationCache = new TwoLevelViewCache(ve.ViewLocationCache);
            ViewEngines.Engines.Add(ve);
             */
             
            AreaRegistration.RegisterAllAreas();
            //GlobalConfiguration.Configure(WebApiConfig.Register);
            RouteConfig.RegisterRoutes(RouteTable.Routes);

            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            //BundleConfig.RegisterBundles(BundleTable.Bundles);

            GlobalConfiguration.Configuration.Formatters.XmlFormatter.SupportedMediaTypes.Clear();
            ApplicationHost.ConfigureStartUp();

            base.Application_Start();
        }

        protected void Application_Error(object sender, EventArgs e)
        {
            try
            {
                ILoggingApplicationService logger = IvinciContainer.Instance.Container().Resolve<ILoggingApplicationService>();
                Exception ex = this.Server.GetLastError();
                OperationCanceledException operationCanceledException = ex as OperationCanceledException;
                if (operationCanceledException != null && operationCanceledException.CancellationToken.IsCancellationRequested)
                {
                    logger.Warn(() => $"ClientDataApi::Application_Error, exception = {ExceptionHelper.AggregateExceptionToString(ex)}");
                }
                else
                {
                    logger.Fatal(() => $"ClientDataApi::Application_Error, exception = {ExceptionHelper.AggregateExceptionToString(ex)}");
                }
            }
            catch (Exception)
            {
                //
            }
        }
    }
}

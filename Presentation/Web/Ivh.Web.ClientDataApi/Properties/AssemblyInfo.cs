using System.Reflection;
using System.Runtime.InteropServices;
using Ivh.Common.Assembly.Attributes;
using Ivh.Common.Assembly.Constants;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("Ivh.Web.ClientDataApi")]
[assembly: AssemblyDescription("")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany(AssemblyCompany.Ivh)]
[assembly: AssemblyProduct("Ivh.Web.ClientDataApi")]
[assembly: AssemblyCopyright(AssemblyCopyright.Ivh)]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]
[assembly: AssemblyDomain(AssemblyDomain.Hospital)]
[assembly: AssemblyApplicationLayer(AssemblyApplicationLayer.Web)]

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("f7d9c953-5e04-40b2-aa76-387bee7b1640")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Revision and Build Numbers 
// by using the '*' as shown below:
[assembly: AssemblyVersion("1.0.0.0")]
[assembly: AssemblyFileVersion("1.0.0.0")]

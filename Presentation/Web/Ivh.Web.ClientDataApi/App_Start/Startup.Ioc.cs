﻿namespace Ivh.Web.ClientDataApi
{
    using System.Threading.Tasks;
    using System.Web.Http;
    using System.Web.Mvc;
    using Autofac.Integration.WebApi;
    using Autofac;
    using Common.DependencyInjection;
    using Common.DependencyInjection.Registration;
    using Owin;
    using Common.Web.Extensions;
    using IocSetup;
    using Mappings;
    using MassTransit;

    public partial class Startup
    {
        public void ConfigureIoc(IAppBuilder app)
        {
            Task mappingTasks = Task.Run(() =>
            {
                ServiceBusMessageSerializationMappingBase.ConfigureMappings();
                using (MappingBuilder mappingBuilder = new MappingBuilder())
                {
                    mappingBuilder
                        .WithBase()
                        .WithProfile<ClientDataApiMappingProfile>();
                }
            });

            IvinciContainer.Instance.RegisterComponent(new RegisterClientDataApi(app));
            IvinciContainer.Instance.RegisterComponent(new RegisterContentApi(app));
            
            IBusControl serviceBus = IvinciContainer.Instance.Container().Resolve<IBusControl>();
            serviceBus?.Start();

            GlobalConfiguration.Configuration.DependencyResolver = new AutofacWebApiDependencyResolver(IvinciContainer.Instance.Container());
            DependencyResolver.SetResolver(IvinciContainer.Instance.GetResolver());

            app.UseAutofacMiddleware(IvinciContainer.Instance.Container());
            app.UseAutofacWebApi(GlobalConfiguration.Configuration);
            app.UseWebApi(GlobalConfiguration.Configuration);

            mappingTasks.Wait();
        }
    }
}
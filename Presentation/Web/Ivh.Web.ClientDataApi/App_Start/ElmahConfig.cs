﻿using System.Web;
using Ivh.Web.ClientDataApi;

[assembly: PreApplicationStartMethod(typeof(ElmahConfig), "Configure")]
namespace Ivh.Web.ClientDataApi
{
    using Common.Data.Services;

    public static class ElmahConfig
    {
        public static void Configure()
        {
            Common.Elmah.ElmahConfig.Configure(ConnectionStringService.Instance.LoggingConnection.Value);
        }
    }
}
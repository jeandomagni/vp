﻿namespace Ivh.Web.Tests.Attributes
{
    using System;
    using System.IO;
    using System.Linq;
    using System.Security.Claims;
    using System.Security.Principal;
    using System.Web;
    using System.Web.Mvc;
    using Application.Core.Common.Dtos;
    using Application.Core.Common.Interfaces;
    using Common.VisitPay.Enums;
    using Moq;
    using NUnit.Framework;
    using Patient.Attributes;

    [TestFixture]
    public class GuarantorAuthorizeAttributeTests
    {
        [Test]
        public void GuarantorAuthorize_AllowsAnonymous()
        {
            this.SetupTest(null);
            AuthorizationContext authorizationContext = this.SetupAuthorizationContext(typeof(AllowAnonymousAttribute));
            new GuarantorAuthorizeAttribute().Authorize(authorizationContext);
            
            Assert.IsNull(authorizationContext.Result);
        }

        [Test]
        public void GuarantorAuthorize_NoGuarantor()
        {
            this.SetupTest(null);
            AuthorizationContext authorizationContext = this.SetupAuthorizationContext(null);
            new GuarantorAuthorizeAttribute().Authorize(authorizationContext);
            
            Assert.AreEqual(typeof(HttpUnauthorizedResult), authorizationContext.Result?.GetType());
        }

        [TestCase(GuarantorTypeEnum.Online, VpGuarantorStatusEnum.Active, null)]
        [TestCase(GuarantorTypeEnum.Online, VpGuarantorStatusEnum.Closed, typeof(HttpUnauthorizedResult))]
        [TestCase(GuarantorTypeEnum.Offline, VpGuarantorStatusEnum.Active, typeof(HttpUnauthorizedResult))]
        [TestCase(GuarantorTypeEnum.Offline, VpGuarantorStatusEnum.Closed, typeof(HttpUnauthorizedResult))]
        public void GuarantorAuthorize(GuarantorTypeEnum type, VpGuarantorStatusEnum status, Type expectedResult)
        {
            GuarantorDto guarantorDto = new GuarantorDto
            {
                VpGuarantorId = 1,
                VpGuarantorTypeEnum = type,
                VpGuarantorStatus = status
            };

            this.SetupTest(guarantorDto);
            AuthorizationContext authorizationContext = this.SetupAuthorizationContext(null);
            new GuarantorAuthorizeAttribute().Authorize(authorizationContext);
            
            Assert.AreEqual(expectedResult, authorizationContext.Result?.GetType());
        }

        [TestCase(GuarantorTypeEnum.Online, VpGuarantorStatusEnum.Active, null)]
        [TestCase(GuarantorTypeEnum.Online, VpGuarantorStatusEnum.Closed, typeof(HttpUnauthorizedResult))]
        [TestCase(GuarantorTypeEnum.Offline, VpGuarantorStatusEnum.Active, null)]
        [TestCase(GuarantorTypeEnum.Offline, VpGuarantorStatusEnum.Closed, typeof(HttpUnauthorizedResult))]
        public void GuarantorAuthorize_AllowOfflineAttribute(GuarantorTypeEnum type, VpGuarantorStatusEnum status, Type expectedResult)
        {
            GuarantorDto guarantorDto = new GuarantorDto
            {
                VpGuarantorId = 1,
                VpGuarantorTypeEnum = type,
                VpGuarantorStatus = status
            };
            
            this.SetupTest(guarantorDto);
            AuthorizationContext authorizationContext = this.SetupAuthorizationContext(typeof(AllowOffline));
            new GuarantorAuthorizeAttribute().Authorize(authorizationContext);

            Assert.AreEqual(expectedResult, authorizationContext.Result?.GetType());
        }
        
        [TestCase(GuarantorTypeEnum.Online, VpGuarantorStatusEnum.Active, typeof(HttpNotFoundResult))]
        [TestCase(GuarantorTypeEnum.Online, VpGuarantorStatusEnum.Closed, typeof(HttpUnauthorizedResult))]
        [TestCase(GuarantorTypeEnum.Offline, VpGuarantorStatusEnum.Active, null)]
        [TestCase(GuarantorTypeEnum.Offline, VpGuarantorStatusEnum.Closed, typeof(HttpUnauthorizedResult))]
        public void GuarantorAuthorize_OfflineOnlyAttribute(GuarantorTypeEnum type, VpGuarantorStatusEnum status, Type expectedResult)
        {
            GuarantorDto guarantorDto = new GuarantorDto
            {
                VpGuarantorId = 1,
                VpGuarantorTypeEnum = type,
                VpGuarantorStatus = status
            };
            
            this.SetupTest(guarantorDto);
            AuthorizationContext authorizationContext = this.SetupAuthorizationContext(typeof(AllowOffline), typeof(OfflineOnly));
            new GuarantorAuthorizeAttribute().Authorize(authorizationContext);

            Assert.AreEqual(expectedResult, authorizationContext.Result?.GetType());
        }

        private void SetupTest(GuarantorDto guarantorDto)
        {
            Mock<IGuarantorApplicationService> service = new Mock<IGuarantorApplicationService>();
            if (guarantorDto != null)
            {
                service.Setup(x => x.GetGuarantor(guarantorDto.VpGuarantorId)).Returns(guarantorDto);
            }
            this.SetupHttpContext(service, guarantorDto?.VpGuarantorId);
        }

        private void SetupHttpContext(Mock<IGuarantorApplicationService> service, int? vpGuarantorId)
        {
            // DependencyResolver
            Mock<IDependencyResolver> mockResolver = new Mock<IDependencyResolver>();
            mockResolver.Setup(s => s.GetService(typeof(IGuarantorApplicationService))).Returns(service.Object);
            DependencyResolver.SetResolver(mockResolver.Object);

            // Identity
            Mock<ClaimsIdentity> mockIdentity = new Mock<ClaimsIdentity>();
            mockIdentity.Setup(x => x.IsAuthenticated).Returns(true);
            mockIdentity.Setup(x => x.FindFirst(nameof(ClaimTypeEnum.GuarantorId))).Returns((string s) => new Claim(s, vpGuarantorId.ToString()));

            Mock<IPrincipal> mockUser = new Mock<IPrincipal>();
            mockUser.Setup(x => x.Identity).Returns(() => mockIdentity.Object);
            
            // HttpContext
            HttpContext.Current = new HttpContext(new HttpRequest("", "http://tempuri.org", ""), new HttpResponse(new StringWriter()))
            {
                User = mockUser.Object
            };
        }

        private AuthorizationContext SetupAuthorizationContext(params Type[] actionAttributeTypes)
        {
            Mock<AuthorizationContext> mockContext = new Mock<AuthorizationContext>();
            mockContext.Setup(x => x.HttpContext).Returns(new HttpContextWrapper(HttpContext.Current));
            mockContext.Setup(x => x.ActionDescriptor).Returns(() =>
            {
                Mock<ActionDescriptor> d = new Mock<ActionDescriptor>();
                d.Setup(x => x.ControllerDescriptor).Returns(new Mock<ControllerDescriptor>().Object);

                if (actionAttributeTypes != null && actionAttributeTypes.Any())
                {
                    foreach (Type actionAttributeType in actionAttributeTypes)
                    {
                        d.Setup(x => x.IsDefined(actionAttributeType, true)).Returns(true);
                    }
                }

                return d.Object;
            });

            return mockContext.Object;
        }
    }
}
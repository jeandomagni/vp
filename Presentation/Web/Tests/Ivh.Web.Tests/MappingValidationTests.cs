﻿namespace Ivh.Web.Tests
{
    using Client.Mappings;
    using Common.Tests;
    using GuestPay.Mappings;
    using NUnit.Framework;
    using Patient.Mappings;

    public class MappingValidationTests
    {
        [Test]
        [NonParallelizable]
        public void TestClientMappings()
        {
            MappingValidator.Validate(mappingBuilder =>
            {
                mappingBuilder
                    .WithProfile<ClientWebMappings>();
            });
        }

        [Test]
        [NonParallelizable]
        public void TestGuestPayMappings()
        {
            MappingValidator.Validate(mappingBuilder =>
            {
                mappingBuilder
                    .WithProfile<GuestPayMappingProfile>();
            });
        }

        [Test]
        [NonParallelizable]
        public void TestPatientMappings()
        {
            MappingValidator.Validate(mappingBuilder =>
            {
                mappingBuilder
                    .WithProfile<PatientWebMappings>();
            });
        }
    }
}
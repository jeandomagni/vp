﻿namespace Ivh.Web.Tests
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web.Mvc;
    using Application.Core.Common.Dtos;
    using Application.Core.Common.Interfaces;
    using Application.Guarantor.Common.Dtos;
    using AutoMapper;
    using Client.Mappings;
    using Common.DependencyInjection.Registration;
    using Common.Tests.Helpers.Registration;
    using Common.VisitPay.Constants;
    using Common.VisitPay.Enums;
    using Common.Web.Interfaces;
    using Common.Web.Models;
    using Ivh.Application.Matching.Common.Interfaces;
    using Moq;
    using Newtonsoft.Json;
    using NUnit.Framework;

    [TestFixture]
    public class RegistrationServiceTests
    {
        [OneTimeSetUp]
        public void OneTimeSetUp()
        {
            using (MappingBuilder mappingBuilder = new MappingBuilder())
            {
                mappingBuilder
                    .WithProfile<ClientWebMappings>();
            }
        }

        [OneTimeTearDown]
        public void OneTimeTearDown()
        {
            Mapper.Reset();
        }

        #region GetPatientDateOfBirth Tests
        [Test]
        public void When_patient_dob_not_configured_it_is_null()
        {
            RegistrationServiceMockBuilder mockBuilder = new RegistrationServiceMockBuilder();
            this.SetUpRegistrationMatchConfigurationForPatientDateOfBirth(mockBuilder, false, false, false);
            IRegistrationService service = mockBuilder.CreateService();
            RegisterPersonalInformationViewModel vm = new RegisterPersonalInformationViewModel(null, null, null);
            DateTime patientDOB = DateTime.Now.AddYears(-20).Date;
            DateTime guarantorDOB = DateTime.Now.AddYears(-40).Date;

            vm.PatientDateOfBirthMonth = patientDOB.Month.ToString();
            vm.PatientDateOfBirthDay = patientDOB.Day.ToString();
            vm.PatientDateOfBirthYear = patientDOB.Year.ToString();

            DateTime? patientDobCalculated = service.GetPatientDateOfBirth(vm, guarantorDOB);

            Assert.IsNull(patientDobCalculated);
        }

        [Test]
        public void When_patient_dob_not_configured_but_matchingApi_is_it_uses_matchingApiConfig()
        {
            RegistrationServiceMockBuilder mockBuilder = new RegistrationServiceMockBuilder();
            this.SetUpRegistrationMatchConfigurationForPatientDateOfBirth(mockBuilder, false, false, true);
            IRegistrationService service = mockBuilder.CreateService();
            RegisterPersonalInformationViewModel vm = new RegisterPersonalInformationViewModel(null, null, null);
            DateTime patientDOB = DateTime.Now.AddYears(-20).Date;
            DateTime guarantorDOB = DateTime.Now.AddYears(-40).Date;

            vm.PatientDateOfBirthMonth = patientDOB.Month.ToString();
            vm.PatientDateOfBirthDay = patientDOB.Day.ToString();
            vm.PatientDateOfBirthYear = patientDOB.Year.ToString();
            vm.ShowPatientDateOfBirth = true;

            DateTime? patientDobCalculated = service.GetPatientDateOfBirth(vm, guarantorDOB);
            RegisterPersonalInformationViewModel model = service.GetRegisterPersonalInformationViewModel(false);

            Assert.AreEqual(patientDobCalculated, patientDOB);

            Assert.True(model.ConfigPatientDateOfBirth.Required, $"{nameof(model.ConfigPatientDateOfBirth)} should be required");
            Assert.False(model.ConfigSsn.Required, $"{nameof(model.ConfigSsn)} should not be required");
            Assert.True(model.ConfigZip.Required, $"{nameof(model.ConfigZip)} should be required");
        }

        [Test]
        public void When_provided_with_patient_dob_it_returns_correctly()
        {
            RegistrationServiceMockBuilder mockBuilder = new RegistrationServiceMockBuilder();
            IRegistrationService service = mockBuilder.CreateService();
            RegisterPersonalInformationViewModel vm = new RegisterPersonalInformationViewModel(null, null, null);
            DateTime patientDOB = DateTime.Now.AddYears(-20).Date;
            DateTime guarantorDOB = DateTime.Now.AddYears(-40).Date;

            vm.PatientDateOfBirthMonth = patientDOB.Month.ToString();
            vm.PatientDateOfBirthDay = patientDOB.Day.ToString();
            vm.PatientDateOfBirthYear = patientDOB.Year.ToString();
            vm.ShowPatientDateOfBirth = true;

            DateTime? patientDobCalculated = service.GetPatientDateOfBirth(vm, guarantorDOB);

            Assert.AreEqual(patientDobCalculated, patientDOB);
        }

        [Test]
        public void When_show_patient_dob_false_but_is_visible_guarantor_dob_is_used()
        {
            RegistrationServiceMockBuilder mockBuilder = new RegistrationServiceMockBuilder();
            this.SetUpRegistrationMatchConfigurationForPatientDateOfBirth(mockBuilder, false, true, false);
            IRegistrationService service = mockBuilder.CreateService();
            RegisterPersonalInformationViewModel vm = new RegisterPersonalInformationViewModel(null, null, null);
            DateTime patientDOB = DateTime.Now.AddYears(-20).Date;
            DateTime guarantorDOB = DateTime.Now.AddYears(-40).Date;

            vm.PatientDateOfBirthMonth = patientDOB.Month.ToString();
            vm.PatientDateOfBirthDay = patientDOB.Day.ToString();
            vm.PatientDateOfBirthYear = patientDOB.Year.ToString();

            DateTime? patientDobCalculated = service.GetPatientDateOfBirth(vm, guarantorDOB);

            Assert.AreEqual(guarantorDOB, patientDobCalculated);
        }
        #endregion


        #region ValidateRecords
        [Test]
        public void NullDateOfBirth_returns_invalid()
        {
            RegistrationServiceMockBuilder mockBuilder = new RegistrationServiceMockBuilder();
            this.SetUpMockbuilderForValidation(mockBuilder);
            IRegistrationService service = mockBuilder.CreateService();
            RegisterPersonalInformationViewModel vm = new RegisterPersonalInformationViewModel(null, null, null);
            vm.GuarantorId = new []{"foo"};
            ModelStateDictionary modelState = new ModelStateDictionary();
            service.ValidateRecords(modelState, vm);
            
            Assert.AreEqual(3, modelState.Count);
            string errorMessage = modelState.Values.SelectMany(x => x.Errors).FirstOrDefault(x => x.ErrorMessage?.Length > 0)?.ErrorMessage;
            
            Assert.AreEqual(TextRegionConstants.DobInvalid, errorMessage);
        }

        [Test]
        public void ValidDateOfBirth()
        {
            RegistrationServiceMockBuilder mockBuilder = new RegistrationServiceMockBuilder();
            this.SetUpMockbuilderForValidation(mockBuilder);
            IRegistrationService service = mockBuilder.CreateService();
            RegisterPersonalInformationViewModel vm = new RegisterPersonalInformationViewModel(null, null, null);
            DateTime dateOfBirth = DateTime.Now.AddYears(-20).Date;

            vm.DateOfBirthMonth = dateOfBirth.Month.ToString();
            vm.DateOfBirthDay = dateOfBirth.Day.ToString();
            vm.DateOfBirthYear = dateOfBirth.Year.ToString();
            vm.GuarantorId = new[] { "foo" };
            ModelStateDictionary modelState = new ModelStateDictionary();
            service.ValidateRecords(modelState, vm);

            Assert.AreEqual(0, modelState.Count);
        }

        [Test]
        public void DateOfBirth_must_be_18()
        {
            RegistrationServiceMockBuilder mockBuilder = new RegistrationServiceMockBuilder();
            this.SetUpMockbuilderForValidation(mockBuilder);
            IRegistrationService service = mockBuilder.CreateService();
            RegisterPersonalInformationViewModel vm = new RegisterPersonalInformationViewModel(null, null, null);
            DateTime dateOfBirth = DateTime.Now.AddYears(-16).Date;

            vm.DateOfBirthMonth = dateOfBirth.Month.ToString();
            vm.DateOfBirthDay = dateOfBirth.Day.ToString();
            vm.DateOfBirthYear = dateOfBirth.Year.ToString();
            vm.GuarantorId = new[] { "foo" };
            ModelStateDictionary modelState = new ModelStateDictionary();
            service.ValidateRecords(modelState, vm);

            Assert.AreEqual(3, modelState.Count);

            string errorMessage = modelState.Values.SelectMany(x => x.Errors).FirstOrDefault(x => x.ErrorMessage?.Length > 0)?.ErrorMessage;

            Assert.AreEqual(TextRegionConstants.DobMinAge, errorMessage);
        }

        [Test]
        public void Invalid_GuarantorId()
        {
            RegistrationServiceMockBuilder mockBuilder = new RegistrationServiceMockBuilder();
            this.SetUpMockbuilderForValidation(mockBuilder);
            IRegistrationService service = mockBuilder.CreateService();
            RegisterPersonalInformationViewModel vm = new RegisterPersonalInformationViewModel(null, null, null);
            DateTime dateOfBirth = DateTime.Now.AddYears(-20).Date;

            vm.DateOfBirthMonth = dateOfBirth.Month.ToString();
            vm.DateOfBirthDay = dateOfBirth.Day.ToString();
            vm.DateOfBirthYear = dateOfBirth.Year.ToString();

            ModelStateDictionary modelState = new ModelStateDictionary();
            service.ValidateRecords(modelState, vm);

            Assert.AreEqual(1, modelState.Count);

            string errorMessage = modelState.Values.SelectMany(x => x.Errors).FirstOrDefault(x => x.ErrorMessage?.Length > 0)?.ErrorMessage;

            Assert.True(errorMessage?.Contains("Acct Number"));
        }
        #endregion


        private void SetUpRegistrationMatchConfigurationForPatientDateOfBirth(RegistrationServiceMockBuilder mockBuilder, bool required, bool visible, bool matchingApiEnabled)
        {
            List<RegistrationMatchFieldConfigurationDto> registrationMatchFieldConfigurationDtos = new List<RegistrationMatchFieldConfigurationDto>();

            registrationMatchFieldConfigurationDtos.Add(new RegistrationMatchFieldConfigurationDto
            {
                Field = RegistrationMatchFieldEnum.PatientDateOfBirth,
                Required = required,
                Visible = visible
            });

            Mock<IClientApplicationService> mockClientApplicationService = new Mock<IClientApplicationService>();
            mockClientApplicationService.Setup(x => x.GetClient()).Returns(() => new ClientDto
            {
                RegistrationMatchConfiguration = JsonConvert.SerializeObject(registrationMatchFieldConfigurationDtos),
                RegistrationIdConfiguration = "[{placeholder : \"Acct Number\"}]"
            });

            mockBuilder.ClientApplicationServiceMock = mockClientApplicationService;

            Mock<IFeatureApplicationService> mockFeatureAppSerice = new Mock<IFeatureApplicationService>();
            mockFeatureAppSerice.Setup(x => x.IsFeatureEnabled(VisitPayFeatureEnum.FeatureMatchingApiIsEnabled)).Returns(matchingApiEnabled);
            mockBuilder.FeatureApplicationServiceMock = mockFeatureAppSerice;

            IList<string> guarantorFields = new List<string> { "PostalCode" };
            IList<string> patientFields = new List<string> { "DOB" };

            Mock<IMatchingApplicationService> mockMatchingAppService = new Mock<IMatchingApplicationService>();
            mockMatchingAppService.Setup(x => x.GetRequiredMatchFieldsGuarantor(It.IsAny<ApplicationEnum>(), It.IsAny<PatternUseEnum>())).Returns(guarantorFields);
            mockMatchingAppService.Setup(x => x.GetRequiredMatchFieldsPatient(It.IsAny<ApplicationEnum>(), It.IsAny<PatternUseEnum>())).Returns(patientFields);
            mockBuilder.MatchingApplicationServiceMock = mockMatchingAppService;
        }

        private void SetUpMockbuilderForValidation(RegistrationServiceMockBuilder mockBuilder)
        {
            Mock<IClientApplicationService> mockClientApplicationService = new Mock<IClientApplicationService>();
            mockClientApplicationService.Setup(x => x.GetClient()).Returns(() => new ClientDto
            {
                RegistrationIdConfiguration = "[{placeholder : \"Acct Number\"}]",
                RegistrationGuarantorIdentifierTextLong = "Acct Number"
            });

            mockBuilder.ClientApplicationServiceMock = mockClientApplicationService;

            mockBuilder.ContentApplicationServiceMock.Setup(x => x.GetTextRegion(It.IsAny<string>(), It.IsAny<IDictionary<string, string>>())).Returns((string s, IDictionary<string, string> d) => s);
        }
    }
}
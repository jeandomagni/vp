﻿namespace Ivh.Web.Tests
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Security.Claims;
    using System.Security.Principal;
    using System.Web;
    using System.Web.Mvc;
    using Application.Core.Common.Dtos;
    using Application.Core.Common.Interfaces;
    using Application.FinanceManagement.Common.Dtos;
    using AutoMapper;
    using Common.DependencyInjection.Registration;
    using Common.VisitPay.Enums;
    using Common.Web.Interfaces;
    using Common.Web.Models.Account;
    using Common.Web.Models.Statement;
    using Common.Web.Services;
    using Common.Web.Utilities;
    using Domain.Guarantor.Entities;
    using Domain.Visit.Entities;
    using Moq;
    using NUnit.Framework;
    using Patient.Mappings;
    
    public class MaskingTests
    {
        private const string MaskedReplacementValue = "THIS_IS_THE_REPLACEMENT_VALUE";

        [OneTimeSetUp]
        public void OneTimeSetUp()
        {
            // same registrations as patient Startup.Ioc.cs
            string[] destinationNamespaces = {"Ivh.Common.Web", "Ivh.Web"};

            using (MappingBuilder mappingBuilder = new MappingBuilder())
            {
                mappingBuilder
                    .WithBase()
                    .WithProfile<PatientWebMappings>()
                    .WithMasking(destinationNamespaces, MaskingHelper.AfterMapAction);
            }
        }
        
        [OneTimeTearDown]
        public void OneTimeTearDown()
        {
            Mapper.Reset();
        }
        
        [Test]
        public void Mask_Masks()
        {
            const string visitDescription = "visit description";
            const string transactionDescription = "transaction description";

            SetupMatchingMaskingDependencies();

            Visit visit = new Visit
            {
                HsGuarantorMap = new HsGuarantorMap
                {
                    MatchOption = MatchOptionEnum.LnameMinorNoSsnNoDob
                },
                VisitDescription = visitDescription
            };
            
            VisitDto visitDto = Mapper.Map<VisitDto>(visit);
            VisitDto selfMapping = Mapper.Map<VisitDto>(visitDto);
            Assert.AreNotEqual(visitDto, selfMapping); // self map broke reference
            Assert.AreEqual(visitDescription, visitDto.VisitDescription); // didn't change the source
            Assert.AreEqual(MaskedReplacementValue, selfMapping.VisitDescription); // did change the destination
            VisitsSearchResultViewModel m1 = Mapper.Map<VisitsSearchResultViewModel>(visitDto);
            Assert.AreEqual(MaskedReplacementValue, m1.VisitDescription);

            StatementVisitDto statementVisitDto = new StatementVisitDto
            {
                Visit = visitDto
            };
            StatementVisitViewModel m2 = Mapper.Map<StatementVisitViewModel>(statementVisitDto);
            Assert.AreEqual(MaskedReplacementValue, m2.Description);

            VisitTransaction transaction = new VisitTransaction
            {
                Visit = visit,
                TransactionDescription = transactionDescription,
                VpTransactionType = new VpTransactionType
                {
                    VpTransactionTypeId = (int)VpTransactionTypeEnum.HsCharge
                }
            };
            VisitTransactionDto visitTransactionDto = Mapper.Map<VisitTransactionDto>(transaction);
            VisitTransactionDto selfMapping2 = Mapper.Map<VisitTransactionDto>(visitTransactionDto);
            Assert.AreNotEqual(visitTransactionDto, selfMapping2);
            VisitTransactionsSearchResultViewModel m3 = Mapper.Map<VisitTransactionsSearchResultViewModel>(visitTransactionDto);
            Assert.AreEqual(MaskedReplacementValue, m3.TransactionDescription);

            // PaymentSummaryGroupedDto paymentSummaryGroupedDto = new PaymentSummaryGroupedDto { VisitDescription = visitDescription};
            // PaymentSummaryGroupedViewModel m4 = Mapper.Map<PaymentSummaryGroupedViewModel> (paymentSummaryGroupedDto);
            // Assert.AreEqual(MaskedReplacementValue, m4.VisitDescription);
        }
        
        private const int ManagedGuarantorId = 1;
        private const int ManagingGuarantorId = 2;

        [TestCase(ManagedGuarantorId, false)]
        [TestCase(ManagingGuarantorId, true)]
        public void ManagedUserDetailsAreMasked(int loggedInVpGuarantorId, bool shouldMask)
        {
            const string visitDescription = "the original visit description";

            SetupConsolidationMaskingDependencies(loggedInVpGuarantorId, true);

            Guarantor managedGuarantor = new Guarantor
            {
                VpGuarantorId = ManagedGuarantorId,
            };

            managedGuarantor.ManagingConsolidationGuarantors = new List<ConsolidationGuarantor>
            {
                new ConsolidationGuarantor
                {
                    ManagedGuarantor = managedGuarantor,
                    ConsolidationGuarantorStatus = ConsolidationGuarantorStatusEnum.Accepted,
                    ManagingGuarantor = new Guarantor {VpGuarantorId = ManagedGuarantorId}
                }
            };

            Visit visit = new Visit
            {
                IsPatientGuarantor = false,
                IsPatientMinor = false,
                VPGuarantor = managedGuarantor,
                VisitDescription = visitDescription
            };
            
            VisitDto visitDto = Mapper.Map<VisitDto>(visit);
            VisitDto redactedDto = Mapper.Map<VisitDto>(visitDto);
            Assert.AreNotSame(visitDto, redactedDto);

            if (shouldMask)
            {
                Assert.AreEqual(MaskedReplacementValue, redactedDto.VisitDescription);
            }
            else
            {
                Assert.AreEqual(visitDescription, redactedDto.VisitDescription);
            }
        }

        [TestCase(MatchOptionEnum.LnameMinorNoSsnNoDob, true)]
        [TestCase(MatchOptionEnum.SsnDobLname, false)]
        [TestCase(null, false)]
        public void MatchingOptionMasks(MatchOptionEnum? matchOption, bool shouldMask)
        {
            const string visitDescription = "the original visit description";

            SetupMatchingMaskingDependencies();
            
            Visit visit = new Visit
            {
                HsGuarantorMap = new HsGuarantorMap
                {
                    MatchOption = matchOption
                },
                VisitDescription = visitDescription
            };
            
            VisitDto visitDto = Mapper.Map<VisitDto>(visit);
            VisitDto redactedDto = Mapper.Map<VisitDto>(visitDto);
            Assert.AreNotSame(visitDto, redactedDto);

            if (shouldMask)
            {
                Assert.AreEqual(MaskedReplacementValue, redactedDto.VisitDescription);
            }
            else
            {
                Assert.AreEqual(visitDescription, redactedDto.VisitDescription);
            }
        }
        
        private static void SetupConsolidationMaskingDependencies(int loggedInVpGuarantorId, bool isFeatureEnabled)
        {
            Mock<IClientApplicationService> mockClientApplicationService = new Mock<IClientApplicationService>();
            mockClientApplicationService.Setup(x => x.GetClient()).Returns(() => new ClientDto
            {
                MaskedReplacementValue = MaskedReplacementValue
            });

            Mock<IFeatureApplicationService> mockFeatureApplicationService = new Mock<IFeatureApplicationService>();
            mockFeatureApplicationService.Setup(x => x.IsFeatureEnabled(VisitPayFeatureEnum.ConsolidationMasking)).Returns(() => isFeatureEnabled);

            // DependencyResolver
            Mock<IDependencyResolver> mockResolver = new Mock<IDependencyResolver>();
            mockResolver.Setup(s => s.GetService(typeof(IMaskingService))).Returns(() => new MaskingService(
                new Lazy<IClientApplicationService>(() => mockClientApplicationService.Object),
                new Lazy<IFeatureApplicationService>(() => mockFeatureApplicationService.Object)));
            
            DependencyResolver.SetResolver(mockResolver.Object);

            // Identity
            Mock<ClaimsIdentity> mockIdentity = new Mock<ClaimsIdentity>();
            mockIdentity.Setup(x => x.FindFirst(nameof(ClaimTypeEnum.GuarantorId))).Returns((string s) => new Claim(s, loggedInVpGuarantorId.ToString()));

            Mock<IPrincipal> mockUser = new Mock<IPrincipal>();
            mockUser.Setup(x => x.Identity).Returns(() => mockIdentity.Object);
            
            // HttpContext
            HttpContext.Current = new HttpContext(new HttpRequest("", "http://tempuri.org", ""), new HttpResponse(new StringWriter()))
            {
                User = mockUser.Object
            };
        }

        private static void SetupMatchingMaskingDependencies()
        {
            Mock<IClientApplicationService> mockClientApplicationService = new Mock<IClientApplicationService>();
            mockClientApplicationService.Setup(x => x.GetClient()).Returns(() => new ClientDto
            {
                MaskedReplacementValue = MaskedReplacementValue,
                MatchOptionMaskingConfiguration = new List<int> { (int)MatchOptionEnum.LnameMinorNoSsnNoDob }
            });
            
            // DependencyResolver
            Mock<IDependencyResolver> mockResolver = new Mock<IDependencyResolver>();
            mockResolver.Setup(s => s.GetService(typeof(IMaskingService))).Returns(() => new MaskingService(
                new Lazy<IClientApplicationService>(() => mockClientApplicationService.Object),
                new Lazy<IFeatureApplicationService>(() => new Mock<IFeatureApplicationService>().Object)));
            
            DependencyResolver.SetResolver(mockResolver.Object);
        }
    }
}
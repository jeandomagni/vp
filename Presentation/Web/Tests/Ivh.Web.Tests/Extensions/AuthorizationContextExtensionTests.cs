﻿namespace Ivh.Web.Tests.Extensions
{
    using System.Web.Mvc;
    using Common.Web.Extensions;
    using Moq;
    using NUnit.Framework;
    using Patient.Attributes;

    [TestFixture]
    public class AuthorizationContextExtensionTests
    {
        [Test]
        public void FindsAttributeOnAction()
        {
            Mock<AuthorizationContext> mockContext = new Mock<AuthorizationContext>();
            mockContext.Setup(x => x.ActionDescriptor).Returns(() =>
            {
                Mock<ActionDescriptor> a = new Mock<ActionDescriptor>();
                a.Setup(x => x.ControllerDescriptor).Returns(new Mock<ControllerDescriptor>().Object);
                a.Setup(x => x.IsDefined(typeof(AllowOffline), true)).Returns(true);

                return a.Object;
            });

            Assert.True(mockContext.Object.IsAttributeDefined<AllowOffline>());
        }

        [Test]
        public void FindsAttributeOnController()
        {
            Mock<AuthorizationContext> mockContext = new Mock<AuthorizationContext>();
            mockContext.Setup(x => x.ActionDescriptor).Returns(() =>
            {
                Mock<ControllerDescriptor> c = new Mock<ControllerDescriptor>();
                c.Setup(x => x.IsDefined(typeof(AllowOffline), true)).Returns(true);

                Mock<ActionDescriptor> a = new Mock<ActionDescriptor>();
                a.Setup(x => x.ControllerDescriptor).Returns(c.Object);

                return a.Object;
            });

            Assert.True(mockContext.Object.IsAttributeDefined<AllowOffline>());
        }
    }
}
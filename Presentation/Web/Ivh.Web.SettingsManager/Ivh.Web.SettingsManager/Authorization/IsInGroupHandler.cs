﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Ivh.Web.SettingsManager.Authorization
{
    using Helpers;
    using Microsoft.AspNetCore.Authorization;

    public class IsInGroupHandler : AuthorizationHandler<IsInGroupRequirement>
    {
        protected override Task HandleRequirementAsync(AuthorizationHandlerContext context, IsInGroupRequirement requirement)
        {
            IEnumerable<string> userGroups = ActiveDirectoryGroupHelper.FindGroupsBySid(context.User);
            IEnumerable<string> requirementGroups = requirement.Groups.ToList();

            bool isInGroup = requirementGroups.Intersect(userGroups).Any();

            if (isInGroup)
            {
                context.Succeed(requirement);
            }
            return Task.CompletedTask;
        }
    }
}

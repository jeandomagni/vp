﻿namespace Ivh.Web.SettingsManager.Authorization
{
    using Microsoft.AspNetCore.Authorization;

    public class IsInGroupRequirement : IAuthorizationRequirement
    {
        public string[] Groups { get; private set; }

        public IsInGroupRequirement(params string[] groups)
        {
            this.Groups = groups;
        }
    }
}

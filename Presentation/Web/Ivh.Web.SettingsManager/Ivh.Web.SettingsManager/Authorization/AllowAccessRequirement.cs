﻿namespace Ivh.Web.SettingsManager.Authorization
{
    using Microsoft.AspNetCore.Authorization;

    /// <summary>
    /// This is intended for local development only
    /// </summary>
    public class AllowAccessRequirement : IAuthorizationRequirement
    {
        public bool AllowAccess { get; private set; }

        public AllowAccessRequirement(bool allowAccess)
        {
            this.AllowAccess = allowAccess;
        }
    }
}

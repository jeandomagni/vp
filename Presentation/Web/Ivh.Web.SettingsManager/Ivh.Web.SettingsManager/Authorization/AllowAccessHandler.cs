﻿using System.Threading.Tasks;

namespace Ivh.Web.SettingsManager.Authorization
{
    using Microsoft.AspNetCore.Authorization;

    public class AllowAccessHandler : AuthorizationHandler<AllowAccessRequirement>
    {
        protected override Task HandleRequirementAsync(AuthorizationHandlerContext context, AllowAccessRequirement requirement)
        {
            if (requirement.AllowAccess)
            {
                context.Succeed(requirement);
            }
            return Task.CompletedTask;
        }
    }
}

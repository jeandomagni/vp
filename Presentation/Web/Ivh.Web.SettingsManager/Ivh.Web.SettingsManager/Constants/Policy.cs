﻿namespace Ivh.Web.SettingsManager.Constants
{
    public static class Policy
    {
        public const string ViewSettings = "RequireViewSettings";
        public const string AddSettings = "RequireAddSettings";
        public const string OverrideSettings = "RequireOverrideSettings";
        public const string ApproveSettings = "RequireApproveSettings";
        public const string DeploySettings = "RequireDeploySettings";
        public const string DeployLocalSettings = "RequireDeployLocalSettings";
        public const string SettingsManagerApplication = "RequireSettingsManagerApplication";
    }
}

﻿namespace Ivh.Web.SettingsManager.Constants
{
    public static class ConfigKey
    {
        public const string ViewSettings = "Role.ViewSettings";
        public const string AddSettings = "Role.AddSettings";
        public const string OverrideSettings = "Role.OverrideSettings";
        public const string ApproveSettings = "Role.ApproveSettings";
        public const string DeploySettings = "Role.DeploySettings";
        public const string DeployLocalSettings = "Role.DeployLocalSettings";
        public const string SettingsManagerApplication = "Role.SettingsManagerApplication";
    }
}

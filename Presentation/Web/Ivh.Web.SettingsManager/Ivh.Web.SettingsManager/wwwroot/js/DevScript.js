﻿var idRelease = "dropdownRelease";

var clientTag = '{client}';
var environmentTag = '{environment}';
var releaseTag = '{release}';

var settingsApiUrl = '../api/settings/sqlmerge/' + clientTag + '/' + environmentTag + '/' + releaseTag;

function setLatestReleaseAsDefault() {
    var releaseSel = document.getElementById(idRelease);
    releaseSel.selectedIndex = releaseSel.options.length - 1;
}

function getDeployContext() {
    return {
        client: $("#dropdownClient").val(),
        environment: $("#dropdownEnvironment").val(),
        release: $("#" + idRelease).val()
    }
}

function getSettingsApiUrl(deployContext) {
    var url = settingsApiUrl.replace(clientTag, deployContext.client).replace(environmentTag, deployContext.environment).replace(releaseTag, deployContext.release);
    return url;
}

function getMergeScript() {
    var deployContext = getDeployContext();
    var url = getSettingsApiUrl(deployContext);
    $("#textAreaSQL").val(""); //clear previous results
    $.when(
        $.get(url)
        )
        .done(function (data) {
            $("#textAreaSQL").val(data);
        })
        .fail(function () {
            //handle errors
        });
}

$(document).ready(function () {
    $("#buttonGenerate").click(getMergeScript);
    setLatestReleaseAsDefault();
});
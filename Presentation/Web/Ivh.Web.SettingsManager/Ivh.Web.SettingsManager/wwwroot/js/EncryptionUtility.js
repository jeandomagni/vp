﻿var generateKeyApiUrl = "../api/settings/encryption/generatekey";
var encryptValueApiUrl = "../api/settings/encryption/encryptvalue";
var decryptSettingsApiUrl = "../api/settings/encryption/decryptsettings";
var clearDecryptSettingsApiUrl = "../api/settings/encryption/cleardecryptsettings";

function generateKey() {

    $.when(
        $.get(generateKeyApiUrl)
    )
        .done(function (data) {
            $("#txtGeneratedKey").val(data);
        })
        .fail(function () {
            //handle errors
        });
}

function encryptValue() {

    $.when(
        $.post(encryptValueApiUrl, {
            secretMessage: $("#txtOriginalValue").val(),
            cryptKey: $("#txtEncryptionKey").val(),
            authKey: $("#txtAuthorizationKey").val(),
            randomLength: $("#numSaltLength").val()
        })
    )
        .done(function (data) {
            $("#txtEncryptedValue").val(data);
        })
        .fail(function () {
            //handle errors
        });
}

function setDecryptionSettings() {

    $.when(
        $.post(decryptSettingsApiUrl, {
            authKey: $("#txtAuthorizationKeyDecrypt").val(),
            cryptKey: $("#txtEncryptionKeyDecrypt").val(),
            randomLength: $("#numSaltLengthDecrypt").val()
        })
    )
        .done(function (data) {
            $('#successHeading').text('Save Decryption Settings');
            $('#successMessage').text('Decryption settings have been saved.');
            $('#successModal').modal('show');
        })
        .fail(function () {
            $('#errorHeading').text('Error - Decryption Settings');
            $('#errorMessage').text('Decryption settings failed to save.');
            $('#errorModal').modal('show');
        });
}

function clearDecryptionSettings() {

    $.when(
        $.get(clearDecryptSettingsApiUrl, {})
    )
        .done(function (data) {
            $('#successHeading').text('Clear Decryption Settings');
            $('#successMessage').text('Decryption settings have been cleared.');
            $('#successModal').modal('show');
            $("#txtAuthorizationKeyDecrypt").val('');
            $("#txtEncryptionKeyDecrypt").val('');
            $("#numSaltLengthDecrypt").val(0);
        })
        .fail(function () {
            $('#errorHeading').text('Error - Clear Decryption Settings');
            $('#errorMessage').text('Decryption settings failed to cleared.');
            $('#errorModal').modal('show');
        });
}

$(document).ready(function () {
    $("#btnGenerateKey").click(generateKey);
    $("#btnEncryptValue").click(encryptValue);
    $("#btnSetDecryptionSettings").click(setDecryptionSettings);
    $("#btnClearDecryptionSettings").click(clearDecryptionSettings);
});
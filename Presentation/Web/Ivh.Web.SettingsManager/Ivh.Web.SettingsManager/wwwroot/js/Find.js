﻿var idClient = "dropdownClient";
var idEnvironment = "dropdownEnvironment";
var idRelease = "dropdownRelease";
var idSettingsGrid = "settingsGrid";
var idButtonApprove = 'buttonApprove';
var idLabelApprove = 'labelApprove';
var idHiddenHashCode = 'hiddenHashCode';

var findApiUrl = "../api/settings/max/bootgrid";
var settingsApprovalHashUrl = "../api/settings/hash/approval";
var settingsHashUrl = "../api/settings/hash";
var settingsAddApprovalUrl = "../api/settings/approval/add";

function escapeHtml(str) {
    var div = document.createElement('div');
    div.appendChild(document.createTextNode(str));
    return div.innerHTML;
}

function setApprovalDisplayOptions(currentHashCode, approvalHashCode) {

    var buttonApproval = $("#" + idButtonApprove);
    var labelApproval = $("#" + idLabelApprove);
    var hiddenHashCode = $("#" + idHiddenHashCode);

    hiddenHashCode.val(currentHashCode);

    if (currentHashCode === '' && approvalHashCode === '') {
        buttonApproval.hide();
        labelApproval.hide();
    } else if (approvalHashCode === '') {
        buttonApproval.show();
        labelApproval.hide();
        buttonApproval.prop('value', 'Approve Settings');
    } else if (currentHashCode !== approvalHashCode) {
        buttonApproval.show();
        labelApproval.hide();
        buttonApproval.prop('value', 'Reapprove Settings');
    } else {
        buttonApproval.hide();
        labelApproval.text("Approved");
        labelApproval.show();
    }
}

function reloadGrid() {
    $("#" + idSettingsGrid).bootgrid("clear").bootgrid("reload");
}

function getDeploymentContext() {
    var deploymentContext = {};
    deploymentContext.ClientId = document.getElementById(idClient).value;
    deploymentContext.EnvironmentId = document.getElementById(idEnvironment).value;
    deploymentContext.ReleaseId = document.getElementById(idRelease).value;
    deploymentContext.HashCode = '';
    return deploymentContext;
}

function calculateApprovalDisplayOptions() {
    var deploymentContext = getDeploymentContext();
    var isServer = deploymentContext.ReleaseId == -1;
    var isLocalDev = deploymentContext.EnvironmentId == 1;
    var btnApprove = document.getElementById(idButtonApprove);
    if (btnApprove) {
        var currentHashCode = '';
        $.post(settingsHashUrl, deploymentContext)
            .done(function (result) {
                currentHashCode = isServer || isLocalDev ? '' : result.hashCode;
                var approvalHashCode = '';
                $.post(settingsApprovalHashUrl, deploymentContext)
                    .done(function (result) {
                        approvalHashCode = isServer || isLocalDev ? '' : result.hashCode;
                        setApprovalDisplayOptions(currentHashCode, approvalHashCode);
                    });
            });
    }
}

function deploymentContextChangeHandler() {
    calculateApprovalDisplayOptions();
    reloadGrid();
}

function addApproval() {
    var deploymentContext = getDeploymentContext();
    var deploymentContextApproval = {};
    deploymentContextApproval.ClientId = deploymentContext.ClientId;
    deploymentContextApproval.EnvironmentId = deploymentContext.EnvironmentId;
    deploymentContextApproval.ReleaseId = deploymentContext.ReleaseId;
    deploymentContextApproval.UserName = ''; // Assigned in controller
    var hiddenHashCode = $("#" + idHiddenHashCode);
    deploymentContextApproval.HashCode = hiddenHashCode.val();
    $.post(settingsAddApprovalUrl, deploymentContextApproval)
        .done(function () {
            $('#successHeading').text('Settings Approval');
            $('#successMessage').text('Settings approval has been saved.');
            $('#successModal').modal('show');
        })
        .fail(function () {
            $('#errorHeading').text('ERROR: Settings Approval');
            $('#errorMessage').text('Settings approval failed to save to the database.');
            $('#errorModal').modal('show');
        })
        .always(function () {
            calculateApprovalDisplayOptions();
        });
}

function buttonApprovalClickHandler() {
    addApproval();
}

$(document).ready(function () {

    document.getElementById(idClient).addEventListener("change", deploymentContextChangeHandler);
    document.getElementById(idEnvironment).addEventListener("change", deploymentContextChangeHandler);
    document.getElementById(idRelease).addEventListener("change", deploymentContextChangeHandler);

    var btnApprove = document.getElementById(idButtonApprove);
    if (btnApprove) {
        btnApprove.addEventListener("click", buttonApprovalClickHandler);
    }

    var releaseSel = document.getElementById(idRelease);
    releaseSel.selectedIndex = releaseSel.options.length - 1;
    calculateApprovalDisplayOptions();

    var grid = $("#" + idSettingsGrid).bootgrid({
        ajax: true,
        ajaxSettings: {
            method: "POST",
            cache: true
        },
        requestHandler: function (request) {
            var deploymentContext = getDeploymentContext();
            request.client = deploymentContext.ClientId;
            request.environment = deploymentContext.EnvironmentId;
            request.release = deploymentContext.ReleaseId;
            // Scan the original sort object from Bootgrid...
            // and populate an array of "SortData"...
            request.sortItems = [];
            if (request.sort === null)
                return request;
            for (var property in request.sort) {
                if (request.sort.hasOwnProperty(property)) {
                    request.sortItems.push({ Field: property, Type: request.sort[property] });
                }
            }
            return request;
        },
        url: findApiUrl,
        multiSort: true,
        rowCount: [25, 50, 100, -1],
        formatters: {
            "tooltipvalue": function (column, row) {
                return '<div data-toggle="tooltip" data-placement="top" title="' + escapeHtml(row.value) + '">' + escapeHtml(row.value) + '</div>';
            },
            "tooltipname": function (column, row) {
                return '<div data-toggle="tooltip" data-placement="top" title="' + escapeHtml(row.name) + '">' + escapeHtml(row.name) + '</div>';
            },
            "selectvalue": function (column, row) {
                return '<input class="form-control" value="' + escapeHtml(row.value) + '"/>';
            },
        }
    });
    grid.on("loaded.rs.jquery.bootgrid", function (e) {
        $('[data-toggle="tooltip"]').tooltip();
    });

});


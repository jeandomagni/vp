﻿var idClient1 = "dropdownClient";
var idClient2 = "dropdownClient2";
var idEnvironment1 = "dropdownEnvironment";
var idEnvironment2 = "dropdownEnvironment2";
var idRelease1 = "dropdownRelease";
var idRelease2 = "dropdownRelease2";
var idButtonCompare = "buttonCompare";
var idCheckboxInline = "checkboxInline";
var idCheckboxOnlyChanges = "checkboxOnlyChanges";
var idDiffOutput = "diffoutput";

var clientApiUrl = "../api/clients";
var environmentApiUrl = "../api/environments";
var releaseApiUrl = "../api/releases";

var clientTag = '{client}';
var environmentTag = '{environment}';
var releaseTag = '{release}';

var urlParams = new URLSearchParams(window.location.search);

var settingsApiUrl = '../api/settings/diffline/' + clientTag + '/' + environmentTag + '/' + releaseTag;

// Added secondary diff library JsDiff that is currently not being used.
// It has char, word and line capabilities.

////Not being used
//function diff2(baseValue, baseName, newValue, newName, diffOutputElement, isInline) {
//    var diff = JsDiff["diffLines"](baseValue, newValue);
//    var fragment = document.createDocumentFragment();
//    for (var i = 0; i < diff.length; i++) {

//        if (diff[i].added && diff[i + 1] && diff[i + 1].removed) {
//            var swap = diff[i];
//            diff[i] = diff[i + 1];
//            diff[i + 1] = swap;
//        }

//        var node;
//        if (diff[i].removed) {
//            node = document.createElement('del');
//            node.appendChild(document.createTextNode(diff[i].value));
//        } else if (diff[i].added) {
//            node = document.createElement('ins');
//            node.appendChild(document.createTextNode(diff[i].value));
//        } else {
//            node = document.createTextNode(diff[i].value);
//        }
//        fragment.appendChild(node);
//    }

//    diffOutputElement.textContent = '';
//    diffOutputElement.appendChild(fragment);
//}

function ToKeyValuePair(text) {
    var settingName = text.split(/ : (.+)/)[0];
    settingName = settingName.replace(" : ", ""); // in case no value was found after the name
    settingName = settingName.replace(/'/g, "''");
    var settingValue = text.split(/ : (.+)/)[1];
    settingValue = settingValue ? settingValue : ""; // use empty string for null
    settingValue = settingValue.replace(/'/g, "''");
    return {"key" : settingName, "value" : settingValue};
}

function getSettingKeyValuePair(opcode, newList) {
    var isDelete = (opcode[0] === 'delete');
    var newIndex = opcode[3];
    var targetText = newList[newIndex];
    var kvp = ToKeyValuePair(targetText);
    if (isDelete) {
        kvp.value = "";
    }
    return kvp;
}

function getDictionary(list)
{
    var dictionary = new Array(); 
    var i;
    for (i = 0; i < list.length; i++) {
        var kvp = ToKeyValuePair(list[i]);
        dictionary[kvp.key] = kvp.value;
    }
    return dictionary;
}

function getKeys(list) {
    var keys = [];
    var i;
    for (i = 0; i < list.length; i++) {
        var kvp = ToKeyValuePair(list[i]);
        keys.push(kvp.key);
    }
    return keys;
}

function getSqlStatement(settingKeyValuePair) {
    var insertSql = "\r\nINSERT INTO @SettingOverrides SELECT '{SettingName}', '{SettingValue}'";
    var sqlStatement = insertSql.replace("{SettingName}", settingKeyValuePair.key);
    sqlStatement = sqlStatement.replace("{SettingValue}", settingKeyValuePair.value);
    return sqlStatement;
}

// need to have this "hidden gem" in the query string"
// ?sql=true&user=IVINCIHEALTH\rschuler
function generateSqlScript(baseList, newList, opcodes) {
    var targetContext = getDeployContextName(idClient2, idEnvironment2, idRelease2);
    var sqlScript = "declare @ClientId int = (select ClientId from Client where name = \'{ClientName}\')\r\ndeclare @ReleaseId int = (select ReleaseId from Release where Label = \'{ReleaseLabel}\')\r\ndeclare @EnvironmentId int = (select EnvironmentId from Environment where Description = \'{EnvironmentName}\')\r\ndeclare @SettingsManagerUserId int = (select SettingsManagerUserId from SettingsManagerUser where UserName = \'{UserName}\')\r\ndeclare @InsertDate datetime2(7) = SYSUTCDATETIME();\r\n\r\nDECLARE @SettingOverrides TABLE (Name NVARCHAR(MAX), Value NVARCHAR(MAX))\r\n";
    sqlScript = sqlScript.replace("{ClientName}", targetContext.clientName);
    sqlScript = sqlScript.replace("{EnvironmentName}", targetContext.environmentName);
    sqlScript = sqlScript.replace("{ReleaseLabel}", targetContext.releaseLabel);
    var userName = urlParams.get('user');
    sqlScript = sqlScript.replace("{UserName}", userName);

    var baseDictionary = getDictionary(baseList);
    var targetDictionary = getDictionary(newList);

    var baseKeys = getKeys(baseList);
    var targetKeys = getKeys(newList);

    baseKeys.forEach(function(baseKey) {
        var doesExist = targetDictionary[baseKey];
        if (doesExist == null) {
            var sqlStatement = "\r\n-- VERIFY WHETHER THIS SETTING IS MISSING, NEEDS TO BE DEPRECATED, OR NEEDS BLANK VALUE" + getSqlStatement({"key" : baseKey, "value" : ""});
            sqlScript += sqlStatement;
        }
    });

    targetKeys.forEach(function(targetKey) {
        var hasDifferentValue = (targetDictionary[targetKey] !== baseDictionary[targetKey]);
        if (hasDifferentValue) {
            var sqlStatement = getSqlStatement({"key" : targetKey, "value" : targetDictionary[targetKey]});
            sqlScript += sqlStatement;
        }
    });

    var insertSqlStatement = "\r\n\r\nINSERT INTO [dbo].[SettingClientRelease]\r\n           ([SettingId]\r\n           ,[ClientId]\r\n           ,[StartReleaseId]\r\n           ,[Value]\r\n           ,[IsEncrypted]\r\n           ,[InsertDate]\r\n           ,[SettingsManagerUserId])\r\nSELECT\r\n\t\t\t[SettingId] = (select SettingId from Setting where Name = so.Name)\r\n           ,[ClientId] = @ClientId\r\n           ,[StartReleaseId] = @ReleaseId\r\n           ,[Value] = so.Value\r\n           ,[IsEncrypted] = 0\r\n           ,[InsertDate] = @InsertDate\r\n           ,[SettingsManagerUserId] = @SettingsManagerUserId\r\nFROM @SettingOverrides so\r\n";
    sqlScript += insertSqlStatement;

    return sqlScript;
}

function diff(baseValue, baseName, newValue, newName, diffOutputElement, isInline, onlyChanges) {

    var baseList = difflib.stringAsLines(baseValue);
    var newList = difflib.stringAsLines(newValue);

    var baseFiltered = baseList.filter(function (val) {
        return newList.indexOf(val) === -1;
    });

    var newFiltered = newList.filter(function (val) {
        return baseList.indexOf(val) === -1;
    });

    if (onlyChanges)
    {
        baseList = baseFiltered;
        newList = newFiltered;
    }

    // create a SequenceMatcher instance that diffs the two sets of lines
    var sm = new difflib.SequenceMatcher(baseList, newList);

    // get the opcodes from the SequenceMatcher instance
    // opcodes is a list of 3-tuples describing what changes should be made to the base text
    // in order to yield the new text
    var opcodes = sm.get_opcodes();
    while (diffOutputElement.firstChild) diffOutputElement.removeChild(diffOutputElement.firstChild);
    var contextSize = null;

    var generateSql = urlParams.get('sql');
    if (generateSql) {
        var sqlScript = generateSqlScript(baseList, newList, opcodes);
        $("#sqlOutput").show();
        $("#sqlOutput").val(sqlScript);
    }
    
    // build the diff view and add it to the current DOM
    diffOutputElement.appendChild(diffview.buildView({
        baseTextLines: baseList,
        newTextLines: newList,
        opcodes: opcodes,
        // set the display titles for each resource
        baseTextName: baseName,
        newTextName: newName,
        contextSize: contextSize,
        viewType: isInline
    }));
}

function getDeployContext(idClient, idEnvironment, idRelease) {
    return {
        client: document.getElementById(idClient).value,
        environment: document.getElementById(idEnvironment).value,
        release: document.getElementById(idRelease).value
    }
}

function getDeployContextName(idClient, idEnvironment, idRelease) {
    var clientSel = document.getElementById(idClient);
    var environmentSel = document.getElementById(idEnvironment);
    var releaseSel = document.getElementById(idRelease);    
    return {
        clientName: clientSel.options[clientSel.selectedIndex].text,
        environmentName: environmentSel.options[environmentSel.selectedIndex].text,
        releaseLabel: releaseSel.options[releaseSel.selectedIndex].text
    }
}

function getDeployContextNameLabel(idClient, idEnvironment, idRelease) {
    var context = getDeployContextName(idClient, idEnvironment, idRelease);
    var label = context.clientName + ' ' + context.environmentName + ' ' + context.releaseLabel;
    return label;
}

function getSettingsApiUrl(deployContext) {
    var url = settingsApiUrl.replace(clientTag, deployContext.client).replace(environmentTag, deployContext.environment).replace(releaseTag, deployContext.release);
    return url;
}

function compareDeployContexts(isInline, onlyChanges) {
    var baseDeployContext = getDeployContext(idClient1, idEnvironment1, idRelease1);
    var baseSettingsApiUrl = getSettingsApiUrl(baseDeployContext);
    var newDeployContext = getDeployContext(idClient2, idEnvironment2, idRelease2);
    var newSettingsApiUrl = getSettingsApiUrl(newDeployContext);

    var baseDeployContextName = getDeployContextNameLabel(idClient1, idEnvironment1, idRelease1);
    var newDeployContextName = getDeployContextNameLabel(idClient2, idEnvironment2, idRelease2);

    $.when(
        $.get(baseSettingsApiUrl), //base values
        $.get(newSettingsApiUrl)  //new values
    )
        .done(function (firstCall, secondCall) {
            var baseText = '';
            Object.keys(firstCall[0]).forEach(function (k) {
                var objVal = firstCall[0][k];
                var objValSingleLine = objVal.replace(/[\r\n]+/g, ''); 
                baseText += (objValSingleLine + "\n");
            });
            var newText = '';
            Object.keys(secondCall[0]).forEach(function (k) {
                var objVal = secondCall[0][k];
                var objValSingleLine = objVal.replace(/[\r\n]+/g, '');
                newText += (objValSingleLine + "\n");
            });
            var diffOutputElement = document.getElementById(idDiffOutput);
            diff(baseText, baseDeployContextName, newText, newDeployContextName, diffOutputElement, isInline, onlyChanges);
        })
        .fail(function () {
            //handle errors
        });
}

function isOnlyChanges() {
    var checkboxOnlyChanges = document.getElementById(idCheckboxOnlyChanges);
    var onlyChanges = checkboxOnlyChanges.checked ? 1 : 0;
    return onlyChanges;
}

function isInline() {
    var checkboxInline = document.getElementById(idCheckboxInline);
    var isInlineChecked = checkboxInline.checked ? 1 : 0;
    return isInlineChecked;
}

function addCompareClickEventHandler(element) {
    element.addEventListener('click', function () {
        compareDeployContexts(isInline(), isOnlyChanges());
    });
}

function attachClickEventHandlers() {

    var buttonCompare = document.getElementById(idButtonCompare);
    addCompareClickEventHandler(buttonCompare);

    var checkboxInline = document.getElementById(idCheckboxInline);
    addCompareClickEventHandler(checkboxInline);

    var checkboxOnlyChanges = document.getElementById(idCheckboxOnlyChanges);
    addCompareClickEventHandler(checkboxOnlyChanges);
}

function setDefaultDropdownSelections() {

    //dropdownRelease dropdownRelease2 - set latest release as the default selected value
    var releaseSel = document.getElementById(idRelease1);
    releaseSel.selectedIndex = releaseSel.options.length - 1;
    var releaseSel2 = document.getElementById(idRelease2);
    releaseSel2.selectedIndex = releaseSel2.options.length - 1;
}

$(document).ready(function () {
    attachClickEventHandlers();
    setDefaultDropdownSelections();   
});

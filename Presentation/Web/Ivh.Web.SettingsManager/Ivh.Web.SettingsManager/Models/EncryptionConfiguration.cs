﻿namespace Ivh.Web.SettingsManager.Models
{
    public class EncryptionConfiguration
    {
        public string AuthKey { get; set; }
        public string CryptKey { get; set; }
        public int SaltLength { get; set; }
    }
}

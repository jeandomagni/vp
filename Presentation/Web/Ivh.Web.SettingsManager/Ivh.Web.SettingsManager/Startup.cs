﻿namespace Ivh.Web.SettingsManager
{
    using Application.SettingsManager.Common.MappingModule;
    using Application.SettingsManager.Modules;
    using Authorization;
    using Common.Data.Core;
    using Common.Data.Core.Interfaces;
    using Constants;
    using Domain.SettingsManager.Modules;
    using Helpers;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Http;
    using Microsoft.Extensions.DependencyInjection.Extensions;
    using Provider.SettingsManager.Modules;
    using Microsoft.AspNetCore.Builder;
    using Microsoft.AspNetCore.Hosting;
    using Microsoft.AspNetCore.Server.IISIntegration;
    using Microsoft.Extensions.Caching.Distributed;
    using Microsoft.Extensions.Configuration;
    using Microsoft.Extensions.DependencyInjection;
    using NHibernate;

    public class Startup
    {
        public Startup(
            IConfiguration configuration,
            IHostingEnvironment environment)
        {
            this.Configuration = configuration;
            this.Environment = environment;
        }

        public IConfiguration Configuration { get; set; }
        public IHostingEnvironment Environment { get; set; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection serviceCollection)
        {           
            serviceCollection.AddSingleton(this.Configuration);

            this.RegisterContextAccessor(serviceCollection);
            this.RegisterDatabase(serviceCollection);
            this.RegisterServices(serviceCollection);
            this.RegisterAuthorizationPolicies(serviceCollection);

            serviceCollection.AddAuthentication(IISDefaults.AuthenticationScheme);

            serviceCollection.AddCors(options =>
            {
                options.AddPolicy("AllowAll",
                    builder =>
                    {
                        builder
                        .AllowAnyOrigin()
                        .AllowAnyMethod()
                        .AllowAnyHeader()
                        .AllowCredentials();
                    });
            });

            serviceCollection.AddMvc();
            serviceCollection.AddDistributedMemoryCache(); // Adds a default in-memory implementation of IDistributedCache
        }

        /// <summary>
        /// Add authorization policies that require roles
        /// Role names can be dynamic from the app.config
        /// Policy constants are used to allow use of controller Authorize attribute
        /// </summary>
        /// <param name="serviceCollection"></param>
        private void RegisterAuthorizationPolicies(IServiceCollection serviceCollection)
        {
            bool isDevelopment = this.Environment.IsDevelopment();
            if (isDevelopment)
            {
                //When developing locally, change these flags to emulate Role Authorization behavior
                bool canViewSettings = true;
                bool canAddSettings = true;
                bool canOverrideSettings = true;
                bool canApproveSettings = true;
                bool canDeploySettings = true;
                bool canDeployLocalSettings = true;

                serviceCollection.AddAuthorization(options =>
                {
                    options.AddPolicy(Policy.SettingsManagerApplication, policy => policy.Requirements.Add(new AllowAccessRequirement(
                        canViewSettings || canAddSettings || canOverrideSettings || canApproveSettings || canDeploySettings || canDeployLocalSettings
                        )));
                    options.AddPolicy(Policy.ViewSettings, policy => policy.Requirements.Add(new AllowAccessRequirement(
                        canViewSettings || canAddSettings || canOverrideSettings || canApproveSettings || canDeployLocalSettings
                        )));
                    options.AddPolicy(Policy.AddSettings, policy => policy.Requirements.Add(new AllowAccessRequirement(canAddSettings)));
                    options.AddPolicy(Policy.OverrideSettings, policy => policy.Requirements.Add(new AllowAccessRequirement(canOverrideSettings)));
                    options.AddPolicy(Policy.ApproveSettings, policy => policy.Requirements.Add(new AllowAccessRequirement(canApproveSettings)));
                    options.AddPolicy(Policy.DeploySettings, policy => policy.Requirements.Add(new AllowAccessRequirement(canDeploySettings)));
                    options.AddPolicy(Policy.DeployLocalSettings, policy => policy.Requirements.Add(new AllowAccessRequirement(canDeployLocalSettings)));
                });
                serviceCollection.AddSingleton<IAuthorizationHandler, AllowAccessHandler>();
            }
            else
            {
                serviceCollection.AddAuthorization(options =>
                {
                    options.AddPolicy(Policy.SettingsManagerApplication, policy => policy.Requirements.Add(new IsInGroupRequirement(
                        ConfigHelper.ViewSettingsRole(this.Configuration),
                        ConfigHelper.AddSettingsRole(this.Configuration),
                        ConfigHelper.OverrideSettingsRole(this.Configuration),
                        ConfigHelper.ApproveSettingsRole(this.Configuration),
                        ConfigHelper.DeployLocalSettingsRole(this.Configuration),
                        ConfigHelper.DeploySettingsRole(this.Configuration)
                        )));
                    options.AddPolicy(Policy.ViewSettings, policy => policy.Requirements.Add(new IsInGroupRequirement(
                        ConfigHelper.ViewSettingsRole(this.Configuration),
                        ConfigHelper.AddSettingsRole(this.Configuration),
                        ConfigHelper.OverrideSettingsRole(this.Configuration),
                        ConfigHelper.ApproveSettingsRole(this.Configuration),
                        ConfigHelper.DeployLocalSettingsRole(this.Configuration)
                    )));
                    options.AddPolicy(Policy.AddSettings, policy => policy.Requirements.Add(new IsInGroupRequirement(ConfigHelper.AddSettingsRole(this.Configuration))));
                    options.AddPolicy(Policy.OverrideSettings, policy => policy.Requirements.Add(new IsInGroupRequirement(ConfigHelper.OverrideSettingsRole(this.Configuration))));
                    options.AddPolicy(Policy.ApproveSettings, policy => policy.Requirements.Add(new IsInGroupRequirement(ConfigHelper.ApproveSettingsRole(this.Configuration))));
                    options.AddPolicy(Policy.DeploySettings, policy => policy.Requirements.Add(new IsInGroupRequirement(ConfigHelper.DeploySettingsRole(this.Configuration))));
                    options.AddPolicy(Policy.DeployLocalSettings, policy => policy.Requirements.Add(new IsInGroupRequirement(ConfigHelper.DeployLocalSettingsRole(this.Configuration))));
                    
                });
                serviceCollection.AddSingleton<IAuthorizationHandler, IsInGroupHandler>();
            }
        }

        private void RegisterDatabase(IServiceCollection serviceCollection)
        {
            serviceCollection.TryAddSingleton<IFluentMappingModule>(new SettingsManagerFluentMappingModule());
            serviceCollection.TryAddSingleton<ISessionFactory>(ctx =>
            {
                string connectionString = this.Configuration.GetConnectionString("DefaultConnection");
                return DataModule.CreateSessionfactory(ctx.GetRequiredService<IFluentMappingModule>(), connectionString, "SettingsManager");
            });
            serviceCollection.TryAddTransient<NHibernate.ISession>(DataModule.OpenSession);
        }

        private void RegisterServices(IServiceCollection serviceCollection)
        {
            serviceCollection.RegisterSettingsManagerRepositories();
            serviceCollection.RegisterSettingsManagerDomainServices();
            serviceCollection.RegisterSettingsManagerApplicationServices();
        }

        private void RegisterContextAccessor(IServiceCollection serviceCollection)
        {
            serviceCollection.TryAddSingleton<IHttpContextAccessor, HttpContextAccessor>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
            public void Configure(
            IApplicationBuilder app, 
            IHostingEnvironment env,
            IDistributedCache cache
            )
        {
            if (env.IsDevelopment())
            {
                app.UseBrowserLink();
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
            }

            app.UseStaticFiles();

            if (env.IsDevelopment())
            {
                app.UseCors("AllowAll");
            }

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");
            });

        }
    }
}

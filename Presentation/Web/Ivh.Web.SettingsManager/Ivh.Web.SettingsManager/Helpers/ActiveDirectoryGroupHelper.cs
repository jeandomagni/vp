﻿using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Security.Principal;

namespace Ivh.Web.SettingsManager.Helpers
{
    public static class ActiveDirectoryGroupHelper
    {
        public static IEnumerable<string> FindGroupsBySid(ClaimsPrincipal claimsPrincipal)
        {
            foreach (Claim claim in claimsPrincipal.Claims.Where(c => c.Type == ClaimTypes.GroupSid))
            {
                string groupName = null;
                try
                {
                    SecurityIdentifier sid = new SecurityIdentifier(claim.Value);
                    IdentityReference ntAccount = sid.Translate(typeof(NTAccount));
                    groupName = ntAccount.Value;
                }
                catch (IdentityNotMappedException)
                {
                }

                if (groupName != null)
                {
                    yield return groupName;
                }
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Ivh.Web.SettingsManager.Helpers
{
    using Microsoft.Extensions.Configuration;
    using SettingsManager.Constants;

    public static class ConfigHelper
    {
        private static IConfigurationSection GetAppConfigSection(IConfiguration config)
        {
            return config.GetSection(ConfigSection.AppConfiguration);
        }
        
        public static string ViewSettingsRole(IConfiguration config)
        {
            return GetAppConfigSection(config)[ConfigKey.ViewSettings];
        }

        public static string AddSettingsRole(IConfiguration config)
        {
            return GetAppConfigSection(config)[ConfigKey.AddSettings];
        }

        public static string OverrideSettingsRole(IConfiguration config)
        {
            return GetAppConfigSection(config)[ConfigKey.OverrideSettings];
        }

        public static string ApproveSettingsRole(IConfiguration config)
        {
            return GetAppConfigSection(config)[ConfigKey.ApproveSettings];
        }

        public static string DeploySettingsRole(IConfiguration config)
        {
            return GetAppConfigSection(config)[ConfigKey.DeploySettings];
        }

        public static string DeployLocalSettingsRole(IConfiguration config)
        {
            return GetAppConfigSection(config)[ConfigKey.DeployLocalSettings];
        }

        public static string SettingsManagerApplicationRole(IConfiguration config)
        {
            return GetAppConfigSection(config)[ConfigKey.SettingsManagerApplication];
        }
    }
}

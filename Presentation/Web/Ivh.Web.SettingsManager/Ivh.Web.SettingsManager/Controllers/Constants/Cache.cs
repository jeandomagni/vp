﻿namespace Ivh.Web.SettingsManager.Controllers.Constants
{
    public static class Cache
    {
        public const string AuthKey = "_AuthKey";
        public const string CryptKey = "_CryptKey";
        public const string SaltLength = "_SaltLength";
        public const int SecondsToExpire = 300; //5 minutes
    }
}

﻿namespace Ivh.Web.SettingsManager.Controllers
{
    using System.Diagnostics;
    using System.Linq;
    using Application.SettingsManager.Common.Interfaces;
    using Domain.SettingsManager.Constants;
    using Models;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.Extensions.Caching.Distributed;
    using SettingsManager.Constants;

    //[Authorize(Policy = Policy.SettingsManagerApplication)]
    public class HomeController : Controller
    {
        private readonly ISettingsManagerApplicationService _settingsManagerApplicationService;
        private readonly IDistributedCache _cache;

        public HomeController(
            ISettingsManagerApplicationService settingsManagerApplicationService,
            IDistributedCache cache)
        {
            this._settingsManagerApplicationService = settingsManagerApplicationService;
            this._cache = cache;
        }

        public IActionResult Index()
        {
            return this.View();
        }

        public IActionResult Error()
        {
            return this.View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? this.HttpContext.TraceIdentifier });
        }

        public IActionResult RoleViewer()
        {
            return this.View();
        }

        [Authorize(Policy.ViewSettings)]
        public IActionResult Find()
        {
            this.ViewData["Message"] = "Find the settings and values for a specific deployment context";
            this.ViewBag.Releases = this._settingsManagerApplicationService.GetReleases(includeServerOption: true);
            this.ViewBag.Environments = this._settingsManagerApplicationService.GetEnvironments();
            this.ViewBag.Clients = this._settingsManagerApplicationService.GetClients();
            return this.View();
        }

        [Authorize(Policy = Policy.ViewSettings)]
        public IActionResult Compare()
        {
            this.ViewData["Message"] = "Compare settings between two deployment contexts";
            this.ViewBag.Releases = this._settingsManagerApplicationService.GetReleases(includeServerOption: true);
            this.ViewBag.Environments = this._settingsManagerApplicationService.GetEnvironments();
            this.ViewBag.Clients = this._settingsManagerApplicationService.GetClients();
            return this.View();
        }

        [Authorize(Policy.ViewSettings)]
        public IActionResult EncryptionUtility()
        {
            EncryptionConfiguration config = new EncryptionConfiguration()
            {
                AuthKey = this._cache.GetString(this.User.Identity.Name + Constants.Cache.AuthKey),
                CryptKey = this._cache.GetString(this.User.Identity.Name + Constants.Cache.CryptKey),
                SaltLength = int.Parse(this._cache.GetString(this.User.Identity.Name + Constants.Cache.SaltLength) ?? "0")
            };
            this.ViewData["Message"] = "Encryption Utility";
            return this.View(config);
        }

        [Authorize(Policy = Policy.DeployLocalSettings)]
        public IActionResult DevScript()
        {
            this.ViewData["Message"] = "Create Developer SQL Merge Script";
            this.ViewBag.Releases = this._settingsManagerApplicationService.GetReleases(includeServerOption: false);
            this.ViewBag.Environments = this._settingsManagerApplicationService.GetEnvironments().Where(x => x.Key == SettingsSource.LocalDev);
            this.ViewBag.Clients = this._settingsManagerApplicationService.GetClients();
            return this.View();
        }

    }
}

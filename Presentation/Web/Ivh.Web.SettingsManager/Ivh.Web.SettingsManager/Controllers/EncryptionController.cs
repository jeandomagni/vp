﻿namespace Ivh.Web.SettingsManager.Controllers
{
    using Common.Encryption.Core;
    using System;
    using Microsoft.AspNetCore.Mvc;
    using System.Security.Cryptography;
    using Microsoft.AspNetCore.Authorization;
    using Constants;
    using Microsoft.Extensions.Caching.Distributed;
    using SettingsManager.Constants;

    [Authorize(Policy.ViewSettings)]
    public class EncryptionController : Controller
    {
        private readonly IDistributedCache _cache;

        public EncryptionController(IDistributedCache cache)
        {
            this._cache = cache;
        }

        [Route("api/settings/encryption/generatekey")]
        [HttpGet]
        public IActionResult GenerateKey()
        {
            return this.Json(AESThenHMAC.NewKeyString());
        }

        [Route("api/settings/encryption/encryptvalue")]
        [HttpPost]
        public IActionResult EncryptValue(string secretMessage, string cryptKey, string authKey, int randomLength)
        {
            byte[] randomBytes = new byte[randomLength];
            new RNGCryptoServiceProvider().GetBytes(randomBytes);
            return this.Json(AESThenHMAC.SimpleEncrypt(secretMessage, cryptKey, authKey, randomBytes));
        }

        [Route("api/settings/encryption/decryptsettings")]
        [HttpPost]
        public IActionResult DecryptSettings(string cryptKey, string authKey, int randomLength)
        {
            DistributedCacheEntryOptions cacheEntryOptions = new DistributedCacheEntryOptions().SetSlidingExpiration(TimeSpan.FromSeconds(Cache.SecondsToExpire));
            this._cache.SetString(this.User.Identity.Name + Cache.AuthKey, authKey ?? "", cacheEntryOptions);
            this._cache.SetString(this.User.Identity.Name + Cache.CryptKey, cryptKey ?? "", cacheEntryOptions);
            this._cache.SetString(this.User.Identity.Name + Cache.SaltLength, randomLength.ToString(), cacheEntryOptions);
            return this.Ok();
        }

        [Route("api/settings/encryption/cleardecryptsettings")]
        [HttpGet]
        public IActionResult ClearDecryptSettings()
        {
            //no clear() or foreach(key) in IDistributedCache
            this._cache.Remove(this.User.Identity.Name + Cache.AuthKey);
            this._cache.Remove(this.User.Identity.Name + Cache.CryptKey);
            this._cache.Remove(this.User.Identity.Name + Cache.SaltLength);
            return this.Ok();
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using Ivh.Application.SettingsManager.Common.Interfaces;
using Ivh.Application.SettingsManager.Common.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using Ivh.Common.Encryption.Core;
using Ivh.Domain.SettingsManager.Common.Models;
using Ivh.Domain.SettingsManager.Constants;
using Microsoft.Extensions.Caching.Distributed;
using Ivh.Web.SettingsManager.Controllers.Constants;

namespace Ivh.Web.SettingsManager.Controllers
{
    using SettingsManager.Constants;

    [Produces("application/json")]
    [Authorize]
    public class SettingsController : Controller
    {

        private readonly ISettingsManagerApplicationService _settingsManagerApplicationService;
        private readonly IDistributedCache _cache;

        public SettingsController(
            ISettingsManagerApplicationService settingsManagerApplicationService,
            IDistributedCache cache
        )
        {
            this._settingsManagerApplicationService = settingsManagerApplicationService;
            this._cache = cache;
        }

        #region Add Settings

        [Authorize(Policy = Policy.AddSettings)]
        [Route("api/settings/add")]
        [HttpPost, ValidateAntiForgeryToken]
        public IActionResult AddSetting(SettingInput model)
        {
            if (this.ModelState.IsValid)
            {
                model.Username = this.User?.Identity?.Name;
                if (model.Username != null)
                {
                    try
                    {
                        this._settingsManagerApplicationService.AddSetting(model);
                        return this.RedirectToAction("ViewAddSettingSuccess", "Settings", new {successMessage = $"Setting \"{model.Name}\" has been saved."});
                    }
                    catch (Exception e)
                    {
                        return this.RedirectToAction("ViewAddSettingError", "Settings", new {errorMessage = e.Message});
                    }

                }
                else
                {
                    throw new Exception("Name of user is unknown");
                }
            }

            this.LoadAddSettingsViewBag();
            return this.View(model);
        }

        private void LoadAddSettingsViewBag()
        {
            this.ViewData["Message"] = "Define a new setting and add it to the database";
            this.ViewBag.DataTypes = this._settingsManagerApplicationService.GetSettingDataTypes();
            this.ViewBag.SettingTypes = this._settingsManagerApplicationService.GetSettingTypes();
            KeyValuePair<int, string> latestRelease = this._settingsManagerApplicationService.GetReleases().Last();
            this.ViewBag.LatestRelease = latestRelease.Value.Split('.');
        }

        [Authorize(Policy = Policy.AddSettings)]
        public IActionResult ViewAddSetting()
        {
            this.LoadAddSettingsViewBag();
            return this.View("AddSetting");
        }

        [Authorize(Policy = Policy.AddSettings)]
        public IActionResult ViewAddSettingSuccess(string successMessage)
        {
            this.ViewBag.SuccessMessage = successMessage;
            return this.ViewAddSetting();
        }

        [Authorize(Policy = Policy.AddSettings)]
        public IActionResult ViewAddSettingError(string errorMessage)
        {
            this.ViewBag.ErrorMessage = errorMessage;
            return this.ViewAddSetting();
        }

        #endregion

        #region Add Settings Override

        [Authorize(Policy = Policy.OverrideSettings)]
        [Route("api/settings/add/override")]
        [HttpPost, ValidateAntiForgeryToken]
        public IActionResult AddSettingOverride(SettingOverrideInput model)
        {
            if (this.ModelState.IsValid)
            {
                model.Username = this.User?.Identity?.Name;
                if (model.Username != null)
                {
                    try
                    {
                        this._settingsManagerApplicationService.AddSettingOverride(model);
                        return this.RedirectToAction("ViewAddSettingOverrideSuccess", "Settings", new {successMessage = $"Setting override has been saved."});
                    }
                    catch (Exception e)
                    {
                        return this.RedirectToAction("ViewAddSettingOverrideError", "Settings", new {errorMessage = e.Message});
                    }
                }
                else
                {
                    throw new Exception("Name of user is unknown");
                }
            }

            this.LoadAddSettingOverrideViewBag();
            return this.View(model);
        }

        private void LoadAddSettingOverrideViewBag()
        {
            this.ViewData["Message"] = "Override a setting and add it to the database";
            this.ViewBag.Environments = this._settingsManagerApplicationService.GetEnvironments(includeAllOption: true);
            this.ViewBag.Clients = this._settingsManagerApplicationService.GetClients(includeAllOption: true);
            this.ViewBag.SettingNames = this._settingsManagerApplicationService.GetSettingNames();
            KeyValuePair<int, string> latestRelease = this._settingsManagerApplicationService.GetReleases().Last();
            this.ViewBag.LatestRelease = latestRelease.Value.Split('.');
        }

        [Authorize(Policy = Policy.OverrideSettings)]
        public IActionResult ViewAddSettingOverride()
        {
            this.LoadAddSettingOverrideViewBag();
            return this.View("AddSettingOverride");
        }

        [Authorize(Policy = Policy.OverrideSettings)]
        public IActionResult ViewAddSettingOverrideSuccess(string successMessage)
        {
            this.ViewBag.SuccessMessage = successMessage;
            return this.ViewAddSettingOverride();
        }

        [Authorize(Policy = Policy.OverrideSettings)]
        public IActionResult ViewAddSettingOverrideError(string errorMessage)
        {
            this.ViewBag.ErrorMessage = errorMessage;
            return this.ViewAddSettingOverride();
        }

        #endregion

        #region Compare

        [Authorize(Policy = Policy.ViewSettings)]
        [Route("api/settings/min/{clientId}/{environmentId}/{releaseId}")]
        [HttpGet]
        public IActionResult GetSettingsMin(int clientId, int environmentId, int releaseId)
        {
            List<SettingMinimum> result = this._settingsManagerApplicationService.GetSettingsMin(clientId, environmentId, releaseId);
            return this.Json(result);
        }

        [Authorize(Policy = Policy.ViewSettings)]
        [Route("api/settings/diffline/{clientId}/{environmentId}/{releaseId}")]
        [HttpGet]
        public IActionResult GetSettingsDiffLine(int clientId, int environmentId, int releaseId)
        {
            List<string> result = null;
            if (releaseId == SettingsSource.Server)
            {
                result = this._settingsManagerApplicationService.GetSettingsDiffLineFromServer(clientId, environmentId); //Client Data API on server
            }
            else
            {
                result = this._settingsManagerApplicationService.GetSettingsDiffLine(clientId, environmentId, releaseId); //Settings DB
            }

            return this.Json(result);
        }

        #endregion

        #region View

        [Authorize(Policy = Policy.ViewSettings)]
        [Route("api/settings/max/{clientId}/{environmentId}/{releaseId}")]
        [HttpGet]
        public IActionResult GetSettingsMax(int clientId, int environmentId, int releaseId)
        {
            List<SettingComposite> result = this._settingsManagerApplicationService.GetSettingsMax(clientId, environmentId, releaseId);
            return this.Json(result);
        }

        [Authorize(Policy = Policy.ViewSettings)]
        [Route("api/settings/max/bootgrid")]
        [HttpPost]
        public IActionResult GetSettingsMaxBootgrid(BootGridRequest model)
        {
            BootGridResponse<SettingComposite> bootGridResponse = this._settingsManagerApplicationService.GetSettingsMaxBootgrid(model);
            this.DecryptSettingValues(bootGridResponse);
            return this.Json(bootGridResponse);
        }

        #endregion

        #region Decryption

        private void DecryptSettingValues(BootGridResponse<SettingComposite> bootGridResponse)
        {
            string authKey = this._cache.GetString(this.User.Identity.Name + Cache.AuthKey);
            string cryptKey = this._cache.GetString(this.User.Identity.Name + Cache.CryptKey);
            int saltLength = int.Parse(this._cache.GetString(this.User.Identity.Name + Cache.SaltLength) ?? "0");

            foreach (SettingComposite setting in bootGridResponse.rows)
            {
                if (setting.IsEncrypted)
                {
                    try
                    {
                        setting.Value = AESThenHMAC.SimpleDecrypt(setting.Value, cryptKey, authKey, saltLength);
                    }
                    catch (Exception)
                    {
                        //Swallow the error since they might have provided incorrect keys
                    }
                }
            }
        }

        #endregion

        #region Deployment

        [Authorize(Policy = Policy.DeployLocalSettings)]
        [Route("api/settings/sqlmerge/{clientId}/{environmentId}/{releaseId}")]
        [HttpGet]
        public IActionResult GetSettingsMergeScriptForDeveloper(int clientId, int environmentId, int releaseId)
        {
            string mergeScript = this._settingsManagerApplicationService.GetClientSettingsMergeScriptForDeveloper(clientId, environmentId, releaseId);
            return this.Json(mergeScript);
        }

        [Authorize(Policy = Policy.DeploySettings)]
        [Route("api/settings/sqlmergedeploy/{client}/{environment}/{release}")]
        [HttpGet]
        public IActionResult GetSettingsMergeScript(string client, string environment, string release)
        {
            DeploymentContextApprovalStatus status = this._settingsManagerApplicationService.GetDeploymentContextApprovalStatus(client, environment, release);
            string mergeScript = $"RAISERROR('{status.ErrorMessage}',16,1)";
            if (status.IsSuccess)
            {
                mergeScript = this._settingsManagerApplicationService.GetClientSettingsMergeScript(client, environment, release);
            }
            return this.Json(mergeScript);
        }

        [Authorize(Policy = Policy.DeploySettings)]
        [Route("api/settings/sqlmergedeploy/latest/{client}/{environment}")]
        [HttpGet]
        public IActionResult GetLatestSettingsMergeScript(string client, string environment)
        {
            string latestReleaseLabel = this._settingsManagerApplicationService.GetLatestReleaseLabel();
            string mergeScript = this._settingsManagerApplicationService.GetClientSettingsMergeScript(client, environment, latestReleaseLabel);
            return this.Json(mergeScript);
        }

        [Authorize(Policy = Policy.DeploySettings)]
        [Route("api/settings/deploy/latest/release")]
        [HttpGet]
        public IActionResult GetLatestRelease()
        {
            string latestReleaseLabel = this._settingsManagerApplicationService.GetLatestReleaseLabel();
            return this.Json(latestReleaseLabel);
        }

        [Authorize(Policy = Policy.DeploySettings)]
        [Route("api/settings/deploy/approval/status/{client}/{environment}/{release}")]
        [HttpGet]
        public IActionResult GetApprovalStatus(string client, string environment, string release)
        {
            DeploymentContextApprovalStatus status = this._settingsManagerApplicationService.GetDeploymentContextApprovalStatus(client, environment, release);
            return this.Json(status);
        }

        #endregion

        #region Approval

        [Authorize(Policy = Policy.ApproveSettings)]
        [Route("api/settings/hash")]
        [HttpPost]
        public IActionResult GetDeploymentContextHashCode(DeploymentContext model)
        {
            DeploymentContext deploymentContext = model;
            deploymentContext.HashCode = this._settingsManagerApplicationService.GetSettingsHashCode(
                clientId: model.ClientId,
                environmentId: model.EnvironmentId,
                releaseId: model.ReleaseId
            );
            return new JsonResult(deploymentContext);
        }

        [Authorize(Policy = Policy.ApproveSettings)]
        [Route("api/settings/hash/approval")]
        [HttpPost]
        public IActionResult GetLatestDeploymentContextApprovedHashCode(DeploymentContext model)
        {
            DeploymentContext deploymentContext = model;
            deploymentContext.HashCode = this._settingsManagerApplicationService.GetLatestApprovedSettingsHashCode(
                clientId: model.ClientId,
                environmentId: model.EnvironmentId,
                releaseId: model.ReleaseId
            );
            return new JsonResult(deploymentContext);
        }

        [Authorize(Policy = Policy.ApproveSettings)]
        [Route("api/settings/approval/add")]
        [HttpPost]
        public IActionResult AddDeploymentContextApproval(DeploymentContextApproval model)
        {
            model.UserName = this.User?.Identity?.Name;
            if (model.UserName != null)
            {
                this._settingsManagerApplicationService.AddClientReleaseEnvironmentApproval(model);
            }
            else
            {
                throw new Exception("Name of user is unknown");
            }

            return this.Ok(); 
           
        }

        #endregion

    }
}
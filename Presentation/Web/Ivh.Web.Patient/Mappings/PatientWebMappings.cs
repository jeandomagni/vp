﻿namespace Ivh.Web.Patient.Mappings
{
    using System.Linq;
    using System.Web;
    using Application.Core.Common.Dtos;
    using Application.Email.Common.Dtos;
    using Application.FinanceManagement.Common.Dtos;
    using Application.Guarantor.Common.Dtos;
    using Application.SecureCommunication.Common.Dtos;
    using Application.User.Common.Dtos;
    using Areas.Mobile.Models;
    using Common.Base.Utilities.Extensions;
    using Common.Base.Utilities.Helpers;
    using Common.EventJournal;
    using Common.VisitPay.Enums;
    using Common.VisitPay.Strings;
    using Common.Web.Extensions;
    using Common.Web.Mapping;
    using Common.Web.Models;
    using Common.Web.Models.Alerts;
    using Common.Web.Models.Consolidate;
    using Common.Web.Models.Guarantor;
    using Common.Web.Models.Payment;
    using Common.Web.Models.Sso;
    using Common.Web.Models.Support;
    using Ivh.Application.Core.Common.Dtos.Eob;
    using Models;
    using Models.Account;
    using Models.KnowledgeBase;
    using Models.Notifications;
    using Models.Sso;
    using Models.Survey;

    public class PatientWebMappings : CommonWebMappings
    {
        public PatientWebMappings()
        {
            this.CreateMap<VisitDto, VisitDetailsViewModel>()
                .IncludeBase<VisitDto, Common.Web.Models.Account.VisitDetailsViewModel>();

            this.CreateMap<VisitEobIndustryCodeIdentificationDto, VisitEobDetailViewModel.RemittanceSummaryRemarkViewModel>();

            this.CreateMap<VisitTransactionSummaryDto, VisitDetailsViewModel>()
                .IncludeBase<VisitTransactionSummaryDto, Common.Web.Models.Account.VisitDetailsViewModel>();

            this.CreateMap<SupportRequestDto, SupportRequestResultViewModel>()
                .ForMember(dest => dest.HasAttachments, opts => opts.MapFrom(x => x.GuarantorMessages.SelectMany(z => z.ActiveSupportRequestMessageAttachments).Any()))
                .ForMember(dest => dest.HasMessages, opts => opts.MapFrom(x => x.GuarantorMessages.Count > 1))
                .ForMember(dest => dest.UnreadMessages, opts => opts.MapFrom(x => x.GuarantorMessages.Where(z => z.SupportRequestMessageType == SupportRequestMessageTypeEnum.ToGuarantor).OrderBy(z => z.InsertDate).Count(z => !z.IsRead)))
                .ForMember(dest => dest.InsertDate, opts => opts.MapFrom(x => MappingHelper.MapToClientDateText(x.InsertDate)))
                .ForMember(dest => dest.MessageBody, opts => opts.MapFrom(x => x.SupportRequestMessages.OrderBy(z => z.InsertDate).Select(z => z.MessageBody).FirstOrDefault()))
                .ForMember(dest => dest.SupportRequestStatus, opts => opts.MapFrom(x => x.SupportRequestStatusSource.SupportRequestStatusName))
                .ForMember(dest => dest.SupportTopic, opts => opts.MapFrom(x => x.SupportTopicDisplayString))
                .ForMember(dest => dest.SubmittedFor, opts => opts.MapFrom(x => MappingHelper.MapSupportRequestSubmittedFor(x, false)));

            this.CreateMap<SupportRequestMessageDto, SupportRequestMessageListingViewModel>()
                .ForMember(dest => dest.InsertDate, opts => opts.MapFrom(x => MappingHelper.MapToFormattedClientDateTimeText(x.InsertDate, Format.DateFormatShortYear)))
                .ForMember(dest => dest.SupportRequestDisplayId, opts => opts.MapFrom(x => x.SupportRequest.SupportRequestDisplayId))
                .ForMember(dest => dest.SupportRequestId, opts => opts.MapFrom(x => x.SupportRequest.SupportRequestId))
                .ForMember(dest => dest.MessageBody, opts => opts.MapFrom(x => $"{new string(x.MessageBody.Take(255).ToArray())}"));

            this.CreateMap<AlertDto, AlertViewModel>();

            this.CreateMap<FinancePlanDto, ConsolidationGuarantorFinancePlanViewModel>()
                .ForMember(dest => dest.FinancePlanOfferId, opts => opts.MapFrom(src => src.FinancePlanOffer.FinancePlanOfferId))
                .ForMember(dest => dest.CurrentFinancePlanBalance, opts => opts.MapFrom(src => src.CurrentFinancePlanBalance.FormatCurrency()))
                .ForMember(dest => dest.CurrentInterestRate, opts => opts.MapFrom(src => src.CurrentInterestRate.FormatPercentage()))
                .ForMember(dest => dest.FinalPaymentDate, opts => opts.MapFrom(src => MappingHelper.MapToFormattedClientDateTimeText(src.LastPaymentDate, Format.MonthYearFormat)))
                .ForMember(dest => dest.MonthlyPaymentAmount, opts => opts.MapFrom(src => src.PaymentAmount.FormatCurrency()))
                .ForMember(dest => dest.OriginatedFinancePlanBalance, opts => opts.MapFrom(src => src.OriginatedFinancePlanBalance.FormatCurrency()))
                .ForMember(dest => dest.CreditAgreementCmsVersionId, opts => opts.MapFrom(src => src.TermsCmsVersionId));

            this.CreateMap<SystemMessageDto, SystemMessageViewModel>();

            this.CreateMap<ArrangePaymentViewModel, ArrangePaymentMobileViewModel>();

            this.CreateMap<CommunicationGroupDto, CommunicationGroupSelectionViewModel>();

            this.CreateMap<SurveyDto, SurveyResultViewModel>();
            this.CreateMap<SurveyRatingGroupDto, SurveyRatingGroupViewModel>();
            this.CreateMap<SurveyRatingDto, SurveyRatingViewModel>();
            this.CreateMap<SurveyResultAnswerDto, SurveyResultAnswerViewModel>()
                .ForMember(dest => dest.SurveyRatingGroupViewModel, opts => opts.MapFrom(src => src.SurveyRatingGroup));

            this.CreateMap<SurveyInfoViewModel, SurveyResultViewModel>();
            this.CreateMap<SurveyResultViewModel, SurveyResultDto>()
                .ForMember(dest => dest.SurveyResultAnswers, opts => opts.MapFrom(src => src.SurveyQuestions)); //TODO: really need to change the wording so it is less confusing
            this.CreateMap<SurveyResultAnswerViewModel, SurveyResultAnswerDto>();

            this.CreateMap<SamlResponseDto, SamlResponseViewModel>();

            this.CreateMap<HealthEquityBalanceDto, HealthEquityBalanceWidgetViewModel>()
                .ForMember(dest => dest.CashBalance, mo => mo.MapFrom(src => $"{src.CashBalance:C}"))
                .ForMember(dest => dest.InvestmentBalance, mo => mo.MapFrom(src => $"{src.InvestmentBalance:C}"))
                .ForMember(dest => dest.ContributionsYtd, mo => mo.MapFrom(src => $"{src.ContributionsYtd:C}"))
                .ForMember(dest => dest.DistributionsYtd, mo => mo.MapFrom(src => $"{src.DistributionsYtd:C}"))
                .ForMember(dest => dest.LastUpdated, mo => mo.MapFrom(src => MappingHelper.MapToFormattedClientDateTimeText(src.LastUpdated, "MM/dd/yyyy hh/mm/ss tt")));

            this.CreateMap<CommunicationSmsBlockedNumberDto, CommunicationSmsBlockedNumberViewModel>();

            this.CreateMap<KnowledgeBaseCategoryDto, KnowledgeBaseCategoryViewModel>();
            this.CreateMap<KnowledgeBaseSubCategoryDto, KnowledgeBaseSubCategoryViewModel>();
            this.CreateMap<KnowledgeBaseQuestionAnswerDto, KnowledgeBaseQuestionAnswerViewModel>();
            this.CreateMap<KnowledgeBaseCategoryQuestionsDto, KnowledgeBaseCategoryQuestionsViewModel>();
            this.CreateMap<KnowledgeBaseCategoryQuestionAnswersDto, KnowledgeBaseCategoryQuestionAnswersViewModel>();
            this.CreateMap<KnowledgeBaseQuestionDto, KnowledgeBaseQuestionViewModel>();

            this.CreateMap<KnowledgeBaseCategoryDto, Common.Web.Models.Faq.KnowledgeBaseCategoryViewModel>();
            this.CreateMap<KnowledgeBaseSubCategoryDto, Common.Web.Models.Faq.KnowledgeBaseSubCategoryViewModel>();
            this.CreateMap<KnowledgeBaseQuestionAnswerDto, Common.Web.Models.Faq.KnowledgeBaseQuestionAnswerViewModel>();

            this.CreateMap<PaymentSummaryGroupedTransactionDto, PaymentSummaryGroupedTransactionPdfViewModel>()
                .ForMember(dest => dest.PatientName, mo => mo.MapFrom(src => src.Visit.PatientName))
                .ForMember(dest => dest.PostDate, mo => mo.MapFrom(src => src.PostDate.ToString(Format.DateFormat)))
                .ForMember(dest => dest.TransactionAmount, mo => mo.MapFrom(src => src.TransactionAmount.FormatCurrency()))
                .ForMember(dest => dest.VisitDate, mo => mo.MapFrom(src => src.Visit.VisitDate.HasValue ? src.Visit.VisitDate.Value.ToString(Format.DateFormat) : ""))
                .ForMember(dest => dest.VisitDescription, mo => mo.MapFrom(src => src.Visit.VisitDescription))
                .ForMember(dest => dest.VisitSourceSystemKeyDisplay, mo => mo.MapFrom(src => $"#{src.Visit.VisitSourceSystemKeyDisplay}"));

            this.CreateMap<HttpContext, HttpContextDto>()
                .ForMember(dest => dest.Url, opts => opts.MapFrom(src => src.Request.Url.ToString()))
                .ForMember(dest => dest.Request, opts => opts.MapFrom(src => src.Request))
                .ForMember(dest => dest.Response, opts => opts.MapFrom(src => src.Response))
                .ForMember(dest => dest.User, opts => opts.MapFrom(src => src.User))
                .ForMember(dest => dest.Session, opts => opts.MapFrom(src => src.Session))
                .ForMember(dest => dest.UserHostAddress, opts => opts.MapFrom(src => src.Request.RequestUserHostAddressString()));

            this.CreateMap<HttpContext, JournalEventHttpContextDto>()
                .ForMember(dest => dest.Url, opts => opts.MapFrom(src => src.Request.Url.AbsolutePath))
                .ForMember(dest => dest.UserHostAddress, opts => opts.MapFrom(src => src.Request.UserHostAddress))
                .ForMember(dest => dest.UserAgent, opts => opts.MapFrom(src => src.Request.UserAgent))
                .ForMember(dest => dest.DeviceType, opts => opts.MapFrom(src => DeviceTypeHelper.GetDeviceType(src.Request.UserAgent)));

            //TODO: remove this temp map once UserEvents are entirely replaced
            this.CreateMap<HttpContextDto, JournalEventHttpContextDto>()
                .ForMember(dest => dest.Url, opts => opts.MapFrom(src => src.Request.Url.AbsolutePath))
                .ForMember(dest => dest.UserHostAddress, opts => opts.MapFrom(src => src.Request.UserHostAddress))
                .ForMember(dest => dest.UserAgent, opts => opts.MapFrom(src => src.Request.UserAgent))
                .ForMember(dest => dest.DeviceType, opts => opts.MapFrom(src => DeviceTypeHelper.GetDeviceType(src.Request.UserAgent)));

            

            this.CreateMap<RequestUsernameViewModel, VisitPayUserFindByDetailsParametersDto>()
                // explicit mappings intended for traceability
                .ForMember(dest => dest.FirstName, opts => opts.MapFrom(src => src.FirstName))
                .ForMember(dest => dest.LastName, opts => opts.MapFrom(src => src.LastName))
                .ForMember(dest => dest.Username, opts => opts.Ignore())
                .ForMember(dest => dest.UseFirstName, opts => opts.MapFrom(src => src.RequireFirstName || src.FirstName.Length > 0))
                .ForMember(dest => dest.UseSsn, opts => opts.MapFrom(src => src.RequireSsn || src.Ssn4.Length > 0))
                .ForMember(dest => dest.UseEmailAddress, opts => opts.MapFrom(src => src.RequireEmailAddress || src.EmailAddress.Length > 0))
                .ForMember(dest => dest.DateOfBirth, opts => opts.MapFrom(src => DateTimeHelper.FromValues(src.DateOfBirthMonth, src.DateOfBirthDay, src.DateOfBirthYear)))
                .ForMember(dest => dest.Ssn4, opts => opts.MapFrom(src => src.RequireSsn ? int.Parse(src.Ssn4) : (int?)null))
                .ForMember(dest => dest.EmailAddress, opts => opts.MapFrom(src => src.EmailAddress));


            this.CreateMap<RequestPasswordViewModel, VisitPayUserFindByDetailsParametersDto>()
                // explicit mappings intended for traceability
                .ForMember(dest => dest.FirstName, opts => opts.MapFrom(src => src.FirstName))
                .ForMember(dest => dest.LastName, opts => opts.MapFrom(src => src.LastName))
                .ForMember(dest => dest.Username, opts => opts.MapFrom(src => src.Username))
                .ForMember(dest => dest.UseFirstName, opts => opts.MapFrom(src => src.RequireFirstName || src.FirstName.Length > 0))
                .ForMember(dest => dest.UseSsn, opts => opts.MapFrom(src => src.RequireSsn || src.Ssn4.Length > 0))
                .ForMember(dest => dest.DateOfBirth, opts => opts.MapFrom(src => DateTimeHelper.FromValues(src.DateOfBirthMonth, src.DateOfBirthDay, src.DateOfBirthYear)))
                .ForMember(dest => dest.Ssn4, opts => opts.MapFrom(src => src.RequireSsn ? int.Parse(src.Ssn4) : (int?) null));
        }
    }
}
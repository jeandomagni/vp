﻿namespace Ivh.Web.Patient.Services
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Security.Claims;
    using System.Security.Principal;
    using System.Web;
    using Application.Core.Common.Dtos;
    using Application.Core.Common.Interfaces;
    using Application.FinanceManagement.Common.Interfaces;
    using AutoMapper;
    using Common.Base.Enums;
    using Common.EventJournal;
    using Common.VisitPay.Enums;
    using Common.Web.Extensions;
    using SessionFacade;

    public class GuarantorFilterService
    {
        private readonly Lazy<IGuarantorApplicationService> _guarantorApplicationService;
        private readonly Lazy<IWebPatientSessionFacade> _sessionFacade;
        private readonly Lazy<IConsolidationGuarantorApplicationService> _consolidationGuarantorApplicationService;
        private readonly Lazy<IVisitPayUserApplicationService> _visitPayUserApplicationService;
        private readonly Lazy<IVisitPayUserJournalEventApplicationService> _visitPayUserJournalEventApplicationService;
        
        private int? _visitPayUserId;
        private int? _vpGuarantorId;

        public GuarantorFilterService(
            Lazy<IWebPatientSessionFacade> sessionFacade,
            Lazy<IConsolidationGuarantorApplicationService> consolidationGuarantorApplicationService,
            Lazy<IGuarantorApplicationService> guarantorApplicationService,
            Lazy<IVisitPayUserApplicationService> visitPayUserApplicationService,
            Lazy<IVisitPayUserJournalEventApplicationService> visitPayUserJournalEventApplicationService)
        {
            this._sessionFacade = sessionFacade;
            this._consolidationGuarantorApplicationService = consolidationGuarantorApplicationService;
            this._guarantorApplicationService = guarantorApplicationService;
            this._visitPayUserApplicationService = visitPayUserApplicationService;
            this._visitPayUserJournalEventApplicationService = visitPayUserJournalEventApplicationService;
        }

        private IIdentity Identity => this.User.Identity;

        private IPrincipal User => HttpContext.Current.User;

        public int VpGuarantorId
        {
            get
            {
                if (this._vpGuarantorId.HasValue)
                {
                    return this._vpGuarantorId.Value;
                }

                this._visitPayUserId = null;

                if (this._sessionFacade.Value.FilteredVpGuarantorId.HasValue)
                {
                    int clarified = this._sessionFacade.Value.GuarantorObfuscator.Clarify(this._sessionFacade.Value.FilteredVpGuarantorId.Value);
                    if (clarified > 0)
                    {
                        this._vpGuarantorId = clarified;
                        return clarified;
                    }
                }

                this._vpGuarantorId = this.User.CurrentGuarantorId() ?? this._visitPayUserApplicationService.Value.GetGuarantorIdFromVisitPayUserId(this.Identity.CurrentUserId());
                return this._vpGuarantorId.Value;
            }
        }

        public int VisitPayUserId
        {
            get
            {
                int vpGuarantorId = this.VpGuarantorId;

                if (!this._visitPayUserId.HasValue)
                {
                    this._visitPayUserId = this._visitPayUserApplicationService.Value.GetVisitPayUserId(vpGuarantorId);
                }
                
                return this._visitPayUserId.Value;
            }
        }

        public void SetGuarantor(Guid obfuscatedVpGuarantorId)
        {
            this._sessionFacade.Value.FilteredVpGuarantorId = obfuscatedVpGuarantorId;
            this._vpGuarantorId = null;
            this._visitPayUserId = null;

            this.LogViewManaged(obfuscatedVpGuarantorId);
        }
        
        public Guid SetGuarantor(int vpGuarantorId)
        {
            Guid obfuscatedVpGuarantorId = this._sessionFacade.Value.GuarantorObfuscator.Obfuscate(vpGuarantorId);
            this._sessionFacade.Value.FilteredVpGuarantorId = obfuscatedVpGuarantorId;
            this._vpGuarantorId = null;
            this._visitPayUserId = null;

            this.LogViewManaged(obfuscatedVpGuarantorId);

            return obfuscatedVpGuarantorId;
        }

        public IDictionary<Guid, GuarantorDto> GetObfuscatedGuarantors()
        {
            GuarantorDto currentGuarantor = this._guarantorApplicationService.Value.GetGuarantor(this.Identity.CurrentUserName());
            IDictionary<Guid, GuarantorDto> obfuscatedGuarantors = this.GetObfuscatedGuarantorsDictionary(currentGuarantor);
            return obfuscatedGuarantors ?? new Dictionary<Guid, GuarantorDto>();
        }

        public void ObfuscateGuarantors(IList<GuarantorDto> guarantors = null)
        {
            if (!this.Identity.IsAuthenticated)
            {
                return;
            }

            GuarantorDto currentGuarantor = this._guarantorApplicationService.Value.GetGuarantor(this.Identity.CurrentUserName());
            guarantors = guarantors ?? this.GetManagedGuarantors(currentGuarantor);
            
            //Always at least use the current guarantor
            if (currentGuarantor != null)
            {
                guarantors.Insert(0, currentGuarantor);
            }

            foreach (int vpGuarantorId in guarantors.Select(x => x.VpGuarantorId))
            {
                this._sessionFacade.Value.GuarantorObfuscator.Obfuscate(vpGuarantorId);
            }
        }

        private IList<GuarantorDto> GetManagedGuarantors(GuarantorDto currentGuarantor)
        {
            if (currentGuarantor == null || currentGuarantor.ConsolidationStatus != GuarantorConsolidationStatusEnum.Managing || !currentGuarantor.ManagedGuarantorIds.Any())
            {
                return new List<GuarantorDto>();
            }

            return this._consolidationGuarantorApplicationService.Value.GetAllManagedGuarantors(currentGuarantor.User.VisitPayUserId)
                .Where(x => x.ConsolidationGuarantorStatus == ConsolidationGuarantorStatusEnum.Accepted)
                .Select(x => x.ManagedGuarantor)
                .OrderBy(x => x.User.DisplayFirstNameLastName)
                .ToList();
        }

        private IDictionary<Guid, GuarantorDto> GetObfuscatedGuarantorsDictionary(GuarantorDto currentGuarantor)
        {
            IDictionary<Guid, GuarantorDto> dict = new Dictionary<Guid, GuarantorDto>();
            
            IList<GuarantorDto> guarantors = this.GetManagedGuarantors(currentGuarantor);

            if (!guarantors.Any()
                && currentGuarantor.ConsolidationStatus == GuarantorConsolidationStatusEnum.Managed
                && currentGuarantor.ManagingGuarantorId.HasValue)
            {
                //User is managed by another guarantor, return no guarantors
                this._sessionFacade.Value.FilteredVpGuarantorId = null;
                this._vpGuarantorId = null;
                this._visitPayUserId = null;
                return null;
            }

            //Always at least return the current guarantor, if not managed
            guarantors.Insert(0, currentGuarantor);

            if (guarantors.All(x => x.VpGuarantorId != this.VpGuarantorId))
            {
                this._sessionFacade.Value.FilteredVpGuarantorId = null;
                this._vpGuarantorId = null;
                this._visitPayUserId = null;
            }

            guarantors.ToList().ForEach(guarantor =>
            {
                Guid value;
                if (this._sessionFacade.Value.GuarantorObfuscator.Map.Forward.TryGetValue(guarantor.VpGuarantorId, out value))
                {
                    dict.Add(new KeyValuePair<Guid, GuarantorDto>(value, guarantor));
                }
            });

            return dict;
        }

        private void LogViewManaged(Guid obfuscatedVpGuarantorId)
        {
            int vpGuarantorId = this._sessionFacade.Value.GuarantorObfuscator.Clarify(obfuscatedVpGuarantorId);
            
            int? emulationClientUserId = this.EmulationClientUserId();
            int currentVpGuarantorId = this.User.CurrentGuarantorId() ?? this._visitPayUserApplicationService.Value.GetGuarantorIdFromVisitPayUserId(this.Identity.CurrentUserId());
            if (currentVpGuarantorId != vpGuarantorId)
            {
                this._visitPayUserJournalEventApplicationService.Value.LogViewManaged(this.Identity.CurrentUserId(), vpGuarantorId, emulationClientUserId, Mapper.Map<JournalEventHttpContextDto>(HttpContext.Current));
            }
        }

        private int? EmulationClientUserId()
        {
            if (!(this.Identity is ClaimsIdentity))
            {
                return null;
            }

            ClaimsIdentity claimsIdentity = (ClaimsIdentity)this.Identity;
            if (!claimsIdentity.HasClaim(ClaimTypeEnum.EmulatedUser))
            {
                return null;
            }


            string clientUserId = null;
            return !claimsIdentity.TryGetClaimValue(ClaimTypeEnum.ClientUserId, out clientUserId) ? (int?) null : Convert.ToInt32(clientUserId);
        }
    }
}
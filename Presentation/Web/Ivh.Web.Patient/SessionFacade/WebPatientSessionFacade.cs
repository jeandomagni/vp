﻿namespace Ivh.Web.Patient.SessionFacade
{
    using System;
    using System.Collections.Generic;
    using Common.Session;

    public class WebPatientSessionFacade : SessionFacadeBase, IWebPatientSessionFacade
    {
        private const string UserIsMobileDeviceSessionKey = "UserIsMobileDevice";
        private const string FilteredVpGuarantorIdSessionKey = "FilteredVpGuarantorId";
        private const string DismissedAlertSessionKey = "DismissedAlerts";
        private const string StackedAndCombinedFinancePlanOptionsPresentedKey = "StackedAndCombinedFinancePlanOptionsPresented";
        private const string SurveyHasBeenPresentedInSessionKey = "SurveyHasBeenPresentedInSession";
        private const string HomepageNavigationCountKey = "HomepageNavigationCount";
        private const string ReferringUrlKey = "ReferringUrl";
        private const string FirstTimeLoginKey = "FirstTimeLogin";

        public bool? UserIsMobileDevice
        {
            get => this.GetSessionValue<bool?>(UserIsMobileDeviceSessionKey);
            set => this.SetSessionValue(UserIsMobileDeviceSessionKey, value);
        }

        public Guid? FilteredVpGuarantorId
        {
            get
            {
                object obj = this.GetSessionValue<object>(FilteredVpGuarantorIdSessionKey);

                return (Guid?) obj;
            }
            set => this.SetSessionValue(FilteredVpGuarantorIdSessionKey, value);
        }

        public IList<int> DismissedAlerts
        {
            get => this.GetSessionValue<IList<int>>(DismissedAlertSessionKey) ??  new List<int>();
            set => this.SetSessionValue(DismissedAlertSessionKey, value);
        }
        
        public bool? StackedAndCombinedFinancePlanOptionsPresented
        {
            get => this.GetSessionValue<bool?>(StackedAndCombinedFinancePlanOptionsPresentedKey) ?? true;
            set => this.SetSessionValue(StackedAndCombinedFinancePlanOptionsPresentedKey, value);
        }

        public bool SurveyHasBeenPresentedInSession
        {
            get => this.GetSessionValue<bool>(SurveyHasBeenPresentedInSessionKey);
            set => this.SetSessionValue(SurveyHasBeenPresentedInSessionKey, value);
        }

        public int HomepageNavigationCount
        {
            get => this.GetSessionValue<int>(HomepageNavigationCountKey);
            set => this.SetSessionValue(HomepageNavigationCountKey, value);
        }

        public string ReferringUrl
        {
            get => this.GetSessionValue<string>(ReferringUrlKey) ?? string.Empty;
            set => this.SetSessionValue(ReferringUrlKey, value ?? string.Empty);
        }
    }
}

﻿namespace Ivh.Web.Patient.SessionFacade
{
    using System;
    using System.Collections.Generic;
    using Common.Session;

    public interface IWebPatientSessionFacade : ISessionFacade
    {
        bool? UserIsMobileDevice { get; set; }
        Guid? FilteredVpGuarantorId { get; set; }
        IList<int> DismissedAlerts { get; set; }
        bool? StackedAndCombinedFinancePlanOptionsPresented { get; set; }
        bool SurveyHasBeenPresentedInSession { get; set; }
        int HomepageNavigationCount { get; set; }
        string ReferringUrl { get; set; }
    }
}
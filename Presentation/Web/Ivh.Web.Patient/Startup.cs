﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Ivh.Web.Patient.Startup))]
namespace Ivh.Web.Patient
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            this.ConfigureIoc(app);
            this.ConfigureAuth(app);
            this.ConfigureMisc(app);
        }
    }
}

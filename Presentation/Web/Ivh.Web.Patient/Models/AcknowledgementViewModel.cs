﻿namespace Ivh.Web.Patient.Models
{
    using Common.Web.Models.Shared;

    public class AcknowledgementViewModel
    {
        public int TermsOfUseCmsVersionId { get; set; }
        public TermsOfUseViewModel TermsOfUseViewModel { get; set; }
		public string ReturnUrl { get; set; }
    }
}
﻿namespace Ivh.Web.Patient.Models
{
    using System.ComponentModel.DataAnnotations;
    using Common.Base.Constants;
    using Common.VisitPay.Constants;
    using Common.Web.Attributes;

    public class LoginViewModel
    {
        [Required(ErrorMessage = "Username is required")]
        [LocalizedDisplayName(TextRegionConstants.Username)]
        public string Username { get; set; }

        [Required(ErrorMessage = "Password is required")]
        [DataType(DataType.Password)]
        [LocalizedDisplayName(TextRegionConstants.Password)]
        public string Password { get; set; }

        public string ReturnUrl { get; set; }

        public bool IsSso { get; set; }

        //public string SsoOneTimePaymentMessage { get; set; }
    }
}
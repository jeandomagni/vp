﻿namespace Ivh.Web.Patient.Models.Sidebar
{
    public class SidebarViewModel
    {
        public decimal TotalBalance { get; set; }
        public bool ShowCreateFinancePlanAction { get; set; }
    }
}
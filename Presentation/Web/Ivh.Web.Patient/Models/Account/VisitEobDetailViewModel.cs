﻿namespace Ivh.Web.Patient.Models.Account
{
    using System.Collections.Generic;
    using System.Linq;
    using Application.Core.Common.Dtos;
    using AutoMapper;
    using Common.Base.Utilities.Extensions;
    using Ivh.Application.Core.Common.Dtos.Eob;

    // todo: this is a very complicated viewmodel, consider doing some or all of this mapping/logic in service layers
    public class VisitEobDetailViewModel
    {
        // WARNING: COUPLING
        // Coupled to text values used in the ETL database
        // SQL used: "select distinct UIDisplay from [DEV_CDI_ETL].[eob].[EobClaimAdjustmentReasonCodes]"
        public const string Coinsurance = "Coinsurance";
        public const string Copayment = "Copayment";
        public const string Deductible = "Deductible";
        public const string NotCovered = "Not covered";
        public const string PlanAdjustment = "Plan Adjustment";
        public const string OtherInsurance = "Amount Paid by Other Insurance";
        
        public string PayerName { get; set; }
        public string PayerLinkUrl { get; set; }
        public string PayerIcon { get; set; }
        public string PlanNumber { get; set; }
        public List<RemittanceViewModel> Remittances { get; set; }

        #region "Internal Structures"

        public class RemittanceViewModel
        {
            public string ClaimPaymentDate { get; set; }
            public string ClaimNumber { get; set; }
            public ClaimStatusEnum Status { get; set; }
            public RemittanceSummaryViewModel RemittanceSummary { get; set; }
        }

        public class RemittanceSummaryViewModel
        {
            public string TotalClaimChargeAmount { get; set; }
            public string ClaimPaymentAmount { get; set; }
            public string AdjustmentAmount { get; set; }
            public string OtherInsurancePaidAmount { get; set; }
            public string DeductibleAmount { get; set; }
            public string CoinsuranceAmount { get; set; }
            public string CopaymentAmount { get; set; }
            public string NotCoveredAmount { get; set; }
            public string TotalCoveredAmount { get; set; }
            public string TotalResponsibility { get; set; }

            public List<RemittanceSummaryRemarkViewModel> RemittanceSummaryRemarks { get; set; }
            public bool HasTruncatedText() => this.RemittanceSummaryRemarks != null && (this.RemittanceSummaryRemarks.Count > 1 || this.RemittanceSummaryRemarks.First().HasTruncatedText());
        }

        public class RemittanceSummaryRemarkViewModel
        {
            private const int TruncationLength = 100;

            public string IndustryCode { get; set; }
            public string Description { get; set; }
            public string FullDisplayText => this.IndustryCode + ": " + this.Description;
            public string ShortDisplayText
            {
                get
                {
                    string text = this.FullDisplayText;
                    if (this.FullDisplayText.Length <= TruncationLength)
                    {
                        return text;
                    }

                    //find next available space after the maximum string length so we don't cut off a word
                    int spaceIndex = this.FullDisplayText.IndexOf(" ", TruncationLength);
                    if (spaceIndex != -1)
                    {
                        text = this.FullDisplayText.Substring(0, spaceIndex);
                    }
                    return text;
                }
            }
            public bool HasTruncatedText()
            {
                return this.FullDisplayText.Length != this.ShortDisplayText.Length;
            }
        }

        private class ClaimStatusCodeMap
        {
            public string[] Codes { get; set; }
            public ClaimStatusEnum ClaimStatus { get; set; }
        }

        public enum ClaimStatusEnum
        {
            Paid = 0,
            Reversed = 1,
            Denied = 2
        }

        #endregion

        #region "Private Static Methods"

        private static VisitEobDetailViewModel GetVisitEobDetailViewModel(IList<VisitEobDetailsResultDto> planDtoList)
        {
            //get first matching item for header information
            VisitEobDetailsResultDto planDtoFirst = planDtoList.First();

            VisitEobDetailViewModel model = new VisitEobDetailViewModel
            {
                //Plan information
                PayerName = planDtoFirst.PayerName,
                PayerLinkUrl = planDtoFirst.PayerLinkUrl,
                PayerIcon = planDtoFirst.PayerIcon,
                PlanNumber = planDtoFirst.PlanNumber,

                //Payment information for claims
                Remittances = new List<RemittanceViewModel>()
            };

            AddRemittances(ref model, planDtoList);

            return model;
        }

        private static void AddRemittances(ref VisitEobDetailViewModel model, IEnumerable<VisitEobDetailsResultDto> planDtoList)
        {
            foreach (VisitEobDetailsResultDto r in planDtoList)
            {
                RemittanceViewModel remit = new RemittanceViewModel
                {
                    ClaimNumber = r.ClaimNumber,
                    ClaimPaymentDate = r.TransactionDate.ToShortDateString()
                };

                ClaimStatusCodeMap statusMap = GetClaimStatusCodeMap(r.ClaimStatusCode);
                if (statusMap != null)
                {
                    remit.Status = statusMap.ClaimStatus;
                }

                decimal totalClaimChargeAmountSum = r.TotalClaimChargeAmount;
                decimal totalClaimPaymentAmountSum = r.ClaimPaymentAmount * -1m;

                //get amounts filtered by UIDisplay and handle nulls if no items are found
                //using the string function "ToUpper()" to protect against casing issues in the database values (see comment in the class RemittanceSummary)

                //Claim Level Payment Information
                decimal adjustmentAmountSum = GetClaimAmount(r.ClaimLevelClaimAdjustments, PlanAdjustment) * -1m;
                decimal otherInsurancePaidAmount = GetClaimAmount(r.ClaimLevelClaimAdjustments, OtherInsurance) * -1m;
                decimal deductibleAmount = GetClaimAmount(r.ClaimLevelClaimAdjustments, Deductible);
                decimal coinsuranceAmount = GetClaimAmount(r.ClaimLevelClaimAdjustments, Coinsurance);
                decimal copaymentAmount = GetClaimAmount(r.ClaimLevelClaimAdjustments, Copayment);
                decimal notCoveredAmount = GetClaimAmount(r.ClaimLevelClaimAdjustments, NotCovered);

                //Service Level Payment Information
                adjustmentAmountSum += GetClaimAmount(r.ServiceLevelClaimAdjustmentsSummed, PlanAdjustment) * -1m;
                otherInsurancePaidAmount += GetClaimAmount(r.ServiceLevelClaimAdjustmentsSummed, OtherInsurance) * -1m;
                deductibleAmount += GetClaimAmount(r.ServiceLevelClaimAdjustmentsSummed, Deductible);
                coinsuranceAmount += GetClaimAmount(r.ServiceLevelClaimAdjustmentsSummed, Coinsurance);
                copaymentAmount += GetClaimAmount(r.ServiceLevelClaimAdjustmentsSummed, Copayment);
                notCoveredAmount += GetClaimAmount(r.ServiceLevelClaimAdjustmentsSummed, NotCovered);

                RemittanceSummaryViewModel remitSum = new RemittanceSummaryViewModel
                {
                    //format decimal values to currency for display
                    TotalClaimChargeAmount = totalClaimChargeAmountSum.FormatCurrency(),
                    ClaimPaymentAmount = totalClaimPaymentAmountSum.FormatCurrency(),
                    AdjustmentAmount = adjustmentAmountSum.FormatCurrency(),
                    OtherInsurancePaidAmount = otherInsurancePaidAmount.FormatCurrency(),
                    DeductibleAmount = deductibleAmount.FormatCurrency(),
                    CoinsuranceAmount = coinsuranceAmount.FormatCurrency(),
                    CopaymentAmount = copaymentAmount.FormatCurrency(),
                    NotCoveredAmount = notCoveredAmount.FormatCurrency(),
                    TotalCoveredAmount = (adjustmentAmountSum + totalClaimPaymentAmountSum + otherInsurancePaidAmount).FormatCurrency(),
                    TotalResponsibility = r.PatientResponsibilityAmount.FormatCurrency(),
                    RemittanceSummaryRemarks = new List<RemittanceSummaryRemarkViewModel>()
                };

                remitSum.RemittanceSummaryRemarks = Mapper.Map<List<RemittanceSummaryRemarkViewModel>>(r.HealthCareRemarkCodes);

                remit.RemittanceSummary = remitSum;
                model.Remittances.Add(remit);
            }
        }

        private static readonly List<ClaimStatusCodeMap> ClaimStatusCodeMaps = new List<ClaimStatusCodeMap>
        {
            new ClaimStatusCodeMap {Codes = new[] {"1", "2", "3", "19", "20", "21"}, ClaimStatus = ClaimStatusEnum.Paid},
            new ClaimStatusCodeMap {Codes = new[] {"22"}, ClaimStatus = ClaimStatusEnum.Reversed},
            new ClaimStatusCodeMap {Codes = new[] {"4", "23"}, ClaimStatus = ClaimStatusEnum.Denied}
        };

        /// <summary>
        /// Returns the ordinal number and friendly text for a claim status code
        /// </summary>
        /// <param name="claimStatusCode"></param>
        /// <returns></returns>
        private static ClaimStatusCodeMap GetClaimStatusCodeMap(string claimStatusCode)
        {
            ClaimStatusCodeMap map = ClaimStatusCodeMaps.FirstOrDefault(m => m.Codes.Contains(claimStatusCode));
            return map;
        }

        /// <summary>
        /// Gets amount filtered by UIDisplay and handle null if no items are found
        /// </summary>
        /// <param name="claimAdjustments"></param>
        /// <param name="uiDisplay"></param>
        /// <returns></returns>
        private static decimal GetClaimAmount(IList<VisitEobClaimAdjustmentDto> claimAdjustments, string uiDisplay)
        {
            //using the string function "ToUpper()" to protect against casing issues in the database values (see comment in the class RemittanceSummary)
            return claimAdjustments.Where(c => c.UIDisplay.ToUpper() == uiDisplay.ToUpper()).Sum(c => (decimal?)c.MonetaryAmount) ?? 0.0m;
        }

        #endregion

        #region "Factory Methods"

        /// <summary>
        /// Converts a list of VisitEobDetailsDto items to a list of VisitEobDetailViewModel items
        /// </summary>
        /// <param name="dtoList"></param>
        /// <returns></returns>
        public static List<VisitEobDetailViewModel> GetVisitEobDetailViewModels(IReadOnlyList<VisitEobDetailsResultDto> dtoList)
        {
            List<VisitEobDetailViewModel> modelList = new List<VisitEobDetailViewModel>();

            if (dtoList == null || !dtoList.Any())
            {
                return modelList;
            }

            // WARNING: COUPLING
            // Ivh.Provider.Visit.ClaimPaymentInformationRepository.Get835RemitIndicators(visits)
            // The LINQ query is sorted by PayerName ascending

            //Get unique insurance plans and sort by the order that they pay claims for the patient
            var uniquePlans = dtoList.Select(v => new { v.PayerName, v.PlanNumber }).Distinct().OrderBy(v => v.PayerName);

            foreach (var plan in uniquePlans)
            {
                //get all matching items to support multiple Remittance objects
                IList<VisitEobDetailsResultDto> planDtoList = dtoList.Where(d => d.PayerName == plan.PayerName && d.PlanNumber == plan.PlanNumber).ToList();

                VisitEobDetailViewModel model = GetVisitEobDetailViewModel(planDtoList);

                modelList.Add(model);
            }

            return modelList;
        }

        #endregion

    }

}
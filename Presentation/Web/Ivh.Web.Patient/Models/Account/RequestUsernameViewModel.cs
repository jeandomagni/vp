namespace Ivh.Web.Patient.Models.Account
{
    using System.ComponentModel.DataAnnotations;
    using Common.Web.Attributes;
    using Common.Base.Constants;
    using Common.VisitPay.Constants;

    public class RequestUsernameViewModel: BaseAccountRecoveryViewModel
    {
        [LocalizedDisplayName(TextRegionConstants.NameLast)]
        [Required]
        public string LastName { get; set; }

        [LocalizedDisplayName(TextRegionConstants.Dob)]
        [LocalizedPrompt(TextRegionConstants.DobMonth)]
        [Required]
        public string DateOfBirthMonth { get; set; }

        [LocalizedDisplayName(TextRegionConstants.DobDay)]
        [LocalizedPrompt(TextRegionConstants.DobDay)]
        [Required]
        [Range(1, 31)]
        [MaxLength(2)]
        public string DateOfBirthDay { get; set; }

        [LocalizedDisplayName(TextRegionConstants.DobYear)]
        [LocalizedPrompt(TextRegionConstants.DobYear)]
        [Required]
        [MinLength(4)]
        [MaxLength(4)]
        public string DateOfBirthYear { get; set; }


        [LocalizedDisplayName(TextRegionConstants.EmailAddress)]
        [RequiredIf(nameof(RequireEmailAddress), true)]
        [RegularExpressionEmailAddress(TextRegionConstants.NotificationEmailInvalid)]
        public string EmailAddress { get; set; }

        public bool RequireEmailAddress => !this.RequireSsn;
    }
}
﻿namespace Ivh.Web.Patient.Models.Account
{
    using System.Collections.Generic;

    public class VisitEobDetailViewListModel
    {
        public List<VisitEobDetailViewModel> VisitEobDetailViewModels { get; set; } = new List<VisitEobDetailViewModel>();
    }
}
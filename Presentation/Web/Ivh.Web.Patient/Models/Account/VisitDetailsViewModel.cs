namespace Ivh.Web.Patient.Models.Account
{
    using Common.Web.Models.Account;

    public class VisitDetailsViewModel : Common.Web.Models.Account.VisitDetailsViewModel
    {
        public VisitTransactionsSearchFilterViewModel Filter { get; set; }
    }
}
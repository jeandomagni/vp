﻿namespace Ivh.Web.Patient.Models.Account
{
    using Common.Base.Enums;
    using Common.VisitPay.Enums;

    public class ActivateTextMessagingViewModel
    {
        public SmsPhoneTypeEnum SmsPhoneType { get; set; }
        public int ActivateTextCmsVersionId { get; set; }
        public bool ActivateTextAgreed { get; set; }
        public string ValidationCode { get; set; }
        public string PhoneNumber { get; set; }
        public int SmsRetryCount { get; set; }
    }
}
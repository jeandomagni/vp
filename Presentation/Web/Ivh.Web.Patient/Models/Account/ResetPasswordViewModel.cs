namespace Ivh.Web.Patient.Models.Account
{
    using System.ComponentModel.DataAnnotations;
    using Common.Base.Constants;
    using Common.VisitPay.Constants;
    using Common.Web.Attributes;

    public class ResetPasswordViewModel
    {
        [Required]
        [LocalizedDisplayName(TextRegionConstants.Username)]
        public string Username { get; set; }
        
        [Required]
        [LocalizedDisplayName(TextRegionConstants.Password)]
        public string Password { get; set; }

        [RequiredIf(nameof(RequireSsn), true)]
        [RegularExpressionSsn4]
        [LocalizedDisplayName(TextRegionConstants.Ssn4)]
        public string Ssn4 { get; set; }

        [Required]
        public string TempPassword { get; set; }

        // Set from AccountRecoveryConfiguration
        public bool RequireSsn { get; set; } = true;
    }
}
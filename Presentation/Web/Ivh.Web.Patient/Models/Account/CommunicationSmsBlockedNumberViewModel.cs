﻿namespace Ivh.Web.Patient.Models.Account
{
    public class CommunicationSmsBlockedNumberViewModel
    {
        public string BlockedPhoneNumber { get; set; }
        public string SmsPhoneNumber { get; set; }
    }
}
﻿namespace Ivh.Web.Patient.Models.Account
{
    public class PaymentSummaryGroupedTransactionPdfViewModel
    {
        public string VisitDate { get; set; }
        public string PatientName { get; set; }
        public string VisitDescription { get; set; }
        public string VisitSourceSystemKeyDisplay { get; set; }
        public string TransactionAmount { get; set; }
        public string PostDate { get; set; }
    }
}
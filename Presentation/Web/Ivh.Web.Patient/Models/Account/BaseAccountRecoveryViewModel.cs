﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Ivh.Web.Patient.Models.Account
{
    using Common.Base.Constants;
    using Common.VisitPay.Constants;
    using Common.Web.Attributes;

    public class BaseAccountRecoveryViewModel
    {
        [LocalizedDisplayName(TextRegionConstants.NameFirst)]
        [RequiredIf(nameof(RequireFirstName), true)]
        public string FirstName { get; set; }

        [LocalizedDisplayName(TextRegionConstants.Ssn4)]
        [RequiredIf(nameof(RequireSsn), true)]
        [RegularExpressionSsn4]
        public string Ssn4 { get; set; }

        public bool RequireSsn { get; set; } = true;
        public bool RequireFirstName { get; set; } = false;
        public bool SsnVisible { get; set; } = true;
        public bool FirstNameVisible { get; set; } = false;
    }
}
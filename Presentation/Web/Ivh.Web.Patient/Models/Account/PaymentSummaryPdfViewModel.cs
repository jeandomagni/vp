﻿namespace Ivh.Web.Patient.Models.Account
{
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using Application.Core.Common.Dtos;
    using Common.Base.Constants;
    using Common.VisitPay.Constants;
    using Common.Web.Attributes;

    public class PaymentSummaryPdfViewModel
    {
        [LocalizedDisplayName(TextRegionConstants.DateRange)]
        public string DateRange { get; set; }

        [LocalizedDisplayName(TextRegionConstants.FileDate)]
        public string FileDate { get; set; }

        [LocalizedDisplayName(TextRegionConstants.ExportedBy)]
        public string ExportedBy { get; set; }
        public string TotalTransactionAmount { get; set; }

        public IList<PaymentSummaryGroupedTransactionPdfViewModel> PaymentSummaries { get; set; }
    }
}
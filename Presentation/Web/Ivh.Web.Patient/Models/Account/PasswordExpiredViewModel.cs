namespace Ivh.Web.Patient.Models.Account
{
    using System.ComponentModel.DataAnnotations;
    using Common.Base.Constants;
    using Common.VisitPay.Constants;
    using Ivh.Common.Web.Attributes;

    public class PasswordExpiredViewModel
    {
        [Required]
        [LocalizedDisplayName(TextRegionConstants.PasswordNew)]
        public string NewPassword { get; set; }

        [Required]
        [LocalizedDisplayName(TextRegionConstants.Password)]
        public string Password { get; set; }
        
        [Required]
        public string Username { get; set; }
    }
}
﻿namespace Ivh.Web.Patient.Models.Landing
{
    using Common.Base.Enums;
    using Common.VisitPay.Enums;
    using Common.Web.Models.Shared;

    public class LandingViewModel
    {
        public CmsViewModel TitleLeft { get; set; }
        public CmsViewModel TitleRight { get; set; }
        public CmsViewModel ContentLeft { get; set; }
        public CmsViewModel ContentRight { get; set; }
        public CmsRegionEnum? SsoCmsRegionEnum { get; set; }
    }
}
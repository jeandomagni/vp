﻿namespace Ivh.Web.Patient.Models.Offline
{
    using Common.Web.Models.Payment;
    using Common.Web.Models.Statement;

    public class OfflineIndexViewModel
    {
        public int VisitPayUserId { get; set; }

        public string PaymentSubmitUrl { get; set; }

        public string PaymentValidateUrl { get; set; }

        public PaymentOptionViewModel FinancePlanPaymentOption { get; set; }

        public StatementListViewModel Statements { get; set; }
    }
}
﻿namespace Ivh.Web.Patient.Models.Offline
{
    using Common.Web.Models.Payment;

    public class OfflineAccountSettingsViewModel
    {
        public bool GuarantorUseAutoPay { get; set; }
        public PaymentMethodsViewModel PaymentMethods { get; set; }
        public string UrlPaymentMethodBank { get; set; }
        public string UrlPaymentMethodCard { get; set; }
        public string UrlPaymentMethodsList { get; set; }
        public string UrlRemovePaymentMethod { get; set; }
        public string UrlSavePaymentMethodBank { get; set; }
        public string UrlSavePaymentMethodCard { get; set; }
        public string UrlSetPrimary { get; set; }
    }
}
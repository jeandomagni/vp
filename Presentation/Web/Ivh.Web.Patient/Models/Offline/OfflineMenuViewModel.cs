﻿namespace Ivh.Web.Patient.Models.Offline
{
    using System;
    using System.Collections.Generic;
    using System.Web;
    using System.Web.Routing;

    public class OfflineMenuViewModel
    {
        public static List<OfflineMenuItem> MenuItems = new List<OfflineMenuItem>
        {
            new OfflineMenuItem("Index", "Offline", "Home", "glyphicon glyphicon-home"),
            new OfflineMenuItem("AccountSettings", "Offline", "Account Settings", "vpicon-gear")
        };

        public static bool IsActive(OfflineMenuItem menuItem)
        {
            RouteData routeData = HttpContext.Current.Request.RequestContext?.RouteData;
            if (routeData == null)
            {
                return false;
            }

            string currentAction = routeData.Values.ContainsKey("action") ? routeData.GetRequiredString("action") : string.Empty;
            if (!string.Equals(currentAction, menuItem.Action, StringComparison.OrdinalIgnoreCase))
            {
                return false;
            }

            string currentController = routeData.Values.ContainsKey("controller") ? routeData.GetRequiredString("controller") : string.Empty;
            if (!string.Equals(currentController, menuItem.Controller, StringComparison.OrdinalIgnoreCase))
            {
                return false;
            }

            return true;
        }
    }
}
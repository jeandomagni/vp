﻿namespace Ivh.Web.Patient.Models.Offline
{
    using System.ComponentModel.DataAnnotations;
    using Common.VisitPay.Constants;
    using Common.Web.Attributes;

    public class CreateAccountViewModel
    {
        [LocalizedDisplayName(TextRegionConstants.EmailAddress)]
        [Required]
        [RegularExpressionEmailAddress(TextRegionConstants.EmailInvalid)]
        public string EmailAddress { get; set; }

        [LocalizedDisplayName(TextRegionConstants.Password)]
        [Required]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        [Required]
        public int TermsOfUseCmsVersionId { get; set; }

        [RequireChecked]
        public bool TermsOfUseAgreed { get; set; }

        [RequireChecked]
        public bool  IsGuarantorSelfVerified { get; set; }
    }
}
﻿namespace Ivh.Web.Patient.Models.Offline
{
    public class OfflineMenuItem
    {
        public OfflineMenuItem(string action, string controller, string text, string icon)
        {
            this.Action = action;
            this.Controller = controller;
            this.Text = text;
            this.Icon = icon;
        }

        public string Action { get; }

        public string Controller { get; }

        public string Text { get; }

        public string Icon { get; }
    }
}
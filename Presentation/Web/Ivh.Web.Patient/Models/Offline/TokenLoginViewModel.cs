﻿namespace Ivh.Web.Patient.Models.Offline
{
    using System.ComponentModel.DataAnnotations;
    using Common.VisitPay.Constants;
    using Common.Web.Attributes;

    public class TokenLoginViewModel
    {
        [Required]
        [LocalizedDisplayName(TextRegionConstants.NameLast)]
        public string LastName { get; set; }

        [LocalizedDisplayName(TextRegionConstants.Dob)]
        [LocalizedPrompt(TextRegionConstants.DobMonth)]
        [Required]
        public string DateOfBirthMonth { get; set; }

        [LocalizedDisplayName(TextRegionConstants.DobDay)]
        [LocalizedPrompt(TextRegionConstants.DobDay)]
        [Required]
        [Range(1, 31)]
        [MaxLength(2)]
        public string DateOfBirthDay { get; set; }

        [LocalizedDisplayName(TextRegionConstants.DobYear)]
        [LocalizedPrompt(TextRegionConstants.DobYear)]
        [Required]
        [MinLength(4)]
        [MaxLength(4)]
        public string DateOfBirthYear { get; set; }

        [Required]
        public string Token { get; set; }
    }
}
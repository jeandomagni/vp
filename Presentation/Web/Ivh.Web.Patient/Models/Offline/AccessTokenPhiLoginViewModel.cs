﻿namespace Ivh.Web.Patient.Models.Offline
{
    using System.ComponentModel.DataAnnotations;
    using Common.VisitPay.Attributes;
    using Common.VisitPay.Constants;
    using Common.Web.Attributes;
    using Domain.User.Entities;

    public class AccessTokenPhiLoginViewModel
    {
        [Required]
        [LocalizedDisplayName(TextRegionConstants.NameLast)]
        public string LastName { get; set; }

        [LocalizedDisplayName(TextRegionConstants.Dob)]
        [LocalizedPrompt(TextRegionConstants.DobMonth)]
        [Required]
        public string DateOfBirthMonth { get; set; }

        [LocalizedDisplayName(TextRegionConstants.DobDay)]
        [LocalizedPrompt(TextRegionConstants.DobDay)]
        [Required]
        [Range(1, 31)]
        [MaxLength(2)]
        public string DateOfBirthDay { get; set; }

        [LocalizedDisplayName(TextRegionConstants.DobYear)]
        [LocalizedPrompt(TextRegionConstants.DobYear)]
        [Required]
        [MinLength(4)]
        [MaxLength(4)]
        public string DateOfBirthYear { get; set; }

        [DataType(nameof(AccessTokenPhi))]
        [LocalizedDisplayName(TextRegionConstants.AccessTokenPhi)]
        [Required]
        [RegularExpression(AccessTokenPhiValueGenerator.ValidationPatternRegularExpression)]
        [Phi]
        public string AccessTokenPhi { get; set; }
    }
}
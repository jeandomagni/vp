﻿namespace Ivh.Web.Patient.Models.Sso
{
    using Common.Base.Enums;
    using Common.VisitPay.Enums;
    using Common.Web.Models.Shared;

    public class SsoTermsViewModel
    {
        public SsoProviderEnum SsoProvider { get; set; }
        public CmsViewModel Description { get; set; }
        public CmsViewModel Terms { get; set; }
    }

    public class SsoTermsActionViewModel
    {
        public SsoProviderEnum SsoProvider { get; set; }
        public CmsViewModel Content { get; set; }
    }
}
﻿namespace Ivh.Web.Patient.Models.Sso
{
    using Common.Base.Enums;
    using Common.VisitPay.Enums;

    public class HealthEquityWidgetViewModel
    {

        public HealthEquityWidgetViewModel()
        {
        }

        public bool Visible
        {
            get
            {
                if (!this.IsUserAuthorizedToSee || this.IsUserEmulating)
                {
                    return false;
                }
                return this.ShowBalance;
            }
        }

        public string UrlToAccept { get; set; }
        public string UrlToReject { get; set; }
        public SsoProviderEnum SsoProviderEnum { get; set; }
        

        //Master feature toggle
        public bool FeatureEnabled { get; set; }

        public bool ShowBalance
        {
            get { return this.FeatureEnabled && this.SsoOutboundAgreed && this.ShowBalanceSetting; }
        }

        //This is if we should show the Balance or not
        public bool ShowBalanceSetting { get; set; }

        //This is the outbound HQY SSO
        public bool ShowLinkToHqySetting { get; set; }

        public bool ShowLinkToHqy
        {
            get { return this.FeatureEnabled && this.SsoOutboundAgreed && this.ShowLinkToHqySetting; }
        }

        //This is the link that allows the user to manage their SSO connections
        public bool ShowSingleSignOnConnectionsSetting { get; set; }

        public bool ShowSsoManagementLink
        {
            get { return this.FeatureEnabled && this.ShowSingleSignOnConnectionsSetting; }
        }
        public bool SsoOutboundAgreed { get; set; }
        public bool IgnoredSso { get; set; }
        public bool IsUserEmulating { get; set; }
        public bool IsUserAuthorizedToSee { get; set; }
        public bool ShouldOpenInitialHqyWidgetDialog { get; set; }
    }
}
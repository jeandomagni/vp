namespace Ivh.Web.Patient.Models.Sso
{
    using Application.Core.Common.Dtos;
    using Common.Base.Enums;
    using Common.VisitPay.Enums;

    public class RedirectToOAuthViewModel
    {
        public SsoProviderEnum? ProviderEnum { get; set; }
        public SsoStatusEnum? SsoStatusEnum { get; set; }
    }
}
﻿namespace Ivh.Web.Patient.Models.Sso
{
    public class HealthEquityBalanceWidgetViewModel
    {
        public string CashBalance { get; set; }
        public string InvestmentBalance { get; set; }
        public string ContributionsYtd { get; set; }
        public string DistributionsYtd { get; set; }
        public string LastUpdated { get; set; }
    }
}
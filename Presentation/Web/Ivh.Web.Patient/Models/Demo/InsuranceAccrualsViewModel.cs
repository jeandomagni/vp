﻿namespace Ivh.Web.Patient.Models.Demo
{
    public class InsuranceAccrualsViewModel
    {
        public string InsurerName { get; set; }
        public string InsurerLogoUrl { get; set; }
    }
}
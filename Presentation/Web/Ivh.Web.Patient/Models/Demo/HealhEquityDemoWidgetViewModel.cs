﻿namespace Ivh.Web.Patient.Models.Demo
{
    public class HealhEquityDemoWidgetViewModel
    {
        public decimal CashBalance { get; set; }

        public decimal InvestmentBalance { get; set; }

        public decimal TotalBalance => this.CashBalance + this.InvestmentBalance;

        public decimal YtdContributions { get; set; }

        public decimal YtdDistributions { get; set; }

        public string UpdatedDate { get; set; }
    }
}
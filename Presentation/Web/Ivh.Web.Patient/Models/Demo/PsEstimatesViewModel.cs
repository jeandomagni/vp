﻿namespace Ivh.Web.Patient.Models.Demo
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    public class PsEstimatesViewModel
    {
        public PsEstimatesViewModel()
        {
            this.HospitalCharges = new List<PsEstimateViewModel>();
            this.InsuranceCharges = new List<PsEstimateViewModel>();
            this.Payments = new List<PsEstimateViewModel>();
        }

        public DateTime Date { get; set; }

        public string Description { get; set; }

        public string Location { get; set; }
        
        public decimal Deductible { get; set; }

        public decimal DeductibleMet { get; set; }

        public IList<PsEstimateViewModel> HospitalCharges { get; set; }

        public IList<PsEstimateViewModel> InsuranceCharges { get; set; }

        public IList<PsEstimateViewModel> Payments { get; set; }

        public decimal HospitalChargesSum => this.HospitalCharges.Sum(x => x.Amount);

        public decimal InsuranceChargesSum => this.InsuranceCharges.Sum(x => x.Amount);

        public decimal PaymentsSum => this.Payments.Sum(x => x.Amount);
        
        public decimal Total => this.HospitalChargesSum + this.InsuranceChargesSum + this.PaymentsSum;

        public bool HasPayment => this.Payments.Any();
    }

    public class PsEstimateViewModel
    {
        public decimal Amount { get; set; }

        public string Description { get; set; }
    }
}
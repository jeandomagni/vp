﻿namespace Ivh.Web.Patient.Models.Notifications
{
    using System.Collections.Generic;

    public class NotificationFilterViewModel
    {
        public NotificationFilterViewModel()
        {
            this.Notifications = new List<SystemMessageViewModel>();
        }

        public IList<SystemMessageViewModel> Notifications { get; set; }
    }
}
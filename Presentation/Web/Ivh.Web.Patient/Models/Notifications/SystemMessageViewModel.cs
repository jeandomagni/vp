﻿namespace Ivh.Web.Patient.Models.Notifications
{
    using Common.Base.Enums;
    using Common.VisitPay.Enums;

    public class SystemMessageViewModel
    {
        public int SystemMessageId { get; set; }
        public string ContentBody { get; set; }
        public string ContentTitle { get; set; }
        public SystemMessageTypeEnum SystemMessageType { get; set; }
        public SystemMessageCriteriaEnum? SystemMessageCriteria { get; set; }

        public bool MessageRead { get; set; }
    }
}
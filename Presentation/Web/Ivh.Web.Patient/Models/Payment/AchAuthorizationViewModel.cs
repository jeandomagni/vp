﻿namespace Ivh.Web.Patient.Models.Payment
{
    using System.Collections.Generic;
    using System.Linq;
    using Common.Web.Models.Shared;

    public class AchAuthorizationViewModel
    {
        public int AchAuthorizationCmsVersionId { get; set; }

        public CmsViewModel Agreement { get; set; } = new CmsViewModel();

        public bool ShowNames => this.FinancePlans.Any(x => x.IsManagedUser);

        public IList<AchAuthorizationFinancePlan> FinancePlans { get; set; } = new List<AchAuthorizationFinancePlan>();

        public string AchLastFour { get; set; }
    }
}
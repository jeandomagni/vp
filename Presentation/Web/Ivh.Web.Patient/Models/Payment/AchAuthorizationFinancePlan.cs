﻿namespace Ivh.Web.Patient.Models.Payment
{
    using System;

    public class AchAuthorizationFinancePlan
    {
        public string DisplayFirstNameLastName { get; set; }

        public bool IsManagedUser { get; set; }

        public decimal MonthlyPaymentAmount { get; set; }

        public int MonthsRemaining { get; set; }

        public DateTime OriginationDate { get; set; }
    }
}
﻿namespace Ivh.Web.Patient.Models.Payment
{
    public class AchAuthorizationAgreeViewModel : AchAuthorizationParametersViewModel
    {
        public int AchAuthorizationCmsVersionId { get; set; }
    }
}
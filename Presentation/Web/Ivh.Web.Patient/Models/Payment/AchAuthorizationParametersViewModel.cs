﻿namespace Ivh.Web.Patient.Models.Payment
{
    using System;
    using System.Collections.Generic;

    public class AchAuthorizationParametersViewModel
    {
        public int PaymentMethodId { get; set; }

        public int? FinancePlanOfferId { get; set; }

        public IList<int> FinancePlanIds { get; set; } = new List<int>();

        public Guid? ConsolidationGuarantorId { get; set; }

        public int? FinancePlanIdToReconfigure { get; set; }
    }
}
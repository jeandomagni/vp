﻿namespace Ivh.Web.Patient.Models.Home
{
    using Common.Web.Models.FinancePlan;

    public class HomeViewModel
    {
        public bool IsDiscountEligible { get; set; }
        public decimal TotalBalance { get; set; }
        public FinancePlanSummaryViewModel FinancePlanSummary { get; set; }
    }
}
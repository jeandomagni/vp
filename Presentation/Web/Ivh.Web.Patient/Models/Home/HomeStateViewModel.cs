﻿namespace Ivh.Web.Patient.Models.Home
{
    using System;

    [Serializable]
    public class HomeStateViewModel
    {
        public bool IsStatementsExpanded { get; set; }
        public bool IsVisitsExpanded { get; set; }
    }
}
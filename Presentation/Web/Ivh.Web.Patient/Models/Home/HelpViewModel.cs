﻿namespace Ivh.Web.Patient.Models.Home
{
    using System.Collections.Generic;
    using Common.Web.Models.Shared;
    using KnowledgeBase;

    public class HelpViewModel
    {
        public HelpViewModel(ContentMenuViewModel contentMenuViewModel, IList<KnowledgeBaseCategoryQuestionsViewModel> knowledgeBaseCategoriesViewModel)
        {
            this.ContentMenu = contentMenuViewModel;
            this.KnowledgeBaseCategoriesViewModel = knowledgeBaseCategoriesViewModel;
        }

        public ContentMenuViewModel ContentMenu { get; }

        public IList<KnowledgeBaseCategoryQuestionsViewModel> KnowledgeBaseCategoriesViewModel { get; }
    }
}
﻿namespace Ivh.Web.Patient.Models.Consolidate
{
    using System.Collections.Generic;
    using System.Linq;
    using Common.Web.Models.Consolidate;

    public class ManageHouseholdViewModel
    {
        public ManageHouseholdViewModel()
        {
            this.ManagedGuarantors = new List<ConsolidationGuarantorViewModel>();
            this.ManagingGuarantors = new List<ConsolidationGuarantorViewModel>();
        }

        public IList<ConsolidationGuarantorViewModel> ManagedGuarantors { get; set; }
        public IList<ConsolidationGuarantorViewModel> ManagingGuarantors { get; set; }

        public bool CanRequestToBeManaged
        {
            get
            {
                if (this.ManagedGuarantors.Any(x => x.IsActive))
                    return false;

                return this.ManagingGuarantors.All(x => x.IsInactive);
            }
        }

        public bool CanRequestToBeManaging
        {
            get
            {
                return !this.ManagingGuarantors.Any(x => x.IsActive);
            }
        }

        // view a managing guarantor would see
        public bool ShowManagingGuarantorView 
        {
            get
            {
                return !this.HasActiveRelationship || this.ManagedGuarantors.Any();
            }
        }

        // view a managed guarantor would see
        public bool ShowManagedGuarantorView 
        {
            get
            {
                return !this.HasActiveRelationship || this.ManagingGuarantors.Any();
            }
        }

        private bool HasActiveRelationship
        {
            get
            {
                return this.ManagingGuarantors.Any(x => x.IsActive) ||
                       this.ManagedGuarantors.Any(x => x.IsActive);
            }
        }
    }
}
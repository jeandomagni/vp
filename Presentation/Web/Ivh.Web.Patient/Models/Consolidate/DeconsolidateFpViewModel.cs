﻿namespace Ivh.Web.Patient.Models.Consolidate
{
    using System;
    using Common.Base.Enums;
    using Common.VisitPay.Enums;

    public class DeconsolidateFpViewModel
    {
        public Guid ConsolidationGuarantorId { get; set; }
        public ConsolidationMatchRequestTypeEnum RequestType { get; set; }
        public string CancelCmsVersionText { get; set; }
    }
}
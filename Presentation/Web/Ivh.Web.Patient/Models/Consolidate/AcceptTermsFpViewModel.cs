﻿namespace Ivh.Web.Patient.Models.Consolidate
{
    using System.Collections.Generic;
    using Common.Web.Models.Consolidate;
    using Common.Web.Models.Shared;

    public class AcceptTermsFpViewModel : AcceptTermsViewModel
    {
        public AcceptTermsFpViewModel()
        {
            this.FinancePlans = new List<ConsolidationGuarantorFinancePlanViewModel>();
        }

        public IList<ConsolidationGuarantorFinancePlanViewModel> FinancePlans { get; set; }

        public bool InitiatedByManaging { get; set; }

        public string CmsVersionText { get; set; }

        public CmsViewModel ConsolidationAcceptTerms { get; set; }
    }
}
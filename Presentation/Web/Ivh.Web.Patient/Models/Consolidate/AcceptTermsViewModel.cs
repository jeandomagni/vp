﻿namespace Ivh.Web.Patient.Models.Consolidate
{
    using System;
    using Common.Base.Enums;
    using Common.VisitPay.Enums;
    using Common.Web.Models.Shared;

    public class AcceptTermsViewModel
    {
        public Guid ConsolidationGuarantorId { get; set; }
        public ConsolidationMatchRequestTypeEnum RequestType { get; set; }
        public CmsViewModel TextCms { get; set; }
        public CmsViewModel TermsCms { get; set; }
        public CmsViewModel HipaaCms { get; set; }
    }
}
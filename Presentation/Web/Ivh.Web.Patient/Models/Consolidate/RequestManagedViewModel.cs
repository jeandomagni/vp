﻿namespace Ivh.Web.Patient.Models.Consolidate
{
    using Common.Web.Models.Shared;

    public class RequestManagedViewModel
    {
        public CmsViewModel TextCms { get; set; }
        public CmsViewModel TermsCms { get; set; }
        public CmsViewModel HipaaCms { get; set; }
    }
}
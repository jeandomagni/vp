﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Ivh.Web.Patient.Models.Consolidate
{
    using Common.Web.Models.Shared;

    public class RequestManagingViewModel
    {
        public CmsViewModel TextCms { get; set; }
        public CmsViewModel TermsCms { get; set; }
    }
}
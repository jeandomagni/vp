﻿namespace Ivh.Web.Patient.Models.Consolidate
{
    using System.ComponentModel.DataAnnotations;
    using Common.Base.Constants;
    using Common.Base.Enums;
    using Common.VisitPay.Constants;
    using Common.VisitPay.Enums;
    using Common.Web.Attributes;

    public class MatchRequestViewModel
    {
        [LocalizedDisplayName(TextRegionConstants.NameFirst)]
        [LocalizedPrompt(TextRegionConstants.NameFirst)]
        [Required, MaxLength(50)]
        public string FirstName { get; set; }

        [LocalizedDisplayName(TextRegionConstants.NameLast)]
        [LocalizedPrompt(TextRegionConstants.NameLast)]
        [Required, MaxLength(50)]
        public string LastName { get; set; }

        [LocalizedDisplayName(TextRegionConstants.YearOfBirth)]
        [LocalizedPrompt(TextRegionConstants.Yyyy)]
        [Required, MinLength(4), MaxLength(4), AgeDateRange(MaximumAge = 120, MinimumAge = 0)]
        public string BirthYear { get; set; }

        [LocalizedDisplayName(TextRegionConstants.EmailAddress)]
        [LocalizedPrompt(TextRegionConstants.EmailAddress)]
        [Required, RegularExpressionEmailAddress(TextRegionConstants.EmailInvalid)]
        [DataType(DataType.EmailAddress)]
        public string EmailAddress { get; set; }

        public ConsolidationMatchRequestTypeEnum MatchRequestType { get; set; }
    }
}
﻿namespace Ivh.Web.Patient.Models.Consolidate
{
    using System;

    public class GuarantorFilterDetailedViewModel
    {
        public string DisplayText { get; set; }
        public string Name { get; set; }
        public Guid ObfuscatedVpGuarantorId { get; set; }
        public decimal Scvb { get; set; }
        public bool Selected { get; set; }
        public bool IsPastDue { get; set; }
    }
}
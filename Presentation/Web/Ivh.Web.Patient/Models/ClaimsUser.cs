﻿namespace Ivh.Web.Patient.Models
{
    using System.Security.Claims;
    using Common.Base.Enums;
    using Common.VisitPay.Enums;

    public class ClaimsUser : ClaimsPrincipal
    {
        public ClaimsUser(ClaimsPrincipal principal) : base(principal)
        {
        }

        public bool IsEmulated
        {
            get
            {
                Claim claim = this.FindFirst(ClaimTypeEnum.EmulatedUser.ToString());
                bool emulating;
                if (claim != null && bool.TryParse(claim.Value, out emulating))
                {
                    return emulating;
                }
                return false;
            }
        }

        public string ClientUsername
        {
            get
            {
                Claim claim = this.FindFirst(ClaimTypeEnum.ClientUsername.ToString());
                return claim == null ? string.Empty : claim.Value;
            }
        }

        public int ClientUserId
        {
            get
            {
                Claim claim = this.FindFirst(ClaimTypeEnum.ClientUserId.ToString());
                int id;
                if (claim != null && int.TryParse(claim.Value, out id))
                {
                    return id;
                }
                return -1;
            }
        }

        public string EmulationToken
        {
            get
            {
                Claim claim = this.FindFirst(ClaimTypeEnum.EmulationToken.ToString());
                return claim == null ? string.Empty : claim.Value;
            }
        }
    }
}
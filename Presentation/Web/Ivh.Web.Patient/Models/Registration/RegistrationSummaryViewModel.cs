﻿namespace Ivh.Web.Patient.Models.Registration
{
    using Common.Web.Models.Shared;

    public class RegistrationSummaryViewModel
    {
        public CmsViewModel SummaryText { get; set; }
        public CmsViewModel SsoText { get; set; }
    }
}
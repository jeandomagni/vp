﻿namespace Ivh.Web.Patient.Models.Registration
{
    using Common.Web.Models.Shared;

    public class RegistrationWelcomeViewModel
    {
        public CmsViewModel CmsInstruction { get; set; }
        public CmsViewModel CmsContent { get; set; }
    }
}
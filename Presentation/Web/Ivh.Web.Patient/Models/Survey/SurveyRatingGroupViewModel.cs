﻿namespace Ivh.Web.Patient.Models.Survey
{
    using System.Collections.Generic;
    using System.Linq;

    public class SurveyRatingGroupViewModel
    {
        public List<SurveyRatingViewModel> SurveyRatings { get; set; }
        public int[] RatingsScores => this.SurveyRatings.Select(x => x.SurveyRatingScore).OrderBy(x => x).ToArray();

    }
}
﻿namespace Ivh.Web.Patient.Models.Survey
{
    public class SurveyRatingViewModel
    {
        public int SurveyRatingId { get; set; }
        public int SurveyRatingScore { get; set; }
        public string SurveyRatingDescription { get; set; }
    }
}
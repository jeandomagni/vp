﻿namespace Ivh.Web.Patient.Models.Survey
{
    using System.Linq;
    using Common.Base.Enums;
    using Common.VisitPay.Enums;

    public class SurveyResultAnswerViewModel
    {
        public int SurveyResultAnswerId { get; set; }
        public int SurveyQuestionId { get; set; }
        public int SurveyRatingGroupId { get; set; }
        public string QuestionText { get; set; }
        public int? SurveyQuestionRating { get; set; }
        public string SurveyRatingDescriptionText { get; set; }
        public int[] SurveyRatingScores => this.SurveyRatingGroupViewModel.RatingsScores;
        public bool isNps => SurveyRatingGroupId == (int) SurveyRatingGroupTypeEnum.NetPromoterScoreFormat;
        public SurveyRatingGroupViewModel SurveyRatingGroupViewModel { get; set; } = new SurveyRatingGroupViewModel();
    }
}
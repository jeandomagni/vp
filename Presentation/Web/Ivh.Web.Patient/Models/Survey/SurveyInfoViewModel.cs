﻿namespace Ivh.Web.Patient.Models.Survey
{
    public class SurveyInfoViewModel
    {
        public string SurveyName { get; set; }

        public int? FinancePlanId { get; set; }

        public int? PaymentId { get; set; }

    }
}
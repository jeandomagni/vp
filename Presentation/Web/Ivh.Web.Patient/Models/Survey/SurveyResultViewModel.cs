﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Ivh.Web.Patient.Models.Survey
{
    using Common.Base.Enums;
    using Common.Base.Utilities.Extensions;
    using Common.Base.Utilities.Helpers;
    using Common.VisitPay.Enums;

    public class SurveyResultViewModel
    {
        public int SurveyId { get; set; }
        public string SurveyName { get; set; }
        public string Title { get; set; }
        public SurveyResultTypeEnum SurveyResultType { get; set; }
        public SurveyRatingGroupTypeEnum SurveyRatingGroupType { get; set; }
        public DateTime SurveyDate { get; set; }
        public int? VisitPayUserId { get; set; }
        public int? FinancePlanId { get; set; }
        public int? PaymentId { get; set; }
        public string SurveyComment { get; set; }
        public List<SurveyResultAnswerViewModel> SurveyQuestions { get; set; }
        public SurveyRatingGroupViewModel SurveyRatingGroup { get; set; }
        private string _deviceType = "";

        public void SetDeviceType(HttpContext httpContext)
        {
            HttpRequest request = httpContext?.Request;
            if (request != null)
            {
                DeviceTypeEnum deviceType = DeviceTypeHelper.GetDeviceType(request.UserAgent);
                _deviceType = deviceType.ToString();
            }
        }

        public string DeviceType => this._deviceType;

        public SurveyResultAnswerViewModel NpsRatingQuestion { get; set; }

        public bool ShouldValidate => this.SurveyQuestions?.Any(c => this._groupsRequiringValidation.Contains((SurveyRatingGroupTypeEnum) c.SurveyRatingGroupId))?? false;

        private List<SurveyRatingGroupTypeEnum> _groupsRequiringValidation = new List<SurveyRatingGroupTypeEnum>
        {
            SurveyRatingGroupTypeEnum.AgreeDisagreeSurveyFormat,
            SurveyRatingGroupTypeEnum.CustomerEffortScoreFormat,
            SurveyRatingGroupTypeEnum.TimeOnTaskFormat
        };
    }
}
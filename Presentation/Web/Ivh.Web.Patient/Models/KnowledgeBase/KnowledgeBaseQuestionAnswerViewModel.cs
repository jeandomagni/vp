namespace Ivh.Web.Patient.Models.KnowledgeBase
{
    using Common.Base.Enums;
    using Common.VisitPay.Enums;

    public class KnowledgeBaseQuestionAnswerViewModel : BaseKnowledgeBaseQuestionViewModel
    {
        public CmsRegionEnum AnswerCmsRegion { get; set; }
        public string AnswerContentBody { get; set; }
        public string AnswerContentTitle { get; set; }
    }
}
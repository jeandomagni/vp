namespace Ivh.Web.Patient.Models.KnowledgeBase
{
    using Common.Base.Enums;
    using Common.VisitPay.Enums;

    public class BaseKnowledgeBaseQuestionViewModel
    {
        public int KnowledgeBaseQuestionAnswerId { get; set; }

        public CmsRegionEnum QuestionCmsRegion { get; set; }

        public string QuestionContentBody { get; set; }

        public string QuestionContentTitle { get; set; }

        public bool IsMobileKnowledgeBase { get; set; }

        public bool IsWebKnowledgeBase { get; set; }

        public bool IsClientKnowledgeBase { get; set; }

        public bool IsActive { get; set; }

        public int DisplayOrder { get; set; }

        public int Rank { get; set; }
    }
}
﻿namespace Ivh.Web.Patient.Models.KnowledgeBase
{
    public class KnowledgeBaseQuestionViewModel : BaseKnowledgeBaseQuestionViewModel
    {
        public int KnowledgeBaseSubCategoryId { get; set; }
    }
}
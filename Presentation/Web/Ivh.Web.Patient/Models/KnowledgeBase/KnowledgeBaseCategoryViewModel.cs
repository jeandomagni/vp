namespace Ivh.Web.Patient.Models.KnowledgeBase
{
    using System.Collections.Generic;
    using System.Linq;

    public class KnowledgeBaseCategoryViewModel : BaseKnowledgeBaseCategoryViewModel
    {
        public List<KnowledgeBaseSubCategoryViewModel> KnowledgeBaseSubCategories { get; set; }

        public List<KnowledgeBaseSubCategoryViewModel> KnowledgeBaseSubCategoriesOrdered => KnowledgeBaseSubCategories.OrderBy(x => x.DisplayOrder).ToList();
    }
}
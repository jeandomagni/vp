namespace Ivh.Web.Patient.Models.KnowledgeBase
{
    using System.Collections.Generic;

    public class KnowledgeBaseCategoryQuestionsViewModel : BaseKnowledgeBaseCategoryViewModel
    {
        public IList<KnowledgeBaseQuestionViewModel> Questions { get; set; }
    }
}
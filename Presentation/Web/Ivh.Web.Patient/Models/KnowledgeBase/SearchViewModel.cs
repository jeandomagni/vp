﻿namespace Ivh.Web.Patient.Models.KnowledgeBase
{
    using System.Collections.Generic;

    public class SearchViewModel
    {
        public SearchViewModel()
        {
            this.SearchData = new List<SearchDatum>();
        }

        public IList<SearchDatum> SearchData { get; set; }
    }

    public class SearchDatum
    {
        public SearchDatum(string identity, string redirectUrl, string tags, object value = null)
        {
            this.Identity = identity;
            this.Value = value;
            this.RedirectUrl = redirectUrl;
            this.Tags = tags;
        }
        public string Identity { get; }
        public string Tags { get; }
        public object Value { get; }
        public string RedirectUrl { get; }

        public override int GetHashCode()
        {
            return this.Identity.GetHashCode();
        }

        public override bool Equals(object obj)
        {
            return this.GetType() == obj?.GetType() && this.Identity.Equals(((SearchDatum) obj).Identity);
        }
    }
}
﻿namespace Ivh.Web.Patient.Models.KnowledgeBase
{
    using System.Collections.Generic;

    public class KnowledgeBaseViewModel
    {
        public List<KnowledgeBaseCategoryViewModel> Categories { get; set; }
        public List<KnowledgeBaseQuestionAnswerViewModel> QuestionAnswers { get; set; }
        public int? ActiveCategoryId { get; set; }
        public int? ActiveSubCategoryId { get; set; }
        public int? ActiveQuestionId { get; set; }
        public bool ShowActiveCategoryContent { get; set; }
        public SearchViewModel Search { get; set; }
    }
}
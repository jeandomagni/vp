﻿namespace Ivh.Web.Patient.Models.KnowledgeBase
{
    using System.Collections.Generic;

    public class KnowledgeBaseCategoryQuestionAnswersViewModel : BaseKnowledgeBaseCategoryViewModel
    {
        public KnowledgeBaseCategoryQuestionAnswersViewModel()
        {
            this.Questions = new List<KnowledgeBaseQuestionAnswerViewModel>();
        }

        public IList<KnowledgeBaseQuestionAnswerViewModel> Questions { get; set; }
    }
}
﻿namespace Ivh.Web.Patient.Models.KnowledgeBase
{
    public class KnowledgeBaseSubCategoryViewModel
    {
        public int KnowledgeBaseSubCategoryId { get; set; }

        public string Name { get; set; }

        public int DisplayOrder { get; set; }
    }
}
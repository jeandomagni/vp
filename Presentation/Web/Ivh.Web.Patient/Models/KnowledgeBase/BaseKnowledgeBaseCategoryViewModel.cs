﻿namespace Ivh.Web.Patient.Models.KnowledgeBase
{
    using Common.Base.Enums;
    using Common.VisitPay.Enums;

    public class BaseKnowledgeBaseCategoryViewModel
    {
        public int KnowledgeBaseCategoryId { get; set; }

        public string Name { get; set; }

        public CmsRegionEnum CmsRegion { get; set; }

        public string ContentBody { get; set; }

        public string ContentTitle { get; set; }

        public bool IsActive { get; set; }

        public int DisplayOrder { get; set; }

        public string IconCssClass { get; set; }
    }
}
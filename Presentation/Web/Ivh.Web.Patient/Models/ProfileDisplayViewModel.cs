namespace Ivh.Web.Patient.Models
{
    public class ProfileDisplayViewModel
    {
        public string Name { get; set; }
        public string UserName { get; set; }
        public string Email { get; set; }
        public string AddressStreet1 { get; set; }
        public string AddressStreet2 { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Zip { get; set; }
        public string MailingAddressStreet1 { get; set; }
        public string MailingAddressStreet2 { get; set; }
        public string MailingCity { get; set; }
        public string MailingState { get; set; }
        public string MailingZip { get; set; }
        public string PhoneNumber { get; set; }
        public string PhoneNumberType { get; set;  }
        public string PhoneNumberSecondary { get; set; }
        public string PhoneNumberSecondaryType { get; set; }
    }
}
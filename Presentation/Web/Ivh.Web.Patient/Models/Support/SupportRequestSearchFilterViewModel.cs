﻿namespace Ivh.Web.Patient.Models.Support
{
    using Common.VisitPay.Enums;

    public class SupportRequestSearchFilterViewModel
    {
        public SupportRequestStatusEnum? SupportRequestStatus { get; set; }
    }
}
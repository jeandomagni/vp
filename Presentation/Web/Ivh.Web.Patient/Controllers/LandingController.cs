﻿namespace Ivh.Web.Patient.Controllers
{
    using System;
    using System.Threading.Tasks;
    using System.Web.Mvc;
    using Application.Content.Common.Interfaces;
    using Application.Core.Common.Interfaces;
    using Attributes;
    using Base;
    using Common.Web.Cookies;
    using Common.Web.Interfaces;

    [Authorize]
    public class LandingController : BaseLandingController
    {
        public LandingController(Lazy<IBaseControllerService> baseControllerService,
            Lazy<IContentApplicationService> contentApplicationService,
            Lazy<ISsoApplicationService> ssoApplicationService,
            Lazy<IWebCookieFacade> webCookie)
            : base(baseControllerService, contentApplicationService, ssoApplicationService, webCookie)
        {
        }

        [MobileRedirect("/mobile/Landing")]
        [AllowAnonymous]
        public override async Task<ActionResult> Index()
        {
            if (!this.IsBrowserSupported)
            {
                return this.View("~/Views/Shared/BrowserNotSupported.cshtml");
            }

            return await base.Index();
        }
    }
}
﻿namespace Ivh.Web.Patient.Controllers
{
    using System;
    using System.Linq;
    using System.Web.Mvc;
    using Application.Core.Common.Interfaces;
    using System.Reflection;
    using System.Threading.Tasks;
    using System.Web;
    using Application.Content.Common.Interfaces;
    using Application.Core.Common.Dtos;
    using Application.User.Common.Dtos;
    using Attributes;
    using AutoMapper;
    using Base;
    using Common.Base.Utilities.Extensions;
    using Common.Cache;
    using Common.VisitPay.Enums;
    using Common.VisitPay.Strings;
    using Common.Web.Constants;
    using Common.Web.Cookies;
    using Common.Web.Extensions;
    using Common.Web.Filters;
    using Common.Web.Interfaces;
    using Common.Web.Models.Sso;
    using System.Web.SessionState;

    [GuarantorAuthorize(Roles = VisitPayRoleStrings.System.Patient + "," + VisitPayRoleStrings.Admin.Administrator)]
    public class SsoController : BasePatientSsoController
    {
        private readonly Lazy<IWebCookieFacade> _webCookie;

        public SsoController(Lazy<IBaseControllerService> baseControllerService,
            Lazy<ISsoApplicationService> ssoApplicationService,
            Lazy<IContentApplicationService> contentApplicationService,
            Lazy<IWebCookieFacade> webCookie,
            Lazy<IGuarantorApplicationService> guarantorApplicationService,
            Lazy<IDistributedCache> distributedCache)
            : base(
            baseControllerService,
            ssoApplicationService,
            contentApplicationService,
            guarantorApplicationService,
            distributedCache
            )
        {
            this._webCookie = webCookie;
        }

        [AllowAnonymous]
        [HttpGet]
        [RequireFeature(VisitPayFeatureEnum.MyChartSso)]
        public async Task<ActionResult> MyChart()
        {
            HttpContext context = System.Web.HttpContext.Current;

            if (context.User.Identity.IsAuthenticated)
            {
                context.AbandonSession();
                if (!this.Request.Cookies.AllKeys.Contains(CookieNames.SsoInvalidateActiveSessionCookieName))
                {
                    this.Response.SetCookie(CookieFactory.GetCookie(CookieNames.SsoInvalidateActiveSessionCookieName, string.Empty, TimeSpan.FromMinutes(1)));
                    return this.Redirect(this.Request.RawUrl);
                }
                else
                {
                    this.Request.Cookies.Remove(CookieNames.SsoInvalidateActiveSessionCookieName);
                    this.RedirectToAction("Login", "Account");
                }
            }

            //This method is for OpenEpic only
            string requestInfo() => this.Request.AggregateWebRequest(MethodBase.GetCurrentMethod().Name);
            this.Logger.Value.Info(requestInfo);
            SsoRequestDto request = new SsoRequestDto
            {
                ProviderEnum = SsoProviderEnum.OpenEpic
            };

            string actualKey = this.Request?.QueryString?.AllKeys.FirstOrDefault(x => string.Equals(x, "context", StringComparison.InvariantCultureIgnoreCase));
            if (actualKey != null)
            {
                //.Replace(' ', '+') is kindof a dirty hack as the + signs in base64 encoded strings get interpreted as spaces
                //Other option is to parse HttpRequest.ServerVariables["QUERY_STRING"] - Dont really want to do that if I dont have to.
                request.EncryptedPayload = this.Request.QueryString[actualKey].Replace(' ', '+');
            }

            SsoResponseDto response = await this.SsoApplicationService.Value.SignInWithSso(Mapper.Map<HttpContextDto>(context), request);
            await this.BruteForceDelayAsync(context.Request.UserHostAddress, () => response.SignInStatus == SignInStatusExEnum.Failure, () => response.SignInStatus == SignInStatusExEnum.Success);
            if (response.SignInStatus == SignInStatusExEnum.Success)
            {
                return this.RedirectToAction("Index", "Home");
            }

            if (response.SignInStatus == SignInStatusExEnum.PasswordExpired)
            {
                return this.RedirectToAction("RedirectToPasswordExpired", "Sso", new { u = response.UserName });
            }

            if (response.SignInStatus == SignInStatusExEnum.AccountClosed)
            {
                return this.RedirectToAction("Index", "Landing");
            }

            //What's the appropriate thing to do when errors happen?  Do we need different classes of errors?
            if (!response.IsError && (response.FoundRegisteredMatches || response.FoundMatchesThatCouldBeRegistered))
            {
                await this._webCookie.Value.SetSsoResponseDtoAsync(context, response);
            }

            return response.FoundRegisteredMatches ? this.RedirectToAction("Login", "Account") : this.RedirectToAction("Index", "Landing");
        }

        [AllowAnonymous]
        [HttpGet]
        [RequireFeature(VisitPayFeatureEnum.MyChartSso)]
        [Common.Web.Attributes.SessionState(SessionStateBehavior.Required)]
        public RedirectToRouteResult RedirectToPasswordExpired(string u)
        {
            this.SetTempData(TempDataKeys.Username, u);

            return this.RedirectToAction("PasswordExpired", "Account");
        }

        [HttpGet]
        [EmulateUser(AllowGet = false)]
        [GuarantorAuthorize(Roles = VisitPayRoleStrings.System.Patient)]
        public ActionResult SamlResponse(SamlResponseTargetEnum target)
        {
            SamlResponseViewModel samlResponseViewModel = this.CreateSamlResponse(target);
            if (samlResponseViewModel.IsError)
            {
                return this.View("SamlResponseFailed", samlResponseViewModel);
            }

            return this.View(samlResponseViewModel);
        }
    }
}
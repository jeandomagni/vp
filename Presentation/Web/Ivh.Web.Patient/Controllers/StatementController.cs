﻿namespace Ivh.Web.Patient.Controllers
{
    using System;
    using System.Web.Mvc;
    using Application.Content.Common.Interfaces;
    using Application.Core.Common.Interfaces;
    using Application.FinanceManagement.Common.Interfaces;
    using Attributes;
    using Common.VisitPay.Strings;
    using Common.Web.Controllers;
    using Common.Web.Filters;
    using Common.Web.Interfaces;
    using Provider.Pdf;
    using Services;
    using SessionFacade;

    [GuarantorAuthorize(Roles = VisitPayRoleStrings.System.Patient + "," + VisitPayRoleStrings.Admin.Administrator)]
    public class StatementController : BaseStatementController
    {
        private readonly Lazy<GuarantorFilterService> _guarantorFilterService;

        public StatementController(
            Lazy<IBaseControllerService> baseControllerService,
            Lazy<IContentApplicationService> contentApplicationService,
            Lazy<IStatementApplicationService> statementApplicationService,
            Lazy<IVisitPayUserApplicationService> visitPayUserApplicationService,
            Lazy<IVisitPayUserJournalEventApplicationService> visitPayUserJournalEventApplicationService,
            Lazy<IPdfConverter> pdfConverter,
            Lazy<GuarantorFilterService> guarantorFilterService,
            Lazy<IWebPatientSessionFacade> patientSession,
            Lazy<IVisitApplicationService> visitApplicationService)
            : base(
                baseControllerService,
                contentApplicationService,
                statementApplicationService,
                visitPayUserApplicationService,
                visitPayUserJournalEventApplicationService,
                pdfConverter,
                visitApplicationService)
        {
            this._guarantorFilterService = guarantorFilterService;
        }

        [HttpGet]
        public ActionResult MyStatements()
        {
            return this.RedirectToAction("Documents", "Account");
        }
        
        [HttpGet]
        [OverrideAuthorization] // <= why is this here??
        [AllowOffline]
        [EmulateUser(AllowGet = false)]
        public ActionResult FinancePlanStatementDownload(int id)
        {
            this.WritePdfInline(this.FinancePlanStatementDownload(id, this._guarantorFilterService.Value.VisitPayUserId, null), "FinancePlanStatement.pdf");

            return null;
        }

        [HttpGet]
        [OverrideAuthorization] // <= why is this here??
        [EmulateUser(AllowGet = false)]
        public ActionResult VisitBalanceSummaryDownload(int id)
        {
            this.WritePdfInline(this.StatementDownload(id, this._guarantorFilterService.Value.VisitPayUserId, null), "VisitBalanceSummary.pdf");

            return null;
        }
    }
}
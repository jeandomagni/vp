﻿namespace Ivh.Web.Patient.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using System.Web.Mvc;
    using System.Web.SessionState;
    using Application.Base.Common.Interfaces;
    using Application.Content.Common.Dtos;
    using Application.Content.Common.Interfaces;
    using Application.Core.Common.Dtos;
    using Application.Core.Common.Interfaces;
    using Application.FinanceManagement.Common.Interfaces;
    using Attributes;
    using AutoMapper;
    using Base;
    using Common.VisitPay.Enums;
    using Common.VisitPay.Strings;
    using Common.Web.Interfaces;
    using Common.Web.Models.Shared;
    using Models.Home;
    using Models.KnowledgeBase;
    using Services;
    using SessionFacade;

    [GuarantorAuthorize(Roles = VisitPayRoleStrings.System.Patient + "," + VisitPayRoleStrings.Admin.Administrator)]
    public class HomeController : BaseHomeController
    {
        public HomeController(
            Lazy<IBaseControllerService> baseControllerService,
            Lazy<IContentApplicationService> contentApplicationService,
            Lazy<IFinancePlanApplicationService> financePlanApplicationService,
            Lazy<IVisitBalanceBaseApplicationService> visitBalanceBaseApplicationService,
            Lazy<IVisitPayUserApplicationService> visitPayUserApplicationService,
            Lazy<GuarantorFilterService> guarantorFilterService,
            Lazy<IWebPatientSessionFacade> patientSession,
            Lazy<IPaymentMethodsApplicationService> paymentMethodsApplicationService,
            Lazy<IPaymentSubmissionApplicationService> paymentSubmissionApplicationService,
            Lazy<IKnowledgeBaseApplicationService> knowledgeBaseApplicationService
        ) :
            base(baseControllerService,
                contentApplicationService,
                financePlanApplicationService,
                visitBalanceBaseApplicationService,
                visitPayUserApplicationService,
                guarantorFilterService,
                patientSession,
                paymentMethodsApplicationService,
                paymentSubmissionApplicationService,
                knowledgeBaseApplicationService)
        {
        }

        const string HideBuildBannerKey = "HideBuildBanner";

        [HttpGet]
        public async Task<ActionResult> Help()
        {
            ContentMenuDto contentMenuDto = await this.ContentApplicationService.Value.GetContentMenuAsync(ContentMenuEnum.HelpCenter, this.User.Identity.IsAuthenticated);
            IList<KnowledgeBaseCategoryQuestionsDto> knowledgeBaseCategoryQuestionsDto = await this.KnowledgeBaseApplicationService.Value.GetKnowledgeBaseCategoryQuestionsFlattenedAsync(KnowledgeBaseApplicationTypeEnum.Web);

            ContentMenuViewModel contentMenuViewModel = Mapper.Map<ContentMenuViewModel>(contentMenuDto);
            IList<KnowledgeBaseCategoryQuestionsViewModel> knowledgeBaseViewModel = Mapper.Map<IList<KnowledgeBaseCategoryQuestionsViewModel>>(knowledgeBaseCategoryQuestionsDto);

            HelpViewModel model = new HelpViewModel(contentMenuViewModel, knowledgeBaseViewModel);
            
            return this.View(model);
        }
        
        [HttpGet]
        public ActionResult Faqs()
        {
            return this.RedirectToAction("Index", "KnowledgeBase");
        }
        
        [RequiresSsoAuthorization(Enabled = false)]
        [AllowAnonymous]
        [HttpGet]
        [Common.Web.Attributes.SessionState(SessionStateBehavior.Required)]
        public ActionResult SwitchTo(string mode)
        {
            bool isMobile = mode.ToUpper() == "MOBILE";

            this.PatientSession.Value.UserIsMobileDevice = isMobile;

            return this.Redirect(isMobile ? "/mobile" : "/");
        }

        [AllowAnonymous]
        [HttpPost]
        [Common.Web.Attributes.SessionState(SessionStateBehavior.Required)]
        public void HideBuildBanner()
        {
            if (this.ShowBuildBanner)
            {
                this.PatientSession.Value.SetSessionValue(HideBuildBannerKey, true);
            }
        }
    }
}
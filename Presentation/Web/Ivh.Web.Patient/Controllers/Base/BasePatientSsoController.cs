﻿namespace Ivh.Web.Patient.Controllers.Base
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Net;
    using System.Threading.Tasks;
    using System.Web;
    using System.Web.Http;
    using System.Web.Mvc;
    using System.Web.SessionState;
    using Application.AppIntelligence.Common.Dtos;
    using Application.Content.Common.Dtos;
    using Application.Content.Common.Interfaces;
    using Application.Core.Common.Dtos;
    using Application.Core.Common.Interfaces;
    using Application.User.Common.Dtos;
    using Attributes;
    using AutoMapper;
    using Common.Base.Utilities.Extensions;
    using Common.Cache;
    using Common.VisitPay.Constants;
    using Common.VisitPay.Enums;
    using Common.VisitPay.Strings;
    using Common.Web.Controllers;
    using Common.Web.Filters;
    using Common.Web.Interfaces;
    using Common.Web.Models.Shared;
    using Common.Web.Models.Sso;
    using Domain.Logging.Interfaces;
    using Models.Sso;

    [GuarantorAuthorize(Roles = VisitPayRoleStrings.System.Patient + "," + VisitPayRoleStrings.Admin.Administrator)]
    public class BasePatientSsoController : BaseSsoController
    {
        private readonly Lazy<ISsoApplicationService> _ssoApplicationService;

        public BasePatientSsoController(Lazy<IBaseControllerService> baseControllerService,
            Lazy<ISsoApplicationService> ssoApplicationService,
            Lazy<IContentApplicationService> contentApplicationService,
            Lazy<IGuarantorApplicationService> guarantorApplicationService,
            Lazy<IDistributedCache> distributedCache
            )
            : base(
                baseControllerService,
                ssoApplicationService,
                contentApplicationService,
                guarantorApplicationService,
                distributedCache
                )
        {
            this._ssoApplicationService = ssoApplicationService;
        }

        const string OpenInitialHQYWidgetDialogKey = "OpenInitialHQYWidgetDialog";

        [System.Web.Mvc.HttpGet]
        public ActionResult Index()
        {
            return new HttpStatusCodeResult(HttpStatusCode.Forbidden);
        }

        [System.Web.Mvc.HttpGet]
        [RequireFeatureAny(VisitPayFeatureEnum.HealthEquityFeature, VisitPayFeatureEnum.MyChartSso)]
        [RequiresSsoAuthorization(Enabled = false)]
        public async Task<ActionResult> Terms()
        {
            // todo: provider is hardcoded
            //Could not clear the cookie...  then this wouldnt need to be hardcoded.
            //await this._patientCookie.Value.SetSsoResponseDtoAsync(context, null);
            SsoProviderDto provider = this.SsoApplicationService.Value.GetProvider(SsoProviderEnum.OpenEpic);

            IDictionary<string, string> additionalValues = new Dictionary<string, string>
            {
                {"[[SsoProviderDisplayName]]", provider.SsoProviderDisplayName}
            };

            CmsVersionDto descriptionVersion = await this.ContentApplicationService.Value.GetCurrentVersionAsync(CmsRegionEnum.SsoTermsDescription, true, additionalValues);
            CmsVersionDto termsVersion = await this.ContentApplicationService.Value.GetCurrentVersionAsync(provider.SsoTermsCmsRegion, true, additionalValues);


            return this.View(new SsoTermsViewModel
            {
                SsoProvider = (SsoProviderEnum)provider.SsoProviderId,
                Description = Mapper.Map<CmsViewModel>(descriptionVersion),
                Terms = Mapper.Map<CmsViewModel>(termsVersion)
            });
        }

        [System.Web.Mvc.HttpPost]
        [RequireFeatureAny(VisitPayFeatureEnum.HealthEquityFeature, VisitPayFeatureEnum.MyChartSso)]
        [RequiresSsoAuthorization(Enabled = false)]
        [EmulateUser(AllowPost = false)]
        [ValidateAntiForgeryToken]
        public async Task<PartialViewResult> Terms(SsoProviderEnum ssoProvider, bool accept)
        {
            if (!this.SsoApplicationService.Value.IsUserAuthorized(ssoProvider))
            {
                throw new HttpResponseException(HttpStatusCode.Unauthorized);
            }

            HttpContext context = System.Web.HttpContext.Current;
            SsoProviderDto provider = this.SsoApplicationService.Value.GetProvider(ssoProvider);

            IDictionary<string, string> additionalValues = new Dictionary<string, string>
            {
                {"[[SsoProviderDisplayName]]", provider.SsoProviderDisplayName}
            };

            SsoTermsActionViewModel model = new SsoTermsActionViewModel
            {
                SsoProvider = ssoProvider
            };

            if (accept)
            {
                this.AcceptSso(context, ssoProvider, this.CurrentUserId);
                model.Content = Mapper.Map<CmsViewModel>(await this.ContentApplicationService.Value.GetCurrentVersionAsync(CmsRegionEnum.SsoTermsAccepted, true, additionalValues));
                return this.PartialView("_TermsAcceptedModal", model);
            }

            this.MetricsProvider.Value.Increment(Metrics.Increment.Hqy.TouDeclined);
            this.SsoApplicationService.Value.DeclineSso(Mapper.Map<HttpContextDto>(context), ssoProvider, this.CurrentUserId, this.CurrentUserId);
            model.Content = Mapper.Map<CmsViewModel>(await this.ContentApplicationService.Value.GetCurrentVersionAsync(CmsRegionEnum.SsoTermsDeclined, true, additionalValues));
            return this.PartialView("_TermsDeclinedModal", model);
        }

        protected SsoVisitPayUserSettingDto AcceptSso(HttpContext context, SsoProviderEnum ssoProvider, int visitPayUserId)
        {
            this.MetricsProvider.Value.Increment(Metrics.Increment.Hqy.TouAccepted);
            return this.SsoApplicationService.Value.AcceptSso(Mapper.Map<HttpContextDto>(context), ssoProvider, visitPayUserId);
        }
        
        [System.Web.Mvc.HttpPost]
        [RequireFeatureAny(VisitPayFeatureEnum.HealthEquityFeature, VisitPayFeatureEnum.MyChartSso)]
        [RequiresSsoAuthorization(Enabled = false)]
        public JsonResult TermsContinue(SsoProviderEnum ssoProvider, bool? ignore)
        {
            if (this.SsoApplicationService.Value.IsUserAuthorized(ssoProvider))
            {
                if (ignore.HasValue && ignore.Value)
                {
                    this.SsoApplicationService.Value.IgnoreSso(ssoProvider, this.CurrentUserId, true);
                }

                return this.Json(this.Url.Action("Index", "Home"));
            }

            throw new HttpResponseException(HttpStatusCode.Unauthorized);
        }

        [System.Web.Mvc.HttpGet]
        [RequireFeatureAny(VisitPayFeatureEnum.HealthEquityFeature, VisitPayFeatureEnum.MyChartSso, VisitPayFeatureEnum.DemoHealthEquityIsEnabled)]
        public ActionResult Settings()
        {
            if (!this.IsFeatureEnabled(VisitPayFeatureEnum.HealthEquityFeature) &&
                !this.IsFeatureEnabled(VisitPayFeatureEnum.MyChartSso) &&
                this.IsFeatureEnabled(VisitPayFeatureEnum.DemoHealthEquityIsEnabled))
            {
                // all sso's disabled, and demo healthequity enabled
                RandomizedTestGroupDto testGroup = this.RandomizedTestApplicationService.Value.GetRandomizedTestGroupMembership(RandomizedTestEnum.FeatureDemoHealthEquity, this.CurrentGuarantorId.GetValueOrDefault(0), false);
                return this.View("~/Views/Demo/_HealthEquitySettings.cshtml", testGroup != null);
            }
            
            return this.View();
        }

        [System.Web.Mvc.HttpGet]
        [RequireFeatureAny(VisitPayFeatureEnum.HealthEquityFeature, VisitPayFeatureEnum.MyChartSso)]
        public JsonResult GetSettings()
        {
            IList<SsoProviderWithUserSettingsDto> dto =
                this.SsoApplicationService.Value.GetAllProvidersWithUserSettings(this.CurrentUserId)
                    .Where(x => this.SsoApplicationService.Value.IsUserAuthorized((SsoProviderEnum)x.Provider.SsoProviderId)).ToList();

            IList<SsoProviderWithUserSettingsViewModel> model = Mapper.Map<IList<SsoProviderWithUserSettingsViewModel>>(dto);

            foreach (SsoProviderWithUserSettingsViewModel setting in model)
            {
                setting.SsoProviderActionUrl = this.Url.Action("RedirectToOAuthAfterPing", new {provider = setting.SsoProviderEnum, area = ""});
                setting.Body = this.CmsView(setting, false);
                setting.Decline = this.CmsDeclineView(setting.SsoProviderName, setting.EnableFromManageSso);
            }

            return this.Json(model, JsonRequestBehavior.AllowGet);
        }

        [System.Web.Mvc.HttpPost]
        [RequireFeatureAny(VisitPayFeatureEnum.HealthEquityFeature, VisitPayFeatureEnum.MyChartSso)]
        [ValidateAntiForgeryToken]
        public JsonResult Decline(SsoProviderEnum ssoProvider)
        {
            if (!this.SsoApplicationService.Value.IsUserAuthorized(ssoProvider))
            {
                throw new HttpResponseException(HttpStatusCode.Unauthorized);
            }

            SsoVisitPayUserSettingDto dto = this.Decline(this.CurrentUserId, ssoProvider, this.CurrentUserId);
            SsoProviderWithUserSettingsViewModel model = Mapper.Map<SsoProviderWithUserSettingsViewModel>(dto);

            IDictionary<string, string> additionalValues = new Dictionary<string, string>
            {
                {"[[SsoProviderDisplayName]]", dto.SsoProvider.SsoProviderDisplayName}
            };

            model.Body = Mapper.Map<CmsViewModel>(this.ContentApplicationService.Value.GetCurrentVersionAsync(CmsRegionEnum.SsoGuarantorDeclined, true, additionalValues).Result);
            model.Decline = this.CmsDeclineView(model.SsoProviderName, model.EnableFromManageSso);

            return this.Json(model, JsonRequestBehavior.AllowGet);
        }

        [System.Web.Mvc.HttpPost]
        [RequireFeatureAny(VisitPayFeatureEnum.HealthEquityFeature, VisitPayFeatureEnum.MyChartSso)]
        [ValidateAntiForgeryToken]
        public JsonResult Ignore(SsoProviderEnum ssoProvider, bool isIgnored)
        {
            return this.Json(this.IgnoreInternal(ssoProvider, isIgnored), JsonRequestBehavior.AllowGet);
        }

        [System.Web.Mvc.NonAction]
        protected SsoProviderWithUserSettingsViewModel IgnoreInternal(SsoProviderEnum ssoProvider, bool isIgnored)
        {
            if (!SsoApplicationService.Value.IsUserAuthorized(ssoProvider))
            {
                throw new HttpResponseException(HttpStatusCode.Unauthorized);
            }

            SsoVisitPayUserSettingDto dto = this.Ignore(this.CurrentUserId, ssoProvider, isIgnored);
            SsoProviderWithUserSettingsViewModel model = Mapper.Map<SsoProviderWithUserSettingsViewModel>(dto);

            IDictionary<string, string> additionalValues = new Dictionary<string, string>
            {
                {"[[SsoProviderDisplayName]]", dto.SsoProvider.SsoProviderDisplayName}
            };

            model.Body = Mapper.Map<CmsViewModel>(this.ContentApplicationService.Value.GetCurrentVersionAsync(CmsRegionEnum.SsoGuarantorDeclined, true, additionalValues).Result);
            model.Decline = this.CmsDeclineView(model.SsoProviderName, model.EnableFromManageSso);

            return model;
        }

        [System.Web.Mvc.HttpPost]
        [RequireFeatureAny(VisitPayFeatureEnum.HealthEquityFeature, VisitPayFeatureEnum.MyChartSso)]
        [ValidateAntiForgeryToken]
        public void AcknowledgeInvalidatedSso(SsoProviderEnum ssoProvider)
        {
            if (!this.SsoApplicationService.Value.IsUserAuthorized(ssoProvider))
            {
                throw new HttpResponseException(HttpStatusCode.Unauthorized);
            }
            this.SsoApplicationService.Value.AcknowledgeInvalidatedSso(ssoProvider, this.CurrentUserId);
        }

        #region HealthEquity Specific

        [System.Web.Mvc.HttpPost]
        [RequireFeatureAny(VisitPayFeatureEnum.HealthEquityFeature)]
        [ValidateAntiForgeryToken]
        public void DismissHqyGettingStartedAlert()
        {
            SsoProviderEnum selectHealthEquitySsoProvider = this._ssoApplicationService.Value.SelectHealthEquitySsoProvider();
            this.SsoApplicationService.Value.IgnoreSso(selectHealthEquitySsoProvider, this.CurrentUserId, true);
        }

        [System.Web.Mvc.HttpGet]
        [EmulateUser(AllowGet = false)]
        [GuarantorAuthorize(Roles = VisitPayRoleStrings.System.Patient)]
        [Common.Web.Attributes.SessionState(SessionStateBehavior.Required)]
        public ActionResult RedirectToOpenDialog(SsoProviderEnum provider, string redirectUrl)
        {
            SsoStatusEnum? result = this.SsoApplicationService.Value.GetUserSsoStatus(provider, this.CurrentUserId);
            if (result.HasValue && result.Value == SsoStatusEnum.Accepted)
            {
                this.SetTempData(OpenInitialHQYWidgetDialogKey, true);
            }
            return this.Redirect(redirectUrl);
        }
        
        [System.Web.Mvc.HttpGet]
        [EmulateUser(AllowGet = false)]
        [GuarantorAuthorize(Roles = VisitPayRoleStrings.System.Patient)]
        public async Task<ActionResult> RedirectToOAuthAfterPing(SsoProviderEnum? provider)
        {
            //this is because HQY returns: https://www-test-providence.visitpaytest.com/Sso/RedirectToOAuthAfterPing?provider=HealthEquityOAuthGrant?error=access_denied&state=SOMESTATE
            //Notice the 2 question marks?  Yah....
            if (!provider.HasValue)
            {
                return this.View(new RedirectToOAuthViewModel() {ProviderEnum = SsoProviderEnum.Unknown, SsoStatusEnum = SsoStatusEnum.Rejected});
            }

            SsoStatusEnum? ssoStatusEnum = this.SsoApplicationService.Value.GetUserSsoStatus(provider.Value, this.CurrentUserId);

            SsoProviderDto providerDto = await this.SsoApplicationService.Value.GetActiveProviderAsync(provider.Value);
            if (providerDto != null && (!ssoStatusEnum.HasValue || ssoStatusEnum.HasValue && ssoStatusEnum.Value != SsoStatusEnum.Accepted))
            {
                this.SsoApplicationService.Value.SetupOAuth(provider.Value, this.CurrentUserId, this.Request, this.Response, this.HttpContext);
            }
            ssoStatusEnum = this.SsoApplicationService.Value.GetUserSsoStatus(provider.Value, this.CurrentUserId);

            //If HQY says the user doesn't qualify for SSO, dismiss the HQY widget
            if (ssoStatusEnum.HasValue && providerDto != null && ssoStatusEnum.Value != SsoStatusEnum.Accepted)
            {
                this.Ignore(this.CurrentUserId, providerDto.SsoProviderEnum, true);
            }

            RedirectToOAuthViewModel model = new RedirectToOAuthViewModel()
            {
                ProviderEnum = providerDto?.SsoProviderEnum,
                SsoStatusEnum = ssoStatusEnum
            };
            return this.View(model);
        }

        [System.Web.Mvc.HttpGet]
        [EmulateUser(AllowGet = false)]
        [GuarantorAuthorize(Roles = VisitPayRoleStrings.System.Patient)]
        public async Task<ActionResult> SsoRedirect(SsoProviderEnum target)
        {
            HttpContext context = System.Web.HttpContext.Current;

            SsoCanRedirectToSsoResult result = await this.SsoApplicationService.Value.RedirectToSso(target, this.CurrentUserId, Mapper.Map<HttpContextDto>(context));

            return this.View("SsoRedirect", result);
        }

        [System.Web.Mvc.HttpGet]
        [RequireFeature(VisitPayFeatureEnum.HealthEquityShowBalance)]
        [EmulateUser(AllowGet = false)]
        public async Task<JsonResult> HealthEquityBalance()
        {
            if (!this.SsoApplicationService.Value.IsUserAuthorized(SsoProviderEnum.HealthEquityOutbound))
            {
                throw new HttpResponseException(HttpStatusCode.Unauthorized);
            }
            //Assuming we're going to need some logging around this?
            int guarantorId = this.GuarantorApplicationService.Value.GetGuarantorId(this.CurrentUserId);
            SsoProviderEnum selectHealthEquitySsoProvider = this.SsoApplicationService.Value.SelectHealthEquitySsoProvider();
            HealthEquityBalanceDto result = await this.GuarantorApplicationService.Value.GetGuarantorHealthEquityBalanceAsnyc(guarantorId, this.CurrentUserId, selectHealthEquitySsoProvider);
            return this.Json(
                new
                {
                    HsaCashBalance = result?.CashBalance.FormatCurrency(),
                    HsaInvestmentBalance = result?.InvestmentBalance.FormatCurrency(),
                    HsaTotalBalance = result?.TotalBalance.FormatCurrency(),
                    HsaContributionsYtd = result?.ContributionsYtd.FormatCurrency(),
                    HsaDistributionsYtd = result?.DistributionsYtd.FormatCurrency(),
                    HsaDate = result?.LastUpdated.ToString(Format.TimeFormat)
                }, JsonRequestBehavior.AllowGet);
        }

        [System.Web.Mvc.HttpGet]
        [RequireFeature(VisitPayFeatureEnum.HealthEquityShowBalance)]
        [EmulateUser(AllowGet = false)]
        public async Task<JsonResult> HealthEquitySsoStatus()
        {
            if (!this.SsoApplicationService.Value.IsUserAuthorized(SsoProviderEnum.HealthEquityOutbound))
            {
                throw new HttpResponseException(HttpStatusCode.Unauthorized);
            }

            int guarantorId = this.GuarantorApplicationService.Value.GetGuarantorId(this.CurrentUserId);
            HealthEquitySsoStatusDto result = await this.GuarantorApplicationService.Value.GetGuarantorHealthEquitySsoStatusAsnyc(guarantorId, this.CurrentUserId);
            return this.Json(
                new
                {
                    IsHqySsoUp = result?.IsHqySsoUp,
                    LastUpdated = result?.LastUpdated.ToString(Format.TimeFormat)
                }, JsonRequestBehavior.AllowGet);
        }

        protected HealthEquityWidgetViewModel HealthEquityWidgetViewModel()
        {
            SsoProviderEnum selectHealthEquitySsoProvider = this.SsoApplicationService.Value.SelectHealthEquitySsoProvider();

            HealthEquityWidgetViewModel model = new HealthEquityWidgetViewModel
            {
                ShowBalanceSetting = this.ClientDto.HealthEquityShowBalance,
                ShowLinkToHqySetting = this.ClientDto.HealthEquityShowOutboundSso,
                ShowSingleSignOnConnectionsSetting = this.ClientDto.HealthEquityShowOutboundSso,
                FeatureEnabled = this.ClientDto.HealthEquityEnabled,
                IsUserEmulating = this.IsCurrentUserEmulating(),
                SsoProviderEnum = selectHealthEquitySsoProvider,
                UrlToAccept = this.Url.Action("Terms", "Sso"),
                UrlToReject = this.Url.Action("Terms", "Sso"),
                SsoOutboundAgreed = this.SsoApplicationService.Value.HasUserAcceptedSsoTerms(selectHealthEquitySsoProvider, this.CurrentUserId),
                IgnoredSso = this.SsoApplicationService.Value.HasUserIgnoredSso(selectHealthEquitySsoProvider, this.CurrentUserId),
                IsUserAuthorizedToSee = this.SsoApplicationService.Value.IsUserAuthorized(selectHealthEquitySsoProvider),
                ShouldOpenInitialHqyWidgetDialog = this.TempData.ContainsKey(OpenInitialHQYWidgetDialogKey) && (bool)this.TempData[OpenInitialHQYWidgetDialogKey]
            };

            return model;
        }
        
        [System.Web.Mvc.HttpGet]
        public ActionResult HealthEquityWidgetPartial()
        {
            if (this.ClientDto.HealthEquityEnabled)
            {
                HealthEquityWidgetViewModel model = this.HealthEquityWidgetViewModel();
                if (model.Visible && model.SsoOutboundAgreed && model.ShowBalanceSetting)
                {
                    this.MetricsProvider.Value.Increment(Metrics.Increment.Hqy.WidgetShown);
                }

                return this.PartialView("_HealthEquityWidget", model);
            }

            return new EmptyResult();
        }

        [System.Web.Mvc.HttpGet]
        [EmulateUser(AllowGet = false)]
        [RequireFeature(VisitPayFeatureEnum.HealthEquityShowBalance)]
        [GuarantorAuthorize(Roles = VisitPayRoleStrings.System.Patient)]
        public PartialViewResult HealthEquityBalanceWidgetPartial()
        {
            return this.PartialView("_HealthEquityBalanceWidget", this.HealthEquityWidgetViewModel());
        }

        [System.Web.Mvc.HttpPost]
        public PartialViewResult HealthEquityNoSsoDialogPartial()
        {
            HealthEquityWidgetViewModel model = this.HealthEquityWidgetViewModel();
            return this.PartialView("_HealthEquityNoSsoDialog", model);
        }

        [System.Web.Mvc.HttpPost]
        public PartialViewResult HealthEquitySsoDialogPartial()
        {
            HealthEquityWidgetViewModel model = this.HealthEquityWidgetViewModel();
            return this.PartialView("_HealthEquitySsoDialog", model);
        }
        [System.Web.Mvc.HttpPost]
        public PartialViewResult HealthEquitySsoAgreeToTermsDialogPartial()
        {
            HealthEquityWidgetViewModel model = this.HealthEquityWidgetViewModel();
            return this.PartialView("_HealthEquityAgreementSsoDialog", model);
        }

        [System.Web.Mvc.HttpPost]
        public PartialViewResult HealthEquitySsoPreAgreeToTermsDialogPartial()
        {
            HealthEquityWidgetViewModel model = this.HealthEquityWidgetViewModel();
            return this.PartialView("_HealthEquityPreAgreementSsoDialog", model);
        }

        [System.Web.Mvc.HttpPost]
        public PartialViewResult HealthEquitySsoSetupFailedDialogPartial()
        {

            HealthEquityWidgetViewModel model = this.HealthEquityWidgetViewModel();
            return this.PartialView("_HealthEquitySsoSetupFailedDialog", model);
        }

        [System.Web.Mvc.HttpPost]
        public PartialViewResult HealthEquitySsoSetupSuccessDialogPartial()
        {
            SsoStatusEnum? ssoStatusEnum = this.SsoApplicationService.Value.GetUserSsoStatus(SsoProviderEnum.HealthEquityOutbound, this.CurrentUserId);
            HealthEquityWidgetViewModel model = this.HealthEquityWidgetViewModel();
            return this.PartialView(ssoStatusEnum.HasValue && ssoStatusEnum.Value == SsoStatusEnum.Accepted ? "_HealthEquitySsoSetupSuccessDialog" : "_HealthEquitySsoSetupInvalidDialog", model);
        }
        
        [System.Web.Mvc.HttpGet]
        public PartialViewResult HealthEquityDontShowDialogPartial()
        {
            return this.PartialView("_HealthEquityDontShowWidgetSsoDialog");
        }

        [System.Web.Mvc.HttpGet]
        public PartialViewResult HealthEquitySsoSetup()
        {
            HealthEquityWidgetViewModel model = this.HealthEquityWidgetViewModel();
            if (model.Visible && model.SsoOutboundAgreed && model.ShowBalanceSetting)
            {
                this.MetricsProvider.Value.Increment(Metrics.Increment.Hqy.WidgetShown);
            }
            return this.PartialView("_HealthEquitySsoSetup", model);
        }

        #endregion
    }
}
﻿namespace Ivh.Web.Patient.Controllers.Base
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using System.Web;
    using Application.Content.Common.Dtos;
    using Application.Content.Common.Interfaces;
    using Application.Core.Common.Interfaces;
    using Application.FinanceManagement.Common.Dtos;
    using Application.FinanceManagement.Common.Interfaces;
    using Application.User.Common.Dtos;
    using AutoMapper;
    using Common.EventJournal;
    using Common.VisitPay.Enums;
    using Common.VisitPay.Strings;
    using Common.Web.Interfaces;
    using Common.Web.Models.Consolidate;
    using Common.Web.Models.Shared;
    using Models.Consolidate;
    using Services;

    public abstract class BaseConsolidateController : BaseController
    {
        protected readonly Lazy<IConsolidationGuarantorApplicationService> ConsolidationGuarantorApplicationService;
        protected readonly Lazy<IContentApplicationService> ContentApplicationService;
        protected readonly Lazy<IFinancePlanApplicationService> FinancePlanApplicationService;
        protected readonly Lazy<IManagedUserApplicationService> ManagedUserApplicationService;
        protected readonly Lazy<IManagingUserApplicationService> ManagingUserApplicationService;
        protected readonly Lazy<IGuarantorApplicationService> GuarantorApplicationService;
        protected readonly Lazy<IVisitApplicationService> _visitService;

        protected BaseConsolidateController(
            Lazy<IBaseControllerService> baseControllerService,
            Lazy<IContentApplicationService> contentApplicationService,
            Lazy<GuarantorFilterService> guarantorFilterService,
            Lazy<IConsolidationGuarantorApplicationService> consolidationGuarantorApplicationService,
            Lazy<IFinancePlanApplicationService> financePlanApplicationService,
            Lazy<IManagedUserApplicationService> managedUserApplicationService,
            Lazy<IManagingUserApplicationService> managingUserApplicationService,
            Lazy<IVisitPayUserJournalEventApplicationService> visitPayUserJournalEventApplicationService,
            Lazy<IGuarantorApplicationService> guarantorApplicationService,
            Lazy<IVisitApplicationService> visitService)
            : base(baseControllerService, guarantorFilterService)
        {
            this.ContentApplicationService = contentApplicationService;
            this.ConsolidationGuarantorApplicationService = consolidationGuarantorApplicationService;
            this.FinancePlanApplicationService = financePlanApplicationService;
            this.ManagedUserApplicationService = managedUserApplicationService;
            this.ManagingUserApplicationService = managingUserApplicationService;
            this.GuarantorApplicationService = guarantorApplicationService;
            this._visitService = visitService;

        }

        protected RequestManagedViewModel GetRequestManagedModel()
        {
            return new RequestManagedViewModel
            {
                TextCms = Mapper.Map<CmsViewModel>(this.ContentApplicationService.Value.GetCurrentVersionAsync(CmsRegionEnum.ConsolidationRequestToBeManaged).Result),
                TermsCms = Mapper.Map<CmsViewModel>(this.ContentApplicationService.Value.GetCurrentVersionAsync(CmsRegionEnum.ConsolidationManagedAcceptTerms).Result),
                HipaaCms = Mapper.Map<CmsViewModel>(this.ContentApplicationService.Value.GetCurrentVersionAsync(CmsRegionEnum.ConsolidationHipaaReleaseAuthorization).Result)
            };
        }


        protected RequestManagingViewModel GetRequestManagingModel()
        {
            return new RequestManagingViewModel
            {
                TextCms = Mapper.Map<CmsViewModel>(this.ContentApplicationService.Value.GetCurrentVersionAsync(CmsRegionEnum.ConsolidationRequestToBeManaging).Result),
                TermsCms = Mapper.Map<CmsViewModel>(this.ContentApplicationService.Value.GetCurrentVersionAsync(CmsRegionEnum.ConsolidationManagingAcceptTerms).Result),
            };
        }

        protected ManageHouseholdViewModel GetManageHouseholdModel(int visitPayUserId)
        {
            IList<ConsolidationGuarantorDto> managedGuarantorsDto = this.ConsolidationGuarantorApplicationService.Value.GetAllManagedGuarantors(visitPayUserId).ToList();
            IList<ConsolidationGuarantorDto> managingGuarantorsDto = this.ConsolidationGuarantorApplicationService.Value.GetAllManagingGuarantors(visitPayUserId).ToList();

            IList<ConsolidationGuarantorViewModel> managedGuarantors = new List<ConsolidationGuarantorViewModel>();
            IList<ConsolidationGuarantorViewModel> managingGuarantors = new List<ConsolidationGuarantorViewModel>();

            IDictionary<int, Guid> obfuscatedIds = this.SessionFacade.Value.ConsolidationGuarantorObfuscator.Obfuscate(
                managedGuarantorsDto.Select(x => x.ConsolidationGuarantorId).Concat(managingGuarantorsDto.Select(x => x.ConsolidationGuarantorId)).ToList());

            foreach (ConsolidationGuarantorDto consolidationGuarantorDto in managedGuarantorsDto)
            {
                ConsolidationGuarantorViewModel vm = new ConsolidationGuarantorViewModel(this.ClientDto);
                Mapper.Map(consolidationGuarantorDto, vm);
                vm.ConsolidationGuarantorId = obfuscatedIds[consolidationGuarantorDto.ConsolidationGuarantorId];
                bool initiatedByCurrentUser = consolidationGuarantorDto.InitiatedByUser.VisitPayUserId == visitPayUserId;

                vm.CanCancelConsolidation = consolidationGuarantorDto.ConsolidationGuarantorStatus == ConsolidationGuarantorStatusEnum.Accepted;
                vm.InitiatedByCurrentUser = initiatedByCurrentUser;
                vm.IsManagingUser = visitPayUserId == consolidationGuarantorDto.ManagingGuarantor.User.VisitPayUserId;

                vm.HasActiveFinancePlans = this.FinancePlanApplicationService.Value.GetActiveFinancePlanCount(consolidationGuarantorDto.ManagedGuarantor.User.VisitPayUserId, consolidationGuarantorDto.ManagedGuarantor.VpGuarantorId) > 0;

                if (consolidationGuarantorDto.IsPending)
                {
                    bool isPendingFinancePlan = consolidationGuarantorDto.ConsolidationGuarantorStatus == ConsolidationGuarantorStatusEnum.PendingFinancePlan;

                    vm.CanCancelPending = consolidationGuarantorDto.InitiatedByUser.VisitPayUserId == visitPayUserId || isPendingFinancePlan;
                    vm.CanAccept = !initiatedByCurrentUser && !isPendingFinancePlan;
                    vm.CanAcceptFinancePlan = isPendingFinancePlan;
                    vm.CanCancelPending = initiatedByCurrentUser;
                    vm.CanDecline = !initiatedByCurrentUser;
                }

                managedGuarantors.Add(vm);
            }

            foreach (ConsolidationGuarantorDto consolidationGuarantorDto in managingGuarantorsDto)
            {
                ConsolidationGuarantorViewModel vm = new ConsolidationGuarantorViewModel(this.ClientDto);
                Mapper.Map(consolidationGuarantorDto, vm);
                vm.ConsolidationGuarantorId = obfuscatedIds[consolidationGuarantorDto.ConsolidationGuarantorId];
                bool initiatedByCurrentUser = consolidationGuarantorDto.InitiatedByUser.VisitPayUserId == visitPayUserId;

                vm.CanCancelConsolidation = consolidationGuarantorDto.ConsolidationGuarantorStatus == ConsolidationGuarantorStatusEnum.Accepted;
                vm.InitiatedByCurrentUser = initiatedByCurrentUser;
                vm.IsManagingUser = visitPayUserId == consolidationGuarantorDto.ManagingGuarantor.User.VisitPayUserId;

                vm.HasActiveFinancePlans = this.FinancePlanApplicationService.Value.GetActiveFinancePlanCount(consolidationGuarantorDto.ManagedGuarantor.User.VisitPayUserId, consolidationGuarantorDto.ManagedGuarantor.VpGuarantorId) > 0;

                if (consolidationGuarantorDto.IsPending)
                {
                    bool isPendingFinancePlan = consolidationGuarantorDto.ConsolidationGuarantorStatus == ConsolidationGuarantorStatusEnum.PendingFinancePlan;

                    vm.CanAccept = !initiatedByCurrentUser && !isPendingFinancePlan;
                    vm.CanAcceptFinancePlan = false;
                    vm.CanCancelPending = initiatedByCurrentUser;
                    vm.CanDecline = !initiatedByCurrentUser;
                }

                managingGuarantors.Add(vm);
            }

            // obfuscate guarantors in case any changes have been made
            this.GuarantorFilterService.Value.ObfuscateGuarantors(managedGuarantorsDto.Select(x => x.ManagedGuarantor).ToList());

            return new ManageHouseholdViewModel
            {
                ManagedGuarantors = managedGuarantors,
                ManagingGuarantors = managingGuarantors
            };
        }

        protected AcceptTermsViewModel GetAcceptTermsModel(Guid id, int requestType)
        {
            ConsolidationMatchRequestTypeEnum requestTypeEnum = (ConsolidationMatchRequestTypeEnum)requestType;

            AcceptTermsViewModel model = new AcceptTermsViewModel
            {
                ConsolidationGuarantorId = id,
                RequestType = (ConsolidationMatchRequestTypeEnum)requestType
            };

            if (requestTypeEnum == ConsolidationMatchRequestTypeEnum.RequestToBeManaging)
            {
                return model;
            }

            model.TextCms = Mapper.Map<CmsViewModel>(this.ContentApplicationService.Value.GetCurrentVersionAsync(CmsRegionEnum.ConsolidationManagedAccept).Result);
            model.TermsCms = Mapper.Map<CmsViewModel>(this.ContentApplicationService.Value.GetCurrentVersionAsync(CmsRegionEnum.ConsolidationManagedAcceptTerms).Result);
            model.HipaaCms = Mapper.Map<CmsViewModel>(this.ContentApplicationService.Value.GetCurrentVersionAsync(CmsRegionEnum.ConsolidationHipaaReleaseAuthorization).Result);

            return model;
        }

        protected async Task<AcceptTermsFpViewModel> GetAcceptTermsFpModel(Guid id, int requestType)
        {
            int consolidationGuarantorIdClear = this.SessionFacade.Value.ConsolidationGuarantorObfuscator.Clarify(id);

            ConsolidationGuarantorDto consolidationGuarantorDto = await this.ConsolidationGuarantorApplicationService.Value.GetConsolidationGuarantorAsync(consolidationGuarantorIdClear, this.CurrentUserId).ConfigureAwait(false);
            IList<FinancePlanDto> consolidationGuarantorFinancePlanDtos = await this.ManagingUserApplicationService.Value.GetConsolidationFinancePlansAsync(consolidationGuarantorIdClear, this.CurrentUserId).ConfigureAwait(false);

            CmsViewModel cmsViewModel = null;
            if ((ConsolidationMatchRequestTypeEnum) requestType == ConsolidationMatchRequestTypeEnum.RequestToBeManaging)
            {
                // if the user has been requested by another to be managed, he or she (mangager) has not been presented with the consolidation terms yet
                cmsViewModel = Mapper.Map<CmsViewModel>(this.ContentApplicationService.Value.GetCurrentVersionAsync(CmsRegionEnum.ConsolidationManagingAcceptTerms).Result);
            }

            AcceptTermsFpViewModel model = new AcceptTermsFpViewModel
            {
                CmsVersionText = "",
                ConsolidationGuarantorId = id,
                FinancePlans = Mapper.Map<List<ConsolidationGuarantorFinancePlanViewModel>>(consolidationGuarantorFinancePlanDtos),
                InitiatedByManaging = consolidationGuarantorDto.InitiatedByUser.VisitPayUserId == consolidationGuarantorDto.ManagingGuarantor.User.VisitPayUserId,
                RequestType = (ConsolidationMatchRequestTypeEnum)requestType,
                ConsolidationAcceptTerms = cmsViewModel
            };

            CmsRegionEnum cmsRegionEnum = this._visitService.Value.GetRicCmsRegionEnum(this.GetVpGuarantorId(this.CurrentUserId));

            IDictionary<int, int> cmsVersionNumbers = this.ContentApplicationService.Value.GetCmsVersionNumbersAsync(cmsRegionEnum).Result;
            foreach (ConsolidationGuarantorFinancePlanViewModel financePlan in model.FinancePlans)
            {
                int financePlanCreditAgreementVersionNum;
                cmsVersionNumbers.TryGetValue(financePlan.CreditAgreementCmsVersionId, out financePlanCreditAgreementVersionNum);

                financePlan.CreditAgreementVersionNum = financePlanCreditAgreementVersionNum;
            }

            if ((ConsolidationMatchRequestTypeEnum)requestType == ConsolidationMatchRequestTypeEnum.RequestToBeManaging)
            {
                IDictionary<string, string> additionalValues = new Dictionary<string, string>();
                additionalValues.Add("[[ExpiryDate]]", consolidationGuarantorDto.InsertDate.AddDays(this.ClientDto.ConsolidationExpirationDays).ToString(Format.DateFormat));
                additionalValues.Add("[[expiry date]]", consolidationGuarantorDto.InsertDate.AddDays(this.ClientDto.ConsolidationExpirationDays).ToString(Format.DateFormat));

                if (model.InitiatedByManaging)
                {
                    model.CmsVersionText = (await this.ContentApplicationService.Value.GetCurrentVersionAsync(CmsRegionEnum.ConsolidationManagingAcceptTermsFpInitiatedByManaging, additionalValues: additionalValues)).ContentBody;
                }
                else
                {
                    model.CmsVersionText = (await this.ContentApplicationService.Value.GetCurrentVersionAsync(CmsRegionEnum.ConsolidationManagingAcceptTermsFp, additionalValues: additionalValues)).ContentBody;
                }
            }

            return model;
        }

        protected DeconsolidateFpViewModel GetDeconsolidateFpViewModel(Guid id, ConsolidationMatchRequestTypeEnum requestType)
        {
            if (requestType != ConsolidationMatchRequestTypeEnum.RequestToBeManaged)
                return null;

            VisitPayUserDto managingUser = this.GetManagingUser();
            IDictionary<string, string> additionalValues = new Dictionary<string, string>();
            additionalValues.Add("[[ManagingUser]]", managingUser.DisplayFirstNameLastName);

            CmsVersionDto cancelCmsVersion = this.ContentApplicationService.Value.GetCurrentVersionAsync(CmsRegionEnum.ConsolidationManagedUserCancelConsolidation, additionalValues: additionalValues).Result;

            return new DeconsolidateFpViewModel
            {
                CancelCmsVersionText = cancelCmsVersion.ContentBody,
                ConsolidationGuarantorId = id,
                RequestType = requestType
            };
        }
        
        protected async Task DoDeclineConsolidation(Guid consolidationGuarantorId, ConsolidationMatchRequestTypeEnum requestType)
        {
            HttpContext context = System.Web.HttpContext.Current;
            
            int consolidationGuarantorIdClear = this.SessionFacade.Value.ConsolidationGuarantorObfuscator.Clarify(consolidationGuarantorId);

            switch (requestType)
            {
                case ConsolidationMatchRequestTypeEnum.RequestToBeManaging:
                    await this.ManagingUserApplicationService.Value.RejectConsolidationAsync(consolidationGuarantorIdClear, this.CurrentUserId).ConfigureAwait(false);
                    break;
                case ConsolidationMatchRequestTypeEnum.RequestToBeManaged:
                    await this.ManagedUserApplicationService.Value.RejectConsolidationAsync(consolidationGuarantorIdClear, this.CurrentUserId).ConfigureAwait(false);
                    break;
            }

            int vpGuarantorId = this.GetVpGuarantorId(this.CurrentUserId);
            this.VisitPayUserJournalEventApplicationService.Value.LogGuarantorConsolidationRequestRejected(this.CurrentUserId, vpGuarantorId, consolidationGuarantorIdClear, Mapper.Map<JournalEventHttpContextDto>(context));
        }

        protected VisitPayUserDto GetManagingUser()
        {
            List<ConsolidationGuarantorDto> managingGuarantors = this.ConsolidationGuarantorApplicationService.Value.GetAllManagingGuarantors(this.CurrentUserId).ToList();
            return managingGuarantors.OrderByDescending(x => x.InsertDate).First().ManagingGuarantor.User;
        }

        protected VisitPayUserDto GetManagedUser(Guid obfuscatedConsolidationGuarantorId)
        {
            int consolidationGuarantorId = this.SessionFacade.Value.ConsolidationGuarantorObfuscator.Clarify(obfuscatedConsolidationGuarantorId);

            List<ConsolidationGuarantorDto> managedGuarantors = this.ConsolidationGuarantorApplicationService.Value.GetAllManagedGuarantors(this.CurrentUserId).ToList();
            return managedGuarantors.Where(x => x.ConsolidationGuarantorId == consolidationGuarantorId).OrderByDescending(x => x.InsertDate).First().ManagedGuarantor.User;
        }
    }
}
﻿namespace Ivh.Web.Patient.Controllers.Base
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using System.Web;
    using System.Web.Helpers;
    using System.Web.Mvc;
    using System.Web.Routing;
    using Application.Content.Common.Dtos;
    using Application.Content.Common.Interfaces;
    using Application.Core.Common.Dtos;
    using Application.Core.Common.Interfaces;
    using Application.Email.Common.Dtos;
    using Application.Email.Common.Interfaces;
    using Application.FinanceManagement.Common.Dtos;
    using Application.FinanceManagement.Common.Interfaces;
    using Application.User.Common.Dtos;
    using Attributes;
    using AutoMapper;
    using Common.Base.Constants;
    using Common.Cache;
    using Common.Web.Cookies;
    using Common.Web.Interfaces;
    using Common.Web.Models;
    using Common.Web.Models.Account;
    using Common.Web.Models.Alerts;
    using Common.Web.Models.Guarantor;
    using Common.Web.Models.Shared;
    using Domain.Logging.Interfaces;
    using Microsoft.AspNet.Identity;
    using Microsoft.Owin.Security;
    using Models;
    using Models.Account;
    using Services;
    using SessionFacade;
    using Common.Base.Utilities.Extensions;
    using Common.EventJournal;
    using Common.VisitPay.Constants;
    using Common.VisitPay.Enums;
    using Common.VisitPay.Strings;
    using Common.Web.Constants;
    using Common.Web.Extensions;
    using Common.Web.Filters;
    using Common.Web.Models.Statement;
    using Newtonsoft.Json;
    using Common.Web.Utilities;
    using Ivh.Application.Core.Common.Dtos.Eob;
    using System.Web.SessionState;

    public abstract class BaseAccountController : BaseController
    {
        protected readonly Lazy<IDistributedCache> Cache;
        protected readonly Lazy<ICommunicationApplicationService> CommunicationApplicationService;
        protected readonly Lazy<IContentApplicationService> ContentApplicationService;
        protected readonly Lazy<IWebCookieFacade> WebCookieFacade;
        protected readonly Lazy<IAlertingApplicationService> AlertingApplicationService;
        protected readonly Lazy<IFinancePlanApplicationService> FinancePlanApplicationService;
        protected readonly Lazy<IStatementApplicationService> StatementApplicationService;
        protected readonly Lazy<IVisitApplicationService> VisitApplicationService;
        protected readonly Lazy<IVisitPayUserApplicationService> VisitPayUserApplicationService;
        protected readonly Lazy<IWebPatientSessionFacade> WebPatientSessionFacade;
        protected readonly Lazy<ISsoApplicationService> SsoApplicationService;
        protected readonly Lazy<ILocalizationCookieFacade> LocalizationCookieFacade;

        protected BaseAccountController(
            Lazy<IBaseControllerService> baseControllerService,
            Lazy<IAlertingApplicationService> alertingApplicationService,
            Lazy<IFinancePlanApplicationService> financePlanApplicationService,
            Lazy<IStatementApplicationService> statementApplicationService,
            Lazy<IVisitApplicationService> visitApplicationService,
            Lazy<IVisitPayUserApplicationService> visitPayUserApplicationService,
            Lazy<GuarantorFilterService> guarantorFilterService,
            Lazy<IWebCookieFacade> webCookieFacade,
            Lazy<ICommunicationApplicationService> communicationApplicationService,
            Lazy<IContentApplicationService> contentApplicationService,
            Lazy<IDistributedCache> cache,
            Lazy<IWebPatientSessionFacade> webPatientSessionFacade,
            Lazy<ISsoApplicationService> ssoApplicationService,
            Lazy<ILocalizationCookieFacade> localizationCookieFacade
        )
            : base(baseControllerService, guarantorFilterService)
        {
            this.AlertingApplicationService = alertingApplicationService;
            this.FinancePlanApplicationService = financePlanApplicationService;
            this.StatementApplicationService = statementApplicationService;
            this.VisitApplicationService = visitApplicationService;
            this.VisitPayUserApplicationService = visitPayUserApplicationService;
            this.CommunicationApplicationService = communicationApplicationService;
            this.ContentApplicationService = contentApplicationService;
            this.WebCookieFacade = webCookieFacade;
            this.Cache = cache;
            this.WebPatientSessionFacade = webPatientSessionFacade;
            this.SsoApplicationService = ssoApplicationService;
            this.LocalizationCookieFacade = localizationCookieFacade;
        }

        protected string AccountClosedMessage => LocalizationHelper.GetLocalizedString(TextRegionConstants.GuarantorCanceledAccount);

        protected IAuthenticationManager AuthenticationManager => this.HttpContext.GetOwinContext().Authentication;

        private async Task<SsoResponseDto> GetRegisteredMatchesSsoResponseDtoAsync(HttpContext context)
        {
            SsoResponseDto ssoResponseDto = await this.WebCookieFacade.Value.GetSsoResponseDtoAsync(context);
            return ssoResponseDto != null && ssoResponseDto.FoundRegisteredMatches ? ssoResponseDto : null;
        }

        #region login, password, username

        [AllowAnonymous]
        [HttpGet]
        [Common.Web.Attributes.SessionState(SessionStateBehavior.Required)]
        public virtual async Task<ActionResult> Login(string returnUrl, int? sm)
        {
            HttpContext context = System.Web.HttpContext.Current;

            if (this.User.Identity.IsAuthenticated)
            {
                return this.ClearExistingSession(returnUrl);
            }

            LoginViewModel model = new LoginViewModel
            {
                IsSso = await this.GetRegisteredMatchesSsoResponseDtoAsync(context) != null,
                //SsoOneTimePaymentMessage = (await this.ContentApplicationService.Value.GetCurrentVersionAsync(CmsRegionEnum.SsoOneTimePaymentMessage)).ContentBody,
                ReturnUrl = returnUrl
            };

            if (sm.HasValue)
            {
                this.ModelState.AddModelError("", "A session error has occurred, please try again.");
            }

            return this.ReturnLoginView(model);
        }

        [AllowAnonymous]
        [HttpPost]
        [Common.Web.Attributes.SessionState(SessionStateBehavior.Required)]
        public async Task<ActionResult> Login(LoginViewModel model)
        {
            HttpContext context = System.Web.HttpContext.Current;
            if (this.User.Identity.IsAuthenticated)
            {
                return this.ClearExistingSession(model.ReturnUrl, true);
            }

            AntiForgery.Validate();
            if (!this.ModelState.IsValid)
            {
                return this.ReturnLoginView(model);
            }

            SsoResponseDto ssoResponseDto = await this.GetRegisteredMatchesSsoResponseDtoAsync(context);
            SsoPasswordSignInRequest request = new SsoPasswordSignInRequest
            {
                UserName = model.Username,
                Password = model.Password,
                SsoResponse = ssoResponseDto
            };

            SignInStatusExEnum result = await this.SsoApplicationService.Value.PasswordSignInAsync(Mapper.Map<HttpContextDto>(context), request);
            await this.BruteForceDelayAsync(context.Request.UserHostAddress, () => result == SignInStatusExEnum.Failure, () => result == SignInStatusExEnum.Success);
            switch (result)
            {
                case SignInStatusExEnum.Success:
                    return this.RedirectToLocal(model.ReturnUrl);

                case SignInStatusExEnum.LockedOut:
                    VisitPayUserDto visitPayUserDto = this.VisitPayUserApplicationService.Value.FindByName(model.Username);
                    return this.View("Lockout", visitPayUserDto.IsDisabled);

                case SignInStatusExEnum.RequiresVerification:
                    this.SetTempData(TempDataKeys.Username, model.Username);
                    return this.RedirectToAction("ResetPassword", new { p = model.Password });

                case SignInStatusExEnum.PasswordExpired:
                    this.SetTempData(TempDataKeys.Username, model.Username);
                    this.SetTempData(TempDataKeys.Password, model.Password);
                    return this.RedirectToAction("PasswordExpired");

                case SignInStatusExEnum.AccountClosed:
                    this.ModelState.AddModelError("", this.AccountClosedMessage);
                    model.IsSso = ssoResponseDto != null;
                    return this.ReturnLoginView(model);

                case SignInStatusExEnum.Failure:
                default:
                    this.ModelState.AddModelError("", LocalizationHelper.GetLocalizedString(TextRegionConstants.UsernameOrPasswordIncorrect));
                    model.IsSso = ssoResponseDto != null;
                    return this.ReturnLoginView(model);
            }
        }

        private ActionResult ReturnLoginView(LoginViewModel model)
        {
            if (this.IsFeatureEnabled(VisitPayFeatureEnum.AlternatePublicPages))
            {
                return this.View("LoginAlt", model);
            }

            return this.View(model);
        }

        [AllowAnonymous]
        [RequiresAcknowledgement(Enabled = false)]
        [RequiresSsoAuthorization(Enabled = false)]
        [Common.Web.Attributes.SessionState(SessionStateBehavior.Required)]
        public ActionResult LogOff(bool timeout = false)
        {
            this.LogOffPrivate(timeout);

            return this.RedirectToAction("Login", "Account");
        }

        [AllowAnonymous]
        public ActionResult Lockout()
        {
            return this.View(false);
        }

        [HttpGet]
        [AllowAnonymous]
        [RequiresAcknowledgement(Enabled = false)]
        [RequiresSsoAuthorization(Enabled = false)]
        [Common.Web.Attributes.SessionState(SessionStateBehavior.Required)]
        public ActionResult MustAccept()
        {
            if (this.User.Identity.IsAuthenticated)
            {
                this.VisitPayUserApplicationService.Value.SignOut(this.User.Identity.Name, false);
                this.AuthenticationManager.SignOut();
                return this.RedirectToAction("MustAccept");
            }

            this.SessionFacade.Value.ClearSession();
            return this.View();
        }

        #region request and reset username/password

        [AllowAnonymous]
        [HttpGet]
        public ActionResult ForgotUsername()
        {
            this.Logger.Value.Info(() => $"{nameof(BaseAccountController)}::{nameof(this.ForgotUsername)} - obsolete action, referrer: {this.Request.UrlReferrer?.ToString() ?? string.Empty}");
            return this.RedirectToActionPermanent("RequestUsername");
        }

        [AllowAnonymous]
        [HttpGet]
        public ActionResult RequestUsername()
        {
            RequestUsernameViewModel requestUserNameViewModel = new RequestUsernameViewModel();
            this.InitializeBaseAccountRecoveryViewModel(requestUserNameViewModel);
            return this.View(requestUserNameViewModel);
        }

        private void InitializeBaseAccountRecoveryViewModel(BaseAccountRecoveryViewModel baseAccountRecoveryViewModel)
        {
            AccountRecoveryConfigurationDto matchConfiguration = JsonConvert.DeserializeObject<AccountRecoveryConfigurationDto>(this.ClientDto.AccountRecoveryConfiguration);
            if (matchConfiguration != null)
            {
                AccountRecoveryFieldConfigurationDto ssn4Configuration = matchConfiguration.ConfigurationForField(AccountRecoveryMatchFieldEnum.Ssn4);
                if (ssn4Configuration != null)
                {
                    baseAccountRecoveryViewModel.SsnVisible = ssn4Configuration.Visible;
                    baseAccountRecoveryViewModel.RequireSsn = ssn4Configuration.Required;
                }

                AccountRecoveryFieldConfigurationDto firstNameConfiguration = matchConfiguration.ConfigurationForField(AccountRecoveryMatchFieldEnum.FirstName);
                if (firstNameConfiguration != null)
                {
                    baseAccountRecoveryViewModel.FirstNameVisible = firstNameConfiguration.Visible;
                    baseAccountRecoveryViewModel.RequireFirstName = firstNameConfiguration.Required;
                }
            }
        }

        private bool UseSsnForPasswordReset()
        {
            AccountRecoveryConfigurationDto matchConfiguration = JsonConvert.DeserializeObject<AccountRecoveryConfigurationDto>(this.ClientDto.AccountRecoveryConfiguration);
            if (matchConfiguration != null)
            {
                AccountRecoveryFieldConfigurationDto ssn4Configuration = matchConfiguration.ConfigurationForField(AccountRecoveryMatchFieldEnum.Ssn4);
                if (ssn4Configuration != null)
                {
                    return ssn4Configuration.Required;
                }
            }
            return true;
        }

        [AllowAnonymous]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> RequestUsername(RequestUsernameViewModel model)
        {
            // validate model
            if (!this.ModelState.IsValid)
            {
                return this.Json(new ResultMessage<Hashtable>(false, this.ModelState.GetErrors()));
            }

            VisitPayUserFindByDetailsParametersDto visitPayUserFindByDetailsParameters = Mapper.Map<VisitPayUserFindByDetailsParametersDto>(model);

            bool success = await this.VisitPayUserApplicationService.Value.RequestUsernameGuarantor(visitPayUserFindByDetailsParameters).ConfigureAwait(true);

            // not showing a success or fail, the same messaging for either
            return this.PartialView("_RequestUsernameConfirmation");
        }

        [AllowAnonymous]
        [HttpGet]
        public ActionResult ForgotPassword()
        {
            this.Logger.Value.Info(() => $"{nameof(BaseAccountController)}::{nameof(this.ForgotPassword)} - obsolete action, referrer: {this.Request.UrlReferrer?.ToString() ?? string.Empty}");
            return this.RedirectToActionPermanent("RequestPassword");
        }

        [AllowAnonymous]
        [HttpGet]
        public ActionResult RequestPassword()
        {
            RequestPasswordViewModel requestPasswordViewModel = new RequestPasswordViewModel();
            this.InitializeBaseAccountRecoveryViewModel(requestPasswordViewModel);
            return this.View(requestPasswordViewModel);
        }

        [AllowAnonymous]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> RequestPassword(RequestPasswordViewModel model)
        {
            // validate model
            if (!this.ModelState.IsValid)
            {
                return this.Json(new ResultMessage<Hashtable>(false, this.ModelState.GetErrors()));
            }

            VisitPayUserFindByDetailsParametersDto visitPayUserFindByDetailsParameters = Mapper.Map<VisitPayUserFindByDetailsParametersDto>(model);

            // request password
            bool success = await this.VisitPayUserApplicationService.Value.RequestPasswordGuarantor(
                    visitPayUserFindByDetailsParameters,
                    Mapper.Map<JournalEventHttpContextDto>(System.Web.HttpContext.Current)
                ).ConfigureAwait(true);

            // not showing a success or fail, the same messaging for either
            return this.PartialView("_RequestPasswordConfirmation");
        }

        [AllowAnonymous]
        [HttpGet]
        public virtual ActionResult ResetPassword()
        {
            ResetPasswordViewModel model = new ResetPasswordViewModel();

            string password = this.Request.QueryString["p"];
            if (string.IsNullOrWhiteSpace(password))
            {
                this.Logger.Value.Info(() => "BaseAccountController::ResetPassword - no temp password");
            }
            else
            {
                model.TempPassword = password.TrimNullSafe();
            }

            string username = this.TempData[TempDataKeys.Username] as string;
            if (!string.IsNullOrWhiteSpace(username))
            {
                model.Username = username.TrimNullSafe();
            }

            model.RequireSsn = this.UseSsnForPasswordReset();

            return this.View(model);
        }

        [AllowAnonymous]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ResetPassword(ResetPasswordViewModel model)
        {
            // validate model
            if (!this.ModelState.IsValid)
            {
                return this.Json(new ResultMessage<Hashtable>(false, this.ModelState.GetErrors()));
            }

            // find the account
            VisitPayUserDto user = await this.VisitPayUserApplicationService.Value.FindByNameAsync(model.Username.TrimNullSafe()).ConfigureAwait(true);
            string userSsn4 = user?.SSN4?.ToString() ?? string.Empty;

            bool requireSsn4Verification = model.RequireSsn;

            bool isSsn4ValidOrBypassed = true;
            if (requireSsn4Verification)
            {
                isSsn4ValidOrBypassed = (model.Ssn4.TrimNullSafe() ?? string.Empty).PadLeft(4, '0') == userSsn4.PadLeft(4, '0');
            }

            if (user == null || !isSsn4ValidOrBypassed)
            {
                this.ModelState.AddModelError(nameof(ResetPasswordViewModel.Username), SystemConstants.AccountNotFound);
                if (requireSsn4Verification)
                {
                    this.ModelState.AddModelError(nameof(ResetPasswordViewModel.Ssn4), string.Empty);
                }

                await this.VisitPayUserApplicationService.Value.LogVerificationResultAsync(model.Username.TrimNullSafe(), user?.VisitPayUserId, false, Mapper.Map<JournalEventHttpContextDto>(System.Web.HttpContext.Current));
                return this.Json(new ResultMessage<Hashtable>(false, this.ModelState.GetErrors()));
            }

            // account is not active
            if (user.IsDisabled || user.IsLocked)
            {
                this.Logger.Value.Info(() => $"{nameof(BaseAccountController)}::{nameof(ResetPassword)} - account disabled or locked, disabled: {user.IsDisabled}, locked: {user.IsLocked}");
                return this.PartialView("_Lockout", user.IsDisabled);
            }

            // temp password invalid
            bool isTempPasswordExpired = this.VisitPayUserApplicationService.Value.IsTempPasswordExpired(user);
            bool isTempPasswordValid = this.VisitPayUserApplicationService.Value.VerifyTempPassword(user.TempPassword, model.TempPassword.TrimNullSafe());
            if (isTempPasswordExpired || !isTempPasswordValid)
            {
                await this.VisitPayUserApplicationService.Value.LogVerificationResultAsync(user.UserName, user.VisitPayUserId, false, Mapper.Map<JournalEventHttpContextDto>(System.Web.HttpContext.Current));
                this.Logger.Value.Info(() => $"{nameof(BaseAccountController)}::{nameof(ResetPassword)} - attempt to reset with invalid temp password, expired: {isTempPasswordExpired}, valid: {isTempPasswordValid}");
                return this.PartialView("_ResetPasswordTokenExpired");
            }

            // validate password
            string password = model.Password.TrimNullSafe();
            bool isPasswordValid = (await this.VisitPayUserApplicationService.Value.IsPasswordStrongEnough(password)).Succeeded;
            if (!isPasswordValid)
            {
                string guarantorPasswordRequirements = await this.VisitPayUserApplicationService.Value.GetGuarantorPasswordRequirements();

                this.ModelState.AddModelError(nameof(ResetPasswordViewModel.Password), guarantorPasswordRequirements);
                return this.Json(new ResultMessage<Hashtable>(false, this.ModelState.GetErrors()));
            }

            // reset the password
            IdentityResult result = await this.VisitPayUserApplicationService.Value.ResetPasswordAsync(
                user.Id,
                model.TempPassword,
                password,
                this.ClientDto.GuarantorPasswordExpLimitInDays,
                this.ClientDto.GuarantorUnrepeatablePasswordCount).ConfigureAwait(true);

            // error
            if (!result.Succeeded)
            {
                foreach (string error in result.Errors)
                {
                    this.ModelState.AddModelError(string.Empty, error);
                }
                this.Logger.Value.Info(() => $"{nameof(BaseAccountController)}::{nameof(ResetPassword)} - result failed {string.Join(",", result.Errors)}");
                return this.Json(new ResultMessage<Hashtable>(false, this.ModelState.GetErrors()));
            }

            // success
            await this.SsoApplicationService.Value.ReLoginAsync(Mapper.Map<HttpContextDto>(System.Web.HttpContext.Current), user.Id).ConfigureAwait(true);
            return this.Json(new ResultMessage(true, this.Url.Action("Index", "Home")));
        }

        #endregion

        #region password expired

        [AllowAnonymous]
        [HttpGet]
        public ActionResult PasswordExpired()
        {
            object password = this.TempData[TempDataKeys.Password];
            object username = this.TempData[TempDataKeys.Username];

            if (username != null)
            {
                return this.View(new PasswordExpiredViewModel
                {
                    Password = (string)password,
                    Username = (string)username
                });
            }

            this.Logger.Value.Info(() => "AccountController::PasswordExpiredViewModel - no username in temp data");
            return this.RedirectToAction("Login", "Account");
        }

        [AllowAnonymous]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> PasswordExpired(PasswordExpiredViewModel model)
        {
            // validate model
            if (!this.ModelState.IsValid)
            {
                return this.Json(new ResultMessage<Hashtable>(false, this.ModelState.GetErrors()));
            }

            // find the account
            VisitPayUserDto user = await this.VisitPayUserApplicationService.Value.FindByNameAsync(model.Username.TrimNullSafe()).ConfigureAwait(true);
            if (user == null)
            {
                this.Logger.Value.Info(() => $"{nameof(BaseAccountController)}::{nameof(PasswordExpired)} - account not found, {model.Username}");

                this.ModelState.AddModelError(nameof(PasswordExpiredViewModel.Username), SystemConstants.AccountNotFound);
                return this.Json(new ResultMessage<Hashtable>(false, this.ModelState.GetErrors()));
            }

            // account is not active
            if (user.IsDisabled || user.IsLocked)
            {
                this.Logger.Value.Info(() => $"{nameof(BaseAccountController)}::{nameof(PasswordExpired)} - account disabled or locked, disabled: {user.IsDisabled}, locked: {user.IsLocked}");
                return this.PartialView("_Lockout", user.IsDisabled);
            }

            // validate password
            bool isPasswordValid = (await this.VisitPayUserApplicationService.Value.IsPasswordStrongEnough(model.NewPassword.TrimNullSafe())).Succeeded;
            if (!isPasswordValid)
            {
                string guarantorPasswordRequirements = await this.VisitPayUserApplicationService.Value.GetGuarantorPasswordRequirements();

                this.ModelState.AddModelError(nameof(PasswordExpiredViewModel.NewPassword), guarantorPasswordRequirements);
                return this.Json(new ResultMessage<Hashtable>(false, this.ModelState.GetErrors()));
            }

            // change the password
            IdentityResult result = await this.VisitPayUserApplicationService.Value.ChangePasswordAsync(user.Id,
                model.Password,
                model.NewPassword,
                this.ClientDto.GuarantorPasswordExpLimitInDays,
                this.ClientDto.GuarantorUnrepeatablePasswordCount).ConfigureAwait(true);

            // error
            if (!result.Succeeded)
            {
                foreach (string error in result.Errors)
                {
                    this.ModelState.AddModelError(string.Empty, error);
                }

                this.Logger.Value.Info(() => $"{nameof(BaseAccountController)}::{nameof(PasswordExpired)} - result failed {string.Join(",", result.Errors)}");
                return this.Json(new ResultMessage<Hashtable>(false, this.ModelState.GetErrors()));
            }

            // success
            await this.SsoApplicationService.Value.ReLoginAsync(Mapper.Map<HttpContextDto>(System.Web.HttpContext.Current), user.Id).ConfigureAwait(true);
            return this.Json(new ResultMessage(true, this.Url.Action("Index", "Home")));
        }

        #endregion

        #region external pagees

        [AllowAnonymous]
        [SkipRestrictedCountryCheck]
        [HttpGet]
        public virtual ActionResult ContactUs(bool countryRestricted = false)
        {
            string view = this.IsFeatureEnabled(VisitPayFeatureEnum.AlternatePublicPages) ? "ContactUsAlt" : "ContactUs";
            return this.View(view, countryRestricted);
        }

        [AllowAnonymous]
        [HttpGet]
        public virtual async Task<ActionResult> LearnMore()
        {
            CmsVersionDto cmsVersionLearnMore = await this.ContentApplicationService.Value.GetCurrentVersionAsync(CmsRegionEnum.LearnMore);
            CmsVersionDto cmsVersionFaqExternal = await this.ContentApplicationService.Value.GetCurrentVersionAsync(CmsRegionEnum.FaqsExternal);
            if (!string.IsNullOrEmpty(cmsVersionFaqExternal.ContentBody))
            {
                cmsVersionLearnMore.ContentBody = cmsVersionLearnMore.ContentBody.Replace("[[ExternalFAQs]]", cmsVersionFaqExternal.ContentBody);
            }

            TextViewModel model = new TextViewModel
            {
                Text = cmsVersionLearnMore.ContentBody
            };

            string view = this.IsFeatureEnabled(VisitPayFeatureEnum.AlternatePublicPages) ? "LearnMoreAlt" : "LearnMore";
            return this.View(view, model);
        }

        [AllowAnonymous]
        [HttpGet]
        public ActionResult Faqs()
        {
            return this.RedirectToAction("LearnMore");
        }

        #endregion

        #region session timer

        [AllowAnonymous]
        [OverrideAuthorization]
        [EmulateUser(AllowPost = true)]
        [RequiresAcknowledgement(Enabled = false)]
        [RequiresSsoAuthorization(Enabled = false)]
        [Common.Web.Attributes.SessionState(SessionStateBehavior.Required)]
        public ActionResult UpdateSession()
        {
            if (!this.User.Identity.IsAuthenticated)
            {
                return this.Json(new { }, JsonRequestBehavior.AllowGet);
            }

            if (this.VisitPayUserApplicationService.Value.IsAccountEnabled(this.User.CurrentUserId()))
            {
                return this.Json(new { }, JsonRequestBehavior.AllowGet);
            }

            this.LogOffPrivate(false);

            //Using this to touch the session to keep it alive.
            return this.Json(new { }, JsonRequestBehavior.AllowGet);
        }

        #endregion

        [NonAction]
        protected void LogOffPrivate(bool timeout)
        {
            if (this.User.Identity.IsAuthenticated)
            {
                this.VisitPayUserApplicationService.Value.SignOut(this.User.Identity.Name, timeout);
                this.AuthenticationManager.SignOut();
            }

            this.SessionFacade.Value.ClearSession();
        }

        private ActionResult ClearExistingSession(string returnUrl, bool showSystemMessage = false)
        {
            this.LogOffPrivate(false);

            RouteValueDictionary routeValues = new RouteValueDictionary
            {
                {"returnUrl", returnUrl}
            };

            if (showSystemMessage)
            {
                routeValues.Add("sm", 1);
            }

            return this.RedirectToAction("Login", "Account", routeValues);
        }

        #endregion

        #region alerts

        [RequiresAcknowledgement(Enabled = false)]
        [RequiresSsoAuthorization(Enabled = false)]
        [AllowOffline]
        [Common.Web.Attributes.SessionState(SessionStateBehavior.Required)]
        public virtual ActionResult Alerts(bool isMobile = false)
        {
            int guarantorId = this.VisitPayUserApplicationService.Value.GetGuarantorIdFromVisitPayUserId(this.User.CurrentUserId());

            IList<int> list = this.WebPatientSessionFacade.Value.DismissedAlerts;
            IList<AlertDto> alerts = this.AlertingApplicationService.Value.GetAlertsForGuarantor(guarantorId, this.SessionFacade.Value, list, isMobile);
            IEnumerable<AlertViewModel> alertViewModels = Mapper.Map<IEnumerable<AlertViewModel>>(alerts).ToList();

            return this.PartialView("_Alerts", new AlertsViewModel
            {
                Count = alerts.Count,
                Alerts = alertViewModels
            });
        }

        [HttpPost]
        [AllowOffline]
        [Common.Web.Attributes.SessionState(SessionStateBehavior.Required)]
        public virtual JsonResult RemoveAlert(int id)
        {
            IList<int> list = this.WebPatientSessionFacade.Value.DismissedAlerts;
            list.Add(id);
            this.WebPatientSessionFacade.Value.DismissedAlerts = list;
            return this.Json(true);
        }

        #endregion

        #region visits

        [NonAction]
        protected VisitEobDetailViewListModel GetVisitEobDetailViewListModel(IReadOnlyList<VisitEobDetailsResultDto> visitEobDetails)
        {
            List<VisitEobDetailViewModel> visitEobDetailViewModels = VisitEobDetailViewModel.GetVisitEobDetailViewModels(visitEobDetails);
            VisitEobDetailViewListModel visitEobDetailViewListModel = new VisitEobDetailViewListModel { VisitEobDetailViewModels = visitEobDetailViewModels };

            return visitEobDetailViewListModel;
        }

        #endregion

        [AllowAnonymous]
        [RequiresAcknowledgement(Enabled = false)]
        [RequiresSsoAuthorization(Enabled = false)]
        [HttpGet]
        public JsonResult ChangeLocale(string locale)
        {
            //Update the cookie with the locale
            this.LocalizationCookieFacade.Value.SetLocale(locale, explicitlySetByUser: true);

            //Update the user locale, if logged in
            if (this.User != null && this.User.Identity.IsAuthenticated)
            {
                this.VisitPayUserApplicationService.Value.SetUserLocale(this.User.Identity.GetUserId(), locale);
            }

            return this.Json(true, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult UpdateUsername(ProfileEditUserNameViewModel model)
        {
            if (!this.ModelState.IsValid)
            {
                return this.Json(new ResultMessage(false, this.GenericErrorMessage));
            }

            bool isUsernameAvailable = this.VisitPayUserApplicationService.Value.IsUsernameAvailable(model.UserName, this.CurrentUserId);
            if (!isUsernameAvailable)
            {
                string resultMessage = LocalizationHelper.GetLocalizedString(TextRegionConstants.UsernameAlreadyExists);
                return this.Json(new ResultMessage(false, resultMessage));
            }

            VisitPayUserDto visitPayUserDto = this.VisitPayUserApplicationService.Value.FindById(this.User.Identity.GetUserId());
            if (visitPayUserDto != null)
            {
                visitPayUserDto.UserName = model.UserName;
                IdentityResult identityResult = this.VisitPayUserApplicationService.Value.UpdateUser(visitPayUserDto);
                string resultMessage = identityResult.Succeeded ? LocalizationHelper.GetLocalizedString(TextRegionConstants.UsernameUpdated) : base.GenericErrorMessage;
                return this.Json(new ResultMessage(identityResult.Succeeded, resultMessage));
            }

            return this.Json(new ResultMessage(false, this.GenericErrorMessage));
        }

        protected JsonResult UserProfileEdit(VisitPayUserDto visitPayUserDto)
        {
            if (visitPayUserDto != null)
            {
                visitPayUserDto.VisitPayUserId = this.CurrentUserId;

                IdentityResult identityResult = this.VisitPayUserApplicationService.Value.UpdateUser(visitPayUserDto);

                string resultMessage = identityResult.Succeeded ? LocalizationHelper.GetLocalizedString(TextRegionConstants.PersonalInformationUpdatedSuccessfully) : this.GenericErrorMessage;
                return this.Json(new ResultMessage(identityResult.Succeeded, resultMessage));
            }

            return this.Json(new ResultMessage(false, this.GenericErrorMessage));
        }

        #region phone and text

        protected string SmsValidationKey => "SMS" + this.User.Identity.GetUserId();

        protected string GenerateToken()
        {
            string token = string.Empty;
            Random generator = new Random(DateTime.UtcNow.Millisecond);
            for (int i = 0; i < 6; i++)
            {
                token += generator.Next(0, 9).ToString();
            }
            this.Cache.Value.SetStringAsync(this.SmsValidationKey, token, 300);
            return token;
        }

        protected bool ValidateToken(string token)
        {
            string cachedToken = Task.Run(async () => await this.Cache.Value.GetStringAsync(this.SmsValidationKey).ConfigureAwait(false)).Result;
            if (!string.IsNullOrWhiteSpace(cachedToken) && string.Compare(cachedToken, token, StringComparison.CurrentCultureIgnoreCase) == 0)
            {
                this.MetricsProvider.Value.Increment(Metrics.Increment.Communication.Sms.ValidAuthCode);
                return true;
            }
            this.MetricsProvider.Value.Increment(Metrics.Increment.Communication.Sms.InvalidAuthCode);
            return false;
        }

        protected EditPhoneViewModel CreateEditPhoneViewModel(SmsPhoneTypeEnum phoneType)
        {
            string visitPayUserId = this.User.Identity.GetUserId();
            VisitPayUserDto visitPayUserDto = this.VisitPayUserApplicationService.Value.FindById(visitPayUserId);
            SmsPhoneTypeEnum? smsPhoneType = this.VisitPayUserApplicationService.Value.GetSmsAcknowledgement(visitPayUserId);

            EditPhoneViewModel model = new EditPhoneViewModel
            {
                VisitPayUserId = visitPayUserId,
                PhoneNumber = phoneType == SmsPhoneTypeEnum.Primary ? visitPayUserDto.PhoneNumber : visitPayUserDto.PhoneNumberSecondary,
                PhoneNumberType = phoneType == SmsPhoneTypeEnum.Primary ? visitPayUserDto.PhoneNumberType : visitPayUserDto.PhoneNumberSecondaryType,
                IsSmsEnabled = this.IsFeatureEnabled(VisitPayFeatureEnum.Sms) && smsPhoneType != null && smsPhoneType.Value == phoneType,
                IsSmsActivateEnabled = this.IsFeatureEnabled(VisitPayFeatureEnum.Sms) && smsPhoneType == null,
                IsSmsSwitchToEnabled = this.IsFeatureEnabled(VisitPayFeatureEnum.Sms) && smsPhoneType != null && smsPhoneType.Value != phoneType,
                SmsPhoneType = phoneType
            };

            return model;
        }

        protected async Task<PhoneTextInformationViewModel> CreatePhoneTextInformationViewModelAsync()
        {
            string visitPayUserId = this.User.Identity.GetUserId();
            VisitPayUserDto visitPayUserDto = this.VisitPayUserApplicationService.Value.FindById(visitPayUserId);
            SmsPhoneTypeEnum? smsPhoneType = this.VisitPayUserApplicationService.Value.GetSmsAcknowledgement(visitPayUserId);
            int cmsVersionId = (await this.ContentApplicationService.Value.GetCurrentVersionAsync(CmsRegionEnum.SmsAcknowledgement, false)).CmsVersionId;

            IList<CommunicationGroupSelectionViewModel> preferences = null;
            if (this.IsFeatureEnabled(VisitPayFeatureEnum.Sms))
            {
                IList<CommunicationGroupDto> communicationGroupDtos = await this.CommunicationApplicationService.Value.GetCommunicationGroupsAsync();
                preferences = Mapper.Map<IList<CommunicationGroupDto>, IList<CommunicationGroupSelectionViewModel>>(communicationGroupDtos);
                if (smsPhoneType.HasValue)
                {
                    foreach (VisitPayUserCommunicationPreferenceDto communicationPreference in this.CommunicationApplicationService.Value.CommunicationPreferences(visitPayUserDto.VisitPayUserId))
                    {
                        if (preferences.Any(g => g.CommunicationGroupId == communicationPreference.CommunicationGroupId))
                        {
                            preferences.First(g => g.CommunicationGroupId == communicationPreference.CommunicationGroupId).Enabled = true;
                        }
                    }
                }
            }

            string phoneNumberType = string.Empty;
            if (!string.IsNullOrWhiteSpace(visitPayUserDto.PhoneNumberType))
            {
                phoneNumberType = await this.ContentApplicationService.Value.GetTextRegionAsync(visitPayUserDto.PhoneNumberType);
            }

            string phoneNumberSecondaryType = string.Empty;
            if (!string.IsNullOrWhiteSpace(visitPayUserDto.PhoneNumberSecondaryType))
            {
                phoneNumberSecondaryType = await this.ContentApplicationService.Value.GetTextRegionAsync(visitPayUserDto.PhoneNumberSecondaryType);
            }

            return new PhoneTextInformationViewModel
            {
                VisitPayUserId = visitPayUserId,
                PhoneNumber = visitPayUserDto.PhoneNumber,
                PhoneNumberType = phoneNumberType,
                PhoneNumberTypeIsMobile = visitPayUserDto.PhoneNumberTypeIsMobile,
                PhoneNumberSecondary = visitPayUserDto.PhoneNumberSecondary,
                PhoneNumberSecondaryType = phoneNumberSecondaryType,
                PhoneNumberSecondaryTypeIsMobile = visitPayUserDto.PhoneNumberSecondaryTypeIsMobile,
                SmsModuleEnabled = this.IsFeatureEnabled(VisitPayFeatureEnum.Sms),
                SmsEnabledFor = smsPhoneType,
                CmsVersionId = cmsVersionId,
                Preferences = preferences
            };
        }

        protected IEnumerable<CommunicationSmsBlockedNumberViewModel> IsNumberBlocked(SmsPhoneTypeEnum smsPhoneType)
        {
            string visitPayUserId = this.User.Identity.GetUserId();
            VisitPayUserDto visitPayUserDto = this.VisitPayUserApplicationService.Value.FindById(visitPayUserId);
            string phone = smsPhoneType == SmsPhoneTypeEnum.Primary ? visitPayUserDto.PhoneNumber : visitPayUserDto.PhoneNumberSecondary;
            return Mapper.Map<IList<CommunicationSmsBlockedNumberViewModel>>(this.CommunicationApplicationService.Value.FindBlockedNumbers(phone));
        }

        protected ActivateTextMessagingViewModel CreateActivateTextMessagingViewModel(SmsPhoneTypeEnum smsPhoneType)
        {
            string visitPayUserId = this.User.Identity.GetUserId();
            VisitPayUserDto visitPayUserDto = this.VisitPayUserApplicationService.Value.FindById(visitPayUserId);
            int cmsVersionId = this.ContentApplicationService.Value.GetCurrentVersionAsync(CmsRegionEnum.SmsAcknowledgement, false).Result.CmsVersionId;
            string phone = smsPhoneType == SmsPhoneTypeEnum.Primary ? visitPayUserDto.PhoneNumber : visitPayUserDto.PhoneNumberSecondary;
            return new ActivateTextMessagingViewModel
            {
                ActivateTextCmsVersionId = cmsVersionId,
                PhoneNumber = phone,
                SmsPhoneType = smsPhoneType
            };
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult PhoneEdit(EditPhoneViewModel model)
        {
            if (!this.ModelState.IsValid)
            {
                return this.Json(new ResultMessage(false, this.GenericErrorMessage));
            }

            IdentityResult identityResult = this.VisitPayUserApplicationService.Value.UpdatePhone(
                visitPayUserId: model.VisitPayUserId,
                phoneNumber: model.PhoneNumber,
                phoneNumberType: model.PhoneNumberType,
                isPrimary: model.SmsPhoneType == SmsPhoneTypeEnum.Primary,
                context: Mapper.Map<HttpContext, JournalEventHttpContextDto>(System.Web.HttpContext.Current),
                clientVisitPayUserId: null);

            string resultMessage = identityResult.Succeeded ? LocalizationHelper.GetLocalizedString(TextRegionConstants.PhoneNumberUpdated) : this.GenericErrorMessage;
            return this.Json(new ResultMessage(identityResult.Succeeded, resultMessage));
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult SecondaryPhoneDelete()
        {
            IdentityResult identityResult = this.VisitPayUserApplicationService.Value.SecondaryPhoneDelete(
                visitPayUserId: this.User.Identity.GetUserId(),
                context: Mapper.Map<HttpContext, JournalEventHttpContextDto>(System.Web.HttpContext.Current),
                clientVisitPayUserId: null);

            string resultMessage = identityResult.Succeeded ? LocalizationHelper.GetLocalizedString(TextRegionConstants.PhoneNumberDeleted) : this.GenericErrorMessage;
            return this.Json(new ResultMessage(identityResult.Succeeded, resultMessage));
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult AddSmsPreference(int id)
        {
            string visitPayUserId = this.User.Identity.GetUserId();
            VisitPayUserDto visitPayUserDto = this.VisitPayUserApplicationService.Value.FindById(visitPayUserId);
            this.CommunicationApplicationService.Value.AddPreference(CommunicationMethodEnum.Sms, visitPayUserDto.VisitPayUserId, id, this.VisitPayUserApplicationService.Value.GetGuarantorIdFromVisitPayUserId(visitPayUserDto.VisitPayUserId), visitPayUserDto.VisitPayUserId, Mapper.Map<JournalEventHttpContextDto>(System.Web.HttpContext.Current));
            return this.Json(true);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult RemoveSmsPreference(int id)
        {
            string visitPayUserId = this.User.Identity.GetUserId();
            VisitPayUserDto visitPayUserDto = this.VisitPayUserApplicationService.Value.FindById(visitPayUserId);
            this.CommunicationApplicationService.Value.RemovePreference(CommunicationMethodEnum.Sms, visitPayUserDto.VisitPayUserId, id, this.VisitPayUserApplicationService.Value.GetGuarantorIdFromVisitPayUserId(visitPayUserDto.VisitPayUserId), visitPayUserDto.VisitPayUserId, Mapper.Map<JournalEventHttpContextDto>(System.Web.HttpContext.Current));
            return this.Json(true);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<JsonResult> RemovePhoneMessage(bool smsEnabled)
        {
            IList<string> messages = new List<string>();

            if (smsEnabled)
            {
                string smsEnabledMessage = await this.ContentApplicationService.Value.GetTextRegionAsync(TextRegionConstants.NumberUsedForTextMessagingDelete);
                messages.Add(smsEnabledMessage);
            }

            string message = await this.ContentApplicationService.Value.GetTextRegionAsync(TextRegionConstants.AreYouSureRemovePhoneNumber);
            messages.Add(message);

            return this.Json(messages);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<JsonResult> GetDisableText()
        {
            string visitPayUserId = this.User.Identity.GetUserId();
            VisitPayUserDto visitPayUserDto = this.VisitPayUserApplicationService.Value.FindById(visitPayUserId);
            SmsPhoneTypeEnum? smsPhoneType = this.VisitPayUserApplicationService.Value.GetSmsAcknowledgement(visitPayUserId);

            Dictionary<string, string> replacementValues = new Dictionary<string, string>
            {
                {"[[PhoneNumber]]", !smsPhoneType.HasValue || smsPhoneType.Value == SmsPhoneTypeEnum.Primary ? visitPayUserDto.PhoneNumber : visitPayUserDto.PhoneNumberSecondary}
            };
            CmsVersionDto version = await this.ContentApplicationService.Value.GetCurrentVersionAsync(CmsRegionEnum.SmsDisableMessage, true, replacementValues);
            return this.Json(Mapper.Map<CmsVersionDto, CmsViewModel>(version));
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<JsonResult> CanActivate(SmsPhoneTypeEnum smsPhoneType)
        {
            List<CommunicationSmsBlockedNumberViewModel> blockedNumbers = this.IsNumberBlocked(smsPhoneType).ToList();
            if (!blockedNumbers.Any())
            {
                return this.Json(new { CanActivate = true });
            }
            CmsVersionDto version = await this.ContentApplicationService.Value.GetCurrentVersionAsync(CmsRegionEnum.CommunicationUnblockNumber, true, new Dictionary<string, string> { { "[[BurnedTwilioPhoneNumber]]", blockedNumbers.First().SmsPhoneNumber } });
            return this.Json(new { CanActivate = false, cms = Mapper.Map<CmsVersionDto, CmsViewModel>(version) });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult SendValidationMessage(ActivateTextMessagingViewModel model)
        {
            string visitPayUserId = this.User.Identity.GetUserId();
            this.VisitPayUserApplicationService.Value.SendSmsValidationMessage(visitPayUserId, model.SmsPhoneType, this.GenerateToken(), Mapper.Map<HttpContext, JournalEventHttpContextDto>(System.Web.HttpContext.Current));

            string resultMessage = LocalizationHelper.GetLocalizedString(TextRegionConstants.MessageSent);
            return this.Json(new ResultMessage(true, resultMessage));
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<JsonResult> EnableSms(ActivateTextMessagingViewModel model)
        {
            if (this.ValidateToken(model.ValidationCode))
            {
                string visitPayUserId = this.User.Identity.GetUserId();
                this.VisitPayUserApplicationService.Value.SetSmsAcknowledgement(
                    visitPayUserId: visitPayUserId,
                    smsPhoneType: model.SmsPhoneType,
                    cmsVersionId: model.ActivateTextCmsVersionId,
                    context: Mapper.Map<HttpContext, JournalEventHttpContextDto>(System.Web.HttpContext.Current)
                    );
                return this.Json(new ResultMessage(true, this.Url.Action("PhoneTextSettings")));
            }

            if (model.SmsRetryCount > 3)
            {
                string tooManyAttemptsMessage = await this.ContentApplicationService.Value.GetTextRegionAsync(TextRegionConstants.SmsEnableTooManyAttempts);
                return this.Json(new ResultMessage(false, tooManyAttemptsMessage));
            }

            string invalidCodeMessage = await this.ContentApplicationService.Value.GetTextRegionAsync(TextRegionConstants.SmsEnableInvalidCode);
            return this.Json(new ResultMessage(false, invalidCodeMessage));
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public void DisableSms()
        {
            this.VisitPayUserApplicationService.Value.RemoveSmsAcknowledgement(
                visitPayUserId: this.User.Identity.GetUserId(),
                fromSmsReply: false,
                clientVisitPayUserId: null,
                context: Mapper.Map<HttpContext, JournalEventHttpContextDto>(System.Web.HttpContext.Current)
                );
        }

        #endregion

        #region document center

        [HttpGet]
        public ActionResult Documents()
        {
            int visitPayUserId = this.GuarantorFilterService.Value.VisitPayUserId;
            GuarantorDto guarantorDto = this.VisitPayUserApplicationService.Value.GetGuarantorFromVisitPayUserId(visitPayUserId);

            StatementListViewModel model = new StatementListViewModel();

            DateTime? nextStatementDate = guarantorDto.NextStatementDate ?? this.StatementApplicationService.Value.GetNextStatementDate(guarantorDto.VpGuarantorId, DateTime.UtcNow);
            if (nextStatementDate.HasValue)
            {
                model.NextStatementDate = nextStatementDate.Value.ToString(Format.DateFormat);
            }

            IList<StatementDto> statementsDto = this.StatementApplicationService.Value.GetStatements(visitPayUserId)
                .OrderByDescending(x => x.StatementDate)
                .ToList();

            IList<FinancePlanStatementDto> fpStatementsDto = this.StatementApplicationService.Value.GetFinancePlanStatements(visitPayUserId)
                .Where(x => x.StatementVersion != VpStatementVersionEnum.Vp2)
                .OrderByDescending(x => x.PeriodEndDate)
                .ToList();

            model.FinancePlanStatements = Mapper.Map<List<StatementListItemViewModel>>(fpStatementsDto);
            model.VisitBalanceSummaryStatements = Mapper.Map<List<StatementListItemViewModel>>(statementsDto);

            return this.View(model);
        }

        #endregion

        [NonAction]
        protected ActionResult RedirectToLocal(string returnUrl)
        {
            if (this.Url.IsLocalUrl(returnUrl))
            {
                return this.Redirect(returnUrl);
            }

            return this.RedirectToAction("Index", "Home");
        }
    }
}
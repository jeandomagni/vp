﻿namespace Ivh.Web.Patient.Controllers.Base
{
    using Application.Core.Common.Interfaces;
    using Common.Web.Controllers;
    using Common.Web.Interfaces;
    using Services;
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using System.Web.Mvc;
    using System.Web.SessionState;
    using Application.Base.Common.Interfaces;
    using Application.Core.Common.Dtos;
    using Application.FinanceManagement.Common.Interfaces;
    using Attributes;
    using Common.VisitPay.Strings;
    using Models.Sidebar;

    [GuarantorAuthorize(Roles = VisitPayRoleStrings.System.Patient + "," + VisitPayRoleStrings.Admin.Administrator)]
    public abstract class BaseSidebarController : BaseWebController
    {
        protected readonly Lazy<IExternalLinkApplicationService> ExternalLinkApplicationService;
        protected readonly Lazy<GuarantorFilterService> GuarantorFilterService;
        protected readonly Lazy<IFinancePlanApplicationService> FinancePlanApplicationService;
        protected readonly Lazy<IVisitBalanceBaseApplicationService> VisitBalanceBaseApplicationService;

        protected BaseSidebarController(
            Lazy<IBaseControllerService> baseControllerService,
            Lazy<IExternalLinkApplicationService> externalLinkApplicationService,
            Lazy<GuarantorFilterService> guarantorFilterService,
            Lazy<IFinancePlanApplicationService> financePlanApplicationService,
            Lazy<IVisitBalanceBaseApplicationService> visitBalanceBaseApplicationService)
            : base(baseControllerService)
        {
            this.ExternalLinkApplicationService = externalLinkApplicationService;
            this.GuarantorFilterService = guarantorFilterService;
            this.FinancePlanApplicationService = financePlanApplicationService;
            this.VisitBalanceBaseApplicationService = visitBalanceBaseApplicationService;
        }

        [HttpGet]
        [ChildActionOnly]
        public virtual PartialViewResult Sidebar()
        {
            int vpGuarantorId = this.GuarantorFilterService.Value.VpGuarantorId;
            decimal totalBalance = this.VisitBalanceBaseApplicationService.Value.GetTotalBalance(vpGuarantorId);

            bool isEligibleForFinancePlan = this.FinancePlanApplicationService.Value.IsEligibleForAnyFinancePlan(vpGuarantorId, this.CurrentUserId);

            SidebarViewModel model = new SidebarViewModel
            {
                TotalBalance = totalBalance,
                ShowCreateFinancePlanAction = isEligibleForFinancePlan
            };

            return this.PartialView("_Sidebar", model);
        }

        [HttpGet]
        [ChildActionOnly]
        public virtual PartialViewResult ExternalLinks()
        {
            int vpGuarantorId = this.GuarantorFilterService.Value.VpGuarantorId;
            IList<ExternalLinkDto> links = this.ExternalLinkApplicationService.Value.GetExternalLinksForActiveVisitInsurancePlans(vpGuarantorId);

            return this.PartialView("_ExternalLinks", links);
        }
    }
}
﻿namespace Ivh.Web.Patient.Controllers.Base
{
    using System;
    using System.Threading.Tasks;
    using System.Web;
    using System.Web.Mvc;
    using System.Web.SessionState;
    using Application.Base.Common.Interfaces;
    using Application.Content.Common.Dtos;
    using Application.Content.Common.Interfaces;
    using Application.Core.Common.Dtos;
    using Application.Core.Common.Interfaces;
    using Application.FinanceManagement.Common.Dtos;
    using Application.FinanceManagement.Common.Interfaces;
    using Attributes;
    using AutoMapper;
    using Common.VisitPay.Enums;
    using Common.VisitPay.Strings;
    using Common.Web.Filters;
    using Common.Web.Interfaces;
    using Common.Web.Models.FinancePlan;
    using Common.Web.Models.Payment;
    using Common.Web.Models.Shared;
    using Models;
    using Models.Home;
    using Services;
    using SessionFacade;

    public abstract class BaseHomeController : BaseController
    {
        protected readonly Lazy<IContentApplicationService> ContentApplicationService;
        protected readonly Lazy<IKnowledgeBaseApplicationService> KnowledgeBaseApplicationService;
        protected readonly Lazy<IWebPatientSessionFacade> PatientSession;
        private readonly Lazy<IFinancePlanApplicationService> _financePlanApplicationService;
        private readonly Lazy<IPaymentMethodsApplicationService> _paymentMethodsApplicationService;
        private readonly Lazy<IPaymentSubmissionApplicationService> _paymentSubmissionApplicationService;
        private readonly Lazy<IVisitBalanceBaseApplicationService> _visitBalanceBaseApplicationService;
        private readonly Lazy<IVisitPayUserApplicationService> _visitPayUserApplicationService;

        protected BaseHomeController(
            Lazy<IBaseControllerService> baseControllerService,
            Lazy<IContentApplicationService> contentApplicationService,
            Lazy<IFinancePlanApplicationService> financePlanApplicationService,
            Lazy<IVisitBalanceBaseApplicationService> visitBalanceBaseApplicationService,
            Lazy<IVisitPayUserApplicationService> visitPayUserApplicationService,
            Lazy<GuarantorFilterService> guarantorFilterService,
            Lazy<IWebPatientSessionFacade> patientSession,
            Lazy<IPaymentMethodsApplicationService> paymentMethodsApplicationService,
            Lazy<IPaymentSubmissionApplicationService> paymentSubmissionApplicationService,
            Lazy<IKnowledgeBaseApplicationService> knowledgeBaseApplicationService)
            : base(baseControllerService, guarantorFilterService)
        {
            this.ContentApplicationService = contentApplicationService;
            this.KnowledgeBaseApplicationService = knowledgeBaseApplicationService;
            this.PatientSession = patientSession;
            this._financePlanApplicationService = financePlanApplicationService;
            this._paymentMethodsApplicationService = paymentMethodsApplicationService;
            this._paymentSubmissionApplicationService = paymentSubmissionApplicationService;
            this._visitBalanceBaseApplicationService = visitBalanceBaseApplicationService;
            this._visitPayUserApplicationService = visitPayUserApplicationService;
        }

        [MobileRedirect("/mobile/Home")]
        [HttpGet]
        [Common.Web.Attributes.SessionState(SessionStateBehavior.Required)]
        public ActionResult Index()
        {
            int vpGuarantorId = this.GuarantorFilterService.Value.VpGuarantorId;

            decimal totalBalance = this._visitBalanceBaseApplicationService.Value.GetTotalBalance(vpGuarantorId);
            bool isDiscountEligible = this._paymentSubmissionApplicationService.Value.IsDiscountEligible(vpGuarantorId);

            FinancePlanSummaryViewModel financePlanSummary = this.GetFinancePlanSummaryViewModel(vpGuarantorId);

            // Show NPS survey- after navigating back to the home page for the second or more times and the user has not been presented a survey in the same session
            this.PatientSession.Value.HomepageNavigationCount++;
            
            HomeViewModel model = new HomeViewModel
            {
                IsDiscountEligible = isDiscountEligible,
                TotalBalance = totalBalance,
                FinancePlanSummary = financePlanSummary
            };

            return this.View(model);
        }
        
        [HttpGet]
        [RequiresAcknowledgement(Enabled = false)]
        [RequiresSsoAuthorization(Enabled = false)]
        public async Task<ActionResult> Acknowledgement()
        {
            CmsVersionDto cmsVersion = await this.ContentApplicationService.Value.GetCurrentVersionAsync(CmsRegionEnum.VppTermsAndConditions);

			// We need to ensure that any referring URL containing a ReturnUrl parameter (like from a login page) can still
			// be used for redirection after accepting the Acknowledgement.
			Uri referrer = this.Request.UrlReferrer;
            string returnUrl = string.Empty;
            if (null != referrer)
            {
	            returnUrl = HttpUtility.ParseQueryString(Request.UrlReferrer.Query)["ReturnUrl"];
            }
            AcknowledgementViewModel model = new AcknowledgementViewModel
            {
				ReturnUrl = returnUrl,
                TermsOfUseViewModel = new TermsOfUseViewModel
                {
                    Text = cmsVersion.ContentBody,
                    IsExport = true, // hides the button
                    UpdatedDate = cmsVersion.UpdatedDate.ToString(Format.MonthDayYearFormat)
                },
                TermsOfUseCmsVersionId = cmsVersion.CmsVersionId
            };

            return this.View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [RequiresAcknowledgement(Enabled = false)]
        [RequiresSsoAuthorization(Enabled = false)]
        public async Task<ActionResult> Acknowledgement(int termsOfUseCmsVersionId)
        {
            CmsVersionDto cmsVersion = await this.ContentApplicationService.Value.GetCurrentVersionAsync(CmsRegionEnum.VppTermsAndConditions);
            if (cmsVersion.CmsVersionId == termsOfUseCmsVersionId)
            {
                this._visitPayUserApplicationService.Value.TermsAcknowledgement(this.CurrentUserIdString, VisitPayUserAcknowledgementTypeEnum.TermsOfUse, termsOfUseCmsVersionId);
            }
            else
            {
                this.Logger.Value.Info(() => $"{nameof(BaseHomeController)}::{nameof(Acknowledgement)} - unexpected CmsVersion, parameter: {termsOfUseCmsVersionId}, expected: {cmsVersion.CmsVersionId}");
            }

			// Get the ReturnUrl input if existing and ONLY reuse it if is a local url.
			string returnUrl = this.Request.Form["ReturnUrl"];
            if (!string.IsNullOrEmpty(returnUrl) && this.Url.IsLocalUrl(returnUrl))
            {
	            return this.Redirect(returnUrl);
            }
            return this.RedirectToAction("Index", "Home");
        }

        [HttpPost]
        [OverrideAuthorization]
        [EmulateUser(AllowPost = true)]
        [ValidateAntiForgeryToken]
        [Common.Web.Attributes.SessionState(SessionStateBehavior.Required)]
        public JsonResult ShouldShowRegistrationSurvey()
        {
            if (this.IsCurrentUserEmulating())
            {
                return this.Json(false);
            }

            if (!this.CurrentUser.IsFirstTimeLogin)
            {
                return this.Json(false);
            }

            RandomizedTestGroupEnum randomizedTestGroupEnum = this.GetRandomizedTestGroup(RandomizedTestEnum.RegistrationSurvey);
            if (randomizedTestGroupEnum == RandomizedTestGroupEnum.RegistrationSurveyShouldSurvey)
            {
                return this.Json(true);
            }
            return this.Json(false);
        }

        private FinancePlanSummaryViewModel GetFinancePlanSummaryViewModel(int vpGuarantorId)
        {
            FinancePlanSummaryDto financePlanSummary = this._financePlanApplicationService.Value.GetFinancePlanSummary(vpGuarantorId);
            if (financePlanSummary.FinancePlans.Count == 0)
            {
                return new FinancePlanSummaryViewModel();
            }

            FinancePlanSummaryViewModel financePlanSummaryViewModel = Mapper.Map<FinancePlanSummaryViewModel>(financePlanSummary);
            
            GuarantorDto guarantor = this._visitPayUserApplicationService.Value.GetGuarantorFromVisitPayUserId(this.CurrentUserId);
            bool canUserTakeAction = guarantor.ConsolidationStatus != GuarantorConsolidationStatusEnum.Managed;
            financePlanSummaryViewModel.CanUserTakeAction = canUserTakeAction;

            if (canUserTakeAction)
            {
                PaymentMethodDto paymentMethodDto = this._paymentMethodsApplicationService.Value.GetPrimaryPaymentMethod(guarantor.VpGuarantorId);
                financePlanSummaryViewModel.PaymentMethod = Mapper.Map<PaymentMethodDisplayViewModel>(paymentMethodDto ?? new PaymentMethodDto());
            }

            return financePlanSummaryViewModel;
        }
    }
}
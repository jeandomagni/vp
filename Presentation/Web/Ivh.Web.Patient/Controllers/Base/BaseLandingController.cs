﻿namespace Ivh.Web.Patient.Controllers.Base
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using System.Web.Mvc;
    using Application.Content.Common.Interfaces;
    using Application.Core.Common.Dtos;
    using Application.Core.Common.Interfaces;
    using AutoMapper;
    using Common.Base.Enums;
    using Common.VisitPay.Enums;
    using Common.Web.Controllers;
    using Common.Web.Cookies;
    using Common.Web.Interfaces;
    using Common.Web.Models.Shared;
    using Models.Landing;

    [AllowAnonymous]
    public class BaseLandingController : BaseWebController
    {
        private readonly Lazy<IContentApplicationService> _contentApplicationService;
        private readonly Lazy<ISsoApplicationService> _ssoApplicationService;
        private readonly Lazy<IWebCookieFacade> _webCookie;

        public BaseLandingController(
            Lazy<IBaseControllerService> baseControllerService,
            Lazy<IContentApplicationService> contentApplicationService,
            Lazy<ISsoApplicationService> ssoApplicationService,
            Lazy<IWebCookieFacade> webCookie)
            : base(baseControllerService)
        {
            this._contentApplicationService = contentApplicationService;
            this._ssoApplicationService = ssoApplicationService;
            this._webCookie = webCookie;
        }

        [HttpGet]
        [AllowAnonymous]
        public virtual async Task<ActionResult> Index()
        {
            if (this.HttpContext.User.Identity.IsAuthenticated)
            {
                return this.RedirectToAction("Index", "Home");
            }

            if (!this.ClientDto.GuarantorIsLandingPageEnabled)
            {
                return this.RedirectToAction("Login", "Account");
            }

            SsoResponseDto ssoResponse = await this._webCookie.Value.GetSsoResponseDtoAsync(System.Web.HttpContext.Current);
            SsoProviderDto ssoProviderDto = null;
            if (ssoResponse != null && ssoResponse.FoundMatchesThatCouldBeRegistered)
            {
                ssoProviderDto = this._ssoApplicationService.Value.GetProvider(ssoResponse.ProviderEnum);
            }

            // alt landing page
            //if (this.IsFeatureEnabled(VisitPayFeatureEnum.AlternatePublicPages))
            //{
                CmsRegionEnum? ssoCmsRegionEnum = null;
                if (ssoProviderDto != null)
                {
                    ssoCmsRegionEnum = await this._contentApplicationService.Value.GetSsoProviderSpecificCmsRegionAsync(ssoProviderDto.SsoProviderEnum, SsoProviderSpecificCmsRegionEnum.LandingContentSso);
                }

                return this.View("IndexAlt", new LandingViewModel
                {
                    SsoCmsRegionEnum = ssoCmsRegionEnum
                });
            //}
            
            /*
            // default landing page
            CmsRegionEnum? titleLeftCmsRegion = null;
            IDictionary<string, string> additionalValues = new Dictionary<string, string>();

            if (ssoProviderDto != null)
            {
                additionalValues.Add("[[SsoProviderDisplayName]]", ssoProviderDto.SsoProviderDisplayName);
                titleLeftCmsRegion = await this._contentApplicationService.Value.GetSsoProviderSpecificCmsRegionAsync(ssoResponse.ProviderEnum, SsoProviderSpecificCmsRegionEnum.LandingPageTitleLeft);
            }

            LandingViewModel model = new LandingViewModel
            {
                ContentLeft = Mapper.Map<CmsViewModel>(await this._contentApplicationService.Value.GetCurrentVersionAsync(CmsRegionEnum.LandingPageContentLeft)),
                ContentRight = Mapper.Map<CmsViewModel>(await this._contentApplicationService.Value.GetCurrentVersionAsync(CmsRegionEnum.LandingPageContentRight)),
                TitleLeft = Mapper.Map<CmsViewModel>(await this._contentApplicationService.Value.GetCurrentVersionAsync(titleLeftCmsRegion.GetValueOrDefault(CmsRegionEnum.LandingPageTitleLeft), true, additionalValues)),
                TitleRight = Mapper.Map<CmsViewModel>(await this._contentApplicationService.Value.GetCurrentVersionAsync(CmsRegionEnum.LandingPageTitleRight))
            };

            return this.View(model);*/
        }
    }
}
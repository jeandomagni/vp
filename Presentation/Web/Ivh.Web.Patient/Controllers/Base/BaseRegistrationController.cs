﻿namespace Ivh.Web.Patient.Controllers.Base
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Linq;
    using System.Net;
    using System.Security.Claims;
    using System.Threading.Tasks;
    using System.Web;
    using System.Web.Mvc;
    using System.Web.SessionState;
    using Application.Content.Common.Dtos;
    using Application.Content.Common.Interfaces;
    using Application.Core.Common.Dtos;
    using Application.Core.Common.Interfaces;
    using Application.FinanceManagement.Common.Dtos;
    using Application.FinanceManagement.Common.Interfaces;
    using Application.User.Common.Dtos;
    using Attributes;
    using AutoMapper;
    using Common.Base.Utilities.Extensions;
    using Common.Base.Utilities.Helpers;
    using Common.EventJournal;
    using Common.ServiceBus.Interfaces;
    using Common.VisitPay.Constants;
    using Common.VisitPay.Enums;
    using Common.VisitPay.Messages.Core;
    using Common.VisitPay.Messages.Matching.Dtos;
    using Common.Web.Cookies;
    using Common.Web.Extensions;
    using Common.Web.Interfaces;
    using Common.Web.Models;
    using Common.Web.Models.Account;
    using Common.Web.Models.Payment;
    using Common.Web.Models.Shared;
    using Domain.Logging.Interfaces;
    using Microsoft.AspNet.Identity;
    using Models.Registration;
    using Services;
    using SessionFacade;

    public class BaseRegistrationController : BaseController
    {
        protected readonly Lazy<IBus> Bus;
        protected readonly Lazy<IContentApplicationService> ContentApplicationService;
        protected readonly Lazy<IPaymentMethodsApplicationService> PaymentMethodsApplicationService;
        protected readonly Lazy<IPaymentMethodAccountTypeApplicationService> PaymentMethodAccountTypeApplicationService;
        protected readonly Lazy<IPaymentMethodProviderTypeApplicationService> PaymentMethodProviderTypeApplicationService;
        protected readonly Lazy<ISecurityQuestionApplicationService> SecurityQuestionApplicationService;
        protected readonly Lazy<IGuarantorApplicationService> GuarantorApplicationService;
        protected readonly Lazy<ISsoApplicationService> SsoApplicationService;
        protected readonly Lazy<IVisitPayUserApplicationService> VisitPayUserApplicationService;
        protected readonly Lazy<IWebCookieFacade> WebCookie;
        protected readonly Lazy<IWebPatientSessionFacade> WebPatientSessionFacade;
        protected readonly Lazy<IRegistrationService> RegistrationService;

        public BaseRegistrationController(
            Lazy<IBaseControllerService> baseControllerService,
            Lazy<IBus> bus,
            Lazy<GuarantorFilterService> guarantorFilterService,
            Lazy<IContentApplicationService> contentApplicationService,
            Lazy<IGuarantorApplicationService> guarantorApplicationService,
            Lazy<IPaymentMethodsApplicationService> paymentMethodsApplicationService,
            Lazy<IPaymentMethodAccountTypeApplicationService> paymentMethodAccountTypeApplicationService,
            Lazy<IPaymentMethodProviderTypeApplicationService> paymentMethodProviderTypeApplicationService,
            Lazy<ISecurityQuestionApplicationService> securityQuestionApplicationService,
            Lazy<ISsoApplicationService> ssoApplicationService,
            Lazy<IVisitPayUserApplicationService> visitPayUserApplicationService,
            Lazy<IWebCookieFacade> webCookie,
            Lazy<IWebPatientSessionFacade> webPatientSessionFacade,
            Lazy<IRegistrationService> registrationService)
            : base(baseControllerService, guarantorFilterService)
        {
            this.Bus = bus;
            this.ContentApplicationService = contentApplicationService;
            this.GuarantorApplicationService = guarantorApplicationService;
            this.PaymentMethodsApplicationService = paymentMethodsApplicationService;
            this.PaymentMethodAccountTypeApplicationService = paymentMethodAccountTypeApplicationService;
            this.PaymentMethodProviderTypeApplicationService = paymentMethodProviderTypeApplicationService;
            this.SecurityQuestionApplicationService = securityQuestionApplicationService;
            this.SsoApplicationService = ssoApplicationService;
            this.VisitPayUserApplicationService = visitPayUserApplicationService;
            this.WebCookie = webCookie;
            this.WebPatientSessionFacade = webPatientSessionFacade;
            this.RegistrationService = registrationService;
        }

        const string RegistrationPaymentMethodsKey = "RegistrationPaymentMethods";

        #region registration base

        [HttpGet]
        [Common.Web.Attributes.SessionState(SessionStateBehavior.Required)]
        public virtual async Task<ActionResult> Index()
        {
            // clear payment methods from session
            this.PaymentMethods = null;

            //TODO: refactor this 
            CmsRegionEnum? instructionCmsRegion = null;
            SsoResponseDto ssoResponseDto = await this.GetRegistrableSsoResponseDto();
            if (ssoResponseDto != null)
            {
                instructionCmsRegion = await this.ContentApplicationService.Value.GetSsoProviderSpecificCmsRegionAsync(ssoResponseDto.ProviderEnum, SsoProviderSpecificCmsRegionEnum.RegistrationWelcomeInstruction);
            }

            RegistrationWelcomeViewModel model = new RegistrationWelcomeViewModel
            {
                CmsContent = Mapper.Map<CmsViewModel>(await this.ContentApplicationService.Value.GetCurrentVersionAsync(CmsRegionEnum.RegistrationWelcome)),
                CmsInstruction = Mapper.Map<CmsViewModel>(await this.ContentApplicationService.Value.GetCurrentVersionAsync(instructionCmsRegion.GetValueOrDefault(CmsRegionEnum.RegistrationWelcomeInstruction))),
            };



            string prefix = this.IsMobile ? MobileAreaPath : "";
            return this.View($"~/{prefix}/Views/Registration/Index.cshtml", model);
        }

        [HttpGet]
        public ActionResult Register()
        {
            this.Logger.Value.Info(() => $"{nameof(BaseAccountController)}::{nameof(Register)} - obsolete action, referrer: {this.Request.UrlReferrer?.ToString() ?? string.Empty}");
            return this.RedirectToActionPermanent("Index");
        }

        #endregion

        #region step - records

        [HttpGet]
        public PartialViewResult RegisterRecordsPartial()
        {
            return this.PartialView("~/Views/Registration/_RegisterStep1.cshtml", new RegisterPersonalInformationViewModel());
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<JsonResult> RegisterRecordsModel()
        {
            SsoResponseDto ssoResponseDto = await this.GetRegistrableSsoResponseDto();
            RegisterPersonalInformationViewModel model = this.RegistrationService.Value.GetRegisterPersonalInformationViewModel(ssoResponseDto != null);

            return this.Json(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<JsonResult> RegisterRecords(RegisterPersonalInformationViewModel model)
        {
            // validate
            this.ValidateRecords(this.ModelState, model);
            if (!this.ModelState.IsValid)
            {
                return this.Json(new ResultMessage<Hashtable>(false, this.ModelState.GetErrors()));
            }

            HttpContext context = System.Web.HttpContext.Current;

            // if sso, guarantorid won't be on the viewmodel
            string guarantorId = await this.GetGuarantorId(model.GuarantorId);
            string lastName = model.LastName.TrimNullSafe();
            string ssn4 = model.Ssn4.TrimNullSafe();
            string zip = model.Zip.TrimNullSafe();
            DateTime dateOfBirth = DateTimeHelper.FromValues(model.DateOfBirthMonth, model.DateOfBirthDay, model.DateOfBirthYear);
            DateTime? patientDateOfBirth = this.RegistrationService.Value.GetPatientDateOfBirth(model, dateOfBirth);

            MatchGuarantorDto guarantor = new MatchGuarantorDto()
            {
                SourceSystemKey = guarantorId,
                SSN4 = ssn4,
                FirstName = model.FirstName.TrimNullSafe(),
                LastName = lastName,
                DOB = dateOfBirth,
                AddressLine1 = model.AddressStreet1.TrimNullSafe(),
                City = model.City.TrimNullSafe(),
                StateProvince = model.State.TrimNullSafe(),
                PostalCode = zip,
            };
            MatchPatientDto patient = new MatchPatientDto
            {
                PatientDOB = patientDateOfBirth
            };

            MatchDataDto matchData = new MatchDataDto
            {
                Guarantor = guarantor,
                Patient = patient,
                ApplicationEnum = ApplicationEnum.VisitPay
            };

            // try to match
            GuarantorMatchResultEnum matchStatus = this.GuarantorApplicationService.Value.IsGuarantorMatch(matchData);

            string message = "";

            // todo: localize
            string guarantorMatchResultAlreadyRegistered = $"This account already exists in our system, try <a href=\"{this.Url.Action("Login", "Account")}\" title=\"Log In\">logging in</a>.";
            string guarantorMatchResultNoMatchFound = await this.ContentApplicationService.Value.GetTextRegionAsync(TextRegionConstants.GuarantorNoMatchFound);
            string guarantorMatchResultCanceledAccount = await this.ContentApplicationService.Value.GetTextRegionAsync(TextRegionConstants.GuarantorCanceledAccount);

            switch (matchStatus)
            {
                case GuarantorMatchResultEnum.AlreadyRegistered:
                    message = guarantorMatchResultAlreadyRegistered;
                    break;
                case GuarantorMatchResultEnum.NoMatchFound:
                    message = guarantorMatchResultNoMatchFound;
                    break;
                case GuarantorMatchResultEnum.Matched:
                    message = null;
                    break;
            }

            if (matchStatus == GuarantorMatchResultEnum.AlreadyRegistered || matchStatus == GuarantorMatchResultEnum.Matched)
            {
                bool credentialMatchCanceledAccount = this.GuarantorApplicationService.Value.CredentialsMatchCanceledAccount(matchData);
                if (credentialMatchCanceledAccount)
                {
                    message = guarantorMatchResultCanceledAccount;
                }
            }

            bool success = string.IsNullOrEmpty(message) && matchStatus == GuarantorMatchResultEnum.Matched;
            if (!success)
            {
                this.ModelState.AddModelError(string.Empty, message);
            }

            this.VisitPayUserJournalEventApplicationService.Value.LogRegistrationAuthentication(success, guarantorId, lastName, dateOfBirth, ssn4, zip, Mapper.Map<JournalEventHttpContextDto>(context));

            return this.Json(new ResultMessage<Hashtable>(success, success ? null : this.ModelState.GetErrors()));
        }

        #endregion

        #region step - account

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult RegisterAccountModel()
        {
            RegisterAccountViewModel model = new RegisterAccountViewModel();
            if (this.IsFeatureEnabled(VisitPayFeatureEnum.GuarantorSecurityQuestionsIsEnabled))
            {
                model.SecurityQuestions = Enumerable.Range(0, this.ClientDto.GuarantorSecurityQuestionsCount).Select(x => new SecurityQuestionViewModel()).ToList();
            }
            return this.Json(model);
        }

        [HttpGet]
        public PartialViewResult RegisterAccountPartial()
        {
            RegisterAccountViewModel model = new RegisterAccountViewModel();

            // security questions 
            if (this.IsFeatureEnabled(VisitPayFeatureEnum.GuarantorSecurityQuestionsIsEnabled))
            {
                List<SecurityQuestionDto> allQuestions = this.SecurityQuestionApplicationService.Value.GetAllSecurityQuestions().ToList();
                List<SelectListItem> listQuestions = new List<SelectListItem>();
                allQuestions.ForEach(x => listQuestions.Add(new SelectListItem
                {
                    Text = x.Question,
                    Value = x.SecurityQuestionId.ToString()
                }));

                for (int i = 0; i < this.ClientDto.GuarantorSecurityQuestionsCount; i++)
                {
                    model.SecurityQuestions.Add(new SecurityQuestionViewModel
                    {
                        SecurityQuestions = listQuestions
                    });
                }
            }

            return this.PartialView("~/Views/Registration/_RegisterStep2.cshtml", model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<JsonResult> RegisterAccount(RegisterAccountViewModel model)
        {
            await this.ValidateAccount(this.ModelState, model);

            bool isValid = this.ModelState.IsValid;

            return this.Json(new ResultMessage<Hashtable>(isValid, isValid ? null : this.ModelState.GetErrors()));
        }

        #endregion

        #region step - terms

        [HttpGet]
        public PartialViewResult RegisterTermsPartial()
        {
            return this.PartialView("~/Views/Registration/_RegisterStep3.cshtml", new RegisterTermsViewModel());
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<JsonResult> RegisterTermsModel()
        {
            CmsVersionDto esignDto = await this.ContentApplicationService.Value.GetCurrentVersionAsync(CmsRegionEnum.EsignTerms, false);
            CmsVersionDto termsAndConditionsDto = await this.ContentApplicationService.Value.GetCurrentVersionAsync(CmsRegionEnum.VppTermsAndConditions, false);

            RegisterTermsViewModel model = new RegisterTermsViewModel
            {
                EsignTermsCmsVersionId = esignDto.CmsVersionId,
                TermsOfUseCmsVersionId = termsAndConditionsDto.CmsVersionId
            };

            return this.Json(model);
        }

        #endregion

        #region process

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Common.Web.Attributes.SessionState(SessionStateBehavior.Required)]
        public async Task<JsonResult> Register(RegisterViewModel model)
        {
            this.ValidateRecords(this.ModelState, model.PersonalInformation);
            await this.ValidateAccount(this.ModelState, model.Account);
            this.ValidateTerms(this.ModelState, model.Terms);

            if (!this.ModelState.IsValid)
            {
                return this.Json(new ResultMessage<Hashtable>(false, this.ModelState.GetErrors()));
            }

            HttpContext context = System.Web.HttpContext.Current;

            string addressStreet1 = model.PersonalInformation.AddressStreet1.TrimNullSafe();
            string city = model.PersonalInformation.City.TrimNullSafe();
            string state = model.PersonalInformation.State.TrimNullSafe();
            string zip = model.PersonalInformation.Zip.TrimNullSafe();

            string mailingAddressStreet1 = model.PersonalInformation.MailingAddressStreet1.TrimNullSafe();
            string mailingAddressStreet2 = model.PersonalInformation.MailingAddressStreet2.TrimNullSafe();
            string mailingCity = model.PersonalInformation.MailingCity.TrimNullSafe();
            string mailingState = model.PersonalInformation.MailingState.TrimNullSafe();
            string mailingZip = model.PersonalInformation.MailingZip.TrimNullSafe();

            string email = model.Account.Email.TrimNullSafe();
            string firstName = model.PersonalInformation.FirstName.TrimNullSafe();
            string lastName = model.PersonalInformation.LastName.TrimNullSafe();
            string ssn4 = model.PersonalInformation.Ssn4.TrimNullSafe();
            string username = model.Account.Username.TrimNullSafe();

            string password = model.Account.Password.TrimNullSafe();
            DateTime dateOfBirth = DateTimeHelper.FromValues(model.PersonalInformation.DateOfBirthMonth, model.PersonalInformation.DateOfBirthDay, model.PersonalInformation.DateOfBirthYear);
            DateTime? patientDateOfBirth = this.RegistrationService.Value.GetPatientDateOfBirth(model.PersonalInformation, dateOfBirth);

            VisitPayUserDto userDto = new VisitPayUserDto
            {
                AddressStreet1 = addressStreet1,
                AddressStreet2 = null,
                City = city,
                State = state,
                Zip = zip,
                MailingAddressStreet1 = mailingAddressStreet1,
                MailingAddressStreet2 = mailingAddressStreet2,
                MailingCity = mailingCity,
                MailingState = mailingState,
                MailingZip = mailingZip,
                DateOfBirth = dateOfBirth,
                Email = email,
                FirstName = firstName,
                LastName = lastName,
                MiddleName = null,
                PhoneNumber = null,
                PhoneNumberType = null,
                PhoneNumberSecondary = null,
                PhoneNumberSecondaryType = null,
                SSN4 = string.IsNullOrWhiteSpace(ssn4) ? (int?)null : Convert.ToInt32(ssn4),
                UserName = username
            };

            List<SecurityQuestionAnswerDto> answerDtos = Mapper.Map<List<SecurityQuestionAnswerDto>>(model.Account.SecurityQuestions);

            string guarantorId = await this.GetGuarantorId(model.PersonalInformation.GuarantorId);

            IdentityResult identityResult = this.VisitPayUserApplicationService.Value.CreatePatient(
                user: userDto,
                password: password,
                registrationMatchString: guarantorId,
                termsOfUseId: model.Terms.TermsOfUseCmsVersionId,
                esignTermsId: model.Terms.EsignTermsCmsVersionId,
                paymentDueDay: null,
                securityQuestionAnswerDtos: answerDtos,
                guarantorPasswordExpLimitInDays: this.ClientDto.GuarantorPasswordExpLimitInDays,
                patientDateOfBirth: patientDateOfBirth);

            if (!identityResult.Succeeded)
            {
                this.ModelState.AddModelError(string.Empty, this.GenericErrorMessage);
                return this.Json(new ResultMessage<Hashtable>(false, this.ModelState.GetErrors()));
            }

            string referrer = this.WebPatientSessionFacade.Value.ReferringUrl;

            VisitPayUserDto createdUserDto = this.VisitPayUserApplicationService.Value.FindByName(userDto.UserName);
            int vpGuarantorId = this.VisitPayUserApplicationService.Value.GetGuarantorIdFromVisitPayUserId(createdUserDto.VisitPayUserId);

            this.PaymentMethodsApplicationService.Value.PersistPaymentMethodsFromRegistration(this.PaymentMethods, createdUserDto.VisitPayUserId, vpGuarantorId);
            this.PaymentMethods = null;

            if (model.PersonalInformation.IsSso)
            {
                SsoResponseDto ssoResponseDto = await this.GetRegistrableSsoResponseDto();

                this.VisitPayUserJournalEventApplicationService.Value.LogRegistrationCompleteSso(
                    ssoResponseDto.ProviderEnum,
                    createdUserDto.VisitPayUserId,
                    vpGuarantorId,
                    guarantorId,
                    lastName,
                    dateOfBirth,
                    ssn4,
                    zip,
                    Mapper.Map<JournalEventHttpContextDto>(context));

                this.MetricsProvider.Value.Increment(Metrics.Increment.Sso.Registration);
            }
            else
            {
                this.VisitPayUserJournalEventApplicationService.Value.LogRegistrationComplete(
                    createdUserDto.VisitPayUserId,
                    vpGuarantorId,
                    guarantorId,
                    lastName,
                    dateOfBirth,
                    ssn4,
                    zip,
                    Mapper.Map<JournalEventHttpContextDto>(context));
            }

            this.PublishMessagesForUiNotifications(createdUserDto.VisitPayUserId);

            await this.SignInAfterRegistrationAsync(model);

            return this.Json(new ResultMessage<string[]>(true, new[]
            {
                this.ClientDto.GuarantorEnrollmentProviderDelaySeconds.ToString(),
                referrer
            }));
        }

        #endregion

        private void PublishMessagesForUiNotifications(int visitPayUserId)
        {
            AddSystemMessageVisitPayUserMessageList messages = new AddSystemMessageVisitPayUserMessageList
            {
                Messages = new List<AddSystemMessageVisitPayUserMessage>()
            };
            messages.Messages.Add(new AddSystemMessageVisitPayUserMessage { VisitPayUserId = visitPayUserId, SystemMessageEnum = SystemMessageEnum.WelcomeMessage });
            messages.Messages.Add(new AddSystemMessageVisitPayUserMessage { VisitPayUserId = visitPayUserId, SystemMessageEnum = SystemMessageEnum.Sms });

            if (this.FeatureApplicationService.Value.IsFeatureEnabled(VisitPayFeatureEnum.MyChartSso))
            {
                messages.Messages.Add(new AddSystemMessageVisitPayUserMessage { VisitPayUserId = visitPayUserId, SystemMessageEnum = SystemMessageEnum.Sso });
            }
            if (this.FeatureApplicationService.Value.IsFeatureEnabled(VisitPayFeatureEnum.PaymentSummary))
            {
                messages.Messages.Add(new AddSystemMessageVisitPayUserMessage { VisitPayUserId = visitPayUserId, SystemMessageEnum = SystemMessageEnum.PaymentSummaryNotification });
            }
            this.Bus.Value.PublishMessage(messages).Wait();

            this.Bus.Value.PublishMessage(new VisitPayUserAddressChangedMessage { VisitPayUserId = visitPayUserId }).Wait();
        }

        #region summary

        [HttpGet]
        [RequiresSsoAuthorization(Enabled = false)]
        public async Task<PartialViewResult> SummaryPartial()
        {
            string userId = this.User.Identity.GetUserId();
            VisitPayUserDto user = this.VisitPayUserApplicationService.Value.FindById(userId);

            RegistrationSummaryViewModel model = new RegistrationSummaryViewModel
            {
                SummaryText = Mapper.Map<CmsViewModel>(await this.ContentApplicationService.Value.GetCurrentVersionAsync(CmsRegionEnum.RegistrationThankYou, additionalValues: new Dictionary<string, string>
                {
                    {"[[VisitPayUser_UserName]]", user.UserName},
                    {"[[VisitPayUser_Email]]", user.Email}
                }).ConfigureAwait(true))
            };

            if (this.User?.Identity is ClaimsIdentity && ((ClaimsIdentity)this.User.Identity).HasClaim(c => c.Type == ClaimTypeEnum.SsoProvider.ToString()))
            {
                SsoProviderEnum ssoProviderEnum;
                if (Enum.TryParse(((ClaimsIdentity)this.User.Identity).FindFirst(ClaimTypeEnum.SsoProvider.ToString()).Value, out ssoProviderEnum))
                {
                    if (ssoProviderEnum != SsoProviderEnum.Unknown)
                    {
                        // this was an sso registration
                        SsoProviderDto ssoProvider = this.SsoApplicationService.Value.GetProvider(ssoProviderEnum);
                        if (ssoProvider != null)
                        {
                            model.SsoText = Mapper.Map<CmsViewModel>(await this.ContentApplicationService.Value.GetCurrentVersionAsync(CmsRegionEnum.RegistrationThankYouSso, true, new Dictionary<string, string>
                            {
                                {"[[SsoProviderDisplayName]]", ssoProvider.SsoProviderDisplayName}
                            }).ConfigureAwait(true));
                        }
                    }
                }
            }
            return this.PartialView($"~/Views/Registration/_Summary.cshtml", model);
        }

        [HttpGet]
        [RequiresSsoAuthorization(Enabled = false)]
        public ActionResult SummaryHasStatement()
        {
            int visitPayUserId = this.CurrentUserId;
            bool hasStatement = this.RegistrationService.Value.HasStatement(visitPayUserId);

            if (hasStatement)
            {
                return this.Json(new ResultMessage(hasStatement, "Statement Created"), JsonRequestBehavior.AllowGet);
            }
            else
            {
                return new HttpStatusCodeResult(HttpStatusCode.NoContent);
            }
        }

        #endregion

        protected void ValidateRecords(ModelStateDictionary modelState, RegisterPersonalInformationViewModel model)
        {
            this.TryValidateModel(model);
            this.RegistrationService.Value.ValidateRecords(modelState, model);
        }

        private async Task ValidateAccount(ModelStateDictionary modelState, RegisterAccountViewModel model)
        {
            this.TryValidateModel(model);

            string username = model.Username.TrimNullSafe();
            if (!this.VisitPayUserApplicationService.Value.IsUsernameValid(username))
            {
                string usernameRequirements = await this.VisitPayUserApplicationService.Value.GetGuarantorUsernameRequirements();
                modelState.AddModelError(nameof(RegisterAccountViewModel.Username), usernameRequirements);
            }
            else
            {
                bool isUsernameValid = this.VisitPayUserApplicationService.Value.IsUsernameAvailable(username);
                if (!isUsernameValid)
                {
                    modelState.AddModelError(nameof(RegisterAccountViewModel.Username), $"The username selected is already registered. Please enter a new username or if you think you already have an account with us, try <a href=\"{this.Url.Action("Login", "Account")}\" title=\"Log In\">logging in</a>.");
                }
            }

            bool isPasswordValid = (await this.VisitPayUserApplicationService.Value.IsPasswordStrongEnough(model.Password.TrimNullSafe())).Succeeded;
            if (!isPasswordValid)
            {
                string guarantorPasswordRequirements = await this.VisitPayUserApplicationService.Value.GetGuarantorPasswordRequirements();
                modelState.AddModelError(nameof(RegisterAccountViewModel.Password), guarantorPasswordRequirements);
            }
        }

        private void ValidateTerms(ModelStateDictionary modelState, RegisterTermsViewModel model)
        {
            this.TryValidateModel(model);
        }

        private async Task<SsoResponseDto> GetRegistrableSsoResponseDto()
        {
            SsoResponseDto ssoResponse = await this.WebCookie.Value.GetSsoResponseDtoAsync(System.Web.HttpContext.Current);
            if (ssoResponse != null &&
                ssoResponse.FoundMatchesThatCouldBeRegistered &&
                ssoResponse.MatchesThatCouldBeRegistered != null &&
                ssoResponse.MatchesThatCouldBeRegistered.Any())
            {
                return ssoResponse;
            }

            return null;
        }

        protected async Task<string> GetGuarantorId(string[] guarantorIdParts)
        {
            string guarantorId = string.Join("_", guarantorIdParts).TrimNullSafe();

            if (!string.IsNullOrEmpty(guarantorId))
            {
                return guarantorId;
            }

            SsoResponseDto ssoResponseDto = await this.GetRegistrableSsoResponseDto();
            return ssoResponseDto?.MatchesThatCouldBeRegistered.First().HsGuarantorSourceSystemKey;
        }

        private async Task SignInAfterRegistrationAsync(RegisterViewModel model)
        {
            SsoPasswordSignInRequest passwordSigninRequest = new SsoPasswordSignInRequest
            {
                Password = model.Account.Password.TrimNullSafe(),
                UserName = model.Account.Username.TrimNullSafe()
            };

            SsoResponseDto ssoResponseDto = await this.GetRegistrableSsoResponseDto();
            if (ssoResponseDto != null && ssoResponseDto.FoundMatchesThatCouldBeRegistered)
            {
                passwordSigninRequest.SsoResponse = ssoResponseDto;
            }

            SignInStatusExEnum result = await this.SsoApplicationService.Value.PasswordSignInAsync(Mapper.Map<HttpContextDto>(System.Web.HttpContext.Current), passwordSigninRequest);
            await this.BruteForceDelayAsync(System.Web.HttpContext.Current.Request.UserHostAddress, () => result == SignInStatusExEnum.Failure, () => result == SignInStatusExEnum.Success);
        }

        #region payment methods

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult GetPaymentMethodsList()
        {
            return this.Json(this.MapPaymentMethodsFromSession());
        }

        [HttpGet]
        public PartialViewResult PaymentMethodBank(BankAccountViewModel model)
        {
            return this.PartialView("~/Views/Payment/_PaymentMethodBankAccount.cshtml", new BankAccountViewModel());
        }

        [HttpGet]
        public PartialViewResult PaymentMethodCard(CardAccountViewModel model)
        {
            IList<PaymentMethodAccountTypeDto> paymentMethodAccountTypes = this.PaymentMethodAccountTypeApplicationService.Value.GetPaymentMethodAccountTypes();
            IList<PaymentMethodProviderTypeDto> paymentMethodProviderTypes = this.PaymentMethodProviderTypeApplicationService.Value.GetPaymentMethodProviderTypes();
            return this.PartialView("~/Views/Payment/_PaymentMethodCardAccount.cshtml", new CardAccountViewModel
            {
                PaymentMethodAccountTypes = Mapper.Map<IList<PaymentMethodAccountTypeViewModel>>(paymentMethodAccountTypes),
                PaymentMethodProviderTypes = Mapper.Map<IList<PaymentMethodProviderTypeViewModel>>(paymentMethodProviderTypes)
            });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult PaymentMethod(int paymentMethodId, bool isNewAch = false)
        {
            PaymentMethodDto paymentMethodDto = new PaymentMethodDto
            {
                IsActive = true
            };

            bool hasExistingPrimary = true;

            if (paymentMethodId > 0)
            {
                paymentMethodDto = this.GetPaymentMethodByIdFromSession(paymentMethodId);
            }
            else
            {
                hasExistingPrimary = this.MapPaymentMethodsFromSession().AllAccounts.Count > 0;
            }

            if (paymentMethodDto.IsAchType || paymentMethodId == 0 && isNewAch)
            {
                BankAccountViewModel bankAccountViewModel = Mapper.Map<BankAccountViewModel>(paymentMethodDto);
                bankAccountViewModel.HasExistingPrimary = hasExistingPrimary;
                bankAccountViewModel.IsPrimary = paymentMethodDto.IsPrimary || !hasExistingPrimary;

                return this.Json(bankAccountViewModel);
            }

            IList<PaymentMethodAccountTypeDto> paymentMethodAccountTypes = this.PaymentMethodAccountTypeApplicationService.Value.GetPaymentMethodAccountTypes();
            IList<PaymentMethodProviderTypeDto> paymentMethodProviderTypes = this.PaymentMethodProviderTypeApplicationService.Value.GetPaymentMethodProviderTypes();

            CardAccountViewModel cardAccountViewModel = Mapper.Map<CardAccountViewModel>(paymentMethodDto);
            cardAccountViewModel.HasExistingPrimary = hasExistingPrimary;
            cardAccountViewModel.IsPrimary = paymentMethodDto.IsPrimary || !hasExistingPrimary;
            cardAccountViewModel.PaymentMethodAccountTypes = Mapper.Map<IList<PaymentMethodAccountTypeViewModel>>(paymentMethodAccountTypes);
            cardAccountViewModel.PaymentMethodProviderTypes = Mapper.Map<IList<PaymentMethodProviderTypeViewModel>>(paymentMethodProviderTypes);

            if (paymentMethodDto.PaymentMethodAccountType != null)
            {
                cardAccountViewModel.AccountTypeId = paymentMethodDto.PaymentMethodAccountType.PaymentMethodAccountTypeId;
            }
            if (paymentMethodDto.PaymentMethodProviderType != null)
            {
                cardAccountViewModel.ProviderTypeId = paymentMethodDto.PaymentMethodProviderType.PaymentMethodProviderTypeId;
            }

            return this.Json(cardAccountViewModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Common.Web.Attributes.SessionState(SessionStateBehavior.Required)]
        public async Task<JsonResult> SavePaymentMethodBank(BankAccountViewModel model)
        {
            HttpContext context = System.Web.HttpContext.Current;

            if (!this.ModelState.IsValid)
            {
                return this.Json(new PaymentMethodResultViewModel(false, this.GenericErrorMessage));
            }

            if (!model.IsPrimary && !this.HasPrimaryPaymentMethod())
            {
                return this.Json(new PaymentMethodResultViewModel(false, "You must make an account primary."));
            }

            PaymentMethodDto paymentMethodDto = model.PaymentMethodId > 0 ? this.GetPaymentMethodByIdFromSession(model.PaymentMethodId) : new PaymentMethodDto { PaymentMethodId = (this.PaymentMethods.Max(x => x.PaymentMethodId) + 1).GetValueOrDefault(1) };

            paymentMethodDto.AccountNickName = model.AccountNickName;
            paymentMethodDto.BankName = model.BankName;
            paymentMethodDto.FirstName = model.FirstName;
            paymentMethodDto.LastName = model.LastName;
            paymentMethodDto.IsAchType = true;
            paymentMethodDto.IsActive = true;
            paymentMethodDto.IsPrimary = false;
            paymentMethodDto.PaymentMethodType = (PaymentMethodTypeEnum)model.PaymentMethodTypeId;

            PaymentMethodResultDto resultDto = await this.SavePaymentMethodToSessionAsync(this.PaymentMethodsApplicationService.Value.ProcessBankPaymentMethodRegistrationAsync, paymentMethodDto, model.IsPrimary, model.AccountNumber, model.RoutingNumber, Mapper.Map<JournalEventHttpContextDto>(context)).ConfigureAwait(true);

            return this.Json(Mapper.Map<PaymentMethodResultViewModel>(resultDto));
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Common.Web.Attributes.SessionState(SessionStateBehavior.Required)]
        public JsonResult SavePaymentMethodCard(CardAccountViewModel model)
        {
            if (!this.ModelState.IsValid)
            {
                return this.Json(new PaymentMethodResultViewModel(false, this.GenericErrorMessage));
            }

            if (!model.IsPrimary && !this.HasPrimaryPaymentMethod())
            {
                return this.Json(new PaymentMethodResultViewModel(false, "You must make an account primary."));
            }

            PaymentMethodDto paymentMethodDto = model.PaymentMethodId > 0 ? this.GetPaymentMethodByIdFromSession(model.PaymentMethodId) : new PaymentMethodDto { PaymentMethodId = (this.PaymentMethods.Max(x => x.PaymentMethodId) + 1).GetValueOrDefault(1) };

            paymentMethodDto.AccountNickName = model.AccountNickName;
            paymentMethodDto.ExpDate = $"{model.ExpirationMonth.ToString().PadLeft(2, '0')}{model.ExpirationYear}";
            paymentMethodDto.IsCardType = true;
            paymentMethodDto.IsActive = true;
            paymentMethodDto.IsPrimary = false;
            paymentMethodDto.NameOnCard = model.NameOnCard;
            paymentMethodDto.PaymentMethodType = (PaymentMethodTypeEnum)model.PaymentMethodTypeId;
            paymentMethodDto.AccountTypeId = model.AccountTypeId;
            paymentMethodDto.ProviderTypeId = model.ProviderTypeId;

            if (!paymentMethodDto.BillingAddresses.Any())
            {
                paymentMethodDto.BillingAddresses.Add(new PaymentMethodBillingAddressDto());
            }

            PaymentMethodBillingAddressDto billingAddressDto = paymentMethodDto.BillingAddresses.First();
            billingAddressDto.Address1 = model.AddressLine1;
            billingAddressDto.Address2 = model.AddressLine2;
            billingAddressDto.City = model.City;
            billingAddressDto.State = model.State;
            billingAddressDto.Zip = model.Zip;

            PaymentMethodResultDto resultDto = this.SaveCardPaymentMethodToSession(System.Web.HttpContext.Current, paymentMethodDto, model.IsPrimary, model.BillingId, model.GatewayToken, model.CardCode, model.LastFour);

            return this.Json(Mapper.Map<PaymentMethodResultViewModel>(resultDto));
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Common.Web.Attributes.SessionState(SessionStateBehavior.Required)]
        public JsonResult RemovePaymentMethod(int paymentMethodId)
        {
            bool result = false;

            PaymentMethodDto paymentMethodDto = this.GetPaymentMethodByIdFromSession(paymentMethodId);
            if (paymentMethodDto != null)
            {
                result = this.RemovePaymentMethodFromSession(paymentMethodDto);
            }

            return this.Json(new PaymentMethodResultViewModel(result, result ? "" : this.GenericErrorMessage));
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Common.Web.Attributes.SessionState(SessionStateBehavior.Required)]
        public JsonResult SetPrimaryPaymentMethod(int paymentMethodId)
        {
            bool result = false;

            PaymentMethodDto paymentMethodDto = this.GetPaymentMethodByIdFromSession(paymentMethodId);
            if (paymentMethodDto != null)
            {
                result = this.SetPrimaryPaymentMethodInSession(paymentMethodDto);
            }

            return this.Json(new PaymentMethodResultViewModel(result, result ? "" : this.GenericErrorMessage));
        }

        private List<PaymentMethodDto> PaymentMethods
        {
            get
            {
                object sessionObj = this.Session[RegistrationPaymentMethodsKey];
                if (sessionObj != null)
                {
                    return (List<PaymentMethodDto>)sessionObj;
                }

                return new List<PaymentMethodDto>();
            }
            set
            {
                this.WebPatientSessionFacade.Value.SetSessionValue(RegistrationPaymentMethodsKey, value);
            }
        }

        private PaymentMethodsViewModel MapPaymentMethodsFromSession()
        {
            CmsVersionDto hsaDisclaimer = this.ContentApplicationService.Value.GetCurrentVersionAsync(CmsRegionEnum.HsaInterestDisclaimer).Result;
            string removePrompt = this.ContentApplicationService.Value.GetTextRegionAsync(TextRegionConstants.RemovePaymentMethodPrompt).Result;
            string removeTitle = this.ContentApplicationService.Value.GetTextRegionAsync(TextRegionConstants.RemovePaymentMethod).Result;
            string primaryChangedText = this.ContentApplicationService.Value.GetTextRegionAsync(TextRegionConstants.SelectedNewPrimary).Result;
            string primaryChangedTitle = this.ContentApplicationService.Value.GetTextRegionAsync(TextRegionConstants.PrimaryPaymentMethodChanged).Result;

            PaymentMethodsViewModel model = new PaymentMethodsViewModel
            {
                AllAccounts = Mapper.Map<IList<PaymentMethodListItemViewModel>>(this.PaymentMethods.OrderBy(x => x.DisplayName).ThenBy(x => x.LastFour)),
                AccountRemoveCms = new CmsViewModel
                {
                    ContentBody = removePrompt,
                    ContentTitle = removeTitle
                },
                HsaInterestDisclaimerCms = Mapper.Map<CmsViewModel>(hsaDisclaimer),
                PrimaryChangedCms = new CmsViewModel
                {
                    ContentBody = primaryChangedText,
                    ContentTitle = primaryChangedTitle
                }
            };

            foreach (PaymentMethodListItemViewModel account in model.AllAccounts)
            {
                account.IsRemoveable = model.AllAccounts.Count == 1 || !account.IsPrimary;
                account.RemoveableMessage = account.IsRemoveable ? null : "Primary payment method cannot be deleted.";
            }

            return model;
        }

        private PaymentMethodDto GetPaymentMethodByIdFromSession(int paymentMethodId)
        {
            PaymentMethodDto paymentMethodDto = this.PaymentMethods.FirstOrDefault(x => x.PaymentMethodId == paymentMethodId);
            if (paymentMethodDto != null)
            {
                paymentMethodDto.IsRemoveable = true;
            }
            return paymentMethodDto;
        }

        private bool RemovePaymentMethodFromSession(PaymentMethodDto paymentMethodDto)
        {
            if (paymentMethodDto == null)
            {
                return false;
            }

            PaymentMethodDto paymentMethod = this.PaymentMethods.FirstOrDefault(x => x.PaymentMethodId == paymentMethodDto.PaymentMethodId);
            if (paymentMethod == null)
            {
                return false;
            }

            List<PaymentMethodDto> paymentMethods = this.PaymentMethods;
            paymentMethods.Remove(paymentMethod);

            this.PaymentMethods = paymentMethods;

            return true;
        }

        private PaymentMethodResultDto SaveCardPaymentMethodToSession(HttpContext context, PaymentMethodDto paymentMethodDto, bool isPrimary, string billingId, string gatewayToken, string cardCode, string lastFour)
        {
            // process against TC without persisting to database
            PaymentMethodResultDto paymentMethodResult = this.PaymentMethodsApplicationService.Value.ProcessCardPaymentMethodRegistration(Mapper.Map<HttpContextDto>(context), paymentMethodDto, billingId, gatewayToken, cardCode, lastFour);
            if (paymentMethodResult.IsError)
            {
                if (paymentMethodDto.PaymentMethodId.GetValueOrDefault(0) == 0)
                {
                    this.RemovePaymentMethodFromSession(paymentMethodDto);
                }
                return paymentMethodResult;
            }

            this.RemovePaymentMethodFromSession(this.PaymentMethods.FirstOrDefault(x => x.PaymentMethodId == paymentMethodDto.PaymentMethodId));
            List<PaymentMethodDto> paymentMethods = this.PaymentMethods;
            paymentMethods.Add(paymentMethodDto);

            this.PaymentMethods = paymentMethods;

            if (isPrimary)
            {
                this.SetPrimaryPaymentMethodInSession(paymentMethodDto);
            }

            return paymentMethodResult;
        }

        private async Task<PaymentMethodResultDto> SavePaymentMethodToSessionAsync(Func<JournalEventHttpContextDto, PaymentMethodDto, string, string, Task<PaymentMethodResultDto>> saveFunc, PaymentMethodDto paymentMethodDto, bool isPrimary, string cardNumber, string cardCode, JournalEventHttpContextDto context)
        {
            // process against TC without persisting to database
            PaymentMethodResultDto paymentMethodResult = await saveFunc(context, paymentMethodDto, cardNumber, cardCode);
            if (paymentMethodResult.IsError)
            {
                if (paymentMethodDto.PaymentMethodId.GetValueOrDefault(0) == 0)
                {
                    this.RemovePaymentMethodFromSession(paymentMethodDto);
                }
                return paymentMethodResult;
            }

            this.RemovePaymentMethodFromSession(this.PaymentMethods.FirstOrDefault(x => x.PaymentMethodId == paymentMethodDto.PaymentMethodId));
            List<PaymentMethodDto> paymentMethods = this.PaymentMethods;
            paymentMethods.Add(paymentMethodDto);

            this.PaymentMethods = paymentMethods;

            if (isPrimary)
            {
                this.SetPrimaryPaymentMethodInSession(paymentMethodDto);
            }

            return paymentMethodResult;
        }

        private bool SetPrimaryPaymentMethodInSession(PaymentMethodDto paymentMethodDto)
        {
            List<PaymentMethodDto> paymentMethods = this.PaymentMethods;

            PaymentMethodDto paymentMethod = paymentMethods.FirstOrDefault(x => x.PaymentMethodId == paymentMethodDto.PaymentMethodId);
            if (paymentMethod == null)
            {
                return false;
            }

            paymentMethods.ForEach(x => x.IsPrimary = false);
            paymentMethod.IsPrimary = true;

            this.PaymentMethods = paymentMethods;

            return true;
        }

        private bool HasPrimaryPaymentMethod()
        {
            return this.PaymentMethods.Count(x => x.IsPrimary) > 0;
        }

        #endregion
    }
}
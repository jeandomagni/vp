﻿namespace Ivh.Web.Patient.Controllers
{
    using System;
    using System.Web.Mvc;
    using Attributes;
    using Common.VisitPay.Strings;
    using Common.Web.Interfaces;
    using Services;

    [GuarantorAuthorize(Roles = VisitPayRoleStrings.System.Patient + "," + VisitPayRoleStrings.Admin.Administrator)]
    public class ManageController : BaseController
    {
        public ManageController(
            Lazy<IBaseControllerService> baseControllerService,
            Lazy<GuarantorFilterService> guarantorFilterService)
            : base(baseControllerService, guarantorFilterService)
        {
        }

        [AllowAnonymous]
        public ActionResult Index()
        {
            return this.RedirectToActionPermanent("Login", "Account");
        }

        [AllowAnonymous]
        public ActionResult Login()
        {
            return this.RedirectToActionPermanent("Login", "Account");
        }

        [AllowAnonymous]
        public ActionResult Enroll()
        {
            return this.RedirectToActionPermanent("Index", "Registration");
        }

        [AllowAnonymous]
        public ActionResult LearnMore()
        {
            return this.RedirectToActionPermanent("LearnMore", "Account");
        }

        [AllowAnonymous]
        public ActionResult Faq()
        {
            return this.RedirectToActionPermanent("Faqs", "Account");
        }

        [AllowAnonymous]
        public ActionResult Contact()
        {
            return this.RedirectToActionPermanent("ContactUs", "Account");
        }
    }
}
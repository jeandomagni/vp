﻿namespace Ivh.Web.Patient.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using System.Web;
    using System.Web.Mvc;
    using System.Web.SessionState;
    using Application.Content.Common.Interfaces;
    using Application.Core.Common.Dtos;
    using Application.Core.Common.Interfaces;
    using Application.FinanceManagement.Common.Interfaces;
    using Application.SecureCommunication.Common.Dtos;
    using Application.SecureCommunication.Common.Interfaces;
    using Attributes;
    using AutoMapper;
    using Common.VisitPay.Enums;
    using Common.VisitPay.Strings;
    using Common.Web.Controllers;
    using Common.Web.Filters;
    using Common.Web.Interfaces;
    using Common.Web.Models;
    using Common.Web.Models.Support;
    using Models.Support;
    using Services;

    [GuarantorAuthorize(Roles = VisitPayRoleStrings.System.Patient + "," + VisitPayRoleStrings.Admin.Administrator)]
    public class SupportController : BaseSupportController
    {
        private readonly Lazy<GuarantorFilterService> _guarantorFilterService;
        
        public SupportController(
            Lazy<IBaseControllerService> baseControllerService,
            Lazy<IContentApplicationService> contentApplicationService,
            Lazy<IConsolidationGuarantorApplicationService> consolidationGuarantorApplicationService,
            Lazy<ISupportRequestApplicationService> supportRequestApplicationService,
            Lazy<ISupportTopicApplicationService> supportTopicApplicationService,
            Lazy<ITreatmentLocationApplicationService> treatmentLocationApplicationService,
            Lazy<IVisitApplicationService> visitApplicationService,
            Lazy<IVisitPayUserApplicationService> visitPayUserApplicationService,
            Lazy<IVisitPayUserJournalEventApplicationService> visitPayUserJournalEventApplicationService,
            Lazy<GuarantorFilterService> guarantorFilterService
            )
            : base(
            baseControllerService,
            contentApplicationService,
            consolidationGuarantorApplicationService,
            supportRequestApplicationService,
            supportTopicApplicationService,
            treatmentLocationApplicationService,
            visitApplicationService,
            visitPayUserApplicationService,
            visitPayUserJournalEventApplicationService)
        {
            this._guarantorFilterService = guarantorFilterService;
        }

        #region support request list

        [HttpGet]
        public ActionResult MySupportRequests()
        {
            return this.View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [OverrideAuthorization, EmulateUser(AllowPost = true)]
        public JsonResult MySupportRequests(GridSearchModel<SupportRequestSearchFilterViewModel> filterModel)
        {
            SupportRequestFilterDto supportRequestFilterDto = new SupportRequestFilterDto
            {
                SortField = filterModel.Sidx,
                SortOrder = filterModel.Sord,
                SupportRequestStatus = filterModel.Model.SupportRequestStatus
            };

            SupportRequestResultsDto supportRequestResultsDto = this.SupportRequestApplicationService.Value.GetSupportRequests(supportRequestFilterDto, filterModel.Page, filterModel.Rows, this.CurrentUserId);

            //For patient, don't ever return attachments that have had malware detected. Don't want to show them in the list.
            foreach (SupportRequestDto supportRequest in supportRequestResultsDto.SupportRequests)
            {
                foreach (SupportRequestMessageDto msg in supportRequest.GuarantorMessages)
                {
                    msg.SupportRequestMessageAttachments = msg.SupportRequestMessageAttachments.Where(a => MalwareScanStatusEnum.Detected != a.MalwareScanStatus).ToList();
                    msg.ActiveSupportRequestMessageAttachments = msg.ActiveSupportRequestMessageAttachments.Where(a => MalwareScanStatusEnum.Detected != a.MalwareScanStatus).ToList();
                }
            }

            return this.Json(new SupportRequestResultsViewModel(filterModel.Page, filterModel.Rows, supportRequestResultsDto.TotalRecords)
            {
                Results = Mapper.Map<List<SupportRequestResultViewModel>>(supportRequestResultsDto.SupportRequests)
            });
        }
        
        [HttpGet]
        public ActionResult MyContactUs()
        {
            if (this.IsFeatureEnabled(VisitPayFeatureEnum.FeatureFacilityContactUsGridIsEnabled))
            {
                IList<FacilityDto> facilities = this.VisitApplicationService.Value.GetUniqueFacilityVisits(this._guarantorFilterService.Value.VpGuarantorId)
                    .OrderBy(x => x.FacilityDescription)
                    .ToList();

                ContactUsResultsViewModel contactUsModel = new ContactUsResultsViewModel
                {
                    Facilities = facilities
                };

                return this.PartialView("_MyContactUs", contactUsModel);
            }

            return new EmptyResult();
        }

        #endregion

        #region create

        [HttpGet]
        public PartialViewResult CreateSupportRequest(int? visitId)
        {
            return this.PartialView("_CreateSupportRequest", this.CreateSupportRequestModel(this.CurrentUserId, this._guarantorFilterService.Value.VpGuarantorId, visitId,false));
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult CreateSupportRequest(CreateSupportRequestViewModel model)
        {
            if (!this.ModelState.IsValid)
            {
                return this.Json(new ResultMessage(false, ""));
            }

            SupportRequestCreateDto supportRequestCreateDto = new SupportRequestCreateDto
            {
                CreatedByVisitPayUserId = this.CurrentUserId,
                MessageBody = model.MessageBody,
                SubmittedForVisitPayUserId = model.SubmittedForVisitPayUserId,
                VisitId = model.VisitId,
                VisitPayUserId = this.CurrentUserId
            };

            SupportRequestDto supportRequestDto = this.SupportRequestApplicationService.Value.CreateSupportRequest(supportRequestCreateDto);

            return this.Json(new ResultMessage<dynamic>(true, new
            {
                SupportRequestId = supportRequestDto.SupportRequestId,
                SupportRequestMessageId = supportRequestDto.SupportRequestMessages.First().SupportRequestMessageId
            }));
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<JsonResult> CreateSupportRequestConfirmation()
        {
            return await this.CreateSupportRequestConfirmation(this.CurrentUserId);
        }

        #endregion

        #region details

        [HttpPost]
        [ValidateAntiForgeryToken]
        [OverrideAuthorization, EmulateUser(AllowPost = true)]
        public JsonResult SupportRequest(int supportRequestId)
        {
            if (!this.IsCurrentUserEmulating())
            {
                this.SupportRequestApplicationService.Value.SetRead(supportRequestId, this.CurrentUserId, SupportRequestMessageTypeEnum.ToGuarantor);
            }

            SupportRequestViewModel model = this.GetSupportRequest(supportRequestId, this.CurrentUserId, false);

            model.InternalMessages = new List<SupportRequestMessageViewModel>();
            model.GuarantorMessages.Where(x => (SupportRequestMessageTypeEnum)x.SupportRequestMessageType == SupportRequestMessageTypeEnum.ToGuarantor).ToList().ForEach(x => x.VisitPayUserName = this.ClientDto.SupportFromName);
            model.GuarantorMessages.Where(x => (SupportRequestMessageTypeEnum)x.SupportRequestMessageType == SupportRequestMessageTypeEnum.FromGuarantor).ToList().ForEach(x => x.VisitPayUserName = "You");

            return this.Json(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult SaveMessage(SupportRequestGenericMessageViewModel model)
        {
            SupportRequestMessageDto supportRequestMessageDto = this.SupportRequestApplicationService.Value.SaveGuarantorMessage(model.SupportRequestId, this.CurrentUserId, model.MessageBody, this.CurrentUserId);

            return this.Json(new ResultMessage(true, supportRequestMessageDto.SupportRequestMessageId.ToString()));
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [OverrideAuthorization, EmulateUser(AllowPost = true)]
        public PartialViewResult Attachments(int supportRequestId, int? supportRequestMessageId)
        {
            return this.GetAttachments(supportRequestId, supportRequestMessageId, this.CurrentUserId, false);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [OverrideAuthorization, EmulateUser(AllowPost = true)]
        public JsonResult PrepareDownload(int supportRequestId, int? supportRequestMessageAttachmentId)
        {
            return this.AttachmentPrepareDownload(supportRequestId, supportRequestMessageAttachmentId, this.CurrentUserId, false);
        }

        #endregion

        #region actions: close/reopen

        [HttpPost]
        [ValidateAntiForgeryToken]
        public PartialViewResult SupportRequestAction(SupportRequestStatusEnum supportRequestStatus, int supportRequestId, string supportRequestDisplayId)
        {
            SupportRequestActionViewModel model = new SupportRequestActionViewModel();

            return base.SupportRequestAction(supportRequestStatus, supportRequestId, supportRequestDisplayId, model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult CompleteSupportRequestAction(SupportRequestActionViewModel model)
        {
            return base.CompleteSupportRequestAction(model, this.CurrentUserId);
        }

        #endregion

        #region shared

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult UploadSupportRequestMessageAttachment()
        {
            HttpRequestBase httpRequest = this.HttpContext.Request;

            return this.UploadSupportRequestMessageAttachment(httpRequest, this.CurrentUserId);
        }

        #endregion

        #region print

        [HttpPost]
        [ValidateAntiForgeryToken]
        [OverrideAuthorization, EmulateUser(AllowPost = true)]
        public ActionResult PreparePrintSupportRequest(int supportRequestId, string sortOrder)
        {
            return this.PrintSupportRequest(supportRequestId, this.CurrentUserId, sortOrder, null);
        }

        [HttpGet]
        [OverrideAuthorization, EmulateUser(AllowPost = true)]
        public new ActionResult PrintSupportRequest()
        {
            PrintSupportRequestViewModel model = base.PrintSupportRequest(false);

            model.SupportRequest.InternalMessages = new List<SupportRequestMessageViewModel>();
            model.SupportRequest.GuarantorMessages.Where(x => (SupportRequestMessageTypeEnum) x.SupportRequestMessageType == SupportRequestMessageTypeEnum.ToGuarantor).ToList().ForEach(x => x.VisitPayUserName = this.ClientDto.SupportFromName);
            model.SupportRequest.GuarantorMessages.Where(x => (SupportRequestMessageTypeEnum) x.SupportRequestMessageType == SupportRequestMessageTypeEnum.FromGuarantor).ToList().ForEach(x => x.VisitPayUserName = "You");

            return this.View("~/Views/Support/PrintSupportRequest.cshtml", model);
        }

        #endregion

        #region drawer
        
        [HttpGet]
        [EmulateUser(AllowGet = true)]
        public JsonResult SupportRequestUnreadCount()
        {
            int count = this.SupportRequestApplicationService.Value.GetSupportRequestsWithUnreadMessagesToGuarantorCount(this.CurrentGuarantorId ?? 0);
            return this.Json(new 
            { 
                Count = count 
            }, JsonRequestBehavior.AllowGet);
        }

        public PartialViewResult SupportRequestDrawer()
        {
            return this.PartialView("~/Views/Support/_SupportRequestDrawer.cshtml", this.CreateSupportRequestWidgetViewModel(false));
        }

        #endregion
    }
}
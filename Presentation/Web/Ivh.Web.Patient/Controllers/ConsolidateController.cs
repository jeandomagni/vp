﻿namespace Ivh.Web.Patient.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using System.Web;
    using System.Web.Mvc;
    using System.Web.SessionState;
    using Application.Content.Common.Dtos;
    using Application.Content.Common.Interfaces;
    using Application.Core.Common.Dtos;
    using Application.Core.Common.Interfaces;
    using Application.FinanceManagement.Common.Dtos;
    using Application.FinanceManagement.Common.Interfaces;
    using Application.User.Common.Dtos;
    using AutoMapper;
    using Base;
    using Common.EventJournal;
    using Common.VisitPay.Enums;
    using Common.Web.Filters;
    using Common.Web.Interfaces;
    using Models.Consolidate;
    using Services;

    [RequireFeature(VisitPayFeatureEnum.Consolidation)]
    public class ConsolidateController : BaseConsolidateController
    {
        public ConsolidateController(
            Lazy<IBaseControllerService> baseControllerService,
            Lazy<IContentApplicationService> contentApplicationService,
            Lazy<IConsolidationGuarantorApplicationService> consolidationGuarantorApplicationService,
            Lazy<IGuarantorApplicationService> guarantorApplicationService,
            Lazy<IFinancePlanApplicationService> financePlanApplicationService,
            Lazy<IManagedUserApplicationService> managedUserApplicationService,
            Lazy<IManagingUserApplicationService> managingUserApplicationService,
            Lazy<IVisitPayUserApplicationService> visitPayUserApplicationService,
            Lazy<IVisitPayUserJournalEventApplicationService> visitPayUserEventJournalApplicationService,
            Lazy<GuarantorFilterService> guarantorFilterService,
            Lazy<IVisitApplicationService> visitService)
            : base(baseControllerService,
                contentApplicationService,
                guarantorFilterService,
                consolidationGuarantorApplicationService,
                financePlanApplicationService,
                managedUserApplicationService,
                managingUserApplicationService,
                visitPayUserEventJournalApplicationService,
                guarantorApplicationService,
                visitService)
        {
        }

        [HttpGet]
        [Common.Web.Attributes.SessionState(SessionStateBehavior.Required)]
        public ActionResult ManageHousehold()
        {
            int visitPayUserId = this.CurrentUserId;

            ManageHouseholdViewModel model = this.GetManageHouseholdModel(visitPayUserId);

            return this.View(model);
        }

        [HttpGet]
        public ActionResult RequestToBeManaged()
        {
            return this.View(this.GetRequestManagedModel());
        }

        [HttpGet]
        public ActionResult RequestToBeManaging()
        {
            return this.View(this.GetRequestManagingModel());
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<JsonResult> ConsolidateMatch(MatchRequestViewModel model)
        {
            HttpContext context = System.Web.HttpContext.Current;
            int visitPayUserId = this.CurrentUserId;
            ConsolidationMatchRequestDto consolidationMatchRequestDto = new ConsolidationMatchRequestDto
            {
                BirthYear = Convert.ToInt32(model.BirthYear),
                EmailAddress = model.EmailAddress,
                FirstName = model.FirstName,
                LastName = model.LastName,
                ConsolidationMatchRequestType = model.MatchRequestType
            };

            ConsolidationMatchResponseDto consolidationMatchResponseDto;

            switch (consolidationMatchRequestDto.ConsolidationMatchRequestType)
            {
                case ConsolidationMatchRequestTypeEnum.RequestToBeManaged:
                    consolidationMatchResponseDto = await this.ManagedUserApplicationService.Value.MatchGuarantorAsync(consolidationMatchRequestDto, visitPayUserId).ConfigureAwait(true);
                    if (consolidationMatchResponseDto.ConsolidationMatchStatus == ConsolidationMatchStatusEnum.Matched)
                    {
                        this.VisitPayUserJournalEventApplicationService.Value.LogConsolidationGuarantorRequestManaged(visitPayUserId, consolidationMatchResponseDto.MatchedGuarantor.VpGuarantorId, Mapper.Map<JournalEventHttpContextDto>(context));
                    }
                    break;

                case ConsolidationMatchRequestTypeEnum.RequestToBeManaging:
                    consolidationMatchResponseDto = await this.ManagingUserApplicationService.Value.MatchGuarantorAsync(consolidationMatchRequestDto, visitPayUserId).ConfigureAwait(true);
                    if (consolidationMatchResponseDto.ConsolidationMatchStatus == ConsolidationMatchStatusEnum.Matched)
                    {
                        this.VisitPayUserJournalEventApplicationService.Value.LogConsolidationGuarantorRequestManaging(visitPayUserId, consolidationMatchResponseDto.MatchedGuarantor.VpGuarantorId, Mapper.Map<JournalEventHttpContextDto>(context));
                    }
                    break;

                default:
                    consolidationMatchResponseDto = new ConsolidationMatchResponseDto {ConsolidationMatchStatus = ConsolidationMatchStatusEnum.NoMatchFound};
                    break;
            }

            CmsVersionDto version;
            switch (consolidationMatchResponseDto.ConsolidationMatchStatus)
            {
                case ConsolidationMatchStatusEnum.Matched:
                    // ui will say pending at this point, so need to kick off whatever logic here
                    version = await this.ContentApplicationService.Value.GetCurrentVersionAsync(CmsRegionEnum.ConsolidationRequestPending);
                    return this.Json(new { Status = consolidationMatchResponseDto.ConsolidationMatchStatus, Title = version.ContentTitle, Message = version.ContentBody, RedirectUrl = this.Url.Action("ManageHousehold") });

                case ConsolidationMatchStatusEnum.NotEligible:
                    version = await this.ContentApplicationService.Value.GetCurrentVersionAsync(CmsRegionEnum.ConsolidationUnableToConsolidate);
                    return this.Json(new { Status = consolidationMatchResponseDto.ConsolidationMatchStatus, Title = version.ContentTitle, Message = version.ContentBody });

                default:
                    version = await this.ContentApplicationService.Value.GetCurrentVersionAsync(CmsRegionEnum.ConsolidationMatchNotFound);
                    return this.Json(new { Status = consolidationMatchResponseDto.ConsolidationMatchStatus, Title = version.ContentTitle, Message = version.ContentBody });
            }
        }

        [HttpGet]
        public JsonResult CancelRequest()
        {
            CmsVersionDto cmsVersion = this.ContentApplicationService.Value.GetCurrentVersionAsync(CmsRegionEnum.ConsolidationManagedUserCancelRequest).Result;

            return this.Json(new
            {
                cmsVersion.ContentTitle,
                cmsVersion.ContentBody
            }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Common.Web.Attributes.SessionState(SessionStateBehavior.Required)]
        public async Task CancelRequest(Guid consolidationGuarantorId, ConsolidationMatchRequestTypeEnum requestType)
        {
            HttpContext context = System.Web.HttpContext.Current;
            int consolidationGuarantorIdClear = this.SessionFacade.Value.ConsolidationGuarantorObfuscator.Clarify(consolidationGuarantorId);
            int visitPayUserId = this.CurrentUserId;

            int vpGuarantorId = this.GuarantorApplicationService.Value.GetGuarantorId(visitPayUserId);
            this.GuarantorFilterService.Value.SetGuarantor(vpGuarantorId);

            switch (requestType)
            {
                case ConsolidationMatchRequestTypeEnum.RequestToBeManaged:
                    await this.ManagedUserApplicationService.Value.CancelConsolidationAsync(consolidationGuarantorIdClear, visitPayUserId).ConfigureAwait(false);
                    break;
                case ConsolidationMatchRequestTypeEnum.RequestToBeManaging:
                    await this.ManagingUserApplicationService.Value.CancelConsolidationAsync(consolidationGuarantorIdClear, visitPayUserId).ConfigureAwait(false);
                    break;
            }

            this.VisitPayUserJournalEventApplicationService.Value.LogGuarantorConsolidationRequestCancelled(visitPayUserId, vpGuarantorId, consolidationGuarantorIdClear, Mapper.Map<JournalEventHttpContextDto>(context));
        }

        [HttpGet]
        public ActionResult AcceptTerms(Guid id, int requestType)
        {
            return this.View(this.GetAcceptTermsModel(id, requestType));
        }

        [HttpGet]
        public async Task<ActionResult> AcceptTermsFp(Guid id, int requestType)
        {
            AcceptTermsFpViewModel model = await this.GetAcceptTermsFpModel(id, requestType).ConfigureAwait(false);

            return this.View(model);
        }

        [HttpGet]
        public ActionResult ConfirmAcceptance()
        {
            CmsVersionDto cmsVersion = this.ContentApplicationService.Value.GetCurrentVersionAsync(CmsRegionEnum.ConsolidationConfirmAcceptance).Result;

            return this.Json(new
            {
                cmsVersion.ContentTitle,
                cmsVersion.ContentBody
            }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<JsonResult> ConfirmAcceptance(Guid consolidationGuarantorId, ConsolidationMatchRequestTypeEnum requestType)
        {
            HttpContext context = System.Web.HttpContext.Current;
            int visitPayUserId = this.CurrentUserId;

            int consolidationGuarantorIdClear = this.SessionFacade.Value.ConsolidationGuarantorObfuscator.Clarify(consolidationGuarantorId);
            bool success = false;
            switch (requestType)
            {
                case ConsolidationMatchRequestTypeEnum.RequestToBeManaging:
                    success = await this.ManagingUserApplicationService.Value.AcceptTermsAsync(consolidationGuarantorIdClear, visitPayUserId).ConfigureAwait(true);
                    
                    break;
                case ConsolidationMatchRequestTypeEnum.RequestToBeManaged:
                    success = await this.ManagedUserApplicationService.Value.AcceptTermsAsync(consolidationGuarantorIdClear, visitPayUserId).ConfigureAwait(true);
                    break;
            }

            int vpGuarantorId = this.GuarantorApplicationService.Value.GetGuarantorId(visitPayUserId);
            this.VisitPayUserJournalEventApplicationService.Value.LogGuarantorConsolidationRequestAccepted(visitPayUserId, vpGuarantorId, consolidationGuarantorIdClear, Mapper.Map<JournalEventHttpContextDto>(context));

            return this.Json(new
            {
                Success = success,
                Message = success ? null : "This request has expired.",
                RedirectUrl = this.Url.Action("ManageHousehold")
            });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<JsonResult> ConfirmAcceptanceFp(Guid consolidationGuarantorId, ConsolidationMatchRequestTypeEnum requestType)
        {
            int visitPayUserId = this.CurrentUserId;

            int consolidationGuarantorIdClear = this.SessionFacade.Value.ConsolidationGuarantorObfuscator.Clarify(consolidationGuarantorId);

            bool success = false;

            switch (requestType)
            {
                case ConsolidationMatchRequestTypeEnum.RequestToBeManaging:
                    success = await this.ManagingUserApplicationService.Value.AcceptFinancePlanTermsAsync(consolidationGuarantorIdClear, visitPayUserId).ConfigureAwait(true);
                    break;
            }

            return this.Json(new
            {
                Success = success,
                Message = success ? null : "This request has expired.",
                RedirectUrl = this.Url.Action("ManageHousehold")
            });
        }

        [HttpGet]
        public ActionResult DeclineConsolidation()
        {
            CmsVersionDto cmsVersion = this.ContentApplicationService.Value.GetCurrentVersionAsync(CmsRegionEnum.ConsolidationDeclineConsolidation).Result;

            return this.Json(new
            {
                cmsVersion.ContentTitle,
                cmsVersion.ContentBody
            }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<JsonResult> DeclineConsolidation(Guid consolidationGuarantorId, ConsolidationMatchRequestTypeEnum requestType)
        {
            await this.DoDeclineConsolidation(consolidationGuarantorId, requestType).ConfigureAwait(false);
            
            return this.Json(new
            {
                RedirectUrl = this.Url.Action("ManageHousehold")
            });
        }

        [HttpGet]
        public async Task<JsonResult> CancelConsolidation(Guid id, ConsolidationMatchRequestTypeEnum requestType)
        {
            CmsVersionDto cmsVersion = new CmsVersionDto();
            IDictionary<string, string> additionalValues = new Dictionary<string, string>();

            switch (requestType)
            {
                case ConsolidationMatchRequestTypeEnum.RequestToBeManaged:
                    
                    VisitPayUserDto managingUser = this.GetManagingUser();
                    additionalValues.Add("[[ManagingUser]]", managingUser.DisplayFirstNameLastName);

                    cmsVersion = await this.ContentApplicationService.Value.GetCurrentVersionAsync(CmsRegionEnum.ConsolidationManagedUserCancelConsolidation, additionalValues: additionalValues);
                    break;
                case ConsolidationMatchRequestTypeEnum.RequestToBeManaging:

                    VisitPayUserDto managedUser = this.GetManagedUser(id);
                    additionalValues.Add("[[ManagedUser]]", managedUser.DisplayFirstNameLastName);

                    cmsVersion = await this.ContentApplicationService.Value.GetCurrentVersionAsync(CmsRegionEnum.ConsolidationManagingUserCancelConsolidation, additionalValues: additionalValues);
                    break;
            }

            return this.Json(new
            {
                cmsVersion.ContentTitle,
                cmsVersion.ContentBody
            }, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult DeconsolidateFp(Guid id, ConsolidationMatchRequestTypeEnum requestType)
        {
            DeconsolidateFpViewModel model = this.GetDeconsolidateFpViewModel(id, requestType);

            if (model == null)
            {
                return this.RedirectToAction("ManageHousehold");
            }

            return this.View(model);
        }

        [HttpGet]
        public PartialViewResult ManagedBy()
        {
            GuarantorDto guarantor = this.CurrentGuarantorId.HasValue ? this.GuarantorApplicationService.Value.GetGuarantor(this.CurrentGuarantorId.Value) : this.GuarantorApplicationService.Value.GetGuarantor(this.CurrentUserName);
            string model = null;

            if (guarantor.ConsolidationStatus == GuarantorConsolidationStatusEnum.Managed && guarantor.ManagingGuarantorId.HasValue)
            {
                GuarantorDto managingGuarantor = this.GuarantorApplicationService.Value.GetGuarantor(guarantor.ManagingGuarantorId.Value);
                if (managingGuarantor != null)
                {
                    model = managingGuarantor.User.DisplayFirstNameLastName;
                }
            }

            return this.PartialView("_ManagedBy", model);
        }
    }
}
﻿namespace Ivh.Web.Patient.Controllers
{
    using System;
    using System.Threading.Tasks;
    using System.Web.Mvc;
    using System.Web.SessionState;
    using Application.AppIntelligence.Common.Interfaces;
    using Application.Content.Common.Interfaces;
    using Application.Core.Common.Interfaces;
    using Application.FinanceManagement.Common.Interfaces;
    using Attributes;
    using Base;
    using Common.ServiceBus.Interfaces;
    using Common.Web.Cookies;
    using Common.Web.Interfaces;
    using Domain.Logging.Interfaces;
    using Services;
    using SessionFacade;

    [AllowAnonymous]
    public class RegistrationController : BaseRegistrationController
    {
        public RegistrationController(
            Lazy<IBaseControllerService> baseControllerService,
            Lazy<IBus> bus,
            Lazy<IContentApplicationService> contentApplicationService,
            Lazy<IGuarantorApplicationService> guarantorApplicationService,
            Lazy<IPaymentMethodsApplicationService> paymentMethodsApplicationService,
            Lazy<IPaymentMethodAccountTypeApplicationService> paymentMethodAccountTypeApplicationService,
            Lazy<IPaymentMethodProviderTypeApplicationService> paymentMethodProviderTypeApplicationService,
            Lazy<ISecurityQuestionApplicationService> securityQuestionApplicationService,
            Lazy<ISsoApplicationService> ssoApplicationService,
            Lazy<IVisitPayUserApplicationService> visitPayUserApplicationService,
            Lazy<IWebCookieFacade> webCookie,
            Lazy<IWebPatientSessionFacade> webPatientSessionFacade,
            Lazy<IRegistrationService> registrationService,
            Lazy<GuarantorFilterService> guarantorFilterService)
            : base(baseControllerService,
                bus,
                guarantorFilterService,
                contentApplicationService,
                guarantorApplicationService,
                paymentMethodsApplicationService,
                paymentMethodAccountTypeApplicationService,
                paymentMethodProviderTypeApplicationService,
                securityQuestionApplicationService,
                ssoApplicationService,
                visitPayUserApplicationService,
                webCookie,
                webPatientSessionFacade,
                registrationService
                )
        {
        }

        [MobileRedirect("/mobile/Registration")]
        [HttpGet]
        [Common.Web.Attributes.SessionState(SessionStateBehavior.Required)]
        public override async Task<ActionResult> Index()
        {
            return await base.Index();
        }
    }
}
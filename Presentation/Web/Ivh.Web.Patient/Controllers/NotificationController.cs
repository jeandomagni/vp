﻿namespace Ivh.Web.Patient.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using System.Web.Mvc;
    using System.Web.SessionState;
    using Application.Core.Common.Dtos;
    using Application.Core.Common.Interfaces;
    using AutoMapper;
    using Common.Web.Filters;
    using Common.Web.Interfaces;
    using Models.Notifications;
    using Services;

    public class NotificationController : BaseController
    {
        private readonly Lazy<ISystemMessageApplicationService> _systemMessageApplicationService;

        public NotificationController(
            Lazy<IBaseControllerService> baseControllerService,
            Lazy<GuarantorFilterService> guarantorFilterService,
            Lazy<ISystemMessageApplicationService> systemMessageApplicationService)
            : base(baseControllerService, guarantorFilterService)
        {
            this._systemMessageApplicationService = systemMessageApplicationService;
        }
        
        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult MarkMessageAsRead(int systemMessageId)
        {
            this._systemMessageApplicationService.Value.MarkMessageAsRead(this.CurrentUserId, systemMessageId);
            return this.Json(string.Empty);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult MarkAllMessagesAsRead()
        {
            this._systemMessageApplicationService.Value.MarkAllMessagesAsRead(this.CurrentUserId);
            return this.Json(string.Empty);
        }

        [HttpGet]
        [EmulateUser(AllowGet = true)]
        public async Task<JsonResult> UnreadMessageCount()
        {
            int unreadNotificationCount = await this._systemMessageApplicationService.Value.GetUnreadMessageCount(this.CurrentUserId, new Dictionary<string, string>());
            return this.Json(new 
            { 
                Count = unreadNotificationCount 
            }, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        [EmulateUser(AllowGet = true)]
        public PartialViewResult NotificationDrawer()
        {
            return this.PartialView("_NotificationDrawer");
        }

        [HttpPost]
        [EmulateUser(AllowPost = true)]
        public async Task<JsonResult> NotificationData()
        {
            IDictionary<string, string> replacementValues = new Dictionary<string, string>
            {
                {"[[UrlMangeHousehold]]", this.Url.Action("ManageHousehold", "Consolidate")},
                {"[[UrlSso]]", this.Url.Action("Settings", "Sso")},
                {"[[UrlPhoneTextSettings]]", this.Url.Action("PhoneTextSettings", "Account")},
                {"[[UrlHelpCenter]]", this.Url.Action("Help", "Home")},
                {"[[UrlPaymentSummary]]", this.Url.Action("Documents", "Account")}
            };
            
            IList<SystemMessageDto> systemMessageDtos = await this._systemMessageApplicationService.Value.GetMessages(this.CurrentUserId, replacementValues);
            IList<SystemMessageViewModel> messages = Mapper.Map<IList<SystemMessageViewModel>>(systemMessageDtos);
            
            return this.Json(messages);
        }
    }
}
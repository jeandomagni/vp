﻿namespace Ivh.Web.Patient.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web.Mvc;
    using System.Web.SessionState;
    using Application.Base.Common.Interfaces;
    using Application.Core.Common.Dtos;
    using Common.Base.Utilities.Extensions;
    using Common.Web.Filters;
    using Common.Web.Interfaces;
    using Common.Web.Models.Consolidate;
    using Services;

    public class GuarantorFilterController : BaseController
    {
        private readonly Lazy<IVisitBalanceBaseApplicationService> _visitBalanceBaseApplicationService;

        public GuarantorFilterController(
            Lazy<IBaseControllerService> baseControllerService,
            Lazy<GuarantorFilterService> guarantorFilterService,
            Lazy<IVisitBalanceBaseApplicationService> visitBalanceBaseApplicationService)
            : base(baseControllerService, guarantorFilterService)
        {
            this._visitBalanceBaseApplicationService = visitBalanceBaseApplicationService;
        }

        [HttpGet]
        [Common.Web.Attributes.SessionState(SessionStateBehavior.Required)]
        public PartialViewResult GetFilter(string title, bool showHouseholdBalance = false)
        {
            IDictionary<Guid, GuarantorDto> guarantors = this.GuarantorFilterService.Value.GetObfuscatedGuarantors();

            IList<SelectListItem> items = new List<SelectListItem>();
            guarantors.ForEach(i =>
            {
                GuarantorDto guarantorDto = i.Value;
                items.Add(new SelectListItem
                {
                    Selected = guarantorDto.VpGuarantorId == this.GuarantorFilterService.Value.VpGuarantorId,
                    Text = guarantorDto.User.DisplayFirstNameLastName,
                    Value = i.Key.ToString()
                });
            });

            GuarantorFilterViewModel model = new GuarantorFilterViewModel
            {
                Title = title,
                Items = items
            };
            
            if (showHouseholdBalance)
            {
                model.Balance = guarantors.Sum(guarantor => this._visitBalanceBaseApplicationService.Value.GetTotalBalance(guarantor.Value.VpGuarantorId));
            }

            return this.PartialView("~/Views/Shared/_GuarantorFilter.cshtml", model);
        }
        
        [HttpPost]
        [ValidateAntiForgeryToken]
        [OverrideAuthorization, EmulateUser(AllowPost = true)]
        [Common.Web.Attributes.SessionState(SessionStateBehavior.Required)]
        public JsonResult SetGuarantor(Guid obfuscatedVpGuarantorId)
        {
            this.GuarantorFilterService.Value.SetGuarantor(obfuscatedVpGuarantorId);

            return this.Json(true);
        }
    }
}
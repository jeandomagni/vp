﻿namespace Ivh.Web.Patient.Controllers
{
    using System;
    using System.Threading.Tasks;
    using System.Web.Mvc;
    using System.Web.SessionState;
    using Application.Content.Common.Dtos;
    using Application.Content.Common.Interfaces;
    using Application.Core.Common.Interfaces;
    using Application.FinanceManagement.Common.Interfaces;
    using Attributes;
    using Common.Base.Utilities.Helpers;
    using Common.VisitPay.Enums;
    using Common.Web.Controllers;
    using Common.Web.Interfaces;
    using Common.Web.Models.Legal;
    using Common.Web.Models.Shared;
    using Ivh.Domain.FinanceManagement.FinancePlan.Entities;
    using Provider.Pdf;
    using System;
    using System.Threading.Tasks;
    using System.Web.Mvc;

    [RequiresAcknowledgement(Enabled = false)]
    [RequiresSsoAuthorization(Enabled = false)]
    [GuarantorAuthorize]
    public class LegalController : BaseLegalController
    {
        private readonly Lazy<IFinancePlanApplicationService> _financePlanApplicationService;

        public LegalController(
            Lazy<IBaseControllerService> baseControllerService,
            Lazy<IGuarantorApplicationService> guarantorApplicationService,
            Lazy<IContentApplicationService> contentApplicationService,
            Lazy<ICreditAgreementApplicationService> creditAgreementApplicationService,
            Lazy<IPdfConverter> pdfConverter,
            Lazy<IFinancePlanApplicationService> financePlanApplicationService
            )
            : base(
                  baseControllerService,
                  guarantorApplicationService,
                  contentApplicationService,
                  creditAgreementApplicationService,
                  pdfConverter)
        {
            this._financePlanApplicationService = financePlanApplicationService;
        }

        [MobileRedirect("/mobile/Legal/")]
        [HttpGet]
        [AllowAnonymous]
        public override async Task<ActionResult> Index()
        {
            return await base.Index();
        }

        [HttpGet]
        [RequiresAcknowledgement(Enabled = true)]
        [Common.Web.Attributes.SessionState(SessionStateBehavior.ReadOnly)]
		public async Task<ActionResult> RetailInstallmentContract(string id)
        {
			int? financePlanOfferId = this.GetFinancePlanOfferId(id);
            if (!financePlanOfferId.HasValue)
			{
				return this.RedirectToAction("NotFound", "Error");

			} 

			ContentDto content = await this.CreditAgreementApplicationService.Value.GetCreditAgreementContentAsync(financePlanOfferId.Value);
	        CreditAgreementViewModel model = new CreditAgreementViewModel
	        {
		        Text = content.ContentBody,
                Title = content.ContentTitle,
		        IsExport = false,
		        FinancePlanOfferId = financePlanOfferId.Value
			};

	        return this.View("_CreditAgreementPage", model);
        }

        [HttpGet]
        [AllowOffline]
        [Common.Web.Attributes.SessionState(SessionStateBehavior.ReadOnly)]
        public async Task<ActionResult> CreditAgreementPartial(int o)
        {
            ContentDto content = await this.CreditAgreementApplicationService.Value.GetCreditAgreementContentAsync(o);

            CreditAgreementViewModel model = new CreditAgreementViewModel
            {
                Text = content.ContentBody,
                Title = content.ContentTitle,
                IsExport = false,
                FinancePlanOfferId = o
            };

            return this.PartialView("_CreditAgreement", model);
        }

        [HttpGet]
        [AllowOffline]
        [Common.Web.Attributes.SessionState(SessionStateBehavior.ReadOnly)]
        public async Task<ActionResult> CreditAgreementPdf(int o, int? v)
        {
            ContentDto content = await this.CreditAgreementApplicationService.Value.GetCreditAgreementContentAsync(o);

            this.ViewData.Model = new CreditAgreementViewModel
            {
                Text = content.ContentBody,
                Title = content.ContentTitle,
                IsExport = true,
                FinancePlanOfferId = o
            };

            string clientName = ReplacementUtility.ReplaceWhiteSpace(ReplacementUtility.RemoveNonAlphaNumeric(this.ClientDto.ClientName), "_");
            string agreementName = ReplacementUtility.ReplaceWhiteSpace(ReplacementUtility.RemoveNonAlphaNumeric(content.ContentTitle), "_");
            string fileName = $"{clientName}_{agreementName}.pdf";

            this.WritePdfInline(this.PdfConverter.Value.CreatePdfFromHtml($"{this.ClientDto.ClientName} {agreementName}", this.RenderViewToString("~/Views/Legal/_CreditAgreementPdf.cshtml", "~/Views/Shared/_LayoutPrint.cshtml")), fileName);

            return this.Json(new { }, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        [AllowAnonymous]
        public ActionResult LegalPartial()
        {
            CmsVersionDto cmsVersion = this.ContentApplicationService.Value.GetCurrentVersionAsync(CmsRegionEnum.Legal).Result;

            TextViewModel model = new TextViewModel
            {
                Text = cmsVersion.ContentBody,
                IsExport = false
            };

            return this.PartialView("_Legal", model);
        }

        [HttpGet]
        [AllowAnonymous]
        public ActionResult LegalPdf()
        {
            CmsVersionDto cmsVersion = this.ContentApplicationService.Value.GetCurrentVersionAsync(CmsRegionEnum.Legal).Result;

            this.ViewData.Model = new TextViewModel
            {
                Text = cmsVersion.ContentBody,
                IsExport = true
            };

            string fileName = ReplacementUtility.ReplaceWhiteSpace(ReplacementUtility.RemoveNonAlphaNumeric(this.ClientDto.ClientName), "_") + "_Legal.pdf";
            this.WritePdfInline(this.PdfConverter.Value.CreatePdfFromHtml(this.ClientDto.ClientName + " Legal", this.RenderViewToString("~/Views/Legal/_Legal.cshtml", "~/Views/Shared/_LayoutPrint.cshtml")), fileName);

            return this.Json(new { }, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult Privacy()
        {
            return this.View(this.GetPrivacy());
        }

        [HttpGet]
        [AllowAnonymous]
        public ActionResult PrivacyPartial()
        {
            return this.PartialView("_Privacy", this.GetPrivacy());
        }

        [HttpGet]
        [AllowAnonymous]
        public ActionResult PrivacyPdf()
        {
            this.ViewData.Model = this.GetPrivacy(false);

            string fileName = ReplacementUtility.ReplaceWhiteSpace(ReplacementUtility.RemoveNonAlphaNumeric(this.ClientDto.ClientName), "_") + "_PrivacyPolicy.pdf";
            this.WritePdfInline(this.PdfConverter.Value.CreatePdfFromHtml(this.ClientDto.ClientName + " Privacy Policy", this.RenderViewToString("~/Views/Legal/_Privacy.cshtml", "~/Views/Shared/_LayoutPrint.cshtml")), fileName);

            return this.Json(new { }, JsonRequestBehavior.AllowGet);
        }

        private TextViewModel GetPrivacy(bool showButton = true)
        {
            CmsVersionDto cmsVersion = this.ContentApplicationService.Value.GetCurrentVersionAsync(CmsRegionEnum.Privacy).Result;

            return new TextViewModel
            {
                Text = cmsVersion.ContentBody,
                IsExport = !showButton
            };
        }

        /// <summary>
        /// If incoming ID is formatted as a finance plan public ID (PHI), 
        /// lookup the finance plan offer ID accordingly. Support legacy contract number 
        /// format for users with access to those links.
        /// </summary>
        private int? GetFinancePlanOfferId(string id)
        {
            int? financePlanOfferId;
            bool isFinancePlanPublicIdPhiFormat = FinancePlanPublicIdPhiSource.IsValidFormat(id);

            if (isFinancePlanPublicIdPhiFormat)
            {
                financePlanOfferId = 
                    this._financePlanApplicationService.Value
                        .GetFinancePlanOfferId(id, this.CurrentGuarantorId);

                return financePlanOfferId;
            }

            financePlanOfferId = 
                this.CreditAgreementApplicationService.Value
                    .GetFinancePlanOfferIdFromContractNumber(id, this.CurrentGuarantorId);

            return financePlanOfferId;
        }
    }
}
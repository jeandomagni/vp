﻿namespace Ivh.Web.Patient.Controllers
{
    using System;
    using System.Web.Mvc;
    using System.Web.SessionState;
    using Attributes;
    using Common.Web.Controllers;
    using Common.Web.Filters;
    using Common.Web.Interfaces;

    public class ErrorController : BaseErrorController
    {

        public ErrorController(Lazy<IBaseControllerService> baseControllerService)
            : base(baseControllerService)
        {
        }

        [HttpGet]
        [AllowAnonymous]
        [RequiresAcknowledgement(Enabled = false)]
        [RequiresSsoAuthorization(Enabled = false)]
        public override ActionResult Index()
        {
            return base.Index();
        }

        [HttpGet]
        [AllowAnonymous]
        [RequiresAcknowledgement(Enabled = false)]
        [RequiresSsoAuthorization(Enabled = false)]
        public override ActionResult NotFound()
        {
            return base.NotFound();
        }

        private const string ErrorFormatString = "event: {0}, jqxhr: {1}, settings: {2}, thrownError: {3}";

        [AllowAnonymous]
        [RequiresAcknowledgement(Enabled = false)]
        [RequiresSsoAuthorization(Enabled = false)]
        [ValidateAntiForgeryToken]
        [HttpPost]
        [OverrideAuthorization, EmulateUser(AllowPost = true)]
        public JsonResult LogError(string ajaxEvent, string jqxhr, string settings, string thrownError)
        {
            try
            {
                this.Logger.Value.Fatal(() => string.Format(ErrorFormatString, ajaxEvent, jqxhr, settings, thrownError));
            }
            catch
            {
                // just in case
            }
            return this.Json(true);
        }

        [AllowAnonymous]
        [RequiresAcknowledgement(Enabled = false)]
        [RequiresSsoAuthorization(Enabled = false)]
        [ValidateAntiForgeryToken]
        [HttpPost]
        [OverrideAuthorization, EmulateUser(AllowPost = true)]
        public override JsonResult LogSecurePanError(string errorMessage)
        {
            return base.LogSecurePanError(errorMessage);
        }
    }
}
﻿namespace Ivh.Web.Patient.Controllers
{
    using System;
    using System.Threading.Tasks;
    using System.Web.Mvc;
    using System.Web.SessionState;
    using Application.Content.Common.Interfaces;
    using Application.Core.Common.Interfaces;
    using Application.FileStorage.Common.Interfaces;
    using Attributes;
    using Common.Base.Enums;
    using Common.VisitPay.Enums;
    using Common.VisitPay.Strings;
    using Common.Web.Controllers;
    using Common.Web.Filters;
    using Common.Web.Interfaces;
    using Common.Web.Models;
    using Common.Web.Models.Itemization;
    using Services;

    [GuarantorAuthorize(Roles = VisitPayRoleStrings.System.Patient + "," + VisitPayRoleStrings.Admin.Administrator)]
    [RequireFeature(VisitPayFeatureEnum.Itemization)]
    public class ItemizationController : BaseItemizationController
    {
        private readonly Lazy<GuarantorFilterService> _guarantorFilterService;

        public ItemizationController(
            Lazy<IBaseControllerService> baseControllerService,
            Lazy<GuarantorFilterService> guarantorFilterService,
            Lazy<IContentApplicationService> contentApplicationService,
            Lazy<IFileStorageApplicationService> fileStorageApplicationService,
            Lazy<IVisitItemizationStorageApplicationService> visitItemizationStorageApplicationService)
            : base(
                baseControllerService,
                contentApplicationService,
                fileStorageApplicationService,
                visitItemizationStorageApplicationService)
        {
            this._guarantorFilterService = guarantorFilterService;
        }

        [HttpGet]
        public ActionResult Itemizations()
        {
            return this.RedirectToAction("Documents", "Account");
        }

        [HttpGet]
        public PartialViewResult ItemizationsFilter()
        {
            ItemizationSearchFilterViewModel model = this.GetItemizationSearchFilter(this._guarantorFilterService.Value.VpGuarantorId);

            return this.PartialView("_ItemizationsFilter", model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [OverrideAuthorization, EmulateUser(AllowPost = true)]
        [Common.Web.Attributes.SessionState(SessionStateBehavior.Required)]
        public JsonResult Itemizations(GridSearchModel<ItemizationSearchFilterViewModel> filterModel)
        {
            ItemizationSearchResultsViewModel results = this.GetItemizations(this._guarantorFilterService.Value.VpGuarantorId, filterModel.Filter, filterModel.Page, filterModel.Rows, filterModel.Sidx, filterModel.Sord);

            return this.Json(results);
        }

        [HttpGet]
        [OverrideAuthorization, EmulateUser(AllowPost = true)]
        public async Task<ActionResult> ItemizationDownload(Guid id)
        {
            return await this.ItemizationDownload(id, this._guarantorFilterService.Value.VpGuarantorId, this.CurrentUserId);
        }
    }
}
﻿namespace Ivh.Web.Patient.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using System.Web.Mvc;
    using System.Web.SessionState;
    using Application.AppIntelligence.Common.Dtos;
    using Application.AppIntelligence.Common.Interfaces;
    using Application.Core.Common.Dtos;
    using Application.Core.Common.Interfaces;
    using Attributes;
    using Common.VisitPay.Enums;
    using Common.VisitPay.Strings;
    using Common.Web.Controllers;
    using Common.Web.Filters;
    using Common.Web.Interfaces;
    using Ivh.Application.Core.Common.Dtos.Eob;
    using Models.Demo;
    using Services;

    [RequiresAcknowledgement(Enabled = false)]
    [RequiresSsoAuthorization(Enabled = false)]
    [GuarantorAuthorize(Roles = VisitPayRoleStrings.System.Patient + "," + VisitPayRoleStrings.Admin.Administrator)]
    public class DemoController : BaseWebController
    {
        private readonly Lazy<GuarantorFilterService> _guarantorFilterService;
        private readonly Lazy<IRandomizedTestApplicationService> _randomizedTestApplicationService;
        private readonly Lazy<IVisitApplicationService> _visitApplicationService;
        private readonly Lazy<IVisitEobApplicationService> _visitEobApplicationService;

        private static readonly IDictionary<VisitPayFeatureEnum, RandomizedTestEnum> FeatureTests = new Dictionary<VisitPayFeatureEnum, RandomizedTestEnum>
        {
            {VisitPayFeatureEnum.DemoHealthEquityIsEnabled, RandomizedTestEnum.FeatureDemoHealthEquity},
            {VisitPayFeatureEnum.DemoInsuranceAccrualUiIsEnabled, RandomizedTestEnum.FeatureDemoInsuranceAccrualUi},
            {VisitPayFeatureEnum.DemoMyChartSSOUiIsEnabled, RandomizedTestEnum.FeatureDemoMyChartSsoUi},
            {VisitPayFeatureEnum.DemoPreServiceUiIsEnabled, RandomizedTestEnum.FeatureDemoPreServiceUi}
        };

        private const string PsArrangePaymentPaymentAmountKey = "Demo.PsArrangePayment.PaymentAmount";
        private const string DemoHealthEquityUpdatedDateKey = "Demo.HealthEquity.UpdatedDate";

        public DemoController(
            Lazy<IBaseControllerService> baseControllerService,
            Lazy<GuarantorFilterService> guarantorFilterService,
            Lazy<IRandomizedTestApplicationService> randomizedTestApplicationService,
            Lazy<IVisitApplicationService> visitApplicationService, 
            Lazy<IVisitEobApplicationService> visitEobApplicationService) : base(baseControllerService)
        {
            this._guarantorFilterService = guarantorFilterService;
            this._randomizedTestApplicationService = randomizedTestApplicationService;
            this._visitApplicationService = visitApplicationService;
            this._visitEobApplicationService = visitEobApplicationService;
        }

        #region preservice

        [RequireFeature(VisitPayFeatureEnum.DemoPreServiceUiIsEnabled)]
        public ActionResult PsIndexPartial()
        {
            if (!this.IsUserInFeatureTestGroup(VisitPayFeatureEnum.DemoPreServiceUiIsEnabled))
            {
                return new EmptyResult();
            }

            decimal.TryParse(this.Session[PsArrangePaymentPaymentAmountKey]?.ToString() ?? "0", out decimal paymentAmount);

            PsEstimatesViewModel estimate1 = new PsEstimatesViewModel
            {
                Date = DateTime.UtcNow.AddDays(30),
                Description = "Tonsillectomy",
                Deductible = 5000m,
                DeductibleMet = 1000m,
                Location = "Dr. Robertson – Washington General Hospital",
                HospitalCharges = new List<PsEstimateViewModel>
                {
                    new PsEstimateViewModel {Amount = 4000m, Description = "Hospital"},
                    new PsEstimateViewModel {Amount = 2000m, Description = "Physician"},
                    new PsEstimateViewModel {Amount = 1000m, Description = "Anesthesia"},
                },
                InsuranceCharges = new List<PsEstimateViewModel>
                {
                    new PsEstimateViewModel {Amount = decimal.Negate(4000), Description = "In-Network Savings"}
                },

            };

            if (paymentAmount > 0m)
            {
                estimate1.Payments = new List<PsEstimateViewModel>
                {
                    new PsEstimateViewModel {Amount = decimal.Negate(paymentAmount), Description = "Your Payment" }
                };
            }

            PsEstimatesViewModel estimate2 = new PsEstimatesViewModel
            {
                Date = DateTime.UtcNow.AddDays(90),
                Description = "Rhinoplasty",
                Deductible = 5000m,
                DeductibleMet = 1000m,
                Location = "Dr. Perry – Washington General Hospital",
                HospitalCharges = new List<PsEstimateViewModel>
                {
                    new PsEstimateViewModel {Amount = 15000m, Description = "Hospital"},
                    new PsEstimateViewModel {Amount = 5500m, Description = "Physician"},
                    new PsEstimateViewModel {Amount = 2000m, Description = "Anesthesia"},
                },
                InsuranceCharges = new List<PsEstimateViewModel>
                {
                    new PsEstimateViewModel {Amount = decimal.Negate(5500), Description = "In-Network Savings"},
                    new PsEstimateViewModel {Amount = decimal.Negate(8000), Description = "Estimated Insurance Payment"}
                }
            };

            IList<PsEstimatesViewModel> model = new List<PsEstimatesViewModel>
            {
                estimate1, estimate2
            };

            return this.PartialView("~/Views/Demo/_PSIndexPartial.cshtml", model);
        }

        [RequireFeature(VisitPayFeatureEnum.DemoPreServiceUiIsEnabled)]
        [HttpPost]
        [Common.Web.Attributes.SessionState(SessionStateBehavior.Required)]
        public ActionResult PsArrangePaymentSavePaymentAmount(decimal amount)
        {
            this.Session[PsArrangePaymentPaymentAmountKey] = amount;
            return this.Json(true);
        }

        #endregion

        #region mychart

        [RequireFeature(VisitPayFeatureEnum.DemoMyChartSSOUiIsEnabled)]
        public ActionResult ProviderPortalPartial()
        {
            if (!this.IsUserInFeatureTestGroup(VisitPayFeatureEnum.DemoMyChartSSOUiIsEnabled))
            {
                return new EmptyResult();
            }

            return this.PartialView("_ProviderPortalPartial");
        }

        [RequireFeature(VisitPayFeatureEnum.DemoMyChartSSOUiIsEnabled)]
        public ActionResult ProviderPortal()
        {
            if (!this.IsUserInFeatureTestGroup(VisitPayFeatureEnum.DemoMyChartSSOUiIsEnabled))
            {
                return new EmptyResult();
            }

            return this.View();
        }

        [RequireFeature(VisitPayFeatureEnum.DemoMyChartSSOUiIsEnabled)]
        public ActionResult ProviderPortalPayBill()
        {
            if (!this.IsUserInFeatureTestGroup(VisitPayFeatureEnum.DemoMyChartSSOUiIsEnabled))
            {
                return new EmptyResult();
            }

            return this.View();
        }

        #endregion

        #region health equity

        [RequireFeature(VisitPayFeatureEnum.DemoHealthEquityIsEnabled)]
        [Common.Web.Attributes.SessionState(SessionStateBehavior.Required)]
        public ActionResult HealthEquityPartial()
        {
            if (!this.IsUserInFeatureTestGroup(VisitPayFeatureEnum.DemoHealthEquityIsEnabled))
            {
                return new EmptyResult();
            }
            
            return this.PartialView("_HealthEquityDemoWidget", this.GetHealthEquityDemoModel());
        }

        [RequireFeature(VisitPayFeatureEnum.DemoHealthEquityIsEnabled)]
        [Common.Web.Attributes.SessionState(SessionStateBehavior.Required)]
        public ActionResult HealthEquityModal()
        {
            if (!this.IsUserInFeatureTestGroup(VisitPayFeatureEnum.DemoHealthEquityIsEnabled))
            {
                return new EmptyResult();
            }
            
            return this.PartialView("_HealthEquityDemoModal", this.GetHealthEquityDemoModel());
        }

        private HealhEquityDemoWidgetViewModel GetHealthEquityDemoModel()
        {
            HealhEquityDemoWidgetViewModel model = new HealhEquityDemoWidgetViewModel {
                CashBalance = 900m, 
                InvestmentBalance = 250m,
                YtdContributions = 1100m,
                YtdDistributions = 500m,
                UpdatedDate = this.GetHealthEquityUpdatedDate()
            };

            return model;
        }

        private string GetHealthEquityUpdatedDate()
        {
            DateTime.TryParse(this.Session[DemoHealthEquityUpdatedDateKey]?.ToString(), out DateTime updatedDate);
            if (updatedDate == default(DateTime))
            {
                updatedDate = DateTime.UtcNow;
            }
  
            if (DateTime.UtcNow.Subtract(updatedDate).TotalMinutes > 29d)
            {
                // the text says it refreshes every 30 min.
                updatedDate = DateTime.UtcNow;
            }
            
            this.Session[DemoHealthEquityUpdatedDateKey] = updatedDate;

            return this.TimeZoneHelper.UtcToClient(updatedDate).ToString(Format.TimeFormat);
        }

        #endregion

        [RequireFeature(VisitPayFeatureEnum.DemoInsuranceAccrualUiIsEnabled)]
        public ActionResult InsuranceAccrualsPartial()
        {
            if (!this.IsUserInFeatureTestGroup(VisitPayFeatureEnum.DemoInsuranceAccrualUiIsEnabled))
            {
                return new EmptyResult();
            }

            VisitResultsDto visits = this._visitApplicationService.Value.GetVisits(this._guarantorFilterService.Value.VisitPayUserId, new VisitFilterDto(), 1, null);
            string insurerName = string.Empty;
            string insurerLogo = string.Empty;

            //grab the first insurer found from this guarantors eob's
            foreach(VisitDto visit in visits.Visits)
            {
                IList<VisitEobDetailsResultDto> eobDetails = this._visitEobApplicationService.Value.GetVisitEobDetails(visit.BillingSystemId, visit.SourceSystemKey);
                VisitEobDetailsResultDto eobDetail = eobDetails.FirstOrDefault();
                
                if (eobDetail != null)
                {
                    insurerName = eobDetail.PayerName;
                    insurerLogo = eobDetail.PayerIcon;
                    break;
                }
            }

            InsuranceAccrualsViewModel model = new InsuranceAccrualsViewModel
            {
                InsurerName = insurerName,
                InsurerLogoUrl = insurerLogo
            };

            return this.PartialView("_ViewInsuranceAccrualsWidgetPartial", model);
        }

        private bool IsUserInFeatureTestGroup(VisitPayFeatureEnum visitPayFeatureEnum)
        {
            RandomizedTestEnum? randomizedTestEnum = FeatureTests.ContainsKey(visitPayFeatureEnum) ? FeatureTests[visitPayFeatureEnum] : (RandomizedTestEnum?)null;
            if (!randomizedTestEnum.HasValue)
            {
                return false;
            }

            RandomizedTestGroupDto testGroup = this._randomizedTestApplicationService.Value.GetRandomizedTestGroupMembership(randomizedTestEnum.Value, this._guarantorFilterService.Value.VpGuarantorId, false);
            return testGroup != null;
        }
    }
}
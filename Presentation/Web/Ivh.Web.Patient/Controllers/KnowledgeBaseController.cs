﻿namespace Ivh.Web.Patient.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Net;
    using System.Threading.Tasks;
    using System.Web.Mvc;
    using System.Web.SessionState;
    using Application.Core.Common.Dtos;
    using Application.Core.Common.Interfaces;
    using AutoMapper;
    using Common.Base.Utilities.Extensions;
    using Common.VisitPay.Enums;
    using Common.Web.Filters;
    using Common.Web.Interfaces;
    using Models.KnowledgeBase;
    using Services;

    public class KnowledgeBaseController : BaseController
    {
        private readonly Lazy<IKnowledgeBaseApplicationService> _knowledgeBaseApplicationService;

        public KnowledgeBaseController(
            Lazy<IBaseControllerService> baseControllerService,
            Lazy<GuarantorFilterService> guarantorFilterService,
            Lazy<IKnowledgeBaseApplicationService> knowledgeBaseApplicationService)
            : base(baseControllerService, guarantorFilterService)
        {
            this._knowledgeBaseApplicationService = knowledgeBaseApplicationService;
        }

        #region drawer

        [HttpGet]
        public async Task<ActionResult> HelpCenterDrawer()
        {
            List<KnowledgeBaseQuestionAnswerDto> questions = await this._knowledgeBaseApplicationService.Value.GetTopVotedKnowledgeBaseQuestionAnswersAsync(KnowledgeBaseApplicationTypeEnum.Web);
            List<KnowledgeBaseQuestionAnswerViewModel> model = Mapper.Map<List<KnowledgeBaseQuestionAnswerViewModel>>(questions);

            return this.PartialView("_HelpCenterDrawer", model);
        }

        #endregion

        [HttpGet]
        public async Task<ActionResult> Index(int? subcategoryId, int? categoryId, int? questionId)
        {
            //if every param is null use first category sorted by display order
            bool useCategoryId = ParameterIsNull(subcategoryId) && ParameterIsNull(categoryId) || ParameterIsNull(subcategoryId) && categoryId.HasValue && categoryId.Value > 0;

            IList<KnowledgeBaseCategoryDto> categories = await this._knowledgeBaseApplicationService.Value.GetKnowledgeBaseCategoriesAsync(KnowledgeBaseApplicationTypeEnum.Web);
            if (ParameterIsNull(categoryId))
            {
                categoryId = categories.OrderBy(x => x.DisplayOrder).First().KnowledgeBaseCategoryId;
            }

            List<KnowledgeBaseCategoryViewModel> categoryViewModels = Mapper.Map<List<KnowledgeBaseCategoryViewModel>>(categories);
            KnowledgeBaseSubCategoryDto subCategory = null;
            bool badQuestionParameter = false;

            if (useCategoryId)
            {
                KnowledgeBaseCategoryDto category = categories.FirstOrDefault(x => x.KnowledgeBaseCategoryId == categoryId.GetValueOrDefault(0));
                if (category != null)
                {
                    if (!questionId.HasValue)
                    {
                        subCategory = category.KnowledgeBaseSubCategories.OrderBy(x => x.DisplayOrder).FirstOrDefault();
                    }
                    else
                    {
                        subCategory = category.KnowledgeBaseSubCategories.FirstOrDefault(x => x.QuestionAnswers.Any(y => y.KnowledgeBaseQuestionAnswerId == questionId.Value));
                        if (subCategory == null)
                        {
                            badQuestionParameter = true;
                            subCategory = category.KnowledgeBaseSubCategories.OrderBy(x => x.DisplayOrder).FirstOrDefault();
                        }
                    }
                }
            }
            else
            {
                subCategory = categories.SelectMany(x => x.KnowledgeBaseSubCategories).OrderBy(x => x.DisplayOrder).FirstOrDefault(s => s.KnowledgeBaseSubCategoryId == subcategoryId);
            }

            List<KnowledgeBaseQuestionAnswerViewModel> questionAnswers = Mapper.Map<List<KnowledgeBaseQuestionAnswerViewModel>>(subCategory?.QuestionAnswers) ?? new List<KnowledgeBaseQuestionAnswerViewModel>();

            int? activeQuestion = null;
            if (!useCategoryId || badQuestionParameter)
            {
                if (ParameterIsNull(questionId) && !badQuestionParameter)
                {
                    activeQuestion = questionAnswers?.OrderBy(x => x.DisplayOrder).FirstOrDefault()?.KnowledgeBaseQuestionAnswerId;
                }
                else
                {
                    KnowledgeBaseQuestionAnswerViewModel questionAnswer = questionAnswers.FirstOrDefault(x => x.KnowledgeBaseQuestionAnswerId == questionId);
                    activeQuestion = questionAnswer?.KnowledgeBaseQuestionAnswerId ?? questionAnswers.OrderBy(x => x.DisplayOrder).FirstOrDefault()?.KnowledgeBaseQuestionAnswerId;
                }
            }

            //collection of distinct KB questions
            ICollection<SearchDatum> searchData = new HashSet<SearchDatum>();

            foreach (KnowledgeBaseSubCategoryDto subCategoryDto in categories.SelectMany(x => x.KnowledgeBaseSubCategories))
            {
                foreach (KnowledgeBaseQuestionAnswerDto question in subCategoryDto.QuestionAnswers)
                {
                    var actionParams = new
                    {
                        subcategoryId = subCategoryDto.KnowledgeBaseSubCategoryId,
                        categoryId = subCategoryDto.KnowledgeBaseCategoryId,
                        questionId = question.KnowledgeBaseQuestionAnswerId
                    };
                    string tags = question.Tags != null ? string.Join(" ", question.Tags) : "";

                    searchData.Add(new SearchDatum(WebUtility.HtmlDecode(question.QuestionContentBody), this.Url.Action("Index", actionParams), tags, actionParams));
                }
            }

            SearchViewModel searchViewModel = new SearchViewModel {SearchData = searchData.ToList()};

            return this.View(new KnowledgeBaseViewModel
            {
                Categories = categoryViewModels.OrderBy(x => x.DisplayOrder).ToList(),
                QuestionAnswers = questionAnswers.OrderBy(x => x.DisplayOrder).ToList(),
                ActiveCategoryId = subCategory?.KnowledgeBaseCategoryId,
                ActiveSubCategoryId = subCategory?.KnowledgeBaseSubCategoryId,
                ShowActiveCategoryContent = useCategoryId && ParameterIsNull(questionId),
                ActiveQuestionId = activeQuestion ?? questionId,
                Search = searchViewModel
            });
        }

        [HttpGet]
        public async Task<JsonResult> SearchData()
        {
            IList<KnowledgeBaseCategoryDto> categories = await this._knowledgeBaseApplicationService.Value.GetKnowledgeBaseCategoriesAsync(KnowledgeBaseApplicationTypeEnum.Web);

            ICollection<SearchDatum> searchData = new HashSet<SearchDatum>();

            foreach (KnowledgeBaseSubCategoryDto subCategoryDto in categories.SelectMany(x => x.KnowledgeBaseSubCategories))
            {
                foreach (KnowledgeBaseQuestionAnswerDto question in subCategoryDto.QuestionAnswers)
                {
                    var actionParams = new
                    {
                        subcategoryId = subCategoryDto.KnowledgeBaseSubCategoryId,
                        categoryId = subCategoryDto.KnowledgeBaseCategoryId,
                        questionId = question.KnowledgeBaseQuestionAnswerId
                    };
                    string tags = question.Tags != null ? string.Join(" ", question.Tags) : "";

                    searchData.Add(new SearchDatum(WebUtility.HtmlDecode(question.QuestionContentBody), this.Url.Action("Index", actionParams), tags, actionParams));
                }
            }

            SearchViewModel searchViewModel = new SearchViewModel {SearchData = searchData.ToList()};

            return this.Json(searchViewModel.SearchData, JsonRequestBehavior.AllowGet);
        }
        
        [HttpPost]
        [OverrideAuthorization, EmulateUser(AllowPost = true)]
        public async Task<PartialViewResult> SearchResults(KnowledgeBaseSearchFilter filter)
        {
            IList<KnowledgeBaseCategoryQuestionAnswersDto> knowledgeBaseCategoryQuestionsDto = await this._knowledgeBaseApplicationService.Value.GetKnowledgeBaseCategoryQuestionAnswersFlattenedAsync(KnowledgeBaseApplicationTypeEnum.Web);

            knowledgeBaseCategoryQuestionsDto = knowledgeBaseCategoryQuestionsDto.Where(x => filter.Questions.Select(y => y.CategoryId).Contains(x.KnowledgeBaseCategoryId)).ToList();
            foreach (KnowledgeBaseCategoryQuestionAnswersDto questionDto in knowledgeBaseCategoryQuestionsDto)
            {
                questionDto.Questions = questionDto.Questions.Where(x => filter.Questions.Select(y => y.QuestionId).Contains(x.KnowledgeBaseQuestionAnswerId)).ToList();
            }

            IList<KnowledgeBaseCategoryQuestionAnswersViewModel> model = Mapper.Map<IList<KnowledgeBaseCategoryQuestionAnswersViewModel>>(knowledgeBaseCategoryQuestionsDto);

            return this.PartialView("_SearchResults", model);
        }

        /*
        //
        // VP-3025 makes this obsolete
        //
        [HttpPost]
        [OverrideAuthorization, EmulateUser(AllowPost = true)]
        public async Task<PartialViewResult> PageBasedResults(string callingController, string callingAction)
        {
            IList<KnowledgeBaseCategoryQuestionAnswersDto> knowledgeBaseCategoryQuestionsDto = await this._knowledgeBaseApplicationService.Value.GetKnowledgeBaseCategoryQuestionAnswersForRouteAsync(KnowledgeBaseApplicationTypeEnum.Web, callingController, callingAction);

            IList<KnowledgeBaseCategoryQuestionAnswersViewModel> model = Mapper.Map<IList<KnowledgeBaseCategoryQuestionAnswersViewModel>>(knowledgeBaseCategoryQuestionsDto);

            return this.PartialView("_SearchResults", model);
        }*/

        [HttpPost]
        [OverrideAuthorization, EmulateUser(AllowPost = true)]
        public async Task<PartialViewResult> QuestionAnswer(int subcategoryId, int categoryId, int questionId)
        {
            KnowledgeBaseCategoryQuestionAnswersDto knowledgeBaseCategoryQuestionAnswerDto = await this._knowledgeBaseApplicationService.Value.GetKnowledgeBaseCategoryQuestionAnswerAsync(
                KnowledgeBaseApplicationTypeEnum.Web,
                subcategoryId,
                categoryId,
                questionId);

            IList<KnowledgeBaseCategoryQuestionAnswersViewModel> model = Mapper.Map<IList<KnowledgeBaseCategoryQuestionAnswersViewModel>>(knowledgeBaseCategoryQuestionAnswerDto.ToListOfOne());

            return this.PartialView("_SearchResults", model);
        }

        [HttpPost]
        [OverrideAuthorization, EmulateUser(AllowPost = true)]
        public async Task LogSearch(string searchText)
        {
            if (string.IsNullOrEmpty(searchText))
            {
                return;
            }

            await this._knowledgeBaseApplicationService.Value.LogSearchTextAsync(searchText);
        }
        
        private static bool ParameterIsNull(int? inputId)
        {
            return inputId == null || inputId == 0;
        }

        [HttpPost]
        public JsonResult SubmitKnowledgeBaseVote(int questionId, int voteValue)
        {
            this._knowledgeBaseApplicationService.Value.SaveKnowledgeBaseQuestionAnswerVote(questionId, voteValue);
            return null;
        }

        [HttpPost]
        public JsonResult SubmitKnowledgeBaseFeedback(int questionId, string feedbackText)
        {
            this._knowledgeBaseApplicationService.Value.SaveKnowledgeBaseQuestionAnswerFeedback(questionId, feedbackText);
            return null;
        }
    }
}
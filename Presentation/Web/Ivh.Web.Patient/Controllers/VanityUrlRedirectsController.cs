﻿namespace Ivh.Web.Patient.Controllers
{
    using System;
    using System.Web.Mvc;
    using Application.Core.Common.Dtos;
    using Application.Core.Common.Interfaces;
    using Attributes;
    using Common.VisitPay.Enums;
    using Common.Web.Controllers;
    using Common.Web.Filters;
    using Common.Web.Interfaces;
    using Domain.Visit.Interfaces;

    [NoLocalization]
    [GuarantorAuthorize, AllowOffline, OfflineOnly]
    [RequiresAcknowledgement(Enabled = false)]
    [RequireFeature(VisitPayFeatureEnum.OfflineVisitPay)]
    public class VanityUrlRedirectsController : BaseWebController
    {
        private Lazy<IVanityUrlApplicationService> _vanityApplicationService;

        public VanityUrlRedirectsController(
            Lazy<IVanityUrlApplicationService> vanityApplicationService,
            Lazy<IBaseControllerService> baseControllerService) 
            : base(baseControllerService)
        {
            this._vanityApplicationService = vanityApplicationService;
        }

        [HttpGet]
        [AllowAnonymous]
        public ActionResult Login(string vanityUrlKeyName)
        {
            VanityUrlDto vanityUrl = this._vanityApplicationService.Value.GetVanityUrl(vanityUrlKeyName);
            return this.RedirectToAction(vanityUrl.RedirectAction, vanityUrl.RedirectController);
        }
    }
}
﻿namespace Ivh.Web.Patient.Controllers
{
    using System;
    using System.Linq;
    using System.Threading.Tasks;
    using System.Web;
    using System.Web.Mvc;
    using System.Web.SessionState;
    using Application.Core.Common.Dtos;
    using Application.Core.Common.Interfaces;
    using AutoMapper;
    using Common.Base.Constants;
    using Common.Base.Enums;
    using Common.Base.Utilities.Helpers;
    using Common.VisitPay.Constants;
    using Common.VisitPay.Enums;
    using Common.Web.Filters;
    using Common.Web.Interfaces;
    using Domain.Logging.Interfaces;
    using Models.Survey;
    using Services;
    using SessionFacade;

    public class SurveyController : BaseController
    {
        private const string MetricSurveyPresented = "Survey.{0}Presented";
        private const string MetricSurveyIgnored = "Survey.{0}Ignored";
        private const string MetricSurveySubmitted = "Survey.{0}Submitted";
        
        protected readonly Lazy<ISurveyApplicationService> SurveyApplicationService;
        protected readonly Lazy<IWebPatientSessionFacade> PatientSession;

        public SurveyController(
            Lazy<IBaseControllerService> baseControllerService, 
            Lazy<GuarantorFilterService> guarantorFilterService,
            Lazy<ISurveyApplicationService> surveyApplicationService,
            Lazy<IWebPatientSessionFacade> patientSession) 
            : base(baseControllerService, guarantorFilterService)
        {
            this.SurveyApplicationService = surveyApplicationService;
            this.PatientSession = patientSession;
        }
        
        [HttpPost]
        [OverrideAuthorization, EmulateUser(AllowPost = true)]
        public  JsonResult ShouldShowSurvey(string surveyName)
        {
            if (this.IsCurrentUserEmulating())
            {
                return this.Json(new {UserIsPresentedWithSurvey = false});
            }

            // Allow Paper Statement Survey to be show even if user has completed another survey 
            if (surveyName == SurveyNames.PaperStatementData)
            {
                return this.Json(new { UserIsPresentedWithSurvey = true });
            }

            bool userHasBeenSurveyedInSession = this.PatientSession.Value.SurveyHasBeenPresentedInSession;
            SurveyPresentationInfoDto showSurvey = this.SurveyApplicationService.Value.ShouldShowSurveyToVisitPayUser(this.CurrentUserId, surveyName, userHasBeenSurveyedInSession);
            return this.Json(showSurvey);
        }

        [HttpGet]
        public async Task<PartialViewResult> ShowSurvey(SurveyInfoViewModel surveyInfo)
        {
            SurveyDto surveyDto = await this.SurveyApplicationService.Value.GetSurvey(surveyInfo.SurveyName);
            SurveyResultViewModel surveyResultViewModel = Mapper.Map<SurveyResultViewModel>(surveyDto);
            Mapper.Map(surveyInfo, surveyResultViewModel);
            //select out the nps question 
            SurveyResultAnswerViewModel npsQuestion = surveyResultViewModel.SurveyQuestions.FirstOrDefault(x => x.SurveyRatingGroupId == (int)SurveyRatingGroupTypeEnum.NetPromoterScoreFormat);
            surveyResultViewModel.SurveyQuestions.Remove(npsQuestion);
            surveyResultViewModel.NpsRatingQuestion = npsQuestion;

            this.IncrementMetric(MetricSurveyPresented, surveyDto.SurveyName);
            return this.PartialView("~/Views/Survey/_Survey.cshtml", surveyResultViewModel);
        }

        [HttpGet]
        public async Task<PartialViewResult> ShowNpsSurvey(SurveyInfoViewModel surveyInfo)
        {
            SurveyDto surveyDto = await this.SurveyApplicationService.Value.GetSurvey(SurveyNames.NetPromoterScore);
            if (surveyDto == null ||
                this.PatientSession.Value.SurveyHasBeenPresentedInSession ||
                (surveyDto.UseNavigationCount && this.PatientSession.Value.HomepageNavigationCount <= 1))
            {
                return null;
            }

            SurveyResultViewModel surveyResultViewModel = Mapper.Map<SurveyResultViewModel>(surveyDto);

            //select out the nps question 
            SurveyResultAnswerViewModel npsQuestion = surveyResultViewModel.SurveyQuestions.FirstOrDefault(x => x.SurveyRatingGroupId == (int)SurveyRatingGroupTypeEnum.NetPromoterScoreFormat);
            surveyResultViewModel.SurveyQuestions.Remove(npsQuestion);
            surveyResultViewModel.NpsRatingQuestion = npsQuestion;

            this.IncrementMetric(MetricSurveyPresented, surveyDto.SurveyName);
            return this.PartialView("~/Views/Survey/_NpsSurvey.cshtml", surveyResultViewModel);
        }
        
        [HttpPost]
        [OverrideAuthorization, EmulateUser(AllowPost = true)]
        [Common.Web.Attributes.SessionState(SessionStateBehavior.Required)]
        public JsonResult ProcessSubmittedSurvey(SurveyResultViewModel surveyResult)
        {
            if (this.IsCurrentUserEmulating())
            {
                return this.Json(false);
            }

            surveyResult.SurveyResultType = SurveyResultTypeEnum.Submitted;
            this.ProcessSurvey(surveyResult);
            return this.Json(true);
        }
        
        [HttpPost]
        [ValidateAntiForgeryToken]
        [OverrideAuthorization, EmulateUser(AllowPost = true)]
        [Common.Web.Attributes.SessionState(SessionStateBehavior.Required)]
        public JsonResult ProcessIgnoredSurvey(SurveyResultViewModel surveyResult)
        {
            if (this.IsCurrentUserEmulating())
            {
                return this.Json(false);
            }

            surveyResult.SurveyResultType = SurveyResultTypeEnum.Ignored;
            this.ProcessSurvey(surveyResult);
            return this.Json(true);
        }

        [NonAction]
        public void ProcessSurvey(SurveyResultViewModel surveyResult)
        {
            this.PatientSession.Value.SurveyHasBeenPresentedInSession = true;

            surveyResult.SurveyDate = DateTime.UtcNow;
            surveyResult.SetDeviceType(System.Web.HttpContext.Current);
            if (surveyResult.NpsRatingQuestion != null)
            {
                surveyResult.NpsRatingQuestion.QuestionText = HttpUtility.HtmlDecode(surveyResult.NpsRatingQuestion?.QuestionText ?? string.Empty);
            }
            
            foreach (SurveyResultAnswerViewModel qa in surveyResult.SurveyQuestions)
            {
                qa.QuestionText = HttpUtility.HtmlDecode(qa.QuestionText ?? string.Empty);
            }

            // append NPS survey question if applicable
            if (surveyResult.NpsRatingQuestion !=null)
            {
                surveyResult.SurveyQuestions.Add(surveyResult.NpsRatingQuestion);
            }
            SurveyResultDto surveyResultDto = Mapper.Map<SurveyResultDto>(surveyResult);

            surveyResultDto.VisitPayUserId = this.CurrentUserId;
            this.SurveyApplicationService.Value.ProcessSurveyResult(surveyResultDto);

            if (surveyResultDto.SurveyResultType == SurveyResultTypeEnum.Ignored)
            {
                this.IncrementMetric(MetricSurveyIgnored, surveyResult.SurveyName);
            }
            else
            {
                this.IncrementMetric(MetricSurveySubmitted, surveyResult.SurveyName);
            }
            
        }

        private void IncrementMetric(string metricKeyTemplate, string surveyName)
        {
            string metric = string.Format(metricKeyTemplate, ReplacementUtility.RemoveWhiteSpace(surveyName));
            this.MetricsProvider.Value.Increment(metric);
        }
    }
}
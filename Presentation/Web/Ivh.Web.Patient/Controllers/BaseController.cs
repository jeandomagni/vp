﻿namespace Ivh.Web.Patient.Controllers
{
    using System;
    using Attributes;
    using Common.VisitPay.Strings;
    using Common.Web.Controllers;
    using Common.Web.Interfaces;
    using Services;

    [GuarantorAuthorize(Roles = VisitPayRoleStrings.System.Patient + "," + VisitPayRoleStrings.Admin.Administrator)]
    public class BaseController : BaseWebController
    {
        protected readonly Lazy<GuarantorFilterService> GuarantorFilterService;

        public BaseController(
            Lazy<IBaseControllerService> baseControllerService,
            Lazy<GuarantorFilterService> guarantorFilterService)
            : base(baseControllerService)
        {
            this.GuarantorFilterService = guarantorFilterService;
        }
    }
}
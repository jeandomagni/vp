﻿namespace Ivh.Web.Patient.Controllers
{
    using System;
    using System.Web.Mvc;
    using Common.Web.Controllers;
    using Common.Web.Interfaces;

    [AllowAnonymous]
    public class OneTimeController : BaseWebController
    {
        public OneTimeController(Lazy<IBaseControllerService> baseControllerService)
            : base(baseControllerService)
        {
        }

        [AllowAnonymous]
        public ActionResult Index()
        {
            string redirect = this.ClientDto.AppGuarantorExpressUrlHost;

            return this.Redirect(string.IsNullOrEmpty(redirect) ? "/" : redirect);
        }
    }
}
﻿namespace Ivh.Web.Patient.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using System.Web;
    using System.Web.Mvc;
    using System.Web.SessionState;
    using Application.Content.Common.Interfaces;
    using Application.Core.Common.Dtos;
    using Application.Core.Common.Interfaces;
    using Application.FinanceManagement.Common.Dtos;
    using Application.FinanceManagement.Common.Interfaces;
    using Attributes;
    using AutoMapper;
    using Common.Base.Utilities.Helpers;
    using Common.EventJournal;
    using Common.VisitPay.Enums;
    using Common.VisitPay.Strings;
    using Common.Web.Controllers;
    using Common.Web.Filters;
    using Common.Web.Interfaces;
    using Common.Web.Models;
    using Common.Web.Models.Account;
    using Common.Web.Models.FinancePlan;
    using Common.Web.Models.Payment;
    using Microsoft.AspNet.Identity;
    using Provider.Pdf;
    using Services;
    using SessionFacade;

    [GuarantorAuthorize(Roles = VisitPayRoleStrings.System.Patient + "," + VisitPayRoleStrings.Admin.Administrator)]
    public class FinancePlanController : BaseFinancePlanController
    {
        private readonly Lazy<GuarantorFilterService> _guarantorFilterService;
        protected readonly Lazy<IWebPatientSessionFacade> WebPatientSessionFacade;

        public FinancePlanController(
            Lazy<IBaseControllerService> baseControllerService,
            Lazy<IContentApplicationService> contentApplicationService,
            Lazy<IPdfConverter> pdfConverter,
            Lazy<IFinancialDataSummaryApplicationService> financialDataSummaryApplicationService,
            Lazy<IFinancePlanApplicationService> financePlanApplicationService,
            Lazy<IGuarantorApplicationService> guarantorApplicationService,
            Lazy<IPaymentDetailApplicationService> paymentDetailApplicationService,
            Lazy<IPaymentMethodsApplicationService> paymentMethodsApplicationService,
            Lazy<IReconfigureFinancePlanApplicationService> reconfigureFinancePlanApplicationService,
            Lazy<IVisitApplicationService> visitApplicationService,
            Lazy<IVisitPayUserApplicationService> visitPayUserApplicationService,
            Lazy<GuarantorFilterService> guarantorFilterService,
            Lazy<IWebPatientSessionFacade> webPatientSessionFacade)
            : base(baseControllerService,
                contentApplicationService,
                financialDataSummaryApplicationService,
                financePlanApplicationService,
                guarantorApplicationService,
                paymentDetailApplicationService,
                paymentMethodsApplicationService,
                reconfigureFinancePlanApplicationService,
                visitApplicationService,
                visitPayUserApplicationService,
                pdfConverter)
        {
            this._guarantorFilterService = guarantorFilterService;
            this.WebPatientSessionFacade = webPatientSessionFacade;
        }

        const string FinancePlansExportKey = "FinancePlansExport";

        #region finance plan setup

        [HttpPost]
        [OverrideAuthorization, EmulateUser(AllowPost = true)]
        [ValidateAntiForgeryToken]
        public async Task<JsonResult> FinancePlanValidate(int statementId, decimal? monthlyPaymentAmount, int? numberMonthlyPayments, int? financePlanOfferSetTypeId, bool combineFinancePlans, int? paymentDueDay, string financePlanOptionStateCode)
        {
            int vpGuarantorId = this._guarantorFilterService.Value.VpGuarantorId;

            FinancePlanTermsResponseViewModel response = await this.ValidateFinancePlanAsync(
                vpGuarantorId, 
                statementId, 
                monthlyPaymentAmount, 
                numberMonthlyPayments, 
                financePlanOfferSetTypeId, 
                combineFinancePlans, 
                paymentDueDay, 
                null,
                financePlanOptionStateCode).ConfigureAwait(false);

            return this.Json(response);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [AllowOffline]
        public async Task<JsonResult> FinancePlanSubmit(ConfirmFinancePlanViewModel model)
        {
            int filteredVisitPayUserId = this._guarantorFilterService.Value.VisitPayUserId;
            int vpGuarantorId = this._guarantorFilterService.Value.VpGuarantorId;
            
            if (model.PaymentDueDay.HasValue)
            {
                // update payment due day before creating finance plan
                // should pass managing guarantor vp user id, even if setting up a managed fp
                this.GuarantorApplicationService.Value.ChangePaymentDueDay(this.CurrentUserId, model.PaymentDueDay.Value, true, this.CurrentUserId);
            }
            
            // creating finance plan
            FinancePlanResultsModel financePlanResultsModel = await this.FinancePlanSubmitAsync(model, filteredVisitPayUserId, vpGuarantorId);
            financePlanResultsModel.StackedAndCombinedFinancePlanOptionsPresented = this.WebPatientSessionFacade.Value.StackedAndCombinedFinancePlanOptionsPresented.GetValueOrDefault();
            
            return this.Json(financePlanResultsModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [AllowOffline]
        public JsonResult FinancePlanCancel(int financePlanId)
        {
            this.FinancePlanApplicationService.Value.CancelFinancePlan(this._guarantorFilterService.Value.VisitPayUserId, financePlanId, "Guarantor Cancelled Pre-Originated Finance Plan", this.CurrentUserId);

            return this.Json(true);
        }

        #endregion
        
        #region reconfigure finance plans
        
        [HttpGet]
        [Common.Web.Attributes.SessionState(SessionStateBehavior.Required)]
        public async Task<ActionResult> ReconfigureFinancePlan(int id, Guid? v)
        {
            if (v.HasValue)
            {
                this._guarantorFilterService.Value.SetGuarantor(v.Value);
            }

            int vpGuarantorId = this._guarantorFilterService.Value.VpGuarantorId;

			ReconfigureFinancePlanViewModel model = await this.GetReconfigureTermsModelAsync(id, vpGuarantorId, null);
            model.ReconfigureAcceptUrl = this.Url.Action("ReconfigureAccept", "FinancePlan");
            model.ReconfigureCancelUrl = this.Url.Action("ReconfigureCancel", "FinancePlan");
            model.ReconfigureSubmitUrl = this.Url.Action("ReconfigureSubmit", "FinancePlan");
            model.ReconfigureValidateUrl = this.Url.Action("ReconfigureValidate", "FinancePlan");
            model.IsEditable = model.Terms == null;

            if (model.Terms != null && model.TermsCmsVersionId != default(int))
            {
                model.TermsCmsVersionId = model.Terms.TermsCmsVersionId;
            }
            else
            {
                int? ricCmsRegionId = this.VisitApplicationService.Value.GetRicCmsRegionId(vpGuarantorId);
                CmsRegionEnum cmsRegionEnum = !ricCmsRegionId.HasValue || ricCmsRegionId == default(int) ? CmsRegionEnum.VppCreditAgreement : (CmsRegionEnum) ricCmsRegionId;

                model.TermsCmsVersionId = (await this.ContentApplicationService.Value.GetCurrentVersionAsync(cmsRegionEnum, false)).CmsVersionId;
            }

            int currentVpGuarantorId = this.VisitPayUserApplicationService.Value.GetGuarantorIdFromVisitPayUserId(this.CurrentUserId);
            model.PrimaryPaymentMethod = Mapper.Map<PaymentMethodListItemViewModel>(this.PaymentMethodsApplicationService.Value.GetPrimaryPaymentMethod(currentVpGuarantorId));
            
            return this.View(model);
        }
        
        [HttpPost]
        [ValidateAntiForgeryToken]
        [AllowOffline]
        public async Task<JsonResult> ReconfigureAccept(int financePlanIdToReconfigure, int termsCmsVersionId)
        {
            int vpGuarantorId = this._guarantorFilterService.Value.VpGuarantorId;
            int loggedInVisitPayUserId = this.CurrentUserId;
            HttpContext context = System.Web.HttpContext.Current;
            JournalEventHttpContextDto journalContext = Mapper.Map<JournalEventHttpContextDto>(context);

            ReconfigurationActionResponseDto responseDto = await this.ReconfigureFinancePlanApplicationService.Value.AcceptReconfigurationAsync(financePlanIdToReconfigure, termsCmsVersionId, vpGuarantorId, loggedInVisitPayUserId, journalContext);
            ReconfigurationActionResponseViewModel model = new ReconfigurationActionResponseViewModel(responseDto, this.GetPostReconfigureRedirectUrl(vpGuarantorId));

            return this.Json(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [AllowOffline]
        public JsonResult ReconfigureCancel(int financePlanIdToReconfigure)
        {
            int vpGuarantorId = this._guarantorFilterService.Value.VpGuarantorId;
            int loggedInVisitPayUserId = this.CurrentUserId;
            HttpContext context = System.Web.HttpContext.Current;
            JournalEventHttpContextDto journalContext = Mapper.Map<JournalEventHttpContextDto>(context);

            ReconfigurationActionResponseDto responseDto = this.ReconfigureFinancePlanApplicationService.Value.CancelReconfiguration(financePlanIdToReconfigure, vpGuarantorId, loggedInVisitPayUserId, "Reconfiguration was declined.", journalContext);
            ReconfigurationActionResponseViewModel model = new ReconfigurationActionResponseViewModel(responseDto, this.GetPostReconfigureRedirectUrl(vpGuarantorId));

            return this.Json(model);
        }
        
        [HttpPost]
        [AllowOffline]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ReconfigureSubmit(int financePlanIdToReconfigure, decimal monthlyPaymentAmount, int? financePlanOfferSetTypeId)
        {
            int vpGuarantorId = this._guarantorFilterService.Value.VpGuarantorId;
            ReconfigurationActionResponseDto responseDto = await this.SubmitReconfiguredFinancePlanAsync(vpGuarantorId, financePlanIdToReconfigure, monthlyPaymentAmount, financePlanOfferSetTypeId);
            ReconfigurationActionResponseViewModel model = new ReconfigurationActionResponseViewModel(responseDto, this.GetPostReconfigureRedirectUrl(vpGuarantorId));

            return this.Json(model);
        }
        
        [HttpPost]
        [OverrideAuthorization, EmulateUser(AllowPost = true)]
        [AllowOffline]
        [ValidateAntiForgeryToken]
        public async Task<JsonResult> ReconfigureValidate(int financePlanIdToReconfigure, decimal? monthlyPaymentAmount, int? numberMonthlyPayments, int? financePlanOfferSetTypeId)
        {
            int vpGuarantorId = this._guarantorFilterService.Value.VpGuarantorId;
            FinancePlanTermsResponseViewModel model = await this.ValidateReconfiguredFinancePlanAsync(vpGuarantorId, financePlanIdToReconfigure, monthlyPaymentAmount, numberMonthlyPayments, financePlanOfferSetTypeId);
            
            return this.Json(model);
        }

        private string GetPostReconfigureRedirectUrl(int vpGuarantorId)
        {
            GuarantorDto guarantorDto = this.GuarantorApplicationService.Value.GetGuarantor(vpGuarantorId);
            if (guarantorDto.IsOfflineGuarantor)
            {
                return this.Url.Action(nameof(OfflineController.Index), "Offline");
            }
            else
            {
                return this.Url.Action(nameof(PaymentController.MyPayments), "Payment");
            }
        }

        #endregion

        #region my finance plans + modals

        [HttpGet]
        public ActionResult MyFinancePlans()
        {
            return this.RedirectToAction("MyPayments", "Payment");
        }
        
        [HttpPost]
        [OverrideAuthorization, EmulateUser(AllowPost = true)]
        [ValidateAntiForgeryToken]
        public JsonResult MyFinancePlans(FinancePlansSearchFilterViewModel model, int page, int rows, string sidx, string sord)
        {
            int currentVpGuarantorId = this.GetVpGuarantorId(this.CurrentUserId);

            model.FinancePlanStatusIds = EnumHelper<FinancePlanStatusEnum>.GetValuesNot(FinancePlanStatusEnumCategory.Pending).Select(x => (int) x).ToList();

            FinancePlansSearchResultsViewModel results = this.FinancePlans(this._guarantorFilterService.Value.VpGuarantorId, model, page, rows, sidx, sord, currentVpGuarantorId);

            return this.Json(new FinancePlanHistoryResultsModel(results.page, rows, results.records)
            {
                Results = results.FinancePlans
            });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Common.Web.Attributes.SessionState(SessionStateBehavior.Required)]
        public async Task<ActionResult> MyFinancePlansExport(FinancePlansSearchFilterViewModel model, string sidx, string sord)
        {
            model.FinancePlanStatusIds = EnumHelper<FinancePlanStatusEnum>.GetValuesNot(FinancePlanStatusEnumCategory.Pending).Select(x => (int)x).ToList();

            //
            FinancePlanFilterDto financePlanFilterDto = new FinancePlanFilterDto
            {
                SortField = sidx,
                SortOrder = sord
            };
            Mapper.Map(model, financePlanFilterDto);

            //
            byte[] bytes = await this.FinancePlanApplicationService.Value.ExportFinancePlansAsync(this._guarantorFilterService.Value.VisitPayUserId, financePlanFilterDto, this.User.Identity.GetUserName());
            this.SetTempData(FinancePlansExportKey, bytes);

            //
            return this.Json(new ResultMessage(true, this.Url.Action("MyFinancePlansExportDownload")));
        }

        [HttpGet]
        public ActionResult MyFinancePlansExportDownload()
        {
            object tempData = this.TempData[FinancePlansExportKey];
            if (tempData != null)
            {
                this.WriteExcelInline((byte[])tempData, "FinancePlans.xlsx");
            }

            return this.Json(new { }, JsonRequestBehavior.AllowGet);
        }
        
        #endregion

        #region finance plan visits / details

        [HttpPost]
        [ValidateAntiForgeryToken]
        public PartialViewResult FinancePlanDetail(int financePlanId)
        {
            FinancePlanDto financePlanDto = this.FinancePlanApplicationService.Value.GetFinancePlan(this._guarantorFilterService.Value.VisitPayUserId, financePlanId);
            this.FinancePlanApplicationService.Value.SetAmountDueForDisplay(financePlanDto);

            FinancePlanDetailsViewModel model = Mapper.Map<FinancePlanDetailsViewModel>(financePlanDto);
            
            VisitResultsDto visitResultsDto = this.VisitApplicationService.Value.GetVisits(this._guarantorFilterService.Value.VisitPayUserId, new VisitFilterDto
            {
                FinancePlanId = financePlanDto.FinancePlanId,
                SortField = nameof(VisitDto.DischargeDate),
                SortOrder = "desc",
            }, 0, 100);

            model.Visits = Mapper.Map<List<VisitsSearchResultViewModel>>(visitResultsDto.Visits);

            return this.PartialView("_FinancePlanDetail", model);
        }
        
        #endregion
    }
}
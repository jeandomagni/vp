﻿namespace Ivh.Web.Patient.Controllers
{
    using System;
    using Application.Base.Common.Interfaces;
    using Application.Core.Common.Interfaces;
    using Application.FinanceManagement.Common.Interfaces;
    using Attributes;
    using Base;
    using Common.VisitPay.Strings;
    using Common.Web.Interfaces;
    using Services;

    [GuarantorAuthorize(Roles = VisitPayRoleStrings.System.Patient + "," + VisitPayRoleStrings.Admin.Administrator)]
    public class SidebarController : BaseSidebarController
    {
        public SidebarController(
            Lazy<IBaseControllerService> baseControllerService,
            Lazy<IExternalLinkApplicationService> externalLinkApplicationService,
            Lazy<IStatementApplicationService> statementApplicationService,
            Lazy<GuarantorFilterService> guarantorFilterService,
            Lazy<IFinancePlanApplicationService> financePlanApplicationService,
            Lazy<IVisitBalanceBaseApplicationService> visitBalanceBaseApplicationService)
            : base(baseControllerService,
                externalLinkApplicationService,
                guarantorFilterService,
                financePlanApplicationService,
                visitBalanceBaseApplicationService)
        {
        }
    }
}
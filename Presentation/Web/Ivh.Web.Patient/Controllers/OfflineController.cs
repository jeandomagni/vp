﻿namespace Ivh.Web.Patient.Controllers
{
    using Application.Content.Common.Dtos;
    using Application.Content.Common.Interfaces;
    using Application.Core.Common.Dtos;
    using Application.Core.Common.Interfaces;
    using Application.FinanceManagement.Common.Dtos;
    using Application.FinanceManagement.Common.Interfaces;
    using Application.User.Common.Dtos;
    using Application.User.Common.Interfaces;
    using Attributes;
    using AutoMapper;
    using Common.Base.Utilities.Extensions;
    using Common.Base.Utilities.Helpers;
    using Common.EventJournal;
    using Common.VisitPay.Constants;
    using Common.VisitPay.Enums;
    using Common.VisitPay.Strings;
    using Common.Web.Controllers;
    using Common.Web.Extensions;
    using Common.Web.Filters;
    using Common.Web.Interfaces;
    using Common.Web.Models;
    using Common.Web.Models.Payment;
    using Common.Web.Models.Statement;
    using Common.Web.Utilities;
    using Microsoft.AspNet.Identity;
    using Models.Offline;
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using System.Web.Mvc;
    using Common.Web.Models.FinancePlan;

    [NoLocalization]
    [GuarantorAuthorize, AllowOffline, OfflineOnly]
    [RequiresAcknowledgement(Enabled = false)]
    [RequireFeature(VisitPayFeatureEnum.OfflineVisitPay)]
    public class OfflineController : BaseWebController
    {
        private readonly Lazy<IFinancePlanApplicationService> _financePlanApplicationService;
        private readonly Lazy<IReconfigureFinancePlanApplicationService> _reconfigureFinancePlanApplicationService;
        private readonly Lazy<IPaymentOptionApplicationService> _paymentOptionApplicationService;
        private readonly Lazy<IStatementApplicationService> _statementApplicationService;
        private readonly Lazy<IVisitPayUserApplicationService> _visitPayUserApplicationService;
        private readonly Lazy<IOfflineUserApplicationService> _offlineUserApplicationService;
        private readonly Lazy<IGuarantorApplicationService> _guarantorApplicationService;
        private readonly Lazy<ISsoApplicationService> _ssoApplicationService;
        private readonly Lazy<IContentApplicationService> _contentApplicationService;
        private readonly Lazy<IVisitApplicationService> _visitApplicationService;
        private readonly Lazy<IPaymentMethodsApplicationService> _paymentMethodsApplicationService;
        private readonly Lazy<IVisitPayUserJournalEventApplicationService> _visitPayUserJournalEventApplicationService;

        public OfflineController(
            Lazy<IBaseControllerService> baseControllerService,
            Lazy<IFinancePlanApplicationService> financePlanApplicationService,
            Lazy<IReconfigureFinancePlanApplicationService> reconfigureFinancePlanApplicationService,
            Lazy<IPaymentOptionApplicationService> paymentOptionApplicationService,
            Lazy<IStatementApplicationService> statementApplicationService,
            Lazy<IVisitPayUserApplicationService> visitPayUserApplicationService,
            Lazy<IOfflineUserApplicationService> offlineUserApplicationService,
            Lazy<IGuarantorApplicationService> guarantorApplicationService,
            Lazy<ISsoApplicationService> ssoApplicationService,
            Lazy<IContentApplicationService> contentApplicationService,
            Lazy<IVisitApplicationService> visitApplicationService,
            Lazy<IPaymentMethodsApplicationService> paymentMethodsApplicationService,
            Lazy<IVisitPayUserJournalEventApplicationService> visitPayUserJournalEventApplicationService) : base(baseControllerService)
        {
            this._financePlanApplicationService = financePlanApplicationService;
            this._reconfigureFinancePlanApplicationService = reconfigureFinancePlanApplicationService;
            this._paymentOptionApplicationService = paymentOptionApplicationService;
            this._statementApplicationService = statementApplicationService;
            this._visitPayUserApplicationService = visitPayUserApplicationService;
            this._offlineUserApplicationService = offlineUserApplicationService;
            this._guarantorApplicationService = guarantorApplicationService;
            this._ssoApplicationService = ssoApplicationService;
            this._contentApplicationService = contentApplicationService;
            this._visitApplicationService = visitApplicationService;
            this._paymentMethodsApplicationService = paymentMethodsApplicationService;
            this._visitPayUserJournalEventApplicationService = visitPayUserJournalEventApplicationService;
        }
        
        [HttpGet]
        [AllowAnonymous]
        public ActionResult Login(string t)
        {
            if (string.IsNullOrEmpty(t))
            {
                this.Logger.Value.Info(() => $"Offline Login without token: {this.Request.RawUrl}");
                return this.RedirectToAction("NotFound", "Error");
            }

            TokenLoginViewModel model = new TokenLoginViewModel
            {
                Token = t.TrimNullSafe()
            };
            
            return this.View(model);
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult Login(TokenLoginViewModel model)
        {
            if (!this.ValidateModelState($"{nameof(OfflineController)}:{nameof(this.Login)}"))
            {
                this.ModelState.AddModelError(string.Empty, LocalizationHelper.GetLocalizedString(TextRegionConstants.ValidationAllFieldsRequired));
                return this.View(model);
            }
            
            DateTime dateOfBirth = DateTimeHelper.FromValues(model.DateOfBirthMonth, model.DateOfBirthDay, model.DateOfBirthYear);
            SignInResultDto signInResult = this._offlineUserApplicationService.Value.SignIn(
                model.LastName,
                dateOfBirth,
                model.Token,
                Mapper.Map<JournalEventHttpContextDto>(System.Web.HttpContext.Current));

            if (signInResult.SignInStatus == SignInStatusExEnum.Success)
            {
                bool hasPendingFinancePlan = this._financePlanApplicationService.Value.HasFinancePlans(signInResult.VisitPayUserId, EnumHelper<FinancePlanStatusEnum>.GetValues(FinancePlanStatusEnumCategory.Pending).ToList());
                if (hasPendingFinancePlan)
                {
                    return this.RedirectToAction("FinancePlan");
                }

                bool hasPendingReconfiguration = this._reconfigureFinancePlanApplicationService.Value.HasPendingReconfiguration(signInResult.VisitPayUserId);
                if (hasPendingReconfiguration)
                {
                    return this.RedirectToAction("ReconfigureFinancePlan");
                }

                return this.RedirectToAction("Index");
            }

            if (signInResult.SignInStatus  == SignInStatusExEnum.PasswordExpired)
            {
                this.ModelState.AddModelError(string.Empty, LocalizationHelper.GetLocalizedString(TextRegionConstants.LinkExpiredAndResent));
            }
            else
            {
                this.ModelState.AddModelError(string.Empty, LocalizationHelper.GetLocalizedString(TextRegionConstants.PleaseEnsureInformationIsCorrect));
            }

            return this.View(model);
        }

        [HttpGet]
        [AllowAnonymous]
        public ActionResult PaperCommunicationLogin() => this.View();

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> PaperCommunicationLogin(AccessTokenPhiLoginViewModel model)
        {
            if (!this.ValidateModelState($"{nameof(OfflineController)}:{nameof(this.PaperCommunicationLogin)}"))
            {
                this.ModelState.AddModelError(string.Empty, LocalizationHelper.GetLocalizedString(TextRegionConstants.ValidationAllFieldsRequired));
                return this.View(model);
            }

            DateTime dob = DateTimeHelper.FromValues(model.DateOfBirthMonth, model.DateOfBirthDay, model.DateOfBirthYear);
            JournalEventHttpContextDto journalEventHttpContextDto = Mapper.Map<JournalEventHttpContextDto>(System.Web.HttpContext.Current);

            SignInResultDto signInResult = 
                this._offlineUserApplicationService.Value
                    .SignIn(model.LastName, dob, model.AccessTokenPhi, journalEventHttpContextDto);

            string requestUserHostAddress = this.ControllerContext.HttpContext.Request.UserHostAddress;

            if (this.IsFailureSignInResult(signInResult.SignInStatus))
            {
                this.Logger.Value.Info(() => $"{nameof(OfflineController)}:{nameof(this.PaperCommunicationLogin)}: Sign-in status \"{signInResult.SignInStatus.ToString()}\" for user host address <{requestUserHostAddress}>");
            }

            await this.BruteForceDelayAsync(
                requestUserHostAddress, 
                () => this.IsFailureSignInResult(signInResult.SignInStatus),
                () => signInResult.SignInStatus.Equals(SignInStatusExEnum.Success));

            if (signInResult.SignInStatus == SignInStatusExEnum.Success)
            {
                if (this._financePlanApplicationService.Value.HasFinancePlans(signInResult.VisitPayUserId, EnumHelper<FinancePlanStatusEnum>.GetValues(FinancePlanStatusEnumCategory.Pending).ToList()))
                {
                    return this.RedirectToAction("FinancePlan");
                }

                return this.RedirectToAction("Index");
            }

            if (signInResult.SignInStatus == SignInStatusExEnum.PasswordExpired)
            {
                this.ModelState.AddModelError(string.Empty, LocalizationHelper.GetLocalizedString(TextRegionConstants.LinkExpiredAndResent));
            }
            else
            {
                this.ModelState.AddModelError(string.Empty, LocalizationHelper.GetLocalizedString(TextRegionConstants.PleaseEnsureInformationIsCorrect));
            }

            return this.View(model);
        }

        [HttpGet]
        [AllowAnonymous]
        public ActionResult Logoff()
        {
            if (this.User.Identity.IsAuthenticated)
            {
                this._visitPayUserApplicationService.Value.SignOut(this.User.Identity.Name, false);
            }

            this.SessionFacade.Value.ClearSession();

            return this.RedirectToAction("Index", "Landing");
        }
        
        [HttpGet]
        public async Task<ActionResult> Index()
        {
            int visitPayUserId = this.CurrentUserId;
            GuarantorDto guarantorDto = this._visitPayUserApplicationService.Value.GetGuarantorFromVisitPayUserId(visitPayUserId);

            OfflineIndexViewModel model = new OfflineIndexViewModel 
            {
                VisitPayUserId = this.CurrentUserId,
                Statements = new StatementListViewModel()
            };

            // todo: refactor StatementAppSvc to return basically this viewmodel
            DateTime? nextStatementDate = guarantorDto.NextStatementDate ?? this._statementApplicationService.Value.GetNextStatementDate(guarantorDto.VpGuarantorId, DateTime.UtcNow);
            if (nextStatementDate.HasValue)
            {
                model.Statements.NextStatementDate = nextStatementDate.Value.ToString(Format.DateFormat);
            }
            
            IList<FinancePlanStatementDto> fpStatementsDto = this._statementApplicationService.Value.GetFinancePlanStatements(visitPayUserId)
                .Where(x => x.StatementVersion != VpStatementVersionEnum.Vp2)
                .OrderByDescending(x => x.PeriodEndDate)
                .ToList();

            model.Statements.FinancePlanStatements = Mapper.Map<List<StatementListItemViewModel>>(fpStatementsDto);
            // /todo

            // get finance plan payment option
            UserPaymentOptionDto paymentOption = await this._paymentOptionApplicationService.Value.GetPaymentOptionAsync(guarantorDto.VpGuarantorId, visitPayUserId, PaymentOptionEnum.SpecificFinancePlans);
            if (paymentOption != null)
            {
                model.PaymentSubmitUrl = this.Url.Action("ArrangePaymentSubmit", "Payment");
                model.PaymentValidateUrl = this.Url.Action("ArrangePaymentValidate", "Payment");
                model.FinancePlanPaymentOption = Mapper.Map<PaymentOptionViewModel>(paymentOption);
            }

            return this.View(model);
        }

        [HttpGet]
        public ActionResult AccountSettings()
        {
            int visitPayUserId = this.CurrentUserId;
            GuarantorDto guarantorDto = this._visitPayUserApplicationService.Value.GetGuarantorFromVisitPayUserId(visitPayUserId);

            OfflineAccountSettingsViewModel model = new OfflineAccountSettingsViewModel 
            {
                GuarantorUseAutoPay = guarantorDto.UseAutoPay,
                PaymentMethods = new PaymentMethodsViewModel {AllowSelection = false},
                UrlPaymentMethodBank = this.Url.Action("PaymentMethodBank", "Payment"),
                UrlPaymentMethodCard = this.Url.Action("PaymentMethodCard", "Payment"),
                UrlPaymentMethodsList = this.Url.Action("GetPaymentMethodsList", "Payment"),
                UrlRemovePaymentMethod = this.Url.Action("RemovePaymentMethod", "Payment"),
                UrlSavePaymentMethodBank = this.Url.Action("SavePaymentMethodBank", "Payment"),
                UrlSavePaymentMethodCard = this.Url.Action("SavePaymentMethodCard", "Payment"),
                UrlSetPrimary = this.Url.Action("SetPrimaryPaymentMethod", "Payment")
            };

            return this.View(model);
        }
        
        [HttpGet]
        public async Task<ActionResult> FinancePlan()
        {
            int vpGuarantorId =  this.CurrentGuarantorId.GetValueOrDefault(0);
            int visitPayUserId = this.CurrentUserId;
            
            UserPaymentOptionDto option = await this._paymentOptionApplicationService.Value.GetFinancePlanPaymentOptionAsync(vpGuarantorId, visitPayUserId);
            FinancePlanConfigurationDto config = option.FinancePlanDefaultConfiguration ?? option.FinancePlanCombinedConfiguration;

            if (config?.FinancePlanId == null)
            {
                return this.RedirectToAction("Index");
            }

            OfflineFinancePlanConfigurationViewModel model = Mapper.Map<OfflineFinancePlanConfigurationViewModel>(config);

            CmsVersionDto esignTermsCmsVersionDto = await this._contentApplicationService.Value.GetCurrentVersionAsync(CmsRegionEnum.EsignTerms, false);
            model.EsignCmsVersionId = esignTermsCmsVersionDto.CmsVersionId;

            GuarantorDto guarantorDto = this._guarantorApplicationService.Value.GetGuarantor(vpGuarantorId);
            model.GuarantorUseAutoPay = guarantorDto.UseAutoPay;

            return this.View(model);
        }
        
        [HttpGet]
        public PartialViewResult FinancePlanDeclined()
        {
            int vpGuarantorId = this.CurrentGuarantorId.GetValueOrDefault(0);
            int visitPayUserId = this.CurrentUserId;

            //User declined FP terms so cancel their account, if applicable
            this._guarantorApplicationService.Value.VpccUserDeclinedFinancePlan(visitPayUserId, vpGuarantorId);
            
            return this.PartialView("_FinancePlanDeclined");
        }

        [HttpGet]
        public async Task<ActionResult> ReconfigureFinancePlan()
        {
            int vpGuarantorId =  this.CurrentGuarantorId.GetValueOrDefault(0);

            ReconfigureFinancePlanViewModel model;

            ReconfigureFinancePlanDto reconfigureInformationDto = await this._reconfigureFinancePlanApplicationService.Value.GetOfflineReconfigurationInformation(vpGuarantorId);
            if (reconfigureInformationDto == null)
            {
                this.Logger.Value.Fatal(() => $"{nameof(this.ReconfigureFinancePlan)}: {nameof(reconfigureInformationDto)} is null");
                string errorMessage = await this._contentApplicationService.Value.GetTextRegionAsync(TextRegionConstants.ReconfigurationNotEligible);
                model = new ReconfigureFinancePlanViewModel { ErrorMessage = errorMessage };
            } 
            else if (reconfigureInformationDto.FinancePlanBoundary.MinimumPaymentAmount <= 0m)
            {
                this.Logger.Value.Fatal(() => $"{nameof(this.ReconfigureFinancePlan)}: minimum payment amount <= 0");
                string errorMessage = await this._contentApplicationService.Value.GetTextRegionAsync(TextRegionConstants.ReconfigurationNotEligible);
                model = new ReconfigureFinancePlanViewModel { ErrorMessage = errorMessage };
            }
            else
            {
                model = Mapper.Map<ReconfigureFinancePlanViewModel>(reconfigureInformationDto);
                model.ReconfigureAcceptUrl = this.Url.Action("ReconfigureAccept", "FinancePlan");
                model.ReconfigureCancelUrl = this.Url.Action("ReconfigureCancel", "FinancePlan");
                model.ReconfigureSubmitUrl = this.Url.Action("ReconfigureSubmit", "FinancePlan");
                model.ReconfigureValidateUrl = this.Url.Action("ReconfigureValidate", "FinancePlan");
                model.IsEditable = model.Terms == null;

                if (model.Terms != null && model.TermsCmsVersionId != default(int))
                {
                    model.TermsCmsVersionId = model.Terms.TermsCmsVersionId;
                }
                else
                {
                    int? ricCmsRegionId = this._visitApplicationService.Value.GetRicCmsRegionId(vpGuarantorId);
                    CmsRegionEnum cmsRegionEnum = !ricCmsRegionId.HasValue || ricCmsRegionId == default(int) ? CmsRegionEnum.VppCreditAgreement : (CmsRegionEnum) ricCmsRegionId;

                    model.TermsCmsVersionId = (await this._contentApplicationService.Value.GetCurrentVersionAsync(cmsRegionEnum, false)).CmsVersionId;
                }

                int currentVpGuarantorId = this._visitPayUserApplicationService.Value.GetGuarantorIdFromVisitPayUserId(this.CurrentUserId);
                model.PrimaryPaymentMethod = Mapper.Map<PaymentMethodListItemViewModel>(this._paymentMethodsApplicationService.Value.GetPrimaryPaymentMethod(currentVpGuarantorId));
            }

            return this.View(model);
        }

        /// <summary>
        /// Gets the partial view for creating an account, with the current Terms/Esign version numbers in the model.
        /// <para />
        /// NOTE: Do not put a "[HttpGet]" attribute on this method or it will throw an exception when rendering the partial from the FP controller.
        /// </summary>
        /// <returns></returns>
        public PartialViewResult CreateAccountPartial()
        {
            CmsVersionDto termsAndConditionsDto = this._contentApplicationService.Value.GetCurrentVersionAsync(CmsRegionEnum.VppTermsAndConditions, false).Result;

            CreateAccountViewModel model = new CreateAccountViewModel
            {
                TermsOfUseCmsVersionId = termsAndConditionsDto.CmsVersionId
            };

            return this.PartialView("_CreateAccount", model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<JsonResult> CreateAccount(CreateAccountViewModel model)
        {
            // validate model
            if (!this.TryValidateModel(model))
            {
                return this.Json(new ResultMessage<Hashtable>(false, this.ModelState.GetErrors()));
            }

            // todo: all this should be moved into an app service method
            // save terms
            bool termsSaved = await this.SaveTerms(model);
            if (!termsSaved)
            {
                return this.Json(new ResultMessage<Hashtable>(false, this.ModelState.GetErrors()));
            }

            // set password
            bool setPasswordSuccessful = this.SetPassword(model.Password.TrimNullSafe()).Result;
            if (!setPasswordSuccessful)
            {
                return this.Json(new ResultMessage<Hashtable>(false, this.ModelState.GetErrors()));
            }

            // update email
            bool updateEmailSuccessful = this.UpdateEmailAndUserName(model.EmailAddress);
            if (!updateEmailSuccessful)
            {
                return this.Json(new ResultMessage<Hashtable>(false, this.ModelState.GetErrors()));
            }

            // convert to online
            this._guarantorApplicationService.Value.ConvertVpccUserToVisitPayUser(this.CurrentUserId);

            // sign in
            await this._ssoApplicationService.Value.ReLoginAsync(Mapper.Map<HttpContextDto>(System.Web.HttpContext.Current), this.CurrentUserId.ToString()).ConfigureAwait(false);

            // log event
            int vpGuarantorId = this._guarantorApplicationService.Value.GetVpGuarantorId(this.CurrentUserId);
            this._visitPayUserJournalEventApplicationService.Value.LogGuarantorSelfVerification(this.CurrentUserId, vpGuarantorId, Mapper.Map<JournalEventHttpContextDto>(System.Web.HttpContext.Current));

            // return homepage url
            return this.Json(new ResultMessage<string>(true, this.Url.Action("Index", "Home")));
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult EsignActAccept(int esignCmsVersionId)
        {
            this._visitPayUserApplicationService.Value.TermsAcknowledgement(this.CurrentUserIdString, VisitPayUserAcknowledgementTypeEnum.EsignAct, esignCmsVersionId);

            return this.Json(true);
        }

        [NonAction]
        private async Task<bool> SaveTerms(CreateAccountViewModel model)
        {
            CmsVersionDto termsAndConditionsDto = await this._contentApplicationService.Value.GetCurrentVersionAsync(CmsRegionEnum.VppTermsAndConditions, false);
            if (termsAndConditionsDto.CmsVersionId != model.TermsOfUseCmsVersionId || !model.TermsOfUseAgreed)
            {
                //Version mismatch or user didn't agree
                this.ModelState.AddModelError(nameof(CreateAccountViewModel.TermsOfUseAgreed), LocalizationHelper.GetLocalizedString(TextRegionConstants.TermsOfUseMustAccept));
                return false;
            }
            
            this._visitPayUserApplicationService.Value.TermsAcknowledgement(this.CurrentUserIdString, VisitPayUserAcknowledgementTypeEnum.TermsOfUse, model.TermsOfUseCmsVersionId);

            return true;
        }

        /// <summary>
        /// sets the email and username of the current user. uses email address for both if a username is not defined
        /// </summary>
        /// <param name="emailAddress"></param>
        /// <param name="username"></param>
        /// <returns></returns>
        [NonAction]
        private bool UpdateEmailAndUserName(string emailAddress, string username = null)
        {
            // update email
            VisitPayUserDto visitPayUserDto = this._offlineUserApplicationService.Value.FindById(this.CurrentUserId);
            visitPayUserDto.Email = emailAddress;
            visitPayUserDto.UserName = username.IsNotNullOrEmpty() ? username : emailAddress;
            IdentityResult updateResult = this._visitPayUserApplicationService.Value.UpdateUser(visitPayUserDto);

            // update email errors
            if (!updateResult.Succeeded)
            {
                foreach (string error in updateResult.Errors)
                {
                    this.ModelState.AddModelError(nameof(CreateAccountViewModel.EmailAddress), error);
                }
                this.Logger.Value.Info(() => $"{nameof(OfflineController)}::{nameof(this.CreateAccount)} - result failed {string.Join(",", updateResult.Errors)}");
                return false;
            }

            return true;
        }

        /// <summary>
        /// sets the password for the current user
        /// </summary>
        /// <param name="password"></param>
        /// <returns></returns>
        [NonAction]
        private async Task<bool> SetPassword(string password)
        {
            // validate password
            bool isPasswordValid = this._visitPayUserApplicationService.Value.IsPasswordStrongEnough(password).Result.Succeeded;
            if (!isPasswordValid)
            {
                string guarantorPasswordRequirements = await this._visitPayUserApplicationService.Value.GetGuarantorPasswordRequirements().ConfigureAwait(false);

                this.ModelState.AddModelError(nameof(CreateAccountViewModel.Password), guarantorPasswordRequirements);
                return false;
            }

            // reset the password
            IdentityResult result = await this._visitPayUserApplicationService.Value.ResetPasswordAsync(
                this.CurrentUserId.ToString(),
                password,
                this.ClientDto.GuarantorPasswordExpLimitInDays,
                this.ClientDto.GuarantorUnrepeatablePasswordCount).ConfigureAwait(false);

            // error
            if (!result.Succeeded)
            {
                foreach (string error in result.Errors)
                {
                    this.ModelState.AddModelError(string.Empty, error);
                }
                this.Logger.Value.Info(() => $"{nameof(OfflineController)}::{nameof(this.CreateAccount)} - result failed {string.Join(",", result.Errors)}");
                return false;
            }

            return true;
        }

        [NonAction]
        private bool IsFailureSignInResult(SignInStatusExEnum status)
        {
            return status.Equals(SignInStatusExEnum.Failure) ||
                   status.Equals(SignInStatusExEnum.AccountClosed) ||
                   status.Equals(SignInStatusExEnum.LockedOut);
        }
    }
}
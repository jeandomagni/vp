﻿namespace Ivh.Web.Patient.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using System.Web.Mvc;
    using System.Web.SessionState;
    using Application.Content.Common.Dtos;
    using Application.Content.Common.Interfaces;
    using Application.Core.Common.Dtos;
    using Application.Core.Common.Interfaces;
    using Application.FinanceManagement.Common.Dtos;
    using Application.FinanceManagement.Common.Interfaces;
    using Attributes;
    using AutoMapper;
    using Common.Base.Utilities.Extensions;
    using Common.EventJournal;
    using Common.VisitPay.Constants;
    using Common.VisitPay.Enums;
    using Common.Web.Controllers;
    using Common.Web.Filters;
    using Common.Web.Interfaces;
    using Common.Web.Models.Account;
    using Common.Web.Models.Payment;
    using Common.Web.Models.Shared;
    using Domain.Logging.Interfaces;
    using Domain.Settings.Interfaces;
    using Microsoft.AspNet.Identity;
    using Models.Payment;
    using Services;
    using SessionFacade;
    
    [GuarantorAuthorize]
    public class PaymentController : BasePaymentController
    {
        private readonly Lazy<GuarantorFilterService> _guarantorFilterService;
        private readonly Lazy<IWebPatientSessionFacade> _webPatientSessionFacade;

        public PaymentController(
            Lazy<IBaseControllerService> baseControllerService,
            Lazy<IContentApplicationService> contentApplicationService,
            Lazy<IFinancePlanApplicationService> financePlanApplicationService,
            Lazy<IGuarantorApplicationService> guarantorApplicationService,
            Lazy<IPaymentActionApplicationService> paymentActionApplicationService,
            Lazy<IPaymentConfigurationService> paymentConfigurationService,
            Lazy<IPaymentDetailApplicationService> paymentDetailApplicationService,
            Lazy<IPaymentMethodsApplicationService> paymentMethodsApplicationService,
            Lazy<IPaymentMethodAccountTypeApplicationService> paymentMethodAccountTypeApplicationService,
            Lazy<IPaymentMethodProviderTypeApplicationService> paymentMethodProviderTypeApplicationService,
            Lazy<IPaymentOptionApplicationService> paymentOptionApplicationService,
            Lazy<IPaymentReversalApplicationService> paymentReversalApplicationService,
            Lazy<IPaymentSubmissionApplicationService> paymentSubmissionApplicationService,
            Lazy<IVisitApplicationService> visitApplicationService,
            Lazy<IVisitPayUserApplicationService> visitPayUserApplicationService,
            Lazy<GuarantorFilterService> guarantorFilterService,
            Lazy<IWebPatientSessionFacade> webPatientSessionFacade)
            : base(
                baseControllerService,
                contentApplicationService,
                financePlanApplicationService,
                guarantorApplicationService,
                paymentActionApplicationService,
                paymentConfigurationService,
                paymentDetailApplicationService,
                paymentMethodsApplicationService,
                paymentMethodAccountTypeApplicationService,
                paymentMethodProviderTypeApplicationService,
                paymentOptionApplicationService,
                paymentReversalApplicationService,
                paymentSubmissionApplicationService,
                visitApplicationService,
                visitPayUserApplicationService)
        {
            this._guarantorFilterService = guarantorFilterService;
            this._webPatientSessionFacade = webPatientSessionFacade;
        }

        #region my payments (pending / history)

        [HttpGet]
        public ActionResult MyPayments()
        {
            return this.View();
        }

        [HttpPost]
        [OverrideAuthorization, EmulateUser(AllowPost = true)]
        [ValidateAntiForgeryToken]
        public JsonResult PaymentPending()
        {
            IList<PaymentPendingSearchResultViewModel> scheduledPayments = this.GetScheduledPayments(this._guarantorFilterService.Value.VpGuarantorId, this.CurrentUserId);

            return this.Json(new PaymentPendingResultsModel(1, scheduledPayments.Count, scheduledPayments.Count)
            {
                Results = scheduledPayments
            });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<JsonResult> PaymentPendingExport()
        {
            return await this.GetPendingPaymentsExportAsync(this._guarantorFilterService.Value.VpGuarantorId);
        }

        [HttpPost]
        [OverrideAuthorization, EmulateUser(AllowPost = true)]
        [ValidateAntiForgeryToken]
        public JsonResult PaymentHistory(PaymentHistorySearchFilterViewModel model, int page, int rows, string sidx, string sord)
        {
            PaymentHistorySearchResultsViewModel paymentHistory = this.GetPaymentHistory(this._guarantorFilterService.Value.VpGuarantorId, model, page, rows, sidx, sord, this.CurrentUserId);

            return this.Json(new PaymentHistoryResultsModel(page, rows, paymentHistory.records)
            {
                Results = paymentHistory.Payments
            });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<JsonResult> PaymentHistoryExport(PaymentHistorySearchFilterViewModel model, string sidx, string sord)
        {
            return await this.GetPaymentHistoryExportAsync(this._guarantorFilterService.Value.VpGuarantorId, model, sidx, sord, this.CurrentUserId);
        }

        #endregion

        #region payment edit/reschedule/cancel

        [HttpGet]
        public PartialViewResult PaymentEdit()
        {
            return this.PartialView("_PaymentEdit");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<JsonResult> PaymentEdit(bool isRecurring, int? paymentId)
        {
            return this.Json(await this.GetPaymentEditModalAsync(isRecurring, paymentId, this._guarantorFilterService.Value.VpGuarantorId).ConfigureAwait(true));
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult PaymentEditValidate(PaymentRescheduleDto paymentRescheduleDto)
        {
            return this.ValidatePaymentEdit(paymentRescheduleDto, this._guarantorFilterService.Value.VpGuarantorId);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult PaymentEditSave(PaymentRescheduleDto paymentRescheduleDto)
        {
            return this.Json(this.PaymentActionApplicationService.Value.ReschedulePayment(this._guarantorFilterService.Value.VpGuarantorId, this.CurrentUserId, paymentRescheduleDto));
        }

        [HttpGet]
        public PartialViewResult PaymentCancel()
        {
            return this.PartialView("_PaymentCancel");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<JsonResult> PaymentCancel(bool isRecurringPayment, int? paymentId = null)
        {
            PaymentCancelViewModel model = await this.GetPaymentCancelModelAsync(isRecurringPayment, paymentId, this._guarantorFilterService.Value.VpGuarantorId).ConfigureAwait(true);

            return this.Json(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult PaymentCancelSubmit(PaymentCancelDto paymentCancelDto)
        {
            this.PaymentActionApplicationService.Value.CancelPayment(this._guarantorFilterService.Value.VpGuarantorId, this.CurrentUserId, paymentCancelDto);

            return this.Json(true);
        }
        
        #endregion

        #region payment details

        [HttpPost]
        [OverrideAuthorization, EmulateUser(AllowPost = true)]
        [ValidateAntiForgeryToken]
        public PartialViewResult PaymentDetail(int paymentProcessorResponseId)
        {
            return this.GetPaymentDetail(paymentProcessorResponseId, this._guarantorFilterService.Value.VpGuarantorId, this.CurrentUserId);
        }

        [HttpPost]
        [OverrideAuthorization, EmulateUser(AllowPost = false)]
        [ValidateAntiForgeryToken]
        public async Task<JsonResult> PaymentDetailExport(int paymentProcessorResponseId)
        {
            return await this.GetPaymentDetailExportAsync(paymentProcessorResponseId, this._guarantorFilterService.Value.VpGuarantorId, this.CurrentUserId);
        }
        
        #endregion

        #region payment methods

        [HttpGet]
        public ActionResult PaymentMethods()
        {
            return this.View(new PaymentMethodsViewModel
            {
                AllowSelection = false
            });
        }

        [AllowAnonymous]
        public PartialViewResult PaymentMethodsListModal(bool allowSelection)
        {
            return this.PartialView("_PaymentMethodsListModal", new PaymentMethodsViewModel
            {
                AllowSelection = allowSelection
            });
        }

        [AllowAnonymous] // for registration
        public PartialViewResult PaymentMethodsListPartial(bool allowSelection)
        {
            return this.PartialView("_PaymentMethodsList", new PaymentMethodsViewModel
            {
                AllowSelection = allowSelection
            });
        }

        [HttpGet]
        [AllowOffline]
        public PartialViewResult PaymentMethodBank()
        {
            return this.PartialView("_PaymentMethodBankAccount", new BankAccountViewModel());
        }

        [HttpGet]
        [AllowOffline]
        public PartialViewResult PaymentMethodAccountProvider(int paymentMethodId)
        {
            PaymentMethodDto paymentMethod = this.PaymentMethodsApplicationService.Value.GetPaymentMethodById(paymentMethodId);
            IList<PaymentMethodAccountTypeDto> paymentMethodAccountTypes = this.PaymentMethodAccountTypeApplicationService.Value.GetPaymentMethodAccountTypes();
            IList<PaymentMethodProviderTypeDto> paymentMethodProviderTypes = this.PaymentMethodProviderTypeApplicationService.Value.GetPaymentMethodProviderTypes();
            return this.PartialView("_PaymentMethodAccountProvider", new PaymentMethodAccountProviderViewModel
            {
                PaymentMethodAccountTypes = Mapper.Map<IList<PaymentMethodAccountTypeViewModel>>(paymentMethodAccountTypes),
                PaymentMethodProviderTypes = Mapper.Map<IList<PaymentMethodProviderTypeViewModel>>(paymentMethodProviderTypes),
                SelectedAccountTypeId = paymentMethod.PaymentMethodAccountType.PaymentMethodAccountTypeId,
                SelectedProviderTypeId = paymentMethod.PaymentMethodProviderType.PaymentMethodProviderTypeId
            });
        }

        [HttpGet]
        [AllowOffline]
        public PartialViewResult PaymentMethodCard()
        {
            IList<PaymentMethodAccountTypeDto> paymentMethodAccountTypes = this.PaymentMethodAccountTypeApplicationService.Value.GetPaymentMethodAccountTypes();
            IList<PaymentMethodProviderTypeDto> paymentMethodProviderTypes = this.PaymentMethodProviderTypeApplicationService.Value.GetPaymentMethodProviderTypes();
            return this.PartialView("_PaymentMethodCardAccount", new CardAccountViewModel
            {
                PaymentMethodAccountTypes = Mapper.Map<IList<PaymentMethodAccountTypeViewModel>>(paymentMethodAccountTypes),
                PaymentMethodProviderTypes = Mapper.Map<IList<PaymentMethodProviderTypeViewModel>>(paymentMethodProviderTypes)
            });
        }

        [HttpPost]
        [AllowOffline]
        [OverrideAuthorization, EmulateUser(AllowPost = true)]
        [ValidateAntiForgeryToken]
        public async Task<JsonResult> GetPaymentMethodsList()
        {
            int vpGuarantorId = this.GetVpGuarantorId(this.CurrentUserId);

            IList<PaymentMethodDto> paymentMethodsDto = this.PaymentMethodsApplicationService.Value.GetPaymentMethods(vpGuarantorId, this.CurrentUserId);

            CmsVersionDto hsaDisclaimer = await this.ContentApplicationService.Value.GetCurrentVersionAsync(CmsRegionEnum.HsaInterestDisclaimer);
            string removePrompt = await this.ContentApplicationService.Value.GetTextRegionAsync(TextRegionConstants.RemovePaymentMethodPrompt);
            string removeTitle = await this.ContentApplicationService.Value.GetTextRegionAsync(TextRegionConstants.RemovePaymentMethod);
            string primaryChangedText = await this.ContentApplicationService.Value.GetTextRegionAsync(TextRegionConstants.SelectedNewPrimary);
            string primaryChangedTitle = await this.ContentApplicationService.Value.GetTextRegionAsync(TextRegionConstants.PrimaryPaymentMethodChanged);
            
            PaymentMethodsViewModel model = new PaymentMethodsViewModel
            {
                AllAccounts = Mapper.Map<IList<PaymentMethodListItemViewModel>>(paymentMethodsDto),
                AccountRemoveCms = new CmsViewModel
                {
                    ContentBody = removePrompt,
                    ContentTitle = removeTitle
                },
                HsaInterestDisclaimerCms = Mapper.Map<CmsViewModel>(hsaDisclaimer),
                PrimaryChangedCms = new CmsViewModel
                {
                    ContentBody = primaryChangedText,
                    ContentTitle = primaryChangedTitle
                }
            };

            return this.Json(model);
        }

        [HttpPost]
        [AllowOffline]
        [EmulateUser(AllowPost = true)]
        [ValidateAntiForgeryToken]
        public JsonResult PaymentMethod(int paymentMethodId, bool isNewAch = false)
        {
            PaymentMethodDto paymentMethodDto = new PaymentMethodDto
            {
                IsActive = true
            };

            bool hasExistingPrimary = true;

            int vpGuarantorId = this.GetVpGuarantorId(this.CurrentUserId);
            if (paymentMethodId > 0)
            {
                paymentMethodDto = this.PaymentMethodsApplicationService.Value.GetPaymentMethodById(paymentMethodId, vpGuarantorId);
            }
            else
            {
                PaymentMethodDto primaryPaymentMethod = this.PaymentMethodsApplicationService.Value.GetPrimaryPaymentMethod(vpGuarantorId);
                hasExistingPrimary = primaryPaymentMethod != null;
            }

            if (paymentMethodDto.IsAchType || (paymentMethodId == 0 && isNewAch))
            {
                BankAccountViewModel bankAccountViewModel = Mapper.Map<BankAccountViewModel>(paymentMethodDto);
                bankAccountViewModel.HasExistingPrimary = hasExistingPrimary;
                bankAccountViewModel.IsPrimary = paymentMethodDto.IsPrimary || !hasExistingPrimary;

                return this.Json(bankAccountViewModel);
            }

            IList<PaymentMethodAccountTypeDto> paymentMethodAccountTypes = this.PaymentMethodAccountTypeApplicationService.Value.GetPaymentMethodAccountTypes();
            IList<PaymentMethodProviderTypeDto> paymentMethodProviderTypes = this.PaymentMethodProviderTypeApplicationService.Value.GetPaymentMethodProviderTypes();

            CardAccountViewModel cardAccountViewModel = Mapper.Map<CardAccountViewModel>(paymentMethodDto);
            cardAccountViewModel.HasExistingPrimary = hasExistingPrimary;
            cardAccountViewModel.IsPrimary = paymentMethodDto.IsPrimary || !hasExistingPrimary;
            cardAccountViewModel.PaymentMethodAccountTypes = Mapper.Map<IList<PaymentMethodAccountTypeViewModel>>(paymentMethodAccountTypes);
            cardAccountViewModel.PaymentMethodProviderTypes = Mapper.Map<IList<PaymentMethodProviderTypeViewModel>>(paymentMethodProviderTypes);

            if (paymentMethodDto.PaymentMethodAccountType != null)
            {
                cardAccountViewModel.AccountTypeId = paymentMethodDto.PaymentMethodAccountType.PaymentMethodAccountTypeId;
            }
            if (paymentMethodDto.PaymentMethodProviderType != null)
            {
                cardAccountViewModel.ProviderTypeId = paymentMethodDto.PaymentMethodProviderType.PaymentMethodProviderTypeId;
            }

            return this.Json(cardAccountViewModel);
        }

        [HttpPost]
        [AllowOffline]
        [ValidateAntiForgeryToken]
        public JsonResult PaymentMethodPrimary()
        {
            int vpGuarantorId = this.GetVpGuarantorId(this.CurrentUserId);

            return this.Json(Mapper.Map<PaymentMethodListItemViewModel>(this.PaymentMethodsApplicationService.Value.GetPrimaryPaymentMethod(vpGuarantorId) ?? new PaymentMethodDto()));
        }
        
        [HttpPost]
        [AllowOffline]
        [ValidateAntiForgeryToken]
        [EmulateUser(AllowPost = false)]
        public async Task<JsonResult> SavePaymentMethodBank(BankAccountViewModel model)
        {
            if (!this.ValidateModelState("BankAccountViewModel Failed Validation"))
            {
                return this.Json(new PaymentMethodResultViewModel(false, this.GenericErrorMessage));
            }

            int vpGuarantorId = this.GetVpGuarantorId(this.CurrentUserId);

            PaymentMethodResultDto resultDto = await this.SavePaymentMethodBank(model, vpGuarantorId, this.CurrentUserId).ConfigureAwait(true);

            return this.Json(Mapper.Map<PaymentMethodResultViewModel>(resultDto));
        }

        [HttpPost]
        [AllowOffline]
        [ValidateAntiForgeryToken]
        [EmulateUser(AllowPost = false)]
        public JsonResult SavePaymentMethodCard(CardAccountViewModel model)
        {
            if (!this.ValidateModelState("CardAccountViewModel Failed Validation"))
            {
                return this.Json(new PaymentMethodResultViewModel(false, this.GenericErrorMessage));
            }

            int vpGuarantorId = this.GetVpGuarantorId(this.CurrentUserId);
            PaymentMethodResultDto resultDto = this.SavePaymentMethodCard(model, vpGuarantorId, this.CurrentUserId);
            this.MetricsProvider.Value.Increment(Metrics.Increment.SecurePan.PaymentMethod.Success);

            return this.Json(Mapper.Map<PaymentMethodResultViewModel>(resultDto));
        }
        
        [HttpPost]
        [AllowOffline]
        [ValidateAntiForgeryToken]
        [EmulateUser(AllowPost = false)]
        public async Task<JsonResult> RemovePaymentMethod(int paymentMethodId)
        {
            int vpGuarantorId = this.GetVpGuarantorId(this.CurrentUserId);
            DeletePaymentMethodResponseDto response = await this.PaymentMethodsApplicationService.Value.DeactivatePaymentMethodAsync(paymentMethodId, vpGuarantorId, this.CurrentUserId);

            return this.Json(new PaymentMethodResultViewModel(!response.IsError, response.ErrorMessage));
        }

        [HttpPost]
        [AllowOffline]
        [ValidateAntiForgeryToken]
        [EmulateUser(AllowPost = false)]
        public JsonResult SetPrimaryPaymentMethod(int paymentMethodId)
        {
            int vpGuarantorId = this.GetVpGuarantorId(this.CurrentUserId);

            PaymentMethodResultDto resultDto = this.PaymentMethodsApplicationService.Value.SetPrimaryPaymentMethod(Mapper.Map<JournalEventHttpContextDto>(System.Web.HttpContext.Current), paymentMethodId, vpGuarantorId, this.CurrentUserId);

            return this.Json(Mapper.Map<PaymentMethodResultViewModel>(resultDto));
        }

        [HttpPost]
        [AllowOffline]
        [ValidateAntiForgeryToken]
        [EmulateUser(AllowPost = false)]
        public JsonResult SetPaymentMethodAccountProviderType(int paymentMethodId, int paymentMethodAccountTypeId, int paymentMethodProviderTypeId)
        {
            int vpGuarantorId = this.VisitPayUserApplicationService.Value.GetGuarantorIdFromVisitPayUserId(this.CurrentUserId);

            PaymentMethodResultDto resultDto = this.PaymentMethodsApplicationService.Value.SetPaymentMethodAccountProviderType(paymentMethodId, paymentMethodAccountTypeId, paymentMethodProviderTypeId, vpGuarantorId, this.CurrentUserId);

            return this.Json(Mapper.Map<PaymentMethodResultViewModel>(resultDto));
        }

        #endregion

        #region ach authorization

        [HttpPost]
        [AllowOffline]
        [EmulateUser(AllowPost = true)]
        [ValidateAntiForgeryToken]
        public async Task<PartialViewResult> AchAuthorization(AchAuthorizationParametersViewModel model)
        {
            int vpGuarantorId = this.CurrentGuarantor().VpGuarantorId;

            // payment method
            PaymentMethodDto paymentMethodDto = this.PaymentMethodsApplicationService.Value.GetPaymentMethodById(model.PaymentMethodId, vpGuarantorId);
            if (paymentMethodDto == null || !paymentMethodDto.IsAchType)
            {
                // ui shouldn't be requesting this for non-ach, but check anyway
                return null;
            }

            int? consolidationGuarantorId = model.ConsolidationGuarantorId.HasValue ? this.SessionFacade.Value.ConsolidationGuarantorObfuscator.Clarify(model.ConsolidationGuarantorId.Value) : (int?) null;

            IList<FinancePlanAchAuthorizationDto> financePlanAchAuthorizations = await this.FinancePlanApplicationService.Value.GetFinancePlansRequiringAchAuthorizationAsync(
                this.CurrentUserId,
                this._guarantorFilterService.Value.VpGuarantorId,
                model.FinancePlanOfferId,
                model.FinancePlanIds,
                model.FinancePlanIdToReconfigure,
                consolidationGuarantorId);

            if (financePlanAchAuthorizations.Count == 0)
            {
                // no finance plans - no need to authorize
                return null;
            }

            IList<AchAuthorizationFinancePlan> financePlans = financePlanAchAuthorizations.Select(x => new AchAuthorizationFinancePlan
            {
                DisplayFirstNameLastName = x.Guarantor.User.DisplayFirstNameLastName,
                IsManagedUser = x.Guarantor.ConsolidationStatus == GuarantorConsolidationStatusEnum.Managed && x.Guarantor.VpGuarantorId != vpGuarantorId,
                MonthlyPaymentAmount = x.MonthlyPaymentAmount,
                MonthsRemaining = x.MonthsRemaining,
                OriginationDate = x.OriginationDate
            }).ToList();


            string achLastFour = $"X-{paymentMethodDto.LastFour}";
            IDictionary<string, string> replacementValues = new Dictionary<string, string>
            {
                {"[[TotalMonthlyPaymentAmount]]", financePlans.Sum(x => x.MonthlyPaymentAmount).FormatCurrency()},
                {"[[AchLastFour]]", achLastFour}
            };

            CmsVersionDto cmsVersionDto = await this.ContentApplicationService.Value.GetCurrentVersionAsync(CmsRegionEnum.AchAuthorization, true, replacementValues);
            AchAuthorizationViewModel vm = new AchAuthorizationViewModel
            {
                AchAuthorizationCmsVersionId = cmsVersionDto.CmsVersionId,
                Agreement = Mapper.Map<CmsViewModel>(cmsVersionDto),
                FinancePlans = financePlans.OrderBy(x => x.IsManagedUser ? 1 : 0).ThenBy(x => x.DisplayFirstNameLastName).ThenBy(x => x.OriginationDate).ToList(),
                AchLastFour = achLastFour
            };

            return this.PartialView("_AchAuthorization", vm);
        }

        [HttpPost]
        [AllowOffline]
        [EmulateUser(AllowPost = false)] // client should not be able to agree to this for a user
        [ValidateAntiForgeryToken]
        public async Task<JsonResult> AchAuthorizationAgree(AchAuthorizationAgreeViewModel model)
        {
            int? consolidationGuarantorId = model.ConsolidationGuarantorId.HasValue ? this.SessionFacade.Value.ConsolidationGuarantorObfuscator.Clarify(model.ConsolidationGuarantorId.Value) : (int?)null;

            IList<FinancePlanAchAuthorizationDto> financePlanAchAuthorizations = await this.FinancePlanApplicationService.Value.GetFinancePlansRequiringAchAuthorizationAsync(
                this.CurrentUserId,
                this._guarantorFilterService.Value.VpGuarantorId,
                model.FinancePlanOfferId,
                model.FinancePlanIds,
                model.FinancePlanIdToReconfigure,
                consolidationGuarantorId);

            this.PaymentMethodsApplicationService.Value.AddAchAuthorization(this.CurrentUserId, model.PaymentMethodId, model.AchAuthorizationCmsVersionId, financePlanAchAuthorizations);

            return this.Json(true);
        }

        #endregion

        #region arrange payment

        [HttpGet]
        public ActionResult MakePayment()
        {
            return this.RedirectToAction("ArrangePayment");
        }

        [HttpGet]
        [Common.Web.Attributes.SessionState(SessionStateBehavior.Required)]
        public async Task<ActionResult> ArrangePayment(PaymentOptionEnum? paymentOption, Guid? v)
        {
            if (v.HasValue) // filteredVpGuarantorId
            {
                this._guarantorFilterService.Value.SetGuarantor(v.Value);
            }

            if (paymentOption.IsInCategory(PaymentOptionEnumCategory.Household))
            {
                int managingVpGuarantorId = this.GetVpGuarantorId(this.CurrentUserId);
                this._guarantorFilterService.Value.SetGuarantor(managingVpGuarantorId);
            }

            int vpGuarantorId = this._guarantorFilterService.Value.VpGuarantorId;

            return this.View(await this.GetArrangePaymentModelAsync(paymentOption, vpGuarantorId, this.CurrentUserId));
        }

        [HttpPost]
        [OverrideAuthorization]
        [ValidateAntiForgeryToken]
        [Common.Web.Attributes.SessionState(SessionStateBehavior.Required)]
        public async Task<JsonResult> ArrangePaymentData()
        {
            return this.Json(await this.GetArrangePaymentModelAsync(null, this._guarantorFilterService.Value.VpGuarantorId, this.CurrentUserId));
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [AllowOffline]
        public JsonResult ArrangePaymentSubmit(ArrangePaymentSubmitViewModel model)
        {
            int actionVisitPayUserId = this.CurrentUserId;
            return this.Json(this.SubmitArrangePayment(model, actionVisitPayUserId, this._guarantorFilterService.Value.VpGuarantorId, "~/Views/Payment/_ArrangePaymentReceipt.cshtml", actionVisitPayUserId));
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [AllowOffline]
        public async Task<JsonResult> ArrangePaymentValidate(ArrangePaymentSubmitViewModel model)
        {
            //Default response
            ArrangePaymentValidationResponseViewModel response = new ArrangePaymentValidationResponseViewModel();

            //Grab guarantorID for use in subsequent method calls
            int vpGuarantorId = this._guarantorFilterService.Value.VpGuarantorId;

            //Validate payment and update response
            ValidatePaymentResponse validatePaymentResponse = this.ValidateArrangePayment(model, this.CurrentUserId, vpGuarantorId);
            if (validatePaymentResponse == null)
            {
                //Payment is valid, update response and load FP offer
                response.PaymentIsValid = true;

                //Check if FP is available for the payment being made, only if making a partial payment
                if (model.PaymentOptionId == PaymentOptionEnum.SpecificVisits)
                {
                    //We are making a partial payment, check for FP offer
                    decimal paymentAmount = model.PaymentAmount.GetValueOrDefault();
                    CalculateFinancePlanTermsResponseDto financePlanOffer = await base.FinancePlanApplicationService.Value.GetAvailableFinancePlanOfferForPayment(this.CurrentUserId, vpGuarantorId, paymentAmount);
                    if (financePlanOffer != null)
                    {
                        //We have a finance plan offer available, so set details on response
                        response.FinancePlanIsAvailable = true;
                        response.MonthlyPaymentAmountTotal = financePlanOffer.Terms.MonthlyPaymentAmountTotal;
                        response.NumberOfMonthlyPayments = financePlanOffer.Terms.NumberMonthlyPayments;

                        //Get offer message from CMS
                        IDictionary<string, string> cmsReplacementValues = new Dictionary<string, string>
                        {
                            { "[[MonthlyPaymentAmountTotal]]", response.MonthlyPaymentAmountTotal.FormatCurrency() },
                            { "[[NumberOfMonthlyPayments]]", response.NumberOfMonthlyPayments.ToString() }
                        };
                        CmsVersionDto cmsVersionDto = await this.ContentApplicationService.Value.GetCurrentVersionAsync(CmsRegionEnum.PartialPaymentFinancePlanOfferMessage, true, cmsReplacementValues);
                        response.FinancePlanOfferMessage = Mapper.Map<CmsViewModel>(cmsVersionDto);
                    }
                }
            }
            else
            {
                //Payment isn't valid, populate error info on response
                response.Prompt = validatePaymentResponse.Prompt;
                response.Message = validatePaymentResponse.Message;
            }
            
            return this.Json(response);
        }

        [NonAction]
        [Common.Web.Attributes.SessionState(SessionStateBehavior.Required)]
        protected override async Task<ArrangePaymentViewModel> GetArrangePaymentModelAsync(PaymentOptionEnum? selectedPaymentOption, int vpGuarantorId, int currentVisitPayUserId)
        {
            ArrangePaymentViewModel model = await base.GetArrangePaymentModelAsync(selectedPaymentOption, vpGuarantorId, currentVisitPayUserId);

            model.DataUrl = this.Url.Action("ArrangePaymentData", "Payment");
            model.FinancePlanSubmitUrl = this.Url.Action("FinancePlanSubmit", "FinancePlan");
            model.FinancePlanValidateUrl = this.Url.Action("FinancePlanValidate", "FinancePlan");
            model.PaymentSubmitUrl = this.Url.Action("ArrangePaymentSubmit", "Payment");
            model.PaymentValidateUrl = this.Url.Action("ArrangePaymentValidate", "Payment");
            model.HomeUrl = "/";
            model.HomeButtonText = "Home";

            this._webPatientSessionFacade.Value.StackedAndCombinedFinancePlanOptionsPresented = model.HasCombinedOption && model.HasDefaultOption;

            return model;
        }
        
        #region demo

        [HttpGet]
        [RequireFeature(VisitPayFeatureEnum.DemoPreServiceUiIsEnabled)]
        [Common.Web.Attributes.SessionState(SessionStateBehavior.Required)]
        public async Task<ActionResult> PsArrangePayment(PaymentOptionEnum? paymentOption, Guid? v)
        {
            if (v.HasValue) // filteredVpGuarantorId
            {
                this._guarantorFilterService.Value.SetGuarantor(v.Value);
            }

            if (paymentOption.IsInCategory(PaymentOptionEnumCategory.Household))
            {
                int managingVpGuarantorId = this.VisitPayUserApplicationService.Value.GetGuarantorIdFromVisitPayUserId(this.User.Identity.GetUserId<int>());
                this._guarantorFilterService.Value.SetGuarantor(managingVpGuarantorId);
            }

            int vpGuarantorId = this._guarantorFilterService.Value.VpGuarantorId;

            return this.View("~/Views/Demo/PSArrangePayment.cshtml", await this.GetArrangePaymentModelAsync(paymentOption, vpGuarantorId, this.User.Identity.GetUserId<int>()));
        }

        [HttpGet]
        [RequireFeature(VisitPayFeatureEnum.DemoInsuranceAccrualUiIsEnabled)]
        [Common.Web.Attributes.SessionState(SessionStateBehavior.Required)]
        public async Task<ActionResult> PayPremiumArrangePayment(PaymentOptionEnum? paymentOption, Guid? v)
        {
            if (v.HasValue) // filteredVpGuarantorId
            {
                this._guarantorFilterService.Value.SetGuarantor(v.Value);
            }

            if (paymentOption.IsInCategory(PaymentOptionEnumCategory.Household))
            {
                int managingVpGuarantorId = this.VisitPayUserApplicationService.Value.GetGuarantorIdFromVisitPayUserId(this.User.Identity.GetUserId<int>());
                this._guarantorFilterService.Value.SetGuarantor(managingVpGuarantorId);
            }

            int vpGuarantorId = this._guarantorFilterService.Value.VpGuarantorId;

            return this.View("~/Views/Demo/PayPremiumArrangePayment.cshtml", await this.GetArrangePaymentModelAsync(paymentOption, vpGuarantorId, this.User.Identity.GetUserId<int>()));
        }

        #endregion
        

        #endregion

        #region discounts (from homepage)

        [HttpGet]
        public PartialViewResult DiscountsPartial()
        {
            return this.PartialView("_Discounts");
        }

        [HttpPost]
        [OverrideAuthorization, EmulateUser(AllowPost = true)]
        public JsonResult GetDiscounts()
        {
            int filteredVpGuarantorId = this._guarantorFilterService.Value.VpGuarantorId;
            
            DiscountOfferDto discountOffer = this.PaymentSubmissionApplicationService.Value.GetDiscountPerVisitForStatementedBalanceInFull(filteredVpGuarantorId);
            if (discountOffer == null || discountOffer.DiscountTotalPercent <= 0)
            {
                return null;
            }

            return this.Json(Mapper.Map<DiscountOfferViewModel>(discountOffer));
        }

        #endregion
        
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public void InvalidCreditCard(string errorMessage, bool isTimeout)
        {
            int? visitPayUserId = null;
            int? vpGuarantorId = null;

            if (this.User.Identity.IsAuthenticated)
            {
                visitPayUserId = this.CurrentUserId;
                vpGuarantorId = this.GetVpGuarantorId(this.CurrentUserId);
            }

            this.LogInvalidCreditCard(visitPayUserId, vpGuarantorId, errorMessage, isTimeout);
        }

        [HttpGet]
        [AllowAnonymous] //this is to allow secure pan usage during registration
        public PartialViewResult SecurePan()
        {
            return this.PartialView("_SecurePan");
        }

        private GuarantorDto CurrentGuarantor()
        {
            return this.CurrentGuarantorId.HasValue ? this.GuarantorApplicationService.Value.GetGuarantor(this.CurrentGuarantorId.Value) : this.GuarantorApplicationService.Value.GetGuarantor(this.CurrentUserName);
        }
    }
}
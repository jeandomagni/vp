﻿namespace Ivh.Web.Patient.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using System.Web;
    using System.Web.Mvc;
    using System.Web.SessionState;
    using Application.AppIntelligence.Common.Interfaces;
    using Application.Content.Common.Dtos;
    using Application.Content.Common.Interfaces;
    using Application.Core.Common.Dtos;
    using Application.Core.Common.Interfaces;
    using Application.Email.Common.Interfaces;
    using Application.FinanceManagement.Common.Dtos;
    using Application.FinanceManagement.Common.Interfaces;
    using Application.User.Common.Dtos;
    using Attributes;
    using AutoMapper;
    using Base;
    using Common.Base.Utilities.Extensions;
    using Common.Cache;
    using Common.EventJournal;
    using Common.VisitPay.Constants;
    using Common.VisitPay.Enums;
    using Common.VisitPay.Strings;
    using Common.Web.Cookies;
    using Common.Web.Constants;
    using Common.Web.Extensions;
    using Common.Web.Filters;
    using Common.Web.Interfaces;
    using Common.Web.Models;
    using Common.Web.Models.Account;
    using Common.Web.Models.Guarantor;
    using Common.Web.Utilities;
    using Domain.Logging.Interfaces;
    using Microsoft.AspNet.Identity;
    using Models;
    using Models.Account;
    using Provider.Pdf;
    using Services;
    using SessionFacade;
    using VisitDetailsViewModel = Models.Account.VisitDetailsViewModel;
    using Ivh.Application.Core.Common.Dtos.Eob;

    [GuarantorAuthorize(Roles = VisitPayRoleStrings.System.Patient + "," + VisitPayRoleStrings.Admin.Administrator)]
    public class AccountController : BaseAccountController
    {
        private readonly Lazy<ISecurityQuestionApplicationService> _securityQuestionApplicationService;
        private readonly Lazy<IServiceGroupApplicationService> _serviceGroupApplicationService;
        private readonly Lazy<IVisitEobApplicationService> _visitEobApplicationService;
        private readonly Lazy<IVisitTransactionApplicationService> _visitTransactionApplicationService;
        private readonly Lazy<IPdfConverter> _pdfConverter;

        public AccountController(
            Lazy<IBaseControllerService> baseControllerService,
            Lazy<IAlertingApplicationService> alertingApplicationService,
            Lazy<IContentApplicationService> contentApplicationService,
            Lazy<IFinancePlanApplicationService> financePlanApplicationService,
            Lazy<GuarantorFilterService> guarantorFilterService,
            Lazy<ISecurityQuestionApplicationService> securityQuestionApplicationService,
            Lazy<IServiceGroupApplicationService> serviceGroupApplicationService,
            Lazy<ISsoApplicationService> ssoApplicationService,
            Lazy<IStatementApplicationService> statementApplicationService,
            Lazy<IVisitApplicationService> visitApplicationService,
            Lazy<IVisitEobApplicationService> visitEobApplicationService,
            Lazy<IVisitPayUserApplicationService> visitPayUserApplicationService,
            Lazy<IVisitTransactionApplicationService> visitTransactionApplicationService,
            Lazy<ICommunicationApplicationService> communicationApplicationService,
            Lazy<IWebCookieFacade> webCookie,
            Lazy<IDistributedCache> cache,
            Lazy<IWebPatientSessionFacade> webPatientSessionFacade,
            Lazy<IPdfConverter> pdfConverter,
            Lazy<ILocalizationCookieFacade> localizationCookieFacade)
            : base(
                baseControllerService,
                alertingApplicationService,
                financePlanApplicationService,
                statementApplicationService,
                visitApplicationService,
                visitPayUserApplicationService,
                guarantorFilterService,
                webCookie,
                communicationApplicationService,
                contentApplicationService,
                cache,
                webPatientSessionFacade,
                ssoApplicationService,
                localizationCookieFacade
            )
        {
            this._securityQuestionApplicationService = securityQuestionApplicationService;
            this._serviceGroupApplicationService = serviceGroupApplicationService;
            this._visitEobApplicationService = visitEobApplicationService;
            this._visitTransactionApplicationService = visitTransactionApplicationService;
            this._pdfConverter = pdfConverter;
        }

        private const string PaymentSummaryExportKey = "PaymentSummaryExport";
        private const string PaymentSummaryExportPdfKey = "PaymentSummaryExportPdf";
        private const string MyVisitsExportKey = "MyVisitsExport";
        private const string VisitTransactionsExportKey = "VisitTransactionsExport";

        [AllowAnonymous]
        public ActionResult LandingPage()
        {
            return this.RedirectToActionPermanent("Index", "Home");
        }

        #region client survey

        [AllowAnonymous]
        public PartialViewResult Survey()
        {
            CmsVersionDto cmsVersion = this.ContentApplicationService.Value.GetCurrentVersionAsync(CmsRegionEnum.ClientSurveyText).Result;

            if (string.IsNullOrEmpty(cmsVersion.ContentBody))
            {
                return null;
            }

            return this.PartialView("~/Views/Account/_ClientSurvey.cshtml", cmsVersion);
        }

        #endregion

        #region External Pages

        [MobileRedirect("/mobile/Account/ContactUs")]
        [AllowAnonymous]
        [SkipRestrictedCountryCheck]
        public override ActionResult ContactUs(bool countryRestricted = false)
        {
            return base.ContactUs(countryRestricted);
        }

        [MobileRedirect("/mobile/Account/LearnMore")]
        [AllowAnonymous]
        [HttpGet]
        public override async Task<ActionResult> LearnMore()
        {
            return await base.LearnMore();
        }

        #endregion

        #region login, password, username

        [MobileRedirect("/mobile/Account/Login")]
        [AllowAnonymous]
        [HttpGet]
        [Common.Web.Attributes.SessionState(SessionStateBehavior.Required)]
        public override async Task<ActionResult> Login(string returnUrl, int? sm)
        {
            return !this.IsBrowserSupported ? this.View("~/Views/Shared/BrowserNotSupported.cshtml") : await base.Login(returnUrl, sm);
        }

        [MobileRedirect("/mobile/Account/ResetPassword", true)]
        [HttpGet]
        [AllowAnonymous]
        public override ActionResult ResetPassword()
        {
            return base.ResetPassword();
        }

        #endregion

        #region emulate user

        [AllowAnonymous]
        [HttpGet]
        public ActionResult LoginWithToken(string token)
        {
            SignInStatusExEnum result = this.VisitPayUserApplicationService.Value.EmulateTokenSignIn(token);

            switch (result)
            {
                case SignInStatusExEnum.Success:
                    return this.RedirectToLocal("");
                default:
                    this.ModelState.AddModelError("", LocalizationHelper.GetLocalizedString(TextRegionConstants.UsernameOrPasswordIncorrect));
                    return this.View("EmulateError");
            }
        }

        [HttpPost]
        [OverrideAuthorization]
        [EmulateUser(AllowPost = true)]
        [ValidateAntiForgeryToken]
        public JsonResult VerifyEmulation(int vpUserId)
        {
            return this.Json(this.HttpContext.Items.Contains(ClaimTypeEnum.ClientUsername) && (this.User.CurrentUserId() != vpUserId) ? this.Url.Action("LogOff", "Account") : "");
        }

        [HttpPost]
        [OverrideAuthorization]
        [EmulateUser(AllowPost = true)]
        [ValidateAntiForgeryToken]
        public JsonResult EndEmulation()
        {
            if (this.User.Identity.IsAuthenticated)
            {
                this.VisitPayUserApplicationService.Value.SignOut(this.User.Identity.Name, false);
                this.AuthenticationManager.SignOut();
            }
            this.SessionFacade.Value.ClearSession();

            return this.Json(new { });
        }

        #endregion

        #region user profile

        [HttpGet]
        public ActionResult UserProfile()
        {
            return this.View();
        }

        [HttpGet]
        [OverrideAuthorization]
        [EmulateUser(AllowPost = true)]
        public ActionResult UserProfilePersonalInformation()
        {
            VisitPayUserDto visitPayUserDto = this.VisitPayUserApplicationService.Value.FindById(this.User.Identity.GetUserId());
            ProfileDisplayViewModel model = new ProfileDisplayViewModel
            {
                AddressStreet1 = visitPayUserDto.AddressStreet1,
                AddressStreet2 = visitPayUserDto.AddressStreet2,
                City = visitPayUserDto.City,
                State = visitPayUserDto.State,
                Zip = visitPayUserDto.Zip,
                MailingAddressStreet1 = visitPayUserDto.MailingAddressStreet1,
                MailingAddressStreet2 = visitPayUserDto.MailingAddressStreet2,
                MailingCity = visitPayUserDto.MailingCity,
                MailingState = visitPayUserDto.MailingState,
                MailingZip = visitPayUserDto.MailingZip,
                Email = visitPayUserDto.Email,
                Name = visitPayUserDto.DisplayFullName,              
                UserName = visitPayUserDto.UserName               
            };

            return this.PartialView("_UserProfilePersonalInformation", model);
        }
        
        [HttpGet]
        public ActionResult UserProfileEdit()
        {
            VisitPayUserDto visitPayUserDto = this.VisitPayUserApplicationService.Value.FindById(this.User.Identity.GetUserId());

            ProfileEditViewModel model = new ProfileEditViewModel
            {
                AddressStreet1 = visitPayUserDto.AddressStreet1,
                AddressStreet2 = visitPayUserDto.AddressStreet2,
                City = visitPayUserDto.City,
                State = visitPayUserDto.State,
                Zip = visitPayUserDto.Zip,
                MailingAddressStreet1 = visitPayUserDto.MailingAddressStreet1,
                MailingAddressStreet2 = visitPayUserDto.MailingAddressStreet2,
                MailingCity = visitPayUserDto.MailingCity,
                MailingState = visitPayUserDto.MailingState,
                MailingZip = visitPayUserDto.MailingZip,
                Email = visitPayUserDto.Email,
                EmailOriginal = visitPayUserDto.Email,
                FirstName = visitPayUserDto.FirstName,
                LastName = visitPayUserDto.LastName,
                MiddleName = visitPayUserDto.MiddleName,              
                UserName = visitPayUserDto.UserName               
            };

            return this.PartialView("_UserProfileEdit", model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult UserProfileEdit(ProfileEditViewModel model)
        {
            if (!this.ModelState.IsValid)
            {
                return this.Json(new ResultMessage(false, this.GenericErrorMessage));
            }

            bool isUsernameAvailable = this.VisitPayUserApplicationService.Value.IsUsernameAvailable(model.UserName, this.CurrentUserId);
            if (!isUsernameAvailable)
            {
                string resultMessage = LocalizationHelper.GetLocalizedString(TextRegionConstants.UsernameAlreadyExists);
                return this.Json(new ResultMessage(false, resultMessage));
            }

            VisitPayUserDto visitPayUserDto = this.VisitPayUserApplicationService.Value.FindById(this.User.Identity.GetUserId());
            if (visitPayUserDto != null)
            {
                visitPayUserDto.AddressStreet1 = model.AddressStreet1;
                visitPayUserDto.AddressStreet2 = model.AddressStreet2;
                visitPayUserDto.City = model.City;
                visitPayUserDto.State = model.State;
                visitPayUserDto.Zip = model.Zip;
                visitPayUserDto.MailingAddressStreet1 = model.MailingAddressStreet1;
                visitPayUserDto.MailingAddressStreet2 = model.MailingAddressStreet2;
                visitPayUserDto.MailingCity = model.MailingCity;
                visitPayUserDto.MailingState = model.MailingState;
                visitPayUserDto.MailingZip = model.MailingZip;
                visitPayUserDto.Email = model.Email;
                visitPayUserDto.FirstName = model.FirstName;
                visitPayUserDto.LastName = model.LastName;
                visitPayUserDto.MiddleName = model.MiddleName;               
                visitPayUserDto.UserName = model.UserName;              
                visitPayUserDto.VisitPayUserId = this.CurrentUserId;

                IdentityResult identityResult = this.VisitPayUserApplicationService.Value.UpdateUser(visitPayUserDto);

                string resultMessage = identityResult.Succeeded ? LocalizationHelper.GetLocalizedString(TextRegionConstants.PersonalInformationUpdatedSuccessfully) : this.GenericErrorMessage;
                return this.Json(new ResultMessage(identityResult.Succeeded, resultMessage));
            }

            return this.Json(new ResultMessage(false, this.GenericErrorMessage));
        }

        [HttpGet]
        public ActionResult UserPasswordEdit()
        {
            return this.PartialView("_UserPasswordEdit", new PasswordEditViewModel());
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<JsonResult> UserPasswordEdit(PasswordEditViewModel model)
        {
            if (!this.ModelState.IsValid)
            {
                return this.Json(new ResultMessage(false, this.GenericErrorMessage));
            }

            if (!this.VisitPayUserApplicationService.Value.IsPasswordStrongEnough(model.NewPassword).Result.Succeeded)
            {
                string resultMessage = LocalizationHelper.GetLocalizedString(TextRegionConstants.PasswordInvalid);
                return this.Json(new ResultMessage(false, resultMessage));
            }

            IdentityResult identityResult = await this.VisitPayUserApplicationService.Value.ChangePasswordAsync(this.User.Identity.GetUserId(),
                model.OldPassword,
                model.NewPassword,
                this.ClientDto.GuarantorPasswordExpLimitInDays,
                this.ClientDto.GuarantorUnrepeatablePasswordCount).ConfigureAwait(true);

            string updatedSuccessMessage = LocalizationHelper.GetLocalizedString(TextRegionConstants.PasswordUpdated);
            return this.Json(identityResult.Succeeded ? new ResultMessage(true, updatedSuccessMessage) : new ResultMessage(false, string.Join(".  ", identityResult.Errors)));
        }

        [HttpGet]
        public ActionResult UserSecurityQuestionsEdit()
        {
            List<SecurityQuestionViewModel> model = new List<SecurityQuestionViewModel>();
            VisitPayUserDto visitPayUserDto = this.VisitPayUserApplicationService.Value.FindById(this.User.Identity.GetUserId());

            if (visitPayUserDto != null)
            {
                // security questions
                List<SecurityQuestionDto> allQuestions = this._securityQuestionApplicationService.Value.GetAllSecurityQuestions().ToList();

                // maximum of client setting or # of current answers
                int minimumNumberOfQuestions = Math.Max(this.ClientDto.GuarantorSecurityQuestionsCount, visitPayUserDto.SecurityQuestionAnswers.Count());

                // existing questions
                foreach (SecurityQuestionAnswerDto securityQuestion in visitPayUserDto.SecurityQuestionAnswers)
                {
                    SecurityQuestionViewModel securityQuestionViewModel = new SecurityQuestionViewModel
                    {
                        Answer = null,
                        SecurityQuestionId = securityQuestion.SecurityQuestion.SecurityQuestionId,
                        SecurityQuestions = new List<SelectListItem>()
                    };

                    allQuestions.ForEach(x => securityQuestionViewModel.SecurityQuestions.Add(new SelectListItem { Text = x.Question, Value = x.SecurityQuestionId.ToString() }));

                    SelectListItem selectedQuestion = securityQuestionViewModel.SecurityQuestions.FirstOrDefault(x => Convert.ToInt32(x.Value) == securityQuestion.SecurityQuestion.SecurityQuestionId);
                    if (selectedQuestion != null)
                    {
                        selectedQuestion.Selected = true;
                    }

                    model.Add(securityQuestionViewModel);
                }

                // any questions needed to reach the minimum required number
                // scenario: client setting changing after a user has registered
                // example: client setting is now 3, but was 2 at time of registration, should be additional question shown here
                // note: if client setting < current answers, continue showing all current answers
                int difference = minimumNumberOfQuestions - model.Count();
                if (difference > 0)
                {
                    for (int i = 0; i < difference; i++)
                    {
                        SecurityQuestionViewModel securityQuestionViewModel = new SecurityQuestionViewModel
                        {
                            Answer = null,
                            SecurityQuestionId = default(int),
                            SecurityQuestions = new List<SelectListItem>()
                        };

                        allQuestions.ForEach(x => securityQuestionViewModel.SecurityQuestions.Add(new SelectListItem { Text = x.Question, Value = x.SecurityQuestionId.ToString() }));

                        model.Add(securityQuestionViewModel);
                    }
                }
            }

            return this.PartialView("_UserSecurityQuestionsEdit", model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult UserSecurityQuestionsEdit(IList<SecurityQuestionViewModel> model)
        {
            if (!this.ModelState.IsValid)
            {
                return this.Json(new ResultMessage(false, this.GenericErrorMessage));
            }

            VisitPayUserDto visitPayUserDto = this.VisitPayUserApplicationService.Value.FindById(this.User.Identity.GetUserId());
            if (visitPayUserDto == null)
            {
                return this.Json(new ResultMessage(false, this.GenericErrorMessage));
            }

            visitPayUserDto.SecurityQuestionAnswers = new List<SecurityQuestionAnswerDto>();
            foreach (SecurityQuestionViewModel question in model)
            {
                SecurityQuestionAnswerDto mapped = Mapper.Map<SecurityQuestionAnswerDto>(question);
                mapped.SecurityQuestion = new SecurityQuestionDto { SecurityQuestionId = question.SecurityQuestionId };
                visitPayUserDto.SecurityQuestionAnswers.Add(mapped);
            }

            IdentityResult identityResult = this.VisitPayUserApplicationService.Value.UpdateUser(visitPayUserDto);
            if (identityResult.Succeeded)
            {
                this.VisitPayUserApplicationService.Value.LogAddEditSecurityQuestions(
                    user: visitPayUserDto,
                    context: Mapper.Map<JournalEventHttpContextDto>(System.Web.HttpContext.Current),
                    isClient: false
                );
            }

            string resultMessage = identityResult.Succeeded ? LocalizationHelper.GetLocalizedString(TextRegionConstants.SecurityQuestionsUpdated) : this.GenericErrorMessage;
            return this.Json(new ResultMessage(identityResult.Succeeded, resultMessage));
        }

        [HttpGet]
        public ActionResult UserNameEdit()
        {
            return this.PartialView("_UserNameEdit", new ProfileEditUserNameViewModel());
        }
        #endregion

        #region Phone and Text Settings

        [HttpGet]
        public async Task<ActionResult> PhoneTextSettings()
        {
            PhoneTextInformationViewModel model = await this.CreatePhoneTextInformationViewModelAsync();
            return this.View(model);
        }

        [HttpGet]
        [OverrideAuthorization]
        [EmulateUser(AllowPost = true)]
        public async Task<ActionResult> PhoneTextInformation()
        {
            PhoneTextInformationViewModel model = await this.CreatePhoneTextInformationViewModelAsync();
            return this.PartialView("_PhoneTextInformation", model);
        }

        public ActionResult PhoneEdit(SmsPhoneTypeEnum phoneType)
        {
            return this.PartialView("_EditPhone", this.CreateEditPhoneViewModel(phoneType));
        }

        [HttpGet]
        public ActionResult ActivateTextMessaging(SmsPhoneTypeEnum smsPhoneType)
        {
            List<CommunicationSmsBlockedNumberViewModel> blockedNumbers = this.IsNumberBlocked(smsPhoneType).ToList();
            if (!blockedNumbers.Any())
            {
                this.MetricsProvider.Value.Increment(Metrics.Increment.Communication.Sms.ShowTerms);
                return this.PartialView(this.CreateActivateTextMessagingViewModel(smsPhoneType));
            }
            return this.RedirectToAction("PhoneTextSettings");
        }

        #endregion

        #region visits

        [HttpGet]
        public ActionResult MyVisits()
        {
            return this.RedirectToAction("MyPayments", "Payment");
        }

        [HttpPost]
        [OverrideAuthorization]
        [EmulateUser(AllowPost = true)]
        [ValidateAntiForgeryToken]
        public async Task<JsonResult> MyVisits(GridSearchModel<VisitsSearchFilterViewModel> filterModel)
        {
            VisitFilterDto filter = this.GetVisitFilterDto(filterModel);

            VisitResultsDto visitResultsDto = this.VisitApplicationService.Value.GetVisits(this.GuarantorFilterService.Value.VisitPayUserId, filter, filterModel.Page, filterModel.Rows);
            VisitResultsModel model = new VisitResultsModel(filterModel.Page, filterModel.Rows, visitResultsDto.TotalRecords)
            {
                Results = Mapper.Map<List<VisitsSearchResultViewModel>>(visitResultsDto.Visits)
            };

            if (!model.Results.Any())
            {
                return this.Json(model);
            }

            //Call client data api to determine if each visit has EOB data
            Dictionary<string, int> hsVisits = model.Results.Where(x => !string.IsNullOrWhiteSpace(x.SourceSystemKey)).Select(x => new { x.BillingSystemId, x.SourceSystemKey }).Distinct().ToList().ToDictionary(x => x.SourceSystemKey, x => x.BillingSystemId);
            IList<EobIndicatorResultDto> eobIndicators = await Task.FromResult(this._visitEobApplicationService.Value.Get835RemitIndicators(hsVisits));

            if (eobIndicators.IsNotNullOrEmpty())
            {
                IEnumerable<VisitsSearchResultViewModel> visitsWithEob = model.Results.Join(eobIndicators,
                    visit => new { visit.BillingSystemId, visit.SourceSystemKey },
                    eob => new { eob.BillingSystemId, SourceSystemKey = eob.VisitSourceSystemKey },
                    (v, e) => v);

                foreach (VisitsSearchResultViewModel v in visitsWithEob)
                {
                    v.HasVisitEobDetails = true;
                    v.EobLogoUrl = eobIndicators.FirstOrDefault(x => x.BillingSystemId == v.BillingSystemId && x.VisitSourceSystemKey == v.SourceSystemKey)?.LogoUrl ?? ImagePath.EobIcon;
                }
            }

            await this.AssignServiceGroupAttributesAsync(model);

            return this.Json(model);
        }

        [HttpPost]
        [OverrideAuthorization]
        [EmulateUser(AllowPost = true)]
        [ValidateAntiForgeryToken]
        // todo: rename to something else - this is the homepage grid
        public async Task<JsonResult> CurrentStatementVisits(int page, int rows, string sidx, string sord)
        {
            GridSearchModel<VisitsSearchFilterViewModel> filterModel = new GridSearchModel<VisitsSearchFilterViewModel>
            {
                Filter = new VisitsSearchFilterViewModel
                {
                    IsOnActiveFinancePlan = false,
                    VisitStateIds = new List<VisitStateEnum>
                    {
                        VisitStateEnum.Active
                    }
                },
                Page = page,
                Rows = rows,
                Sidx = sidx,
                Sord = sord
            };

            return await this.MyVisits(filterModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Common.Web.Attributes.SessionState(SessionStateBehavior.Required)]
        public async Task<ActionResult> MyVisitsExport(GridSearchModel<VisitsSearchFilterViewModel> filterModel)
        {
            VisitFilterDto filter = this.GetVisitFilterDto(filterModel);

            byte[] bytes = await this.VisitApplicationService.Value.ExportVisitsAsync(this.GuarantorFilterService.Value.VisitPayUserId, filter, this.User.Identity.GetUserName());
            this.SetTempData(MyVisitsExportKey, bytes);

            return this.Json(new ResultMessage(true, this.Url.Action("MyVisitsExportDownload")));
        }

        [HttpGet]
        public ActionResult MyVisitsExportDownload()
        {
            object tempData = this.TempData[MyVisitsExportKey];
            if (tempData != null)
            {
                this.WriteExcelInline((byte[])tempData, "MyVisits.xlsx");
            }

            return this.Json(new { }, JsonRequestBehavior.AllowGet);
        }

        private VisitFilterDto GetVisitFilterDto(GridSearchModel<VisitsSearchFilterViewModel> model)
        {
            model.Filter.VisitStateIds = model.Filter.VisitStateIds ?? new List<VisitStateEnum>();

            VisitFilterDto filter = new VisitFilterDto
            {
                SortField = model.Sidx,
                SortOrder = model.Sord
            };

            Mapper.Map(model.Filter, filter);

            return filter;
        }

        private async Task AssignServiceGroupAttributesAsync(VisitResultsModel model)
        {
            IList<ServiceGroupDto> serviceGroupDtos = await this._serviceGroupApplicationService.Value.GetAllServiceGroupsWithContentAsync();
            
            foreach (VisitsSearchResultViewModel visit in model.Results.Where(x => x.ServiceGroupId.HasValue))
            {
                ServiceGroupDto serviceGroupDto = serviceGroupDtos.FirstOrDefault(x => x.ServiceGroupId == visit.ServiceGroupId);
                if (serviceGroupDto == null || string.IsNullOrWhiteSpace(serviceGroupDto.Logo))
                {
                    continue;
                }
                
                visit.ServiceGroupLogoUrl = $"{UrlPath.ClientImagesPath}{serviceGroupDto.Logo}";
                visit.ServiceGroupDescription = serviceGroupDto.Description ?? string.Empty;
            }
        }

        #endregion

        #region visit details / transactions

        [HttpGet]
        public async Task<ActionResult> VisitDetails(int visitId)
        {
            VisitDetailsViewModel model = await this.GetVisitDetailsModelAsync(visitId);

            return this.PartialView("_VisitDetails", model);
        }

        [HttpGet]
        public async Task<ActionResult> VisitDetailsContent(int visitId)
        {
            VisitDetailsViewModel model = await this.GetVisitDetailsModelAsync(visitId);
            
            return this.PartialView("_VisitDetailsContent", model);
        }

        private async Task<VisitDetailsViewModel> GetVisitDetailsModelAsync(int visitId)
        {
            int userId = this.GuarantorFilterService.Value.VisitPayUserId;

            VisitDto visit = this.VisitApplicationService.Value.GetVisit(userId, visitId);
            VisitTransactionSummaryDto summary = this._visitTransactionApplicationService.Value.GetTransactionSummary(userId, visitId);

            VisitDetailsViewModel model = Mapper.Map<VisitDetailsViewModel>(visit);
            Mapper.Map(summary, model);

            if (this.IsFeatureEnabled(VisitPayFeatureEnum.ExplanationOfBenefitsUserInterface))
            {
                //Call client data api to determine if each visit has EOB data
                if (!string.IsNullOrWhiteSpace(visit.SourceSystemKey))
                {
                    Dictionary<string, int> hsVisits = new { visit.BillingSystemId, visit.SourceSystemKey }.ToListOfOne().ToDictionary(x => x.SourceSystemKey, x => x.BillingSystemId);
                    IList<EobIndicatorResultDto> eobIndicators = await Task.FromResult(this._visitEobApplicationService.Value.Get835RemitIndicators(hsVisits));
                    EobIndicatorResultDto eobIndicator = eobIndicators?.FirstOrDefault(x => x.BillingSystemId == visit.BillingSystemId && x.VisitSourceSystemKey == visit.SourceSystemKey);
                    if (eobIndicator != null)
                    {
                        model.HasEob = true;
                        model.EobLogoUrl = eobIndicator.LogoUrl ?? ImagePath.EobIcon;
                    }
                }
            }

            return model;
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [OverrideAuthorization, EmulateUser(AllowPost = true)]
        public async Task<PartialViewResult> VisitDetailsEob(int visitId)
        {
            if (!this.IsFeatureEnabled(VisitPayFeatureEnum.ExplanationOfBenefitsUserInterface))
            {
                return this.PartialView("_VisitDetailsEob", null);
            }

            int visitPayUserId = this.GuarantorFilterService.Value.VisitPayUserId;
            VisitDto visit = this.VisitApplicationService.Value.GetVisit(visitPayUserId, visitId);

            try
            {
                IReadOnlyList<VisitEobDetailsResultDto> visitEobDetailsResultDtos = await Task.FromResult((IReadOnlyList<VisitEobDetailsResultDto>)this._visitEobApplicationService.Value.GetVisitEobDetails(visit.BillingSystemId, visit.SourceSystemKey));
                VisitEobDetailViewListModel visitEobDetailViewListModel = this.GetVisitEobDetailViewListModel(visitEobDetailsResultDtos);

                return this.PartialView("_VisitDetailsEob", visitEobDetailViewListModel);
            }
            catch (Exception ex)
            {
                return this.PartialView("_VisitDetailsEob", null);
            }
        }

        [HttpPost]
        [OverrideAuthorization]
        [EmulateUser(AllowPost = true)]
        [ValidateAntiForgeryToken]
        public JsonResult VisitTransactions(GridSearchModel<VisitTransactionsSearchFilterViewModel> filterModel)
        {
            VisitTransactionFilterDto visitTransactionFilterDto = new VisitTransactionFilterDto();
            Mapper.Map(filterModel, visitTransactionFilterDto);

            int visitPayUserId = this.GuarantorFilterService.Value.VisitPayUserId;
            VisitTransactionResultsDto visitTransactionResultsDto = this._visitTransactionApplicationService.Value.GetTransactions(visitPayUserId, visitTransactionFilterDto, filterModel.Page, filterModel.Rows);

            return this.Json(new VisitTransactionResultsModel(filterModel.Page, filterModel.Rows, visitTransactionResultsDto.TotalRecords)
            {
                Results = Mapper.Map<IList<VisitTransactionsSearchResultViewModel>>(visitTransactionResultsDto.VisitTransactions)
            });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Common.Web.Attributes.SessionState(SessionStateBehavior.Required)]
        public async Task<JsonResult> VisitTransactionsExport(GridSearchModel<VisitTransactionsSearchFilterViewModel> model)
        {
            VisitTransactionFilterDto visitTransactionFilterDto = new VisitTransactionFilterDto();
            Mapper.Map(model, visitTransactionFilterDto);

            int visitPayUserId = this.GuarantorFilterService.Value.VisitPayUserId;
            byte[] bytes = await this._visitTransactionApplicationService.Value.ExportVisitTransactionsAsync(visitPayUserId, visitTransactionFilterDto, this.User.Identity.GetUserName());
            this.SetTempData(VisitTransactionsExportKey, bytes);

            return this.Json(new ResultMessage(true, this.Url.Action("VisitTransactionsExportDownload")));
        }

        [HttpGet]
        public ActionResult VisitTransactionsExportDownload()
        {
            object tempData = this.TempData[VisitTransactionsExportKey];
            if (tempData != null)
            {
                this.WriteExcelInline((byte[])tempData, "MyTransactions.xlsx");
            }

            return this.Json(new { }, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region payment summary

        [HttpGet]
        [RequireFeature(VisitPayFeatureEnum.PaymentSummary)]
        public ActionResult PaymentSummaryPartial()
        {
            return this.PartialView("_PaymentSummary", new PaymentSummarySearchFilterViewModel());
        }

        [HttpPost]
        [OverrideAuthorization]
        [EmulateUser(AllowPost = true)]
        [ValidateAntiForgeryToken]
        [RequireFeature(VisitPayFeatureEnum.PaymentSummary)]
        public JsonResult PaymentSummary(GridSearchModel<PaymentSummarySearchFilterViewModel> filterModel)
        {
            PaymentSummaryFilterDto filter = Mapper.Map<PaymentSummaryFilterDto>(filterModel);

            PaymentSummarySearchResultsDto summaryDto = this._visitTransactionApplicationService.Value.GetPaymentSummary(this.GuarantorFilterService.Value.VpGuarantorId, filter, filterModel.Page, filterModel.Rows);

            PaymentSummaryResultsViewModel model = new PaymentSummaryResultsViewModel(filterModel.Page, filterModel.Rows, summaryDto.TotalRecords, summaryDto.SummaryBalanceTotal)
            {
                Results = Mapper.Map<List<PaymentSummaryGroupedViewModel>>(summaryDto.Visits)
            };

            return this.Json(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [RequireFeature(VisitPayFeatureEnum.PaymentSummary)]
        [Common.Web.Attributes.SessionState(SessionStateBehavior.Required)]
        public async Task<JsonResult> PaymentSummaryExport(GridSearchModel<PaymentSummarySearchFilterViewModel> filterModel)
        {
            PaymentSummaryFilterDto filter = Mapper.Map<PaymentSummaryFilterDto>(filterModel);

            byte[] bytes = await this._visitTransactionApplicationService.Value.ExportPaymentSummaryAsync(this.GuarantorFilterService.Value.VpGuarantorId, filter, this.User.Identity.GetUserName());
            this.SetTempData(PaymentSummaryExportKey, bytes);

            return this.Json(new ResultMessage(true, this.Url.Action("PaymentSummaryExportDownload")));
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [RequireFeature(VisitPayFeatureEnum.PaymentSummary)]
        [Common.Web.Attributes.SessionState(SessionStateBehavior.Required)]
        public ActionResult PaymentSummaryExportPdf(GridSearchModel<PaymentSummarySearchFilterViewModel> filterModel)
        {
            PaymentSummaryFilterDto filter = Mapper.Map<PaymentSummaryFilterDto>(filterModel);

            PaymentSummarySearchResultsDto summaryDto = this._visitTransactionApplicationService.Value.GetPaymentSummary(this.GuarantorFilterService.Value.VpGuarantorId, filter, 1, 10000);

            this.ViewData.Model = new PaymentSummaryPdfViewModel
            {
                DateRange = $"{filter.PostDateBegin?.ToString(Format.MonthDayYearFormat)} - {filter.PostDateEnd?.ToString(Format.MonthDayYearFormat)}",
                FileDate = DateTime.UtcNow.ToString(Format.MonthDayYearFormat),
                ExportedBy = this.User.Identity.GetUserName(),
                TotalTransactionAmount = summaryDto.SummaryBalanceTotal.FormatCurrency(),
                PaymentSummaries = Mapper.Map<IList<PaymentSummaryGroupedTransactionPdfViewModel>>(summaryDto.Visits.SelectMany(x => x.Payments).OrderBy(x => x.PostDate))
            };

            this.SetTempData(PaymentSummaryExportPdfKey, this._pdfConverter.Value.CreatePdfFromHtml("Payment Summary", this.RenderViewToString("~/Views/Account/_PaymentSummaryPdf.cshtml", "~/Views/Shared/_LayoutPrint.cshtml")));

            HttpContext context = System.Web.HttpContext.Current;
            VisitPayUserDto visitPayUserDto = this.VisitPayUserApplicationService.Value.FindById(this.User.Identity.GetUserId());
            this.VisitPayUserJournalEventApplicationService.Value.LogPaymentSummaryDownload(visitPayUserDto?.VisitPayUserId ?? 0, Mapper.Map<JournalEventHttpContextDto>(context));

            return this.Json(new ResultMessage(true, this.Url.Action("PaymentSummaryExportDownload", new { pdf = true })));
        }

        [HttpGet]
        public ActionResult PaymentSummaryExportDownload(bool? pdf)
        {
            if (true.Equals(pdf))
            {
                object tempData = this.TempData[PaymentSummaryExportPdfKey];
                if (tempData != null)
                {
                    this.WritePdfInline((byte[])tempData, "PaymentSummary.pdf");
                }
            }
            else
            {
                object tempData = this.TempData[PaymentSummaryExportKey];
                if (tempData != null)
                {
                    this.WriteExcelInline((byte[])tempData, "PaymentSummary.xlsx");
                }
            }

            return this.Json(new { }, JsonRequestBehavior.AllowGet);
        }

        #endregion
    }
}
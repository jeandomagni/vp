﻿namespace Ivh.Web.Patient.Attributes
{
    using System;
    using System.Web.Mvc;
    using Application.Core.Common.Dtos;
    using Application.Core.Common.Interfaces;
    using Common.VisitPay.Enums;
    using Common.VisitPay.Strings;
    using Common.Web.Extensions;

    public class GuarantorAuthorizeAttribute : AuthorizeAttribute
    {
        public GuarantorAuthorizeAttribute()
        {
            this.Roles = $"{VisitPayRoleStrings.System.Patient},{VisitPayRoleStrings.Admin.Administrator}";
        }

        public override void OnAuthorization(AuthorizationContext filterContext)
        {
            base.OnAuthorization(filterContext);
            this.Authorize(filterContext);
        }

        public void Authorize(AuthorizationContext filterContext)
        {
            bool skipAuthorization = filterContext.IsAttributeDefined<AllowAnonymousAttribute>();
            if (skipAuthorization)
            {
                return;
            }

            if (!filterContext.HttpContext.User.Identity.IsAuthenticated)
            {
                return;
            }

            VpGuarantorStatusEnum? currentGuarantorStatus = filterContext.HttpContext.User.CurrentGuarantorStatus();
            GuarantorTypeEnum? currentGuarantorType = filterContext.HttpContext.User.CurrentGuarantorType();

            if (!currentGuarantorStatus.HasValue || !currentGuarantorType.HasValue)
            {
                int? currentGuarantorId = filterContext.HttpContext.User.CurrentGuarantorId();
                if (currentGuarantorId.HasValue)
                {
                    IGuarantorApplicationService guarantorApplicationService = DependencyResolver.Current.GetService<IGuarantorApplicationService>();
                    GuarantorDto vpGuarantor = guarantorApplicationService.GetGuarantor(currentGuarantorId.Value);

                    currentGuarantorStatus = vpGuarantor.VpGuarantorStatus;
                    currentGuarantorType = vpGuarantor.VpGuarantorTypeEnum;
                }
            }

            if (!currentGuarantorStatus.HasValue || !currentGuarantorType.HasValue)
            {
                // not authorized
                filterContext.Result = new HttpUnauthorizedResult();
                return;
            }

            if (currentGuarantorStatus == VpGuarantorStatusEnum.Closed)
            {
                // not authorized
                filterContext.Result = new HttpUnauthorizedResult("Guarantor Account is closed");
                filterContext.Controller?.ViewData?.ModelState?.AddModelError("", "Guarantor Account is closed");
                return;
            }

            if (currentGuarantorType == GuarantorTypeEnum.Offline && !filterContext.IsAttributeDefined<AllowOffline>())
            {
                // not authorized
                filterContext.Result = new HttpUnauthorizedResult();
                return;
            }

            if (currentGuarantorType != GuarantorTypeEnum.Offline && filterContext.IsAttributeDefined<OfflineOnly>())
            {
                // not authorized - throw a 404
                filterContext.Result = new HttpNotFoundResult();
                return;
            }

            // ...authorized
        }
    }
    
    public class AllowOffline : Attribute
    {
    }

    public class OfflineOnly : Attribute
    {
    }
}
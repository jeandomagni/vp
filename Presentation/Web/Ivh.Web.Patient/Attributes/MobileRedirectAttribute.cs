﻿namespace Ivh.Web.Patient.Attributes
{
    using System.Web;
    using System.Web.Mvc;
    using SessionFacade;

    public class MobileRedirectAttribute : ActionFilterAttribute
    {
        private readonly string _url;
        private readonly bool _appendQueryString;

        public MobileRedirectAttribute(string url = "/mobile", bool appendQueryString = false)
        {
            this._url = url;
            this._appendQueryString = appendQueryString;
        }

        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            IWebPatientSessionFacade session = DependencyResolver.Current.GetService<IWebPatientSessionFacade>();
            HttpRequestBase request = filterContext.RequestContext.HttpContext.Request;

            bool isMobileDevice = session.UserIsMobileDevice.GetValueOrDefault(request.Browser.IsMobileDevice);

            // a mobile device or user has requested mobile site, but a request to the desktop site
            if (isMobileDevice && !request.Path.StartsWith("/mobile", true, null))
            {
                string redirectUrl = this._url;
                if (this._appendQueryString && request.QueryString.HasKeys())
                {
                    redirectUrl += $"?{request.QueryString}";
                }

                filterContext.Result = new RedirectResult(redirectUrl);
            }

            base.OnActionExecuting(filterContext);
        }
    }
}
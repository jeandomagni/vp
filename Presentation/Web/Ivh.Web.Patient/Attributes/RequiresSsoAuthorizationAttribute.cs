﻿namespace Ivh.Web.Patient.Attributes
{
    using System;
    using System.Security.Claims;
    using System.Web.Mvc;
    using System.Web.Routing;
    using Common.Base.Enums;
    using Common.VisitPay.Enums;

    public sealed class RequiresSsoAuthorizationAttribute : ActionFilterAttribute
    {
        public RequiresSsoAuthorizationAttribute()
        {
            this.Enabled = true;
        }

        public bool Enabled { get; set; }

        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            base.OnActionExecuting(filterContext);
            if (this.Enabled && 
                filterContext.HttpContext.User != null && 
                filterContext.HttpContext.User.Identity is ClaimsIdentity && 
                ((ClaimsIdentity) filterContext.HttpContext.User.Identity).HasClaim(c => c.Type.Equals(ClaimTypeEnum.RequiresSsoAuthorization.ToString(), StringComparison.OrdinalIgnoreCase)))
            {
                filterContext.Result = new RedirectToRouteResult(new RouteValueDictionary
                {
                    {"Controller", "Sso"},
                    {"Action", "Terms"}
                });
            }
        }
    }
}
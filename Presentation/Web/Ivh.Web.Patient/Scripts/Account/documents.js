﻿window.VisitPay.Itemizations = (function () {

    var ItemizationsViewModel = function () {

        var self = this;

        self.Filter = {
            DateRange: ko.observable(''),
            FacilityCode: ko.observable(''),
            PatientName: ko.observable('')
        };

        return self;
    };

    var $scope = $('#section-itemizations');
    var model = new ItemizationsViewModel();
    var $filter;
    var gridVm = {};

    function bindFilter() {

        if ($filter !== undefined && $filter !== null && $filter.length > 0) {
            ko.cleanNode($filter[0]);
        }

        $filter = $scope.find('#frmItemizationsFilter');

        ko.applyBindings(model, $filter[0]);
    }

    function init() {

        if ($scope.length === 0) {
            return;
        }

        //Apply filter bindings
        bindFilter();

        //Setup VP Grid
        window.VisitPay.VpGridCommon.WithLazyGrid(gridVm);
        var url = '/Itemization/Itemizations';
        gridVm.Grid = window.VisitPay.VpGrid(model.Filter, url, 5, null, null);
        ko.applyBindings(gridVm, $('#gridItemizations')[0]);

        //Perform initial load
        gridVm.Grid.Search(true);

        //Filter form submit
        $scope.on('submit', 'form', function (e) {
            e.preventDefault();

            if ($(this).valid()) {
                gridVm.Grid.Search(true);
            }
        });

        //Filter reset
        $scope.on('click', 'button[data-reset=true]', function (e) {

            e.preventDefault();
            
            //Reset Grid
            model.Filter.FacilityCode(null);
            model.Filter.PatientName(null);
            model.Filter.DateRange(null);

            //Reset form errors
            window.VisitPay.Common.ResetUnobtrusiveValidation($(this).parents('form').eq(0));

            //Perform search
            gridVm.Grid.Search(true);
        });

        //Download Itemization
        $scope.on('click', '.download-anchor', function (e) {
            var visitFileStoredId = $(e.target).data('visitFileStoredId');

            $.blockUI();

            $.get('/Itemization/ItemizationDownloadModal', function (cmsVersion) {
                $.unblockUI();

                window.VisitPay.Common.ModalGenericConfirmationMd(cmsVersion.ContentTitle, cmsVersion.ContentBody, 'Continue Download', 'Cancel').then(function () {
                    window.open('/Itemization/ItemizationDownload/' + visitFileStoredId);
                });
            });
        });
    }
    
    return {
        init: init
    };

});

window.VisitPay.Statements = (function(ko) {

    var statementViewModel = function() {
        var self = this;

        self.StatementedSnapshotTotalBalance = ko.observable('');
        self.StatementDate = ko.observable('');
        self.StatementId = ko.observable(0);
        self.StatementIsSelected = ko.computed(function() {
            return ko.unwrap(self.StatementId) > 0;
        });
        return self;
    };

    var $scope = $('#section-statements #statements-list');
    var statementModel;
    var visitBalanceSummaryModel;

    function downloadDocument(e) {

        e.preventDefault();

        if (!e.data.model.StatementIsSelected()) {
            bindStatement(e, $(this));
        }

        if (window.VisitPay.State.IsEmulation) {
            $('.modal').modal('hide');
            $('#modalEmulate').modal('show');
            $('#modalEmulate').find('.modal-body').html('<p>Emulated User Not Authorized for this action.</p>');
        } else {
            window.open(e.data.actionUrl + e.data.model.StatementId());
            window.VisitPay.Survey.ShowSurvey(
                {
                    "SurveyName": 'ViewingStatement'
                });
        }
    };

    function bindStatement(e, innerScope) {
        var $innerScope = innerScope || $(this);
        var id = $innerScope.find('input[type=hidden][name=statementId]').val();
        var balance = $innerScope.find('input[type=hidden][name=statementBalance]').val();
        var date = $innerScope.find('input[type=hidden][name=statementDate]').val();

        e.data.model.StatementId(id);
        e.data.model.StatementDate(date);
        e.data.model.StatementedSnapshotTotalBalance(balance);
    };

    function init() {
        statementModel = new statementViewModel();
        visitBalanceSummaryModel = new statementViewModel();

        $scope.on('click', '#ddStatement button.btn-download', { model: statementModel, actionUrl: '/Statement/FinancePlanStatementDownload/' }, downloadDocument);
        $scope.on('click', '#ddVisitBalanceSummary button.btn-download', { model: visitBalanceSummaryModel, actionUrl: '/Statement/VisitBalanceSummaryDownload/' }, downloadDocument);
        $scope.on('click', '#ddStatement li:not(.archived-separator)', { model: statementModel }, bindStatement);
        $scope.on('click', '#ddVisitBalanceSummary li:not(.archived-separator)', { model: visitBalanceSummaryModel }, bindStatement);

        var $fpStatements = $scope.find('#ddStatement')[0];
        var $visitBalanceStatements = $scope.find('#ddVisitBalanceSummary')[0];

        if ($fpStatements) {
            ko.applyBindings(statementModel, $fpStatements);
        }
        if ($visitBalanceStatements) {
            ko.applyBindings(visitBalanceSummaryModel, $visitBalanceStatements);
        }
    };
    
    return {
        init: init
    };

});

window.VisitPay.DocumentCenter = (function(ko) {

    $(document).ready(function() {

        var itemizations = new window.VisitPay.Itemizations();
        var statements = new window.VisitPay.Statements(ko);

        statements.init();
        itemizations.init();

        $(document).on('guarantorFilterChanged', function(e) {

            $.blockUI();
            window.location.reload();

        });

    });

})(window.ko);
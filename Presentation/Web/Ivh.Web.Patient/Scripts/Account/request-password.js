﻿(function () {

    $(document).ready(function () {

        var $scope = $('#section-requestpassword');

        $scope.find('#Ssn4').numbersOnly(4);
        $scope.find('#DateOfBirthDay').numbersOnly(2);
        $scope.find('#DateOfBirthYear').numbersOnly(4);
        $scope.find('#frmRequestPassword').commonForm($scope).done(function(url) {
            window.location.href = url;
        });

    });

})();
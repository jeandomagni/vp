﻿(function () {

    $(document).ready(function () {

        var $scope = $('#section-passwordexpired');

        $scope.find('.switch-password').switchPassword();
        $scope.find('#frmPasswordExpired').commonForm($scope).done(function(url) {
            window.location.href = url;
        });

    });

})();
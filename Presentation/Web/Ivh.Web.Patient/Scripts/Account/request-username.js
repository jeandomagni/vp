﻿(function () {

    $(document).ready(function () {

        var $scope = $('#section-requestusername');

        $scope.find('#Ssn4').numbersOnly(4);
        $scope.find('#DateOfBirthDay').numbersOnly(2);
        $scope.find('#DateOfBirthYear').numbersOnly(4);
        $scope.find('#frmRequestUsername').commonForm($scope).done(function(url) {
            window.location.href = url;
        });

    });

})();
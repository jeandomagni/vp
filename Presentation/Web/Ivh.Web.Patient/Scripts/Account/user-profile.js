﻿(function (visitPay, $) {

    var reloadPersonalInformation = function () {

        $.get('/Account/UserProfilePersonalInformation', {}, function (html) {
            $('#section-userpersonalinformation').html(html);
            registerEvents();
        });

    };

    var modalSubmit = function (e, form, url, reload) {

        e.preventDefault();
        visitPay.Common.ResetValidationSummary(form);

        if (form.valid()) {

            $.blockUI();

            $.post(url, form.serialize(), function (result) {

                $.unblockUI();

                if (!result.Result)
                    visitPay.Common.AppendMessageToValidationSummary(form, result.Message);
                else {
                    form.parents('#modal:eq(0)').modal('hide');
                    $('#modalResult').find('.modal-body').html(result.Message);
                    $('#modalResult').modal('show');

                    if (reload)
                        reloadPersonalInformation();
                }
            });

        }

    };

    var modalLoadCallback = function (postUrl, reload) {
        $.unblockUI();

        var form = $('#modal form:eq(0)');
        form.removeData('validator').removeData('unobtrusiveValidation');
        $.validator.unobtrusive.parse(form);

        form.find('#Zip').zipCode();

        form.off('submit').on('submit', function(e) {
            modalSubmit(e, $(this), postUrl, reload);
        });

    };

    function setProfileEditModalSize(small) {
        var modalDialog = $('#profile-modal-dialog');
        if (small) {
            modalDialog.removeClass('modal-md');
            modalDialog.addClass('modal-sm');
        } else {
            modalDialog.removeClass('modal-sm');
            modalDialog.addClass('modal-md');
        }
    }

    var registerEvents = function () {
        $('#btnEditPersonalInformation').on('click', function (e) {
            $.blockUI();
            setProfileEditModalSize(false);
            visitPay.Common.ModalAsync($('#modal'), window.VisitPay.Localization.EditPersonalInformation, '/Account/UserProfileEdit', null, null, function () { modalLoadCallback('/Account/UserProfileEdit', true); });
        });

        $('#btnEditPassword').on('click', function (e) {
            $.blockUI();
            setProfileEditModalSize(true);
            visitPay.Common.ModalAsync($('#modal'), window.VisitPay.Localization.EditPassword, '/Account/UserPasswordEdit', null, null, function () { modalLoadCallback('/Account/UserPasswordEdit', false); });
        });

        $('#btnEditSecurityQuestions').on('click', function (e) {
            $.blockUI();
            setProfileEditModalSize(false);
            visitPay.Common.ModalAsync($('#modal'), window.VisitPay.Localization.EditSecurityQuestions, '/Account/UserSecurityQuestionsEdit', null, null, function () {

                modalLoadCallback('/Account/UserSecurityQuestionsEdit', false);

                var ensureUniqueSelection = function () {
                    var selects = $('select[name$=".SecurityQuestionId"]');
                    selects.each(function () {

                        var self = $(this);
                        self.find('option').prop('disabled', false);

                        selects.not(self).each(function () {
                            if ($(this).val().length > 0)
                                self.find('option[value="' + $(this).val() + '"]').attr('disabled', 'disabled');
                        });

                    });
                };

                $('select[name$=".SecurityQuestionId"]').each(ensureUniqueSelection);
                $('select[name$=".SecurityQuestionId"]').on('change', function () {
                    ensureUniqueSelection();
                    $(this).parents('.row:eq(0)').next().next().find('input[type=text][name$=".Answer"]').val('');
                });

            });
        });

        $('#btnEditUserName').on('click', function (e) {
            $.blockUI();
            setProfileEditModalSize(true);
            visitPay.Common.ModalAsync($('#modal'), window.VisitPay.Localization.EditUsername, '/Account/UserNameEdit', null, null, function () { modalLoadCallback('/Account/UpdateUsername', true); });
        });

    };

    $(document).ready(function () {
        registerEvents();
    });

})(window.VisitPay, jQuery);
﻿(function() {
    $(document).ready(function () {
        $(document).on('gridrowclick.common', '.grid-visits > .grid-row, .grid-visits-single-line > .grid-row', function (e, obj) {
            if (obj.isVisible) {
                if ($.trim(obj.target.html()).length === 0) {
                    var data = ko.dataFor(obj.sender[0]);
                    var details = window.VisitPay.VisitDetails();
                    details.inline(obj.target, ko.unwrap(data.VisitId));
                }
            }
        });
    });
})();
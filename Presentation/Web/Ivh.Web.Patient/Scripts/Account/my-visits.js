﻿// this file is shared between mobile and web

(function () {

    var myVisitsUrl = '/Account/MyVisits';

    var visitStateEnum = {
        Active: 1,
        Inactive: 2
    };
    
    function showDrugDisclosure(b) {

        var $disclosure = $('#drug-disclosure');

        if (b === false) {
            $disclosure.hide();
            return;
        }
        
        var $filter = $('#GuarantorFilterItems');
        var $allUsers = $filter.find('.guarantor-option');

        if ($filter.length === 0 || $allUsers.length === 0) {
            $disclosure.hide();
            return;
        }

        var $selectedUser = $filter.find('#selected-guarantor').text().trim();
        var $managingUser = $($allUsers[0]).text().trim();
        
        if ($selectedUser === $managingUser) {
            $disclosure.hide();
            return;
        }

        $disclosure.show();
    }

    function getFilter(visitState, isOnActiveFinancePlan) {
        var obj = $.extend({ isOnActiveFinancePlan: isOnActiveFinancePlan }, {}, {});
        if (visitState != null) {
            obj = $.extend({ visitStateIds: visitState }, obj, {});
        }
        return obj;
    }

    $(document).ready(function() {
        
        // 
        var vm = {};
        window.VisitPay.VpGridCommon.WithLazyGrid(vm);
        vm.Grid = window.VisitPay.VpGrid(function() { return getFilter([visitStateEnum.Active, visitStateEnum.Inactive], null); }, myVisitsUrl, 25, 'DischargeDate', 'desc');
        ko.applyBindings(vm, $('#gridVisits').parent()[0]);
        
        // grid click
        $(document).off('gridrowclick.visits').on('gridrowclick.visits', '.grid-visits > .grid-row', function(e, obj) {
            window._paq.push(['trackEvent', 'My Visits', 'Click', 'Visit Details - ' + (obj.isVisible ? 'Show' : 'Hide')]);
        });
        
        // export piwik
        $(document).on('click.exportvisits.piwik', '#btnExportVisits', function() { window._paq.push(['trackEvent', 'Visits', 'Export']); });

        // load
        var isExpandedOnLoad = false;
        vm.IsExpanded.subscribe(function (newValue) {

            if (ko.unwrap(newValue) === true) {
                if (!ko.unwrap(vm.Load.ShouldLoad)) {
                    return;
                }
                var promise = vm.Grid.Search(!isExpandedOnLoad);
                vm.Load.Start(promise);
                promise.done(function() {
                    showDrugDisclosure();
                });
            }
            
            var action = ko.unwrap(newValue) ? 'Show' : 'Hide';
            window._paq.push(['trackEvent', 'Visits', 'Click', 'Open - ' + action]);
        });
        vm.IsExpanded(isExpandedOnLoad);
    });

})();
﻿(function (visitPay, $, ko) {

    // view model
    var ActivateViewModel = function () {
        //
        var self = this;

        self.ActivateTextCmsVersionId = ko.observable('');
        self.ClientTermsOfUseCmsVersionId = ko.observable('');
        self.ActivateTextAgreed = ko.observable(false);
        self.ClientTermsOfUseAgreed = ko.observable(false);
        self.ValidationCode = ko.observable('');
        self.PhoneNumber = ko.observable('');
        self.SmsPhoneType = ko.observable('');
        self.SmsRetryCount = ko.observable(0);

        self.State = {
            CurrentStep: ko.observable(1),
            MaxStep: ko.observable(1),

            IsStepDisabled: function (step) {
                return self.State.MaxStep() < step;
            },
            IsStepVisible: function (step) {
                return self.State.CurrentStep() != step;
            },

            GoStep: function (step) {

                if (!step) {
                    step = ko.unwrap(self.State.MaxStep);
                    if (step === ko.unwrap(self.State.CurrentStep))
                        step = step + 1;
                }

                var previousStep = self.State.CurrentStep();

                self.State.CurrentStep(step);

                if (previousStep > step) {
                    self.State.MaxStep(step);
                } else {
                    self.State.MaxStep(Math.max(ko.unwrap(self.State.MaxStep), step));
                }

                var index = (ko.unwrap(self.State.CurrentStep) - 1);
                $('#section-activate-text-messaging .panel-collapse').not(':eq(' + index + ')').collapse('hide');
                $('#section-activate-text-messaging .panel-collapse:eq(' + index + ')').collapse('show');

            }
        };
    };

    //
    window.VisitPay.InitActivateTextMessaging = function ($modal) {
        var model = new ActivateViewModel();
        ko.applyBindings(model, $('#section-activate-text-messaging')[0]);

        //
        ActivateViewModel.prototype.toJSON = function () {
            var copy = ko.toJS(this);
            delete copy.State;

            copy.ActivateTextCmsVersionId = $('#ActivateTextCmsVersionId').val();
            copy.ClientTermsOfUseCmsVersionId = $('#ClientTermsOfUseCmsVersionId').val();
            copy.PhoneNumber = $('#PhoneNumber').val();
            copy.SmsPhoneType = $('#SmsPhoneType').val();

            return copy;
        };

        //
        var forms = $('#section-activate-text-messaging form');

        $.validator.setDefaults({
            ignore: null
        });

        // all sections
        forms.on('submit', function (e) {
            e.preventDefault();
            visitPay.Common.ResetValidationSummary($(this));
        });

        // accept text messaging
        $(forms[0]).on('submit', function (e) {

            var self = $(this);
            if (self.valid()) {

                $.blockUI();
                
                $.post(self.attr('action'), JSON.parse(ko.toJSON(model)), function (result) {

                    $.unblockUI();

                    if (result.Result)
                        model.State.GoStep();
                    else
                        visitPay.Common.AppendMessageToValidationSummary(self, result.Message);
                }, 'json');
            }
        });

        // validate 
        $(forms[1]).on('submit', function (e) {

            var self = $(this);
            if (self.valid()) {

                $.blockUI();
                
                $.ajax({
                    url: self.attr('action'),
                    type: 'POST',
                    dataType: 'json',
                    data: JSON.parse(ko.toJSON(model))
                }).done(function(result) {
                    if (result.Result) {
                        $.unblockUI();
                        $modal.modal('hide');
                         window.VisitPay.Survey.ShowSurvey(
                            {
                                "SurveyName": 'SigningUpForSMS'
                            }).done(function() {                              
                                window.location.href = result.Message;
                             });
                    } else {
                        $.unblockUI();
                        model.SmsRetryCount(model.SmsRetryCount() + 1);
                        model.ValidationCode('');
                        visitPay.Common.AppendMessageToValidationSummary(self, result.Message);
                    }
                });
            }

        });
    };

})(window.VisitPay, jQuery, window.ko);
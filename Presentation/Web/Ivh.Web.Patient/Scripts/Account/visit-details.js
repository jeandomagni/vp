﻿// this file is shared between mobile and web

window.VisitPay.VisitDetails = (function () {

    var scrollDuration = 250;
    var ViewModel = function() {

        var transactionFilterEnum = {
            OtherFilter: 4,
            ChargeFilter: 1,
            InsuranceFilter: 2,
            InterestFilter: 3,
            PaymentFilter: 5
        };

        var menuItemEnum = {
            Adjustments: 'Adjustments',
            Charges: 'Charges',
            Eob: 'Eob',
            FeesInterest: 'FeesInterest',
            Insurance: 'Insurance',
            Payments: 'Payments'
        };

        var self = {};

        self.Visible = ko.observable();
        self.Visible.Is = function (str) {
            return ko.unwrap(self.Visible) === str;
        }

        self.Eob = {
            IsExpanded: ko.observable(),
            IsVisible: ko.computed(function() {
                return self.Visible.Is(menuItemEnum.Eob);
            }),
            Show: function() {
                self.Visible(menuItemEnum.Eob);
            }
        };
        self.Eob.ToggleExpanded = function () {
            self.Eob.IsExpanded(!ko.unwrap(self.Eob.IsExpanded));
            var action = ko.unwrap(self.Eob.IsExpanded) ? 'Show' : 'Hide';
            window._paq.push(['trackEvent', 'Visit', 'Click', 'Eob - ' + action]);
        };
        
        self.TransactionsFilter = {
            TransactionType: ko.observable()
        };
        
        self.Adjustments = {
            IsVisible: ko.computed(function() {
                return self.Visible.Is(menuItemEnum.Adjustments);
            }),
            Show: function() {
                self.TransactionsFilter.TransactionType(transactionFilterEnum.OtherFilter);
                self.Visible(menuItemEnum.Adjustments);
            }
        };
        self.Charges = {
            IsVisible: ko.computed(function() {
                return self.Visible.Is(menuItemEnum.Charges);
            }),
            Show: function() {
                self.TransactionsFilter.TransactionType(transactionFilterEnum.ChargeFilter);
                self.Visible(menuItemEnum.Charges);
            }
        };
        self.FeesInterest = {
            IsVisible: ko.computed(function () {
                return self.Visible.Is(menuItemEnum.FeesInterest);
            }),
            Show: function () {
                self.TransactionsFilter.TransactionType(transactionFilterEnum.InterestFilter);
                self.Visible(menuItemEnum.FeesInterest);
            }
        };
        self.Insurance = {
            IsVisible: ko.computed(function() {
                return self.Visible.Is(menuItemEnum.Insurance);
            }),
            Show: function () {
                self.TransactionsFilter.TransactionType(transactionFilterEnum.InsuranceFilter);
                self.Visible(menuItemEnum.Insurance);
            }
        };
        self.Payments = {
            IsVisible: ko.computed(function () {
                return self.Visible.Is(menuItemEnum.Payments);
            }),
            Show: function () {
                self.TransactionsFilter.TransactionType(transactionFilterEnum.PaymentFilter);
                self.Visible(menuItemEnum.Payments);
            }
        };
        self.Transactions = {
            IsVisible: ko.computed(function () {
                return ko.unwrap(self.Adjustments.IsVisible) ||
                    ko.unwrap(self.Charges.IsVisible) ||
                    ko.unwrap(self.FeesInterest.IsVisible) ||
                    ko.unwrap(self.Insurance.IsVisible) ||
                    ko.unwrap(self.Payments.IsVisible);
            })
        };

        return self;

    };

    function getScrollDistance($scrollContainer) {
        return $scrollContainer[0].scrollWidth - $scrollContainer[0].offsetWidth;
    };

    var setup = function($element, visitId) {

        var $content = $element.find('.visit-details-content');
        var promise = $.Deferred();
        var model = new ViewModel();

        // setup grid
        model.Transactions.Grid = window.VisitPay.VpGrid(
            //grid filter
            function () {
                return $.extend({}, ko.mapping.toJS(model.TransactionsFilter), { visitId: visitId });
            },
            '/Account/VisitTransactions',
            10,
            'DisplayDate',
            'asc',
            //export filter
            function () {
                return { visitId: visitId };
            }
        );

        // bind
        ko.applyBindings(model, $element[0]);
        
        // setup scrolling
        if (getScrollDistance($content) > 0) {
            // don't select by default
            promise.resolve();
        } else {
            // set charges to active and search
            model.Charges.Show();
            model.Transactions.Grid.Search(false).done(function () {
                promise.resolve();
            });
        }
        
        // scrolling actions
        model.Visible.subscribe(function(newValue) {
            if (!newValue || newValue.length <= 0) {
                return;
            }
            var scrollDistance = getScrollDistance($content);
            if (scrollDistance <= 0) {
                return;
            }
            $content.animate({ scrollLeft: scrollDistance }, scrollDuration);
        });
        $element.off('click.back').on('click.back', '.line-items h4', function(e) {
            e.preventDefault();
            var scrollDistance = getScrollDistance($content);
            if (scrollDistance <= 0) {
                return;
            }
            model.Visible('');
            $content.animate({ scrollLeft: 0 }, scrollDuration);
        });

        // click events - eob
        $element.off('click.eobs').on('click.eobs', '#btnVisitDetailsEob', function (e) {
            e.preventDefault();
            model.Eob.Show();
            var $details = $content.find('.eobs');
            if ($details.attr('data-load') !== 'true') {
                return;
            }
            $details.attr('data-load', false);
            $.post('/Account/VisitDetailsEob', { visitId: visitId }, function(html) {
                $details.html(html);
                $details.find('.grid-row').eq(0).click(); // expand the first eob
                $details.find('[data-toggle="tooltip"]').tooltip();
            });
        });
        $element.off('gridrowclick.eob').on('gridrowclick.eob', '.grid-eob > .grid-row', function (e, obj) {
            var action = obj.isVisible ? 'Show' : 'Hide';
            window._paq.push(['trackEvent', 'Visit', 'Click', 'Eob - Summary - ' + action]);
        });
        
        // click events - support
        $element.off('click.support').on('click.support', '#aVisitTransactionsContactSupport', function(e) {
            e.preventDefault();
            if ($element.is('.modal')) {
                $element.off('hidden.bs.modal.local');
                $element.modal('hide');
            }
            $.blockUI();
            if (window.VisitPay && window.VisitPay.CreateSupportRequest) {
                window.VisitPay.CreateSupportRequest.Initialize($('#modalCreateSupportRequestContainer'), visitId);
            } else if (window.VisitPayMobile && window.VisitPayMobile.Common) {
                window.VisitPayMobile.Common.OpenCreateSupportRequest(visitId);
            }
        });

        // tooltips
        $('[data-toggle="tooltip-html"]').tooltipHtml(true, '480px');

        // piwik tracking
        $(document).off('click.insurer.piwik').on('click.insurer.piwik', '.eob-detail a', function () {
            window._paq.push(['trackEvent', 'Visit', 'Click', 'Eob - Insurer']);
        });
        $(document).off('click.exportdetails.piwik').on('click.export.piwik', '#btnExportTransactions', function() {
            window._paq.push(['trackEvent', 'Visit', 'Export']);
        });
        
        // when menu changes and it's a grid, this will fire to refresh the grid data
        model.TransactionsFilter.TransactionType.subscribe(function (newValue) {
            if (newValue) {
                model.Transactions.Grid.Search(false);
                window._paq.push(['trackEvent', 'Visit', 'Click', 'Visit Details > Click ' + newValue]);
            }
        });

        //
        return promise;

    };

    var init = function(visitId) {

        // loads in a modal
        $.blockUI();
        window._paq.push(['trackEvent', 'Visit', 'Click', 'View visit details - Modal']);

        var promise = $.Deferred();

        var url = '/Account/VisitDetails?visitId=' + visitId;

        window.VisitPay.Common.ModalNoContainerAsync('modalVisitDetails', url, 'GET', null, true).done(function ($modal) {

            $.unblockUI();

            $modal.on('shown.bs.modal', function() {
                setup($modal, visitId).done(function() {
                });
            });

            $modal.on('hidden.bs.modal.local', function() {
                promise.resolve();
            });

        });

        return promise;

    };

    var inline = function($element, visitId) {

        var promise = $.Deferred();
        var url = '/Account/VisitDetailsContent?visitId=' + visitId;

        $.get(url, function(html) {
            
            $element.html(html);
            setup($element.find('div.visit-details'), visitId).done(function() {});
            promise.resolve();

        });

        return promise;
    }

    return {
        init: init,
        inline: inline
    };

});
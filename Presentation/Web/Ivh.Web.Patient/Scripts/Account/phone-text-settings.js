﻿(function (visitPay, $) {

    var $scope = $('#section-phonetext');
    
    var cancelText = window.VisitPay.Localization.Cancel || 'Cancel';
    var continueText = window.VisitPay.Localization.Continue || 'Continue';
    var okText = window.VisitPay.Localization.Ok || 'OK';
    var noText = window.VisitPay.Localization.No || 'No';
    var successText = window.VisitPay.Localization.Success || 'Success';
    var yesText = window.VisitPay.Localization.Yes || 'Yes';

    var lastChangedTextMessageCheckbox = null;

    function bindTextTogglers() {
        //Turn off any listeners in case they are already on
        $scope.off('change', '.text-toggle input[type=checkbox]');

        var $ptToggle = $scope.find('.text-toggle');
        $ptToggle.find('input[type=checkbox]').bootstrapToggle({
            onstyle: 'vp-toggle',
            offstyle: 'vp-toggle',
            on: null,
            off: null,
            size: 'small'
        });

        $scope.on('change', '.text-toggle input[type=checkbox]', setTextMessaging);
    }

    function setTextMessaging(e) {
        var checkbox = $(e.target);

        //Save reference to the checkbox that just changed, used if action is cancelled (to flip state back)
        lastChangedTextMessageCheckbox = checkbox;

        var isOn = $(this).prop('checked');
        if (isOn) {
            //Check if we are editing primary or secondary
            var isPrimary = checkbox.data('primary');
            var modalTitle = checkbox.data('activationModalTitle');

            canActivate(isPrimary, modalTitle);
        } else {
            //User wants to turn off SMS
            disableSms();
        }
    }

    function bindPreferences() {
        var $preferences = $scope.find('.preferences');
        $preferences.find('.preference input[type=checkbox]').bootstrapToggle({ 
            onstyle: 'vp-toggle',
            offstyle: 'vp-toggle',
            on: null,
            off: null,
            size: 'small'
        });
        $preferences.find('.preference').css('visibility', 'visible');
    }

    function setPreference(e) {
        var postUrl = $(this).prop('checked') === true ? '/Account/AddSmsPreference' : '/Account/RemoveSmsPreference';
        $.post(postUrl, { id: $(this).data('id') }, function () { });
    }

    function reload() {

        var promise = $.Deferred();

        $.get('/Account/PhoneTextInformation', {}, function (html) {

            $scope.html(html);
            bindPreferences();
            bindTextTogglers();
            promise.resolve();

        });

        return promise;

    }

    function savePhone($form) {

        var promise = $.Deferred();
        $form.parseValidation();

        if (!$form.valid()) {
            promise.reject();
        }

        $.post('/Account/PhoneEdit', $form.serialize(), function(result) {

            if (result.Result) {
                promise.resolve(result.Message);
            } else {
                promise.reject(result.Message);
            }

        });

        return promise;

    }

    function editPhone(e) {

        e.preventDefault();
        $.blockUI();

        var title = $(this).attr('title') || 'Edit Phone Number';
        var isPrimary = $(this).data('primary');

        visitPay.Common.ModalNoContainerAsync('modalPhoneEdit', '/Account/PhoneEdit?phoneType=' + (isPrimary ? 'Primary' : 'Secondary'), 'GET', {}, true).done(function($modal) {
            
            $modal.find('.modal-title').text(title);

            var $form = $modal.find('form');
            $form.parseValidation();

            $form.find('#PhoneNumber').mask('(999) 999-9999');

            $form.on('submit', function(e) {

                e.preventDefault();
                $.blockUI();

                savePhone($form).done(function(message) {

                    $modal.modal('hide');

                    reload().done(function() {
                        $.unblockUI();
                        visitPay.Common.ModalGenericAlert(successText, message, okText);
                    });

                }).fail(function(message) {

                    $.unblockUI();
                    if (message && message.length > 0) {
                        visitPay.Common.AppendMessageToValidationSummary($form, message);
                    }

                });
            });

            $.unblockUI();

        });

    }

    function removePhone(e) {

        var title = $(this).attr('title') || 'Delete secondary phone number';
        $.post('/Account/RemovePhoneMessage', { smsEnabled: $(this).data('smsenabledfor') }, function(linesOfText) {

            var html = '';
            for (var i = 0; i < linesOfText.length; i++) {
                html += '<p>' + linesOfText[i] + '</p>';
            }

            visitPay.Common.ModalGenericConfirmationMd(title, html, yesText, noText).done(function() {
                $.blockUI();
                $.post('/Account/SecondaryPhoneDelete', {}, function() {
                    reload();
                    $.unblockUI();
                });
            });
        });
    }

    function disableSms() {
        $.blockUI();
        $.post('/Account/GetDisableText', {}, function(cms) {
            $.unblockUI();
            visitPay.Common.ModalGenericConfirmationMd(cms.ContentTitle, cms.ContentBody, continueText, cancelText).done(function () {
                $.blockUI();
                $.post('/Account/DisableSms', {}, function () {
                    reload();
                    $.unblockUI();
                });
            }).fail(function () {
                //User decided not to disable SMS, so turn checkbox back on
                setCheckboxStateWithoutEvents(lastChangedTextMessageCheckbox, 'on');
            });
        });

    }

    function setCheckboxStateWithoutEvents($checkbox, state) {
        //Temporarily disable change listeners
        $scope.off('change', '.text-toggle input[type=checkbox]');

        //Turn checkbox back on since user cancelled disable
        $checkbox.bootstrapToggle(state);

        //Turn listeners back on
        $scope.on('change', '.text-toggle input[type=checkbox]', setTextMessaging);
    }

    function canActivate(isPrimary, modalTitle) {
        $.blockUI();
        $.post('/Account/CanActivate', { smsPhoneType: (isPrimary ? 'Primary' : 'Secondary') }, function (response) {
            if (response.CanActivate) {
                var activateUrl = '/Account/ActivateTextMessaging?smsPhoneType=' + (isPrimary ? 'Primary' : 'Secondary');
                showTextActivationModal(activateUrl, modalTitle);
            } else {
                // add slight delay to allow animation to complete 
                setTimeout(function () {
                    $.unblockUI();
                    visitPay.Common.ModalGenericConfirmationMd(response.cms.ContentTitle, response.cms.ContentBody, okText, cancelText)
                        .done(function () {
                            canActivate(isPrimary);
                        });
                }, 500);
                
            }
        });
    }

    function showTextActivationModal(url, modalTitle) {
        visitPay.Common.ModalNoContainerAsync('modalActivateText', url, 'GET', {}, true, activateModalCancelled).done(function ($modal) {
            window.VisitPay.InitActivateTextMessaging($modal);
            $modal.find('.modal-title').text(modalTitle);
            $.unblockUI();
        });
    }

    function activateModalCancelled($modal) {
        //Activation modal cancelled, so turn checkbox back off
        setCheckboxStateWithoutEvents(lastChangedTextMessageCheckbox, 'off');
    }

    $(document).ready(function () {
        bindPreferences();
        bindTextTogglers();

        $scope.on('change', '.preferences input[type=checkbox]', setPreference);
        $scope.on('click', '#btnEditPrimary', editPhone);
        $scope.on('click', '#btnEditSecondary', editPhone);
        $scope.on('click', '#btnDeleteSecondary', removePhone);

    });

})(window.VisitPay, jQuery);

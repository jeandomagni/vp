﻿(function() {

    $(document).ready(function() {

        var $scope = $('#section-resetpassword');

        $scope.find('#Ssn4').numbersOnly(4);
        $scope.find('#btnSwitchPassword').switchPassword();
        $scope.find('#frmResetPassword').commonForm($scope).done(function(url) {
            window.location.href = url;
        });

    });

})();
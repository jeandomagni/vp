﻿window.VisitPay.PaymentSummary = (function () {

    var ViewModel = function () {

        var self = this;

        self.Filter = {
            PaymentPeriod: ko.observable(),
            PostDateBegin: ko.observable(),
            PostDateEnd: ko.observable()
        };

        return self;
    };

    function setDates(model, period) {
        var dateFormat = 'MM/DD/YYYY';
        var begin = '';
        var end = '';

        if (period === 'YTD') {
            begin = moment().dayOfYear(1).format(dateFormat);
            end = moment().format(dateFormat);
        } else if (period === 'PY') {
            begin = moment().dayOfYear(1).add(-1, 'y').format(dateFormat);
            end = moment().dayOfYear(1).add(-1, 'd').format(dateFormat);
        }

        model.Filter.PostDateBegin(begin);
        model.Filter.PostDateEnd(end);

        var opts = {
            dateFormat: dateFormat,
            todayHighlight: true,
            autoclose: true,
            forceParse: false,
            immediateUpdates: true
        };
        
        $('#frmPaymentSummaryFilter #PostDateBegin').parent('.input-group.date').datepicker('remove').datepicker(opts);
        $('#frmPaymentSummaryFilter #PostDateEnd').parent('.input-group.date').datepicker('remove').datepicker(opts);
        
    }

    var init = function () {

        var model = new ViewModel();
        model.Filter.PaymentPeriod.subscribe(function(newValue) {
            setDates(model, newValue);
        });

        //Setup filter form bindings
        ko.applyBindings(model, $('#frmPaymentSummaryFilter')[0]);
        setDates(model, model.Filter.PaymentPeriod());

        //Setup VP Grid
        var vm = {};
        window.VisitPay.VpGridCommon.WithLazyGrid(vm);
        var url = '/Account/PaymentSummary';
        vm.Grid = window.VisitPay.VpGrid(model.Filter, url, 5, null, null);
        ko.applyBindings(vm, $('#gridPaymentSummary')[0]);


        //Initial load of grid
        vm.Grid.Search(true);
        
        //Filter form submit
        $(document).on('submit', '#section-paymentsummary #frmPaymentSummaryFilter', function (e) {
            e.preventDefault();
            if ($(this).valid()) {
                vm.Grid.Search(true);
            }
        });

        //Filter reset button
        $(document).on('click', '#section-paymentsummary button[data-reset=true]', function (e) {
            e.preventDefault();
            window.VisitPay.Common.ResetUnobtrusiveValidation($(this).parents('form').eq(0));
            model.Filter.PaymentPeriod('YTD');
            setDates(model, model.Filter.PaymentPeriod());
            vm.Grid.Search(true);
        });

        //Export excel
        $(document).on('click', '#section-paymentsummary #btnPaymentSummaryExport', function (e) {
            e.preventDefault();
            vm.Grid.Export('/Account/PaymentSummaryExport');
        });

        //Export PDF
        $(document).on('click', '#section-paymentsummary #btnPaymentSummaryExportPdf', function (e) {
            e.preventDefault();
            vm.Grid.Export('/Account/PaymentSummaryExportPdf');
        });

    };

    return {
        init: init
    };

});

(function() {
    $(document).ready(function() {
        var summary = new window.VisitPay.PaymentSummary();
        summary.init();
    });
})();
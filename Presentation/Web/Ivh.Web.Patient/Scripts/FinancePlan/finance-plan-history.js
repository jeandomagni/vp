﻿(function() {
    $(document).ready(function () {
        var vm = {};
        window.VisitPay.VpGridCommon.WithLazyGrid(vm);
        vm.Grid = window.VisitPay.VpGrid(function () { return {}; }, '/FinancePlan/MyFinancePlans', 100, 'OriginationDate', 'desc');
        ko.applyBindings(vm, $('#gridFinancePlans').parent()[0]);
        
        // grid click
        $(document).on('gridrowclick.common', '.grid-finance-plan-history > .grid-row', function (e, obj) {
            if (!obj.isVisible || $.trim(obj.target.html()).length > 0) {
                return;
            }

            var data = ko.mapping.toJS(ko.dataFor(obj.sender[0]));
            $.post('/FinancePlan/FinancePlanDetail', { financePlanId: data.FinancePlanId, includeVisits: true }, function (html) {
                obj.target.html(html);
                obj.target.find('a[data-visitid]').off('click').on('click', function (e) {
                    e.preventDefault();
                    var visitId = $(this).attr('data-visitid');
                    var details = window.VisitPay.VisitDetails();
                    details.init(visitId);
                });
            });
        });
        
        // export piwik
        $(document).on('click.exportfinanceplans.piwik', '#btnFinancePlanHistoryExport', function() { window._paq.push(['trackEvent', 'Finance Plans', 'Export']); });
        
        // load
        var isExpandedOnLoad = false;
        vm.IsExpanded.subscribe(function (newValue) {
            if (ko.unwrap(newValue) === true) {
                if (!ko.unwrap(vm.Load.ShouldLoad)) {
                    return;
                }
                var promise = vm.Grid.Search(!isExpandedOnLoad);
                vm.Load.Start(promise);
                promise.done(function() {});
            }
            
            var action = ko.unwrap(newValue) ? 'Show' : 'Hide';
            window._paq.push(['trackEvent', 'Finance Plans', 'Click', action]);
        });
        vm.IsExpanded(isExpandedOnLoad);
    });
})();
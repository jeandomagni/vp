﻿window.VisitPay.ReconfigureFinancePlanViewModel = (function(data) {
    
    var self = new window.VisitPay.FinancePlanSetupBaseViewModel(data);
    self.FinancePlanIdToReconfigure = ko.observable(data.FinancePlanIdToReconfigure);
    self.IsError = ko.observable(false);

    // post models
    self.AcceptModel = function() {
        return {
            FinancePlanIdToReconfigure: ko.unwrap(self.FinancePlanIdToReconfigure),
            TermsCmsVersionId: ko.unwrap(self.TermsCmsVersionId)
        };
    };
    self.CancelModel = function() {
        return {
            FinancePlanIdToReconfigure: ko.unwrap(self.FinancePlanIdToReconfigure)
        };
    };
    self.SubmitModel = function() {
        return $.extend({}, self.SubmitModelBase(), {
            FinancePlanIdToReconfigure: ko.unwrap(self.FinancePlanIdToReconfigure)
        });
    };
    self.ValidateModel = function() {
        return $.extend({}, self.ValidateModelBase(), {
            FinancePlanIdToReconfigure: ko.unwrap(self.FinancePlanIdToReconfigure)
        });
    };

    return self;

});

window.VisitPay.ReconfigureFinancePlan = (function() {

    var $scope = $('#reconfigure-finance-plan');
    var model;
    var urls = {};

    function onFail(result) {
        model.IsError(result.IsError);
        $.unblockUI();
    };

    function onCancelReconfiguration(e) {
        e.preventDefault();
        $.blockUI();
        $.post(urls.ReconfigureCancelUrl, model.CancelModel(), function(result) {
            if (result.IsError === true) {
                onFail(result);
            } else {
                window.location.href = result.RedirectUrl;
            }
        });
    };

    function onSubmitReconfiguration(e) {
        e.preventDefault();
        $.blockUI();
        
        var promiseAch = $.Deferred();
        var achAuthorize = new window.VisitPay.AchAuthorization().promptReconfig(model.PrimaryPaymentMethod, ko.unwrap(model.FinancePlanIdToReconfigure), ko.unwrap(model.Terms().FinancePlanOfferId));
        achAuthorize.then(function() {
            $.blockUI();
            promiseAch.resolve();
        }).fail(function() {
            // don't allow continue - they didn't accept the ach authorization
        });

        promiseAch.done(function() {
            var promiseCreate = $.Deferred();
            if (ko.unwrap(model.IsEditable)) {
                // need to create first
                $.post(urls.ReconfigureSubmitUrl, model.SubmitModel(), function(result) {
                    if (result.IsError === true) {
                        onFail(result);
                    } else {
                        promiseCreate.resolve();
                    }
                });
            } else {
                // it's already a pending config, only accept
                promiseCreate.resolve();
            }

            promiseCreate.done(function() {
                // accept
                $.post(urls.ReconfigureAcceptUrl, model.AcceptModel(), function(result) {
                    if (result.IsError === true) {
                        onFail(result);
                    } else {
                        if (window.VisitPayMobile === undefined) {
                            window.location.href = result.RedirectUrl;
                        } else {
                            window.location.href = '/mobile' + result.RedirectUrl;
                        }
                    }
                });
            });
        });
    };

    return {
        Initialize: function(data) {

            urls = {
                ReconfigureAcceptUrl: data.ReconfigureAcceptUrl,
                ReconfigureCancelUrl: data.ReconfigureCancelUrl,
                ReconfigureSubmitUrl: data.ReconfigureSubmitUrl,
                ReconfigureValidateUrl: data.ReconfigureValidateUrl
            };

            model = new window.VisitPay.ReconfigureFinancePlanViewModel(data);
            model.PrimaryPaymentMethod = data.PrimaryPaymentMethod;
            ko.applyBindings(model, $scope[0]);

            // common
            window.VisitPay.FinancePlanSetupCommon.BindEvents($scope, model, urls.ReconfigureValidateUrl);

            // events
            $scope.on('click', '#btnReconfigureCancel', onCancelReconfiguration);
            $scope.on('click', '#btnReconfigureSubmit', onSubmitReconfiguration);

            // show the UI
            $scope.css('visibility', 'visible');
        }
    }

})();
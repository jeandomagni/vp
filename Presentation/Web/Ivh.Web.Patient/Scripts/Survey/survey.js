﻿window.VisitPay.Survey = (function(common, $) {

    var validate = true;

    
    function shouldShowSurvey(surveyData) {

        var promise = $.Deferred();

        var surveyName = { "surveyName": surveyData.SurveyName };
        $.post('/Survey/ShouldShowSurvey', surveyName, function(result) {
            if (result.UserIsPresentedWithSurvey) {
                //surveyData.SurveyName can change depending on what the controller decides.
                surveyData.SurveyName = result.SurveyName;
                promise.resolve(surveyData);
            }
            else {
                promise.reject();
            }
        });

        return promise;
    }

    function loadModal(surveyData) {

        var promise = $.Deferred();

        window.VisitPay.Common.ModalNoContainerAsync('surveyModal', '/Survey/ShowSurvey', 'GET', surveyData, false).done(function($modal) {
            promise.resolve($modal);
        });

        return promise;
    }

    function loadNps(surveyData) {

        var promise = $.Deferred();

        $.ajax({
            method: 'GET',
            url: '/Survey/ShowNpsSurvey',
            data: surveyData
        }).done(function(html) {

            if (html != null && html.length > 0) {
                $('#nps-survey').append(html);

                var $modal = $('#nps-survey').find('#surveyModal');
                promise.resolve($modal);
            } else {
                promise.reject();
            }

        });

        return promise;
    }

    function applyNpsSurvey() {
        
        $(document).on('click.dismiss-toaster', 'div .dismiss-toaster.nps-survey', function (e) {
            $('#nps-survey').hide();
        });
       
    };
    
    function trackPiwikSurveyPresented(surveyName) {
        window._paq.push(['trackEvent', 'Survey', 'Click', surveyName + ' presented']);
    }

    function applyCommonSurveyTasks($form) {

        var $ingore = $form.find('#btnIgnore');
        $('.number-box-rating-container').on('click', '.btn', function () {
            $(this).closest('.number-box-rating-container').find('.btn').removeClass('nps-selected');
            $(this).addClass('nps-selected');
        });
        $ingore.on('click', function(e) {
            $form.attr('action', '/Survey/ProcessIgnoredSurvey');
            validate = false;
            $form.submit();
        });

        $form.find('.js-survey-radio').change(function() {
            var index = parseInt(this.value) - 1;
            var surveyRatings = $(this).closest('.rate').data('survey-ratings-text');
            var ratingText = surveyRatings[index];
            $(this).closest('.form-group').find('.survey-rating-desc > span').text(ratingText);
            $(this).closest('.form-group').find('.js-survey-rating-text').val(ratingText);
        });

        $form.find('.js-survey-radio-label').hover(function() {
                var container = $(this).parents('.form-group');
                var ratingDescription = $(container).find('.survey-rating-desc > span');
                var hoverRatingDescription = $(container).find('.survey-rating-desc-hover > span');

                var index = parseInt($('#' + $(this).attr('for')).val() - 1);
                var surveyRatings = $(this).closest('.rate').data('survey-ratings-text');
                var ratingText = surveyRatings[index];

                $(hoverRatingDescription).text(ratingText);
                $(hoverRatingDescription).show();
                $(ratingDescription).hide();
            },
            function() {
                $(this).parents('.form-group').find('.survey-rating-desc > span').show();
                $(this).parents('.form-group').find('.survey-rating-desc-hover > span').hide();
            }
        );

        $form.find('input[type=hidden]').each(function () {
            $(this).val(he.decode($(this).val()));
        });
    };

    function isSurveyFormValid($form) {

        // if the user hits the ignore button don't validate 
        if (validate === false) {
             return true;
        }
           
        // shouldValidateRatings is provided in the _Survey.cshtml 
        if (shouldValidateRatings === true) {
            var checked = $form.find('input[type="radio"]:checked');
            if (checked.length === 0) {
                if ($form.find('#SurveyComment').val().length === 0) {
                    $form.find('.survey-validation').show();
                    return false;
                }
            }
        } else {
            if ($form.find('#SurveyComment').val().length === 0) {
                $form.find('.survey-validation').show();
                return false;
            }
        }
        return true;
    }

    return {
        //{
        //    'SurveyName': 'FinancePlanCreation',
        //    'FinancePlanId': financePlanId,
        //     'PaymentId': paymentId
        //}
        ShowSurvey: function(surveyData, isNps) {
            var promise = $.Deferred();

            shouldShowSurvey(surveyData).then(function (surveyData) {

                if (isNps === true) {
                    loadNps(surveyData).then(function($modal) {
                        var $form = $modal.find('form');
                        applyCommonSurveyTasks($form);
                        applyNpsSurvey();

                        $form.on('submit', function(e) {
                            e.preventDefault();

                            var action = $form.attr('action');
                            $.post(action, $form.serialize(), function (result) { });

                            $form.find(".thank-you").fadeIn(650, function() {
                                setTimeout(function () {
                                    $('#nps-survey').fadeOut(650);
                                }, 1500);
                            });
                            promise.resolve(true);
                        });

                        $('#nps-survey').show().animate({ bottom: 0 }, 650, function () { });
                        trackPiwikSurveyPresented(surveyData.SurveyName);
                    });
                } else {
                    loadModal(surveyData).then(function($modal) {
                        
                        var $form = $modal.find('form');
                        applyCommonSurveyTasks($form);

                        $form.on('submit', function(e) {
                            e.preventDefault();

                            if (isSurveyFormValid($form) === false) {
                                promise.resolve();
                                return;
                            }

                            var action = $form.attr('action');
                            $.post(action, $form.serialize(), function(result) {
                                promise.resolve();
                            });
                            $modal.modal('hide');
                        });

                        $modal.modal('show');
                        $(document).on('click.dismiss-toaster', '#surveyModal button.close', function (e) {
                            $modal.modal('hide');
                            $form.attr('action', '/Survey/ProcessIgnoredSurvey');
                            validate = false;
                            $form.submit();
                            promise.resolve();
                        });
                        trackPiwikSurveyPresented(surveyData.SurveyName);
                    });
                }
            }).fail(function() {
                promise.resolve();
            });

            return promise;
        }
    };
})(window.VisitPay.Common = window.VisitPay.Common || {}, jQuery);
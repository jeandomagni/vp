﻿window.VisitPay.OfflineLoginViewModel = (function () {

    var self = this;

    self.IsGuarantorSelfVerified = ko.observable(false);

    self.CanContinue = ko.computed(function () {
        return self.IsGuarantorSelfVerified();
    });

    return self;
});

window.VisitPay.OfflineLogin = (function () {

    var $scope = $('#section-offline-login');

    return {
        Initialize: function () {
            var vm = new window.VisitPay.OfflineLoginViewModel();
            ko.applyBindings(vm, $scope[0]);
        }
    };
})();
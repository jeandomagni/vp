﻿window.VisitPay.OfflinePaymentMethods = (function () {

    function init(offlineAccountSettingsViewModel) {
        var $scope = $('#section-paymentmethods');
        var viewModel = {
            PaymentMethods: new window.VisitPay.paymentMethodsViewModel(),
            IsSetPrimaryEnabled: ko.observable(offlineAccountSettingsViewModel.GuarantorUseAutoPay)
        };

        ko.applyBindings(viewModel, $scope[0]);
        window.VisitPay.PaymentMethods(viewModel, $scope, {
            urlPaymentMethodBank: offlineAccountSettingsViewModel.UrlPaymentMethodBank,
            urlPaymentMethodCard: offlineAccountSettingsViewModel.UrlPaymentMethodCard,
            urlPaymentMethodsList: offlineAccountSettingsViewModel.UrlPaymentMethodsList,
            urlRemovePaymentMethod: offlineAccountSettingsViewModel.UrlRemovePaymentMethod,
            urlSavePaymentMethodBank: offlineAccountSettingsViewModel.UrlSavePaymentMethodBank,
            urlSavePaymentMethodCard: offlineAccountSettingsViewModel.UrlSavePaymentMethodCard, 
            urlSetPrimary: offlineAccountSettingsViewModel.UrlSetPrimary
        });
    }

    return {
        init: init
    };

})();
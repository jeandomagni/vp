﻿window.VisitPay.OfflineMakePayment = (function() {

    function paymentComplete(receiptText) {

        $.unblockUI();

        var $modal = window.VisitPay.Common.ModalGeneric('Payment Successful', receiptText, 'model-md');
        $modal.attr('id', 'modalOfflinePaymentReceipt');
        $modal.find('.modal-footer').show();
        $modal.on('click', 'button[type=submit]', function() {
            $modal.modal('hide');
        });
        $modal.modal('show');
        $modal.on('hidden.bs.modal', function() {
            window.location.reload();
        });

        // don't want further callbacks to fire
        return $.Deferred().reject();

    };

    function init(offlineIndexViewModel) {

        var paymentMethodsModule = new window.VisitPay.ArrangePayment.PaymentMethodsModule(offlineIndexViewModel.VisitPayUserId);
        paymentMethodsModule.setup(function(vm) {
            vm.ShowModalAfterInitialPaymentMethodAdded(true);
        });
        paymentMethodsModule.init().done(function() {

            var $scope = $('#offline-make-payment');

            // disable primary
            paymentMethodsModule.setPrimaryEnabled(false);

            // payment module
            var paymentModule = new window.VisitPay.ArrangePayment.PaymentModule(paymentMethodsModule, {
                submitUrl: offlineIndexViewModel.PaymentSubmitUrl,
                validateUrl: offlineIndexViewModel.PaymentValidateUrl,
                onPaymentComplete: paymentComplete,
                additionalParameters: {}
            });
            paymentModule.messenger.subscribe(paymentMethodsModule.changePaymentMethod, null, 'changePaymentMethod');
            paymentModule.init(offlineIndexViewModel.FinancePlanPaymentOption);

            // callback after the module has initialized
            paymentModule.setup(function(vm) {

                // make date readonly
                vm.ActivePaymentOption().IsDateReadonly(true);

                // hide, but enable, payment bucket zero, if eligible
                if (ko.unwrap(vm.ActivePaymentOption().ShowPayBucketZero) === true) {
                    $scope.find('.pay-bucket-zero').hide();
                    vm.ActivePaymentOption().PayBucketZero(true);
                }

                // default each finance plan to it's AmountDue
                ko.utils.arrayForEach(ko.unwrap(vm.ActivePaymentOption().FinancePlans), function(fp) {
                    fp.PaymentAmount(parseFloat(ko.unwrap(fp.AmountDueDecimal)).toFixed(2));
                    vm.ActivePaymentOption().FinancePlans.Change(fp);
                });

            });

            // tooltips
            $('[data-toggle="tooltip"]').tooltip({ animation: false, container: 'body' });
            $('[data-toggle="tooltip-html"]').tooltipHtml();

            // show it
            $scope.css('visibility', 'visible');
        });

    };

    return {
        init: init
    };

})();
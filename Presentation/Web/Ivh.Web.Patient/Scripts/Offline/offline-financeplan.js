﻿window.VisitPay.OfflineFinancePlanViewModel = (function (data, primaryPaymentMethod) {

    var self = new window.VisitPay.FinancePlanSetupBaseViewModel(data);

    //
    self.IsCombined = ko.observable(data.IsCombined);
    self.FinancePlanId = ko.observable(data.FinancePlanId);
    self.StatementId = ko.observable(data.StatementId);
    self.EsignCmsVersionId = ko.observable(data.EsignCmsVersionId);
    self.EsignCmsVersionId.IsChecked = ko.observable(false);

    //
    self.PrimaryPaymentMethod = primaryPaymentMethod;
    self.PrimaryPaymentMethod.Any = ko.computed(function () {
        var primary = ko.unwrap(self.PrimaryPaymentMethod);
        return primary && ko.unwrap(primary.PaymentMethodId) > 0;
    });

    // post models
    self.SubmitModel = function () {
        return $.extend({}, self.SubmitModelBase(), {
            CombineFinancePlans: ko.unwrap(self.IsCombined),
            FinancePlanId: ko.unwrap(self.FinancePlanId),
            StatementId: ko.unwrap(self.StatementId),
            TermsCmsVersionId: ko.unwrap(self.TermsCmsVersionId)
        });
    };

    return self;

});

window.VisitPay.OfflineFinancePlan = (function () {

    var $scope = $('#offline-finance-plan');
    var paymentMethodsModule;
    var model;

    function replaceContent(html) {
        $('#offline-finance-plan-container').html(html);
    }

    function postFinancePlan() {
        var submitModel = { model: model.SubmitModel() };
        $.post(window.VisitPay.Urls.FinancePlanSubmitUrl, submitModel, function (result) {
            if (result.Result === true) {
                $.post('/Offline/EsignActAccept', { esignCmsVersionId: ko.unwrap(model.EsignCmsVersionId) });
                replaceContent(result.Message);
                window.VisitPay.OfflineCreateAccount.init();
            }
            $.unblockUI();
        });
    };

    function declineTerms() {

        var modalTitle = window.VisitPay.Localization.DeclineFinancePlanTerms;
        var modalContent = window.VisitPay.Localization.DeclineFinancePlanTermsWarning;
        var confirmText = window.VisitPay.Localization.Confirm;
        var cancelText = window.VisitPay.Localization.Cancel;
        window.VisitPay.Common.ModalGenericConfirmationMd(modalTitle, modalContent, confirmText, cancelText).done(function () {
            $.blockUI();
            $.post(window.VisitPay.Urls.FinancePlanCancelUrl, { financePlanId: ko.unwrap(model.FinancePlanId) }, function (result) {
                if (result === true) {
                    $.get(window.VisitPay.Urls.OfflineFinancePlanDeclinedUrl, function (html) {
                        replaceContent(html);
                        window.VisitPay.OfflineCreateAccount.init();
                        $.unblockUI();
                    });
                }
            });
        });

    };

    function submitFinancePlan(e) {

        e.preventDefault();

        if (ko.unwrap(model.RequirePaymentMethod)) {
            var primaryPaymentMethod = paymentMethodsModule.paymentMethods.PrimaryPaymentMethod();
            var financePlanOfferId = ko.unwrap(model.Terms().FinancePlanOfferId);

            var achAuthorize = new window.VisitPay.AchAuthorization().promptOffer(primaryPaymentMethod, financePlanOfferId);
            achAuthorize.then(function() {
                $.blockUI();
                postFinancePlan();
            }).fail(function() {
                // don't allow continue - they didn't accept the ach authorization
            });
        } else {
            $.blockUI();
            postFinancePlan();
        }

    };

    return {
        Initialize: function (data) {
            
            var paymentMethodsPromise = $.Deferred();

            if (data.GuarantorUseAutoPay) {
                paymentMethodsModule = new window.VisitPay.ArrangePayment.PaymentMethodsModule(data.VisitPayUserId);
                paymentMethodsPromise = paymentMethodsModule.init().then(function() {
                    paymentMethodsPromise.resolve(true);
                });
            }
            else
            {
                paymentMethodsPromise.resolve(false);
            }

            paymentMethodsPromise.then(function (requirePaymentMethod) {

                var primaryPaymentMethod = {};
                if (requirePaymentMethod) {
                    paymentMethodsModule.setPrimaryEnabled(true);
                    primaryPaymentMethod = paymentMethodsModule.paymentMethods.PrimaryPaymentMethod;
                }

                model = new window.VisitPay.OfflineFinancePlanViewModel(data, primaryPaymentMethod);
                model.ChangePrimary = function () {
                    if (requirePaymentMethod) {
                        paymentMethodsModule.changePrimary();
                    }
                };

                model.RequirePaymentMethod = ko.observable(requirePaymentMethod);
                model.PaymentMethodValid = ko.computed(function () {
                    var isRequired = ko.unwrap(model.RequirePaymentMethod);
                    if (!isRequired) {
                        return true;
                    }
                    return ko.unwrap(model.PrimaryPaymentMethod.Any());
                });

                ko.applyBindings(model, $scope[0]);

                // common
                window.VisitPay.FinancePlanSetupCommon.BindEvents($scope, model, window.VisitPay.Urls.FinancePlanValidateUrl);

                // events
                $scope.off('click.confirm').on('click.confirm', '#btnConfirmFinancePlan', submitFinancePlan);
                $scope.off('click.declineterms').on('click.declineterms', '#btnDeclineTerms', declineTerms);

                // show the UI
                $scope.css('visibility', 'visible');

            });
        }
    };

})();
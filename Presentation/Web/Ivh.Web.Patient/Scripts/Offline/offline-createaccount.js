﻿window.VisitPay.OfflineCreateAccount = (function () {

    var showHiddenFields = function () {
        //Show hidden fields, if needed
        if (!$('.hidden-create-account-item').is(':visible')) {
            $('.hidden-create-account-item').show('slow');
        }
    };

    return {
        init: function () {
            var $scope = $('#offline-create-account');
            var $form = $scope.find('#frmCreateAccount');

            $form.commonForm($scope).done(function (url) {
                window.location.href = url;
            });
            
            $('#createEmail').on('focus keydown keypress change', showHiddenFields);
            $('#createPassword').on('focus keydown keypress change', showHiddenFields);
            $('#btnCreateAccount').click(showHiddenFields);
        }
    };

})();
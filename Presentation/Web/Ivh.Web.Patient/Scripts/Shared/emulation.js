﻿(function () {
    if (window.VisitPay.State.IsEmulation) {
        if (window == window.top) {
            // NOT IN FRAME
            window.location.href = "/account/logoff";
        }
    }
})();
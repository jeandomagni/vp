﻿/*
 This JS handles switching locales from any of the 3 menus that allow for switching locales.
 1 menu for desktop, 2 menus for mobile
 */
(function () {
    $(document).ready(function () {
        $('.localization-option').click(function () {
            //Update the locale in the dropdown
            var localeDisplay = $(this).data('localeDisplay');
            $('#selected-locale').text(localeDisplay);

            //Get selected locale and make call to update it
            var selectedLocale = $(this).data('locale');
            VisitPay.ChangeLocale(selectedLocale);
        });

        $('#mobile-localization-menu-open').on("click", function (e) {
            $(this).next('ul').toggle();
            e.stopPropagation();
            e.preventDefault();
        });
    });
})();
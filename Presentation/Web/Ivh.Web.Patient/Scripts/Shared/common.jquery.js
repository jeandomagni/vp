﻿$.fn.NumericOnly = function () {
    return this.each(function () {
        $(this).keypress(function (e) {
            e = e || window.event;
            if (e.keyCode !== 8 && e.keyCode !== 9 && e.keyCode !== 13 && !e.ctrlKey && !e.metaKey && !e.altKey) {
                var charCode = (typeof e.which == "undefined") ? e.keyCode : e.which;
                if (charCode && !/\d/.test(String.fromCharCode(charCode))) {
                    return false;
                }
            }
            return true;
        });
    });
};

String.prototype.cleanUrl = function () {
    var pathArray = this.split('/');
    if (pathArray == undefined || pathArray.length < 3) {
        //http://blah/ should have at least 3
        return this;
    }
    var protocol = pathArray[0].toString();
    if (protocol.toLowerCase().indexOf("http") < 0) {
        //Seems like this isnt url...
        return this;
    }
    var host = pathArray[2];
    return host;
};

(function ($) {
    
    $.fn.extend({
        allCheckedIfNoneChecked: function(e) {
            if ($(this).not(':checked').length === $(this).length) {
                $(this).prop('checked', true);
                if (e) {
                    e.preventDefault();
                }
            }
        },
        makeDatepicker: function(endDate, opts) {

            var dateFormat = 'MM/DD/YYYY';

            opts = $.extend({}, {
                dateFormat: dateFormat,
                startDate: moment().format(dateFormat),
                todayHighlight: true,
                autoclose: true,
                forceParse: false,
                immediateUpdates: true
            }, opts);

            if (endDate) {
                $.extend(opts, {
                    endDate: moment(endDate).format(dateFormat)
                });
            }

            var $elements = this;
            $elements.each(function() {
                var $el = $(this);
                if ($el.parent().is('.date')) {
                    $el.parent().datepicker('remove').datepicker(opts);
                } else {
                    $el.datepicker('remove').datepicker(opts);
                }
            });

        },
        createDefaultDatepicker: function(opts) {
            var $form = this;
            $form.find('.input-group.date').each(function() {
                var element = $(this);

                opts = $.extend({}, {
                    clearBtn: true,
                    format: 'mm/dd/yyyy',
                    orientation: 'bottom left',
                    autoclose: true
                }, opts || {});

                element.datepicker(
                    opts
                ).on('hide', function() {
                    if ($form.is('form') && $form.valid()) {
                        $form.parseValidation();
                    }
                }).on('clearDate', function() {
                    element.find('input').trigger('change');
                });
            });
        },
        makeNumericInput: function(opts) {

            opts = $.extend({ allowDecimal: 2, allowNegative: false, addClass: true }, opts || {});
            
            var numberCodes = [48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 96, 97, 98, 99, 100, 101, 102, 103, 104, 105]; // top row numbers, numpad
            var controlCodes = [8, 9, 10, 13, 35, 36, 37, 39, 45, 46]; // backspace, shift, tab, enter, end, home, arrow left, arrow right, insert, delete
            if (opts.allowDecimal && opts.allowDecimal !== false) {
                numberCodes = numberCodes.concat([190, 110]); // period
            }
            if (opts.allowNegative) {
                numberCodes = numberCodes.concat([173, 109]); // -
            }

            var keyCodes = [].concat(numberCodes, controlCodes);

            var $elements = this;
            $elements.each(function() {
                var $el = $(this);
                $el.on('keydown', function(e) {
                    var charCode = (e.which) ? e.which : e.keyCode;
                    var isSelected = false;
                    var cursorIndex = 0;
                    if (window.getSelection) {
                        try {
                            var obj = $(this)[0];
                            isSelected = obj.selectionStart !== obj.selectionEnd;
                            cursorIndex = obj.selectionEnd;
                        } catch (e) {
                            // ignore
                        }
                    }
                    if (isSelected) {
                        return keyCodes.indexOf(charCode) > -1;
                    }
                    if (opts.allowDecimal && $.isNumeric(opts.allowDecimal)) {
                        var index = $(this).val().indexOf('.');
                        if (index > -1) {
                            if (cursorIndex <= index) {
                                return keyCodes.indexOf(charCode) > -1;
                            }
                            var s = $(this).val().split('.')[1];
                            if (s.length >= opts.allowDecimal) {
                                return controlCodes.indexOf(charCode) > -1;
                            }
                        }
                    }
                    return keyCodes.indexOf(charCode) > -1;
                });
                if (opts.addClass) {
                    $el.addClass('numeric');
                }
                $el.focus(function() {
                    $(this).select();
                });
            });
        },
        parseValidation: function() {

            var $form = this;

            if ($form.length > 0) {
                $form.removeData('validator').removeData('unobtrusiveValidation');
                $.validator.unobtrusive.parse($form);
                $form.validate().resetForm();
                $form.find('[data-valmsg-summary=true]').removeClass('validation-summary-errors').addClass('validation-summary-valid').find('ul').empty();
                $form.find('[data-valmsg-summary-custom=true]').removeClass('validation-summary-errors').addClass('validation-summary-valid').find('ul').empty();
                $form.find('.field-validation-error').removeClass('field-validation-error').addClass('field-validation-valid');
                $form.find('.input-validation-error').removeClass('input-validation-error').addClass('valid');
            }
        },

        deserializeArray: function(serializedArray) {
            if (serializedArray !== undefined && serializedArray !== null && $.isArray(serializedArray)) {

                // clear
                $.each($(this).find('input:text,select,textarea'), function() {
                    $(this).val('');
                });

                $.each($(this).find('input:checkbox,input:radio'), function() {
                    $(this).prop('checked', false);
                });

                // set
                for (var i = 0; i < serializedArray.length; i++) {

                    var $element = $(this).find('[name="' + serializedArray[i].name + '"]');
                    var value = serializedArray[i].value;

                    if ($element.is('input:text,select,textarea')) {
                        $element.val(value);
                    } else if ($element.is('input:checkbox,input:radio')) {
                        // won't be in array if unchecked
                        $.each($element, function() {
                            if ($(this).val().toString() === value.toString()) {
                                $(this).prop('checked', true);
                            }
                        });
                    }
                }
            }
        },
        serializeToObject: function(removePrefixes) {
            var data = {};
            var serializedArray = removePrefixes ? $(this).serializeArrayWithoutPrefixes() : $(this).serializeArray();
            serializedArray.map(function(item) {
                var occurences = 0;
                for (var i = 0; i < serializedArray.length; i++) {
                    if (serializedArray[i].name === item.name) {
                        occurences++;
                    }
                }
                if (occurences > 1) {
                    data[item.name] = data[item.name] || [];
                    data[item.name].push(item.value);
                } else {
                    data[item.name] = item.value;
                }
            });
            return data;
        },
        serializeArrayWithoutPrefixes: function() {
            var serialized = $(this).serializeArray();
            var mapped = serialized.map(function(obj) {
                obj.name = obj.name.indexOf('.') > -1 ? obj.name.split('.')[1] : obj.name;
                return obj;
            });
            return mapped;
        },

        numbersOnly: function(maxlength) {
            return this.each(function() {
                if (maxlength !== undefined && maxlength !== null && !isNaN(maxlength)) {
                    $(this).prop('maxlength', maxlength);
                }
                $(this).on('input', function(e) {
                    if (/\D/g.test($(this).val())) {
                        $(this).val($(this).val().replace(/\D/g, ''));
                    }
                });
            });
        },
        zipCode: function() {
            return this.each(function() {

                // if iOS device, switch type to "text"
                // iOS type=tel doesn't have a '-'
                if (/iP/i.test(navigator.userAgent)) {
                    $(this).prop('type', 'text');
                }

                $(this).prop('maxlength', 10);
                $(this).on('input', function(e) {
                    if (/[^0-9\-]/g.test($(this).val())) {
                        $(this).val($(this).val().replace(/[^0-9\-]/g, ''));
                    }
                });
            });
        },
        switchPassword: function () {

            this.each(function() {
                var $el = $(this);
                $el.off('click.switchpassword').on('click.switchpassword', function() {

                    var $password = $el.siblings('input');

                    var type = $password.prop('type').toString().toUpperCase() === 'PASSWORD' ? 'text' : 'password';
                    $password.prop('type', type);

                    var showText = $el.attr('data-switch-show');
                    var hideText = $el.attr('data-switch-hide');
                    var text = type === 'password' ? showText : hideText;
                    $el.text(text);
                    $el.attr('title', text);

                    $password.focus();

                });
            });

        },
        tooltipHtml: function(allowHtml, maxWidth) {
            this.each(function () {
                var $sibling = $(this).siblings('[data-tooltip-html]');
                var text = (allowHtml || false) ? $sibling.html() : $sibling.text();
                var html = $('<div/>').append($('<div/>').css('max-width', maxWidth || '280px').html(text)).html();
                $(this).tooltip('destroy').tooltip({
                    animation: false,
                    container: 'body',
                    html: true,
                    title: html
                });
            });
        },

        addCustomErrors: function(errorMessageJson) {

            if (!this.is('form')) {
                return;
            }

            if (errorMessageJson === undefined || errorMessageJson === null) {
                return;
            }

            var errorMessages = [];
            var fieldErrors = {};

            var keys = Object.keys(errorMessageJson);
            if (keys.length === 0) {
                return;
            }

            for (var i = 0; i < keys.length; i++) {
                var key = keys[i];
                var value = errorMessageJson[key];
                if (!Array.isArray(value)) {
                    continue;
                }

                errorMessages = errorMessages.concat(value);

                if (key.length > 0 && key.trim().length > 0) {
                    if (this.find('#' + key).length > 0) {
                        fieldErrors[key] = value;
                    }
                }
            }

            if (Object.keys(fieldErrors).length > 0) {
                this.validate().showErrors(fieldErrors);
            }

            if (errorMessages.length > 0) {
                var $customSummary = this.find('[data-valmsg-summary-custom="true"]');
                if ($customSummary.length === 0) {
                    return;
                }

                $customSummary.removeClass('validation-summary-valid').addClass('validation-summary-errors');

                var $list = $customSummary.find('ul');
                $list.html('');

                for (var x = 0; x < errorMessages.length; x++) {
                    var errorMessage = errorMessages[x];
                    if (errorMessage === undefined || errorMessage === null || errorMessage.length === 0) {
                        continue;
                    }
                    var $item = $('<li/>');
                    $item.html(errorMessages[x]);
                    $list.append($item);
                }
            }

        },

        hasScrollBar: function () {
            return this.get(0).scrollHeight > this.height();
        },

        commonForm: function($parent) {

            var promise = $.Deferred();
            var $form = this;

            $form.off('submit').on('submit', function(e) {

                e.preventDefault();

                $form.parseValidation();

                if (!$form.valid()) {
                    return;
                }

                $.blockUI();

                $.post($form.attr('action'), $form.serialize(), function (result) {

                    if (result.hasOwnProperty('Result')) {
                        if (result.Result === true) {
                            promise.resolve(result.Message);
                        } else {
                            $form.addCustomErrors(result.Message);
                            $.unblockUI();
                        }
                    } else {
                        if ($parent !== undefined && $parent !== null) {
                            $parent.html(result);
                            $.unblockUI();
                        }
                    }

                });

            });

            return promise;

        }

    });

})(jQuery);

(function ($) {
    
    $.ajaxPrefilter(function (options, originalOptions, xhr) {
        xhr.retry = function (opts) {
            this.interval = opts.interval || 1000;
            this.times = opts.times || 5;
            this.statusCodes = opts.statusCodes || [];
            return this.pipe(successCallback, errorCallback);
        };
    });

    function successCallback(data, textStatus, xhr) {
        return callback(this, xhr, textStatus, data);
    }

    function errorCallback(xhr, textStatus) {
        return callback(this, xhr, textStatus);
    }

    function callback(ajaxOptions, xhr, textStatus, data) {

        var promise = $.Deferred();
        
        if (xhr.times > 1 && (!xhr.statusCodes || $.inArray(xhr.status, xhr.statusCodes) > -1)) {

            setTimeout(function () {

                $.ajax(ajaxOptions)
                    .retry({ times: xhr.times - 1, interval: xhr.interval, statusCodes: xhr.statusCodes })
                    .pipe(promise.resolve, promise.reject);

            }, xhr.interval);

        } else if (xhr.times <= 1) {
            
            promise.rejectWith(this, [xhr, textStatus, '']);

        } else {
            
            promise.resolveWith(this, [data, textStatus, xhr]);
            
        }
        return promise;
    }

})(jQuery);

$.fn.clickToggle = function (a, b) {
    var event = 'click.clickToggle';
    return this.each(function () {
        var clicked = false;
        $(this).off(event).on(event, function () {
            if (clicked) {
                clicked = false;
                return b.apply(this, arguments);
            }
            clicked = true;
            return a.apply(this, arguments);
        });
    });
};

$.fn.clickKeypressToggle = function (a, b) {
    var events = 'click.clickKeypressToggle keypress.clickKeypressToggle';
    return this.each(function () {
        var clicked = false;
        $(this).off(events).on(events, function () {
            if (clicked) {
                clicked = false;
                return b.apply(this, arguments);
            }
            clicked = true;
            return a.apply(this, arguments);
        });
    });
};
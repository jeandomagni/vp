﻿window.VisitPay = window.VisitPay || {};

VisitPay.ChatSession = (function () {
    var obj = VisitPay.ChatSession || {};

    //Session storage variable names
    var sessionChatUrl = 'chatUrl';
    var sessionChatActive = 'chatSessionActive';
    var sessionChatShowing = 'chatShowing';
    var sessionThreadId = 'chatThreadId';
    var sessionOperatorEndedThread = 'chatOperatorEndedThread';
    var sessionOperatorConnected = 'chatOperatorConnected';
    var sessionLogoutRedirect = 'chatLogoutRedirect';

    obj.ActiveSession = function () {
        var activeSession = sessionStorage.getItem(sessionChatActive);
        if (activeSession != null) {
            return JSON.parse(activeSession);
        }
        return false;
    };

    obj.ChatUrl = function () {
        var chatUrl = sessionStorage.getItem(sessionChatUrl);
        return chatUrl;
    };

    obj.ChatIsShowing = function () {
        var showing = sessionStorage.getItem(sessionChatShowing);
        if (showing != null) {
            return JSON.parse(showing);
        }
        return false;
    };

    obj.SetActiveSession = function (chatUrl) {
        sessionStorage.setItem(sessionChatUrl, chatUrl);
        sessionStorage.setItem(sessionChatActive, true);
    };

    obj.SetChatShowing = function (showing) {
        sessionStorage.setItem(sessionChatShowing, showing);
    };

    obj.EndSession = function () {
        sessionStorage.removeItem(sessionChatUrl);
        sessionStorage.removeItem(sessionThreadId);
        sessionStorage.removeItem(sessionOperatorEndedThread);
        sessionStorage.removeItem(sessionOperatorConnected);
        sessionStorage.setItem(sessionChatActive, false);
    };

    obj.ThreadId = function () {
        var threadId = sessionStorage.getItem(sessionThreadId);
        return threadId;
    };

    obj.SetThreadId = function (threadId) {
        sessionStorage.setItem(sessionThreadId, threadId);
    };

    obj.OperatorEndedThread = function () {
        var operatorEndedThread = sessionStorage.getItem(sessionOperatorEndedThread);
        if (operatorEndedThread != null) {
            return JSON.parse(operatorEndedThread);
        }
        return false;
    };

    obj.SetOperatorEndedThread = function () {
        sessionStorage.setItem(sessionOperatorEndedThread, true);
    };

    obj.IsOperatorConnected = function () {
        var operatorConnected = sessionStorage.getItem(sessionOperatorConnected);
        if (operatorConnected != null) {
            return JSON.parse(operatorConnected);
        }
        return false;
    };

    obj.SetOperatorConnected = function (connected) {
        sessionStorage.setItem(sessionOperatorConnected, connected);
    };

    obj.SetLogoutRedirect = function (url) {
        sessionStorage.setItem(sessionLogoutRedirect, url);
    };

    obj.ClearLogoutRedirect = function () {
        sessionStorage.removeItem(sessionLogoutRedirect);
    };

    obj.LogoutRedirect = function () {
        var url = sessionStorage.getItem(sessionLogoutRedirect);
        return url;
    };

    obj.ShouldLogout = function () {
        var logoutUrl = sessionStorage.getItem(sessionLogoutRedirect);
        if (logoutUrl != null) {
            return true;
        }
        return false;
    };

    return obj;
})();
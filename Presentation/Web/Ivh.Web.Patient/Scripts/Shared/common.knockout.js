﻿(function (ko) {

    ko.bindingHandlers.display = (function () {
        function set(element, valueAccessor) {
            var display = ko.unwrap(valueAccessor());
            $(element).css('display', display ? 'block' : 'none');
        }

        return { init: set, update: set };
    })();

    ko.bindingHandlers.hide = (function () {
        function set(element, valueAccessor) {
            var display = ko.unwrap(valueAccessor());
            $(element).css('display', display ? 'none' : 'block');
        }

        return { init: set, update: set };
    })();

    ko.bindingHandlers.hidden = (function () {
        function set(element, valueAccessor) {
            var hidden = ko.unwrap(valueAccessor());
            $(element).css('visibility', hidden ? 'hidden' : 'visible');
        }

        return { init: set, update: set };
    })();

    ko.bindingHandlers.currencyText = {
        update: function (element, valueAccessor, allBindingsAccessor) {

            var precision = ko.utils.unwrapObservable(allBindingsAccessor().precision) || ko.bindingHandlers.currencyText.defaultPrecision;
            var symbol = ko.utils.unwrapObservable(allBindingsAccessor().symbol) || ko.bindingHandlers.currencyText.defaultSymbol;
            var emptyText = ko.utils.unwrapObservable(allBindingsAccessor().emptyText) || ko.bindingHandlers.currencyText.emptyText;

            var value = ko.utils.unwrapObservable(valueAccessor());
            if (value != null && $.isNumeric(value)) {

                var formattedValue = symbol + parseFloat(value.toString().replace(/,/g, '')).toFixed(precision).toString().replace(/(\d)(?=(\d{3})+\b)/g, '$1,');

                ko.bindingHandlers.text.update(element, function() {
                    return formattedValue;
                });
            } else {
                ko.bindingHandlers.text.update(element, function () {
                    return emptyText;
                });
            }

        },
        defaultPrecision: 2,
        defaultSymbol: '$',
        defaultEmptyText: null
    };

    ko.bindingHandlers.percentText = {
        update: function (element, valueAccessor, allBindingsAccessor) {

            var precision = ko.utils.unwrapObservable(allBindingsAccessor().precision) || ko.bindingHandlers.percentText.defaultPrecision;
            var symbol = ko.utils.unwrapObservable(allBindingsAccessor().symbol) || ko.bindingHandlers.percentText.defaultSymbol;
            var emptyText = ko.utils.unwrapObservable(allBindingsAccessor().emptyText) || ko.bindingHandlers.currencyText.emptyText;

            var value = ko.utils.unwrapObservable(valueAccessor());
            if (value != null) {
                var formattedValue = (parseFloat(value.toString().replace(/,/g, '')) * 100).toFixed(precision).toString().replace(/(\d)(?=(\d{3})+\b)/g, '$1,') + ' ' + symbol;

                ko.bindingHandlers.text.update(element, function() {
                    return formattedValue;
                });
            } else {
                ko.bindingHandlers.text.update(element, function () {
                    return emptyText;
                });
            }
        },
        defaultPrecision: 1,
        defaultSymbol: '%',
        emptyText: null
    };

    ko.bindingHandlers.textWithDefault = {
        update: function (element, valueAccessor, allBindingsAccessor) {
            
            var emptyText = ko.utils.unwrapObservable(allBindingsAccessor().emptyText) || ko.bindingHandlers.currencyText.emptyText;

            var value = ko.utils.unwrapObservable(valueAccessor());
            if (value != null) {
                ko.bindingHandlers.text.update(element, function () {
                    return value;
                });
            } else {
                ko.bindingHandlers.text.update(element, function () {
                    return emptyText;
                });
            }
        },
        emptyText: null
    };

    ko.bindingHandlers.ordinalPaymentDueDayText = {
        update: function (element, valueAccessor, allBindingsAccessor) {

            var emptyText = ko.utils.unwrapObservable(allBindingsAccessor().emptyText) || ko.bindingHandlers.ordinalPaymentDueDayText.emptyText;
            var value = ko.utils.unwrapObservable(valueAccessor());

            if (value != null) {

                function getOrdinal(n) {

                    var firstDigit = n % 10,
                        firstTwoDigits = n % 100;

                    if (firstTwoDigits > 27) {
                        if (window.VisitPay && window.VisitPay.Common && window.VisitPay.Localization && window.VisitPay.Localization.LastDayText) {
                            return (window.VisitPay.Localization.LastDayText || '').toLowerCase();
                        }
                        return 'last day';
                    }
                    if (firstDigit === 1 && firstTwoDigits !== 11) {
                        return n + 'st';
                    }
                    if (firstDigit === 2 && firstTwoDigits !== 12) {
                        return n + 'nd';
                    }
                    if (firstDigit === 3 && firstTwoDigits !== 13) {
                        return n + 'rd';
                    }
                    return n + 'th';
                }

                ko.bindingHandlers.text.update(element, function () {
                    return getOrdinal(value);
                });
            } else {
                ko.bindingHandlers.text.update(element, function () {
                    return emptyText;
                });
            }
        },
        emptyText: null
    }

    ko.bindingHandlers.initial = {
        init: function (element, valueAccessor) {
            if (element.getAttribute('value')) {
                valueAccessor()(element.getAttribute('value'));
            } else if (element.tagName === 'SELECT') {
                valueAccessor()(element.options[element.selectedIndex].value);
            }
        },
        update: function (element, valueAccessor) {
            var value = valueAccessor();
            element.setAttribute('value', ko.utils.unwrapObservable(value));
        }
    };

    ko.bindingHandlers.creditCard = {
        init: function (element, valueAccessor) {

            var value = valueAccessor();
            var observable = value.value;

            if (ko.isObservable(observable)) {

                $(element).on('input', function () {

                    observable($(element).val().replace(/_/g, '').replace(/-/g, ''));

                    var cardNumber = $(element).val();
                    var isAmex = cardNumber.indexOf('34') === 0 || cardNumber.indexOf('37') === 0;
                    var mask = isAmex ? '9999-999999-99999' : '9999-9999-9999-9[999999]';
                    var previousMask = $(element).data('mask');

                    if (!previousMask || mask !== previousMask) {
                        $(element).data('mask', mask);
                        $(element).inputmask(mask);
                    }

                });

                $(element).on('focusout change', function () {

                    if ($(element).inputmask('isComplete')) {
                        if ($(element)[0].inputmask !== undefined)
                            observable($(element)[0].inputmask.unmaskedvalue());
                    } else {
                        observable(null);
                    }

                });
            }

        },
        update: function (element, valueAccessor) {

            var mask = valueAccessor();

            var observable = mask.value;

            if (ko.isObservable(observable)) {

                var valuetoWrite = observable();

                $(element).val(valuetoWrite);
            }

        }
    };

    ko.bindingHandlers.preventBubble = {
        init: function (element, valueAccessor) {
            var eventName = ko.utils.unwrapObservable(valueAccessor());
            ko.utils.registerEventHandler(element, eventName, function (event) {
                event.cancelBubble = true;
                if (event.stopPropagation) {
                    event.stopPropagation();
                }
            });
        }
    };

    ko.subscribable.fn.subscribeChanged = function (callback) {
        var savedValue = this.peek();
        return this.subscribe(function (latestValue) {
            var oldValue = savedValue;
            savedValue = latestValue;
            callback(latestValue, oldValue);
        });
    };

})(window.ko);
﻿(function ($) {

    $(document).ready(function () {
        if ($('*[data-intro]').length > 0) {
            //Have page help elements on the page, show the help button
            $('#pageHelpButton').show();

            //Initialize chardin
            $('body').chardinJs();

            //Show/Hide help on button click
            $('#pageHelpButton').click(function () {
                $('body').data('chardinJs').toggle();
            });
        } else {
            //No page help elements on the page, remove the button
            $('#pageHelpButton').remove();
        }
    });

})(jQuery);
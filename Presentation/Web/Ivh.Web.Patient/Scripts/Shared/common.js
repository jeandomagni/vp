﻿window.VisitPay = window.VisitPay || {};

VisitPay.Common = (function ($) {
    var self = {};

    self.BsCollapseUl = function (ul, className, isOneLevelNested) {

        var isOneLevel = isOneLevelNested || false;

        ul.hide();

        var classNameSelector = '.' + className;

        ul.attr('role', 'tablist').addClass(className);
        ul.attr('id', className + $('body').find(classNameSelector).index(ul).toString() + '_root');

        ul.children('li').addClass('panel');
        ul.children('li').each(function () {

            var index = $(classNameSelector).find('li').index($(this)).toString();
            var id = className + index;
            var text = $(this).contents().get(0).nodeValue.trim();
            var html = $(this).html().replace(text, '<a class="collapsed title" data-toggle="collapse" data-parent="#' + ul.attr('id') + '" href="#' + id + '">' + text + '</a>');

            $(this).html(html);

            $(this).children('ul').attr('id', id).addClass('collapse').attr('role', 'tabpanel');
            $(this).children('ul').each(function () {

                $(this).children('li').addClass('panel');
                $(this).children('li').each(function () {

                    if (!isOneLevel) {
                        var index = $(classNameSelector).find('li').index($(this)).toString();
                        var thisId = className + index;

                        var text = $(this).contents().get(0).nodeValue.trim();
                        var html = $(this).html().replace(text, '<a class="collapsed title" data-toggle="collapse" data-parent="#' + id + '" href="#' + thisId + '">' + text + '</a>');

                        $(this).html(html);

                        $(this).children('ul').attr('id', thisId).addClass('collapse').attr('role', 'tabpanel');
                    }
                });
            });

        });

        ul.on('click', '> li > a.title', function () {
            $(this).parent().parent().children('li').children('a.title').not('.collapsed').each(function () {
                $(this).siblings('ul[id^="' + className + '"]').children('li').each(function () {
                    $(this).find('ul[id^="' + className + '"].in').collapse('hide');
                });
            });
        });

        ul.collapse();
        ul.show();
    };

    var createAlertModal = function (title, content, confirmText, modalClass) {

        var promise = $.Deferred();
        
        var okText = window.VisitPay.Localization.Ok || 'OK';
        var html = '<div id="modalGenericAlert" class="modal modal-nested modal-vcenter fade" tabindex="-1" role="dialog" data-backdrop="static"><div class="modal-dialog ' + modalClass + '"><div class="modal-content"><div class="modal-header"><button type="button" class="close" data-dismiss="modal"><span>&times;</span></button><h4 class="modal-title"></h4></div><div class="modal-body text-center"></div><div class="modal-footer text-center"><button type="submit" class="btn btn-primary" title="' + okText + '">' + okText + '</button></div></div></div></div>';
        $('body').append(html);

        var $modal = $('#modalGenericAlert');

        $modal.find('.modal-title').html(title);
        $modal.find('.modal-body').html(content);

        if (confirmText) {
            $modal.find('.modal-footer button[type=submit]').text(confirmText);
            $modal.find('.modal-footer button[type=submit]').attr('title', confirmText);
        }

        $modal.modal('show');

        $modal.on('hidden.bs.modal', function () {
            $modal.remove();
        });

        $modal.on('click', 'button[type=submit]', function () {
            $modal.modal('hide');
            promise.resolve();
        });

        $modal.on('click', '[data-dismiss="modal"]', function () {
            promise.reject();
        });

        return promise;

    };

    var createConfirmationModal = function (title, content, confirmText, cancelText, modalClass, rejectOnCancelButtonOnly, opts) {

        opts = opts || {};
        var promise = $.Deferred();

        var yesText = window.VisitPay.Localization.Yes || 'Yes';
        var noText = window.VisitPay.Localization.No || 'No';
        var html = '<div id="modalGenericConfirmation" class="modal modal-nested modal-vcenter fade" tabindex="-1" role="dialog" data-backdrop="static"><div class="modal-dialog ' + modalClass + '"><div class="modal-content"><div class="modal-header"><button type="button" class="close" data-dismiss="modal"><span>&times;</span></button><h4 class="modal-title"></h4></div><div class="modal-body text-center"></div><div class="modal-footer text-center"><button type="submit" class="btn btn-primary" title="' + yesText + '">' + yesText + '</button><button type="button" class="btn btn-primary" data-dismiss="modal" title="' + noText + '">' + noText + '</button></div></div></div></div>';
        $('body').append(html);

        var $modal = $('#modalGenericConfirmation');

        $modal.find('.modal-title').html(title);
        $modal.find('.modal-body').html(content);

        if (confirmText) {
            $modal.find('.modal-footer button[type=submit]').text(confirmText);
            $modal.find('.modal-footer button[type=submit]').attr('title', confirmText);
        }
        if (cancelText) {
            $modal.find('.modal-footer button[type=button]').text(cancelText);
            $modal.find('.modal-footer button[type=button]').attr('title', cancelText);
        }
        if (content === undefined || content === null || content.length === 0)
            $modal.find('.modal-body').remove();

        $modal.modal('show');

        $modal.on('hidden.bs.modal', function () {
            $modal.remove();
        });

        $modal.on('click', 'button[type=submit]', function () {
            if (opts.resolveAfterHidden) {
                $modal.on('hidden.bs.modal', function () {
                    promise.resolve();
                });
                $modal.modal('hide');
            } else {
                $modal.modal('hide');
                promise.resolve();
            }
        });

        rejectOnCancelButtonOnly = rejectOnCancelButtonOnly || false;
        if (rejectOnCancelButtonOnly) {

            $modal.on('click', '.modal-footer button[type=button]', function () {
                $modal.modal('hide');
                promise.reject();
            });

        } else {

            $modal.on('click', '[data-dismiss="modal"]', function () {
                promise.reject();
            });

        }

        return promise;

    };

    self.Modal = function(modalId, html, show) {
        
        $('body').append(html);

        var $modal = $('body').find('#' + modalId + '.modal');

        $modal.on('hidden.bs.modal', function() {
            $modal.remove();
        });

        if (show) {
            $modal.modal();
        }

        return $modal;

    }

    self.ModalAsync = function (element, title, url, method, data, fn) {

        method = method || 'GET';

        $.ajax({
            method: method,
            url: url,
            data: data
        }).done(function(html) {
            element.find('.modal-body').html(html).scrollTop(0);
            element.find('.modal-title').text(title);
            element.modal();

            if (fn !== undefined && fn !== null)
                fn();
        });

    };

    self.ModalContainerAsync = function (element, url, method, data, show) {

        var promise = $.Deferred();

        $.ajax({
            method: method,
            url: url,
            data: data
        }).done(function(html) {
            element.html(html);

            var modal = element.find('.modal');

            if (show)
                modal.modal();

            promise.resolve(modal);
        });

        return promise;

    };

    self.ModalNoContainerAsync = function (modalId, url, method, data, show, callbackAfterClose, callbackOnShow) {

        var promise = $.Deferred();

        $.ajax({
            method: method,
            url: url,
            data: data
        }).done(function(html) {

            $('body').append(html);

            var $modal = $('body').find('#' + modalId + '.modal');

            $modal.on('hidden.bs.modal', function() {
                if (!$modal.hasClass('disable-trigger')) {
                    $modal.remove();
                    if (callbackAfterClose && typeof (callbackAfterClose) === 'function') {
                        callbackAfterClose($modal);
                    }
                }
            });
            $modal.on('shown.bs.modal', function() {
                if (callbackOnShow && typeof (callbackOnShow) === 'function') {
                    callbackOnShow($modal);
                }
            });

            if (show)
                $modal.modal();

            promise.resolve($modal);

        });

        return promise;

    };

    self.ModalGeneric = function (title, content, modalClass) {

        modalClass = modalClass || 'modal-md';
        var html = '<div id="modalGeneric" class="modal modal-nested modal-vcenter fade" tabindex="-1" role="dialog" data-backdrop="static"><div class="modal-dialog ' + modalClass + '"><div class="modal-content"><div class="modal-header"><button type="button" class="close" data-dismiss="modal"><span>&times;</span></button><h4 class="modal-title"></h4></div><div class="modal-body"></div><div class="modal-footer text-center"><button type="submit" class="btn btn-primary" title="OK">OK</button></div></div></div></div>';
        $('body').append(html);

        var $modal = $('#modalGeneric');

        $modal.find('.modal-title').html(title);
        $modal.find('.modal-body').html(content);
        $modal.find('.modal-footer').hide();

        $modal.on('hidden.bs.modal', function () {
            $modal.remove();
        });
        
        return $modal;
    };

    self.ModalGenericConfirmation = function (title, content, confirmText, cancelText, rejectOnCancelButtonOnly, opts) {
        return createConfirmationModal(title, content, confirmText, cancelText, 'modal-sm', rejectOnCancelButtonOnly, opts);
    }

    self.ModalGenericConfirmationMd = function (title, content, confirmText, cancelText, rejectOnCancelButtonOnly, opts) {
        return createConfirmationModal(title, content, confirmText, cancelText, '', rejectOnCancelButtonOnly, opts);
    }

    self.ModalGenericConfirmationLg = function (title, content, confirmText, cancelText, rejectOnCancelButtonOnly, opts) {
        return createConfirmationModal(title, content, confirmText, cancelText, 'modal-lg', rejectOnCancelButtonOnly, opts);
    }

    self.ModalGenericAlert = function (title, content, confirmText) {
        return createAlertModal(title, content, confirmText, 'modal-sm');
    };

    self.ModalGenericAlertMd = function (title, content, confirmText) {
        return createAlertModal(title, content, confirmText, 'modal-md');
    };

    self.ModalGenericAlertLg = function (title, content, confirmText) {
        return createAlertModal(title, content, confirmText, 'modal-lg');
    };

    self.DisablePaste = function (selector, scopeElement) {
        scopeElement = scopeElement || $(document);
        scopeElement.on('paste', selector, function (e) {
            e.preventDefault();
        });
        scopeElement.on('contextmenu', selector, function (e) {
            e.preventDefault();
        });
    };

    self.AppendMessageToValidationSummary = function (form, message, isMobileSummary) {

        isMobileSummary = isMobileSummary || false;

        var container = isMobileSummary ? form.find('[data-mob-valmsg-summary="true"]') : form.find('[data-valmsg-summary="true"]');
        if (container && container.length) {
            container.removeClass('validation-summary-valid').addClass('validation-summary-errors');
            var list = container.find('ul');
            if (list && list.length && !$('li:contains("' + message + '")').length) {
                $('<li />').html(message).appendTo(list);
            }
        }

    };

    self.PrependMessageToValidationSummary = function (form, message, isMobileSummary) {

        isMobileSummary = isMobileSummary || false;

        var container = isMobileSummary ? form.find('[data-mob-valmsg-summary="true"]') : form.find('[data-valmsg-summary="true"]');
        if (container && container.length) {
            container.removeClass('validation-summary-valid').addClass('validation-summary-errors');
            var list = container.find('ul');
            if (list && list.length && !$('li:contains("' + message + '")').length) {
                $('<li />').html(message).prependTo(list);
            }
        }

    };

    self.ResetUnobtrusiveValidation = function (form) {
        form.removeData('validator').removeData('unobtrusiveValidation');
        $.validator.unobtrusive.parse(form);
        form.validate().resetForm();
        form.find('[data-valmsg-summary=true]').removeClass('validation-summary-errors').addClass('validation-summary-valid').find('ul').empty();
        form.find('.field-validation-error').removeClass('field-validation-error').addClass('field-validation-valid');
        form.find('.input-validation-error').removeClass('input-validation-error').addClass('valid');
    };

    self.ResetValidationSummary = function (form, isMobileSummary) {

        isMobileSummary = isMobileSummary || false;

        var container = isMobileSummary ? form.find('[data-mob-valmsg-summary="true"]') : form.find('[data-valmsg-summary="true"]');
        if (container && container.length) {
            var list = container.find('ul');

            if (list && list.length) {
                list.empty();
                container.addClass('validation-summary-valid').removeClass('validation-summary-errors');
            }
        }

    };

    // hack to remove "...is required" from validation summarys
    self.SuppressRequiredErrorsFromValidationSummary = function (forms) {

        forms.each(function () {
            var messages = $(this).data('validator').settings.messages;
            for (var propertyName in messages) {
                if (messages.hasOwnProperty(propertyName)) {
                    messages[propertyName].conditionalrequired = '';
                    messages[propertyName].required = '';
                }
            }
        });
        $.validator.unobtrusive.parse(document);

    };

    self.TurnOffValidation = function (form) {
        var settings = form.validate().settings;
        for (var ruleIndex in settings.rules) {
            if (settings.rules.hasOwnProperty(ruleIndex)) {
                delete settings.rules[ruleIndex];
            }
        }
    };

    self.AddAntiForgeryToken = function (data) {
        data.__RequestVerificationToken = $('#__AjaxAntiForgeryForm input[name=__RequestVerificationToken]').val();
        return data;
    };

    self.InitTooltips = function ($container) {
        $container.find('[data-toggle="tooltip"]').tooltip({ animation: false, container: 'body' });
    }

    self.expansion = function (e) {
        e.preventDefault();
        if ($(this).hasClass('expanded')) {
            $(this).removeClass('expanded');
            $(this).parent().find('.list').hide();
        } else {
            $(this).addClass('expanded');
            $(this).parent().find('.list').show();
        }
    }

    self.RefreshAlerts = function() {
        var promise = $.Deferred();
        if (!VisitPay.State.IsOfflineUser) {
            $.ajax({
                url: '/Account/Alerts'
            }).done(function(result) {
                $('.alerts').replaceWith(result);
                promise.resolve();
            });
        } else {
            //User is offline user, so don't check alerts
            promise.resolve();
        }
        
        return promise;
    };

    self.getQuery = function (query) {
        query = query.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
        var expr = "[\\?&]" + query + "=([^&#]*)";
        var regex = new RegExp(expr);
        var results = regex.exec(window.location.href);
        if (results !== null) {
            return results[1];
        } else {
            return false;
        }
    };

    self.setInputMask = function (element, mask) {
        mask = mask || element.attr('data-mask');
        element.inputmask(mask, { "clearIncomplete": true });
    };

    self.initMobileDatePicker = function (element) {
        var maxYears = element.data('val-agedaterange-maximumage');
        var minYears = element.data('val-agedaterange-minimumage');
        var startYear = window.moment().year() - 50;

        element.attr({
            min: window.moment().add(1, 'days').subtract(maxYears, 'years').format('YYYY-MM-DD'),
            max: window.moment().subtract(minYears, 'years').format('YYYY-MM-DD'),
            defaultValue: startYear + '-01-01'
        });
    };

    self.getFileType = function(fileType) {

        switch (fileType) {
            case 'application/msword':
            case 'application/vnd.openxmlformats-officedocument.wordprocessingml.document':
                return 'file-type-word';
            case 'application/vnd.ms-excel':
            case 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet':
                return 'file-type-excel';
            case 'application/vnd.ms-powerpoint':
            case 'application/vnd.openxmlformats-officedocument.presentationml.presentation':
                return 'file-type-ppt';
            case 'application/pdf':
                return 'file-type-pdf';
            case 'application/x-zip-compressed':
                return 'file-type-zip';
            case 'image/gif':
            case 'image/png':
            case 'image/jpg':
            case 'image/jpeg':
                return 'file-type-image';
            default:
                return 'file-type-text';
        }
    };

    self.ReloadSidebar = function () {
        var promise = $.Deferred();

        if ($('.sidebar-inner').length > 0) {
            $.get('/Sidebar/Sidebar', {}, function(html) {
                $('.sidebar-inner').html($(html).html());
                promise.resolve();
            });

        }
        return promise;
    };

    self.AddUrlParam = function (key, value) {
        key = escape(key);
        value = escape(value);

        var kvp = document.location.search.substr(1).split('&');
        if (kvp == '') {
            //Assign to document.location.search if you want to reload the page.
            return '?' + key + '=' + value;
        }
        else {

            var i = kvp.length; var x; while (i--) {
                x = kvp[i].split('=');

                if (x[0] == key) {
                    x[1] = value;
                    kvp[i] = x.join('=');
                    break;
                }
            }

            if (i < 0) { kvp[kvp.length] = [key, value].join('='); }

            //Assign to document.location.search if you want to reload the page.
            return kvp.join('&');
        }
    };

    $(document).on('click', '.expandable-grid .header', self.expansion);

    $(document).ajaxError(function (event, jqxhr, settings, thrownError) {

        $.unblockUI();

        // if either of these are true, then it's not a true error and we don't care
        // navigating before ajax completes for example
        if (jqxhr.status === 0 || jqxhr.readyState === 0) {
            return;
        }

        var url = settings && settings.url ? settings.url.split('?')[0].toUpperCase() : '';

        if (url !== window.VisitPay.Urls.ErrorLogging.toUpperCase()) {

            try {
                $.ajax({
                    global: false,
                    url: window.VisitPay.Urls.ErrorLogging,
                    type: 'POST',
                    data: { 'ajaxEvent': JSON.stringify(event), 'jqxhr': jqxhr.status, 'settings': JSON.stringify(settings), 'thrownError': thrownError }
                }).done(function () {
                    // nothing
                }).fail(function () {
                    // nothing
                });
            } catch (x) {
                // nothing
            }

            if (url !== window.VisitPay.Urls.UpdateSession.toUpperCase()) {
                if (jqxhr.status === 401) { // emulate error
                    var unauthorizedErrorMessage = 'Unauthorized';
                    
                    if (thrownError && thrownError.length > 0) {
                        unauthorizedErrorMessage = thrownError;
                    } else {
                        var response = jqxhr.getResponseHeader(window.VisitPay.Constants.EmulatedUserNotAuthorized);
                        if (response) {
                            unauthorizedErrorMessage = response;
                        }
                    }
     
                    $('.modal').modal('hide');
                    $('#modalEmulate').modal('show');
                    $('#modalEmulate').find('.modal-body').html('<p>' + unauthorizedErrorMessage + '</p>');
                } else {
                    var processingError = window.VisitPay.Localization.ProcessingError || 'Processing Error';
                    var errorText = window.VisitPay.Localization.ErrorOccurredProcessingRequest || 'An error occurred while processing your request.';
                    var okText = window.VisitPay.Localization.Ok || 'OK';
                    self.ModalGenericAlert(processingError, '<p>' + errorText + '</p>', okText);
                }
            }
        }

    });
    
    return self;

})(jQuery);
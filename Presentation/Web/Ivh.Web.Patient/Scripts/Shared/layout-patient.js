﻿// initialize common layout components
(function () {

    $(document).ready(function () {
        
        //Show NPS survey on any page
        if ($('#nps-survey').length > 0) {

            setTimeout(function () {
                window.VisitPay.Survey.ShowSurvey(
                    {
                        'SurveyName': 'NetPromoterScore'
                    }, true);
            }, 1000);

            $('#nps-survey').on('click', function () {
                $(this).addClass('expanded');
            });
        }

    });

})();
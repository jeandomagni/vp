﻿(function () {

    var $scope = $('.sidebar');

    $(document).ready(function () {

        $scope.on('click.tracker', '#aPayBill', function (e) {
            window._paq.push(['trackEvent', 'Payments', 'Access From', window.location.href]);
            window._paq.push(['trackEvent', 'Payments', 'Entry Point', 'Pay Bill Button']);
        });

        $scope.on('click.tracker', '#aCreateFinancePlan', function (e) {
            window._paq.push(['trackEvent', 'Payments', 'Access From', window.location.href]);
            window._paq.push(['trackEvent', 'Payments', 'Entry Point', 'Create Finance Plan Button']);
        });

    });


})();
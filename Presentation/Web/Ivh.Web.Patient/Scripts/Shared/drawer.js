﻿window.VisitPay.DrawerComponent = (function (factory) {

    var promise = $.Deferred();
    var isLoading = false;
    var isLoaded = false;

    return {
        load: function () {
            if (!isLoading && !isLoaded) {
                isLoading = true;
                factory().done(function () {
                    promise.resolve();
                    isLoaded = true;
                });
            }
            return promise;
        }
    };

});

(function() {
    
    var $mask = $('#drawer-mask');
    var $drawer = $('#drawer');
    var $links = $('#navbar-settings [data-drawer]');
    var animationDuration = 200;

    var containers = {};
    var drawers = {};
    var links = {};
    
    function trackPiwikDrawer(targetDrawer, show) {
        var actionName = show ? 'Show' : 'Hide';
        window._paq.push(['trackEvent', 'Menu', 'Click', actionName + ' ' + targetDrawer]);
    };

    function setDrawerMaxHeight() {
        if ($drawer.data('open') != null && $drawer.data('open').length > 0) {
            var pageHeaderOffset = $('#header').height();
            var pageFooterTop = $('#footer').position().top;
            var maxDrawerOuterHeight = pageFooterTop - pageHeaderOffset - 2;

            $drawer.css('max-height', maxDrawerOuterHeight);
            $drawer.css('overflow-y', 'auto');
        }
    }

    function hideDrawer(e, targetDrawerName, hideMask) {
        if (e) {
            e.preventDefault();
        }

        hideMask = hideMask == null ? true : hideMask;
        
        $links.each(function () {
            if ($(this).data('drawer') !== (targetDrawerName || '')) {
                $(this).parent().removeClass('open');
            }
        });
        
        $drawer.data('open', '');
        $drawer.find('[data-drawer]:visible').slideUp(animationDuration);
        $drawer.unblock({ fadeOut: 0 });

        if (hideMask) {
            setTimeout(function() { $drawer.hide(); }, animationDuration);
            $mask.fadeOut(animationDuration);
        }
    }

    function showDrawer(e) {

        e.preventDefault();
        $(this).parent().addClass('open');
        
        var previousDrawerName = $drawer.data('open');
        var targetDrawerName = $(this).data('drawer');
        
        var close = previousDrawerName === targetDrawerName;
        if (close) {
            targetDrawerName = null;
        }

        hideDrawer(null, targetDrawerName, close);

        if (close) {
            trackPiwikDrawer(targetDrawerName, false);
            return;
        }
        
        
        $drawer.css('overflow-y', 'visible');
        $drawer.data('open', targetDrawerName);
        trackPiwikDrawer(targetDrawerName, true);
        
        // load the content
        var drawerComponent = drawers[targetDrawerName];
        var $container = containers[targetDrawerName];

        if (drawerComponent == null) {
            return;
        }

        var promise = drawerComponent.load();
        
        if (promise.state() !== 'resolved') {
            $drawer.block();
            $container.hide();
        }
            
        $drawer.show();
        $mask.fadeIn(animationDuration);

        promise.done(function () {
            setDrawerMaxHeight();
            $drawer.unblock({ fadeOut: animationDuration });
            $container.slideDown(animationDuration);
        });
        
    };

    function assignLinkAttributes(link, data) {
        if (data.count > 0) {
            link.attr('data-count', data.count);
        } else {
            link.removeAttr('data-count');
        }
    }
    
    var resizeTimeout;
    $(window).on('resize', function () {
        if (resizeTimeout) {
            window.clearTimeout(resizeTimeout);
        }
        if ($drawer.data('open') != null && $drawer.data('open').length > 0) {
            resizeTimeout = window.setTimeout(function() {
                setDrawerMaxHeight();
            }, 250);
        }
    });

    $(document).ready(function() {

        if (!window.VisitPay.State.IsUserLoggedIn) {
            return;
        }

        var selectors = {
            account: '[data-drawer="account"]',
            help: '[data-drawer="help"]',
            notifications: '[data-drawer="notifications"]'
        };

        // links that trigger the drawer
        links.account = $('#navbar-settings').find(selectors.account);
        links.help = $('#navbar-settings').find(selectors.help);
        links.notifications = $('#navbar-settings').find(selectors.notifications);

        // set containers for the content to load into
        containers.account = $drawer.find(selectors.account);
        containers.help = $drawer.find(selectors.help);
        containers.notifications = $drawer.find(selectors.notifications);
        
        // callbacks
        containers.notifications.on('drawer:change', function (e, data) {
            assignLinkAttributes(links.notifications, data);
        });
        containers.help.on('drawer:change', function (e, data) {
            assignLinkAttributes(links.help, data);
        });

        // component instances
        var helpComponent = new window.VisitPay.HelpWidget(containers.help);
        var notificationsComponent = new window.VisitPay.Notifications(containers.notifications);
        
        // drawers
        drawers.account = new window.VisitPay.DrawerComponent(function() { return $.Deferred().resolve(); });
        drawers.help = new window.VisitPay.DrawerComponent(helpComponent.LoadContent);
        drawers.notifications = new window.VisitPay.DrawerComponent(notificationsComponent.LoadContent);
        
        // click handlers
        $mask.on('click.drawer', hideDrawer);
        $links.on('click.drawer', showDrawer);

    });

})();
﻿(function ($) {

    var message = 'Please wait';
    if (window.VisitPay != null && window.VisitPay.Localization != null) {
        message = window.VisitPay.Localization.PleaseWait;
    }

    $.blockUI.defaults.baseZ = 99999;
    $.blockUI.defaults.css.padding = 18;
    $.blockUI.defaults.overlayCSS = {
        backgroundColor: '#ffffff',
        cursor: 'wait',
        opacity: 0.85
    },
    $.blockUI.defaults.message = '<h1 style="line-height: 32px;"><img src="/Content/Images/loading.gif" alt="" /><span> &nbsp; ' + message + '...</span></h1>';
    $.blockUI.defaults.ignoreIfBlocked = true;

    var modalVerticalCenter = function($modal) {

        var offset = 0;

        $modal.css('display', 'block');

        var $dialog = $modal.find('.modal-dialog');
        if ($dialog)
            offset = ($(window).height() - $dialog.height() - 100) / 2;

        $dialog.css('margin-top', Math.max(offset, 0));

    };
    var setUiComponents = function() {

        $('body.modal-open').css('padding-right', '0');
        $('[placeholder]').watermark({ textAttr: 'placeholder' });
        $('[data-mask]').each(function() {
            $(this).mask($(this).data('mask').toString());
        });
        $('[data-toggle="tooltip"]').tooltip({ animation: false, container: 'body' });
    };
    
    $(document).ready(function () {

        window._paq = window._paq || [];

        setUiComponents();

        $.ajaxSetup({ data: { __RequestVerificationToken: $('#__AjaxAntiForgeryForm input[name=__RequestVerificationToken]').val() } });

        $('.modal').on('shown.bs.modal', setUiComponents);
        
        $(document).on('show.bs.modal', function (e) {

            var $modal = $(e.target);
            if ($modal.hasClass('modal-vcenter')) {

                modalVerticalCenter($modal);

            }

        });

        $(document).on('shown.bs.modal.top', function (e) {
            $(e.target).find('.modal-body').scrollTop(0);
        });

        $(document).on('click', '.guarantor-filter .guarantor-option', function () {
            $.blockUI();
            var obfuscatedVpGuarantorId = $(this).data('guarantorId');
            
            $.post('/GuarantorFilter/SetGuarantor', { obfuscatedVpGuarantorId: obfuscatedVpGuarantorId }).then(function () {
                $.unblockUI();

                var event = $.Event('guarantorFilterChanged');
                event.obfuscatedVpGuarantorId = obfuscatedVpGuarantorId;
                $(document).trigger(event);
            });
        });
        
        // this fixes bootstrap tooltip behavior (intermittent) in IE
        // when the tooltip is open and a modal windows open but doesn't cover the tooltip, the tooltip never hides
        $(document).on('click.tooltip', '[data-toggle="tooltip"]', function () {
            $(this).blur();
        });

        $(document).on('click.alert-tracker', '.alerts a[href*="/Payment/ArrangePayment"]', function () {
            window._paq.push(['trackEvent', 'Payments', 'Access From', window.location.href]);
            window._paq.push(['trackEvent', 'Payments', 'Entry Point', 'Enter from Alert']);
        });

        $(document).on('click.alerts.dismiss-alert', '.alerts > div.list > div .dismiss-alert', function (e) {
            e.preventDefault();
            $.blockUI();
            var alertId = $(this).data('alert-id');
            $.post('/Account/RemoveAlert', { id: alertId }, function () {
                window.VisitPay.Common.RefreshAlerts().done(function () { $.unblockUI(); });
            });
        });

        $(document).on('click.open-support-request', '[data-click="create-support-request"]', function (e) {
            e.preventDefault();
            window.VisitPay.CreateSupportRequest.Initialize($('#modalCreateSupportRequestContainer'), '');
        });

        $(document).on('click.dismiss-sso-invalidated', '[data-click="dismiss-sso-invalidated"]', function (e) {
            e.preventDefault();
            $.blockUI();
            $.post('/Sso/AcknowledgeInvalidatedSso', { ssoProvider : 2 }, function () {
                window.VisitPay.Common.RefreshAlerts().done(function() { $.unblockUI(); });
            });
        });

        $(document).on('click.dismiss-hqy-getstarted', '[data-click="dismiss-hqy-getstarted"]', function (e) {
            e.preventDefault();
            $.blockUI();
            $.post('/Sso/DismissHqyGettingStartedAlert', { }, function () {
                window.VisitPay.Common.RefreshAlerts().done(function() { $.unblockUI(); });
            });
        });

        $(document).on('click.hqy-getstarted', '[data-click="hqy-getstarted"]', function (e) {
            window._paq.push(['trackEvent', 'HealthEquity', 'AlertClick']);
            window.VisitPay.HealthEquityCommon.initNoSsoAgreement(false);
        });

        $(document).on('click', '[data-piwik-click]', function (e) {
            var category = $(this).data("piwik-event-category");
            var eventName = $(this).data("piwik-event-name");
            window._paq.push(['trackEvent', category, 'Access From', window.location.href]);
            window._paq.push(['trackEvent', category, 'Click', eventName]);
        });
        
        // global modals

        $(document).on('click', 'a[href="#Privacy"]', function(e) {
            e.preventDefault();
            window.VisitPay.Common.ModalAsync($('#modalFooter'), 'Privacy Policy', '/Legal/PrivacyPartial');
        });

        $(document).on('click', 'a[href$="/Legal/Privacy"]', function (e) {
            if (window.VisitPay.State.IsUserLoggedIn) {
                e.preventDefault();
                window.VisitPay.Common.ModalAsync($('#modalFooter'), 'Privacy Policy', '/Legal/PrivacyPolicy');
            }
        });

        $(document).on('click', 'a[href="#Legal"]', function (e) {
            e.preventDefault();
            window.VisitPay.Common.ModalAsync($('#modalFooter'), 'Legal', '/Legal/LegalPartial');
        });

        $(document).on('click', 'a[href="#EsignAct"], [data-href="#EsignAct"]', function (e) {
            e.preventDefault();
            window.VisitPay.Common.ModalAsync($('#modalFooter'), 'E-Sign Act Terms', '/Legal/EsignPartial');
        });
        
        $(document).on('click', 'a[href="#TermsOfUse"], [data-href="#TermsOfUse"]', function(e) {
            e.preventDefault();
            window.VisitPay.Common.ModalAsync($('#modalFooter'), 'Terms of Use', '/Legal/TermsPartial');
        });

        $(document).on('click', 'a[href$="/Legal/TermsOfUse"]', function (e) {
            if (window.VisitPay.State.IsUserLoggedIn) {
                e.preventDefault();
                window.VisitPay.Common.ModalAsync($('#modalFooter'), 'Terms of Use', '/Legal/TermsPartial');
            }
        });

        $(document).on('click', 'a[href="#CreditAgreement"]', function(e) {
            e.preventDefault();

            var url = '/Legal/CreditAgreementPartial';
            var params = [];
            
            var offerid = $(this).attr('data-offerid');
            if (offerid && offerid.length > 0) {
                params.push('o=' + offerid);
            }

            if (params.length > 0) {
                url += '?' + params.join('&');
            }

            window.VisitPay.Common.ModalNoContainerAsync('modalCreditAgreement', url, 'GET', {}, true);

        });

        $('table.static-grid td').each(function () {
            $(this).attr('title', $(this).text().trim());
        });

    });

})(jQuery);

var getFileType = function (fileType) {

    switch (fileType) {
        case 'application/msword':
        case 'application/vnd.openxmlformats-officedocument.wordprocessingml.document':
            return 'file-type-word';
        case 'application/vnd.ms-excel':
        case 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet':
            return 'file-type-excel';
        case 'application/vnd.ms-powerpoint':
        case 'application/vnd.openxmlformats-officedocument.presentationml.presentation':
            return 'file-type-ppt';
        case 'application/pdf':
            return 'file-type-pdf';
        case 'application/x-zip-compressed':
            return 'file-type-zip';
        case 'image/gif':
        case 'image/png':
        case 'image/jpg':
        case 'image/jpeg':
            return 'file-type-image';
        default:
            return 'file-type-text';

    }
};

window.onerror = function (message, file, line, col, error) {
    try {
        $.ajax({
            global: false,
            url: window.VisitPay.Urls.WindowOnError,
            type: 'POST',
            data: { 'message': message, 'file': file, 'line': line, 'col': col, 'error': error }
        }).done(function () {
            // nothing
        }).fail(function () {
            // nothing
        });
    } catch (x) {
        // nothing
    }
    return false;
};
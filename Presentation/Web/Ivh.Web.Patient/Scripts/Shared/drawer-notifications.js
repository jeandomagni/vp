﻿window.VisitPay.Notifications = (function ($container) {
   
    var urls = {
        NotificationsData: '/Notification/NotificationData',
        NotificationsPartial: '/Notification/NotificationDrawer',
        NotificationRead: '/Notification/MarkMessageAsRead',
        NotificationReadAll: '/Notification/MarkAllMessagesAsRead',
        NotificationsUnread: '/Notification/UnreadMessageCount'
    };

    function trackPiwik(actionName) {
        window._paq.push(['trackEvent', 'Menu', 'Click', actionName]);
    };
    
    function NotificationVm(obj) {

        var self = {};

        self.ContentBody = ko.observable(obj.ContentBody);
        self.ContentTitle = ko.observable(obj.ContentTitle);
        self.MessageRead = ko.observable(obj.MessageRead);
        self.SystemMessageId = ko.observable(obj.SystemMessageId);

        self.Click = function (item, e) {
            
            var promise = self.MarkAsRead();
            var href;
            
            var $element = $(e.target).is('.drawer-item') ? $(e.target) : $(e.target).parents('.drawer-item').eq(0);
            var $link = $element.find('a').eq(0);
            if ($link.length > 0) {
                href = $link.attr('href');
            }

            trackPiwik('Notification ' + ko.unwrap(self.SystemMessageId) + ' click');
            
            promise.done(function() {
                // redirect to the first link in the content
                if (href && href.length > 0 && href.indexOf('javascript:') === -1 && href !== '#') {
                    window.location.href = href;
                }
            });

        };
        self.MarkAsRead  = function () {

            var promise = $.Deferred();
            
            if (ko.unwrap(self.MessageRead) !== true) {
                self.MessageRead(true);
                var systemMessageId = ko.unwrap(self.SystemMessageId);
                $.post(urls.NotificationRead, { systemMessageId: systemMessageId }, function() {
                    promise.resolve();
                });
                trackPiwik('Notification ' + systemMessageId + ' read');
            } else {
                promise.resolve();
            }

            return promise;
        };
        
        return self;
    };

    function NotificationsVm() {

        var self = {};

        self.Notifications = ko.observableArray([]);
        self.Notifications.Any = ko.computed(function () {
            return ko.unwrap(self.Notifications).length > 0;
        });
        self.Notifications.UnreadCount = ko.computed(function () {
            var unread = ko.utils.arrayFilter(ko.unwrap(self.Notifications), function (notification) {
                return ko.unwrap(notification.MessageRead) !== true;
            });
            return unread.length;
        });
        self.MarkAllRead = function (item, e) {
            
            $(e.target).blur();

            ko.utils.arrayForEach(self.Notifications(), function(notification) {
                notification.MessageRead(true);
            });
            
            $.post(urls.NotificationReadAll);
            trackPiwik('Notification mark all as read');
            
        };

        return self;

    };
    
    function loadHtml() {
        var promise = $.Deferred();

        $.get(urls.NotificationsPartial, function(html) {
            promise.resolve(html);
        });

        return promise;
    };

    function loadData() {
        var promise = $.Deferred();

        $.post(urls.NotificationsData, function (data) {
            promise.resolve(data);
        });

        return promise;
    };

    function triggerChange(count) {
        var promise = $.Deferred();
        if (count == null) {
            $.get(urls.NotificationsUnread, function(data) {
                promise.resolve({ count: data.Count });
            });
        } else {
            promise.resolve({ count: count });
        }

        promise.done(function(obj) {
            $container.trigger('drawer:change', obj);
        });
    };
    
    // send initial change to drawer
    triggerChange();
    
    return {
        LoadContent: function() {
            
            var promise = $.Deferred();

            $.when(loadHtml(), loadData()).done(function(html, data) {

                $container.html(html);
                
                var model = new NotificationsVm();

                for (var i = 0; i < data.length; i++) {
                    var notification = new NotificationVm(data[i]);
                    model.Notifications.push(notification);
                }

                model.Notifications.UnreadCount.subscribe(function(newValue) {
                    $container.trigger('drawer:change', {
                        count: newValue
                    });
                });
                
                ko.cleanNode($container);
                ko.applyBindings(model, $container[0]);
                
                promise.resolve();

            });
            
            return promise;
        }
    };

});
﻿(function(components) {

    components.StepsComponent = (function () {

        return $.Deferred().resolve({
            template: '#steps-component',
            data: function() {
                return {
                    steps: []
                }
            },
            created: function() {

                // set steps initially
                this.setSteps(this.$route);

                // set steps array after route change
                this.$router.afterEach(this.setSteps);

                // prevent forward route change unless complete
                this.$router.beforeEach(function(to, from, next) {

                    if (to.meta.number < from.meta.number || from.meta.isComplete) {
                        next();
                        return;
                    }

                    next(false);

                });

            },
            methods: {
                setSteps: function(to) {

                    var routes = this.$router.options.routes;
                    routes.forEach(function(route) {
                        // assign a number based on routing structure
                        route.meta.number = routes.indexOf(route) + 1;

                        // any step < current route number is not complete
                        route.meta.isComplete = route.meta.number < to.meta.number;
                    });

                    var steps = [];
                    this.$router.options.routes.forEach(function(route) {
                        steps.push({
                            isCurrent: route.meta.number && route.meta.number === to.meta.number,
                            isComplete: route.meta.isComplete,
                            label: route.meta.label,
                            number: route.meta.number,
                            path: route.path
                        });
                    });

                    this.steps = steps;
                }
            }
        });

    });

})(window.VisitPay.Components);
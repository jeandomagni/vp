﻿window.VisitPay.Chat = (function ($) {

    var urls = {
        GenerateThreadId: VisitPay.ChatSettings.ChatApiUrl + 'thread/threadId',
        ChatIframeUrl: VisitPay.ChatSettings.ChatIframeUrl,
        HelpCenter: '/Home/Help'
    };

    var chatDialogOpened = function () {
        log.debug('chat opened');
        setChatButtonActive(true);
        $('#chatButton').removeClass('pulse-button');

        //Save that chat is showing
        VisitPay.ChatSession.SetChatShowing(true);
    };

    var chatDialogHidden = function () {
        log.debug('chat hidden');
        setChatButtonActive(false);

        //Pulse button if active session, otherwise clear session
        if (VisitPay.ChatSession.ActiveSession()) {
            $('#chatButton').addClass('pulse-button');

            //Save that chat is hidden
            VisitPay.ChatSession.SetChatShowing(false);
        } else {
            VisitPay.ChatSession.EndSession();
        }
    };

    var chatDialogClosed = function () {
        log.debug('chat closed');
        //Close dialog
        setChatButtonActive(false);
        VisitPay.ChatSession.SetChatShowing(false);

        //Clear out the iframe URL for good measure
        $('#chatIframe').attr('src', null);

        if ($('#chatDialog').dialog("instance")) {
            log.debug('destroying chat dialog');
            $('#chatDialog').dialog("destroy");
            $('#chatDialog').remove();
        }
    };

    function setChatButtonActive(isActive) {
        //Get rid of focus
        $('#chatButton').blur();

        if (isActive) {
            $('#chatButton').addClass('active');
        } else {
            $('#chatButton').removeClass('active');
        }

        //Disable or enable any other help buttons
        $('.helpButton:not(#chatButton)').prop('disabled', isActive);
    }

    function postMessageToIframe(message) {
        $('#chatIframe')[0].contentWindow.postMessage(message, '*');
    }

    function repositionChat() {
        var existingChatDialog = $('#chatDialog').dialog("instance");
        
        if (existingChatDialog) {
            //We have a chat dialog showing, so update its height and reposition it
            $('#chatDialog').dialog("option", "position", {
                my: "right bottom",
                at: "left bottom",
                of: $('#chatButton')
            });
        }
    }

    function minimizeChatButtonClicked() {
        var $chatDialog = $('#chatDialog');
        var dialogOpen = $chatDialog.dialog('isOpen');
        if (dialogOpen) {
            $chatDialog.dialog('close');
            chatDialogHidden();
        }
    }

    function chatButtonClicked() {
        log.debug('chat button clicked');
        //Check if we have the chat dialog loaded
        var $chatDialog = $('#chatDialog');
        var existingChatDialog = $chatDialog.dialog("instance");

        if (existingChatDialog) {
            log.debug('dialog loaded');
            //Open or close dialog
            var dialogOpen = $chatDialog.dialog('isOpen');
            if (dialogOpen) {
                $chatDialog.dialog('close');
                chatDialogHidden();
            } else {
                $chatDialog.dialog('open');

                //Check chat status
                performStatusCheck(true);
            }
        } else {
            log.debug('dialog not loaded');
            if ($chatDialog.length) {
                //Chat dialog is loaded, but not initialized
                initDialog(true);
            } else {
                //Chat dialog isn't loaded
                loadChatDialog(true);
            }
        }
    }

    function loadChatIframe(apiToken) {
        //Load chat iframe (use saved session, if applicable)
        var iframe = $('#chatIframe');

        //See if we have a chat url saved
        var url = VisitPay.ChatSession.ChatUrl();
        if (!url) {
            log.debug('connecting chat iframe for first time');
            //Get base url
            url = urls.ChatIframeUrl;

            //Build up url params
            url += '?userToken=' + VisitPay.ChatSettings.GuarantorChatToken;
            url += '&apiToken=' + apiToken;
            url += '&threadId=' + VisitPay.ChatSession.ThreadId();
            url += '&firstName=' + VisitPay.ChatSettings.GuarantorFirstName;
            url += '&lastName=' + VisitPay.ChatSettings.GuarantorLastName;

            //Provide styling in URL
            var chatPanelBody = $('#chat-body');
            var styles = {
                header: {
                    visible: false
                },
                launcher: {
                    visible: false
                },
                window: {
                    width: chatPanelBody.width() + 'px',
                    height: chatPanelBody.height() + 'px',
                    bottom: '0px',
                    right: '0px'
                },
                theme: VisitPay.ChatSettings.ChatThemeColor
            };

            //Add context param
            var context = {
                styles: styles
            };
            url += '&context=' + JSON.stringify(context);
        }
        
        log.debug('using url for iframe: ', url);
        iframe.attr('src', url);

        //Save chat session info
        VisitPay.ChatSession.SetActiveSession(url);
    }

    function showThreadClosedMessage() {
        //Update title/status message
        $('#chatTitle').text(VisitPay.ChatSettings.ChatTitles.SessionEnded);
        $('#chat-status-message').text(VisitPay.ChatSettings.ChatStatusMessages.OperatorDisconnected);

        //Hide overlay, show iframe
        $('#chatIframeOverlay').css('display', 'block');
        $('#chatIframe').addClass('chat-iframe-hidden');
    }

    function showChatHideOverlay() {
        //Update title
        $('#chatTitle').text(VisitPay.ChatSettings.ChatTitles.NowChatting);

        //Hide overlay, show iframe
        $('#chatIframeOverlay').css('display', 'none');
        $('#chatIframe').removeClass('chat-iframe-hidden');
    }

    function performStatusCheck(dialogAlreadyOpen) {
        log.debug('perform status check, dialog open? ', dialogAlreadyOpen);
        var chatTitle = $('#chatTitle');
        var statusMessage = $('#chat-status-message');

        var activeSession = VisitPay.ChatSession.ActiveSession();
        if (activeSession && dialogAlreadyOpen) {
            //Dialog open and session is active, don't need to reload
            return;
        } else if (activeSession) {
            //We have an active session, so just reconnect it
            loadChatIframe();

            //If operator is already connected, go ahead and show the iframe
            if (VisitPay.ChatSession.IsOperatorConnected()) {
                showChatHideOverlay();
            } else {
                //Update title/status message to waiting for operator
                chatTitle.text(VisitPay.ChatSettings.ChatTitles.WaitingForOperator);
                statusMessage.text(VisitPay.ChatSettings.ChatStatusMessages.WaitingForOperator);
                $('#minimizeChatButton').show();
            }
        } else {
            //No active session yet, do all the status checking
            var statusPromise = VisitPay.ChatCommon.CheckChatStatus($('#chat-status-message'));
            statusPromise.done(function () {
                log.debug('status promise was successful, load iframe');

                //Chat is available, load iframe behind the scenes. This will send beacon and we can wait for operator.
                VisitPay.ChatSettings.ChatApiToken().done(function (apiToken) {
                    $.ajax({
                        url: urls.GenerateThreadId,
                        type: 'GET',
                        cache: false,
                        beforeSend: function (xhr) {
                            xhr.setRequestHeader('Authorization', 'Bearer ' + apiToken);
                        },
                        success: function (threadId) {
                            //Save the generated threadId so we can use it when connecting to iframe
                            VisitPay.ChatSession.SetThreadId(threadId);
                            loadChatIframe(apiToken);
                        }
                    });
                });

                //Update title/status message
                chatTitle.text(VisitPay.ChatSettings.ChatTitles.WaitingForOperator);
                statusMessage.text(VisitPay.ChatSettings.ChatStatusMessages.WaitingForOperator);
                $('#minimizeChatButton').show();
            }).fail(function () {
                log.warn('status promise failed. chat not available.');
                $('#need-help-now').show();
                chatTitle.text(VisitPay.ChatSettings.ChatTitles.ChatUnavailable);
            });
        }
    }

    function loadChatDialog(openAfterLoad) {
        $.get('/Chat/ChatDialog', function (html) {
            //Add dialog to the page
            $('body').append(html);
            
            //Listen for button clicks
            $('#goToHelp').click(function (e) {
                e.preventDefault();

                closeChat();

                //Go to help
                var url = window.location.origin;
                url = url + urls.HelpCenter;
                window.location.href = url;
            });

            //Check chat status
            performStatusCheck();

            //Initialize the dialog
            initDialog(openAfterLoad);
        });
    }

    function closeChat() {
        //End session and close chat dialog
        $("#chatDialog").dialog('close');
        VisitPay.ChatSession.EndSession();
        chatDialogClosed();
    }

    function endChat() {
        if (VisitPay.ChatSession.OperatorEndedThread()) {
            //No confirmation needed, end session and close chat dialog
            closeChat();
        } else if (VisitPay.ChatSession.ActiveSession()) {
            //Confirm user wants to end the thread
            promptToEndChat();
        } else {
            //No active session, allow close
            closeChat();
        }
    }

    function promptToEndChat() {
        var promptPromise = $.Deferred();

        var title = VisitPay.ChatSettings.ChatPrompts.EndSessionTitle;
        var message = VisitPay.ChatSettings.ChatPrompts.EndSessionMessage;
        var cancel = VisitPay.ChatSettings.ChatPrompts.Cancel;
        var confirmationPromise = VisitPay.Common.ModalGenericConfirmation(title, message, title, cancel);
        confirmationPromise.done(function () {
            //Tell iframe we are ready to end chat
            log.warn('sending endChatRequest to iframe');
            postMessageToIframe('endChatRequest');

            promptPromise.resolve();
        }).fail(function() {
            promptPromise.reject();
        });
        
        return promptPromise;
    }

    function initDialog(autoOpen) {
        $('#chatDialog').dialog({
            autoOpen: autoOpen,
            position: {
                my: 'right bottom',
                at: 'left bottom',
                of: $('#chatButton')
            },
            draggable: false,
            resizable: false,
            height: 450,
            width: 350,
            classes: {
                'ui-dialog': 'chat-dialog',
                'ui-dialog-titlebar': 'chat-dialog-hidden-titlebar',
                'ui-dialog-content': 'chat-dialog-content'
            },
            open: chatDialogOpened,
            show: true, //Would like a slide up animation here or something
            hide: true
        });

        //Setup dialog listeners
        $('#endChatButton').click(function () {
            endChat();
        });
        $('#minimizeChatButton').click(function () {
            minimizeChatButtonClicked();
        });
    }

    function setupIframeEventListeners() {
        //Listen for events from chat iframe
        window.addEventListener('message', function (event) {
            log.debug('Chat.js received event: ', event, event.data);
            
            if (event) {
                if (event.data === 'chatClosed') {
                    //End session and close chat dialog
                    closeChat();

                    //Check if session was ending, logout if so
                    if (VisitPay.ChatSession.ShouldLogout()) {
                        //Session was ending, perform logout
                        var logoutUrl = VisitPay.ChatSession.LogoutRedirect();
                        VisitPay.ChatSession.ClearLogoutRedirect();
                        window.location.href = logoutUrl;
                    }
                } else if (event.data === 'userConnected') {
                    //Operator is connected, show chat if it isn't already showing
                    if (!VisitPay.ChatSession.IsOperatorConnected()) {
                        VisitPay.ChatSession.SetOperatorConnected(true);
                        showChatHideOverlay();

                        //Tell iframe we are ready to chat
                        log.warn('sending guarantor ready to chat event to iframe');
                        postMessageToIframe('guarantorReady');
                    }
                } else if (event.data === 'chatThreadClosed') {
                    log.debug('chatThreadClosed event received from iframe');
                    //Let guarantor know operator ended thread
                    showThreadClosedMessage();

                    //End the session
                    VisitPay.ChatSession.EndSession();

                    //Flag the session so we don't have to prompt user when they close the chat dialog
                    VisitPay.ChatSession.SetOperatorConnected(false);
                    VisitPay.ChatSession.SetOperatorEndedThread();

                    $('#minimizeChatButton').hide();
                }
            }
        });
    }

    $(document).ready(function () {
        var activeSession = VisitPay.ChatSession.ActiveSession();
        if (activeSession) {
            log.debug('session is active');

            var chatShowing = VisitPay.ChatSession.ChatIsShowing();
            loadChatDialog(chatShowing);
            $('#chatButton').addClass('pulse-button');
        }

        $('#chatButton').click(function (e) {
            e.preventDefault();
            chatButtonClicked();
        });

        $(document).scroll(function () {
            repositionChat();
        });

        $(window).resize(function () {
            repositionChat();
        });

        $(window).on('SessionTimeout', function (event, logoutUrl) {
            log.debug('chat.js received SESSION TIMEOUT event: ', logoutUrl);

            //Store the logout url
            VisitPay.ChatSession.SetLogoutRedirect(logoutUrl);

            //Tell iframe we are ready to end chat, it will post back when done and user will be logged out
            log.warn('SESSION TIMEOUT :: sending endChatRequest to iframe');
            postMessageToIframe('endChatRequest');
        });

        $('#btnNavSignout').click(function (e) {
            log.debug('LOG OUT BUTTON CLICKED: ', $(e.target).attr('href'));
            
            if (VisitPay.ChatSession.ActiveSession()) {
                log.debug('had active session on logout, check if user wants to end');
                e.preventDefault();

                //Save logout URL
                VisitPay.ChatSession.SetLogoutRedirect($(e.target).attr('href'));

                var confirmEndThreadPromise = promptToEndChat();
                confirmEndThreadPromise.done(function () {
                    //User wants to end their session and log out. The prompt to end will send message to iframe to close the session.
                    //Callback from iframe will log user out afterwards.
                    $.blockUI();
                }).fail(function() {
                    //User doesn't want to end their chat session, don't logout
                    VisitPay.ChatSession.ClearLogoutRedirect();
                });
            }
        });
        
        //Listen for events from chat iframe
        setupIframeEventListeners();
    });

})(jQuery);
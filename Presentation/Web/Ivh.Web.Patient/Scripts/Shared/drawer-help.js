﻿window.VisitPay.HelpWidget = (function($container) {
    
    var urls = {
        HelpCenterPartial: '/KnowledgeBase/HelpCenterDrawer',
        SupportRequestsData: '/Support/MySupportRequests',
        SupportRequestsPartial: '/Support/SupportRequestDrawer',
        SupportRequestsUnreadCount: '/Support/SupportRequestUnreadCount'
    };
    var gridModel;
    
    function loadHelpPartialView() {
        var promise = $.Deferred();

        $.get(urls.HelpCenterPartial, function (html) {
            promise.resolve(html);
        });

        return promise;
    };
    
    function loadSupportRequestsPartialView() {
        var promise = $.Deferred();

        $.get(urls.SupportRequestsPartial, {}, function (html) {
            promise.resolve(html);
        });

        return promise;
    };
    
    function triggerChange(count) {
        var promise = $.Deferred();
        if (count == null) {
            $.get(urls.SupportRequestsUnreadCount, function(data) {
                promise.resolve({ count: data.Count });
            });
        } else {
            promise.resolve({
                count: count
            });
        }

        promise.done(function(obj) {
            $container.trigger('drawer:change', obj);
        });
    };

    // send initial change to drawer
    triggerChange();

    // global support request events
    var events = ['supportRequestActionCompleted.sidebar', 'supportRequestCreated.sidebar', 'supportRequestChanged.sidebar'];
    for (var i = 0; i < events.length; i++) {
        $(document).off(events[i]).on(events[i], function() {
            if (gridModel != null) {
                // this will trigger 'triggerChange' when it finishes
                gridModel.Refresh();
            } else {
                // no model - just get the counts
                triggerChange();
            }
        });
    }

    return {
        LoadContent: function () {
                
            var $promise = $.Deferred();
                
            var helpPromise = loadHelpPartialView();
            var supportRequestsPartialViewPromise = loadSupportRequestsPartialView();
            gridModel = window.VisitPay.VpGrid(function() { return { supportRequestStatus: 1 }; }, urls.SupportRequestsData, 25, 'InsertDate', 'desc');

            // extend model with click event
            gridModel.OnSupportRequestClick = function (item) {
                $.blockUI();
                window.VisitPay.SupportRequest.Initialize($('#modalSupportRequestContainer'), ko.unwrap(item.SupportRequestId));
            };
                
            $.when(helpPromise, supportRequestsPartialViewPromise, gridModel.Search(false)).done(function (helpWidgetPartialView, supportRequestPartialView) {
                    
                // reset
                $container.html('');

                // help
                $container.append(helpWidgetPartialView);
                $container.on('click.question', '.question', function () {
                    $(this).toggleClass('active');
                    var $answer = $(this).siblings('.answer:eq(0)');
                    if ($answer.is(':visible')) {
                        $answer.slideUp(150);
                    } else {
                        $answer.slideDown(150);
                    }
                });
                    
                // support requests
                $container.append(supportRequestPartialView);
                ko.cleanNode($container);
                ko.applyBindings(gridModel, $container[0]);
                    
                // trigger change after updates
                gridModel.Data.SupportRequestsWithUnreadMessages.subscribe(function(newValue) {
                    triggerChange(newValue);
                });

                // trigger initial update
                triggerChange(ko.unwrap(gridModel.Data.SupportRequestsWithUnreadMessages));
                    
                $promise.resolve();
            });

            return $promise;
        }
    };
});
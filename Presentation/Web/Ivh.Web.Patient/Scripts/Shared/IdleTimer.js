﻿// JQMIGRATE: jQXHR.success is deprecated and removed

(function ($) {
    if (window.VisitPay.State.IsUserLoggedIn) {
        var userIdleTime = 0;
        var baseIncrement = window.VisitPay.State.IsEmulation ? 10000 : 30000; //10 sec if emulating else 30 sec
        var incrementsPerMinute = Math.ceil(60000 / baseIncrement);
        //30 seconds before logout
        var incrementWarning = (window.VisitPay.ClientSettings.SessionTimeoutInMinutes * incrementsPerMinute) - Math.ceil((incrementsPerMinute / 2));
        var incrementLogout = (window.VisitPay.ClientSettings.SessionTimeoutInMinutes * incrementsPerMinute);
        var dialogBox = null;
        var idleInterval = null;
        var countdownInterval = null;
        var sessionTimeoutEvent = 'SessionTimeout';


        function logout(url) {
            if (VisitPay.ChatSession && VisitPay.ChatSession.ActiveSession()) {
                //Let chat handle logout
                $(window).trigger(sessionTimeoutEvent, [url]);
            } else {
                window.location.href = url;
            }
        }

        function countdownIncrement() {

            var secondsRemaining = (incrementLogout * 60 / incrementsPerMinute) - (incrementWarning * 60 / incrementsPerMinute);
            var element = dialogBox.find('#timeRemaining');
            element.text(secondsRemaining);

            countdownInterval = window.setInterval(function () {

                if (secondsRemaining === 1) {
                    window.clearInterval(countdownInterval);
                    logout(window.VisitPay.Urls.LogoffUrl + '?timeout=true');
                }

                secondsRemaining--;
                element.text(secondsRemaining);

            }, 1000);

        }

        function timerIncrement() {
            userIdleTime = userIdleTime + 1;

            //console.log('idle for ' + (userIdleTime * 30) + ' seconds, warning at ' + ((incrementWarning + 1) * 30) + ' seconds');

            if (userIdleTime >= incrementWarning) {
                if (dialogBox !== null && !dialogBox.data('bs.modal').isShown) {
                    dialogBox.modal('show');
                    countdownIncrement();
                }
            }

            if (window.VisitPay.State.IsEmulation) {
                $.post(window.VisitPay.Urls.VerifyEmulation, { vpUserId: window.VisitPay.State.VpUserId }, function (data) {
                    if (data != null && data.length > 0) {
                        window.location.href = data;
                    }
                }).fail(function (jqxhr) {
                    if (jqxhr.status === 401 || jqxhr.status === 500) {
                        logout('/account/logoff');
                    }
                });
            }

            //Make sure the session doesnt expire if the user is doing things. If 401 then session is dead, redirect to logoff
            $.ajax(window.VisitPay.Urls.UpdateSession, { cache: false }).fail(function(jqxhr) {
                if (jqxhr.status === 401) {
                    logout('/account/logoff');
                }
            }).done(function (result, status, request) {
                var response = JSON.parse(request.getResponseHeader('X-Responded-JSON'));
                if (response && response.status && response.status === 401) {
                    logout('/account/logoff');
                }
            });
        }

        function timerReset() {
            userIdleTime = 0;
            if (dialogBox !== null) {
                dialogBox.modal('hide');
                window.clearInterval(countdownInterval);
            }
        }

        function continueClicked() {
            timerReset();
        }

        $(document).ready(function () {
            //Increment the idle time counter every minute.
            idleInterval = setInterval(timerIncrement, baseIncrement);
            dialogBox = $("#TimeoutWarning");
            dialogBox.modal('hide');
            dialogBox.find(".btn-primary").click(continueClicked);

            //Zero the idle timer on mouse movement.
            $(this).mousemove(function (e) {

                if (dialogBox !== null && !dialogBox.data('bs.modal').isShown) {
                    timerReset();
                }
            });
            $(this).keypress(function (e) {

                if (dialogBox !== null && !dialogBox.data('bs.modal').isShown) {
                    timerReset();
                }
            });
        });
    }
})(jQuery);
﻿window.VisitPay.ResourceKeys = {
    RegisterStepLabelRecords: 'RegisterStepLabelRecords',
    RegisterStepLabelAccount: 'RegisterStepLabelAccount',
    RegisterStepLabelTerms: 'RegisterStepLabelTerms',
    GuarantorEnrollmentProviderDelayText: 'GuarantorEnrollmentProviderDelayText'
};

window.VisitPay.ResourceManager = function (resourceKeys) {

    var resources = [];

    function checkKey(resourceKey) {
        if (!resourceKeys.hasOwnProperty(resourceKey)) {
            console.error('invalid resource key', resourceKey);
            return false;
        }
        return true;
    }

    return {
        get: function (resourceKey) {

            if (!checkKey(resourceKey)) {
                console.error('invalid resource key', resourceKey);
                return '';
            }

            return resources[resourceKey] || '';
        },
        set: function (resourceKey, value) {

            if (!checkKey(resourceKey)) {
                console.error('invalid resource key', resourceKey);
                return;
            }

            resources[resourceKey] = he.decode(value);
        }
    };

}(window.VisitPay.ResourceKeys);
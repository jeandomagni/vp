﻿(function () {

    function onErrors(event, validator) { // 'this' is the form element
        var container = $(this).find('[data-valmsg-summary=true]');
        var list = container.find('ul');

        if (list && list.length && validator.errorList.length) {
            list.empty();
            container.addClass('validation-summary-errors').removeClass('validation-summary-valid');
            $.each(validator.errorList, function () {
                //Do not show error messages for required fields, only highlight the field red
                if (this.method !== 'required' && this.method !== 'requiredif') {
                    $('<li />').html(this.message).appendTo(list);
                }
            });

            //If there are no errors in the list, don't show the validation summary
            if (list[0] && list[0].children.length === 0) {
                container.removeClass('validation-summary-errors').addClass('validation-summary-valid');
            }
        }
    };

    $.validator.unobtrusive.options = {
        invalidHandler: onErrors
    }

})();
﻿Vue.directive('numbers', {
    inserted: function (el, props) {
        if (props !== undefined && props !== null && props.value !== undefined && props.value !== null && !isNaN(props.value)) {
            $(el).numbersOnly(props.value);
        } else {
            $(el).numbersOnly();
        }
    }
});

Vue.directive('zipcode', {
    inserted: function (el) {
        $(el).zipCode();
    }
});

Vue.directive('ssn4', {
    inserted: function (el) {
        $(el).numbersOnly(4);
    }
});

Vue.directive('show-password', {
    inserted: function(el) {
        $(el).switchPassword();
    }
});

Vue.directive('unique-selection', {
    inserted: function (el) {

        var nameParts = ($(el).attr('name') || '').split('.');
        var name = nameParts[nameParts.length - 1] || '';

        if (name.length === 0) {
            return;
        }

        $(el).off('change.unique').on('change.unique', function () {

            var $elements = $('select[name$=".' + name + '"]');
            $elements.each(function () {
                var $element = $(this);
                $element.find('option').prop('disabled', false);
                $elements.not($element).each(function () {
                    if ($(this).val().length > 0) {
                        $element.find('option[value="' + $(this).val() + '"]').prop('disabled', true);
                    }
                });
            });

        });

    }
});

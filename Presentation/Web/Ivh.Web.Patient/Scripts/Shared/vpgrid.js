﻿window.VisitPay.VpGrid = (function(filter, searchUrl, rows, sortField, sortOrder, overrideExportFilter) {

    var self = {};

    function getFilter(targetFilter) {
        targetFilter = targetFilter || filter;
        var currentFilter = {};
        if (targetFilter !== undefined && targetFilter !== null && targetFilter !== {}) {
            if ($.isFunction(targetFilter)) {
                currentFilter = targetFilter();
            } else {
                currentFilter = targetFilter;
            }
        }
        return currentFilter;
    };

    function getPage() {
        return ko.unwrap(self.Data.page || 1);
    };

    function getRecords() {
        return ko.unwrap(self.Data.records || 0);
    }

    function getData(page, block) {

        block = block === undefined || block === null ? true : block;

        if (block === true) {
            $.blockUI();
        }

        var promise = $.Deferred();
        $.post(searchUrl, { model: getFilter(), page: page, rows: ko.unwrap(self.Rows), sidx: ko.unwrap(self.SortField), sord: ko.unwrap(self.SortOrder) }, function (data) {
            ko.mapping.fromJS(data, {}, self.Data);
            if (block === true) {
                $.unblockUI();
            }
            promise.resolve();
        });

        return promise;
    };

    self.Data = {
        Results: ko.observableArray([]),
        page: ko.observable(),
        records: ko.observable(),
        total: ko.observable()
    };
    self.GetData = getData;

    self.Rows = ko.observable(rows || 20);
    self.SortField = ko.observable(sortField);
    self.SortOrder = ko.observable(sortOrder);

    self.Records = ko.computed(function() {
        return getRecords();
    });

    self.HasResults = ko.computed(function() {
        return getRecords() > 0;
    });

    self.Refresh = function(block) {
        return getData(getPage(), block);
    };
    self.Search = function (block) {
        return getData(1, block);
    };

    self.Prev = function(block) {
        return getData(Math.max(getPage() - 1, 1), block);
    };
    self.Prev.IsEnabled = ko.computed(function() {
        return getPage() > 1;
    });

    self.Next = function(block) {
        return getData(getPage() + 1, block);
    };
    self.Next.IsEnabled = ko.computed(function() {
        return getPage() < Math.ceil(getRecords() / Math.max(ko.unwrap(self.Rows), 1));
    });

    self.PageText = ko.computed(function() {

        var totalRecords = getRecords();
        var firstRecord = totalRecords === 0 ? 0 : getPage() * ko.unwrap(self.Rows) - (ko.unwrap(self.Rows) - 1);
        var lastRecord = Math.min(firstRecord + (ko.unwrap(self.Rows) - 1), totalRecords);

        return 'Showing ' + firstRecord + ' - ' + lastRecord + ' of ' + totalRecords;

    });
    self.PageText.IsVisible = ko.computed(function() {
        return ko.unwrap(self.Prev.IsEnabled) || ko.unwrap(self.Next.IsEnabled);
    });

    self.Export = function(exportUrl) {

        var url = typeof (exportUrl) === 'string' ? exportUrl : (searchUrl + 'Export');
        var exportFilter = overrideExportFilter !== undefined && overrideExportFilter !== null && overrideExportFilter !== {} ? overrideExportFilter : getFilter();

        $.post(url, {
            Filter: getFilter(exportFilter),
            sidx: ko.unwrap(self.SortField),
            sord: ko.unwrap(self.SortOrder)
        }, function(result) {
            if (result.Result === true) {
                window.location.href = result.Message;
            }
        }, 'json');
    }
    
    return self;

});

window.VisitPay.VpGridCommon = (function () {

    function withLazyGrid(vm) {
        
        vm.Load = {};
        vm.Load.IsLoaded = ko.observable(false);
        vm.Load.IsLoading = ko.observable(false);
        vm.Load.ShouldLoad = ko.computed(function () {
            return !ko.unwrap(vm.Load.IsLoading) && !ko.unwrap(vm.Load.IsLoaded);
        });
        vm.Load.Start = function (promise) {
            vm.Load.IsLoading(true);
            promise.done(function () {
                vm.Load.IsLoading(false);
                vm.Load.IsLoaded(true);
            });
        };
        
        vm.IsExpanded = ko.observable(false);
        vm.IsExpanded.Ready = ko.computed(function () {
            return ko.unwrap(vm.IsExpanded) && ko.unwrap(vm.Load.IsLoaded);
        });
        vm.IsExpanded.Toggle = function () {
            vm.IsExpanded(!ko.unwrap(vm.IsExpanded));
        }

        return self;

    };

    return {
        WithLazyGrid: withLazyGrid
    }

})();

(function () {

    $(document).ready(function() {

        $(document).off('click.vpgridrow').on('click.vpgridrow', '.vpgrid-clickable > .grid-row', function(e) {
            
            e.preventDefault();
            var $target = $(this).next('.grid-detail');
            var isVisible = $target.is(':visible');
            if (isVisible) {
                $(this).removeClass('active');
                $target.removeClass('active');
            } else {
                $(this).addClass('active');
                $target.addClass('active');
            }
            
            $(this).trigger('gridrowclick', {
                sender: $(this),
                target: $target,
                isVisible: !isVisible
            });

        });

        $(document).off('click.vpgridlink').on('click.vpgridlink', '.vpgrid-link > .grid-row', function(e) {
            e.preventDefault();
            $(this).trigger('gridlinkclick', {
                sender: $(this)
            });
        });

    });

})();
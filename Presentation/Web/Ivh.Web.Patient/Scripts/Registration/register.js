﻿(function(apps, urls, clientSettings) {

    $(document).ready(function() {
        $('#modalRegisterStep0').modal('show');

        // open legal in new tab
        $('#footer #aLegal').attr('target', '_blank');

        // open links from legal agreements in a new window
        $(document).on('shown.bs.modal.registration', function (e) {
            $(e.target).find('#section-esign a, #section-terms a').attr('target', '_blank');
        });

        // sample statement
        $(document).on('click.samplestatement', '#sampleStatement', function (e) {
            e.preventDefault();
            $('#modalSampleStatement').modal('show');
        });

        // app
        var registrationApp = new apps.RegistrationApp('#section-register');
        registrationApp.$on('account-ready', function() {

            $.get(urls.registerPaymentMethodsListPartial + '?allowSelection=false', function(html) {

                // payment methods
                var $paymentMethods = $('#section-paymentmethods');
                $paymentMethods.html(html);

                var model = { PaymentMethods: new window.VisitPay.paymentMethodsViewModel() };
                ko.applyBindings(model, $paymentMethods[0]);

                var btns = [$paymentMethods.find('#btnBankAccountAdd'), $paymentMethods.find('#btnCardAccountAdd')];
                $.each(btns, function() {
                    this.removeClass('btn-primary').addClass('btn-accent btn-outline');
                    this.text(this.text().replace('+', '').trim());
                });
                $paymentMethods.show();

                window.VisitPay.PaymentMethods(model, $paymentMethods, {
                    urlPaymentMethod: urls.registerPaymentMethod,
                    urlPaymentMethodBank: urls.registerPaymentMethodBank,
                    urlPaymentMethodCard: urls.registerPaymentMethodCard,
                    urlPaymentMethodsList: urls.registerPaymentMethodsList,
                    urlRemovePaymentMethod: urls.registerRemovePaymentMethod,
                    urlSavePaymentMethodBank: urls.registerSavePaymentMethodBank,
                    urlSavePaymentMethodCard: urls.registerSavePaymentMethodCard,
                    urlSetPrimary: urls.registerSetPrimary
                });

                // piwik
                $(document).on('click.registration', '#btnBankAccountAdd', function () {
                    window._paq.push(['trackEvent', 'Registration', 'AddPaymentMethod']);
                });
                $(document).on('click.registration', '#btnCardAccountAdd', function () {
                    window._paq.push(['trackEvent', 'Registration', 'AddPaymentMethod']);
                });

            });

        });

        function showGoToApplicationButton() {
            $('#btnGoToApplication').css('visibility', 'visible');
            $('#step4spinner').hide();
        }

        registrationApp.$on('registration-processing-complete', function(secondsToWait) {
            window.VisitPay.Common.ModalAsync($('#modalRegisterSummary'), '', urls.registrationSummary, 'GET', null, function () {
                $.unblockUI();
                var retryInterval = 1000;
                var retryCodes = [204];
                $.get(urls.registrationSummaryHasStatement)
                    .retry({ times: secondsToWait, timeout: retryInterval, statusCodes: retryCodes }) // retry if NoContent returned
                    .always(showGoToApplicationButton);
            });
        });
    });

})(window.VisitPay.Apps, window.VisitPay.Urls, window.VisitPay.ClientSettings);
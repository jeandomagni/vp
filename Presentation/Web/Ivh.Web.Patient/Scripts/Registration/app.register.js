﻿(function(apps, components, resourceManager, resourceKeys, urls) {

    var registerRecordsComponent = (function() {

        var promise = $.Deferred();
        var promiseHtml = $.Deferred();
        var promiseModel = $.Deferred();

        $.when(promiseHtml, promiseModel).done(function(html, model) {

            promise.resolve({
                template: html,
                data: function() {
                    return model;
                },
                methods: {
                    onSubmit: function(e) {

                        e.preventDefault();

                        var $form = $(e.target);
                        $form.parseValidation();

                        if (!$form.valid()) {
                            return;
                        }

                        $.blockUI();

                        var self = this;
                        $.post($form.attr('action'), { model: this.$data }, function(result) {

                            if (result.Result === true) {
                                self.$emit('records-complete', self.$data);
                            } else {
                                $form.addCustomErrors(result.Message);
                                $.unblockUI();
                            }

                        });

                    }
                }
            });

        });

        $.get(urls.registerRecordsPartial, function(html) {
            promiseHtml.resolve(html);
        });

        $.post(urls.registerRecordsModel, function (model) {
            promiseModel.resolve(model);
        });

        return promise;

    });

    var registerAccountComponent = (function() {

        var promise = $.Deferred();
        var promiseHtml = $.Deferred();
        var promiseModel = $.Deferred();

        $.when(promiseHtml, promiseModel).done(function(html, model) {

            promise.resolve({
                template: html,
                data: function() {
                    return model;
                },
                watch: {
                    Email: function(value) {
                        if (this.$data.UseEmailAsUsername === true) {
                            this.$data.Username = value;
                        }
                    },
                    UseEmailAsUsername: function(value) {
                        if (value === false) {
                            window._paq.push(['trackEvent', 'Registration', 'DoNotUseEmailForUsername']);
                            this.$data.Username = '';
                        } else {
                            window._paq.push(['trackEvent', 'Registration', 'UseEmailForUsername']);
                            this.$data.Username = this.$data.Email;
                        }
                    }
                },
                created: function() {
                    this.$nextTick(function() {
                        this.$emit('account-ready');
                    });
                },
                methods: {
                    onSubmit: function(e) {

                        e.preventDefault();

                        var $form = $(e.target);
                        $form.parseValidation();

                        if (!$form.valid()) {
                            return;
                        }

                        $.blockUI();

                        var self = this;
                        $.post($form.attr('action'), { model: this.$data }, function(result) {

                            if (result.Result === true) {
                                self.$emit('account-complete', self.$data);
                            } else {
                                $form.addCustomErrors(result.Message);

                                var errorKeys = Object.keys(result.Message || {});

                                // if username is in the errors list, make sure it's visible
                                if (errorKeys.indexOf('Username') > -1) {
                                    if (self.$data.UseEmailAsUsername === true) {
                                        self.$data.UseEmailAsUsername = false;
                                    }
                                }

                                $.unblockUI();
                            }

                        });
                    }
                }
            });

        });

        $.get(urls.registerAccountPartial, function(html) {
            promiseHtml.resolve(html);
        });

        $.post(urls.registerAccountModel, function(model) {
            $.extend(model, {
                UseEmailAsUsername: true,
                PasswordType: 'password',
                PaymentDueDay: ''
            });
         if (model.SecurityQuestions) {
             for (var i = 0; i < model.SecurityQuestions.length; i++) {
                 model.SecurityQuestions[i].SecurityQuestionId = '';
             }
         }
            promiseModel.resolve(model);
        });

        return promise;

    });

    var registerTermsComponent = (function() {

        var promise = $.Deferred();
        var promiseHtml = $.Deferred();
        var promiseModel = $.Deferred();

        $.when(promiseHtml, promiseModel).done(function(html, model) {

            promise.resolve({
                template: html,
                data: function() {
                    return model;
                },
                methods: {
                    onSubmit: function(e) {

                        e.preventDefault();

                        var $form = $(e.target);
                        $form.parseValidation();

                        if (!$form.valid()) {
                            return;
                        }

                        this.$emit('terms-complete', { model: this.$data, form: $form });

                    }
                }
            });

        });

        $.get(urls.registerTermsPartial, function(html) {
            promiseHtml.resolve(html);
        });

        $.post(urls.registerTermsModel, {}, function(model) {
            promiseModel.resolve(model);
        });

        return promise;

    });

    apps.RegistrationApp = (function(selector, routerMode) {

        // setup router
        var routes = [
            {
                name: 'records',
                path: '/records',
                meta: { label: resourceManager.get(resourceKeys.RegisterStepLabelRecords) },
                component: registerRecordsComponent
            },
            {
                name: 'account',
                path: '/account',
                meta: { label: resourceManager.get(resourceKeys.RegisterStepLabelAccount) },
                component: registerAccountComponent
            },
            {
                name: 'terms',
                path: '/terms',
                meta: { label: resourceManager.get(resourceKeys.RegisterStepLabelTerms) },
                component: registerTermsComponent
            }
        ];
        var router = new VueRouter({
            mode: routerMode || 'hash',
            routes: routes
        });
        router.afterEach(function(to, from) {
            $.unblockUI();
        });

        // app
        var app = new Vue({
            el: selector,
            router: router,
            components: {
                'steps-component': components.StepsComponent
            },
            data: function() {
                return {
                    model: {
                        Account: {},
                        PersonalInformation: {},
                        Terms: {}
                    }
                }
            },
            created: function() {
                window._paq.push(['enableHeartBeatTimer']);
            },
            methods: {
                onRecordsComplete: function(model) {

                    window._paq.push(['trackEvent', 'Registration', 'PersonalInformation']);
                    this.model.PersonalInformation = model;
                    this.nextStep('/account');

                },
                onAccountReady: function() {
                    this.$emit('account-ready');
                },
                onAccountComplete: function(model) {

                    window._paq.push(['trackEvent', 'Registration', 'AccountSetup']);
                    this.model.Account = model;
                    this.nextStep('/terms');

                },
                onTermsComplete: function(parameters) {

                    this.model.Terms = parameters.model;

                    var message = 'Please wait';
                    if (window.VisitPay != null && window.VisitPay.Localization != null) {
                        message = window.VisitPay.Localization.PleaseWait;
                    }

                    $.blockUI({
                        blockMsgClass: 'blockMsg blockMsg-solid',
                        message: '<h1 style="line-height: 32px;"><img src="/Content/Images/loading.gif" alt="" /> &nbsp; ' + message + '...</h1><div>' + resourceManager.get(resourceKeys.GuarantorEnrollmentProviderDelayText) + '</div>'
                    });

                    var self = this;
                    $.post(parameters.form.attr('action'), { model: this.model }, function(result) {

                        if (result.Result === true) {

                            var referrer = result.Message.length > 1 ? result.Message[1] : '';
                            window._paq.push(['trackEvent', 'Registration', 'CompleteRegistration', referrer]);

                            var guarantorEnrollmentProviderDelaySeconds = result.Message.length > 0 ? parseInt(result.Message[0]) : 0;
                            self.$emit('registration-processing-complete', guarantorEnrollmentProviderDelaySeconds);

                        } else {
                            parameters.form.addCustomErrors(result.Message);
                            $.unblockUI();
                        }

                    });

                },
                nextStep: function(path) {

                    // mark step complete
                    this.$route.meta.isComplete = true;
                    // next
                    this.$router.push(path);

                }
            }
        });

        router.replace(routes[0].path);

        return app;

    });

})(window.VisitPay.Apps, window.VisitPay.Components, window.VisitPay.ResourceManager, window.VisitPay.ResourceKeys, window.VisitPay.Urls);
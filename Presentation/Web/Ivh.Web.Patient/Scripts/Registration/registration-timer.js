﻿(function () {

    var userIdleTime = 0;
    var sessionIdleTime = 0;
    var baseIncrement = 30000; //30sec
    //30 seconds before logout
    var incrementWarning = (window.VisitPay.ClientSettings.SessionTimeoutInMinutes * 2) - 1;
    var incrementLogout = (window.VisitPay.ClientSettings.SessionTimeoutInMinutes * 2);
    var dialogBox = null;
    var idleInterval = null;
    var countdownInterval = null;

    function countdownIncrement() {

        var secondsRemaining = (incrementLogout * 60) - (incrementWarning * 60);
        var element = dialogBox.find('#timeRemaining');
        element.text(secondsRemaining);

        countdownInterval = window.setInterval(function () {

            if (secondsRemaining === 1) {
                window.clearInterval(countdownInterval);
                window.location.pathname = '/';
            }

            secondsRemaining--;
            element.text(secondsRemaining);

        }, 1000);

    }

    function timerIncrement() {
        userIdleTime = userIdleTime + 1;
        sessionIdleTime = sessionIdleTime + 1;

        if (userIdleTime > incrementWarning) { // 20 minutes
            if (dialogBox !== null && !dialogBox.data('bs.modal').isShown) {
                dialogBox.modal('show');
                countdownIncrement();
            }
        }

        if (sessionIdleTime > incrementLogout) {
            //Make sure the session doesnt expire if the user is doing things.
            sessionIdleTime = 0;
            $.ajax(window.VisitPay.Urls.UpdateSession);
        }
    }

    function timerReset() {
        userIdleTime = 0;
        if (dialogBox !== null) {
            dialogBox.modal('hide');
            window.clearInterval(countdownInterval);
        }
    }

    function continueClicked() {
        timerReset();
    }

    $(document).ready(function () {

        //Increment the idle time counter every minute.
        idleInterval = setInterval(timerIncrement, baseIncrement);
        dialogBox = $("#RegistrationTimeoutWarning");
        dialogBox.modal('hide');
        dialogBox.find(".btn-primary").click(continueClicked);

        //Zero the idle timer on mouse movement.
        $(this).mousemove(function (e) {
            if (dialogBox !== null && !dialogBox.data('bs.modal').isShown) {
                timerReset();
            }
        });
        $(this).keypress(function (e) {
            if (dialogBox !== null && !dialogBox.data('bs.modal').isShown) {
                timerReset();
            }
        });
    });

})();
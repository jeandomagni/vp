﻿(function(visitPay, $, ko) {

    visitPay.PaymentMethodsConsolidation = function() {

        var viewModel = function() {

            var self = this;

            self.PaymentMethods = new visitPay.paymentMethodsViewModel();
            self.PaymentMethods.HasPrimaryPaymentMethod = ko.computed(function() {
                return self.PaymentMethods.PrimaryPaymentMethod() != null;
            });
            self.IsSetPrimaryEnabled = ko.observable(true);
            self.EditPrimary = ko.observable(false);
            self.PrimaryPaymentMethod = ko.computed(function () {
                return ko.unwrap(self.PaymentMethods.PrimaryPaymentMethod());
            });

            return self;
        };

        var model;
        var $scopeElement;

        function bindPaymentMethods() {

            return visitPay.PaymentMethods(model, $('#section-paymentmethods'), {});

        };

        function addBankAccountPrimaryClick(e) {
            e.preventDefault();
            model.EditPrimary(false);
            $('#section-paymentmethods').find('[id^="btnBankAccountAdd"]').click();
        };

        function addCardAccountPrimaryClick(e) {
            e.preventDefault();
            model.EditPrimary(false);
            $('#section-paymentmethods').find('[id^="btnCardAccountAdd"]').click();
        };

        function changePrimaryClick() {

            model.EditPrimary(true);

            var paymentMethods = $scopeElement.find('#section-paymentmethods');

            paymentMethods.hide();
            paymentMethods.slideDown(350, function() {
                $(this).removeAttr('style');
            });

            $('html,body').animate({ scrollTop: $(document).height() }, 350);

        };

        return {
            init: function (primaryPaymentMethod) {
                //
                $scopeElement = $('section.payment-methods-consolidation');
                model = new viewModel();
                ko.applyBindings(model, $scopeElement[0]);

                bindPaymentMethods().then(function() {

                    $scopeElement.on('click', '#btnAddBankAccountPrimary', addBankAccountPrimaryClick);
                    $scopeElement.on('click', '#btnAddCardAccountPrimary', addCardAccountPrimaryClick);
                    $scopeElement.on('click', '#btnChangePrimary', changePrimaryClick);

                    primaryPaymentMethod(ko.unwrap(model.PaymentMethods.PrimaryPaymentMethod));

                    model.PaymentMethods.PrimaryPaymentMethod.subscribe(function (value) {
                        primaryPaymentMethod(ko.unwrap(model.PaymentMethods.PrimaryPaymentMethod));
                        if (ko.unwrap(model.EditPrimary) === true) {
                            var paymentMethods = $scopeElement.find('#section-paymentmethods');
                            paymentMethods.slideUp(500, function() {
                                $(this).removeAttr('style');
                                model.EditPrimary(false);
                            });
                        }
                    });

                });
            }
        };
    }

}(window.VisitPay || {}, jQuery || {}, window.ko || {}));
﻿(function(common) {

    var vm = {
        PrimaryPaymentMethod: ko.observable()
    };

    function acceptTerms(e) {

        e.preventDefault();
        
        var $form = $(this);
        var consolidationGuarantorId = $('#ConsolidationGuarantorId').val();
        var requestType = $('#RequestType').val();

        var $terms = $('#cbTerms');
        var termsValid = $terms.length === 0 || $terms.valid();

        var $hipaa = $('#cbHipaa');
        var hipaaValid = $hipaa.length === 0 || $hipaa.valid();

        if (!termsValid || !hipaaValid) {
            return;
        }

        $.blockUI();
        $.get('/Consolidate/ConfirmAcceptance', function (cmsVersion) {

            $.unblockUI();
            common.ModalGenericConfirmationMd(cmsVersion.ContentTitle, cmsVersion.ContentBody, 'Confirm Acceptance', 'Close Window').then(function () {

                var data = {
                    consolidationGuarantorId: consolidationGuarantorId,
                    requestType: requestType,
                    __RequestVerificationToken: $('#__AjaxAntiForgeryForm input[name=__RequestVerificationToken]').val()
                };

                var achAuthorize = window.VisitPay.PaymentMethodsConsolidation ? new window.VisitPay.AchAuthorization().promptConsolidation(ko.unwrap(vm.PrimaryPaymentMethod), consolidationGuarantorId) : $.Deferred().resolve();
                achAuthorize.then(function() {
                    $.blockUI();
                    $.post($form.attr('action'), data, function(response) {
                        if (response.Success) {
                            window.location.href = response.RedirectUrl;
                        } else {
                            $.unblockUI();
                            common.ModalGenericAlert('Request Expired', response.Message, 'OK').then(function() {
                                window.location.href = response.RedirectUrl;
                            });
                        }
                    });
                }).fail(function() {
                    // don't allow continue - they didn't accept the ach authorization
                });

            });
        });

    };

    function declineTerms() {
        
        var consolidationGuarantorId = $('#ConsolidationGuarantorId').val();
        var requestType = $('#RequestType').val();

        $.blockUI();

        $.get('/Consolidate/DeclineConsolidation', function (cmsVersion) {

            $.unblockUI();
            common.ModalGenericConfirmationMd(cmsVersion.ContentTitle, cmsVersion.ContentBody, 'Confirm Cancellation', 'Close Window').then(function () {

                $.blockUI();

                var data = {
                    consolidationGuarantorId: consolidationGuarantorId,
                    requestType: requestType,
                    __RequestVerificationToken: $('#__AjaxAntiForgeryForm input[name=__RequestVerificationToken]').val()
                };

                $.post('/Consolidate/DeclineConsolidation', data, function (response) {
                    window.location.href = response.RedirectUrl;
                });

            });
        });

    };

    $(document).ready(function() {

        $('#frmConfirmAcceptance').on('submit', acceptTerms);
        $('#btnDecline').on('click', declineTerms);

        if (window.VisitPay.PaymentMethodsConsolidation) {
            new window.VisitPay.PaymentMethodsConsolidation().init(vm.PrimaryPaymentMethod);
        }

    });

})(window.VisitPay.Common || {});
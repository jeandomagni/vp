﻿(function (common) {

    function matchSubmit(e) {

        e.preventDefault();
        var $form = $(this);

        var $terms = $('#cbTerms');
        var termsValid = $terms.length === 0 || $terms.valid();

        var $hipaa = $('#cbHipaa');
        var hipaaValid = $hipaa.length === 0 || $hipaa.valid();

        if (!$form.valid() || !termsValid || !hipaaValid) {
            return;
        }

        $.blockUI();

        $.post($form.attr('action'), $form.serialize(), function (response) {

            $.unblockUI();

            if (response.Status === 0) { // no match found
                common.ModalGenericAlert(response.Title, response.Message, 'Ok');
                $form[0].reset();
            } else if (response.Status === 1) { // not eligible
                common.ModalGenericAlertMd(response.Title, response.Message, 'Ok');
                $form[0].reset();
            } else if (response.Status === 2) { // found
                common.ModalGenericAlertMd(response.Title, response.Message, 'Ok').then(function () {
                    window.location.href = response.RedirectUrl;
                });
            }


        });

    };

    $(document).ready(function () {

        var $form = $('#frmConsolidateMatch');
        $form.on('submit', matchSubmit);
        $form.find('#BirthYear').NumericOnly();


    });

})(window.VisitPay.Common || {});
﻿(function () {

    var vm = {
        PrimaryPaymentMethod: ko.observable()
    };

    function deconsolidateSubmit(e) {

        e.preventDefault();

        var $form = $(this);
        var consolidationGuarantorId = $form.find('#ConsolidationGuarantorId').val();

        var data = {
            consolidationGuarantorId: consolidationGuarantorId,
            requestType: $form.find('#RequestType').val(),
            __RequestVerificationToken: $('#__AjaxAntiForgeryForm input[name=__RequestVerificationToken]').val()
        };

        var achAuthorize = new window.VisitPay.AchAuthorization().promptConsolidation(ko.unwrap(vm.PrimaryPaymentMethod), consolidationGuarantorId);
        achAuthorize.then(function() {
            $.blockUI();
            $.post($form.attr('action'), data, function() {
                window.location.href = '/Consolidate/ManageHousehold';
            });
        }).fail(function() {
            // don't allow continue - they didn't accept the ach authorization
        });

    };

    $(document).ready(function () {

        $('#frmDeconsolidateFp').on('submit', deconsolidateSubmit);

        new window.VisitPay.PaymentMethodsConsolidation().init(vm.PrimaryPaymentMethod);

    });

})();
﻿(function (common) {

    function declineTerms(consolidationGuarantorId, requestType) {

        $.blockUI();

        $.get('/Consolidate/DeclineConsolidation', function (cmsVersion) {

            $.unblockUI();
            common.ModalGenericConfirmationMd(cmsVersion.ContentTitle, cmsVersion.ContentBody, 'Confirm Cancellation', 'Close Window').then(function () {

                $.blockUI();

                var data = {
                    consolidationGuarantorId: consolidationGuarantorId,
                    requestType: requestType,
                    __RequestVerificationToken: $('#__AjaxAntiForgeryForm input[name=__RequestVerificationToken]').val()
                };

                $.post('/Consolidate/DeclineConsolidation', data, function (response) {
                    window.location.href = response.RedirectUrl;
                });

            });
        });

    };

    function actionChange($select, requestType) {

        var consolidationGuarantorId = $select.parents('tr').find('input[type=hidden][name="ConsolidationGuarantorId"]').val();
        var value = parseInt($select.val());

        if (value === 1) { // cancel pending

            $.blockUI();

            $.get('/Consolidate/CancelRequest', function (cmsVersion) {

                $.unblockUI();
                common.ModalGenericConfirmationMd(cmsVersion.ContentTitle, cmsVersion.ContentBody, 'Confirm Cancellation', 'Close Window').then(function () {

                    $.blockUI();

                    var data = {
                        consolidationGuarantorId: consolidationGuarantorId,
                        requestType: requestType,
                        __RequestVerificationToken: $('#__AjaxAntiForgeryForm input[name=__RequestVerificationToken]').val()
                    };

                    $.post('/Consolidate/CancelRequest', data, function () {
                        window.location.reload();
                    });

                });
            });

        } else if (value === 2) { // accept terms (no fp)

            window.location.href = '/Consolidate/AcceptTerms/' + consolidationGuarantorId + '?requestType=' + requestType;

        } else if (value === 3) { // decline

            declineTerms(consolidationGuarantorId, requestType);

        } else if (value === 4) { // accept terms (fp)

            window.location.href = '/Consolidate/AcceptTermsFp/' + consolidationGuarantorId + '?requestType=' + requestType;

        } else if (value === 5) { // deconsolidate (no fp)

            $.get('/Consolidate/CancelConsolidation/' + consolidationGuarantorId, {  requestType: requestType }, function (cmsVersion) {

                $.unblockUI();
                common.ModalGenericConfirmationMd(cmsVersion.ContentTitle, cmsVersion.ContentBody, 'Confirm Cancellation', 'Close Window').then(function () {

                    $.blockUI();

                    var data = {
                        consolidationGuarantorId: consolidationGuarantorId,
                        requestType: requestType,
                        __RequestVerificationToken: $('#__AjaxAntiForgeryForm input[name=__RequestVerificationToken]').val()
                    };

                    $.post('/Consolidate/CancelRequest', data, function () {
                        window.location.reload();
                    });

                });
            });

        } else if (value === 6) { // deconsolidate (fp)

            window.location.href = '/Consolidate/DeconsolidateFp/' + consolidationGuarantorId + '?requestType=' + requestType;

        }


        $select.val('');

    };

    function managedActionChange() {
        actionChange($(this), 1);
    };

    function managingActionChange() {
        actionChange($(this), 2);
    };

    $(document).ready(function() {

        var $managedAction = $('select[name="ManagedActionOptions"]');
        $managedAction.on('change', managedActionChange);

        var $managingAction = $('select[name="ManagingActionOptions"]');
        $managingAction.on('change', managingActionChange);

    });

})(window.VisitPay.Common || {});
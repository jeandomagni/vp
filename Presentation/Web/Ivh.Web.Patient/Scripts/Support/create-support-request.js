﻿window.VisitPay.CreateSupportRequest = (function (common, $) {

    //
    var createSupportRequest = {};
    var internal = {};

    internal.submitComplete = function () {

        $.post('/Support/CreateSupportRequestConfirmation', {}, function (cms) {
            createSupportRequest.OnCreated();
            $(document).trigger('supportRequestCreated');
            $.unblockUI();
            common.ModalGenericAlert(cms.ContentTitle, '<p class="text-left">' + cms.ContentBody + '</p>', 'Ok');
        });

    };
    internal.submitForm = function (e) {

        e.preventDefault();

        $(this).find('textarea').each(function() { $(this).val($(this).val().trim()); });
        if ($(this).valid()) {
            $(this).parents('.modal').modal('hide');
            $(this).find('#MessageBody').val($(this).find('#MessageBody').val().replace(/>/g, '').replace(/</g, ''));
            $.blockUI();
            $.post('/Support/CreateSupportRequest', $(this).serialize() + '&__RequestVerificationToken=' + $('#__AjaxAntiForgeryForm input[name=__RequestVerificationToken]').val(), function (result) {

                if (result.Result) {
                    internal.uploadFiles(result.Message);
                }

            });

        }

    };
    internal.uploadFiles = function (data) {

        data.__RequestVerificationToken = $('#__AjaxAntiForgeryForm input[name=__RequestVerificationToken]').val();

        var $form = $('#frmCreateSupportRequestAttachments');

        if ($form.find('tbody.files > tr').length > 0) {

            $form.fileupload('option', {
                formData: data
            });

            $form.find('button.start').click();

        } else {

            internal.submitComplete();

        }

    };

    //
    var bindFileUploader = function() {

        var $form = $('#frmCreateSupportRequestAttachments');
        var fileCount = 0;

        $form.fileupload({
                acceptFileTypes: /(\.|\/)(doc|docx|gif|jpe?g|pdf|png|ppt|pptx|txt|xls|xlsx|zip)$/i,
                autoUpload: false,
                dataType: 'json',
                maxFileSize: 5000000, //5mb in bytes
                url: '/Support/UploadSupportRequestMessageAttachment',
                change: function(e, data) {
                    $form.find('.error').text('');
                },
                processfail: function(e, data) {
                    var error = data.files[data.index].error;
                    var errorMessage = error;
                    if (error.indexOf('too large') > -1)
                        errorMessage = 'We cannot accept files over 5MB.';
                    else if (error.indexOf('not allowed') > -1)
                        errorMessage = 'This is an unsupported file type.';

                    $form.find('.error').text(errorMessage);
                }
            })
            .on('fileuploadadded', function() {

                fileCount++;

                $form.find('button.cancel').off('click').on('click', function() {
                    fileCount--;
                    $form.find('.error').text('');
                });
            })
            .on('fileuploadalways', function() {
                fileCount--;
                if (fileCount <= 0)
                    internal.submitComplete();
            });
    };

    //
    var initialize = function ($scope) {

        //
        $('#frmCreateSupportRequest').parseValidation();

        //
        bindFileUploader();

        //
        $scope.on('submit', '#frmCreateSupportRequest', internal.submitForm);
        $scope.on('click', '#btnCreateSupportRequestSubmit', function(e) {

            e.preventDefault();
            $scope.find('#frmCreateSupportRequest').submit();

        });

        $scope.modal('show');
        $.unblockUI();

        //
        window._paq.push(['trackEvent', 'CreateCustomerCareRequest', 'Click']);

    }

    createSupportRequest.Initialize = function (modalContainer, visitId) {

        var promise = window.VisitPay.Common.ModalContainerAsync(modalContainer, '/Support/CreateSupportRequest', 'GET', { visitId: visitId }, false);
        promise.done(function (modal) {
            initialize(modal);
        });

    };
    createSupportRequest.OnCreated = function() {};

    return createSupportRequest;

})(window.VisitPay.Common, $);
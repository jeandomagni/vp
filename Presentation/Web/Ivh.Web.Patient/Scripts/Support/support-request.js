﻿window.VisitPay.SupportRequest = (function(supportRequestAction, common, $, ko) {

    //
    var supportRequestViewModel = function() {

        var self = this;

        self.ClosedBy = ko.observable();
        self.ClosedDate = ko.observable();
        self.CreatedDate = ko.observable();
        self.IsPreviousConsolidation = ko.observable();
        self.GuarantorFullName = ko.observable();
        self.PatientFullName = ko.observable();
        self.SubmittedFor = ko.observable();
        self.SupportRequestId = ko.observable();
        self.SupportRequestDisplayId = ko.observable();
        self.SupportRequestStatusId = ko.observable();
        self.SupportRequestStatusId.IsClosed = ko.computed(function () {
            return ko.unwrap(self.SupportRequestStatusId) !== 1;
        });
        self.SupportRequestStatusId.IsOpen = ko.computed(function() {
            return ko.unwrap(self.SupportRequestStatusId) === 1;
        });
        self.SupportRequestStatusName = ko.observable();
        self.Topic = ko.observable();
        self.TreatmentDate = ko.observable();
        self.TreatmentLocation = ko.observable();
        self.TreatmentLocation.Any = ko.computed(function () {
            return ko.unwrap(self.TreatmentLocation) && ko.unwrap(self.TreatmentLocation).length > 0;
        });
        self.VisitSourceSystemKeyDisplay = ko.observable();
        self.VisitDescription = ko.observable();
        self.HasVisit = ko.observable();

        self.GuarantorMessages = ko.observableArray([]);
        self.GuarantorMessages.SortOrder = ko.observable(1);
        self.GuarantorMessages.Sorted = ko.computed(function() {
            return self.GuarantorMessages().sort(function (l, r) {
                if (self.GuarantorMessages.SortOrder() === 0)
                    return l.SupportRequestMessageId() === r.SupportRequestMessageId() ? 0 : (l.SupportRequestMessageId() < r.SupportRequestMessageId() ? -1 : 1);
                else
                    return l.SupportRequestMessageId() === r.SupportRequestMessageId() ? 0 : (l.SupportRequestMessageId() < r.SupportRequestMessageId() ? 1 : -1);
            });
        });
        self.GuarantorMessages.ToggleSortOrder = function () {
            self.GuarantorMessages.SortOrder() === 0 ? self.GuarantorMessages.SortOrder(1) : self.GuarantorMessages.SortOrder(0);
        };

        self.Attachments = ko.observableArray([]);
        self.Attachments.Count = ko.computed(function () {
            return self.Attachments().length;
        });

    };

    //
    var model;
    var $scope;

    //
    var supportRequest = {};

    //
    var internal = {};
    internal.supportRequestId = null;

    var actions = {};

    //
    var loadData = function () {

        var promiseData = $.Deferred();
        $.post('/Support/SupportRequest', { supportRequestId: internal.supportRequestId }, function (data) {
            promiseData.resolve(data);
        }, 'json');

        return promiseData;

    };

    var bindLessMore = function () {
        $scope.find('.messages .body').each(function () {
            var height = $(this).find('p:eq(0)').height();
            if (height > 36) {
                $(this).find('.more').show();
            }
        });
    };
    var downloadAttachment = function (supportRequestMessageAttachmentId) {

        supportRequestMessageAttachmentId = supportRequestMessageAttachmentId || null;

        $.post('/Support/PrepareDownload', {
            supportRequestId: internal.supportRequestId,
            supportRequestMessageAttachmentId: supportRequestMessageAttachmentId,
            visitPayUserId: internal.visitPayUserId
        }, function(result) {
            if (result.Result === true)
                window.location.href = result.Message;
        }, 'json');

    };
    var saveComplete = function () {

        loadData().done(function (data) {
            ko.mapping.fromJS(data, null, model);
            actions.showEditor(false);
            bindLessMore();
            $.unblockUI();
            $(document).trigger('supportRequestChanged');
        });

    };
    var saveAttachments = function (supportRequestMessageId) {

        var $form = $scope.find('#frmEditorAttachments');

        if ($form.find('tbody.files > tr').length > 0) {

            $form.fileupload('option', {
                formData: {
                    SupportRequestId: internal.supportRequestId,
                    SupportRequestMessageId: supportRequestMessageId,
                    __RequestVerificationToken: $('#__AjaxAntiForgeryForm input[name=__RequestVerificationToken]').val()
                }
            });

            $form.find('button.start').click();

        } else {

            saveComplete();
        }

    };
    var sendReply = function (messageBody) {

        $.post('/Support/SaveMessage', { model: { SupportRequestId: internal.supportRequestId, MessageBody: messageBody } }, function (result) {

            if (result.Result) {
                saveAttachments(result.Message);
            }

        }, 'json');
    };

    //
    actions.bindFileUploader = function () {

        var $form = $('#frmEditorAttachments');
        var $error = $form.find('.error');
        if ($error.length === 0)
            $error = $('.editor-buttons').find('.error');

        var fileCount = 0;

        $form.find('.files').empty();

        $form.fileupload({
            acceptFileTypes: /(\.|\/)(doc|docx|gif|jpe?g|pdf|png|ppt|pptx|txt|xls|xlsx|zip)$/i,
            autoUpload: false,
            dataType: 'json',
            maxFileSize: 5000000, //5mb in bytes
            url: '/Support/UploadSupportRequestMessageAttachment',
            processfail: function(e, data) {
                var error = data.files[data.index].error;
                var errorMessage = error;
                if (error.indexOf('too large') > -1)
                    errorMessage = 'We cannot accept files over 5MB.';
                else if (error.indexOf('not allowed') > -1)
                    errorMessage = 'This is an unsupported file type.';

                $error.text(errorMessage);
            }
        }).off('fileuploadchange').on('fileuploadchange', function() {

            $error.text('');

        }).off('fileuploadadded').on('fileuploadadded', function() {

            fileCount++;
            actions.showUpload(fileCount);

            $form.find('button.cancel').off('click').on('click', function() {
                fileCount--;
                $error.text('');
                actions.showUpload(fileCount);
            });

        }).off('fileuploadalways').on('fileuploadalways', function(e) {
            fileCount--;
            if (fileCount <= 0)
                saveComplete();
        });

    };
    actions.printRequest = function () {
        $.blockUI();
        $.post('/Support/PreparePrintSupportRequest', { supportRequestId: internal.supportRequestId, sortOrder: model.GuarantorMessages.SortOrder() === 1 ? 'desc' : 'asc' }, function (url) {
            $.unblockUI();
            window.open(url);
        }, 'json');
    };
    actions.refreshSupportRequest = function () {

        $.blockUI();

        loadData().done(function (data) {

            $.unblockUI();
            ko.mapping.fromJS(data, null, model);
            bindLessMore();
            if (model.SupportRequestStatusId() === 1)
                $scope.modal('show');

        });

    };
    actions.showAttachments = function (supportRequestMessageId) {

        supportRequestMessageId = supportRequestMessageId || null;

        $.blockUI();

        var promise = common.ModalNoContainerAsync('modalSupportAttachments', '/Support/Attachments', 'POST', {
            supportRequestId: internal.supportRequestId,
            supportRequestMessageId: supportRequestMessageId,
            visitPayUserId: internal.visitPayUserId
        }, false);

        promise.done(function ($modal) {

            $modal.find('.icon-file-type').each(function () {

                $(this).addClass(getFileType($(this).prev('input[type=hidden]').val()));

            });

            $modal.find('a').click(function (e) {

                e.preventDefault();
                downloadAttachment($(this).prev('input[type=hidden]').val());

            });

            $modal.find('#btnDownloadAttachmentsAll').click(function (e) {

                e.preventDefault();
                downloadAttachment();

            });

            $modal.modal('show');

            $.unblockUI();
        });
    }
    actions.showRequestAction = function () {

        $scope.modal('hide');

        var promise = supportRequestAction.Initialize($('#modalSupportRequestActionContainer'), model.SupportRequestStatusId, model.SupportRequestId, model.SupportRequestDisplayId);

        promise.done(function ($modal) {
            var dismiss = $modal.find('[data-dismiss="modal"]');
            dismiss.on('click', function () {
                dismiss.off('click');
                $scope.modal('show');
            });
            $modal.on('hidden.bs.modal', function() {
                dismiss.off('click');
            });
        });

    };
    actions.showEditor = function (show) {

        //
        var $editor = $scope.find('.editor');
        var $form = $editor.find('#frmEditor');
        var $textarea = $editor.find('textarea');

        //
        var $buttonsPrimary = $scope.find('.modal-footer .primary-buttons');
        var $buttonsInternal = $scope.find('.modal-footer .editor-buttons');

        //
        var isActive = $buttonsInternal.is(':visible');

        if (show === true && !isActive) {

            //
            actions.showUpload(0);
            actions.bindFileUploader();

            //
            common.ResetUnobtrusiveValidation($form);
            $textarea.val('').removeClass('inactive');

            //
            $buttonsPrimary.hide();
            $buttonsInternal.fadeIn(500);

        } else if (show === false && isActive) {

            //
            actions.showUpload(0);

            //
            common.ResetUnobtrusiveValidation($form);
            $textarea.val('').addClass('inactive').blur();

            //
            $buttonsInternal.hide();
            $buttonsPrimary.fadeIn(500);

        }

    };
    actions.showUpload = function (fileCount) {

        var $form = $('#frmEditorAttachments');

        if (fileCount > 0 && !$form.is(':visible')) {
            $('#frmEditor').animate({ width: '560px' }, 150);
            $('#frmEditorAttachments').show().animate({ width: '293px' }, 150);
        } else if (fileCount === 0 && $form.is(':visible')) {
            $('#frmEditor').animate({ width: '867px' }, 150);
            $('#frmEditorAttachments').animate({ width: '0' }, 150, function () {
                $(this).hide();
            });
        }

    };
    actions.sendReply = function () {

        var $form = $scope.find('#frmEditor');
        var $textarea = $form.find('textarea');

        if ($form.valid()) {
            $.blockUI();
            sendReply($textarea.val());
        }
    };

    //
    var initialize = function ($modal, data) {

        $scope = $modal;

        //
        model = new supportRequestViewModel();
        ko.mapping.fromJS(data, null, model);
        ko.applyBindings(model, $scope[0]);

        //
        $.unblockUI();
        
        $scope.on('shown.bs.modal', bindLessMore);
        $scope.modal('show');
        
        $scope.on('change', '#ddSupportRequestAction', function (e) {

            e.preventDefault();

            switch (parseInt($(this).val())) {
                case 1:
                    actions.printRequest();
                    break;
                case 2:
                    actions.showRequestAction();
                    break;
            }

            $(this).val('');

        });
        
        $scope.on('click', '#btnSupportRequestDoAction', actions.showRequestAction);
        
        $scope.on('click', '.messages .title a', function (e) {

            e.preventDefault();
            var context = ko.contextFor(this);
            actions.showAttachments(context.$data.SupportRequestMessageId());

        });
        
        $scope.on('click', '.messages .body .more a', function(e) {

            e.preventDefault();
            $(this).parent().hide();
            $(this).parents('.body').find('.less').show();
            $(this).parents('.body').css('height', 'auto');

        });

        $scope.on('click', '.messages .body .less a', function(e) {

            e.preventDefault();
            $(this).parent().hide();
            $(this).parents('.body').css('height', '36px');
            $(this).parents('.body').find('.more').show();

        });

        $scope.on('focus', '#frmEditor textarea', function(e) {

            actions.showEditor(true);

        });
        
        $scope.on('click', '#btnSendReply', actions.sendReply);

        $scope.on('click', '#btnEditorAttach', function () {
            $('#frmEditorAttachments').find('input[type=file]').click();
        });
        
        $scope.on('click', '#btnEditorDiscard', function() {

            var promise = common.ModalGenericConfirmation('Confirm Discard', 'This will not be saved as a draft. Do you want to discard?');
            promise.done(function() {
                actions.showEditor(false);
            });
            promise.fail(function() {
                $scope.find('#frmEditor textarea').focus();
            });

        });
        
        $scope.on('click', '#btnSupportRequestAttachments', function() {

            actions.showAttachments();

        });
    };
    supportRequest.Initialize = function (modalContainer, supportRequestId) {

        $.blockUI();

        internal.supportRequestId = supportRequestId;

        var promiseModal = window.VisitPay.Common.ModalContainerAsync(modalContainer, '/Support/SupportRequest', 'GET', false);
        var promiseData = loadData(supportRequestId);

        $.when(promiseModal, promiseData).done(function (modal, data) {
            initialize(modal, data);
            $(document).trigger('supportRequestChanged');
        });

        //
        $(document).off('supportRequestActionCompleted.supportrequest').on('supportRequestActionCompleted.supportrequest', actions.refreshSupportRequest);

    };

    //
    return supportRequest;

})(window.VisitPay.SupportRequestAction, window.VisitPay.Common, jQuery, window.ko);
﻿window.VisitPay.SupportRequestAction = (function(common, $) {

    //
    var supportRequestAction = {};

    supportRequestAction.Initialize = function (modalContainer, supportRequestStatus, supportRequestId, supportRequestDisplayId, visitPayUserId) {

        var promise = window.VisitPay.Common.ModalContainerAsync(modalContainer, '/Support/SupportRequestAction', 'POST', {
            supportRequestStatus: supportRequestStatus,
            supportRequestId: supportRequestId,
            supportRequestDisplayId: supportRequestDisplayId,
            visitPayUserId: visitPayUserId
        }, false);

        promise.done(function (modal) {

            modal.modal('show');

            var $form = modal.find('form');

            common.ResetUnobtrusiveValidation($form);

            setTimeout(function() {
                $form.find('textarea').focus();
            }, 500);

            modal.on('click', '#btnCloseSupportRequestSubmit', function(e) {

                e.preventDefault();

                if ($form.valid()) {

                    modal.modal('hide');
                    $.blockUI();

                    $.post('/Support/CompleteSupportRequestAction', $form.serialize() + '&__RequestVerificationToken=' + $('#__AjaxAntiForgeryForm input[name=__RequestVerificationToken]').val(), function() {

                        $.unblockUI();
                        $(document).trigger('supportRequestActionCompleted');

                    });

                }

            });

            $.unblockUI();
        });

        return promise;

    };

    return supportRequestAction;

})(window.VisitPay.Common, jQuery);
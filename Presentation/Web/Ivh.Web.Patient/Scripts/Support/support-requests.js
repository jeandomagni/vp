﻿(function() {

    var isMobile = window.VisitPayMobile !== undefined;
    
    function createSupportRequest() {
        if (isMobile) {
            window.VisitPayMobile.Common.OpenCreateSupportRequest();
        } else {
            window.VisitPay.CreateSupportRequest.Initialize($('#modalCreateSupportRequestContainer'), null);
        }
    }

    function openSupportRequest(supportRequestId) {
        if (isMobile) {
            window.VisitPayMobile.Common.OpenSupportRequest(supportRequestId);
        } else {
            window.VisitPay.SupportRequest.Initialize($('#modalSupportRequestContainer'), supportRequestId);
        }
    };

    $(document).ready(function() {

        var $scope = $('#section-customer-care');

        var gridModel = window.VisitPay.VpGrid(function() { return {}; }, '/Support/MySupportRequests', 25, 'InsertDate', 'desc');
        ko.applyBindings(gridModel, $scope.find('.grid-customer-care').parent()[0]);

        gridModel.Search(false).done(function() { 
            $('.grid-customer-care').removeClass('vpgrid-mask');
        });
        
        $(document).on('gridlinkclick.local', '.grid-customer-care > .grid-row', function(e, obj) {
            var data = ko.mapping.toJS(ko.dataFor(obj.sender[0]));
            openSupportRequest(ko.unwrap(data.SupportRequestId));
        });

        $(document).on('guarantorFilterChanged', function(e, obfuscatedVpGuarantorId) {
            $.blockUI();
            window.location.reload(true);
        });

        $scope.on('click', '#btnCreateSupportRequest', function(e) {
            e.preventDefault();
            $.blockUI();
            createSupportRequest();
        });
        
        if (isMobile) {
            $(window).on('supportRequestChanged', gridModel.Refresh);
        } else {
            $(document).on('supportRequestActionCompleted.grid', gridModel.Refresh);
            $(document).on('supportRequestChanged.grid', gridModel.Refresh);
            window.VisitPay.CreateSupportRequest.OnCreated = gridModel.Refresh;
        }

    });
})();
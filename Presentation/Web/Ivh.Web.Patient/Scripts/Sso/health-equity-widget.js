﻿(function (common, $) {

    var $scope = $('.sidebar');
    var model = JSON.parse((window.VisitPay.Sso && window.VisitPay.Sso.HealthEquity) ? window.VisitPay.Sso.HealthEquity.Model || '{}' : '{}');
    var balanceModel = {};
    var initLoadBalPromise = null;

    var setBalanceModalProperties = function (result, modalScope) {

        modalScope.find('.healthequity-hsa-cash-balance-modal').html(result['hsaModalCashBalance']);
        modalScope.find('.healthequity-hsa-investment-balance-modal').html(result['hsaModalInvestmentBalance']);
        modalScope.find('.healthequity-hsa-total-balance-modal').html(result['hsaModalTotalBalance']);
        modalScope.find('.healthequity-hsa-contributions-ytd-modal').html(result['hsaModalContributionsYtd']);
        modalScope.find('.healthequity-hsa-distributions-ytd-modal').html(result['hsaModalDistributionsYtd']);
        modalScope.find('.healthequity-lastcalltoapi-modal').html(result['hsaModalDate']);
        modalScope.find('.healthequity-error-modal').html(result['hsaModalError']);

        modalScope.find('.balances').removeClass('loading');
        var $balanceMessage = modalScope.find('#divHqyBalanceMessage');

        if (result['hasBalance']) {
            $balanceMessage.find($('.balance-current')).show();
            $balanceMessage.find($('.balance-na')).hide();
        } else {
            $balanceMessage.find($('.balance-current')).hide();
            $balanceMessage.find($('.balance-na')).show();
        }

    }
    var setBalanceWidgetProperties = function (result, additionalScope) {
        $scope.find('.healthequity-hsa-cash-balance').html(result['hsaWidgetCashBalance']);
        $scope.find('.healthequity-lastcalltoapi').html(result['hsaWidgetDate']);
        
        if (additionalScope !== undefined && additionalScope !== null) {
            additionalScope.find('.healthequity-hsa-cash-balance').html(result['hsaWidgetCashBalance']);
            additionalScope.find('.healthequity-lastcalltoapi').html(result['hsaWidgetDate']);
        }

        // pay bill page widget
        var $paybillWidget = $('.paybill-healthequity-balance');
        if ($paybillWidget.length > 0) {
            $paybillWidget.find('.healthequity-hsa-cash-balance').html(result['hsaWidgetCashBalance']);
            $paybillWidget.find('.healthequity-lastcalltoapi').html(result['hsaWidgetDate']);
            $paybillWidget.show();
        }
    }

    var loadSso = function() {
        var promise = $.Deferred();
        $.get('/Sso/HealthEquitySsoStatus', {}, function (result) {
            if (result !== undefined && result !== null && result.IsHqySsoUp) {
                balanceModel.hsaModalError = '';
            } else {
                balanceModel.hsaModalError = "<p><span class='text-italic'>HealthEquity Single Sign-On currently unavailable. Please try again in a few minutes.<span></p>";
            }
            promise.resolve(balanceModel);
        });
        return promise;
    }
    var loadBalance = function () {
        var promise = $.Deferred();
        if (model.ShowBalance && balanceModel['hsaWidgetCashBalance'] === undefined) {
            $.get('/Sso/HealthEquityBalance', {}, function (result) {
                if (result !== undefined && result !== null && result.HsaCashBalance !== undefined && result.HsaCashBalance !== null) {
                    balanceModel.hsaWidgetCashBalance = result.HsaCashBalance;
                    balanceModel.hsaModalCashBalance = result.HsaCashBalance;
                    balanceModel.hsaModalInvestmentBalance = result.HsaInvestmentBalance;
                    balanceModel.hsaModalTotalBalance = result.HsaTotalBalance;
                    balanceModel.hsaModalContributionsYtd = result.HsaContributionsYtd;
                    balanceModel.hsaModalDistributionsYtd = result.HsaDistributionsYtd;

                    balanceModel.hasBalance = true;
                } else {
                    balanceModel.hsaWidgetCashBalance = 'Currently Unavailable';
                    balanceModel.hsaModalCashBalance = '-';
                    balanceModel.hsaModalInvestmentBalance = '-';
                    balanceModel.hsaModalTotalBalance = '-';
                    balanceModel.hsaModalContributionsYtd = '-';
                    balanceModel.hsaModalDistributionsYtd = '-';
                    
                    balanceModel.hasBalance = false;
                }
                if (result !== undefined && result !== null && result.HsaDate !== undefined && result.HsaDate !== null) {
                    balanceModel.hsaWidgetDate = 'as of ' + result.HsaDate;
                    balanceModel.hsaModalDate = 'as of ' + result.HsaDate;
                } else {
                    balanceModel.hsaWidgetDate = '';
                    balanceModel.hsaModalDate = '';
                }
                promise.resolve(balanceModel);
            });
        }
        else if (model.ShowBalance && balanceModel['hsaWidgetCashBalance'] !== undefined) {
            promise.resolve(balanceModel);
        } else {
            promise.reject();
        }
        return promise;
    };
    var loadModel = function () {
        var promise = $.Deferred();
        var promiseBalance = loadBalance();
        var promiseSso = loadSso();
        $.when.apply($, [promiseBalance, promiseSso]).done(function () { promise.resolve(balanceModel) });
        return promise;
    }

    var openSetupSuccessDialog = function () {

        $.blockUI();
        common.ModalNoContainerAsync('modalHealthEquitySso', '/Sso/HealthEquitySsoDialogPartial', 'POST', {}, true).done(function($modal) {
            window._paq.push(['trackEvent', 'HealthEquity', 'WidgetClick']);
            $modal.on('click', '#aGotoHealthEquity', function(e) {
                $modal.modal('hide');
            });

            $.unblockUI();

            var ssoSetFunct = function(result) {
                setBalanceModalProperties(result, $modal);
            };

            if (initLoadBalPromise !== null) {
                initLoadBalPromise.done(ssoSetFunct);
            } else {
                loadModel().done(ssoSetFunct);
            }
        });

    };

    $(document).on('click.piwik', 'div.btn-vp-toggle:enabled:not(.off)', function () {
        window._paq.push(['trackEvent', 'HealthEquity', 'ToggleWidgetOff']);
    });

    $(document).on('click.piwik', 'div.btn-vp-toggle.off', function () {
        window._paq.push(['trackEvent', 'HealthEquity', 'ToggleWidgetOn']);
    });

    $(document).ready(function () {

        if (model.Visible === true && model.SsoOutboundAgreed === true) {
            initLoadBalPromise = loadModel();
            initLoadBalPromise.done(function (result) {
                initLoadBalPromise = null;
                if (result['hsaWidgetCashBalance'] !== undefined) {
                    setBalanceWidgetProperties(result, $('body'));
                }
                if (window.VisitPay.Sso.HealthEquity && window.VisitPay.Sso.HealthEquity.OpenInitialDialog === 'True') {
                    openSetupSuccessDialog();
                }
            });

            $scope.find('#healthequity-sso-outbound-link').on('click', openSetupSuccessDialog);

        }
        else if (model.Visible === true) {
            $scope.find('#healthequity-accept-dialog-link').on('click', function () {
                window._paq.push(['trackEvent', 'HealthEquity', 'WidgetClick']);
                window.VisitPay.HealthEquityCommon.initNoSsoAgreement(false);
            });
        }

    });

})(VisitPay.Common || {}, jQuery);
﻿window.VisitPay.HealthEquityCommon = (function (common) {

    function openSetupSuccessDialog() {
        function callbackAfterClose() {
            $.blockUI();
            window.location.reload(false);
        };
        $.blockUI();
        common.ModalNoContainerAsync('modalHealthEquitySsoSetupSuccess', '/Sso/HealthEquitySsoSetupSuccessDialogPartial', 'POST', {}, true, callbackAfterClose)
            .done(function ($modal) {
                $.unblockUI();
            });
    };

    function openSetupFailedDialog() {
        $.blockUI();
        common.ModalNoContainerAsync('modalHealthEquitySsoSetupFailed', '/Sso/HealthEquitySsoSetupFailedDialogPartial', 'POST', {}, true)
            .done(function ($modal) {
                $.unblockUI();
            });
    };

    function openTermsDialog() {
        var action = null;
        function callbackAfterClose() {
            if (action === null) {
                return;
            }
            if (action === 'btnHqyAgreeDialogSave') {
                openSetupSuccessDialog();
            }
            if (action === 'btnHqyDeclineDialogSave') {
                openSetupFailedDialog();
            }
            action = null;
        }
        function callbackAfterShown($modal) {
            $modal.find('#btnHqyAgreeDialogSave').on('click', function () {
                $.blockUI();
                window._paq.push(['trackEvent', 'HealthEquity', 'Accept']);
                $.ajax({
                    method: 'POST',
                    url: '/Sso/Terms',
                    data: { ssoProvider: 2, accept: true }
                }).done(function(response) {
                    action = 'btnHqyAgreeDialogSave';
                    $modal.modal('hide');
                    $.unblockUI();
                }).fail(function() {
                    $.unblockUI();
                });
            });
            $modal.find('#btnHqyDeclineDialogSave').on('click', function () {
                $.blockUI();
                $.ajax({
                    method: 'POST',
                    url: '/Sso/Terms',
                    data: { ssoProvider: 2, accept: false }
                }).done(function(response) {
                    action = 'btnHqyDeclineDialogSave';
                    $modal.modal('hide');
                    $.unblockUI();
                }).fail(function() {
                    $.unblockUI();
                });
            });
        };
        $.blockUI();
        common.ModalNoContainerAsync('modalHealthEquityAgreeToTermsSso', '/Sso/HealthEquitySsoAgreeToTermsDialogPartial', 'POST', {}, true, callbackAfterClose, callbackAfterShown)
            .done(function ($modal) {
                $.unblockUI();
            });
    };

    function openPreTermsDialog() {
        var action = null;
        function callbackAfterClose() {
            if (action === null) {
                return;
            }
            if (action === 'btnHqyPreAgreeDialogContinue') {
                openTermsDialog();
            }
            action = null;
        }
        function callbackAfterShown($modal) {
            $modal.find('#btnHqyPreAgreeDialogContinue').on('click', function () {
                action = 'btnHqyPreAgreeDialogContinue';
                $modal.modal('hide');
            });
            $modal.find('#btnHqyPreAgreeDialogCancel').on('click', function () {
                $modal.modal('hide');
            });
        };
        $.blockUI();
        common.ModalNoContainerAsync('modalHealthEquityPreAgreeToTermsSso', '/Sso/HealthEquitySsoPreAgreeToTermsDialogPartial', 'POST', {}, true, callbackAfterClose, callbackAfterShown)
            .done(function ($modal) {
                $.unblockUI();
            });
    }

    function openDontShowWidgetDialog() {
        
        $.blockUI();
        $.post('/Sso/Ignore', { ssoProvider: 2, isIgnored: true }, function () {
            common.ModalNoContainerAsync('modalHealthEquityDontShowWidgetSso', '/Sso/HealthEquityDontShowDialogPartial', 'GET', null, true).done(function ($modal) {

                window.VisitPay.Common.ReloadSidebar().done(function() {
                    $.unblockUI();
                });

            });

        });

    }

    function initNoSsoAgreement(skipStep1, selectedItem) {
        var ssoProvider = -1;
        if (selectedItem !== undefined && selectedItem !== null) {
            ssoProvider = selectedItem.ssoProviderEnum();
        }
        var model = JSON.parse((window.VisitPay.Sso && window.VisitPay.Sso.HealthEquity) ? window.VisitPay.Sso.HealthEquity.Model || '{}' : '{}');
        if (ssoProvider === -1 && model !== undefined && model !== null && model.hasOwnProperty('SsoProviderEnum')) {
            ssoProvider = model.SsoProviderEnum;
        }

        var oAuthWindowOpen = function ($modal, url) {
            //These are the 2 outbound oauth health equity providers
            if ((url || window.VisitPay.Sso.HealthEquity.OAuthUrl) == undefined) return;
            $modal.UnblockUI = false;
            var win;
            var checkConnect;
            win = window.open(url || window.VisitPay.Sso.HealthEquity.OAuthUrl, 'OAuthWindow', 'width=1045,height=660,modal=yes,alwaysRaised=yes');

            checkConnect = setInterval(function() {
                //I wonder if i should put a max time to wait here?
                if (!win || !win.closed) return;
                clearInterval(checkConnect);
                if (window.VisitPay.Sso != undefined && window.VisitPay.Sso.HealthEquity != undefined && window.VisitPay.Sso.HealthEquity.AfterOAuthWindowClosesUrl != undefined) {
                    window.location = window.VisitPay.Sso.HealthEquity.AfterOAuthWindowClosesUrl;
                } else {
                    window.location.reload();
                }
            }, 100);
        };

        var action = null;
        var callbackAfterClose = function ($modal) {
            if (action === null) {
                return;
            }
            if ((ssoProvider == 3 || ssoProvider == 4) && action === 'OpenPreTermsDialog') {
                oAuthWindowOpen($modal);
            } else if (action === 'OpenPreTermsDialog') {
                //This is using the outbound saml
                openPreTermsDialog();
            }

            if (action === 'OpenDontShowWidgetDialog') {
                openDontShowWidgetDialog();
            }
            action = null;
        }
        function callbackAfterShown($modal) {
            $modal.find('#healthequity-open-agreement-link').on('click', function () {
                action = 'OpenPreTermsDialog';
                $modal.modal('hide');
            });

            $modal.on('click', '#aMore', function () {
                $(this).toggleClass('expanded');
                $modal.find('#divMore').toggle();
            });
        };
        
        
        if (skipStep1) {
            if (ssoProvider == 3 || ssoProvider == 4) {
                var urlToUse = undefined;
                if (selectedItem !== undefined && selectedItem !== null) {
                    urlToUse = selectedItem.ssoProviderActionUrl();
                }
                oAuthWindowOpen({}, urlToUse);
            } else {
                $.blockUI();
                openPreTermsDialog();
            }
        } else {
            $.blockUI();
            common.ModalNoContainerAsync('modalHealthEquityNoSso', '/Sso/HealthEquityNoSsoDialogPartial', 'POST', {}, true, callbackAfterClose, callbackAfterShown).done(function ($modal) {
                if (!$modal.hasOwnProperty('UnblockUI') || ($modal.hasOwnProperty('UnblockUI') && $modal.UnblockUI)) {
                    $.unblockUI();
                }
            });
        }
    };

    return {
        initNoSsoAgreement: initNoSsoAgreement

    };

})(window.VisitPay.Common || {});
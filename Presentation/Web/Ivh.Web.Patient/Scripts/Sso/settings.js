﻿window.VisitPay.Sso = (function () {
    
    var activeText = window.VisitPay.Localization.Active || 'Active';
    var acceptText = window.VisitPay.Localization.Accept || 'Accept';
    var declineText = window.VisitPay.Localization.Decline || 'Decline';
    var disableText = window.VisitPay.Localization.Disable || 'Disable';
    var disabledText = window.VisitPay.Localization.Disabled || 'Disabled';
    var enableText = window.VisitPay.Localization.Enable || 'Enable';
    var noText = window.VisitPay.Localization.No || 'No';
    var yesText = window.VisitPay.Localization.Yes || 'Yes';

    var ViewModel = function(settings) {
        var self = this;
        self.body = ko.observable(settings.Body.ContentBody);
        self.decline = settings.Decline;
        self.clientBrandName = ko.observable(settings.ClientBrandName);
        self.ssoProviderName = ko.observable(settings.SsoProviderName);
        self.ssoProviderEnum = ko.observable(settings.SsoProviderEnum);
        self.ssoProviderActionUrl = ko.observable(settings.SsoProviderActionUrl);
        self.ssoProviderId = ko.observable(settings.SsoProviderId);
        self.shortDescription = ko.observable(settings.ShortDescription);
        self.isAccepted = ko.observable(settings.IsAccepted);
        self.isDeclined = ko.observable(settings.IsDeclined);
        self.isIgnored = ko.observable(settings.IsIgnored);
        self.isNotIgnored = ko.observable(!settings.IsIgnored);
        self.actionDateTime = ko.observable(settings.ActionDateTime);
        self.actionVisitPayUser = ko.observable(settings.ActionVisitPayUser);
        self.enableFromManageSso = ko.observable(settings.EnableFromManageSso);
        self.activeDisplay = ko.computed(function() {
            return self.isAccepted() ? activeText : disabledText;
        }, self);

        self.actionText = ko.computed(function() {
            return self.isAccepted() ? disableText : enableText;
        });

        self.updateFromModel = function(model) {
            self.body(model.Body.ContentBody);
            self.decline = model.Decline;
            self.clientBrandName(model.ClientBrandName);
            self.ssoProviderName(model.SsoProviderName);
            self.ssoProviderId(model.SsoProviderId);
            self.shortDescription(model.ShortDescription);
            self.isAccepted(model.IsAccepted);
            self.isDeclined(model.IsDeclined);
            self.isIgnored(model.IsIgnored);
            self.isNotIgnored(!model.IsIgnored);
            self.actionDateTime(model.ActionDateTime);
            self.actionVisitPayUser(model.ActionVisitPayUser);
            self.enableFromManageSso(model.EnableFromManageSso);
        }
        self.action = function () {
            if (ko.unwrap(self.isAccepted)) {
                self.disable();
            } else {
                self.enable();
            }
        }
        self.ignore = function() {
            $.blockUI();
            $.post('/Sso/Ignore', { ssoProvider: ko.unwrap(self.ssoProviderId), isIgnored: ko.unwrap(self.isIgnored) }, function(response) {
                self.updateFromModel(response);
                $.unblockUI();
            });
        }
        self.disable = function() {
            window.VisitPay.Common.ModalGenericConfirmationMd(self.decline.ContentTitle, self.decline.ContentBody, self.enableFromManageSso() ? acceptText : yesText, self.enableFromManageSso() ? declineText : noText)
                .done(function() {
                    $.post('/Sso/Decline', { ssoProvider: ko.unwrap(self.ssoProviderId) }, function(response) {
                        self.updateFromModel(response);
                        $('.preference').bootstrapToggle('destroy').bootstrapToggle({
                            onstyle: 'vp-toggle',
                            offstyle: 'vp-toggle',
                            on: null,
                            off: null,
                            size: 'small' });
                    });
                });
        }
        self.enable = function () {
            if (window.VisitPay.HealthEquityCommon !== undefined && window.VisitPay.HealthEquityCommon !== null) {
                window.VisitPay.HealthEquityCommon.initNoSsoAgreement(true, self);
            } else if (window.VisitPayMobile.HealthEquityCommon !== undefined && window.VisitPayMobile.HealthEquityCommon !== null) {
                window.VisitPayMobile.HealthEquityCommon.initNoSsoAgreement(self);
            }
        }

        self.isIgnored.subscribe(function (newValue) {
            self.ignore();
        });

        return self;
    };

    var ParentViewModel = function() {
        var self = this;
        self.children = [];
    }

    var init = function (configuration, $scope) {

        var vm = new ParentViewModel();

        $.get(configuration.SettingsUrl, function(data) {
            var results = data;

            $.each(results, function(key, value) {
                vm.children.push(new ViewModel(value));
            });

            ko.applyBindings(vm, $scope[0]);
            $scope.show();

            $('[data-toggle="tooltip"]').tooltip();

            $('.preference').bootstrapToggle({
                onstyle: 'vp-toggle',
                offstyle: 'vp-toggle',
                on: null,
                off: null,
                size: 'small'
            });
            $('.preference').on('change', function() {
                var data = ko.dataFor($(this)[0]);
                $.post('/Sso/Ignore', {
                    ssoProvider: ko.unwrap(data.ssoProviderId),
                    isIgnored: !this.checked
                });
            });

        });

    }

    return {
        init: init
    };
});

﻿(function () {

	function continueSubmit(sender, e) {
		e.preventDefault();
		$.blockUI();

		$.post(sender.attr('action'), sender.serialize(), function (url) {
			window.location.href = url;
		});
	}

	function confirmAction(sender, accept) {
		$.blockUI();

		var $form = sender.parents('form').eq(0);
		var model = $form.serializeArray();
		model.push({ name: 'accept', value: accept });

		$.post($form.attr('action'), $.param(model), function (html) {
			$('#termsModalContainer').html(html);
			var $modal = $('#termsModalContainer .modal');
			$modal.on('hidden.bs.modal', function () {
				$modal.remove();
			});
			$modal.modal('show');

			$modal.on('submit', 'form', function (e) {
				$modal.modal('hide');
				continueSubmit($(this), e);
			});

			$.unblockUI();
		});
	}

	$(document).ready(function () {
		$('#section-sso-terms').on('click', '#btnAccept', function () {
			confirmAction($(this), true);
		});

		$('#section-sso-terms').on('click', '#btnDecline', function () {
			confirmAction($(this), false);
		});
	});
})();
﻿window.VisitPay.NotFound = (function(redirect) {

    $(document).ready(function() {

        var i = 5;
        var interval = setInterval(function() {

            i--;
            $('#redirect-timer').text(Math.max(i, 1));

            if (i === 1) {
                clearInterval(interval);
                interval = null;

                window.location.href = redirect;
            }

        }, 1000);

    });

});
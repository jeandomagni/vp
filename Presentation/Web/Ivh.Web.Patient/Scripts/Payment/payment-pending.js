﻿(function() {
    $(document).ready(function() {
        var vm = {};
        window.VisitPay.VpGridCommon.WithLazyGrid(vm);
        vm.Grid = window.VisitPay.VpGrid(function () { return {}; }, '/Payment/PaymentPending', 100, 'ScheduledPaymentDate', 'asc');
        ko.applyBindings(vm, $('#gridPaymentPending').parent()[0]);
        
        // grid click
        $(document).on('gridrowclick.common', '.grid-payment-pending > .grid-row', function(e, obj) {
            obj.sender.removeClass('active');
            if (!obj.isVisible || $.trim(obj.target.html()).length > 0) {
                return;
            }

            var data = ko.mapping.toJS(ko.dataFor(obj.sender[0]));

            window.VisitPay.PaymentEdit.Initialize(data.PaymentId).done(function(hasChanged) {
                if (hasChanged) {
                    vm.Grid.Search(true);
                }
            });

        });

        // export piwik
        $(document).on('click.exportpending.piwik', '#btnPaymentPendingExport', function() { window._paq.push(['trackEvent', 'Payment Pending', 'Export']); });

        // load
        var isExpandedOnLoad = true;
        vm.IsExpanded.subscribe(function (newValue) {
            if (ko.unwrap(newValue) === true) {
                if (!ko.unwrap(vm.Load.ShouldLoad)) {
                    return;
                }
                var promise = vm.Grid.Search(!isExpandedOnLoad);
                vm.Load.Start(promise);
                promise.done(function() {});
            }
            
            var action = ko.unwrap(newValue) ? 'Show' : 'Hide';
            window._paq.push(['trackEvent', 'Payment Pending', 'Click', action]);

        });
        vm.IsExpanded(isExpandedOnLoad);
    });
})();
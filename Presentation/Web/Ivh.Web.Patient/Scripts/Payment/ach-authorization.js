﻿(function (visitPay, $, ko) {

    visitPay.AchAuthorization = function () {
        
        function loadModal(paymentMethod, financePlanOfferId, financePlanIds, consolidationGuarantorId, financePlanIdToReconfigure) {
            var promise = $.Deferred();
            var model = {
                ConsolidationGuarantorId: consolidationGuarantorId,
                PaymentMethodId: ko.unwrap(paymentMethod.PaymentMethodId),
                FinancePlanOfferId: ko.unwrap(financePlanOfferId),
                FinancePlanIds: financePlanIds,
                FinancePlanIdToReconfigure: financePlanIdToReconfigure
            };

            $.blockUI();
            $.post('/Payment/AchAuthorization', model, function (html) {
                if (html == null || html.length === 0) {
                    promise.resolve(null);
                } else {
                    var $modal = window.VisitPay.Common.Modal('modalAchAuthorization', html, true);
                    promise.resolve($modal);
                }
            });

            return promise;
        }

        function submit($form, paymentMethodId, financePlanOfferId, financePlanIds, consolidationGuarantorId, financePlanIdToReconfigure) {

            var promise = $.Deferred();
            var model = {
                AchAuthorizationCmsVersionId: $form.find('#AchAuthorizationCmsVersionId').val(),
                ConsolidationGuarantorId: consolidationGuarantorId,
                FinancePlanOfferId: financePlanOfferId,
                FinancePlanIds: financePlanIds,
                PaymentMethodId: paymentMethodId,
                FinancePlanIdToReconfigure: financePlanIdToReconfigure
            };

            $.post($form.attr('action'), model, function () {
                promise.resolve();
            });

            return promise;
            
        }

        function promptAll(paymentMethod, financePlanOfferId, financePlanIds, consolidationGuarantorId, financePlanIdToReconfigure) {

            if (!paymentMethod || !ko.unwrap(paymentMethod.IsAchType)) {
                // no acknowledgement necessary
                return $.Deferred().resolve();
            }

            var promise = $.Deferred();
            $.blockUI();
            loadModal(paymentMethod, financePlanOfferId, financePlanIds, consolidationGuarantorId, financePlanIdToReconfigure).done(function ($modal) {

                if ($modal == null) {
                    // no acknowledgement necessary
                    promise.resolve();
                    $.unblockUI();

                } else {
                    var $form = $modal.find('#frmAchAuthorization');
                    $form.off('submit').on('submit', function (e) {
                        e.preventDefault();
                        submit($form, ko.unwrap(paymentMethod.PaymentMethodId), financePlanOfferId, financePlanIds, consolidationGuarantorId, financePlanIdToReconfigure).done(function () {
                            $modal.modal('hide');
                            promise.resolve();
                        });
                    });

                    var $dismiss = $modal.find('[data-dismiss="modal"]');
                    $dismiss.off('click.dismiss').on('click.dismiss', function (e) {
                        promise.reject();
                    });

                    var model = {
                        IsAccepted: ko.observable(false)
                    };
                    ko.applyBindings(model, $form[0]);
                    $.unblockUI();

                }
            });

            return promise;

        }

        function prompt(paymentMethod) {
            return promptAll(paymentMethod, null, null, null, null);
        }

        function promptConsolidation(paymentMethod, consolidationGuarantorId) {
            return promptAll(paymentMethod, null, null, consolidationGuarantorId, null);
        }

        function promptFinancePlan(paymentMethod, financePlanIds) {
            return promptAll(paymentMethod, null, financePlanIds, null, null);
        }

        function promptOffer(paymentMethod, financePlanOfferId) {
            return promptAll(paymentMethod, financePlanOfferId, null, null, null);
        }

        function promptReconfig(paymentMethod, financePlanIdToReconfigure, financePlanOfferId) {
            return promptAll(paymentMethod, financePlanOfferId, null, null, financePlanIdToReconfigure);
        }

        return {
            prompt: prompt,
            promptConsolidation: promptConsolidation,
            promptOffer: promptOffer,
            promptFinancePlan: promptFinancePlan,
            promptReconfig: promptReconfig
        }

    };

})(window.VisitPay = window.VisitPay || {}, jQuery, window.ko = window.ko || {});
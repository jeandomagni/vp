﻿(function(arrangePayment) {

    var PaymentMethodsWrapperViewModel = function(currentVisitPayUserId) {

        var self = this;

        //
        self.PaymentMethods = new window.VisitPay.paymentMethodsViewModel(currentVisitPayUserId);
        
        function formatDisplay(target, withImage) {

            var str = '<strong>' + ko.unwrap(target.DisplayName) + '</strong>';

            withImage = withImage === undefined || withImage === null ? true : withImage;
            if (!withImage) {
                return str;
            }

            var html = '<img src="' + ko.unwrap(target.DisplayImage) + '" alt=""/>' + str;

            if (target.PaymentMethodProviderType &&
                target.PaymentMethodProviderType.DisplayImage &&
                ko.unwrap(target.PaymentMethodProviderType.DisplayImage).length > 0) {
                return '<img class="logo-provider" src="' + ko.unwrap(target.PaymentMethodProviderType.DisplayImage) + '" alt=""/>' + html;
            }

            return html;
        }

        //
        self.SelectedPaymentMethod = ko.computed(function () {
            return ko.unwrap(self.PaymentMethods.SelectedPaymentMethod);
        });
        self.SelectedPaymentMethod.Display = ko.computed(function() {
            var selected = ko.unwrap(self.SelectedPaymentMethod);
            if (!selected) {
                return 'Select a Payment Method';
            }

            return formatDisplay(selected, null);
        });

        //
        self.PrimaryPaymentMethod = ko.computed(function () {
            return ko.unwrap(self.PaymentMethods.PrimaryPaymentMethod());
        });
        self.PrimaryPaymentMethod.Display = ko.computed(function() {
            var primary = ko.unwrap(self.PrimaryPaymentMethod);
            if (!primary) {
                return 'Select a Payment Method';
            }
            return formatDisplay(primary);
        });
        self.PrimaryPaymentMethod.DisplayShort = ko.computed(function () {
            var primary = ko.unwrap(self.PrimaryPaymentMethod);
            if (!primary) {
                return 'Select a Payment Method';
            }
            return formatDisplay(primary, false);
        });

        //
        self.IsChangePrimary = ko.observable(false);

        // set primary enabled
        self.IsSetPrimaryEnabled = ko.observable(false);

        self.ShowModalAfterChangePrimary = ko.observable(false);
        self.ShowModalAfterInitialPaymentMethodAdded = ko.observable(false);

        return self;

    };

    arrangePayment.PaymentMethodsModule = (function (currentVisitPayUserId) {

        var $scope = $('#section-paymentmethods');

        //
        var vm = new PaymentMethodsWrapperViewModel(currentVisitPayUserId);
        vm.SelectedPaymentMethod.subscribeChanged(function (newValue, oldValue) {
            if (!vm.IsChangePrimary()) {
                if (!newValue || !ko.unwrap(newValue)) {
                    return;
                }

                hide();
            }

        });
        vm.PrimaryPaymentMethod.subscribeChanged(function (newValue, oldValue) {
            if (vm.IsChangePrimary()) {
                if (!newValue || !ko.unwrap(newValue)) {
                    hide();
                    return;
                }

                if (((oldValue && newValue.PaymentMethodId() == oldValue.PaymentMethodId()) || !oldValue)) {
                    //Payment method wasn't changed, don't hide
                    return;
                }
                
                hide();
            }

        });
        $.each($('[data-context="paymentmethods"]'), function() {
            ko.applyBindings(vm, $(this)[0]);
        });

        //
        function changePaymentMethod() {
            $('#modalPaymentMethods').modal();
        }
        function changePrimary() {
            vm.IsChangePrimary(true);
            $('#modalPaymentMethods').modal();
        };
        function hide() {
            vm.IsChangePrimary(false);
            $('#modalPaymentMethods').modal('hide');
        };
        function selectPrimary() {
            vm.PaymentMethods.SelectedPaymentMethod(ko.unwrap(vm.PrimaryPaymentMethod));
        };
        function setSingleUse(date) {

            if (!date || !ko.unwrap(date)) {
                vm.PaymentMethods.AllowSingleUse(false);
                return;
            }

            var isPromptPay = moment().isSame(moment(ko.unwrap(date), 'MM/DD/YYYY'), 'day');
            vm.PaymentMethods.AllowSingleUse(isPromptPay);
        };
        function setPrimaryEnabled(enabled) {
            vm.IsSetPrimaryEnabled(enabled);
        }
        function show() {
            if (!$('#modalPaymentMethods').is(':visible')) {
                $('#modalPaymentMethods').modal();
            }
        };
        function unselect() {
            vm.PaymentMethods.SelectedPaymentMethod(null);
        };
        function visible() {
            return $scope.is(':visible');
        };

        //
        $scope.on('click', '.account-item [id*="AccountSelect_"]', function () {
            if (!vm.IsChangePrimary()) {
                hide();
            }
        });

        return {
            changePaymentMethod: changePaymentMethod,
            changePrimary: changePrimary,
            hide: hide,
            paymentMethods: vm,
            selectPrimary: selectPrimary,
            setSingleUse: setSingleUse,
            setPrimaryEnabled: setPrimaryEnabled,
            setup: function (callback) {
                if (callback != null && typeof callback === 'function') {
                    callback(vm);
                }
            },
            show: show,
            unselect: unselect,
            visible: visible,
            init: function () {
                return window.VisitPay.PaymentMethods(vm, $scope);
            }
        };

    });

})(window.VisitPay.ArrangePayment = window.VisitPay.ArrangePayment || {});
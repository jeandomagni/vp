﻿(function (arrangePayment) {

    var paymentOptionEnum = {
        Resubmit: 1,
        FinancePlan: 4,
        SpecificAmount: 5,
        SpecificVisits: 6,
        SpecificFinancePlans: 7,
        AccountBalance: 8,
        CurrentNonFinancedBalance: 9,
        HouseholdCurrentNonFinancedBalance: 10
    };

    var PaymentMenuViewModel = function () {

        var self = this;
        var allMenuItems = [];

        //
        function map(source, setSelected) {

            allMenuItems = [];
            ko.utils.arrayForEach(source.PaymentMenuItems, function (item) {
                allMenuItems.push(item);
                if (item.PaymentMenuItems && item.PaymentMenuItems.length > 0) {
                    allMenuItems.push.apply(allMenuItems, item.PaymentMenuItems);
                }
            });

            //
            ko.mapping.fromJS(source.PaymentMenuItems, {}, self.PaymentMenuItems);
            if (setSelected) {
                self.SelectedPaymentMenuItemId(source.SelectedPaymentMenuItemId);
            } else {
                self.SelectedPaymentMenuItemId(null);
            }

            //
            self.HasOptionsEnabled(ko.utils.arrayFirst(allMenuItems, function (item) {
                return item.IsEnabled === true;
            }));
        };

        // selected
        self.SelectedPaymentMenuItemId = ko.observable();
        self.SelectedPaymentMenuItemId.subscribe(function (newValue) {

            var selectedId = ko.unwrap(newValue);
            var foundItem = ko.utils.arrayFirst(allMenuItems, function(item) {
                return item.PaymentMenuItemId === selectedId;
            });

            if (!foundItem) {
                self.PaymentMenuItems.Selected(null);
                return;
            }

            var paymentMenuItems = foundItem.PaymentMenuItems;

            if (paymentMenuItems && paymentMenuItems.length > 0) {
                var firstEnabledItem = ko.utils.arrayFirst(ko.unwrap(paymentMenuItems), function(item) { return ko.unwrap(item.IsEnabled) === true; });
                if (firstEnabledItem) {
                    self.SelectedPaymentMenuItemId(ko.unwrap(firstEnabledItem.PaymentMenuItemId));
                }
            } else {
                self.PaymentMenuItems.Selected(ko.utils.arrayFirst(allMenuItems, function(item) {
                    return item.PaymentMenuItemId === selectedId;
                }));
            }

        });

        // menu items
        self.PaymentMenuItems = ko.observableArray([]).extend({ extendedArray: {} });
        self.PaymentMenuItems.Selected = ko.observable();
        self.PaymentMenuItems.Selected.Any = ko.computed(function () {
            var selected = ko.unwrap(self.PaymentMenuItems.Selected);
            return selected !== undefined && selected !== null;
        });
        self.PaymentMenuItems.Selected.Is = function (paymentMenuItemId) {
            if (!self.PaymentMenuItems.Selected.Any()) {
                return false;
            }

            var checkValue = ko.unwrap(paymentMenuItemId);
            var selected = self.PaymentMenuItems.Selected();

            return checkValue === ko.unwrap(selected.PaymentMenuItemId) || checkValue === ko.unwrap(selected.ParentPaymentMenuItemId);
        };
        self.PaymentMenuItems.Selected.Parent = ko.computed(function() {

            var selected = ko.unwrap(self.PaymentMenuItems.Selected);
            if (!selected) {
                return {};
            }

            var parentId = ko.unwrap(selected.ParentPaymentMenuItemId);
            if (!$.isNumeric(parentId)) {
                return selected;
            }

            var parentItem = ko.utils.arrayFirst(allMenuItems, function (item) {
                return item.PaymentMenuItemId === parentId;
            });
            return parentItem || {};

        });
        self.HasOptionsEnabled = ko.observable(false);

        //
        self.Init = function (data, setSelected) {
            map(data, setSelected);
            return $.Deferred().resolve();
        };
        self.SetPaymentOption = function (paymentOptionId) {

            if (paymentOptionId === undefined || paymentOptionId === null) {
                // select first enabled
                ko.utils.arrayFirst(allMenuItems, function(item) {
                    if (item.IsEnabled === true) {
                        self.SelectedPaymentMenuItemId(item.PaymentMenuItemId);
                        return true;
                    }
                    return false;
                });
            } else {
                // select specified
                ko.utils.arrayFirst(allMenuItems, function(item) {
                    if (item.IsEnabled === true && item.PaymentOptionId === ko.unwrap(paymentOptionId)) {
                        self.SelectedPaymentMenuItemId(item.PaymentMenuItemId);
                        return true;
                    }
                    return false;
                });
            }

        };

        // Finance Plan Offer
        self.financePlanOffer = ko.observable();
        self.financePlanOffer.reset = function () {
            self.financePlanOffer(null);
        };
        self.ShowFinancePlanOffer = function (financePlanOfferDetails) {
            //Set the FP offer so we can pass it into the finance plan module when initializing it
            self.financePlanOffer(financePlanOfferDetails);

            //Switch to FP tab, the FP offer will be passed in to the FP module when it inits after changing payment option
            self.SetPaymentOption(paymentOptionEnum.FinancePlan);
        };

        //
        return self;

    };

    var ReceiptViewModel = function () {

        var self = this;
        self.messenger = new ko.subscribable();

        self.HomeButtonText = ko.observable();
        self.ReceiptText = ko.observable();
        self.ShowMakeAnotherPayment = ko.observable();
        self.IsVisible = ko.computed(function () {
            return ko.unwrap(self.ReceiptText) != undefined && ko.unwrap(self.ReceiptText).length > 0;
        });

        self.GoHome = function () {
            self.messenger.notifySubscribers(null, 'goHome');
        };
        self.SetDefaultPaymentOption = function () {
            self.messenger.notifySubscribers(null, 'setPaymentOption');
        };

        self.Clear = function () {
            self.ReceiptText('');
            self.ShowMakeAnotherPayment(true);
        }
        self.Clear();

        return self;

    };

    arrangePayment.Main = (function (data) {

        function setAdditionalParameters(sourceData) {
            var p = {};
            if (sourceData.VisitPayUserId !== undefined && sourceData.VisitPayUserId !== null) {
                p['currentVisitPayUserId'] = data.VisitPayUserId;
            }
            return p;
        };

        var paymentMenuVm = new PaymentMenuViewModel();
        var receiptVm = new ReceiptViewModel();
        var additionalParameters = setAdditionalParameters(data);

        function showReceipt(receiptText, hasEnabledOptions) {
            $('.arrange-payment-module').hide();
            $('#receipt').show();
            receiptVm.Clear();
            receiptVm.ReceiptText(receiptText);
            receiptVm.HomeButtonText(data.HomeButtonText);
            receiptVm.ShowMakeAnotherPayment(ko.unwrap(hasEnabledOptions));
        };

        function goHome() {
            window.location.href = data.HomeUrl;
        };

        function refreshData() {

            var promise = $.Deferred();

            $.post(data.DataUrl, additionalParameters, function (newData) {

                data = newData;
                additionalParameters = setAdditionalParameters(data);

                var promiseAlerts = $.Deferred();
                promiseAlerts.done(function () {
                    paymentMenuVm.Init(newData.PaymentMenu, false);
                    promise.resolve();
                });

                if ($.isFunction(window.VisitPay.Common.RefreshAlerts)) {
                    window.VisitPay.Common.RefreshAlerts().done(function() { promiseAlerts.resolve(); });
                } else {
                    promiseAlerts.resolve();
                }

            });

            return promise;

        };

        function paymentComplete(receiptText) {

            var selectedMenu = ko.unwrap(paymentMenuVm.PaymentMenuItems.Selected);
            window._paq.push(['trackEvent', 'Payments', 'Completion', ko.unwrap(selectedMenu.Title)]);

            var promise = $.Deferred();
            refreshData().done(function () {
                showReceipt(receiptText, paymentMenuVm.HasOptionsEnabled());
                promise.resolve();
            });

            return promise;

        };

        $(document).ready(function () {
            log.debug('payment options on ready: ', data.PaymentOptions);
            // scroll to below the header on load
            $(document).on('scrollTo.arrange-payment', function () {
                $(document).off('scrollTo.arrange-payment');
                $('html, body').animate({
                    scrollTop: $('#header').outerHeight() + 30
                }, 500);
            });

            // payment menu
            $.each($('[data-context="menu"]'), function () {
                ko.applyBindings(paymentMenuVm, $(this)[0]);
            });

            // receipt
            $.each($('[data-context="receipt"]'), function () {
                ko.applyBindings(receiptVm, $(this)[0]);
            });

            // payment method module
            var paymentMethodsModule = new arrangePayment.PaymentMethodsModule(data.VisitPayUserId);

            // payment module
            var paymentModule = new arrangePayment.PaymentModule(paymentMethodsModule, {
                submitUrl: data.PaymentSubmitUrl,
                validateUrl: data.PaymentValidateUrl,
                onPaymentComplete: paymentComplete,
                additionalParameters: additionalParameters
            });

            // finance plan module
            var financePlanModule = new arrangePayment.FinancePlanModule(paymentMethodsModule, {
                submitUrl: data.FinancePlanSubmitUrl,
                validateUrl: data.FinancePlanValidateUrl,
                onPaymentComplete: paymentComplete,
                additionalParameters: additionalParameters
            });

            //finance plan state selection module
            var financePlanStateSelectionModule = new arrangePayment.FinancePlanStateSelectionModule(paymentMethodsModule, {});

            var selectPaymentOption = function(newValue) {
                // reset all modules
                paymentModule.init();
                financePlanModule.init();
                paymentMethodsModule.hide(false);
                receiptVm.Clear();
                financePlanStateSelectionModule.init();

                if (!newValue) {
                    return;
                }

                var selectedPaymentOptionId = ko.unwrap(newValue.PaymentOptionId);
                if (!selectedPaymentOptionId) {
                    return;
                }

                if (selectedPaymentOptionId !== paymentOptionEnum.FinancePlan) {
                    //Non-FP option selected
                    var selectedPaymentOption = ko.utils.arrayFirst(data.PaymentOptions, function(item) {
                        return item.PaymentOptionId === selectedPaymentOptionId;
                    });

                    if (selectedPaymentOption) {
                        // piwik
                        window._paq.push(['trackEvent', 'Payments', 'Arrival', ko.unwrap(selectedPaymentOption.Title)]);

                        // hide modules
                        $('.arrange-payment-module').hide();

                        // init payment module
                        $('#payment-submit-container').show();
                        paymentMethodsModule.setPrimaryEnabled(false);
                        paymentModule.init(selectedPaymentOption);

                        $(document).trigger('scrollTo.arrange-payment');

                        // tooltips
                        $('[data-toggle="tooltip"]').tooltip({ animation: false, container: 'body' });
                        $('[data-toggle="tooltip-html"]').tooltipHtml();

                    }
                } else {
                    //FP option selected
                    var financePlanPaymentOptions = [];

                    //If a specific state code is provided, choose that. Otherwise get all FP options
                    if (newValue.FinancePlanOptionStateCode) {
                        //Specific state selected
                        var fpOptionForState = ko.utils.arrayFirst(data.PaymentOptions, function(item) {
                            return item.PaymentOptionId === selectedPaymentOptionId && item.FinancePlanOptionStateCode === newValue.FinancePlanOptionStateCode;
                        });
                        financePlanPaymentOptions.push(fpOptionForState);
                    } else {
                        ko.utils.arrayForEach(data.PaymentOptions, function(item) {
                            if (item.PaymentOptionId === selectedPaymentOptionId) {
                                financePlanPaymentOptions.push(item);
                            }
                        });
                    }

                    //If more than one FP option, load state selection module. Otherwise load regular FP module
                    if (financePlanPaymentOptions.length > 1) {
                        //piwik
                        window._paq.push(['trackEvent', 'Payments', 'Arrival', ko.unwrap(financePlanPaymentOptions[0].Title)]);

                        //Hide modules
                        $('.arrange-payment-module').hide();

                        //Have multiple state options, load state selection module
                        $('#finance-plan-state-selection').show();
                        financePlanStateSelectionModule.init(financePlanPaymentOptions, ko.unwrap(paymentMenuVm.financePlanOffer));
                    } else {
                        // piwik
                        window._paq.push(['trackEvent', 'Payments', 'Arrival', ko.unwrap(financePlanPaymentOptions[0].Title)]);

                        // hide modules
                        $('.arrange-payment-module').hide();

                        //Only have FPs for a single state, load FP module
                        $('#finance-plan').show();
                        paymentMethodsModule.setPrimaryEnabled(true);

                        financePlanModule.init(financePlanPaymentOptions[0], ko.unwrap(paymentMenuVm.financePlanOffer));
                        paymentMenuVm.financePlanOffer.reset(); //Reset FP offer so it's cleared out if tabs change
                    }
                }
            };

            // subscribe to things
            paymentModule.messenger.subscribe(paymentMethodsModule.changePaymentMethod, null, 'changePaymentMethod');
            paymentModule.messenger.subscribe(paymentMenuVm.SetPaymentOption, null, 'setPaymentOption');
            paymentModule.messenger.subscribe(paymentMenuVm.ShowFinancePlanOffer, null, 'showFinancePlanOffer');
            receiptVm.messenger.subscribe(paymentMenuVm.SetPaymentOption, null, 'setPaymentOption');
            receiptVm.messenger.subscribe(goHome, null, 'goHome');
            financePlanStateSelectionModule.messenger.subscribe(selectPaymentOption, null, 'financePlanStateSelected');
            financePlanModule.messenger.subscribe(selectPaymentOption, null, 'financePlanChangeState');

            // subscribe to menu change
            paymentMenuVm.PaymentMenuItems.Selected.subscribe(selectPaymentOption);

            // init
            $('.arrange-payment-module').hide();
            $.when(paymentMenuVm.Init(data.PaymentMenu, true), paymentMethodsModule.init()).done(function() {
                if (!paymentMenuVm.HasOptionsEnabled()) {
                    $('#noOptions').show();
                }
                $('#arrange-payment').css('visibility', 'visible');
            });

            // on guarantor filter change
            $(document).on('guarantorFilterChanged', function (e) {
                window._paq.push(['trackEvent', 'Payments', 'Arrival', 'VPG Filter']);
                $.blockUI();
                if (window.location.href.indexOf('?') === -1 || !e.obfuscatedVpGuarantorId) {
                    window.location.reload();
                } else {
                    //Have query params, switch them to the newly selected guarantor
                    var params = [];
                    var kvps = window.location.href.split('?')[1].split('&');
                    for (var i = 0; i < kvps.length; i++) {
                        var key = kvps[i].split('=')[0];
                        if (key === 'v') {
                            continue;
                        }
                        params.push(kvps[i]);
                    }
                    window.location.href = (window.location.href.split('?')[0] + '?v=' + e.obfuscatedVpGuarantorId + '&' + params.join('&')).replace('#', '');
                }
            });

        });

    });

})(window.VisitPay.ArrangePayment = window.VisitPay.ArrangePayment || {});
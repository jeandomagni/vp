﻿window.VisitPay.PaymentMethodListItemVm = (function() {

    var self = this;

    self.BankName = ko.observable();

    self.PaymentMethodId = ko.observable();
    self.PaymentMethodId.Any = ko.computed(function () {
        return ko.unwrap(self.PaymentMethodId) && ko.unwrap(self.PaymentMethodId).toString().length > 0 && parseInt(ko.unwrap(self.PaymentMethodId)) > 0;
    });

    self.DisplayAccountTypeText = ko.observable();
    self.DisplayProviderTypeText = ko.observable();
    self.DisplayProviderTypeImage = ko.observable();
    self.DisplayProviderTypeImage.Any = ko.computed(function () {
        return ko.unwrap(self.DisplayProviderTypeImage) !== undefined && ko.unwrap(self.DisplayProviderTypeImage) !== null && ko.unwrap(self.DisplayProviderTypeImage).length > 0;
    });
    self.DisplayAccountTypeText.WithProvider = ko.computed(function () {

        var account = ko.unwrap(self.DisplayAccountTypeText);
        var accountText = account !== undefined && account !== null && account.length > 0 ? account : '';

        var provider = ko.unwrap(self.DisplayProviderTypeText);
        var providerText = provider !== undefined && provider !== null && provider.length > 0 ? provider : '';

        if (accountText.length > 0 && providerText.length > 0) {
            return providerText + ' ' + accountText;
        }

        return '';

    });

    self.PaymentMethodType = ko.observable();
    self.AccountNickName = ko.observable();
    self.AccountNickName.Any = ko.computed(function () {
        return ko.unwrap(self.AccountNickName) && ko.unwrap(self.AccountNickName).length > 0;
    });
    self.DisplayImage = ko.observable();
    self.DisplayName = ko.observable();
    self.DisplayName.Any = ko.computed(function() {
        return ko.unwrap(self.DisplayName) && ko.unwrap(self.DisplayName).length > 0;
    });
    self.ExpDateForDisplay = ko.observable();
    self.ExpDateForDisplay.Display = ko.computed(function() {
        return 'exp. ' + ko.unwrap(self.ExpDateForDisplay);
    });
    self.ExpirationMessage = ko.observable();
    self.ExpirationMessage.Any = ko.computed(function() {
        return ko.unwrap(self.ExpirationMessage) && ko.unwrap(self.ExpirationMessage).length > 0;
    });
    self.IsAchType = ko.observable();
    self.IsCardType = ko.observable();
    self.IsActive = ko.observable();
    self.IsPrimary = ko.observable();
    self.IsRemoveable = ko.observable();
    self.LastFour = ko.observable();
    self.LastFour.Display = ko.computed(function() {
        var lastFour = ko.unwrap(self.LastFour);
        if (lastFour && lastFour.length > 0) {
            return 'X-' + ko.unwrap(self.LastFour);
        }
        return '';
    });
    self.RemoveableMessage = ko.observable();
    self.RemoveableMessage.Display = ko.computed(function() {
        return ko.unwrap(self.IsRemoveable) ? window.VisitPay.Localization.RemovePaymentMethod || 'Remove Payment Method' : ko.unwrap(self.RemoveableMessage);
    });
    self.Amount = ko.observable();
    
    return self;

});

window.VisitPay.PaymentMethodVm = (function(allowSingleUse) {

    var self = this;

    //
    self.AllowSingleUse = ko.observable(allowSingleUse);
    self.HasUserAcceptedSsoTerms = ko.observable();

    // shared
    self.AccountNickName = ko.observable();
    self.BillingId = ko.observable();
    self.AvsCode = ko.observable();
    self.ExpirationMessage = ko.observable();
    self.ExpirationMessage.Any = ko.computed(function() {
        return ko.unwrap(self.ExpirationMessage) && ko.unwrap(self.ExpirationMessage).length > 0;
    });
    self.GatewayToken = ko.observable();
    self.IsActive = ko.observable(true);
    self.IsPrimary = ko.observable();
    self.IsRemoveable = ko.observable();
    self.LastFour = ko.observable();
    self.PaymentMethodId = ko.observable(0);
    self.PaymentMethodId.IsNew = ko.computed(function () {
        return ko.unwrap(self.PaymentMethodId) === 0;
    });
    self.PaymentMethodId.SaveEnabled = ko.computed(function() {
        var isNew = ko.unwrap(self.PaymentMethodId.IsNew);
        if (isNew !== undefined && isNew !== null && isNew === false) {
            return true;
        }
        if (window.VisitPay.State.IsEmulation) {
            return false;
        }
        return true;
    });
    self.PaymentMethodTypeId = ko.observable();
    self.PaymentMethodTypeId.IsAchType = ko.computed(function() {
        var paymentMethodTypeId = ko.unwrap(self.PaymentMethodTypeId);
        return paymentMethodTypeId >= 5 && paymentMethodTypeId <= 6;
    });
    self.PaymentMethodTypeId.IsCardType = ko.computed(function() {
        var paymentMethodTypeId = ko.unwrap(self.PaymentMethodTypeId);
        return paymentMethodTypeId >= 1 && paymentMethodTypeId <= 4;
    });
    self.PaymentMethodTypeId.IsAmex = ko.computed(function() {
        return ko.unwrap(self.PaymentMethodTypeId) === 3;
    });
    self.PaymentMethodTypeId.IsDiscover = ko.computed(function() {
        return ko.unwrap(self.PaymentMethodTypeId) === 4;
    });
    self.PaymentMethodTypeId.IsMastercard = ko.computed(function() {
        return ko.unwrap(self.PaymentMethodTypeId) === 2;
    });
    self.PaymentMethodTypeId.IsVisa = ko.computed(function() {
        return ko.unwrap(self.PaymentMethodTypeId) === 1;
    });
    self.PaymentMethodTypeId.IsChecking = ko.computed(function() {
        return ko.unwrap(self.PaymentMethodTypeId) === 5;
    });
    self.PaymentMethodTypeId.IsSavings = ko.computed(function() {
        return ko.unwrap(self.PaymentMethodTypeId) === 6;
    });

    // ach specific
    self.AccountNumber = ko.observable();
    self.AccountNumberConfirm = ko.observable();
    self.AchTypeString = ko.observable();
    self.BankName = ko.observable();
    self.FirstName = ko.observable();
    self.LastName = ko.observable();
    self.RoutingNumber = ko.observable();

    // card specific
    self.AddressLine1 = ko.observable();
    self.AddressLine2 = ko.observable();
    self.CardCode = ko.observable();
    self.CardCodeRequired = ko.observable(true);
    self.City = ko.observable();
    self.ExpirationMonth = ko.observable();
    self.ExpirationYear = ko.observable();

    self.AccountTypeId = ko.observable();
    self.ProviderTypeId = ko.observable();

    self.NameOnCard = ko.observable();
    self.PaymentMethodAccountTypes = ko.observable();
    self.PaymentMethodProviderTypes = ko.observable();
    self.State = ko.observable();
    self.Zip = ko.observable();

    // mobile
    self.PaymentMethodParentType = ko.observable(0);
    self.PaymentMethodParentType.Any = ko.computed(function () {
        return parseInt(ko.unwrap(self.PaymentMethodParentType)) > 0;
    });
    self.PaymentMethodParentType.IsAchType = ko.computed(function() {
        return parseInt(ko.unwrap(self.PaymentMethodParentType)) === 2;
    });
    self.PaymentMethodParentType.IsCardType = ko.computed(function () {
        return parseInt(ko.unwrap(self.PaymentMethodParentType)) === 1;
    });
    self.PaymentMethodParentType.SetValue = ko.observable(0);

    return self;

});

window.VisitPay.PaymentMethodSelectListVm = (function () {
    var self = this;

    self.PaymentMethodId = ko.observable();
    self.LastFour = ko.observable();
    self.LastFour.Display = ko.computed(function () {
        return 'X-' + ko.unwrap(self.LastFour);
    });
    self.DisplayName = ko.observable();
    self.DisplayName.Any = ko.computed(function () {
        return ko.unwrap(self.DisplayName) && ko.unwrap(self.DisplayName).length > 0;
    });
    self.DisplayNameDisplay = ko.computed(function () {
        return ko.unwrap(self.DisplayName.Any) ? ko.unwrap(self.DisplayName) : ko.unwrap(self.LastFour.Display);
    });

    return self;
});

window.VisitPay.PaymentMethodAccountTypeVm = (function () {
    var self = this;

    self.PaymentMethodAccountTypeId = ko.observable();
    self.PaymentMethodAccountTypeListDisplayText = ko.observable();
    self.PaymentMethodAccountTypeSelectedDisplayText = ko.observable();
    self.DisplayOrder = ko.observable();
    return self;
});

window.VisitPay.PaymentMethodProviderTypeVm = (function () {
    var self = this;

    self.PaymentMethodProviderTypeId = ko.observable();
    self.PaymentMethodProviderTypeText = ko.observable();
    self.PaymentMethodProviderTypeDisplayText = ko.observable();
    self.ImageName = ko.observable();
    self.DisplayImage = ko.observable();
    self.DisplayOrder = ko.observable();

    self.DisplayImage.Any = ko.computed(function () {
        return ko.unwrap(self.DisplayImage).length > 0;
    });

    return self;
});

window.VisitPay.PaymentAccountProviderHelpers = (function () {

    function accountTypeChanged($selectedAccountTypeId, $selectedProviderTypeId) {
        var isNonOfTheAbove = $selectedAccountTypeId.find('option').first().prop('selected') || $selectedAccountTypeId.find('option').last().prop('selected');
        if (isNonOfTheAbove) {
            $selectedProviderTypeId.find('option:eq(0)').prop('selected', true);
            $selectedProviderTypeId.prop('disabled', true);
        } else {
            $selectedProviderTypeId.prop('disabled', false);
        }
    }

    return {
        accountTypeChanged: accountTypeChanged
    };

})();
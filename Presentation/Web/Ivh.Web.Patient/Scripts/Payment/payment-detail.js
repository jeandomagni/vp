﻿window.VisitPay.PaymentDetail = (function() {

    var self = {};

    self.Initialize = function(paymentProcessorResponseId) {

        //
        var $scope = $('.payment-details');

        $scope.find('a[data-visitid]').off('click').on('click', function(e) {
            e.preventDefault();
            var visitId = $(this).attr('data-visitid');
            var details = window.VisitPay.VisitDetails();
            details.init(visitId);
        });

        $scope.find('#btnPaymentDetailExport').on('click', function(e) {
            e.preventDefault();
            $.post('/Payment/PaymentDetailExport', {
                paymentProcessorResponseId: paymentProcessorResponseId
            }, function(result) {
                if (result.Result === true) {
                    window.location.href = result.Message;
                }
                window._paq.push(['trackEvent', 'Payment', 'Export']);
            }, 'json');
        });

    };

    return self;

})();
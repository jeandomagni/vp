﻿(function() {
    $(document).ready(function() {
        var vm = {};
        window.VisitPay.VpGridCommon.WithLazyGrid(vm);
        vm.Grid = window.VisitPay.VpGrid(function() { return {}; }, '/Payment/PaymentHistory', 10, 'InsertDate', 'desc');
        ko.applyBindings(vm, $('#gridPaymentHistory').parent()[0]);

        // grid click
        $(document).on('gridrowclick.common', '.grid-payment-history > .grid-row', function(e, obj) {
            if (!obj.isVisible || $.trim(obj.target.html()).length > 0) {
                return;
            }

            var data = ko.mapping.toJS(ko.dataFor(obj.sender[0]));
            $.post('/Payment/PaymentDetail', { paymentProcessorResponseId: data.PaymentProcessorResponseId }, function(html) {
                obj.target.html(html);
                window.VisitPay.PaymentDetail.Initialize(data.PaymentProcessorResponseId);
                $.unblockUI();
            });
        });
        
        // export piwik
        $(document).on('click.exporthistory.piwik', '#btnPaymentHistoryExport', function() { window._paq.push(['trackEvent', 'Payment History', 'Export']); });
        
        // load
        var isExpandedOnLoad = false;
        vm.IsExpanded.subscribe(function (newValue) {
            if (ko.unwrap(newValue) === true) {
                if (!ko.unwrap(vm.Load.ShouldLoad)) {
                    return;
                }
                var promise = vm.Grid.Search(!isExpandedOnLoad);
                vm.Load.Start(promise);
                promise.done(function() {});
            }
            
            var action = ko.unwrap(newValue) ? 'Show' : 'Hide';
            window._paq.push(['trackEvent', 'Payment History', 'Click', action]);

        });
        vm.IsExpanded(isExpandedOnLoad);
        
        // guarantor filter
        $(document).on('guarantorFilterChanged', function(e) {
            $.blockUI();
            window.location.reload(true);
        });
    });
})();
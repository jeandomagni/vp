﻿(function (arrangePayment) {

    function formatDecimal(value) {
        if (value === undefined || value === null || isNaN(value)) {
            return value;
        }
        return parseFloat(value).toFixed(2);
    }

    var FinancePlanViewModel = function (initialData, primaryPaymentMethod, messenger) {

        var self = new window.VisitPay.FinancePlanSetupBaseViewModel(initialData);

        //
        self.IsVisible = ko.observable(false);

        //
        self.AmountToFinance = ko.observable();
        self.FinancePlanId = ko.observable();
        self.FinancePlanTotalMonthlyPaymentAmount = ko.observable();
        self.HasPendingResubmitPayments = ko.observable();
        self.StatementId = ko.observable();
        self.UserApplicableBalance = ko.observable();
        self.FinancePlanOptionStateCode = ko.observable(null);
        self.FinancePlanOptionStateName = ko.observable(null);
        self.OtherFinancePlanStatesAreAvailable = ko.observable(false);
        self.PaymentOptionId = ko.observable(null);

        //
        self.PaymentDueDay = ko.observable();
        self.PaymentDueDay.IsActive = ko.observable(false);
        self.PaymentDueDay.IsEnabled = ko.observable(false);
        self.PaymentDueDay.Toggle = function () {
            self.PaymentDueDay.IsActive(!ko.unwrap(self.PaymentDueDay.IsActive));
        };

        //
        self.PrimaryPaymentMethod = primaryPaymentMethod;
        self.PrimaryPaymentMethod.Any = ko.computed(function () {
            var primary = ko.unwrap(self.PrimaryPaymentMethod);
            return primary && ko.unwrap(primary.PaymentMethodId) > 0;
        });

        //
        self.Options = ko.observableArray([]);
        self.Option = ko.observable();
        self.Option.Is = function (s) {
            var option = ko.unwrap(self.Option);
            if (option === undefined || option === null) {
                return false;
            }
            return parseInt(option) === parseInt(s);
        };
        self.Option.HasOptions = ko.computed(function () {
            return ko.unwrap(self.Options).length > 1;
        });
        self.Option.CombineFinancePlans = ko.computed(function () {
            return self.Option.Is(1);
        });
        self.Option.ShowOptions = ko.computed(function () {
            if (ko.unwrap(self.Option.HasOptions)) {
                return true;
            }
            if (ko.unwrap(self.Option.CombineFinancePlans)) {
                return true;
            }
            return false;
        });
        self.Option.ActiveFinancePlanBalance = ko.computed(function () {
            var active = getActiveOption();
            if (active !== undefined && active !== null) {
                return ko.unwrap(active.ActiveFinancePlanBalance);
            }
            return 0;
        });
        self.Option.TextClientOfferSet = ko.computed(function () {
            var active = getActiveOption();
            if (active !== undefined && active !== null) {
                var isHtmlEntityLibAvailable = 'he' in window;
                return isHtmlEntityLibAvailable ? window.he.decode(ko.unwrap(active.TextClientOfferSet)) : '';
            }
            return '';
        });

        //
        function getActiveOption() {
            var combined = ko.unwrap(self.Option.CombineFinancePlans);
            var active = ko.utils.arrayFirst(ko.unwrap(self.Options), function (o) {
                return ko.unwrap(o.IsCombined) === combined;
            });
            return active;
        }

        //
        self.IsMenuVisible = ko.computed(function() {
            var option = ko.unwrap(self.Option);
            if (option === undefined || option === null) {
                return false;
            }
            
            if (ko.unwrap(self.IsEditable) === false) {
                return false;
            }

            return ko.unwrap(self.Option.ShowOptions) === true || ko.unwrap(self.IsEditable) === true;
        });
        
        //
        self.Messages = {
            Errors: ko.observableArray().extend({ extendedArray: {} })
        };

        //
        self.SubmitModel = function() {
            var model = $.extend({}, self.SubmitModelBase(), {
                CombineFinancePlans: ko.unwrap(self.Option.CombineFinancePlans),
                FinancePlanId: ko.unwrap(self.FinancePlanId),
                StatementId: ko.unwrap(self.StatementId),
                TermsCmsVersionId: ko.unwrap(self.TermsCmsVersionId),
                FinancePlanOptionStateCode: ko.unwrap(self.FinancePlanOptionStateCode)
            });
            
            if (ko.unwrap(self.PaymentDueDay.IsEnabled)) {
                $.extend(model, { PaymentDueDay: ko.unwrap(self.PaymentDueDay) });
            }

            return model;
        };
        self.ValidateModel = function() {
            var model = $.extend({}, self.ValidateModelBase(), {
                StatementId: ko.unwrap(self.StatementId),
                CombineFinancePlans: ko.unwrap(self.Option.CombineFinancePlans),
                PaymentDueDay: ko.unwrap(self.PaymentDueDay),
                FinancePlanOptionStateCode: ko.unwrap(self.FinancePlanOptionStateCode)
            });
            return model;
        };

        //
        var baseMapFunction = self.Map;
        self.Map = function(data) {
            
            self.SetOptions(data);
            var isCombined = ko.unwrap(self.Option.CombineFinancePlans);
            var activeOption = ko.utils.arrayFirst(ko.unwrap(self.Options), function (o) {
                return ko.unwrap(o.IsCombined) === isCombined;
            });
            baseMapFunction(activeOption);

            self.HasPendingResubmitPayments(data.HasPendingResubmitPayments);
            self.FinancePlanTotalMonthlyPaymentAmount(data.FinancePlanTotalMonthlyPaymentAmount);
            self.IsVisible(!data.HasPendingResubmitPayments);
            self.UserApplicableBalance(data.UserApplicableBalance);
            if (activeOption != null) {
                var o = ko.unwrap(activeOption);
                if (o != null && ko.unwrap(o.IsEditable) === false) {
                    self.OfferCalculationStrategy(ko.unwrap(o.OfferCalculationStrategy));
                }
            }
            self.SetOption();

            // pdd
            self.PaymentDueDay(data.SuggestedPaymentDueDay);
            self.PaymentDueDay.IsActive(false);
            self.PaymentDueDay.IsEnabled(data.CanSetPaymentDueDay);

            //FP State
            self.FinancePlanOptionStateCode(data.FinancePlanOptionStateCode);
            self.FinancePlanOptionStateName(data.FinancePlanOptionStateName);
            self.OtherFinancePlanStatesAreAvailable(data.OtherFinancePlanStatesAreAvailable);
            self.PaymentOptionId(data.PaymentOptionId);
        };
        self.SetOptions = function(data) {
            // combo/stack options
            var hasCombinedOption = data.FinancePlanCombinedConfiguration != null;
            var hasStackedOption = data.FinancePlanDefaultConfiguration != null;

            var options = [];
            if (hasStackedOption) {
                var stackedOption = data.FinancePlanDefaultConfiguration;
                stackedOption.IsCombined = false;
                stackedOption.Value = 0;
                options.push(stackedOption);
            }

            if (hasCombinedOption) {
                var combinedOption = data.FinancePlanCombinedConfiguration;
                combinedOption.IsCombined = true;
                combinedOption.Value = 1;
                options.push(combinedOption);
            }

            options.sort(function(left, right) {
                return parseInt(left.DisplayOrder) > parseInt(right.DisplayOrder) ? 1 : -1;
            });

            self.Options(options);
            
            if (ko.unwrap(self.Option.HasOptions)) {

                var hasCombinedPendingFinancePlan = hasCombinedOption && data.FinancePlanCombinedConfiguration.Terms.FinancePlanOfferId !== undefined && parseInt(data.FinancePlanCombinedConfiguration.Terms.FinancePlanOfferId) > 0;
                var hasDefaultPendingFinancePlan = hasStackedOption && data.FinancePlanDefaultConfiguration.Terms.FinancePlanOfferId !== undefined && parseInt(data.FinancePlanDefaultConfiguration.Terms.FinancePlanOfferId) > 0;

                if (hasDefaultPendingFinancePlan) {
                    self.Option(0);
                    return;
                } else if (hasCombinedPendingFinancePlan) {
                    self.Option(1);
                    return;
                }
            }

            self.Option(hasCombinedOption ? 1 : 0);
        };
        self.SetOption = function() {
            var isCombined = self.Option.CombineFinancePlans();
            var found = ko.utils.arrayFirst(ko.unwrap(self.Options), function(o) {
                return ko.unwrap(o.IsCombined) === isCombined;
            });
            var config = ko.mapping.toJS(found);
            if (config == null) {
                config = {};
            }
            
            // config data
            self.AmountToFinance(config.AmountToFinance);
            self.FinancePlanId(config.FinancePlanId);
            if (parseInt(config.FinancePlanOfferSetTypeId) !== parseInt(ko.unwrap(self.FinancePlanOfferSetTypeId))) {
                self.FinancePlanOfferSetTypeId(config.FinancePlanOfferSetTypeId);
            }
            self.FinancePlanOfferSetTypes(config.FinancePlanOfferSetTypes);
            self.InterestRates(config.InterestRates);
            self.IsEditable(config.IsEditable);
            self.MaximumMonthlyPayments(config.MaximumMonthlyPayments);
            self.MinimumPaymentAmount(config.MinimumPaymentAmount);
            self.StatementId(config.StatementId);
            self.TermsCmsVersionId(config.TermsCmsVersionId);
            self.UpdateFromTerms(config);

            if (!self.Terms().IsValid()) {
                var monthlyPaymentAmount = config.SuggestedMonthlyPaymentAmount;
                var numberMonthlyPayments = config.SuggestedNumberMonthlyPayments;

                if (monthlyPaymentAmount && numberMonthlyPayments) {
                    self.UserMonthlyPaymentAmount(formatDecimal(monthlyPaymentAmount));
                    self.UserNumberMonthlyPayments(numberMonthlyPayments);
                }
            }

            //Reset terms alert timeout
            self.TermsAlertTimeoutMet(false);
        };
        self.SetSubscriptions = function() {
            self.Option.subscribe(function(newValue) {
                if (newValue == null) {
                    return;
                }
                self.SetOption();
                self.ResetValues();
            });
            self.OfferCalculationStrategy.subscribe(self.ResetValues);
        };

        self.ChangeStateClicked = function() {
            log.debug('Change state clicked');
            self.FinancePlanOptionStateCode(null);
            messenger.notifySubscribers(ko.toJS(self), 'financePlanChangeState');
        };

        return self;

    };

    arrangePayment.FinancePlanModule = (function (paymentMethodsModule, opts) {
        var messenger = new ko.subscribable();

        var vm = new FinancePlanViewModel({}, paymentMethodsModule.paymentMethods.PrimaryPaymentMethod, messenger);
        vm.ChangePrimary = function () {
            paymentMethodsModule.changePrimary();
        };

        $.each($('[data-context="financeplan"]'), function () {
            ko.applyBindings(vm, $(this)[0]);
        });
        
        function onPaymentDueDayChange() {
            vm.PaymentDueDay.Toggle();
            if (!ko.unwrap(vm.HasPaymentParameters)) {
                // don't submit if the rest of the form isn't filled out
                return;
            }
            var validateModel = vm.ValidateModel();
            $.post(opts.validateUrl, validateModel, function(result) {
                if (result.IsError === true) {
                    // if changing the pdd results in invalid terms, clear the form
                    vm.ResetValues();
                    vm.UpdateBoundaries(result);
                } else {
                    // terms are valid, just update the terms
                    vm.UpdateFromTerms(result);
                }
                window.VisitPay.FinancePlanSetupCommon.SetTooltips($('#finance-plan'));
            });
        };
        
        function declineTerms() {

            var text = '<p class="text-left">You will continue to be responsible for the visit balance due. You may make a payment or create a new finance plan, but the terms available to you may be different than those provided here.</p>';
            text += '<p>Please confirm that you would like to decline the terms of this finance plan.</p>';

            window.VisitPay.Common.ModalGenericConfirmationMd('Decline Terms', text, 'Confirm', 'Close').done(function () {
                $.blockUI();
                $.post('/FinancePlan/FinancePlanCancel', { financePlanId: ko.unwrap(vm.FinancePlanId) }, function() {
                    window.location.reload();
                });
            });

        };

        function showSurvey(financePlanId, paymentId) {
            if (!window.VisitPayMobile) {
                window.VisitPay.Survey.ShowSurvey(
                    {
                        'SurveyName': 'FinancePlanCreation',
                        'FinancePlanId': financePlanId,
                        'PaymentId': paymentId
                    });
            }
        };

        function postFinancePlan() {
            var submitModel = { model: vm.SubmitModel() };
            $.extend(submitModel, opts.additionalParameters);

            $.post(opts.submitUrl, submitModel, function(result) {
                if (result.Result === true) {
                    opts.onPaymentComplete(result.Message).done(function() {
                        showSurvey(result.FinancePlanId, result.PaymentId);
                        $.unblockUI();
                    });
                } else {
                    vm.Messages.Errors.push(result.Message);
                    $.unblockUI();
                }
            });
        };
        
        function submitFinancePlan(e) {

            e.preventDefault();
            
            var primaryPaymentMethod = paymentMethodsModule.paymentMethods.PrimaryPaymentMethod();
            var financePlanOfferId = ko.unwrap(vm.Terms().FinancePlanOfferId);

            var achAuthorize = new window.VisitPay.AchAuthorization().promptOffer(primaryPaymentMethod, financePlanOfferId);
            achAuthorize.then(function() {
                $.blockUI();
                postFinancePlan();
            }).fail(function() {
                // don't allow continue - they didn't accept the ach authorization
            });

        };
        
        //
        return {
            init: function (paymentOption, financePlanOfferDetails) {
                var $scope = $('#finance-plan');

                vm.Messages.Errors.Clear();
                vm.Option(null);

                if (paymentOption == null) {
                    vm.IsVisible(false);
                    return;
                }
                
                //
                paymentMethodsModule.setSingleUse(false);

                //
                $scope.css('visibility', 'hidden');
                
                // setup
                vm.Map(paymentOption);
                vm.SetSubscriptions();

                // common
                window.VisitPay.FinancePlanSetupCommon.BindEvents($scope, vm, opts.validateUrl, opts.additionalParameters);
                
                // click events
                $scope.off('click.confirm').on('click.confirm', '#btnConfirmFinancePlan', submitFinancePlan);
                $scope.off('click.declineterms').on('click.declineterms', '#btnDeclineTerms', declineTerms);
                $scope.off('change.pdd').on('change.pdd', '#PaymentDueDay', onPaymentDueDayChange);
                
                // show
                $scope.css('visibility', 'visible');

                // Conditional setup: 
                // If we were provided with a FP offer, populate it and calculate terms
                if (financePlanOfferDetails !== undefined && financePlanOfferDetails !== null) {
                    vm.UserMonthlyPaymentAmount(financePlanOfferDetails.MonthlyPaymentAmountTotal);
                    $scope.find('#frmTerms').submit();
                }
            },
            messenger: messenger
        };

    });

})(window.VisitPay.ArrangePayment = window.VisitPay.ArrangePayment || {});
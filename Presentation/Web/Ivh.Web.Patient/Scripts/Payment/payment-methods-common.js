﻿(function(visitPay, $, ko) {

    visitPay.paymentMethodsViewModel = function() {

        var self = this;

        //
        self.AllAccounts = ko.observableArray([]);
        self.AllAccounts.Count = ko.computed(function() {
            return ko.unwrap(self.AllAccounts).length;
        });

        //
        self.StandardAccounts = ko.computed(function() {
            return ko.utils.arrayFilter(ko.unwrap(self.AllAccounts), function(paymentMethod) {
                return ko.unwrap(paymentMethod.IsAchType) || ko.unwrap(paymentMethod.IsCardType);
            });
        });
        self.StandardAccounts.Count = ko.computed(function() {
            return ko.unwrap(self.StandardAccounts).length;
        });

        //
        self.PaymentMethodAccountTypes = ko.observableArray([]);
        self.PaymentMethodProviderTypes = ko.observableArray([]);

        //
        self.AllowSingleUse = ko.observable(false);

        //
        self.SelectedPaymentMethod = ko.observable();
        self.SelectedPaymentMethod.Set = function(paymentMethodId, previousSelection, singleUsePaymentMethod) {

            var selected;

            if (paymentMethodId && parseInt(paymentMethodId) > 0) {
                selected = ko.utils.arrayFirst(ko.unwrap(self.AllAccounts), function(paymentMethod) {
                    return ko.unwrap(paymentMethod.PaymentMethodId) === paymentMethodId;
                });
            }
            // is it a new single use payment method?
            else if (singleUsePaymentMethod !== undefined && singleUsePaymentMethod !== null) {
                selected = ko.mapping.fromJS(singleUsePaymentMethod, null, new window.VisitPay.PaymentMethodListItemVm());
            }
            // is previously selected method still available?
            else if (previousSelection !== undefined && previousSelection !== null) {

                if (ko.unwrap(previousSelection.IsActive)) {

                    selected = ko.utils.arrayFirst(ko.unwrap(self.AllAccounts), function(paymentMethod) {
                        return ko.unwrap(paymentMethod.PaymentMethodId) === ko.unwrap(previousSelection.PaymentMethodId);
                    });

                } else {

                    selected = previousSelection;

                }
            }

            // nothing found, get primary
            if (selected === undefined || selected === null) {
                selected = ko.unwrap(self.PrimaryPaymentMethod);
            }

            // set value
            if (selected === undefined || selected === null) {
                self.SelectedPaymentMethod(null);
            } else {
                self.SelectedPaymentMethod(ko.unwrap(selected));
            }

        };

        //
        self.PrimaryPaymentMethod = ko.observable();
        self.PrimaryPaymentMethod.Set = function() {

            var primary = ko.utils.arrayFirst(ko.unwrap(self.AllAccounts), function(paymentMethod) {

                return ko.unwrap(paymentMethod.IsPrimary);

            });

            self.PrimaryPaymentMethod(primary);

        };
        self.PrimaryPaymentMethod.Enabled = ko.observable(false);

    };

    visitPay.PaymentMethods = function(model, $scopeElement, opts) {

        opts = $.extend({}, {
            urlPaymentMethod: '/Payment/PaymentMethod',
            urlPaymentMethodBank: '/Payment/PaymentMethodBank',
            urlPaymentMethodCard: '/Payment/PaymentMethodCard',
            urlPaymentMethodAccountProvider: '/Payment/PaymentMethodAccountProvider',
            urlPaymentMethodsList: '/Payment/GetPaymentMethodsList',
            urlRemovePaymentMethod: '/Payment/RemovePaymentMethod',
            urlSavePaymentMethodBank: '/Payment/SavePaymentMethodBank',
            urlSavePaymentMethodCard: '/Payment/SavePaymentMethodCard',
            urlSetPrimary: '/Payment/SetPrimaryPaymentMethod',
            urlSetAccountProviderType: '/Payment/SetPaymentMethodAccountProviderType'
        }, opts || {});
        
        // default to true, registration will override
        opts.promptForAchAuthorization = opts.promptForAchAuthorization === undefined ? true : opts.promptForAchAuthorization;

        //
        var paymentMethodInstance = new visitPay.PaymentMethod(opts);
        
        var okText = window.VisitPay.Localization.Ok || 'OK';
        var noText = window.VisitPay.Localization.No || 'No';
        var yesText = window.VisitPay.Localization.Yes || 'Yes';
        var cms = {
            AccountRemoveCms: {},
            HsaInterestDisclaimerCms: {},
            PrimaryChangedCms: {},
        };

        //
        function loadPaymentMethods(refreshAlerts, singleUsePaymentMethod) {

            refreshAlerts = refreshAlerts === undefined || refreshAlerts === null ? true : refreshAlerts;

            var promise = $.Deferred();

            // get selected payment method
            var previousSelection = ko.unwrap(model.PaymentMethods.SelectedPaymentMethod);

            // post
            $.post(opts.urlPaymentMethodsList, {}, function(data) {
                // map
                ko.mapping.fromJS(data.AllAccounts, {
                    create: function(options) {
                        return ko.mapping.fromJS(options.data, {}, new window.VisitPay.PaymentMethodListItemVm());
                    }
                }, model.PaymentMethods.AllAccounts);

                ko.mapping.fromJS(data.PaymentMethodAccountTypes, {
                    create: function(o) {
                        return ko.mapping.fromJS(o.data, {}, new window.VisitPay.PaymentMethodAccountTypeVm());
                    }
                }, model.PaymentMethods.PaymentMethodAccountTypes);

                ko.mapping.fromJS(data.PaymentMethodProviderTypes, {
                    create: function(o) {
                        return ko.mapping.fromJS(o.data, {}, new window.VisitPay.PaymentMethodProviderTypeVm());
                    }
                }, model.PaymentMethods.PaymentMethodProviderTypes);

                // cms
                cms.AccountRemoveCms = data.AccountRemoveCms;
                cms.HsaInterestDisclaimerCms = data.HsaInterestDisclaimerCms;
                cms.PrimaryChangedCms = data.PrimaryChangedCms;

                // set primary
                model.PaymentMethods.PrimaryPaymentMethod.Set();

                // set selected
                model.PaymentMethods.SelectedPaymentMethod.Set(null, previousSelection, singleUsePaymentMethod);

                promise.resolve();

                if (refreshAlerts && window.VisitPay.State.IsUserLoggedIn) {
                    visitPay.Common.RefreshAlerts();
                }

            });

            return promise;
        }

        //
        var deferred = $.Deferred();
        loadPaymentMethods(false).done(function() {
            deferred.resolve();
        });

        //
        function showModalIfNecessary(b) {

            b = b === undefined || b === null ? true : b;

            var parentModal = $($scopeElement.parents('.modal')[0]);
            if (parentModal.length > 0) {
                if (b) {
                    parentModal.removeClass('disable-trigger');
                    parentModal.modal('show');
                } else {
                    parentModal.addClass('disable-trigger');
                    parentModal.modal('hide');
                }
            }

        };

        function showErrorMessage(message) {
            showModalIfNecessary(false);
            visitPay.Common.ModalGenericAlertMd(window.VisitPay.Localization.Error || 'Error', message, okText).always(showModalIfNecessary);
        };

        function showPrimaryNotification() {

            showModalIfNecessary(false);
            var text = '<p>' + cms.PrimaryChangedCms.ContentBody + '</p>';
            if (cms.HsaInterestDisclaimerCms !== undefined && cms.HsaInterestDisclaimerCms !== null) {
                var hsa = (cms.HsaInterestDisclaimerCms.ContentBody || '');
                if (hsa.length > 0) {
                    text += '<hr/><div class="text-small text-left"><p>' + hsa + '</p></div>';
                }
            }
            visitPay.Common.ModalGenericAlertMd(cms.PrimaryChangedCms.ContentTitle, text, okText).always(function() {
                //Check if we should show the modal again after primary change
                if ('ShowModalAfterChangePrimary' in model && !ko.unwrap(model.ShowModalAfterChangePrimary)) {
                    //Shouldn't show modal again
                    return;
                }
                //Should show modal again
                showModalIfNecessary();
            });

        };

        //
        function remove(e) {
            if (e) {
                e.preventDefault();
                e.stopPropagation();
            }

            var paymentMethod = ko.dataFor(this);
            if (!ko.unwrap(paymentMethod.IsRemoveable)) {
                return;
            }

            visitPay.Common.ModalGenericConfirmation(cms.AccountRemoveCms.ContentTitle, cms.AccountRemoveCms.ContentBody, yesText, noText).done(function() {

                $.blockUI();
                $.post(opts.urlRemovePaymentMethod, { paymentMethodId: ko.unwrap(paymentMethod.PaymentMethodId) }, function(result) {

                    loadPaymentMethods().done(function() {

                        $.unblockUI();
                        if (result.IsError) {
                            showErrorMessage(result.ErrorMessage);
                        }

                    });

                });

            });

        };

        function setPrimary(e) {
            var currentPrimaryPaymentMethod = ko.unwrap(model.PaymentMethods.PrimaryPaymentMethod());
            var currentPrimaryPaymentMethodId = currentPrimaryPaymentMethod ? ko.unwrap(currentPrimaryPaymentMethod.PaymentMethodId) : null;
            var paymentMethod = ko.dataFor(this);
            var paymentMethodId = ko.unwrap(paymentMethod.PaymentMethodId);

            if (currentPrimaryPaymentMethodId === paymentMethodId) {
                e.preventDefault();
                paymentMethod.IsPrimary(true);
                return;
            }

            showModalIfNecessary(false);
            
            var promise = opts.promptForAchAuthorization ? new window.VisitPay.AchAuthorization().prompt(paymentMethod) : $.Deferred().resolve();

            promise.then(function () {
                $.blockUI();
                $.post(opts.urlSetPrimary, { paymentMethodId: paymentMethodId }, function(result) {

                    loadPaymentMethods().done(function() {

                        $.unblockUI();

                        if (result.IsError) {
                            showErrorMessage(result.ErrorMessage);
                        } else if (result.PrimaryChanged) {
                            showPrimaryNotification();
                        }

                    });

                });

            }).fail(function() {
                paymentMethod.IsPrimary(false);
            });

        };

        function select(e) {
            model.PaymentMethods.SelectedPaymentMethod.Set(ko.unwrap(ko.dataFor(this).PaymentMethodId));
        };

        // callback after add or edit
        function paymentMethodChanged(result) {
            // single use payment method, saved as inactive
            var singleUsePaymentMethod = null;
            if (!ko.unwrap(result.PaymentMethod.IsActive)) {
                singleUsePaymentMethod = ko.mapping.fromJS(result.PaymentMethod, {}, new window.VisitPay.PaymentMethodListItemVm());
            }

            var paymentMethodCountBefore = model.PaymentMethods.AllAccounts.Count();
            loadPaymentMethods(true, singleUsePaymentMethod).done(function () {
                var paymentMethodCountAfter = model.PaymentMethods.AllAccounts.Count();

                if (result.PrimaryChanged && ko.unwrap(model.PaymentMethods.AllAccounts.Count) > 1) {
                    showPrimaryNotification();
                } else {
                    //Check if we were changing primary
                    if ('IsChangePrimary' in model && ko.unwrap(model.IsChangePrimary)) {
                        //Check if we should show the modal again after primary change
                        if ('ShowModalAfterChangePrimary' in model && !ko.unwrap(model.ShowModalAfterChangePrimary)) {
                            //We don't want to show the modal again, if the primary was actually changed
                            //Make sure primary was changed, and it wasn't the first primary method added
                            if (!result.PrimaryChanged && model.PaymentMethods.AllAccounts.Count() > 1) {
                                //It was change primary, but primary method wasn't changed. Show modal again.
                                showModalIfNecessary();
                                return;
                            }

                            //Either the primary method wasn't changed, or it was the first primary method added. Don't show modal again
                            return;
                        }
                    }

                    //If this is the first payment method being added, check if we should show the modal again
                    if (paymentMethodCountBefore === 0 && paymentMethodCountAfter > 0) {
                        //The first payment method has been added. Check if we should reshow the modal.
                        if ('ShowModalAfterInitialPaymentMethodAdded' in model && !ko.unwrap(model.ShowModalAfterInitialPaymentMethodAdded)) {
                            //Don't want to show the modal after the first payment was added
                            return;
                        }
                    }
                    else if (singleUsePaymentMethod != null) {
                        showModalIfNecessary(false);
                        return;
                    }

                    //If we reach this we should show the modal again
                    showModalIfNecessary();
                }

            });

        };

        function showSelectedPaymentMethod(shouldShow, paymentMethodId) {
            //Need to either show active payment method list item, or show none active
            $('li[name="paymentmethod-select"]').each(function () {
                if (shouldShow && ko.dataFor(this)) {
                    if (ko.unwrap(ko.dataFor(this).PaymentMethodId) === paymentMethodId) {
                        //This is the selected payment method
                        $(this).addClass('active');
                    } else {
                        //This is not the selected payment method
                        $(this).removeClass('active');
                    }
                } else {
                    //Shouldn't show selection on payment method
                    $(this).removeClass('active');
                }
            });
        }

        // event bindings
        $scopeElement.off('click');
        $scopeElement.off('change');
        
        // account events
        $scopeElement.on('click', '[id^="btnBankAccountAdd"]', function(e) {
            e.preventDefault();
            showModalIfNecessary(false);
            paymentMethodInstance.LoadBankModal(0, ko.unwrap(model.PaymentMethods.AllowSingleUse()), true).then(paymentMethodChanged).fail(showModalIfNecessary);
        });
        $scopeElement.on('click', '[id^="btnCardAccountAdd"]', function(e) {
            e.preventDefault();
            showModalIfNecessary(false);
            paymentMethodInstance.LoadCardModal(0, ko.unwrap(model.PaymentMethods.AllowSingleUse()), false).then(paymentMethodChanged).fail(showModalIfNecessary);
        });
        $scopeElement.on('click', '[name="paymentmethod-edit"]', function(e) {

            e.preventDefault();
            e.stopPropagation();

            var data = ko.dataFor(this);

            if (ko.unwrap(data.IsCardType)) {
                showModalIfNecessary(false);
                paymentMethodInstance.LoadCardModal(ko.unwrap(data.PaymentMethodId), false).then(paymentMethodChanged).fail(showModalIfNecessary);

            } else {
                showModalIfNecessary(false);
                paymentMethodInstance.LoadBankModal(ko.unwrap(data.PaymentMethodId), false, false).then(paymentMethodChanged).fail(showModalIfNecessary);

            }

        });
        $scopeElement.on('click', '[name="paymentmethod-remove"]', remove);
        $scopeElement.on('click', '[name="paymentmethod-select"]', select);
        $scopeElement.on('change', '[name="paymentmethod-setprimary"]', setPrimary);

        // account/provider action.  
        $scopeElement.on('click', 'a.hsaType', function(e) {
            var data = ko.dataFor(this);
            e.preventDefault();
            showModalIfNecessary(false);
            paymentMethodInstance.LoadAccountProviderModal(ko.unwrap(data.PaymentMethodId), false).then(paymentMethodChanged).fail(showModalIfNecessary);
        });

        // subscriptions
        if ('IsChangePrimary' in model) {
            //Have 'IsChangePrimary' property in the model, subscribe to its change
            model.IsChangePrimary.subscribe(function (isChangePrimary) {
                if (isChangePrimary) {
                    //Change primary. Don't show selected method as active, and turn off listener for change
                    showSelectedPaymentMethod(false);
                    $scopeElement.off('click', '[name="paymentmethod-select"]');
                } else {
                    //Not change primary. Show selected method as active and listen for changes to selection.
                    showSelectedPaymentMethod(true);
                    $scopeElement.on('click', '[name="paymentmethod-select"]', select);
                }
            });
        }

        model.PaymentMethods.SelectedPaymentMethod.subscribe(function(previousValue) {

            if (previousValue === undefined || previousValue === null) {
                return;
            }

            // when switching away from a "single use" payment method, deactivate it.
            // it's already inactive, but this will also remove the billing id.
            // doing this b/c there's no way to switch back to it.
            if (!ko.unwrap(previousValue.IsActive)) {
                $.post(opts.urlRemovePaymentMethod, { paymentMethodId: ko.unwrap(previousValue.PaymentMethodId) });
            }

        }, self, 'beforeChange');

        model.PaymentMethods.SelectedPaymentMethod.subscribe(function(newValue) {
            var paymentMethod = ko.unwrap(newValue);
            var paymentMethodId = paymentMethod ? ko.unwrap(paymentMethod.PaymentMethodId) : -1;
            showSelectedPaymentMethod(true, paymentMethodId);
        });

        // deselect single use after switching to an option that does not allow it
        model.PaymentMethods.AllowSingleUse.subscribe(function(newValue) {
            if (!newValue && model.PaymentMethods.SelectedPaymentMethod && ko.unwrap(model.PaymentMethods.SelectedPaymentMethod) && !model.PaymentMethods.SelectedPaymentMethod().IsActive()) {
                model.PaymentMethods.SelectedPaymentMethod.Set(null, null, null);
            }
        });

        return deferred;

    };

})(window.VisitPay = window.VisitPay || {}, jQuery, window.ko = window.ko || {});

(function(visitPay, $, ko) {

    visitPay.PaymentMethod = function(opts) {

        //
        var okText = window.VisitPay.Localization.Ok || 'OK';

        //
        var model;
        var modalPromise;
        var $securepanframe;

        function showError($form, message) {

            var parentModal = $($form.parents('.modal-paymentmethod')[0]);
            parentModal.addClass('disable-trigger');
            parentModal.modal('hide');

            visitPay.Common.ModalGenericAlertMd('An Error Has Occurred', message, okText).always(function() {
                parentModal.removeClass('disable-trigger');
                parentModal.modal('show');
            });

        };

        function loadData(paymentMethodId, allowSingleUse, isNewAch) {

            var promise = $.Deferred();

            model = new window.VisitPay.PaymentMethodVm(allowSingleUse);

            $.post(opts.urlPaymentMethod, { paymentMethodId: paymentMethodId, isNewAch: isNewAch }, function(data) {

                var mapping = isNewAch ? {} : {
                    'PaymentMethodAccountTypes': {
                        create: function(o) {
                            return ko.mapping.fromJS(o.data, {}, new window.VisitPay.PaymentMethodAccountTypeVm());
                        }
                    }
                };

                ko.mapping.fromJS(data, mapping, model);
                promise.resolve();
            });

            return promise;

        };

        function loadModal(url) {

            var promise = $.Deferred();

            visitPay.Common.ModalNoContainerAsync('modalPaymentMethod', url, 'GET', {}, false).done(function($modal) {
                promise.resolve($modal);
            });

            return promise;
        };

        function loadAll(url, paymentMethodId, allowSingleUse, isNewAch) {

            $.blockUI();

            var promise = $.Deferred();

            loadData(paymentMethodId, allowSingleUse, isNewAch).done(function() {
                loadModal(url).done(function($modal) {
                    promise.resolve($modal);
                    ko.applyBindings(model, $modal[0]);
                });
            });

            return promise;

        };

        function save($form) {

            var promise = $.Deferred();
            var securePanPromise = $.Deferred();

            var isCardType = ko.unwrap(model.PaymentMethodTypeId.IsCardType);
            var postUrl = isCardType ? opts.urlSavePaymentMethodCard : opts.urlSavePaymentMethodBank;
            var postModel = ko.mapping.toJS(model, { 'ignore': ['AchTypes', 'Months', 'Years', 'PaymentMethodAccountTypes', 'PaymentMethodProviderTypes', 'AccountTypes', 'ProviderTypes'] });

            if (!postModel.IsActive) {
                postModel.IsPrimary = false;
            }

            if (isCardType) {

                postModel.AccountTypeId = $('#AccountTypeId').val();
                if ($form.find('#ProviderTypeId').prop('disabled')) {
                    postModel.ProviderTypeId = 0;
                } else {
                    postModel.ProviderTypeId = $('#ProviderTypeId').val();
                }

                if (ko.unwrap(model.PaymentMethodId.IsNew)) {
                    // on securepan saved
                    $securepanframe.off('securepan.onsaved').on('securepan.onsaved', function(e, data) {

                        postModel.BillingId = data.BillingId;
                        postModel.GatewayToken = data.TransactionId;
                        postModel.LastFour = data.LastFour;
                        postModel.AvsCode = data.AvsCode;

                        securePanPromise.resolve();
                        $securepanframe.off('securepan.onsaved');

                    });

                    // trigger securepan save (this will trigger .onsaved or an error in the callback)
                    $securepanframe.trigger('securepan.save', {
                        cvv: postModel.CardCode,
                        expM: postModel.ExpirationMonth,
                        expY: postModel.ExpirationYear,
                        name: postModel.NameOnCard,
                        address1: postModel.AddressLine1,
                        address2: postModel.AddressLine2,
                        city: postModel.City,
                        state: postModel.State,
                        zip: postModel.Zip
                    });

                } else {

                    securePanPromise.resolve();

                }

            } else {

                if (ko.unwrap(model.PaymentMethodId.IsNew)) {
                    postModel.RoutingNumber = $form.find('#RoutingNumber').val().replace(/\D/g, '');
                    postModel.AccountNumber = $form.find('#AccountNumber').val().replace(/\D/g, '');
                    postModel.AccountNumberConfirm = $form.find('#AccountNumberConfirm').val().replace(/\D/g, '');
                }
                securePanPromise.resolve();

            }

            securePanPromise.done(function() {

                $.post(postUrl, { model: postModel, __RequestVerificationToken: $form.find('[name="__RequestVerificationToken"]').val() }, function(result) {

                    $.unblockUI();

                    if (result.IsSuccess) {

                        promise.resolve(result);

                    } else {

                        promise.reject();
                        showError($form, result.ErrorMessage);

                    }

                });

            });

            return promise;

        };

        function validate($form) {

            var promise = $.Deferred();

            $form.parseValidation();
            var formIsValid = $form.valid();

            if ($securepanframe && $securepanframe.length > 0 && ko.unwrap(model.PaymentMethodId.IsNew)) {

                // on securepan clientside validation complete
                $securepanframe.off('securepan.onvalidate').on('securepan.onvalidate', function(e, result) {

                    if (formIsValid && result.isValid) {
                        // form + securepan valid
                        promise.resolve();
                    } else {
                        // one or both invalid
                        promise.reject();
                    }

                    $securepanframe.off('securepan.onvalidate');
                });

                // trigger securepan to run clientside validation  (this will trigger .onvalidate or an error in the callback)
                $securepanframe.trigger('securepan.validate');

            } else {

                if (formIsValid) {
                    promise.resolve();
                } else {
                    promise.reject();
                }

            }

            return promise;

        };

        function submit(e) {

            var $form = $(this);

            e.preventDefault();

            validate($form).done(function() {

                $.blockUI();
                save($form).done(function(result) {

                    $($form.parents('.modal-paymentmethod')[0]).modal('hide');
                    modalPromise.resolve(result);

                });

            });

        };

        function showAccountProviderModel($modal) {
            var $form = $modal.find('form');
            $form.parseValidation();
            var $selectedAccountTypeId = $form.find('#SelectedAccountTypeId');
            var $selectedProviderTypeId = $form.find('#SelectedProviderTypeId');

            // save
            $form.on('submit',
                function(e) {
                    e.preventDefault();
                    if (!$form.valid()) {
                        return;
                    }
                    var data = ko.dataFor($form[0]);
                    var paymentMethodId = ko.unwrap(data.PaymentMethodId);
                    var accountTypeId = $selectedAccountTypeId.val();
                    var providerTypeId = $selectedProviderTypeId.val();

                    if ($selectedProviderTypeId.prop('disabled'))
                        providerTypeId = 0;

                    $.blockUI();
                    $.post(opts.urlSetAccountProviderType,
                        {
                            paymentMethodId: paymentMethodId,
                            paymentMethodAccountTypeId: accountTypeId,
                            paymentMethodProviderTypeId: providerTypeId
                        },
                        function(result) {
                            $($form.parents('.modal-paymentmethod')[0]).modal('hide');
                            $.unblockUI();
                            modalPromise.resolve(result);
                        });
                });

            $selectedAccountTypeId.on('change', function() {
                $form.parseValidation();
                window.VisitPay.PaymentAccountProviderHelpers.accountTypeChanged($selectedAccountTypeId, $selectedProviderTypeId);
            });
            window.VisitPay.PaymentAccountProviderHelpers.accountTypeChanged($selectedAccountTypeId, $selectedProviderTypeId);

            // on close
            $modal.find('[data-dismiss="modal"]').on('click.pm', function() {
                modalPromise.reject();
            });

            // show modal
            $modal.modal('show');
            $.unblockUI();

        };

        function showModal($modal) {

            var $form = $modal.find('form');
            $form.parseValidation();

            // bank masks
            var accountMask = '9999[9999999999999]';
            $form.on('focus', '#AccountNumber', function() {
                $(this).inputmask('remove').inputmask(accountMask, { 'clearIncomplete': true });
            });
            $form.on('focus', '#AccountNumberConfirm', function() {
                $(this).inputmask('remove').inputmask(accountMask, { 'clearIncomplete': true });
            });
            $form.on('blur', '#AccountNumberConfirm', function() {
                // remove mask values (underscore) and revalidate so compare works
                $(this).inputmask(accountMask, { 'clearIncomplete': true });
                $form.validate().element($(this));
            });
            $form.on('focus', '#RoutingNumber', function() {
                $(this).inputmask('remove').inputmask('999999999', { 'clearIncomplete': true });
            });

            //
            $form.find('#CardCode').numbersOnly();
            $form.find('#Zip').zipCode();

            // image tooltips
            $modal.on('shown.bs.modal', function() {

                $form.find('.aRoutingAccountHelp').popover({
                    html: true,
                    trigger: 'hover',
                    placement: 'right',
                    container: 'body',
                    content: function() {
                        return '<img src="/Content/Images/check-routing-number.jpg" alt="" style="width: 450px; height: 213px;"/>';
                    }
                }).on('show.bs.popover', function() {
                    $(this).data('bs.popover').tip().css("max-width", '100%');
                });

                $form.find('#aCardCodeHelp').popover({
                    html: true,
                    trigger: 'hover',
                    placement: 'right',
                    container: 'body',
                    content: function() {
                        return '<img src="/Content/Images/cvv.jpg" alt="" style="width: 251px; height: 266px;" />';
                    }
                }).on('show.bs.popover', function() {
                    $(this).data('bs.popover').tip().css("max-width", '100%');
                });

            });

            // set primary tooltips
            $form.find('a.checkbox-tip').each(function(e) {
                var $element = $(this);
                var title = $element.attr('title') || $element.attr('title-old');
                $element.attr('title-old', title).removeAttr('title');
                $element.tooltip({
                    animation: false,
                    container: 'body',
                    html: true,
                    title: '<span style="display: block; width: 320px;">' + title + '</span>'
                });
            });

            // save
            $form.on('submit', submit);

            // on close
            $modal.find('[data-dismiss="modal"]').on('click.pm', function() {
                modalPromise.reject();
            });

            // show modal
            $modal.modal('show');
            $.unblockUI();

        };

        function loadCardModal(paymentMethodId, allowSingleUse) {

            $securepanframe = null;

            loadAll(opts.urlPaymentMethodCard, paymentMethodId, allowSingleUse, false).done(function($modal) {

                var $form = $modal.find('form');

                // hsa account/provider
                var $selectedAccountTypeId = $form.find('#AccountTypeId');
                var $selectedProviderTypeId = $form.find('#ProviderTypeId');
                $selectedAccountTypeId.off('change').on('change', function() {
                    $form.parseValidation();
                    window.VisitPay.PaymentAccountProviderHelpers.accountTypeChanged($selectedAccountTypeId, $selectedProviderTypeId);
                });
                window.VisitPay.PaymentAccountProviderHelpers.accountTypeChanged($selectedAccountTypeId, $selectedProviderTypeId);

                // if new payment method, setup securepan
                if (ko.unwrap(model.PaymentMethodId.IsNew)) {

                    if (window.VisitPay.State.IsEmulation) {

                        showModal($modal);

                    } else {

                        // setup secure pan
                        visitPay.SecurePan.Initialize().done(function($iframe) {

                            $securepanframe = $iframe;

                            // on card type changed
                            $securepanframe.off('securepan.oncardtypechanged').on('securepan.oncardtypechanged', function(e, paymentMethodTypeId) {
                                model.PaymentMethodTypeId(paymentMethodTypeId);
                            });

                            // on error
                            $securepanframe.off('securepan.onerror').on('securepan.onerror', function(e, result) {
                                $.unblockUI();
                                var $li = $form.find('.securepan-validationmessage').parents('li:eq(0)');
                                var $summary = $li.parents('.validation-summary-errors:eq(0)');

                                $li.remove();
                                if ($summary.find('li').length === 0) {
                                    $summary.removeClass('validation-summary-errors').addClass('validation-summary-valid');
                                }

                                if (result.errorMessage && result.errorMessage.length > 0) {
                                    window.VisitPay.Common.PrependMessageToValidationSummary($form, '<span class="securepan-validationmessage">' + result.errorMessage + '</span>');

                                    // audit log
                                    try {
                                        $.post('/payment/InvalidCreditCard', {
                                            errorMessage: result.errorMessage,
                                            isTimeout: result.isTimeout,
                                            __RequestVerificationToken: $('body').find('input[name=__RequestVerificationToken]').eq(0).val()
                                        });
                                    } catch (ex) {
                                    }
                                }
                            });

                            showModal($modal);

                        }).fail(function(retry) {

                            // securepan did not load successfully
                            $modal.remove();

                            if (retry) {
                                // call same function we're in again
                                loadCardModal(paymentMethodId, allowSingleUse);
                            }

                        });

                    }

                } else {
                    showModal($modal);
                }

            });

        };

        return {
            LoadAccountProviderModal: function(paymentMethodId, allowSingleUse) {

                modalPromise = $.Deferred();
                $securepanframe = null;

                loadAll(opts.urlPaymentMethodAccountProvider + '?paymentMethodId=' + paymentMethodId, paymentMethodId, allowSingleUse, false).done(function($modal) {

                    showAccountProviderModel($modal);

                });

                return modalPromise;

            },
            LoadBankModal: function(paymentMethodId, allowSingleUse, isNewAch) {

                modalPromise = $.Deferred();
                $securepanframe = null;

                loadAll(opts.urlPaymentMethodBank, paymentMethodId, allowSingleUse, isNewAch).done(function($modal) {

                    showModal($modal);

                });

                return modalPromise;

            },
            LoadCardModal: function(paymentMethodId, allowSingleUse) {
                modalPromise = $.Deferred();
                loadCardModal(paymentMethodId, allowSingleUse);
                return modalPromise;
            }
        };
    };

})(window.VisitPay = window.VisitPay || {}, jQuery, window.ko = window.ko || {});
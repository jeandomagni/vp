﻿window.VisitPay.PaymentEditVm = (function() {

    var self = {};

    self.Option = ko.observable(0);
    self.Option.Set = function(option) {
        if (self.Option.Is(option)) {
            self.Option(0);
        } else {
            self.Option(parseInt(option));
        }
    };
    self.Option.SetOne = function() {
        self.Option.Set(1);
    };
    self.Option.SetTwo = function() {
        self.Option.Set(2);
    };
    self.Option.SetThree = function() {
        self.Option.Set(3);
    };
    self.Option.Is = function(option) {
        return ko.unwrap(self.Option) && parseInt(ko.unwrap(self.Option)) === parseInt(option);
    };

    // payment methods
    self.PaymentMethods = {};
    self.IsSetPrimaryEnabled = ko.observable(false);
    self.IsPaymentMethodsEnabled = ko.observable(false);

    //
    self.AllowRescheduleDateChanged = ko.observable(false);
    self.IsEligibleToCancel = ko.observable(false);
    self.IsEligibleToReschedule = ko.observable(false);
    self.ShowPastDueFinancePlanMessage = ko.observable(false);
    self.IsCancelEnabled = ko.observable(false);
    self.IsRescheduleEnabled = ko.observable(false);
    self.IsRecurringPayment = ko.observable();
    self.PaymentType = ko.observable();
    self.MaxPaymentDate = ko.observable();
    self.OverrideRescheduledPaymentDate = ko.observable();
    self.RescheduledPaymentDate = ko.observable();
    self.ScheduledPaymentDate = ko.observable();
    self.PaymentRescheduleReasonId = ko.observable();
    self.PaymentRescheduleDescription = ko.observable();
    self.PaymentRescheduleReasons = ko.observable();
    self.PaymentRescheduleReasons.IsRequireInput = ko.computed(function() {

        if (!ko.unwrap(self.PaymentRescheduleReasonId)) {
            self.PaymentRescheduleDescription('');
            return false;
        }

        var selectedItem = ko.utils.arrayFirst(ko.unwrap(self.PaymentRescheduleReasons), function(item) {
            return parseInt(ko.unwrap(item.PaymentActionReasonId)) === parseInt(ko.unwrap(self.PaymentRescheduleReasonId));
        });

        if (!selectedItem) {
            self.PaymentRescheduleDescription('');
            return false;
        }

        if (!ko.unwrap(selectedItem.RequireInput)) {
            self.PaymentRescheduleDescription('');
            return false;
        }

        return true;

    });
    self.IsUserPermittedCancel = ko.observable(false);
    self.IsUserPermittedReschedule = ko.observable(false);
    self.IsAnyOptionEnabled = ko.observable(false);

    return self;
});

window.VisitPay.PaymentEdit = (function() {

    var isMobile = window.VisitPayMobile != null;
    var paymentMethodSourceEnum = {
        none: 'none',
        mobile: 'mobile',
        desktop: 'desktop'
    };
    
    function submit(params) {

        var promise = $.Deferred();

        $.post('/Payment/PaymentEditSave', params, function() {

            $.unblockUI();
            promise.resolve();

        }, 'json');

        return promise;

    };

    function validate(params) {

        var promise = $.Deferred();

        $.post('/Payment/PaymentEditValidate', params, function(result) {

            if (result == null || result === true) {

                promise.resolve();

            } else {

                $.unblockUI();

                if (result.Prompt === true) {
                    window.VisitPay.Common.ModalGenericConfirmationMd('Payment Alert', result.Message, 'Continue', 'Make Changes', false, { resolveAfterHidden: true }).done(function() {
                        promise.resolve();
                    }).fail(function() {
                        promise.reject();
                    });
                } else {
                    window.VisitPay.Common.ModalGenericAlertMd('Error', result.Message, 'Ok');
                    promise.reject();
                }

            }

        }, 'json');

        return promise;

    };

    function save($paymentDateForm, model, paymentId) {

        var promise = $.Deferred();

        if ($paymentDateForm.length > 0) {

            // validate even if hidden from view
            $paymentDateForm.parseValidation();
            var validatorSettings = $.data($paymentDateForm[0], 'validator').settings;
            validatorSettings.ignore = '';

            // remove readonly for validation
            var $date = $paymentDateForm.find('#RescheduledPaymentDate');
            $date.prop('readonly', false);

            // only validate if something is filled out
            var isValid = true;
            if ((ko.unwrap(model.RescheduledPaymentDate) || '').toString().length > 0 || (ko.unwrap(model.PaymentRescheduleReasonId) || '').toString().length > 0 || (ko.unwrap(model.PaymentRescheduleDescription) || '').toString().length > 0) {
                isValid = $paymentDateForm.valid();
            }

            // re-add readonly
            $date.prop('readonly', true);

            // return if invalid
            if (!isValid) {
                model.Option(2);
                promise.reject();
                return promise;
            }
        }

        $.blockUI();

        var paymentMethodId = null;
        if (isMobile || ko.unwrap(model.IsRecurringPayment)) {
            // mobile calls savePaymentMethod instead, don't need to save here
            // recurring payments set primary and don't need to be saved
        } else if (ko.unwrap(model.IsPaymentMethodsEnabled)) {
            paymentMethodId = model.PaymentMethods.SelectedPaymentMethod().PaymentMethodId();
        }

        var postModel = {
            PaymentId: paymentId,
            PaymentMethodId: paymentMethodId,
            PaymentType: ko.unwrap(model.PaymentType),
            RescheduledPaymentDate: ko.unwrap(model.RescheduledPaymentDate) || ko.unwrap(model.ScheduledPaymentDate),
            PaymentRescheduleReasonId: ko.unwrap(model.PaymentRescheduleReasonId),
            PaymentRescheduleDescription: ko.unwrap(model.PaymentRescheduleDescription)
        };

        validate(postModel).done(function() {
            submit(postModel).done(function() {
                promise.resolve();
            }).fail(function() {
                promise.reject();
            });
        }).fail(function() {
            promise.reject();
        });

        return promise;

    };

    function savePaymentMethod(paymentMethodId, paymentId) {
        $.blockUI();
        var promise = $.Deferred();
        var postModel = {
            PaymentMethodId: paymentMethodId,
            PaymentId: paymentId
        };
        validate(postModel).done(function() {
            submit(postModel).done(function() {
                promise.resolve();
            }).fail(function() {
                promise.reject();
            });
        }).fail(function() {
            promise.reject();
        });

        return promise;
    };

    function loadData(model, isRecurringPayment, paymentId) {

        var promise = $.Deferred();

        $.post('/Payment/PaymentEdit', { isRecurring: isRecurringPayment, paymentId: paymentId }, function(data) {
            ko.mapping.fromJS(data, {}, model);
            promise.resolve();
        });

        return promise;

    };

    function loadModal() {

        var promise = $.Deferred();

        window.VisitPay.Common.ModalNoContainerAsync('modalPaymentEdit', '/Payment/PaymentEdit', 'GET', null, false).done(function($modal) {

            if (isMobile) {
                $modal.find('[data-hide="desktop"]').remove();
            } else {
                $modal.find('[data-hide="mobile"]').remove();
            }

            promise.resolve($modal);

        });

        return promise;
    }

    function loadPaymentMethods(model, $modal) {

        if (!ko.unwrap(model.IsPaymentMethodsEnabled)) {
            // not necessary to load
            return $.Deferred().resolve(paymentMethodSourceEnum.none);
        }

        var promise = $.Deferred();
        if (isMobile) {
            // mobile payment methods
            promise.resolve(paymentMethodSourceEnum.mobile);
        }
        else {
            // desktop payment methods
            var isRecurringPayment = ko.unwrap(model.IsRecurringPayment);
            var url = '/Payment/PaymentMethodsListPartial?allowSelection=' + !isRecurringPayment;
            $.get(url, function(html) {

                $modal.find('#s1').html(html);

                if (!isRecurringPayment) {
                    $modal.find('#s1').addClass('hide-primary');
                }
                
                model.PaymentMethods = new window.VisitPay.paymentMethodsViewModel();
                window.VisitPay.PaymentMethods(model, $modal.find('.section-paymentmethods'), {}).done(function() {
                    promise.resolve(paymentMethodSourceEnum.desktop);
                });

            });
        }

        return promise;
    };

    function initialize(paymentId) {
        
        $.blockUI();
        
        var model = new window.VisitPay.PaymentEditVm();
        
        // a recurring payment will not have a paymentId
        var isRecurringPayment = paymentId == null;
        model.IsSetPrimaryEnabled(isRecurringPayment);

        var hasChanged = false;
        var promise = $.Deferred();
        var promiseData = loadData(model, isRecurringPayment, paymentId);
        var promiseModal = loadModal(isMobile);

        $.when(promiseData, promiseModal).done(function (data, $modal) {

            var promisePaymentMethods = loadPaymentMethods(model, $modal);
            promisePaymentMethods.done(function(paymentMethodSource) {
                
                //
                ko.applyBindings(model, $modal[0]);

                //
                if (paymentMethodSource === paymentMethodSourceEnum.desktop) {
                    model.PaymentMethods.PrimaryPaymentMethod.subscribe(function() {
                        hasChanged = true;
                    });
                    model.PaymentMethods.SelectedPaymentMethod.Set(ko.unwrap(model.PaymentMethodId));
                }

                //
                var $paymentDateForm = $modal.find('form');
                if ($paymentDateForm.length > 0) {

                    $paymentDateForm.parseValidation();
                    $paymentDateForm.on('submit', function(e) {
                        e.preventDefault();
                    });

                    if (!isMobile && $('#RescheduledPaymentDate').length > 0) {
                        var endDate = moment(ko.unwrap(model.MaxPaymentDate), 'MM/DD/YYYY');
                        $modal.find('#RescheduledPaymentDate').makeDatepicker(endDate);
                    }
                }

                //
                $modal.on('click', '[data-dismiss="modal"]', function(e) {
                    e.preventDefault();
                    promise.resolve(hasChanged);
                    $modal.modal('hide');
                });

                $modal.on('click', '.modal-footer button[type=submit]', function(e) {

                    e.preventDefault();

                    save($paymentDateForm, model, paymentId).done(function() {
                        promise.resolve(true);
                        $modal.modal('hide');
                    }).always(function() {
                        $.unblockUI();
                    });

                });

                $modal.on('click', '#btnPaymentEditPaymentMethods', function (e) {

                    e.preventDefault();

                    if (paymentMethodSource === paymentMethodSourceEnum.desktop) {

                        model.Option.SetOne();

                    } else {

                        // initialize mobile payment methods window
                        function onPaymentMethodSaved(savedPaymentMethodId) {
                            if (savedPaymentMethodId && savedPaymentMethodId !== ko.unwrap(model.PaymentMethodId)) {
                                savePaymentMethod(savedPaymentMethodId, paymentId).done(function () {
                                    promise.resolve(true);
                                    $modal.modal('hide');
                                }).fail(function () {
                                    $.unblockUI();
                                });
                            } else {
                                promise.resolve(false);
                                $modal.modal('hide');
                            }
                        }

                        function onClose() {
                            $('body').addClass('modal-open');
                            $modal.siblings('.modal-backdrop').show();
                            $modal.show();
                        }
                        
                        // doing this instead of $modal.modal('hide') so that all the event listeners aren't destroyed
                        $modal.siblings('.modal-backdrop').hide();
                        $modal.hide();
                        $('body').removeClass('modal-open');
                        
                        var initPromise;
                        if (ko.unwrap(model.IsRecurringPayment)) {
                            initPromise = window.VisitPayMobile.PaymentEditPaymentMethod.initializeRecurring();
                        } else {
                            initPromise = window.VisitPayMobile.PaymentEditPaymentMethod.initializeScheduled();
                        }
                        initPromise.done(onPaymentMethodSaved).fail(onClose);

                    }
                });

                $modal.on('click', '#btnPaymentCancel', function(e) {

                    e.preventDefault();

                    $modal.addClass('disable-trigger').modal('hide');

                    window.VisitPay.PaymentCancel.Initialize(ko.unwrap(model.IsRecurringPayment), paymentId).then(function() {

                        $modal.remove();
                        promise.resolve(true);

                    }).fail(function() {

                        $modal.removeClass('disable-trigger').modal('show');

                    });

                });

                $modal.on('click', '#aPaymentEditContactSupport', function(e) {
                    e.preventDefault();
                    promise.resolve(hasChanged);
                    $modal.modal('hide');
                    $.blockUI();

                    if (isMobile) {
                        window.VisitPayMobile.Common.OpenCreateSupportRequest();
                    } else {
                        window.VisitPay.CreateSupportRequest.Initialize($('#modalCreateSupportRequestContainer'), '');
                    }
                });
                
                $.unblockUI();
                $modal.modal('show');

            });

        });

        return promise;
    }

    return {
        Initialize: function (paymentId) {
            return initialize(paymentId);
        },
        SavePaymentMethod: function(paymentMethodId, paymentId) {

            $.blockUI();

            var promise = $.Deferred();
            var postModel = {
                PaymentMethodId: paymentMethodId,
                PaymentId: paymentId
            };

            validate(postModel).done(function() {
                submit(postModel).done(function() {
                    promise.resolve();
                }).fail(function() {
                    promise.reject();
                });
            }).fail(function() {
                promise.reject();
            });

            return promise;
        }
    };

})();
﻿(function(arrangePayment) {
    
    var continueText = window.VisitPay.Localization.Continue || 'Continue';
    var errorText = window.VisitPay.Localization.Error || 'Error';
    var makeChangesText = window.VisitPay.Localization.MakeChanges || 'Make Changes';
    var okText = window.VisitPay.Localization.Ok || 'Ok';
    var paymentAlertText = window.VisitPay.Localization.PaymentAlert || 'Payment Alert';

    var paymentOptionEnum = {
        Resubmit: 1,
        FinancePlan: 4,
        SpecificAmount: 5,
        SpecificVisits: 6,
        SpecificFinancePlans: 7,
        AccountBalance: 8,
        CurrentNonFinancedBalance: 9,
        HouseholdCurrentNonFinancedBalance: 10
    };

    var ArrangePaymentValidationResponseViewModel = function (data) {
        var self = this;

        self.PaymentIsValid = ko.observable();
        self.Prompt = ko.observable();
        self.Message = ko.observable();
        self.FinancePlanIsAvailable = ko.observable();
        self.MonthlyPaymentAmountTotal = ko.observable();
        self.NumberOfMonthlyPayments = ko.observable();
        self.FinancePlanOfferMessage = ko.observable();

        ko.mapping.fromJS(data, {}, self);
        
        return self;
    };

    var dateFormat = 'MM/DD/YYYY';
    var PaymentOptionViewModel = function(data) {

        var self = this;
        
        // properties from model
        self.DiscountAmount = ko.observable();
        self.IsDiscountEligible = ko.observable();
        self.IsDiscountPromptPayOnly = ko.observable();
        self.DateBoundary = ko.observable();
        self.PaymentDate = ko.observable();
        self.PaymentDateDefault = '';
        self.IsDateReadonly = ko.observable();
        self.IsAmountReadonly = ko.observable();
        self.PaymentOptionId = ko.observable();
        self.PaymentOptionId.IsAccountBalance = ko.computed(function () {
            return ko.unwrap(self.PaymentOptionId) === paymentOptionEnum.AccountBalance;
        });
        self.PaymentOptionId.IsCurrentNonFinancedBalance = ko.computed(function () {
            return ko.unwrap(self.PaymentOptionId) === paymentOptionEnum.CurrentNonFinancedBalance;
        });
        self.PaymentOptionId.IsHouseholdCurrentNonFinancedBalance = ko.computed(function () {
            return ko.unwrap(self.PaymentOptionId) === paymentOptionEnum.HouseholdCurrentNonFinancedBalance;
        });
        self.PaymentOptionId.IsResubmit = ko.computed(function () {
            return ko.unwrap(self.PaymentOptionId) === paymentOptionEnum.Resubmit;
        });
        self.PaymentOptionId.IsSpecificFinancePlans = ko.computed(function () {
            return ko.unwrap(self.PaymentOptionId) === paymentOptionEnum.SpecificFinancePlans;
        });
        self.PaymentOptionId.IsSpecificVisits = ko.computed(function() {
            return ko.unwrap(self.PaymentOptionId) === paymentOptionEnum.SpecificVisits;
        });

        self.PaymentOptionId.IsPayInFull = ko.computed(function () {
            if (self.PaymentOptionId.IsAccountBalance()) {
                return true;
            }
            if (self.PaymentOptionId.IsHouseholdCurrentNonFinancedBalance()) {
                return true;
            }
            if (self.PaymentOptionId.IsCurrentNonFinancedBalance()) {
                return true;
            }
            return false;
        });

        self.UserApplicableBalance = ko.observable();
        self.MaximumPaymentAmount = ko.observable();
        self.PaymentAmount = ko.observable().extend({ money: { minValue: 0, maxValue: self.MaximumPaymentAmount } });

        // lists from model
        self.FinancePlans = ko.observableArray([]).extend({ selectablePaymentGrid: { idKey: 'FinancePlanId', amountKey: 'PaymentAmount', defaultKey: 'CurrentFinancePlanBalanceDecimal' } });
        self.FinancePlans.AllowInput = ko.computed(function () {
            return ko.unwrap(self.PaymentOptionId.IsSpecificFinancePlans);
        });
        self.HouseholdBalances = ko.observableArray([]).extend({ selectablePaymentGrid: { idKey: 'VpStatementId', amountKey: 'TotalBalanceDecimal', defaultKey: 'TotalBalanceDecimal', setPaymentAmount: true } });
        self.HouseholdBalances.WithBalances = ko.computed(function() {
            return ko.utils.arrayFilter(self.HouseholdBalances(), function(balance) {
                return ko.unwrap(balance.TotalBalanceDecimal) > 0;
            });
        });
        self.HouseholdBalances.WithoutBalances = ko.computed(function() {
            return ko.utils.arrayFilter(self.HouseholdBalances(), function(balance) {
                return ko.unwrap(balance.TotalBalanceDecimal) <= 0;
            });
        });
        self.Payments = ko.observableArray([]).extend({ selectablePaymentGrid: { idKey: 'FinancePlanId', amountKey: 'PaymentAmount', defaultKey: 'ScheduledPaymentAmountDecimal' } });
        self.Visits = ko.observableArray([]).extend({ selectablePaymentGrid: { idKey: 'VisitId', amountKey: 'PaymentAmount', defaultKey: 'DisplayBalanceDecimal' } });
        self.Visits.AllowInput = ko.computed(function() {
            return ko.unwrap(self.PaymentOptionId.IsSpecificVisits);
        });

        // bucket zero
        self.AmountDueTotal = ko.pureComputed(function() {
            var sum = 0;
            ko.utils.arrayForEach(self.FinancePlans(), function(fp) {
                sum += parseFloat(ko.unwrap(fp.AmountDueDecimal));
            });
            return sum;
        });
        self.ShowPayBucketZero = ko.pureComputed(function() {
            return ko.unwrap(self.PaymentOptionId) === paymentOptionEnum.SpecificFinancePlans
                && ko.unwrap(self.HasBucketZero) === true;
        });
        self.HasBucketZero = ko.observable(false);
        self.PayBucketZero = ko.observable(false);
        self.PayBucketZero.subscribe(function(newValue) {
            if (newValue) {
                //reset to default date when it's turned on
                self.PaymentDate(self.PaymentDateDefault);
            }
        });
        self.CanSchedulePayment = function() {
            if (ko.unwrap(self.PaymentOptionId) === paymentOptionEnum.SpecificFinancePlans) {
                return ko.unwrap(self.PayBucketZero) !== undefined
                    && ko.unwrap(self.PayBucketZero) !== null
                    && ko.unwrap(self.PayBucketZero) !== true;
            }
            return true;
        }
        self.IsDateReadonlyOverride = ko.pureComputed(function() {
            var isDateReadonly = ko.unwrap(self.IsDateReadonly);
            if (isDateReadonly) {
                return true;
            }
            if (ko.unwrap(self.PaymentOptionId) === paymentOptionEnum.SpecificFinancePlans) {
                return !self.CanSchedulePayment();
            }
            return isDateReadonly;
        });

        // computed properties
        self.PaymentDate.IsPromptPay = ko.computed(function () {
            var isPromptPay = moment().isSame(moment(ko.unwrap(self.PaymentDate), dateFormat), 'day');
            return isPromptPay;
        });
        self.DiscountAmount.Calculated = ko.computed(function() {
            
            if (ko.unwrap(self.IsDiscountPromptPayOnly) === true) {
                if (ko.unwrap(self.PaymentDate.IsPromptPay) === false) {
                    return 0;
                }
            }

            if (ko.unwrap(self.PaymentOptionId) === paymentOptionEnum.HouseholdCurrentNonFinancedBalance) {
                // household
                var discountAmount = 0;
                ko.utils.arrayForEach(self.HouseholdBalances.Selected(), function (balance) {
                    if (ko.unwrap(balance.IsDiscountEligible)) {
                        discountAmount += parseFloat(ko.unwrap(balance.DiscountAmount));
                    }
                });
                return discountAmount;
            }

            // not household
            return ko.unwrap(self.DiscountAmount);

        });
        self.PaymentAmount.Calculated = ko.computed(function() {

            if (!self.IsAmountReadonly()) {
                return self.PaymentAmount();
            }

            if (ko.unwrap(self.PaymentOptionId.IsHouseholdCurrentNonFinancedBalance)) {
                var paymentAmount = 0;
                ko.utils.arrayForEach(self.HouseholdBalances.Selected(), function(balance) {
                    paymentAmount += parseFloat(balance.TotalBalanceDecimal());
                });
                paymentAmount -= self.DiscountAmount.Calculated();
                return paymentAmount;
            }

            if (ko.unwrap(self.IsDiscountEligible)) {
                var amount = parseFloat(self.UserApplicableBalance.Calculated()) - parseFloat(self.DiscountAmount.Calculated());
                return amount;
            }

            if (ko.unwrap(self.PaymentOptionId.IsSpecificFinancePlans)) {
                return self.FinancePlans.Selected.Sum();
            }

            if (ko.unwrap(self.PaymentOptionId.IsResubmit)) {
                return self.Payments.Selected.Sum();
            }

            if (ko.unwrap(self.PaymentOptionId.IsSpecificVisits)) {
                return self.Visits.Selected.Sum();
            }

            return self.UserApplicableBalance.Calculated();
        });
        self.PaymentAmount.Calculated.IsValid = ko.computed(function() {
            return !self.IsAmountReadonly() || self.PaymentAmount.Calculated() > 0;
        });
        self.UserApplicableBalance.Calculated = ko.computed(function() {
            if (ko.unwrap(self.PaymentOptionId) === paymentOptionEnum.HouseholdCurrentNonFinancedBalance) {
                // household
                return self.HouseholdBalances.Selected.Sum();
            }

            // not household
            return ko.unwrap(self.UserApplicableBalance);
        });

        // map
        var mapping = {
            'DateBoundary': {
                update: function(options) {
                    return moment.utc(options.data, dateFormat).format(dateFormat);
                }
            },
            'HouseholdBalances': {
                update: function(options) {
                    var model = ko.mapping.fromJS(options.data, {});
                    return model;
                }
            },
            'PaymentDate': {
                update: function(options) {
                    self.PaymentDateDefault = moment.utc(options.data, dateFormat).format(dateFormat);
                    return moment.utc(options.data, dateFormat).format(dateFormat);
                }
            },
            'FinancePlans': {
                create: function(options) {
                    var model = ko.mapping.fromJS(options.data);
                    model.PaymentAmount = ko.observable(0).extend({ minmaxMoney: { minValue: 0, maxValue: model.CurrentFinancePlanBalanceDecimal } });
                    return model;
                }
            },
            'Visits': {
                create: function(options) {
                    var model = ko.mapping.fromJS(options.data);
                    model.PaymentAmount = ko.observable(0).extend({ minmaxMoney: { minValue: 0, maxValue: model.DisplayBalanceDecimal } });
                    return model;
                }
            },
            'Payments': {
                create: function(options) {
                    var model = ko.mapping.fromJS(options.data);
                    var defaultPaymentAmount = ko.unwrap(model.ScheduledPaymentAmountDecimal).toFixed(2);
                    model.PaymentAmount = ko.observable(defaultPaymentAmount).extend({ minmaxMoney: { minValue: 0, maxValue: model.ScheduledPaymentAmountDecimal } });
                    return model;
                }
            }
        };
        ko.mapping.fromJS(data, mapping, self);

        //
        return self;

    };

    var PaymentViewModel = function(messenger, discountsVm) {

        var self = this;

        // Response from PaymentValidate
        self.ArrangePaymentValidationResponseViewModel = ko.observable(new ArrangePaymentValidationResponseViewModel());

        // Finance Plan Offer
        self.ViewFinancePlanOffer = function () {
            //Dismiss payment submit modal
            var $modal = $('#modalArrangePaymentSubmit');
            $modal.modal('hide');

            //Let the main controller know that we want to show the FP offer
            messenger.notifySubscribers(ko.unwrap(self.ArrangePaymentValidationResponseViewModel), 'showFinancePlanOffer');
        }

        //
        self.ActivePaymentOption = ko.observable();
        self.ActivePaymentOption.Any = ko.computed(function() {
            return ko.unwrap(self.ActivePaymentOption) && ko.unwrap(self.ActivePaymentOption().PaymentOptionId) > 0;
        });
        self.ActivePaymentOption.PostModel = function() {
            var o = self.ActivePaymentOption();
            
            if (ko.unwrap(o.PaymentOptionId.IsAccountBalance)) {
                o.FinancePlans.EnsureAll();
                o.Visits.EnsureAll();
            }

            return {
                PaymentOptionId: o.PaymentOptionId(),
                PaymentAmount: o.PaymentAmount.Calculated(),
                PaymentDate: o.PaymentDate(),
                PaymentMethodId: self.SelectedPaymentMethod().PaymentMethodId(),
                Payments: o.Payments.Selected.AsDictionary(),
                HouseholdBalances: o.HouseholdBalances.Selected.AsDictionary(),
                FinancePlans: o.FinancePlans.Selected.AsDictionary(),
                Visits: o.Visits.Selected.AsDictionary(),
                PayBucketZero: o.PayBucketZero()
            };
        };

        //
        self.SelectedPaymentMethod = ko.observable();

        //
        self.Messages = {
            Errors: ko.observableArray().extend({ extendedArray: {} })
        };

        //
        self.ChangePaymentMethod = function() {
            messenger.notifySubscribers(null, 'changePaymentMethod');
        };
        self.SetSpecificFinancePlans = function() {
            messenger.notifySubscribers(paymentOptionEnum.SpecificFinancePlans, 'setPaymentOption');
        };
        self.ShowDiscounts = function() {
            discountsVm(new window.VisitPay.DiscountsVm(ko.unwrap(self.ActivePaymentOption().DiscountGuarantorOffer), ko.unwrap(self.ActivePaymentOption().DiscountManagedOffers)));
            $('#modalDiscounts').modal('show');
        };
        self.ShowDiscountsFor = function(obj) {
            if (ko.unwrap(obj.IsManaged)) {
                discountsVm(new window.VisitPay.DiscountsVm(null, [obj.DiscountOffer]));
            } else {
                discountsVm(new window.VisitPay.DiscountsVm(obj.DiscountOffer));
            }
            $('#modalDiscounts').modal('show');
        };

        //
        self.IsValid = ko.computed(function() {
            if (!ko.unwrap(self.ActivePaymentOption) || !ko.unwrap(self.SelectedPaymentMethod)) {
                return false;
            }
            return ko.unwrap(self.SelectedPaymentMethod().PaymentMethodId) > 0 && self.ActivePaymentOption().PaymentAmount.Calculated.IsValid();
        });

        return self;

    };

    arrangePayment.PaymentModule = (function(paymentMethodsModule, opts) {

        var messenger = new ko.subscribable();

        //
        var discountsVm = ko.observable(new window.VisitPay.DiscountsVm());
        var vm = new PaymentViewModel(messenger, discountsVm);
        vm.SelectedPaymentMethod = paymentMethodsModule.paymentMethods.SelectedPaymentMethod;

        $.each($('[data-context="payment"]'), function() {
            ko.applyBindings(vm, $(this)[0]);
        });

        $.each($('[data-context="discounts"]'), function() {
            ko.applyBindings(discountsVm, $(this)[0]);
        });

        function submitPayment() {

            var $modal = $('#modalArrangePaymentSubmit');
            $modal.modal('show');

            $modal.find('button[type=submit]').off('click').on('click', function(e) {

                e.preventDefault();
                $modal.modal('hide');
                $.blockUI();

                var postModel = { model: vm.ActivePaymentOption.PostModel() };
                $.extend(postModel, opts.additionalParameters);

                var isPayInFull = ko.unwrap(vm.ActivePaymentOption).PaymentOptionId.IsPayInFull();

                $.post(opts.submitUrl, postModel, function(result) {
                    if (result.Result === true) {
                        opts.onPaymentComplete(result.Message).done(function () {
                            var paymentSurvey = (isPayInFull) ? 'PayInFull' : 'ManualPayment';
                            window.VisitPay.Survey.ShowSurvey(
                                {
                                    "SurveyName": paymentSurvey,
                                    "PaymentId": result.PaymentId
                                });
                            $.unblockUI();
                        });
                    } else {
                        vm.Messages.Errors.push(result.Message);
                        $.unblockUI();
                    }

                });

            });

        }

        function validatePayment(e) {

            e.preventDefault();
            vm.Messages.Errors.Clear();

            if (!$(this).valid()) {
                return;
            }

            paymentMethodsModule.hide(false);
            $.blockUI();

            var postModel = { model: vm.ActivePaymentOption.PostModel() };
            $.extend(postModel, opts.additionalParameters);

            $.post(opts.validateUrl, postModel, function(result) {

                $.unblockUI();
                
                if (result === undefined || result === null) {
                    //We expected a response but didn't get one, show generic error message
                    window.VisitPay.Common.ModalGenericAlertMd(errorText, result.Message, okText);
                } else {
                    //We got a response, check if it's valid
                    if (result.PaymentIsValid === true) {
                        //Update the model with the reponse
                        vm.ArrangePaymentValidationResponseViewModel(result);

                        //Show submit payment modal
                        submitPayment();
                    } else if (result.Prompt === true) {
                        //Payment isn't valid and we have a message to show
                        window.VisitPay.Common.ModalGenericConfirmationMd(paymentAlertText, result.Message, continueText, makeChangesText, false, { resolveAfterHidden: true }).done(submitPayment);
                    } else {
                        //Payment isn't valid and we have no message, show generic error
                        window.VisitPay.Common.ModalGenericAlertMd(errorText, result.Message, okText);
                    }
                }
            });

        }

        return {
            init: function(paymentOption) {

                vm.Messages.Errors.Clear();

                if (!paymentOption) {
                    vm.ActivePaymentOption(new PaymentOptionViewModel());
                    return;
                }

                var paymentOptionVm = new PaymentOptionViewModel(paymentOption);
                paymentMethodsModule.setSingleUse(paymentOptionVm.PaymentDate);
                paymentOptionVm.PaymentDate.subscribe(paymentMethodsModule.setSingleUse);

                vm.ActivePaymentOption(paymentOptionVm);

                if (paymentOption.PaymentOptionId === paymentOptionEnum.Resubmit) { // resubmit
                    vm.ActivePaymentOption().Payments.SelectAll();
                    paymentMethodsModule.unselect();
                } else if (paymentOption.PaymentOptionId === paymentOptionEnum.HouseholdCurrentNonFinancedBalance) { //household
                    vm.ActivePaymentOption().HouseholdBalances.SelectAll();
                }

                if (paymentOption.PaymentOptionId !== paymentOptionEnum.Resubmit) {
                    if (!ko.unwrap(paymentMethodsModule.paymentMethods.SelectedPaymentMethod)) {
                        paymentMethodsModule.selectPrimary();
                    }
                }

                var $scope = $('#payment-submit form');
                $scope.find('#PaymentDate').makeDatepicker(moment.utc(paymentOption.DateBoundary, dateFormat));
                $scope.find('.numeric').makeNumericInput();
                $scope.parseValidation();
                $scope.off('submit').on('submit', validatePayment);

            },
            setup: function (callback) {
                if (callback != null && typeof callback === 'function') {
                    callback(vm);
                }
            },
            messenger: messenger
        }

    });

})(window.VisitPay.ArrangePayment = window.VisitPay.ArrangePayment || {});
﻿(function(visitPay, $) {

    $(document).ready(function() {

        var opts = {
            urlPaymentMethodBank: '/Payment/PaymentMethodBank',
            urlPaymentMethodCard: '/Payment/PaymentMethodCard',
            urlPaymentMethodsList: '/Payment/GetPaymentMethodsList',
            urlRemovePaymentMethod: '/Payment/RemovePaymentMethod',
            urlSavePaymentMethodBank: '/Payment/SavePaymentMethodBank',
            urlSavePaymentMethodCard: '/Payment/SavePaymentMethodCard',
            urlSetPrimary: '/Payment/SetPrimaryPaymentMethod'
        };

        var viewModel = {
            PaymentMethods: new visitPay.paymentMethodsViewModel(),
            IsSetPrimaryEnabled: ko.observable(true)
        };

        var scopeElement = $('#section-paymentmethods');

        ko.applyBindings(viewModel, scopeElement[0]);
        visitPay.PaymentMethods(viewModel, scopeElement, opts);

    });

})(window.VisitPay = window.VisitPay || {}, jQuery);
﻿(function(urls, $) {

    var voteValueConstant = 3;

    var submitVote = function(questionId, voteValue) {
        var data = { questionid: questionId, voteValue: voteValue };
        var voteSubmitUrl = urls.KnowledgeBaseVoteSubmit;
        $.post(voteSubmitUrl, data);
    };

    var submitFeedback = function(questionId, feedbackText) {
        var data = { questionid: questionId, feedbackText: feedbackText };
        var feedbackSubmitUrl = urls.KnowledgeBaseFeedbackSubmit;
        $.post(feedbackSubmitUrl, data);
    };

    var getFeedbackContainer = function($elem) {
        return $elem.closest('.feedback-container');
    };

    var acknowledgeVote = function($feedbackContainer) {

        $feedbackContainer.find('.feedback-controls,.additional-feedback').hide();
        $feedbackContainer.find('.feedback-thanks').show();
    };

    $(document).ready(function() {

        $('.thumbs-up').off('click').on('click', function() {
            var $feedbackContainer = getFeedbackContainer($(this));
            acknowledgeVote($feedbackContainer);
            var questionId = $feedbackContainer.data('question-id');
            submitVote(questionId, voteValueConstant);
        });

        $('.thumbs-down').off('click').on('click', function() {
            var $feedbackContainer = getFeedbackContainer($(this));
            var questionId = $feedbackContainer.data('question-id');
            submitVote(questionId, voteValueConstant * -1);
            $feedbackContainer.find('.additional-feedback').show();
        });

        $('.additional-feedback-submit').off('click').on('click', function() {
            var $feedbackContainer = getFeedbackContainer($(this));
            var $feedbackText = $feedbackContainer.find('.feedback-text');

            if ($feedbackText.val().length == 0) {
                acknowledgeVote($feedbackContainer);
                return;
            }
            acknowledgeVote($feedbackContainer);

            var questionId = $feedbackContainer.data('question-id');
            submitFeedback(questionId, $feedbackText.val());
        });

        $('.additional-feedback-ingnore').off('click').on('click', function() {
            var $feedbackContainer = getFeedbackContainer($(this));
            acknowledgeVote($feedbackContainer);
        });

    });

})(window.VisitPay.Urls = window.VisitPay.Urls || {}, jQuery);
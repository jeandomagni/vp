﻿(function (search, ko) {
    
    var noSearchResultsText = window.VisitPay.Localization.NoSearchResults || 'Sorry, we couldn\'t find anything like that. Try updating your search term.';
    var searchForText = window.VisitPay.Localization.SearchFor || 'Search for';
    var suggestedResultsText = window.VisitPay.Localization.SuggestedResults || 'Suggested Results';

    search.Main = (function (data) {

        var minQueryLength = 4;

        function SearchViewModel() {
            var self = this;

            self.QueryText = ko.observable('');
            self.FoundLiteralMatches = ko.observable(false);
            self.FoundTaggedMatches = ko.observable(false);
            self.CanSearch = ko.computed(function() {
                return ko.unwrap(self.QueryText) != undefined &&
                    ko.unwrap(self.QueryText).length >= minQueryLength &&
                    (ko.unwrap(self.FoundLiteralMatches) || ko.unwrap(self.FoundTaggedMatches));
            });
            self.DisplaySearchResults = ko.observable(false);
            
            return self;
        };

        function initTypeahead(element, dataSources) {
            element.typeahead('destroy');
            element.typeahead({
                    hint: true,
                    highlight: true,
                    minLength: minQueryLength
                },
                dataSources
                );
        }

        function setupTypeahead(redirectToKnowledgeBase, model, searchContainer, searchResultsContainer) {

            searchResultsContainer = searchResultsContainer || searchContainer;

            var sourceByIdentity = new Bloodhound({
                datumTokenizer: Bloodhound.tokenizers.obj.whitespace('Identity'),
                queryTokenizer: Bloodhound.tokenizers.whitespace,
                local: data
            });

            var sourceByTags = new Bloodhound({
                datumTokenizer: Bloodhound.tokenizers.obj.whitespace('Tags'),
                queryTokenizer: Bloodhound.tokenizers.whitespace,
                unionResults: true,
                local: data
            });

            var dataSources = 
            [{  
                name: 'searchdata',
                display: 'Identity',
                limit: 5,
                source: sourceByIdentity,
                templates: {
                    footer: function(obj) {
                        return '<div id="tt-searchfor" class="tt-suggestion tt-selectable">' + searchForText + '"<strong>' + obj.query + '</strong>"</div>';
                    },
                    notFound: function(obj) {
                        if(ko.unwrap(model.FoundTaggedMatches)) {
                            return '';
                        }
                        return '<div class="tt-suggestion">' + noSearchResultsText + '</div>';
                    }
                }
             },
             {
                 name: 'searchdatatagged',
                 display: 'Identity',
                 limit: 5,
                 source: sourceByTags,
                 templates: {
                     header: function(obj) {
                         return '<div class="tt-tagged-header">' + suggestedResultsText + '</div>';
                     }
                 }
             }];

            var typeahead = searchContainer.find('.search .typeahead');
            initTypeahead(typeahead, dataSources);
            
            var submitSearch = function(datumValues) {
                searchContainer.block();
                initTypeahead(typeahead, dataSources);

                function showSearchResults(html) {
                    model.DisplaySearchResults(true);
                    searchResultsContainer.find('.search-results').html(html);
                    searchContainer.unblock();
                };

                if (datumValues) {

                    $.post('/KnowledgeBase/QuestionAnswer', datumValues, function(html) {
                        showSearchResults(html);
                    });

                } else {

                    var query = typeahead.typeahead('val');

                    var searchResults = [];
                    ko.utils.arrayForEach(dataSources, function(dataSource) {
                        dataSource.source.search(query, function(results) {
                            ko.utils.arrayForEach(results, function(result) {
                                searchResults.push(result.Value);
                            });
                        });
                    });

                    var postModel = {
                        Questions: searchResults
                    };

                    $.post('/KnowledgeBase/SearchResults', postModel, function(html) {
                        showSearchResults(html);
                    });
                }
                    
            };

            typeahead.bind('typeahead:select', function(ev, suggestion) {
                if (redirectToKnowledgeBase) {
                    window.location.href = suggestion.RedirectUrl;
                } else {
                    submitSearch(suggestion.Value);
                }
            });

            typeahead.bind('typeahead:render', function (ev, data) {
                if (data.dataset === 'searchdata') {
                    model.FoundLiteralMatches(data.suggestions.length > 0);
                }else if (data.dataset === 'searchdatatagged') {
                    model.FoundTaggedMatches(data.suggestions.length > 0);
                }
                searchContainer.find('#tt-searchfor').on('click', function(e) {
                    submitSearch();
                });
            });

            searchContainer.find('.searchKnowledgeBase').on('submit', function (e) {
                e.preventDefault();
                submitSearch();
            });

            searchContainer.find('.input-group.search').bind('focusout', function() {
                var searchText = model.QueryText().trim();
                if (searchText) {
                    var postModel = {
                        searchText: searchText
                    };
                    $.post('/KnowledgeBase/LogSearch', postModel);
                }
            });
        }

        return {
            init: function(searchContainer, opts) {
                
                var searchResultsElement = opts.searchResultsElement;
                var redirectToKnowledgeBase = opts.redirectToKnowledgeBase;
                var model = new SearchViewModel();

                ko.applyBindings(model, searchContainer[0]);
                
                if (searchResultsElement) {
                    ko.applyBindings(model, searchResultsElement[0]);
                }
                
                setupTypeahead(redirectToKnowledgeBase, model, searchContainer, searchResultsElement);

            }

        };
    });

})(window.VisitPay.Search = window.VisitPay.Search || {}, window.ko);

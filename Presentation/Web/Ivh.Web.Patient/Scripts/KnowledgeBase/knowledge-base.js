﻿(function(knowledgeBase,search, ko) {

    knowledgeBase.Main = (function (data) {

        $(document).ready(function () {

            var searchModule = new search.Main(data);
            var searchInputElement = $('#livesearch');
            var searchResultsElement = $('#knowledge-base');
            searchModule.init(searchInputElement, {
                redirectToKnowledgeBase: true,
                searchResultsElement: searchResultsElement
            });
            
        });
    });
    
})(window.VisitPay.KnowledgeBase = window.VisitPay.KnowledgeBase || {}, window.VisitPay.Search = window.VisitPay.Search || {}, window.ko);
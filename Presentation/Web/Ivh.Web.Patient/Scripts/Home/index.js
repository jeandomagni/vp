﻿(function() {
    
    var $scope;

    function trackClick(name) {
        window._paq.push(['trackEvent', 'Homepage', 'Click', name]);
    };
    
    function showDiscounts() {

        $.blockUI();

        if ($('#modalDiscounts')) {
            $('#modalDiscounts').remove();
        }

        window.VisitPay.Common.ModalNoContainerAsync('modalDiscounts', '/Payment/DiscountsPartial', 'GET', null, false).then(function($modal) {
            $.post('/Payment/GetDiscounts', function(data) {
                var discountsVm = new window.VisitPay.DiscountsVm(data, null);
                var $discounts = $('#modalDiscounts').find('[data-context="discounts"]');
                ko.cleanNode($discounts[0]);
                ko.applyBindings(discountsVm, $discounts[0]);
                $.unblockUI();
                $modal.modal('show');
                trackClick('Discounts');
            });
        });

    };
    
    $(document).ready(function () {

        $scope = $('#section-home');
        
        $scope.on('click', '#aDiscountEligible', showDiscounts);

        $(document).on('guarantorFilterChanged', function (e, obfuscatedVpGuarantorId) {
            $.blockUI();
            window.location.reload(true);
        });

        $.post('/Home/ShouldShowRegistrationSurvey', function (result) {
            if (result) {
                window.VisitPay.Survey.ShowSurvey(
                    {
                        "SurveyName": 'Registration'
                    });
            }
        });



    });

})();
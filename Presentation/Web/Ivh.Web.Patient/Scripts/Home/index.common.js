﻿(function () {
    
    function loadVisits() {

        // elements
        var $grid = $('.grid-visits');

        // setup grid
        var gridModel = window.VisitPay.VpGrid(function () { return {}; }, '/Account/CurrentStatementVisits', 10, 'DischargeDate', 'desc');
        ko.applyBindings(gridModel, $grid[0]);

        // grid clicks
        $(document).off('gridrowclick.visits').on('gridrowclick.visits', '.grid-visits > .grid-row', function (e, obj) {
            window._paq.push(['trackEvent', 'Homepage', 'Click', 'Visit Details - ' + (obj.isVisible ? 'Show' : 'Hide')]);
        });

        // load
        gridModel.Search(false).done(function () {
            $grid.slideDown(150);
        });
    };
    
    $(document).ready(function () {
        
        loadVisits();
        
        $('#btnEditPayment').on('click', function (e) {
            e.preventDefault();
            window.VisitPay.PaymentEdit.Initialize(null).then(function (hasChanged) {
                if (hasChanged) {
                    window.location.reload(true);
                }
            });
        });
    });

})();
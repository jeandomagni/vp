﻿namespace Ivh.Web.Patient
{
    using System;
    using System.Web.Helpers;
    using System.Web.Mvc;
    using System.Web.Optimization;
    using System.Web.Routing;
    using Autofac;
    using Common.DependencyInjection;
    using Common.ServiceBus.Constants;
    using Common.Web;
    using Common.Web.Administration;
    using MassTransit;
    using Services;
    using SessionFacade;

    public class MvcApplication : MvcApplicationBase
    {
        public MvcApplication() : base("Patient")
        {
        }

        protected override void Application_Start()
        {
            base.Application_Start();

            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            AntiForgeryConfig.SuppressXFrameOptionsHeader = true;
            ApplicationHost.ConfigureStartUp();
        }

        protected void Session_Start()
        {
            if (this.Session.IsReadOnly)
            {
                return;
            }

            this.Session["temp"] = DateTime.UtcNow.Ticks.ToString();

            IWebPatientSessionFacade sessionFacade = DependencyResolver.Current.GetService<IWebPatientSessionFacade>();
            sessionFacade.ReferringUrl = this.Request.UrlReferrer?.ToString();

            if (this.User.Identity.IsAuthenticated)
            {
                GuarantorFilterService guarantorFilterService = DependencyResolver.Current.GetService<GuarantorFilterService>();
                guarantorFilterService.ObfuscateGuarantors();
            }
        }

        protected override void Application_End(object sender, EventArgs e)
        {
            try
            {
                IBusControl serviceBus = IvinciContainer.Instance.Container().Resolve<IBusControl>();
                if (serviceBus != null)
                {
                    serviceBus?.Stop(TimeSpan.FromSeconds(ServiceBus.StopTimeoutSeconds));
                    IvinciContainer.Instance.Container().Dispose();
                }
            }
            catch
            {
            }
            finally
            {
                base.Application_End(sender, e);
            }
        }
    }
}
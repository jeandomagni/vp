﻿using System.Web;

[assembly: PreApplicationStartMethod(typeof(Ivh.Web.Patient.ElmahConfig), "Configure")]
namespace Ivh.Web.Patient
{
    using Common.Data.Services;

    public static class ElmahConfig
    {
        public static void Configure()
        {
            Common.Elmah.ElmahConfig.Configure(ConnectionStringService.Instance.LoggingConnection.Value);
        }
    }
}
﻿using System.Web.Mvc;
using System.Web.Routing;

namespace Ivh.Web.Patient
{
    using System.Web.Http;

    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");
            routes.IgnoreRoute("{*scss}", new { scss = @".*\.scss(/.*)?" });
            routes.IgnoreRoute("content/client/{*image}", new { image = @".*\.(png|gif|jpg|jpeg|pdf)(/.)?" });

            routes.MapRoute(
                name: "VanityUrlRedirects",
                url: "r/{VanityUrlKeyName}",
                defaults: new { controller = "VanityUrlRedirects", action = "Login", vanityUrlKeyName = "default" }
            );

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Landing", action = "Index", id = UrlParameter.Optional },
                namespaces: new[] { "Ivh.Web.Patient.Controllers" }
            );
        }
    }
}

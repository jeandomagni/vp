﻿namespace Ivh.Web.Patient
{
    using System.Web.Mvc;
    using Attributes;
    using Common.VisitPay.Enums;
    using Common.Web.Attributes;
    using Common.Web.Filters;

    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new LoggingCorrelationAttribute());
            filters.Add(new ContentSecurityPolicyFilterAttribute());
            filters.Add(new TransportSecurityPolicyFilterAttribute());
            filters.Add(new RedirectAttribute());
            filters.Add(new HandleErrorAttribute());
            filters.Add(new EmulateUserAttribute());
            filters.Add(new ForceLogoutFilterAttribute());
            filters.Add(new RequiresAcknowledgementAttribute());
            filters.Add(new RequiresSsoAuthorizationAttribute());
            filters.Add(new DisableCacheAttribute());
            filters.Add(new IvinciHandleErrorAttribute());
            filters.Add(new RestrictedCountryCheckAttribute(ApplicationEnum.VisitPay));
            filters.Add(new LocalizationFilterAttribute(clientDto => clientDto.LanguageAppDefaultName, clientDto => clientDto.LanguageAppSupported));
        }
    }
}
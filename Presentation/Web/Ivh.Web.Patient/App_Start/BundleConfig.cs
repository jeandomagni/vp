﻿namespace Ivh.Web.Patient
{
    using System.Linq;
    using System.Web.Optimization;

    public static class BundleConstants
    {
        public const string AccountPasswordExpired = "~/bundles/scripts/account/passwordexpired";
        public const string AccountRequestPassword = "~/bundles/scripts/account/requestpassword";
        public const string AccountRequestUsername = "~/bundles/scripts/account/requestusername";
        public const string AccountResetPassword = "~/bundles/scripts/account/resetpassword";
        public const string AccountMyVisits = "~/bundles/scripts/account/myvisits";
        public const string MyPayments = "~/bundles/scripts/payment/mypayments";
        public const string OfflineAccountSettings = "~/bundles/scripts/offline/offlineaccountsettings";
        public const string OfflineFinancePlan = "~/bundles/scripts/offline/offlinefinanceplan";
        public const string OfflineIndex = "~/bundles/scripts/offline/index";
        public const string OfflineLogin = "~/bundles/scripts/offlinelogin";
        public const string ReconfigureFinancePlan = "~/bundles/scripts/financeplan/reconfigureterms";
        public const string CommonVisitDetails = "~/bundles/scripts/common/visitdetails";
        public const string CommonPaymentMethods = "~/bundles/scripts/common/paymentmethods";
        public const string CommonFinancePlanSetup = "~/bundles/scripts/common/financeplansetup";
        public const string MobileFinancePlanSetup = "~/bundles/mobile/scripts/payment/financeplansetup";
    }

    public class BundleConfig
    {
        private static readonly string[] RegistrationAppScripts = {
            "~/Scripts/Shared/component.steps.js",
            "~/Scripts/Registration/app.register.js"
        };

        private static readonly string[] CommonPaymentMethodsScripts = {
            "~/Scripts/Payment/secure-pan.js",
            "~/Scripts/Payment/payment-methods-models.js",
            "~/Scripts/Payment/payment-methods-common.js",
            "~/Scripts/Payment/ach-authorization.js"
        };
        
        private static readonly string[] CommonFinancePlanScripts =
        {
            "~/Scripts/FinancePlan/finance-plan-setup-models.js",
                "~/Scripts/FinancePlan/finance-plan-setup-common.js"
        };

        public static void RegisterBundles(BundleCollection bundles)
        {
            #region javascript libraries and base/layouts

            bundles.Add(new ScriptBundle("~/bundles/scripts/jquery").Include(
                "~/Scripts/Libraries/jquery-3.2.1.js",
                "~/Scripts/Libraries/jquery.validate.js",
                "~/Scripts/Libraries/jquery.validate.unobtrusive.js",
                "~/Scripts/Libraries/jquery.maskedinput.js",
                "~/Scripts/Libraries/knockout-3.4.2.js",
                "~/Scripts/Libraries/knockout.mapping-latest.js",
                "~/Scripts/Libraries/bootstrap.js",
                "~/Scripts/Libraries/respond.js",
                "~/Scripts/Libraries/bootstrap-datepicker.js",
                "~/Scripts/Libraries/bootstrap-toggle.js",
                "~/Scripts/Libraries/inputMask/inputmask.js",
                "~/Scripts/Libraries/inputMask/jquery.inputmask.js",
                "~/Scripts/Libraries/jquery.watermark.js",
                "~/Scripts/Libraries/jquery.blockUI.js",
                "~/Scripts/Libraries/jquery.date-dropdowns.js",
                "~/Scripts/Libraries/moment.js",
                "~/Scripts/Libraries/he.js",
                "~/Scripts/Shared/jquery.validate.overrides.js"));

            bundles.Add(new ScriptBundle("~/bundles/scripts/vuejs").Include(
                "~/Scripts/Libraries/vue.js",
                "~/Scripts/Libraries/vue-router.js",
                "~/Scripts/Shared/common.vue.js"
            ));

            bundles.Add(new ScriptBundle("~/bundles/scripts/VisitPayBase").Include(
                "~/Scripts/Shared/common.js",
                "~/Scripts/Shared/common.logging.js",
                "~/Scripts/Shared/common.jquery.js",
                "~/Scripts/Shared/common.resources.js",
                "~/Scripts/Shared/common.knockout.js",
                "~/Scripts/Shared/common.knockout.extenders.js",
                "~/Scripts/Shared/vpgrid.js",
                "~/Scripts/Shared/global.js",
                "~/Scripts/Shared/validationAdapters.js",
                "~/Scripts/Shared/IdleTimer.js",
                "~/Scripts/Shared/localizationMenu.js"));

            bundles.Add(new ScriptBundle("~/bundles/scripts/layout-patient").Include(
                // support request common
                "~/Scripts/Libraries/jQuery-File-Upload-9.10.6/js/tmpl.js",
                "~/Scripts/Libraries/jQuery-File-Upload-9.10.6/js/jquery.ui.widget.js",
                "~/Scripts/Libraries/jQuery-File-Upload-9.10.6/js/jquery.iframe-transport.js",
                "~/Scripts/Libraries/jQuery-File-Upload-9.10.6/js/jquery.fileupload.js",
                "~/Scripts/Libraries/jQuery-File-Upload-9.10.6/js/jquery.fileupload-ui.js",
                "~/Scripts/Libraries/jQuery-File-Upload-9.10.6/js/jquery.fileupload-process.js",
                "~/Scripts/Libraries/jQuery-File-Upload-9.10.6/js/jquery.fileupload-validate.js",
                "~/Scripts/Support/support-request-action.js",
                // create support request
                "~/Scripts/Support/create-support-request.js",
                // view support request
                "~/Scripts/Support/support-request.js",
                // drawers
                "~/Scripts/Shared/drawer-help.js",
                "~/Scripts/Shared/drawer-notifications.js",
                "~/Scripts/Shared/drawer.js",
                // surveys
                "~/Scripts/Survey/survey.js",
                // common
                "~/Scripts/Shared/layout-patient.js"
            ));

            bundles.Add(new StyleBundle("~/bundles/css/base").Include(
                "~/Content/bootstrap-datepicker3.css",
                "~/Content/bootstrap-toggle.css",
                "~/Content/Css/jquery.date-dropdowns.css",
                "~/Scripts/Libraries/jQuery-File-Upload-9.10.6/css/jquery.fileupload.css",
                "~/Content/chardinjs.css"));

            #endregion

            #region Chat

            bundles.Add(new ScriptBundle("~/bundles/scripts/chat").Include(
                "~/Scripts/Libraries/jquery-ui.js",
                "~/Scripts/Libraries/jquery.dialogextend.js",
                "~/Scripts/Shared/chatSession.js",
                "~/Scripts/Shared/chat.js"
            ));

            bundles.Add(new StyleBundle("~/bundles/css/chat").Include(
                "~/Content/jquery-ui.css"
            ));

            #endregion Chat

            #region /home

            bundles.Add(new ScriptBundle("~/bundles/scripts/home").Include("~/Scripts/Home/home.js"));

            #endregion

            #region /home/index

            bundles.Add(new ScriptBundle("~/bundles/scripts/home/index").Include(
                "~/Scripts/Payment/discounts-vm.js",
                "~/Scripts/Payment/payment-cancel.js",
                "~/Scripts/Payment/payment-edit.js",
                "~/Scripts/Home/index.common.js",
                "~/Scripts/Home/index.js"));

            #endregion

            #region /account/documents

            bundles.Add(new ScriptBundle("~/bundles/scripts/documents").Include(
                "~/Scripts/Account/documents.js"));

            #endregion

            #region /account/myvisits

            bundles.Add(new ScriptBundle(BundleConstants.AccountMyVisits).Include("~/Scripts/Account/my-visits.js"));

            #endregion

            #region /account/userprofile

            bundles.Add(new ScriptBundle("~/bundles/scripts/account/userprofile").Include("~/Scripts/Account/user-profile.js"));

            #endregion

            #region /account/phonetextsettings

            bundles.Add(new ScriptBundle("~/bundles/scripts/account/phonetextsettings").Include("~/Scripts/Account/phone-text-settings.js"));

            #endregion

            #region /account/activatetextmessaging

            bundles.Add(new ScriptBundle("~/bundles/scripts/account/activatetextmessaging").Include("~/Scripts/Account/activate-text-messaging.js"));

            #endregion

            #region /account/requestusername

            bundles.Add(new ScriptBundle(BundleConstants.AccountRequestUsername).Include(
                "~/Scripts/Account/request-username.js"));

            #endregion

            #region /account/passwordexpired

            bundles.Add(new ScriptBundle(BundleConstants.AccountPasswordExpired).Include(
                "~/Scripts/Account/password-expired.js"));

            #endregion

            #region /account/requestpassword

            bundles.Add(new ScriptBundle(BundleConstants.AccountRequestPassword).Include(
                "~/Scripts/Account/request-password.js"));

            #endregion

            #region /account/resetpassword

            bundles.Add(new ScriptBundle(BundleConstants.AccountResetPassword).Include(
                "~/Scripts/Account/reset-password.js"));

            #endregion

            #region /account/paymentsummary

            bundles.Add(new ScriptBundle("~/bundles/scripts/account/paymentsummary").Include(
                "~/Scripts/Account/payment-summary.js"));

            #endregion

            #region /consolidate/acceptterms

            bundles.Add(new ScriptBundle("~/bundles/scripts/consolidate/accept-terms").Include("~/Scripts/Consolidate/accept-terms.js"));

            #endregion

            #region /consolidate/accepttermsfp

            bundles.Add(new ScriptBundle("~/bundles/scripts/consolidate/accept-terms-fp").Include(
                "~/Scripts/Consolidate/accept-terms.js",
                "~/Scripts/Consolidate/payment-methods-consolidation.js"));

            #endregion

            #region /consolidate/deconsolidatefp

            bundles.Add(new ScriptBundle("~/bundles/scripts/consolidate/deconsolidate-fp").Include(
                "~/Scripts/Consolidate/deconsolidate-fp.js",
                "~/Scripts/Consolidate/payment-methods-consolidation.js"));

            #endregion

            #region /consolidate/managehousehold

            bundles.Add(new ScriptBundle("~/bundles/scripts/consolidate/manage-household").Include("~/Scripts/Consolidate/manage-household.js"));

            #endregion

            #region /consolidate/request-match

            bundles.Add(new ScriptBundle("~/bundles/scripts/consolidate/request-match").Include("~/Scripts/Consolidate/request-match.js"));

            #endregion

            #region financeplan/reconfigure

            bundles.Add(new ScriptBundle(BundleConstants.ReconfigureFinancePlan).Include(
                "~/Scripts/Payment/ach-authorization.js",
                "~/Scripts/FinancePlan/reconfigure-finance-plan.js"));

            #endregion

            #region /home/helpcenter

            bundles.Add(new ScriptBundle("~/bundles/scripts/helpcenter").Include("~/Scripts/HelpCenter/help-center.js"));

            #endregion

            #region /knowledgebase/index

            bundles.Add(new ScriptBundle("~/bundles/scripts/knowledgebase").Include(
                "~/Scripts/Libraries/typeahead.bundle.js",
                "~/Scripts/Libraries/typeahead.jquery.js",
                "~/Scripts/Libraries/bloodhound.js",
                "~/Scripts/KnowledgeBase/index.js",
                "~/Scripts/KnowledgeBase/search.js",
                "~/Scripts/KnowledgeBase/knowledgebase-voting.js",
                "~/Scripts/KnowledgeBase/knowledge-base.js"));

            #endregion

            #region /offline

            bundles.Add(new ScriptBundle(BundleConstants.OfflineAccountSettings)
                .Include(CommonPaymentMethodsScripts)
                .Include("~/Scripts/Offline/offline-paymentmethods.js"));

            bundles.Add(new ScriptBundle(BundleConstants.OfflineIndex)
                .Include(CommonPaymentMethodsScripts)
                .Include(
                    // make payment
                    "~/Scripts/Payment/discounts-vm.js",
                    "~/Scripts/Payment/arrange-payment.payment-methods.js",
                    "~/Scripts/Payment/arrange-payment.payment.js",
                    "~/Scripts/Offline/offline-makepayment.js",
                    // statements
                    "~/Scripts/Account/documents.js",
                    "~/Scripts/Offline/offline-createaccount.js"));

            bundles.Add(new ScriptBundle("~/bundles/scripts/offline/offlinefinanceplan")
                .Include(CommonPaymentMethodsScripts)
                .Include(CommonFinancePlanScripts)
                .Include(
                    "~/Scripts/Payment/arrange-payment.payment-methods.js",
                    "~/Scripts/Offline/offline-createaccount.js",
                    "~/Scripts/Offline/offline-financeplan.js"));

            #endregion

            #region offline/login

            bundles.Add(new ScriptBundle(BundleConstants.OfflineLogin).Include(
                "~/Scripts/Offline/offline-login.js"
            ));

            #endregion

            #region /payment/arrangepayment

            bundles.Add(new ScriptBundle("~/bundles/scripts/payment/arrangepayment").Include(
                "~/Scripts/Payment/discounts-vm.js",
                "~/Scripts/Payment/arrange-payment.payment-methods.js",
                "~/Scripts/Payment/arrange-payment.payment.js",
                "~/Scripts/Payment/arrange-payment.finance-plan.js",
                "~/Scripts/Payment/arrange-payment.finance-plan-state-selection.js",
                "~/Scripts/Payment/arrange-payment.main.js"));

            #endregion

            #region /payment/mypayments

            bundles.Add(new ScriptBundle(BundleConstants.MyPayments).Include(
                "~/Scripts/Account/visit-details.js",
                "~/Scripts/Payment/payment-cancel.js",
                "~/Scripts/Payment/payment-edit.js",
                "~/Scripts/Payment/payment-detail.js",
                "~/Scripts/Payment/payment-pending.js",
                "~/Scripts/Payment/payment-history.js",
                "~/Scripts/FinancePlan/finance-plan-history.js"));

            #endregion

            #region /payment/paymentmethods

            bundles.Add(new ScriptBundle("~/bundles/scripts/payment/paymentmethods").Include(
                "~/Scripts/Payment/payment-methods.js"));

            #endregion

            #region /registration/register

            bundles.Add(new ScriptBundle("~/bundles/scripts/registration/register").Include(RegistrationAppScripts.Concat(new[]
            {
                "~/Scripts/Registration/registration-timer.js",
                "~/Scripts/Registration/register.js"
            }).ToArray()));

            #endregion

            #region sso

            bundles.Add(new ScriptBundle("~/bundles/scripts/sso/settings").Include(
                "~/Scripts/Sso/health-equity-common.js",
                "~/Scripts/Sso/settings.js"));

            bundles.Add(new ScriptBundle("~/bundles/scripts/sso/terms").Include(
                "~/Scripts/Sso/terms.js"));

            #endregion

            #region sso/healthEquity

            bundles.Add(new ScriptBundle("~/bundles/scripts/sso/healthEquityWidget").Include(
                "~/Scripts/Sso/health-equity-common.js",
                "~/Scripts/Sso/health-equity-widget.js"));

            #endregion

            #region /support/mysupportrequests

            bundles.Add(new ScriptBundle("~/bundles/scripts/support/supportrequests").Include("~/scripts/support/support-requests.js"));

            #endregion

            #region /support/supportrequesst

            bundles.Add(new ScriptBundle("~/bundles/scripts/support/supportrequest").Include("~/Scripts/Support/support-request.js"));

            #endregion

            #region /error/notfound

            bundles.Add(new ScriptBundle("~/bundles/scripts/error/notfound").Include("~/Scripts/Error/not-found.js"));

            #endregion

            #region print

            bundles.Add(new StyleBundle("~/Content/Css/print").Include("~/Content/Css/print.css"));

            #endregion

            #region sidebar

            bundles.Add(new ScriptBundle("~/bundles/scripts/sidebar").Include(
                "~/Scripts/Shared/sidebar.js",
                "~/Scripts/Sso/health-equity-common.js",
                "~/Scripts/Sso/health-Equity-widget.js"));

            #endregion

            RegisterCommonBundles(bundles);
            RegisterMobileBundles(bundles);
        }

        private static void RegisterCommonBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle(BundleConstants.CommonPaymentMethods).Include(CommonPaymentMethodsScripts));

            bundles.Add(new ScriptBundle(BundleConstants.CommonVisitDetails).Include(
                "~/Scripts/Account/visits-common.js",
                "~/Scripts/Account/visit-details.js"));

            bundles.Add(new ScriptBundle(BundleConstants.CommonFinancePlanSetup).Include(CommonFinancePlanScripts));
        }

        private static void RegisterMobileBundles(BundleCollection bundles)
        {
            #region javascript libraries and base/layouts
            
            bundles.Add(new ScriptBundle("~/bundles/mobile/scripts/base").Include(
                "~/Scripts/Libraries/jquery-3.2.1.js",
                "~/Scripts/Libraries/jquery.validate.js",
                "~/Scripts/Libraries/jquery.validate.unobtrusive.js",
                "~/Scripts/Libraries/knockout-3.4.2.debug.js",
                "~/Scripts/Libraries/knockout.mapping-latest.debug.js",
                "~/Scripts/Libraries/bootstrap.js",
                "~/Scripts/Libraries/bootstrap-toggle.js",
                "~/Scripts/Libraries/inputMask/inputmask.js",
                "~/Scripts/Libraries/inputMask/jquery.inputmask.js",
                "~/Scripts/Libraries/jquery.blockUI.js",
                "~/Scripts/Libraries/moment.js",
                "~/Scripts/Libraries/he.js",
                "~/Scripts/Shared/jquery.validate.overrides.js"));

            bundles.Add(new ScriptBundle("~/bundles/mobile/scripts/vpbase").Include(
                "~/Scripts/Shared/validationAdapters.js",
                "~/Scripts/Shared/common.js",
                "~/Scripts/Shared/common.logging.js",
                "~/Scripts/Shared/common.jquery.js",
                "~/Scripts/Shared/common.knockout.js",
                "~/Scripts/Shared/common.knockout.extenders.js",
                "~/Scripts/Shared/common.resources.js",
                "~/Scripts/Shared/localizationMenu.js",
                "~/Scripts/Shared/vpgrid.js",
                "~/Areas/Mobile/Scripts/Shared/common.js",
                "~/Areas/Mobile/Scripts/layout.js",
                "~/Scripts/Shared/IdleTimer.js"));

            #endregion

            #region Chat

            bundles.Add(new ScriptBundle("~/bundles/mobile/scripts/chat").Include(
                "~/Scripts/Libraries/jquery-ui.js",
                "~/Scripts/Shared/chatSession.js",
                "~/Areas/Mobile/Scripts/Shared/chat.js"
            ));

            #endregion Chat

            #region /account/documents

            bundles.Add(new ScriptBundle("~/bundles/mobile/scripts/documents").Include(
                "~/Areas/Mobile/Scripts/Shared/grid.js",
                "~/Areas/Mobile/Scripts/Account/documents.js"));

            #endregion
            
            #region account/visitdetails

            bundles.Add(new StyleBundle("~/bundles/mobile/css/visitdetails").Include(
                "~/Areas/Mobile/Content/Css/Account/visit-details.css"));

            #endregion
            
            #region account/userprofile

            bundles.Add(new ScriptBundle("~/bundles/mobile/scripts/account/userprofile").Include(
                "~/Areas/Mobile/Scripts/Account/user-profile.js"));

            bundles.Add(new ScriptBundle("~/bundles/mobile/scripts/account/userprofileedit").Include(
                "~/Areas/Mobile/Scripts/Account/user-profile-edit.js"));

            bundles.Add(new ScriptBundle("~/bundles/mobile/scripts/account/smsacknowledgement").Include(
                "~/Areas/Mobile/Scripts/Account/sms-acknowledgement.js"));

            bundles.Add(new ScriptBundle("~/bundles/mobile/scripts/account/smsvalidatetoken").Include(
                "~/Areas/Mobile/Scripts/Account/sms-validate-token.js"));
            
            #endregion

            #region account/phonetext

            bundles.Add(new ScriptBundle("~/bundles/mobile/scripts/account/phonetext").Include(
                "~/Areas/Mobile/Scripts/Account/user-phone.js"));

            bundles.Add(new ScriptBundle("~/bundles/mobile/scripts/account/phonetextedit").Include(
                "~/Areas/Mobile/Scripts/Account/user-phone-edit.js"));

            #endregion

            #region account/editsecurityQuestions

            bundles.Add(new ScriptBundle("~/bundles/mobile/scripts/account/SecurityQuestionsEdit").Include(
                "~/Areas/Mobile/Scripts/Account/security-questions-edit.js"));

            #endregion

            #region account/editPassword

            bundles.Add(new ScriptBundle("~/bundles/mobile/scripts/account/editPassword").Include(
                "~/Areas/Mobile/Scripts/Account/edit-password.js"));

            #endregion

            #region consolidate/consolidation

            bundles.Add(new ScriptBundle("~/bundles/mobile/scripts/consolidation").Include(
                "~/Scripts/Payment/secure-pan.js",
                "~/Scripts/Payment/ach-authorization.js",
                "~/Scripts/Payment/payment-methods-models.js",
                "~/Areas/Mobile/Scripts/Payment/payment-methods.js",
                "~/Areas/Mobile/Scripts/Consolidate/consolidation.js"));

            #endregion

            #region financeplan/myfinanceplans

            bundles.Add(new ScriptBundle("~/bundles/mobile/scripts/myfinanceplans").Include(
                "~/Areas/Mobile/Scripts/Shared/grid.js"));

            #endregion

            #region financeplan/reconfigure

            bundles.Add(new ScriptBundle("~/bundles/mobile/scripts/reconfigure").Include(
                "~/Scripts/Payment/ach-authorization.js",
                "~/Areas/Mobile/Scripts/FinancePlan/reconfigure.js"));

            #endregion

            #region home/index

            bundles.Add(new ScriptBundle("~/bundles/mobile/scripts/home").Include(
                "~/Scripts/Payment/discounts-vm.js",
                "~/Areas/Mobile/Scripts/Payment/payment-edit-payment-method.js",
                "~/Scripts/Payment/payment-cancel.js",
                "~/Scripts/Payment/payment-edit.js",
                "~/Scripts/Home/index.common.js",
                "~/Areas/Mobile/Scripts/Home/index.js"));

            #endregion

            #region payment/arrangepayment

            bundles.Add(new ScriptBundle("~/bundles/mobile/scripts/payment/arrangepayment").Include(
                "~/Scripts/Payment/secure-pan.js",
                "~/Scripts/Payment/ach-authorization.js",
                "~/Scripts/Payment/payment-methods-models.js",
                "~/Areas/Mobile/Scripts/Payment/payment-methods.js",
                "~/Areas/Mobile/Scripts/Payment/arrange-payment.js"));

            bundles.Add(new ScriptBundle(BundleConstants.MobileFinancePlanSetup).Include(
                "~/Scripts/Payment/secure-pan.js",
                "~/Scripts/Payment/ach-authorization.js",
                "~/Scripts/Payment/payment-methods-models.js",
                "~/Areas/Mobile/Scripts/Payment/payment-methods.js",
                "~/Scripts/Payment/arrange-payment.finance-plan.js",
                "~/Scripts/Payment/arrange-payment.finance-plan-state-selection.js",
                "~/Areas/Mobile/Scripts/Payment/arrange-payment.finance-plan.js"));

            #endregion

            #region payment/mypayments

            bundles.Add(new ScriptBundle("~/bundles/mobile/scripts/mypayments").Include(
                "~/Areas/Mobile/Scripts/Payment/payment-edit-payment-method.js"
            ));

            #endregion
            
            #region payment/paymentmethods

            bundles.Add(new ScriptBundle("~/bundles/mobile/scripts/payment/paymentmethods").Include(
                "~/Scripts/Payment/secure-pan.js",
                "~/Scripts/Payment/ach-authorization.js",
                "~/Scripts/Payment/payment-methods-models.js",
                "~/Areas/Mobile/Scripts/Payment/payment-methods.js"));

            #endregion

            #region account/paymentsummary

            bundles.Add(new ScriptBundle("~/bundles/mobile/scripts/paymentsummary").Include(
                "~/Areas/Mobile/Scripts/Shared/grid.js",
                "~/Areas/Mobile/Scripts/Account/payment-summary.js"));

            #endregion

            #region registration/register

            bundles.Add(new ScriptBundle("~/bundles/mobile/scripts/registration/register").Include(RegistrationAppScripts.Concat(new[]
            {
                "~/Scripts/Registration/registration-timer.js",
                "~/Areas/Mobile/Scripts/Registration/register.js"
            }).ToArray()));

            #endregion

            #region sso/settings

            bundles.Add(new ScriptBundle("~/bundles/mobile/scripts/sso/settings").Include(
                "~/Areas/Mobile/Scripts/Sso/health-equity-common.js",
                "~/Scripts/Sso/settings.js"));

            #endregion

            #region sso/terms

            bundles.Add(new ScriptBundle("~/bundles/mobile/scripts/sso/terms").Include(
                "~/Areas/Mobile/Scripts/Sso/terms.js"));

            #endregion

            #region sso/healthEquity

            bundles.Add(new ScriptBundle("~/bundles/mobile/scripts/sso/healthEquityWidget").Include(
                "~/Areas/Mobile/Scripts/Sso/health-equity-common.js",
                "~/Areas/Mobile/Scripts/Sso/health-equity-widget.js"));

            #endregion

            #region support/createsupportrequest

            bundles.Add(new ScriptBundle("~/bundles/mobile/scripts/createsupportrequest").Include(
                "~/Areas/Mobile/Scripts/Support/create-support-request.js"));

            #endregion

            #region support/supportrequest

            bundles.Add(new ScriptBundle("~/bundles/mobile/scripts/supportrequest").Include(
                "~/Areas/Mobile/Scripts/Support/support-request.js"));

            #endregion

            #region support/supportrequests

            bundles.Add(new ScriptBundle("~/bundles/mobile/scripts/supportrequests").Include("~/scripts/support/support-requests.js"));

            #endregion
        }
    }
}

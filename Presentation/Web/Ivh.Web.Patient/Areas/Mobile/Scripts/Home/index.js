﻿(function() {
    
    $(document).ready(function() {

        var $scope = $('#section-home');
        
        $scope.on('click.discounts', '#aDiscountEligible', function(e) {
            e.preventDefault();
            window.VisitPayMobile.Common.OpenDiscounts();
        });

        $scope.on('click.tracker', '#aPayBill', function (e) {
            window._paq.push(['trackEvent', 'Payments', 'Access From', window.location.href]);
            window._paq.push(['trackEvent', 'Payments', 'Entry Point', 'Pay Bill Button (Mobile)']);
        });

        $scope.on('click.tracker', '#aCreateFinancePlan', function (e) {
            window._paq.push(['trackEvent', 'Payments', 'Access From', window.location.href]);
            window._paq.push(['trackEvent', 'Payments', 'Entry Point', 'Create Finance Plan Button (Mobile)']);
        });

        $scope.find('button.widget').not('.widget-disabled').on('click', function(e) {

            if ($(e.target).is('a') || $(e.target).attr('onclick') != undefined || $(e.target).parents().attr('onclick') != undefined) {
                return;
            }

            var expanded = $(this).hasClass('expanded');
            if (expanded) {
                $(this).removeClass('expanded');
            } else {
                $(this).addClass('expanded');
            }

        });

        $(window).off('supportRequestChanged').on('supportRequestChanged', function() {
            $.get('/mobile/Support/SupportRequestsWidget', {}, function(html) {
                var $temp = $(html);
                $('.widget-support-requests').html($temp.html());
            });
        });

    });

})();
﻿window.VisitPayMobile.Models.SupportRequestVm = (function (json) {

    var self = this;

    ko.mapping.fromJS(json, {}, self);
    self.TreatmentLocationAny = ko.computed(function () {
        return ko.unwrap(self.TreatmentLocation) && ko.unwrap(self.TreatmentLocation).length > 0;
    });

    self.GuarantorMessages.SortOrder = ko.observable('desc');
    self.GuarantorMessages.SortOrder.IsAsc = ko.computed(function() {
        return ko.unwrap(self.GuarantorMessages.SortOrder) === 'asc';
    });
    self.GuarantorMessages.SortOrder.IsDesc = ko.computed(function () {
        return ko.unwrap(self.GuarantorMessages.SortOrder) === 'desc';
    });
    self.GuarantorMessages.Sorted = ko.computed(function () {
        return self.GuarantorMessages().sort(function (l, r) {
            if (self.GuarantorMessages.SortOrder.IsAsc())
                return l.SupportRequestMessageId() === r.SupportRequestMessageId() ? 0 : (l.SupportRequestMessageId() < r.SupportRequestMessageId() ? -1 : 1);
            else
                return l.SupportRequestMessageId() === r.SupportRequestMessageId() ? 0 : (l.SupportRequestMessageId() < r.SupportRequestMessageId() ? 1 : -1);
        });
    });
    self.GuarantorMessages.ToggleSortOrder = function () {
        self.GuarantorMessages.SortOrder() === 'asc' ? self.GuarantorMessages.SortOrder('desc') : self.GuarantorMessages.SortOrder('asc');
    };

    self.Actions = ko.computed(function() {
        var actions = [{ text: 'Send Reply', value: 'reply' }];

        var attachments = self.Attachments();
        if (attachments.length > 0) {
            actions.push({ text: 'View Attachments (' + attachments.length + ')', value: 'attachments' });
        }

        if (self.IsOpen()) {
            actions.push({ text: 'Mark Request Complete', value: 'close' });
        }

        if (self.IsClosed()) {
            actions.push({ text: 'Re-Open Request', value: 'reopen' });
        }

        return actions;
    });
    self.Reload = function () {

        var promise = $.Deferred();

        $.post('/Support/SupportRequest', { supportRequestId: self.SupportRequestId() }, function(data) {
            ko.mapping.fromJS(data, {}, self);
            promise.resolve();
        });

        return promise;
    };

    return self;

});

window.VisitPayMobile.SupportRequest = (function (json) {

    var model = new window.VisitPayMobile.Models.SupportRequestVm(json);

    var $scope = $('#overlay-support-request');

    ko.cleanNode($scope[0]);
    ko.applyBindings(model, $scope[0]);

    //

    function bindLessMore() {

        $scope.find('.messages .body').each(function () {

            var $p = $(this).find('p:eq(0)');
            setTimeout(function () {
                if ($p.height() > 36) {
                    $p.parent().find('.more').show();
                }
            }, 500);

        });

        $scope.off('click').on('click', '.messages .body', function (e) {

            e.preventDefault();

            var $less = $(this).find('.less');
            var $more = $(this).find('.more');

            if ($more.is(':visible')) {
                $less.show();
                $more.hide();
                $(this).css('height', 'auto');
            } else if ($less.is(':visible')) {
                $less.hide();
                $more.show();
                $(this).css('height', '54px');
            }

        });

    };

    function triggerChange() {
        $(window).trigger('supportRequestChanged');
        bindLessMore();
    };

    /* actions */

    function saveAction($form, params) {

        var $textarea = $form.find('textarea');

        var messageBody = $textarea.val();
        if (messageBody.length === 0) {
            $textarea.addClass('input-validation-error');
            return;
        }

        params.supportRequestId = model.SupportRequestId();
        params.messageBody = messageBody;

        $.blockUI();
        $.post($form.attr('action'), params, function () {

            model.Reload().then(function () {
                triggerChange();
                $.unblockUI();
                window.history.back();
            });

        });

    };

    function saveReply(e) {
        e.preventDefault();
        saveAction($(this), {});
    };

    function saveReopen(e) {
        e.preventDefault();
        saveAction($(this), { CurrentSupportRequestStatus: 2 });
    };

    function saveClose(e) {
        e.preventDefault();
        saveAction($(this), { CurrentSupportRequestStatus: 1 });
    }

    function openAction(url, fnAction) {

        $.blockUI();

        if ($('#overlay-support-request-action').length === 0)
            $('body').append('<div class="overlay" id="overlay-support-request-action"></div>');

        var $overlay = $('#overlay-support-request-action');
        $overlay.html('');

        $.get(url, function (html) {

            $overlay.html(html);

            ko.cleanNode($overlay[0]);
            ko.applyBindings(model, $overlay[0]);

            $.unblockUI();
            window.location.href = '#' + $overlay.attr('id');

            var $form = $overlay.find('form');
            var $textarea = $form.find('textarea');

            $textarea.focus();
            $textarea.off('keyup').on('keyup', function () {
                if ($(this).val().length === 0) {
                    $(this).addClass('input-validation-error');
                } else {
                    $(this).removeClass('input-validation-error');
                }
            });
            $form.off('submit').on('submit', fnAction);

        });

    };

    function openReply() {
        openAction('/mobile/Support/SupportRequestReply', saveReply);
    };

    /* attachments */

    function download(supportRequestMessageAttachmentId) {

        supportRequestMessageAttachmentId = supportRequestMessageAttachmentId || null;

        $.blockUI();

        $.post('/Support/PrepareDownload', {
            supportRequestId: model.SupportRequestId(),
            supportRequestMessageAttachmentId: supportRequestMessageAttachmentId
        }, function (result) {
            $.unblockUI();
            if (result.Result === true)
                window.location.href = result.Message;
        }, 'json');

    }

    function downloadAttachment(e) {

        e.preventDefault();

        var context = ko.contextFor($(this).find('a')[0]).$data;
        download(context.SupportRequestMessageAttachmentId());

    };

    function downloadAll(e) {

        e.preventDefault();
        download();

    }

    function openAttachments(attachments) {

        $.blockUI();

        attachments = attachments || model.Attachments;

        if ($('#overlay-support-request-attachments').length === 0)
            $('body').append('<div class="overlay" id="overlay-support-request-attachments"></div>');

        var $overlay = $('#overlay-support-request-attachments');
        $overlay.html('');

        $.get('/mobile/Support/SupportRequestAttachments', function(html) {

            $overlay.html(html);

            ko.cleanNode($overlay[0]);
            ko.applyBindings({
                Attachments: attachments,
                GetFileType: function(fileType) {
                    return window.VisitPay.Common.getFileType(fileType());
                },
            }, $overlay[0]);

            $.unblockUI();
            window.location.href = '#' + $overlay.attr('id');

            $overlay.find('.mobile-link').off('click').on('click', downloadAttachment);
            $overlay.find('#btnDownloadAll').off('click').on('click', downloadAll);

        });

    };

    /* main */

    function bindActions() {
        var $select = $scope.find('.actions select');
        $select.off('change').on('change', function () {
            if ($(this).val() === 'attachments') {
                openAttachments();
            } else if ($(this).val() === 'reply') {
                openReply();
            } else if ($(this).val() === 'reopen') {
                openAction('/mobile/Support/SupportRequestReopen', saveReopen);
            } else if ($(this).val() === 'close') {
                openAction('/mobile/Support/SupportRequestClose', saveClose);
            }
            $(this).val('');
        });
    };

    function bindMessageActions() {
        $('.messages').off('click').on('click', 'button.btn-icon', function (e) {
            e.preventDefault();
            var context = ko.contextFor($(this)[0]).$data;
            openAttachments(context.Attachments);
        });

        $('#btnSendReply').off('click').on('click', function () {
            openReply();
        });
    };

    bindActions();
    bindLessMore();
    bindMessageActions();

});
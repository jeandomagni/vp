﻿window.VisitPayMobile.CreateSupportRequest = (function() {

    var $scope = $('#section-support-request-create');
    var $form = $scope.find('form');
    window.VisitPay.Common.ResetUnobtrusiveValidation($form);

    $form.off('submit').on('submit', function(e) {

        e.preventDefault();

        if (!$form.valid())
            return;

        $.blockUI();
        $form.find('#MessageBody').val($(this).find('#MessageBody').val().replace(/>/g, '').replace(/</g, ''));
        $.post($form.attr('action'), $form.serialize() + '', function(result) {

            $.unblockUI();
            if (result.Result === true) {

                $.post('/mobile/Support/CreateSupportRequestConfirmation', {}, function (cms) {
                    $(window).trigger('supportRequestChanged');
                    $.unblockUI();
                    window.VisitPay.Common.ModalGenericAlert(cms.ContentTitle, '<p class="text-left">' + cms.ContentBody + '</p>', 'Ok').done(function() {
                        window.history.back();
                    });
                });

            }

        });

    });

});
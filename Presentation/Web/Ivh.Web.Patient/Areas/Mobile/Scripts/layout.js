﻿(function ($, common, mobileCommon) {

    function modalVerticalCenter($modal) {

        var offset = 0;

        $modal.css('display', 'block');

        var $dialog = $modal.find('.modal-dialog');
        if ($dialog)
            offset = ($(window).height() - $dialog.height() - 100) / 2;

        $dialog.css('margin-top', Math.max(offset, 0));

    };

    function setOverlaySize() {
        $('.overlay > section').css('width', $(window).outerWidth());
        $('.overlay').not('.overlay-menu-visible').css('min-height', $('#wrapper').outerHeight());
        $('.overlay.overlay-menu-visible').css('min-height', $('#wrapper').outerHeight() - 86);
    }

    function initOverlay() {

        history.replaceState('', document.title, window.location.search + '#');

        var lastHash = '';
        $(window).on('hashchange', function () {

            setOverlaySize();
            var currentHash = (window.location.hash || '').replace('#', '');

            if (lastHash.length > 0) {
                if ($(lastHash).hasClass('overlay-menu-visible'))
                    $(lastHash).css('width', '0');
                else
                    $(lastHash).removeClass('slide-in').addClass('slide-out');
            }

            if (currentHash.length > 0) {
                if ($(window.location.hash).hasClass('overlay-menu-visible'))
                    $(window.location.hash).css('width', '100%');
                else
                    $(window.location.hash).removeClass('slide-out').addClass('slide-in');
            }

            lastHash = window.location.hash;

            $(window).scrollTop(0);

        });

    };

    $(document).ready(function () {

        // nav
        var $nav = $('.navbar-collapse');
        $(document).on('click', function (event) {
            if ($nav !== event.target && !$nav.has(event.target).length && $nav.hasClass('in')) {
                $nav.slideUp(250, function () {
                    $(this).removeClass('in');
                    $(this).show();
                });
            }
        });

        // global
        $(document).on('click', '.mobile-link:not(.disabled)', function () {
            var $link = $(this).find('a').not('.disabled').eq('0');
            if ($link)
                window.location.href = $link.attr('href');
        });

        $(document).on('click', 'a[href="#CreditAgreement"]', function (e) {
            e.preventDefault();
            
            var offerId = $(this).attr('data-offerid');

            mobileCommon.OpenCreditAgreement(offerId);
        });

        $(document).on('click', 'a[href="#Privacy"]', function (e) {
            e.preventDefault();

            mobileCommon.OpenPrivacy();
        });

        $(document).on('click', 'a[href$="/Legal/Privacy"]', function (e) {
            e.preventDefault();
            if (window.VisitPayMobile.State.IsUserLoggedIn) {
                mobileCommon.OpenPrivacy();
            } else {
                window.location.href = '/mobile/Legal/Privacy';
            }
        });

        $(document).on('click', 'a[href="#EsignAct"], [data-href="#EsignAct"]', function (e) {
            e.preventDefault();
            mobileCommon.OpenEsign();
        });

        $(document).on('click', 'a[href$="/Legal/TermsOfUse"], a[href="#TermsOfUse"], [data-href="#TermsOfUse"]', function (e) {
            e.preventDefault();
            mobileCommon.OpenTerms();
        });

        $(document).on('click', 'a.disabled', function (e) {
            e.preventDefault();
        });

        $(document).on('click', '.detail-grid .detail', function (e) {

            var exempt = $(e.target).is('a') || $(e.target).parents('.grid').length > 0 || $(e.target).hasClass('no-expanded');
            if (exempt)
                return;

            var expanded = $(this).hasClass('expanded');
            if (expanded)
                $(this).removeClass('expanded');
            else
                $(this).addClass('expanded');

        });

        $(document).on('click', '#section-terms a', function (e) {
            if ($(this).attr('href') === '/') {
                e.preventDefault();
                if ($(this).attr('target') === '_blank') {
                    window.open('/mobile');
                } else {
                    window.location.href = '/mobile';
                }
            }
        });
        
        $(document).on('click.open-support-request', '[data-click="create-support-request"]', function (e) {
            e.preventDefault();
            var $modal = $('.modal');
            $modal.modal('hide');
            window.VisitPayMobile.Common.OpenCreateSupportRequest();
        });

        $(document).on('click.dismiss-sso-invalidated', '[data-click="dismiss-sso-invalidated"]', function (e) {
            e.preventDefault();
            $.blockUI();
            $.post('/Sso/AcknowledgeInvalidatedSso', { ssoProvider: 2 }, function () {
                window.VisitPayMobile.Common.RefreshAlerts().done(function () { $.unblockUI(); });
            });
        });

        $(document).on('click', '[data-piwik-click]', function (e) {
            var category = $(this).data("piwik-event-category");
            var eventName = $(this).data("piwik-event-name");
            if (eventName.indexOf("Mobile") <= 0) eventName = eventName + " (Mobile)";
            window._paq.push(['trackEvent', category, 'Access From', window.location.href]);
            window._paq.push(['trackEvent', category, 'Click', eventName]);
        });

        // model
        $(window).on('show.bs.modal', function (e) {

            var $modal = $(e.target);
            if ($modal.hasClass('modal-vcenter')) {
                modalVerticalCenter($modal);
            }

        });

        // other
        common.DisablePaste('input[type=password]', $(document));

        // overlay
        initOverlay();

        // guarantor filter
        $(document).on('click', '.guarantor-filter .guarantor-option', function () {

            if ($('.guarantor-filter').attr('data-ignore') === 'true')
                return;

            $.blockUI();
            var obfuscatedVpGuarantorId = $(this).data('guarantorId');
            $.post('/GuarantorFilter/SetGuarantor', { obfuscatedVpGuarantorId: obfuscatedVpGuarantorId }).then(function () {
                window.location.reload();
            });

        });

        //
        $(document).on('click.alerts.dismiss-alert', '.alerts > div.list > div .dismiss-alert', function (e) {
            e.preventDefault();
            $.blockUI();
            var alertId = $(this).data('alert-id');
            $.post('/Account/RemoveAlert', { id: alertId }, function () {
                window.VisitPayMobile.Common.RefreshAlerts().done(function() { $.unblockUI(); });
            });
        });

        // add cursor: pointer so that iOS listens to click events (VPNG-14615)
        /iP/i.test(navigator.userAgent) && $('*').css('cursor', 'pointer');

    });

    $.blockUI.defaults.baseZ = 99999;
    $.blockUI.defaults.css.padding = 0;
    $.blockUI.defaults.overlayCSS = {
        backgroundColor: '#fff',
        cursor: 'wait',
        opacity: 0.7
    },
    $.blockUI.defaults.message = '';

    $.ajaxSetup({ data: { __RequestVerificationToken: $('#__AjaxAntiForgeryForm input[name=__RequestVerificationToken]').val() } });


}(jQuery, window.VisitPay.Common, window.VisitPayMobile.Common));
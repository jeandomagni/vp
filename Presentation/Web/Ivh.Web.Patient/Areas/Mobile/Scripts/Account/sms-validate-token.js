﻿(function (visitPay, $) {
    $(document).ready(function () {
        var form = $('#div-sms-validate-token form');

        form.on('submit', function (e) {

            e.preventDefault();
            visitPay.Common.ResetValidationSummary(form, true);
            submitForm();
        });
    });

    function submitForm() {
        $.blockUI();

        var form = $('#div-sms-validate-token form');
        var model = form.serialize();

        $.post('/Mobile/Account/EnableSms', model, function (result) {
            if (!result.Result) {
                visitPay.Common.AppendMessageToValidationSummary(form, result.Message, true);
                $('#SmsRetryCount').val(1 + parseInt($('#SmsRetryCount').val()));
            } else {
                window.location.href = "/Mobile/Account/PhoneTextSettings";
            }
            $.unblockUI();
        });
    }
})(window.VisitPay, jQuery);
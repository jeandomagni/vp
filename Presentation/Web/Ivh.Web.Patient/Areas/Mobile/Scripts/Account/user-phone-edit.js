﻿(function (visitPay, $) {
    
    var confirmText = window.VisitPay.Localization.Confirm || 'Confirm';
    var noText = window.VisitPay.Localization.No || 'No';
    var yesText = window.VisitPay.Localization.Yes || 'Yes';

    function deletePhone(e) {

        $.post('/Account/RemovePhoneMessage', { smsEnabled: false }, function(linesOfText) {

            var html = '';
            for (var i = 0; i < linesOfText.length; i++) {
                html += '<p>' + linesOfText[i] + '</p>';
            }

            visitPay.Common.ModalGenericConfirmation(confirmText, html, yesText, noText).done(function() {
                $.post('/Mobile/Account/SecondaryPhoneDelete', {}, function(result) {
                    if (!result.Result)
                        visitPay.Common.AppendMessageToValidationSummary(form, result.Message, true);
                    else {
                        window.location.href = '/Mobile/Account/PhoneTextSettings';
                    }
                    $.unblockUI();
                });
            });
        });

    };

    function submitPhone(e) {
        
        e.preventDefault();
        var $form = $(this);
        visitPay.Common.ResetValidationSummary($form, true);

        if ($form.valid()) {

            $.blockUI();

            $.post('/Account/PhoneEdit', $form.serialize(), function(result) {
                if (result.Result) {
                    window.location.href = '/Mobile/Account/PhoneTextSettings';
                } else {
                    visitPay.Common.AppendMessageToValidationSummary(form, result.Message, true);
                }
                $.unblockUI();
            });

        }
    };

    $(document).ready(function () {

        var $scope = $('#div-phone-edit');

        visitPay.Common.setInputMask($scope.find('#PhoneNumber'));

        $scope.on('submit', 'form', submitPhone);
        $scope.on('click', '#delete', deletePhone);
    });

})(window.VisitPay, jQuery);
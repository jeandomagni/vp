﻿(function (visitPay, $) {
    $(document).ready(function () {
        var form = $('#div-sms-acknowledgement form');
        $(':input[type="submit"]').prop('disabled', true);
        var canContinue = function() {
            return $('#ActivateTextAgreed').is(':checked');
        }
        $('#ActivateTextAgreed')
            .on('change', function() {
                $(':input[type="submit"]').prop('disabled', !canContinue());
            });
        form.on('submit', function (e) {

            e.preventDefault();
            visitPay.Common.ResetValidationSummary(form, true);
            submitForm();
        });
    });

    function submitForm() {
        $.blockUI();

        var form = $('#div-sms-acknowledgement form');
        var model = form.serialize();

        $.post('/Mobile/Account/SendValidationMessage', model, function (result) {
            if (!result.Result)
                visitPay.Common.AppendMessageToValidationSummary(form, result.Message, true);
            else {
                window.location.href = "/Mobile/Account/SmsValidateToken?phoneType=" + $('#SmsPhoneType').val();
            }
            $.unblockUI();
        });
    }
})(window.VisitPay, jQuery);
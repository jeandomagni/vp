﻿(function (visitPay, $) {
    $(document).ready(function () {
        var scopeElement = $('#div-profile-edit');

        scopeElement.find('#Zip').zipCode();

        var form = $('#div-profile-edit form');

        form.on('submit', function (e) {

            e.preventDefault();
            visitPay.Common.ResetValidationSummary(form, true);

            if (form.valid()) {
                showSaveConfirmDialog();
            }
        });
    });

    function showSaveConfirmDialog() {
        var promise = visitPay.Common.ModalGenericConfirmation('Confirm', 'Do you want to save the changes?', 'Yes', 'Cancel');
        promise.done(submitForm);

    };

    var formSubmitUrl = {
        Name: '/Mobile/Account/UpdateName',
        Username: '/Mobile/Account/UpdateUsername',
        Email: '/Mobile/Account/UpdateEmail',
        PhysicalAddress: '/Mobile/Account/UpdatePhysicalAddress'
    }

    var getUrl = function(formElement) {
        var id = $(formElement).attr('id');
        switch (id) {
        case 'form-name':
            return formSubmitUrl.Name;
        case 'form-username':
            return formSubmitUrl.Username;
        case 'form-Email':
            return formSubmitUrl.Email;
        case 'form-physical-address':
            return formSubmitUrl.PhysicalAddress;
        }
        return '';
    }

    function submitForm() {
        $.blockUI();

        var form = $('#div-profile-edit form');
        var model = form.serialize();

        $.post(getUrl(form), model, function (result) {
            $.unblockUI();

            if (!result.Result)
                visitPay.Common.AppendMessageToValidationSummary(form, result.Message, true);
            else {
                window.location.href = "/Mobile/Account/UserProfile";
            }
        });
    }
})(window.VisitPay, jQuery);
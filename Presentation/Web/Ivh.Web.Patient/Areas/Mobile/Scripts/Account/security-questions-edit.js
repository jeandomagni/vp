﻿(function (visitPay, $) {

    var init = function () {

        $('select[name$=".SecurityQuestionId"]').each(ensureUniqueSelection);
        $('select[name$=".SecurityQuestionId"]').on('change', function () {
            ensureUniqueSelection();
            displaySelection($(this));
            $(this).parents('.row:eq(0)').next().next().find('input[type=text][name$=".Answer"]').val('');
        });
    }

    var ensureUniqueSelection = function () {
        var selects = $('select[name$=".SecurityQuestionId"]');
        selects.each(function () {

            var self = $(this);
            self.find('option').prop('disabled', false);

            displaySelection(self);

            selects.not(self).each(function () {
                if ($(this).val().length > 0)
                    self.find('option[value="' + $(this).val() + '"]').attr('disabled', 'disabled');
            });

        });
    };

    var displaySelection = function (select) {
        if (select.children(':selected').val()) {
            var selectedText = select.children(':selected').text();
            select.parents('.form-group').find('span[id^="questionText_"]').text(selectedText);
        }
        else
            select.parents('.form-group').find('span[id^="questionText_"]').text('');
    }

    $(document).ready(function () {

        init();

        var form = $('#div-securiy-questions form:eq(0)');

        form.submit(function (e) {
            $('#alertSuccess').hide();

            e.preventDefault();
            visitPay.Common.ResetValidationSummary(form, true);

            if (form.valid()) {

                $.blockUI();


                var model = form.serialize();
                $.post("/Mobile/Account/UserSecurityQuestionsEdit", model, function (result) {
                    $.unblockUI();

                    if (!result.Result)
                        visitPay.Common.AppendMessageToValidationSummary(form, result.Message, true);
                    else {
                        window.location.href = "/Mobile";
                    }
                });
            }
        });
    });

   
})(window.VisitPay, jQuery);
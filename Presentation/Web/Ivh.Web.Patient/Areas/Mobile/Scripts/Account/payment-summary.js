﻿window.VisitPayMobile.Models.PaymentSummarySearchVm = (function () {

    var self = this;

    self.PaymentPeriod = ko.observable();
    self.PostDateBegin = ko.observable();
    self.PostDateEnd = ko.observable();

    self.toJS = function () {
        return {
            PaymentPeriod: self.PaymentPeriod(),
            PostDateBegin: self.PostDateBegin(),
            PostDateEnd: self.PostDateEnd()
        };
    };

    self.PaymentPeriod.subscribe(function (newValue) {

        var dateFormat = 'YYYY-MM-DD';
        var begin = '';
        var end = '';

        if (newValue === 'YTD') {
            begin = moment().dayOfYear(1).format(dateFormat);
            end = moment().format(dateFormat);
        } else if (newValue === 'PY') {
            begin = moment().dayOfYear(1).add(-1, 'y').format(dateFormat);
            end = moment().dayOfYear(1).add(-1, 'd').format(dateFormat);
        }

        self.PostDateBegin(begin);
        self.PostDateEnd(end);

    });

    return self;

});

window.VisitPayMobile.Models.PaymentSummaryDetailsVm = (function() {

    var self = this;

    self.PatientName = ko.observable();
    self.VisitDescription = ko.observable();
    self.VisitDate = ko.observable();
    self.VisitSourceSystemKeyDisplay = ko.observable();
    self.TotalTransactionAmount = ko.observable();
    self.Payments = ko.observableArray();

    return self;

});

window.VisitPayMobile.PaymentSummary = (function () {

    var opts = {
        Rows: 10,
        SortField: 'VisitDate',
        SortOrder: 'asc'
    };

    var detailsVm = new window.VisitPayMobile.Models.PaymentSummaryDetailsVm();
    var filterVm = new window.VisitPayMobile.Models.PaymentSummarySearchVm();
    var grid = window.VisitPayMobile.Models.GridVm({ Data: { SummaryBalanceTotal: 0 }}, filterVm, {}, '/Account/PaymentSummary', opts);
    grid.ResetCallback = function () {
        filterVm.PaymentPeriod('YTD');
    };

    ko.applyBindings(grid, $('#section-payment-summary')[0]);
    ko.applyBindings(grid, $('#info')[0]);
    ko.applyBindings(grid, $('#section-payment-summary-search')[0]);
    ko.applyBindings(detailsVm, $('#overlay-payment-summary-details')[0]);

    grid.Refresh();

    $('#section-payment-summary').on('click', '.grid .mobile-link', function (e) {

        $.blockUI();

        var context = ko.mapping.toJS(ko.contextFor($(this).find('a')[0]).$data);
        ko.mapping.fromJS(context, {}, detailsVm);

        var $overlay = $('#overlay-payment-summary-details');

        $.unblockUI();
        window.location.href = '#' + $overlay.attr('id');

    });

});
﻿(function (visitPay, $) {

    $(document).ready(function () {

        var anchorElement = $('#table-user-profile').find('a');

        anchorElement.on('click', function () {
            var editSection = $(this).attr('data-edit-section');

            window.location.href = '/Mobile/Account/UserProfileEdit?section=' + editSection;
        })
    });

})(window.VisitPay, jQuery);
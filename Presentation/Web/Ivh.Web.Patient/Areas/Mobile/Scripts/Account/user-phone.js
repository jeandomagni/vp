﻿(function (visitPay, $) {

    var $scope = $('#section-phone-text');
    
    var cancelText = window.VisitPay.Localization.Cancel || 'Cancel';
    var continueText = window.VisitPay.Localization.Continue || 'Continue';
    var okText = window.VisitPay.Localization.Ok || 'OK';
    
    function activate(smsPhoneType) {
        $.blockUI();
        $.post('/Account/CanActivate', { smsPhoneType: smsPhoneType }, function (response) {
            if (response.CanActivate) {
                window.location.href = '/Mobile/Account/SmsAcknowledgement?phoneType=' + smsPhoneType;
            } else {
                $.unblockUI();
                visitPay.Common.ModalGenericConfirmationMd(response.cms.ContentTitle, response.cms.ContentBody, okText, cancelText)
                    .done(function () {
                        activate(smsPhoneType);
                    });
            }
        });
    }

    function activatePhone(e) {
        e.preventDefault();
        e.stopPropagation();
        activate($(this).data('activate-phone-type'));
    }

    function editPhone(e) {
        e.preventDefault();

        window.location.href = '/Mobile/Account/PhoneEdit?phoneType=' + $(this).data('edit-phone-type');

    };

    function disableSms(e) {

        e.preventDefault();
        $.post('/Account/GetDisableText', {}, function(cms) {
            visitPay.Common.ModalGenericConfirmationMd(cms.ContentTitle, cms.ContentBody, continueText, cancelText).done(function() {
                $.blockUI();
                $.post('/Account/DisableSms', {}, function() {
                    window.location.reload();
                    $.unblockUI();
                });
            });
        });

    }

    function setPreference($cb) {

        $.post($cb.is(':checked') ? '/Account/AddSmsPreference' : '/Account/RemoveSmsPreference', { id: $cb.data('id') }, function () {});

    }

    $(document).ready(function() {
        
        $scope.find('.preference input[type=checkbox]').bootstrapToggle({ 
            onstyle: 'vp-toggle',
            offstyle: 'vp-toggle',
            on: null,
            off: null,
            size: 'normal'
        });
        
        $scope.on('click', '.preference > .click', function (e) {
            var $cb = $(this).parents('.row').eq(0).find('input[type=checkbox]');
            $cb.prop('checked', !$cb.prop('checked')).change();
            setPreference($cb);
        });

        $scope.on('click', 'a[data-edit-phone-type]', editPhone);

        $scope.on('click', 'a[data-activate-phone-type]', activatePhone);

        $scope.on('click', '#btnDisableSms', disableSms);
    });

})(window.VisitPay, jQuery);
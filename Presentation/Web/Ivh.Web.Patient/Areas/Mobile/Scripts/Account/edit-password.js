﻿(function (visitPay, $, ko) {

    var passwordViewModel = {
        isSavedSuccess: ko.observable(false)
    };

    $(document).ready(function () {
        
        ko.applyBindings(passwordViewModel);

        visitPay.Common.DisablePaste($('#frmEditPassword input[type=password]'));
        var form = $('#frmEditPassword');
        $(form).on('submit', function (e) {
            e.preventDefault();
            visitPay.Common.ResetValidationSummary(form, true);

            if (form.valid()) {

                $.blockUI();

                $.post('/Mobile/Account/EditPassword', form.serialize(), function (result) {

                    $.unblockUI();

                    if (!result.Result)
                        visitPay.Common.AppendMessageToValidationSummary(form, result.Message, true);
                    else {
                        passwordViewModel.isSavedSuccess(true);
                    }
                });

            }
        });

        $('#btnReturn').click(function (e) {
            window.location.href = "/mobile";
        })
    });

})(window.VisitPay, jQuery, window.ko)
﻿window.VisitPayMobile.ArrangeFinancePlan = (function(data) {

    // shim to work with desktop script
    var PaymentMethodsWrapperViewModel = (function(paymentMethod) {

        var self = this;

        function formatDisplay(target, withImage) {

            var str = '<strong>' + ko.unwrap(target.DisplayName) + '</strong>';

            withImage = withImage === undefined || withImage === null ? true : withImage;
            if (!withImage) {
                return str;
            }

            var html = '<img src="' + ko.unwrap(target.DisplayImage) + '" alt=""/>' + str;

            if (target.PaymentMethodProviderType &&
                target.PaymentMethodProviderType.DisplayImage &&
                ko.unwrap(target.PaymentMethodProviderType.DisplayImage).length > 0) {
                return '<img class="logo-provider" src="' + ko.unwrap(target.PaymentMethodProviderType.DisplayImage) + '" alt=""/>' + html;
            }

            return html;
        }

        //
        self.PrimaryPaymentMethod = ko.observable(null);
        self.PrimaryPaymentMethod.Display = ko.computed(function() {
            var primary = ko.unwrap(self.PrimaryPaymentMethod);
            if (!primary) {
                return 'Select a Payment Method';
            }
            return formatDisplay(primary);
        });
        self.PrimaryPaymentMethod.DisplayShort = ko.computed(function() {
            var primary = ko.unwrap(self.PrimaryPaymentMethod);
            if (!primary) {
                return 'Select a Payment Method';
            }
            return formatDisplay(primary, false);
        });
        self.SetPaymentMethod = function(paymentMethod) {
            if (paymentMethod == null) {
                self.PrimaryPaymentMethod(null);
            } else {
                self.PrimaryPaymentMethod(ko.mapping.fromJS(ko.mapping.toJS(paymentMethod), {}, paymentMethod));
            }
        };

        return self;

    });

    // shim to work with desktop script
    var PaymentMethodsModule = (function(primaryPaymentMethod) {

        //
        var vm = new PaymentMethodsWrapperViewModel();
        vm.SetPaymentMethod(primaryPaymentMethod);

        var paymentMethodsInstance = null;

        $(window).on('paymentMethodPrimarySet', function(e, paymentMethod) {
            if (paymentMethod === undefined || paymentMethod === null) {
                paymentMethod = new window.VisitPay.PaymentMethodListItemVm();
            } else {
                window.location.hash = '';
            }
            vm.SetPaymentMethod(paymentMethod);
        });

        function changePrimary(e) {
            if (paymentMethodsInstance == null) {
                paymentMethodsInstance = new window.VisitPayMobile.PaymentMethods(2);
            }
            if (e) {
                e.preventDefault();
            }
            $.blockUI();
            paymentMethodsInstance.Init().done(function() {
                $.unblockUI();
                window.location.hash = 'overlay-list';
            });
        };

        function setSingleUse(date) {
            // just a stub to keep the finance plan script happy
        };

        return {
            changePrimary: changePrimary,
            paymentMethods: vm,
            setSingleUse: setSingleUse
        };

    });

    function onPaymentComplete(message) {
        
        // open overlay
        window.location.href = '#overlay-receipt';
        $('#overlay-receipt').find('#text').html(message);

        // prevent back button, send home instead
        setTimeout(function() {
            $(window).off('hashchange').on('hashchange', function() {
                if (location.hash !== '#overlay-receipt') {
                    window.location.href = data.HomeUrl;
                }
            });
        }, 1000);

        return $.Deferred().resolve();

    };

    function init() {
        var opts = {
            submitUrl: data.FinancePlanSubmitUrl,
            validateUrl: data.FinancePlanValidateUrl,
            onPaymentComplete: onPaymentComplete
        };
        
        //Start modules hidden
        $('#finance-plan-state-selection').hide();
        $('#finance-plan').hide();

        //Setup Modules
        var financePlanStateSelectionModule = new window.VisitPay.ArrangePayment.FinancePlanStateSelectionModule(new PaymentMethodsModule(data.PaymentMethod), opts);
        var arrangeFinancePlan = new window.VisitPay.ArrangePayment.FinancePlanModule(new PaymentMethodsModule(data.PaymentMethod), opts);

        //Initial load of payment option(s)
        if (data.PaymentOptions.length > 1) {
            //Multiple FP options, show state selection
            financePlanStateSelectionModule.init(data.PaymentOptions);
            $('#finance-plan-state-selection').show();
        } else {
            arrangeFinancePlan.init(data.PaymentOptions[0]);
            $('#finance-plan').show();
        }

        var selectPaymentOption = function(newValue) {
            if (!newValue) {
                return;
            }

            var selectedPaymentOptionId = ko.unwrap(newValue.PaymentOptionId);
            if (!selectedPaymentOptionId) {
                return;
            }

            //Keep track of available option(s)
            var financePlanPaymentOptions = [];

            //If a specific state code is provided, choose that. Otherwise get all FP options
            if (newValue.FinancePlanOptionStateCode) {
                //Specific state selected
                var fpOptionForState = ko.utils.arrayFirst(data.PaymentOptions, function(item) {
                    return item.PaymentOptionId === selectedPaymentOptionId && item.FinancePlanOptionStateCode === newValue.FinancePlanOptionStateCode;
                });
                financePlanPaymentOptions.push(fpOptionForState);
            } else {
                ko.utils.arrayForEach(data.PaymentOptions, function(item) {
                    if (item.PaymentOptionId === selectedPaymentOptionId) {
                        financePlanPaymentOptions.push(item);
                    }
                });
            }

            if (financePlanPaymentOptions.length > 1) {
                //Have multiple state options, load state selection module
                $('#finance-plan').hide();
                financePlanStateSelectionModule.init(financePlanPaymentOptions);
                $('#finance-plan-state-selection').show();
            } else {
                //Single option selected, load FP module
                $('#finance-plan-state-selection').hide();
                arrangeFinancePlan.init(financePlanPaymentOptions[0]);
                $('#finance-plan').show();
            }
        };

        //Subscribe to messages
        financePlanStateSelectionModule.messenger.subscribe(selectPaymentOption, null, 'financePlanStateSelected');
        arrangeFinancePlan.messenger.subscribe(selectPaymentOption, null, 'financePlanChangeState');

        // guarantor filter
        $('.guarantor-filter').attr('data-ignore', true); // override default behavior
        $(document).on('click', '.guarantor-filter .guarantor-option', function() {
            $.blockUI();
            var obfuscatedVpGuarantorId = $(this).data('guarantorId');
            $.post('/GuarantorFilter/SetGuarantor', { obfuscatedVpGuarantorId: obfuscatedVpGuarantorId }).then(function() {
                if (window.location.href.indexOf('?') === -1) {
                    window.location.reload();
                } else {
                    var params = [];
                    var kvps = window.location.href.split('?')[1].split('&');
                    for (var i = 0; i < kvps.length; i++) {
                        var key = kvps[i].split('=')[0];
                        if (key === 'v') {
                            continue;
                        }
                        params.push(kvps[i]);
                    }
                    window.location.href = (window.location.href.split('?')[0] + '?v=' + obfuscatedVpGuarantorId + '&' + params.join('&')).replace('#', '');
                }
            });
        });
    }

    return {
        init: init
    };

});
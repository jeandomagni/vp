﻿window.VisitPayMobile.ArrangePayment = (function(data) {
    
    var continueText = window.VisitPay.Localization.Continue || 'Continue';
    var errorText = window.VisitPay.Localization.Error || 'Error';
    var makeChangesText = window.VisitPay.Localization.MakeChanges || 'Make Changes';
    var okText = window.VisitPay.Localization.Ok || 'Ok';
    var paymentAlertText = window.VisitPay.Localization.PaymentAlert || 'Payment Alert';

    var paymentOptionEnum = {
        Resubmit: 1,
        FinancePlan: 4,
        SpecificAmount: 5,
        SpecificVisits: 6,
        SpecificFinancePlans: 7,
        AccountBalance: 8,
        CurrentNonFinancedBalance: 9,
        HouseholdCurrentNonFinancedBalance: 10
    };

    // viewmodels
    var ArrangePaymentValidationResponseViewModel = function (data) {
        var self = this;

        self.PaymentIsValid = ko.observable();
        self.Prompt = ko.observable();
        self.Message = ko.observable();
        self.FinancePlanIsAvailable = ko.observable();
        self.MonthlyPaymentAmountTotal = ko.observable();
        self.NumberOfMonthlyPayments = ko.observable();
        self.FinancePlanOfferMessage = ko.observable();

        ko.mapping.fromJS(data, {}, self);

        return self;
    };

    var dateFormat = 'YYYY-MM-DD';
    var PaymentOptionViewModel = function(data) {

        var self = this;

        // properties from model
        self.DiscountAmount = ko.observable();
        self.DiscountGuarantorOffer = {
            VisitOffers: ko.observableArray([]).extend({ extendedArray: {} })
        };
        self.DiscountGuarantorOffer.Any = ko.computed(function() {
            return ko.unwrap(self.DiscountGuarantorOffer) !== undefined && ko.unwrap(self.DiscountGuarantorOffer) !== null && self.DiscountGuarantorOffer.VisitOffers.Any();
        });
        self.DiscountManagedOffers = ko.observableArray([]).extend({ extendedArray: {} });
        self.IsDiscountEligible = ko.observable();
        self.IsDiscountPromptPayOnly = ko.observable();
        self.DateBoundary = ko.observable();
        self.PaymentDate = ko.observable();
        self.IsDateReadonly = ko.observable();
        self.IsAmountReadonly = ko.observable();
        self.PaymentOptionId = ko.observable();
        self.PaymentOptionId.IsAccountBalance = ko.computed(function () {
            return ko.unwrap(self.PaymentOptionId) === paymentOptionEnum.AccountBalance;
        });
        self.PaymentOptionId.IsHouseholdCurrentNonFinancedBalance = ko.computed(function () {
            return ko.unwrap(self.PaymentOptionId) === paymentOptionEnum.HouseholdCurrentNonFinancedBalance;
        });
        self.PaymentOptionId.IsResubmit = ko.computed(function () {
            return ko.unwrap(self.PaymentOptionId) === paymentOptionEnum.Resubmit;
        });
        self.PaymentOptionId.IsSpecificFinancePlans = ko.computed(function () {
            return ko.unwrap(self.PaymentOptionId) === paymentOptionEnum.SpecificFinancePlans;
        });
        self.PaymentOptionId.IsSpecificVisits = ko.computed(function () {
            return ko.unwrap(self.PaymentOptionId) === paymentOptionEnum.SpecificVisits;
        });
        self.UserApplicableBalance = ko.observable();
        self.MaximumPaymentAmount = ko.observable();
        self.PaymentAmount = ko.observable();

        // lists from model
        self.FinancePlans = ko.observableArray([]).extend({ selectablePaymentGrid: { idKey: 'FinancePlanId', amountKey: 'PaymentAmount', defaultKey: 'CurrentFinancePlanBalanceDecimal', setPaymentAmount: true } });
        self.FinancePlans.AllowInput = ko.computed(function () {
            return ko.unwrap(self.PaymentOptionId.IsSpecificFinancePlans);
        });
        self.HouseholdBalances = ko.observableArray([]).extend({ selectablePaymentGrid: { idKey: 'VpStatementId', amountKey: 'TotalBalanceDecimal', defaultKey: 'TotalBalanceDecimal', setPaymentAmount: true } });
        self.HouseholdBalances.WithBalances = ko.computed(function () {
            return ko.utils.arrayFilter(self.HouseholdBalances(), function (balance) {
                return ko.unwrap(balance.TotalBalanceDecimal) > 0;
            });
        });
        self.HouseholdBalances.WithoutBalances = ko.computed(function () {
            return ko.utils.arrayFilter(self.HouseholdBalances(), function (balance) {
                return ko.unwrap(balance.TotalBalanceDecimal) <= 0;
            });
        });
        self.Payments = ko.observableArray([]).extend({ selectablePaymentGrid: { idKey: 'FinancePlanId', amountKey: 'PaymentAmount', defaultKey: 'ScheduledPaymentAmountDecimal', setPaymentAmount: true } });
        self.Visits = ko.observableArray([]).extend({ selectablePaymentGrid: { idKey: 'VisitId', amountKey: 'PaymentAmount', defaultKey: 'DisplayBalanceDecimal', setPaymentAmount: true } });
        self.Visits.AllowInput = ko.computed(function () {
            return ko.unwrap(self.PaymentOptionId.IsSpecificVisits);
        });

        // computed properties
        self.PaymentDate.IsPromptPay = ko.computed(function () {
            var isPromptPay = moment().isSame(moment(ko.unwrap(self.PaymentDate), dateFormat), 'day');
            return isPromptPay;
        });
        self.DiscountAmount.Calculated = ko.computed(function() {

            if (ko.unwrap(self.IsDiscountPromptPayOnly) === true) {
                if (ko.unwrap(self.PaymentDate.IsPromptPay) === false) {
                    return 0;
                }
            }

            if (ko.unwrap(self.PaymentOptionId) === paymentOptionEnum.HouseholdCurrentNonFinancedBalance) {
                // household
                var discountAmount = 0;
                ko.utils.arrayForEach(self.HouseholdBalances.Selected(), function (balance) {
                    if (ko.unwrap(balance.IsDiscountEligible)) {
                        discountAmount += parseFloat(ko.unwrap(balance.DiscountAmount));
                    }
                });
                return discountAmount;
            }

            // not household
            return self.DiscountAmount();
        });
        self.PaymentAmount.Calculated = ko.computed(function() {

            if (!self.IsAmountReadonly()) {
                return self.PaymentAmount();
            }

            if (ko.unwrap(self.PaymentOptionId.IsHouseholdCurrentNonFinancedBalance)) {
                var paymentAmount = 0;
                ko.utils.arrayForEach(self.HouseholdBalances.Selected(), function (balance) {
                    paymentAmount += parseFloat(balance.TotalBalanceDecimal());
                });
                paymentAmount -= self.DiscountAmount.Calculated();
                return paymentAmount;
            }

            if (self.IsDiscountEligible()) {
                var amount = parseFloat(self.UserApplicableBalance.Calculated()) - parseFloat(self.DiscountAmount.Calculated());
                return amount;
            }

            if (ko.unwrap(self.PaymentOptionId.IsSpecificFinancePlans)) {
                return self.FinancePlans.Selected.Sum();
            }

            if (ko.unwrap(self.PaymentOptionId.IsResubmit)) {
                return self.Payments.Selected.Sum();
            }

            if (ko.unwrap(self.PaymentOptionId.IsSpecificVisits)) {
                return self.Visits.Selected.Sum();
            }

            return self.UserApplicableBalance.Calculated();
        });
        self.PaymentAmount.Calculated.IsValid = ko.computed(function() {
            return !self.IsAmountReadonly() || self.PaymentAmount.Calculated() > 0;
        });
        self.PaymentDate.Formatted = ko.computed(function () {
            return moment.utc(self.PaymentDate(), dateFormat).format('MM/DD/YYYY');
        });
        self.UserApplicableBalance.Calculated = ko.computed(function () {
            if (ko.unwrap(self.PaymentOptionId) === paymentOptionEnum.HouseholdCurrentNonFinancedBalance) {
                // household
                return self.HouseholdBalances.Selected.Sum();
            }

            // not household
            return ko.unwrap(self.UserApplicableBalance);
        });
        self.SelectedAmount = ko.computed(function() {
            if (ko.unwrap(self.PaymentOptionId) === paymentOptionEnum.HouseholdCurrentNonFinancedBalance) {
                // household
                return self.HouseholdBalances.Selected.Sum();
            }

            // not household
            return ko.unwrap(self.PaymentAmount.Calculated);
        });

        // map
        var mapping = {
            'DateBoundary': {
                update: function(options) {
                    if (options.data === undefined || options.data === null) {
                        return moment.utc().format(dateFormat).add('years', 1);
                    }
                    return moment.utc(options.data, 'MM/DD/YYYY').format(dateFormat);
                }
            },
            'DiscountGuarantorOffer': {
                update: function(options) {
                    return options.data === undefined || options.data === null ? {} : ko.mapping.fromJS(options.data, {}, self.DiscountGuarantorOffer);
                }
            },
            'HouseholdBalances': {
                update: function (options) {
                    var model = ko.mapping.fromJS(options.data, {});
                    return model;
                }
            },
            'PaymentDate': {
                update: function (options) {
                    return moment.utc(options.data, 'MM/DD/YYYY').format(dateFormat);
                }
            },
            'FinancePlans': {
                create: function(options) {
                    var model = ko.mapping.fromJS(options.data);
                    model.PaymentAmount = ko.observable(0).extend({ minmaxMoney: { minValue: 0, maxValue: model.CurrentFinancePlanBalanceDecimal } });
                    return model;
                }
            },
            'Visits': {
                create: function(options) {
                    var model = ko.mapping.fromJS(options.data);
                    model.PaymentAmount = ko.observable(0).extend({ minmaxMoney: { minValue: 0, maxValue: model.DisplayBalanceDecimal } });
                    return model;
                }
            },
            'Payments': {
                create: function(options) {
                    var model = ko.mapping.fromJS(options.data);
                    model.PaymentAmount = ko.observable(0).extend({ minmaxMoney: { minValue: 0, maxValue: model.ScheduledPaymentAmountDecimal } });
                    return model;
                }
            }
        };
        ko.mapping.fromJS(data, mapping, self);

        if (ko.unwrap(self.PaymentOptionId) === paymentOptionEnum.Resubmit) {
            // resubmit
            self.Payments.SelectAll();
        } else if (ko.unwrap(self.PaymentOptionId) === paymentOptionEnum.HouseholdCurrentNonFinancedBalance) {
            // household
            self.HouseholdBalances.SelectAll();
        }

        //
        return self;

    };

    var PaymentViewModel = function(data) {

        var self = this;

        // Response from PaymentValidate
        self.ArrangePaymentValidationResponseViewModel = ko.observable(new ArrangePaymentValidationResponseViewModel());

        //
        self.ActivePaymentOption = ko.observable(new PaymentOptionViewModel(data.PaymentOptions[0]));
        self.ActivePaymentOption.PostModel = function() {
            var o = self.ActivePaymentOption();

            if (ko.unwrap(o.PaymentOptionId.IsAccountBalance)) {
                o.FinancePlans.EnsureAll();
                o.Visits.EnsureAll();
            }

            return {
                PaymentOptionId: o.PaymentOptionId(),
                PaymentAmount: o.PaymentAmount.Calculated(),
                PaymentDate: o.PaymentDate(),
                PaymentMethodId: self.PaymentMethod.PaymentMethodId(),
                Payments: o.Payments.Selected.AsDictionary(),
                HouseholdBalances: o.HouseholdBalances.Selected.AsDictionary(),
                FinancePlans: o.FinancePlans.Selected.AsDictionary(),
                Visits: o.Visits.Selected.AsDictionary()
            };
        };
        self.ActivePaymentOption.RequiresSelection = ko.computed(function() {
            var paymentOptionId = self.ActivePaymentOption().PaymentOptionId();
            return paymentOptionId === paymentOptionEnum.Resubmit ||
                paymentOptionId === paymentOptionEnum.SpecificVisits ||
                paymentOptionId === paymentOptionEnum.SpecificFinancePlans ||
                paymentOptionId === paymentOptionEnum.HouseholdCurrentNonFinancedBalance;
        });
        self.ActivePaymentOption.HasSelection = ko.computed(function() {
            var option = ko.unwrap(self.ActivePaymentOption);
            var paymentOptionId = ko.unwrap(option.PaymentOptionId);
            if (paymentOptionId === paymentOptionEnum.Resubmit) {
                return option.Payments.Selected.Sum() > 0;
            }
            if (paymentOptionId === paymentOptionEnum.HouseholdCurrentNonFinancedBalance) {
                return option.HouseholdBalances.Selected.Sum() > 0;
            }
            if (paymentOptionId === paymentOptionEnum.SpecificVisits) {
                return option.Visits.Selected.Sum() > 0;
            }
            if (paymentOptionId === paymentOptionEnum.SpecificFinancePlans) {
                return option.FinancePlans.Selected.Sum() > 0;
            }
            return false;
        });

        //
        self.PaymentMethod = new window.VisitPay.PaymentMethodListItemVm();
        ko.mapping.fromJS(data.PaymentMethod, {}, self.PaymentMethod);

        //
        self.Messages = {
            Errors: ko.observableArray().extend({ extendedArray: {} }),
        };

        //
        self.Step = ko.observable(ko.unwrap(self.ActivePaymentOption.RequiresSelection) ? 1 : 2);
        self.Step.Is = function(s) {
            return ko.unwrap(self.Step) === s;
        }
        self.Step.One = function() {
            self.Step(1);
        };
        self.Step.IsOne = ko.computed(function() {
            return self.Step.Is(1);
        });
        self.Step.Two = function() {
            self.Step(2);
        };
        self.Step.IsTwo = ko.computed(function() {
            return self.Step.Is(2);
        });

        //
        self.IsValid = ko.computed(function() {
            if (!ko.unwrap(self.ActivePaymentOption) || !ko.unwrap(self.PaymentMethod)) {
                return false;
            }
            return ko.unwrap(self.PaymentMethod.PaymentMethodId) > 0 && self.ActivePaymentOption().PaymentAmount.Calculated.IsValid();
        });

        return self;

    };

    var DiscountsVm = function () {

        var self = this;

        self.DiscountGuarantorOffer = {
            DiscountTotal: ko.observable(''),
            FirstNameLastName: ko.observable(''),
            VisitOffers: ko.observableArray([]).extend({ extendedArray: {} })
        };
        self.DiscountGuarantorOffer.Any = ko.computed(function () {
            return ko.unwrap(self.DiscountGuarantorOffer) !== undefined && ko.unwrap(self.DiscountGuarantorOffer) !== null && self.DiscountGuarantorOffer.VisitOffers.Any();
        });
        self.DiscountManagedOffers = ko.observableArray([]).extend({ extendedArray: {} });

        self.Refresh = function (guarantorOffer, managedOffers) {

            self.DiscountGuarantorOffer.VisitOffers.Clear();

            if (guarantorOffer === undefined || guarantorOffer === null) {
                self.DiscountGuarantorOffer.DiscountTotal(0);
                self.DiscountGuarantorOffer.FirstNameLastName('');
            } else {
                self.DiscountGuarantorOffer.DiscountTotal(ko.unwrap(guarantorOffer.DiscountTotal));
                self.DiscountGuarantorOffer.FirstNameLastName(ko.unwrap(guarantorOffer.FirstNameLastName) );
                ko.utils.arrayForEach(ko.unwrap(guarantorOffer.VisitOffers) || [], function (offer) {
                    if (offer) {
                        self.DiscountGuarantorOffer.VisitOffers.push(ko.unwrap(offer));
                    }
                });
            }

            self.DiscountManagedOffers.Clear();
            ko.utils.arrayForEach(managedOffers || [], function (offer) {
                if (offer) {
                    self.DiscountManagedOffers.push(ko.unwrap(offer));
                }
            });

        }

        return self;

    };

    // model
    var vm = new PaymentViewModel(data);

    // payment methods
    var paymentMethods = null;

    function setSingleUse(paymentDate) {
        if (paymentMethods) {
            var isPromptPay = moment().isSame(moment(ko.unwrap(paymentDate), dateFormat), 'day');
            paymentMethods.SetAllowSingleUse(isPromptPay);

            if (!isPromptPay && vm.PaymentMethod && ko.unwrap(vm.PaymentMethod) && !ko.unwrap(vm.PaymentMethod.IsActive)) {
                setPaymentMethod(null);
            }

        }
    }

    function openPaymentMethods(paymentDate) {

        if (paymentMethods === null || paymentMethods === undefined) {
            paymentMethods = new window.VisitPayMobile.PaymentMethods(1);
        }

        $.blockUI();

        setSingleUse(paymentDate);
        paymentMethods.Init().done(function() {
            $.unblockUI();
            window.location.hash = 'overlay-list';
        });

    };

    function setPaymentMethod(paymentMethod) {

        if (!ko.unwrap(vm.PaymentMethod.IsActive) && ko.unwrap(vm.PaymentMethod.PaymentMethodId.Any)) {
            $.post('/Payment/RemovePaymentMethod', { paymentMethodId: ko.unwrap(vm.PaymentMethod.PaymentMethodId) });
        }

        if (paymentMethod === undefined || paymentMethod === null) {
            paymentMethod = new window.VisitPay.PaymentMethodListItemVm();
        };

        ko.mapping.fromJS(ko.mapping.toJS(paymentMethod), {}, vm.PaymentMethod);
    };

    // functions
    function showReceipt(receiptText) {

        // open overlay
        window.location.href = '#overlay-receipt';
        $('#overlay-receipt').find('#text').html(receiptText);

        // prevent back button, send home instead
        setTimeout(function() {
            $(window).off('hashchange').on('hashchange', function() {
                if (location.hash !== '#overlay-receipt') {
                    window.location.href = data.HomeUrl;
                }
            });
        }, 1000);

    }

    function submitPayment() {

        window.location.href = '#overlay-submit';

        $('#overlay-submit').find('form').off('submit').on('submit', function(e) {

            e.preventDefault();
            $.blockUI();

            $.post($(this).attr('action'), { model: vm.ActivePaymentOption.PostModel() }, function(result) {

                if (result.Result === true) {
                    showReceipt(result.Message);
                } else {
                    vm.Messages.Errors.push(result.Message);
                    window.history.back();
                }

                $.unblockUI();

            });

        });

    };

    function validatePayment(e) {

        e.preventDefault();
        vm.Messages.Errors.Clear();

        if (!$(this).valid()) {
            return;
        }

        $.blockUI();

        $.post($(this).attr('action'), { model: vm.ActivePaymentOption.PostModel() }, function(result) {

            $.unblockUI();

            if (result === undefined || result === null) {
                //We expected a response but didn't get one, show generic error message
                window.VisitPay.Common.ModalGenericAlertMd(errorText, result.Message, okText);
            } else {
                //We got a response, check if it's valid
                if (result.PaymentIsValid === true) {
                    //Update the model with the reponse
                    vm.ArrangePaymentValidationResponseViewModel(result);

                    //Show submit payment modal
                    submitPayment();
                } else if (result.Prompt === true) {
                    //Payment isn't valid and we have a message to show
                    window.VisitPay.Common.ModalGenericConfirmationMd(paymentAlertText, result.Message, continueText, makeChangesText, false, { resolveAfterHidden: true }).done(submitPayment);
                } else {
                    //Payment isn't valid and we have no message, show generic error
                    window.VisitPay.Common.ModalGenericAlertMd(errorText, result.Message, okText);
                }
            }
        });

    }

    //
    function init() {

        var $scope = $('#section-arrange-payment');
        var discountsVm = new DiscountsVm();

        // bindings
        ko.applyBindings(vm, $scope[0]);
        ko.applyBindings(vm, $('#overlay-submit')[0]);
        ko.applyBindings(discountsVm, $('#overlay-discounts')[0]);
        $scope.show();

        // form
        var $form = $scope.find('#frmArrangePayment');
        $form.parseValidation();
        $form.off('submit').on('submit', validatePayment);
        $form.find('.numeric').makeNumericInput();

        // payment methods
        $scope.on('click', '#btnPaymentMethods', function(e) {
            e.preventDefault();
            openPaymentMethods(vm.ActivePaymentOption().PaymentDate);
        });
        $(window).on('paymentMethodDeleted', function(e, paymentMethodId) {
            if (ko.unwrap(vm.PaymentMethod.PaymentMethodId) === paymentMethodId) {
                setPaymentMethod(null);
            }
        });
        $(window).on('paymentMethodSelected', function (e, paymentMethod) {
            setPaymentMethod(paymentMethod);
            window.location.hash = '';
        });

        // back button
        $('.back > a').off('click').on('click', function(e) {
            if (ko.unwrap(vm.Step) === 2 && ko.unwrap(vm.ActivePaymentOption.RequiresSelection) === true) {
                e.preventDefault();
                vm.Step.One();
            }
        });

        // guarantor filter
        $('.guarantor-filter').attr('data-ignore', true); // override default behavior
        $(document).on('click', '.guarantor-filter .guarantor-option', function () {

            $.blockUI();
            var obfuscatedVpGuarantorId = $(this).data('guarantorId');
            $.post('/GuarantorFilter/SetGuarantor', { obfuscatedVpGuarantorId: obfuscatedVpGuarantorId }).then(function () {
                if (window.location.href.indexOf('?') === -1) {
                    window.location.reload();
                } else {
                    var params = [];
                    var kvps = window.location.href.split('?')[1].split('&');
                    for (var i = 0; i < kvps.length; i++) {
                        var key = kvps[i].split('=')[0];
                        if (key === 'v') {
                            continue;
                        }
                        params.push(kvps[i]);
                    }
                    window.location.href = (window.location.href.split('?')[0] + '?v=' + obfuscatedVpGuarantorId + '&' + params.join('&')).replace('#', '');
                }
            });

        });

        // discounts
        discountsVm.Refresh(vm.ActivePaymentOption().DiscountGuarantorOffer, vm.ActivePaymentOption().DiscountManagedOffers());
        vm.ActivePaymentOption().HouseholdBalances.Selected.subscribe(function () {

            var guarantorOffer = {};
            var managedOffers = [];

            ko.utils.arrayForEach(vm.ActivePaymentOption().HouseholdBalances.Selected(), function (balance) {
                if (ko.unwrap(balance.IsManaged)) {
                    managedOffers.push(ko.unwrap(balance.DiscountOffer));
                } else {
                    guarantorOffer = ko.unwrap(balance.DiscountOffer);
                }
            });

            discountsVm.Refresh(guarantorOffer, managedOffers);

        }, null, 'arrayChange');
        vm.ActivePaymentOption().PaymentDate.subscribe(setSingleUse);
        
    };

    return {
        init: init
    };

});
﻿window.VisitPayMobile.Models.PaymentMethodsVm = (function (selectMode, urls) {

    // select mode (what happens when you click on a method in the list)
    // 0 - just edit, no dialog
    // 1 - choose or edit in options dialog - return to sending page on add
    // 2 - make primary or edit in options dialog - return to sending page on add

    var self = this;
    self.SelectMode = parseInt(selectMode) || 0;
    self.ErrorMessage = ko.observable('');
    self.ErrorMessage.HasErrorMessage = ko.computed(function () {
        return self.ErrorMessage != undefined && ko.unwrap(self.ErrorMessage).length > 0;
    });

    self.PaymentMethodActive = ko.observable(new window.VisitPay.PaymentMethodVm(false));
    var paymentMethodsOther = ko.observableArray();
    var paymentMethodsPrimary = ko.observableArray();
    var invalidSubmissionMessage = window.VisitPay.Localization.PaymentValidationGeneralError;

    var $securepanframe;
    var baseHash = '';
    var cms = {
        AccountRemoveCms: {
            // this one is not actually cms
            ContentBody: '<p>' + window.VisitPay.Localization.RemovePaymentMethodPrompt + '</p>',
            ContentTitle: window.VisitPay.Localization.RemovePaymentMethod
        }
    };

    function loadCard($form) {

        $('#securepanmask').remove();
        $('#section-payment-method-detail').append('<div id="securepanmask" style="background: #fff; left: 0; right: 0; top: 0; bottom: 0; position: fixed;"></div>');

        window.VisitPay.PaymentAccountProviderHelpers.accountTypeChanged($('#AccountTypeId'), $('#ProviderTypeId'));
        $form.on('change', '#AccountTypeId', function () {
            $form.parseValidation();
            window.VisitPay.PaymentAccountProviderHelpers.accountTypeChanged($('#AccountTypeId'), $('#ProviderTypeId'));
        });

        //
        $form.find('#CardCode').numbersOnly();
        $form.find('#Zip').zipCode();

        $('.secure-pan-container').html('');

        // the iframe html
        $.get({ url: '/payment/SecurePan', cache: false }, function (html) {

            $('.secure-pan-container').html(html);

            // setup secure pan
            window.VisitPay.SecurePan.Initialize().done(function ($iframe) {

                $securepanframe = $iframe;

                // on card type changed
                $securepanframe.off('securepan.oncardtypechanged').on('securepan.oncardtypechanged', function(e, paymentMethodTypeId) {
                    self.PaymentMethodActive().PaymentMethodTypeId(paymentMethodTypeId);
                });

                // on error
                $securepanframe.off('securepan.onerror').on('securepan.onerror', function(e, result) {
                    $.unblockUI();
                    if (result.errorMessage && result.errorMessage.length > 0) {
                        if ($form.valid()) {
                            self.ErrorMessage(result.errorMessage);
                        }

                        // audit log
                        try {
                            $.post('/payment/InvalidCreditCard', {
                                errorMessage: result.errorMessage,
                                isTimeout: result.isTimeout,
                                __RequestVerificationToken: $('body').find('input[name=__RequestVerificationToken]').eq(0).val()
                            });
                        } catch (ex) { }

                    } else {
                        self.ErrorMessage('');
                    }
                });

                $('#securepanmask').remove();
                $.unblockUI();

            }).fail(function (retry) {

                if (retry) {
                    loadCard($form);
                } else {
                    $('#securepanmask').remove();
                    window.history.go(-1);
                }

            });
        });

    };

    function add() {

        self.PaymentMethodActive(new window.VisitPay.PaymentMethodVm(ko.unwrap(self.AllowSingleUse)));
        self.ErrorMessage('');

        baseHash = window.location.hash;
        window.location.hash = '#overlay-edit';

        var $form = $('#overlay-edit').find('form');
        var lastValue = 0;
        $form.off('change').on('change', '#PaymentMethodParentType', function(e) {

            var value = parseInt($(this).val());
            if (value !== lastValue) {

                lastValue = value;

                $.blockUI();
                $.post(urls.urlPaymentMethod, { paymentMethodId: 0, isNewAch: parseInt(value) !== 1 }, function(data) {

                    var methodModel = new window.VisitPay.PaymentMethodVm(ko.unwrap(self.AllowSingleUse));
                    if (data != null) {
                        ko.mapping.fromJS(data, {}, methodModel);
                    }
                    methodModel.PaymentMethodParentType(value);
                    methodModel.PaymentMethodParentType.SetValue(value);
                    self.PaymentMethodActive(methodModel);

                    self.ErrorMessage('');

                    if (value === 1) {

                        loadCard($form, methodModel);

                    } else {

                        $.unblockUI();

                    }

                });
            }

        });

    };

    function edit(paymentMethod) {
        
        $.post(urls.urlPaymentMethod, { paymentMethodId: ko.unwrap(paymentMethod.PaymentMethodId) }, function (data) {

            var newInstance = new window.VisitPay.PaymentMethodVm(false);
            ko.mapping.fromJS(data, {}, newInstance);
            newInstance.PaymentMethodParentType(ko.unwrap(paymentMethod.IsCardType) ? 1 : 2);

            self.PaymentMethodActive(newInstance);
            self.ErrorMessage('');

            window.location.hash = '#overlay-edit';

            var $form = $('#overlay-edit').find('form');
            $form.parseValidation();

            window.VisitPay.PaymentAccountProviderHelpers.accountTypeChanged($('#AccountTypeId'), $('#ProviderTypeId'));
            $form.on('change', '#AccountTypeId', function () {
                $form.parseValidation();
                window.VisitPay.PaymentAccountProviderHelpers.accountTypeChanged($('#AccountTypeId'), $('#ProviderTypeId'));
            });

        });

    };

    function setPrimary(paymentMethod) {
        
        var promise = $.Deferred();
        new window.VisitPay.AchAuthorization().prompt(paymentMethod).then(function() {
            $.post(urls.urlSetPrimary, { paymentMethodId: paymentMethod.PaymentMethodId() }, function(result) {
                if (result.IsSuccess === true) {
                    promise.resolve();
                    self.Refresh(null, true);
                } else {
                    $.unblockUI();
                    var title = window.VisitPay.Localization.Error;
                    var confirm = window.VisitPay.Localization.Ok;
                    window.VisitPay.Common.ModalGenericAlert(title, result.ErrorMessage, confirm);
                    promise.reject();
                }
            });
        });

        return promise;
    };

    function getData(json) {

        var promise = $.Deferred();

        if (json !== undefined && json !== null)
            promise.resolve(json);
        else
            $.post(urls.urlPaymentMethodsList, function (data) {
                promise.resolve(data);
            });

        return promise;
    };

    function triggerPaymentMethodDeleted(paymentMethodId) {
        $(window).trigger('paymentMethodDeleted', paymentMethodId);
    };
    
    function triggerPaymentMethodEdited() {
        $(window).trigger('paymentMethodEdited');
    };
    
    function triggerPaymentMethodSelected(paymentMethod) {
        $(window).trigger('paymentMethodSelected', paymentMethod);
    };
    
    function triggerPrimarySet(paymentMethod) {
        $(window).trigger('paymentMethodPrimarySet', paymentMethod);
    };

    self.PaymentMethodsOther = ko.computed(function () {
        return paymentMethodsOther();
    });
    self.PaymentMethodsPrimary = ko.computed(function () {
        return paymentMethodsPrimary();
    });
    self.PaymentMethodsCount = ko.computed(function() {
        return parseInt(ko.unwrap(self.PaymentMethodsOther).length) + parseInt(ko.unwrap(self.PaymentMethodsPrimary).length);
    });
    self.AllowSingleUse = ko.observable(false);

    self.Clear = function () {
        paymentMethodsOther.removeAll();
        paymentMethodsPrimary.removeAll();
    };
    self.Refresh = function (json, refreshAlerts) {

        var promise = $.Deferred();

        self.Clear();

        getData(json).then(function (data) {

            ko.utils.arrayForEach(data.AllAccounts, function(account) {

                var paymentMethod = new window.VisitPay.PaymentMethodListItemVm();
                ko.mapping.fromJS(account, {}, paymentMethod);

                if (account.IsPrimary)
                    paymentMethodsPrimary.push(paymentMethod);
                else
                    paymentMethodsOther.push(paymentMethod);

            });
            
            promise.resolve();

            if (refreshAlerts && window.VisitPay.State.IsUserLoggedIn) {
                window.VisitPayMobile.Common.RefreshAlerts();
            }

        });

        return promise;
    };
    self.Delete = function (paymentMethod) {
        
        window.VisitPay.Common.ModalGenericConfirmation(cms.AccountRemoveCms.ContentTitle, cms.AccountRemoveCms.ContentBody, window.VisitPay.Localization.Yes, window.VisitPay.Localization.No, true).done(function () {

            $.blockUI();

            var paymentMethodId = ko.unwrap(paymentMethod.PaymentMethodId);
            var isPrimary = ko.unwrap(paymentMethod.IsPrimary);

            $.post(urls.urlRemovePaymentMethod, { paymentMethodId: paymentMethodId }, function (result) {
                if (result.IsSuccess) {
                    self.Refresh(null, true).then(function () {
                        triggerPaymentMethodDeleted(paymentMethodId);
                        if (isPrimary) {
                            triggerPrimarySet(null);
                        }
                        window.location.hash = baseHash;
                        $.unblockUI();
                    });
                }
            });
        });
    };

    self.Add = add;
    self.Action = function (paymentMethod) {

        if (self.SelectMode === 1) { // choose or edit

            window.VisitPay.Common.ModalGenericConfirmation(window.VisitPay.Localization.ChooseAction, '', window.VisitPay.Localization.PayWithThisAccount, window.VisitPay.Localization.Edit, true).done(function () {

                triggerPaymentMethodSelected(paymentMethod);

            }).fail(function () {

                edit(paymentMethod);

            });

        } else if (self.SelectMode === 2) { // make primary or edit

            /*
             Pass "ResolveAfterHidden" option when opening the modal here so that the modal that was already open will close
             before the new modal is open. Otherwise the new modal won't be able to scroll.
             */
            window.VisitPay.Common.ModalGenericConfirmation(window.VisitPay.Localization.ChooseAction, '', window.VisitPay.Localization.MakePrimary, window.VisitPay.Localization.Edit, true, { resolveAfterHidden: true }).done(function () {

                setPrimary(paymentMethod).done(function() {
                    triggerPrimarySet(paymentMethod);
                });

            }).fail(function () {

                edit(paymentMethod);

            });

        } else { // just edit, no dialog

            edit(paymentMethod);

        }

    };

    var save = function($form) {

        var promise = $.Deferred();
        var securePanPromise = $.Deferred();

        var isNew = ko.unwrap(self.PaymentMethodActive().PaymentMethodId.IsNew);
        var isCardType = ko.unwrap(self.PaymentMethodActive().PaymentMethodParentType.IsCardType);
        var postUrl = isCardType ? urls.urlSavePaymentMethodCard : urls.urlSavePaymentMethodBank;
        var postModel = ko.mapping.toJS(self.PaymentMethodActive(), {
            'include': ['IsActive'],
            'ignore': ['AchTypes', 'Months', 'Years', 'PaymentMethodAccountTypes', 'PaymentMethodProviderTypes', 'AccountTypes', 'ProviderTypes']
        });

        if (!postModel.IsActive) {
            postModel.IsPrimary = false;
        }

        if (isCardType) { // card

            if ($form.find('#ProviderTypeId').prop('disabled')) {
                postModel.ProviderTypeId = 0;
            }

            if (isNew) {

                // on securepan saved
                $securepanframe.off('securepan.onsaved').on('securepan.onsaved', function(e, data) {

                    postModel.BillingId = data.BillingId;
                    postModel.GatewayToken = data.TransactionId;
                    postModel.LastFour = data.LastFour;
                    postModel.AvsCode = data.AvsCode;

                    securePanPromise.resolve();
                    $securepanframe.off('securepan.onsaved');

                });

                // trigger securepan save (this will trigger .onsaved or an error in the callback)
                $securepanframe.trigger('securepan.save', {
                    cvv: postModel.CardCode,
                    expM: postModel.ExpirationMonth,
                    expY: postModel.ExpirationYear,
                    name: postModel.NameOnCard,
                    address1: postModel.AddressLine1,
                    address2: postModel.AddressLine2,
                    city: postModel.City,
                    state: postModel.State,
                    zip: postModel.Zip
                });

            } else {

                securePanPromise.resolve();

            }

        } else  {

            if (isNew) {
                postModel.RoutingNumber = $form.find('#RoutingNumber').val().replace(/\D/g, '');
                postModel.AccountNumber = $form.find('#AccountNumber').val().replace(/\D/g, '');
                postModel.AccountNumberConfirm = $form.find('#AccountNumberConfirm').val().replace(/\D/g, '');
            }

            securePanPromise.resolve();

        }

        securePanPromise.done(function() {

            $.post(postUrl, { model: postModel }, function(result) {

                if (result.IsSuccess) {

                    self.Refresh(null, true).then(function() {

                        if (window.VisitPayMobile.PaymentMethodUrlBehaviorOverrides && window.VisitPayMobile.PaymentMethodUrlBehaviorOverrides.AfterSave &&
                            typeof window.VisitPayMobile.PaymentMethodUrlBehaviorOverrides.AfterSave === 'function') {
                            // registration needs to override the hash change
                            window.VisitPayMobile.PaymentMethodUrlBehaviorOverrides.AfterSave();
                        } else {
                            // default behavior
                            window.location.hash = baseHash;
                            triggerPaymentMethodEdited();
                        }

                        var savedPaymentMethod = ko.mapping.fromJS(result.PaymentMethod, {}, new window.VisitPay.PaymentMethodListItemVm());
                        if (isNew) {
                            if (self.SelectMode === 1) {
                                triggerPaymentMethodSelected(savedPaymentMethod);
                            } else if (self.SelectMode === 2 && ko.unwrap(savedPaymentMethod.IsPrimary)) {
                                triggerPrimarySet(savedPaymentMethod);
                            }
                        }

                        $.unblockUI();
                        promise.resolve();

                    });

                } else {

                    self.ErrorMessage(result.ErrorMessage);
                    $(window).scrollTop(0);
                    $.unblockUI();
                    promise.reject();

                }

            });

        });

        return promise;
    };

    self.Save = function (e) {

        e.cancelBubble = true;
        if (e.stopPropagation)
            e.stopPropagation();

        self.ErrorMessage('');

        var $form = $(e);
        $form.parseValidation();

        var formIsValid = $form.valid();
        var paymentMethod = ko.unwrap(self.PaymentMethodActive);
        var promise = $.Deferred();

        if ($securepanframe && $securepanframe.length > 0 && ko.unwrap(paymentMethod.PaymentMethodParentType.IsCardType) && ko.unwrap(paymentMethod.PaymentMethodId.IsNew)) {

            // on securepan clientside validation complete
            $securepanframe.off('securepan.onvalidate').on('securepan.onvalidate', function(ev, result) {

                if (formIsValid && result.isValid) {
                    // form + securepan valid
                    promise.resolve(true);
                } else {
                    // one or both invalid
                    promise.reject();
                }

                $securepanframe.off('securepan.onvalidate');
            });

            // trigger securepan to run clientside validation  (this will trigger .onvalidate or an error in the callback)
            $securepanframe.trigger('securepan.validate');

        }
        else
        {
            if (formIsValid) {
                promise.resolve();
            } else {
                promise.reject();
            }
        }

        promise.done(function() {

            $.blockUI();
            save($form);

        }).fail(function() {

            $.unblockUI();
            self.ErrorMessage(invalidSubmissionMessage);

        });

    };

    return self;

});

window.VisitPayMobile.PaymentMethods = (function (selectMode, opts) {

    var urls = $.extend({}, {
        urlPaymentMethodsList: '/Payment/GetPaymentMethodsList',
        urlPaymentMethod: '/Payment/PaymentMethod',
        urlSavePaymentMethodBank: '/Payment/SavePaymentMethodBank',
        urlSavePaymentMethodCard: '/Payment/SavePaymentMethodCard',
        urlRemovePaymentMethod: '/Payment/RemovePaymentMethod',
        urlSetPrimary: '/Payment/SetPrimaryPaymentMethod',
    }, opts || {});

    var model = new window.VisitPayMobile.Models.PaymentMethodsVm(selectMode, urls);

    ko.cleanNode($('#section-payment-method-list')[0]);
    ko.cleanNode($('#section-payment-method-detail')[0]);

    ko.applyBindings(model, $('#section-payment-method-list')[0]);
    ko.applyBindings(model, $('#section-payment-method-detail')[0]);

    var self = this;

    self.Init = function (json) {

        var promise = $.Deferred();

        model.Refresh(json).then(function () {
            promise.resolve();
        });

        return promise;

    };

    self.SetAllowSingleUse = function(b) {
        model.AllowSingleUse(b);
    }

    self.GetPrimary = function () {
        return model.PaymentMethodsPrimary().length > 0 ? model.PaymentMethodsPrimary()[0] : null;
    };

    self.SetSelectMode = function (selectMode) {
        model.SelectMode = selectMode;
    }

    $(document).ready(function () {

        $('body').on('focus', '#RoutingNumber', function () {
            $(this).inputmask('remove').inputmask('999999999', { 'clearIncomplete': true });
        });

        var accountMask = '9999[9999999999999]';
        $('body').on('focus', '#AccountNumber', function () {
            $(this).inputmask('remove').inputmask(accountMask, { 'clearIncomplete': true });
        });

        $('body').on('focus', '#AccountNumberConfirm', function () {
            $(this).inputmask('remove').inputmask(accountMask, { 'clearIncomplete': true });
        });

        $('body').on('blur', '#AccountNumberConfirm', function () {
            // remove mask values (underscore) and revalidate so compare works
            $(this).inputmask('remove');
            $($(this).parents('form')[0]).validate().element($(this));
            $(this).inputmask(accountMask, { 'clearIncomplete': true });
        });

    });

    return self;

});
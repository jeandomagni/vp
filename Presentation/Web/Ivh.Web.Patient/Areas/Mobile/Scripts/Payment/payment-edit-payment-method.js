﻿window.VisitPayMobile.PaymentEditPaymentMethod = (function() {
    
    var paymentMethods = null;
    var promise = null;

    function paymentMethodSelected(e, paymentMethod) {
        window.location.hash = '';
        promise.resolve(ko.unwrap(paymentMethod.PaymentMethodId));
    };

    function initialize(selectMode) {

        promise = $.Deferred();
        
        $(window).off('paymentMethodDeleted').on('paymentMethodDeleted', function() { promise.resolve(); });
        $(window).off('paymentMethodEdited').on('paymentMethodEdited', function() { promise.resolve(); });
        $(window).off('paymentMethodSelected').on('paymentMethodSelected', paymentMethodSelected);
        $(window).off('paymentMethodPrimarySet').on('paymentMethodPrimarySet', paymentMethodSelected);
        
        if (paymentMethods == null) {
            paymentMethods = new window.VisitPayMobile.PaymentMethods(selectMode);
        } else {
            paymentMethods.SetSelectMode(selectMode);
        }
        
        $.blockUI();

        paymentMethods.Init().done(function() {
            $('#section-payment-method-list').off('click.back').on('click.back', '.back', function (e) {
                e.preventDefault();
                window.location.hash = '';
                promise.reject();
            });
            $.unblockUI();
            window.location.hash = 'overlay-list';
        });

        return promise;

    };

    function initializeRecurring() {
        return initialize(2); // 2 - make primary or edit in options dialog
    }

    function initializeScheduled() {
        return initialize(1); // 1 - choose or edit in options dialog 
    }

    return {
        initializeRecurring: initializeRecurring,
        initializeScheduled: initializeScheduled
    };

})();
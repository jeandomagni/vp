﻿window.VisitPayMobile.Common = (function() {

    var self = this;

    function findOrCreateOverlay(overlayIdName, isMenuVisible) {

        $.blockUI();

        var overlayIdSelector = '#' + overlayIdName;

        var className = 'overlay';
        if (isMenuVisible) {
            className += ' overlay-menu-visible';
        }

        if ($(overlayIdSelector).length === 0)
            $('body').append('<div class="' + className + '" id="' + overlayIdName + '"></div>');

        var $overlay = $(overlayIdSelector);
        $overlay.html(''); // clear exisiting
        return $overlay;

    }

    // $overlay: jQuery object 
    // html: string of html
    // callback: function to run after setting html
    function setOverlayContent(overlay, html, callback) {

        overlay.html(html);
        typeof callback === 'function' && callback();
        window.location.href = '#' + overlay.attr('id');
        $(document).trigger('overlay:shown', overlay);
        $.unblockUI();

    }

    // url: content url
    // callbackAsPromise: see self.OpenDiscounts for ajax example, self.OpenCreateSupportRequest for simple function example
    function navigateToOverlayUrl(overlayIdName, isMenuVisible, url, callbackAsPromise) {

        var $overlay = findOrCreateOverlay(overlayIdName, isMenuVisible);

        var contentPromise = $.Deferred();
        $.get(url, function(html) {
            contentPromise.resolve(html);
        });

        $.when(contentPromise, (callbackAsPromise || $.Deferred().resolve())).done(function(html, callback) {
            setOverlayContent($overlay, html, callback);
        });

    };

    function navigateToOverlayHtml(overlayIdName, isMenuVisible, html, callback) {

        var $overlay = findOrCreateOverlay(overlayIdName, isMenuVisible);
        setOverlayContent($overlay, html, callback);

    };

    self.OpenCreditAgreement = function(offerId, version) {

        var url = '/mobile/Legal/CreditAgreementPartial?o=' + offerId + '&v=' + version;
        navigateToOverlayUrl('overlay-credit-agreement', false, url, null);

    };

    self.OpenPrivacy = function(version) {

        var url = '/mobile/Legal/PrivacyPartial';
        navigateToOverlayUrl('overlay-privacy', false, url, null);

    };

    self.OpenDiscounts = function() {

        var url = '/mobile/Payment/DiscountsPartial';
        var callbackPromise = $.Deferred();

        $.post('/Payment/GetDiscounts', {}, function(data) {
            callbackPromise.resolve(function() {
                var discountsVm = new window.VisitPay.DiscountsVm(data, null);
                var $discounts = $('#section-payment-discounts');
                ko.cleanNode($discounts[0]);
                ko.applyBindings(discountsVm, $discounts[0]);
            });
        });

        navigateToOverlayUrl('overlay-discounts', false, url, callbackPromise);

    };

    self.OpenEsign = function() {

        var url = '/mobile/Legal/EsignPartial';
        navigateToOverlayUrl('overlay-esign', false, url, null);

    };

    self.OpenTerms = function() {

        var url = '/mobile/Legal/TermsPartial';
        navigateToOverlayUrl('overlay-terms', false, url, null);

    };
    
    self.OpenSupportRequest = function(supportRequestId) {

        var url = '/mobile/Support/SupportRequestPartial';
        var callbackPromise = $.Deferred();

        $.post('/Support/SupportRequest', { supportRequestId: supportRequestId }, function(data) {
            callbackPromise.resolve(function() {
                var obj = new window.VisitPayMobile.SupportRequest(data);
            });
        });

        navigateToOverlayUrl('overlay-support-request', true, url, callbackPromise);

    };
    
    self.OpenCreateSupportRequest = function(visitId) {

        var url = '/mobile/Support/CreateSupportRequest';
        if (visitId) {
            url += '?visitId=' + visitId;
        }

        navigateToOverlayUrl('overlay-support-request-create', false, url, $.Deferred().resolve(function() {
            var obj = new window.VisitPayMobile.CreateSupportRequest();
        }));

    };

    self.OpenVisitDetails = function(visitId, visitPayUserId) {

        window.VisitPay.VisitDetails().init(visitId, visitPayUserId);

    };

    self.OpenEobClaimSummary = function (htmlOverlayIdName) {
        window._paq.push(['trackEvent', 'Visit', 'Click', 'Mobile Eob - Open Claim Summary'] );
        //get html from htmlOverlayIdName
        var htmlOverlayIdSelector = '#' + htmlOverlayIdName;
        var $htmlOverlay = $(htmlOverlayIdSelector);
        var overlayHtml = $htmlOverlay.html();

        navigateToOverlayHtml('overlay-eob-claim-summary', true, overlayHtml, null);

    };

    self.RefreshAlerts = function() {
        var promise = $.Deferred();
        $.get('/mobile/Account/Alerts', function(result) {
            $('.alerts').replaceWith(result);
            promise.resolve();
        });
        return promise;
    }

    return self;

})();
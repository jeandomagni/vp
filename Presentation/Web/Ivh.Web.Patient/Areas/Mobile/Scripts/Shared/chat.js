﻿window.VisitPayMobile.Chat = (function ($) {
    var modalInstance = null;

    var urls = {
        GenerateThreadId: VisitPay.ChatSettings.ChatApiUrl + 'thread/threadId',
        ChatIframeUrl: VisitPay.ChatSettings.ChatIframeUrl,
        HelpCenter: '/mobile/Home/HelpCenter'
    };

    var closeChatButtonClicked = function (e) {
        e.preventDefault();

        if (VisitPay.ChatSession.OperatorEndedThread()) {
            //No confirmation needed, end session and close chat dialog
            closeChat();
        } else if (VisitPay.ChatSession.ActiveSession()) {
            //Confirm user wants to end the thread
            promptToEndChat();
        } else {
            //No active session, allow close
            closeChat();
        }
    };

    var chatModalClosed = function ($modal) {
        $('#helpButtonGroup').show();
    };

    function postMessageToIframe(message) {
        $('#chatIframe')[0].contentWindow.postMessage(message, '*');
    }

    function promptToEndChat() {
        var title = VisitPay.ChatSettings.ChatPrompts.EndSessionTitle;
        var message = VisitPay.ChatSettings.ChatPrompts.EndSessionMessage;
        var cancel = VisitPay.ChatSettings.ChatPrompts.Cancel;
        var confirmationPromise = VisitPay.Common.ModalGenericConfirmation(title, message, title, cancel);
        confirmationPromise.done(function () {
            //Tell iframe we are ready to end chat
            log.warn('sending endChatRequest to iframe');
            postMessageToIframe('endChatRequest');
        });
    }

    function closeChat() {
        //End session and close chat modal
        VisitPay.ChatSession.EndSession();
        modalInstance.modal('hide');
    }

    function showThreadClosedMessage() {
        //Update title/status message
        $('#chatTitle').text(VisitPay.ChatSettings.ChatTitles.SessionEnded);
        $('#chat-status-message').text(VisitPay.ChatSettings.ChatStatusMessages.OperatorDisconnected);

        //Hide overlay, show iframe
        $('#chatIframeOverlay').css('display', 'block');
        $('#chatIframe').addClass('chat-iframe-hidden');
    }

    function showChatHideOverlay() {
        //Update title
        $('#chatTitle').text(VisitPay.ChatSettings.ChatTitles.NowChatting);

        //Hide overlay, show iframe
        $('#chatIframeOverlay').css('display', 'none');
        $('#chatIframe').removeClass('chat-iframe-hidden');
    }

    //Makes room on page for help buttons
    function padContent() {
        $('#content').css('padding-bottom', $('#helpButtonGroup').height() + 35);
    }

    function loadChatIframe(apiToken) {
        var iframe = $('#chatIframe');
        log.debug('connecting chat');
        var url = urls.ChatIframeUrl;

        //Build up url params
        url += '?userToken=' + VisitPay.ChatSettings.GuarantorChatToken;
        url += '&apiToken=' + apiToken;
        url += '&threadId=' + VisitPay.ChatSession.ThreadId();
        url += '&firstName=' + VisitPay.ChatSettings.GuarantorFirstName;
        url += '&lastName=' + VisitPay.ChatSettings.GuarantorLastName;

        //Provide styling in URL
        var chatModalBody = $('.chat-modal-body');
        var styles = {
            header: {
                visible: false
            },
            launcher: {
                visible: false
            },
            window: {
                width: chatModalBody.width() + 'px',
                height: chatModalBody.height() + 'px',
                bottom: '0px',
                right: '0px'
            },
            theme: VisitPay.ChatSettings.ChatThemeColor
        };

        //Add context param
        var context = {
            styles: styles
        };
        url += '&context=' + JSON.stringify(context);
        
        log.debug('connecting to iframe with url: ', url);
        iframe.attr('src', url);

        //Save chat session info
        VisitPay.ChatSession.SetActiveSession(url);
    }

    function initChat(existingSession) {
        var chatTitle = $('#chatTitle');
        var statusMessage = $('#chat-status-message');

        if (!existingSession) {
            //New session, check chat status
            var statusPromise = VisitPay.ChatCommon.CheckChatStatus(statusMessage);
            statusPromise.done(function() {
                log.debug('status promise was successful, load iframe');

                //Chat is available, load iframe behind the scenes. This will send beacon and we can wait for operator.
                VisitPay.ChatSettings.ChatApiToken().done(function (apiToken) {
                    $.ajax({
                        url: urls.GenerateThreadId,
                        type: 'GET',
                        cache: false,
                        beforeSend: function (xhr) {
                            xhr.setRequestHeader('Authorization', 'Bearer ' + apiToken);
                        },
                        success: function (threadId) {
                            //Save the generated threadId so we can use it when connecting to iframe
                            VisitPay.ChatSession.SetThreadId(threadId);
                            loadChatIframe(apiToken);

                            //Update title/status message
                            chatTitle.text(VisitPay.ChatSettings.ChatTitles.WaitingForOperator);
                            statusMessage.text(VisitPay.ChatSettings.ChatStatusMessages.WaitingForOperator);
                        }
                    });
                });
            }).fail(function() {
                log.warn('status promise failed. chat not available.');
                $('#need-help-now').show();
                chatTitle.text(VisitPay.ChatSettings.ChatTitles.ChatUnavailable);
            });
        } else {
            log.debug('existing session, loading url in iframe');
            //Existing session, load it in iframe
            var iframe = $('#chatIframe');
            iframe.attr('src', VisitPay.ChatSession.ChatUrl());

            //If operator is already connected, go ahead and show the iframe
            if (VisitPay.ChatSession.IsOperatorConnected()) {
                showChatHideOverlay();
            } else {
                //Update title/status message to waiting for operator
                chatTitle.text(VisitPay.ChatSettings.ChatTitles.WaitingForOperator);
                statusMessage.text(VisitPay.ChatSettings.ChatStatusMessages.WaitingForOperator);
            }
        }

        //Setup Listeners
        $('#chat-dismiss-button').click(closeChatButtonClicked);
        
        $('#goToHelp').click(function (e) {
            e.preventDefault();

            closeChat();

            //Go to help
            var url = window.location.origin;
            url = url + urls.HelpCenter;
            window.location.href = url;
        });

        $(window).on('SessionTimeout', function (event, logoutUrl) {
            log.debug('mobile chat.js received SESSION TIMEOUT event: ', logoutUrl);

            //Store the logout url
            VisitPay.ChatSession.SetLogoutRedirect(logoutUrl);

            //Tell iframe we are ready to end chat, it will post back when done and user will be logged out
            log.warn('SESSION TIMEOUT :: sending endChatRequest to iframe');
            postMessageToIframe('endChatRequest');
        });
    }

    function loadChatModal(existingSession) {
        $.blockUI();
        var chatModalPromise = VisitPay.Common.ModalNoContainerAsync('chatModal', '/mobile/Chat/ChatModal', 'GET', null, true, chatModalClosed);
        chatModalPromise.done(function ($modal) {
            log.debug('chat modal loading done. showing now.');
            $.unblockUI();
            $('#helpButtonGroup').hide();
            //Show the modal
            $modal.modal();
            modalInstance = $modal;

            //Init
            initChat(existingSession);
        });
    }

    function setupIframeEventListeners() {
        //Listen for events from chat iframe
        window.addEventListener('message', function (event) {
            if (event) {
                if (event.data === 'chatClosed') {
                    //End session and close chat dialog
                    closeChat();

                    //Check if session was ending and logout if needed
                    if (VisitPay.ChatSession.ShouldLogout()) {
                        //Session was ending, perform logout
                        var logoutUrl = VisitPay.ChatSession.LogoutRedirect();
                        VisitPay.ChatSession.ClearLogoutRedirect();
                        window.location.href = logoutUrl;
                    }
                } else if (event.data === 'userConnected') {
                    //Operator is connected, show chat if it isn't already showing
                    if (!VisitPay.ChatSession.IsOperatorConnected()) {
                        VisitPay.ChatSession.SetOperatorConnected(true);
                        showChatHideOverlay();

                        //Tell iframe we are ready to chat
                        log.warn('sending guarantor ready to chat event to iframe');
                        postMessageToIframe('guarantorReady');
                    }
                } else if (event.data === 'chatThreadClosed') {
                    log.debug('chatThreadClosed event received from iframe');
                    //Let guarantor know operator ended thread
                    showThreadClosedMessage();

                    //End the session
                    VisitPay.ChatSession.EndSession();

                    //Flag the session so we don't have to prompt user when they close the chat dialog
                    VisitPay.ChatSession.SetOperatorConnected(false);
                    VisitPay.ChatSession.SetOperatorEndedThread();
                }
            }
        });
    }

    $(document).ready(function () {
        //Add padding to bottom of content to account for help button group
        padContent();

        //Check if there is an active session, load it if so
        if (VisitPay.ChatSession.ActiveSession()) {
            log.debug('had existing chat session on document ready');
            
            //Load for an existing session
            loadChatModal(true);
        }

        //Listen for chat button
        $('#chatButton').click(function (e) {
            e.preventDefault();
            log.debug('mobile chat button click');

            //Load for a new session
            loadChatModal(false);
        });

        //Listen for events from chat iframe
        setupIframeEventListeners();
    });

})(jQuery);
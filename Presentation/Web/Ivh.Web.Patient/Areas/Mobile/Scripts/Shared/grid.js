﻿window.VisitPayMobile.Models.GridVm = (function (json, searchVm, mapping, searchUrl, opts) {

    opts = opts || {};

    json = json || {};
    json.Data = $.extend({}, {
        Results: [],
        page: 1,
        records: 0,
        total: 0
    }, json.Data || {});

    var initialJson = json;
    var self = this;
    var reset = function() {

        self.Rows(opts.Rows || initialJson.Rows);
        self.SortField(opts.SortField || initialJson.SortField);
        self.SortOrder(opts.SortOrder || initialJson.SortOrder);
        ko.mapping.fromJS(initialJson.Data, mapping, self.Data);
        ko.mapping.fromJS(initialJson.Filter || initialJson.GlobalFilter, {}, self.Filter);

        if (self.ResetCallback !== undefined && $.isFunction(self.ResetCallback)) {
            self.ResetCallback();
        }

    };
    var lastFilter;

    function getData(page, additionalParameters) {
        var filter = self.Filter !== undefined && $.isFunction(self.Filter.toJS) ? self.Filter.toJS() : {};

        $.extend(filter, additionalParameters || {});

        lastFilter = filter;

        return $.post(searchUrl, { model: filter, page: page, rows: self.Rows(), sidx: self.SortField(), sord: self.SortOrder() }, function (data) {
            ko.mapping.fromJS(data, mapping, self.Data);
        });
    };

    self.Data = {};
    self.Filter = searchVm;
    self.Rows = ko.observable();
    self.SortField = ko.observable();
    self.SortOrder = ko.observable();

    reset();

    self.GetData = getData;
    self.HasResults = ko.computed(function () {
        return self.Data.records() > 0;
    });

    self.Reset = reset;
    self.Refresh = function () {
        getData(self.Data.page()).then(function () {
        });
    };
    self.Submit = function (e) {
        e.cancelBubble = true;
        if (e.stopPropagation)
            e.stopPropagation();

        var $form = $(e);
        if ($form.valid()) {
            $.blockUI();
            getData(1).then(function () {
                $.unblockUI();
                window.history.back();
            });
        }
    };

    self.Prev = function () {
        $.blockUI();
        getData(self.Data.page() - 1, lastFilter).then(function () {
            $.unblockUI();
        });
    };
    self.Prev.IsEnabled = ko.computed(function () {
        return self.Data.page() > 1;
    });

    self.Next = function () {
        $.blockUI();
        getData(self.Data.page() + 1, lastFilter).then(function () {
            $.unblockUI();
        });
    };
    self.Next.IsEnabled = ko.computed(function () {
        var rows = Math.max(self.Rows(), 1);
        return self.Data.page() < Math.ceil(self.Data.records() / rows);
    });

    self.PageText = ko.computed(function () {

        var firstRecord = self.Data.records() === 0 ? 0 : self.Data.page() * self.Rows() - (self.Rows() - 1);
        var lastRecord = Math.min(firstRecord + (self.Rows() - 1), self.Data.records());

        return firstRecord + ' - ' + lastRecord + ' of ' + self.Data.records();

    });

    return self;

});
﻿(function () {

	function continueSubmit(sender, e) {
		e.preventDefault();
		$.blockUI();

		$.post(sender.attr('action'), sender.serialize(), function (url) {
			window.location.href = url;
		});
	}

	function confirmAction(sender, accept) {
		$.blockUI();

		var $form = sender.parents('form').eq(0);
		var model = $form.serializeArray();
		model.push({ name: 'accept', value: accept });

		$.post($form.attr('action'), $.param(model), function (html) {

		    $.blockUI();

		    if ($('#overlay-terms-action').length === 0)
		        $('body').append('<div class="overlay" id="overlay-terms-action"></div>');

		    var $overlay = $('#overlay-terms-action');
		    $overlay.html(html);
			$overlay.on('submit', 'form', function (e) {
				continueSubmit($(this), e);
			});

			$.unblockUI();
			window.location.href = '#' + $overlay.attr('id');
		});
	}

	$(document).ready(function () {
		$('#section-sso-terms').on('click', '#btnAccept', function () {
			confirmAction($(this), true);
		});

		$('#section-sso-terms').on('click', '#btnDecline', function () {
			confirmAction($(this), false);
		});
	});
})();
﻿window.VisitPayMobile.HealthEquityCommon = (function() {

    var self = this;

    self.OpenSuccess = function() {

        $.blockUI();
        window._paq.push(['trackEvent', 'HealthEquity', 'Accept']);

        if ($('#overlay-healthequity-success').length !== 0)
            $('#overlay-healthequity-success').remove();

        $('body').append('<div class="overlay" id="overlay-healthequity-success"></div>');

        var $overlay = $('#overlay-healthequity-success');
        $overlay.html('');

        $.get('/mobile/Sso/HealthEquitySsoSetupSuccess', function (html) {

            $.unblockUI();
            $overlay.html(html);
            window.location.href = '#' + $overlay.attr('id');

            $overlay.on('click', '#btnHqySsoGo', function() {
                window.location.reload();
            });

            $overlay.on('click', '#btnHqySsoSuccessClose', function () {
                window.location.reload();
            });

        });

    };

    self.OpenFailed = function () {

        $.blockUI();

        if ($('#overlay-healthequity-failed').length !== 0)
            $('#overlay-healthequity-failed').remove();

        $('body').append('<div class="overlay" id="overlay-healthequity-failed"></div>');

        var $overlay = $('#overlay-healthequity-failed');
        $overlay.html('');

        $.get('/mobile/Sso/HealthEquitySsoSetupFailed', function (html) {

            $.unblockUI();
            $overlay.html(html);
            window.location.href = '#' + $overlay.attr('id');

            $overlay.on('click', '#btnHqySsoFailedClose', function() {
                window.location.hash = '';
            });

        });

    };

    self.OpenAgreement = function() {

        $.blockUI();

        if ($('#overlay-healthequity-agreement').length !== 0)
            $('#overlay-healthequity-agreement').remove();

        $('body').append('<div class="overlay" id="overlay-healthequity-agreement"></div>');

        var $overlay = $('#overlay-healthequity-agreement');
        $overlay.html('');

        $.get('/mobile/Sso/HealthEquityAgreementSso', function (html) {

            $.unblockUI();
            $overlay.html(html);
            window.location.href = '#' + $overlay.attr('id');

            $overlay.on('click', '#btnHqySsoAgreementAccept', self.OpenSuccess);
            $overlay.on('click', '#btnHqySsoAgreementDecline', self.OpenFailed);

        });

    };

    self.OpenPreTermsAgreement = function () {

        $.blockUI();

        if ($('#overlay-healthequity-preterms').length !== 0)
            $('#overlay-healthequity-preterms').remove();

        $('body').append('<div class="overlay" id="overlay-healthequity-preterms"></div>');

        var $overlay = $('#overlay-healthequity-preterms');
        $overlay.html('');

        $.get('/mobile/Sso/HealthEquityPreAgreementSso', function(html) {

            $.unblockUI();
            $overlay.html(html);
            window.location.href = '#' + $overlay.attr('id');

            $overlay.on('click', '#btnHqySsoPreAgreementContinue', self.OpenAgreement);

        });

    };

    self.initNoSsoAgreement = function (selectedItem) {
        var ssoProvider = -1;
        if (selectedItem !== undefined && selectedItem !== null) {
            ssoProvider = selectedItem.ssoProviderEnum();
        }
        var model = JSON.parse((window.VisitPay.Sso && window.VisitPay.Sso.HealthEquity) ? window.VisitPay.Sso.HealthEquity.Model || '{}' : '{}');
        if (ssoProvider === -1 && model !== undefined && model !== null && model.hasOwnProperty('SsoProviderEnum')) {
            ssoProvider = model.SsoProviderEnum;
        }

        var oAuthWindowOpen = function ($modal, url) {
            //These are the 2 outbound oauth health equity providers
            if ((url || window.VisitPay.Sso.HealthEquity.OAuthUrl) == undefined) return;
            $modal.UnblockUI = false;
            var win;
            var checkConnect;
            win = window.open(url || window.VisitPay.Sso.HealthEquity.OAuthUrl, 'OAuthWindow', 'width=1045,height=660,modal=yes,alwaysRaised=yes');

            checkConnect = setInterval(function () {
                //I wonder if i should put a max time to wait here?
                if (!win || !win.closed) return;
                clearInterval(checkConnect);
                if (window.VisitPay.Sso != undefined && window.VisitPay.Sso.HealthEquity != undefined && window.VisitPay.Sso.HealthEquity.AfterOAuthWindowClosesUrl != undefined) {
                    window.location = window.VisitPay.Sso.HealthEquity.AfterOAuthWindowClosesUrl;
                } else {
                    window.location.reload();
                }
            }, 100);
        };

        if (ssoProvider == 3 || ssoProvider == 4) {
            var urlToUse = undefined;
            if (selectedItem !== undefined && selectedItem !== null) {
                urlToUse = selectedItem.ssoProviderActionUrl();
            }
            oAuthWindowOpen({}, urlToUse);
        } else {
            $.blockUI();
            OpenPreTermsAgreement();
        }
    };


    $(document).on('click.piwik', 'div.btn-vp-toggle:enabled:not(.off)', function () {
        window._paq.push(['trackEvent', 'HealthEquity', 'ToggleWidgetOff']);
    });

    $(document).on('click.piwik', 'div.btn-vp-toggle.off', function () {
        window._paq.push(['trackEvent', 'HealthEquity', 'ToggleWidgetOn']);
    });


    return self;

})();
﻿(function(common, $) {

    var $scope = $('body');
    var model = undefined;

    if (window.VisitPay != undefined && window.VisitPay.Sso != undefined && window.VisitPay.Sso.HealthEquity != undefined) {
        model = JSON.parse(window.VisitPay.Sso.HealthEquity.Model || '{}');
    }

    var balanceModel = {};

    var setBalanceDomProperites = function(result) {
        // widget
        $scope.find('.healthequity-hsa-cash-balance').html(result['hsaWidgetCashBalance']);
        $scope.find('.healthequity-lastcalltoapi').html(result['hsaWidgetDate']);
        // sso page
        $scope.find('.healthequity-hsa-cash-balance-modal').html(result['hsaModalCashBalance']);
        $scope.find('.healthequity-hsa-investment-balance-modal').html(result['hsaModalInvestmentBalance']);
        $scope.find('.healthequity-hsa-total-balance-modal').html(result['hsaModalTotalBalance']);
        $scope.find('.healthequity-hsa-contributions-ytd-modal').html(result['hsaModalContributionsYtd']);
        $scope.find('.healthequity-hsa-distributions-ytd-modal').html(result['hsaModalDistributionsYtd']);
        $scope.find('.healthequity-lastcalltoapi-modal').html(result['hsaModalDate']);
        $scope.find('.healthequity-error-modal').html(result['hsaModalError']);

        $scope.find('.balances').removeClass('loading');
        var $balanceMessage = $scope.find('#divHqyBalanceMessage');

        if (result['hasBalance']) {
            $balanceMessage.find($('.balance-current')).show();
            $balanceMessage.find($('.balance-na')).hide();
        } else {
            $balanceMessage.find($('.balance-current')).hide();
            $balanceMessage.find($('.balance-na')).show();
        }

    }

    var loadSso = function() {
        var promise = $.Deferred();
        $.get('/Sso/HealthEquitySsoStatus', {}, function(result) {
            if (result !== undefined && result !== null && result.IsHqySsoUp) {
                balanceModel.hsaModalError = '';
            } else {
                balanceModel.hsaModalError = "<p><span class='text-italic'>HealthEquity Single Sign-On currently unavailable. Please try again in a few minutes.<span></p>";
            }
            promise.resolve(balanceModel);
        });
        return promise;
    };
    var loadBalance = function() {
        var promise = $.Deferred();
        if (model.ShowBalance && balanceModel['hsaWidgetCashBalance'] === undefined) {
            $.get('/Sso/HealthEquityBalance', {}, function(result) {
                if (result !== undefined && result !== null && result.HsaCashBalance !== undefined && result.HsaCashBalance !== null) {
                    balanceModel.hsaWidgetCashBalance = result.HsaCashBalance;
                    balanceModel.hsaModalCashBalance = result.HsaCashBalance;
                    balanceModel.hsaModalInvestmentBalance = result.HsaInvestmentBalance;
                    balanceModel.hsaModalTotalBalance = result.HsaTotalBalance;
                    balanceModel.hsaModalContributionsYtd = result.HsaContributionsYtd;
                    balanceModel.hsaModalDistributionsYtd = result.HsaDistributionsYtd;

                    balanceModel.hasBalance = true;

                } else {
                    balanceModel.hsaWidgetCashBalance = 'Currently Unavailable';
                    balanceModel.hsaModalCashBalance = '-';
                    balanceModel.hsaModalInvestmentBalance = '-';
                    balanceModel.hsaModalTotalBalance = '-';
                    balanceModel.hsaModalContributionsYtd = '-';
                    balanceModel.hsaModalDistributionsYtd = '-';

                    balanceModel.hasBalance = false;
                }
                if (result !== undefined && result !== null && result.HsaDate !== undefined && result.HsaDate !== null) {
                    balanceModel.hsaWidgetDate = 'as of ' + result.HsaDate;
                    balanceModel.hsaModalDate = 'as of ' + result.HsaDate;
                } else {
                    balanceModel.hsaWidgetDate = '';
                    balanceModel.hsaModalDate = '';
                }
                promise.resolve(balanceModel);
            });
        } else if (model.ShowBalance && balanceModel['hsaWidgetCashBalance'] !== undefined) {
            promise.resolve(balanceModel);
        } else {
            promise.reject();
        }
        return promise;
    };
    var loadModel = function() {
        var promise = $.Deferred();
        var promiseBalance = loadBalance();
        var promiseSso = loadSso();
        $.when.apply($, [promiseBalance, promiseSso]).done(function() { promise.resolve(balanceModel) });
        return promise;
    };

    function openWidgetNoSsoOverlay() {

        $.blockUI();
        window._paq.push(['trackEvent', 'HealthEquity', 'WidgetClick']);

        if ($('#overlay-healthequity-nosso').length !== 0)
            $('#overlay-healthequity-nosso').remove();

        $('body').append('<div class="overlay" id="overlay-healthequity-nosso"></div>');

        var $overlay = $('#overlay-healthequity-nosso');
        $overlay.html('');

        $.get(window.VisitPay.Sso.HealthEquity.NoSsoUrl, function(html) {

            $.unblockUI();
            $overlay.html(html);
            window.location.href = '#' + $overlay.attr('id');

            $overlay.on('click', '#aMore', function() {
                $(this).toggleClass('expanded');
                $overlay.find('#divMore').toggle();
            });

            $overlay.on('click', '#btnHqySsoGetStarted', function () {
                window.VisitPayMobile.HealthEquityCommon.initNoSsoAgreement();
            });

        });

    };

    function openWidgetSsoOverlay() {

        if (model.Visible === true && model.SsoOutboundAgreed === true) {
            window._paq.push(['trackEvent', 'HealthEquity', 'WidgetClick']);
            $.blockUI();

            if ($('#overlay-healthequity-sso').length !== 0)
                $('#overlay-healthequity-sso').remove();

            $('body').append('<div class="overlay" id="overlay-healthequity-sso"></div>');

            var $overlay = $('#overlay-healthequity-sso');
            $overlay.html('');

            loadModel().done(function(result) {

                $.get(window.VisitPay.Sso.HealthEquity.SsoUrl, function(html) {

                    $.unblockUI();
                    $overlay.html(html);
                    window.location.href = '#' + $overlay.attr('id');

                    if (result['hsaWidgetCashBalance'] !== undefined) {
                        setBalanceDomProperites(result);
                    }
                });

            });
        }

    };

    $(document).ready(function () {

        if (model === undefined) {
            return;
        }


        $(document).on('click.dismiss-hqy-getstarted', '[data-click="dismiss-hqy-getstarted"]', function (e) {
            e.preventDefault();
            $.blockUI();
            $.post('/Sso/DismissHqyGettingStartedAlert', { }, function () {
                window.VisitPay.Common.RefreshAlerts().done(function() { $.unblockUI(); });
            });
        });

        $(document).on('click.hqy-getstarted', '[data-click="hqy-getstarted"]', openWidgetNoSsoOverlay );
        $scope.on('click', '#HealthEquitySsoButton', openWidgetSsoOverlay);

        if (model.Visible === true && model.SsoOutboundAgreed === true) {
            loadModel().done(function(result) {
                if (result['hsaWidgetCashBalance'] !== undefined) {
                    setBalanceDomProperites(result);
                }
            });
        }

    });

})(VisitPay.Common || {}, jQuery);
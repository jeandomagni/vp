﻿(function(apps, urls) {

    $(document).ready(function () {

        var $registrationStartOverlay = $("#overlay-register-step0");
        $registrationStartOverlay.show();
        $registrationStartOverlay.find("#btnStartRegistration, .close").on('click', function () {
            $registrationStartOverlay.hide();
        });

        // open legal in new tab
        $('#footer #aLegal').attr('target', '_blank');

        // open links from legal agreements in a new window
        $(document).on('overlay:shown.registration', function (e) {
            $(e.target).find('#section-esign a, #section-terms a').attr('target', '_blank');
        });

        // sample statement
        $(document).on('click.samplestatement', '#sampleStatement', function(e) {
            e.preventDefault();
            $('#modalSampleStatement').modal('show');
        });

        // payment method url override
        window.VisitPayMobile.PaymentMethodUrlBehaviorOverrides = {
            AfterSave: function () {
                window.history.go(-1);
            }
        };

        // app
        var registrationApp = new apps.RegistrationApp('#section-register', 'abstract');
        registrationApp.$on('account-ready', function() {

            $.get(urls.registerPaymentMethodsListPartial, function(html) {

                // payment methods
                var $paymentMethods = $('#section-payment-methods');
                $paymentMethods.html(html);

                var btns = [$paymentMethods.find('.btn-default')];
                $.each(btns, function() {
                    this.removeClass('btn-default').addClass('btn-accent btn-outline');
                    this.attr('tabindex', '-1');
                    this.text(this.text().replace('+', '').trim());
                });

                $paymentMethods.show();

                var paymentMethods = new window.VisitPayMobile.PaymentMethods(0, {
                    urlPaymentMethod: urls.registerPaymentMethod,
                    urlPaymentMethodBank: urls.registerPaymentMethodBank,
                    urlPaymentMethodCard: urls.registerPaymentMethodCard,
                    urlPaymentMethodsList: urls.registerPaymentMethodsList,
                    urlRemovePaymentMethod: urls.registerRemovePaymentMethod,
                    urlSavePaymentMethodBank: urls.registerSavePaymentMethodBank,
                    urlSavePaymentMethodCard: urls.registerSavePaymentMethodCard,
                    urlSetPrimary: urls.registerSetPrimary
                });
                paymentMethods.Init();

                // piwik
                $(document).on('click.registration', '#btnAddPaymentMethod', function () {
                    window._paq.push(['trackEvent', 'Registration', 'AddPaymentMethod']);
                });

            });
        });

        registrationApp.$on('registration-processing-complete', function(secondsToWait) {

            var summaryContents = $.Deferred();

            $.get(urls.registrationSummary, function (html) {
                summaryContents.resolve(html);
            });

            $.when(summaryContents).done(function(html) {

                var $overlayRegisterSummary = $("#overlay-register-summary");
                $overlayRegisterSummary.find('#overlay-register-summary-content').html(html);
                $overlayRegisterSummary.show();
                $.unblockUI();

                var retryInterval = 1000;
                var retryCodes = [204];
                $.get(urls.registrationSummaryHasStatement)
                    .retry({ times: secondsToWait, timeout: retryInterval, statusCodes: retryCodes }) // retry if NoContent returned
                    .always(function() {
                        $('#btnGoToApplication').css('visibility', 'visible');
                        $('#step4spinner').hide();
                    });

            });

        });

    });

})(window.VisitPay.Apps, window.VisitPay.Urls);
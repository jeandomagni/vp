﻿window.VisitPayMobile.Models.ManageHouseholdVm = (function (json) {

    var isViewManaged = ko.observable(window.VisitPay.Common.getQuery('managed') === '1');
    var self = this;

    self.PaymentMethod = new window.VisitPay.PaymentMethodListItemVm();
    ko.mapping.fromJS(json, {}, self);

    //
    self.IsViewManaged = ko.computed(function () {
        return ko.unwrap(isViewManaged) === true;
    });
    self.IsViewManaged.Set = function () {
        isViewManaged(true);
    }
    self.IsViewManaging = ko.computed(function () {
        return ko.unwrap(isViewManaged) === false;
    });
    self.IsViewManaging.Set = function () {
        isViewManaged(false);
    }

    //
    self.HasManagedGuarantors = ko.computed(function () {
        return self.ManagedGuarantors().length > 0;
    });
    self.HasManagingGuarantors = ko.computed(function () {
        return self.ManagingGuarantors().length > 0;
    });

    //
    self.ActiveConsolidationGuarantor = ko.observable();
    self.ActiveConsolidationGuarantor.Set = function (consolidationGuarantor) {

        self.ActiveConsolidationGuarantor(consolidationGuarantor);

    };
    
    //
    self.Reload = function () {

        var promise = $.Deferred();

        $.post('/mobile/Consolidate/GetHouseholdData', function (data) {

            var activeConsolidationGuarantor = self.ActiveConsolidationGuarantor();

            ko.mapping.fromJS(data, {}, self);

            if (activeConsolidationGuarantor != undefined) {
                var array = self.ManagedGuarantors().concat(self.ManagingGuarantors());

                var active = ko.utils.arrayFirst(array, function(guarantor) {
                    return (guarantor.ConsolidationGuarantorId() === activeConsolidationGuarantor.ConsolidationGuarantorId());
                });

                self.ActiveConsolidationGuarantor.Set(active);
            }

            promise.resolve();
        });

        return promise;
    };

    return self;

});

window.VisitPayMobile.Models.StepVm = (function() {

    var self = this;

    self.Step = ko.observable(1);
    self.Step.Is = function (step) {
        return ko.unwrap(self.Step) === step;
    };
    self.Step.Next = function () {
        var step = ko.unwrap(self.Step);
        self.Step(Math.min(step + 1, 4));
    };
    self.Step.Previous = function() {
        var step = ko.unwrap(self.Step);
        self.Step(Math.max(step - 1, 1));
    }

    return self;

});

window.VisitPayMobile.ManageHousehold = (function (json) {

    // global

    var model = new window.VisitPayMobile.Models.ManageHouseholdVm(json);
    ko.applyBindings(model, $('#section-household')[0]);
    ko.applyBindings(model, $('#overlay-consolidation-details')[0]);
    
    var paymentMethods = null;

    // shared

    function declineTerms(consolidationGuarantorId, requestType, backAfter) {

        $.blockUI();

        $.get('/Consolidate/DeclineConsolidation', function (cmsVersion) {

            $.unblockUI();
            window.VisitPay.Common.ModalGenericAlertMd(cmsVersion.ContentTitle, cmsVersion.ContentBody, 'Confirm Cancellation').then(function () {

                $.blockUI();

                var data = {
                    consolidationGuarantorId: consolidationGuarantorId,
                    requestType: requestType,
                    __RequestVerificationToken: $('#__AjaxAntiForgeryForm input[name=__RequestVerificationToken]').val()
                };

                $.post('/mobile/Consolidate/DeclineConsolidation', data, function () {
                    model.Reload().then(function () {
                        $.unblockUI();
                        if (backAfter)
                            window.history.back();
                    });
                });

            });
        });

    };

    function openPaymentMethods(e) {
        if (paymentMethods == null)
            paymentMethods = new window.VisitPayMobile.PaymentMethods(2);

        e.preventDefault();
        $.blockUI();

        paymentMethods.Init().done(function () {
            $.unblockUI();
            window.location.hash = 'overlay-list';
        });

    };

    // accept

    function acceptTerms(e) {

        e.preventDefault();

        var $form = $(this);
        var consolidationGuarantorId = $('#ConsolidationGuarantorId').val();
        var requestType = $('#RequestType').val();

        var $hipaa = $('#cbHipaa');
        var hipaaValid = $hipaa.length === 0 || $hipaa.valid();

        if (!hipaaValid) {
            return;
        }

        $.blockUI();

        $.get('/Consolidate/ConfirmAcceptance', function (cmsVersion) {

            $.unblockUI();
            window.VisitPay.Common.ModalGenericAlertMd(cmsVersion.ContentTitle, cmsVersion.ContentBody, 'Confirm Acceptance').then(function () {
                
                var data = {
                    consolidationGuarantorId: consolidationGuarantorId,
                    requestType: requestType,
                    __RequestVerificationToken: $('#__AjaxAntiForgeryForm input[name=__RequestVerificationToken]').val()
                };
                var achAuthorize = new window.VisitPay.AchAuthorization().promptConsolidation(model.PaymentMethod, consolidationGuarantorId);
                achAuthorize.then(function () {
                    $.blockUI();
                    $.post($form.attr('action'), data, function(response) {
                        if (response.Success) {
                            model.Reload().then(function() {
                                window.location.hash = '#overlay-consolidation-details';
                                $.unblockUI();
                            });
                        } else {
                            $.unblockUI();
                            window.VisitPay.Common.ModalGenericAlert('Request Expired', response.Message, 'OK').then(function() {
                                model.Reload().then(function() {
                                    window.location.hash = '#overlay-consolidation-details';
                                    $.unblockUI();
                                });
                            });
                        }
                    });
                }).fail(function () {
                    // don't allow continue - they didn't accept the ach authorization
                });

            });
        });

    };

    function openAcceptTerms(consolidationGuarantorId, requestType) {

        $.blockUI();

        if ($('#overlay-accept').length === 0)
            $('body').append('<div class="overlay" id="overlay-accept"></div>');

        var $overlay = $('#overlay-accept');
        $overlay.html('');

        $.get('/mobile/Consolidate/AcceptTerms/' + consolidationGuarantorId + '?requestType=' + requestType, function (html) {

            $.unblockUI();
            $overlay.html(html);

            if (requestType === 1) {

                var $section = $('#section-accept-terms-consolidation');
                var stepVm = new window.VisitPayMobile.Models.StepVm();
                ko.applyBindings(stepVm, $section[0]);

                $('#overlay-accept > a:eq(0)').removeAttr('onclick').off('click').on('click', function (e) {
                    e.preventDefault();
                    if (ko.unwrap(stepVm.Step) === 1) {
                        window.history.back();
                    } else {
                        stepVm.Step.Previous();
                    }
                });

                var $forms = $section.find('form#step2');
                $forms.parseValidation();
                $forms.off('submit').on('submit', function (e) {
                    e.preventDefault();
                    if (!$(this).valid()) {
                        return;
                    }
                    stepVm.Step.Next();
                });

                $section.find('form.agree').parseValidation();

            } else {

                $('#overlay-accept > a:eq(0)').attr('onclick', 'javascript: window.history.back();');

            }

            window.location.href = '#' + $overlay.attr('id');

            var $form = $('#frmConfirmAcceptance');
            $form.parseValidation();
            $form.off('submit').on('submit', acceptTerms);
            $form.find('#btnDecline').off('click').on('click', function () {
                declineTerms(consolidationGuarantorId, requestType, true);
            });

        });

    };

    function openAcceptTermsFp(requestType) {

        $.blockUI();

        if ($('#overlay-accept').length === 0)
            $('body').append('<div class="overlay" id="overlay-accept"></div>');

        var $overlay = $('#overlay-accept');
        $overlay.html('');

        $.get('/mobile/Consolidate/AcceptTermsFp/' + model.ActiveConsolidationGuarantor().ConsolidationGuarantorId() + '?requestType=' + requestType, function(html) {

            $.post('/Payment/PaymentMethodPrimary', function (data) {

                ko.mapping.fromJS(data, {}, model.PaymentMethod);
               
                $.unblockUI();
                $overlay.html(html);
                window.location.href = '#' + $overlay.attr('id');

                ko.cleanNode($overlay[0]);
                ko.applyBindings(model, $overlay[0]);
                $overlay.find('#btnPaymentMethods').off('click').on('click', openPaymentMethods);

                var $form = $('#frmConfirmAcceptance');
                window.VisitPay.Common.ResetUnobtrusiveValidation($form);
                $form.off('submit').on('submit', acceptTerms);
                $form.find('#btnDecline').off('click').on('click', function () {
                    declineTerms(model.ActiveConsolidationGuarantor().ConsolidationGuarantorId(), requestType, true);
                });

                $(window).off('paymentMethodPrimarySet').on('paymentMethodPrimarySet', function (e, paymentMethod) {
                    if (paymentMethod === undefined || paymentMethod === null) {
                        paymentMethod = new window.VisitPay.PaymentMethodListItemVm();
                    }

                    ko.mapping.fromJS(ko.mapping.toJS(paymentMethod), {}, model.PaymentMethod);
                    window.location.hash = '#overlay-accept';

                });

            });

        });

    }

    // deconsolidate fp

    function deconsolidateFp(e) {

        e.preventDefault();

        var $form = $(this);
        var consolidationGuarantorId = $form.find('#ConsolidationGuarantorId').val();

        var data = {
            consolidationGuarantorId: consolidationGuarantorId,
            requestType: $form.find('#RequestType').val(),
            __RequestVerificationToken: $('#__AjaxAntiForgeryForm input[name=__RequestVerificationToken]').val()
        };

        var achAuthorize = new window.VisitPay.AchAuthorization().promptConsolidation(model.PaymentMethod, consolidationGuarantorId);
        achAuthorize.then(function() {
            $.blockUI();
            $.post($form.attr('action'), data, function() {
                model.Reload().then(function() {
                    window.location.hash = '#overlay-consolidation-details';
                    $.unblockUI();
                });
            });
        }).fail(function() {
            // don't allow continue - they didn't accept the ach authorization
        });

    };

    function openDeconsolidateFp(requestType) {

        $.blockUI();

        if ($('#overlay-deconsolidate').length === 0)
            $('body').append('<div class="overlay" id="overlay-deconsolidate"></div>');

        var $overlay = $('#overlay-deconsolidate');
        $overlay.html('');

        $.get('/mobile/Consolidate/DeconsolidateFp/' + model.ActiveConsolidationGuarantor().ConsolidationGuarantorId() + '?requestType=' + requestType, function (html) {

            $.post('/Payment/PaymentMethodPrimary', function (data) {

                ko.mapping.fromJS(data, {}, model.PaymentMethod);

                $.unblockUI();
                $overlay.html(html);
                window.location.href = '#' + $overlay.attr('id');

                ko.cleanNode($overlay[0]);
                ko.applyBindings(model, $overlay[0]);
                $overlay.find('#btnPaymentMethods').off('click').on('click', openPaymentMethods);

                var $form = $('#frmDeconsolidateFp');
                window.VisitPay.Common.ResetUnobtrusiveValidation($form);
                $form.off('submit').on('submit', deconsolidateFp);
                
                $(window).off('paymentMethodPrimarySet').on('paymentMethodPrimarySet', function (e, paymentMethod) {
                    if (paymentMethod === undefined || paymentMethod === null) {
                        paymentMethod = new window.VisitPay.PaymentMethodListItemVm();
                    }

                    ko.mapping.fromJS(ko.mapping.toJS(paymentMethod), {}, model.PaymentMethod);
                    window.location.hash = '#overlay-deconsolidate';

                });

            });

        });

    }

    // details

    function actionChange() {

        var $select = $(this);

        if ($select.val() === '')
            return;

        var context = ko.contextFor($select[0]).$data;
        var requestType = context.IsManagingUser() ? 2 : 1;
        var consolidationGuarantorId = context.ConsolidationGuarantorId();
        var value = parseInt($select.val());

        if (value === 1) { // cancel pending

            $.blockUI();

            $.get('/Consolidate/CancelRequest', function (cmsVersion) {

                $.unblockUI();
                window.VisitPay.Common.ModalGenericAlertMd(cmsVersion.ContentTitle, cmsVersion.ContentBody, 'Confirm Cancellation').then(function () {

                    $.blockUI();

                    var data = {
                        consolidationGuarantorId: consolidationGuarantorId,
                        requestType: requestType,
                        __RequestVerificationToken: $('#__AjaxAntiForgeryForm input[name=__RequestVerificationToken]').val()
                    };

                    $.post('/Consolidate/CancelRequest', data, function () {
                        model.Reload().then(function () {
                            $.unblockUI();
                        });
                    });

                });
            });

        } else if (value === 2) { // accept terms (no fp)

            openAcceptTerms(consolidationGuarantorId, requestType);

        } else if (value === 3) { // decline

            declineTerms(consolidationGuarantorId, requestType, false);

        } else if (value === 4) { // accept terms (fp)

            openAcceptTermsFp(requestType);

        } else if (value === 5) { // deconsolidate (no fp)

            $.get('/Consolidate/CancelConsolidation/' + consolidationGuarantorId, { requestType: requestType }, function (cmsVersion) {

                $.unblockUI();
                window.VisitPay.Common.ModalGenericAlertMd(cmsVersion.ContentTitle, cmsVersion.ContentBody, 'Confirm Cancellation').then(function () {

                    $.blockUI();

                    var data = {
                        consolidationGuarantorId: consolidationGuarantorId,
                        requestType: requestType,
                        __RequestVerificationToken: $('#__AjaxAntiForgeryForm input[name=__RequestVerificationToken]').val()
                    };

                    $.post('/Consolidate/CancelRequest', data, function () {
                        model.Reload().then(function () {
                            $.unblockUI();
                        });
                    });

                });
            });

        } else if (value === 6) { // deconsolidate (fp)

            openDeconsolidateFp(requestType);

        }


        $select.val('');

    };

    // match request

    function matchSubmit(e) {

        e.preventDefault();
        var $form = $(this);

        if (!$form.valid())
            return;

        $.blockUI();

        $.post($form.attr('action'), $form.serialize(), function (response) {

            $.unblockUI();

            if (response.Status === 0) { // no match found
                window.VisitPay.Common.ModalGenericAlert(response.Title, response.Message, 'Ok');
                $form[0].reset();
            } else if (response.Status === 1) { // not eligible
                window.VisitPay.Common.ModalGenericAlertMd(response.Title, response.Message, 'Ok');
                $form[0].reset();
            } else if (response.Status === 2) { // found
                window.VisitPay.Common.ModalGenericAlertMd(response.Title, response.Message, 'Ok').then(function () {
                    $.blockUI();
                    model.Reload().then(function () {
                        $.unblockUI();
                        window.history.back();
                    });
                });
            }

        });

    };

    // main

    function openConsolidationDetails(e) {

        var context = ko.contextFor($(this).find('a')[0]).$data;
        model.ActiveConsolidationGuarantor.Set(context);

        var $overlay = $('#overlay-consolidation-details');
        window.location.href = '#' + $overlay.attr('id');

    };

    function openRequest(url) {

        $.blockUI();

        if ($('#overlay-request').length === 0) {
            $('body').append('<div class="overlay" id="overlay-request"></div>');
        }

        var $overlay = $('#overlay-request');
        $overlay.html('');

        $.get(url, function (html) {

            $.unblockUI();
            $overlay.html(html);

            if ($('#section-request-managed').length > 0) {

                var $section = $('#section-request-managed');
                var stepVm = new window.VisitPayMobile.Models.StepVm();
                ko.applyBindings(stepVm, $section[0]);

                $('#overlay-request > a:eq(0)').removeAttr('onclick').off('click').on('click', function(e) {
                    e.preventDefault();
                    if (ko.unwrap(stepVm.Step) === 1) {
                        window.history.back();
                    } else {
                        stepVm.Step.Previous();
                    }
                });

                var $forms = $section.find('form#step2, form#step3');
                $forms.parseValidation();
                $forms.off('submit').on('submit', function(e) {
                    e.preventDefault();
                    if (!$(this).valid()) {
                        return;
                    }
                    stepVm.Step.Next();
                });

            } else {

                $('#overlay-request > a:eq(0)').attr('onclick', 'javascript: window.history.back();');

            }

            window.location.href = '#' + $overlay.attr('id');

            var $formMatch = $('#frmConsolidateMatch');
            window.VisitPay.Common.ResetUnobtrusiveValidation($formMatch);
            $formMatch.off('submit').on('submit', matchSubmit);
            $formMatch.find('#BirthYear').NumericOnly();

        });

    };

    $(document).ready(function () {

        $('body').on('click', '#section-household .grid .mobile-link', openConsolidationDetails);

        $('body').on('change', '#section-consolidation-details select[name="Actions"]', actionChange);

        $('body').on('click', '#btnRequestManaged', function (e) {
            e.preventDefault();
            openRequest('/mobile/Consolidate/RequestToBeManaged');
        });

        $('body').on('click', '#btnRequestManaging', function (e) {
            e.preventDefault();
            openRequest('/mobile/Consolidate/RequestToBeManaging');
        });

    });

});
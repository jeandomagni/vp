﻿namespace Ivh.Web.Patient.Areas.Mobile.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using System.Web.Mvc;
    using System.Web.SessionState;
    using Application.Content.Common.Interfaces;
    using Application.Core.Common.Interfaces;
    using Application.FinanceManagement.Common.Dtos;
    using Application.FinanceManagement.Common.Interfaces;
    using Attributes;
    using AutoMapper;
    using Common.Base.Utilities.Extensions;
    using Common.VisitPay.Constants;
    using Common.VisitPay.Enums;
    using Common.VisitPay.Strings;
    using Common.Web.Controllers;
    using Common.Web.HtmlHelpers;
    using Common.Web.Interfaces;
    using Common.Web.Mapping;
    using Common.Web.Models.Payment;
    using Domain.Logging.Interfaces;
    using Domain.Settings.Interfaces;
    using Models;
    using Services;
    using SessionFacade;

    [GuarantorAuthorize(Roles = VisitPayRoleStrings.System.Patient + "," + VisitPayRoleStrings.Admin.Administrator)]
    public class PaymentController : BasePaymentController
    {
        private readonly Lazy<GuarantorFilterService> _guarantorFilterService;
        protected readonly Lazy<IWebPatientSessionFacade> WebPatientSessionFacade;

        public PaymentController(
            Lazy<IBaseControllerService> baseControllerService,
            Lazy<IContentApplicationService> contentApplicationService,
            Lazy<IFinancePlanApplicationService> financePlanApplicationService,
            Lazy<IGuarantorApplicationService> guarantorApplicationService,
            Lazy<IPaymentActionApplicationService> paymentActionApplicationService,
            Lazy<IPaymentConfigurationService> paymentConfigurationService,
            Lazy<IPaymentDetailApplicationService> paymentDetailApplicationService,
            Lazy<IPaymentOptionApplicationService> paymentOptionApplicationService,
            Lazy<IPaymentMethodsApplicationService> paymentMethodsApplicationService,
            Lazy<IPaymentMethodAccountTypeApplicationService> paymentMethodAccountTypeApplicationService,
            Lazy<IPaymentMethodProviderTypeApplicationService> paymentMethodProviderTypeApplicationService,
            Lazy<IPaymentReversalApplicationService> paymentReversalApplicationService,
            Lazy<IPaymentSubmissionApplicationService> paymentSubmissionApplicationService,
            Lazy<IVisitApplicationService> visitApplicationService,
            Lazy<IVisitPayUserApplicationService> visitPayUserApplicationService,
            Lazy<GuarantorFilterService> guarantorFilterService,
            Lazy<IWebPatientSessionFacade> webPatientSessionFacade)
            : base(
                baseControllerService,
                contentApplicationService,
                financePlanApplicationService,
                guarantorApplicationService,
                paymentActionApplicationService,
                paymentConfigurationService,
                paymentDetailApplicationService,
                paymentMethodsApplicationService,
                paymentMethodAccountTypeApplicationService,
                paymentMethodProviderTypeApplicationService,
                paymentOptionApplicationService,
                paymentReversalApplicationService,
                paymentSubmissionApplicationService,
                visitApplicationService,
                visitPayUserApplicationService)
        {
            this._guarantorFilterService = guarantorFilterService;
            this.WebPatientSessionFacade = webPatientSessionFacade;
        }

        public ActionResult MyPayments()
        {
            return this.View();
        }

        #region arrange payment

        [HttpGet]
        public ActionResult MakePayment()
        {
            return this.RedirectToAction("ArrangePayment");
        }

        [HttpGet]
        [Common.Web.Attributes.SessionState(SessionStateBehavior.Required)]
        public async Task<ActionResult> ArrangePayment(PaymentOptionEnum? paymentOption, Guid? v)
        {
            if (v.HasValue) // filteredVpGuarantorId
            {
                this._guarantorFilterService.Value.SetGuarantor(v.Value);
            }

            if (paymentOption.IsInCategory(PaymentOptionEnumCategory.Household))
            {
                int managingVpGuarantorId = this.GetVpGuarantorId(this.CurrentUserId);
                this._guarantorFilterService.Value.SetGuarantor(managingVpGuarantorId);
            }

            int vpGuarantorId = this._guarantorFilterService.Value.VpGuarantorId;

            ArrangePaymentViewModel model = await this.GetArrangePaymentModelAsync(paymentOption, vpGuarantorId, this.CurrentUserId);
            if (paymentOption.HasValue || model.PaymentMenu.PaymentMenuItems.Count == 1)
            {
                List<PaymentMenuItemViewModel> allPaymentMenuItems = new List<PaymentMenuItemViewModel>();
                allPaymentMenuItems.AddRange(model.PaymentMenu.PaymentMenuItems);
                allPaymentMenuItems.AddRange(model.PaymentMenu.PaymentMenuItems.SelectMany(x => x.PaymentMenuItems));
                allPaymentMenuItems = allPaymentMenuItems.Where(x => x.IsEnabled && !x.PaymentMenuItems.Any()).ToList();
                
                //Handle FP options
                if (paymentOption == PaymentOptionEnum.FinancePlan)
                {
                    IList<PaymentMenuItemViewModel> financePlanOptions = allPaymentMenuItems.Where(i => i.PaymentOptionId == PaymentOptionEnum.FinancePlan).ToList();
                    if (financePlanOptions.Any())
                    {
                        IList<PaymentOptionViewModel> financePlanPaymentOptions = model.PaymentOptions.Where(x => x.PaymentOptionId == PaymentOptionEnum.FinancePlan).ToList();
                        if (financePlanPaymentOptions.Any())
                        {
                            model.PaymentMenu.PaymentMenuItems = financePlanOptions;
                            model.PaymentOptions = financePlanPaymentOptions.ToList();

                            return this.View("ArrangePaymentFinancePlan", this.MapArrangePaymentMobile(model, true));
                        }
                    }
                }

                //Handle non-FP options. Include fallback FP logic for safety
                PaymentMenuItemViewModel selectedItem = allPaymentMenuItems.FirstOrDefault(x => x.PaymentOptionId == paymentOption) ?? allPaymentMenuItems.FirstOrDefault();
                if (selectedItem != null)
                {
                    // show just this one
                    PaymentOptionViewModel activePaymentOption = model.PaymentOptions.FirstOrDefault(x => x.PaymentOptionId == selectedItem.PaymentOptionId);
                    if (activePaymentOption != null)
                    {
                        model.PaymentMenu.PaymentMenuItems = selectedItem.ToListOfOne();
                        model.PaymentOptions = activePaymentOption.ToListOfOne();

                        string view = activePaymentOption.PaymentOptionId == PaymentOptionEnum.FinancePlan ? "ArrangePaymentFinancePlan" : "ArrangePayment";

                        return this.View(view, this.MapArrangePaymentMobile(model, true));
                    }
                }
            }

            // return the menu
            return this.View("ArrangePaymentMenu", this.MapArrangePaymentMobile(model, false));
        }

        [NonAction]
        [Common.Web.Attributes.SessionState(SessionStateBehavior.Required)]
        protected override async Task<ArrangePaymentViewModel> GetArrangePaymentModelAsync(PaymentOptionEnum? selectedPaymentOption, int vpGuarantorId, int currentVisitPayUserId)
        {
            ArrangePaymentViewModel model = await base.GetArrangePaymentModelAsync(selectedPaymentOption, vpGuarantorId, currentVisitPayUserId);

            // these actions not mobile specific
            model.FinancePlanSubmitUrl = this.Url.Action("FinancePlanSubmit", "FinancePlan", new { area = "" });
            model.FinancePlanValidateUrl = this.Url.Action("FinancePlanValidate", "FinancePlan", new { area = "" });
            model.PaymentSubmitUrl = this.Url.Action("ArrangePaymentSubmit", "Payment", new { area = ""});
            model.PaymentValidateUrl = this.Url.Action("ArrangePaymentValidate", "Payment", new { area = "" });
            model.HomeUrl = "/mobile/";
            model.HomeButtonText = await this.ContentApplicationService.Value.GetTextRegionAsync(TextRegionConstants.ButtonHome);
            
            this.WebPatientSessionFacade.Value.StackedAndCombinedFinancePlanOptionsPresented = model.HasCombinedOption && model.HasDefaultOption;

            return model;
        }

        private ArrangePaymentMobileViewModel MapArrangePaymentMobile(ArrangePaymentViewModel viewModel, bool withPaymentMethod)
        {
            ArrangePaymentMobileViewModel model = Mapper.Map<ArrangePaymentMobileViewModel>(viewModel);
            model.ReturnUrl = this.Request.Url != null && this.Request.UrlReferrer != null && this.Request.Url.Host == this.Request.UrlReferrer.Host ? this.Request.UrlReferrer.ToString() : this.Url.Action("ArrangePayment", "Payment", new { area = "Mobile" });
            model.ClientNow = MappingHelper.MapToClientDateTime(DateTime.UtcNow) ?? DateTime.UtcNow;

            if (!withPaymentMethod || !model.PaymentOptions.Any())
            {
                return model;
            }

            if (model.PaymentOptions.First().PaymentOptionId == PaymentOptionEnum.Resubmit)
            {
                // no payment method should be selected on resubmit
                model.PaymentMethod = new PaymentMethodListItemViewModel();
                return model;
            }

            int currentUserVpGuarantorId = this.GetVpGuarantorId(this.CurrentUserId);

            model.PaymentMethod = Mapper.Map<PaymentMethodListItemViewModel>(this.PaymentMethodsApplicationService.Value.GetPrimaryPaymentMethod(currentUserVpGuarantorId));
            
            return model;
        }

        #endregion

        #region payment methods

        [HttpPost]
        [ValidateAntiForgeryToken]
        public string GeneratePanUrl()
        {
            return SecurePanAuthorization.GenerateUrl(this.ApplicationSettingsService.Value);
        }

        [AllowAnonymous] // for access during registration
        [HttpGet]
        public PartialViewResult PaymentMethodsListPartial()
        {
            return this.PartialView("_PaymentMethodList");
        }

        [HttpGet]
        public ActionResult PaymentMethods()
        {
            int vpGuarantorId = this.GetVpGuarantorId(this.CurrentUserId);

            IList<PaymentMethodDto> paymentMethodsDto = this.PaymentMethodsApplicationService.Value.GetPaymentMethods(vpGuarantorId, this.CurrentUserId);

            PaymentMethodsViewModel model = new PaymentMethodsViewModel
            {
                AllAccounts = Mapper.Map<IList<PaymentMethodListItemViewModel>>(paymentMethodsDto)
            };

            return this.View(model);
        }
        
        #endregion

        #region discounts (from homepage)

        [HttpGet]
        public PartialViewResult DiscountsPartial()
        {
            return this.PartialView("_Discounts");
        }

        #endregion
    }
}
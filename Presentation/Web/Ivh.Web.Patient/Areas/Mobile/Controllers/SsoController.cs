﻿namespace Ivh.Web.Patient.Areas.Mobile.Controllers
{
    using System;
    using System.Threading.Tasks;
    using System.Web;
    using System.Web.Mvc;
    using System.Web.SessionState;
    using Application.Content.Common.Interfaces;
    using Application.Core.Common.Interfaces;
    using Attributes;
    using Common.Base.Enums;
    using Common.Cache;
    using Common.VisitPay.Enums;
    using Common.VisitPay.Strings;
    using Common.Web.Interfaces;
    using Common.Web.Models.Sso;
    using Domain.Logging.Interfaces;
    using Microsoft.AspNet.Identity;
    using Patient.Controllers.Base;
    using Patient.Models.Sso;

    [GuarantorAuthorize(Roles = VisitPayRoleStrings.System.Patient + "," + VisitPayRoleStrings.Admin.Administrator)]
    public class SsoController : BasePatientSsoController
    {
        public SsoController(Lazy<IBaseControllerService> baseControllerService,
            Lazy<ISsoApplicationService> ssoApplicationService,
            Lazy<IContentApplicationService> contentApplicationService,
            Lazy<IGuarantorApplicationService> guarantorApplicationService,
            Lazy<IDistributedCache> distributedCache
        )
            : base(
                baseControllerService,
                ssoApplicationService,
                contentApplicationService,
                guarantorApplicationService,
                distributedCache)
        {

        }

        [HttpGet]
        public PartialViewResult HealthEquityNoSso()
        {
            HealthEquityWidgetViewModel model = this.HealthEquityWidgetViewModel();
            return this.PartialView(model);
        }

        [HttpGet]
        public PartialViewResult HealthEquitySso()
        {
            HealthEquityWidgetViewModel model = this.HealthEquityWidgetViewModel();
            return this.PartialView(model);
        }

        [HttpGet]
        public PartialViewResult HealthEquityPreAgreementSso()
        {
            HealthEquityWidgetViewModel model = this.HealthEquityWidgetViewModel();
            return this.PartialView(model);
        }

        [HttpGet]
        public PartialViewResult HealthEquityAgreementSso()
        {
            HealthEquityWidgetViewModel model = this.HealthEquityWidgetViewModel();
            return this.PartialView(model);
        }

        [HttpGet]
        public PartialViewResult HealthEquitySsoSetupFailed()
        {
            HealthEquityWidgetViewModel model = this.HealthEquityWidgetViewModel();
            
            Task<PartialViewResult> terms = this.Terms(SsoProviderEnum.HealthEquityOutbound, false);

            return this.PartialView(model);
        }

        [HttpGet]
        public PartialViewResult HealthEquitySsoSetupSuccess()
        {
            HttpContext context = System.Web.HttpContext.Current;
            //VPNG-18889 SsoVisitPayUserSetting.SsoStatus has to be set before fetching it (previously ssoStatusEnum was null)
            this.AcceptSso(context, SsoProviderEnum.HealthEquityOutbound, this.CurrentUserId);
            SsoStatusEnum? ssoStatusEnum = this.SsoApplicationService.Value.GetUserSsoStatus(SsoProviderEnum.HealthEquityOutbound, this.CurrentUserId);
            HealthEquityWidgetViewModel model = this.HealthEquityWidgetViewModel();
            return this.PartialView(ssoStatusEnum.HasValue && ssoStatusEnum.Value == SsoStatusEnum.Accepted ? "HealthEquitySsoSetupSuccess" : "HealthEquitySsoSetupInvalid", model);
        }

        [HttpGet]
        [GuarantorAuthorize(Roles = VisitPayRoleStrings.System.Patient)]
        public ActionResult SamlResponse(SamlResponseTargetEnum target)
        {
            SamlResponseViewModel samlResponseViewModel = this.CreateSamlResponse(target);
            if (samlResponseViewModel.IsError)
            {
                return this.View("SamlResponseFailed", samlResponseViewModel);
            }
            return this.View(samlResponseViewModel);
        }

        [HttpGet]
        public new PartialViewResult Ignore(SsoProviderEnum ssoProvider, bool isIgnored)
        {
            SsoProviderWithUserSettingsViewModel model = base.IgnoreInternal(ssoProvider, isIgnored);

            return this.PartialView("HealthEquityDontShowWidgetSso");
        }
    }
}
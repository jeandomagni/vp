﻿namespace Ivh.Web.Patient.Areas.Mobile.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web.Mvc;
    using Application.Core.Common.Interfaces;
    using Application.FinanceManagement.Common.Interfaces;
    using Attributes;
    using AutoMapper;
    using Common.Base.Utilities.Extensions;
    using Common.Web.Cookies;
    using Common.Web.Models;
    using Common.Web.Models.Account;
    using Common.Web.Interfaces;
    using Microsoft.AspNet.Identity;
    using Models;
    using Patient.Controllers.Base;
    using Patient.Models;
    using Services;
    using System.Threading.Tasks;
    using Application.Content.Common.Dtos;
    using Application.Content.Common.Interfaces;
    using Application.Email.Common.Interfaces;
    using Common.Cache;
    using Common.Web.Filters;
    using Patient.Models.Account;
    using SessionFacade;
    using Application.User.Common.Dtos;
    using Common.EventJournal;
    using Common.VisitPay.Constants;
    using Common.VisitPay.Enums;
    using Common.VisitPay.Strings;
    using Common.Web.Models.Guarantor;
    using Common.Web.Utilities;

    [GuarantorAuthorize(Roles = VisitPayRoleStrings.System.Patient + "," + VisitPayRoleStrings.Admin.Administrator)]
    public class AccountController : BaseAccountController
    {
        private readonly Lazy<ISecurityQuestionApplicationService> _securityQuestionApplicationService;

        public AccountController(
            Lazy<IBaseControllerService> baseControllerService,
            Lazy<IAlertingApplicationService> alertingApplicationService,
            Lazy<IContentApplicationService> contentApplicationService,
            Lazy<IFinancePlanApplicationService> financePlanApplicationService,
            Lazy<GuarantorFilterService> guarantorFilterService,
            Lazy<ISecurityQuestionApplicationService> securityQuestionApplicationService,
            Lazy<ISsoApplicationService> ssoApplicationService,
            Lazy<IStatementApplicationService> statementApplicationService,
            Lazy<IVisitApplicationService> visitApplicationService,
            Lazy<IVisitPayUserApplicationService> visitPayUserApplicationService,
            Lazy<ICommunicationApplicationService> communicationApplicationService,
            Lazy<IWebCookieFacade> webCookieFacade,
            Lazy<IDistributedCache> cache,
            Lazy<IWebPatientSessionFacade> webPatientSessionFacade,
            Lazy<ILocalizationCookieFacade> localizationCookieFacade)
            : base(
                baseControllerService,
                alertingApplicationService,
                financePlanApplicationService,
                statementApplicationService,
                visitApplicationService,
                visitPayUserApplicationService,
                guarantorFilterService,
                webCookieFacade,
                communicationApplicationService,
                contentApplicationService,
                cache,
                webPatientSessionFacade,
                ssoApplicationService,
                localizationCookieFacade)
        {
            this._securityQuestionApplicationService = securityQuestionApplicationService;
        }
        
        #region my visits

        [HttpGet]
        public ActionResult MyVisits()
        {
            return this.RedirectToAction("MyPayments", "Payment");
        }

        #endregion
        
        #region payment summary

        [HttpGet]
        [RequireFeature(VisitPayFeatureEnum.PaymentSummary)]
        public ActionResult PaymentSummary()
        {
            return this.View(new PaymentSummarySearchFilterViewModel());
        }
        
        #endregion
        
        #region account settings

        public ActionResult AccountSettings()
        {
            return this.View();
        }

        #endregion

        #region Phone and Text Settings

        [HttpGet]
        public async Task<ActionResult> PhoneTextSettings()
        {
            PhoneTextInformationViewModel model = await this.CreatePhoneTextInformationViewModelAsync();
            return this.View(model);
        }

        [HttpGet]
        public ActionResult PhoneEdit(SmsPhoneTypeEnum phoneType)
        {
            return this.View("UserPhoneEdit", this.CreateEditPhoneViewModel(phoneType));
        }

        [HttpGet]
        public ActionResult SmsAcknowledgement(SmsPhoneTypeEnum phoneType)
        {
            List<CommunicationSmsBlockedNumberViewModel> blockedNumbers = this.IsNumberBlocked(phoneType).ToList();
            if (!blockedNumbers.Any())
            {
                this.MetricsProvider.Value.Increment(Metrics.Increment.Communication.Sms.ShowTerms);
                return this.View(this.CreateActivateTextMessagingViewModel(phoneType));
            }
            return this.RedirectToAction("PhoneTextSettings");
        }

        [HttpGet]
        public ActionResult SmsValidateToken(SmsPhoneTypeEnum phoneType)
        {
            return this.View(this.CreateActivateTextMessagingViewModel(phoneType));
        }

        #endregion

        #region Personal Information
        public ActionResult UserProfile()
        {
            return this.View();
        }

        public ActionResult UserPersonalInformation()
        {
            VisitPayUserDto visitPayUserDto = this.VisitPayUserApplicationService.Value.FindById(this.User.Identity.GetUserId());
            ProfileDisplayViewModel model = new ProfileDisplayViewModel
            {
                AddressStreet1 = visitPayUserDto.AddressStreet1,
                AddressStreet2 = visitPayUserDto.AddressStreet2,
                City = visitPayUserDto.City,
                State = visitPayUserDto.State,
                Zip = visitPayUserDto.Zip,
                MailingAddressStreet1 = visitPayUserDto.MailingAddressStreet1,
                MailingAddressStreet2 = visitPayUserDto.MailingAddressStreet2,
                MailingCity = visitPayUserDto.MailingCity,
                MailingState = visitPayUserDto.MailingState,
                MailingZip = visitPayUserDto.MailingZip,
                Email = visitPayUserDto.Email,
                Name = visitPayUserDto.DisplayFullName,
                PhoneNumber = visitPayUserDto.PhoneNumber,
                PhoneNumberType = visitPayUserDto.PhoneNumberType,
                PhoneNumberSecondary = visitPayUserDto.PhoneNumberSecondary,
                PhoneNumberSecondaryType = visitPayUserDto.PhoneNumberSecondaryType,
                UserName = visitPayUserDto.UserName
            };

            return this.PartialView("_UserPersonalInformation", model);
        }

        #region Profile Edit

        public ActionResult UserProfileEdit(int section)
        {
            if (Enum.IsDefined(typeof(UserProfileEditSectionsEnum), section))
            {
                UserProfileEditSectionsEnum editSectionEnum = (UserProfileEditSectionsEnum)section;

                VisitPayUserDto visitPayUserDto = this.VisitPayUserApplicationService.Value.FindById(this.User.Identity.GetUserId());

                ProfileEditMobileViewModel model = new ProfileEditMobileViewModel
                {
                    ProfileEditAddress =
                    {
                        AddressStreet1 = visitPayUserDto.AddressStreet1,
                        AddressStreet2 = visitPayUserDto.AddressStreet2,
                        City = visitPayUserDto.City,
                        State = visitPayUserDto.State,
                        Zip = visitPayUserDto.Zip
                    },
                    ProfileEditEmail =
                    {
                        Email = visitPayUserDto.Email,
                        EmailOriginal = visitPayUserDto.Email
                    },
                    ProfileEditName =
                    {
                        FirstName = visitPayUserDto.FirstName,
                        LastName = visitPayUserDto.LastName,
                        MiddleName = visitPayUserDto.MiddleName
                    },
                    ProfileEditUserName =
                    {
                        UserName = visitPayUserDto.UserName,
                        UserNameOriginal = visitPayUserDto.UserName
                    },
                    ProfileEditPhoneNumber =
                    {
                        PhoneNumber = visitPayUserDto.PhoneNumber,
                        PhoneNumberSecondary = visitPayUserDto.PhoneNumberSecondary,
                        PhoneNumberSecondaryType = visitPayUserDto.PhoneNumberSecondaryType,
                        PhoneNumberType = visitPayUserDto.PhoneNumberType
                    }
                };

                this.ViewBag.PageTitle = editSectionEnum.GetDescription();

                return this.View(model);
            }
            else
                return this.RedirectToAction("UserProfile");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult UpdateName(ProfileEditNameViewModel model)
        {
            if (!this.ModelState.IsValid)
                return this.Json(new ResultMessage(false, this.GenericErrorMessage));

            VisitPayUserDto visitPayUserDto = this.VisitPayUserApplicationService.Value.FindById(this.User.Identity.GetUserId());
            if (visitPayUserDto != null)
            {
                visitPayUserDto.FirstName = model.FirstName;
                visitPayUserDto.LastName = model.LastName;
                visitPayUserDto.MiddleName = model.MiddleName;
            }

            return this.UserProfileEdit(visitPayUserDto);
        }

        

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult UpdateEmail(ProfileEditEmailViewModel model)
        {
            if (!this.ModelState.IsValid)
                return this.Json(new ResultMessage(false, this.GenericErrorMessage));

            VisitPayUserDto visitPayUserDto = this.VisitPayUserApplicationService.Value.FindById(this.User.Identity.GetUserId());
            if (visitPayUserDto != null)
            {
                visitPayUserDto.Email = model.Email;
            }

            return this.UserProfileEdit(visitPayUserDto);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult UpdatePhysicalAddress(ProfileEditAddressViewModel model)
        {
            if (!this.ModelState.IsValid)
                return this.Json(new ResultMessage(false, this.GenericErrorMessage));

            VisitPayUserDto visitPayUserDto = this.VisitPayUserApplicationService.Value.FindById(this.User.Identity.GetUserId());
            if (visitPayUserDto != null)
            {
                visitPayUserDto.AddressStreet1 = model.AddressStreet1;
                visitPayUserDto.AddressStreet2 = model.AddressStreet2;
                visitPayUserDto.City = model.City;
                visitPayUserDto.State = model.State;
                visitPayUserDto.Zip = model.Zip;
            }

            return this.UserProfileEdit(visitPayUserDto);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult UpdatePhoneNumber(ProfileEditPhoneNumberViewModel model)
        {
            if (!this.ModelState.IsValid)
                return this.Json(new ResultMessage(false, this.GenericErrorMessage));

            VisitPayUserDto visitPayUserDto = this.VisitPayUserApplicationService.Value.FindById(this.User.Identity.GetUserId());
            if (visitPayUserDto != null)
            {
                visitPayUserDto.PhoneNumber = model.PhoneNumber;
                visitPayUserDto.PhoneNumberSecondary = model.PhoneNumberSecondary;
                visitPayUserDto.PhoneNumberSecondaryType = model.PhoneNumberSecondaryType;
                visitPayUserDto.PhoneNumberType = model.PhoneNumberType;
            }

            return this.UserProfileEdit(visitPayUserDto);
        }

        #endregion

        public ActionResult EditPassword()
        {
            return this.View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<JsonResult> EditPassword(PasswordEditViewModel model)
        {
            if (!this.ModelState.IsValid)
                return this.Json(new ResultMessage(false, this.GenericErrorMessage));

            if (!this.VisitPayUserApplicationService.Value.IsPasswordStrongEnough(model.NewPassword).Result.Succeeded)
            {
                string invalidPasswordMessage = LocalizationHelper.GetLocalizedString(TextRegionConstants.PasswordInvalid);
                return this.Json(new ResultMessage(false, invalidPasswordMessage));
            }

            IdentityResult identityResult = await this.VisitPayUserApplicationService.Value.ChangePasswordAsync(this.User.Identity.GetUserId(),
                model.OldPassword,
                model.NewPassword,
                this.ClientDto.GuarantorPasswordExpLimitInDays,
                this.ClientDto.GuarantorUnrepeatablePasswordCount).ConfigureAwait(true);

            string passwordUpdatedMessage = LocalizationHelper.GetLocalizedString(TextRegionConstants.PasswordUpdated);
            return this.Json(identityResult.Succeeded ? new ResultMessage(true, passwordUpdatedMessage) : new ResultMessage(false, string.Join(".  ", identityResult.Errors)));
        }

        #endregion

        #region Security Questions

        [HttpGet]
        public ActionResult UserSecurityQuestionsEdit()
        {
            List<SecurityQuestionViewModel> model = new List<SecurityQuestionViewModel>();
            VisitPayUserDto visitPayUserDto = this.VisitPayUserApplicationService.Value.FindById(this.User.Identity.GetUserId());

            if (visitPayUserDto != null)
            {
                // security questions
                List<SecurityQuestionDto> allQuestions = this._securityQuestionApplicationService.Value.GetAllSecurityQuestions().ToList();

                // maximum of client setting or # of current answers
                int minimumNumberOfQuestions = Math.Max(this.ClientDto.GuarantorSecurityQuestionsCount, visitPayUserDto.SecurityQuestionAnswers.Count());

                // existing questions
                foreach (SecurityQuestionAnswerDto securityQuestion in visitPayUserDto.SecurityQuestionAnswers)
                {
                    SecurityQuestionViewModel securityQuestionViewModel = new SecurityQuestionViewModel
                    {
                        Answer = null,
                        SecurityQuestionId = securityQuestion.SecurityQuestion.SecurityQuestionId,
                        SecurityQuestions = new List<SelectListItem>()
                    };

                    allQuestions.ForEach(x => securityQuestionViewModel.SecurityQuestions.Add(new SelectListItem { Text = x.Question, Value = x.SecurityQuestionId.ToString() }));

                    SelectListItem selectedQuestion = securityQuestionViewModel.SecurityQuestions.FirstOrDefault(x => Convert.ToInt32(x.Value) == securityQuestion.SecurityQuestion.SecurityQuestionId);
                    if (selectedQuestion != null)
                        selectedQuestion.Selected = true;

                    model.Add(securityQuestionViewModel);
                }

                // any questions needed to reach the minimum required number
                // scenario: client setting changing after a user has registered
                // example: client setting is now 3, but was 2 at time of registration, should be additional question shown here
                // note: if client setting < current answers, continue showing all current answers
                int difference = minimumNumberOfQuestions - model.Count();
                if (difference > 0)
                {
                    for (int i = 0; i < difference; i++)
                    {
                        SecurityQuestionViewModel securityQuestionViewModel = new SecurityQuestionViewModel
                        {
                            Answer = null,
                            SecurityQuestionId = default(int),
                            SecurityQuestions = new List<SelectListItem>()
                        };

                        allQuestions.ForEach(x => securityQuestionViewModel.SecurityQuestions.Add(new SelectListItem { Text = x.Question, Value = x.SecurityQuestionId.ToString() }));

                        model.Add(securityQuestionViewModel);
                    }
                }
            }

            return this.View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult UserSecurityQuestionsEdit(IList<SecurityQuestionViewModel> model)
        {
            if (!this.ModelState.IsValid)
                return this.Json(new ResultMessage(false, this.GenericErrorMessage));

            VisitPayUserDto visitPayUserDto = this.VisitPayUserApplicationService.Value.FindById(this.User.Identity.GetUserId());
            if (visitPayUserDto == null)
                return this.Json(new ResultMessage(false, this.GenericErrorMessage));

            visitPayUserDto.SecurityQuestionAnswers = new List<SecurityQuestionAnswerDto>();
            foreach (SecurityQuestionViewModel question in model)
            {
                SecurityQuestionAnswerDto mapped = Mapper.Map<SecurityQuestionAnswerDto>(question);
                mapped.SecurityQuestion = new SecurityQuestionDto { SecurityQuestionId = question.SecurityQuestionId };
                visitPayUserDto.SecurityQuestionAnswers.Add(mapped);
            }

            IdentityResult identityResult = this.VisitPayUserApplicationService.Value.UpdateUser(visitPayUserDto);
            if (identityResult.Succeeded)
            {
                this.VisitPayUserApplicationService.Value.LogAddEditSecurityQuestions(
                    user: visitPayUserDto,
                    context: Mapper.Map<JournalEventHttpContextDto>(System.Web.HttpContext.Current),
                    isClient: false
                );
            }

            string resultMessage = identityResult.Succeeded ? LocalizationHelper.GetLocalizedString(TextRegionConstants.SecurityQuestionsUpdated) : this.GenericErrorMessage;
            return this.Json(new ResultMessage(identityResult.Succeeded, resultMessage));
        }

        #endregion
        
        #region client survey

        [AllowAnonymous]
        public PartialViewResult ClientSurvey()
        {
            CmsVersionDto cmsVersion = this.ContentApplicationService.Value.GetCurrentVersionAsync(CmsRegionEnum.ClientSurveyText).Result;

            if (string.IsNullOrEmpty(cmsVersion.ContentBody))
            {
                return null;
            }

            return this.PartialView("~/Areas/Mobile/Views/Account/_ClientSurvey.cshtml", cmsVersion);
        }

        #endregion

        #region alerts

        [RequiresAcknowledgement(Enabled = false)]
        [RequiresSsoAuthorization(Enabled = false)]
        public override ActionResult Alerts(bool isMobile = false)
        {
            // overriding so that ajax can hit this and realize the request is still "mobile".
            // matters for links within the alert partial
            return base.Alerts(true);
        }

        #endregion
    }
}
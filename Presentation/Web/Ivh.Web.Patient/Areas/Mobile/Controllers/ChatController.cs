﻿
namespace Ivh.Web.Patient.Areas.Mobile.Controllers
{
    using Application.Core.Common.Interfaces;
    using Common.Web.Controllers;
    using Common.Web.Interfaces;
    using Ivh.Common.VisitPay.Strings;
    using Ivh.Web.Patient.Attributes;
    using System;
    using System.Web.Mvc;

    /// <summary>
    /// Mobile Chat Controller
    /// </summary>
    //NOTE: Feature bound on BaseChatController
    [GuarantorAuthorize(Roles = VisitPayRoleStrings.System.Patient + "," + VisitPayRoleStrings.Admin.Administrator)]
    public class ChatController : BaseChatController
    {
        public ChatController(
            Lazy<IBaseControllerService> baseControllerService,
            Lazy<IChatApplicationService> chatApplicationService
        ) : base(baseControllerService, chatApplicationService)
        {

        }
        
        [HttpGet]
        public ActionResult ChatModal()
        {
            return this.PartialView("_ChatModal");
        }
    }
}
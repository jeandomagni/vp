﻿namespace Ivh.Web.Patient.Areas.Mobile.Controllers
{
    using System;
    using System.Threading.Tasks;
    using System.Web.Mvc;
    using System.Web.SessionState;
    using Application.Content.Common.Interfaces;
    using Application.Core.Common.Interfaces;
    using Application.FinanceManagement.Common.Interfaces;
    using Application.SecureCommunication.Common.Interfaces;
    using Attributes;
    using Common.VisitPay.Strings;
    using Common.Web.Controllers;
    using Common.Web.Interfaces;
    using Services;

    [GuarantorAuthorize(Roles = VisitPayRoleStrings.System.Patient + "," + VisitPayRoleStrings.Admin.Administrator)]
    public class SupportController : BaseSupportController
    {
        private readonly Lazy<GuarantorFilterService> _guarantorFilterService;

        public SupportController(
            Lazy<IBaseControllerService> baseControllerService,
            Lazy<IContentApplicationService> contentApplicationService,
            Lazy<IConsolidationGuarantorApplicationService> consolidationGuarantorApplicationService,
            Lazy<ISupportRequestApplicationService> supportRequestApplicationService,
            Lazy<ISupportTopicApplicationService> supportTopicApplicationService,
            Lazy<ITreatmentLocationApplicationService> treatmentLocationApplicationService,
            Lazy<IVisitApplicationService> visitApplicationService,
            Lazy<IVisitPayUserApplicationService> visitPayUserApplicationService,
            Lazy<IVisitPayUserJournalEventApplicationService> visitPayUserJournalEventApplicationService,
            Lazy<GuarantorFilterService> guarantorFilterService
        )
            : base(baseControllerService,
                contentApplicationService,
                consolidationGuarantorApplicationService,
                supportRequestApplicationService,
                supportTopicApplicationService,
                treatmentLocationApplicationService,
                visitApplicationService,
                visitPayUserApplicationService,
                visitPayUserJournalEventApplicationService)
        {
            this._guarantorFilterService = guarantorFilterService;
        }

        [HttpGet]
        public PartialViewResult CreateSupportRequest(int? visitId)
        {
            return this.PartialView("_CreateSupportRequest", this.CreateSupportRequestModel(this.CurrentUserId, this._guarantorFilterService.Value.VpGuarantorId, visitId,true));
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<JsonResult> CreateSupportRequestConfirmation()
        {
            return await this.CreateSupportRequestConfirmation(this.CurrentUserId);
        }

        [HttpGet]
        public ActionResult MySupportRequests()
        {
            return this.View();
        }

        [HttpGet]
        public PartialViewResult SupportRequestPartial()
        {
            return this.PartialView("_SupportRequest");
        }

        [HttpGet]
        public PartialViewResult SupportRequestReply()
        {
            return this.PartialView("_SupportRequestReply");
        }

        [HttpGet]
        public PartialViewResult SupportRequestReopen()
        {
            return this.PartialView("_SupportRequestReopen");
        }

        [HttpGet]
        public PartialViewResult SupportRequestClose()
        {
            return this.PartialView("_SupportRequestClose");
        }

        [HttpGet]
        public PartialViewResult SupportRequestAttachments()
        {
            return this.PartialView("_SupportRequestAttachments");
        }

        [HttpGet]
        public PartialViewResult SupportRequestsWidget()
        {
            return this.PartialView("_SupportRequestsWidget", this.CreateSupportRequestWidgetViewModel(true));
        }
    }
}
﻿namespace Ivh.Web.Patient.Areas.Mobile.Controllers
{
    using System;
    using Application.Base.Common.Interfaces;
    using Application.Core.Common.Interfaces;
    using Application.FinanceManagement.Common.Interfaces;
    using Attributes;
    using Common.VisitPay.Strings;
    using Common.Web.Interfaces;
    using Patient.Controllers.Base;
    using Services;

    [GuarantorAuthorize(Roles = VisitPayRoleStrings.System.Patient + "," + VisitPayRoleStrings.Admin.Administrator)]
    public class SidebarController : BaseSidebarController
    {
        public SidebarController(
            Lazy<IBaseControllerService> baseControllerService,
            Lazy<IExternalLinkApplicationService> externalLinkApplicationService,
            Lazy<GuarantorFilterService> guarantorFilterService,
            Lazy<IFinancePlanApplicationService> financePlanApplicationService,
            Lazy<IVisitBalanceBaseApplicationService> visitBalanceBaseApplicationService)
            : base(baseControllerService,
                externalLinkApplicationService,
                guarantorFilterService,
                financePlanApplicationService,
                visitBalanceBaseApplicationService)
        {
        }
    }
}
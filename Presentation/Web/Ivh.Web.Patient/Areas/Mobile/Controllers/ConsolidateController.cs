﻿namespace Ivh.Web.Patient.Areas.Mobile.Controllers
{
    using System;
    using System.Threading.Tasks;
    using System.Web.Mvc;
    using System.Web.SessionState;
    using Application.Content.Common.Interfaces;
    using Application.Core.Common.Interfaces;
    using Application.FinanceManagement.Common.Interfaces;
    using Common.Base.Enums;
    using Common.VisitPay.Enums;
    using Common.Web.Filters;
    using Common.Web.Interfaces;
    using Microsoft.AspNet.Identity;
    using Patient.Controllers.Base;
    using Patient.Models.Consolidate;
    using Services;

    [RequireFeature(VisitPayFeatureEnum.Consolidation)]
    public class ConsolidateController : BaseConsolidateController
    {
        public ConsolidateController(
            Lazy<IBaseControllerService> baseControllerService,
            Lazy<IContentApplicationService> contentApplicationService,
            Lazy<GuarantorFilterService> guarantorFilterService,
            Lazy<IConsolidationGuarantorApplicationService> consolidationGuarantorApplicationService,
            Lazy<IFinancePlanApplicationService> financePlanApplicationService,
            Lazy<IManagedUserApplicationService> managedUserApplicationService,
            Lazy<IManagingUserApplicationService> managingUserApplicationService,
            Lazy<IVisitPayUserJournalEventApplicationService> visitPayUserJournalEventApplicationService,
            Lazy<IGuarantorApplicationService> guarantorApplicationService,
            Lazy<IVisitApplicationService> visitService)
            : base(
                baseControllerService,
                contentApplicationService,
                guarantorFilterService,
                consolidationGuarantorApplicationService,
                financePlanApplicationService,
                managedUserApplicationService,
                managingUserApplicationService,
                visitPayUserJournalEventApplicationService,
                guarantorApplicationService,
                visitService)
        {
        }

        [HttpGet]
        [Common.Web.Attributes.SessionState(SessionStateBehavior.Required)]
        public ActionResult ManageHousehold()
        {
            ManageHouseholdViewModel model = this.GetManageHouseholdModel(this.CurrentUserId);
            return this.View(model);
        }

        [HttpPost]
        [Common.Web.Attributes.SessionState(SessionStateBehavior.Required)]
        public JsonResult GetHouseholdData()
        {
            ManageHouseholdViewModel model = this.GetManageHouseholdModel(this.CurrentUserId);
            return this.Json(model);
        }

        [HttpGet]
        public PartialViewResult RequestToBeManaged()
        {
            return this.PartialView("_RequestToBeManaged", this.GetRequestManagedModel());
        }

        [HttpGet]
        public PartialViewResult RequestToBeManaging()
        {
            return this.PartialView("_RequestToBeManaging");
        }

        [HttpGet]
        public PartialViewResult AcceptTerms(Guid id, int requestType)
        {
            return this.PartialView("_AcceptTerms", this.GetAcceptTermsModel(id, requestType));
        }

        [HttpGet]
        public async Task<PartialViewResult> AcceptTermsFp(Guid id, int requestType)
        {
            AcceptTermsFpViewModel model = await this.GetAcceptTermsFpModel(id, requestType).ConfigureAwait(false);

            return this.PartialView("_AcceptTermsFp", model);
        }
        
        [HttpGet]
        public ActionResult DeconsolidateFp(Guid id, ConsolidationMatchRequestTypeEnum requestType)
        {
            var model = this.GetDeconsolidateFpViewModel(id, requestType);

            return this.PartialView("_DeconsolidateFp", model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task DeclineConsolidation(Guid consolidationGuarantorId, ConsolidationMatchRequestTypeEnum requestType)
        {
            await this.DoDeclineConsolidation(consolidationGuarantorId, requestType).ConfigureAwait(false);
        }
    }
}
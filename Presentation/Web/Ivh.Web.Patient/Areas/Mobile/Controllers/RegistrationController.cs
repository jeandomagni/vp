﻿namespace Ivh.Web.Patient.Areas.Mobile.Controllers
{
    using System;
    using System.Web.Mvc;
    using Application.Content.Common.Interfaces;
    using Application.Core.Common.Interfaces;
    using Application.FinanceManagement.Common.Interfaces;
    using Common.ServiceBus.Interfaces;
    using Common.Web.Cookies;
    using Common.Web.Interfaces;
    using Patient.Controllers.Base;
    using Services;
    using SessionFacade;

    [AllowAnonymous]
    public class RegistrationController : BaseRegistrationController
    {
        public RegistrationController(
            Lazy<IBaseControllerService> baseControllerService,
            Lazy<IBus> bus,
            Lazy<GuarantorFilterService> guarantorFilterService,
            Lazy<IContentApplicationService> contentApplicationService,
            Lazy<IGuarantorApplicationService> guarantorApplicationService,
            Lazy<IPaymentMethodsApplicationService> paymentMethodsApplicationService,
            Lazy<IPaymentMethodAccountTypeApplicationService> paymentMethodAccountTypeApplicationService,
            Lazy<IPaymentMethodProviderTypeApplicationService> paymentMethodProviderTypeApplicationService,
            Lazy<ISecurityQuestionApplicationService> securityQuestionApplicationService,
            Lazy<ISsoApplicationService> ssoApplicationService,
            Lazy<IVisitPayUserApplicationService> visitPayUserApplicationService,
            Lazy<IWebCookieFacade> webCookie,
            Lazy<IWebPatientSessionFacade> webPatientSessionFacade,
            Lazy<IRegistrationService> registrationService)
            : base(baseControllerService,
                bus,
                guarantorFilterService,
                contentApplicationService,
                guarantorApplicationService,
                paymentMethodsApplicationService,
                paymentMethodAccountTypeApplicationService,
                paymentMethodProviderTypeApplicationService,
                securityQuestionApplicationService,
                ssoApplicationService,
                visitPayUserApplicationService,
                webCookie,
                webPatientSessionFacade,
                registrationService)
        {
        }
    }
}
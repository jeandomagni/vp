﻿namespace Ivh.Web.Patient.Areas.Mobile.Controllers
{
    using System;
    using System.Threading.Tasks;
    using System.Web.Mvc;
    using Application.Content.Common.Interfaces;
    using Application.Core.Common.Interfaces;
    using Application.FinanceManagement.Common.Interfaces;
    using Attributes;
    using AutoMapper;
    using Common.VisitPay.Enums;
    using Common.VisitPay.Strings;
    using Common.Web.Controllers;
    using Common.Web.Interfaces;
    using Common.Web.Models.FinancePlan;
    using Common.Web.Models.Payment;
    using Provider.Pdf;
    using Services;

    [GuarantorAuthorize(Roles = VisitPayRoleStrings.System.Patient + "," + VisitPayRoleStrings.Admin.Administrator)]
    public class FinancePlanController : BaseFinancePlanController
    {
        private readonly Lazy<GuarantorFilterService> _guarantorFilterService;

        public FinancePlanController(
            Lazy<IBaseControllerService> baseControllerService,
            Lazy<IContentApplicationService> contentApplicationService,
            Lazy<IFinancialDataSummaryApplicationService> financialDataSummaryApplicationService,
            Lazy<IFinancePlanApplicationService> financePlanApplicationService,
            Lazy<IGuarantorApplicationService> guarantorApplicationService,
            Lazy<IPaymentDetailApplicationService> paymentDetailApplicationService,
            Lazy<IPaymentMethodsApplicationService> paymentMethodsApplicationService,
            Lazy<IReconfigureFinancePlanApplicationService> reconfigureFinancePlanApplicationService,
            Lazy<IVisitApplicationService> visitApplicationService,
            Lazy<IVisitPayUserApplicationService> visitPayUserApplicationService,
            Lazy<GuarantorFilterService> guarantorFilterService,
            Lazy<IPdfConverter> pdfConverter)
            : base(baseControllerService,
                contentApplicationService,
                financialDataSummaryApplicationService,
                financePlanApplicationService,
                guarantorApplicationService,
                paymentDetailApplicationService,
                paymentMethodsApplicationService,
                reconfigureFinancePlanApplicationService,
                visitApplicationService,
                visitPayUserApplicationService,
                pdfConverter)
        {
            this._guarantorFilterService = guarantorFilterService;
        }

        [HttpGet]
        public ActionResult MyFinancePlans()
        {
            return this.RedirectToAction("MyPayments", "Payment");
        }

        [HttpGet]
        public async Task<ActionResult> ReconfigureFinancePlan(int id)
        {
            int vpGuarantorId = this._guarantorFilterService.Value.VpGuarantorId;

			ReconfigureFinancePlanViewModel model = await this.GetReconfigureTermsModelAsync(id, vpGuarantorId, null);
            model.ReconfigureAcceptUrl = this.Url.Action("ReconfigureAccept", "FinancePlan", new {area = ""});
            model.ReconfigureCancelUrl = this.Url.Action("ReconfigureCancel", "FinancePlan", new {area = ""});
            model.ReconfigureSubmitUrl = this.Url.Action("ReconfigureSubmit", "FinancePlan", new {area = ""});
            model.ReconfigureValidateUrl = this.Url.Action("ReconfigureValidate", "FinancePlan", new {area = ""});
            model.IsEditable = model.Terms == null;

            if (model.Terms != null && model.TermsCmsVersionId != default(int))
            {
                model.TermsCmsVersionId = model.Terms.TermsCmsVersionId;
            }
            else
            {
                int? ricCmsRegionId = this.VisitApplicationService.Value.GetRicCmsRegionId(vpGuarantorId);
                CmsRegionEnum cmsRegionEnum = !ricCmsRegionId.HasValue || ricCmsRegionId == default(int) ? CmsRegionEnum.VppCreditAgreement : (CmsRegionEnum) ricCmsRegionId;

                model.TermsCmsVersionId = (await this.ContentApplicationService.Value.GetCurrentVersionAsync(cmsRegionEnum, false)).CmsVersionId;
            }

            int currentVpGuarantorId = this.VisitPayUserApplicationService.Value.GetGuarantorIdFromVisitPayUserId(this.CurrentUserId);
            model.PrimaryPaymentMethod = Mapper.Map<PaymentMethodListItemViewModel>(this.PaymentMethodsApplicationService.Value.GetPrimaryPaymentMethod(currentVpGuarantorId));

            return this.View(model);
        }
    }
}
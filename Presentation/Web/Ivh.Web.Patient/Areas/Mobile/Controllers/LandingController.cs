﻿namespace Ivh.Web.Patient.Areas.Mobile.Controllers
{
    using System;
    using System.Web.Mvc;
    using Application.Content.Common.Interfaces;
    using Application.Core.Common.Interfaces;
    using Common.Web.Cookies;
    using Common.Web.Interfaces;
    using Patient.Controllers.Base;

    [AllowAnonymous]
    public class LandingController : BaseLandingController
    {
        public LandingController(Lazy<IBaseControllerService> baseControllerService,
            Lazy<IContentApplicationService> contentApplicationService,
            Lazy<ISsoApplicationService> ssoApplicationService,
            Lazy<IWebCookieFacade> webCookie)
            : base(baseControllerService, contentApplicationService, ssoApplicationService, webCookie)
        {
        }
    }
}
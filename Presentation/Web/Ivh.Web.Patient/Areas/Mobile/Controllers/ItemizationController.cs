﻿namespace Ivh.Web.Patient.Areas.Mobile.Controllers
{
    using System;
    using System.Web.Mvc;
    using System.Web.SessionState;
    using Application.Content.Common.Interfaces;
    using Application.Core.Common.Interfaces;
    using Application.FileStorage.Common.Interfaces;
    using Attributes;
    using Common.Base.Enums;
    using Common.VisitPay.Enums;
    using Common.VisitPay.Strings;
    using Common.Web.Controllers;
    using Common.Web.Filters;
    using Common.Web.Interfaces;
    using Models;
    using Services;

    [GuarantorAuthorize(Roles = VisitPayRoleStrings.System.Patient + "," + VisitPayRoleStrings.Admin.Administrator)]
    [RequireFeature(VisitPayFeatureEnum.Itemization)]
    public class ItemizationController : BaseItemizationController
    {
        private readonly Lazy<GuarantorFilterService> _guarantorFilterService;

        public ItemizationController(
            Lazy<IBaseControllerService> baseControllerService,
            Lazy<GuarantorFilterService> guarantorFilterService,
            Lazy<IContentApplicationService> contentApplicationService,
            Lazy<IFileStorageApplicationService> fileStorageApplicationService,
            Lazy<IVisitItemizationStorageApplicationService> visitItemizationStorageApplicationService)
            : base(baseControllerService,
            contentApplicationService,
            fileStorageApplicationService,
            visitItemizationStorageApplicationService)
        {
            this._guarantorFilterService = guarantorFilterService;
        }

        [HttpGet]
        public ActionResult Itemizations()
        {
            return this.RedirectToAction("Documents", "Account", new {area = "mobile"});
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Common.Web.Attributes.SessionState(SessionStateBehavior.Required)]
        public JsonResult ItemizationsData()
        {
            ItemizationsViewModel model = new ItemizationsViewModel
            {
                Filter = this.GetItemizationSearchFilter(this._guarantorFilterService.Value.VpGuarantorId),
                Rows = 10,
                SortField = "DischargeDate",
                SortOrder = "desc"
            };

            model.Data = this.GetItemizations(this._guarantorFilterService.Value.VpGuarantorId, model.Filter, 1, model.Rows, model.SortField, model.SortOrder);

            return this.Json(model);
        }
    }
}
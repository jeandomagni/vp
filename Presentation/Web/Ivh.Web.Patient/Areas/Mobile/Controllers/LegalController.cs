﻿namespace Ivh.Web.Patient.Areas.Mobile.Controllers
{
    using System;
    using System.Threading.Tasks;
    using System.Web.Mvc;
    using System.Web.SessionState;
    using Application.Content.Common.Dtos;
    using Application.Content.Common.Interfaces;
    using Application.Core.Common.Interfaces;
    using Application.FinanceManagement.Common.Interfaces;
    using Attributes;
    using Common.VisitPay.Strings;
    using Common.Web.Controllers;
    using Common.Web.Interfaces;
    using Common.Web.Models.Legal;
    using Provider.Pdf;

    [GuarantorAuthorize(Roles = VisitPayRoleStrings.System.Patient + "," + VisitPayRoleStrings.Admin.Administrator)]
    [RequiresAcknowledgement(Enabled = false)]
    [RequiresSsoAuthorization(Enabled = false)]
    public class LegalController : BaseLegalController
    {
        public LegalController(
            Lazy<IBaseControllerService> baseControllerService,
            Lazy<IGuarantorApplicationService> guarantorApplicationService,
            Lazy<IContentApplicationService> contentApplicationService,
            Lazy<ICreditAgreementApplicationService> creditAgreementApplicationService,
            Lazy<IPdfConverter> pdfConverter)
            : base(baseControllerService, guarantorApplicationService, contentApplicationService, creditAgreementApplicationService, pdfConverter)
        {
        }
        
        [HttpGet]
        [Common.Web.Attributes.SessionState(SessionStateBehavior.ReadOnly)]
        public async Task<ActionResult> CreditAgreementPartial(int o)
        {
            ContentDto content = await this.CreditAgreementApplicationService.Value.GetCreditAgreementContentAsync(o);

            CreditAgreementViewModel model = new CreditAgreementViewModel
            {
                Text = content.ContentBody,
                Title = content.ContentTitle,
                IsExport = false,
                FinancePlanOfferId = o
            };

            return this.PartialView("_CreditAgreement", model);
        }

        [HttpGet]
        [AllowAnonymous]
        public ActionResult Legal()
        {
            return this.View();
        }
        
        [HttpGet]
        [AllowAnonymous]
        public ActionResult Privacy()
        {
            return this.View();
        }

        [HttpGet]
        [AllowAnonymous]
        public ActionResult PrivacyPartial()
        {
            return this.PartialView("_Privacy");
        }
    }
}
﻿namespace Ivh.Web.Patient.Areas.Mobile.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using System.Web.Mvc;
    using Application.Base.Common.Interfaces;
    using Application.Content.Common.Dtos;
    using Application.Content.Common.Interfaces;
    using Application.Core.Common.Dtos;
    using Application.Core.Common.Interfaces;
    using Application.FinanceManagement.Common.Interfaces;
    using Attributes;
    using AutoMapper;
    using Common.Base.Enums;
    using Common.VisitPay.Enums;
    using Common.VisitPay.Strings;
    using Common.Web.Interfaces;
    using Common.Web.Models.Faq;
    using Common.Web.Models.Shared;
    using Patient.Controllers.Base;
    using Services;
    using SessionFacade;

    [GuarantorAuthorize(Roles = VisitPayRoleStrings.System.Patient + "," + VisitPayRoleStrings.Admin.Administrator)]
    public class HomeController : BaseHomeController
    {
        public HomeController(
            Lazy<IBaseControllerService> baseControllerService,
            Lazy<IContentApplicationService> contentApplicationService,
            Lazy<IFinancePlanApplicationService> financePlanApplicationService,
            Lazy<IVisitBalanceBaseApplicationService> visitBalanceBaseApplicationService,
            Lazy<IVisitPayUserApplicationService> visitPayUserApplicationService,
            Lazy<GuarantorFilterService> guarantorFilterService,
            Lazy<IWebPatientSessionFacade> patientSession,
            Lazy<IPaymentMethodsApplicationService> paymentMethodsApplicationService,
            Lazy<IPaymentSubmissionApplicationService> paymentSubmissionApplicationService,
            Lazy<IKnowledgeBaseApplicationService> knowledgeBaseApplicationService) :
            base(baseControllerService,
                contentApplicationService,
                financePlanApplicationService,
                visitBalanceBaseApplicationService,
                visitPayUserApplicationService,
                guarantorFilterService,
                patientSession,
                paymentMethodsApplicationService,
                paymentSubmissionApplicationService,
                knowledgeBaseApplicationService)
        {
        }

        [HttpGet]
        public ActionResult HelpCenter()
        {
            ContentMenuDto contentMenuDto = this.ContentApplicationService.Value.GetContentMenuAsync(ContentMenuEnum.HelpCenterMobile, this.User.Identity.IsAuthenticated).Result;

            return this.View(Mapper.Map<ContentMenuViewModel>(contentMenuDto));
        }

        [HttpGet]
        public async Task<ActionResult> Faqs()
        {
            IList<KnowledgeBaseCategoryDto> categoryDtos = await this.KnowledgeBaseApplicationService.Value.GetKnowledgeBaseCategoriesAsync(KnowledgeBaseApplicationTypeEnum.Mobile);

            List<KnowledgeBaseCategoryViewModel> categoryViewModels = Mapper.Map<List<KnowledgeBaseCategoryViewModel>>(categoryDtos);
            return this.View(categoryViewModels);
        }
    }
}
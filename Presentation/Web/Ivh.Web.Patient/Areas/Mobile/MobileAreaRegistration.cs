﻿namespace Ivh.Web.Patient.Areas.Mobile
{
    using System.Web.Mvc;

    public class MobileAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get { return "Mobile"; }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "Mobile_default",
                "mobile/{controller}/{action}/{id}",
                new {controller = "Home", action = "Index", id = UrlParameter.Optional});
        }
    }
}
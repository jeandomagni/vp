﻿namespace Ivh.Web.Patient.Areas.Mobile.Models
{
    using Common.Web.Models.Itemization;

    public class ItemizationsViewModel
    {
        public ItemizationSearchResultsViewModel Data { get; set; }
        public ItemizationSearchFilterViewModel Filter { get; set; }

        public int Rows { get; set; }
        public string SortField { get; set; }
        public string SortOrder { get; set; }
    }
}
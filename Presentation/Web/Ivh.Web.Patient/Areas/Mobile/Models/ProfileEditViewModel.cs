﻿namespace Ivh.Web.Patient.Areas.Mobile.Models
{
    using System.ComponentModel.DataAnnotations;
    using Common.Base.Constants;
    using Common.Base.Utilities.Helpers;
    using Common.VisitPay.Constants;
    using Common.Web.Attributes;
    using Common.Web.Models.Account;

    public class ProfileEditMobileViewModel
    {
        public ProfileEditMobileViewModel()
        {
            this.ProfileEditName = new ProfileEditNameViewModel();
            this.ProfileEditUserName = new ProfileEditUserNameViewModel();
            this.ProfileEditEmail = new ProfileEditEmailViewModel();
            this.ProfileEditAddress = new ProfileEditAddressViewModel();
            this.ProfileEditPhoneNumber = new ProfileEditPhoneNumberViewModel();
        }

        public ProfileEditNameViewModel ProfileEditName { get; set; }
        public ProfileEditUserNameViewModel ProfileEditUserName { get; set; }
        public ProfileEditEmailViewModel ProfileEditEmail { get; set; }
        public ProfileEditAddressViewModel ProfileEditAddress { get; set; }
        public ProfileEditPhoneNumberViewModel ProfileEditPhoneNumber { get; set; }
    }

    public class ProfileEditNameViewModel
    {
        [LocalizedDisplayName(TextRegionConstants.NameFirst)]
        [LocalizedPrompt(TextRegionConstants.NameFirst)]
        [Required(ErrorMessage = "First Name is required.")]
        public string FirstName { get; set; }

        [LocalizedDisplayName(TextRegionConstants.NameMiddle)]
        [LocalizedPrompt(TextRegionConstants.NameMiddle)]
        public string MiddleName { get; set; }

        [LocalizedDisplayName(TextRegionConstants.NameLast)]
        [LocalizedPrompt(TextRegionConstants.NameLast)]
        [Required(ErrorMessage = "Last Name is required.")]
        public string LastName { get; set; }

        public string LastNameFirstName => FormatHelper.LastNameFirstName(this.LastName, this.FirstName);

        public string FirstNameLastName => FormatHelper.FirstNameLastName(this.FirstName, this.LastName);
    }


    public class ProfileEditEmailViewModel
    {
        [LocalizedDisplayName(TextRegionConstants.EmailAddress)]
        [LocalizedPrompt(TextRegionConstants.EmailAddress)]
        [Required(ErrorMessage = "Notification Email is required.")]
        [RegularExpressionEmailAddress(TextRegionConstants.NotificationEmailInvalid)]
        [MaxLength(99, ErrorMessage = "The Email must be a maximum length of '99'.")]
        [DataType(DataType.EmailAddress)]
        public string Email { get; set; }

        public string EmailOriginal { get; set; }

        [LocalizedDisplayName(TextRegionConstants.ConfirmEmailAddress)]
        [LocalizedPrompt(TextRegionConstants.ConfirmEmailAddress)]
        [DataType(DataType.EmailAddress)]
        [CompareToIfNotEqual("Email", "EmailOriginal", "Email", false, TextRegionConstants.NotificationEmailsDoNotMatch)]
        public string EmailConfirm { get; set; }
    }

    public class ProfileEditAddressViewModel
    {
        [LocalizedDisplayName(TextRegionConstants.Address)]
        [LocalizedPrompt(TextRegionConstants.AddressStreetPo)]
        [Required(ErrorMessage = "Street or PO Box is required.")]
        public string AddressStreet1 { get; set; }

        [LocalizedPrompt(TextRegionConstants.AptSuiteOrBuildingNumber)]
        public string AddressStreet2 { get; set; }

        [LocalizedDisplayName(TextRegionConstants.AddressCity)]
        [LocalizedPrompt(TextRegionConstants.AddressCity)]
        [Required(ErrorMessage = "City is required.")]
        public string City { get; set; }

        [LocalizedDisplayName(TextRegionConstants.State)]
        [Required(ErrorMessage = "State is required.")]
        public string State { get; set; }

        [LocalizedDisplayName(TextRegionConstants.AddressZip)]
        [LocalizedPrompt(TextRegionConstants.AddressZip)]
        [Required(ErrorMessage = "Zip is required.")]
        [DataType("Zip")]
        [RegularExpressionZipCode(TextRegionConstants.InvalidZip)]
        public string Zip { get; set; }
    }

    public class ProfileEditPhoneNumberViewModel
    {
        [LocalizedDisplayName(TextRegionConstants.PhonePrimary)]
        [LocalizedPrompt(TextRegionConstants.PhonePrimary)]
        [Required(ErrorMessage = "Primary Phone is required.")]
        [DataType(DataType.PhoneNumber)]
        public string PhoneNumber { get; set; }

        [Required(ErrorMessage = "Primary Phone Type is required.")]
        public string PhoneNumberType { get; set; }

        [LocalizedDisplayName(TextRegionConstants.PhoneSecondary)]
        [LocalizedPrompt(TextRegionConstants.PhoneSecondary)]
        [DataType(DataType.PhoneNumber)]
        [NotEqual("PhoneNumber", TextRegionConstants.PhoneNumbersShouldNotMatch)]
        public string PhoneNumberSecondary { get; set; }

        [ConditionalRequired("PersonalInformation", "PhoneNumberSecondary", ErrorMessage = "Secondary Phone Type is required.")]
        public string PhoneNumberSecondaryType { get; set; }
    }
}
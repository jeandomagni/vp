﻿namespace Ivh.Web.Patient.Areas.Mobile.Models
{
    using System;
    using System.Linq;
    using Common.Web.Models.Payment;

    public class ArrangePaymentMobileViewModel : ArrangePaymentViewModel
    {
        public PaymentMethodListItemViewModel PaymentMethod { get; set; }

        public string SelectedPaymentMenuItemTitle
        {
            get
            {
                if (!this.PaymentMenu.PaymentMenuItems.Any())
                {
                    return string.Empty;
                }

                return this.PaymentMenu.PaymentMenuItems.First().Title;
            }
        }

        public string ReturnUrl { get; set; }

        public DateTime ClientNow { get; set; }
    }
}
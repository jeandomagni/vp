﻿namespace Ivh.Web.HttpRedirect.Tests
{
    using System.Reflection;
    using NUnit.Framework;

    [TestFixture]
    public class DependencyCheckerTest : Ivh.Common.Testing.DependencyCheckerTest
    {
        [Test]
        public void CheckDependencies()
        {
            base.CheckDependencies(Assembly.GetExecutingAssembly().GetName());
        }
    }
}

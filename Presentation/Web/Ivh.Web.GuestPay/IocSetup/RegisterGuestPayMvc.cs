﻿namespace Ivh.Web.GuestPay.IocSetup
{
    using System.Web;
    using Autofac;
    using Autofac.Integration.Mvc;
    using Autofac.Integration.WebApi;
    using Common.DependencyInjection;
    using Common.DependencyInjection.Registration;
    using Common.DependencyInjection.Registration.Register;
    using Common.Session;
    using Common.Web.Cookies;
    using Common.Web.Interfaces;
    using Common.Web.Services;
    using MassTransit;
    using Microsoft.Owin;
    using Owin;

    public class RegisterGuestPayMvc : IRegistrationModule
    {
        private readonly IAppBuilder _app;

        public RegisterGuestPayMvc(IAppBuilder app)
        {
            this._app = app;
        }

        public void Register(ContainerBuilder builder)
        {
            RegistrationSettings registrationSettings = RegistrationSettingsService.GetRegistrationSettings();
            Base.Register(builder, registrationSettings);
            VisitPay.Register(builder, registrationSettings, true);
            Data.Register(builder, registrationSettings)
                .AddGuestPayDatabase()
                .AddVisitPayDatabase();// wish we didn't have to have this session factory for so few entities
            Mvc.Register(builder, this._app);

            builder.RegisterType<BaseControllerService>().As<IBaseControllerService>().InstancePerRequest();
            builder.RegisterControllers(typeof(MvcApplication).Assembly);
            builder.RegisterApiControllers(typeof(MvcApplication).Assembly);
            builder.RegisterType<WebGuestPaySessionFacade>().As<IWebGuestPaySessionFacade>().PreserveExistingDefaults();
            builder.RegisterType<GuestPayCookieFacade>().As<IGuestPayCookieFacade>().PreserveExistingDefaults();
            builder.Register(c => HttpContext.Current.GetOwinContext().Authentication).InstancePerRequest();
            builder.Register(ctx => HttpContext.Current.GetOwinContext()).As<IOwinContext>();
            builder.RegisterType<LocalizationCookieFacade>().As<ILocalizationCookieFacade>().PreserveExistingDefaults();

            IBusControl serviceBus = IvinciContainer.Instance.Container().Resolve<IBusControl>();
            serviceBus?.Start();
        }
    }
}
﻿namespace Ivh.Web.GuestPay.Controllers
{
    using System;
    using Common.Web.Controllers;
    using Common.Web.Interfaces;

    public class BaseApiController : BaseWebApiController
    {
        public BaseApiController(Lazy<IBaseControllerService> baseControllerService)
            : base(baseControllerService)
        {
        }
    }
}
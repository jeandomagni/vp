﻿namespace Ivh.Web.GuestPay.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using System.Web;
    using System.Web.Http;
    using System.Web.Http.Description;
    using Application.GuestPay.Common.Dtos;
    using Application.GuestPay.Common.Interfaces;
    using Application.Logging.Common.Interfaces;
    using AutoMapper;
    using Common.Base.Constants;
    using Common.Base.Enums;
    using Common.EventJournal;
    using Common.Web.Interfaces;
    using Domain.Logging.Interfaces;
    using Models.RemotePayment;

    [Authorize]
    public class RemotePaymentController : BaseApiController
    {
        private readonly Lazy<IPaymentApplicationService> _paymentApplicationService;

        public RemotePaymentController(
            Lazy<IBaseControllerService> baseControllerService,
            Lazy<IPaymentApplicationService> paymentApplicationService) : base(baseControllerService)

        {
            this._paymentApplicationService = paymentApplicationService;
        }

        /// <summary>
        /// Make a remote payment
        /// </summary>
        /// <param name="remotePaymentInfo"></param>
        /// <returns></returns>
        /// <remarks>Make a one time payment for a guarantor</remarks>
        [HttpPost]
        [ResponseType(typeof(RemotePaymentResponse))]
        public async Task<IHttpActionResult> Post(RemotePaymentInfo remotePaymentInfo)
        {
            HttpContextWrapper context = this.Request.Properties["MS_HttpContext"] as HttpContextWrapper;
            JournalEventHttpContextDto journalContext = Mapper.Map<JournalEventHttpContextDto>(context);
            RemotePaymentInfoDto remotePaymentInfoDto = Mapper.Map<RemotePaymentInfoDto>(remotePaymentInfo);
            ProcessPaymentResponseDto paymentResponseDto = await this._paymentApplicationService.Value.ProcessRemotePayment(remotePaymentInfoDto, journalContext);
            RemotePaymentResponse remotePaymentResponse = Mapper.Map<RemotePaymentResponse>(paymentResponseDto);
            return this.Json(remotePaymentResponse);
        }
    }


}

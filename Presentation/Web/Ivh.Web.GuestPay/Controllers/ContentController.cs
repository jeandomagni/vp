﻿namespace Ivh.Web.GuestPay.Controllers
{
    using System;
    using System.Web.Mvc;
    using Application.Content.Common.Interfaces;
    using AutoMapper;
    using Common.Base.Enums;
    using Common.VisitPay.Enums;
    using Common.Web.Interfaces;
    using Common.Web.Models.Shared;

    public class ContentController : BaseController
    {
        private readonly Lazy<IContentApplicationService> _contentApplicationService;

        public ContentController(
            Lazy<IBaseControllerService> baseControllerService,
            Lazy<IContentApplicationService> contentApplicationService)
            : base(baseControllerService)
        {
            this._contentApplicationService = contentApplicationService;
        }

        [HttpGet]
        public ActionResult Faqs()
        {
            return this.View(Mapper.Map<CmsViewModel>(this._contentApplicationService.Value.GetCurrentVersionAsync(CmsRegionEnum.GuestPayFaqs).Result));
        }

        [HttpGet]
        public ActionResult ContactUs()
        {
            return this.RedirectToActionPermanent("ContactUs", "Account");
        }
    }
}
﻿namespace Ivh.Web.GuestPay.Controllers
{
    using System;
    using System.Web.Http;
    using System.Web.Http.Description;
    using Application.GuestPay.Common.Dtos;
    using Application.GuestPay.Common.Interfaces;
    using AutoMapper;
    using Common.Web.Interfaces;
    using Models.RemotePayment;

    [Authorize]
    public class GuarantorPaymentLookupController : BaseApiController
    {
        private readonly Lazy<IPaymentApplicationService> _paymentApplicationService;

        public GuarantorPaymentLookupController(
            Lazy<IBaseControllerService> baseControllerService,
            Lazy<IPaymentApplicationService> paymentApplicationService) : base(baseControllerService)

        {
            this._paymentApplicationService = paymentApplicationService;
        }

        /// <summary>
        /// Validate the guarantor identifier and return the additional identifiers needed to make a remote payment
        /// </summary>
        /// <param name="sourceSystemKey">Guarantor identifier</param>
        /// <returns></returns>
        [HttpGet]
        [ResponseType(typeof(PaymentMatchUnitResponse))]
        public IHttpActionResult Get(string sourceSystemKey)
        {
            PaymentMatchUnitResponseDto paymentMatchUnitResponseDto = this._paymentApplicationService.Value.GetPaymentMatchUnitResponse(sourceSystemKey);
            PaymentMatchUnitResponse paymentMatchUnitResponse = Mapper.Map<PaymentMatchUnitResponse>(paymentMatchUnitResponseDto);
            return this.Json(paymentMatchUnitResponse);
        }

    }
}
﻿namespace Ivh.Web.GuestPay.Controllers
{
    using System;
    using Common.Web.Controllers;
    using Common.Web.Interfaces;

    public class BaseController : BaseWebController
    {
        public BaseController(Lazy<IBaseControllerService> baseControllerService)
            : base(baseControllerService)
        {
        }
    }
}
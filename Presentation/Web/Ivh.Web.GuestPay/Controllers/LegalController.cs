﻿namespace Ivh.Web.GuestPay.Controllers
{
    using System;
    using System.Web.Mvc;
    using Application.Content.Common.Dtos;
    using Application.Content.Common.Interfaces;
    using AutoMapper;
    using Common.Base.Enums;
    using Common.Base.Utilities.Helpers;
    using Common.VisitPay.Enums;
    using Common.Web.Interfaces;
    using Common.Web.Models.Shared;
    using Provider.Pdf;

    // do not inherit from BaseLegalController, as BaseLegalController requires authorization.
    // guestpay should only use BaseController
    public class LegalController : BaseController
    {
        private readonly Lazy<IContentApplicationService> _contentApplicationService;
        private readonly Lazy<IPdfConverter> _pdfConverter;

        public LegalController(
            Lazy<IBaseControllerService> baseControllerService,
            Lazy<IContentApplicationService> contentApplicationService,
            Lazy<IPdfConverter> pdfConverter)
            : base(baseControllerService)
        {
            this._contentApplicationService = contentApplicationService;
            this._pdfConverter = pdfConverter;
        }

        [HttpGet]
        public ActionResult Index()
        {
            ContentMenuDto contentMenuDto = this._contentApplicationService.Value.GetContentMenuAsync(ContentMenuEnum.LegalAgreementsGuestPay, false).Result;

            return this.View(Mapper.Map<ContentMenuViewModel>(contentMenuDto));
        }

        [HttpGet]
        public ActionResult Legal()
        {
            return this.View(Mapper.Map<CmsViewModel>(this._contentApplicationService.Value.GetCurrentVersionAsync(CmsRegionEnum.GuestPayLegal).Result));
        }

        [HttpGet]
        public ActionResult LegalPdf()
        {
            this.ViewData.Model = Mapper.Map<CmsViewModel>(this._contentApplicationService.Value.GetCurrentVersionAsync(CmsRegionEnum.GuestPayLegal).Result);

            string fileName = string.Format("{0}_Legal.pdf", ReplacementUtility.ReplaceWhiteSpace(ReplacementUtility.RemoveNonAlphaNumeric(this.ClientDto.GuestPayClientBrandName), "_"));
            this.WritePdfInline(this._pdfConverter.Value.CreatePdfFromHtml(string.Format("{0} Legal", this.ClientDto.GuestPayClientBrandName), this.RenderViewToString("~/Views/Shared/PrintContent.cshtml", "~/Views/Shared/Print.cshtml")), fileName);

            return null;
        }

        [HttpGet]
        public ActionResult PrivacyPolicy()
        {
            return this.View(Mapper.Map<CmsViewModel>(this._contentApplicationService.Value.GetCurrentVersionAsync(CmsRegionEnum.GuestPayPrivacyPolicy).Result));
        }

        [HttpGet]
        public ActionResult PrivacyPolicyPdf()
        {
            this.ViewData.Model = Mapper.Map<CmsViewModel>(this._contentApplicationService.Value.GetCurrentVersionAsync(CmsRegionEnum.GuestPayPrivacyPolicy).Result);

            string fileName = string.Format("{0}_PrivacyPolicy.pdf", ReplacementUtility.ReplaceWhiteSpace(ReplacementUtility.RemoveNonAlphaNumeric(this.ClientDto.GuestPayClientBrandName), "_"));
            this.WritePdfInline(this._pdfConverter.Value.CreatePdfFromHtml(string.Format("{0} Privacy Policy", this.ClientDto.GuestPayClientBrandName), this.RenderViewToString("~/Views/Shared/PrintContent.cshtml", "~/Views/Shared/Print.cshtml")), fileName);

            return null;
        }

        [HttpGet]
        public ActionResult TermsOfUse()
        {
            return this.View(Mapper.Map<CmsViewModel>(this._contentApplicationService.Value.GetCurrentVersionAsync(CmsRegionEnum.GuestPayTermsOfUse).Result));
        }

        [HttpGet]
        public ActionResult TermsOfUsePdf()
        {
            this.ViewData.Model = Mapper.Map<CmsViewModel>(this._contentApplicationService.Value.GetCurrentVersionAsync(CmsRegionEnum.GuestPayTermsOfUse).Result);

            string fileName = string.Format("{0}_TermsOfUse.pdf", ReplacementUtility.ReplaceWhiteSpace(ReplacementUtility.RemoveNonAlphaNumeric(this.ClientDto.GuestPayClientBrandName), "_"));
            this.WritePdfInline(this._pdfConverter.Value.CreatePdfFromHtml(string.Format("{0} Terms of Use", this.ClientDto.GuestPayClientBrandName), this.RenderViewToString("~/Views/Shared/PrintContent.cshtml", "~/Views/Shared/Print.cshtml")), fileName);

            return null;
        }
    }
}
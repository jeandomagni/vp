﻿namespace Ivh.Web.GuestPay.Controllers
{
    using System;
    using Common.Web.Controllers;
    using Common.Web.Interfaces;

    public class ErrorController : BaseErrorController
    {
        public ErrorController(
            Lazy<IBaseControllerService> baseControllerService)
            : base(baseControllerService)
        {
        }
    }
}
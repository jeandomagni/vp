﻿namespace Ivh.Web.GuestPay.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using System.Web.Mvc;
    using System.Web.SessionState;
    using Application.Content.Common.Dtos;
    using Application.Content.Common.Interfaces;
    using Application.GuestPay.Common.Dtos;
    using Application.GuestPay.Common.Interfaces;
    using Application.Logging.Common.Interfaces;
    using AutoMapper;
    using Common.Base.Utilities.Extensions;
    using Common.Base.Utilities.Helpers;
    using Common.EventJournal;
    using Common.VisitPay.Constants;
    using Common.VisitPay.Enums;
    using Common.Web.Cookies;
    using Common.Web.HtmlHelpers;
    using Common.Web.Interfaces;
    using Common.Web.Models;
    using Common.Web.Models.Shared;
    using Common.Web.Utilities;
    using Domain.Settings.Interfaces;
    using GpCommon;
    using Ivh.Common.Session;
    using Models.Payment;
    using Provider.Pdf;

    public class PaymentController : BaseController
    {
        private readonly Lazy<IContentApplicationService> _contentApplicationService;
        private readonly Lazy<IPaymentApplicationService> _paymentApplicationService;
        private readonly Lazy<IPaymentConfigurationService> _paymentConfigurationService;
        private readonly Lazy<IGuestPayJournalEventApplicationService> _guestPayJournalEventApplicationService;
        private readonly Lazy<IGuestPayGuarantorAuthenticationApplicationService> _guestPayGuarantorAuthenticationApplicationService;
        private readonly Lazy<IPdfConverter> _pdfConverter;
        private readonly Lazy<IGuestPayCookieFacade> _guestPayCookieFacade;
        private readonly Lazy<IWebGuestPaySessionFacade> _webGuestPaySessionFacade;
        private readonly Lazy<ILoggingApplicationService> _logger;

        private const string TimeoutUrl = "/#timeout";

        public PaymentController(
            Lazy<IBaseControllerService> baseControllerService,
            Lazy<IContentApplicationService> contentApplicationService,
            Lazy<IPaymentApplicationService> paymentApplicationService,
            Lazy<IPaymentConfigurationService> paymentConfigurationService,
            Lazy<IGuestPayJournalEventApplicationService> guestPayJournalEventApplicationService,
            Lazy<IPdfConverter> pdfConverter,
            Lazy<IGuestPayCookieFacade> guestPayCookieFacade,
            Lazy<IGuestPayGuarantorAuthenticationApplicationService> guestPayGuarantorAuthenticationApplicationService,
            Lazy<IWebGuestPaySessionFacade> webGuestPaySessionFacade,
            Lazy<ILoggingApplicationService> logger)
            : base(baseControllerService)
        {
            this._contentApplicationService = contentApplicationService;
            this._paymentApplicationService = paymentApplicationService;
            this._paymentConfigurationService = paymentConfigurationService;
            this._guestPayJournalEventApplicationService = guestPayJournalEventApplicationService;
            this._pdfConverter = pdfConverter;
            this._guestPayCookieFacade = guestPayCookieFacade;
            this._guestPayGuarantorAuthenticationApplicationService = guestPayGuarantorAuthenticationApplicationService;
            this._webGuestPaySessionFacade = webGuestPaySessionFacade;
            this._logger = logger;
        }

        const string HideBuildBannerKey = "HideBuildBanner";

        [HttpGet]
        [GuestPayLockout]
        public ActionResult Index()
        {
            PaymentSubmitViewModel paymentSubmitViewModel = new PaymentSubmitViewModel
            {
                PaymentMethodParentTypes = Mapper.Map<IList<SelectListItem>>(this._paymentConfigurationService.Value.AcceptedPaymentMethodParentTypes),
                CardAccount = new CardAccountViewModel()
            };

            PaymentViewModel model = new PaymentViewModel
            {
                GuarantorMatchRequestViewModel = new GuarantorMatchRequestViewModel
                {
                    GuarantorAuthenticationFields = this._guestPayGuarantorAuthenticationApplicationService.Value.GetGuestPayGuarantorAuthenticationFields()
                },
                PaymentSubmitViewModel = paymentSubmitViewModel,
            };

            return this.View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [GuestPayLockout]
        [Common.Web.Attributes.SessionState(SessionStateBehavior.Required)]
        public async Task<JsonResult> GuarantorMatch(GuarantorMatchRequestViewModel model)
        {
            if (!this.ModelState.IsValid)
            {
                return this.Json(new GuarantorMatchErrorViewModel(false, "Invalid submission."));
            }

            PaymentUnitAuthenticationDto paymentUnit = this._paymentApplicationService.Value.AuthenticatePaymentUnit(Mapper.Map<PaymentUnitMatchDto>(model), Mapper.Map<JournalEventHttpContextDto>(this.HttpContext));

            await this.BruteForceDelayAsync(this.ControllerContext.HttpContext.Request.UserHostAddress, () => paymentUnit == null, () => paymentUnit != null);

            if (paymentUnit == null)
            {
                if (this._guestPayCookieFacade.Value.IncrementAuthenticationAttempts(this.ClientDto.GuestPayMaxFailedAttemptCount, DateTime.UtcNow.AddMinutes(this.ClientDto.GuestPayAuthenticationLockoutTimeInMinutes)))
                {
                    this.SessionFacade.Value.ClearSession();
                    return this.Json(new GuarantorMatchErrorViewModel(false, this.Url.Action("SessionLocked"), true));
                }

                CmsVersionDto cmsVersion = this._contentApplicationService.Value.GetCurrentVersionAsync(CmsRegionEnum.GuestPayAccountNotFound).Result;
                return this.Json(new GuarantorMatchErrorViewModel(false, cmsVersion.ContentBody));
            }

            this._guestPayCookieFacade.Value.ResetAuthenticationAttempts();

            return this.Json(new ResultMessage<GuarantorMatchResponseViewModel>(true, new GuarantorMatchResponseViewModel
            {
                Guarantor = new GuarantorToken().Obfuscate(paymentUnit.PaymentUnitAuthenticationId, paymentUnit.PaymentUnitSourceSystemKey, paymentUnit.GuarantorLastName),
                Balance = this.ClientDto.GuestPayDisplayCurrentBalance && paymentUnit.Balance >= 0.01m ? paymentUnit.Balance : 0m,
                DisplayCurrentBalance = this.ClientDto.GuestPayDisplayCurrentBalance || paymentUnit.Balance == 0m
            }));
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [GuestPayLockout]
        [Common.Web.Attributes.SessionState(SessionStateBehavior.Required)]
        public async Task<JsonResult> SubmitPayment(PaymentSubmitViewModel model)
        {
            if (!this.ModelState.IsValid)
            {
                List<string> invalidKeys = this.ModelState.ToDictionary(k => k.Key, v => v.Value).Where(x => x.Value.Errors.Any()).Select(x => x.Key).ToList();
                this._logger.Value.Warn(() =>  $"PaymentController::SubmitPayment - model state is not valid for {string.Join(", ", invalidKeys)}");

                string resultMessage = LocalizationHelper.GetLocalizedString(TextRegionConstants.GuestPayPaymentValidationGeneralError);
                return this.Json(new ResultMessage(false, resultMessage));
            }

            GuarantorToken token = new GuarantorToken(model.Guarantor.Guarantor);
            PaymentSubmitDto paymentSubmitDto = Mapper.Map<PaymentSubmitDto>(model);
            paymentSubmitDto.PaymentSystemType = PaymentSystemTypeEnum.GuestPay;
            paymentSubmitDto.IpAddress = this.HttpContext.Request.UserHostAddress;
            ProcessPaymentResponseDto paymentResponseDto = await this._paymentApplicationService.Value.ProcessPaymentAsync(paymentSubmitDto, token.PaymentUnitAutheticationId, token.PaymentUnitSourceSystemKey, Mapper.Map<JournalEventHttpContextDto>(this.HttpContext));
            if (paymentResponseDto.IsError)
            {
                this._logger.Value.Warn(() =>  $"PaymentController::SubmitPayment - ProcessPaymentAsync unsuccessful - {string.Join(", ", paymentResponseDto.ErrorMessages)} - for PaymentUnitAuthenticationId: {token.PaymentUnitAutheticationId}, PaymentUnitSourceSystemKey: {token.PaymentUnitSourceSystemKey}, LastName: {token.GuarantorLastName}");
                return this.Json(new ResultMessage<IList<string>>(false, paymentResponseDto.ErrorMessages));
            }
            this.MetricsProvider.Value.Increment(Metrics.Increment.SecurePan.PaymentMethod.Success);

            GuarantorToken clarified = new GuarantorToken(model.Guarantor.Guarantor);
            Guid obfuscated = new GuarantorToken().Obfuscate(clarified.PaymentUnitAutheticationId, clarified.PaymentUnitSourceSystemKey, clarified.GuarantorLastName, paymentResponseDto.Payment.PaymentId);

            return this.Json(new ResultMessage(true, this.Url.Action("Receipt", new { token = obfuscated })));
        }

        [HttpGet]
        public ActionResult Receipt(Guid token)
        {
            GuarantorToken guarantorToken = new GuarantorToken(token);
            if (!guarantorToken.PaymentId.HasValue)
            {
                return this.Redirect(TimeoutUrl);
            }

            PaymentDto paymentDto = this._paymentApplicationService.Value.GetPayment(guarantorToken.PaymentId.Value, guarantorToken.PaymentUnitSourceSystemKey);
            if (paymentDto == null)
            {
                return this.Redirect(TimeoutUrl);
            }

            ReceiptViewModel model = Mapper.Map<ReceiptViewModel>(paymentDto) ?? new ReceiptViewModel();
            model.Token = token;
            model.TimeoutUrl = TimeoutUrl;

            return this.View(model);
        }

        [HttpGet]
        public ActionResult ReceiptPrint(Guid token)
        {
            GuarantorToken guarantorToken = new GuarantorToken(token);
            if (!guarantorToken.PaymentId.HasValue)
            {
                return this.Redirect(TimeoutUrl);
            }

            PaymentDto paymentDto = this._paymentApplicationService.Value.GetPayment(guarantorToken.PaymentId.Value, guarantorToken.PaymentUnitSourceSystemKey);
            if (paymentDto == null)
            {
                return this.Redirect(TimeoutUrl);
            }

            this._guestPayJournalEventApplicationService.Value.LogReceiptDownload(Mapper.Map<JournalEventHttpContextDto>(this.HttpContext), guarantorToken.PaymentUnitAutheticationId, guarantorToken.PaymentUnitSourceSystemKey, guarantorToken.GuarantorLastName, paymentDto.TransactionId);

            this.ViewData.Model = Mapper.Map<ReceiptViewModel>(paymentDto) ?? new ReceiptViewModel();

            string fileName = $"{ReplacementUtility.ReplaceWhiteSpace(ReplacementUtility.RemoveNonAlphaNumeric(this.ClientDto.GuestPayClientBrandName), "_")}_Receipt_{paymentDto.TransactionId}.pdf";
            this.WritePdfInline(this._pdfConverter.Value.CreatePdfFromHtml($"{this.ClientDto.GuestPayClientBrandName} Receipt ({paymentDto.TransactionId})", this.RenderViewToString("~/Views/Payment/ReceiptPrint.cshtml", "~/Views/Shared/Print.cshtml")), fileName);

            return null;
        }

        [HttpGet]
        public ActionResult SessionLocked()
        {
            CmsVersionDto cmsVersion = this._contentApplicationService.Value.GetCurrentVersionAsync(CmsRegionEnum.GuestPaySessionLocked).Result;

            return this.View(Mapper.Map<CmsViewModel>(cmsVersion));
        }

        [HttpGet]
        public ActionResult SessionTimeout(Guid? token)
        {
            GuarantorToken guarantorToken = null;

            if (token.HasValue && token.Value != default(Guid))
            {
                guarantorToken = new GuarantorToken(token.Value);
            }

            if (guarantorToken != null)
            {
                string paymentUnitSourceSystemKey = guarantorToken.PaymentUnitSourceSystemKey;
                int paymentUnitAuthenticationId = guarantorToken.PaymentUnitAutheticationId;

                this._guestPayJournalEventApplicationService.Value.LogSessionTimeout(Mapper.Map<JournalEventHttpContextDto>(this.HttpContext), paymentUnitAuthenticationId, paymentUnitSourceSystemKey);
            }

            this.SessionFacade.Value.ClearSession();

            return this.Redirect(TimeoutUrl);
        }

        [HttpPost]
        [Common.Web.Attributes.SessionState(SessionStateBehavior.Required)]
        public void HideBuildBanner()
        {
            if (this.ShowBuildBanner)
            {
                this._webGuestPaySessionFacade.Value.SetSessionValue(HideBuildBannerKey, true);
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [GuestPayLockout]
        public void InvalidCreditCard(string errorMessage, bool isTimeout, Guid tokenGuid)
        {
            if (errorMessage.IsNotNullOrEmpty() && !isTimeout)
            {
                string upper = errorMessage.ToUpper();
                if (upper.Contains(SecurePanConstants.ErrorMessagePartialInvalidCardNumber) ||
                    upper.Contains(SecurePanConstants.ErrorMessagePartialInvalidCardNumber19) ||
                    upper.Contains(SecurePanConstants.ErrorMessagePartialIncorrectInformation))
                {
                    this.MetricsProvider.Value.Increment(Metrics.Increment.SecurePan.PaymentMethod.FailedValidation);
                }
                else if (upper.Contains(SecurePanConstants.ErrorMessagePartialUnableToProcess))
                {
                    this.MetricsProvider.Value.Increment(Metrics.Increment.SecurePan.PaymentMethod.CallFailed);
                }
                else if (upper.Contains(SecurePanConstants.ErrorMessagePartialAvsFailed))
                {
                    this.MetricsProvider.Value.Increment(Metrics.Increment.SecurePan.PaymentMethod.AvsFailed);
                }
            }

            if (isTimeout)
            {
                this.MetricsProvider.Value.Increment(Metrics.Increment.SecurePan.GatewayDown);
            }

            GuarantorToken token = new GuarantorToken(tokenGuid);

            this._guestPayJournalEventApplicationService.Value.LogPaymentMethodFailure(Mapper.Map<JournalEventHttpContextDto>(this.HttpContext), token.PaymentUnitAutheticationId, token.PaymentUnitSourceSystemKey, token.GuarantorLastName);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [GuestPayLockout]
        public string GeneratePanUrl()
        {
            return SecurePanAuthorization.GenerateUrl(this.ApplicationSettingsService.Value);
        }

        [HttpGet]
        public PartialViewResult SecurePan()
        {
            return this.PartialView("_SecurePan");
        }
    }
}
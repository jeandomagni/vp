﻿namespace Ivh.Web.GuestPay.Controllers
{
    using System;
    using System.Web.Mvc;
    using Application.Content.Common.Interfaces;
    using AutoMapper;
    using Common.Base.Enums;
    using Common.VisitPay.Enums;
    using Common.Web.Cookies;
    using Common.Web.Filters;
    using Common.Web.Interfaces;
    using Common.Web.Models.Shared;

    public class AccountController : BaseController
    {
        private readonly Lazy<IContentApplicationService> _contentApplicationService;
        private readonly Lazy<ILocalizationCookieFacade> LocalizationCookieFacade;

        public AccountController(
            Lazy<IBaseControllerService> baseControllerService,
            Lazy<IContentApplicationService> contentApplicationService,
            Lazy<ILocalizationCookieFacade> localizationCookieFacade)
            : base(baseControllerService)
        {
            this._contentApplicationService = contentApplicationService;
            this.LocalizationCookieFacade = localizationCookieFacade;
        }

        [SkipRestrictedCountryCheck]
        [HttpGet]
        public ActionResult ContactUs(bool countryRestricted = false)
        {
            return this.View(countryRestricted ? "ContactUsBlocked" : "ContactUs",Mapper.Map<CmsViewModel>(this._contentApplicationService.Value.GetCurrentVersionAsync(CmsRegionEnum.GuestPayContactUs).Result));
        }

        [AllowAnonymous]
        [HttpGet]
        public JsonResult ChangeLocale(string locale)
        {
            //Update the cookie with the locale
            this.LocalizationCookieFacade.Value.SetLocale(locale, explicitlySetByUser: true);

            return this.Json(true, JsonRequestBehavior.AllowGet);
        }
    }
}
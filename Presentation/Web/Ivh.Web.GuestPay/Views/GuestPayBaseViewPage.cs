﻿namespace Ivh.Web.GuestPay.Views
{
    using Common.Web.Views;

    public class GuestPayBaseViewPage<T> : BaseViewPage<T>
    {
    }

    public class GuestPayBaseViewPage : GuestPayBaseViewPage<dynamic>
    {
    }
}
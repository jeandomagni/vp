﻿namespace Ivh.Web.GuestPay
{
    using System;
    using System.Web.Http;
    using System.Web.Mvc;
    using System.Web.Optimization;
    using System.Web.Routing;
    using Autofac;
    using Common.DependencyInjection;
    using Common.ServiceBus.Constants;
    using Common.Web;
    using Common.Web.Administration;
    using MassTransit;

    public class MvcApplication : MvcApplicationBase
    {
        public MvcApplication() : base("GuestPay")
        {
        }

        protected override void Application_Start()
        {
            base.Application_Start();

            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            WebApiConfig.Register(GlobalConfiguration.Configuration);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            ApplicationHost.ConfigureStartUp();
        }

        protected override void Application_End(object sender, EventArgs e)
        {
            try
            {
                IBusControl serviceBus = IvinciContainer.Instance.Container().Resolve<IBusControl>();
                if (serviceBus != null)
                {
                    serviceBus?.Stop(TimeSpan.FromSeconds(ServiceBus.StopTimeoutSeconds));
                    IvinciContainer.Instance.Container().Dispose();
                }
            }
            catch
            {
            }
            finally
            {
                base.Application_End(sender, e);
            }
        }
    }
}
﻿namespace Ivh.Web.GuestPay
{
    using System;
    using System.Web.Helpers;
    using Application.Logging.Common.Interfaces;
    using Autofac;
    using Common.Base.Constants;
    using Common.DependencyInjection;
    using Common.Web.Cookies;
    using Domain.Logging.Interfaces;
    using Owin;

    public partial class Startup
    {
        public void ConfigureMisc(IAppBuilder app)
        {
            ILoggingApplicationService logger = IvinciContainer.Instance.Container().Resolve<ILoggingApplicationService>();
            logger.Info(() => $"Startup.Misc::ConfigureMisc<IAppBuilder> - Start:  {DateTime.UtcNow}");
            // for future use
            AntiForgeryConfig.CookieName = CookieNames.GuestPay.AntiForgeryString;
            logger.Info(() => $"Startup.Misc::ConfigureMisc<IAppBuilder> - End:  {DateTime.UtcNow}");
        }
    }
}
﻿namespace Ivh.Web.GuestPay
{
    using System.Web.Mvc;
    using System.Web.Routing;

    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");
            routes.IgnoreRoute("{*scss}", new { scss = @".*\.scss(/.*)?" });
            routes.IgnoreRoute("content/client/{*image}", new { image = @".*\.(png|gif|jpg|jpeg|pdf)(/.)?" });
            routes.IgnoreRoute("ping");

            routes.MapRoute("Default", "{controller}/{action}/{id}", new
            {
                controller = "Payment",
                action = "Index",
                id = UrlParameter.Optional
            });
        }
    }
}
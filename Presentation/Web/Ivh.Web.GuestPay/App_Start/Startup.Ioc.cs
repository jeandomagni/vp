﻿namespace Ivh.Web.GuestPay
{
    using System;
    using System.Threading.Tasks;
    using System.Web.Http;
    using System.Web.Mvc;
    using Application.Logging.Common.Interfaces;
    using Autofac;
    using Autofac.Integration.WebApi;
    using Common.Api.Client;
    using Common.DependencyInjection;
    using Common.DependencyInjection.Registration;
    using Common.Web.Extensions;
    using Domain.Settings.Interfaces;
    using IocSetup;
    using Mappings;
    using Owin;

    [System.Runtime.InteropServices.GuidAttribute("8C8EBF52-DAF8-426D-A96C-E2DB35DFD2B5")]
    public partial class Startup
    {
        public void ConfigureIoc(IAppBuilder app)
        {
            Task mappingTasks = Task.Run(() =>
            {
                ServiceBusMessageSerializationMappingBase.ConfigureMappings();

                using (MappingBuilder mappingBuilder = new MappingBuilder())
                {
                    mappingBuilder
                        .WithBase()
                        .WithProfile<GuestPayMappingProfile>();
                }
            });

            IvinciContainer.Instance.RegisterComponent(new RegisterGuestPayMvc(app));

            DependencyResolver.SetResolver(IvinciContainer.Instance.GetResolver());

            ILoggingApplicationService logger = IvinciContainer.Instance.Container().Resolve<ILoggingApplicationService>();
            logger.Info(() => $"Startup.Ioc::ConfigureIoc<IAppBuilder> - Start:  {DateTime.UtcNow}");

            app.UseAutofacMiddleware(IvinciContainer.Instance.Container());
            app.UseAutofacMvc();

            HttpConfiguration config = GlobalConfiguration.Configuration;
            config.DependencyResolver = new AutofacWebApiDependencyResolver(IvinciContainer.Instance.Container());

            //Configure and add api key authorization handler for RemotePay controllers
            IApplicationSettingsService applicationSettingsService = IvinciContainer.Instance.Container().Resolve<IApplicationSettingsService>();
            config.MessageHandlers.Add(new SimpleApiKeyHandler(applicationSettingsService.GuestPayRemotePayApiKey.Value));

            mappingTasks.Wait();
            logger.Info(() => $"Startup.Ioc::ConfigureIoc<IAppBuilder> - End:  {DateTime.UtcNow}");
        }
    }
}
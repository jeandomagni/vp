﻿using Ivh.Web.GuestPay;
using Microsoft.Owin;

[assembly: OwinStartup(typeof (Startup))]

namespace Ivh.Web.GuestPay
{
    using Owin;

    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            this.ConfigureIoc(app);
            this.ConfigureMisc(app);
        }
    }
}
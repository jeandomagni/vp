﻿namespace Ivh.Web.GuestPay
{
    using System.Web.Optimization;

    public class BundleConfig
    {
        public static void RegisterBundles(BundleCollection bundles)
        {
            #region base

            bundles.Add(new ScriptBundle("~/bundles/scripts/base").Include(
                "~/Scripts/Libraries/jquery-3.2.1.js",
                "~/Scripts/Libraries/jquery.validate.js",
                "~/Scripts/Libraries/jquery.validate.unobtrusive.js",
                "~/Scripts/Libraries/knockout-3.4.2.js",
                "~/Scripts/Libraries/knockout.mapping-latest.js",
                "~/Content/Bootstrap/scripts/bootstrap.js",
                "~/Scripts/Libraries/autoNumeric/autoNumeric.js",
                "~/Scripts/Libraries/inputMask/inputmask.js",
                "~/Scripts/Libraries/inputMask/jquery.inputmask.js",
                "~/Scripts/Libraries/jquery.blockUI.js",
                "~/Scripts/Libraries/he.js"));
            
            bundles.Add(new ScriptBundle("~/bundles/scripts/vpbase").Include(
                "~/Scripts/Shared/global.js",
                "~/Scripts/Shared/common.visitpay.js",
                "~/Scripts/Shared/common.knockout.js",
                "~/Scripts/Shared/validationAdapters.js"));

            bundles.Add(new ScriptBundle("~/bundles/scripts/idletimer").Include());

            #endregion

            bundles.Add(new ScriptBundle("~/bundles/scripts/payment").Include(
                "~/Scripts/Shared/idleTimer.js",
                "~/Scripts/Payment/secure-pan.js",
                "~/Scripts/Payment/guarantor-match.js",
                "~/Scripts/Payment/payment-submit.js",
                "~/Scripts/Payment/payment.js"));

            bundles.Add(new ScriptBundle("~/bundles/scripts/statementlookup").Include(
                "~/Scripts/Libraries/typeahead.bundle.js",
                "~/Scripts/Libraries/typeahead.jquery.js",
                "~/Scripts/Libraries/bloodhound.js",
                "~/Scripts/Payment/statement-lookup.js"));

            bundles.Add(new ScriptBundle("~/bundles/scripts/receipt").Include(
                "~/Scripts/Shared/idleTimer.js",
                "~/Scripts/Payment/receipt.js"));
        }
    }
}
﻿namespace Ivh.Web.GuestPay.Models.Payment
{
    using Common.Web.Models;

    public class GuarantorMatchErrorViewModel : ResultMessage
    {
        public GuarantorMatchErrorViewModel(bool result, string message, bool isLocked = false) : base(result, message)
        {
            this.IsLocked = isLocked;
        }

        public bool IsLocked { get; set; }
    }
}
﻿namespace Ivh.Web.GuestPay.Models.Payment
{
    using System.ComponentModel.DataAnnotations;
    using Common.Base.Constants;
    using Common.Base.Enums;
    using Common.VisitPay.Constants;
    using Common.VisitPay.Enums;
    using Common.Web.Attributes;

    public class CardAccountViewModel
    {
        public CardAccountViewModel()
        {
            this.IsCountryUsa = true;
        }
        
        [LocalizedDisplayName(TextRegionConstants.GuestPayNameOnCard)]
        [LocalizedPrompt(TextRegionConstants.GuestPayNameOnCard)]
        [Required(ErrorMessage = "This field is required.")]
        [MaxLength(50, ErrorMessage = "Must be 1-50 characters.")]
        public string CardAccountHolderName { get; set; }
        
        [LocalizedDisplayName(TextRegionConstants.GuestPayCardNumber)]
        [LocalizedPrompt(TextRegionConstants.GuestPayCreditCardNumber)]
        public string BillingId { get; set; }

        public string GatewayToken { get; set; }
        public string LastFour { get; set; }
        
        [LocalizedDisplayName(TextRegionConstants.GuestPaySecurityCodeCvv)]
        [LocalizedPrompt(TextRegionConstants.GuestPayCvv)]
        [Required(ErrorMessage = "This field is required.")]
        [Range(typeof(string), "0", "9999", ErrorMessage = "This field is invalid.")]
        public string CardSecurityCode { get; set; }
        
        [LocalizedDisplayName(TextRegionConstants.GuestPayExpirationDate)]
        [Required(ErrorMessage = "This field is required.")]
        public int ExpirationDateM { get; set; }

        [Required(ErrorMessage = "This field is required.")]
        public int ExpirationDateY { get; set; }
        
        [LocalizedDisplayName(TextRegionConstants.GuestPayCardNumber)]
        [LocalizedPrompt(TextRegionConstants.GuestPayCreditCardNumber)]
        [Required(ErrorMessage = "This field is required.")]
        public PaymentMethodTypeEnum? PaymentMethodType { get; set; }
        
        [LocalizedDisplayName(TextRegionConstants.GuestPayBillingAddress)]
        [LocalizedPrompt(TextRegionConstants.GuestPayAddressLine1)]
        [Required(ErrorMessage = "This field is required.")]
        public string AddressStreet1 { get; set; }

        [LocalizedPrompt(TextRegionConstants.GuestPayAddressLine2)]
        public string AddressStreet2 { get; set; }

        [LocalizedPrompt(TextRegionConstants.GuestPayAddressCity)]
        [Required(ErrorMessage = "This field is required.")]
        public string City { get; set; }

        [RequiredIf("IsCountryUsa", true, ErrorMessage = "This field is required.")]
        public string State { get; set; }

        [LocalizedPrompt(TextRegionConstants.GuestPayStateProvinceRegion)]
        [RequiredIf("IsCountryUsa", false, ErrorMessage = "This field is required.")]
        public string StateProvinceRegion { get; set; }

        [LocalizedPrompt(TextRegionConstants.GuestPayAddressZip)]
        [RequiredIf("IsCountryUsa", true, ErrorMessage = "Zip is required.")]
        [RegularExpressionZipCode(TextRegionConstants.GuestPayInvalidZip)]
        public string Zip { get; set; }

        [LocalizedPrompt(TextRegionConstants.GuestPayPostalCode)]
        public string PostalCode { get; set; }

        [Display(Prompt = "")] //Empty string doesn't need to be localized
        public bool IsCountryUsa { get; set; }
    }
}
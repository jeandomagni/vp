﻿namespace Ivh.Web.GuestPay.Models.Payment
{
    using Ivh.Common.Base.Utilities.Helpers;
    using System;

    public class ReceiptViewModel
    {
        public string Date { get; set; }
        public string Time { get; set; }
        public string GuarantorNumber { get; set; }
        public string GuarantorFirstName { get; set; }
        public string GuarantorLastName { get; set; }
        public string EmailAddress { get; set; }
        public string LastFour { get; set; }
        public string PaymentAccountHolderName { get; set; }
        public string PaymentAmount { get; set; }
        public string PaymentMethodType { get; set; }
        public string TransactionId { get; set; }
        public Guid Token { get; set; }
        public string TimeoutUrl { get; set; }
        public string GuarantorLastNameFirstName => FormatHelper.LastNameFirstName(this.GuarantorLastName, this.GuarantorFirstName);
        public string GuarantorFirstNameLastName => FormatHelper.FirstNameLastName(this.GuarantorFirstName, this.GuarantorLastName);
    }
}
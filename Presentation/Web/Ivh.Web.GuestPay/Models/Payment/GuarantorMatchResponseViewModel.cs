﻿namespace Ivh.Web.GuestPay.Models.Payment
{
    using System;

    public class GuarantorMatchResponseViewModel
    {
        public Guid Guarantor { get; set; }
        public decimal Balance { get; set; }
        public bool DisplayCurrentBalance { get; set; }
        public bool HasCurrentBalance => this.Balance > 0m;
    }
}
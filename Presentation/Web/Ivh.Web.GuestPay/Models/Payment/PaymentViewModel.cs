﻿namespace Ivh.Web.GuestPay.Models.Payment
{
    public class PaymentViewModel
    {
        public PaymentViewModel()
        {
            this.PaymentSubmitViewModel = new PaymentSubmitViewModel();
            this.GuarantorMatchRequestViewModel = new GuarantorMatchRequestViewModel();
        }

        public GuarantorMatchRequestViewModel GuarantorMatchRequestViewModel { get; set; }

        public PaymentSubmitViewModel PaymentSubmitViewModel { get; set; }
    }
}
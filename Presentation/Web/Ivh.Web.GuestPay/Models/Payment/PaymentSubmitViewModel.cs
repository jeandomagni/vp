﻿namespace Ivh.Web.GuestPay.Models.Payment
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.Globalization;
    using System.Web.Mvc;
    using Common.Base.Constants;
    using Common.Base.Enums;
    using Common.VisitPay.Constants;
    using Common.VisitPay.Enums;
    using Common.Web.Attributes;

    public class PaymentSubmitViewModel
    {
        public PaymentSubmitViewModel()
        {
            this.PaymentMethodParentTypes = new List<SelectListItem>();
        }

        [Display(Prompt = "0.00")] //Doesn't need to be localized
        [LocalizedDisplayName(TextRegionConstants.GuestPayPaymentAmount)]
        [Required(ErrorMessage = "This field is required.")]
        public decimal PaymentAmount { get; set; }
        
        [LocalizedDisplayName(TextRegionConstants.GuestPayPaymentType)]
        [Required(ErrorMessage = "This field is required.")]
        public PaymentMethodParentTypeEnum PaymentMethodParentType { get; set; }

        [LocalizedDisplayName(TextRegionConstants.GuestPayEmailAddress)]
        [LocalizedPrompt(TextRegionConstants.GuestPayEmailAddress)]
        [Required(ErrorMessage = "This field is required."), RegularExpressionEmailAddress(TextRegionConstants.GuestPayEmailInvalid)]
        public string EmailAddress { get; set; }
        
        [LocalizedDisplayName(TextRegionConstants.GuestPayConfirmEmailAddress)]
        [LocalizedPrompt(TextRegionConstants.GuestPayConfirmEmailAddress)]
        [Required(ErrorMessage = "This field is required."), System.ComponentModel.DataAnnotations.Compare("EmailAddress", ErrorMessage = "E-mails do not match.")]
        public string EmailAddressConfirm { get; set; }

        public string SourceSystemKey { get; set; }

        public BankAccountViewModel BankAccount { get; set; }

        public CardAccountViewModel CardAccount { get; set; }

        public GuarantorMatchResponseViewModel Guarantor { get; set; }

        public IList<SelectListItem> PaymentMethodParentTypes { get; set; }

        public IList<SelectListItem> Months
        {
            get
            {
                List<SelectListItem> items = new List<SelectListItem>();
                for (int i = 1; i <= 12; i++)
                {
                    items.Add(new SelectListItem
                    {
                        Text = $"{i.ToString().PadLeft(2, '0')} - {CultureInfo.CurrentUICulture.DateTimeFormat.GetMonthName(i)}",
                        Value = i.ToString()
                    });
                }

                return items;
            }
        }

        public IList<SelectListItem> Years
        {
            get
            {
                List<SelectListItem> items = new List<SelectListItem>();
                for (int i = DateTime.UtcNow.Year; i <= DateTime.UtcNow.Year + 10; i++)
                {
                    items.Add(new SelectListItem
                    {
                        Text = i.ToString(),
                        Value = i.ToString().Substring(2, 2)
                    });
                }

                return items;
            }
        }
    }
}
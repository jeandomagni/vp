﻿namespace Ivh.Web.GuestPay.Models.Payment
{
    using System.Collections.Generic;
    using Application.GuestPay.Common.Dtos;
    using Common.Base.Constants;
    using Common.VisitPay.Constants;
    using Common.Web.Attributes;

    public class GuarantorMatchRequestViewModel
    {
        public GuarantorMatchRequestViewModel()
        {
            this.GuarantorAuthenticationFields = new List<AuthenticationMatchFieldDto>();
            this.GuarantorAuthenticationValues = new Dictionary<string, string>();
        }

        [RequireChecked(ErrorMessage = TextRegionConstants.GuestPayTermsOfUseMustAccept)]
        public bool TermsAccepted { get; set; }

        public IList<AuthenticationMatchFieldDto> GuarantorAuthenticationFields { get; set; }

        public IDictionary<string, string> GuarantorAuthenticationValues { get; set; }
    }
}
﻿namespace Ivh.Web.GuestPay.Models.Payment
{
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.Web.Mvc;
    using Common.Base.Enums;
    using Common.VisitPay.Constants;
    using Common.VisitPay.Enums;
    using Ivh.Common.Base.Constants;
    using Ivh.Common.Base.Utilities.Helpers;
    using Ivh.Common.Web.Attributes;

    public class BankAccountViewModel
    {
        public BankAccountViewModel()
        {
            this.IsCountryUsa = true;
        }
        
        [LocalizedDisplayName(TextRegionConstants.GuestPayAccountNumber)]
        [LocalizedPrompt(TextRegionConstants.GuestPayAccountNumber)]
        [Required(ErrorMessage = "This field is required.")]
        [MinLength(4, ErrorMessage = "This field is invalid.")]
        [MaxLength(17, ErrorMessage = "This field is invalid.")]
        public string AccountNumber { get; set; }

        public IList<SelectListItem> BankAccountTypes { get; set; }

        [LocalizedDisplayName(TextRegionConstants.GuestPayBankName)]
        [LocalizedPrompt(TextRegionConstants.GuestPayBankName)]
        [Required(ErrorMessage = "This field is required.")]
        public string BankName { get; set; }
        
        [LocalizedDisplayName(TextRegionConstants.GuestPayConfirmAccountNumber)]
        [LocalizedPrompt(TextRegionConstants.GuestPayConfirmAccountNumber)]
        [Required(ErrorMessage = "This field is required."), System.ComponentModel.DataAnnotations.Compare("AccountNumber", ErrorMessage = "Account Numbers do not match.")]
        public string ConfirmAccountNumber { get; set; }
        
        [LocalizedDisplayName(TextRegionConstants.GuestPayNameFirst)]
        [LocalizedPrompt(TextRegionConstants.GuestPayNameFirst)]
        [Required(ErrorMessage = "This field is required.")]
        public string FirstName { get; set; }
        
        [LocalizedDisplayName(TextRegionConstants.GuestPayNameLast)]
        [LocalizedPrompt(TextRegionConstants.GuestPayNameLast)]
        [Required(ErrorMessage = "This field is required.")]
        public string LastName { get; set; }
        
        [LocalizedDisplayName(TextRegionConstants.GuestPayAccountType)]
        [Required(ErrorMessage = "This field is required.")]
        public PaymentMethodTypeEnum PaymentMethodType { get; set; }
        
        [LocalizedDisplayName(TextRegionConstants.GuestPayRoutingNumber)]
        [LocalizedPrompt(TextRegionConstants.GuestPayRoutingNumber)]
        [Required(ErrorMessage = "This field is required.")]
        [MinLength(9, ErrorMessage = "This field is invalid.")]
        [MaxLength(9, ErrorMessage = "This field is invalid.")]
        public string RoutingNumber { get; set; }

        [Display(Prompt = "")] //Empty string doesn't need to be localized
        public bool IsCountryUsa { get; set; }

        public string LastNameFirstName => FormatHelper.LastNameFirstName(this.LastName, this.FirstName);

        public string FirstNameLastName => FormatHelper.FirstNameLastName(this.FirstName, this.LastName);
    }
}
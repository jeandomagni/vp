﻿namespace Ivh.Web.GuestPay.Models.RemotePayment
{
    using Common.Base.Enums;
    using Common.VisitPay.Enums;

    public class PaymentMatchUnitResponse
    {
        /// <summary>
        /// The payment authentication identifier required to make a payment 
        /// </summary>
        public int PaymentUnitAuthenticationId { get; set; }
        /// <summary>
        /// Indicates whether the guarantor identifier is valid
        /// </summary>
        public bool IsValid { get; set; }
        /// <summary>
        /// The status code received from the payment application
        /// </summary>
        public PaymentMatchUnitResponseStatusEnum StatusCode { get; set; }
        /// <summary>
        /// Balance due from the guarantor
        /// </summary>
        public decimal Balance { get; set; }
    }
}
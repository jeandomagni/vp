﻿namespace Ivh.Web.GuestPay.Models.RemotePayment
{
    using System.ComponentModel.DataAnnotations;
    using Common.Base.Enums;
    using Common.VisitPay.Enums;

    /// <summary>
    /// RemotePaymentInfo
    /// </summary>
    public class RemotePaymentInfo
    {
        /// <summary>
        /// Identifier of the Guarantor from the Hospital's system
        /// </summary>
        [Required]
        public string PaymentUnitSourceSystemKey { get; set; }

        /// <summary>
        /// Authentication identifier of the guarantor
        /// </summary>
        [Required]
        public int PaymentUnitAuthenticationId { get; set; }

        /// <summary>
        /// Amount of the payment
        /// </summary>
        [Required]
        public decimal PaymentAmount { get; set; }

        /// <summary>
        /// Type of the payment method
        /// </summary>
        [Required]
        public PaymentMethodTypeEnum PaymentMethodType { get; set; }

        /// <summary>
        /// Identifier used by the payment provider that references the payment method
        /// </summary>
        [Required]
        public string BillingId { get; set; }

        /// <summary>
        /// Last four digits of the credit card account number
        /// </summary>
        [Required]
        public string LastFour { get; set; }

        /// <summary>
        /// Expiration date of the credit card account in MM/YYYY format
        /// </summary>
        [Required]
        public string ExpDate { get; set; }

        /// <summary>
        /// Name displayed on the physical credit card
        /// </summary>
        public string NameOnCard { get; set; }

        /// <summary>
        /// IP Address of the original requester (when relayed from another API)
        /// </summary>
        public string OriginalRequestIpAddress { get; set; }
    }
}
﻿namespace Ivh.Web.GuestPay.Models.RemotePayment
{
    using Common.Base.Enums;
    using Common.VisitPay.Enums;

    public class RemotePaymentResponse
    {
        /// <summary>
        /// Transaction identifier returned by the payment processor
        /// </summary>
        public string TransactionId { get; set; }

        /// <summary>
        /// Amount of payment processed, should match requested amount
        /// </summary>
        public decimal Amount { get; set; }

        /// <summary>
        /// Authorization code received from the payment processor
        /// </summary>
        public string AuthCode { get; set; }

        /// <summary>
        /// The status code received from the payment application
        /// </summary>
        public ProcessPaymentResponseStatusEnum StatusCode { get; set; }

        /// <summary>
        /// Indicates if the payment was successful
        /// </summary>
        public bool IsSuccess => this.StatusCode == ProcessPaymentResponseStatusEnum.Success;
    }
}
﻿namespace Ivh.Web.GuestPay.GpCommon
{
    using System;
    using System.Collections.Generic;
    using Common.Session.Obfuscation;

    public class GuarantorToken
    {
        private const string Delimiter = "|";
        private GenericObfuscator<string> _obfuscator;

        public GuarantorToken()
        {
        }

        public GuarantorToken(Guid obfuscated)
        {
            string clarified = this.Obfuscator.Clarify(obfuscated);
            if (string.IsNullOrEmpty(clarified))
            {
                return;
            }

            string[] split = clarified.Split(Delimiter.ToCharArray());

            int paymentUnitId;
            if (!int.TryParse(split[0], out paymentUnitId))
            {
                return;
            }

            this.PaymentUnitAutheticationId = paymentUnitId;
            this.PaymentUnitSourceSystemKey = split.Length > 1 ? split[1] : null;
            this.GuarantorLastName = split.Length > 2 ? split[2] : null;
            this.PaymentId = split.Length > 3 ? Convert.ToInt32(split[3]) : (int?) null;
        }

        private GenericObfuscator<string> Obfuscator
        {
            get { return this._obfuscator ?? (this._obfuscator = new GenericObfuscator<string>()); }
        }

        public string PaymentUnitSourceSystemKey { get; private set; }
        public int? PaymentId { get; private set; }
        public int PaymentUnitAutheticationId { get; private set; }
        public string GuarantorLastName { get; private set; }

        public Guid Obfuscate(int paymentUnitId, string paymentUnitSourceSystemKey, string guarantorLastName, int? paymentId = null)
        {
            IList<string> values = new List<string>
            {
                paymentUnitId.ToString(),
                paymentUnitSourceSystemKey,
                guarantorLastName
            };

            if (paymentId.HasValue)
            {
                values.Add(paymentId.Value.ToString());
            }

            return this.Obfuscator.Obfuscate(string.Join(Delimiter, values));
        }
    }
}
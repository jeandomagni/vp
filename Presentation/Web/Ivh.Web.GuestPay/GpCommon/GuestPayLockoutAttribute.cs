﻿namespace Ivh.Web.GuestPay.GpCommon
{
    using System.Web.Mvc;
    using Common.Web.Cookies;
    using Controllers;

    public class GuestPayLockoutAttribute : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            IGuestPayCookieFacade cookieFacade = DependencyResolver.Current.GetService<IGuestPayCookieFacade>();

            BaseController controller = filterContext.Controller as BaseController;
            
            if (controller != null && cookieFacade.AuthenticationAttempts >= controller.ClientDto.GuestPayMaxFailedAttemptCount)
            {
                filterContext.Result = new RedirectResult(controller.Url.Action("SessionLocked", "Payment"));
            }

            base.OnActionExecuting(filterContext);
        }
    }
}
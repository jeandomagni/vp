﻿namespace Ivh.Web.GuestPay.Mappings
{
    using System;
    using System.Web;
    using System.Web.Mvc;
    using Application.Content.Common.Dtos;
    using Application.GuestPay.Common.Dtos;
    using Application.Monitoring.Common.Dtos;
    using AutoMapper;
    using Common.Base.Utilities.Extensions;
    using Common.EventJournal;
    using Common.VisitPay.Enums;
    using Common.VisitPay.Strings;
    using Common.Web.Mapping;
    using Common.Web.Models.Monitoring;
    using Common.Web.Models.Shared;
    using Common.Web.Utilities;
    using Models.Payment;
    using Models.RemotePayment;

    public class GuestPayMappingProfile : Profile
    {
        public GuestPayMappingProfile()
        {
            this.CreateMap<GuarantorMatchRequestViewModel, PaymentUnitMatchDto>()
                .AfterMap((src, dest) =>
                {
                    Func<string, string> getValue = (k) =>
                    {
                        string value;
                        if (src.GuarantorAuthenticationValues.TryGetValue(k, out value))
                        {
                            return value.Trim();
                        }

                        return null;
                    };

                    dest.GuarantorFirstName = getValue("GuarantorFirstName");
                    dest.GuarantorLastName = getValue("GuarantorLastName");
                    dest.SourceSystemKey = getValue("PaymentUnitSourceSystemKey");
                    dest.StatementIdentifierId = getValue("StatementIdentifierId");
                });

            this.CreateMap<PaymentSubmitViewModel, PaymentSubmitDto>()
                .ForMember(dest => dest.PaymentAmount, opts => opts.MapFrom(src => src.PaymentAmount))
                .ForMember(dest => dest.EmailAddress, opts => opts.MapFrom(src => src.EmailAddress))
                .AfterMap((src, dest) =>
                {
                    switch (src.PaymentMethodParentType)
                    {
                        case PaymentMethodParentTypeEnum.Ach:
                            dest.BankName = src.BankAccount.BankName;
                            dest.BankAccountNumber = src.BankAccount.AccountNumber;
                            dest.BankRoutingNumber = src.BankAccount.RoutingNumber;
                            dest.BankAccountHolderFirstName = src.BankAccount.FirstName;
                            dest.BankAccountHolderLastName = src.BankAccount.LastName;
                            dest.PaymentMethodType = src.BankAccount.PaymentMethodType;
                            dest.IsCountryUsa = src.BankAccount.IsCountryUsa;
                            break;
                        case PaymentMethodParentTypeEnum.CreditDebitCard:
                            dest.AddressStreet1 = src.CardAccount.AddressStreet1;
                            dest.AddressStreet2 = src.CardAccount.AddressStreet2;
                            dest.CardAccountHolderName = src.CardAccount.CardAccountHolderName;
                            dest.BillingId = src.CardAccount.BillingId;
                            dest.GatewayToken = src.CardAccount.GatewayToken;
                            dest.LastFour = src.CardAccount.LastFour;
                            dest.City = src.CardAccount.City;
                            dest.ExpDateM = src.CardAccount.ExpirationDateM;
                            dest.ExpDateY = src.CardAccount.ExpirationDateY;
                            dest.PaymentMethodType = src.CardAccount.PaymentMethodType ?? PaymentMethodTypeEnum.Visa;
                            dest.SecurityCode = src.CardAccount.CardSecurityCode;
                            if (src.CardAccount.IsCountryUsa)
                            {
                                dest.State = src.CardAccount.State;
                                dest.Zip = src.CardAccount.Zip;
                            }
                            else
                            {
                                dest.State = src.CardAccount.StateProvinceRegion;
                                dest.Zip = src.CardAccount.PostalCode;
                            }

                            dest.IsCountryUsa = src.CardAccount.IsCountryUsa;
                            break;
                    }
                });

            this.CreateMap<PaymentDto, ReceiptViewModel>()
                .ForMember(dest => dest.Date, opts => opts.MapFrom(src => MappingHelper.MapToClientDateText(src.PaymentDate)))
                .ForMember(dest => dest.Time, opts => opts.MapFrom(src => MappingHelper.MapToFormattedClientDateTimeText(src.PaymentDate, Format.TimeFormat)))
                .ForMember(dest => dest.EmailAddress, opts => opts.MapFrom(src => src.EmailAddress))
                .ForMember(dest => dest.GuarantorFirstName, opts => opts.MapFrom(src => src.PaymentUnitAuthentication.GuarantorFirstName))
                .ForMember(dest => dest.GuarantorLastName, opts => opts.MapFrom(src => src.GuarantorLastName))
                .ForMember(dest => dest.GuarantorNumber, opts => opts.MapFrom(src => src.GuarantorNumber))
                .ForMember(dest => dest.PaymentAmount, opts => opts.MapFrom(src => src.PaymentAmount.FormatCurrency()))
                .ForMember(dest => dest.PaymentMethodType, opts => opts.MapFrom(src => src.PaymentMethodType.GetDescription()))
                .ForMember(dest => dest.TransactionId, opts => opts.MapFrom(src => src.TransactionId));

            this.CreateMap<PaymentMethodParentTypeEnum, SelectListItem>()
                .ForMember(dest => dest.Text, opts => opts.MapFrom(src => LocalizationHelper.GetLocalizedString(src.GetDescription())))
                .ForMember(dest => dest.Value, opts => opts.MapFrom(src => (int) src));

            this.CreateMap<CmsVersionDto, CmsViewModel>();

            this.CreateMap<ContentMenuDto, ContentMenuViewModel>();
            this.CreateMap<ContentMenuItemDto, ContentMenuItemViewModel>();

            this.CreateMap<SystemHealthDetailsDto, SystemHealthViewModel>();
            this.CreateMap<SystemInfoDetailsDto, SystemInfoViewModel>();
            this.CreateMap<HttpContextWrapper, JournalEventHttpContextDto>();
            this.CreateMap<RemotePaymentInfo, RemotePaymentInfoDto>();
            this.CreateMap<PaymentMatchUnitResponseDto, PaymentMatchUnitResponse>();

            this.CreateMap<ProcessPaymentResponseDto, RemotePaymentResponse>()
                .ForMember(dest => dest.Amount, opts => opts.MapFrom(src => src.Payment == null ? 0m : src.Payment.PaymentAmount))
                .ForMember(dest => dest.AuthCode, opts => opts.MapFrom(src => src.Payment == null ? string.Empty : src.Payment.AuthCode))
                .ForMember(dest => dest.TransactionId, opts => opts.MapFrom(src => src.Payment == null ? string.Empty : src.Payment.TransactionId))
                .ForMember(dest => dest.StatusCode, opts => opts.MapFrom(src => src.StatusCode));
        }
    }
}
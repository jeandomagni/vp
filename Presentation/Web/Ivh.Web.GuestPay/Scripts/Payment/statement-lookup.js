﻿(function (statementLookup, ko) {

    statementLookup.Main = function (data) {

        var minQueryLength = 0;

        function SearchViewModel() {
            var self = this;

            self.QueryText = ko.observable('');
            self.FoundLiteralMatches = ko.observable(false);
            self.DisplaySearchResults = ko.observable(false);

            return self;
        };

        function initTypeahead(element, dataSources) {
            element.typeahead('destroy');
            element.typeahead({
                hint: true,
                highlight: true,
                minLength: minQueryLength
            },
                dataSources
            );
        }

        function setupTypeahead(model, searchContainer) {

            data.sort(function (a,b) {
                if (a.Label < b.Label) {
                    return -1;
                }
                if (a.Label > b.Label) {
                    return 1;
                }
                return 0;
            });

            var sourceByIdentity = new Bloodhound({
                datumTokenizer: Bloodhound.tokenizers.obj.whitespace('Label'),
                queryTokenizer: Bloodhound.tokenizers.whitespace,
                local: data
            });

            var dataSources =
                [{
                    name: 'searchdata',
                    display: 'Label',
                    limit: 8,
                    source: sourceByIdentity
                }];

            var typeahead = searchContainer.find('.typeahead');
            initTypeahead(typeahead, dataSources);

            typeahead.bind('typeahead:select', function (ev, suggestion) {
                model.FoundLiteralMatches(true);
                $('#StatementIdentifierId').val(suggestion.Key);
            });

            typeahead.bind('typeahead:render', function (ev, data) {
                model.FoundLiteralMatches(false);
            });

            typeahead.bind('focusout', function () {

                var $statementId = $('#StatementIdentifierId');
                
                if (!ko.unwrap(model.FoundLiteralMatches) || $statementId.length < 1) {
                    var query = typeahead.typeahead('val');

                    var searchResults = [];
                    ko.utils.arrayForEach(dataSources, function (dataSource) {
                        dataSource.source.search(query, function (results) {
                            ko.utils.arrayForEach(results, function (result) {
                                searchResults.push(result.Key);
                            });
                        });
                    });

                    if (searchResults.length === 1) {
                        $statementId.val(searchResults[0]);
                    }
                }
                
            });
            
        }

        return {
            init: function (searchContainer) {

                if (searchContainer === null || searchContainer === undefined || searchContainer.length === 0) {
                    return;
                }

                var model = new SearchViewModel();

                ko.applyBindings(model, searchContainer[0]);

                setupTypeahead(model, searchContainer);

                $.validator.setDefaults({ ignore: '.tt-hint' });
            }

        };
    };

})(window.VisitPay.StatementLookup, window.ko);

﻿(function (ko) {
    window.VisitPay.Models.BankAccountViewModel = function () {

        var self = this;

        self.AccountNumber = ko.observable();
        self.AccountNumber.LastFour = ko.computed(function () {
            var accountNumber = ko.unwrap(self.AccountNumber);
            if (accountNumber) {
                accountNumber = accountNumber.trim();
                return accountNumber.length >= 4 ? accountNumber.substr(accountNumber.length - 4, 4) : '';
            }
            return '';
        });
        self.BankName = ko.observable();
        self.ConfirmAccountNumber = ko.observable();
        self.FirstName = ko.observable();
        self.LastName = ko.observable();
        self.RoutingNumber = ko.observable();
        self.PaymentMethodType = ko.observable();

        return self;
    };

    window.VisitPay.Models.CardAccountViewModel = function () {

        var self = this;

        self.GatewayToken = ko.observable();
        self.BillingId = ko.observable();
        self.AvsCode = ko.observable();
        self.LastFour = ko.observable();

        self.ExpirationDateM = ko.observable(new Date().getMonth() + 1);
        self.ExpirationDateY = ko.observable(new Date().getFullYear());
        self.PaymentMethodType = ko.observable();
        self.PaymentMethodType.IsAmex = ko.computed(function () {
            return ko.unwrap(self.PaymentMethodType) === 3;
        });
        self.PaymentMethodType.IsDiscover = ko.computed(function () {
            return ko.unwrap(self.PaymentMethodType) === 4;
        });
        self.PaymentMethodType.IsMastercard = ko.computed(function () {
            return ko.unwrap(self.PaymentMethodType) === 2;
        });
        self.PaymentMethodType.IsVisa = ko.computed(function () {
            return ko.unwrap(self.PaymentMethodType) === 1;
        });
        self.PaymentMethodType.IsValid = ko.computed(function () {
            return self.PaymentMethodType.IsVisa() || self.PaymentMethodType.IsMastercard() || self.PaymentMethodType.IsDiscover() || self.PaymentMethodType.IsAmex();
        });
        self.Countries = ko.observableArray([{ Value: true, Text: (window.VisitPay.Localization.UnitedStates || 'United States') }, { Value: false, Text: (window.VisitPay.Localization.OutsideTheUnitedStates || 'Outside the United States') }]);

        self.CardAccountHolderName = ko.observable();
        self.IsCountryUsa = ko.observable(true);
        self.AddressStreet1 = ko.observable();
        self.AddressStreet2 = ko.observable();
        self.City = ko.observable();
        self.State = ko.observable();
        self.Zip = ko.observable();

        self.CardSecurityCode = ko.observable();

        self.BillingId.DisplayText = ko.computed(function () {
            if (self.PaymentMethodType() === 3)
                return 'American Express';
            if (self.PaymentMethodType() === 4)
                return 'Discover';
            if (self.PaymentMethodType() === 2)
                return 'MasterCard';
            if (self.PaymentMethodType() === 1)
                return 'Visa';
            return '';
        });

        return self;

    };

    window.VisitPay.Models.PaymentSubmitViewModel = function (guarantor) {

        var self = this;
        var confirm = ko.observable();

        self.Guarantor = guarantor;
        self.BankAccount = new window.VisitPay.Models.BankAccountViewModel();
        self.CardAccount = new window.VisitPay.Models.CardAccountViewModel();

        self.EmailAddress = ko.observable();
        self.EmailAddressConfirm = ko.observable();
        self.PaymentAmount = ko.observable();

        self.PaymentMethodParentType = ko.observable();
        self.PaymentMethodParentType.HasPaymentType = ko.computed(function () {
            return self.PaymentMethodParentType() && self.PaymentMethodParentType().length > 0;
        });
        self.PaymentMethodParentType.IsCreditCard = ko.computed(function () {
            return self.PaymentMethodParentType() && parseInt(self.PaymentMethodParentType()) === 1;
        });
        self.PaymentMethodParentType.IsAch = ko.computed(function () {
            return self.PaymentMethodParentType() && parseInt(self.PaymentMethodParentType()) === 2;
        });
        self.PaymentMethodParentType.DisplayText = ko.computed(function () {
            return self.PaymentMethodParentType.IsCreditCard() ? self.CardAccount.BillingId.DisplayText() : (window.VisitPay.Localization.BankAccount || 'Bank Account');
        });
        self.PaymentMethodParentType.LastFour = ko.computed(function () {
            return self.PaymentMethodParentType.IsCreditCard() ? self.CardAccount.LastFour() : self.BankAccount.AccountNumber.LastFour();
        });

        self.fn = {
            ClearSensitiveFields: function () {
                self.BankAccount.AccountNumber('');
                self.BankAccount.ConfirmAccountNumber('');
                self.BankAccount.RoutingNumber('');
                self.CardAccount.CardSecurityCode('');
            },

            IsStep3: ko.computed(function () {
                return ko.unwrap(confirm);
            }),
            IsStep2: ko.computed(function () {
                var guarantor = ko.unwrap(self.Guarantor);
                return !ko.unwrap(confirm) && (guarantor != null && ko.unwrap(guarantor.IsMatch()) && ko.unwrap(guarantor.HasCurrentBalance));
            }),

            SetConfirm: function () {
                confirm(true);
            },
            SetEntry: function () {
                confirm(false);
            },

            toJs: function () {
                var mapping = {
                    'ignore': ['fn', self.PaymentMethodParentType.IsCreditCard() ? 'BankAccount' : 'CardAccount']
                };

                return ko.mapping.toJS(self, mapping);
            }
        };

        return self;

    };

})(window.ko);

window.VisitPay.PaymentSubmit = (function (visitPay, $, ko) {

    var self = {
        ViewModel: {}
    };

    var $scope = $('#section-submitPayment');
    var $securepanframe;

    function resetValidation() {
        visitPay.Common.ResetUnobtrusiveValidation($('#frmPaymentInfo'));
    };

    function setPaymentValidationRules($element) {

        var max = ko.unwrap(self.ViewModel.Guarantor.Balance);

        $element.rules('remove', 'range');
        $element.rules('add', {
            range: [1.00, max],
            messages: {
                range: function (params) {
                    return '$' + params[0].toFixed(2) + ' - $' + params[1].toFixed(2) + ' is required';
                }
            }
        });
    }

    function setNumericInput($elements) {
        $elements.each(function () {
            $(this).autoNumeric('init', { aSep: '', lZero: 'deny' });
            $(this).keydown(function (e) {
                if (e.keyCode === 188) {
                    e.preventDefault();
                }
            });
            $(this).off('focus').focus(function () { $(this).select(); });
        });
    };

    function showError(message) {

        if (message === false) {
            $scope.find('.alert').addClass('hidden');
            return;
        }

        self.ViewModel.fn.ClearSensitiveFields();
        self.ViewModel.fn.SetEntry();

        var text = Array.isArray(message) ? message.join('<br />') : message;
        $scope.find('.alert').removeClass('hidden').text(text);

    };

    function submitInfo(e) {

        e.preventDefault();
        var promise = $.Deferred();

        if (self.ViewModel.PaymentMethodParentType.IsAch()) {

            promise.resolve();

        } else if (self.ViewModel.CardAccount.PaymentMethodType()) {

            // on securepan clientside validation complete
            $securepanframe.off('securepan.onvalidate').on('securepan.onvalidate', function(e, result) {

                if (result.isValid) {
                    // form + securepan valid
                    self.ViewModel.CardAccount.LastFour(result.lastFour);
                    promise.resolve();
                } else {
                    // one or both invalid
                    promise.reject();
                }

                $securepanframe.off('securepan.onvalidate');
            });

            // trigger securepan to run clientside validation  (this will trigger .onvalidate or an error in the callback)
            $securepanframe.trigger('securepan.validate');

        }

        promise.done(function() {
            showError(false);
            self.ViewModel.fn.SetConfirm();
        });

    };

    function submitPayment(e) {

        e.preventDefault();
        $.blockUI();
        showError(false);

        var $form = $(this);
        if (!$form.valid()) {
            return;
        }

        var postModel = self.ViewModel.fn.toJs();
        var promise = $.Deferred();
        
        if (self.ViewModel.PaymentMethodParentType.IsCreditCard()) {

            $securepanframe.off('securepan.onsaved').on('securepan.onsaved', function(e, data) {

                postModel.CardAccount.BillingId = data.BillingId;
                postModel.CardAccount.GatewayToken = data.TransactionId;
                postModel.CardAccount.LastFour = data.LastFour;
                postModel.CardAccount.AvsCode = data.AvsCode;

                promise.resolve();
                $securepanframe.off('securepan.onsaved');

            });

            // trigger securepan save (this will trigger .onsaved or an error in the callback)
            $securepanframe.trigger('securepan.save', {
                cvv: postModel.CardAccount.CardSecurityCode,
                expM: postModel.CardAccount.ExpirationDateM,
                expY: postModel.CardAccount.ExpirationDateY,
                name: postModel.CardAccount.CardAccountHolderName,
                address1: postModel.CardAccount.AddressStreet1,
                address2: postModel.CardAccount.AddressStreet2,
                city: postModel.CardAccount.City,
                state: postModel.CardAccount.State,
                zip: postModel.CardAccount.Zip
            });

        } else {

            promise.resolve();

        }

        promise.done(function() {
            $.post($form.attr('action'), { model: postModel, __RequestVerificationToken: $form.find('input[name=__RequestVerificationToken]').val() }, function(result) {

                if (result.Result === true) {
                    window.location.href = result.Message;
                } else {
                    $.unblockUI();
                    showError(result.Message);
                }

            }, 'json');
        });

    };

    function loadSecurePan() {

        $('#securepanmask').remove();
        $('.paymenttype-creditcard').append('<div id="securepanmask" style="background: #fff; left: 0; right: 0; top: 0; bottom: 0; position: absolute;"></div>');
        $('.secure-pan-container').html('');

        // the iframe html
        $.get({ url: '/payment/SecurePan', cache: false }, function (html) {

            $('.secure-pan-container').html(html);

            window.VisitPay.SecurePan.Initialize().done(function($iframe) {

                $securepanframe = $iframe;

                // on card type changed
                $securepanframe.off('securepan.oncardtypechanged').on('securepan.oncardtypechanged', function(e, paymentMethodTypeId) {
                    ko.unwrap(self.ViewModel.CardAccount).PaymentMethodType(paymentMethodTypeId);
                });

                // on error
                $securepanframe.off('securepan.onerror').on('securepan.onerror', function(e, result) {

                    $.unblockUI();
                    
                    if (result.errorMessage && result.errorMessage.length > 0) {
                        showError(result.errorMessage);
                        
                        // audit log
                        try {
                            $.post('/payment/InvalidCreditCard', {
                                errorMessage: result.errorMessage,
                                isTimeout: result.isTimeout,
                                tokenGuid: self.ViewModel.Guarantor.Guarantor(),
                                __RequestVerificationToken: $('body').find('input[name=__RequestVerificationToken]').eq(0).val()
                            });
                        } catch (ex) { }

                    } else {
                        showError(false);
                    }

                });

                $('#securepanmask').remove();
                $.unblockUI();

            }).fail(function (retry) {

                if (retry) {
                    loadSecurePan();
                } else {
                    self.ViewModel.PaymentMethodParentType(null);
                    showError('We\'re having trouble accessing our secure credit card portal.  Please try again in a few minutes.');
                }

            });

        });

    };

    self.Initialize = function (guarantor) {

        self.ViewModel = new window.VisitPay.Models.PaymentSubmitViewModel(guarantor);
        self.ViewModel.PaymentMethodParentType.subscribe(function (newValue) {

            $securepanframe = null;
            resetValidation();
            setPaymentValidationRules($scope.find('input.numeric'));

            if (newValue && newValue.toString() === '1') {
                $.blockUI();
                loadSecurePan();

                $('#CardAccount_CardSecurityCode').numbersOnly();
                $('#CardAccount_Zip').zipCode();

            }

        });

        self.ViewModel.Guarantor.Balance.subscribe(function (newValue) {
            if (newValue && $.isNumeric(newValue) && parseFloat(newValue) >= 1.0) {
                self.ViewModel.PaymentAmount(parseFloat(newValue).toFixed(2));
                setPaymentValidationRules($scope.find('input.numeric'));
            }
        });

        // hide payment method type if only 1 available option
        if ($('#PaymentMethodParentType option').length === 2) {  // the '-' label counts as one
            self.ViewModel.PaymentMethodParentType($('#PaymentMethodParentType option:eq(1)').val());
            $('#PaymentMethodParentType').hide();
            $('label[for=PaymentMethodParentType]').removeClass('required');
        }

        ko.applyBindings(self.ViewModel, $scope[0]);

        $scope.on('submit', '#frmPaymentInfo', submitInfo);
        $scope.on('submit', '#frmSubmitPayment', submitPayment);

        setNumericInput($scope.find('input.numeric'));
        
        $('form').on('invalid-form.validate', function() {
            if (self.ViewModel.PaymentMethodParentType.IsCreditCard() && !self.ViewModel.CardAccount.PaymentMethodType()) {
                if (self.ViewModel.CardAccount.PaymentMethodType()) {
                    $('.secure-pan-frame').removeClass('input-validation-error');
                } else {
                    $('.secure-pan-frame').addClass('input-validation-error');
                }
            }
        });

    };

    return self;

})(window.VisitPay, window.jQuery, window.ko);
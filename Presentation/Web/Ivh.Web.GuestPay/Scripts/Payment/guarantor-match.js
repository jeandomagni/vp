﻿(function(ko) {

    window.VisitPay.Models.GuarantorMatchRequestViewModel = function(guarantor) {

        var self = this;

        self.Guarantor = guarantor;
        self.StatementImage = ko.observable();
        self.StatementImage.IsSelected = ko.computed(function() {
            var statementImage = ko.unwrap(self.StatementImage);
            return statementImage != null && statementImage.length > 0;
        });

        return self;

    };

})(window.ko);

window.VisitPay.GuarantorMatch = (function($, ko) {

    var self = {
        ViewModel: {}
    };

    var $scope = $('#section-matchGuarantor');

    function showError(text) {

        var alert = $scope.find('.alert');

        if (text === false) {
            alert.addClass('hidden');
            return;
        }

        alert.removeClass('hidden');
        alert.find('div:eq(0)').html(text);

    };

    function submitForm(e) {

        e.preventDefault();
        $.blockUI();

        var $form = $(this);

        var model = {
            GuarantorAuthenticationValues: [],
            TermsAccepted: $form.find('#TermsAccepted').val()
        };

        ko.utils.arrayForEach($form.serializeArray(), function(item) {
            if (item.name === 'TermsAccepted' || item.name === 'StatementReferenceImageId') {
                return;
            }
            model.GuarantorAuthenticationValues.push({ Key: item.name, Value: item.value });
        });

        $.post($form.attr('action'), { model: model, __RequestVerificationToken: $('input[name=__RequestVerificationToken]').val() }, function(data) {

            showError(false);

            if (data.Result) {
                $.unblockUI();
                ko.mapping.fromJS(data.Message, {}, self.ViewModel.Guarantor);
            } else {
                if (data.IsLocked) {
                    window.location.href = data.Message;
                } else {
                    $.unblockUI();
                    showError(data.Message);
                    $form.find('input[type=checkbox]').prop('checked', false);
                    $form.find('input[type=text]').val('');
                    $form.find('input[type=text]:eq(0)').focus();
                }
            }

        }, 'json');

    };

    self.Initialize = function(guarantor) {

        self.ViewModel = new window.VisitPay.Models.GuarantorMatchRequestViewModel(guarantor);

        ko.applyBindings(self.ViewModel, $scope[0]);
        ko.applyBindings(self.ViewModel, $('#modalStatement')[0]);
        
        $scope.on('submit', 'form', submitForm);

        var $statementElement = $('[data-element="statement"]');
        if ($statementElement.is('select')) {
            $statementElement.off('change').on('change', function() {
                var $selectedOption = $statementElement.find(':selected');
                self.ViewModel.StatementImage($selectedOption.attr('data-statement'));
            });
        } else {
            self.ViewModel.StatementImage($statementElement.attr('data-statement'));
        }

        $('#section-payment').removeClass('mask');

    };

    return self;

})(window.jQuery, window.ko);
﻿window.VisitPay.Models.GuarantorMatchResponseViewModel = function () {

    var self = this;

    self.Guarantor = ko.observable();
    self.Balance = ko.observable();
    self.DisplayCurrentBalance = ko.observable();
    self.HasCurrentBalance = ko.observable();
    self.IsMatch = ko.computed(function() {
        return ko.unwrap(self.Guarantor) && ko.unwrap(self.Guarantor).length > 0 || false;
    });

    return self;

};

(function($, ko, guarantorMatch, paymentSubmit, idleTimer) {
    function showSessionAlert(b) {

        var alertSession = $('#section-payment').find('#alert-session');
        if (b) {
            alertSession.removeClass('hidden');
        } else {
            alertSession.addClass('hidden');
        }
    }

    $(document).ready(function() {
        var hash = (window.location.hash || '').replace('#', '');
        if (hash === 'timeout') {
            showSessionAlert(true);
            history.pushState('', document.title, window.location.pathname + window.location.search);
        } else {
            showSessionAlert(false);
        }
        
        var guarantorViewModel = new window.VisitPay.Models.GuarantorMatchResponseViewModel();
        guarantorViewModel.IsMatch.subscribe(function(value) {
            if (value) {
                showSessionAlert(false);
            }

            idleTimer.Initialize(guarantorViewModel.Guarantor());
        });

        guarantorMatch.Initialize(guarantorViewModel);
        paymentSubmit.Initialize(guarantorViewModel);

        idleTimer.Initialize(guarantorViewModel.Guarantor());

        $("#PaymentAmount")
            .focusout(function () {
                if (isNaN($('#PaymentAmount').val()))
                    return;
                if (isNaN(guarantorViewModel.Balance()))
                    return;
                let paymentAmount = parseFloat($('#PaymentAmount').val(), 2);
                let balance = parseFloat(guarantorViewModel.Balance(), 2);
                let showWarning = (paymentAmount < balance);
                if (showWarning) {
                    $("#guestPayPartialPaymentWarning").show();
                }
                else {
                    $("#guestPayPartialPaymentWarning").hide();
                }
            });
    });

})(window.jQuery, window.ko, window.VisitPay.GuarantorMatch, window.VisitPay.PaymentSubmit, window.VisitPay.IdleTimer);
﻿(function ($, idleTimer) {

    $(document).ready(function () {

        idleTimer.Initialize($('#section-receipt input[type=hidden]#Guarantor').val());

    });

})(window.jQuery, window.VisitPay.IdleTimer);
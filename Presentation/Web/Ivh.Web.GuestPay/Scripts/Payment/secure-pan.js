﻿/* keep this in sync (client, patient, guestpay) - these files should remain identical */

window.SecurePanTimeoutDuration = 10000;
window.SecurePanTimeoutRetryAttempts = 0;
window.SecurePanTimeoutRetryMaximum = 2; // a 2 here means 2 additional retries after the initial try

window.VisitPay.SecurePan = (function() {

    // match this with securepan's enum
    var messageTypeEnum = {
        error: 'error',
        load: 'load',
        log: 'log',
        notification: 'notification',
        saved: 'saved',
        validation: 'validation'
    };

    var $securepanframe;

    function logError(errorMessage, additionalData) {

        var msg = {
            errorMessage: errorMessage || ''
        };

        if (additionalData !== undefined && additionalData !== null) {
            msg = $.extend({}, msg, additionalData);
        }

        try {
            $.post('/Error/LogSecurePanError', { errorMessage: JSON.stringify(msg), __RequestVerificationToken: $('body').find('input[name=__RequestVerificationToken]').eq(0).val() });
        } catch (e) {}

    };

    function logMessageError(eventData) {

        var errorMessage = (eventData.hasOwnProperty('errorMessage') ? eventData.errorMessage : '') || '';
        var binData = (eventData.hasOwnProperty('binData') ? eventData.binData : null) || null;
        var rawResponse = (eventData.hasOwnProperty('rawResponse') ? eventData.rawResponse : '') || '';

        if (errorMessage.length > 0 || rawResponse.length > 0 || binData !== null) {

            var additionalData = {
                rawResponse: '',
                binData: {}
            };

            if (binData !== null) {
                additionalData.binData = binData;
            }

            if (rawResponse.length > 0) {
                additionalData.rawResponse = rawResponse;
            }

            logError(errorMessage, additionalData);

        }

    };

    function processErrorMessage(errorMessage, isTimeout) {

        $securepanframe.trigger('securepan.onerror', {
            errorMessage: errorMessage,
            isTimeout: isTimeout || false
        });

    };

    function processNotificationMessage(eventData) {

        var result = eventData.result;

        if (result.hasOwnProperty('cardType')) {

            var cardType = result.cardType === undefined || result.cardType === null || isNaN(result.cardType) ? null : parseInt(result.cardType);
            $securepanframe.trigger('securepan.oncardtypechanged', cardType);

        } else if (result.hasOwnProperty('focus')) {

            if (result.focus === true) {

                $securepanframe.addClass('focus');

            } else {

                $securepanframe.removeClass('focus');
                $securepanframe.parents('.row:eq(0)').next('.row').find('input,select,button').eq(0).focus();

            }

        } else if (result.hasOwnProperty('timeout')) {

            processErrorMessage(eventData.errorMessage, true);

        }
    };

    function processSavedMessage(result) {
        $securepanframe.trigger('securepan.onsaved', result || {});
    };

    function processValidationMessage(result) {

        result = result || {
            isValid: false
        };

        if (result.isValid === true) {
            $('.secure-pan-frame').removeClass('input-validation-error');
            $securepanframe.trigger('securepan.onerror', {});
        } else {
            $('.secure-pan-frame').addClass('input-validation-error');
        }

        $securepanframe.trigger('securepan.onvalidate', result);

    };

    function validateEventOrigin(event) {

        var origin = ((event.origin || event.originalEvent.origin) || '').toString().cleanUrl();
        var validOrigin = $securepanframe[0].src.cleanUrl();
        var isValidOrigin = origin === validOrigin;

        /* VP-5664 remove noisy log 
        if (!isValidOrigin) {
            logError('invalid origin', {
                origin: origin,
                validOrigin: validOrigin,
                source: 'app'
            });
        }
        */

        return isValidOrigin;

    };

    function onMessage(event) {

        // validate origin is in our valid origin list
        if (!validateEventOrigin(event)) {
            return;
        }

        // validate we have data
        if (!event.data) {
            return;
        }

        logMessageError(event.data);

        switch (event.data.messageType) {
            case messageTypeEnum.error:
            {
                processErrorMessage(event.data.errorMessage);
                break;
            }
            case messageTypeEnum.log:
            {
                // handled in logMessageError above
                break;
            }
            case messageTypeEnum.notification:
            {
                processNotificationMessage(event.data);
                break;
            }
            case messageTypeEnum.saved:
            {
                processSavedMessage(event.data.result);
                break;
            }
            case messageTypeEnum.validation:
            {
                processValidationMessage(event.data.result);
                break;
            }
        };

    };

    function sendMessage(messageType, data) {

        data = $.extend({ messageType: messageType }, data || {});
        $securepanframe[0].contentWindow.postMessage(data, $securepanframe[0].src);

    };

    return {
        Initialize: function() {

            $securepanframe = $('.secure-pan-frame');

            // setup triggers
            $securepanframe.off('securepan.validate').on('securepan.validate', function() {
                sendMessage('validate');
            });

            $securepanframe.off('securepan.save').on('securepan.save', function(e, data) {
                sendMessage('save', data);
            });

            var promise = $.Deferred();

            // setup a timeout for cases when securepan does not load
            // we'll clear this when we get the load event below
            var timeout = window.setTimeout(function() {

                // securepan did not load
                // increment attempts
                window.SecurePanTimeoutRetryAttempts = window.SecurePanTimeoutRetryAttempts + 1;

                // log that it failed
                logError('load failed', { attempts: window.SecurePanTimeoutRetryAttempts });

                // see if we can retry
                var retry = window.SecurePanTimeoutRetryAttempts <= window.SecurePanTimeoutRetryMaximum;

                // let the app know if we can retry
                promise.reject(retry);

                if (!retry) {
                    // max retry reached, show an error message
                    $.unblockUI();
                    if (window.VisitPay.Common.ModalGenericAlert) {
                        window.VisitPay.Common.ModalGenericAlert('Error', '<p>We\'re having trouble accessing our secure credit card portal.  Please try again in a few minutes.</p>', 'Ok');
                    }
                    window.SecurePanTimeoutRetryAttempts = 0;
                }

            }, window.SecurePanTimeoutDuration);

            // listen for load event
            window.addEventListener('message', function(event) {

                if (event === undefined || event === null) {
                    return;
                }

                if (!validateEventOrigin(event)) {
                    return;
                }

                var eventData = event.data || {};

                if (eventData.messageType !== messageTypeEnum.load || eventData.result !== true) {
                    // we only care about the load event here
                    return;
                }

                // securepan has loaded, clear timeout and resolve promise
                window.clearTimeout(timeout);
                window.SecurePanTimeoutRetryAttempts = 0;
                promise.resolve($securepanframe);

            });

            // listen for all other events
            window.addEventListener('message', onMessage);

            //
            return promise;

        }

    };

})();
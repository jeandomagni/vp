﻿window.VisitPay.IdleTimer = (function () {

    var userIdleTime = 0;
    var baseIncrement = 30000;
    var incrementsPerMinute = Math.ceil(60000 / baseIncrement);
    var incrementWarning = (window.VisitPay.ClientSettings.SessionTimeoutInMinutes * incrementsPerMinute) - Math.ceil((incrementsPerMinute / 2));
    var incrementLogout = (window.VisitPay.ClientSettings.SessionTimeoutInMinutes * incrementsPerMinute);
    var dialogBox = null;
    var idleInterval = null;
    var countdownInterval = null;
    var redirUrl;

    function countdownIncrement() {

        var secondsRemaining = (incrementLogout * 60 / incrementsPerMinute) - (incrementWarning * 60 / incrementsPerMinute);
        var element = dialogBox.find('#timeRemaining');
        element.text(secondsRemaining);

        countdownInterval = window.setInterval(function () {

            if (secondsRemaining === 1) {
                window.clearInterval(countdownInterval);
                window.location.href = redirUrl;
            }

            secondsRemaining--;
            element.text(secondsRemaining);

        }, 1000);

    }

    function timerIncrement() {

        userIdleTime = userIdleTime + 1;

        if (userIdleTime >= incrementWarning) {
            if (dialogBox !== null && !dialogBox.data('bs.modal').isShown) {
                dialogBox.modal('show');
                countdownIncrement();
            }
        }

    }

    function timerReset(force) {

        // prevent reset if dialog box is visible
        if (!force && dialogBox !== null && dialogBox.data('bs.modal').isShown) {
            return;
        }

        userIdleTime = 0;

        if (dialogBox !== null) {
            dialogBox.modal('hide');
        }

        window.clearInterval(countdownInterval);
    }

    function continueClicked() {
        timerReset(true);
    }

    var self = this;

    self.Initialize = function(token) {

        timerReset(true);

        idleInterval = setInterval(timerIncrement, baseIncrement);
        redirUrl = '/Payment/SessionTimeout/?token=' + (token || '');

        dialogBox = $("#TimeoutWarning");
        if (dialogBox.length === 0) {
            return;
        }
        dialogBox.modal('hide');
        dialogBox.find(".btn-primary").click(continueClicked);

        // clear timer on user input.
        $(this).mousemove(function() { timerReset(false) });
        $(this).keypress(function () { timerReset(false) });

    };

    return self;

})();
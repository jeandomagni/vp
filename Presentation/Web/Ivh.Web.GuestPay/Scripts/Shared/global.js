﻿(function($) {
    
    var message = 'Please wait';
    if (window.VisitPay != null && window.VisitPay.Localization != null) {
        message = window.VisitPay.Localization.PleaseWait;
    }
    
    $.blockUI.defaults.baseZ = 99999;
    $.blockUI.defaults.css = {};
    $.blockUI.defaults.overlayCSS = {
        backgroundColor: '#ffffff',
        cursor: 'wait',
        opacity: 0.8
    },
    $.blockUI.defaults.message = '<h1><img src="/Content/Images/loading.gif" alt="" /> &nbsp; ' + message + '...</h1>';
    
    String.prototype.cleanUrl = function () {
        var pathArray = this.split('/');
        if (pathArray == undefined || pathArray.length < 3) {
            //http://blah/ should have at least 3
            return this;
        }
        var protocol = pathArray[0];
        if (protocol.toLowerCase().indexOf('http') < 0) {
            //Seems like this isnt url...
            return this;
        }
        var host = pathArray[2];
        return host;
    };

    $.fn.extend({
        numbersOnly: function () {
            return this.each(function () {
                $(this).on('input', function (e) {
                    if (/\D/g.test($(this).val())) {
                        $(this).val($(this).val().replace(/\D/g, ''));
                    }
                });
            });
        },
        zipCode: function () {
            return this.each(function () {

                // if iOS device, switch type to "number" so a better keypad shows up
                if (/iP/i.test(navigator.userAgent)) {
                    $(this).attr('type', 'number');
                }

                $(this).attr('maxlength', 10);

                $(this).on('input', function (e) {
                    if (/[^0-9\-]/g.test($(this).val())) {
                        $(this).val($(this).val().replace(/[^0-9\-]/g, ''));
                    }
                });
            });
        },
    });

    var modalVerticalCenter = function ($modal) {

        var offset = 0;

        $modal.css('display', 'block');

        var $dialog = $modal.find('.modal-dialog');
        if ($dialog) {
            var px = ($(window).height() - $dialog.height() - 200) / 2;
            offset = Math.max(px, 0);
        }

        $dialog.css('margin-top', Math.max(offset, 0));

    };

    $(window).on('show.bs.modal', function(e) {

        var $modal = $(e.target);
        modalVerticalCenter($modal);

    });

    //PIWIK tracking initialization done via attributes    
    $(document).on('click', '[data-piwik-click]', function (e) {
        var category = $(this).data("piwik-event-category");
        var eventName = $(this).data("piwik-event-name");
        window._paq.push(['trackEvent', category, 'Access From', window.location.href]);
        window._paq.push(['trackEvent', category, 'Click', eventName]);
    });

    $(document).ready(function() {

        // nav
        var $nav = $('.navbar-collapse');
        $(document).on('click', function (event) {
            if ($nav !== event.target && !$nav.has(event.target).length && $nav.hasClass('in')) {
                $nav.slideUp(250, function () {
                    $(this).removeClass('in');
                    $(this).css('display', '');
                });
            }
        });

        // assuming for now '<dl>' is accordion grid
        $('dl dt').off('click').on('click', function () {
            var $dd = $(this).next('dd');
            if ($dd.is(':visible')) {
                $(this).removeClass('active');
                $dd.slideUp(250);
            } else {
                $dd.siblings('dt').removeClass('active');
                $dd.siblings('dd').slideUp(250);
                $(this).addClass('active');
                $dd.slideDown(250);
            }
        });

    });

})(jQuery);

window.onerror = function (message, file, line, col, error) {
    try {

        var token = $('#__AjaxAntiForgeryForm input[name=__RequestVerificationToken]').val();

        $.ajax({
            global: false,
            url: window.VisitPay.Urls.WindowOnError,
            type: 'POST',
            data: {
                'message': message,
                'file': file,
                'line': line,
                'col': col,
                'error': error,
                __RequestVerificationToken: token
            }
        }).done(function() {
            // nothing
        }).fail(function() {
            // nothing
        });
    } catch (x) {
        // nothing
    }
    return false;
};
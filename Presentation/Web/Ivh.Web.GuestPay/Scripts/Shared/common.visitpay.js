﻿window.VisitPay = window.VisitPay || {};

VisitPay.Common = (function($) {

    var self = {};

    self.AppendMessageToValidationSummary = function (form, message, isMobileSummary) {

        isMobileSummary = isMobileSummary || false;

        var container = isMobileSummary ? form.find('[data-mob-valmsg-summary="true"]') : form.find('[data-valmsg-summary="true"]');
        if (container && container.length) {
            container.removeClass('validation-summary-valid').addClass('validation-summary-errors');
            var list = container.find('ul');
            if (list && list.length && !$('li:contains("' + message + '")').length)
                $('<li />').html(message).appendTo(list);
        }

    };

    self.ResetUnobtrusiveValidation = function(form) {
        form.removeData('validator').removeData('unobtrusiveValidation');
        $.validator.unobtrusive.parse(form);
        form.validate().resetForm();
        form.find('[data-valmsg-summary=true]')
            .removeClass('validation-summary-errors')
            .addClass('validation-summary-valid')
            .find('ul')
            .empty();
        form.find('.field-validation-error').removeClass('field-validation-error').addClass('field-validation-valid').html('');
        form.find('.input-validation-error').removeClass('input-validation-error').addClass('valid');
    };

    self.ResetValidationSummary = function (form, isMobileSummary) {

        isMobileSummary = isMobileSummary || false;

        var container = isMobileSummary ? form.find('[data-mob-valmsg-summary="true"]') : form.find('[data-valmsg-summary="true"]');
        if (container && container.length) {
            var list = container.find('ul');

            if (list && list.length) {
                list.empty();
                container.addClass('validation-summary-valid').removeClass('validation-summary-errors');
            }
        }

    };


    return self;

})(window.jQuery);
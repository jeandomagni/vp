﻿(function (ko) {

    ko.bindingHandlers.display = (function () {
        function set(element, valueAccessor) {
            var display = ko.unwrap(valueAccessor());
            $(element).css('display', display ? 'block' : 'none');
        }

        return { init: set, update: set };
    })();

    ko.bindingHandlers.hidden = (function () {
        function set(element, valueAccessor) {
            var hidden = ko.unwrap(valueAccessor());
            $(element).css('visibility', hidden ? 'hidden' : 'visible');
        }

        return { init: set, update: set };
    })();

    ko.bindingHandlers.show = (function() {
        function set(element, valueAccessor) {
            var show = ko.unwrap(valueAccessor());
            if (show)
                $(element).removeClass('hidden');
            else
                $(element).addClass('hidden');
        }

        return { init: set, update: set };
    })();

    ko.bindingHandlers.currencyText = {
        update: function (element, valueAccessor, allBindingsAccessor) {

            var precision = ko.utils.unwrapObservable(allBindingsAccessor().precision) || ko.bindingHandlers.currencyText.defaultPrecision;
            var symbol = ko.utils.unwrapObservable(allBindingsAccessor().symbol) || ko.bindingHandlers.currencyText.defaultSymbol;
            var emptyText = ko.utils.unwrapObservable(allBindingsAccessor().emptyText) || ko.bindingHandlers.currencyText.emptyText;

            var value = ko.utils.unwrapObservable(valueAccessor());
            if (value != null && $.isNumeric(value)) {

                var formattedValue = symbol + parseFloat(value.toString().replace(/,/g, '')).toFixed(precision).toString().replace(/(\d)(?=(\d{3})+\b)/g, '$1,');

                ko.bindingHandlers.text.update(element, function () {
                    return formattedValue;
                });
            } else {
                ko.bindingHandlers.text.update(element, function () {
                    return emptyText;
                });
            }

        },
        defaultPrecision: 2,
        defaultSymbol: '$',
        defaultEmptyText: null
    };

    ko.bindingHandlers.percentText = {
        update: function (element, valueAccessor, allBindingsAccessor) {

            var precision = ko.utils.unwrapObservable(allBindingsAccessor().precision) || ko.bindingHandlers.percentText.defaultPrecision;
            var symbol = ko.utils.unwrapObservable(allBindingsAccessor().symbol) || ko.bindingHandlers.percentText.defaultSymbol;
            var emptyText = ko.utils.unwrapObservable(allBindingsAccessor().emptyText) || ko.bindingHandlers.currencyText.emptyText;

            var value = ko.utils.unwrapObservable(valueAccessor());
            if (value != null) {
                var formattedValue = (parseFloat(value.toString().replace(/,/g, '')) * 100).toFixed(precision).toString().replace(/(\d)(?=(\d{3})+\b)/g, '$1,') + ' ' + symbol;

                ko.bindingHandlers.text.update(element, function () {
                    return formattedValue;
                });
            } else {
                ko.bindingHandlers.text.update(element, function () {
                    return emptyText;
                });
            }
        },
        defaultPrecision: 1,
        defaultSymbol: '%',
        emptyText: null
    };

    ko.bindingHandlers.creditCard = {
        init: function (element, valueAccessor) {

            var value = valueAccessor();
            var observable = value.value;

            if (ko.isObservable(observable)) {

                $(element).on('input', function () {

                    observable($(element).val().replace(/_/g, '').replace(/-/g, ''));

                    var cardNumber = $(element).val();
                    var isAmex = cardNumber.indexOf('34') === 0 || cardNumber.indexOf('37') === 0;
                    var mask = isAmex ? '9999-999999-99999' : '9999-9999-9999-9[999999]';
                    var previousMask = $(element).data('mask');

                    if (!previousMask || mask !== previousMask) {
                        $(element).data('mask', mask);
                        $(element).inputmask(mask);
                    }

                });

                $(element).on('focusout change', function() {

                    if ($(element).inputmask('isComplete')) {
                        observable($(element).inputmask('unmaskedvalue'));
                    } else {
                        observable(null);
                    }

                });
            }

        },
        update: function (element, valueAccessor) {

            var mask = valueAccessor();
        
            var observable = mask.value;

            if (ko.isObservable(observable)) {

                var valuetoWrite = observable();

                $(element).val(valuetoWrite);
            }

        }
    };

    ko.bindingHandlers.stopParentBinding = {
        init: function () {
            return { controlsDescendantBindings: true };
        }
    };

    ko.virtualElements.allowedBindings.stopParentBinding = true;

})(window.ko);
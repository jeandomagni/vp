﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Ivh.Web.QATools.Models
{
    using System.ComponentModel.DataAnnotations;
    using Application.FinanceManagement.Common.Dtos;

    public class InterestRateViewModel
    {
        [Required]
        [Display(Name = "Id")]
        public int InterestRateId { get; set; }

        [Required]
        [Display(Name = "Duration Range Start")]
        public int DurationRangeStart { get; set; }

        [Required]
        [Display(Name = "Duration Range End")]
        public int DurationRangeEnd { get; set; }

        [Required]
        [Display(Name = "Interest Rate")]
        public decimal Rate { get; set; }

        [Required]
        [Display(Name = "Interest Free Period")]
        public int InterestFreePeriod { get; set; }
        
        [Required]
        [Display(Name = "Display Text")]
        public string DisplayText { get; set; }

        public IList<InterestRateViewModel> InterestRates { get; set; }
    }
}
﻿namespace Ivh.Web.QATools.Models.Etl
{
    using System;
    using System.ComponentModel;
    using Domain.HospitalData.Visit.Entities;

    public class OutboundVisitTransactionViewModel
    {
        [DisplayName("VpOutboundVisitTransactionId")]
        public int VpOutboundVisitTransactionId { get; set; }

        [DisplayName("VpPaymentAllocationId")]
        public int VpPaymentAllocationId { get; set; }

        [DisplayName("InsertDate")]
        public DateTime InsertDate { get; set; }

        [DisplayName("TransactionAmount")]
        public decimal TransactionAmount { get; set; }

        [DisplayName("ProcessedDate")]
        public DateTime ProcessedDate { get; set; }

        [DisplayName("VisitBillingSystemId")]
        public virtual int VisitBillingSystemId { get; set; }

        [DisplayName("VisitSourceSystemKey")]
        public virtual string VisitSourceSystemKey { get; set; }

        [DisplayName("Visit")]
        public virtual VisitViewModel Visit { get; set; }

        [DisplayName("BalanceTransferStatusDisplay")]
        public virtual string BalanceTransferStatusDisplay {
            get { return this.BalanceTransferStatus == null ? "" : this.BalanceTransferStatus.BalanceTransferStatusName; }
        }
        public virtual BalanceTransferStatus BalanceTransferStatus { get; set; }

        public string Processed
        {
            get { return this.ProcessedDate > DateTime.MinValue ? "YES" : "NO"; }
        }
    }
}
﻿namespace Ivh.Web.QATools.Models
{
    using Common.Base.Utilities.Helpers;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.Web.Mvc;

    public class AddHsGuarantorViewModel
    {
        public IList<SelectListItem> BillingSystems { get; set; }
        [Display(Name = "Billing System")]
        public int HsBillingSystemId { get; set; }
        [Display(Name = "Source System Key")]
        public string SourceSystemKey { get; set; }
        [Display(Name = "First Name")]
        public string FirstName { get; set; }
        [Display(Name = "Last Name")]
        public string LastName { get; set; }

        [Display(Name = "Date of Birth"), DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}")]
        public DateTime? DOB { get; set; }
        [Display(Name = "Last 4 of SSN")]
        public int? SSN4 { get; set; }
        [Display(Name = "Address Line 1")]
        public string AddressLine1 { get; set; }
        [Display(Name = "Address Line 2")]
        public string AddressLine2 { get; set; }
        [Display(Name = "City")]
        public string City { get; set; }
        [Display(Name = "State / Prov")]
        public string StateProvince { get; set; }
        [Display(Name = "Postal Code")]
        public string PostalCode { get; set; }
        [Display(Name = "Country")]
        public string Country { get; set; }
        [Display(Name = "Full SSN")]
        public string SSN { get; set; }
        public string LastNameFirstName => FormatHelper.LastNameFirstName(this.LastName, this.FirstName);
        public string FirstNameLastName => FormatHelper.FirstNameLastName(this.FirstName, this.LastName);
        public bool VpEligible { get; set; } = true;

    }
}
﻿namespace Ivh.Web.QATools.Models.Etl
{
    using System;
    using System.ComponentModel.DataAnnotations;
    using Common.Base.Enums;
    using Common.VisitPay.Enums;

    public class VisitTransactionViewModel
    {
        [Display(Name = "Source System Key")]
        public string SourceSystemKey { get; set; }

        [Display(Name = "Transaction Date")]
        public DateTime? TransactionDate { get; set; }

        [Display(Name = "Post Date")]
        public DateTime? PostDate { get; set; }

        [Display(Name = "Transaction Amount")]
        public decimal? TransactionAmount { get; set; }

        public string VpTransactionType { get; set; }

        [Display(Name = "Transaction Type")]
        public int? VpTransactionTypeId { get; set; }

        [Required]
        [Display(Name = "Transaction Type")]
        public string VpTransactionTypeName { get; set; }

        [Display(Name = "Transaction Code")]
        public int? TransactionCode { get; set; }

        [Display(Name = "Transaction Description")]
        public string TransactionDescription { get; set; }

        [Display(Name = "Data Load Id")]
        public int DataLoadId { get; set; }

        [Display(Name = "Visit Transaction Id")]
        public int VisitTransactionId { get; set; }

        [Display(Name = "Visit Change Set Id")]
        public int VisitChangeSetId { get; set; }

        [Display(Name = "Change Type")]
        public ChangeTypeEnum ChangeType { get; set; }

        [Display(Name = "Billing System Id")]
        public int BillingSystemId { get; set; }

        [Display(Name = "Visit Id")]
        public int VisitId { get; set; }

        [Display(Name = "Payment Allocation Id")]
        public int? PaymentAllocationId { get; set; }

        [Display(Name = "Surpress Reassessment")]
        public bool ReassessmentExempt { get; set; }
    }
}
﻿namespace Ivh.Web.QATools.Models.Etl
{
    using System;
    using System.ComponentModel.DataAnnotations;

    public class VpVisitReconciliationViewModel
    {
        public int BaseVisitId { get; set; }
        
        [Display(Name = "Balance")]
        public decimal VpVisitBalance { get; set; }
        
        [Display(Name = "Interest Paid")]
        public decimal SumOfVppInterestPaid { get; set; }
        
        [Display(Name = "Interest Assessed")]
        public decimal SumOfVppInterestAssessed { get; set; }
        
        [Display(Name = "Ach Unsettled Principal")]
        public decimal SumOfAchUnsettledPrincipal { get; set; }
        
        [Display(Name = "Ach Unsettled Interest")]
        public decimal SumOfAchUnsettledInterest { get; set; }

        [Display(Name = "Last Updated")]
        public DateTime LastUpdated { get; set; }
    }
}
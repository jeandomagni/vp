﻿namespace Ivh.Web.QATools.Models.Etl
{
    using Ivh.Common.Base.Enums;

    public class HsGuarantorFilterViewModel
    {
        public int? VPGID { get; set; }
        public string HsSourceKey { get; set; }
        public string LastName { get; set; }
        public bool VpRegistered { get; set; }
        public bool VpUnregistered { get; set; }
    }
}

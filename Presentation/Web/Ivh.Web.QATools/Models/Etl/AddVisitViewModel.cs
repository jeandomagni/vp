﻿namespace Ivh.Web.QATools.Models.Etl
{
    using System.Collections.Generic;
    using System.Web.Mvc;

    public class AddVisitViewModel
    {
        public AddVisitViewModel()
        {
            if (this.Visits == null)
            {
                this.Visits = new List<VisitViewModel>();
            }
        }

        public int HsGuarantorId { get; set; }
        public GuarantorViewModel Guarantor { get; set; }
        public IList<SelectListItem> BillingSystems { get; set; }
        public IList<SelectListItem> BillingApplications { get; set; }
        public IList<SelectListItem> TransactionCounts { get; set; }
        public IList<SelectListItem> TransactionTypes { get; set; }
        public IList<SelectListItem> TransactionCodes { get; set; }
        public IList<SelectListItem> InsurancePlanTypes { get; set; }
        public IList<SelectListItem> InsurancePlanTypesWithNothingSelected { get; set; }

        public VisitViewModel Visit { get; set; }

        public IList<VisitTransactionViewModel> Transactions { get; set; }
        public List<SelectListItem> LifeCycleStages { get; set; }
        public List<SelectListItem> AgingTiers { get; set; }
        public List<SelectListItem> PatientTypes { get; set; }
        public List<SelectListItem> PrimaryInsuranceTypes { get; set; }
        public List<SelectListItem> RevenueWorkCompanies { get; set; }
        public IList<VisitInsurancePlanViewModel> InsurancePlans { get; set; }
        public List<SelectListItem> ServiceGroups { get; set; }
        public List<SelectListItem> PaymentPriorities { get; set; }

        public List<SelectListItem> ChangeEventTypes { get; set; }
        public List<SelectListItem> Facilities { get; set; }

        public bool ProcessChangeSet { get; set; }
        public int ChangeEventType { get; set; } = 2; // VisitTransactionDataChanged

        public IList<VisitViewModel> Visits { get; set; }
    }
}
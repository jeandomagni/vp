﻿namespace Ivh.Web.QATools.Models.Etl
{
    using Common.Base.Enums;

    public class VpOutboundHsGuarantorViewModel
    {
        public int VpOutboundHsGuarantorId { get; set; }
        public string VpOutboundHsGuarantorMessageType { get; set; }
        public string InsertDate  { get; set; }
        public string ProcessedDate { get; set; }
    }
}
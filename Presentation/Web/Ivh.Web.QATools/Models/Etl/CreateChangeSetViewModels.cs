﻿namespace Ivh.Web.QATools.Models.Etl
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.Linq;
    using System.Web.Mvc;
    using Common.Base.Utilities.Helpers;

    public class CreateChangeSetViewModel
    {
        private int? _serviceGroupId; 
        private int? _paymentPriority;
        
        public CreateChangeSetViewModel()
        {
            this.ChangeEventFormViewModel = new ChangeEventViewModel();
            this.TransactionTypes = new List<SelectListItem>();
            this.TransactionCodes = new List<SelectListItem>();
            this.Transactions = new List<VisitTransactionViewModel>();
            this.UnclearedPaymentAllocations = new List<UnclearedPaymentAllocationViewModel>();
        }

        public int HsGuarantorId { get; set; }
        public string GuarantorSourceSystemKey { get; set; }
        public string GuarantorFirstName { get; set; }
        public string GuarantorLastName { get; set; }
        public string VisitBillingSystem { get; set; }
        public string VisitSourceSystemKey { get; set; }
        public string VisitSourceSystemKeyDisplay { get; set; }
        public IList<SelectListItem> TransactionTypes { get; set; }
        public IList<SelectListItem> TransactionCodes { get; set; }
        public ChangeEventViewModel ChangeEventFormViewModel { get; set; }
        public List<SelectListItem> LifeCycleStages { get; set; }
        public SelectListItem LifeCycleStage { get; set; }
        public bool CreateScoringChangeEvent { get; set; }
        public int? AgingTier { get; set; }
        public List<SelectListItem> AgingTiers { get; set; }
        public List<SelectListItem> Facilities { get; set; }
        public List<SelectListItem> ServiceGroups { get; set; }
        public List<SelectListItem> PaymentPriorities { get; set; }

        [Display(Name = "Life Cycle Stage")]
        public int LifeCycleStageId { get; set; }

        [Display(Name = "Self Pay Date")]
        public DateTime? FirstSelfPayDate { get; set; }

        [Display(Name = "Self Pay Balance")]
        public decimal SelfPayBalance { get; set; }

        [Display(Name = "Patient IS Guarantor")]
        public bool IsPatientGuarantor { get; set; }

        [Display(Name = "Patient IS Minor")]
        public bool IsPatientMinor { get; set; }

        [Display(Name = "VP Eligible")]
        public virtual bool VpEligible { get; set; }
        [Display(Name = "Billing Hold")]
        public virtual bool BillingHold { get; set; }
        [Display(Name = "Redact")]
        public virtual bool Redact { get; set; }
        [Display(Name = "Financial Assistance 0% Interest:")]
        public bool InterestZero { get; set; }

        [Display(Name = "Financial Assistance Refund Interest:")]
        public bool InterestRefund { get; set; }

        [Display(Name = "Admit Date")]
        public DateTime? AdmitDate { get; set; }

        [Display(Name = "Billing Application")]
        public string BillingApplication { get; set; }

        [Display(Name = "Discharge Date")]
        public DateTime? DischargeDate { get; set; }

        [Display(Name = "HS Current Balance")]
        public decimal HsCurrentBalance { get; set; }

        [Display(Name = "Patient First Name")]
        public string PatientFirstName { get; set; }

        [Display(Name = "Patient Last Name")]
        public string PatientLastName { get; set; }

        public int VisitId { get; set; }

        [Display(Name = "Service Group")]
        public int? ServiceGroupId {
            get
            {
                return this._serviceGroupId;
            }
            set
            {
                this._serviceGroupId = value == -1 ? null : value;
            }
        }

        [Display(Name = "Payment Priority")]
        public int? PaymentPriority {
            get
            {
                return this._paymentPriority;
            }
            set
            {
                this._paymentPriority = value == -1 ? null : value;
            }
        }

        [Display(Name = "Visit Description")]
        public string VisitDescription { get; set; }

        [Display(Name = "Transactions")]
        public IList<VisitTransactionViewModel> Transactions { get; set; }

        public IList<VisitPaymentAllocationViewModel> VisitPaymentAllocations { get; set; }

        public IList<UnclearedPaymentAllocationViewModel> UnclearedPaymentAllocations { get; set; }

        [Display(Name = "Visit Transaction Data Changed")]
        public int VisitTransactionDataChanged { get; set; }

        [Display(Name = "Facility ID")]
        public int? FacilityId { get; set; }

        [Display(Name = "Facility Description")]
        public string FacilityDescription { get; set; }

        public IList<SelectListItem> OptionOrder
        {
            get { return Enumerable.Range(0, 6).Select((i, x) => new SelectListItem() { Text = i.ToString(), Value = i.ToString() }).ToList(); }
        }

        public string GuarantorLastNameFirstName => FormatHelper.LastNameFirstName(this.GuarantorLastName, this.GuarantorFirstName);

        public string GuarantorFirstNameLastName => FormatHelper.FirstNameLastName(this.GuarantorFirstName, this.GuarantorLastName);

        public string PatientLastNameFirstName => FormatHelper.LastNameFirstName(this.PatientLastName, this.PatientFirstName);

        public string PatientFirstNameLastName => FormatHelper.FirstNameLastName(this.PatientFirstName, this.PatientLastName);
    }

    public class VisitPaymentAllocationViewModel
    {
        public int PaymentAllocationId { get; set; }
    }
}
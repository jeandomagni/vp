﻿namespace Ivh.Web.QATools.Models.Etl
{
    using System;
    using System.ComponentModel.DataAnnotations;
    using Common.Base.Utilities.Helpers;

    public class VisitViewModel
    {
        private int? _serviceGroupId;
        private int? _paymentPriority;

        public int VisitId { get; set; }

        [Required]
        [Display(Name = "Guarantor Id"), Editable(false, AllowInitialValue = true)]
        public int HsGuarantorId { get; set; }

        [Required]
        [Display(Name = "Billing System")]
        public int BillingSystemId { get; set; }

        [Required]
        [Display(Name = "Source System Key")]
        public string SourceSystemKey { get; set; }

        [Display(Name = "Display Source System Key")]
        public string SourceSystemKeyDisplay { get; set; }

        [Required]
        [Display(Name = "Visit Description")]
        public string VisitDescription { get; set; }

        [Display(Name = "Service Group")]
        public int? ServiceGroupId {
            get
            {
                return this._serviceGroupId;
            }
            set
            {
                this._serviceGroupId = value == -1 ? null : value;
            }
        }

        [Display(Name = "Payment Priority")]
        public int? PaymentPriority {
            get
            {
                return this._paymentPriority;
            }
            set
            {
                this._paymentPriority = value == -1 ? null : value;
            }
        }

        [Required]
        [Display(Name = "Admit Date")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0}:MM/dd/yyyy")]
        public DateTime AdmitDate { get; set; }

        [Required]
        [Display(Name = "Discharge Date")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0}:MM/dd/yyyy")]
        public DateTime DischargeDate { get; set; }

        [Required]
        [Display(Name = "HS Current Balance")]
        public decimal HsCurrentBalance { get; set; }

        [Required]
        [Display(Name = "Insurance Balance")]
        public decimal InsuranceBalance { get; set; }

        [Required]
        [Display(Name = "Self Pay Balance")]
        public decimal SelfPayBalance { get; set; }

        [Display(Name = "First Self Pay Date")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0}:MM/dd/yyyy")]
        public DateTime? FirstSelfPayDate { get; set; }

        [Required]
        [Display(Name = "Patient First Name")]
        public string PatientFirstName { get; set; }

        [Required]
        [Display(Name = "Patient Last Name")]
        public string PatientLastName { get; set; }

        [Required]
        [Display(Name = "Patient DOB")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0}:MM/dd/yyyy")]
        public DateTime PatientDob { get; set; }

        [Required]
        [Display(Name = "Primary Insurance Type")]
        public string PrimaryInsuranceType { get; set; }
        [Required]
        [Display(Name = "Patient Type")]
        public string PatientType { get; set; }
        [Required]
        [Display(Name = "Revenue work company")]
        public string RevenueWorkCompany { get; set; }
        [Required]
        [Display(Name = "Life Cycle Stage")]
        public string LifeCycleStage { get; set; }


        [Required]
        [Display(Name = "Billing Application")]
        public string BillingApplication { get; set; }

        [Display(Name = "Patient IS Guarantor")]
        public bool IsPatientGuarantor { get; set; }
        [Display(Name = "Patient IS Minor")]
        public bool IsPatientMinor { get; set; }
        
        [Display(Name = "VP Eligible")]
        public virtual bool VpEligible { get; set; } = true;
        [Display(Name = "Billing Hold")]
        public virtual bool BillingHold { get; set; }
        [Display(Name = "Redact")]
        public virtual bool Redact { get; set; }

        [Display(Name = "Aging Tier")]
        public int? AgingTier { get; set; }

        [Display(Name = "Visit Is On Pre-payment Plan")]
        public bool OnPrePaymentPlan { get; set; }

        [Display(Name = "Facility State Code")]
        public string FacilityStateCode { get; set; }

        [Display(Name = "Facility Description")]
        public string FacilityDescription { get; set; }

        [Display(Name = "Facility")]
        public int? FacilityId { get; set; }

        public string PatientLastNameFirstName => FormatHelper.LastNameFirstName(this.PatientLastName,this.PatientFirstName);
        public string PatientFirstNameLastName => FormatHelper.FirstNameLastName(this.PatientFirstName,this.PatientLastName);
    }
}
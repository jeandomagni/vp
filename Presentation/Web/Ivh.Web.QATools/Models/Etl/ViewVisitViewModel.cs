﻿namespace Ivh.Web.QATools.Models.Etl
{
    using System.Collections.Generic;

    public class ViewVisitViewModel
    {
        public GuarantorViewModel Guarantor { get; set; }
        public VisitViewModel Visit { get; set; }
        public IList<VisitTransactionViewModel> VisitTransactions { get; set; }
        public IList<ChangeEventViewModel> ChangeSets { get; set; }
        public IList<OutboundVisitTransactionViewModel> OutboundVisitTransactions { get; set; }
        public IList<OutboundVisitViewModel> OutboundVisits { get; set; }
        public IList<VisitInsurancePlanViewModel> VisitInsurancePlans { get; set; }
    }
}
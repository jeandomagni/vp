﻿namespace Ivh.Web.QATools.Models.Etl
{
    using System;

    public class ChangeEventViewModel
    {
        public virtual DateTime ChangeEventDateTime { get; set; }
        public virtual string ChangeEventStatus { get; set; }
        public virtual string ChangeEventType { get; set; }
    }
}
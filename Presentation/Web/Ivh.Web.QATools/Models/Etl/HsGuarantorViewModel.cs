﻿namespace Ivh.Web.QATools.Models
{
    using Ivh.Common.Base.Utilities.Helpers;
    using System;
    using System.ComponentModel.DataAnnotations;

    public class HsGuarantorViewModel
    {
        [Required]
        public int HsGuarantorId { get; set; }
        [Required]
        public int HsBillingSystemId { get; set; }
        [Required]
        public string SourceSystemKey { get; set; }
        [Required]
        public string FirstName { get; set; }
        [Required]
        public string LastName { get; set; }

        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}",ApplyFormatInEditMode = true)]
        public DateTime? DOB { get; set; }

        [Required]
        public int? SSN4 { get; set; }
        [Required]
        public string SSN { get; set; }
        public string AddressLine1 { get; set; }
        public string AddressLine2 { get; set; }
        public string City { get; set; }
        public string StateProvince { get; set; }
        public string PostalCode { get; set; }
        public string Country { get; set; }
        public virtual bool VpEligible { get; set; }
        public virtual int? GenderId { get; set; }
        public virtual string Email { get; set; }
        [Required]
        public int? HsGuarantorTypeCode { get; set; }
        public string LastNameFirstName => FormatHelper.LastNameFirstName(this.LastName, this.FirstName);
        public string FirstNameLastName => FormatHelper.FirstNameLastName(this.FirstName, this.LastName);
    }
}
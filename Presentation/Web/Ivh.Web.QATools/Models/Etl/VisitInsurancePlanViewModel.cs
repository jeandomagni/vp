﻿namespace Ivh.Web.QATools.Models.Etl
{
    using System.ComponentModel.DataAnnotations;

    public class VisitInsurancePlanViewModel
    {
        [Display(Name = "Data Load Id")]
        public int DataLoadId { get; set; }

        [Display(Name = "Change Set Id")]
        public int ChangeSetId { get; set; }

        [Display(Name = "Visit Id")]
        public int VisitId { get; set; }

        [Display(Name = "Pay Order")]
        public int PayOrder { get; set; }

        [Display(Name = "Insurance Plan")]
        public int InsurancePlanId { get; set; }

        [Display(Name = "Insurance Plan")]
        public string InsurancePlanName { get; set; }
    }
}
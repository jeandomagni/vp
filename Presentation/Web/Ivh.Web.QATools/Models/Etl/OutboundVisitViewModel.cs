﻿namespace Ivh.Web.QATools.Models.Etl
{
    using System;
    using System.ComponentModel;
    using System.ComponentModel.DataAnnotations;
    using Common.Base.Enums;

    public class OutboundVisitViewModel
    {
        [DisplayName("InsertDate")]
        public int VpOutboundVisitId { get; set; }

        [DisplayName("Visit")]
        public VisitViewModel Visit { get; set; }

        [DisplayName("BillingSystemId")]
        public int BillingSystemId { get; set; }

        [DisplayName("SourceSystemKey")]
        public string SourceSystemKey { get; set; }

        [DisplayName("VpVisitId")]
        public int VpVisitId { get; set; }

        [DisplayName("VpOutboundVisitMessageTypeId")]
        public string VpOutboundVisitMessageTypeId { get; set; }

        [DisplayName("VpOutboundVisitMessageTypeName")]
        public string VpOutboundVisitMessageTypeName { get; set; }

        [DisplayName("ActionDate")]
        public DateTime ActionDate { get; set; }

        [DisplayName("InsertDate")]
        public DateTime InsertDate { get; set; }
    }
}
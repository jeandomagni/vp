﻿using System;
using Ivh.Application.Core.Common.Dtos;
using Ivh.Application.FinanceManagement.Common.Dtos;
using Ivh.Common.Base.Enums;

namespace Ivh.Web.QATools.Models.PaymentMethods
{
    using Application.User.Common.Dtos;
    using Common.VisitPay.Enums;

    #region base

    public class TestPaymentMethod : PaymentMethodDto
    {
        public TestPaymentMethod(VisitPayUserDto userDto, PaymentMethodTypeEnum paymentMethodType, GuarantorDto guarantor)
        {
            this.IsActive = true;
            this.IsPrimary = false;
            this.PaymentMethodType = paymentMethodType;
            this.CreatedDate = DateTime.UtcNow;
            this.CreatedUserId = userDto.VisitPayUserId;
            this.UpdatedDate = DateTime.UtcNow;
            this.UpdatedUserId = userDto.VisitPayUserId;
            if (guarantor != null)
            {
                this.VpGuarantorId = guarantor.VpGuarantorId;
            }
        }
    }

    public class TestPaymentMethodCard : TestPaymentMethod
    {
        public TestPaymentMethodCard(VisitPayUserDto userDto, PaymentMethodTypeEnum paymentMethodType, GuarantorDto guarantor) : base(userDto, paymentMethodType, guarantor)
        {
            this.NameOnCard = userDto.DisplayFirstNameLastName;
        }

        public string AccountNumber { get; set; }
        public string SecurityNumber { get; set; }
    }

    public class TestPaymentMethodAch : TestPaymentMethod
    {
        public TestPaymentMethodAch(VisitPayUserDto userDto, PaymentMethodTypeEnum paymentMethodType, GuarantorDto guarantor)
            : base(userDto, paymentMethodType, guarantor)
        {
            this.FirstName = userDto.FirstName;
            this.LastName = userDto.LastName;
        }

        public string AccountNumber { get; set; }
        public string RoutingNumber { get; set; }
    }

    #endregion

    #region good cards

    public class TestPaymentMethodVisaGood : TestPaymentMethodCard
    {
        public TestPaymentMethodVisaGood(VisitPayUserDto userDto, GuarantorDto guarantor) : base(userDto, PaymentMethodTypeEnum.Visa, guarantor)
        {
            this.AccountNickName = "TestVisaGood";
            this.AccountNumber = "4111111111111111";
            this.SecurityNumber = "123";
            this.ExpDate = "0429";
            this.BillingAddresses.Add(new PaymentMethodBillingAddressDto
            {
                Address1 = "123 Test St.",
                City = "Somewhere",
                State = "CA",
                Zip = "90001"
            });
        }
    }

    public class TestPaymentMethodVisaGoodExpiredLastMonth : TestPaymentMethodCard
    {
        public TestPaymentMethodVisaGoodExpiredLastMonth(VisitPayUserDto userDto, GuarantorDto guarantor)
            : base(userDto, PaymentMethodTypeEnum.Visa, guarantor)
        {
            this.AccountNickName = "TestExpiredLastMonth";
            this.AccountNumber = "4111111111111111";
            this.SecurityNumber = "123";
            this.ExpDate = DateTime.UtcNow.AddMonths(-1).ToString("MMyy");
            this.BillingAddresses.Add(new PaymentMethodBillingAddressDto
            {
                Address1 = "123 Test St.",
                City = "Somewhere",
                State = "CA",
                Zip = "90001"
            });
        }
    }

    public class TestPaymentMethodVisaGoodExpiringThisMonth : TestPaymentMethodCard
    {
        public TestPaymentMethodVisaGoodExpiringThisMonth(VisitPayUserDto userDto, GuarantorDto guarantor)
            : base(userDto, PaymentMethodTypeEnum.Visa, guarantor)
        {
            this.AccountNickName = "TestExpiringThisMonth";
            this.AccountNumber = "4111111111111111";
            this.SecurityNumber = "123";
            this.ExpDate = DateTime.UtcNow.AddMonths(0).ToString("MMyy");
            this.BillingAddresses.Add(new PaymentMethodBillingAddressDto
            {
                Address1 = "123 Test St.",
                City = "Somewhere",
                State = "CA",
                Zip = "90001"
            });
        }
    }

    public class TestPaymentMethodVisaGoodExpiringNextMonth : TestPaymentMethodCard
    {
        public TestPaymentMethodVisaGoodExpiringNextMonth(VisitPayUserDto userDto, GuarantorDto guarantor)
            : base(userDto, PaymentMethodTypeEnum.Visa, guarantor)
        {
            this.AccountNickName = "TestExpiringNextMonth";
            this.AccountNumber = "4111111111111111";
            this.SecurityNumber = "123";
            this.ExpDate = DateTime.UtcNow.AddMonths(1).ToString("MMyy");
            this.BillingAddresses.Add(new PaymentMethodBillingAddressDto
            {
                Address1 = "123 Test St.",
                City = "Somewhere",
                State = "CA",
                Zip = "90001"
            });
        }
    }

    public class TestPaymentMethodMastercardGood : TestPaymentMethodCard
    {
        public TestPaymentMethodMastercardGood(VisitPayUserDto userDto, GuarantorDto guarantor)
            : base(userDto, PaymentMethodTypeEnum.Mastercard, guarantor)
        {
            this.AccountNickName = "TestMastercardGood";
            this.AccountNumber = "5411111111111115";
            this.SecurityNumber = "777";
            this.ExpDate = "0429";
            this.BillingAddresses.Add(new PaymentMethodBillingAddressDto
            {
                Address1 = "4000 Main St.",
                City = "Anytown",
                State = "AZ",
                Zip = "85001"
            });
        }
    }

    public class TestPaymentMethodAmexGood : TestPaymentMethodCard
    {
        public TestPaymentMethodAmexGood(VisitPayUserDto userDto, GuarantorDto guarantor)
            : base(userDto, PaymentMethodTypeEnum.AmericanExpress, guarantor)
        {
            this.AccountNickName = "TestAmexGood";
            this.AccountNumber = "341111111111111";
            this.SecurityNumber = "4000";
            this.ExpDate = "0429";
            this.BillingAddresses.Add(new PaymentMethodBillingAddressDto
            {
                Address1 = "12 Colorado Blvd.",
                City = "Elsewhere",
                State = "IL",
                Zip = "40000"
            });
        }
    }

    public class TestPaymentMethodDiscoverGood : TestPaymentMethodCard
    {
        public TestPaymentMethodDiscoverGood(VisitPayUserDto userDto, GuarantorDto guarantor)
            : base(userDto, PaymentMethodTypeEnum.Discover, guarantor)
        {
            this.AccountNickName = "TestDiscoverGood";
            this.AccountNumber = "6011111111111117";
            this.SecurityNumber = "";
            this.ExpDate = "0429";
            this.BillingAddresses.Add(new PaymentMethodBillingAddressDto
            {
                Address1 = "6789 Green Ave.",
                City = "Nowhere",
                State = "MA",
                Zip = "12345"
            });
        }
    }

    #endregion

    #region bad cards

    public class TestPaymentMethodDecline : TestPaymentMethodCard
    {
        public TestPaymentMethodDecline(VisitPayUserDto userDto, GuarantorDto guarantor)
            : base(userDto, PaymentMethodTypeEnum.Visa, guarantor)
        {
            this.AccountNickName = "TestDecline";
            this.AccountNumber = "4012345678909";
            this.SecurityNumber = "";
            this.ExpDate = "0429";
            this.BillingAddresses.Add(new PaymentMethodBillingAddressDto
            {
                Address1 = "123 Test St.",
                City = "Somewhere",
                State = "CA",
                Zip = "90001"
            });
        }
    }

    public class TestPaymentMethodCall : TestPaymentMethodCard
    {
        public TestPaymentMethodCall(VisitPayUserDto userDto, GuarantorDto guarantor)
            : base(userDto, PaymentMethodTypeEnum.Visa, guarantor)
        {
            this.AccountNickName = "TestDeclineCall";
            this.AccountNumber = "5555444433332226";
            this.SecurityNumber = "";
            this.ExpDate = "0429";
            this.BillingAddresses.Add(new PaymentMethodBillingAddressDto
            {
                Address1 = "123 Test St.",
                City = "Somewhere",
                State = "CA",
                Zip = "90001"
            });
        }
    }

    public class TestPaymentMethodCardError : TestPaymentMethodCard
    {
        public TestPaymentMethodCardError(VisitPayUserDto userDto, GuarantorDto guarantor) : base(userDto, PaymentMethodTypeEnum.Visa, guarantor)
        {
            this.AccountNickName = "TestDeclineCardError";
            this.AccountNumber = "4444111144441111";
            this.SecurityNumber = "";
            this.ExpDate = "0429";
            this.BillingAddresses.Add(new PaymentMethodBillingAddressDto
            {
                Address1 = "123 Test St.",
                City = "Somewhere",
                State = "CA",
                Zip = "90001"
            });
        }
    }

    public class TestPaymentMethodExpired : TestPaymentMethodCard
    {
        public TestPaymentMethodExpired(VisitPayUserDto userDto, GuarantorDto guarantor)
            : base(userDto, PaymentMethodTypeEnum.Visa, guarantor)
        {
            this.AccountNickName = "TestExpired";
            this.AccountNumber = "4111111111111111";
            this.SecurityNumber = "123";
            this.ExpDate = "0115";
            this.BillingAddresses.Add(new PaymentMethodBillingAddressDto
            {
                Address1 = "123 Test St.",
                City = "Somewhere",
                State = "CA",
                Zip = "90001"
            });
        }
    }

    #endregion

    #region good ach

    public class TestPaymentMethodAchCheckingGood : TestPaymentMethodAch
    {
        public TestPaymentMethodAchCheckingGood(
            VisitPayUserDto userDto, 
            GuarantorDto guarantor)
            : base(userDto, PaymentMethodTypeEnum.AchChecking, guarantor)
        {
            this.AccountNickName = "TestAchCheckingGood";
            this.AccountNumber = "55544433221";
            this.RoutingNumber = "789456124";
        }
    }

    public class TestPaymentMethodAchSavingsGood : TestPaymentMethodAch
    {
        public TestPaymentMethodAchSavingsGood(
            VisitPayUserDto userDto,
            GuarantorDto guarantor)
            : base(userDto, PaymentMethodTypeEnum.AchSavings, guarantor)
        {
            this.AccountNickName = "TestAchSavingsGood";
            this.AccountNumber = "55544433221";
            this.RoutingNumber = "789456124";
        }
    }

    #endregion

    #region bad ach

    public class TestPaymentMethodAchCheckingBad : TestPaymentMethodAch
    {
        public TestPaymentMethodAchCheckingBad(
            VisitPayUserDto userDto, 
            GuarantorDto guarantor)
            : base(userDto, PaymentMethodTypeEnum.AchChecking, guarantor)
        {
            this.AccountNickName = "TestAchCheckingBad";
            this.AccountNumber = "10000000002";
            this.RoutingNumber = "789456124";
        }
    }

    public class TestPaymentMethodAchSavingsBad : TestPaymentMethodAch
    {
        public TestPaymentMethodAchSavingsBad(
            VisitPayUserDto userDto,
            GuarantorDto guarantor)
            : base(userDto, PaymentMethodTypeEnum.AchSavings, guarantor)
        {
            this.AccountNickName = "TestAchCheckingBad";
            this.AccountNumber = "10000000002";
            this.RoutingNumber = "789456124";
        }
    }

    #endregion
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Ivh.Web.QATools.Models
{
    using System.ComponentModel.DataAnnotations;
    using Application.FinanceManagement.Common.Dtos;

    public class FinancePlanMinPaymentTierViewModel
    {
        [Required]
        [Display(Name = "Id")]
        public int FinancePlanMinimumPaymentTierId { get; set; }

        [Required]
        [Display(Name = "Exisiting Recurring Payment Total")]
        public decimal ExisitingRecurringPaymentTotal { get; set; }

        [Required]
        [Display(Name = "Minimum Payment")]
        public decimal MinimumPayment { get; set; }

        public IList<FinancePlanMinPaymentTierViewModel> FinancePlanMinPaymentTiers { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Ivh.Application.Core.Common.Dtos;
using Ivh.Common.Base.Utilities.Helpers;

namespace Ivh.Web.QATools.Models
{
    public class GuarantorViewModel
    {
        public string UserName {get; set;}
        public string FirstName {get; set;}
        public string LastName {get; set;}
        public short PaymentDueDay {get; set;}
        public DateTime RegistrationDate {get; set;}
        public int VisitPayUserId { get; set; }
        public int VpGuarantorId { get; set; }
        public string LastNameFirstName => FormatHelper.LastNameFirstName(this.LastName, this.FirstName);
        public string FirstNameLastName => FormatHelper.FirstNameLastName(this.FirstName, this.LastName);
    }
}
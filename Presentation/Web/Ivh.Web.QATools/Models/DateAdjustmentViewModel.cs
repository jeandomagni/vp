﻿namespace Ivh.Web.QATools.Models
{
    public class DateAdjustmentViewModel
    {
        public int guarantorId { get; set; }
        public string datePart { get; set; }
        public int adjustment { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Ivh.Web.QATools.Models
{
    public class VisitTransactionViewModel
    {
        public string UserName { get; set; }
        public int VisitID { get; set; }
        public DateTime TransactionDate { get; set; }
        public double TransactionAmount { get; set; }
        public int VpTransactionTypeID { get; set; }
        public string TransactionDescription { get; set; }
        public DateTime InsertDate { get; set; } // translate to datetimeoffset(2)
        public int RelatedChangeToVisitStateId { get; set; } 
        public int VpGuarantorId { get; set; }
        public bool ReassessmentExempt { get; set; }
    }
}
namespace Ivh.Web.QATools.Models
{
    using Application.Core.Common.Dtos;

    public class SsoGeneratorViewModel
    {
        public GuarantorDto Guarantor { get; set; }
    }
}
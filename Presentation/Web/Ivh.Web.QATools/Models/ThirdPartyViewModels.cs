﻿namespace Ivh.Web.QATools.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.Web.Mvc;
    using Common.Base.Enums;
    using Common.VisitPay.Enums;

    public class ThirdPartyViewModel
    {
        [Required]
        [Display(Name = "Transaction ID(s) [separate with ';']")]
        public string TransactionIds { get; set; }
        
        [Required]
        [Display(Name = "Ach File Type")]
        public AchFileTypeEnum AchFileType { get; set; }
        
        [Required]
        [Display(Name = "Download Date")]
        public DateTime DownloadDate { get; set; }

        [Display(Name = "Return Reason Code (returns only, or return as settlement)")]
        public AchReturnCodeEnum ReturnReasonCode { get; set; }

        [Display(Name = "Return Amount (returns only)")]
        public decimal ReturnAmount { get; set; }

        [Display(Name = "Process Settlements & Returns")]
        public bool Process { get; set; }

        public IList<SelectListItem> AchFileTypes { get; set; }

        public IList<SelectListItem> AchReturnCodes { get; set; }
    }
}
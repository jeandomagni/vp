﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Ivh.Web.QATools.Models
{
    public class TextRegionLocaleSupport
    {
        public string LocaleName { get; set; }
        public bool IsSupported { get; set; }
    }
}
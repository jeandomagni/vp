namespace Ivh.Web.QATools.Models
{
    public class SsoResponseModel
    {
        public string SsoUrl { get; set; }
        public string UrlExpiration { get; set; }
    }
}
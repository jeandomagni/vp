﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Ivh.Web.QATools.Models
{
    public class TextRegionInfo
    {
        public TextRegionInfo()
        {
            LocaleSupport = new List<TextRegionLocaleSupport>();
        }

        public string TextRegionPropertyName { get; set; }

        public string TextRegionKey { get; set; }

        public List<TextRegionLocaleSupport> LocaleSupport { get; set; }
    }
}
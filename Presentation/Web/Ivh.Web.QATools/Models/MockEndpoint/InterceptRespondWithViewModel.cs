﻿namespace Ivh.Web.QATools.Models.MockEndpoint
{
    using System.Collections.Generic;
    using System.Web.Mvc;

    public class InterceptRespondWithViewModel
    {
        public InterceptRespondWithViewModel()
        {
            this.StatusCodesCommon = new List<SelectListItem>();
            this.StatusCodesOther = new List<SelectListItem>();
        }

        public int? StatusCode { get; set; }
        public IList<SelectListItem> StatusCodesCommon { get; set; }
        public IList<SelectListItem> StatusCodesOther { get; set; }
    }
}
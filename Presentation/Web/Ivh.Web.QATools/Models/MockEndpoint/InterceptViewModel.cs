﻿namespace Ivh.Web.QATools.Models.MockEndpoint
{
    public class InterceptViewModel
    {
        public InterceptViewModel()
        {
            this.RespondWith = new InterceptRespondWithViewModel();
        }

        public string UrlName { get; set; }
        public string KeyRequest { get; set; }
        public string KeyResponse { get; set; }
        public InterceptRespondWithViewModel RespondWith { get; set; }
        public bool HasResponsed { get; set; }
    }
}
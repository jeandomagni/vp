﻿namespace Ivh.Web.QATools.Models.MockEndpoint
{
    using System.Collections.Generic;
    using System.Web.Mvc;

    public class InterceptorsViewModel
    {
        public IList<SelectListItem> Urls { get; set; }
    }
}
namespace Ivh.Web.QATools.Models
{
    public class MockAuthenticationAuthenticateResponse
    {
        public string clientid { get; set; }
        public string token { get; set; }
    }
}
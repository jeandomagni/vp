﻿namespace Ivh.Web.QATools.Models.MockPaymentProcessor
{
    using System.Collections.Generic;
    using System.Web.Mvc;
    using Application.Qat.Common.Enums;
    using Common.Base.Enums;
    using Common.Base.Utilities.Helpers;
    using Common.VisitPay.Enums;
    using Ivh.Common.Base.Utilities.Extensions;

    public class MockPaymentViewModel
    {
        public MockPaymentViewModel()
        {
            this.AvsCodes = new List<SelectListItem>();
            this.ResponseTypes = new List<SelectListItem>();
        }

        public string RequestGuid { get; set; }
        public string Action { get; set; }
        public string Amount { get; set; }

        public string VpAccountName { get; set; }
        public string VpLastFour { get; set; }
        public PaymentMethodTypeEnum VpPaymentMethodType { get; set; }

        public PpiResponseTypeEnum? CompletedResponseType { get; set; }

        public string CompletedResponseAvs { get; set; }

        public string CompletedResponseTypeText
        {
            get
            {
                if (!this.CompletedResponseType.HasValue)
                {
                    return string.Empty;
                }

                return this.CompletedResponseType.ToString();
            }
        }

        public IList<SelectListItem> ResponseTypes { get; set; }

        public IList<SelectListItem> AvsCodes { get; set; }

        public bool IsPending 
        {
            get { return this.CompletedResponseType == null; }
        }

        public bool IsSuccess 
        {
            get
            {
                if (this.CompletedResponseType == null)
                {
                    return false;
                }

                return this.CompletedResponseType.Value.IsInCategory(PpiResponseTypeEnumCategory.Success);
            }
        }

        public bool IsFail
        {
            get
            {
                return !this.IsPending && !this.IsSuccess;
            }
        }
    }
}
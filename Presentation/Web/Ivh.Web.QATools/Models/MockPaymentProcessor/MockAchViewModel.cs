﻿using System;

namespace Ivh.Web.QATools.Models.MockPaymentProcessor
{
    using System.Collections.Generic;
    using System.Web.Mvc;
    using Common.Base.Enums;
    using Common.VisitPay.Enums;

    public class MockAchViewModel
    {
        public MockAchViewModel()
        {
            this.AchFileTypes = new List<SelectListItem>();
            this.AchReturnCodes = new List<SelectListItem>();
        }

        public string RequestGuid { get; set; }
        public string Action { get; set; }
        public string Amount { get; set; }

        public string VpAccountName { get; set; }
        public string VpLastFour { get; set; }

        public AchFileTypeEnum AchFileType { get; set; }
        public string AchProcessDate { get; set; }
        public AchReturnCodeEnum? AchReturnCode { get; set; }
        public string AchReturnAmount { get; set; }
        public bool AchProcessed { get; set; }

        public IList<SelectListItem> AchFileTypes { get; set; }
        public IList<SelectListItem> AchReturnCodes { get; set; }

        public bool IsAchActionSet
        {
            get { return !string.IsNullOrEmpty(this.AchProcessDate); }
        }
    }
}
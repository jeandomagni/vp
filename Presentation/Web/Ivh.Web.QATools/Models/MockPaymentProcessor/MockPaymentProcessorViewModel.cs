﻿namespace Ivh.Web.QATools.Models.MockPaymentProcessor
{
    using System.Collections.Generic;
    using System.Web.Mvc;

    public class MockPaymentProcessorViewModel
    {
        public MockPaymentProcessorViewModel()
        {
            this.Payments = new List<MockPaymentViewModel>();
        }

        public bool DefaultSuccessful { get; set; }
        public int DelayMs { get; set; }
        public bool IsActive { get; set; }

        public IList<SelectListItem> DefaultActions
        {
            get
            {
                return new List<SelectListItem>
                {
                    new SelectListItem {Text = "Success", Value = "true"},
                    new SelectListItem {Text = "Fail", Value = "false"}
                };
            }
        }

        public IList<SelectListItem> Delays
        {
            get
            {
                return new List<SelectListItem>
                {
                    new SelectListItem {Text = "None", Value = (1).ToString()},
                    new SelectListItem {Text = "15 seconds", Value = (15*1000).ToString()},
                    new SelectListItem {Text = "30 seconds", Value = (30*1000).ToString()},
                    new SelectListItem {Text = "60 seconds", Value = (60*1000).ToString()}
                };
            }
        }

        public IList<MockPaymentViewModel> Payments { get; set; }
    }
}
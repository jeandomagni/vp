﻿namespace Ivh.Web.QATools.Models.MockPaymentProcessor
{
    using System.Collections.Generic;

    public class MockAchProcessorViewModel
    {
        public MockAchProcessorViewModel()
        {
            this.Payments = new List<MockAchViewModel>();
        }

        public IList<MockAchViewModel> Payments { get; set; }
    }
}
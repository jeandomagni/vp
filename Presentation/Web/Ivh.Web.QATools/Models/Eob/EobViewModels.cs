﻿namespace Ivh.Web.QATools.Models.Eob
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.Web.Mvc;
    using Common.Web.Attributes;

    [MapKo("Eob/add-835-vm.js")]
    public class Add835ViewModel
    {
        public Add835ViewModel()
        {
            this.FunctionalGroupHeaderTrailerHeaders = new List<FunctionalGroupHeaderHeaderTrailerViewModel>();
            this.ClaimAdjustmentReasonCodes = new List<SelectListItem>();
            this.EobRemittanceAdviceRemarkCodes = new List<SelectListItem>();
        }

        [Required]
        public int VisitId { get; set; }

        public IList<FunctionalGroupHeaderHeaderTrailerViewModel> FunctionalGroupHeaderTrailerHeaders { get; set; }

        public IList<SelectListItem> AdjudicationInformationTypes => new List<SelectListItem>
        {
            new SelectListItem {Text = "-", Value = "None"},
            new SelectListItem {Text = "Inpatient", Value = "Inpatient"},
            new SelectListItem {Text = "Outpatient", Value = "Outpatient"},
        };

        public IList<SelectListItem> ClaimStatusCodes => new List<SelectListItem>
        {
            new SelectListItem { Text = "Processed as Primary", Value = "1" },
            new SelectListItem { Text = "Processed as Secondary", Value = "2" },
            new SelectListItem { Text = "Processed as Tertiary", Value = "3" },
            new SelectListItem { Text = "Denied", Value = "4" },
            new SelectListItem { Text = "Processed as Primary, Fwd to Addtl Payer", Value = "19" },
            new SelectListItem { Text = "Processed as Secondary, Fwd to Addtl Payer", Value = "20" },
            new SelectListItem { Text = "Processed as Tertiary, Fwd to Addtl Payer", Value = "21" },
            new SelectListItem { Text = "Reversal of Previous Payment", Value = "22" },
            new SelectListItem { Text = "Not our Claim, Fwd to Addtl Payer", Value = "23" },
            new SelectListItem { Text = "Predetermination Pricing Only - No Pymt", Value = "25" },
        };

        public IList<SelectListItem> ClaimAdjustmentGroupCodes => new List<SelectListItem>
        {
            new SelectListItem {Text = "Contractual Obligations", Value = "CO"},
            new SelectListItem {Text = "Other Adjustments", Value = "OA"},
            new SelectListItem {Text = "Payer Initiated Reductions", Value = "PI"},
            new SelectListItem {Text = "Patient Responsibility", Value = "PR"}
        };

        public IList<SelectListItem> ClaimAdjustmentReasonCodes { get; set; }

        public IList<SelectListItem> EobRemittanceAdviceRemarkCodes { get; set; }

        public IList<SelectListItem> PayerFilters { get; set; }
    }

    [MapKo("Eob/functional-group-header-trailer-vm.js")]
    public class FunctionalGroupHeaderHeaderTrailerViewModel
    {
        public FunctionalGroupHeaderHeaderTrailerViewModel()
        {
            this.TransactionHeaders = new List<TransactionSetHeaderTrailer835ViewModel>();
        }

        public IList<TransactionSetHeaderTrailer835ViewModel> TransactionHeaders { get; set; }
    }

    [MapKo("Eob/transaction-set-header-trailer835-vm.js")]
    public class TransactionSetHeaderTrailer835ViewModel
    {
        public TransactionSetHeaderTrailer835ViewModel()
        {
            this.Transactions = new List<HeaderNumberViewModel>();
            this.PayerFilterId = 0;
        }

        [Required]
        [Display(Name = "Remit Date")]
        public DateTime? DtmProductionDate { get; set; }

        public IList<HeaderNumberViewModel> Transactions { get; set; }
        public int PayerFilterId { get; set; }
    }

    [MapKo("Eob/header-number-vm.js")]
    public class HeaderNumberViewModel
    {
        public HeaderNumberViewModel()
        {
            this.ClaimPaymentInformations = new List<ClaimPaymentInformationViewModel>();
        }

        public IList<ClaimPaymentInformationViewModel> ClaimPaymentInformations { get; set; }
    }

    [MapKo("Eob/claim-payment-information-vm.js")]
    public class ClaimPaymentInformationViewModel
    {
        public ClaimPaymentInformationViewModel()
        {
            this.AdjudicationInformation = new AdjudicationInformationViewModel();
            this.ClaimAdjustmentPivots = new List<ClaimAdjustmentPivotViewModel>();
            this.ServicePaymentInformations = new List<ServicePaymentInformationViewModel>();
        }

        [Required]
        [Display(Name = "Hospital Claim #")]
        public string Clp01ClaimSubmittersIdentifier { get; set; }

        [Required]
        [Display(Name = "Claim Status Code")]
        public string Clp02ClaimStatusCode { get; set; }

        [Required]
        [Display(Name = "Total Claim Charge")]
        public decimal? Clp03MonetaryAmount { get; set; }

        [Required]
        [Display(Name = "Claim Payment Amount")]
        public decimal? Clp04MonetaryAmount { get; set; }

        [Required]
        [Display(Name = "Patient Resp. Amount")]
        public decimal? Clp05MonetaryAmount { get; set; }

        [Required]
        [Display(Name = "Insurance Claim #")]
        public string Clp07ReferenceIdentifier { get; set; }

        [Display(Name = "Plan ID#")]
        [Required]
        public string Nm109IdentificationCode { get; set; }

        public AdjudicationInformationViewModel AdjudicationInformation { get; set; }
        public IList<ClaimAdjustmentPivotViewModel> ClaimAdjustmentPivots { get; set; }
        public IList<ServicePaymentInformationViewModel> ServicePaymentInformations { get; set; }
    }

    [MapKo("Eob/claim-adjustment-pivot-vm.js")]
    public class ClaimAdjustmentPivotViewModel
    {
        [Required]
        [Display(Name = "Claim Adj Group Code")]
        public string ClaimAdjustmentGroupCode { get; set; }

        [Required]
        [Display(Name = "Claim Adj Reason Code")]
        public string ClaimAdjustmentReasonCode { get; set; }

        [Required]
        [Display(Name = "Amount")]
        public decimal? MonetaryAmount { get; set; }
    }

    [MapKo("Eob/adjudication-information-vm.js")]
    public class AdjudicationInformationViewModel
    {
        [Display(Name = "Adjudication Type")]
        public string AdjudicationInformationType { get; set; }

        [Display(Name = "Claim Level Remark Code")]
        public string MiaMoaRemarkCode { get; set; }
    }

    [MapKo("Eob/service-payment-information-vm.js")]
    public class ServicePaymentInformationViewModel
    {
        public ServicePaymentInformationViewModel()
        {
            this.ClaimAdjustmentPivots = new List<ClaimAdjustmentPivotViewModel>();
            this.LqHealthCareRemarkCodes = new List<IndustryCodeIdentificationViewModel>();
        }

        public IList<ClaimAdjustmentPivotViewModel> ClaimAdjustmentPivots { get; set; }

        public IList<IndustryCodeIdentificationViewModel> LqHealthCareRemarkCodes { get; set; }
    }

    [MapKo("Eob/industry-code-identification-vm.js")]
    public class IndustryCodeIdentificationViewModel
    {
        [Display(Name = "Service Level Remark Codes")]
        public string Lq02IndustryCode { get; set; }
    }

    [MapKo("Eob/payer-filters-vm.js")]
    public class PayerFilterViewModel
    {
        public int EobPayerFilterId { get; set; }

        [Display(Name = "Payer Name")]
        public string UniquePayerValue { get; set; }
    }
}
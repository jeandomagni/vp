﻿namespace Ivh.Web.QATools.Models
{
    public class ScoringThirdPartyResponse
    {
        public virtual int ScoringThirdPartyResponseId { get; set; }
        public virtual string FirstName { get; set; }
        public virtual string LastName { get; set; }
        public virtual string AddressLine1 { get; set; }
        public virtual string StateProvince { get; set; }
        public virtual string PostalCode { get; set; }
        public virtual string MessageId { get; set; }
        public virtual string ResponseType { get; set; }
        public virtual string Response { get; set; }
        public virtual int? ReturnErrorCode { get; set; }
    }
}
﻿namespace Ivh.Web.QATools.Models.Setting
{
    using System.ComponentModel.DataAnnotations;
    using System.Web;

    public class LogoSettingViewModel
    {
        [Required]
        [FileExtensions(Extensions = ".png,.jpg,.jpeg", ErrorMessage = "Only .png,.jpg and .jpeg allowed")]
        public HttpPostedFileBase LogoFile { get; set; }

        public string LogoSettingKey { get; set; } = @"/theme.client-logo-path";
    }
}
﻿namespace Ivh.Web.QATools.Models.Setting
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    public class DemoSettingsViewModel
    {
        public DemoSettingsViewModel()
        {
        }

        public DemoSettingsViewModel(string colorPaletteName)
        {
            List<ColorPalette> colorPalettes = this.GetColorPalettes();
            this.ColorPalette = colorPaletteName == "" ? colorPalettes.FirstOrDefault() : colorPalettes.FirstOrDefault(x => x.PaletteName == colorPaletteName);
            this.SettingKeyValues = this.ColorPalette?.GetSettingsKeyValues().OrderBy(x => x.SettingKey).ToList();
        }

        public List<SettingKeyValueViewModel> SettingKeyValues { get; set; }

        public ColorPalette ColorPalette { get; set; }

        // offer pre-packaged selection (a base in which to start)
        public List<ColorPalette> ColorPalettes => this.GetColorPalettes();

        private List<ColorPalette> GetColorPalettes()
        {
            ColorPalette intermountain = new ColorPalette
            {
                PaletteName = "Intermountain",
                PrimaryColor = "#024985",
                PrimarySecondaryColor = "#3766b1",
                AccentColor = "#8dc840",
                AccentSecondaryColor = "#4a7235",
                LinkColor = "#024985"
            };

            ColorPalette prov = new ColorPalette
            {
                PaletteName = "prov",
                PrimaryColor = "#003779",
                PrimarySecondaryColor = "#005498",
                AccentColor = "#ef8522",
                AccentSecondaryColor = "#fdcb18",
                LinkColor = "#003779"
            };

            ColorPalette stlukes = new ColorPalette
            {
                PaletteName = "stlukes",
                PrimaryColor = "#014a8e",
                PrimarySecondaryColor = "#f45500",
                AccentColor = "#ff7a32",
                AccentSecondaryColor = "#f45500",
                LinkColor = "#014a8e"
            };

            return new List<ColorPalette> {intermountain, prov, stlukes};
        }
    }

    public class SettingKeyValueViewModel
    {
        public string SettingKey { get; set; }
        public string SettingValue { get; set; }
        public string CssFriendlyName => this.GetCssFriendlyName();

        // currently we are expecting hex but the jscolor control does not add the # sign
        public string SettingValueHex => this.SettingValue.IndexOf("#", StringComparison.Ordinal) < 0 ? $"#{this.SettingValue}" : this.SettingValue;

        private string GetCssFriendlyName()
        {
            string cssFriendlyName = this.SettingKey.Replace(".", "-");
            cssFriendlyName = cssFriendlyName.Replace("-hover", ":hover");
            cssFriendlyName = cssFriendlyName.Replace(".", "-");
            cssFriendlyName = cssFriendlyName.Replace("/", ".");
            return cssFriendlyName;
        }
    }

    public class ColorPalette
    {
        public string PaletteName { get; set; }
        public string PrimaryColor { get; set; } //base color typically used for headers
        public string PrimarySecondaryColor { get; set; } //a touch lighter than the primary - some use primary or accent for this

        public string AccentColor { get; set; } //used for payment button and other things
        public string AccentSecondaryColor { get; set; } //could be a hover for AccentColor

        public string LinkColor { get; set; } // typically will be a dark blue no matter the color palette

        public List<KeyValuePair<string, string>> ColorProperties => new List<KeyValuePair<string, string>>
        {
            new KeyValuePair<string, string>(nameof(this.PrimaryColor), this.PrimaryColor),
            new KeyValuePair<string, string>(nameof(this.PrimarySecondaryColor), this.PrimarySecondaryColor),
            new KeyValuePair<string, string>(nameof(this.AccentColor), this.AccentColor),
            new KeyValuePair<string, string>(nameof(this.AccentSecondaryColor), this.AccentSecondaryColor),
            new KeyValuePair<string, string>(nameof(this.LinkColor), this.LinkColor)
        };

        //the idea here is to set a default for groups of settings
        // it is not going to be perfect but is a general guide 
        // the user will still be able to edit individual theme colors
        public Dictionary<string, List<string>> ColorPaletteThemeSettingAssignments => new Dictionary<string, List<string>>
        {
            [nameof(this.PrimaryColor)] = new List<string>
            {
                "/theme.client-button-primary-bg-hover",
                "/theme.client-primary"
            },
            [nameof(this.PrimarySecondaryColor)] = new List<string>
            {
                "/theme.client-button-primary-bg"
            },
            [nameof(this.AccentColor)] = new List<string>
            {
                "/theme.client-accent",
                "/theme.client-button-payment-bg"
            },
            [nameof(this.AccentSecondaryColor)] = new List<string>
            {
                "/theme.client-link-hover-color",
                "/theme.client-button-payment-bg-hover"
            },
            [nameof(this.LinkColor)] = new List<string>
            {
                "/theme.client-link-color"
            }
        };

        public List<SettingKeyValueViewModel> GetSettingsKeyValues()
        {
            List<SettingKeyValueViewModel> settingKeyValues = new List<SettingKeyValueViewModel>();

            foreach (KeyValuePair<string, string> prop in this.ColorProperties)
            {
                List<string> themes = this.ColorPaletteThemeSettingAssignments[prop.Key];
                foreach (string theme in themes)
                {
                    SettingKeyValueViewModel settingKeyValue = new SettingKeyValueViewModel {SettingKey = theme, SettingValue = prop.Value};
                    settingKeyValues.Add(settingKeyValue);
                }
            }

            return settingKeyValues;
        }
    }
}
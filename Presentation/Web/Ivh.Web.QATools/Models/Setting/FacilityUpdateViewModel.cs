﻿namespace Ivh.Web.QATools.Models.Setting
{
    using System.Web;

    public class FacilityUpdateViewModel
    {
        public HttpPostedFileBase LogoFile { get; set; }

        public string SourceSystemKey { get; set; }

        public int FacilityId { get; set; }
        
        public string FacilityName { get; set; }

        public string LogoFileName { get; set; }
    }
}
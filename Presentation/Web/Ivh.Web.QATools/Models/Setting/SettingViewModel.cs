﻿namespace Ivh.Web.QATools.Models.Setting
{
    using System.Collections.Generic;
    using System.Linq;

    public class SettingViewModel
    {
        public IReadOnlyDictionary<string, string> ManagedSettings { get; set; }
        public IReadOnlyDictionary<string, string> EditableManagedSettings { get; set; }
        public string SettingName { get; set; }
        public string SettingValue { get; set; }
        public IEnumerable<KeyValuePair<string, string>> DisabledManagedSettings => this.ManagedSettings.ToList().Except(this.EditableManagedSettings.ToList());
        public LogoSettingViewModel LogoSettingViewModel { get; set; } = new LogoSettingViewModel();
    }
}
﻿namespace Ivh.Web.QATools.Models
{
    using System.ComponentModel.DataAnnotations;

    public class QaToolsDataDeploymentViewModel
    {
        [Display(Name = "Select Client")]
        public string ClientName { get; set; }
    }
}

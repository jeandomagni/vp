﻿namespace Ivh.Web.QATools.Models
{
    using System;
    using Application.Core.Common.Dtos;

    public class GuarantorDetailViewModel
    {
        public GuarantorDto Data { get; set; }
        public int VpGuarantorStatusId { get; set; }
        public bool HasVisits { get; set; }
        public string NextPaymentDueDate { get; set; }


        public string MyChartSsoLink { get; set; }
        public DateTime MyChartSsoTokenTimeoutTime { get; set; }

        public bool IsGracePeriod { get; set; }
        public bool IsAwaitingStatement { get; set; }
    }
}
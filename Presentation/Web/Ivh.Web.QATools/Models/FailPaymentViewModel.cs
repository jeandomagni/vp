﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Ivh.Web.QATools.Models
{
    using System.ComponentModel.DataAnnotations;
    using System.Web.Mvc;
    using Common.Base.Enums;
    using Common.VisitPay.Enums;

    public class FailPaymentViewModel
    {   
        public FailedPaymentErrorCodeEnum FailPaymentErrorCode { get; set; }

        
        public IList<SelectListItem> FailPaymentErrorCodes { get; set; }
    }
}
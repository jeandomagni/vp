﻿namespace Ivh.Web.QATools.Models
{
    using System.ComponentModel.DataAnnotations;

    public class ExportPersonasViewModel
    {
        [Display(Prompt = "Last Name Contains... (Separate with a comma)")]
        public string Search { get; set; }
    }
}
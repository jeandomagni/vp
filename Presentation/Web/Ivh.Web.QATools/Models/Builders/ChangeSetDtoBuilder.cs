﻿namespace Ivh.Web.QATools.Models.Builders
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Common.Base.Enums;
    using Common.Base.Utilities.Extensions;
    using Common.Base.Utilities.Helpers;
    using Common.VisitPay.Enums;
    using Common.VisitPay.Messages.HospitalData.Dtos;
    using Etl;

    public class ChangeSetDtoBuilder
    {
        private readonly ChangeSetDto _changeSetDto;

        public ChangeSetDtoBuilder(ChangeSetDto changeSetDto)
        {
            this._changeSetDto = changeSetDto;
        }

        #region visit data

        public ChangeSetDtoBuilder MergeVisit(CreateChangeSetViewModel model)
        {
            VisitChangeDto visitChangeDto = this.GetVisitChangeDto();
            if (visitChangeDto == null)
            {
                return this;
            }

            visitChangeDto.ChangeType = ChangeTypeEnum.Update;
            visitChangeDto.IsChanged = true;
            visitChangeDto.AdmitDate = model.AdmitDate;
            visitChangeDto.BillingApplication = model.BillingApplication;
            visitChangeDto.DischargeDate = model.DischargeDate;
            visitChangeDto.HsCurrentBalance = model.HsCurrentBalance;
            visitChangeDto.FirstSelfPayDate = model.FirstSelfPayDate;
            visitChangeDto.SelfPayBalance = model.SelfPayBalance;
            visitChangeDto.LifeCycleStage = new LifeCycleStageDto { LifeCycleStageId = model.LifeCycleStageId };
            visitChangeDto.PatientFirstName = model.PatientFirstName;
            visitChangeDto.PatientLastName = model.PatientLastName;
            visitChangeDto.VisitDescription = model.VisitDescription;
            visitChangeDto.HsGuarantorId = model.HsGuarantorId;
            visitChangeDto.SourceSystemKeyDisplay = model.VisitSourceSystemKeyDisplay;
            visitChangeDto.IsPatientGuarantor = model.IsPatientGuarantor;
            visitChangeDto.IsPatientMinor = model.IsPatientMinor;
            visitChangeDto.InterestZero = model.InterestZero;
            visitChangeDto.InterestRefund = model.InterestRefund;
            visitChangeDto.Facility = model.FacilityId.HasValue ? new FacilityDto { FacilityId = model.FacilityId } : null;
            visitChangeDto.PaymentPriority = model.PaymentPriority;

            if (model.AgingTier != null)
            {
                visitChangeDto.AgingTier = (AgingTierEnum) model.AgingTier;
            }

            visitChangeDto.VpEligible = model.VpEligible;
            visitChangeDto.BillingHold = model.BillingHold;
            visitChangeDto.Redact = model.Redact;
            visitChangeDto.ServiceGroupId = model.ServiceGroupId;

            return this;
        }

        public ChangeSetDtoBuilder MergeTransactions(IList<VisitTransactionViewModel> model)
        {
            VisitChangeDto visitChangeDto = this.GetVisitChangeDto();
            if (visitChangeDto == null)
            {
                return this;
            }

            foreach (VisitTransactionViewModel transactionViewModel in model.Where(w => w.VpTransactionTypeId.GetValueOrDefault(0) != 0))
            {
                VisitTransactionChangeDto visitTransactionChangeDto = visitChangeDto.VisitTransactions.FirstOrDefault(x => x.VisitTransactionId != default(int) && x.VisitTransactionId == transactionViewModel.VisitTransactionId);
                if (visitTransactionChangeDto == null)
                {
                    visitTransactionChangeDto = new VisitTransactionChangeDto
                    {
                        ChangeType = ChangeTypeEnum.Insert,
                        SourceSystemKey = RandomSourceSystemKeyGenerator.New("NNNNNNNNNN"),
                        BillingSystemId = visitChangeDto.BillingSystemId,
                        TransactionCodeId = transactionViewModel.TransactionCode ?? int.MinValue,
                        VisitId = visitChangeDto.VisitId,
                        ReassessmentExempt = transactionViewModel.ReassessmentExempt
                };
                    visitChangeDto.VisitTransactions.Add(visitTransactionChangeDto);
                }
                else
                {
                    visitTransactionChangeDto.ChangeType = ChangeTypeEnum.Update;
                }

                visitTransactionChangeDto.PaymentAllocationId = transactionViewModel.PaymentAllocationId;
                visitTransactionChangeDto.PostDate = transactionViewModel.PostDate.GetValueOrDefault(DateTime.UtcNow);
                visitTransactionChangeDto.TransactionAmount = transactionViewModel.TransactionAmount.GetValueOrDefault(0);
                visitTransactionChangeDto.TransactionDate = transactionViewModel.TransactionDate.GetValueOrDefault(DateTime.UtcNow);
                visitTransactionChangeDto.TransactionDescription = transactionViewModel.TransactionDescription;
                visitTransactionChangeDto.VpTransactionTypeId = transactionViewModel.VpTransactionTypeId.Value;
            }

            return this;
        }
        
        public ChangeSetDtoBuilder MergePaymentAllocations(IList<int> paymentAllocationIds)
        {
            VisitChangeDto visitChangeDto = this.GetVisitChangeDto();
            if (visitChangeDto == null)
            {
                return this;
            }

            visitChangeDto.VisitPaymentAllocations = visitChangeDto.VisitPaymentAllocations ?? new List<VisitPaymentAllocationChangeDto>();

            paymentAllocationIds = paymentAllocationIds.Where(x => !visitChangeDto.VisitPaymentAllocations.Select(z => z.PaymentAllocationId).Contains(x)).ToList();

            visitChangeDto.VisitPaymentAllocations.AddRange(paymentAllocationIds.Select(x => new VisitPaymentAllocationChangeDto
            {
                PaymentAllocationId = x,
                VisitId = visitChangeDto.VisitId
            }));

            return this;
        }

        public ChangeSetDtoBuilder SetVisitBalance(decimal? balance = null)
        {
            VisitChangeDto visitChangeDto = this.GetVisitChangeDto();
            if (visitChangeDto == null)
            {
                return this;
            }

            if (balance.HasValue)
            {
                visitChangeDto.HsCurrentBalance = balance.Value;
            }
            else
            {
                visitChangeDto.HsCurrentBalance = visitChangeDto.VisitTransactions.Where(x => x.VpTransactionTypeId != (int)VpTransactionTypeEnum.VppInterestPayment).Sum(x => x.TransactionAmount);
            }

            return this;
        }

        private VisitChangeDto GetVisitChangeDto()
        {
            if (this._changeSetDto.VisitChanges.IsNullOrEmpty())
            {
                return null;
            }

            VisitChangeDto visitChangeDto = this._changeSetDto.VisitChanges[0];
            visitChangeDto.ChangeType = ChangeTypeEnum.Update;
            visitChangeDto.IsChanged = true;

            return visitChangeDto;
        }

        #endregion

        #region change events
        
        public ChangeSetDtoBuilder AddChangeEventScoring(bool condition, string eventTriggerDescription, int visitId)
        {
            if (condition)
            {
                this.AddChangeEvent(DateTime.UtcNow, ChangeEventTypeEnum.NewFirstSelfPayDate, eventTriggerDescription, visitId);
            }

            return this;
        }

        public ChangeSetDtoBuilder AddChangeEventVisitTransactionDataChanged(bool condition, string eventTriggerDescription)
        {
            if (condition)
            {
                this.AddChangeEvent(DateTime.UtcNow, ChangeEventTypeEnum.DataChange, eventTriggerDescription);
            }

            return this;
        }

        private void AddChangeEvent(DateTime changeEventDateTime, ChangeEventTypeEnum changeEventTypeEnum, string eventTriggerDescription, int? visitId = null, int? hsGuarantorId = null)
        {
            this._changeSetDto.ChangeEvents.Add(new ChangeEventDto
            {
                ChangeEventDateTime = changeEventDateTime,
                ChangeEventType = new ChangeEventTypeDto { ChangeEventTypeId = (int)changeEventTypeEnum },
                EventTriggerDescription = eventTriggerDescription,
                VisitId = visitId,
                HsGuarantorId = hsGuarantorId
            });
        }

        #endregion
    }
}
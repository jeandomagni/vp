﻿namespace Ivh.Web.QATools.Models
{
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;

    public class PaymentMethodsViewModel
    {
        public int? GuarantorId { get; set; }
        public string UserName { get; set; }
        public IList<string> PaymentMethods { get; set; }
        public int PaymentMethodsCount { get; set; }

        [Display(Name = "Visa Good")]
        public bool TestPaymentMethodVisaGood { get; set; }

        [Display(Name = "Visa Good (Expired at the end of last month)")]
        public bool TestPaymentMethodVisaGoodExpiredLastMonth { get; set; }

        [Display(Name = "Visa Good (Expires at the end of current month)")]
        public bool TestPaymentMethodVisaGoodExpiringThisMonth { get; set; }

        [Display(Name = "Visa Good (Expires at the end of next month)")]
        public bool TestPaymentMethodVisaGoodExpiringNextMonth { get; set; }

        [Display(Name = "MasterCard Good")]
        public bool TestPaymentMethodMastercardGood { get; set; }

        [Display(Name = "AMEX Good")]
        public bool TestPaymentMethodAmexGood { get; set; }

        [Display(Name = "Discover Good")]
        public bool TestPaymentMethodDiscoverGood { get; set; }

        [Display(Name = "Visa Decline")]
        public bool TestPaymentMethodDecline { get; set; }

        [Display(Name = "Visa Decline/Call")]
        public bool TestPaymentMethodCall { get; set; }

        [Display(Name = "Visa Card Error")]
        public bool TestPaymentMethodCardError { get; set; }

        [Display(Name = "Checking Good")]
        public bool TestPaymentMethodAchCheckingGood { get; set; }

        [Display(Name = "Savings Good")]
        public bool TestPaymentMethodAchSavingsGood { get; set; }

        public bool IsAchEnabled { get; set; }
        public bool IsCreditCardEnabled { get; set; }
    }
}
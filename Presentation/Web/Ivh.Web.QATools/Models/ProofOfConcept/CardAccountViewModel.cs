﻿using System;
using System.Collections.Generic;

namespace Ivh.Web.QATools.Models.ProofOfConcept
{
    using System.ComponentModel.DataAnnotations;
    using System.Globalization;
    using System.Web.Mvc;
    using Common.Base.Enums;
    using Common.VisitPay.Enums;
    using Common.Web.Attributes;

    public class CardAccountViewModel
    {
        public CardAccountViewModel()
        {
            this.PaymentMethodId = 0;
            this.IsPrimary = false;
            this.IsSave = true;
            this.PaymentMethodTypeId = (int) PaymentMethodTypeEnum.Visa;
        }

        public int VpGuarantorId { get; set; }

        public int PaymentMethodId { get; set; }
        public string BillingId { get; set; }

        [Display(Name = "Set as primary payment method")]
        public bool IsPrimary { get; set; }

        [Display(Name = "Save payment method")]
        public bool IsSave { get; set; }

        public bool HasScheduledPayments { get; set; }

        [Display(Name = "Account Nickname", Prompt = "Account Nickname")]
        public string AccountNickName { get; set; }

        public string LastFour { get; set; }

         
        [Display(Name = "Card Type")]
        [Required(ErrorMessage = "Card Type is required.")]
        public int PaymentMethodTypeId { get; set; }

        [Display(Name = "Name on Card", Prompt = "Name on Card")]
        [Required(ErrorMessage = "Name on Card is required.")]
        public string NameOnCard { get; set; }

        [Display(Name = "Card Number", Prompt = "Card Number")]
        [RequiredIf("PaymentMethodId", 0, ErrorMessage = "Card Number is required.")]
        [LuhnCheck(ErrorMessage = "Invalid Card Number.")]
        public string CardNumber { get; set; }

        [Display(Name = "Expiration Date")]
        [Required(ErrorMessage = "Expiration Date (Month) is required.")]
        public int ExpirationMonth { get; set; }

        [Required(ErrorMessage = "Expiration Date (Year) is required.")]
        public int ExpirationYear { get; set; }

        public string ExpirationDisplay
        {
            get
            {
                string month = this.ExpirationMonth.ToString().PadLeft(2, '0');
                string year = (this.ExpirationYear + 2000).ToString();

                return string.Concat(month, "/", year);
            }
        }

        public bool IsExpired { get; set; }

        public bool IsExpiring { get; set; }

        [Display(Name = "Card Code", Prompt = "Card Code")]
        [Required(ErrorMessage = "Card Code is required.")]
        public string CardCode { get; set; }

        [Display(Name = "Billing Address", Prompt = "Address Line 1")]
        [Required(ErrorMessage = "Billing Address Line 1 is required.")]
        public string AddressLine1 { get; set; }

        [Display(Prompt = "Address Line 2")]
        public string AddressLine2 { get; set; }

        [Display(Prompt = "City")]
        [Required(ErrorMessage = "City is required.")]
        public string City { get; set; }

        [Required(ErrorMessage = "State is required.")]
        public string State { get; set; }

        [Display(Prompt = "Zip")]
        [Required(ErrorMessage = "Zip is required.")]
        [DataType("Zip")]
        public string Zip { get; set; }

        public IList<SelectListItem> Months
        {
            get
            { 
                List<SelectListItem> items = new List<SelectListItem>(); 
                for (int i = 1; i <= 12; i++)
                {
                    items.Add(new SelectListItem 
                    {
                        Text = $"{i.ToString().PadLeft(2, '0')} - {CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(i)}",
                        Value = i.ToString()
                    });
                }

                return items;
            }
        }
        public IList<SelectListItem> Years
        {
            get 
            { 
                List<SelectListItem> items = new List<SelectListItem>();
                for (int i = DateTime.UtcNow.Year; i <= DateTime.UtcNow.Year + 10; i++)
                {
                    items.Add(new SelectListItem
                    {
                        Text = i.ToString(),
                        Value = i.ToString().Substring(2, 2)
                    });
                }

                return items;
            }
        }
       
        public string CardImage
        {
            get 
            { 
                string image = ""; 

                switch (this.PaymentMethodTypeId)
                {
                    case 1:
                        image = "/Content/Images/visa.png";
                        break;
                    case 2:
                        image = "/Content/Images/mastercard.png";
                        break;
                    case 3:
                        image = "/Content/Images/amex.png";
                        break;
                    case 4:
                        image = "/Content/Images/discover.png";
                        break;
                }

                return image;
            }
            
        }
    }
}
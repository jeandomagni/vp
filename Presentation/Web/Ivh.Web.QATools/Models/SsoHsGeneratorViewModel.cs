﻿namespace Ivh.Web.QATools.Models
{
    using System;

    public class SsoHsGeneratorViewModel
    {
        public int HsGuarantorId { get; set; }
        public DateTime? DateOfBirth { get; set; }
    }
}
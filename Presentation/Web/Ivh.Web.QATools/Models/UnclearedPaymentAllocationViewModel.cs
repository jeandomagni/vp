﻿namespace Ivh.Web.QATools.Models
{
    public class UnclearedPaymentAllocationViewModel
    {
        public int VpOutboundVisitTransactionId { get; set; }
        public int VpPaymentAllocationId { get; set; }
        public int? VpPaymentAllocationTypeId { get; set; }
        public string InsertDate { get; set; }
        public string TransactionAmount { get; set; }
    }
}
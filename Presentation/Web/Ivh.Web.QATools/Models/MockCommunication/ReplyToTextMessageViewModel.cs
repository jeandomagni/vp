﻿namespace Ivh.Web.QATools.Models.MockCommunication
{
    using System.Collections.Generic;

    public class ReplyToTextMessageViewModel
    {
        public List<PayByTextCommunication> UsersWithPayByTextCommunication { get; set; }
        public string SelectedBody { get; set; }
        public string ReplyText { get; set; }
        public int SelectedSentToUserId { get; set; }
        public string SelectedPhoneNumber { get; set; }
    }

    public class PayByTextCommunication
    {
        public int SentToUserId { get; set; }
        public string UserName { get; set; }
        public string PhoneNumber { get; set; }
        public string Body { get; set; }
    }
    
    
}
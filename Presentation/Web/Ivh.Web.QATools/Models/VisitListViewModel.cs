﻿namespace Ivh.Web.QATools.Models
{
    public class VisitListViewModel
    {
        public int VisitPayUserId { get; set; }
        public int VisitId { get; set; }
        public int BillingId { get; set; }
        public string VisitDescription { get; set; }
        public string SourceSystemKey { get; set; }
    }
}
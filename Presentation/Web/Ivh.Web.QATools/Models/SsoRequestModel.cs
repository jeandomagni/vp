namespace Ivh.Web.QATools.Models
{
    using System;

    public class SsoRequestModel
    {
        public int VpGuarantorId { get; set; }
        public int HsGuarantorId { get; set; }
        public DateTime DateOfBirth { get; set; }
        public string SsoSsk { get; set; }
    }
}
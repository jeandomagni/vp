﻿namespace Ivh.Web.QATools.Models.Persona
{
    using System.Linq;

    public class ManagePersonasViewModel
    {
        public string[] DemoAccountUsernames { get; set; }
        public string[] TrainingAccountUsernames { get; set; }
        public string[] OtherUsernames { get; set; } = new string[0];
        public string[] HsDataScenarios { get; set;} = new string[0];
        public int[] AdditionalCloneOptions { get; set; } = new int[0];

        public string[] Usernames
        {
            get { return this.DemoAccountUsernames.Concat(this.TrainingAccountUsernames).Concat(this.OtherUsernames).OrderBy(x => x).ToArray(); }
        }
    }
}
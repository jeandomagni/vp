﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Ivh.Web.QATools.Models.Persona
{
    using System.ComponentModel.DataAnnotations;

    public class PersonaCreateViewModel
    {
        [Display(Name="User Name")]
        public string PersonaUserName { get; set; }

        [Display(Name = "Password")]
        public string Password { get; set; }
        public List<string> PersonaUserNames { get; set; }
    }
}
﻿namespace Ivh.Web.QATools.Models.Persona
{
    public class PersonaViewModel
    {
        public int VpGuarantorId { get; set; }
        public int VisitPayUserId { get; set; }
        public string UserName { get; set; }
    }
}
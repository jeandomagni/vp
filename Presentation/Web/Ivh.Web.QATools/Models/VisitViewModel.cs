﻿using Ivh.Common.Base.Utilities.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Ivh.Web.QATools.Models
{
    public class VisitViewModel
    {
        public string UserName { get; set; }
        public string BillingApplication { get; set; }
        public string VisitDescription { get; set; }
        public string PatientFirstName { get; set; }
        public string PatientLastName { get; set; }
        public DateTime DischargeDate { get; set; }
        public Decimal HsCurrentBalanceOffsetFromVpBalance { get; set; }
        public int VisitId { get; set; }
        public int VpGuarantorId { get; set; }
        public string PatientLastNameFirstName => FormatHelper.LastNameFirstName(this.PatientLastName, this.PatientFirstName);
        public string PatientFirstNameLastName => FormatHelper.FirstNameLastName(this.PatientFirstName, this.PatientLastName);
    }
}
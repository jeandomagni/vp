﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace Ivh.Web.QATools.Models
{
    public class KeepCurrentViewModel
    {
        public int VpGuarantorId { get; set; }

        // for the menu???
        public string UserName { get; set; }

        [Display(Name = "Anchor")]
        [Required]
        public string KeepCurrentAnchor { get; set; }

        [Display(Name = "Offset")]
        [Required]
        public int KeepCurrentOffset { get; set; }

        public IList<SelectListItem> KeepCurrentAnchors
        {
            get
            {
                return new List<SelectListItem>
                {
                    new SelectListItem {Text = "Registration Date", Value = "RegistrationDate"},
                    new SelectListItem {Text = "Last Statement Date", Value = "LastStatementDate"}
                };
            }
        }
    }
}
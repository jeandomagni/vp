﻿
namespace Ivh.Web.QATools.Models
{

    public class DeploymentHistoryViewModel
    {
        public int DeploymentHistoryId { get; set; }

        public string UserName { get; set; }

        public string ClientName { get; set; }

        public string DeploymentDate { get; set; }

    }
}
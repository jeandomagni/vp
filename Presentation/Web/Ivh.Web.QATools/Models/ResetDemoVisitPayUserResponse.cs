﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Ivh.Web.QATools.Models
{
    public class ResetDemoVisitPayUserResponse
    {
        public bool HasError { get; set; }
        public string Message { get; set; }
    }
}
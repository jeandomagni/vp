﻿namespace Ivh.Web.QATools.Models
{
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.ComponentModel.DataAnnotations;
    using System.Web.Mvc;

    public class DemoVisitPayUserResetViewModel
    {
        /// <summary>
        /// The existing clone's user name that needs to be reset
        /// </summary>
        [DisplayName("Select a user to reset")]
        public string DemoVisitPayUserName { get; set; }

        [Required]
        public string Password { get; set; }

        public List<string> UserNames { get; set; }


    }
}
﻿namespace Ivh.Web.QATools.Interfaces
{
    using Common.Base.Interfaces;
    using Models;

    public interface IScoringThirdPartyRepository : IRepository<ScoringThirdPartyResponse>
    {
    }
}
﻿namespace Ivh.Web.QATools.Interfaces
{
    using System.Collections.Generic;
    using Models;

    public interface IDeploymentService
    {
        bool ReDeployQaToolsData(string clientName);
        bool IsValidEnvironment();
        bool DeploymentInProgress(string clientName);
        IList<DeploymentHistoryViewModel> GetDeploymentHistory();
        void LogDeploymentHistory(string userName, string clientName);
    }
}

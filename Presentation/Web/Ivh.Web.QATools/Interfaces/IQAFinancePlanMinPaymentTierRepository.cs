﻿
namespace Ivh.Web.QATools.Interfaces
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using Ivh.Web.QATools.Models;
    using Ivh.Common.Base.Interfaces;
    using Ivh.Web.QATools.Repositories;

    public interface IQAFinancePlanMinPaymentTierRepository : IRepository<FinancePlanMinPaymentTierViewModel>
    {
        Task CreateFinancePlanMinPaymentTierAsync(FinancePlanMinPaymentTierViewModel financePlanMinPaymentTierViewModel);
        Task DeleteFinancePlanMinPaymentTierAsync(int id);
        Task<IEnumerable<FinancePlanMinPaymentTierViewModel>> GetFinancePlanMinPaymentTierAsync();
    }
}

﻿
namespace Ivh.Web.QATools.Interfaces
{
    using System.Collections.Generic;
    using Common.Base.Interfaces;
    using Entities;
    using Models;

    public interface IDeploymentHistoryRepository : IRepository<DeploymentHistory>
    {
        IList<DeploymentHistory> GetDeploymentHistory();
    }
}

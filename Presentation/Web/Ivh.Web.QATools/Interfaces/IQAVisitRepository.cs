﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ivh.Web.QATools.Models;
using Ivh.Common.Base.Interfaces;

namespace Ivh.Web.QATools.Interfaces
{
    public interface IQAVisitRepository : IRepository<VisitViewModel>
    {
        void CreateVisit (VisitViewModel visit);
        void CreateVisitTransaction (VisitTransactionViewModel transaction);
    }
}

﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Ivh.Web.QATools.Startup))]
namespace Ivh.Web.QATools
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            this.ConfigureIoc(app);
            this.ConfigureAuth(app);
            this.ConfigureMisc(app);
        }
    }
}

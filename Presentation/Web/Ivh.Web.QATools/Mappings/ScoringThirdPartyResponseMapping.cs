﻿namespace Ivh.Web.QATools.Mappings
{
    using FluentNHibernate.Mapping;
    using Models;

    public class ScoringThirdPartyResponseMapping : ClassMap<ScoringThirdPartyResponse>
    {
        public ScoringThirdPartyResponseMapping()
        {
            this.Table("ScoringThirdPartyResponse");
            this.Schema("qa");
            this.Id(x => x.ScoringThirdPartyResponseId);
            this.Map(x => x.FirstName).Not.Nullable();
            this.Map(x => x.LastName).Not.Nullable();
            this.Map(x => x.AddressLine1).Not.Nullable();
            this.Map(x => x.StateProvince).Not.Nullable();
            this.Map(x => x.PostalCode).Not.Nullable();
            this.Map(x => x.ResponseType).Not.Nullable();
            this.Map(x => x.Response).Not.Nullable();
            this.Map(x => x.ReturnErrorCode).Nullable();
            this.Map(x => x.MessageId).Nullable();
        }
    }
}
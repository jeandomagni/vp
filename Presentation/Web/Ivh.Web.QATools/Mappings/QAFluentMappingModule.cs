﻿namespace Ivh.Web.QATools.Mappings
{
    using System.Reflection;
    using Common.DependencyInjection.Registration;
    using FluentNHibernate.Cfg;

    public class QaFluentMappingModule : VisitPayFluentMappingModule
    {
        public override void MapAssemblies(MappingConfiguration m)
        {
            base.MapAssemblies(m);
            m.FluentMappings.AddFromAssembly(Assembly.GetAssembly(typeof(Ivh.Domain.Qat.Modules.QatFluentMappingModule)));
        }
    }
}
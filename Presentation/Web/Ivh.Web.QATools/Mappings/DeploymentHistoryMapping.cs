﻿namespace Ivh.Web.QATools.Mappings
{
    using Entities;
    using FluentNHibernate.Mapping;

    public class DeploymentHistoryMapping : ClassMap<DeploymentHistory>
    {
        public DeploymentHistoryMapping()
        {
            this.Table("DeploymentHistory");
            this.Schema("qa");
            this.Id(x => x.DeploymentHistoryId);
            this.Map(x => x.UserName).Not.Nullable();
            this.Map(x => x.ClientName).Not.Nullable();
            this.Map(x => x.DeploymentDate).Not.Nullable();
        }
    }
}
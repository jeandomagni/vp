﻿namespace Ivh.Web.QATools.Mappings
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web.Mvc;
    using Application.Monitoring.Common.Dtos;
    using Application.Qat.Common.Dtos;
    using Application.Qat.Common.Enums;
    using AutoMapper;
    using Common.Base.Utilities.Extensions;
    using Common.Base.Utilities.Helpers;
    using Common.VisitPay.Enums;
    using Common.VisitPay.Messages.HospitalData.Dtos;
    using Common.VisitPay.Strings;
    using Common.Web.Mapping;
    using Common.Web.Models.Monitoring;
    using Domain.FinanceManagement.FinancePlan.Entities;
    using Entities;
    using Models;
    using Models.Etl;
    using Models.MockCommunication;
    using Models.MockPaymentProcessor;

    public class QaToolsMappingProfile : Profile
    {
        public QaToolsMappingProfile()
        {
            this.CreateMap<HsGuarantorDto, Models.Etl.GuarantorViewModel>();
            this.CreateMap<HsGuarantorDto, Models.GuarantorViewModel>();
            this.CreateMap<AddHsGuarantorViewModel, HsGuarantorDto>();

            this.CreateMap<HsVisitDto, Models.Etl.VisitViewModel>()
                .ForMember(dest => dest.LifeCycleStage, opts => opts.MapFrom(src => src.LifeCycleStage.LifeCycleStageName))
                .ForMember(dest => dest.PrimaryInsuranceType, opts => opts.MapFrom(src => $"{src.PrimaryInsuranceType.PrimaryInsuranceTypeName} ({src.PrimaryInsuranceType.SelfPayClass})"))
                .ForMember(d => d.FacilityStateCode, s => s.MapFrom(src => src.Facility.StateCode))
                .ForMember(d => d.FacilityId, s => s.MapFrom(src => src.Facility.FacilityId))
                .ForMember(d => d.FacilityDescription, s => s.MapFrom(src => src.Facility.FacilityDescription))
                .ForMember(dest => dest.AgingTier, opts => opts.MapFrom(src => src.AgingTierId));

            this.CreateMap<Models.Etl.VisitViewModel, HsVisitDto>()
                .ForMember(d => d.PatientType, s => s.Ignore())
                .ForMember(d => d.PrimaryInsuranceType, s => s.Ignore())
                .ForMember(d => d.RevenueWorkCompany, s => s.Ignore())
                .ForMember(d => d.LifeCycleStage, s => s.Ignore())
                .ForMember(d => d.Facility, opts => opts.Ignore())
                .ForMember(dest => dest.AgingTierId, opts => opts.MapFrom(src => src.AgingTier));

            this.CreateMap<HsGuarantorDto, CreateChangeSetViewModel>()
                .ForMember(dest => dest.GuarantorFirstName, opts => opts.MapFrom(src => src.FirstName))
                .ForMember(dest => dest.GuarantorLastName, opts => opts.MapFrom(src => src.LastName))
                .ForMember(dest => dest.GuarantorSourceSystemKey, opts => opts.MapFrom(src => src.SourceSystemKey));

            this.CreateMap<HsVisitDto, CreateChangeSetViewModel>()
                .ForMember(dest => dest.LifeCycleStage, opts => opts.Ignore())
                .ForMember(dest => dest.FacilityId, opts => opts.MapFrom(src => src.Facility.FacilityId))
                .ForMember(dest => dest.FacilityDescription, opts => opts.MapFrom(src => src.Facility.FacilityDescription))
                .ForMember(dest => dest.VisitBillingSystem, opts => opts.MapFrom(src => (src.BillingSystem != null) ? src.BillingSystem.BillingSystemName : ""))
                .ForMember(dest => dest.VisitSourceSystemKey, opts => opts.MapFrom(src => src.SourceSystemKey))
                .ForMember(dest => dest.VisitSourceSystemKeyDisplay, opts => opts.MapFrom(src => src.SourceSystemKeyDisplay))
                .ForMember(dest => dest.ChangeEventFormViewModel, opts => opts.MapFrom(src => src));

            this.CreateMap<HsVisitDto, ChangeEventViewModel>();

            this.CreateMap<Models.Etl.VisitTransactionViewModel, HsVisitTransactionDto>()
                .ForMember(dest => dest.VpPaymentAllocationId, opts => opts.MapFrom(src => src.PaymentAllocationId));

            this.CreateMap<HsVisitTransactionDto, Models.Etl.VisitTransactionViewModel>()
                .ForMember(dest => dest.TransactionCode, opts => opts.MapFrom(src => src.TransactionCodeId))
                .ForMember(dest => dest.VpTransactionType, opts => opts.MapFrom(src => src.VpTransactionType.VpTransactionTypeName))
                .ForMember(dest => dest.PaymentAllocationId, opts => opts.MapFrom(src => src.VpPaymentAllocationId));

            this.CreateMap<VisitPaymentAllocationDto, VisitPaymentAllocationViewModel>();

            this.CreateMap<ChangeEventDto, ChangeEventViewModel>()
                  .ForMember(dest => dest.ChangeEventStatus, opts => opts.MapFrom(src => src.ChangeEventStatus.ToString()))
                  .ForMember(dest => dest.ChangeEventType, opts => opts.MapFrom(src => src.ChangeEventType.ChangeEventTypeId.ToString()));

            this.CreateMap<List<ChangeEventDto>, List<ChangeEventTypeEnum>>()
                .ConvertUsing(s1 => s1.Select(s => (ChangeEventTypeEnum)s.ChangeEventType.ChangeEventTypeId).ToList());

            this.CreateMap<VpOutboundVisitTransactionDto, OutboundVisitTransactionViewModel>();
            this.CreateMap<OutboundVisitTransactionViewModel, VpOutboundVisitTransactionDto>();

            this.CreateMap<VpOutboundVisitDto, OutboundVisitViewModel>();
            this.CreateMap<OutboundVisitViewModel, VpOutboundVisitDto>();

            this.CreateMap<HsGuarantorViewModel, HsGuarantorDto>();

            this.CreateMap<HsGuarantorViewModel, HsGuarantorChangeDto>()
                .ForMember(d => d.BillingSystemId, x => x.MapFrom(s => s.HsBillingSystemId));

            this.CreateMap<HsGuarantorDto, HsGuarantorViewModel>()
                .ForMember(dest => dest.SSN4, opts => opts.MapFrom(src => !string.IsNullOrEmpty(src.SSN4) ? Convert.ToInt32(src.SSN4) : (int?)null))
                .ForMember(dest => dest.HsGuarantorTypeCode, opts => opts.MapFrom(src => 0));

            this.CreateMap<HsGuarantorFilterViewModel, HsGuarantorFilterDto>();

            this.CreateMap<HsVisitDto, VisitChangeDto>()
                .ForMember(d => d.Facility, o => o.MapFrom(s => s.Facility));

            this.CreateMap<HsVisitTransactionDto, VisitTransactionChangeDto>();

            this.CreateMap<Models.Etl.VisitViewModel, VisitChangeDto>()
                .ForMember(d => d.PatientType, s => s.Ignore())
                .ForMember(d => d.PrimaryInsuranceType, s => s.Ignore())
                .ForMember(d => d.RevenueWorkCompany, s => s.Ignore())
                .ForMember(d => d.LifeCycleStage, s => s.Ignore());

            this.CreateMap<Models.Etl.VisitTransactionViewModel, VisitTransactionChangeDto>();
            this.CreateMap<VisitInsurancePlanViewModel, VisitInsurancePlanChangeDto>();
            this.CreateMap<VisitInsurancePlanDto, VisitInsurancePlanViewModel>()
                .ForMember(dest => dest.InsurancePlanId, x => x.MapFrom(src => src.InsurancePlan.InsurancePlanId))
                .ForMember(dest => dest.InsurancePlanName, x => x.MapFrom(src => src.InsurancePlan.InsurancePlanName));

            this.CreateMap<VisitInsurancePlanDto, VisitInsurancePlanChangeDto>()
                .ForMember(dest => dest.InsurancePlanId, opts => opts.MapFrom(src => src.InsurancePlan == null ? 0 : src.InsurancePlan.InsurancePlanId));

            this.CreateMap<SystemHealthDetailsDto, SystemHealthViewModel>();
            this.CreateMap<SystemInfoDetailsDto, SystemInfoViewModel>();

            this.CreateMap<PpiRequestDto, MockAchViewModel>()
                .ForMember(dest => dest.Amount, opts => opts.MapFrom(src => MapMockPaymentAmount(src)))
                .ForMember(dest => dest.AchFileTypes, opts => opts.MapFrom(src => MapMockAchFileTypes()))
                .ForMember(dest => dest.AchProcessDate, opts => opts.MapFrom(src => src.AchProcessDate.HasValue ? src.AchProcessDate.Value.ToString("yyyy-MM-dd") : null))
                .ForMember(dest => dest.AchReturnCodes, opts => opts.MapFrom(src => MapMockAchReturnCodes()))
                .ForMember(dest => dest.AchReturnAmount, opts => opts.MapFrom(src => src.AchReturnAmount.HasValue ? src.AchReturnAmount.Value.ToString("0.00") : null));

            this.CreateMap<PpiRequestDto, MockPaymentViewModel>()
                .ForMember(dest => dest.Amount, opts => opts.MapFrom(src => MapMockPaymentAmount(src)))
                .ForMember(dest => dest.CompletedResponseType, opts => opts.MapFrom(src => src.ResponseType))
                .ForMember(dest => dest.CompletedResponseAvs, opts => opts.MapFrom(src => src.ResponseAvsCode == null ? string.Empty : src.ResponseAvsCode.Value.ToString()))
                .ForMember(dest => dest.ResponseTypes, opts => opts.MapFrom(src => MapMockPaymentResponseTypes(src)))
                .ForMember(dest => dest.AvsCodes, opts => opts.MapFrom(src => MapMockAvsCodes(src)));

            this.CreateMap<DeploymentHistory, DeploymentHistoryViewModel>()
                .ForMember(dest => dest.DeploymentHistoryId, opts => opts.MapFrom(src => src.DeploymentHistoryId))
                .ForMember(dest => dest.UserName, opts => opts.MapFrom(src => src.UserName))
                .ForMember(dest => dest.ClientName, opts => opts.MapFrom(src => src.ClientName))
                .ForMember(dest => dest.DeploymentDate, opts => opts.MapFrom(src => MappingHelper.MapToVisitPayDateTimeText(src.DeploymentDate)));

            this.CreateMap<UnclearedPaymentAllocationDto, UnclearedPaymentAllocationViewModel>()
                .ForMember(dest => dest.InsertDate, opts => opts.MapFrom(src => src.InsertDate.ToString(Format.DateTimeFormatSql)))
                .ForMember(dest => dest.TransactionAmount, opts => opts.MapFrom(src => src.TransactionAmount.ToString("0.00")));

            this.CreateMap<UnclearedPaymentAllocationDto, Models.Etl.VisitTransactionViewModel>()
                .ForMember(dest => dest.PostDate, opts => opts.MapFrom(src => src.InsertDate))
                .ForMember(dest => dest.TransactionAmount, opts => opts.MapFrom(src => src.TransactionAmount));

            this.CreateMap<CommunicationDto, PayByTextCommunication>()
                .ForMember(dest => dest.Body, opts => opts.MapFrom(src => src.Body.Replace("\\n", "<br/>")));

            this.CreateMap<InterestRateViewModel, InterestRate>();
            this.CreateMap<InterestRate, InterestRateViewModel>();

            this.CreateMap<FinancePlanMinPaymentTierViewModel, FinancePlanMinimumPaymentTier>();
            this.CreateMap<FinancePlanMinimumPaymentTier, FinancePlanMinPaymentTierViewModel>();
        }


        private static string MapMockPaymentAmount(PpiRequestDto requestDto)
        {
            List<char> array = requestDto.Amount.ToCharArray().ToList();
            if (array.Count >= 2)
            {
                array.Insert(array.Count - 2, '.');
            }

            return array.Count == 0 ? "-" : Convert.ToDecimal(string.Join("", array)).FormatCurrency();
        }

        private static IList<SelectListItem> MapMockPaymentResponseTypes(PpiRequestDto requestDto)
        {
            List<SelectListItem> items = new List<SelectListItem>();

            IDictionary<string, SelectListGroup> dict = new Dictionary<string, SelectListGroup>
            {
                {"success", new SelectListGroup {Name = "Success"}},
                {"decline", new SelectListGroup {Name = "Decline"}},
                {"error", new SelectListGroup {Name = "Error"}}
            };

            foreach (KeyValuePair<string, SelectListGroup> group in dict)
            {
                items.AddRange(requestDto.ResponseTypes.Where(x => EnumHelper<PpiResponseTypeEnum>.Contains(x, group.Key)).Select(x => new SelectListItem
                {
                    Text = x.ToString(),
                    Value = ((int)x).ToString(),
                    Group = group.Value
                }));
            }

            return items;
        }

        private static IList<SelectListItem> MapMockAvsCodes(PpiRequestDto requestDto)
        {
            List<SelectListItem> items = new List<SelectListItem>();

            if (requestDto.AcceptedAvsCodes == null || !requestDto.AcceptedAvsCodes.Any())
            {
                return items;
            }

            SelectListGroup validGroup = new SelectListGroup { Name = "Valid" };
            SelectListGroup invalidGroup = new SelectListGroup { Name = "Invalid" };

            string[] avsCodes = { "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z" };

            return avsCodes.Select(x => new SelectListItem
            {
                Text = x,
                Value = x,
                Group = requestDto.AcceptedAvsCodes.Any(z => string.Equals(z, x, StringComparison.CurrentCultureIgnoreCase)) ? validGroup : invalidGroup
            }).ToList();
        }

        private static IList<SelectListItem> MapMockAchFileTypes()
        {
            IList<SelectListItem> items = new List<SelectListItem>();

            foreach (AchFileTypeEnum fileType in Enum.GetValues(typeof(AchFileTypeEnum)))
            {
                items.Add(new SelectListItem { Text = fileType.ToString(), Value = ((int)fileType).ToString() });
            }

            return items;
        }

        private static IList<SelectListItem> MapMockAchReturnCodes()
        {
            IList<SelectListItem> items = new List<SelectListItem>();

            foreach (AchReturnCodeEnum returnCode in Enum.GetValues(typeof(AchReturnCodeEnum)))
            {
                items.Add(new SelectListItem { Text = returnCode.ToString(), Value = ((int)returnCode).ToString() });
            }

            return items;
        }
    }
}
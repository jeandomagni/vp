﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Ivh.Web.QATools.Services
{
    using Application.Core.Common.Dtos;
    using Application.Core.Common.Interfaces;
    using Application.Email.Common.Interfaces;
    using Application.Email.Internal.Dtos;
    using Common.Base.Utilities.Extensions;
    using Common.VisitPay.Attributes;
    using Common.VisitPay.Enums;
    using Common.VisitPay.Strings;
    using Common.Web.Constants;
    using Domain.Email.Entities;
    using Domain.Email.Interfaces;
    using Domain.Settings.Entities;
    using Domain.Settings.Interfaces;
    using Ivh.Application.FinanceManagement.Common.Dtos;
    using Ivh.Application.FinanceManagement.Common.Interfaces;
    using Ivh.Application.User.Common.Dtos;
    using Ivh.Domain.FinanceManagement.FinancePlan.Entities;
    using Ivh.Domain.Logging.Interfaces;

    public class SendSamplePaperMailService : ISendSamplePaperMailService
    {
        private readonly Lazy<ICommunicationApplicationServiceQa> _communicationApplicationServiceQa;
        private readonly Lazy<IGuarantorApplicationService> _guarantorApplicationService;
        private readonly Lazy<ICommunicationService> _communicationService;
        private readonly Lazy<ICommunicationExportApplicationService> _communicationExportApplicationService;
        private readonly Lazy<IClientService> _clientService;
        private readonly Lazy<IFinancePlanApplicationService> _financePlanApplicationService;
        private readonly Lazy<ILoggingService> _loggingService;

        public SendSamplePaperMailService(
            Lazy<ICommunicationApplicationServiceQa> communicationApplicationServiceQa,
            Lazy<IGuarantorApplicationService> guarantorApplicationService,
            Lazy<ICommunicationService> communicationService,
            Lazy<ICommunicationExportApplicationService> communicationExportApplicationService,
            Lazy<IClientService> clientService,
            Lazy<IFinancePlanApplicationService> financePlanApplicationService,
            Lazy<ILoggingService> loggingService
            )
        {
            this._communicationApplicationServiceQa = communicationApplicationServiceQa;
            this._guarantorApplicationService = guarantorApplicationService;
            this._communicationService = communicationService;
            this._communicationExportApplicationService = communicationExportApplicationService;
            this._clientService = clientService;
            this._financePlanApplicationService = financePlanApplicationService;
            this._loggingService = loggingService;
        }

        public IList<string> Send(int vpGuarantorId)
        {
            GuarantorDto guarantor = this._guarantorApplicationService.Value.GetGuarantor(vpGuarantorId);

            if (guarantor == null)
            {
                throw new Exception($"GuarantorId {vpGuarantorId} was not found in the database");
            }

            if (!guarantor.IsOfflineGuarantor)
            {
                throw new Exception($"GuarantorId {vpGuarantorId} is not an offline guarantor");
            }

            IList<string> resultMessages = new List<string>();

            SendTo sendTo = SendTo.Guarantor(vpGuarantorId);
            Client client = this._clientService.Value.GetClient();
            DateTime statementDate = DateTime.Now;
            DateTime statementDueDate = statementDate.AddDays(21);
            DateTime actionDate = DateTime.Now.AddDays(30);
            DateTime expiryDate = DateTime.Now.AddDays(client.ConsolidationExpirationDays);
            decimal statementBalance = 256.00m;
            string tempPassword = "password1234";
            string tempPasswordExpiration = "24";
            string creditAgreementUrl = string.Format(client.AppCreditAgreementUrl, 1);
            const string clientCentralBusinessOffice = "Central Business Office";

            // set up stand-in value for Finance Plan public ID
            FinancePlanPublicIdPhiValueGenerator valueGenerator = new FinancePlanPublicIdPhiValueGenerator();
            FinancePlanPublicIdPhiSource idSource = new FinancePlanPublicIdPhiSource(valueGenerator);
            string financePlanPublicIdPhi = idSource.RawValue.ToString(FinancePlanPublicIdPhiValueGenerator.DisplayFormat);

            FinancePlanDto financePlanDto = null;
            IList<FinancePlanDto> financePlans = this._financePlanApplicationService.Value.GetFinancePlans(vpGuarantorId);

            if (financePlans.Any())
            {
                FinancePlanDto financePlanWithPublicId =
                    financePlans.Where(fp => fp.FinancePlanPublicIdPhi.IsNotNullOrEmpty()).FirstOrDefault();

                if (financePlanWithPublicId != null)
                {
                    financePlanDto = financePlanWithPublicId;

                    financePlanPublicIdPhi =
                        long.Parse(financePlanWithPublicId.FinancePlanPublicIdPhi)
                            .ToString(FinancePlanPublicIdPhiValueGenerator.DisplayFormat);
                }
            }

            IEnumerable<CommunicationTypeEnum> values = Enum.GetValues(typeof(CommunicationTypeEnum)).Cast<CommunicationTypeEnum>();

            foreach (CommunicationTypeEnum communicationTypeEnum in values)
            {
                CommunicationAttribute attribute = communicationTypeEnum.GetAttribute<CommunicationAttribute>();

                if (attribute == null || !attribute.SendMail)
                {
                    continue;
                }

                CommunicationType communicationType = this._communicationService.Value.GetCommunicationType(communicationTypeEnum, CommunicationMethodEnum.Mail);
                bool isCommunicationGroupExempt = communicationType != null && (communicationType.CommunicationGroupId ?? 0) == 0; // null or zero means always send
                bool hasCommunicationPreference = this._communicationService.Value.HasCommunicationPreference(guarantor.User.VisitPayUserId, CommunicationMethodEnum.Mail, communicationTypeEnum);

                if (!hasCommunicationPreference && !isCommunicationGroupExempt)
                {
                    string errorMessage = $"GuarantorId {vpGuarantorId} does not have a paper mail communication preference set for communication type {communicationTypeEnum}";
                    resultMessages.Add($"Error: {errorMessage}");
                    this._loggingService.Value.Fatal(() => errorMessage);
                    continue;
                }

                IDictionary<string, string> replacementFactory(VisitPayUserDto visitPayUser)
                {
                    return new Dictionary<string, string>
                        {
                            {"[[PersonName]]", visitPayUser.FirstName},
                            {"[[StatementDate]]", statementDate.ToString(Format.FullMonthYearFormat)},
                            {"[[PaymentDueDate]]", statementDueDate.ToString(Format.MonthDayYearFormat)},
                            {"[[StatementBalance]]", statementBalance.FormatCurrency()},
                            {"[[StatementMonth]]", statementDate.ToString(Format.MonthFormat)},
                            {"[[GuarantorVpccTokenLogin]]", UrlPath.GuarantorVpccTokenLogin},
                            {"[[Token]]", tempPassword},
                            {"[[GuarantorVpccAccessTokenPhiLogin]]", UrlPath.GuarantorVpccAccessTokenPhiLogin},
                            {"[[AccessTokenPhi]]", visitPayUser.AccessTokenPhi},
                            {"[[TempPasswordExpLimit]]", tempPasswordExpiration},
                            {"[[Name]]", visitPayUser.FirstName},
                            {"[[AppCreditAgreementUrl]]", creditAgreementUrl},
                            {"[[FinancePlanExpirationDate]]", actionDate.Date.ToString(Format.DateFormat)},
                            {"[[NameA]]", visitPayUser.FirstName},
                            {"[[FirstName]]", visitPayUser.FirstName},
                            {"[[ExpiryDate]]", expiryDate.ToString(Format.DateFormat)},
                            {"[[ClientCentralBusinessOffice]]", clientCentralBusinessOffice},
                            {"[[FinancePlanID#]]", financePlanPublicIdPhi},
                            {"[[RecipientMailingAddressName1]]", $"{visitPayUser.FirstName} {visitPayUser.LastName}"},
                            {"[[RecipientMailingAddressAddress1]]", visitPayUser.MailingAddressStreet1},
                            {"[[RecipientMailingAddressAddress2]]", visitPayUser.MailingAddressStreet2},
                            {"[[RecipientMailingAddressCity]]", visitPayUser.MailingCity},
                            {"[[RecipientMailingAddressState]]", visitPayUser.MailingState},
                            {"[[RecipientMailingAddressZip]]", visitPayUser.MailingZip}
                        };
                }

                this._communicationApplicationServiceQa.Value
                    .SendCommunication(
                        sendTo,
                        communicationTypeEnum,
                        communicationTypeEnum,
                        replacementFactory,
                        financePlanDto
                    );

                resultMessages.Add($"Success: Processed \"{communicationTypeEnum}\" communication type.");
            }

            this._communicationExportApplicationService.Value.ExportUnsentPaperCommunications();
            return resultMessages;
        }
    }
}
﻿namespace Ivh.Web.QATools.Services
{
    using System;
    using System.Collections.Generic;
    using Application.Core.Common.Dtos;
    using Application.Core.Common.Interfaces;
    using Application.Settings.Common.Dtos;
    using Common.Base.Enums;
    using Common.Base.Utilities.Extensions;
    using Common.ServiceBus.Interfaces;
    using Common.VisitPay.Enums;
    using Common.VisitPay.Messages.Communication;
    using Common.VisitPay.Messages.Consolidation;
    using Common.VisitPay.Messages.Core;
    using Common.VisitPay.Messages.FinanceManagement;
    using Common.VisitPay.Strings;

    public class SendSampleEmailsService : ISendSampleEmailsService
    {
        private readonly IBus _bus;
        private readonly Lazy<IClientApplicationService> _clientApplicationService;

        public SendSampleEmailsService(
            IBus bus, 
            Lazy<IClientApplicationService> clientApplicationService)
        {
            this._bus = bus;
            this._clientApplicationService = clientApplicationService;
        }

        private const string FirstName = "YourFirstName";
        private const string Email = "email@email.com";
        private const string TempPassword = "TempPa$$w0rd";
        
        // defaultclient
        private const int DefaultClientUserId = 2; 
        private const string DefaultClientUserEmail = "defaultclient@visitpaytest.com";
        private const string DefaultClientUserFirstName = "Default";

        // steve.training (sending most emails to this guy)
        private const int DefaultGuarantorUserId = 82608;
        private const int DefaultGuarantorId = 1001639;
        private const int DefaultGuarantorPaymentProcessorResponseId = 815369;
        
        // jane.training (jane manages steve, should get cc'ed on some of steve's emails)
        private const int DefaultManagingGuarantorId = 1001640;
        
        public void Send()
        {
            this.SendClientSupportEmails();
            this.SendConsolidationEmails();
            this.SendFinancePlanEmails();
            this.SendPaymentEmails();
            this.SendStatementEmails();
            this.SendSupportRequestEmails();
            this.SendVisitEmails();
            this.SendUserAccountEmails();
            this.SendGuestPayEmails();
        }

        // SendClientSupportRequestEmailsApplicationService
        private void SendClientSupportEmails()
        {
            // ClientSupportRequestMessageNotification
            this._bus.PublishMessage(new SendClientSupportRequestMessageAdminNotificationEmailMessage { ClientSupportRequestDisplayId = "ABC-123", InsertDate = DateTime.UtcNow });
            
            // ClientSupportRequestMessageNotification
            this._bus.PublishMessage(new SendClientSupportRequestMessageNotificationEmailMessage { VisitPayUserId = DefaultClientUserId, ClientSupportRequestDisplayId = "ABC-123", InsertDate = DateTime.UtcNow });
            
            // ClientSupportRequestConfirmation
            this._bus.PublishMessage(new SendClientSupportRequestConfirmationEmailMessage  { VisitPayUserId = DefaultClientUserId, ClientSupportRequestDisplayId = "ABC-123" });
            
            // ClientSupportRequestAdminNotification
            this._bus.PublishMessage(new SendClientSupportRequestAdminNotificationEmailMessage { ClientSupportRequestDisplayId = "ABC-123" });
        }
        
        // SendConsolidationEmailsApplicationService
        private void SendConsolidationEmails()
        {
            // ConsolidateHouseholdRequestToManaging
            this._bus.PublishMessage(new ConsolidationHouseholdRequestToManagingMessage { ManagingVpGuarantorId = DefaultManagingGuarantorId, ManagedVpGuarantorId = DefaultGuarantorId, ConsolidationRequestExpiryDay = DateTime.UtcNow.AddDays(this._clientApplicationService.Value.GetClient().ConsolidationExpirationDays).ToString(Format.DateFormat) }).Wait();

            // ConsolidateHouseholdRequestToManaged
            this._bus.PublishMessage(new ConsolidationHouseholdRequestToManagedMessage { ManagingVpGuarantorId = DefaultManagingGuarantorId, ManagedVpGuarantorId = DefaultGuarantorId, ConsolidationRequestExpiryDay = DateTime.UtcNow.AddDays(this._clientApplicationService.Value.GetClient().ConsolidationExpirationDays).ToString(Format.DateFormat) }).Wait();

            // ConsolidateHouseholdRequestCompleteToManaged
            this._bus.PublishMessage(new ConsolidationHouseholdAcceptedToManagedMessage { ManagingVpGuarantorId = DefaultManagingGuarantorId, ManagedVpGuarantorId = DefaultGuarantorId }).Wait();

            // ConsolidateHouseholdRequestAcceptedByManagedToManaging
            this._bus.PublishMessage(new ConsolidationHouseholdRequestAcceptedByManagedToManagingMessage { ManagingVpGuarantorId = DefaultManagingGuarantorId, ManagedVpGuarantorId = DefaultGuarantorId }).Wait();

            // ConsolidateHouseholdRequestAcceptedByManagedWithFpToManaging
            this._bus.PublishMessage(new ConsolidationHouseholdRequestAcceptedByManagedWithFpToManagingMessage { ManagingVpGuarantorId = DefaultManagingGuarantorId, ManagedVpGuarantorId = DefaultGuarantorId, ConsolidationRequestExpiryDay = DateTime.UtcNow.AddDays(this._clientApplicationService.Value.GetClient().ConsolidationExpirationDays).ToString(Format.DateFormat) }).Wait();

            // ConsolidateHouseholdRequestDeclinedToManaged
            this._bus.PublishMessage(new ConsolidationHouseholdRequestDeclinedToManagedMessage { ManagingVpGuarantorId = DefaultManagingGuarantorId, ManagedVpGuarantorId = DefaultGuarantorId }).Wait();

            // ConsolidateHouseholdRequestDeclinedToManaging
            this._bus.PublishMessage(new ConsolidationHouseholdRequestDeclinedToManagingMessage { ManagingVpGuarantorId = DefaultManagingGuarantorId, ManagedVpGuarantorId = DefaultGuarantorId }).Wait();
            
            // ConsolidateHouseholdRequestCancelledByManagedToManaging
            this._bus.PublishMessage(new ConsolidationHouseholdRequestCancelledByManagedToManagingMessage { ManagingVpGuarantorId = DefaultManagingGuarantorId, ManagedVpGuarantorId = DefaultGuarantorId }).Wait();
            
            // ConsolidateHouseholdRequestCancelledByManagingToManaged
            this._bus.PublishMessage(new ConsolidationHouseholdRequestCancelledByManagingToManagedMessage { ManagingVpGuarantorId = DefaultManagingGuarantorId, ManagedVpGuarantorId = DefaultGuarantorId }).Wait();

            // ConsolidateHouseholdRequestCancelledByManagedToManagingWithPendingFpTerms
            this._bus.PublishMessage(new ConsolidationHouseholdRequestCancelledByManagedToManagingWithPendingFpTermsMessage { ManagingVpGuarantorId = DefaultManagingGuarantorId, ManagedVpGuarantorId = DefaultGuarantorId }).Wait();

            // ConsolidateLinkageCancelledByManagingToManaging
            this._bus.PublishMessage(new ConsolidationLinkageCancelledByManagingToManagingMessage { ManagingVpGuarantorId = DefaultManagingGuarantorId, ManagedVpGuarantorId = DefaultGuarantorId }).Wait();

            // ConsolidateLinkageCancelledByManagingToManaged
            this._bus.PublishMessage(new ConsolidationLinkageCancelledByManagingToManagedMessage { ManagingVpGuarantorId = DefaultManagingGuarantorId, ManagedVpGuarantorId = DefaultGuarantorId }).Wait();

            // ConsolidateLinkageCancelledByManagingToManagedWithFp
            this._bus.PublishMessage(new ConsolidationLinkageCancelledByManagingToManagedWithFpMessage { ManagingVpGuarantorId = DefaultManagingGuarantorId, ManagedVpGuarantorId = DefaultGuarantorId }).Wait();

            // ConsolidateLinkageCancelledByManagedToManaging
            this._bus.PublishMessage(new ConsolidationLinkageCancelledByManagedToManagingMessage { ManagingVpGuarantorId = DefaultManagingGuarantorId, ManagedVpGuarantorId = DefaultGuarantorId }).Wait();

            // ConsolidateLinkageCancelledByManagedToManaged
            this._bus.PublishMessage(new ConsolidationLinkageCancelledByManagedToManagedMessage { ManagingVpGuarantorId = DefaultManagingGuarantorId, ManagedVpGuarantorId = DefaultGuarantorId }).Wait();

            // ConsolidateLinkageCancelledByManagedToManagedWithFp
            this._bus.PublishMessage(new ConsolidationLinkageCancelledByManagedToManagedWithFpMessage { ManagingVpGuarantorId = DefaultManagingGuarantorId, ManagedVpGuarantorId = DefaultGuarantorId }).Wait();
        }

        // SendFinancePlanEmailsApplicationService
        private void SendFinancePlanEmails()
        {
            // FinancePlanAdditionalBalance
            this._bus.PublishMessage(new SendFinancePlanAdditionalBalanceEmailMessage { VpGuarantorId = DefaultGuarantorId }).Wait();

            // FinancePlanClosed
            this._bus.PublishMessage(new SendFinancePlanClosedEmailMessage { VpGuarantorId = DefaultGuarantorId }).Wait();

            // FinancePlanCreation
            this._bus.PublishMessage(new SendFinancePlanCreationEmailMessage { ActionDate = DateTime.UtcNow, VpGuarantorId = DefaultGuarantorId }).Wait();

            // FinancePlanModification
            this._bus.PublishMessage(new SendFinancePlanModificationEmailMessage { NextPaymentDueDate = DateTime.UtcNow, VpGuarantorId = DefaultGuarantorId }).Wait();
            
            // FinancePlanTermination
            this._bus.PublishMessage(new SendFinancePlanTerminationEmailMessage { VpGuarantorId = DefaultGuarantorId }).Wait();
            
            // FinancePlanVisitNotEligible
            this._bus.PublishMessage(new SendFinancePlanVisitNotEligibleEmailMessage { VpGuarantorId = DefaultGuarantorId }).Wait();

            // FinancePlanVisitSuspended
            this._bus.PublishMessage(new SendFinancePlanVisitSuspendedEmailMessage { VpGuarantorId = DefaultGuarantorId }).Wait();
        }

        // SendPaymentEmailsApplicationService
        private void SendPaymentEmails()
        {
            // CardExpired
            this._bus.PublishMessage(new SendCardExpiredEmailMessage { VpGuarantorId = DefaultGuarantorId, ExpiryDate = DateTime.UtcNow.ToShortDateString() }).Wait();

            // CardExpiring
            this._bus.PublishMessage(new SendCardExpiringEmailMessage { VpGuarantorId = DefaultGuarantorId, ExpiryDate = DateTime.UtcNow.ToShortDateString() }).Wait();

            // BalanceDueReminder
            this._bus.PublishMessage(new SendPaymentDueReminderEmailMessage { VpGuarantorId = DefaultGuarantorId, PaymentDate = DateTime.UtcNow }).Wait();
            
            // PaymentDueDateChange
            this._bus.PublishMessage(new SendPaymentDueDateChangeEmailMessage { VpGuarantorId = DefaultGuarantorId, NextPaymentDueDate = DateTime.UtcNow }).Wait();

            // PaymentFailure
            this._bus.PublishMessage(new SendPaymentFailureEmailMessage { VpGuarantorId = DefaultGuarantorId }).Wait();

            // PaymentMethodChange
            this._bus.PublishMessage(new SendPaymentMethodChangeEmailMessage { VpGuarantorId = DefaultGuarantorId }).Wait();
            
            // PendingPayment
            this._bus.PublishMessage(new SendPendingPaymentEmailMessage { VpGuarantorId = DefaultGuarantorId, PaymentDate = DateTime.UtcNow }).Wait();
            
            // PaymentReversalNotification
            this._bus.PublishMessage(new SendPaymentReversalConfirmationEmailMessage { VpGuarantorId = DefaultGuarantorId, TransactionNumber = "1", PaymentType = PaymentTypeEnum.PartialRefund }).Wait();
            this._bus.PublishMessage(new SendPaymentReversalConfirmationEmailMessage { VpGuarantorId = DefaultGuarantorId, TransactionNumber = "1", PaymentType = PaymentTypeEnum.Refund }).Wait();
            this._bus.PublishMessage(new SendPaymentReversalConfirmationEmailMessage { VpGuarantorId = DefaultGuarantorId, TransactionNumber = "1", PaymentType = PaymentTypeEnum.Void }).Wait();
            
            // PaymentSubmittedConfirmation
            this._bus.PublishMessage(new SendPaymentConfirmationEmailMessage { VpGuarantorId = DefaultGuarantorId, TransactionNumber = new List<string> { "1", "2", "3" }.ToCommaSeparatedString(true), PaymentProcessorResponseIds = DefaultGuarantorPaymentProcessorResponseId.ToListOfOne(), IsAchType = false }).Wait();
            
            // PrimaryPaymentMethodChange
            this._bus.PublishMessage(new SendPrimaryPaymentMethodChangeEmailMessage { VpGuarantorId = DefaultGuarantorId }).Wait();
            
        }

        // SendStatementEmailsApplicationService
        private void SendStatementEmails()
        {
            // StatementNotificationFinalPastDue, if has managing user, will send: StatementNotificationFinalPastDueToManaging
            this._bus.PublishMessage(new StatementNotificationMessage { VpGuarantorId = DefaultGuarantorId, StatementDate = DateTime.UtcNow, StatementDueDate = DateTime.UtcNow.AddDays(21), StatementBalance = 1000.00M, StatementNotificationTierEnum = StatementNotificationTierEnum.FinalPastDue }).Wait();

            // StatementNotificationGoodStanding, if has managing user, will send: StatementNotificationGoodStandingToManaging
            this._bus.PublishMessage(new StatementNotificationMessage { VpGuarantorId = DefaultGuarantorId, StatementDate = DateTime.UtcNow, StatementDueDate = DateTime.UtcNow.AddDays(21), StatementBalance = 1000.00M, StatementNotificationTierEnum = StatementNotificationTierEnum.Good }).Wait();

            // StatementNotificationPastDue, if has managing user, will send: StatementNotificationPastDueToManaging
            this._bus.PublishMessage(new StatementNotificationMessage { VpGuarantorId = DefaultGuarantorId, StatementDate = DateTime.UtcNow, StatementDueDate = DateTime.UtcNow.AddDays(21), StatementBalance = 1000.00M, StatementNotificationTierEnum = StatementNotificationTierEnum.PastDue }).Wait();

            // StatementNotificationUncollectable, if has managing user, will send: StatementNotificationUncollectableToManaging
            this._bus.PublishMessage(new StatementNotificationMessage { VpGuarantorId = DefaultGuarantorId, StatementDate = DateTime.UtcNow, StatementDueDate = DateTime.UtcNow.AddDays(21), StatementBalance = 1000.00M, StatementNotificationTierEnum = StatementNotificationTierEnum.Uncollectable }).Wait();
            
            // FinancePlanStatementNotificationFinalPastDue, if managing user, will send: FinancePlanStatementNotificationFinalPastDueToManaging
            this._bus.PublishMessage(new FinancePlanStatementNotificationMessage { VpGuarantorId = DefaultGuarantorId, StatementDate = DateTime.UtcNow, StatementDueDate = DateTime.UtcNow.AddDays(21), StatementBalance = 1000.00M, StatementNotificationTierEnum = StatementNotificationTierEnum.FinalPastDue }).Wait();

            // FinancePlanStatementNotificationGoodStanding, if managing user, will send: FinancePlanStatementNotificationGoodStandingToManaging
            this._bus.PublishMessage(new FinancePlanStatementNotificationMessage { VpGuarantorId = DefaultGuarantorId, StatementDate = DateTime.UtcNow, StatementDueDate = DateTime.UtcNow.AddDays(21), StatementBalance = 1000.00M, StatementNotificationTierEnum = StatementNotificationTierEnum.Good }).Wait();

            // FinancePlanStatementNotificationPastDue, if managing user, will send: FinancePlanStatementNotificationPastDueToManaging
            this._bus.PublishMessage(new FinancePlanStatementNotificationMessage { VpGuarantorId = DefaultGuarantorId, StatementDate = DateTime.UtcNow, StatementDueDate = DateTime.UtcNow.AddDays(21), StatementBalance = 1000.00M, StatementNotificationTierEnum = StatementNotificationTierEnum.PastDue }).Wait();

            // FinancePlanStatementNotificationUncollectable, if managing user, will send: FinancePlanStatementNotificationUncollectableToManaging
            this._bus.PublishMessage(new FinancePlanStatementNotificationMessage { VpGuarantorId = DefaultGuarantorId, StatementDate = DateTime.UtcNow, StatementDueDate = DateTime.UtcNow.AddDays(21), StatementBalance = 1000.00M, StatementNotificationTierEnum = StatementNotificationTierEnum.Uncollectable }).Wait();
        }

        // SendSupportRequestEmailsApplicationService
        private void SendSupportRequestEmails()
        {
             // SupportRequestConfirmation
            this._bus.PublishMessage(new SendSupportRequestCreatedEmailMessage { VpGuarantorId = DefaultGuarantorId, SupportRequestDisplayId = "1" }).Wait();

            // SupportRequestUpdate
            this._bus.PublishMessage(new SendSupportRequestUpdatedEmailMessage { VpGuarantorId = DefaultGuarantorId, SupportRequestDisplayId = "1", InsertDate = DateTime.UtcNow.ToString(Format.DateFormat), ModificationStatus = "ModificationStatus" }).Wait();

        }

        // SendVisitEmailApplicationService
        private void SendVisitEmails()
        {
            // FinalPastDueNotificationFinancePlan
            this._bus.PublishMessage(new SendFinalPastDueNotificationEmailMessage { VpGuarantorId = DefaultGuarantorId, IsOnFinancePlan = true }).Wait();

            // FinalPastDueNotificationNonFinancePlan
            this._bus.PublishMessage(new SendFinalPastDueNotificationEmailMessage { VpGuarantorId = DefaultGuarantorId, IsOnFinancePlan = false }).Wait();
            
            // NewVisitLoaded
            this._bus.PublishMessage(new NewVisitLoadedMessage { VpGuarantorId = DefaultGuarantorId }).Wait();
            
            // UncollectableBalanceNotfication
            this._bus.PublishMessage(new SendPendingUncollectableEmailMessage { VpGuarantorId = DefaultGuarantorId }).Wait();
            
            // VisitsPastDue
            this._bus.PublishMessage(new SendPastDueVisitsEmailMessage { VpGuarantorId = DefaultGuarantorId }).Wait();
        }

        // SendUserAccountEmailsApplicationService
        private void SendUserAccountEmails()
        {
            #region client
            
            // ClientAccountLockout SendClientAccountLockoutEmailMessage
            this._bus.PublishMessage(new SendClientAccountLockoutEmailMessage { Email = DefaultClientUserEmail, Name = DefaultClientUserFirstName, VisitPayUserId = DefaultClientUserId }).Wait();

            // ClientProfileChange
            this._bus.PublishMessage(new SendClientProfileChangeEmailMessage { Email = DefaultClientUserEmail, FirstName = DefaultClientUserFirstName, VisitPayUserId = DefaultClientUserId }).Wait();
            
            // ForgotPasswordWithoutPinClient
            this._bus.PublishMessage(new SendClientPasswordWithoutPinEmailMessage { Email = DefaultClientUserEmail, Name = DefaultClientUserFirstName, TempPassword = TempPassword, VisitPayUserId = DefaultClientUserId }).Wait();
            
            // NewUserWithoutPinClient
            this._bus.PublishMessage(new SendNewUserClientPasswordWithoutPinEmailMessage { Email = DefaultClientUserEmail, Name = DefaultClientUserFirstName, TempPassword = TempPassword, VisitPayUserId = DefaultClientUserId }).Wait();
            
            // PasswordResetConfirmationClient
            this._bus.PublishMessage(new SendClientResetPasswordEmailMessage { Email = DefaultClientUserEmail, Name = DefaultClientUserFirstName, VisitPayUserId = DefaultClientUserId }).Wait();

            #endregion

            #region patient

            // AccountCancelationComplete
            this._bus.PublishMessage(new SendAccountClosedCompleteMessage { Email = Email, Name = FirstName, VisitPayUserId = DefaultGuarantorUserId }).Wait();

            // AccountCredentialChangeConfirmationPatient
            this._bus.PublishMessage(new SendAccountCredentialChangePatientEmailMessage { Email = Email, Name = FirstName, VisitPayUserId = DefaultGuarantorUserId }).Wait();
            
            // AccountReactivationComplete
            this._bus.PublishMessage(new SendAccountReactivationCompleteMessage { Email = Email, Name = FirstName, VisitPayUserId = DefaultGuarantorUserId, UserName = "username" }).Wait();
            
            // ForgotPasswordWithoutPinPatient
            this._bus.PublishMessage(new SendPasswordWithoutPinEmailMessage { Email = Email, Name = FirstName, TempPassword = TempPassword, VisitPayUserId = DefaultGuarantorUserId }).Wait();

            // ForgotUsername
            this._bus.PublishMessage(new SendUsernameReminderEmailMessage { Email = Email, Name = FirstName, UserName = "UserName", VisitPayUserId = DefaultGuarantorUserId }).Wait();
            
            // GuarantorAccountLockout
            this._bus.PublishMessage(new SendGuarantorAccountLockoutEmailMessage { Email = Email, Name = FirstName, VisitPayUserId = DefaultGuarantorUserId }).Wait();

            // RegistrationSuccess
            this._bus.PublishMessage(new SendPostRegistrationWelcomeMessage { Email = Email, Name = FirstName, VisitPayUserId = DefaultGuarantorUserId }).Wait();
            
            // SmsActivateEmailAlert
            this._bus.PublishMessage(new SendSmsActivateAlertEmailMessage { Email = Email, Name = FirstName, Phone = "222-222-2222", VisitPayUserId = DefaultGuarantorUserId }).Wait();
            
            // SmsDeactivateEmailAlert
            this._bus.PublishMessage(new SendSmsDeactivateAlertEmailMessage { Email = Email, Name = FirstName, Phone = "222-222-2222", VisitPayUserId = DefaultGuarantorUserId }).Wait();

            // UserProfileChange
            this._bus.PublishMessage(new SendUserProfileChangeEmailMessage { Email = Email, Name = FirstName, VisitPayUserId = DefaultGuarantorUserId }).Wait();

            #endregion

            #region other
            
            // NewUserInvitation
            this._bus.PublishMessage(new SendNewUserInvitationEmailMessage { FirstName = FirstName, Email = Email, Token = Guid.NewGuid() }).Wait();
            
            #endregion
        }

        // SendGuestPayEmailsApplicationService
        public void SendGuestPayEmails()
        {
            ClientDto clientDto = this._clientApplicationService.Value.GetClient();
            
            // for each client supported language, send the emails
            foreach (ClientCultureInfoDto cultureInfoDto in this.GetSupportedLocales(clientDto.LanguageGuestPaySupported))
            {
                // GuestPayPaymentConfirmation
                this._bus.PublishMessage(new SendGuestPayPaymentConfirmationEmailMessage { EmailAddress = Email, GuarantorFirstName = FirstName, Locale = cultureInfoDto.Name, PaymentUnitId = 1, TransactionId = "123-1234" }).Wait();
            }

            // these two aren't localized
            // GuestPayPaymentFailure
            this._bus.PublishMessage(new SendGuestPayPaymentFailureEmailMessage {EmailAddress = Email, GuarantorFirstName = FirstName, PaymentUnitId = 1, TransactionId = "123-1234" }).Wait();
            
            // GuestPayPaymentReversalNotification
            this._bus.PublishMessage(new SendGuestPayPaymentReversalConfirmationEmailMessage {EmailAddress = Email, GuarantorFirstName = FirstName, PaymentUnitId = 1, PaymentType = PaymentTypeEnum.Void, TransactionId = "123-1234" }).Wait();
        }

        private IEnumerable<ClientCultureInfoDto> GetSupportedLocales(IList<ClientCultureInfoDto> locales)
        {
            if (locales.IsNullOrEmpty())
            {
                locales = new List<ClientCultureInfoDto>
                {
                    new ClientCultureInfoDto {Name = "en-US"}
                };
            }

            return locales;
        }

        public void SendSms(int vpGuarantorId)
        {
            #region EmailType04 (StatementNotification)
            this._bus.PublishMessage(new StatementNotificationMessage
            {
                VpGuarantorId = DefaultGuarantorId,
                StatementDate = DateTime.UtcNow,
                StatementDueDate = DateTime.UtcNow.AddDays(21),
                StatementBalance = 1000.00M,
                StatementNotificationTierEnum = StatementNotificationTierEnum.Good
            }).Wait();
            this._bus.PublishMessage(new StatementNotificationMessage
            {
                VpGuarantorId = DefaultGuarantorId,
                StatementDate = DateTime.UtcNow,
                StatementDueDate = DateTime.UtcNow.AddDays(21),
                StatementBalance = 1000.00M,
                StatementNotificationTierEnum = StatementNotificationTierEnum.PastDue
            }).Wait();
            this._bus.PublishMessage(new StatementNotificationMessage
            {
                VpGuarantorId = DefaultGuarantorId,
                StatementDate = DateTime.UtcNow,
                StatementDueDate = DateTime.UtcNow.AddDays(21),
                StatementBalance = 1000.00M,
                StatementNotificationTierEnum = StatementNotificationTierEnum.FinalPastDue
            }).Wait();
            this._bus.PublishMessage(new StatementNotificationMessage
            {
                VpGuarantorId = DefaultGuarantorId,
                StatementDate = DateTime.UtcNow,
                StatementDueDate = DateTime.UtcNow.AddDays(21),
                StatementBalance = 1000.00M,
                StatementNotificationTierEnum = StatementNotificationTierEnum.Uncollectable
            }).Wait();
            #endregion

            #region EmailType06 (VisitsPastDue)
            this._bus.PublishMessage(new SendPastDueVisitsEmailMessage
            {
                VpGuarantorId = vpGuarantorId
            }).Wait();
            #endregion

            #region EmailType07 (FinalPastDueNotification)
            this._bus.PublishMessage(new SendFinalPastDueNotificationEmailMessage
            {
                VpGuarantorId = vpGuarantorId
            }).Wait();
            #endregion

            #region EmailType08 (UncollectableBalanceNotfication)
            this._bus.PublishMessage(new SendPendingUncollectableEmailMessage
            {
                VpGuarantorId = vpGuarantorId
            }).Wait();
            #endregion

            #region EmailType18 (PendingPayment)
            this._bus.PublishMessage(new SendPendingPaymentEmailMessage
            {
                VpGuarantorId = vpGuarantorId,
                PaymentDate = DateTime.UtcNow
            }).Wait();
            #endregion

            #region EmailType19 (PaymentFailure)
            this._bus.PublishMessage(new SendPaymentFailureEmailMessage
            {
                VpGuarantorId = vpGuarantorId
            }).Wait();
            #endregion

            #region EmailTyep32 (CardExpired)
            this._bus.PublishMessage(new SendCardExpiredEmailMessage
            {
                VpGuarantorId = vpGuarantorId,
                ExpiryDate = DateTime.UtcNow.ToShortDateString()
            }).Wait();
            #endregion

            #region EmailType33 (CardExpiring)
            this._bus.PublishMessage(new SendCardExpiringEmailMessage
            {
                VpGuarantorId = vpGuarantorId,
                ExpiryDate = DateTime.UtcNow.ToShortDateString()
            }).Wait();
            #endregion

            #region EmailType36 (BalanceDueReminder)
            this._bus.PublishMessage(new SendPaymentDueReminderEmailMessage
            {
                VpGuarantorId = vpGuarantorId,
                PaymentDate = DateTime.UtcNow,
            }).Wait();
            #endregion

            #region EmailType38 (SupportRequestUpdate)
            this._bus.PublishMessage(new SendSupportRequestUpdatedEmailMessage
            {
                VpGuarantorId = vpGuarantorId,
                SupportRequestDisplayId = "1",
                InsertDate = DateTime.UtcNow.ToString(Format.DateFormat),
                ModificationStatus = "ModificationStatus"
            }).Wait();
            #endregion

            #region FinancePlanStatementNotification
            this._bus.PublishMessage(new FinancePlanStatementNotificationMessage
            {
                VpGuarantorId = DefaultGuarantorId,
                StatementDate = DateTime.UtcNow,
                StatementDueDate = DateTime.UtcNow.AddDays(21),
                StatementBalance = 1000.00M,
                StatementNotificationTierEnum = StatementNotificationTierEnum.Good
            }).Wait();
            this._bus.PublishMessage(new FinancePlanStatementNotificationMessage
            {
                VpGuarantorId = DefaultGuarantorId,
                StatementDate = DateTime.UtcNow,
                StatementDueDate = DateTime.UtcNow.AddDays(21),
                StatementBalance = 1000.00M,
                StatementNotificationTierEnum = StatementNotificationTierEnum.PastDue
            }).Wait();
            this._bus.PublishMessage(new FinancePlanStatementNotificationMessage
            {
                VpGuarantorId = DefaultGuarantorId,
                StatementDate = DateTime.UtcNow,
                StatementDueDate = DateTime.UtcNow.AddDays(21),
                StatementBalance = 1000.00M,
                StatementNotificationTierEnum = StatementNotificationTierEnum.FinalPastDue
            }).Wait();
            this._bus.PublishMessage(new FinancePlanStatementNotificationMessage
            {
                VpGuarantorId = DefaultGuarantorId,
                StatementDate = DateTime.UtcNow,
                StatementDueDate = DateTime.UtcNow.AddDays(21),
                StatementBalance = 1000.00M,
                StatementNotificationTierEnum = StatementNotificationTierEnum.Uncollectable
            }).Wait();
            #endregion
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Services;
using System.Web.Services.Description;
using System.Web.Services.Protocols;
using Ivh.Web.QATools.Interfaces;
using Ivh.Application.Qat.Common.Interfaces;

namespace Ivh.Web.QATools.Services
{
    /// <summary>
    /// Summary description for NachaGatewayInceptorService
    /// </summary>
    [System.Web.Services.WebServiceBindingAttribute(Name = "GatewaySoap", Namespace = namespaceBase)]
    public class NachaGatewayInceptorService : SoapHttpClientProtocol, INachaGatewayInceptorService
    {
        private const string namespaceBase = "http://api.ach.com/";
        private readonly IAchInterceptorApplicationService _achInterceptorApplicationService;

        public NachaGatewayInceptorService() : base()
        {
            _achInterceptorApplicationService = DependencyResolver.Current.GetService<IAchInterceptorApplicationService>();
        }

        public NachaGatewayInceptorService(IAchInterceptorApplicationService achInterceptorApplicationService) : base()
        {
            _achInterceptorApplicationService = achInterceptorApplicationService;
        }

        [WebMethod()]
        [SoapDocumentMethodAttribute(namespaceBase + "GetReturnFile", RequestNamespace = namespaceBase, ResponseNamespace = namespaceBase, Use = SoapBindingUse.Literal, ParameterStyle = SoapParameterStyle.Wrapped)]
        public string GetReturnFile(string NachaID, string BeginDate, string EndDate, string FileFormat)
        {
            DateTime startDateTime, endDateTime;

            if (DateTime.TryParseExact(BeginDate, "yyyyMMdd", CultureInfo.InvariantCulture, DateTimeStyles.None, out startDateTime) && DateTime.TryParseExact(EndDate, "yyyyMMdd", CultureInfo.InvariantCulture, DateTimeStyles.None, out endDateTime))
                return _achInterceptorApplicationService.GetReturnFile(startDateTime, endDateTime, FileFormat);

            return string.Empty;
        }

        [WebMethod]
        [SoapDocumentMethodAttribute(namespaceBase + "GetReport", RequestNamespace = namespaceBase, ResponseNamespace = namespaceBase, Use = SoapBindingUse.Literal, ParameterStyle = SoapParameterStyle.Wrapped)]
        public string GetReport(string token, string reporttypeid, string odfiid, string nachaid, string resellerid, string startdate, string enddate, string datetypeid, string originationfileid, string salespersonid, string exportformat)
        {
            DateTime startDateTime, endDateTime;

            if (DateTime.TryParseExact(startdate, "yyyyMMdd", CultureInfo.InvariantCulture, DateTimeStyles.None, out startDateTime) && DateTime.TryParseExact(enddate, "yyyyMMdd", CultureInfo.InvariantCulture, DateTimeStyles.None, out endDateTime))
                return _achInterceptorApplicationService.GetReport(reporttypeid, startDateTime, endDateTime, datetypeid, exportformat);
                
            return string.Empty;
        }
    }
}

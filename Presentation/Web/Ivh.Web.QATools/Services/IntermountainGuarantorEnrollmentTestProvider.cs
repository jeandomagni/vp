﻿namespace Ivh.Web.QATools.Services
{
    using System;
    using System.Threading.Tasks;
    using Common.ServiceBus.Interfaces;
    using Domain.Logging.Interfaces;
    using Domain.Settings.Interfaces;
    using Provider.Enrollment.Intermountain;
    using Provider.Enrollment.Intermountain.Models;

    public class IntermountainGuarantorEnrollmentTestProvider : IntermountainGuarantorEnrollmentProvider
    {
        public IntermountainGuarantorEnrollmentTestProvider(
            Lazy<IApplicationSettingsService> applicationSettingsService, 
            Lazy<ILoggingProvider> logger, 
            Lazy<IBus> bus)
            : base(applicationSettingsService, logger, bus)
        {
        }

        public async Task<AuthenticateResponseModel> TestGetJwtTokenAsync(string test)
        {
            return await this.GetJwtTokenAsync(test, test, test);
        }
    }
}
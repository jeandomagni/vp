﻿namespace Ivh.Web.QATools.Services
{
    using Application.Email.Internal.Dtos;
    using Application.User.Common.Dtos;
    using Common.VisitPay.Enums;
    using Ivh.Application.FinanceManagement.Common.Dtos;
    using System;
    using System.Collections.Generic;

    public interface ICommunicationApplicationServiceQa
    {
        bool SendCommunication(
            SendTo sendTo,
            CommunicationTypeEnum communicationTypeEnum,
            CommunicationTypeEnum managingUserCommunicationTypeEnum,
            Func<VisitPayUserDto, IDictionary<string, string>> replacementFactory,
            FinancePlanDto financePlanDto,
            string overrideEmail = null);
    }
}

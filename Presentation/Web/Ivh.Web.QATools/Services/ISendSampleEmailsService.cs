﻿namespace Ivh.Web.QATools.Services
{
    public interface ISendSampleEmailsService
    {
        void Send();
        void SendSms(int vpGuarantorId);
    }
}
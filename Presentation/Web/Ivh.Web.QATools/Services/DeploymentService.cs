﻿namespace Ivh.Web.QATools.Services
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Net;
    using AutoMapper;
    using Common.Base.Utilities.Extensions;
    using Common.Configuration.Interfaces;
    using Common.VisitPay.Enums;
    using Domain.Settings.Interfaces;
    using Entities;
    using Interfaces;
    using Models;
    using Octopus.Client;
    using Octopus.Client.Model;

    public class DeploymentService : IDeploymentService
    {
        private readonly Lazy<IOctopusRepository> _octopusRepository;
        private readonly Lazy<IApplicationSettingsService> _applicationSettings;
        private readonly Lazy<IDeploymentHistoryRepository> _deploymentHistoryRepository;

        private const string QaToolsDataProjectName = "VisitPay - QaTools Data";

        private static readonly IvhEnvironmentEnum[] ValidEnvironments =
        {
            IvhEnvironmentEnum.Demo,
            IvhEnvironmentEnum.demo01,
            IvhEnvironmentEnum.demo02,
            IvhEnvironmentEnum.demo03,
            IvhEnvironmentEnum.demo04,
            IvhEnvironmentEnum.Qa,
            IvhEnvironmentEnum.Qa2,
            IvhEnvironmentEnum.qa01,
            IvhEnvironmentEnum.qa02,
            IvhEnvironmentEnum.qa03,
            IvhEnvironmentEnum.qa04,
            IvhEnvironmentEnum.qaa01,
            IvhEnvironmentEnum.qaa02,
            IvhEnvironmentEnum.qaa03,
            IvhEnvironmentEnum.qaa04,
            IvhEnvironmentEnum.Test,
            IvhEnvironmentEnum.test01,
            IvhEnvironmentEnum.test02,
            IvhEnvironmentEnum.Training,
            IvhEnvironmentEnum.training01,
            IvhEnvironmentEnum.training02
        };

        public DeploymentService(
            Lazy<IApplicationSettingsService> applicationSettings,
            Lazy<IDeploymentHistoryRepository> deploymentHistoryRepository,
            Lazy<IAppSettingsProvider> appSettingsProvider)
        {
            this._octopusRepository = new Lazy<IOctopusRepository>(() =>
            {
                OctopusServerEndpoint endpoint = new OctopusServerEndpoint(appSettingsProvider.Value.Get("Octopus.Url"), appSettingsProvider.Value.Get("Octopus.QaToolsApiKey"))
                {
                    Proxy = new WebProxy(appSettingsProvider.Value.Get("ProxyServerIP_W_Port"))
                };

                return new OctopusRepository(endpoint);
            });
            this._applicationSettings = applicationSettings;
            this._deploymentHistoryRepository = deploymentHistoryRepository;
        }

        public bool IsValidEnvironment()
        {
            return ValidEnvironments.Select(x => x.ToString()).ToList().Contains(this._applicationSettings.Value.Environment.Value);
        }

        public bool ReDeployQaToolsData(string clientName)
        {
            if (!this.IsValidEnvironment())
            {
                return false;
            }

            string environmentName = this._applicationSettings.Value.Environment.Value.ToString();

            return this.ReDeployProject(QaToolsDataProjectName, environmentName, clientName);
        }

        private bool ReDeployProject(string projectName, string environmentName, string clientName)
        {
            DeploymentResource lastDeployment = this.GetLatestDeployment(projectName, environmentName, clientName);
            bool deployed = false;

            if (lastDeployment != null)
            {
                bool lastDeploymentInProgress = this._octopusRepository.Value.Tasks.GetAllActive().Any(x => x.Id == lastDeployment.TaskId);

                if (!lastDeploymentInProgress)
                {
                    DeploymentResource newDeployment = new DeploymentResource
                    {
                        ReleaseId = lastDeployment.ReleaseId,
                        ProjectId = lastDeployment.ProjectId,
                        EnvironmentId = lastDeployment.EnvironmentId,
                        TenantId = lastDeployment.TenantId
                    };

                    this._octopusRepository.Value.Deployments.Create(newDeployment);
                    deployed = true;
                }
            }

            return deployed;
        }

        private DeploymentResource GetLatestDeployment(string projectName, string environmentName, string clientName)
        {
            ProjectResource project = this._octopusRepository.Value.Projects.FindByName(projectName);
            EnvironmentResource environment = this._octopusRepository.Value.Environments.FindByName(environmentName);
            TenantResource tenant = this._octopusRepository.Value.Tenants.FindByName(clientName);

            if (project == null || environment == null || tenant == null)
            {
                return null;
            }

            DeploymentResource deployment = null;

            //get latest deployments from Octopus
            this._octopusRepository.Value.Deployments.Paginate(project.Id.ToArrayOfOne(), environment.Id.ToArrayOfOne(), page =>
            {
                if (!page.Items.Any())
                {
                    //end of results
                    return false;
                }

                //Get the tenant's latest
                DeploymentResource latest = page.Items.Where(x => x.TenantId == tenant.Id).OrderByDescending(i => i.Created).FirstOrDefault();

                //check the next page
                if (latest == null)
                {
                    return true;
                }

                //found latest for tenant
                deployment = latest;
                return false;
            });

            return deployment;
        }

        public bool DeploymentInProgress(string clientName)
        {
            string environmentName = this._applicationSettings.Value.Environment.Value.ToString();
            DeploymentResource lastDeployment = this.GetLatestDeployment(QaToolsDataProjectName, environmentName, clientName);
            bool lastDeploymentInProgress = false;

            if (lastDeployment != null)
            {
                lastDeploymentInProgress = this._octopusRepository.Value.Tasks.GetAllActive().Any(x => x.Id == lastDeployment.TaskId);
            }

            return lastDeploymentInProgress;
        }

        public IList<DeploymentHistoryViewModel> GetDeploymentHistory()
        {
            IList<DeploymentHistory> deploymentHistoryList = this._deploymentHistoryRepository.Value.GetDeploymentHistory();

            return deploymentHistoryList.Select(Mapper.Map<DeploymentHistoryViewModel>).ToList();
        }

        public void LogDeploymentHistory(string userName, string clientName)
        {
            DeploymentHistory deploymentHistory = new DeploymentHistory
            {
                UserName = userName,
                ClientName = clientName,
                DeploymentDate = DateTime.UtcNow
            };
            this._deploymentHistoryRepository.Value.Insert(deploymentHistory);
        }

    }
}
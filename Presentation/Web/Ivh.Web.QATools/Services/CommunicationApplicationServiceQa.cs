﻿namespace Ivh.Web.QATools.Services
{
    using Application.Base.Common.Interfaces;
    using Application.Email.ApplicationServices;
    using Application.Email.Internal.Dtos;
    using Application.Email.Internal.Interfaces;
    using Application.User.Common.Dtos;
    using Common.VisitPay.Enums;
    using Domain.Guarantor.Interfaces;
    using Domain.User.Interfaces;
    using Ivh.Application.FinanceManagement.Common.Dtos;
    using Ivh.Domain.Email.Interfaces;
    using System;
    using System.Collections.Generic;

    public class CommunicationApplicationServiceQa :
        CommunicationApplicationServiceBase,
        ICommunicationApplicationServiceQa
    {
        public CommunicationApplicationServiceQa(
            Lazy<IApplicationServiceCommonService> applicationServiceCommonService,
            Lazy<ICommunicationSender> communicationSender,
            Lazy<ICommunicationTemplateLoader> communicationTemplateLoader,
            Lazy<ICommunicationService> communicationService,
            Lazy<IEmailService> emailService,
            Lazy<IGuarantorService> guarantorService,
            Lazy<IVisitPayUserRepository> visitPayUserRepository,
            Lazy<IVisitPayUserService> visitPayUserService
           ) : base(
            applicationServiceCommonService,
            communicationSender,
            communicationTemplateLoader,
            communicationService,
            emailService,
            guarantorService,
            visitPayUserRepository,
            visitPayUserService)
        {
        }

        public bool SendCommunication(
            SendTo sendTo,
            CommunicationTypeEnum communicationTypeEnum,
            CommunicationTypeEnum managingUserCommunicationTypeEnum,
            Func<VisitPayUserDto, IDictionary<string, string>> replacementFactory,
            FinancePlanDto financePlanDto,
            string overrideEmail = null)
        {
            return this.SendCommunications(
                sendTo,
                communicationTypeEnum,
                managingUserCommunicationTypeEnum,
                replacementFactory,
                financePlanDto,
                overrideEmail);
        }
    }
}
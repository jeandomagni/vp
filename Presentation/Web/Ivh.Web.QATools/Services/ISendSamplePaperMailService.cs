﻿using System.Collections.Generic;

namespace Ivh.Web.QATools.Services
{
    public interface ISendSamplePaperMailService
    {
        IList<string> Send(int vpGuarantorId);
    }
}

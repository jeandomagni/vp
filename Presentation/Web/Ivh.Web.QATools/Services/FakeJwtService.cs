﻿namespace Ivh.Web.QATools.Services
{
    using System;
    using System.IdentityModel.Tokens.Jwt;
    using System.Linq;
    using System.Security.Claims;
    using Microsoft.IdentityModel.Tokens;
    using SecurityToken = Microsoft.IdentityModel.Tokens;
    using SecurityTokenDescriptor = Microsoft.IdentityModel.Tokens.SecurityTokenDescriptor;
    using SigningCredentials = Microsoft.IdentityModel.Tokens.SigningCredentials;
    using SymmetricSecurityKey = Microsoft.IdentityModel.Tokens.SymmetricSecurityKey;
    using SecurityAlgorithms = Microsoft.IdentityModel.Tokens.SecurityAlgorithms;

    public class FakeJwtService
    {
        private const string FakeSecurityKey = "SuperSecret";
        private const int NumberOfMinutesValid = 30;

        public bool ValidateJwt(string jwtToken)
        {
            JwtSecurityTokenHandler tokenHandler = new JwtSecurityTokenHandler();
            JwtSecurityToken token = new JwtSecurityToken(jwtToken);
            byte[] securityKey = this.GetBytes(FakeSecurityKey);

            TokenValidationParameters validationParameters = new TokenValidationParameters()
            {
                ValidAudience = "https://www.mywebsite.com",
                IssuerSigningKey = new SymmetricSecurityKey(securityKey),
                ValidIssuer = "self"
            };

            try
            {
                ClaimsPrincipal principal = tokenHandler.ValidateToken(token.RawData, validationParameters, out SecurityToken.SecurityToken securityToken);
                Claim userData = principal.Claims.FirstOrDefault();
                if (userData != null)
                {
                    string isValid = userData.Value;
                    if (isValid != "IsValid")
                    {
                        return false;
                    }
                }
                else
                {
                    return false;
                }
            }
            catch (Exception)
            {
                // log the exception details here                     
                return false;
            }

            return true;
        }

        public string CreateJwt()
        {
            JwtSecurityTokenHandler tokenHandler = new JwtSecurityTokenHandler();
            byte[] securityKey = this.GetBytes(FakeSecurityKey);
            DateTime now = DateTime.UtcNow;

            SecurityTokenDescriptor tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new Claim[]
                {
                    new Claim(ClaimTypes.UserData, "IsValid", ClaimValueTypes.String, "(local)"),
                    new Claim(ClaimTypes.Name, "MR.DEV"),
                    new Claim(ClaimTypes.Role, "JWTTest"),
                }),
                Issuer = "self",
                Audience = "https://www.mywebsite.com",
                Expires = now.AddMinutes(NumberOfMinutesValid),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(securityKey), SecurityAlgorithms.HmacSha256Signature),
            };

            SecurityToken.SecurityToken token = tokenHandler.CreateToken(tokenDescriptor);
            string tokenString = tokenHandler.WriteToken(token);

            return tokenString;

        }
        private byte[] GetBytes(string str)
        {
            byte[] bytes = new byte[str.Length * sizeof(char)];
            Buffer.BlockCopy(str.ToCharArray(), 0, bytes, 0, bytes.Length);
            return bytes;
        }
    }
}
﻿window.QaTools = {};
window.QaTools.MockAchProcessor = (function () {

    var RequestModel = function () {

        var self = this;

        self.RequestGuid = ko.observable();
        self.Action = ko.observable();
        self.Amount = ko.observable();

        self.AchFileType = ko.observable();
        self.AchFileTypes = ko.observableArray();
        self.AchReturnCode = ko.observable();
        self.AchReturnCodes = ko.observableArray();
        self.AchReturnAmount = ko.observable();
        self.AchProcessDate = ko.observable();
        self.AchProcessed = ko.observable();

        self.IsAchActionSet = ko.observable();

        self.VpAccountName = ko.observable();
        self.VpLastFour = ko.observable();

        return self;

    };

    var ContainerModel = function (model) {

        var self = this;

        self.PaymentsQueued = ko.observableArray();

        if (model.Payments !== undefined && model.Payments !== null && model.Payments.length > 0) {
            for (var i = 0; i < model.Payments.length; i++) {
                self.PaymentsQueued.push(ko.mapping.fromJS(model.Payments[i], null, new RequestModel()));
            }
        }

        self.MergePayment = function (payment) {

            var m = ko.utils.arrayFirst(self.PaymentsQueued(), function (item) {
                return item.RequestGuid() === payment.RequestGuid;
            });

            if (m != null) {
                ko.mapping.fromJS(payment, null, m);
            } else {
                self.PaymentsQueued.unshift(ko.mapping.fromJS(payment, null, new RequestModel()));
            }

        };

        return self;

    };

    function block(b) {
        $('#block').remove();
        if (b) {
            $('body').append('<div id="block"></div>');
        }
    }

    function initDatepicker() {
        $('body').find('.datepicker').datepicker({
            dateFormat: 'yy-mm-dd',
            defaultDate: new Date()
        });
    }

    function init(model) {

        // knockout

        var vm = new ContainerModel(model);
        ko.applyBindings(vm, $('body')[0]);

        // signalr

        var hub = $.connection.mockPaymentProcessorHub;

        hub.client.achQueued = function (request) {
            vm.MergePayment(request);
            initDatepicker();
        };

        hub.client.achActionSet = function (request) {
            vm.MergePayment(request);
        };

        hub.client.achProcessed = function (requests) {
            for (var i = 0; i < requests.length; i++) {
                vm.MergePayment(requests[i]);
            }
        };

        // signalr start
        $.connection.hub.start().done(function () {
        });

        // jquery

        initDatepicker();

        $('header').on('click', '#btnProcess', function () {
            var processDate = $('#txtProcessDate').val();
            block(true);
            $.post('/MockPaymentProcessor/Ach', { processDate: processDate }, function (result) {
                block(false);
                if (result.Success) {
                    hub.server.achProcessed(result.RequestsProcessed);
                } else {
                    alert('error');
                }
            });
        });

        $('main').on('click', 'button', function () {
            var data = ko.dataFor($(this)[0]);
            hub.server.setAchAction(data.RequestGuid(), data.AchFileType(), data.AchProcessDate(), data.AchReturnCode(), data.AchReturnAmount());
        });

    };

    return {
        init: init
    }

});
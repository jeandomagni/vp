﻿window.QaTools = {};
window.QaTools.MockPaymentProcessor = (function () {

    var RequestModel = function () {

        var self = this;

        self.RequestGuid = ko.observable();
        self.Action = ko.observable();
        self.Action.IsStoreVerify = ko.computed(function () {
            var action = (self.Action() || '').toString();
            return action.toUpperCase() === 'STORE' || action.toUpperCase() === 'VERIFY';
        });
        self.Amount = ko.observable();

        self.ResponseTypes = ko.observableArray();
        self.ResponseTypes.Grouped = ko.computed(function () {

            var index = {};
            var groups = [];

            ko.utils.arrayForEach(self.ResponseTypes(), function (item) {
                var name = ko.utils.unwrapObservable(item.Group.Name);
                if (!index.hasOwnProperty(name)) {
                    index[name] = {
                        label: name,
                        items: []
                    };
                    groups.push(index[name]);
                }
                index[name].items.push(item);
            });

            return groups;

        });
        self.SelectedResponseType = ko.observable();
        self.CompletedResponseType = ko.observable();
        self.CompletedResponseTypeText = ko.observable();

        self.AvsCodes = ko.observableArray();
        self.AvsCodes.Any = ko.computed(function () {
            var codes = ko.utils.unwrapObservable(self.AvsCodes);
            return codes && codes.length > 0;
        });
        self.AvsCodes.Grouped = ko.computed(function () {

            var index = {};
            var groups = [];

            ko.utils.arrayForEach(self.AvsCodes(), function (item) {
                var name = ko.utils.unwrapObservable(item.Group.Name);
                if (!index.hasOwnProperty(name)) {
                    index[name] = {
                        label: name,
                        items: []
                    };
                    groups.push(index[name]);
                }
                index[name].items.push(item);
            });

            return groups;

        });
        self.SelectedResponseAvs = ko.observable();
        self.CompletedResponseAvs = ko.observable();
        self.CompletedResponseAvs.HasValue = ko.computed(function () {
            var value = ko.utils.unwrapObservable(self.CompletedResponseAvs);
            return value && value.length > 0;
        });

        self.IsPending = ko.observable();
        self.IsSuccess = ko.observable();
        self.IsFail = ko.observable();

        self.VpPaymentMethodType = ko.observable();
        self.VpPaymentMethodType.Icon = ko.computed(function () {
            var v = ko.unwrap(self.VpPaymentMethodType);
            switch (v) {
                case 1:
                    return '/content/images/visa.png';
                case 2:
                    return '/content/images/mastercard.png';
                case 3:
                    return '/content/images/amex.png';
                case 4:
                    return '/content/images/discover.png';
                case 5:
                case 6:
                    return '/content/images/bank.png';
            }
            return '';
        });
        self.VpAccountName = ko.observable();
        self.VpLastFour = ko.observable();

        return self;

    };

    var ContainerModel = function (model) {

        var self = this;

        self.IsActivated = ko.observable(model.IsActive);
        self.DefaultSuccessful = ko.observable(model.DefaultSuccessful);
        self.DelayMs = ko.observable(model.DelayMs);
        self.PaymentsQueued = ko.observableArray();

        if (model.Payments !== undefined && model.Payments !== null && model.Payments.length > 0) {
            for (var i = 0; i < model.Payments.length; i++) {
                self.PaymentsQueued.push(ko.mapping.fromJS(model.Payments[i], null, new RequestModel()));
            }
        }

        self.MergePayment = function (payment) {

            var m = ko.utils.arrayFirst(self.PaymentsQueued(), function (item) {
                return item.RequestGuid() === payment.RequestGuid;
            });

            if (m != null) {
                ko.mapping.fromJS(payment, null, m);
            } else {
                self.PaymentsQueued.unshift(ko.mapping.fromJS(payment, null, new RequestModel()));
            }

        };

        return self;

    };

    function init(model) {

        // knockout

        var vm = new ContainerModel(model);
        ko.applyBindings(vm, $('body')[0]);

        // signalr

        var hub = $.connection.mockPaymentProcessorHub;

        hub.client.isActivated = function (isActivated) {
            vm.IsActivated(isActivated);
        };

        hub.client.defaultsSet = function (delayMs, defaultSuccessful) {
            vm.DelayMs(delayMs);
            vm.DefaultSuccessful(defaultSuccessful.toString().toLowerCase());
        };

        hub.client.paymentQueued = function (request) {
            vm.MergePayment(request);
        };

        hub.client.paymentComplete = function (request) {
            vm.MergePayment(request);
        };

        // signalr start
        $.connection.hub.start();

        // jquery

        $('header').on('click', '#btnOn', function () {
            $.post('/MockPaymentProcessor/Activate', { isActivated: true });
        });

        $('header').on('click', '#btnOff', function () {
            $.post('/MockPaymentProcessor/Activate', { isActivated: false });
        });

        $('header').on('click', '#btnSet', function () {
            hub.server.setDefaults(vm.DelayMs(), vm.DefaultSuccessful());
        });

        $('main').on('click', 'button', function () {
            var data = ko.dataFor($(this)[0]);
            hub.server.completePaymentRequest(data.RequestGuid(), data.SelectedResponseType(), data.SelectedResponseAvs());
        });

    };

    return {
        init: init
    };

});
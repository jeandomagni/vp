﻿window.QaTools = {};
window.QaTools.MockCommunication = (function () {

    var ContainerModel = function (model) {
        var self = this;
        self.IsActivated = ko.observable(model.IsActive);
        return self;
    };

    function init(model) {

        // knockout

        var vm = new ContainerModel(model);
        ko.applyBindings(vm, $('body')[0]);

        // jquery

        $('header').on('click', '#btnOn', function () {
            $.post('/MockCommunication/Activate', { isActivated: true });
            location.reload();
        });

        $('header').on('click', '#btnOff', function () {
            $.post('/MockCommunication/Activate', { isActivated: false });
            location.reload();
        });

        $('header')
            .on('click', '#btnSend', function() {
                $.post('/MockCommunication/SendSmsMessages', { vpGuarantorId: $('#vpGuarantorId').val() });
            });
    };

    return {
        init: init
    };

});
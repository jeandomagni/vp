﻿$(document).ready(function () {
    $("#SelectedSentToUserId").on("change", function () {
        var selectedValue = $(this).val();
        var result = model.filter(obj => obj.SentToUserId == selectedValue);
        
        $("#text-message").html(result[0].Body);
        $('#SelectedPhoneNumber').val(result[0].PhoneNumber);
        
        $.ajax(
            {
                url: '/MockCommunication/GetPaymentsPartial',
                type: 'GET',
                data: { visitPayUserId: selectedValue },
                contentType: 'application/json; charset=utf-8',
                success: function (data) {
                    $("#payment-table").html(data);
                },
                error: function () {
                    alert("error");
                }
            });
    });
});
﻿window.QaTools.Components = {};
window.QaTools.Components.InterceptorsComponent = (function () {

    var promise = $.Deferred();
    var promiseModel = $.Deferred();

    $.when(promiseModel).done(function (model) {

        promise.resolve({
            template: $('#tmplInterceptorsComponent').html(),
            data: function () {
                return model;
            },
            methods: {
                onSubmit: function (e) {
                    e.preventDefault();
                    var self = this;
                    $.post(window.QaTools.Config.UrlInterceptorsSet, { model: model }, function (data) {
                        self.Urls = data.Urls;
                    });
                }
            }
        });

    });

    $.post(window.QaTools.Config.UrlInterceptorsData, function (model) {
        promiseModel.resolve(model);
    });

    return promise;

});

window.QaTools.Components.InterceptListComponent = (function () {

    return $.Deferred().resolve({
        template: $('#tmplInterceptListComponent').html(),
        data: function () {
            return {
                Requests: [],
            }
        },
        created: function () {
            var self = this;
            messageBus.$on('incoming', function (interceptVm) {
                self.Requests.unshift(interceptVm);
            });
        },
        methods: {
            click: function (request) {
                request.HasResponded = true;
                messageBus.$emit('outgoing', request);
                this.$forceUpdate();
            }
        }
    });

});

var messageBus = new Vue({
    methods: {
        init: function () {

            var self = this;

            // signalr
            var hub = $.connection.mockEndpointHub;

            // outgoing response from js
            self.$on('outgoing', function (interceptVm) {
                console.info('outgoing', interceptVm.RespondWith.StatusCode);
                hub.server.outgoingResponse(interceptVm);
            });

            // incoming request from controller
            hub.client.incomingRequest = function (interceptVm) {
                self.$emit('incoming', interceptVm);
            };

            // signalr start
            $.connection.hub.start().done(function () {
                self.$emit('connected', 'signalr connected');
            });

        }
    }
});

$(document).ready(function () {

    var app = new Vue({
        el: $('#app')[0],
        components: {
            'interceptors-component': window.QaTools.Components.InterceptorsComponent,
            'intercept-list-component': window.QaTools.Components.InterceptListComponent
        }
    });

    messageBus.$on('connected', function (message) {
        console.info(message);
    });
    messageBus.init();

});
﻿window.QaTools = (function ($) {

    $(document).ready(function() {
            var $scope = $('#create-user-container');
            var form = $scope.find('#frmCreatePersona');
            var refreshPassword = $scope.find("#btnRefreshPassword");

            $(form).on('submit', function(e) {
                e.preventDefault();
                $.blockUI();
                $.post(form.attr('action'), form.serialize(), function(result) {
                    $.unblockUI();
                    $("#persona-user-message").html(result);
                });
            });

            $(refreshPassword).on('click', function (e) {
                var url = $(this).data('refresh-password-url');
                $.get(url, function (data) {
                    $(".js-password").val(data);
                });
            });

    });
})(jQuery);
﻿
function checkStatus() {
    var clientName = $("#clientNameId").val();
    var options = {
        type: "POST",
        url: '/Deployment/CheckDeployStatus',
        data: { clientName: clientName },
        success: function (response) {
            if (response.deployFinished) {
                $('#statusIcon').html("<span class=\"ok glyphicon glyphicon-ok\"></span>");
            }
            else {
                setTimeout(checkStatus, 4000);
            }
        }
    };
    $.ajax(options);
}

function refreshGrid() {
    var url = '/Deployment/GetDeploymentHistory';

    $('#resetDataHistoryGrid').jqGrid({
        url: url,
        datatype: 'json',
        mtype: 'GET',
        emptyrecords: 'No records to display',
        colNames: ['Id', 'User Name', 'Client', 'Reset Date'],
        colModel: [
            { key: true, name: 'DeploymentHistoryId', index: 'DeploymentHistoryId', width: 25, align: 'center' },
            { key: false, name: 'UserName', index: 'UserName', width: 85, align: 'left' },
            { key: false, name: 'ClientName', index: 'ClientName', width: 85, align: 'left' },
            { key: false, name: 'DeploymentDate', index: 'DeploymentDate', width: 85, align: 'left' }
        ],
        jsonReader: {
            root: 'Data',
            id: 'DeploymentHistoryId',
            repeatitems: true
        },
        pager: 'resetDataHistoryPager',
        rowNum: 50,
        autowidth: true,
        height: 'auto',
        viewrecords: true,
        gridComplete: function() {
            $('#jqGridSupportRequestsPager').find('#input_jqGridPager').find('input').off('change').on('change', function () {
                grid.jqGrid().trigger('reloadGrid', [{ page: $(this).val() }]);
            });
        },
        defaults: {
            recordtext: 'View {0} - {1} of {2}',
            emptyrecords: 'No records to view',
            loadtext: 'Loading...',
            pgtext: 'Page {0} of {1}'
        }
    });
    $('#resetDataHistoryGrid').jqGrid('setGridParam', { datatype: 'json', url: url }).trigger('reloadGrid');
}



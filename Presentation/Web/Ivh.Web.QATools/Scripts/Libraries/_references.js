/// <autosync enabled="true" />
/// <reference path="../_references.js" />
/// <reference path="bootstrap.js" />
/// <reference path="jqGrid-4.8.2/js/jquery.jqGrid.min.js" />
/// <reference path="jqGrid-4.8.2/js/jquery.jqGrid.src.js" />
/// <reference path="jqGrid-4.8.2/plugins/grid.addons.js" />
/// <reference path="jqGrid-4.8.2/plugins/grid.postext.js" />
/// <reference path="jqGrid-4.8.2/plugins/grid.setcolumns.js" />
/// <reference path="jqGrid-4.8.2/plugins/jquery.contextmenu.js" />
/// <reference path="jqGrid-4.8.2/plugins/jquery.searchFilter.js" />
/// <reference path="jqGrid-4.8.2/plugins/jquery.tablednd.js" />
/// <reference path="jqGrid-4.8.2/plugins/ui.multiselect.js" />
/// <reference path="jqGrid-4.8.2/src/grid.base.js" />
/// <reference path="jqGrid-4.8.2/src/grid.celledit.js" />
/// <reference path="jqGrid-4.8.2/src/grid.common.js" />
/// <reference path="jqGrid-4.8.2/src/grid.filter.js" />
/// <reference path="jqGrid-4.8.2/src/grid.formedit.js" />
/// <reference path="jqGrid-4.8.2/src/grid.grouping.js" />
/// <reference path="jqGrid-4.8.2/src/grid.import.js" />
/// <reference path="jqGrid-4.8.2/src/grid.inlinedit.js" />
/// <reference path="jqGrid-4.8.2/src/grid.jqueryui.js" />
/// <reference path="jqGrid-4.8.2/src/grid.pivot.js" />
/// <reference path="jqGrid-4.8.2/src/grid.subgrid.js" />
/// <reference path="jqGrid-4.8.2/src/grid.treegrid.js" />
/// <reference path="jqGrid-4.8.2/src/grid.utils.js" />
/// <reference path="jqGrid-4.8.2/src/jqDnR.js" />
/// <reference path="jqGrid-4.8.2/src/jqModal.js" />
/// <reference path="jqGrid-4.8.2/src/jquery.fmatter.js" />
/// <reference path="jqGrid-4.8.2/src/jquery.jqGrid.js" />
/// <reference path="jquery.signalr-2.2.2.min.js" />
/// <reference path="jquery.validate.min.js" />
/// <reference path="jquery.validate.unobtrusive.js" />
/// <reference path="jquery-2.1.4.js" />
/// <reference path="jquery-ui-1.11.4.custom/external/jquery/jquery.js" />
/// <reference path="jquery-ui-1.11.4.custom/jquery-ui.js" />
/// <reference path="knockout.mapping-latest.js" />
/// <reference path="knockout-3.4.2.js" />
/// <reference path="modernizr-2.8.3.js" />
/// <reference path="moment.min.js" />
/// <reference path="moment-with-locales.min.js" />
/// <reference path="npm.js" />
/// <reference path="respond.js" />
/// <reference path="respond.matchmedia.addListener.js" />
/// <reference path="codemirror.min.js" />
/// <reference path="searchcursor.min.js" />
/// <reference path="mergely.js" />
/// <reference path="../mockcommunication/mockcommunication.js" />
/// <reference path="../MockPaymentProcessor/mockachprocessor.js" />
/// <reference path="../MockPaymentProcessor/mockpaymentprocessor.js" />
/// <reference path="../models/eob/add-835-vm.js" />
/// <reference path="../models/eob/adjudication-information-vm.js" />
/// <reference path="../models/eob/claim-adjustment-pivot-vm.js" />
/// <reference path="../models/eob/claim-payment-information-vm.js" />
/// <reference path="../models/eob/functional-group-header-trailer-vm.js" />
/// <reference path="../models/eob/header-number-vm.js" />
/// <reference path="../models/eob/industry-code-identification-vm.js" />
/// <reference path="../models/eob/service-payment-information-vm.js" />
/// <reference path="../models/eob/transaction-set-header-trailer835-vm.js" />
/// <reference path="../models/ko-model.js" />
/// <reference path="../Shared/_sideNav.js" />
/// <reference path="../Shared/common.js" />
/// <reference path="../Shared/enums.js" />
/// <reference path="../Shared/IdleTimer.js" />
/// <reference path="../Shared/navMenu.js" />
/// <reference path="../Shared/Tooltip.js" />
/// <reference path="../../Views/ETL/addVisitTransaction.js" />
/// <reference path="../../Views/ETL/Guarantors/_ssoGeneration.js" />
/// <reference path="../../Views/ETL/Guarantors/guarantor.js" />
/// <reference path="../../Views/ETL/Guarantors/guarantorDetails.js" />
/// <reference path="../../Views/ETL/hsGuarantorVisits.js" />
/// <reference path="../../Views/ETL/recalls.js" />
/// <reference path="../../Views/Guarantors/clearGuarantorData.js" />
/// <reference path="../../Views/Guarantors/createGuarantor.js" />
/// <reference path="../../Views/Guarantors/guarantor.js" />
/// <reference path="../../Views/Guarantors/guarantorAuditLog.js" />
/// <reference path="../../Views/Guarantors/guarantorDateAdjustments.js" />
/// <reference path="../../Views/Guarantors/guarantorDetail.js" />
/// <reference path="../../Views/Guarantors/resolveMatches.js" />
/// <reference path="../../Views/Guarantors/Visits/createVisit.js" />
/// <reference path="../../Views/Guarantors/VisitTransactions/createVisitTransaction.js" />
/// <reference path="../../Views/Shared/_modalControl.js" />

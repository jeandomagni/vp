﻿(function () {
    window.QaTools = window.QaTools || (window.QaTools = {});
    window.QaTools.Vue = window.QaTools.Vue || (window.QaTools.Vue = {});
    window.QaTools.Vue.Mixins = window.QaTools.Vue.Mixins || (window.QaTools.Vue.Mixins = {});
    window.QaTools.Vue.Mixins.TableMixin = (function (url, overrideOptions) {

        var customSorting = {};
        var decimalColumns = overrideOptions && overrideOptions.decimalColumns ? overrideOptions.decimalColumns : [];
        if (decimalColumns.length > 0) {
            for (var i = 0; i < decimalColumns.length; i++) {
                var columnName = decimalColumns[i];
                customSorting[columnName] = function (ascending) {
                    return function (a, b) {
                        var d1 = parseFloat(a[columnName]);
                        var d2 = parseFloat(b[columnName]);
                        return (ascending) ? d1 >= d2 ? 1 : -1 : d1 <= d2 ? 1 : -1;
                    }
                }
            }
        }

        var options = {
            columns: [],
            customSorting: customSorting,
            tableData: [],
            sortable: [],
            headings: {},
            orderBy: {},
            perPage: 20,
            texts: {
                filter: ''
            }
        };

        $.extend(options, overrideOptions);

        options.perPageValues = [options.perPage];

        return {
            data: {
                loaded: false,
                options: options,
                selectedValues: [],
                tableData: []
            },
            created: function () {
                this.loadData();
            },
            computed: {
                hasSelectedValues: function () {
                    return this.selectedValues.length > 0;
                }
            },
            methods: {
                loadData: function () {
                    var self = this;
                    self.selectedValues = [];
                    $.post(url, function (data) {

                        if (!data || data.length === 0) {
                            self.loaded = false;
                            return;
                        }

                        var keyNames = Object.keys(data[0]);
                        var sortable = keyNames;

                        if (overrideOptions.notSortable) {
                            sortable = sortable.filter(x => overrideOptions.notSortable.indexOf(x) === -1);
                        }

                        var tooltips = {};
                        for (var i = 0; i < keyNames.length; i++) {
                            tooltips[keyNames[i]] = keyNames[i];
                        }

                        self.columns = keyNames;
                        self.options.headingsTooltips = tooltips;
                        self.options.sortable = sortable;
                        self.tableData = data;
                        self.loaded = true;

                    });
                }
            }
        };
    });
})();
﻿if (_sideNav === undefined) {
    var _sideNav = {};
}
_sideNav = (
    function($) {

        var MenuItem = function (id, visible) {
            var _context = $("[name='sideNav']").attr("id"),
                _id = id,
                _element = $("#" + id),
                _link = $($(_element).find("a"));

            this.setActive = function (active) {
                active = (typeof active != 'undefined' ? active : true);
                $("[name=sideNav] li").removeClass("active");
                if (active) {
                    $(_element).addClass("active");
                }
            }
            this.getId = function() {
                return _id;
            }
            this.setActionUrl = function(newUrl) {
                $(_link).data("action-url", newUrl);
            }
            this.getActionUrl = function () {
                return $(_link).data("action-url");
            }
            this.getActionLink = function () {
                return $(_link).attr("href");
            }
            this.setActionLink = function (parms) {
                var actionUrl = this.getActionUrl(),
                    hasParms = typeof parms != "undefined";
                actionUrl += hasParms ? "?" : "";

                if (hasParms) {
                    $.each(parms, function (key, value) {
                        actionUrl += (key + "=" + value + "&");
                    });
                }
                $(_link).attr("href", "javascript: window.location.href='" + actionUrl + "';");
            }
            this.setVisible = function(show) {
                $(_element).removeClass("hidden");
                show = (typeof show != 'undefined' ? show : true);
                if (!show) {
                    $(_element).addClass("hidden");
                }
            }
            this.getContext = function () {
                return _context;
            }
        }

        var MenuList = function(htmlMenuList) {
            var _list = [];

            this.setMenuItems = function (menuItems) {
                $.each(menuItems, function (idx, item) {
                    var i = new MenuItem($(item).attr("id"), true);
                    i.setActionLink();
                    _list.push(i);
                });
            }
            this.addMenuItem = function(menuItem) {
                _list.push(menuItem);
            }
            this.getMenuItemById = function (id) {
                var retObj = {};
                $.each(_list, function (idx, item) {
                    if (item.getId() === id) {
                        retObj = item;
                    }
                });
                return retObj;
            }
            this.getMenuItems = function() {
                return _list;
            }
            this.setMenuItems(htmlMenuList);
            
        }

        var _sideNav = {
            getContext: function() {
                return $("[name='sideNav']").attr("id");
            },
            menuList: {},
            initMenuItems: function() {
                this.menuList = new MenuList($("[name='sideNav']").find("li"));
            },

            setLink: function (menuItemId) {
                var url = '';
                switch (menuItemId) {
                    case 'CreateVisitView':
                        url = '/Visits/CreateVisitView?userName=' + parms.userName + '&guarantorId=' + parms.guarantorId + "&hasVisits=" + parms.hasVisits;
                        break;

                    case 'CreateVisitTransactionView':
                        url = '/Visits/CreateVisitTransactionView?userName=' + parms.userName + '&guarantorId=' + parms.guarantorId + "&hasVisits=" + parms.hasVisits;
                        break;
                }

                el.attr("href", "javascript: window.location.href= '" + url + "';");

            },

            init: function () {
                // nothing here
                this.initMenuItems();
            },

            refreshMenu: function() {
                
            },

            showMenuItem: function (el, state) {

                state = (typeof state != 'undefined' ? state : true);
                $("#" + el).removeClass("hidden");
                $("#" + el).addClass(state ? "" : "hidden");
            },

            checkMenuOptionsState: function () {
                var url = window.location.href,
                    context = this.context();

                switch (this.context()) {
                    case "guarantors":
                        /*
                        var showDateAdjustments = url.indexOf("GetGuarantor") > -1,
                            showCreateGuarantor = true,
                            showCreateVisit = url.indexOf("GetGuarantor") > -1,
                            showCreateVisitTransaction = false;
                        if ($("#GuarantorHasVisits")) {
                            showCreateVisitTransaction = parseInt($("#GuarantorHasVisits").val()) > 0;
                        }
                        this.showMenuItem("GuarantorDatesAdjustment", showDateAdjustments);
                        this.showMenuItem("CreateGuarantor", showCreateGuarantor);
                        this.showMenuItem("CreateVisit", showCreateVisit);
                        this.showMenuItem("CreateVisitTransaction", showCreateVisitTransaction);
                        */
                        break;
                    default:
                        break;
                }

            }
        }

        _sideNav.init();

        return _sideNav;
})(jQuery);
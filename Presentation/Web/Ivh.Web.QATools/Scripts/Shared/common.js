﻿window.VisitPay = window.VisitPay || {};
window.VisitPay.QATools = window.VisitPay.QATools || {};

(function ($) {

    $.fn.parseValidation = function() {
        var $form = $(this);
        $form.removeData("validator").removeData("unobtrusiveValidation");

        $.validator.unobtrusive.parse($form);

        $form.on('invalid-form.validate', function (form, validator) {
            validator.settings && (validator.settings.focusInvalid = false);
        });

        $form.validate();

        return $form;
    };

})(jQuery);

if (qaCommon === undefined) {
    var qaCommon = {};
}

qaCommon = (function ($) {
    function setSaveByEnter() {
        if ($("#_Save")) {
            $(document).on("keydown", function (e) {
                if (e.keyCode === 13 && document.activeElement !== "text") {
                    e.preventDefault();
                    if (!$("#_Save").is(":disabled")) {
                        $("#_Save").click();
                    }
                }
            });
        }
    }

    var qaCommon = {

        init: function() {
            //setSaveByEnter();
        },

        monthName: ["", "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December" ],

        parseDate: function (str) {
            // http://stackoverflow.com/questions/8937408/regular-expression-for-date-format-dd-mm-yyyy-in-javascript 
            var m = str.match(/^(\d{1,2})-(\d{1,2})-(\d{4})$/);
            return (m) ? new Date(m[3], m[2] - 1, m[1]) : null;
        },

        getFormattedDate: function (date, pattern) {
            var m = date.getMonth() + 1,
                d = date.getDate(),
                y = date.getFullYear();

            switch (pattern) {
                case "m/d/yyyy":
                    return m + "/" + d + "/" + y;
                     break;

                case "mmm/d/yyyy":
                    return this.monthName[m] + " " + d + ", " + y;
                    break;

                case "yyyy/m/d":
                    return y + "/" + m + "/" + d;
                    break;

                case "yyyy/mmm/d":
                    return y + " " + this.monthName[m] + " " + d;
                    break;

                default:
                    return m + "/" + d + "/" + y;
            }
        },

        getCurrentDate: function () {
            // http://stackoverflow.com/questions/12409299/how-to-get-current-formatted-date-dd-mm-yyyy-in-javascript-and-append-it-to-an-i
            var today = new Date();
            var dd = today.getDate();
            var mm = today.getMonth() + 1; //January is 0!

            var yyyy = today.getFullYear();
            if (dd < 10) {
                dd = '0' + dd
            }
            if (mm < 10) {
                mm = '0' + mm
            }
            var today = mm + '/' + dd + '/' + yyyy;

            return today;
        },

        validateEmail: function (email) {
            // http://stackoverflow.com/questions/46155/validate-email-address-in-javascript 
            var re = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
            return re.test(email);
        },

        pad: function (num, size) {
            // http://stackoverflow.com/questions/2998784/how-to-output-integers-with-leading-zeros-in-javascript
            var s = num + "";
            while (s.length < size) s = "0" + s;
            return s;
        },

        isNumeric: function (n) {
            // http://stackoverflow.com/questions/9716468/is-there-any-function-like-isnumeric-in-javascript-to-validate-numbers
            return !isNaN(parseFloat(n)) && isFinite(n);
        },

        getEnumString: function (enumObj, val) {
            var lit = enumObj;
            for (var i in lit) {
                if (lit[i] === val) {
                    return i;
                }
            }
            return "No string found in enum object for value " + val;
        },

        getParameterByName: function (name) {
            // http://stackoverflow.com/questions/901115/how-can-i-get-query-string-values-in-javascript
            name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
            var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
                results = regex.exec(location.search);
            return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
        },

        alertMessage: function (type, message, delay) {

            //Have to do this because bootstrap actually removes the div from the DOM upon hitting the "close" button in the alert.
            var alertHtml =
                      "<div id='messageAlert' class='alert alert-message text-left hidden' style='position: absolute; width: 500px; right:400px; margin-top: 20px;'>"
                    + "     <a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a>"
                    + "     <div id='messageContent'></div>" 
                    + "</div>",
                selector = "";

            $("#alertMessagePlaceHolder").html(alertHtml);
            selector = $('#messageAlert');
            type = (type === "error") ? "danger" : type;  // danger and error are the same.
            delay = delay || 5000;
            type = type || "success";
            $(selector).find("#messageContent").html(message);
            $(selector).removeClass("alert-success");
            $(selector).removeClass("alert-warning");
            $(selector).removeClass("alert-danger");
            $(selector).removeClass("hidden");
            $(selector).css('display', 'none');
            $(selector).addClass("alert-" + type);

            //console.log(blockFade);
            $(selector).fadeIn(350, function() {});

            setTimeout(function() {
                $(selector).fadeOut(350, function() { $(selector).find("#messageContent").html("<strong></strong>"); });
            }, delay);
            /*if (!blockFade) {
                $(selector).fadeIn(delay, function () { }).fadeOut(delay, function () { $(selector).find("#messageContent").html("<strong></strong>"); });
            } else {
                $(selector).fadeIn(delay, function () { });
            }*/
        }
    }

    qaCommon.init();

    return qaCommon;
})(jQuery);
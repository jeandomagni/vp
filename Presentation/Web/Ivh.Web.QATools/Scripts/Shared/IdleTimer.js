﻿(function () {
    if (VisitPay.State.IsUserLoggedIn) {
        var userIdleTime = 0;
        var sessionIdleTime = 0;
        var baseIncrement = 30000; //30sec
        //30 seconds before logout
        var incrementWarning = (VisitPay.ClientSettings.SessionTimeoutInMinutes * 2) - 1;
        var incrementLogout = (VisitPay.ClientSettings.SessionTimeoutInMinutes * 2);
        var dialogBox = null;
        var idleInterval = null;

        function timerIncrement() {
            userIdleTime = userIdleTime + 1;
            sessionIdleTime = sessionIdleTime + 1;
            if (userIdleTime > incrementWarning) { // 20 minutes
                dialogBox.modal('show');
            }
            if (userIdleTime > incrementLogout) {
                if (idleInterval != null) {
                    clearTimeout(idleInterval);
                }
                window.location.pathname = VisitPay.Urls.LogoffUrl;
            }
            if (sessionIdleTime > incrementLogout) {
                //Make sure the session doesnt expire if the user is doing things.
                sessionIdleTime = 0;
                $.ajax(VisitPay.Urls.UpdateSession);
            }
        }

        function timerReset() {
            userIdleTime = 0;
            if (dialogBox !== null) {
                dialogBox.modal('hide');
            }
        }

        function continueClicked() {
            timerReset();
        }

        $(document).ready(function () {
            //Increment the idle time counter every minute.
            idleInterval = setInterval(timerIncrement, baseIncrement);
            dialogBox = $("#TimeoutWarning");
            dialogBox.modal('hide');
            dialogBox.find(".btn-primary").click(continueClicked);

            //Zero the idle timer on mouse movement.
            $(this).mousemove(function (e) {
                if (dialogBox !== null && !dialogBox.data('bs.modal').isShown) {
                    timerReset();
                }
            });
            $(this).keypress(function (e) {
                if (dialogBox !== null && !dialogBox.data('bs.modal').isShown) {
                    timerReset();
                }
            });
        });
    }
})();
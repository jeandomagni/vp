﻿if (qaEnums === undefined) {
    var qaEnums = {};
}

qaEnums = (function ($) {
    var qaEnums = {
        GuarantorStatuses: {
            "Pending": 1, 
            "Acknowledged": 2,
            "Visits Loaded": 3,
            "Closed": 4,
            "PendingClosed": 5
        }
    }
    return qaEnums;
})(jQuery);

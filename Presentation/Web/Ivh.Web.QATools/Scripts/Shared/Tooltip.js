﻿/*
 * USAGE:  To give an html element enhanced tooltip capabilities
 * add attribute: data-original-title="tooltip info"
 * also, add rel='tooltipTop/Bottom/Left/Right'
 * to designate which of the four tooltip positions
 * is desired.
 */
jQuery(function ($) {
    var toolTip = {
        refreshToolTips: function() {
            $('[rel=tooltipTop]').tooltip({
                placement: "top",
                trigger: "hover"
            });

            $('[rel=tooltipBottom]').tooltip({
                placement: "bottom",
                trigger: "hover"
            });

            $('[rel=tooltipLeft]').tooltip({
                placement: "left",
                trigger: "hover"
            });


            $('[rel=tooltipRight]').tooltip({
                placement: "right",
                trigger: "hover"
            });
        }

    }
    toolTip.refreshToolTips();
    window.toolTip = toolTip;
});

﻿$(document).ready(function() {

    Vue.use(VueTables.ClientTable);

    var app = new Vue({
        el: "#grid-unbatched",
        data: function() {
            return {
                isRealtime: false
            }
        },
        mixins: [
            new window.QaTools.Vue.Mixins.TableMixin('/Jobs/GetUnclearedPaymentAllocations', {
                headings: {
                    VpOutboundVisitTransactionId: ''
                },
                orderBy: { column: 'InsertDate', ascending: false },
                notSortable: ['VpOutboundVisitTransactionId'],
                decimalColumns: ['TransactionAmount']
            })
        ],
        methods: {
            onSubmit: function(values) {
                var self = this;
                $.post('/Jobs/ClearPaymentAllocations', { model: values}, function() {
                    self.loadData();
                });
            },
            onSubmitAll: function() {
                var values = [];
                for (var i = 0; i < this.tableData.length; i++) {
                    values.push(this.tableData[i].VpOutboundVisitTransactionId);
                }
                this.onSubmit(values);
            },
            onSubmitSelected: function() {
                this.onSubmit(this.selectedValues);
            }
        }
    });

});
﻿/*
ApiUrls are set in _JavascriptNamespaces.cshtml
*/


function showSettingValue() {
    var selectedValue = $('#settingKeySelect').val();
    $('#settingValue').val(selectedValue);
}

function setEditability() {
    var selectedOption = $('#settingKeySelect').find(":selected");
    var isEditable = selectedOption.attr("data-editable") === "true";
    if (isEditable) {
        $("#buttonChangeSetting").prop("disabled", false);
        $("#buttonRestoreSetting").prop("disabled", false);
    } else {
        $("#buttonChangeSetting").prop("disabled", true);
        $("#buttonRestoreSetting").prop("disabled", true);
    }
}

function changeValueOnServer() {
    var settingValue = $('#settingValue').val();
    var settingName = $("#settingKeySelect option:selected").text();
    $.when(
        $.post(VisitPay.Urls.overrideSettingValueApiUrl, {
            settingName: settingName,
            settingValue: settingValue
        })
    )
        .done(function (isSuccess) {
            if (isSuccess) {
                $('#successHeading').text('Change Setting Value');
                $('#successMessage').text('Setting: ' + settingName + ' was successfully changed.');
                $('#successModal').modal('show');
            } else {
                $('#errorHeading').text('Error - Change Setting Value');
                $('#errorMessage').text('Request to change "' + settingName + '" was rejected.');
                $('#errorModal').modal('show');
            }
        })
        .fail(function () {
            $('#errorHeading').text('Error - Change Setting Value');
            $('#errorMessage').text('Setting: ' + settingName + ' failed to get changed due to an error.');
            $('#errorModal').modal('show');
        });
}

function clearSelectedOverrideValueOnServer() {
    var settingName = $("#settingKeySelect option:selected").text();
    $.when(
        $.post(VisitPay.Urls.clearSettingOverrideValueApiUrl, {
            key: settingName
        })
    )
        .done(function (isSuccess) {
            if (isSuccess) {
                $('#successHeading').text('Restore Setting Value');
                $('#successMessage').text('The value of ' + settingName +  ' was successfully restored.');
                $('#successModal').modal('show');
            } else {
                $('#errorHeading').text('Error - Restore Setting Value');
                $('#errorMessage').text('Request to restore the value of ' + settingName + ' was rejected.');
                $('#errorModal').modal('show');
            }
        })
        .fail(function () {
            $('#errorHeading').text('Error - Restore Setting Value');
            $('#errorMessage').text('Request to restore the value of ' + settingName + ' failed due to an error.');
            $('#errorModal').modal('show');
        });
}

function clearAllOverrideValuesOnServer() {
    $.when(
        $.post(VisitPay.Urls.clearSettingOverrideValuesApiUrl)
    )
        .done(function (isSuccess) {
            if (isSuccess) {
                $('#successHeading').text('Restore All Settings');
                $('#successMessage').text('All setting values were successfully restored.');
                $('#successModal').modal('show');
            } else {
                $('#errorHeading').text('Error - Restore All Settings');
                $('#errorMessage').text('Request to restore all setting values was rejected.');
                $('#errorModal').modal('show');
            }
        })
        .fail(function () {
            $('#errorHeading').text('Error - Restore All Settings');
            $('#errorMessage').text('All setting values failed to be restored due to an error.');
            $('#errorModal').modal('show');
        });
}

function showLogoUploadCompleteMessage(success) {
        if (success) {
        $('#successHeading').text('Logo Upload');
        $('#successMessage').text('Logo was successfully uploaded.');
        $('#successModal').modal('show');
    } else
    {
        $('#errorHeading').text('Error - Logo Upload');
        $('#errorMessage').text('Request to change the logo was rejected.');
        $('#errorModal').modal('show');
    } 
}

function showDemoPayerValues() {

    var $selectedItem = $('option:selected', this);
    if ($selectedItem) {
        $('#payorIcon').attr('src', $selectedItem.data('icon'));
        $('#payorUrl').attr('href', $selectedItem.data('url'));
        $('#payorUrl').text($selectedItem.data('url'));
        $('#payorText').text($selectedItem.text());
    }
}

function submitDemoPayer(e) {
    e.preventDefault();
    var $selectedItem = $('option:selected', this);
    var id = $selectedItem.val();
    if (id && id.length > 0) {
        $.post($(this).attr('action'), { eobExternalLinkId: id }, function(success) {
            if (success) {
                $('#successHeading').text('Payor Updated');
                $('#successMessage').text('Payor was successfully updated.');
                $('#successModal').modal('show');
            } else {
                $('#errorHeading').text('Error - Payor Update');
                $('#errorMessage').text('Request to change the payor was rejected.');
                $('#errorModal').modal('show');
            }
        });
    }
}

$(document).ready(function() {

    setTimeout(function() {
        $('.js-upload-alert').fadeOut(1000);
    }, 2000);

    $('#settingKeySelect').on('change', function() {
        showSettingValue();
        setEditability();
    });
    $('#buttonChangeSetting').click(function() {
            changeValueOnServer();
        }
    );
    $('#buttonRestoreAllSettings').click(function() {
            clearAllOverrideValuesOnServer();
        }
    );
    $('#buttonRestoreSetting').click(function() {
            clearSelectedOverrideValueOnServer();
        }
    );
    $('#buttonDemoSettings').click(function() {
            window.location = VisitPay.Urls.filterDemoSettingsApiUrl;
        }
    );

    $('#successModal').on('hidden.bs.modal', function() {
        location.reload();
    });

    $('#demoPayersSelect').on('change', showDemoPayerValues);

    $('#demoPayersForm').on('submit', submitDemoPayer);
});
﻿$(document).ready(function () {

    var $form = $("#frmUpdateThemeSettings");

    $('.color-palette-selector').on('click', function() {
        var name = $(this).data('color-pallate-name');
       
        var url = demoSettingColorSwitchApiUrl + '?colorPaletteName=' + name;
        window.location.replace(url);
    });

    $form.on("submit", function (event) {
        event.preventDefault();
        var data = $(this).serialize();
        url = $(this).attr('action');
        $.post(url, data, function (result) {
            $("#info-message").html(result);
            $('.js-theme-change-alert').show();
        });
    });

    $('.jscolor').on('change', function () {
        var styleSheet = document.getElementById("dynamicStyleSheet").sheet;
        var cssSelector = $(this).data("css-name");
        var cssRule = getCssRule(cssSelector, styleSheet);
        var cssRuleIndex = cssRule.index;
        //add the new color
        //if it is not a hex should throw potentially (rgb works though) 
        var newValue = (isNaN(parseInt(this.value, 16))) ?this.value :'#' + this.value;
        var cssText = cssRule.cssText.replace(/(?:color:[^;]*);/g, 'color: ' + newValue + ';');
        styleSheet.deleteRule(cssRuleIndex);
        addCSSRule(styleSheet, cssText, cssRuleIndex);
    });

    function getCssRule(cssSelector, styleSheet) {
        var classes = styleSheet.rules || styleSheet.cssRules;
        for (var x = 0; x < classes.length; x++) {
            if (classes[x].selectorText === cssSelector) {
                var cssText = (classes[x].cssText) ? classes[x].cssText : classes[x].style.cssText;
                return {
                    index: x,
                    cssText: cssText
                }
            }
        }
        return {};
    }

    function addCSSRule(sheet, rule, index) {
        if ("insertRule" in sheet) {
            sheet.insertRule(rule, index);
        }
        else if ("addRule" in sheet) {
            sheet.addRule(rule, index);
        }
    }

});
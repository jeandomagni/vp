﻿namespace Ivh.Web.QATools
{
    using System;
    using System.Web.Mvc;
    using System.Web.Optimization;
    using System.Web.Routing;
    using Autofac;
    using Common.DependencyInjection;
    using Common.ServiceBus.Constants;
    using Common.Web.Administration;
    using Common.Web.Application;
    using MassTransit;

    public class MvcApplication : HttpApplicationBase
    {
        protected override void Application_Start()
        {
            ViewEngines.Engines.Clear();
            ViewEngines.Engines.Add(new RazorViewEngine());
            /*
            var ve = new RazorViewEngine();
            ve.ViewLocationCache = new TwoLevelViewCache(ve.ViewLocationCache);
            ViewEngines.Engines.Add(ve);
             */
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            ApplicationHost.ConfigureStartUp();

            base.Application_Start();
        }

        protected override void Application_End(object sender, EventArgs e)
        {
            try
            {
                IBusControl serviceBus = IvinciContainer.Instance.Container().Resolve<IBusControl>();
                if (serviceBus != null)
                {
                    serviceBus?.Stop(TimeSpan.FromSeconds(ServiceBus.StopTimeoutSeconds));
                    IvinciContainer.Instance.Container().Dispose();
                }
            }
            catch
            {
            }
            finally
            {
                base.Application_End(sender, e);
            }
        }
    }
}

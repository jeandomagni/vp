﻿namespace Ivh.Web.QATools.Controllers
{
    using System;
    using Application.Content.Common.Interfaces;
    using Application.Monitoring.Common.Interfaces;
    using Application.Settings.Common.Interfaces;
    using Common.Web.Controllers;
    using Domain.Settings.Interfaces;

    public class MonitoringController : BaseMonitoringController
    {
        public MonitoringController(
            Lazy<ISystemHealthApplicationService> monitoringApplicationService,
            Lazy<IApplicationSettingsService> applicationSettingsService,
            Lazy<ISettingsApplicationService> settingsApplicationService,
            Lazy<IContentApplicationService> contentApplicationService
            ) : base(monitoringApplicationService, applicationSettingsService, settingsApplicationService, contentApplicationService)
        {

        }
    }
}
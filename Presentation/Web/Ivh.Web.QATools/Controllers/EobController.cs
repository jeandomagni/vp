﻿namespace Ivh.Web.QATools.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using System.Web.Mvc;
    using Application.Core.Common.Dtos;
    using Application.Core.Common.Interfaces;
    using Common.VisitPay.Messages.HospitalData.Dtos;
    using Common.Web.Interfaces;
    using Domain.HospitalData.Eob.Entities;
    using Domain.HospitalData.Eob.Interfaces;
    using Ivh.Application.Core.Common.Dtos.Eob;
    using Ivh.Domain.Eob.Entities;
    using Models.Eob;
    using IChangeSetApplicationService = Application.HospitalData.Common.Interfaces.IChangeSetApplicationService;

    public class EobController : BaseController
    {
        private readonly Lazy<IChangeSetApplicationService> _changeSetApplicationService;
        private readonly Lazy<IVisitApplicationService> _visitApplicationService;
        private readonly Lazy<IVisitEobApplicationService> _visitEobApplicationService;
        private readonly Lazy<IEob835Service> _eob835Service;
        private readonly Lazy<IEobClaimAdjustmentReasonCodeRepository> _remittanceCodeRepository;
        private readonly Lazy<IEobRemittanceAdviceRemarkCodeRepository> _remittanceRemarkCodeRepository;
        private readonly Lazy<IEobPayerFilterRepository> _eobPayerFilterRepository;

        public EobController(
            Lazy<IBaseControllerService> baseControllerService,
            Lazy<IChangeSetApplicationService> changeSetApplicationService,
            Lazy<IVisitApplicationService> visitApplicationService,
            Lazy<IVisitEobApplicationService> visitEobApplicationService,
            Lazy<IEob835Service> eob835Service,
            Lazy<IEobClaimAdjustmentReasonCodeRepository> remittanceCodeRepository,
            Lazy<IEobRemittanceAdviceRemarkCodeRepository> remittanceRemarkCodeRepository,
            Lazy<IEobPayerFilterRepository> eobPayerFilterRepository) : base(baseControllerService)
        {
            this._changeSetApplicationService = changeSetApplicationService;
            this._visitApplicationService = visitApplicationService;
            this._eob835Service = eob835Service;
            this._remittanceCodeRepository = remittanceCodeRepository;
            this._remittanceRemarkCodeRepository = remittanceRemarkCodeRepository;
            this._eobPayerFilterRepository = eobPayerFilterRepository;
            this._visitEobApplicationService = visitEobApplicationService;
        }

        [HttpGet]
        public async Task<ActionResult> GetVisitEboDetails(int billingSystemId, string visitSourceSystemKey)
        {
            try
            {
                IList<VisitEobDetailsResultDto> visitEobDetails = await Task.FromResult(this._visitEobApplicationService.Value.GetVisitEobDetails(billingSystemId, visitSourceSystemKey));
                return this.Content(Newtonsoft.Json.JsonConvert.SerializeObject(visitEobDetails));
            }
            catch (Exception e)
            {
                return this.Content(e.Message);
            }
        }

        [HttpGet]
        public ActionResult Add835(int id)
        {
            HsVisitDto hsVisitDto = this._changeSetApplicationService.Value.GetVisit(id);

            Add835ViewModel model = new Add835ViewModel
            {
                VisitId = hsVisitDto.VisitId,
                ClaimAdjustmentReasonCodes = this._remittanceCodeRepository.Value.GetQueryable().Where(x => x.EndDate == null).ToList().Select(x => new SelectListItem { Text = $"{x.ClaimAdjustmentReasonCode} - {new string(x.Description.Take(35).ToArray())}", Value = x.ClaimAdjustmentReasonCode }).ToList(),
                EobRemittanceAdviceRemarkCodes = this._remittanceRemarkCodeRepository.Value.GetQueryable().Where(x => x.EndDate == null).ToList().Select(x => new SelectListItem { Text = $"{x.RemittanceAdviceRemarkCode} - {new string(x.Description.Take(35).ToArray())}", Value = x.RemittanceAdviceRemarkCode }).ToList(),
                PayerFilters = this._eobPayerFilterRepository.Value.GetQueryable().Where(x => x.IsActive).ToList().Select(x => new SelectListItem { Text = $"{x.UniquePayerValue}", Value = x.EobPayerFilterId.ToString() }).ToList(),
            };

            return this.View(model);
        }

        [HttpPost]
        public JsonResult Add835(Add835ViewModel model)
        {
            HsVisitDto hsVisitDto = this._changeSetApplicationService.Value.GetVisit(model.VisitId);

            InterchangeControlHeaderTrailer healthCareRemit835 = new InterchangeControlHeaderTrailer
            {
                FunctionalGroupHeaderTrailerHeaders = new List<FunctionalGroupHeaderTrailer>(),
                FileTrackerId = null
            };
            SetInterchangeControlHeaderTrailerValues(healthCareRemit835);

            foreach (FunctionalGroupHeaderHeaderTrailerViewModel functionalGroupHeaderTrailerViewModel in model.FunctionalGroupHeaderTrailerHeaders)
            {
                FunctionalGroupHeaderTrailer functionalGroupHeaderTrailer = new FunctionalGroupHeaderTrailer
                {
                    TransactionHeaders = new List<TransactionSetHeaderTrailer835>()
                };
                SetFunctionalGroupHeaderTrailerValues(functionalGroupHeaderTrailer);

                foreach (TransactionSetHeaderTrailer835ViewModel transactionSetHeaderViewModel in functionalGroupHeaderTrailerViewModel.TransactionHeaders)
                {
                    Domain.HospitalData.Eob.Entities.EobPayerFilter selectHealthPayerFilter = this._eobPayerFilterRepository.Value.GetById(transactionSetHeaderViewModel.PayerFilterId);
                    PayerIdentification payerDeatils = new PayerIdentification
                    {
                        N101EntityIdentifierCode = "PR",
                        N102PayerName = selectHealthPayerFilter.UniquePayerValue,
                    };

                    TransactionSetHeaderTrailer835 transactionSetHeader = new TransactionSetHeaderTrailer835
                    {
                        Bpr16Date = transactionSetHeaderViewModel.DtmProductionDate.GetValueOrDefault(DateTime.UtcNow),
                        DtmProductionDate = new DateTimeReference { Dtm01DateTimeQualifier = "405", Dtm02Date = transactionSetHeaderViewModel.DtmProductionDate.GetValueOrDefault(DateTime.UtcNow) },
                        Transactions = new List<HeaderNumber>(),
                        EobPayerFilter = selectHealthPayerFilter,
                        PayerDetails = payerDeatils,
                        St02TransactionSetControlNumber = "1"
                    };

                    int lx01Count = 0;
                    foreach (HeaderNumberViewModel headerNumberViewModel in transactionSetHeaderViewModel.Transactions)
                    {
                        lx01Count++;
                        HeaderNumber headerNumber = new HeaderNumber
                        {
                            Lx01AssignedNumber = lx01Count,
                            ClaimPaymentInformations = new List<ClaimPaymentInformation>()
                        };

                        foreach (ClaimPaymentInformationViewModel claimPaymentInformationViewModel in headerNumberViewModel.ClaimPaymentInformations)
                        {
                            ClaimPaymentInformation claimPaymentInformation = new ClaimPaymentInformation
                            {
                                VisitSourceSystemKey = hsVisitDto.SourceSystemKey,
                                BillingSystemId = hsVisitDto.BillingSystemId,
                                Nm11PatientName = new IndividualOrOrganizationalName
                                {
                                    Nm103NameLastOrganizationName = hsVisitDto.PatientLastName,
                                    Nm104NameFirst = hsVisitDto.PatientFirstName,
                                    Nm109IdentificationCode = claimPaymentInformationViewModel.Nm109IdentificationCode,
                                },
                                Clp01ClaimSubmittersIdentifier = claimPaymentInformationViewModel.Clp01ClaimSubmittersIdentifier, // store the hospital claim number in CLP01 
                                Clp07ReferenceIdentifier = claimPaymentInformationViewModel.Clp07ReferenceIdentifier,  // store the insurance claim number in CLP07
                                Clp02ClaimStatusCode = claimPaymentInformationViewModel.Clp02ClaimStatusCode,
                                Clp03MonetaryAmount = claimPaymentInformationViewModel.Clp03MonetaryAmount.GetValueOrDefault(0),
                                Clp04MonetaryAmount = claimPaymentInformationViewModel.Clp04MonetaryAmount.GetValueOrDefault(0),
                                Clp05MonetaryAmount = claimPaymentInformationViewModel.Clp05MonetaryAmount.GetValueOrDefault(0),
                                ClaimAdjustmentPivots = new List<ClaimAdjustmentPivot>(),
                                Dtm1StatementFromOrToDates = new List<DateTimeReference>(),
                                ServicePaymentInformations = new List<ServicePaymentInformation>()
                            };

                            string adjudicationType = claimPaymentInformationViewModel.AdjudicationInformation?.AdjudicationInformationType ?? string.Empty;
                            if (string.Equals(adjudicationType, "Inpatient", StringComparison.CurrentCultureIgnoreCase))
                            {
                                claimPaymentInformation.MiaInpatientInformation = new InpatientAdjudicationInformation { Mia05ReferenceIdentifier = claimPaymentInformationViewModel.AdjudicationInformation?.MiaMoaRemarkCode };
                            }
                            else if (string.Equals(adjudicationType, "Outpatient", StringComparison.CurrentCultureIgnoreCase))
                            {
                                claimPaymentInformation.MoaOutpatientInformation = new OutpatientAdjudicationInformation { Moa03ReferenceIdentifier = claimPaymentInformationViewModel.AdjudicationInformation?.MiaMoaRemarkCode };
                            }

                            AddClaimAdjustmentPivots(claimPaymentInformationViewModel.ClaimAdjustmentPivots, claimPaymentInformation.ClaimAdjustmentPivots);
                            AddServicePaymentInformations(claimPaymentInformationViewModel.ServicePaymentInformations, claimPaymentInformation.ServicePaymentInformations);

                            headerNumber.ClaimPaymentInformations.Add(claimPaymentInformation);
                        }

                        transactionSetHeader.Transactions.Add(headerNumber);
                    }

                    functionalGroupHeaderTrailer.TransactionHeaders.Add(transactionSetHeader);
                }

                healthCareRemit835.FunctionalGroupHeaderTrailerHeaders.Add(functionalGroupHeaderTrailer);
            }

            List<ClaimPaymentInformationHeader> cpiList = this._eob835Service.Value.ExtractClaimPaymentInformation(healthCareRemit835);
            foreach (ClaimPaymentInformationHeader claimPaymentInformationHeader in cpiList)
            {
                //Assign natural key to the header from the serialized ClaimPaymentInformation child
                claimPaymentInformationHeader.BillingSystemId = claimPaymentInformationHeader.ClaimPaymentInformation.BillingSystemId;
                claimPaymentInformationHeader.VisitSourceSystemKey = claimPaymentInformationHeader.ClaimPaymentInformation.VisitSourceSystemKey;
            }
            this._eob835Service.Value.SendClaimPaymentInformationNotifications(cpiList);

            return this.Json(new { Url = this.Url.Action("ViewVisit", "ETL", new { id = hsVisitDto.VisitId }) });
        }

        private void AddClaimAdjustmentPivots(IList<ClaimAdjustmentPivotViewModel> source, IList<ClaimAdjustmentPivot> target)
        {
            if (source == null || !source.Any())
            {
                return;
            }

            foreach (ClaimAdjustmentPivotViewModel model in source)
            {
                target.Add(new ClaimAdjustmentPivot
                {
                    ClaimAdjustmentGroupCode = model.ClaimAdjustmentGroupCode,
                    ClaimAdjustmentReasonCode = model.ClaimAdjustmentReasonCode,
                    MonetaryAmount = model.MonetaryAmount.GetValueOrDefault(0),
                    EobClaimAdjustmentReasonCode = this._remittanceCodeRepository.Value.GetEobClaimAdjustmentReasonCode(model.ClaimAdjustmentReasonCode)
                });
            }
        }
        
        private void AddServicePaymentInformations(IList<ServicePaymentInformationViewModel> source, IList<ServicePaymentInformation> target)
        {
            if (source == null || !source.Any())
            {
                return;
            }

            foreach (ServicePaymentInformationViewModel model in source)
            {
                ServicePaymentInformation servicePaymentInformation = new ServicePaymentInformation
                {
                    LqHealthCareRemarkCodes = model.LqHealthCareRemarkCodes.Where(x => !string.IsNullOrEmpty(x.Lq02IndustryCode)).Select(x => new IndustryCodeIdentification
                    {
                        Lq01CodeListQualifierCode = "",
                        Lq02IndustryCode = x.Lq02IndustryCode
                    }).ToList(),
                    ClaimAdjustmentPivots = new List<ClaimAdjustmentPivot>(),
                    DtmServiceDates = new List<DateTimeReference>(),
                };

                AddClaimAdjustmentPivots(model.ClaimAdjustmentPivots, servicePaymentInformation.ClaimAdjustmentPivots);

                target.Add(servicePaymentInformation);
            }
        }

        private void SetFunctionalGroupHeaderTrailerValues(FunctionalGroupHeaderTrailer target)
        {
            // hardcoded from values copied from from running the test file
            target.Gs01FunctionalIdentifierCode = "HP";
            target.Gs02ApplicationSenderCode = "HT000015-001";
            target.Gs03ApplicationReceiverCode = "HT007214-001";
            target.Gs0405DateTime = new DateTime(2016, 08, 01, 05, 18, 00);
            target.Gs06GroupControlNumber = 1;
            target.Gs07AgencyCode = "X";
            target.Gs08Version = "005010X221A1";
            target.Ge01TransactionsCount = 1;
            target.Ge02GroupTrailerControlNumber = 1;
        }

        private void SetInterchangeControlHeaderTrailerValues(InterchangeControlHeaderTrailer target)
        {
            // hardcoded from values copied from from running the test file
            target.Isa01AuthorizationInformationQualifier = 0;
            target.Isa02AuthorizationInformation = string.Empty;
            target.Isa03SecurityInformationQualifier = "00";
            target.Isa04SecurityInformation = string.Empty;
            target.Isa05IdQualifier = "ZZ";
            target.Isa06SenderId = "HT000015-001";
            target.Isa07IdQualifier = "ZZ";
            target.Isa08ReceiverId = "HT007214-001";
            target.Isa0910DateTime = new DateTime(2017, 04, 17, 05, 18, 00);
            target.Isa11ControlStandardsId = "^";
            target.Isa12ControlVersion = 501;
            target.Isa13ControlNumber = 6487;
            target.Isa14AcknowledgementRequested = false;
            target.Isa15UsageIndicator = "P";
            target.Isa16ComponentElementSeparator = ':';
            target.Iea01GroupsCount = 1;
            target.Iea02TrailerControlNumber = 6487;
        }
    }
}
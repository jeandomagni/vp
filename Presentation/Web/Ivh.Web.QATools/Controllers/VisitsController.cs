﻿namespace Ivh.Web.QATools.Controllers
{
    using System;
    using System.Collections.Generic;
    using Application.Core.Common.Interfaces;
    using System.Linq;
    using System.Web.Mvc;
    using Common.Web.Interfaces;
    using Domain.Qat.Interfaces;
    using Domain.Visit.Entities;
    using Ivh.Web.QATools.Interfaces;
    using Ivh.Web.QATools.Models;
    using Ivh.Domain.Visit.Interfaces;

    public class VisitsController : BaseController
    {
        private readonly IQAVisitRepository _repository;
        private readonly IVisitTransactionRepository _transactionRepository;
        private readonly IVisitApplicationService _visitApplicationService;
        private readonly IVisitService _visitService;
        private readonly IQaVisitTransactionService _qaVisitTransactionService;

        public VisitsController(
            Lazy<IBaseControllerService> baseControllerService,
            IVisitApplicationService visitApplicationService,
            IVisitService visitService,
            IQAVisitRepository repository,
            IVisitTransactionRepository transactionRepository,
            IQaVisitTransactionService qaVisitTransactionService
            )
            : base(baseControllerService)
        {
            this._repository = repository;
            this._visitApplicationService = visitApplicationService;
            this._transactionRepository = transactionRepository;
            this._visitService = visitService;
            this._qaVisitTransactionService = qaVisitTransactionService;
        }

        [HttpGet]
        public ActionResult Index()
        {
            return View();
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult CreateVisitView(string userName, int guarantorId)
        {
            return View("~/Views/Guarantors/Visits/CreateVisit.cshtml", new VisitViewModel { UserName = userName, VpGuarantorId = guarantorId });
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult CreateVisitTransactionView(string userName, int guarantorId)
        {
            return View("~/Views/Guarantors/VisitTransactions/CreateVisitTransaction.cshtml", new VisitTransactionViewModel { UserName = userName, VpGuarantorId = guarantorId });
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult GetVisitsByGuarantor(int guarantorId)
        {
            IReadOnlyList<Visit> data = this._visitService.GetVisits(guarantorId);

            var entity =
                data.
                Select(e => new
                {
                    e.VisitId,
                    e.VisitDescription
                }).ToList();

            return Json(entity, JsonRequestBehavior.AllowGet);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public void CreateVisit(VisitViewModel viewModel)
        {
            this._repository.CreateVisit(viewModel);
        }


        [AcceptVerbs(HttpVerbs.Post)]
        public void CreateVisitTransaction(VisitTransactionViewModel viewModel)
        {
            this._repository.CreateVisitTransaction(viewModel);
        }


        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult GetTransactionTypes()
        {
            var data = this._transactionRepository.GetTransactionTypes();
            return Json(data, JsonRequestBehavior.AllowGet);
        }


        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult GetVisitStates()
        {
            var data = this._visitApplicationService.GetVisitStates();
            return Json(data, JsonRequestBehavior.AllowGet);
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public void OffsetVisitTransactionDates(string systemSourceKey, int billingSystemId, int numberOfDays)
        {
            this._qaVisitTransactionService.AdjustVisitBalanceDates(systemSourceKey, billingSystemId, numberOfDays);
        }

    }
}
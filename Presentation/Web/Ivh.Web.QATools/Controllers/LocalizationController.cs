﻿
namespace Ivh.Web.QATools.Controllers
{
    using System;
    using System.Collections.Concurrent;
    using System.Collections.Generic;
    using System.Linq;
    using System.Net.Http;
    using System.Reflection;
    using System.Threading;
    using System.Threading.Tasks;
    using System.Web;
    using System.Web.Mvc;
    using Ivh.Application.Core.Common.Dtos;
    using Ivh.Application.Settings.Common.Dtos;
    using Ivh.Common.Api.Client;
    using Ivh.Common.Base.Utilities.Extensions;
    using Ivh.Common.Base.Utilities.Helpers;
    using Ivh.Common.VisitPay.Constants;
    using Ivh.Common.VisitPay.Enums;
    using Ivh.Common.Web.Interfaces;
    using Ivh.Domain.Settings.Enums;
    using Ivh.Domain.Settings.Interfaces;
    using Ivh.Web.QATools.Models;
    using Newtonsoft.Json;

    [Authorize]
    public class LocalizationController : BaseController
    {
        private static string _applicationName;

        private static string _contentApiUrl;
        private static string _contentApiAppId;
        private static string _contentApiAppKey;

        private const int ServiceCallMaxIterations = 20;
        private const int ServiceCallIntervalWait = 100;

#if DEBUG
        private const double ApiTimeout = 300d;
#else
        private const double ApiTimeout = 15d;
#endif

        private static readonly object Lock = new object();


        public LocalizationController(
            Lazy<IBaseControllerService> baseControllerService, 
            Lazy<IApplicationSettingsService> applicationSettingService) 
            : base(baseControllerService)
        {
            if (_contentApiUrl.IsNullOrEmpty()
                || _contentApiAppId.IsNullOrEmpty()
                || _contentApiAppKey.IsNullOrEmpty())
            {
                lock (Lock)
                {
                    _contentApiUrl = string.Concat(applicationSettingService.Value.GetUrl(UrlEnum.ClientDataApi).AbsoluteUri, "api/content/");
                    _contentApiAppId = applicationSettingService.Value.ClientDataApiAppId.Value;
                    _contentApiAppKey = applicationSettingService.Value.ClientDataApiAppKey.Value;
                }
            }

            if (_applicationName == null)
            {
                lock (Lock)
                {
                    _applicationName = Ivh.Common.Properties.Settings.Default.ApplicationName;
                }
            }
        }

        // GET: Localization
        public ActionResult Index(ApplicationEnum application = ApplicationEnum.VisitPay)
        {
            IList<FieldInfo> allTextRegions = typeof(TextRegionConstants).GetAllPublicConstantFieldsOfType<string>();

            IList<TextRegionInfo> textRegions;
            List<ClientCultureInfoDto> supportedLanguages;
            if (application == ApplicationEnum.GuestPay)
            {
                textRegions = allTextRegions
                                .Where(tr => tr.Name.StartsWith("GuestPay"))
                                .Select(tr => new TextRegionInfo()
                                {
                                    TextRegionPropertyName = tr.Name,
                                    TextRegionKey = (string)tr.GetRawConstantValue()
                                })
                                .ToList();

                supportedLanguages = ClientDto.LanguageGuestPaySupported;
            }
            else
            {
                textRegions = allTextRegions
                                .Where(tr => !tr.Name.StartsWith("GuestPay"))
                                .Select(tr => new TextRegionInfo()
                                {
                                    TextRegionPropertyName = tr.Name,
                                    TextRegionKey = (string)tr.GetRawConstantValue()
                                })
                                .ToList();

                supportedLanguages = ClientDto.LanguageAppSupported;
            }

            foreach (ClientCultureInfoDto locale in supportedLanguages)
            {
                Dictionary<int, string> dictionary = this.GetTextRegionDictionary(locale.Name, application).Result;

                //Check all of the text regions 
                foreach (TextRegionInfo textRegion in textRegions)
                {
                    TextRegionLocaleSupport supportInfo = new TextRegionLocaleSupport()
                    {
                        LocaleName = locale.Name,
                        IsSupported = (dictionary != null ? dictionary.ContainsKey(textRegion.TextRegionKey.GetConsistentHashCode()) : false)
                    };
                    textRegion.LocaleSupport.Add(supportInfo);
                }
            }

            //Sort
            textRegions = textRegions
                            .OrderBy(tr => tr.TextRegionPropertyName)
                            .ThenBy(tr => tr.TextRegionKey)
                            .ToList();

            ViewBag.Application = application;

            return View(textRegions);
        }

        private async Task<Dictionary<int, string>> GetTextRegionDictionary(string locale, ApplicationEnum application)
        {
            //Get value from the API
            string parameters = $"locale={UrlEncode(locale)}&application={application.ToString()}";
            Dictionary<int, string> apiResponse = await this.MakeApiCallAsync<Dictionary<int, string>>("GetAllTextRegions", parameters).ConfigureAwait(false);
            return apiResponse;
        }

        private Task<TReturn> MakeApiCallAsync<TReturn>(string action, string parameters)
        {
            string request = string.Concat(_contentApiUrl, action, "?", parameters);

            Exception ex = null;
            for (int i = 0; i < ServiceCallMaxIterations; i++)
            {
                HttpClient client = HttpClientFactory.Create(new ApiHandler(_contentApiAppId, _contentApiAppKey, _applicationName));
                client.Timeout = TimeSpan.FromSeconds(ApiTimeout);
                try
                {
                    HttpResponseMessage response = Task.Run(async () => await client.GetAsync(request)).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        string responseString = Task.Run(async () => await response.Content.ReadAsStringAsync()).Result;
                        TReturn returnValue = JsonConvert.DeserializeObject<TReturn>(responseString);

                        return Task.FromResult(returnValue);
                    }
                }
                catch (Exception ex1)
                {
                    if (i < ServiceCallMaxIterations - 1)
                    {
                        Thread.Sleep(ServiceCallIntervalWait);
                        continue;
                    }

                    ex = ex1;
                    break;
                }
            }

            string exceptionMessage1 = $"Failed to call the Content API. Request: {request}, Exception: {ExceptionHelper.AggregateExceptionToString(ex)}";
            throw new ApplicationException(exceptionMessage1);
        }

        private static readonly ConcurrentDictionary<string, string> UrlEncodedValues = new ConcurrentDictionary<string, string>();
        private static string UrlEncode(string input)
        {
            if (UrlEncodedValues.TryGetValue(input, out string encoded))
            {
                return encoded;
            }

            string encodedValue = HttpUtility.UrlEncode(input);
            UrlEncodedValues[input] = encodedValue;

            return encodedValue;
        }
    }
}
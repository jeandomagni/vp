﻿﻿namespace Ivh.Web.QATools.Controllers
{
    using System;
    using System.Threading.Tasks;
    using System.Web.Mvc;
    using System.Web.SessionState;
    using Common.Base.Utilities.Extensions;
    using Common.Web.Constants;
    using Models;
    using Provider.Enrollment.Intermountain.Models;
    using Services;

    public class MockAuthenticationController : Controller
    {
        private FakeJwtService JwtService { get; set; }
        private readonly IntermountainGuarantorEnrollmentTestProvider _enrollmentProvider;

        public MockAuthenticationController(IntermountainGuarantorEnrollmentTestProvider enrollmentProvider)
        {
            this.JwtService = new FakeJwtService();
            this._enrollmentProvider = enrollmentProvider;
        }

        [HttpGet]
        public async Task<ActionResult> Index()
        {
            return await Task.Run(() => this.View());
        }
        
        [HttpGet]
        public ActionResult Authenticate()
        {
            MockAuthenticationAuthenticateResponse response = new MockAuthenticationAuthenticateResponse();

            string clientid = this.Request.ServerVariables.Get("HTTP_CLIENTID");
            string username = this.Request.ServerVariables.Get("HTTP_USERNAME");
            string password = this.Request.ServerVariables.Get("HTTP_PASSWORD");
            if (clientid.IsNullOrEmpty())
            {
                this.Response.StatusCode = 403;                
                return this.Json(new { }, JsonRequestBehavior.AllowGet);
            }
            if (username.IsNullOrEmpty())
            {
                this.Response.StatusCode = 403;
                return this.Json(new { }, JsonRequestBehavior.AllowGet);
            }
            if (password.IsNullOrEmpty())
            {
                this.Response.StatusCode = 403;
                return this.Json(new { }, JsonRequestBehavior.AllowGet);
            }

            if (clientid == "ivinciClient" && username == "ivinciUsername" && password == "NotSecurePassword")
            {
                response.clientid = clientid;
                response.token = this.JwtService.CreateJwt();
                return this.Json(response, JsonRequestBehavior.AllowGet);
            }
            switch (clientid)
            {
                case "301Moved":
                    this.Response.StatusCode = 301;
                    break;
                case "302Redirect":
                    this.Response.StatusCode = 302;
                    break;
                case "400BadRequest":
                    this.Response.StatusCode = 400;
                    break;
                case "401Unauthorized":
                    this.Response.StatusCode = 401;
                    this.Response.Headers.Set(HttpHeaders.Response.SuppressRedirect, "True");

                    break;
                case "403Forbidden":
                    this.Response.StatusCode = 403;
                    break;
                case "404NotFound":
                    this.Response.StatusCode = 404;
                    break;
                case "405MethodNotAllowed":
                    this.Response.StatusCode = 405;
                    break;
                case "406NotAcceptable":
                    this.Response.StatusCode = 406;
                    break;
                case "407ProxyAuthenticationRequired":
                    this.Response.StatusCode = 407;
                    break;
                case "408RequestTimeout":
                    this.Response.StatusCode = 408;
                    break;
                case "409Conflict":
                    this.Response.StatusCode = 409;
                    break;
                case "410Gone":
                    this.Response.StatusCode = 410;
                    break;
                case "413PayloadTooLarge":
                    this.Response.StatusCode = 413;
                    break;
                case "414URITooLong":
                    this.Response.StatusCode = 414;
                    break;
                case "420MethodFailure": // spring framework
                    this.Response.StatusCode = 420;
                    break;
                case "500ServerError":
                    this.Response.StatusCode = 500;
                    break;
                case "501NotImplemented":
                    this.Response.StatusCode = 501;
                    break;
                case "502BadGateway":
                    this.Response.StatusCode = 502;
                    break;
                case "503ServiceUnavailable":
                    this.Response.StatusCode = 503;
                    break;
                case "504GatewayTimeout":
                    this.Response.StatusCode = 504;
                    break;
                case "505HTTPVersionNotSupported":
                    this.Response.StatusCode = 505;
                    break;
                default:
                    this.Response.StatusCode = 403;
                    break;
            }
            return this.Json(new { }, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult ValidateToken(string token)
        {
            return this.Json(new { isValid = this.JwtService.ValidateJwt(token) }, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult TestAuthentication()
        {
            Task<AuthenticateResponseModel> t = Task.Run(async () => await this._enrollmentProvider.TestGetJwtTokenAsync("301Moved"));
            t.Wait();
            t = Task.Run(async () => await this._enrollmentProvider.TestGetJwtTokenAsync("302Redirect"));
            t.Wait();
            t = Task.Run(async () => await this._enrollmentProvider.TestGetJwtTokenAsync("400BadRequest"));
            t.Wait();
            t = Task.Run(async () => await this._enrollmentProvider.TestGetJwtTokenAsync("401Unauthorized"));
            t.Wait();
            t = Task.Run(async () => await this._enrollmentProvider.TestGetJwtTokenAsync("404NotFound"));
            t.Wait();
            t = Task.Run(async () => await this._enrollmentProvider.TestGetJwtTokenAsync("405MethodNotAllowed"));
            t.Wait();
            t = Task.Run(async () => await this._enrollmentProvider.TestGetJwtTokenAsync("406NotAcceptable"));
            t.Wait();
            t = Task.Run(async () => await this._enrollmentProvider.TestGetJwtTokenAsync("407ProxyAuthenticationRequired"));
            t.Wait();
            t = Task.Run(async () => await this._enrollmentProvider.TestGetJwtTokenAsync("408RequestTimeout"));
            t.Wait();
            t = Task.Run(async () => await this._enrollmentProvider.TestGetJwtTokenAsync("409Conflict"));
            t.Wait();
            t = Task.Run(async () => await this._enrollmentProvider.TestGetJwtTokenAsync("410Gone"));
            t.Wait();
            t = Task.Run(async () => await this._enrollmentProvider.TestGetJwtTokenAsync("413PayloadTooLarge"));
            t.Wait();
            t = Task.Run(async () => await this._enrollmentProvider.TestGetJwtTokenAsync("414URITooLong"));
            t.Wait();
            t = Task.Run(async () => await this._enrollmentProvider.TestGetJwtTokenAsync("420MethodFailure"));
            t.Wait();
            t = Task.Run(async () => await this._enrollmentProvider.TestGetJwtTokenAsync("500ServerError"));
            t.Wait();
            t = Task.Run(async () => await this._enrollmentProvider.TestGetJwtTokenAsync("501NotImplemented"));
            t.Wait();
            t = Task.Run(async () => await this._enrollmentProvider.TestGetJwtTokenAsync("502BadGateway"));
            t.Wait();
            t = Task.Run(async () => await this._enrollmentProvider.TestGetJwtTokenAsync("503ServiceUnavailable"));
            t.Wait();
            t = Task.Run(async () => await this._enrollmentProvider.TestGetJwtTokenAsync("504GatewayTimeout"));
            t.Wait();
            t = Task.Run(async () => await this._enrollmentProvider.TestGetJwtTokenAsync("505HTTPVersionNotSupported"));
            t.Wait();
            if (t.Exception != null) Console.WriteLine(t.Exception.Message);
            return this.Json(new {isValid = true}, JsonRequestBehavior.AllowGet);
        }
    }
}
﻿namespace Ivh.Web.QATools.Controllers
{
    using Common.Web.Interfaces;
    using System;
    using System.Collections.Generic;
    using System.Web;
    using System.Web.Mvc;
    using Ivh.Application.FileStorage.Common.Dtos;
    using System.ComponentModel.DataAnnotations;
    using Ivh.Application.FileStorage.Common.Interfaces;
    using System.Threading.Tasks;
    using Ivh.Application.FileStorage.Common.Enums;
    
    public class FileStorageController : BaseController
    {

        private readonly Lazy<IFileStorageApplicationService> _fileStorageApplicationService;

        public FileStorageController(
            Lazy<IBaseControllerService> baseControllerService,
            Lazy<IFileStorageApplicationService> fileStorageApplicationService
            )
            : base(baseControllerService)
        {
            this._fileStorageApplicationService = fileStorageApplicationService;
        }

        // GET: FileStorage
        public ActionResult Index()
        {
            return this.View();
        }

        // GET: FileStorage/Details/5
        public ActionResult Details(int id)
        {
            return this.View();
        }

        // GET: FileStorage/Create
        public ActionResult Create()
        {
            return this.View();
        }

        // POST: FileStorage/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create(FileStorageViewModel vm)
        {
            if (this.ModelState.IsValid)
            {
                byte[] uploadedFile = new byte[vm.FileContents.InputStream.Length];
                vm.FileContents.InputStream.Read(uploadedFile, 0, uploadedFile.Length);

                var dto = new FileStoredDto()
                {
                    FileContents = uploadedFile,
                    FileDateTime = DateTime.UtcNow,
                    FileMimeType = vm.FileContents.ContentType,
                    ExternalKey = Guid.NewGuid(),
                    Filename = vm.FileContents.FileName,
                };

                var result = await this._fileStorageApplicationService.Value.SaveFileToDatabaseAsync(dto).ConfigureAwait(true);
            }
            return this.RedirectToAction("Index");
        }


        public ActionResult CreatePackage()
        {
            var vm = new FileStorageViewModel()
            {
                Defects = new List<SelectListItem>()
            };

            return this.View(vm);
        }

        // POST: FileStorage/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CreatePackage(FileStorageViewModel vm)
        {
            //TODO: potentially make async for consistency but this is a low-volume activity
            this._fileStorageApplicationService.Value.CreatePackageFromFolder(vm.DefectId);
            return this.RedirectToAction("CreatePackage");
        }

        // POST: FileStorage/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> UploadPackage(FileStorageViewModel vm)
        {
            if (this.ModelState.IsValid)
            {
                byte[] uploadedFile = new byte[vm.FileContents.InputStream.Length];
                vm.FileContents.InputStream.Read(uploadedFile, 0, uploadedFile.Length);

                var dto = new FileStoredDto()
                {
                    FileContents = uploadedFile,
                    FileDateTime = DateTime.UtcNow,
                    FileMimeType = vm.FileContents.ContentType,
                    ExternalKey = Guid.NewGuid(),
                    Filename = vm.FileContents.FileName,
                };

                var result = await this._fileStorageApplicationService.Value.SaveFileToIncomingFolderAsync(dto).ConfigureAwait(true);
            }

            return this.RedirectToAction("UploadPackage");
        }

        // GET: FileStorage/Edit/5
        public ActionResult Edit(int id)
        {
            return this.View();
        }

        // POST: FileStorage/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return this.RedirectToAction("Index");
            }
            catch
            {
                return this.View();
            }
        }

        // GET: FileStorage/Delete/5
        public ActionResult Delete(int id)
        {
            return this.View();
        }

        // POST: FileStorage/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return this.RedirectToAction("Index");
            }
            catch
            {
                return this.View();
            }
        }

    }

    public class FileStorageViewModel
    {
        [Required]
        public HttpPostedFileBase FileContents { get; set; }

        [Display(Name = "Select Package Defect")]
        public DefectTypeEnum DefectId { get; set; }

        [Display(Name="Apply Package Defect")]
        public IList<SelectListItem> Defects { get; set; }
    }

}

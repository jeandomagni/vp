﻿namespace Ivh.Web.QATools.Controllers
{
    using System;
    using System.Linq;
    using System.Threading.Tasks;
    using System.Web;
    using System.Web.Mvc;
    using Application.Core.Common.Interfaces;
    using Application.User.Common.Dtos;
    using AutoMapper;
    using Common.VisitPay.Strings;
    using Common.Web.Interfaces;
    using Microsoft.AspNet.Identity;
    using Microsoft.Owin.Security;
    using Models;

    [Authorize(Roles = VisitPayRoleStrings.System.QATools + "," + VisitPayRoleStrings.Admin.Administrator)]
    public class ManageController : BaseController
    {
        private readonly IVisitPayUserApplicationService _visitPayUserApplicationService;

        public ManageController(
            Lazy<IBaseControllerService> baseControllerService, 
            IVisitPayUserApplicationService visitPayUserApplicationService)
            : base(baseControllerService)
        {
            this._visitPayUserApplicationService = visitPayUserApplicationService;
        }

        //
        // GET: /Manage/Index
        public async Task<ActionResult> Index(ManageMessageId? message)
        {
            this.ViewBag.StatusMessage =
                message == ManageMessageId.ChangePasswordSuccess
                    ? "Your password has been changed."
                    : message == ManageMessageId.SetPasswordSuccess
                        ? "Your password has been set."
                        : message == ManageMessageId.SetTwoFactorSuccess
                            ? "Your two-factor authentication provider has been set."
                            : message == ManageMessageId.Error
                                ? "An error has occurred."
                                : message == ManageMessageId.AddPhoneSuccess
                                    ? "Your phone number was added."
                                    : message == ManageMessageId.RemovePhoneSuccess
                                        ? "Your phone number was removed."
                                        : "";

            var userId = this.CurrentUserIdString;
            var model = new IndexViewModel
            {
                HasPassword = await this.HasPassword().ConfigureAwait(true),
                PhoneNumber = await this._visitPayUserApplicationService.GetPhoneNumberAsync(userId).ConfigureAwait(true),
                TwoFactor = await this._visitPayUserApplicationService.GetTwoFactorEnabledAsync(userId).ConfigureAwait(true),
                Logins = await this._visitPayUserApplicationService.GetLoginsAsync(userId).ConfigureAwait(true),
                BrowserRemembered = await this.AuthenticationManager.TwoFactorBrowserRememberedAsync(userId).ConfigureAwait(true)
            };
            return this.View(model);
        }

        //
        // POST: /Manage/RemoveLogin
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> RemoveLogin(string loginProvider, string providerKey)
        {
            ManageMessageId? message;
            var result =
                await this._visitPayUserApplicationService.RemoveLoginAsync(this.CurrentUserIdString,
                        new UserLoginInfo(loginProvider, providerKey)).ConfigureAwait(true);
            if (result.Succeeded)
            {
                await this._visitPayUserApplicationService.ReLoginAsync(this.CurrentUserIdString, Mapper.Map<HttpContextDto>(System.Web.HttpContext.Current)).ConfigureAwait(true);
                message = ManageMessageId.RemoveLoginSuccess;
            }
            else
            {
                message = ManageMessageId.Error;
            }
            return this.RedirectToAction("ManageLogins", new { Message = message });
        }

        //
        // GET: /Manage/AddPhoneNumber
        public ActionResult AddPhoneNumber()
        {
            return this.View();
        }

        //
        // POST: /Manage/AddPhoneNumber
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> AddPhoneNumber(AddPhoneNumberViewModel model)
        {
            if (!this.ModelState.IsValid)
            {
                return this.View(model);
            }

            // Generate the token and send it
            var code =
                await this._visitPayUserApplicationService.GenerateChangePhoneNumberTokenAsync(this.CurrentUserIdString,
                        model.Number).ConfigureAwait(true);
            await this._visitPayUserApplicationService.SendSmsMessageForCode(model.Number, code).ConfigureAwait(true);

            return this.RedirectToAction("VerifyPhoneNumber", new { PhoneNumber = model.Number });
        }

        //
        // GET: /Manage/VerifyPhoneNumber
        public async Task<ActionResult> VerifyPhoneNumber(string phoneNumber)
        {
            var code =
                await this._visitPayUserApplicationService.GenerateChangePhoneNumberTokenAsync(this.CurrentUserIdString,
                        phoneNumber).ConfigureAwait(true);
            // Send an SMS through the SMS provider to verify the phone number
            return phoneNumber == null
                ? this.View("Error")
                : this.View(new VerifyPhoneNumberViewModel { PhoneNumber = phoneNumber });
        }

        //
        // POST: /Manage/VerifyPhoneNumber
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> VerifyPhoneNumber(VerifyPhoneNumberViewModel model)
        {
            if (!this.ModelState.IsValid)
            {
                return this.View(model);
            }
            var result =
                await this._visitPayUserApplicationService.ChangePhoneNumberAsync(this.CurrentUserIdString,
                        model.PhoneNumber, model.Code).ConfigureAwait(true);
            if (result.Succeeded)
            {
                await this._visitPayUserApplicationService.ReLoginAsync(this.CurrentUserIdString, Mapper.Map<HttpContextDto>(System.Web.HttpContext.Current)).ConfigureAwait(true);
                return this.RedirectToAction("Index", new { Message = ManageMessageId.AddPhoneSuccess });
            }

            // If we got this far, something failed, redisplay form
            this.ModelState.AddModelError("", "Failed to verify phone");
            return this.View(model);
        }

        //
        // GET: /Manage/RemovePhoneNumber
        public async Task<ActionResult> RemovePhoneNumber()
        {
            var result = await this._visitPayUserApplicationService.SetPhoneNumberAsync(this.CurrentUserIdString, null).ConfigureAwait(true);
            if (!result.Succeeded)
            {
                return this.RedirectToAction("Index", new { Message = ManageMessageId.Error });
            }
            await this._visitPayUserApplicationService.ReLoginAsync(this.CurrentUserIdString, Mapper.Map<HttpContextDto>(System.Web.HttpContext.Current)).ConfigureAwait(true);
            return this.RedirectToAction("Index", new { Message = ManageMessageId.RemovePhoneSuccess });
        }

        //
        // GET: /Manage/ChangePassword
        public ActionResult ChangePassword()
        {
            return this.View();
        }

        //
        // POST: /Manage/ChangePassword
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ChangePassword(ChangePasswordViewModel model)
        {
            if (!this.ModelState.IsValid)
            {
                return this.View(model);
            }
            var result =
                await this._visitPayUserApplicationService.ChangePasswordAsync(this.CurrentUserIdString, model.OldPassword, model.NewPassword, 999, 10).ConfigureAwait(true);
            if (result.Succeeded)
            {
                await this._visitPayUserApplicationService.ReLoginAsync(this.CurrentUserIdString, Mapper.Map<HttpContextDto>(System.Web.HttpContext.Current)).ConfigureAwait(true);
                return this.RedirectToAction("Index", new { Message = ManageMessageId.ChangePasswordSuccess });
            }
            this.AddErrors(result);
            return this.View(model);
        }

        //
        // GET: /Manage/SetPassword
        public ActionResult SetPassword()
        {
            return this.View();
        }

        //
        // POST: /Manage/SetPassword
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> SetPassword(SetPasswordViewModel model)
        {
            if (this.ModelState.IsValid)
            {
                var result =
                    await this._visitPayUserApplicationService.AddPasswordAsync(this.CurrentUserIdString,
                            model.NewPassword).ConfigureAwait(true);
                if (result.Succeeded)
                {
                    await this._visitPayUserApplicationService.ReLoginAsync(this.CurrentUserIdString, Mapper.Map<HttpContextDto>(System.Web.HttpContext.Current)).ConfigureAwait(true);
                    return this.RedirectToAction("Index", new { Message = ManageMessageId.SetPasswordSuccess });
                }
                this.AddErrors(result);
            }

            // If we got this far, something failed, redisplay form
            return this.View(model);
        }

        //
        // GET: /Manage/ManageLogins
        public async Task<ActionResult> ManageLogins(ManageMessageId? message)
        {
            this.ViewBag.StatusMessage =
                message == ManageMessageId.RemoveLoginSuccess
                    ? "The external login was removed."
                    : message == ManageMessageId.Error
                        ? "An error has occurred."
                        : "";
            var user = await this._visitPayUserApplicationService.FindByIdAsync(this.CurrentUserIdString).ConfigureAwait(true);
            if (user == null)
            {
                return this.View("Error");
            }
            var userLogins = await this._visitPayUserApplicationService.GetLoginsAsync(this.CurrentUserIdString).ConfigureAwait(true);
            var otherLogins =
                this.AuthenticationManager.GetExternalAuthenticationTypes()
                    .Where(auth => userLogins.All(ul => auth.AuthenticationType != ul.LoginProvider))
                    .ToList();
            this.ViewBag.ShowRemoveButton = user.PasswordHash != null || userLogins.Count > 1;
            return this.View(new ManageLoginsViewModel
            {
                CurrentLogins = userLogins,
                OtherLogins = otherLogins
            });
        }

        //
        // POST: /Manage/LinkLogin
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult LinkLogin(string provider)
        {
            // Request a redirect to the external login provider to link a login for the current user
            return new AccountController.ChallengeResult(provider, this.Url.Action("LinkLoginCallback", "Manage"),
                this.CurrentUserIdString);
        }

        //
        // GET: /Manage/LinkLoginCallback
        public async Task<ActionResult> LinkLoginCallback()
        {
            var loginInfo =
                await this.AuthenticationManager.GetExternalLoginInfoAsync(XsrfKey, this.CurrentUserIdString).ConfigureAwait(true);
            if (loginInfo == null)
            {
                return this.RedirectToAction("ManageLogins", new { Message = ManageMessageId.Error });
            }
            var result =
                await this._visitPayUserApplicationService.AddLoginAsync(this.CurrentUserIdString, loginInfo.Login).ConfigureAwait(true);
            return result.Succeeded
                ? this.RedirectToAction("ManageLogins")
                : this.RedirectToAction("ManageLogins", new { Message = ManageMessageId.Error });
        }

        #region Helpers

        // Used for XSRF protection when adding external logins
        private const string XsrfKey = "XsrfId";

        private IAuthenticationManager AuthenticationManager
        {
            get { return this.HttpContext.GetOwinContext().Authentication; }
        }

        private void AddErrors(IdentityResult result)
        {
            foreach (var error in result.Errors)
            {
                this.ModelState.AddModelError("", error);
            }
        }

        private async Task<bool> HasPassword()
        {
            var user = await this._visitPayUserApplicationService.FindByIdAsync(this.CurrentUserIdString).ConfigureAwait(true);
            if (user != null)
            {
                return user.PasswordHash != null;
            }
            return false;
        }

        public enum ManageMessageId
        {
            AddPhoneSuccess,
            ChangePasswordSuccess,
            SetTwoFactorSuccess,
            SetPasswordSuccess,
            RemoveLoginSuccess,
            RemovePhoneSuccess,
            Error
        }

        #endregion
    }
}
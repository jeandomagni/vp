﻿namespace Ivh.Web.QATools.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using System.Web.Mvc;
    using Application.Core.Common.Interfaces;
    using Application.FileStorage.Common.Interfaces;
    using Application.FinanceManagement.Common.Interfaces;
    using Application.HospitalData.Common.Interfaces;
    using Application.Qat.Common.Dtos;
    using Application.Qat.Common.Interfaces;
    using Attributes;
    using AutoMapper;
    using Common.Base.Utilities.Extensions;
    using Common.Cache;
    using Common.VisitPay.Enums;
    using Common.VisitPay.Messages.HospitalData.Dtos;
    using Common.Web.Interfaces;
    using Domain.FinanceManagement.Payment.Entities;
    using Domain.HospitalData.Scoring.Interfaces;
    using Ivh.Application.Matching.Common.Interfaces;
    using Ivh.Domain.HospitalData.Eob.Interfaces;
    using Models;
    using Models.Builders;
    using Newtonsoft.Json;
    using Services;
    using VisitTransactionViewModel = Models.Etl.VisitTransactionViewModel;

    [AutomationAuthorize]
    public class JobsController : BaseController
    {
        private readonly Lazy<IChangeEventApplicationService> _changeEventApplicationService;
        private readonly Lazy<IStatementCreationApplicationService> _statementCreationApplicationService;
        private readonly Lazy<IScheduledPaymentApplicationService> _scheduledPaymentApplicationService;
        private readonly Lazy<IPastDueUncollectableApplicationService> _pastDueUncollectableApplicationService;
        private readonly Lazy<IAchApplicationService> _achApplicationService;
        private readonly Lazy<IPaymentMethodsApplicationService> _paymentMethodsApplicationService;
        private readonly Lazy<IFinancialDataSummaryApplicationService> _financialDataSummaryApplicationService;
        private readonly Lazy<IPaymentBatchOperationsApplicationService> _paymentBatchOperationsApplicationService;
        private readonly Lazy<IConsolidationGuarantorApplicationService> _consolidationGuarantorApplicationService;
        private readonly Lazy<ISystemExceptionApplicationService> _systemExceptionApplicationService;
        private readonly Lazy<ISendSampleEmailsService> _sendSampleEmailsService;
        private readonly Lazy<ISendSamplePaperMailService> _sendSamplePaperMailService;
        private readonly Lazy<IFileStorageApplicationService> _fileStorageApplicationService;
        private readonly Lazy<IDistributedCache> _cache;
        private readonly Lazy<IVisitPayUserApplicationService> _visitPayUserApplicationService;
        private readonly Lazy<IStatementExportApplicationService> _statementExportApplicationService;
        private readonly Lazy<IQaPaymentBatchApplicationService> _qaPaymentBatchApplicationService;
        private readonly Lazy<Application.HospitalData.Common.Interfaces.IChangeSetApplicationService> _changeSetApplicationService;
        private readonly Lazy<IScoringGuarantorMatchingService> _scoringGuarantorMatchingService;
        private readonly Lazy<IEob835Service> _load835Service;
        private readonly Lazy<IMatchingApplicationService> _matchingApplicationService;
        private readonly Lazy<IHsDataInboundApplicationService> _hospitalInboundApplicationService;

        public JobsController(
            Lazy<IBaseControllerService> baseControllerService,
            Lazy<IChangeEventApplicationService> changeEventApplicationService,
            Lazy<IStatementCreationApplicationService> statementCreationApplicationService,
            Lazy<IScheduledPaymentApplicationService> scheduledPaymentApplicationService,
            Lazy<IPastDueUncollectableApplicationService> pastDueUncollectableApplicationService,
            Lazy<IAchApplicationService> achApplicationService,
            Lazy<IPaymentMethodsApplicationService> paymentMethodsApplicationService,
            Lazy<IFinancialDataSummaryApplicationService> financialDataSummaryApplicationService,
            Lazy<IPaymentBatchOperationsApplicationService> paymentBatchOperationsApplicationService,
            Lazy<IConsolidationGuarantorApplicationService> consolidationGuarantorApplicationService,
            Lazy<ISystemExceptionApplicationService> systemExceptionApplicationService,
            Lazy<ISendSampleEmailsService> sendSampleEmailsService,
            Lazy<ISendSamplePaperMailService> sendSamplePaperMailService,
            Lazy<IFileStorageApplicationService> fileStorageApplicationService,
            Lazy<IDistributedCache> cache,
            Lazy<IVisitPayUserApplicationService> visitPayUserApplicationService,
            Lazy<IStatementExportApplicationService> statementExportApplicationService,
            Lazy<IQaPaymentBatchApplicationService> qaPaymentBatchApplicationService,
            Lazy<Application.HospitalData.Common.Interfaces.IChangeSetApplicationService> changeSetApplicationService,
            Lazy<IScoringGuarantorMatchingService> scoringGuarantorMatchingService,
            Lazy<IEob835Service> load835Service,
            Lazy<IMatchingApplicationService> matchingApplicationService,
            Lazy<IHsDataInboundApplicationService> hospitalInboundApplicationService
            )
            : base(baseControllerService)
        {
            this._changeEventApplicationService = changeEventApplicationService;
            this._statementCreationApplicationService = statementCreationApplicationService;
            this._scheduledPaymentApplicationService = scheduledPaymentApplicationService;
            this._pastDueUncollectableApplicationService = pastDueUncollectableApplicationService;
            this._achApplicationService = achApplicationService;
            this._paymentMethodsApplicationService = paymentMethodsApplicationService;
            this._financialDataSummaryApplicationService = financialDataSummaryApplicationService;
            this._paymentBatchOperationsApplicationService = paymentBatchOperationsApplicationService;
            this._consolidationGuarantorApplicationService = consolidationGuarantorApplicationService;
            this._systemExceptionApplicationService = systemExceptionApplicationService;
            this._sendSampleEmailsService = sendSampleEmailsService;
            this._sendSamplePaperMailService = sendSamplePaperMailService;
            this._fileStorageApplicationService = fileStorageApplicationService;
            this._cache = cache;
            this._visitPayUserApplicationService = visitPayUserApplicationService;
            this._statementExportApplicationService = statementExportApplicationService;
            this._qaPaymentBatchApplicationService = qaPaymentBatchApplicationService;
            this._changeSetApplicationService = changeSetApplicationService;
            this._scoringGuarantorMatchingService = scoringGuarantorMatchingService;
            this._load835Service = load835Service;
            this._matchingApplicationService = matchingApplicationService;
            this._hospitalInboundApplicationService = hospitalInboundApplicationService;
        }

        public ActionResult VpInternalBatchProcesses()
        {
            this.ViewBag.FinalPastDueNotificationDaysBeforeDueDate = base.ClientDto.FinalPastDueNotificationDaysBeforeDueDate - 1;
            this.ViewBag.UncollectableMaxAgingCountFinancePlans = base.ClientDto.UncollectableMaxAgingCountFinancePlans - 1;

            return this.View();
        }

        public ActionResult HospitalDataSync()
        {
            return View();
        }

        public ActionResult RunStatementProcessing(string date)
        {
            DateTime convertedDate = Convert.ToDateTime(date);

            this._statementCreationApplicationService.Value.QueueAllPendingStatementGuarantors(convertedDate.Date);
            return this.Content("Done!");
        }

        public ActionResult RunProcessAch(string date)
        {
            DateTime dateTime = Convert.ToDateTime(date).Date;
            this._achApplicationService.Value.PublishAchReturnsAndSettlements(dateTime, dateTime.AddDays(1));
            return this.Content("Done!");
        }

        public ActionResult RunPaymentProcessing(string date)
        {
            DateTime convertedDate = Convert.ToDateTime(date);

            this._scheduledPaymentApplicationService.Value.QueueScheduledPaymentsForGuarantorsChargedByDate(convertedDate.Date);
            return this.Content("Done!");
        }

        public ActionResult RunFinalPastDueAndUncollectablesNotificationsJob(string date)
        {
            this._pastDueUncollectableApplicationService.Value.QueueFinalPastDueAndActiveUncollectablesNotifications(Convert.ToDateTime(date).Date.AddSeconds(59).AddMinutes(59).AddHours(23));
            return this.Content("Done!");
        }

        public ActionResult RunClosedUncollectableNotificationsJob(string date)
        {
            this._pastDueUncollectableApplicationService.Value.QueueClosedUncollectableNotifications(Convert.ToDateTime(date).Date.AddSeconds(59).AddMinutes(59).AddHours(23));
            return this.Content("Done!");
        }

        public ActionResult SyncIncomingHsVisitsAndTransactions()
        {
            this._changeEventApplicationService.Value.QueueAllUnprocessedChangeEvents();
            return this.Content("Done!");
        }

        public ActionResult SyncHsGurantorMatchAcknowledgement()
        {
            //this._guarantorAcknowledgementApplicationService.QueueNewlyAcknowledgedHsGuarantors();
            return this.Content("Congratulations! This Feature has been removed");
        }

        [HttpPost]
        public ActionResult RunNotifyExpiringPrimaryPaymentMethods(DateTime date)
        {
            try
            {
                DateTime offsetDate = date.ToDateTime(TimeZone.CurrentTimeZone.StandardName);
                this._paymentMethodsApplicationService.Value.NotifyExpiringPrimaryPaymentMethods(offsetDate);
                return this.Content("Done!");
            }
            catch (Exception e)
            {
                return this.Content(e.Message);
            }
        }

        [HttpPost]
        public ActionResult RunConsolidationExpiration(DateTime date)
        {
            try
            {
                DateTime offsetDate = date.ToDateTime(TimeZone.CurrentTimeZone.StandardName);
                this._consolidationGuarantorApplicationService.Value.CheckForExpiringConsolidations(offsetDate);
                return this.Content("Done!");
            }
            catch (Exception e)
            {
                return this.Content(e.Message);
            }
        }

        [HttpPost]
        public ActionResult RunConsolidationReminders(DateTime date)
        {
            try
            {
                DateTime offsetDate = date.ToDateTime(TimeZone.CurrentTimeZone.StandardName);
                this._consolidationGuarantorApplicationService.Value.SendConsolidationReminders(offsetDate);
                return this.Content("Done!");
            }
            catch (Exception e)
            {
                return this.Content(e.Message);
            }
        }

        [HttpPost]
        public ActionResult QueueBalanceDueReminders(DateTime date)
        {
            try
            {
                DateTime offsetDate = date.ToDateTime(TimeZone.CurrentTimeZone.StandardName);
                this._financialDataSummaryApplicationService.Value.QueueBalanceDueReminders(offsetDate);
                return this.Content("Done!");
            }
            catch (Exception e)
            {
                return this.Content(e.Message);
            }
        }

        [HttpPost]
        public ActionResult QueuePaymentReminderProcessing(DateTime date)
        {
            try
            {
                this._scheduledPaymentApplicationService.Value.QueueScheduledPaymentReminders(date);
                return this.Content("Done!");
            }
            catch (Exception e)
            {
                return this.Content(e.Message);
            }
        }

        [HttpPost]
        public ActionResult RunScheduledPaymentProcessor(int guarantorId, DateTime date)
        {
            try
            {
                DateTime offsetDate = date.ToDateTime(TimeZone.CurrentTimeZone.StandardName);
                this._scheduledPaymentApplicationService.Value.ProcessScheduledPayment(guarantorId, offsetDate);
                return this.Content("Done!");
            }
            catch (Exception e)
            {
                return this.Content(e.Message);
            }
        }

        [HttpPost]
        public async Task<ActionResult> RunPaymentReconciliation(DateTime beginDate, DateTime endDate, string transactionIds = null)
        {
            try
            {
                DateTime beginDateTime = beginDate.ToDateTime(TimeZone.CurrentTimeZone.StandardName);
                DateTime endDateTime = endDate.ToDateTime(TimeZone.CurrentTimeZone.StandardName);
                int recordCount = 0;

                if (!string.IsNullOrEmpty(transactionIds))
                {
                    List<QueryPaymentTransaction> values = transactionIds.Split(';').Select(x => new QueryPaymentTransaction
                    {
                        TransactionId = x
                    }).ToList();

                    string serializedValues = JsonConvert.SerializeObject(values);
                    await this._cache.Value.SetStringAsync("Ivh.Provider.FinanceManagement.TrustCommerce.IPaymentBatchOperationsProvider.GetPaymentTransactionsAsync", serializedValues);

                    IPaymentBatchOperationsApplicationService paymentBatchOperationsApplicationService = DependencyResolver.Current.GetService<IPaymentBatchOperationsApplicationService>();
                    recordCount = await paymentBatchOperationsApplicationService.ReconcilePaymentsAsync(beginDateTime, endDateTime);
                }
                else
                {
                    recordCount = await this._paymentBatchOperationsApplicationService.Value.ReconcilePaymentsAsync(beginDateTime, endDateTime);
                }
                return this.Content("Done! Records Processed: " + recordCount);
            }
            catch (Exception e)
            {
                return this.Content(e.Message);
            }
        }

        [HttpPost]
        public async Task<ActionResult> RunPaymentReconciliationReversal(PaymentTypeEnum reversalType, string transactionId, decimal? amount = null)
        {
            try
            {
                DateTime beginDateTime = DateTime.UtcNow.Date;
                DateTime endDateTime = DateTime.UtcNow.Date;
                int recordCount = 0;

                IList<QueryPaymentTransaction> values = new QueryPaymentTransaction
                {
                    Amount = amount.HasValue && reversalType == PaymentTypeEnum.PartialRefund ? amount.Value : default(decimal),
                    TransactionId = transactionId,
                    PaymentType = reversalType
                }.ToListOfOne();

                string serializedValues = JsonConvert.SerializeObject(values);
                await this._cache.Value.SetStringAsync("Ivh.Provider.FinanceManagement.TrustCommerce.IPaymentBatchOperationsProvider.GetPaymentTransactionsAsync", serializedValues);

                recordCount = await this._paymentBatchOperationsApplicationService.Value.ReconcilePaymentsAsync(beginDateTime, endDateTime);

                return this.Content("Done! Records Processed: " + recordCount);
            }
            catch (Exception e)
            {
                return this.Content(e.Message);
            }
        }



        [HttpPost]
        public ActionResult RunSystemExceptionLogging(DateTime date)
        {
            try
            {
                DateTime dateTime = date.ToDateTime(TimeZone.CurrentTimeZone.StandardName);
                this._systemExceptionApplicationService.Value.LogSystemExceptions(dateTime);
                return this.Content("Done!");
            }
            catch (Exception e)
            {
                return this.Content(e.Message);
            }
        }

        [HttpPost]
        public ActionResult SendSampleEmails()
        {
            try
            {
                this._sendSampleEmailsService.Value.Send();
                return this.Content("Done!");
            }
            catch (Exception e)
            {
                return this.Content(e.Message);
            }
        }

        [HttpPost]
        public ActionResult SendSamplePaperMail(int vpGuarantorId)
        {
            try
            {
                IList<string> resultMessages = this._sendSamplePaperMailService.Value.Send(vpGuarantorId);
                return this.Content(string.Join(" ", resultMessages));
            }
            catch (Exception e)
            {
                return this.Content(e.Message);
            }
        }

        [HttpPost]
        public async Task<ActionResult> ImportPackages()
        {
            try
            {
                await this._fileStorageApplicationService.Value.ImportDirectoryAsync();
                return this.Content("Done!");
            }
            catch (Exception e)
            {
                return this.Content(e.Message);
            }
        }

        [HttpPost]
        public async Task<ActionResult> MatchVisits()
        {
            try
            {
                await this._fileStorageApplicationService.Value.MatchFilesWithVisitsAsync();
                return this.Content("Done!");
            }
            catch (Exception e)
            {
                return this.Content(e.Message);
            }
        }

        [HttpPost]
        public ActionResult DeactivateInactiveClientUsers()
        {
            try
            {
                this._visitPayUserApplicationService.Value.DeactivateInactiveClientUsers();
                return this.Content("Done!");
            }
            catch (Exception e)
            {
                return this.Content(e.Message);
            }
        }

        [HttpPost]
        public ActionResult BatchProcessStatementFiles(DateTime date)
        {
            try
            {
                this._statementExportApplicationService.Value.ExportPaperStatements(date);
                return this.Content("Done!");
            }
            catch (Exception e)
            {
                return this.Content(e.Message);
            }
        }

        public ActionResult ImportEobFiles()
        {
            try
            {
                this._load835Service.Value.Load835File();
                return this.Content("Done!");
            }
            catch (Exception e)
            {
                return this.Content(e.Message);
            }
        }

        [HttpPost]
        public ActionResult QueueMatchRegistryPopulation(DateTime? date)
        {
            try
            {
                this._matchingApplicationService.Value.QueueMatchRegistryPopulation(date);
                return this.Content("Done!");
            }
            catch (Exception e)
            {
                return this.Content(e.Message);
            }
        }

        public ActionResult QueueFindNewHsGuarantorMatches()
        {
            try
            {
                this._matchingApplicationService.Value.QueueFindNewHsGuarantorMatches();
                return this.Content("Done!");
            }
            catch (Exception e)
            {
                return this.Content(e.Message);
            }
        }

        [HttpPost]
        public ActionResult QueueMatchReconciliation()
        {
            try
            {
                this._matchingApplicationService.Value.QueueMatchReconciliation();
                return this.Content("Done!");
            }
            catch (Exception e)
            {
                return this.Content(e.Message);
            }
        }

        [HttpPost]
        public ActionResult SyncGuestPayData()
        {
            this._hospitalInboundApplicationService.Value.QueueSyncGuestPayData();

            return this.Json("Message sent!");
        }

        #region payment batch processing

        [HttpGet]
        public ActionResult PaymentBatchProcessing()
        {
            return this.View();
        }

        [HttpPost]
        public JsonResult GetUnclearedPaymentAllocations()
        {
            IList<UnclearedPaymentAllocationDto> vpOutboundVisitTransactionDtos = this._qaPaymentBatchApplicationService.Value.GetUnclearedOutboundVisitTransactions();

            IList<UnclearedPaymentAllocationViewModel> model = Mapper.Map<IList<UnclearedPaymentAllocationViewModel>>(vpOutboundVisitTransactionDtos);

            return this.Json(model);
        }

        [HttpPost]
        public JsonResult ClearPaymentAllocations(int[] model)
        {
            if (model.IsNullOrEmpty())
            {
                return this.Json(false);
            }

            // get all outbound transactions from the UI input
            IList<UnclearedPaymentAllocationDto> unclearedPaymentAllocations = this._qaPaymentBatchApplicationService.Value.GetOutboundVisitTransactionsById(model);

            // create a changeset for each visit with an outbound transaction
            foreach (IGrouping<int, UnclearedPaymentAllocationDto> grouping in unclearedPaymentAllocations.GroupBy(x => x.HsVisitId))
            {
                int hsVisitId = grouping.Key;
                IList<UnclearedPaymentAllocationDto> unclearedPaymentAllocationsForVisit = grouping.ToList();

                // transaction for each allocation
                IList<VisitTransactionViewModel> transactions = unclearedPaymentAllocationsForVisit.Select(x =>
                {
                    VpTransactionTypeEnum vpTransactionType = VpTransactionTypeEnum.VppPrincipalPayment;
                    if (x.VpPaymentAllocationTypeId.GetValueOrDefault(0) == (int)PaymentAllocationTypeEnum.VisitInterest)
                    {
                        vpTransactionType = VpTransactionTypeEnum.VppInterestPayment;
                    }
                    else if (x.VpPaymentAllocationTypeId.GetValueOrDefault(0) == (int)PaymentAllocationTypeEnum.VisitDiscount)
                    {
                        vpTransactionType = VpTransactionTypeEnum.VppDiscount;
                    }

                    VisitTransactionViewModel vm = Mapper.Map<VisitTransactionViewModel>(x);
                    vm.VpTransactionTypeId = (int)vpTransactionType;
                    return vm;
                }).ToList();

                //
                ChangeSetDto changeSetDto = this._changeSetApplicationService.Value.CreateChangeSetFromVisit(hsVisitId);

                // build up changeset
                new ChangeSetDtoBuilder(changeSetDto)
                    .AddChangeEventVisitTransactionDataChanged(true, "VisitTransactionDataChanged from QAT.ClearPaymentAllocations")
                    .MergeTransactions(transactions)
                    .MergePaymentAllocations(unclearedPaymentAllocationsForVisit.Select(x => x.VpPaymentAllocationId).ToList())
                    .SetVisitBalance();

                // save
                this._changeEventApplicationService.Value.SaveChangeEvents(changeSetDto, ChangeSetTypeEnum.Visit);
            }

            // queue
            this._changeEventApplicationService.Value.QueueAllUnprocessedChangeEvents();

            return this.Json(true);
        }


        #endregion

        #region scoring And segmentation processing

        [HttpPost]
        [AutomationAuthorize]
        public ActionResult ExecuteScoring()
        {
            try
            {
                this._scoringGuarantorMatchingService.Value.ExecuteScoring();
                return this.Content("Done!");
            }
            catch (Exception e)
            {
                return this.Content(e.Message);
            }
        }

        #endregion

    }
}
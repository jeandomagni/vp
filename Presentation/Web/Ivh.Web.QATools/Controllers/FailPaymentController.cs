﻿namespace Ivh.Web.QATools.Controllers
{
    using System;
    using System.Web.Mvc;
    using Common.Web.Controllers;
    using Common.Web.Interfaces;
    using Common.Web.Models.FailPayment;

    public class FailPaymentController : BaseWebController
    {
        public FailPaymentController(Lazy<IBaseControllerService> baseControllerService)
            : base(baseControllerService)
        {
        }

        protected FailPaymentViewModel GetModelForIndex(bool? failPayment)
        {
            FailPaymentViewModel failPaymentViewModel = new FailPaymentViewModel { FailPaymentProviderIsActive = failPayment.HasValue && failPayment.Value };

            /*if (IvhEnvironment.IsNonProduction)
            {
                ContainerBuilder builder = new ContainerBuilder();
                RegisterVisitPayBase registerVisitPayBase = new RegisterVisitPayBase();

                registerVisitPayBase.RegisterPaymentProvider(builder, failPaymentViewModel.FailPaymentProviderIsActive);
                IContainer container = IvinciContainer.Instance.Container();
                builder.Update(container);

                IDistributedCache cache = DependencyResolver.Current.GetService<IDistributedCache>();
                bool success = cache.SetString("FailPaymentProviderIsActive", failPaymentViewModel.FailPaymentProviderIsActive.ToString());
            }*/

            return failPaymentViewModel;
        }
        public ActionResult Index(bool? failPayment)
        {
            return this.View(this.GetModelForIndex(failPayment));
        }
    }
}
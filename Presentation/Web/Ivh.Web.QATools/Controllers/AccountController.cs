﻿namespace Ivh.Web.QATools.Controllers
{
    using System;
    using System.Threading.Tasks;
    using System.Web;
    using System.Web.Mvc;
    using Application.Core.Common.Interfaces;
    using Application.User.Common.Dtos;
    using AutoMapper;
    using Common.Base.Enums;
    using Common.VisitPay.Enums;
    using Common.VisitPay.Strings;
    using Common.Web.Interfaces;
    using Microsoft.AspNet.Identity;
    using Microsoft.Owin.Security;
    using Models;

    [Authorize(Roles = VisitPayRoleStrings.System.QATools+","+VisitPayRoleStrings.Admin.Administrator)]
    public class AccountController : BaseController
    {
        private readonly Lazy<IVisitPayUserApplicationService> _visitPayUserApplicationService;

        public AccountController(
            Lazy<IBaseControllerService> baseControllerService, 
            Lazy<IVisitPayUserApplicationService> visitPayUserApplicationService
            )
            : base(baseControllerService)
        {
            this._visitPayUserApplicationService = visitPayUserApplicationService;
        }

        // GET: /Account/Login
        [AllowAnonymous]
        public ActionResult OAuthLogin(string returnUrl, string ssoToken)
        {
            AuthenticationTicket ticket = Startup.OAuthBearerAuthenticationOptions.AccessTokenFormat.Unprotect(ssoToken);
            if (ticket.Properties.ExpiresUtc > DateTime.UtcNow && ticket.Identity.HasClaim("urn:oauth:scope", "sso"))
            {
                var user = this._visitPayUserApplicationService.Value.FindByName(ticket.Identity.Name);
                if (user != null)
                {
                    this._visitPayUserApplicationService.Value.SignIn(user, false, false);
                }
                return this.RedirectToAction("Index", "Home");
            }
            return this.RedirectToAction("Login");
        }

        // GET: /Account/Login
        [AllowAnonymous]
        public ActionResult Login(string returnUrl)
        {
            return this.View(new LoginViewModel { ReturnUrl = returnUrl });
        }

    


        //
        // POST: /Account/Login
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Login(LoginViewModel model)
        {
            HttpContext context = System.Web.HttpContext.Current;
            if (!this.ModelState.IsValid)
            {
                return this.View(model);
            }

            // This doesn't count login failures towards account lockout
            // To enable password failures to trigger account lockout, change to shouldLockout: true
            var result = await this._visitPayUserApplicationService.Value.PasswordSignInAsync(Mapper.Map<HttpContextDto>(context), model.Username, model.Password, false, false, VisitPayRoleStrings.System.QATools);
            await this.BruteForceDelayAsync(context.Request.UserHostAddress, () => result == SignInStatusExEnum.Failure, () => result == SignInStatusExEnum.Success);
            switch (result)
            {
                case SignInStatusExEnum.Success:
                    return this.RedirectToLocal(model.ReturnUrl);
                case SignInStatusExEnum.LockedOut:
                    return this.View("Lockout");
                case SignInStatusExEnum.RequiresVerification:
                    return this.RedirectToAction("SendCode", new { ReturnUrl = model.ReturnUrl });
                case SignInStatusExEnum.Failure:
                default:
                    this.ModelState.AddModelError("", "Invalid login attempt.");
                    return this.View(model);
            }
        }


        //
        // GET: /Account/Register
        [AllowAnonymous]
        public ActionResult Register()
        {
            return this.View();
        }

        //
        // POST: /Account/Register
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Register(RegisterViewModel model)
        {
            if (this.ModelState.IsValid)
            {
                var user = new VisitPayUserDto { UserName = model.Email, Email = model.Email };
                var result = await this._visitPayUserApplicationService.Value.CreateWithBasicRoleAsync(user, model.Password, VisitPayRoleStrings.System.QATools);
                if (result.Succeeded)
                {
                    this._visitPayUserApplicationService.Value.SignIn(user, false, false);

                    // For more information on how to enable account confirmation and password reset please visit http://go.microsoft.com/fwlink/?LinkID=320771
                    // Send an email with this link
                    // string code = await UserManager.GenerateEmailConfirmationTokenAsync(user.Id);
                    // var callbackUrl = Url.Action("ConfirmEmail", "Account", new { userId = user.Id, code = code }, protocol: Request.Url.Scheme);
                    // await UserManager.SendEmailAsync(user.Id, "Confirm your account", "Please confirm your account by clicking <a href=\"" + callbackUrl + "\">here</a>");

                    return this.RedirectToAction("Index", "Home");
                }
                this.AddErrors(result);
            }

            // If we got this far, something failed, redisplay form
            return this.View(model);
        }

        //
        // GET: /Account/ConfirmEmail
        [AllowAnonymous]
        public async Task<ActionResult> ConfirmEmail(string userId, string code)
        {
            if (userId == null || code == null)
            {
                return this.View("Error");
            }
            var result = await this._visitPayUserApplicationService.Value.ConfirmEmailAsync(userId, code);
            return this.View(result.Succeeded ? "ConfirmEmail" : "Error");
        }

        //
        // GET: /Account/ForgotPassword
        [AllowAnonymous]
        public ActionResult ForgotPassword()
        {
            return this.View();
        }

        //
        // POST: /Account/ForgotPassword
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ForgotPassword(ForgotPasswordViewModel model)
        {
            if (this.ModelState.IsValid)
            {
                var user = await this._visitPayUserApplicationService.Value.FindByNameAsync(model.Email);
                if (user == null || !(await this._visitPayUserApplicationService.Value.IsEmailConfirmedAsync(user.Id)))
                {
                    // Don't reveal that the user does not exist or is not confirmed
                    return this.View("ForgotPasswordConfirmation");
                }

                // For more information on how to enable account confirmation and password reset please visit http://go.microsoft.com/fwlink/?LinkID=320771
                // Send an email with this link
                // string code = await UserManager.GeneratePasswordResetTokenAsync(user.Id);
                // var callbackUrl = Url.Action("ResetPassword", "Account", new { userId = user.Id, code = code }, protocol: Request.Url.Scheme);		
                // await UserManager.SendEmailAsync(user.Id, "Reset Password", "Please reset your password by clicking <a href=\"" + callbackUrl + "\">here</a>");
                // return RedirectToAction("ForgotPasswordConfirmation", "Account");
            }

            // If we got this far, something failed, redisplay form
            return this.View(model);
        }

        //
        // GET: /Account/ForgotPasswordConfirmation
        [AllowAnonymous]
        public ActionResult ForgotPasswordConfirmation()
        {
            return this.View();
        }

        //
        // GET: /Account/ResetPassword
        [AllowAnonymous]
        public ActionResult ResetPassword(string code)
        {
            return code == null ? this.View("Error") : this.View();
        }

        //
        // POST: /Account/ResetPassword
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ResetPassword(ResetPasswordViewModel model)
        {
            if (!this.ModelState.IsValid)
            {
                return this.View(model);
            }
            var user = await this._visitPayUserApplicationService.Value.FindByNameAsync(model.Email);
            if (user == null)
            {
                // Don't reveal that the user does not exist
                return this.RedirectToAction("ResetPasswordConfirmation", "Account");
            }
            var result = await this._visitPayUserApplicationService.Value.ResetPasswordAsync(user.Id, model.Code, model.Password, 999, 10);
            if (result.Succeeded)
            {
                return this.RedirectToAction("ResetPasswordConfirmation", "Account");
            }
            this.AddErrors(result);
            return this.View();
        }

        //
        // GET: /Account/ResetPasswordConfirmation
        [AllowAnonymous]
        public ActionResult ResetPasswordConfirmation()
        {
            return this.View();
        }

        //
        // POST: /Account/LogOff
        [AllowAnonymous]
        public ActionResult LogOff()
        {
            if (this.User.Identity.IsAuthenticated)
            {
                this.AuthenticationManager.SignOut();
            }
            this.SessionFacade.Value.ClearSession();
            return this.RedirectToAction("Index", "Home");
        }

        [AllowAnonymous]
        public ActionResult UpdateSession()
        {
            //Using this to touch the session to keep it alive.
            return this.Json(true, JsonRequestBehavior.AllowGet);
        }

        #region Helpers

        // Used for XSRF protection when adding external logins
        private const string XsrfKey = "XsrfId";

        private IAuthenticationManager AuthenticationManager
        {
            get { return this.HttpContext.GetOwinContext().Authentication; }
        }

        private void AddErrors(IdentityResult result)
        {
            foreach (var error in result.Errors)
            {
                this.ModelState.AddModelError("", error);
            }
        }

        private ActionResult RedirectToLocal(string returnUrl)
        {
            if (this.Url.IsLocalUrl(returnUrl))
            {
                return this.Redirect(returnUrl);
            }
            return this.RedirectToAction("Index", "Home");
        }

        internal class ChallengeResult : HttpUnauthorizedResult
        {
            public ChallengeResult(string provider, string redirectUri)
                : this(provider, redirectUri, null)
            {
            }

            public ChallengeResult(string provider, string redirectUri, string userId)
            {
                this.LoginProvider = provider;
                this.RedirectUri = redirectUri;
                this.UserId = userId;
            }

            public string LoginProvider { get; set; }
            public string RedirectUri { get; set; }
            public string UserId { get; set; }

            public override void ExecuteResult(ControllerContext context)
            {
                var properties = new AuthenticationProperties { RedirectUri = this.RedirectUri };
                if (this.UserId != null)
                {
                    properties.Dictionary[XsrfKey] = this.UserId;
                }
                context.HttpContext.GetOwinContext().Authentication.Challenge(properties, this.LoginProvider);
            }
        }

        #endregion
    }
}
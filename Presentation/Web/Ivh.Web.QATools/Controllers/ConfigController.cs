﻿namespace Ivh.Web.QATools.Controllers
{
    using System.Threading.Tasks;
    using Common.Web.Interfaces;
    using Interfaces;
    using Models;
    using System;
    using System.Linq;
    using System.Web.Mvc;


    public class ConfigController : BaseController
    {
        private readonly IQAInterestRateRepository _interestRateRepository;
        private readonly IQAFinancePlanMinPaymentTierRepository _financePlanMinPaymentTier;

        public ConfigController(
            Lazy<IBaseControllerService> baseControllerService,
            IQAInterestRateRepository interestRateRepository,
            IQAFinancePlanMinPaymentTierRepository financePlanMinPaymentTier
               )
            : base(baseControllerService)
        {
            this._interestRateRepository = interestRateRepository;
            this._financePlanMinPaymentTier = financePlanMinPaymentTier;

        }

        // GET: Config
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult InterestRate()
        {
            InterestRateViewModel model = new InterestRateViewModel()
            {
                InterestRates = this._interestRateRepository.GetInterestRateAsync().Result.ToList()
            };

            return this.View(model);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public async Task<ActionResult> CreateInterestRate(InterestRateViewModel viewModel)
        {
            await this._interestRateRepository.CreateInterestRateAsync(viewModel);
            return this.RedirectToAction("InterestRate");
        }

        public async Task<ActionResult> DeleteInterestRate(int id)
        {
            await this._interestRateRepository.DeleteInterestRateAsync(id);
            return this.RedirectToAction("InterestRate");
        }

        public ActionResult FinancePlanMinPaymentTier()
        {

            FinancePlanMinPaymentTierViewModel model = new FinancePlanMinPaymentTierViewModel()
            {
                FinancePlanMinPaymentTiers = this._financePlanMinPaymentTier.GetFinancePlanMinPaymentTierAsync().Result.ToList()

            };

            return this.View(model);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public async Task<ActionResult> CreateFinancePlanMinPaymentTier(FinancePlanMinPaymentTierViewModel viewModel)
        {
            await this._financePlanMinPaymentTier.CreateFinancePlanMinPaymentTierAsync(viewModel);
            return this.RedirectToAction("FinancePlanMinPaymentTier");
        }

        public async Task<ActionResult> DeleteFinancePlanMinPaymentTier(int id)
        {
            await this._financePlanMinPaymentTier.DeleteFinancePlanMinPaymentTierAsync(id);
            return this.RedirectToAction("FinancePlanMinPaymentTier");
        }

    }
}
﻿namespace Ivh.Web.QATools.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Net.Http;
    using System.Net.Http.Headers;
    using System.Text;
    using System.Web.Mvc;
    using Application.Email.Common.Interfaces;
    using Application.Qat.Common.Dtos;
    using Application.Qat.Common.Interfaces;
    using AutoMapper;
    using Common.Base.Utilities.Extensions;
    using Common.Base.Utilities.Helpers;
    using Common.VisitPay.Messages.Communication;
    using Common.Web.Extensions;
    using Domain.Settings.Enums;
    using Domain.Settings.Interfaces;
    using Models.MockCommunication;
    using Services;

    [Authorize]
    public class MockCommunicationController : Controller
    {
        private readonly Lazy<ICommunicationInterceptorApplicationService> _communicationInterceptorApplicationService;
        private readonly Lazy<ISendSampleEmailsService> _sendSampleEmailsService;
        private readonly Lazy<IApplicationSettingsService> _applicationSettingService;
        private readonly Lazy<IQaTextToPayApplicationService> _qaTextToPayApplicationService;

        public MockCommunicationController(
            Lazy<ISendSampleEmailsService> sendSampleEmailsService,
            Lazy<ICommunicationInterceptorApplicationService> communicationInterceptorApplicationService,
            Lazy<ITwilioSmsEventApplicationService> twilioSmsEventApplication,
            Lazy<IApplicationSettingsService> applicationSettingService,
            Lazy<IQaTextToPayApplicationService> qaTextToPayApplicationService
            )
        {
            this._communicationInterceptorApplicationService = communicationInterceptorApplicationService;
            this._sendSampleEmailsService = sendSampleEmailsService;
            this._applicationSettingService = applicationSettingService;
            this._qaTextToPayApplicationService = qaTextToPayApplicationService;
        }

        [HttpGet]
        public ActionResult Index()
        {
            return this.View(new MockCommunicationViewModel
            {
                IsActive = this._communicationInterceptorApplicationService.Value.IsActive()
            });
        }

        [HttpPost, AllowAnonymous]
        public ActionResult SendMessage(string to, string from, string body, string accountId)
        {
            return this.Json(new { Sid = Guid.NewGuid().ToString(), status = "accepted" });
        }

        [HttpPost]
        public void Activate(bool isActivated)
        {
            if (this._communicationInterceptorApplicationService.Value.IsActive())
            {
                this._communicationInterceptorApplicationService.Value.Deactivate();
            }
            else
            {
                Uri uri = this.Request.Url;
                if (uri != null)
                    this._communicationInterceptorApplicationService.Value.Activate(string.Format("{0}://{1}:{2}/{3}", uri.Scheme, uri.Host, uri.Port, "MockCommunication"));
            }
        }

        [HttpPost]
        public void SendSmsMessages(int vpGuarantorId)
        {
            this._sendSampleEmailsService.Value.SendSms(vpGuarantorId);
        }

        [HttpGet]
        public ActionResult ReplyToTextMessage(int? visitPayUserId)
        {
            List<CommunicationDto> communications = this._qaTextToPayApplicationService.Value.GetTextToPayPaymentOptionCommunications();
            List<PayByTextCommunication> payByTextCommunications = Mapper.Map<List<PayByTextCommunication>>(communications);

            PayByTextCommunication payByTextCommunication = payByTextCommunications.FirstOrDefault();
            if (visitPayUserId.HasValue)
            {
                payByTextCommunication = payByTextCommunications.FirstOrDefault(x => x.SentToUserId == visitPayUserId);
            }
           
            ReplyToTextMessageViewModel replyToTextMessageViewModel = new ReplyToTextMessageViewModel
            {
                UsersWithPayByTextCommunication = payByTextCommunications,
                SelectedBody = payByTextCommunication?.Body,
                SelectedSentToUserId = payByTextCommunication?.SentToUserId ?? 0,
                SelectedPhoneNumber = payByTextCommunication?.PhoneNumber ?? ""
            };

            return this.View(replyToTextMessageViewModel);            
        }

        /// <summary>
        /// For local development you will need to change Twilio.CallbackUrlBase to 
        /// http://localhost:50409/
        /// found in Presentation\Web\Ivh.Web.ClientDataApi\AppSettings.Config
        /// 
        /// Requires Ivh.Web.SendMailWebHookEvents to be running
        /// </summary>
        /// <param name="replyToTextMessageViewModel"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult ProcessReplyToTextMessage(ReplyToTextMessageViewModel replyToTextMessageViewModel)
        {
            string twillioApiCredentials = $"{this._applicationSettingService.Value.TwilioCallbackUsername.Value}:{this._applicationSettingService.Value.TwilioCallbackPassword.Value}";
            //twilio callback splits the Authorization Header by empty space taking array index 1
            string authorizationHeaderValue = $"AuthorizationValue {Convert.ToBase64String(twillioApiCredentials.ToByteArrayUTF8())}";

            string phoneNumber = FormatHelper.VisitPayUserPhoneNumberToTwilioPhoneNumberFormat(replyToTextMessageViewModel.SelectedPhoneNumber);
            TwilioCallbackMessage twilioCallbackMessage = new TwilioCallbackMessage { From = phoneNumber, Body = replyToTextMessageViewModel.ReplyText };
           
            //twilio callback currently requires all properties to be passed in 
            string callbackMessageAsQueryString = twilioCallbackMessage.GetQueryString();
            StringContent content = new StringContent(callbackMessageAsQueryString, Encoding.UTF8, "application/x-www-form-urlencoded");

            using (HttpClient client = new HttpClient())
            {
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Add("Authorization", authorizationHeaderValue);
                
                //Note: local development will require you to edit the config value for TwilioCallbackBase
                HttpResponseMessage response = client.PostAsync($"{this._applicationSettingService.Value.TwilioCallbackBase.Value}TwilioMessageCallback", content).Result;
                if (response.IsSuccessStatusCode)
                {
                    return this.RedirectToAction("ReplyToTextMessage", new { visitPayUserId = replyToTextMessageViewModel.SelectedSentToUserId });
                }
                return this.Json(new { status = "An error occurred calling Twilio Callback api" }, JsonRequestBehavior.AllowGet);
            }
            
        }

        [HttpGet]
        public ActionResult GetPaymentsPartial(int visitPayUserId)
        {
            List<TextToPayPaymentDto> textToPayPayments = this._qaTextToPayApplicationService.Value.GetTextToPayPayments(visitPayUserId);
            return this.PartialView("_TextToPayPaymentsPartial", textToPayPayments);
        }
    }
}
﻿namespace Ivh.Web.QATools.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using System.Web.Mvc;
    using System.Web.SessionState;
    using Application.FinanceManagement.Common.Interfaces;
    using Application.Qat.Common.Dtos;
    using Application.Qat.Common.Interfaces;
    using AutoMapper;
    using Models.MockPaymentProcessor;

    [Authorize]
    public class MockPaymentProcessorController : Controller
    {
        private readonly Lazy<IPaymentProcessorInterceptorApplicationService> _paymentProcessorInterceptorApplicationService;
        private readonly Lazy<IAchInterceptorApplicationService> _achInterceptorApplicationService;
        private readonly Lazy<IAchApplicationService> _achApplicationService;

        public MockPaymentProcessorController(
            Lazy<IPaymentProcessorInterceptorApplicationService> paymentProcessorInterceptorApplicationService,
            Lazy<IAchInterceptorApplicationService> achInterceptorApplicationService,
            Lazy<IAchApplicationService> achApplicationService)
        {
            this._paymentProcessorInterceptorApplicationService = paymentProcessorInterceptorApplicationService;
            this._achInterceptorApplicationService = achInterceptorApplicationService;
            this._achApplicationService = achApplicationService;
        }

        [HttpGet]
        public ActionResult Index()
        {
            return this.View(new MockPaymentProcessorViewModel
            {
                DefaultSuccessful = this._paymentProcessorInterceptorApplicationService.Value.DefaultSuccessful(),
                DelayMs = this._paymentProcessorInterceptorApplicationService.Value.DelayMs(),
                IsActive = this._paymentProcessorInterceptorApplicationService.Value.IsActive(),
                Payments = Mapper.Map<IList<MockPaymentViewModel>>(this._paymentProcessorInterceptorApplicationService.Value.QueryRequestQueue(new PpiRequestFilterDto()))
            });
        }

        [HttpGet]
        public ActionResult Ach()
        {
            return this.View(new MockAchProcessorViewModel
            {
                Payments = Mapper.Map<IList<MockAchViewModel>>(this._paymentProcessorInterceptorApplicationService.Value.QueryRequestQueue(new PpiRequestFilterDto
                {
                    IsAch = true,
                    IsSuccess = true
                }))
            });
        }

        [HttpPost]
        public JsonResult Ach(DateTime processDate)
        {
            try
            {
                List<Guid> requestsProcessed = this._paymentProcessorInterceptorApplicationService.Value.QueryRequestQueue(new PpiRequestFilterDto
                {
                    AchProcessDate = processDate,
                    AchProcessed = false,
                    IsSuccess = true,
                    IsAch = true
                }).Select(x => x.RequestGuid).ToList();

                this._achApplicationService.Value.DownloadAchReturnsAndSettlements(processDate, processDate.AddDays(1));
                this._achApplicationService.Value.PublishAchReturnsAndSettlements(processDate, processDate.AddDays(1));

                return this.Json(new
                {
                    Success = true,
                    RequestsProcessed = requestsProcessed
                });
            }
            catch (Exception)
            {
                return this.Json(new
                {
                    Success = false,
                    RequestsProcessed = new List<Guid>()
                });
            }
        }

        [HttpPost]
        public void Activate(bool isActivated)
        {
            if (this._paymentProcessorInterceptorApplicationService.Value.IsActive())
            {
                this._paymentProcessorInterceptorApplicationService.Value.Deactivate();
            }
            else
            {
                Uri uri = this.Request.Url;
                if (uri != null)
                {
                    this._paymentProcessorInterceptorApplicationService.Value.Activate(string.Format("{0}://{1}:{2}/{3}", uri.Scheme, uri.Host, uri.Port, "MockPaymentProcessor"));
                    this._achInterceptorApplicationService.Value.Activate(string.Format("{0}://{1}:{2}/{3}", uri.Scheme, uri.Host, uri.Port, "Services/NachaGatewayInceptorService.asmx"));
                }
            }
        }

        [HttpPost]
        [AllowAnonymous]
        public async Task<ContentResult> Trans(FormCollection collection)
        {
            string response = await this._paymentProcessorInterceptorApplicationService.Value.GetResponseAsync(collection.AllKeys.ToDictionary(k => k, v => collection[v])).ConfigureAwait(true);

            return new ContentResult
            {
                Content = response
            };
        }

        [HttpPost]
        [AllowAnonymous]
        public ContentResult Query(FormCollection collection)
        {
            return new ContentResult();
        }
    }
}
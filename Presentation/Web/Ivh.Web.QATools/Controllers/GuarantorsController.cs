﻿namespace Ivh.Web.QATools.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using System.Web.Mvc;
    using System.Web.SessionState;
    using Application.Core.Common.Dtos;
    using Application.Core.Common.Interfaces;
    using Application.FinanceManagement.Common.Dtos;
    using Application.FinanceManagement.Common.Interfaces;
    using Application.Qat.Common.Interfaces;
    using Application.User.Common.Dtos;
    using Attributes;
    using Common.Base.Enums;
    using Common.Base.Utilities.Extensions;
    using Common.ServiceBus.Interfaces;
    using Common.VisitPay.Enums;
    using Common.VisitPay.Messages.FinanceManagement;
    using Common.VisitPay.Strings;
    using Common.Web.Interfaces;
    using Domain.FinanceManagement.Statement.Interfaces;
    using Domain.Qat.Entities;
    using Domain.Qat.Interfaces;
    using Domain.Settings.Interfaces;
    using Entities;
    using Interfaces;
    using Models;
    using Models.PaymentMethods;
    using Repositories;

    [Authorize]
    public class GuarantorsController : BaseController
    {
        private readonly Lazy<IFinancialDataSummaryApplicationService> _financialDataSummaryApplicationService;
        private readonly Lazy<IGuarantorApplicationService> _guarantorApplicationService;
        private readonly Lazy<IVpGuarantorKeepCurrentRepository> _guarantorKeepCurrentRepository;
        private readonly Lazy<IGuarantorRepository> _guarantorRepository;
        private readonly Lazy<IPaymentMethodsApplicationService> _paymentMethodsApplicationService;
        private readonly Lazy<IPaymentSubmissionApplicationService> _paymentSubmissionApplicationService;
        private readonly Lazy<IVisitApplicationService> _visitApplicationService;
        private readonly Lazy<IVisitPayGuarantorAuditLogApplicatonService> _visitPayGuarantorAuditLogApplicatonService;
        private readonly Lazy<IVisitPayUserApplicationService> _visitPayUserApplicationService;
        private readonly Lazy<IVisitPayUserIssueResultApplicationService> _visitPayUserIssueResultApplicationService;
        private readonly Lazy<IStatementApplicationService> _statementApplicationService;
        private readonly Lazy<ISsoApplicationService> _ssoApplicationService;
        private readonly Lazy<IApplicationSettingsService> _applicationSettingService;
        private readonly Lazy<ITestPaymentMethodsApplicationService> _testPaymentMethodsApplicationService;
        private readonly Lazy<IFeatureService> _featureService;
        private readonly Lazy<ICloneGuarantorApplicationService> _cloneGuarantorApplicationService;
        private readonly Lazy<IPersonasApplicationService> _personasApplicationService;
        private readonly Lazy<IQaAgingApplicationService> _qaAgingApplicationService;
        private readonly Lazy<IVpStatementService> _statementService;
        private readonly Lazy<IBus> _bus;

        public GuarantorsController(
            Lazy<IBaseControllerService> baseControllerService,
            Lazy<IFinancialDataSummaryApplicationService> financialDataSummaryApplicationService,
            Lazy<IGuarantorApplicationService> guarantorApplicationService,
            Lazy<IGuarantorRepository> guarantorRepository,
            Lazy<IVpGuarantorKeepCurrentRepository> guarantorKeepCurrentRepository,
            Lazy<IPaymentMethodsApplicationService> paymentMethodsApplicationService,
            Lazy<IPaymentSubmissionApplicationService> paymentSubmissionApplicationService,
            Lazy<IVisitApplicationService> visitApplicationService,
            Lazy<IVisitPayUserApplicationService> visitPayUserApplicationService,
            Lazy<IVisitPayGuarantorAuditLogApplicatonService> visitPayGuarantorAuditLogApplicationService,
            Lazy<IVisitPayUserIssueResultApplicationService> visitPayUserIssueResultApplicationService,
            Lazy<IStatementApplicationService> statementApplicationService,
            Lazy<ISsoApplicationService> ssoApplicationService,
            Lazy<IApplicationSettingsService> applicationSettingService,
            Lazy<ITestPaymentMethodsApplicationService> testPaymentMethodsApplicationService,
            Lazy<IFeatureService> featureService,
            Lazy<ICloneGuarantorApplicationService> cloneGuarantorApplicationService,
            Lazy<IPersonasApplicationService> personasApplicationService,
            Lazy<IQaAgingApplicationService> qaAgingApplicationService,
            Lazy<IVpStatementService> statementService,
            Lazy<IBus> bus)
            : base(baseControllerService)
        {
            this._financialDataSummaryApplicationService = financialDataSummaryApplicationService;
            this._guarantorApplicationService = guarantorApplicationService;
            this._guarantorRepository = guarantorRepository;
            this._guarantorKeepCurrentRepository = guarantorKeepCurrentRepository;
            this._paymentMethodsApplicationService = paymentMethodsApplicationService;
            this._paymentSubmissionApplicationService = paymentSubmissionApplicationService;
            this._visitPayUserApplicationService = visitPayUserApplicationService;
            this._visitApplicationService = visitApplicationService;
            this._visitPayGuarantorAuditLogApplicatonService = visitPayGuarantorAuditLogApplicationService;
            this._visitPayUserIssueResultApplicationService = visitPayUserIssueResultApplicationService;
            this._statementApplicationService = statementApplicationService;
            this._ssoApplicationService = ssoApplicationService;
            this._applicationSettingService = applicationSettingService;
            this._testPaymentMethodsApplicationService = testPaymentMethodsApplicationService;
            this._featureService = featureService;
            this._cloneGuarantorApplicationService = cloneGuarantorApplicationService;
            this._personasApplicationService = personasApplicationService;
            this._qaAgingApplicationService = qaAgingApplicationService;
            this._statementService = statementService;
            this._bus = bus;
        }

        const string UsernameKey = "Username";
        const string VpGuarantorIdKey = "VpGuarantorId";

        public ActionResult Index()
        {
            return this.View();
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult GuarantorAuditLog(int id)
        {
            return this.View();
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult GetGuarantorAuditLog(int id, int page, int rows, string sidx, string sord)
        {
            IReadOnlyList<VisitPayGuarantorAuditLogDto> data = this._visitPayGuarantorAuditLogApplicatonService.Value.GetVisitPayGuarantorAuditLog(id);
            int totalRecords = data.Count;

            IEnumerable<VisitPayGuarantorAuditLogDto> sortedData = SortAuditLog(data.ToList(), sidx, sord);

            var results = new
            {
                AuditLog = sortedData.Select(e => new
                {
                    e.UserEventHistoryId,
                    EventDate = e.EventDate.ToString("MM-dd-yyyy"),
                    e.UserName,
                    e.IpAddress,
                    e.EmulateUserId,
                    e.UserAgent,
                    e.EventType
                }).ToList().Skip((page * rows) - rows).Take(rows).ToList(),
                SortOrder = sord,
                SortField = sidx,
                page,
                total = Math.Ceiling((decimal)totalRecords / rows),
                records = totalRecords
            };
            return this.Json(results, JsonRequestBehavior.AllowGet);
        }

        private static IEnumerable<VisitPayGuarantorAuditLogDto> SortAuditLog(IList<VisitPayGuarantorAuditLogDto> auditLogDtos, string sortField, string sortOrder)
        {
            Func<VisitPayGuarantorAuditLogDto, object> orderByFunc = log => log;

            switch (sortField)
            {
                case "EventDate":
                    {
                        orderByFunc = log => log.EventDate;
                        break;
                    }
                case "UserName":
                    {
                        orderByFunc = log => log.UserName;
                        break;
                    }
                case "IpAddress":
                    {
                        orderByFunc = log => log.IpAddress;
                        break;
                    }
                case "EventType":
                    {
                        orderByFunc = log => log.EventType;
                        break;
                    }
                case "UserAgent":
                    {
                        orderByFunc = log => log.UserAgent;
                        break;
                    }
            }

            bool isAscendingOrder = "asc".Equals(sortOrder, StringComparison.InvariantCultureIgnoreCase);
            return ((isAscendingOrder) ? auditLogDtos.OrderBy(orderByFunc) : auditLogDtos.OrderByDescending(orderByFunc)).ToList();
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult GetGuarantors(string filter, string value, int page, int rows, string sidx, string sord)
        {
            GuarantorFilterDto guarantorFilterDto = new GuarantorFilterDto
            {
                SortField = sidx,
                SortOrder = sord,
                UserNameOrEmailAddress = (filter == "userName" || filter == "emailAddress") ? value : null,
                Ssn4 = (filter == "SSN4") ? value : null
            };
            JqGridObject jqGridData = new JqGridObject();

            GuarantorResultsDto data = this._guarantorApplicationService.Value.GetGuarantors(guarantorFilterDto, page, rows);
            jqGridData.Data = data.Guarantors.ToList();
            jqGridData.records = data.TotalRecords;
            jqGridData.page = page;
            jqGridData.total = (int)Math.Ceiling((decimal)data.TotalRecords / rows);
            JsonResult results = this.Json(jqGridData, JsonRequestBehavior.AllowGet);
            return results;
        }

        [HttpGet]
        [Common.Web.Attributes.SessionState(SessionStateBehavior.Required)]
        public ActionResult GetGuarantor(int id)
        {
            FinancialDataSummaryDto financialDataSummaryDto = this._financialDataSummaryApplicationService.Value.GetFinancialDataSummaryByGuarantor(id);

            GuarantorDetailViewModel viewModel = new GuarantorDetailViewModel();
            GuarantorDto data = this._guarantorApplicationService.Value.GetGuarantor(id);
            viewModel.Data = data;
            viewModel.VpGuarantorStatusId = (int)data.VpGuarantorStatus;
            viewModel.HasVisits = this._visitApplicationService.Value.GetVisitTotals(data.User.VisitPayUserId, new VisitFilterDto()) > 0;

            viewModel.NextPaymentDueDate = financialDataSummaryDto != null && financialDataSummaryDto.StatementNextPaymentDueDate.HasValue ? financialDataSummaryDto.StatementNextPaymentDueDate.Value.ToString(Format.DateFormat) : string.Empty;
            viewModel.IsAwaitingStatement = financialDataSummaryDto != null && financialDataSummaryDto.IsAwaitingStatement;
            viewModel.IsGracePeriod = financialDataSummaryDto != null && financialDataSummaryDto.IsGracePeriod;

            this.Session[UsernameKey] = data.User.UserName;
            this.Session[VpGuarantorIdKey] = data.VpGuarantorId;

            return this.View("GuarantorDetail", viewModel);
        }

        [HttpGet]
        [OverrideAuthorization, AutomationAuthorize]
        public ActionResult GetGuarantorNextStatementDate(int guarantorId)
        {
            GuarantorDto guarantorDto = this._guarantorApplicationService.Value.GetGuarantor(guarantorId);

            return this.Content(guarantorDto.NextStatementDate?.ToString("d"));
        }

        [HttpGet]
        [OverrideAuthorization, AutomationAuthorize]
        public ActionResult GetGuarantorNextPaymentDate(int guarantorId)
        {
            StatementDto currentStatement = this._statementApplicationService.Value.GetLastStatement(guarantorId);

            return this.Content(currentStatement?.PaymentDueDate.ToString("d"));
        }

        [HttpGet]
        [OverrideAuthorization, AutomationAuthorize]
        public ActionResult IsGracePeriod(int guarantorId)
        {
            StatementDto currentStatement = this._statementApplicationService.Value.GetLastStatement(guarantorId);

            return this.Content(currentStatement?.IsGracePeriod.ToString());
        }

        [HttpGet]
        [OverrideAuthorization, AutomationAuthorize]
        public ActionResult IsAwaitingStatement(int guarantorId)
        {
            StatementDto currentStatement = this._statementApplicationService.Value.GetLastStatement(guarantorId);
            return this.Content(this.IsAwaitingStatement(currentStatement).ToString());
        }

        [HttpGet]
        [OverrideAuthorization, AutomationAuthorize]
        public JsonResult StatementDataForGuarantor(int guarantorId)
        {
            StatementDto currentStatement = this._statementApplicationService.Value.GetLastStatement(guarantorId);
            GuarantorDto guarantorDto = this._guarantorApplicationService.Value.GetGuarantor(guarantorId);
            return this.Json(new
            {
                NextStatementDate = guarantorDto.NextStatementDate?.ToString("d"),
                NextPaymentDate = currentStatement?.PaymentDueDate.ToString("d"),
                currentStatement?.IsGracePeriod,
                IsAwaitingStatement = this.IsAwaitingStatement(currentStatement)
            }, JsonRequestBehavior.AllowGet);
        }

        private bool IsAwaitingStatement(StatementDto currentStatement)
        {
            return (currentStatement == null || (bool)!currentStatement?.IsGracePeriod);
        }

        [HttpGet]
        public async Task<JsonResult> GetGuarantorHealthEquityBalance(int guarantorId)
        {
            GuarantorDto guarantor = this._guarantorApplicationService.Value.GetGuarantor(guarantorId);
            SsoProviderEnum selectHealthEquitySsoProvider = this._ssoApplicationService.Value.SelectHealthEquitySsoProvider();
            HealthEquityBalanceDto balance = await this._guarantorApplicationService.Value.GetGuarantorHealthEquityBalanceAsnyc(guarantorId, guarantor.User.VisitPayUserId, selectHealthEquitySsoProvider);
            return this.Json(new
            {
                balance.CashBalance,
                balance.InvestmentBalance,
                balance.ContributionsYtd,
                balance.DistributionsYtd,
            }, JsonRequestBehavior.AllowGet);
        }

        [AcceptVerbs(HttpVerbs.Get)]
        [Common.Web.Attributes.SessionState(SessionStateBehavior.Required)]
        public ActionResult SsoGenerator(int id)
        {
            SsoGeneratorViewModel model = new SsoGeneratorViewModel();
            GuarantorDto data = this._guarantorApplicationService.Value.GetGuarantor(id);
            this.Session[UsernameKey] = data.User.UserName;
            this.Session[VpGuarantorIdKey] = data.VpGuarantorId;

            model.Guarantor = data;
            return this.View("SsoGeneration", model);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult MyChartSsoGenerate(SsoRequestModel model)
        {
            SsoResponseModel responseModel = new SsoResponseModel();
            responseModel.SsoUrl = string.Format("{0}Sso/MyChart?context={1}", this.ClientDto.AppGuarantorUrlHost, this._ssoApplicationService.Value.GenerateEncryptedSsoToken(model.VpGuarantorId, SsoProviderEnum.OpenEpic, model.SsoSsk, model.DateOfBirth));
            responseModel.UrlExpiration = DateTime.UtcNow.AddMinutes(this._applicationSettingService.Value.SsoTimeoutTokensAfterMinutes.Value).ToString("yyyy-MM-dd hh:mm:ss");
            return this.Json(responseModel, JsonRequestBehavior.AllowGet);
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult GuarantorDateAdjustmentsView(int guarantorId)
        {
            GuarantorDetailViewModel viewModel = new GuarantorDetailViewModel();
            GuarantorDto data = this._guarantorApplicationService.Value.GetGuarantor(guarantorId);
            viewModel.Data = data;
            viewModel.VpGuarantorStatusId = (int)data.VpGuarantorStatus;
            viewModel.HasVisits = this._visitApplicationService.Value.GetVisitTotals(data.User.VisitPayUserId, new VisitFilterDto()) > 0;
            return this.View("GuarantorDateAdjustments", viewModel);
        }

        [HttpPost]
        [OverrideAuthorization, AutomationAuthorize]
        public ActionResult RunDateAdjustments(DateAdjustmentViewModel model)
        {
            string[] results = this._visitPayUserIssueResultApplicationService.Value.AdjustAllGuarantorDates(model.guarantorId, model.datePart, model.adjustment);

            //right now we are only supporting roll back for VisitAging
            if (model.adjustment < 0)
            {
                int daysToRollBack = model.adjustment;
                if (model.datePart == "Month")
                {
                    DateTime dateInPast = DateTime.UtcNow.AddMonths(model.adjustment);
                    daysToRollBack = (DateTime.UtcNow - dateInPast).Days * -1;
                }
                this._qaAgingApplicationService.Value.AddVisitAgesWithRollback(model.guarantorId, daysToRollBack);
            }
            if (results == null)
            {
                return new EmptyResult();
            }
            return this.Json(results, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult ClearGuarantorData(int guarantorId)
        {
            GuarantorDto guarantor = this._guarantorApplicationService.Value.GetGuarantor(guarantorId);
            GuarantorViewModel model = new GuarantorViewModel
            {
                UserName = guarantor.User.UserName
            };

            return this.View("ClearGuarantorData", model);
        }

        [HttpPost]
        [OverrideAuthorization, AutomationAuthorize]
        public ActionResult RunClearGuarantorData(int guarantorId)
        {
            string[] results = this._visitPayUserIssueResultApplicationService.Value.ClearGuarantorData(guarantorId);

            if (results == null)
                return new EmptyResult();

            return this.Json(results, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        [OverrideAuthorization, AutomationAuthorize]
        public ActionResult SetAgingTier(string sourceSystemKey, int billingSystemId, int agingTier)
        {
            this._qaAgingApplicationService.Value.SetAgingTier(sourceSystemKey, billingSystemId, agingTier);
            return this.Json(true, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult ResolveMatches(int guarantorId)
        {
            GuarantorDto guarantor = this._guarantorApplicationService.Value.GetGuarantor(guarantorId);
            GuarantorViewModel model = new GuarantorViewModel
            {
                UserName = guarantor.User.UserName
            };

            return this.View("ResolveMatches", model);
        }

        [HttpPost]
        [OverrideAuthorization, AutomationAuthorize]
        public ActionResult RunResolveMatches(int guarantorId)
        {
            this._guarantorApplicationService.Value.ResolveMatches(guarantorId);

            return new EmptyResult();
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult CreateGuarantorView()
        {
            return this.View("CreateGuarantor", new GuarantorViewModel());
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult CreateGuarantor(GuarantorViewModel modelView)
        {
            CreateGuarantorResult data = this._guarantorRepository.Value.CreateGuarantor(modelView);
            return this.Json(data, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult KeepCurrent(string userName)
        {
            VisitPayUserDto userDto = this._visitPayUserApplicationService.Value.FindByName(userName);
            if (userDto != null)
            {
                GuarantorDto guarantorDto = this._visitPayUserApplicationService.Value.GetGuarantorFromVisitPayUserId(userDto.VisitPayUserId);
                VpGuarantorKeepCurrent keepCurrentEntity = this._guarantorKeepCurrentRepository.Value.GetById(guarantorDto.VpGuarantorId);

                KeepCurrentViewModel model = new KeepCurrentViewModel
                {
                    VpGuarantorId = guarantorDto.VpGuarantorId,
                    UserName = guarantorDto.User.UserName
                };

                if (keepCurrentEntity != null)
                {
                    model.KeepCurrentAnchor = keepCurrentEntity.KeepCurrentAnchor;
                    model.KeepCurrentOffset = keepCurrentEntity.KeepCurrentOffset;
                }

                return this.View(model);
            }

            this.ModelState.AddModelError("", "User Not Found");
            return this.View(new KeepCurrentViewModel());
        }

        [HttpPost]
        [OverrideAuthorization, AutomationAuthorize]
        public ActionResult RollBackStatementPaymentDueDateToYesterdayForGuarantor(int guarantorId)
        {
            StatementDto lastStatement = this._statementApplicationService.Value.GetLastStatement(guarantorId);
            if (lastStatement == null)
            {
                return new EmptyResult();
            }

            TimeSpan diff = DateTime.UtcNow.Date - lastStatement.PaymentDueDate;
            diff = diff.Subtract(new TimeSpan(1, 0, 0, 0));

            return this.RunDateAdjustments(new DateAdjustmentViewModel
            {
                guarantorId = guarantorId,
                datePart = "Day",
                adjustment = diff.Days
            });
        }

        [HttpPost]
        [OverrideAuthorization, AutomationAuthorize]
        public ActionResult RollBackStatementDateToYesterdayForGuarantor(int guarantorId)
        {
            StatementDto lastStatement = this._statementApplicationService.Value.GetLastStatement(guarantorId);
            if (lastStatement == null)
            {
                return new EmptyResult();
            }

            TimeSpan diff = DateTime.UtcNow.Date - lastStatement.PaymentDueDate.AddMonths(1).AddDays(-21);
            diff = diff.Subtract(new TimeSpan(1, 0, 0, 0));

            return this.RunDateAdjustments(new DateAdjustmentViewModel
            {
                guarantorId = guarantorId,
                datePart = "Day",
                adjustment = diff.Days
            });
        }

        [HttpPost]
        public ActionResult KeepCurrent(KeepCurrentViewModel model)
        {
            if (!this.ModelState.IsValid)
            {
                return this.View(model);
            }

            VpGuarantorKeepCurrent keepCurrentEntity = this._guarantorKeepCurrentRepository.Value.GetById(model.VpGuarantorId) ?? new VpGuarantorKeepCurrent { VpGuarantorId = model.VpGuarantorId };

            keepCurrentEntity.KeepCurrentAnchor = model.KeepCurrentAnchor;
            keepCurrentEntity.KeepCurrentOffset = model.KeepCurrentOffset;

            this._guarantorKeepCurrentRepository.Value.Insert(keepCurrentEntity);

            VisitPayUserDto userDto = this._guarantorApplicationService.Value.GetGuarantor(model.VpGuarantorId).User;

            return this.RedirectToAction("KeepCurrent", new { userName = userDto.UserName });
        }

        [HttpPost]
        public ActionResult CloneGuarantor(int vpGuarantorId, string appendValue, bool consolidate)
        {
            if (appendValue.IsNullOrEmpty())
            {
                return new EmptyResult();
            }
            this._cloneGuarantorApplicationService.Value.CloneGuarantor(vpGuarantorId, appendValue, "", consolidate);
            return this.Json("Message sent to clone guarantor.  Fingers crossed it works!");
        }

        [HttpGet]
        public ActionResult PaymentMethods(string userName)
        {
            VisitPayUserDto userDto = this._visitPayUserApplicationService.Value.FindByName(userName);
            int guarantorId = this._visitPayUserApplicationService.Value.GetGuarantorIdFromVisitPayUserId(userDto.VisitPayUserId);

            List<PaymentMethodDto> paymentMethods = this._paymentMethodsApplicationService.Value.GetPaymentMethods(guarantorId, userDto.VisitPayUserId).OrderByDescending(x => x.IsPrimary).ThenBy(x => x.PaymentMethodId).ToList();

            PaymentMethodsViewModel model = new PaymentMethodsViewModel
            {
                UserName = userName,
                PaymentMethods = new List<string>(),
                IsAchEnabled = this._featureService.Value.IsFeatureEnabled(VisitPayFeatureEnum.PaymentsAch),
                IsCreditCardEnabled = this._featureService.Value.IsFeatureEnabled(VisitPayFeatureEnum.PaymentsCreditCard)
            };

            paymentMethods.ForEach(x => model.PaymentMethods.Add(string.Format("{0} X-{1} {2}", !string.IsNullOrEmpty(x.AccountNickName) ? x.AccountNickName : x.PaymentMethodType.GetDescription(), x.LastFour, x.IsPrimary ? "(Primary)" : "")));
            model.PaymentMethodsCount = paymentMethods.Count;

            if (!model.PaymentMethods.Any())
            {
                model.TestPaymentMethodAchCheckingGood = true;
                model.TestPaymentMethodAchSavingsGood = false;

                model.TestPaymentMethodVisaGood = true;
                model.TestPaymentMethodAmexGood = false;
                model.TestPaymentMethodMastercardGood = false;
                model.TestPaymentMethodDiscoverGood = false;
                model.TestPaymentMethodDecline = true;
                model.TestPaymentMethodCall = false;
                model.TestPaymentMethodCardError = false;
                model.TestPaymentMethodVisaGoodExpiringThisMonth = false;
                model.TestPaymentMethodVisaGoodExpiringNextMonth = false;
                model.TestPaymentMethodVisaGoodExpiredLastMonth = false;
            }

            return this.View(model);
        }

        [OverrideAuthorization, AutomationAuthorize]
        public async Task<ActionResult> PaymentMethodsAddTestAccounts(PaymentMethodsViewModel model)
        {
            VisitPayUserDto userDto;
            GuarantorDto guarantorDto;
            bool redirect = true;

            if (!string.IsNullOrEmpty(model.UserName))
            {
                // ui uses this
                userDto = this._visitPayUserApplicationService.Value.FindByName(model.UserName);
                guarantorDto = this._guarantorApplicationService.Value.GetGuarantor(model.UserName);
            }
            else if (model.GuarantorId.HasValue)
            {
                // automation test will set this
                guarantorDto = this._guarantorApplicationService.Value.GetGuarantor(model.GuarantorId.Value);
                userDto = guarantorDto.User;
                redirect = false;
            }
            else
            {
                return this.RedirectToAction("PaymentMethods", new { model.UserName });
            }

            IList<TestPaymentMethodCard> testCards = new List<TestPaymentMethodCard>();
            Action<bool, TestPaymentMethodCard> addCard = (b, p) =>
            {
                if (b) testCards.Add(p);
            };

            Func<PaymentMethodDto, string> formatName = pm => $"{(!string.IsNullOrEmpty(pm.AccountNickName) ? pm.AccountNickName : pm.PaymentMethodType.GetDescription())} X-{pm.LastFour} {(pm.IsPrimary ? "(Primary)" : "")}";

            IList<PaymentMethodDto> existingPaymentMethods = this._paymentMethodsApplicationService.Value.GetPaymentMethods(guarantorDto.VpGuarantorId, userDto.VisitPayUserId);

            if (this._featureService.Value.IsFeatureEnabled(VisitPayFeatureEnum.PaymentsCreditCard))
            {
                addCard(model.TestPaymentMethodVisaGood, new TestPaymentMethodVisaGood(userDto, guarantorDto));
                addCard(model.TestPaymentMethodVisaGoodExpiredLastMonth, new TestPaymentMethodVisaGoodExpiredLastMonth(userDto, guarantorDto));
                addCard(model.TestPaymentMethodVisaGoodExpiringThisMonth, new TestPaymentMethodVisaGoodExpiringThisMonth(userDto, guarantorDto));
                addCard(model.TestPaymentMethodVisaGoodExpiringNextMonth, new TestPaymentMethodVisaGoodExpiringNextMonth(userDto, guarantorDto));
                addCard(model.TestPaymentMethodMastercardGood, new TestPaymentMethodMastercardGood(userDto, guarantorDto));
                addCard(model.TestPaymentMethodAmexGood, new TestPaymentMethodAmexGood(userDto, guarantorDto));
                addCard(model.TestPaymentMethodDiscoverGood, new TestPaymentMethodDiscoverGood(userDto, guarantorDto));
                addCard(model.TestPaymentMethodDecline, new TestPaymentMethodDecline(userDto, guarantorDto));
                addCard(model.TestPaymentMethodCall, new TestPaymentMethodCall(userDto, guarantorDto));
                addCard(model.TestPaymentMethodCardError, new TestPaymentMethodCardError(userDto, guarantorDto));

                if (model.PaymentMethodsCount == 0 && testCards.Count > 0)
                {
                    testCards.First().IsPrimary = true;
                }

                foreach (TestPaymentMethodCard testCard in testCards.Where(x => !existingPaymentMethods.Select(y => formatName(y)).Contains(formatName(x))))
                {
                    await this._testPaymentMethodsApplicationService.Value.SavePaymentMethodAsync(testCard, testCard.CreatedUserId, testCard.IsPrimary, testCard.AccountNumber, testCard.SecurityNumber).ConfigureAwait(true);
                }
            }

            if (this._featureService.Value.IsFeatureEnabled(VisitPayFeatureEnum.PaymentsAch))
            {
                IList<TestPaymentMethodAch> testAchs = new List<TestPaymentMethodAch>();
                Action<bool, TestPaymentMethodAch> addAch = (b, p) =>
                {
                    if (b) testAchs.Add(p);
                };
                addAch(model.TestPaymentMethodAchCheckingGood, new TestPaymentMethodAchCheckingGood(userDto, guarantorDto));
                addAch(model.TestPaymentMethodAchSavingsGood, new TestPaymentMethodAchSavingsGood(userDto, guarantorDto));

                if (testAchs.Count > 0)
                {
                    testAchs.First().IsPrimary = true;
                }

                foreach (TestPaymentMethodAch testAch in testAchs.Where(x => !existingPaymentMethods.Select(y => formatName(y)).Contains(formatName(x))))
                {
                    await this._testPaymentMethodsApplicationService.Value.SavePaymentMethodAsync(testAch, testAch.CreatedUserId, testAch.IsPrimary, testAch.AccountNumber, testAch.RoutingNumber).ConfigureAwait(true);
                }
            }

            // from ui
            if (redirect)
            {
                return this.RedirectToAction("PaymentMethods", new { model.UserName });
            }

            // from automation
            return new ContentResult { Content = "Payment Method Added" };
        }

        public class JqGridObject
        {
            public int page { get; set; }
            public int total { get; set; }
            public int records { get; set; }
            public int PageSize { get; set; }
            public string SortColumn { get; set; }
            public string SortOrder { get; set; }
            public IList<GuarantorDto> Data { get; set; }
        }

        [HttpGet]
        public PartialViewResult ExportPersonas()
        {
            return this.PartialView("_ExportPersonas", new ExportPersonasViewModel());
        }

        [HttpPost]
        public ActionResult ExportPersonas(ExportPersonasViewModel model)
        {
            byte[] bytes = this._personasApplicationService.Value.ExportPersonas(model.Search);
            if (bytes != null)
            {
                this.WriteExcelInline(bytes, "Personas.xlsx");
            }

            return this.RedirectToAction("Index", "Home");
        }

        [OverrideAuthorization, AutomationAuthorize]
        public async Task<ActionResult> QueueStatementForGuarantor(int vpGuarantorId)
        {
            await this._bus.Value.PublishMessage(new CreateStatementMessage { VpGuarantorId = vpGuarantorId, DateToProcess = DateTime.UtcNow.Date, ImmediateStatement = false });

            return new EmptyResult();
        }

        [OverrideAuthorization, AutomationAuthorize]
        public async Task<ActionResult> DateAdjustAndQueueStatementForGuarantor(int vpGuarantorId)
        {
            this.RollBackStatementDateToYesterdayForGuarantor(vpGuarantorId);
            await this._bus.Value.PublishMessage(new CreateStatementMessage { VpGuarantorId = vpGuarantorId, DateToProcess = DateTime.UtcNow.Date, ImmediateStatement = false });

            return new EmptyResult();
        }

        [OverrideAuthorization, AutomationAuthorize]
        public async Task<ActionResult> QueuePaymentsForGuarantor(int vpGuarantorId)
        {
            this.RollBackStatementPaymentDueDateToYesterdayForGuarantor(vpGuarantorId);

            StatementDto statementDto = this._statementApplicationService.Value.GetLastStatement(vpGuarantorId);

            this._statementService.Value.MarkStatementAsQueued(statementDto.VpStatementId, vpGuarantorId, "QATools GuarantorsController::QueuePaymentsForGuarantor");
            await this._bus.Value.PublishMessage(new ProcessScheduledPaymentWithGuarantorMessage { VpGuarantorId = vpGuarantorId, DateToProcess = DateTime.UtcNow.Date });

            return new EmptyResult();
        }

        [OverrideAuthorization, AutomationAuthorize]
        public async Task<ActionResult> ProcessPaymentIndividualGuarantorToday(int vpGuarantorId)
        {
            await this._bus.Value.PublishMessage(new ProcessScheduledPaymentWithGuarantorMessage { VpGuarantorId = vpGuarantorId, DateToProcess = DateTime.UtcNow.Date });

            return new EmptyResult();
        }

        [OverrideAuthorization, AutomationAuthorize]
        public ActionResult ImportLockBoxPayment(int vpGuarantorId, decimal lockBoxPaymentAmount, DateTime financePlanPeriodStartDate)
        {
            IList<ProcessPaymentResponse> processPaymentResponses = this._paymentSubmissionApplicationService.Value.ImportLockBoxFinancePlanPayment(lockBoxPaymentAmount, financePlanPeriodStartDate, -1, vpGuarantorId);
            ProcessPaymentResponse error = processPaymentResponses.FirstOrDefault(x => x.IsError);
            if (error != null)
            {
                return this.Content(error.ErrorMessage);
            }
            return this.Content("Lock Box payment was successfully processed");
        }
    }
}
﻿namespace Ivh.Web.QATools.Controllers
{
    using System;
    using System.Web.Mvc;
    using Common.Web.Interfaces;

    public class VPPUsersController : BaseController
    {

        public VPPUsersController(Lazy<IBaseControllerService> baseControllerService)
            : base(baseControllerService)
        {
        }


        public ActionResult ViewGuarantors()
        {
            return View();
        }

        public ActionResult CreateGuarantor()
        {
            return View();
        }

    }
}
﻿namespace Ivh.Web.QATools.Controllers
{
    using System;
    using Common.Web.Interfaces;
    using System.Web.Mvc;

    public class ElmahController : BaseController
    {
        public ElmahController(Lazy<IBaseControllerService> baseControllerService)
            : base(baseControllerService)
        {
        }


        public ActionResult Index()
        {
            return this.View();
        }
    }
}
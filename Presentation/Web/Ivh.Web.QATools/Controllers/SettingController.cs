﻿namespace Ivh.Web.QATools.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Web.Mvc;
    using System.Web.SessionState;
    using Application.Core.Common.Dtos;
    using Application.Core.Common.Dtos.Eob;
    using Application.Core.Common.Interfaces;
    using Application.Settings.Common.Interfaces;
    using Ivh.Application.Qat.Common.Interfaces;
    using Ivh.Common.Web.Interfaces;
    using Models.Setting;
    
    [Authorize]
    public class SettingController : BaseController
    {
        private readonly Lazy<ISettingsApplicationService> _applicationSettingsService;
        private readonly Lazy<IFacilityApplicationService> _facilityApplicationService;
        private readonly Lazy<IQaVisitApplicationService> _demoExternalLinkApplicationService;
        private readonly Lazy<IQaSettingsApplicationService> _qaSettingsApplicationService;
        private readonly Lazy<IQaImageApplicationService> _qaImageApplicationService;
        private readonly Lazy<IQaFacilityApplicationService> _qaFacilityApplicationService;

        public SettingController(
            Lazy<IBaseControllerService> baseControllerService,
            Lazy<ISettingsApplicationService> applicationSettingsService,
            Lazy<IFacilityApplicationService> facilityApplicationService,
            Lazy<IQaVisitApplicationService> demoExternalLinkApplicationService,
            Lazy<IQaSettingsApplicationService> qaSettingsApplicationService,
            Lazy<IQaImageApplicationService> qaImageApplicationService,
            Lazy<IQaFacilityApplicationService> qaFacilityApplicationService
        ) : base(baseControllerService)
        {
            this._applicationSettingsService = applicationSettingsService;
            this._facilityApplicationService = facilityApplicationService;
            this._demoExternalLinkApplicationService = demoExternalLinkApplicationService;
            this._qaSettingsApplicationService = qaSettingsApplicationService;
            this._qaImageApplicationService = qaImageApplicationService;
            this._qaFacilityApplicationService = qaFacilityApplicationService;
        }

        const string UploadResultKey = "UploadResult";

        public async Task<ActionResult> Index(bool filterDemo = false)
        {
            IReadOnlyDictionary<string, string> allManagedSettings = await this._applicationSettingsService.Value.GetAllManagedSettingsAsync();
            IReadOnlyDictionary<string, string> allEditableManagedSettings = await this._applicationSettingsService.Value.GetAllEditableManagedSettingsAsync();
            
            if (filterDemo)
            {
                allManagedSettings = this.FilterDemo(allManagedSettings);
                allEditableManagedSettings = this.FilterDemo(allEditableManagedSettings);
            }
            
            SettingViewModel model = new SettingViewModel()
            {
                ManagedSettings = allManagedSettings,
                EditableManagedSettings = allEditableManagedSettings
            };

            return this.View(model);
        }

        [HttpPost]
        public async Task<ActionResult> OverrideManagedSetting(string settingName, string settingValue)
        {
            bool result = await this._applicationSettingsService.Value.OverrideManagedSettingAsync(settingName, settingValue);
            return this.Json(result);
        }

        [HttpPost]
        public async Task<ActionResult> ClearManagedSettingOverrides()
        {
            bool result = await this._applicationSettingsService.Value.ClearManagedSettingOverridesAsync();
            return this.Json(result);
        }

        [HttpPost]
        public async Task<ActionResult> ClearManagedSettingOverride(string key)
        {
            bool result = await this._applicationSettingsService.Value.ClearManagedSettingOverrideAsync(key);
            return this.Json(result);
        }

        [HttpPost]
        [Common.Web.Attributes.SessionState(SessionStateBehavior.Required)]
        public async Task<ActionResult> UpdateLogoSetting(LogoSettingViewModel logoSettingViewModel)
        {
            byte[] fileInBytes;
            using (BinaryReader theReader = new BinaryReader(logoSettingViewModel.LogoFile.InputStream))
            {
                fileInBytes = theReader.ReadBytes(logoSettingViewModel.LogoFile.ContentLength);
                theReader.Close();
            }
            
            bool result = await this._qaSettingsApplicationService.Value.ChangeClientLogo(logoSettingViewModel.LogoFile.FileName, fileInBytes);
            
            this.SetTempData(UploadResultKey, result ? "success" : "fail");

            return this.RedirectToAction("Index");
        }

        public async Task<ActionResult> ThemeSettings(string colorPaletteName = "")
        {
            return await Task.Run(() =>
            {
                DemoSettingsViewModel demoSetttings = new DemoSettingsViewModel(colorPaletteName);
                return this.View(demoSetttings);
            });
        }

        [HttpPost]
        public async Task<ActionResult> UpdateThemeSettings(List<SettingKeyValueViewModel> settingKeyValues)
        {
            StringBuilder result = new StringBuilder();
            foreach (SettingKeyValueViewModel settingKeyValue in settingKeyValues)
            {
                bool updated = await this._applicationSettingsService.Value.OverrideManagedSettingAsync(settingKeyValue.SettingKey, settingKeyValue.SettingValueHex);
                string message = (updated) ? "successfully updated" : "<strong>did not update</strong>";
                result.AppendLine($"{settingKeyValue.SettingKey} {message} <br />");
            }
            return this.Json(result.ToString());
        }

        [HttpGet]
        public PartialViewResult SelectDemoPayer()
        {
            IList<EobExternalLinkDto> eobExternalLinkDtos = this._demoExternalLinkApplicationService.Value.GetEobExternalLinks();
            
            return this.PartialView("_SelectDemoPayer", eobExternalLinkDtos.OrderBy(x => x.Text).ToList());
        }
        
        [HttpPost]
        public ActionResult UpdateDemoPayer(int eobExternalLinkId)
        {
            bool result = this._demoExternalLinkApplicationService.Value.UpdateDemoEobPayerExternalLink(eobExternalLinkId);

            return this.Json(result);
        }
        
        private IReadOnlyDictionary<string, string> FilterDemo(IReadOnlyDictionary<string, string> dictionary)
        {
            List<string> demoSettings = new List<string>
            {
                "/theme.client-primary",
                "/theme.client-accent",
                "/theme.client-button-primary-bg",
                "/theme.client-button-primary-bg-hover",
                "/theme.client-button-payment-bg",
                "/theme.client-button-payment-bg-hover",
                "/theme.client-link-color",
                "/theme.client-link-hover-color",
                "/theme.client-logo-width",
                "/theme.client-payment-icon-bg",
                "/theme-print.statement-border-color",
                "/theme-print.statement-header1-bg",
                "/theme-print.statement-header2-bg",
                "/theme-print.support-request-header",
                "ClientAddressSingleLine",
                "FinancialAssistanceSupportPhone",
                "FinancialAssistanceUrlDisplay",
                nameof(ClientDto.ClientBrandName),
                nameof(ClientDto.ClientFullName),
                nameof(ClientDto.ClientFullNameUpperCase),
                nameof(ClientDto.ClientFullNameAlt),
                nameof(ClientDto.ClientFullNameAltUpperCase),
                nameof(ClientDto.ClientName),
                nameof(ClientDto.ClientNameUpperCase),
                nameof(ClientDto.ClientShortName),
                nameof(ClientDto.FinancialAssistanceUrl),
                nameof(ClientDto.FeatureDemoClientReportDashboardIsEnabled),
                nameof(ClientDto.FeatureDemoHealthEquityIsEnabled),
                nameof(ClientDto.FeatureDemoInsuranceAccrualUiIsEnabled),
                nameof(ClientDto.FeatureDemoMyChartSsoUiIsEnabled),
                nameof(ClientDto.FeatureDemoPreServiceUiIsEnabled),
                nameof(ClientDto.GuestPayClientBrandName),
                nameof(ClientDto.GuestPaySupportFromEmail),
                nameof(ClientDto.GuestPaySupportFromName),
                nameof(ClientDto.GuestPaySupportPhone),
                nameof(ClientDto.SupportFromEmail),
                nameof(ClientDto.SupportFromName),
                nameof(ClientDto.SupportPhone),
                nameof(ClientDto.SupportRequestCasePrefix),
                nameof(ClientDto.VpGuarantorPatientIdentifier),
                nameof(ClientDto.VpGuarantorPatientText)
            };

            return dictionary.Where(x => demoSettings.Contains(x.Key)).ToDictionary(d => d.Key, d => d.Value);
        }

        [HttpGet]
        public ActionResult UpdateFacilities()
        {
            IEnumerable<FacilityDto> facilityDtos = this._facilityApplicationService.Value.GetAllFacilities()
                .Where(x => x.FacilityId < 0)
                .OrderByDescending(x => x.FacilityId); // demo facilities identified as negative id's

            List<FacilityUpdateViewModel> models = facilityDtos.Select(x => new FacilityUpdateViewModel
                {
                    FacilityId = x.FacilityId,
                    FacilityName = x.FacilityDescription,
                    LogoFileName = string.IsNullOrWhiteSpace(x.LogoFilename) ? string.Empty :  $"/content/client/{x.LogoFilename}",
                    SourceSystemKey = x.SourceSystemKey
                })
                .ToList();

            return this.PartialView("_UpdateFacilities", models);
        }

        [HttpPost]
        public async Task<ActionResult> UpdateDemoFacility(FacilityUpdateViewModel model)
        {
            byte[] bytes = null;
            if (model.LogoFile != null)
            {
                using (BinaryReader reader = new BinaryReader(model.LogoFile.InputStream))
                {
                    bytes = reader.ReadBytes(model.LogoFile.ContentLength);
                    reader.Close();
                }
            }

            string uploadedFilename = null;
            bool updateImage = bytes != null;
            if (updateImage)
            {
                uploadedFilename = await this._qaImageApplicationService.Value.UploadImageAsync(model.LogoFile.FileName, bytes);
            }

            this._qaFacilityApplicationService.Value.UpdateFacility(model.FacilityId, model.FacilityName, updateImage, uploadedFilename);
            
            return this.RedirectToAction("Index");
        }
    }
}
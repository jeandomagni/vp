﻿namespace Ivh.Web.QATools.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web.Mvc;
    using Application.FinanceManagement.Common.Dtos;
    using Application.FinanceManagement.Common.Interfaces;
    using Common.Base.Enums;
    using Common.Base.Utilities.Extensions;
    using Common.VisitPay.Enums;
    using Common.VisitPay.Strings;
    using Common.Web.Interfaces;
    using Models;

    using Ivh.Common.Cache;

    [Authorize(Roles = VisitPayRoleStrings.System.QATools + "," + VisitPayRoleStrings.Admin.Administrator)]
    public class ThirdPartyController : BaseController
    {
        private readonly Lazy<IAchApplicationService> _achApplicationService;

        public ThirdPartyController(
            Lazy<IBaseControllerService> baseControllerService,
            Lazy<IAchApplicationService> achApplicationService)
            : base(baseControllerService)
        {
            this._achApplicationService = achApplicationService;
        }

        // GET: /Account/Register
        [AllowAnonymous]
        public ActionResult Ach()
        {

            ThirdPartyViewModel model = new ThirdPartyViewModel
            {
                DownloadDate = DateTime.Today,
                AchFileTypes = new List<SelectListItem>(),
                AchReturnCodes = new List<SelectListItem>(),
                Process = true
            };
            model.AchFileTypes.AddRange(Enum.GetValues(typeof(AchFileTypeEnum)).Cast<AchFileTypeEnum>().Select(s => new SelectListItem { Value = ((int)s).ToString(), Text = s.ToString() }).ToList());
            model.AchReturnCodes.AddRange(Enum.GetValues(typeof(AchReturnCodeEnum)).Cast<AchReturnCodeEnum>()
                .OrderBy(x => x)
                .Select(s => new SelectListItem { Value = ((int)s).ToString(), Text = s.ToString(), Selected = s == AchReturnCodeEnum.None}).ToList());

            return View(model);
        }

        //
        // POST: /Account/Login
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult Ach(ThirdPartyViewModel model)
        {
            if (!this.ModelState.IsValid)
            {
                return View(model);
            }

            AchFileDto achFileDto = new AchFileDto
            {
                AchFileType = model.AchFileType,
                DownloadedDate = model.DownloadDate.Date
            };
            achFileDto.FileDateFrom = achFileDto.DownloadedDate.Date;
            achFileDto.FileDateTo = achFileDto.DownloadedDate.Date;
            achFileDto.FileReferenceToken = Guid.NewGuid();
            achFileDto.ProcessedDate = null;

            foreach (var transactionId in model.TransactionIds.Trim().Split(new char[] {',', ';', ':'}))
            {
                switch (achFileDto.AchFileType)
                {
                    case AchFileTypeEnum.Return:
                        AchReturnDetailDto achReturnDetailDto = new AchReturnDetailDto
                        {
                            AchFile = achFileDto,
                            DownloadedDate = achFileDto.DownloadedDate,
                            ProcessedDate = null,
                            IndividualId = transactionId.Trim(),
                            ReturnReasonCode = this._achApplicationService.Value.GetReturnCode(model.ReturnReasonCode),
                            EffectiveDate = achFileDto.DownloadedDate.Date.ToString("yyMMdd"),
                            Amount = ((int)(model.ReturnAmount * 100)).ToString()
                        };
                        achFileDto.ReturnDetails = new List<AchReturnDetailDto> {achReturnDetailDto};
                        break;
                    case AchFileTypeEnum.Settlement:
                        AchSettlementDetailDto achSettlementDetailDto = new AchSettlementDetailDto
                        {
                            AchFile = achFileDto,
                            DownloadedDate = achFileDto.DownloadedDate,
                            ProcessedDate = null,
                            IndividualId = transactionId.Trim(),
                            Status = model.ReturnReasonCode == AchReturnCodeEnum.None ? null : "Return",
                            ReturnCode = this._achApplicationService.Value.GetReturnCode(model.ReturnReasonCode),
                            ProcessingDate = achFileDto.DownloadedDate.Date.ToString("yyMMdd")
                        };
                        achFileDto.SettlementDetails = new List<AchSettlementDetailDto> {achSettlementDetailDto};
                        break;
                }

                this._achApplicationService.Value.AddAchFile(achFileDto);
            }

            if (model.Process)
            {
                this._achApplicationService.Value.PublishAchReturnsAndSettlements(model.DownloadDate.Date, model.DownloadDate.Date.AddDays(1));
            }

            return this.RedirectToAction("Ach", "ThirdParty");
        }

        // GET: /Account/Register
        [AllowAnonymous]
        public ActionResult FailPayment()
        {
            FailPaymentViewModel model = new FailPaymentViewModel
            {
                FailPaymentErrorCodes = Enum.GetValues(typeof(FailedPaymentErrorCodeEnum)).Cast<FailedPaymentErrorCodeEnum>().Select(s => new SelectListItem { Value = ((int)s).ToString(), Text = s.ToString() }).ToList()
            };

            IDistributedCache cache = DependencyResolver.Current.GetService<IDistributedCache>();
            string failPaymentErrorCodeString = cache.GetStringAsync("FailedPaymentErrorCodeEnum").Result;
            FailedPaymentErrorCodeEnum failPaymentErrorCode = FailedPaymentErrorCodeEnum.Unknown;

            if (Enum.TryParse<FailedPaymentErrorCodeEnum>(failPaymentErrorCodeString, true, out failPaymentErrorCode))
            {
                model.FailPaymentErrorCode = failPaymentErrorCode;
            }

            return this.View(model);
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult FailPayment(FailPaymentViewModel model)
        {
            IDistributedCache cache = DependencyResolver.Current.GetService<IDistributedCache>();
            bool success = cache.SetStringAsync("FailedPaymentErrorCodeEnum", model.FailPaymentErrorCode.ToString()).Result;

            return this.RedirectToAction("FailPayment");
        }
    }
}
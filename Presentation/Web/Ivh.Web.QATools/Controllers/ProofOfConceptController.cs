﻿namespace Ivh.Web.QATools.Controllers
{
    using System;
    using System.Collections;
    using System.Web.Mvc;
    using Common.Web.Models.Payment;
    using Microsoft.Reporting.WebForms;

    public class ProofOfConceptController : Controller
    {
        // GET: ProofOfConcept
        public ActionResult Index()
        {
            return this.View(new CardAccountViewModel());
        }


        public ActionResult SsrsReport()
        {

            ReportViewer reportViewer = new ReportViewer();
            reportViewer.ProcessingMode = ProcessingMode.Remote;
            reportViewer.SizeToReportContent = true;
            //reportViewer.Width = Unit.Percentage(900);
            //reportViewer.Height = Unit.Percentage(900);
            //reportViewer.DocumentMapWidth = Unit.Percentage(100);
            reportViewer.ServerReport.ReportServerUrl = new Uri("http://jhunn2/ReportServer_MSSQLDEV"); //Set the ReportServer Url
            reportViewer.ServerReport.ReportPath = "/Reports/Report2"; //

            ArrayList reportParam = this.ReportDefaultParam();
            ReportParameter[] param = new ReportParameter[reportParam.Count];
            for (int k = 0; k < reportParam.Count; k++)
            {
                param[k] = (ReportParameter)reportParam[k];
            }

            reportViewer.ServerReport.SetParameters(param); //Set Report Parameters
            reportViewer.ServerReport.Refresh();

            ViewBag.ReportViewer = reportViewer;

            return View();
        }

        private ArrayList ReportDefaultParam()
        {
            ArrayList arrLstDefaultParam = new ArrayList();
            arrLstDefaultParam.Add(this.CreateReportParameter("ReportTitle", "Title of Report"));
            arrLstDefaultParam.Add(this.CreateReportParameter("ReportSubTitle", "Sub Title of Report"));
            return arrLstDefaultParam;
        }

        private ReportParameter CreateReportParameter(string paramName, string pramValue)
        {
            ReportParameter aParam = new ReportParameter(paramName, pramValue);
            return aParam;
        }
    }
}
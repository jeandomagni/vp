﻿namespace Ivh.Web.QATools.Controllers
{
    using System;
    using System.IO;
    using System.Linq;
    using System.Text;
    using System.Web;
    using System.Web.Mvc;
    using System.Xml;
    using System.Xml.Serialization;
    using Domain.HospitalData.Scoring.Entities;
    using Domain.Settings.Interfaces;
    using Interfaces;
    using Models;

    public class MockThirdPartyController : Controller
    {
        private readonly Lazy<IScoringThirdPartyRepository> _repository;
        private readonly Lazy<IApplicationSettingsService> _applicationSettingsService;

        public MockThirdPartyController(Lazy<IScoringThirdPartyRepository> repository,
                                        Lazy<IApplicationSettingsService> applicationSettingsService)
        {
            this._repository = repository;
            this._applicationSettingsService = applicationSettingsService;
        }

        [HttpPost]
        public ActionResult RealTime()
        {
            ThirdPartyRequestInput requestInput = this.GenerateRequestInput(this.GetDocumentContents());
            ScoringThirdPartyResponse responseModel = this._repository.Value.GetQueryable().FirstOrDefault(r =>
                r.FirstName.Equals(requestInput.FirstName) &&
                r.LastName.Equals(requestInput.LastName) &&
                r.AddressLine1.Equals(requestInput.AddressLine1) &&
                r.StateProvince.Equals(requestInput.State) &&
                r.MessageId.Equals(requestInput.MessageId) &&
                r.PostalCode.Equals(requestInput.ZipCode));
            if (responseModel?.ReturnErrorCode != null)
            {
                this.Response.StatusCode = (int)responseModel.ReturnErrorCode;
                throw new HttpException((int)responseModel.ReturnErrorCode, $"Expected error {responseModel.ReturnErrorCode} for ID: {responseModel.ScoringThirdPartyResponseId}.");
            }
            if (!string.IsNullOrEmpty(responseModel?.Response))
            {
                return this.Content(responseModel.Response, "application/xml");
            }
            return this.Content(this.GenerateRandomData(!string.IsNullOrEmpty(responseModel?.Response), requestInput.MessageId, requestInput.Userfield1), "application/xml");
        }

        private string GetDocumentContents()
        {
            string documentContents;
            using (Stream receiveStream = this.Request.InputStream)
            {
                using (StreamReader readStream = new StreamReader(receiveStream, Encoding.UTF8))
                {
                    documentContents = readStream.ReadToEnd();
                }
            }
            return documentContents;
        }

        private string GenerateRandomData(bool emptyData, string messageId, int dataset)
        {
            Random rnd = new Random();
            decimal paidProb = emptyData ? 0 : this.GeneratePaidProb();
            int writeOffs = emptyData ? 0 : rnd.Next(1, 9999);
            int writeOffsContinuities = emptyData ? 0 : rnd.Next(1, 999);
            int creditCardPayments1Year = emptyData ? 0 : rnd.Next(1, 999);
            int creditCardPayments5Year = emptyData ? 0 : rnd.Next(1, 9999);
            int continuityContracts = emptyData ? 0 : rnd.Next(1, 999);
            char income = emptyData ? '\0' : this.GenerateIncome();
            int memberCount = emptyData ? 0 : rnd.Next(1, 99);
            int ownRent = emptyData ? 0 : rnd.Next(0, 9);
            string xmlns = this._applicationSettingsService.Value.ScoringThirdPartyXmlNamespace.Value;

            string dataset1 = $@"<?xml version=""1.0"" encoding=""UTF-8""?>
                                <RTResponse xmlns=""{xmlns}"" xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xsi:schemaLocation=""{xmlns}"">
                                    <ResponseType>S</ResponseType>
                                    <MessageID>{messageId}</MessageID>
                                    <VALUE NAME=""PAID_PROB"">{paidProb}</VALUE>
                                    <VALUE NAME=""IN_TOT_TOT_BILL_LVL_WO"">{writeOffs}</VALUE>
                                    <VALUE NAME=""IN_CONT_TOT_WO_COUNT"">{writeOffsContinuities}</VALUE>
                                    <VALUE NAME=""IN_TOT_Y1_PAY_COUNT_CC"">{creditCardPayments1Year}</VALUE>
                                    <VALUE NAME=""IN_TOT_TOT_PAY_COUNT_CC"">{creditCardPayments5Year}</VALUE>
                                    <VALUE NAME=""IN_TOT_TOT_CT_CONT_WO"">{continuityContracts}</VALUE>
                                    <VALUE NAME=""HH_INCOME"">{income}</VALUE>
                                    <VALUE NAME=""HH_MEMBER_COUNT"">{memberCount}</VALUE>
                                    <VALUE NAME=""OWN_RENT"">{ownRent}</VALUE>
                                    <VALUE NAME=""IMPUTED_HH_INCOME"">1</VALUE>
                                    <VALUE NAME=""IMPUTED_HH_MEMBER_COUNT"">1</VALUE>
                                </RTResponse>";

            string dataset2 = $@"<?xml version=""1.0"" encoding=""UTF-8""?>
                                <RTResponse xmlns=""{xmlns}"" xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xsi:schemaLocation=""{xmlns}"">
                                    <ResponseType>S</ResponseType>
                                    <MessageID>{messageId}</MessageID>
                                    <VALUE NAME=""PAID_PROB"">{paidProb}</VALUE>
                                    <VALUE NAME=""IN_TOT_TOT_BILL_LVL_WO"">{writeOffs}</VALUE>
                                    <VALUE NAME=""IN_CONT_TOT_WO_COUNT"">{writeOffsContinuities}</VALUE>
                                    <VALUE NAME=""IN_TOT_Y1_PAY_COUNT_CC"">{creditCardPayments1Year}</VALUE>
                                    <VALUE NAME=""IN_TOT_TOT_PAY_COUNT_CC"">{creditCardPayments5Year}</VALUE>
                                    <VALUE NAME=""IN_TOT_TOT_CT_CONT_WO"">{continuityContracts}</VALUE>
                                    <VALUE NAME=""OWN_RENT"">{ownRent}</VALUE>
                                </RTResponse>";

            string dataset3 = $@"<?xml version=""1.0"" encoding=""UTF-8""?>
                                <RTResponse xmlns=""{xmlns}"" xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xsi:schemaLocation=""{xmlns}"">
                                    <ResponseType>S</ResponseType>
                                    <MessageID>{messageId}</MessageID>
                                    <VALUE NAME=""HH_INCOME"">{income}</VALUE>
                                    <VALUE NAME=""HH_MEMBER_COUNT"">{memberCount}</VALUE>
                                    <VALUE NAME=""IMPUTED_HH_INCOME"">1</VALUE>
                                    <VALUE NAME=""IMPUTED_HH_MEMBER_COUNT"">1</VALUE>
                                </RTResponse>";

            string result;
            switch (dataset)
            {
                case 1:
                    result = dataset1;
                    break;
                case 2:
                    result = dataset2;
                    break;
                case 3:
                    result = dataset3;
                    break;
                default:
                    result = dataset1;
                    break;
            }

            return result;
        }

        private decimal GeneratePaidProb()
        {
            Random rnd = new Random();
            decimal paidProb = rnd.Next(1, 100) / (decimal)100;

            return paidProb;
        }

        private char GenerateIncome()
        {
            char[] incomeValues =
            {
                'A', 'B','C','D','E','F','G','H','I','J','K','L',
                 'M','N','O','P','Q','R','S','T','U','V','W','X',
                 'Y','Z','1','2','3','4'
            };
            Random rnd = new Random();
            int index = rnd.Next(0, incomeValues.Length);
            char income = incomeValues[index];
            return income;
        }

        private ThirdPartyRequestInput GenerateRequestInput(string xml)
        {
            StringReader strReader = null;
            XmlTextReader xmlReader = null;
            ThirdPartyRequestInput requestInput;
            try
            {
                strReader = new StringReader(xml);
                XmlSerializer serializer = new XmlSerializer(typeof(ThirdPartyRequestInput));
                xmlReader = new XmlTextReader(strReader);
                requestInput = (ThirdPartyRequestInput)serializer.Deserialize(xmlReader);
            }
            finally
            {
                xmlReader?.Close();
                strReader?.Close();
            }
            return requestInput;
        }


    }

    public class ThirdPartyRequesInputModel
    {
        public string RequestType { get; set; }
        public string SecurityToken { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string AddressLine1 { get; set; }
        public string State { get; set; }
        public string ZipCode { get; set; }
        public string VariableList { get; set; }
    }

    public class ThirdPartyResponseModel
    {
        public string ResponseStatus { get; set; }
        public string ResponseCode { get; set; }
        public string ResponseMessage { get; set; }
    }


}
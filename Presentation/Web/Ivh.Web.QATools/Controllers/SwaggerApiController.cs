﻿namespace Ivh.Web.QATools.Controllers
{
    using System;
    using System.Web.Mvc;
    using Common.Configuration.Interfaces;
    using Common.VisitPay.Strings;
    using Common.Web.Interfaces;

    [Authorize(Roles = VisitPayRoleStrings.System.QATools+","+VisitPayRoleStrings.Admin.Administrator)]
    public class SwaggerApiController : BaseController
    {
        private readonly Lazy<IAppSettingsProvider> _appSettings;

        public SwaggerApiController(
            Lazy<IBaseControllerService> baseControllerService,
            Lazy<IAppSettingsProvider> appSettings) 
            : base(baseControllerService)
        {
            this._appSettings = appSettings;
        }

        public RedirectResult SecurePanApi()
        {
            return new RedirectResult(this._appSettings.Value.Get("Swagger.SecurePanApi.Url"));
        }

        public RedirectResult PaymentApi()
        {
            return new RedirectResult(this._appSettings.Value.Get("Swagger.PaymentApi.Url"));
        }

        public RedirectResult GuestPayApi()
        {
            return new RedirectResult(this._appSettings.Value.Get("Swagger.GuestPayApi.Url"));
        }
    }
}
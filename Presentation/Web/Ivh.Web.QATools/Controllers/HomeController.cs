﻿namespace Ivh.Web.QATools.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using System.Web.Mvc;
    using Application.Email.Common.Dtos;
    using Application.Email.Common.Interfaces;
    using Application.Qat.Common.Interfaces;
    using Common.Web.Interfaces;
    using Common.Web.Models;
    using Domain.Settings.Interfaces;
    using Interfaces;
    using Models;

    [Authorize]
    public class HomeController : BaseController
    {
        private readonly Lazy<IApplicationSettingsService> _applicationSettings;
        private readonly Lazy<IDemoVisitPayUserApplicationService> _demoVisitPayUserApplicationService;
        private readonly Lazy<IDeploymentService> _deploymentService;
        private readonly Lazy<IEmailApplicationService> _emailApplicationService;

        public HomeController(
            Lazy<IBaseControllerService> baseControllerService,
            Lazy<IEmailApplicationService> emailApplicationService,
            Lazy<IDeploymentService> deploymentService,
            Lazy<IApplicationSettingsService> applicationSettings,
            Lazy<IDemoVisitPayUserApplicationService> demoVisitPayUserApplicationService)
            : base(baseControllerService)
        {
            this._emailApplicationService = emailApplicationService;
            this._deploymentService = deploymentService;
            this._applicationSettings = applicationSettings;
            this._demoVisitPayUserApplicationService = demoVisitPayUserApplicationService;
        }

        public ActionResult Index()
        {
            HomeViewModel model = new HomeViewModel();

            if (this._deploymentService.Value.IsValidEnvironment())
            {
                model.ResetDataModel = new QaToolsDataDeploymentViewModel();
                string currentClient = this._applicationSettings.Value.Client.Value.ToString();
                model.ResetDataModel.ClientName = currentClient;
            }

            /*model.DemoVisitPayUserReset = new DemoVisitPayUserResetViewModel
            {
                UserNames = this._demoVisitPayUserApplicationService.Value.GetResetableUsers()
            };*/

            return this.View(model);
        }

        public ActionResult ViewLastEmailsSent()
        {
            IList<CommunicationDto> emails = this._emailApplicationService.Value.GetLastSentEmails(75);
            return this.View(emails);
        }

        public async Task<ActionResult> ResetDemoVisitPayUser(DemoVisitPayUserResetViewModel demoVisitPayUserResetViewModel)
        {
            ResetDemoVisitPayUserResponse results = new ResetDemoVisitPayUserResponse();
            if (!this.ModelState.IsValid)
            {
                results.HasError = true;
                results.Message = "All fields are required";
                return this.Json(results, JsonRequestBehavior.AllowGet);
            }

            ResultMessage<string> result = await this._demoVisitPayUserApplicationService.Value.ResetDemoVisitPayUser(demoVisitPayUserResetViewModel.DemoVisitPayUserName, demoVisitPayUserResetViewModel.Password);

            return this.Json(result, JsonRequestBehavior.AllowGet);
        }
    }
}
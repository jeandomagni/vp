﻿using System;
using System.Web.Mvc;

namespace Ivh.Web.QATools.Controllers
{
    using System.Collections.Generic;
    using Common.Web.Interfaces;
    using Interfaces;
    using Models;

    [Authorize]
    public class DeploymentController : BaseController
    {
        private readonly Lazy<IDeploymentService> _deploymentService;
        public DeploymentController(
            Lazy<IBaseControllerService> baseControllerService,
            Lazy<IDeploymentService> deploymentService)
            : base(baseControllerService)
        {
            this._deploymentService = deploymentService;
        }

        [HttpPost]
        public ActionResult RedeployQaToolsData(string clientName)
        {
            bool result = this._deploymentService.Value.ReDeployQaToolsData(clientName);
            if (result)
            {
                this._deploymentService.Value.LogDeploymentHistory(this.User.Identity.Name, clientName);
            }

            return this.RedirectToAction("ResetDataStatus", "Deployment", new {clientName});
        }

        public ActionResult ResetDataStatus(string clientName)
        {
            return View(new QaToolsDataDeploymentViewModel {ClientName = clientName});
        }

        public ActionResult CheckDeployStatus(string clientName)
        {
            bool inProgress = this._deploymentService.Value.DeploymentInProgress(clientName);

            return this.Json(new { deployFinished = !inProgress });
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult GetDeploymentHistory()
        {
            IList<DeploymentHistoryViewModel> data = this._deploymentService.Value.GetDeploymentHistory();
            JsonResult results = this.Json(data, JsonRequestBehavior.AllowGet);
            return results;
        }


    }
}
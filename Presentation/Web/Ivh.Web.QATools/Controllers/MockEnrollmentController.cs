﻿﻿namespace Ivh.Web.QATools.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Web.Mvc;
    using Application.Qat.Common.Dtos;
    using Application.Qat.Common.Interfaces;
    using Common.Base.Constants;
    using Common.ServiceBus.Interfaces;
    using Common.VisitPay.Messages.Core;
    using Common.Web.Constants;
    using Services;

    public class MockEnrollmentController : Controller
    {
        private readonly Lazy<IBus> _bus;
        private readonly Lazy<IIhcEnrollmentApplicationService> _ihcEnrollmentApplicationService;
        private readonly FakeJwtService _fakeJwtService;

        public MockEnrollmentController(Lazy<IBus> bus, Lazy<IIhcEnrollmentApplicationService> ihcEnrollmentApplicationService)
        {
            this._bus = bus;
            this._ihcEnrollmentApplicationService = ihcEnrollmentApplicationService;
            this._fakeJwtService = new FakeJwtService();
        }

        [HttpPost]
        public ActionResult Enroll(EnrollRequestViewModel model)
        {
            string clientid = this.Request.ServerVariables.Get("HTTP_CLIENTID");
            string token = this.Request.ServerVariables.Get("HTTP_TOKEN");
            
            if (model.guarantorId != null &&  clientid != null && token != null && this._fakeJwtService.ValidateJwt(token) && clientid == "ivinciClient")
            {
                IhcEnrollmentResponseDto responseModel = this._ihcEnrollmentApplicationService.Value.GetByGuarantorId(model.guarantorId);
                if (responseModel != null)
                {
                    switch (responseModel.ReturnErrorCode ?? 0)
                    {
                        case 301:
                            this.Response.StatusCode = 301;
                            break;
                        case 302:
                            this.Response.StatusCode = 302;
                            break;
                        case 400:
                            this.Response.StatusCode = 400;
                            break;
                        case 401:
                            this.Response.StatusCode = 401;
                            this.Response.Headers.Set(HttpHeaders.Response.SuppressRedirect, "True");
                            break;
                        case 403:
                            this.Response.StatusCode = 403;
                            break;
                        case 404:
                            this.Response.StatusCode = 404;
                            break;
                        case 405:
                            this.Response.StatusCode = 405;
                            break;
                        case 406:
                            this.Response.StatusCode = 406;
                            break;
                        case 407:
                            this.Response.StatusCode = 407;
                            break;
                        case 408:
                            this.Response.StatusCode = 408;
                            break;
                        case 409:
                            this.Response.StatusCode = 409;
                            break;
                        case 410:
                            this.Response.StatusCode = 410;
                            break;
                        case 413:
                            this.Response.StatusCode = 413;
                            break;
                        case 414:
                            this.Response.StatusCode = 414;
                            break;
                        case 420: // spring framework
                            this.Response.StatusCode = 420;
                            break;
                        case 500:
                            this.Response.StatusCode = 500;
                            break;
                        case 501:
                            this.Response.StatusCode = 501;
                            break;
                        case 502:
                            this.Response.StatusCode = 502;
                            break;
                        case 503:
                            this.Response.StatusCode = 503;
                            break;
                        case 504:
                            this.Response.StatusCode = 504;
                            break;
                        case 505:
                            this.Response.StatusCode = 505;
                            break;
                    }
                    if (responseModel.ExpectedEnrollmentStatus == model.enrollmentStatus)
                    {
                        return this.Content(responseModel.Response, MimeTypes.Application.Json);
                    }
                }
                return this.Json(new EnrollResponseModel { responseCode = "Success", accounts = new List<EnrollAccountResponseModel> { new EnrollAccountResponseModel() } });
                
            }
            this.Response.StatusCode = 403;
            return this.Json(new { }, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult PublishEnrollment(int vpguarantorId)
        {
            this._bus.Value.PublishMessage(new EnrollGuarantorMessage { VpGuarantorId = vpguarantorId }).Wait();

            return this.Json(new {}, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult PublishUnEnrollment(int vpguarantorId)
        {
            this._bus.Value.PublishMessage(new UnEnrollGuarantorMessage { VpGuarantorId = vpguarantorId }).Wait();

            return this.Json(new { }, JsonRequestBehavior.AllowGet);
        }
    }

    public class EnrollRequestViewModel
    {
        public string guarantorId { get; set; }
        public string enrollmentStatus { get; set; }
    }

    public class EnrollAccountResponseModel
    {
        public string facilityId { get; set; }
        public string accountNumber { get; set; }
        public string eligiblitStatus { get; set; }
        public string originatingSystem { get; set; }
    }

    public class EnrollResponseModel
    {
        public string guarantorId { get; set; }
        public string enrollmentStatus { get; set; }
        public string responseCode { get; set; }
        public string responseMessage { get; set; }
        public IList<EnrollAccountResponseModel> accounts { get; set; }
    }
}
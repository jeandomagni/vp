﻿namespace Ivh.Web.QATools.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Net;
    using System.Threading.Tasks;
    using System.Web.Mvc;
    using System.Web.Routing;
    using Common.Base.Utilities.Extensions;
    using Common.Cache;
    using Domain.Settings.Enums;
    using Domain.Settings.Interfaces;
    using Hubs;
    using Microsoft.AspNet.SignalR;
    using Microsoft.AspNet.SignalR.Infrastructure;
    using Models.MockEndpoint;
    using Newtonsoft.Json;

    public class MockEndpointController : Controller
    {
        private const int LockRetries = 30;
        private const int LockTimeoutMilliseconds = 2000;
        private readonly Lazy<IDistributedCache> _distributedCache;
        private readonly Lazy<HubConfiguration> _hubConfiguration;
        private readonly Lazy<IUrlInterceptService> _urlInterceptService;

        public MockEndpointController(
            Lazy<IUrlInterceptService> urlInterceptService,
            Lazy<HubConfiguration> hubConfiguration,
            Lazy<IDistributedCache> distributedCache)
        {
            this._urlInterceptService = urlInterceptService;
            this._hubConfiguration = hubConfiguration;
            this._distributedCache = distributedCache;
        }

        [HttpGet]
        public ActionResult Index()
        {
            return this.View();
        }

        [HttpPost]
        public JsonResult InterceptorsData()
        {
            List<UrlEnum> urlEnums = Enum.GetValues(typeof(UrlEnum)).Cast<UrlEnum>().Where(x => x.GetAttribute<UrlEnumAttribute>()?.CanIntercept ?? false).ToList();
            List<SelectListItem> urls = urlEnums.Select(urlEnum =>
            {
                Uri value = this._urlInterceptService.Value.GetUriFromCache(urlEnum);
                return new SelectListItem
                {
                    Selected = value != null,
                    Text = urlEnum.ToString(),
                    Value = ((int) urlEnum).ToString()
                };
            }).ToList();

            return this.Json(new InterceptorsViewModel
            {
                Urls = urls
            });
        }

        [HttpPost]
        public JsonResult InterceptorsSet(InterceptorsViewModel model)
        {
            Uri requestUri = this.Request.Url;
            if (requestUri == null)
            {
                throw new Exception("request url is null");
            }

            string thisUrl = $"{requestUri.Scheme}{Uri.SchemeDelimiter}{requestUri.Host}";
            if (requestUri.Port != 80)
            {
                thisUrl += $":{requestUri.Port}";
            }

            foreach (SelectListItem url in model.Urls)
            {
                UrlEnum urlEnum = (UrlEnum) Convert.ToInt32(url.Value);
                if (url.Selected)
                {
                    string routeToUse = RouteConfig.GetCustomInterceptRouteForUrlEnum(urlEnum) != null ? "" : RouteConfig.GetDefaultInterceptRouteForUrlEnum(urlEnum);
                    this._urlInterceptService.Value.SetUriIntoCache(urlEnum, new Uri($"{thisUrl}/{routeToUse}"));
                }
                else
                {
                    this._urlInterceptService.Value.RemoveUriFromCache(urlEnum);
                }
            }

            return this.InterceptorsData();
        }

        [AllowAnonymous]
        public async Task<ActionResult> Intercept()
        {
            // get the UrlEnum from the route
            Route currentRoute = (Route) this.RouteData.Route;
            if (currentRoute == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.Unused);
            }

            UrlEnum currentUrlEnum = Enum.GetValues(typeof(UrlEnum))
                .Cast<UrlEnum>()
                .FirstOrDefault(urlEnum => currentRoute.Url == RouteConfig.GetDefaultInterceptRouteForUrlEnum(urlEnum) ||
                                           currentRoute.Url == (urlEnum.GetAttribute<UrlEnumAttribute>()?.CustomInterceptRoute ?? ""));

            // unique keys
            string keyRequest = Guid.NewGuid().ToString();
            string keyResponse = Guid.NewGuid().ToString();

            // get the request lock
            await Task.Run(() => this._distributedCache.Value.GetLock(keyRequest, LockRetries, LockTimeoutMilliseconds));

            // get the signalr hub context and send out a message to all connected clients
            IHubContext mockEndpointHubContext = this._hubConfiguration.Value.Resolver.Resolve<IConnectionManager>().GetHubContext<MockEndpointHub>();
            mockEndpointHubContext.Clients.All.incomingRequest(new InterceptViewModel
            {
                UrlName = currentUrlEnum.ToString(),
                KeyRequest = keyRequest,
                KeyResponse = keyResponse,
                RespondWith = new InterceptRespondWithViewModel
                {
                    StatusCodesCommon = GetStatusCodeList(StatusCodesCommon),
                    StatusCodesOther = GetStatusCodeList(StatusCodesOther)
                }
            });

            // the signalr hub will release the request lock, and set a InterceptRespondWithViewModel into the cache after the user responds
            // wait until that happens
            bool gotLock = await Task.Run(() => this._distributedCache.Value.GetLock(keyRequest, LockRetries, LockTimeoutMilliseconds));
            if (gotLock)
            {
                // hub has released the request lock, but we have it again, so release it immediately
                this._distributedCache.Value.ReleaseLock(keyRequest);

                // get and remove the status code the user set
                string json = await this._distributedCache.Value.GetStringAsync(keyResponse);
                InterceptRespondWithViewModel respondWith = JsonConvert.DeserializeObject<InterceptRespondWithViewModel>(json);
                await this._distributedCache.Value.RemoveAsync(keyResponse);

                // for now, just the status code
                if (respondWith?.StatusCode != null)
                {
                    return new HttpStatusCodeResult(respondWith.StatusCode.Value);
                }
            }
            else
            {
                // the hub never released, and the timeout expired
                this._distributedCache.Value.ReleaseLock(keyRequest);
            }

            // respond with a 306 by default
            return new HttpStatusCodeResult(HttpStatusCode.Unused);
        }

        #region status code lists

        private static IList<SelectListItem> GetStatusCodeList(IEnumerable<HttpStatusCode> statusCodes)
        {
            return statusCodes.Select(x => new SelectListItem
            {
                Text = $"{(int) x} - {x.ToString()}",
                Value = ((int) x).ToString()
            }).ToList();
        }

        private static IEnumerable<HttpStatusCode> StatusCodesCommon => new[]
        {
            HttpStatusCode.BadRequest,
            HttpStatusCode.Unauthorized,
            HttpStatusCode.Forbidden,
            HttpStatusCode.NotFound,
            HttpStatusCode.MethodNotAllowed,
            HttpStatusCode.RequestTimeout,
            HttpStatusCode.InternalServerError,
            HttpStatusCode.BadGateway,
            HttpStatusCode.ServiceUnavailable,
            HttpStatusCode.GatewayTimeout
        };

        private static IEnumerable<HttpStatusCode> StatusCodesOther => new[]
        {
            HttpStatusCode.MultipleChoices,
            HttpStatusCode.Moved,
            HttpStatusCode.Redirect,
            HttpStatusCode.SeeOther,
            HttpStatusCode.NotModified,
            HttpStatusCode.UseProxy,
            HttpStatusCode.TemporaryRedirect,
            HttpStatusCode.RedirectKeepVerb,
            HttpStatusCode.PaymentRequired,
            HttpStatusCode.NotAcceptable,
            HttpStatusCode.ProxyAuthenticationRequired,
            HttpStatusCode.Conflict,
            HttpStatusCode.Gone,
            HttpStatusCode.LengthRequired,
            HttpStatusCode.PreconditionFailed,
            HttpStatusCode.RequestEntityTooLarge,
            HttpStatusCode.RequestUriTooLong,
            HttpStatusCode.UnsupportedMediaType,
            HttpStatusCode.RequestedRangeNotSatisfiable,
            HttpStatusCode.ExpectationFailed,
            HttpStatusCode.UpgradeRequired,
            HttpStatusCode.HttpVersionNotSupported
        };

        /* skipping these for now */
        /*
            Continue = 100,
            SwitchingProtocols = 101,
            OK = 200,
            Created = 201,
            Accepted = 202,
            NonAuthoritativeInformation = 203,
            NoContent = 204,
            ResetContent = 205,
            PartialContent = 206,
        */

        #endregion
    }
}
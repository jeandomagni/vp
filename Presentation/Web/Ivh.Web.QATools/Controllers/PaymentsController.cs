﻿namespace Ivh.Web.QATools.Controllers
{
    using System;
    using System.Web.Mvc;
    using Common.Web.Interfaces;

    public class PaymentsController : BaseController
    {
        public PaymentsController(
            Lazy<IBaseControllerService> baseControllerService)
            : base(baseControllerService)
        {
        }

        public ActionResult Index()
        {
            return View();
        }
    }
}
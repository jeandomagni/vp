﻿namespace Ivh.Web.QATools.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web.Mvc;
    using Application.Core.Common.Dtos;
    using Application.Core.Common.Interfaces;
    using Application.HospitalData.Common.Interfaces;
    using Application.Qat.Common.Interfaces;
    using Attributes;
    using AutoMapper;
    using Common.Base.Utilities.Extensions;
    using Common.Base.Utilities.Helpers;
    using Common.VisitPay.Enums;
    using Common.VisitPay.Messages.HospitalData.Dtos;
    using Common.VisitPay.Strings;
    using Common.Web.Interfaces;
    using Domain.Guarantor.Entities;
    using Domain.HospitalData.Visit.Entities;
    using Domain.HospitalData.Visit.Interfaces;
    using Domain.Settings.Interfaces;
    using Ivh.Domain.Qat.Interfaces;
    using Models;
    using Models.Builders;
    using Models.Etl;
    using FacilityDto = Common.VisitPay.Messages.HospitalData.Dtos.FacilityDto;
    using GuarantorViewModel = Models.Etl.GuarantorViewModel;
    using HsGuarantorDto = Common.VisitPay.Messages.HospitalData.Dtos.HsGuarantorDto;
    using HsGuarantorFilterDto = Common.VisitPay.Messages.HospitalData.Dtos.HsGuarantorFilterDto;
    using IChangeSetApplicationService = Application.HospitalData.Common.Interfaces.IChangeSetApplicationService;
    using IHsGuarantorApplicationService = Application.HospitalData.Common.Interfaces.IHsGuarantorApplicationService;
    using VisitTransactionViewModel = Models.Etl.VisitTransactionViewModel;
    using VisitViewModel = Models.Etl.VisitViewModel;

    public class EtlController : BaseController
    {
        private const string VisitViewModelKey = "VisitViewModel";
        private const string GuarantorAttributeDescription = "GuarantorType";

        private readonly Lazy<IBillingSystemApplicationService> _billingSystemApplicationService;
        private readonly Lazy<IChangeEventApplicationService> _changeEventApplicationService;
        private readonly Lazy<IChangeSetApplicationService> _changeSetApplicationService;
        private readonly Lazy<IHsGuarantorApplicationService> _hsGuarantorApplicationService;
        private readonly Lazy<IQaSsoApplicationService> _ssoApplicationService;
        private readonly Lazy<IApplicationSettingsService> _applicationSettingsService;
        private readonly Lazy<IQaPaymentBatchApplicationService> _qaPaymentBatchApplicationService;
        private readonly Lazy<IVisitStateApplicationService> _visitStateApplicationService;
        private readonly Lazy<IVpGuarantorHsMatchApplicationService> _vpGuarantorHsMatchApplicationService;
        private readonly Lazy<IVpOutboundHsGuarantorApplicationService> _vpOutboundHsGuarantorApplicationService;
        private readonly Lazy<IQAInsurancePlanService> _insurancePlanService;
        private readonly Lazy<IQaMerchantAccountApplicationService> _qaMerchantAccountApplicationService;
        private readonly Lazy<IFacilityService> _facilityService;
        private readonly Lazy<IAppBaseDataService> _appBaseDataService;


        public EtlController(
            Lazy<IBaseControllerService> baseControllerService,
            Lazy<IVisitStateApplicationService> visitStateApplicationService,
            Lazy<IBillingSystemApplicationService> billingSystemApplicationService,
            Lazy<IHsGuarantorApplicationService> hsGuarantorApplicationService,
            Lazy<IChangeEventApplicationService> changeEventApplicationService,
            Lazy<IChangeSetApplicationService> changeSetApplicationService,
            Lazy<IVpGuarantorHsMatchApplicationService> vpGuarantorHsMatchApplicationService,
            Lazy<IVpOutboundHsGuarantorApplicationService> vpOutboundHsGuarantorApplicationService,
            Lazy<IQaSsoApplicationService> ssoApplicationService,
            Lazy<IApplicationSettingsService> applicationSettingsService,
            Lazy<IQaPaymentBatchApplicationService> qaPaymentBatchApplicationService,
            Lazy<IQAInsurancePlanService> insurancePlanService,
            Lazy<IQaMerchantAccountApplicationService> qaMerchantAccountApplicationService,
            Lazy<IFacilityService> facilityService,
            Lazy<IAppBaseDataService> appBaseDataService
            )
            : base(baseControllerService)
        {
            this._visitStateApplicationService = visitStateApplicationService;
            this._billingSystemApplicationService = billingSystemApplicationService;
            this._hsGuarantorApplicationService = hsGuarantorApplicationService;
            this._changeEventApplicationService = changeEventApplicationService;
            this._changeSetApplicationService = changeSetApplicationService;
            this._vpGuarantorHsMatchApplicationService = vpGuarantorHsMatchApplicationService;
            this._vpOutboundHsGuarantorApplicationService = vpOutboundHsGuarantorApplicationService;
            this._ssoApplicationService = ssoApplicationService;
            this._applicationSettingsService = applicationSettingsService;
            this._qaPaymentBatchApplicationService = qaPaymentBatchApplicationService;
            this._insurancePlanService = insurancePlanService;
            this._qaMerchantAccountApplicationService = qaMerchantAccountApplicationService;
            this._facilityService = facilityService;
            this._appBaseDataService = appBaseDataService;
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult HsGuarantorsView(int? id)
        {
            return this.View("~/Views/ETL/Guarantors/Index.cshtml", new HsGuarantorFilterViewModel
            {
                VPGID = id
            });
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult HsGetBillingSystemTypes()
        {
            IList<BillingSystemDto> data = this._billingSystemApplicationService.Value.GetAllBillingSystemTypes();
            return this.Json(data, JsonRequestBehavior.AllowGet);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult GetHsGuarantors(HsGuarantorFilterViewModel filter)
        {
            HsGuarantorFilterDto hsGuarantorFilterDto = Mapper.Map<HsGuarantorFilterDto>(filter);
            IReadOnlyList<HsGuarantorSearchResultDto> data = this._hsGuarantorApplicationService.Value.GetHsGuarantors(hsGuarantorFilterDto);

            // get application service here -- mock data for now
            return this.Json(data, JsonRequestBehavior.AllowGet);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult MyChartSsoGenerateHsGuarantor(SsoRequestModel model)
        {
            SsoResponseModel responseModel = new SsoResponseModel();
            responseModel.SsoUrl = $"{this.ClientDto.AppGuarantorUrlHost}Sso/MyChart?context={this._ssoApplicationService.Value.GenerateEncryptedSsoTokenHsGuarantor(model.HsGuarantorId, SsoProviderEnum.OpenEpic, model.SsoSsk, model.DateOfBirth)}";
            responseModel.UrlExpiration = DateTime.UtcNow.AddMinutes(this._applicationSettingsService.Value.SsoTimeoutTokensAfterMinutes.Value).ToString("yyyy-MM-dd hh:mm:ss");
            return this.Json(responseModel, JsonRequestBehavior.AllowGet);
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult HsGuarantorDetailsView(int id)
        {
            HsGuarantorDto hsGuarantor = this._hsGuarantorApplicationService.Value.GetHsGuarantor(id);
            HsGuarantorViewModel model = Mapper.Map<HsGuarantorViewModel>(hsGuarantor);
            return this.View("~/Views/ETL/Guarantors/GuarantorDetails.cshtml", model);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult HsGuarantorDetailsUpdate(HsGuarantorViewModel model)
        {
            HsGuarantorDto hsGuarantor = this._hsGuarantorApplicationService.Value.GetHsGuarantor(model.HsGuarantorId);
            Action<bool, ChangeEventTypeEnum, string> sendChangeSet = (b, t, m) =>
            {
                if (b)
                {
                    return;
                }

                HsGuarantorChangeDto hsGuarantorChangeDto = Mapper.Map<HsGuarantorChangeDto>(model);
                ChangeEventDto changeEventDto = new ChangeEventDto
                {
                    EventTriggerDescription = m,
                    ChangeEventType = new ChangeEventTypeDto { ChangeEventTypeId = (int)t },
                    ChangeEventStatus = ChangeEventStatusEnum.Unprocessed,
                    ChangeEventDateTime = DateTime.UtcNow
                };


                this._changeEventApplicationService.Value.SaveChangeEvent(changeEventDto, hsGuarantorChangeDto);
                this._appBaseDataService.Value.AddHsGuarantor(hsGuarantorChangeDto.SourceSystemKey, hsGuarantorChangeDto.BillingSystemId);
                this._changeEventApplicationService.Value.QueueAllUnprocessedChangeEvents();
            };

            Func<HsGuarantorViewModel, HsGuarantorViewModel, bool> hsGuarantorMatchDiscrepancy = (a, b) =>
            {
                return (a.LastName == b.LastName
                        && a.DOB == b.DOB
                        && a.SSN4 == b.SSN4
                        && a.FirstName == b.FirstName
                        && a.AddressLine1 == b.AddressLine1
                        && a.StateProvince == b.StateProvince
                        && a.City == b.City);
            };

            Func<HsGuarantorViewModel, HsGuarantorViewModel, bool> hsGuarantorEligibilityChange = (a, b) => a.VpEligible.Equals(b.VpEligible);

            HsGuarantorViewModel modelSaved = Mapper.Map<HsGuarantorViewModel>(hsGuarantor);

            sendChangeSet(hsGuarantorMatchDiscrepancy(model, modelSaved), ChangeEventTypeEnum.HsGuarantorMatchDiscrepancy, "Match Field Discrepancy");
            sendChangeSet(hsGuarantorEligibilityChange(model, modelSaved), ChangeEventTypeEnum.DataChange, "Guarantor Type Ineligible");

            return this.RedirectToAction("HsGuarantorDetailsView", new { id = model.HsGuarantorId });
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult GetVpGuarantorHsMatches(int id)
        {
            IReadOnlyList<VpGuarantorHsMatchDto> model = this._vpGuarantorHsMatchApplicationService.Value.GetVpGuarantorHsMatches(id);
            var entity =
                model.Select(e => new
                {
                    HsGuarantorId = e.HsGuarantorId,
                    DOB = e.DOB.ToString(Format.DateFormat),
                    PatientDOB = e.PatientDOB.ToString(Format.DateFormat),
                    HsConfirmedOn = e.HsConfirmedOn.HasValue ? e.HsConfirmedOn.Value.ToString(Format.DateFormat) : string.Empty,
                    MatchedOn = e.MatchedOn.ToString(Format.DateFormat),
                    UnmatchedOn = e.UnmatchedOn.HasValue ? e.UnmatchedOn.Value.ToString(Format.DateFormat) + "[" + e.HsGuarantorMatchStatus.ToString() + "]" : string.Empty,
                    SentToHsOn = e.SentToHsOn.HasValue ? e.SentToHsOn.Value.ToString(Format.DateFormat) : string.Empty,
                    IsActive = e.IsActive ? "Yes" : "No",
                    LastName = e.LastName,
                    PostalCode = e.PostalCode,
                    Ssn4 = e.Ssn4,
                    VpGuarantorId = e.VpGuarantorId
                }).ToList();
            JsonResult results = this.Json(entity, JsonRequestBehavior.AllowGet);
            return results;
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult UnmatchGuarantor(HsGuarantorViewModel model)
        {
            HsGuarantorChangeDto hsGuarantorChangeDto = Mapper.Map<HsGuarantorChangeDto>(model);

            ChangeEventDto changeSetDto = new ChangeEventDto
            {
                EventTriggerDescription = "Unmatch Guarantor",
                ChangeEventType = new ChangeEventTypeDto { ChangeEventTypeId = (int)ChangeEventTypeEnum.HsGuarantorUnmatch },
                ChangeEventStatus = ChangeEventStatusEnum.Unprocessed,
                ChangeEventDateTime = DateTime.UtcNow,
                HsGuarantorId = hsGuarantorChangeDto.HsGuarantorId
            };

            this._changeEventApplicationService.Value.SaveChangeEvent(changeSetDto, hsGuarantorChangeDto);
            this._changeEventApplicationService.Value.QueueAllUnprocessedChangeEvents();

            return this.RedirectToAction("HsGuarantorDetailsView", new { id = model.HsGuarantorId });
        }


        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult GetHsGuarantorEvents(int id)
        {
            IReadOnlyList<VpOutboundHsGuarantorDto> outboundEvents = this._vpOutboundHsGuarantorApplicationService.Value.GetOutboundEvents(id);

            List<VpOutboundHsGuarantorViewModel> model = outboundEvents.Select(outboundEvent => new VpOutboundHsGuarantorViewModel
            {
                VpOutboundHsGuarantorId = outboundEvent.VpOutboundHsGuarantorId,
                VpOutboundHsGuarantorMessageType = outboundEvent.VpOutboundHsGuarantorMessageType.ToString(),
                InsertDate = outboundEvent.InsertDate.ToString(Format.DateFormat),
                ProcessedDate = outboundEvent.ProcessedDate.HasValue ? outboundEvent.ProcessedDate.Value.ToString(Format.DateFormat) : ""
            }).ToList();

            return this.Json(model, JsonRequestBehavior.AllowGet);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public void VpEligible(string visitNumber, int billingSystemId)
        {
            this._visitStateApplicationService.Value.SetVisitEligible(visitNumber, billingSystemId);
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult VpEligibleView()
        {
            IList<BillingSystemDto> model = this._billingSystemApplicationService.Value.GetAllBillingSystemTypes();
            return this.View("~/Views/ETL/VpEligible.cshtml", Mapper.Map<List<BillingSystem>>(model));
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public void VpIneligible(string visitNumber, int billingSystemId, string reason)
        {
            this._visitStateApplicationService.Value.SetVisitIneligible(visitNumber, billingSystemId, reason);
        }

        [HttpPost]
        public JsonResult RecallAllVisits(int hsGuarantorId)
        {
            IReadOnlyList<HsVisitDto> hsVisits = this._changeEventApplicationService.Value.GetVisitsForGuarantor(hsGuarantorId);

            foreach (VisitChangeDto visitChangeDto in hsVisits.Select(Mapper.Map<VisitChangeDto>))
            {
                visitChangeDto.ChangeType = ChangeTypeEnum.Update;
                visitChangeDto.IsChanged = true;
                visitChangeDto.VpEligible = false;
                visitChangeDto.VisitTransactions.ToList().ForEach(x => x.ChangeType = ChangeTypeEnum.Update);

                ChangeEventDto changeEvent = new ChangeEventDto
                {
                    EventTriggerDescription = "Set VpEligible to false",
                    ChangeEventType = new ChangeEventTypeDto { ChangeEventTypeId = (int)ChangeEventTypeEnum.DataChange },
                    ChangeEventDateTime = DateTime.UtcNow,
                    VisitId = visitChangeDto.VisitId
                };
                this._changeEventApplicationService.Value.SaveChangeEvent(changeEvent, visitChangeDto);
            }

            this._changeEventApplicationService.Value.QueueAllUnprocessedChangeEvents();

            return this.Json(true);
        }

        #region hs guarantor visits

        [HttpGet]
        public ActionResult HsGuarantorDetails(int id)
        {
            HsGuarantorDto hsGuarantorDto = this._hsGuarantorApplicationService.Value.GetHsGuarantor(id);

            return this.View(Mapper.Map<GuarantorViewModel>(hsGuarantorDto));
        }

        [HttpGet]
        public ActionResult HsGuarantorVisits(int id)
        {
            HsGuarantorDto hsGuarantorDto = this._hsGuarantorApplicationService.Value.GetHsGuarantor(id);

            if (hsGuarantorDto == null)
            {
                return this.RedirectToAction("HsGuarantorsView");
            }

            return this.View(Mapper.Map<GuarantorViewModel>(hsGuarantorDto));
        }

        [HttpPost]
        public JsonResult HsGuarantorVisitsData(int hsGuarantorId)
        {
            IReadOnlyList<HsVisitDto> visits = this._changeEventApplicationService.Value.GetVisitsForGuarantor(hsGuarantorId);

            return this.Json(visits);
        }

        [HttpGet]
        public ActionResult AddHsGuarantor()
        {
            AddHsGuarantorViewModel model = new AddHsGuarantorViewModel
            {
                BillingSystems = this._billingSystemApplicationService.Value.GetAllBillingSystemTypes().Select(s => new SelectListItem { Value = s.BillingSystemId.ToString(), Text = s.BillingSystemName }).ToList(),
                SourceSystemKey = RandomSourceSystemKeyGenerator.New("NNNNNNNNNN"),
                DOB = DateTime.Today.AddYears(-25).AddDays(-25)
            };
            return this.View(model);
        }

        [HttpPost]
        public ActionResult AddHsGuarantor(AddHsGuarantorViewModel model)
        {
            HsGuarantorDto hsGuarantor = Mapper.Map<HsGuarantorDto>(model);
            this._hsGuarantorApplicationService.Value.AddHsGuarantor(hsGuarantor);
            this._appBaseDataService.Value.AddHsGuarantor(hsGuarantor.SourceSystemKey, hsGuarantor.HsBillingSystemId);

            return this.RedirectToAction("HsGuarantorsView");
        }

        // /Etl/
        [HttpGet]
        public ActionResult AddVisit(int id)
        {
            HsGuarantorDto hsGuarantorDto = this._hsGuarantorApplicationService.Value.GetHsGuarantor(id);
            this.KillCache(VisitViewModelKey);
            return this.View(this.BuildVisits(hsGuarantorDto));
        }

        [HttpGet]
        public ActionResult AddAdditionalVisit(int id)
        {
            HsGuarantorDto hsGuarantorDto = this._hsGuarantorApplicationService.Value.GetHsGuarantor(id);
            return this.View("AddVisit", this.BuildVisits(hsGuarantorDto));
        }

        [HttpPost]
        public ActionResult AddVisit(AddVisitViewModel model)
        {
            List<AddVisitViewModel> visits = this.GetCache<AddVisitViewModel>(VisitViewModelKey);
            visits.Add(model);

            if (!model.ProcessChangeSet)
            {
                return this.RedirectToAction("AddAdditionalVisit", new { id = model.HsGuarantorId });
            }

            this.ProcessAddVisitChangeSet(visits);

            this.KillCache(VisitViewModelKey);
            return this.RedirectToAction("HsGuarantorVisits", new { id = model.HsGuarantorId });
        }

        [HttpPost]
        [AutomationAuthorize]
        public ActionResult AddVisitAutomation(AddVisitViewModel model)
        {
            DateTime now = DateTime.UtcNow;

            // set a random ssk if one does not exist
            model.Visit.SourceSystemKey = model.Visit.SourceSystemKey.IsNullOrEmpty() ? RandomSourceSystemKeyGenerator.New("NNNNNNNNNN") : model.Visit.SourceSystemKey;
            model.Visit.SourceSystemKeyDisplay = model.Visit.SourceSystemKey;

            // set defaults if automation hasn't
            model.Visit.AdmitDate = model.Visit.AdmitDate == default(DateTime) ? now : model.Visit.AdmitDate;
            model.Visit.BillingSystemId = model.Visit.BillingSystemId == default(int) ? this._billingSystemApplicationService.Value.GetAllBillingSystemTypes().Select(x => x.BillingSystemId).FirstOrDefault() : model.Visit.BillingSystemId;
            model.Visit.BillingApplication = string.IsNullOrWhiteSpace(model.Visit.BillingApplication) ? "HB" : model.Visit.BillingApplication;
            model.Visit.DischargeDate = model.Visit.DischargeDate == default(DateTime) ? model.Visit.AdmitDate : model.Visit.DischargeDate;
            model.Visit.LifeCycleStage = string.IsNullOrWhiteSpace(model.Visit.LifeCycleStage) ? "3" : model.Visit.LifeCycleStage;
            model.Visit.PatientDob = model.Visit.PatientDob == default(DateTime) ? DateTime.Today.AddYears(-25).AddDays(-25) : model.Visit.PatientDob;
            model.Visit.PatientFirstName = string.IsNullOrWhiteSpace(model.Visit.PatientFirstName) ? "firstname" : model.Visit.PatientFirstName;
            model.Visit.PatientLastName = string.IsNullOrWhiteSpace(model.Visit.PatientLastName) ? "lastname" : model.Visit.PatientLastName;
            model.Visit.PatientType = string.IsNullOrWhiteSpace(model.Visit.PatientType) ? "3" : model.Visit.PatientType;
            model.Visit.PrimaryInsuranceType = string.IsNullOrWhiteSpace(model.Visit.PrimaryInsuranceType) ? "1" : model.Visit.PrimaryInsuranceType;
            model.Visit.RevenueWorkCompany = string.IsNullOrWhiteSpace(model.Visit.RevenueWorkCompany) ? "1" : model.Visit.RevenueWorkCompany;
            model.Visit.VisitDescription = string.IsNullOrWhiteSpace(model.Visit.VisitDescription) ? "Automated Visit Description" : model.Visit.VisitDescription;

            // transactions
            if (model.Transactions == null || model.Transactions.Count == 0)
            {
                model.Transactions = model.Transactions ?? new List<VisitTransactionViewModel>();
            }

            foreach (VisitTransactionViewModel transaction in model.Transactions)
            {
                transaction.TransactionDate = transaction.TransactionDate.GetValueOrDefault(now);
                transaction.PostDate = transaction.PostDate.GetValueOrDefault(transaction.TransactionDate.Value);
                transaction.VpTransactionTypeId = transaction.VpTransactionTypeId.GetValueOrDefault((int)VpTransactionTypeEnum.HsCharge);
                transaction.TransactionDescription = string.IsNullOrWhiteSpace(transaction.TransactionDescription) ? "Automated Transaction Description" : transaction.TransactionDescription;
            }

            // insurance plans
            if (model.InsurancePlans == null || model.InsurancePlans.Count == 0)
            {
                VisitInsurancePlanViewModel defaultInsurancePlan = this._changeEventApplicationService.Value.GetInsurancePlans()
                    .Where(x => !string.IsNullOrWhiteSpace(x.InsurancePlanName))
                    .Where(x => x.InsurancePlanName.Contains("AFLAC"))
                    .Select(x => new VisitInsurancePlanViewModel
                    {
                        InsurancePlanId = x.InsurancePlanId
                    }).FirstOrDefault();

                model.InsurancePlans = model.InsurancePlans ?? new List<VisitInsurancePlanViewModel>();
                model.InsurancePlans.Add(defaultInsurancePlan);
            }

            this.ProcessAddVisitChangeSet(new List<AddVisitViewModel> { model });

            return this.Content("Done");
        }

        private void ProcessAddVisitChangeSet(List<AddVisitViewModel> models)
        {
            int transactionCode = this._changeEventApplicationService.Value.GetVisitTransactionCodes().FirstOrDefault()?.TransactionCodeId ?? 2;
            int changeEventTypeId = models.First().ChangeEventType;

            ChangeEventDto changeEvent = new ChangeEventDto
            {
                ChangeEventDateTime = DateTime.UtcNow,
                ChangeEventStatus = ChangeEventStatusEnum.Unprocessed,
                ChangeEventType = new ChangeEventTypeDto { ChangeEventTypeId = changeEventTypeId },
                EventTriggerDescription = "VisitTransactionDataChanged from QAT.AddVisit"
            };

            foreach (AddVisitViewModel model in models)
            {
                VisitChangeDto visitChangeDto = Mapper.Map<VisitChangeDto>(model.Visit);
                visitChangeDto.ChangeType = ChangeTypeEnum.Insert;

                visitChangeDto.VisitTransactions = new List<VisitTransactionChangeDto>();
                visitChangeDto.VisitTransactions.AddRange(model.Transactions.Where(w => w.VpTransactionTypeId != default(int)).Select(s =>
                {
                    VisitTransactionChangeDto visitTransactionDto = Mapper.Map<VisitTransactionChangeDto>(s);
                    visitTransactionDto.ChangeType = ChangeTypeEnum.Insert;
                    visitTransactionDto.BillingSystemId = visitChangeDto.BillingSystemId;
                    visitTransactionDto.SourceSystemKey = RandomSourceSystemKeyGenerator.New("NNNNNNNNNN");
                    if (s.TransactionCode != null && s.TransactionCode < transactionCode)
                    {
                        s.TransactionCode = null;
                    }
                    visitTransactionDto.TransactionCodeId = s.TransactionCode ?? transactionCode;
                    visitTransactionDto.VisitId = visitChangeDto.VisitId;
                    return visitTransactionDto;
                }));

                visitChangeDto.VisitInsurancePlans = new List<VisitInsurancePlanChangeDto>();
                visitChangeDto.VisitInsurancePlans.AddRange(model.InsurancePlans.Where(w => w.InsurancePlanId != -1).Select(s =>
                {
                    VisitInsurancePlanChangeDto visitInsurancePlanChange = Mapper.Map<VisitInsurancePlanChangeDto>(s);
                    visitInsurancePlanChange.VisitId = visitChangeDto.VisitId;
                    InsurancePlanDto insurancePlanDto = this._changeEventApplicationService.Value.GetInsurancePlans().FirstOrDefault(x => x.InsurancePlanId == s.InsurancePlanId);

                    visitInsurancePlanChange.InsurancePlanId = (insurancePlanDto ?? this.CreateNewQaToolsInsurancePlan()).InsurancePlanId;
                    return visitInsurancePlanChange;
                }));

                this.SetFacilityDto(model.Visit.FacilityId, visitChangeDto);
                this.SetRevenueCompanyDto(model.Visit.RevenueWorkCompany, visitChangeDto);
                this.SetPrimaryInsuranceTypeDto(model.Visit.PrimaryInsuranceType, visitChangeDto);
                this.SetPatientTypeDto(model.Visit.PatientType, visitChangeDto);
                this.SetLifeCycleStageDto(model.Visit.LifeCycleStage, visitChangeDto);

                this._changeEventApplicationService.Value.SaveChangeEvent(changeEvent, visitChangeDto);
                this._appBaseDataService.Value.AddVisit(visitChangeDto.SourceSystemKey, visitChangeDto.BillingSystemId);

            }
            this._changeEventApplicationService.Value.QueueAllUnprocessedChangeEvents();

        }

        private InsurancePlanDto CreateNewQaToolsInsurancePlan()
        {
            InsurancePlanDto insurancePlanDto = this.GetQaInsurancePlans().TakeRandom(1).First();

            insurancePlanDto.InsurancePlanId = this._changeEventApplicationService.Value.GetInsurancePlans().Max(x => x.InsurancePlanId) + 1;
            insurancePlanDto.SourceSystemKey = RandomSourceSystemKeyGenerator.New("NNNNNNNNNNNNN");
            insurancePlanDto.InsurancePlanName = $"QaTools_{insurancePlanDto.SourceSystemKey}";

            this._insurancePlanService.Value.AddInsurancePlan(insurancePlanDto);

            return insurancePlanDto;
        }

        private IList<InsurancePlanDto> GetQaInsurancePlans()
        {
            IList<string> insurancePlansFilter = new List<string>() { "-100", "1019011", "1028319", "1028323", "1028325", "1028215", "122227", "1221468", "1223627", "1223628", "1028019", "1028342", "1028343", "1912", "1921", "1970", "1028325" };

            return this._changeEventApplicationService.Value.GetInsurancePlans()
                .Where(x => insurancePlansFilter.Contains(x.SourceSystemKey) || x.InsurancePlanName != null && (x.InsurancePlanName.Contains("BLUE CROSS") || x.InsurancePlanName.Contains("QaTools_")))
                .ToList();
        }

        private List<T> GetCache<T>(string name)
        {
            if (this.Session[name] == null)
            {
                this.Session[name] = new List<T>();
            }
            return (List<T>)this.Session[name];
        }

        private void KillCache(string name)
        {
            if (this.Session[name] != null)
            {
                this.Session.Remove(name);
            }
        }

        private AddVisitViewModel BuildVisits(HsGuarantorDto hsGuarantorDto)
        {
            string sourceSystemKey = RandomSourceSystemKeyGenerator.New("NNNNNNNNNN");

            List<SelectListItem> transactionTypes = this._changeEventApplicationService.Value.GetVisitTransactionTypes()
                .Where(w => w.TransactionSource == "HS" || w.VpTransactionTypeId == -1)
                .Select(ToSelectListItem)
                .ToList();

            AddVisitViewModel model = new AddVisitViewModel
            {
                HsGuarantorId = hsGuarantorDto.HsGuarantorId,
                Visit = new VisitViewModel
                {
                    HsGuarantorId = hsGuarantorDto.HsGuarantorId,
                    SourceSystemKey = sourceSystemKey,
                    SourceSystemKeyDisplay = sourceSystemKey,
                    AdmitDate = DateTime.Today,
                    DischargeDate = DateTime.Today,
                    PatientDob = DateTime.Today.AddYears(-25).AddDays(-25)
                },
                Transactions = Enumerable.Range(0, 10).Select(s => new VisitTransactionViewModel {PostDate = DateTime.Today, TransactionDate = DateTime.Today}).ToList(),
                InsurancePlans = Enumerable.Range(0, 5).Select(s => new VisitInsurancePlanViewModel {PayOrder = s + 1}).ToList(),
                Guarantor = Mapper.Map<GuarantorViewModel>(hsGuarantorDto),
                BillingSystems = this._billingSystemApplicationService.Value.GetAllBillingSystemTypes().Select(s => new SelectListItem {Value = s.BillingSystemId.ToString(), Text = s.BillingSystemName}).ToList(),
                BillingApplications = new[] {"HB", "PB"}.Select(s => new SelectListItem {Value = s, Text = s}).ToList(),
                TransactionCounts = Enumerable.Range(1, 10).Select(s => new SelectListItem {Value = s.ToString(), Text = s.ToString()}).ToList(),
                TransactionTypes = transactionTypes,
                TransactionCodes = this.GetTransactionCodes(),
                InsurancePlanTypesWithNothingSelected = new List<SelectListItem>(),
                ServiceGroups = this._qaMerchantAccountApplicationService.Value.GetServiceGroupIds().Select(s => new SelectListItem {Value = s.ToString(), Text = s.ToString()}).ToList(),
                ChangeEventTypes = this.GetChangeEventTypes(),
                PaymentPriorities = Enumerable.Range(1, 10).Select(s => new SelectListItem {Value = s.ToString(), Text = s.ToString()}).ToList(),
            };

            IList<InsurancePlanDto> insurancePlans = this.GetQaInsurancePlans();

            model.InsurancePlanTypes = insurancePlans
                .OrderBy(x => x.InsurancePlanName)
                .Select(x => new SelectListItem {Value = x.InsurancePlanId.ToString(), Text = x.InsurancePlanName})
                .ToList();

            model.InsurancePlanTypes.Insert(0, new SelectListItem {Value = "0", Text = "- New Insurance Plan"});
            model.TransactionTypes.Insert(0, new SelectListItem {Selected = true, Value = "0", Text = " - Select"});
            model.TransactionCodes.Insert(0, new SelectListItem {Selected = true, Value = "-1", Text = " - Select"});
            model.ServiceGroups.Insert(0, new SelectListItem {Selected = true, Value = "-1", Text = " - Select"});
            model.PaymentPriorities.Insert(0, new SelectListItem {Selected = true, Value = "-1", Text = " - Select"});

            foreach (SelectListItem item in model.InsurancePlanTypes)
            {
                model.InsurancePlanTypesWithNothingSelected.Add(new SelectListItem {Value = item.Value, Text = item.Text});
            }

            model.InsurancePlanTypesWithNothingSelected.Insert(0, new SelectListItem {Value = "-1", Text = " - Select"});

            model.Facilities = this.GetFacilitiesSelectList();
            model.Facilities.Insert(0, new SelectListItem {Value = "-1", Text = " - Select"});

            SelectListItem defaultInsuranceType = model.InsurancePlanTypes.FirstOrDefault(x => x.Text.Contains("AFLAC"));
            if (defaultInsuranceType != null)
            {
                defaultInsuranceType.Selected = true;
            }

            model.LifeCycleStages = this._changeEventApplicationService.Value.GetLifeCycleStages()
                .Select(s => new SelectListItem {Value = s.LifeCycleStageId.ToString(), Text = s.LifeCycleStageName}).ToList();

            IEnumerable<AgingTierEnum> values = Enum.GetValues(typeof(AgingTierEnum)).Cast<AgingTierEnum>();
            model.AgingTiers = values.Select(x => new SelectListItem {Value = ((int) x).ToString(), Text = x.ToString()}).ToList();

            SelectListItem lifeCycleStage = model.LifeCycleStages.FirstOrDefault(x => string.Equals(x.Value, "5", StringComparison.InvariantCultureIgnoreCase));
            if (lifeCycleStage != null)
            {
                lifeCycleStage.Selected = true;
            }

            model.PatientTypes = this._changeEventApplicationService.Value.GetPatientTypes()
                .Select(s => new SelectListItem {Value = s.PatientTypeId.ToString(), Text = s.PatientTypeName}).ToList();
            SelectListItem patientTypes = model.PatientTypes.FirstOrDefault(x => string.Equals(x.Value, "3", StringComparison.InvariantCultureIgnoreCase));
            if (patientTypes != null)
            {
                patientTypes.Selected = true;
            }

            model.PrimaryInsuranceTypes = this._changeEventApplicationService.Value.GetPrimaryInsuranceTypes()
                .Select(s => new SelectListItem {Value = s.PrimaryInsuranceTypeId.ToString(), Text = $"{s.PrimaryInsuranceTypeName} ({s.SelfPayClass})"}).ToList();
            SelectListItem primaryInsuranceType = model.PrimaryInsuranceTypes.FirstOrDefault(x => string.Equals(x.Value, "1", StringComparison.InvariantCultureIgnoreCase));
            if (primaryInsuranceType != null)
            {
                primaryInsuranceType.Selected = true;
            }

            model.RevenueWorkCompanies = this._changeEventApplicationService.Value.GetRevenueWorkCompanies()
                .Select(s => new SelectListItem {Value = s.RevenueWorkCompanyId.ToString(), Text = s.RevenueWorkCompanyName}).ToList();
            SelectListItem defaultRevenueWorkCompany = model.RevenueWorkCompanies.FirstOrDefault(x => string.Equals(x.Value, "5", StringComparison.InvariantCultureIgnoreCase));
            if (defaultRevenueWorkCompany != null)
            {
                defaultRevenueWorkCompany.Selected = true;
            }

            model.Visits = this.GetCache<AddVisitViewModel>(VisitViewModelKey).Select(x => x.Visit).ToList();

            return model;
        }

        private void SetLifeCycleStageDto(string lifeCycleStage, VisitChangeDto visitChangeDto)
        {
            IList<LifeCycleStageDto> companies = this._changeEventApplicationService.Value.GetLifeCycleStages();
            LifeCycleStageDto found = companies.FirstOrDefault(x => string.Equals(x.LifeCycleStageId.ToString(), lifeCycleStage, StringComparison.InvariantCultureIgnoreCase));
            if (found != null)
            {
                visitChangeDto.LifeCycleStage = found;
            }
        }

        private void SetPatientTypeDto(string patientType, VisitChangeDto visitChangeDto)
        {
            IList<PatientTypeDto> companies = this._changeEventApplicationService.Value.GetPatientTypes();
            PatientTypeDto found = companies.FirstOrDefault(x => string.Equals(x.PatientTypeId.ToString(), patientType, StringComparison.InvariantCultureIgnoreCase));
            if (found != null)
            {
                visitChangeDto.PatientType = found;
            }
        }

        private void SetPrimaryInsuranceTypeDto(string primaryInsuranceType, VisitChangeDto visitChangeDto)
        {
            IList<PrimaryInsuranceTypeDto> companies = this._changeEventApplicationService.Value.GetPrimaryInsuranceTypes();
            PrimaryInsuranceTypeDto found = companies.FirstOrDefault(x => string.Equals(x.PrimaryInsuranceTypeId.ToString(), primaryInsuranceType, StringComparison.InvariantCultureIgnoreCase));
            if (found != null)
            {
                visitChangeDto.PrimaryInsuranceType = found;
            }
        }

        private void SetRevenueCompanyDto(string company, VisitChangeDto visitChangeDto)
        {
            IList<RevenueWorkCompanyDto> companies = this._changeEventApplicationService.Value.GetRevenueWorkCompanies();
            RevenueWorkCompanyDto found = companies.FirstOrDefault(x => string.Equals(x.RevenueWorkCompanyId.ToString(), company, StringComparison.InvariantCultureIgnoreCase));
            if (found != null)
            {
                visitChangeDto.RevenueWorkCompany = found;
            }
        }

        private void SetFacilityDto(int? facilityId, VisitChangeDto visitChangeDto)
        {
            IList<FacilityDto> facilities = this._changeEventApplicationService.Value.GetFacilities();
            FacilityDto found = facilities.FirstOrDefault(x => x.FacilityId.Equals(facilityId));
            if (found != null)
            {
                visitChangeDto.Facility = found;
            }
        }

        [HttpGet]
        public ActionResult ViewVisit(int id)
        {
            HsVisitDto hsVisitDto = this._changeSetApplicationService.Value.GetVisit(id);
            HsGuarantorDto hsGuarantorDto = this._hsGuarantorApplicationService.Value.GetHsGuarantor(hsVisitDto.HsGuarantorId);

            ViewVisitViewModel model = new ViewVisitViewModel
            {
                Guarantor = Mapper.Map<GuarantorViewModel>(hsGuarantorDto),
                Visit = Mapper.Map<VisitViewModel>(hsVisitDto),
                ChangeSets = Mapper.Map<IList<ChangeEventDto>, IList<ChangeEventViewModel>>(hsVisitDto.ChangeEvents.Where(x => x != null).ToList()),
                OutboundVisitTransactions = Mapper.Map<IList<VpOutboundVisitTransactionDto>, IList<OutboundVisitTransactionViewModel>>(hsVisitDto.OutboundVisitTransactionDtos),
                OutboundVisits = Mapper.Map<IList<VpOutboundVisitDto>, IList<OutboundVisitViewModel>>(hsVisitDto.OutboundVisitDtos),
                VisitTransactions = Mapper.Map<IList<VisitTransactionViewModel>>(hsVisitDto.VisitTransactions),
                VisitInsurancePlans = Mapper.Map<IList<VisitInsurancePlanViewModel>>(hsVisitDto.VisitInsurancePlans),
            };

            return this.View(model);
        }

        #endregion

        #region create change set

        private void SetTransactionTypes(CreateChangeSetViewModel model)
        {
            IReadOnlyList<VpTransactionTypeDto> transactionTypes = this._changeEventApplicationService.Value.GetVisitTransactionTypes();
            List<SelectListItem> list = transactionTypes
                .Where(w => new[] { "HS", "VPP-HS" }.Contains(w.TransactionSource) ||
                            w.VpTransactionTypeId == (int)VpTransactionTypeEnum.VppPrincipalPayment ||
                            w.VpTransactionTypeId == (int)VpTransactionTypeEnum.VppInterestPayment ||
                            w.VpTransactionTypeId == (int)VpTransactionTypeEnum.VppDiscount ||
                            w.VpTransactionTypeId == -1)
                .OrderBy(x => x.TransactionSource)
                .ThenBy(x => x.VpTransactionTypeId)
                .Select(ToSelectListItem).ToList();

            model.TransactionTypes = list;
        }

        private static SelectListItem ToSelectListItem(VpTransactionTypeDto vpTransactionTypeDto)
        {
            string text = vpTransactionTypeDto.VpTransactionTypeName;
            if (string.IsNullOrWhiteSpace(text))
            {
                text = $"{vpTransactionTypeDto.TransactionType}";
            }
            text += $" ({vpTransactionTypeDto.VpTransactionTypeId})";

            return new SelectListItem
            {
                Value = vpTransactionTypeDto.VpTransactionTypeId.ToString(),
                Text = text
            };
        }

        [HttpGet]
        public ActionResult CreateChangeSet(int id)
        {
            HsVisitDto hsVisitDto = this._changeSetApplicationService.Value.GetVisit(id);
            HsGuarantorDto hsGuarantorDto = this._hsGuarantorApplicationService.Value.GetHsGuarantor(hsVisitDto.HsGuarantorId);

            CreateChangeSetViewModel model = new CreateChangeSetViewModel();
            Mapper.Map(hsVisitDto, model);
            Mapper.Map(hsGuarantorDto, model);
            model.Transactions = Mapper.Map<List<VisitTransactionViewModel>>(hsVisitDto.VisitTransactions);
            model.TransactionCodes = this.GetTransactionCodes();
            model.UnclearedPaymentAllocations = Mapper.Map<IList<UnclearedPaymentAllocationViewModel>>(this._qaPaymentBatchApplicationService.Value.GetUnclearedOutboundVisitTransactions(hsVisitDto.VisitId));
            model.LifeCycleStages = this._changeEventApplicationService.Value.GetLifeCycleStages()
                .Select(s => new SelectListItem { Value = s.LifeCycleStageId.ToString(), Text = s.LifeCycleStageName }).ToList();

            SelectListItem lifeCycleStage = model.LifeCycleStages.FirstOrDefault(x => string.Equals(x.Value, hsVisitDto.LifeCycleStage?.LifeCycleStageId.ToString() ?? "0", StringComparison.InvariantCultureIgnoreCase));
            if (lifeCycleStage != null)
            {
                lifeCycleStage.Selected = true;
            }
            model.LifeCycleStage = lifeCycleStage;
            model.LifeCycleStageId = hsVisitDto.LifeCycleStage?.LifeCycleStageId ?? 0;

            model.Facilities = this.GetFacilitiesSelectList();

            this.SetTransactionTypes(model);

            model.VisitTransactionDataChanged = 1;
            model.IsPatientGuarantor = hsVisitDto.IsPatientGuarantor ?? false;
            model.IsPatientMinor = hsVisitDto.IsPatientMinor ?? false;
            model.InterestZero = hsVisitDto.InterestZero.GetValueOrDefault(false);
            model.InterestRefund = hsVisitDto.InterestRefund.GetValueOrDefault(false);
            model.FacilityId = hsVisitDto.Facility?.FacilityId.GetValueOrDefault(0);
            model.FacilityDescription = hsVisitDto.Facility?.FacilityDescription;

            IEnumerable<AgingTierEnum> values = Enum.GetValues(typeof(AgingTierEnum)).Cast<AgingTierEnum>();
            model.AgingTiers = values.Select(x => new SelectListItem { Value = ((int)x).ToString(), Text = x.ToString() }).ToList();
            model.AgingTier = hsVisitDto.AgingTierId;
            if (model.AgingTier >= (int)AgingTierEnum.MaxAge)
            {
                model.AgingTier = (int)AgingTierEnum.MaxAge;
            }

            model.PaymentPriorities = Enumerable.Range(1, 10).Select(s => new SelectListItem { Value = s.ToString(), Text = s.ToString() }).ToList();
            model.PaymentPriorities.Insert(0, new SelectListItem { Selected = true, Value = "-1", Text = " - Select" });

            model.ServiceGroups = this._qaMerchantAccountApplicationService.Value.GetServiceGroupIds().Select(s => new SelectListItem { Value = s.ToString(), Text = s.ToString() }).ToList();
            model.ServiceGroups.Insert(0, new SelectListItem { Selected = true, Value = "-1", Text = " - Select" });
            model.ServiceGroupId = hsVisitDto.ServiceGroupId;

            model.VpEligible = hsVisitDto.VpEligible ?? false;
            model.BillingHold = hsVisitDto.BillingHold ?? false;
            model.Redact = hsVisitDto.Redact ?? false;

            return this.View(model);
        }

        [AutomationAuthorize]
        [HttpPost]
        public JsonResult CreateChangeSet(CreateChangeSetViewModel model)
        {
            //
            ChangeSetDto changeSetDto = this._changeSetApplicationService.Value.CreateChangeSetFromVisit(model.VisitId);
            ChangeSetDtoBuilder builder = new ChangeSetDtoBuilder(changeSetDto);

            //
            foreach (int i in model.OptionOrder.Select(x => int.Parse(x.Value)).Where(x => x > 0))
            {
                builder.AddChangeEventVisitTransactionDataChanged(model.VisitTransactionDataChanged == i, "VisitTransactionDataChanged from QAT.CreateChangeSet");

                if (model.VisitTransactionDataChanged == i)
                {
                    builder
                        .MergeVisit(model)
                        .MergeTransactions(model.Transactions)
                        .MergePaymentAllocations(model.UnclearedPaymentAllocations?.Select(x => x.VpPaymentAllocationId).ToList() ?? new List<int>());
                }
            }

            // scoring change event
            builder.AddChangeEventScoring(model.CreateScoringChangeEvent, "FirstSelfPayDate from QAT.CreateChangeSet", model.VisitId);

            // save and send
            this._changeEventApplicationService.Value.SaveChangeEvents(changeSetDto, ChangeSetTypeEnum.Visit);
            this._changeEventApplicationService.Value.QueueAllUnprocessedChangeEvents();

            foreach (VisitChangeDto visitChangeDto in changeSetDto.VisitChanges ?? Enumerable.Empty<VisitChangeDto>())
            {
                this._appBaseDataService.Value.AddVisit(visitChangeDto.SourceSystemKey, visitChangeDto.BillingSystemId);
            }
            foreach (HsGuarantorChangeDto hsGuarantorChangeDto in changeSetDto.HsGuarantorChanges ?? Enumerable.Empty<HsGuarantorChangeDto>())
            {
                this._appBaseDataService.Value.AddHsGuarantor(hsGuarantorChangeDto.SourceSystemKey, hsGuarantorChangeDto.BillingSystemId);
            }

            return this.Json(new
            {
                Success = true,
                Message = "/Etl/HsGuarantorVisits/" + model.HsGuarantorId
            });
        }

        #endregion

        private IList<SelectListItem> GetTransactionCodes()
        {
            List<SelectListItem> list = this._changeEventApplicationService.Value.GetVisitTransactionCodes().Select(s => new SelectListItem()
            {
                Value = s.TransactionCodeId.ToString(),
                Text = s.TransactionCodeName
            }).OrderBy(x => x.Text).ToList();

            list.Insert(0, new SelectListItem() { Text = "-", Value = "" });

            return list;
        }

        private List<SelectListItem> GetChangeEventTypes()
        {
            List<SelectListItem> list = new List<SelectListItem>
            {
                new SelectListItem {Value = "2", Text = "VisitTransactionDataChanged"},
                new SelectListItem {Value = "6", Text = "VisitPlaced"}
            };
            return list;
        }

        private List<SelectListItem> GetFacilitiesSelectList()
        {
            IReadOnlyList<Facility> facilities = this._facilityService.Value.GetAllFacilities();
            
            return facilities.Select(x => new SelectListItem
                {
                    Value = x.FacilityId.ToString(),
                    Text = $"{x.StateCode} - {x.FacilityDescription}"
                })
                .OrderBy(x => x.Text)
                .ToList();
        }
    }
}
﻿namespace Ivh.Web.QATools.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web.Mvc;
    using Application.Qat.Common;
    using Application.Qat.Common.Enums;
    using Application.Qat.Common.Interfaces;
    using Models.Persona;
    using Utilities;
    
    public class PersonaController: Controller
    {
        private readonly IPersonaCreationApplicationService _personaCreationApplicationService;
        private readonly Lazy<ICloneGuarantorApplicationService> _cloneGuarantorApplicationService;

        public PersonaController(
            IPersonaCreationApplicationService personaCreationApplicationService,
            Lazy<ICloneGuarantorApplicationService> cloneGuarantorApplicationService)
        {
            this._personaCreationApplicationService = personaCreationApplicationService;
            this._cloneGuarantorApplicationService = cloneGuarantorApplicationService;
        }
        
        /*
         public ActionResult Index()
        {
            DateTime now = DateTime.UtcNow;

            IList<PersonaResultDto> personaResultDtos = new List<PersonaResultDto>();
            
            //personaResultDtos.Add(this._personaCreationApplicationService.StatementGuy9());
            //personaResultDtos.Add(this._personaCreationApplicationService.DemoGeneric("Jane", "Smith", "99"));
            //personaResultDtos.AddRange(this._personaCreationApplicationService.DemoConsolidated("Mary", "Park", "99", "Larry", "Park", "99"));

            //personaResultDtos.Add(this._personaCreationApplicationService.AaronTraining());
            //personaResultDtos.Add(this._personaCreationApplicationService.BobTraining());
            //personaResultDtos.Add(this._personaCreationApplicationService.HankTraining());
            //personaResultDtos.AddRange(this._personaCreationApplicationService.HenryAndJoniTraining());
            //personaResultDtos.AddRange(this._personaCreationApplicationService.JaneAndSteveTraining());
            //personaResultDtos.Add(this._personaCreationApplicationService.JulieTraining());
            //personaResultDtos.Add(this._personaCreationApplicationService.LanceTraining());
            //personaResultDtos.Add(this._personaCreationApplicationService.RickAndSusieTraining());
            //personaResultDtos.Add(this._personaCreationApplicationService.SaraTraining());
            
            StringBuilder sb = new StringBuilder();
            sb.AppendLine("<hr/>");
            sb.AppendLine("<p>these scripts will run KeepCurrent, as if you were deploying your database</p>");
            sb.AppendLine("<p><strong>do not run these if you are baselining</strong></p>");
            sb.AppendLine("<pre>");

            foreach (PersonaResultDto personaResultDto in personaResultDtos)
            {
                sb.AppendLine($"-- {personaResultDto.VpGuarantorDto.User.UserName}");
                sb.AppendLine($"DECLARE @hsgid INT = {personaResultDto.HsGuarantorDto.HsGuarantorId.ToString()},");
                sb.AppendLine($"        @vpgid INT = {personaResultDto.VpGuarantorDto.VpGuarantorId.ToString()},");
                sb.AppendLine("        @adj INT;");
                sb.AppendLine("DECLARE @tbl TABLE (adj INT);");
                sb.AppendLine($"INSERT INTO @tbl (adj) EXEC dev_vp_app.qa.GetDaysToAdjustGuarantorKeepCurrent @VpGuarantorId = @vpgid, @Anchor = \'LastStatementDate\', @DayOffset = {personaResultDto.KeepCurrentOffset.ToString()}");
                sb.AppendLine("SELECT @adj = adj FROM @tbl;");
                sb.AppendLine("EXEC dev_vp_app.qa.AdjustAppGuarantorDates @VpGuarantorId = @vpgid, @DatePart = 'day', @Adjustment = @adj;");
                sb.AppendLine("EXEC dev_cdi_etl.qa.AdjustEtlGuarantorDates @HsGuarantorId = @hsgid, @DatePart = 'day', @Adjustment = @adj;");
                sb.AppendLine("GO");
                sb.AppendLine("");
            }

            sb.AppendLine("</pre>");
            
            return this.Content($"time to complete: {DateTime.UtcNow.Subtract(now).TotalSeconds.ToString()}<br /><br />{sb}");
        }
        */
        [HttpGet]
        public ActionResult Manage(bool loadtest = false)
        {
            ManagePersonasViewModel model = new ManagePersonasViewModel
            {
                DemoAccountUsernames = PersonaConstants.DemoAccountUsernames.Where(x => !PersonaConstants.ManagedAccounts.Contains(x)).ToArray(),
                TrainingAccountUsernames = PersonaConstants.TrainingAccountUsernames.Where(x => !PersonaConstants.ManagedAccounts.Contains(x)).ToArray(),
                HsDataScenarios = PersonaConstants.HsDataScenarios
            };

            if (loadtest)
            {
                model.OtherUsernames = new[]
                {
                    PersonaConstants.LoadTestGuyUsername
                };
                model.AdditionalCloneOptions = new []{ 1000 };
            }

            return this.View(model);
        }

        [HttpPost]
        public ActionResult Clone(string sourceVisitPayUserName, string suffix, string hsGuarantorSourceSystemKey)
        {
            CloneGuarantorResultEnum result = this._cloneGuarantorApplicationService.Value.CloneGuarantorSynchronous(sourceVisitPayUserName, suffix);
            if (result == CloneGuarantorResultEnum.MissingUser)
            {
                // persona is missing, build
                this.Rebuild(sourceVisitPayUserName, hsGuarantorSourceSystemKey);

                // clone
                this._cloneGuarantorApplicationService.Value.CloneGuarantorSynchronous(sourceVisitPayUserName, suffix);
            }

            return this.Json(true);
        }

        [HttpPost]
        public ActionResult Rebuild(string personaUsername, string hsGuarantorSourceSystemKey)
        {
            this._personaCreationApplicationService.Run(personaUsername, hsGuarantorSourceSystemKey);

            return this.Json(true);
        }

        [HttpGet]
        public ActionResult CreatePersona()
        {
            PersonaCreateViewModel personaCreateViewModel = new PersonaCreateViewModel();
            List<string> userNames = this._personaCreationApplicationService.GetUsabilityUserNames();
            personaCreateViewModel.Password = PasswordGenerator.GeneratePassword();
            personaCreateViewModel.PersonaUserNames = userNames;
            return this.View(personaCreateViewModel);
        }

        [HttpGet]
        public JsonResult GeneratePassword()
        {
            return this.Json(PasswordGenerator.GeneratePassword(), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult CreatePersona(PersonaCreateViewModel model)
        {
            this._personaCreationApplicationService.CreateUsabilityPersona(model.PersonaUserName, model.Password);
            return this.Json($"{model.PersonaUserName} with password {model.Password} has been successfully created", JsonRequestBehavior.AllowGet);
        }
        
        #region testing
        
        public ActionResult QueueChangeSets()
        {
            this._personaCreationApplicationService.Queue();
            return this.Content("queued");
        }
        
        #endregion
    }
}
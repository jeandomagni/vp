﻿namespace Ivh.Web.QATools.Attributes
{
    using System;
    using System.Text;
    using System.Web;
    using System.Web.Mvc;
    using Common.Web.Constants;

    public class AutomationAuthorizeAttribute : AuthorizeAttribute
    {
        protected override bool AuthorizeCore(HttpContextBase httpContext)
        {
            bool isAuthorized = base.AuthorizeCore(httpContext);
            if (isAuthorized)
            {
                return true;
            }

            string auth = httpContext.Request.Headers[HttpHeaders.Request.Authorization];
            if (string.IsNullOrEmpty(auth))
            {
                return false;
            }

            string[] credential = Encoding.ASCII.GetString(Convert.FromBase64String(auth.Substring(6))).Split(':');

            return credential[0] == "test" && credential[1] == "password";
        }
    }
}
﻿(
    function ($) {
        function resetModalButtons() {
            $('#_Close').removeClass('hide');
            $('#_Cancel').removeClass('hide');
            $('#_Save').removeClass('hide');

            $('#_Close').addClass('hide');
            $('#_Cancel').addClass('hide');
            $('#_Save').addClass('hide');
        }
    }
)(jQuery);



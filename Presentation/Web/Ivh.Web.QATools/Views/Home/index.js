﻿
$(document).ready(function () {
    var $form = $("#frmResetDemoUser");
    $form.on('submit', function (e) {
            e.preventDefault();
            var $messageContainer = $("#reset-user-message");
            toggleMessageClass(false, $messageContainer);

            
            if (!$form.valid()) {
                $messageContainer.text('');
            } else {
                
                $("#form-container").hide();
                $messageContainer.text("Processing...");
                var action = $form.attr('action');
                $.post(action, $form.serialize(), function (result) {
                    
                    if (result.HasError === true) {
                        toggleMessageClass(true, $messageContainer);
                        $messageContainer.text(result.Message);
                    } else {
                        $("#reset-user-message").text(result.Message);
                    }

                    $("#form-container").show();

                });
            }
           
        });

        function toggleMessageClass(isError, $messageContainer ) {
            if (isError === true) {
                $messageContainer.addClass("error");
                $messageContainer.removeClass("success");
            } else {
                $messageContainer.addClass("success");
                $messageContainer.removeClass("error");
            }
        }
    });



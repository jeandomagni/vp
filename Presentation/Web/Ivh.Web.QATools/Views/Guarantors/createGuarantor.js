﻿if (createGuarantor === undefined) {
    var createGuarantor = {};
}

createGuarantor = (
    function ($, qaCommon, sideNav) {
        var that = this;

        that.GuarantorViewModel = {};

        function buildModel() {
            that.GuarantorViewModel.UserName = $("#UserName").val();
            that.GuarantorViewModel.FirstName = $("#FirstName").val();
            that.GuarantorViewModel.LastName = $("#LastName").val();
            that.GuarantorViewModel.PaymentDueDay = parseInt($("#PaymentDueDate").val().split("-")[1]);
            that.GuarantorViewModel.RegistrationDate = $("#RegistrationDate").val();
        }

        function resetFields() {
            $("input").val("");
            $("#_Save").attr("disabled", true);
        }

        function init() {
            var that = this,
                parms = {
                    userName: "", guarantorId: 0, hasVisits: false
                },
                menuList = sideNav.menuList;
            that.parms = parms;
            that.menuList = menuList;

            menuList.getMenuItemById("CreateGuarantor").setActive();

            $("#UserName, #FirstName, #LastName, #PaymentDueDay, #RegistrationDate").on("input", function () { $("#_Save").removeAttr("disabled"); });

            $("#RegistrationDate").datepicker({
                dateFormat: 'mm-dd-yy',
                defaultDate: new Date()
            });

            $("#PaymentDueDate").datepicker({
                dateFormat: 'mm-dd-yy',
                defaultDate: new Date()
            });

            $("#_Save").on("click", function () {
                buildModel();
                $.ajax({
                    type: "POST",
                    url: "/Guarantors/CreateGuarantor",
                    data: JSON.stringify(that.GuarantorViewModel),
                    contentType: "application/json; charset=utf-8"
                }).done(function(results, status, xhr) {

                    qaCommon.alertMessage("success", "Guarantor successfully created.");

                    $("#VisitPayUserId").text(results.VisitPayUserId);
                    $("#VpGuarantorId").text(results.VpGuarantorId);

                    that.parms.guarantorId = results.VpGuarantorId;
                    that.parms.userName = $("#UserName").val();

                    that.menuList.getMenuItemById("CreateVisit").setActionLink(that.parms);
                    that.menuList.getMenuItemById("CreateVisit").setVisible();

                    that.menuList.getMenuItemById("CreateVisitTransaction").setActionLink(that.parms);
                    that.menuList.getMenuItemById("CreateVisitTransaction").setVisible(false);

                    resetFields();
                    $("#UserName").focus();
                }).fail(function() {
                    qaCommon.alertMessage("danger", "Error creating guarantor.");
                });
            });

            $("#_Cancel").on("click", function () {
                var url = "/Guarantors/Index";
                window.location.href = url;
            });
        }
        init();
    }
)(jQuery, qaCommon, _sideNav);



﻿if (guarantor === undefined) {
    var guarantor = {};
}

guarantor = (
    function ($, qaEnums, qaCommon) {

        function init() {
            $('#frmSearch').on('submit', function (e) {
                e.preventDefault();
                runSearch($("#SearchField"));
            });
            runSearch($("#SearchField"));
        }

        function runSearch(el) {
            var val = "" + el.val(),
                isDate = !!qaCommon.parseDate(val.replace(/\//g, "-")),
                currDate = new Date(),
                dateVal = isDate ? new Date(el.val()) : null,
                isValidDate = isDate && (dateVal <= currDate),
                pad4 = qaCommon.isNumeric(val) ? qaCommon.pad(val, 4) : "",
                isSSN4 = !!pad4 && !!pad4.match(/^\d{4}$/),
                isEmail = qaCommon.validateEmail(val),
                parm;

            this._el = el;
            $(this._el).removeAttr('rel');
            $(this._el).removeAttr('data-original-title');

            if (isDate) {
                if (isValidDate) {
                    parm = { filter: "DOB", value: val };
                } else {
                    $(this._el).addClass('error');
                    $(this._el).attr('rel', 'toolTipTop');
                    $(this._el).attr('data-original-title', 'DOB must be prior to current day.');
                    $(this._el).tooltip('show');
                    $(this._el).focus();
                }
            } else if (isSSN4) {
                parm = { filter: "SSN4", value: pad4 };
            } else if (isEmail) {
                parm = { filter: "emailAddress", value: val };
            } else  if (val.length === 0) {
                parm = { filter: null, value: null };
            } else {
                parm = { filter: "userName", value: val };
            }
            refreshGrid(parm, true);
        }

        function loadDetail(el, id, userName) {
            var url = "/Guarantors/GetGuarantor/" + id;
            window.location.href = url;
        }

        function refreshGrid(searchParm, forceReload) {
            var url = '/Guarantors/GetGuarantors';
            if (searchParm) {
                url += '?filter=' + searchParm.filter + '&value=' + searchParm.value;
            }

            $("#myGrid").jqGrid({
                url: url,
                onSelectRow: function (rowId, status, e) {
                    var rowObj = $(this).jqGrid('getLocalRow', rowId),
                        el = $("#PartialViewContainer");
                    loadDetail(el, rowId, rowObj["VpGuarantorStatus"], rowObj["User.UserName"]);
                },
                datatype: 'json',
                mtype: 'GET',
                emptyrecords: 'No records to display',
                colNames: ['Id', 'User Name', 'First Name', 'Last Name', 'SSN4', 'Status'],
                colModel: [
                    { key: true, name: 'VpGuarantorId', index: 'VpGuarantorId', width: 25, align: 'center', sortable: true},
                    { key: false, name: 'User.UserName', index: 'User.UserName', width: 85, align: 'left', sortable: true },
                    { key: false, name: 'User.FirstName', index: 'User.FirstName', width: 85, align: 'left', sortable: true },
                    { key: false, name: 'User.LastName', index: 'User.LastName', width: 105, align: 'left', sortable: true },
                    { key: false, name: 'User.SSN4', index: 'User.SSN4', width: 45, align: 'center', formatter: function (value) { return qaCommon.pad(value, 4); }, sortable: true },
                    { key: false, name: 'VpGuarantorStatus', index: 'VpGuarantorStatus', align: 'center', width: 90, formatter: function (value) { return qaCommon.getEnumString(qaEnums.GuarantorStatuses, value); }, sortable: true }
                ],
                jsonReader: {
                    root: 'Data',
                    id: 'VpGuarantorId',
                    repeatitems: true
                },
                pager: $('#myPager'),
                rowNum: 100,
                autowidth: true,
                height: 'auto',
                viewrecords: true,
                caption: "",
                sortName: "VpGuarantorId",
                sortOrder: "asc",
                gridview: true,
                defaults: {
                    recordtext: "View {0} - {1} of {2}",
                    emptyrecords: "No records to view",
                    loadtext: "Loading...",
                    pgtext: "Page {0} of {1}"
                }
            });
            if (forceReload) {
                $("#myGrid").jqGrid('setGridParam', { datatype: 'json', url: url }).trigger('reloadGrid');
            } else {
                $("#myGrid").trigger('reloadGrid');
            }

        }
    init();
}
)(jQuery, qaEnums, qaCommon);


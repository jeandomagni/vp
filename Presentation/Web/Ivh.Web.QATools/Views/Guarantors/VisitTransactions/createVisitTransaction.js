﻿if (createVisitTransaction === undefined) {
    var createVisitTransaction = {};
}

createVisitTransaction = (
    function ($, qaCommon, sideNav) {
        var that = this;

        that.VisitTransactionViewModel = {};

        function buildModel() {
            var date = $("#TransactionDate").val().split('-'),
                transactionDate = date[2] + '-' + date[0] + '-' + date[1],
                insertDate;

            date = $("#TransactionDate").val().split('-');
            insertDate = date[2] + '-' + date[0] + '-' + date[1];
            that.VisitTransactionViewModel.UserName = $("#UserName").text();
            that.VisitTransactionViewModel.TransactionDate = transactionDate;
            that.VisitTransactionViewModel.TransactionAmount = $("#TransactionAmount").val();
            that.VisitTransactionViewModel.TransactionDescription = $("#TransactionDescription").val();
            that.VisitTransactionViewModel.VpTransactionTypeID = $("#VpTransactionTypeID").val();
            that.VisitTransactionViewModel.InsertDate = insertDate;
            that.VisitTransactionViewModel.RelatedChangeToVisitStateId = $("#RelatedChangeToVisitStateId").val();
            that.VisitTransactionViewModel.VisitID = $("#VisitList").val();
        }

        function populateVisits(guarantorId) {
            var results;
            $.ajax({
                type: "GET",
                url: "/Guarantors/Visits/GetVisitsByGuarantor/" + guarantorId
            }).done(function(data, textStatus, jqXHR) {
                results = data;
                var options = $("#VisitList");
                options.empty();
                options.append($("<option />").val("-1").text(" -- Choose Visit -- "));
                $.each(data,
                    function() {
                        var obj = this;
                        options.append($("<option />").val(obj.VisitId).text(obj.VisitDescription));
                    });
            }).fail(function(jqXHR, textStatus, errorThrown) {
                alert("Error retrieving visit statuses -  " + errorThrown);
            });
        }

        function populateVisitStates() {
            var results;
            $.ajax({
                type: "GET",
                url: "/Visits/GetVisitStates"
            }).done(function(data, textStatus, jqXHR) {
                results = data;
                var options = $("#RelatedChangeToVisitStateId");
                options.empty();
                options.append($("<option />").val("-1").text(" -- Choose Visit Status -- "));
                $.each(data,
                    function() {
                        var obj = this;
                        options.append($("<option />").val(obj.VisitStateId).text(obj.VisitStateDisplayName));
                    });
            }).fail(function(jqXHR, textStatus, errorThrown) {
                alert("Error retrieving visit statuses -  " + errorThrown);
            });

        }

        function populateTransactionType() {
            var results;
            $.ajax({
                type: "GET",
                url: "/Visits/GetTransactionTypes"
            }).done(function(data, textStatus, jqXHR) {
                results = data;
                var options = $("#VpTransactionTypeID");
                options.empty();
                options.append($("<option />").val("-1").text(" -- Choose Transaction Type -- "));
                $.each(data,
                    function() {
                        var obj = this;
                        options.append($("<option />").val(obj.VpTransactionTypeId).text(obj.VpTransactionTypeName));
                    });
            }).fail(function(jqXHR, textStatus, errorThrown) {
                alert("Error retrieving transaction types -  " + errorThrown);
            });
        }

        function resetFields() {
            $("#VisitList option[value='-1']").show();
            $("#VpTransactionTypeID option[value='-1']").show();
            $("#RelatedChangeToVisitStateId option[value='-1']").show();
            $("select option[value=-1]").attr("selected", true);
            $("#TransactionDate, #TransactionAmount, #InsertDate").val("");
            $("#TransactionDescription").val("");
            $("#_Save").attr("disabled", true);
            $("#VisitList").focus();
        }

        function init() {

            var that = this,
                parms = { userName: $("#UserName").text(), guarantorId: $("#VpGuarantorId").val(), hasVisits: qaCommon.getParameterByName("hasVisits") },
                menuList = sideNav.menuList;

            that.parms = parms;
            that.menuList = sideNav.menuList;

            $("#TransactionDate, #TransactionAmount, #InsertDate, #TransactionDescription, #VpTransactionTypeID, #RelatedChangeToVisitStateId").on("input", function () { $("#_Save").removeAttr("disabled"); });

            $("#InsertDate").datepicker({
                dateFormat: 'mm-dd-yy',
                defaultDate: new Date()
            });

            $("#TransactionDate").datepicker({
                dateFormat: 'mm-dd-yy',
                defaultDate: new Date()
            });


            menuList.getMenuItemById("CreateVisit").setActionLink(parms);
            menuList.getMenuItemById("CreateVisit").setVisible(parms);

            menuList.getMenuItemById("CreateVisitTransaction").setActionLink(parms);
            menuList.getMenuItemById("CreateVisitTransaction").setVisible(parms);

            populateVisits(parms.guarantorId);
            $("#VisitList").on("change", function () {
                $("#VisitList option[value='-1']").hide();
            });

            populateTransactionType();
            $("#VpTransactionTypeID").on("change", function () {
                $("#VpTransactionTypeID option[value='-1']").hide();
            });

            populateVisitStates();
            $("#RelatedChangeToVisitStateId").on("change", function () {
                $("#RelatedChangeToVisitStateId option[value='-1']").hide();
            });

            $("#_Save").on("click", function () {
                buildModel();
                $.ajax({
                    type: "POST",
                    url: "/Visits/CreateVisitTransaction",
                    data: JSON.stringify(that.VisitTransactionViewModel),
                    contentType: "application/json; charset=utf-8"
                }).done(function() {
                    qaCommon.alertMessage("success", "Visit transaction successfully created.");
                    resetFields();
                }).fail(function() {
                    qaCommon.alertMessage("danger", "Failure creating transaction.");
                });
            });

            $("#_Cancel").on("click", function () {
                var url = "/Guarantors/Index";
                window.location.href = url;
            });

        }

        init();

}
)(jQuery, qaCommon, _sideNav);

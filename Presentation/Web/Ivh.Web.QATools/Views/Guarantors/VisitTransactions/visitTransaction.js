﻿if (visitTransaction === undefined) {
    var visitTransaction = {};
}

visitTransaction = (
    function ($, qaCommon, _sideNav) {
        this.VisitTransactionViewModel = {};
        var _sideNav = _sideNav;
        var common = qaCommon;

        init();
        function buildModel() {
            var date = $("#TransactionDate").val().split('-'),
                transactionDate = date[1] + '/' + date[2] + '/' + date[0],
                insertDate;

            date = $("#TransactionDate").val().split('-');
            insertDate = date[1] + '/' + date[2] + '/' + date[0];
             
            this.VisitTransactionViewModel.UserName = $("#UserName").val();
            this.VisitTransactionViewModel.TransactionDate = transactionDate;
            this.VisitTransactionViewModel.TransactionAmount = $("#TransactionAmount").val();
            this.VisitTransactionViewModel.TransactionDescription = $("#TransactionDescription").val();
            this.VisitTransactionViewModel.VpTransactionTypeID = $("#VpTransactionTypeID").val();
            this.VisitTransactionViewModel.InsertDate = insertDate;
            this.VisitTransactionViewModel.RelatedChangeToVisitStatusId = $("#RelatedChangeToVisitStatusId").val();
            this.VisitTransactionViewModel.VisitID = $("#VisitList").val();
        }

        function populateVisits(guarantorId) {
            var results;
            $.ajax({
                type: "GET",
                url: "/Guarantors/Visits/GetVisitsByGuarantor/" + guarantorId,
                success: function (data, textStatus, jqXHR) {
                    results = data;
                    var options = $("#VisitList");
                    options.empty();
                    options.append($("<option />").val("-1").text(" -- Choose Visit -- "));
                    $.each(data,
                        function () {
                            var obj = this;
                            options.append($("<option />").val(obj.VisitId).text(this.VisitDescription));
                        });
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    alert("Error retrieving visit statuses -  " + errorThrown);
                }
            });
        }

        function populateVisitStatuses() {
            var results;
            $.ajax({
                type: "GET",
                url: "/Visits/GetVisitStatuses",
                success: function (data, textStatus, jqXHR) {
                    results = data;
                    var options = $("#RelatedChangeToVisitStatusId");
                    options.empty();
                    options.append($("<option />").val("-1").text(" -- Choose Visit Status -- "));
                    $.each(data,
                        function () {
                            var obj = this;
                            options.append($("<option />").val(obj.VisitStatusId).text(this.VisitStatusDisplayName));
                        });
                },
                error: function (jqXHR, textStatus, errorThrown) { alert("Error retrieving visit statuses -  " + errorThrown); }
            });

        }

        function populateTransactionType() {
            var results;
            $.ajax({
                type: "GET",
                url: "/Visits/GetTransactionTypes",
                success: function (data, textStatus, jqXHR) {
                    results = data;
                    var options = $("#VpTransactionTypeID");
                    options.empty();
                    options.append($("<option />").val("-1").text(" -- Choose Transaction Type -- "));
                    $.each(data,
                        function () {
                            var obj = this;
                            options.append($("<option />").val(obj.VpTransactionTypeId).text(this.VpTransactionTypeName));
                    });
                },
                error: function (jqXHR, textStatus, errorThrown) { alert("Error retrieving transaction types -  " + errorThrown);}
            });
        }

        function resetFields() {
            $("#VisitList option[value='-1']").show();
            $("#VpTransactionTypeID option[value='-1']").show();
            $("#RelatedChangeToVisitStatusId option[value='-1']").show();
            $("select option[value=-1]").attr("selected", true);
            $("#TransactionDate, #TransactionAmount, #InsertDate").val("");
            $("#TransactionDescription").val("");
            $("#_Save").attr("disabled", true);
            $("#VisitList").focus();
        }

        function init() {
            var that = this;
            var _guarantorId = $("#VisitPayGuarantorID").val();
            $("#TransactionDate, #TransactionAmount, #InsertDate, #TransactionDescription, #VpTransactionTypeID, #RelatedChangeToVisitStatusId").on("input", function () { $("#_Save").removeAttr("disabled"); });

            _sideNav.setLink('CreateVisitViewOptionLink', 'enabled', { userName: $("#UserName").val(), guarantorId: _guarantorId });
            _sideNav.setLink('CreateVisitTransactionViewLink', 'enabled', { userName: $("#UserName").val(), guarantorId: _guarantorId });

            populateVisits(_guarantorId);
            $("#VisitList").on("change", function () {
                $("#VisitList option[value='-1']").hide();
            });

            populateTransactionType();
            $("#VpTransactionTypeID").on("change", function () {
                $("#VpTransactionTypeID option[value='-1']").hide();
            });

            populateVisitStatuses();
            $("#RelatedChangeToVisitStatusId").on("change", function () {
                $("#RelatedChangeToVisitStatusId option[value='-1']").hide();
            });

            $("#_Save").on("click", function () {
                buildModel();
                $.ajax({
                    type: "POST",
                    url: "/Visits/CreateVisitTransaction",
                    data: JSON.stringify(that.VisitTransactionViewModel),
                    contentType: "application/json; charset=utf-8",
                    success: function () {
                        qaCommon.createAutoClosingAlert("success", "Visit transaction successfully created.");
                        resetFields();
                    },
                    error: function () {
                        qaCommon.createAutoClosingAlert("danger", "Failure creating transaction.");
                    }
                });
            });

            $("#_Cancel").on("click", function () {
                var url = "/Guarantors/Index";
                window.location.href = url;
            });

        }
}
)(jQuery, qaCommon, _sideNav);

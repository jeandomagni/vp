﻿if (clearGuarantorData === undefined) {
    var clearGuarantorData = {};
}

clearGuarantorData = (
    function ($, qaCommon) {
        
        function handleCustomAlert(data) {
            var html = "<h4 class='pull-left'>Success running, with warnings from SQL:</h4><br /><br />";
            $.each(data, function(i, value) {
                html += value + " <hr>";
            });

            qaCommon.alertMessage("warning",  html, 20000, true);
        }

        function init() {
            
            $("#btnClearGuarantorData").on('click', function (e) {

                e.preventDefault();

                $.post('/Guarantors/RunClearGuarantorData', { guarantorId: parseInt(qaCommon.getParameterByName("guarantorId")) }, function(data) {
                    if (data) {
                        handleCustomAlert(data);
                    } else {
                        qaCommon.alertMessage('success', 'Success running clear guarantor data');
                    }

                });
            });

        }
        init();
    }
)(jQuery, qaCommon);



﻿if (guarantorDetail === undefined) {
    var guarantorDetail = {};
}

guarantorDetail = (
    function ($, qaEnums, qaCommon, sideNav) {
        var _sideNav = sideNav;

        function init() {
            var vpStatusId = parseInt($("#GuarantorStatusId").val()),
                status = qaCommon.getEnumString(qaEnums.GuarantorStatuses, vpStatusId),
                hasVisits = ($("#GuarantorHasVisits").val() === "True"),
                guarantorId = $("#VpGuarantorId").text(),
                userName = $("#UserName").text(),
                parms = {
                    guarantorId: guarantorId,
                    hasVisits: hasVisits,
                    UserName: userName
                };
                //menuList = _sideNav.menuList;
            $("#GuarantorStatus").text(status);

            $("#GetHQYBalance").on('click', function (e) {
                e.preventDefault();
                $.ajax({
                    type: "GET",
                    url: "/Guarantors/GetGuarantorHealthEquityBalance?guarantorId=" + $("#VpGuarantorId").text(),
                    contentType: "application/json; charset=utf-8"
                }).done(function(results, status, xhr) {
                    console.log(results);
                    var balanceToUse;
                    if (results.CashBalance == null || results.CashBalance == undefined) {
                        balanceToUse = "Unknown";
                    } else {
                        balanceToUse = results.CashBalance;
                    }
                    $("#HQYDisplay").html(balanceToUse);
                }).fail(function() {
                    qaCommon.alertMessage("danger", "Call to get balance failed.");
                });
            });

            $("#CloneGuarantor").on('click', function (e) {
                e.preventDefault();
                $.ajax({
                    type: "POST",
                    url: "/Guarantors/CloneGuarantor?vpGuarantorId=" + $("#VpGuarantorId").text() + "&appendValue=" + $("#AppendValue").val() + "&consolidate=" + $('#cpConsolidate').is(':checked'),
                    contentType: "application/json; charset=utf-8"
                }).done(function(result) {
                    if (result === "") {
                        qaCommon.alertMessage("danger", "Append value is required.");
                    } else {
                        qaCommon.alertMessage("success", result);
                    }
                });
            });

        }

        init();
    }
)(jQuery, qaEnums, qaCommon, _sideNav);


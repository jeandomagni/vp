﻿if (guarantorAuditLog === undefined) {
    var guarantorAuditLog = {};
}

GuarantorAuditLog = (
    function ($, qaCommon) {
        var that = this,
            guarantorId = $("#VpGuarantorId").val(),
            grid = $("#AuditLogGrid");

        var guarantorAuditLog = {
            BuildGrid: function (guarantorId) {
                var url = "/Guarantors/GetGuarantorAuditLog";
                grid.jqGrid({
                    url: url,
                    postData: { id: guarantorId },
                    jsonReader: { root: 'AuditLog' },
                    mtype: 'POST',
                    datatype: 'json',
                    height: 'auto',
                    emptyrecords: 'No records to display',
                    pager: $("#AuditLogPager"),
                    rowNum: 25,
                    sortname: 'EventDate',
                    sortorder: 'desc',
                    viewrecords: true,
                    colNames: ['Event Date', 'User Name', 'Ip Address', 'Event Type', 'User Agent'],
                    colModel: [
                        { key: false, name: 'EventDate', label: "EventDate", width: 100, align: 'center', sortable: true, resizable: false },
                        { key: false, name: 'UserName', label: "UserName", width: 100, align: 'center', sortable: true, resizable: false },
                        { key: false, name: 'IpAddress', label: "IpAddress", width: 100, align: 'center', sortable: true, resizable: false },
                        { key: false, name: 'EventType', label: "EventType", width: 200, align: 'center', sortable: true, resizable: false },
                        { key: false, name: 'UserAgent', label: "UserAgent", width: 300, align: 'center', sortable: true, resizable: false }
                    ]
                });
            },

            Initialize: function () {
                this.BuildGrid(guarantorId);
            }
        }
        guarantorAuditLog.Initialize();
    }
)(jQuery, qaCommon);


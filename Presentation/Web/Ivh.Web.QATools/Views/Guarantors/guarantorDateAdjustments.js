﻿if (guarantorDateAdjustments === undefined) {
    var guarantorDateAdjustments = {};
}

guarantorDateAdjustments = (
    function ($, qaCommon, sideNav) {
        var that = this;

        that.dateAdjustmentModel = {};

        function buildModel() {
            that.dateAdjustmentModel = {
                guarantorId: parseInt(qaCommon.getParameterByName("guarantorId")),
                datePart: $("input[name=datePart]:checked").val(),
                adjustment: $("#AdjustmentValue").val()
            }
        }

        function resetFields() {
            $("#adjustByDays").prop("checked", true);
            $("#AdjustmentValue").val("");
            $("#AdjustmentValue").focus();
        }


        function handleCustomAlert(data) {
            var html = "<h4 class='pull-left'>Success running, with warnings from SQL:</h4><br /><br />";
            $.each(data, function(i, value) {
                html += value + " <hr>";
            });

            qaCommon.alertMessage("warning",  html, null, true);
        }

        function init() {
            $("input[name=adjustBy").on("click", function () {
                $("#RunAdjustment").prop("disabled", $("#AdjustmentValue").val() === "");
                });
            $("#AdjustmentValue").on("input", function () {
                $("#RunAdjustment").prop("disabled", $(this).val()==="");
            });

            $("#AdjustmentValue").focus();
        
            
            $("#RunPaymentDueDayYesterday").on("click", function (e) {
                e.preventDefault();
                $.ajax({
                    type: "POST",
                    url: "/Guarantors/RollBackStatementPaymentDueDateToYesterdayForGuarantor",
                    data: { guarantorId: parseInt(qaCommon.getParameterByName("guarantorId")) }
                }).done(function(data, textStatus, jqXHR) {
                    if (data) {
                        handleCustomAlert(data);
                    } else {
                        qaCommon.alertMessage("success", "Success running date adjustments.");
                    }
                }).fail(function(jqXHR, textStatus, errorThrown) {
                    qaCommon.alertMessage("danger", "Failure running date adjustments.");
                });
            });
            $("#frmGuarantorDateAdjustment").on("submit", function (e) {
                e.preventDefault();
                buildModel();
                $.ajax({
                    type: "POST",
                    url: "/Guarantors/RunDateAdjustments",
                    data: JSON.stringify(that.dateAdjustmentModel),
                    contentType: "application/json; charset=utf-8"
                }).done(function(data, textStatus, jqXHR) {
                    if (data) {
                        handleCustomAlert(data);
                    } else {
                        qaCommon.alertMessage("success", "Success running date adjustments.");
                    }
                    resetFields();
                }).fail(function(jqXHR, textStatus, errorThrown) {
                    qaCommon.alertMessage("danger", "Failure running date adjustments.");
                });
            });

        }
        init();
    }
)(jQuery, qaCommon, _sideNav);



﻿if (createVisit === undefined) {
    var createVisit = {};
}

createVisit = (
    function ($, qaCommon, sideNav) {
        var that = this;
        that.VisitViewModel = {};
        that.UserName = "";
        function buildModel() {
            var date = $("#DischargeDate").val().split('-'),
                dischargeDate = date[2] + '-' + date[0] + '-' + date[1];
            that.VisitViewModel.VpGuarantorId = $("#VpGuarantorId").val();
            that.VisitViewModel.UserName = $("#UserName").text();
            that.VisitViewModel.BillingApplication = $("input[name=BillingApplication]:checked").val();
            that.VisitViewModel.VisitDescription = $("#VisitDescription").val();
            that.VisitViewModel.PatientFirstName = $("#PatientFirstName").val();
            that.VisitViewModel.PatientLastName = $("#PatientLastName").val();
            that.VisitViewModel.DischargeDate = dischargeDate;
            that.VisitViewModel.HsCurrentBalanceOffsetFromVpBalance = $("#HsCurrentBalanceOffsetFromVPBalance").val();
            that.VisitViewModel.VisitId = 0;
        }

        function resetFields() {
            $("input").val("");
            $("textarea").val("");
            $("#BillingApplication_PB").val('HB');
            $("#BillingApplication_HB").val('PB');
            $("#BillingApplication_HB").prop("checked", true);
            $("#VpGuarantorId").val(that.parms.guarantorId);
            $("#UserName").val(that.VisitViewModel.UserName);
            $("#_Save").attr("disabled", true);
            $("#PatientFirstName").focus();
        }

        function init() {
            debugger;
            var that = this,
                menuList = sideNav.menuList,
                parms = { userName: $("#UserName").text(), guarantorId: that.$("#VpGuarantorId").val(), hasVisits: qaCommon.getParameterByName("hasVisits") === "true" };

            $("#PatientFirstName, #PatientLastName, #VisitDescription, #DischargeDate, #HsCurrentBalanceOffsetFromVPBalance").on("input", function () { $("#_Save").removeAttr("disabled"); });
            $("#DischargeDate").datepicker({
                dateFormat: "mm-dd-yy",
                defaultDate: new Date()
            });

            that.parms = parms;

            menuList.getMenuItemById("CreateVisit").setActive();
            menuList.getMenuItemById("CreateVisit").setActionLink(parms);
            menuList.getMenuItemById("CreateVisit").setVisible();

            menuList.getMenuItemById("CreateVisitTransaction").setActionLink(parms);
            menuList.getMenuItemById("CreateVisitTransaction").setVisible(parms.hasVisits);

            $("#_Save").on("click", function () {
                buildModel();
                $.ajax({
                    type: "POST",
                    url: "/Visits/CreateVisit",
                    data: JSON.stringify(that.VisitViewModel),
                    contentType: "application/json; charset=utf-8"
                }).done(function() {
                    qaCommon.alertMessage("success", "Visit transaction successfully created.");
                    that.parms.hasVisits = true;
                    menuList.getMenuItemById("CreateVisitTransaction").setActionLink(that.parms);
                    sideNav.showMenuItem("CreateVisitTransaction");
                    resetFields();
                    $("#_Save").attr("disabled", true);
                }).fail(function() {
                    qaCommon.alertMessage("danger", "Error Creating Visit transaction.");
                });
            });

            $("#_Cancel").on("click", function () {
                var url = "/Guarantors/Index";
                window.location.href = url;
            });

        }

        init();
    }
)(jQuery, qaCommon, _sideNav);



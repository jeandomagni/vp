﻿if (visit === undefined) {
    var visit = {};
}

visit = (
    function ($, qaCommon, sideNav) {
        var _sideNav = sideNav,
            common = qaCommon,
            that = this;

        that.VisitViewModel = {};
        that.UserName = "";

        init();
        function buildModel() {
            var date = $("#DischargeDate").val().split('-');
            var dischargeDate = date[1] + '/' + date[2] + '/' + date[0];

            that.VisitViewModel.UserName = $("#UserName").val();
            that.VisitViewModel.BillingApplication = $("input[name=BillingApplication]:checked").val();
            that.VisitViewModel.VisitDescription = $("#VisitDescription").val();
            that.VisitViewModel.PatientFirstName = $("#PatientFirstName").val();
            that.VisitViewModel.PatientLastName = $("#PatientLastName").val();
            that.VisitViewModel.DischargeDate = dischargeDate;
            that.VisitViewModel.HsCurrentBalanceOffsetFromVPBalance = $("#HsCurrentBalanceOffsetFromVPBalance").val();
            that.VisitViewModel.VisitID = 0;
        }

        function resetFields() {
            $("input").val("");
            $("textarea").val("");
            $("#BillingApplication_PB").val('HB');
            $("#BillingApplication_HB").val('PB');
            $("#BillingApplication_HB").prop("checked", true);
            $("#VisitPayGuarantorID").val(that.guarantorId);
            $("#UserName").val(that.VisitViewModel.UserName);
            $("#_Save").attr("disabled", true);
            $("#PatientFirstName").focus();
        }

        function init() {
            var that = this;
            that.guarantorId = $("#VisitPayGuarantorID").val();
            $("#PatientFirstName, #PatientLastName, #VisitDescription, #DischargeDate, #HsCurrentBalanceOffsetFromVPBalance").on("input", function () { $("#_Save").removeAttr("disabled"); });
            _sideNav.setLink('CreateVisitViewOptionLink', 'enabled', { userName: $("#UserName").val(), guarantorId: common.getParameterByName('guarantorId') });
            _sideNav.setLink('CreateVisitTransactionViewLink', 'enabled', { userName: $("#UserName").val(), guarantorId: common.getParameterByName('guarantorId') });
            $("#_Save").on("click", function () {
                buildModel();
                $.ajax({
                    type: "POST",
                    url: "/Visits/CreateVisit",
                    data: JSON.stringify(that.VisitViewModel),
                    contentType: "application/json; charset=utf-8",
                    success: function () {
                        qaCommon.createAutoClosingAlert("success", "Visit transaction successfully created.");
                        resetFields();
                        $("#_Save").attr("disabled", true);
                    },
                    error: function () {
                        alert("Create Visit Failure");
                    }
                });
            });

            $("#_Cancel").on("click", function () {
                var url = "/Guarantors/Index";
                window.location.href = url;
            });

        }
}
)(jQuery, qaCommon, _sideNav);



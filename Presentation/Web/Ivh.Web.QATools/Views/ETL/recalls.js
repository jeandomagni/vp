﻿if (recalls === undefined) {
    var recalls = {};
}

recalls = (
    function ($, qaEnums, qaCommon) {
        var recalls = {
            resetFields: function () {
                $("#billingSystemType option[value='-1']").show();
                $("select option[value=1]").attr("selected", true);
                $("#reason").val("");
                $("#visitNumber").val("");
                $("#billingSystemType").focus();
            },

            setRecall: function (visitNumber, billingSystemId, reason) {
                var that = this;
                $.ajax({
                    type: "POST",
                    url: "/Etl/VpIneligible",
                    data: JSON.stringify({
                        visitNumber: visitNumber,
                        billingSystemId: billingSystemId,
                        reason: reason
                    }),
                    contentType: "application/json; charset=utf-8"
                }).done(function(results, status, xhr) {
                    qaCommon.alertMessage("success", "VpIneligible successfully set.");
                    that.resetFields();
                }).fail(function() {
                    qaCommon.alertMessage("danger", "Error setting VpIneligible.");
                });
            },
            setUnRecall: function (visitNumber, billingSystemId) {
                var that = this;
                $.ajax({
                    type: "POST",
                    url: "/Etl/SetVpEligible",
                    data: JSON.stringify({
                        visitNumber: visitNumber,
                        billingSystemId: billingSystemId
                    }),
                    contentType: "application/json; charset=utf-8"
                }).done(function(results, status, xhr) {
                    qaCommon.alertMessage("success", "VpEligible successfully set.");
                    that.resetFields();
                }).fail(function() {
                    qaCommon.alertMessage("danger", "Error setting VpEligible.");
                });
            },

            init: function () {
                var that = this;

                $("#billingSystemType").on("change", function () {
                    $("#billingSystemType option[value='-1']").hide();
                });

                $("#runRecall").on("click", function () {
                    var billingSystemId = parseInt($("#billingSystemType option:selected").val()),
                        visitNumber = $("#visitNumber").val(),
                        reason = $("#reason").val();
                    that.setRecall(visitNumber, billingSystemId, reason);
                });
                $("#runUnRecall").on("click", function () {
                    var billingSystemId = parseInt($("#billingSystemType option:selected").val()),
                        visitNumber = $("#visitNumber").val();
                    that.setUnRecall(visitNumber, billingSystemId, reason);
                });
            }

        }
        recalls.init();
        return recalls;
    }
)(jQuery, qaEnums, qaCommon);


﻿if (setVisitState === undefined) {
    var createVisit = {};
}

createVisit = (
    function ($, qaCommon, sideNav) {
        var _sideNav = sideNav,
            common = qaCommon,
            that = this;

        that.VisitViewModel = {};
        that.UserName = "";

        init();
        function buildModel() {
            var date = $("#DischargeDate").val().split('-');
            var dischargeDate = date[1] + '/' + date[2] + '/' + date[0];

            that.VisitViewModel.UserName = $("#UserName").val();
            that.VisitViewModel.BillingApplication = $("input[name=BillingApplication]:checked").val();
            that.VisitViewModel.VisitDescription = $("#VisitDescription").val();
            that.VisitViewModel.PatientFirstName = $("#PatientFirstName").val();
            that.VisitViewModel.PatientLastName = $("#PatientLastName").val();
            that.VisitViewModel.DischargeDate = dischargeDate;
            that.VisitViewModel.HsCurrentBalanceOffsetFromVPBalance = $("#HsCurrentBalanceOffsetFromVPBalance").val();
            that.VisitViewModel.VisitID = 0;
        }

        function resetFields() {
            $("input").val("");
            $("textarea").val("");
            $("#BillingApplication_PB").val('HB');
            $("#BillingApplication_HB").val('PB');
            $("#BillingApplication_HB").prop("checked", true);
            $("#VpGuarantorId").val(that.guarantorId);
            $("#UserName").val(that.VisitViewModel.UserName);
            $("#_Save").attr("disabled", true);
            $("#PatientFirstName").focus();
        }

        function init() {
            var that = this,
                hasVisits = common.getParameterByName('hasVisits') === 'true';

            that.guarantorId = $("#VpGuarantorId").val();
            
            $("#PatientFirstName, #PatientLastName, #VisitDescription, #DischargeDate, #HsCurrentBalanceOffsetFromVPBalance").on("input", function () { $("#_Save").removeAttr("disabled"); });
            _sideNav.setLink('CreateVisitViewOptionLink', 'show', { userName: $("#UserName").text(), guarantorId: that.guarantorId, hasVisits: hasVisits });
            _sideNav.setLink('CreateVisitTransactionViewLink', (hasVisits ? 'show' : 'hidden'), { userName: $("#UserName").text(), guarantorId: that.guarantorId, hasVisits: hasVisits });

            $("#_Save").on("click", function () {
                buildModel();
                $.ajax({
                    type: "POST",
                    url: "/Visits/CreateVisit",
                    data: JSON.stringify(that.VisitViewModel),
                    contentType: "application/json; charset=utf-8"
                }).done(function() {
                    qaCommon.alertMessage("success", "Visit transaction successfully created.");
                    resetFields();
                    $("#_Save").attr("disabled", true);
                }).fail(function() {
                    qaCommon.alertMessage("danger", "Error Creating Visit transaction.");
                });
            });

            $("#_Cancel").on("click", function () {
                var url = "/Guarantors/Index";
                window.location.href = url;
            });

        }
}
)(jQuery, qaCommon, _sideNav);



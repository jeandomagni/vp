﻿(function ($) {

    var bindGrid = function (json) {

        //
        var grid = $('#jqGrid');
        var gridDefaultSortOrder = 'asc';
        var gridDefaultSortName = 'SourceSystemKey';

        //
        grid.jqGrid({
            datatype: 'local',
            data: json,
            loadonce: true,
            autoencode: true,
            autowidth: true,
            height: 345,
            rowNum: 15,
            pager: 'jqGridPager',
            sortname: gridDefaultSortName,
            sortorder: gridDefaultSortOrder,
            loadComplete: function (data) {

                var gridContainer = grid.parents('.ui-jqgrid:eq(0)');
                var gridNoRecords = $('#jqGridNoRecords');

                if (data != null && data.rows.length > 0) {

                    gridContainer.show();
                    gridNoRecords.removeClass('in');

                } else {

                    gridContainer.hide();
                    gridNoRecords.addClass('in');

                }

            },
            colModel: [
                { label: 'Billing System', name: 'BillingSystem.BillingSystemName', sorttype: 'text' },
                { label: 'Source System Key', name: 'SourceSystemKey', sorttype: 'text' },
                { label: 'Description', name: 'VisitDescription', sorttype: 'text' },
                { label: 'Discharge Date', name: 'DischargeDate', formatter: 'date', sorttype: 'date' },
                { label: 'HS Current Balance', name: 'HsCurrentBalance', sorttype: 'currency', formatter: 'currency', formatoptions: { decimalSeparator: '.', thousandsSeparator: ',', decimalPlaces: 2, prefix: '$' } },
                { label: 'Patient First Name', name: 'PatientFirstName', sorttype: 'text' },
                {
                    label: 'Action', name: 'VisitId', sortable: false, formatter: function (cellValue, options, rowObject) {
                        return '<a href="/Etl/ViewVisit/' + cellValue + '" title="Details" style="display: block; text-align: center; text-decoration: underline;">Details</span>';
                    }
                }
            ]
        });

    };

    $(document).ready(function() {

        $.post('/Etl/HsGuarantorVisitsData', { hsGuarantorId: $('#HsGuarantorId').val() }, function(data) {
            bindGrid(data);
        }, 'json');

    });

})(jQuery)
﻿$("#GenerateSsoLink").click(function() {
    var myData = { DateOfBirth: $("#dob").val(), SsoSsk: $("#SsoSsk").val(), HsGuarantorId: $("#SsoHsGuarantorId").val() };

    $.ajax({
        url: $("#SsoHsPostUrl").val(),
        type: 'POST',
        data: myData
    }).done(function(data) {
        $("#generatedLinks").append('<li>Generated link will expire on ' + data.UrlExpiration + ' please click: <a target="_blank" href="' + data.SsoUrl + '">here</a></li>');
    }).fail(function(obj, status, err) {
        console.log("error");
        console.log(obj);
        console.log(status);
        console.log(err);
    });
});
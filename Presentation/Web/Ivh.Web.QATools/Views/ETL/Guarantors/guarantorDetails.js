﻿    if (guarantorDetails === undefined) {
    var guarantorDetails = {};
}

guarantorDetails = (
    function ($, qaEnums, qaCommon, sideNav) {

        function forceRefresh(elId, data) {
            var grid = $("#" + elId);
            grid.jqGrid("clearGridData");
            grid.jqGrid("setGridParam", { data: data });
            grid.trigger("reloadGrid");
        }


        function bindVpGuarantorHsMatchGrid(data) {
            var grid = $("#VpGuarantorHsMatchesGrid");

            grid.jqGrid({
                datatype: "local",
                data: data,
                loadonce: false,
                autoencode: true,
                autowidth: true,
                height: 50,
                rowNum: 15,
                pager: "#VpGuarantorHsMatchesPager",
                onCellSelect: function (rowid, index, contents, event) {
                    var rowData = $(grid).getRowData(rowid);
                },
                colModel: [
                    { key: true, name: "VpGuarantorId", index: "VpGuarantorId", width: 10, align: "center", sortable: true, label: "VPGID" },
                    { key: false, name: "IsActive", index: "IsActive", width: 10, align: "center", sortable: true, label: "Is Active" },
                    { key: false, name: "MatchedOn", index: "MatchedOn", width: 20, align: "center", sortable: true, label: "Matched On" },
                    { key: false, name: "UnmatchedOn", index: "UnmatchedOn", width: 30, align: "center", sortable: true, label: "Unmatched On" },
                    { key: false, name: "LastName", index: "LastName", width: 20, align: "center", sortable: true, label: "Last Name" },
                    { key: false, name: "DOB", index: "DOB", width: 10, align: "center", sortable: true },
                    { key: false, name: "Ssn4", index: "Ssn4", width: 10, align: "left", sortable: true, hidden: true, label: "SSN4" },
                    { key: false, name: "PostalCode", index: "PostalCode", width: 10, align: "left", sortable: true, label: "Postal Code" },
                    
                ]
            });

        }

        function bindHsGuarantorEventsGrid(data) {
            var grid = $("#HsGuarantorEventsGrid");

            // HsGuarantorEventId, HsGuarantorId, HsGuarantorEventTypeId, EventDateTime, ProcessedOn
            grid.jqGrid({
                datatype: "local",
                data: data,
                loadonce: false,
                autoencode: true,
                autowidth: true,
                rowNum: 15,
                pager: "HsGuarantorEventsPager",
                colModel: [
                    { key: true, name: "InsertDate", index: "InsertDate", width: 20, align: "center", sortable: true, label: "Insert Date" },
                    { key: true, name: "VpOutboundHsGuarantorMessageType", index: "VpOutboundHsGuarantorMessageType", width: 20, align: "center", sortable: true, label: "Event Type" },
                    { key: true, name: "ProcessededDate", index: "ProcessedDate", width: 20, align: "center", sortable: true, label: "Processed Date" }
                ]
            });

        }

        function getVpGuarantorHsMatches(guarantorId) {
            var url = "/Etl/Guarantors/GetVpGuarantorHsMatches/" + guarantorId;
            $.ajax({
                type: "GET",
                url: url,
                contentType: "application/json;charset=utf-8",
                dataType: "json"
            }).done(function(data) {
                bindVpGuarantorHsMatchGrid(data);
                forceRefresh("VpGuarantorHsMatchesGrid", data);
            }).fail(function(response) {
                qaCommon.alertMessage("danger", "Error Getting VpGuarantorHsMatches");
            });
        }

        function getHsGuarantorEvents(guarantorId) {
            var url = "/Etl/Guarantors/GetHsGuarantorEvents/" + guarantorId;
            $.ajax({
                type: "GET",
                url: url,
                contentType: "application/json;charset=utf-8",
                dataType: "json"
            }).done(function(data) {
                bindHsGuarantorEventsGrid(data);
                forceRefresh("HsGuarantorEventsGrid", data);
            }).fail(function(response) {
                qaCommon.alertMessage("danger", "Error Getting HsGuarantor Events");
            });
        }

        function populateBillingSystemTypes(el) {
            var url = "/Etl/Guarantors/HsGetBillingSystemTypes";
            $.ajax({
                type: "GET",
                url: url,
                contentType: "application/json;charset=utf-8",
                dataType: "json"
            }).done(function(data) {
                $.each(data, function(key, value) {
                    $(el)
                        .append($("<option></option>")
                            .attr("value", value.BillingSystemId)
                            .text(value.BillingSystemName));
                });
            }).fail(function(response) {
                qaCommon.alertMessage("danger", "Error Getting HsBillingSystemTypes");
            });
        }

        function recallAllVisits(hsGuarantorId) {
            $.post('/Etl/RecallAllVisits', { hsGuarantorId: hsGuarantorId }, function() {
                window.location.reload();
            });
        }

        function init() {
             var hsGuarantorId = parseInt($("#HsGuarantorId").val());

            $('#btnRecallAllVisits').on('click', function (e) {
                recallAllVisits(hsGuarantorId);
            });

            $('#btnHsGuarantorDetailsUpdate').on('click', function () {
                $('form').attr('action', '/Etl/HsGuarantorDetailsUpdate');
                $('#submitButton').click();
            });

            $('#btnHsGuarantorUnmatch').on('click', function () {
                $('form').attr('action', '/Etl/UnmatchGuarantor');
                $('#submitButton').click();
            });
            
            populateBillingSystemTypes($("#billingSystemType"));
            getVpGuarantorHsMatches(hsGuarantorId);
            getHsGuarantorEvents(hsGuarantorId);
        }

        init();
    }
)(jQuery, qaEnums, qaCommon, _sideNav);


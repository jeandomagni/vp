﻿if (guarantor === undefined) {
    var guarantor = {};
}

guarantor = (
    function ($) {

        function init() {
            var gridUrl = "/Etl/GetHsGuarantors";
            $('#frmFilter').on('submit', function (e) {
                e.preventDefault(); runSearch(gridUrl); });

            runSearch(gridUrl);

            $('#Last-Name').focus();
        }

        function buildModel() {
            var model = {}
            model.LastName = $("#Last-Name").val();
            model.HsSourceKey = $("#HS-Source-Key").val();
            model.VPGID = parseInt($("#VPGID").val());
            model.VpRegistered = $("#VP-Registered").prop("checked");
            model.VpUnregistered = $("#VP-Unregistered").prop("checked");
                    return model;
        }

        function forceRefresh(json) {
            var grid = $("#myGrid");
            grid.jqGrid('clearGridData');
            grid.jqGrid('setGridParam', { data: json });
            grid.trigger('reloadGrid');

        }

        function detailLink(cellValue, options, rowdata, action) {
            return "<a href=\"javascript: window.location.href='/Etl/Guarantors/HsGuarantorDetailsView/" + rowdata.HsGuarantorID + "';\"><u>Details</u></a>";
        }
        
        function visitsLink(cellValue, options, rowdata, action) {
            var html = "<a href=\"javascript: window.location.href='/Etl/HsGuarantorVisits/" + rowdata.HsGuarantorID + "'; \"><u>" + rowdata.VisitCount + " Visit" + (rowdata.VisitCount > 1 ? "s" : "") + "</u></a>";
            return html;
        }

        function translateRegistered(cellValue, options, rowdata, action) {
            return rowdata.VpRegistered === true ? "Yes" : "No";
        }

        /*function getVPGID (cellValue, options, rowdata, action) {
            return $("#VPGID").val();
        }*/

        function bindGrid(json) {
            //
            var grid = $('#myGrid');

            //
            grid.jqGrid({
                datatype: 'jsonstring',
                data: json,
                loadonce: false,
                autoencode: true,
                autowidth: true,
                height: 'auto',
                rowNum: 100,
                pager: 'myPager',
                loadcomplete: function() {
                },
                colModel: [
                    { key: true, name: "HsGuarantorID", index: "HsGuarantorID", width: 95, align: "left", sortable: true },
                    { key: false, name: "FirstName", index: "FirstName", width: 105, align: "left", sortable: true, label: "First Name"},
                    { key: false, name: "LastName", index: "LastName", width: 105, align: "left", sortable: true, label: "Last Name" },
                    { key: false, name: "Registered", index: "VpRegistered", width: 55, align: "center", sortable: true, formatter: translateRegistered },
                    { key: false, name: "VpGuarantorID", index: "VpGuarantorID", label: 'VPGID', width: 45, align: "center", sortable: true },
                    { key: false, name: "Visits", index: "Visits", align: "center", width: 50, formatter: visitsLink },
                    { key: false, name: "Details", index: "Details", align: "center", width: 50, formatter: detailLink }                ]
            });

        }

        function runSearch(url) {
            $.post(
                url,
                buildModel(),
                function (data) {
                    bindGrid(data);
                    forceRefresh(data);
                },
                "json");
        }

        init();
    }
)(jQuery);


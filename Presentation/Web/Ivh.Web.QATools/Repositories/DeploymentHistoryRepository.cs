﻿using Ivh.Web.QATools.Interfaces;
using NHibernate;

namespace Ivh.Web.QATools.Repositories
{
    using System.Collections.Generic;
    using Common.Data;
    using Common.Data.Connection.Connections;
    using Common.Data.Interfaces;
    using Entities;

    public class DeploymentHistoryRepository : RepositoryBase<DeploymentHistory, VisitPay>, IDeploymentHistoryRepository
    {
        public DeploymentHistoryRepository(ISessionContext<VisitPay> sessionContext) : base(sessionContext)
        {
        }

        public IList<DeploymentHistory> GetDeploymentHistory()
        {
            IList<DeploymentHistory> query = this.Session.QueryOver<DeploymentHistory>()
                .OrderBy(d => d.DeploymentDate).Desc.List();

            return query;
        }

    }
}
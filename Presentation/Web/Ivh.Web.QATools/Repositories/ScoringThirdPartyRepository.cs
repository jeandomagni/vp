﻿namespace Ivh.Web.QATools.Repositories
{
    using Common.Data;
    using Common.Data.Connection.Connections;
    using Common.Data.Interfaces;
    using Interfaces;
    using Models;
    using NHibernate;

    public class ScoringThirdPartyRepository : RepositoryBase<ScoringThirdPartyResponse, CdiEtl>, IScoringThirdPartyRepository
    {
        public ScoringThirdPartyRepository(ISessionContext<CdiEtl> sessionContext) : base(sessionContext)
        {
        }
    }
}
﻿namespace Ivh.Web.QATools.Repositories
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using AutoMapper;
    using Common.Data;
    using Common.Data.Connection.Connections;
    using Common.Data.Interfaces;
    using Domain.FinanceManagement.FinancePlan.Entities;
    using Domain.FinanceManagement.FinancePlan.Interfaces;
    using Interfaces;
    using Models;
    using NHibernate;
    using Provider.FinanceManagement.FinancePlan;

    public class QAInterestRateRepository : RepositoryBase<InterestRateViewModel, VisitPay>, IQAInterestRateRepository
    {
        private readonly IInterestRateRepository _interestRateRepository;

        public QAInterestRateRepository(ISessionContext<VisitPay> session)
            : base(session)
        {
            this._interestRateRepository = new InterestRateRepository(session);
        }


        public async Task CreateInterestRateAsync(InterestRateViewModel interestRate)
        {
            await Task.Run(() =>
            {
                using (UnitOfWork unitOfWork = new UnitOfWork(this.Session))
                {
                    this._interestRateRepository.InsertOrUpdate(Mapper.Map<InterestRateViewModel, InterestRate>(interestRate));
                    unitOfWork.Commit();
                }
            });
        }

        public async Task DeleteInterestRateAsync(int id)
        {
            await Task.Run(() =>
            {
                using (UnitOfWork unitOfWork = new UnitOfWork(this.Session))
                {
                    InterestRate interestRate = this._interestRateRepository.GetById(id);
                    this._interestRateRepository.Delete(interestRate);
                    unitOfWork.Commit();
                }
            });
        }

        public async Task<IEnumerable<InterestRateViewModel>> GetInterestRateAsync()
        {
            return await Task.Run(() => this._interestRateRepository.GetQueryable().Select(Mapper.Map<InterestRate, InterestRateViewModel>));
        }
    }
}
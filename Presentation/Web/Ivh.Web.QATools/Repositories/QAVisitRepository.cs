﻿namespace Ivh.Web.QATools.Repositories
{
    using Ivh.Common.Base.Utilities.Extensions;
    using Ivh.Common.Data;
    using Ivh.Web.QATools.Interfaces;
    using NHibernate;
    using System;
    using System.Text;
    using Common.Data.Connection.Connections;
    using Common.Data.Interfaces;
    using Visit = Ivh.Web.QATools.Models.VisitViewModel;
    using VisitTransaction = Ivh.Web.QATools.Models.VisitTransactionViewModel;

    public class QAVisitRepository : RepositoryBase<Visit, VisitPay>, IQAVisitRepository
    {
        public QAVisitRepository(ISessionContext<VisitPay> sessionContext)
            : base(sessionContext)
        {
        }
        public void CreateVisit(Visit visit) {
            StringBuilder sql = new StringBuilder();
            sql.Append("exec [qa].[CreateVisit] ")
                .Append("@UserName=:UserName, ")
                .Append("@BillingApplication=:BillingApplication, ")
                .Append("@VisitDescription=:VisitDescription, ")
                .Append("@PatientFirstName=:PatientFirstName, ")
                .Append("@PatientLastName=:PatientLastName, ")
                .Append("@DischargeDate=:DischargeDate, ")
                .Append("@HsCurrentBalanceOffsetFromVPBalance=:HsCurrentBalanceOffsetFromVPBalance, ")
                .Append("@VisitID=:VisitID output");

            var query = this.Session.CreateSQLQuery(sql.ToString());
            query.SetString("UserName", visit.UserName);
            query.SetString("BillingApplication", visit.BillingApplication);
            query.SetString("VisitDescription", visit.VisitDescription);
            query.SetString("PatientFirstName",visit.PatientFirstName);
            query.SetString("PatientLastName", visit.PatientLastName);
            query.SetDateTime("DischargeDate", visit.DischargeDate);
            query.SetDecimal("HsCurrentBalanceOffsetFromVPBalance", visit.HsCurrentBalanceOffsetFromVpBalance);
            query.SetInt32("VisitID", visit.VisitId);
            query.ExecuteUpdate();
        }


        public void CreateVisitTransaction(VisitTransaction transaction)
        {
            StringBuilder sql = new StringBuilder();
            sql.Append("exec [qa].[CreateVisitTransaction] ")
                .Append("@UserName=:UserName, ")
                .Append("@VisitID=:VisitID, ")
                .Append("@TransactionDate=:TransactionDate, ")
                .Append("@TransactionAmount=:TransactionAmount, ")
                .Append("@VpTransactionTypeID=:VpTransactionTypeID, ")
                .Append("@TransactionDescription=:TransactionDescription, ")
                .Append("@InsertDate=:InsertDate, ")
                .Append("@RelatedChangeToVisitStateId=:RelatedChangeToVisitStateId");

            var query = this.Session.CreateSQLQuery(sql.ToString());
            query.SetString("UserName", transaction.UserName);
            query.SetInt32("VisitID", transaction.VisitID);
            query.SetDateTime("TransactionDate", transaction.TransactionDate);
            query.SetDouble("TransactionAmount", transaction.TransactionAmount);
            query.SetInt32("VpTransactionTypeID", transaction.VpTransactionTypeID);
            query.SetString("TransactionDescription", transaction.TransactionDescription);
            query.SetDateTime("InsertDate", transaction.InsertDate.ToDateTime(TimeZone.CurrentTimeZone.StandardName));
            query.SetInt32("RelatedChangeToVisitStateId", transaction.RelatedChangeToVisitStateId);
            query.ExecuteUpdate();
        }

    }
}

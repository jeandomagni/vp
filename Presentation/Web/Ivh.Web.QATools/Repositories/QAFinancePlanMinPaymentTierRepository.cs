﻿namespace Ivh.Web.QATools.Repositories
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using AutoMapper;
    using Common.Data;
    using Common.Data.Connection.Connections;
    using Common.Data.Interfaces;
    using Domain.FinanceManagement.FinancePlan.Entities;
    using Domain.FinanceManagement.FinancePlan.Interfaces;
    using Interfaces;
    using Models;
    using NHibernate;
    using Provider.FinanceManagement.FinancePlan;

    public class QAFinancePlanMinPaymentTierRepository : RepositoryBase<FinancePlanMinPaymentTierViewModel, VisitPay>, IQAFinancePlanMinPaymentTierRepository
    {
        private readonly IFinancePlanMinimumPaymentTierRepository _financePlanMinimumPaymentTierRepository;

        public QAFinancePlanMinPaymentTierRepository(ISessionContext<VisitPay> sessionContext)
            : base(sessionContext)
        {
            this._financePlanMinimumPaymentTierRepository = new FinancePlanMinimumPaymentTierRepository(sessionContext);
        }

        public async Task CreateFinancePlanMinPaymentTierAsync(FinancePlanMinPaymentTierViewModel financePlanMinPaymentTierViewModel)
        {
            await Task.Run(() =>
            {
                using (UnitOfWork unitOfWork = new UnitOfWork(this.Session))
                {
                    this._financePlanMinimumPaymentTierRepository.InsertOrUpdate(Mapper.Map<FinancePlanMinPaymentTierViewModel, FinancePlanMinimumPaymentTier>(financePlanMinPaymentTierViewModel));
                    unitOfWork.Commit();
                }
            });
        }

        public async Task DeleteFinancePlanMinPaymentTierAsync(int id)
        {
            await Task.Run(() =>
            {
                using (UnitOfWork unitOfWork = new UnitOfWork(this.Session))
                {
                    FinancePlanMinimumPaymentTier financePlanMinimumPaymentTier = this._financePlanMinimumPaymentTierRepository.GetById(id);
                    this._financePlanMinimumPaymentTierRepository.Delete(financePlanMinimumPaymentTier);
                    unitOfWork.Commit();
                }
            });
        }

        public Task<IEnumerable<FinancePlanMinPaymentTierViewModel>> GetFinancePlanMinPaymentTierAsync()
        {
            return Task.FromResult(this._financePlanMinimumPaymentTierRepository.GetQueryable().Select(Mapper.Map<FinancePlanMinimumPaymentTier, FinancePlanMinPaymentTierViewModel>));
        }
    }
}
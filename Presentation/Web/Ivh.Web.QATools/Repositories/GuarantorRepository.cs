﻿namespace Ivh.Web.QATools.Repositories
{
    using Models;
    using Ivh.Common.Data;
    using Ivh.Web.QATools.Interfaces;
    using NHibernate;
    using System;
    using System.Text;
    using Common.Data.Connection.Connections;
    using Common.Data.Interfaces;
    using NHibernate.Transform;


    public class CreateGuarantorResult
    {
        public int VisitPayUserId { get; set; }
        public int VpGuarantorId { get; set; }
    }

    public class GuarantorRepository : RepositoryBase<GuarantorViewModel, VisitPay>, IGuarantorRepository
    {
        public GuarantorRepository(ISessionContext<VisitPay> sessionContext)
            : base(sessionContext)
        {
        }

        public CreateGuarantorResult CreateGuarantor(GuarantorViewModel guarantor)
        {
            StringBuilder sql = new StringBuilder();
            sql.Append("exec [qa].[CreateVisitPayUserGuarantor] ")
                .Append("@UserName=:UserName, ")
                .Append("@FirstName=:FirstName, ")
                .Append("@LastName=:LastName, ")
                .Append("@PaymentDueDate=:PaymentDueDay, ")
                .Append("@RegistrationDate=:RegistrationDate, ")
                .Append("@VisitPayUserId=:VisitPayUserId output, ")
                .Append("@VpGuarantorId=:VpGuarantorId output");

            ISQLQuery query = this.Session.CreateSQLQuery(sql.ToString());
            query.SetString("UserName", guarantor.UserName);
            query.SetString("FirstName", guarantor.FirstName);
            query.SetString("LastName", guarantor.LastName);
            query.SetInt16("PaymentDueDay", guarantor.PaymentDueDay);
            query.SetDateTime("RegistrationDate", guarantor.RegistrationDate);
            query.SetInt32("VisitPayUserId", guarantor.VisitPayUserId);
            query.SetInt32("VpGuarantorId", guarantor.VpGuarantorId);
            return query.SetResultTransformer(Transformers.AliasToBean<CreateGuarantorResult>()).UniqueResult<CreateGuarantorResult>();
        }
    }
}

﻿namespace Ivh.Web.QATools.Hubs
{
    using System;
    using System.Threading.Tasks;
    using Common.Cache;
    using Microsoft.AspNet.SignalR;
    using Models.MockEndpoint;
    using Newtonsoft.Json;

    public class MockEndpointHub : Hub
    {
        private readonly Lazy<IDistributedCache> _distributedCache;

        public MockEndpointHub(Lazy<IDistributedCache> distributedCache)
        {
            this._distributedCache = distributedCache;
        }

        public async Task OutgoingResponse(InterceptViewModel model)
        {
            string json = JsonConvert.SerializeObject(model.RespondWith);

            await this._distributedCache.Value.SetStringAsync(model.KeyResponse, json);
            this._distributedCache.Value.ReleaseLock(model.KeyRequest);
        }
    }
}
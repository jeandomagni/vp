﻿namespace Ivh.Web.QATools.Hubs
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Application.Qat.Common.Dtos;
    using Application.Qat.Common.Enums;
    using Application.Qat.Common.Interfaces;
    using AutoMapper;
    using Common.Base.Constants;
    using Common.Base.Enums;
    using Common.Cache;
    using Microsoft.AspNet.SignalR;
    using Models.MockPaymentProcessor;
    using Common.Base.Utilities.Extensions;
    using Common.VisitPay.Constants;
    using Common.VisitPay.Enums;

    public class MockPaymentProcessorHub : Hub
    {
        private readonly Lazy<IPaymentProcessorInterceptorApplicationService> _paymentProcessorInterceptorApplicationService;

        public MockPaymentProcessorHub(
            IDistributedCache distributedCache,
            Lazy<IPaymentProcessorInterceptorApplicationService> paymentProcessorInterceptorApplicationService)
        {
            this._paymentProcessorInterceptorApplicationService = paymentProcessorInterceptorApplicationService;

            distributedCache.Subscribe(PaymentInterceptorCacheKeys.Messaging.IsActivatedChannel, this.SendIsActivated);
            distributedCache.Subscribe(PaymentInterceptorCacheKeys.Messaging.PaymentComplete, this.SendPaymentComplete);
            distributedCache.Subscribe(PaymentInterceptorCacheKeys.Messaging.PaymentQueued, this.SendPaymentQueued);
        }

        // incoming
        public void CompletePaymentRequest(string requestGuid, PpiResponseTypeEnum responseTypeEnum, char? responseAvsCode)
        {
            this._paymentProcessorInterceptorApplicationService.Value.CompletePaymentRequest(new Guid(requestGuid), responseTypeEnum, responseAvsCode);
        }

        // incoming, outgoing
        public void SetDefaults(int delayMs, bool defaultSuccessful)
        {
            this._paymentProcessorInterceptorApplicationService.Value.SetDelayMs(delayMs);
            this._paymentProcessorInterceptorApplicationService.Value.SetDefaultSuccessful(defaultSuccessful);

            this.Clients.All.defaultsSet(delayMs, defaultSuccessful);
        }

        // incoming, outgoing
        public void SetAchAction(string requestGuid, AchFileTypeEnum fileType, DateTime processDate, AchReturnCodeEnum? returnCode, decimal? amount)
        {
            PpiRequestDto request = this._paymentProcessorInterceptorApplicationService.Value.SetAchAction(new Guid(requestGuid), fileType, processDate, returnCode, amount);

            this.Clients.All.AchActionSet(Mapper.Map<MockAchViewModel>(request));
        }

        // incoming, outgoing
        public void AchProcessed(IList<string> requestsProcessed)
        {
            IList<PpiRequestDto> processed = this._paymentProcessorInterceptorApplicationService.Value.QueryRequestQueue(new PpiRequestFilterDto
            {
                RequestGuids = requestsProcessed.Select(x => new Guid(x)).ToList()
            });

            this.Clients.All.achProcessed(Mapper.Map<IList<MockAchViewModel>>(processed));
        }

        // outgoing
        private void SendIsActivated(string isActivated)
        {
            bool b;
            bool.TryParse(isActivated, out b);

            this.Clients.All.isActivated(b);
        }

        // outgoing
        private void SendPaymentComplete(string requestGuid)
        {
            PpiRequestDto request = this._paymentProcessorInterceptorApplicationService.Value.GetPaymentRequest(new Guid(requestGuid));

            if (request == null)
            {
                return;
            }

            this.Clients.All.paymentComplete(Mapper.Map<MockPaymentViewModel>(request));

            if (request.VpPaymentMethodType.HasValue && request.VpPaymentMethodType.Value.IsInCategory(PaymentMethodTypeEnumCategory.Ach) &&
                request.IsSuccess &&
                request.Action.ToUpper() == "SALE")
            {
                this.Clients.All.achQueued(Mapper.Map<MockAchViewModel>(request));
            }
        }

        // outgoing
        private void SendPaymentQueued(string requestGuid)
        {
            PpiRequestDto request = this._paymentProcessorInterceptorApplicationService.Value.GetPaymentRequest(new Guid(requestGuid));

            if (request != null)
            {
                this.Clients.All.paymentQueued(Mapper.Map<MockPaymentViewModel>(request));
            }
        }
    }
}
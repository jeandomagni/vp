﻿namespace Ivh.Web.QATools
{
    using System;
    using Microsoft.AspNet.Identity;
    using Owin;
    using System.Web.Mvc;
    using Application.Core.Common.Interfaces;
    using Common.Web.Cookies;
    using Common.Web.Encryption;
    using Common.Web.Extensions;
    using Domain.Settings.Interfaces;
    using Microsoft.Owin;
    using Microsoft.Owin.Cors;
    using Microsoft.Owin.Security.Cookies;
    using Microsoft.Owin.Security.DataHandler;
    using Microsoft.Owin.Security.OAuth;

    public partial class Startup
    {
        public static OAuthBearerAuthenticationOptions OAuthBearerAuthenticationOptions;

        // For more information on configuring authentication, please visit http://go.microsoft.com/fwlink/?LinkId=301864
        public void ConfigureAuth(IAppBuilder app)
        {
            IApplicationSettingsService settingsService = DependencyResolver.Current.GetService<IApplicationSettingsService>();
            ISecurityStampValidatorApplicationService securityStampValidatorApplicationService = DependencyResolver.Current.GetService<ISecurityStampValidatorApplicationService>();
            // Enable the application to use a cookie to store information for the signed in user
            // and to use a cookie to temporarily store information about a user logging in with a third party login provider
            // Configure the sign in cookie
            app.UseCookieAuthentication(CookieAuthenticationOptionsFactory.GetCookieAuthenticationOptions(
                cookieName: CookieNames.QATools.Authentication,
                loginPath: "/Account/Login",
                validateInterval: TimeSpan.FromMinutes(30),
                onValidateIdentity: securityStampValidatorApplicationService.OnValidateIdentity(validateInterval: TimeSpan.FromMinutes(30)),
                onApplyRedirect: ctx =>
                {
                    if (ctx.Request.IsAjaxRequest() || IsApiResponse(ctx.Response))
                    {
                        return;
                    }

                    ctx.Response.Redirect(ctx.RedirectUri); // web login (default)
                }));
            app.UseExternalSignInCookie(DefaultAuthenticationTypes.ExternalCookie);
            OAuthBearerAuthenticationOptions = new Microsoft.Owin.Security.OAuth.OAuthBearerAuthenticationOptions()
            {
                AccessTokenFormat = new TicketDataFormat(new AESThenHmacTicketProtector(settingsService.HealthEquityOAuthCryptKey.Value, settingsService.HealthEquityOAuthAuthKey.Value)),
            };
            app.UseOAuthBearerAuthentication(OAuthBearerAuthenticationOptions);
            app.UseCors(CorsOptions.AllowAll);
        }

        private static bool IsApiResponse(IOwinResponse response)
        {
            IHeaderDictionary responseHeader = response.Headers;

            if (responseHeader == null)
            {
                return false;
            }

            return (responseHeader["Suppress-Redirect"] == "True");
        }
    }
}
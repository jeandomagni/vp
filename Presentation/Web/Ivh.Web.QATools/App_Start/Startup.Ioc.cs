﻿namespace Ivh.Web.QATools
{
    using System.Threading.Tasks;
    using System.Web.Mvc;
    using Application.Qat.Mappings;
    using Autofac.Integration.SignalR;
    using Common.DependencyInjection;
    using Common.DependencyInjection.Registration;
    using Common.Web.Extensions;
    using IocSetup;
    using Mappings;
    using Microsoft.AspNet.SignalR;
    using Owin;

    public partial class Startup
    {
        public void ConfigureIoc(IAppBuilder app)
        {
            Task mappingTasks = Task.Run(() =>
            {
                ServiceBusMessageSerializationMappingBase.ConfigureMappings();

                using (MappingBuilder mappingBuilder = new MappingBuilder())
                {
                    mappingBuilder
                        .WithBase()
                        .WithProfile<AutomapperProfile>()
                        .WithProfile<QaToolsMappingProfile>();
                }
            });

            // signalr hub config
            HubConfiguration hubConfiguration = new HubConfiguration();

            IvinciContainer.Instance.RegisterComponent(new RegisterQaToolsMvc(app, hubConfiguration));

            System.Web.Mvc.IDependencyResolver resolver = IvinciContainer.Instance.GetResolver();
            DependencyResolver.SetResolver(resolver);

            app.UseAutofacMiddleware(IvinciContainer.Instance.Container());
            app.UseAutofacMvc();

            // signalr
            hubConfiguration.Resolver = new AutofacDependencyResolver(IvinciContainer.Instance.Container());
            app.MapSignalR("/signalr", hubConfiguration);

            mappingTasks.Wait();
        }
    }
}
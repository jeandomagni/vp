﻿namespace Ivh.Web.QATools
{
    using System;
    using System.Linq;
    using System.Web.Mvc;
    using System.Web.Routing;
    using Common.Base.Utilities.Extensions;
    using Domain.Settings.Enums;

    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");
            routes.IgnoreRoute("{*x}", new { x = @".*\.asmx(/.*)?" }); 
            routes.IgnoreRoute("content/client/{*image}", new { image = @".*\.(png|gif|jpg|jpeg|pdf)(/.)?" });

            #region "Jobs"

            routes.MapRoute("RunScheduledPaymentProcessor", "Jobs/RunScheduledPaymentProcessor/{parms}", new { controller = "Jobs", action = "RunScheduledPaymentProcessor", parms = UrlParameter.Optional }
                );

            routes.MapRoute("StatementProcessing", "Jobs/StatementProcessing/{date}", new { controller = "Jobs", action = "StatementProcessing", date = UrlParameter.Optional }
                );

            #endregion

            #region "ETL/HS"

            routes.MapRoute("AddHsGuarantorAcknowledgementEvent", "Etl/Guarantors/AddHsGuarantorAcknowledgementEvent/{id}", new { controller = "Etl", action = "AddHsGuarantorAcknowledgementEvent", id = UrlParameter.Optional }
                );

            routes.MapRoute("AddHsGuarantorRegMatches", "Etl/Guarantors/AddHsGuarantorRegMatch", new { controller = "Etl", action = "AddHsGuarantorRegMatch" }
                );

            routes.MapRoute("DeleteHsGuarantorRegistrationMatch", "Etl/Guarantors/DeleteHsGuarantorRegistrationMatch", new { controller = "Etl", action = "DeleteHsGuarantorRegistrationMatch" }
                );

           routes.MapRoute("GetHsGuarantorDetails", "Etl/Guarantors/HsGuarantorDetailsView/{id}", new { controller = "Etl", action = "HsGuarantorDetailsView" }
                );

            routes.MapRoute("GetHsGuarantorEvents", "Etl/Guarantors/GetHsGuarantorEvents/{id}", new { controller = "Etl", action = "GetHsGuarantorEvents", id = UrlParameter.Optional }
                );

            routes.MapRoute("GetHsGuarantorEventTypes", "Etl/Guarantors/EtlGetHsGuarantorEventTypes", new { controller = "Etl", action = "EtlGetHsGuarantorEventTypes" }
                );

            routes.MapRoute("GetVpGuarantorHsMatches", "Etl/Guarantors/GetVpGuarantorHsMatches/{id}", new { controller = "Etl", action = "GetVpGuarantorHsMatches", id = UrlParameter.Optional }
                );

            routes.MapRoute("GetHsGuarantorRegistrationMatches", "Etl/Guarantors/GetHsGuarantorRegistrationMatches/{id}", new { controller = "Etl", action = "GetHsGuarantorRegistrationMatches" }
                );

            routes.MapRoute("GetHsGuarantors", "Etl/Guarantors/GetGuarantors/{parms}", new { controller = "Etl", action = "GetGuarantors", parms = UrlParameter.Optional }
                );

            routes.MapRoute("HsGetBillingSystemTypes", "Etl/Guarantors/HsGetBillingSystemTypes", new { controller = "Etl", action = "HsGetBillingSystemTypes" }
                );

            #endregion

            #region "VP Guarantors"


            routes.MapRoute("CreateGuarantor", "Guarantors/CreateGuarantor/{viewModel}", new {controller = "Guarantors", action = "CreateGuarantor", parms = UrlParameter.Optional}
                );

            routes.MapRoute("CreateVisit", "Guarantors/Visits/CreateVisit/{userName}", new {controller = "Visits", action = "CreateVisitView", parms = UrlParameter.Optional}
                );

            routes.MapRoute("CreateVisitTransaction", "Guarantors/Visits/CreateVisitTransaction/{userName}", new {controller = "Visits", action = "CreateVisitView", parms = UrlParameter.Optional}
                );

            routes.MapRoute("GetGuarantors", "Guarantors/GetGuarantors/{parms}", new { controller = "Guarantors", action = "GetGuarantors", parms = UrlParameter.Optional }
                );

            routes.MapRoute("GetVisits", "Guarantors/Visits/GetVisitsByGuarantor/{guarantorId}", new { controller = "Visits", action = "GetVisitsByGuarantor", parms = UrlParameter.Optional }
                );

            routes.MapRoute("GetVisitStates", "Guarantors/Visits/GetVisitStates", new {controller = "Visits", action = "GetVisitStates", parms = UrlParameter.Optional}
                );

            routes.MapRoute("GuarantorAuditLog", "Guarantors/GuarantorAuditLog/", new { controller = "Guarantors", action = "GuarantorAuditLog"}
                );

            #endregion

            routes.MapRoute("SendSms", "MockCommunication/2010-04-01/Accounts/{accountId}/Messages.json/", new { controller = "MockCommunication", action = "SendMessage" });
            
            // map intercept routes from UrlEnumAttribute
            Enum.GetValues(typeof(UrlEnum)).Cast<UrlEnum>().ToList().ForEach(urlEnum =>
            {
                routes.MapRoute($"Intercept{urlEnum.ToString()}", GetInterceptRouteForUrlEnum(urlEnum), new {controller = "MockEndpoint", action = "Intercept"});
            });

            // default route
            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            );
        }
        
        public static string GetCustomInterceptRouteForUrlEnum(UrlEnum urlEnum)
        {
            string customInterceptRoute = urlEnum.GetAttribute<UrlEnumAttribute>()?.CustomInterceptRoute;
            return string.IsNullOrWhiteSpace(customInterceptRoute) ? null : customInterceptRoute;
        }

        public static string GetDefaultInterceptRouteForUrlEnum(UrlEnum urlEnum)
        {
            return $"MockEndpoint/Intercept/{urlEnum.ToString()}";
        }

        public static string GetInterceptRouteForUrlEnum(UrlEnum urlEnum)
        {
            return GetCustomInterceptRouteForUrlEnum(urlEnum) ?? GetDefaultInterceptRouteForUrlEnum(urlEnum);
        }
    }
}

﻿using System;
using System.Reflection;
using System.Web;
using Elmah;

namespace Ivh.Web.QATools
{
    using Common.Configuration;

    public class GeneralErrorLogPageFactory : Elmah.ErrorLogPageFactory
    {
        public override IHttpHandler GetHandler(HttpContext context, string requestType, string url, string pathTranslated)
        {
            string appName = context.Request.Params["app"];
            if (string.IsNullOrEmpty(appName))
            {
                if (context.Request.UrlReferrer != null && !"/stylesheet".Equals(context.Request.PathInfo, StringComparison.OrdinalIgnoreCase))
                {
                    appName = HttpUtility.ParseQueryString(context.Request.UrlReferrer.Query)["app"];
                    context.Response.Redirect(string.Format("{0}{1}app={2}", context.Request.RawUrl, context.Request.Url.Query.Length > 0 ? "&" : "?", appName));
                }
            }

            IHttpHandler handler = base.GetHandler(context, requestType, url, pathTranslated);
            SqlErrorLog error = new Elmah.SqlErrorLog(ConnectionStringsProvider.Instance.GetConnectionString("LoggingConnection"))
            {
                ApplicationName = appName ?? "Unknown"
            };

            FieldInfo fieldInfo = typeof(ErrorLog).GetField("_contextKey", BindingFlags.NonPublic | BindingFlags.Static);
            if (fieldInfo == null)
                return handler;

            context.Items[fieldInfo.GetValue(null)] = error;

            return handler;
        }
    }
}
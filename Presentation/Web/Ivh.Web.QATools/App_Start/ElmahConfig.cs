﻿using System.Web;

[assembly: PreApplicationStartMethod(typeof(Ivh.Web.QATools.ElmahConfig), "Configure")]
namespace Ivh.Web.QATools
{
    using Common.Data.Services;

    public static class ElmahConfig
    {
        public static void Configure()
        {
            Common.Elmah.ElmahConfig.Configure(ConnectionStringService.Instance.LoggingConnection.Value);
        }
    }
}

﻿using System.Web.Optimization;

namespace Ivh.Web.QATools
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {

            bundles.Add(new ScriptBundle("~/bundles/scripts/jquery").Include("~/Scripts/Libraries/jquery-3.2.1.js",
                                                         "~/Scripts/Libraries/jquery.validate.js",
                                                         "~/Scripts/Libraries/jquery.validate.unobtrusive.js",
                                                         "~/Scripts/Libraries/jquery-ui-1.11.4.custom/jquery-ui.js",
                                                         "~/Scripts/Libraries/jquery.blockUI.js"
                                                         ));

            bundles.Add(new ScriptBundle("~/bundles/scripts/nav").Include(
                                                        "~/Scripts/Shared/navMenu.js",
                                                        "~/Scripts/Shared/toolTip.js"
                                                        ));

            bundles.Add(new ScriptBundle("~/bundles/scripts/common").Include(
                                                        "~/Scripts/Shared/common.js",
                                                        "~/Scripts/Shared/enums.js",
                                                        "~/Scripts/Shared/_sideNav.js"
                                                        ));

            bundles.Add(new ScriptBundle("~/bundles/views/guarantors").Include("~/Views/guarantors/guarantor.js"));
            bundles.Add(new ScriptBundle("~/bundles/views/createGuarantor").Include("~/Views/Guarantors/createGuarantor.js"));
            bundles.Add(new ScriptBundle("~/bundles/views/guarantorDetail").Include("~/Views/Guarantors/guarantorDetail.js"));
            bundles.Add(new ScriptBundle("~/bundles/views/guarantorDateAdjustments").Include("~/Views/Guarantors/guarantorDateAdjustments.js"));
            bundles.Add(new ScriptBundle("~/bundles/views/clearGuarantorData").Include("~/Views/Guarantors/clearGuarantorData.js"));
            bundles.Add(new ScriptBundle("~/bundles/views/resolveMatches").Include("~/Views/Guarantors/resolveMatches.js"));
            bundles.Add(new ScriptBundle("~/bundles/views/guarantorAuditLog").Include("~/Views/Guarantors/guarantorAuditLog.js"));

            bundles.Add(new ScriptBundle("~/bundles/views/etl").Include("~/Views/ETL/recalls.js"));

            bundles.Add(new ScriptBundle("~/bundles/views/etl/guarantors").Include("~/Views/Etl/Guarantors/guarantor.js"));
            bundles.Add(new ScriptBundle("~/bundles/views/etl/guarantorDetails")
                .Include("~/Views/Etl/Guarantors/guarantorDetails.js")
                .Include("~/Views/Etl/Guarantors/_ssoGeneration.js")
                );

            bundles.Add(new ScriptBundle("~/bundles/views/etl/hsGuarantorVisits").Include("~/Views/ETL/hsGuarantorVisits.js"));

            bundles.Add(new ScriptBundle("~/bundles/views/etl/createChangeSet").Include("~/Views/ETL/createChangeSet.js"));
            bundles.Add(new ScriptBundle("~/bundles/views/thirdparty/ach").Include("~/Views/thirdparty/ach.js"));
            bundles.Add(new ScriptBundle("~/bundles/views/thirdparty/ach").Include("~/Views/thirdparty/failpayment.js"));

            bundles.Add(new ScriptBundle("~/bundles/views/visits").Include("~/Views/Guarantors/Visits/createVisit.js"));
            bundles.Add(new ScriptBundle("~/bundles/views/visitTransactions").Include("~/Views/Guarantors/VisitTransactions/createVisitTransaction.js"));

            bundles.Add(new ScriptBundle("~/bundles/shared/modalControl").Include("~/Views/Shared/_modalControl.js"));

            bundles.Add(new StyleBundle("~/bundles/css/jqgrid").Include("~/Scripts/Libraries/jqGrid-4.8.2/css/ui.jqgrid.css",
                                                                        "~/Scripts/Libraries/jqGrid-4.8.2/css/jquery-ui.theme.css"));

            bundles.Add(new ScriptBundle("~/bundles/scripts/jqgrid").Include("~/Scripts/Libraries/jqGrid-4.8.2/js/jquery.jqGrid.src.js",
                                                                             "~/Scripts/Libraries/jqGrid-4.8.2/js/i18n/grid.locale-en.js",
                                                                             "~/Scripts/Shared/common.jqgrid.js"));


            bundles.Add(new ScriptBundle("~/bundles/scripts/knockout").Include("~/Scripts/Libraries/knockout-3.4.2.js",
                                                                   "~/Scripts/Libraries/knockout.mapping-latest.js"));

            bundles.Add(new ScriptBundle("~/bundles/scripts/bootstrap").Include("~/Scripts/Libraries/bootstrap.js",
                                                                                "~/Scripts/Libraries/respond.js"));
            bundles.Add(new ScriptBundle("~/bundles/scripts/VisitPayBase").Include("~/Scripts/Shared/IdleTimer.js"));

            bundles.Add(new ScriptBundle("~/bundles/scripts/resetdata").Include(
                "~/Scripts/ResetData/resetdata.js"
            ));

            bundles.Add(new ScriptBundle("~/bundles/views/MockCommunication").Include("~/Scripts/MockCommunication/replyToTextMessage.js"));

            bundles.Add(new StyleBundle("~/Content/css/base").Include(
                "~/Content/bootstrap.css",
                "~/Content/site.css",
                "~/Content/jquery-ui.css",
                "~/Content/nav.css",
                "~/Content/sidenav.css"
            ));

            bundles.Add(new StyleBundle("~/bundles/css/mockpaymentprocessor").Include(
                "~/Content/bootstrap.css",
                "~/Content/jquery-ui.css",
                "~/Content/Css/MockPaymentProcessor/mockpaymentprocessor.css"));

            bundles.Add(new ScriptBundle("~/bundles/scripts/mockpaymentprocessor").Include(
                "~/Scripts/Libraries/jquery-3.2.1.js",
                "~/Scripts/Libraries/jquery-ui-1.11.4.custom/jquery-ui.js",
                "~/Scripts/Libraries/knockout-3.4.2.debug.js",
                "~/Scripts/Libraries/knockout.mapping-latest.debug.js",
                "~/Scripts/Libraries/jquery.signalR-2.2.1.min.js"));

            bundles.Add(new StyleBundle("~/bundles/css/mockcommunication").Include(
                "~/Content/bootstrap.css",
                "~/Content/jquery-ui.css",
                "~/Content/Css/MockCommunication/mockCommunication.css",
                "~/Content/Css/MockCommunication/replyToTextToPay.css"
                ));

            bundles.Add(new StyleBundle("~/bundles/css/resetdata").Include(
                "~/Content/bootstrap.css",
                "~/Content/jquery-ui.css",
                "~/Content/Css/ResetData/resetdata.css"));

            bundles.Add(new ScriptBundle("~/bundles/scripts/mockcommunication").Include(
                "~/Scripts/Libraries/jquery-3.2.1.js",
                "~/Scripts/Libraries/jquery-ui-1.11.4.custom/jquery-ui.js",
                "~/Scripts/Libraries/knockout-3.4.2.debug.js",
                "~/Scripts/Libraries/knockout.mapping-latest.debug.js"));

            bundles.Add(new StyleBundle("~/bundles/css/diff").Include(
                "~/Content/diffview.css"));

            bundles.Add(new ScriptBundle("~/bundles/scripts/diff").Include(
                "~/Scripts/Libraries/difflib.js",
                "~/Scripts/Libraries/diffview.js"));

            bundles.Add(new ScriptBundle("~/bundles/scripts/vuetable").Include(
                "~/Scripts/Libraries/vue.min.js",
                "~/Scripts/Libraries/vue-tables-2.min.js",
                "~/Scripts/Shared/vuetable.common.js"
            ));

            // This is temporary so we can debug javascript without minification...
            foreach (Bundle bundle in BundleTable.Bundles)
            {
                bundle.Transforms.Clear();
            }
        }
    }
}

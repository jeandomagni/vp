﻿namespace Ivh.Web.QATools
{
    using System.Web.Mvc;
    using Common.Web.Attributes;
    using Common.Web.Filters;

    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new LoggingCorrelationAttribute());
            filters.Add(new TransportSecurityPolicyFilterAttribute());
            filters.Add(new RedirectAttribute());
            filters.Add(new HandleErrorAttribute());
            filters.Add(new IvinciHandleErrorAttribute());
        }
    }
}

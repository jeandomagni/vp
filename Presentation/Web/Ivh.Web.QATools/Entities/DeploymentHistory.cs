﻿using System;

namespace Ivh.Web.QATools.Entities
{
    public class DeploymentHistory
    {
        public virtual int DeploymentHistoryId { get; set; }
        public virtual string UserName { get; set; }
        public virtual string ClientName { get; set; }
        public virtual DateTime DeploymentDate { get; set; }
    }
}
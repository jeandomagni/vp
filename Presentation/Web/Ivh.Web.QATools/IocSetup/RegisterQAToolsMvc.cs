﻿namespace Ivh.Web.QATools.IocSetup
{
    using System.Web;
    using Autofac;
    using Autofac.Integration.Mvc;
    using Autofac.Integration.SignalR;
    using Common.Data;
    using Common.DependencyInjection;
    using Common.DependencyInjection.Registration;
    using Common.DependencyInjection.Registration.AutofacModules;
    using Common.DependencyInjection.Registration.Register;
    using Common.Session;
    using Common.Web.Interfaces;
    using Common.Web.Services;
    using Domain.Qat.Interfaces;
    using Domain.Qat.Services;
    using Interfaces;
    using Ivh.Domain.Guarantor.Interfaces;
    using Ivh.Provider.Guarantor;
    using Mappings;
    using MassTransit;
    using Microsoft.AspNet.SignalR;
    using Microsoft.Owin;
    using Owin;
    using Repositories;
    using Services;
    using GuarantorRepository = Repositories.GuarantorRepository;
    using IGuarantorRepository = Interfaces.IGuarantorRepository;

    public class RegisterQaToolsMvc : IRegistrationModule
    {
        private readonly IAppBuilder _app;
        private readonly HubConfiguration _hubConfiguration;

        public RegisterQaToolsMvc(IAppBuilder app, HubConfiguration hubConfiguration)
        {
            this._app = app;
            this._hubConfiguration = hubConfiguration;
        }

        public void Register(ContainerBuilder builder)
        {
            RegistrationSettings registrationSettings = RegistrationSettingsService.GetRegistrationSettings();
            Base.Register(builder, registrationSettings);
            VisitPay.Register(builder, registrationSettings, true);
            Data.Register(builder, registrationSettings)
                .AddVisitPayDatabase()
                .AddCdiEtlDatabase()
                .AddEnterpriseDatabase()
                .AddGuestPayDatabase()
                .AddStorageDatabase();
            Mvc.Register(builder, this._app);

            builder.RegisterType<BaseControllerService>().As<IBaseControllerService>().InstancePerRequest();
            builder.RegisterControllers(typeof (MvcApplication).Assembly);
            builder.RegisterType<SessionFacadeBase>().As<ISessionFacade>().PreserveExistingDefaults();
            builder.Register(ctx => HttpContext.Current.GetOwinContext()).As<IOwinContext>();
            builder.Register(c => HttpContext.Current.GetOwinContext().Authentication).InstancePerRequest();
            builder.Register(ctx => HttpContext.Current.GetOwinContext()).As<IOwinContext>();

            builder.RegisterType<QAVisitRepository>().As<IQAVisitRepository>();
            builder.RegisterType<GuarantorRepository>().As<IGuarantorRepository>();
            builder.RegisterType<HsGuarantorMapRepository>().As<IHsGuarantorMapRepository>();
            builder.RegisterType<QAInterestRateRepository>().As<IQAInterestRateRepository>();
            builder.RegisterType<QAFinancePlanMinPaymentTierRepository>().As<IQAFinancePlanMinPaymentTierRepository>();
            builder.RegisterType<SendSampleEmailsService>().As<ISendSampleEmailsService>();
            builder.RegisterType<SendSamplePaperMailService>().As<ISendSamplePaperMailService>();
            builder.RegisterType<IntermountainGuarantorEnrollmentTestProvider>().As<IntermountainGuarantorEnrollmentTestProvider>();
            builder.RegisterType<QaFluentMappingModule>().As<IFluentMappingModule>();
            builder.RegisterType<NachaGatewayInceptorService>().As<INachaGatewayInceptorService>();
            builder.RegisterType<DeploymentService>().As<IDeploymentService>();
            builder.RegisterType<QaVisitTransactionService>().As<IQaVisitTransactionService>();
            builder.RegisterType<QAInsurancePlanService>().As<IQAInsurancePlanService>();
            builder.RegisterType<CommunicationApplicationServiceQa>().As<ICommunicationApplicationServiceQa>();          
            builder.RegisterType<DeploymentHistoryRepository>().As<IDeploymentHistoryRepository>();
            builder.RegisterType<ScoringThirdPartyRepository>().As<IScoringThirdPartyRepository>();
            builder.RegisterModule(new AgingModule());

            // signalr
            builder.RegisterHubs(typeof(MvcApplication).Assembly).SingleInstance();
            builder.RegisterInstance(this._hubConfiguration);
           
            IBusControl serviceBus = IvinciContainer.Instance.Container().Resolve<IBusControl>();
            serviceBus?.Start();
            
            // override default trustcommerce provider
            //builder.RegisterType<StoreBadAccountsProvider>().As<IPaymentProvider>().WithParameters(new[]
            //{
            //    new ResolvedParameter((pi, ctx) => pi.ParameterType == typeof (string) && pi.Name == "trustCommerceCustomerId", (pi, ctx) => ctx.Resolve<Client>().TrustCommerceCustomerId),
            //    new ResolvedParameter((pi, ctx) => pi.ParameterType == typeof (string) && pi.Name == "trustCommercePassword", (pi, ctx) => ctx.Resolve<Client>().TrustCommercePassword)
            //});
        }
    }
}
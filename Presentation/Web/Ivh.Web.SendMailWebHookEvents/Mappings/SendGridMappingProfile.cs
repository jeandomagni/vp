﻿namespace Ivh.Web.SendMailWebHookEvents.Mappings
{
    using Application.Monitoring.Common.Dtos;
    using AutoMapper;
    using Common.Web.Models.Monitoring;

    public class SendGridMappingProfile : Profile
    {
        public SendGridMappingProfile()
        {
            this.CreateMap<SystemHealthDetailsDto, SystemHealthViewModel>();
            this.CreateMap<SystemInfoDetailsDto, SystemInfoViewModel>();
        }
    }
}
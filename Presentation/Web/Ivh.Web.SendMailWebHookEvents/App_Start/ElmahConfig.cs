﻿using System.Web;
using Ivh.Web.SendMailWebHookEvents;

[assembly: PreApplicationStartMethod(typeof (ElmahConfig), "Configure")]

namespace Ivh.Web.SendMailWebHookEvents
{
    using Common.Data.Services;

    public static class ElmahConfig
    {
        public static void Configure()
        {
            Common.Elmah.ElmahConfig.Configure(ConnectionStringService.Instance.LoggingConnection.Value);
        }
    }
}
﻿namespace Ivh.Web.SendMailWebHookEvents
{
    using System;
    using System.Web.Mvc;
    using Application.Core.Common.Interfaces;
    using Common.Web.Constants;
    using Common.Web.Cookies;
    using Microsoft.AspNet.Identity;
    using Owin;

    public partial class Startup
    {
        // For more information on configuring authentication, please visit http://go.microsoft.com/fwlink/?LinkId=301864
        public void ConfigureAuth(IAppBuilder app)
        {

            ISecurityStampValidatorApplicationService securityStampValidatorApplicationService = DependencyResolver.Current.GetService<ISecurityStampValidatorApplicationService>();
            // Enable the application to use a cookie to store information for the signed in user
            // and to use a cookie to temporarily store information about a user logging in with a third party login provider
            // Configure the sign in cookie
            app.UseCookieAuthentication(CookieAuthenticationOptionsFactory.GetCookieAuthenticationOptions(
                cookieName: CookieNames.SendMail.Authentication,
                loginPath: "/Account/Login",
                validateInterval: TimeSpan.FromMinutes(30),
                onValidateIdentity: securityStampValidatorApplicationService.OnValidateIdentity(validateInterval: TimeSpan.FromMinutes(30)),
                onApplyRedirect: ctx =>
                {
                    ctx.Response.Headers.Set(HttpHeaders.Response.WwwAuthenticate, "Basic realm=\"Realm\"");
                }));
            app.UseExternalSignInCookie(DefaultAuthenticationTypes.ExternalCookie);
        }
    }
}
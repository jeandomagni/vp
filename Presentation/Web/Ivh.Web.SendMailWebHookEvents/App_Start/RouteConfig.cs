﻿namespace Ivh.Web.SendMailWebHookEvents
{
    using System.Web.Http;
    using System.Web.Mvc;
    using System.Web.Routing;

    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute("DefaultApi", "api/SendMailEvent/{id}", new { controller = "SendMailEvent", action = "Post", id = UrlParameter.Optional });

            //routes.MapHttpRoute(
            //    name: "Default",
            //    routeTemplate: "api/{controller}/{action}/{id}",
            //    defaults: new { controller = "Home", action = "Index", id = RouteParameter.Optional }
            //);
            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                    defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
                );
            routes.MapRoute(
                name: "ApiDefault",
                url: "api/{controller}/{action}/{id}",
                    defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
                );
        }
    }
}
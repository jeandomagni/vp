﻿namespace Ivh.Web.SendMailWebHookEvents
{
    using System.Threading.Tasks;
    using System.Web.Http;
    using System.Web.Mvc;
    using Autofac;
    using Autofac.Integration.WebApi;
    using Common.DependencyInjection;
    using Common.DependencyInjection.Registration;
    using Common.Web.Extensions;
    using IocSetup;
    using Mappings;
    using MassTransit;
    using Owin;

    public partial class Startup
    {
        public void ConfigureIoc(IAppBuilder app)
        {
            Task mappingTasks = Task.Run(() =>
            {
                ServiceBusMessageSerializationMappingBase.ConfigureMappings();

                using (MappingBuilder mappingBuilder = new MappingBuilder())
                {
                    mappingBuilder
                        .WithBase()
                        .WithProfile<SendGridMappingProfile>();
                }
            });

            IvinciContainer.Instance.RegisterComponent(new RegisterSendGridEmailEvent(app));

            GlobalConfiguration.Configuration.DependencyResolver = new AutofacWebApiDependencyResolver(IvinciContainer.Instance.Container());
            DependencyResolver.SetResolver(IvinciContainer.Instance.GetResolver());

            app.UseAutofacMiddleware(IvinciContainer.Instance.Container());
            //app.UseAutofacWebApi(GlobalConfiguration.Configuration);
            //app.UseWebApi(GlobalConfiguration.Configuration);
            app.UseAutofacMvc();

            IBusControl serviceBus = IvinciContainer.Instance.Container().Resolve<IBusControl>();
            serviceBus?.Start();

            mappingTasks.Wait();
        }
    }
}
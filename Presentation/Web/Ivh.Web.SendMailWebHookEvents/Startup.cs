﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Ivh.Web.SendMailWebHookEvents.Startup))]
namespace Ivh.Web.SendMailWebHookEvents
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            this.ConfigureIoc(app);
            this.ConfigureAuth(app);
            this.ConfigureMisc(app);
        }
    }
}
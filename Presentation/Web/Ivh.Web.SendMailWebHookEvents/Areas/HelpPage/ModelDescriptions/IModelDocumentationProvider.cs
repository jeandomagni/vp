using System;
using System.Reflection;

namespace Ivh.Web.SendMailWebHookEvents.Areas.HelpPage.ModelDescriptions
{
    public interface IModelDocumentationProvider
    {
        string GetDocumentation(MemberInfo member);

        string GetDocumentation(Type type);
    }
}
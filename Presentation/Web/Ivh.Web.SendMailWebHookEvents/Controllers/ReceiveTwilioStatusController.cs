﻿﻿namespace Ivh.Web.SendMailWebHookEvents.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Text;
    using System.Web;
    using System.Web.Mvc;
    using Application.Email.Common.Constants;
    using Application.Logging.Common.Interfaces;
    using Common.Base.Utilities.Helpers;
    using Common.ServiceBus.Interfaces;
    using Common.VisitPay.Messages.Communication;
    using Common.Web.Constants;
    using Domain.Settings.Interfaces;
    using Ivh.Common.Base.Utilities.Extensions;

    public class ReceiveTwilioStatusController : Controller
    {
        private readonly IBus _bus;
        private readonly Lazy<ILoggingApplicationService> _logger;
        private readonly Lazy<IApplicationSettingsService> _applicationSettingsService;
        private readonly Dictionary<string, string> _errorCodes = new Dictionary<string, string>
        {
              { "30001", "Queue overflow" }
            , { "30002", "Account suspended"}
            , { "30003", "Ureachable destinatio handset"}
            , { "30004", "Message blocked"}
            , { "30005", "Unknown destination handset"}
            , { "30006", "Landline or unreachable carrier"}
            , { "30007", "Carrier violation"}
            , { "30008", "Unknown error"}
            , { "30009", "Missing segment"}
            , { "30010", "Message price exceeds max price"}
        };

        public ReceiveTwilioStatusController(
            Lazy<ILoggingApplicationService> logger,
            Lazy<IApplicationSettingsService> applicationSettingsSerivce,
            IBus bus)
        {
            this._logger = logger;
            this._applicationSettingsService = applicationSettingsSerivce;
            this._bus = bus;
        }

        [HttpPost]
        [AllowAnonymous]
        public ActionResult Process()
        {
            if (this.Request?.InputStream != null && this.Authorize())
            {
                string queryString = null;
                try
                {
                    Stream req = this.Request.InputStream;
                    req.Seek(0, SeekOrigin.Begin);
                    using (StreamReader reader = new StreamReader(req))
                    {
                        queryString = reader.ReadToEnd();
                    }
                    this._logger.Value.Info(() => $"ReceiveTwilioStatusController::Process got a status back = {queryString}");
                    string urlDecode = HttpUtility.UrlDecode(queryString);
                    if (urlDecode != null)
                    {
                        Dictionary<string, string> response = urlDecode.Split(new[] {'&'}, StringSplitOptions.RemoveEmptyEntries).Select(part => part.Split('=')).ToDictionary(split => split[0], split => split[1]);
                        if (!response.ContainsKey("MessageSid") || !response.ContainsKey("MessageStatus"))
                        {
                            this._logger.Value.Fatal(() => $"ReceiveTwilioStatusController::Post Message = {queryString} Unable to cross reference message" + $"WebRequest = {this.Request.AggregateWebRequest(System.Reflection.MethodBase.GetCurrentMethod().Name)}");
                        }
                        string uniqueIdString = response["SmsSid"].Substring(2);// to remove the leading "SM"
                        Guid.TryParse(uniqueIdString, out Guid uniqueId);
                        TwilioSmsEventMessage message = new TwilioSmsEventMessage
                        {
                            Attempt = "N/A",
                            Category = "SMS",
                            EmailAddress = response["To"],
                            EventId = response["SmsSid"],
                            EventName = response["MessageStatus"],
                            Response = response["MessageStatus"],
                            EventType = "SMS",
                            GroupId = response["MessagingServiceSid"],
                            MessageId = response["MessageSid"],
                            SmtpId = response["AccountSid"],
                            SourceApp = "Visit Pay",
                            UniqueId = uniqueId,
                            RawData = queryString,
                            Timestamp = new DateTimeOffset(DateTime.UtcNow, TimeSpan.Zero).ToUnixTimeSeconds().ToString(),
                        };
                        if ((response["MessageStatus"] == CommunicationEventType.Failed || response["MessageStatus"] == CommunicationEventType.Undelivered) && response.ContainsKey("ErrorCode"))
                        {
                            message.Response = $"Error Code: {response["ErrorCode"]}, Error Message: {this._errorCodes[response["ErrorCode"]]}";
                        }
                        this._bus.PublishMessage(message).Wait();
                    }
                    else
                    {
                        this._logger.Value.Fatal(() => $"ReceiveTwilioStatusController::Post Message = {queryString} Unable to parse message" + $"WebRequest = {this.Request.AggregateWebRequest(System.Reflection.MethodBase.GetCurrentMethod().Name)}");
                    }
                }
                catch (Exception ex)
                {
                    this._logger.Value.Fatal(() => string.Format("ReceiveTwilioStatusController::Post Message = {1} Exception = {0}\nWebRequest = {2}",
                        ExceptionHelper.AggregateExceptionToString(ex),
                        queryString ?? "ValueIsNull",
                        this.Request.AggregateWebRequest(System.Reflection.MethodBase.GetCurrentMethod().Name)
                    ));
                }
            }
            else
            {
                this._logger.Value.Info(() => $"this.Request != null: {this.Request != null}, this.Request.InputStream != null: {this.Request != null && this.Request.InputStream != null}, Authorize: {this.Authorize()}");
                return new HttpUnauthorizedResult();
            }
            return new EmptyResult();
        }


        private bool Authorize()
        {
            try
            {
                string authHeader = this.Url.RequestContext.HttpContext.Request.Headers[HttpHeaders.Request.Authorization];
                if (authHeader != null)
                {
                    string[] authHeaderValues = authHeader.Split(' ');
                    if (authHeaderValues.Length > 0)
                    {
                        string base64HeaderText = authHeaderValues[1];
                        byte[] authHeaderData = Convert.FromBase64String(base64HeaderText);
                        string authHeaderText = Encoding.UTF8.GetString(authHeaderData);
                        string authApplicationText = $"{this._applicationSettingsService.Value.TwilioCallbackUsername.Value}:{this._applicationSettingsService.Value.TwilioCallbackPassword.Value}";
                        return string.CompareOrdinal(authHeaderText, authApplicationText) == 0;
                    }
                    else
                    {
                        //Authorization Header did not Split(' ')
                        return false;
                    }
                }
                else
                {
                    //Authorization Header is missing
                    return false;
                }
            }
            catch (Exception ex)
            {
                string headerText = HttpUtility.UrlDecode(this.Url.RequestContext.HttpContext.Request.Headers.ToString());
                this._logger.Value.Fatal(() => $"ReceiveTwilioStatusController::Authorize Header = {headerText} Exception = {ExceptionHelper.AggregateExceptionToString(ex)}");
            }
            return false;
        }
    }
}
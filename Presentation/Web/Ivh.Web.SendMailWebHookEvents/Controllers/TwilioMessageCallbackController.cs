﻿﻿namespace Ivh.Web.SendMailWebHookEvents.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Text;
    using System.Web;
    using System.Web.Mvc;
    using Application.Email.Common.Dtos;
    using Application.Email.Common.Interfaces;
    using Application.Logging.Common.Interfaces;
    using Common.Base.Utilities.Extensions;
    using Common.Base.Utilities.Helpers;
    using Common.VisitPay.Messages.Communication;
    using Common.Web.Constants;
    using Domain.Settings.Interfaces;
    using Twilio.AspNet.Mvc;
    using Twilio.TwiML;
    using Twilio.TwiML.Messaging;

    /// <summary>
    /// This is the controller that recieves the messages back from SMS, EG.  Someone sends "stop" or something.
    /// </summary>
    public class TwilioMessageCallbackController : TwilioController
    {
        private readonly Lazy<ILoggingApplicationService> _logger;
        private readonly Lazy<IApplicationSettingsService> _applicationSettingsSerivce;
        private readonly Lazy<ITwilioSmsEventApplicationService> _smsEventApplicationService;

        public TwilioMessageCallbackController(
            Lazy<ILoggingApplicationService> logger,
            Lazy<IApplicationSettingsService> applicationSettingsSerivce,
            Lazy<ITwilioSmsEventApplicationService> smsEventApplicationService)
        {
            this._logger = logger;
            this._applicationSettingsSerivce = applicationSettingsSerivce;
            this._smsEventApplicationService = smsEventApplicationService;
        }

        [HttpPost]
        [AllowAnonymous]
        public ActionResult Index()
        {
            MessagingResponse returnMessage = new MessagingResponse();
            if (this.Request?.InputStream != null && this.Authorize())
            {
                
                string queryString = null;
                try
                {
                    Stream req = this.Request.InputStream;
                    req.Seek(0, SeekOrigin.Begin);
                    using (StreamReader reader = new StreamReader(req))
                    {
                        queryString = reader.ReadToEnd();
                    }
                    this._logger.Value.Info(() => $"TwilioMessageCallbackController::Index QueryString = {queryString}");
                    string urlDecode = HttpUtility.UrlDecode(queryString);
                    if (!string.IsNullOrWhiteSpace(urlDecode))
                    {
                        Dictionary<string, string> response = urlDecode.Split(new[] {'&'}, StringSplitOptions.RemoveEmptyEntries).Select(part => part.Split('=')).ToDictionary(split => split[0], split => split[1]);
                        
                        TwilioCallbackMessage message = new TwilioCallbackMessage()
                        {
                            SmsMessageSid = response["SmsMessageSid"],
                            MessageSid = response["MessageSid"],
                            SmsSid = response["SmsSid"],
                            AccountSid = response["AccountSid"],
                            MessagingServiceSid = response["MessagingServiceSid"],
                            From = response["From"],
                            To = response["To"],
                            Body = response["Body"],
                            NumMedia = response["NumMedia"],
                            RawData = queryString
                        };
                        SmsResponseResult result = this._smsEventApplicationService.Value.SmsMessage(message);
                        Message twilioMessage = new Message();
                        if (!string.IsNullOrEmpty(result.Body)) {
                            twilioMessage.Body(result.Body);
                        }
                        returnMessage.Append(twilioMessage);

                        this._logger.Value.Info(() => $"TwilioMessageCallbackController::Index resultFromApp Svc ResponseAction:{result.ResponseActionEnum} body:{result.Body} ");
                    }
                    else
                    {
                        this._logger.Value.Fatal(() => $"TwilioMessageCallbackController::Index Message = {queryString} Unable to parse message" + $"WebRequest = {this.Request.AggregateWebRequest(System.Reflection.MethodBase.GetCurrentMethod().Name)}");
                    }
                }
                catch (Exception ex)
                {
                    this._logger.Value.Fatal(() => string.Format("TwilioMessageCallbackController::Index Message = {1} Exception = {0}\nWebRequest = {2}",
                        ExceptionHelper.AggregateExceptionToString(ex),
                        queryString ?? "ValueIsNull",
                        this.Request.AggregateWebRequest(System.Reflection.MethodBase.GetCurrentMethod().Name)
                    ));
                }
            }
            else
            {
                this._logger.Value.Info(() => $"this.Request != null: {this.Request != null}, this.Request.InputStream != null: {this.Request != null && this.Request.InputStream != null}, Authorize: {this.Authorize()}");
                return new HttpUnauthorizedResult();
            }
            return this.TwiML(returnMessage);
        }


        private bool Authorize()
        {
            try
            {
                byte[] data = Convert.FromBase64String(this.Url.RequestContext.HttpContext.Request.Headers[HttpHeaders.Request.Authorization].Split(' ')[1]);
                string auth = Encoding.UTF8.GetString(data);
                return string.CompareOrdinal(auth, $"{this._applicationSettingsSerivce.Value.TwilioCallbackUsername.Value}:{this._applicationSettingsSerivce.Value.TwilioCallbackPassword.Value}") == 0;
            }
            catch (Exception ex)
            {
                this._logger.Value.Fatal(() => $"TwilioMessageCallbackController::Authorize Exception = {ExceptionHelper.AggregateExceptionToString(ex)}");
                return false;
            }
        }
    }
}
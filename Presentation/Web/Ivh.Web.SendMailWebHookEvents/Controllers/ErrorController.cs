﻿namespace Ivh.Web.SendMailWebHookEvents.Controllers
{
    using System.Web.Mvc;

    public class ErrorController : Controller
    {
        public virtual ActionResult Index()
        {
            this.Response.StatusCode = 403;
            return this.View();
        }

        public virtual ActionResult NotFound()
        {
            this.Response.StatusCode = 404;
            return this.View();
        }
    }
}
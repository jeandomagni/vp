﻿﻿namespace Ivh.Web.SendMailWebHookEvents.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Text;
    using System.Web.Mvc;
    using Application.Core.Common.Dtos;
    using Application.Core.Common.Interfaces;
    using Application.Logging.Common.Interfaces;
    using Common.Base.Utilities.Extensions;
    using Common.Base.Utilities.Helpers;
    using Common.ServiceBus.Common;
    using Common.ServiceBus.Interfaces;
    using Common.VisitPay.Messages.Communication;
    using Common.Web.Constants;
    using Newtonsoft.Json;
    
    public class SendMailEventController : Controller
    {
        private readonly IBus _bus;
        private readonly Lazy<IClientApplicationService> _clientApplicationService;
        private readonly Lazy<ILoggingApplicationService> _logger;

        public SendMailEventController(
            Lazy<IClientApplicationService> clientApplicationService,
            IBus bus, Lazy<ILoggingApplicationService> logger)
        {
            this._clientApplicationService = clientApplicationService;
            this._bus = bus;
            this._logger = logger;
        }

        // POST: SendMailEvent
        [HttpPost]
        [AllowAnonymous]
        public void Post()
        {
            if (this.Request?.InputStream != null && this.Authorize())
            {
                string json = null;
                List<SendGridEmailEventMessage> sendGridEmailEventMessages = null;
                SendGridEmailEventMessage message = null;

                Exception sendGridEmailEventMessagesException = null;
                Exception messageException = null;

                try
                {
                    Stream req = this.Request.InputStream;
                    req.Seek(0, SeekOrigin.Begin);
                    using (StreamReader reader = new StreamReader(req))
                    {
                        json = reader.ReadToEnd();
                    }

                    try
                    {
                        sendGridEmailEventMessages = JsonConvert.DeserializeObject<List<SendGridEmailEventMessage>>(json);
                    }
                    catch(Exception e)
                    {
                        sendGridEmailEventMessagesException = e;
                    }
                    if (sendGridEmailEventMessages == null)
                    {
                        //Assuming more of the messages are going to be arrays.
                        try
                        {
                            message = JsonConvert.DeserializeObject<SendGridEmailEventMessage>(json);
                        }
                        catch(Exception e)
                        {
                            messageException = e;                       
                        }
                    }

                    if (message != null)
                    {
                        message.RawData = json;
                        this._bus.PublishMessage(message).Wait();
                    }
                    else if (sendGridEmailEventMessages != null)
                    {
                        foreach (SendGridEmailEventMessage current in sendGridEmailEventMessages)
                        {
                            current.RawData = json;
                            this._bus.PublishMessage(current).Wait();
                        }
                    }
                    else
                    {
                        string jsonText = json?.ToString() ?? "ValueIsNull";
                        string webRequest = this.Request.AggregateWebRequest(System.Reflection.MethodBase.GetCurrentMethod().Name);
                        string messageExceptionText = messageException == null ? "" : ExceptionHelper.AggregateExceptionToString(messageException);
                        string sendGridEmailEventMessagesExceptionText = sendGridEmailEventMessagesException == null ? "" : ExceptionHelper.AggregateExceptionToString(sendGridEmailEventMessagesException);
                        this._logger.Value.Warn(() =>  $"SendMailEventController::Post JSON Deserialization Message = {jsonText}\nMessageException = {messageExceptionText}\nMessageListException = {sendGridEmailEventMessagesExceptionText}\nWebRequest = {webRequest}"); 
                    }
                }
                catch (Exception ex)
                {
                    this._logger.Value.Fatal(() => string.Format("SendMailEventController::Post Message = {1} Exception = {0}\n" +
                                                           "WebRequest = {2}",
                                                           ExceptionHelper.AggregateExceptionToString(ex),
                                                           json?.ToString() ?? "ValueIsNull",
                                                            this.Request.AggregateWebRequest(System.Reflection.MethodBase.GetCurrentMethod().Name)
                                                           ));
                }
            }
            else
            {
                this._logger.Value.Info(() => $"this.Request != null: {this.Request != null}, this.Request.InputStream != null: {this.Request != null && this.Request.InputStream != null}, Authorize: {this.Authorize()}");
            }
        }

        private bool Authorize()
        {
            try
            {
                ClientDto clientDto = this._clientApplicationService.Value.GetClient();
                byte[] data = Convert.FromBase64String(this.Url.RequestContext.HttpContext.Request.Headers[HttpHeaders.Request.Authorization].Split(' ')[1]);
                string auth = Encoding.UTF8.GetString(data);
                return string.CompareOrdinal(auth, $"{clientDto.SendGridUserName}:{clientDto.SendGridPassword}") == 0;
            }
            catch (Exception ex)
            {
                this._logger.Value.Fatal(() => $"SendMailEventController::Authorize Exception = {ExceptionHelper.AggregateExceptionToString(ex)}");
                return false;
            }
        }
    }
}
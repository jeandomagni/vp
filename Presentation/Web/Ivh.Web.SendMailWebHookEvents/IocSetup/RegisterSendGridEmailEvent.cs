﻿namespace Ivh.Web.SendMailWebHookEvents.IocSetup
{
    using System.Web;
    using Autofac;
    using Autofac.Integration.Mvc;
    using Common.DependencyInjection;
    using Common.DependencyInjection.Registration;
    using Common.DependencyInjection.Registration.Register;
    using Owin;

    public class RegisterSendGridEmailEvent : IRegistrationModule
    {
        private readonly IAppBuilder _app;

        public RegisterSendGridEmailEvent(IAppBuilder app)
        {
            this._app = app;
        }

        public void Register(ContainerBuilder builder)
        {
            RegistrationSettings registrationSettings = RegistrationSettingsService.GetRegistrationSettings();
            Base.Register(builder, registrationSettings);
            VisitPay.Register(builder, registrationSettings, true);
            Data.Register(builder, registrationSettings)
                .AddVisitPayDatabase();
            Mvc.Register(builder, this._app);

            builder.RegisterControllers(typeof(WebApiApplication).Assembly);
            builder.Register(c => HttpContext.Current.GetOwinContext().Authentication).InstancePerRequest();
        }
    }
}
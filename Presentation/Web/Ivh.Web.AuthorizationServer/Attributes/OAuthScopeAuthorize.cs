﻿namespace Ivh.Web.AuthorizationServer.Attributes
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Security.Claims;
    using System.Web;
    using System.Web.Mvc;
    using Microsoft.Owin.Security;

    public sealed class OAuthScopeAuthorize : ActionFilterAttribute
    {
        public OAuthScopeAuthorize(params string[] scope)
        {
            this.Scopes = scope;
        }

        public IList<string> Scopes { get; set; }

        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            base.OnActionExecuting(filterContext);

            try
            {
                IAuthenticationManager authentication = filterContext.HttpContext.GetOwinContext().Authentication;

                if (!(filterContext.HttpContext.User?.Identity is ClaimsIdentity) ||

                    //if there are any missing claims, reject
                    this.Scopes.Any(x => !(authentication.User.HasClaim("urn:oauth:scope", x))))
                {
                    filterContext.Result = filterContext.Result = new HttpUnauthorizedResult();
                }
            }
            catch (Exception)
            {
                filterContext.Result = filterContext.Result = new HttpUnauthorizedResult();
            }

        }
    }
}
﻿namespace Ivh.Web.AuthorizationServer
{
    using System.Threading.Tasks;
    using System.Web.Mvc;
    using Common.DependencyInjection;
    using Common.DependencyInjection.Registration;
    using Common.Web.Extensions;
    using IocSetup;
    using Owin;
    
    public partial class Startup
    {
        public void ConfigureIoc(IAppBuilder app)
        {
            Task mappingTasks = Task.Run(() =>
            {
                ServiceBusMessageSerializationMappingBase.ConfigureMappings();

                using (MappingBuilder mappingBuilder = new MappingBuilder())
                {
                    mappingBuilder.WithBase();
                }
            });

            IvinciContainer.Instance.RegisterComponent(new RegisterAuthorizationVisitPayMvc(app));

            IDependencyResolver dependencyResolver = IvinciContainer.Instance.GetResolver();
            DependencyResolver.SetResolver(dependencyResolver);

            app.UseAutofacMiddleware(IvinciContainer.Instance.Container());
            app.UseAutofacMvc();
            
            mappingTasks.Wait();
        }
    }
}
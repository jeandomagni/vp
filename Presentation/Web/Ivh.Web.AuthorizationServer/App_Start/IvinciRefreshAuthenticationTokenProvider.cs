namespace Ivh.Web.AuthorizationServer
{
    using System;
    using Application.Core.Common.Dtos;
    using Application.Core.Common.Interfaces;
    using Application.User.Common.Dtos;
    using Autofac;
    using Autofac.Core.Lifetime;
    using Common.Base.Constants;
    using Common.DependencyInjection;
    using Microsoft.Owin.Security.Infrastructure;

    public class IvinciRefreshAuthenticationTokenProvider : AuthenticationTokenProvider
    {
        public override void Create(AuthenticationTokenCreateContext context)
        {
            ILifetimeScope requestScope = null;
            try
            {
                requestScope = IvinciContainer.Instance.Container().BeginLifetimeScope(MatchingScopeLifetimeTags.RequestLifetimeScopeTag);
                //Get the client 
                //Validate the url
                var ssoApplicationService = requestScope.Resolve<ISsoOauthIssuerApplicationService>();
                var userAppSvc = requestScope.Resolve<IVisitPayUserApplicationService>();

                if (context.Ticket.Identity.IsAuthenticated)
                {
                    context.Ticket.Properties.ExpiresUtc = DateTime.UtcNow.AddDays(100);
                    var username = context.Ticket.Identity.Name;
                    VisitPayUserDto vpUser =  userAppSvc.FindByName(username);
                    var refreshToken = ssoApplicationService.CreateRefreshToken(vpUser, context.SerializeTicket());
                    context.SetToken(refreshToken.Token);

                }
                else
                {
                    context.Response.StatusCode = 401;
                    context.Response.ContentType = MimeTypes.Application.Json;
                    context.Response.ReasonPhrase = "Not Authenticated";
                    return;
                }
            }
            finally
            {
                if (requestScope != null)
                {
                    requestScope.Dispose();
                    requestScope = null;
                }
            }

            base.Create(context);
        }

        public override void Receive(AuthenticationTokenReceiveContext context)
        {
            ILifetimeScope requestScope = null;
            try
            {
                requestScope = IvinciContainer.Instance.Container().BeginLifetimeScope(MatchingScopeLifetimeTags.RequestLifetimeScopeTag);
                //Get the client 
                //Validate the url


                var ssoApplicationService = requestScope.Resolve<ISsoOauthIssuerApplicationService>();
                var refreshToken = ssoApplicationService.GetRefreshTokenWithRefreshTokenString(context.Token);
                if (refreshToken.IsActive)
                {
                    context.DeserializeTicket(refreshToken.Ticket);

                }
                else
                {
                    context.Response.StatusCode = 401;
                    context.Response.ContentType = MimeTypes.Application.Json;
                    context.Response.ReasonPhrase = "Not Authenticated";
                    return;
                }
            }
            finally
            {
                if (requestScope != null)
                {
                    requestScope.Dispose();
                    requestScope = null;
                }
            }


            base.Receive(context);
        }
    }
}
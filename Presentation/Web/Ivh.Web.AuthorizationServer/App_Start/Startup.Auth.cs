﻿namespace Ivh.Web.AuthorizationServer
{
    using System;
    using Microsoft.Owin;
    using Microsoft.Owin.Security.Cookies;
    using Owin;
    using System.Web.Mvc;
    using Application.Core.Common.Dtos;
    using Application.Core.Common.Interfaces;
    using Common.Base.Enums;
    using Common.VisitPay.Enums;
    using Common.Web.Cookies;
    using Common.Web.Encryption;
    using Common.Web.Extensions;
    using Domain.Settings.Interfaces;
    using Microsoft.AspNet.Identity;
    using Microsoft.Owin.Security;
    using Microsoft.Owin.Security.DataHandler;
    using Microsoft.Owin.Security.OAuth;

    public partial class Startup
    {
        public static OAuthBearerAuthenticationOptions OAuthBearerAuthenticationOptions;


        public void ConfigureAuth(IAppBuilder app)
        {
            ISecurityStampValidatorApplicationService securityStampValidatorApplicationService = DependencyResolver.Current.GetService<ISecurityStampValidatorApplicationService>();
            IClientApplicationService clientApplicationService = DependencyResolver.Current.GetService<IClientApplicationService>();
            IApplicationSettingsService settingsService = DependencyResolver.Current.GetService<IApplicationSettingsService>();
            ISsoApplicationService ssoApplicationService = DependencyResolver.Current.GetService<ISsoApplicationService>();
            int timeoutMinutes = clientApplicationService.GetClient().ClientSessionTimeOutInMinutes;
            SsoProviderDto grantProvider = ssoApplicationService.GetProvider(SsoProviderEnum.HealthEquityOAuthImplicit);

            // Enable Application Sign In Cookie
            CookieAuthenticationOptions cookieAuthenticationOptions = CookieAuthenticationOptionsFactory.GetCookieAuthenticationOptions(
                cookieName: CookieNames.AuthorizationServer.Authentication,
                loginPath: grantProvider.LoginPath,
                validateInterval: TimeSpan.FromMinutes(timeoutMinutes),
                onValidateIdentity: securityStampValidatorApplicationService.OnValidateIdentity(validateInterval: TimeSpan.FromMinutes(timeoutMinutes)),
                onApplyRedirect: ctx =>
                {
                    if (ctx.Request.IsAjaxRequest())
                    {
                        return;
                    }

                    ctx.Response.Redirect(ctx.RedirectUri); // web login (default)
                });
            cookieAuthenticationOptions.AuthenticationType = "Application";
            //cookieAuthenticationOptions.AuthenticationMode = AuthenticationMode.Passive;
            cookieAuthenticationOptions.LogoutPath = new PathString(grantProvider.LogoutPath);
            cookieAuthenticationOptions.ExpireTimeSpan = TimeSpan.FromMinutes(timeoutMinutes);

            app.UseCookieAuthentication(cookieAuthenticationOptions);
            app.UseExternalSignInCookie(DefaultAuthenticationTypes.ExternalCookie);

            OAuthBearerAuthenticationOptions = new Microsoft.Owin.Security.OAuth.OAuthBearerAuthenticationOptions()
            {
                AccessTokenFormat = new TicketDataFormat(new AESThenHmacTicketProtector(settingsService.HealthEquityOAuthCryptKey.Value, settingsService.HealthEquityOAuthAuthKey.Value)),
            };
            app.UseOAuthBearerAuthentication(OAuthBearerAuthenticationOptions);

            // Setup Authorization Server
            app.UseOAuthAuthorizationServer(new OAuthAuthorizationServerOptions
            {
                AuthorizeEndpointPath = new PathString(grantProvider.AuthorizePath),
                TokenEndpointPath = new PathString(grantProvider.TokenPath),
                AccessTokenExpireTimeSpan = TimeSpan.FromMinutes(5),
#if DEBUG
                ApplicationCanDisplayErrors = true,
                AllowInsecureHttp = true,
#endif
                // Authorization server provider which controls the lifecycle of Authorization Server
                Provider = new IvinciOAuthAuthorizationServerProvider(),

                // Authorization code provider which creates and receives authorization code
                AuthorizationCodeProvider = new IvinciAuthenticationTokenProvider(),

                // Refresh token provider which creates and receives referesh token
                RefreshTokenProvider = new IvinciRefreshAuthenticationTokenProvider(),

                //This is the default implementation here.
                //Seems like this requires that the machine key is the same in the webconfigs for all services consuming this, which I wasnt thrilled about
                //AccessTokenFormat = new TicketDataFormat(app.CreateDataProtector(
                //    typeof(OAuthAuthorizationServerMiddleware).Namespace,
                //    "Access_Token", "v1")),
                //RefreshTokenFormat = new TicketDataFormat(app.CreateDataProtector(
                //    typeof(OAuthAuthorizationServerMiddleware).Namespace,
                //    "Refresh_Token", "v1")),
                AccessTokenFormat = new TicketDataFormat(new AESThenHmacTicketProtector(settingsService.HealthEquityOAuthCryptKey.Value, settingsService.HealthEquityOAuthAuthKey.Value)),
                RefreshTokenFormat = new TicketDataFormat(new AESThenHmacTicketProtector(settingsService.HealthEquityOAuthCryptKey.Value, settingsService.HealthEquityOAuthAuthKey.Value)),
            });
        }
    }

}
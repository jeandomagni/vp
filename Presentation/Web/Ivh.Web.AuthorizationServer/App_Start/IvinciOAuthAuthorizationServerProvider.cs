﻿namespace Ivh.Web.AuthorizationServer
{
    using System;
    using System.Linq;
    using System.Security.Claims;
    using System.Security.Principal;
    using System.Threading.Tasks;
    using Application.Core.Common.Dtos;
    using Application.Core.Common.Interfaces;
    using Autofac;
    using Autofac.Core.Lifetime;
    using Common.Base.Utilities.Extensions;
    using Common.DependencyInjection;
    using Microsoft.Owin.Security.OAuth;

    public class IvinciOAuthAuthorizationServerProvider : OAuthAuthorizationServerProvider
    {
        public override Task ValidateClientRedirectUri(OAuthValidateClientRedirectUriContext context)
        {
            if (context.ClientId != null && context.ClientId.IsNotNullOrEmpty())
            {
                ILifetimeScope requestScope = null;
                try
                {
                    requestScope = IvinciContainer.Instance.Container().BeginLifetimeScope(MatchingScopeLifetimeTags.RequestLifetimeScopeTag);
                    //Get the client 
                    //Validate the url
                    var ssoApplicationService = requestScope.Resolve<ISsoApplicationService>();
                    var provider = ssoApplicationService.GetProviderBySourceSystemKey(context.ClientId);

                    if (provider != null && provider.IsEnabled && string.Equals(context.RedirectUri, provider.Callback, StringComparison.InvariantCultureIgnoreCase))
                    {
                        context.Validated();
                    }
                }
                finally
                {
                    if (requestScope != null)
                    {
                        requestScope.Dispose();
                        requestScope = null;
                    }
                }
            }
            //If it wasnt validated above, reject
            if (!context.IsValidated)
            {
                context.Rejected();
            }
            return base.ValidateClientRedirectUri(context);
        }

        public override Task ValidateClientAuthentication(OAuthValidateClientAuthenticationContext context)
        {
            ILifetimeScope requestScope = null;
            try
            {
                string clientId;
                string clientSecret;
                if (context.TryGetBasicCredentials(out clientId, out clientSecret) ||
                    context.TryGetFormCredentials(out clientId, out clientSecret))
                {
                    requestScope = IvinciContainer.Instance.Container().BeginLifetimeScope(MatchingScopeLifetimeTags.RequestLifetimeScopeTag);

                    ISsoApplicationService ssoApplicationService = requestScope.Resolve<ISsoApplicationService>();
                    SsoProviderDto provider = ssoApplicationService.GetProviderBySourceSystemKey(context.ClientId);
                    
                    if (provider != null && provider.IsEnabled && clientId == provider.SsoProviderSourceSystemKey && clientSecret == provider.SsoProviderSecret)
                    {
                        context.Validated();
                    }
                }
            }
            finally
            {
                if (requestScope != null)
                {
                    requestScope.Dispose();
                    requestScope = null;
                }
            }
            //If it wasnt validated above, reject
            if (!context.IsValidated)
            {
                context.Rejected();
            }
            return base.ValidateClientAuthentication(context);
        }

        public override Task GrantResourceOwnerCredentials(OAuthGrantResourceOwnerCredentialsContext context)
        {
            ILifetimeScope requestScope = null;
            try
            {
                requestScope = IvinciContainer.Instance.Container().BeginLifetimeScope(MatchingScopeLifetimeTags.RequestLifetimeScopeTag);

                var identity = new ClaimsIdentity(new GenericIdentity(context.UserName, OAuthDefaults.AuthenticationType), context.Scope.Select(x => new Claim("urn:oauth:scope", x)));

                context.Validated(identity);
                
            }
            finally
            {
                if (requestScope != null)
                {
                    requestScope.Dispose();
                    requestScope = null;
                }
            }
            return base.GrantResourceOwnerCredentials(context);
        }

        public override Task GrantClientCredentials(OAuthGrantClientCredentialsContext context)
        {
            ILifetimeScope requestScope = null;
            try
            {
                requestScope = IvinciContainer.Instance.Container().BeginLifetimeScope(MatchingScopeLifetimeTags.RequestLifetimeScopeTag);

                var identity = new ClaimsIdentity(new GenericIdentity(context.ClientId, OAuthDefaults.AuthenticationType), context.Scope.Select(x => new Claim("urn:oauth:scope", x)));

                context.Validated(identity);
                
            }
            finally
            {
                if (requestScope != null)
                {
                    requestScope.Dispose();
                    requestScope = null;
                }
            }
            return base.GrantClientCredentials(context);
        }

        public override Task ValidateTokenRequest(OAuthValidateTokenRequestContext context)
        {
            return base.ValidateTokenRequest(context);
        }

        public override Task GrantRefreshToken(OAuthGrantRefreshTokenContext context)
        {
            return base.GrantRefreshToken(context);
        }
    }
}
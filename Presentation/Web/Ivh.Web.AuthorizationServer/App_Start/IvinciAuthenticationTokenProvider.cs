namespace Ivh.Web.AuthorizationServer
{
    using System;
    using Application.Core.Common.Dtos;
    using Application.Core.Common.Interfaces;
    using Application.User.Common.Dtos;
    using Autofac;
    using Autofac.Core.Lifetime;
    using Common.DependencyInjection;
    using Microsoft.Owin.Security.Infrastructure;

    public class IvinciAuthenticationTokenProvider : AuthenticationTokenProvider
    {
        public override void Create(AuthenticationTokenCreateContext context)
        {
            ILifetimeScope requestScope = null;
            try
            {
                requestScope = IvinciContainer.Instance.Container().BeginLifetimeScope(MatchingScopeLifetimeTags.RequestLifetimeScopeTag);
                //Get the client 
                //Validate the url
                var ssoApplicationService = requestScope.Resolve<ISsoOauthIssuerApplicationService>();
                var userAppSvc = requestScope.Resolve<IVisitPayUserApplicationService>();
                
                if (context.Ticket.Identity.IsAuthenticated && !string.IsNullOrWhiteSpace(context.Ticket.Identity.Name))
                {
                    var username = context.Ticket.Identity.Name;
                    VisitPayUserDto vpUser = userAppSvc.FindByName(username);
                    var accessToken = ssoApplicationService.CreateAccessToken(vpUser, context.SerializeTicket());
                    context.SetToken(accessToken.Token);
                }
                else
                {
                    //This causes an error on the client side.  This is what we want.
                    //Technically the Ticket's identity having bogus info and the lack of access token freaks this out.
                    context.Response.StatusCode = 401;
                    return;
                }
            }
            finally
            {
                if (requestScope != null)
                {
                    requestScope.Dispose();
                    requestScope = null;
                }
            }

            base.Create(context);
        }

        public override void Receive(AuthenticationTokenReceiveContext context)
        {
            ILifetimeScope requestScope = null;
            try
            {
                requestScope = IvinciContainer.Instance.Container().BeginLifetimeScope(MatchingScopeLifetimeTags.RequestLifetimeScopeTag);
                //Get the client 
                //Validate the url


                var ssoApplicationService = requestScope.Resolve<ISsoOauthIssuerApplicationService>();
                var accessToken = ssoApplicationService.GetAccessTokenWithTokenString(context.Token);
                if (accessToken.IsActive)
                {
                    context.DeserializeTicket(accessToken.Ticket);
                }
                //else
                //{
                //    context.Response.StatusCode = 401;
                //    context.Response.ContentType = MimeTypes.Application.Json;
                //    context.Response.ReasonPhrase = "Not Authenticated";
                //    return;
                //}
            }
            finally
            {
                if (requestScope != null)
                {
                    requestScope.Dispose();
                    requestScope = null;
                }
            }


            base.Receive(context);
        }
    }
}
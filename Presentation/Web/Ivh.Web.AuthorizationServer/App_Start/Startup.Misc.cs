﻿namespace Ivh.Web.AuthorizationServer
{
    using System.Web.Helpers;
    using Common.Web.Cookies;
    using Owin;

    public partial class Startup
    {
        public void ConfigureMisc(IAppBuilder app)
        {
            // for future use
            AntiForgeryConfig.CookieName = CookieNames.AuthorizationServer.AntiForgeryString;

        }
    }
}
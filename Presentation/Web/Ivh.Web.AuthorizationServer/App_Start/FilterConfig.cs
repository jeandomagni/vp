﻿namespace Ivh.Web.AuthorizationServer
{
    using System.Web.Mvc;
    using Common.Web.Filters;

    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new LoggingCorrelationAttribute());
            filters.Add(new HandleErrorAttribute());
        }
    }
}

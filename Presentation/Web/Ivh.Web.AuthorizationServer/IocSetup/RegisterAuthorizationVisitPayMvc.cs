﻿namespace Ivh.Web.AuthorizationServer.IocSetup
{
    using System.Web;
    using AuthorizationServer;
    using Autofac;
    using Autofac.Integration.Mvc;
    using Common.DependencyInjection;
    using Common.DependencyInjection.Registration;
    using Common.DependencyInjection.Registration.Register;
    using Common.Session;
    using Common.Web.Interfaces;
    using Common.Web.Services;
    using Microsoft.Owin;
    using Microsoft.Owin.Security;
    using Owin;

    public class RegisterAuthorizationVisitPayMvc : IRegistrationModule
    {
        private readonly IAppBuilder _app;

        public RegisterAuthorizationVisitPayMvc(IAppBuilder app)
        {
            this._app = app;
        }

        public void Register(ContainerBuilder builder)
        {
            RegistrationSettings registrationSettings = RegistrationSettingsService.GetRegistrationSettings();
            Base.Register(builder, registrationSettings);
            VisitPay.Register(builder, registrationSettings, true);
            Data.Register(builder, registrationSettings)
                .AddVisitPayDatabase()
                .AddEnterpriseDatabase()
                .WithQaToolsFluentMappingModules();
            Mvc.Register(builder, this._app);

            builder.RegisterType<BaseControllerService>().As<IBaseControllerService>().InstancePerRequest();
            builder.RegisterControllers(typeof(MvcApplication).Assembly);
            builder.Register<IAuthenticationManager>(c => HttpContext.Current.GetOwinContext().Authentication).InstancePerRequest();
            builder.Register(ctx => HttpContext.Current.GetOwinContext()).As<IOwinContext>();
            builder.RegisterType<WebAuthorizationServerSessionFacade>().As<IWebAuthorizationServerSessionFacade>();
        }
    }
}
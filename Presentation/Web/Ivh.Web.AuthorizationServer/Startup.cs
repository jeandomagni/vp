﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Ivh.Web.AuthorizationServer.Startup))]
namespace Ivh.Web.AuthorizationServer
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            this.ConfigureIoc(app);
            this.ConfigureAuth(app);
            this.ConfigureMisc(app);
        }
    }
}

﻿namespace Ivh.Web.AuthorizationServer
{
    using System.Web.Mvc;
    using System.Web.Optimization;
    using System.Web.Routing;
    using Common.Web.Administration;

    public class MvcApplication : System.Web.HttpApplication //This does not use the service bus, so it does not need to inherit Ivh.Common.Web.Application.HttpApplicationBase
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            ApplicationHost.ConfigureStartUp();
        }
    }
}

﻿namespace Ivh.Web.AuthorizationServer.Controllers
{
    using System;
    using System.Web.Mvc;
    using Common.Web.Controllers;
    using Common.Web.Filters;
    using Common.Web.Interfaces;

    //[Authorize(Roles = VisitPayRoleStrings.System.Client + "," + VisitPayRoleStrings.Admin.Administrator)]
    public class BaseController : BaseWebController
    {
        public BaseController(Lazy<IBaseControllerService> baseControllerService)
            : base(baseControllerService)
        {
        }
    }
}
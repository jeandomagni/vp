﻿namespace Ivh.Web.AuthorizationServer.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Security.Claims;
    using System.Threading.Tasks;
    using System.Web;
    using System.Web.Helpers;
    using Application.Core.Common.Interfaces;
    using Common.Web.Interfaces;
    using System.Web.Mvc;
    using System.Web.Routing;
    using AutoMapper;
    using Common.Base.Enums;
    using Common.VisitPay.Enums;
    using Common.VisitPay.Strings;
    using Microsoft.AspNet.Identity;
    using Microsoft.Owin.Security;
    using Models;
    using Ivh.Application.User.Common.Dtos;

    [Authorize(Roles = VisitPayRoleStrings.System.Patient + "," + VisitPayRoleStrings.Admin.Administrator)]
    public class AccountController : BaseController
    {
        private readonly Lazy<IVisitPayUserApplicationService> _visitPayUserApplicationService;

        public AccountController(
            Lazy<IBaseControllerService> baseControllerService,
            Lazy<IVisitPayUserApplicationService> visitPayUserApplicationService
            )
            : base(baseControllerService)
        {
            this._visitPayUserApplicationService = visitPayUserApplicationService;
        }

        [AllowAnonymous]
        public ActionResult LogOff(bool timeout = false)
        {
            this.LogOffPrivate(timeout);
            return this.RedirectToAction("Index", "Home");
        }

        private void LogOffPrivate(bool timeout = false)
        {
            //var authentication = this.HttpContext.GetOwinContext().Authentication;
            //authentication.SignOut();
            if (this.User.Identity.IsAuthenticated)
            {
                this._visitPayUserApplicationService.Value.SignOut(this.User.Identity.Name, timeout);
                this.AuthenticationManager.SignOut();
            }
            this.SessionFacade.Value.ClearSession();
        }

        [AllowAnonymous]
        public ActionResult OAuthLogin(string returnUrl, string ssoToken)
        {
            AuthenticationTicket ticket = Startup.OAuthBearerAuthenticationOptions.AccessTokenFormat.Unprotect(ssoToken);
            if (ticket.Properties.ExpiresUtc > DateTime.UtcNow && ticket.Identity.HasClaim("urn:oauth:scope", "sso"))
            {
                var user = this._visitPayUserApplicationService.Value.FindByName(ticket.Identity.Name);
                if (user != null)
                {
                    ClaimsIdentity identity = this.HttpContext.User.Identity.IsAuthenticated
                    ? (ClaimsIdentity)this.HttpContext.User.Identity
                    : new ClaimsIdentity(ticket.Identity.Claims, "Application");

                    //add oauth claims to authenticated user
                    if (this.HttpContext.User.Identity.IsAuthenticated)
                    {
                        foreach (Claim claim in ticket.Identity.Claims.Where(x => x.Type.Equals("urn:oauth:scope")))
                        {
                            if (!identity.HasClaim(c => c.Equals(claim)))
                            {
                                identity.AddClaim(claim);
                            }
                        }
                    }

                    this.AuthenticationManager.AuthenticationResponseGrant = new AuthenticationResponseGrant(new ClaimsPrincipal(identity), new AuthenticationProperties() { IsPersistent = true });

                    if (!this.HttpContext.User.Identity.IsAuthenticated)
                    {
                        this.AuthenticationManager.SignIn(new AuthenticationProperties { IsPersistent = true }, identity);
                    }
                }
                return this.RedirectToAction("HealthEquityAccount", "MockSsoResource");
            }
            return this.RedirectToAction("Login");
        }


        [HttpGet]
        [AllowAnonymous]
        public ActionResult Login(string returnUrl, int? sm)
        {
            if (this.User.Identity.IsAuthenticated)
                return this.KillExistingSession(returnUrl);

            LoginViewModel model = new LoginViewModel { ReturnUrl = returnUrl };

            if (sm.HasValue)
            {
                this.ModelState.AddModelError("", "A session error has occurred, please try again.");
            }

            return !this.IsBrowserSupported ? this.View("~/Views/Shared/BrowserNotSupported.cshtml") : this.View(model);
        }


        //
        // POST: /Account/Login
        [HttpPost]
        [AllowAnonymous]
        public async Task<ActionResult> Login(LoginViewModel model)
        {
            HttpContext httpContext = System.Web.HttpContext.Current;
            if (this.User.Identity.IsAuthenticated)
            {
                return this.KillExistingSession(model.ReturnUrl, true);
            }

            AntiForgery.Validate();

            if (!this.ModelState.IsValid)
            {
                return this.View(model);
            }
            //I ripped a bunch of the guts out of this, we should refactor the patient/Client to use common controller perhaps if we're going to use this in production
            SignInStatusExEnum result = await this._visitPayUserApplicationService.Value.PasswordSignInAsync(Mapper.Map<HttpContextDto>(httpContext), model.Username, model.Password, false, true, VisitPayRoleStrings.System.Patient, this.AddSsoProperties, authenticationType: "Application");
            await this.BruteForceDelayAsync(httpContext.Request.UserHostAddress, () => result == SignInStatusExEnum.Failure, () => result == SignInStatusExEnum.Success);
            switch (result)
            {
                case SignInStatusExEnum.Success:
                    return this.RedirectToLocal(model.ReturnUrl);
                //break;
                case SignInStatusExEnum.LockedOut:
                    this.ModelState.AddModelError("", string.Format("This account is locked out.  Cannot login."));
                    break;
                case SignInStatusExEnum.RequiresVerification:
                    this.ModelState.AddModelError("", string.Format("This account requires verification.  Cannot login."));
                    break;
                case SignInStatusExEnum.PasswordExpired:
                    this.ModelState.AddModelError("", string.Format("Password is expired.  Cannot login."));
                    break;
                case SignInStatusExEnum.RequiresSecurityQuestions:
                    this.ModelState.AddModelError("", string.Format("This account requires security questions.  Cannot login."));
                    break;
                case SignInStatusExEnum.AccountClosed:
                    this.ModelState.AddModelError("", string.Format("This account has been canceled."));
                    break;
                default:
                    this.ModelState.AddModelError("", "Invalid login attempt.");
                    break;
            }
            return this.View(model);
        }
        private async Task AddSsoProperties(SignInStatusExEnum result, int visitPayUserId, HttpContextDto context, IList<Claim> claims)
        {
            await Task.Run(() =>
            {
                //if (result == SignInStatusEx.Success)
                //{
                //    Guarantor guarantor = this._guarantorService.Value.GetGuarantorEx(visitPayUserId);
                //    var authentication = context.GetOwinContext().Authentication;
                //    authentication.SignIn(
                //        new AuthenticationProperties { IsPersistent = true },
                //        new ClaimsIdentity(new[] { new Claim(ClaimsIdentity.DefaultNameClaimType, guarantor.User.UserName) }, "Application"));
                //}
            });
        }


        [HttpPost]
        [AllowAnonymous]
        public ActionResult External()
        {
            //Seems like this might just be if we werent going to use the Idenity stuff
            IAuthenticationManager authentication = this.HttpContext.GetOwinContext().Authentication;
            if (this.Request.HttpMethod == "POST")
            {
                foreach (string key in this.Request.Form.AllKeys)
                {
                    if (key.StartsWith("submit.External.") && !string.IsNullOrEmpty(this.Request.Form.Get(key)))
                    {
                        string authType = key.Substring("submit.External.".Length);
                        authentication.Challenge(authType);
                        return new HttpUnauthorizedResult();
                    }
                }
            }
            ClaimsIdentity identity = authentication.AuthenticateAsync("External").Result.Identity;
            if (identity != null)
            {
                authentication.SignOut("External");
                authentication.SignIn(
                    new AuthenticationProperties { IsPersistent = true },
                    new ClaimsIdentity(identity.Claims, "Application", identity.NameClaimType, identity.RoleClaimType));
                return this.Redirect(this.Request.QueryString["ReturnUrl"]);
            }

            return View();
        }

        #region Helpers

        // Used for XSRF protection when adding external logins
        private const string XsrfKey = "XsrfId";

        private IAuthenticationManager AuthenticationManager
        {
            get { return this.HttpContext.GetOwinContext().Authentication; }
        }

        private void AddErrors(IdentityResult result)
        {
            foreach (string error in result.Errors)
            {
                this.ModelState.AddModelError("", error);
            }
        }

        private ActionResult RedirectToLocal(string returnUrl)
        {
            if (this.Url.IsLocalUrl(returnUrl))
            {
                return this.Redirect(returnUrl);
            }
            return this.RedirectToAction("Index", "Home");
        }

        private ActionResult KillExistingSession(string returnUrl, bool showSystemMessage = false)
        {

            this._visitPayUserApplicationService.Value.SignOut(this.User.Identity.Name, false);
            this.AuthenticationManager.SignOut();
            this.SessionFacade.Value.ClearSession();

            RouteValueDictionary routeValues = new RouteValueDictionary { { "returnUrl", returnUrl } };

            if (showSystemMessage)
                routeValues.Add("sm", 1);

            return this.RedirectToAction("Login", "Account", routeValues);
        }
        #endregion

    }
}
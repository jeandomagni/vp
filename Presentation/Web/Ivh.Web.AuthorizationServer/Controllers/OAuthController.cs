﻿namespace Ivh.Web.AuthorizationServer.Controllers
{
    using System.Web;
    using System.Web.Mvc;
    using System.Security.Claims;
    //https://github.com/DotNetOpenAuth/DotNetOpenAuth/blob/develop/samples/OAuthAuthorizationServer/Controllers/OAuthController.cs
    //That is probably the better example here.  
    //https://github.com/IdentityServer/IdentityServer3
    //This is probably the framework we should have used here.  It's a katana/.net based and seems much more flexible than the framework used here.
    public class OAuthController : Controller
    {
        public ActionResult Authorize()
        {
            if (Response.StatusCode != 200)
            {
                return View("AuthorizeError");
            }

            var authentication = HttpContext.GetOwinContext().Authentication;
            var ticket = authentication.AuthenticateAsync("Application").Result;
            var identity = ticket != null ? ticket.Identity : null;
            if (identity == null)
            {
                authentication.Challenge("Application");
                return new HttpUnauthorizedResult();
            }

            var scopes = (Request.QueryString.Get("scope") ?? "").Split(' ');

            if (Request.HttpMethod == "POST")
            {
                if (!string.IsNullOrEmpty(Request.Form.Get("submit.Grant")))
                {
                    identity = new ClaimsIdentity(identity.Claims, "Bearer", identity.NameClaimType, identity.RoleClaimType);
                    foreach (var scope in scopes)
                    {
                        identity.AddClaim(new Claim("urn:oauth:scope", scope));
                    }
                    authentication.SignIn(identity);
                }
                if (!string.IsNullOrEmpty(Request.Form.Get("submit.Login")))
                {
                    authentication.SignOut("Application");
                    authentication.Challenge("Application");
                    return new HttpUnauthorizedResult();
                }
                if (!string.IsNullOrEmpty(Request.Form.Get("submit.Decline")))
                {
                    //Do nothing?
                    identity = new ClaimsIdentity(null, "Bearer", identity.NameClaimType, identity.RoleClaimType);
                    authentication.SignIn(identity);
                }
            }

            return View();
        }
    }
}
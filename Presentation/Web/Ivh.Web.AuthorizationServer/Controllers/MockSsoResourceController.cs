﻿﻿using Ivh.Application.Qat.Common.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace Ivh.Web.AuthorizationServer.Controllers
{
    using System.Globalization;
    using System.Security.Claims;
    using Application.Qat.Common.Dtos;
    using Attributes;
    using Common.Base.Enums;
    using Common.Base.Utilities.Extensions;
    using Common.VisitPay.Enums;
    using Common.Web.Constants;
    using Microsoft.AspNet.Identity;
    using Models;
    using Models.HealthEquityApiModels;

    [Authorize]
    public class MockSsoResourceController : Controller
    {
        private readonly Lazy<IMockHealthEquityEndpointApplicationService> _mockHealthEquityEndpointApplicationService;

        public MockSsoResourceController(
            Lazy<IMockHealthEquityEndpointApplicationService> mockHealthEquityEndpointApplicationService
            )
        {
            this._mockHealthEquityEndpointApplicationService = mockHealthEquityEndpointApplicationService;
        }

        [HttpGet]
        public ActionResult OAuthGetSsoToken()
        {
            string accessToken = null;
            if (this.Request.Headers.AllKeys.Contains(HttpHeaders.Request.Authorization))
            {
                accessToken = this.Request.Headers[HttpHeaders.Request.Authorization].Replace("Bearer ", "");
            }
            return this.Json(new
            {
                SsoToken = accessToken
            }, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        [OAuthScopeAuthorize("sso")]
        public ActionResult HealthEquityAccount()
        {
            HealthEquityAccountViewModel model = new HealthEquityAccountViewModel();

            if (this.User.Identity is ClaimsIdentity identity)
            {
                Claim data = identity.FindFirst(nameof(ClaimTypeEnum.GuarantorId));
                int vpGuarantorId = int.Parse(data.Value);
                int accountFlags = 1;
                int balanceTypeFlags = 1;
                IList<AccountStatusApiModelEnum> accountFlagsList = this.FlagsMaskToEnumList<AccountStatusApiModelEnum>(accountFlags).ToList();
                IList<AccountTypeFlagApiModelEnum> balanceTypeFlagsList = this.FlagsMaskToEnumList<AccountTypeFlagApiModelEnum>(balanceTypeFlags).ToList();

                IList<SavingsAccountInfoDto> savingsDtos = this._mockHealthEquityEndpointApplicationService.Value.GetSavingsAccountInfoDtos(vpGuarantorId, accountFlagsList, balanceTypeFlagsList);

                model.AvailableBalance = savingsDtos.Sum(x => x.AvailableBalance).FormatCurrency();
                model.InvestmentAccountBalance = savingsDtos.Sum(x => x.Investments).FormatCurrency();
                model.InvestmentReturnPercent = 0.018m.FormatPercentage();

                string firstName = identity.Claims.FirstOrDefault(x => x.Type.Equals(nameof(ClaimTypeEnum.ClientFirstName)))?.Value;
                string lastName = identity.Claims.FirstOrDefault(x => x.Type.Equals(nameof(ClaimTypeEnum.ClientLastName)))?.Value;
                string fullName = $"{firstName} {lastName}";
                fullName = fullName.Length > 0 ? fullName : identity.GetUserName();

                model.UserName = fullName;
            }

            return this.View(model);
        }

        [HttpGet]
        [OAuthScopeAuthorize("hsabalance")]
        public ActionResult GetHealthEquityHsaBalance(int? accountFlags, int? balanceTypeFlags)
        {
            MemberBalanceReturnApiModel model = new MemberBalanceReturnApiModel() { Status = ReturnStatusApiModelEnum.MemberNotFound };

            if (this.User.Identity is ClaimsIdentity identity)
            {
                Claim data = identity.FindFirst(nameof(ClaimTypeEnum.GuarantorId));
                model.MemberId = data.Value;
                model.Status = ReturnStatusApiModelEnum.Success;
                model.ReimbursementAccountInfos = new List<ReimbursementAccountInfoApiModel>();
                model.SavingsAccountInfos = new List<SavingsAccountInfoApiModel>();

                IList<AccountStatusApiModelEnum> accountFlagsList = this.FlagsMaskToEnumList<AccountStatusApiModelEnum>(accountFlags ?? 1).ToList();
                IList<AccountTypeFlagApiModelEnum> balanceTypeFlagsList = this.FlagsMaskToEnumList<AccountTypeFlagApiModelEnum>(balanceTypeFlags ?? 1).ToList();
                int vpGuarantorId = int.Parse(data.Value);

                IList<SavingsAccountInfoDto> savingsDtos = this._mockHealthEquityEndpointApplicationService.Value.GetSavingsAccountInfoDtos(vpGuarantorId, accountFlagsList, balanceTypeFlagsList);

                foreach (SavingsAccountInfoDto savingsDto in savingsDtos)
                {
                    SavingsAccountInfoApiModel savings = new SavingsAccountInfoApiModel()
                    {
                        AvailableBalance = savingsDto.AvailableBalance,
                        Investments = savingsDto.Investments,
                        Contributions = savingsDto.Contributions,
                        Distributions = savingsDto.Distributions,
                        Type = savingsDto.Type,
                        Status = savingsDto.Status
                    };
                    model.SavingsAccountInfos.Add(savings);
                }
            }

            return this.Json(model, JsonRequestBehavior.AllowGet);
        }

        private IEnumerable<TFlagEnum> FlagsMaskToEnumList<TFlagEnum>(int flagsMask) where TFlagEnum : struct, IConvertible
        {
            foreach (TFlagEnum flagEnum in Enum.GetValues(typeof(TFlagEnum)).Cast<TFlagEnum>())
            {
                int enumValue = flagEnum.ToInt32(CultureInfo.InvariantCulture.NumberFormat);

                //mask & targ == targ
                //0011 & 0010 == 0010
                bool flagSet = (flagsMask & enumValue) == enumValue;
                if (flagSet)
                {
                    yield return flagEnum;
                }
            }
        }
    }
}
﻿namespace Ivh.Web.AuthorizationServer.Controllers
{
    using System.Web.Mvc;
    using Common.VisitPay.Strings;


    [Authorize(Roles = VisitPayRoleStrings.System.Patient + "," + VisitPayRoleStrings.Admin.Administrator)]
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

    }
}
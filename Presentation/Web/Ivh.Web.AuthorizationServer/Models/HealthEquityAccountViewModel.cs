﻿namespace Ivh.Web.AuthorizationServer.Models
{
    public class HealthEquityAccountViewModel
    {
        public string AvailableBalance { get; set; }
        public string InvestmentAccountBalance { get; set; }
        public string InvestmentReturnPercent { get; set; }
        public string UserName { get; set; }
    }
}
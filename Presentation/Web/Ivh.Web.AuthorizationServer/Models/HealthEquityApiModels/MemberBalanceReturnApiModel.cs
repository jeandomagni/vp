﻿namespace Ivh.Web.AuthorizationServer.Models.HealthEquityApiModels
{
    using System.Collections.Generic;

    /// <summary>
    /// Copy from the HQY SOAP generated structures.  Assuming the structure is going to be the same for OAuth balance service.
    /// </summary>
    internal class MemberBalanceReturnApiModel
    {
        public string MemberId { get; set; }
        public ReturnStatusApiModelEnum Status { get; set; }
        public IList<SavingsAccountInfoApiModel> SavingsAccountInfos { get; set; }
        public IList<ReimbursementAccountInfoApiModel> ReimbursementAccountInfos { get; set; }
    }
}
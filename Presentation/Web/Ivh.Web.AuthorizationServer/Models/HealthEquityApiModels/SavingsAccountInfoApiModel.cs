﻿namespace Ivh.Web.AuthorizationServer.Models.HealthEquityApiModels
{
    using Common.Base.Enums;
    using Common.VisitPay.Enums;

    /// <summary>
    /// Copy from the HQY SOAP generated structures.  Assuming the structure is going to be the same for OAuth balance service.
    /// </summary>
    internal class SavingsAccountInfoApiModel
    {
        public AccountTypeFlagApiModelEnum Type { get; set; }
        public string AccountId { get; set; }
        public string Description { get; set; }
        public decimal AvailableBalance { get; set; }
        public decimal Investments { get; set; }
        public decimal Contributions { get; set; }
        public decimal Distributions { get; set; }
        public AccountStatusApiModelEnum Status { get; set; }
    }
}
REM Lower the time a connection sits in TIME_WAIT to the minimum value
echo y|reg add HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Services\Tcpip\Parameters /v TcpTimedWaitDelay /t REG_DWORD /d 30
pause
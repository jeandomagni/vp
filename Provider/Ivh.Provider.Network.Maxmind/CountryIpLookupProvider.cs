﻿namespace Ivh.Provider.Network.Maxmind
{
    using System;
    using System.Threading;
    using Application.Logging.Common.Interfaces;
    using Common.Base.Enums;
    using Common.Base.Utilities.Helpers;
    using Common.VisitPay.Enums;
    using Domain.Core.RestrictedCountryAccess.Interfaces;
    using Domain.Settings.Entities;
    using Domain.Settings.Interfaces;
    using MaxMind.GeoIP2;
    using MaxMind.GeoIP2.Responses;
    
    public class CountryIpLookupProvider : ICountryIpLookupProvider
    {
        private readonly Lazy<ILoggingApplicationService> _logger;
        private static WebServiceClient _webServiceClient = null;
        private static readonly SemaphoreSlim ClientLock = new SemaphoreSlim(1, 1);
        private static DateTime _expTime = DateTime.UtcNow;
        private const int MinutesToCache = 10;

        private void SetupWebClient(int? maxmindAccountId, string maxmindLicenseKey)
        {
            if ((_webServiceClient == null || _expTime <= DateTime.UtcNow) && maxmindAccountId.HasValue && maxmindLicenseKey != null)
            {
                _webServiceClient = new WebServiceClient(maxmindAccountId.Value, maxmindLicenseKey);//AccountId, and License key
            }
        }

        public CountryIpLookupProvider(
            Lazy<IClientService> clientService,
            Lazy<IFeatureService> featureService,
            Lazy<ILoggingApplicationService> logger
            )
        {
            this._logger = logger;
            if (_expTime <= DateTime.UtcNow)
            {
                if (ClientLock.Wait(new TimeSpan(0, 0, 10)))
                {
                    try
                    {
                        if (featureService.Value.IsFeatureEnabled(VisitPayFeatureEnum.FeatureRestrictIpAddressByCountry))
                        {
                            Client client = clientService.Value.GetClient();
                            this.SetupWebClient(client.MaxmindAccountNumber, client.MaxmindLicenseKey);
                            _expTime = DateTime.UtcNow.AddMinutes(MinutesToCache);
                        }
                    }
                    finally
                    {
                        ClientLock.Release();
                    }
                }
            }
        }

        public string GetCountryForIp(string ip)
        {
            if (_webServiceClient == null)
            {
                return string.Empty;
            }

            CountryResponse result = null;
            try
            {
                result = _webServiceClient.Country(ip);
            }
            catch (Exception e)
            {
                this._logger.Value.Fatal(() => string.Format("Calling the MaxMind GeoIP2 service threw, {0}", ExceptionHelper.AggregateExceptionToString(e)));
                return string.Empty;
            }

            if (result?.Country != null)
            {
                return result.Country.IsoCode;
            }

            return string.Empty;
        }
    }
}
﻿namespace Ivh.Provider.FinanceManagement.HealthEquity
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Net;
    using System.Net.Http;
    using System.ServiceModel;
    using System.ServiceModel.Description;
    using System.ServiceModel.Dispatcher;
    using System.Threading.Tasks;
    using ApiModels;
    using Application.FinanceManagement.Common.Dtos;
    using AutoMapper;
    using Common.Base.Enums;
    using Common.Base.Utilities.Extensions;
    using Common.Base.Utilities.Helpers;
    using Domain.Core.Sso.Entities;
    using Domain.FinanceManagement.Interfaces;
    using Domain.Logging.Interfaces;
    using Domain.Settings.Enums;
    using Domain.Settings.Interfaces;
    using DotNetOpenAuth.OAuth2;
    using Newtonsoft.Json;

    public class HealthEquityBalanceWebServiceProvider : IHsaBalanceProvider, ISsoTokenProvider
    {
        private readonly Lazy<ILoggingProvider> _logger;
        private readonly Uri _healthEquityApiUri;
        private readonly IDictionary<string, string> _userNames;
        private readonly IDictionary<string, string> _passwords;
        private readonly string _healthEquityOAuthApiPath;
        private readonly string _healthEquityOAuthApiResourceBase;
        private readonly string _healthEquityOAuthSsoTokenApiPath;
        private readonly Lazy<IApplicationSettingsService> _applicationSettingsService;

        private WebServerClient GetWebServerClient(SsoProvider ssoProvider)
        {
            Uri authorizationServerUri = new Uri(ssoProvider.AuthorizationServerBaseAddress);
            AuthorizationServerDescription authorizationServer = new AuthorizationServerDescription
            {
                AuthorizationEndpoint = new Uri(authorizationServerUri, ssoProvider.AuthorizePath),
                TokenEndpoint = new Uri(authorizationServerUri, ssoProvider.TokenPath)
            };

            string ssoProviderSecret = this._applicationSettingsService.Value.GetString(ssoProvider.SsoProviderSecretSettingsKey).Value;
            WebServerClient webServerClient = new WebServerClient(authorizationServer, ssoProvider.SsoProviderSourceSystemKey, ssoProviderSecret);
            return webServerClient;
        }

        public HealthEquityBalanceWebServiceProvider(
            Lazy<IApplicationSettingsService> applicationSettingsService,
            Lazy<ILoggingProvider> logger
            )
        {
            this._applicationSettingsService = applicationSettingsService;
            this._logger = logger;
            this._healthEquityApiUri = applicationSettingsService.Value.GetUrl(UrlEnum.HealthEquityApi);
            this._userNames = applicationSettingsService.Value.HealthEquityApiUserNames.Value;
            this._passwords = applicationSettingsService.Value.HealthEquityApiPasswords.Value;
            this._healthEquityOAuthApiPath = applicationSettingsService.Value.HealthEquityOAuthMemberBalancePath.Value;
            this._healthEquityOAuthApiResourceBase = applicationSettingsService.Value.HealthEquityOAuthResourceServerBaseAddress.Value;
            this._healthEquityOAuthSsoTokenApiPath = applicationSettingsService.Value.HealthEquityOAuthSsoTokenPath.Value;
        }

        public async Task<HealthEquitySsoTokenResponse> GetSsoToken(string authToken, SsoProvider ssoProvider)
        {
            HealthEquitySsoTokenResponse returnResponse = new HealthEquitySsoTokenResponse() { WasSuccessful = false};
            if (string.IsNullOrWhiteSpace(authToken))
            {
                returnResponse.AuthTokenExpired = true;
                return returnResponse;
            }
            WebServerClient webServerClient = this.GetWebServerClient(ssoProvider);
            string message = String.Empty;
            using (HttpClient httpClient = new HttpClient(webServerClient.CreateAuthorizingHandler(authToken)))
            {
                HttpResponseMessage response = null;
                try
                {

                    Uri baseUri = new Uri(this._healthEquityOAuthApiResourceBase);
                    Uri myUri = new Uri(baseUri, string.Format(this._healthEquityOAuthSsoTokenApiPath));

                    response = await httpClient.GetAsync(myUri);
                    response.EnsureSuccessStatusCode();
                    message = await response.Content.ReadAsStringAsync();
                    HealthEquitySsoTokenApiModel ssoTokenResponse = JsonConvert.DeserializeObject<HealthEquitySsoTokenApiModel>(message);
                    if (ssoTokenResponse != null)
                    {
                        returnResponse.SsoToken = ssoTokenResponse.SsoToken;
                        returnResponse.WasSuccessful = true;
                    }
                }
                catch (Exception e)
                {
                    returnResponse.AuthTokenExpired = true;

                    this._logger.Value.Fatal(() => $"HealthEquityBalanceOauthProvider::GetSsoToken Response={message}, Exception={ExceptionHelper.AggregateExceptionToString(e)}");
                    return returnResponse;
                }
            }
            return returnResponse;
        }
        public async Task<HealthEquityBalanceResponse> GetBalanceForOauthAsync(string authToken, SsoProvider ssoProvider)
        {
            HealthEquityBalanceResponse returnResponse = new HealthEquityBalanceResponse() { WasSuccessful = false };
            MemberBalanceReturnApiModel hqyModel = null;
            WebServerClient webServerClient = this.GetWebServerClient(ssoProvider);
            string message = String.Empty;
            if (string.IsNullOrWhiteSpace(authToken))
            {
                returnResponse.AuthTokenExpired = true;
                return returnResponse;
            }
            //Cant use a static HttpClient, since the httpclient requires a special handler to make this work per request.  Didnt see a way to setup the request via the getstringasync or similar
            using (HttpClient httpClient = new HttpClient(webServerClient.CreateAuthorizingHandler(authToken)))
            {
                HttpResponseMessage response = null;
                try
                {
                    const int accountFlags = 1; // DO NOT CHANGE this value with out consulting product. There could be legal implications.
                    const int balanceTypeFlags = 1; // DO NOT CHANGE this value with out consulting product. There could be legal implications.

                    Uri baseUri = new Uri(this._healthEquityOAuthApiResourceBase);
                    Uri myUri = new Uri(baseUri, string.Format(this._healthEquityOAuthApiPath, accountFlags, balanceTypeFlags));

                    response = await httpClient.GetAsync(myUri);
                    response.EnsureSuccessStatusCode();
                    message = await response.Content.ReadAsStringAsync();
                    hqyModel = JsonConvert.DeserializeObject<MemberBalanceReturnApiModel>(message);
                    if (hqyModel != null
                        && hqyModel.SavingsAccountInfos.Where(x => x.Type == AccountTypeFlagApiModelEnum.HSA).IsNotNullOrEmpty()
                    )
                    {
                        returnResponse.CashBalance = hqyModel.SavingsAccountInfos.FirstOrDefault(x => x.Type == AccountTypeFlagApiModelEnum.HSA).AvailableBalance;
                        returnResponse.InvestmentBalance = hqyModel.SavingsAccountInfos.FirstOrDefault(x => x.Type == AccountTypeFlagApiModelEnum.HSA).Investments;
                        returnResponse.ContributionsYtd = hqyModel.SavingsAccountInfos.FirstOrDefault(x => x.Type == AccountTypeFlagApiModelEnum.HSA).Contributions;
                        returnResponse.DistributionsYtd = hqyModel.SavingsAccountInfos.FirstOrDefault(x => x.Type == AccountTypeFlagApiModelEnum.HSA).Distributions;
                    }
                }
                catch (Exception e)
                {
                    returnResponse.AuthTokenExpired = true;
                    
                    this._logger.Value.Fatal(() => string.Format("HealthEquityBalanceOauthProvider::GetBalanceForOauthAsync Response={0}, Exception={1}", message, ExceptionHelper.AggregateExceptionToString(e)));
                    return returnResponse;
                }
            }

            if (hqyModel != null
                && hqyModel.SavingsAccountInfos.Where(x => x.Type == AccountTypeFlagApiModelEnum.HSA).IsNotNullOrEmpty())
            {
                returnResponse = Mapper.Map<HealthEquityBalanceResponse>(returnResponse);
                returnResponse.WasSuccessful = true;
            }
            return returnResponse;
        }

        public async Task<HealthEquityBalanceResponse> GetBalanceForSsnAsync(string ssn, string employerId)
        {
            return await Task.Run(() =>
            {
                HealthEquityBalanceResponse returnResponse = new HealthEquityBalanceResponse() {WasSuccessful = false};
                MemberBalanceReturn memberBalanceReturn = null;
                string serialized = string.Empty;
                try
                {
                    const int accountFlags = 1; // DO NOT CHANGE this value with out consulting product. There could be legal implications.
                    const int balanceTypeFlags = 1 | 2 | 4 | 8; // DO NOT CHANGE this value with out consulting product. There could be legal implications.

                    MemberBalanceWebService client = new MemberBalanceWebService()
                    {
                        Url = this._healthEquityApiUri.AbsoluteUri,
                        MemberBalanceServiceAuthHeaderValue = new MemberBalanceServiceAuthHeader() {Username = this._userNames[employerId], Password = this._passwords[employerId]}
                    };

                    memberBalanceReturn = client.GetMemberBalance(Guid.NewGuid().ToString(), ssn, accountFlags, balanceTypeFlags);
                    if (memberBalanceReturn != null
                        && memberBalanceReturn.Status == ReturnStatus.Success
                        && memberBalanceReturn.SavingsAccountInfos.IsNotNullOrEmpty()
                    )
                    {
                        returnResponse.CashBalance = memberBalanceReturn.SavingsAccountInfos[0].AvailableBalance;
                        returnResponse.InvestmentBalance = memberBalanceReturn.SavingsAccountInfos[0].Investments;
                        returnResponse.ContributionsYtd = memberBalanceReturn.SavingsAccountInfos[0].Contributions;
                        returnResponse.DistributionsYtd = memberBalanceReturn.SavingsAccountInfos[0].Distributions;
                    }

                    serialized = JsonConvert.SerializeObject(memberBalanceReturn);
                }
                catch (Exception e)
                {
                    this._logger.Value.Fatal(() => $"HealthEquityBalanceProvider::GetBalanceForSsn Response={serialized}, Exception={ExceptionHelper.AggregateExceptionToString(e)}");
                    return returnResponse;
                }
                finally
                {
                    //Do I need to clean anything up?
                }
                this._logger.Value.Info(() => $"HealthEquityBalanceProvider::GetBalanceForSsn Response={serialized}");

                if (memberBalanceReturn != null
                    && memberBalanceReturn.Status == ReturnStatus.Success
                    && memberBalanceReturn.SavingsAccountInfos.IsNotNullOrEmpty())
                {
                    returnResponse = Mapper.Map<HealthEquityBalanceResponse>(returnResponse);
                    returnResponse.WasSuccessful = true;
                }

                return returnResponse;
            });
        }


    }
    public class SimpleMessageInspector : IClientMessageInspector
    {
        private readonly Lazy<ILoggingService> _logger;

        public SimpleMessageInspector(Lazy<ILoggingService> logger)
        {
            this._logger = logger;
        }

        public void AfterReceiveReply(ref System.ServiceModel.Channels.Message reply, object correlationState)
        {
            System.ServiceModel.Channels.Message reply1 = reply;
            this._logger.Value.Info(() => $"SimpleMessageInspector::AfterReceiveReply {reply1.ToString()}");
        }

        public object BeforeSendRequest(ref System.ServiceModel.Channels.Message request, IClientChannel channel)
        {
            System.ServiceModel.Channels.Message request1 = request;
            this._logger.Value.Info(() => $"SimpleMessageInspector::BeforeSendRequest {request1.ToString()}");
            return null;
        }
    }
    public class SimpleEndpointBehavior : IEndpointBehavior
    {
        private readonly Lazy<ILoggingService> _logger;

        public SimpleEndpointBehavior(Lazy<ILoggingService> logger)
        {
            this._logger = logger;
        }
        public void AddBindingParameters(ServiceEndpoint endpoint, System.ServiceModel.Channels.BindingParameterCollection bindingParameters)
        {
            // No implementation necessary  
        }

        public void ApplyClientBehavior(ServiceEndpoint endpoint, ClientRuntime clientRuntime)
        {
            clientRuntime.MessageInspectors.Add(new SimpleMessageInspector(this._logger));
        }

        public void ApplyDispatchBehavior(ServiceEndpoint endpoint, EndpointDispatcher endpointDispatcher)
        {
            // No implementation necessary  
        }

        public void Validate(ServiceEndpoint endpoint)
        {
            // No implementation necessary  
        }
    }
}

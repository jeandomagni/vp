namespace Ivh.Provider.FinanceManagement.HealthEquity
{
    /// <summary>
    /// Copy from the HQY SOAP generated structures.  Assuming the structure is going to be the same for OAuth balance service.
    /// </summary>
    internal enum AccountStatusApiModelEnum
    {
        Open = 1,
        Pending = 2,
        Active = 4,
    }
}
namespace Ivh.Provider.FinanceManagement.HealthEquity
{
    /// <summary>
    /// Copy from the HQY SOAP generated structures.  Assuming the structure is going to be the same for OAuth balance service.
    /// </summary>
    internal enum ReturnStatusApiModelEnum
    {
        Success = 1,
        InvalidCredentials = 2,
        MemberNotFound = 4,
        ServerError = 8,
        OptOut = 16,
    }
}
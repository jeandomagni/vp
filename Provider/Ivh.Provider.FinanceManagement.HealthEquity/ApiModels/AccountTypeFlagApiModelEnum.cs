namespace Ivh.Provider.FinanceManagement.HealthEquity
{
    /// <summary>
    /// Copy from the HQY SOAP generated structures.  Assuming the structure is going to be the same for OAuth balance service.
    /// </summary>
    internal enum AccountTypeFlagApiModelEnum
    {
        HSA = 1,
        HRA = 2,
        FSA = 4,
        DCRA = 8,
    }
}
﻿namespace Ivh.Provider.FinanceManagement.HealthEquity
{
    /// <summary>
    /// Copy from the HQY SOAP generated structures.  Assuming the structure is going to be the same for OAuth balance service.
    /// </summary>
    internal class ReimbursementAccountInfoApiModel
    {
        public AccountTypeFlagApiModelEnum Type { get; set; }
        public string AccountId { get; set; }
        public string Description { get; set; }
        public decimal AccountBalance { get; set; }
        public decimal AvailableBalance { get; set; }
        public decimal ElectionAmount { get; set; }
        public decimal YtdClaims { get; set; }
        public decimal YtdClaimsAllowed { get; set; }
        public decimal YtdClaimsDenied { get; set; }
        public decimal YtdClaimsPaid { get; set; }
        public decimal YtdClaimsWaitingPayment { get; set; }
        public decimal CarryOverbalance { get; set; }
        public int PlanYear { get; set; }
        public AccountStatusApiModelEnum Status { get; set; }
    }
}
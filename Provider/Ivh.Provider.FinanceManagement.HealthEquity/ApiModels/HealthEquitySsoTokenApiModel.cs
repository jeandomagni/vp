﻿namespace Ivh.Provider.FinanceManagement.HealthEquity.ApiModels
{
    public class HealthEquitySsoTokenApiModel
    {
        public string SsoToken { get; set; }
    }
}
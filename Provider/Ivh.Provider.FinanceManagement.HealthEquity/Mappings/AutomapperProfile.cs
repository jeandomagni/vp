﻿namespace Ivh.Provider.FinanceManagement.HealthEquity.Mappings
{
    using System.Linq;
    using Application.FinanceManagement.Common.Dtos;
    using AutoMapper;
    using HealthEquityService;

    public class AutomapperProfile : Profile
    {
        public AutomapperProfile()
        {
            this.CreateMap<MemberBalanceMemberBalanceReturn, HealthEquityBalanceResponse>()
                .ForMember(x => x.CashBalance, opts => opts.MapFrom(x => x.SavingsAccountInfos != null ?
                    x.SavingsAccountInfos.Where(z =>
                            (z.Status == MemberBalanceAccountStatus.Active || z.Status == MemberBalanceAccountStatus.Open)
                            && z.Type == MemberBalanceAccountTypeFlag.HSA
                        )
                        .Sum(y => y.AvailableBalance)
                    : 0m))
                .ForMember(x => x.Ssn, opts => opts.MapFrom(x => x.MemberId));
            
        }
    }
}

﻿namespace Ivh.Provider.Core.Audit
{
    using System;
    using System.Collections.Generic;
    using Common.Data.Connection.Connections;
    using Common.Data.Interfaces;
    using Domain.Core.Audit.Entities;
    using Domain.Core.Audit.Interfaces;
    using NHibernate;
    using NHibernate.Transform;

    public class AuditRepository : IAuditRepository
    {
        private ISession Session { get; set; }
        public AuditRepository(ISessionContext<VisitPay> sessionContext)
        {
            this.Session = sessionContext.Session;
        }

        public IList<AuditUserAccess> GetUserAccessLogsSince(DateTime startDate, DateTime? endDate)
        {
            return Session.CreateSQLQuery("exec dbo.PCI_Audit_UserAccess :startDate, :endDate")
                .SetParameter("startDate", startDate)
                .SetParameter("endDate", endDate ?? DateTime.UtcNow)
                .SetResultTransformer(Transformers.AliasToBean(typeof(AuditUserAccess)))
                .List<AuditUserAccess>();
        }

        public IList<AuditUserChange> GetUserChangeLogsSince(DateTime startDate, DateTime? endDate)
        {
            try
            {
                return Session.CreateSQLQuery("exec dbo.PCI_Audit_UserChange :startDate, :endDate")
                        .SetParameter("startDate", startDate)
                        .SetParameter("endDate", endDate ?? DateTime.UtcNow)
                        .SetResultTransformer(Transformers.AliasToBean(typeof(AuditUserChange)))
                        .List<AuditUserChange>();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                throw;
            }
        }

        public IList<AuditUserChangeDetail> GetUserChangeDetailLogsSince(DateTime startDate, DateTime? endDate)
        {
            try
            {
                return Session.CreateSQLQuery("exec dbo.PCI_Audit_UserChangeDetails :startDate, :endDate")
                        .SetParameter("startDate", startDate)
                        .SetParameter("endDate", endDate ?? DateTime.UtcNow)
                        .SetResultTransformer(Transformers.AliasToBean(typeof(AuditUserChangeDetail)))
                        .List<AuditUserChangeDetail>();
            }
            catch (Exception ex)
            {

                Console.WriteLine(ex);
                throw;
            }
        }

        public IList<AuditRoleChange> GetRoleChangeLogsSince(DateTime startDate, DateTime? endDate)
        {
            try
            {
                return Session.CreateSQLQuery("exec dbo.PCI_Audit_RoleChange :startDate, :endDate")
                        .SetParameter("startDate", startDate)
                        .SetParameter("endDate", endDate ?? DateTime.UtcNow)
                        .SetResultTransformer(Transformers.AliasToBean(typeof(AuditRoleChange)))
                        .List<AuditRoleChange>();
            }
            catch (Exception ex)
            {

                Console.WriteLine(ex);
                throw;
            }
        }
    }
}
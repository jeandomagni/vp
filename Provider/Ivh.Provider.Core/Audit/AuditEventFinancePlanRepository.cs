﻿using Ivh.Common.Data;
using NHibernate;

namespace Ivh.Provider.Core.Audit
{
    using System;
    using Common.Data.Connection.Connections;
    using Common.Data.Interfaces;
    using Domain.Core.Audit.Entities;
    using Domain.Core.Audit.Interfaces;

    public class AuditEventFinancePlanRepository : RepositoryBase<AuditEventFinancePlan, VisitPay>, IAuditEventFinancePlanRepository, IDisposable
    {
        public AuditEventFinancePlanRepository(ISessionContext<VisitPay> sessionContext) : base(sessionContext)
        {
        }

        public override void Insert(AuditEventFinancePlan entity)
        {
            if (this.Session.Transaction.IsActive)
            {
                base.Insert(entity);
            }
            else
            {
                using (ITransaction transaction = this.Session.BeginTransaction())
                {
                    base.Insert(entity);
                    transaction.Commit();
                }
            }
        }
        public void Dispose()
        {
            base.CallBackAfterCommit = null;
        }
    }
}

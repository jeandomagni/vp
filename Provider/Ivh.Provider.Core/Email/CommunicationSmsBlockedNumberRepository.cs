﻿namespace Ivh.Provider.Core.Email
{
    using Common.Data;
    using Common.Data.Connection.Connections;
    using Common.Data.Interfaces;
    using Domain.Email.Entities;
    using Domain.Email.Interfaces;
    using NHibernate;

    public class CommunicationSmsBlockedNumberRepository : RepositoryBase<CommunicationSmsBlockedNumber, VisitPay>, ICommunicationSmsBlockedNumberRepository
    {
        public CommunicationSmsBlockedNumberRepository(ISessionContext<VisitPay> sessionContext)
            : base(sessionContext)
        {
        }
    }
}
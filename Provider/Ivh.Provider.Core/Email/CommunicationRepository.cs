﻿namespace Ivh.Provider.Core.Email
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Common.Base.Enums;
    using Common.Base.Utilities.Extensions;
    using Common.Cache;
    using Common.Data;
    using Common.Data.Connection.Connections;
    using Common.Data.Interfaces;
    using Common.VisitPay.Enums;
    using Domain.Email.Entities;
    using Domain.Email.Interfaces;
    using Magnum.Extensions;
    using NHibernate;
    using NHibernate.Criterion;
    using NHibernate.Criterion.Lambda;

    public class CommunicationRepository : RepositoryCacheBase<Communication, VisitPay>, ICommunicationRepository
    {
        public CommunicationRepository(ISessionContext<VisitPay> sessionContext, Lazy<IDistributedCache> cache)
            : base(sessionContext, cache)
        {
        }

        public Communication GetCommunicationByUniqueId(Guid uniqueId)
        {
            return this.GetCached(
                x => x.UniqueId, uniqueId,
                (u) =>
                {
                    ICriteria emailCriteria = this.Session.CreateCriteria(typeof(Communication));
                    emailCriteria.Add(Restrictions.Eq("UniqueId", u));
                    return emailCriteria.UniqueResult<Communication>();
                });
        }

        public int GetCommunicationIdByUniqueId(Guid uniqueId)
        {
            return this.GetCachedId(
                x => x.UniqueId, uniqueId,
                (x) =>
                {
                    ICriteria emailCriteria = this.Session.CreateCriteria(typeof(Communication));
                    emailCriteria.Add(Restrictions.Eq("UniqueId", x));
                    return emailCriteria.UniqueResult<Communication>();
                });
        }

        public int GetUserIdForCommunicationId(int communicationId)
        {
            ICriteria emailCriteria = this.Session.CreateCriteria(typeof(Communication));
            emailCriteria.Add(Restrictions.Eq("CommunicationId", communicationId));
            emailCriteria.SetProjection(Projections.Property("SentToUserId"));
            return emailCriteria.UniqueResult<int?>() ?? 0;
        }

        public EmailResults ForUserId(int visitPayUserId, CommunicationFilter filter, int batch, int batchSize)
        {
            return new EmailResults
            {
                TotalRecords = this.GetCommunicationsTotals(visitPayUserId, filter),
                Emails = this.GetCommunicationsForUserId(visitPayUserId, filter, batch, batchSize)
            };
        }

        public int ForUserIdTotals(int visitPayUserId, CommunicationFilter filter)
        {
            return this.GetCommunicationsTotals(visitPayUserId, filter);
        }

        public IList<Communication> GetUnsentCommunications(CommunicationMethodEnum communicationMethod)
        {
            int communicationMethodId = (int)communicationMethod;
            List<CommunicationStatusEnum> unsentStatusList = new List<CommunicationStatusEnum> { CommunicationStatusEnum.Created, CommunicationStatusEnum.Failed };
            return this.Session.QueryOver<Communication>()
                .Where(x => x.CommunicationStatus.IsIn(unsentStatusList))
                .And(x => x.CommunicationType.CommunicationMethod.CommunicationMethodId == communicationMethodId)
                .List();
        }

        public IList<Communication> GetLastSentEmails(int limit)
        {
            return this.Session.QueryOver<Communication>()
                .OrderBy(x => x.CommunicationId).Desc
                .Take(limit).List();
        }

        public IList<int> GetHeldCommunicationIds(IList<CommunicationTypeEnum> emailTypes = null)
        {
            CommunicationType communicationType = null;

            IQueryOver<Communication, Communication> query = this.Session.QueryOver<Communication>()
                .JoinAlias(x => x.CommunicationType, () => communicationType)
                .Where(x => x.CommunicationStatus == CommunicationStatusEnum.Hold)
                .And(x => communicationType.CommunicationTypeStatus == CommunicationTypeStatusEnum.Active);

            if (emailTypes.IsNotNullOrEmpty())
            {
                query.And(() => communicationType.CommunicationTypeId.IsIn(emailTypes.Select(x => (int)x).ToList()));
            }

            query.Select(Projections.ProjectionList()
                .Add(Projections.Property<Communication>(x => x.CommunicationId)));

            return query.List<int>();
        }

        public Communication GetLatestTextToPayMessageByPhone(string visitPayFormattedPhoneNumber)
        {
            int smsCommunicationMethodId = (int)CommunicationMethodEnum.Sms;
            Communication mostRecentTextToPayCommunication = this.Session.QueryOver<Communication>()
                .Where(x => x.CommunicationType.CommunicationTypeId == CommunicationTypeEnum.TextToPayPaymentOption)
                .Where(x => x.CommunicationType.CommunicationMethod.CommunicationMethodId == smsCommunicationMethodId)
                .Where(x => x.ToAddress == visitPayFormattedPhoneNumber)
                .OrderBy(x => x.CreatedDate).Desc
                .Take(1)
                .SingleOrDefault();

            return mostRecentTextToPayCommunication;
        }

        private IList<Communication> GetCommunicationsForUserId(int visitPayUserId, CommunicationFilter filter, int batch, int batchSize)
        {
            IQueryOver<Communication, Communication> query = this.QueryOverCommunications(visitPayUserId, filter);
            return this.Sort(filter, query).Skip((batch - 1) * batchSize).Take(batchSize).List();
        }

        private int GetCommunicationsTotals(int visitPayUserId, CommunicationFilter filter)
        {
            return this.QueryOverCommunications(visitPayUserId, filter)
                .Select(Projections.Count<Communication>(x => x.CommunicationId))
                .Take(1).SingleOrDefault<int>();
        }

        private IQueryOver<Communication, Communication> QueryOverCommunications(int visitPayUserId, CommunicationFilter filter)
        {
            IQueryOver<Communication, Communication> query = this.Session.QueryOver<Communication>().Where(x => x.SentToUserId == visitPayUserId);

            this.Filter(filter, query);

            return query;
        }

        private void Filter(CommunicationFilter filter, IQueryOver<Communication, Communication> query)
        {
            query.If(filter.CommunicationTypeId.HasValue, q => q.Where(x => (int)x.CommunicationType.CommunicationTypeId == filter.CommunicationTypeId));
            query.If(filter.CommunicationMethodId.HasValue, q => q.Where(x => (int)x.CommunicationType.CommunicationMethod.CommunicationMethodId == filter.CommunicationMethodId));

            if (filter.CommunicationDateRangeFrom.HasValue)
            {
                query.Where(x => x.CreatedDate >= filter.CommunicationDateRangeFrom);
                query.If(filter.CommunicationDateRangeTo.HasValue, q => q.Where(x => x.CreatedDate.Date <= filter.CommunicationDateRangeTo));
            }
            else
            {
                // Broke with VP-6891, fixed with VP-6937
                // Not really sure the difference between CommunicationDateRangeFrom and DateRangeFrom
                // but DateRangeFrom only has one reference and that reference requires the granularity of time
                query.If(filter.DateRangeFrom.HasValue, q => q.Where(x => x.CreatedDate >= filter.DateRangeFrom));
            }
        }

        private IQueryOver<Communication> Sort(CommunicationFilter filter, IQueryOver<Communication, Communication> query)
        {
            if (filter.SortField.IsEmpty())
                return query;

            bool isAscendingOrder = "asc".Equals(filter.SortOrder, StringComparison.InvariantCultureIgnoreCase);

            if(filter.SortField == "CommunicationMethod")
            {
                IQueryOverOrderBuilder<Communication, CommunicationMethod> tempQuery = query.JoinQueryOver(x => x.CommunicationType)
                    .JoinQueryOver(x => x.CommunicationMethod)
                    .OrderBy(x => x.CommunicationMethodName);
                return isAscendingOrder ? tempQuery.Asc : tempQuery.Desc;
            }
            query.UnderlyingCriteria.AddOrder(new Order(filter.SortField, isAscendingOrder));
            return query;
        }

        protected override int GetId(Communication entity)
        {
            return entity.CommunicationId;
        }
    }
}
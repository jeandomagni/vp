﻿namespace Ivh.Provider.Core.Email
{
    using System;
    using System.Linq;
    using Common.Base.Enums;
    using Common.Data;
    using Common.Data.Connection.Connections;
    using Common.Data.Interfaces;
    using Common.VisitPay.Enums;
    using Domain.Email.Entities;
    using Domain.Email.Interfaces;
    using NHibernate;
    using NHibernate.Linq;

    public class CommunicationPreferenceRepository : RepositoryBase<VisitPayUserCommunicationPreference, VisitPay>, ICommunicationPreferenceRepository
    {
        public CommunicationPreferenceRepository(ISessionContext<VisitPay> sessionContext)
            : base(sessionContext)
        {
        }

        public bool Exists(int visitPayUserId, CommunicationMethodEnum communicationMethod, CommunicationTypeEnum communicationTypeId)
        {
            try
            {
                int communicationMethodId = (int)communicationMethod;
                return this.Session
                        .Query<VisitPayUserCommunicationPreference>()
                        .Any(x => x.VisitPayUserId == visitPayUserId &&
                            x.CommunicationMethodId == communicationMethod &&
                            x.CommunicationGroup.CommunicationTypes.Any(ct => ct.CommunicationTypeId == communicationTypeId && ct.CommunicationMethod.CommunicationMethodId == communicationMethodId));
            }
            catch (System.Exception ex)
            {
                Console.WriteLine(ex.Message);
                throw;
            }
        }
    }
}
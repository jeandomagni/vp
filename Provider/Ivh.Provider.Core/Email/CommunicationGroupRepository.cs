﻿namespace Ivh.Provider.Core.Email
{
    using Common.Data;
    using Common.Data.Connection.Connections;
    using Common.Data.Interfaces;
    using Domain.Email.Entities;
    using Domain.Email.Interfaces;
    using NHibernate;

    public class CommunicationGroupRepository : RepositoryBase<CommunicationGroup, VisitPay>, ICommunicationGroupRepository
    {
        public CommunicationGroupRepository(ISessionContext<VisitPay> sessionContext)
            : base(sessionContext)
        {
        }
    }
}
﻿namespace Ivh.Provider.Core.Email
{
    using System.Collections.Generic;
    using System.Linq;
    using Common.Base.Enums;
    using Common.Base.Utilities.Extensions;
    using Common.Data;
    using Common.Data.Connection.Connections;
    using Common.Data.Interfaces;
    using Common.VisitPay.Enums;
    using Domain.Email.Entities;
    using Domain.Email.Interfaces;
    using NHibernate;
    using NHibernate.Criterion;

    public class CommunicationTrackerRepository : RepositoryBase<CommunicationTracker, VisitPay>, ICommunicationTrackerRepository
    {
        public CommunicationTrackerRepository(ISessionContext<VisitPay> sessionContext) : base(sessionContext)
        {
        }

        public bool IsCommunicationForPaymentSent(IList<int> paymentProcessorResponseIds, IList<CommunicationTypeEnum> communicationTypes, CommunicationMethodEnum? communicationMethod)
        {
            if (paymentProcessorResponseIds == null || !paymentProcessorResponseIds.Any())
            {
                return false;
            }

            ICriteria criteria = this.Session.CreateCriteria(typeof(CommunicationTracker));
            criteria.Add(Restrictions.In("PaymentProcessorResponseId", paymentProcessorResponseIds.ToList()));
            return this.IsSent(criteria, communicationTypes, communicationMethod);
        }

        public bool IsCommunicationForFinancePlanSent(int financePlanId, CommunicationTypeEnum communicationType, CommunicationMethodEnum? communicationMethod)
        {
            ICriteria criteria = this.Session.CreateCriteria(typeof(CommunicationTracker));
            criteria.Add(Restrictions.Eq("FinancePlanId", financePlanId));
            return this.IsSent(criteria, communicationType.ToListOfOne(), communicationMethod);
        }

        public bool IsCommunicationForVpStatementSent(int vpStatementId, CommunicationTypeEnum communicationType, CommunicationMethodEnum? communicationMethod)
        {
            ICriteria criteria = this.Session.CreateCriteria(typeof(CommunicationTracker));
            criteria.Add(Restrictions.Eq("VpStatementId", vpStatementId));
            return this.IsSent(criteria, communicationType.ToListOfOne(), communicationMethod);
        }

        private bool IsSent(ICriteria criteria, IList<CommunicationTypeEnum> communicationTypes, CommunicationMethodEnum? communicationMethod)
        {
            criteria.Add(Restrictions.In("CommunicationType.CommunicationTypeId", communicationTypes.ToList()));
            if (communicationMethod.HasValue)
            {
                criteria.Add(Restrictions.Eq("CommunicationType.CommunicationMethod.CommunicationMethodId", (int)communicationMethod.Value));
            }

            return criteria
                .SetProjection(Projections.RowCount())
                .UniqueResult<int>() > 0;
        }
    }
}

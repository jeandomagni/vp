﻿namespace Ivh.Provider.Core.Email
{
    using System;
    using System.Collections.Generic;
    using Common.Data;
    using Common.Data.Connection.Connections;
    using Common.Data.Interfaces;
    using Domain.Email.Entities;
    using Domain.Email.Interfaces;
    using NHibernate;
    using NHibernate.Criterion;

    public class CommunicationEventRepository : RepositoryBase<CommunicationEvent, VisitPay>, ICommunicationEventRepository
    {
        public CommunicationEventRepository(ISessionContext<VisitPay> sessionContext)
            : base(sessionContext)
        {
        }

        public CommunicationEventResults ForCommunicationId(int communicationId, int batch, int batchSize, string sortField, string sortOrder)
        {
            return new CommunicationEventResults
            {
                TotalRecords = this.GetEventTotals(communicationId),
                Events = this.GetEventsForEmailId(communicationId, batch, batchSize, sortField, sortOrder)
            };
        }

        private IList<CommunicationEvent> GetEventsForEmailId(int emailId, int batch, int batchSize, string sortField, string sortOrder)
        {
            IQueryOver<CommunicationEvent, CommunicationEvent> query = this.QueryOverEmailEvents(emailId);
            this.Sort(sortField, sortOrder, query);
            return query.Skip((batch - 1) * batchSize).Take(batchSize).List();
        }

        private int GetEventTotals(int emailId)
        {
            return this.QueryOverEmailEvents(emailId)
                        .Select(Projections.Count<CommunicationEvent>(x => x.CommunicationEventId))
                        .Take(1).SingleOrDefault<int>();
        }

        private IQueryOver<CommunicationEvent, CommunicationEvent> QueryOverEmailEvents(int emailId)
        {
            return this.Session.QueryOver<CommunicationEvent>().Where(x => x.Communication.CommunicationId == emailId);
        }

        private void Sort(string sortField, string sortOrder, IQueryOver<CommunicationEvent, CommunicationEvent> query)
        {
            if (string.IsNullOrEmpty(sortField))
                return;

            bool isAscendingOrder = "asc".Equals(sortOrder, StringComparison.InvariantCultureIgnoreCase);
            query.UnderlyingCriteria.AddOrder(new Order(sortField, isAscendingOrder));
        }
    }
}
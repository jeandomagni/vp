﻿
namespace Ivh.Provider.Core.Email
{
    using Common.Data;
    using Common.Data.Interfaces;
    using Domain.Email.Entities;
    using Domain.Email.Interfaces;
    using Ivh.Common.Data.Connection.Connections;

    public class CommunicationDocumentRepository : RepositoryBase<CommunicationDocument, VisitPay>, ICommunicationDocumentRepository
    {
        public CommunicationDocumentRepository(ISessionContext<VisitPay> sessionContext) : base(sessionContext)
        {
        }
    }
}

﻿namespace Ivh.Provider.Core.Email
{
    using System.Linq;
    using Common.Data;
    using Common.Data.Connection.Connections;
    using Common.Data.Interfaces;
    using Common.VisitPay.Enums;
    using Domain.Email.Entities;
    using Domain.Email.Interfaces;
    using NHibernate;

    public class CommunicationTypeRepository : RepositoryBase<CommunicationType, VisitPay>, ICommunicationTypeRepository
    {
        public CommunicationTypeRepository(ISessionContext<VisitPay> sessionContext)
            : base(sessionContext)
        {
        }

        public CommunicationType GetByCommunicationTypeId(CommunicationTypeEnum communicationTypeEnum, CommunicationMethodEnum communicationMethodEnum)
        {
            int communicationMethodId = (int)communicationMethodEnum;

            CommunicationType item = this.Session
                .QueryOver<CommunicationType>()
                .Cacheable()
                .CacheMode(CacheMode.Normal)
                .List()
                .FirstOrDefault(x => x.CommunicationTypeId == communicationTypeEnum && x.CommunicationMethod.CommunicationMethodId == communicationMethodId);

            return item;
        }
    }
}
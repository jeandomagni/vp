﻿namespace Ivh.Provider.Core.SystemMessage
{
    using System;
    using System.Collections.Generic;
    using Common.Data;
    using Common.Data.Connection.Connections;
    using Common.Data.Interfaces;
    using Domain.Core.SystemMessage.Entities;
    using Domain.Core.SystemMessage.Interfaces;
    using NHibernate;
    using NHibernate.Criterion;

    public class SystemMessageVisitPayUserRepository : RepositoryBase<SystemMessageVisitPayUser, VisitPay>, ISystemMessageVisitPayUserRepository
    {
        public SystemMessageVisitPayUserRepository(ISessionContext<VisitPay> sessionContext)
            : base(sessionContext)
        {
        }

        public IList<SystemMessageVisitPayUser> GetActiveSystemMessagesVisitPayUser(int visitPayUserId)
        {
            SystemMessageVisitPayUser systemMessageVisitPayUserAlias = null;
            SystemMessage systemMessageAlias = null;

            IQueryOver<SystemMessageVisitPayUser, SystemMessageVisitPayUser> query = this.Session.QueryOver(() => systemMessageVisitPayUserAlias)
                .JoinAlias(() => systemMessageVisitPayUserAlias.SystemMessage, () => systemMessageAlias)
                .Where(smvu => smvu.VisitPayUserId == visitPayUserId)
                .Where(smvu => !smvu.MessageHidden)
                .Where(smvu => !smvu.MessageDismissed)
                .Where(smvu => systemMessageAlias.DateActive != null && systemMessageAlias.DateActive < DateTime.UtcNow)
                .Where(smvu => systemMessageAlias.DateExpires == null || systemMessageAlias.DateExpires > DateTime.UtcNow);

            return query.List<SystemMessageVisitPayUser>();
        }

        public SystemMessageVisitPayUser GetSystemMessageVisitPayUser(int visitPayUserId, int systemMessageId)
        {
            SystemMessageVisitPayUser systemMessageVisitPayUserAlias = null;
            SystemMessage systemMessageAlias = null;

            IQueryOver<SystemMessageVisitPayUser, SystemMessageVisitPayUser> query = this.Session.QueryOver(() => systemMessageVisitPayUserAlias)
                .JoinAlias(() => systemMessageVisitPayUserAlias.SystemMessage, () => systemMessageAlias)
                .Where(smvu => smvu.VisitPayUserId == visitPayUserId)
                .Where(smvu => smvu.SystemMessageId == systemMessageId)
                .Where(smvu => systemMessageAlias.DateActive != null && systemMessageAlias.DateActive < DateTime.UtcNow)
                .Where(smvu => systemMessageAlias.DateExpires == null || systemMessageAlias.DateExpires > DateTime.UtcNow);

            return query.Take(1).SingleOrDefault<SystemMessageVisitPayUser>();
        }

        public bool SystemMessageIsEnabled(int systemMessageId)
        {
            SystemMessage systemMessageAlias = null;

            IQueryOver<SystemMessage, SystemMessage> query = this.Session.QueryOver(() => systemMessageAlias)
                .Where(sm => sm.SystemMessageId == systemMessageId)
                .Where(sm => sm.DateActive != null && sm.DateActive < DateTime.UtcNow)
                .Where(sm => sm.DateExpires == null || sm.DateExpires > DateTime.UtcNow);

            return query.RowCount() > 0;
        }
    }
}
﻿namespace Ivh.Provider.Core.TreatmentLocation
{
    using Common.Data;
    using Common.Data.Connection.Connections;
    using Common.Data.Interfaces;
    using Domain.Core.TreatmentLocation.Entities;
    using Domain.Core.TreatmentLocation.Interfaces;
    using NHibernate;

    public class TreatmentLocationRepository : RepositoryBase<TreatmentLocation, VisitPay>, ITreatmentLocationRepository
    {
        public TreatmentLocationRepository(ISessionContext<VisitPay> sessionContext)
            : base(sessionContext)
        {
        }
    }
}

﻿namespace Ivh.Provider.Core.ThirdPartyKey
{
    using Ivh.Common.Data;
    using Ivh.Common.Data.Connection.Connections;
    using Ivh.Common.Data.Interfaces;
    using Ivh.Domain.Core.ThirdPartyKey.Entities;
    using Ivh.Domain.Core.ThirdPartyKey.Interfaces;
    using System.Linq;

    public class ThirdPartyValueRepository : RepositoryBase<ThirdPartyValue, VisitPay>, IThirdPartyValueRepository
    {
        public ThirdPartyValueRepository(ISessionContext<VisitPay> sessionContext) : base(sessionContext)
        {
        }
    }
}

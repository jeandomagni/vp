﻿namespace Ivh.Provider.Core.ThirdPartyKey
{
    using Domain.Core.ThirdPartyKey.Entities;
    using Domain.Core.ThirdPartyKey.Interfaces;
    using Ivh.Common.Data;
    using Ivh.Common.Data.Connection.Connections;
    using Ivh.Common.Data.Interfaces;
    using Ivh.Common.VisitPay.Enums;
    using NHibernate.Linq;
    using System.Linq;
    using System.Threading.Tasks;

    public class ThirdPartyKeyRepository : RepositoryBase<ThirdPartyKey, VisitPay>, IThirdPartyKeyRepository
    {
        public ThirdPartyKeyRepository(IStatelessSessionContext<VisitPay> sessionContext) : base(sessionContext)
        {
        }

        public IQueryable<ThirdPartyKey> GetAllStateless()
        {
            return this.StatelessSession.Query<ThirdPartyKey>();
        }
    }
}

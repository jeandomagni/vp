﻿namespace Ivh.Provider.Core.LegacyUser
{
    using System;
    using Common.Data;
    using Common.Data.Connection.Connections;
    using Common.Data.Interfaces;
    using Domain.Core.LegacyUser.Entities;
    using Domain.Core.LegacyUser.Interfaces;
    using NHibernate;

    public class LegacyUserRepository : RepositoryBase<LegacyUser, VisitPay>, ILegacyUserRepository
    {
        public LegacyUserRepository(ISessionContext<VisitPay> sessionContext) : base(sessionContext)
        {
        }

        public bool IsNonMigratedLegacyUser(string username)
        {
            int count = this.Session.QueryOver<LegacyUser>()
                .Where(x => x.IsMigrated == false && x.UserName == username)
                .RowCount();

            return count > 0;
        }

        public override void Delete(LegacyUser entity)
        {
            throw new NotImplementedException();
        }

        public override void Update(LegacyUser entity)
        {
            throw new NotImplementedException();
        }

        public override void Insert(LegacyUser entity)
        {
            throw new NotImplementedException();
        }

        public override void InsertOrUpdate(LegacyUser entity)
        {
            throw new NotImplementedException();
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ivh.Provider.Core.VanityUrl
{
    using Common.Data;
    using Common.Data.Connection.Connections;
    using Common.Data.Interfaces;
    using Domain.Core.VanityUrl.Entities;
    using Domain.Core.VanityUrl.Interfaces;

    public class VanityUrlRepository : RepositoryBase<VanityUrl, VisitPay>, IVanityUrlRepository
    {
        public VanityUrlRepository(ISessionContext<VisitPay> sessionContext) : base(sessionContext)
        {
        }

        public IList<VanityUrl> GetVanityUrls()
        {
            return this.Session.QueryOver<VanityUrl>().List();
        }
    }
}

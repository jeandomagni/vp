﻿namespace Ivh.Provider.Core.IpAccess
{
    using Common.Data;
    using Common.Data.Connection.Connections;
    using Common.Data.Interfaces;
    using Domain.Core.IpAccess.Entities;
    using Domain.Core.IpAccess.Interfaces;
    using NHibernate;

    public class IpAccessRepository : RepositoryBase<IpAccess, VisitPay>, IIpAccessRepository
    {
        public IpAccessRepository(ISessionContext<VisitPay> newSession) : base(newSession)
        {
        }
    }
}
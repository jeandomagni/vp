﻿namespace Ivh.Provider.Core.SystemException
{
    using Common.Data;
    using Common.Data.Connection.Connections;
    using Common.Data.Interfaces;
    using Domain.Core.SystemException.Entities;
    using Domain.Core.SystemException.Interfaces;
    using NHibernate;

    public class SystemExceptionRepository : RepositoryBase<SystemException, VisitPay>, ISystemExceptionRepository
    {
        public SystemExceptionRepository(ISessionContext<VisitPay> sessionContext) : base(sessionContext)
        {
        }
    }
}
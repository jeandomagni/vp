﻿namespace Ivh.Provider.Core.Unmatching
{
    using System;
    using Common.Base.Enums;
    using Common.Data;
    using Common.Data.Connection.Connections;
    using Common.Data.Interfaces;
    using Common.VisitPay.Enums;
    using Domain.Visit.Unmatching.Entities;
    using Domain.Visit.Unmatching.Interfaces;
    using NHibernate;

    public class HsGuarantorUnmatchReasonRepository : RepositoryBase<HsGuarantorUnmatchReason, VisitPay>, IHsGuarantorUnmatchReasonRepository, IDisposable
    {
        public HsGuarantorUnmatchReasonRepository(ISessionContext<VisitPay> sessionContext)
            : base(sessionContext)
        {
        }

        public void Dispose()
        {
            base.CallBackAfterCommit = null;
        }

        public HsGuarantorUnmatchReason GetById(HsGuarantorUnmatchReasonEnum hsGuarantorUnmatchReasonEnum)
        {
            return this.GetById((int)hsGuarantorUnmatchReasonEnum);
        }
    }
}
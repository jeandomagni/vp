﻿namespace Ivh.Provider.Core.Unmatching
{
    using System;
    using System.Collections.Generic;
    using Common.Base.Enums;
    using Common.Data;
    using Common.Data.Connection.Connections;
    using Common.Data.Interfaces;
    using Common.VisitPay.Enums;
    using Domain.Guarantor.Entities;
    using Domain.User.Entities;
    using Domain.Visit.Unmatching.Entities;
    using Domain.Visit.Unmatching.Interfaces;
    using NHibernate;
    using NHibernate.Criterion;
    using NHibernate.SqlCommand;

    public class HsGuarantorMatchDiscrepancyRepository : RepositoryBase<HsGuarantorMatchDiscrepancy, VisitPay>, IHsGuarantorMatchDiscrepancyRepository, IDisposable
    {
        public HsGuarantorMatchDiscrepancyRepository(ISessionContext<VisitPay> sessionContext)
            : base(sessionContext)
        {
        }

        public void Dispose()
        {
            this.CallBackAfterCommit = null;
        }

        public IReadOnlyList<HsGuarantorMatchDiscrepancyStatus> GetAllHsGuarantorMatchDiscrepancyStatuses()
        {
            return (IReadOnlyList<HsGuarantorMatchDiscrepancyStatus>) this.Session.QueryOver<HsGuarantorMatchDiscrepancyStatus>().List();
        }

        public HsGuarantorMatchDiscrepancyResults GetAllHsGuarantorMatchDiscrepancies(HsGuarantorMatchDiscrepancyFilter filter, bool attributeChangeOnly, int pageNumber, int pageSize)
        {
            HsGuarantorMatchDiscrepancyResults results = this.GetAllHsGuarantorMatchDiscrepancyTotals(filter, attributeChangeOnly);

            results.HsGuarantorMatchDiscrepancies = this.DoGetAllHsGuarantorMatchDiscrepancies(filter, attributeChangeOnly, pageNumber, pageSize);

            return results;
        }

        private HsGuarantorMatchDiscrepancyResults GetAllHsGuarantorMatchDiscrepancyTotals(HsGuarantorMatchDiscrepancyFilter filter, bool attributeChangeOnly)
        {
            int totalRecords = this.QueryOverDiscrepancies(filter, attributeChangeOnly)
                .RowCount();

            return new HsGuarantorMatchDiscrepancyResults
            {
                TotalRecords = totalRecords
            };
        }

        private IReadOnlyList<HsGuarantorMatchDiscrepancy> DoGetAllHsGuarantorMatchDiscrepancies(HsGuarantorMatchDiscrepancyFilter filter, bool attributeChangeOnly, int pageNumber, int pageSize)
        {
            IQueryOver<HsGuarantorMatchDiscrepancy, HsGuarantorMatchDiscrepancy> query = this.QueryOverDiscrepancies(filter, attributeChangeOnly);
            Sort(filter, query);

            IList<HsGuarantorMatchDiscrepancy> discrepancies = query
                .Skip(pageNumber*pageSize)
                .Take(pageSize)
                .List();

            return (IReadOnlyList<HsGuarantorMatchDiscrepancy>) discrepancies;
        }

        private IQueryOver<HsGuarantorMatchDiscrepancy, HsGuarantorMatchDiscrepancy> QueryOverDiscrepancies(HsGuarantorMatchDiscrepancyFilter filter, bool attributeChangeOnly)
        {
            HsGuarantorMatchDiscrepancy discrepancyAlias = null;
            HsGuarantorMatchDiscrepancyStatus statusAlias = null;
            HsGuarantorMap hsGuarantorAlias = null;
            Guarantor hsGuarantorVpGuarantorAlias = null;
            Guarantor vpGuarantorAlias = null;
            VisitPayUser visitPayUserAlias = null;
            VisitPayUser actionVisitPayUserAlias = null;
            HsGuarantorUnmatchReason unmatchReasonAlias = null;

            IQueryOver<HsGuarantorMatchDiscrepancy, HsGuarantorMatchDiscrepancy> query = this.Session.QueryOver(() => discrepancyAlias)
                .JoinAlias(sts => sts.HsGuarantorMatchDiscrepancyStatusSource, () => statusAlias)
                .JoinAlias(hsg => hsg.HsGuarantorMap, () => hsGuarantorAlias)
                .JoinAlias(hsgvpg => hsGuarantorAlias.VpGuarantor, () => hsGuarantorVpGuarantorAlias)
                .JoinAlias(vpg => discrepancyAlias.VpGuarantor, () => vpGuarantorAlias)
                .JoinAlias(vpu => vpGuarantorAlias.User, () => visitPayUserAlias)
                .JoinAlias(umr => discrepancyAlias.HsGuarantorUnmatchReason, () => unmatchReasonAlias, JoinType.LeftOuterJoin)
                .JoinAlias(avp => discrepancyAlias.ActionVisitPayUser, () => actionVisitPayUserAlias, JoinType.LeftOuterJoin);
                
            Filter(filter, attributeChangeOnly, query);

            return query;
        }

        private static void Filter(HsGuarantorMatchDiscrepancyFilter filter, bool attributeChangeOnly, IQueryOver<HsGuarantorMatchDiscrepancy, HsGuarantorMatchDiscrepancy> query)
        {
            HsGuarantorMatchDiscrepancy discrepancyAlias = null;
            HsGuarantorMap hsGuarantorAlias = null;
            HsGuarantorUnmatchReason unmatchReasonAlias = null;

            if (attributeChangeOnly)
            {
                List<int> reasonIds = new List<int>
                {
                    (int)HsGuarantorUnmatchReasonEnum.ClientUnmatchedMatchCriteriaChanged,
                    (int)HsGuarantorUnmatchReasonEnum.ClientGuarantorMatchingCriteriaValidated
                };
                query = query.Where(Restrictions.Or(Restrictions.IsNull(Projections.Property(() => discrepancyAlias.HsGuarantorUnmatchReason)), Restrictions.In(Projections.Property(() => unmatchReasonAlias.HsGuarantorUnmatchReasonId), reasonIds)));
            }
            
            query.If(filter.VpGuarantorId.HasValue, q => q.Where(() => discrepancyAlias.VpGuarantor.VpGuarantorId == filter.VpGuarantorId));
            query.If(!string.IsNullOrEmpty(filter.HsGuarantorSourceSystemKey) && !string.IsNullOrWhiteSpace(filter.HsGuarantorSourceSystemKey), q => q.Where(() => hsGuarantorAlias.SourceSystemKey == filter.HsGuarantorSourceSystemKey));
            query.If(filter.HsGuarantorMatchDiscrepancyStatus.HasValue, q => q.Where(() => discrepancyAlias.HsGuarantorMatchDiscrepancyStatus == filter.HsGuarantorMatchDiscrepancyStatus));
            query.If(filter.DateRangeFrom.HasValue, q =>
            {
                DateTime date = DateTime.UtcNow.AddDays(filter.DateRangeFrom.GetValueOrDefault(-30)).Date;
                query.Where(x => x.InsertDate >= date);
            });
            query.If(filter.SpecificDateFrom.HasValue, q =>
            {
                DateTime date = filter.SpecificDateFrom.GetValueOrDefault(DateTime.UtcNow).AddDays(-1).Date;
                query.Where(Restrictions.Gt(Projections.Cast(NHibernateUtil.Date, Projections.Property(() => discrepancyAlias.InsertDate)), date));
            });
            query.If(filter.SpecificDateTo.HasValue, q =>
            {
                DateTime date = filter.SpecificDateTo.GetValueOrDefault(DateTime.UtcNow).AddDays(1).Date;
                query.Where(Restrictions.Lt(Projections.Cast(NHibernateUtil.Date, Projections.Property(() => discrepancyAlias.InsertDate)), date));
            });
        }

        private static void Sort(HsGuarantorMatchDiscrepancyFilter filter, IQueryOver query)
        {
            if (string.IsNullOrEmpty(filter.SortField))
                return;

            HsGuarantorMatchDiscrepancy discrepancyAlias = null;
            HsGuarantorMatchDiscrepancyStatus statusAlias = null;
            HsGuarantorMap hsGuarantorAlias = null;
            Guarantor hsGuarantorVpGuarantorAlias = null;
            VisitPayUser visitPayUserAlias = null;
            VisitPayUser actionVisitPayUserAlias = null;
            HsGuarantorUnmatchReason unmatchReasonAlias = null;

            bool isAscendingOrder = "asc".Equals(filter.SortOrder, StringComparison.InvariantCultureIgnoreCase);

            switch (filter.SortField)
            {
                case "ActionDate":
                    query.UnderlyingCriteria.AddOrder(new Order(Projections.Property(() => discrepancyAlias.ActionDate), isAscendingOrder));
                    break;
                case "ActionUserName":
                    query.UnderlyingCriteria.AddOrder(new Order(Projections.Property(() => actionVisitPayUserAlias.UserName), isAscendingOrder));
                    break;
                case "HsGuarantorMatchDiscrepancyStatusName":
                    query.UnderlyingCriteria.AddOrder(new Order(Projections.Property(() => statusAlias.HsGuarantorMatchDiscrepancyStatusName), isAscendingOrder));
                    query.UnderlyingCriteria.AddOrder(new Order(Projections.Cast(NHibernateUtil.Date, Projections.Property(() => discrepancyAlias.InsertDate)), isAscendingOrder));
                    query.UnderlyingCriteria.AddOrder(new Order(Projections.Property(() => visitPayUserAlias.LastName), isAscendingOrder));
                    query.UnderlyingCriteria.AddOrder(new Order(Projections.Property(() => visitPayUserAlias.FirstName), isAscendingOrder));
                    break;
                case "InsertDate":
                    query.UnderlyingCriteria.AddOrder(new Order(Projections.Property(() => discrepancyAlias.InsertDate), isAscendingOrder));
                    query.UnderlyingCriteria.AddOrder(new Order(Projections.Property(() => discrepancyAlias.ActionDate), isAscendingOrder));
                    query.UnderlyingCriteria.AddOrder(new Order(Projections.Property(() => hsGuarantorAlias.SourceSystemKey), isAscendingOrder));
                    break;
                case "HsGuarantorSourceSystemKey":
                case "HsGuarantorSourceSystemKeyCurrent":
                    query.UnderlyingCriteria.AddOrder(new Order(Projections.Property(() => hsGuarantorAlias.SourceSystemKey), isAscendingOrder));
                    break;
                case "HsGuarantorUnmatchReasonDisplayName":
                    query.UnderlyingCriteria.AddOrder(new Order(Projections.Property(() => unmatchReasonAlias.HsGuarantorUnmatchReasonDisplayName), isAscendingOrder));
                    break;
                case "VpGuarantorFullName":
                    query.UnderlyingCriteria.AddOrder(new Order(Projections.Property(() => visitPayUserAlias.LastName), isAscendingOrder));
                    query.UnderlyingCriteria.AddOrder(new Order(Projections.Property(() => visitPayUserAlias.FirstName), isAscendingOrder));
                    break;
                case "VpGuarantorId":
                    query.UnderlyingCriteria.AddOrder(new Order(Projections.Property(() => discrepancyAlias.VpGuarantor.VpGuarantorId), isAscendingOrder));
                    break;
                case "VpGuarantorIdCurrent":
                    query.UnderlyingCriteria.AddOrder(new Order(Projections.Conditional(Restrictions.Eq(Projections.Property(() => statusAlias.HsGuarantorMatchDiscrepancyStatusId), (int)HsGuarantorMatchDiscrepancyStatusEnum.ValidMatch),
                        Projections.Property(() => hsGuarantorVpGuarantorAlias.VpGuarantorId),
                        Projections.Constant(-1)),
                        isAscendingOrder));
                    break;
            }
        }


        public IList<HsGuarantorMatchDiscrepancy> GetHsGuarantorMatchDiscrepancies(int hsGuarantorId, int vpGuarantorId)
        {
            HsGuarantorMatchDiscrepancy discrepancyAlias = null;
            HsGuarantorMap hsGuarantorAlias = null;

            IQueryOver<HsGuarantorMatchDiscrepancy, HsGuarantorMatchDiscrepancy> queryOver = this.Session.QueryOver(() => discrepancyAlias)
                .JoinAlias(hsg => hsg.HsGuarantorMap, () => hsGuarantorAlias);

            queryOver.Where(x => hsGuarantorAlias.HsGuarantorId == hsGuarantorId && discrepancyAlias.VpGuarantor.VpGuarantorId == vpGuarantorId);
            return queryOver.List<HsGuarantorMatchDiscrepancy>();
        }

        public HsGuarantorMatchDiscrepancy GetHsGuarantorMatchDiscrepancy(int hsGuarantorId, int vpGuarantorId, int? hsGuarantorMatchDiscrepancyId = null, HsGuarantorMatchDiscrepancyStatusEnum? status = null)
        {
            HsGuarantorMatchDiscrepancy discrepancyAlias = null;
            HsGuarantorMap hsGuarantorAlias = null;

            IQueryOver<HsGuarantorMatchDiscrepancy, HsGuarantorMatchDiscrepancy> queryOver = this.Session.QueryOver(() => discrepancyAlias)
                .JoinAlias(hsg => hsg.HsGuarantorMap, () => hsGuarantorAlias);

            queryOver.Where(x => hsGuarantorAlias.HsGuarantorId == hsGuarantorId && discrepancyAlias.VpGuarantor.VpGuarantorId == vpGuarantorId);

            if (hsGuarantorMatchDiscrepancyId.HasValue)
            {
                queryOver.Where(x => x.HsGuarantorMatchDiscrepancyId == hsGuarantorMatchDiscrepancyId);
            }
            
            if (status.HasValue)
            {
                queryOver.Where(x => x.HsGuarantorMatchDiscrepancyStatus == status);
            }

            return queryOver.Take(1).SingleOrDefault();
        }
    }
}
﻿namespace Ivh.Provider.Core.Alert
{
    using System.Collections.Generic;
    using Common.Data;
    using Common.Data.Connection.Connections;
    using Common.Data.Interfaces;
    using Domain.Core.Alert.Entities;
    using Domain.Core.Alert.Interfaces;
    using NHibernate;

    public class AlertRepository : RepositoryBase<Alert, VisitPay>, IAlertRepository
    {
        public AlertRepository(ISessionContext<VisitPay> sessionContext) : base(sessionContext)
        {
        }

        public IList<Alert> GetActiveAlerts()
        {
            IList<Alert> activeAlerts = this.Session
                .QueryOver<Alert>()
                .Where(x => x.IsActive)
                .Cacheable()
                .List();

            return activeAlerts;
        }
    }
}
﻿namespace Ivh.Provider.Core.HsResolutionProviders.Client.Intermountain
{
    using System;
    using Domain.Visit.Interfaces;

    public class HsBillingSystemResolutionProvider : IHsBillingSystemResolutionProvider
    {
        public int ResolveBillingSystemId(string visitSourceSystemKey)
        {
            string[] parts = visitSourceSystemKey.Split('_');
            if (parts.Length > 0)
            {
                switch (parts[0].ToLower())
                {
                    case "fac":
                        return 2;
					case "cpa":
					case "cpm":
                        return 3;
                }
            }
			throw new NotImplementedException();
        }
    }
}
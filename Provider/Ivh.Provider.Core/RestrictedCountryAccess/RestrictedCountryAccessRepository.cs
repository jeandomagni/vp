﻿namespace Ivh.Provider.Core.RestrictedCountryAccess
{
    using Common.Data;
    using Common.Data.Connection.Connections;
    using Common.Data.Interfaces;
    using Domain.Core.RestrictedCountryAccess.Entities;
    using Domain.Core.RestrictedCountryAccess.Interfaces;
    using NHibernate;

    public class RestrictedCountryAccessRepository : RepositoryBase<RestrictedCountryAccess, VisitPay>, IRestrictedCountryAccessRepository
    {
        public RestrictedCountryAccessRepository(ISessionContext<VisitPay> sessionContext) : base(sessionContext)
        {
        }
    }
}
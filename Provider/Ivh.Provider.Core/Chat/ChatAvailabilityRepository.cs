﻿
namespace Ivh.Provider.Core.Chat
{
    using Common.Data;
    using Common.Data.Connection.Connections;
    using Common.Data.Interfaces;
    using Domain.Core.Chat.Entities;
    using Domain.Core.Chat.Interfaces;

    public class ChatAvailabilityRepository : RepositoryBase<ChatAvailability, VisitPay>, IChatAvailabilityRepository
    {
        public ChatAvailabilityRepository(ISessionContext<VisitPay> sessionContext) : base(sessionContext)
        {
        }
    }
}

﻿namespace Ivh.Provider.Core.Modules
{
    using SystemException;
    using Audit;
    using Autofac;
    using Autofac.Core;
    using Domain.Core.Audit.Interfaces;
    using Domain.Core.IpAccess.Interfaces;
    using Domain.Core.MergeException.Interfaces;
    using Domain.Core.MergeException.Services;
    using Domain.Core.SystemException.Interfaces;
    using Domain.Core.SystemException.Services;
    using Domain.Core.SystemMessage.Interfaces;
    using Domain.Core.TreatmentLocation.Interfaces;
    using Domain.Email.Interfaces;
    using Email;
    using IpAccess;
    using MergeException;
    using NHibernate;
    using SystemMessage;
    using Alert;
    using Chat;
    using Common.Data.Connection;
    using Common.Data.Connection.Connections;
    using Common.Data.Interfaces;
    using Domain.Core.Alert.Interfaces;
    using Domain.Core.Chat.Interfaces;
    using Domain.Core.KnowledgeBase.Interfaces;
    using Domain.Core.Malware.Interfaces;
    using Domain.Core.VanityUrl.Interfaces;
    using Domain.Core.VisitPayDocument.Interfaces;
    using KnowledgeBase;
    using TreatmentLocation;
    using VisitPayDocument;
    using Ivh.Provider.Core.ThirdPartyKey;
    using Ivh.Domain.Core.ThirdPartyKey.Interfaces;
    using Malware;
    using VanityUrl;

    public class Module : Autofac.Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            //Giving this it's own session so that it's not dependent on requests
            //See notes in the Attribute: IpAccessCheckAttribute
            ResolvedParameter newSession = new ResolvedParameter((pi, ctx) =>
            {
                return pi.ParameterType == typeof(ISessionContext<VisitPay>) && pi.Name == "newSession";
            }, (pi, ctx) =>
            {
                return new SessionContext<VisitPay>(ctx.ResolveNamed<ISessionFactory>(VisitPay.Instance.FactoryName).OpenSession());
            });
            builder.RegisterType<IpAccessRepository>().As<IIpAccessRepository>().WithParameters(new[] { newSession });
            
            builder.RegisterType<CommunicationEventRepository>().As<ICommunicationEventRepository>();
            builder.RegisterType<CommunicationRepository>().As<ICommunicationRepository>();
            builder.RegisterType<CommunicationDocumentRepository>().As<ICommunicationDocumentRepository>();
            builder.RegisterType<CommunicationSmsBlockedNumberRepository>().As<ICommunicationSmsBlockedNumberRepository>();
            builder.RegisterType<CommunicationTrackerRepository>().As<ICommunicationTrackerRepository>();
            builder.RegisterType<CommunicationTypeRepository>().As<ICommunicationTypeRepository>();
            builder.RegisterType<MergeExceptionService>().As<IMergeExceptionService>();
            builder.RegisterType<MergeExceptionRepository>().As<IMergeExceptionRepository>();

            builder.RegisterType<SystemExceptionRepository>().As<ISystemExceptionRepository>();
            builder.RegisterType<SystemExceptionService>().As<ISystemExceptionService>();
            builder.RegisterType<SystemMessageVisitPayUserRepository>().As<ISystemMessageVisitPayUserRepository>();
            builder.RegisterType<TreatmentLocationRepository>().As<ITreatmentLocationRepository>();
            builder.RegisterType<AuditRepository>().As<IAuditRepository>();
            builder.RegisterType<CommunicationPreferenceRepository>().As<ICommunicationPreferenceRepository>();
            builder.RegisterType<CommunicationGroupRepository>().As<ICommunicationGroupRepository>();
            builder.RegisterType<AlertRepository>().As<IAlertRepository>();

            builder.RegisterType<KnowledgeBaseCategoryRepository>().As<IKnowledgeBaseCategoryRepository>();
            builder.RegisterType<KnowledgeBaseQuestionAnswerVoteRepository>().As<IKnowledgeBaseQuestionAnswerVoteRepository>();
            builder.RegisterType<KnowledgeBaseQuestionAnswerFeedbackRepository>().As<IKnowledgeBaseQuestionAnswerFeedbackRepository>();
            builder.RegisterType<KnowledgeBaseQuestionAnswerRepository>().As<IKnowledgeBaseQuestionAnswerRepository>();

            builder.RegisterType<AuditEventFinancePlanRepository>().As<IAuditEventFinancePlanRepository>();
            builder.RegisterType<SearchTextLogRepository>().As<ISearchTextLogRepository>();

            builder.RegisterType<VisitPayDocumentRepository>().As<IVisitPayDocumentRepository>();

            builder.RegisterType<ChatAvailabilityRepository>().As<IChatAvailabilityRepository>();

            builder.RegisterType<ThirdPartyKeyRepository>().As<IThirdPartyKeyRepository>();
            builder.RegisterType<ThirdPartyValueRepository>().As<IThirdPartyValueRepository>();

            builder.RegisterType<MalwareScanningProvider>().As<IMalwareScanningProvider>();

            builder.RegisterType<VanityUrlRepository>().As<IVanityUrlRepository>();
        }
    }
}
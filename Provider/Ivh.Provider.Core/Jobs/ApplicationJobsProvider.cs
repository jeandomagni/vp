﻿namespace Ivh.Provider.Core.Jobs
{
    using System;
    using Application.Core.Common.Interfaces;
    using Common.Data.Connection.Connections;
    using Common.Data.Interfaces;
    using NHibernate;

    public class ApplicationJobsProvider : IApplicationJobsProvider
    {
        private readonly ISession _session;
        public ApplicationJobsProvider(ISessionContext<VisitPay> session)
        {
            this._session = session.Session;
        }
    }
}

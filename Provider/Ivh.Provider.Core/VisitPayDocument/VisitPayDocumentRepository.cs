﻿namespace Ivh.Provider.Core.VisitPayDocument
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Application.Core.Common.Dtos;
    using Common.Data;
    using Common.Data.Connection.Connections;
    using Common.Data.Interfaces;
    using Domain.Core.VisitPayDocument.Entities;
    using Domain.Core.VisitPayDocument.Interfaces;
    using NHibernate;
    using NHibernate.Criterion;
    using NHibernate.Transform;

    public class VisitPayDocumentRepository : RepositoryBase<VisitPayDocument, VisitPay>, IVisitPayDocumentRepository
    {
        public VisitPayDocumentRepository(ISessionContext<VisitPay> sessionContext) : base(sessionContext)
        {
        }

        public IList<VisitPayDocumentResult> GetVisitPayDocumentResults(VisitPayDocumentFilter visitPayDocumentFilter)
        {
            IQueryOver<VisitPayDocumentResult, VisitPayDocumentResult> query = this.Session.QueryOver<VisitPayDocumentResult>();

            if (visitPayDocumentFilter?.VisitPayDocumentId != null)
            {
                query.Where(x => x.VisitPayDocumentId == visitPayDocumentFilter.VisitPayDocumentId);
            }

            if (visitPayDocumentFilter!=null && visitPayDocumentFilter.IsApproved)
            {
                query.Where(x => x.ApprovedByVpUserId > 0);
                query.Where(a => a.ActiveDate <= DateTime.UtcNow.Date);
            }

            if (visitPayDocumentFilter.CanSort)
            {
                query.UnderlyingCriteria.AddOrder(new Order(visitPayDocumentFilter.SortField, visitPayDocumentFilter.SortOrder=="asc"));
            }

            if (visitPayDocumentFilter.CanPage)
            {
                query.Skip(visitPayDocumentFilter.Page - 1 * visitPayDocumentFilter.Rows).Take(visitPayDocumentFilter.Rows);
            }

            return query.List();;
        }
    }
}
namespace Ivh.Provider.Core.Sso
{
    using System.Collections.Generic;
    using Common.Base.Enums;
    using Common.Data;
    using Common.Data.Connection.Connections;
    using Common.Data.Interfaces;
    using Common.VisitPay.Enums;
    using Domain.Core.Sso.Entities;
    using Domain.Core.Sso.Interfaces;
    using NHibernate;
    using NHibernate.Criterion;


    public class SsoVisitPayUserOauthTokenRepository : RepositoryBase<SsoVisitPayUserOauthToken, VisitPay>, ISsoVisitPayUserOauthTokenRepository
    {
        public SsoVisitPayUserOauthTokenRepository(ISessionContext<VisitPay> sessionContext)
            : base(sessionContext)
        {
        }

        public IList<SsoVisitPayUserOauthToken> GetAllTokensForUser(int visitPayUserId, OAuthTokenTypeEnum type, bool? active = null)
        {
            ICriteria criteria = this.Session.CreateCriteria(typeof(SsoVisitPayUserOauthToken));
            criteria.Add(Restrictions.Eq("VisitPayUserId", visitPayUserId));
            criteria.Add(Restrictions.Eq("TokenType", type));
            if (active.HasValue)
            {
                criteria.Add(Restrictions.Eq("IsActive", active));
            }
            var all =  criteria.List<SsoVisitPayUserOauthToken>();
            return all;
        }

        public SsoVisitPayUserOauthToken GetByTokenString(string tokenString, OAuthTokenTypeEnum type)
        {
            if (string.IsNullOrEmpty(tokenString)) return null;
            ICriteria criteria = this.Session.CreateCriteria(typeof(SsoVisitPayUserOauthToken));
            criteria.Add(Restrictions.Eq("Token", tokenString));
            criteria.Add(Restrictions.Eq("TokenType", type));
            return criteria.UniqueResult<SsoVisitPayUserOauthToken>();
        }
    }
}
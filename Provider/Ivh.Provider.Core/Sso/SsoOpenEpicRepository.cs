﻿namespace Ivh.Provider.Core.Sso
{
    using System;
    using System.Collections.Generic;
    using Application.Core.Common.Dtos;
    using Common.Data;
    using Common.Data.Connection;
    using Common.Data.Connection.Connections;
    using Common.Data.Interfaces;
    using Domain.Core.Sso.Entities;
    using Domain.Core.Sso.Interfaces;
    using NHibernate;
    using NHibernate.Transform;

    public class SsoOpenEpicRepository : ISsoSpecificRespository
    {
        private readonly ISession _hospitalDataSession;

        public SsoOpenEpicRepository(ISessionContext<CdiEtl> hospitalDataSession)
        {
            this._hospitalDataSession = hospitalDataSession.Session;
        }

        public IList<SsoPossibleVpGuarantorResult> HasPossibleVisitPayGuarantors(string ssoSourceSystemKey, DateTime dateOfBirth)
        {
            //This should probably return a list of guarantors??

            IQuery query = this._hospitalDataSession.CreateSQLQuery(
                "EXEC Vp.SingleSignOn_VpGuarantorMatches @SsoSourceSystemKey =:SsoSsk,@DOB =:Dob");

            query.SetParameter("SsoSsk", ssoSourceSystemKey);
            query.SetParameter("Dob", dateOfBirth);

            query.SetReadOnly(true);
            query.SetResultTransformer(new AliasToBeanResultTransformer(typeof(SsoPossibleVpGuarantorResult)));

            return (IList<SsoPossibleVpGuarantorResult>)query.List<SsoPossibleVpGuarantorResult>();
        }

        public IList<SsoPossibleHsGuarantorResult> HasPossibleHsGuarantorsToRegister(string ssoSourceSystemKey, DateTime dateOfBirth)
        {
            //This also should probably return a list of guarantors?
            IQuery query = this._hospitalDataSession.CreateSQLQuery(
                "EXEC Vp.SingleSignOn_RegistrableMatches @SsoSourceSystemKey =:SsoSsk,@DOB =:Dob");

            query.SetParameter("SsoSsk", ssoSourceSystemKey);
            query.SetParameter("Dob", dateOfBirth);

            query.SetReadOnly(true);
            query.SetResultTransformer(new AliasToBeanResultTransformer(typeof(SsoPossibleHsGuarantorResult)));

            return (IList<SsoPossibleHsGuarantorResult>)query.List<SsoPossibleHsGuarantorResult>();
        }

        public IList<SsoVerificationResult> RegisterVpGuarantor(int vpGuarantorId, string ssoSourceSystemKey, DateTime dateOfBirth)
        {
            //This also should probably return a list of guarantors?
            IQuery query = this._hospitalDataSession.CreateSQLQuery(
                "EXEC Vp.SingleSignOn_addVpGuarantorMatch @SsoSourceSystemKey =:SsoSsk,@DOB =:Dob, @VpGuarantorId =:VpGuarantorId");

            query.SetParameter("SsoSsk", ssoSourceSystemKey);
            query.SetParameter("Dob", dateOfBirth);
            query.SetParameter("VpGuarantorId", vpGuarantorId);

            query.SetReadOnly(true);
            query.SetResultTransformer(new AliasToBeanResultTransformer(typeof(SsoVerificationResult)));

            return (IList<SsoVerificationResult>)query.List<SsoVerificationResult>();
        }

        public IList<SsoVerification> GetVerificationsByVpGuarantor(int vpGuarantorId)
        {
            IQueryOver<SsoVerification> query = this._hospitalDataSession.QueryOver<SsoVerification>()
                .Where(x => x.VpGuarantorId == vpGuarantorId);

            return query.List();
        }

        public void RemoveAllVerificationsForVpGuarantor(int vpGuarantorId)
        {
            IQuery query = this._hospitalDataSession.CreateSQLQuery(
                "EXEC Vp.SingleSignOn_RemoveVpGuarantorMatchs @VpGuarantorId =:VpGuarantorId");

            query.SetParameter("VpGuarantorId", vpGuarantorId);
            query.ExecuteUpdate();
        }

        public bool IsVpGuarantorEligible(int vpGuarantorId)
        {
            IQuery query = this._hospitalDataSession.CreateSQLQuery(
                "EXEC Vp.SingleSignOn_IsVpGuarantorEligible @VpGuarantorId =:VpGuarantorId");

            query.SetParameter("VpGuarantorId", vpGuarantorId);
            query.SetReadOnly(true);

            int count = query.UniqueResult<int>();
            return count > 0;
        }
    }
}

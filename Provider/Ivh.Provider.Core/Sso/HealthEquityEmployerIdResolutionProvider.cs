﻿
namespace Ivh.Provider.Core.Sso
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Common.Base.Utilities.Extensions;
    using Domain.Core.Sso.Interfaces;
    using Domain.Visit.Entities;
    using Domain.Visit.Interfaces;
    using Domain.FinanceManagement.Interfaces;
    using Domain.Settings.Interfaces;

    public class HealthEquityEmployerIdResolutionProvider : IHealthEquityEmployerIdResolutionProvider
    {
        private readonly Lazy<IClientService> _clientService;
        private readonly Lazy<IVisitService> _visitService;

        public HealthEquityEmployerIdResolutionProvider(
            Lazy<IClientService> clientService,
            Lazy<IVisitService> visitService)
        {
            this._clientService = clientService;
            this._visitService = visitService;
        }

        public string GeHealthEquityEmployerId(int guarantorId)
        {
            IDictionary<string, string> employerIdMap = this._clientService.Value.GetClient().HealthEquitySsoEmployerIdMap;
            if (employerIdMap.Count > 0)
            {
                IReadOnlyList<Visit> visits = this._visitService.Value.GetVisits(guarantorId).ToList();
                visits = visits.Where(x => x.VisitInsurancePlans.Any(y => y.InsurancePlan != null && y.InsurancePlan.SourceSystemKey.IsNotNullOrEmpty())).OrderByDescending(x => x.InsertDate).ToList();

                return visits.OrderByDescending(x => x.DischargeDate ?? x.AdmitDate ?? x.InsertDate)
                    .SelectMany(x => x.VisitInsurancePlans)
                    .Join(employerIdMap, o => o.InsurancePlan.SourceSystemKey, i => i.Key, (o, i) => i.Value)
                    .FirstOrDefault();
            }
            return "";
        }
    }
}

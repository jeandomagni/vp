﻿namespace Ivh.Provider.Core.Sso
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Common.Base.Utilities.Extensions;
    using Common.Data;
    using Common.Data.Connection.Connections;
    using Common.Data.Interfaces;
    using Domain.Core.Sso.Entities;
    using Domain.Core.Sso.Interfaces;
    using NHibernate;
    using NHibernate.Criterion;
    using NHibernate.Linq;

    public class SsoProviderRepository : RepositoryBase<SsoProvider, VisitPay>, ISsoProviderRepository, IDisposable
    {
        public SsoProviderRepository(ISessionContext<VisitPay> sessionContext)
            : base(sessionContext)
        {
        }

        public void Dispose()
        {
            this.CallBackAfterCommit = null;
        }

        public IList<SsoProvider> GetEnabledProviders()
        {
            IQueryable<SsoProvider> query = this.Session.Query<SsoProvider>().Where(x => x.IsEnabled);

            return query.ToList();
        }

        public SsoProvider GetBySourceSystemKey(string sourceSystemKey)
        {
            if (sourceSystemKey.IsNullOrEmpty()) return null;
            ICriteria criteria = this.Session.CreateCriteria(typeof(SsoProvider));
            criteria.Add(Restrictions.Eq("SsoProviderSourceSystemKey", sourceSystemKey));
            return criteria.UniqueResult<SsoProvider>();
        }
    }
}
﻿namespace Ivh.Provider.Core.Sso
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Common.Base.Enums;
    using Common.Data;
    using Common.Data.Connection.Connections;
    using Common.Data.Interfaces;
    using Common.VisitPay.Enums;
    using Domain.Core.Sso.Entities;
    using Domain.Core.Sso.Interfaces;
    using NHibernate;
    using NHibernate.Linq;

    public class SsoVisitPayUserSettingRepository : RepositoryBase<SsoVisitPayUserSetting, VisitPay>, ISsoVisitPayUserSettingRepository, IDisposable
    {
        public SsoVisitPayUserSettingRepository(ISessionContext<VisitPay> sessionContext)
            : base(sessionContext)
        {
        }

        public IList<SsoVisitPayUserSetting> GetByVisitPayUserId(int visitPayUserId)
        {
            IQueryOver<SsoVisitPayUserSetting> query = this.Session.QueryOver<SsoVisitPayUserSetting>()
                .Where(x => x.VisitPayUser.VisitPayUserId == visitPayUserId);

            return query.List();
        }

        public SsoVisitPayUserSetting GetByVisitPayUserIdAndProvider(SsoProviderEnum ssoProvider, int visitPayUserId)
        {
            int ssoProviderId = (int) ssoProvider;

            IQueryOver<SsoVisitPayUserSetting> query = this.Session.QueryOver<SsoVisitPayUserSetting>()
                .Where(x => x.VisitPayUser.VisitPayUserId == visitPayUserId &&
                            x.SsoProvider.SsoProviderId == ssoProviderId);

            return query.Take(1).SingleOrDefault();
        }

        /// <summary>
        /// find providers either accepted or (rejected + ignored) for a given user
        /// </summary>
        /// <param name="visitPayUserId"></param>
        /// <returns></returns>
        public IList<SsoProviderEnum> GetEnabledProvidersForUser(int visitPayUserId)
        {
            IQueryOver<SsoVisitPayUserSetting> query = this.Session.QueryOver<SsoVisitPayUserSetting>()
                .Where(x => x.VisitPayUser.VisitPayUserId == visitPayUserId && (x.SsoStatus == SsoStatusEnum.Accepted || (x.SsoStatus == SsoStatusEnum.Rejected && x.PromptAfterDateTime != null && x.PromptAfterDateTime.Value.Date == DateTime.MaxValue.Date)))
                .Select(x => x.SsoProvider.SsoProviderId);

            return query.List<int>().Select(x => (SsoProviderEnum) x).ToList();
        }

        public void Dispose()
        {
            this.CallBackAfterCommit = null;
        }
    }
}
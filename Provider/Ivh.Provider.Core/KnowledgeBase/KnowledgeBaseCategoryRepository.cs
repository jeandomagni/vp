﻿namespace Ivh.Provider.Core.KnowledgeBase
{
    using System.Collections.Generic;
    using Common.Data;
    using Common.Data.Connection.Connections;
    using Common.Data.Interfaces;
    using Domain.Core.KnowledgeBase.Entities;
    using Domain.Core.KnowledgeBase.Interfaces;
    using NHibernate;

    public class KnowledgeBaseCategoryRepository : RepositoryBase<KnowledgeBaseCategory, VisitPay>, IKnowledgeBaseCategoryRepository
    {
        public KnowledgeBaseCategoryRepository(ISessionContext<VisitPay> sessionContext) : base(sessionContext)
        {
        }

        public IList<KnowledgeBaseCategory> GetActiveKnowledgeBaseCategories()
        {
            IQueryOver<KnowledgeBaseCategory> query = this.Session.QueryOver<KnowledgeBaseCategory>()
                .Where(x => x.IsActive)
                .Cacheable()
                .CacheMode(CacheMode.Normal);
            return query.List();
        }

        public KnowledgeBaseQuestionAnswer GetKnowledgeBaseQuestionAnswerById(int knowledgeBaseQuestionAnswerId)
        {
            IQueryOver<KnowledgeBaseQuestionAnswer> query = this.Session.QueryOver<KnowledgeBaseQuestionAnswer>()
                .Where(x => x.KnowledgeBaseQuestionAnswerId == knowledgeBaseQuestionAnswerId);
            return query.Take(1).SingleOrDefault();
        }
    }
}
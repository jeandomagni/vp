﻿namespace Ivh.Provider.Core.KnowledgeBase
{
    using Common.Data;
    using Common.Data.Connection.Connections;
    using Common.Data.Interfaces;
    using Domain.Core.KnowledgeBase.Entities;
    using Domain.Core.KnowledgeBase.Interfaces;
    using NHibernate;

    public class SearchTextLogRepository : RepositoryBase<SearchTextLog, VisitPay>, ISearchTextLogRepository
    {
        public SearchTextLogRepository(ISessionContext<VisitPay> sessionContext) : base(sessionContext) { }
    }
}
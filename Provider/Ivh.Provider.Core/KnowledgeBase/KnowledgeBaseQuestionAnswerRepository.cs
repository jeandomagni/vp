﻿namespace Ivh.Provider.Core.KnowledgeBase
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Common.Base.Utilities.Extensions;
    using Common.Data;
    using Common.Data.Connection.Connections;
    using Common.Data.Interfaces;
    using Domain.Core.KnowledgeBase.Entities;
    using Domain.Core.KnowledgeBase.Interfaces;
    using MassTransit.Util;
    using NHibernate;

    public class KnowledgeBaseQuestionAnswerRepository : RepositoryBase<KnowledgeBaseQuestionAnswer, VisitPay>, IKnowledgeBaseQuestionAnswerRepository
    {
        public KnowledgeBaseQuestionAnswerRepository(ISessionContext<VisitPay> sessionContext) : base(sessionContext)
        {
        }

        public IList<KnowledgeBaseQuestionAnswer> GetTopVotedKnowledgeBaseQuestionAnswers(int windowInDays, int numberOfQuestions)
        {
            return this.GetQueryable().ToList().OrderByDescending(x => x.Rank(windowInDays)).ThenByDescending(x => x.Rank(null)).Take(numberOfQuestions).ToList();
        }
    }
}
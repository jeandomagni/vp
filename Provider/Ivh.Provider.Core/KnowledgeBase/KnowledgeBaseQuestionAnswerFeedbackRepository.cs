﻿namespace Ivh.Provider.Core.KnowledgeBase
{
    using Common.Data;
    using Common.Data.Connection.Connections;
    using Common.Data.Interfaces;
    using Domain.Core.KnowledgeBase.Entities;
    using Domain.Core.KnowledgeBase.Interfaces;
    using NHibernate;

    public class KnowledgeBaseQuestionAnswerFeedbackRepository : RepositoryBase<KnowledgeBaseQuestionAnswerFeedback, VisitPay>, IKnowledgeBaseQuestionAnswerFeedbackRepository
    {
        public KnowledgeBaseQuestionAnswerFeedbackRepository(ISessionContext<VisitPay> sessionContext) : base(sessionContext)
        {
        }
    }
}

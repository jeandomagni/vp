﻿namespace Ivh.Provider.Core.KnowledgeBase
{
    using Common.Data;
    using Common.Data.Connection.Connections;
    using Common.Data.Interfaces;
    using Domain.Core.KnowledgeBase.Entities;
    using Domain.Core.KnowledgeBase.Interfaces;
    using NHibernate;

    public class KnowledgeBaseQuestionAnswerVoteRepository : RepositoryBase<KnowledgeBaseQuestionAnswerVote, VisitPay>, IKnowledgeBaseQuestionAnswerVoteRepository
    {
        public KnowledgeBaseQuestionAnswerVoteRepository(ISessionContext<VisitPay> sessionContext) : base(sessionContext)
        {
        }
    }
}
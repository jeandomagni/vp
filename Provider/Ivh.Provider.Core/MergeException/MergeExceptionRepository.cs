﻿namespace Ivh.Provider.Core.MergeException
{
    using Common.Data;
    using Common.Data.Connection.Connections;
    using Common.Data.Interfaces;
    using Domain.Core.MergeException.Entities;
    using Domain.Core.MergeException.Interfaces;
    using NHibernate;

    public class MergeExceptionRepository : RepositoryBase<MergeException, VisitPay>, IMergeExceptionRepository
    {
        public MergeExceptionRepository(ISessionContext<VisitPay> sessionContext)
            : base(sessionContext)
        {
        }
    }
}
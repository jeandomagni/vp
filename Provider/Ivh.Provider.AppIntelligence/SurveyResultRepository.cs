﻿namespace Ivh.Provider.AppIntelligence
{
    using System.Collections.Generic;
    using Common.Data;
    using Common.Data.Connection.Connections;
    using Common.Data.Interfaces;
    using Domain.AppIntelligence.Survey.Entities;
    using Domain.AppIntelligence.Survey.Interfaces;
    using NHibernate;

    public class SurveyResultRepository : RepositoryBase<SurveyResult, VisitPay>, ISurveyResultRepository
    {
        public SurveyResultRepository(ISessionContext<VisitPay> sessionContext) : base(sessionContext)
        {
        }

        public IList<SurveyResult> GetSurveyResultsForVisitPayUser(int visitpayUser, int surveyId)
        {
            IQueryOver<SurveyResult> query = this.Session.QueryOver<SurveyResult>()
                .Where(x => x.SurveyId == surveyId)
                .Where(x => x.VisitPayUserId == visitpayUser);

            return query.List();
        }

        public IList<SurveyResult> GetAllSurveyResultsForVisitPayUser(int visitpayUser)
        {
            IQueryOver<SurveyResult> query = this.Session.QueryOver<SurveyResult>()
                .Where(x => x.VisitPayUserId == visitpayUser);

            return query.List();
        }

    }
}
﻿namespace Ivh.Provider.AppIntelligence
{
    using System.Collections.Generic;
    using Common.Data;
    using Common.Data.Connection.Connections;
    using Common.Data.Interfaces;
    using Domain.AppIntelligence.Entities;
    using Domain.AppIntelligence.Interfaces;
    using NHibernate;

    public class RandomizedTestGroupMembershipRepository : RepositoryBase<RandomizedTestGroupMembership, VisitPay>, IRandomizedTestGroupMembershipRepository
    {
        public RandomizedTestGroupMembershipRepository(ISessionContext<VisitPay> sessionContext) : base(sessionContext)
        {
        }

        public RandomizedTestGroupMembership GetRandomizedTestGroupMembership(RandomizedTest randomizedTest, int vpGuarantorId)
        {
            return this.Session.QueryOver<RandomizedTestGroupMembership>()
                .Where(x => x.RandomizedTest.RandomizedTestId == randomizedTest.RandomizedTestId)
                .Where(x => x.VpGuarantorId == vpGuarantorId)
                .Take(1)
                .SingleOrDefault<RandomizedTestGroupMembership>();
        }

        public IList<int> GetManagedGuarantorIds(int managingGuarantorId)
        {
            string query = "SELECT cg.ManagedGuarantorId FROM dbo.ConsolidationGuarantor cg WHERE cg.ManagingGuarantorId = :managingGuarantorId;";

            return this.Session.CreateSQLQuery(query)
                .SetInt32("managingGuarantorId", managingGuarantorId)
                .List<int>();
        }


        public IList<int> GetManagingGuarantorIds(int managedGuarantorId)
        {
            string query = "SELECT cg.ManagingGuarantorId FROM dbo.ConsolidationGuarantor cg WHERE cg.ManagedGuarantorId = :managedGuarantorId;";

            return this.Session.CreateSQLQuery(query)
                .SetInt32("managedGuarantorId", managedGuarantorId)
                .List<int>();
        }
    }
}

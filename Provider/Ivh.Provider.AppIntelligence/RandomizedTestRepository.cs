﻿namespace Ivh.Provider.AppIntelligence
{
    using Common.Base.Enums;
    using Common.Data;
    using Common.Data.Connection.Connections;
    using Common.Data.Interfaces;
    using Common.VisitPay.Enums;
    using Domain.AppIntelligence.Entities;
    using Domain.AppIntelligence.Interfaces;
    using NHibernate;

    public class RandomizedTestRepository : RepositoryBase<RandomizedTest, VisitPay>, IRandomizedTestRepository
    {
        public RandomizedTestRepository(ISessionContext<VisitPay> sessionContext)
            : base(sessionContext)
        {
        }

        public RandomizedTest GetActiveRandomizedTest(RandomizedTestEnum randomizedTestEnum)
        {
            // second level cache
            RandomizedTest randomizedTest = this.Session.QueryOver<RandomizedTest>()
                .Where(x => x.RandomizedTestId == (int)randomizedTestEnum)
                .Cacheable()
                .CacheMode(CacheMode.Normal)
                .SingleOrDefault();

            return randomizedTest != null && randomizedTest.IsActive() ? randomizedTest : null;
        }
    }
}

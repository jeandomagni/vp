﻿namespace Ivh.Provider.AppIntelligence
{
    using Common.Data;
    using Common.Data.Connection.Connections;
    using Common.Data.Interfaces;
    using Domain.AppIntelligence.Entities;
    using Domain.AppIntelligence.Interfaces;
    using NHibernate;

    public class FinancePlanTermsDisplayedRepository : RepositoryBase<FinancePlanTermsDisplayed, VisitPay>, IFinancePlanTermsDisplayedRepository
    {
        public FinancePlanTermsDisplayedRepository(ISessionContext<VisitPay> sessionContext)
            : base(sessionContext)
        {
        }
    }
}
﻿namespace Ivh.Provider.AppIntelligence
{
    using Common.Data;
    using Common.Data.Connection.Connections;
    using Common.Data.Interfaces;
    using Domain.AppIntelligence.Survey.Entities;
    using Domain.AppIntelligence.Survey.Interfaces;
    using NHibernate;

    public class SurveyRepository : RepositoryBase<Survey, VisitPay>, ISurveyRepository
    {
        public SurveyRepository(ISessionContext<VisitPay> sessionContext) : base(sessionContext)
        {
        }

        public Survey GetSurveyBySurveyName(string surveyName)
        {
            IQueryOver<Survey> query = this.Session.QueryOver<Survey>()
                .Where(x => x.SurveyName == surveyName);

            return query.SingleOrDefault();

        }
    }
}
﻿namespace Ivh.Provider.AppIntelligence.Modules
{
    using Autofac;
    using Domain.AppIntelligence.Interfaces;
    using Domain.AppIntelligence.Survey.Interfaces;

    public class Module : Autofac.Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<FinancePlanTermsDisplayedRepository>().As<IFinancePlanTermsDisplayedRepository>();

            builder.RegisterType<RandomizedTestGroupMembershipRepository>().As<IRandomizedTestGroupMembershipRepository>();
            builder.RegisterType<RandomizedTestRepository>().As<IRandomizedTestRepository>();

            builder.RegisterType<SurveyRepository>().As<ISurveyRepository>();
            builder.RegisterType<SurveyResultRepository>().As<ISurveyResultRepository>();
        }
    }
}
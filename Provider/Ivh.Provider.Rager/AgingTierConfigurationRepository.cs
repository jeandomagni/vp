﻿namespace Ivh.Provider.Rager
{
    using System;
    using System.Collections.Generic;
    using Common.Data;
    using Common.Data.Connection;
    using Common.Data.Connection.Connections;
    using Common.Data.Interfaces;
    using Domain.Rager.Entities;
    using Domain.Rager.Interfaces;
    using NHibernate;

    public class AgingTierConfigurationRepository : RepositoryBase<AgingTierConfiguration, CdiEtl>, IAgingTierConfigurationRepository
    {
        public AgingTierConfigurationRepository(ISessionContext<CdiEtl> sessionContext) : base(sessionContext)
        {
        }

        public IList<AgingTierConfiguration> GetActiveAgingTierConfigurations()
        {
            IList<AgingTierConfiguration> agingTierConfigurations = this.Session.QueryOver<AgingTierConfiguration>()
                .Where(x => x.ActiveDate < DateTime.UtcNow).List();
            return agingTierConfigurations;
        }
    }
}
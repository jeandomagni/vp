﻿namespace Ivh.Provider.Rager
{
    using System.Linq;
    using Common.Data;
    using Common.Data.Connection.Connections;
    using Common.Data.Interfaces;
    using Domain.Rager.Entities;
    using Domain.Rager.Interfaces;

    public class AgingGuarantorStateRepository : RepositoryBase<AgingGuarantorState, CdiEtl>, IAgingGuarantorStateRepository
    {
        public AgingGuarantorStateRepository(ISessionContext<CdiEtl> sessionContext) : base(sessionContext)
        {
        }

        public bool? AgingGuarantorIsInactive(int vpGuarantorId)
        {
            IQueryable<bool?> query = this.GetQueryable()
                .Where(x => x.VpGuarantorId == vpGuarantorId)
                .OrderByDescending(x => x.InsertDate)
                .ThenByDescending(x => x.AgingGuarantorStateId)
                .Select(x => (bool?)x.IsInactive);

            bool? isInactive = query.FirstOrDefault();

            return isInactive;
        }
    }
}

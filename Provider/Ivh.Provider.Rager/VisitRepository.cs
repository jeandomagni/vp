﻿namespace Ivh.Provider.Rager
{
    using System;
    using System.Collections.Generic;
    using Common.Data;
    using Common.Data.Connection;
    using Common.Data.Connection.Connections;
    using Common.Data.Interfaces;
    using Domain.Rager.Entities;
    using Domain.Rager.Interfaces;
    using NHibernate;
    using NHibernate.Criterion;

    public class VisitRepository : RepositoryBase<Visit,CdiEtl>, IVisitRepository
    {
        public VisitRepository(ISessionContext<CdiEtl> sessionContext) : base(sessionContext)
        {
        }

        public Visit GetVisitBySourceKeyAndBillingId(string sourceSystemKey, int billingSystemId)
        {
            Visit visit = this.Session.QueryOver<Visit>().Where(x => x.VisitSourceSystemKey == sourceSystemKey)
                .Where(x => x.VisitBillingSystemId == billingSystemId).SingleOrDefault();
            return visit;
        }

        public IList<int> GetEligibleToAgeVisitIds(DateTime dateToProcess)
        {
            Visit visitAlias = null;

            // VisitFinancePlan
            QueryOver<VisitFinancePlan> fpSubQuery = QueryOver.Of<VisitFinancePlan>()
                .Where(vfp => vfp.Visit.VisitId == visitAlias.VisitId)
                .Select(vfp => vfp.IsOnFinancePlan)
                .OrderBy(vfp => vfp.InsertDate)
                .Desc
                .OrderBy(vfp => vfp.VisitFinancePlanId)
                .Desc
                .Take(1);

            Disjunction fpDisjunction = Restrictions.Disjunction();
            fpDisjunction.Add(Subqueries.WhereValue(false).Eq(fpSubQuery));
            fpDisjunction.Add(Subqueries.WhereNotExists(fpSubQuery));

            //VisitRecall
            QueryOver<VisitRecall> recallSubQuery = QueryOver.Of<VisitRecall>()
                .Where(vfp => vfp.Visit.VisitId == visitAlias.VisitId)
                .Select(vfp => vfp.IsRecalled)
                .OrderBy(vfp => vfp.InsertDate)
                .Desc
                .OrderBy(vfp => vfp.VisitRecallId)
                .Desc
                .Take(1);

            Disjunction recallDisjunction = Restrictions.Disjunction();
            recallDisjunction.Add(Subqueries.WhereValue(false).Eq(recallSubQuery));
            recallDisjunction.Add(Subqueries.WhereNotExists(recallSubQuery));

            //VisitSuspension
            QueryOver<VisitSuspension> suspensionSubQuery = QueryOver.Of<VisitSuspension>()
                .Where(vfp => vfp.Visit.VisitId == visitAlias.VisitId)
                .Select(vfp => vfp.IsSuspended)
                .OrderBy(vfp => vfp.InsertDate)
                .Desc
                .OrderBy(vfp => vfp.VisitSuspensionId)
                .Desc
                .Take(1);

            Disjunction suspensionDisjunction = Restrictions.Disjunction();
            suspensionDisjunction.Add(Subqueries.WhereValue(false).Eq(suspensionSubQuery));
            suspensionDisjunction.Add(Subqueries.WhereNotExists(suspensionSubQuery));

            //VisitState
            QueryOver<VisitState> stateSubQuery = QueryOver.Of<VisitState>()
                .Where(vfp => vfp.Visit.VisitId == visitAlias.VisitId)
                .Select(vfp => vfp.IsInactive)
                .OrderBy(vfp => vfp.InsertDate)
                .Desc
                .OrderBy(vfp => vfp.VisitStateId)
                .Desc
                .Take(1);

            Disjunction stateDisjunction = Restrictions.Disjunction();
            stateDisjunction.Add(Subqueries.WhereValue(false).Eq(stateSubQuery));
            stateDisjunction.Add(Subqueries.WhereNotExists(stateSubQuery));

            //GuarantorState
            QueryOver<AgingGuarantorState> guarantorStateSubQuery = QueryOver.Of<AgingGuarantorState>()
                .Where(vgs => vgs.VpGuarantorId == visitAlias.VpGuarantorId)
                .Select(vgs => vgs.IsInactive)
                .OrderBy(vgs => vgs.InsertDate)
                .Desc
                .OrderBy(vgs => vgs.AgingGuarantorStateId)
                .Desc
                .Take(1);

            Disjunction guarantorStateDisjunction = Restrictions.Disjunction();
            guarantorStateDisjunction.Add(Subqueries.WhereValue(false).Eq(guarantorStateSubQuery));
            guarantorStateDisjunction.Add(Subqueries.WhereNotExists(guarantorStateSubQuery));

            //GuarantorType
            QueryOver<AgingGuarantorType> guarantorTypeSubQuery = QueryOver.Of<AgingGuarantorType>()
                .Where(vgt => vgt.VpGuarantorId == visitAlias.VpGuarantorId)
                .Select(vgt => vgt.IsCallCenter)
                .OrderBy(vgt => vgt.InsertDate)
                .Desc
                .OrderBy(vgt => vgt.AgingGuarantorTypeId)
                .Desc
                .Take(1);

            Disjunction guarantorTypeDisjunction = Restrictions.Disjunction();
            guarantorTypeDisjunction.Add(Subqueries.WhereValue(false).Eq(guarantorTypeSubQuery));
            guarantorTypeDisjunction.Add(Subqueries.WhereNotExists(guarantorTypeSubQuery));

            IList<int> eligibleVisits = this.Session.QueryOver<Visit>(() => visitAlias)
                .Where(x => x.ProcessedDate != dateToProcess.Date || x.ProcessedDate == null)
                .And(fpDisjunction)
                .And(recallDisjunction)
                .And(suspensionDisjunction)
                .And(stateDisjunction)
                .And(guarantorStateDisjunction)
                .And(guarantorTypeDisjunction)
                .Select(v => v.VisitId)
                .List<int>();

            return eligibleVisits;
        }


    }
}
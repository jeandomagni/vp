﻿namespace Ivh.Provider.Rager
{
    using System.Linq;
    using Common.Data;
    using Common.Data.Connection.Connections;
    using Common.Data.Interfaces;
    using Domain.Rager.Entities;
    using Domain.Rager.Interfaces;

    public class AgingGuarantorTypeRepository : RepositoryBase<AgingGuarantorType, CdiEtl>, IAgingGuarantorTypeRepository
    {
        public AgingGuarantorTypeRepository(ISessionContext<CdiEtl> sessionContext) : base(sessionContext)
        {
        }

        public bool? AgingGuarantorIsCallCenter(int vpGuarantorId)
        {
            IQueryable<bool?> query = this.GetQueryable()
                .Where(x => x.VpGuarantorId == vpGuarantorId)
                .OrderByDescending(x => x.InsertDate)
                .ThenByDescending(x => x.AgingGuarantorTypeId)
                .Select(x => (bool?)x.IsCallCenter);

            bool? isCallCenter = query.FirstOrDefault();

            return isCallCenter;
        }
    }
}

﻿namespace Ivh.Provider.Pdf
{
    using System.Drawing.Printing;
    using System.IO;
    using TuesPechkin;

    /// <summary>
    /// See documentation for working with HTML-to-PDF conversion here:
    /// https://ivinci.atlassian.net/wiki/spaces/SD/pages/322437291
    /// </summary>
    public class TuesPechkinPdfConverter : IPdfConverter
    {
        //Do not directly access this!
        private static IConverter _converter;

        private static readonly object ConverterLock = new object();

        private static IConverter Converter
        {
            get
            {
                if (_converter == null)
                {
                    lock (ConverterLock)
                    {
                        if (_converter == null)
                        {
                            _converter =
                                new ThreadSafeConverter(
                                    new RemotingToolset<PdfToolset>(
                                        new Win64EmbeddedDeployment(new TempFolderDeployment())));
                            //new Win32EmbeddedDeployment(new TempFolderDeployment())));
                        }
                    }
                }
                return _converter;
            }
        }

        public byte[] CreatePdfFromHtml(string pdfTitle, string htmlContent)
        {
            const decimal mmInPostScriptPoint = 0.352777778m;

            HtmlToPdfDocument document = new HtmlToPdfDocument
            {
                GlobalSettings =
                {
                    ProduceOutline = true,
                    DocumentTitle = pdfTitle,
                    PaperSize = PaperKind.Letter, // Implicit conversion to PechkinPaperSize
                    Margins =
                    {
                        Left = (double) (40m*mmInPostScriptPoint),
                        Right = (double) (40m*mmInPostScriptPoint),
                        Top = (double) (40m*mmInPostScriptPoint),
                        Bottom = (double) (40m*mmInPostScriptPoint),
                        Unit = Unit.Millimeters
                    }

                },
                Objects =
                {
                    new ObjectSettings
                    {
                        HtmlText = htmlContent,
                        WebSettings =
                        {
                            DefaultEncoding = "UTF-8",
                            PrintBackground = true,
                            MinimumFontSize = 5
                        }
                    }
                }
            };

            return Converter.Convert(document);
        }

        public void CreateFileFromHtml(string pdfTitle, string htmlContent, string pathAndFileName, bool overwrite = false)
        {
            if (!File.Exists(pathAndFileName) || overwrite)
            {
                byte[] result = this.CreatePdfFromHtml(pdfTitle, htmlContent);

                if (result != null)
                {
                    File.WriteAllBytes(pathAndFileName, result);
                }
            }
        }
    }
}
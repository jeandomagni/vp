﻿namespace Ivh.Provider.Pdf
{
    public interface IPdfConverter
    {
        byte[] CreatePdfFromHtml(string pdfTitle, string htmlContent);
        void CreateFileFromHtml(string pdfTitle, string htmlContent, string pathAndFileName,  bool overwrite = false);
    }
}
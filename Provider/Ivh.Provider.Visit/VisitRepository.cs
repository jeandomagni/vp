﻿namespace Ivh.Provider.Visit
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Application.Core.Common.Dtos;
    using Common.Base.Utilities.Extensions;
    using Common.Base.Utilities.Helpers;
    using Common.Cache;
    using Common.Data;
    using Common.Data.Connection.Connections;
    using Common.Data.Interfaces;
    using Common.ServiceBus.Interfaces;
    using Common.VisitPay.Enums;
    using Common.VisitPay.Messages.Core;
    using Common.VisitPay.Messages.HospitalData;
    using Domain.FinanceManagement.FinancePlan.Entities;
    using Domain.FinanceManagement.Statement.Entities;
    using Domain.Guarantor.Entities;
    using Domain.Settings.Interfaces;
    using Domain.User.Entities;
    using Domain.Visit.Entities;
    using Domain.Visit.Interfaces;
    using Domain.Visit.Unmatching.Entities;
    using NHibernate;
    using NHibernate.Criterion;
    using NHibernate.Criterion.Lambda;
    using NHibernate.SqlCommand;
    using NHibernate.Transform;

    public class VisitRepository : RepositoryCacheBase<Visit, VisitPay>, IVisitRepository, IDisposable
    {
        private readonly IList<VisitBalanceChangedEvent> _visitChangedBalanceEvents = new List<VisitBalanceChangedEvent>();
        private readonly IList<VisitChangedStatusMessage> _visitChangedStatusMessages = new List<VisitChangedStatusMessage>();
        private readonly IList<OutboundVisitMessage> _outboundVisitMessages = new List<OutboundVisitMessage>();
        private readonly Lazy<IBus> _bus;
        private readonly Lazy<IApplicationSettingsService> _applicationSettings;

        public VisitRepository(
            ISessionContext<VisitPay> sessionContext,
            Lazy<IBus> bus,
            Lazy<IApplicationSettingsService> applicationSettings,
            Lazy<IDistributedCache> cache) : base(sessionContext, cache)
        {
            this._bus = bus;
            this._applicationSettings = applicationSettings;
            this.PublishMessagesAfterRollbackCallback();
            //commit callback will be registered the first time something is saved.
        }

        public bool DoesGuarantorHaveVisitWithInsurancePlanInList(int vpGuarantorId, List<string> insurancePlanSsks)
        {
            //If the list is empty allow all guarantors to view.
            if (insurancePlanSsks == null || insurancePlanSsks.Count == 0)
            {
                return true;
            }

            ICriteria criteria = this.Session.CreateCriteria<Visit>("v")
                .CreateAlias("v.VisitInsurancePlans", "vip")
                .CreateAlias("vip.InsurancePlan", "ip")
                .Add(Restrictions.Eq("v.VPGuarantor", new Guarantor { VpGuarantorId = vpGuarantorId }))
                .Add(Restrictions.In("ip.SourceSystemKey", insurancePlanSsks))
                .SetProjection(Projections.RowCount());

            return criteria.UniqueResult<int>() > 0;
        }

        public Visit GetVisit(int vpGuarantorId, int visitId)
        {
            Guarantor guarantorAlias = null;

            return this.Session.QueryOver<Visit>()
                .JoinAlias(v => v.VPGuarantor, () => guarantorAlias)
                .Where(() => guarantorAlias.VpGuarantorId == vpGuarantorId)
                .And(v => v.VisitId == visitId)
                .Take(1)
                .SingleOrDefault();
        }

        public Visit GetBySourceSystemKey(string sourceSystemKey, int billingSystemId)
        {
            if (billingSystemId < 0 || sourceSystemKey == null)
            {
                return null;
            }

            return this.GetCached(
                x => x.BillingSystemId, billingSystemId,
                x => x.SourceSystemKey, sourceSystemKey,
                (b, s) =>
                {
                    ICriteria criteria = this.Session.CreateCriteria(typeof(Visit));
                    criteria.Add(Restrictions.Eq("SourceSystemKey", s));
                    criteria.Add(Restrictions.Eq("BillingSystem.BillingSystemId", b));
                    return criteria.UniqueResult<Visit>();
                });
        }

        public Visit GetVisitBySystemSourceKeyUnmatched(int vpGuarantorId, string sourceSystemKey, int billingSystemId)
        {
            if (billingSystemId < 0 || sourceSystemKey == null)
            {
                return null;
            }

            return this.GetCached(
                x => x.MatchedBillingSystem.BillingSystemId, billingSystemId,
                x => x.MatchedSourceSystemKey, sourceSystemKey,
                x => x.VPGuarantor.VpGuarantorId, vpGuarantorId,
                (b, s, g) =>
                {
                    ICriteria criteria = this.Session.CreateCriteria(typeof(Visit));
                    criteria.Add(Restrictions.Eq("MatchedSourceSystemKey", s));
                    criteria.Add(Restrictions.Eq("MatchedBillingSystem.BillingSystemId", b));
                    criteria.Add(Restrictions.Eq("VPGuarantor.VpGuarantorId", g));
                    criteria.AddOrder(Order.Desc("InsertDate"));
                    return criteria.UniqueResult<Visit>();
                });
        }

        public IList<Visit> GetVisitsBySystemSourceKeyUnmatched(int vpGuarantorId, string sourceSystemKey, int billingSystemId)
        {
            if (billingSystemId < 0 || sourceSystemKey == null)
            {
                return null;
            }

            ICriteria criteria = this.Session.CreateCriteria(typeof(Visit));
            criteria.Add(Restrictions.Eq("MatchedSourceSystemKey", sourceSystemKey));
            criteria.Add(Restrictions.Eq("MatchedBillingSystem.BillingSystemId", billingSystemId));
            criteria.Add(Restrictions.Eq("VPGuarantor.VpGuarantorId", vpGuarantorId));
            return criteria.List<Visit>();
        }

        /*public IList<Visit> GetBySourceSystemKeyUnmatched(int vpGuarantorId, string sourceSystemKey)
        {
            if (sourceSystemKey.IsNullOrEmpty())
            {
                return null;
            }

            ICriteria criteria = this.Session.CreateCriteria(typeof(Visit));
            criteria.Add(Restrictions.Eq("MatchedSourceSystemKey", sourceSystemKey));
            criteria.Add(Restrictions.Eq("VPGuarantor.VpGuarantorId", vpGuarantorId));

            return criteria.List<Visit>();
        }*/

        public IReadOnlyList<Visit> GetVisits(int vpGuarantorId)
        {
            Visit visitAlias = null;
            Guarantor guarantorAlias = null;

            IQueryOver<Visit, Visit> query = this.Session.QueryOver(() => visitAlias)
                .JoinAlias(() => visitAlias.VPGuarantor, () => guarantorAlias)
                .Where(v => v.VPGuarantor.VpGuarantorId == vpGuarantorId)
                .OrderBy(() => visitAlias.DischargeDate).Asc;

            IList<Visit> visits = query.List();
            return (IReadOnlyList<Visit>)visits;
        }

        public IReadOnlyList<Visit> GetVisits(int vpGuarantorId, IList<int> visitIds)
        {
            if (visitIds.IsNullOrEmpty())
            {
                return new List<Visit>();
            }

            Visit visitAlias = null;
            Guarantor guarantorAlias = null;

            IQueryOver<Visit, Visit> query = this.Session.QueryOver(() => visitAlias)
                .JoinAlias(() => visitAlias.VPGuarantor, () => guarantorAlias)
                .Where(v => v.VPGuarantor.VpGuarantorId == vpGuarantorId)
                .Where(Restrictions.In("VisitId", visitIds.ToList()));

            IList<Visit> visits = query.List();
            return (IReadOnlyList<Visit>)visits;
        }

        public IList<int> GetVisitIds(int vpGuarantorId, VisitStateEnum visitStateEnum)
        {
            IList<int> list = this.Session.QueryOver<Visit>()
                .Where(x => x.VPGuarantor.VpGuarantorId == vpGuarantorId)
                .Where(x => x.CurrentVisitState == visitStateEnum)
                .Select(x => x.VisitId)
                .List<int>();

            return list;
        }

        /// <summary>
        /// This returns a non-distinct list of all facilities associated with the <param name="vpGuarantorId"></param>
        /// value passed in.
        /// </summary>
        /// <param name="vpGuarantorId"></param>
        /// <returns></returns>
        public IList<Facility> GetFacilitiesForGuarantor(int vpGuarantorId)
        {
	        return this.Session.QueryOver<Visit>()
		        .Where(x => x.VPGuarantor.VpGuarantorId == vpGuarantorId)
		        .Where(x => x.Facility != null)
		        .Where(x => x.BillingSystem.BillingSystemId > 0)
		        .Where(x => !x.BillingHold)
		        .Where(x => x.VpEligible)
				.Select(x => x.Facility)
		        .List<Facility>();
        }

        /// <summary>
        /// Lists all facilities on active visits associated with the given guarantor.
        /// </summary>
        private IList<Facility> GetFacilitiesOnActiveVisitsForGuarantor(int vpGuarantorId)
        {
            return this.Session.QueryOver<Visit>()
                .Where(x => x.VPGuarantor.VpGuarantorId == vpGuarantorId)
                .Where(x => x.Facility != null)
                .Where(x => x.BillingSystem.BillingSystemId > 0)
                .Where(x => x.CurrentVisitState == VisitStateEnum.Active)
                .Where(x => !x.BillingHold)
                .Where(x => x.VpEligible)
                .Select(x => x.Facility)
                .List<Facility>();
        }


        public IList<Visit> GetVisitsForGuarantor(int vpGuarantorId)
        {
            Visit visitAlias = null;
            Guarantor guarantorAlias = null;
            VisitPayUser userAlias = null;

            IQueryOver<Visit, Visit> query = this.Session.QueryOver(() => visitAlias)
                .JoinAlias(() => visitAlias.VPGuarantor, () => guarantorAlias)
                .JoinAlias(() => guarantorAlias.User, () => userAlias)
                .Where(v => v.VPGuarantor.VpGuarantorId == vpGuarantorId);

            IList<Visit> visits = query.List();
            return visits;
        }

        public IReadOnlyList<Visit> GetVisitsUnmatched(int vpGuarantorId)
        {
            IQueryOver<Visit> query = this.Session.QueryOver<Visit>()
                .Where(x => x.VPGuarantor.VpGuarantorId == vpGuarantorId &&
                            x.IsUnmatched == true);

            return (IReadOnlyList<Visit>)query.List();
        }

        public int GetVisitCount(int vpGuarantorId, IEnumerable<VisitStateEnum> visitStates)
        {
            ICriteria criteria = this.Session.CreateCriteria<Visit>("v")
                .Add(Restrictions.Eq("VPGuarantor", new Guarantor() { VpGuarantorId = vpGuarantorId }))
                .Add(Restrictions.In(nameof(Visit.CurrentVisitState), visitStates.ToList()))
                .SetProjection(Projections.RowCount());

            //only count unmatched visits in the client portal
            if (this._applicationSettings.Value.IsWebApplication.Value
                && !this._applicationSettings.Value.IsClientApplication.Value)
            {
                criteria.Add(Restrictions.Eq(nameof(Visit.IsUnmatched), false));
            }

            return criteria.UniqueResult<int>();
        }

        public bool HasVisits(int vpGuarantorId, IEnumerable<VisitStateEnum> visitStates)
        {
            return this.GetVisitCount(vpGuarantorId, visitStates) > 0;
        }

        public VisitResults GetVisits(int vpGuarantorId, VisitFilter filter, int? batch, int? batchSize)
        {
            Visit visitAlias = null;
            Guarantor guarantorAlias = null;

            IQueryOver<Visit, Visit> query = this.Session.QueryOver(() => visitAlias)
                .JoinAlias(() => visitAlias.VPGuarantor, () => guarantorAlias)
                .Where(v => v.VPGuarantor.VpGuarantorId == vpGuarantorId);

            Filter(filter, query, this._applicationSettings.Value);
            Sort(filter, query);

            IList<Visit> pagedList = batch.HasValue && batchSize.HasValue ? query.Skip(batch.Value * batchSize.Value).Take(batchSize.Value).List() : query.List();

            VisitResults visitResults = new VisitResults
            {
                TotalRecords = this.GetVisitTotals(vpGuarantorId, filter),
                Visits = pagedList
            };

            return visitResults;
        }

        public int GetVisitTotals(int vpGuarantorId, VisitFilter filter)
        {
            Visit visitAlias = null;
            Guarantor guarantorAlias = null;

            IQueryOver<Visit, Visit> query = this.Session.QueryOver(() => visitAlias)
                .JoinAlias(() => visitAlias.VPGuarantor, () => guarantorAlias)
                .Where(x => x.VPGuarantor.VpGuarantorId == vpGuarantorId);

            Filter(filter, query, this._applicationSettings.Value);

            return query.RowCount();
        }

        public IReadOnlyList<Visit> GetVisits(IList<VisitStateEnum> visitStates)
        {
            ICriteria criteria = this.Session.CreateCriteria<Visit>("v")
                .Add(Restrictions.In(nameof(Visit.CurrentVisitState), visitStates.ToList()));

            //only count unmatched visits in the client portal
            if (this._applicationSettings.Value.IsWebApplication.Value
                && !this._applicationSettings.Value.IsClientApplication.Value)
            {
                criteria.Add(Restrictions.Eq(nameof(Visit.IsUnmatched), false));
            }

            return (IReadOnlyList<Visit>)criteria.List<Visit>();
        }

        public IReadOnlyList<Visit> GetVisits(VisitFilter filter)
        {
            Visit visitAlias = null;

            IQueryOver<Visit, Visit> query = this.Session.QueryOver(() => visitAlias);
            Filter(filter, query, this._applicationSettings.Value);
            Sort(filter, query);

            return (IReadOnlyList<Visit>)query.List<Visit>();
        }

        public IReadOnlyList<Visit> GetAllActiveVisitsForVpGuarantor(int vpGuarantorId)
        {
            Visit visitAlias = null;
            Guarantor guarantorAlias = null;

            List<Visit> results = 
                this.Session.QueryOver(() => visitAlias)
                    .JoinAlias(() => visitAlias.VPGuarantor, () => guarantorAlias)
                    .Where(v => v.VPGuarantor.VpGuarantorId == vpGuarantorId)
                    .Where(v => v.VpEligible)
                    .List()
                    .ToList();

            return results;
        }

        public bool GuarantorHasVisitsInMultipleStates(int vpGuarantorId)
        {
            List<string> stateCodes = this.GetStateCodesForGuarantorVisits(vpGuarantorId).ToList();
            return stateCodes.Count > 1;
        }

        public IList<string> GetStateCodesForGuarantorVisits(int vpGuarantorId)
        {
            IList<Facility> facilitiesForActiveVisits = this.GetFacilitiesOnActiveVisitsForGuarantor(vpGuarantorId);
            List<string> stateCodes = facilitiesForActiveVisits.Select(f => f?.RicState?.StateCode).Distinct().ToList();
            return stateCodes;
        }

        public IList<Visit> GetAllWithIntList(IList<int> toList, bool useLazyLoading = false)
        {
            IList<Visit> visitList = new List<Visit>();
            const int chunkSize = 500;
            IEnumerable<IEnumerable<int>> visitIdChunks = toList.SplitIntoChunks(chunkSize);

            foreach (IEnumerable<int> chunk in visitIdChunks)
            {
                if (useLazyLoading)
                {
                    ICriteria criteria = this.Session.CreateCriteria<VisitLazy>();
                    criteria.Add(Restrictions.In("VisitId", chunk.ToList()));
                    IList<VisitLazy> myVisitList = criteria.List<VisitLazy>();
                    visitList.AddRange(myVisitList);
                }
                else
                {
                    ICriteria criteria = this.Session.CreateCriteria<Visit>();
                    criteria.Add(Restrictions.In("VisitId", chunk.ToList()));
                    IList<Visit> myVisitList = criteria.List<Visit>();
                    visitList.AddRange(myVisitList);
                }
            }

            return visitList;
        }

        /*public DateTime? GetFirstStatementPeriodEndDate(Visit visit)
        {
            DetachedCriteria subQuery = DetachedCriteria.For<VpStatement>("s2")
                .Add(Restrictions.EqProperty("s2.VpGuarantorId", "s1.VpGuarantorId"))
                .AddOrder(new Order("StatementDate", true))
                .SetMaxResults(1)
                .SetProjection(Projections.Property("VpStatementId"));

            ICriteria criteria = this.Session.CreateCriteria<VpStatement>("s1");
            criteria.Add(Restrictions.Eq("VpGuarantorId", visit.VPGuarantor.VpGuarantorId));
            criteria.Add(Subqueries.PropertyIn("VpStatementId", subQuery));
            criteria.SetProjection(Projections.Property("PeriodEndDate"));
            return criteria.UniqueResult<DateTime?>();
        }*/

        /*public DateTime? GetMostRecentStatementPeriodEndDate(Visit visit)
        {
            DetachedCriteria subQuery = DetachedCriteria.For<VpStatement>("s2")
                .Add(Restrictions.EqProperty("s2.VpGuarantorId", "s1.VpGuarantorId"))
                .AddOrder(new Order("StatementDate", false))
                .SetMaxResults(1)
                .SetProjection(Projections.Property("VpStatementId"));

            ICriteria criteria = this.Session.CreateCriteria<VpStatement>("s1");
            criteria.Add(Restrictions.Eq("VpGuarantorId", visit.VPGuarantor.VpGuarantorId));
            criteria.Add(Subqueries.PropertyIn("VpStatementId", subQuery));
            criteria.SetProjection(Projections.Property("PeriodEndDate"));
            return criteria.UniqueResult<DateTime?>();
        }*/

        public IList<VisitResult> GetFinalPastDueVisitsMissingNotification(int? vpGuarantorId = null)
        {
            ICriteria criteria = this.Session.CreateCriteria<Visit>("v");
            criteria.CreateAlias("v.CurrentVisitAgingHistory", "a");
            criteria.CreateAlias("v.CurrentVisitStateHistory", "s");

            criteria.Add(Restrictions.Eq("a.AgingTier", AgingTierEnum.FinalPastDue));
            criteria.Add(Restrictions.Not(Restrictions.Eq("s.EvaluatedState", VisitStateEnum.Inactive)));
            criteria.Add(Restrictions.IsNull("v.FinalPastDueEmailSentDate"));
            if (vpGuarantorId != null)
            {
                criteria.Add(Restrictions.Eq("v.VpGuarantorId", vpGuarantorId));
            }
            criteria.SetProjection(Projections.ProjectionList()
                    .Add(Projections.Property("v.VisitId"), "VisitId")
                    .Add(Projections.Property("v.VPGuarantor.VpGuarantorId"), "GuarantorId")
                )
                .SetResultTransformer(Transformers.AliasToBean(typeof(VisitResult)));

            return criteria.List<VisitResult>();
        }

        public IList<VisitResult> GetAllVisitResults()
        {
            ICriteria criteria = this.Session.CreateCriteria<Visit>();
            criteria.SetProjection(Projections.ProjectionList()
                                .Add(Projections.Property("VPGuarantor.VpGuarantorId"), "GuarantorId")
                                .Add(Projections.Property("VisitId"), "VisitId")
                                )
                .SetResultTransformer(Transformers.AliasToBean(typeof(VisitResult)));

            return criteria.List<VisitResult>();
        }

        public override void InsertOrUpdate(Visit entity)
        {
            this.QueueUnpublishedMessages(entity);
            base.InsertOrUpdate(entity);
        }

        public StatementVisitStatusModel GetStatementVisitStateModel(Visit visit)
        {
            DetachedCriteria subQuery = DetachedCriteria.For<VpStatement>("s2")
                .Add(Restrictions.EqProperty("s2.VpGuarantorId", "s1.VpGuarantorId"))
                .AddOrder(new Order("StatementDate", false))
                .SetMaxResults(1)
                .SetProjection(Projections.Property("VpStatementId"));

            ICriteria criteria = this.Session.CreateCriteria<VpStatement>("s1");
            criteria.Add(Restrictions.Eq("VpGuarantorId", visit.VPGuarantor.VpGuarantorId));
            criteria.Add(Subqueries.PropertyIn("VpStatementId", subQuery));
            VpStatement statement = criteria.UniqueResult<VpStatement>();

            return new StatementVisitStatusModel()
            {
                IsInGracePeriod = statement != null && statement.IsGracePeriod,
                StatementPeriodEndDate = statement?.PeriodEndDate,
                VisitId = visit.VisitId
            };
        }

        /*public bool IsInGracePeriod(Visit visit)
        {
            DetachedCriteria subQuery = DetachedCriteria.For<VpStatement>("s2")
                .Add(Restrictions.EqProperty("s2.VpGuarantorId", "s1.VpGuarantorId"))
                .AddOrder(new Order("StatementDate", false))
                .SetMaxResults(1)
                .SetProjection(Projections.Property("VpStatementId"));

            ICriteria criteria = this.Session.CreateCriteria<VpStatement>("s1");
            criteria.Add(Restrictions.Eq("VpGuarantorId", visit.VPGuarantor.VpGuarantorId));
            criteria.Add(Subqueries.PropertyIn("VpStatementId", subQuery));
            VpStatement statement = criteria.UniqueResult<VpStatement>();
            return statement != null && statement.IsGracePeriod;
        }*/

        public bool IsOnFinancePlan(Visit visit)
        {
            IList<FinancePlanStatusEnum> financePlanStatuses = EnumHelper<FinancePlanStatusEnum>.GetValues(FinancePlanStatusEnumCategory.Active).ToList();

            return this.IsOnFinancePlanInStatus(visit, financePlanStatuses);
        }

        /*public bool IsOnFinancePlan(int visitId)
        {
            IList<FinancePlanStatusEnum> financePlanStatuses = EnumHelper<FinancePlanStatusEnum>.GetValues(FinancePlanStatusEnumCategory.Active).ToList();

            return this.IsOnFinancePlanInStatus(visitId, financePlanStatuses);
        }*/

        private bool IsOnFinancePlanInStatus(int visitId, IList<FinancePlanStatusEnum> financePlanStatuses)
        {
            FinancePlan fp = null;
            FinancePlanVisit fpv = null;

            int count = this.Session.QueryOver(() => fpv)
                .JoinAlias(() => fpv.FinancePlan, () => fp)
                .Where(() => fpv.VisitId == visitId)
                .Where(Restrictions.In(Projections.Property(() => fp.CurrentFinancePlanStatus.FinancePlanStatusId), financePlanStatuses.ToList()))
                .Where(Restrictions.IsNull(Projections.Property(() => fpv.HardRemoveDate)))
                .RowCount();

            return count > 0;
        }

        public bool IsOnFinancePlanInStatus(Visit visit, IList<FinancePlanStatusEnum> financePlanStatuses)
        {
            return this.IsOnFinancePlanInStatus(visit.VisitId, financePlanStatuses);
        }

        public bool IsOnStatement(Visit visit)
        {
            DetachedCriteria subQuery = DetachedCriteria.For<VpStatement>("s2")
                .Add(Restrictions.EqProperty("s2.VpGuarantorId", "s1.VpGuarantorId"))
                .AddOrder(new Order("StatementDate", false))
                .SetMaxResults(1)
                .SetProjection(Projections.Property("VpStatementId"));

            ICriteria criteria = this.Session.CreateCriteria<VpStatement>("s1");
            criteria.CreateCriteria("s1.VpStatementVisits", "s3", JoinType.InnerJoin);
            criteria.Add(Restrictions.Eq("s3.Visit.VisitId", visit.VisitId));
            criteria.Add(Restrictions.Eq("VpGuarantorId", visit.VPGuarantor.VpGuarantorId));
            criteria.Add(Subqueries.PropertyIn("VpStatementId", subQuery));
            criteria.SetProjection(Projections.RowCount());

            return criteria.UniqueResult<int>() > 0;
        }

        /*public bool IsOnStatement(Visit visit, DateTime startDate, DateTime endDate)
        {
            DetachedCriteria subQuery = DetachedCriteria.For<VpStatement>("s2")
                .Add(Restrictions.EqProperty("s2.VpGuarantorId", "s1.VpGuarantorId"))
                .AddOrder(new Order("StatementDate", false))
                .SetMaxResults(1)
                .SetProjection(Projections.Property("VpStatementId"));

            ICriteria criteria = this.Session.CreateCriteria<VpStatement>("s1");
            criteria.CreateCriteria("s1.VpStatementVisits", "s3", JoinType.InnerJoin);
            criteria.Add(Restrictions.Eq("s3.Visit.VisitId", visit.VisitId));
            criteria.Add(Restrictions.Eq("VpGuarantorId", visit.VPGuarantor.VpGuarantorId));
            criteria.Add(Subqueries.PropertyIn("VpStatementId", subQuery));
            criteria.SetProjection(Projections.RowCount());

            return criteria.UniqueResult<int>() > 0;
        }*/

        public IList<VisitBalanceResult> GetVisitBalances(int vpGuarantorId, IList<int> visitIds)
        {
            Visit v = null;
            VisitBalanceResult result = null;

            IQueryOver<Visit, Visit> query = this.Session.QueryOver(() => v)
                .Where(() => v.VPGuarantor.VpGuarantorId == vpGuarantorId);

            if (visitIds != null)
            {
                query = query.Where(Restrictions.In(Projections.Property(() => v.VisitId), visitIds.ToList()));
            }

            IList<VisitBalanceResult> visitBalances = query
                .SelectList(x => x
                    .Select(() => v.VisitId).WithAlias(() => result.VisitId)
                    .Select(() => v.CurrentBalance).WithAlias(() => result.CurrentBalance)
                )
                .TransformUsing(Transformers.AliasToBean(typeof(VisitBalanceResult)))
                .List<VisitBalanceResult>();

            return visitBalances;
        }

        public IList<int> GetVisitsThatWereActiveForDateRange(int vpGuarantorId, DateTime startDate, DateTime endDate)
        {
            VisitStateHistory visitStateHistory = null;
            Visit visitAlias = null;
            List<int> activeStates = new List<int>() { (int)VisitStateEnum.Active };

            IList<int> results = this.Session.QueryOver(() => visitStateHistory)
                .JoinAlias(v => v.Entity, () => visitAlias)
                .Where(x => visitStateHistory.DateTime >= startDate)
                .Where(x => visitStateHistory.DateTime <= endDate)
                .Where(x => visitAlias.VPGuarantor.VpGuarantorId == vpGuarantorId)
                .Where(Restrictions.In(Projections.Property(() => (int)visitStateHistory.EvaluatedState), activeStates))
                .SelectList(x => x
                    .Select(() => visitAlias.VisitId)
                ).List<int>();
            return results;
        }

        private static void Filter(VisitFilter filter, IQueryOver<Visit, Visit> query, IApplicationSettingsService applicationSettings)
        {
            Visit visitAlias = null;

            bool isClientPortal = (applicationSettings.IsWebApplication.Value && applicationSettings.IsClientApplication.Value);

            //remove unmatched visits, or use the filter value in the Client portal
            if (applicationSettings.IsWebApplication.Value && !applicationSettings.IsClientApplication.Value)
            {
                List<int> allStatuses = Enum.GetValues(typeof(VisitStateEnum)).Cast<int>().ToList();
                if (filter.VisitStateIds == null || !filter.VisitStateIds.Any())
                {
                    filter.VisitStateIds = allStatuses;
                }

                query.Where(x => !x.IsUnmatched);
            }
            else if (filter.IsUnmatched.HasValue)
            {
                query.Where(x => x.IsUnmatched == filter.IsUnmatched.Value);
            }

            //Client portal is filtering by CurrentVisitStateDerivedEnum (derived property not in DB), so filtering by VisitStateId is not applicable
            if (!isClientPortal && filter.VisitStateIds != null && filter.VisitStateIds.Any())
            {
                query.Where(Restrictions.In(Projections.Property(() => visitAlias.CurrentVisitState), filter.VisitStateIds.ToList()));
            }

            if (!string.IsNullOrWhiteSpace(filter.BillingApplication))
            {
                query.Where(v => v.BillingApplication == filter.BillingApplication);
            }

            if (!string.IsNullOrWhiteSpace(filter.VisitDateRangeFrom))
            {
                query.Where(v => v.DischargeDate >= DateTime.Parse(filter.VisitDateRangeFrom));
            }

            if (!string.IsNullOrWhiteSpace(filter.VisitDateRangeTo))
            {
                query.Where(v => v.DischargeDate <= DateTime.Parse(filter.VisitDateRangeTo));
            }

            if (filter.VisitIds != null)
            {
                query.Where(Restrictions.In(Projections.Property(() => visitAlias.VisitId), filter.VisitIds.ToList()));
            }

            if (filter.BillingHold.HasValue)
            {
                query.Where(x => x.BillingHold == filter.BillingHold.Value);
            }

            if (filter.IsPastDue.HasValue || filter.AgingCountMin.HasValue || filter.AgingCountMax.HasValue || filter.IsMaxAge.HasValue)
            {
                VisitAgingHistory currentVisitAgingHistoryAlias = null;
                query.JoinAlias(v => v.CurrentVisitAgingHistory, () => currentVisitAgingHistoryAlias);

                if (filter.IsPastDue.HasValue)
                {
                    query.Where(() => currentVisitAgingHistoryAlias.AgingTier == AgingTierEnum.PastDue);
                }

                if (filter.IsMaxAge.HasValue)
                {
                    query.Where(() => currentVisitAgingHistoryAlias.AgingTier == AgingTierEnum.MaxAge);
                }

                if (filter.AgingCountMin.HasValue)
                {
                    query.Where(() => (int)currentVisitAgingHistoryAlias.AgingTier >= filter.AgingCountMin.Value);
                }

                if (filter.AgingCountMax.HasValue)
                {
                    query.Where(() => (int)currentVisitAgingHistoryAlias.AgingTier <= filter.AgingCountMax.Value);
                }

                if (filter.IsMaxAge.HasValue)
                {
                    query.Where(() => currentVisitAgingHistoryAlias.AgingTier == AgingTierEnum.MaxAge);
                }
            }

        }

        private static void Sort(VisitFilter filter, IQueryOver<Visit, Visit> query)
        {
            Visit visitAlias = null;

            bool isAscendingOrder = "asc".Equals(filter.SortOrder, StringComparison.InvariantCultureIgnoreCase);
            switch (filter.SortField ?? string.Empty)
            {
                case nameof(Visit.BillingApplication):
                case nameof(Visit.DischargeDate):
                case nameof(Visit.SourceSystemKey):
                case nameof(Visit.VisitDescription):
                case nameof(Visit.VisitId):
                case nameof(Visit.BillingHold):
                    {
                        query.UnderlyingCriteria.AddOrder(new Order(filter.SortField, isAscendingOrder));
                        break;
                    }
                case nameof(Visit.AgingTier):
                    {
                        VisitAgingHistory currentVisitAgingHistoryAlias = null;

                        query.JoinAlias(x => x.CurrentVisitAgingHistory, () => currentVisitAgingHistoryAlias);

                        SortOrder(isAscendingOrder, query.OrderBy(Projections.Conditional(
                            Restrictions.Eq(Projections.Property(() => visitAlias.IsUnmatched), true),
                            Projections.Constant(isAscendingOrder ? int.MaxValue : int.MinValue, NHibernateUtil.Int32),
                            Projections.Property(() => currentVisitAgingHistoryAlias.AgingTier)
                        )));
                        break;
                    }

                case nameof(Visit.CurrentBalance):
                    {
                        QueryOver<VisitBalanceHistory> visitBalanceHistorySubquery = QueryOver.Of<VisitBalanceHistory>()
                            .Where(x => x.Visit.VisitId == visitAlias.VisitId)
                            .OrderBy(x => x.InsertDate).Desc
                            .Select(x => x.CurrentBalance)
                            .Take(1);

                        SortOrder(isAscendingOrder, query.OrderBy(Projections.SubQuery(visitBalanceHistorySubquery.DetachedCriteria)));
                        break;
                    }

                case nameof(Visit.PatientName):
                    {
                        IQueryOverOrderBuilder<Visit, Visit> orderBy = query.OrderBy(() => visitAlias.PatientFirstName);
                        SortOrder(isAscendingOrder, orderBy);
                        orderBy = query.ThenBy(() => visitAlias.PatientLastName);
                        SortOrder(isAscendingOrder, orderBy);
                        break;
                    }

                case nameof(Visit.SourceSystemKeyDisplay):
                    {
                        SortOrder(isAscendingOrder, query.OrderBy(Projections.Conditional(
                            Restrictions.IsNull(Projections.Property(() => visitAlias.SourceSystemKeyDisplay)),
                            Projections.Property(() => visitAlias.SourceSystemKey),
                            Projections.Property(() => visitAlias.SourceSystemKeyDisplay))));
                        break;
                    }

                case nameof(Visit.TotalCharges):
                    {
                        SortOrder(isAscendingOrder, query.OrderBy(Projections.Conditional(
                            Restrictions.Eq(Projections.Property(() => visitAlias.IsUnmatched), true),
                            Projections.Constant(isAscendingOrder ? int.MaxValue : int.MinValue, NHibernateUtil.Decimal),
                            Projections.Property(() => visitAlias.TotalCharges)
                        )));
                        break;
                    }

                case "VisitStateFullDisplayName":
                case "StatusDisplay":
                    {
                        SortOrder(isAscendingOrder, query.OrderBy(() => Projections.Property(() => visitAlias.CurrentVisitState)));
                        break;
                    }
            }
        }

        private static void SortOrder(bool isAscendingOrder, IQueryOverOrderBuilder<Visit, Visit> orderBy)
        {
            if (isAscendingOrder)
            {
                orderBy.Asc();
            }
            else
            {
                orderBy.Desc();
            }
        }

        public void PublishMessagesAfterRollbackCallback()
        {
            this.Session.Transaction.RegisterPostCommitFailureAction((p1, p2, p3) =>
            {
                p1.SafeClear();
                p2.SafeClear();
                p3.SafeClear();
            }, this._outboundVisitMessages, this._visitChangedStatusMessages, this._visitChangedBalanceEvents);
        }

        private IList<VisitBalanceChangedMessage> TransformEventsToMessages(IEnumerable<VisitBalanceChangedEvent> visitBalanceChangedEvents)
        {
            return visitBalanceChangedEvents.Select(visitBalanceChangedEvent => new VisitBalanceChangedMessage
            {
                VpGuarantorId = visitBalanceChangedEvent.Visit.VPGuarantor.VpGuarantorId,
                VisitId = visitBalanceChangedEvent.Visit.VisitId
            }).ToList();
        }

        private void QueueUnpublishedMessages(Visit entity)
        {
            if (entity.OutboundVisitMessages.IsNotNullOrEmpty())
            {
                //only register the callback once.
                if (!this._outboundVisitMessages.Any())
                {
                    this.Session.Transaction.RegisterPostCommitSuccessAction((p1) =>
                    {
                        if (p1.IsNotNullOrEmpty())
                        {
                            this._bus.Value.PublishMessage(new OutboundVisitMessageList() { Messages = p1.ToList() }).Wait();
                            p1.Clear();
                        }
                    }, this._outboundVisitMessages);
                }
                this._outboundVisitMessages.AddRange(entity.OutboundVisitMessages);
                entity.OutboundVisitMessages.Clear();
            }

            if (entity.UnpublishedStatusChangeMessages.IsNotNullOrEmpty())
            {
                //only register the callback once.
                if (!this._visitChangedStatusMessages.Any())
                {
                    this.Session.Transaction.RegisterPostCommitSuccessAction((p1) =>
                    {
                        if (p1.IsNotNullOrEmpty())
                        {
                            this._bus.Value.PublishMessage(new VisitChangedStatusAggregatedMessages(p1)).Wait();
                            this._visitChangedStatusMessages.Clear();
                        }
                    }, this._visitChangedStatusMessages);
                }
                this._visitChangedStatusMessages.AddRange(entity.UnpublishedStatusChangeMessages);
                entity.UnpublishedStatusChangeMessages.Clear();
            }

            if (entity.UnpublishedVisitBalanceChanged.IsNotNullOrEmpty())
            {
                //only register the callback once.
                if (!this._visitChangedBalanceEvents.Any())
                {
                    this.Session.Transaction.RegisterPostCommitSuccessAction((p1) =>
                    {
                        if (p1.IsNotNullOrEmpty())
                        {
                            this._bus.Value.PublishMessage(new VisitBalanceChangedMessageList() { Messages = this.TransformEventsToMessages(p1).ToList() }).Wait();
                            this._visitChangedBalanceEvents.Clear();
                        }
                    }, this._visitChangedBalanceEvents);
                }
                this._visitChangedBalanceEvents.AddRange(entity.UnpublishedVisitBalanceChanged);
                entity.UnpublishedVisitBalanceChanged.Clear();
            }
        }

        public void Dispose()
        {
            base.CallBackAfterCommit = null;
        }

        #region visit unmatch search

        public VisitUnmatchResults GetAllVisitUnmatches(VisitUnmatchFilter filter, int pageNumber, int pageSize)
        {
            VisitUnmatchResults results = this.GetAllVisitUnmatchTotals(filter);

            results.VisitUnmatches = this.DoGetAllVisitUnmatches(filter, pageNumber, pageSize);

            return results;
        }

        private VisitUnmatchResults GetAllVisitUnmatchTotals(VisitUnmatchFilter filter)
        {
            int totalRecords = this.QueryOverVisitUnmatches(filter)
                .RowCount();

            return new VisitUnmatchResults
            {
                TotalRecords = totalRecords
            };
        }

        private IReadOnlyList<Visit> DoGetAllVisitUnmatches(VisitUnmatchFilter filter, int pageNumber, int pageSize)
        {
            IQueryOver<Visit, Visit> query = this.QueryOverVisitUnmatches(filter);
            SortVisitUnmatches(filter, query);

            IList<Visit> visits = query
                .Skip(pageNumber * pageSize)
                .Take(pageSize)
                .List();

            return (IReadOnlyList<Visit>)visits;
        }

        private IQueryOver<Visit, Visit> QueryOverVisitUnmatches(VisitUnmatchFilter filter)
        {
            List<int> filterReasons = new List<HsGuarantorUnmatchReasonEnum>
            {
                HsGuarantorUnmatchReasonEnum.ClientUnmatchedGuarantorManual,
                HsGuarantorUnmatchReasonEnum.ClientUnmatchedMatchCriteriaChanged,
                HsGuarantorUnmatchReasonEnum.ClientUnmatchedVisitManual,
                HsGuarantorUnmatchReasonEnum.SystemUnmatchedGuarantorTypeChange,
                HsGuarantorUnmatchReasonEnum.SystemUnmatchedHSGIDUpdated,
            }.Select(x => (int)x).ToList();

            Visit visitAlias = null;
            HsGuarantorMap hsGuarantorAlias = null;
            HsGuarantorUnmatchReason unmatchReasonAlias = null;
            VisitPayUser actionVisitPayUserAlias = null;

            IQueryOver<Visit, Visit> query = this.Session.QueryOver(() => visitAlias)
                .JoinAlias(hsg => visitAlias.HsGuarantorMap, () => hsGuarantorAlias)
                .JoinAlias(umr => visitAlias.HsGuarantorUnmatchReason, () => unmatchReasonAlias)
                .JoinAlias(avp => visitAlias.UnmatchActionVisitPayUser, () => actionVisitPayUserAlias)
                .Where(Restrictions.In(Projections.Property(() => unmatchReasonAlias.HsGuarantorUnmatchReasonId), filterReasons));

            FilterVisitUnmatches(filter, query);

            return query;
        }

        private static void FilterVisitUnmatches(VisitUnmatchFilter filter, IQueryOver<Visit, Visit> query)
        {
            Visit visitAlias = null;
            HsGuarantorMap hsGuarantorAlias = null;

            query.If(!string.IsNullOrEmpty(filter.MatchedSourceSystemKey) && !string.IsNullOrWhiteSpace(filter.MatchedSourceSystemKey), q => q.Where(() => visitAlias.MatchedSourceSystemKey == filter.MatchedSourceSystemKey));
            query.If(!string.IsNullOrEmpty(filter.HsGuarantorSourceSystemKey) && !string.IsNullOrWhiteSpace(filter.HsGuarantorSourceSystemKey), q => q.Where(() => hsGuarantorAlias.SourceSystemKey == filter.HsGuarantorSourceSystemKey));
            query.If(filter.VpGuarantorId.HasValue, q => q.Where(() => visitAlias.VPGuarantor.VpGuarantorId == filter.VpGuarantorId));
            query.If(filter.DateRangeFrom.HasValue, q =>
            {
                DateTime date = DateTime.UtcNow.AddDays(filter.DateRangeFrom.GetValueOrDefault(-30)).Date;
                query.Where(x => x.UnmatchActionDate >= date);
            });
            query.If(filter.SpecificDateFrom.HasValue, q =>
            {
                DateTime date = filter.SpecificDateFrom.GetValueOrDefault(DateTime.UtcNow).AddDays(-1).Date;
                query.Where(Restrictions.Gt(Projections.Cast(NHibernateUtil.Date, Projections.Property(() => visitAlias.UnmatchActionDate)), date));
            });
            query.If(filter.SpecificDateTo.HasValue, q =>
            {
                DateTime date = filter.SpecificDateTo.GetValueOrDefault(DateTime.UtcNow).AddDays(1).Date;
                query.Where(Restrictions.Lt(Projections.Cast(NHibernateUtil.Date, Projections.Property(() => visitAlias.UnmatchActionDate)), date));
            });
        }

        private static void SortVisitUnmatches(VisitUnmatchFilter filter, IQueryOver query)
        {
            Visit visitAlias = null;
            HsGuarantorUnmatchReason unmatchReasonAlias = null;
            HsGuarantorMap hsGuarantorAlias = null;

            bool isAscendingOrder = "asc".Equals(filter.SortOrder, StringComparison.InvariantCultureIgnoreCase);

            switch (filter.SortField)
            {
                case "MatchedSourceSystemKey":
                    query.UnderlyingCriteria.AddOrder(new Order(Projections.Property(() => visitAlias.MatchedSourceSystemKey), isAscendingOrder));
                    break;
                case "HsGuarantorSourceSystemKey":
                    query.UnderlyingCriteria.AddOrder(new Order(Projections.Property(() => hsGuarantorAlias.SourceSystemKey), isAscendingOrder));
                    break;
                case "VpGuarantorId":
                    query.UnderlyingCriteria.AddOrder(new Order(Projections.Property(() => visitAlias.VPGuarantor.VpGuarantorId), isAscendingOrder));
                    break;
                case "HsGuarantorUnmatchReasonDisplayName":
                    query.UnderlyingCriteria.AddOrder(new Order(Projections.Property(() => unmatchReasonAlias.HsGuarantorUnmatchReasonDisplayName), isAscendingOrder));
                    break;
                case "UnmatchActionDate":
                    query.UnderlyingCriteria.AddOrder(new Order(Projections.Property(() => visitAlias.UnmatchActionDate), isAscendingOrder));
                    query.UnderlyingCriteria.AddOrder(new Order(Projections.Property(() => visitAlias.MatchedSourceSystemKey), isAscendingOrder));
                    break;
                case "UnmatchActionUserName":
                    query.UnderlyingCriteria.AddOrder(new Order(Projections.Property(() => visitAlias.UnmatchActionVisitPayUser), isAscendingOrder));
                    break;
            }
        }

        #endregion

        protected override int GetId(Visit entity)
        {
            return entity.VisitId;
        }
    }
}
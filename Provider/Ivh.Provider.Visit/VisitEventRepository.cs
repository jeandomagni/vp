﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ivh.Provider.Visit
{
    using Common.Data.Connection.Connections;
    using Common.Data.Interfaces;
    using Domain.Visit.Entities;
    using Domain.Visit.Interfaces;
    using Ivh.Common.Data;
    using NHibernate;

    public class VisitEventRepository : RepositoryBase<VisitEvent, VisitPay>, IVisitEventRepository
    {
        public VisitEventRepository(ISessionContext<VisitPay> sessionContext, IStatelessSessionContext<VisitPay> statelessSessionContext)
            : base(sessionContext, statelessSessionContext)
        {
        }

        public IList<VisitEvent> GetEventsForVisit(int vpVisitId)
        {
            IList<VisitEvent> outboundMessages = this.Session
                .QueryOver<VisitEvent>()
                .Where(outboundMessage => outboundMessage.VisitId == vpVisitId)
                .OrderBy(outboundMessage => outboundMessage.ActionDate).Desc
                .List();

            return outboundMessages;
        }
        
        public void Add(VisitEvent message)
        {
            base.InsertOrUpdate(message);
        }
    }
}

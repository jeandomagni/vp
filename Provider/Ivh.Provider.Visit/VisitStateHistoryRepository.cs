﻿namespace Ivh.Provider.Visit
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Common.Data;
    using Common.Data.Connection.Connections;
    using Common.Data.Interfaces;
    using Common.VisitPay.Enums;
    using Domain.Visit.Entities;
    using Domain.Visit.Interfaces;
    using Ivh.Common.Base.Utilities.Extensions;
    using NHibernate;
    using NHibernate.Criterion;

    public class VisitStateHistoryRepository : RepositoryBase<VisitStateHistory, VisitPay>, IVisitStateHistoryRepository
    {

        public VisitStateHistoryRepository(ISessionContext<VisitPay> sessionContext)
            : base(sessionContext)
        {
        }

        public VisitStateHistoryResults GetAllVisitStateHistory(int vpGuarantorId, VisitStateHistoryFilter filter, int batch, int batchSize, bool onlyChanges = false)
        {
            VisitStateHistoryResults visitTransactionResults = this.GetVisitStateHistoryTotals(vpGuarantorId, filter, onlyChanges);

            visitTransactionResults.VisitStateHistories = this.DoGetVisitStateHistory(vpGuarantorId, filter, batch, batchSize, onlyChanges);

            return visitTransactionResults;
        }

        private IReadOnlyList<VisitStateHistory> DoGetVisitStateHistory(int vpGuarantorId, VisitStateHistoryFilter filter, int batch, int batchSize, bool onlyChanges = false)
        {
            IList<VisitStateHistory> visitStateHistoryList = null;

            if (onlyChanges)
            {
                visitStateHistoryList = this.GetOnlyChangedVisitStateHistoryList(vpGuarantorId, filter);

                // Apply grid paging filter that was passed in
                visitStateHistoryList = visitStateHistoryList
                    .Skip(batch * batchSize)
                    .Take(batchSize)
                    .ToList();
            }
            else
            {
                IQueryOver<VisitStateHistory, VisitStateHistory> query = this.QueryOverVisitStateHistory(vpGuarantorId, filter, onlyChanges: false);
                Sort(filter, query);
                visitStateHistoryList = query
                    .Skip(batch * batchSize)
                    .Take(batchSize)
                    .List();
            }

            return (IReadOnlyList<VisitStateHistory>)visitStateHistoryList;
        }

        private VisitStateHistoryResults GetVisitStateHistoryTotals(int vpGuarantorId, VisitStateHistoryFilter filter, bool onlyChanges = false)
        {
            int totalRecords;

            if (onlyChanges)
            {
                totalRecords = this
                    .GetOnlyChangedVisitStateHistoryList(vpGuarantorId, filter)
                    .Count();
            }
            else
            {
                totalRecords = this
                    .QueryOverVisitStateHistory(vpGuarantorId, filter, onlyChanges: false)
                    .RowCount();
            }

            return new VisitStateHistoryResults
            {
                TotalRecords = totalRecords
            };
        }

        private IQueryOver<VisitStateHistory, VisitStateHistory> QueryOverVisitStateHistory(int vpGuarantorId, VisitStateHistoryFilter filter, bool onlyChanges = false)
        {
            Visit visitAlias = null;
            VisitStateHistory visitStateHistoryAlias = null;

            IQueryOver<VisitStateHistory, VisitStateHistory> query = this.Session
                                                                           .QueryOver<VisitStateHistory>(() => visitStateHistoryAlias)
                                                                           .JoinAlias(v => v.Entity, () => visitAlias)
                                                                           .Where(x => visitAlias.VPGuarantor.VpGuarantorId == vpGuarantorId);

            this.Filter(filter, query, onlyChanges);

            return query;
        }

        private void Filter(VisitStateHistoryFilter filter, IQueryOver<VisitStateHistory, VisitStateHistory> query, bool onlyChanges = false)
        {
            Visit visitAlias = null;

            query.Where(q => visitAlias.VisitId == filter.VisitId)
                 .If(!onlyChanges && filter.VisitStates.Any(), q => q.Where(x => x.EvaluatedState.IsIn(filter.VisitStates.ToList())));
        }

        private static void Sort(VisitStateHistoryFilter filter, IQueryOver<VisitStateHistory, VisitStateHistory> query)
        {
            if (filter.SortField.IsNullOrEmpty())
                return;

            VisitStateHistory visitStateHistoryAlias = null;

            bool isAscendingOrder = "asc".Equals(filter.SortOrder, StringComparison.InvariantCultureIgnoreCase);

            switch (filter.SortField)
            {
                case "ChangeDescription":
                    query.UnderlyingCriteria.AddOrder(new Order(Projections.Property(() => visitStateHistoryAlias.RuleName), isAscendingOrder));
                    break;
                case "InsertDate":
                    query.UnderlyingCriteria.AddOrder(new Order(Projections.Property(() => visitStateHistoryAlias.DateTime), isAscendingOrder));
                    break;
                case "VisitStateFullDisplayName":
                    query.UnderlyingCriteria.AddOrder(new Order(Projections.Property(() => visitStateHistoryAlias.EvaluatedState), isAscendingOrder));
                    break;

            }
        }

        #region Only Changes     

        private IList<VisitStateHistory> GetOnlyChangedVisitStateHistoryList(int vpGuarantorId, VisitStateHistoryFilter filter)
        {
            IQueryOver<VisitStateHistory, VisitStateHistory> query = this.QueryOverVisitStateHistory(vpGuarantorId, filter, onlyChanges: true);

            IList<VisitStateHistory> visitStateHistoryList = new List<VisitStateHistory>();

            //Get all rows ordered by DateTime ascending
            IOrderedEnumerable<VisitStateHistory> allVisitStateHistoryOrdered = query.List().OrderBy(x => x.DateTime);

            //Iterate over rows and only add rows that do not match the previous EvaluatedState
            VisitStateEnum lastValue = VisitStateEnum.NotSet;
            foreach (VisitStateHistory visitStateHistory in allVisitStateHistoryOrdered)
            {
                if (lastValue != visitStateHistory.EvaluatedState)
                {
                    visitStateHistoryList.Add(visitStateHistory);
                }
                lastValue = visitStateHistory.EvaluatedState;
            }

            //Filter first, then sort rows from the built up list above
            visitStateHistoryList = this.FilterOnlyChanges(filter, visitStateHistoryList);
            visitStateHistoryList = this.SortOnlyChanges(filter, visitStateHistoryList);

            return visitStateHistoryList;
        }

        private IList<VisitStateHistory> FilterOnlyChanges(VisitStateHistoryFilter filter, IList<VisitStateHistory> visitStateHistoryList)
        {
            if (filter.VisitStates.Any())
            {
                IList<VisitStateHistory> filteredList = visitStateHistoryList.Where(x => filter.VisitStates.Contains(x.EvaluatedState)).ToList();
                return filteredList;
            }

            return visitStateHistoryList;
        }

        private IList<VisitStateHistory> SortOnlyChanges(VisitStateHistoryFilter filter, IList<VisitStateHistory> visitStateHistoryList)
        {
            if (filter.SortField.IsNullOrEmpty())
                return visitStateHistoryList;

            bool isAscendingOrder = "asc".Equals(filter.SortOrder, StringComparison.InvariantCultureIgnoreCase);

            IEnumerable<VisitStateHistory> query = visitStateHistoryList.Select(x => x);

            switch (filter.SortField)
            {
                case "ChangeDescription":
                    if (isAscendingOrder)
                    {
                        query = query.OrderBy(x => x.RuleName);
                    }
                    else
                    {
                        query = query.OrderByDescending(x => x.RuleName);
                    }
                    break;
                case "InsertDate":
                    if (isAscendingOrder)
                    {
                        query = query.OrderBy(x => x.DateTime);
                    }
                    else
                    {
                        query = query.OrderByDescending(x => x.DateTime);
                    }
                    break;
                case "VisitStateFullDisplayName":
                    if (isAscendingOrder)
                    {
                        query = query.OrderBy(x => x.EvaluatedState);
                    }
                    else
                    {
                        query = query.OrderByDescending(x => x.EvaluatedState);
                    }
                    break;
            }

            visitStateHistoryList = query.ToList();

            return visitStateHistoryList;
        }

        #endregion
    }
}

namespace Ivh.Provider.Visit
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Common.Base.Utilities.Extensions;
    using Common.Cache;
    using Common.Data;
    using Common.Data.Connection.Connections;
    using Common.Data.Interfaces;
    using Common.VisitPay.Enums;
    using Common.VisitPay.Utilities;
    using Domain.Visit.Entities;
    using Domain.Visit.Interfaces;
    using NHibernate;
    using NHibernate.Criterion;

    public class VisitTransactionRepository : RepositoryCacheBase<VisitTransaction, VisitPay>, IVisitTransactionRepository, IVisitTransactionWriteRepository
    {
        private const int BatchSizeForSelect = 500;

        public VisitTransactionRepository(
            ISessionContext<VisitPay> sessionContext, 
            IStatelessSessionContext<VisitPay> statelessSessionContext, 
            Lazy<IDistributedCache> cache)
            : base(
                sessionContext, 
                statelessSessionContext, 
                cache)
        {
        }

        public IReadOnlyList<string> GetTransactionTypesAsStrings()
        {
            return (IReadOnlyList<string>)this.Session
                .QueryOver<VpTransactionType>()
                .OrderBy(vp => vp.TransactionType).Asc
                .Select(
                    Projections.Distinct(
                        Projections.Property<VpTransactionType>(vp => vp.TransactionType)
                        )
                )
                .List<string>();
        }

        public VisitTransactionResults GetTransactions(int vpGuarantorId, VisitTransactionFilter filter, int batch, int batchSize)
        {
            VisitTransactionResults visitTransactionResults = new VisitTransactionResults
            {
                TotalRecords = this.GetVisitTransactionTotals(vpGuarantorId, filter),
                VisitTransactions = this.DoGetTransactions(vpGuarantorId, filter, batch, batchSize).ToList()
            };

            return visitTransactionResults;
        }

        public VisitTransaction GetBySourceSystemKey(string sourceSystemKey, int billingSystemId)
        {
            if (billingSystemId < 0 || sourceSystemKey == null)
            {
                return null;
            }

            return this.GetCached(
                x => x.BillingSystem.BillingSystemId, billingSystemId,
                x => x.SourceSystemKey, sourceSystemKey,
                (b, s) =>
                {
                    ICriteria criteria = this.Session.CreateCriteria(typeof(VisitTransaction));
                    criteria.Add(Restrictions.Eq("SourceSystemKey", s));
                    criteria.Add(Restrictions.Eq("BillingSystem.BillingSystemId", b));
                    return criteria.UniqueResult<VisitTransaction>();
                });
        }

        public IList<VisitTransaction> GetBySourceSystemKey(IList<string> sourceSystemKeys, int billingSystemId)
        {
            if (billingSystemId < 0)
            {
                return new List<VisitTransaction>();
            }

            sourceSystemKeys = sourceSystemKeys?.Where(x => x != null).ToList();
            if (sourceSystemKeys == null || !sourceSystemKeys.Any())
            {
                return new List<VisitTransaction>();
            }

            List<VisitTransaction> visitTransactions = new List<VisitTransaction>();

            int currentCount = 0;
            int loopCounter = 0;
            while (sourceSystemKeys.Count > currentCount)
            {
                List<string> keys = sourceSystemKeys.Skip(currentCount).Take(BatchSizeForSelect).ToList();
                
                VisitTransaction alias = null;
                IList<VisitTransaction> results = this.Session.QueryOver(() => alias)
                    .Where(Restrictions.In(Projections.Property(() => alias.SourceSystemKey), keys))
                    .Where(Restrictions.Eq(Projections.Property(() => alias.BillingSystem.BillingSystemId), billingSystemId))
                    .List();

                if (results.IsNotNullOrEmpty())
                {
                    visitTransactions.AddRange(results);
                }

                currentCount += BatchSizeForSelect;
                loopCounter += 1;
                if (loopCounter > 30)
                {
                    //Never want infinite loops, setting this hard upper bound to 15k transactions (500 * 30)
                    break;
                }
            }

            return visitTransactions;
        }

        public IList<VisitTransaction> GetBySourceSystemKeyUnmatch(IList<string> sourceSystemKeys, int billingSystemId)
        {
            sourceSystemKeys = sourceSystemKeys?.Where(x => x != null).ToList();
            if (sourceSystemKeys == null || !sourceSystemKeys.Any())
            {
                return new List<VisitTransaction>();
            }

            List<VisitTransaction> visitTransactions = new List<VisitTransaction>();

            int currentCount = 0;
            int loopCounter = 0;
            while (sourceSystemKeys.Count > currentCount)
            {
                List<string> keys = sourceSystemKeys.Skip(currentCount).Take(BatchSizeForSelect).ToList();
                
                VisitTransaction alias = null;
                IList<VisitTransaction> results = this.Session.QueryOver(() => alias)
                    .Where(Restrictions.In(Projections.Property(() => alias.MatchedSourceSystemKey), keys))
                    .Where(Restrictions.Eq(Projections.Property(() => alias.MatchedBillingSystem.BillingSystemId), billingSystemId))
                    .List();

                if (results.IsNotNullOrEmpty())
                {
                    visitTransactions.AddRange(results);
                }

                currentCount += BatchSizeForSelect;
                loopCounter += 1;
                if (loopCounter > 30)
                {
                    //Never want infinite loops, setting this hard upper bound to 15k transactions (500 * 30)
                    break;
                }
            }

            return visitTransactions;
        }
        
        private static readonly CacheObject<IList<VpTransactionType>> VpTransactionTypes = new CacheObject<IList<VpTransactionType>>(TimeSpan.FromHours(1));
        public IList<VpTransactionType> GetTransactionTypes()
        {
            IList<VpTransactionType> Factory()
            {
                return this.StatelessSession.QueryOver<VpTransactionType>().List();
            }

            IList<VpTransactionType> list = VpTransactionTypes.Get(Factory);
            return list;
        }
        
        public decimal GetSumOfAllInterestAssessedForDateRange(int vpGuarantorId, DateTime startDate, DateTime endDate)
        {
            ICriteria criteria = this.Session.CreateCriteria<VisitTransaction>("vt")
                .CreateAlias("vt.VisitTransactionAmount", "amount");

            criteria.Add(Restrictions.Eq("vt.VpGuarantorId", vpGuarantorId));
            criteria.Add(Restrictions.Ge("vt.InsertDate", startDate));
            criteria.Add(Restrictions.Le("vt.InsertDate", endDate));
            criteria.Add(Restrictions.In("vt.VpTransactionType.VpTransactionTypeId", new List<VpTransactionTypeEnum>()
            {
                VpTransactionTypeEnum.VppInterestCharge,
                VpTransactionTypeEnum.VppInterestWriteoff,
            }))
            .SetProjection(Projections.Sum("amount.TransactionAmount"));

            return criteria.UniqueResult<decimal>();
        }

        public IList<VisitTransaction> GetVisitTransactions(int vpGuarantorId, int? visitId = null)
        {
            IQueryOver<VisitTransaction, VisitTransaction> query = this.Session
                .QueryOver<VisitTransaction>()
                .Where(x => x.VpGuarantorId == vpGuarantorId);
            if (visitId.HasValue)
            {
                query.And(x => x.Visit.VisitId == visitId.Value);
            }

            return query.List();
        }

        public int GetVisitTransactionTotals(int vpGuarantorId, VisitTransactionFilter filter)
        {
            return this
                .QueryOverVisitTransactions(vpGuarantorId, filter)
                .RowCount();
        }

        private IReadOnlyList<VisitTransaction> DoGetTransactions(int vpGuarantorId, VisitTransactionFilter filter, int batch, int batchSize)
        {
            IQueryOver<VisitTransaction, VisitTransaction> query = this.QueryOverVisitTransactions(vpGuarantorId, filter);
            Sort(filter, query);

            IList<VisitTransaction> visitTransactions = query
                .Skip(batch * batchSize)
                .Take(batchSize)
                .List();

            return (IReadOnlyList<VisitTransaction>)visitTransactions;
        }

        private IQueryOver<VisitTransaction, VisitTransaction> QueryOverVisitTransactions(int vpGuarantorId, VisitTransactionFilter filter)
        {
            Visit visitAlias = null;
            VpTransactionType vpTransactionTypeAlias = null;

            IQueryOver<VisitTransaction, VisitTransaction> query = this.Session
                .QueryOver<VisitTransaction>()
                .JoinAlias(vt => vt.VpTransactionType, () => vpTransactionTypeAlias)
                .JoinAlias(v => v.Visit, () => visitAlias)
                .Where(vp => vp.VpGuarantorId == vpGuarantorId);

            Filter(filter, query);

            return query;
        }

        private static void Filter(VisitTransactionFilter filter, IQueryOver<VisitTransaction, VisitTransaction> query)
        {
            //
            Visit visitAlias = null;
            VpTransactionType vpTransactionTypeAlias = null;

            List<int> vpTransactionTypes = new List<int>();
            if (filter.TransactionTypes != null && filter.TransactionTypes.Any())
            {
                vpTransactionTypes = filter.TransactionTypes.Cast<int>().ToList();
            }

            //
            query.If(!string.IsNullOrWhiteSpace(filter.TransactionDateRangeFrom), q => q.Where(vp => vp.TransactionDate >= DateTime.Parse(filter.TransactionDateRangeFrom)))
                .If(!string.IsNullOrWhiteSpace(filter.TransactionDateRangeTo), q => q.Where(vp => vp.TransactionDate <= DateTime.Parse(filter.TransactionDateRangeTo)))
                .If(vpTransactionTypes.Count > 0, q => q.Where(Restrictions.In(Projections.Property<VpTransactionType>(x => vpTransactionTypeAlias.VpTransactionTypeId), vpTransactionTypes)))
                .If(filter.VisitId.HasValue, q => q.Where(() => visitAlias.VisitId == filter.VisitId))
                .If(filter.HideOffsettingVisitTransactions, q => q.Where(x => !x.CanIgnore));
        }

        private static void Sort(VisitTransactionFilter filter, IQueryOver<VisitTransaction, VisitTransaction> query)
        {
            if (string.IsNullOrEmpty(filter.SortField))
            {
                return;
            }

            Visit visitAlias = null;
            VpTransactionType vpTransactionTypeAlias = null;

            bool isAscendingOrder = "asc".Equals(filter.SortOrder, StringComparison.InvariantCultureIgnoreCase);

            switch (filter.SortField.ToUpper())
            {
                case "DISPLAYDATE":
                    query.UnderlyingCriteria.AddOrder(new Order(
                        Projections.Conditional(
                            Restrictions.Eq(Projections.Property(() => vpTransactionTypeAlias.TransactionType), "Charge"),
                            Projections.Property<VisitTransaction>(x => x.TransactionDate),
                            Projections.Property<VisitTransaction>(x => x.PostDate)
                        ),
                        isAscendingOrder));
                    break;
                case "POSTDATE":
                case "TRANSACTIONDATE":
                case "TRANSACTIONDESCRIPTION":
                case "VISITTRANSACTIONID":
                    query.UnderlyingCriteria.AddOrder(new Order(filter.SortField, isAscendingOrder));
                    break;

                case "TRANSACTIONTYPE":
                    query.UnderlyingCriteria.AddOrder(new Order(Projections.Property(() => vpTransactionTypeAlias.TransactionType), isAscendingOrder));
                    break;

                case "PATIENTNAME":
                    query.UnderlyingCriteria.AddOrder(new Order(Projections.Property(() => visitAlias.PatientLastName), isAscendingOrder));
                    query.UnderlyingCriteria.AddOrder(new Order(Projections.Property(() => visitAlias.PatientFirstName), isAscendingOrder));
                    break;

                case "VISITSOURCESYSTEMKEY":
                    query.UnderlyingCriteria.AddOrder(new Order(Projections.Property(() => visitAlias.SourceSystemKey), isAscendingOrder));
                    break;

                case "TRANSACTIONAMOUNT":

                    int removedValue = isAscendingOrder ? int.MaxValue : int.MinValue;

                    VisitTransactionAmount visitTransactionAmountAlias = null;
                    query.JoinAlias(t => t.VisitTransactionAmount, () => visitTransactionAmountAlias);
                    query.UnderlyingCriteria.AddOrder(new Order(
                        Projections.Conditional(
                            Restrictions.Eq(Projections.Property(() => visitAlias.IsUnmatched), true),
                            Projections.Constant(removedValue, NHibernateUtil.Decimal),
                            Projections.Property(() => visitTransactionAmountAlias.TransactionAmount)
                        ),
                        isAscendingOrder));

                    break;

                case "VISITSTATUSFULLDISPLAYNAME":
                    query.UnderlyingCriteria.AddOrder(new Order(Projections.Property(() => visitAlias.CurrentVisitState), isAscendingOrder));
                    break;
            }
        }

        protected override int GetId(VisitTransaction entity)
        {
            return entity.VisitTransactionId;
        }
    }
}

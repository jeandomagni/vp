﻿namespace Ivh.Provider.Visit
{
    using Common.Data;
    using Common.Data.Connection.Connections;
    using Common.Data.Interfaces;
    using Domain.Visit.Entities;
    using Domain.Visit.Interfaces;

    public class PrimaryInsuranceTypeRepository : RepositoryBase<PrimaryInsuranceType, VisitPay>, IPrimaryInsuranceTypeRepository
    {
        public PrimaryInsuranceTypeRepository(ISessionContext<VisitPay> sessionContext) : base(sessionContext)
        {
        }
    }
}
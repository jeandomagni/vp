﻿namespace Ivh.Provider.Visit
{
    using System;
    using System.Collections.Generic;
    using Common.Base.Utilities.Extensions;
    using Common.Data;
    using Common.Data.Connection.Connections;
    using Common.Data.Interfaces;
    using Domain.Visit.Entities;
    using Domain.Visit.Interfaces;
    using NHibernate;
    using NHibernate.Criterion;

    public class VisitAgingHistoryRepository : RepositoryBase<VisitAgingHistory, VisitPay>, IVisitAgingHistoryRepository
    {
        public VisitAgingHistoryRepository(ISessionContext<VisitPay> sessionContext)
            : base(sessionContext)
        {
        }

        public VisitAgingHistoryResults GetAllVisitAgingHistory(int vpGuarantorId, VisitAgingHistoryFilter filter, int batch, int batchSize)
        {
            VisitAgingHistoryResults visitAgingResults = this.GetVisitAgingHistoryTotals(vpGuarantorId, filter);

            visitAgingResults.VisitAgingHistories = this.DoGetVisitAgingHistory(vpGuarantorId, filter, batch, batchSize);

            return visitAgingResults;
        }

        private IReadOnlyList<VisitAgingHistory> DoGetVisitAgingHistory(int vpGuarantorId, VisitAgingHistoryFilter filter, int batch, int batchSize)
        {
            IQueryOver<VisitAgingHistory, VisitAgingHistory> query = this.QueryOverVisitAgingHistory(vpGuarantorId, filter);
            Sort(filter, query);

            IList<VisitAgingHistory> visitAgingHistory = query
                .Skip(batch * batchSize)
                .Take(batchSize)
                .List();

            return (IReadOnlyList<VisitAgingHistory>)visitAgingHistory;
        }

        private VisitAgingHistoryResults GetVisitAgingHistoryTotals(int vpGuarantorId, VisitAgingHistoryFilter filter)
        {
            int totalRecords = this
                .QueryOverVisitAgingHistory(vpGuarantorId, filter)
                .RowCount();

            return new VisitAgingHistoryResults
            {
                TotalRecords = totalRecords
            };
        }

        private IQueryOver<VisitAgingHistory, VisitAgingHistory> QueryOverVisitAgingHistory(int vpGuarantorId, VisitAgingHistoryFilter filter)
        {
            IQueryOver<VisitAgingHistory, VisitAgingHistory> query = this.Session
                .QueryOver<VisitAgingHistory>()
                .Where(vah => vah.Visit.VisitId == filter.VisitId);

            Filter(filter, query);

            return query;
        }

        private static void Filter(VisitAgingHistoryFilter filter, IQueryOver<VisitAgingHistory, VisitAgingHistory> query)
        {
            query.Where(q => q.Visit.VisitId == filter.VisitId);

        }

        private static void Sort(VisitAgingHistoryFilter filter, IQueryOver<VisitAgingHistory, VisitAgingHistory> query)
        {
            if (filter.SortField.IsNullOrEmpty())
                return;

            bool isAscendingOrder = "asc".Equals(filter.SortOrder, StringComparison.InvariantCultureIgnoreCase);

            switch (filter.SortField)
            {
                default:
                case "InsertDate":
                    query.UnderlyingCriteria.AddOrder(new Order(filter.SortField, isAscendingOrder));
                    break;
            }
        }
    }
}

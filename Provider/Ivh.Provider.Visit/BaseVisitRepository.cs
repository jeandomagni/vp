﻿namespace Ivh.Provider.Visit
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Common.Cache;
    using Common.Data;
    using Common.Data.Connection.Connections;
    using Common.Data.Interfaces;
    using Domain.Visit.Entities;
    using Domain.Visit.Interfaces;

    public class BaseVisitRepository : RepositoryBase<BaseVisit, VisitPay> , IBaseVisitRepository
    {
        public BaseVisitRepository(IStatelessSessionContext<VisitPay> statelessSessionContext, Lazy<IDistributedCache> cache = null) : base(statelessSessionContext, cache)
        {
        }

        public IList<BaseVisit> GetPaymentSummaryVisits(IList<int> hsGuarantorIds)
        {
            IList<BaseVisit> paymentSummaryVisits = this.StatelessSession.QueryOver<BaseVisit>()
                .WhereRestrictionOn(x => x.HsGuarantorId).IsIn(hsGuarantorIds.ToList())
                .List();

            return paymentSummaryVisits;
        }

        public BaseVisit GetVisit(string visitSourceSystemKey, int visitBillingSystemId)
        {
            BaseVisit visit = this.StatelessSession.QueryOver<BaseVisit>()
                .WhereStringEq(x => x.SourceSystemKey, visitSourceSystemKey)
                .Where(x => x.BillingSystemId == visitBillingSystemId)
                .SingleOrDefault();

            return visit;
        }
    }
}

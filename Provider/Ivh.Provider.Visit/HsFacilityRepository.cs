﻿namespace Ivh.Provider.Visit
{
    using Common.Data;
    using Common.Data.Connection.Connections;
    using Common.Data.Interfaces;
    using Domain.Visit.Entities;
    using Domain.Visit.Interfaces;
    using NHibernate;

    public class HsFacilityRepository : RepositoryBase<HsFacility, VisitPay>, IHsFacilityRepository
    {
        public HsFacilityRepository(ISessionContext<VisitPay> sessionContext)
            : base(sessionContext)
        {
        }
    }
}
﻿namespace Ivh.Provider.Visit
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Common.Base.Enums;
    using Common.Cache;
    using Common.Data;
    using Common.Data.Connection.Connections;
    using Common.Data.Interfaces;
    using Common.VisitPay.Enums;
    using Domain.Visit.Entities;
    using Domain.Visit.Interfaces;
    using NHibernate;
    using NHibernate.Criterion;
    using NHibernate.Linq;

    public class InsurancePlanRepository : RepositoryCacheBase<InsurancePlan, VisitPay>, IInsurancePlanRepository
    {
        public InsurancePlanRepository(ISessionContext<VisitPay> sessionContext, Lazy<IDistributedCache> cache) : base(sessionContext, cache)
        {
        }

        public IReadOnlyList<VisitInsurancePlan> GetInsurancePlansForVisits(IList<VisitStateEnum> visitStates, int vpGuarantorId)
        {
            DetachedCriteria dcVisits = DetachedCriteria.For<Visit>("visit")
                .Add(Restrictions.Eq("VPGuarantor.VpGuarantorId", vpGuarantorId))
                .Add(Restrictions.In(Projections.Property<Visit>(x => x.CurrentVisitState), visitStates.ToList()));

            dcVisits.SetProjection(Projections.Property("visit.VisitId"));

            IList<VisitInsurancePlan> list = this.Session.CreateCriteria<VisitInsurancePlan>()
                .Add(Subqueries.PropertyIn("Visit.VisitId", dcVisits))
                .List<VisitInsurancePlan>();

            return (IReadOnlyList<VisitInsurancePlan>)list;
        }

        public override IQueryable<InsurancePlan> GetQueryable()
        {
            return base.GetQueryable().WithOptions(x =>
            {
                x.SetCacheable(true);
                x.SetCacheMode(CacheMode.Normal);
            });
        }

        public InsurancePlan GetInsurancePlan(string sourceSystemKey, int billingSystemId)
        {
            return this.GetCached(
                x => x.BillingSystemId, billingSystemId,
                x => x.SourceSystemKey, sourceSystemKey,
                (b, s) =>
                {
                    return this.Session.QueryOver<InsurancePlan>()
                        .Where(x => x.SourceSystemKey == s && x.BillingSystemId == b)
                        .Cacheable()
                        .CacheMode(CacheMode.Normal)
                        .Take(1).SingleOrDefault();
                });
        }

        protected override int GetId(InsurancePlan entity)
        {
            return entity.InsurancePlanId;
        }
    }
}
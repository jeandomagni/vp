﻿
namespace Ivh.Provider.Visit
{
    using System.Collections.Generic;
    using System.Linq;
    using Common.Base.Utilities.Extensions;
    using Common.Data;
    using Common.Data.Connection.Connections;
    using Common.Data.Interfaces;
    using Domain.Guarantor.Entities;
    using Domain.Visit.Interfaces;
    using NHibernate;
    using NHibernate.Transform;

    public class VisitPayUserIssueResultRepository : RepositoryBase<IssueResult, VisitPay>, IVisitPayUserIssueResultRepository
    {
        private readonly ISession _cdiSession;

        public VisitPayUserIssueResultRepository(ISessionContext<VisitPay> sessionContext, ISessionContext<CdiEtl> cdiSession)
            : base(sessionContext)
        {
            this._cdiSession = cdiSession.Session;
        }

        public IReadOnlyList<IssueResult> AdjustAllGuarantorDates(int guarantorId, string datePart, int adjustment)
        {

            IList<IssueResult> results = this.Session.CreateSQLQuery("EXEC qa.AdjustAppGuarantorDates @VpGuarantorId =:VpGuarantorId, @DatePart =:DatePart, @Adjustment =:Adjustment")
                    .SetParameter("VpGuarantorId", guarantorId)
                    .SetParameter("DatePart", datePart)
                    .SetParameter("Adjustment", adjustment)
                    .SetResultTransformer(Transformers.AliasToBean<IssueResult>())
                    .List<IssueResult>();
            IList<IssueResult> results2 = this._cdiSession.CreateSQLQuery("EXEC qa.AdjustEtlGuarantorDates @VpGuarantorId =:VpGuarantorId, @DatePart =:DatePart, @Adjustment =:Adjustment")
                .SetParameter("VpGuarantorId", guarantorId)
                .SetParameter("DatePart", datePart)
                .SetParameter("Adjustment", adjustment)
                .SetResultTransformer(Transformers.AliasToBean<IssueResult>())
                .List<IssueResult>();

            results.AddRange(results2);
            return results.ToList();
        }

        public IReadOnlyList<IssueResult> ClearGuarantorData(int guarantorId)
        {
            IList<IssueResult> results = this.Session.CreateSQLQuery("EXEC qa.ResetAppGuarantor @VpGuarantorId =:VpGuarantorId")
                                             .SetParameter("VpGuarantorId", guarantorId)
                                             .SetResultTransformer(Transformers.AliasToBean<IssueResult>())
                                             .List<IssueResult>();

            IList<IssueResult> results2 = this._cdiSession.CreateSQLQuery("EXEC qa.ResetEtlGuarantor @VpGuarantorId =:VpGuarantorId")
                .SetParameter("VpGuarantorId", guarantorId)
                .SetResultTransformer(Transformers.AliasToBean<IssueResult>())
                .List<IssueResult>();
            results.AddRange(results2);

            return results.ToList();
        }
    }
}
﻿namespace Ivh.Provider.Visit.Modules
{
    using Autofac;
    using Common.State.Interfaces;
    using Common.VisitPay.Enums;
    using Domain.Visit.Entities;
    using Domain.Visit.Interfaces;

    public class Module : Autofac.Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<VisitAgingHistoryRepository>().As<IVisitAgingHistoryRepository>();
            builder.RegisterType<VisitStateHistoryRepository>().As<IVisitStateHistoryRepository>();
            builder.RegisterType<VisitTransactionRepository>().As<IVisitTransactionRepository>();
            builder.RegisterType<VisitTransactionRepository>().As<IVisitTransactionWriteRepository>();
            builder.RegisterType<VisitRepository>().As<IVisitRepository>();
            builder.RegisterType<VisitStateContextProvider>().As<IContextProvider<Visit, VisitStateEnum>>();
            builder.RegisterType<VisitItemizationStorageRepository>().As<IVisitItemizationStorageRepository>();
            builder.RegisterType<VisitPayUserIssueResultRepository>().As<IVisitPayUserIssueResultRepository>();
            builder.RegisterType<PrimaryInsuranceTypeRepository>().As<IPrimaryInsuranceTypeRepository>();
            builder.RegisterType<InsurancePlanRepository>().As<IInsurancePlanRepository>();
            builder.RegisterType<HsFacilityRepository>().As<IHsFacilityRepository>();
            builder.RegisterType<FacilityRepository>().As<IFacilityRepository>();
            builder.RegisterType<VisitEventRepository>().As<IVisitEventRepository>();
        }
    }
}

﻿namespace Ivh.Provider.Visit
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Application.HospitalData.Common.Models;
    using Common.Base.Utilities.Extensions;
    using Common.Data.Connection.Connections;
    using Common.Data.Interfaces;
    using Common.VisitPay.Enums;
    using Domain.Guarantor.Entities;
    using Domain.Settings.Interfaces;
    using Domain.Visit.Entities;
    using Domain.Visit.Interfaces;
    using Ivh.Common.Data;
    using NHibernate;
    using NHibernate.Criterion;

    public class VisitItemizationStorageRepository : RepositoryBase<VisitItemizationStorage, VisitPay>, IVisitItemizationStorageRepository
    {
        private readonly Lazy<IApplicationSettingsService> _applicationSettingsService;
        private readonly Lazy<IHsBillingSystemResolutionProvider> _hsBillingSystemResolutionProvider;

        public VisitItemizationStorageRepository(
            ISessionContext<VisitPay> sessionContext,
            IStatelessSessionContext<VisitPay> statelessSessionContext,
            Lazy<IApplicationSettingsService> applicationSettingsService,
            Lazy<IHsBillingSystemResolutionProvider> hsBillingSystemResolutionProvider)
            : base(sessionContext, statelessSessionContext)
        {
            this._applicationSettingsService = applicationSettingsService;
            this._hsBillingSystemResolutionProvider = hsBillingSystemResolutionProvider;
        }

        public VisitItemizationStorage GetItemization(int vpGuarantorId, int visitFileStoreId, int currentVisitPayUserId)
        {
            VisitItemizationStorage result = this.Session.QueryOver<VisitItemizationStorage>()
                .Where(x => x.VisitFileStoredId == visitFileStoreId
                    && x.VpGuarantor.VpGuarantorId == vpGuarantorId
            ).Take(1).SingleOrDefault<VisitItemizationStorage>();

            if (result != null)
            {
                result = this.SupplementVisitItemizationStorageAsync(result);
                if (result != null)
                {
                    IList<VisitItemizationStorage> results = this.PostQueryFilter(result.ToListOfOne(), new VisitItemizationStorageFilter() { CurrentVisitPayUserId = currentVisitPayUserId }).ToList();
                    if (results.IsNotNullOrEmpty())
                    {
                        return results.FirstOrDefault();
                    }
                }
            }
            return null;
        }

        public VisitItemizationStorage GetItemizationForVisit(int vpGuarantorId, string accountNumber, string empi, int currentVisitPayUserId)
        {
            VisitItemizationStorage result = this.Session.QueryOver<VisitItemizationStorage>()
                .Where(x => x.VpGuarantor.VpGuarantorId == vpGuarantorId
                    && x.AccountNumber == accountNumber
                    && x.Empi == empi
                ).Take(1).SingleOrDefault<VisitItemizationStorage>();

            if (result != null)
            {
                result = this.SupplementVisitItemizationStorageAsync(result);
                if (result != null)
                {
                    IList<VisitItemizationStorage> results = this.PostQueryFilter(result.ToListOfOne(), new VisitItemizationStorageFilter() { CurrentVisitPayUserId = currentVisitPayUserId }).ToList();
                    if (results.IsNotNullOrEmpty())
                    {
                        return results.FirstOrDefault();
                    }
                }
            }
            return null;
        }

        public VisitItemizationStorage GetByExternalKey(Guid externalKey, int currentVisitPayUserId)
        {
            ICriteria fsQuery = this.Session.CreateCriteria<VisitItemizationStorage>();
            fsQuery.Add(Restrictions.Eq("ExternalStorageKey", externalKey));

            VisitItemizationStorage result = fsQuery.UniqueResult<VisitItemizationStorage>();
            if (result != null)
            {
                result = this.SupplementVisitItemizationStorageAsync(result);
                if (result != null)
                {
                    IList<VisitItemizationStorage> results = this.PostQueryFilter(result.ToListOfOne(), new VisitItemizationStorageFilter() { CurrentVisitPayUserId = currentVisitPayUserId }).ToList();
                    if (results.IsNotNullOrEmpty())
                    {
                        return results.FirstOrDefault();
                    }
                }
            }
            return null;
        }

        public VisitItemizationStorageResults GetItemizations(int vpGuarantorId, VisitItemizationStorageFilter filter, int pageNumber, int pageSize)
        {
            VisitItemizationStorageResults results = this.GetItemizationTotals(vpGuarantorId, filter);
            if (results.TotalRecords > 0)
            {
                results.Itemizations = this.DoGetItemizations(vpGuarantorId, filter, pageNumber, pageSize);
            }
            else
            {
                results.Itemizations = new List<VisitItemizationStorage>();
            }
            return results;
        }

        public IList<VisitItemizationStorage> GetItemizations(int vpGuarantorId, int currentVisitPayUserId)
        {
            return this.DoGetItemizations(vpGuarantorId, new VisitItemizationStorageFilter() { CurrentVisitPayUserId = currentVisitPayUserId }, 0, int.MaxValue);
        }

        public VisitItemizationStorageResults GetItemizationTotals(int vpGuarantorId, VisitItemizationStorageFilter filter)
        {
            IList<VisitItemizationStorage> results = this.QueryOverItemizations(vpGuarantorId).List();
            if (results.IsNotNullOrEmpty())
            {
                results = results.Select(this.SupplementVisitItemizationStorageAsync).ToList();
                if (results.IsNotNullOrEmpty())
                {
                    results = this.PostQueryFilter(results, filter).ToList();
                    if (results.IsNotNullOrEmpty())
                    {
                        return new VisitItemizationStorageResults
                        {
                            TotalRecords = results.Count,
                        };
                    }
                }
            }
            return new VisitItemizationStorageResults
            {
                TotalRecords = 0
            };
        }

        private IList<VisitItemizationStorage> DoGetItemizations(int vpGuarantorId, VisitItemizationStorageFilter filter, int batch, int batchSize)
        {
            IQueryOver<VisitItemizationStorage, VisitItemizationStorage> query = this.QueryOverItemizations(vpGuarantorId);

            IList<VisitItemizationStorage> results = query.List();
            if (results.IsNotNullOrEmpty())
            {
                results = results.Select(this.SupplementVisitItemizationStorageAsync).ToList();
                if (results.IsNotNullOrEmpty())
                {
                    results = this.PostQueryFilter(results, filter).ToList();
                    if (results.IsNotNullOrEmpty())
                    {
                        return Sort(filter, results)
                            .Skip(batch * batchSize)
                            .Take(batchSize).ToList();
                    }
                }
            }
            return new List<VisitItemizationStorage>();
        }

        private IQueryOver<VisitItemizationStorage, VisitItemizationStorage> QueryOverItemizations(int vpGuarantorId)
        {
            return this.Session
                .QueryOver<VisitItemizationStorage>()
                .Where(x => x.VpGuarantor == new Guarantor { VpGuarantorId = vpGuarantorId });
        }

        private IEnumerable<VisitItemizationStorage> PostQueryFilter(IEnumerable<VisitItemizationStorage> results, VisitItemizationStorageFilter filter)
        {
            bool ApplicationOverride() => this._applicationSettingsService.Value.IsClientApplication.Value ||
                                          this._applicationSettingsService.Value.IsSystemApplication.Value ||
                                          this._applicationSettingsService.Value.Application.Value == ApplicationEnum.QATools;

            bool UserVisibilityFilter(VisitItemizationStorage v, VisitItemizationStorageFilter f) =>
                ApplicationOverride() 
                || v.VpGuarantor.User.VisitPayUserId == f.CurrentVisitPayUserId;

            bool ConsolidationMaskFilter(VisitItemizationStorage v, VisitItemizationStorageFilter f) =>
                ApplicationOverride() 
                || v.VpGuarantor.User.VisitPayUserId == f.CurrentVisitPayUserId 
                || v.IsPatientGuarantor.GetValueOrDefault(false) || v.IsPatientMinor.GetValueOrDefault(false);

            bool UnmatchedFilter(VisitItemizationStorage v, VisitItemizationStorageFilter f) =>
                ApplicationOverride() 
                || !v.IsRemoved;

            bool PatientFilter(VisitItemizationStorage v, VisitItemizationStorageFilter f) =>
                f.PatientName.IsNullOrEmpty() 
                || f.PatientName == v.PatientName;

            bool FacilityFilter(VisitItemizationStorage v, VisitItemizationStorageFilter f) =>
                f.FacilityCode.IsNullOrEmpty() 
                || f.FacilityCode == v.FacilityCode;

            bool DateRangeFilter(VisitItemizationStorage v, VisitItemizationStorageFilter f) =>
                !f.DateRange.HasValue 
                || filter.DateRange.HasValue && v.VisitDischargeDate >= DateTime.UtcNow.AddDays(-filter.DateRange.Value);

            bool DateFromFilter(VisitItemizationStorage v, VisitItemizationStorageFilter f) =>
                !f.DateRangeFrom.HasValue 
                || v.VisitDischargeDate >= filter.DateRangeFrom;

            bool DateToFilter(VisitItemizationStorage v, VisitItemizationStorageFilter f) =>
                !f.DateRangeTo.HasValue 
                || v.VisitDischargeDate <= filter.DateRangeTo;

            bool CompoundFilter(VisitItemizationStorage v, VisitItemizationStorageFilter f) =>
                UserVisibilityFilter(v, f) && 
                ConsolidationMaskFilter(v, f) && 
                UnmatchedFilter(v, f) &&
                PatientFilter(v, f) && 
                FacilityFilter(v, f) && 
                DateRangeFilter(v, f) && 
                DateFromFilter(v, f) &&
                DateToFilter(v, f);

            return results.Where(x => CompoundFilter(x, filter));
        }

        private static IList<VisitItemizationStorage> Sort(VisitItemizationStorageFilter filter, IList<VisitItemizationStorage> results)
        {
            if (filter == null || string.IsNullOrEmpty(filter.SortField))
            {
                return results;
            }

            bool isAscendingOrder = "asc".Equals(filter.SortOrder, StringComparison.InvariantCultureIgnoreCase);

            switch (filter.SortField)
            {
                case "DischargeDate":
                    return isAscendingOrder
                        ? results.OrderBy(x => x.VisitDischargeDate.GetValueOrDefault(DateTime.UtcNow.Date)).ToList()
                        : results.OrderByDescending(x => x.VisitDischargeDate.GetValueOrDefault(DateTime.UtcNow.Date)).ToList();
            }
            return results;
        }

        private VisitItemizationStorage SupplementVisitItemizationStorageAsync(VisitItemizationStorage visitItemizationStorage)
        {
            if (visitItemizationStorage != null)
            {
                int billingSystemId = visitItemizationStorage.BillingSystemId ?? this._hsBillingSystemResolutionProvider.Value.ResolveBillingSystemId(visitItemizationStorage.VisitSourceSystemKey);
                string visitSourceSystemKey = visitItemizationStorage.VisitSourceSystemKey;
                string hsGuarantorSourceSystemKey = visitItemizationStorage.HsGuarantorSourceSystemKey;

                VisitItemizationDetailsResultModel visitItemizationDetailsModel = this.GetVisitItemizationDetails(billingSystemId, hsGuarantorSourceSystemKey, visitSourceSystemKey);

                if (visitItemizationDetailsModel != default(VisitItemizationDetailsResultModel))
                {
                    visitItemizationStorage.PatientFirstName = visitItemizationDetailsModel.PatientFirstName;
                    visitItemizationStorage.PatientLastName = visitItemizationDetailsModel.PatientLastName;
                    visitItemizationStorage.VisitDischargeDate = visitItemizationDetailsModel.VisitDischargeDate;
                    visitItemizationStorage.VisitAdmitDate = visitItemizationDetailsModel.VisitAdmitDate;
                    visitItemizationStorage.IsPatientGuarantor = visitItemizationDetailsModel.IsPatientGuarantor;
                    visitItemizationStorage.IsPatientMinor = visitItemizationDetailsModel.IsPatientMinor;
                }
            }

            return visitItemizationStorage;
        }

        public VisitItemizationDetailsResultModel GetVisitItemizationDetails(int billingSystemId, string hsGuarantorSourceSystemKey, string visitSourceSystemKey)
        {
            BaseVisit visit = this.StatelessSession.QueryOver<BaseVisit>()
                    .WhereStringEq(x => x.SourceSystemKey, visitSourceSystemKey)
                    .Where(x => x.BillingSystemId == billingSystemId)
                    .SingleOrDefault();

            if (visit != null)
            {
                BaseHsGuarantor hsGuarantor = this.StatelessSession.QueryOver<BaseHsGuarantor>()
                    .WhereStringEq(x => x.SourceSystemKey, hsGuarantorSourceSystemKey)
                    .Where(x => x.HsBillingSystemId == billingSystemId)
                    .SingleOrDefault();

                if (hsGuarantor != null)
                {
                    IList<HsGuarantorMap> vpGuarantorHsMatches = this.StatelessSession.QueryOver<HsGuarantorMap>().Where(x => x.HsGuarantorId == hsGuarantor.HsGuarantorId).List();
                    HsGuarantorMap vpGuarantorHsMatch = vpGuarantorHsMatches.FirstOrDefault(x => x.HsGuarantorMatchStatus.IsInCategory(HsGuarantorMatchStatusEnumCategory.Matched));
                    if (vpGuarantorHsMatch != null)
                    {
                        return new VisitItemizationDetailsResultModel()
                        {
                            BillingSystemId = visit.BillingSystemId,
                            HsGuarantorId = vpGuarantorHsMatch.HsGuarantorId,
                            VpGuarantorId = vpGuarantorHsMatch.VpGuarantor.VpGuarantorId,
                            PatientLastName = visit.PatientLastName,
                            PatientFirstName = visit.PatientFirstName,
                            VisitSourceSystemKeyDisplay = visit.SourceSystemKeyDisplay,
                            VisitAdmitDate = visit.AdmitDate,
                            VisitDischargeDate = visit.DischargeDate,
                            IsPatientGuarantor = visit.IsPatientGuarantor,
                            IsPatientMinor = visit.IsPatientMinor
                        };
                    }
                }
            }
            return null;
        }
    }
}
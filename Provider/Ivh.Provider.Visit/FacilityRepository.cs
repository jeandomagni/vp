﻿namespace Ivh.Provider.Visit
{
    using System;
    using Common.Cache;
    using Common.Data;
    using Common.Data.Connection.Connections;
    using Common.Data.Interfaces;
    using Domain.Visit.Entities;
    using Domain.Visit.Interfaces;

    public class FacilityRepository
        : RepositoryCacheBase<Facility, VisitPay>, IFacilityRepository, IDisposable
    {
        public FacilityRepository(
            ISessionContext<VisitPay> sessionContext,
            Lazy<IDistributedCache> cache)
                : base(sessionContext, cache)
        {
        }

        protected override int GetId(Facility entity) => entity.FacilityId;

        public void Dispose()
        {
            base.CallBackAfterCommit = null;
        }
    }
}
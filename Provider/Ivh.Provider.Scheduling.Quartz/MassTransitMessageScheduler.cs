﻿namespace Ivh.Provider.Scheduling.Quartz
{
    //Note:  Some of this:
    // Copyright 2007-2016 Chris Patterson, Dru Sellers, Travis Smith, et. al.
    //  
    // Licensed under the Apache License, Version 2.0 (the "License"); you may not use
    // this file except in compliance with the License. You may obtain a copy of the 
    // License at 
    // 
    //     http://www.apache.org/licenses/LICENSE-2.0 
    // 
    // Unless required by applicable law or agreed to in writing, software distributed
    // under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR 
    // CONDITIONS OF ANY KIND, either express or implied. See the License for the 
    // specific language governing permissions and limitations under the License.

    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Text;
    using System.Xml.Linq;
    using Application.Scheduling.Interfaces;
    using Domain.Logging.Interfaces;
    using MassTransit;
    using MassTransit.Scheduling;
    using MassTransit.Serialization;
    using Newtonsoft.Json;
    using Newtonsoft.Json.Linq;
    using global::Quartz;

    public class MassTransitMessageScheduler : IMassTransitMessageScheduler
    {
        private readonly Lazy<ILoggingProvider> _logger;
        private readonly Lazy<IScheduler> _scheduler;

        public MassTransitMessageScheduler(
            Lazy<ILoggingProvider> logger,
            Lazy<IScheduler> scheduler)
        {
            this._logger = logger;
            this._scheduler = scheduler;
        }

        public void ScheduleMessage(ConsumeContext<ScheduleMessage> context)
        {
            string correlationId = context.Message.CorrelationId.ToString("N");

            JobKey jobKey = new JobKey(correlationId);

            this._logger.Value.Info(() => "ScheduleMessage: {0} at {1}", jobKey, context.Message.ScheduledTime);

            IJobDetail jobDetail = this.CreateJobDetail(context, context.Message.Destination, jobKey);

            ITrigger trigger = TriggerBuilder.Create()
                .ForJob(jobDetail)
                .StartAt(context.Message.ScheduledTime)
                .WithSchedule(SimpleScheduleBuilder.Create().WithMisfireHandlingInstructionFireNow())
                .WithIdentity(new TriggerKey(correlationId))
                .Build();

            this._scheduler.Value.ScheduleJob(jobDetail, trigger);
        }

        public void ScheduleRecurringMessage(ConsumeContext<ScheduleRecurringMessage> context)
        {
            this._logger.Value.Info(() => "ScheduleRecurringMessage: {0} at {1}", context.Message.CorrelationId, context.Message.Schedule.ScheduleId);

            JobKey jobKey = new JobKey(context.Message.Schedule.ScheduleId, context.Message.Schedule.ScheduleGroup);

            IJobDetail jobDetail = this.CreateJobDetail(context, context.Message.Destination, jobKey);

            TriggerKey triggerKey = new TriggerKey($"Recurring.Trigger.{context.Message.Schedule.ScheduleId}", context.Message.Schedule.ScheduleGroup);

            ITrigger trigger = this.CreateTrigger(context.Message.Schedule, jobDetail, triggerKey);

            if (this._scheduler.Value.CheckExists(triggerKey))
            {
                this._scheduler.Value.RescheduleJob(triggerKey, trigger);
            }
            else
            {
                this._scheduler.Value.ScheduleJob(jobDetail, trigger);
            }
        }

        private ITrigger CreateTrigger(RecurringSchedule schedule, IJobDetail jobDetail, TriggerKey triggerKey)
        {
            TimeZoneInfo tz = TimeZoneInfo.Local;
            if (!string.IsNullOrWhiteSpace(schedule.TimeZoneId) && schedule.TimeZoneId != tz.Id)
            {
                tz = TimeZoneInfo.FindSystemTimeZoneById(schedule.TimeZoneId);
            }

            TriggerBuilder triggerBuilder = TriggerBuilder.Create()
                .ForJob(jobDetail)
                .WithIdentity(triggerKey)
                .StartAt(schedule.StartTime)
                //.WithDescription(schedule.Description)
                .WithCronSchedule(schedule.CronExpression, x =>
                {
                    x.InTimeZone(tz);
                    switch (schedule.MisfirePolicy)
                    {
                        case MissedEventPolicy.Skip:
                            x.WithMisfireHandlingInstructionDoNothing();
                            break;
                        case MissedEventPolicy.Send:
                            x.WithMisfireHandlingInstructionFireAndProceed();
                            break;
                    }
                });

            if (schedule.EndTime.HasValue)
            {
                triggerBuilder.EndAt(schedule.EndTime);
            }

            return triggerBuilder.Build();
        }

        private IJobDetail CreateJobDetail(ConsumeContext context, Uri destination, JobKey jobKey, Guid? tokenId = default(Guid?))
        {
            string body;
            using (MemoryStream ms = new MemoryStream())
            {
                using (Stream bodyStream = context.ReceiveContext.GetBodyStream())
                {
                    bodyStream.CopyTo(ms);
                }

                body = Encoding.UTF8.GetString(ms.ToArray());
            }

            if (string.Compare(context.ReceiveContext.ContentType.MediaType, JsonMessageSerializer.JsonContentType.MediaType, StringComparison.OrdinalIgnoreCase) == 0)
            {
                body = this.TranslateJsonBody(body, destination.ToString());
            }
            else if (string.Compare(context.ReceiveContext.ContentType.MediaType, XmlMessageSerializer.XmlContentType.MediaType, StringComparison.OrdinalIgnoreCase) == 0)
            {
                body = this.TranslateXmlBody(body, destination.ToString());
            }
            else
            {
                throw new InvalidOperationException("Only JSON and XML messages can be scheduled");
            }

            JobBuilder builder = JobBuilder.Create<ScheduledMessageJob>()
                .RequestRecovery(true)
                .WithIdentity(jobKey)
                .UsingJobData("Destination", this.ToString(destination))
                .UsingJobData("ResponseAddress", this.ToString(context.ResponseAddress))
                .UsingJobData("FaultAddress", this.ToString(context.FaultAddress))
                .UsingJobData("Body", body)
                .UsingJobData("ContentType", context.ReceiveContext.ContentType.MediaType);

            if (context.MessageId.HasValue)
            {
                builder = builder.UsingJobData("MessageId", context.MessageId.Value.ToString());
            }

            if (context.CorrelationId.HasValue)
            {
                builder = builder.UsingJobData("CorrelationId", context.CorrelationId.Value.ToString());
            }

            if (context.ConversationId.HasValue)
            {
                builder = builder.UsingJobData("ConversationId", context.ConversationId.Value.ToString());
            }

            if (context.InitiatorId.HasValue)
            {
                builder = builder.UsingJobData("InitiatorId", context.InitiatorId.Value.ToString());
            }

            if (context.RequestId.HasValue)
            {
                builder = builder.UsingJobData("RequestId", context.RequestId.Value.ToString());
            }

            if (context.ExpirationTime.HasValue)
            {
                builder = builder.UsingJobData("ExpirationTime", context.ExpirationTime.Value.ToString());
            }

            if (tokenId.HasValue)
            {
                builder = builder.UsingJobData("TokenId", tokenId.Value.ToString("N"));
            }

            IJobDetail jobDetail = builder
                .UsingJobData("HeadersAsJson", JsonConvert.SerializeObject(context.Headers.GetAll()))
                .Build();

            return jobDetail;
        }

        private string TranslateJsonBody(string body, string destination)
        {
            JObject envelope = JObject.Parse(body);

            envelope["destinationAddress"] = destination;

            JToken message = envelope["message"];

            JToken payload = message["payload"];
            JToken payloadType = message["payloadType"];

            envelope["message"] = payload;
            envelope["messageType"] = payloadType;

            return JsonConvert.SerializeObject(envelope, Formatting.Indented);
        }

        private string TranslateXmlBody(string body, string destination)
        {
            using (StringReader reader = new StringReader(body))
            {
                XDocument document = XDocument.Load(reader);

                XElement envelope = (from e in document.Descendants("envelope") select e).Single();

                XElement destinationAddress = (from a in envelope.Descendants("destinationAddress") select a).Single();

                XElement message = (from m in envelope.Descendants("message") select m).Single();
                IEnumerable<XElement> messageType = (from mt in envelope.Descendants("messageType") select mt);

                XElement payload = (from p in message.Descendants("payload") select p).Single();
                IEnumerable<XElement> payloadType = (from pt in message.Descendants("payloadType") select pt);

                message.Remove();
                messageType.Remove();

                destinationAddress.Value = destination;

                message = new XElement("message");
                message.Add(payload.Descendants());
                envelope.Add(message);

                envelope.Add(payloadType.Select(x => new XElement("messageType", x.Value)));

                return document.ToString();
            }
        }

        private string ToString(Uri uri)
        {
            return uri == null ? "" : uri.ToString();
        }
    }
}

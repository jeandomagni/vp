﻿namespace Ivh.Provider.Scheduling.Quartz
{
    // Copyright 2007-2016 Chris Patterson, Dru Sellers, Travis Smith, et. al.
    //  
    // Licensed under the Apache License, Version 2.0 (the "License"); you may not use
    // this file except in compliance with the License. You may obtain a copy of the 
    // License at 
    // 
    //     http://www.apache.org/licenses/LICENSE-2.0 
    // 
    // Unless required by applicable law or agreed to in writing, software distributed
    // under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR 
    // CONDITIONS OF ANY KIND, either express or implied. See the License for the 
    // specific language governing permissions and limitations under the License.
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.IO;
    using System.Linq;
    using System.Net.Mime;
    using System.Text;
    using System.Xml.Linq;
    using Autofac;
    using Common.DependencyInjection;
    using global::Quartz;
    using GreenPipes;
    using MassTransit;
    using MassTransit.Logging;
    using MassTransit.Serialization;
    using MassTransit.Util;
    using Newtonsoft.Json;
    using Newtonsoft.Json.Linq;

    public class ScheduledMessageJob :
        IJob
    {
        private static readonly ILog _log = Logger.Get<ScheduledMessageJob>();
        private readonly IBus _bus;

        public ScheduledMessageJob()
        {
            this._bus = IvinciContainer.Instance.Container().Resolve<IBus>();
        }

        public string Destination { get; set; }
        public string ExpirationTime { get; set; }
        public string ResponseAddress { get; set; }
        public string FaultAddress { get; set; }
        public string Body { get; set; }
        public string MessageId { get; set; }
        public string MessageType { get; set; }
        public string ContentType { get; set; }
        public string RequestId { get; set; }
        public string CorrelationId { get; set; }
        public string ConversationId { get; set; }
        public string InitiatorId { get; set; }
        public string TokenId { get; set; }
        public string HeadersAsJson { get; set; }
        public string PayloadMessageHeadersAsJson { get; set; }

        public void Execute(IJobExecutionContext context)
        {
            try
            {
                Uri destinationAddress = new Uri(this.Destination);
                Uri sourceAddress = this._bus.Address;

                IPipe<SendContext> sendPipe = this.CreateMessageContext(sourceAddress, destinationAddress, context.Trigger.Key.Name);

                ISendEndpoint endpoint = TaskUtil.Await(() => this._bus.GetSendEndpoint(destinationAddress));

                Scheduled scheduled = new Scheduled();

                TaskUtil.Await(() => endpoint.Send(scheduled, sendPipe));
            }
            catch (Exception ex)
            {
                string message = string.Format(CultureInfo.InvariantCulture,
                    "An exception occurred sending message {0} to {1}", this.MessageType, this.Destination);
                _log.Error(message, ex);

                throw new JobExecutionException(message, ex);
            }
        }

        private IPipe<SendContext> CreateMessageContext(Uri sourceAddress, Uri destinationAddress, string triggerKey)
        {
            IPipe<SendContext> sendPipe = Pipe.New<SendContext>(x =>
            {
                x.UseExecute(context =>
                {
                    context.DestinationAddress = destinationAddress;
                    context.SourceAddress = sourceAddress;
                    context.ResponseAddress = ToUri(this.ResponseAddress);
                    context.FaultAddress = ToUri(this.FaultAddress);

                    this.SetHeaders(context);

                    context.MessageId = ConvertIdToGuid(this.MessageId);
                    context.RequestId = ConvertIdToGuid(this.RequestId);
                    context.CorrelationId = ConvertIdToGuid(this.CorrelationId);
                    context.ConversationId = ConvertIdToGuid(this.ConversationId);
                    context.InitiatorId = ConvertIdToGuid(this.InitiatorId);

                    Guid? tokenId = ConvertIdToGuid(this.TokenId);
                    if (tokenId.HasValue)
                    {
                        context.Headers.Set(MessageHeaders.SchedulingTokenId, tokenId.Value.ToString("N"));
                    }

                    context.Headers.Set(MessageHeaders.QuartzTriggerKey, triggerKey);

                    if (!string.IsNullOrEmpty(this.ExpirationTime))
                        context.TimeToLive = DateTime.UtcNow - DateTime.Parse(this.ExpirationTime);

                    if (string.Compare(this.ContentType, JsonMessageSerializer.JsonContentType.MediaType, StringComparison.OrdinalIgnoreCase) == 0)
                        this.Body = this.UpdateJsonHeaders(this.Body);
                    else if (string.Compare(this.ContentType, XmlMessageSerializer.XmlContentType.MediaType, StringComparison.OrdinalIgnoreCase) == 0)
                        this.Body = this.UpdateXmlHeaders(this.Body);

                    context.Serializer = new ScheduledBodySerializer(new ContentType(this.ContentType), Encoding.UTF8.GetBytes(this.Body));
                });
            });

            return sendPipe;
        }

        private static Guid? ConvertIdToGuid(string id)
        {
            if (string.IsNullOrWhiteSpace(id))
                return default(Guid?);

            Guid messageId;
            if (Guid.TryParse(id, out messageId))
                return messageId;

            throw new FormatException("The Id was not a Guid: " + id);
        }

        private void SetHeaders(SendContext context)
        {
            if (string.IsNullOrEmpty(this.HeadersAsJson))
                return;

            IDictionary<string, object> headers = JsonConvert.DeserializeObject<IDictionary<string, object>>(this.HeadersAsJson);
            foreach (KeyValuePair<string, object> header in headers)
                context.Headers.Set(header.Key, header.Value);
        }

        private string UpdateJsonHeaders(string body)
        {
            if (string.IsNullOrEmpty(this.PayloadMessageHeadersAsJson))
                return body;

            JObject envelope = JObject.Parse(body);

            Dictionary<string, object> payloadHeaders = JObject.Parse(this.PayloadMessageHeadersAsJson).ToObject<Dictionary<string, object>>();

            JToken headersToken = envelope["headers"] ?? new JObject();
            Dictionary<string, object> headers = headersToken.ToObject<Dictionary<string, object>>();

            foreach (KeyValuePair<string, object> payloadHeader in payloadHeaders)
            {
                headers[payloadHeader.Key] = payloadHeader.Value;
            }
            envelope["headers"] = JToken.FromObject(headers);

            return JsonConvert.SerializeObject(envelope, Formatting.Indented);
        }

        private string UpdateXmlHeaders(string body)
        {
            if (string.IsNullOrEmpty(this.PayloadMessageHeadersAsJson))
                return body;

            using (StringReader reader = new StringReader(body))
            {
                XDocument document = XDocument.Load(reader);

                XElement envelope = (from e in document.Descendants("envelope") select e).Single();

                XElement headers = (from h in envelope.Descendants("headers") select h).SingleOrDefault();
                if (headers == null)
                {
                    headers = new XElement("headers");
                    envelope.Add(headers);
                }

                Dictionary<string, object> payloadHeaders = JObject.Parse(this.PayloadMessageHeadersAsJson).ToObject<Dictionary<string, object>>();

                foreach (KeyValuePair<string, object> payloadHeader in payloadHeaders)
                {
                    headers.Add(new XElement(payloadHeader.Key, payloadHeader.Value));
                }

                return document.ToString();
            }
        }

        private static Uri ToUri(string s)
        {
            if (string.IsNullOrEmpty(s))
                return null;

            return new Uri(s);
        }


        private class Scheduled
        {
        }


        private class ScheduledBodySerializer :
            IMessageSerializer
        {
            private readonly byte[] _body;

            public ScheduledBodySerializer(ContentType contentType, byte[] body)
            {
                this.ContentType = contentType;
                this._body = body;
            }

            public ContentType ContentType { get; private set; }

            public void Serialize<T>(Stream stream, SendContext<T> context)
                where T : class
            {
                stream.Write(this._body, 0, this._body.Length);
            }
        }
    }
}

﻿namespace Ivh.Provider.Tests.PresumptiveCharityEvaluationProviders
{
    using System.Collections.Generic;
    using Domain.HospitalData.Segmentation.Entities;
    using Domain.HospitalData.Visit.Entities;
    using HospitalData.Segmentation.PresumptiveCharityEvaluationProviders;
    using NUnit.Framework;

    [TestFixture]
    public class EvaluateForPcFlagPresumptiveCharityEvaluationProviderTests
    {

        private EvaluateForPcFlagPresumptiveCharityEvaluationProvider _presumptiveCharityEvaluationProvider;
        private IList<PresumptiveCharityEligibleVisit> _potentialVisits;

        [SetUp]
        public void BeforeEachTest()
        {
            PresumptiveCharityEvaluationProviderBuilder builder = new PresumptiveCharityEvaluationProviderBuilder();
            this._potentialVisits = builder.InitializeVisits();
            this._presumptiveCharityEvaluationProvider = new EvaluateForPcFlagPresumptiveCharityEvaluationProvider();
        }

        [Test]
        public void GetVisitsWithFirstSelfPayDatesThatAreBeforeDate()
        {
            IList<PresumptiveCharityGuarantorVisitDaily> eligibleVisits = this._presumptiveCharityEvaluationProvider.GetEligibleVisits(this._potentialVisits, 0);

            Assert.AreEqual(4, eligibleVisits.Count);
            Assert.AreEqual(eligibleVisits[0].VisitId, 2);
            Assert.AreEqual(eligibleVisits[1].VisitId, 4);
            Assert.AreEqual(eligibleVisits[2].VisitId, 6);
            Assert.AreEqual(eligibleVisits[3].VisitId, 9);
        }

    }
}

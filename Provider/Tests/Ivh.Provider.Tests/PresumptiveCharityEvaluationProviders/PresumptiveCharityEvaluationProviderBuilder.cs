﻿namespace Ivh.Provider.Tests.PresumptiveCharityEvaluationProviders
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Common.Base.Utilities.Helpers;
    using Common.Tests.Builders;
    using Common.VisitPay.Enums;
    using Domain.HospitalData.Scoring.Enums;
    using Domain.HospitalData.Segmentation.Entities;
    using Domain.HospitalData.Visit.Entities;
    using Domain.Settings.Entities;
    using Domain.Settings.Interfaces;
    using Moq;

    public class PresumptiveCharityEvaluationProviderBuilder
    {
        public IList<PresumptiveCharityEligibleVisit> InitializeVisits()
        {

            IList<Visit> potentialVisits = new List<Visit>
            {
                new HsVisitBuilder().WithDefaultValues(1000, SelfPayClassEnum.PureSelfPay, false, 1).Build(),  // old, sp
                new HsVisitBuilder().WithDefaultValues(1000, SelfPayClassEnum.SelfPayAfterInsurance, true, 2).Build(),  // old, spai
                new HsVisitBuilder().WithDefaultValues(1000, SelfPayClassEnum.SelfPayAfterMedicare, false, 3).Build(),  // old, spam
                new HsVisitBuilder().WithDefaultValues(1000, SelfPayClassEnum.PureSelfPay, true, 4).Build(),  // old, flagged
                new HsVisitBuilder().WithDefaultValues(1000, SelfPayClassEnum.SelfPayAfterMedicare, false, 5).Build(),  // old, not flagged

                new HsVisitBuilder().WithDefaultValues(10, SelfPayClassEnum.PureSelfPay, true, 6).Build(),  // new, sp
                new HsVisitBuilder().WithDefaultValues(10, SelfPayClassEnum.SelfPayAfterInsurance, false, 7).Build(),  // new, spai
                new HsVisitBuilder().WithDefaultValues(10, SelfPayClassEnum.SelfPayAfterMedicare, false, 8).Build(),  // new, spam
                new HsVisitBuilder().WithDefaultValues(10, SelfPayClassEnum.SelfPayAfterInsurance, true, 9).Build(),  // new, flagged
                new HsVisitBuilder().WithDefaultValues(10, SelfPayClassEnum.PureSelfPay, false, 10).Build(),  // new, not flagged
            };

            IList<PresumptiveCharityEligibleVisit> presumptiveCharityEligibleVisits = potentialVisits.Select(potentialVisit => new PresumptiveCharityEligibleVisit
                {
                    VisitId = potentialVisit.VisitId,
                    HsGuarantorId = potentialVisit.HsGuarantorId,
                    EvaluateForPCSegmentation = potentialVisit.EvaluateForPCSegmentation,
                    FirstSelfPayDate = potentialVisit.FirstSelfPayDate,
                    SelfPayClassId = (int)potentialVisit.PrimaryInsuranceType.SelfPayClass
                })
                .ToList();

            return presumptiveCharityEligibleVisits;

        }

        public Mock<IClientService> MockClientService(bool multipleSelfPayClassIds = false)
        {
            Lazy<TimeZoneHelper> timeZoneHelper = new Lazy<TimeZoneHelper>(() => new TimeZoneHelper(TimeZoneHelper.TimeZones.PacificStandardTime));
            Mock<IClientService> clientService = new Mock<IClientService>();
            clientService.Setup(t => t.GetClient()).Returns(() => new Client(new Dictionary<string, string>
            {
                {"Segmentation.PCDaysSinceFirstSelfPayDate", "20"},
                {"Segmentation.PCMinSelfPayDate", "2017-12-01"},
                {"Segmentation.PCSelfPayClassIds", multipleSelfPayClassIds ? "1,3" : "3"}
            }, timeZoneHelper));

            return clientService;
        }
    }
}
﻿namespace Ivh.Provider.Tests.PresumptiveCharityEvaluationProviders
{
    using System;
    using System.Collections.Generic;
    using Domain.HospitalData.Segmentation.Entities;
    using Domain.HospitalData.Visit.Entities;
    using Domain.Settings.Entities;
    using Domain.Settings.Interfaces;
    using HospitalData.Segmentation.PresumptiveCharityEvaluationProviders;
    using Moq;
    using NUnit.Framework;

    [TestFixture]
    public class SelfPayClassPresumptiveCharityEvaluationProviderTests
    {

        private Mock<IClientService> _clientService;
        private SelfPayClassPresumptiveCharityEvaluationProvider _presumptiveCharityEvaluationProvider;
        private IList<PresumptiveCharityEligibleVisit> _potentialVisits;
        private PresumptiveCharityEvaluationProviderBuilder _builder; 

        [SetUp]
        public void BeforeEachTest()
        {
            this._builder = new PresumptiveCharityEvaluationProviderBuilder();
            this._clientService = this._builder.MockClientService();
            this._potentialVisits = this._builder.InitializeVisits();

            this._presumptiveCharityEvaluationProvider = new SelfPayClassPresumptiveCharityEvaluationProvider(
                new Lazy<IClientService>(() => this._clientService.Object)
                );
        }

        [Test]
        public void GetVisitsWithPrimaryInsuranceOfSelfPayTypeAndFirstSelfPayDatesThatAreBeforeDate()
        {
            IList<PresumptiveCharityGuarantorVisitDaily> eligibleVisits = this._presumptiveCharityEvaluationProvider.GetEligibleVisits(this._potentialVisits, 0);

            Assert.AreEqual(2, eligibleVisits.Count);
            Assert.AreEqual(eligibleVisits[0].VisitId, 1);
            Assert.AreEqual(eligibleVisits[1].VisitId, 4);
        }

        [Test]
        public void GetVisitsWithMultiplePrimaryInsuranceOfSelfPayTypeAndFirstSelfPayDatesThatAreBeforeDate()
        {
            this._clientService = this._builder.MockClientService(true);
            IList<PresumptiveCharityGuarantorVisitDaily> eligibleVisits = this._presumptiveCharityEvaluationProvider.GetEligibleVisits(this._potentialVisits, 0);

            Assert.AreEqual(3, eligibleVisits.Count);
            Assert.AreEqual(eligibleVisits[0].VisitId, 1);
            Assert.AreEqual(eligibleVisits[1].VisitId, 2);
            Assert.AreEqual(eligibleVisits[2].VisitId, 4);
        }

    }
}


﻿namespace Ivh.Provider.Tests.PresumptiveCharityEvaluationProviders
{
    using System;
    using System.Collections.Generic;
    using Domain.HospitalData.Segmentation.Entities;
    using Domain.HospitalData.Visit.Entities;
    using Domain.Settings.Interfaces;
    using HospitalData.Segmentation.PresumptiveCharityEvaluationProviders;
    using Moq;
    using NUnit.Framework;

    [TestFixture]
    public class DefaultPresumptiveCharityEvaluationProviderTests
    {
        private Mock<IClientService> _clientService;
        private DefaultPresumptiveCharityEvaluationProvider _presumptiveCharityEvaluationProvider;
        private IList<PresumptiveCharityEligibleVisit> _potentialVisits;

        [SetUp]
        public void BeforeEachTest()
        {
            PresumptiveCharityEvaluationProviderBuilder builder = new PresumptiveCharityEvaluationProviderBuilder();
            this._clientService = builder.MockClientService();
            this._potentialVisits = builder.InitializeVisits();

            this._presumptiveCharityEvaluationProvider = new DefaultPresumptiveCharityEvaluationProvider(
                new Lazy<IClientService>(() => this._clientService.Object)
                );
        }

        [Test]
        public void GetVisitsWithFirstSelfPayDatesThatAreBeforeDate()
        {
            IList<PresumptiveCharityGuarantorVisitDaily> eligibleVisits = this._presumptiveCharityEvaluationProvider.GetEligibleVisits(this._potentialVisits, 0);

            Assert.AreEqual(5, eligibleVisits.Count);
            Assert.AreEqual(eligibleVisits[0].VisitId, 1);
            Assert.AreEqual(eligibleVisits[1].VisitId, 2);
            Assert.AreEqual(eligibleVisits[2].VisitId, 3);
            Assert.AreEqual(eligibleVisits[3].VisitId, 4);
            Assert.AreEqual(eligibleVisits[4].VisitId, 5);
        }

    }
}

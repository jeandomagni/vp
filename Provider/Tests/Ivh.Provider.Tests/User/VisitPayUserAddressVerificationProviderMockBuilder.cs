﻿using System;

namespace Ivh.Provider.Tests.User
{
    using Common.Cache;
    using Common.Tests.Helpers;
    using Common.Web.Interfaces;
    using Domain.Logging.Interfaces;
    using Domain.Settings.Interfaces;
    using Domain.User.Interfaces;
    using Moq;
    using Provider.User;

    public class VisitPayUserAddressVerificationProviderMockBuilder
        : IMockBuilder<IVisitPayUserAddressVerificationProvider, VisitPayUserAddressVerificationProviderMockBuilder>
    {
        public IApplicationSettingsService ApplicationSettingsService;
        public IDistributedCache DistributedCache;
        public ILoggingService LoggingService;
        public IMetricsProvider MetricsProvider;
        public IHttpMessageHandlerFactory HttpMessageHandlerFactory;
        public IStaticHttpClientManager StaticHttpClientManager;

        public Mock<IApplicationSettingsService> ApplicationSettingsServiceMock;
        public Mock<IDistributedCache> DistributedCacheMock;
        public Mock<ILoggingService> LoggingServiceMock;
        public Mock<IMetricsProvider> MetricsProviderMock;
        public Mock<IHttpMessageHandlerFactory> HttpMessageHandlerFactoryMock;
        public Mock<IStaticHttpClientManager> StaticHttpClientManagerMock;

        public VisitPayUserAddressVerificationProviderMockBuilder()
        {
            this.ApplicationSettingsServiceMock = new Mock<IApplicationSettingsService>();
            this.DistributedCacheMock = new Mock<IDistributedCache>();
            this.LoggingServiceMock = new Mock<ILoggingService>();
            this.MetricsProviderMock = new Mock<IMetricsProvider>();
            this.HttpMessageHandlerFactoryMock = new Mock<IHttpMessageHandlerFactory>();
        }

        public IVisitPayUserAddressVerificationProvider CreateService()
        {
            return new VisitPayUserAddressVerificationProvider(
                new Lazy<IApplicationSettingsService>(() => this.ApplicationSettingsService ?? this.ApplicationSettingsServiceMock.Object),
                new Lazy<IHttpMessageHandlerFactory>(() => this.HttpMessageHandlerFactory ?? this.HttpMessageHandlerFactoryMock.Object),
                new Lazy<IStaticHttpClientManager>(()=> this.StaticHttpClientManager ?? this.StaticHttpClientManagerMock.Object),
                new Lazy<ILoggingService>(() => this.LoggingService ?? this.LoggingServiceMock.Object),
                new Lazy<IMetricsProvider>(() => this.MetricsProvider ?? this.MetricsProviderMock.Object),
                new Lazy<IDistributedCache>(() => this.DistributedCache ?? this.DistributedCacheMock.Object));
        }

        public VisitPayUserAddressVerificationProviderMockBuilder Setup(
            Action<VisitPayUserAddressVerificationProviderMockBuilder> configAction)
        {
            configAction(this);
            return this;
        }
    }
}

﻿namespace Ivh.Provider.Tests.User
{
    using Application.Base.Common.Dtos;
    using Common.Web.Interfaces;
    using Domain.Settings.Entities;
    using Domain.Settings.Interfaces;
    using Domain.User.Entities;
    using Domain.User.Interfaces;
    using Moq;
    using Moq.Protected;
    using Newtonsoft.Json;
    using NUnit.Framework;
    using Provider.User;
    using System.Collections.Generic;
    using System.Linq;
    using System.Net;
    using System.Net.Http;
    using System.Threading;
    using System.Threading.Tasks;

    [TestFixture]
    public class VisitPayUserAddressVerificationProviderTests
    {
        private const string HandlerMethodName = "SendAsync";

        private const string Address2 = "517 S MIDDLETON RD";
        private const string City = "MIDDLETON";
        private const string State = "ID";
        private const string Zip5 = "83644";
        private const string Zip4 = "5992";

        [Test]
        public async Task GetStandardizedMailingAddress_ApiReturnsVerifiedAddress_ValidUspsAddressResult()
        {
            string apiResponsePayload = $"<AddressValidateResponse>\r\n\t<Address ID=\"0\">\r\n\t\t<Address2>{Address2}</Address2>\r\n\t\t<City>{City}</City>\r\n\t\t<State>{State}</State>\r\n\t\t<Zip5>{Zip5}</Zip5>\r\n\t\t<Zip4>{Zip4}</Zip4>\r\n\t</Address>\r\n</AddressValidateResponse>";
            Mock<HttpMessageHandler> handlerMock = new Mock<HttpMessageHandler>(MockBehavior.Loose);

            VisitPayUser user = CreateVisitPayUser();

            handlerMock
                .Protected()
                .Setup<Task<HttpResponseMessage>>(
                    HandlerMethodName,
                    ItExpr.IsAny<HttpRequestMessage>(),
                    ItExpr.IsAny<CancellationToken>())
                .ReturnsAsync(new HttpResponseMessage
                {
                    StatusCode = HttpStatusCode.OK,
                    Content = new StringContent(apiResponsePayload),
                })
                .Verifiable();

            IVisitPayUserAddressVerificationProvider vpUserAddressVerificationProvider = this.CreateService(handlerMock.Object);

            DataResult<IEnumerable<string>, bool> dataResult =
                await vpUserAddressVerificationProvider
                    .GetStandardizedMailingAddressAsync(
                        user.VisitPayUserId,
                        user.CurrentMailingAddress,
                        false);

            UspsAddressDto uspsAddress = JsonConvert.DeserializeObject<UspsAddressDto>(dataResult.Data.First());

            Assert.AreEqual(null, uspsAddress.Address2);
            Assert.AreEqual(Address2, uspsAddress.Address1);
            Assert.AreEqual(City, uspsAddress.City);
            Assert.AreEqual(State, uspsAddress.State);
            Assert.AreEqual(Zip5, uspsAddress.Zip5);
            Assert.AreEqual(Zip4, uspsAddress.Zip4);
            Assert.IsTrue(dataResult.Result);
        }

        [Test]
        public async Task GetStandardizedMailingAddress_ApiReturnsErrorResponse_FalseResultAndErrorDescriptionDataAsync()
        {
            const string errorDescription = "The element 'Address' has incomplete content. List of possible elements expected: 'Zip4'.";
            string apiResponsePayload = $"<Error>\r\n  <Number>-2147221202</Number>\r\n  <Source>Common:XmlParse</Source>\r\n  <Description>{errorDescription}</Description>\r\n  <HelpFile />\r\n  <HelpContext />\r\n</Error>";
            Mock<HttpMessageHandler> handlerMock = new Mock<HttpMessageHandler>(MockBehavior.Loose);
            VisitPayUser user = CreateVisitPayUser();

            handlerMock
                .Protected()
                .Setup<Task<HttpResponseMessage>>(
                    HandlerMethodName,
                    ItExpr.IsAny<HttpRequestMessage>(),
                    ItExpr.IsAny<CancellationToken>())
                .ReturnsAsync(new HttpResponseMessage
                {
                    StatusCode = HttpStatusCode.OK,
                    Content = new StringContent(apiResponsePayload),
                });

            IVisitPayUserAddressVerificationProvider vpUserAddressVerificationProvider = this.CreateService(handlerMock.Object);

            DataResult<IEnumerable<string>, bool> dataResult =
                await vpUserAddressVerificationProvider
                    .GetStandardizedMailingAddressAsync(
                        user.VisitPayUserId,
                        user.CurrentMailingAddress,
                        false);

            Assert.AreEqual(errorDescription, dataResult.Data.First());
            Assert.IsFalse(dataResult.Result);
        }

        [Test]
        public async Task GetStandardizedMailingAddress_ApiCallExceedsTimeout_FalseResultAndErrorDescriptionDataAsync()
        {
            Mock<HttpMessageHandler> handlerMock = new Mock<HttpMessageHandler>(MockBehavior.Loose);
            VisitPayUser user = CreateVisitPayUser();

            // HttpClient will throw a TaskCanceledException (not TimeoutException) on timeout
            handlerMock
                .Protected()
                .Setup<Task<HttpResponseMessage>>(
                    HandlerMethodName,
                    ItExpr.IsAny<HttpRequestMessage>(),
                    ItExpr.IsAny<CancellationToken>())
                .Throws(new TaskCanceledException());

            IVisitPayUserAddressVerificationProvider vpUserAddressVerificationProvider = this.CreateService(handlerMock.Object);

            DataResult<IEnumerable<string>, bool> dataResult =
                await vpUserAddressVerificationProvider
                    .GetStandardizedMailingAddressAsync(
                        user.VisitPayUserId,
                        user.CurrentMailingAddress,
                        false);

            Assert.AreEqual(VisitPayUserAddressVerificationProvider.ExternalCallFailedErrorMessage, dataResult.Data.First());
            Assert.IsFalse(dataResult.Result);
        }

        private static VisitPayUser CreateVisitPayUser()
        {
            return new VisitPayUser
            {
                VisitPayUserId = 345678,
                CurrentMailingAddress = new VisitPayUserAddress
                {
                    AddressStreet1 = Address2,
                    City = City,
                    StateProvince = State,
                    PostalCode = Zip5
                }
            };
        }

        private IVisitPayUserAddressVerificationProvider CreateService(HttpMessageHandler msgHandler)
        {
            VisitPayUserAddressVerificationProviderMockBuilder providerMockBuilder = new VisitPayUserAddressVerificationProviderMockBuilder();

            Mock<IApplicationSettingsService> appSettingsSvcMock = new Mock<IApplicationSettingsService>();
            appSettingsSvcMock.SetupGet(builder => builder.UspsApiUrlHostname).Returns(new ApplicationSetting<string>("UspsApi.Url.Hostname", new Dictionary<string, string>(), () => "host"));
            appSettingsSvcMock.SetupGet(builder => builder.UspsApiUrlPath).Returns(new ApplicationSetting<string>("UspsApi.Url.Path", new Dictionary<string, string>(), () => "path"));
            appSettingsSvcMock.Setup(builder => builder.GetAddressVerificationProviderCredentials()).Returns(new AddressVerificationProviderCredentials("id", "pwd"));

            Mock<IHttpMessageHandlerFactory> httpMsgHandlerFactoryMock = new Mock<IHttpMessageHandlerFactory>();
            httpMsgHandlerFactoryMock.Setup(f => f.Create()).Returns(msgHandler);

            Mock<IStaticHttpClientManager> staticHttpClientManagerMock = new Mock<IStaticHttpClientManager>();
            staticHttpClientManagerMock.Setup(m => m.UseExistingStaticHttpClient).Returns(false);

            providerMockBuilder.Setup(builder =>
            {
                builder.ApplicationSettingsService = appSettingsSvcMock.Object;
                builder.HttpMessageHandlerFactory = httpMsgHandlerFactoryMock.Object;
                builder.StaticHttpClientManager = staticHttpClientManagerMock.Object;
            });

            IVisitPayUserAddressVerificationProvider vpUserAddressVerificationProvider = providerMockBuilder.CreateService();
            return vpUserAddressVerificationProvider;
        }
    }
}

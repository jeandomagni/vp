﻿namespace Ivh.Provider.Tests.User
{
    using System;
    using System.Xml;
    using Common.VisitPay.Enums;
    using Domain.Settings.Entities;
    using Domain.User.Entities;
    using FluentAssertions;
    using NUnit.Framework;
    using Provider.User;

    [TestFixture]
    public class UspsAddressValidationTests
    {
        [Test]
        public void UspsAddressValidateRequest_ValidVpUserAddress_ValidXmlOutputWithUspsExpectedFormat()
        {
            const string userId = "userId345";
            const string password = "password!";
            string expectedXmlOutput = $"<AddressValidateRequest USERID=\"{userId}\" PASSWORD=\"{password}\">\r\n  <Address ID=\"0\">\r\n    <Address1>9297 Forest Bluffs View</Address1>\r\n    <Address2></Address2>\r\n    <City>Colorado Springs</City>\r\n    <State>CO</State>\r\n    <Zip5>80920</Zip5>\r\n    <Zip4></Zip4>\r\n  </Address>\r\n</AddressValidateRequest>";

            VisitPayUserAddress theBestTexMex = new VisitPayUserAddress
            {
                AddressStreet1 = "9297 Forest Bluffs View",
                AddressStreet2 = null,
                City = "Colorado Springs",
                StateProvince = "CO",
                PostalCode = "80920",
                VisitPayUserAddressType = VisitPayUserAddressTypeEnum.Mailing,
                AddressStatus = AddressStatusEnum.Unverified
            };

            AddressVerificationProviderCredentials credentials = new AddressVerificationProviderCredentials(userId, password);

            UspsAddressValidationRequest validationRequest = new UspsAddressValidationRequest(theBestTexMex, credentials);
            string xmlString = validationRequest.ToString();

            Assert.AreEqual(expectedXmlOutput, xmlString);
        }

        [Test]
        public void UspsAddressValidateRequest_ValidVpUserAddressWithNineDigitZip_ValidXmlOutputWithUspsExpectedFormat()
        {
            const string userId = "userId345";
            const string password = "password!";
            string expectedXmlOutput = $"<AddressValidateRequest USERID=\"{userId}\" PASSWORD=\"{password}\">\r\n  <Address ID=\"0\">\r\n    <Address1>9297 Forest Bluffs View</Address1>\r\n    <Address2></Address2>\r\n    <City>Colorado Springs</City>\r\n    <State>CO</State>\r\n    <Zip5>80920</Zip5>\r\n    <Zip4>4090</Zip4>\r\n  </Address>\r\n</AddressValidateRequest>";

            VisitPayUserAddress theBestTexMex = new VisitPayUserAddress
            {
                AddressStreet1 = "9297 Forest Bluffs View",
                AddressStreet2 = null,
                City = "Colorado Springs",
                StateProvince = "CO",
                PostalCode = "80920-4090",
                VisitPayUserAddressType = VisitPayUserAddressTypeEnum.Mailing,
                AddressStatus = AddressStatusEnum.Unverified
            };

            AddressVerificationProviderCredentials credentials = new AddressVerificationProviderCredentials(userId, password);

            UspsAddressValidationRequest validationRequest = new UspsAddressValidationRequest(theBestTexMex, credentials);
            string xmlString = validationRequest.ToString();

            Assert.AreEqual(expectedXmlOutput, xmlString);
        }

        [Test]
        public void UspsAddressValidateResponse_ValidXmlPayload_ValidDeserializedResult()
        {
            const string address1 = "123 MAIN ST";
            const string address2 = "APT 202";
            const string city = "MIDDLETON";
            const string state = "ID";
            const string zip5 = "83644";
            const string zip4 = "5992";
            string xmlPayload = $"<?xml version=\"1.0\" encoding=\"UTF-8\"?>\r\n<AddressValidateResponse>\r\n\t<Address ID=\"0\">\r\n\t<Address1>{address1}</Address1>\r\n\t<Address2>{address2}</Address2>\r\n\t<City>{city}</City>\r\n\t<State>{state}</State>\r\n\t<Zip5>{zip5}</Zip5>\r\n\t<Zip4>{zip4}</Zip4>\r\n\t</Address>\r\n\t</AddressValidateResponse>";

            UspsAddressValidationResponse response = new UspsAddressValidationResponse(xmlPayload);
            UspsApiResponse result = response.DeserializedResult;

            Assert.IsInstanceOf<AddressValidationSuccessResponse>(result);
            AddressValidationSuccessResponse validationSuccessResponse = result as AddressValidationSuccessResponse;

            Assert.NotNull(validationSuccessResponse);
            Assert.AreEqual(address1, validationSuccessResponse.Address.Address1);
            Assert.AreEqual(address2, validationSuccessResponse.Address.Address2);
            Assert.AreEqual(city, validationSuccessResponse.Address.City);
            Assert.AreEqual(state, validationSuccessResponse.Address.State);
            Assert.AreEqual(zip5, validationSuccessResponse.Address.Zip5);
            Assert.AreEqual(zip4, validationSuccessResponse.Address.Zip4);
        }

        [Test]
        public void UspsAddressValidateResponse_ErrorXmlPayload_ValidDeserializedResult()
        {
            const string xmlPayload = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\r\n<Error>\r\n\t<Number>-2147221202</Number>\r\n\t<Source>Common:XmlParse</Source>\r\n\t<Description>The element 'Address' has incomplete content. List of possible elements expected: 'Zip4'.</Description>\r\n\t<HelpFile/><HelpContext/>\r\n</Error>";

            UspsAddressValidationResponse response = new UspsAddressValidationResponse(xmlPayload);
            UspsApiResponse result = response.DeserializedResult;

            Assert.IsInstanceOf<AddressValidationErrorResponse>(result);
            AddressValidationErrorResponse errorResponse = result as AddressValidationErrorResponse;

            Assert.NotNull(errorResponse);
            Assert.IsNotEmpty(errorResponse.Number);
            Assert.IsNotEmpty(errorResponse.Source);
            Assert.IsNotEmpty(errorResponse.Description);
        }

        [Test]
        public void UspsAddressValidateResponse_SuccessWithNestedErrorXmlPayload_ValidDeserializedResult()
        {
            const string xmlPayload = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\r\n<AddressValidateResponse>\r\n<Address ID=\"0\">\r\n<Error>\r\n<Number>-2147219401</Number>\r\n<Source>clsAMS</Source>\r\n<Description>Address Not Found.  </Description>\r\n<HelpFile/>\r\n<HelpContext/>\r\n</Error>\r\n</Address>\r\n</AddressValidateResponse>";

            UspsAddressValidationResponse response = new UspsAddressValidationResponse(xmlPayload);
            UspsApiResponse result = response.DeserializedResult;

            Assert.IsInstanceOf<AddressValidationResponseWithNestedError>(result);
            AddressValidationResponseWithNestedError errorResponse = result as AddressValidationResponseWithNestedError;

            Assert.NotNull(errorResponse);
            Assert.NotNull(errorResponse.AddressWithError);
            Assert.NotNull(errorResponse.AddressWithError.Error);
            Assert.IsNotEmpty(errorResponse.AddressWithError.Error.Number);
            Assert.IsNotEmpty(errorResponse.AddressWithError.Error.Source);
            Assert.IsNotEmpty(errorResponse.AddressWithError.Error.Description);
        }

        [Test]
        public void UspsAddressValidateResponse_PotentialBillionLaughsAttackXmlPayload_ThrowsXmlException()
        {
            const string xmlPayload = "<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?> \r\n<!DOCTYPE foo [\r\n  <!ELEMENT foo ANY>\r\n  <!ENTITY bar \"World \">\r\n  <!ENTITY t1 \"&bar;&bar;\">\r\n  <!ENTITY t2 \"&t1;&t1;&t1;&t1;\">\r\n  <!ENTITY t3 \"&t2;&t2;&t2;&t2;&t2;\">\r\n]>\r\n<foo>\r\n  Hello &t3;\r\n</foo>\r\n";

            const string expectedInnerExceptionMessage = "DTD is prohibited in this XML document.";

            Action uspsAddressValidationResponseCtor = () => new UspsAddressValidationResponse(xmlPayload);

            uspsAddressValidationResponseCtor
                .Should().Throw<InvalidOperationException>()
                .WithInnerException<XmlException>()
                .WithMessage(expectedInnerExceptionMessage);
        }
    }
}

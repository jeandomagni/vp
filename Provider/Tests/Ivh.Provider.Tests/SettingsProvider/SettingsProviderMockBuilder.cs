namespace Ivh.Provider.Tests.SettingsProvider
{
    using System;
    using Common.Base.Utilities.Helpers;
    using Common.Cache;
    using Common.Configuration.Interfaces;
    using Common.VisitPay.Enums;
    using Domain.Settings.Interfaces;
    using Logging.Database.Interfaces;
    using Moq;
    using NHibernate;
    using Settings;

    public class SettingsProviderMockBuilder
    {
        public Mock<IClientSettingRepository> ClientSettingRepositoryMock;
        public Mock<ISession> Session;
        public Mock<ICache> CacheMock;
        public Mock<IAppSettingsProvider> AppSettingsProviderMock;
        public SettingsProviderParameters SettingsProviderParameters;

        public SettingsProviderMockBuilder()
        {
            Mock<ITransaction> transactionMock = new Mock<ITransaction>();
            transactionMock.SetupGet(x => x.IsActive).Returns(true);
            this.Session = new Mock<ISession>();
            this.Session.SetupGet(x => x.Transaction).Returns(transactionMock.Object);
            this.Session.Setup(x => x.BeginTransaction()).Returns(transactionMock.Object);
            
            this.ClientSettingRepositoryMock = new Mock<IClientSettingRepository>();
            this.CacheMock = new Mock<ICache>();
            this.AppSettingsProviderMock = new Mock<IAppSettingsProvider>();

            this.SettingsProviderParameters = new SettingsProviderParameters()
            {
                Application = ApplicationEnum.ClientDataApi,
                ApplicationName = "ClientDataApi",
                ClientDataApiAppId = "57116b4edbe44004bb7fee36fd25318d",
                ClientDataApiUrl = "http://localhost:15647/",
                ClientDataApiTimeToLive = 15,
                ClientDataApiAppKey = "G7EcX5aAICwVHZEFKicUtVUgIAA4xWHWIOj96wlCs3A="
            };
        }

        public SettingsProvider CreateSettingsProvider()
        {
            return new SettingsProvider(
                new Lazy<IClientSettingRepository>(() => this.ClientSettingRepositoryMock.Object),
                new Lazy<ICache>(() => this.CacheMock.Object),
                new Lazy<IAppSettingsProvider>(()=> this.AppSettingsProviderMock.Object),
                this.SettingsProviderParameters
            );
        }

        public void Setup(Action<SettingsProviderMockBuilder> configAction)
        {
            configAction(this);
        }
    }
}
﻿namespace Ivh.Provider.Tests.SettingsProvider
{
    using System;
    using Common.ResiliencePolicies.Interfaces;
    using Common.Tests.Helpers;
    using Common.VisitPay.Enums;
    using Logging.Database.Interfaces;
    using Moq;
    using Settings;

    public class SettingsProviderClientMockBuilder : IMockBuilder<SettingsProviderClient, SettingsProviderClientMockBuilder>
    {
        public IHttpRequestPolicy HttpRequestPolicy;

        public Mock<IHttpRequestPolicy> HttpRequestPolicyMock;
        public Mock<ILoggingProvider> LoggerMock;
        public SettingsProviderParameters SettingsProviderParameters;

        public SettingsProviderClientMockBuilder()
        {
            this.LoggerMock = new Mock<ILoggingProvider>();
            this.HttpRequestPolicyMock = new Mock<IHttpRequestPolicy>();
            this.SettingsProviderParameters = new SettingsProviderParameters()
            {
                Application = ApplicationEnum.ClientDataApi,
                ApplicationName = "ClientDataApi",
                ClientDataApiAppId = "57116b4edbe44004bb7fee36fd25318d",
                ClientDataApiUrl = "http://localhost:15647/",
                ClientDataApiTimeToLive = 15,
                ClientDataApiAppKey = "G7EcX5aAICwVHZEFKicUtVUgIAA4xWHWIOj96wlCs3A="
            };
        }

        public SettingsProviderClient CreateService()
        {
            return new SettingsProviderClient(
                new Lazy<IHttpRequestPolicy>(() => this.HttpRequestPolicy ?? this.HttpRequestPolicyMock.Object),
                new Lazy<ILoggingProvider>(() => this.LoggerMock.Object),
                this.SettingsProviderParameters
            );
        }

        public SettingsProviderClientMockBuilder Setup(Action<SettingsProviderClientMockBuilder> configAction)
        {
            configAction(this);
            return this;
        }

    }
}

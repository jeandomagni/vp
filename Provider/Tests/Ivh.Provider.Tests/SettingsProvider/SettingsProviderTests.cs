﻿namespace Ivh.Provider.Tests.SettingsProvider
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Net;
    using System.Reflection;
    using System.Threading;
    using System.Threading.Tasks;
    using Common.Base.Collections;
    using Common.Cache;
    using Common.ResiliencePolicies.Policies;
    using Common.Tests.Builders;
    using Common.VisitPay.Enums;
    using Domain.Settings.Entities;
    using HttpMock;
    using Moq;
    using Newtonsoft.Json;
    using NUnit.Framework;
    using Polly.Caching;
    using Settings;

    [TestFixture]
    [Parallelizable(ParallelScope.None)]
    public class SettingsProviderTests
    {
        [SetUp]
        public void ResetPolicy()
        {
            FieldInfo policy = typeof(SettingsProviderClient).GetField("_requestPolicy", BindingFlags.Static | BindingFlags.NonPublic);

            if (policy == null)
            {
                throw new ArgumentException($"unable to find {nameof(SettingsProviderClient)}_requestPolicy.");
            }

            policy?.SetValue(null, null);
        }


        [Test]
        [Parallelizable(ParallelScope.None)]
        public void BasicConstructorTest()
        {
            SettingsProviderMockBuilder builder = new SettingsProviderMockBuilder();
            builder.Setup((x) =>
            {
                //Nothing for this one
            });

            SettingsProvider settingsProvider = builder.CreateSettingsProvider();

            Assert.IsNotNull(settingsProvider);
        }

        [Test]
        [Parallelizable(ParallelScope.None)]
        public void AsClientDataApi_GetAllAsync_OneThreadTest()
        {
            SettingsProviderMockBuilder builder = new SettingsProviderMockBuilder();
            builder.Setup((x) =>
            {
                x.ClientSettingRepositoryMock.Setup(y => y.GetAllSettings()).Returns(new List<ClientSetting>()
                {
                    new ClientSetting()
                    {
                        ClientSettingKey = "TestKey",
                        ClientSettingValue = "TestValue"
                    }
                });
                //Nothing for this one
            });

            SettingsProvider settingsProvider = builder.CreateSettingsProvider();

            IReadOnlyDictionary<string, string> settings = settingsProvider.GetAllSettings();
            Assert.IsNotNull(settings);
            Assert.Greater(settings.Keys.Count(), 0);
        }

        [Test]
        [Parallelizable(ParallelScope.None)]
        public void AsClientDataApi_GetAllAsync_Slower_OneThreadTest()
        {

            SettingsProviderMockBuilder builder = new SettingsProviderMockBuilder();
            builder.Setup((x) =>
            {
                Random r = new Random(DateTime.UtcNow.Millisecond);

                x.ClientSettingRepositoryMock.Setup(y => y.GetAllSettings()).Callback(() => { Thread.Sleep(r.Next(40, 200)); })
                    .Returns(new List<ClientSetting>() { new ClientSetting() { ClientSettingKey = "TestKey", ClientSettingValue = "TestValue" } });
                //Nothing for this one
            });

            SettingsProvider settingsProvider = builder.CreateSettingsProvider();

            IReadOnlyDictionary<string, string> settings = settingsProvider.GetAllSettings();
            Assert.IsNotNull(settings);
            Assert.Greater(settings.Keys.Count(), 0);
        }

        [Test]
        [Parallelizable(ParallelScope.None)]
        public void AsClientDataApi_GetAllAsync_Slower_ManyThreadTest()
        {
            int maxCount = 20;
            SemaphoreSlim semaphore = new SemaphoreSlim(0, maxCount);
            for (int i = 0; i < maxCount; i++)
            {
                //Modified
                int i3 = i;
                ThreadPool.QueueUserWorkItem(delegate
                {
                    try
                    {
                        SettingsProviderMockBuilder builder = new SettingsProviderMockBuilder();
                        builder.Setup((x) =>
                        {
                            Random r = new Random(DateTime.UtcNow.Millisecond);

                            x.ClientSettingRepositoryMock.Setup(y => y.GetAllSettings()).Callback(() =>
                                {
                                    int millisecondsTimeout = r.Next(40, 200);
                                    Console.WriteLine($"Thread {i3}, is waiting {millisecondsTimeout}");
                                    Thread.Sleep(millisecondsTimeout);
                                })
                                .Returns(new List<ClientSetting>() { new ClientSetting() { ClientSettingKey = "TestKey", ClientSettingValue = "TestValue" } });
                        });

                        SettingsProvider settingsProvider = builder.CreateSettingsProvider();

                        IReadOnlyDictionary<string, string> settings = settingsProvider.GetAllSettings();
                        Assert.IsNotNull(settings);
                        Assert.Greater(settings.Keys.Count(), 0);
                    }
                    finally
                    {
                        semaphore.Release();
                    }
                });
            }

            for (int i = 0; i < maxCount; i++)
            {
                semaphore.Wait();
                Console.WriteLine("Finished {0}", i);
            }
        }

        [Test]
        [Parallelizable(ParallelScope.None)]
        public void AsPatient_GetAllAsync_ItsInTheCache()
        {
            CaseInsensitiveDictionary<string> mySettingsToReturn = new CaseInsensitiveDictionary<string>()
            {
                {"TestKey", "TestValue"},
            };

            SettingsProviderClientMockBuilder builder = new SettingsProviderClientMockBuilder();
            builder.Setup((x) =>
            {
                x.SettingsProviderParameters.ApplicationName = "Patient";
                x.SettingsProviderParameters.Application = ApplicationEnum.VisitPay;
                x.SettingsProviderParameters.ClientDataApiUrl = "http://localhost:9191";
                
                HttpRequestPolicyMockBuilder requestPolicyMockBuilder = new HttpRequestPolicyMockBuilder();
                requestPolicyMockBuilder.CacheProviderMock.As<IAsyncCacheProvider>().Setup(y => y.GetAsync(It.IsAny<string>(), It.IsAny<CancellationToken>(), It.IsAny<bool>())).ReturnsAsync(() => mySettingsToReturn);
                requestPolicyMockBuilder.DistributedCacheMock.Setup(y => y.GetObjectAsync<CaseInsensitiveDictionary<string>>(It.IsAny<string>())).Returns(Task.FromResult(mySettingsToReturn));
                requestPolicyMockBuilder.DistributedCacheMock.Setup(y => y.GetStringAsync(It.IsAny<string>())).Returns(Task.FromResult("string"));
                requestPolicyMockBuilder.DistributedCacheMock.Setup(y => y.Deserialize<CaseInsensitiveDictionary<string>>(It.IsAny<string>())).Returns(mySettingsToReturn);
                requestPolicyMockBuilder.SimpleCircuitBreakerPolicy = new SimpleCircuitBreakerPolicy();

                x.HttpRequestPolicy = requestPolicyMockBuilder.CreateService();
            });

            SettingsProviderClient settingsProvider = builder.CreateService();

            IReadOnlyDictionary<string, string> settings = settingsProvider.GetAllSettings();
            Assert.IsNotNull(settings);
            Assert.Greater(settings.Keys.Count(), 0);
        }

        [Test]
        [Parallelizable(ParallelScope.None)]
        public void AsPatient_GetAllAsync_Its_NotInCache_ApiResponds()
        {
            CaseInsensitiveDictionary<string> mySettingsToReturn = new CaseInsensitiveDictionary<string>()
            {
                {"TestKey", "TestValue"},
            };

            SettingsProviderClientMockBuilder builder = new SettingsProviderClientMockBuilder();
            builder.Setup((x) =>
            {
                x.SettingsProviderParameters.ApplicationName = "Patient";
                x.SettingsProviderParameters.Application = ApplicationEnum.VisitPay;
                x.SettingsProviderParameters.ClientDataApiUrl = "http://localhost:9191/";
                
                HttpRequestPolicyMockBuilder requestPolicyMockBuilder = new HttpRequestPolicyMockBuilder();
                requestPolicyMockBuilder.CacheProviderMock.As<IAsyncCacheProvider>().Setup(y => y.GetAsync(It.IsAny<string>(), It.IsAny<CancellationToken>(), It.IsAny<bool>())).ReturnsAsync(() => null);
                requestPolicyMockBuilder.SimpleCircuitBreakerPolicy = new SimpleCircuitBreakerPolicy();

                x.HttpRequestPolicy = requestPolicyMockBuilder.CreateService();

            });
            string serizlizedSettings = JsonConvert.SerializeObject(mySettingsToReturn);

            IHttpServer _stubHttp = HttpMockRepository.At(builder.SettingsProviderParameters.ClientDataApiUrl.TrimEnd('/'));
            _stubHttp.Stub(x => x.Get("/api/settings/getsettings"))
                .Return(serizlizedSettings)
                .OK();

            SettingsProviderClient settingsProvider = builder.CreateService();

            IReadOnlyDictionary<string, string> settings = settingsProvider.GetAllSettings();
            Assert.IsNotNull(settings);
            Assert.Greater(settings.Keys.Count(), 0);
        }

        [Test]
        [Parallelizable(ParallelScope.None)]
        public void AsPatient_GetAllAsync_Its_NotInCache_ApiRespondsWithError_BackupWorks()
        {
            CaseInsensitiveDictionary<string> mySettingsToReturn = new CaseInsensitiveDictionary<string>()
            {
                {"TestKey", "TestValue"},
            };

            SettingsProviderClientMockBuilder builder = new SettingsProviderClientMockBuilder().Setup((x) =>
            {
                x.SettingsProviderParameters.ApplicationName = "Patient";
                x.SettingsProviderParameters.Application = ApplicationEnum.VisitPay;
                x.SettingsProviderParameters.ClientDataApiUrl = "http://localhost:9191/";
            });

            HttpRequestPolicyMockBuilder requestPolicyMockBuilder = new HttpRequestPolicyMockBuilder();
            requestPolicyMockBuilder.CacheProviderMock.As<IAsyncCacheProvider>().Setup(y => y.GetAsync(It.IsAny<string>(), It.IsAny<CancellationToken>(), It.IsAny<bool>())).ReturnsAsync(() => null);
            requestPolicyMockBuilder.DistributedCacheMock.Setup(y => y.GetObjectAsync<CaseInsensitiveDictionary<string>>(It.IsAny<string>())).Returns(Task.FromResult(mySettingsToReturn));
            requestPolicyMockBuilder.DistributedCacheMock.Setup(y => y.GetStringAsync(It.IsAny<string>())).Returns(Task.FromResult("string"));
            requestPolicyMockBuilder.DistributedCacheMock.Setup(y => y.Deserialize<CaseInsensitiveDictionary<string>>(It.IsAny<string>())).Returns(mySettingsToReturn);
            requestPolicyMockBuilder.SimpleCircuitBreakerPolicy = new SimpleCircuitBreakerPolicy(); 

            builder.HttpRequestPolicy = requestPolicyMockBuilder.CreateService();

            IHttpServer _stubHttp = HttpMockRepository.At(builder.SettingsProviderParameters.ClientDataApiUrl.TrimEnd('/'));
            _stubHttp.Stub(x => x.Get("/api/settings/getsettings"))
                .Return("")
                .WithStatus(HttpStatusCode.InternalServerError);

            SettingsProviderClient settingsProvider = builder.CreateService();

            IReadOnlyDictionary<string, string> settings = settingsProvider.GetAllSettings();
            Assert.IsNotNull(settings);
            Assert.Greater(settings.Keys.Count(), 0);
        }

        [Test]
        [Parallelizable(ParallelScope.None)]
        public void AsPatient_GetAllAsync_Its_NotInCache_ApiRespondsWithError_BackupWorks_MultiThreaded()
        {
            CaseInsensitiveDictionary<string> mySettingsToReturn = new CaseInsensitiveDictionary<string>()
            {
                {"TestKey", "TestValue"},
            };
            string clientDataApiUrl = "http://localhost:9093/";

            //Not sure this HttpMockRepo is threadsafe...
            IHttpServer _stubHttp = HttpMockRepository.At(clientDataApiUrl.TrimEnd('/'));
            _stubHttp.Stub(x => x.Get("/api/settings/getsettings"))
                .Return("")
                .WithStatus(HttpStatusCode.InternalServerError);

            int maxCount = 20;
            SemaphoreSlim semaphore = new SemaphoreSlim(0, maxCount);
            for (int i = 0; i < maxCount; i++)
            {
                //Modified
                int i3 = i;
                ThreadPool.QueueUserWorkItem(delegate
                {
                    try
                    {
                        SettingsProviderClientMockBuilder builder = new SettingsProviderClientMockBuilder();
                        builder.Setup((x) =>
                        {
                            x.SettingsProviderParameters.ApplicationName = "Patient";
                            x.SettingsProviderParameters.Application = ApplicationEnum.VisitPay;
                            x.SettingsProviderParameters.ClientDataApiUrl = clientDataApiUrl;

                            HttpRequestPolicyMockBuilder requestPolicyMockBuilder = new HttpRequestPolicyMockBuilder();
                            requestPolicyMockBuilder.CacheProviderMock.As<IAsyncCacheProvider>().Setup(y => y.GetAsync(It.IsAny<string>(), It.IsAny<CancellationToken>(), It.IsAny<bool>())).ReturnsAsync(() => null);
                            requestPolicyMockBuilder.DistributedCacheMock.Setup(y => y.GetObjectAsync<CaseInsensitiveDictionary<string>>(It.IsAny<string>())).Returns(Task.FromResult(mySettingsToReturn));
                            requestPolicyMockBuilder.DistributedCacheMock.Setup(y => y.GetStringAsync(It.IsAny<string>())).Returns(Task.FromResult("string"));
                            requestPolicyMockBuilder.DistributedCacheMock.Setup(y => y.Deserialize<CaseInsensitiveDictionary<string>>(It.IsAny<string>())).Returns(mySettingsToReturn);
                            requestPolicyMockBuilder.SimpleCircuitBreakerPolicy = new SimpleCircuitBreakerPolicy();

                            x.HttpRequestPolicy = requestPolicyMockBuilder.CreateService();
                        });

                        SettingsProviderClient settingsProvider = builder.CreateService();

                        IReadOnlyDictionary<string, string> settings = settingsProvider.GetAllSettings();
                        Assert.IsNotNull(settings);
                        Assert.Greater(settings.Keys.Count(), 0);
                        Console.WriteLine($"Thread {i3} successful");
                    }
                    finally
                    {
                        semaphore.Release();
                    }
                });
            }
            for (int i = 0; i < maxCount; i++)
            {
                semaphore.Wait();
                Console.WriteLine("Finished {0}", i);
            }
        }
    }
}

﻿namespace Ivh.Provider.Tests.Twilio
{
    using System;
    using Common.Cache;
    using Common.VisitPay.Enums;
    using Domain.Email.Entities;
    using Domain.Email.Interfaces;
    using Domain.Logging.Interfaces;
    using Domain.Settings.Entities;
    using Domain.Settings.Enums;
    using Domain.Settings.Interfaces;
    using Ivh.Provider.Communication.Twilio;
    using Moq;
    using NUnit.Framework;

    [TestFixture]
    public class SendCommunicationTests
    {
        private const string ValidTestNumber = "+15005550006";

        private ISmsCommunicationProvider CreateProvider()
        {
            Mock<ILoggingProvider> mockLogger = new Mock<ILoggingProvider>();
            Mock<IApplicationSettingsService> mockApplicationSettings = new Mock<IApplicationSettingsService>();
            Mock<IApplicationSetting<IvhEnvironmentTypeEnum>> mockEnvironmentType = new Mock<IApplicationSetting<IvhEnvironmentTypeEnum>>();
            mockApplicationSettings.SetupGet(s => s.EnvironmentType).Returns(mockEnvironmentType.Object);
            mockApplicationSettings.SetupGet(s => s.TwilioAccountSid).Returns(() => new ApplicationSetting<string>(() => new Lazy<string>(() => "")));
            mockApplicationSettings.SetupGet(s => s.TwilioAuthToken).Returns(() => new ApplicationSetting<string>(() => new Lazy<string>(() => "")));
            mockApplicationSettings.SetupGet(s => s.TwilioMessagingServiceSid).Returns(() => new ApplicationSetting<string>(() => new Lazy<string>(() => "")));
            mockApplicationSettings.Setup(s => s.GetUrl(UrlEnum.TwilioCallbackBase)).Returns(() => new Uri("http://localhost:47696/"));
            mockApplicationSettings.SetupGet(s => s.TwilioCallbackUsername).Returns(() => new ApplicationSetting<string>(() => new Lazy<string>(() => "")));
            mockApplicationSettings.SetupGet(s => s.TwilioCallbackPassword).Returns(() => new ApplicationSetting<string>(() => new Lazy<string>(() => "")));
            mockApplicationSettings.Setup(s => s.GetUrl(UrlEnum.TwilioApi)).Returns(() => new Uri("http://localhost:47696/"));
            Mock<IDistributedCache> mockDistributedCache = new Mock<IDistributedCache>();
            Mock<Lazy<IMetricsProvider>> mockMetricsProvider = new Mock<Lazy<IMetricsProvider>>();
            ISmsCommunicationProvider provider = new TwilioSmsProvider(
                mockLogger.Object,
                mockApplicationSettings.Object, 
                mockDistributedCache.Object, 
                mockMetricsProvider.Object);
            return provider;
        }

        [Test]
        public void SendToValidNumber()
        {
            Communication communication = new Communication
            {
                ToAddress = ValidTestNumber,
                Body = "Twilio::SendToValidNumber"
            };
            ISmsCommunicationProvider provider = this.CreateProvider();
            provider.SendCommunication(communication);
        }
    }
}
﻿namespace Ivh.Provider.Tests.TrustCommerce
{
    using Autofac;
    using Common.DependencyInjection;
    using Common.DependencyInjection.Registration;
    using Common.DependencyInjection.Registration.Register;
    using Common.VisitPay.Enums;
    using Domain.FinanceManagement.Payment.Entities;
    using Domain.GuestPay.Interfaces;
    using NUnit.Framework;
    using Payment = Domain.GuestPay.Entities.Payment;

    public class RegistrationModule : IRegistrationModule
    {
        public void Register(ContainerBuilder builder)
        {
            RegistrationSettings registrationSettings = RegistrationSettingsService.GetRegistrationSettings();
            VisitPay.Register(builder, registrationSettings, true);
            Data.Register(builder, registrationSettings)
                .AddVisitPayDatabase()
                .AddCdiEtlDatabase()
                .AddEnterpriseDatabase()
                .AddGuestPayDatabase()
                .AddStorageDatabase();
        }
    }

    [TestFixture, Ignore("This doesn't work with Assembly.GetEntryAssembly(); for the appSetting 'ServiceBusName'")]
    public class RefundAndVoidTests
    {
        [OneTimeSetUp]
        public void BeforeAllTests()
        {
            using (MappingBuilder mappingBuilder = new MappingBuilder())
            {
                mappingBuilder.WithBase();
            }

            IvinciContainer.Instance.RegisterComponent(new RegistrationModule());
        }

        private Payment CreateBasicPayment(decimal amount)
        {
            return new Payment
            {
                CardAccountHolderName = "Statement Guy",
                //CardNumber = "4111111111111111",
                ExpDateM = 8,
                ExpDateY = 20,
                SecurityCode = "123",
                PaymentMethodType = PaymentMethodTypeEnum.Visa,
                PaymentAmount = amount
            };
        }

        [Test]
        public void RefundFull()
        {
            Payment payment = this.CreateBasicPayment(100m);
            Payment reversalPayment = this.CreateBasicPayment(100m);

            IPaymentProvider paymentProvider = IvinciContainer.Instance.Container().Resolve<IPaymentProvider>();
            Domain.FinanceManagement.Payment.Interfaces.IPaymentProcessorRepository paymentProcessorRepository = IvinciContainer.Instance.Container().Resolve<Domain.FinanceManagement.Payment.Interfaces.IPaymentProcessorRepository>();
            PaymentProcessor paymentProcessor = paymentProcessorRepository.GetByName(paymentProvider.PaymentProcessorName);
            PaymentProcessorResponse paymentProcessorResponse = paymentProvider.SubmitPaymentAsync(payment, paymentProcessor).Result;
            payment.PaymentProcessorResponse = paymentProcessorResponse;
            IPaymentReversalProvider paymentReversalProvider = IvinciContainer.Instance.Container().Resolve<IPaymentReversalProvider>();
            paymentProcessorResponse = paymentReversalProvider.SubmitPaymentRefundAsync(payment, reversalPayment, paymentProcessor).Result;

            Assert.IsTrue(paymentProcessorResponse.WasSuccessful());
        }

        [Test]
        public void RefundPartial()
        {
            Payment payment = this.CreateBasicPayment(100m);
            Payment reversalPayment = this.CreateBasicPayment(-90m);

            IPaymentProvider paymentProvider = IvinciContainer.Instance.Container().Resolve<IPaymentProvider>();
            Domain.FinanceManagement.Payment.Interfaces.IPaymentProcessorRepository paymentProcessorRepository = IvinciContainer.Instance.Container().Resolve<Domain.FinanceManagement.Payment.Interfaces.IPaymentProcessorRepository>();
            PaymentProcessor paymentProcessor = paymentProcessorRepository.GetByName(paymentProvider.PaymentProcessorName);
            PaymentProcessorResponse paymentProcessorResponse = paymentProvider.SubmitPaymentAsync(payment, paymentProcessor).Result;
            payment.PaymentProcessorResponse = paymentProcessorResponse;
            IPaymentReversalProvider paymentReversalProvider = IvinciContainer.Instance.Container().Resolve<IPaymentReversalProvider>();
            paymentProcessorResponse = paymentReversalProvider.SubmitPaymentRefundAsync(payment, reversalPayment, paymentProcessor, -reversalPayment.PaymentAmount).Result;

            Assert.IsTrue(paymentProcessorResponse.WasSuccessful());
        }

        [Test]
        public void Void()
        {
            Payment payment = this.CreateBasicPayment(100m);
            Payment reversalPayment = this.CreateBasicPayment(100m);

            IPaymentProvider paymentProvider = IvinciContainer.Instance.Container().Resolve<IPaymentProvider>();
            Domain.FinanceManagement.Payment.Interfaces.IPaymentProcessorRepository paymentProcessorRepository = IvinciContainer.Instance.Container().Resolve<Domain.FinanceManagement.Payment.Interfaces.IPaymentProcessorRepository>();
            PaymentProcessor paymentProcessor = paymentProcessorRepository.GetByName(paymentProvider.PaymentProcessorName);
            PaymentProcessorResponse paymentProcessorResponse = paymentProvider.SubmitPaymentAsync(payment, paymentProcessor).Result;
            payment.PaymentProcessorResponse = paymentProcessorResponse;
            IPaymentReversalProvider paymentReversalProvider = IvinciContainer.Instance.Container().Resolve<IPaymentReversalProvider>();
            paymentProcessorResponse = paymentReversalProvider.SubmitPaymentVoidAsync(payment, reversalPayment, paymentProcessor).Result;

            Assert.IsTrue(paymentProcessorResponse.WasSuccessful());
        }
    }
}
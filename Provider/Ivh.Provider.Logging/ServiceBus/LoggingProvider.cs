﻿namespace Ivh.Provider.Logging.ServiceBus
{
    using System;
    using System.Threading;
    using Common.Base.Logging;
    using Common.Base.Utilities.Helpers;
    using Common.ServiceBus.Interfaces;
    using Common.VisitPay.Enums;
    using Common.VisitPay.Messages.Logging;
    using Domain.Logging.Interfaces;

    public class LoggingProvider : ILoggingProvider
    {
        private readonly IBus _bus;
        private string _applicationName;
        private string _applicationVersion;
        private int? _threadId;
        private DateTime? _date;
        private Guid? _correlationId;

        public LoggingProvider(
            string applicationName,
            string applicationVersion,
            IBus bus)
        {
            this._bus = bus;
            this._applicationName = applicationName;
            this._applicationVersion = applicationVersion;
        }

        public virtual void Debug(Func<string> message, params object[] parameters)
        {
            this.InsertLog(LogMessageTypeEnum.Debug, message, null, parameters);
        }

        public virtual void Fatal(Func<string> message, params object[] parameters)
        {
            this.InsertLog(LogMessageTypeEnum.Fatal, message, Environment.StackTrace, parameters);
        }

        public void Fatal(Func<string> message, string stackTrace, params object[] parameters)
        {
            this.InsertLog(LogMessageTypeEnum.Fatal, message, stackTrace, parameters);
        }

        public virtual void Info(Func<string> message, params object[] parameters)
        {
            this.InsertLog(LogMessageTypeEnum.Info, message, null, parameters);
        }

        public virtual void Warn(Func<string> message, params object[] parameters)
        {
            this.InsertLog(LogMessageTypeEnum.Warn, message, null, parameters);
        }

        public void SetApplicationNameAndVersion(string applicationName, string applicationVersion, DateTime date, int threadId, Guid correlationId)
        {
            this._applicationName = applicationName;
            this._applicationVersion = applicationVersion;
            this._date = date;
            this._threadId = threadId;
            this._correlationId = correlationId;
        }

        public void SetApplicationNameAndVersion(string applicationName, string applicationVersion)
        {
            this.SetApplicationNameAndVersion(
                applicationName,
                applicationVersion,
                DateTime.UtcNow,
                Thread.CurrentThread.ManagedThreadId,
                CorrelationManager.CorrelationId);
        }
        private void InsertLog(LogMessageTypeEnum level, Func<string> message, string stackTrace, params object[] parameters)
        {
            string messageLocal = default(string);
            try
            {
                messageLocal = message();
            }
            catch (Exception ex)
            {
                this.Fatal(() => ExceptionHelper.AggregateExceptionToString(ex));
                return;
            }

            LogMessage logMessage = new LogMessage
            {
                Date = this._date ?? DateTime.UtcNow,
                LogMessageTypeEnum = level,
                Message = messageLocal,
                Parameters = parameters,
                ApplicationName = this._applicationName,
                ApplicationVersion = this._applicationVersion,
                ThreadId = this._threadId ?? Thread.CurrentThread.ManagedThreadId,
                CorrelationId = this._correlationId ?? CorrelationManager.CorrelationId,
                StackTrace = stackTrace

            };
            this._bus.PublishMessage(logMessage).Wait();
        }


    }
}

﻿namespace Ivh.Provider.Logging.Modules
{
    using Autofac;

    public class Module : Autofac.Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            string applicationName = Ivh.Common.Properties.Settings.Default.ApplicationName ?? "ClientDataApi"; // this is just a fallback in case it is not set in the config file

            string applicationVersion = this.GetType().Assembly.GetName().Version.ToString();

            builder.RegisterType<ServiceBus.LoggingProvider>()
                  .As<Domain.Logging.Interfaces.ILoggingProvider>()
                  .WithParameters(new[]
                  {
                    new NamedParameter("applicationName", applicationName),
                    new NamedParameter("applicationVersion", applicationVersion)
                  });

        }
    }
}

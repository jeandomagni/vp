﻿namespace Ivh.Provider.Monitoring
{
    using Common.Base.Enums;
    using Common.Data;
    using Common.Data.Connection.Connections;
    using Common.Data.Interfaces;
    using Common.VisitPay.Enums;
    using Domain.Monitoring.Entities;
    using Domain.Monitoring.Interfaces;
    using NHibernate;

    public class MonitorRepository : RepositoryBase<Monitor, VisitPay>, IMonitorRepository
    {
        public MonitorRepository(ISessionContext<VisitPay> sessionContext)
            : base(sessionContext)
        {
        }

        public Monitor GetById(MonitorEnum monitorEnum)
        {
            return this.GetById((int) monitorEnum);
        }
    }
}

﻿namespace Ivh.Provider.Monitoring.Modules
{
    using Autofac;
    using Domain.Monitoring.Interfaces;

    public class Module : Autofac.Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<MonitorRepository>().As<IMonitorRepository>();
            builder.RegisterType<MonitoringRepository>().As<IMonitoringRepository>();
            builder.RegisterType<PerformanceTimingRepository>().As<IPerformanceTimingRepository>();
            builder.RegisterType<PerformanceTimingSourceRepository>().As<IPerformanceTimingSourceRepository>();
        }
    }
}

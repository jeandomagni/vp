﻿namespace Ivh.Provider.Monitoring
{
    using Common.Data;
    using Common.Data.Connection.Connections;
    using Common.Data.Interfaces;
    using Domain.Monitoring.Entities;
    using Domain.Monitoring.Interfaces;
    using NHibernate;

    public class PerformanceTimingRepository : RepositoryBase<PerformanceTiming, VisitPay>, IPerformanceTimingRepository
    {
        public PerformanceTimingRepository(ISessionContext<VisitPay> sessionContext) : base(sessionContext)
        {
        }
    }
}
﻿namespace Ivh.Provider.Monitoring
{
    using Common.Data;
    using Common.Data.Connection.Connections;
    using Common.Data.Interfaces;
    using Domain.Monitoring.Entities;
    using Domain.Monitoring.Interfaces;
    using NHibernate;

    public class PerformanceTimingSourceRepository : RepositoryBase<PerformanceTimingSource, VisitPay>, IPerformanceTimingSourceRepository
    {
        public PerformanceTimingSourceRepository(ISessionContext<VisitPay> sessionContext) : base(sessionContext)
        {
        }
    }
}
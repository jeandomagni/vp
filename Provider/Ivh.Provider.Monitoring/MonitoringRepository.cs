﻿namespace Ivh.Provider.Monitoring
{
    using System;
    using System.Text;
    using Common.Base.Utilities.Extensions;
    using Common.Data.Connection.Connections;
    using Common.Data.Interfaces;
    using Common.VisitPay.Attributes;
    using Common.VisitPay.Enums;
    using Domain.Monitoring.Interfaces;
    using NHibernate;

    public class MonitoringRepository : IMonitoringRepository
    {
        private readonly ISession _session;
        private readonly ISession _etlSession;
        private readonly ISession _cdiEtlSession;
        private readonly ISession _guestPaySession;
        private readonly ISession _loggingSession;
        public MonitoringRepository(
            ISessionContext<VisitPay> sessionContext,
            ISessionContext<CdiEtl> etlSession,
            ISessionContext<CdiEtl> cdiEtlSession,
            ISessionContext<GuestPay> guestPaySession,
            ISessionContext<Logging> loggingSession)
        {
            this._session = sessionContext.Session;
            this._etlSession = etlSession.Session;
            this._cdiEtlSession = cdiEtlSession.Session;
            this._guestPaySession = guestPaySession.Session;
            this._loggingSession = loggingSession.Session;
        }

        public T GetDatum<T>(DatumAttribute datumAttribute, DateTime? dateTimeBegin = null, DateTime? dateTimeEnd = null)
        {
            return this.GetDatum<T>(datumAttribute.Datum, dateTimeBegin, dateTimeEnd);
        }

        public T GetDatum<T>(DatumEnum datumEnum, DateTime? dateTimeBegin = null, DateTime? dateTimeEnd = null)
        {
            StringBuilder sql = new StringBuilder();
            sql.Append(string.Format("exec [monitor].[Get_{0}] ", datumEnum.ToString()));
            sql.Append("@DateTimeBegin=:DateTimeBegin, ");
            sql.Append("@DateTimeEnd=:DateTimeEnd");

            DatumLocationAttribute datumLocationAttribute = datumEnum.GetAttribute<DatumLocationAttribute>();
            if (datumLocationAttribute == null || datumLocationAttribute.DatumLocation == DatumLocationEnum.Unknown)
            {
                throw new ArgumentException("datumEnum");
            }

            ISession session = null;
            switch (datumLocationAttribute.DatumLocation)
            {
                case DatumLocationEnum.App:
                    session = this._session;
                    break;
                case DatumLocationEnum.Etl:
                    session = this._etlSession;
                    break;
                case DatumLocationEnum.Cdi:
                    session = this._cdiEtlSession;
                    break;
                case DatumLocationEnum.Express:
                    session = this._guestPaySession;
                    break;
                case DatumLocationEnum.Logging:
                    session = this._loggingSession;
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }

            ISQLQuery query = session.CreateSQLQuery(sql.ToString());
            query.SetDateTime("DateTimeBegin", dateTimeBegin ?? DateTime.UtcNow);
            query.SetDateTime("DateTimeEnd", dateTimeEnd ?? DateTime.UtcNow);

            object result = query.UniqueResult();
            if (!(result is T))
            {
                return (T)Convert.ChangeType(result, typeof(T));
            }
            return (T)result;
        }
    }
}

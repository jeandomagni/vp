namespace Ivh.Provider.Powershell
{
    using System;
    using System.Globalization;
    using System.Management.Automation.Host;
    using System.Threading;

    internal class LoggingPsHost : PSHost
    {
        private readonly Guid _hostId = Guid.NewGuid();
        private readonly LoggingPsHostUserInterface _ui = new LoggingPsHostUserInterface();

        public override Guid InstanceId
        {
            get { return this._hostId; }
        }

        public override string Name
        {
            get { return "LoggingPsHost"; }
        }

        public override Version Version
        {
            get { return new Version(1, 0); }
        }

        public override PSHostUserInterface UI
        {
            get { return this._ui; }
        }

        public override CultureInfo CurrentCulture
        {
            get { return Thread.CurrentThread.CurrentCulture; }
        }

        public override CultureInfo CurrentUICulture
        {
            get { return Thread.CurrentThread.CurrentUICulture; }
        }

        public override void EnterNestedPrompt()
        {
            throw new NotImplementedException();
        }

        public override void ExitNestedPrompt()
        {
            throw new NotImplementedException();
        }

        public override void NotifyBeginApplication()
        {
        }

        public override void NotifyEndApplication()
        {
        }

        public override void SetShouldExit(int exitCode)
        {
        }
    }
}
﻿namespace Ivh.Provider.Powershell
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Management.Automation;
    using System.Management.Automation.Runspaces;
    using Common.Base.Utilities.Extensions;
    using Common.Data;
    using Common.Data.Connection;
    using Common.Data.Interfaces;
    using Common.VisitPay.Enums;
    using Domain.Powershell.Interfaces;
    using Domain.Settings.Enums;
    using Domain.Settings.Interfaces;

    public class PowershellProvider : IPowershellProvider
    {
        private readonly Lazy<IConnectionStringService> _connectionStrings;
        private readonly Lazy<IApplicationSettingsService> _applicationSettingsService;

        public PowershellProvider(
            Lazy<IConnectionStringService> connectionStrings,
            Lazy<IApplicationSettingsService> applicationSettingsService)
        {
            this._connectionStrings = connectionStrings;
            this._applicationSettingsService = applicationSettingsService;
        }

        #region notImplemented
        public string ExportCdiDataToFile()
        {
            throw new NotImplementedException();
        }

        public string HsGuarantorMatchDataMove()
        {
            throw new NotImplementedException();
        }

        public string InsertFileNametoFileTracker()
        {
            throw new NotImplementedException();
        }

        public string MoveVpDataToCdi()
        {
            throw new NotImplementedException();
        }

        public string ReRunnableOutboundSQL()
        {
            throw new NotImplementedException();
        }
        #endregion

        #region Inbound

        public string Inbound_LoadFiles()
        {
            IList<CommandParameter> parameters = this.GetParametersForPowershell();
            parameters.Add(new CommandParameter("StageTableLoaderPath", this._applicationSettingsService.Value.EXE_Directory_StageTableLoader.Value));
            parameters.Add(new CommandParameter("DailyFilePath", this._applicationSettingsService.Value.JamsEtlFromEpicFolder.Value));
            return this.RunCommand(this.GetFullPathToScript("Inbound\\Inbound_LoadFiles.ps1", this._applicationSettingsService.Value.GetUrl(UrlEnum.PathToPowershellScripts2)), parameters, true);
        }

        public string Inbound_LoadHistory()
        {
            IList<CommandParameter> parameters = this.GetParametersForPowershell();
            return this.RunCommand(this.GetFullPathToScript("Inbound\\Inbound_LoadHistory.ps1", this._applicationSettingsService.Value.GetUrl(UrlEnum.PathToPowershellScripts2)), parameters, true);
        }

        public string Inbound_LoadSnapshotAndDelta()
        {
            IList<CommandParameter> parameters = this.GetParametersForPowershell();
            return this.RunCommand(this.GetFullPathToScript("Inbound\\Inbound_LoadSnapshotAndDelta.ps1", this._applicationSettingsService.Value.GetUrl(UrlEnum.PathToPowershellScripts2)), parameters, true);
        }

        public string Inbound_LoadBaseStage()
        {
            IList<CommandParameter> parameters = this.GetParametersForPowershell();
            return this.RunCommand(this.GetFullPathToScript("Inbound\\Inbound_LoadBaseStage.ps1", this._applicationSettingsService.Value.GetUrl(UrlEnum.PathToPowershellScripts2)), parameters, true);
        }

        public string Inbound_LoadChangeEvents_Part1()
        {
            IList<CommandParameter> parameters = this.GetParametersForPowershell(true);

            return this.RunCommand(this.GetFullPathToScript("Inbound\\Inbound_LoadChangeEvents_Part1.ps1", this._applicationSettingsService.Value.GetUrl(UrlEnum.PathToPowershellScripts2)), parameters, true);
        }

        public string Inbound_LoadBase()
        {
            IList<CommandParameter> parameters = this.GetParametersForPowershell();
            return this.RunCommand(this.GetFullPathToScript("Inbound\\Inbound_LoadBase.ps1", this._applicationSettingsService.Value.GetUrl(UrlEnum.PathToPowershellScripts2)), parameters, true);
        }

        public string Inbound_LoadChangeEvents_Part2()
        {
            IList<CommandParameter> parameters = this.GetParametersForPowershell();
            return this.RunCommand(this.GetFullPathToScript("Inbound\\Inbound_LoadChangeEvents_Part2.ps1", this._applicationSettingsService.Value.GetUrl(UrlEnum.PathToPowershellScripts2)), parameters, true);
        }

        public string Inbound_GuestPay()
        {
            IList<CommandParameter> parameters = this.GetInboundScriptParameters();

            return this.RunCommand(
                this.GetFullPathToScript("Common_Inbound\\Inbound_60_GuestPay.ps1", this._applicationSettingsService.Value.GetUrl(UrlEnum.PathToPowershellScripts2)),
                parameters,
                false);
        }

        private IList<CommandParameter> GetInboundScriptParameters()
        {
            ConnectionStringHelper cdiEtl = new ConnectionStringHelper(this._connectionStrings.Value.CdiEtlConnection.Value);
            ConnectionStringHelper vpApp = new ConnectionStringHelper(this._connectionStrings.Value.ApplicationConnection.Value);

            IList<CommandParameter> parameters = new List<CommandParameter>
            {
                new CommandParameter("CdiEtlServer", cdiEtl.Server),
                new CommandParameter("CdiEtlDatabase", cdiEtl.Database),
                new CommandParameter("VpAppServer", vpApp.Server),
                new CommandParameter("VpAppDatabase", vpApp.Database),
                new CommandParameter("LoadType", (int)LoadTypeEnum.Inbound), 
                new CommandParameter("PsModulePath", new Uri(this._applicationSettingsService.Value.GetUrl(UrlEnum.PathToPowershellScripts), "Modules").AbsolutePath),
            };

            PSCredential vpAppPsCredential = this.GetPsCredential(vpApp);
            if (vpAppPsCredential != null)
            {
                parameters.Add(new CommandParameter("VpAppCredential", vpAppPsCredential));
            }

            PSCredential cdiEtlPsCredential = this.GetPsCredential(cdiEtl);
            if (cdiEtlPsCredential != null)
            {
                parameters.Add(new CommandParameter("CdiEtlCredential", cdiEtlPsCredential));
            }

            return parameters;
        }

        #endregion

        private PSCredential GetPsCredential(ConnectionStringHelper connectionString)
        {
            PSCredential credentials = null;
            try
            {
                credentials = new PSCredential(connectionString.UserName, connectionString.Password.ToSecureString());
            }
            catch
            {
                #if DEBUG
                // let it use integrated security locally
                #else
                throw;
                #endif
            }

            return credentials;
        }

        public string CloneCdiAppGuarantor(int hsGuarantorId, int vpGuarantorId, string appendValue, string userName, bool consolidate)
        {
            ConnectionStringHelper cdiEtl = new ConnectionStringHelper(this._connectionStrings.Value.CdiEtlConnection.Value);
            ConnectionStringHelper app = new ConnectionStringHelper(this._connectionStrings.Value.ApplicationConnection.Value);

            IList<CommandParameter> parameters = new List<CommandParameter>
            {
                new CommandParameter("HsGuarantorId", hsGuarantorId),
                new CommandParameter("VpGuarantorId", vpGuarantorId),
                new CommandParameter("Append", appendValue),
                new CommandParameter("UserName", userName),
                new CommandParameter("Consolidate", consolidate),
                new CommandParameter("CdiEtlServer", cdiEtl.Server),
                new CommandParameter("CdiEtlDatabase", cdiEtl.Database),
                new CommandParameter("VpAppServer", app.Server),
                new CommandParameter("VpAppDatabase", app.Database),
                new CommandParameter("PowershellDirectory", this._applicationSettingsService.Value.GetUrl(UrlEnum.PathToPowershellScripts).AbsolutePath),
            };

            PSCredential credentials = this.GetPsCredential(cdiEtl);

            if (credentials != null)
            {
                parameters.Add(new CommandParameter("DbCredential", credentials));
            }

            return this.RunCommand(this.GetFullPathToScript("CloneGurantor\\CloneCdiAppGuarantor.ps1", this._applicationSettingsService.Value.GetUrl(UrlEnum.PathToPowershellScripts2)), parameters, true);
        }

        public string PublishCdiEtlToVpEtl()
        {
            IList<CommandParameter> parameters = this.GetParametersForPowershell();

            return this.RunCommand(this.GetFullPathToScript("PublishCdiEtlToVpEtl.ps1", this._applicationSettingsService.Value.GetUrl(UrlEnum.PathToPowershellScripts)), parameters);
        }

        private IList<CommandParameter> GetParametersForPowershell(bool includePowershellDirs = false)
        {
            ConnectionStringHelper cdiEtl = new ConnectionStringHelper(this._connectionStrings.Value.CdiEtlConnection.Value);

            IList<CommandParameter> parameters = new List<CommandParameter>
            {
                new CommandParameter("CdiEtlServer", cdiEtl.Server),
                new CommandParameter("CdiEtlDatabase", cdiEtl.Database),
                new CommandParameter("VpEtlServer", cdiEtl.Server),
                new CommandParameter("VpEtlDatabase", cdiEtl.Database),

            };
            if (includePowershellDirs)
            {
                parameters.Add(new CommandParameter("PowershellDirectory", this._applicationSettingsService.Value.GetUrl(UrlEnum.PathToPowershellScripts).AbsolutePath));
                parameters.Add(new CommandParameter("PowershellDirectory2", this._applicationSettingsService.Value.GetUrl(UrlEnum.PathToPowershellScripts2).AbsolutePath));
            }

            return parameters;
        }

        private string RunCommand(string scriptPath, IEnumerable<CommandParameter> parameters, bool moreRobustButLessLogging = false)
        {
            string myOutput = string.Empty;
            Console.Write("Creating Remote runspace connection...");
            LoggingPsHost host = new LoggingPsHost();

            //Create and open a runspace.
            using (Runspace runspace = RunspaceFactory.CreateRunspace(host))
            {
                runspace.Open();
                Console.WriteLine("done");

                PowerShell powershell = PowerShell.Create();
                powershell.Runspace = runspace;

                Console.Write("Setting write-host...");
                if (moreRobustButLessLogging)
                {
                    powershell.AddScript("function write-host($object){ $Null = @(write-output $object) }");
                }
                else
                {
                    powershell.AddScript("function write-host($object){ write-output $object }");
                }
                powershell.Invoke();
                powershell.Commands.Clear();

                Console.Write("Setting $VerbosePreference=\"Continue\"...");
                powershell.AddScript("$ErrorActionPreference=\"Stop\"");
                powershell.Invoke();
                powershell.Commands.Clear();
                powershell.AddScript("$VerbosePreference=\"Continue\"");
                powershell.Invoke();

                Console.WriteLine("done");

                Console.WriteLine("Importing Workflow module...");
                powershell.Commands.Clear();

                //Import the module in to the PowerShell runspace. A XAML file could also be imported directly by using Import-Module.
                foreach (Uri moduleUri in this.GetModulesToImport())
                {
                    Console.WriteLine($"Importing module: {moduleUri.AbsolutePath}");
                    powershell.AddCommand("Import-Module").AddArgument(moduleUri.AbsolutePath);
                    powershell.Invoke();
                    powershell.Commands.Clear();
                }
                Console.WriteLine("done");

                // Add the PowerShell script to be run
                Console.Write("Executing base script...");
                powershell.AddScript(this.ReadPowerShellScript(scriptPath)).AddParameters(parameters.Select(x => x.Value).ToList());
                foreach (Command command1 in powershell.Commands.Commands)
                {
                    command1.MergeMyResults(PipelineResultTypes.Error, PipelineResultTypes.Output);
                }
                powershell.AddCommand("out-default");
                powershell.Invoke();
                Console.WriteLine("done");

                myOutput = ((LoggingPsHostUserInterface)host.UI).Output;
                Console.WriteLine("All done, output:");
                Console.Write(myOutput);
                runspace.Close();
            }
            return myOutput;
        }

        private string GetFullPathToScript(string scriptName, Uri basePath)
        {
            Uri scriptUri = new Uri(scriptName, UriKind.Relative);
            Uri scriptAbsoluteUri = new Uri(basePath, scriptUri);
            return scriptAbsoluteUri.AbsolutePath;
        }

        public string ReadPowerShellScript(string script)
        {
            string strContent = string.Empty;
            try
            {
                using (StreamReader objReader = new StreamReader(script))
                {
                    strContent = objReader.ReadToEnd();
                }
            }
            catch
            {
                //Just returning an empty string if we cant load the file for whatever reason.
            }
            return strContent;
        }

        private IEnumerable<Uri> GetModulesToImport()
        {
            return Directory.GetFiles(this._applicationSettingsService.Value.GetUrl(UrlEnum.PathToPowershellScripts).AbsolutePath, "*.psm1", SearchOption.AllDirectories)
                .Select(x => new Uri(x));
        }
    }
}
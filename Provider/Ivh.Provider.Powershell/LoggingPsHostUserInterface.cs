namespace Ivh.Provider.Powershell
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Management.Automation;
    using System.Management.Automation.Host;
    using System.Security;
    using System.Text;

    internal class LoggingPsHostUserInterface : PSHostUserInterface
    {
        private readonly StringBuilder _sb;

        public LoggingPsHostUserInterface()
        {
            this._sb = new StringBuilder();
        }

        public override void Write(ConsoleColor foregroundColor, ConsoleColor backgroundColor, string value)
        {
            this._sb.Append(value);
        }

        public override void Write(string value)
        {
            this._sb.Append(value);
        }

        public override void WriteDebugLine(string message)
        {
            this._sb.AppendLine("DEBUG: " + message);
        }

        public override void WriteErrorLine(string value)
        {
            this._sb.AppendLine("ERROR: " + value);
        }

        public override void WriteLine(string value)
        {
            this._sb.AppendLine(value);
        }

        public override void WriteVerboseLine(string message)
        {
            this._sb.AppendLine("VERBOSE: " + message);
        }

        public override void WriteWarningLine(string message)
        {
            this._sb.AppendLine("WARNING: " + message);
        }

        public override void WriteProgress(long sourceId, ProgressRecord record)
        {
        }

        public string Output
        {
            get { return this._sb.ToString(); }
        }

        public override Dictionary<string, PSObject> Prompt(
            string caption,
            string message,
            Collection<FieldDescription> descriptions)
        {
            throw new NotImplementedException();
        }

        public override int PromptForChoice(string caption, string message, Collection<ChoiceDescription> choices, int defaultChoice)
        {
            return defaultChoice;
        }

        public override PSCredential PromptForCredential(string caption, string message, string userName, string targetName, PSCredentialTypes allowedCredentialTypes, PSCredentialUIOptions options)
        {
            throw new NotImplementedException();
        }

        public override PSCredential PromptForCredential(string caption, string message, string userName, string targetName)
        {
            throw new NotImplementedException();
        }

        public override PSHostRawUserInterface RawUI
        {
            get { return null; }
        }

        public override string ReadLine()
        {
            return Console.ReadLine();
        }

        public override SecureString ReadLineAsSecureString()
        {
            throw new NotImplementedException();
        }
    }
}
﻿namespace Ivh.Provider.Powershell.Modules
{
    using Autofac;
    using Domain.Powershell.Interfaces;

    public class Module : Autofac.Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<PowershellProvider>().As<IPowershellProvider>();
        }
    }
}
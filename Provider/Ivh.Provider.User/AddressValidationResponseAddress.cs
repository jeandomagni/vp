﻿namespace Ivh.Provider.User
{
    using System.Xml.Serialization;

    public class AddressValidationResponseAddress
    {
        [XmlElement]
        public string Address1 { get; set; }

        [XmlElement]
        public string Address2 { get; set; }

        [XmlElement]
        public string City { get; set; }

        [XmlElement]
        public string State { get; set; }

        [XmlElement]
        public string Zip5 { get; set; }

        [XmlElement]
        public string Zip4 { get; set; }
    }
}
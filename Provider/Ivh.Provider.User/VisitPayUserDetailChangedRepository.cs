﻿namespace Ivh.Provider.User
{
    using System.Linq;
    using Common.Data.Connection.Connections;
    using Common.Data.Interfaces;
    using Domain.User.Entities;
    using Domain.User.Interfaces;
    using NHibernate;
    using NHibernate.Criterion;

    public class VisitPayUserDetailChangedRepository : IVisitPayUserDetailChangedRepository
    {
        private readonly ISession _session;

        public VisitPayUserDetailChangedRepository(
            ISessionContext<VisitPay> sessionContext
        )
        {
            this._session = sessionContext.Session;
        }

        public bool DuplicateVisitPayerUserAddressExists(int visitPayUserId)
        {

            int addressHash = this._session.Query<VisitPayUser>()
                .Where(x => x.VisitPayUserId == visitPayUserId)
                .Select(x => x.CurrentPhysicalAddress.AddressHash).FirstOrDefault();

            bool addressExists = false;
            if (addressHash > 0)
            {
                 addressExists = this._session
                    .Query<VisitPayUser>()
                    .Where(x => x.VisitPayUserId != visitPayUserId)
                    .Any(x => x.CurrentPhysicalAddress.AddressHash == addressHash);
            }

            return addressExists;
        }
    }
}
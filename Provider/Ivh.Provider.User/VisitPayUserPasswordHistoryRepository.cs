﻿namespace Ivh.Provider.User
{
    using System;
    using System.Collections.Generic;
    using Common.Data;
    using Common.Data.Connection.Connections;
    using Common.Data.Interfaces;
    using Domain.User.Entities;
    using Domain.User.Interfaces;
    using NHibernate;

    public class VisitPayUserPasswordHistoryRepository : RepositoryBase<VisitPayUserPasswordHistory, VisitPay>, IVisitPayUserPasswordHistoryRepository
    {
        public VisitPayUserPasswordHistoryRepository(ISessionContext<VisitPay> sessionContext) : base(sessionContext)
        {
        }

        public IList<VisitPayUserPasswordHistory> GetUserPasswordHistory(int visitPayUserId, int passwordReuseLimit)
        {
            IQueryOver<VisitPayUserPasswordHistory> query = this.Session.QueryOver<VisitPayUserPasswordHistory>()
                .Where(x => x.VisitPayUserId == visitPayUserId)
                .OrderBy(x => x.PasswordExpiresUtc).Desc;

            // if the # <= 0, consider them all
            if (passwordReuseLimit > 0)
            {
                query = query.Take(passwordReuseLimit);
            }

            return query.List();
        }

        public void SavePasswordHistory(int userId, string passwordHash, DateTime passwordExpiresUtc, int passwordHashConfigurationVersionId)
        {
            VisitPayUserPasswordHistory history = new VisitPayUserPasswordHistory
            {
                VisitPayUserId = userId,
                PasswordHash = passwordHash,
                PasswordExpiresUtc = passwordExpiresUtc,
                PasswordHashConfigurationVersionId = passwordHashConfigurationVersionId
            };

            this.Session.Save(history);
        }
    }
}

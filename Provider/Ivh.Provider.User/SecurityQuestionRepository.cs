﻿namespace Ivh.Provider.User
{
    using Common.Data;
    using Common.Data.Connection.Connections;
    using Common.Data.Interfaces;
    using Domain.User.Entities;
    using Domain.User.Interfaces;
    using NHibernate;

    public class SecurityQuestionRepository : RepositoryBase<SecurityQuestion, VisitPay>, ISecurityQuestionRepository
    {
        public SecurityQuestionRepository(ISessionContext<VisitPay> sessionContext) 
            : base(sessionContext)
        {
        }
    }
}

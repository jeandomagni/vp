﻿namespace Ivh.Provider.User.Modules
{
    using Autofac;
    using Common.Web.Factories;
    using Common.Web.Interfaces;
    using Common.Web.Utilities;
    using Domain.User.Interfaces;
    using User;

    public class Module : Autofac.Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<VisitPayRoleRepository>().As<IVisitPayRoleRepository>();
            builder.RegisterType<VisitPayUserPasswordHistoryRepository>().As<IVisitPayUserPasswordHistoryRepository>();
            builder.RegisterType<VpUserLoginSummaryRepository>().As<IVpUserLoginSummaryRepository>();
            builder.RegisterType<SecurityQuestionRepository>().As<ISecurityQuestionRepository>();
            builder.RegisterType<SecurityQuestionAnswerRepository>().As<ISecurityQuestionAnswerRepository>();
            builder.RegisterType<VisitPayUserAddressRepository>().As<IVisitPayUserAddressRepository>();
            builder.RegisterType<StaticHttpClientManager>().As<IStaticHttpClientManager>();
            builder.RegisterType<HttpClientHandlerFactory>().As<IHttpMessageHandlerFactory>();
            builder.RegisterType<VisitPayUserAddressVerificationProvider>().As<IVisitPayUserAddressVerificationProvider>();
        }
    }
}
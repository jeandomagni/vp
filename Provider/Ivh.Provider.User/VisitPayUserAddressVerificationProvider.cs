﻿namespace Ivh.Provider.User
{
    using Application.Base.Common.Dtos;
    using Common.Base.Utilities.Extensions;
    using Common.Cache;
    using Common.VisitPay.Constants;
    using Common.Web.Interfaces;
    using Domain.Logging.Interfaces;
    using Domain.Settings.Entities;
    using Domain.Settings.Interfaces;
    using Domain.User.Entities;
    using Domain.User.Interfaces;
    using Newtonsoft.Json;
    using System;
    using System.Collections.Generic;
    using System.Net.Http;
    using System.Threading.Tasks;

    public class VisitPayUserAddressVerificationProvider : IVisitPayUserAddressVerificationProvider
    {
        public const string ExternalCallFailedErrorMessage = "Unable to verify address.";

        private readonly Lazy<IApplicationSettingsService> _applicationSettingsService;
        private readonly Lazy<ILoggingService> _loggingService;
        private readonly Lazy<IMetricsProvider> _metricsProvider;
        private readonly Lazy<IDistributedCache> _cache;

        private static HttpClient _httpClient;
        private static readonly object Lock = new object();

        public VisitPayUserAddressVerificationProvider(
            Lazy<IApplicationSettingsService> applicationSettingsService,
            Lazy<IHttpMessageHandlerFactory> httpMessageHandlerFactory,
            Lazy<IStaticHttpClientManager> httpClientManager,
            Lazy<ILoggingService> loggingService,
            Lazy<IMetricsProvider> metricsProvider,
            Lazy<IDistributedCache> cache)
        {
            this._applicationSettingsService = applicationSettingsService;
            this._loggingService = loggingService;
            this._metricsProvider = metricsProvider;
            this._cache = cache;

            if (_httpClient != null && httpClientManager.Value.UseExistingStaticHttpClient)
            {
                return;
            }

            lock (Lock)
            {
                if (_httpClient != null && httpClientManager.Value.UseExistingStaticHttpClient)
                {
                    return;
                }

                const int maxMegabytesInResponse = 2;
                const int maxTimeoutInSeconds = 3;

                _httpClient = HttpClientFactory.Create(httpMessageHandlerFactory.Value.Create());
                _httpClient.Timeout = TimeSpan.FromSeconds(maxTimeoutInSeconds);
                _httpClient.MaxResponseContentBufferSize = maxMegabytesInResponse * 1000000;

                AppDomain.CurrentDomain.DomainUnload += (sender, eventArgs) => { _httpClient?.Dispose(); };
            }
        }

        public async Task<DataResult<IEnumerable<string>, bool>> GetStandardizedMailingAddressAsync(
            int vpUserId,
            VisitPayUserAddress vpUserAddress,
            bool tryGetFromCache)
        {
            // in the future, may want to allow end user to try to validate a different address (if first doesn't pass validation),
            // so only get from cache if they're just opting to use USPS version of the one already provided
            if (tryGetFromCache)
            {
                string cachedAddress = await this.GetCachedAddressAsync(vpUserId);
                if (cachedAddress.IsNotNullOrEmpty())
                {
                    this._loggingService.Value.Info(() => "Getting cached USPS address.");
                    return GetDataResult(cachedAddress, true);
                }
            }

            Uri uspsUri = await this.BuildUriForAddressVerificationAsync(vpUserAddress);
            HttpResponseMessage response;

            try
            {
                response = await _httpClient.GetAsync(uspsUri);
            }
            catch (Exception e)
            {
                this._metricsProvider.Value.Increment(Metrics.Increment.Usps.AddressVerification.Failure);
                this._loggingService.Value.Fatal(() => this.GetApiRequestFailureMessage(e.Message), e.StackTrace);
                return GetDataResult(ExternalCallFailedErrorMessage, false);
            }

            this._metricsProvider.Value.Increment(Metrics.Increment.Usps.AddressVerification.Success);
            DataResult<IEnumerable<string>, bool> result = await this.GetResultFromUspsResponseAsync(response, vpUserId);
            return result;
        }

        private async Task<DataResult<IEnumerable<string>, bool>> GetResultFromUspsResponseAsync(HttpResponseMessage response, int vpUserId)
        {
            string xmlPayload = await response.Content.ReadAsStringAsync();
            UspsApiResponse deserializedResult = new UspsAddressValidationResponse(xmlPayload).DeserializedResult;

            switch (deserializedResult)
            {
                case AddressValidationErrorResponse validationError:
                    {
                        this._loggingService.Value.Info(() => "Received validation error from USPS API.");
                        return GetDataResult(validationError.Description, false);
                    }

                case AddressValidationResponseWithNestedError successWithNestedError:
                    {
                        this._loggingService.Value.Info(() => "Received success response, with nested error, from USPS API.");
                        return GetDataResult(successWithNestedError.AddressWithError.Error.Description, false);
                    }

                case AddressValidationSuccessResponse successResponse:
                    {
                        string uspsAddressCacheKey = GetUspsAddressCacheKey(vpUserId);
                        string serializedUspsAddressJson = successResponse.ConvertToSerializedVpFormat();

                        await this._cache.Value.SetStringAsync(uspsAddressCacheKey, serializedUspsAddressJson);
                        this._loggingService.Value.Info(() => "Received validation success response from USPS API.");

                        return GetDataResult(serializedUspsAddressJson, true);
                    }

                default:
                    {
                        const string deserializationFailureMessage = "Failed to deserialize API response.";
                        this._loggingService.Value.Warn(() => deserializationFailureMessage);
                        return GetDataResult(deserializationFailureMessage, false);
                    }
            }
        }

        private async Task<Uri> BuildUriForAddressVerificationAsync(VisitPayUserAddress visitPayUserAddress)
        {
            string hostname = this._applicationSettingsService.Value.UspsApiUrlHostname.Value;
            string path = this._applicationSettingsService.Value.UspsApiUrlPath.Value;
            AddressVerificationProviderCredentials credentials = this._applicationSettingsService.Value.GetAddressVerificationProviderCredentials();

            this._loggingService.Value.Info(() => $"Constructing USPS API request with USPS Web Tools ID ending in ...{credentials.UserId.Substring(credentials.UserId.Length - 4)}");

            Dictionary<string, string> queryStringDictionary = new Dictionary<string, string>
            {
                {"API", "Verify"},
                {"XML", new UspsAddressValidationRequest(visitPayUserAddress, credentials).ToString()}
            };

            FormUrlEncodedContent formUrlEncodedQuery = new FormUrlEncodedContent(queryStringDictionary);
            string query = await formUrlEncodedQuery.ReadAsStringAsync();

            UriBuilder uriBuilder = new UriBuilder
            {
                Host = hostname,
                Path = path,
                Query = query,
                Scheme = Uri.UriSchemeHttps
            };

            return uriBuilder.Uri;
        }

        private async Task<string> GetCachedAddressAsync(int vpUserId)
        {
            string uspsAddressCacheKey = GetUspsAddressCacheKey(vpUserId);
            string verifiedAddressFromCache = await this._cache.Value.GetStringAsync(uspsAddressCacheKey);
            return verifiedAddressFromCache;
        }

        private static string GetUspsAddressCacheKey(int vpUserId)
        {
            return $"UspsAddress{vpUserId}";
        }

        private string GetApiRequestFailureMessage(string errorMessage)
        {
            return $"{nameof(VisitPayUserAddressVerificationProvider)}:{nameof(this.GetStandardizedMailingAddressAsync)} - USPS API call failure: {errorMessage}";
        }

        private static DataResult<IEnumerable<string>, bool> GetDataResult(string data, bool result)
        {
            return new DataResult<IEnumerable<string>, bool>
            {
                Data = data.ToListOfOne(),
                Result = result
            };
        }
    }
}
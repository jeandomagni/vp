﻿namespace Ivh.Provider.User
{
    using System;
    using System.IO;
    using System.Xml;
    using System.Xml.Serialization;

    public class UspsAddressValidationResponse
    {
        public UspsApiResponse DeserializedResult { get; }

        public UspsAddressValidationResponse(string xmlStringPayload)
        {
            XmlSerializer serializer;

            // serialize to expected types, partially to guard against XXE attacks
            try
            {
                if (this.IsValidationResponseWithNestedError(xmlStringPayload))
                {
                    serializer = new XmlSerializer(typeof(AddressValidationResponseWithNestedError));
                    using (XmlTextReader reader = new XmlTextReader(new StringReader(xmlStringPayload)))
                    {
                        reader.DtdProcessing = DtdProcessing.Prohibit;
                        this.DeserializedResult = serializer.Deserialize(reader) as AddressValidationResponseWithNestedError;
                        return;
                    }
                }

                serializer = new XmlSerializer(typeof(AddressValidationSuccessResponse));
                using (XmlTextReader reader = new XmlTextReader(new StringReader(xmlStringPayload)))
                {
                    reader.DtdProcessing = DtdProcessing.Prohibit;
                    this.DeserializedResult = serializer.Deserialize(reader) as AddressValidationSuccessResponse;
                }
            }
            catch (Exception)
            {
                serializer = new XmlSerializer(typeof(AddressValidationErrorResponse));
                using (XmlTextReader reader = new XmlTextReader(new StringReader(xmlStringPayload)))
                {
                    reader.DtdProcessing = DtdProcessing.Prohibit;
                    this.DeserializedResult = serializer.Deserialize(reader) as AddressValidationErrorResponse;
                }
            }
        }

        /// <summary>
        /// For "address not found" responses, we expect USPS to send an Error element wrapped in AddressValidateResponse.
        /// </summary>
        private bool IsValidationResponseWithNestedError(string xmlPayload)
        {
            return xmlPayload.Contains("<AddressValidateResponse>") && xmlPayload.Contains("<Error>");
        }
    }
}
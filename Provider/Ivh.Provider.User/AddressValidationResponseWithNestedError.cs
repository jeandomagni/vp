﻿namespace Ivh.Provider.User
{
    using System.Xml.Serialization;

    [XmlRoot(ElementName = "AddressValidateResponse")]
    public class AddressValidationResponseWithNestedError : UspsApiResponse
    {
        [XmlElement(ElementName = "Address")]
        public AddressValidationResponseAddressWithError AddressWithError { get; set; }
    }
}
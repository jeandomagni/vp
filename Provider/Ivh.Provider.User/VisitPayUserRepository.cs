﻿namespace Ivh.Provider.User
{
    using Application.User.Common.Dtos;
    using Common.Base.Utilities.Extensions;
    using Common.Base.Utilities.Helpers;
    using Common.Data;
    using Common.Data.Connection.Connections;
    using Common.Data.Interfaces;
    using Common.VisitPay.Enums;
    using Domain.User.Entities;
    using Domain.User.Interfaces;
    using Microsoft.AspNet.Identity;
    using NHibernate;
    using NHibernate.Criterion;
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Globalization;
    using System.Linq;
    using System.Linq.Expressions;
    using System.Security.Claims;
    using System.Threading.Tasks;

    public class VisitPayUserRepository<TUser> :
        IVisitPayUserRepository,
        IUserLoginStore<TUser>,
        IUserClaimStore<TUser>,
        IUserRoleStore<TUser>,
        IUserPasswordStore<TUser>,
        IUserSecurityStampStore<TUser>,
        IQueryableUserStore<TUser>,
        IUserStore<TUser>,
        IUserLockoutStore<TUser, string>,
        IUserEmailStore<TUser>,
        IUserPhoneNumberStore<TUser>,
        IUserTwoFactorStore<TUser, string>,
        IUserTokenProvider<TUser, string>,
        IDisposable where TUser : VisitPayUser
    {
        private bool _disposed;

        /// <summary>
        /// If true then disposing this object will also dispose (close) the session. False means that external code is responsible for disposing the session.
        /// </summary>
        public bool ShouldDisposeSession { get; set; }

        public ISession Session { get; private set; }

        public VisitPayUserRepository(ISessionContext<VisitPay> sessionContext)
        {
            this.ShouldDisposeSession = true;
            this.Session = sessionContext?.Session ?? throw new ArgumentNullException(nameof(sessionContext));
        }

        public virtual Task<TUser> FindByIdAsync(string userId)
        {
            this.ThrowIfDisposed();
            return Task.Run(() =>
            {
                ICriteria userCriteria = this.Session.CreateCriteria(typeof(TUser));
                userCriteria.Add(Restrictions.Eq("VisitPayUserId", int.Parse(userId)));
                TUser user = userCriteria.UniqueResult<TUser>();

                return user;
            });
        }

        public virtual VisitPayUser FindById(string userId)
        {
            ICriteria userCriteria = this.Session.CreateCriteria(typeof(TUser));
            userCriteria.Add(Restrictions.Eq("VisitPayUserId", Int32.Parse(userId)));
            TUser user = userCriteria.UniqueResult<TUser>();

            return user;
        }

        public virtual Task<TUser> FindByNameAsync(string userName)
        {
            this.ThrowIfDisposed();

            return Task.Run(() =>
            {
                TUser user;

                ICriteria userCriteria = this.Session.CreateCriteria(typeof(TUser));
                userCriteria.Add(Restrictions.Eq("UserName", userName));
                user = userCriteria.UniqueResult<TUser>();

                return user;
            });
        }

        public virtual Task CreateAsync(TUser user)
        {
            this.ThrowIfDisposed();
            if ((object)user == null)
            {
                throw new ArgumentNullException(nameof(user));
            }

            this.Session.SaveOrUpdate(user);
            this.Session.Flush();

            return Task.FromResult(0);
        }

        public virtual Task DeleteAsync(TUser user)
        {
            if ((object)user == null)
            {
                throw new ArgumentNullException(nameof(user));
            }

            this.Session.Delete(user);
            this.Session.Flush();

            return Task.FromResult(0);
        }

        public virtual Task UpdateAsync(TUser user)
        {
            this.ThrowIfDisposed();
            if ((object)user == null)
            {
                throw new ArgumentNullException(nameof(user));
            }

            this.Session.SaveOrUpdate(user);
            this.Session.Flush();

            return Task.FromResult(0);
        }

        private void ThrowIfDisposed()
        {
            if (this._disposed)
            {
                throw new ObjectDisposedException(this.GetType().Name);
            }
        }

        public void Dispose()
        {
            this.Dispose(true);
            GC.SuppressFinalize((object)this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposing && this.Session != null && this.ShouldDisposeSession)
            {
                this.Session.Dispose();
            }
            this._disposed = true;
            this.Session = null;
        }

        public virtual Task<TUser> FindAsync(UserLoginInfo login)
        {
            this.ThrowIfDisposed();
            if (login == null)
            {
                throw new ArgumentNullException(nameof(login));
            }

            IQueryable<TUser> query = from u in this.Session.Query<TUser>()
                                      from l in u.Logins
                                      where l.LoginProvider == login.LoginProvider && l.ProviderKey == login.ProviderKey
                                      select u;

            return Task.FromResult(query.Take(1).SingleOrDefault());
        }

        public virtual Task AddLoginAsync(TUser user, UserLoginInfo login)
        {
            this.ThrowIfDisposed();
            if ((object)user == null)
            {
                throw new ArgumentNullException(nameof(user));
            }
            if (login == null)
            {
                throw new ArgumentNullException(nameof(login));
            }

            user.Logins.Add(new VisitPayUserLogin()
            {
                ProviderKey = login.ProviderKey,
                LoginProvider = login.LoginProvider
            });

            this.Session.SaveOrUpdate(user);
            return Task.FromResult<int>(0);
        }

        public virtual Task RemoveLoginAsync(TUser user, UserLoginInfo login)
        {
            this.ThrowIfDisposed();
            if ((object)user == null)
            {
                throw new ArgumentNullException(nameof(user));
            }
            if (login == null)
            {
                throw new ArgumentNullException(nameof(login));
            }

            VisitPayUserLogin info = user.Logins.Take(1).SingleOrDefault(x => x.LoginProvider == login.LoginProvider && x.ProviderKey == login.ProviderKey);
            if (info != null)
            {
                user.Logins.Remove(info);
                this.Session.Update(user);
            }

            return Task.FromResult<int>(0);
        }

        public virtual Task<IList<UserLoginInfo>> GetLoginsAsync(TUser user)
        {
            this.ThrowIfDisposed();
            if ((object)user == null)
            {
                throw new ArgumentNullException(nameof(user));
            }

            IList<UserLoginInfo> result = new List<UserLoginInfo>();
            foreach (VisitPayUserLogin identityUserLogin in (IEnumerable<VisitPayUserLogin>)user.Logins)
            {
                result.Add(new UserLoginInfo(identityUserLogin.LoginProvider, identityUserLogin.ProviderKey));
            }

            return Task.FromResult<IList<UserLoginInfo>>(result);
        }

        public virtual Task<IList<Claim>> GetClaimsAsync(TUser user)
        {
            this.ThrowIfDisposed();
            if ((object)user == null)
            {
                throw new ArgumentNullException(nameof(user));
            }

            IList<Claim> result = new List<Claim>();
            foreach (VisitPayUserClaim identityUserClaim in (IEnumerable<VisitPayUserClaim>)user.Claims)
            {
                result.Add(new Claim(identityUserClaim.ClaimType, identityUserClaim.ClaimValue));
            }

            return Task.FromResult<IList<Claim>>(result);
        }

        public virtual Task AddClaimAsync(TUser user, Claim claim)
        {
            this.ThrowIfDisposed();
            if ((object)user == null)
            {
                throw new ArgumentNullException(nameof(user));
            }
            if (claim == null)
            {
                throw new ArgumentNullException(nameof(claim));
            }

            user.Claims.Add(new VisitPayUserClaim()
            {
                User = user,
                ClaimType = claim.Type,
                ClaimValue = claim.Value
            });

            return Task.FromResult<int>(0);
        }

        public virtual Task RemoveClaimAsync(TUser user, Claim claim)
        {
            this.ThrowIfDisposed();
            if ((object)user == null)
            {
                throw new ArgumentNullException(nameof(user));
            }
            if (claim == null)
            {
                throw new ArgumentNullException(nameof(claim));
            }

            foreach (VisitPayUserClaim identityUserClaim in Enumerable.ToList<VisitPayUserClaim>(Enumerable.Where<VisitPayUserClaim>((IEnumerable<VisitPayUserClaim>)user.Claims, (Func<VisitPayUserClaim, bool>)(uc =>
          {
              if (uc.ClaimValue == claim.Value)
              {
                  return uc.ClaimType == claim.Type;
              }
              else
              {
                  return false;
              }
          }))))
            {
                user.Claims.Remove(identityUserClaim);
            }

            return Task.FromResult<int>(0);
        }

        public virtual Task AddToRoleAsync(TUser user, string role)
        {
            this.ThrowIfDisposed();
            if ((object)user == null)
            {
                throw new ArgumentNullException(nameof(user));
            }
            if (string.IsNullOrWhiteSpace(role))
            {
                throw new ArgumentException("ValueCannotBeNullOrEmpty", nameof(role));
            }

            VisitPayRole visitPayRole = Queryable.SingleOrDefault<VisitPayRole>(this.Session.Query<VisitPayRole>(), (Expression<Func<VisitPayRole, bool>>)(r => r.Name.ToUpper() == role.ToUpper()));
            if (visitPayRole == null)
            {
                throw new InvalidOperationException(string.Format((IFormatProvider)CultureInfo.CurrentCulture, "RoleNotFound", new object[1] { (object)role }));
            }
            user.Roles.Add(visitPayRole);

            return Task.FromResult<int>(0);

        }

        public virtual Task RemoveFromRoleAsync(TUser user, string role)
        {
            this.ThrowIfDisposed();
            if ((object)user == null)
            {
                throw new ArgumentNullException(nameof(user));
            }
            if (string.IsNullOrWhiteSpace(role))
            {
                throw new ArgumentException("ValueCannotBeNullOrEmpty", nameof(role));
            }

            VisitPayRole visitPayUserUserRole = Enumerable.FirstOrDefault<VisitPayRole>(Enumerable.Where<VisitPayRole>((IEnumerable<VisitPayRole>)user.Roles, (Func<VisitPayRole, bool>)(r => r.Name.ToUpper() == role.ToUpper())));
            if (visitPayUserUserRole != null)
            {
                user.Roles.Remove(visitPayUserUserRole);
            }

            return Task.FromResult<int>(0);
        }

        public virtual Task<IList<string>> GetRolesAsync(TUser user)
        {
            this.ThrowIfDisposed();
            if ((object)user == null)
            {
                throw new ArgumentNullException(nameof(user));
            }
            else
            {
                return Task.FromResult<IList<string>>((IList<string>)Enumerable.ToList<string>(Enumerable.Select<VisitPayRole, string>((IEnumerable<VisitPayRole>)user.Roles, (Func<VisitPayRole, string>)(u => u.Name))));
            }
        }

        public virtual Task<bool> IsInRoleAsync(TUser user, string role)
        {
            this.ThrowIfDisposed();
            if ((object)user == null)
            {
                throw new ArgumentNullException(nameof(user));
            }
            if (string.IsNullOrWhiteSpace(role))
            {
                throw new ArgumentException("ValueCannotBeNullOrEmpty", nameof(role));
            }
            else
            {
                return Task.FromResult<bool>(Enumerable.Any<VisitPayRole>((IEnumerable<VisitPayRole>)user.Roles, (Func<VisitPayRole, bool>)(r => r.Name.ToUpper() == role.ToUpper())));
            }
        }

        public virtual Task SetPasswordHashAsync(TUser user, string passwordHash)
        {
            this.ThrowIfDisposed();
            if ((object)user == null)
            {
                throw new ArgumentNullException(nameof(user));
            }
            user.PasswordHash = passwordHash;
            return Task.FromResult<int>(0);
        }

        public virtual Task<string> GetPasswordHashAsync(TUser user)
        {
            this.ThrowIfDisposed();
            if ((object)user == null)
            {
                throw new ArgumentNullException(nameof(user));
            }
            else
            {
                return Task.FromResult<string>(user.PasswordHash);
            }
        }

        public virtual Task SetSecurityStampAsync(TUser user, string stamp)
        {
            this.ThrowIfDisposed();
            if ((object)user == null)
            {
                throw new ArgumentNullException(nameof(user));
            }
            user.SecurityStamp = stamp;
            return Task.FromResult<int>(0);
        }

        public virtual Task<string> GetSecurityStampAsync(TUser user)
        {
            this.ThrowIfDisposed();
            if ((object)user == null)
            {
                throw new ArgumentNullException(nameof(user));
            }
            else
            {
                return Task.FromResult<string>(user.SecurityStamp);
            }
        }

        public virtual Task<bool> HasPasswordAsync(TUser user)
        {
            return Task.FromResult<bool>(user.PasswordHash != null);
        }

        public IQueryable<TUser> Users
        {
            get
            {
                this.ThrowIfDisposed();
                return this.Session.Query<TUser>();
            }
        }

        public Task<int> GetAccessFailedCountAsync(TUser user)
        {
            this.ThrowIfDisposed();
            if (user == null)
            {
                throw new ArgumentNullException(nameof(user));
            }
            return Task.FromResult<int>(user.AccessFailedCount);
        }

        public virtual Task<bool> GetLockoutEnabledAsync(TUser user)
        {
            this.ThrowIfDisposed();
            if (user == null)
            {
                throw new ArgumentNullException(nameof(user));
            }
            return Task.FromResult<bool>(user.LockoutEnabled);
        }

        public virtual Task<DateTimeOffset> GetLockoutEndDateAsync(TUser user)
        {
            DateTimeOffset dateTimeOffset;
            this.ThrowIfDisposed();
            if (user == null)
            {
                throw new ArgumentNullException(nameof(user));
            }
            if (user.LockoutEndDateUtc.HasValue)
            {
                DateTime? lockoutEndDateUtc = user.LockoutEndDateUtc;
                dateTimeOffset = new DateTimeOffset(DateTime.SpecifyKind(lockoutEndDateUtc.Value, DateTimeKind.Utc));
            }
            else
            {
                dateTimeOffset = new DateTimeOffset();
            }
            return Task.FromResult<DateTimeOffset>(dateTimeOffset);
        }

        public virtual Task<int> IncrementAccessFailedCountAsync(TUser user)
        {
            this.ThrowIfDisposed();
            if (user == null)
            {
                throw new ArgumentNullException(nameof(user));
            }
            user.AccessFailedCount = user.AccessFailedCount + 1;
            return Task.FromResult(user.AccessFailedCount);
        }

        public virtual Task ResetAccessFailedCountAsync(TUser user)
        {
            this.ThrowIfDisposed();
            if (user == null)
            {
                throw new ArgumentNullException(nameof(user));
            }
            user.AccessFailedCount = 0;
            return Task.FromResult<int>(0);
        }

        public virtual Task SetLockoutEnabledAsync(TUser user, bool enabled)
        {
            this.ThrowIfDisposed();
            if (user == null)
            {
                throw new ArgumentNullException(nameof(user));
            }
            user.LockoutEnabled = enabled;
            return Task.FromResult<int>(0);
        }

        public virtual Task SetLockoutEndDateAsync(TUser user, DateTimeOffset lockoutEnd)
        {
            DateTime? nullable;
            this.ThrowIfDisposed();
            if (user == null)
            {
                throw new ArgumentNullException(nameof(user));
            }
            if (lockoutEnd == DateTime.MinValue)
            {
                nullable = null;
            }
            else
            {
                nullable = lockoutEnd.UtcDateTime;
            }
            user.LockoutEndDateUtc = nullable;
            return Task.FromResult<int>(0);
        }

        public virtual Task<TUser> FindByEmailAsync(string email)
        {
            this.ThrowIfDisposed();
            //return this.GetUserAggregateAsync((TUser u) => u.Email.ToUpper() == email.ToUpper());
            return Task.Run(() =>
            {
                TUser user;

                ICriteria userCriteria = this.Session.CreateCriteria(typeof(TUser));
                userCriteria.Add(Restrictions.Eq("Email", email));
                user = userCriteria.UniqueResult<TUser>();

                return user;
                //return this.Context.QueryOver<TUser>().Where(x => x.Email.ToUpper() == email.ToUpper()).Take(1).SingleOrDefault();
            });
        }

        public virtual Task<string> GetEmailAsync(TUser user)
        {
            this.ThrowIfDisposed();
            if (user == null)
            {
                throw new ArgumentNullException(nameof(user));
            }
            return Task.FromResult<string>(user.Email);
        }

        public virtual Task<bool> GetEmailConfirmedAsync(TUser user)
        {
            this.ThrowIfDisposed();
            if (user == null)
            {
                throw new ArgumentNullException(nameof(user));
            }
            return Task.FromResult<bool>(user.EmailConfirmed);
        }

        public virtual Task SetEmailAsync(TUser user, string email)
        {
            this.ThrowIfDisposed();
            if (user == null)
            {
                throw new ArgumentNullException(nameof(user));
            }
            user.Email = email;
            return Task.FromResult<int>(0);
        }

        public virtual Task SetEmailConfirmedAsync(TUser user, bool confirmed)
        {
            this.ThrowIfDisposed();
            if (user == null)
            {
                throw new ArgumentNullException(nameof(user));
            }
            user.EmailConfirmed = confirmed;
            return Task.FromResult<int>(0);
        }

        public virtual Task<string> GetPhoneNumberAsync(TUser user)
        {
            this.ThrowIfDisposed();
            if (user == null)
            {
                throw new ArgumentNullException(nameof(user));
            }
            return Task.FromResult<string>(user.PhoneNumber);
        }

        public virtual Task<bool> GetPhoneNumberConfirmedAsync(TUser user)
        {
            this.ThrowIfDisposed();
            if (user == null)
            {
                throw new ArgumentNullException(nameof(user));
            }
            return Task.FromResult<bool>(user.PhoneNumberConfirmed);
        }

        public virtual Task SetPhoneNumberAsync(TUser user, string phoneNumber)
        {
            this.ThrowIfDisposed();
            if (user == null)
            {
                throw new ArgumentNullException(nameof(user));
            }
            user.PhoneNumber = phoneNumber;
            return Task.FromResult<int>(0);
        }

        public virtual Task SetPhoneNumberConfirmedAsync(TUser user, bool confirmed)
        {
            this.ThrowIfDisposed();
            if (user == null)
            {
                throw new ArgumentNullException(nameof(user));
            }
            user.PhoneNumberConfirmed = confirmed;
            return Task.FromResult<int>(0);
        }

        public virtual Task<bool> GetTwoFactorEnabledAsync(TUser user)
        {
            this.ThrowIfDisposed();
            if (user == null)
            {
                throw new ArgumentNullException(nameof(user));
            }
            return Task.FromResult<bool>(user.TwoFactorEnabled);
        }

        public virtual Task SetTwoFactorEnabledAsync(TUser user, bool enabled)
        {
            this.ThrowIfDisposed();
            if (user == null)
            {
                throw new ArgumentNullException(nameof(user));
            }
            user.TwoFactorEnabled = enabled;
            return Task.FromResult<int>(0);
        }

        public virtual Task<VisitPayUser> FindByDetailsAsync(VisitPayUserFindByDetailsParametersDto visitPayUserFindByDetailsParameters)
        {
            this.ThrowIfDisposed();
            return Task.Run(() =>
            {

                IQueryOver<VisitPayUser, VisitPayUser> query = this.Session.QueryOver<VisitPayUser>()
                    .Where(x => x.LastName == visitPayUserFindByDetailsParameters.LastName)
                    .Where(x => x.DateOfBirth == visitPayUserFindByDetailsParameters.DateOfBirth);

                // client defined match fields
                if (visitPayUserFindByDetailsParameters.UseSsn)
                {
                    query.Where(x => x.SSN4 == visitPayUserFindByDetailsParameters.Ssn4);
                }

                if (visitPayUserFindByDetailsParameters.UseFirstName)
                {
                    query.Where(x => x.FirstName == visitPayUserFindByDetailsParameters.FirstName);
                }

                if (visitPayUserFindByDetailsParameters.UseEmailAddress)
                {
                    query.Where(x => x.Email == visitPayUserFindByDetailsParameters.EmailAddress);
                }

                VisitPayUser user = query.SingleOrDefault();

                return user;
            });
        }

        public Task<VisitPayUser> FindByDetailsAsync(string firstName, string lastName, int yearOfBirth, string emailAddress)
        {
            this.ThrowIfDisposed();
            return Task.Run(() =>
            {
                DateTime yearOfBirthBegin = new DateTime(yearOfBirth, 1, 1, 0, 0, 0);
                DateTime yearOfBirthEnd = yearOfBirthBegin.AddYears(1).AddMilliseconds(-1);

                ICriteria userCriteria = this.Session.CreateCriteria(typeof(TUser));
                userCriteria.Add(Restrictions.Eq("FirstName", firstName));
                userCriteria.Add(Restrictions.Eq("LastName", lastName));
                userCriteria.Add(Restrictions.Between("DateOfBirth", yearOfBirthBegin, yearOfBirthEnd));
                userCriteria.Add(Restrictions.Eq("Email", emailAddress));
                TUser user = userCriteria.UniqueResult<TUser>();

                return (VisitPayUser)user;
            });
        }

        public virtual Task<VisitPayUser> FindByEmulateTokenAsync(string token)
        {
            this.ThrowIfDisposed();
            return Task.Run(() =>
            {
                ICriteria userCriteria = this.Session.CreateCriteria(typeof(TUser));
                userCriteria.Add(Restrictions.Eq("EmulateToken", token));
                TUser user = userCriteria.UniqueResult<TUser>();

                return (VisitPayUser)user;
            });
        }

        public IReadOnlyList<VisitPayUser> FindByNameAndDob(string lastName, DateTime dateOfBirth)
        {
            this.ThrowIfDisposed();

            VisitPayUser userAlias = null;

            IQueryOver<VisitPayUser> userCriteria = this.Session
                .QueryOver(() => userAlias)
                .Where(Restrictions.Eq(Projections.Property(() => userAlias.LastName), lastName))
                .Where(Restrictions.Eq(Projections.Property(() => userAlias.DateOfBirth), dateOfBirth));

            IReadOnlyList<TUser> users = userCriteria.List<TUser>().ToList();

            return users;
        }

        public IReadOnlyList<VisitPayUser> FindByDetailsAndAccessTokenPhi(string lastName, DateTime dob, string accessTokenPhi)
        {
            this.ThrowIfDisposed();

            VisitPayUser userAlias = null;

            IQueryOver<VisitPayUser> userCriteria = this.Session
                .QueryOver(() => userAlias)
                .Where(Restrictions.Eq(Projections.Property(() => userAlias.LastName), lastName))
                .Where(Restrictions.Eq(Projections.Property(() => userAlias.DateOfBirth), dob))
                .Where(Restrictions.Eq(Projections.Property(()=> userAlias.AccessTokenPhi), accessTokenPhi));

            IReadOnlyList<TUser> users = userCriteria.List<TUser>().ToList();

            return users;
        }

        //private Task<TUser> GetUserAggregateAsync(Expression<Func<TUser, bool>> filter)
        //{
        //    return Task.Run(() =>
        //    {
        //        // no cartesian product, batch call. Don't know if it's really needed: should we eager load or let lazy loading do its stuff?
        //        var query = this.Context.Query<TUser>().Where(filter);
        //        query.Fetch(p => p.Roles).ToFuture();
        //        query.Fetch(p => p.Claims).ToFuture();
        //        query.Fetch(p => p.Logins).ToFuture();
        //        return query.ToFuture().FirstOrDefault();
        //    });
        //}


        // I'm not sure if this is used, but I'm keeping the generating temp password using the default hash 
        // as that's what it's doing today, and I'm expecting it to be using that hash when I verify the temp
        // password in VisitPayUserService.VerifyTempPassword().
        // todo if the hash used to generate the temp password changes, this should change as well.
        public virtual Task<string> GenerateAsync(string purpose, UserManager<TUser, string> manager, TUser user)
        {
            this.ThrowIfDisposed();
            TokenPurposeEnum tokenPurposeEnum = (TokenPurposeEnum)Enum.Parse(typeof(TokenPurposeEnum), purpose);
            return Task.Run(() =>
            {
                switch (tokenPurposeEnum)
                {
                    case TokenPurposeEnum.ResetPassword:
                        string tempPassword = RandomStringGeneratorUtility.GetRandomStringForTempPassword(10);
                        user.TempPassword = manager.PasswordHasher.HashPassword(tempPassword);
                        return tempPassword;
                    case TokenPurposeEnum.EmulateUser:
                        string emulateToken = Guid.NewGuid().ToString();
                        user.EmulateToken = emulateToken;
                        return emulateToken;
                    case TokenPurposeEnum.VpccLogin:
                        string vpccToken = this.GenerateVpccTokenForUser(manager, user, tokenPurposeEnum);
                        return vpccToken;
                    default:
                        return string.Empty;
                }
            });
        }

        // I'm not sure if this is used, but as the temp password is always generated with the original hash, 
        // and I'm expecting it to be using that hash when I verify the temp password in VisitPayUserService.VerifyTempPassword(),
        // I'm not going to change anything here.
        // todo if the hash used to generate the temp password changes, this should change as well.
        public virtual Task<bool> ValidateAsync(string purpose, string token, UserManager<TUser, string> manager, TUser user)
        {
            this.ThrowIfDisposed();
            TokenPurposeEnum tokenPurposeEnum = (TokenPurposeEnum)Enum.Parse(typeof(TokenPurposeEnum), purpose);
            return Task.Run(() =>
            {
                bool result;
                switch (tokenPurposeEnum)
                {
                    case TokenPurposeEnum.TempLogin:
                        return this.ValidateTempPasswordForUser(manager, user, token);
                    case TokenPurposeEnum.ResetPassword:
                        result = this.ValidateTempPasswordForUser(manager, user, token);
                        if (result)
                        {
                            user.TempPassword = null;
                            user.TempPasswordExpDate = null;
                        }
                        return result;
                    case TokenPurposeEnum.EmulateUser:
                        result = !string.IsNullOrWhiteSpace(user.EmulateToken) && user.EmulateExpDate.HasValue && DateTime.UtcNow <= user.EmulateExpDate && user.EmulateToken.Equals(token, StringComparison.InvariantCultureIgnoreCase);
                        if (result)
                        {
                            user.EmulateToken = null;
                            user.EmulateExpDate = null;
                        }
                        return result;
                    case TokenPurposeEnum.VpccLogin:
                        result = this.ValidateTempPasswordForUser(manager, user, token);
                        return result;
                    default:
                        return false;
                }
            });
        }

        private string GenerateVpccTokenForUser(UserManager<TUser, string> manager, TUser user, TokenPurposeEnum tokenPurpose)
        {
            string token = Guid.NewGuid().ToString();
            string tokenHash = manager.PasswordHasher.HashPassword(token);
            user.TempPassword = tokenHash;
            return token;
        }

        private bool ValidateTempPasswordForUser(UserManager<TUser, string> manager, TUser user, string token)
        {
            DateTime now = DateTime.UtcNow;

            bool notExpired = now <= user.TempPasswordExpDate;
            bool passwordMatches = manager.PasswordHasher.VerifyHashedPassword(user.TempPassword, token) == PasswordVerificationResult.Success;

            return notExpired && passwordMatches;
        }

        public virtual Task NotifyAsync(string token, UserManager<TUser, string> manager, TUser user)
        {
            this.ThrowIfDisposed();
            return Task.FromResult(0);
        }

        public virtual Task<bool> IsValidProviderForUserAsync(UserManager<TUser, string> manager, TUser user)
        {
            this.ThrowIfDisposed();
            return Task.FromResult(true);
        }

        public VisitPayUserResults GetVisitPayUsers(VisitPayUserFilter filter, int baseRoleId, int batch, int batchSize)
        {
            VisitPayUserResults visitPayUserResults = this.GetVisitPayUserTotals(filter, baseRoleId);

            visitPayUserResults.VisitPayUsers = this.DoGetVisitPayUsers(filter, baseRoleId, batch, batchSize);

            return visitPayUserResults;
        }

        public IReadOnlyList<VisitPayUser> GetInactiveVisitPayClientUsers(DateTime lastLoginDate)
        {
            VisitPayUserFilter filter = new VisitPayUserFilter
            {
                LastLoginDate = lastLoginDate,
                RolesExclude = new List<int> { 13 } // ivhadmin
            };

            IQueryOver<VisitPayUser, VisitPayUser> query = this.QueryOverVisitPayUsers(filter, 3);

            return (IReadOnlyList<VisitPayUser>)query.List();
        }

        public bool IsAccountEnabled(int visitPayUserId)
        {
            ICriteria userCriteria = this.Session.CreateCriteria(typeof(TUser));
            userCriteria.Add(Restrictions.Eq("VisitPayUserId", visitPayUserId));
            userCriteria.SetProjection(Projections.Property("LockoutEndDateUtc"));

            DateTime? lockoutEndDateUtc = userCriteria.UniqueResult<DateTime?>();
            return lockoutEndDateUtc == null || lockoutEndDateUtc.Value.Date != DateTime.MaxValue.Date;
        }

        public IList<VisitPayUser> GetVisitPayUsersWithSmsNumber(string phoneNumber)
        {
            string number = FormatHelper.TwilioPhoneNumberToVisitPayUserPhoneNumberFormat(phoneNumber);
            IList<VisitPayUser> users = this.Session.QueryOver<VisitPayUserAcknowledgement>()
                .Where(a => a.VisitPayUserAcknowledgementType == VisitPayUserAcknowledgementTypeEnum.SmsAccept && a.SmsPhoneType != null && a.SmsPhoneType == SmsPhoneTypeEnum.Primary)
                .JoinQueryOver(a => a.VisitPayUser)
                .Where(u => u.PhoneNumber == number)
                .Select(a => a.VisitPayUser).List<VisitPayUser>();
            users.AddRange(this.Session.QueryOver<VisitPayUserAcknowledgement>()
                .Where(a => a.VisitPayUserAcknowledgementType == VisitPayUserAcknowledgementTypeEnum.SmsAccept && a.SmsPhoneType != null && a.SmsPhoneType == SmsPhoneTypeEnum.Secondary)
                .JoinQueryOver(a => a.VisitPayUser)
                .Where(u => u.PhoneNumberSecondary == number)
                .Select(a => a.VisitPayUser).List<VisitPayUser>());
            return users;
        }

        public string GetUserLocale(string userId)
        {
            VisitPayUser vpUser = this.FindById(userId);
            return vpUser?.Locale;
        }

        public void SetUserLocale(string userId, string locale)
        {
            VisitPayUser vpUser = this.FindById(userId);

            if (vpUser != null)
            {
                vpUser.Locale = locale;
                this.UpdateAsync((TUser)vpUser);
            }
        }

        private IReadOnlyList<VisitPayUser> DoGetVisitPayUsers(VisitPayUserFilter filter, int baseRoleId, int batch, int batchSize)
        {
            IQueryOver<VisitPayUser, VisitPayUser> query = this.QueryOverVisitPayUsers(filter, baseRoleId);
            Sort(filter, query);
            query = query.Fetch(x => x.VpUserLoginSummary).Eager();

            IList<VisitPayUser> visitPayUsers;

            if (filter.SortField.IsNotNullOrEmpty() && (filter.SortField == "IsLocked" || filter.SortField == "IsDisabled"))
            {
                visitPayUsers = PostSort(filter, query)
                    .Skip(batch * batchSize)
                    .Take(batchSize)
                    .ToList();
            }
            else
            {
                visitPayUsers = query
                    .Skip(batch * batchSize)
                    .Take(batchSize)
                    .List();
            }

            return (IReadOnlyList<VisitPayUser>)visitPayUsers;
        }

        private VisitPayUserResults GetVisitPayUserTotals(VisitPayUserFilter filter, int baseRoleId)
        {
            int totalRecords = this.QueryOverVisitPayUsers(filter, baseRoleId)
                .RowCount();

            return new VisitPayUserResults
            {
                TotalRecords = totalRecords
            };
        }

        private IQueryOver<VisitPayUser, VisitPayUser> QueryOverVisitPayUsers(VisitPayUserFilter filter, int baseRoleId)
        {
            VisitPayUser visitPayUserAlias = null;
            VisitPayUser userAlias = null;

            IQueryOver<VisitPayUser, VisitPayUser> query = this.Session.QueryOver(() => visitPayUserAlias);

            QueryOver<VisitPayRole> userRolesSubquery = QueryOver.Of<VisitPayRole>()
                .JoinAlias(x => x.Users, () => userAlias)
                .Where(b => userAlias.VisitPayUserId == visitPayUserAlias.VisitPayUserId)
                .Where(b => b.VisitPayRoleId == baseRoleId)
                .ToRowCountQuery();

            query.Where(Restrictions.Gt(Projections.SubQuery(userRolesSubquery.DetachedCriteria), 0));

            Filter(filter, query);

            return query;
        }

        private static void Filter(VisitPayUserFilter filter, IQueryOver<VisitPayUser, VisitPayUser> query)
        {
            VisitPayUser visitPayUserAlias = null;

            query.If(filter.FirstName.IsNotNullOrEmpty(), q => q.Where(x => visitPayUserAlias.FirstName == filter.FirstName))
                .If(filter.LastName.IsNotNullOrEmpty(), q => q.Where(x => visitPayUserAlias.LastName == filter.LastName))
                .If(filter.UserName.IsNotNullOrEmpty(), q => q.Where(x => visitPayUserAlias.UserName == filter.UserName));

            if (filter.IsNotIvhAdminOrIvhUserAuditor)
            {
                //User should not be able to see Ivh users
                if (filter.RolesExclude == null)
                {
                    filter.RolesExclude = new List<int>();
                }

                filter.RolesExclude.Add((int)VisitPayRoleEnum.IvhEmployee);
            }

            if (filter.IsNotClientUserManagementOrClientUserAuditor)
            {
                //User should only be able to see Ivh users
                if (filter.RolesInclude == null)
                {
                    filter.RolesInclude = new List<int>();
                }

                filter.RolesInclude.Add((int)VisitPayRoleEnum.IvhEmployee);
            }

            if (filter.RolesInclude != null && filter.RolesInclude.Any())
            {
                // include only these roles
                VisitPayUser userAlias = null;
                QueryOver<VisitPayRole> userRolesSubquery = QueryOver.Of<VisitPayRole>()
                    .JoinAlias(x => x.Users, () => userAlias)
                    .Where(b => userAlias.VisitPayUserId == visitPayUserAlias.VisitPayUserId)
                    .WhereRestrictionOn(b => b.VisitPayRoleId).IsIn(filter.RolesInclude.ToList())
                    .ToRowCountQuery();

                query.Where(Restrictions.Gt(Projections.SubQuery(userRolesSubquery.DetachedCriteria), 0));
            }

            if (filter.RolesExclude != null && filter.RolesExclude.Any())
            {
                // exclude these roles
                VisitPayUser userAlias = null;
                QueryOver<VisitPayRole> userRolesSubquery = QueryOver.Of<VisitPayRole>()
                    .JoinAlias(x => x.Users, () => userAlias)
                    .Where(b => userAlias.VisitPayUserId == visitPayUserAlias.VisitPayUserId)
                    .WhereRestrictionOn(b => b.VisitPayRoleId).IsIn(filter.RolesExclude.ToList())
                    .ToRowCountQuery();

                query.Where(Restrictions.Eq(Projections.SubQuery(userRolesSubquery.DetachedCriteria), 0));
            }

            if (filter.LastLoginDate.HasValue)
            {
                // check last login date
                VpUserLoginSummary loginAlias = null;
                QueryOver<VpUserLoginSummary> loginSubquery = QueryOver.Of(() => loginAlias)
                    .Where(() => loginAlias.VpUserId == visitPayUserAlias.VisitPayUserId)
                    .OrderBy(() => loginAlias.LastLoginDate).Desc
                    .Select(Projections.Property(() => loginAlias.LastLoginDate))
                    .Take(1);

                query.Where(Restrictions.Or(
                    Restrictions.Lt(Projections.Cast(NHibernateUtil.Date, Projections.SubQuery(loginSubquery)), filter.LastLoginDate.Value.Date),
                    Restrictions.IsNull(Projections.SubQuery(loginSubquery))));
            }

            if (filter.VisitPayUserIds != null && filter.VisitPayUserIds.Any())
            {
                query.Where(Restrictions.In("VisitPayUserId", filter.VisitPayUserIds.ToList()));
            }

            if (filter.IsDisabled.HasValue)
            {
                if (filter.IsDisabled.Value)
                {
                    query.Where(x => x.LockoutEndDateUtc != null && x.LockoutEndDateUtc > DateTime.MaxValue.Date.AddDays(-1));
                }
                else
                {
                    query.Where(x => x.LockoutEndDateUtc == null || x.LockoutEndDateUtc <= DateTime.MaxValue.Date.AddDays(-1));
                }
            }
        }

        private static void Sort(VisitPayUserFilter filter, IQueryOver<VisitPayUser, VisitPayUser> query)
        {
            if (filter.SortField.IsNullOrEmpty())
            {
                return;
            }

            VisitPayUser visitPayUserAlias = null;

            bool isAscendingOrder = "asc".Equals(filter.SortOrder, StringComparison.InvariantCultureIgnoreCase);

            switch (filter.SortField)
            {
                case "FirstName":
                case "LastName":
                case "UserName":
                case "InsertDate":
                    query.UnderlyingCriteria.AddOrder(new Order(filter.SortField, isAscendingOrder));
                    break;
                case "LastLoginDate":
                    QueryOver<VpUserLoginSummary> lastLoginSubquery = QueryOver.Of<VpUserLoginSummary>()
                        .Where(b => b.VpUserId == visitPayUserAlias.VisitPayUserId)
                        .OrderBy(b => b.LastLoginDate).Desc
                        .Select(x => x.LastLoginDate)
                        .Take(1);

                    query.UnderlyingCriteria.AddOrder(new Order(Projections.SubQuery(lastLoginSubquery), isAscendingOrder));
                    break;
            }
        }

        private static IList<VisitPayUser> PostSort(VisitPayUserFilter filter, IQueryOver<VisitPayUser, VisitPayUser> query)
        {
            IList<VisitPayUser> list = query.List();

            bool isAscendingOrder = "asc".Equals(filter.SortOrder, StringComparison.InvariantCultureIgnoreCase);

            switch (filter.SortField)
            {
                case "IsDisabled":
                    list = list.OrderBy(x => x.IsDisabled ? "Y" : "N", isAscendingOrder).ThenBy(x => x.LastName).ToList();
                    break;
                case "IsLocked":
                    list = list.OrderBy(x => x.IsLocked ? "Y" : "N", isAscendingOrder).ThenBy(x => x.LastName).ToList();
                    break;
            }

            return list;
        }

        public bool AccessTokenPhiValueExists(string accessTokenPhi, int visitPayUserId)
        {
            bool doesAccessTokenAlreadyExist = this.Session.Query<VisitPayUser>()
                .Where(x => x.AccessTokenPhi == accessTokenPhi && x.VisitPayUserId != visitPayUserId)
                .Select(x => x.AccessTokenPhi)
                .Any();
            return doesAccessTokenAlreadyExist;
        }

        #region unreferenced code

        /*public virtual Task<VisitPayUser> FindByDetailsAsync(string lastName, DateTime dateOfBirth, int ssn4)
        {
            this.ThrowIfDisposed();
            return Task.Run(() =>
            {
                ICriteria userCriteria = this.Context.CreateCriteria(typeof(TUser));
                userCriteria.Add(Restrictions.Eq("LastName", lastName));
                userCriteria.Add(Restrictions.Eq("SSN4", ssn4));
                userCriteria.Add(Restrictions.Eq("DateOfBirth", dateOfBirth));
                TUser user = userCriteria.UniqueResult<TUser>();

                return (VisitPayUser) user;
            });
        }*/

        #endregion
    }
}
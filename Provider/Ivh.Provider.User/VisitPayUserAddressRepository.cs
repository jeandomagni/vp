﻿namespace Ivh.Provider.User
{
    using Common.Data;
    using Common.Data.Connection.Connections;
    using Common.Data.Interfaces;
    using Domain.User.Entities;
    using Domain.User.Interfaces;

    public class VisitPayUserAddressRepository : RepositoryBase<VisitPayUserAddress, VisitPay>, IVisitPayUserAddressRepository
    {
        public VisitPayUserAddressRepository(ISessionContext<VisitPay> sessionContext) 
            : base(sessionContext)
        {
        }
    }
}

﻿namespace Ivh.Provider.User
{
    using Newtonsoft.Json;
    using System.Xml.Serialization;

    [XmlRoot(ElementName = "AddressValidateResponse")]
    public class AddressValidationSuccessResponse : UspsApiResponse
    {
        [XmlElement]
        public AddressValidationResponseAddress Address { get; set; }

        /// <summary>
        ///     Returns the USPS-supplied address as a JSON-serialized string,
        ///     and also assigns the Address 1 and Address 2 values as they will be expected downstream.
        /// </summary>
        /// <remarks>
        ///     USPS returns a verified address with "secondary" info (such as apartment number)
        ///     in the "Address1" field. VisitPay expects this in the "Address2" field.
        ///     This method adjusts the response accordingly.
        /// </remarks>
        public string ConvertToSerializedVpFormat()
        {
            if (this.Address == null)
            {
                return string.Empty;
            }

            string address1FromUsps = this.Address.Address1;
            string address2FromUsps = this.Address.Address2;

            this.Address.Address1 = address2FromUsps;
            this.Address.Address2 = address1FromUsps;

            string serialized = JsonConvert.SerializeObject(this.Address);
            return serialized;
        }
    }
}
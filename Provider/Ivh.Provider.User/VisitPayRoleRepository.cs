﻿namespace Ivh.Provider.User
{
    using Common.Data;
    using Common.Data.Connection.Connections;
    using Common.Data.Interfaces;
    using Domain.User.Entities;
    using Domain.User.Interfaces;
    using NHibernate;
    using NHibernate.Criterion;

    public class VisitPayRoleRepository : RepositoryBase<VisitPayRole, VisitPay>, IVisitPayRoleRepository
    {
        public VisitPayRoleRepository(ISessionContext<VisitPay> sessionContext) : base(sessionContext)
        {
        }

        public VisitPayRole GetRoleByName(string value)
        {
            var criteria = this.Session.CreateCriteria(typeof (VisitPayRole));
            criteria.Add(Restrictions.Eq("Name", value));
            return criteria.UniqueResult<VisitPayRole>();
        }
    }
}
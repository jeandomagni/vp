﻿namespace Ivh.Provider.User
{
    using Common.Base.Utilities.Extensions;
    using Domain.Settings.Entities;
    using Domain.User.Entities;
    using System;
    using System.Text.RegularExpressions;
    using System.Xml.Linq;

    public class UspsAddressValidationRequest
    {
        private readonly XDocument _xDoc;

        public UspsAddressValidationRequest(
            VisitPayUserAddress vpUserAddress,
            AddressVerificationProviderCredentials credentials)
        {
            if (vpUserAddress == null)
            {
                throw new ArgumentNullException(nameof(vpUserAddress));
            }

            if (credentials == null)
            {
                throw new ArgumentNullException(nameof(credentials));
            }

            XElement addressElement = new XElement("Address",
                new XAttribute("ID", "0"),
                new XElement("Address1", vpUserAddress.AddressStreet1.GetValueOrDefault(string.Empty)),
                new XElement("Address2", vpUserAddress.AddressStreet2.GetValueOrDefault(string.Empty)),
                new XElement("City", vpUserAddress.City.GetValueOrDefault(string.Empty)),
                new XElement("State", vpUserAddress.StateProvince.GetValueOrDefault(string.Empty)));

            this.SetPostalCodeElements(addressElement, vpUserAddress);

            XAttribute userIdAttribute = new XAttribute("USERID", credentials.UserId);
            XAttribute passwordAttribute = new XAttribute("PASSWORD", credentials.Password);
            XElement addressValidationElement = new XElement("AddressValidateRequest", userIdAttribute, passwordAttribute, addressElement);

            this._xDoc = new XDocument(addressValidationElement);
        }

        public override string ToString() => this._xDoc.ToString();

        private void SetPostalCodeElements(XContainer addressElement, VisitPayUserAddress vpUserAddress)
        {
            const string fiveDigitZipCodeElementName = "Zip5";
            const string zipCodePlusFourElementName = "Zip4";

            Regex pattern = new Regex(@"[^\d]");
            string zipCodeNumericOnly = pattern.Replace(vpUserAddress.PostalCode, string.Empty);

            if (zipCodeNumericOnly.Length.Equals(5))
            {
                addressElement.Add(new XElement(fiveDigitZipCodeElementName, zipCodeNumericOnly));
                addressElement.Add(new XElement(zipCodePlusFourElementName, string.Empty));
                return;
            }

            addressElement.Add(new XElement(fiveDigitZipCodeElementName, zipCodeNumericOnly.Substring(0, 5)));
            addressElement.Add(new XElement(zipCodePlusFourElementName, zipCodeNumericOnly.Substring(5)));
        }
    }
}
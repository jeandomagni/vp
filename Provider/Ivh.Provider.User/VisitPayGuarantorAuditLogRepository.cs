﻿namespace Ivh.Provider.User
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Common.Configuration.Interfaces;
    using Common.Data;
    using Common.Data.Connection.Connections;
    using Common.Data.Interfaces;
    using Domain.User.Entities;
    using Domain.User.Interfaces;
    using NHibernate.Transform;

    public class VisitPayGuarantorAuditLogRepository : RepositoryBase<VisitPayGuarantorAuditLog, VisitPay>, IVisitPayGuarantorAuditLogRepository
    {
        private readonly Lazy<IConnectionStringsProvider> _connectionStringsProvider;

        public VisitPayGuarantorAuditLogRepository(
            ISessionContext<VisitPay> sessionContext,
            Lazy<IConnectionStringsProvider> connectionStringsProvider)
            : base(sessionContext)
        {
            this._connectionStringsProvider = connectionStringsProvider;
        }

        public IReadOnlyList<VisitPayGuarantorAuditLog> GetVisitPayGuarantorAuditLog(int vpGuarantorId)
        {
            this.Session.Connection.Close();
            this.Session.Connection.ConnectionString = this._connectionStringsProvider.Value.GetConnectionString("LoggingConnection");

            this.Session.Connection.Open();
            string sql = @"SELECT  "
                      + " UserEventHistoryId "
                      + ",EventDate "
                      + ",UserName "
                      + ",UserId "
                      + ",IpAddress "
                      + ",UserAgent "
                      + ",EmulateUserId "
                      + ",VpGuarantorId "
                      + ",t.EventType "
                      + "FROM [log].[UserEventHistory] h "
                      + " left join [log].[UserEventType] t "
                      + " on h.UserEventTypeID = t.UserEventTypeID WHERE VpGuarantorId = :vpGuarantorId";
            
            List<VisitPayGuarantorAuditLog> result = this.Session.CreateSQLQuery(sql)
                .SetInt32("vpGuarantorId", (int) vpGuarantorId)
                .SetResultTransformer(Transformers.AliasToBean<VisitPayGuarantorAuditLog>())
                .List<VisitPayGuarantorAuditLog>()
                .ToList();

            return result;
        }
    }
}
﻿namespace Ivh.Provider.User
{
    using Common.Data;
    using Common.Data.Connection.Connections;
    using Common.Data.Interfaces;
    using Domain.User.Entities;
    using Domain.User.Interfaces;
    using NHibernate;

    public class SecurityQuestionAnswerRepository : RepositoryBase<SecurityQuestionAnswer, VisitPay>, ISecurityQuestionAnswerRepository
    {
        public SecurityQuestionAnswerRepository(ISessionContext<VisitPay> sessionContext)
            : base(sessionContext)
        {
        }
    }
}
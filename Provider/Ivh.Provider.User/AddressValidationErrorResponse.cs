﻿namespace Ivh.Provider.User
{
    using System.Xml.Serialization;

    [XmlRoot("Error")]
    public class AddressValidationErrorResponse : UspsApiResponse
    {
        [XmlElement]
        public string Number { get; set; }

        [XmlElement]
        public string Source { get; set; }

        [XmlElement]
        public string Description { get; set; }

        [XmlElement]
        public string HelpFile { get; set; }

        [XmlElement]
        public string HelpContext { get; set; }
    }
}
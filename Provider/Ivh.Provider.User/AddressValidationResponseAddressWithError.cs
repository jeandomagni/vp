﻿namespace Ivh.Provider.User
{
    using System.Xml.Serialization;

    public class AddressValidationResponseAddressWithError
    {
        [XmlElement(ElementName = "Error")]
        public AddressValidationErrorResponse Error { get; set; }
    }
}
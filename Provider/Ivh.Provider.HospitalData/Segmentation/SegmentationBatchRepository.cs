﻿namespace Ivh.Provider.HospitalData.Segmentation
{
    using System;
    using Common.Data;
    using Common.Data.Connection.Connections;
    using Common.Data.Interfaces;
    using Domain.HospitalData.Segmentation.Entities;
    using Domain.HospitalData.Segmentation.Enums;
    using Domain.HospitalData.Segmentation.Interfaces;
    using NHibernate;

    public class SegmentationBatchRepository : RepositoryBase<SegmentationBatch, CdiEtl>, ISegmentationBatchRepository
    {
        public SegmentationBatchRepository(ISessionContext<CdiEtl> sessionContext) : base(sessionContext)
        {
        }

        public int GenerateSegmentationBatchId(SegmentationTypeEnum segmentationTypeEnum)
        {
            SegmentationBatch segmentationBatch = new SegmentationBatch
            {
                BatchStartDateTime = DateTime.UtcNow,
                SegmentationTypeEnum = segmentationTypeEnum
            };
            base.InsertOrUpdate(segmentationBatch);
            return segmentationBatch.SegmentationBatchId;
        }
    }
}
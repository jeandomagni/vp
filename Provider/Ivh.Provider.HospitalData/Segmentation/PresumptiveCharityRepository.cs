﻿namespace Ivh.Provider.HospitalData.Segmentation
{
    using System;
    using System.Collections.Generic;
    using Common.Data.Connection.Connections;
    using Common.Data.Interfaces;
    using Domain.HospitalData.HsGuarantor.Entities;
    using Domain.HospitalData.Segmentation.Entities;
    using Domain.HospitalData.Segmentation.Interfaces;
    using Domain.HospitalData.Visit.Entities;
    using Enums;
    using NHibernate;
    using NHibernate.Criterion;
    using NHibernate.SqlCommand;
    using NHibernate.Transform;

    public class PresumptiveCharityRepository : IPresumptiveCharityRepository
    {
        private ISession Session { get; set; }
        private IStatelessSession StatelessSession { get; set; }

        public PresumptiveCharityRepository(ISessionContext<CdiEtl> sessionContext, IStatelessSessionContext<CdiEtl> statelessSessionContext)
        {
            this.Session = sessionContext.Session;
            this.StatelessSession = statelessSessionContext.Session;
        }

        public void ExecutePresumptiveCharitySegmentation(bool evaluateVisitsWithoutPtpScores)
        {
            this.Session.CreateSQLQuery(@"exec [scoring].[PresumptiveCharitySegmentation] :EvaluateVisitsWithoutPtpScores")
                .SetParameter("EvaluateVisitsWithoutPtpScores", evaluateVisitsWithoutPtpScores)
                .ExecuteUpdate();
        }

        public void ExecutePresumptiveCharityHistoryUpdate(int segmentationBatchId)
        {
            this.Session.CreateSQLQuery(@"exec [scoring].[PresumptiveCharitySegmentationHistoryUpdates] :SegmentationBatchId")
                .SetParameter("SegmentationBatchId", segmentationBatchId)
                .ExecuteUpdate();
        }

        public void ClearPresumptiveCharityGuarantorVisitDaily()
        {
            this.Session.CreateSQLQuery(@"DELETE FROM scoring.PresumptiveCharityGuarantorVisitDaily;")
                .ExecuteUpdate();
        }

        public IList<PresumptiveCharityEligibleVisit> GetBasePcEligibleVisits(DateTime minSelfPayDate, int batchSize, int lastProcessedVisitId)
        {
            List<int> lifecycleStageIds = new List<int> {(int) LifeCycleStageEnum.FullSelfPayDue, (int) LifeCycleStageEnum.PaymentPlan};
            Visit v = null;
            HsGuarantor hsg = null;
            PresumptiveCharityVisitHistory pcvh = null;
            PresumptiveCharityEligibleVisit rVisit = null;
            PrimaryInsuranceType pit = null;
            Facility facility = null;

            IList<PresumptiveCharityEligibleVisit> visits = this.StatelessSession.QueryOver(() => v)
                .JoinEntityAlias(() => hsg, () => v.HsGuarantorId == hsg.HsGuarantorId)
                .JoinEntityAlias(() => pcvh, () => v.VisitId == pcvh.VisitId, JoinType.LeftOuterJoin)
                .JoinEntityAlias(() => pit, () => v.PrimaryInsuranceType.PrimaryInsuranceTypeId == pit.PrimaryInsuranceTypeId)
                .JoinEntityAlias(() => facility, () => v.Facility.FacilityId == facility.FacilityId)
                .SelectList(list => list
                    .Select(z => v.VisitId).WithAlias(() => rVisit.VisitId)
                    .Select(z => v.HsGuarantorId).WithAlias(() => rVisit.HsGuarantorId)
                    .Select(z => v.EvaluateForPCSegmentation).WithAlias(() => rVisit.EvaluateForPCSegmentation)
                    .Select(z => v.FirstSelfPayDate).WithAlias(() => rVisit.FirstSelfPayDate)
                    .Select(z => (int)pit.SelfPayClass).WithAlias(() => rVisit.SelfPayClassId))
                .TransformUsing(Transformers.AliasToBean<PresumptiveCharityEligibleVisit>())
                .Where(() => facility.SegmentationEnabled)
                .Where(Restrictions.In(Projections.Property(() => v.LifeCycleStage.LifeCycleStageId), lifecycleStageIds))
                .Where(() => v.SelfPayBalance > 0)
                .Where(() => pcvh.VisitId == null) // only evaluate a visit once and only once
                .Where(() => hsg.VpEligible) // only evaluate VpEligible guarantors
                .Where(() => v.FirstSelfPayDate >= minSelfPayDate)
                .Where(() => v.VisitId > lastProcessedVisitId)
                .OrderBy(() => v.VisitId).Asc
                .Take(batchSize)
                .List<PresumptiveCharityEligibleVisit>();

            return visits;
        }
    }
}
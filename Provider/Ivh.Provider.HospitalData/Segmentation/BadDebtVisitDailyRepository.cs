﻿namespace Ivh.Provider.HospitalData.Segmentation
{
    using System.Collections.Generic;
    using Common.Data;
    using Common.Data.Connection.Connections;
    using Common.Data.Interfaces;
    using Domain.HospitalData.Segmentation.Entities;
    using Domain.HospitalData.Segmentation.Interfaces;
    using NHibernate;
    using NHibernate.Criterion;

    public class BadDebtVisitDailyRepository : RepositoryBase<BadDebtVisitDaily, CdiEtl>, IBadDebtVisitDailyRepository
    {

        public BadDebtVisitDailyRepository(ISessionContext<CdiEtl> sessionContext, IStatelessSessionContext<CdiEtl> statelessSessionContext) : base(sessionContext, statelessSessionContext)
        {
        }

        public void ExecuteBadDebtSegmentation(int segmentationBatchId)
        {
            this.Session.CreateSQLQuery(@"exec [scoring].[BadDebtSegmentation] :SegmentationBatchId")
                .SetParameter("SegmentationBatchId", segmentationBatchId)
                .ExecuteUpdate();
        }

        public IList<BadDebtVisitDaily> GetDailyBadDebtVisits(int segmentationBatchId, int batchSize, int lastProcessedGuarantorId)
        {
            IList<BadDebtVisitDaily> badDebtDailyResults = this.StatelessSession.CreateCriteria<BadDebtVisitDaily>("bd")
                .Add(Restrictions.Eq("SegmentationBatchId", segmentationBatchId))
                .Add(Restrictions.Gt("HsGuarantorId", lastProcessedGuarantorId))
                .AddOrder(Order.Asc("HsGuarantorId"))
                .SetMaxResults(batchSize)
                .List<BadDebtVisitDaily>();
            return badDebtDailyResults;
        }

    }
}


﻿namespace Ivh.Provider.HospitalData.Segmentation
{
    using System.Collections.Generic;
    using Common.Data;
    using Common.Data.Connection.Connections;
    using Common.Data.Interfaces;
    using Domain.HospitalData.Segmentation.Entities;
    using Domain.HospitalData.Segmentation.Interfaces;
    using NHibernate;
    using NHibernate.Criterion;

    public class AccountsReceivableVisitDailyRepository : RepositoryBase<AccountsReceivableVisitDaily, CdiEtl>, IAccountsReceivableVisitDailyRepository
    {

        public AccountsReceivableVisitDailyRepository(
            ISessionContext<CdiEtl> sessionContext, IStatelessSessionContext<CdiEtl> statelessSessionContext
        ) : base(sessionContext, statelessSessionContext)
        {
        }

        public void ExecuteAccountsReceivableSegmentation(int segmentationBatchId)
        {
            this.Session.CreateSQLQuery(@"EXEC [scoring].[AccountsReceivableSegmentation] :SegmentationBatchId")
                .SetParameter("SegmentationBatchId", segmentationBatchId)
                .ExecuteUpdate();
        }

        public IList<AccountsReceivableVisitDaily> GetDailyAccountsReceivableVisits(int segmentationBatchId, int batchSize, int lastProcessedGuarantorId)
        {
            IList<AccountsReceivableVisitDaily> accountsReceivableDailyResults = this.StatelessSession.CreateCriteria<AccountsReceivableVisitDaily>("ac")
                .Add(Restrictions.Eq("SegmentationBatchId", segmentationBatchId))
                .Add(Restrictions.Gt("HsGuarantorId", lastProcessedGuarantorId))
                .AddOrder(Order.Asc("HsGuarantorId"))
                .SetMaxResults(batchSize)
                .List<AccountsReceivableVisitDaily>();
            return accountsReceivableDailyResults;
        }

    }
}

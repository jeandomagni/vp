﻿namespace Ivh.Provider.HospitalData.Segmentation
{
    using System.Collections.Generic;
    using System.Linq;
    using Common.Data;
    using Common.Data.Connection.Connections;
    using Common.Data.Interfaces;
    using Domain.HospitalData.Segmentation.Entities;
    using Domain.HospitalData.Segmentation.Interfaces;
    using NHibernate;
    using NHibernate.Transform;

    public class ActivePassiveVisitDailyRepository : RepositoryBase<ActivePassiveVisitDaily, CdiEtl>, IActivePassiveVisitDailyRepository
    {

        public ActivePassiveVisitDailyRepository(ISessionContext<CdiEtl> sessionContext, IStatelessSessionContext<CdiEtl> statelessSessionContext) : base(sessionContext, statelessSessionContext)
        {
        }

        public void ExecuteActivePassiveSegmentation(int segmentationBatchId)
        {
            this.Session.CreateSQLQuery(@"EXEC [scoring].[ActivePassiveSegmentation] :SegmentationBatchId")
                .SetParameter("SegmentationBatchId", segmentationBatchId)
                .ExecuteUpdate();
        }

        public IList<ActivePassiveVisitDaily> GetDailyActivePassiveVisits(int segmentationBatchId, int batchSize, int lastProcessedGuarantorId)
        {
            return this.StatelessSession.CreateSQLQuery(@"EXEC [scoring].[ActivePassiveVisits] :SegmentationBatchId, :LastProcessedGuarantorId, :BatchSize")
                .SetParameter("SegmentationBatchId", segmentationBatchId)
                .SetParameter("LastProcessedGuarantorId", lastProcessedGuarantorId)
                .SetParameter("BatchSize", batchSize)
                .SetResultTransformer(Transformers.AliasToBean<ActivePassiveVisitDaily>())
                .List<ActivePassiveVisitDaily>()
                .ToList();
        }

    }
}

﻿namespace Ivh.Provider.HospitalData.Segmentation
{
    using System;
    using Common.Data;
    using Common.Data.Connection.Connections;
    using Common.Data.Interfaces;
    using Domain.HospitalData.Segmentation.Entities;
    using Domain.HospitalData.Segmentation.Interfaces;
    using NHibernate;

    public class GuarantorSegmentationDailyRepository : RepositoryBase<GuarantorSegmentationDaily, CdiEtl>, IGuarantorSegmentationDailyRepository
    {
        public GuarantorSegmentationDailyRepository(ISessionContext<CdiEtl> sessionContext) : base(sessionContext) { }

        public GuarantorSegmentationDailyRepository(ISessionContext<CdiEtl> sessionContext, IStatelessSessionContext<CdiEtl> statelessSessionContext) : base(sessionContext, statelessSessionContext) { }

        public void UpdateAccountsReceivableGuarantorSegmentationHistory(int segmentationBatchId)
        {
            this.Session.CreateSQLQuery(@"exec [scoring].[AccountsReceivableSegmentationHistoryUpdates] :SegmentationBatchId")
                .SetParameter("SegmentationBatchId", segmentationBatchId)
                .ExecuteUpdate();
        }

        public void UpdateActivePassiveGuarantorSegmentationHistory(int segmentationBatchId)
        {
            this.Session.CreateSQLQuery(@"exec [scoring].[ActivePassiveSegmentationHistoryUpdates] :SegmentationBatchId")
                .SetParameter("SegmentationBatchId", segmentationBatchId)
                .ExecuteUpdate();
        }

        public void UpdateBadDebtGuarantorSegmentationHistory(int segmentationBatchId)
        {
            this.Session.CreateSQLQuery(@"exec [scoring].[BadDebtSegmentationHistoryUpdates] :SegmentationBatchId")
                .SetParameter("SegmentationBatchId", segmentationBatchId)
                .ExecuteUpdate();
        }

    }
}

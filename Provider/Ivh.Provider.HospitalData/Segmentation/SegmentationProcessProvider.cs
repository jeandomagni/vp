﻿namespace Ivh.Provider.HospitalData.Segmentation
{
    using System;
    using System.Net.Http;
    using Common.Api.Client;
    using Common.Api.Helpers;
    using Domain.HospitalData.Segmentation.Interfaces;

    public class SegmentationProcessProvider : ISegmentationProcessProvider
    {
        public string CallSegmentationFunctionService(object scoringServiceInput, string baseUrl, string functionName, string authorizationCode, string appId, string appKey, string requestingAppName)
        {
            string url = $"/api/{functionName}?code={authorizationCode}";
            return HttpClientHelper.CallApiWith<string, object>(
                baseUrl,
                url,
                HttpMethod.Post,
                scoringServiceInput,
                (message, dto) => { },
                (message, s) => throw new Exception($"CallScoringServiceApi::GuarantorSegmentation - Something went wrong while calling the API.  StatusCode - {message.StatusCode}, ApiName - {requestingAppName}"),
                () => HttpClientFactory.Create(new ApiHandler(
                    appId,
                    appKey,
                    requestingAppName
                )),
                false
            );
        }
    }
}

﻿namespace Ivh.Provider.HospitalData.Segmentation.PresumptiveCharityEvaluationProviders
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Domain.HospitalData.Segmentation.Entities;
    using Domain.HospitalData.Segmentation.Interfaces;
    using Domain.HospitalData.Visit.Entities;
    using Domain.Settings.Interfaces;

    public class DefaultPresumptiveCharityEvaluationProvider : IPresumptiveCharityEvaluationProvider
    {
        private readonly Lazy<IClientService> _clientService;

        public DefaultPresumptiveCharityEvaluationProvider(
            Lazy<IClientService> clientService)
        {
            this._clientService = clientService;
        }

        public IList<PresumptiveCharityGuarantorVisitDaily> GetEligibleVisits(IList<PresumptiveCharityEligibleVisit> potentialVisits, int segmentationBatchId)
        {
            int daysSinceFirstSelfPayDate = this._clientService.Value.GetClient().SegmentationPresumptiveCharityDaysSinceFirstSelfPayDate;
            DateTime minValidFirstSelfPayDate = DateTime.Today.AddDays(-daysSinceFirstSelfPayDate);

            List<PresumptiveCharityGuarantorVisitDaily> eligibleVisits = potentialVisits
                .Where(x => x.FirstSelfPayDate?.Date <= minValidFirstSelfPayDate.Date)
                .Select(x => new PresumptiveCharityGuarantorVisitDaily {
                    VisitId = x.VisitId,
                    HsGuarantorId = x.HsGuarantorId,
                    SegmentationBatchId = segmentationBatchId
                }).ToList();

            return eligibleVisits;
        }
    }
}
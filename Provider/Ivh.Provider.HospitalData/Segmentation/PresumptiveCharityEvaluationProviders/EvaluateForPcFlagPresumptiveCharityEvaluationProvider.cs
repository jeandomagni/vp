﻿namespace Ivh.Provider.HospitalData.Segmentation.PresumptiveCharityEvaluationProviders
{
    using System.Collections.Generic;
    using System.Linq;
    using Domain.HospitalData.Segmentation.Entities;
    using Domain.HospitalData.Segmentation.Interfaces;
    using Domain.HospitalData.Visit.Entities;

    public class EvaluateForPcFlagPresumptiveCharityEvaluationProvider : IPresumptiveCharityEvaluationProvider
    {

        public IList<PresumptiveCharityGuarantorVisitDaily> GetEligibleVisits(IList<PresumptiveCharityEligibleVisit> potentialVisits, int segmentationBatchId)
        {
            List<PresumptiveCharityGuarantorVisitDaily> eligibleVisits = potentialVisits
                .Where(x => x.EvaluateForPCSegmentation.HasValue && x.EvaluateForPCSegmentation.Value)
                .Select(x => new PresumptiveCharityGuarantorVisitDaily
                {
                    VisitId = x.VisitId,
                    HsGuarantorId = x.HsGuarantorId,
                    SegmentationBatchId = segmentationBatchId
                }).ToList();

            return eligibleVisits;
        }
    }
}
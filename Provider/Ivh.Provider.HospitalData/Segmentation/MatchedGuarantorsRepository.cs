﻿namespace Ivh.Provider.HospitalData.Segmentation
{
    using System.Collections.Generic;
    using Common.Data;
    using Common.Data.Connection.Connections;
    using Common.Data.Interfaces;
    using Domain.HospitalData.Segmentation.Entities;
    using Domain.HospitalData.Segmentation.Interfaces;
    using NHibernate;
    using NHibernate.Criterion;

    public class MatchedGuarantorsRepository : RepositoryBase<MatchedGuarantors, CdiEtl>, IMatchedGuarantorsRepository
    {
        public MatchedGuarantorsRepository(ISessionContext<CdiEtl> sessionContext) : base(sessionContext)
        {
        }

        public IList<MatchedGuarantors> GetMatchedGuarantors(int parentHsGuarantorId)
        {
            ICriteria criteria = this.Session.CreateCriteria<MatchedGuarantors>();
            criteria.Add(Restrictions.Eq("ParentHsGuarantorId", parentHsGuarantorId));
            return criteria.List<MatchedGuarantors>();
        }
    }
}
﻿namespace Ivh.Provider.HospitalData.Segmentation
{
    using Common.Data;
    using Common.Data.Connection.Connections;
    using Common.Data.Interfaces;
    using Domain.HospitalData.Segmentation.Entities;
    using Domain.HospitalData.Segmentation.Interfaces;
    using NHibernate;

    public class ActivePassiveSegmentationThresholdRepository : RepositoryBase<ActivePassiveSegmentationThreshold, CdiEtl>, IActivePassiveSegmentationThresholdRepository
    {
        public ActivePassiveSegmentationThresholdRepository(ISessionContext<CdiEtl> sessionContext) : base(sessionContext)
        {
        }

    }
}
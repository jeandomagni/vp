﻿namespace Ivh.Provider.HospitalData.Segmentation
{
    using Common.Data;
    using Common.Data.Connection.Connections;
    using Common.Data.Interfaces;
    using Domain.HospitalData.Segmentation.Entities;
    using Domain.HospitalData.Segmentation.Interfaces;
    using NHibernate;

    public class AccountsReceivableSegmentationThresholdRepository : RepositoryBase<AccountsReceivableSegmentationThreshold, CdiEtl>, IAccountsReceivableSegmentationThresholdRepository
    {
        public AccountsReceivableSegmentationThresholdRepository(ISessionContext<CdiEtl> sessionContext) : base(sessionContext)
        {
        }
      
    }
}
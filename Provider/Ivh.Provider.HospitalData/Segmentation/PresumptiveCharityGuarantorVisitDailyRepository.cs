﻿namespace Ivh.Provider.HospitalData.Segmentation
{
    using Common.Data;
    using Common.Data.Connection.Connections;
    using Common.Data.Interfaces;
    using Domain.HospitalData.Segmentation.Entities;
    using Domain.HospitalData.Segmentation.Interfaces;

    public class PresumptiveCharityGuarantorVisitDailyRepository : RepositoryBase<PresumptiveCharityGuarantorVisitDaily, CdiEtl>, IPresumptiveCharityGuarantorVisitDailyRepository
    {
        public PresumptiveCharityGuarantorVisitDailyRepository(ISessionContext<CdiEtl> sessionContext, IStatelessSessionContext<CdiEtl> statelessSessionContext)
            : base(sessionContext, statelessSessionContext)
        {
        }

    }
}
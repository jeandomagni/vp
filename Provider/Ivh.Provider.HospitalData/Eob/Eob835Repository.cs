﻿namespace Ivh.Provider.HospitalData.Eob
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Common.Data;
    using Common.Data.Connection.Connections;
    using Common.Data.Interfaces;
    using Domain.HospitalData.Eob.Entities;
    using Domain.HospitalData.Eob.Interfaces;
    using NHibernate;
    using NHibernate.Linq;

    [Obsolete("This will be removed once all clients are no longer using EOB data from the CDI DB")]
    public class Eob835Repository : RepositoryBase<InterchangeControlHeaderTrailer, CdiEtl>, IEob835Repository
    {
        public Eob835Repository(IStatelessSessionContext<CdiEtl> statelessSessionContext) : base(statelessSessionContext)
        {
        }

        private const string EobServiceWarning = "Use Eob835Service instead to save EOB entities.";

        public override void InsertOrUpdate(InterchangeControlHeaderTrailer entity)
        {
            throw new Exception(EobServiceWarning);
        }

        public override void Update(InterchangeControlHeaderTrailer entity)
        {
            throw new Exception(EobServiceWarning);
        }

        public override void Insert(InterchangeControlHeaderTrailer entity)
        {
            throw new Exception(EobServiceWarning);
        }

        public virtual void UpdateMigrationDate(InterchangeControlHeaderTrailer entity)
        {
            this.StatelessSession.Query<InterchangeControlHeaderTrailer>()
                .Where(x => x.InterchangeControlHeaderTrailerId == entity.InterchangeControlHeaderTrailerId)
                .UpdateBuilder()
                .Set(y => y.MigrationDate, y => DateTime.UtcNow).Update();
        }

        public virtual InterchangeControlHeaderTrailer GetStatelessById(int id)
        {
            return this.StatelessSession.Get<InterchangeControlHeaderTrailer>(id);
        }

        public virtual IQueryable<InterchangeControlHeaderTrailer> GetStatelessAll()
        {
            return this.StatelessSession.Query<InterchangeControlHeaderTrailer>();
        }

        [Obsolete("Not worth the time to create objects and maps needed for one time migration")]
        public virtual IList<int> GetUnmigratedDisplayedEobInterchangeIds()
        {
            //No parameters used, so not currently vulnerable to injection
            string sql = @"select DISTINCT i.[InterchangeControlHeaderTrailerId]
            from [eob].[835InterchangeControlHeaderTrailer] i
            inner join [eob].[835FunctionalGroupHeaderTrailer] g on g.[InterchangeControlHeaderTrailerId] = i.[InterchangeControlHeaderTrailerId]
            inner join [eob].[835TransactionSetHeaderTrailer] t on t.[FunctionalGroupHeaderTrailerId] = g.[FunctionalGroupHeaderTrailerId]
            inner join [eob].[835HeaderNumber] h on h.[TransactionSetHeaderTrailerId] = t.[TransactionSetHeaderTrailerId]
            inner join [eob].[835ClaimPaymentInformation] c on c.[HeaderNumberId] = h.[HeaderNumberId]
            inner join [eob].[EobClaimsLink] cl on cl.ClaimNumber = c.CLP01
            inner join [Vp].[VpAppStatusReconciliation] v on cl.BillingSystemId = v.BillingSystemId and cl.[VisitSourceSystemKey] = v.VisitSSK
            where i.MigrationDate is null";

            IList<int> interchangeIds = this.StatelessSession.CreateSQLQuery(sql).List<int>();

            return interchangeIds;
        }

    }
}
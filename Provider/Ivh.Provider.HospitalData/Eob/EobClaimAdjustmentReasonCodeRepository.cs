﻿namespace Ivh.Provider.HospitalData.Eob
{
    using System.Collections.Generic;
    using Common.Data;
    using Common.Data.Connection.Connections;
    using Common.Data.Interfaces;
    using Domain.HospitalData.Eob.Entities;
    using Domain.HospitalData.Eob.Interfaces;
    using NHibernate;
    using NHibernate.Criterion;

    public class EobClaimAdjustmentReasonCodeRepository : RepositoryBase<EobClaimAdjustmentReasonCode, CdiEtl>, IEobClaimAdjustmentReasonCodeRepository
    {
        public EobClaimAdjustmentReasonCodeRepository(ISessionContext<CdiEtl> sessionContext) : base(sessionContext)
        {
        }

        public EobClaimAdjustmentReasonCode GetEobClaimAdjustmentReasonCode(string remitCode)
        {
            ICriteria query = this.Session.CreateCriteria<EobClaimAdjustmentReasonCode>();
            query.Add(Restrictions.Eq("ClaimAdjustmentReasonCode", remitCode));
            return query.UniqueResult<EobClaimAdjustmentReasonCode>();
        }

    }
}
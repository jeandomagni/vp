﻿namespace Ivh.Provider.HospitalData.Eob
{
    using Common.Data;
    using Common.Data.Connection.Connections;
    using Common.Data.Interfaces;
    using Domain.HospitalData.Eob.Entities;
    using Domain.HospitalData.Eob.Interfaces;
    using NHibernate;
    using NHibernate.Criterion;

    public class EobRemittanceAdviceRemarkCodeRepository : RepositoryBase<EobRemittanceAdviceRemarkCode, CdiEtl>, IEobRemittanceAdviceRemarkCodeRepository
    {
        public EobRemittanceAdviceRemarkCodeRepository(ISessionContext<CdiEtl> sessionContext) : base(sessionContext)
        {
        }

        public EobRemittanceAdviceRemarkCode GetEobRemittanceAdviceRemarkCode(string remarkCode)
        {
            ICriteria query = this.Session.CreateCriteria<EobRemittanceAdviceRemarkCode>();
            query.Add(Restrictions.Eq("RemittanceRemarkCode", remarkCode));
            return query.UniqueResult<EobRemittanceAdviceRemarkCode>();
        }

    }
}
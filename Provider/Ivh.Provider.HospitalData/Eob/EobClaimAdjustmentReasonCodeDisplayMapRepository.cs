﻿namespace Ivh.Provider.HospitalData.Eob
{
    using Common.Data;
    using Common.Data.Connection.Connections;
    using Common.Data.Interfaces;
    using Domain.HospitalData.Eob.Entities;
    using Domain.HospitalData.Eob.Interfaces;

    public class EobClaimAdjustmentReasonCodeDisplayMapRepository : RepositoryBase<EobClaimAdjustmentReasonCodeDisplayMap, CdiEtl>, IEobClaimAdjustmentReasonCodeDisplayMapRepository
    {
        public EobClaimAdjustmentReasonCodeDisplayMapRepository(ISessionContext<CdiEtl> sessionContext) : base(sessionContext)
        {
        }
    }
}

﻿namespace Ivh.Provider.HospitalData.Eob
{
    using Common.Data;
    using Common.Data.Connection.Connections;
    using Common.Data.Interfaces;
    using Domain.HospitalData.Eob.Entities;
    using Domain.HospitalData.Eob.Interfaces;
    using NHibernate;

    public class EobPayerFilterRepository : RepositoryBase<EobPayerFilter, CdiEtl>, IEobPayerFilterRepository
    {
        public EobPayerFilterRepository(ISessionContext<CdiEtl> sessionContext, IStatelessSessionContext<CdiEtl> statelessSessionContext) : base(sessionContext, statelessSessionContext)
        {
        }
    }
}
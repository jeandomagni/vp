﻿namespace Ivh.Provider.HospitalData.Eob
{
    using Common.Data;
    using Common.Data.Connection.Connections;
    using Common.Data.Interfaces;
    using Domain.HospitalData.Eob.Entities;
    using Domain.HospitalData.Eob.Interfaces;
    using NHibernate;

    public class ExternalLinkRepository : RepositoryBase<ExternalLink, CdiEtl>, IExternalLinkRepository
    {
        public ExternalLinkRepository(ISessionContext<CdiEtl> sessionContext, IStatelessSessionContext<CdiEtl> statelessSessionContext) : base(sessionContext, statelessSessionContext)
        {

        }
    }
}

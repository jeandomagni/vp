﻿namespace Ivh.Provider.HospitalData.Eob
{
    using System.Collections.Generic;
    using System.Linq;
    using Common.Base.Utilities.Extensions;
    using Common.Data;
    using Common.Data.Connection.Connections;
    using Common.Data.Interfaces;
    using Domain.HospitalData.Eob.Entities;
    using Domain.HospitalData.Eob.Interfaces;
    using NHibernate;
    using NHibernate.Criterion;
    using NHibernate.Transform;

    public class EobClaimsLinkRepository : RepositoryBase<EobClaimsLink, CdiEtl>, IEobClaimsLinkRepository
    {
        public EobClaimsLinkRepository(ISessionContext<CdiEtl> sessionContext) : base(sessionContext)
        {
        }

        public EobClaimsLink GetClaimsLink(string claimNumber)
        {
            ICriteria claimsLinkQuery = this.Session.CreateCriteria<EobClaimsLink>();
            claimsLinkQuery.Add(Restrictions.Eq("ClaimNumber", claimNumber));
            return claimsLinkQuery.UniqueResult<EobClaimsLink>();
        }

        public List<EobClaimsLink> GetClaimsLinks(List<string> claimNumbers)
        {
            List<EobClaimsLink> eobClaimsLinkList = new List<EobClaimsLink>();
            foreach (IEnumerable<string> chunk in claimNumbers.Chunk(500))
            {
                EobClaimsLink eobClaimsLinkAlias = null;
                IList<EobClaimsLink> chunkList = this.Session.QueryOver<EobClaimsLink>(() => eobClaimsLinkAlias)
                    .WhereRestrictionOn(x => x.ClaimNumber).IsIn(chunk.ToList())
                    .SelectList(list => list
                        .SelectMax(x => x.EobClaimsLinkId).WithAlias(() => eobClaimsLinkAlias.EobClaimsLinkId)
                        .SelectGroup(x => x.ClaimNumber).WithAlias(() => eobClaimsLinkAlias.ClaimNumber)
                        .SelectGroup(x => x.VisitSourceSystemKey).WithAlias(() => eobClaimsLinkAlias.VisitSourceSystemKey)
                        .SelectGroup(x => x.BillingSystemId).WithAlias(() => eobClaimsLinkAlias.BillingSystemId)
                    )
                    .TransformUsing(Transformers.AliasToBean<EobClaimsLink>())
                    .List<EobClaimsLink>();

                eobClaimsLinkList.AddRange(chunkList);
            }

            return eobClaimsLinkList;
        }

    }
}
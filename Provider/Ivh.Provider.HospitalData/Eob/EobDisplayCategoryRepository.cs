﻿namespace Ivh.Provider.HospitalData.Eob
{
    using Common.Data;
    using Common.Data.Connection.Connections;
    using Common.Data.Interfaces;
    using Domain.HospitalData.Eob.Entities;
    using Domain.HospitalData.Eob.Interfaces;

    public class EobDisplayCategoryRepository : RepositoryBase<EobDisplayCategory, CdiEtl>, IEobDisplayCategoryRepository
    {
        public EobDisplayCategoryRepository(ISessionContext<CdiEtl> sessionContext) : base(sessionContext)
        {
        }
    }
}

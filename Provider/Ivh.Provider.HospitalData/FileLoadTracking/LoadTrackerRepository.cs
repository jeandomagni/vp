﻿namespace Ivh.Provider.HospitalData.FileLoadTracking
{
    using System.Collections.Generic;
    using System.Linq;
    using Common.Data;
    using Common.Data.Connection.Connections;
    using Common.Data.Interfaces;
    using Common.VisitPay.Enums;
    using Domain.HospitalData.FileLoadTracking.Entities;
    using Domain.HospitalData.FileLoadTracking.Interfaces;
    using NHibernate.Criterion;

    public class LoadTrackerRepository : RepositoryBase<LoadTracker, CdiEtl>, ILoadTrackerRepository
    {
        public LoadTrackerRepository(ISessionContext<CdiEtl> sessionContext) : base(sessionContext)
        {
        }

        public IList<LoadTracker> GetLastEobLoadTrackers()
        {
            List<LoadTracker> loadTrackers = new List<LoadTracker>();

            DetachedCriteria lastProcessedLoadTrackerFolder = DetachedCriteria.For<LoadTracker>("lt")
                .SetMaxResults(1)
                .SetProjection(Projections.Property("LoadTrackerId"))
                .AddOrder(Order.Desc("LoadTrackerId"))
                .Add(Restrictions.Eq("LoadType.LoadTypeId", (int)LoadTypeEnum.Daily835RemitInboundProcess))
                .Add(Restrictions.Eq("LoadStatus.LoadStatusId", (int)LoadStatusEnum.Completed));

            DetachedCriteria unProcessedLoadTrackerFolders = DetachedCriteria.For<LoadTracker>("lt")
                .SetProjection(Projections.Property("LoadTrackerId"))
                .Add(Restrictions.Eq("LoadType.LoadTypeId", (int)LoadTypeEnum.Daily835RemitInboundProcess))
                .Add(Restrictions.Eq("LoadStatus.LoadStatusId", (int)LoadStatusEnum.InProgress)); 

            LoadTracker loadTracker = this.Session.CreateCriteria<LoadTracker>()
                .Add(Subqueries.PropertyEq("LoadTrackerId", lastProcessedLoadTrackerFolder))
                .UniqueResult<LoadTracker>();
            if (loadTracker != null)
            {
                loadTrackers.Add(loadTracker);

                // SE-124 - Get any load trackers more recent than last Completed LoadTracker folder (except any in progress. all of those are retrieved below)
                loadTrackers.AddRange(this.Session.CreateCriteria<LoadTracker>("lt")
                    .Add(Restrictions.Eq("LoadType.LoadTypeId", (int)LoadTypeEnum.Daily835RemitInboundProcess))
                    .Add(Restrictions.Not(Restrictions.Eq("LoadStatus.LoadStatusId", (int)LoadStatusEnum.InProgress)))
                    .Add(Restrictions.Gt("LoadTrackerId", loadTracker.LoadTrackerId))
                    .List<LoadTracker>());
            }

            loadTrackers.AddRange(this.Session.CreateCriteria<LoadTracker>()
                .Add(Subqueries.PropertyIn("LoadTrackerId", unProcessedLoadTrackerFolders))
                .List<LoadTracker>().ToList());

            return loadTrackers;
        }

        public LoadTracker GetLastFailedEobLoadTracker()
        {
            return this.Session.Query<LoadTracker>()
                .Where(x => x.LoadStatus.LoadStatusId == (int)LoadStatusEnum.Failed)
                .Where(x => x.LoadType.LoadTypeId == (int)LoadTypeEnum.Daily835RemitInboundProcess)
                .OrderByDescending(x => x.LoadTrackerId)
                .FirstOrDefault();
        }

        public IList<LoadTrackerStep> GetLoadTrackerSteps(int beforeLoadTrackerStepId)
        {
            IList<LoadTrackerStep> steps = this.Session.QueryOver<LoadTrackerStep>().Where(x => x.LoadTrackerStepId < beforeLoadTrackerStepId).List();

            return steps;
        }

        public LoadTracker GetLastLoadTracker(int loadTypeId)
        {
            return this.Session.Query<LoadTracker>()
                .Where(x => x.LoadType.LoadTypeId == loadTypeId)
                .OrderByDescending(x => x.LoadTrackerId)
                .FirstOrDefault();
        }
    }
}
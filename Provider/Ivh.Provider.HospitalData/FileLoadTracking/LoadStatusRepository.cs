﻿namespace Ivh.Provider.HospitalData.FileLoadTracking
{
    using Common.Data;
    using Common.Data.Connection.Connections;
    using Common.Data.Interfaces;
    using Domain.HospitalData.FileLoadTracking.Entities;
    using Domain.HospitalData.FileLoadTracking.Interfaces;

    public class LoadStatusRepository : RepositoryBase<LoadStatus, CdiEtl>, ILoadStatusRepository
    {
        public LoadStatusRepository(ISessionContext<CdiEtl> sessionContext) : base(sessionContext)
        {
        }
    }
}
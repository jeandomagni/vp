﻿namespace Ivh.Provider.HospitalData.FileLoadTracking
{
    using Common.Data;
    using Common.Data.Connection.Connections;
    using Common.Data.Interfaces;
    using Domain.HospitalData.FileLoadTracking.Entities;
    using Domain.HospitalData.FileLoadTracking.Interfaces;

    public class LoadTypeRepository : RepositoryBase<LoadType, CdiEtl>, ILoadTypeRepository
    {
        public LoadTypeRepository(ISessionContext<CdiEtl> sessionContext) : base(sessionContext)
        {
        }
    }
}
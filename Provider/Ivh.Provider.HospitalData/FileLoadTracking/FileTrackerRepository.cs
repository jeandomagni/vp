﻿namespace Ivh.Provider.HospitalData.FileLoadTracking
{
    using System.Linq;
    using Common.Data;
    using Common.Data.Connection.Connections;
    using Common.Data.Interfaces;
    using Domain.HospitalData.FileLoadTracking.Entities;
    using Domain.HospitalData.FileLoadTracking.Interfaces;

    public class FileTrackerRepository : RepositoryBase<FileTracker, CdiEtl>, IFileTrackerRepository
    {
        public FileTrackerRepository(ISessionContext<CdiEtl> sessionContext) : base(sessionContext)
        {
        }

    }
}
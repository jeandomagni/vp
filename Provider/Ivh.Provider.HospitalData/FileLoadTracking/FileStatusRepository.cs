﻿namespace Ivh.Provider.HospitalData.FileLoadTracking
{
    using Common.Data;
    using Common.Data.Connection.Connections;
    using Common.Data.Interfaces;
    using Domain.HospitalData.FileLoadTracking.Entities;
    using Domain.HospitalData.FileLoadTracking.Interfaces;
    using NHibernate;

    public class FileStatusRepository : RepositoryBase<FileStatus, CdiEtl>, IFileStatusRepository
    {
        public FileStatusRepository(ISessionContext<CdiEtl> sessionContext) : base(sessionContext)
        {
        }
    }
}
﻿namespace Ivh.Provider.HospitalData.FileLoadTracking
{
    using Common.Data;
    using Common.Data.Connection.Connections;
    using Common.Data.Interfaces;
    using Domain.HospitalData.FileLoadTracking.Entities;
    using Domain.HospitalData.FileLoadTracking.Interfaces;

    public class LoadTrackerStepHistoryRepository : RepositoryBase<LoadTrackerStepHistory, CdiEtl>, ILoadTrackerStepHistoryRepository
    {
        public LoadTrackerStepHistoryRepository(ISessionContext<CdiEtl> sessionContext) : base(sessionContext)
        {
        }
    }
}

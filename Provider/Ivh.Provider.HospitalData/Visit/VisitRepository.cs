﻿namespace Ivh.Provider.HospitalData.Visit
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Common.Cache;
    using Common.Data;
    using Common.Data.Connection.Connections;
    using Common.Data.Interfaces;
    using Domain.HospitalData.BillingSystem.Entities;
    using Domain.HospitalData.Visit.Entities;
    using Domain.HospitalData.Visit.Interfaces;
    using NHibernate;
    using NHibernate.Criterion;

    public class VisitRepository : RepositoryCacheBase<Visit, CdiEtl>, IVisitRepository
    {
        public VisitRepository(ISessionContext<CdiEtl> sessionContext, Lazy<IDistributedCache> cache) : base(sessionContext, cache)
        {
        }

        public Visit GetVisitWithBillingSystem(int visitId)
        {
            ICriteria hsVisitQuery = this.Session.CreateCriteria<Visit>();
            hsVisitQuery.Add(Restrictions.Eq("VisitId", visitId));

            Visit hsVisit = hsVisitQuery.UniqueResult<Visit>();

            ICriteria billingSystemQuery = this.Session.CreateCriteria<HsBillingSystem>();
            billingSystemQuery.Add(Restrictions.Eq("BillingSystemId", hsVisit.BillingSystemId));

            hsVisit.BillingSystem = billingSystemQuery.UniqueResult<HsBillingSystem>();

            return hsVisit;
        }

        public Visit GetVisit(int billingSystemId, string sourceSystemKey)
        {
            return this.GetCached(
                x => x.BillingSystemId, billingSystemId,
                x => x.SourceSystemKey, sourceSystemKey,
                (b, s) =>
                {
                    ICriteria hsVisitQuery = this.Session.CreateCriteria<Visit>();
                    hsVisitQuery.Add(Restrictions.Eq("BillingSystemId", b));
                    hsVisitQuery.Add(Restrictions.Eq("SourceSystemKey", s));
                    return hsVisitQuery.UniqueResult<Visit>();
                });
        }

        public IReadOnlyList<Visit> GetVisitsForGuarantor(int hsGuarantorId)
        {
            ICriteria hsVisitQuery = this.Session.CreateCriteria<Visit>();
            hsVisitQuery.Add(Restrictions.Eq("HsGuarantorId", hsGuarantorId));

            try
            {
                IList<Visit> hsVisits = hsVisitQuery.List<Visit>();

                ICriteria billingSystemQuery = this.Session.CreateCriteria<HsBillingSystem>();

                IList<HsBillingSystem> hsBillingSystems = billingSystemQuery.List<HsBillingSystem>();

                foreach (Visit hsVisit in hsVisits)
                {
                    hsVisit.BillingSystem = hsBillingSystems.FirstOrDefault(x => x.BillingSystemId == hsVisit.BillingSystemId);
                }

                return (IReadOnlyList<Visit>)hsVisits;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                throw;
            }
        }

        public bool IsVisitSuspended(int visitId)
        {
            ICriteria criteria = this.Session.CreateCriteria<VisitSuspensionList>()
                .Add(Restrictions.Eq("VisitId", visitId))
                .SetProjection(Projections.RowCount());

            return criteria.UniqueResult<int>() > 0;
        }

        public bool GuarantorHasBadDebtVisits(IEnumerable<int> hsGuarantorIds)
        {
            //LifeCycleStage 
            //8 Primary Bad Debt
            //9 Secondary Bad Debt
            ICriteria hsVisitQuery = this.Session.CreateCriteria<Visit>()
                .Add(Restrictions.In(nameof(Visit.HsGuarantorId), hsGuarantorIds.ToList()))
                .Add(Restrictions.In(nameof(Visit.LifeCycleStage), new List<int> { 8, 9 }))
                .SetProjection(Projections.RowCount());
            return hsVisitQuery.UniqueResult<int>() > 0;
        }

        public bool GuarantorHasVisitOnPrePaymentPlan(IEnumerable<int> hsGuarantorIds)
        {
            ICriteria hsVisitQuery = this.Session.CreateCriteria<Visit>()
                .Add(Restrictions.In(nameof(Visit.HsGuarantorId), hsGuarantorIds.ToList()))
                .Add(Restrictions.Eq(nameof(Visit.OnPrePaymentPlan), true))
                .SetProjection(Projections.RowCount());
            return hsVisitQuery.UniqueResult<int>() > 0;
        }

        protected override int GetId(Visit entity)
        {
            return entity.VisitId;
        }
    }
}

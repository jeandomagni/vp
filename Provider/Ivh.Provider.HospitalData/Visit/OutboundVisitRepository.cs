﻿namespace Ivh.Provider.HospitalData.Visit
{
    using System.Collections.Generic;
    using Common.Data;
    using Common.Data.Connection.Connections;
    using Common.Data.Interfaces;
    using Domain.HospitalData.Visit.Entities;
    using Domain.HospitalData.Visit.Interfaces;
    using NHibernate;

    public class OutboundVisitRepository : RepositoryBase<VpOutboundVisit, CdiEtl>, IOutboundVisitRepository
    {
        public OutboundVisitRepository(ISessionContext<CdiEtl> sessionContext)
            : base(sessionContext)
        {
        }

        public IList<VpOutboundVisit> GetOutboundVisitsByVisit(int visitId)
        {
            IList<VpOutboundVisit> result = this.Session.QueryOver<VpOutboundVisit>().Where(x => x.Visit.VisitId == visitId).List<VpOutboundVisit>();
            return result;
        }
    }
}
﻿namespace Ivh.Provider.HospitalData.Visit
{
    using System.Collections.Generic;
    using Common.Data;
    using Common.Data.Connection.Connections;
    using Common.Data.Interfaces;
    using Domain.HospitalData.Visit.Entities;
    using Domain.HospitalData.Visit.Interfaces;
    using NHibernate;

    public class RevenueWorkCompanyRepository : RepositoryBase<RevenueWorkCompany, CdiEtl>, IRevenueWorkCompanyRepository
    {
        public RevenueWorkCompanyRepository(ISessionContext<CdiEtl> sessionContext) : base(sessionContext)
        {
        }

        public IList<RevenueWorkCompany> GetAllRevenueWorkCompany()
        {
            return this.Session.QueryOver<RevenueWorkCompany>()
                .OrderBy(vs => vs.RevenueWorkCompanyId).Asc
                .Cacheable()
                .CacheMode(CacheMode.Normal)
                .List();
        }
    }
}
﻿namespace Ivh.Provider.HospitalData.Visit
{
    using Common.Data;
    using Common.Data.Connection.Connections;
    using Common.Data.Interfaces;
    using Domain.HospitalData.Visit.Entities;
    using Domain.HospitalData.Visit.Interfaces;

    public class VisitChangeRepository : RepositoryBase<VisitChange,CdiEtl>, IVisitChangeRepository
    {
        public VisitChangeRepository(ISessionContext<CdiEtl> sessionContext)
            : base(sessionContext)
        {
        }
    }
}

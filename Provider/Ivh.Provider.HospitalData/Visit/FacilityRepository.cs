﻿namespace Ivh.Provider.HospitalData.Visit
{
    using System.Collections.Generic;
    using Common.Data;
    using Common.Data.Connection.Connections;
    using Common.Data.Interfaces;
    using Domain.HospitalData.Visit.Entities;
    using Domain.HospitalData.Visit.Interfaces;
    using NHibernate;

    public class FacilityRepository 
        : RepositoryBase<Facility, CdiEtl>, IFacilityRepository
    {
        public FacilityRepository(ISessionContext<CdiEtl> sessionContext) 
            : base(sessionContext)
        {
        }

        public IList<Facility> GetAllFacilities()
        {
            return
                this.Session.QueryOver<Facility>()
                    .OrderBy(f => f.FacilityId).Asc
                    .Cacheable()
                    .CacheMode(CacheMode.Normal)
                    .List();
        }
    }
}
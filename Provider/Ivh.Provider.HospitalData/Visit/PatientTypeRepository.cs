﻿namespace Ivh.Provider.HospitalData.Visit
{
    using System.Collections.Generic;
    using Common.Data;
    using Common.Data.Connection.Connections;
    using Common.Data.Interfaces;
    using Domain.HospitalData.Visit.Entities;
    using Domain.HospitalData.Visit.Interfaces;
    using NHibernate;

    public class PatientTypeRepository : RepositoryBase<PatientType,CdiEtl>, IPatientTypeRepository
    {
        public PatientTypeRepository(ISessionContext<CdiEtl> sessionContext) : base(sessionContext)
        {
        }

        public IList<PatientType> GetAllPatientType()
        {
            return this.Session.QueryOver<PatientType>()
                .OrderBy(vs => vs.PatientTypeId).Asc
                .Cacheable()
                .CacheMode(CacheMode.Normal)
                .List();
        }
    }
}
﻿namespace Ivh.Provider.HospitalData.Visit
{
    using System.Collections.Generic;
    using System.Linq;
    using Common.Data;
    using Common.Data.Connection.Connections;
    using Common.Data.Interfaces;
    using Domain.HospitalData.Visit.Entities;
    using Domain.HospitalData.Visit.Interfaces;
    using NHibernate;

    public class LifeCycleStageRepository : RepositoryBase<LifeCycleStage, CdiEtl>, ILifeCycleStageRepository
    {
        public LifeCycleStageRepository(ISessionContext<CdiEtl> sessionContext) : base(sessionContext)
        {
        }

        public IList<LifeCycleStage> GetAllLifeCycleStage()
        {
            return this.Session.QueryOver<LifeCycleStage>()
                .OrderBy(vs => vs.LifeCycleStageId).Asc
                .Cacheable()
                .CacheMode(CacheMode.Normal)
                .List();
        }
    }
}
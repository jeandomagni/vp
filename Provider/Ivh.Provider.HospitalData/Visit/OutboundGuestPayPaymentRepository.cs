﻿namespace Ivh.Provider.HospitalData.Visit
{
    using System.Collections.Generic;
    using Common.Data;
    using Common.Data.Connection.Connections;
    using Common.Data.Interfaces;
    using Domain.HospitalData.Visit.Entities;
    using Domain.HospitalData.Visit.Interfaces;
    using NHibernate;

    public class OutboundGuestPayPaymentRepository : RepositoryBase<VpOutboundGuestPayPayment, CdiEtl>, IOutboundGuestPayPaymentRepository
    {
        public OutboundGuestPayPaymentRepository(
            ISessionContext<CdiEtl> sessionContext)
            : base(sessionContext)
        {
        }

        public IList<VpOutboundGuestPayPayment> GetVpOutboundGuestPayPayment(int vpPaymentId, int vpPaymentUnitId)
        {
            IQueryOver<VpOutboundGuestPayPayment, VpOutboundGuestPayPayment> query = this.Session.QueryOver<VpOutboundGuestPayPayment>()
                .Where(x => x.VpPaymentId == vpPaymentId && x.VpPaymentUnitId == vpPaymentUnitId);

            return query.List();
        }
    }
}
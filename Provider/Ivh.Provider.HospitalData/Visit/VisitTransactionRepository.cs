﻿namespace Ivh.Provider.HospitalData.Visit
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Common.Base.Utilities.Extensions;
    using Common.Base.Utilities.Helpers;
    using Common.Data;
    using Common.Data.Connection.Connections;
    using Common.Data.Interfaces;
    using Common.VisitPay.Enums;
    using Domain.HospitalData.Visit.Entities;
    using Domain.HospitalData.Visit.Interfaces;
    using NHibernate;
    using NHibernate.Criterion;

    public class VisitTransactionRepository : RepositoryBase<VisitTransaction, CdiEtl>, IVisitTransactionRepository
    {
        public VisitTransactionRepository(ISessionContext<CdiEtl> sessionContext) : base(sessionContext)
        {
        }

        public IList<TransactionCode> GetTransactionCodes()
        {
            return this.Session.Query<TransactionCode>().ToList();
        }

        public IList<VpTransactionType> GetTransactionTypes()
        {
            return this.Session.Query<VpTransactionType>().ToList();
        }

        public IList<VisitTransaction> GetVisitTransactions(int hsGuarantorId, ICollection<VpTransactionTypeEnum> vpTransactionTypes, DateTime? postDateBegin = null, DateTime? postDateEnd = null)
        {
            Visit visitAlias = null;
            VpTransactionType vpTransactionTypeAlias = null;

            IQueryOver<VisitTransaction, VisitTransaction> query = this.Session
                .QueryOver<VisitTransaction>()
                .JoinAlias(vt => vt.VpTransactionType, () => vpTransactionTypeAlias)
                .JoinAlias(v => v.Visit, () => visitAlias)
                .Where(vp => visitAlias.HsGuarantorId == hsGuarantorId);

            query.If(postDateBegin.HasValue, q => q.Where(vp => vp.PostDate >= postDateBegin.Value))
                .If(postDateEnd.HasValue, q => q.Where(vp => vp.PostDate <= postDateEnd.Value))
                .If(vpTransactionTypes.IsNotNullOrEmpty(), q => q.Where(Restrictions.In(Projections.Property(() => vpTransactionTypeAlias.VpTransactionTypeId), vpTransactionTypes.ToList())));

            return query.List();
        }

        public IList<VisitTransaction> GetGuarantorAccountTransactions(int visitId, DateTime? postDateBegin = null)
        {
            Visit visitAlias = null;
            VpTransactionType vpTransactionTypeAlias = null;

            this.Session.Clear();
            IQueryOver<VisitTransaction, VisitTransaction> query = this.Session
                .QueryOver<VisitTransaction>()
                .JoinAlias(vt => vt.VpTransactionType, () => vpTransactionTypeAlias)
                .JoinAlias(v => v.Visit, () => visitAlias)
                .Where(v => v.Visit.VisitId == visitId);

            List<VpTransactionTypeEnum> vpTransactionTypes = EnumHelper<VpTransactionTypeEnum>.GetValues(VpTransactionTypeEnumCategory.Hospital).ToList();
            query.If(postDateBegin.HasValue, q => q.Where(vp => vp.PostDate >= postDateBegin.Value))
                .If(vpTransactionTypes.IsNotNullOrEmpty(), q => q.Where(Restrictions.In("VpTransactionType.VpTransactionTypeId", vpTransactionTypes.ToList())));

            return query.List();
        }
    }
}
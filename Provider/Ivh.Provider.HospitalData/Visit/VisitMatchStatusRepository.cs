﻿namespace Ivh.Provider.HospitalData.Visit
{
    using System.Collections.Generic;
    using Common.Data;
    using Common.Data.Connection.Connections;
    using Common.Data.Interfaces;
    using Domain.HospitalData.Visit.Entities;
    using Domain.HospitalData.Visit.Interfaces;
    using Domain.HospitalData.VpGuarantor.Entities;
    using NHibernate;
    using NHibernate.Criterion;

    public class VisitMatchStatusRepository : RepositoryBase<VisitMatchStatus,CdiEtl>, IVisitMatchStatusRepository
    {
        public VisitMatchStatusRepository(ISessionContext<CdiEtl> sessionContext)
            : base(sessionContext)
        {
        }

        public VisitMatchStatus GetVisitMatchStatus(VpGuarantorHsMatch vpGuarantorHsMatch, int visitId)
        {
            IQueryOver<VisitMatchStatus, VisitMatchStatus> query = this.Session.QueryOver<VisitMatchStatus>();
            query.Where(x => x.HsGuarantorId == vpGuarantorHsMatch.HsGuarantorId);
            query.And(x => x.VpGuarantorId == vpGuarantorHsMatch.VpGuarantorId);
            query.And(x => x.VisitId == visitId);

            return query.Take(1).SingleOrDefault();
        }

        public IList<VisitMatchStatus> GetAllVisitMatchStatues(IList<VpGuarantorHsMatch> vpGuarantorHsMatches)
        {
            IQueryOver<VisitMatchStatus, VisitMatchStatus> query = this.Session.QueryOver<VisitMatchStatus>();
            var holdOr = Restrictions.Disjunction();
            foreach (VpGuarantorHsMatch vpGuarantorHsMatch in vpGuarantorHsMatches)
            {
                var holdAnd = Restrictions.Conjunction();
                holdAnd.Add(Restrictions.Eq("VpGuarantorId" ,vpGuarantorHsMatch.VpGuarantorId));
                holdAnd.Add(Restrictions.Eq("HsGuarantorId" ,vpGuarantorHsMatch.HsGuarantorId));
                holdOr.Add(holdAnd);
            }
            query.Where(holdOr);
            
            return query.List();
        }
    }
}

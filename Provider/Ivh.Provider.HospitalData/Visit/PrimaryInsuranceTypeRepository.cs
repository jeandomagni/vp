﻿namespace Ivh.Provider.HospitalData.Visit
{
    using System.Collections.Generic;
    using Common.Data;
    using Common.Data.Connection.Connections;
    using Common.Data.Interfaces;
    using Domain.HospitalData.Visit.Entities;
    using Domain.HospitalData.Visit.Interfaces;
    using NHibernate;

    public class PrimaryInsuranceTypeRepository : RepositoryBase<PrimaryInsuranceType, CdiEtl>, IPrimaryInsuranceTypeRepository
    {
        public PrimaryInsuranceTypeRepository(ISessionContext<CdiEtl> sessionContext) : base(sessionContext)
        {
        }
        public IList<PrimaryInsuranceType> GetAllPrimaryInsuranceType()
        {
            return this.Session.QueryOver<PrimaryInsuranceType>()
                .OrderBy(vs => vs.PrimaryInsuranceTypeId).Asc
                .Cacheable()
                .CacheMode(CacheMode.Normal)
                .List();
        }
    }
}

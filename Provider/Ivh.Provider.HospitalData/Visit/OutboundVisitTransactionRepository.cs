﻿namespace Ivh.Provider.HospitalData.Visit
{
    using System.Collections.Generic;
    using Common.Data;
    using Common.Data.Connection.Connections;
    using Common.Data.Interfaces;
    using Domain.HospitalData.Visit.Entities;
    using Domain.HospitalData.Visit.Interfaces;
    using NHibernate;
    using NHibernate.Criterion;

    public class OutboundVisitTransactionRepository : RepositoryBase<VpOutboundVisitTransaction, CdiEtl>, IOutboundVisitTransactionRepository
    {
        public OutboundVisitTransactionRepository(ISessionContext<CdiEtl> sessionContext)
            : base(sessionContext)
        {
        }

        public IList<VpOutboundVisitTransaction> GetOutboundVisitTransactionsByVisit(int visitId)
        {
            IList<VpOutboundVisitTransaction> result = this.Session.QueryOver<VpOutboundVisitTransaction>().Where(x => x.Visit.VisitId == visitId).List<VpOutboundVisitTransaction>();
            return result;
        }

        public IList<VpOutboundVisitTransaction> GetUnclearedOutboundVisitTransactions()
        {
            IQueryOver<VpOutboundVisitTransaction, VpOutboundVisitTransaction> query = this.GetUnclearedOutboundVisitTransactionsQuery();
            return query.List<VpOutboundVisitTransaction>();
        }

        public IList<VpOutboundVisitTransaction> GetUnclearedOutboundVisitTransactions(int visitId)
        {
            IQueryOver<VpOutboundVisitTransaction, VpOutboundVisitTransaction> query = this.GetUnclearedOutboundVisitTransactionsQuery();
            query = query.Where(x => x.Visit.VisitId == visitId);

            return query.List<VpOutboundVisitTransaction>();
        }

        private IQueryOver<VpOutboundVisitTransaction, VpOutboundVisitTransaction> GetUnclearedOutboundVisitTransactionsQuery()
        {
            VpOutboundVisitTransaction vpOutboundVisitTransactionAlias = null;

            QueryOver<VisitPaymentAllocationChange, VisitPaymentAllocationChange> fileChangeSubquery = QueryOver.Of<VisitPaymentAllocationChange>()
                .Where(x => x.PaymentAllocationId == vpOutboundVisitTransactionAlias.VpPaymentAllocationId)
                .ToRowCountQuery();

            IQueryOver<VpOutboundVisitTransaction, VpOutboundVisitTransaction> query = this.Session.QueryOver(() => vpOutboundVisitTransactionAlias)
                .Where(x => x.VpPaymentAllocationId != null)
                .Where(Restrictions.Eq(Projections.SubQuery(fileChangeSubquery), 0));

            return query;
        }
    }
}
﻿namespace Ivh.Provider.HospitalData.Visit
{
    using Common.Data;
    using Common.Data.Connection.Connections;
    using Common.Data.Interfaces;
    using Domain.HospitalData.Visit.Entities;
    using Domain.HospitalData.Visit.Interfaces;
    using NHibernate;

    public class VisitInsurancePlanChangeRepository : RepositoryBase<VisitInsurancePlanChange,CdiEtl>, IVisitInsurancePlanChangeRepository
    {
        public VisitInsurancePlanChangeRepository(ISessionContext<CdiEtl> sessionContext)
            : base(sessionContext)
        {
        }
    }
}
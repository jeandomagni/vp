﻿namespace Ivh.Provider.HospitalData.Visit
{
    using System.Collections.Generic;
    using Common.Data;
    using Common.Data.Connection.Connections;
    using Common.Data.Interfaces;
    using Domain.HospitalData.Visit.Entities;
    using Domain.HospitalData.Visit.Interfaces;
    using NHibernate;

    public class RealTimeVisitEnrollmentRepository : RepositoryBase<RealTimeVisitEnrollment, CdiEtl>, IRealTimeVisitEnrollmentRepository
    {
        public RealTimeVisitEnrollmentRepository(ISessionContext<CdiEtl> sessionContext, IStatelessSessionContext<CdiEtl> statelessSessionContext)
            : base(sessionContext, statelessSessionContext)
        {
        }

        public bool PendingRealtimeEnrollmentExists(int visitId, int billingSystemId, int hsGuarantorId)
        {
            return this.Session.QueryOver<RealTimeVisitEnrollment>()
                .Where(x => x.VisitId == visitId && x.BillingSystemId == billingSystemId && x.HsGuarantorId == hsGuarantorId && x.OverrideExpireDate == null)
                .List()
                .Count > 0;
        }
    }
}
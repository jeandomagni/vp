﻿namespace Ivh.Provider.HospitalData.Visit
{
    using System.Collections.Generic;
    using Common.Data;
    using Common.Data.Connection.Connections;
    using Common.Data.Interfaces;
    using Domain.HospitalData.Visit.Entities;
    using Domain.HospitalData.Visit.Interfaces;
    using NHibernate;

    public class InsurancePlanRepository : RepositoryBase<InsurancePlan, CdiEtl>, IInsurancePlanRepository
    {
        public InsurancePlanRepository(ISessionContext<CdiEtl> sessionContext)
            : base(sessionContext)
        {
        }

        public IList<InsurancePlan> GetAllInsurancePlan()
        {
            return this.Session.QueryOver<InsurancePlan>()
                .OrderBy(vs => vs.InsurancePlanId).Asc
                .Cacheable()
                .CacheMode(CacheMode.Normal)
                .List();
        }
    }
}

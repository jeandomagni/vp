﻿namespace Ivh.Provider.HospitalData.Visit
{
    using Common.Data;
    using Common.Data.Connection.Connections;
    using Common.Data.Interfaces;
    using Domain.HospitalData.Visit.Entities;
    using Domain.HospitalData.Visit.Interfaces;
    using NHibernate;
    public class VisitTransactionChangeRepository : RepositoryBase<VisitTransactionChange, CdiEtl>, IVisitTransactionChangeRepository
    {
        public VisitTransactionChangeRepository(ISessionContext<CdiEtl> sessionContext)
            : base(sessionContext)
        {
        }
    }
}

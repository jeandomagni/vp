﻿namespace Ivh.Provider.HospitalData.Enums
{
    public enum LifeCycleStageEnum
    {
        FullSelfPayDue = 6,
        PaymentPlan = 7,
        PrimaryBadDebt = 8,
        SecondaryBadDebt = 9
    }
}
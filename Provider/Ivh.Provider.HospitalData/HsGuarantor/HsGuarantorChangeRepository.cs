﻿namespace Ivh.Provider.HospitalData.HsGuarantor
{
    using Common.Data;
    using Common.Data.Connection.Connections;
    using Common.Data.Interfaces;
    using Domain.HospitalData.HsGuarantor.Entities;
    using Domain.HospitalData.HsGuarantor.Interfaces;
    using NHibernate;

    public class HsGuarantorChangeRepository : RepositoryBase<HsGuarantorChange, CdiEtl>, IHsGuarantorChangeRepository
    {
        public HsGuarantorChangeRepository(ISessionContext<CdiEtl> sessionContext)
            : base(sessionContext)
        {
        }
    }
}
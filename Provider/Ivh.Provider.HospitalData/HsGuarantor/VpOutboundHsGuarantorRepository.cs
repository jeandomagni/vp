﻿namespace Ivh.Provider.HospitalData.HsGuarantor
{
    using System.Collections.Generic;
    using System.Linq;
    using Common.Data;
    using Common.Data.Connection.Connections;
    using Common.Data.Interfaces;
    using Domain.HospitalData.HsGuarantor.Entities;
    using Domain.HospitalData.HsGuarantor.Interfaces;
    using NHibernate;

    public class VpOutboundHsGuarantorRepository : RepositoryBase<VpOutboundHsGuarantor, CdiEtl>, IVpOutboundHsGuarantorRepository
    {
        public VpOutboundHsGuarantorRepository(ISessionContext<CdiEtl> sessionContext)
            : base(sessionContext)
        {
        }

        public IReadOnlyList<VpOutboundHsGuarantor> GetOutboundEvents(int hsGuarantorId)
        {
            return this.GetQueryable().Where(i => i.HsGuarantorId == hsGuarantorId).ToList();
        }
    }
}
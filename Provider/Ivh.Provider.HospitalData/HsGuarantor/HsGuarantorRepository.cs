﻿namespace Ivh.Provider.HospitalData.HsGuarantor
{
    using System.Collections.Generic;
    using Common.Data;
    using Domain.HospitalData.HsGuarantor.Entities;
    using Domain.HospitalData.HsGuarantor.Interfaces;
    using NHibernate;
    using NHibernate.Criterion;
    using NHibernate.Transform;
    using System;
    using Common.Cache;
    using Common.Data.Connection.Connections;
    using Common.Data.Interfaces;
    using Ivh.Common.Base.Enums;

    public class HsGuarantorRepository : RepositoryCacheBase<HsGuarantor, CdiEtl>, IHsGuarantorRepository
    {
        public HsGuarantorRepository(ISessionContext<CdiEtl> sessionContext, Lazy<IDistributedCache> cache)
            : base(sessionContext, cache)
        {
        }

        public IReadOnlyList<HsGuarantorSearchResult> GetGuarantors(HsGuarantorFilter filter)
        {
            IQuery query = this.Session.CreateSQLQuery("EXEC base.HsGuarantor_search @vpRegistered =:VpRegistered,@vpUnregistered =:VpUnregistered,@vpGuarantorID =:VpGuarantorID,@sourceSystemKey =:SourceSystemKey,@lastName =:LastName");

            query.SetParameter("SourceSystemKey", filter.HsSourceKey);
            query.SetParameter("LastName", filter.LastName);
            query.SetParameter("VpGuarantorID", filter.VPGID);
            query.SetParameter("VpRegistered", filter.VpRegistered);
            query.SetParameter("VpUnregistered", filter.VpUnregistered);

            query.SetReadOnly(true);
            query.SetResultTransformer(new AliasToBeanResultTransformer(typeof(HsGuarantorSearchResult)));

            return (IReadOnlyList<HsGuarantorSearchResult>)query.List<HsGuarantorSearchResult>();
        }

        public HsGuarantor GetHsGuarantor(int billingSystemId, string sourceSystemKey)
        {
            return this.GetCached(
                x => x.HsBillingSystemId, billingSystemId,
                x => x.SourceSystemKey, sourceSystemKey,
                (b, s) =>
                {
                    ICriteria hsGuarantorQuery = this.Session.CreateCriteria<HsGuarantor>();
                    hsGuarantorQuery.Add(Restrictions.Eq("HsBillingSystemId", b));
                    hsGuarantorQuery.Add(Restrictions.Eq("SourceSystemKey", s));

                    return hsGuarantorQuery.UniqueResult<HsGuarantor>();
                });
        }

        public IList<string> GetEligibleVisitsAfterEnrollment(string hsGuarantorSourceSystemKey, int billingSystem)
        {
            IQuery query = this.Session.CreateSQLQuery("EXEC base.VisitSourceSystemKeyGetByGuarantorSourceSystemKey @GuarantorSourceSystemKey =:guarantorSourceSystemKey,@billingSystemId =:billingSystemId");

            query.SetParameter("guarantorSourceSystemKey", hsGuarantorSourceSystemKey);
            query.SetParameter("billingSystemId", billingSystem);
            query.SetReadOnly(true);

            return query.List<string>();
        }

        public IReadOnlyList<HsGuarantor> GetHsGuarantors(int[] hsGuarantorIds, string sourceSystemKey, string firstName, string lastName, string ssn4, string dob)
        {
            ICriteria hsGuarantorQuery = this.Session.CreateCriteria<HsGuarantor>();
            if (hsGuarantorIds != null && hsGuarantorIds.Length > 0)
            {
                hsGuarantorQuery.Add(Restrictions.In("HsGuarantorId", hsGuarantorIds));
            }
            if (!string.IsNullOrWhiteSpace(sourceSystemKey))
            {
                hsGuarantorQuery.Add(Restrictions.Eq("SourceSystemKey", sourceSystemKey));
            }
            if (!string.IsNullOrWhiteSpace(firstName))
            {
                hsGuarantorQuery.Add(Restrictions.Eq("FirstName", firstName));
            }
            if (!string.IsNullOrWhiteSpace(lastName))
            {
                hsGuarantorQuery.Add(Restrictions.Eq("LastName", lastName));
            }
            if (!string.IsNullOrWhiteSpace(ssn4))
            {
                hsGuarantorQuery.Add(Restrictions.Eq("SSN4", ssn4));
            }
            DateTime dobDateTime;
            DateTime.TryParse(dob, out dobDateTime);
            if (!string.IsNullOrWhiteSpace(dob))
            {
                hsGuarantorQuery.Add(Restrictions.Eq("DOB", dobDateTime));
            }

            return (IReadOnlyList<HsGuarantor>)hsGuarantorQuery.List<HsGuarantor>();
        }

        protected override int GetId(HsGuarantor entity)
        {
            return entity.HsGuarantorId;
        }
    }
}
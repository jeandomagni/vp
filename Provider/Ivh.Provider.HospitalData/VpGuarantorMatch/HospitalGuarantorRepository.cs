﻿namespace Ivh.Provider.HospitalData.VpGuarantorMatch
{
    using System;
    using System.Linq;
    using Application.HospitalData.Common.Responses;
    using Common.Base.Enums;
    using Common.Data;
    using Common.Data.Connection.Connections;
    using Common.Data.Interfaces;
    using Common.VisitPay.Enums;
    using Domain.HospitalData.Visit.Entities;
    using Domain.HospitalData.VpGuarantor.Entities;
    using Domain.HospitalData.VpGuarantor.Interfaces;
    using Domain.HospitalData.VpGuarantor.Mappings;
    using NHibernate;
    using NHibernate.Criterion;

    public class HospitalGuarantorRepository : RepositoryBase<VpGuarantorMatchInfo, CdiEtl>, IHospitalGuarantorRepository
    {
        public HospitalGuarantorRepository(ISessionContext<CdiEtl> sessionContext)
            : base(sessionContext)
        {
        }

        public GuarantorMatchResponse GetInitialMatchExpress(VpGuarantorMatchInfo vpGuarantorMatchInfo)
        {
            IQuery sqlQuery = this.Session.CreateSQLQuery(@"exec [Vp].[VpGuarantor_getInitalMatchExpress] :HsBillingSystemID, :RegistrationMatchString, :LastName ")
                .SetParameter("HsBillingSystemID", vpGuarantorMatchInfo.HsBillingSystemID)
                .SetParameter("RegistrationMatchString", vpGuarantorMatchInfo.RegistrationMatchString)
                .SetParameter("LastName", vpGuarantorMatchInfo.LastName)
                .SetResultTransformer(new GuarantorMatchResponseTransformer());

            return sqlQuery.UniqueResult<GuarantorMatchResponse>();
        }

        public GuarantorMatchResponse GetInitialMatch(VpGuarantorMatchInfo vpGuarantorMatchInfo)
        {
            IQuery sqlQuery = this.Session.CreateSQLQuery(@"exec [Vp].[VpGuarantor_getInitalMatch] :HsBillingSystemID, :RegistrationMatchString, :LastName, :SSN4, :DOB, :PostalCodeShort, :PatientDOB")
                .SetParameter("HsBillingSystemID", vpGuarantorMatchInfo.HsBillingSystemID)
                .SetParameter("RegistrationMatchString", vpGuarantorMatchInfo.RegistrationMatchString)
                .SetParameter("LastName", vpGuarantorMatchInfo.LastName)
                .SetParameter("SSN4", vpGuarantorMatchInfo.SSN4)
                .SetParameter("DOB", vpGuarantorMatchInfo.DOB)
                .SetParameter("PostalCodeShort", vpGuarantorMatchInfo.PostalCodeShort)
                .SetParameter("PatientDOB", vpGuarantorMatchInfo.PatientDOB)
                .SetResultTransformer(new GuarantorMatchResponseTransformer());

            return sqlQuery.UniqueResult<GuarantorMatchResponse>();
        }

        public void AddInitialMatch(VpGuarantorMatchInfo vpGuarantorMatchInfo)
        {
            this.Session.CreateSQLQuery(@"exec [Vp].[VpGuarantor_addInitalMatch] :VpGuarantorId, :HsGuarantorID, :HsBillingSystemID, :RegistrationMatchString, :LastName, :SSN4, :DOB, :PostalCodeShort, :PatientDOB")
                .SetParameter("VpGuarantorId", vpGuarantorMatchInfo.VpGuarantorId)
                .SetParameter("HsGuarantorID", vpGuarantorMatchInfo.HsGuarantorId)
                .SetParameter("HsBillingSystemID", vpGuarantorMatchInfo.HsBillingSystemID)
                .SetParameter("RegistrationMatchString", vpGuarantorMatchInfo.RegistrationMatchString)
                .SetParameter("LastName", vpGuarantorMatchInfo.LastName)
                .SetParameter("SSN4", vpGuarantorMatchInfo.SSN4)
                .SetParameter("DOB", vpGuarantorMatchInfo.DOB)
                .SetParameter("PostalCodeShort", vpGuarantorMatchInfo.PostalCodeShort)
                .SetParameter("PatientDOB", vpGuarantorMatchInfo.PatientDOB)
                .ExecuteUpdate();
        }

        public void AddInitialMatchVpcc(VpGuarantorMatchInfo vpGuarantorMatchInfo)
        {
            this.Session.CreateSQLQuery(@"exec [Vp].[VpGuarantor_addInitalMatch] :VpGuarantorId, :HsGuarantorID, :HsBillingSystemID, :RegistrationMatchString, :LastName, :SSN4, :DOB, :PostalCodeShort,:PatientDOB, :HsGuarantorMatchStatusId")
                .SetParameter("VpGuarantorId", vpGuarantorMatchInfo.VpGuarantorId)
                .SetParameter("HsGuarantorID", vpGuarantorMatchInfo.HsGuarantorId)
                .SetParameter("HsBillingSystemID", vpGuarantorMatchInfo.HsBillingSystemID)
                .SetParameter("RegistrationMatchString", vpGuarantorMatchInfo.RegistrationMatchString)
                .SetParameter("LastName", vpGuarantorMatchInfo.LastName)
                .SetParameter("SSN4", vpGuarantorMatchInfo.SSN4)
                .SetParameter("DOB", vpGuarantorMatchInfo.DOB)
                .SetParameter("PostalCodeShort", vpGuarantorMatchInfo.PostalCodeShort)
                .SetParameter("PatientDOB", vpGuarantorMatchInfo.PatientDOB)
                .SetParameter("HsGuarantorMatchStatusId", HsGuarantorMatchStatusEnum.VpccPending)
                .ExecuteUpdate();
        }

        public void UpdateMatch(VpGuarantorMatchInfo vpGuarantorMatchInfo)
        {
            this.Session.CreateSQLQuery(@"exec [Vp].[VpGuarantor_updateMatchInfo] :VpGuarantorId, :LastName, :PostalCodeShort")
                .SetParameter("VpGuarantorId", vpGuarantorMatchInfo.VpGuarantorId)
                .SetParameter("LastName", vpGuarantorMatchInfo.LastName)
                .SetParameter("PostalCodeShort", vpGuarantorMatchInfo.PostalCodeShort)
                .ExecuteUpdate();
        }

        public bool CredentialsMatchCanceledAccount(string lastName, DateTime dob, string ssn4, string postalCodeShort)
        {
            ICriteria criteria = this.Session.CreateCriteria<VpGuarantorMatchInfo>()
                .Add(Restrictions.Eq("LastName", lastName))
                .Add(Restrictions.Eq("DOB", dob))
                .Add(Restrictions.Eq("SSN4", ssn4))
                .Add(Restrictions.Eq("PostalCodeShort", postalCodeShort))
                .Add(Restrictions.IsNotNull("CancelledDate"));

            return (criteria.List<VpGuarantorMatchInfo>()).Any();
        }

        public void GetNewMatches(int vpGuarantorId)
        {
            this.Session.CreateSQLQuery(@"exec [Vp].[VpGuarantor_getNewMatches] :VpGuarantorId ")
                .SetParameter("VpGuarantorId", vpGuarantorId)
                .ExecuteUpdate();
        }
    }
}
﻿namespace Ivh.Provider.HospitalData.VpGuarantorMatch
{
    using System.Collections.Generic;
    using System.Linq;
    using Common.Base.Enums;
    using Common.Base.Utilities.Extensions;
    using Common.Data;
    using Common.Data.Connection.Connections;
    using Common.Data.Interfaces;
    using Common.VisitPay.Enums;
    using Domain.HospitalData.HsGuarantor.Entities;
    using Domain.HospitalData.HsGuarantor.Interfaces;
    using Domain.HospitalData.VpGuarantor.Entities;
    using NHibernate;
    using NHibernate.Criterion;

    public class VpGuarantorHsMatchRepository : RepositoryBase<VpGuarantorHsMatch, CdiEtl>, IVpGuarantorHsMatchRepository
    {
        public VpGuarantorHsMatchRepository(ISessionContext<CdiEtl> sessionContext)
            : base(sessionContext)
        {
        }

        public IList<VpGuarantorHsMatch> GetVpGuarantorHsMatches(int hsGuarantorId, int vpGuarantorId)
        {
            return this.GetQueryable().Where(i => i.HsGuarantorId == hsGuarantorId).Where(i => i.VpGuarantorId == vpGuarantorId).ToList();
        }

        public IList<VpGuarantorHsMatch> GetVpGuarantorHsMatches(int hsGuarantorId)
        {
            return this.GetQueryable().Where(i => i.HsGuarantorId == hsGuarantorId).ToList();
        }

        public IList<VpGuarantorHsMatch> GetHsGuarantorVpMatches(int vpGuarantorId)
        {
            return this.GetQueryable().Where(i => i.VpGuarantorId == vpGuarantorId).ToList();
        }

        public IList<VpGuarantorHsMatch> GetById(int hsGuarantorId, int vpGuarantorId, IList<HsGuarantorMatchStatusEnum> hsGuarantorMatchStatuses = null)
        {
            IQueryOver<VpGuarantorHsMatch, VpGuarantorHsMatch> criteria = this.Session.QueryOver<VpGuarantorHsMatch>();
            criteria.Where(x => x.HsGuarantorId == hsGuarantorId);
            criteria.And(x => x.VpGuarantorId == vpGuarantorId);

            if (hsGuarantorMatchStatuses.IsNotNullOrEmpty())
            {
                return criteria.List().Where(x => hsGuarantorMatchStatuses.Contains(x.HsGuarantorMatchStatus)).ToList();
            }

            return criteria.List();
        }

        public IList<HsGuarantor> GetGuarantorsByVpGuarantorId(int vpGuarantorId, bool activeMatchesOnly = false)
        {
            DetachedCriteria vpGuarantorHsMatch = DetachedCriteria.For<VpGuarantorHsMatch>("match")
                .SetProjection(Projections.Property("match.HsGuarantorId"))
                .Add(Restrictions.Eq("match.VpGuarantorId", vpGuarantorId));
            if (activeMatchesOnly)
            {
                vpGuarantorHsMatch.Add(Restrictions.Eq("match.HsGuarantorMatchStatus", HsGuarantorMatchStatusEnum.Matched));
            }

            IList<HsGuarantor> list = this.Session.CreateCriteria<HsGuarantor>()
                .Add(Subqueries.PropertyIn("HsGuarantorId", vpGuarantorHsMatch))
                .List<HsGuarantor>();

            return list.ToList();
        }
    }
}

﻿namespace Ivh.Provider.HospitalData.VpGuarantorMatch
{
    using Common.Data;
    using Common.Data.Connection;
    using Common.Data.Connection.Connections;
    using Common.Data.Interfaces;
    using Domain.HospitalData.VpGuarantor.Entities;
    using Domain.HospitalData.VpGuarantor.Interfaces;
    using NHibernate;

    public class GuarantorMatchingLogRepository : RepositoryBase<GuarantorMatchingLog, Logging>, IGuarantorMatchingLogRepository
    {
        public GuarantorMatchingLogRepository(ISessionContext<Logging> sessionContext) : base(sessionContext)
        {
        }
    }
}
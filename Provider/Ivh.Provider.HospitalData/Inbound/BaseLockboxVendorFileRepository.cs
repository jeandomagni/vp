﻿namespace Ivh.Provider.HospitalData.Inbound
{
    using Common.Data;
    using Common.Data.Connection.Connections;
    using Common.Data.Interfaces;
    using Domain.HospitalData.Inbound.Entities;
    using Domain.HospitalData.Inbound.Interfaces;

    public class BaseLockboxVendorFileRepository : RepositoryBase<BaseLockboxVendorPayment, CdiEtl>, IBaseLockboxVendorFileRepository
    {
        public BaseLockboxVendorFileRepository(ISessionContext<CdiEtl> sessionContext) : base(sessionContext)
        {
        }
    }
}
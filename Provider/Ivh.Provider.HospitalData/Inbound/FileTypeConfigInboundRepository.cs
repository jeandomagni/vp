﻿namespace Ivh.Provider.HospitalData.Inbound
{
    using System.Collections.Generic;
    using System.Linq;
    using Common.Data;
    using Common.Data.Connection.Connections;
    using Common.Data.Interfaces;
    using Domain.HospitalData.Inbound.Entities;
    using Domain.HospitalData.Inbound.Interfaces;

    public class FileTypeConfigInboundRepository : RepositoryBase<FileTypeConfigInbound, CdiEtl>, IFileTypeConfigInboundRepository
    {
        public FileTypeConfigInboundRepository(ISessionContext<CdiEtl> sessionContext) : base(sessionContext)
        {
        }

        public IList<FileTypeConfigInbound> GetFileTypeConfigInbounds()
        {
            return this.Session.Query<FileTypeConfigInbound>()
                .Where(x => x.FileTypeProvider != null && x.FileTypeProvider != "")
                .Where(x => x.IsActive)
                .ToList();
        }
    }
}
﻿namespace Ivh.Provider.HospitalData.DataLoad
{
    using Common.Data;
    using Common.Data.Connection.Connections;
    using Common.Data.Interfaces;
    using Domain.HospitalData.Change.Entities;
    using Domain.HospitalData.Change.Interfaces;

    public class DataLoadRepository : RepositoryBase<DataLoad, CdiEtl>, IDataLoadRepository
    {
        public DataLoadRepository(ISessionContext<CdiEtl> sessionContext)
            : base(sessionContext)
        {
        }

    }

}

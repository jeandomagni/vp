﻿namespace Ivh.Provider.HospitalData.Change
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Common.Base.Utilities.Extensions;
    using Common.Data;
    using Common.Data.Connection.Connections;
    using Common.Data.Interfaces;
    using Common.VisitPay.Enums;
    using Domain.HospitalData.Change.Entities;
    using Domain.HospitalData.Change.Interfaces;
    using Domain.HospitalData.HsGuarantor.Entities;
    using Domain.HospitalData.Visit.Entities;
    using Domain.HospitalData.Visit.Interfaces;
    using Domain.HospitalData.VpGuarantor.Entities;
    using NHibernate;
    using NHibernate.Criterion;
    using NHibernate.Linq;
    using NHibernate.SqlCommand;

    public class ChangeEventRepository : RepositoryBase<ChangeEvent, CdiEtl>, IChangeEventRepository
    {
        private readonly Lazy<ITransactionCodeRepository> _transactionCodeRepository;

        public ChangeEventRepository(ISessionContext<CdiEtl> sessionContext,
            Lazy<ITransactionCodeRepository> transactionCodeRepository)
            : base(sessionContext)

        {
            this._transactionCodeRepository = transactionCodeRepository;
        }

        public IList<ChangeEvent> GetAllUnprocessedChangeEvents(int? batchSize = null)
        {
            ICriteria criteria = this.Session.CreateCriteria<ChangeEvent>("ce");
            criteria.CreateCriteria("ce.ChangeEventType", "ct", JoinType.InnerJoin);
            criteria.CreateCriteria("ct.Application", "a", JoinType.InnerJoin);
            criteria.Add(Restrictions.Eq("ce.ChangeEventStatus", ChangeEventStatusEnum.Unprocessed));
            criteria.Add(Restrictions.Eq("a.ApplicationId", (int)ApplicationEnum.VisitPay));
            if (batchSize.HasValue)
            {
                criteria.SetMaxResults(batchSize.Value);
            }
            IList<ChangeEvent> changeEvents = criteria.List<ChangeEvent>();
            return changeEvents.ToList();
        }

        public IList<ChangeEventType> GetAllChangeEventTypes()
        {
            return this.Session.Query<ChangeEventType>()
                .WithOptions(x =>
                {
                    x.SetCacheable(true);
                    x.SetCacheMode(CacheMode.Normal);
                })
                .ToList();
        }

        public override ChangeEvent GetById(int id)
        {
            return this.Session.Get<ChangeEvent>(id);
        }

        public void BulkUpdateIneligibleToScoreChangeEvents(int scoringBatchId)
        {
            this.Session.CreateSQLQuery(@"exec [scoring].[BulkUpdateIneligibleToScoreChangeEvents] :ScoringBatchId")
                .SetParameter("ScoringBatchId", scoringBatchId)
                .ExecuteUpdate();

        }

        public IList<VpGuarantorHsMatch> GetAllVpGuarantorHsMatchesByChangeEvent(ChangeEvent changeEvent, IList<HsGuarantorMatchStatusEnum> hsGuarantorMatchStatuses = null)
        {
            List<VpGuarantorHsMatch> vpGuarantorHsMatch = (from v in this.Session.Query<VisitChange>()
                                                           join g in this.Session.Query<VpGuarantorHsMatch>()
                                                               on v.HsGuarantorId equals g.HsGuarantorId
                                                           where v.ChangeEventId == changeEvent.ChangeEventId
                                                           select g).Distinct().ToList();

            vpGuarantorHsMatch.AddRange((from v in this.Session.Query<HsGuarantorChange>()
                                         join g in this.Session.Query<VpGuarantorHsMatch>()
                                             on v.HsGuarantorId equals g.HsGuarantorId
                                         where v.ChangeEventId == changeEvent.ChangeEventId
                                         select g).Distinct().ToList());

            if (hsGuarantorMatchStatuses != null && hsGuarantorMatchStatuses.Any())
            {
                vpGuarantorHsMatch = vpGuarantorHsMatch.Where(x => hsGuarantorMatchStatuses.Contains(x.HsGuarantorMatchStatus)).ToList();
            }

            return vpGuarantorHsMatch;
        }

        public IList<HsGuarantor> GetHsGuarantorsByChangeEvent(int changeEventId)
        {
            IList<HsGuarantor> changeGuarantors = (from v in this.Session.Query<VisitChange>()
                                                   join g in this.Session.Query<HsGuarantor>()
                                                       on v.HsGuarantorId equals g.HsGuarantorId
                                                   where v.ChangeEventId == changeEventId
                                                   select g).Distinct().ToList();

            changeGuarantors.AddRange((from v in this.Session.Query<HsGuarantorChange>()
                                       join g in this.Session.Query<HsGuarantor>()
                                           on v.HsGuarantorId equals g.HsGuarantorId
                                       where v.ChangeEventId == changeEventId
                                       select g).Distinct().ToList());

            return changeGuarantors.GroupBy(x => x.HsGuarantorId).Select(x => x.First()).ToList();
        }

        public IList<VisitTransactionChange> GetVisitTransactionChangesForChangeEvent(int changeEventId)
        {
            IList<VisitTransactionChange> visitTransactionChanges = this.Session.QueryOver<VisitTransactionChange>()
                .Where(x => x.ChangeEventId == changeEventId)
                .List();
            return this.SupplementTransactionCodes(visitTransactionChanges);
        }

        private IList<VisitTransactionChange> SupplementTransactionCodes(IList<VisitTransactionChange> visitTransactionChanges)
        {
            if (visitTransactionChanges.IsNullOrEmpty())
            {
                return new List<VisitTransactionChange>();
            }

            foreach (VisitTransactionChange visitTransactionChange in visitTransactionChanges)
            {
                TransactionCode transactionCode = this._transactionCodeRepository.Value.GetById(visitTransactionChange.TransactionCodeId);
                if (transactionCode != null)
                {
                    visitTransactionChange.TransactionCodeBillingSystemId = transactionCode.BillingSystemId;
                    visitTransactionChange.TransactionCodeSourceSystemKey = transactionCode.SourceSystemKey;
                }
            }
            return visitTransactionChanges;
        }

        public VisitChange GetVisitChangeForChangeEvent(int changeEventId)
        {
            VisitChange visitChange = this.Session.QueryOver<VisitChange>()
                .Where(x => x.ChangeEventId == changeEventId)
                .Take(1)
                .SingleOrDefault();

            // nHibernate doesn't seem function correctly when joining on a composite key and no records are returned.
            // so do the subqueries ourself.
            if (visitChange != null)
            {
                visitChange.VisitTransactions = this.Session.QueryOver<VisitTransactionChange>()
                    .Where(x => x.ChangeEventId == changeEventId)
                    .And(x => x.VisitId == visitChange.VisitId)
                    .List();

                visitChange.VisitInsurancePlans = this.Session.QueryOver<VisitInsurancePlanChange>()
                    .Where(x => x.ChangeEventId == changeEventId)
                    .And(x => x.VisitId == visitChange.VisitId)
                    .List();

                visitChange.VisitPaymentAllocations = this.Session.QueryOver<VisitPaymentAllocationChange>()
                    .Where(x => x.ChangeEventId == changeEventId)
                    .And(x => x.VisitId == visitChange.VisitId)
                    .List();
            }

            return visitChange;
        }

        public HsGuarantorChange GetHsGuarantorChangeForChangeEvent(int changeEventId)
        {
            HsGuarantorChange hsGuarantorChange = this.Session.QueryOver<HsGuarantorChange>()
                .Where(x => x.ChangeEventId == changeEventId)
                .Take(1)
                .SingleOrDefault();
            return hsGuarantorChange;
        }

        public IList<ChangeEvent> GetChangeEventsByVisit(int visitId)
        {
            return this.Session.QueryOver<ChangeEvent>()
                .Where(x => x.VisitId == visitId)
                .List();
        }

        public IReadOnlyList<VpGuarantorHsMatch> GetAllMatchesByVpGuarantor(int vpGuarantorId)
        {
            return (IReadOnlyList<VpGuarantorHsMatch>)this.Session.CreateCriteria<VpGuarantorHsMatch>()
                .Add(Restrictions.Eq("VpGuarantorId", vpGuarantorId))
                .List<VpGuarantorHsMatch>();
        }

    }
}
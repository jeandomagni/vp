﻿namespace Ivh.Provider.HospitalData.Scoring
{
    using Common.Data;
    using Common.Data.Connection.Connections;
    using Common.Data.Interfaces;
    using Domain.HospitalData.Scoring.Entities;
    using Domain.HospitalData.Scoring.Interfaces;
    using NHibernate;

    public class ScoringSagaLogRepository : RepositoryBase<ScoringSagaLog, CdiEtl>, ISagaLogRepository
    {
        public ScoringSagaLogRepository(ISessionContext<CdiEtl> sessionContext) : base(sessionContext)
        {
        }

    }
}
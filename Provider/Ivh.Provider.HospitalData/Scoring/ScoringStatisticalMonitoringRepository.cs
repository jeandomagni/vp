﻿namespace Ivh.Provider.HospitalData.Scoring
{
    using Common.Data;
    using Common.Data.Connection.Connections;
    using Common.Data.Interfaces;
    using Domain.HospitalData.Scoring.Entities;
    using Domain.HospitalData.Scoring.Interfaces;
    using NHibernate;

    public class ScoringStatisticalMonitoringRepository : RepositoryBase<ScoringStatisticalMonitoring, CdiEtl>, IScoringStatisticalMonitoringRepository
    {
        public ScoringStatisticalMonitoringRepository(ISessionContext<CdiEtl> sessionContext) : base(sessionContext)
        {
        }
    }
}
﻿namespace Ivh.Provider.HospitalData.Scoring
{
    using System;
    using System.Collections.Generic;
    using Common.Data;
    using Common.Data.Connection.Connections;
    using Common.Data.Interfaces;
    using Domain.HospitalData.Scoring.Entities;
    using Domain.HospitalData.Segmentation.Interfaces;
    using NHibernate;
    using NHibernate.Criterion;

    public class ScoringBatchRepository : RepositoryBase<ScoringBatch, CdiEtl>, IScoringBatchRepository
    {
        public ScoringBatchRepository(ISessionContext<CdiEtl> sessionContext) : base(sessionContext)
        {
        }

        public int GenerateScoringBatchId()
        {
            ScoringBatch scoringBatch = new ScoringBatch { BatchStartDateTime = DateTime.UtcNow };
            base.InsertOrUpdate(scoringBatch);
            return scoringBatch.ScoringBatchId;
        }

        public ScoringBatch GetLatestScoringBatch()
        {
            return this.Session.QueryOver<ScoringBatch>()
                .OrderBy(x => x.BatchStartDateTime).Desc
                .Take(1)
                .SingleOrDefault();
        }

        public IList<ScoringBatch> GetScoringBatchesForDays(int numberOfDaysBeforeToday)
        {
            DateTime daysAgo = DateTime.UtcNow.AddDays(numberOfDaysBeforeToday * -1).Date;
            ICriteria criteria = this.Session.CreateCriteria(typeof(ScoringBatch))
                .Add(Restrictions.Gt("BatchStartDateTime", daysAgo));
            return criteria.List<ScoringBatch>();
        }
    }
}
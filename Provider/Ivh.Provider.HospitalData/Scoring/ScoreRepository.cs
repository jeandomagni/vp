﻿namespace Ivh.Provider.HospitalData.Scoring
{
    using System.Collections.Generic;
    using System.Linq;
    using Common.Data;
    using Common.Data.Connection.Connections;
    using Common.Data.Interfaces;
    using Common.VisitPay.Enums;
    using Domain.HospitalData.HsGuarantor.Entities;
    using Domain.HospitalData.Scoring.Entities;
    using Domain.HospitalData.Scoring.Interfaces;

    public class ScoreRepository : RepositoryBase<Score, CdiEtl>, IScoreRepository
    {
        public ScoreRepository(ISessionContext<CdiEtl> sessionContext) : base(sessionContext) { }

        public IList<PtpScore> GetPtpScore(Dictionary<string, List<int>> hsGuarantors)
        {
            // Fetch query results using most restrictive value (SystemSourceKey)
            // This may contain entries that do not match the BillingSystemId that belongs to the SystemSourceKey provided in the arguments
            var potentialScores = (from s in this.Session.Query<Score>()
                                   join g in this.Session.Query<HsGuarantor>() on s.ParentHsGuarantorId equals g.HsGuarantorId
                                   where hsGuarantors.Keys.Contains(g.SourceSystemKey)
                                   select new { g.HsGuarantorId, g.SourceSystemKey, g.HsBillingSystemId, s.PtpScore, s.ScoringBatchId, s.ScoreId, s.ScoreTypeId, s.Phase2ScriptVersion, s.ProtoScore.ProtoScore2 }).ToList();

            // reduce the results (in memory) to rows that match SystemSourceKey AND HsBillingSystemId provided from the arguments
            // select the latest row
            var latestScore = (
                from s in potentialScores
                from h in hsGuarantors
                where s.SourceSystemKey == h.Key && (h.Value ?? new List<int>()).Contains(s.HsBillingSystemId)
                orderby s.ScoringBatchId descending
                select s
            ).FirstOrDefault();

            List<PtpScore> scores = new List<PtpScore>();
            PtpScore ptpScore = new PtpScore
            {
                ScoreId = latestScore?.ScoreId ?? 0,
                Score = latestScore?.PtpScore ?? 0,
                ScoreTypeId = latestScore?.ScoreTypeId ?? (int)ScoreTypeEnum.PtpOriginal,
                Version = latestScore?.Phase2ScriptVersion
            };
            scores.Add(ptpScore);

            PtpScore protoScore = new PtpScore
            {
                ScoreId = latestScore?.ScoreId ?? 0,
                Score = latestScore?.ProtoScore2 ?? 0,
                ScoreTypeId = (int)ScoreTypeEnum.ProtoScore,
                Version = latestScore?.Phase2ScriptVersion
            };
            scores.Add(protoScore);

            return scores;
        }
    }
}
﻿namespace Ivh.Provider.HospitalData.Scoring
{
    using System;
    using System.Linq;
    using Common.Data;
    using Common.Data.Connection.Connections;
    using Common.Data.Interfaces;
    using Domain.HospitalData.Scoring.Entities;
    using Domain.HospitalData.Scoring.Interfaces;

    public class ScoringAndSegmentationSagaRepository : RepositoryBase<ScoringAndSegmentationSaga, CdiEtl>, IScoringAndSegmentationSagaRepository
    {
        public ScoringAndSegmentationSagaRepository(ISessionContext<CdiEtl> sessionContext) : base(sessionContext)
        {
        }

        public ScoringAndSegmentationSaga GetScoringAndSegmentationSaga(Guid correlationId)
        {
            ScoringAndSegmentationSaga scoringAndSegmentationSaga = this.GetQueryable().FirstOrDefault(x => x.CorrelationId == correlationId);

            return scoringAndSegmentationSaga;
        }


    }
}
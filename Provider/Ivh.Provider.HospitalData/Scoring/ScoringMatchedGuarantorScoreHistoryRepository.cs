﻿namespace Ivh.Provider.HospitalData.Scoring
{
    using Common.Data;
    using Common.Data.Connection.Connections;
    using Common.Data.Interfaces;
    using Domain.HospitalData.Scoring.Entities;
    using Domain.HospitalData.Scoring.Interfaces;
    using NHibernate;

    public class ScoringMatchedGuarantorScoreHistoryRepository : RepositoryBase<MatchedGuarantorScoreHistory, CdiEtl>, IScoringMatchedGuarantorScoreHistoryRepository
    {
        public ScoringMatchedGuarantorScoreHistoryRepository(ISessionContext<CdiEtl> sessionContext) : base(sessionContext)
        {
        }

        public void GenerateMatchedGuarantorScoreHistory(int scoringBatchId)
        {
            this.Session.CreateSQLQuery(@"exec [scoring].[GenerateMatchedGuarantorScoreHistory] :ScoringBatchId")
                .SetParameter("ScoringBatchId", scoringBatchId)
                .ExecuteUpdate();
        }
    }
}
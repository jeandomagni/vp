﻿namespace Ivh.Provider.HospitalData.Scoring
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using AutoMapper.QueryableExtensions;
    using Common.Data;
    using Common.Data.Connection.Connections;
    using Common.Data.Interfaces;
    using Common.VisitPay.Enums;
    using Domain.HospitalData.Scoring.Entities;
    using Domain.HospitalData.Scoring.Interfaces;
    using NHibernate;
    using NHibernate.Criterion;
    using NHibernate.SqlCommand;
    using NHibernate.Transform;

    public class ThirdPartyDataRepository : RepositoryBase<ThirdPartyData, CdiEtl>, IThirdPartyDataRepository
    {
        public ThirdPartyDataRepository(ISessionContext<CdiEtl> sessionContext, IStatelessSessionContext<CdiEtl> statelessSessionContext) : base(sessionContext, statelessSessionContext) { }

        public void InsertStateless(ThirdPartyData thirdPartyData)
        {
            this.StatelessSession.Insert(thirdPartyData);
        }
        public ThirdPartyData GetThirdPartyData(int hsGuarantorId, ScoringThirdPartyDataSetTypeEnum dataSetType, int refreshAge)
        {
            ScoringThirdPartyDatasetEnum dataSet = ScoringThirdPartyDatasetEnum.Scoring;
            if (dataSetType == ScoringThirdPartyDataSetTypeEnum.Segmentation)
            {
                dataSet = ScoringThirdPartyDatasetEnum.PresumptiveCharity;
            }
            ThirdPartyData thirdPartyData = this.GetQueryable().Where(x => x.CreatedDate.CompareTo(DateTime.UtcNow.AddDays((double)refreshAge * -1)) > 0 && x.ProbabilityScore > 0)
                .OrderByDescending(v => v.CreatedDate)
                .FirstOrDefault(x => x.HsGuarantorId == hsGuarantorId && (x.Dataset == 1 || x.Dataset == (int)dataSet));

            return thirdPartyData;
        }

        public ThirdPartyData GetThirdPartyDataByRequestInput(ThirdPartyRequestInput thirdPartyRequestInput, ScoringThirdPartyDataSetTypeEnum dataSetType, int refreshAge)
        {
            ScoringThirdPartyDatasetEnum dataSet = ScoringThirdPartyDatasetEnum.Scoring;
            if (dataSetType == ScoringThirdPartyDataSetTypeEnum.Segmentation)
            {
                dataSet = ScoringThirdPartyDatasetEnum.PresumptiveCharity;
            }
            ThirdPartyData thirdPartyData = this.GetQueryable().Where(x => x.CreatedDate.CompareTo(DateTime.UtcNow.AddDays((double) refreshAge * -1)) > 0 && x.ProbabilityScore > 0)
                .OrderByDescending(v => v.CreatedDate)
                .FirstOrDefault(x => thirdPartyRequestInput.RequestInputString == x.RequestInputString && (x.Dataset == 1 || x.Dataset == (int) dataSet));

            return thirdPartyData;
        }

        public IList<ThirdPartyDataMonitoring> GetThirdPartyDataByDate(DateTime date)
        {
            ICriteria query = this.Session.CreateCriteria<ThirdPartyData>("thirdParty")
                .CreateCriteria("thirdParty.HouseholdIncomeCodeToIncomeAmount", "hhIncome", JoinType.LeftOuterJoin)
                .Add(Restrictions.Eq(Projections.SqlFunction("date", NHibernateUtil.Date, Projections.Property("thirdParty.CreatedDate")), date.Date))
                .SetProjection(
                    Projections.ProjectionList()
                        .Add(Projections.Property("thirdParty.ProbabilityScore"), "ProbabilityScore")
                        .Add(Projections.Property("thirdParty.OwnRent"), "OwnRent")
                        .Add(Projections.Property("hhIncome.IncomeAmount"), "HouseholdIncome")
                        .Add(Projections.Property("thirdParty.HouseholdMemberCount"), "HouseholdMemberCount")
                )
                .SetResultTransformer(Transformers.AliasToBean(typeof(ThirdPartyDataMonitoring)));

            return query.List<ThirdPartyDataMonitoring>();
        }

        public IList<ThirdPartyDataMonitoring> GetThirdPartyDataByDates(DateTime minDate, DateTime maxDate)
        {
            ICriteria query = this.Session.CreateCriteria<ThirdPartyData>("thirdParty")
            .CreateCriteria("thirdParty.HouseholdIncomeCodeToIncomeAmount", "hhIncome", JoinType.LeftOuterJoin)
                .Add(Restrictions.Ge(Projections.SqlFunction("date", NHibernateUtil.Date, Projections.Property("thirdParty.CreatedDate")), minDate.Date))
                .Add(Restrictions.Le(Projections.SqlFunction("date", NHibernateUtil.Date, Projections.Property("thirdParty.CreatedDate")), maxDate.Date))
            .SetProjection(
                    Projections.ProjectionList()
                        .Add(Projections.Property("thirdParty.ProbabilityScore"), "ProbabilityScore")
                        .Add(Projections.Property("thirdParty.OwnRent"), "OwnRent")
                        .Add(Projections.Property("hhIncome.IncomeAmount"), "HouseholdIncome")
                        .Add(Projections.Property("thirdParty.HouseholdMemberCount"), "HouseholdMemberCount")
            )
            .SetResultTransformer(Transformers.AliasToBean(typeof(ThirdPartyDataMonitoring)));

            return query.List<ThirdPartyDataMonitoring>();
        }

        public IList<int> GetThirdPartyDataSet(int thirdPartyExpireDays, params ScoringThirdPartyDatasetEnum[] datasets)
        {
            DateTime expirationDate = DateTime.UtcNow.AddDays(thirdPartyExpireDays * -1);
            IQueryable<int> thirdPartyDataList = this.StatelessSession.Query<ThirdPartyData>().Where(t =>  datasets.Select(ds => (int?)ds).Contains(t.Dataset) && expirationDate.CompareTo(t.CreatedDate) < 0)
                .Select(c => c.HsGuarantorId);
            return thirdPartyDataList.ToList();
        }

    }
}

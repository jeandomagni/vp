﻿namespace Ivh.Provider.HospitalData.Scoring
{
    using Common.Data;
    using Common.Data.Connection.Connections;
    using Common.Data.Interfaces;
    using Domain.HospitalData.Scoring.Entities;
    using Domain.HospitalData.Scoring.Interfaces;
    using NHibernate;

    public class VisitBatchRepository : RepositoryBase<VisitBatch, CdiEtl>, IVisitBatchRepository
    {
        public VisitBatchRepository(ISessionContext<CdiEtl> sessionContext) : base(sessionContext)
        {
        }

    }
}
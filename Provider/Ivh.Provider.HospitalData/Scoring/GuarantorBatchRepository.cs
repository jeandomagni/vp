﻿namespace Ivh.Provider.HospitalData.Scoring
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Common.Data;
    using Common.Data.Connection.Connections;
    using Common.Data.Interfaces;
    using Domain.HospitalData.Scoring.Entities;
    using Domain.HospitalData.Scoring.Interfaces;

    public class GuarantorBatchRepository : RepositoryBase<GuarantorBatch, CdiEtl>, IGuarantorBatchRepository
    {
        public GuarantorBatchRepository(ISessionContext<CdiEtl> sessionContext, IStatelessSessionContext<CdiEtl> stateLessSessionContext) : base(sessionContext, stateLessSessionContext)
        {
        }

        public GuarantorBatch GetByGuarantorIdScoringBatchId(int hsGuarantorId, int scoringBatchId)
        {
            return this.StatelessSession.QueryOver<GuarantorBatch>()
                .Where(x => x.ChildHsGuarantorId == hsGuarantorId)
                .Where(x => x.ScoringBatchId == scoringBatchId).List().FirstOrDefault();
        }

        public IList<int> GetPresumptiveCharityGuarantorBatches(int thirdPartyExpireDays, bool evaluateVisitsWithoutPtpScores)
        {
            DateTime expirationDate = DateTime.UtcNow.AddDays(thirdPartyExpireDays * -1);
            return this.StatelessSession.CreateSQLQuery(@"EXEC scoring.GetPCEligibleGuarantorsWithoutThirdParty :ExpirationDate, :EvaluateVisitsWithoutPtpScores")
                .SetParameter("ExpirationDate", expirationDate)
                .SetParameter("EvaluateVisitsWithoutPtpScores", evaluateVisitsWithoutPtpScores)
                .List<int>()
                .ToList();
        }
    }
}
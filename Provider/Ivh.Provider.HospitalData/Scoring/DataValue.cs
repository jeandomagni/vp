﻿namespace Ivh.Provider.HospitalData.Scoring
{
    using System.Xml.Serialization;

    [XmlType(TypeName = "VALUE")]
    public class DataValue
    {
        [XmlAttribute("NAME")]
        public string Name { get; set; }

        [XmlText]
        public string Value { get; set; }
    }
}

﻿namespace Ivh.Provider.HospitalData.Scoring
{
    using System;
    using Common.Cache;
    using Common.Data;
    using Common.Data.Connection.Connections;
    using Common.Data.Interfaces;
    using Domain.HospitalData.Scoring.Entities;
    using Domain.HospitalData.Scoring.Interfaces;
    using NHibernate;

    public class HouseholdIncomeCodeToIncomeAmountMapRepository : RepositoryBase<HouseholdIncomeCodeToIncomeAmountMap, CdiEtl>, IHouseholdIncomeCodeToIncomeAmountMapRepository
    {
        public HouseholdIncomeCodeToIncomeAmountMapRepository(ISessionContext<CdiEtl> sessionContext) : base(sessionContext)
        {
        }
    }
}
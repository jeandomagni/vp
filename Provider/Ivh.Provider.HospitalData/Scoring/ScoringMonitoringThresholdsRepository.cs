﻿namespace Ivh.Provider.HospitalData.Scoring
{
    using System;
    using System.Linq;
    using Common.Base.Enums;
    using Common.Data;
    using Common.Data.Connection.Connections;
    using Common.Data.Interfaces;
    using Common.VisitPay.Enums;
    using Domain.HospitalData.Scoring.Entities;
    using Domain.HospitalData.Scoring.Interfaces;
    using NHibernate;

    public class ScoringMonitoringThresholdsRepository : RepositoryBase<ScoringMonitoringThresholds, CdiEtl>, IScoringMonitoringThresholdsRepository
    {
        public ScoringMonitoringThresholdsRepository(ISessionContext<CdiEtl> sessionContext) : base(sessionContext)
        {
        }

        public ScoringMonitoringThresholds GetScoringMonitoringThresholds(ScoringMonitoringTypeEnum scoringMonitoringTypeEnum)
        {
            return this.GetQueryable().FirstOrDefault(x => x.ScoringMonitoringTypeId == (int)scoringMonitoringTypeEnum);
        }
    }
}
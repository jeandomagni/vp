﻿namespace Ivh.Provider.HospitalData.Scoring
{
    using System.Xml.Serialization;

    [XmlType(TypeName = "Error")]
    public class ErrorResponse
    {
        [XmlElement("ErrorType")]
        public string ErrorType { get; set; }

        [XmlElement("ErrorMessage")]
        public string ErrorMessage { get; set; }
    }
}

﻿namespace Ivh.Provider.HospitalData.Scoring
{
    using System;
    using System.IO;
    using System.Net.Http;
    using System.Net.Http.Headers;
    using System.Text;
    using System.Threading;
    using System.Threading.Tasks;
    using System.Xml;
    using System.Xml.Serialization;
    using Domain.HospitalData.Scoring.Interfaces;
    using Domain.HospitalData.Scoring.Entities;
    using Domain.Logging.Interfaces;
    using Domain.Settings.Interfaces;
    using Polly;
    using Polly.Retry;
    using Ivh.Common.Base.Utilities.Extensions;

    public class ThirdPartyDataProvider : IThirdPartyDataProvider
    {
        private const int RetryTimes = 3; // Retry 3 times
        private const int RetryWaitTimeMs = 300; // Wait 300ms between each try.

        private const string PaidProb = "PAID_PROB";
        private const string InTotTotBillLvlWo = "IN_TOT_TOT_BILL_LVL_WO";
        private const string InContTotWoCount = "IN_CONT_TOT_WO_COUNT";
        private const string InTotY1PayCountCc = "IN_TOT_Y1_PAY_COUNT_CC";
        private const string InTotTotPayCountCc = "IN_TOT_TOT_PAY_COUNT_CC";
        private const string InTotTotCtContWo = "IN_TOT_TOT_CT_CONT_WO";
        private const string HhIncome = "HH_INCOME";
        private const string HhMemberCount = "HH_MEMBER_COUNT";
        private const string OwnRent = "OWN_RENT";
        private const string ImputedHhIncome = "IMPUTED_HH_INCOME";
        private const string ImputedHhMemberCount = "IMPUTED_HH_MEMBER_COUNT";
        private const string AmericaResponseTag = "RTResponse";

        private readonly Lazy<ILoggingService> _loggingService;
        private readonly Lazy<IApplicationSettingsService> _appliationSettingsService;

        public ThirdPartyDataProvider(Lazy<ILoggingService> loggingService,
                                      Lazy<IApplicationSettingsService> appliationSettingsService)
        {
            this._loggingService = loggingService;
            this._appliationSettingsService = appliationSettingsService;
        }

        public async Task<ThirdPartyData> CallThirdPartyDataService(ThirdPartyRequestInput requestInput, string url)
        {
            CancellationTokenSource cancellationTokenSource = new CancellationTokenSource();
            CancellationToken cancellationToken = cancellationTokenSource.Token;
            int retries = 0;
            string responseXml = string.Empty;
            RetryPolicy policy = Policy.Handle<Exception>().WaitAndRetryAsync(
                RetryTimes, 
                attempt => TimeSpan.FromMilliseconds(RetryWaitTimeMs),
                (exception, calculatedWaitDuration) =>
                {
                    this._loggingService.Value.Info(() => $"Third Party API access failed for {url}. Attempt {++retries} of {RetryTimes}. Exception: {exception}: ResponseXML: {responseXml}");
                });

            ThirdPartyData thirdPartyData = null;
            HttpClient client = new HttpClient();
            await policy.ExecuteAsync(async token =>
            {
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/xml"));
                string xmlInput = requestInput.SerializeToXml();
                HttpResponseMessage response = await client.PostAsync(url, new StringContent(xmlInput, new UTF8Encoding(false), "application/xml"), token);
                response.EnsureSuccessStatusCode();
                responseXml = await response.Content.ReadAsStringAsync();
                this._loggingService.Value.Info(() => $"Raw Third Party API data for Guarantor {requestInput.HsGuarantorId}: {responseXml}");
                RtResponse rtResponse = this.DeserializeXmlResponse(responseXml);
                if (rtResponse != null && rtResponse.ResponseType.Equals("S", StringComparison.OrdinalIgnoreCase))
                {
                    thirdPartyData = this.ResolveThirdPartyData(rtResponse, requestInput);
                }
                else
                {
                    this._loggingService.Value.Info(() => $"Third Party API error response. ErrorType: {rtResponse?.Error?.ErrorType} - Error Message: {rtResponse?.Error?.ErrorMessage}");
                }
            }, cancellationToken);

            return thirdPartyData;
        }

        private RtResponse DeserializeXmlResponse(string xml)
        {
            StringReader strReader = null;
            XmlTextReader xmlReader = null;
            RtResponse rtResponse;
            try
            {
                strReader = new StringReader(xml);
                XmlRootAttribute root = new XmlRootAttribute
                {
                    Namespace = this._appliationSettingsService.Value.ScoringThirdPartyXmlNamespace.Value,
                    ElementName = AmericaResponseTag
                };
                XmlSerializer serializer = new XmlSerializer(typeof(RtResponse), root);
                xmlReader = new XmlTextReader(strReader);
                rtResponse = (RtResponse) serializer.Deserialize(xmlReader);
            }
            catch(Exception e)
            {
                this._loggingService.Value.Info(() => $"Failed serializing third party xml: {xml} with Exception {e}");
                throw;
            }
            finally
            {
                xmlReader?.Close();
                strReader?.Close();
            }

            return rtResponse;
        }

        private ThirdPartyData ResolveThirdPartyData(RtResponse rtResponse, ThirdPartyRequestInput requestInput)
        {
            ThirdPartyData thirdPartyData = new ThirdPartyData
            {
                HsGuarantorId = requestInput.HsGuarantorId,
                ThirdPartyProviderId = requestInput.ThirdPartyProvider,
                MessageId = rtResponse.MessageId,
                CreatedDate = DateTime.UtcNow,
                Dataset = requestInput.Userfield1,
                RequestInputString = requestInput.RequestInputString
            };
            foreach (DataValue v in rtResponse.Values)
            {
                switch (v.Name)
                {
                    case PaidProb:
                        thirdPartyData.ProbabilityScore = GetValue<decimal>(v.Value);
                        break;
                    case InTotTotBillLvlWo:
                        thirdPartyData.WriteOffs5Years = string.IsNullOrEmpty(v.Value) ? (int?) null : GetValue<int>(v.Value);
                        break;
                    case InContTotWoCount:
                        thirdPartyData.WriteOffsContinuity5Years = string.IsNullOrEmpty(v.Value) ? (int?) null : GetValue<int>(v.Value);
                        break;
                    case InTotY1PayCountCc:
                        thirdPartyData.CreditCardPayments1Year = string.IsNullOrEmpty(v.Value) ? (int?) null : GetValue<int>(v.Value);
                        break;
                    case InTotTotPayCountCc:
                        thirdPartyData.CreditCardPayments5Year = string.IsNullOrEmpty(v.Value) ? (int?) null : GetValue<int>(v.Value);
                        break;
                    case InTotTotCtContWo:
                        thirdPartyData.ContinuityContractWriteOffs5Years = string.IsNullOrEmpty(v.Value) ? (int?) null : GetValue<int>(v.Value);
                        break;
                    case HhIncome:
                        thirdPartyData.HouseholdIncome = v.Value;
                        break;
                    case HhMemberCount:
                        thirdPartyData.HouseholdMemberCount = string.IsNullOrEmpty(v.Value) ? (int?)null : GetValue<int>(v.Value);
                        break;
                    case OwnRent:
                        thirdPartyData.OwnRent = string.IsNullOrEmpty(v.Value) ? (int?)null : GetValue<int>(v.Value);
                        break;
                    case ImputedHhIncome:
                        thirdPartyData.ImputedHouseholdIncome = v.Value;
                        break;
                    case ImputedHhMemberCount:
                        thirdPartyData.ImputedMemberCount = v.Value;
                        break;
                }
            }

            return thirdPartyData;
        }

        private static T GetValue<T>(string str)
        {
            if (string.IsNullOrEmpty(str)) return default(T);
            return (T)Convert.ChangeType(str, typeof(T));
        }

    }

}


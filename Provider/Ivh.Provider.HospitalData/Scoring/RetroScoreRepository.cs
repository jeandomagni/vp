﻿namespace Ivh.Provider.HospitalData.Scoring
{
    using System;
    using Common.Cache;
    using Common.Data;
    using Common.Data.Connection.Connections;
    using Common.Data.Interfaces;
    using Domain.HospitalData.Scoring.Entities;
    using Domain.HospitalData.Scoring.Interfaces;
    using NHibernate;

    public class RetroScoreRepository : RepositoryBase<RetroScore, CdiEtl>, IRetroScoreRepository
    {
        public RetroScoreRepository(ISessionContext<CdiEtl> sessionContext, Lazy<IDistributedCache> cache = null) : base(sessionContext)
        {
        }

        public RetroScoreRepository(ISessionContext<CdiEtl> sessionContext, IStatelessSessionContext<CdiEtl> statelessSessionContext, Lazy<IDistributedCache> cache = null) : base(sessionContext, statelessSessionContext, cache)
        {
        }
    }
}
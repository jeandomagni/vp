﻿namespace Ivh.Provider.HospitalData.Scoring
{
    using System.Xml.Serialization;
    using System.Collections.Generic;

    [XmlType(TypeName = "RTResponse")]
    public class RtResponse
    {

        [XmlElement(ElementName = "ResponseType")]
        public string ResponseType { get; set; }

        [XmlElement(ElementName = "MessageID")]
        public string MessageId { get; set; }

        [XmlElement("VALUE")]
        public List<DataValue> Values { get; set; }

        [XmlElement("Error")]
        public ErrorResponse Error { get; set; }
    }
}

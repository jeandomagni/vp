﻿namespace Ivh.Provider.HospitalData.Scoring
{
    using System.Collections.Generic;
    using System.Linq;
    using Common.Data;
    using Common.Data.Connection.Connections;
    using Common.Data.Interfaces;
    using Domain.HospitalData.Scoring.Entities;
    using Domain.HospitalData.Scoring.Interfaces;
    using NHibernate;
    using NHibernate.Criterion;

    public class BaseScoreRepository : RepositoryBase<BaseScore, CdiEtl>, IBaseScoreRepository
    {
        public BaseScoreRepository(ISessionContext<CdiEtl> sessionContext, IStatelessSessionContext<CdiEtl> statelessSessionContext) : base(sessionContext, statelessSessionContext)
        {
        }

        public IList<BaseScore> GetScoresByBatchId(int scoringBatchId)
        {
            return this.StatelessSession.QueryOver<BaseScore>()
                .Where(x => x.ScoringBatchId == scoringBatchId)
                .List<BaseScore>();
        }

        public IList<BaseScore> GetScoresByBatchIds(IList<int> scoringBatchIds)
        {
            ICriteria criteria = this.StatelessSession.CreateCriteria(typeof(BaseScore));
            criteria.Add(Restrictions.In("ScoringBatchId", scoringBatchIds.ToList()));
            return criteria.List<BaseScore>();
        }


        public IList<BaseScore> GetTopScoresNotInScoringBatchId(int count, int scoringBatchId)
        {
            return this.StatelessSession.QueryOver<BaseScore>()
                .Where(x => x.ScoringBatchId != scoringBatchId)
                .OrderBy(x => x.ScoringBatchId).Desc
                .Take(count)
                .List<BaseScore>();
        }

    }
}
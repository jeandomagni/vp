﻿namespace Ivh.Provider.HospitalData.Scoring
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Net.Http;
    using Common.Api.Client;
    using Common.Api.Helpers;
    using Common.Base.Enums;
    using Common.Base.Utilities.Helpers;
    using Common.Data;
    using Common.Data.Connection.Connections;
    using Common.Data.Interfaces;
    using Common.VisitPay.Enums;
    using Domain.HospitalData.Scoring.Entities;
    using Domain.HospitalData.Scoring.Interfaces;
    using Domain.Logging.Interfaces;
    using Newtonsoft.Json;
    using NHibernate;
    using NHibernate.Criterion;
    using Ivh.Domain.HospitalData.Visit.Entities;
    using Ivh.Domain.HospitalData.BillingSystem.Entities;
    using NHibernate.SqlCommand;

    public class MatchedGuarantorsVisitBatchRepository : RepositoryBase<MatchedGuarantorsVisitBatch, CdiEtl>, IMatchedGuarantorsVisitBatchRepository
    {
        private readonly Lazy<ILoggingProvider> _logger;
        public MatchedGuarantorsVisitBatchRepository(ISessionContext<CdiEtl> sessionContext, Lazy<ILoggingProvider> logger) : base(sessionContext)
        {

            this._logger = logger;
        }

        public IList<MatchedGuarantorsVisitBatch> GetMatchedGuarantors(int scoringBatchId)
        {
            MatchedGuarantorsVisitBatch matchGuartantorBatchAlias = null;
            VisitBatch visitBatchAlias = null;
            Visit visit = null;
            Facility facility = null;
            HsBillingSystem billingSystem = null;
            HsRegion region = null;

            IQueryOver<MatchedGuarantorsVisitBatch, MatchedGuarantorsVisitBatch> query = this.Session.QueryOver(() => matchGuartantorBatchAlias)
                .Left.JoinAlias(x => x.VisitBatch, () => visitBatchAlias, x => x.ScoringBatchId == matchGuartantorBatchAlias.ScoringBatchId)
                .JoinAlias(() => visitBatchAlias.Visit, () => visit, JoinType.InnerJoin)
                .JoinAlias(() => visit.Facility, () => facility, JoinType.InnerJoin)
                .JoinAlias(() => visit.BillingSystem, () => billingSystem, JoinType.InnerJoin)
                .Left.JoinAlias(() => billingSystem.Region, () => region, x => x.RegionId == billingSystem.RegionId)
                .Where(x => x.ScoringBatchId == scoringBatchId)
                .Where(x => visitBatchAlias.IsEligibleToScore);

            return query.List<MatchedGuarantorsVisitBatch>();
        }

        public void RunDailyBatchLoadSp(int batchId,
            decimal minimumHsCurrentBalanceToScore,
            decimal minimumGuarantorBatchBalanceToScore,
            decimal minimumGuarantorBalanceToScore)
        {
            this.Session.CreateSQLQuery(@"exec [scoring].[DailyVisitLoad] :ScoringBatchId, :MinimumHsCurrentBalanceToScore, :MinimumGuarantorBatchBalanceToScore, :MinimumGuarantorBalanceToScore")
                .SetParameter("ScoringBatchId", batchId)
                .SetParameter("MinimumHsCurrentBalanceToScore", minimumHsCurrentBalanceToScore)
                .SetParameter("MinimumGuarantorBatchBalanceToScore", minimumGuarantorBatchBalanceToScore)
                .SetParameter("MinimumGuarantorBalanceToScore", minimumGuarantorBalanceToScore)
                .ExecuteUpdate();
        }

        public void RunRetroBatchLoadSp(int batchId,
            DateTime dateToScore,
            decimal minimumHsCurrentBalanceToScore,
            decimal minimumGuarantorBatchBalanceToScore,
            decimal minimumGuarantorBalanceToScore)
        {
            this.Session.CreateSQLQuery(@"exec [scoring].[RetroVisitLoad] :ScoringBatchId, :DateToScore, :MinimumHsCurrentBalanceToScore, :MinimumGuarantorBatchBalanceToScore, :MinimumGuarantorBalanceToScore")
                .SetParameter("ScoringBatchId", batchId)
                .SetParameter("DateToScore", dateToScore)
                .SetParameter("MinimumHsCurrentBalanceToScore", minimumHsCurrentBalanceToScore)
                .SetParameter("MinimumGuarantorBatchBalanceToScore", minimumGuarantorBatchBalanceToScore)
                .SetParameter("MinimumGuarantorBalanceToScore", minimumGuarantorBalanceToScore)
                .ExecuteUpdate();
        }

        // DEV NOTE:
        // If this fails locally in development, make sure you have run this on the CDI_ETL database:
        // exec scoring.initializeScoring
        public void RunDailyGuarantorMatching(bool useSskForGuarantorMatching)
        {
            this.Session.CreateSQLQuery(@"exec [scoring].[DailyGuarantorMatching] :UseSskForGuarantorMatching")
                .SetParameter("UseSskForGuarantorMatching", useSskForGuarantorMatching)
                .ExecuteUpdate();
        }

        // This should only be run by the RetroScore tool and not by any other part of scoring
        public void RunGuarantorMatchingForRetroScore(bool useSskForGuarantorMatching)
        {
            Console.WriteLine($"EXEC [scoring].[GuarantorMatching] @UseSskForGuarantorMatching = {useSskForGuarantorMatching}. This can take 20-30 minutes depending on base.HsGuarantor size.");
            this.Session.CreateSQLQuery(@"exec [scoring].[GuarantorMatching] :UseSskForGuarantorMatching")
                .SetParameter("UseSskForGuarantorMatching", useSskForGuarantorMatching)
                .ExecuteUpdate();
        }

        public string CallScoringServiceApi(object scoringServiceInput, string baseUrl, string functionName, string authorizationCode, string appId, string appKey, string requestingAppName)
        {
            string url = $"/api/{functionName}?code={authorizationCode}";
            return HttpClientHelper.CallApiWith<string, object>(
                baseUrl,
                url,
                HttpMethod.Post,
                scoringServiceInput,
                (message, dto) => { },
                (message, s) => throw new Exception($"CallScoringServiceApi::GuarantorScore - Something went wrong while calling the API.  StatusCode - {message.StatusCode}, ApiName - {requestingAppName}"),
                () => HttpClientFactory.Create(new ApiHandler(
                    appId,
                    appKey,
                    requestingAppName
                )),
                false
            );
        }

        public void UpdateMatchedGuarantorsVisitBatchStatus(int scoringBatchId, ScoringStatusEnum scoringStatusEnum, IEnumerable<int> guarantorIds)
        {
            this.Session.CreateSQLQuery("EXEC scoring.SetMatchedGuarantorsVisitBatchStatus :ScoringBatchId, :ScoringStatusId, :IsEligibleToScore, :GuarantorIds")
                .SetParameter("ScoringBatchId", scoringBatchId)
                .SetParameter("ScoringStatusId", (int)scoringStatusEnum)
                .SetParameter("IsEligibleToScore", 1)
                .SetParameter("GuarantorIds", JsonConvert.SerializeObject(guarantorIds?.ToArray() ?? new int[] { }))
                .ExecuteUpdate();
        }

        public void UpdateMatchedGuarantorsVisitBatchStatus(int parentHsGuarantorId, IList<VisitBatch> vistBatches, ScoringStatusEnum scoringStatusEnum)
        {
            foreach (VisitBatch visit in vistBatches)
            {
                try
                {
                    MatchedGuarantorsVisitBatch matchedGuarantorsVisitBatch = this.GetMatchedGuarantorsVisitBatch(parentHsGuarantorId, visit.HsGuarantorId, visit.VisitBatchId, visit.ScoringBatchId);
                    matchedGuarantorsVisitBatch.ScoringStatusId = (int)scoringStatusEnum;
                    this.InsertOrUpdate(matchedGuarantorsVisitBatch);
                }
                catch (Exception e)
                {
                    this._logger.Value.Fatal(() => "MatchedGuarantorsVisitBatchRepository::UpdateMatchedGuarantorsVisitBatchStatus - Exception updating MatchedGuarantorsVisitBatch " +
                                             $"ParentHsGuarantorId = {parentHsGuarantorId}, ChildHsGuarantorId = {visit.HsGuarantorId}, VisitBatchId = {visit.VisitBatchId}, ScoringBatchId = {visit.ScoringBatchId} " +
                                             $"to ScoringStatus {scoringStatusEnum} - exception = {ExceptionHelper.AggregateExceptionToString(e)}");
                    throw;
                }
            }

        }

        public MatchedGuarantorsVisitBatch GetMatchedGuarantorsVisitBatch(int parentHsGuarantorId, int childHsGuarantorId, int visitBatchId, int scoringBatchId)
        {
            ICriteria matchedGuarantorsVisitBatch = this.Session.CreateCriteria<MatchedGuarantorsVisitBatch>();
            matchedGuarantorsVisitBatch
                .Add(Restrictions.Eq("ParentHsGuarantorId", parentHsGuarantorId))
                .Add(Restrictions.Eq("ChildHsGuarantorId", childHsGuarantorId))
                .Add(Restrictions.Eq("VisitBatchId", visitBatchId))
                .Add(Restrictions.Eq("ScoringBatchId", scoringBatchId));
            return matchedGuarantorsVisitBatch.UniqueResult<MatchedGuarantorsVisitBatch>();

        }

    }
}

﻿namespace Ivh.Provider.HospitalData.Analytics
{
    using System;
    using Common.Cache;
    using Common.Data;
    using Common.Data.Connection.Connections;
    using Common.Data.Interfaces;
    using Domain.HospitalData.Analytics.Entities;
    using Domain.HospitalData.Analytics.Interfaces;

    public class AnalyticReportClassificationRepository : RepositoryBase<AnalyticReportClassification, CdiEtl>, IAnalyticReportClassificationRepository
    {
        public AnalyticReportClassificationRepository(ISessionContext<CdiEtl> sessionContext, Lazy<IDistributedCache> cache = null) : base(sessionContext)
        {
        }
    }
}
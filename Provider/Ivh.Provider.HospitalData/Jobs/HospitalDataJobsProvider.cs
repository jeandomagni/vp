﻿namespace Ivh.Provider.HospitalData.Jobs
{
    using Application.HospitalData.Common.Interfaces;
    using Common.Data.Connection.Connections;
    using Common.Data.Interfaces;
    using NHibernate;

    public class HospitalDataJobsProvider : IHospitalDataJobsProvider
    {
        private readonly ISession _session;
        public HospitalDataJobsProvider(ISessionContext<CdiEtl> session)
        {
            this._session = session.Session;
        }
    }
}

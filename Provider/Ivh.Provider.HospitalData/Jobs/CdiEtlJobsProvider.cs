﻿namespace Ivh.Provider.HospitalData.Jobs
{
    using Application.HospitalData.Common.Interfaces;
    using Common.Base.Enums;
    using Common.Data.Connection.Connections;
    using Common.Data.Interfaces;
    using Common.VisitPay.Enums;
    using NHibernate;

    public class CdiEtlJobsProvider : ICdiEtlJobsProvider
    {
        private readonly ISession _session;
        public CdiEtlJobsProvider(ISessionContext<CdiEtl> sessionContext)
        {
            this._session = sessionContext.Session;
        }

        public void HSPersonDailyRecordCount()
        {
            ISQLQuery query = this._session.CreateSQLQuery("exec [monitor].[Get_HSPersonDailyRecordCount]; SELECT 1; ");
            int result = query.UniqueResult<int>();
        }
        public void HSPersonDeltaRecordCount()
        {
            ISQLQuery query = this._session.CreateSQLQuery("exec [monitor].[Get_HSPersonDeltaRecordCount]; SELECT 1; ");
            int result = query.UniqueResult<int>();
        }
        public void HSPersonSnapshotRecordCount()
        {
            ISQLQuery query = this._session.CreateSQLQuery("exec [monitor].[Get_HSGuarantorStageRecordCount ]; SELECT 1; ");
            int result = query.UniqueResult<int>();
        }
        public void HSGuarantorStageRecordCount()
        {
            ISQLQuery query = this._session.CreateSQLQuery("exec [monitor].[Get_HSPersonSnapshotRecordCount]; SELECT 1; ");
            int result = query.UniqueResult<int>();
        }
        public void HSGuarantorRecordCdiCount()
        {
            ISQLQuery query = this._session.CreateSQLQuery("exec [monitor].[Get_HSGuarantorRecordCdiCount ]; SELECT 1; ");
            int result = query.UniqueResult<int>();
        }
        public void GuarantorSnapshotAndHist()
        {
            ISQLQuery query = this._session.CreateSQLQuery("exec [etl].[SP_GuarantorSnapshotAndHist]; SELECT 1; ");
            int result = query.UniqueResult<int>();
        }
        
        public void AccountSnapshotAndHist()
        {
            ISQLQuery query = this._session.CreateSQLQuery("exec [etl].[SP_AccountSnapshotAndHist]; SELECT 1; ");
            int result = query.UniqueResult<int>();
        }
        public void TransactionSnapshotAndHist()
        {
            ISQLQuery query = this._session.CreateSQLQuery("exec [etl].[SP_TransactionSnapshotAndHist]; SELECT 1; ");
            int result = query.UniqueResult<int>();
        }

        public void QueueChangeEvents()
        {
            IQuery query = this._session.CreateSQLQuery("exec [base].[SP_QueueChangeEvents] @ApplicationId =:ApplicationId; SELECT 1; ")
                .SetParameter("ApplicationId", (int)ApplicationEnum.VisitPay);
            int result = query.UniqueResult<int>();
        }

        public void HSGuarantorStageTruncInsert()
        {
            ISQLQuery query = this._session.CreateSQLQuery("exec [etl].[SP_HSGuarantorStageTruncInsert]; SELECT 1; ");
            int result = query.UniqueResult<int>();
        }

        public void VisitStageTruncInsert()
        {
            ISQLQuery query = this._session.CreateSQLQuery("exec [etl].[SP_VisitStageTruncInsert]; SELECT 1; ");
            int result = query.UniqueResult<int>();
        }

        public void VisitTransactionStageTruncInsert()
        {
            ISQLQuery query = this._session.CreateSQLQuery("exec [etl].[SP_VisitTransactionStageTruncInsert]; SELECT 1;");
            int result = query.UniqueResult<int>();
        }

        public void HsGuarantor()
        {
            ISQLQuery query = this._session.CreateSQLQuery("exec [base].[SP_HSGuarantor]; SELECT 1; ");
            int result = query.UniqueResult<int>();
        }

        public void Visit()
        {
            ISQLQuery query = this._session.CreateSQLQuery("exec [base].[SP_Visit]; SELECT 1; ");
            int result = query.UniqueResult<int>();
        }
        
        public void VisitTransaction()
        {
            ISQLQuery query = this._session.CreateSQLQuery("exec [base].[SP_VisitTransaction]; SELECT 1; ");
            int result = query.UniqueResult<int>();
        }
    }
}

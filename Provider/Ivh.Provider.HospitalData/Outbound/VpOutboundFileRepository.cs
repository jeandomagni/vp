﻿namespace Ivh.Provider.HospitalData.Outbound
{
    using System.Collections.Generic;
    using System.Linq;
    using Common.Data;
    using Common.Data.Connection.Connections;
    using Common.Data.Interfaces;
    using Domain.HospitalData.Outbound.Entities;
    using Domain.HospitalData.Outbound.Interfaces;

    public class VpOutboundFileRepository : RepositoryBase<VpOutboundFile, CdiEtl>, IVpOutboundFileRepository
    {
        public VpOutboundFileRepository(ISessionContext<CdiEtl> sessionContext) : base(sessionContext)
        {
        }

        public List<VpOutboundFile> GetRecordsByType(int loadTrackerId, string vpOutboundFileTypeIds)
        {
            List<int> fileTypeIds = vpOutboundFileTypeIds.Split(',').Select(int.Parse).ToList();
            return this.Session.Query<VpOutboundFile>()
                .Where(x => x.OutboundLoadTrackerId == loadTrackerId)
                .Where(x => fileTypeIds.Contains(x.VpOutboundFileTypeId))
                .ToList();
        }

        public void UpdateSentFileTrackerId(IList<VpOutboundFile> fileRecords, int fileTrackerId)
        {
            foreach (VpOutboundFile vpOutboundFile in fileRecords)
            {
                vpOutboundFile.SentFileTrackerId = fileTrackerId;
                this.InsertOrUpdate(vpOutboundFile);
            }
        }

    }
}
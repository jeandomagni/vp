﻿namespace Ivh.Provider.HospitalData.Outbound
{
    using System.Collections.Generic;
    using System.Linq;
    using Common.Data;
    using Common.Data.Connection.Connections;
    using Common.Data.Interfaces;
    using Domain.HospitalData.Outbound.Entities;
    using Domain.HospitalData.Outbound.Interfaces;

    public class FileTypeConfigOutboundRepository : RepositoryBase<FileTypeConfigOutbound, CdiEtl>, IFileTypeConfigOutboundRepository
    {
        public FileTypeConfigOutboundRepository(ISessionContext<CdiEtl> sessionContext) : base(sessionContext)
        {
        }

        public IList<FileTypeConfigOutbound> GetFileTypeConfigOutbounds()
        {
            return this.Session.Query<FileTypeConfigOutbound>()
                .Where(x => x.FileTypeProvider != null && x.FileTypeProvider != "")
                .Where(x => x.IsActive)
                .ToList();
        }
    }
}
﻿
namespace Ivh.Provider.HospitalData.Outbound
{
    using Domain.FileStorage.Entities;
    using Domain.HospitalData.Outbound.Interfaces;
    using Domain.Logging.Interfaces;
    using Ionic.Zip;
    using Ivh.Common.Base.Utilities.Helpers;
    using Ivh.Domain.FileStorage.Interfaces;
    using System;
    using System.Collections.Generic;
    using System.IO;

    public class OutboundZipFileProvider : IOutboundFileProvider
    {
        private readonly Lazy<IFileStorageService> _fileStorageService;
        private readonly Lazy<IFileManifestProvider> _fileManifestProvider;
        private readonly Lazy<ILoggingProvider> _logger;
        private const string ZipFileExtension = ".ZIP";

        public OutboundZipFileProvider(
            Lazy<IFileStorageService> fileStorageService,
            Lazy<IFileManifestProvider> fileManifestProvider,
            Lazy<ILoggingProvider> logger
        )
        {
            this._fileStorageService = fileStorageService;
            this._fileManifestProvider = fileManifestProvider;
            this._logger = logger;
        }

        public bool GenerateOutboundFile(IList<string> fileGuids, string destinationDirectory, string fileName, bool includeManifest = false)
        {
            //Validation - if one of these fails then the configuration is bad, so throw exception
            if (string.IsNullOrEmpty(destinationDirectory) || string.IsNullOrEmpty(fileName))
            {
                string message = $"{nameof(this.GenerateOutboundFile)} - Failed - destinationDirectory or fileName parameter is missing";
                this._logger.Value.Fatal(() => message);
                throw new Exception(message);
            }

            if (!fileName.ToUpper().EndsWith(ZipFileExtension))
            {
                string message = $"{nameof(this.GenerateOutboundFile)} - Failed - Zip file extension missing from fileName: {fileName}";
                this._logger.Value.Fatal(() => message);
                throw new Exception(message);
            }

            //Create the directory if it doesn't exist
            try
            {
                if (!Directory.Exists(destinationDirectory))
                {
                    Directory.CreateDirectory(destinationDirectory);
                }
            }
            catch (Exception ex)
            {
                this._logger.Value.Fatal(() => $"{nameof(this.GenerateOutboundFile)} - Failed - Stack: {ExceptionHelper.AggregateExceptionToString(ex)}");
                throw;
            }

            //Load files from fileStorage
            List<FileStored> files = this.GetFiles(fileGuids);

            //Zip up files
            bool zipResult = this.ZippyZipZip(files, destinationDirectory, fileName, includeManifest);
            return zipResult;
        }

        private bool ZippyZipZip(List<FileStored> files, string destinationDirectory, string fileName, bool includeManifest)
        {
            try
            {
                using (ZipFile zipFile = new ZipFile())
                {
                    //Add all files to the zip
                    foreach (FileStored file in files)
                    {
                        zipFile.AddEntry(file.Filename, file.FileContents);
                    }

                    //VP-7837: Disabling zip manifest by default as it is breaking letterLogic, but allowing for it to be used in the future
                    if (includeManifest)
                    {
                        //Create the file manifest
                        string manifest = this._fileManifestProvider.Value.CreateFileManifestStringForFiles(files);

                        //Add the manifest to the zip
                        string manifestFileName = fileName.ToUpper().Replace(ZipFileExtension, "_MANIFEST.TXT");
                        zipFile.AddEntry(manifestFileName, manifest);
                    }

                    //Save zip
                    string fullFilePath = $"{destinationDirectory}\\{fileName}";
                    zipFile.Save(fullFilePath);
                }

                return true;
            }
            catch (Exception ex)
            {
                this._logger.Value.Fatal(() => $"{nameof(this.GenerateOutboundFile)} - Failed - Stack: {ExceptionHelper.AggregateExceptionToString(ex)}");
                return false;
            }
        }

        private List<FileStored> GetFiles(IList<string> fileGuids)
        {
            List<FileStored> files = new List<FileStored>();
            foreach (string fileGuidString in fileGuids)
            {
                try
                {
                    Guid fileGuid;
                    if (Guid.TryParse(fileGuidString, out fileGuid))
                    {
                        FileStored file = this._fileStorageService.Value.GetFileAsync(fileGuid).Result;
                        if (file == null)
                        {
                            //File not found, fail
                            throw new Exception($"{nameof(this.GenerateOutboundFile)} - Failed - File with ExternalKey '{fileGuidString}' not found");
                        }

                        files.Add(file);
                    }
                    else
                    {
                        //Unable to parse guid, fail
                        throw new Exception($"{nameof(this.GenerateOutboundFile)} - Failed - Unable to parse ExternalKey '{fileGuidString}'");
                    }
                }
                catch (Exception ex)
                {
                    this._logger.Value.Fatal(() => $"{nameof(this.GenerateOutboundFile)} - Failed - Stack: {ExceptionHelper.AggregateExceptionToString(ex)}");
                    throw;
                }
            }

            return files;
        }
    }
}

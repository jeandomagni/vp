﻿namespace Ivh.Provider.Enterprise.Api
{
    using System.Linq;
    using Common.Base.Enums;
    using Common.Data;
    using Common.Data.Connection.Connections;
    using Common.Data.Interfaces;
    using Common.VisitPay.Enums;
    using Domain.Enterprise.Api.Entities;
    using Domain.Enterprise.Api.Interfaces;
    using NHibernate;
    using NHibernate.Linq;

    public class ApplicationRegistrationRepository : RepositoryBase<ApplicationRegistration, Enterprise>, IApplicationRegistrationRepository
    {
        public ApplicationRegistrationRepository(ISessionContext<Enterprise> sessionContext)
            : base(sessionContext)
        {
        }

        public ApplicationRegistration GetApplicationRegistration(ApiEnum api, string appId)
        {
            return this.Session.Query<ApplicationRegistration>()
                .Where(x => x.Api == api && x.AppId == appId)
                .WithOptions(x =>
                {
                    x.SetCacheable(true);
                    x.SetCacheMode(CacheMode.Get);
                })
                .Take(1).SingleOrDefault();
        }
    }
}

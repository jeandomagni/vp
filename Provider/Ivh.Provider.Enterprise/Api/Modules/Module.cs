﻿namespace Ivh.Provider.Enterprise.Api.Modules
{
    using Autofac;
    using Domain.Enterprise.Api.Interfaces;

    public class Module : Autofac.Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<ApplicationRegistrationRepository>().As<IApplicationRegistrationRepository>();
        }
    }
}
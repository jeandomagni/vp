﻿namespace Ivh.Provider.Enterprise.Monitoring
{
    using System;
    using System.Collections.Generic;
    using System.Configuration;
    using System.Data.SqlClient;
    using System.Data;
    using Common.Base.Utilities.Extensions;
    using Common.Data.Interfaces;
    using Domain.Enterprise.Monitoring.Entities;
    using Domain.Enterprise.Monitoring.Interfaces;

    public class GaugeProvider : IGaugeProvider
    {
        private const int RunGaugeInternalConnectionTimeout = 5;
        private const int RunGaugeInternalMaxRetry = 10;

        private readonly IConnectionStringService _connectionStringService;

        public GaugeProvider(IConnectionStringService connectionStringService)
        {
            this._connectionStringService = connectionStringService;
        }

        public decimal RunGauge(Gauge gauge)
        {
            return this.RunGaugeInternal(gauge);
        }

        public IDictionary<Gauge, decimal> RunGauges(IEnumerable<Gauge> gauges, bool continueAfterFailure = false)
        {
            IDictionary<Gauge, decimal> gaugeResults = new Dictionary<Gauge, decimal>();

            foreach (Gauge gauge in gauges)
            {
                try
                {
                    gaugeResults.Add(gauge, this.RunGaugeInternal(gauge));
                }
                catch// allow
                {
                    if (!continueAfterFailure)
                    {
                        throw;
                    }
                }
            }
            return gaugeResults;
        }

        private decimal RunGaugeInternal(Gauge gauge)
        {
            string connectionString = this._connectionStringService.GetByKey(gauge.ConnectionStringKey);
            if (connectionString.IsNotNullOrEmpty())
            {
                SqlConnectionStringBuilder sqlConnectionStringBuilder = new SqlConnectionStringBuilder(connectionString) { ConnectTimeout = RunGaugeInternalConnectionTimeout };

                for (int i = 0; i < RunGaugeInternalMaxRetry; i++)
                {
                    try
                    {
                        using (SqlConnection dbConnection = new SqlConnection(sqlConnectionStringBuilder.ConnectionString))
                        {
                            dbConnection.Open();
                            using (SqlTransaction dbTransaction = dbConnection.BeginTransaction(Ivh.Common.Data.Constants.IsolationLevel.Default))
                            {
                                object result = null;
                                using (SqlCommand dbCommand = dbConnection.CreateCommand())
                                {
                                    dbCommand.Transaction = dbTransaction;
                                    dbCommand.CommandType = CommandType.Text;
                                    dbCommand.CommandText = gauge.CommandText;
                                    if (gauge.CommandTimeout.HasValue)
                                    {
                                        dbCommand.CommandTimeout = gauge.CommandTimeout.Value;
                                    }

                                    result = dbCommand.ExecuteScalar();
                                }
                                dbConnection.Close();
                                return Convert.ToDecimal(result);
                            }
                        }
                    }
                    catch (SqlException)
                    {
                        if (i >= RunGaugeInternalMaxRetry)
                        {
                            throw;
                        }
                    }
                }
            }
            else
            {
                throw new ConfigurationErrorsException($"A connection string with the key {gauge.ConnectionStringKey} could not be found.");
            }
            throw new Exception($"Gauge: {gauge.GaugeName} failed to process after {RunGaugeInternalMaxRetry} attempts.");
        }
    }
}

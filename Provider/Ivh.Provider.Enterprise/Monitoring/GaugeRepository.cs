﻿namespace Ivh.Provider.Enterprise.Monitoring
{
    using System.Collections.Generic;
    using System.Linq;
    using Common.Data;
    using Common.Data.Connection.Connections;
    using Common.Data.Interfaces;
    using Domain.Enterprise.Monitoring.Entities;
    using Domain.Enterprise.Monitoring.Interfaces;
    using NHibernate;
    using NHibernate.Criterion;

    public class GaugeRepository : RepositoryBase<Gauge, Enterprise>, IGaugeRepository
    {
        public GaugeRepository(ISessionContext<Enterprise> sessionContext) : base(sessionContext)
        {
        }

        public Gauge GetByName(string gaugeName)
        {
            return this.Session.QueryOver<Gauge>().Where(x => x.GaugeName == gaugeName).Take(1).SingleOrDefault();
        }

        public IEnumerable<Gauge> GetBySetName(string gaugeSetName)
        {
            return this.Session.QueryOver<Gauge>().Where(x => x.GaugeSetName == gaugeSetName).List<Gauge>();
        }

        public IEnumerable<Gauge> GetByNames(IEnumerable<string> gaugeNames)
        {
            ICriteria gaugeCriteria = this.Session.CreateCriteria<Gauge>();
            gaugeCriteria.Add(Restrictions.In("GaugeName", gaugeNames.ToList()));
            return gaugeCriteria.List<Gauge>();
        }

        public IEnumerable<Gauge> GetBySetNames(IEnumerable<string> gaugeSetNames)
        {
            ICriteria gaugeCriteria = this.Session.CreateCriteria<Gauge>();
            gaugeCriteria.Add(Restrictions.In("GaugeSetName", gaugeSetNames.ToList()));
            return gaugeCriteria.List<Gauge>();
        }
    }
}

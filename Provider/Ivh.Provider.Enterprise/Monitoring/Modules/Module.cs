﻿namespace Ivh.Provider.Enterprise.Monitoring.Modules
{
    using Autofac;
    using Autofac.Core;
    using Common.Data;
    using Common.Data.Connection;
    using Domain.Enterprise.Monitoring.Interfaces;
    using NHibernate;

    public class Module : Autofac.Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<GaugeProvider>().As<IGaugeProvider>();
            builder.RegisterType<GaugeRepository>().As<IGaugeRepository>();
        }
    }
}
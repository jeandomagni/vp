﻿namespace Ivh.Provider.SettingsManager
{
    using System.Collections.Generic;
    using System.Linq;
    using Common.Data.Core;
    using Domain.SettingsManager.Entities;
    using Domain.SettingsManager.Interfaces;
    using NHibernate;

    public class ReleaseRepository : RepositoryBase<Release>, IReleaseRepository
    {
        private readonly ISettingsManagerUserRepository _settingsManagerUserRepository;
        
        public ReleaseRepository(
            ISession session, 
            ISettingsManagerUserRepository settingsManagerUserRepository) : base(session)
        {
            this._settingsManagerUserRepository = settingsManagerUserRepository;
        }

        public ReleaseRepository(
            ISession session, 
            IStatelessSession statelessSession, 
            ISettingsManagerUserRepository settingsManagerUserRepository) : base(session, statelessSession)
        {
            this._settingsManagerUserRepository = settingsManagerUserRepository;
        }

        public IEnumerable<Release> GetReleases()
        {
            return this.GetQueryable().OrderBy(x => x.Number).AsEnumerable();
        }

        public string GetLatestReleaseLabel()
        {
            return this.GetQueryable().OrderByDescending(x => x.Number).FirstOrDefault()?.Label ?? "";
        }

        public int AddRelease(int major, int minor, int patch, int build, string insertedBy)
        {
            int userId = this._settingsManagerUserRepository.AddSettingsManagerUser(insertedBy);
            Release release = this.GetRelease(major, minor, patch, build);
            if (release == null)
            {
                release = new Release()
                {
                    Major = major,
                    Minor = minor,
                    Patch = patch,
                    Build = build,
                    SettingsManagerUserId = userId
                };
                this.Insert(release);
            }

            return release.ReleaseId;
        }

        public Release GetRelease(int major, int minor, int patch, int build)
        {
            return this.GetQueryable().FirstOrDefault(
                x => x.Major == major
                && x.Minor == minor
                && x.Patch == patch
                && x.Build == build
            );
        }

        public Release GetLatestReleaseForVersion(int major, int minor, int patch, int build)
        {
            return this.GetQueryable()
                .OrderByDescending(x => x.Number)
                .FirstOrDefault(
                    x => x.Major <= major
                    && x.Minor <= minor
                    && x.Patch <= patch
                    && x.Build <= build);
        }
    }
}
﻿namespace Ivh.Provider.SettingsManager
{
    using Common.Data.Core;
    using Domain.SettingsManager.Entities;
    using Domain.SettingsManager.Interfaces;
    using NHibernate;

    public class ClientReleaseEnvironmentApprovalRepository : RepositoryBase<ClientReleaseEnvironmentApproval>, IClientReleaseEnvironmentApprovalRepository
    {
        public ClientReleaseEnvironmentApprovalRepository(ISession session) : base(session)
        {
        }

        public ClientReleaseEnvironmentApprovalRepository(ISession session, IStatelessSession statelessSession) : base(session, statelessSession)
        {
        }
    }
}

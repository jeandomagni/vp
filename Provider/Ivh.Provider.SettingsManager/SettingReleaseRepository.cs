﻿namespace Ivh.Provider.SettingsManager
{
    using Common.Data.Core;
    using Domain.SettingsManager.Entities;
    using Domain.SettingsManager.Interfaces;
    using NHibernate;

    public class SettingReleaseRepository : RepositoryBase<SettingRelease>, ISettingReleaseRepository
    {
        public SettingReleaseRepository(ISession session) : base(session)
        {
        }

        public SettingReleaseRepository(ISession session, IStatelessSession statelessSession) : base(session, statelessSession)
        {
        }
    }
}

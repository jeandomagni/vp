﻿namespace Ivh.Provider.SettingsManager
{
    using System.Linq;
    using Common.Data.Core;
    using Domain.SettingsManager.Common.Models;
    using Domain.SettingsManager.Interfaces;
    using NHibernate;
    using NHibernate.Transform;

    public class SettingCompositeRepository : RepositoryBase<SettingComposite>, ISettingCompositeRepository
    {

        public SettingCompositeRepository(ISession session) : base(session)
        {
        }

        public SettingCompositeRepository(ISession session, IStatelessSession statelessSession) : base(session, statelessSession)
        {
        }

        public IQueryable<SettingComposite> GetSettings(int clientId, int environmentId, int releaseId)
        {
            IQuery query = this.Session.CreateSQLQuery("select * from [dbo].[GetSettings] (:clientId,:environmentId,:releaseId)");
            query.SetInt32("clientId", clientId);
            query.SetInt32("environmentId", environmentId);
            query.SetInt32("releaseId", releaseId);
            query.SetReadOnly(true);
            query.SetResultTransformer(new AliasToBeanResultTransformer(typeof(SettingComposite)));
            return query.List<SettingComposite>().AsQueryable();
        }
    }
}
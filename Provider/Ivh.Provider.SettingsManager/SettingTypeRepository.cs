﻿namespace Ivh.Provider.SettingsManager
{
    using Common.Data.Core;
    using Domain.SettingsManager.Entities;
    using Domain.SettingsManager.Enums;
    using Domain.SettingsManager.Interfaces;
    using NHibernate;

    public class SettingTypeRepository : RepositoryBase<SettingType>, ISettingTypeRepository
    {
        public SettingTypeRepository(ISession session) : base(session)
        {
        }

        public SettingTypeRepository(ISession session, IStatelessSession statelessSession) : base(session, statelessSession)
        {
        }

        public SettingType GetSettingType(SettingTypeEnum settingTypeEnum)
        {
            return this.GetById((int)settingTypeEnum);
        }
    }
}
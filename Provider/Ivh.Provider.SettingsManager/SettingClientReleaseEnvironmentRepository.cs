﻿namespace Ivh.Provider.SettingsManager
{
    using Common.Data.Core;
    using Domain.SettingsManager.Entities;
    using Domain.SettingsManager.Interfaces;
    using NHibernate;

    public class SettingClientReleaseEnvironmentRepository : RepositoryBase<SettingClientReleaseEnvironment>, ISettingClientReleaseEnvironmentRepository
    {
        public SettingClientReleaseEnvironmentRepository(ISession session) : base(session)
        {
        }

        public SettingClientReleaseEnvironmentRepository(ISession session, IStatelessSession statelessSession) : base(session, statelessSession)
        {
        }
    }
}

﻿namespace Ivh.Provider.SettingsManager
{
    using System.Linq;
    using Common.Data.Core;
    using Domain.SettingsManager.Entities;
    using Domain.SettingsManager.Enums;
    using Domain.SettingsManager.Interfaces;
    using NHibernate;

    public class SettingsManagerUserRepository : RepositoryBase<SettingsManagerUser>, ISettingsManagerUserRepository
    {
        public SettingsManagerUserRepository(ISession session) : base(session)
        {
        }

        public SettingsManagerUserRepository(ISession session, IStatelessSession statelessSession) : base(session, statelessSession)
        {
        }

        public SettingsManagerUser GetSettingsManagerUser(int id)
        {
            return this.GetById(id);
        }

        public SettingsManagerUser GetSettingsManagerUserFromUserName(string userName)
        {
            return this.GetQueryable().FirstOrDefault(
                x => x.UserName == userName
            );
        }

        public int AddSettingsManagerUser(string userName)
        {
            SettingsManagerUser user = this.GetSettingsManagerUserFromUserName(userName);
            if (user == null)
            {
                user = new SettingsManagerUser()
                {
                    UserName = userName
                };
                this.Insert(user);
            }

            return user.SettingsManagerUserId;
        }
    }
}
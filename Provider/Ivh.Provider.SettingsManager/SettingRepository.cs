﻿namespace Ivh.Provider.SettingsManager
{
    using Common.Data.Core;
    using Domain.SettingsManager.Entities;
    using Domain.SettingsManager.Interfaces;
    using NHibernate;

    public class SettingRepository : RepositoryBase<Setting>, ISettingRepository
    {
        public SettingRepository(ISession session) : base(session)
        {
        }

        public SettingRepository(ISession session, IStatelessSession statelessSession) : base(session, statelessSession)
        {
        }

    }
}
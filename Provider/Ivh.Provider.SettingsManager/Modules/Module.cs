﻿namespace Ivh.Provider.SettingsManager.Modules
{
    using Domain.SettingsManager.Interfaces;
    using Microsoft.Extensions.DependencyInjection;

    public static class Module
    {
        public static void RegisterSettingsManagerRepositories(this IServiceCollection serviceCollection)
        {
            serviceCollection.AddTransient<IClientRepository, ClientRepository>();
            serviceCollection.AddTransient<IEnvironmentRepository, EnvironmentRepository>();
            serviceCollection.AddTransient<IReleaseRepository, ReleaseRepository>();
            serviceCollection.AddTransient<ISettingClientReleaseEnvironmentRepository, SettingClientReleaseEnvironmentRepository>();
            serviceCollection.AddTransient<ISettingClientReleaseRepository, SettingClientReleaseRepository>();
            serviceCollection.AddTransient<ISettingCompositeRepository, SettingCompositeRepository>();
            serviceCollection.AddTransient<ISettingEnvironmentReleaseRepository, SettingEnvironmentReleaseRepository>();
            serviceCollection.AddTransient<ISettingReleaseRepository, SettingReleaseRepository>();
            serviceCollection.AddTransient<ISettingRepository, SettingRepository>();
            serviceCollection.AddTransient<ISettingTypeRepository, SettingTypeRepository>();
            serviceCollection.AddTransient<ISettingsManagerUserRepository, SettingsManagerUserRepository>();
            serviceCollection.AddTransient<IClientReleaseEnvironmentApprovalRepository, ClientReleaseEnvironmentApprovalRepository>();
        }
    }
}

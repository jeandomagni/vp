﻿namespace Ivh.Provider.SettingsManager
{
    using Common.Data.Core;
    using Domain.SettingsManager.Entities;
    using Domain.SettingsManager.Interfaces;
    using NHibernate;

    public class SettingEnvironmentReleaseRepository : RepositoryBase<SettingEnvironmentRelease>, ISettingEnvironmentReleaseRepository
    {
        public SettingEnvironmentReleaseRepository(ISession session) : base(session)
        {
        }

        public SettingEnvironmentReleaseRepository(ISession session, IStatelessSession statelessSession) : base(session, statelessSession)
        {
        }
    }
}

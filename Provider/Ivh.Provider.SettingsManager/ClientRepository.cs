﻿namespace Ivh.Provider.SettingsManager
{
    using System.Collections.Generic;
    using System.Linq;
    using Common.Data.Core;
    using Domain.SettingsManager.Entities;
    using Domain.SettingsManager.Interfaces;
    using NHibernate;

    public class ClientRepository : RepositoryBase<Client>, IClientRepository
    {
        public ClientRepository(ISession session) : base(session)
        {
        }

        public ClientRepository(ISession session, IStatelessSession statelessSession) : base(session, statelessSession)
        {
        }

        public IEnumerable<Client> GetClients()
        {
            return this.GetAll().Where(x=>x.DeprecateDate == null).OrderBy(x => x.Name);
        }
    }
}
﻿namespace Ivh.Provider.SettingsManager
{
    using Common.Data.Core;
    using Domain.SettingsManager.Entities;
    using Domain.SettingsManager.Interfaces;
    using NHibernate;

    public class SettingClientReleaseRepository : RepositoryBase<SettingClientRelease>, ISettingClientReleaseRepository
    {
        public SettingClientReleaseRepository(ISession session) : base(session)
        {
        }

        public SettingClientReleaseRepository(ISession session, IStatelessSession statelessSession) : base(session, statelessSession)
        {
        }
    }
}

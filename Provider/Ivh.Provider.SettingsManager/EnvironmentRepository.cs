﻿namespace Ivh.Provider.SettingsManager
{
    using System.Collections.Generic;
    using System.Linq;
    using Common.Data.Core;
    using Domain.SettingsManager.Entities;
    using Domain.SettingsManager.Interfaces;
    using NHibernate;

    public class EnvironmentRepository : RepositoryBase<Environment>, IEnvironmentRepository
    {
        public EnvironmentRepository(ISession session) : base(session)
        {
        }

        public EnvironmentRepository(ISession session, IStatelessSession statelessSession) : base(session, statelessSession)
        {
        }

        public IEnumerable<Environment> GetEnvironments()
        {
            return this.GetAll().Where(x=>x.DeprecateDate == null).OrderBy(x => x.Name);
        }
    }
}
﻿namespace Ivh.Provider.Qat
{
    using Common.Data;
    using Common.Data.Connection.Connections;
    using Common.Data.Interfaces;
    using Domain.Qat.Entities;
    using Domain.Qat.Interfaces;

    public class IhcEnrollmentRepository : RepositoryBase<IhcEnrollmentResponse, CdiEtl>, IIhcEnrollmentRepository
    {
        public IhcEnrollmentRepository(ISessionContext<CdiEtl> sessionContext)
            : base(sessionContext)
        {
        }
    }
}
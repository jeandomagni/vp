﻿namespace Ivh.Provider.Qat
{
    using Common.Data;
    using Common.Data.Connection.Connections;
    using Common.Data.Interfaces;
    using Domain.Qat.Entities;
    using Domain.Qat.Interfaces;
    using NHibernate;

    public class DemoVisitPayUserRepository : RepositoryBase<DemoVisitPayUserClone, VisitPay>, IDemoVisitPayUserRepository
    {
        public DemoVisitPayUserRepository(ISessionContext<VisitPay> sessionContext) : base(sessionContext)
        {
        }

        public void RenameVisitPayUser(string visitPayUserName)
        {
            //TODO- this needs to get pushed into the service or application service layer

            //VisitPayUser temp = this._session.QueryOver<VisitPayUser>().Where(x => x.UserName == visitPayUserName).SingleOrDefault();
            //if (temp == null)
            //{
            //    return;
            //}
            //temp.UserName = Guid.NewGuid().ToString();

            //this._session.Save(temp);
            //this._session.Flush();
        }
    }
}
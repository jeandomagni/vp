﻿namespace Ivh.Provider.Qat
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using System.Transactions;
    using Common.Base.Utilities.Extensions;
    using Common.Base.Utilities.Helpers;
    using Common.Cache;
    using Common.VisitPay.Enums;
    using Domain.FinanceManagement.Entities;
    using Domain.FinanceManagement.Interfaces;
    using Domain.FinanceManagement.Payment.Entities;
    using Domain.FinanceManagement.Payment.Interfaces;
    using Domain.FinanceManagement.ValueTypes;
    using Newtonsoft.Json;
    using GuestPayEntities = Domain.GuestPay.Entities;
    using GuestPayInterfaces = Domain.GuestPay.Interfaces;

    public class ThrowsErrorsPaymentProvider :
        IPaymentProvider,
        GuestPayInterfaces.IPaymentProvider,
        IPaymentReversalProvider,
        GuestPayInterfaces.IPaymentReversalProvider,
        IPaymentBatchOperationsProvider
    {
        private readonly IPaymentProcessorResponseService _paymentProcessorResponseService;
        private readonly IDistributedCache _cache;

        public ThrowsErrorsPaymentProvider(IPaymentProcessorResponseService paymentProcessorResponseService, IDistributedCache cache)
        {
            this._cache = cache;
            this._paymentProcessorResponseService = paymentProcessorResponseService;
        }

        public string ProductName => "NGVP";

        async Task<BillingIdResult> GuestPayInterfaces.IPaymentProvider.VerifyPaymentMethodAsync(GuestPayEntities.Payment payment, PaymentProcessor paymentProcessor)
        {
            throw new NotImplementedException();
#pragma warning disable 162
            return await Task.Run(() => default(BillingIdResult));
#pragma warning restore 162
        }

        public async Task<PaymentProcessorResponse> SubmitPaymentAsync(Domain.GuestPay.Entities.Payment payment, PaymentProcessor paymentProcessor)
        {
            throw new NotImplementedException();
#pragma warning disable 162
            return await Task.Run(() => default(PaymentProcessorResponse));
#pragma warning restore 162
        }


        async Task<PaymentProcessorResponse> GuestPayInterfaces.IPaymentReversalProvider.SubmitPaymentVoidAsync(GuestPayEntities.Payment originalPayment, GuestPayEntities.Payment reversalPayment, PaymentProcessor paymentProcessor)
        {
            return await Task.Run(() =>
            {
                FailedPaymentErrorCodeEnum errorType = this.GetFailedPaymentErrorCode();

                return new PaymentProcessorResponse
                {
                    PaymentProcessorSourceKey = string.Empty,
                    PaymentProcessor = paymentProcessor,
                    PaymentProcessorResponseStatus = GetPaymentProcessorResponseStatusFromEnum(errorType),
                    RawResponse = "",
                    ResponseField1 = string.Empty, //transactionId,
                    ResponseField2 = GetStatusFromEnum(errorType), //status,
                    ResponseField3 = string.Empty, //authCode,
                    ResponseField4 = string.Empty, //avs,
                    ResponseField5 = GetDeclineTypeFromEnum(errorType), // declineType,
                    ResponseField6 = GetErrorFromEnum(errorType), //error,
                    ResponseField7 = GetErrorTypeFromEnum(errorType), //errorType,
                    ResponseField8 = string.Empty, //offenders,
                    ResponseField9 = string.Empty, //responseCode,
                    ResponseField10 = string.Empty, //responseCodeDescriptor
                    SnapshotTotalPaymentAmount = reversalPayment.PaymentAmount,
                    PaymentSystemType = reversalPayment.PaymentSystemType
                };
            });
        }

        async Task<PaymentProcessorResponse> GuestPayInterfaces.IPaymentReversalProvider.SubmitPaymentRefundAsync(GuestPayEntities.Payment originalPayment, GuestPayEntities.Payment reversalPayment, PaymentProcessor paymentProcessor, decimal? amount)
        {
            return await Task.Run(() =>
            {
                FailedPaymentErrorCodeEnum errorType = this.GetFailedPaymentErrorCode();

                return new PaymentProcessorResponse
                {
                    PaymentProcessorSourceKey = string.Empty,
                    PaymentProcessor = paymentProcessor,
                    PaymentProcessorResponseStatus = GetPaymentProcessorResponseStatusFromEnum(errorType),
                    RawResponse = "",
                    ResponseField1 = string.Empty, //transactionId,
                    ResponseField2 = GetStatusFromEnum(errorType), //status,
                    ResponseField3 = string.Empty, //authCode,
                    ResponseField4 = string.Empty, //avs,
                    ResponseField5 = GetDeclineTypeFromEnum(errorType), // declineType,
                    ResponseField6 = GetErrorFromEnum(errorType), //error,
                    ResponseField7 = GetErrorTypeFromEnum(errorType), //errorType,
                    ResponseField8 = string.Empty, //offenders,
                    ResponseField9 = string.Empty, //responseCode,
                    ResponseField10 = string.Empty, //responseCodeDescriptor
                    SnapshotTotalPaymentAmount = reversalPayment.PaymentAmount,
                    PaymentSystemType = reversalPayment.PaymentSystemType
                };
            });
        }

        public async Task<ICollection<QueryPaymentTransaction>> GetPaymentTransactionsAsync(DateTime beginDate, DateTime endDate, string transactionId, bool logResponse)
        {
            string serializedValues = await this._cache.GetStringAsync("Ivh.Provider.FinanceManagement.TrustCommerce.IPaymentBatchOperationsProvider.GetPaymentTransactionsAsync");

            if (string.IsNullOrEmpty(serializedValues))
            {
                return Enumerable.Empty<QueryPaymentTransaction>().ToList();
            }

            IList<QueryPaymentTransaction> values = JsonConvert.DeserializeObject<IList<QueryPaymentTransaction>>(serializedValues);

            List<QueryPaymentTransaction> returnValues = new List<QueryPaymentTransaction>();

            foreach (QueryPaymentTransaction value in values)
            {
                PaymentProcessorResponse paymentProcessorResponse = this._paymentProcessorResponseService.GetPaymentProcessorResponse(value.TransactionId);

                returnValues.Add(new QueryPaymentTransaction
                {
                    TransactionId = RandomSourceSystemKeyGenerator.New("NNN-NNNNNNNNNN"),
                    TransactionDateTime = DateTime.UtcNow,
                    RefTransid = value.TransactionId,

                    Amount = value.Amount == default(decimal) 
                        ? paymentProcessorResponse.SnapshotTotalPaymentAmount 
                        : value.Amount,

                    PaymentType = value.PaymentType == default(PaymentTypeEnum) 
                        ? paymentProcessorResponse.Payments.First().PaymentType 
                        : value.PaymentType,

                    PaymentProcessorResponseStatus = value.PaymentProcessorResponseStatus == default(PaymentProcessorResponseStatusEnum) 
                        ? PaymentProcessorResponseStatusEnum.Accepted 
                        : value.PaymentProcessorResponseStatus
                });
            }

            return returnValues;
        }

        public string PaymentProcessorName => "TrustCommerce";

        public async Task<BillingIdResult> StoreBankAccountBillingIdAsync(PaymentMethod paymentMethod, string accountNumber, string routingNumber)
        {
            return await Task.Run(() => this.GetBillingResult(routingNumber));
        }

        public async Task<BillingIdResult> StoreCreditDebitCardBillingIdAsync(PaymentMethod paymentMethod, string accountNumber, string securityCode)
        {
            return await Task.Run(() => this.GetBillingResult(securityCode));
        }

        public Task<bool> UpdateCreditCardExpirationAsync(string billingId, string expiration, string cvv)
        {
            return Task.FromResult(true);
        }

        public Task<bool> UnstoreBillingIdAsync(string billingId)
        {
            return Task.FromResult(true);
        }

        public bool UnstoreBillingId(string billingId)
        {
            return true;
        }

        async Task<IList<PaymentProcessorResponse>> IPaymentProvider.SubmitPaymentsAsync(IList<Payment> payments, PaymentProcessor paymentProcessor)
        {
            return await Task.Run(() =>
            {
                List<PaymentProcessorResponse> responses = new List<PaymentProcessorResponse>();
                List<string> differentPaymentMethods = payments.Select(x => x.PaymentMethod.BillingId).Distinct().ToList();

                foreach (string billingId in differentPaymentMethods)
                {
                    List<Payment> paymentsInQuestion = payments.Where(x => x.PaymentMethod.BillingId == billingId).ToList();
                    Payment firstPayment = paymentsInQuestion.FirstOrDefault();
                    if (firstPayment == null) continue;

                    FailedPaymentErrorCodeEnum errorType = this.GetFailedPaymentErrorCode();

                    PaymentProcessorResponse paymentProcessorResponse = new PaymentProcessorResponse
                    {
                        PaymentProcessorSourceKey = string.Empty,
                        PaymentProcessor = paymentProcessor,
                        PaymentProcessorResponseStatus = GetPaymentProcessorResponseStatusFromEnum(errorType),
                        RawResponse = "",
                        ResponseField1 = string.Empty, //transactionId,
                        ResponseField2 = GetStatusFromEnum(errorType), //status,
                        ResponseField3 = string.Empty, //authCode,
                        ResponseField4 = string.Empty, //avs,
                        ResponseField5 = GetDeclineTypeFromEnum(errorType), // declineType,
                        ResponseField6 = GetErrorFromEnum(errorType), //error,
                        ResponseField7 = GetErrorTypeFromEnum(errorType), //errorType,
                        ResponseField8 = string.Empty, //offenders,
                        ResponseField9 = string.Empty, //responseCode,
                        ResponseField10 = string.Empty, //responseCodeDescriptor
                        SnapshotTotalPaymentAmount = paymentsInQuestion.Sum(x => x.ActualPaymentAmount),
                        PaymentMethod = firstPayment.PaymentMethod,
                        PaymentSystemType = PaymentSystemTypeEnum.VisitPay
                    };

                    paymentProcessorResponse.RawResponse = paymentProcessorResponse.ResponseField2 + "-"
                                                           + paymentProcessorResponse.ResponseField5 + "-"
                                                           + paymentProcessorResponse.ResponseField6 + "-"
                                                           + paymentProcessorResponse.ResponseField7 + "-";

                    foreach (Payment payment in paymentsInQuestion)
                    {
                        PaymentPaymentProcessorResponse current = new PaymentPaymentProcessorResponse
                        {
                            InsertDate = DateTime.UtcNow,
                            Payment = payment,
                            PaymentProcessorResponse = paymentProcessorResponse,
                            SnapshotPaymentAmount = payment.ActualPaymentAmount
                        };
                        payment.PaymentPaymentProcessorResponses.Add(current);
                        paymentProcessorResponse.PaymentPaymentProcessorResponses.Add(current);
                    }

                    responses.Add(paymentProcessorResponse);
                }
                return responses;
            });
        }


        async Task<PaymentProcessorResponse> IPaymentReversalProvider.SubmitPaymentsVoidAsync(IList<Payment> originalPayments, IList<Payment> reversalPayments, PaymentProcessor paymentProcessor)
        {
            return await Task.Run(() =>
            {
                FailedPaymentErrorCodeEnum errorType = this.GetFailedPaymentErrorCode();

                return new PaymentProcessorResponse
                {
                    PaymentProcessorSourceKey = string.Empty,
                    PaymentProcessor = paymentProcessor,
                    PaymentProcessorResponseStatus = GetPaymentProcessorResponseStatusFromEnum(errorType),
                    RawResponse = "",
                    ResponseField1 = string.Empty, //transactionId,
                    ResponseField2 = GetStatusFromEnum(errorType), //status,
                    ResponseField3 = string.Empty, //authCode,
                    ResponseField4 = string.Empty, //avs,
                    ResponseField5 = GetDeclineTypeFromEnum(errorType), // declineType,
                    ResponseField6 = GetErrorFromEnum(errorType), //error,
                    ResponseField7 = GetErrorTypeFromEnum(errorType), //errorType,
                    ResponseField8 = string.Empty, //offenders,
                    ResponseField9 = string.Empty, //responseCode,
                    ResponseField10 = string.Empty, //responseCodeDescriptor
                    SnapshotTotalPaymentAmount = reversalPayments.Sum(x => x.ActualPaymentAmount),
                    PaymentMethod = reversalPayments.First().PaymentMethod,
                    PaymentSystemType = PaymentSystemTypeEnum.VisitPay
                };
            });
        }

        async Task<PaymentProcessorResponse> IPaymentReversalProvider.SubmitPaymentsRefundAsync(IList<Payment> originalPayments, IList<Payment> reversalPayments, PaymentProcessor paymentProcessor, decimal? amount)
        {
            return await Task.Run(() =>
            {
                FailedPaymentErrorCodeEnum errorType = this.GetFailedPaymentErrorCode();

                return new PaymentProcessorResponse
                {
                    PaymentProcessorSourceKey = string.Empty,
                    PaymentProcessor = paymentProcessor,
                    PaymentProcessorResponseStatus = GetPaymentProcessorResponseStatusFromEnum(errorType),
                    RawResponse = "",
                    ResponseField1 = string.Empty, //transactionId,
                    ResponseField2 = GetStatusFromEnum(errorType), //status,
                    ResponseField3 = string.Empty, //authCode,
                    ResponseField4 = string.Empty, //avs,
                    ResponseField5 = GetDeclineTypeFromEnum(errorType), // declineType,
                    ResponseField6 = GetErrorFromEnum(errorType), //error,
                    ResponseField7 = GetErrorTypeFromEnum(errorType), //errorType,
                    ResponseField8 = string.Empty, //offenders,
                    ResponseField9 = string.Empty, //responseCode,
                    ResponseField10 = string.Empty, //responseCodeDescriptor
                    SnapshotTotalPaymentAmount = reversalPayments.Sum(x => x.ActualPaymentAmount),
                    PaymentMethod = reversalPayments.First().PaymentMethod,
                    PaymentSystemType = PaymentSystemTypeEnum.VisitPay
                };
            });
        }

        public IReadOnlyDictionary<string, string> ParseResponse(string rawResponse)
        {
            if (rawResponse.IsNullOrEmpty())
                return new Dictionary<string, string>();

            return rawResponse.Split(new[] { "\r\n", "\n" }, StringSplitOptions.RemoveEmptyEntries)
                .Select(x => x.Split('='))
                .ToDictionary(x => x[0], x => x.Length > 1 ? x[1].Trim() : null);
        }

        private BillingIdResult GetBillingResult(string securityCode)
        {
            throw new NotImplementedException();
        }

        private static string GetErrorFromEnum(FailedPaymentErrorCodeEnum errorType)
        {
            switch (errorType)
            {
                default:
                    return "";
                case FailedPaymentErrorCodeEnum.BadData_missingfields:
                    return "missingfields";
                case FailedPaymentErrorCodeEnum.BadData_badformat:
                    return "badformat";
                case FailedPaymentErrorCodeEnum.BadData_badlength:
                    return "badlength";
                case FailedPaymentErrorCodeEnum.BadData_extrafields:
                    return "extrafields";
                case FailedPaymentErrorCodeEnum.BadData_merchantaccept:
                    return "merchantaccept";
                case FailedPaymentErrorCodeEnum.BadData_mismatch:
                    return "mismatch";
            }
        }

        private static string GetErrorTypeFromEnum(FailedPaymentErrorCodeEnum errorType)
        {
            switch (errorType)
            {
                default:
                    return "";
                case FailedPaymentErrorCodeEnum.ErrorType_cantconnect:
                    return "cantconnect";
                case FailedPaymentErrorCodeEnum.ErrorType_dnsfailure:
                    return "dnsfailure";
                case FailedPaymentErrorCodeEnum.ErrorType_failtoprocess:
                    return "failtoprocess";
                case FailedPaymentErrorCodeEnum.ErrorType_linkfailure:
                    return "linkfailure";
                case FailedPaymentErrorCodeEnum.ErrorType_notallowed:
                    return "notallowed";
            }
        }

        private static string GetDeclineTypeFromEnum(FailedPaymentErrorCodeEnum errorType)
        {
            switch (errorType)
            {
                default:
                    return "";
                case FailedPaymentErrorCodeEnum.DeclineType_velocity:
                    return "velocity";
                case FailedPaymentErrorCodeEnum.DeclineType_fraud:
                    return "fraud";
                case FailedPaymentErrorCodeEnum.DeclineType_expiredcard:
                    return "expiredcard";
                case FailedPaymentErrorCodeEnum.DeclineType_decline:
                    return "decline";
                case FailedPaymentErrorCodeEnum.DeclineType_cvv:
                    return "cvv";
                case FailedPaymentErrorCodeEnum.DeclineType_carderror:
                    return "carderror";
                case FailedPaymentErrorCodeEnum.DeclineType_call:
                    return "call";
                case FailedPaymentErrorCodeEnum.DeclineType_blacklist:
                    return "blacklist";
                case FailedPaymentErrorCodeEnum.DeclineType_avs:
                    return "avs";
                case FailedPaymentErrorCodeEnum.DeclineType_authexpired:
                    return "authexpired";
            }
        }

        private FailedPaymentErrorCodeEnum GetFailedPaymentErrorCode()
        {
            FailedPaymentErrorCodeEnum errorType = FailedPaymentErrorCodeEnum.Unknown;
            string codeName = this._cache.GetStringAsync("FailedPaymentErrorCodeEnum").Result;// using the synchronous GetString. Async method gets lost.
            Enum.TryParse<FailedPaymentErrorCodeEnum>(codeName, true, out errorType);
            return errorType;
        }

        private static string GetStatusFromEnum(FailedPaymentErrorCodeEnum errorType)
        {
            switch (errorType)
            {
                default:
                case FailedPaymentErrorCodeEnum.Unknown:
                    return "Unknown";
                case FailedPaymentErrorCodeEnum.ErrorType_cantconnect:
                case FailedPaymentErrorCodeEnum.ErrorType_dnsfailure:
                case FailedPaymentErrorCodeEnum.ErrorType_failtoprocess:
                case FailedPaymentErrorCodeEnum.ErrorType_linkfailure:
                case FailedPaymentErrorCodeEnum.ErrorType_notallowed:
                    return "Error";
                case FailedPaymentErrorCodeEnum.DeclineType_velocity:
                case FailedPaymentErrorCodeEnum.DeclineType_fraud:
                case FailedPaymentErrorCodeEnum.DeclineType_expiredcard:
                case FailedPaymentErrorCodeEnum.DeclineType_decline:
                case FailedPaymentErrorCodeEnum.DeclineType_cvv:
                case FailedPaymentErrorCodeEnum.DeclineType_carderror:
                case FailedPaymentErrorCodeEnum.DeclineType_call:
                case FailedPaymentErrorCodeEnum.DeclineType_blacklist:
                case FailedPaymentErrorCodeEnum.DeclineType_avs:
                case FailedPaymentErrorCodeEnum.DeclineType_authexpired:
                    return "Decline";
                case FailedPaymentErrorCodeEnum.ConnectionError_ApiDown:
                    return "ConnectionError_ApiDown";
                case FailedPaymentErrorCodeEnum.ConnectionError_LinkFailure:
                    return "ConnectionError_LinkFailure";
                case FailedPaymentErrorCodeEnum.BadData_missingfields:
                case FailedPaymentErrorCodeEnum.BadData_badformat:
                case FailedPaymentErrorCodeEnum.BadData_badlength:
                case FailedPaymentErrorCodeEnum.BadData_extrafields:
                case FailedPaymentErrorCodeEnum.BadData_merchantaccept:
                case FailedPaymentErrorCodeEnum.BadData_mismatch:
                    return "BadData";
            }
        }

        private static PaymentProcessorResponseStatusEnum GetPaymentProcessorResponseStatusFromEnum(FailedPaymentErrorCodeEnum errorType)
        {
            switch (errorType)
            {
                default:
                case FailedPaymentErrorCodeEnum.Unknown:
                    return PaymentProcessorResponseStatusEnum.Unknown;
                case FailedPaymentErrorCodeEnum.ErrorType_cantconnect:
                case FailedPaymentErrorCodeEnum.ErrorType_dnsfailure:
                case FailedPaymentErrorCodeEnum.ErrorType_failtoprocess:
                case FailedPaymentErrorCodeEnum.ErrorType_linkfailure:
                case FailedPaymentErrorCodeEnum.ErrorType_notallowed:
                    return PaymentProcessorResponseStatusEnum.Error;
                case FailedPaymentErrorCodeEnum.DeclineType_velocity:
                case FailedPaymentErrorCodeEnum.DeclineType_fraud:
                case FailedPaymentErrorCodeEnum.DeclineType_expiredcard:
                case FailedPaymentErrorCodeEnum.DeclineType_decline:
                case FailedPaymentErrorCodeEnum.DeclineType_cvv:
                case FailedPaymentErrorCodeEnum.DeclineType_carderror:
                case FailedPaymentErrorCodeEnum.DeclineType_call:
                case FailedPaymentErrorCodeEnum.DeclineType_blacklist:
                case FailedPaymentErrorCodeEnum.DeclineType_avs:
                case FailedPaymentErrorCodeEnum.DeclineType_authexpired:
                    return PaymentProcessorResponseStatusEnum.Decline;
                case FailedPaymentErrorCodeEnum.ConnectionError_ApiDown:
                case FailedPaymentErrorCodeEnum.ResponseCode_102:
                case FailedPaymentErrorCodeEnum.ResponseCode_500:
                    return PaymentProcessorResponseStatusEnum.CallToProcessorFailed;
                case FailedPaymentErrorCodeEnum.ConnectionError_LinkFailure:
                    return PaymentProcessorResponseStatusEnum.LinkFailure;
                case FailedPaymentErrorCodeEnum.BadData_missingfields:
                case FailedPaymentErrorCodeEnum.BadData_badformat:
                case FailedPaymentErrorCodeEnum.BadData_badlength:
                case FailedPaymentErrorCodeEnum.BadData_extrafields:
                case FailedPaymentErrorCodeEnum.BadData_merchantaccept:
                case FailedPaymentErrorCodeEnum.BadData_mismatch:
                    return PaymentProcessorResponseStatusEnum.BadData;
            }
        }


    }
}
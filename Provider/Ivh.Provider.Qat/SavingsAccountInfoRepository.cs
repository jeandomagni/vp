﻿namespace Ivh.Provider.Qat
{
    using System.Collections.Generic;
    using System.Linq;
    using Common.Base.Enums;
    using Common.Data;
    using Common.Data.Connection.Connections;
    using Common.Data.Interfaces;
    using Common.VisitPay.Enums;
    using Domain.Qat.Entities;
    using Domain.Qat.Interfaces;
    using NHibernate;
    using NHibernate.Criterion;

    public class SavingsAccountInfoRepository : RepositoryBase<SavingsAccountInfo, VisitPay>, ISavingsAccountInfoRepository
    {
        public SavingsAccountInfoRepository(ISessionContext<VisitPay> sessionContext) : base(sessionContext)
        {
        }

        public IList<SavingsAccountInfo> GetSavingsAccountInfos(int vpguarantorId, IList<AccountStatusApiModelEnum> statuses, IList<AccountTypeFlagApiModelEnum> types)
        {
            ICriteria criteria = this.Session.CreateCriteria<SavingsAccountInfo>();
            criteria.Add(Restrictions.Eq(nameof(SavingsAccountInfo.VpGuarantorId), vpguarantorId));
            criteria.Add(Restrictions.In(nameof(SavingsAccountInfo.SavingsAccountStatusId), statuses.ToList()));
            criteria.Add(Restrictions.In(nameof(SavingsAccountInfo.SavingsAccountTypeId), types.ToList()));
            return criteria.List<SavingsAccountInfo>();
        }
    }
}

﻿namespace Ivh.Provider.Qat
{
    using System.Collections.Generic;
    using System.Linq;
    using Common.Data;
    using Common.Data.Connection.Connections;
    using Common.Data.Interfaces;
    using Domain.Qat.Interfaces;
    using Ivh.Domain.HospitalData.Visit.Entities;
    using NHibernate;

    public class QaHsVisitRepository : RepositoryBase<Visit, CdiEtl>, IQaHsVisitRepository
    {
        public QaHsVisitRepository(
            ISessionContext<CdiEtl> sessionContext,
            IStatelessSessionContext<CdiEtl> statelessSessionContext)
            : base(sessionContext, statelessSessionContext)
        {

        }

        public void UpdateQaVisitInsurancePlans(InsurancePlan insurancePlan)
        {
            IQueryable<Visit> visits = this.StatelessSession.Query<Visit>().Where(x => x.VisitInsurancePlans.Count > 0);

            foreach (Visit visit in visits)
            {
                visit.VisitInsurancePlans = new List<VisitInsurancePlan>
                {
                    new VisitInsurancePlan
                    {
                        InsurancePlan = insurancePlan
                    }
                };
                this.StatelessSession.Update(visit);
            }
        }

        //TODO: this doesnt really belong here
        public void UpdateQaEobPayor(int externalLinkId)
        {
            this.Session.CreateSQLQuery(@"exec [qa].[UpdateQaEobPayor] :externalLinkId")
                .SetInt32("externalLinkId", externalLinkId)
                .ExecuteUpdate();
        }

    }
}

﻿namespace Ivh.Provider.Qat.Modules
{
    using Autofac;
    using Domain.Qat.Interfaces;
    
    public class Module : Autofac.Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<QaCommunicationRepository>().As<IQaCommunicationRepository>();
            builder.RegisterType<QaPaymentRepository>().As<IQaPaymentRepository>();
            builder.RegisterType<VpGuarantorKeepCurrentRepository>().As<IVpGuarantorKeepCurrentRepository>();
            builder.RegisterType<HsGuarantorKeepCurrentRepository>().As<IHsGuarantorKeepCurrentRepository>();
            builder.RegisterType<IhcEnrollmentRepository>().As<IIhcEnrollmentRepository>();
            builder.RegisterType<MutableBaseVisitRepository>().As<IMutableBaseVisitRepository>();
            builder.RegisterType<MutableBaseHsGuarantorRepository>().As<IMutableBaseHsGuarantorRepository>();
        }
    }
}

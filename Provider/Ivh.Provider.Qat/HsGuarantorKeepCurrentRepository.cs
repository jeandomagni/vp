﻿namespace Ivh.Provider.Qat
{
    using System;
    using Common.Data.Connection.Connections;
    using Common.Data.Interfaces;
    using Domain.Qat.Entities;
    using Domain.Qat.Interfaces;
    using NHibernate;

    public class HsGuarantorKeepCurrentRepository : IHsGuarantorKeepCurrentRepository
    {
        private readonly ISession _session;

        public HsGuarantorKeepCurrentRepository(ISessionContext<CdiEtl> sessionContext)
        {
            this._session = sessionContext.Session;
        }

        public HsGuarantorKeepCurrent GetById(int hsGuarantorId)
        {
            ISQLQuery query = this._session.CreateSQLQuery("SELECT HsGuarantorId, KeepCurrentAnchor, KeepCurrentOffset FROM qa.HsGuarantorKeepCurrent WHERE HsGuarantorId = :HsGuarantorId");
            query.SetParameter("HsGuarantorId", hsGuarantorId);

            object[] result = (object[])query.UniqueResult();

            if (result != null)
            {
                return new HsGuarantorKeepCurrent
                {
                    KeepCurrentAnchor = result[1].ToString(),
                    KeepCurrentOffset = Convert.ToInt32(result[2]),
                    HsGuarantorId = Convert.ToInt32(result[0])
                };
            }

            return null;
        }

        public void Insert(HsGuarantorKeepCurrent entity)
        {
            HsGuarantorKeepCurrent e = this.GetById(entity.HsGuarantorId);
            if (e != null)
            {
                ISQLQuery query = this._session.CreateSQLQuery("UPDATE qa.HsGuarantorKeepCurrent SET KeepCurrentAnchor = :KeepCurrentAnchor, KeepCurrentOffset = :KeepCurrentOffset WHERE HsGuarantorId = :HsGuarantorId");
                query.SetParameter("KeepCurrentAnchor", entity.KeepCurrentAnchor);
                query.SetParameter("KeepCurrentOffset", entity.KeepCurrentOffset);
                query.SetParameter("HsGuarantorId", entity.HsGuarantorId);
                query.ExecuteUpdate();
            }
            else
            {
                ISQLQuery query = this._session.CreateSQLQuery("INSERT qa.HsGuarantorKeepCurrent (HsGuarantorId,KeepCurrentAnchor,KeepCurrentOffset) VALUES (:HsGuarantorId, :KeepCurrentAnchor,  :KeepCurrentOffset)");
                query.SetParameter("KeepCurrentAnchor", entity.KeepCurrentAnchor);
                query.SetParameter("KeepCurrentOffset", entity.KeepCurrentOffset);
                query.SetParameter("HsGuarantorId", entity.HsGuarantorId);
                query.ExecuteUpdate();
            }
        }
    }
}
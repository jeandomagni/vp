﻿namespace Ivh.Provider.Qat
{
    using Common.Data;
    using Common.Data.Connection.Connections;
    using Common.Data.Interfaces;
    using Domain.Qat.Entities;
    using Domain.Qat.Interfaces;

    public class MutableBaseHsGuarantorRepository : RepositoryBase<MutableBaseHsGuarantor, VisitPay>, IMutableBaseHsGuarantorRepository
    {
        public MutableBaseHsGuarantorRepository(ISessionContext<VisitPay> sessionContext) : base(sessionContext)
        {
        }
    }
}

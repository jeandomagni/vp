﻿namespace Ivh.Provider.Qat
{
    using System;
    using Common.Data.Connection.Connections;
    using Common.Data.Interfaces;
    using Domain.Qat.Entities;
    using Domain.Qat.Interfaces;
    using NHibernate;

    public class VpGuarantorKeepCurrentRepository : IVpGuarantorKeepCurrentRepository
    {
        private readonly ISession _session;

        public VpGuarantorKeepCurrentRepository(ISessionContext<VisitPay> sessionContext)
        {
            this._session = sessionContext.Session;
        }

        public VpGuarantorKeepCurrent GetById(int vpGuarantorId)
        {
            ISQLQuery query = this._session.CreateSQLQuery("SELECT VpGuarantorId, KeepCurrentAnchor, KeepCurrentOffset FROM qa.VpGuarantorKeepCurrent WHERE VpGuarantorId = :VpGuarantorId");
            query.SetParameter("VpGuarantorId", vpGuarantorId);

            object[] result = (object[])query.UniqueResult();

            if (result != null)
            {
                return new VpGuarantorKeepCurrent
                {
                    KeepCurrentAnchor = result[1].ToString(),
                    KeepCurrentOffset = Convert.ToInt32(result[2]),
                    VpGuarantorId = Convert.ToInt32(result[0])
                };
            }

            return null;
        }

        public void Insert(VpGuarantorKeepCurrent entity)
        {
            VpGuarantorKeepCurrent e = this.GetById(entity.VpGuarantorId);
            if (e != null)
            {
                ISQLQuery query = this._session.CreateSQLQuery("UPDATE qa.VpGuarantorKeepCurrent SET KeepCurrentAnchor = :KeepCurrentAnchor, KeepCurrentOffset = :KeepCurrentOffset WHERE VpGuarantorId = :VpGuarantorId");
                query.SetParameter("KeepCurrentAnchor", entity.KeepCurrentAnchor);
                query.SetParameter("KeepCurrentOffset", entity.KeepCurrentOffset);
                query.SetParameter("VpGuarantorId", entity.VpGuarantorId);
                query.ExecuteUpdate();
            }
            else
            {
                ISQLQuery query = this._session.CreateSQLQuery("INSERT qa.VpGuarantorKeepCurrent (VpGuarantorId,KeepCurrentAnchor,KeepCurrentOffset) VALUES (:VpGuarantorId, :KeepCurrentAnchor,  :KeepCurrentOffset)");
                query.SetParameter("KeepCurrentAnchor", entity.KeepCurrentAnchor);
                query.SetParameter("KeepCurrentOffset", entity.KeepCurrentOffset);
                query.SetParameter("VpGuarantorId", entity.VpGuarantorId);
                query.ExecuteUpdate();
            }
        }
    }
}
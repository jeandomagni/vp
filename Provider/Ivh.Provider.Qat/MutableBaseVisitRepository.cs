﻿namespace Ivh.Provider.Qat
{
    using Common.Data;
    using Common.Data.Connection.Connections;
    using Common.Data.Interfaces;
    using Domain.Qat.Entities;
    using Domain.Qat.Interfaces;

    public class MutableBaseVisitRepository : RepositoryBase<MutableBaseVisit, VisitPay>, IMutableBaseVisitRepository
    {
        public MutableBaseVisitRepository(ISessionContext<VisitPay> sessionContext) : base(sessionContext)
        {
        }

    }
}

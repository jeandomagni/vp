﻿namespace Ivh.Provider.Qat
{
    using Common.Data;
    using Common.Data.Connection.Connections;
    using Common.Data.Interfaces;
    using Domain.Email.Entities;
    using Domain.Qat.Interfaces;
    using NHibernate;

    public class QaCommunicationRepository : RepositoryBase<Communication, VisitPay>, IQaCommunicationRepository
    {
        public QaCommunicationRepository(ISessionContext<VisitPay> sessionContext) : base(sessionContext)
        {
        }
    }
}
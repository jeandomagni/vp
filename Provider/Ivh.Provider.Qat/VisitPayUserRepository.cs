﻿namespace Ivh.Provider.Qat
{
    using Common.Data;
    using Common.Data.Connection.Connections;
    using Common.Data.Interfaces;
    using Domain.Qat.Interfaces;
    using Domain.User.Entities;
    using NHibernate;

    public class QaVisitPayUserRepository:RepositoryBase<VisitPayUser, VisitPay>, IQaVisitPayUserRepository
    {
        public QaVisitPayUserRepository(ISessionContext<VisitPay> sessionContext) : base(sessionContext)
        {
        }

    }
}

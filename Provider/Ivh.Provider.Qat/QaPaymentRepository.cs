﻿namespace Ivh.Provider.Qat
{
    using Common.Data;
    using Common.Data.Connection.Connections;
    using Common.Data.Interfaces;
    using Domain.FinanceManagement.Payment.Entities;
    using Domain.Qat.Interfaces;
    using NHibernate;

    public class QaPaymentRepository : RepositoryBase<Payment, VisitPay>, IQaPaymentRepository
    {
        public QaPaymentRepository(ISessionContext<VisitPay> sessionContext) : base(sessionContext)
        {
        }
    }
}
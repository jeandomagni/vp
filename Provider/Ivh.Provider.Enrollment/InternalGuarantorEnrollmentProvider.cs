﻿namespace Ivh.Provider.Enrollment
{
    using System;
    using System.Collections.Generic;
    using System.Net.Http;
    using System.Threading.Tasks;
    using Common.Api.Client;
    using Common.Api.Helpers;
    using Common.ServiceBus.Interfaces;
    using Common.VisitPay.Enums;
    using Common.VisitPay.Messages.Core;
    using Common.VisitPay.Messages.HospitalData.Dtos;
    using Domain.Logging.Interfaces;
    using Domain.Settings.Enums;
    using Domain.Settings.Interfaces;
    using Ivh.Domain.Guarantor.Interfaces;

    public class InternalGuarantorEnrollmentProvider : IGuarantorEnrollmentProvider
    {
        private readonly Lazy<IBus> _bus;
        private readonly Lazy<IApplicationSettingsService> _applicationSettingsService;
        private readonly Lazy<ILoggingProvider> _logger;
        private readonly Lazy<Ivh.Domain.HospitalData.HsGuarantor.Interfaces.IHsGuarantorService> _hsGuarantorService;

        public InternalGuarantorEnrollmentProvider(
            Lazy<IBus> bus,
            Lazy<IApplicationSettingsService> applicationSettingsService,
            Lazy<Ivh.Domain.HospitalData.HsGuarantor.Interfaces.IHsGuarantorService> hsGuarantorService,
            Lazy<ILoggingProvider> logger)
        {
            this._bus = bus;
            this._applicationSettingsService = applicationSettingsService;
            this._logger = logger;
            this._hsGuarantorService = hsGuarantorService;
        }
        private HttpClient GenerateHttpClient()
        {
            return HttpClientFactory.Create(new ApiHandler(this._applicationSettingsService.Value.ClientDataApiAppId.Value, this._applicationSettingsService.Value.ClientDataApiAppKey.Value, this._applicationSettingsService.Value.ApplicationName.Value));
        }

        public async Task<EnrollmentResponseDto> EnrollGuarantorAsync(string guarantorIdToEnroll, int billingSystemId, int vpGuarantor)
        {
            return await Task.Run(() =>
            {
                IList<string> result = this._hsGuarantorService.Value.GetEligibleVisitsAfterEnrollment(guarantorIdToEnroll, billingSystemId);

                EnrollmentResponseDto returnObj = new EnrollmentResponseDto() {Accounts = new List<EnrollmentAccountResponseDto>(), EnrollmentResult = EnrollmentResultEnum.Success, EnrollmentStatus = true, GuarantorBillingId = billingSystemId, GuarantorSourceSystemKey = guarantorIdToEnroll, RawResponse = null};
                foreach (string visitSsk in result)
                {
                    returnObj.Accounts.Add(new EnrollmentAccountResponseDto() {eligiblitStatus = EnrollmentAccountEligibilityEnum.Eligible, AccountMatchString = visitSsk, facilityId = null, originatingSystem = null});
                }

                return returnObj;
            });
        }

        public async Task<EnrollmentResponseDto> UnEnrollGuarantorAsync(string guarantorIdToEnroll)
        {
            return await Task.Run(() => new EnrollmentResponseDto { EnrollmentResult = EnrollmentResultEnum.Success });
        }

        public async Task PublishEnrollGuarantorMessageAsync(int vpGuarantorId)
        {
            await this._bus.Value.PublishMessage(new EnrollGuarantorMessage
            {
                VpGuarantorId = vpGuarantorId
            });
        }

        public async Task PublishUnEnrollGuarantorMessageAsync(int vpGuarantorId)
        {
            await Task.Run(() => false);
            //Do nothing for this implementation
        }
    }
}
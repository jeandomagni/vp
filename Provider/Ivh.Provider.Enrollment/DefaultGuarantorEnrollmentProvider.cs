﻿namespace Ivh.Provider.Enrollment
{
    using System;
    using System.Threading.Tasks;
    using Common.Base.Utilities.Helpers;
    using Common.ServiceBus.Interfaces;
    using Common.VisitPay.Enums;
    using Common.VisitPay.Messages.FinanceManagement;
    using Common.VisitPay.Messages.HospitalData.Dtos;
    using Domain.Guarantor.Interfaces;

    public class DefaultGuarantorEnrollmentProvider : IGuarantorEnrollmentProvider
    {
        private readonly Lazy<IBus> _bus;
        private readonly Lazy<TimeZoneHelper> _timeZoneOffset;

        public DefaultGuarantorEnrollmentProvider(
            Lazy<IBus> bus, 
            Lazy<TimeZoneHelper> timeZoneOffset)
        {
            this._bus = bus;
            this._timeZoneOffset = timeZoneOffset;
        }

        public async Task<EnrollmentResponseDto> EnrollGuarantorAsync(string guarantorIdToEnroll, int billingSystemId, int vpGuarantor)
        {
            return await Task.Run(() => default(EnrollmentResponseDto));
        }

        public async Task<EnrollmentResponseDto> UnEnrollGuarantorAsync(string guarantorIdToEnroll)
        {
            return await Task.Run(() => new EnrollmentResponseDto {EnrollmentResult = EnrollmentResultEnum.Success});
        }

        public async Task PublishEnrollGuarantorMessageAsync(int vpGuarantorId)
        {
            await this._bus.Value.PublishMessage(new CreateStatementMessage
            {
                VpGuarantorId = vpGuarantorId,
                DateToProcess = this._timeZoneOffset.Value.Client.Now, ImmediateStatement = true
            });
        }

        public async Task PublishUnEnrollGuarantorMessageAsync(int vpGuarantorId)
        {
            await Task.Run(() => false);//this does nothing
        }
    }
}
﻿namespace Ivh.Provider.Eob.Modules
{
    using Autofac;
    using Domain.Eob.Interfaces;

    public class Module : Autofac.Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<ClaimPaymentInformationRepository>().As<IClaimPaymentInformationRepository>();
            builder.RegisterType<EobPayerFilterRepository>().As<IEobPayerFilterRepository>();
            builder.RegisterType<EobPayerFilterEobExternalLinkRepository>().As<IEobPayerFilterEobExternalLinkRepository>();
            builder.RegisterType<EobExternalLinkRepository>().As<IEobExternalLinkRepository>();
            builder.RegisterType<EobDisplayCategoryRepository>().As<IEobDisplayCategoryRepository>();
            builder.RegisterType<EobClaimAdjustmentReasonCodeRepository>().As<IEobClaimAdjustmentReasonCodeRepository>();
            builder.RegisterType<EobClaimAdjustmentReasonCodeDisplayMapRepository>().As<IEobClaimAdjustmentReasonCodeDisplayMapRepository>();
        }
    }
}

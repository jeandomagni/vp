﻿namespace Ivh.Provider.Eob
{
    using System.Collections.Generic;
    using System.Linq;
    using Common.Data;
    using Common.Data.Connection.Connections;
    using Common.Data.Interfaces;
    using Domain.Eob.Entities;
    using Domain.Eob.Interfaces;
    using NHibernate.Criterion;

    public class EobClaimAdjustmentReasonCodeDisplayMapRepository : RepositoryBase<EobClaimAdjustmentReasonCodeDisplayMap, VisitPay>, IEobClaimAdjustmentReasonCodeDisplayMapRepository
    {
        public EobClaimAdjustmentReasonCodeDisplayMapRepository(ISessionContext<VisitPay> sessionContext)
            : base(sessionContext)
        {
        }

        public void SaveEobClaimAdjustmentReasonCodeDisplayMap(EobClaimAdjustmentReasonCodeDisplayMap eobClaimAdjustmentReasonCodeDisplayMap)
        {
            EobClaimAdjustmentReasonCodeDisplayMap existing = this.GetById(eobClaimAdjustmentReasonCodeDisplayMap.EobClaimAdjustmentReasonCodeDisplayMapId);
            if (existing == null)
            {
                this.Insert(eobClaimAdjustmentReasonCodeDisplayMap);
            }
            else
            {
                //Populate existing from the different instance that was passed in
                existing.ClaimAdjustmentReasonCode = eobClaimAdjustmentReasonCodeDisplayMap.ClaimAdjustmentReasonCode;
                existing.EobDisplayCategoryId = eobClaimAdjustmentReasonCodeDisplayMap.EobDisplayCategoryId;
                existing.UniquePayerValue = eobClaimAdjustmentReasonCodeDisplayMap.UniquePayerValue;
                existing.ClaimAdjustmentGroupCode = eobClaimAdjustmentReasonCodeDisplayMap.ClaimAdjustmentGroupCode;
                existing.ClaimStatusCode = eobClaimAdjustmentReasonCodeDisplayMap.ClaimStatusCode;

                this.Update(existing);
            }
        }

        public int? GetEobDisplayCategoryId(string claimAdjustmentReasonCode, string uniquePayerValue, string claimStatusCode, string claimAdjustmentGroupCode)
        {
            List<EobClaimAdjustmentReasonCodeDisplayMap> mapList = this.GetQueryable().ToList();

            EobClaimAdjustmentReasonCodeDisplayMap map;

            // Try finding by Payer first
            map = this.GetMostSpecificMap(mapList, uniquePayerValue, claimAdjustmentReasonCode, claimStatusCode, claimAdjustmentGroupCode);

            //Then try finding without Payer
            if (map == null)
            {
                map = this.GetMostSpecificMap(mapList, null, claimAdjustmentReasonCode, claimStatusCode, claimAdjustmentGroupCode);
            }

            return map?.EobDisplayCategoryId;
        }

        private EobClaimAdjustmentReasonCodeDisplayMap GetMostSpecificMap(List<EobClaimAdjustmentReasonCodeDisplayMap> mapList, string uniquePayerValue, string claimAdjustmentReasonCode, string claimStatusCode, string claimAdjustmentGroupCode)
        {
            EobClaimAdjustmentReasonCodeDisplayMap map = null;

            // Most specific match
            map = mapList.FirstOrDefault(x => x.ClaimAdjustmentReasonCode == claimAdjustmentReasonCode
                && x.UniquePayerValue == uniquePayerValue
                && x.ClaimAdjustmentGroupCode == claimAdjustmentGroupCode
                && x.ClaimStatusCode == claimStatusCode
            );

            // Drop ClaimStatusCode
            if (map == null)
            {
                map = mapList.FirstOrDefault(x => x.ClaimAdjustmentReasonCode == claimAdjustmentReasonCode
                    && x.UniquePayerValue == uniquePayerValue
                    && x.ClaimAdjustmentGroupCode == claimAdjustmentGroupCode
                    && x.ClaimStatusCode == null
                );
            }

            // Drop ClaimAdjustmentGroupCode
            if (map == null)
            {
                map = mapList.FirstOrDefault(x => x.ClaimAdjustmentReasonCode == claimAdjustmentReasonCode
                    && x.UniquePayerValue == uniquePayerValue
                    && x.ClaimAdjustmentGroupCode == null
                    && x.ClaimStatusCode == claimStatusCode
                );
            }

            // Drop ClaimStatusCode & ClaimAdjustmentGroupCode
            if (map == null)
            {
                map = mapList.FirstOrDefault(x => x.ClaimAdjustmentReasonCode == claimAdjustmentReasonCode
                    && x.UniquePayerValue == uniquePayerValue
                    && x.ClaimAdjustmentGroupCode == null
                    && x.ClaimStatusCode == null
                );
            }

            return map;
        }
    }
}

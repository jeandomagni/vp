﻿namespace Ivh.Provider.Eob
{
    using Ivh.Common.Data;
    using Ivh.Domain.Eob.Entities;
    using Ivh.Domain.Eob.Interfaces;
    using System.Collections.Generic;
    using System.Linq;
    using Common.Data.Connection.Connections;
    using Common.Data.Interfaces;
    using NHibernate.Criterion;
    using NHibernate.SqlCommand;
    using NHibernate.Transform;

    public class ClaimPaymentInformationRepository : RepositoryBase<ClaimPaymentInformationHeader, VisitPay>, IClaimPaymentInformationRepository
    {

        public ClaimPaymentInformationRepository(ISessionContext<VisitPay> sessionContext)
            : base(sessionContext)
        {
        }

        private IList<Eob835Result> GetEob835ResultList(int billingSystemId, string visitSourceSystemKey)
        {
            IList<Eob835Result> eobResultList = this.Session.CreateCriteria<ClaimPaymentInformationHeader>("cpi")
                .CreateCriteria("cpi.EobPayerFilter", "filter", JoinType.InnerJoin)
                .Add(Restrictions.Eq("cpi.VisitSourceSystemKey", visitSourceSystemKey))
                .Add(Restrictions.Eq("cpi.BillingSystemId", billingSystemId))
                .Add(Restrictions.Eq("filter.IsActive", true))
                .AddOrder(Order.Desc("cpi.TransactionDate"))
                .AddOrder(Order.Desc("cpi.TransactionSetLineNumber"))
                .SetProjection(
                    Projections.ProjectionList()
                    .Add(Projections.Property("cpi.ClaimPaymentInformationId"), "TransactionSetHeaderTrailerId")
                    .Add(Projections.Property("cpi.ClaimPaymentInformationId"), "HeaderNumberId")
                    .Add(Projections.Property("cpi.ClaimPaymentInformationId"), "ClaimPaymentInformationId")
                    .Add(Projections.Property("cpi.TransactionDate"), "TransactionDate")
                    .Add(Projections.Property("cpi.TransactionSetLineNumber"), "HeaderNumber")
                    .Add(Projections.Property("filter.EobPayerFilterId"), "EobPayerFilterId")
                )
                .SetResultTransformer(Transformers.AliasToBean(typeof(Eob835Result)))
                .List<Eob835Result>();

            return eobResultList;
        }

        public IList<Eob835Result> Get835Remits(int billingSystemId, string visitSourceSystemKey)
        {

            IList<Eob835Result> eobResults = this.GetEob835ResultList(billingSystemId, visitSourceSystemKey).ToList();

            int[] claimPaymentIds = eobResults.Select(x => x.ClaimPaymentInformationId).ToArray();

            //Get rows with JSON serialized ClaimPaymentInformation data
            IList<ClaimPaymentInformationHeader> clpList = this.Session.CreateCriteria<ClaimPaymentInformationHeader>("cpi")
                .Add(Restrictions.In("cpi.ClaimPaymentInformationId", claimPaymentIds))
                .List<ClaimPaymentInformationHeader>();

            //Get rows with EobPayerFilterEobExternalLink data
            int[] eobPayerFilterIds = eobResults.Select(x => x.EobPayerFilterId).ToArray();
            IList<EobPayerFilterEobExternalLink> payerFilters = this.Session.CreateCriteria<EobPayerFilterEobExternalLink>("pfel")
                .Add(Restrictions.In("pfel.EobPayerFilter.EobPayerFilterId", eobPayerFilterIds))
                .List<EobPayerFilterEobExternalLink>();

            //Assign rows to empty ClaimPaymentInformations and EobPayerFilterEobExternalLinks collections in Eob835Result
            foreach (Eob835Result result in eobResults)
            {
                result.ClaimPaymentInformations =
                    clpList.Where(x => x.ClaimPaymentInformationId == result.ClaimPaymentInformationId).Select(x => x.ClaimPaymentInformation); // && x.HeaderNumberId == result.HeaderNumberId);
                result.EobPayerFilterEobExternalLinks =
                    payerFilters.Where(x => x.EobPayerFilter.EobPayerFilterId == result.EobPayerFilterId).ToList();
            }
            
            // VP-4883 Distinct() should filter out duplicates that only vary by the carrier's internal control number
            return eobResults.Distinct().ToList();
        }

        /// <summary>
        /// Returns a list of visits that have EOB data
        /// </summary>
        /// <param name="visits">Key = BillingSystemId, Value = VisitSourceSystemKey</param>
        /// <returns></returns>
        public IList<EobIndicatorResult> Get835RemitIndicators(IDictionary<string, int> visits)
        {
            // This uses IQueryable<>, so the SQL query generated will only contain columns and tables referenced in the LINQ query
            // Modify with caution due to NHibernate's poor LINQ to SQL implementation
            List<EobIndicatorResult> remitIndicators =
                (
                    from claimPaymentInformation in this.GetQueryable()
                    where visits.Values.ToArray().Contains(claimPaymentInformation.BillingSystemId)
                        && visits.Keys.ToArray().Contains(claimPaymentInformation.VisitSourceSystemKey)
                        && claimPaymentInformation.EobPayerFilter.IsActive
                    select new EobIndicatorResult
                    {
                        BillingSystemId = claimPaymentInformation.BillingSystemId,
                        VisitSourceSystemKey = claimPaymentInformation.VisitSourceSystemKey,
                        HasEob = true,
                        Bpr16Date = claimPaymentInformation.TransactionDate,
                        Lx01AssignedNumber = claimPaymentInformation.TransactionSetLineNumber,
                        EobPayerFilterEobExternalLinks = (
                            from eobPayerFilterExternalLink in claimPaymentInformation.EobPayerFilter.EobPayerFilterEobExternalLinks
                            select eobPayerFilterExternalLink
                        ).ToList()
                    }
                ).ToList();

            return remitIndicators;
        }

        public bool Has835Remits(int billingSystemId, string visitSourceSystemKey)
        {
            return this.GetEob835ResultList(billingSystemId, visitSourceSystemKey).Any();
        }

        public void SaveClaimPaymentInformationHeader(ClaimPaymentInformationHeader claimPaymentInformationHeader)
        {
            this.Insert(claimPaymentInformationHeader);
        }
    }
}

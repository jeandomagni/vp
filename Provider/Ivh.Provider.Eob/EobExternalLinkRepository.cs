﻿namespace Ivh.Provider.Eob
{
    using Common.Data;
    using Common.Data.Connection.Connections;
    using Common.Data.Interfaces;
    using Domain.Eob.Entities;
    using Domain.Eob.Interfaces;

    public class EobExternalLinkRepository : RepositoryBase<EobExternalLink, VisitPay>, IEobExternalLinkRepository
    {
        public EobExternalLinkRepository(ISessionContext<VisitPay> sessionContext)
            : base(sessionContext)
        {
        }

        public void SaveEobExternalLink(EobExternalLink eobExternalLink)
        {
            EobExternalLink existing = this.GetById(eobExternalLink.EobExternalLinkId);
            if (existing == null)
            {
                this.Insert(eobExternalLink);
            } else
            {
                //Populate existing from the different instance that was passed in
                existing.ClaimNumberLocation = eobExternalLink.ClaimNumberLocation;
                existing.Icon = eobExternalLink.Icon;
                existing.PersistDays = eobExternalLink.PersistDays;
                existing.Text = eobExternalLink.Text;
                existing.Url = eobExternalLink.Url;

                this.Update(existing);
            } 
        }
    }
}

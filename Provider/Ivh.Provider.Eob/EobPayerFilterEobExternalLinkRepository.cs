﻿namespace Ivh.Provider.Eob
{
    using Common.Data;
    using Common.Data.Connection.Connections;
    using Common.Data.Interfaces;
    using Domain.Eob.Entities;
    using Domain.Eob.Interfaces;

    public class EobPayerFilterEobExternalLinkRepository : RepositoryBase<EobPayerFilterEobExternalLink, VisitPay>, IEobPayerFilterEobExternalLinkRepository
    {
        public EobPayerFilterEobExternalLinkRepository(ISessionContext<VisitPay> sessionContext)
            : base(sessionContext)
        {
        }

        public void SaveEobPayerFilterExternalLink(EobPayerFilterEobExternalLink eobPayerFilterEobExternalLink)
        {
            EobPayerFilterEobExternalLink existing = this.GetById(eobPayerFilterEobExternalLink.EobPayerFilterEobExternalLinkId);
            if (existing == null)
            {
                this.Insert(eobPayerFilterEobExternalLink);
            }
            else
            {
                //Populate existing from the different instance that was passed in
                existing.EobExternalLinkId = eobPayerFilterEobExternalLink.EobExternalLinkId;
                existing.EobPayerFilterId = eobPayerFilterEobExternalLink.EobPayerFilterId;
                existing.InsurancePlanBillingSystemId = eobPayerFilterEobExternalLink.InsurancePlanBillingSystemId;
                existing.InsurancePlanSourceSystemKey = eobPayerFilterEobExternalLink.InsurancePlanSourceSystemKey;

                this.Update(existing);
            }
        }
    }
}

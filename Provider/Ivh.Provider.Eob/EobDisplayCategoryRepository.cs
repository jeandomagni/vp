﻿namespace Ivh.Provider.Eob
{
    using Common.Data;
    using Common.Data.Connection.Connections;
    using Common.Data.Interfaces;
    using Domain.Eob.Entities;
    using Domain.Eob.Interfaces;

    public class EobDisplayCategoryRepository : RepositoryBase<EobDisplayCategory, VisitPay>, IEobDisplayCategoryRepository
    {
        public EobDisplayCategoryRepository(ISessionContext<VisitPay> sessionContext)
            : base(sessionContext)
        {
        }

        public void SaveEobDisplayCategory(EobDisplayCategory eobDisplayCategory)
        {
            EobDisplayCategory existing = this.GetById(eobDisplayCategory.EobDisplayCategoryId);
            if (existing == null)
            {
                this.Insert(eobDisplayCategory);
            } else
            {
                //Populate existing from the different instance that was passed in
                existing.DisplayCategoryName = eobDisplayCategory.DisplayCategoryName;

                this.Update(existing);
            } 
        }
    }
}

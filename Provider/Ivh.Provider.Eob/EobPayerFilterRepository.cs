﻿namespace Ivh.Provider.Eob
{
    using Common.Data;
    using Common.Data.Connection.Connections;
    using Common.Data.Interfaces;
    using Domain.Eob.Entities;
    using Domain.Eob.Interfaces;

    public class EobPayerFilterRepository : RepositoryBase<EobPayerFilter, VisitPay>, IEobPayerFilterRepository
    {
        public EobPayerFilterRepository(ISessionContext<VisitPay> sessionContext)
            : base(sessionContext)
        {
        }
        
        public void SaveEobPayerFilter(EobPayerFilter eobPayerFilter)
        {
            EobPayerFilter existing = this.GetById(eobPayerFilter.EobPayerFilterId);
            if (existing == null)
            {
                this.Insert(eobPayerFilter);
            } else
            {
                //Populate existing from the different instance that was passed in
                existing.IsActive = eobPayerFilter.IsActive;
                existing.UniquePayerValue = eobPayerFilter.UniquePayerValue;

                this.Update(existing);
            }          
        }
    }
}

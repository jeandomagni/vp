﻿namespace Ivh.Provider.Eob
{
    using Common.Data;
    using Common.Data.Connection.Connections;
    using Common.Data.Interfaces;
    using Domain.Eob.Entities;
    using Domain.Eob.Interfaces;

    public class EobClaimAdjustmentReasonCodeRepository : RepositoryBase<EobClaimAdjustmentReasonCode, VisitPay>, IEobClaimAdjustmentReasonCodeRepository
    {
        public EobClaimAdjustmentReasonCodeRepository(ISessionContext<VisitPay> sessionContext)
            : base(sessionContext)
        {
        }

        public void SaveEobClaimAdjustmentReasonCode(EobClaimAdjustmentReasonCode eobClaimAdjustmentReasonCode)
        {
            EobClaimAdjustmentReasonCode existing = this.GetById(eobClaimAdjustmentReasonCode.EobClaimAdjustmentReasonCodeId);
            if (existing == null)
            {
                this.Insert(eobClaimAdjustmentReasonCode);
            } else
            {
                //Populate existing from the different instance that was passed in
                existing.ClaimAdjustmentReasonCode = eobClaimAdjustmentReasonCode.ClaimAdjustmentReasonCode;
                existing.Description = eobClaimAdjustmentReasonCode.Description;
                existing.StartDate = eobClaimAdjustmentReasonCode.StartDate;
                existing.EndDate = eobClaimAdjustmentReasonCode.EndDate;

                this.Update(existing);
            } 
        }
    }
}

﻿namespace Ivh.Provider.FinanceManagement.Ach.Modules
{
    using Autofac;
    using Domain.FinanceManagement.Ach.Interfaces;

    public class Module : Autofac.Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<AchFileRepository>().As<IAchFileRepository>();
            builder.RegisterType<AchReturnDetailRepository>().As<IAchReturnDetailRepository>();
            builder.RegisterType<AchSettlementDetailRepository>().As<IAchSettlementDetailRepository>();
        }
    }
}
﻿namespace Ivh.Provider.FinanceManagement.Ach
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Text;
    using System.Xml.Serialization;
    using Common.Base.Constants;
    using Common.Base.Utilities.Extensions;
    using Common.Base.Utilities.Helpers;
    using Common.VisitPay.Constants;
    using Domain.FinanceManagement.Ach.Entities;
    using Domain.FinanceManagement.Ach.Interfaces;
    using Domain.FinanceManagement.Payment.Entities;
    using Domain.FinanceManagement.Payment.Interfaces;
    using Domain.Logging.Interfaces;
    using Domain.Settings.Entities;
    using Domain.Settings.Interfaces;

    public class AchProvider : IAchProvider
    {
        private const string DateFormat = "yyyyMMdd";
        private readonly Lazy<IApplicationSettingsService> _applicationSettingsService;
        private readonly Lazy<INachaGateway> _nachaGateway;
        private readonly Lazy<IMerchantAccountAllocationService> _merchantAccountAllocationService;
        private readonly Lazy<ILoggingService> _loggingService;

        public AchProvider(
            Lazy<IApplicationSettingsService> applicationSettingsService, 
            Lazy<INachaGateway> nachaGateway, 
            Lazy<IMerchantAccountAllocationService> merchantAccountAllocationService, 
            Lazy<ILoggingService> loggingService)
        {
            this._applicationSettingsService = applicationSettingsService;
            this._nachaGateway = nachaGateway;
            this._merchantAccountAllocationService = merchantAccountAllocationService;
            this._loggingService = loggingService;
        }

        /// <summary>
        /// </summary>
        /// <param name="startDate"></param>
        /// <param name="endDate"></param>
        /// <returns></returns>
        public IList<Response> DownloadReturnsFiles(DateTime startDate, DateTime endDate)
        {
            IList<Response> responses = new List<Response>();
            foreach (MerchantAccount merchantAccount in this._merchantAccountAllocationService.Value.GetAllMerchantAccounts())
            {
                PaymentProviderCredentials paymentProviderCredentials = this._applicationSettingsService.Value.GetPaymentProviderCredentials(
                    merchantAccount.CustomerIdSettingsKey,
                    merchantAccount.PasswordSettingsKey,
                    merchantAccount.PasswordQueryApiSettingsKey,
                    merchantAccount.AchCustomerIdSettingsKey,
                    merchantAccount.AchPasswordSettingsKey);

                string nachaId = paymentProviderCredentials.NachaId;
                string nachaSecurityToken = paymentProviderCredentials.NachaSecurityToken;

                try
                {
                    string result = this._nachaGateway.Value.GetReturnFile(
                        nachaId,
                        nachaSecurityToken,
                        startDate.Date.ToString(DateFormat),
                        endDate.Date.ToString(DateFormat),
                        AchFileFormats.Csv);

                    responses.Add(DeserializeResponse(result));
                }
                catch (Exception ex)
                {
                    this._loggingService.Value.Fatal(() => $"Unable to download ACH Return File for Merchant Account: {merchantAccount.Account} ({merchantAccount.Description}) Exception: {ExceptionHelper.AggregateExceptionToString(ex)}");
                }
            }
            return responses;
        }

        /// <summary>
        /// </summary>
        /// <param name="startDate"></param>
        /// <param name="endDate"></param>
        /// <returns></returns>
        public IList<Response> DownloadSettlementsFiles(DateTime startDate, DateTime endDate)
        {
            IList<Response> responses = new List<Response>();
            foreach (MerchantAccount merchantAccount in this._merchantAccountAllocationService.Value.GetAllMerchantAccounts())
            {
                PaymentProviderCredentials paymentProviderCredentials = this._applicationSettingsService.Value.GetPaymentProviderCredentials(
                    merchantAccount.CustomerIdSettingsKey,
                    merchantAccount.PasswordSettingsKey,
                    merchantAccount.PasswordQueryApiSettingsKey,
                    merchantAccount.AchCustomerIdSettingsKey,
                    merchantAccount.AchPasswordSettingsKey);

                string nachaId = paymentProviderCredentials.NachaId;
                string nachaSecurityToken = paymentProviderCredentials.NachaSecurityToken;

                try
                {
                    string result = this._nachaGateway.Value.GetReport(
                        nachaId,
                        nachaSecurityToken,
                        AchReportTypeId.SettlementReport,
                        null,
                        null,
                        startDate.Date.ToString(DateFormat),
                        endDate.Date.ToString(DateFormat),
                        AchDateTypeId.SettlementDate,
                        null,
                        null,
                        AchFileFormats.Csv);

                    responses.Add(DeserializeResponse(result, true));
                }
                catch (Exception ex)
                {
                    this._loggingService.Value.Fatal(() => $"Unable to download ACH Settlement File for Merchant Account: {merchantAccount.Account} ({merchantAccount.Description}) Exception: {ExceptionHelper.AggregateExceptionToString(ex)}");
                }
            }
            return responses;
        }

        private static Response DeserializeResponse(string responseString, bool base64Decode = false)
        {
            if (base64Decode)
            {
                if (!string.IsNullOrEmpty(responseString) && responseString.IsBase64Encoded())
                {
                    byte[] decodedBytes = Convert.FromBase64String(responseString);

                    using (MemoryStream stream = new MemoryStream(decodedBytes, 0, decodedBytes.Length))
                    {
                        stream.Position = 0;
                        StreamReader sr = new StreamReader(stream);
                        return new Response() { ValueRaw = responseString = sr.ReadToEnd(), Code = "000" };
                    }
                }
            }

            XmlSerializer deserializer = new XmlSerializer(typeof(Response));
            MemoryStream memStream = new MemoryStream(Encoding.UTF8.GetBytes(responseString));
            Response response = (Response)deserializer.Deserialize(memStream);
            return response;
        }
    }
}
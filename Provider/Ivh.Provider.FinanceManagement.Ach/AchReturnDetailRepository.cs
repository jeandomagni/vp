﻿namespace Ivh.Provider.FinanceManagement.Ach
{
    using System;
    using System.Collections.Generic;
    using Common.Data;
    using Common.Data.Connection.Connections;
    using Common.Data.Interfaces;
    using Domain.FinanceManagement.Ach.Entities;
    using Domain.FinanceManagement.Ach.Interfaces;
    using NHibernate;

    public class AchReturnDetailRepository : RepositoryBase<AchReturnDetail, VisitPay>, IAchReturnDetailRepository
    {
        public AchReturnDetailRepository(ISessionContext<VisitPay> sessionContext)
            : base(sessionContext)
        {
        }

        public IReadOnlyList<AchReturnDetail> GetDetailsForFile(int achFileId)
        {
            IQueryOver<AchReturnDetail, AchReturnDetail> query = this.Session.QueryOver<AchReturnDetail>();
            query.Where(w => w.AchFile == new AchFile() { AchFileId = achFileId });
            return (IReadOnlyList<AchReturnDetail>)query.List<AchReturnDetail>();
        }

        public IReadOnlyList<AchReturnDetail> GetReturnDetails(DateTime startDate, DateTime endDate, bool unprocessedOnly = false)
        {
            IQueryOver<AchReturnDetail, AchReturnDetail> query = this.Session.QueryOver<AchReturnDetail>();
            query.Where(w => w.DownloadedDate >= startDate.Date && w.DownloadedDate < endDate.Date);
            if (unprocessedOnly)
            {
                query.And(w => w.ProcessedDate == null);
            }
            return (IReadOnlyList<AchReturnDetail>)query.List<AchReturnDetail>();
        }

        public int GetReturnDetailsCount(AchFile achFile, bool unprocessedOnly = false)
        {
            IQueryOver<AchReturnDetail, AchReturnDetail> query = this.Session.QueryOver<AchReturnDetail>();
            query.Where(w => w.AchFile == achFile);
            if (unprocessedOnly)
            {
                query.And(w => w.ProcessedDate == null);
            }
            return query.RowCount();
        }
    }
}
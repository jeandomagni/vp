﻿namespace Ivh.Provider.FinanceManagement.Ach
{
    using System.Collections.Generic;
    using Domain.FinanceManagement.Ach.Entities;
    using Domain.FinanceManagement.Ach.Interfaces;

    public class ThrowsErrorsAchProvider : IAchProvider
    {

        public IList<Response> DownloadReturnsFiles(System.DateTime startDate, System.DateTime endDate)
        {
            throw new System.NotImplementedException();
        }

        public IList<Response> DownloadSettlementsFiles(System.DateTime startDate, System.DateTime endDate)
        {
            throw new System.NotImplementedException();
        }
    }
}
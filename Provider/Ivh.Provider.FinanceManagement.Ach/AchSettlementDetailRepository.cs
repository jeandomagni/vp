﻿namespace Ivh.Provider.FinanceManagement.Ach
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Common.Base.Utilities.Extensions;
    using Common.Data;
    using Common.Data.Connection.Connections;
    using Common.Data.Interfaces;
    using Domain.FinanceManagement.Ach.Entities;
    using Domain.FinanceManagement.Ach.Interfaces;
    using NHibernate;
    using NHibernate.SqlCommand;

    public class AchSettlementDetailRepository : RepositoryBase<AchSettlementDetail, VisitPay>, IAchSettlementDetailRepository
    {
        public AchSettlementDetailRepository(ISessionContext<VisitPay> sessionContext)
            : base(sessionContext)
        {
        }

        public IReadOnlyList<AchSettlementDetail> GetDetailsForFile(int achFileId)
        {
            IQueryOver<AchSettlementDetail, AchSettlementDetail> query = this.Session.QueryOver<AchSettlementDetail>();
            query.Where(w => w.AchFile == new AchFile() { AchFileId = achFileId });
            return (IReadOnlyList<AchSettlementDetail>)query.List<AchSettlementDetail>();
        }

        public IReadOnlyList<AchSettlementDetail> GetSettlementDetails(DateTime startDate, DateTime endDate, bool unprocessedOnly = false)
        {
            IQueryOver<AchSettlementDetail, AchSettlementDetail> query = this.Session.QueryOver<AchSettlementDetail>();
            query.Where(w => w.DownloadedDate >= startDate.Date && w.DownloadedDate < endDate.Date);
            if (unprocessedOnly)
            {
                query.And(w => w.ProcessedDate == null);
            }
            return (IReadOnlyList<AchSettlementDetail>)query.List<AchSettlementDetail>();
        }

        public int GetSettlementDetailsCount(AchFile achFile, bool unprocessedOnly = false)
        {
            IQueryOver<AchSettlementDetail, AchSettlementDetail> query = this.Session.QueryOver<AchSettlementDetail>();
            query.Where(w => w.AchFile == achFile);
            if (unprocessedOnly)
            {
                query.And(w => w.ProcessedDate == null);
            }
            return query.RowCount();
        }

        public IList<AchSettlementDetail> GetAchSettlementDetailsByTransactionId(string transactionId)
        {
            IList<AchSettlementDetail> achSettlementDetails = this.GetQueryable().Where(x => x.IndividualId == transactionId).ToList();
            return achSettlementDetails;
        }

        private IQueryable<AchSettlementDetail> GetAchSettlementDetailsWithFile()
        {
            return this.Session.Query<AchSettlementDetail>().Where(x => x.AchFile != null);
        }

        private IQueryable<AchReturnDetail> GetAchReturnDetailsWithFile()
        {
            return this.Session.Query<AchReturnDetail>().Where(x => x.AchFile != null);
        }

        public IList<string> GetAchSettlementTransactionIdsWithoutFile()
        {
            IList<string> achSettlementTransactionIds = this.Session.Query<AchSettlementDetail>()
                .Where(x => x.AchFile == null)
                .Where(x => this.GetAchSettlementDetailsWithFile().FirstOrDefault(f => f.IndividualId == x.IndividualId) == null)
                .Where(x => this.GetAchReturnDetailsWithFile().FirstOrDefault(r => r.IndividualId == x.IndividualId) == null)
                .Select(x=> x.IndividualId)
                .ToList();

            return achSettlementTransactionIds;
        }
    }
}
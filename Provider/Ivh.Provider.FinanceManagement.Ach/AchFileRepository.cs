﻿namespace Ivh.Provider.FinanceManagement.Ach
{
    using Common.Data;
    using Common.Data.Connection.Connections;
    using Common.Data.Interfaces;
    using Domain.FinanceManagement.Ach.Entities;
    using Domain.FinanceManagement.Ach.Interfaces;
    using NHibernate;

    public class AchFileRepository : RepositoryBase<AchFile, VisitPay>, IAchFileRepository
    {
        public AchFileRepository(ISessionContext<VisitPay> sessionContext)
            : base(sessionContext)
        {
        }
    }
}
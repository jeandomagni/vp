﻿namespace Ivh.Provider.FinanceManagement.Ach
{
    using System;
    using System.Threading.Tasks;
    using Common.Base.Constants;
    using Common.Base.Enums;
    using Common.Base.Utilities.Helpers;
    using Common.Cache;
    using Common.VisitPay.Constants;
    using Common.VisitPay.Enums;
    using Domain.Settings.Enums;
    using Domain.Settings.Interfaces;
    using Ivh.Domain.FinanceManagement.Ach.Interfaces;

    public partial class NachaGateway : System.Web.Services.Protocols.SoapHttpClientProtocol, INachaGateway
    {
        private const string NamespaceBase = "http://api.ach.com/";
        private string _nachaId;
        private string _nachaSecurityToken;

        public NachaGateway(
            Lazy<IApplicationSettingsService> applicationSettingsService,
            Lazy<IDistributedCache> distributedCache)
        {
            this.Url = applicationSettingsService.Value.GetUrl(UrlEnum.NachaGateway).AbsoluteUri;
            if (EnumHelper<IvhEnvironmentTypeEnum>.Contains(applicationSettingsService.Value.EnvironmentType.Value, "Lower"))
            {
                string uriString = Task.Run(async () => await distributedCache.Value.GetStringAsync(AchInterceptorCacheKeys.NachaGatewayOverrideUri).ConfigureAwait(false)).Result;
                if (!string.IsNullOrEmpty(uriString))
                {
                    if (Uri.IsWellFormedUriString(uriString, UriKind.Absolute))
                        this.Url = uriString;
                }
            }
        }


        string INachaGateway.GetReturnFile(string nachaId, string nachaSecurityToken, string beginDate, string endDate, string fileFormat)
        {
            this._nachaId = nachaId;
            this._nachaSecurityToken = nachaSecurityToken;
            return this.GetReturnFile(nachaId, beginDate, endDate, fileFormat, nachaSecurityToken);
        }

        string INachaGateway.GetReport(string nachaId, string nachaSecurityToken, string reporttypeid, string odfiid, string resellerid, string startdate, string enddate, string datetypeid, string originationfileid, string salespersonid, string exportformat)
        {
            this._nachaId = nachaId;
            this._nachaSecurityToken = nachaSecurityToken;
            return this.GetReport(this._nachaSecurityToken, reporttypeid, odfiid, this._nachaId, resellerid, startdate, enddate, datetypeid, originationfileid, salespersonid, exportformat);
        }
    }
}
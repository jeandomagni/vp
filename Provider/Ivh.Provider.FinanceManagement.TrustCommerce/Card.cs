namespace Ivh.Provider.FinanceManagement.TrustCommerce
{
    public class Card
    {
        public Card(string cardType, string nameOnCard, string cardNumber, string expDate, string address1, string address2, string city, string state,
            string zip, string cvv = null, string billingId = null)
        {
            this.CardNumber = cardNumber;
            this.CardType = cardType;
            this.NameOnCard = nameOnCard;
            this.ExpDate = expDate;
            this.Address1 = address1;
            this.Address2 = address2 ?? string.Empty;
            this.City = city;
            this.State = state;
            this.Zip = zip;
            this.Cvv = cvv;
            this.BillingId = !string.IsNullOrEmpty(billingId) ? billingId : null;
        }

        public string CardType { get; set; }
        public string CardNumber { get; set; }
        public string NameOnCard { get; set; }
        public string ExpDate { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Zip { get; set; }
        public string Cvv { get; set; }
        public string BillingId { get; set; }
    }
}
namespace Ivh.Provider.FinanceManagement.TrustCommerce
{
    internal class AuthAndCaptureCreditCardRequest : AuthAndCaptureRequest
    {
        /// <summary>
        /// number (3-4).  The three- or four-character field located in the front or back of the card. For Visa, MasterCard, and Discover, this is a three-character field located on the back of the card to the far right. For American Express, it is a four-character value located on the front of the card above the primary account number.
        /// </summary>
        public string CardVerificationValue { get; set; }

        /// <summary>
        /// number (13-16).  Cardholder primary account number (PAN) (for example, a credit or debit card number)
        /// </summary>
        public string CreditCardNumber { get; set; }

        /// <summary>
        /// number (4).  Payment card expiration date in "MMYY" format
        /// </summary>
        public string CreditCardExpiration { get; set; }

        /// <summary>
        /// string (80)
        /// </summary>
        public string Address1 { get; set; }

        /// <summary>
        /// string (20)
        /// </summary>
        public string Zip { get; set; }
    }
}
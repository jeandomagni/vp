﻿namespace Ivh.Provider.FinanceManagement.TrustCommerce
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Net.Http;
    using System.Threading.Tasks;
    using Common.Base.Utilities.Extensions;
    using Domain.FinanceManagement.Entities;
    using Domain.FinanceManagement.Interfaces;
    using Domain.FinanceManagement.Payment.Entities;
    using Domain.FinanceManagement.ValueTypes;

    public class StoreBadAccountsProvider : IPaymentProvider
    {
        private readonly string _trustCommerceCustomerId;
        private readonly string _trustCommercePassword;

        public StoreBadAccountsProvider(string trustCommerceCustomerId, string trustCommercePassword)
        {
            this._trustCommerceCustomerId = trustCommerceCustomerId;
            this._trustCommercePassword = trustCommercePassword;
        }

        public string PaymentProcessorName
        {
            get { return "TrustCommerce"; }
        }

        public string ProductName
        {
            get { return "NGVP"; }
        }

        public async Task<BillingIdResult> StoreBankAccountBillingIdAsync(PaymentMethod paymentMethod, string accountNumber, string routingNumber)
        {
            StoreBillingIdRequest request = new StoreBillingIdRequest
            {
                BillingId = paymentMethod.BillingId,
                Name = paymentMethod.FirstNameLastName,
                Media = "ach",
                Account = accountNumber,
                Routing = routingNumber
            };

            return await this.TryStoreBillingId(request);
        }

        public async Task<BillingIdResult> StoreCreditDebitCardBillingIdAsync(PaymentMethod paymentMethod, string accountNumber, string securityCode)
        {
            PaymentMethodBillingAddress billingAddress = paymentMethod.BillingAddresses.First();
            Card card = new Card(
                string.Empty,
                paymentMethod.NameOnCard,
                accountNumber,
                paymentMethod.ExpDate,
                billingAddress.Address1,
                string.Empty,
                billingAddress.City,
                billingAddress.State,
                billingAddress.Zip,
                securityCode);

            return await this.Store(card);
        }

        public async Task<bool> UpdateCreditCardExpirationAsync(string billingId, string expiration, string cvv)
        {
            IDictionary<string, string> request = this.GetBasicRequest();
            request["action"] = "unstore";
            request["billingid"] = billingId;
            request["exp"] = expiration;
            request["cvv"] = cvv;

            IReadOnlyDictionary<string, string> response = await this.GetTcApiRequestResponse(request);

            return this.Succeeded(response);
        }

        public async Task<bool> UnstoreBillingIdAsync(string billingId)
        {
            Dictionary<string, string> request = this.GetBasicRequest();
            request["action"] = "unstore";
            request["billingid"] = billingId;

            await this.GetTcApiRequestResponse(request);

            //return this.Succeeded(response);
            // don't want to prevent billing id from being unstored if not found
            return true;
        }

        public async Task<IList<PaymentProcessorResponse>> SubmitPaymentsAsync(IList<Payment> payments, PaymentProcessor paymentProcessor)
        {
            throw new NotImplementedException();
#pragma warning disable 162
            return await Task.Run(() => default(IList<PaymentProcessorResponse>));
#pragma warning restore 162
        }

        private Dictionary<string, string> GetBasicRequest()
        {
            return new Dictionary<string, string>
            {
                {"custid", this._trustCommerceCustomerId},
                {"password", this._trustCommercePassword},
                {"demo", "y"}
            };
        }

        private void AddCardToRequest(IDictionary<string, string> request, Card card)
        {
            request["cc"] = card.CardNumber;
            request["exp"] = card.ExpDate;
            request["name"] = card.NameOnCard;
            request["address1"] = card.Address1;
            request["city"] = card.City;
            request["state"] = card.State;
            request["zip"] = card.Zip;
            if (card.Cvv != null) request["cvv"] = card.Cvv;
        }

        private async Task<BillingIdResult> SubmitBillingIdRequest(IDictionary<string, string> request)
        {
            IReadOnlyDictionary<string, string> responseItems = await this.GetTcApiRequestResponse(request);

            return new BillingIdResult
            {
                BillingID = this.GetResponseValue(responseItems, "billingid"),
                GatewayToken = this.GetResponseValue(responseItems, "transid"),
                Succeeded = this.Succeeded(responseItems)
            };
        }

        private async Task<IReadOnlyDictionary<string, string>> GetTcApiRequestResponse(IDictionary<string, string> request)
        {
            string responseText = await this.GetTcApiRequestRawResponse(request);
            return this.ParseResponse(responseText);
        }

        public IReadOnlyDictionary<string, string> ParseResponse(string rawResponse)
        {
            if (rawResponse.IsNullOrEmpty())
                return new Dictionary<string, string>();

            return rawResponse.Split(new[] { "\r\n", "\n" }, StringSplitOptions.RemoveEmptyEntries)
                .Select(x => x.Split('='))
                .ToDictionary(x => x[0], x => x.Length > 1 ? x[1].Trim() : null);
        }

        private async Task<string> GetTcApiRequestRawResponse(IDictionary<string, string> request)
        {
            try
            {
                using (HttpClient client = new HttpClient())
                {
                    client.BaseAddress = new Uri("https://vault.trustcommerce.com");
                    FormUrlEncodedContent formUrlEncodedContent = new FormUrlEncodedContent(request);
                    HttpResponseMessage response = await client.PostAsync("/trans/", formUrlEncodedContent);
                    if (response.IsSuccessStatusCode)
                    {
                        return await response.Content.ReadAsStringAsync();
                    }
                }
                return "status=CallToProcessorFailed\n";
            }
            catch (Exception)
            {
                return "status=CallToProcessorFailed\n";
            }
        }

        private bool Succeeded(IReadOnlyDictionary<string, string> responseItems)
        {
            return true;
            //return this.GetResponseValue(responseItems, "status") == "approved" &&
            //this._avsCodes.IsGood(this.GetResponseValue(responseItems, "avs"));
        }

        private string GetResponseValue(IReadOnlyDictionary<string, string> responseItems, string name)
        {
            return responseItems.ContainsKey(name) ? responseItems[name] : null;
        }

        private async Task<BillingIdResult> Store(Card card)
        {
            Dictionary<string, string> request = this.GetBasicRequest();
            request["action"] = "store";
            request["verify"] = "n";
            this.AddCardToRequest(request, card);
            return await this.SubmitBillingIdRequest(request);
        }

        private async Task<BillingIdResult> TryStoreBillingId(StoreBillingIdRequest request)
        {
            StoreBillingIdResponse response = await this.StoreBillingId(request);
            bool succeeded = this.WasSuccessful(response);

            return new BillingIdResult
            {
                Succeeded = succeeded,
                BillingID = response.BillingId,
                GatewayToken = response.TransactionId
            };
        }

        private bool WasSuccessful(StoreBillingIdResponse response)
        {
            //return response.Status.ToUpper() == "ACCEPTED" || response.Status.ToUpper() == "APPROVED";
            return true;
        }

        private async Task<StoreBillingIdResponse> StoreBillingId(StoreBillingIdRequest requestFields)
        {
            #region request / response

            List<KeyValuePair<string, string>> request = this.CreateTcApiRequest("store");
            request.Add(new KeyValuePair<string, string>("media", requestFields.Media));
            request.Add(new KeyValuePair<string, string>("name", requestFields.Name));

            if (requestFields.Media == "ach")
            {
                request.Add(new KeyValuePair<string, string>("account", requestFields.Account));
                request.Add(new KeyValuePair<string, string>("routing", requestFields.Routing));
            }
            else
            {
                request.Add(new KeyValuePair<string, string>("verify", "n"));
                request.Add(new KeyValuePair<string, string>("cc", requestFields.CreditCardNumber));
                request.Add(new KeyValuePair<string, string>("city", requestFields.City));
                request.Add(new KeyValuePair<string, string>("exp", requestFields.CreditCardExpiration));
                request.Add(new KeyValuePair<string, string>("state", requestFields.State));
                request.Add(new KeyValuePair<string, string>("zip", requestFields.Zip));
                request.Add(new KeyValuePair<string, string>("cvv", requestFields.CardVerificationValue));
            }

            if (!string.IsNullOrEmpty(requestFields.BillingId) && requestFields.BillingId != "0")
                request.Add(new KeyValuePair<string, string>("billingid", requestFields.BillingId));

            IReadOnlyDictionary<string, string> response = await this.GetTcApiRequestResponse(request.ToDictionary(p => p.Key, p => p.Value));

            #endregion

            #region parse response

            string authCode;
            string avs;
            string billingId;
            string error;
            string errorType;
            string offenders;
            string responseCode;
            string responseCodeDescriptor;
            string status;
            string transactionId;

            response.TryGetValue("authcode", out authCode);
            response.TryGetValue("avs", out avs);
            response.TryGetValue("billingid", out billingId);
            response.TryGetValue("error", out error);
            response.TryGetValue("errortype", out errorType);
            response.TryGetValue("offenders", out offenders);
            response.TryGetValue("responsecode", out responseCode);
            response.TryGetValue("responsecodedescriptor", out responseCodeDescriptor);
            response.TryGetValue("status", out status);
            response.TryGetValue("transid", out transactionId);

            StoreBillingIdResponse storeBillingIdResponse = new StoreBillingIdResponse
            {
                AuthCode = authCode,
                Avs = avs,
                BillingId = billingId,
                Error = error,
                ErrorType = errorType,
                Offenders = offenders,
                ResponseCode = responseCode,
                ResponseCodeDescriptor = responseCodeDescriptor,
                Status = status,
                TransactionId = transactionId
            };

            if (!string.IsNullOrEmpty(storeBillingIdResponse.Status) &&
                !string.IsNullOrEmpty(requestFields.BillingId))
            {
                storeBillingIdResponse.BillingId = requestFields.BillingId;
            }

            #endregion

            return storeBillingIdResponse;
        }

        private List<KeyValuePair<string, string>> CreateTcApiRequest(string action)
        {
            return new List<KeyValuePair<string, string>>
            {
                new KeyValuePair<string, string>("action", action),
                new KeyValuePair<string, string>("custid", this._trustCommerceCustomerId),
                new KeyValuePair<string, string>("password", this._trustCommercePassword),
                new KeyValuePair<string, string>("demo", "y")
            };
        }
    }
}
﻿namespace Ivh.Provider.FinanceManagement.TrustCommerce.Providers.Mappings
{
    using System;
    using System.Globalization;
    using System.Threading;
    using CsvHelper.Configuration;
    using CsvHelper.TypeConversion;
    using Entities;

    public sealed class QueryPaymentTransactionMap : CsvClassMap<QueryPaymentTransaction>
    {
        public QueryPaymentTransactionMap()
        {
            int i = 0;
            this.Map(m => m.Cc).Name("cc");i++;
            this.Map(m => m.MediaName).Name("media_name"); i++;
            this.Map(m => m.Exp).Name("exp"); i++;
            this.Map(m => m.TransDate).Name("trans_date"); i++;
            this.Map(m => m.Transid).Name("transid"); i++;
            this.Map(m => m.RefTransid).Name("ref_transid"); i++;
            this.Map(m => m.Amount).Name("amount"); i++;
            this.Map(m => m.AuthAmount).Name("auth_amount"); i++;
            this.Map(m => m.BankAmount).Name("bank_amount"); i++;
            this.Map(m => m.CreditAmount).Name("credit_amount"); i++;
            this.Map(m => m.ChargebackAmount).Name("chargeback_amount"); i++;
            this.Map(m => m.ActionName).Name("action_name"); i++;
            this.Map(m => m.StatusName).Name("status_name"); i++;
            this.Map(m => m.Name).Name("name"); i++;
            this.Map(m => m.Address1).Name("address1"); i++;
            this.Map(m => m.Address2).Name("address2"); i++;
            this.Map(m => m.City).Name("city"); i++;
            this.Map(m => m.State).Name("state"); i++;
            this.Map(m => m.Zip).Name("zip"); i++;
            this.Map(m => m.Phone).Name("phone"); i++;
            this.Map(m => m.Email).Name("email"); i++;
            this.Map(m => m.ShipToSame).Name("shiptosame"); i++;
            this.Map(m => m.ShipToName).Name("shipto_name"); i++;
            this.Map(m => m.ShipToAddress1).Name("shipto_address1"); i++;
            this.Map(m => m.ShipToAddress2).Name("shipto_address2");
            this.Map(m => m.ShipToCity).Name("shipto_city"); i++;
            this.Map(m => m.ShipToState).Name("shipto_state"); i++;
            this.Map(m => m.ShipToZip).Name("shipto_zip"); i++;
            this.Map(m => m.Expired).Name("expired"); i++;
            this.Map(m => m.ReAuth).Name("reauth"); i++;
            this.Map(m => m.Chain).Name("chain"); i++;
            this.Map(m => m.ChainHead).Name("chain_head"); i++;
            this.Map(m => m.Ticket).Name("ticket"); i++;
            this.Map(m => m.BatchNum).Name("batchnum"); i++;
            this.Map(m => m.AuthCode).Name("authcode");
            this.Map(m => m.BillingId).Name("billingid"); i++;
            this.Map(m => m.CustId).Name("custid"); i++;
            this.Map(m => m.FailName).Name("fail_name"); i++;
            this.Map(m => m.Avs).Name("avs"); i++;
            this.Map(m => m.Operator).Name("operator"); i++;
            this.Map(m => m.Server).Name("server"); i++;
            this.Map(m => m.TransId2).Index(i++);//.Name("transid"); i++;
            this.Map(m => m.BillingId2).Index(i++);//.Name("billingid"); i++;
            this.Map(m => m.CustomField2).Name("customfield2"); i++;
            this.Map(m => m.CustomField3).Name("customfield3"); i++;
            this.Map(m => m.CustomField4).Name("customfield4"); i++;
            this.Map(m => m.CustomField5).Name("customfield5"); i++;
            this.Map(m => m.CustomField6).Name("customfield6"); i++;
            this.Map(m => m.CustomField7).Name("customfield7"); i++;
            this.Map(m => m.CustomField8).Name("customfield8"); i++;
            this.Map(m => m.CustomField9).Name("customfield9"); i++;
            this.Map(m => m.CustomField10).Name("customfield10"); i++;
            this.Map(m => m.CustomField11).Name("customfield11"); i++;
            this.Map(m => m.CustomField12).Name("customfield12"); i++;
            this.Map(m => m.CountryCode).Name("country_code"); i++;
            this.Map(m => m.Tax).Name("tax"); i++;
            this.Map(m => m.PurchaseOrderNum).Name("purchaseordernum"); i++;
            this.Map(m => m.BatchId).Name("batchid"); i++;
            this.Map(m => m.Closed).Name("closed"); i++;
            this.Map(m => m.EntryMode).Name("entry_mode"); i++;
            this.Map(m => m.ResponseCode).Name("responsecode"); i++;
            this.Map(m => m.TelecheckTraceId).Name("telecheck_traceid"); i++;
            this.Map(m => m.Savings).Name("savings"); i++;
            this.Map(m => m.EmailReceiptStatusPublic).Name("emailreceiptstatus"); i++;
        }
    }

    internal class CurrencyTypeConverter : ITypeConverter
    {
        public const string DateTimeFormatString = "MM-dd-yyyy HH:mm:ssZ";

        public bool CanConvertFrom(Type type)
        {
            return type.AssemblyQualifiedName == typeof(decimal).AssemblyQualifiedName;
        }

        public bool CanConvertTo(Type type)
        {
            return type.AssemblyQualifiedName == typeof(decimal).AssemblyQualifiedName;
        }

        public object ConvertFromString(TypeConverterOptions options, string text)
        {
            decimal returnValue;
            return decimal.TryParse(text, NumberStyles.Currency, CultureInfo.InvariantCulture, out returnValue) ? returnValue : returnValue;
        }

        public string ConvertToString(TypeConverterOptions options, object value)
        {
            return ((DateTime)value).ToString(DateTimeFormatString);
        }
    }

    internal class DateTimeTypeConverter : ITypeConverter
    {
        public const string DateTimeFormatString = "MM-dd-yyyy HH:mm:ssZ";

        public bool CanConvertFrom(Type type)
        {
            return type.AssemblyQualifiedName == typeof(DateTime).AssemblyQualifiedName;
        }

        public bool CanConvertTo(Type type)
        {
            return type.AssemblyQualifiedName == typeof(DateTime).AssemblyQualifiedName;
        }

        public object ConvertFromString(TypeConverterOptions options, string text)
        {
            if (text.EndsWith("PDT"))
            {
                text = text.Replace(" PDT", "-7:00");
            }
            else if (text.EndsWith("PDT"))
            {
                text = text.Replace(" PST", "-8:00");
            }
            else
            {
                throw new InvalidOperationException("Date string does not end with either PDT or PST");
            }

            DateTime dateTimeOffset;
            return DateTime.TryParseExact(text, DateTimeFormatString, CultureInfo.InvariantCulture, DateTimeStyles.AssumeLocal, out dateTimeOffset) ? dateTimeOffset : DateTime.UtcNow;
        }

        public string ConvertToString(TypeConverterOptions options, object value)
        {
            return ((DateTime)value).ToString(DateTimeFormatString);
        }
    }

    internal class StringTypeConverter : ITypeConverter
    {
        public bool CanConvertFrom(Type type)
        {
            return type.AssemblyQualifiedName == typeof(string).AssemblyQualifiedName;
        }

        public bool CanConvertTo(Type type)
        {
            return type.AssemblyQualifiedName == typeof(string).AssemblyQualifiedName;
        }

        public object ConvertFromString(TypeConverterOptions options, string text)
        {
            return text.Trim();
        }

        public string ConvertToString(TypeConverterOptions options, object value)
        {
            return value.ToString().Trim();
        }
    }
}
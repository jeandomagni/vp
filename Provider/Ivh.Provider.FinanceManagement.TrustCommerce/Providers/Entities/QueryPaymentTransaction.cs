﻿namespace Ivh.Provider.FinanceManagement.TrustCommerce.Providers.Entities
{
    using CsvHelper;

    public class QueryPaymentTransaction
    {
        //public virtual string TransactionId { get; set; }
        //public virtual DateTime TransactionDateTime { get; set; }
        //public decimal Amount { get; set; }
        //public virtual string Action { get; set; }
        //public virtual string Status { get; set; }

        public virtual string Cc { get; set; }
        public virtual string MediaName { get; set; }
        public virtual string Exp { get; set; }
        public virtual string TransDate { get; set; }
        public virtual string Transid { get; set; }
        public virtual string RefTransid { get; set; }
        public virtual string Amount { get; set; }
        public virtual string AuthAmount { get; set; }
        public virtual string BankAmount { get; set; }
        public virtual string CreditAmount { get; set; }
        public virtual string ChargebackAmount { get; set; }
        public virtual string ActionName { get; set; }
        public virtual string StatusName { get; set; }
        public virtual string Name { get; set; }
        public virtual string Address1 { get; set; }
        public virtual string Address2 { get; set; }
        public virtual string City { get; set; }
        public virtual string State { get; set; }
        public virtual string Zip { get; set; }
        public virtual string Phone { get; set; }
        public virtual string Email { get; set; }
        public virtual string ShipToSame { get; set; }
        public virtual string ShipToName { get; set; }
        public virtual string ShipToAddress1 { get; set; }
        public virtual string ShipToAddress2 { get; set; }
        public virtual string ShipToCity { get; set; }
        public virtual string ShipToState { get; set; }
        public virtual string ShipToZip { get; set; }
        public virtual string Expired { get; set; }
        public virtual string ReAuth { get; set; }
        public virtual string Chain { get; set; }
        public virtual string ChainHead { get; set; }
        public virtual string Ticket { get; set; }
        public virtual string BatchNum { get; set; }
        public virtual string AuthCode { get; set; }
        public virtual string BillingId { get; set; }
        public virtual string CustId { get; set; }
        public virtual string FailName { get; set; }
        public virtual string Avs { get; set; }
        public virtual string Operator { get; set; }
        public virtual string Server { get; set; }
        public virtual string TransId2 { get; set; }
        public virtual string BillingId2 { get; set; }
        public virtual string CustomField2 { get; set; }
        public virtual string CustomField3 { get; set; }
        public virtual string CustomField4 { get; set; }
        public virtual string CustomField5 { get; set; }
        public virtual string CustomField6 { get; set; }
        public virtual string CustomField7 { get; set; }
        public virtual string CustomField8 { get; set; }
        public virtual string CustomField9 { get; set; }
        public virtual string CustomField10 { get; set; }
        public virtual string CustomField11 { get; set; }
        public virtual string CustomField12 { get; set; }
        public virtual string CountryCode { get; set; }
        public virtual string Tax { get; set; }
        public virtual string PurchaseOrderNum { get; set; }
        public virtual string BatchId { get; set; }
        public virtual string Closed { get; set; }
        public virtual string EntryMode { get; set; }
        public virtual string ResponseCode { get; set; }
        public virtual string TelecheckTraceId { get; set; }
        public virtual string Savings { get; set; }
        public virtual string EmailReceiptStatusPublic { get; set; }
    }
}
namespace Ivh.Provider.FinanceManagement.TrustCommerce
{
    internal class StoreBillingIdRequest
    {
        /// <summary>
        /// string (6).  This is a six-character alphanumeric code used to retrieve customer payment card and ACH information for future, one-time and recurring/installment transactions.
        /// </summary>
        public string BillingId { get; set; }

        /// <summary>
        /// name of person
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// cc or ach
        /// </summary>
        public string Media { get; set; }

        /// <summary>
        /// used for Credit Card.  Required for media=cc
        /// </summary>
        public string City { get; set; }
        /// <summary>
        /// used for Credit Card.  Required for media=cc
        /// </summary>
        public string State { get; set; }
        /// <summary>
        /// used for State.  Required for media=cc
        /// </summary>
        public string Zip { get; set; }

        /// <summary>
        /// number (4).  Payment card expiration date in "MMYY" format
        /// </summary>
        public string CreditCardExpiration { get; set; }

        /// <summary>
        /// number (13-16).  Cardholder primary account number (PAN) (for example, a credit or debit card number).
        /// </summary>
        public string CreditCardNumber { get; set; }

        /// <summary>
        /// number (3-4).  The three- or four-character field located in the front or back of the card. For Visa, MasterCard, and Discover, this is a three-character field located on the back of the card to the far right. For American Express, it is a four-character value located on the front of the card above the primary account number.
        /// </summary>
        public string CardVerificationValue { get; set; }

        /// <summary>
        /// number (3-17).  checking or savings account number
        /// </summary>
        public string Account { get; set; }

        /// <summary>
        /// number (9).  checking or savings account routing number
        /// </summary>
        public string Routing { get; set; }


        /// <summary>
        /// For ACH, is the Account a Savings account
        /// </summary>
        public bool Savings { get; set; }
    }
}
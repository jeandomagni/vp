﻿namespace Ivh.Provider.FinanceManagement.TrustCommerce
{
    internal class AuthAndCaptureAchRequest : AuthAndCaptureRequest
    {
        /// <summary>
        /// number (3-17).  checking or savings account number
        /// </summary>
        public string Account { get; set; }

        /// <summary>
        /// number (9).  checking or savings account routing number
        /// </summary>
        public string Routing { get; set; }
    }
}
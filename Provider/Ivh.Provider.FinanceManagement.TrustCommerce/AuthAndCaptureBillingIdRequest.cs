namespace Ivh.Provider.FinanceManagement.TrustCommerce
{
    internal class AuthAndCaptureBillingIdRequest : AuthAndCaptureRequest
    {
        /// <summary>
        /// string (6).  This is a six-character alphanumeric code used to retrieve customer payment card and ACH information for future, one-time and recurring/installment transactions.
        /// </summary>
        public string BillingId { get; set; }
    }
}
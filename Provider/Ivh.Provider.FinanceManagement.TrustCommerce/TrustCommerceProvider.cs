﻿namespace Ivh.Provider.FinanceManagement.TrustCommerce
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Configuration;
    using System.Globalization;
    using System.IO;
    using System.Linq;
    using System.Net.Http;
    using System.Threading;
    using System.Threading.Tasks;
    using Common.Base.Enums;
    using Common.Base.Utilities.Extensions;
    using Common.Base.Utilities.Helpers;
    using Common.ServiceBus.Interfaces;
    using Common.VisitPay.Enums;
    using Common.VisitPay.Messages.FinanceManagement;
    using Common.VisitPay.Strings;
    using CsvHelper;
    using Domain.FinanceManagement.Entities;
    using Domain.FinanceManagement.Interfaces;
    using Domain.FinanceManagement.Payment.Entities;
    using Domain.FinanceManagement.ValueTypes;
    using Domain.Logging.Interfaces;
    using Domain.Settings.Entities;
    using Domain.Settings.Enums;
    using Domain.Settings.Interfaces;
    using GuestPayEntities = Domain.GuestPay.Entities;
    using GuestPayInterfaces = Domain.GuestPay.Interfaces;
    using Providers.Mappings;
    using Ivh.Domain.FinanceManagement.Payment.Interfaces;
    using Timer = Common.Base.Utilities.Helpers.Timer;

    public class TrustCommercePaymentProvider :
        IPaymentProvider,
        GuestPayInterfaces.IPaymentProvider,
        IPaymentReversalProvider,
        GuestPayInterfaces.IPaymentReversalProvider,
        IPaymentBatchOperationsProvider
    {
        private readonly Lazy<ILoggingProvider> _logger;
        private readonly Lazy<IBus> _bus;
        private readonly Lazy<Uri> _trustCommerceApiUri;
        private readonly Lazy<bool> _trustCommerceIsLiveMode;
        private readonly Lazy<IPaymentConfigurationService> _paymentConfigurationService;
        private readonly Lazy<IApplicationSettingsService> _applicationSettingsService;
        private readonly Lazy<IClientService> _clientService;
        private readonly Lazy<IMerchantAccountAllocationService> _merchantAccountAllocationService;

        private static readonly HttpClient HttpClient = new HttpClient();

        public TrustCommercePaymentProvider(
            Lazy<IPaymentConfigurationService> paymentConfigurationService,
            Lazy<IApplicationSettingsService> applicationSettingsService,
            Lazy<IClientService> clientService,
            Lazy<ILoggingProvider> logger,
            Lazy<IBus> bus,
            Lazy<IMerchantAccountAllocationService> merchantAccountAllocationService)
        {
            this._paymentConfigurationService = paymentConfigurationService;
            this._applicationSettingsService = applicationSettingsService;
            this._clientService = clientService;
            this._logger = logger;
            this._bus = bus;

            this._trustCommerceApiUri = new Lazy<Uri>(() => this._applicationSettingsService.Value.GetUrl(UrlEnum.TrustCommerceApi));
            this._trustCommerceIsLiveMode = new Lazy<bool>(() => paymentConfigurationService.Value.TrustCommerceIsLiveMode);
            this._merchantAccountAllocationService = merchantAccountAllocationService;

            if (HttpClient.BaseAddress == default(Uri))
            {
                HttpClient.BaseAddress = this._trustCommerceApiUri.Value;
            }
        }

        async Task<BillingIdResult> GuestPayInterfaces.IPaymentProvider.VerifyPaymentMethodAsync(GuestPayEntities.Payment payment, PaymentProcessor paymentProcessor)
        {
            if (payment == null)
            {
                throw new ArgumentNullException(nameof(payment));
            }

            if (paymentProcessor == null)
            {
                throw new ArgumentNullException(nameof(paymentProcessor));
            }

            if (!this._paymentConfigurationService.Value.IsPaymentMethodTypeAccepted(payment.PaymentMethodType))
            {
                throw new ConfigurationErrorsException("acceptedPaymentMethodTypes");
            }

            if (payment.IsAchType)
            {
                return new BillingIdResult
                {
                    Succeeded = true
                };
            }

            if (payment.IsCardType)
            {
                Card card = new Card(string.Empty,
                    payment.PaymentAccountHolderName,
                    //payment.CardNumber,
                    payment.BillingId,
                    payment.ExpDate,
                    payment.AddressStreet1,
                    payment.AddressStreet2,
                    payment.City,
                    payment.State,
                    payment.Zip,
                    payment.SecurityCode);

                return await this.VerifyAsync(card);
            }

            return new BillingIdResult
            {
                Succeeded = false
            };
        }

        private const string NoSuchBillingId = "nosuchbillingid";
        async Task<PaymentProcessorResponse> GuestPayInterfaces.IPaymentProvider.SubmitPaymentAsync(GuestPayEntities.Payment payment, PaymentProcessor paymentProcessor)
        {
            PaymentProcessorResponse paymentProcessorResponse = null;
            for (int i = 1; i <= 4; i *= 2)
            {
                paymentProcessorResponse = await this.SubmitPaymentAsyncInternal(payment, paymentProcessor);
                if (!paymentProcessorResponse.WasSuccessful()
                    && paymentProcessorResponse.ResponseField7 != null
                    && NoSuchBillingId.Equals(paymentProcessorResponse.ResponseField7, StringComparison.OrdinalIgnoreCase))
                {
                    Thread.Sleep(TimeSpan.FromSeconds(i));
                    continue;
                }
                break;
            }
            return paymentProcessorResponse;
        }

        private async Task<PaymentProcessorResponse> SubmitPaymentAsyncInternal(GuestPayEntities.Payment payment, PaymentProcessor paymentProcessor)
        {
            if (payment == null)
            {
                throw new ArgumentNullException(nameof(payment));
            }

            if (paymentProcessor == null)
            {
                throw new ArgumentNullException(nameof(paymentProcessor));
            }

            if (!this._paymentConfigurationService.Value.IsPaymentMethodTypeAccepted(payment.PaymentMethodType))
            {
                throw new ConfigurationErrorsException("acceptedPaymentMethodTypes");
            }

            IDictionary<string, string> request = this.GetBasicRequest();
            bool hasMerchantAccount = payment.MerchantAccountId > 0;
            if (hasMerchantAccount)
            {
                AssignMerchantAccount(ref request, payment.MerchantAccount);
            }

            SetAction(request, TrustCommerceConstants.TransactionType.Sale);
            request[TrustCommerceConstants.Fields.General.Amount] = $"{(int)(payment.PaymentAmount * 100m):D4}";

            request[TrustCommerceConstants.Fields.Custom.CustomField2] = payment.GuarantorFirstName ?? string.Empty;
            request[TrustCommerceConstants.Fields.Custom.CustomField3] = payment.GuarantorLastName ?? string.Empty;
            request[TrustCommerceConstants.Fields.Custom.CustomField4] = this._clientService.Value.GetClient().GuestPayBillingSystem;
            request[TrustCommerceConstants.Fields.Custom.CustomField5] = GetCustomField5Text(payment.PaymentSystemType);
            request[TrustCommerceConstants.Fields.Custom.CustomField6] = payment.GuarantorNumber ?? string.Empty;
            request[TrustCommerceConstants.Fields.Custom.CustomField12] = GetCustomField12Text(payment.PaymentSystemType);

            request[TrustCommerceConstants.Fields.General.Name] = payment.PaymentAccountHolderName;

            if (payment.IsAchType)
            {
                request[TrustCommerceConstants.Fields.General.Media] = TrustCommerceConstants.Media.Ach;
                request[TrustCommerceConstants.Fields.Ach.Required.Routing] = payment.BankRoutingNumber;
                request[TrustCommerceConstants.Fields.Ach.Required.Account] = payment.BankAccountNumber;
                if (payment.PaymentMethodType == PaymentMethodTypeEnum.AchSavings)
                {
                    request[TrustCommerceConstants.Fields.Ach.Optional.Savings] = "y";
                }
            }
            else if (payment.IsCardType)
            {
                request[TrustCommerceConstants.Fields.General.BillingId] = payment.BillingId;

                request[TrustCommerceConstants.Fields.CreditCard.Required.Exp] = payment.ExpDate;
                request[TrustCommerceConstants.Fields.CreditCard.CardVerification.Cvv] = payment.SecurityCode;

                Action<string, string> setValue = (k, v) =>
                {
                    if (!string.IsNullOrEmpty(v))
                    {
                        request[k] = v;
                    }
                };
                setValue(TrustCommerceConstants.Fields.CreditCard.Optional.Address1, payment.AddressStreet1);
                setValue(TrustCommerceConstants.Fields.CreditCard.Optional.Address2, payment.AddressStreet2);
                setValue(TrustCommerceConstants.Fields.CreditCard.Optional.City, payment.City);
                setValue(TrustCommerceConstants.Fields.CreditCard.Optional.State, payment.State);
                setValue(TrustCommerceConstants.Fields.CreditCard.Optional.Zip, payment.Zip);

                if (!payment.IsCountryUsa)
                {
                    request[TrustCommerceConstants.Fields.CreditCard.Optional.Avs] = "n";
                }
            }

            string rawResponse = await this.GetTcApiBaseRequestRawResponseAsync(request, "trans").ConfigureAwait(false);
           
            IReadOnlyDictionary<string, string> response = ParseResponse(rawResponse);
            ResponseValues responseValues = GetResponseValues(response);

            payment.PaymentProcessorResponse = new PaymentProcessorResponse
            {
                PaymentProcessorSourceKey = responseValues.TransactionId ?? string.Empty,
                PaymentProcessor = paymentProcessor,
                PaymentProcessorResponseStatus = MapStatusCode(responseValues),
                RawResponse = rawResponse,
                ResponseField1 = responseValues.TransactionId,
                ResponseField2 = responseValues.Status,
                ResponseField3 = responseValues.AuthCode,
                ResponseField4 = responseValues.Avs,
                ResponseField5 = responseValues.DeclineType,
                ResponseField6 = responseValues.Error,
                ResponseField7 = responseValues.ErrorType,
                ResponseField8 = responseValues.Offenders,
                ResponseField9 = responseValues.ResponseCode,
                ResponseField10 = responseValues.ResponseCodeDescriptor,
                SnapshotTotalPaymentAmount = payment.PaymentAmount,
                ErrorMessage = (responseValues.ErrorType ?? string.Empty) + "-" + (responseValues.Error ?? string.Empty),
                PaymentSystemType = payment.PaymentSystemType
            };

            return payment.PaymentProcessorResponse;
        }

        async Task<PaymentProcessorResponse> GuestPayInterfaces.IPaymentReversalProvider.SubmitPaymentRefundAsync(GuestPayEntities.Payment originalPayment, GuestPayEntities.Payment reversalPayment, PaymentProcessor paymentProcessor, decimal? amount)
        {
            string originalPaymentTransactionId = this.ValidateReversalParameters(originalPayment, reversalPayment, paymentProcessor);

            if (amount.HasValue && amount != originalPayment.PaymentAmount)
            {
                amount = originalPayment.PaymentAmount;
            }

            PaymentProcessorResponse paymentProcessorResponse = await this.SubmitPaymentsRefundAsync(originalPaymentTransactionId, null, paymentProcessor, amount, originalPayment.PaymentSystemType).ConfigureAwait(false);
            paymentProcessorResponse.SnapshotTotalPaymentAmount = -(amount ?? originalPayment.PaymentAmount);

            reversalPayment.PaymentProcessorResponse = new PaymentProcessorResponse
            {
                PaymentProcessorSourceKey = paymentProcessorResponse.PaymentProcessorSourceKey ?? string.Empty,
                PaymentProcessor = paymentProcessor,
                PaymentProcessorResponseStatus = paymentProcessorResponse.PaymentProcessorResponseStatus,
                RawResponse = paymentProcessorResponse.RawResponse,
                ResponseField1 = paymentProcessorResponse.ResponseField1,
                ResponseField2 = paymentProcessorResponse.ResponseField2,
                ResponseField3 = paymentProcessorResponse.ResponseField3,
                ResponseField4 = paymentProcessorResponse.ResponseField4,
                ResponseField5 = paymentProcessorResponse.ResponseField5,
                ResponseField6 = paymentProcessorResponse.ResponseField6,
                ResponseField7 = paymentProcessorResponse.ResponseField7,
                ResponseField8 = paymentProcessorResponse.ResponseField8,
                ResponseField9 = paymentProcessorResponse.ResponseField9,
                ResponseField10 = paymentProcessorResponse.ResponseField10,
                SnapshotTotalPaymentAmount = paymentProcessorResponse.SnapshotTotalPaymentAmount,
                ErrorMessage = paymentProcessorResponse.ErrorMessage,
                PaymentSystemType = reversalPayment.PaymentSystemType
            };
            return reversalPayment.PaymentProcessorResponse;
        }

        async Task<PaymentProcessorResponse> GuestPayInterfaces.IPaymentReversalProvider.SubmitPaymentVoidAsync(GuestPayEntities.Payment originalPayment, GuestPayEntities.Payment reversalPayment, PaymentProcessor paymentProcessor)
        {
            string originalPaymentTransactionId = this.ValidateReversalParameters(originalPayment, reversalPayment, paymentProcessor);

            PaymentProcessorResponse paymentProcessorResponse = await this.SubmitPaymentsVoidAsync(originalPaymentTransactionId, null, paymentProcessor, originalPayment.PaymentSystemType).ConfigureAwait(false);
            paymentProcessorResponse.SnapshotTotalPaymentAmount = reversalPayment.PaymentAmount;

            reversalPayment.PaymentProcessorResponse = new PaymentProcessorResponse
            {
                PaymentProcessorSourceKey = paymentProcessorResponse.PaymentProcessorSourceKey ?? string.Empty,
                PaymentProcessor = paymentProcessor,
                PaymentProcessorResponseStatus = paymentProcessorResponse.PaymentProcessorResponseStatus,
                RawResponse = paymentProcessorResponse.RawResponse,
                ResponseField1 = paymentProcessorResponse.ResponseField1,
                ResponseField2 = paymentProcessorResponse.ResponseField2,
                ResponseField3 = paymentProcessorResponse.ResponseField3,
                ResponseField4 = paymentProcessorResponse.ResponseField4,
                ResponseField5 = paymentProcessorResponse.ResponseField5,
                ResponseField6 = paymentProcessorResponse.ResponseField6,
                ResponseField7 = paymentProcessorResponse.ResponseField7,
                ResponseField8 = paymentProcessorResponse.ResponseField8,
                ResponseField9 = paymentProcessorResponse.ResponseField9,
                ResponseField10 = paymentProcessorResponse.ResponseField10,
                SnapshotTotalPaymentAmount = paymentProcessorResponse.SnapshotTotalPaymentAmount,
                ErrorMessage = paymentProcessorResponse.ErrorMessage,
                PaymentSystemType = reversalPayment.PaymentSystemType
            };

            return reversalPayment.PaymentProcessorResponse;
        }

        public async Task<ICollection<QueryPaymentTransaction>> GetPaymentTransactionsAsync(DateTime beginDate, DateTime endDate, string transactionId, bool logResponse)
        {
            Func<DateTime, string> formatDate = d => d.ToString(Format.DateTimeFormat_MMddyyyyHHmmss);
            IDictionary<string, string> request = this.GetBasicQueryRequest();

            request[TrustCommerceConstants.Fields.Query.General.QueryType] = TrustCommerceConstants.Fields.Query.QueryType.Transaction;
            request[TrustCommerceConstants.Fields.Query.General.Format] = TrustCommerceConstants.Fields.Query.Format.Text;
            request[TrustCommerceConstants.Fields.Query.General.BeginDate] = formatDate(beginDate);
            request[TrustCommerceConstants.Fields.Query.General.EndDate] = formatDate(endDate);
            request[TrustCommerceConstants.Fields.Query.General.ShowCount] = "n";
            
            if (!string.IsNullOrWhiteSpace(transactionId))
            {
                request[TrustCommerceConstants.Fields.General.TransId] = transactionId;
            }

            Func<string, decimal> toDecimal = s =>
            {
                decimal returnValue;
                return decimal.TryParse(s, NumberStyles.Currency, CultureInfo.InvariantCulture, out returnValue) ? returnValue / 100 : returnValue;
            };

            Func<string, DateTime> toDateTime = s =>
            {
                DateTime dateTimeOffset;
                return DateTime.TryParseExact(s, Format.DateTimeFormat_MMddyyyyHHmmss, CultureInfo.InvariantCulture, DateTimeStyles.AssumeLocal, out dateTimeOffset) ? dateTimeOffset : DateTime.UtcNow;
            };

            Collection<QueryPaymentTransaction> queryPaymentTransactions = new Collection<QueryPaymentTransaction>();

            IList<MerchantAccount> merchantAccounts = this._merchantAccountAllocationService.Value.GetAllMerchantAccounts();
            foreach (MerchantAccount merchantAccount in merchantAccounts)
            {
                PaymentProviderCredentials paymentProviderCredentials = this._applicationSettingsService.Value.GetPaymentProviderCredentials(
                    merchantAccount.CustomerIdSettingsKey,
                    merchantAccount.PasswordSettingsKey,
                    merchantAccount.PasswordQueryApiSettingsKey,
                    merchantAccount.AchCustomerIdSettingsKey,
                    merchantAccount.AchPasswordSettingsKey);

                string queryApiCustomerId = paymentProviderCredentials.TrustCommerceCustomerId;
                string queryApiPassword = paymentProviderCredentials.TrustCommercePasswordQueryApi;

                //Overwrite CustomerId and Password with merchant account's query api CustomerId and Password
                request[TrustCommerceConstants.Fields.General.CustomerId] = queryApiCustomerId;
                request[TrustCommerceConstants.Fields.General.Password] = queryApiPassword;

                int offset = 0;
                int recordCount = 0;
                IDictionary<int, QueryPaymentTransaction> returnValues = new Dictionary<int, QueryPaymentTransaction>();
                do
                {
                    recordCount = 0;
                    request[TrustCommerceConstants.Fields.Query.General.Offset] = offset.ToString();
                    string result = string.Empty;
                    try
                    {
                        result = await this.GetTcApiQueryResponseAsync(request).ConfigureAwait(false);
                    }
                    catch (Exception ex)
                    {
                        this._logger.Value.Fatal(() => $"Unable to download TrustCommerce transactions file for Merchant Account: {merchantAccount.Account} ({merchantAccount.Description}) Exception: {ExceptionHelper.AggregateExceptionToString(ex)}");
                    }

                    if (string.IsNullOrEmpty(result))
                    {
                        break;
                    }

                    if ("ERROR: login failed".Equals(result))
                    {
                        UnauthorizedAccessException ex = new UnauthorizedAccessException("TrustCommerce login failed");
                        this._logger.Value.Fatal(() => $"Error Logging in to TrustCommerce to download transactions file for Merchant Account: {merchantAccount.Account} ({merchantAccount.Description}) Exception: {ExceptionHelper.AggregateExceptionToString(ex)}");
                        break;
                    }

                    using (CsvReader reader = new CsvReader(new StringReader(result)))
                    {
                        reader.Configuration.RegisterClassMap<QueryPaymentTransactionMap>();
                        IEnumerable<Providers.Entities.QueryPaymentTransaction> queryPaymentTransactionsFromFile = reader.GetRecords<Providers.Entities.QueryPaymentTransaction>();
                        foreach (Providers.Entities.QueryPaymentTransaction queryPaymentTransaction in queryPaymentTransactionsFromFile)
                        {
                            if (logResponse)
                            {
                                this._logger.Value.Info(() => $"{nameof(TrustCommercePaymentProvider)}::{nameof(this.GetPaymentTransactionsAsync)} - Start: {beginDate.ToString(Format.DateTimeFormat_MMddyyyyHHmmss)}, End: {endDate.ToString(Format.DateTimeFormat_MMddyyyyHHmmss)}, Transaction: {queryPaymentTransaction.ToJSON(false, true, true)} ");
                            }

                            QueryPaymentTransaction returnValue = new QueryPaymentTransaction
                            {
                                PaymentProcessorResponseStatus = MapStatusCode(queryPaymentTransaction.StatusName, queryPaymentTransaction.CustomField7),
                                TransactionDateTime = toDateTime(queryPaymentTransaction.TransDate),
                                Amount = toDecimal(queryPaymentTransaction.Amount),
                                PaymentType = MapActionCode(queryPaymentTransaction.ActionName),
                                TransactionId = queryPaymentTransaction.Transid,
                                RefTransid = queryPaymentTransaction.RefTransid,
                                CustId = queryPaymentTransaction.CustId
                            };
                            // Unfortunately, the Trust Commerce batching doesn't quite work.
                            // We get overlaps and therefore we get duplicates.
                            // this prevents us from adding duplicates to the return collection
                            int hashCode = returnValue.GetHashCode();
                            if (!returnValues.ContainsKey(hashCode))
                            {
                                returnValues[hashCode] = returnValue;
                                recordCount++;
                            }
                        }
                    }

                    offset += recordCount;
                } while (recordCount > 0);

                queryPaymentTransactions.AddRange(returnValues.Values);
            }

            return queryPaymentTransactions;
        }

        public string PaymentProcessorName
        {
            get { return "TrustCommerce"; }
        }

        async Task<BillingIdResult> IPaymentProvider.StoreBankAccountBillingIdAsync(PaymentMethod paymentMethod, string accountNumber, string routingNumber)
        {
            StoreBillingIdRequest request = new StoreBillingIdRequest
            {
                BillingId = paymentMethod.BillingId,
                Name = paymentMethod.FirstNameLastName,
                Media = TrustCommerceConstants.Media.Ach,
                Account = accountNumber,
                Routing = routingNumber,
                Savings = paymentMethod.PaymentMethodType == PaymentMethodTypeEnum.AchSavings
            };

            return await this.TryStoreBillingIdAsync(request).ConfigureAwait(false);
        }

        async Task<BillingIdResult> IPaymentProvider.StoreCreditDebitCardBillingIdAsync(PaymentMethod paymentMethod, string accountNumber, string securityCode)
        {
            PaymentMethodBillingAddress billingAddress = paymentMethod.BillingAddresses.First();
            Card card = new Card(
                string.Empty,
                paymentMethod.NameOnCard,
                accountNumber,
                paymentMethod.ExpDate,
                billingAddress.Address1,
                string.Empty,
                billingAddress.City,
                billingAddress.State,
                billingAddress.Zip,
                securityCode,
                paymentMethod.BillingId);

            BillingIdResult result = await this.VerifyAsync(card).ConfigureAwait(false);
            if (result.Succeeded)
            {
                result = await this.StoreAsync(card);
            }

            return result;
        }

        /// <summary>
        /// Updates the expiration date MMYY of an existing credit card
        /// </summary>
        /// <param name="billingId">BillingId of credit card account already stored in TC Citadel</param>
        /// <param name="expiration">Expiration in MMYY format</param>
        /// <param name="cvv">three digit cvv code</param>
        /// <returns></returns>
        public async Task<bool> UpdateCreditCardExpirationAsync(string billingId, string expiration, string cvv)
        {
            IDictionary<string, string> request = this.GetBasicRequest();
            SetAction(request, TrustCommerceConstants.TransactionType.Store);
            request[TrustCommerceConstants.Fields.General.BillingId] = billingId;
            request[TrustCommerceConstants.Fields.CreditCard.Required.Exp] = expiration;
            request[TrustCommerceConstants.Fields.CreditCard.CardVerification.Cvv] = cvv;

            IReadOnlyDictionary<string, string> response = await this.GetTcApiRequestResponseAsync(request).ConfigureAwait(false);
            bool isAccepted = this.Accepted(response) || this.Succeeded(response);
            string responseFlattened = string.Join(";", response.Select(x => x.Key + "=" + x.Value).ToArray());
            if (!isAccepted)
            {
                this._logger.Value.Fatal(() => $"TrustCommerceProvider::UpdateCreditCardExpirationAsync - Failed to update credit card expiration date to {expiration} for BillingId {billingId}. Response: {responseFlattened}");
            }
            else
            {
                this._logger.Value.Info(() => $"TrustCommerceProvider::UpdateCreditCardExpirationAsync - Updated credit card expiration date to {expiration} for BillingId {billingId}. Response: {responseFlattened}");
            }
            return isAccepted;
        }

        async Task<IList<PaymentProcessorResponse>> IPaymentProvider.SubmitPaymentsAsync(IList<Payment> payments, PaymentProcessor paymentProcessor)
        {
            if (payments.Any(x => !this._paymentConfigurationService.Value.IsPaymentMethodTypeAccepted(x.PaymentMethod.PaymentMethodType)))
            {
                throw new ConfigurationErrorsException("acceptedPaymentMethodTypes");
            }

            IList<PaymentProcessorResponse> responses = new List<PaymentProcessorResponse>();

            //Guarantor
            foreach (IGrouping<int, Payment> guarantorPayments in payments.GroupBy(x => x.Guarantor.VpGuarantorId))
            {
                var differentPaymentConfigurations = guarantorPayments.Select(x => new { x.PaymentMethod.BillingId, x.PaymentMethod.PaymentMethodType, x.MerchantAccountId }).Distinct().ToList();

                //PaymentMethod and MerchantAccount
                foreach (var paymentConfig in differentPaymentConfigurations)
                {
                    IList<Payment> paymentsInQuestion = guarantorPayments.Where(x => x.PaymentMethod.BillingId == paymentConfig.BillingId && x.MerchantAccountId == paymentConfig.MerchantAccountId).ToList();
                    Payment firstPayment = paymentsInQuestion.FirstOrDefault();
                    //If there aren't any payments or an amount to charge, don't do it.
                    if (paymentsInQuestion.Count == 0 || paymentsInQuestion.Sum(x => x.ActualPaymentAmount) <= 0 || firstPayment == null)
                    {
                        continue;
                    }

                    IDictionary<string, string> request = this.GetBasicRequest();

                    bool hasMerchantAccount = paymentConfig.MerchantAccountId > 0;
                    if (hasMerchantAccount)
                    {
                        this.AssignMerchantAccount(ref request, firstPayment);
                    }

                    SetAction(request, TrustCommerceConstants.TransactionType.Sale);

                    request[TrustCommerceConstants.Fields.General.BillingId] = paymentConfig.BillingId;
                    request[TrustCommerceConstants.Fields.General.Amount] = $"{(int)(paymentsInQuestion.Sum(x => x.ActualPaymentAmount) * 100m):D4}";
                    request[TrustCommerceConstants.Fields.Custom.CustomField2] = firstPayment.Guarantor.User.FirstName;
                    request[TrustCommerceConstants.Fields.Custom.CustomField3] = firstPayment.Guarantor.User.LastName;
                    request[TrustCommerceConstants.Fields.Custom.CustomField4] = GetBillingSystemName(firstPayment);
                    request[TrustCommerceConstants.Fields.Custom.CustomField6] = GetHsGuarantorAccountIdList(firstPayment);
                    request[TrustCommerceConstants.Fields.Custom.CustomField9] = firstPayment.Guarantor.VpGuarantorId.ToString(CultureInfo.InvariantCulture);
                    request[TrustCommerceConstants.Fields.Custom.CustomField5] = GetCustomField5Text(PaymentSystemTypeEnum.VisitPay);
                    request[TrustCommerceConstants.Fields.Custom.CustomField12] = GetCustomField12Text(PaymentSystemTypeEnum.VisitPay);

                    string rawResponse = await this.GetTcApiBaseRequestRawResponseAsync(request, "trans").ConfigureAwait(false);

                    IReadOnlyDictionary<string, string> response = this.ParseResponse(rawResponse);
                    ResponseValues responseValues = GetResponseValues(response);
                    PaymentProcessorResponse paymentProcessorResponse = new PaymentProcessorResponse
                    {
                        PaymentProcessorSourceKey = responseValues.TransactionId ?? string.Empty,
                        PaymentProcessor = paymentProcessor,
                        PaymentProcessorResponseStatus = MapStatusCode(responseValues),
                        RawResponse = rawResponse,
                        ResponseField1 = responseValues.TransactionId,
                        ResponseField2 = responseValues.Status,
                        ResponseField3 = responseValues.AuthCode,
                        ResponseField4 = responseValues.Avs,
                        ResponseField5 = responseValues.DeclineType,
                        ResponseField6 = responseValues.Error,
                        ResponseField7 = responseValues.ErrorType,
                        ResponseField8 = responseValues.Offenders,
                        ResponseField9 = responseValues.ResponseCode,
                        ResponseField10 = responseValues.ResponseCodeDescriptor,
                        SnapshotTotalPaymentAmount = paymentsInQuestion.Sum(x => x.ActualPaymentAmount),
                        PaymentMethod = firstPayment.PaymentMethod,
                        ErrorMessage = (responseValues.ErrorType ?? string.Empty) + "-" + (responseValues.Error ?? string.Empty),
                        PaymentSystemType = PaymentSystemTypeEnum.VisitPay
                    };

                    foreach (Payment payment in paymentsInQuestion)
                    {
                        PaymentPaymentProcessorResponse current = new PaymentPaymentProcessorResponse
                        {
                            InsertDate = DateTime.UtcNow,
                            Payment = payment,
                            PaymentProcessorResponse = paymentProcessorResponse,
                            SnapshotPaymentAmount = payment.ActualPaymentAmount
                        };
                        payment.PaymentPaymentProcessorResponses.Add(current);
                        paymentProcessorResponse.PaymentPaymentProcessorResponses.Add(current);
                    }

                    responses.Add(paymentProcessorResponse);
                }
            }

            return responses;
        }

        void AssignMerchantAccount(ref IDictionary<string, string> request, Payment payment)
        {
            this.AssignMerchantAccount(ref request, payment.MerchantAccount);
        }

        void AssignMerchantAccount(ref IDictionary<string, string> request, MerchantAccount merchantAccount)
        {
            // https://vault.trustcommerce.com/downloads/TC_Link_API_Developer_Guide.pdf
            // Shared Access BillingIDs
            // page 76

            //Get app settings keys from the merchant account row in the database
            PaymentProviderCredentials paymentProviderCredentials = this._applicationSettingsService.Value.GetPaymentProviderCredentials(
                merchantAccount.CustomerIdSettingsKey,
                merchantAccount.PasswordSettingsKey,
                merchantAccount.PasswordQueryApiSettingsKey,
                merchantAccount.AchCustomerIdSettingsKey,
                merchantAccount.AchPasswordSettingsKey);

            string merchantAccountCustomerId = paymentProviderCredentials.TrustCommerceCustomerId;
            string merchantAccountPassword = paymentProviderCredentials.TrustCommercePassword;

            if (string.IsNullOrWhiteSpace(merchantAccountCustomerId) || string.IsNullOrWhiteSpace(merchantAccountPassword))
            {
                throw new ConfigurationErrorsException("merchantAccountCustomerId, merchantAccountPassword");
            }

            bool isSameCustomer = (request[TrustCommerceConstants.Fields.General.CustomerId] == merchantAccountCustomerId);

            if (!isSameCustomer)
            {
                // Set MASTER merchant account CustomerId and Password fields from the existing CustomerId and Password fields (should be from the primary merchant account)
                // All payment method billing Ids should belong to the primary (not umbrella) merchant account
                request[TrustCommerceConstants.Fields.General.MasterCustomerId] = request[TrustCommerceConstants.Fields.General.CustomerId];
                request[TrustCommerceConstants.Fields.General.MasterPassword] = request[TrustCommerceConstants.Fields.General.Password];

                //Overwrite CustomerId and Password with merchant account CustomerId and Password
                request[TrustCommerceConstants.Fields.General.CustomerId] = merchantAccountCustomerId;
                request[TrustCommerceConstants.Fields.General.Password] = merchantAccountPassword;
            }
        }
        async Task<PaymentProcessorResponse> IPaymentReversalProvider.SubmitPaymentsRefundAsync(IList<Payment> originalPayments, IList<Payment> reversalPayments, PaymentProcessor paymentProcessor, decimal? amount)
        {
            string originalPaymentTransactionId = this.ValidateReversalParameters(originalPayments, reversalPayments, paymentProcessor);

            PaymentProcessorResponse paymentProcessorResponse = await this.SubmitPaymentsRefundAsync(originalPaymentTransactionId, originalPayments, paymentProcessor, amount, PaymentSystemTypeEnum.VisitPay).ConfigureAwait(false);
            paymentProcessorResponse.SnapshotTotalPaymentAmount = reversalPayments.Sum(x => x.ScheduledPaymentAmount);

            foreach (Payment reversalPayment in reversalPayments)
            {
                PaymentPaymentProcessorResponse current = new PaymentPaymentProcessorResponse
                {
                    InsertDate = DateTime.UtcNow,
                    Payment = reversalPayment,
                    PaymentProcessorResponse = paymentProcessorResponse,
                    SnapshotPaymentAmount = reversalPayment.ScheduledPaymentAmount
                };
                reversalPayment.PaymentPaymentProcessorResponses.Add(current);
                paymentProcessorResponse.PaymentPaymentProcessorResponses.Add(current);
            }

            return paymentProcessorResponse;
        }

        async Task<PaymentProcessorResponse> IPaymentReversalProvider.SubmitPaymentsVoidAsync(IList<Payment> originalPayments, IList<Payment> reversalPayments, PaymentProcessor paymentProcessor)
        {
            string originalPaymentTransactionId = this.ValidateReversalParameters(originalPayments, reversalPayments, paymentProcessor);

            PaymentProcessorResponse paymentProcessorResponse = await this.SubmitPaymentsVoidAsync(originalPaymentTransactionId, originalPayments, paymentProcessor, PaymentSystemTypeEnum.VisitPay).ConfigureAwait(false);
            paymentProcessorResponse.SnapshotTotalPaymentAmount = reversalPayments.Sum(x => x.ScheduledPaymentAmount);

            foreach (Payment reversalPayment in reversalPayments)
            {
                PaymentPaymentProcessorResponse current = new PaymentPaymentProcessorResponse
                {
                    InsertDate = DateTime.UtcNow,
                    Payment = reversalPayment,
                    PaymentProcessorResponse = paymentProcessorResponse,
                    SnapshotPaymentAmount = reversalPayment.ScheduledPaymentAmount
                };
                reversalPayment.PaymentPaymentProcessorResponses.Add(current);
                paymentProcessorResponse.PaymentPaymentProcessorResponses.Add(current);
            }

            return paymentProcessorResponse;
        }

        private string ValidateReversalParameters(GuestPayEntities.Payment originalPayment, GuestPayEntities.Payment reversalPayment, PaymentProcessor paymentProcessor)
        {
            if (originalPayment == null)
            {
                throw new ArgumentNullException(nameof(originalPayment));
            }

            string originalPaymentTransactionId = originalPayment.TransactionId;
            if (string.IsNullOrEmpty(originalPaymentTransactionId))
            {
                throw new ArgumentException("Original TransactionId missing", nameof(originalPayment));
            }

            if (paymentProcessor == null)
            {
                throw new ArgumentNullException(nameof(paymentProcessor));
            }

            return originalPaymentTransactionId;
        }

        private string ValidateReversalParameters(IList<Payment> originalPayments, ICollection<Payment> reversalPayments, PaymentProcessor paymentProcessor)
        {
            if (originalPayments.IsNullOrEmpty())
            {
                throw new ArgumentNullException(nameof(originalPayments));
            }

            if (reversalPayments.IsNullOrEmpty())
            {
                throw new ArgumentNullException(nameof(reversalPayments));
            }

            if (reversalPayments.Count != originalPayments.Count)
            {
                throw new ArgumentException("Number of reversal payments must match number of original payments,", nameof(reversalPayments));
            }

            string originalPaymentTransactionId = originalPayments.First().TransactionId;
            if (string.IsNullOrEmpty(originalPaymentTransactionId))
            {
                throw new ArgumentException("Original TransactionId missing", nameof(originalPayments));
            }

            if (originalPayments.Any(x => x.TransactionId != originalPaymentTransactionId))
            {
                throw new ArgumentException("Original TransactionId must be from the same original transaction", nameof(originalPayments));
            }

            if (paymentProcessor == null)
            {
                throw new ArgumentNullException(nameof(paymentProcessor));
            }

            return originalPaymentTransactionId;
        }

        private async Task<PaymentProcessorResponse> SubmitPaymentsRefundAsync(string originalPaymentTransactionId, IList<Payment> originalPayments, PaymentProcessor paymentProcessor, decimal? amount, PaymentSystemTypeEnum paymentSystemTypeEnum)
        {
            Dictionary<string, string> request = this.GetBasicRequest();
            SetAction(request, TrustCommerceConstants.TransactionType.Credit);
            request[TrustCommerceConstants.Fields.General.TransId] = originalPaymentTransactionId;
            if (amount.HasValue)
            {
                request[TrustCommerceConstants.Fields.General.Amount] = $"{(int)(amount.Value * 100m):D4}";
            }

            if (originalPayments.IsNotNullOrEmpty())
            {
                Payment originalPayment = originalPayments.First();
                request[TrustCommerceConstants.Fields.Custom.CustomField2] = originalPayment.Guarantor.User.FirstName;
                request[TrustCommerceConstants.Fields.Custom.CustomField3] = originalPayment.Guarantor.User.LastName;
                request[TrustCommerceConstants.Fields.Custom.CustomField4] = GetBillingSystemName(originalPayment);
                request[TrustCommerceConstants.Fields.Custom.CustomField6] = GetHsGuarantorAccountIdList(originalPayment);
            }
            request[TrustCommerceConstants.Fields.Custom.CustomField5] = GetCustomField5Text(paymentSystemTypeEnum);
            request[TrustCommerceConstants.Fields.Custom.CustomField12] = GetCustomField12Text(paymentSystemTypeEnum);

            string rawResponse = await this.GetTcApiBaseRequestRawResponseAsync(request, "trans").ConfigureAwait(false);

            IReadOnlyDictionary<string, string> response = ParseResponse(rawResponse);
            ResponseValues responseValues = GetResponseValues(response);
            PaymentProcessorResponse paymentProcessorResponse = new PaymentProcessorResponse
            {
                PaymentProcessorSourceKey = responseValues.TransactionId ?? string.Empty,
                PaymentProcessor = paymentProcessor,
                PaymentProcessorResponseStatus = MapStatusCode(responseValues),
                RawResponse = rawResponse,
                ResponseField1 = responseValues.TransactionId,
                ResponseField2 = responseValues.Status,
                ResponseField3 = responseValues.AuthCode,
                ResponseField4 = responseValues.Avs,
                ResponseField5 = responseValues.DeclineType,
                ResponseField6 = responseValues.Error,
                ResponseField7 = responseValues.ErrorType,
                ResponseField8 = responseValues.Offenders,
                ResponseField9 = responseValues.ResponseCode,
                ResponseField10 = responseValues.ResponseCodeDescriptor,
                ErrorMessage = (responseValues.ErrorType ?? string.Empty) + "-" + (responseValues.Error ?? string.Empty),
                PaymentSystemType = PaymentSystemTypeEnum.VisitPay
            };

            return paymentProcessorResponse;
        }

        private async Task<PaymentProcessorResponse> SubmitPaymentsVoidAsync(string originalPaymentTransactionId, IList<Payment> originalPayments, PaymentProcessor paymentProcessor, PaymentSystemTypeEnum paymentSystemTypeEnum)
        {
            IDictionary<string, string> request = this.GetBasicRequest();
            SetAction(request, TrustCommerceConstants.TransactionType.Void);
            request[TrustCommerceConstants.Fields.General.TransId] = originalPaymentTransactionId;

            if (originalPayments.IsNotNullOrEmpty())
            {
                Payment originalPayment = originalPayments.First();
                request[TrustCommerceConstants.Fields.Custom.CustomField2] = originalPayment.Guarantor.User.FirstName;
                request[TrustCommerceConstants.Fields.Custom.CustomField3] = originalPayment.Guarantor.User.LastName;
                request[TrustCommerceConstants.Fields.Custom.CustomField4] = GetBillingSystemName(originalPayment);
                request[TrustCommerceConstants.Fields.Custom.CustomField6] = GetHsGuarantorAccountIdList(originalPayment);
            }
            request[TrustCommerceConstants.Fields.Custom.CustomField5] = GetCustomField5Text(paymentSystemTypeEnum);
            request[TrustCommerceConstants.Fields.Custom.CustomField12] = GetCustomField12Text(paymentSystemTypeEnum);

            string rawResponse = await this.GetTcApiBaseRequestRawResponseAsync(request, "trans").ConfigureAwait(false);

            IReadOnlyDictionary<string, string> response = ParseResponse(rawResponse);
            ResponseValues responseValues = GetResponseValues(response);
            PaymentProcessorResponse paymentProcessorResponse = new PaymentProcessorResponse
            {
                PaymentProcessorSourceKey = responseValues.TransactionId ?? string.Empty,
                PaymentProcessor = paymentProcessor,
                PaymentProcessorResponseStatus = MapStatusCode(responseValues),
                RawResponse = rawResponse,
                ResponseField1 = responseValues.TransactionId,
                ResponseField2 = responseValues.Status,
                ResponseField3 = responseValues.AuthCode,
                ResponseField4 = responseValues.Avs,
                ResponseField5 = responseValues.DeclineType,
                ResponseField6 = responseValues.Error,
                ResponseField7 = responseValues.ErrorType,
                ResponseField8 = responseValues.Offenders,
                ResponseField9 = responseValues.ResponseCode,
                ResponseField10 = responseValues.ResponseCodeDescriptor,
                ErrorMessage = (responseValues.ErrorType ?? string.Empty) + "-" + (responseValues.Error ?? string.Empty),
                PaymentSystemType = PaymentSystemTypeEnum.VisitPay
            };

            return paymentProcessorResponse;
        }

        private static PaymentProcessorResponseStatusEnum MapStatusCode(ResponseValues responseValues)
        {
            //this is a special case to satisfy VPNG-1870
            if (responseValues.ResponseCodeValue.HasValue
                && (responseValues.ResponseCodeValue == 500 || responseValues.ResponseCodeValue == 102))
            {
                return PaymentProcessorResponseStatusEnum.CallToProcessorFailed;
            }
            return MapStatusCode(responseValues.Status, responseValues.ErrorType);
        }

        private static PaymentProcessorResponseStatusEnum MapStatusCode(string responseStatus, string errorType)
        {
            switch (responseStatus.ToLowerInvariant())
            {
                case TrustCommerceConstants.Codes.ReturnParameters.Accepted:
                    return PaymentProcessorResponseStatusEnum.Accepted;
                case TrustCommerceConstants.Codes.ReturnParameters.Approved:
                    return PaymentProcessorResponseStatusEnum.Approved;
                case TrustCommerceConstants.Codes.ReturnParameters.BadData:
                    return PaymentProcessorResponseStatusEnum.BadData;
                case TrustCommerceConstants.Codes.ReturnParameters.Decline:
                case TrustCommerceConstants.Codes.ReturnParameters.Declined:
                    return PaymentProcessorResponseStatusEnum.Decline;
                case TrustCommerceConstants.Codes.ReturnParameters.Error:
                    if (string.Equals(errorType, "linkfailure", StringComparison.CurrentCultureIgnoreCase))
                    {
                        return PaymentProcessorResponseStatusEnum.LinkFailure;
                    }
                    return PaymentProcessorResponseStatusEnum.Error;
                case TrustCommerceConstants.Codes.ErrorType.CallToProcessorFailed:
                    return PaymentProcessorResponseStatusEnum.CallToProcessorFailed;
                case TrustCommerceConstants.Codes.ErrorType.LinkFailure:
                    return PaymentProcessorResponseStatusEnum.LinkFailure;
                case TrustCommerceConstants.Codes.ReturnParameters.Rejected:
                    return PaymentProcessorResponseStatusEnum.Unknown;
                default:
                    return PaymentProcessorResponseStatusEnum.Unknown;
            }
        }

        private static string GetCustomField12Text(PaymentSystemTypeEnum paymentSystemTypeEnum)
        {
            switch (paymentSystemTypeEnum)
            {
                case PaymentSystemTypeEnum.VisitPay:
                    return "NGVP";
                case PaymentSystemTypeEnum.GuestPay:
                    return "GuestPay";
                case PaymentSystemTypeEnum.PaymentApi:
                    return "PAPI";
                default:
                    return string.Empty;
            }
        }

        private static string GetCustomField5Text(PaymentSystemTypeEnum paymentSystemTypeEnum)
        {
            switch (paymentSystemTypeEnum)
            {
                case PaymentSystemTypeEnum.VisitPay:
                    return "No";
                case PaymentSystemTypeEnum.GuestPay:
                case PaymentSystemTypeEnum.PaymentApi:
                    return "Yes";
                default:
                    return string.Empty;
            }
        }

        private static PaymentTypeEnum MapActionCode(string action)
        {
            switch (action.ToLowerInvariant())
            {
                case TrustCommerceConstants.TransactionType.Void:
                    return PaymentTypeEnum.Void;
                case TrustCommerceConstants.TransactionType.Credit:
                    return PaymentTypeEnum.Refund;
                default:
                    return PaymentTypeEnum.Unknown;
            }
        }

        private static string GetHsGuarantorAccountIdList(Payment payment)
        {
            string hsGuarantorAccountIdList = string.Join(",",
                payment.Guarantor.HsGuarantorMaps
                    .Where(g => g != null)
                    .Select(g => g.SourceSystemKey)
                    .Distinct());
            if (string.IsNullOrWhiteSpace(hsGuarantorAccountIdList) && payment.Guarantor != null && payment.Guarantor.HsGuarantorMaps != null)
            {
                hsGuarantorAccountIdList = string.Join(",", payment.Guarantor.HsGuarantorMaps.Select(x => x.SourceSystemKey));
            }
            return hsGuarantorAccountIdList;
        }

        private static string GetBillingSystemName(Payment payment)
        {
            string billingSystemName = payment.PaymentAllocations.Select(pd => (pd.PaymentVisit.BillingSystemId ?? pd.PaymentVisit.MatchedBillingSystemId).ToString())
                .Distinct()
                .FirstOrDefault();

            if (string.IsNullOrWhiteSpace(billingSystemName) && payment.Guarantor?.HsGuarantorMaps != null)
            {
                billingSystemName = string.Join(",", payment.Guarantor.HsGuarantorMaps.Select(x => x.BillingSystem.BillingSystemId));
            }

            return billingSystemName;
        }

        private Dictionary<string, string> GetBasicRequest()
        {
            return new Dictionary<string, string>
            {
                {TrustCommerceConstants.Fields.General.CustomerId, this._paymentConfigurationService.Value.PaymentProviderCredentials.TrustCommerceCustomerId},
                {TrustCommerceConstants.Fields.General.Password, this._paymentConfigurationService.Value.PaymentProviderCredentials.TrustCommercePassword},
                {TrustCommerceConstants.Fields.General.Demo, this._trustCommerceIsLiveMode.Value ? "n" : "y"}
            };
        }

        private static void SetAction(IDictionary<string, string> request, string action)
        {
            request[TrustCommerceConstants.Fields.General.Action] = action;
        }

        private static void AddCardToRequest(IDictionary<string, string> request, Card card, bool isCountryUsa = true)
        {
            request[TrustCommerceConstants.Fields.CreditCard.Required.Cc] = card.CardNumber;
            request[TrustCommerceConstants.Fields.CreditCard.Required.Exp] = card.ExpDate;
            request[TrustCommerceConstants.Fields.General.Name] = card.NameOnCard;
            if (!string.IsNullOrEmpty(card.Address1))
            {
                request[TrustCommerceConstants.Fields.CreditCard.Optional.Address1] = card.Address1;
            }
            if (!string.IsNullOrEmpty(card.Address2))
            {
                request[TrustCommerceConstants.Fields.CreditCard.Optional.Address2] = card.Address2;
            }
            if (!string.IsNullOrEmpty(card.City))
            {
                request[TrustCommerceConstants.Fields.CreditCard.Optional.City] = card.City;
            }

            if (!string.IsNullOrEmpty(card.State))
            {
                request[isCountryUsa ? TrustCommerceConstants.Fields.CreditCard.Optional.State : TrustCommerceConstants.Fields.Custom.CustomField13] = card.State;
            }
            if (!string.IsNullOrEmpty(card.Zip))
            {
                request[isCountryUsa ? TrustCommerceConstants.Fields.CreditCard.Optional.Zip : TrustCommerceConstants.Fields.Custom.CustomField14] = card.Zip;
            }

            if (!string.IsNullOrEmpty(card.Cvv))
            {
                request[TrustCommerceConstants.Fields.CreditCard.CardVerification.Cvv] = card.Cvv;
            }
            if (!string.IsNullOrEmpty(card.BillingId))
            {
                request[TrustCommerceConstants.Fields.General.BillingId] = card.BillingId;
            }
        }

        private async Task<BillingIdResult> SubmitBillingIdRequestAsync(IDictionary<string, string> request)
        {
            IReadOnlyDictionary<string, string> responseItems = await this.GetTcApiRequestResponseAsync(request).ConfigureAwait(false);

            return new BillingIdResult
            {
                BillingID = GetResponseValue(responseItems, TrustCommerceConstants.Fields.General.BillingId),
                GatewayToken = GetResponseValue(responseItems, TrustCommerceConstants.Fields.General.TransId),
                Succeeded = this.Succeeded(responseItems),
                Response = responseItems
            };
        }

        private async Task<IReadOnlyDictionary<string, string>> GetTcApiRequestResponseAsync(IDictionary<string, string> request)
        {
            string responseText = await this.GetTcApiBaseRequestRawResponseAsync(request, "trans").ConfigureAwait(false);
            return ParseResponse(responseText);
        }

        public IReadOnlyDictionary<string, string> ParseResponse(string rawResponse)
        {
            if (rawResponse.IsNullOrEmpty())
            {
                return new Dictionary<string, string>();
            }

            return rawResponse.Split(new[] { "\r\n", "\n" }, StringSplitOptions.RemoveEmptyEntries)
                .Select(x => x.Split('='))
                .ToDictionary(x => x[0], x => x.Length > 1 ? x[1].Trim() : null);
        }

        private bool Succeeded(IReadOnlyDictionary<string, string> responseItems)
        {
            return GetResponseValue(responseItems, "status") == TrustCommerceConstants.Codes.ReturnParameters.Approved && this._paymentConfigurationService.Value.IsAvsCodeAccepted(GetResponseValue(responseItems, "avs"));
        }

        private bool Accepted(IReadOnlyDictionary<string, string> responseItems)
        {
            return GetResponseValue(responseItems, "status") == TrustCommerceConstants.Codes.ReturnParameters.Accepted;
        }

        private static string GetResponseValue(IReadOnlyDictionary<string, string> responseItems, string name)
        {
            return responseItems.ContainsKey(name) ? responseItems[name] : null;
        }

        private static ResponseValues GetResponseValues(IReadOnlyDictionary<string, string> responseItems)
        {
            return new ResponseValues
            {
                Avs = GetResponseValue(responseItems, TrustCommerceConstants.Codes.ReturnParameters.Avs),
                AuthCode = GetResponseValue(responseItems, TrustCommerceConstants.Codes.ReturnParameters.AuthCode),
                DeclineType = GetResponseValue(responseItems, TrustCommerceConstants.Codes.ReturnParameters.DeclineType),
                Error = GetResponseValue(responseItems, TrustCommerceConstants.Codes.ReturnParameters.Error),
                ErrorType = GetResponseValue(responseItems, TrustCommerceConstants.Codes.ReturnParameters.ErrorType),
                Offenders = GetResponseValue(responseItems, TrustCommerceConstants.Codes.ReturnParameters.Offenders),
                ResponseCode = GetResponseValue(responseItems, TrustCommerceConstants.Codes.ReturnParameters.ResponseCode),
                ResponseCodeDescriptor = GetResponseValue(responseItems, TrustCommerceConstants.Codes.ReturnParameters.ResponseCodeDescriptor),
                Status = GetResponseValue(responseItems, TrustCommerceConstants.Codes.ReturnParameters.Status),
                TransactionId = GetResponseValue(responseItems, TrustCommerceConstants.Codes.ReturnParameters.TransactionId)
            };
        }

        private async Task<BillingIdResult> VerifyAsync(Card card)
        {
            Dictionary<string, string> request = this.GetBasicRequest();
            SetAction(request, TrustCommerceConstants.TransactionType.Verify);
            AddCardToRequest(request, card);
            return await this.SubmitBillingIdRequestAsync(request).ConfigureAwait(false);
        }

        private async Task<BillingIdResult> StoreAsync(Card card)
        {
            Dictionary<string, string> request = this.GetBasicRequest();
            SetAction(request, TrustCommerceConstants.TransactionType.Store);
            request["verify"] = "y";
            AddCardToRequest(request, card);
            return await this.SubmitBillingIdRequestAsync(request).ConfigureAwait(false);
        }

        private async Task<BillingIdResult> TryStoreBillingIdAsync(StoreBillingIdRequest request)
        {
            StoreBillingIdResponse response = await this.StoreBillingIdAsync(request).ConfigureAwait(false);
            bool succeeded = this.WasSuccessful(response);

            return new BillingIdResult
            {
                Succeeded = succeeded,
                BillingID = response.BillingId,
                GatewayToken = response.TransactionId,
                Response = response.Response
            };
        }

        private bool WasSuccessful(StoreBillingIdResponse response)
        {
            return response.Status == TrustCommerceConstants.Codes.ReturnParameters.Accepted
                   || response.Status == TrustCommerceConstants.Codes.ReturnParameters.Approved;
        }

        private async Task<StoreBillingIdResponse> StoreBillingIdAsync(StoreBillingIdRequest requestFields)
        {
            #region request / response

            List<KeyValuePair<string, string>> request = this.CreateTcApiRequest(TrustCommerceConstants.TransactionType.Store);
            request.Add(new KeyValuePair<string, string>(TrustCommerceConstants.Fields.General.Media, requestFields.Media));
            request.Add(new KeyValuePair<string, string>(TrustCommerceConstants.Fields.General.Name, requestFields.Name));

            if (requestFields.Media == TrustCommerceConstants.Media.Ach)
            {
                request.Add(new KeyValuePair<string, string>(TrustCommerceConstants.Fields.Ach.Required.Account, requestFields.Account));
                request.Add(new KeyValuePair<string, string>(TrustCommerceConstants.Fields.Ach.Required.Routing, requestFields.Routing));
                if (requestFields.Savings)
                {
                    request.Add(new KeyValuePair<string, string>(TrustCommerceConstants.Fields.Ach.Optional.Savings, "y"));
                }
            }
            else
            {
                request.Add(new KeyValuePair<string, string>(TrustCommerceConstants.TransactionType.Verify, "y"));
                request.Add(new KeyValuePair<string, string>(TrustCommerceConstants.Fields.CreditCard.Required.Cc, requestFields.CreditCardNumber));
                request.Add(new KeyValuePair<string, string>(TrustCommerceConstants.Fields.CreditCard.Optional.City, requestFields.City));
                request.Add(new KeyValuePair<string, string>(TrustCommerceConstants.Fields.CreditCard.Required.Exp, requestFields.CreditCardExpiration));
                request.Add(new KeyValuePair<string, string>(TrustCommerceConstants.Fields.CreditCard.Optional.State, requestFields.State));
                request.Add(new KeyValuePair<string, string>(TrustCommerceConstants.Fields.CreditCard.Optional.Zip, requestFields.Zip));
                request.Add(new KeyValuePair<string, string>(TrustCommerceConstants.Fields.CreditCard.CardVerification.Cvv, requestFields.CardVerificationValue));
            }

            if (!string.IsNullOrEmpty(requestFields.BillingId) && requestFields.BillingId != "0")
            {
                request.Add(new KeyValuePair<string, string>(TrustCommerceConstants.Fields.General.BillingId, requestFields.BillingId));
            }

            IReadOnlyDictionary<string, string> response = await this.GetTcApiRequestResponseAsync(request.ToDictionary(p => p.Key, p => p.Value));

            #endregion

            #region parse response

            string authCode;
            string avs;
            string billingId;
            string error;
            string errorType;
            string offenders;
            string responseCode;
            string responseCodeDescriptor;
            string status;
            string transactionId;

            response.TryGetValue(TrustCommerceConstants.Codes.ReturnParameters.AuthCode, out authCode);
            response.TryGetValue(TrustCommerceConstants.Codes.ReturnParameters.Avs, out avs);
            response.TryGetValue(TrustCommerceConstants.Fields.General.BillingId, out billingId);
            response.TryGetValue(TrustCommerceConstants.Codes.ReturnParameters.Error, out error);
            response.TryGetValue(TrustCommerceConstants.Codes.ReturnParameters.ErrorType, out errorType);
            response.TryGetValue(TrustCommerceConstants.Codes.ReturnParameters.Offenders, out offenders);
            response.TryGetValue(TrustCommerceConstants.Codes.ReturnParameters.ResponseCode, out responseCode);
            response.TryGetValue(TrustCommerceConstants.Codes.ReturnParameters.ResponseCodeDescriptor, out responseCodeDescriptor);
            response.TryGetValue(TrustCommerceConstants.Codes.ReturnParameters.Status, out status);
            response.TryGetValue(TrustCommerceConstants.Codes.ReturnParameters.TransactionId, out transactionId);

            StoreBillingIdResponse storeBillingIdResponse = new StoreBillingIdResponse
            {
                AuthCode = authCode,
                Avs = avs,
                BillingId = billingId,
                Error = error,
                ErrorType = errorType,
                Offenders = offenders,
                ResponseCode = responseCode,
                ResponseCodeDescriptor = responseCodeDescriptor,
                Status = status,
                TransactionId = transactionId
            };

            if (!string.IsNullOrEmpty(storeBillingIdResponse.Status) &&
                !string.IsNullOrEmpty(requestFields.BillingId) &&
                (storeBillingIdResponse.Status == TrustCommerceConstants.Codes.ReturnParameters.Accepted ||
                 storeBillingIdResponse.Status == TrustCommerceConstants.Codes.ReturnParameters.Approved))
            {
                storeBillingIdResponse.BillingId = requestFields.BillingId;
                storeBillingIdResponse.Response = response;
            }

            #endregion

            return storeBillingIdResponse;
        }

        private List<KeyValuePair<string, string>> CreateTcApiRequest(string action)
        {
            return new List<KeyValuePair<string, string>>
            {
                new KeyValuePair<string, string>(TrustCommerceConstants.Fields.General.Action, action),
                new KeyValuePair<string, string>(TrustCommerceConstants.Fields.General.CustomerId, this._paymentConfigurationService.Value.PaymentProviderCredentials.TrustCommerceCustomerId),
                new KeyValuePair<string, string>(TrustCommerceConstants.Fields.General.Password, this._paymentConfigurationService.Value.PaymentProviderCredentials.TrustCommercePassword),
                new KeyValuePair<string, string>(TrustCommerceConstants.Fields.General.Demo, this._trustCommerceIsLiveMode.Value ? "n" : "y")
            };
        }

        private async Task<string> GetTcApiQueryResponseAsync(IDictionary<string, string> request)
        {
            return await this.GetTcApiBaseRequestRawResponseAsync(request, "query").ConfigureAwait(false);
        }

        private async Task<string> GetTcApiBaseRequestRawResponseAsync(IDictionary<string, string> request, string type)
        {
            try
            {
                FormUrlEncodedContent formUrlEncodedContent = new FormUrlEncodedContent(request);
                HttpResponseMessage response = null;
                using (Timer.Start(s =>
                {
                    this._logger.Value.Debug(() => $"{nameof(TrustCommercePaymentProvider)}::{nameof(System.Net.Http.HttpClient.PostAsync)}: Request to {HttpClient.BaseAddress} completed in {s.ElapsedMilliseconds} ms");
                }))
                {
                    response = await HttpClient.PostAsync(type + "/", formUrlEncodedContent).ConfigureAwait(false);
                }

                response.EnsureSuccessStatusCode();
                if (response.IsSuccessStatusCode)
                {
                    return await response.Content.ReadAsStringAsync().ConfigureAwait(false);
                }

                if ((int)response.StatusCode >= 400 && (int)response.StatusCode < 500)
                {
                    //client side error
                    this._logger.Value.Fatal(() => $"TrustCommercePaymentProvider::GetTcApiBaseRequestRawResponseAsync - Something went wrong while calling TrustCommerce.  StatusCode - {response.StatusCode}");
                    return "status=CallToProcessorFailed\n";
                }

                if ((int)response.StatusCode >= 500 && (int)response.StatusCode < 600)
                {
                    //server side error
                    this._logger.Value.Fatal(() => $"TrustCommercePaymentProvider::GetTcApiBaseRequestRawResponseAsync - Something went wrong while calling TrustCommerce.  StatusCode - {response.StatusCode}");
                    //TrustCommerce will return a "LinkFailure" as "errortype=linkfailure" + Environment.NewLine + "status=error" with a success response
                    //return "status=LinkFailure\n";
                    return "status=CallToProcessorFailed";
                }

            }
            catch (Exception e)
            {
                this._logger.Value.Fatal(() => $"TrustCommercePaymentProvider::GetTcApiBaseRequestRawResponseAsync - Something went wrong while calling TrustCommerce.  Exception - {ExceptionHelper.AggregateExceptionToString(e)}");
            }
            return "status=CallToProcessorFailed\n";
        }

        private Dictionary<string, string> GetBasicQueryRequest()
        {
            return new Dictionary<string, string>
            {
                {TrustCommerceConstants.Fields.General.CustomerId, this._paymentConfigurationService.Value.PaymentProviderCredentials.TrustCommerceCustomerId},
                {TrustCommerceConstants.Fields.General.Password, this._paymentConfigurationService.Value.PaymentProviderCredentials.TrustCommercePasswordQueryApi}
            };
        }

        internal struct ResponseValues
        {
            internal string Avs { get; set; }
            internal string AuthCode { get; set; }
            internal string DeclineType { get; set; }
            internal string Error { get; set; }
            internal string ErrorType { get; set; }
            internal string Offenders { get; set; }
            internal string ResponseCode { get; set; }
            internal string ResponseCodeDescriptor { get; set; }
            internal string Status { get; set; }
            internal string TransactionId { get; set; }

            private int? _responseCodeValue;
            internal int? ResponseCodeValue
            {
                get
                {
                    if (!this._responseCodeValue.HasValue)
                    {
                        int value = 0;
                        if (int.TryParse(this.ResponseCode, out value))
                        {
                            this._responseCodeValue = value;
                        }
                    }
                    return this._responseCodeValue;
                }
            }
        }

        internal static class TrustCommerceConstants
        {
            internal static class Codes
            {
                internal static class ReturnParameters
                {
                    internal const string Approved = "approved";
                    internal const string Accepted = "accepted";
                    internal const string Rejected = "rejected";
                    internal const string Decline = "decline";
                    internal const string Declined = "declined";
                    internal const string DeclineType = "declinetype";
                    internal const string BadData = "baddata";
                    internal const string Error = "error";
                    internal const string ErrorType = "errortype";
                    internal const string Offenders = "offenders";
                    internal const string AuthCode = "authcode";
                    internal const string ResponseCode = "responsecode";
                    internal const string ResponseCodeDescriptor = "responsecodedescriptor";
                    internal const string Status = "status";
                    internal const string TransactionId = "transid";
                    internal const string Avs = "avs";
                }

                internal static class DeclineType
                {
                    internal const string Decline = "decline";
                    internal const string Avs = "avs";
                    internal const string Cvv = "cvv";
                    internal const string Call = "call";
                    internal const string ExpiredCard = "expiredcard";
                    internal const string CardError = "carderror";
                    internal const string AuthExpired = "authexpired";
                    internal const string Fraud = "fraud";
                    internal const string Blacklist = "blacklist";
                    internal const string Velocity = "velocity";
                    internal const string DailyLimit = "dailylimit";
                    internal const string WeeklyLimit = "weeklylimit";
                    internal const string MonthlyLimit = "monthlylimit";
                }

                internal static class BadData
                {
                    internal const string MissingFields = "missingfields";
                    internal const string ExtraFields = "extrafields";
                    internal const string BadFormat = "badformat";
                    internal const string BadLength = "badlength";
                    internal const string MerchantCantAccept = "merchantcantaccept";
                    internal const string Mismatch = "mismatch";
                }

                internal static class ErrorType
                {
                    internal const string CantConnect = "CantConnect";
                    internal const string DnsFailure = "DnsFailure";
                    internal const string LinkFailure = "linkfailure";
                    internal const string FailToProcess = "failtoprocess";
                    internal const string CallToProcessorFailed = "calltoprocessorfailed";
                }

                internal static class AvsReturnCodes
                {
                    internal const string X = "X";
                    internal const string Y = "Y";
                    internal const string A = "A";
                    internal const string W = "W";
                    internal const string Z = "Z";
                    internal const string N = "N";
                    internal const string U = "U";
                    internal const string G = "G";
                    internal const string R = "R";
                    internal const string E = "E";
                    internal const string S = "S";
                    internal const string _0 = "0";
                }
            }

            internal static class TransactionType
            {
                internal const string Credit = "credit";
                internal const string Void = "void";
                internal const string Sale = "sale";
                internal const string Store = "store";
                internal const string Verify = "verify";
            }

            internal static class Media
            {
                internal const string Ach = "ach";
            }

            internal static class Fields
            {
                internal static class General
                {
                    internal const string Media = "media";
                    internal const string Amount = "amount";
                    internal const string CustomerId = "custid";
                    internal const string Password = "password";
                    internal const string Action = "action";
                    internal const string Demo = "demo";
                    internal const string Ticket = "ticket";
                    internal const string Operator = "operator";
                    internal const string TransId = "transid";
                    internal const string BillingId = "billingid";
                    internal const string Name = "name";
                    internal const string MasterCustomerId = "master_custid";
                    internal const string MasterPassword = "master_password";
                }

                internal static class Query
                {
                    internal static class General
                    {
                        internal const string QueryType = "querytype";
                        internal const string Format = "format";
                        internal const string BeginDate = "begindate";
                        internal const string EndDate = "enddate";
                        internal const string Limit = "limit";
                        internal const string ShowCount = "showcount";
                        internal const string Offset = "offset";
                    }

                    internal static class QueryType
                    {
                        internal const string Chain = "chain";
                        internal const string Transaction = "transaction";
                        internal const string Summary = "summary";
                        internal const string BillingId = "billingid";
                    }

                    internal static class Format
                    {
                        internal const string Text = "text";
                        internal const string Html = "html";
                    }
                }

                internal static class Custom
                {
                    /// <summary>
                    /// paymentid (Whole Number Disabled Searchable)
                    /// </summary>
                    internal const string CustomField1 = "customfield1";
                    /// <summary>
                    /// First Name (Text Searchable)
                    /// </summary>
                    internal const string CustomField2 = "customfield2";
                    /// <summary>
                    /// Last Name (Text Searchable)
                    /// </summary>
                    internal const string CustomField3 = "customfield3";
                    /// <summary>
                    /// Billing System (Text Searchable)
                    /// </summary>
                    internal const string CustomField4 = "customfield4";
                    /// <summary>
                    /// Is Exception (Text Searchable)
                    /// </summary>
                    internal const string CustomField5 = "customfield5";
                    /// <summary>
                    /// Unique Identifier (Text Searchable)
                    /// </summary>
                    internal const string CustomField6 = "customfield6";
                    /// <summary>
                    /// controller (Text Non-Searchable)
                    /// </summary>
                    internal const string CustomField7 = "customfield7";
                    /// <summary>
                    /// action (Text Non-Searchable)
                    /// </summary>
                    internal const string CustomField8 = "customfield8";
                    /// <summary>
                    /// gid (Whole Number Non-Searchable)
                    /// </summary>
                    internal const string CustomField9 = "customfield9";
                    /// <summary>
                    /// Is Primary (Text Non-Searchable)
                    /// </summary>
                    internal const string CustomField10 = "customfield10";
                    /// <summary>
                    /// Account Type (Text Non-Searchable)
                    /// </summary>
                    internal const string CustomField11 = "customfield11";
                    /// <summary>
                    /// VisitPay Source (Text Searchable)
                    /// </summary>
                    internal const string CustomField12 = "customfield12";
                    /// <summary>
                    /// ProvinceState (Text Searchable)
                    /// </summary>
                    internal const string CustomField13 = "customfield13";
                    /// <summary>
                    /// PostalCode (Text Searchable)
                    /// </summary>
                    internal const string CustomField14 = "customfield14";
                }

                internal static class CreditCard
                {
                    internal static class Required
                    {
                        internal const string Amount = "amount";
                        internal const string Cc = "cc";
                        internal const string Exp = "exp";
                    }

                    internal static class CardVerification
                    {
                        internal const string Cvv = "cvv";
                        internal const string CheckCvv = "checkcvv";
                    }

                    internal static class CardPresent
                    {
                        internal const string Track1 = "track1";
                        internal const string Track2 = "track2";
                    }

                    internal static class Optional
                    {
                        internal const string Currency = "currency";
                        internal const string Avs = "avs";
                        internal const string Address1 = "address1";
                        internal const string Address2 = "address2";
                        internal const string City = "city";
                        internal const string State = "state";
                        internal const string Zip = "zip";
                        internal const string Country = "country";
                        internal const string Phone = "phone";
                        internal const string Email = "email";
                        internal const string Ip = "ip";
                        internal const string OfflineAuthCode = "offlineauthcode";
                    }
                }

                internal static class Ach
                {
                    internal static class Required
                    {
                        internal const string Routing = "routing";
                        internal const string Account = "account";
                    }

                    internal static class Optional
                    {
                        internal const string Savings = "savings";
                    }
                }
            }
        }
    }
}
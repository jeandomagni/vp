﻿namespace Ivh.Provider.FinanceManagement.TrustCommerce
{
    internal class AuthAndCaptureRequest
    {
        /// <summary>
        /// number (3-8).  Transaction amount in cents (for example, $1.00 would be submitted as "100")
        /// </summary>
        public int AmountInCents { get; set; }

        /// <summary>
        /// string (1024).  payment id
        /// </summary>
        public string CustomField1 { get; set; }

        /// <summary>
        /// string (1024).  first name
        /// </summary>
        public string CustomField2 { get; set; }

        /// <summary>
        /// string (1024).  last name
        /// </summary>
        public string CustomField3 { get; set; }

        /// <summary>
        /// string (1024).  billing system
        /// </summary>
        public string CustomField4 { get; set; }

        /// <summary>
        /// string (1024).  is exception
        /// </summary>
        public string CustomField5 { get; set; }

        /// <summary>
        /// string (1024).  unique identifer
        /// </summary>
        public string CustomField6 { get; set; }

        /// <summary>
        /// string (1024).  controller
        /// </summary>
        public string CustomField7 { get; set; }

        /// <summary>
        /// string (1024).  action
        /// </summary>
        public string CustomField8 { get; set; }

        /// <summary>
        /// string (1024).  gid
        /// </summary>
        public string CustomField9 { get; set; }

        /// <summary>
        /// string (1024).  is primary
        /// </summary>
        public string CustomField10 { get; set; }

        /// <summary>
        /// string (1024).  account type
        /// </summary>
        public string CustomField11 { get; set; }

        /// <summary>
        /// string (1024).  visit pay source
        /// </summary>
        public string CustomField12 { get; set; }
    }
}

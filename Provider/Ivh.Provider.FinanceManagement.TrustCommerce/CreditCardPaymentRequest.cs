namespace Ivh.Provider.FinanceManagement.TrustCommerce
{
    internal class CreditCardPaymentRequest
    {
        /// <summary>
        /// number (3-4).  The three- or four-character field located in the front or back of the card. For Visa, MasterCard, and Discover, this is a three-character field located on the back of the card to the far right. For American Express, it is a four-character value located on the front of the card above the primary account number.
        /// </summary>
        public string CardVerificationValue { get; set; }

        /// <summary>
        /// number (13-16).  Cardholder primary account number (PAN) (for example, a credit or debit card number)
        /// </summary>
        public string CreditCardNumber { get; set; }

        /// <summary>
        /// number (4).  Payment card expiration date in "MMYY" format
        /// </summary>
        public string CreditCardExpiration { get; set; }

        /// <summary>
        /// string (80)
        /// </summary>
        public string Address1 { get; set; }

        /// <summary>
        /// string (20)
        /// </summary>
        public string Zip { get; set; }

        /// <summary>
        /// number (3-8).  Transaction amount in cents (for example, $1.00 would be submitted as "100")
        /// </summary>
        public int AmountInCents { get; set; }

        /// <summary>
        /// string (1024).  payment id
        /// </summary>
        public string CustomField1 { get; set; }

        /// <summary>
        /// string (1024).  first name
        /// </summary>
        public string CustomField2 { get; set; }

        /// <summary>
        /// string (1024).  last name
        /// </summary>
        public string CustomField3 { get; set; }

        /// <summary>
        /// string (1024).  billing system
        /// </summary>
        public string CustomField4 { get; set; }

        /// <summary>
        /// string (1024).  is exception
        /// </summary>
        public string CustomField5 { get; set; }

        /// <summary>
        /// string (1024).  unique identifer
        /// </summary>
        public string CustomField6 { get; set; }

        /// <summary>
        /// string (1024).  controller
        /// </summary>
        public string CustomField7 { get; set; }

        /// <summary>
        /// string (1024).  action
        /// </summary>
        public string CustomField8 { get; set; }

        /// <summary>
        /// string (1024).  gid
        /// </summary>
        public string CustomField9 { get; set; }

        /// <summary>
        /// string (1024).  is primary
        /// </summary>
        public string CustomField10 { get; set; }

        /// <summary>
        /// string (1024).  account type
        /// </summary>
        public string CustomField11 { get; set; }

        /// <summary>
        /// string (1024).  visit pay source
        /// </summary>
        public string CustomField12 { get; set; }

    }
}
namespace Ivh.Provider.FinanceManagement.TrustCommerce
{
    internal class AuthAndCaptureResponse
    {
        /// <summary>
        /// This is the numeric or alphanumeric code received from the processor in response to a transaction.
        /// </summary>
        public string AuthCode { get; set; }

        /// <summary>
        /// The response code returned by the address verification system (AVS) for the transaction when the verify parameter (verify=y) is included in the request.
        /// </summary>
        public string Avs { get; set; }

        /// <summary>
        /// If the response field status is returned as "decline", the "declinetype" field will also be included, providing more detail regarding the reason for the decline.
        /// </summary>
        public string DeclineType { get; set; }

        /// <summary>
        /// If the response field status is returned as "baddata", the error field will also be included, providing more detail regarding the reason for the response.
        /// </summary>
        public string Error { get; set; }

        /// <summary>
        /// If the response field status is returned as "error", the "errortype" field will also be included, providing more detail regarding the reason for the error.
        /// </summary>
        public string ErrorType { get; set; }

        /// <summary>
        /// If the error response field is included, the "offenders" response field will also be included. This will indicate the field or fields that are responsible for the error.
        /// </summary>
        public string Offenders { get; set; }

        /// <summary>
        /// The response code as sent to TrustCommerce from the processing platform.
        /// </summary>
        public string ResponseCode { get; set; }

        /// <summary>
        /// The textual description of what this code indicates, as documented in the processing platform�s messaging documentation.
        /// </summary>
        public string ResponseCodeDescriptor { get; set; }

        /// <summary>
        /// This is a status indicator of your transaction request. Possible values include:
        /// approved - The transaction was successfully authorized.
        /// declined - The transaction was declined; see Decline Type for further details.
        /// baddata - Invalid fields were passed; see Error Type for further details.
        /// error - System Error when processing the transaction; see Error Type for further details.
        /// </summary>
        public string Status { get; set; }

        /// <summary>
        /// This is the 14-character unique identifier assigned to each transaction regardless of the status. It is composed of 13 numbers and 1 hyphen formatted as follows: �123-1234567890�
        /// </summary>
        public string TransactionId { get; set; }
    }
}
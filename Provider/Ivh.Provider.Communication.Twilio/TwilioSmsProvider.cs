﻿namespace Ivh.Provider.Communication.Twilio
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Net;
    using System.Net.Http;
    using System.Net.Http.Headers;
    using System.Text;
    using System.Threading.Tasks;
    using System.Web.Helpers;
    using Common.Base.Constants;
    using Common.Base.Enums;
    using Common.Base.Utilities.Extensions;
    using Common.Base.Utilities.Helpers;
    using Common.Cache;
    using Common.VisitPay.Constants;
    using Domain.Email.Entities;
    using Domain.Email.Interfaces;
    using Domain.Logging.Interfaces;
    using Domain.Settings.Enums;
    using Domain.Settings.Interfaces;
    using Logging;

    public class TwilioSmsProvider : ISmsCommunicationProvider
    {
        private readonly ILoggingProvider _logger;
        private readonly string _accountSid;
        private readonly string _authToken;
        private readonly string _messagingServiceSid;
        private readonly string _smsCallbackUrlBase;
        private readonly string _callbackUsername;
        private readonly string _callbackPassword;
        private readonly Lazy<Uri> _twilioApiUri;
        private readonly IApplicationSettingsService _applicationSettings;
        private readonly Lazy<IMetricsProvider> _metricsProvider;

        public TwilioSmsProvider(
            ILoggingProvider logger,
            IApplicationSettingsService applicationSettings,
            IDistributedCache distributedCache,
            Lazy<IMetricsProvider> metricsProvider)
        {
            this._applicationSettings = applicationSettings;
            this._logger = logger;
            this._accountSid = applicationSettings.TwilioAccountSid.Value;
            this._authToken = applicationSettings.TwilioAuthToken.Value;
            this._messagingServiceSid = applicationSettings.TwilioMessagingServiceSid.Value;
            this._smsCallbackUrlBase = applicationSettings.GetUrl(UrlEnum.TwilioCallbackBase).AbsoluteUri;
            this._callbackUsername = applicationSettings.TwilioCallbackUsername.Value;
            this._callbackPassword = applicationSettings.TwilioCallbackPassword.Value;
            this._twilioApiUri = new Lazy<Uri>(() => applicationSettings.GetUrl(UrlEnum.TwilioApi));
            this._metricsProvider = metricsProvider;
        }

        private static class TwilioSmsProviderUrls
        {
            public const string CallbackPostMethod = "{0}://{1}:{2}@{3}/ReceiveTwilioStatus/Process";
        }

        private string GenerateCallbackUrl()
        {
            if (this._smsCallbackUrlBase == null)
            {
                return null;
            }

            Uri url = new Uri(this._smsCallbackUrlBase);
            if (url.IsLoopback)
            {
                return null;
            }
            return string.Format(TwilioSmsProviderUrls.CallbackPostMethod, url.Scheme,this._callbackUsername, this._callbackPassword, url.Authority);
        }
        public async Task<bool> SendCommunication(Communication communication)
        {
            //https://www.twilio.com/docs/api/rest/sending-messages-copilot
            //TODO:  We're going to need to change the format of the text number.  Currently our numbers are stored as (208) 555-1234, needs to be "+12085551234
            string url = $"/2010-04-01/Accounts/{this._accountSid}/Messages.json";
            using (HttpClient client = new HttpClient())
            {
                client.BaseAddress = this._twilioApiUri.Value;
                client.DefaultRequestHeaders.Accept.Clear();

                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(MimeTypes.Application.Json));
                client.DefaultRequestHeaders.AcceptEncoding.Add(new StringWithQualityHeaderValue("utf-8"));

                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Convert.ToBase64String(Encoding.UTF8.GetBytes($"{this._accountSid}:{this._authToken}")));

                string toAddress = communication.ToAddress;

                string body = communication.Body.Replace("\\n", "\n");
                
                if (this._applicationSettings.RerouteSmsForTesting.Value)
                {
                    body = $"[{toAddress}]: {body}";
                    toAddress = this._applicationSettings.RerouteSmsTo.Value;
                }
                
                HttpResponseMessage response = null;
                try
                {
                    FormUrlEncodedContent formContent;
                    string generateCallbackUrl = this.GenerateCallbackUrl();
                    if (generateCallbackUrl == null)
                    {
                        formContent = new FormUrlEncodedContent(new[]
                        {
                            new KeyValuePair<string, string>("To", toAddress),
                            new KeyValuePair<string, string>("Body", body),
                            new KeyValuePair<string, string>("From", this._messagingServiceSid),
                        });
                    }
                    else
                    {
                        formContent = new FormUrlEncodedContent(new[]
                        {
                            new KeyValuePair<string, string>("To", toAddress),
                            new KeyValuePair<string, string>("Body", body),
                            new KeyValuePair<string, string>("From", this._messagingServiceSid),
                            new KeyValuePair<string, string>("StatusCallback", generateCallbackUrl)
                        });
                    }
                    response = client.PostAsync(url, formContent).Result;
                    response.EnsureSuccessStatusCode();
                }
                catch (HttpRequestException e)
                {
                    if (this.ResponseHandler(response, e, "SendCommunication"))
                    {
                        this._metricsProvider.Value.Increment(Metrics.Increment.Communication.Sms.SendFailures);
                        throw;
                    }
                }
                catch (Exception e)
                {
                    this._metricsProvider.Value.Increment(Metrics.Increment.Communication.Sms.SendFailures);
                    this._logger.Fatal(() => $"TwilioSmsProvider::SendCommunication - Failed to send message - Stack: {ExceptionHelper.AggregateExceptionToString(e)}");
                    Console.WriteLine(ExceptionHelper.AggregateExceptionToString(e));
                    throw;
                }
                if (response != null)
                {
                    string jsonString = await response.Content.ReadAsStringAsync();
                    this._logger.Info(() => $"TwilioSmsProvider::SendCommunication - Response = {Json.Encode(jsonString)}");
                    try
                    {
                        dynamic results = Json.Decode(jsonString);
                        string uniqueIdString = results.Sid.ToString().Substring(2);// to remove the leading "SM"
                        Guid.TryParse(uniqueIdString, out Guid uniqueId);
                        communication.UniqueId = uniqueId;
                        communication.SerializedMessage = jsonString;
                        // https://www.twilio.com/docs/api/rest/message#sms-status-values
                        List<string> successStatuses = new List<string> { "accepted", "queued", "sending", "sent", "receiving", "received", "delivered" };

                        if (successStatuses.Contains(results.status))
                        {
                            this._metricsProvider.Value.Increment(statName:"Sms.SendSuccesses", tags:new [] { $"CommunicationTypeId:{communication.CommunicationType.CommunicationTypeId}"});
                            return true;
                        }
                        else return false;

                    }
                    catch (Exception e)
                    {
                        this._logger.Fatal(() => $"TwilioSmsProvider::SendCommunication - Failed to serialize message - Stack:  {ExceptionHelper.AggregateExceptionToString(e)}");
                        Console.WriteLine(ExceptionHelper.AggregateExceptionToString(e));
                        throw;
                    }
                }
                else
                {
                    this._logger.Fatal(() => "TwilioSmsProvider::SendCommunication - Failed to get response message");
                }
                return false;
            }
        }

        private bool ResponseHandler(HttpResponseMessage response, HttpRequestException ex, string method)
        {
            if (response != null)
            {
                switch (response.StatusCode)
                {
                    case HttpStatusCode.Moved:
                    case HttpStatusCode.Redirect:
                    case HttpStatusCode.NotFound:
                    case HttpStatusCode.Gone:
                        this._logger.Fatal(() => string.Format("TwilioSmsProvider::{2} - UrlResolutionChange - StatusCode = {0}, Exception = {1}", response.StatusCode, ExceptionHelper.AggregateExceptionToString(ex), method));
                        return true;
                    case HttpStatusCode.Unauthorized:
                    case HttpStatusCode.Forbidden:
                    case HttpStatusCode.ProxyAuthenticationRequired:
                        this._logger.Fatal(() => string.Format("TwilioSmsProvider::{2} - AccessFailure - StatusCode = {0}, Exception = {1}", response.StatusCode, ExceptionHelper.AggregateExceptionToString(ex), method));
                        return true;
                    case HttpStatusCode.RequestTimeout:
                    case HttpStatusCode.GatewayTimeout:
                        this._logger.Fatal(() => string.Format("TwilioSmsProvider::{2} - TimeoutResponse - StatusCode = {0}, Exception = {1}", response.StatusCode, ExceptionHelper.AggregateExceptionToString(ex), method));
                        return true;
                    case HttpStatusCode.BadRequest:
                    case HttpStatusCode.MethodNotAllowed:
                    case HttpStatusCode.NotAcceptable:
                    case HttpStatusCode.Conflict:
                    case HttpStatusCode.RequestEntityTooLarge:
                    case HttpStatusCode.RequestUriTooLong:
                    case HttpStatusCode.HttpVersionNotSupported:
                        this._logger.Fatal(() => string.Format("TwilioSmsProvider::{2} - ConfigurationError - StatusCode = {0}, Exception = {1}", response.StatusCode, ExceptionHelper.AggregateExceptionToString(ex), method));
                        return true;
                    case HttpStatusCode.ServiceUnavailable:
                    case HttpStatusCode.InternalServerError:
                    case HttpStatusCode.NotImplemented:
                    case HttpStatusCode.BadGateway:
                    case (HttpStatusCode)420: // Method Failure Sprint Framework
                        this._logger.Fatal(() => string.Format("TwilioSmsProvider::{2} - RemoteServerError - StatusCode = {0}, Exception = {1}", response.StatusCode, ExceptionHelper.AggregateExceptionToString(ex), method));
                        return true;
                    default:
                        this._logger.Fatal(() => string.Format("TwilioSmsProvider::{2} - UnknownResponseError - StatusCode = {0}, Exception = {1}", response.StatusCode, ExceptionHelper.AggregateExceptionToString(ex), method));
                        return true;
                    case HttpStatusCode.Accepted:
                    case HttpStatusCode.OK:
                        //Shouldnt ever get here, but if it does i want it to continue.
                        this._logger.Warn(() => string.Format("TwilioSmsProvider::{2} - SuccessResponseCodeThrown - StatusCode = {0}, Exception = {1}", response.StatusCode, ExceptionHelper.AggregateExceptionToString(ex), method));
                        break;
                }
            }
            return false;
        }
    }
}

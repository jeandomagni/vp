﻿namespace Ivh.Provider.Guarantor.Modules
{
    using Autofac;
    using Common.Base.Enums;
    using Common.State.Interfaces;
    using Common.VisitPay.Enums;
    using Domain.Guarantor.Entities;
    using Domain.Guarantor.Interfaces;

    public class Module : Autofac.Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<HsGuarantorMapRepository>().As<IHsGuarantorMapRepository>();
            builder.RegisterType<GuarantorInvitationRepository>().As<IGuarantorInvitationRepository>();
            builder.RegisterType<GuarantorStateContextProvider>().As<IContextProvider<Guarantor, VpGuarantorStatusEnum>>();
        }
    }
}
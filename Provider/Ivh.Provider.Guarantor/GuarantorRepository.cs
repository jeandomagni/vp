﻿namespace Ivh.Provider.Guarantor
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Net.Http;
    using Application.Core.Common.Dtos;
    using Common.Api.Client;
    using Common.Api.Helpers;
    using Common.Base.Utilities.Extensions;
    using Common.Base.Utilities.Helpers;
    using Common.Cache;
    using Common.Data;
    using Common.Data.Connection.Connections;
    using Common.Data.Interfaces;
    using Common.ServiceBus.Common.Messages;
    using Common.ServiceBus.Interfaces;
    using Common.VisitPay.Enums;
    using Common.VisitPay.Messages.Core;
    using Common.VisitPay.Messages.Matching.Dtos;
    using Domain.Guarantor.Entities;
    using Domain.Guarantor.Interfaces;
    using Domain.Logging.Interfaces;
    using Domain.Settings.Enums;
    using Domain.Settings.Interfaces;
    using Domain.User.Entities;
    using NHibernate;
    using NHibernate.Criterion;
    using NHibernate.SqlCommand;

    public class GuarantorRepository : RepositoryBase<Guarantor, VisitPay>, IGuarantorRepository
    {
        private const string VpuidVpgidCacheKeyPrefix = nameof(GuarantorRepository) + "VPUID_VPGID::";
        private const string VpunVpgidCacheKeyPrefix = nameof(GuarantorRepository) + "VPUN_VPGID::";
        private const string VpgidVpuidCacheKeyPrefix = nameof(GuarantorRepository) + "VPGID_VPUID::";

        private static class MatchingUrlsGuarantorRepo
        {
            public const string IsGuarantorMatch = "/api/Matching/IsGuarantorMatch";
            public const string AddInitialMatches = "/api/Matching/AddInitialMatches";
            public const string GetGuarantorMatchSsn = "/api/Matching/GetGuarantorMatchSsn";
        }

        private readonly Lazy<IApplicationSettingsService> _applicationSettingsService;
        private readonly Lazy<IHsGuarantorMapRepository> _hsGuarantorMapRepository;
        private readonly Lazy<IMemoryCache> _cache;
        private readonly Lazy<ILoggingProvider> _logger;
        private readonly Lazy<IBus> _bus;

        private readonly IList<GuarantorChangedEvent> _guarantorChangedEvents = new List<GuarantorChangedEvent>();

        public GuarantorRepository(
            Lazy<IApplicationSettingsService> applicationSettingsService,
            Lazy<IHsGuarantorMapRepository> hsGuarantorMapRepository,
            ISessionContext<VisitPay> sessionContext,
            Lazy<IMemoryCache> cache,
            Lazy<ILoggingProvider> logger,
            Lazy<IBus> bus
            )
            : base(sessionContext)
        {
            this._applicationSettingsService = applicationSettingsService;
            this._hsGuarantorMapRepository = hsGuarantorMapRepository;
            this._cache = cache;
            this._logger = logger;
            this._bus = bus;
            this.PublishMessagesAfterRollbackCallback();
        }

        public override Guarantor GetById(int id)
        {
            return this.PersistToVisitPayUserIdCache(base.GetById(id));
        }

        public Guarantor GetGuarantor(string visitPayUserName)
        {
            ICriteria criteria = this.Session.CreateCriteria<Guarantor>("g")
                .CreateCriteria("g.User", JoinType.InnerJoin)
                .Add(Restrictions.Eq("UserName", visitPayUserName));
            return this.PersistToVisitPayUserIdCache(criteria.UniqueResult<Guarantor>());
        }

        private static readonly object GetGuarantorIdVisitPayUserIdLock = new object();
        public int GetGuarantorId(int visitPayUserId)
        {
            string key = VpuidVpgidCacheKeyPrefix + visitPayUserId;
            int guarantorId = this._cache.Value.GetObjectAsync<int>(key).Result;
            if (guarantorId == 0)
            {
                lock (GetGuarantorIdVisitPayUserIdLock)
                {
                    guarantorId = this._cache.Value.GetObjectAsync<int>(key).Result;

                    if (guarantorId == 0)
                    {
                        Guarantor guarantor = this.Session.QueryOver<Guarantor>()
                            .Where(x => x.User.VisitPayUserId == visitPayUserId)
                            //.Cacheable()
                            //.CacheMode(CacheMode.Normal)
                            .Take(1).SingleOrDefault();

                        if (guarantor?.User != null)
                        {
                            guarantorId = guarantor.VpGuarantorId;
                            this.PersistToVisitPayUserIdCache(guarantor);
                        }
                    }
                }
            }

            return guarantorId;
        }

        private static readonly object GetGuarantorIdVisitPayUserGuidLock = new object();
        public int GetGuarantorId(Guid visitPayUserGuid)
        {
            string key = VpuidVpgidCacheKeyPrefix + visitPayUserGuid;
            int guarantorId = this._cache.Value.GetObjectAsync<int>(key).Result;
            if (guarantorId == 0)
            {
                lock (GetGuarantorIdVisitPayUserGuidLock)
                {
                    guarantorId = this._cache.Value.GetObjectAsync<int>(key).Result;

                    if (guarantorId == 0)
                    {
                        Guarantor g = null;
                        VisitPayUser user = null;
                        Guarantor guarantor = this.Session.QueryOver<Guarantor>(() => g)
                            .JoinAlias(() => g.User, () => user, JoinType.InnerJoin)
                            .Where(() => user.VisitPayUserGuid == visitPayUserGuid)
                            .Take(1).SingleOrDefault();

                        if (guarantor?.User != null)
                        {
                            guarantorId = guarantor.VpGuarantorId;
                            this.PersistToVisitPayUserIdCache(guarantor);
                        }
                    }
                }
            }

            return guarantorId;
        }

        private static readonly object GetGuarantorIdVisitPayUserNameLock = new object();
        public int GetGuarantorId(string visitPayUserName)
        {
            string key = VpunVpgidCacheKeyPrefix + visitPayUserName;
            int guarantorId = this._cache.Value.GetObjectAsync<int>(key).Result;
            if (guarantorId == 0)
            {
                lock (GetGuarantorIdVisitPayUserNameLock)
                {
                    guarantorId = this._cache.Value.GetObjectAsync<int>(key).Result;

                    if (guarantorId == 0)
                    {
                        Guarantor guarantor = this.GetGuarantor(visitPayUserName);

                        if (guarantor != null)
                        {
                            guarantorId = guarantor.VpGuarantorId;
                        }
                    }
                }
            }

            return guarantorId;
        }

        private static readonly object GetVisitPayUserIdLock = new object();
        public int GetVisitPayUserId(int vpGuarantorId)
        {
            string key = VpgidVpuidCacheKeyPrefix + vpGuarantorId;
            int visitPayUserId = this._cache.Value.GetObjectAsync<int>(key).Result;

            if (visitPayUserId != default(int))
            {
                return visitPayUserId;
            }

            lock (GetVisitPayUserIdLock)
            {
                int? result = this.Session.QueryOver<Guarantor>()
                    .Where(x => x.VpGuarantorId == vpGuarantorId)
                    .Select(x => x.User.VisitPayUserId)
                    .Take(1)
                    .List<int?>()
                    .FirstOrDefault();

                if (result.HasValue && result != default(int))
                {
                    visitPayUserId = result.Value;
                    this._cache.Value.SetObjectAsync(key, visitPayUserId, 60 * 10);
                }
            }

            return visitPayUserId;
        }

        public IList<Guarantor> GetActiveGuarantorsWithPendingStatement(DateTime date)
        {
            ICriteria criteria = this.Session.CreateCriteria(typeof(Guarantor));
            criteria.Add(Restrictions.IsNull("AccountClosedDate"));
            criteria.Add(Restrictions.Or(Restrictions.Le("NextStatementDate", date), Restrictions.IsNull("NextStatementDate")));
            return criteria.List<Guarantor>();
        }

        public IList<int> GetGuarantorIds(ICollection<VpGuarantorStatusEnum> guarantorStatuses)
        {
            ICriteria criteria = this.Session.CreateCriteria<Guarantor>("g");
            criteria.Add(Restrictions.In("g.VpGuarantorStatus", guarantorStatuses.ToList()));
            criteria.SetProjection(Projections.Property("g.VpGuarantorId"));
            return criteria.List<int>();
        }

        public IList<Guarantor> GetGuarantors(GuarantorFilter filter)
        {
            ICriteria criteria = this.GetGuarantorCriteria(filter);
            return criteria.List<Guarantor>();
        }

        public int GetGuarantorCount(GuarantorFilter filter)
        {
            ICriteria criteria = this.GetGuarantorCriteria(filter, false);
            criteria.SetProjection(Projections.RowCount());
            return (int)criteria.UniqueResult();
        }

        public IList<Guarantor> GetGuarantors(GuarantorFilter filter, int page, int rows)
        {
            ICriteria criteria = this.GetGuarantorCriteria(filter);
            // dynamic paging
            // SQL paging used zero based indexing so we need to subtract 1 from the page number
            criteria.SetFirstResult((page - 1) * rows);
            criteria.SetMaxResults(rows);
            return criteria.List<Guarantor>();
        }

        private ICriteria GetGuarantorCriteria(GuarantorFilter filter, bool isOrdered = true)
        {
            ICriteria criteria = this.Session.CreateCriteria<Guarantor>("g")
                .CreateCriteria("g.User", "user", JoinType.InnerJoin);

            if (filter.HasAddressFilters)
            {
                DetachedCriteria address = DetachedCriteria.For<VisitPayUserAddress>("userAddress")
                    .SetProjection(Projections.Property("userAddress.VisitPayUser.VisitPayUserId"))
                    .Add(Restrictions.EqProperty("g.User.VisitPayUserId", "userAddress.VisitPayUser.VisitPayUserId"));

                if (!string.IsNullOrEmpty(filter.AddressStreet1))
                {
                    address.Add(Restrictions.InsensitiveLike("userAddress.AddressStreet1", filter.AddressStreet1, MatchMode.Anywhere));
                }

                if (!string.IsNullOrEmpty(filter.AddressStreet2))
                {
                    address.Add(Restrictions.InsensitiveLike("userAddress.AddressStreet2", filter.AddressStreet2, MatchMode.Anywhere));
                }

                if (!string.IsNullOrEmpty(filter.City))
                {
                    address.Add(Restrictions.Eq("userAddress.City", filter.City));
                }

                if (!string.IsNullOrEmpty(filter.StateProvince))
                {
                    address.Add(Restrictions.InsensitiveLike("userAddress.StateProvince", filter.StateProvince, MatchMode.Anywhere));
                }

                if (!string.IsNullOrEmpty(filter.PostalCode))
                {
                    address.Add(Restrictions.Eq("userAddress.PostalCode", filter.PostalCode));
                }

                criteria.Add(Subqueries.Exists(address));
            }

            //TODO: VPNG-14775 Make sure this actually looks at their selected SMS number not just the PhoneNumber field
            if (!string.IsNullOrWhiteSpace(filter.PhoneNumber))
            {
                criteria.Add(Restrictions.Or(Restrictions.Eq("user.PhoneNumber", filter.PhoneNumber), Restrictions.Eq("user.PhoneNumberSecondary", filter.PhoneNumber)));
            }

            if (!string.IsNullOrWhiteSpace(filter.FirstName))
                criteria.Add(Restrictions.Eq("user.FirstName", filter.FirstName));
            if (!string.IsNullOrWhiteSpace(filter.LastName))
                criteria.Add(Restrictions.Eq("user.LastName", filter.LastName));
            if (!string.IsNullOrWhiteSpace(filter.Ssn4))
            {
                int ssn4 = -1;
                int.TryParse(filter.Ssn4, out ssn4);
                criteria.Add(Restrictions.Eq("user.SSN4", ssn4));
            }
            if (filter.DOB.HasValue)
            {
                criteria.Add(Restrictions.Eq("user.DateOfBirth", filter.DOB));
            }
            if (filter.VpGuarantorId.HasValue)
                criteria.Add(Restrictions.Eq("g.VpGuarantorId", filter.VpGuarantorId.Value));
            if (!string.IsNullOrWhiteSpace(filter.UserNameOrEmailAddress))
                criteria.Add(new Disjunction()
                    .Add(Restrictions.Eq("user.UserName", filter.UserNameOrEmailAddress))
                    .Add(Restrictions.Eq("user.Email", filter.UserNameOrEmailAddress)));

            if (!string.IsNullOrWhiteSpace(filter.HsGuarantorId))
            {
                List<HsGuarantorMatchStatusEnum> matchedStatuses = EnumHelper<HsGuarantorMatchStatusEnum>.GetValues(HsGuarantorMatchStatusEnumCategory.Matched).ToList();

                DetachedCriteria hsGuarantor = DetachedCriteria.For<HsGuarantorMap>("hsGuarantor")
                    .SetProjection(Projections.Property("hsGuarantor.VpGuarantor.VpGuarantorId"))
                    .Add(Restrictions.EqProperty("g.VpGuarantorId", "hsGuarantor.VpGuarantor.VpGuarantorId"))
                    .Add(Restrictions.Eq("hsGuarantor.SourceSystemKey", filter.HsGuarantorId))
                    .Add(Restrictions.In("hsGuarantor.HsGuarantorMatchStatus", matchedStatuses));

                criteria.Add(Subqueries.Exists(hsGuarantor));
            }

            if (isOrdered)
            {
                bool isAscendingOrder = "asc".Equals(filter.SortOrder, StringComparison.InvariantCultureIgnoreCase);

                switch (filter.SortField ?? string.Empty)
                {
                    case "DateOfBirth":
                        {
                            criteria.AddOrder(new Order("user.DateOfBirth", isAscendingOrder));
                            break;
                        }
                    case "Name":
                        {
                            criteria.AddOrder(new Order("user.FirstName", isAscendingOrder));
                            criteria.AddOrder(new Order("user.LastName", isAscendingOrder));
                            break;
                        }
                    case "Ssn4":
                        {
                            criteria.AddOrder(new Order("user.SSN4", isAscendingOrder));
                            break;
                        }
                    case "UserNameAndEmailAddress":
                        {
                            criteria.AddOrder(new Order("user.UserName", isAscendingOrder));
                            criteria.AddOrder(new Order("user.Email", isAscendingOrder));
                            break;
                        }
                    case "VpGuarantorId":
                        {
                            criteria.AddOrder(new Order("g.VpGuarantorId", isAscendingOrder));
                            break;
                        }
                    case "VpGuarantorStatus":
                        {
                            criteria.AddOrder(new Order("g.VpGuarantorStatus", isAscendingOrder));
                            break;
                        }
                }
            }

            return criteria;
        }

        public void InsertHsGuarantorMap(HsGuarantorMap appHsGuarantor)
        {
            this._hsGuarantorMapRepository.Value.InsertOrUpdate(appHsGuarantor);
        }

        public IEnumerable<Guarantor> GetActiveGuarantorsWithPaymentDueDate(DateTime dueDate)
        {
            ICriteria criteria = this.Session.CreateCriteria<Guarantor>()
                .Add(Restrictions.IsNull("AccountClosedDate"))
                .Add(Restrictions.Eq("PaymentDueDay", dueDate.Day));
            return criteria.List<Guarantor>();
        }

        private Guarantor PersistToVisitPayUserIdCache(Guarantor guarantor)
        {
            if (guarantor?.User != null)
            {
                this._cache.Value.SetObjectAsync(VpuidVpgidCacheKeyPrefix + guarantor.User.VisitPayUserId, guarantor.VpGuarantorId, 60 * 10);
                this._cache.Value.SetObjectAsync(VpunVpgidCacheKeyPrefix + guarantor.User.UserName, guarantor.VpGuarantorId, 60 * 10);
            }
            return guarantor;
        }

        private HttpClient GenerateHttpClient()
        {
            return HttpClientFactory.Create(new ApiHandler(this._applicationSettingsService.Value.ClientDataApiAppId.Value, this._applicationSettingsService.Value.ClientDataApiAppKey.Value, this._applicationSettingsService.Value.ApplicationName.Value));
        }

        public string GetGuarantorMatchSsn(int vpGuarantorId)
        {
            string result = HttpClientHelper.CallApiWith<string, object>(
                this._applicationSettingsService.Value.GetUrl(UrlEnum.ClientDataApi).AbsoluteUri,
                MatchingUrlsGuarantorRepo.GetGuarantorMatchSsn,
                HttpMethod.Post,
                vpGuarantorId,
                (message, dto) =>
                {
                    //Success
                },
                (message, s) =>
                {
                    this._logger.Value.Fatal(() => $"{nameof(GuarantorRepository)}::{nameof(this.IsGuarantorMatch)} - Something went wrong while calling the API.  StatusCode - {message.StatusCode}, Response - {s}");
                },
                this.GenerateHttpClient
            );

            return result;
        }

        public GuarantorMatchResult IsGuarantorMatch(MatchDataDto matchData, PatternUseEnum patternUseEnum)
        {
            GuarantorMatchResult result = HttpClientHelper.CallApiWith<GuarantorMatchResult, object>(
                this._applicationSettingsService.Value.GetUrl(UrlEnum.ClientDataApi).AbsoluteUri,
                MatchingUrlsGuarantorRepo.IsGuarantorMatch,
                HttpMethod.Post,
                new GuarantorMatchRequest
                {
                    MatchData = matchData
                },
                (message, dto) =>
                {
                    //Success
                },
                (message, s) =>
                {
                    this._logger.Value.Fatal(() => $"{nameof(GuarantorRepository)}::{nameof(this.IsGuarantorMatch)} - Something went wrong while calling the API.  StatusCode - {message.StatusCode}, Response - {s}");
                },
                this.GenerateHttpClient
            );

            return result;
        }

        public bool IsGuarantorAccountClosed(int vpGuarantorId)
        {
            return this.GetGuarantorAccountClosedDate(vpGuarantorId).HasValue;
        }

        public DateTime? GetGuarantorAccountClosedDate(int vpGuarantorId)
        {
            ICriteria criteria = this.Session.CreateCriteria<Guarantor>("g");
            criteria.Add(Restrictions.Eq("g.VpGuarantorId", vpGuarantorId));
            criteria.SetProjection(Projections.Property("g.AccountCancellationDate"));
            return criteria.UniqueResult<DateTime?>();
        }

        public IList<VpGuarantorCancellationReason> GetAllCancellationReasons()
        {
            IList<VpGuarantorCancellationReason> reasons = this.Session.QueryOver<VpGuarantorCancellationReason>()
                .Where(x => x.Selectable)
                .OrderBy(x => x.DisplaySortOrder).Asc
                .Cacheable()
                .CacheMode(CacheMode.Normal)
                .List<VpGuarantorCancellationReason>();
            return reasons;
        }

        public IList<GuarantorMatchResult> AddInitialMatches(int vpGuarantorId, MatchGuarantorDto guarantor, MatchPatientDto patient)
        {
            IList<GuarantorMatchResult> result = HttpClientHelper.CallApiWith<IList<GuarantorMatchResult>, object>(
                this._applicationSettingsService.Value.GetUrl(UrlEnum.ClientDataApi).AbsoluteUri,
                MatchingUrlsGuarantorRepo.AddInitialMatches,
                HttpMethod.Post,
                new GuarantorMatchAddRequest
                {
                    VpGuarantorId = vpGuarantorId,
                    MatchData = new MatchDataDto
                    {
                        Guarantor = guarantor,
                        Patient = patient,
                        ApplicationEnum = ApplicationEnum.VisitPay
                    }
                },
                (message, dto) =>
                {
                    //Success
                },
                (message, s) =>
                {
                    this._logger.Value.Fatal(() => $"{nameof(GuarantorRepository)}::{nameof(this.AddInitialMatches)} - Something went wrong while calling the API.  StatusCode - {message.StatusCode} Message - {s}");
                },
                this.GenerateHttpClient
            );

            return result;
        }

        private void PublishMessagesAfterRollbackCallback()
        {
            this.Session.Transaction.RegisterPostCommitFailureAction((p1) =>
            {
                p1.SafeClear();
            }, this._guarantorChangedEvents);
        }

        public override void InsertOrUpdate(Guarantor guarantor)
        {
            this.QueueUnpublishedMessages(guarantor);
            base.InsertOrUpdate(guarantor);
        }

        public override void Insert(Guarantor guarantor)
        {
            this.QueueUnpublishedMessages(guarantor);
            base.Insert(guarantor);
        }

        public override void Update(Guarantor guarantor)
        {
            this.QueueUnpublishedMessages(guarantor);
            base.Update(guarantor);
        }

        private void QueueUnpublishedMessages(Guarantor entity)
        {
            if (entity.UnpublishedGuarantorChangedEvent != null)
            {
                //only register the callback once.
                if (!this._guarantorChangedEvents.Any())
                {
                    this.Session.Transaction.RegisterPostCommitSuccessAction((guarantorChangedEvents) =>
                    {
                        if (guarantorChangedEvents.IsNotNullOrEmpty())
                        {
                            // group by guarantorId
                            IEnumerable<IGrouping<int, GuarantorChangedEvent>> changedEventsGroupedByGuarantor = guarantorChangedEvents.GroupBy(x => x.Guarantor.VpGuarantorId);

                            foreach (IGrouping<int, GuarantorChangedEvent> guarantorChangedEvent in changedEventsGroupedByGuarantor)
                            {
                                // get latest values for guarantor change events
                                GuarantorChangedMessage message = this.GetGuarantorChangedMessage(guarantorChangedEvent, guarantorChangedEvent.Key);

                                // publish
                                if (message != null)
                                {
                                    this._bus.Value.PublishMessage(message).Wait();
                                }
                            }
                            guarantorChangedEvents.Clear();
                        }
                    }, this._guarantorChangedEvents);
                }
                this._guarantorChangedEvents.Add(entity.UnpublishedGuarantorChangedEvent);
                entity.UnpublishedGuarantorChangedEvent = null;
            }
        }

        private GuarantorChangedMessage GetGuarantorChangedMessage(IEnumerable<GuarantorChangedEvent> events, int vpGuarantorId)
        {
            // get the latest event
            GuarantorChangedEvent latestEvent = events.OrderByDescending(x => x.EventDate).FirstOrDefault();

            if (latestEvent == null)
            {
                return null;
            }

            // get the latest values
            GuarantorChangedMessage message = new GuarantorChangedMessage()
            {
                VpGuarantorId = vpGuarantorId,
                GuarantorTypeEnum = latestEvent.Guarantor.VpGuarantorTypeEnum,
                VpGuarantorStatus = latestEvent.Guarantor.VpGuarantorStatus,
                EventDate = latestEvent.EventDate
            };

            return message;
        }
    }
}
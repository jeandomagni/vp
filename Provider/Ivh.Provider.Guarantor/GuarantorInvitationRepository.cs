﻿namespace Ivh.Provider.Guarantor
{
    using System;
    using Common.Data;
    using Common.Data.Connection.Connections;
    using Common.Data.Interfaces;
    using Domain.Guarantor.Entities;
    using Domain.Guarantor.Interfaces;
    using NHibernate;
    using NHibernate.Criterion;

    public class GuarantorInvitationRepository : RepositoryBase<GuarantorInvitation, VisitPay>, IGuarantorInvitationRepository
    {
        public GuarantorInvitationRepository(ISessionContext<VisitPay> sessionContext)
            : base(sessionContext)
        {
        }

        public GuarantorInvitation GetGuarantorInvitation(Guid invitationToken)
        {
            ICriteria criteria = this.Session.CreateCriteria<GuarantorInvitation>()
                .Add(Restrictions.Eq("InvitationToken", invitationToken));
            return criteria.UniqueResult<GuarantorInvitation>();
        }
    }
}
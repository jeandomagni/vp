﻿namespace Ivh.Provider.Guarantor
{
    using Common.Data;
    using Common.Data.Connection.Connections;
    using Common.Data.Interfaces;
    using Common.VisitPay.Enums;
    using Domain.Guarantor.Entities;
    using Domain.Guarantor.Interfaces;
    using NHibernate;

    public class VpGuarantorScoreRepository : RepositoryBase<VpGuarantorScore, VisitPay>, IVpGuarantorScoreRepository
    {
        public VpGuarantorScoreRepository(ISessionContext<VisitPay> sessionContext, IStatelessSessionContext<VisitPay> statelessSessionContext) : base(sessionContext, statelessSessionContext)
        {
        }

        public VpGuarantorScore GetScore(int vpGuarantorId, ScoreTypeEnum scoreType)
        {
            VpGuarantorScore vpGuarantorScore = this.StatelessSession.QueryOver<VpGuarantorScore>()
                .Where(x => x.VpGuarantorId == vpGuarantorId)
                .Where(x => x.ScoreTypeId == (int) scoreType)
                .OrderBy(x => x.ScoreCreationDate).Desc
                .Take(1).SingleOrDefault();

            return vpGuarantorScore;
        }
    }
}
﻿namespace Ivh.Provider.Guarantor
{
    using System.Collections.Generic;
    using Common.Data;
    using Common.Data.Connection.Connections;
    using Common.Data.Interfaces;
    using Domain.Guarantor.Entities;
    using Domain.Guarantor.Interfaces;

    public class BaseHsGuarantorRepository : ReadOnlyRepositoryBase<BaseHsGuarantor, VisitPay>, IBaseHsGuarantorRepository
    {
        public BaseHsGuarantorRepository(IStatelessSessionContext<VisitPay> statelessSessionContext) : base(statelessSessionContext)
        {
        }

        public BaseHsGuarantor GetBaseHsGuarantor(int billingSystemId, string hsGuarantorSourceSystemKey)
        {
            BaseHsGuarantor guarantor = this.StatelessSession.QueryOver<BaseHsGuarantor>()
                .Where(x => x.HsBillingSystemId == billingSystemId)
                .WhereStringEq(x => x.SourceSystemKey, hsGuarantorSourceSystemKey).SingleOrDefault();

            return guarantor;
        }

        public IList<BaseHsGuarantor> GetBaseHsGuarantors(string lastName, string hsGuarantorSourceSystemKey)
        {
            IList<BaseHsGuarantor> guarantors = this.StatelessSession.QueryOver<BaseHsGuarantor>()
                .WhereStringEq(x => x.SourceSystemKey, hsGuarantorSourceSystemKey)
                .WhereStringEq(x => x.LastName, lastName)
                .List();

            return guarantors;
        }
    }
}

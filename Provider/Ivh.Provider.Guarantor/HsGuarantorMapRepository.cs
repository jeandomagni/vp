﻿namespace Ivh.Provider.Guarantor
{
    using System;
    using System.Collections.Generic;
    using Common.Cache;
    using Common.Data;
    using Common.Data.Connection.Connections;
    using Common.Data.Interfaces;
    using Common.VisitPay.Enums;
    using Domain.Guarantor.Entities;
    using Domain.Guarantor.Interfaces;
    using NHibernate;

    public class HsGuarantorMapRepository : RepositoryCacheBase<HsGuarantorMap, VisitPay>, IHsGuarantorMapRepository
    {
        public HsGuarantorMapRepository(ISessionContext<VisitPay> sessionContext, Lazy<IDistributedCache> cache)
            : base(sessionContext, cache)
        {
        }

        public IList<HsGuarantorMap> GetGuarantors(int hsGuarantorId, HsGuarantorMatchStatusEnum? hsGuarantorMatchStatusEnum = null)
        {
            IQueryOver<HsGuarantorMap, HsGuarantorMap> criteria = this.Session.QueryOver<HsGuarantorMap>();
            criteria.Where(x => x.HsGuarantorId == hsGuarantorId);
            if (hsGuarantorMatchStatusEnum.HasValue)
            {
                criteria.And(x => x.HsGuarantorMatchStatus == hsGuarantorMatchStatusEnum.Value);
            }

            return criteria.List();
        }

        public HsGuarantorMap GetHsGuarantor(string sourceSystemKey, int billingSystemId)
        {
            return this.GetCached(
                x => x.BillingSystem.BillingSystemId, billingSystemId,
                x => x.SourceSystemKey, sourceSystemKey,
                (b, s) =>
                {
                    IQueryOver<HsGuarantorMap, HsGuarantorMap> criteria = this.Session.QueryOver<HsGuarantorMap>();
                    criteria.Where(x => x.SourceSystemKey == s);
                    criteria.And(x => x.BillingSystem.BillingSystemId == b);
                    return criteria.Take(1).SingleOrDefault();
                });
        }

        /// <summary>
        /// Only used when BillingSystemId is not known
        /// </summary>
        /// <param name="sourceSystemKey"></param>
        /// <returns></returns>
        public IList<HsGuarantorMap> GetHsGuarantors(string sourceSystemKey)
        {
            IQueryOver<HsGuarantorMap, HsGuarantorMap> criteria = this.Session.QueryOver<HsGuarantorMap>();
            criteria.Where(x => x.SourceSystemKey == sourceSystemKey);
            return criteria.List<HsGuarantorMap>();
        }

        protected override int GetId(HsGuarantorMap entity)
        {
            return entity.HsGuarantorMapId;
        }
    }
}
﻿namespace Ivh.Provider.Guarantor
{
    using System.Collections.Generic;
    using System.Linq;
    using Common.Data;
    using Common.Data.Connection.Connections;
    using Common.Data.Interfaces;
    using Domain.Guarantor.Entities;
    using Domain.Guarantor.Interfaces;
    using NHibernate;

    public class BillingSystemRepository : RepositoryBase<BillingSystem, VisitPay>, IBillingSystemRepository
    {
        public BillingSystemRepository(ISessionContext<VisitPay> sessionContext) : base(sessionContext)
        {
        }

        public IList<BillingSystem> GetAllBillingSystemTypes(bool includeUnmatched = false)
        {
            return this.Session.QueryOver<BillingSystem>()
                .OrderBy(vs => vs.BillingSystemId).Asc
                .Cacheable()
                .CacheMode(CacheMode.Normal)
                .List().Where(x => includeUnmatched || x.BillingSystemId > 0).ToList();
        }
    }
}
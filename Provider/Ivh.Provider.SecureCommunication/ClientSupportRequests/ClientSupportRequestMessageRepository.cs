﻿namespace Ivh.Provider.SecureCommunication.ClientSupportRequests
{
    using Common.Base.Enums;
    using Common.Data;
    using Common.Data.Connection.Connections;
    using Common.Data.Interfaces;
    using Common.VisitPay.Enums;
    using Domain.SecureCommunication.ClientSupportRequests.Entities;
    using Domain.SecureCommunication.ClientSupportRequests.Interfaces;
    using NHibernate;

    public class ClientSupportRequestMessageRepository : RepositoryBase<ClientSupportRequestMessage, VisitPay>, IClientSupportRequestMessageRepository
    {
        public ClientSupportRequestMessageRepository(ISessionContext<VisitPay> sessionContext)
            : base(sessionContext)
        {
        }

        public int GetUnreadMessagesCountFromClient(int? assignedVisitPayUserId)
        {
            ClientSupportRequest clientSupportRequestAlias = null;

            IQueryOver<ClientSupportRequestMessage, ClientSupportRequestMessage> query = this.Session.QueryOver<ClientSupportRequestMessage>()
                .JoinAlias(x => x.ClientSupportRequest, () => clientSupportRequestAlias)
                .Where(x => x.ClientSupportRequestMessageType == ClientSupportRequestMessageTypeEnum.FromClient && !x.IsRead);

            if (assignedVisitPayUserId.HasValue)
            {
                query = query.Where(x => clientSupportRequestAlias.AssignedVisitPayUser.VisitPayUserId == assignedVisitPayUserId.Value);
            }

            return query.RowCount();
        }

        public int GetUnreadMessagesCountToClient(int? visitPayUserId)
        {
            ClientSupportRequest clientSupportRequestAlias = null;

            IQueryOver<ClientSupportRequestMessage, ClientSupportRequestMessage> query = this.Session.QueryOver<ClientSupportRequestMessage>()
                .JoinAlias(x => x.ClientSupportRequest, () => clientSupportRequestAlias)
                .Where(x => x.ClientSupportRequestMessageType == ClientSupportRequestMessageTypeEnum.ToClient && !x.IsRead);

            if (visitPayUserId.HasValue)
            {
                query = query.Where(x => clientSupportRequestAlias.VisitPayUser.VisitPayUserId == visitPayUserId.Value);
            }

            return query.RowCount();
        }

    }
}
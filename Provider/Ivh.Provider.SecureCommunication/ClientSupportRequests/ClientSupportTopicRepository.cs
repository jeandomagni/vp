﻿namespace Ivh.Provider.SecureCommunication.ClientSupportRequests
{
    using Common.Data;
    using Common.Data.Connection.Connections;
    using Common.Data.Interfaces;
    using Domain.SecureCommunication.ClientSupportRequests.Entities;
    using Domain.SecureCommunication.ClientSupportRequests.Interfaces;
    using NHibernate;

    public class ClientSupportTopicRepository : RepositoryBase<ClientSupportTopic, VisitPay>, IClientSupportTopicRepository
    {
        public ClientSupportTopicRepository(ISessionContext<VisitPay> sessionContext) : base(sessionContext)
        {
            
        }
    }
}
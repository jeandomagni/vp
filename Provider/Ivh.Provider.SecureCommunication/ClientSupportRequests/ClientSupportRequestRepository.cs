﻿namespace Ivh.Provider.SecureCommunication.ClientSupportRequests
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text.RegularExpressions;
    using Common.Data;
    using Common.Data.Connection.Connections;
    using Common.Data.Interfaces;
    using Domain.SecureCommunication.ClientSupportRequests.Entities;
    using Domain.SecureCommunication.ClientSupportRequests.Interfaces;
    using Ivh.Domain.User.Entities;
    using NHibernate;
    using NHibernate.Criterion;
    using NHibernate.SqlCommand;

    public class ClientSupportRequestRepository : RepositoryBase<ClientSupportRequest, VisitPay>, IClientSupportRequestRepository
    {
        public ClientSupportRequestRepository(ISessionContext<VisitPay> sessionContext)
            : base(sessionContext)
        {
        }

        public ClientSupportRequestResults GetClientSupportRequests(ClientSupportRequestFilter filter)
        {
            ClientSupportRequestResults results = this.GetClientSupportRequestTotals(filter);

            results.ClientSupportRequests = this.DoGetClientSupportRequests(filter);

            return results;
        }

        public byte[] GetAttachmentContent(int clientSupportRequestMessageAttachmentId)
        {
            return this.Session.QueryOver<ClientSupportRequestMessageAttachment>()
                .Where(x => x.ClientSupportRequestMessageAttachmentId == clientSupportRequestMessageAttachmentId)
                .Select(x => x.FileContent)
                .SingleOrDefault<byte[]>();
        }

        public IReadOnlyList<int> GetAllClientUsersWithSupportRequest()
        {
            VisitPayUser createdVisitPayUserAlias = null;

            IList<int> query = this.Session.QueryOver<ClientSupportRequest>()
                .JoinAlias(x => x.VisitPayUser, () => createdVisitPayUserAlias)
                .Select(Projections.Distinct(Projections.Property<VisitPayUser>(x => createdVisitPayUserAlias.VisitPayUserId)
                )).List<int>();

            return query.OrderBy(x => x).ToList();
        }
        
        public IList<int> GetAllAssignedUsers()
        {
            VisitPayUser assignedVisitPayUserAlias = null;

            return this.Session.QueryOver<ClientSupportRequest>()
                .JoinAlias(x => x.AssignedVisitPayUser, () => assignedVisitPayUserAlias, JoinType.LeftOuterJoin)
                .Where(x => x.AssignedVisitPayUser != null)
                .Select(Projections.Distinct(Projections.Property(() => assignedVisitPayUserAlias.VisitPayUserId)))
                .List<int>();
        }

        private ClientSupportRequestResults GetClientSupportRequestTotals(ClientSupportRequestFilter filter)
        {
            int totalRecords = this.QueryOverClientSupportRequests(filter).RowCount();

            return new ClientSupportRequestResults
            {
                TotalRecords = totalRecords
            };
        }

        private IReadOnlyList<ClientSupportRequest> DoGetClientSupportRequests(ClientSupportRequestFilter filter)
        {
            IQueryOver<ClientSupportRequest, ClientSupportRequest> query = this.QueryOverClientSupportRequests(filter);
            Sort(filter, query);

            IList<ClientSupportRequest> supportRequests = query
                .Skip((filter.Page - 1)*filter.Rows)
                .Take(filter.Rows)
                .List();

            return (IReadOnlyList<ClientSupportRequest>) supportRequests;
        }

        private IQueryOver<ClientSupportRequest, ClientSupportRequest> QueryOverClientSupportRequests(ClientSupportRequestFilter filter)
        {
            ClientSupportRequest clientSupportRequestAlias = null;
            ClientSupportRequestStatus clientSupportRequestStatusAlias = null;
            ClientSupportTopic clientSupportTopicAlias = null;
            ClientSupportTopic parentClientSupportTopicAlias = null;
            VisitPayUser assignedVisitPayUserAlias = null;
            VisitPayUser createdVisitPayUserAlias = null;

            IQueryOver<ClientSupportRequest, ClientSupportRequest> query = this.Session.QueryOver(() => clientSupportRequestAlias)
                .JoinAlias(x => x.AssignedVisitPayUser, () => assignedVisitPayUserAlias, JoinType.LeftOuterJoin) // because it's nullable
                .JoinAlias(x => x.VisitPayUser, () => createdVisitPayUserAlias)
                .JoinAlias(x => x.ClientSupportRequestStatusSource, () => clientSupportRequestStatusAlias)
                .JoinAlias(x => x.ClientSupportTopic, () => clientSupportTopicAlias)
                .JoinAlias(x => clientSupportTopicAlias.ParentClientSupportTopic, () => parentClientSupportTopicAlias);

            Filter(filter, query);

            return query;
        }

        private static void Filter(ClientSupportRequestFilter filter, IQueryOver<ClientSupportRequest, ClientSupportRequest> query)
        {
            ClientSupportRequest clientSupportRequestAlias = null;

            string supportRequestId = !string.IsNullOrEmpty(filter.ClientSupportRequestDisplayId) ? string.Format("-{0}", Regex.Replace(filter.ClientSupportRequestDisplayId, "[^0-9]", "")) : null;

            query.If(!string.IsNullOrEmpty(supportRequestId), q => q.Where(x => x.ClientSupportRequestDisplayId.IsLike(supportRequestId, MatchMode.End)))
                .If(filter.ClientSupportRequestStatus.HasValue, q => q.Where(x => x.ClientSupportRequestStatus == filter.ClientSupportRequestStatus.Value))
                .If(filter.CreatedByVisitPayUserId.HasValue, q => q.Where(x => x.VisitPayUser.VisitPayUserId == filter.CreatedByVisitPayUserId.Value))
                .If(filter.TargetVpGuarantorId.HasValue, q => q.Where(x => x.TargetVpGuarantor.VpGuarantorId == filter.TargetVpGuarantorId.Value))
                .If(!string.IsNullOrEmpty(filter.JiraId), q => q.Where(x => x.JiraId.IsLike(filter.JiraId, MatchMode.Anywhere)));

            if (filter.AssignedToVisitPayUserId.HasValue)
            {
                query = filter.AssignedToVisitPayUserId.Value == -1 ? query.Where(x => x.AssignedVisitPayUser == null) : query.Where(x => x.AssignedVisitPayUser.VisitPayUserId == filter.AssignedToVisitPayUserId.Value);
            }

            QueryOver<ClientSupportRequestMessage> lastMessageDateSubquery = QueryOver.Of<ClientSupportRequestMessage>()
                .Where(q => q.ClientSupportRequest.ClientSupportRequestId == clientSupportRequestAlias.ClientSupportRequestId)
                .OrderBy(q => q.InsertDate).Desc
                .Select(q => q.InsertDate)
                .Take(1);

            query.If(filter.DaysOffset.HasValue, q => q.Where(Restrictions.Gt(Projections.SubQuery(lastMessageDateSubquery.DetachedCriteria), DateTime.UtcNow.AddDays(filter.DaysOffset.GetValueOrDefault(0)))));
        }

        private static void Sort(ClientSupportRequestFilter filter, IQueryOver<ClientSupportRequest, ClientSupportRequest> query)
        {
            if (string.IsNullOrEmpty(filter.SortField))
            {
                return;
            }

            ClientSupportRequest clientSupportRequestAlias = null;
            ClientSupportRequestStatus clientSupportRequestStatusAlias = null;
            ClientSupportTopic clientSupportTopicAlias = null;
            ClientSupportTopic parentClientSupportTopicAlias = null;
            VisitPayUser assignedVisitPayUserAlias = null;
            VisitPayUser createdVisitPayUserAlias = null;

            bool isAscendingOrder = "asc".Equals(filter.SortOrder, StringComparison.InvariantCultureIgnoreCase);

            switch (filter.SortField)
            {
                case "AssignedToVisitPayUserName":
                    query.UnderlyingCriteria.AddOrder(new Order(Projections.Property(() => assignedVisitPayUserAlias.LastName), isAscendingOrder));
                    query.UnderlyingCriteria.AddOrder(new Order(Projections.Property(() => assignedVisitPayUserAlias.FirstName), isAscendingOrder));
                    break;
                case "CreatedByVisitPayUserName":
                    query.UnderlyingCriteria.AddOrder(new Order(Projections.Property(() => createdVisitPayUserAlias.LastName), isAscendingOrder));
                    query.UnderlyingCriteria.AddOrder(new Order(Projections.Property(() => createdVisitPayUserAlias.FirstName), isAscendingOrder));
                    break;
                case "InsertDate":
                    query.UnderlyingCriteria.AddOrder(new Order(Projections.Property(() => clientSupportRequestAlias.InsertDate), isAscendingOrder));
                    break;
                case "JiraId":
                    query.UnderlyingCriteria.AddOrder(new Order(Projections.Property(() => clientSupportRequestAlias.JiraId), isAscendingOrder));
                    break;
                case "LastModifiedByVisitPayUserName":
                    VisitPayUser messageVisitPayUserAlias = null;
                    QueryOver<ClientSupportRequestMessage> subquery1 = QueryOver.Of<ClientSupportRequestMessage>()
                        .JoinAlias(x => x.CreatedByVisitPayUser, () => messageVisitPayUserAlias)
                        .Where(x => x.ClientSupportRequest.ClientSupportRequestId == clientSupportRequestAlias.ClientSupportRequestId)
                        .OrderBy(x => x.InsertDate).Desc
                        .Select(x => messageVisitPayUserAlias.LastName)
                        .Take(1);

                    query.UnderlyingCriteria.AddOrder(new Order(Projections.SubQuery(subquery1.DetachedCriteria), isAscendingOrder));
                    break;
                case "LastModifiedDate":
                    QueryOver<ClientSupportRequestMessage> subquery2 = QueryOver.Of<ClientSupportRequestMessage>()
                        .Where(x => x.ClientSupportRequest.ClientSupportRequestId == clientSupportRequestAlias.ClientSupportRequestId)
                        .OrderBy(x => x.InsertDate).Desc
                        .Select(x => x.InsertDate)
                        .Take(1);

                    query.UnderlyingCriteria.AddOrder(new Order(Projections.SubQuery(subquery2.DetachedCriteria), isAscendingOrder));
                    break;
                case "StatusDisplay":
                    query.UnderlyingCriteria.AddOrder(new Order(Projections.Property(() => clientSupportRequestStatusAlias.ClientSupportRequestStatusName), isAscendingOrder));
                    break;
                case "TargetVpGuarantorId":
                    query.UnderlyingCriteria.AddOrder(new Order(Projections.Property(() => clientSupportRequestAlias.TargetVpGuarantor.VpGuarantorId), isAscendingOrder));
                    break;
                case "TopicDisplay":
                    query.UnderlyingCriteria.AddOrder(new Order(Projections.Property(() => parentClientSupportTopicAlias.ClientSupportTopicName), isAscendingOrder));
                    query.UnderlyingCriteria.AddOrder(new Order(Projections.Property(() => clientSupportTopicAlias.ClientSupportTopicName), isAscendingOrder));
                    break;
            }

            query.UnderlyingCriteria.AddOrder(new Order(Projections.Property(() => clientSupportRequestAlias.ClientSupportRequestId), isAscendingOrder));
        }
    }
}
﻿namespace Ivh.Provider.SecureCommunication
{
    using Autofac;
    using ClientSupportRequests;
    using Domain.SecureCommunication.ClientSupportRequests.Interfaces;
    using Domain.SecureCommunication.GuarantorSupportRequests.Interfaces;
    using GuarantorSupportRequests;

    public class Module : Autofac.Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<ClientSupportRequestRepository>().As<IClientSupportRequestRepository>();
            builder.RegisterType<ClientSupportRequestMessageRepository>().As<IClientSupportRequestMessageRepository>();
            builder.RegisterType<ClientSupportTopicRepository>().As<IClientSupportTopicRepository>();

            builder.RegisterType<SupportRequestRepository>().As<ISupportRequestRepository>();
            builder.RegisterType<SupportRequestMessageRepository>().As<ISupportRequestMessageRepository>();
            builder.RegisterType<SupportTopicRepository>().As<ISupportTopicRepository>();
            builder.RegisterType<SupportTemplateRepository>().As<ISupportTemplateRepository>();
        }
    }
}

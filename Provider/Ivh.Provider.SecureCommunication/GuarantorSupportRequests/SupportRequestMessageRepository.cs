﻿namespace Ivh.Provider.SecureCommunication.GuarantorSupportRequests
{
    using System.Collections.Generic;
    using System.Linq;
    using Common.Data;
    using Common.Data.Connection.Connections;
    using Common.Data.Interfaces;
    using Common.VisitPay.Enums;
    using Domain.Guarantor.Entities;
    using Domain.SecureCommunication.GuarantorSupportRequests.Entities;
    using Domain.SecureCommunication.GuarantorSupportRequests.Interfaces;
    using NHibernate;
    using NHibernate.Criterion;
    using NHibernate.Dialect.Function;

    public class SupportRequestMessageRepository : RepositoryBase<SupportRequestMessage, VisitPay>, ISupportRequestMessageRepository
    {
        public SupportRequestMessageRepository(ISessionContext<VisitPay> sessionContext)
            : base(sessionContext)
        {
        }

        public SupportRequestMessage GetSupportRequestMessage(int supportRequestId, int supportRequestMessageId)
        {
            IQueryOver<SupportRequestMessage> query = this.Session.QueryOver<SupportRequestMessage>()
                                                          .Where(x => x.SupportRequest.SupportRequestId == supportRequestId &&
                                                                      x.SupportRequestMessageId == supportRequestMessageId)
                                                          .Take(1);

            return query.List().FirstOrDefault();
        }

        public IList<SupportRequestMessage> GetUnreadMessages(int vpGuarantorId)
        {
            SupportRequest supportRequestAlias = null;
            Guarantor guarantorAlias = null;
            IList<SupportRequestMessage> unreadMessages = this.Session.QueryOver<SupportRequestMessage>()
                                                              .JoinAlias(s => s.SupportRequest, () => supportRequestAlias)
                                                              .JoinAlias(g => supportRequestAlias.VpGuarantor, () => guarantorAlias)
                                                              .Where(x => x.SupportRequestMessageType == SupportRequestMessageTypeEnum.ToGuarantor && guarantorAlias.VpGuarantorId == vpGuarantorId && !x.IsRead)
                                                              .List();


            return unreadMessages;
        }

        public IList<SupportRequestMessageCountResult> GetUnreadMessagesToGuarantorCount(int vpGuarantorId)
        {
            SupportRequest supportRequestAlias = null;
            Guarantor guarantorAlias = null;

            IList<int> unreadMessagesSupportRequestIds = this.Session.QueryOver<SupportRequestMessage>()
                .JoinAlias(s => s.SupportRequest, () => supportRequestAlias)
                .JoinAlias(g => supportRequestAlias.VpGuarantor, () => guarantorAlias)
                .Where(x => x.SupportRequestMessageType == SupportRequestMessageTypeEnum.ToGuarantor && guarantorAlias.VpGuarantorId == vpGuarantorId && !x.IsRead)
                .SelectList(x => x.Select(() => supportRequestAlias.SupportRequestId))
                .List<int>();

            IList<SupportRequestMessageCountResult> results = unreadMessagesSupportRequestIds.GroupBy(x => x).Select(x => new SupportRequestMessageCountResult
            {
                Count = x.Count(),
                SupportRequestId = x.Key
            }).ToList();

            return results;
        }

        public int GetAllUnreadMessagesFromGuarantorsCount()
        {
            SupportRequest supportRequestAlias = null;

            return this.Session.QueryOver<SupportRequestMessage>()
                .JoinAlias(s => s.SupportRequest, () => supportRequestAlias)
                .Where(Restrictions.GtProperty(Projections.SqlFunction(new SQLFunctionTemplate(NHibernateUtil.DateTime, "dateadd(SECOND, -1, ?1)"), NHibernateUtil.DateTime, Projections.Property<SupportRequestMessage>(x => x.InsertDate)), Projections.Property(() => supportRequestAlias.InsertDate))) // first message is not considered unread
                .Where(x => x.SupportRequestMessageType == SupportRequestMessageTypeEnum.FromGuarantor && !x.IsRead)
                .RowCount();
        }

        public SupportRequestMessage GetSupportRequestDraftMessage(int supportRequestId, int createdByVpUserId)
        {
            IQueryOver<SupportRequestMessage> query = this.Session.QueryOver<SupportRequestMessage>()
                .Where(x => x.SupportRequest.SupportRequestId == supportRequestId &&
                            x.CreatedByVpUserId == createdByVpUserId &&
                            x.SupportRequestMessageType == SupportRequestMessageTypeEnum.DraftMessage)
                .Take(1);

            return query.List().FirstOrDefault();
        }
    }
}
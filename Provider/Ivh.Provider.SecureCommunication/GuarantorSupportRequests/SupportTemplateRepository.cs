﻿namespace Ivh.Provider.SecureCommunication.GuarantorSupportRequests
{
    using System.Collections.Generic;
    using Common.Data;
    using Common.Data.Connection.Connections;
    using Common.Data.Interfaces;
    using Domain.SecureCommunication.GuarantorSupportRequests.Entities;
    using Domain.SecureCommunication.GuarantorSupportRequests.Interfaces;
    using NHibernate;

    public class SupportTemplateRepository : RepositoryBase<SupportTemplate, VisitPay>, ISupportTemplateRepository
    {
        public SupportTemplateRepository(ISessionContext<VisitPay> sessionContext) : base(sessionContext)
        { }

        public IList<SupportTemplate> GetSupportTemplatesForUser(int vpUserId)
        {
            //Get templates for the user, as well as global templates (will have a null userId)
            IList<SupportTemplate> query = this.Session.QueryOver<SupportTemplate>()
                .Where(x => x.VisitPayUserId == vpUserId || x.VisitPayUserId == null)
                .List();

            return query;
        }
    }
}

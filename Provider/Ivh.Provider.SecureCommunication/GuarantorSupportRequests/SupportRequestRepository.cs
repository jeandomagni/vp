﻿namespace Ivh.Provider.SecureCommunication.GuarantorSupportRequests
{
    using Common.Data;
    using Common.Data.Connection.Connections;
    using Common.Data.Interfaces;
    using Common.VisitPay.Enums;
    using Domain.Guarantor.Entities;
    using Domain.SecureCommunication.GuarantorSupportRequests.Entities;
    using Domain.SecureCommunication.GuarantorSupportRequests.Interfaces;
    using Domain.User.Entities;
    using Magnum.Extensions;
    using NHibernate;
    using NHibernate.Criterion;
    using NHibernate.SqlCommand;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text.RegularExpressions;

    public class SupportRequestRepository : RepositoryBase<SupportRequest, VisitPay>, ISupportRequestRepository
    {
        public SupportRequestRepository(ISessionContext<VisitPay> sessionContext)
            : base(sessionContext)
        {
        }

        public SupportRequest GetSupportRequest(int vpGuarantorId, int supportRequestId)
        {
            IQueryOver<SupportRequest> query = this.Session.QueryOver<SupportRequest>()
                                                .Where(x => x.VpGuarantor.VpGuarantorId == vpGuarantorId && x.SupportRequestId == supportRequestId)
                                                .Take(1);

            return query.List().FirstOrDefault();
        }

        public IList<SupportRequest> GetSupportRequestForVisit(int vpGuarantorId, int visitId)
        {
            IList<SupportRequest> query = this.Session.QueryOver<SupportRequest>()
                .Where(x => x.VpGuarantor.VpGuarantorId == vpGuarantorId)
                .Where(x => x.Visit != null && x.Visit.VisitId == visitId)
                .List();

            return query;
        }

        public IList<int> GetAllAssignedUsers()
        {
            VisitPayUser assignedVisitPayUserAlias = null;

            return this.Session.QueryOver<SupportRequest>()
                .JoinAlias(x => x.AssignedVisitPayUser, () => assignedVisitPayUserAlias, JoinType.LeftOuterJoin)
                .Where(x => x.AssignedVisitPayUser != null)
                .Select(Projections.Distinct(Projections.Property(() => assignedVisitPayUserAlias.VisitPayUserId)))
                .List<int>();
        }

        public SupportRequestResults GetSupportRequests(int? vpGuarantorId, SupportRequestFilter filter, int batch, int batchSize)
        {
            //if they don't want to see read or unread messages, we know we have nothing to show them. No need to go to the database.
            if (filter.ReadMessages.HasValue && filter.UnreadMessages.HasValue && filter.ReadMessages == false && filter.UnreadMessages == false)
            {
                return new SupportRequestResults();
            }

            SupportRequestResults supportRequestResults = this.GetSupportRequestTotals(vpGuarantorId, filter);
            if (supportRequestResults.TotalRecords == 0)
            {
                supportRequestResults.SupportRequests = new List<SupportRequest>();
            }
            else
            {
                supportRequestResults.SupportRequests = this.DoGetSupportRequests(vpGuarantorId, filter, batch, batchSize);
            }

            return supportRequestResults;
        }

        private IReadOnlyList<SupportRequest> PostQueryFilter(SupportRequestFilter filter, IReadOnlyList<SupportRequest> supportRequests)
        {
            if (!filter.ReadMessages.HasValue && !filter.UnreadMessages.HasValue || filter.ReadMessages == true && filter.UnreadMessages == true)
            {
                return supportRequests;
            }

            if (filter.ReadMessages == true)
            {
                return supportRequests
                    .Where(p => p.SupportRequestMessages
                                    .Where(z => z.SupportRequestMessageType == SupportRequestMessageTypeEnum.FromGuarantor)
                                    .OrderBy(z => z.InsertDate)
                                    .Skip(1)
                                    .Count(z => !z.IsRead) == 0).ToList();
            }
            return supportRequests
                .Where(p => p.SupportRequestMessages
                                .Where(z => z.SupportRequestMessageType == SupportRequestMessageTypeEnum.FromGuarantor)
                                .OrderBy(z => z.InsertDate)
                                .Skip(1)
                                .Count(z => !z.IsRead) > 0).ToList();
        }

        private IReadOnlyList<SupportRequest> DoGetSupportRequests(int? vpGuarantorId, SupportRequestFilter filter, int batch, int batchSize)
        {
            IQueryOver<SupportRequest, SupportRequest> query = this.QueryOverSupportRequests(vpGuarantorId, filter);
            Sort(filter, query);

            IList<SupportRequest> supportRequests = query
                .Skip(batch * batchSize)
                .Take(batchSize)
                .List();

            return (IReadOnlyList<SupportRequest>)supportRequests;
        }

        public SupportRequestResults GetSupportRequestTotals(int? vpGuarantorId, SupportRequestFilter filter)
        {
            int totalRecords = this.QueryOverSupportRequests(vpGuarantorId, filter)
                                   .RowCount();

            return new SupportRequestResults
            {
                TotalRecords = totalRecords
            };
        }

        private IQueryOver<SupportRequest, SupportRequest> QueryOverSupportRequests(int? vpGuarantorId, SupportRequestFilter filter)
        {
            SupportRequest supportRequestAlias = null;
            SupportRequestStatus supportRequestStatusAlias = null;
            SupportTopic supportTopicAlias = null;
            SupportTopic parentSupportTopicAlias = null;
            Guarantor guarantorAlias = null;
            Guarantor submittedGuarantorAlias = null;
            VisitPayUser visitPayUserAlias = null;
            VisitPayUser submittedUserAlias = null;
            VisitPayUser assignedVisitPayUserAlias = null;

            IQueryOver<SupportRequest, SupportRequest> query = this.Session.QueryOver(() => supportRequestAlias)
                .JoinAlias(ss => ss.SupportRequestStatusSource, () => supportRequestStatusAlias)
                .JoinAlias(st => st.SupportTopic, () => supportTopicAlias, JoinType.LeftOuterJoin)
                .JoinAlias(stp => supportTopicAlias.ParentSupportTopic, () => parentSupportTopicAlias, JoinType.LeftOuterJoin)
                .JoinAlias(g => g.VpGuarantor, () => guarantorAlias)
                .JoinAlias(vpu => guarantorAlias.User, () => visitPayUserAlias)
                .JoinAlias(sg => sg.SubmittedForVpGuarantor, () => submittedGuarantorAlias, JoinType.LeftOuterJoin)
                .JoinAlias(su => submittedGuarantorAlias.User, () => submittedUserAlias, JoinType.LeftOuterJoin)
                .JoinAlias(x => x.AssignedVisitPayUser, () => assignedVisitPayUserAlias, JoinType.LeftOuterJoin);

            query.If(vpGuarantorId.HasValue, q => q.Where(x => x.VpGuarantor.VpGuarantorId == vpGuarantorId));

            Filter(filter, query);

            return query;
        }

        private static void Filter(SupportRequestFilter filter, IQueryOver<SupportRequest, SupportRequest> query)
        {
            SupportRequest supportRequestAlias = null;

            int? supportRequestId = null;

            if (filter.SupportRequestDisplayId.IsNotEmpty())
            {
                supportRequestId = Convert.ToInt32(Regex.Replace(filter.SupportRequestDisplayId, "[^0-9]", ""));
            }

            if (filter.AssignedToVisitPayUserId.HasValue)
            {
                query = filter.AssignedToVisitPayUserId.Value == -1 ? query.Where(x => x.AssignedVisitPayUser == null) : query.Where(x => x.AssignedVisitPayUser.VisitPayUserId == filter.AssignedToVisitPayUserId.Value);
            }

            QueryOver<SupportRequestMessage> lastMessageDateSubquery = QueryOver.Of<SupportRequestMessage>()
                                                                         .Where(x => x.SupportRequest.SupportRequestId == supportRequestAlias.SupportRequestId)
                                                                         .OrderBy(x => x.InsertDate).Desc
                                                                         .Select(x => x.InsertDate)
                                                                         .Take(1);

            query.If(filter.SupportRequestStatus.HasValue, q => q.Where(() => supportRequestAlias.SupportRequestStatus == filter.SupportRequestStatus.Value))
                 .If(filter.DateRangeFrom.HasValue, q => q.Where(Restrictions.Gt(Projections.SubQuery(lastMessageDateSubquery.DetachedCriteria), filter.DateRangeFrom.Value)))
                 .If(supportRequestId.HasValue, q => q.Where(() => supportRequestAlias.SupportRequestId == supportRequestId));

            FilterRead(query, filter.ReadMessages, filter.UnreadMessages);
        }

        private static void FilterRead(IQueryOver<SupportRequest, SupportRequest> query, bool? readMessages, bool? unreadMessages)
        {
            if (!readMessages.HasValue && !unreadMessages.HasValue)
            {
                // neither, nothing to do
                return;
            }

            if (readMessages.GetValueOrDefault(false) && unreadMessages.GetValueOrDefault(false))
            {
                // it's both, nothing to do
                return;
            }

            SupportRequest supportRequestAlias = null;
            QueryOver<SupportRequestMessage> subQuery = QueryOver.Of<SupportRequestMessage>()
                .Where(x => x.SupportRequest.SupportRequestId == supportRequestAlias.SupportRequestId &&
                            x.SupportRequestMessageType == SupportRequestMessageTypeEnum.FromGuarantor)
                .OrderBy(x => x.InsertDate).Asc
                .Select(x => x.IsRead)
                .Skip(1);

            if (unreadMessages.HasValue && unreadMessages.Value)
            {
                // where any message has IsRead = false
                query.WithSubquery.WhereValue(false).In(subQuery);
            }
            else
            {
                // where all messages don't have IsRead = false
                query.WithSubquery.WhereValue(false).NotIn(subQuery);
            }
        }

        // WARNING: COUPLING
        // The sorting of name properties in "switch (filter.SortField)" below 
        // assumes FirstName and LastName in derived properties are displayed in the same order as the criteria
        private static void Sort(SupportRequestFilter filter, IQueryOver<SupportRequest, SupportRequest> query)
        {
            if (filter.SortField.IsEmpty())
            {
                return;
            }

            SupportRequest supportRequestAlias = null;
            SupportRequestStatus supportRequestStatusAlias = null;
            SupportTopic supportTopicAlias = null;
            SupportTopic parentSupportTopicAlias = null;
            VisitPayUser visitPayUserAlias = null;
            VisitPayUser submittedUserAlias = null;
            VisitPayUser assignedVisitPayUserAlias = null;

            bool isAscendingOrder = "asc".Equals(filter.SortOrder, StringComparison.InvariantCultureIgnoreCase);

            switch (filter.SortField)
            {
                case "AssignedToVisitPayUserName":
                    query.UnderlyingCriteria.AddOrder(new Order(Projections.Property(() => assignedVisitPayUserAlias.FirstName), isAscendingOrder)); // used in derived name property
                    query.UnderlyingCriteria.AddOrder(new Order(Projections.Property(() => assignedVisitPayUserAlias.LastName), isAscendingOrder)); // used in derived name property
                    SortLastModifiedDate(query, false);
                    break;
                case "GuarantorFullName":
                    query.UnderlyingCriteria.AddOrder(new Order(Projections.Property(() => visitPayUserAlias.FirstName), isAscendingOrder)); // used in derived name property
                    query.UnderlyingCriteria.AddOrder(new Order(Projections.Property(() => visitPayUserAlias.LastName), isAscendingOrder)); // used in derived name property
                    break;
                case "InsertDate":
                    query.UnderlyingCriteria.AddOrder(new Order("InsertDate", isAscendingOrder));
                    break;
                case "LastMessageBy":
                    SortLastModifiedDate(query, isAscendingOrder);
                    break;
                case "LastMessageDate":
                    QueryOver<SupportRequestMessage> lastMessageDateSubquery = QueryOver.Of<SupportRequestMessage>()
                                                                                 .Where(x => x.SupportRequest.SupportRequestId == supportRequestAlias.SupportRequestId)
                                                                                 .OrderBy(x => x.InsertDate).Desc
                                                                                 .Select(x => x.InsertDate)
                                                                                 .Take(1);

                    query.UnderlyingCriteria.AddOrder(new Order(Projections.SubQuery(lastMessageDateSubquery.DetachedCriteria), isAscendingOrder));
                    break;
                case "SupportRequestDisplayId":
                    query.UnderlyingCriteria.AddOrder(new Order("SupportRequestId", isAscendingOrder));
                    query.UnderlyingCriteria.AddOrder(new Order("InsertDate", isAscendingOrder));
                    break;
                case "SupportRequestStatus":
                    query.UnderlyingCriteria.AddOrder(new Order(Projections.Property(() => supportRequestStatusAlias.SupportRequestStatusName), isAscendingOrder));
                    break;
                case "IsFlaggedForFollowUp":
                    query.UnderlyingCriteria.AddOrder(new Order("IsFlaggedForFollowUp", isAscendingOrder));
                    break;
                case "SupportTopic":
                    query.UnderlyingCriteria.AddOrder(new Order(Projections.Property(() => parentSupportTopicAlias.SupportTopicName), isAscendingOrder));
                    query.UnderlyingCriteria.AddOrder(new Order(Projections.Property(() => supportTopicAlias.SupportTopicName), isAscendingOrder));
                    break;
                case "SubmittedFor":
                    query.UnderlyingCriteria.AddOrder(new Order(Projections.Conditional(Restrictions.On<SupportRequest>(x => x.SubmittedForVpGuarantor).IsNull,
                        Projections.Property(() => visitPayUserAlias.FirstName), // used in derived name property
                        Projections.Property(() => submittedUserAlias.FirstName)), // used in derived name property
                        isAscendingOrder));

                    query.UnderlyingCriteria.AddOrder(new Order(Projections.Conditional(Restrictions.On<SupportRequest>(x => x.SubmittedForVpGuarantor).IsNull,
                        Projections.Property(() => visitPayUserAlias.LastName), // used in derived name property
                        Projections.Property(() => submittedUserAlias.LastName)), // used in derived name property
                        isAscendingOrder));

                    query.UnderlyingCriteria.AddOrder(new Order("InsertDate", isAscendingOrder));
                    break;

            }

            if (filter.SortField != "SupportRequestDisplayId")
            {
                query.UnderlyingCriteria.AddOrder(new Order(Projections.Property(() => supportRequestAlias.SupportRequestId), isAscendingOrder));
            }
        }

        // WARNING: COUPLING
        // The sorting of name properties below assumes FirstName and LastName 
        // in derived properties are displayed in the same order as the criteria
        private static void SortLastModifiedDate(IQueryOver<SupportRequest, SupportRequest> query, bool isAscendingOrder)
        {
            SupportRequest supportRequestAlias = null;
            VisitPayUser messageVisitPayUserAlias = null;

            QueryOver<SupportRequestMessage> lastMessageByFirstNameSubquery = QueryOver.Of<SupportRequestMessage>()
                .JoinAlias(x => x.VisitPayUser, () => messageVisitPayUserAlias)
                .Where(x => x.SupportRequest.SupportRequestId == supportRequestAlias.SupportRequestId)
                .OrderBy(x => x.InsertDate).Desc
                .Select(x => messageVisitPayUserAlias.FirstName) // used in derived name property
                .Take(1);

            QueryOver<SupportRequestMessage> lastMessageByLastNameSubquery = QueryOver.Of<SupportRequestMessage>()
                .JoinAlias(x => x.VisitPayUser, () => messageVisitPayUserAlias)
                .Where(x => x.SupportRequest.SupportRequestId == supportRequestAlias.SupportRequestId)
                .OrderBy(x => x.InsertDate).Desc
                .Select(x => messageVisitPayUserAlias.LastName) // used in derived name property
                .Take(1);

            // assumes derived properties using FirstName and LastName are displayed in the same order as the criteria
            query.UnderlyingCriteria.AddOrder(new Order(Projections.SubQuery(lastMessageByFirstNameSubquery.DetachedCriteria), isAscendingOrder));
            query.UnderlyingCriteria.AddOrder(new Order(Projections.SubQuery(lastMessageByLastNameSubquery.DetachedCriteria), isAscendingOrder));
        }
    }
}
﻿namespace Ivh.Provider.SecureCommunication.GuarantorSupportRequests
{
    using Common.Data;
    using Common.Data.Connection.Connections;
    using Common.Data.Interfaces;
    using Domain.SecureCommunication.GuarantorSupportRequests.Entities;
    using Domain.SecureCommunication.GuarantorSupportRequests.Interfaces;
    using NHibernate;

    public class SupportTopicRepository : RepositoryBase<SupportTopic, VisitPay>, ISupportTopicRepository
    {
        public SupportTopicRepository(ISessionContext<VisitPay> sessionContext)
            : base(sessionContext)
        {
        }
    }
}

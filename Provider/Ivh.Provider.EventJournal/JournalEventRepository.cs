﻿namespace Ivh.Provider.EventJournal
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Common.Base.Enums;
    using Common.Base.Utilities.Extensions;
    using Common.Data;
    using Common.Data.Connection.Connections;
    using Common.Data.Interfaces;
    using Common.VisitPay.Enums;
    using Domain.EventJournal.Entities;
    using Domain.EventJournal.Interfaces;
    using NHibernate;
    using NHibernate.Criterion;

    public class JournalEventRepository : RepositoryBase<JournalEvent, Logging>, IJournalEventRepository
    {
        public JournalEventRepository(ISessionContext<Logging> sessionContext) : base(sessionContext)
        {
        }

        public JournalEventResults GetJournalEvents(JournalEventFilter filter)
        {
            JournalEventResults journalEventResults = new JournalEventResults
            {
                TotalRecords = this.JournalEventCount(filter),
                JournalEvents = this.JournalEvents(filter)
            };
            return journalEventResults;
        }

        public JournalEvent GetLastEventOfType(JournalEventTypeEnum journalEventType)
        {
            JournalEvent journalEvent = this.Session.QueryOver<JournalEvent>()
                .Where(x => x.JournalEventType == journalEventType)
                .OrderBy(x => x.EventDateTime)
                .Desc
                .Take(1).SingleOrDefault();

            return journalEvent;
        }

        public int JournalEventCount(JournalEventFilter filter)
        {
            ICriteria criteria = this.Session.CreateCriteria<JournalEvent>();
            this.Filter(criteria, filter);
            return criteria.SetProjection(Projections.RowCount()).UniqueResult<int>();
        }

        private IReadOnlyList<JournalEvent> JournalEvents(JournalEventFilter filter)
        {
            ICriteria criteria = this.Session.CreateCriteria<JournalEvent>();
            this.Filter(criteria, filter);

            bool isAscendingOrder = "asc".Equals(filter.SortOrder, StringComparison.InvariantCultureIgnoreCase);

            switch (filter.SortField ?? string.Empty)
            {
                case "VpGuarantorId":
                    criteria.AddOrder(new Order(nameof(JournalEvent.VpGuarantorId), isAscendingOrder));
                    break;
                case "UserName":
                    criteria.CreateCriteria(nameof(JournalEvent.EventVisitPayUser))
                        .AddOrder(new Order(nameof(JournalEventVisitPayUser.UserName), isAscendingOrder));
                    break;
                case "EventDate":
                    criteria.AddOrder(new Order(nameof(JournalEvent.EventDateTime), isAscendingOrder));
                    break;
            }

            if (filter.Page.HasValue && filter.Rows.HasValue)
            {
                criteria.SetFirstResult((filter.Page.Value - 1) * filter.Rows.Value);
                criteria.SetMaxResults(filter.Rows.Value);
            }

            return (IReadOnlyList<JournalEvent>)criteria.List<JournalEvent>();
        }

        private void Filter(ICriteria criteria, JournalEventFilter filter)
        {
            criteria.Add(Restrictions.Ge(nameof(JournalEvent.EventDateTime), filter.StartDate))
                .Add(Restrictions.Le(nameof(JournalEvent.EventDateTime), filter.EndDate));

            if (filter.JournalEventTypes.IsNotNullOrEmpty())
            {
                criteria.Add(Restrictions.In(nameof(JournalEvent.JournalEventType), filter.JournalEventTypes.ToArray()));
            }
            if (filter.JournalEventCategories.IsNotNullOrEmpty())
            {
                criteria.Add(Restrictions.In(nameof(JournalEvent.JournalEventCategory), filter.JournalEventCategories.ToArray()));
            }
            if (filter.VpGuarantorId.HasValue)
            {
                criteria.Add(Restrictions.Eq(nameof(JournalEvent.VpGuarantorId), filter.VpGuarantorId));
            }
            if (filter.UserName.IsNotNullOrEmpty())
            {
                criteria.CreateCriteria(nameof(JournalEvent.EventVisitPayUser))
                    .Add(Restrictions.Eq(nameof(JournalEventVisitPayUser.UserName), filter.UserName));
            }
        }
    }
}

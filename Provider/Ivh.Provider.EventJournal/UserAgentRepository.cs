﻿namespace Ivh.Provider.EventJournal
{
    using System.Security.Cryptography.X509Certificates;
    using Common.Data;
    using Common.Data.Connection.Connections;
    using Common.Data.Interfaces;
    using Domain.EventJournal.Entities;
    using Domain.EventJournal.Interfaces;
    using NHibernate;
    using NHibernate.Criterion;

    public class UserAgentRepository : RepositoryBase<UserAgent, VisitPay>, IUserAgentRepository
    {
        public UserAgentRepository(ISessionContext<VisitPay> sessionContext) : base(sessionContext)
        {

        }

        public bool Exists(int userAgentId)
        {
            ICriteria criteria = this.Session.CreateCriteria(typeof(UserAgent));
            criteria.Add(Restrictions.Eq("UserAgentId", userAgentId));
            return criteria.List<UserAgent>().Count > 0;
        }
    }
}

﻿namespace Ivh.Provider.EventJournal.Modules
{
    using Autofac;
    using Domain.EventJournal.Interfaces;

    public class Module : Autofac.Module
    {
      
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<JournalEventRepository>().As<IJournalEventRepository>();
            builder.RegisterType<UserAgentRepository>().As<IUserAgentRepository>();
        }
    }
}

﻿namespace Ivh.Provider.Settings
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Diagnostics;
    using System.Threading;
    using System.Threading.Tasks;
    using Common.Base.Utilities.Extensions;
    using Common.Cache;
    using Domain.Settings.Entities;
    using Domain.Settings.Interfaces;
    using System.Linq;
    using System.Security.Cryptography;
    using System.Text;
    using Common.Base.Collections;
    using Common.Configuration.Interfaces;
    using Common.VisitPay.Constants;
    using Common.VisitPay.Enums;
    using Common.Encryption;

    public class SettingsProviderParameters
    {

#if DEBUG
        public const int DefaultTimeToLive = 0; //This is in seconds
#else
        public const int DefaultTimeToLive = 60 * 5; //This is in seconds
#endif

        public string ClientDataApiUrl { get; set; }
        public string ClientDataApiAppId { get; set; }
        public string ClientDataApiAppKey { get; set; }
        public int ClientDataApiTimeToLive { get; set; } = DefaultTimeToLive;
        public ApplicationEnum Application { get; set; }
        public string ApplicationName { get; set; }
    }

    public class SettingsProvider : ISettingsProvider
    {
        private readonly Lazy<IClientSettingRepository> _clientSettingRepository;
        private readonly Lazy<ICache> _cache;
        private readonly Lazy<IAppSettingsProvider> _appSettingsProvider;
        private readonly SettingsProviderParameters _parameters;

        public static long Ticks = DateTime.UtcNow.Ticks;

        private readonly string _cacheKey;
        private readonly string _backupCacheKey;

        private static volatile IReadOnlyDictionary<string, string> _settings = null;
        private static volatile string _settingsHashCode = null;

        private static DateTime _expires = DateTime.UtcNow;

        public string SettingsHashCode => _settingsHashCode;

        private IReadOnlyDictionary<string, string> SettingsFromDatastores
        {
            get
            {
                if (_settings.IsNullOrEmpty() || _expires <= DateTime.UtcNow)
                {
                    if (SettingsSemaphore.Wait(5000))
                    {
                        try
                        {
                            if (_settings == null || _expires <= DateTime.UtcNow)
                            {
                                CaseInsensitiveDictionary<string> appSettings = this.GetSettingsFromAppConfig();
                                IDictionary<string,string> settings = appSettings;
                                CaseInsensitiveDictionary<string> dbSettings = this.GetSettingsFromDb(decrypt: true);
                                foreach (KeyValuePair<string, string> setting in dbSettings)
                                {
                                    if (settings.ContainsKey(setting.Key))
                                    {
                                        settings[setting.Key] = setting.Value;
                                    }
                                    else
                                    {
                                        settings.Add(setting);
                                    }
                                }
                                _settings = new ReadOnlyDictionary<string, string>(settings);
                                _expires = DateTime.UtcNow.AddSeconds(SettingsProviderParameters.DefaultTimeToLive);

                                if (_settings != null)
                                {
                                    _settingsHashCode = _settings.GetHashCode<string, string, UTF8Encoding, SHA1Managed>();
                                }
                            }
                        }
                        finally
                        {
                            SettingsSemaphore.Release();
                        }
                    }
                    else
                    {
                        Trace.TraceWarning("SettingsFromDb::WARNING Semaphore wasn't entered!");
                    }
                }

                return _settings;
            }
        }

        private static readonly SemaphoreSlim SettingsSemaphore = new SemaphoreSlim(1, 1);

        public static string GetCacheKey(string apiAppId, string apiAppKey, bool backup = false)
        {
            string key = string.Empty;
            if (backup)
            {
                key = $"Ivh.Provider.Settings.SettingsProvider.Settings.{apiAppId}.{apiAppKey}.Backup";
            }
            else
            {
                key = $"Ivh.Provider.Settings.SettingsProvider.Settings.{apiAppId}.{apiAppKey}";
            }
            return key.GetHashCode().ToString();
        }



        public SettingsProvider(
            Lazy<IClientSettingRepository> clientSettingRepository,
            Lazy<ICache> cache,
            Lazy<IAppSettingsProvider> appSettingsProvider,
            SettingsProviderParameters parameters
            )
        {
            this._cache = cache;
            this._appSettingsProvider = appSettingsProvider;
            this._parameters = parameters;
            this._clientSettingRepository = clientSettingRepository;
            this._cacheKey = GetCacheKey(this._parameters.ClientDataApiAppId, this._parameters.ClientDataApiAppKey);
            this._backupCacheKey = GetCacheKey(this._parameters.ClientDataApiAppId, this._parameters.ClientDataApiAppKey, true);
        }

        public string GetSettingsHash()
        {
            return this.GetAllSettings().GetHashCodeHex<string, string, UTF8Encoding, SHA1Managed>();
        }

        public string GetManagedSettingsHash()
        {
            return this.GetAllManagedSettings().GetHashCodeHex<string, string, UTF8Encoding, SHA1Managed>();
        }

        public string GetEditableManagedSettingsHash()
        {
            return this.GetAllEditableManagedSettings().GetHashCodeHex<string, string, UTF8Encoding, SHA1Managed>();
        }

        public IReadOnlyDictionary<string, string> GetAllSettings()
        {
            return this.SettingsFromDatastores;
        }

        public IReadOnlyDictionary<string, string> GetAllManagedSettings()
        {
            CaseInsensitiveDictionary<string> settings = this.GetSettingsFromDb();
            return new ReadOnlyDictionary<string, string>(settings);
        }

        public IReadOnlyDictionary<string, string> GetAllEditableManagedSettings()
        {
            CaseInsensitiveDictionary<string> settings = this.GetSettingsFromDb(decrypt: false, onlyEditable: true);
            return new ReadOnlyDictionary<string, string>(settings);
        }

        private CaseInsensitiveDictionary<string> GetSettingsFromAppConfig()
        {
            Trace.TraceInformation($"{nameof(this.GetSettingsFromAppConfig)}");
            CaseInsensitiveDictionary<string> settings = new CaseInsensitiveDictionary<string>();
            foreach (string key in this._appSettingsProvider.Value.AllKeys)
            {
                settings[key] = this._appSettingsProvider.Value.Get(key);
            }
            return settings;
        }

        private CaseInsensitiveDictionary<string> GetSettingsFromDb(bool decrypt = false, bool onlyEditable = false)
        {
            Trace.TraceInformation($"{nameof(this.GetSettingsFromDb)}");
            CaseInsensitiveDictionary<string> settings = new CaseInsensitiveDictionary<string>();
            foreach (ClientSetting clientSetting in this._clientSettingRepository.Value.GetAllSettings())
            {
                string settingValue = clientSetting.ClientSettingValueOverride ?? clientSetting.ClientSettingValue;
                if (decrypt && clientSetting.IsEncrypted)
                {
                    settingValue = AESThenHMAC.SimpleDecrypt(
                       secretMessage: settingValue,
                       cryptKey: this._appSettingsProvider.Value.Get(ClientSettingConstants.EncryptionKey),
                       authKey: this._appSettingsProvider.Value.Get(ClientSettingConstants.AuthKey),
                       nonSecretPayloadLength: ClientSettingConstants.RandomnessLength
                    );
                }

                if (onlyEditable)
                {
                    if (clientSetting.EditInUI)
                    {
                        settings[clientSetting.ClientSettingKey] = settingValue;
                    }
                }
                else
                {
                    settings[clientSetting.ClientSettingKey] = settingValue;
                }
            }

            return settings;
        }

        public async Task<bool> OverrideManagedSettingAsync(string key, string overrideValue)
        {
            ClientSetting clientSetting = this._clientSettingRepository.Value.GetQueryable().SingleOrDefault(x => x.ClientSettingKey == key);
            if (clientSetting != null && clientSetting.EditInUI)
            {
                clientSetting.ClientSettingValueOverride = overrideValue;
                this._clientSettingRepository.Value.InsertOrUpdate(clientSetting);

                // Need to invalidate the cache so a reload from the DB is needed the next time a setting is requested
                return await this.InvalidateCacheAsync();
            }
            return false;
        }

        public async Task<bool> ClearManagedSettingOverridesAsync()
        {
            List<ClientSetting> clientSettingOverrides = this._clientSettingRepository.Value.GetQueryable().Where(x => x.ClientSettingValueOverride != null).ToList();
            foreach (ClientSetting clientSetting in clientSettingOverrides)
            {
                clientSetting.ClientSettingValueOverride = null;
                this._clientSettingRepository.Value.InsertOrUpdate(clientSetting);
            }
            return await this.InvalidateCacheAsync();
        }

        public async Task<bool> ClearManagedSettingOverrideAsync(string key)
        {
            ClientSetting clientSetting = this._clientSettingRepository.Value.GetQueryable().SingleOrDefault(x => x.ClientSettingKey == key);
            if (clientSetting != null)
            {
                clientSetting.ClientSettingValueOverride = null;
                this._clientSettingRepository.Value.InsertOrUpdate(clientSetting);
            }
            return await this.InvalidateCacheAsync();
        }

        public async Task<bool> InvalidateCacheAsync()
        {
            return await Task.Run(() =>
            {
                try
                {
                    SettingsSemaphore.Wait();
                    _settings = null;
                    _expires = DateTime.MinValue;
                    return this._cache.Value.RemoveAsync(this._cacheKey).Result
                           && this._cache.Value.RemoveAsync(this._backupCacheKey).Result;//Do we really want to remove the backup?
                }
                finally
                {
                    SettingsSemaphore.Release();
                }
            });
        }
    }
}
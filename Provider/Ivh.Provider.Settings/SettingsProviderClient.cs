﻿namespace Ivh.Provider.Settings
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Diagnostics;
    using System.Net.Http;
    using System.Security.Cryptography;
    using System.Text;
    using System.Threading;
    using System.Threading.Tasks;
    using Common.Api.Client;
    using Common.Base.Collections;
    using Common.Base.Utilities.Extensions;
    using Common.ResiliencePolicies.Interfaces;
    using Domain.Settings.Interfaces;
    using Logging.Database.Interfaces;
    using Newtonsoft.Json;

    public class SettingsProviderClient : ISettingsProvider
    {
        private static readonly object Lock = new object();
        private static HttpClient _httpClient;
        private static string _requestUrl;

        private const int ServiceCallMaxIterations = 20;
        private const int ServiceCallIntervalWait = 100;
        private const double ApiTimeout = 15d;

        private static volatile IReadOnlyDictionary<string, string> _settings = null;
        private static volatile string _settingsHashCode = null;
        private static readonly SemaphoreSlim SettingsSemaphore = new SemaphoreSlim(1, 1);
        private static DateTime _expires = DateTime.UtcNow;

        private readonly SettingsProviderParameters _parameters;
        private readonly Lazy<ILoggingProvider> _logger;

        private static Lazy<IHttpRequestPolicy> _requestPolicy;

        public SettingsProviderClient(
            Lazy<IHttpRequestPolicy> requestPolicy,
            Lazy<ILoggingProvider> logger,
            SettingsProviderParameters parameters
            )
        {
            this._logger = logger;
            this._parameters = parameters;

            if (_requestPolicy == null)
            {
                _requestPolicy = requestPolicy;
            }

            if (_httpClient == null)
            {
                lock (Lock)
                {
                    if (_httpClient == null)
                    {
                        _httpClient = HttpClientFactory.Create(new ApiHandler(this._parameters.ClientDataApiAppId, this._parameters.ClientDataApiAppKey, this._parameters.ApplicationName));
                        _httpClient.Timeout = TimeSpan.FromSeconds(ApiTimeout);
                        _requestUrl = parameters.ClientDataApiUrl + "api/settings/getsettings";

                        AppDomain.CurrentDomain.DomainUnload += (e, o) => { _httpClient?.Dispose(); };
                    }
                }
            }

        }

        private string GetCacheKey()
        {
            // TODO: settings by build version.. and application?
            return _requestUrl;
        }

        public string SettingsHashCode => _settingsHashCode;
        public string GetSettingsHash()
        {
            return this.GetAllSettings().GetHashCodeHex<string, string, UTF8Encoding, SHA1Managed>();
        }

        public string GetManagedSettingsHash()
        {
            string settingsHash = this.QueryClientDataApi<string>("GetManagedSettingsHash");

            return settingsHash;
        }

        public string GetEditableManagedSettingsHash()
        {
            string settingsHash = this.QueryClientDataApi<string>("GetEditableManagedSettingsHash");

            return settingsHash;
        }

        public IReadOnlyDictionary<string, string> GetAllSettings()
        {
            for (int i = 0; i < ServiceCallMaxIterations; i++)
            {
                IReadOnlyDictionary<string, string> settings = this.Settings;
                if (settings == null)
                {
                    Thread.Sleep(ServiceCallIntervalWait);
                }
                else
                {
                    return settings;
                }
            }

            string exceptionMessage1 () => string.Format("Failed to call the Settings API (no cached settings).");
            this._logger.Value.Fatal(exceptionMessage1);
            throw new ApplicationException(exceptionMessage1());
        }

        public IReadOnlyDictionary<string, string> GetAllManagedSettings()
        {
            IReadOnlyDictionary<string,string> settings = this.QueryClientDataApi<ReadOnlyDictionary<string, string>>("GetManagedSettings");

            return settings;
        }

        public IReadOnlyDictionary<string, string> GetAllEditableManagedSettings()
        {
            IReadOnlyDictionary<string, string> settings = this.QueryClientDataApi<ReadOnlyDictionary<string, string>>("GetEditableManagedSettings");

            return settings;
        }

        public async Task<bool> InvalidateCacheAsync()
        {
            _settings = null;
            _expires = DateTime.MinValue;

            return await _requestPolicy.Value.InvalidateCache();
        }

        public Task<bool> OverrideManagedSettingAsync(string key, string overrideValue)
        {
            bool result = this.PostClientDataApi<bool>("OverrideManagedSetting", "settings", new Dictionary<string, string>
            {
                {"key", key},
                {"overrideValue", overrideValue}
            });

            return Task.FromResult(result);
        }

        public Task<bool> ClearManagedSettingOverridesAsync()
        {
            bool result = this.QueryClientDataApi<bool>("ClearManagedSettingOverrides");

            return Task.FromResult(result);
        }

        public Task<bool> ClearManagedSettingOverrideAsync(string key)
        {
            string parameters = $"key={key}";
            bool result = this.QueryClientDataApi<bool>("ClearManagedSettingOverride", parameters: parameters);

            return Task.FromResult(result);
        }

        private IReadOnlyDictionary<string, string> Settings
        {
            get
            {
                if (_settings.IsNullOrEmpty() || _expires <= DateTime.UtcNow)
                {
                    if (SettingsSemaphore.Wait(5000))
                    {
                        try
                        {
                            if (_settings == null || _expires <= DateTime.UtcNow)
                            {
                                _settings = this.GetSettings();
                                if (_settings != null)
                                {
                                    _settingsHashCode = _settings.GetHashCode<string, string, UTF8Encoding, SHA1Managed>();
                                    _expires = DateTime.UtcNow.AddSeconds(SettingsProviderParameters.DefaultTimeToLive);
                                }
                            }
                        }
                        finally
                        {
                            SettingsSemaphore.Release();
                        }
                    }
                    else
                    {
                        Trace.TraceWarning("Settings::WARNING Semaphore wasn't entered!");
                    }
                }

                return _settings;
            }
        }

        //This should only really get called from the Settings property, the lock should be engaged before this method is called.
        private IReadOnlyDictionary<string, string> GetSettings()
        {
            string requestCacheKey = this.GetCacheKey();

            IReadOnlyDictionary<string, string> result = Task.Run(async () => await _requestPolicy.Value.GetAsync<CaseInsensitiveDictionary<string>>(
                        _requestUrl,
                        _httpClient,
                        requestCacheKey)
                    .ConfigureAwait(false)).Result;

            return result;
        }

        private HttpClient CreateClientDataApiHttpClient()
        {
            HttpClient client = HttpClientFactory.Create(new ApiHandler(this._parameters.ClientDataApiAppId, this._parameters.ClientDataApiAppKey, this._parameters.ApplicationName));
            client.Timeout = TimeSpan.FromSeconds(ApiTimeout);
            return client;
        }

        private string GetClientDataApiRequestString(string action, string controller, string parameters)
        {
            string requestString = $"{this._parameters.ClientDataApiUrl}api/{controller}/{action}";

            if (!string.IsNullOrEmpty(parameters))
            {
                requestString += $"?{parameters}";
            }

            return requestString;
        }

        private TResponse QueryClientDataApi<TResponse>(string action, string controller = "settings", string parameters = "")
        {
            string request = this.GetClientDataApiRequestString(action, controller, parameters);

            using(HttpClient client = this.CreateClientDataApiHttpClient())
            {
                HttpResponseMessage response = Task.Run(async () => await client.GetAsync(request).ConfigureAwait(false)).Result;
                if (response.IsSuccessStatusCode)
                {
                    string responseString = Task.Run(async () => await response.Content.ReadAsStringAsync().ConfigureAwait(false)).Result;
                    TResponse responseObject = JsonConvert.DeserializeObject<TResponse>(responseString);
                    
                    return responseObject;
                }

                this._logger.Value.Fatal(() => $"SettingsProvider::Failed to call the Settings API because of response code. WebResponse: {response.AggregateWebResponse(System.Reflection.MethodBase.GetCurrentMethod().Name)}");

                return default(TResponse);
            }
        }

        private TResponse PostClientDataApi<TResponse>(string action, string controller, IDictionary<string, string> parameters)
        {
            string request = this.GetClientDataApiRequestString(action, controller, string.Empty);

            using(HttpClient client = this.CreateClientDataApiHttpClient())
            {
                HttpResponseMessage response = Task.Run(async () => await client.PostAsync(request, new FormUrlEncodedContent(parameters)).ConfigureAwait(false)).Result;
                if (response.IsSuccessStatusCode)
                {
                    string responseString = Task.Run(async () => await response.Content.ReadAsStringAsync().ConfigureAwait(false)).Result;
                    TResponse responseObject = JsonConvert.DeserializeObject<TResponse>(responseString);
                    
                    return responseObject;
                }

                this._logger.Value.Fatal(() => $"SettingsProvider::Failed to call the Settings API because of response code. WebResponse: {response.AggregateWebResponse(System.Reflection.MethodBase.GetCurrentMethod().Name)}");

                return default(TResponse);
            }
        }  
    }
}

﻿namespace Ivh.Provider.Settings
{
    using System.Collections.Generic;
    using System.Linq;
    using Common.Data;
    using Common.Data.Connection;
    using Common.Data.Connection.Connections;
    using Common.Data.Interfaces;
    using Domain.Settings.Entities;
    using Domain.Settings.Interfaces;

    public class ClientSettingRepository : RepositoryBase<ClientSetting, VisitPay>, IClientSettingRepository
    {
        public ClientSettingRepository(ISessionContext<VisitPay> sessionContext, IStatelessSessionContext<VisitPay> statelessSessionContext) : base(sessionContext, statelessSessionContext)
        {

        }

        public IList<ClientSetting> GetAllSettings()
        {
            return this.StatelessSession.CreateCriteria<ClientSetting>().SetCacheable(true).List<ClientSetting>();
            //return this.GetAll().ToList();
            //return this.Session..CreateCriteria<ClientSetting>().SetCacheable(true).List<ClientSetting>();
        }
    }
}
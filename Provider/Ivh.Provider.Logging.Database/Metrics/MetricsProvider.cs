﻿namespace Ivh.Provider.Logging.Database.Metrics
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading;
    using Common.VisitPay.Enums;
    using Domain.Logging.Interfaces;
    using StatsdClient;

    public class MetricsProvider : IMetricsProvider
    {
        private readonly string _applicationName;
        private readonly string _environment;
        private readonly IvhEnvironmentTypeEnum _environmentType;
        private readonly string _clientName;
        private static readonly object DogStatsDLock = new object();

        private static volatile bool _isConfigured = false;

        public MetricsProvider(
            string statsDServerName,
            string statsdPort,
            string applicationName,
            IvhEnvironmentTypeEnum environmentType,
            string environment,
            string clientName)
        {
            this._clientName = clientName;
            this._environment = environment;
            this._environmentType = environmentType;
            this._applicationName = applicationName;


            if (!_isConfigured
                && !string.IsNullOrWhiteSpace(statsDServerName)
                && !string.IsNullOrWhiteSpace(statsdPort)
                && !string.IsNullOrWhiteSpace(applicationName))
            {
                lock (DogStatsDLock)
                {
                    if (!_isConfigured && !string.IsNullOrWhiteSpace(statsDServerName) && !string.IsNullOrWhiteSpace(statsdPort) && !string.IsNullOrWhiteSpace(applicationName))
                    {
                        int parsedStatsdPort = -1;
                        if (int.TryParse(statsdPort, out parsedStatsdPort))
                        {
                            var dogstatsdConfig = new StatsdConfig
                            {
                                StatsdServerName = statsDServerName, //"127.0.0.1",
                                StatsdPort = parsedStatsdPort, //8125, // Optional; default is 8125
                                Prefix = applicationName
                            };
                            DogStatsd.Configure(dogstatsdConfig);
                            _isConfigured = true;
                        }
                    }
                }
            }
        }

        public void Gauge<T>(string statName, T value, double sampleRate = 1, params string[] tags)
        {
            if (_isConfigured)
            {
                ThreadPool.QueueUserWorkItem(x =>
                {
                    try
                    {
                        DogStatsd.Gauge(statName, value, sampleRate, this.mergeTags(this.BuildDefaultTags(), tags));
                    }
                    catch
                    {
                        //Do nothing if it fails.
                    }
                });
            }
        }

        public void Increment(string statName, int value = 1, double sampleRate = 1, params string[] tags)
        {
            if (_isConfigured)
            {
                ThreadPool.QueueUserWorkItem(x =>
                {
                    try
                    {
                        DogStatsd.Increment(statName, value, sampleRate, this.mergeTags(this.BuildDefaultTags(), tags));
                    }
                    catch
                    {
                        //Do nothing if it fails.
                    }
                });
            }
        }



        public void Decrement(string statName, int value = 1, double sampleRate = 1, params string[] tags)
        {
            if (_isConfigured)
            {
                ThreadPool.QueueUserWorkItem(x =>
                {
                    try
                    {
                        DogStatsd.Decrement(statName, value, sampleRate, this.mergeTags(this.BuildDefaultTags(), tags));
                    }
                    catch
                    {
                        //Do nothing if it fails.
                    }
                });
            }
        }

        private string[] mergeTags(string[] strings, string[] tags)
        {
            var castedStrings = strings != null ? strings.ToList() : new List<string>();
            if (tags != null)
            {
                castedStrings.AddRange(tags);
            }
            return castedStrings.ToArray();
        }

        private string[] BuildDefaultTags()
        {
            return new[] {$"machine:{System.Environment.MachineName}", $"client:{this._clientName}", $"application:{this._applicationName}", $"environmentType:{this._environmentType}", $"environment:{this._environment}"};
        }
    }
}
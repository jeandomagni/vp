﻿namespace Ivh.Provider.Logging.Database.Metrics
{
    using System;
    using Common.Data.Connection.Connections;
    using Common.Data.Interfaces;
    using Common.VisitPay.Constants;
    using Domain.Logging.Interfaces;

    public class MetricsLogger : LoggingProvider, ILoggingProvider
    {
        private readonly IMetricsProvider _metricsProvider;

        public MetricsLogger(
            IMetricsProvider metricsProvider,
            string applicationName,
            string applicationVersion,
            Lazy<ILogStackTraceRepository> logStackTraceRepository,
            Lazy<ILogRepository> logRepository,
            Lazy<IStatelessSessionContext<Logging>> statelessSessionContext)
            : base(applicationName, applicationVersion, logStackTraceRepository, logRepository, statelessSessionContext)
        {
            this._metricsProvider = metricsProvider;
        }

        public override void Debug(Func<string> message, params object[] parameters)
        {
            this._metricsProvider.Increment(Metrics.Increment.Logging.Debug);
            base.Debug(message, parameters);
        }

        public override void Fatal(Func<string> message, string stackTrace, params object[] parameters)
        {
            this._metricsProvider.Increment(Metrics.Increment.Logging.Fatal);
            base.Fatal(message, stackTrace, parameters);
        }

        public override void Info(Func<string> message, params object[] parameters)
        {
            this._metricsProvider.Increment(Metrics.Increment.Logging.Info);
            base.Info(message, parameters);
        }

        public override void Warn(Func<string> message, params object[] parameters)
        {
            this._metricsProvider.Increment(Metrics.Increment.Logging.Warn);
            base.Warn(message, parameters);
        }
    }
}
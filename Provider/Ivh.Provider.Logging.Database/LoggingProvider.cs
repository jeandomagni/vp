﻿namespace Ivh.Provider.Logging.Database
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Linq;
    using System.Threading;
    using Common.Base.Utilities.Extensions;
    using Common.Base.Utilities.Helpers;
    using Common.Data;
    using Common.Data.Connection.Connections;
    using Common.Data.Interfaces;
    using Common.ServiceBus.Attributes;
    using Common.ServiceBus.Enums;
    using Common.VisitPay.Enums;
    using Common.VisitPay.Messages.Logging;
    using Domain.Logging.Entities;
    using Domain.Logging.Interfaces;
    using MassTransit;
    using CorrelationManager = Common.Base.Logging.CorrelationManager;

    public class LoggingProvider : Interfaces.ILoggingProvider
    {
        static LoggingProvider()
        {
            Stopwatch.Start();
        }

        private class Level
        {
            public const string Debug = nameof(Debug);
            public const string Fatal = nameof(Fatal);
            public const string Info = nameof(Info);
            public const string Warn = nameof(Warn);
        }

        private const int BatchSize = 25;
        private static readonly TimeSpan BatchMaxAge = TimeSpan.FromSeconds(60);
        private static readonly TimeSpan StopwatchMaxElapsedTime = TimeSpan.FromSeconds(5);
        private static readonly Stopwatch Stopwatch = new Stopwatch();

        private static readonly SemaphoreSlim Semaphore = new SemaphoreSlim(1, 1);
        private Guid? _correlationId;
        private int? _threadId;
        private DateTime? _date;
        private string _applicationName;
        private string _applicationVersion;
        private readonly Lazy<ILogStackTraceRepository> _stackTraceRepository;
        private readonly Lazy<ILogRepository> _logRepository;
        private readonly Lazy<IStatelessSessionContext<Logging>> _statelessSession;

        private static readonly IList<string> Filter = new List<string>()
        {
            "System.Environment.GetStackTrace",
            "System.Environment.get_StackTrace",
            "DbLoggerBase",
            "MetricLogger"
        };

        public LoggingProvider(
            string applicationName,
            string applicationVersion,
            Lazy<ILogStackTraceRepository> stackTraceRepository,
            Lazy<ILogRepository> logRepository,
            Lazy<IStatelessSessionContext<Logging>> statelessSessionContext)
        {
            this._applicationName = applicationName;
            this._applicationVersion = applicationVersion;
            this._stackTraceRepository = stackTraceRepository;
            this._logRepository = logRepository;
            this._statelessSession = statelessSessionContext;
        }

        public void SetApplicationNameAndVersion(string applicationName, string applicationVersion, DateTime date, int threadId, Guid correlationId)
        {
            this._applicationName = applicationName;
            this._applicationVersion = applicationVersion;
            this._date = date;
            this._threadId = threadId;
            this._correlationId = correlationId;
        }

        public void SetApplicationNameAndVersion(string applicationName, string applicationVersion)
        {
            this.SetApplicationNameAndVersion(
                applicationName,
                applicationVersion,
                DateTime.UtcNow,
                Thread.CurrentThread.ManagedThreadId,
                CorrelationManager.CorrelationId);
        }

        public virtual void Debug(Func<string> message, params object[] parameters)
        {
            this.InsertLog(Level.Debug, message, null, parameters);
        }

        public virtual void Fatal(Func<string> message, params object[] parameters)
        {
            string stack = StackUtility.GetStack(1).FirstOrDefault();
            int? stackTraceHashCode = this.InsertLogStackTrace(stack);
            this.InsertLog(Level.Fatal, message, stackTraceHashCode, parameters);
        }

        public virtual void Fatal(Func<string> message, string stackTrace, params object[] parameters)
        {
            int? stackTraceHashCode = this.InsertLogStackTrace(stackTrace);
            this.InsertLog(Level.Fatal, message, stackTraceHashCode, parameters);
        }

        public virtual void Info(Func<string> message, params object[] parameters)
        {
            this.InsertLog(Level.Info, message, null, parameters);
        }

        public virtual void Warn(Func<string> message, params object[] parameters)
        {
            this.InsertLog(Level.Warn, message, null, parameters);
        }

        private int? InsertLogStackTrace(string stackTrace)
        {
            if (stackTrace.IsNotNullOrEmpty())
            {
                string Trim(string s)
                {
                    IEnumerable<string> p = s.Trim()
                        .Split(new string[] { "\r\n" }, StringSplitOptions.RemoveEmptyEntries)
                        .Where(q => !Filter.Any(q.Contains))
                        .Select(x => x.Trim());
                    return string.Join("\r\n", p).Trim();
                }

                stackTrace = Trim(stackTrace);
                int stackTraceHashCode = stackTrace.GetHashCode();

                try
                {
                    Semaphore.Wait();
                    using (StatelessUnitOfWork statelessUnitOfWork = new StatelessUnitOfWork(this._statelessSession.Value.Session))
                    {
                        // check so see if this one is already in the database // if not, add it
                        if (!this._stackTraceRepository.Value.Exists(x => x.StackTraceHashCode == stackTraceHashCode))
                        {
                            this._stackTraceRepository.Value.Insert(new LogStackTrace()
                            {
                                StackTrace = stackTrace,
                                StackTraceHashCode = stackTraceHashCode,
                                FirstOccurrence = DateTime.UtcNow,
                                MostRecentOccurrence = DateTime.UtcNow,
                                NumberOfOccurrences = 1
                            });
                        }
                        else
                        {
                            this._stackTraceRepository.Value.Increment(stackTraceHashCode);
                        }
                        statelessUnitOfWork.Commit();
                    }
                }
                finally
                {
                    Semaphore.Release();
                }

                return stackTraceHashCode;
            }
            return default(int?);
        }

        private void InsertLog(string level, Func<string> message, int? stackTraceHashCode, params object[] parameters)
        {
            string messageLocal = default(string);
            try
            {
                messageLocal = message();
            }
            catch (Exception ex)
            {
                try
                {
                    this.Fatal(() => ExceptionHelper.AggregateExceptionToString(ex));
                }
                catch (Exception ex1)
                {
                    this.Fatal(() => $"Swallowing exception to prevent infinite in logging provider. Exception = {ex1.Message}; Original Exception = {ex1.Message}");
                }
                return;
            }

            this._logRepository.Value.BatchedInsert(entity: new Log()
            {
                Date = this._date ?? DateTime.UtcNow,
                Level = level,
                Message = parameters == null || parameters.Length == 0 ? messageLocal : string.Format(messageLocal, parameters),
                Logger = this._applicationName,
                Version = this._applicationVersion,
                Thread = (this._threadId ?? Thread.CurrentThread.ManagedThreadId).ToString(),
                CorrelationId = this._correlationId ?? CorrelationManager.CorrelationId,
                StackTraceHashCode = stackTraceHashCode
            },
            // will batch log entries until either:
            insertWhen: x =>
            {
                // a) BatchSize is reached; or
                if (x.Count >= BatchSize)
                {
                    Stopwatch.Restart();
                    return true;
                }
                // b) Most recent message is FATAL
                if (x.Any(y => y.Level.Equals(Level.Fatal)))
                {
                    Stopwatch.Restart();
                    return true;
                }
                // c) The oldest message in the batch is more than _batchMaxAge in the past
                if ((Stopwatch.Elapsed > StopwatchMaxElapsedTime && (DateTime.UtcNow - x.First().Date) >= BatchMaxAge))
                {
                    Stopwatch.Restart();
                    return true;
                }

                return false;
            });
        }

        #region Message Consumers

        [UniversalConsumer(UniversalQueuesEnum.VisitPayProcessor, UniversalPriorityEnum.Priority40)]
        public void ConsumeMessage(LogMessage message, ConsumeContext<LogMessage> consumeContext)
        {
            this.SetApplicationNameAndVersion(message.ApplicationName, message.ApplicationVersion, message.Date, message.ThreadId, message.CorrelationId);

            switch (message.LogMessageTypeEnum)
            {
                case LogMessageTypeEnum.Debug:
                    this.Debug(() => message.Message, message.Parameters);
                    break;
                case LogMessageTypeEnum.Info:
                    this.Info(() => message.Message, message.Parameters);
                    break;
                case LogMessageTypeEnum.Warn:
                    this.Warn(() => message.Message, message.Parameters);
                    break;
                case LogMessageTypeEnum.Fatal:
                    this.Fatal(() => message.Message, message.StackTrace, message.Parameters);
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        public bool IsMessageValid(LogMessage message, ConsumeContext<LogMessage> consumeContext)
        {
            bool isValid = true;
            return isValid;
        }

        #endregion
    }
}
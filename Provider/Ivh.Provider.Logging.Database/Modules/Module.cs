﻿namespace Ivh.Provider.Logging.Database.Modules
{
    using Autofac;
    using Common.Base.Utilities.Extensions;
    using Common.Configuration;
    using Common.VisitPay.Enums;
    using Domain.Logging.Interfaces;
    using Metrics;

    public class Module : Autofac.Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            string applicationName = Ivh.Common.Properties.Settings.Default.ApplicationName ?? "ClientDataApi"; // this is just a fallback in case it is not set in the config file

            string dataDogServerName = AppSettingsProvider.Instance.Get("DataDogServerName");
            string dataDogPort = AppSettingsProvider.Instance.Get("DataDogPort");
            string applicationVersion = this.GetType().Assembly.GetName().Version.ToString();
            IvhEnvironmentTypeEnum environmentType = AppSettingsProvider.Instance.Get("EnvironmentType").ToEnum(IvhEnvironmentTypeEnum.Unknown);
            string environment = AppSettingsProvider.Instance.Get("Environment") ?? "Unknown";
            string client = AppSettingsProvider.Instance.Get("Client");
            string connectionString = ConnectionStringsProvider.Instance.GetConnectionString("LoggingConnection");

            builder.RegisterType<LogStackTraceRepository>().As<ILogStackTraceRepository>();
            builder.RegisterType<LogRepository>().As<ILogRepository>();

            builder.RegisterType<MetricsProvider>().As<IMetricsProvider>().WithParameters(new[]
            {
                new NamedParameter("statsDServerName", dataDogServerName),
                new NamedParameter("statsdPort", dataDogPort),
                new NamedParameter("applicationName", applicationName),
                new NamedParameter("environmentType", environmentType),
                new NamedParameter("environment", environment),
                new NamedParameter("clientName", client)
            });


            // this will force database logging
            builder.RegisterType<MetricsLogger>()
                .As<Database.Interfaces.ILoggingProvider>()
                .WithParameters(new[]
                {
                    new NamedParameter("applicationName", applicationName),
                    new NamedParameter("applicationVersion", applicationVersion),
                    new NamedParameter("connectionString", connectionString),
                });

        }
    }
}

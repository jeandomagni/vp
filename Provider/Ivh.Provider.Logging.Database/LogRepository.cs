﻿namespace Ivh.Provider.Logging.Database
{
    using Common.Data;
    using Common.Data.Connection.Connections;
    using Common.Data.Interfaces;
    using Domain.Logging.Entities;
    using Domain.Logging.Interfaces;

    public class LogRepository : RepositoryBase<Log, Logging>, ILogRepository
    {
        public LogRepository(IStatelessSessionContext<Logging> sessionContext) : base(sessionContext)
        {
        }
    }
}

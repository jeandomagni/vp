﻿namespace Ivh.Provider.Logging.Database
{
    using System;
    using Common.Base.Constants;
    using Common.Data;
    using Common.Data.Connection.Connections;
    using Common.Data.Interfaces;
    using Domain.Logging.Entities;
    using Domain.Logging.Interfaces;
    using NHibernate;
    using System.Linq;
    using NHibernate.Linq;

    public class LogStackTraceRepository : RepositoryBase<LogStackTrace, Logging>, ILogStackTraceRepository
    {
        public LogStackTraceRepository(ISessionContext<Logging> sessionContext, IStatelessSessionContext<Logging> statelessSessionContext) : base(sessionContext, statelessSessionContext)
        {
        }

        public void Increment(int stackTraceHashCode)
        {
            this.StatelessSession.Query<LogStackTrace>()
                .Where(c => c.StackTraceHashCode == stackTraceHashCode)
                .UpdateBuilder()
                .Set(c => c.MostRecentOccurrence, DateTime.UtcNow)
                .Set(c => c.NumberOfOccurrences, c => c.NumberOfOccurrences + 1)
                .Update();
        }

        public override void Insert(LogStackTrace logStackTrace)
        {
            this.StatelessSession.Insert(logStackTrace);
        }
    }
}

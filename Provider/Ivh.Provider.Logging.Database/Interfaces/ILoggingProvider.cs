﻿namespace Ivh.Provider.Logging.Database.Interfaces
{
    using Common.VisitPay.Messages.Logging;
    using Ivh.Common.ServiceBus.Interfaces;

    public interface ILoggingProvider : Ivh.Domain.Logging.Interfaces.ILoggingProvider, IUniversalConsumerService<LogMessage>
    {
    }
}

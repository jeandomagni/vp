﻿namespace Ivh.Provider.Content.Modules
{
    using Autofac;
    using Common.DependencyInjection;
    using Common.VisitPay.Enums;
    using Content;
    using Domain.Content.Interfaces;

    public class ContentProviderModule : BaseModule
    {
        private readonly RegistrationSettings _registrationSettings;

        public ContentProviderModule(RegistrationSettings registrationSettings)
            : base()
        {
            this._registrationSettings = registrationSettings;
        }

        protected override void Load(ContainerBuilder builder)
        {
            if(this._registrationSettings.Application == ApplicationEnum.ClientDataApi)
            {
                builder.RegisterType<ContentProvider>().As<IContentProvider>();
            }
            else
            {
                builder.RegisterType<ContentProviderClient>().As<IContentProvider>();
            }

            builder.RegisterType<ImageProvider>().As<IImageProvider>();
            builder.RegisterType<ThemeProvider>().As<IThemeProvider>();
            builder.RegisterType<CmsRegionRepository>().As<ICmsRegionRepository>();
            builder.RegisterType<CmsVersionRepository>().As<ICmsVersionRepository>();
            builder.RegisterType<ContentMenuRepository>().As<IContentMenuRepository>();
        }
    }
}
﻿namespace Ivh.Provider.Content
{
    using System;
    using System.Collections.Concurrent;
    using System.Collections.Generic;
    using System.Linq;
    using System.Security.Cryptography;
    using System.Text;
    using System.Web;
    using Common.Base.Utilities.Extensions;
    using Common.Base.Utilities.Helpers;
    using Common.Cache;
    using Common.VisitPay.Enums;
    using Domain.Content.Entities;
    using Domain.Content.Interfaces;
    using Domain.Logging.Interfaces;
    using Domain.Settings.Interfaces;
    using NSass;

    internal class ThemeCacheItem
    {
        public ThemeCacheItem()
        {
            this.Stylesheets = new Dictionary<string, string>();
        }

        public string SettingsHash { get; set; }

        public IDictionary<string, string> Stylesheets { get; }
    }

    public class ThemeProvider : IThemeProvider
    {
        private static readonly IDictionary<string, ThemeConfiguration> ThemeConfiguration = new Dictionary<string, ThemeConfiguration>(StringComparer.OrdinalIgnoreCase)
        {
            {
                "/theme", new ThemeConfiguration
                {
                    BaseFile = "_base.scss", 
                    IncludeDirectory = HttpContext.Current.Server.MapPath("~/content/css"),
                    SettingPrefix = "/theme"
                }
            },
            {
                "/theme-mobile", new ThemeConfiguration
                {
                    BaseFile = "_base.scss", 
                    IncludeDirectory = HttpContext.Current.Server.MapPath("~/areas/mobile/content/css"),
                    SettingPrefix = "/theme"
                }
            },
            {
                "/theme-print", new ThemeConfiguration
                {
                    BaseFile = "_print.scss", 
                    IncludeDirectory = HttpContext.Current.Server.MapPath("~/content/css"),
                    SettingPrefix = "/theme-print"
                }
            },
            {
                "/theme-guestpay", new ThemeConfiguration
                {
                    BaseFile = "_base.scss",
                    IncludeDirectory = HttpContext.Current.Server.MapPath("~/content/css"),
                    SettingPrefix = "/theme"
                }
            },
        };
        
        private const int CacheExpiration = 60 * 60 * 12;// 12 hours.

        private readonly Lazy<ISettingsService> _settingsService;
        private readonly Lazy<IApplicationSettingsService> _applicationSettingsService;
        private readonly Lazy<IMemoryCache> _cache;
        private readonly Lazy<ILoggingProvider> _logger;
        private static readonly ConcurrentDictionary<string, string> ThemeHash = new ConcurrentDictionary<string, string>();
        private static readonly ThemeCacheItem ThemeCache = new ThemeCacheItem();
        private static readonly string SettingsHashCacheKey = $"{nameof(ThemeProvider)}::SettingsHash";
        private static readonly SHA1Managed Sha1Managed = new SHA1Managed();
        
        public ThemeProvider(
            Lazy<ISettingsService> settingsService,
            Lazy<IApplicationSettingsService> applicationSettingsService,
            Lazy<IMemoryCache> cache,
            Lazy<ILoggingProvider> logger)
        {
            this._settingsService = settingsService;
            this._applicationSettingsService = applicationSettingsService;
            this._cache = cache;
            this._logger = logger;
        }

        public string GetStylesheet(string requestedFile)
        {
            return this.GetStylesheetInternal(requestedFile);
        }

        public string GetStylesheetHash(string requestedFile)
        {
            this.GetStylesheetInternal(requestedFile);

            return ThemeHash.ContainsKey(requestedFile) ? ThemeHash[requestedFile] : string.Empty;
        }

        private string GetStylesheetInternal(string requestedFile)
        {
            IReadOnlyDictionary<string, string> settings = this._settingsService.Value.GetAllSettingsAsync().Result;

            string settingsHash = this._settingsService.Value.GetSettingsHash();
            string compareHash = this._cache.Value.GetStringAsync(SettingsHashCacheKey).Result ?? string.Empty;

            bool settingsHaveChanged = string.CompareOrdinal(settingsHash, compareHash) != 0;
            if (!settingsHaveChanged)
            {
                if (IvhEnvironment.IsDevelopment)
                {
                    // don't use cached stylesheet
                }
                else
                {
                    if (ThemeCache.SettingsHash == settingsHash &&
                        ThemeCache.Stylesheets.ContainsKey(requestedFile))
                    {
                        return ThemeCache.Stylesheets[requestedFile];
                    }
                }
            }
            
            this._cache.Value.SetStringAsync(SettingsHashCacheKey, settingsHash, CacheExpiration).Wait();

            string stylesheet = this.BuildStylesheet(requestedFile, settings);
            if (stylesheet.IsNullOrEmpty())
            {
                return string.Empty;
            }

            ThemeCache.SettingsHash = settingsHash;
            ThemeCache.Stylesheets[requestedFile] = stylesheet;

            byte[] hash = Sha1Managed.ComputeHash(Encoding.UTF8.GetBytes(stylesheet));
            ThemeHash[requestedFile] = string.Concat(hash.Select(b => b.ToString("x2")));

            return stylesheet;
        }

        private string BuildStylesheet(string requestedFile, IReadOnlyDictionary<string, string> settings)
        {
            ThemeConfiguration themeConfiguration = this.GetThemeConfiguration(requestedFile);
            if (themeConfiguration == null)
            {
                return string.Empty;
            }

            string rawContent = this.GetThemeSettings(requestedFile, settings);
            SassCompiler sassCompiler = new SassCompiler();

            try
            {
                string result = sassCompiler.Compile(rawContent, OutputStyle.Compressed, false, new[] { themeConfiguration.IncludeDirectory });
                return result;
                /*ScssOptions options = new ScssOptions
                {
                    OutputStyle = ScssOutputStyle.Compressed,
                    SourceComments = false,
                    IncludePaths = {themeConfiguration.IncludeDirectory}
                };
                
                ScssResult result = Scss.ConvertToCss(rawContent, options);
                return result.Css;*/
            }
            catch (Exception ex)
            {
                this._logger.Value.Warn(() =>  "Ivh.Provider.Content.ThemeApplicationService.BuildStylesheet failed to compile: {0} {1}", requestedFile, ExceptionHelper.AggregateExceptionToString(ex));

#if DEBUG
                throw;
#else
                return string.Empty;
#endif
            }
        }

        private string GetThemeSettings(string requestedFile, IReadOnlyDictionary<string, string> settings)
        {
            ThemeConfiguration themeConfiguration = this.GetThemeConfiguration(requestedFile);
            if (themeConfiguration == null)
            {
                return string.Empty;
            }
            
            StringBuilder stringBuilder = new StringBuilder();
            string prefix = string.Concat(themeConfiguration.SettingPrefix, ".");

            var themeSettings = settings
                .Where(k => k.Key.StartsWith(prefix) && !k.Key.StartsWith(prefix + "override"))
                .Where(k => !string.IsNullOrWhiteSpace(k.Value))
                .Select(kvp => new
                {
                    Key = kvp.Key.Substring(prefix.Length),
                    kvp.Value,
                    Dependency = kvp.Value.Contains("$") ? 1 : 0
                })
                .OrderBy(x => x.Dependency);

            foreach (var themeSetting in themeSettings)
            {
                stringBuilder.AppendLine($"${themeSetting.Key}: {ReplacementUtility.ReplaceWhiteSpace(themeSetting.Value, string.Empty)};");
            }
            
            stringBuilder.AppendFormat("@import '{0}';", themeConfiguration.BaseFile);

            string overrides;

            
            switch (this._applicationSettingsService.Value.Application.Value)
            {
                case ApplicationEnum.Client:
                    overrides = this.GetOverrides(settings, prefix + "override.client");
                    break;
                case ApplicationEnum.VisitPay:
                    overrides = this.GetOverrides(settings, prefix + "override.patient");
                    break;
                default:
                    overrides = string.Empty;
                    break;
            }

            stringBuilder.AppendLine();
            stringBuilder.Append(overrides);

            return stringBuilder.ToString();
        }

        private ThemeConfiguration GetThemeConfiguration(string requestedFile)
        {
            if (requestedFile.IsNullOrEmpty())
            {
                return null;
            }
            
            string key = requestedFile.Substring(0, requestedFile.LastIndexOf("."));
            ThemeConfiguration themeConfiguration = ThemeConfiguration.ContainsKey(key) ? ThemeConfiguration[key] : null;

            return themeConfiguration;
        }

        private string GetOverrides(IReadOnlyDictionary<string, string> settings, string key)
        {
            return settings.ContainsKey(key) ? settings[key] ?? string.Empty : string.Empty;
        }
    }
}

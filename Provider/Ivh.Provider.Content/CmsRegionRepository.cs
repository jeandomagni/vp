﻿namespace Ivh.Provider.Content
{
    using Common.Data;
    using Common.Data.Connection.Connections;
    using Common.Data.Interfaces;
    using Common.VisitPay.Enums;
    using Domain.Content.Entities;
    using Domain.Content.Interfaces;

    public class CmsRegionRepository : RepositoryBase<CmsRegion, VisitPay>, ICmsRegionRepository
    {
        public CmsRegionRepository(ISessionContext<VisitPay> sessionContext) : base(sessionContext)
        {
        }

        public CmsRegion GetByNameAndType(string cmsRegionName, CmsTypeEnum cmsType)
        {
            return this.Session.QueryOver<CmsRegion>()
                .Where(x => x.CmsRegionName == cmsRegionName && x.CmsType == cmsType)
                .Take(1).SingleOrDefault();
        }

        public int GetNextCmsRegionId()
        {
            return this.Session.QueryOver<CmsRegion>()
                       .OrderBy(x => x.CmsRegionId).Desc
                       .Take(1).SingleOrDefault()
                       .CmsRegionId
                       + 1;
        }
    }
}
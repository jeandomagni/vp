﻿namespace Ivh.Provider.Content
{
    using System;
    using System.Collections.Concurrent;
    using System.Collections.Generic;
    using System.Globalization;
    using System.Net.Http;
    using System.Threading;
    using System.Threading.Tasks;
    using System.Web;
    using Common.Api.Client;
    using Common.Base.Utilities.Helpers;
    using Common.ResiliencePolicies.Interfaces;
    using Common.VisitPay.Enums;
    using Domain.Content.Entities;
    using Domain.Content.Interfaces;
    using Domain.Logging.Interfaces;
    using Domain.Settings.Interfaces;
    using Ivh.Domain.Settings.Enums;

    public class ContentProviderClient : IContentProvider
    {
#if DEBUG
        private const double ApiTimeout = 300d;
#else
        private const double ApiTimeout = 15d;
#endif

        private static readonly object Lock = new object();
        private static HttpClient _httpClient;
        private static string _requestUrl;

        private static string _contentApiUrl;
        private static string _contentApiAppId;
        private static string _contentApiAppKey;
        private static string _applicationName;
        private static ApplicationEnum? _application;
        
        private readonly Lazy<ILoggingProvider> _logger;
        private static Lazy<IHttpRequestPolicy> _requestPolicy;

        public ContentProviderClient(
            Lazy<IApplicationSettingsService> applicationSettingService,
            Lazy<ILoggingProvider> logger,
            Lazy<IHttpRequestPolicy> requestPolicy
            )
        {
            this._logger = logger;

            if(_requestPolicy == null)
            {
                _requestPolicy = requestPolicy;
            }

            _contentApiUrl = string.Concat(applicationSettingService.Value.GetUrl(UrlEnum.ClientDataApi).AbsoluteUri, "api/content/");
            _contentApiAppId = applicationSettingService.Value.ClientDataApiAppId.Value;
            _contentApiAppKey = applicationSettingService.Value.ClientDataApiAppKey.Value;

            if (new WebHelper().IsWebApplication)
            {
                ApplicationEnum application = ApplicationEnum.Unknown;
                if (Enum.TryParse(Ivh.Common.Properties.Settings.Default.Application, out application))
                {
                    _application = application;
                }
            }

            if (_applicationName == null)
            {
                _applicationName = Ivh.Common.Properties.Settings.Default.ApplicationName;
            }

            if (_httpClient == null)
            {
                lock (Lock)
                {
                    if (_httpClient == null)
                    {
                        _httpClient = HttpClientFactory.Create(new ApiHandler(_contentApiAppId, _contentApiAppKey, _applicationName));
                        _httpClient.Timeout = TimeSpan.FromSeconds(ApiTimeout);

                        AppDomain.CurrentDomain.DomainUnload += (e, o) => { _httpClient?.Dispose(); };
                    }
                }
            }
        }

        private static readonly ConcurrentDictionary<ContentMenuEnum, string> GetContentMenuAsyncRequestDictionary =
                            new ConcurrentDictionary<ContentMenuEnum, string>();
        public async Task<ContentMenu> GetContentMenuAsync(ContentMenuEnum contentMenuEnum)
        {
            string RequestString(ContentMenuEnum contentMenu)
            {
                string parameters = $"contentMenuEnum={contentMenu.ToString()}";
                return CreateRequestString("GetContentMenu", parameters);
            }

            return await this.MakeApiCallAsync<ContentMenu, ContentMenuEnum>(contentMenuEnum, GetContentMenuAsyncRequestDictionary, RequestString).ConfigureAwait(false);
        }

        private static readonly ConcurrentDictionary<(CmsRegionEnum, int?, string), string> GetVersionAsyncRequestDictionary =
                            new ConcurrentDictionary<(CmsRegionEnum, int?, string), string>();
        public async Task<CmsVersion> GetVersionAsync(CmsRegionEnum cmsRegionEnum, int? versionId = null)
        {
            string uiLocale = this.GetCurrentUiLocale();
            (CmsRegionEnum cmsRegionEnum, int? versionId, string currentUiLocale) requestParameters = (cmsRegionEnum, versionId, uiLocale);

            string RequestString((CmsRegionEnum cmsRegionEnum, int? versionId, string currentUiLocale) requestTuple)
            {
                string parameters = $"cmsRegion={requestTuple.cmsRegionEnum.ToString()}&locale={UrlEncode(requestTuple.currentUiLocale)}";
                if (requestTuple.versionId.HasValue)
                {
                    parameters = $"{parameters}&cmsVersionId={requestTuple.versionId.ToString()}";
                }

                return CreateRequestString("GetVersion", parameters);
            }

            return await this.MakeApiCallAsync<CmsVersion, (CmsRegionEnum, int?, string)>(requestParameters, GetVersionAsyncRequestDictionary, RequestString).ConfigureAwait(false);
        }


        private static readonly ConcurrentDictionary<(string, CmsTypeEnum, string, string), string> GetVersionByRegionNameAndTypeAsyncRequestDictionary = 
                            new ConcurrentDictionary<(string, CmsTypeEnum, string, string), string>();
        public async Task<CmsVersion> GetVersionByRegionNameAndTypeAsync(string cmsRegionName, CmsTypeEnum cmsType, string defaultLabel)
        {
            string uiLocale = this.GetCurrentUiLocale();
            (string cmsRegionName, CmsTypeEnum cmsType, string defaultLabel, string currentUiLocale) requestParameters = (cmsRegionName, cmsType, defaultLabel, uiLocale);

            string RequestString((string cmsRegionName, CmsTypeEnum cmsType, string defaultLabel, string currentUiLocale) requestTuple)
            {
                string parameters = $"cmsRegionName={UrlEncode(requestTuple.cmsRegionName)}&cmsType={requestTuple.cmsType.ToString()}&defaultLabel={UrlEncode(requestTuple.defaultLabel)}&locale={UrlEncode(requestTuple.currentUiLocale)}";
                return CreateRequestString("GetVersionByRegionNameAndType", parameters);
            }

            return await this.MakeApiCallAsync<CmsVersion, (string, CmsTypeEnum, string, string)>(requestParameters, GetVersionByRegionNameAndTypeAsyncRequestDictionary, RequestString).ConfigureAwait(false);
        }

        private static readonly ConcurrentDictionary<(ApplicationEnum, string, string), string> GetTextRegionAsyncRequestDictionary = 
                            new ConcurrentDictionary<(ApplicationEnum, string, string), string>();
        public async Task<string> GetTextRegionAsync(string textRegion)
        {
            string uiLocale = this.GetCurrentUiLocale();
            ApplicationEnum application = _application ?? ApplicationEnum.Unknown;
            (ApplicationEnum applicationEnum, string textRegion, string currentUiLocale) requestParameters = (application, textRegion, uiLocale);

            string RequestString((ApplicationEnum applicationEnum, string textRegion, string currentUiLocale) requestTuple)
            {
                string parameters = $"textRegion={UrlEncode(requestTuple.textRegion)}&locale={UrlEncode(requestTuple.currentUiLocale)}&application={requestTuple.applicationEnum.ToString()}";
                return CreateRequestString("GetTextRegion", parameters);
            }

            return await this.MakeApiCallAsync<string, (ApplicationEnum applicationEnum, string textRegion, string currentUiLocale)>(requestParameters, GetTextRegionAsyncRequestDictionary, RequestString).ConfigureAwait(false);
        }

        private static readonly ConcurrentDictionary<(CmsRegionEnum, string), string> GetCmsVersionNumbersAsyncRequestDictionary =
                            new ConcurrentDictionary<(CmsRegionEnum, string), string>();
        public async Task<IDictionary<int, int>> GetCmsVersionNumbersAsync(CmsRegionEnum cmsRegion)
        {
            string uiLocale = this.GetCurrentUiLocale();
            (CmsRegionEnum cmsRegionEnum, string currentUiLocale) requestParameters = (cmsRegion, uiLocale);

            string RequestString((CmsRegionEnum cmsRegionEnum, string currentUiLocale) requestTuple)
            {
                string parameters = $"cmsRegion={requestTuple.cmsRegionEnum.ToString()}&locale={UrlEncode(requestTuple.currentUiLocale)}";
                return CreateRequestString("GetCmsVersionNumbers", parameters);
            }

            return await this.MakeApiCallAsync<IDictionary<int, int>, (CmsRegionEnum, string)>(requestParameters, GetCmsVersionNumbersAsyncRequestDictionary, RequestString).ConfigureAwait(false);
        }

        private async Task<TReturn> MakeApiCallAsync<TReturn, TParameters>(TParameters requestParameters, IDictionary<TParameters, string> requestDictionary, Func<TParameters, string> requestStringBuilder) 
            where TParameters : struct
        {
            // try to get the request string from the dictionary cache
            if (requestDictionary.TryGetValue(requestParameters, out string requestString))
            {
                return await this.MakeApiCallAsync<TReturn>(requestString).ConfigureAwait(false);
            }

            // create and cache the request string
            requestString = requestStringBuilder(requestParameters);
            requestDictionary[requestParameters] = requestString;

            return await this.MakeApiCallAsync<TReturn>(requestString).ConfigureAwait(false);
        }

        private static readonly ConcurrentDictionary<string, string> UrlEncodedValues = new ConcurrentDictionary<string, string>();
        private static string UrlEncode(string input)
        {
            if (UrlEncodedValues.TryGetValue(input, out string encoded))
            {
                return encoded;
            }

            string encodedValue = HttpUtility.UrlEncode(input);
            UrlEncodedValues[input] = encodedValue;

            return encodedValue;
        }

        private string GetCurrentUiLocale()
        {
            //Get the current culture
            CultureInfo culture = Thread.CurrentThread.CurrentUICulture;

            return culture.Name;
        }

        private static string CreateRequestString(string action, string parameters) => string.Concat(_contentApiUrl, action, "?", parameters);


        private Task<TReturn> MakeApiCallAsync<TReturn>(string request)
        {
            TReturn result = Task.Run(async () => await _requestPolicy.Value.GetAsync<TReturn>(
                    request,
                    _httpClient).ConfigureAwait(false)).Result;

            return Task.FromResult(result);
        }

        private HttpClient CreateClientDataApiHttpClient()
        {
            HttpClient client = HttpClientFactory.Create(new ApiHandler(_contentApiAppId, _contentApiAppKey, _applicationName));
            client.Timeout = TimeSpan.FromSeconds(ApiTimeout);
            return client;
        }

        public async Task<bool> InvalidateCacheAsync()
        {
            return await _requestPolicy.Value.InvalidateCache();
        }

        #region NotImplemented

        public IDictionary<int, int> ApiGetCmsVersionNumbers(CmsRegionEnum cmsRegionEnum, string locale)
        {
            throw new NotImplementedException();
        }

        public CmsVersion ApiGetVersionByRegionNameAndType(string cmsRegionName, CmsTypeEnum cmsType, string defaultLabel, string locale)
        {
            throw new NotImplementedException();
        }

        public CmsVersion ApiGetVersion(CmsRegionEnum cmsRegionEnum, string locale, int? versionId = null)
        {
            throw new NotImplementedException();
        }

        #endregion NotImplemented
    }
}

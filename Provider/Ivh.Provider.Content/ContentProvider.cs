﻿namespace Ivh.Provider.Content
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Common.Data;
    using Common.Data.Connection.Connections;
    using Common.Data.Interfaces;
    using Common.VisitPay.Constants;
    using Common.VisitPay.Enums;
    using Domain.Content.Entities;
    using Domain.Content.Interfaces;
    using NHibernate;

    public class ContentProvider : IContentProvider
    {
        private readonly ISession _session;
        private readonly Lazy<IContentMenuRepository> _contentMenuRepository;
        private readonly Lazy<ICmsRegionRepository> _cmsRegionRepository;
        private readonly Lazy<ICmsVersionRepository> _cmsVersionRepository;

        public ContentProvider(
            Lazy<IContentMenuRepository> contentMenuRepository,
            Lazy<ICmsRegionRepository> cmsRegionRepository,
            Lazy<ICmsVersionRepository> cmsVersionRepository,
            ISessionContext<VisitPay> sessionContext
            )
        {
            this._session = sessionContext?.Session;

            this._cmsRegionRepository = cmsRegionRepository;
            this._cmsVersionRepository = cmsVersionRepository;
            this._contentMenuRepository = contentMenuRepository;
        }

        public Task<ContentMenu> GetContentMenuAsync(ContentMenuEnum contentMenuEnum)
        {
            ContentMenu contentMenu = this._contentMenuRepository.Value.GetById((int)contentMenuEnum);
            return Task.FromResult(contentMenu);
        }

        public CmsVersion ApiGetVersion(CmsRegionEnum cmsRegionEnum, string locale, int? versionId = null)
        {
            return this._cmsVersionRepository.Value.GetVersion(cmsRegionEnum, locale, versionId);
        }

        public Task<CmsVersion> GetVersionAsync(CmsRegionEnum cmsRegionEnum, int? versionId = null)
        {
            //API should call API specific method
            throw new NotImplementedException();
        }

        public CmsVersion ApiGetVersionByRegionNameAndType(string cmsRegionName, CmsTypeEnum cmsType, string defaultLabel, string locale)
        {
            CmsVersion cmsVersion = this._cmsVersionRepository.Value.GetVersionByRegionNameAndType(cmsRegionName, locale, cmsType);
            if (cmsVersion != null)
            {
                this._session.Evict(cmsVersion);
                return cmsVersion;
            }
            cmsVersion = this.InsertCmsLabel(cmsRegionName, cmsType, defaultLabel);
            if (cmsVersion != null)
            {
                this._session.Evict(cmsVersion);
                return cmsVersion;
            }
            return null;
        }

        public Task<CmsVersion> GetVersionByRegionNameAndTypeAsync(string cmsRegionName, CmsTypeEnum cmsType, string defaultLabel)
        {
            //API should call API specific method
            throw new NotImplementedException();
        }

        public Task<string> GetTextRegionAsync(string textRegion)
        {
            //Don't need to implement this for now since we aren't storing this in the DB currently. Would normally call the repo here.
            throw new NotImplementedException();
        }

        public Task<bool> InvalidateCacheAsync()
        {
            //API does not have a cache
            return Task.FromResult(true);
        }

        public IDictionary<int, int> ApiGetCmsVersionNumbers(CmsRegionEnum cmsRegionEnum, string locale)
        {
            return this._cmsVersionRepository.Value.GetCmsVersionNumbers(cmsRegionEnum, locale);
        }

        public Task<IDictionary<int, int>> GetCmsVersionNumbersAsync(CmsRegionEnum cmsRegionEnum)
        {
            //API should call API specific method
            throw new NotImplementedException();
        }

        private CmsVersion InsertCmsLabel(string cmsRegionName, CmsTypeEnum cmsType, string content)
        {
            int cmsRegionId;
            using (IUnitOfWork uow = new UnitOfWork(this._session))
            {
                // create region
                CmsRegion cmsRegion = this._cmsRegionRepository.Value.GetByNameAndType(cmsRegionName, cmsType);
                if (cmsRegion == null)
                {
                    cmsRegionId = this._cmsRegionRepository.Value.GetNextCmsRegionId();
                    cmsRegion = new CmsRegion
                    {
                        CmsRegionId = cmsRegionId,
                        CmsRegionName = cmsRegionName,
                        CmsType = cmsType
                    };
                    this._cmsRegionRepository.Value.Insert(cmsRegion);
                }
                else
                {
                    cmsRegionId = cmsRegion.CmsRegionId;
                }
                uow.Commit();
                this._session.Evict(cmsRegion);
            }
            using (IUnitOfWork uow = new UnitOfWork(this._session))
            {
                CmsRegion cmsRegion = this._cmsRegionRepository.Value.GetByNameAndType(cmsRegionName, cmsType);
                // create version
                CmsVersion cmsVersion = new CmsVersion
                {
                    CmsRegion = cmsRegion,
                    CmsRegionId = cmsRegionId,
                    ContentBody = content ?? string.Empty,
                    ContentTitle = null,
                    ExpiryDate = null,
                    UpdatedDate = DateTime.UtcNow,
                    UpdatedByUserId = SystemUsers.SystemUserId,
                    VersionNum = 1
                };
                this._cmsVersionRepository.Value.Insert(cmsVersion);
                uow.Commit();

                // detach from session
                // seems like any client setting replacement values are being saved post commit.
                this._session.Evict(cmsVersion);
                return cmsVersion;
            }
        }
    }
}

﻿namespace Ivh.Provider.Content
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Common.Base.Enums;
    using Common.Base.Utilities.Helpers;
    using Common.Cache;
    using Common.Data;
    using Common.Data.Connection.Connections;
    using Common.Data.Interfaces;
    using Common.VisitPay.Enums;
    using Domain.Content.Entities;
    using Domain.Content.Interfaces;
    using NHibernate;
    using NHibernate.SqlCommand;

    public class CmsVersionRepository : RepositoryBase<CmsVersion, VisitPay>, ICmsVersionRepository
    {
        private readonly ICache _cache;

        public CmsVersionRepository(ISessionContext<VisitPay> sessionContext, ICache cache) : base(sessionContext)
        {
            this._cache = cache;
        }

        public CmsVersion GetVersion(CmsRegionEnum cmsRegionEnum, string locale, int? versionId = null)
        {
            string key = versionId.HasValue ? CmsCacheKey.SpecificVersion((int)cmsRegionEnum, versionId.Value, locale) : CmsCacheKey.CurrentVersion((int)cmsRegionEnum, locale);

            CmsVersion cmsVersion = this._cache.GetObjectAsync<CmsVersion>(key).Result;
            if (cmsVersion != null)
            {
                return cmsVersion;
            }

            return this.PersistToCmsCache(this.GetVersion(cmsRegionEnum, null, locale, null, versionId), !versionId.HasValue);
        }

        public CmsVersion GetVersionByRegionNameAndType(string regionName, string locale, CmsTypeEnum cmsType)
        {
            CmsVersion cmsVersion = this._cache.GetObjectAsync<CmsVersion>(CmsCacheKey.CurrentVersionByNameType(regionName, cmsType, locale)).Result;
            if (cmsVersion != null)
            {
                return cmsVersion;
            }

            return this.PersistToCmsCache(this.GetVersion(null, regionName, locale, cmsType), true);
        }

        public IDictionary<int, int> GetCmsVersionNumbers(CmsRegionEnum cmsRegionEnum, string locale)
        {
            int cmsRegionId = (int)cmsRegionEnum;

            IList<object[]> results = this.Session.QueryOver<CmsVersion>()
                .Where(x => x.CmsRegionId == cmsRegionId)
                .SelectList(list => list
                    .Select(x => x.CmsVersionId)
                    .Select(x => x.VersionNum)
                ).List<object[]>();

            return results.ToDictionary(x => Convert.ToInt32(x[0]), x => Convert.ToInt32(x[1]));
        }

        private CmsVersion GetVersion(CmsRegionEnum? cmsRegionEnum, string regionName, string locale, CmsTypeEnum? cmsType, int? versionId = null)
        {
            CmsVersion versionAlias = null;
            CmsRegion regionAlias = null;

            IQueryOver<CmsVersion, CmsVersion> query = this.Session.QueryOver<CmsVersion>(() => versionAlias)
                .JoinAlias(a => a.CmsRegion, () => regionAlias, JoinType.InnerJoin)
                .Where(x => x.Locale == locale);

            query.If(cmsRegionEnum.HasValue, q =>
            {
                int cmsRegionId = (int)cmsRegionEnum.GetValueOrDefault(0);
                q.Where(x => x.CmsRegionId == cmsRegionId);
            });

            if (!string.IsNullOrEmpty(regionName))
            {
                query = query.Where(() => regionAlias.CmsRegionName == regionName);
            }

            if (cmsType.HasValue)
            {
                query = query.Where(() => regionAlias.CmsType == cmsType);
            }

            if (versionId.HasValue)
            {
                int cmsVersionId = versionId.Value;
                query = query.Where(x => x.CmsVersionId == cmsVersionId);
            }

            return query.OrderBy(x => x.VersionNum).Desc.Take(1).SingleOrDefault();
        }

        private CmsVersion PersistToCmsCache(CmsVersion cmsVersion, bool isCurrent)
        {
            if (cmsVersion == null)
            {
                return null;
            }

            Task.Run(() =>
            {
                if (cmsVersion.CmsRegion.CmsType == CmsTypeEnum.EmailContent)
                {
                    cmsVersion.ContentTitle = ReplacementUtility.ReplaceWhiteSpace(cmsVersion.ContentTitle, " ");
                    cmsVersion.ContentBody = ReplacementUtility.ReplaceWhiteSpace(cmsVersion.ContentBody, " ");
                }

                if (isCurrent)
                {
                    this._cache.SetObjectAsync(CmsCacheKey.CurrentVersion(cmsVersion.CmsRegionId, cmsVersion.Locale), cmsVersion, 60 * 10);
                }

                this._cache.SetObjectAsync(CmsCacheKey.SpecificVersion(cmsVersion.CmsRegionId, cmsVersion.CmsVersionId, cmsVersion.Locale), cmsVersion, 60 * 10);

                this._cache.SetObjectAsync(CmsCacheKey.CurrentVersionByNameType(cmsVersion.CmsRegion.CmsRegionName, cmsVersion.CmsRegion.CmsType, cmsVersion.Locale), cmsVersion, 60 * 10);
            });

            return cmsVersion;
        }

        private static class CmsCacheKey
        {
            public static string CurrentVersion(int cmsRegionId, string locale)
            {
                return $"{nameof(CmsRegionRepository)}::{nameof(CurrentVersion)}:{cmsRegionId}:{locale}";
            }

            public static string CurrentVersionByNameType(string cmsRegionName, CmsTypeEnum cmsType, string locale)
            {
                return $"{nameof(CmsRegionRepository)}::{nameof(CurrentVersionByNameType)}:{cmsRegionName.ToUpper()}.{(int)cmsType}:{locale}";
            }

            public static string SpecificVersion(int cmsRegionId, int cmsVersionId, string locale)
            {
                return $"{nameof(CmsRegionRepository)}::{nameof(SpecificVersion)}:{cmsRegionId}.{cmsVersionId}:{locale}";
            }
        }
    }
}
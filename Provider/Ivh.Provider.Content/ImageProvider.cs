﻿namespace Ivh.Provider.Content
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Net.Http;
    using System.Threading.Tasks;
    using Domain.Content.Entities;
    using Domain.Content.Interfaces;
    using Domain.Settings.Enums;
    using Domain.Settings.Interfaces;
    using MimeTypes;

    public class ImageProvider : IImageProvider
    {
        private static readonly object Lock = new object();
        private readonly Lazy<IApplicationSettingsService> _applicationSettingsService;

        public ImageProvider(Lazy<IApplicationSettingsService> applicationSettingsService)
        {
            this._applicationSettingsService = applicationSettingsService;
        }

        //TODO: need a resilience policy for this
        public async Task<Image> GetImageAsync(string fileName)
        {
            fileName = fileName.ToLower().Replace("client", this._applicationSettingsService.Value.Client.Value.ToString());
            string contentApiUrl = this._applicationSettingsService.Value.GetUrl(UrlEnum.ClientDataApi).AbsoluteUri;
            if (fileName.StartsWith("/") && contentApiUrl.EndsWith("/"))
            {
                fileName = fileName.Substring(1);
            }
            string request = string.Format(contentApiUrl + fileName);
            HttpClient client = HttpClientFactory.Create();
            HttpResponseMessage response = Task.Run(async () => await client.GetAsync(request)).Result;
            if (response.IsSuccessStatusCode)
            {
                  return new Image()
                    {
                        ImageBytes = await response.Content.ReadAsByteArrayAsync(),
                        ContentType = MimeTypeMap.GetMimeType(Path.GetExtension(fileName)),
                    };

            }
            return null;
        }

        public async Task<bool> InvalidateCacheAsync(string localCacheDirectory)
        {
            return await Task.Run(() =>
            {
                throw new NotImplementedException();
#pragma warning disable 162
                return default(bool);
#pragma warning restore 162
                //await Task.Run(() =>
                //{
                //    lock (Lock)
                //    {
                //        List<string> cachedFiles = Directory.GetFiles(localCacheDirectory).ToList();
                //        foreach (var cachedFile in cachedFiles)
                //        {
                //            File.Delete(cachedFile);
                //        }
                //        _intialized = true;
                //    }
                //});

                //return _intialized;
            });
        }
    }
}

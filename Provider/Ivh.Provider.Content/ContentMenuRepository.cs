﻿namespace Ivh.Provider.Content
{
    using System.Threading.Tasks;
    using Common.Cache;
    using Common.Data;
    using Common.Data.Connection.Connections;
    using Common.Data.Interfaces;
    using Domain.Content.Entities;
    using Domain.Content.Interfaces;
    using NHibernate;

    public class ContentMenuRepository : RepositoryBase<ContentMenu, VisitPay>, IContentMenuRepository
    {
        private readonly ICache _cache;

        public ContentMenuRepository(ICache cache, ISessionContext<VisitPay> sessionContext)
            : base(sessionContext)
        {
            this._cache = cache;
        }

        public override ContentMenu GetById(int id)
        {
            ContentMenu contentMenu = this._cache.GetObjectAsync<ContentMenu>(ContentMenuCacheKey.ContentMenu(id)).Result;
            if (contentMenu != null)
            {
                return contentMenu;
            }

            return this.PersistToContentMenuCache(base.GetById(id));
        }

        private ContentMenu PersistToContentMenuCache(ContentMenu contentMenu)
        {
            if (contentMenu == null)
            {
                return null;
            }

            Task.Run(() =>
            {
                this._cache.SetObjectAsync(ContentMenuCacheKey.ContentMenu(contentMenu.ContentMenuId), contentMenu, 60 * 10);
            });

            return contentMenu;
        }

        private static class ContentMenuCacheKey
        {
            public static string ContentMenu(int contentMenuId)
            {
                return string.Format("ContentMenuRepository::{0}", contentMenuId);
            }
        }
    }
}
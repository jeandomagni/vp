﻿namespace Ivh.Provider.GuestPay
{
    using System.Collections.Generic;
    using Common.Data;
    using Common.Data.Connection.Connections;
    using Common.Data.Interfaces;
    using Domain.GuestPay.Entities;
    using Domain.GuestPay.Interfaces;
    using Domain.Settings.Entities;
    using Domain.Settings.Interfaces;
    using NHibernate;
    using NHibernate.Criterion;

    public class PaymentUnitAuthenticationRepository : RepositoryBase<PaymentUnitAuthentication, GuestPay>, IPaymentUnitAuthenticationRepository
    {
        private readonly Client _client;
        public PaymentUnitAuthenticationRepository(ISessionContext<GuestPay> sessionContext, IClientService clientService) : base(sessionContext)
        {
            this._client = clientService.GetClient();
        }

        public IList<PaymentUnitAuthentication> AuthenticatePaymentUnit(PaymentUnitAuthentication paymentUnitAuthentication, IList<string> authenticationFields)
        {
            ICriteria criteria = this.Session.CreateCriteria<PaymentUnitAuthentication>();
            foreach (string authenticationField in authenticationFields)
            {
                criteria.Add(Restrictions.Eq(authenticationField, paymentUnitAuthentication.GetValueByName(authenticationField)));
            }
            return criteria.List<PaymentUnitAuthentication>();
        }

        public PaymentUnitAuthentication GetPaymentUnitAuthentication(int paymentUnitAuthenticationId, string paymentUnitSourceSystemKey)
        {
            ICriteria criteria = this.Session.CreateCriteria<PaymentUnitAuthentication>()
                .Add(Restrictions.Eq("PaymentUnitAuthenticationId", paymentUnitAuthenticationId))
                .Add(Restrictions.Eq("PaymentUnitSourceSystemKey", paymentUnitSourceSystemKey));
            return criteria.UniqueResult<PaymentUnitAuthentication>();
        }

        public IList<PaymentUnitAuthentication> GetPaymentUnitAuthentications(string paymentUnitSourceSystemKey)
        {
            IQueryOver<PaymentUnitAuthentication> query = this.Session.QueryOver<PaymentUnitAuthentication>()
                .Where(x => x.PaymentUnitSourceSystemKey == paymentUnitSourceSystemKey);

            IList<PaymentUnitAuthentication> allPaymentUnitAuthentications = query.List();

            return allPaymentUnitAuthentications;
        }
    }
}
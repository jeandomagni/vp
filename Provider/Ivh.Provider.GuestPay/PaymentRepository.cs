﻿namespace Ivh.Provider.GuestPay
{
    using System.Collections.Generic;
    using System.Linq;
    using Common.Base.Enums;
    using Common.Data;
    using Common.Data.Connection.Connections;
    using Common.Data.Interfaces;
    using Common.VisitPay.Enums;
    using Domain.GuestPay.Entities;
    using Domain.GuestPay.Interfaces;
    using NHibernate;

    public class PaymentRepository : RepositoryBase<Payment, GuestPay>, IPaymentRepository
    {
        public PaymentRepository(ISessionContext<GuestPay> sessionContext)
            : base(sessionContext)
        {
        }

        public Payment GetPayment(int paymentId, string enteredAuthenticationKey)
        {
            IQueryOver<Payment, Payment> query = this.Session.QueryOver<Payment>()
                .Where(x => x.PaymentId == paymentId && x.EnteredAuthenticationKey == enteredAuthenticationKey);

            return query.Take(1).SingleOrDefault();
        }


        public Payment GetPayment(string paymentProcessorSourceKey)
        {
            IQueryOver<Payment> query = this.Session.QueryOver<Payment>()
                .JoinQueryOver(x => x.PaymentProcessorResponse)
                .Where(x => x.PaymentProcessorSourceKey == paymentProcessorSourceKey);

            return query.Take(1).SingleOrDefault<Payment>();
        }

        public IList<Payment> GetPaymentByTransactionId(string transactionId, IList<PaymentStatusEnum> paymentStatuses = null)
        {
            IQueryOver<Payment> query = this.Session.QueryOver<Payment>()
                .JoinQueryOver(x => x.PaymentProcessorResponse)
                .Where(x => x.PaymentProcessorSourceKey == transactionId
                            && (x.PaymentSystemType == PaymentSystemTypeEnum.GuestPay || x.PaymentSystemType == PaymentSystemTypeEnum.PaymentApi));

            IList<Payment> allPayments = query.List();
            if (paymentStatuses != null && paymentStatuses.Any())
                allPayments = allPayments.Where(payment => paymentStatuses.Contains(payment.PaymentStatus)).ToList();

            return allPayments;
        }
    }
}
﻿namespace Ivh.Provider.FinanceManagement.Payment
{
    using System.Collections.Generic;
    using Common.Data;
    using Common.Data.Connection.Connections;
    using Common.Data.Interfaces;
    using Common.VisitPay.Enums;
    using Domain.FinanceManagement.Payment.Entities;
    using Domain.FinanceManagement.Payment.Interfaces;
    using NHibernate;

    public class PaymentActionHistoryRepository : RepositoryBase<PaymentActionHistory, VisitPay>, IPaymentActionHistoryRepository
    {
        public PaymentActionHistoryRepository(ISessionContext<VisitPay> sessionContext, IStatelessSessionContext<VisitPay> statelessSessionContext) : base(sessionContext, statelessSessionContext)
        {
        }
        
        public IList<PaymentActionHistory> GetPaymentActionsByType(int vpGuarantorId, int actionVisitPayUserId, PaymentActionTypeEnum paymentActionType)
        {
            IQueryOver<PaymentActionHistory, PaymentActionHistory> query = this.StatelessSession.QueryOver<PaymentActionHistory>()
                .Where(x => x.PaymentActionType == paymentActionType)
                .Where(x => x.VpGuarantorId == vpGuarantorId)
                .Where(x => x.ActionVisitPayUser.VisitPayUserId == actionVisitPayUserId);

            return query.List();
        }
    }
}
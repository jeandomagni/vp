﻿namespace Ivh.Provider.FinanceManagement.Payment
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Common.Data;
    using Common.Data.Connection.Connections;
    using Common.Data.Interfaces;
    using Common.VisitPay.Enums;
    using Domain.FinanceManagement.Payment.Entities;
    using Domain.FinanceManagement.Payment.Interfaces;
    using NHibernate;
    using NHibernate.Criterion;
    using NHibernate.Transform;

    public class PaymentAllocationRepository : RepositoryBase<PaymentAllocation, VisitPay>, IPaymentAllocationRepository
    {
        public PaymentAllocationRepository(ISessionContext<VisitPay> sessionContext) : base(sessionContext)
        {
        }
        
        public IList<PaymentAllocation> GetAllPaymentAllocationsForVisits(IList<int> visitIds, PaymentBatchStatusEnum? paymentBatchStatus)
        {
            IList<PaymentAllocation> results = this.GetPaymentAllocationsForVisitsQueryOver(visitIds, paymentBatchStatus)
                .List<PaymentAllocation>();

            return results;
        }

        public IList<PaymentAllocation> GetNonInterestPaymentAllocationsForVisits(IList<int> visitIds, PaymentBatchStatusEnum? paymentBatchStatus)
        {
            IList<PaymentAllocation> results = this.GetPaymentAllocationsForVisitsQueryOver(visitIds, paymentBatchStatus)
                .Where(x => x.PaymentAllocationType != PaymentAllocationTypeEnum.VisitInterest)
                .List<PaymentAllocation>();

            return results;
        }

        public IList<PaymentAllocationVisitAmountResult> GetAllPaymentAllocationAmountsForVisits(IList<int> visitIds, PaymentBatchStatusEnum paymentBatchStatus)
        {
            IQueryOver<PaymentAllocation, PaymentAllocation> query = this.GetPaymentAllocationsForVisitsQueryOver(visitIds, paymentBatchStatus);

            return this.ToResult(query);
        }

        public IList<PaymentAllocationVisitAmountResult> GetNonInterestPaymentAllocationAmountsForVisits(IList<int> visitIds, PaymentBatchStatusEnum paymentBatchStatus, DateTime? startDate = null, DateTime? endDate = null)
        {
            IQueryOver<PaymentAllocation, PaymentAllocation> query = this.GetPaymentAllocationsForVisitsQueryOver(visitIds, paymentBatchStatus)
                .Where(x => x.PaymentAllocationType != PaymentAllocationTypeEnum.VisitInterest);

            if(startDate.HasValue)
            {
                query = query.Where(x => x.InsertDate >= startDate);
            }
            if(endDate.HasValue)
            {
                query = query.Where(x => x.InsertDate <= endDate);
            }

            return this.ToResult(query);
        }
        
        public void RemovePaymentAllocations(Payment payment)
        {
            using (IUnitOfWork uow = new UnitOfWork(this.Session))
            {
                foreach (PaymentAllocation paymentAllocation in payment.PaymentAllocations)
                {
                    paymentAllocation.Payment = null;
                    this.Delete(paymentAllocation);
                }
                payment.PaymentAllocations.Clear();
                uow.Commit();
            }
        }
        
        private IQueryOver<PaymentAllocation, PaymentAllocation> GetPaymentAllocationsForVisitsQueryOver(IEnumerable<int> visitIds, PaymentBatchStatusEnum? paymentBatchStatus)
        {
            IQueryOver<PaymentAllocation, PaymentAllocation> queryover = this.Session.QueryOver<PaymentAllocation>()
                .Where(Restrictions.In(Projections.Property<PaymentAllocation>(x => x.PaymentVisit.VisitId), visitIds.ToArray()));
                
            if (paymentBatchStatus.HasValue)
            {
                queryover = queryover.Where(x => x.PaymentBatchStatus == paymentBatchStatus);
            }

            return queryover;
        }

        private IList<PaymentAllocationVisitAmountResult> ToResult(IQueryOver<PaymentAllocation, PaymentAllocation> query)
        {
            PaymentAllocationVisitAmountResult result = null;

            return query.SelectList(x => x
                    .Select(z => z.PaymentVisit.VisitId).WithAlias(() => result.VisitId)
                    .Select(z => z.ActualAmount).WithAlias(() => result.ActualAmount)
                    .Select(z => z.InsertDate).WithAlias(() => result.InsertDate)
                    .Select(z => z.PaymentAllocationType).WithAlias(() => result.PaymentAllocationType)
                    .Select(z => z.PaymentAllocationId).WithAlias(()=> result.PaymentAllocationId)
                )
                .TransformUsing(Transformers.AliasToBean<PaymentAllocationVisitAmountResult>())
                .List<PaymentAllocationVisitAmountResult>();
        }
    }
}

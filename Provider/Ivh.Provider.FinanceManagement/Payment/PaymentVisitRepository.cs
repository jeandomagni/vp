﻿namespace Ivh.Provider.FinanceManagement.Payment
{
    using System.Collections.Generic;
    using System.Linq;
    using Common.Base.Enums;
    using Common.Base.Utilities.Extensions;
    using Common.Data;
    using Common.Data.Connection.Connections;
    using Common.Data.Interfaces;
    using Common.VisitPay.Enums;
    using Domain.FinanceManagement.FinancePlan.Entities;
    using Domain.FinanceManagement.Payment.Entities;
    using Domain.FinanceManagement.Payment.Interfaces;
    using Domain.FinanceManagement.Statement.Entities;
    using NHibernate;
    using NHibernate.Criterion;
    using NHibernate.SqlCommand;
    using NHibernate.Transform;

    public class PaymentVisitRepository : RepositoryBase<PaymentVisit, VisitPay>, IPaymentVisitRepository
    {
        public PaymentVisitRepository(
            ISessionContext<VisitPay> sessionContext)
            : base(sessionContext)
        {
        }

        public IList<PaymentVisit> GetCurrentStatementPaymentVisits(int vpGuarantorId, bool excludeFinancePlanVisits)
        {
            //Todo: https://ivinci.atlassian.net/browse/VP-158
            //Is there a way to remove this?
            //References to VpStatement here probably isnt ideal, same with FP.
            //Note: Please review PaymentAllocationServiceProductTestsBase PaymentAllocationServiceMockBuilder setup when refactoring

            DetachedCriteria subQuery = DetachedCriteria.For<VpStatement>("s")
                    .Add(Restrictions.Eq("s.VpGuarantorId", vpGuarantorId))
                    .AddOrder(new Order("s.StatementDate", false))
                    .SetMaxResults(1)
                    .SetProjection(Projections.Property("VpStatementId"));

            ICriteria criteria = this.Session.CreateCriteria<VpStatementVisit>("s1")
                .Add(Subqueries.PropertyEq("s1.VpStatement.VpStatementId", subQuery))
                .SetProjection(Projections.Property<VpStatementVisit>(x => x.Visit.VisitId));

            HashSet<int> visitIds = criteria.List<int>().Distinct().ToHashSet();
            if (excludeFinancePlanVisits)
            {
                //Get all active FP statuses
                List<int> listOfActive = EnumExt.GetAllInCategory<FinancePlanStatusEnum>(FinancePlanStatusEnumCategory.Active).Select(x => (int)x).ToList();
                //Only select active financeplans
                DetachedCriteria fpSubQuery = DetachedCriteria.For<FinancePlan>("f")
                    .Add(Restrictions.Eq("f.VpGuarantor.VpGuarantorId", vpGuarantorId))
                    .Add(Restrictions.In("f.CurrentFinancePlanStatus.FinancePlanStatusId", listOfActive))
                    .SetProjection(Projections.Property("FinancePlanId"));
                //Only select visitIds on active finance plans
                ICriteria fpCriteria = this.Session.CreateCriteria<FinancePlanVisit>("fpv")
                    .Add(Subqueries.PropertyIn("fpv.FinancePlan.FinancePlanId", fpSubQuery))
                    .Add(Restrictions.In(Projections.Property<FinancePlanVisit>(x => x.VisitId), visitIds.ToList()))
                    .SetProjection(Projections.Property<FinancePlanVisit>(x => x.VisitId));

                //Remove visits on active finance plans
                IList<int> visitsOnFinancePlan = fpCriteria.List<int>();
                foreach (int i in visitsOnFinancePlan)
                {
                    visitIds.Remove(i);
                }
            }
            //Get the PaymentVisits
            return this.GetPaymentVisits(visitIds.ToList());
        }
        
        public IList<PaymentVisit> GetPaymentVisitsByVpGuarantorId(int vpGuarantorId)
        {
            IList<PaymentVisit> result = this.Session
                .CreateCriteria(typeof(PaymentVisit))
                .Add(Restrictions.Eq("VpGuarantorId", vpGuarantorId))
                .List<PaymentVisit>();
            return result;
        }

        public IList<PaymentVisit> GetPaymentVisits(IList<int> visitIds)
        {
            IList<PaymentVisit> result = this.Session
                .CreateCriteria(typeof(PaymentVisit))
                .Add(Restrictions.In("VisitId", visitIds.ToList()))
                .List<PaymentVisit>();
            return result;
        }
    }
}
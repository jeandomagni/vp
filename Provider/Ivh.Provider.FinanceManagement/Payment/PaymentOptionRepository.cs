﻿namespace Ivh.Provider.FinanceManagement.Payment
{
    using System.Linq;
    using Common.Data;
    using Common.Data.Connection.Connections;
    using Common.Data.Interfaces;
    using Domain.FinanceManagement.Payment.Entities;
    using Domain.FinanceManagement.Payment.Interfaces;
    using NHibernate;
    using NHibernate.Linq;

    public class PaymentOptionRepository : RepositoryBase<PaymentOption, VisitPay>, IPaymentOptionRepository
    {
        public PaymentOptionRepository(ISessionContext<VisitPay> sessionContext)
            : base(sessionContext)
        {
            
        }

        public override IQueryable<PaymentOption> GetQueryable()
        {
            return base.GetQueryable().WithOptions(x =>
            {
                x.SetCacheable(true);
                x.SetCacheMode(CacheMode.Normal);
            });
        }
    }
}
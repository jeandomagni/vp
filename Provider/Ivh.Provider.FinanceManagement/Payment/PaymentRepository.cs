﻿namespace Ivh.Provider.FinanceManagement.Payment
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Common.Base.Utilities.Extensions;
    using Common.Base.Utilities.Helpers;
    using Common.Data;
    using Common.Data.Connection.Connections;
    using Common.Data.Interfaces;
    using Common.VisitPay.Enums;
    using Domain.FinanceManagement.Entities;
    using Domain.FinanceManagement.Payment.Entities;
    using Domain.FinanceManagement.Payment.Interfaces;
    using Domain.FinanceManagement.ValueTypes;
    using NHibernate;
    using NHibernate.Criterion;
    using NHibernate.SqlCommand;
    using NHibernate.Transform;

    public class PaymentRepository : RepositoryBase<Payment, VisitPay>, IPaymentRepository
    {
        public PaymentRepository(ISessionContext<VisitPay> sessionContext, IStatelessSessionContext<VisitPay> statelessSessionContext) : base(sessionContext, statelessSessionContext)
        {
        }

        private IQueryOver<Payment, Payment> QueryPayment(int? vpGuarantorId)
        {
            IQueryOver<Payment, Payment> query = this.Session.QueryOver<Payment>();

            if (vpGuarantorId.HasValue)
            {
                query = query.WhereGuarantorEq(vpGuarantorId.Value);
            }

            return query;
        }

        #region payment statuses

        public IList<PaymentStatus> GetPaymentStatuses()
        {
            return this.Session.QueryOver<PaymentStatus>()
                .Cacheable()
                .CacheMode(CacheMode.Normal)
                .List();
        }

        #endregion

        #region generic payment queries

        public Payment GetPayment(int vpGuarantorId, int paymentId)
        {
            IQueryOver<Payment, Payment> query = this.QueryPayment(vpGuarantorId).WherePaymentIdEq(paymentId);

            return query.SingleOrDefault();
        }

        public IReadOnlyList<Payment> GetPayments(int vpGuarantorId, IEnumerable<int> paymentIds)
        {
            IQueryOver<Payment, Payment> query = this.QueryPayment(vpGuarantorId).WherePaymentIdIn(paymentIds.ToArray());

            return (IReadOnlyList<Payment>)query.List();
        }

        public IReadOnlyList<Payment> GetPayments(int vpGuarantorId, IEnumerable<PaymentStatusEnum> paymentStatuses)
        {
            IQueryOver<Payment, Payment> query = this.QueryPayment(vpGuarantorId).WherePaymentStatusIn(paymentStatuses.ToArray()); ;

            return (IReadOnlyList<Payment>)query.List<Payment>();
        }

        public IReadOnlyList<Payment> GetPayments(int vpGuarantorId, IEnumerable<PaymentTypeEnum> paymentTypes)
        {
            IQueryOver<Payment, Payment> query = this.QueryPayment(vpGuarantorId).WherePaymentTypeIn(paymentTypes.ToArray());

            return (IReadOnlyList<Payment>)query.List<Payment>();
        }

        #endregion

        #region scheduled payment queries

        private static readonly PaymentStatusEnum[] PendingPaymentStatuses = {
            PaymentStatusEnum.ActivePending,
            PaymentStatusEnum.ActiveGatewayError
        };

        private static readonly PaymentStatusEnum[] FailedTransmissionStatuses = {
            PaymentStatusEnum.ActiveGatewayError
        };

        private static readonly PaymentTypeEnum[] ManualScheduledPaymentTypes = EnumHelper<PaymentTypeEnum>.GetValuesIntersect(PaymentTypeEnumCategory.Manual, PaymentTypeEnumCategory.Scheduled).ToArray();
        private static readonly PaymentTypeEnum[] RecurringPaymentTypes = EnumHelper<PaymentTypeEnum>.GetValues(PaymentTypeEnumCategory.Recurring).ToArray();

        private IQueryOver<Payment, Payment> QueryScheduledPayments(int? vpGuarantorId)
        {
            IQueryOver<Payment, Payment> query = this.QueryPayment(vpGuarantorId)
                    .WherePaymentStatusIn(PendingPaymentStatuses)
                    .WherePaymentTypeIn(ManualScheduledPaymentTypes);

            return query;
        }

        private IQueryOver<Payment, Payment> QueryUnsentRecurringPayments(int? vpGuarantorId)
        {
            IQueryOver<Payment, Payment> query = this.QueryPayment(vpGuarantorId)
            .WherePaymentStatusIn(FailedTransmissionStatuses)
            .WherePaymentTypeIn(RecurringPaymentTypes);

            return query;
        }

        public IReadOnlyList<ScheduledPaymentResult> GetScheduledPaymentResults(int vpGuarantorId, int? paymentId = null)
        {
            Payment p = null;
            PaymentMethod pm = null;
            ScheduledPaymentResult result = null;

            QueryOver<PaymentScheduledAmount, PaymentScheduledAmount> sumQuery = QueryOver.Of<PaymentScheduledAmount>()
                .Where(x => x.Payment.PaymentId == p.PaymentId)
                .Select(Projections.Sum(Projections.Property<PaymentScheduledAmount>(x => x.ScheduledAmount)));

            IQueryOver<Payment, Payment> query = this.StatelessSession.QueryOver(() => p)
                .JoinAlias(x => x.PaymentMethod, () => pm, JoinType.LeftOuterJoin)
                .WhereGuarantorEq(vpGuarantorId)
                .WherePaymentTypeIn(ManualScheduledPaymentTypes)
                .WherePaymentStatusIn(PendingPaymentStatuses);

            if (paymentId.HasValue)
            {
                query = query.WherePaymentIdEq(paymentId.Value);
            }

            query = query.SelectList(builder => builder
                    .Select(() => p.Guarantor.VpGuarantorId).WithAlias(() => result.MadeForVpGuarantorId)
                    .Select(() => p.PaymentId).WithAlias(() => result.PaymentId)
                    .Select(() => p.PaymentType).WithAlias(() => result.PaymentType)
                    .Select(() => p.ScheduledPaymentDate).WithAlias(() => result.ScheduledPaymentDate)
                    .Select(Projections.SubQuery(sumQuery)).WithAlias(() => result.ScheduledPaymentAmount)
                    .Select(() => pm.VpGuarantorId).WithAlias(() => result.MadeByVpGuarantorId)
                    .Select(() => pm.PaymentMethodId).WithAlias(() => result.PaymentMethodId))
                .TransformUsing(Transformers.AliasToBean(typeof(ScheduledPaymentResult)));

            return (IReadOnlyList<ScheduledPaymentResult>)query.List<ScheduledPaymentResult>();
        }

        public IReadOnlyList<Payment> GetUnsentRecurringPayments(int? vpGuarantorId, DateTime? scheduledPaymentDate)
        {
            IQueryOver<Payment, Payment> query = this.QueryUnsentRecurringPayments(vpGuarantorId);

            if (scheduledPaymentDate.HasValue)
            {
                query = query.WhereScheduledPaymentDateLessThanOrEq(scheduledPaymentDate.Value.Date);
            }

            return (IReadOnlyList<Payment>)query.List();
        }

        public IReadOnlyList<Payment> GetScheduledPayments(int? vpGuarantorId, DateTime? scheduledPaymentDate)
        {
            IQueryOver<Payment, Payment> query = this.QueryScheduledPayments(vpGuarantorId);

            if (scheduledPaymentDate.HasValue)
            {
                query = query.WhereScheduledPaymentDateLessThanOrEq(scheduledPaymentDate.Value.Date);
            }

            return (IReadOnlyList<Payment>)query.List();
        }

        public IReadOnlyList<Payment> GetScheduledPaymentsWithExactDate(DateTime scheduledPaymentDate)
        {
            IQueryOver<Payment, Payment> query = this.QueryScheduledPayments(null)
                .WhereScheduledPaymentDateEq(scheduledPaymentDate.Date);

            return (IReadOnlyList<Payment>)query.List();
        }

        public IList<int> GetScheduledPaymentGuarantorIds(DateTime scheduledPaymentDate)
        {
            IQueryOver<Payment, Payment> queryScheduledManualPaymentGuarantors = this.QueryScheduledPayments(null)
                .WhereScheduledPaymentDateLessThanOrEq(scheduledPaymentDate.Date)
                .Select(x => x.Guarantor.VpGuarantorId);

            IQueryOver<Payment, Payment> queryUnsentRecurringPaymentGuarantors = this.QueryUnsentRecurringPayments(null)
                .WhereScheduledPaymentDateLessThanOrEq(scheduledPaymentDate.Date)
                .Select(x => x.Guarantor.VpGuarantorId);

            List<int> scheduledManualPaymentsGuarantors = queryScheduledManualPaymentGuarantors.List<int>().Distinct().ToList();
            List<int> unsentRecurringPaymentGuarantors = queryUnsentRecurringPaymentGuarantors.List<int>().Distinct().ToList();
            List<int> allPaymentGuarantors = scheduledManualPaymentsGuarantors.Union(unsentRecurringPaymentGuarantors).ToList();

            return allPaymentGuarantors;
        }

        public int GetScheduledPaymentsCountByGuarantorPaymentMethod(int vpGuarantorId)
        {
            PaymentMethod pm = null;

            IQueryOver<Payment, Payment> query = this.Session.QueryOver<Payment>()
                .JoinAlias(x => x.PaymentMethod, () => pm, JoinType.InnerJoin)
                .Where(() => pm.VpGuarantorId == vpGuarantorId)
                .WherePaymentTypeIn(ManualScheduledPaymentTypes)
                .WherePaymentStatusIn(PendingPaymentStatuses);

            return query.RowCount();
        }

        #endregion

        public bool HasLinkFailurePayments(int vpGuarantorId)
        {
            int count = this.QueryPayment(vpGuarantorId)
                .WherePaymentStatusIn(PaymentStatusEnum.ActivePendingLinkFailure.ToArrayOfOne())
                .RowCount();

            return count > 0;
        }

        public IList<Payment> GetPaymentsFromPaymentAllocationIds(IList<int> allocationIds)
        {
            PaymentAllocation pa = null;

            IQueryOver<Payment, Payment> query = this.QueryPayment(null)
                .JoinAlias(x => x.PaymentAllocations, () => pa)
                .Where(Restrictions.In(Projections.Property(() => pa.PaymentAllocationId), allocationIds.ToArray()));

            return query.List();
        }

        public IList<Payment> GetPaymentsFromTransactionId(string transactionId, IList<PaymentStatusEnum> paymentStatuses = null)
        {
            PaymentPaymentProcessorResponse paymentPaymentProcessorResponsepaymentAlias = null;

            IQueryOver<PaymentProcessorResponse, PaymentProcessorResponse> query = this.Session.QueryOver<PaymentProcessorResponse>()
                .JoinAlias(x => x.PaymentPaymentProcessorResponses, () => paymentPaymentProcessorResponsepaymentAlias)
                .Where(x => x.PaymentProcessorSourceKey == transactionId && x.PaymentSystemType == PaymentSystemTypeEnum.VisitPay);

            IList<PaymentProcessorResponse> responses = query.List();
            IList<PaymentPaymentProcessorResponse> ppr = responses.SelectMany(x => x.PaymentPaymentProcessorResponses).ToList();
            IList<Payment> allPayments = ppr.Select(x => x.Payment).ToList();

            if (paymentStatuses != null && paymentStatuses.Any())
            {
                allPayments = allPayments.Where(payment => paymentStatuses.Contains(payment.PaymentStatus)).ToList();
            }

            IList<Payment> groupedPayments = allPayments
                .GroupBy(payment => payment.PaymentId)
                .Select(group => group.FirstOrDefault())
                .Where(payment => payment != null)
                .ToList();

            return groupedPayments;
        }

        public IReadOnlyList<Payment> GetPaymentsFromVisit(int visitId)
        {
            PaymentAllocation pa = null;
            PaymentVisit pv = null;

            IQueryOver<Payment, Payment> query = this.QueryPayment(null)
                .JoinAlias(x => x.PaymentAllocations, () => pa)
                .JoinAlias(() => pa.PaymentVisit, () => pv)
                .Where(() => pv.VisitId == visitId);

            IReadOnlyList<Payment> payments = (IReadOnlyList<Payment>)query.List<Payment>();
            return payments;
        }

        public IList<Payment> GetPaymentsSubmittedOnDateInStatus(DateTime dateTime, IList<PaymentStatusEnum> statuses)
        {
            IQueryOver<Payment, Payment> query = this.QueryPayment(null)
                .WherePaymentStatusIn(statuses.ToArray())
                .Where(x => x.ActualPaymentDate <= dateTime);

            return query.List<Payment>();
        }

        public IList<int> GetPaymentMethodIdsAssociatedWithPayments(int vpGuarantorId, IList<PaymentStatusEnum> paymentStatuses)
        {
            Payment p = null;
            PaymentMethod pm = null;

            IList<int> paymentMethodIds = this.Session.QueryOver(() => p)
                .JoinAlias(() => p.PaymentMethod, () => pm)
                .Where(() => p.PaymentMethod != null)
                .Where(() => pm.VpGuarantorId == vpGuarantorId)
                .WherePaymentStatusIn(paymentStatuses.ToArray())
                .Select(x => pm.PaymentMethodId)
                .List<int>();

            return paymentMethodIds;
        }

        #region payment history grid (returns list of payment processor responses)

        public IReadOnlyList<PaymentProcessorResponse> GetPaymentHistory(PaymentProcessorResponseFilter filter)
        {
            List<PaymentProcessorResponse> paymentProcessorResponseList = new List<PaymentProcessorResponse>();

            if (!filter.VpGuarantorId.Any() && !filter.MadeByVpGuarantorId.HasValue)
            {
                return new List<PaymentProcessorResponse>();
            }

            IList<DetachedCriteria> subQueryPaymentResponseList = this.GetPaymentHistoryGuarantorSubQueries(filter);

            foreach (DetachedCriteria subQuery in subQueryPaymentResponseList)
            {
                ICriteria criteria = this.Session.CreateCriteria<PaymentProcessorResponse>("r");
                criteria.Add(Subqueries.PropertyIn("PaymentProcessorResponseId", subQuery));

                FilterPaymentResponses(criteria, filter);
                IList<PaymentProcessorResponse> filteredPaymentResponseList = criteria.List<PaymentProcessorResponse>();

                IList<PaymentProcessorResponse> postFilterPaymentResponseList = PostFilterPaymentResponses(filteredPaymentResponseList, filter);
                paymentProcessorResponseList.AddRange(postFilterPaymentResponseList);
            }

            List<PaymentProcessorResponse> uniquePaymentProcessorResponseList = paymentProcessorResponseList.GroupBy(x => x.PaymentProcessorResponseId).Select(y => y.First()).ToList();

            return uniquePaymentProcessorResponseList;
        }

        /// <summary>
        /// Get subqueries to filter by guarantor (do not combine subqueries in the same query)
        /// </summary>
        /// <param name="filter"></param>
        /// <returns></returns>
        private IList<DetachedCriteria> GetPaymentHistoryGuarantorSubQueries(PaymentProcessorResponseFilter filter)
        {
            List<DetachedCriteria> detachedCriteriaList = new List<DetachedCriteria>();
            filter.VpGuarantorId = filter.VpGuarantorId ?? new List<int>();

            if (filter.VpGuarantorId.IsNullOrEmpty() && !filter.MadeByVpGuarantorId.HasValue)
            {
                throw new ArgumentException(nameof(filter));
            }

            DetachedCriteria subQueryPaymentResponseIds = DetachedCriteria.For<PaymentPaymentProcessorResponse>("ppp")
                .CreateCriteria("ppp.Payment", "p", JoinType.InnerJoin)
                .CreateCriteria("p.Guarantor", "g", JoinType.InnerJoin);

            if (filter.VpGuarantorId.Any())
            {
                subQueryPaymentResponseIds.Add(Restrictions.In("g.VpGuarantorId", filter.VpGuarantorId.ToList()));
            }

            if (filter.MadeByVpGuarantorId.HasValue)
            {
                subQueryPaymentResponseIds.CreateCriteria("p.PaymentMethod", "pm", JoinType.InnerJoin);
                subQueryPaymentResponseIds.Add(Restrictions.Eq("pm.VpGuarantorId", filter.MadeByVpGuarantorId.Value));
            }

            subQueryPaymentResponseIds.CreateCriteria("ppp.PaymentProcessorResponse", "pr", JoinType.InnerJoin).SetProjection(Projections.Property("pr.PaymentProcessorResponseId"));
            detachedCriteriaList.Add(subQueryPaymentResponseIds);

            return detachedCriteriaList;
        }

        private static void FilterPaymentResponses(ICriteria criteria, PaymentProcessorResponseFilter filter)
        {
            // filter
            if (filter.InsertDateRange.HasValue)
            {
                criteria.Add(Restrictions.Gt("r.InsertDate", DateTime.UtcNow.Date.AddDays(-filter.InsertDateRange.Value)));
            }

            if (filter.InsertDateRangeFrom.HasValue)
            {
                criteria.Add(Restrictions.Gt("r.InsertDate", filter.InsertDateRangeFrom.Value.Date));
            }

            if (filter.InsertDateRangeTo.HasValue)
            {
                criteria.Add(Restrictions.Lt("r.InsertDate", filter.InsertDateRangeTo.Value.Date.AddDays(1)));
            }

            if (filter.PaymentMethodId.HasValue)
            {
                criteria.Add(Restrictions.Eq("r.PaymentMethod.PaymentMethodId", filter.PaymentMethodId.Value));
            }

            if (!string.IsNullOrEmpty(filter.PaymentProcessorSourceKey) && !string.IsNullOrWhiteSpace(filter.PaymentProcessorSourceKey))
            {
                criteria.Add(Restrictions.Eq("r.PaymentProcessorSourceKey", filter.PaymentProcessorSourceKey));
            }
        }

        private static IList<PaymentProcessorResponse> PostFilterPaymentResponses(IList<PaymentProcessorResponse> responses, PaymentProcessorResponseFilter filter)
        {
            #region filters

            if (!string.IsNullOrEmpty(filter.PaymentProcessorResponseId) && !string.IsNullOrWhiteSpace(filter.PaymentProcessorResponseId))
            {
                int paymentProcessorResponseId;
                int.TryParse(filter.PaymentProcessorResponseId, out paymentProcessorResponseId);

                responses = responses.Where(x => x.PaymentProcessorResponseId == paymentProcessorResponseId).ToList();
            }

            if (filter.PaymentStatus.IsNotNullOrEmpty())
            {
                responses = responses.Where(x => x.Payments.Any(z => filter.PaymentStatus.Contains(z.PaymentStatus))).ToList();
            }

            if (filter.PaymentType.IsNotNullOrEmpty())
            {
                responses = responses.Where(x => x.Payments.Any(z => filter.PaymentType.Contains(z.PaymentType))).ToList();
            }

            #endregion

            #region special cases

            // remove when processor status is failed, but payment has closed (after successful resubmit...resubmit gives a new processor response, causing duplicate-ish entries in the list)
            IList<int> excludeFailedClosed = responses
                .Where(x => x.PaymentProcessorResponseStatus != PaymentProcessorResponseStatusEnum.Accepted && x.PaymentProcessorResponseStatus != PaymentProcessorResponseStatusEnum.Approved && x.Payments.Any(z => z.PaymentStatus == PaymentStatusEnum.ClosedPaid))
                .Select(x => x.PaymentProcessorResponseId)
                .ToList();

            responses = responses.Where(x => !excludeFailedClosed.Contains(x.PaymentProcessorResponseId)).ToList();

            //special case for ACH Returns: this filters out all ACH returns where the resubmit payment is still in PendingAchApproval
            IEnumerable<int> include = responses
                .SelectMany(x => x.Payments)
                .Where(x => x.PaymentMethod != null && x.PaymentMethod.IsAchType)
                .Where(x => x.PaymentStatus == PaymentStatusEnum.ActivePendingGuarantorAction || x.PaymentStatus == PaymentStatusEnum.ActivePendingACHApproval || x.PaymentStatus == PaymentStatusEnum.ClosedPaid)
                .Select(x => new { x.PaymentId, PaymentProcessorResponseId = x.PaymentPaymentProcessorResponses.Max(y => y.PaymentProcessorResponse.PaymentProcessorResponseId) })
                .Select(x => x.PaymentProcessorResponseId)
                .ToHashSet<int>();

            IEnumerable<int> excludeAchPending = responses.SelectMany(x => x.Payments)
                .Where(x => x.PaymentMethod != null && x.PaymentMethod.IsAchType)
                .Where(x => x.PaymentStatus == PaymentStatusEnum.ActivePendingGuarantorAction || x.PaymentStatus == PaymentStatusEnum.ActivePendingACHApproval || x.PaymentStatus == PaymentStatusEnum.ClosedPaid)
                .SelectMany(x => x.PaymentPaymentProcessorResponses)
                .Select(x => x.PaymentProcessorResponse.PaymentProcessorResponseId)
                .Where(x => !include.Contains(x))
                .ToHashSet<int>();

            responses = responses.Where(x => !excludeAchPending.Contains(x.PaymentProcessorResponseId)).ToList();

            #endregion

            #region recurring

            // get distinct payments for recurring finance plans
            List<Payment> payments = responses.SelectMany(response => response.Payments)
                .Where(payment => payment.IsRecurringPayment() && payment.PaymentStatus != PaymentStatusEnum.ClosedFailed)
                .GroupBy(payment => payment.PaymentId)
                .Select(payment => payment.First())
                .ToList();

            // get each distinct last response
            List<PaymentProcessorResponse> lastResponses = payments
                .Select(payment => payment.PaymentProcessorResponses.OrderByDescending(response => response.InsertDate).First())
                .GroupBy(response => response.PaymentProcessorResponseId)
                .Select(group => group.First())
                .ToList();

            // remove responses for recurring payments that have been added to another months payment
            List<int> a = payments.SelectMany(z => z.PaymentProcessorResponses.Select(y => y.PaymentProcessorResponseId)).ToList();
            responses = responses.Where(x => !a.Contains(x.PaymentProcessorResponseId)).ToList();

            // add distinct responses
            responses.AddRange(lastResponses);

            #endregion

            return responses;
        }

        #endregion
    }

    internal static class PaymentQueryOverExtensions
    {
        public static IQueryOver<Payment, Payment> WherePaymentStatusIn(this IQueryOver<Payment, Payment> query, PaymentStatusEnum[] paymentStatuses)
        {
            return query.Where(Restrictions.In(Projections.Property<Payment>(x => x.CurrentPaymentStatus), paymentStatuses));
        }

        public static IQueryOver<Payment, Payment> WherePaymentTypeIn(this IQueryOver<Payment, Payment> query, PaymentTypeEnum[] paymentTypes)
        {
            return query.Where(Restrictions.In(Projections.Property<Payment>(x => x.PaymentType), paymentTypes));
        }

        public static IQueryOver<Payment, Payment> WhereScheduledPaymentDateEq(this IQueryOver<Payment, Payment> query, DateTime scheduledPaymentDate)
        {
            return query.Where(x => x.ScheduledPaymentDate.Date == scheduledPaymentDate.Date);
        }

        public static IQueryOver<Payment, Payment> WhereScheduledPaymentDateLessThanOrEq(this IQueryOver<Payment, Payment> query, DateTime scheduledPaymentDate)
        {
            return query.Where(x => x.ScheduledPaymentDate <= scheduledPaymentDate.Date);
        }

        public static IQueryOver<Payment, Payment> WhereGuarantorEq(this IQueryOver<Payment, Payment> query, int vpGuarantorId)
        {
            return query.Where(x => x.Guarantor.VpGuarantorId == vpGuarantorId);
        }

        public static IQueryOver<Payment, Payment> WherePaymentIdEq(this IQueryOver<Payment, Payment> query, int paymentId)
        {
            return query.Where(x => x.PaymentId == paymentId);
        }

        public static IQueryOver<Payment, Payment> WherePaymentIdIn(this IQueryOver<Payment, Payment> query, params int[] paymentIds)
        {
            return query.Where(Restrictions.In(Projections.Property<Payment>(x => x.PaymentId), paymentIds));
        }
    }
}
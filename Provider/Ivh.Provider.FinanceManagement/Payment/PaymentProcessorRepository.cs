namespace Ivh.Provider.FinanceManagement
{
    using System;
    using Common.Data;
    using Common.Data.Connection.Connections;
    using Common.Data.Interfaces;
    using Domain.FinanceManagement.Payment.Entities;
    using Domain.FinanceManagement.Payment.Interfaces;
    using NHibernate;

    public class PaymentProcessorRepository : RepositoryBase<PaymentProcessor, VisitPay>, IPaymentProcessorRepository
    {
        public PaymentProcessorRepository(ISessionContext<VisitPay> sessionContext) : base(sessionContext)
        {
        }

        public PaymentProcessor GetByName(string name)
        {
            return this.Session.QueryOver<PaymentProcessor>()
                .Where(pp => pp.PaymentProcessorName == name)
                .Take(1).SingleOrDefault();
        }
    }
}
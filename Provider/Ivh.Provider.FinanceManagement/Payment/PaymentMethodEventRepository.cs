﻿namespace Ivh.Provider.FinanceManagement
{
    using Common.Base.Enums;
    using Common.Data;
    using Common.Data.Connection.Connections;
    using Common.Data.Interfaces;
    using Common.VisitPay.Enums;
    using Domain.FinanceManagement.Entities;
    using Domain.FinanceManagement.Interfaces;
    using NHibernate;

    public class PaymentMethodEventRepository : RepositoryBase<PaymentMethodEvent, VisitPay>, IPaymentMethodEventRepository
    {
        public PaymentMethodEventRepository(ISessionContext<VisitPay> sessionContext)
            : base(sessionContext)
        {
        }

        public PaymentMethodEvent GetMostRecentPaymentMethodEvent(int vpGuarantorId, PaymentMethodStatusEnum paymentMethodStatus)
        {
            return this.Session.QueryOver<PaymentMethodEvent>()
                .Where(x => x.PaymentMethodStatus == paymentMethodStatus &&
                            x.VpGuarantorId == vpGuarantorId)
                .OrderBy(x => x.InsertDate).Desc
                .Take(1)
                .SingleOrDefault();
        }
    }
}
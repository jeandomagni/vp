﻿namespace Ivh.Provider.FinanceManagement.Payment
{
    using System.Linq;
    using Common.Data;
    using Common.Data.Connection.Connections;
    using Common.Data.Interfaces;
    using Domain.FinanceManagement.Payment.Entities;
    using Domain.FinanceManagement.Payment.Interfaces;
    using NHibernate;
    using NHibernate.Linq;

    public class PaymentMenuRepository : RepositoryBase<PaymentMenu, VisitPay>, IPaymentMenuRepository
    {
        public PaymentMenuRepository(ISessionContext<VisitPay> sessionContext)
            : base(sessionContext)
        {
        }

        public override IQueryable<PaymentMenu> GetQueryable()
        {
            return base.GetQueryable().WithOptions(x =>
            {
                x.SetCacheable(true);
                x.SetCacheMode(CacheMode.Normal);
            });
        }
    }
}
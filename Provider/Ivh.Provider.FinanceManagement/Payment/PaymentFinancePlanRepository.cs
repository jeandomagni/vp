﻿namespace Ivh.Provider.FinanceManagement.Payment
{
    using System.Collections.Generic;
    using System.Linq;
    using Common.Base.Enums;
    using Common.Base.Utilities.Extensions;
    using Common.Data;
    using Common.Data.Connection.Connections;
    using Common.Data.Interfaces;
    using Common.VisitPay.Enums;
    using Domain.FinanceManagement.FinancePlan.Entities;
    using Domain.FinanceManagement.Payment.Entities;
    using Domain.FinanceManagement.Payment.Interfaces;
    using NHibernate;
    using NHibernate.Criterion;

    public class PaymentFinancePlanRepository : RepositoryBase<PaymentFinancePlan, VisitPay>, IPaymentFinancePlanRepository
    {
        public PaymentFinancePlanRepository(
            ISessionContext<VisitPay> sessionContext)
            : base(sessionContext)
        {

        }
        
        public IList<PaymentFinancePlan> GetPaymentFinancePlans(IList<int> financePlanIds)
        {
            IList<PaymentFinancePlan> result = this.Session
                .CreateCriteria(typeof(PaymentFinancePlan))
                .Add(Restrictions.In(Projections.Property<PaymentFinancePlan>(x => x.FinancePlanId), financePlanIds.ToList()))
                .List<PaymentFinancePlan>();

            return result;
        } 
        
        public IList<PaymentFinancePlan> GetActivePaymentFinancePlansFromVisits(IList<PaymentVisit> visits)
        {
            if(visits.IsNullOrEmpty())
            {
                return new List<PaymentFinancePlan>();
            }
            
            FinancePlan financePlanAlias = null;
            PaymentFinancePlan paymentFinancePlanAlias = null;

            QueryOver<FinancePlanVisit, FinancePlan> idSubQuery = this.GetIdSubquery(visits.Select(x => x.VisitId).ToList())
                .Select(Projections.Distinct(Projections.Property(() => financePlanAlias.FinancePlanId)));

            IList<PaymentFinancePlan> result = this.Session.QueryOver(() => paymentFinancePlanAlias)
                .WithSubquery.WhereProperty(() => paymentFinancePlanAlias.FinancePlanId)
                .In(idSubQuery)
                .List<PaymentFinancePlan>();

            return result;
        }

        public IList<PaymentFinancePlanVisit> GetFinancePlanVisitsOnActiveFiancePlans(IList<PaymentVisit> visits)
        {
            if(visits.IsNullOrEmpty())
            {
                return new List<PaymentFinancePlanVisit>();
            }
            
            FinancePlanVisit financePlanVisitAlias = null;
            PaymentFinancePlanVisit paymentFinancePlanVisitAlias = null;

            QueryOver<FinancePlanVisit, FinancePlan> idSubQuery = this.GetIdSubquery(visits.Select(x => x.VisitId).ToList())
                .Select(Projections.Distinct(Projections.Property(() => financePlanVisitAlias.FinancePlanVisitId)));

            IList<PaymentFinancePlanVisit> result = this.Session.QueryOver(() => paymentFinancePlanVisitAlias)
                .WithSubquery.WhereProperty(() => paymentFinancePlanVisitAlias.FinancePlanVisitId)
                .In(idSubQuery)
                .List<PaymentFinancePlanVisit>();
        
            return result;
        }

        private QueryOver<FinancePlanVisit, FinancePlan> GetIdSubquery(List<int> visitIds)
        {
            FinancePlan financePlanAlias = null;
            FinancePlanVisit financePlanVisitAlias = null;

            List<int> activeFinancePlanStatusIds = EnumExt.GetAllInCategory<FinancePlanStatusEnum>(FinancePlanStatusEnumCategory.Active).Select(x => (int)x).ToList();
            
            return QueryOver.Of(() => financePlanVisitAlias)
                .JoinQueryOver(fpv => fpv.FinancePlan, () => financePlanAlias)
                .Where(Restrictions.In(Projections.Property(() => financePlanAlias.CurrentFinancePlanStatus.FinancePlanStatusId), activeFinancePlanStatusIds))
                .Where(Restrictions.IsNull(Projections.Property(() => financePlanVisitAlias.HardRemoveDate)))
                .Where(Restrictions.In(Projections.Property(() => financePlanVisitAlias.VisitId), visitIds));
        }
    }
}
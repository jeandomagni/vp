﻿namespace Ivh.Provider.FinanceManagement.Payment
{
    using Ivh.Common.Data;
    using Ivh.Common.Data.Connection.Connections;
    using Ivh.Common.Data.Interfaces;
    using Ivh.Domain.FinanceManagement.Payment.Entities;
    using Ivh.Domain.FinanceManagement.Payment.Interfaces;
    using NHibernate;
    using NHibernate.Criterion;
    using System.Collections.Generic;
    using System.Linq;

    public class PaymentImportRepository : RepositoryBase<PaymentImport, VisitPay>, IPaymentImportRepository
    {
        public PaymentImportRepository(ISessionContext<VisitPay> sessionContext) : base(sessionContext)
        {
        }

        public IList<string> GetAllPaymentImportBatchNumbers()
        {
            ICriteria criteria = this.Session.CreateCriteria<PaymentImport>("p");
            criteria.SetProjection(Projections.Distinct(Projections.Property("p.BatchNumber")));
            IList<string> batchNumbers = criteria.List<string>().OrderBy(b => b).ToList();
            return batchNumbers;
        }

        public IList<PaymentImport> GetPaymentImports(PaymentImportFilter filter)
        {
            ICriteria criteria = this.GetPaymentImportCriteria(filter);
            return criteria.List<PaymentImport>();
        }

        private ICriteria GetPaymentImportCriteria(PaymentImportFilter filter)
        {
            ICriteria criteria = this.Session.CreateCriteria<PaymentImport>("p");

            if (filter.DateRangeFrom.HasValue)
            {
                criteria.Add(Restrictions.Ge("p.BatchCreditDate", filter.DateRangeFrom.Value.Date));
            }

            if (filter.DateRangeTo.HasValue)
            {
                criteria.Add(Restrictions.Le("p.BatchCreditDate", filter.DateRangeTo.Value.Date));
            }

            if (filter.PaymentImportState.HasValue)
            {
                criteria.Add(Restrictions.Eq("p.PaymentImportStateEnum", filter.PaymentImportState.Value));
            }

            if (!string.IsNullOrEmpty(filter.BatchNumber))
            {
                criteria.Add(Restrictions.Eq("p.BatchNumber", filter.BatchNumber));
            }

            return criteria;
        }
    }
}

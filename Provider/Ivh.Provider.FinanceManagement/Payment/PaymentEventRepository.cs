﻿namespace
    Ivh.Provider.FinanceManagement.Payment
{
    using System.Collections.Generic;
    using Common.Data;
    using Common.Data.Connection.Connections;
    using Common.Data.Interfaces;
    using Domain.FinanceManagement.Payment.Entities;
    using Domain.FinanceManagement.Payment.Interfaces;
    using NHibernate;
    using NHibernate.Criterion;

    public class PaymentEventRepository : RepositoryBase<PaymentEvent, VisitPay>, IPaymentEventRepository
    {
        public PaymentEventRepository(ISessionContext<VisitPay> sessionContext)
            : base(sessionContext)
        {
        }

        public IReadOnlyList<PaymentEvent> GetPaymentEvents(List<int> paymentIds, int VpGuarantorId)
        {
            ICriteria criteria = this.Session.CreateCriteria<PaymentEvent>("e");
            criteria.Add(Restrictions.Eq("e.VpGuarantorId", VpGuarantorId));
            criteria.Add(Restrictions.In("e.Payment.PaymentId", paymentIds));

            return (IReadOnlyList<PaymentEvent>)criteria.List<PaymentEvent>();
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ivh.Provider.FinanceManagement.Payment
{
    using System.Linq;
    using Common.Data;
    using Common.Data.Connection.Connections;
    using Common.Data.Interfaces;
    using Common.VisitPay.Enums;
    using Domain.FinanceManagement.Payment.Entities;
    using Domain.FinanceManagement.Payment.Interfaces;
    using NHibernate;
    using NHibernate.Linq;
    using Ivh.Common.Base.Enums;

    public class BalanceTransferStatusMerchantAccountRepository : RepositoryBase<BalanceTransferStatusMerchantAccount, VisitPay>, IBalanceTransferStatusMerchantAccountRepository
    {
        public BalanceTransferStatusMerchantAccountRepository(ISessionContext<VisitPay> sessionContext)
            : base(sessionContext)
        {
        }

        public override IQueryable<BalanceTransferStatusMerchantAccount> GetQueryable()
        {
            return base.GetQueryable().WithOptions(x =>
            {
                x.SetCacheable(true);
                x.SetCacheMode(CacheMode.Normal);
            });
        }

        public int GetBalanceTransferMerchantAccountId()
        {
            int btMerchantAccountId = this.GetQueryable()
                .Where(x => x.BalanceTransferStatus.BalanceTransferStatusId == (int)BalanceTransferStatusEnum.ActiveBalanceTransfer)
                .Select(x => x.MerchantAccount.MerchantAccountId).FirstOrDefault();
            return btMerchantAccountId;
        }
    }
}

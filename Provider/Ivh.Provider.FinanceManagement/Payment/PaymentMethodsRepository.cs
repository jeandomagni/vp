﻿namespace Ivh.Provider.FinanceManagement.Payment
{
    using System.Collections.Generic;
    using System.Linq;
    using Common.Data;
    using Common.Data.Connection.Connections;
    using Common.Data.Interfaces;
    using Domain.FinanceManagement.Entities;
    using Domain.FinanceManagement.Interfaces;
    using Domain.Guarantor.Entities;
    using NHibernate;
    using NHibernate.Criterion;

    public class PaymentMethodsRepository : RepositoryBase<PaymentMethod, VisitPay>, IPaymentMethodsRepository
    {
        public PaymentMethodsRepository(ISessionContext<VisitPay> sessionContext, IStatelessSessionContext<VisitPay> statelessSessionContext) : base(sessionContext, statelessSessionContext)
        {
        }
        
        public PaymentMethod GetByIdStateless(int paymentMethodId)
        {
            IQueryOver<PaymentMethod> query = this.StatelessSession.QueryOver<PaymentMethod>()
                .Where(x => x.PaymentMethodId == paymentMethodId)
                .Take(1);

            return query.SingleOrDefault();
        }

        public int GetPaymentMethodsCount(int guarantorId)
        {
            ICriteria criteria = this.QueryPaymentMethodsForGuarantor(guarantorId, false);
            criteria.SetProjection(Projections.RowCount());

            return criteria.UniqueResult<int>();
        }

        public IList<PaymentMethod> GetPaymentMethods(int guarantorId, bool includingInactive = false)
        {
            return this.QueryPaymentMethodsForGuarantor(guarantorId, includingInactive).List<PaymentMethod>();
        }

        public PaymentMethod GetPrimaryPaymentMethodStateless(int vpGuarantorId)
        {
            PaymentMethod paymentMethod = this.StatelessSession.QueryOver<PaymentMethod>()
                .GetPrimaryPaymentMethod(vpGuarantorId)
                .Take(1)
                .SingleOrDefault();

            return paymentMethod;
        }

        public PaymentMethod GetPrimaryPaymentMethod(int vpGuarantorId)
        {
            PaymentMethod paymentMethod = this.Session.QueryOver<PaymentMethod>()
                .GetPrimaryPaymentMethod(vpGuarantorId)
                .Take(1)
                .SingleOrDefault();

            return paymentMethod;
        }

        public IList<PaymentMethod> GetExpiringPrimaryPaymentMethods(string expDate)
        {
            // todo: queryover
            DetachedCriteria subQuery = DetachedCriteria.For<Guarantor>("g")
                .Add(Restrictions.IsNull("AccountClosedDate"))
                .SetProjection(Projections.Property("VpGuarantorId"));

            ICriteria criteria = this.Session.CreateCriteria<PaymentMethod>("pm")
                .Add(Restrictions.Eq("IsPrimary", true))
                .Add(Restrictions.Eq("IsActive", true))
                .Add(Restrictions.Eq("ExpDate", expDate))
                .Add(Subqueries.PropertyIn("VpGuarantorId", subQuery))
                .Add(Restrictions.IsNull("ExpiringOn"));

            IList<PaymentMethod> list = criteria.List<PaymentMethod>();
            
            List<PaymentMethod> paymentMethods = new List<PaymentMethod>();
            paymentMethods.AddRange(list);

            return paymentMethods;
        }
        
        public PaymentMethod GetByBillingId(string billingId)
        {
            IQueryOver<PaymentMethod> query = this.Session.QueryOver<PaymentMethod>()
                .Where(x => x.BillingId == billingId)
                .Take(1);

            return query.List().FirstOrDefault();
        }

        private ICriteria QueryPaymentMethodsForGuarantor(int guarantorId, bool includingInactive)
        {
            ICriteria criteria = this.Session.CreateCriteria(typeof(PaymentMethod));
            criteria.Add(Restrictions.Eq("VpGuarantorId", guarantorId));

            if (!includingInactive)
            {
                criteria.Add(Restrictions.Eq("IsActive", true));
            }

            return criteria;
        }
    }

    internal static class PaymentMethodQueryOverExtensions
    {
        public static IQueryOver<PaymentMethod, PaymentMethod> GetPrimaryPaymentMethod(this IQueryOver<PaymentMethod, PaymentMethod> query, int vpGuarantorId)
        {
            return query.Where(x => x.VpGuarantorId == vpGuarantorId &&
                            x.IsPrimary &&
                            x.IsActive);
        }
    }
}

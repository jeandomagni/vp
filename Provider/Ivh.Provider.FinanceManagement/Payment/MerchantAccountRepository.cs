﻿namespace Ivh.Provider.FinanceManagement.Payment
{
    using System.Linq;
    using Common.Data;
    using Common.Data.Connection.Connections;
    using Common.Data.Interfaces;
    using Domain.FinanceManagement.Payment.Entities;
    using Domain.FinanceManagement.Payment.Interfaces;
    using NHibernate;
    using NHibernate.Linq;

    public class MerchantAccountRepository : 
        RepositoryBase<MerchantAccount, VisitPay>,
        IMerchantAccountRepository
    {
        public MerchantAccountRepository(ISessionContext<VisitPay> sessionContext)
            : base(sessionContext)
        {
        }

        public IQueryable<MerchantAccount> GetAll(bool includeMasterAccount = false)
        {
            return base.GetQueryable().WithOptions(x =>
                {
                    x.SetCacheable(true);
                    x.SetCacheMode(CacheMode.Normal);
                })
                .Where(x => includeMasterAccount || x.MasterMerchantAccount != null);
        }
    }
}

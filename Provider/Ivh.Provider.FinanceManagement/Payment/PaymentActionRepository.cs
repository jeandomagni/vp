﻿namespace Ivh.Provider.FinanceManagement.Payment
{
    using Common.Data;
    using Common.Data.Connection.Connections;
    using Common.Data.Interfaces;
    using Domain.FinanceManagement.Payment.Entities;
    using Domain.FinanceManagement.Payment.Interfaces;
    using NHibernate;

    public class PaymentActionReasonRepository : RepositoryBase<PaymentActionReason, VisitPay>, IPaymentActionReasonRepository
    {
        public PaymentActionReasonRepository(ISessionContext<VisitPay> sessionContext) : base(sessionContext)
        {
        }
    }
}
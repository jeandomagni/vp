﻿
namespace Ivh.Provider.FinanceManagement.Payment
{
    using Common.Data;
    using Common.Data.Connection.Connections;
    using Common.Data.Interfaces;
    using Domain.FinanceManagement.Entities;
    using Domain.FinanceManagement.Interfaces;
    using NHibernate;

    public class PaymentMethodAccountTypeRepository : RepositoryBase<PaymentMethodAccountType, VisitPay>, IPaymentMethodAccountTypeRepository
    {
        public PaymentMethodAccountTypeRepository(ISessionContext<VisitPay> sessionContext) : base(sessionContext)
        {

        }
    }
}

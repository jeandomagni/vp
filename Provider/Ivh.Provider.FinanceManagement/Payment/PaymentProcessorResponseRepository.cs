﻿namespace Ivh.Provider.FinanceManagement.Payment
{
    using System.Collections.Generic;
    using System.Linq;
    using Common.Data;
    using Common.Data.Connection.Connections;
    using Common.Data.Interfaces;
    using Domain.FinanceManagement.Payment.Entities;
    using Domain.FinanceManagement.Payment.Interfaces;
    using Magnum.Extensions;
    using NHibernate;
    using NHibernate.Criterion;

    public class PaymentProcessorResponseRepository : RepositoryBase<PaymentProcessorResponse, VisitPay>, IPaymentProcessorResponseRepository
    {
        public PaymentProcessorResponseRepository(ISessionContext<VisitPay> sessionContext)
            : base(sessionContext)
        {
        }

        public PaymentProcessorResponse GetPaymentProcessorResponse(int paymentProcessorResponseId)
        {
            IQueryOver<PaymentProcessorResponse> queryOver = this.Session.QueryOver<PaymentProcessorResponse>()
                .Where(x => x.PaymentProcessorResponseId == paymentProcessorResponseId);

            return queryOver.Take(1).SingleOrDefault<PaymentProcessorResponse>();
        }


        public IList<PaymentProcessorResponse> GetPaymentProcessorResponses(IList<int> paymentProcessorResponseIds)
        {
            ICriteria criteria = this.Session.CreateCriteria<PaymentProcessorResponse>("p")
                .Add(Restrictions.In("p.PaymentProcessorResponseId", paymentProcessorResponseIds.ToList()));

            return criteria.List<PaymentProcessorResponse>();
        }

        public PaymentProcessorResponse GetPaymentProcessorResponse(string paymentProcessorSourceKey)
        {
            IQueryOver<PaymentProcessorResponse> queryOver = this.Session.QueryOver<PaymentProcessorResponse>()
                .Where(x => x.PaymentProcessorSourceKey == paymentProcessorSourceKey);

            return queryOver.Take(1).SingleOrDefault<PaymentProcessorResponse>();
        }
    }
}
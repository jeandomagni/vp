﻿namespace Ivh.Provider.FinanceManagement.ServiceGroup
{
    using System.Linq;
    using Common.Data;
    using Common.Data.Connection.Connections;
    using Common.Data.Interfaces;
    using Domain.FinanceManagement.ServiceGroup.Entities;
    using Domain.FinanceManagement.ServiceGroup.Interfaces;
    using NHibernate;
    using NHibernate.Linq;

    public class ServiceGroupRepository : 
        RepositoryBase<ServiceGroup, VisitPay>, 
        IServiceGroupRepository
    {
        public ServiceGroupRepository(ISessionContext<VisitPay> sessionContext) 
            : base(sessionContext)
        {
        }

        public override IQueryable<ServiceGroup> GetQueryable()
        {
            return base.GetQueryable().Cacheable().CacheMode(CacheMode.Normal);
        }
    }
}
﻿namespace Ivh.Provider.FinanceManagement.ServiceGroup
{
    using System.Linq;
    using Common.Data;
    using Common.Data.Connection.Connections;
    using Common.Data.Interfaces;
    using Domain.FinanceManagement.Payment.Entities;
    using Domain.FinanceManagement.ServiceGroup.Entities;
    using Domain.FinanceManagement.ServiceGroup.Interfaces;
    using NHibernate;
    using NHibernate.Linq;

    public class ServiceGroupMerchantAccountRepository :
        RepositoryBase<ServiceGroupMerchantAccount, VisitPay>,
        IServiceGroupMerchantAccountRepository
    {
        public ServiceGroupMerchantAccountRepository(ISessionContext<VisitPay> sessionContext)
            : base(sessionContext)
        {
        }

        public int? GetMerchantAccountId(int? serviceGroupId)
        {
            int? merchantAccountId = this.GetQueryableMerchantAccoutByServiceGroupId(serviceGroupId).Select(x => x.MerchantAccountId).FirstOrDefault();
            return merchantAccountId;
        }

        public MerchantAccount GetMerchantAccoutByServiceGroupId(int? serviceGroupId)
        {
            return this.GetQueryableMerchantAccoutByServiceGroupId(serviceGroupId).FirstOrDefault();
        }

        private IQueryable<MerchantAccount> GetQueryableMerchantAccoutByServiceGroupId(int? serviceGroupId)
        {
            return this.GetQueryable()
                .Where(x => x.ServiceGroupId == serviceGroupId)
                .Select(x => x.MerchantAccount)
                .WithOptions(x => 
                {
                    x.SetCacheable(true);
                    x.SetCacheMode(CacheMode.Normal);
                });
        }
    }
}
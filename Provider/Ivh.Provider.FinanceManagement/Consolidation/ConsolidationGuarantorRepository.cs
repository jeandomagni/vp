﻿namespace Ivh.Provider.FinanceManagement.Consolidation
{
    using System;
    using System.Collections.Generic;
    using Common.Base.Enums;
    using Common.Data;
    using Common.Data.Connection.Connections;
    using Common.Data.Interfaces;
    using Common.VisitPay.Enums;
    using Domain.FinanceManagement.Consolidation.Interfaces;
    using Ivh.Domain.Guarantor.Entities;
    using NHibernate;

    public class ConsolidationGuarantorRepository : RepositoryBase<ConsolidationGuarantor, VisitPay>, IConsolidationGuarantorRepository
    {
        public ConsolidationGuarantorRepository(ISessionContext<VisitPay> sessionContext)
            : base(sessionContext)
        {
        }

        public IEnumerable<ConsolidationGuarantor> GetAllExpiringConsolidations(DateTime insertDate)
        {
            return this.Session.QueryOver<ConsolidationGuarantor>()
                .Where(x => (x.ConsolidationGuarantorStatus == ConsolidationGuarantorStatusEnum.Pending || x.ConsolidationGuarantorStatus == ConsolidationGuarantorStatusEnum.PendingFinancePlan)
                            && x.InsertDate <= insertDate).List();
        }

        public IEnumerable<ConsolidationGuarantor> GetPendingRequests()
        {
            return this.Session.QueryOver<ConsolidationGuarantor>()
                .Where(x => x.ConsolidationGuarantorStatus == ConsolidationGuarantorStatusEnum.Pending
                            || x.ConsolidationGuarantorStatus == ConsolidationGuarantorStatusEnum.PendingFinancePlan).List();
        }

        public IEnumerable<ConsolidationGuarantor> GetManagingRequests(Guarantor managingGuarantor)
        {
            return this.Session.QueryOver<ConsolidationGuarantor>()
                .Where(x => x.ManagingGuarantor == managingGuarantor).List();
        }

        public IEnumerable<ConsolidationGuarantor> GetManagedRequests(Guarantor managedGuarantor)
        {
            return this.Session.QueryOver<ConsolidationGuarantor>()
                .Where(x => x.ManagedGuarantor == managedGuarantor).List();
        }

        public IEnumerable<ConsolidationGuarantor> GetAllManagedGuarantors(Guarantor managingGuarantor)
        {
            return this.Session.QueryOver<ConsolidationGuarantor>()
                .Where(x => x.ManagingGuarantor == managingGuarantor)
                .OrderBy(x => x.InsertDate).Desc
                .List();
        }

        public IEnumerable<int> GetAllManagedGuarantorVpGuarantorIds(int managingVpGuarantorId)
        {
            IList<int> list = this.Session.QueryOver<ConsolidationGuarantor>()
                .Where(x => x.ManagingGuarantor.VpGuarantorId == managingVpGuarantorId)
                .Where(x => x.ConsolidationGuarantorStatus == ConsolidationGuarantorStatusEnum.Accepted)
                .Select(x => x.ManagedGuarantor.VpGuarantorId)
                .List<int>();

            return list;
        }

        public IEnumerable<ConsolidationGuarantor> GetAllManagingGuarantors(Guarantor managedGuarantor)
        {
            return this.Session.QueryOver<ConsolidationGuarantor>()
                .Where(x => x.ManagedGuarantor == managedGuarantor)
                .OrderBy(x => x.InsertDate).Desc
                .List();
        }

        public IEnumerable<ConsolidationGuarantor> GetAllRequests(Guarantor guarantor)
        {
            return this.Session.QueryOver<ConsolidationGuarantor>()
                .Where(x => x.ManagedGuarantor == guarantor || x.ManagingGuarantor == guarantor)
                .OrderBy(x => x.InsertDate).Desc
                .List();
        }
    }
}
﻿namespace Ivh.Provider.FinanceManagement.Discount
{
    using System.Linq;
    using Common.Data;
    using Common.Data.Connection.Connections;
    using Common.Data.Interfaces;
    using Domain.FinanceManagement.Discount.Entities;
    using Domain.FinanceManagement.Discount.Interfaces;
    using NHibernate;
    using NHibernate.Linq;

    public class DiscountBalanceTierRepository : RepositoryBase<DiscountBalanceTier, VisitPay>, IDiscountBalanceTierRepository
    {
        public DiscountBalanceTierRepository(ISessionContext<VisitPay> sessionContext) : base(sessionContext)
        {
        }

        public override IQueryable<DiscountBalanceTier> GetQueryable()
        {
            return base.GetQueryable() .WithOptions(x =>
            {
                x.SetCacheable(true);
            });
        }
    }
}
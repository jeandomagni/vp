﻿namespace Ivh.Provider.FinanceManagement.RoboRefund
{
    using System.Collections.Generic;
    using Common.Base.Enums;
    using Common.Data;
    using Common.Data.Connection.Connections;
    using Common.Data.Interfaces;
    using Common.VisitPay.Enums;
    using Domain.FinanceManagement.FinancePlan.Entities;
    using Domain.FinanceManagement.RoboRefund.Entities;
    using Domain.FinanceManagement.RoboRefund.Interfaces;
    using NHibernate;
    using NHibernate.Criterion;
    using NHibernate.Transform;

    public class RoboRefundRepository: RepositoryBase<RoboRefundPayment, VisitPay>, IRoboRefundRepository
    {
        public RoboRefundRepository(ISessionContext<VisitPay> sessionContext)
            : base(sessionContext)
        {
        }

        public IList<RoboRefundEventResult> GetUnRefundedEventResults(int guarantorId)
        {
            FinancePlanVisit fpv = null;
            FinancePlanVisitPrincipalAmount principal = null;
            RoboRefundEventResult result = null;
            RoboRefundQualifyingEvent qualifyingEvent = null;

            DetachedCriteria qcp = QueryOver.Of<RoboRefundQualifyingEvent>(() => qualifyingEvent)
                .Where(() => qualifyingEvent.FinancePlanVisitPrincipalAmountId == principal.FinancePlanVisitPrincipalAmountId)
                .Select(x => x.RoboRefundQualifyingEventId)
                .DetachedCriteria;

            IList<RoboRefundEventResult> results = this.Session.QueryOver(() => principal)
                .JoinAlias(() => principal.FinancePlanVisit, () => fpv)
                .Where((() => principal.Amount < 0))
                .And((() => principal.FinancePlanVisitPrincipalAmountType == FinancePlanVisitPrincipalAmountTypeEnum.Adjustment))
                .And(() => fpv.VpGuarantorId == guarantorId)
                .Where(Subqueries.NotExists(qcp))
                .SelectList(x => x
                    .Select(() => fpv.VisitId).WithAlias(() => result.VisitId)
                    .Select(() => fpv.VpGuarantorId).WithAlias(() => result.VpGuarantorId)
                    .Select(() => fpv.FinancePlanVisitId).WithAlias(() => result.FinancePlanVisitId)
                    .Select(() => principal.Amount).WithAlias(() => result.Amount)
                    .Select(() => principal.FinancePlanVisitPrincipalAmountId).WithAlias(() => result.FinancePlanVisitPrincipalAmountId)
                    .Select(() => principal.InsertDate).WithAlias(() => result.FinancePlanVisitPrincipalAmountInsertDate)
                    .Select(() => principal.OverrideInsertDate).WithAlias(() => result.FinancePlanVisitPrincipalAmountOverrideInsertDate)
                )
                .TransformUsing(Transformers.AliasToBean(typeof(RoboRefundEventResult)))
                .List<RoboRefundEventResult>();

            return results;
        }
    }
}
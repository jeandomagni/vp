﻿namespace Ivh.Provider.FinanceManagement.FinancePlan
{
    using System.Linq;
    using Common.Base.Enums;
    using Common.Data;
    using Common.Data.Connection.Connections;
    using Common.Data.Interfaces;
    using Common.VisitPay.Enums;
    using Domain.FinanceManagement.FinancePlan.Entities;
    using Domain.FinanceManagement.FinancePlan.Interfaces;
    using NHibernate;
    using NHibernate.Linq;

    public class InterestRateRepository : RepositoryBase<InterestRate, VisitPay>, IInterestRateRepository
    {
        public InterestRateRepository(ISessionContext<VisitPay> sessionContext)
            : base(sessionContext)
        {
        }

        public override IQueryable<InterestRate> GetQueryable()
        {
            return base.GetQueryable().WithOptions(x =>
            {
                x.SetCacheable(true);
                x.SetCacheMode(CacheMode.Normal);
            });
        }

        public int GetMaxDurationRangeEnd(FinancePlanOfferSetTypeEnum financialOfferSetType)
        {
            InterestRate interestRate = this.GetQueryable().Where(x => x.FinancePlanOfferSetType.FinancePlanOfferSetTypeId == (int) financialOfferSetType)
                .OrderByDescending(x => x.DurationRangeEnd)
                .FirstOrDefault();

            if (interestRate != null)
            {
                return interestRate.DurationRangeEnd;
            }
            return 0;
        }
    }
}
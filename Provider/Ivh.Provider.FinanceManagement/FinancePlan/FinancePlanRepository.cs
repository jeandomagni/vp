﻿namespace Ivh.Provider.FinanceManagement.FinancePlan
{
    using Common.Base.Utilities.Extensions;
    using Common.Base.Utilities.Helpers;
    using Common.Data;
    using Common.Data.Connection.Connections;
    using Common.Data.Interfaces;
    using Common.ServiceBus.Interfaces;
    using Common.VisitPay.Enums;
    using Common.VisitPay.Messages.Core;
    using Common.VisitPay.Messages.FinanceManagement;
    using Common.VisitPay.Messages.HospitalData;
    using Domain.FinanceManagement.FinancePlan.Entities;
    using Domain.FinanceManagement.FinancePlan.Interfaces;
    using Domain.FinanceManagement.Statement.Entities;
    using Domain.Guarantor.Entities;
    using Domain.Visit.Entities;
    using Magnum.Extensions;
    using NHibernate;
    using NHibernate.Criterion;
    using NHibernate.Linq;
    using NHibernate.SqlCommand;
    using NHibernate.Transform;
    using System;
    using System.Collections.Generic;
    using System.Linq;

    public class FinancePlanRepository : RepositoryBase<FinancePlan, VisitPay>, IFinancePlanRepository, IDisposable
    {
        private readonly Lazy<IBus> _bus;

        //Publish these one at a time
        private readonly List<FinancePlanPaymentAmountChangedMessage> _financePlanPaymentAmountChangedMessages = new List<FinancePlanPaymentAmountChangedMessage>();
        //Publish these as a list
        private readonly List<FinancePlanChangedStatusMessage> _financePlanChangedStatusMessages = new List<FinancePlanChangedStatusMessage>();
        //publish these one at a time
        private readonly List<UpdateReconfiguredFinancePlanInterestMessage> _reconfiguredFinancePlanInterestMessages = new List<UpdateReconfiguredFinancePlanInterestMessage>();

        private readonly List<AuditEventFinancePlanMessage> _unpublishedAuditEvents = new List<AuditEventFinancePlanMessage>();

        private readonly List<OutboundVisitMessage> _outboundVisitMessages = new List<OutboundVisitMessage>();

        private readonly List<FinancePlanVisitInterestDue> _financePlanVisitInterestDueItems = new List<FinancePlanVisitInterestDue>();

        public FinancePlanRepository(
            ISessionContext<VisitPay> sessionContext,
            Lazy<IBus> bus)
            : base(sessionContext)
        {
            this._bus = bus;
        }

        public IList<FinancePlan> GetFinancePlansByIds(int? vpGuarantorId, IList<int> financePlanIds)
        {
            FinancePlan fp = null;

            IQueryOver<FinancePlan, FinancePlan> query = this.Session.QueryOver<FinancePlan>(() => fp)
                .Where(Restrictions.In(Projections.Property(() => fp.FinancePlanId), financePlanIds.ToList()));

            if (vpGuarantorId.HasValue)
            {
                query.Where(() => fp.VpGuarantor.VpGuarantorId == vpGuarantorId.Value);
            }

            IList<FinancePlan> list = query.List<FinancePlan>();

            return list;
        }

        public FinancePlan GetFinancePlan(int vpGuarantorId, int financePlanId)
        {
            FinancePlan queryFinancePlan = this.GetById(financePlanId);

            if (queryFinancePlan != null && queryFinancePlan.VpGuarantor.VpGuarantorId == vpGuarantorId)
            {
                return queryFinancePlan;
            }

            return null;
        }

        public IReadOnlyList<FinancePlan> GetFinancePlans(IList<FinancePlanStatusEnum> financePlanStatuses)
        {
            IQueryOver<FinancePlan, FinancePlan> query = this.QueryOverFinancePlans(new FinancePlanFilter { FinancePlanStatusIds = financePlanStatuses });

            return (IReadOnlyList<FinancePlan>)query.List();
        }

        public IList<FinancePlan> GetAllFinancePlans(int vpGuarantorId, IList<FinancePlanStatusEnum> financePlanStatuses)
        {
            return this.GetAllFinancePlans(new List<int> { vpGuarantorId }, financePlanStatuses);
        }

        public IList<FinancePlan> GetAllFinancePlans(IList<int> vpGuarantorIds, IList<FinancePlanStatusEnum> financePlanStatuses)
        {
            IQueryOver<FinancePlan, FinancePlan> query = this.QueryOverFinancePlans(vpGuarantorIds, new FinancePlanFilter { FinancePlanStatusIds = financePlanStatuses });
            // Attempts to use the cache has resulted in issues (VP-2433 and others)
            // Commenting out Cacheable() until we can find an alternative
            //return query.Cacheable().List(); 
            return query.List();
        }

        public IList<FinancePlan> GetAllOpenFinancePlans(int vpGuarantorId)
        {
            return this.GetAllOpenFinancePlans(new List<int> { vpGuarantorId });
        }

        public IList<FinancePlan> GetAllOpenFinancePlans(IList<int> vpGuarantorIds)
        {
            return this.GetAllFinancePlans(vpGuarantorIds, new List<FinancePlanStatusEnum> { FinancePlanStatusEnum.GoodStanding, FinancePlanStatusEnum.PastDue, FinancePlanStatusEnum.UncollectableActive });
        }

        public bool HaveOpenFinancePlans(IList<int> vpGuarantorIds)
        {
            IQueryOver<FinancePlan, FinancePlan> query = this.QueryOverFinancePlans(vpGuarantorIds,
                new FinancePlanFilter
                {
                    FinancePlanStatusIds = new List<FinancePlanStatusEnum>
                    {
                        FinancePlanStatusEnum.GoodStanding, FinancePlanStatusEnum.PastDue, FinancePlanStatusEnum.UncollectableActive

                    }
                });

            return query.RowCount() > 0;
        }

        public IList<FinancePlan> GetAllPendingAcceptanceFinancePlans(IList<int> vpGuarantorIds, bool isReconfiguration = false)
        {
            IList<FinancePlan> financePlans = this.QueryOverFinancePlans(vpGuarantorIds, new FinancePlanFilter
            {
                FinancePlanStatusIds = new List<FinancePlanStatusEnum> { FinancePlanStatusEnum.PendingAcceptance },
                IncludePendingReconfigured = isReconfiguration
            }).List();

            return financePlans;
        }

        public IList<Guarantor> GetFirstPendingAcceptanceFinancePlanClientOnlyOfferGuarantor(IList<int> vpGuarantorIds, bool isReconfiguration = false)
        {
            IQueryOver<FinancePlan, FinancePlan> query = this.QueryOverFinancePlans(vpGuarantorIds, new FinancePlanFilter
            {
                FinancePlanStatusIds = new List<FinancePlanStatusEnum> { FinancePlanStatusEnum.PendingAcceptance },
                IncludePendingReconfigured = isReconfiguration
            });

            Guarantor g = null;
            FinancePlanOffer fpo = null;
            FinancePlanOfferSetType fpost = null;

            IList<Guarantor> list = query
                .JoinAlias(x => x.VpGuarantor, () => g)
                .JoinAlias(x => x.FinancePlanOffer, () => fpo)
                .JoinAlias(x => fpo.FinancePlanOfferSetType, () => fpost)
                .Where(x => fpost.IsClient && !fpost.IsPatient)
                .Select(x => x.VpGuarantor)
                .List<Guarantor>();

            return list.GroupBy(x => x.VpGuarantorId).Select(x => x.First()).ToList();
        }

        public IList<Guarantor> GetFirstPendingAcceptanceFinancePlanPatientOfferGuarantor(IList<int> vpGuarantorIds, bool isReconfiguration = false)
        {
            IQueryOver<FinancePlan, FinancePlan> query = this.QueryOverFinancePlans(vpGuarantorIds, new FinancePlanFilter
            {
                FinancePlanStatusIds = new List<FinancePlanStatusEnum> { FinancePlanStatusEnum.PendingAcceptance },
                IncludePendingReconfigured = isReconfiguration
            });

            Guarantor g = null;
            FinancePlanOffer fpo = null;
            FinancePlanOfferSetType fpost = null;

            IList<Guarantor> list = query
                .JoinAlias(x => x.VpGuarantor, () => g)
                .JoinAlias(x => x.FinancePlanOffer, () => fpo)
                .JoinAlias(x => fpo.FinancePlanOfferSetType, () => fpost)
                .Where(x => fpost.IsPatient)
                .Select(x => x.VpGuarantor)
                .List<Guarantor>();

            return list.GroupBy(x => x.VpGuarantorId).Select(x => x.First()).ToList();
        }

        public IList<FinancePlan> GetAllPendingAcceptanceFinancePlans(int vpGuarantorId, DateTime originationDate)
        {
            //DetachedCriteria detachedCriteria = DetachedCriteria.For<FinancePlanStatusHistory>("history")
            //    .SetProjection(Projections.Property("history.FinancePlanStatus.FinancePlanStatusId"))
            //    .Add(Restrictions.EqProperty("fp.FinancePlanId", "history.FinancePlan.FinancePlanId"))
            //    .AddOrder(Order.Desc("history.InsertDate"))
            //    .AddOrder(Order.Desc("history.FinancePlanStatusHistoryId"))
            //    .SetMaxResults(1);
            //ICriteria criteria = this.Session.CreateCriteria<FinancePlan>("fp")
            //    .Add(Restrictions.Eq("VpGuarantor.VpGuarantorId", VpGuarantorId))
            //    .Add(Subqueries.Eq(FinancePlanStatusEnum.PendingAcceptance, detachedCriteria))
            //    .Add(Restrictions.Le("OriginationDate", originationDate.Date));
            IList<FinancePlan> financePlans = this.QueryOverFinancePlans(vpGuarantorId, new FinancePlanFilter
            {
                FinancePlanStatusIds = new List<FinancePlanStatusEnum> { FinancePlanStatusEnum.PendingAcceptance },
                IncludePendingReconfigured = false,
                OriginationDateRangeTo = originationDate.Date
            }).List();

            return financePlans;
        }

        public IList<FinancePlan> GetAllPendingFinancePlans(int vpGuarantorId)
        {
            return this.GetAllFinancePlans(vpGuarantorId, new List<FinancePlanStatusEnum> { FinancePlanStatusEnum.PendingAcceptance, FinancePlanStatusEnum.TermsAccepted });
        }

        public IList<FinancePlan> GetAllActiveFinancePlans(int vpGuarantorId)
        {
            IList<FinancePlan> list = this.GetAllActiveFinancePlansQuery(vpGuarantorId)
                .List<FinancePlan>();

            return list;
        }

        public IList<int> GetAllActiveFinancePlanIds(int vpGuarantorId)
        {
            IList<int> list = this.GetAllActiveFinancePlansQuery(vpGuarantorId)
                .Select(x => x.FinancePlanId)
                .List<int>();

            return list;
        }

        private IQueryOver<FinancePlan, FinancePlan> GetAllActiveFinancePlansQuery(int vpGuarantorId)
        {
            return this.QueryOverFinancePlans(vpGuarantorId, new FinancePlanFilter
            {
                FinancePlanStatusIds = new List<FinancePlanStatusEnum>
                {
                    FinancePlanStatusEnum.PendingAcceptance,
                    FinancePlanStatusEnum.TermsAccepted,
                    FinancePlanStatusEnum.GoodStanding,
                    FinancePlanStatusEnum.PastDue,
                    FinancePlanStatusEnum.UncollectableActive,
                }
            });
        }

        public decimal GetTotalPaymentAmountForOpenFinancePlans(int vpGuarantorId)
        {
            FinancePlanStatusEnum[] financePlanStatuses = EnumHelper<FinancePlanStatusEnum>.GetValues(FinancePlanStatusEnumCategory.Open).ToArray();

            FinancePlan financePlanAlias = null;
            FinancePlanPaymentAmountHistory paymentAmountHistoryAlias = null;

            QueryOver<FinancePlanPaymentAmountHistory> financePlanPaymentAmountHistorySubQuery = QueryOver.Of(() => paymentAmountHistoryAlias)
                .Where(() => paymentAmountHistoryAlias.FinancePlan.FinancePlanId == financePlanAlias.FinancePlanId)
                .OrderBy(() => paymentAmountHistoryAlias.InsertDate).Desc.ThenBy(() => paymentAmountHistoryAlias.FinancePlanPaymentAmountHistoryId).Desc
                .Select(Projections.Property(() => paymentAmountHistoryAlias.PaymentAmount))
                .Take(1);

            IList<decimal> paymentAmounts = this.Session.QueryOver(() => financePlanAlias)
                .Where(Restrictions.Eq(Projections.Property(() => financePlanAlias.VpGuarantor.VpGuarantorId), vpGuarantorId))
                .Where(Restrictions.In(Projections.Property(() => financePlanAlias.CurrentFinancePlanStatus.FinancePlanStatusId), financePlanStatuses))
                .Select(Projections.SubQuery(financePlanPaymentAmountHistorySubQuery))
                .List<decimal>();

            return paymentAmounts.Sum();
        }

        public FinancePlanCreditAgreement GetFinancePlanCreditAgreementByContractNumber(string contactNumber)
        {
            IQueryOver<FinancePlanCreditAgreement> query = this.Session.QueryOver<FinancePlanCreditAgreement>()
                .Where(x => x.ContractNumber == contactNumber)
                .Take(1);
            return query.List().FirstOrDefault();
        }

        public FinancePlan GetFinancePlanByOfferId(int financePlanOfferId)
        {
            IQueryOver<FinancePlan> query = this.Session.QueryOver<FinancePlan>()
                .Where(x => x.FinancePlanOffer.FinancePlanOfferId == financePlanOfferId)
                .Take(1);

            return query.List().FirstOrDefault();
        }

        public bool FinancePlanUniqueIdIsUnique(string financePlanUniqueId)
        {
            IQueryOver<FinancePlan> query = this.Session.QueryOver<FinancePlan>()
                .Where(x => x.FinancePlanUniqueId == financePlanUniqueId);

            return query.RowCount() == 0;
        }

        public int GetFinancePlanCount(int vpGuarantorId, IList<FinancePlanStatusEnum> financePlanStatuses)
        {
            IQueryOver<FinancePlan, FinancePlan> query = this.GetFinancePlansForGuarantorWithStatuses(vpGuarantorId, financePlanStatuses);

            return query.RowCount();
        }

        public bool HasFinancePlans(int vpGuarantorId, IList<FinancePlanStatusEnum> financePlanStatuses)
        {
            return this.GetFinancePlanCount(vpGuarantorId, financePlanStatuses) > 0;
        }

        public IList<FinancePlan> GetFinancePlans(int vpGuarantorId, IList<FinancePlanStatusEnum> financePlanStatuses)
        {
            IQueryOver<FinancePlan, FinancePlan> query = this.GetFinancePlansForGuarantorWithStatuses(vpGuarantorId, financePlanStatuses);

            return query.List();
        }

        private IQueryOver<FinancePlan, FinancePlan> GetFinancePlansForGuarantorWithStatuses(int vpGuarantorId, IList<FinancePlanStatusEnum> financePlanStatuses)
        {
            IQueryOver<FinancePlan, FinancePlan> query = this.QueryOverFinancePlans(vpGuarantorId, new FinancePlanFilter { FinancePlanStatusIds = financePlanStatuses });

            return query;
        }

        public int GetActiveFinancePlanCount(int vpGuarantorId)
        {
            return this.GetFinancePlanCount(vpGuarantorId, new List<FinancePlanStatusEnum>
            {
                        FinancePlanStatusEnum.PendingAcceptance,
                        FinancePlanStatusEnum.TermsAccepted,
                        FinancePlanStatusEnum.GoodStanding,
                        FinancePlanStatusEnum.PastDue,
                        FinancePlanStatusEnum.UncollectableActive,
                    });
        }

        public IList<FinancePlanStatus> GetFinancePlanStatuses()
        {
            return this.Session.Query<FinancePlanStatus>()
                .WithOptions(x =>
                {
                    x.SetCacheable(true);
                    x.SetCacheMode(CacheMode.Normal);
                })
                .ToList();
        }

        public IList<FinancePlanType> GetFinancePlanTypes()
        {
            return this.Session.Query<FinancePlanType>()
                .WithOptions(x =>
                {
                    x.SetCacheable(true);
                    x.SetCacheMode(CacheMode.Normal);
                })
                .ToList();
        }

        public IList<int> AreVisitIdsOnActiveFinancePlan(IList<int> visitIds, DateTime? asOfDate = null)
        {
            IList<FinancePlanVisitResult> result = this.AreVisitsOnFinancePlan_FinancePlanVisitResult(visitIds.ToList(), asOfDate, new List<FinancePlanStatusEnum>
            {
                FinancePlanStatusEnum.GoodStanding,
                FinancePlanStatusEnum.PastDue,
                FinancePlanStatusEnum.UncollectableActive
            });
            HashSet<int> hashSetOfInts = result.Select(x => x.VisitId).ToHashSet();

            return visitIds.Where(x => hashSetOfInts.Contains(x)).ToList();
        }

        public IList<Visit> AreVisitsOnActiveFinancePlan(IList<Visit> visits, DateTime? asOfDate = null)
        {
            IList<FinancePlanVisitResult> result = this.AreVisitsOnFinancePlan_FinancePlanVisitResult(visits.Select(x => x.VisitId).ToList(), asOfDate, new List<FinancePlanStatusEnum>
            {
                FinancePlanStatusEnum.GoodStanding,
                FinancePlanStatusEnum.PastDue,
                FinancePlanStatusEnum.UncollectableActive
            });
            HashSet<int> hashSetOfInts = result.Select(x => x.VisitId).ToHashSet();

            return visits.Where(x => hashSetOfInts.Contains(x.VisitId)).ToList();
        }

        public IList<Visit> AreVisitsOnActiveOrPendingFinancePlan(IList<Visit> visits, DateTime? asOfDate = null)
        {
            IList<FinancePlanVisitResult> result = this.AreVisitsOnFinancePlan_FinancePlanVisitResult(visits.Select(x => x.VisitId).ToList(), asOfDate, new List<FinancePlanStatusEnum>
            {
                FinancePlanStatusEnum.GoodStanding,
                FinancePlanStatusEnum.PastDue,
                FinancePlanStatusEnum.PendingAcceptance,
                FinancePlanStatusEnum.TermsAccepted,
                FinancePlanStatusEnum.UncollectableActive
            });
            HashSet<int> hashSetOfInts = result.Select(x => x.VisitId).ToHashSet();

            return visits.Where(x => hashSetOfInts.Contains(x.VisitId)).ToList();
        }

        public IList<FinancePlanVisitResult> AreVisitsOnFinancePlan_FinancePlanVisitResult(IList<int> visitIds, DateTime? asOfDate, List<FinancePlanStatusEnum> listOfStatuses = null)
        {
            const int chunkSize = 500;
            IEnumerable<IEnumerable<int>> visitIdChunks = visitIds.SplitIntoChunks(chunkSize);

            IList<FinancePlanVisitResult> financePlanVisitResults = new List<FinancePlanVisitResult>();

            foreach (IEnumerable<int> chunk in visitIdChunks)
            {
                FinancePlan fp = null;
                FinancePlanVisit fpv = null;
                FinancePlanVisitVisit fpvv = null;
                FinancePlanVisitResult result = null;

                IQueryOver<FinancePlanVisit, FinancePlanVisit> query = this.AreVisitsOnFinancePlan_Query(chunk.ToList(), asOfDate, listOfStatuses);

                IList<FinancePlanVisitResult> list = query.SelectList(x => x
                        .Select(() => fpv.FinancePlan.FinancePlanId).WithAlias(() => result.FinancePlanId)
                        .Select(() => fp.CurrentFinancePlanStatus.FinancePlanStatusId).WithAlias(() => result.FinancePlanStatus)
                        .Select(() => fpv.HardRemoveDate).WithAlias(() => result.HardRemoveDate)
                        .Select(() => fpvv.BillingSystemId).WithAlias(() => result.VisitBillingSystemId)
                        .Select(() => fpv.VisitId).WithAlias(() => result.VisitId)
                        .Select(() => fpvv.SourceSystemKey).WithAlias(() => result.VisitSourceSystemKey)
                    )
                    .TransformUsing(Transformers.AliasToBean(typeof(FinancePlanVisitResult)))
                    .List<FinancePlanVisitResult>();

                financePlanVisitResults.AddRange(list);
            }

            return financePlanVisitResults;
        }

        public IList<FinancePlanVisit> AreVisitsOnFinancePlan_FinancePlanVisit(IList<int> visitIds, DateTime? asOfDate, List<FinancePlanStatusEnum> listOfStatuses = null)
        {
            IQueryOver<FinancePlanVisit, FinancePlanVisit> query = this.AreVisitsOnFinancePlan_Query(visitIds, asOfDate, listOfStatuses);

            IList<FinancePlanVisit> list = query.List<FinancePlanVisit>();

            return list;
        }

        private IQueryOver<FinancePlanVisit, FinancePlanVisit> AreVisitsOnFinancePlan_Query(IList<int> visitIds, DateTime? asOfDate, List<FinancePlanStatusEnum> listOfStatuses = null)
        {
            List<FinancePlanStatusEnum> defaultList = new List<FinancePlanStatusEnum>
            {
                FinancePlanStatusEnum.PaidInFull,
                FinancePlanStatusEnum.NonPaidInFull,
                FinancePlanStatusEnum.GoodStanding,
                FinancePlanStatusEnum.PastDue,
                FinancePlanStatusEnum.PendingAcceptance,
                FinancePlanStatusEnum.TermsAccepted,
                FinancePlanStatusEnum.UncollectableActive
            };

            FinancePlan fp = null;
            FinancePlanVisit fpv = null;
            FinancePlanVisitVisit fpvv = null;

            IQueryOver<FinancePlanVisit, FinancePlanVisit> query = this.Session.QueryOver(() => fpv)
                .JoinAlias(() => fpv.FinancePlan, () => fp, JoinType.InnerJoin)
                .JoinAlias(() => fpv.FinancePlanVisitVisit, () => fpvv, JoinType.InnerJoin)
                .Where(Restrictions.In(Projections.Property(() => fpv.VisitId), visitIds.Distinct().ToList()))
                .Where(Restrictions.In(Projections.Property(() => fp.CurrentFinancePlanStatus.FinancePlanStatusId), listOfStatuses ?? defaultList));

            if (asOfDate != null)
            {
                query = query.Where(Restrictions.Or(Restrictions.IsNull(Projections.Property(() => fpv.HardRemoveDate)), Restrictions.Le(Projections.Property(() => fpv.HardRemoveDate), asOfDate.Value)));
            }
            else
            {
                query = query.Where(Restrictions.IsNull(Projections.Property(() => fpv.HardRemoveDate)));
            }

            return query;
        }

        public FinancePlan GetFinancePlanWithVisit(Visit visit)
        {
            return this.GetFinancePlanWithVisitId(visit.VisitId);
        }

        public FinancePlan GetFinancePlanWithVisitId(int visitId)
        {
            return this.GetFinancePlansFromVisits(visitId.ToListOfOne()).FirstOrDefault();
        }

        public IList<FinancePlan> GetFinancePlansFromVisits(IList<int> visitIds)
        {
            return this.FinancePlansFromVisitsWithStatusList(visitIds, new List<FinancePlanStatusEnum>
                {
                    FinancePlanStatusEnum.GoodStanding,
                    FinancePlanStatusEnum.PastDue,
                    FinancePlanStatusEnum.PendingAcceptance,
                    FinancePlanStatusEnum.TermsAccepted,
                    FinancePlanStatusEnum.UncollectableActive
                });
        }

        private IList<FinancePlan> FinancePlansFromVisitsWithStatusList(IEnumerable<int> visitIds,
            IEnumerable<FinancePlanStatusEnum> statusList,
            bool excludeFinancePlansOfRemovedVisits = true,
            params Func<IQueryOver<FinancePlan, FinancePlan>, FinancePlan, FinancePlanVisit, IQueryOver<FinancePlan, FinancePlan>>[] additionalQueryFilters)
        {
            FinancePlan fp = null;
            FinancePlanVisit fpv = null;

            IQueryOver<FinancePlan, FinancePlan> query = this.Session.QueryOver(() => fp)
                .JoinAlias(() => fp.FinancePlanVisits, () => fpv, JoinType.InnerJoin)
                .Where(Restrictions.In(Projections.Property(() => fpv.VisitId), visitIds.ToList()))
                .Where(Restrictions.In(Projections.Property(() => fp.CurrentFinancePlanStatus.FinancePlanStatusId), statusList.ToList()));

            if (excludeFinancePlansOfRemovedVisits)
            {
                query = query.Where(Restrictions.IsNull(Projections.Property(() => fpv.HardRemoveDate)));
            }

            foreach (Func<IQueryOver<FinancePlan, FinancePlan>, FinancePlan, FinancePlanVisit, IQueryOver<FinancePlan, FinancePlan>> additionalQueryFilter in additionalQueryFilters)
            {
                query = additionalQueryFilter(query, fp, fpv);
            }

            IList<FinancePlan> fps = query.List<FinancePlan>();
            return fps;
        }

        public bool IsVisitOnAnyStatusFinancePlan(int visitId)
        {
            FinancePlanVisit fpv = null;

            int count = this.Session.QueryOver(() => fpv)
                .Where(() => fpv.VisitId == visitId)
                .RowCount();

            return count > 0;
        }

        public IList<FinancePlan> GetFinancePlansIncludingClosedFromVisits(IList<int> visitIds)
        {
            return this.FinancePlansFromVisitsWithStatusList(visitIds, Enum.GetValues(typeof(FinancePlanStatusEnum)).Cast<FinancePlanStatusEnum>())
                .GroupBy(p => p.FinancePlanId)
                .Select(g => g.First())
                .ToList();
        }

        public IList<FinancePlan> GetAllOriginatedFinancePlansIncludingClosedFromVisits(IList<int> visitIds)
        {
            IList<FinancePlan> financePlans = this.FinancePlansFromVisitsWithStatusList(visitIds,
                    Enum.GetValues(typeof(FinancePlanStatusEnum)).Cast<FinancePlanStatusEnum>(),
                    false,
                    (q, fp, fpv) => q.Where(Restrictions.IsNotNull(Projections.Property(() => fp.OriginationDate))));

            return financePlans;
        }

        public IDictionary<int, FinancePlan> GetMostRecentFinancePlanForVisitIds(IList<int> visitIds)
        {
            IList<FinancePlan> financePlans = this.GetAllOriginatedFinancePlansIncludingClosedFromVisits(visitIds);

            IDictionary<int, FinancePlan> latestFinancePlansForVisits = new Dictionary<int, FinancePlan>();

            foreach (int visitId in visitIds)
            {
                IList<FinancePlan> financePlansForVisits = financePlans.Where(x => x.FinancePlanVisits.Any(y => y.VisitId == visitId)).ToList();

                FinancePlan latestFinancePlan = financePlansForVisits
                    .OrderByDescending(x => x.FinancePlanVisits.First(fpv => fpv.VisitId == visitId).HardRemoveDate.HasValue ? 0 : 1)
                    .ThenByDescending(x => x.OriginationDate)
                    .FirstOrDefault();

                if (latestFinancePlan != null)
                {
                    latestFinancePlansForVisits.Add(visitId, latestFinancePlan);
                }
            }

            return latestFinancePlansForVisits;
        }

        public override void InsertOrUpdate(FinancePlan entity)
        {
            if (this.Session.Transaction.IsActive)
            {
                this.PrivateInsert(entity);
            }
            else
            {
                using (ITransaction transaction = this.Session.BeginTransaction())
                {
                    this.PrivateInsert(entity);
                    transaction.Commit();
                }
            }
        }

        public override void Update(FinancePlan entity)
        {
            if (this.Session.Transaction.IsActive)
            {
                this.PrivateUpdate(entity);
            }
            else
            {
                using (ITransaction transaction = this.Session.BeginTransaction())
                {
                    this.PrivateUpdate(entity);
                    transaction.Commit();
                }
            }
        }

        private void PrivateInsert(FinancePlan entity)
        {
            base.InsertOrUpdate(entity);
            this.SaveMessagesForPublishing(entity);
        }

        private void PrivateUpdate(FinancePlan entity)
        {
            base.Update(entity);
            this.SaveMessagesForPublishing(entity);
        }

        private void SaveMessagesForPublishing(FinancePlan entity)
        {
            if (entity.UnpublishedFinancePlanPaymentAmountChangedMessage != null)
            {
                if (entity.UnpublishedFinancePlanPaymentAmountChangedMessage.FinancePlanId == 0)
                {
                    entity.UnpublishedFinancePlanPaymentAmountChangedMessage.FinancePlanId = entity.FinancePlanId;
                }
                this._financePlanPaymentAmountChangedMessages.Add(entity.UnpublishedFinancePlanPaymentAmountChangedMessage);
                entity.UnpublishedFinancePlanPaymentAmountChangedMessage = null;
            }

            if (entity.UnpublishedFinancePlanChangedStatusMessages != null && entity.UnpublishedFinancePlanChangedStatusMessages.Count > 0)
            {
                this._financePlanChangedStatusMessages.AddRange(entity.UnpublishedFinancePlanChangedStatusMessages.ToList());
                entity.UnpublishedFinancePlanChangedStatusMessages.Clear();
            }

            if (entity.UnpublishedUpdateReconfiguredFinancePlanInterestMessage != null)
            {
                this._reconfiguredFinancePlanInterestMessages.Add(entity.UnpublishedUpdateReconfiguredFinancePlanInterestMessage);
                entity.UnpublishedUpdateReconfiguredFinancePlanInterestMessage = null;
            }

            if (entity.OutboundVisitMessages != null && entity.OutboundVisitMessages.Count > 0)
            {
                this._outboundVisitMessages.AddRange(entity.OutboundVisitMessages.ToList());
                entity.OutboundVisitMessages.Clear();
            }

            if (entity.FinancePlanVisitInterestDueItems != null && entity.FinancePlanVisitInterestDueItems.Any())
            {
                this._financePlanVisitInterestDueItems.AddRange(entity.FinancePlanVisitInterestDueItems.ToList());
                entity.FinancePlanVisitInterestDueItems.Clear();
            }

            if (entity.UnpublishedFinancePlanAuditEvents != null && entity.UnpublishedFinancePlanAuditEvents.Count > 0)
            {
                List<string> auditEvents = new List<string>();
                auditEvents.AddRange(entity.UnpublishedFinancePlanAuditEvents.Where(x => x != AuditEventTypeFinancePlanEnum.Default).Select(x => x.ToString()));

                if (entity.HasBalanceChanged)
                {
                    auditEvents.Add(nameof(AuditEventTypeFinancePlanEnum.BalanceChange));
                }

                if (!auditEvents.Any())
                {
                    auditEvents.Add(Environment.StackTrace);
                }

                this._unpublishedAuditEvents.Add(new AuditEventFinancePlanMessage
                {
                    FinancePlanId = entity.FinancePlanId,
                    AuditEvents = auditEvents
                });

                entity.UnpublishedFinancePlanAuditEvents.Clear();
            }

            this.Session.Transaction.RegisterPostCommitSuccessAction(this.CallBackAfterCommitToPublishMessages);
            this.Session.Transaction.RegisterPostCommitFailureAction(this.CallBackAfterRollbackToPublishMessages);
        }

        private void CallBackAfterRollbackToPublishMessages()
        {
            this._financePlanPaymentAmountChangedMessages?.Clear();
            this._financePlanChangedStatusMessages?.Clear();
            this._reconfiguredFinancePlanInterestMessages?.Clear();
            this._unpublishedAuditEvents?.Clear();
            this._outboundVisitMessages?.Clear();
            this._financePlanVisitInterestDueItems?.Clear();
        }

        private void CallBackAfterCommitToPublishMessages()
        {
            if (this._financePlanPaymentAmountChangedMessages != null && this._financePlanPaymentAmountChangedMessages.Count > 0)
            {
                foreach (FinancePlanPaymentAmountChangedMessage financePlanPaymentAmountChangedMessage in this._financePlanPaymentAmountChangedMessages)
                {
                    this._bus.Value.PublishMessage(financePlanPaymentAmountChangedMessage).Wait();
                }
                this._financePlanPaymentAmountChangedMessages.Clear();
            }

            if (this._financePlanChangedStatusMessages != null && this._financePlanChangedStatusMessages.Count > 0)
            {
                this._bus.Value.PublishMessage(new FinancePlanChangedStatusMessageList { Messages = this._financePlanChangedStatusMessages }).Wait();
                this._financePlanChangedStatusMessages.Clear();
            }

            if (this._reconfiguredFinancePlanInterestMessages != null && this._reconfiguredFinancePlanInterestMessages.Count > 0)
            {
                foreach (UpdateReconfiguredFinancePlanInterestMessage reconfig in this._reconfiguredFinancePlanInterestMessages)
                {
                    this._bus.Value.PublishMessage(reconfig).Wait();
                }
                this._reconfiguredFinancePlanInterestMessages.Clear();
            }

            if (this._unpublishedAuditEvents?.Count > 0)
            {
                foreach (AuditEventFinancePlanMessage auditEvent in this._unpublishedAuditEvents)
                {
                    this._bus.Value.PublishMessage(auditEvent).Wait();
                }
                this._unpublishedAuditEvents.Clear();
            }

            if (this._outboundVisitMessages != null && this._outboundVisitMessages.Count > 0)
            {
                this._bus.Value.PublishMessage(new OutboundVisitMessageList() { Messages = this._outboundVisitMessages }).Wait();
                this._outboundVisitMessages.Clear();
            }

            if (this._financePlanVisitInterestDueItems != null && this._financePlanVisitInterestDueItems.Any())
            {
                InterestEventMessageList interestEventMessageList = new InterestEventMessageList(this._financePlanVisitInterestDueItems
                    .Where(x => x.PaymentAllocationId == null)
                    .Where(x => x.InterestDue != 0m)
                    .Select(financePlanVisitInterestDue => new InterestEventMessage
                    {
                        VisitBillingSystemId = financePlanVisitInterestDue.FinancePlanVisit.BillingSystemId,
                        VisitSourceSystemKey = financePlanVisitInterestDue.FinancePlanVisit.SourceSystemKey,
                        VpFinancePlanVisitInterestDueId = financePlanVisitInterestDue.FinancePlanVisitInterestDueId,
                        VpFinancePlanVisitInterestDueParentId = financePlanVisitInterestDue.ParentFinancePlanVisitInterestDue?.FinancePlanVisitInterestDueId,
                        VpFinancePlanVisitInterestDueTypeId = (int)financePlanVisitInterestDue.FinancePlanVisitInterestDueType,
                        VpFinancePlanVisitInterestInsertDate = financePlanVisitInterestDue.InsertDate,
                        VpFinancePlanVisitInterestAmount = financePlanVisitInterestDue.InterestDue,
                        VpFinancePlanId = financePlanVisitInterestDue.FinancePlanVisit.FinancePlan.FinancePlanId,
                        VpGuarantorId = financePlanVisitInterestDue.FinancePlanVisit.VpGuarantorId,
                        ProcessedDate = financePlanVisitInterestDue.InsertDate
                    }).ToList());

                this._bus.Value.PublishMessage(interestEventMessageList).Wait();
                this._financePlanVisitInterestDueItems.Clear();
            }
        }

        public FinancePlanResults GetFinancePlans(int vpGuarantorId, FinancePlanFilter filter, int batch, int batchSize)
        {
            FinancePlanResults financePlanResults = this.GetFinancePlanTotals(vpGuarantorId, filter);

            financePlanResults.FinancePlans = this.DoGetFinancePlans(vpGuarantorId, filter, batch, batchSize);

            return financePlanResults;
        }

        private FinancePlanResults GetFinancePlanTotals(int vpGuarantorId, FinancePlanFilter filter)
        {
            IList<FinancePlan> financePlans = this.QueryOverFinancePlans(vpGuarantorId, filter).List();

            int totalRecords = financePlans.Count();
            decimal totalCurrentBalance = financePlans.Sum(x => x.CurrentFinancePlanBalance);

            return new FinancePlanResults
            {
                TotalRecords = totalRecords,
                TotalCurrentBalance = totalCurrentBalance
            };
        }

        public FinancePlanCount GetFinancePlanTotalsWithoutSum(int vpGuarantorId, FinancePlanFilter filter)
        {
            FinancePlan financePlanAlias = null;

            IQueryOver<FinancePlan, FinancePlan> query = this.Session.QueryOver(() => financePlanAlias)
                .Where(() => financePlanAlias.VpGuarantor.VpGuarantorId == vpGuarantorId);

            this.Filter(filter, query);

            IList<FinancePlanStatusEnum> statuses = query.Select(q => q.CurrentFinancePlanStatus.FinancePlanStatusId).List<FinancePlanStatusEnum>();

            return new FinancePlanCount
            {
                Total = statuses.Count,
                PastDue = statuses.Count(x => new List<FinancePlanStatusEnum> { FinancePlanStatusEnum.PastDue, FinancePlanStatusEnum.UncollectableActive, FinancePlanStatusEnum.UncollectableClosed }.Contains(x))
            };
        }

        private IList<FinancePlan> DoGetFinancePlans(int vpGuarantorId, FinancePlanFilter filter, int batch, int batchSize)
        {
            IQueryOver<FinancePlan, FinancePlan> query = this.QueryOverFinancePlans(vpGuarantorId, filter);

            this.Sort(filter, query);

            IList<FinancePlan> list = query.List();

            list = this.PostSort(filter, list);

            return list.Skip(batch * batchSize)
                       .Take(batchSize)
                       .ToList();
        }

        private IQueryOver<FinancePlan, FinancePlan> QueryOverFinancePlans(int vpGuarantorId, FinancePlanFilter filter)
        {
            return this.QueryOverFinancePlans(new List<int> { vpGuarantorId }, filter);
        }

        private IQueryOver<FinancePlan, FinancePlan> QueryOverFinancePlans(IEnumerable<int> vpGuarantorIds, FinancePlanFilter filter)
        {
            FinancePlan financePlanAlias = null;
            IQueryOver<FinancePlan, FinancePlan> query = this.Session.QueryOver(() => financePlanAlias)
                .Where(Restrictions.In(Projections.Property(() => financePlanAlias.VpGuarantor.VpGuarantorId), vpGuarantorIds.ToList()));

            this.Filter(filter, query);

            return query;
        }

        private IQueryOver<FinancePlan, FinancePlan> QueryOverFinancePlans(FinancePlanFilter filter)
        {
            FinancePlan financePlanAlias = null;
            IQueryOver<FinancePlan, FinancePlan> query = this.Session.QueryOver<FinancePlan>(() => financePlanAlias);
            this.Filter(filter, query);

            return query;
        }

        private void Filter(FinancePlanFilter filter, IQueryOver<FinancePlan, FinancePlan> query)
        {
            FinancePlan financePlanAlias = null;

            // exclude pending reconfigured by default
            bool excludePendingReconfigured = !filter.IncludePendingReconfigured.GetValueOrDefault(false);
            query.If(excludePendingReconfigured, q => q.Where(new Disjunction()
                .Add(Restrictions.IsNull(Projections.Property(() => financePlanAlias.OriginalFinancePlan)))
                .Add(Restrictions.And(
                    Restrictions.IsNotNull(Projections.Property(() => financePlanAlias.OriginalFinancePlan)),
                    Restrictions.Not(Restrictions.In(
                        Projections.Property(() => financePlanAlias.CurrentFinancePlanStatus.FinancePlanStatusId), new List<FinancePlanStatusEnum> { FinancePlanStatusEnum.PendingAcceptance }))))));

            query.If(filter.FinancePlanId.HasValue, q => q.Where(x => x.FinancePlanId == filter.FinancePlanId))
                .If(filter.OriginationDateRangeFrom.HasValue, q => q.Where(x => x.OriginationDate >= filter.OriginationDateRangeFrom))
                .If(filter.OriginationDateRangeTo.HasValue, q => q.Where(x => x.OriginationDate <= filter.OriginationDateRangeTo))
                .If(filter.OriginationDateSince.HasValue, q => q.Where(x => x.OriginationDate >= filter.OriginationDateSince.Value))
                .If(filter.FinancePlanStatusIds.Any(), q => q.Where(Restrictions.In(Projections.Property(() => financePlanAlias.CurrentFinancePlanStatus.FinancePlanStatusId), filter.FinancePlanStatusIds.ToList())))
                .If(filter.FinancePlansIds.Any(), q => q.Where(Restrictions.In(Projections.Property(() => financePlanAlias.FinancePlanId), filter.FinancePlansIds)));

        }

        private void Sort(FinancePlanFilter filter, IQueryOver<FinancePlan, FinancePlan> query)
        {
            if (filter.SortField.IsEmpty())
            {
                return;
            }

            bool isAscendingOrder = "asc".Equals(filter.SortOrder, StringComparison.InvariantCultureIgnoreCase);

            switch (filter.SortField)
            {
                case nameof(FinancePlan.CurrentBucket):
                case nameof(FinancePlan.CurrentFinancePlanBalance):
                case nameof(FinancePlan.InterestAssessed):
                case nameof(FinancePlan.OriginatedFinancePlanBalance):
                case nameof(FinancePlan.OriginationDate):
                    query.UnderlyingCriteria.AddOrder(new Order(filter.SortField, isAscendingOrder));
                    query.UnderlyingCriteria.AddOrder(new Order(nameof(FinancePlan.FinancePlanId), isAscendingOrder));
                    break;
            }
        }

        private IList<FinancePlan> PostSort(FinancePlanFilter filter, IList<FinancePlan> financePlans)
        {
            if (filter.SortField.IsEmpty())
            {
                return financePlans;
            }

            bool isAscendingOrder = "asc".Equals(filter.SortOrder, StringComparison.InvariantCultureIgnoreCase);

            switch (filter.SortField)
            {
                case "FinancePlanStatusFullDisplayName":
                    financePlans = financePlans.OrderBy(x => x.FinancePlanStatus.FinancePlanStatusDisplayName, isAscendingOrder).ToList();
                    break;

                case "InterestPaidToDate":
                    financePlans = financePlans.OrderBy(x => x.InterestPaidForDate(DateTime.UtcNow), isAscendingOrder).ToList();
                    break;

                case "PaymentAmount":
                    financePlans = financePlans.OrderBy(x => x.PaymentAmount, isAscendingOrder).ToList();
                    break;

                case "PrincipalPaidToDate":
                    financePlans = financePlans.OrderBy(x => x.PrincipalPaidForDate(DateTime.UtcNow), isAscendingOrder).ToList();
                    break;
            }

            return financePlans;
        }

        public IList<FinancePlanVisitResult> GetVisitOnFinancePlanResultsBySourceSystemKey(IList<string> sourceSystemKeys, int billingSystemId)
        {
            IList<FinancePlanStatusEnum> financePlanStatuses = new List<FinancePlanStatusEnum>
            {
                FinancePlanStatusEnum.GoodStanding,
                FinancePlanStatusEnum.PastDue,
                FinancePlanStatusEnum.UncollectableActive
            };

            FinancePlan fp = null;
            FinancePlanVisit fpv = null;
            FinancePlanVisitVisit fpvv = null;
            FinancePlanVisitResult result = null;

            IList<FinancePlanVisitResult> list = this.Session.QueryOver(() => fpv)
                .JoinAlias(() => fpv.FinancePlan, () => fp, JoinType.InnerJoin)
                .JoinAlias(() => fpv.FinancePlanVisitVisit, () => fpvv, JoinType.InnerJoin)
                .Where(() => fpvv.BillingSystemId == billingSystemId)
                .Where(Restrictions.In(Projections.Property(() => fpvv.SourceSystemKey), sourceSystemKeys.ToList()))
                .Where(Restrictions.In(Projections.Property(() => fp.CurrentFinancePlanStatus.FinancePlanStatusId), financePlanStatuses.ToList()))
                .Where(Restrictions.IsNull(Projections.Property(() => fpv.HardRemoveDate)))
                .SelectList(x => x
                    .Select(() => fpv.FinancePlan.FinancePlanId).WithAlias(() => result.FinancePlanId)
                    .Select(() => fp.CurrentFinancePlanStatus.FinancePlanStatusId).WithAlias(() => result.FinancePlanStatus)
                    .Select(() => fpv.HardRemoveDate).WithAlias(() => result.HardRemoveDate)
                    .Select(() => fpvv.BillingSystemId).WithAlias(() => result.VisitBillingSystemId)
                    .Select(() => fpv.VisitId).WithAlias(() => result.VisitId)
                    .Select(() => fpvv.SourceSystemKey).WithAlias(() => result.VisitSourceSystemKey)
                )
                .TransformUsing(Transformers.AliasToBean(typeof(FinancePlanVisitResult)))
                .List<FinancePlanVisitResult>();

            return list;
        }

        public FinancePlanVisit GetVisitOnFinancePlan(int visitId)
        {
            FinancePlan fp = null;

            FinancePlanVisit financePlanVisit = this.GetVisitOnFinancePlanQuery(visitId)
                .OrderBy(() => fp.InsertDate).Desc
                .Take(1)
                .SingleOrDefault();

            return financePlanVisit;
        }

        public IList<FinancePlanVisit> GetVisitOnAnyFinancePlan(int visitId)
        {
            IList<FinancePlanVisit> financePlanVisits = this.GetVisitOnFinancePlanQuery(visitId)
                .List();

            return financePlanVisits;
        }

        public IList<int> GetVisitIdsOnFinancePlans(int vpGuarantorId, int? financePlanId, IList<FinancePlanStatusEnum> checkStatuses)
        {
            FinancePlan fp = null;
            FinancePlanVisit fpv = null;

            IQueryOver<FinancePlanVisit, FinancePlanVisit> query = this.Session.QueryOver(() => fpv)
                .JoinAlias(() => fpv.FinancePlan, () => fp, JoinType.InnerJoin)
                .Where(() => fp.VpGuarantor.VpGuarantorId == vpGuarantorId)
                .Where(Restrictions.IsNull(Projections.Property(() => fpv.HardRemoveDate)))
                .Where(Restrictions.In(Projections.Property(() => fp.CurrentFinancePlanStatus.FinancePlanStatusId), checkStatuses.ToList()));

            if (financePlanId.HasValue)
            {
                query = query.Where(() => fp.FinancePlanId == financePlanId.Value);
            }

            IList<int> list = query.Select(x => x.VisitId).List<int>();

            return list;
        }

        private IQueryOver<FinancePlanVisit, FinancePlanVisit> GetVisitOnFinancePlanQuery(int visitId, IList<FinancePlanStatusEnum> financePlanStatuses = null)
        {
            financePlanStatuses = financePlanStatuses ?? EnumHelper<FinancePlanStatusEnum>.GetValuesNot(FinancePlanStatusEnumCategory.Pending).ToList();

            FinancePlan fp = null;
            FinancePlanVisit fpv = null;

            IQueryOver<FinancePlanVisit, FinancePlanVisit> query = this.Session.QueryOver(() => fpv)
                .JoinAlias(() => fpv.FinancePlan, () => fp, JoinType.InnerJoin)
                .Where(() => fpv.VisitId == visitId)
                .Where(Restrictions.IsNull(Projections.Property(() => fpv.HardRemoveDate)))
                .Where(Restrictions.In(Projections.Property(() => fp.CurrentFinancePlanStatus.FinancePlanStatusId), financePlanStatuses.ToList()));

            return query;
        }

        public void Dispose()
        {
            base.CallBackAfterCommit = null;
        }

        public IList<int> GetFinancePlanGuarantorIdsByStatementDueDate(DateTime paymentDueDate)
        {
            VpStatement statement = null;

            QueryOver<VpStatement, VpStatement> subquery = QueryOver.Of(() => statement)
                .Where(() => statement.PaymentDueDate == paymentDueDate)
                .Select(x => x.VpGuarantorId);

            FinancePlan fp = null;

            IQueryOver<FinancePlan, FinancePlan> query = this.Session.QueryOver(() => fp)
                .WithSubquery.WhereProperty(() => fp.VpGuarantor.VpGuarantorId).In(subquery)
                .Select(x => x.VpGuarantor.VpGuarantorId);

            return query.List<int>();
        }

        public IList<int> GetFinancePlanIdsWithAmountDueForDate(DateTime dateToCheck, int? vpGuarantorId = null)
        {
            //Get all active FP statuses
            List<int> listOfActive = EnumExt.GetAllInCategory<FinancePlanStatusEnum>(FinancePlanStatusEnumCategory.Active).Select(x => (int)x).ToList();

            FinancePlan financePlanAlias = null;
            FinancePlanAmountDue financePlanAmountDueAlias = null;

            IQueryOver<FinancePlanAmountDue, FinancePlan> queryOver = this.Session.QueryOver(() => financePlanAmountDueAlias)
                .JoinQueryOver(fpv => fpv.FinancePlan, () => financePlanAlias)
                .Where(Restrictions.In(Projections.Property(() => financePlanAlias.CurrentFinancePlanStatus.FinancePlanStatusId), listOfActive))
                .Where(
                    Restrictions.Or(
                        Restrictions.Lt(Projections.Property(() => financePlanAmountDueAlias.DueDate), dateToCheck),
                        Restrictions.IsNull(Projections.Property(() => financePlanAmountDueAlias.DueDate))
                    )
                )
                .Where(Restrictions.Gt(Projections.Sum(() => financePlanAmountDueAlias.AmountDue), 0m));

            if (vpGuarantorId.HasValue)
            {
                Guarantor vpGuarantorAlias = null;
                queryOver.JoinQueryOver(fp => fp.VpGuarantor, () => vpGuarantorAlias)
                    .Where(Restrictions.Eq(Projections.Property(() => vpGuarantorAlias.VpGuarantorId), vpGuarantorId.Value));
            }

            IList<int> result = queryOver
                .Select(Projections.GroupProperty(Projections.Property(() => financePlanAmountDueAlias.FinancePlan.FinancePlanId))
                ).List<int>();

            return result;
        }

        public IList<FinancePlanVisitInterestDueResult> GetFinancePlanVisitInterestDues(IList<int> visitIds, IList<FinancePlanStatusEnum> checkStatuses, DateTime? startDate = null, DateTime? endDate = null)
        {
            if (visitIds.IsNullOrEmpty())
            {
                return new List<FinancePlanVisitInterestDueResult>();
            }

            FinancePlanVisitInterestDue i = null;
            FinancePlanVisit fpv = null;
            FinancePlan fp = null;
            FinancePlanVisitInterestDueResult result = null;

            IQueryOver<FinancePlanVisitInterestDue, FinancePlanVisitInterestDue> query = this.Session.QueryOver(() => i)
                .JoinAlias(() => i.FinancePlanVisit, () => fpv)
                .JoinAlias(() => i.FinancePlan, () => fp)
                .Where(Restrictions.In(Projections.Property(() => fpv.VisitId), visitIds.ToList()));

            if (checkStatuses != null)
            {
                query = query.Where(Restrictions.In(Projections.Property(() => fp.CurrentFinancePlanStatus.FinancePlanStatusId), checkStatuses.Cast<int>().ToList()));
            }

            if (startDate.HasValue || endDate.HasValue)
            {
                Disjunction restrictionsOr = Restrictions.Disjunction();

                Conjunction overrideInsertDateAnd = Restrictions.Conjunction();
                Conjunction insertDateAnd = Restrictions.Conjunction();
                insertDateAnd.Add(Restrictions.IsNull(Projections.Property(() => i.OverrideInsertDate))); // only use InsertDate if OverrideInsertDate is null

                if (startDate.HasValue)
                {
                    overrideInsertDateAnd.Add(Restrictions.Ge(Projections.Property(() => i.OverrideInsertDate), startDate));
                    insertDateAnd.Add(Restrictions.Ge(Projections.Property(() => i.InsertDate), startDate));
                }

                if (endDate.HasValue)
                {
                    overrideInsertDateAnd.Add(Restrictions.Le(Projections.Property(() => i.OverrideInsertDate), endDate));
                    insertDateAnd.Add(Restrictions.Le(Projections.Property(() => i.InsertDate), endDate));
                }

                restrictionsOr.Add(overrideInsertDateAnd);
                restrictionsOr.Add(insertDateAnd);

                query = query.Where(restrictionsOr);
            }

            IList<FinancePlanVisitInterestDueResult> results = query
                .SelectList(x => x
                    .Select(() => fpv.VisitId).WithAlias(() => result.VisitId)
                    .Select(() => i.InterestDue).WithAlias(() => result.InterestDue)
                    .Select(() => i.InsertDate).WithAlias(() => result.InsertDate)
                    .Select(() => i.OverrideInsertDate).WithAlias(() => result.OverrideInsertDate)
                    .Select(() => i.FinancePlanVisitInterestDueType).WithAlias(() => result.FinancePlanVisitInterestDueType)
                )
                .TransformUsing(Transformers.AliasToBean(typeof(FinancePlanVisitInterestDueResult)))
                .List<FinancePlanVisitInterestDueResult>();

            return results;
        }

        public IList<FinancePlanVisitInterestDueResult> GetFinancePlanVisitInterestAssessments(IList<int> visitIds)
        {
            if (visitIds.IsNullOrEmpty())
            {
                return new List<FinancePlanVisitInterestDueResult>();
            }

            FinancePlanVisitInterestDue i = null;
            FinancePlanVisit fpv = null;
            FinancePlanVisitInterestDueResult result = null;

            IList<FinancePlanVisitInterestDueResult> results = this.Session.QueryOver(() => i)
                .JoinAlias(() => i.FinancePlanVisit, () => fpv)
                .Where(Restrictions.In(Projections.Property(() => fpv.VisitId), visitIds.ToList()))
                .Where(() => i.FinancePlanVisitInterestDueType == FinancePlanVisitInterestDueTypeEnum.InterestAssessment)
                .SelectList(x => x
                    .Select(() => fpv.VisitId).WithAlias(() => result.VisitId)
                    .Select(() => i.InterestDue).WithAlias(() => result.InterestDue)
                    .Select(() => i.InsertDate).WithAlias(() => result.InsertDate)
                    .Select(() => i.OverrideInsertDate).WithAlias(() => result.OverrideInsertDate)
                    .Select(() => i.FinancePlanVisitInterestDueType).WithAlias(() => result.FinancePlanVisitInterestDueType)
                )
                .TransformUsing(Transformers.AliasToBean(typeof(FinancePlanVisitInterestDueResult)))
                .List<FinancePlanVisitInterestDueResult>();

            return results;
        }

        public IList<FinancePlanVisitPrincipalAmountResult> GetFinancePlanVisitPrincipalAmounts(IList<int> visitIds, IList<FinancePlanStatusEnum> checkStatuses)
        {
            if (visitIds.IsNullOrEmpty())
            {
                return new List<FinancePlanVisitPrincipalAmountResult>();
            }

            FinancePlanVisitPrincipalAmount fpvpa = null;
            FinancePlanVisit fpv = null;
            FinancePlan fp = null;
            FinancePlanVisitPrincipalAmountResult result = null;

            IQueryOver<FinancePlanVisitPrincipalAmount, FinancePlanVisitPrincipalAmount> query = this.Session.QueryOver(() => fpvpa)
                .JoinAlias(() => fpvpa.FinancePlanVisit, () => fpv)
                .JoinAlias(() => fpvpa.FinancePlan, () => fp)
                .Where(Restrictions.In(Projections.Property(() => fpv.VisitId), visitIds.ToList()))
                .Where(Restrictions.IsNull(Projections.Property(() => fpv.HardRemoveDate)));

            if (checkStatuses != null)
            {
                query = query.Where(Restrictions.In(Projections.Property(() => fp.CurrentFinancePlanStatus.FinancePlanStatusId), checkStatuses.Cast<int>().ToList()));
            }

            IList<FinancePlanVisitPrincipalAmountResult> results = query
                .SelectList(x => x
                    .Select(() => fpv.VisitId).WithAlias(() => result.VisitId)
                    .Select(() => fpvpa.Amount).WithAlias(() => result.Amount)
                    .Select(() => fpvpa.InsertDate).WithAlias(() => result.InsertDate)
                )
                .TransformUsing(Transformers.AliasToBean(typeof(FinancePlanVisitPrincipalAmountResult)))
                .List<FinancePlanVisitPrincipalAmountResult>();

            return results;
        }

        public FinancePlan GetFinancePlanByPublicIdPhi(string publicId)
        {
            IQueryOver<FinancePlan> query =
                this.Session.QueryOver<FinancePlan>()
                    .Where(fp => fp.FinancePlanPublicIdPhi == publicId)
                    .Take(1);

            FinancePlan financePlan = query.List().FirstOrDefault();
            return financePlan;
        }

        public IList<FinancePlan> GetFinancePlans(int vpGuarantorId)
        {
            IList<FinancePlan> financePlans =
                this.Session.QueryOver<FinancePlan>()
                    .Where(fp => fp.VpGuarantor.VpGuarantorId == vpGuarantorId)
                    .List();

            return financePlans;
        }

        public bool PublicIdPhiExists(string financePlanPublicIdPhi)
        {
            bool exists = 
                this.Session.QueryOver<FinancePlan>()
                    .Where(fp => fp.FinancePlanPublicIdPhi == financePlanPublicIdPhi)
                    .RowCount() > 0;

            return exists;
        }

        /*public IList<FinancePlanVisitPeriodTotal> GetVisitsOnFinancePlansWithTotalNonPaymentTransactionsForDateRange(int guarantorId, DateTime startingDate, DateTime endingDate)
        {
            IProjection transactionInsertDate = Projections.SqlFunction("COALESCE"
                    , NHibernateUtil.DateTime
                    , Projections.Property("t.OverrideInsertDate")
                    , Projections.Property("t.InsertDate")
                );

            // todo: references visit
            ICriteria criteria = this.Session.CreateCriteria<FinancePlanVisit>("fpv")
                .CreateCriteria("fpv.Visit", "visit")
                .CreateCriteria("visit.Transactions", "t")
                .CreateCriteria("t.VisitTransactionAmount", "ta")
                .Add(Restrictions.Lt(transactionInsertDate, endingDate))
                .Add(Restrictions.Ge(transactionInsertDate, startingDate))
                .Add(Restrictions.In("t.VpTransactionType.VpTransactionTypeId", new List<VpTransactionTypeEnum>
                {
                    VpTransactionTypeEnum.HsCharge,
                    VpTransactionTypeEnum.HsCharity,
                    VpTransactionTypeEnum.HsPatientNonCash,
                    VpTransactionTypeEnum.HsPayorCash,
                    VpTransactionTypeEnum.HsPayorNonCash,
                    VpTransactionTypeEnum.OtherTransaction
                }))
                .Add(Restrictions.Eq("visit.VPGuarantor.VpGuarantorId", guarantorId))
                .SetProjection(Projections.ProjectionList()
                    .Add(Projections.Alias(Projections.GroupProperty("visit.VisitId"), "VisitId"))
                    .Add(Projections.Alias(Projections.GroupProperty("fpv.FinancePlan.FinancePlanId"), "FinancePlanId"))
                    .Add(Projections.Alias(Projections.Sum("ta.TransactionAmount"), "TotalTransactionAmount"))
                )
                //Cant to restrictions on Alias projections apparently.
                //.Add(Restrictions.Lt("TotalTransactionAmount", 0))
                .SetResultTransformer(Transformers.AliasToBean<FinancePlanVisitPeriodTotal>());

            return criteria.List<FinancePlanVisitPeriodTotal>();
        }*/

    }
}
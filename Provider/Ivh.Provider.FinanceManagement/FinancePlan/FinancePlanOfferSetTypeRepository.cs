﻿namespace Ivh.Provider.FinanceManagement.FinancePlan
{
    using System.Linq;
    using Common.Data;
    using Common.Data.Connection.Connections;
    using Common.Data.Interfaces;
    using Domain.FinanceManagement.FinancePlan.Entities;
    using Domain.FinanceManagement.FinancePlan.Interfaces;
    using NHibernate;
    using NHibernate.Linq;

    public class FinancePlanOfferSetTypeRepository : RepositoryBase<FinancePlanOfferSetType, VisitPay>, IFinancePlanOfferSetTypeRepository
    {
        public FinancePlanOfferSetTypeRepository(ISessionContext<VisitPay> sessionContext)
            : base(sessionContext)
        {
        }

        public override IQueryable<FinancePlanOfferSetType> GetQueryable()
        {
            return base.GetQueryable().WithOptions(x =>
            {
                x.SetCacheable(true);
                x.SetCacheMode(CacheMode.Normal);
            });
        }
    }
}
﻿namespace Ivh.Provider.FinanceManagement.FinancePlan
{
    using Common.Data;
    using Common.Data.Connection.Connections;
    using Common.Data.Interfaces;
    using Domain.FinanceManagement.FinancePlan.Entities;
    using Domain.FinanceManagement.FinancePlan.Interfaces;
    using NHibernate;

    public class FinancePlanAmountDueRepository : RepositoryBase<FinancePlanAmountDue, VisitPay>, IFinancePlanAmountDueRepository
    {
        public FinancePlanAmountDueRepository(ISessionContext<VisitPay> sessionContext) : base(sessionContext)
        {

        }
    }
}


﻿namespace Ivh.Provider.FinanceManagement.FinancePlan
{
    using System;
    using Common.Data;
    using Common.Data.Connection.Connections;
    using Common.Data.Interfaces;
    using Domain.FinanceManagement.FinancePlan.Entities;
    using System.Collections.Generic;
    using System.Linq;
    using Domain.FinanceManagement.FinancePlan.Interfaces;
    using Ivh.Common.Base.Utilities.Extensions;
    using Ivh.Common.VisitPay.Enums;

    public class FinancePlanReadOnlyRepository : ReadOnlyRepositoryBase<FinancePlan, VisitPay>, IFinancePlanReadOnlyRepository
    {
        public FinancePlanReadOnlyRepository(
            IStatelessSessionContext<VisitPay> statelessSessionContext)
            : base(statelessSessionContext)
        {
        }

        public IList<FinancePlanAmountDueInfo> GetFinancePlanAmountDueInfoForDateWithoutAPaymentExactDate(DateTime dateToCheck, int? vpGuarantorId = null)
        {
            List<FinancePlanAmountDueInfo> amountDueList = this.FinancePlansWithAmountDue(dateToCheck, true, vpGuarantorId).ToList();
            List<FinancePlanAmountDueInfo> paymentNeededList = amountDueList
                .Where(x => x.HasStatement && !x.HasPaymentAttemptForStatement)
                .ToList();

            return paymentNeededList;
        }

        public IList<FinancePlanAmountDueInfo> GetFinancePlanAmountDueInfoForDateWithoutAPayment(DateTime dateToCheck, int? vpGuarantorId = null)
        {
            List<FinancePlanAmountDueInfo> amountDueList = this.FinancePlansWithAmountDue(dateToCheck, false, vpGuarantorId).ToList();
            List<FinancePlanAmountDueInfo> paymentNeededList = amountDueList
                .Where(x => x.HasStatement && !x.HasPaymentAttemptForStatement)
                .ToList();

            return paymentNeededList;
        }

        private IOrderedQueryable<FinancePlanAmountDue> GetOrderedStatementFinancePlanAmountDue(DateTime dateToCheck, int? vpGuarantorId = null)
        {
            IOrderedQueryable<FinancePlanAmountDue> query =
                (
                    from fpadStatement in this.StatelessSession.Query<FinancePlanAmountDue>()
                    where fpadStatement.VpStatementId != null
                    && dateToCheck >= (fpadStatement.OverrideDueDate ?? fpadStatement.DueDate)
                    && (vpGuarantorId == null || fpadStatement.FinancePlan.VpGuarantor.VpGuarantorId == vpGuarantorId)
                    orderby fpadStatement.FinancePlanAmountDueId descending
                    select fpadStatement
                );
            return query;
        }

        private IOrderedQueryable<FinancePlanAmountDue> GetOrderedRecurringPaymentFinancePlanAmountDue(int? vpGuarantorId = null)
        {
            IOrderedQueryable<FinancePlanAmountDue> query =
                (
                    from fpadPayment in this.StatelessSession.Query<FinancePlanAmountDue>()
                    where fpadPayment.PaymentId != null &&
                    fpadPayment.PaymentType == PaymentTypeEnum.RecurringPaymentFinancePlan &&
                    (vpGuarantorId == null || fpadPayment.FinancePlan.VpGuarantor.VpGuarantorId == vpGuarantorId)
                    orderby fpadPayment.FinancePlanAmountDueId descending
                    select fpadPayment
                );
            return query;
        }

        private IQueryable<FinancePlanAmountDueInfo> FinancePlansWithAmountDue(DateTime dateToCheck, bool isEqualTo = false, int? vpGuarantorId = null)
        {
            List<int> listOfActive = EnumExt.GetAllInCategory<FinancePlanStatusEnum>(FinancePlanStatusEnumCategory.Active).Select(x => (int)x).ToList();

            IQueryable<FinancePlanAmountDueInfo> queryFinancePlanTotal =
                from fpad in this.StatelessSession.Query<FinancePlanAmountDue>()
                // payments do not have DueDate value, statements will have a DueDate value and possible override in OverrideDueDate
                where (
                          (isEqualTo && (fpad.OverrideDueDate ?? fpad.DueDate) == dateToCheck) // statement amounts due on dateToCheck if requested (isEqualTo)
                          || (!isEqualTo && (fpad.OverrideDueDate ?? fpad.DueDate) <= dateToCheck) // statement amounts due on or before dateToCheck if requested (isEqualTo)
                          || fpad.DueDate == null // all payment amounts
                      ) &&
                      listOfActive.Contains(fpad.FinancePlan.CurrentFinancePlanStatus.FinancePlanStatusId) &&
                      (vpGuarantorId == null || fpad.FinancePlan.VpGuarantor.VpGuarantorId == vpGuarantorId)

                group fpad by new
                {
                    fpad.FinancePlan.FinancePlanId,
                    fpad.FinancePlan.VpGuarantor.VpGuarantorId,
                } into financePlanGroup

                select new FinancePlanAmountDueInfo
                {
                    VpGuarantorId = financePlanGroup.Key.VpGuarantorId,
                    FinancePlanId = financePlanGroup.Key.FinancePlanId,
                    TotalAmountDue = financePlanGroup.Sum(x => x.AmountDue),
                    AsOfDate = dateToCheck,
                    StatementFinancePlanAmountDueId = (
                        from fpadStatement in this.GetOrderedStatementFinancePlanAmountDue(dateToCheck, vpGuarantorId)
                        where fpadStatement.FinancePlan.FinancePlanId == financePlanGroup.Key.FinancePlanId
                        select fpadStatement.FinancePlanAmountDueId
                        ).FirstOrDefault(),
                    StatementFinancePlanAmountDueDueDate = (
                        from fpadStatement in this.GetOrderedStatementFinancePlanAmountDue(dateToCheck, vpGuarantorId)
                        where fpadStatement.FinancePlan.FinancePlanId == financePlanGroup.Key.FinancePlanId
                        select fpadStatement.OverrideDueDate ?? fpadStatement.DueDate
                    ).FirstOrDefault(),
                    StatementId = (
                        from fpadStatement in this.GetOrderedStatementFinancePlanAmountDue(dateToCheck, vpGuarantorId)
                        where fpadStatement.FinancePlan.FinancePlanId == financePlanGroup.Key.FinancePlanId
                        select fpadStatement.VpStatementId
                    ).FirstOrDefault(),
                    PaymentFinancePlanAmountDueId = (
                        from fpadPayment in this.GetOrderedRecurringPaymentFinancePlanAmountDue(vpGuarantorId)
                        where fpadPayment.FinancePlan.FinancePlanId == financePlanGroup.Key.FinancePlanId
                        select fpadPayment.FinancePlanAmountDueId
                        ).FirstOrDefault(),
                    PaymentFinancePlanAmountDueInsertDate = (
                        from fpadPayment in this.GetOrderedRecurringPaymentFinancePlanAmountDue(vpGuarantorId)
                        where fpadPayment.FinancePlan.FinancePlanId == financePlanGroup.Key.FinancePlanId
                        select fpadPayment.InsertDate
                    ).FirstOrDefault()
                };

            return queryFinancePlanTotal.Where(x => x.TotalAmountDue > 0);
        }       
    }
}

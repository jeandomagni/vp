﻿namespace Ivh.Provider.FinanceManagement.FinancePlan
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Common.Cache;
    using Common.Data;
    using Common.Data.Connection.Connections;
    using Common.Data.Interfaces;
    using Domain.FinanceManagement.FinancePlan.Entities;
    using Domain.FinanceManagement.FinancePlan.Interfaces;
    using NHibernate;
    using NHibernate.Criterion;

    public class FinancePlanOfferRepository : RepositoryBase<FinancePlanOffer, VisitPay>, IFinancePlanOfferRepository
    {
        private readonly IDistributedCache _cache;

        public FinancePlanOfferRepository(ISessionContext<VisitPay> sessionContext, IDistributedCache cache)
            : base(sessionContext)
        {
            this._cache = cache;
        }
        public FinancePlanOffer ManagedInsert(FinancePlanOffer entity)
        {
            FinancePlanOffer connectedEntity = null;
            if (entity.StatementedCurrentAccountBalance <= 0)
            {
                //Dont save anything if they dont have a balance to generate a FP.
                return null;
            }
            string key = $"FinancePlanOfferRepository::Insert({entity.VpGuarantor.VpGuarantorId},{entity.InterestRate},{entity.StatementedCurrentAccountBalance},{entity.MinPayment},{entity.MaxPayment},{entity.DurationRangeStart},{entity.DurationRangeEnd},{entity.FinancePlanOfferSetType.FinancePlanOfferSetTypeId})";
            string check = this._cache.GetString(key);

            int id = -1;
            if (int.TryParse(check, out id))
            {
                connectedEntity = base.GetById(id);
                if (connectedEntity == null)
                {
                    //When that entity doesnt actually exist make it insert
                    id = -1;
                }
            }

            if (check == null || id == -1)
            {
                //Only insert if this isnt in the cache, dont want to spam the db.
                base.InsertOrUpdate(entity);
                connectedEntity = entity;
                this._cache.SetString(key, entity.FinancePlanOfferId.ToString(), 60);
            }
            return connectedEntity;
        }

        public IList<FinancePlanOffer> GetOffersWithCorrelationGuid(int vpGuarantorId, Guid guid)
        {
            ICriteria criteria = this.Session.CreateCriteria<FinancePlanOffer>()
                .Add(Restrictions.Eq("CorrelationGuid", guid))
                .Add(Restrictions.Eq("VpGuarantor.VpGuarantorId", vpGuarantorId));
                
            return criteria.List<FinancePlanOffer>();

        }
    }
}
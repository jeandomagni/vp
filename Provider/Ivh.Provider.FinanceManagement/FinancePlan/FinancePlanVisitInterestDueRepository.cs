﻿namespace Ivh.Provider.FinanceManagement.FinancePlan
{
    using System;
    using System.Linq;
    using Common.Data;
    using Common.Data.Connection.Connections;
    using Common.Data.Interfaces;
    using Domain.FinanceManagement.FinancePlan.Entities;
    using Domain.FinanceManagement.FinancePlan.Interfaces;
    using NHibernate;

    public class FinancePlanVisitInterestDueRepository : RepositoryBase<FinancePlanVisitInterestDue, VisitPay>, IFinancePlanVisitInterestDueRepository
    {
        public FinancePlanVisitInterestDueRepository(ISessionContext<VisitPay> sessionContext) : base(sessionContext)
        {
        }

        public decimal GetInterestDueForStatementFinancePlan(int vpStatementId, int financePlanId)
        {
            decimal interestDue = this.GetQueryable()
                .Where(x => x.FinancePlan.FinancePlanId == financePlanId && x.VpStatementId == vpStatementId)
                .Sum(x => (decimal?)x.InterestDue) ?? 0m;
            return interestDue;
        }
    }
}

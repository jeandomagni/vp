﻿
namespace Ivh.Provider.FinanceManagement.Statement
{
    using Common.Data;
    using Common.Data.Connection.Connections;
    using Common.Data.Interfaces;
    using Domain.FinanceManagement.Statement.Entities;
    using Domain.FinanceManagement.Statement.Interfaces;

    public class VpStatementDocumentRepository : RepositoryBase<VpStatementDocument, VisitPay>, IVpStatementDocumentRepository
    {
        public VpStatementDocumentRepository(ISessionContext<VisitPay> sessionContext) : base(sessionContext)
        {
        }
    }
}

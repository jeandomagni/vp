﻿namespace Ivh.Provider.FinanceManagement.Statement
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Common.Base.Utilities.Extensions;
    using Common.Data;
    using Common.Data.Connection.Connections;
    using Common.Data.Interfaces;
    using Common.VisitPay.Enums;
    using Domain.FinanceManagement.Statement.Entities;
    using Domain.FinanceManagement.Statement.Interfaces;
    using Domain.Guarantor.Entities;
    using Domain.Visit.Entities;
    using NHibernate;
    using NHibernate.Criterion;
    using NHibernate.Linq;
    using NHibernate.Transform;

    public class VpStatementRepository : RepositoryBase<VpStatement, VisitPay>, IVpStatementRepository
    {
        public VpStatementRepository(ISessionContext<VisitPay> sessionContext) : base(sessionContext)
        {
        }

        public IList<int> GetAllPendingStatementGuarantors()
        {
            return new List<int>();
        }

        public VpStatement GetVpStatement(int vpGuarantorId, int vpStatementId)
        {
            VpStatement vpStatement = this.GetById(vpStatementId);
            if (vpStatement != null && vpStatement.VpGuarantorId == vpGuarantorId)
            {
                return vpStatement;
            }

            return null;
        }

        public IReadOnlyList<VpStatement> GetVpStatements(int vpGuarantorId)
        {
            return this.GetRealStatements(vpGuarantorId);
        }

        public IList<VpStatement> GetStatementsForCreatingPaperStatements()
        {
            ICriteria criteria = this.Session.CreateCriteria<VpStatement>();
            criteria.Add(Restrictions.Eq("CreatePaperStatement", true));
            criteria.Add(Restrictions.IsNull("PaperStatementSentDate"));
            return criteria.List<VpStatement>();
        }

        public int GetVpStatementTotals(int vpGuarantorId)
        {
            return this.GetRealStatementsCount(vpGuarantorId);
        }

        public IList<VpStatement> GetStatementsAroundDateForGuarantor(int vpGuarantorId, DateTime dateToProcess, int dayBracket)
        {
            DateTime lowerDate = dateToProcess.AddDays(-1 * dayBracket);
            DateTime upperDate = dateToProcess.AddDays(dayBracket);

            ICriteria criteria = this.Session.CreateCriteria<VpStatement>();
            criteria.Add(Restrictions.Ge("StatementDate", lowerDate));
            criteria.Add(Restrictions.Le("StatementDate", upperDate));
            criteria.Add(Restrictions.Eq("VpGuarantorId", vpGuarantorId));
            return criteria.List<VpStatement>();
        }

        public IList<VpStatement> GetTopVpStatementByGuarantor(int vpGuarantorId, int topCount)
        {
            ICriteria criteria = this.Session.CreateCriteria<VpStatement>();
            criteria.Add(Restrictions.Eq("VpGuarantorId", vpGuarantorId));
            criteria.AddOrder(Order.Desc("StatementDate"));
            criteria.SetMaxResults(topCount);
            return criteria.List<VpStatement>();
        }

        private IOrderedQueryable<VpStatement> GetVpStatementsOrderedByStatementDate(DateTime dateTime)
        {
            IOrderedQueryable<VpStatement> query =
                from s in this.Session.Query<VpStatement>()
                where s.PaymentDueDate <= dateTime
                orderby s.StatementDate descending
                select s;
            return query;
        }

        private IQueryable<GuarantorStatementInfo> GetLatestGuarantorStatements(DateTime dateTime)
        {
            IQueryable<GuarantorStatementInfo> query = (
                from g in this.Session.Query<Guarantor>()
                where g.AccountClosedDate == null
                select new GuarantorStatementInfo()
                {
                    VpGuarantorId = g.VpGuarantorId,
                    VpStatementId = (
                        from s in this.GetVpStatementsOrderedByStatementDate(dateTime)
                        where s.VpGuarantorId == g.VpGuarantorId
                        select s.VpStatementId
                    ).FirstOrDefault(),
                    VpStatementProcessingStatus = (
                        from s in this.GetVpStatementsOrderedByStatementDate(dateTime)
                        where s.VpGuarantorId == g.VpGuarantorId
                        select s.ProcessingStatus
                    ).FirstOrDefault(),
                    AsOfDate = dateTime
                }
            );
            return query;
        }

        public IList<GuarantorStatementInfo> GetGuarantorStatementInfoToBeChargedWithDate(DateTime dateTime)
        {
            IList<GuarantorStatementInfo> statementsNeedingPayment = (
                from gs in this.GetLatestGuarantorStatements(dateTime)
                where gs.VpStatementProcessingStatus == ChangeEventStatusEnum.Unprocessed
                select gs
            ).ToList();

            return statementsNeedingPayment;
        }

        public bool DoesGuarantorHaveStatement(int vpGuarantorId)
        {
            // Force selection of primary key only and return all primary keys since we only expect one.
            // Avoiding the use of First() and Any() since we want a seek from the index on the VpGuarantorId column.
            List<int> vpStatementIds = this.GetQueryable().Where(x => x.VpGuarantorId == vpGuarantorId).Select(x => x.VpStatementId).ToList();
            return vpStatementIds.Any(); // Doing Any() in memory to allow maximum performance of SQL query
        }

        public VpStatement GetVpStatementWithDate(int vpGuarantorId, DateTime date)
        {
            ICriteria criteria = this.Session.CreateCriteria<VpStatement>();
            criteria.Add(Restrictions.Ge("StatementDate", date.Date));
            criteria.Add(Restrictions.Le("PaymentDueDate", date.Date));
            criteria.Add(Restrictions.Eq("VpGuarantorId", vpGuarantorId));
            return criteria.UniqueResult<VpStatement>();
        }

        public bool IsVisitOnHardStatement(int visitId, DateTime startDate, DateTime endDate)
        {
            VpStatementVisit sv = null;

            DateTime startDateToCheck = startDate.Date;
            DateTime endDateToCheck = endDate.Date;

            int statementId = this.Session.QueryOver<VpStatement>()
                .JoinAlias(s => s.VpStatementVisits, () => sv)
                .Where(s => s.PeriodStartDate.Date >= startDateToCheck && s.PeriodEndDate.Date <= endDateToCheck && sv.Visit.VisitId == visitId && sv.DeletedDate == null)
                .Select(s => s.VpStatementId)
                .Take(1)
                .SingleOrDefault<int>();

            return statementId != 0;
        }

        private ICriteria GetMostRecentStatementCriteria(IList<int> vpGuarantorIds)
        {
            DetachedCriteria subQuery = DetachedCriteria.For<VpStatement>("s2")
                .Add(Restrictions.EqProperty("s2.VpGuarantorId", "s1.VpGuarantorId"))
                .AddOrder(new Order("StatementDate", false))
                .SetMaxResults(1)
                .SetProjection(Projections.Property("VpStatementId"));

            ICriteria criteria = this.Session.CreateCriteria<VpStatement>("s1");
            criteria.Add(Restrictions.In("VpGuarantorId", vpGuarantorIds.ToList()));
            criteria.Add(Subqueries.PropertyIn("VpStatementId", subQuery));

            return criteria;
        }

        private VpStatementForProcessPaymentResult GetMostRecentStatementForProcessPaymentResult(int vpGuarantorId)
        {
            DetachedCriteria subQuery = DetachedCriteria.For<VpStatement>("s2")
                .Add(Restrictions.EqProperty("s2.VpGuarantorId", "s1.VpGuarantorId"))
                .AddOrder(new Order("StatementDate", false))
                .SetMaxResults(1)
                .SetProjection(Projections.Property("VpStatementId"));

            ICriteria criteria = this.Session.CreateCriteria<VpStatement>("s1");
            criteria.Add(Restrictions.Eq("VpGuarantorId", vpGuarantorId));
            criteria.Add(Subqueries.PropertyIn("VpStatementId", subQuery));

            criteria.SetProjection(Projections.ProjectionList()
                    .Add(Projections.Property(nameof(VpStatement.VpStatementId)), nameof(VpStatementForProcessPaymentResult.VpStatementId))
                    .Add(Projections.Property(nameof(VpStatement.PaymentDueDate)), nameof(VpStatementForProcessPaymentResult.PaymentDueDate))
                    .Add(Projections.Property(nameof(VpStatement.ProcessingStatus)), nameof(VpStatementForProcessPaymentResult.ProcessingStatus)))
                .SetResultTransformer(Transformers.AliasToBean(typeof(VpStatementForProcessPaymentResult)));

            return criteria.UniqueResult<VpStatementForProcessPaymentResult>();
        }

        public VpStatement GetMostRecentStatement(Guarantor guarantor)
        {
            if (guarantor.MostRecentVpStatementId.HasValue)
            {
                VpStatement vpStatement = this.GetById(guarantor.MostRecentVpStatementId.Value);
                if (vpStatement != null && vpStatement.VpGuarantorId == guarantor.VpGuarantorId)
                {
                    return vpStatement;
                }
            }

            return this.GetMostRecentStatementCriteria(guarantor.VpGuarantorId.ToListOfOne())
                //Todo: Need distributed second level cache.
                //.SetCacheable(true)
                //.SetCacheMode(CacheMode.Normal)
                .List<VpStatement>().FirstOrDefault();
        }

        public VpStatementForProcessPaymentResult GetMostRecentStatementForProcessPayment(Guarantor guarantor)
        {
            if (guarantor.MostRecentVpStatementId.HasValue)
            {
                VpStatementForProcessPaymentResult result = this.GetVpStatementForProcessPaymentResult(guarantor.VpGuarantorId, guarantor.MostRecentVpStatementId.Value);
                return result;
            }

            return this.GetMostRecentStatementForProcessPaymentResult(guarantor.VpGuarantorId);
        }

        public VpStatementForProcessPaymentResult GetVpStatementForProcessPaymentResult(int vpGuarantorId, int vpStatementId)
        {
            VpStatementForProcessPaymentResult resultAlias = null;
            VpStatementForProcessPaymentResult result = this.Session.QueryOver<VpStatement>()
                .Where(x => x.VpStatementId == vpStatementId)
                .And(x => x.VpGuarantorId == vpGuarantorId)
                .SelectList(list => list
                    .Select(x => x.VpStatementId).WithAlias(() => resultAlias.VpStatementId)
                    .Select(x => x.VpGuarantorId).WithAlias(() => resultAlias.VpGuarantorId)
                    .Select(x => x.PaymentDueDate).WithAlias(() => resultAlias.PaymentDueDate)
                    .Select(x => x.ProcessingStatus).WithAlias(() => resultAlias.ProcessingStatus)
                )
                .TransformUsing(Transformers.AliasToBean(typeof(VpStatementForProcessPaymentResult)))
                .SingleOrDefault<VpStatementForProcessPaymentResult>();

            return result;
        }

        public bool HasMostRecentStatement(IList<int> vpGuarantorIds)
        {
            ICriteria criteria = this.GetMostRecentStatementCriteria(vpGuarantorIds);
            return criteria.SetProjection(Projections.Count(Projections.Id())).UniqueResult<int>() > 0;
        }

        public IList<VpStatementQueueStatusResult> GetMostRecentStatementQueueStatus(IList<int> vpGuarantorIds)
        {
            DetachedCriteria subQuery = DetachedCriteria.For<VpStatement>("s2")
                .Add(Restrictions.EqProperty("s2.VpGuarantorId", "s1.VpGuarantorId"))
                .AddOrder(new Order("StatementDate", false))
                .SetMaxResults(1)
                .SetProjection(Projections.Property("VpStatementId"));

            ICriteria criteria = this.Session.CreateCriteria<VpStatement>("s1");
            criteria.Add(Restrictions.In("VpGuarantorId", vpGuarantorIds.ToList()));
            criteria.Add(Subqueries.PropertyIn("VpStatementId", subQuery));
            criteria.SetProjection(Projections.ProjectionList()
                .Add(Projections.Property("s1.VpStatementId"), "VpStatementId")
                .Add(Projections.Property("s1.VpGuarantorId"), "VpGuarantorId")
                .Add(Projections.Property("s1.ProcessingStatus"), "ProcessingStatus")
                );
            criteria.SetResultTransformer(Transformers.AliasToBean(typeof(VpStatementQueueStatusResult)));

            return criteria.List<VpStatementQueueStatusResult>();
        }

        public IList<PendingUncollectableStatementsResult> GetAllPendingUncollectableStatements(int maxAgingCountFinancePlans, DateTime statementDueDate)
        {
            IQuery query = this.Session.CreateSQLQuery("EXEC dbo.GetAllPendingUncollectableStatements @maxAgingTierVisits =:MaxAgingTierVisits, @maxAgingCountFinancePlans =:MaxAgingCountFinancePlans,@statementDueDate =:StatementDueDate");

            query.SetParameter("MaxAgingTierVisits", (int)AgingTierEnum.MaxAge);
            query.SetParameter("MaxAgingCountFinancePlans", maxAgingCountFinancePlans);
            query.SetParameter("StatementDueDate", statementDueDate);
            query.SetResultTransformer(Transformers.AliasToBean(typeof(PendingUncollectableStatementsResult)));

            return query.List<PendingUncollectableStatementsResult>();
        }

        public IList<int> GuarantorsIdsWhoHaventHadUncollectableEmailForCurrentStatement(IList<int> guarantors)
        {
            DetachedCriteria subQuery2 = DetachedCriteria.For<VpStatement>("s2")
                .Add(Restrictions.EqProperty("s2.VpGuarantorId", "s1.VpGuarantorId"))
                .AddOrder(new Order("StatementDate", false))
                .SetMaxResults(1)
                .SetProjection(Projections.Property("VpStatementId"));

            DetachedCriteria subQuery1 = DetachedCriteria.For<VpStatement>("s1")
                .Add(Restrictions.In("VpGuarantorId", guarantors.ToList()))
                .Add(Restrictions.EqProperty("s1.VpGuarantorId", "g.VpGuarantorId"))
                .Add(Restrictions.Or(Restrictions.Eq("UncollectableEmailSent", false), Restrictions.IsNull("UncollectableEmailSent")))
                .Add(Subqueries.PropertyIn("VpStatementId", subQuery2))
                .SetProjection(Projections.Property("VpGuarantorId"));

            ICriteria criteria = this.Session.CreateCriteria<Guarantor>("g")
                .Add(Restrictions.IsNull("g.AccountClosedDate"))
                .Add(Subqueries.PropertyIn("g.VpGuarantorId", subQuery1))
                .SetProjection(Projections.Property("g.VpGuarantorId"));
            return criteria.List<int>();
        }

        public IList<int> GuarantorsIdsWhoHaventHadPastDueEmailForCurrentStatement(IList<int> guarantors)
        {
            DetachedCriteria subQuery2 = DetachedCriteria.For<VpStatement>("s2")
                .Add(Restrictions.EqProperty("s2.VpGuarantorId", "s1.VpGuarantorId"))
                .AddOrder(new Order("StatementDate", false))
                .SetMaxResults(1)
                .SetProjection(Projections.Property("VpStatementId"));

            DetachedCriteria subQuery1 = DetachedCriteria.For<VpStatement>("s1")
                .Add(Restrictions.In("VpGuarantorId", guarantors.ToList()))
                .Add(Restrictions.EqProperty("s1.VpGuarantorId", "g.VpGuarantorId"))
                .Add(Restrictions.Or(Restrictions.Eq("PastDueEmailSent", false), Restrictions.IsNull("PastDueEmailSent")))
                .Add(Subqueries.PropertyIn("VpStatementId", subQuery2))
                .SetProjection(Projections.Property("VpGuarantorId"));

            ICriteria criteria = this.Session.CreateCriteria<Guarantor>("g")
                .Add(Restrictions.IsNull("AccountClosedDate"))
                .Add(Subqueries.PropertyIn("g.VpGuarantorId", subQuery1))
                .SetProjection(Projections.Property("g.VpGuarantorId"));
            return criteria.List<int>();
        }

        public IList<VisitOnStatementResult> GetVisitOnStatementResultsForVisits(IList<Visit> visitsForNewStatement)
        {
            ICriteria criteria = this.Session.CreateCriteria<VpStatementVisit>("sv");
            criteria.Add(Restrictions.In("sv.Visit.VisitId", visitsForNewStatement.Select(x => x.VisitId).ToList()));
            criteria.SetProjection(Projections.ProjectionList()
                                .Add(Projections.Property("sv.Visit.VisitId"), "VisitId")
                                .Add(Projections.Property("sv.VpStatement.VpStatementId"), "VpStatementId")
                                .Add(Projections.Property("sv.InsertDate"), "InsertDate")
                                )
                .SetResultTransformer(Transformers.AliasToBean(typeof(VisitOnStatementResult)));

            return criteria.List<VisitOnStatementResult>();
        }

        public IList<StatementPeriodResult> GetAllStatementPeriodsForGuarantor(int vpGuarantorId)
        {
            ICriteria criteria = this.Session.CreateCriteria<VpStatement>("s");
            criteria.Add(Restrictions.Eq("s.VpGuarantorId", vpGuarantorId));
            criteria.SetProjection(Projections.ProjectionList()
                                .Add(Projections.Property("s.VpStatementId"), "VpStatementId")
                                .Add(Projections.Property("s.VpGuarantorId"), "VpGuarantorId")
                                .Add(Projections.Property("s.PeriodEndDate"), "PeriodEndDate")
                                .Add(Projections.Property("s.PeriodStartDate"), "PeriodStartDate")
                                .Add(Projections.Property("s.StatementDate"), "StatementDate")
                                .Add(Projections.Property("s.PaymentDueDate"), "PaymentDueDate")
                                )
                .SetResultTransformer(Transformers.AliasToBean(typeof(StatementPeriodResult)));

            return criteria.List<StatementPeriodResult>();
        }

        public void MarkStatementAsQueued(int statementId)
        {
            //Didnt want to load the Statment object as it's fairly expensive to load for batch operations like publishing to the bus.
            this.Session.Query<VpStatement>()
                .Where(x => x.VpStatementId == statementId)
                .UpdateBuilder()
                .Set(x => x.ProcessingStatus, ChangeEventStatusEnum.Queued)
                .Update();
        }

        public void MarkStatementAsProcessed(int statementId)
        {
            this.Session.Query<VpStatement>()
                .Where(x => x.VpStatementId == statementId)
                .UpdateBuilder()
                .Set(x => x.ProcessingStatus, ChangeEventStatusEnum.Processed)
                .Update();
        }

        public void MarkStatementAsUnprocessed(int statementId)
        {
            this.Session.Query<VpStatement>()
                .Where(x => x.VpStatementId == statementId)
                .UpdateBuilder()
                .Set(x => x.ProcessingStatus, ChangeEventStatusEnum.Unprocessed)
                .Update();
        }

        public IList<VpStatementPaymentDueDateResult> GetStatementsWithPaymentDueDateResults(DateTime dueDate)
        {
            ICriteria criteria = this.Session.CreateCriteria<VpStatement>();
            criteria.Add(Restrictions.Gt("PaymentDueDate", DateTime.UtcNow.Date));
            criteria.Add(Restrictions.Le("PaymentDueDate", dueDate.Date));
            criteria.Add(Restrictions.Eq("BalanceDueReminderMessageSent", false));
            criteria.SetProjection(Projections.ProjectionList()
                            .Add(Projections.Property("VpStatementId"), "VpStatementId")
                            .Add(Projections.Property("VpGuarantorId"), "VpGuarantorId")
                            .Add(Projections.Property("PaymentDueDate"), "PaymentDueDate")
                            .Add(Projections.Property("BalanceDueReminderMessageSent"), "BalanceDueReminderMessageSent")
                            )
                .SetResultTransformer(Transformers.AliasToBean(typeof(VpStatementPaymentDueDateResult)));
            return criteria.List<VpStatementPaymentDueDateResult>();
        }

        public byte[] GetStatementContent(int vpGuarantorId, int vpStatementId)
        {
            ISQLQuery query = this.Session.CreateSQLQuery("SELECT TOP 1 ArchivedFileContent FROM dbo.VpStatement WHERE VpGuarantorId = ? AND VpStatementId = ?")
                .AddScalar("ArchivedFileContent", NHibernateUtil.Binary);

            query.SetInt32(0, vpGuarantorId);
            query.SetInt32(1, vpStatementId);

            byte[] bytes = (byte[])query.UniqueResult();

            return bytes;
        }

        public int GetCurrentStatementIdForDate(DateTime paymentActualPaymentDate, int vpGuarantorId)
        {
            ICriteria criteria = this.Session.CreateCriteria<VpStatement>();
            criteria.Add(Restrictions.Lt("StatementDate", paymentActualPaymentDate))
                .Add(Restrictions.Eq("VpGuarantorId", vpGuarantorId))
                .AddOrder(new Order("StatementDate", false))
                .SetMaxResults(1);
            criteria.SetProjection(Projections.ProjectionList()
                    .Add(Projections.Property("VpStatementId"))
            );
            return criteria.UniqueResult<int>();
        }

        private IReadOnlyList<VpStatement> GetRealStatements(int vpGuarantorId)
        {
            return (IReadOnlyList<VpStatement>)this.QueryOverRealStatements(vpGuarantorId).List();
        }

        private int GetRealStatementsCount(int vpGuarantorId)
        {
            return this.QueryOverRealStatements(vpGuarantorId).RowCount();
        }

        private IQueryOver<VpStatement> QueryOverRealStatements(int vpGuarantorId)
        {
            return this.Session.QueryOver<VpStatement>()
                .Where(s => s.VpGuarantorId == vpGuarantorId &&
                            s.VpStatementType != VpStatementTypeEnum.MigratedManagedUser);
        }
    }
}
﻿namespace Ivh.Provider.FinanceManagement.Statement
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Common.Base.Utilities.Extensions;
    using Common.Data;
    using Common.Data.Connection.Connections;
    using Common.Data.Interfaces;
    using Domain.FinanceManagement.Statement.Entities;
    using Domain.FinanceManagement.Statement.Interfaces;
    using Domain.Guarantor.Entities;
    using Ivh.Common.VisitPay.Messages.FinanceManagement;
    using NHibernate;
    using NHibernate.Criterion;
    using NHibernate.Transform;

    public class FinancePlanStatementRepository : RepositoryBase<FinancePlanStatement, VisitPay>, IFinancePlanStatementRepository
    {
        public FinancePlanStatementRepository(ISessionContext<VisitPay> sessionContext) : base(sessionContext)
        {
        }

        public FinancePlanStatement GetMostRecentStatement(Guarantor guarantor)
        {
            /*
            //Should probably do something like this
            if (guarantor.MostRecentVpStatementId.HasValue)
            {
                VpStatement vpStatement = this.GetById(guarantor.MostRecentVpStatementId.Value);
                if (vpStatement != null && vpStatement.VpGuarantorId == guarantor.VpGuarantorId)
                {
                    return vpStatement;
                }
            }
            */
            return this.GetMostRecentStatementCriteria(guarantor.VpGuarantorId.ToListOfOne())
                .List<FinancePlanStatement>().FirstOrDefault();
        }

        public FinancePlanStatement GetFinancePlanStatementByPeriodStartDate(int guarantorId, DateTime periodStartDate)
        {
            DateTime beginDate = periodStartDate.Date;
            DateTime endDate = new DateTime(periodStartDate.Year, periodStartDate.Month, periodStartDate.Day, 23, 59, 59);
            endDate = endDate.AddMilliseconds(999);

            FinancePlanStatement financePlanStatementAlias = null;
            FinancePlanStatement result = this.Session.QueryOver(() => financePlanStatementAlias)
                .Where(Restrictions.Eq(Projections.Property(() => financePlanStatementAlias.VpGuarantorId), guarantorId))
                .Where(Restrictions.Between(Projections.Property(() => financePlanStatementAlias.PeriodStartDate), beginDate, endDate))
                .SingleOrDefault<FinancePlanStatement>();
            return result;
        }

        public FinancePlanStatement GetFinancePlanStatementByVpStatementId(int guarantorId, int vpStatementId)
        {
            //ICriteria criteria = this.Session.CreateCriteria<FinancePlanStatement>();
            //criteria.Add(Restrictions.Eq("VpGuarantorId", guarantorId));
            //criteria.Add(Restrictions.Eq("VpStatementId", vpStatementId));
            //return criteria.UniqueResult<FinancePlanStatement>();
            FinancePlanStatement financePlanStatementAlias = null;
            FinancePlanStatement result = this.Session.QueryOver(() => financePlanStatementAlias)
                .Where(Restrictions.Eq(Projections.Property(() => financePlanStatementAlias.VpGuarantorId), guarantorId))
                .Where(Restrictions.Eq(Projections.Property(() => financePlanStatementAlias.VpStatementId), vpStatementId))
                .SingleOrDefault<FinancePlanStatement>();
            return result;
        }

        public IReadOnlyList<FinancePlanStatement> GetFinancePlanStatements(int vpGuarantorId)
        {
            return (IReadOnlyList<FinancePlanStatement>)this.Session.QueryOver<FinancePlanStatement>()
                .Where(s => s.VpGuarantorId == vpGuarantorId)
                .List();
        }

        private ICriteria GetMostRecentStatementCriteria(IList<int> vpGuarantorIds)
        {
            DetachedCriteria subQuery = DetachedCriteria.For<FinancePlanStatement>("fps2")
                .Add(Restrictions.EqProperty("fps2.VpGuarantorId", "fps1.VpGuarantorId"))
                .AddOrder(new Order("StatementDate", false))
                .SetMaxResults(1)
                .SetProjection(Projections.Property("FinancePlanStatementId"));

            ICriteria criteria = this.Session.CreateCriteria<FinancePlanStatement>("fps1");
            criteria.Add(Restrictions.In("VpGuarantorId", vpGuarantorIds.ToList()));
            criteria.Add(Subqueries.PropertyIn("FinancePlanStatementId", subQuery));

            return criteria;
        }

        public IList<FinancePlanStatement> GetFinancePlanStatementByVpStatementIds(IList<int> vpStatementIds)
        {
            List<FinancePlanStatement> financePlanStatementList = new List<FinancePlanStatement>();

            foreach (IEnumerable<int> chunk in vpStatementIds.Chunk(100))
            {
                IList<FinancePlanStatement> chunkedFinancePlanStatementList = this.Session.QueryOver<FinancePlanStatement>()
                    .WhereRestrictionOn(x => x.VpStatementId).IsIn(chunk.ToList())
                    .List();
                financePlanStatementList.AddRange(chunkedFinancePlanStatementList);
            }

            return financePlanStatementList;
        }

        [Obsolete("Temporary code only needed until 7/14/2019 for VP-6889")]
        public InterestEventMessageList FixMissingInterest(int vpStatementId)
        {
            IQuery sqlQuery = this.Session.CreateSQLQuery(@"EXEC migration.FixMissingInterest :VpStatementId")
            .SetParameter("VpStatementId", vpStatementId);

            IList<InterestEventMessage> messages =  sqlQuery.SetResultTransformer(Transformers.AliasToBean(typeof(InterestEventMessage))).List<InterestEventMessage>();
            InterestEventMessageList messageList = new InterestEventMessageList(messages);
            return messageList;
        }
    }
}
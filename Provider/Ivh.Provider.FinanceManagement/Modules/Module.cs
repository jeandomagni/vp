﻿namespace Ivh.Provider.FinanceManagement.Modules
{
    using Autofac;
    using Consolidation;
    using Discount;
    using Domain.FinanceManagement.Consolidation.Interfaces;
    using Domain.FinanceManagement.Discount.Interfaces;
    using Domain.FinanceManagement.FinancePlan.Interfaces;
    using Domain.FinanceManagement.Interfaces;
    using Domain.FinanceManagement.Payment.Interfaces;
    using Domain.FinanceManagement.ServiceGroup.Interfaces;
    using FinancePlan;
    using Payment;
    using ServiceGroup;

    public class Module : Autofac.Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<ConsolidationGuarantorRepository>().As<IConsolidationGuarantorRepository>();
            builder.RegisterType<DiscountBalanceTierRepository>().As<IDiscountBalanceTierRepository>();
            builder.RegisterType<InterestRateRepository>().As<IInterestRateRepository>();
            builder.RegisterType<FinancePlanMinimumPaymentTierRepository>().As<IFinancePlanMinimumPaymentTierRepository>();
            builder.RegisterType<FinancePlanOfferRepository>().As<IFinancePlanOfferRepository>();
            builder.RegisterType<FinancePlanOfferSetTypeRepository>().As<IFinancePlanOfferSetTypeRepository>();
            builder.RegisterType<FinancePlanRepository>().As<IFinancePlanRepository>();
            builder.RegisterType<FinancePlanReadOnlyRepository>().As<IFinancePlanReadOnlyRepository>();
            builder.RegisterType<PaymentActionHistoryRepository>().As<IPaymentActionHistoryRepository>();
            builder.RegisterType<PaymentActionReasonRepository>().As<IPaymentActionReasonRepository>();
            builder.RegisterType<PaymentEventRepository>().As<IPaymentEventRepository>();
            builder.RegisterType<PaymentMenuRepository>().As<IPaymentMenuRepository>();
            builder.RegisterType<PaymentMethodsRepository>().As<IPaymentMethodsRepository>();
            builder.RegisterType<PaymentMethodAccountTypeRepository>().As<IPaymentMethodAccountTypeRepository>();
            builder.RegisterType<PaymentMethodProviderTypeRepository>().As<IPaymentMethodProviderTypeRepository>();
            builder.RegisterType<PaymentMethodEventRepository>().As<IPaymentMethodEventRepository>();
            builder.RegisterType<PaymentOptionRepository>().As<IPaymentOptionRepository>();
            builder.RegisterType<PaymentProcessorRepository>().As<IPaymentProcessorRepository>();
            builder.RegisterType<PaymentAllocationRepository>().As<IPaymentAllocationRepository>();
            builder.RegisterType<FinancePlanVisitInterestDueRepository>().As<IFinancePlanVisitInterestDueRepository>();
            builder.RegisterType<PaymentImportRepository>().As<IPaymentImportRepository>();
            builder.RegisterType<ServiceGroupRepository>().As<IServiceGroupRepository>();
        }
    }
}
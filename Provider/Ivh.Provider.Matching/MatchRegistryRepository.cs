﻿namespace Ivh.Provider.Matching
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Application.Matching.Common.Dtos;
    using Common.Data.Connection.Connections;
    using Common.Data.Interfaces;
    using Common.VisitPay.Enums;
    using Ivh.Common.Data;
    using Ivh.Domain.Matching.Interfaces;
    using Ivh.Domain.Matching.Entities;

    public class MatchRegistryRepository : RepositoryBase<MatchRegistry, VisitPay>, IMatchRegistryRepository
    {

        public MatchRegistryRepository(IStatelessSessionContext<VisitPay> sessionContext) : base(sessionContext)
        {
        }

        public DateTime? GetLastUpdateDate(IList<PatternSetEnum> enabledPatternSetEnums)
        {
            DateTime? maxDate = this.StatelessSession.Query<MatchRegistry>()
                .Where(x => enabledPatternSetEnums.Contains(x.PatternSetId))
                .Max(x => (DateTime?) x.DataChangeDate);

            return maxDate;
        }

        public int? GetMaxHsGuarantorId(IList<PatternSetEnum> enabledPatternSetEnums)
        {
            int? maxId = this.StatelessSession.Query<MatchRegistry>()
                .Where(x => enabledPatternSetEnums.Contains(x.PatternSetId))
                .Max(x => (int?) x.HsGuarantorId);

            return maxId;
        }

        public IList<MatchRegistry> GetMatchRegistriesForSetAndPattern(string matchString, MatchOptionEnum patternEnum, PatternSetEnum setEnum, PatternUseEnum patternUseEnum)
        {
            return this.StatelessSession.Query<MatchRegistry>()
                .Where(x => x.PatternUseId == patternUseEnum)
                .Where(x => x.PatternSetId == setEnum)
                .Where(x => x.MatchOptionId == patternEnum)
                .Where(x => x.MatchString == matchString)
                .Where(x => x.DeletedDate == null)
                .ToList();
        }

        public IList<MatchRegistry> GetMatchRegistries(int hsGuarantorId)
        {
            return this.StatelessSession.QueryOver<MatchRegistry>()
                .Where(x => x.HsGuarantorId == hsGuarantorId)
                .Where(x => x.DeletedDate == null)
                .List<MatchRegistry>();
        }

        public IList<MatchRegistry> GetMatchRegistries(IList<int> hsGuarantorIds)
        {
            return this.StatelessSession.Query<MatchRegistry>()
                .Where(x => hsGuarantorIds.Contains(x.HsGuarantorId))
                .Where(x => x.DeletedDate == null)
                .ToList();
        }

        // virtual for testing
        public virtual int? UpdateMatchRegistryMatchString(MatchRegistryDto dto)
        {
            int? matchRegistryChanged = this.StatelessSession
                .CreateSQLQuery(@"exec dbo.UpdateMatchRegistry :BillingSystemId, :SourceSystemKey, :MatchOptionId, :PatternSetId, :PatternUseId, :MatchString, :DataChangeDate, :HsGuarantorId, :BaseVisitId")
                .SetParameter("BillingSystemId", dto.BillingSystemId)
                .SetParameter("SourceSystemKey", dto.SourceSystemKey)
                .SetParameter("MatchOptionId", dto.MatchOptionId)
                .SetParameter("PatternSetId", dto.PatternSetId)
                .SetParameter("PatternUseId", dto.PatternUseId)
                .SetParameter("MatchString", dto.MatchString)
                .SetParameter("DataChangeDate", dto.DataChangeDate)
                .SetParameter("HsGuarantorId", dto.HsGuarantorId)
                .SetParameter("BaseVisitId", dto.BaseVisitId)
                .UniqueResult<int?>();

            return matchRegistryChanged;
        }
    }
}

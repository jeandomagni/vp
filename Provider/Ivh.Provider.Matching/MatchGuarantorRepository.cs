﻿namespace Ivh.Provider.Matching
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Common.Data.Connection.Connections;
    using Common.Data.Interfaces;
    using Ivh.Common.Base.Utilities.Extensions;
    using Ivh.Common.Data;
    using Ivh.Domain.Matching.Entities;
    using Ivh.Domain.Matching.Interfaces;
    using NHibernate;
    using NHibernate.Criterion;
    using NHibernate.SqlCommand;
    using NHibernate.Transform;

    public class MatchGuarantorRepository : RepositoryBase<Guarantor,CdiEtl>, IMatchGuarantorRepository
    {
        public MatchGuarantorRepository(ISessionContext<CdiEtl> sessionContext, IStatelessSessionContext<CdiEtl> statelessSessionContext) : base(sessionContext, statelessSessionContext)
        {
        }

        public IEnumerable<IEnumerable<int>> GetMatchesToQueue(DateTime processDate, int maxHsGuarantorId, bool patientDataRequired, bool updatedHsGuarantors, int chunkSize)
        {
            IEnumerable<int> results = patientDataRequired
                ? this.GetMatchesToQueueGuarantor(processDate, maxHsGuarantorId, updatedHsGuarantors).Union(this.GetMatchesToQueuePatient(processDate, maxHsGuarantorId, updatedHsGuarantors))
                : this.GetMatchesToQueueGuarantor(processDate, maxHsGuarantorId, updatedHsGuarantors);

            return results.SplitIntoChunks(chunkSize);
        }

        private IEnumerable<int> GetMatchesToQueueGuarantor(DateTime processDate, int maxHsGuarantorId, bool updatesOnly)
        {
            IQueryable<Guarantor> guarantorQuery = this.StatelessSession.Query<Guarantor>();

            guarantorQuery = updatesOnly 
                ? guarantorQuery.Where(x => x.HsGuarantorId <= maxHsGuarantorId)
                : guarantorQuery.Where(x => x.HsGuarantorId > maxHsGuarantorId);

            if (processDate > DateTime.MinValue)
            {
                guarantorQuery = guarantorQuery.Where(x => x.DataChangeDate > processDate);
            }

            IList<int> guarantorResults = guarantorQuery.Select(x => x.HsGuarantorId).ToList();
            return guarantorResults;
        }

        private IEnumerable<int> GetMatchesToQueuePatient(DateTime processDate, int maxHsGuarantorId, bool updatesOnly)
        {
            IQueryable<Patient> patientQuery = this.StatelessSession.Query<Patient>();

            patientQuery = updatesOnly
                ? patientQuery.Where(x => x.HsGuarantorId <= maxHsGuarantorId)
                : patientQuery.Where(x => x.HsGuarantorId > maxHsGuarantorId);

            if (processDate > DateTime.MinValue)
            {
                patientQuery = patientQuery.Where(x => x.DataChangeDate > processDate);
            }

            IList<int> patientResults = patientQuery.Select(x => x.HsGuarantorId).ToList();
            return patientResults;
        }

        public IList<MatchData> GetMatchData(string sourceSystemKey, int billingSystemId, bool includePatient)
        {
            return this.GetMatchDataInternal(includePatient, 
                q => q.Where(x => x.HsBillingSystemId == billingSystemId),
                q => q.WhereStringEq(x => x.SourceSystemKey, sourceSystemKey));
        }

        public IList<MatchData> GetMatchData(IList<int> hsGuarantorIds, bool includePatient)
        {
            return this.GetMatchDataInternal(includePatient, 
                q => q.Where(x => x.HsGuarantorId.IsIn(hsGuarantorIds.ToArray())));
        }

        private IList<MatchData> GetMatchDataInternal(bool includePatient, params Func<IQueryOver<Guarantor, Guarantor>, IQueryOver<Guarantor, Guarantor>>[] filters)
        {
            Guarantor g = null;
            Patient p = null;
            MatchData d = null;

            /* generates a query like:
             
             select g.*, p.*
             from base.HsGuarantor g
             left outer join base.Visit p 
                on p.hsGuarantorId = g.hsGuarantorId
             left outer join guestpay.Guarantor_Authentication gpa on  ( 
               gpa.HsGuarantorId = p.HsGuarantorId 
               and gpa.ServiceGroupId = p.ServiceGroupId 
               and gpa.InsertLoadTrackerId in 
               (
             	SELECT gpa2.InsertLoadTrackerId
             	FROM guestpay.Guarantor_Authentication gpa2 
             	WHERE p.HsGuarantorId = gpa2.HsGuarantorId 
             	and p.ServiceGroupId = gpa2.ServiceGroupId 
             	ORDER BY gpa2.InsertLoadTrackerId desc OFFSET 0 ROWS FETCH FIRST 1 ROWS ONLY
             	)
             ) 
             where 1=1
             and (filters)
             
             */

            IQueryOver<Guarantor, Guarantor> query = this.StatelessSession.QueryOver(() => g);

            if (includePatient)
            {
                query = query.JoinEntityAlias(() => p, () => g.HsGuarantorId == p.HsGuarantorId, JoinType.LeftOuterJoin);
            }

            foreach (Func<IQueryOver<Guarantor, Guarantor>, IQueryOver<Guarantor, Guarantor>> filter in filters)
            {
                query = filter(query);
            }

            IList<MatchData> results = query
                .SelectList(list =>
                {
                    list = list.Select(x => g.AsEntity()).WithAlias(() => d.Guarantor);

                    if (includePatient)
                    {
                        list = list.Select(x => p.AsEntity()).WithAlias(() => d.Patient);
                    }

                    return list;
                })
                .TransformUsing(Transformers.AliasToBean(typeof(MatchData)))
                .List<MatchData>();

            return results;
        }
    }
}

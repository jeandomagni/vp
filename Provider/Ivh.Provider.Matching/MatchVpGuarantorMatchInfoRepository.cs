﻿namespace Ivh.Provider.Matching
{
    using System.Linq;
    using Common.Data;
    using Common.Data.Connection.Connections;
    using Common.Data.Interfaces;
    using Common.VisitPay.Enums;
    using Domain.Matching.Entities;
    using Domain.Matching.Interfaces;

    public class MatchVpGuarantorMatchInfoRepository : RepositoryBase<MatchVpGuarantorMatchInfo, VisitPay>, IMatchVpGuarantorMatchInfoRepository
    {
        public MatchVpGuarantorMatchInfoRepository(ISessionContext<VisitPay> sessionContext) : base(sessionContext)
        {
        }

        public bool ActiveVpGuarantorRegistryMatchExists(int registryId)
        {
            return this.GetQueryable()
                .Any(x => x.Matches.Any(m => m.Registry.RegistryId == registryId));
        }

        public MatchVpGuarantorMatchInfo GetVpGuarantorRegistryMatch(int hsGuarantorId)
        {
            return this.GetQueryable()
                .SingleOrDefault(x => x.Matches.Any(m => m.HsGuarantorId == hsGuarantorId && m.HsGuarantorMatchStatusId != HsGuarantorMatchStatusEnum.UnmatchedSoft));
        }

        public string GetGuarantorMatchSsn(int vpGuarantorId)
        {
            return this.GetQueryable().FirstOrDefault(x => x.VpGuarantorId == vpGuarantorId)?.MatchSsn;
        }
    }
}

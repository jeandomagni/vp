﻿namespace Ivh.Provider.Matching.Modules
{
    using Autofac;
    using Ivh.Domain.Matching.Interfaces;

    public class Module : Autofac.Module
    {
        protected override void Load(ContainerBuilder builder)
        {           
            builder.RegisterType<MatchRegistryRepository>().As<IMatchRegistryRepository>();
            builder.RegisterType<MatchGuarantorRepository>().As<IMatchGuarantorRepository>();
            builder.RegisterType<MatchVpGuarantorMatchInfoRepository>().As<IMatchVpGuarantorMatchInfoRepository>();
            builder.RegisterType<MatchReconciliationRepository>().As<IMatchReconciliationRepository>();
        }
    }
}

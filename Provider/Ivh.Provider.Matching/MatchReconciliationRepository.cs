﻿namespace Ivh.Provider.Matching
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Common.Cache;
    using Common.Data;
    using Common.Data.Connection.Connections;
    using Common.Data.Interfaces;
    using Domain.Matching.Entities;
    using Domain.Matching.Interfaces;

    public class MatchReconciliationRepository : RepositoryBase<MatchReconciliationMatchInfo, CdiEtl>, IMatchReconciliationRepository
    {
        public MatchReconciliationRepository(IStatelessSessionContext<CdiEtl> statelessSessionContext, Lazy<IDistributedCache> cache = null) : base(statelessSessionContext, cache)
        {
        }

        public IList<int> GetAllMatchReconciliationVpGuarantorIds()
        {
            IList<int> result = this.StatelessSession.QueryOver<MatchReconciliationMatchInfo>()
                .Select(x => x.VpGuarantorId)
                .List<int>();

            return result;
        }

        public IList<MatchReconciliationMatchInfo> GetMatchReconciliationMatchInfo(IList<int> vpGuarantorIds)
        {
            IList<MatchReconciliationMatchInfo> result = this.StatelessSession.Query<MatchReconciliationMatchInfo>()
                .Where(x => vpGuarantorIds.Contains(x.VpGuarantorId))
                .ToList();

            return result;
        }
    }
}

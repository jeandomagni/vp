﻿namespace Ivh.Provider.FileStorage
{
    using Common.Data;
    using Domain.FileStorage.Entities;
    using Domain.FileStorage.Interfaces;
    using NHibernate;
    using NHibernate.Criterion;
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using System.Linq;
    using Common.Data.Connection.Connections;
    using Common.Data.Interfaces;

    public class FileStorageRepository : RepositoryBase<FileStored, Storage>, IFileStorageRepository
    {
        public FileStorageRepository(ISessionContext<Storage> sessionContext)
            : base(sessionContext)
        {}

        public FileStored GetByKey(Guid externalKey)
        {
            ICriteria fsQuery = this.Session.CreateCriteria<FileStored>();
            fsQuery.Add(Restrictions.Eq("ExternalKey", externalKey));

            return fsQuery.UniqueResult<FileStored>();
        }

        public IList<FileStored> GetUnreferenced()
        {
            ICriteria fsQuery = this.Session.CreateCriteria<FileStored>();
            fsQuery.Add(Restrictions.IsNull("ExternalReferenceDate"));
            fsQuery.Add(Restrictions.IsNull("RemoveDate"));
            var list = fsQuery.List<FileStored>();
            return list;

        }

        /// <summary>
        /// Hard delete of FileContents but soft delete of the row
        /// </summary>
        /// <param name="fs"></param>
        public void RemoveUnreferenced(FileStored fs)
        {
            fs.RemoveDate = DateTime.UtcNow;
            fs.FileContents = null; 
            this.InsertOrUpdate(fs);
        }
    }
}
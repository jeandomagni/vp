﻿
namespace Ivh.Provider.FileStorage
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using Common.Base.Utilities.Extensions;
    using Common.Base.Utilities.Helpers;
    using Domain.FileStorage.Entities;
    using Domain.FileStorage.Interfaces;
    using Ivh.Domain.Logging.Interfaces;

    public class FileManifestProvider : IFileManifestProvider
    {
        private readonly Lazy<ILoggingProvider> _logger;

        public FileManifestProvider(
            Lazy<ILoggingProvider> logger
        )
        {
            this._logger = logger;
        }

        public string CreateFileManifestStringForFiles(IList<FileStored> files)
        {
            return this.CreateManifestForFiles(files);
        }

        public byte[] CreateFileManifestBytesForFiles(IList<FileStored> files)
        {
            string manifest = this.CreateManifestForFiles(files);
            byte[] manifestBytes = Encoding.UTF8.GetBytes(manifest);

            return manifestBytes;
        }

        public void WriteFileManifestForFiles(IList<FileStored> files, string fileName, string directory)
        {
            //Create the directory
            this.CreateManifestDirectory(directory);

            //Create the manifest
            string manifest = this.CreateManifestForFiles(files);

            //Save manifest
            string fullPath = $"{directory}\\{fileName}";
            File.WriteAllText(fullPath, manifest);
        }

        private void CreateManifestDirectory(string directory)
        {
            try
            {
                if (!Directory.Exists(directory))
                {
                    Directory.CreateDirectory(directory);
                }
            }
            catch (Exception ex)
            {
                this._logger.Value.Fatal(() => $"{nameof(this.CreateManifestDirectory)} - Failed - Stack: {ExceptionHelper.AggregateExceptionToString(ex)}");
                throw;
            }
        }

        private string CreateManifestForFiles(IList<FileStored> files)
        {
            StringBuilder manifest = new StringBuilder();

            //Add header
            manifest.AppendLine("File Name === File Date === File Size");

            //Add all files
            foreach (FileStored file in files)
            {
                manifest.AppendLine($"{file.Filename} === {file.FileDateTime.ToDateTimeText()} === {file.FileContents.Length} bytes");
            }

            //Add total file count
            manifest.AppendLine($"Total File Count: {files.Count}");

            return manifest.ToString();
        }
    }
}

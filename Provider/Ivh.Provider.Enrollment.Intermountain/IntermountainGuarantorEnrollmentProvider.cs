﻿namespace Ivh.Provider.Enrollment.Intermountain
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Net;
    using System.Net.Http;
    using System.Net.Http.Headers;
    using System.Security.Authentication;
    using System.Text;
    using System.Threading.Tasks;
    using System.Web.Helpers;
    using AutoMapper;
    using Common.Base.Utilities.Extensions;
    using Common.Base.Utilities.Helpers;
    using Common.ServiceBus.Interfaces;
    using Common.VisitPay.Messages.Core;
    using Common.VisitPay.Messages.HospitalData;
    using Common.VisitPay.Messages.HospitalData.Dtos;
    using Domain.Guarantor.Interfaces;
    using Domain.Logging.Interfaces;
    using Domain.Settings.Enums;
    using Domain.Settings.Interfaces;
    using Models;
    using Newtonsoft.Json;

    public class IntermountainGuarantorEnrollmentProvider : IGuarantorEnrollmentProvider
    {
        private readonly Lazy<IApplicationSettingsService> _applicationSettingsService;
        private readonly Lazy<ILoggingProvider> _logger;
        private readonly Lazy<IBus> _bus;

        public IntermountainGuarantorEnrollmentProvider(Lazy<IApplicationSettingsService> applicationSettingsService,
            Lazy<ILoggingProvider> logger,
            Lazy<IBus> bus)
        {
            this._applicationSettingsService = applicationSettingsService;
            this._logger = logger;
            this._bus = bus;
        }

        public async Task<EnrollmentResponseDto> UnEnrollGuarantorAsync(string guarantorIdToEnroll)
        {
            return await this.CallEnrollServiceAsync(new EnrollGuarantorRequestModel {guarantorId = guarantorIdToEnroll, enrollmentStatus = "F"}, "UnEnrollGuarantor");
        }

        public async Task<EnrollmentResponseDto> EnrollGuarantorAsync(string guarantorIdToEnroll, int billingSystemId, int vpGuarantorId)
        {
            string jsonString = string.Empty;
            EnrollGuarantorResponseModel responseModel = null;
            EnrollmentResponseDto returnDto = new EnrollmentResponseDto();
            AuthenticateResponseModel getToken = await this.GetJwtTokenAsync();
            if (getToken != null && getToken.token != null)
            {
                try
                {
                    this._logger.Value.Info(() => $"IntermountainGuarantorEnrollmentProvider::EnrollGuarantor - Start - Guarantor SourceSystemKey = {guarantorIdToEnroll}");
                    EnrollGuarantorRequestModel requestModel = new EnrollGuarantorRequestModel();

                    //ServicePointManager.ServerCertificateValidationCallback += (sender, cert, chain, sslPolicyErrors) => true;
                    ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;

                    //todo: pretty sure this is wrong
                    //Guessing i'm going to have to loop through the HsGuarantors and register each?
                    requestModel.guarantorId = guarantorIdToEnroll;
                    requestModel.enrollmentStatus = "T";

                    using (HttpClient client = new HttpClient())
                    {
                        client.DefaultRequestHeaders.Accept.Clear();
                        client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                        client.DefaultRequestHeaders.Add("token", getToken.token);
                        client.DefaultRequestHeaders.Add("clientid", getToken.clientid);

                        HttpResponseMessage response = null;
                        try
                        {
                            response = await client.PostAsync(this._applicationSettingsService.Value.GetUrl(UrlEnum.Enrollment), new StringContent(JsonConvert.SerializeObject(requestModel), Encoding.UTF8, "application/json"));
                            response.EnsureSuccessStatusCode();
                        }
                        catch (HttpRequestException e)
                        {
                            if (this.ResponseHandler(response, e, "EnrollGuarantor"))
                            {
                                throw;
                            }
                        }

                        if (response != null)
                        {
                            jsonString = await response.Content.ReadAsStringAsync();
                            returnDto.RawResponse = Json.Encode(jsonString);
                            this._logger.Value.Info(() => $"IntermountainGuarantorEnrollmentProvider::EnrollGuarantor - Guarantor SSK = {guarantorIdToEnroll}, Response = {Json.Encode(jsonString)}");
                            responseModel = JsonConvert.DeserializeObject<EnrollGuarantorResponseModel>(jsonString);

                            this.PublishResponse(responseModel, billingSystemId, vpGuarantorId, returnDto);
                        }
                    }
                }
                catch (Exception e)
                {
                    this._logger.Value.Fatal(() => $"IntermountainGuarantorEnrollmentProvider::EnrollGuarantor - Unhandled Exception = {ExceptionHelper.AggregateExceptionToString(e)}");
                    throw;
                }
            }
            else
            {
                string message() => $"IntermountainGuarantorEnrollmentProvider::EnrollGuarantor - Didn't get a token while trying to call for Guarantor SSK = {guarantorIdToEnroll}";
                this._logger.Value.Fatal(message);
                throw new AuthenticationException(message());
            }


            returnDto = Mapper.Map(responseModel, returnDto);
            returnDto.PublishChangeEvent = false;
            return returnDto;
        }

        private void PublishResponse(EnrollGuarantorResponseModel response, int billingSystemId, int vpGuarantorId, EnrollmentResponseDto returnDto)
        {
            if (response.accounts.IsNotNullOrEmpty())
            {
                Dictionary<string, int> billingOriginatingSystems = new Dictionary<string, int> { ["FAC"] = 2, ["CPA"] = 3, ["CPM"] = 3 };

                List<string> originatingSystems =
                    billingOriginatingSystems.Where(x => x.Value == billingSystemId)
                        .Select(x => x.Key)
                        .ToList();

                // "C" accounts are closed and not eligible for VP.  Don't even look at them.
                IList<EnrollGuarantorAccountResponseModel> accountsForBillingSystem = response.accounts.Where(x => originatingSystems.Contains(x.originatingSystem) && x.eligiblitStatus != "C").ToList();
                response.accounts = accountsForBillingSystem;

                List<RealTimeEnrollmentAccountDto> accounts = response.accounts.Select(x => new RealTimeEnrollmentAccountDto
                    {
                        SourceSystemKey = $"{x.originatingSystem}_{x.facilityId.PadLeft(3, '0')}_{x.accountNumber}",
                        BillingSystemId = billingOriginatingSystems[x.originatingSystem],
                        BillingHold = !x.eligiblitStatus.Equals("A")
                    }
                ).ToList();

                EnrollmentResponseDto responseDto = Mapper.Map(response, returnDto);
                this._bus.Value.PublishMessage(new RealTimeVisitEnrollmentMessage
                {
                    HsGuarantorSourceSystemKey = response.guarantorId,
                    GuarantorAccounts = accounts,
                    VpGuarantorId = vpGuarantorId,
                    Response = responseDto
                }).Wait();
            }
        }

        public async Task PublishEnrollGuarantorMessageAsync(int vpGuarantorId)
        {
            await this._bus.Value.PublishMessage(new EnrollGuarantorMessage
            {
                VpGuarantorId = vpGuarantorId
            });
        }

        public async Task PublishUnEnrollGuarantorMessageAsync(int vpGuarantorId)
        {
            await this._bus.Value.PublishMessage(new UnEnrollGuarantorMessage
            {
                VpGuarantorId = vpGuarantorId
            });
        }

        protected async Task<AuthenticateResponseModel> GetJwtTokenAsync()
        {
            return await this.GetJwtTokenAsync(
                this._applicationSettingsService.Value.EnrollmentUsername.Value,
                this._applicationSettingsService.Value.EnrollmentPassword.Value,
                this._applicationSettingsService.Value.EnrollmentClientId.Value);
        }

        protected async Task<AuthenticateResponseModel> GetJwtTokenAsync(string username, string password, string clientId)
        {
            AuthenticateResponseModel model = null;
            string jsonString = string.Empty;
            try
            {
                //ServicePointManager.ServerCertificateValidationCallback += (sender, cert, chain, sslPolicyErrors) => true;
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;

                this._logger.Value.Info(() => "IntermountainGuarantorEnrollmentProvider::GetJwtJokenAsync - Getting token - Start");
                //Lets do this old school since HttpClient doesn't let you set the headers for get (Content-Type specifically)
                HttpWebRequest request = (HttpWebRequest) WebRequest.Create(this._applicationSettingsService.Value.GetUrl(UrlEnum.EnrollmentJwtAuthentication));
                request.Headers.Add("username", username);
                request.Headers.Add("password", password);
                request.Headers.Add("clientid", clientId);
                request.ContentType = "application/json";
                request.Accept = "application/json";
                request.Method = WebRequestMethods.Http.Get;

                using (HttpWebResponse response = (HttpWebResponse) request.GetResponse())
                {
                    //Handle the 403 etc.
                    switch (response.StatusCode)
                    {
                        case HttpStatusCode.Moved:
                        case HttpStatusCode.Redirect:
                        case HttpStatusCode.NotFound:
                        case HttpStatusCode.Gone:
                            this._logger.Value.Fatal(() => $"IntermountainGuarantorEnrollmentProvider::GetJwtTokenAsync - UrlResolutionChange - StatusCode = {response.StatusCode}, Info = {this.generateResponseDetails(response)}");
                            return null;
                        case HttpStatusCode.Unauthorized:
                        case HttpStatusCode.Forbidden:
                        case HttpStatusCode.ProxyAuthenticationRequired:
                            this._logger.Value.Fatal(() => $"IntermountainGuarantorEnrollmentProvider::GetJwtTokenAsync - AccessFailure - StatusCode = {response.StatusCode}, Info = {this.generateResponseDetails(response)}");
                            return null;
                        case HttpStatusCode.RequestTimeout:
                        case HttpStatusCode.GatewayTimeout:
                            this._logger.Value.Fatal(() => $"IntermountainGuarantorEnrollmentProvider::GetJwtTokenAsync - TimeoutResponse - StatusCode = {response.StatusCode}, Info = {this.generateResponseDetails(response)}");
                            return null;
                        case HttpStatusCode.BadRequest:
                        case HttpStatusCode.MethodNotAllowed:
                        case HttpStatusCode.NotAcceptable:
                        case HttpStatusCode.Conflict:
                        case HttpStatusCode.RequestEntityTooLarge:
                        case HttpStatusCode.RequestUriTooLong:
                        case HttpStatusCode.HttpVersionNotSupported:
                            this._logger.Value.Fatal(() => $"IntermountainGuarantorEnrollmentProvider::GetJwtTokenAsync - ConfigurationError - StatusCode = {response.StatusCode}, Info = {this.generateResponseDetails(response)}");
                            return null;
                        case HttpStatusCode.ServiceUnavailable:
                        case HttpStatusCode.InternalServerError:
                        case HttpStatusCode.NotImplemented:
                        case HttpStatusCode.BadGateway:
                        case (HttpStatusCode) 420: // Method Failure Sprint Framework
                            this._logger.Value.Fatal(() => $"IntermountainGuarantorEnrollmentProvider::GetJwtTokenAsync - RemoteServerError - StatusCode = {response.StatusCode}, Info = {this.generateResponseDetails(response)}");
                            return null;
                        default:
                            this._logger.Value.Fatal(() => $"IntermountainGuarantorEnrollmentProvider::GetJwtTokenAsync - UnknownResponseError - StatusCode = {response.StatusCode}, Info = {this.generateResponseDetails(response)}");
                            return null;
                        case HttpStatusCode.Accepted:
                        case HttpStatusCode.OK:
                            break;
                    }

                    Stream responseStream = response.GetResponseStream();
                    if (responseStream != null)
                    {
                        using (StreamReader reader = new StreamReader(responseStream))
                        {
                            jsonString = await reader.ReadToEndAsync();
                            reader.Close();
                        }

                        this._logger.Value.Debug(() => $"IntermountainGuarantorEnrollmentProvider::GetJwtTokenAsync - Debug jsonString =  {Json.Encode(jsonString)}");
                        model = JsonConvert.DeserializeObject<AuthenticateResponseModel>(jsonString);
                    }

                    response.Close();
                }
            }
            catch (Exception e)
            {
                this._logger.Value.Fatal(() => $"IntermountainGuarantorEnrollmentProvider::GetJwtJokenAsync - Unhandled Exception = {ExceptionHelper.AggregateExceptionToString(e)}");
                throw;
            }

            if (model != null)
            {
                this._logger.Value.Info(() => $"IntermountainGuarantorEnrollmentProvider::GetJwtJokenAsync - Getting token - end - Model.clientid = {model.clientid}, model.token = {model.token}");
            }


            return model;
        }

        private async Task<EnrollmentResponseDto> CallEnrollServiceAsync(EnrollGuarantorRequestModel model, string method)
        {
            EnrollGuarantorResponseModel responseModel = null;
            AuthenticateResponseModel getToken = await this.GetJwtTokenAsync();
            EnrollmentResponseDto returnDto = new EnrollmentResponseDto();
            try
            {
                this._logger.Value.Info(() => $"IntermountainGuarantorEnrollmentProvider::{method} - Start - Guarantor SourceSystemKey = {model.guarantorId}");

                //ServicePointManager.ServerCertificateValidationCallback += (sender, cert, chain, sslPolicyErrors) => true;
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;

                using (HttpClient client = new HttpClient())
                {
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                    client.DefaultRequestHeaders.Add("token", getToken.token);
                    client.DefaultRequestHeaders.Add("clientid", getToken.clientid);

                    HttpResponseMessage response = null;
                    try
                    {
                        response = await client.PostAsync(this._applicationSettingsService.Value.GetUrl(UrlEnum.Enrollment), new StringContent(JsonConvert.SerializeObject(model), Encoding.UTF8, "application/json"));
                        response.EnsureSuccessStatusCode();
                    }
                    catch (HttpRequestException e)
                    {
                        if (this.ResponseHandler(response, e, method))
                        {
                            throw;
                        }
                    }

                    if (response != null)
                    {
                        string jsonString = await response.Content.ReadAsStringAsync();
                        returnDto.RawResponse = Json.Encode(jsonString);
                        this._logger.Value.Info(() => $"IntermountainGuarantorEnrollmentProvider::{method} - Guarantor Source System Key = {model.guarantorId}, Response = {Json.Encode(jsonString)}");
                        responseModel = JsonConvert.DeserializeObject<EnrollGuarantorResponseModel>(jsonString);
                    }
                }
            }
            catch (Exception e)
            {
                this._logger.Value.Fatal(() => $"IntermountainGuarantorEnrollmentProvider::{method} - Unhandled Exception = {ExceptionHelper.AggregateExceptionToString(e)}");
                throw;
            }

            returnDto = Mapper.Map(responseModel, returnDto);
            return returnDto;
        }

        private bool ResponseHandler(HttpResponseMessage response, HttpRequestException ex, string method)
        {
            if (response != null)
            {
                switch (response.StatusCode)
                {
                    case HttpStatusCode.Moved:
                    case HttpStatusCode.Redirect:
                    case HttpStatusCode.NotFound:
                    case HttpStatusCode.Gone:
                        this._logger.Value.Fatal(() => $"IntermountainGuarantorEnrollmentProvider::{method} - UrlResolutionChange - StatusCode = {response.StatusCode}, Exception = {ExceptionHelper.AggregateExceptionToString(ex)}");
                        return true;
                    case HttpStatusCode.Unauthorized:
                    case HttpStatusCode.Forbidden:
                    case HttpStatusCode.ProxyAuthenticationRequired:
                        this._logger.Value.Fatal(() => $"IntermountainGuarantorEnrollmentProvider::{method} - AccessFailure - StatusCode = {response.StatusCode}, Exception = {ExceptionHelper.AggregateExceptionToString(ex)}");
                        return true;
                    case HttpStatusCode.RequestTimeout:
                    case HttpStatusCode.GatewayTimeout:
                        this._logger.Value.Fatal(() => $"IntermountainGuarantorEnrollmentProvider::{method} - TimeoutResponse - StatusCode = {response.StatusCode}, Exception = {ExceptionHelper.AggregateExceptionToString(ex)}");
                        return true;
                    case HttpStatusCode.BadRequest:
                    case HttpStatusCode.MethodNotAllowed:
                    case HttpStatusCode.NotAcceptable:
                    case HttpStatusCode.Conflict:
                    case HttpStatusCode.RequestEntityTooLarge:
                    case HttpStatusCode.RequestUriTooLong:
                    case HttpStatusCode.HttpVersionNotSupported:
                        this._logger.Value.Fatal(() => $"IntermountainGuarantorEnrollmentProvider::{method} - ConfigurationError - StatusCode = {response.StatusCode}, Exception = {ExceptionHelper.AggregateExceptionToString(ex)}");
                        return true;
                    case HttpStatusCode.ServiceUnavailable:
                    case HttpStatusCode.InternalServerError:
                    case HttpStatusCode.NotImplemented:
                    case HttpStatusCode.BadGateway:
                    case (HttpStatusCode) 420: // Method Failure Sprint Framework
                        this._logger.Value.Fatal(() => $"IntermountainGuarantorEnrollmentProvider::{method} - RemoteServerError - StatusCode = {response.StatusCode}, Exception = {ExceptionHelper.AggregateExceptionToString(ex)}");
                        return true;
                    default:
                        this._logger.Value.Fatal(() => $"IntermountainGuarantorEnrollmentProvider::{method} - UnknownResponseError - StatusCode = {response.StatusCode}, Exception = {ExceptionHelper.AggregateExceptionToString(ex)}");
                        return true;
                    case HttpStatusCode.Accepted:
                    case HttpStatusCode.OK:
                        //Shouldnt ever get here, but if it does i want it to continue.
                        this._logger.Value.Warn(() => $"IntermountainGuarantorEnrollmentProvider::{method} - SuccessResponseCodeThrown - StatusCode = {response.StatusCode}, Exception = {ExceptionHelper.AggregateExceptionToString(ex)}");
                        break;
                }
            }

            return false;
        }

        private string generateResponseDetails(HttpWebResponse response)
        {
            string headers = string.Empty;
            foreach (string header in response.Headers.Keys)
            {
                headers += header + ":" + response.Headers[header] + ",";
            }

            return $"StatusCode: '{response.StatusCode}', Headers: '{headers}', ContentEncoding: '{response.ContentEncoding}', ContentLength: {response.ContentLength}, ContentType: {response.ContentType}, ";
        }
    }
}
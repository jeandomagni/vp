﻿namespace Ivh.Provider.Enrollment.Intermountain.Models
{
    public class AuthenticateResponseModel
    {
        public string clientid { get; set; }
        public string token { get; set; }
    }
}
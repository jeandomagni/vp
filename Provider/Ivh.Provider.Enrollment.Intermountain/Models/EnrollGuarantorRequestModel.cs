﻿namespace Ivh.Provider.Enrollment.Intermountain.Models
{
    public class EnrollGuarantorRequestModel
    {
        public string guarantorId { get; set; }
        public string enrollmentStatus { get; set; }
    }
}
namespace Ivh.Provider.Enrollment.Intermountain.Models
{
    public class EnrollGuarantorAccountResponseModel
    {
        public string facilityId { get; set; }
        public string accountNumber { get; set; }
        public string eligiblitStatus { get; set; }
        public string originatingSystem { get; set; }
    }
}
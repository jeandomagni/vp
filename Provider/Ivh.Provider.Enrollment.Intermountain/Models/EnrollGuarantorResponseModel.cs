﻿namespace Ivh.Provider.Enrollment.Intermountain.Models
{
    using System.Collections.Generic;

    public class EnrollGuarantorResponseModel
    {
        public string guarantorId { get; set; }
        public string enrollmentStatus { get; set; }
        public string responseCode { get; set; }
        public string responseMessage { get; set; }
        public IList<EnrollGuarantorAccountResponseModel> accounts { get; set; }
    }
}
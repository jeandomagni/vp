﻿namespace Ivh.Provider.Enrollment.Intermountain.Mappings
{
    using System;
    using AutoMapper;
    using Common.Base.Enums;
    using Common.VisitPay.Enums;
    using Common.VisitPay.Messages.HospitalData.Dtos;
    using Models;

    public class AutomapperProfile : Profile
    {
        public AutomapperProfile()
        {
            this.CreateMap<EnrollGuarantorResponseModel, EnrollmentResponseDto>()
                .ForMember(dest => dest.EnrollmentResult, opt => opt.ResolveUsing(fr =>
                {
                    EnrollmentResultEnum resultEnum;
                    return Enum.TryParse(fr.responseCode, true, out resultEnum) ? resultEnum : EnrollmentResultEnum.Invalid;
                }))
                .ForMember(dest => dest.EnrollmentStatus, opt => opt.MapFrom((fr) => string.Equals(fr.enrollmentStatus, "T", StringComparison.InvariantCultureIgnoreCase)))
                .ForMember(dest => dest.GuarantorSourceSystemKey, opt => opt.MapFrom((fr) => fr.guarantorId))
                .ForMember(dest => dest.Accounts, opt => opt.MapFrom((fr) => fr.accounts));


            this.CreateMap<EnrollGuarantorAccountResponseModel, EnrollmentAccountResponseDto>()
                .ForMember(dest => dest.eligiblitStatus, opt => opt.ResolveUsing(fr =>
                {
                    if (fr.eligiblitStatus == null) return EnrollmentAccountEligibilityEnum.NotEligible;
                    switch (fr.eligiblitStatus.ToLowerInvariant())
                    {
                        case "n":
                        case "c":
                        default:
                            return EnrollmentAccountEligibilityEnum.NotEligible;
                        case "a":
                            return EnrollmentAccountEligibilityEnum.Eligible;
                        case "s":
                            return EnrollmentAccountEligibilityEnum.Suspended;
                    }
                }));


        }
    }
}

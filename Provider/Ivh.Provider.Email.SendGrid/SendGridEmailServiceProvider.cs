﻿namespace Ivh.Provider.Email.SendGrid
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Net;
    using System.Net.Mail;
    using System.Text;
    using System.Threading.Tasks;
    using Common.Base.Utilities.Helpers;
    using Domain.Email.Interfaces;
    using Domain.Logging.Interfaces;
    using Domain.Settings.Interfaces;
    using Exceptions;
    using global::SendGrid;

    public class SendGridEmailServiceProvider : IEmailServiceProvider
    {

        private readonly Lazy<ILoggingProvider> _logger;
        private readonly Lazy<IApplicationSettingsService> _applicationSettingsService;
        
        /// <summary>
        ///     Initializes a new instance of the <see cref="SendGridEmailServiceProvider" /> class.
        /// </summary>
        public SendGridEmailServiceProvider(
            Lazy<IApplicationSettingsService> applicationSettingsService,
            Lazy<ILoggingProvider> logger)
        {
            this._logger = logger;
            this._applicationSettingsService = applicationSettingsService;
        }

        /// <summary>
        ///     Sends this instance.
        /// </summary>
        /// <returns></returns>
        public async Task<bool> SendAsync(MailMessage mailMessage, Guid uniqueId)
        {
            if (IvhEnvironment.IsNonProduction)
            {
                bool preventEmailsFromSending = false;
                try
                {
                    preventEmailsFromSending = this._applicationSettingsService.Value.PreventEmailsFromSending.Value;
                }
                catch
                {
                    //Do nothing.
                }
                if (preventEmailsFromSending)
                    return true;
            }

            bool isSuccess;

            SendGridMessage sendGridMessage = new SendGridMessage {From = mailMessage.From, Subject = mailMessage.Subject};

            // Mail Body
            if (mailMessage.IsBodyHtml)
                sendGridMessage.Html = mailMessage.Body;
            else
                sendGridMessage.Text = mailMessage.Body;

            //send prod emails
            if (!this._applicationSettingsService.Value.RerouteEmailForTesting.Value)
            {
                // To Email(s)
                sendGridMessage.AddTo(mailMessage.To.Select(x => x.Address));

                // Cc Email(s)
                foreach (MailAddress ccEmail in mailMessage.CC)
                {
                    sendGridMessage.AddCc(ccEmail);
                }

                // Bcc Email(s)
                foreach (MailAddress bccEmail in mailMessage.Bcc)
                {
                    sendGridMessage.AddBcc(bccEmail);
                }
            }
            else //reroute email to test account
            {
                //set new rerouted To Address
                string rerouteEmailTo = this._applicationSettingsService.Value.RerouteEmailTo.Value;

                if (rerouteEmailTo.Contains(";"))
                {
                    string[] toAddresses = rerouteEmailTo.Split(';');
                    foreach (string t in toAddresses.Where(t => t.Length > 0))
                    {
                        sendGridMessage.AddTo(t);
                    }
                }
                else
                {
                    sendGridMessage.AddTo(rerouteEmailTo);
                }

                //create string listing all the original To, CC, and BCC emails.
                string lnBreak = mailMessage.IsBodyHtml ? "<br/>" : Environment.NewLine;
                string origDestination = lnBreak + lnBreak + "***Email Rerouted for Testing***";

                // Add origial To Email(s) to string
                origDestination += lnBreak + lnBreak + "Original To Address(es): ";
                origDestination = mailMessage.To.Aggregate(origDestination, (current, toEmail) => current + (toEmail + "; "));

                // Add origial Cc Email(s) to string
                if (mailMessage.CC.Count > 0)
                {
                    origDestination += lnBreak + lnBreak + "Original CC Address(es): ";
                    origDestination = mailMessage.CC.Aggregate(origDestination, (current, ccEmail) => current + (ccEmail + "; "));
                }

                // Add origial Bcc Email(s) to string
                if (mailMessage.Bcc.Count > 0)
                {
                    origDestination += lnBreak + lnBreak + "Original BCC Address(es): ";
                    origDestination = mailMessage.Bcc.Aggregate(origDestination, (current, bccEmail) => current + (bccEmail + "; "));
                }

                // Mail Subject
                sendGridMessage.Subject = string.Format("{{{0}}} {1}", Environment.MachineName, mailMessage.Subject);

                // Mail Body
                if (mailMessage.IsBodyHtml)
                    sendGridMessage.Html += origDestination;
                else
                    sendGridMessage.Text += origDestination;
            }

            Dictionary<string, string> uniqueArgs = new Dictionary<string, string>
            {
                {"SourceApp", "VisitPay"},
                {"category", sendGridMessage.Subject},
                {"UniqueId", uniqueId.ToString()}
            };

            sendGridMessage.AddUniqueArgs(uniqueArgs);

            try
            {
                string smtpUsername = this._applicationSettingsService.Value.SmtpUsername.Value.Trim();
                string smtpPassword = this._applicationSettingsService.Value.SmtpPassword.Value.Trim();

                Web transportInstance = new Web(new NetworkCredential(smtpUsername, smtpPassword));
                await transportInstance.DeliverAsync(sendGridMessage).ConfigureAwait(false);
                isSuccess = true;
            }
            catch (InvalidApiRequestException ex)
            {
                StringBuilder detail = new StringBuilder();

                detail.AppendLine("ResponseStatusCode: " + ex.ResponseStatusCode + ".   ");
                for (int i = 0; i < ex.Errors.Count(); i++)
                {
                    detail.AppendLine(" -- Error #" + i.ToString() + " : " + ex.Errors[i]);
                }

                this._logger.Value.Warn(() => string.Format("SendGridEmailServiceProvider::DeliverAsync failed UniqueId = {0} Exception = {1} Details = {2}", uniqueId, ExceptionHelper.AggregateExceptionToString(ex), detail.ToString()));
                isSuccess = false;
            }

            return isSuccess;
        }
    }
}
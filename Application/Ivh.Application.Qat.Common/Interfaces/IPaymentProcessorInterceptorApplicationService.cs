﻿namespace Ivh.Application.Qat.Common.Interfaces
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Dtos;
    using Enums;
    using Ivh.Common.Base.Enums;
    using Ivh.Common.Base.Interfaces;
    using Ivh.Common.VisitPay.Enums;

    public interface IPaymentProcessorInterceptorApplicationService : IApplicationService
    {
        bool DefaultSuccessful();
        void SetDefaultSuccessful(bool b);

        int DelayMs();
        void SetDelayMs(int delayMs);

        bool IsActive();
        void Activate(string uri);
        void Deactivate();

        Task<string> GetResponseAsync(IDictionary<string, string> request);
        PpiRequestDto GetPaymentRequest(Guid requestGuid);
        void CompletePaymentRequest(Guid requestGuid, PpiResponseTypeEnum responseType, char? responseAvsCode);
        PpiRequestDto SetAchAction(Guid requestGuid, AchFileTypeEnum fileType, DateTime processDate, AchReturnCodeEnum? returnCode, decimal? returnAmount);
        IList<PpiRequestDto> QueryRequestQueue(PpiRequestFilterDto ppiRequestFilter);
    }
}

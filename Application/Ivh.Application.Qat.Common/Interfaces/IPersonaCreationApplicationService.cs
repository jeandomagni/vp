﻿namespace Ivh.Application.Qat.Common.Interfaces
{
    using System.Collections.Generic;
    using Dtos;
    using Ivh.Common.Base.Interfaces;

    public interface 
        IPersonaCreationApplicationService : IApplicationService
    {
        void Run(string personaUsername, string hsGuarantorSourceSystemKey);

        #region statementguy

        PersonaResultDto StatementGuy9();

        #endregion

        #region training accounts
        
        void AaronTraining();
        void BobTraining();
        void HankTraining();
        void JoniAndHenryTraining();
        void JaneAndSteveTraining();
        void JulieTraining();
        void LanceTraining();
        void RickAndSusieTraining();
        void SaraTraining();

        #endregion

        #region demo accounts
        
        void JaneSmith();
        void MaryAndLarryPark();

        #endregion

        #region loadtest

        void LoadTestGuy();

        #endregion

        #region existing hs accounts

        void CallCenterAllStatements(string hsGuarantorSourceSystemKey);

        #endregion

        #region usability accounts

        void CreateUsabilityPersona(string personaUserName, string password);
        List<string> GetUsabilityUserNames();

        #endregion

        void Test(int numberOfAccounts);
        void Queue();

    }
}
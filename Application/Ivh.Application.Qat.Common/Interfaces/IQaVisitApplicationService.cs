﻿namespace Ivh.Application.Qat.Common.Interfaces
{
    using System.Collections.Generic;
    using Core.Common.Dtos.Eob;

    public interface IQaVisitApplicationService
    {
        IList<EobExternalLinkDto> GetEobExternalLinks();
        bool UpdateDemoEobPayerExternalLink(int eobExternalLinkId);
    }
}
﻿namespace Ivh.Application.Qat.Common.Interfaces
{
    using System.Threading.Tasks;
    using FinanceManagement.Common.Dtos;
    using Ivh.Common.Base.Interfaces;

    public interface ITestPaymentMethodsApplicationService : IApplicationService
    {
        Task<int> SavePaymentMethodAsync(PaymentMethodDto paymentMethodDto, int visitPayUserId, bool isPrimary, string accountNumber, string securityCodeOrRoutingNumber);
    }
}
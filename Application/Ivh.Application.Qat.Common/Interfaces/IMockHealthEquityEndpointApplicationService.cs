﻿namespace Ivh.Application.Qat.Common.Interfaces
{
    using System.Collections.Generic;
    using Dtos;
    using Ivh.Common.Base.Enums;
    using Ivh.Common.VisitPay.Enums;

    public interface IMockHealthEquityEndpointApplicationService
    {
        IList<SavingsAccountInfoDto> GetSavingsAccountInfoDtos(int vpguarantorId, IList<AccountStatusApiModelEnum> statuses, IList<AccountTypeFlagApiModelEnum> types);
    }
}

﻿namespace Ivh.Application.Qat.Common.Interfaces
{
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Ivh.Common.Web.Models;

    public interface IDemoVisitPayUserApplicationService
    {
        Task<ResultMessage<string>> ResetDemoVisitPayUser(string sourceUsername, string sourceUserPassword);
        List<string> GetResetableUsers();
    }
}
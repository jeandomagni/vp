﻿namespace Ivh.Application.Qat.Common.Interfaces
{
    using System.Threading.Tasks;

    public interface IQaSettingsApplicationService
    {
        Task<bool> ChangeClientLogo(string filename, byte[] logo);
    }
}
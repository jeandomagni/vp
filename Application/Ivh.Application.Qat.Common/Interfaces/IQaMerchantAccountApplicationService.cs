﻿namespace Ivh.Application.Qat.Common.Interfaces
{
    using System.Collections.Generic;
    using Ivh.Common.Base.Interfaces;

    public interface IQaMerchantAccountApplicationService : IApplicationService
    {
        IList<int> GetServiceGroupIds();
    }
}
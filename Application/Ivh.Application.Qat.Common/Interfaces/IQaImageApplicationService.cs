﻿namespace Ivh.Application.Qat.Common.Interfaces
{
    using System.Threading.Tasks;

    public interface IQaImageApplicationService
    {
        Task<string> UploadImageAsync(string filename, byte[] bytes);
    }
}
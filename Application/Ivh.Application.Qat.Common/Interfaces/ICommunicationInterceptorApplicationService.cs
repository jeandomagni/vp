﻿namespace Ivh.Application.Qat.Common.Interfaces
{
    using Ivh.Common.Base.Interfaces;

    public interface ICommunicationInterceptorApplicationService : IApplicationService
    {
        bool IsActive();
        void Activate(string uri);
        void Deactivate();
    }
}
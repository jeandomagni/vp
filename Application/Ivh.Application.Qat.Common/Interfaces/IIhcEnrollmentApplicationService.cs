﻿namespace Ivh.Application.Qat.Common.Interfaces
{
    using Dtos;

    public interface IIhcEnrollmentApplicationService
    {
        IhcEnrollmentResponseDto GetByGuarantorId(string guarantorId);
    }
}
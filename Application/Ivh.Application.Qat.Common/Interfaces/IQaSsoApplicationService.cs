﻿namespace Ivh.Application.Qat.Common.Interfaces
{
    using System;
    using Ivh.Common.VisitPay.Enums;

    public interface IQaSsoApplicationService
    {
        string GenerateEncryptedSsoTokenHsGuarantor(int hsGuarantorId, SsoProviderEnum providerEnum, string ssoSsk, DateTime dob);
    }
}

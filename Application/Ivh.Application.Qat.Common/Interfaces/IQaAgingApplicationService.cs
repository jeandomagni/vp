﻿namespace Ivh.Application.Qat.Common.Interfaces
{
    public interface IQaAgingApplicationService
    {
        void AddVisitAgesWithRollback(int vpGuarantorId, int daysToRollBack);
        void SetAgingTier(string sourceSystemKey, int billingSystemId, int agingTier);
    }
}
﻿namespace Ivh.Application.Qat.Common.Interfaces
{
    using System.Collections.Generic;
    using Dtos;
    using Ivh.Common.Base.Interfaces;

    public interface IQaPaymentBatchApplicationService : IApplicationService
    {
        IList<UnclearedPaymentAllocationDto> GetUnclearedOutboundVisitTransactions();
        IList<UnclearedPaymentAllocationDto> GetUnclearedOutboundVisitTransactions(int visitId);
        IList<UnclearedPaymentAllocationDto> GetOutboundVisitTransactionsById(IList<int> vpOutboundVisitTransactionIds);
        void ImportLockboxPaymentsFromFile(string filePath);
    }
}
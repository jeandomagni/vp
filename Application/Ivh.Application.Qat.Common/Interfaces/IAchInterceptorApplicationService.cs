﻿namespace Ivh.Application.Qat.Common.Interfaces
{
    using System;
    using Ivh.Common.Base.Interfaces;

    public interface IAchInterceptorApplicationService : IApplicationService
    {
        void Activate(string uri);
        void Deactivate();

        string GetReturnFile(DateTime startDate, DateTime endDate, string fileFormat);
        string GetReport(string reportTypeId, DateTime startDate, DateTime endDate, string dateTypeId, string exportFormat);
    }
}

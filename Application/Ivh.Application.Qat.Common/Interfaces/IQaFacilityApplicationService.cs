﻿namespace Ivh.Application.Qat.Common.Interfaces
{
    public interface IQaFacilityApplicationService
    {
        void UpdateFacility(int facilityId, string facilityName, bool updateLogo, string logoFilename);
    }
}
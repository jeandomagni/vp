﻿namespace Ivh.Application.Qat.Common.Interfaces
{
    using Ivh.Common.Base.Interfaces;

    public interface IPersonasApplicationService : IApplicationService
    {
        byte[] ExportPersonas(string search);
    }
}
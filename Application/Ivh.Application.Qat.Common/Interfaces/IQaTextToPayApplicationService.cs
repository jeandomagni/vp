﻿namespace Ivh.Application.Qat.Common.Interfaces
{
    using System.Collections.Generic;
    using Dtos;
    using Ivh.Common.Base.Interfaces;

    public interface IQaTextToPayApplicationService : IApplicationService
    {
        List<CommunicationDto> GetTextToPayPaymentOptionCommunications();

        CommunicationDto GetTextToPayPaymentOptionCommunication(int vpUserId);

        List<TextToPayPaymentDto> GetTextToPayPayments(int visitPayUserId);
    }
}
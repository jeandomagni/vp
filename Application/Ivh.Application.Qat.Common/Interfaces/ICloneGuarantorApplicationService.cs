﻿namespace Ivh.Application.Qat.Common.Interfaces
{
    using Enums;
    using Ivh.Common.Base.Interfaces;

    public interface ICloneGuarantorApplicationService : IApplicationService
    {
        void CloneGuarantor(int vpGuarantorId, string appendValue, string userName, bool consolidate);
        CloneGuarantorResultEnum CloneGuarantorSynchronous(string sourceVisitPayUserName, string appendValue);
    }
}
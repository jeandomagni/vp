﻿namespace Ivh.Application.Qat.Common
{
    public static class PersonaConstants
    {
        // demo
        public const string TimJonesUsername = "tim.jones";
        public const string JaneSmithUsername = "jane.smith";
        public const string BillSmithUsername = "bill.smith";
        public const string MaryParkUsername = "mary.park";
        public const string LarryParkUsername = "larry.park"; // managed by mary

        // training
        public const string AaronTrainingUsername = "aaron.training";
        public const string BobTrainingUsername = "bob.training";
        public const string HankTrainingUsername = "hank.training";
        public const string HenryTrainingUsername = "henry.training"; // managed by Joni
        public const string JoniTrainingUsername = "joni.training";
        public const string JaneTrainingUsername = "jane.training";
        public const string SteveTrainingUsername = "steve.training"; // managed by Jane
        public const string JulieTrainingUsername = "julie.training";
        public const string LanceTrainingUsername = "lance.training";
        public const string RickTrainingUsername = "rick.training";
        public const string SusieTrainingUsername = "susie.training"; // managed by Rick
        public const string SaraTrainingUsername = "sara.training";

        // load test
        public const string LoadTestGuyUsername = "loadtestguy";

        // hs data scenarios
        public const string CallCenterAllStatements = "CallCenterAllStatements";

        public const string CallCenterOutboundMessagesTest = "CallCenterOutboundMessagesTest";
        public const string OutboundMessagesTest = "OutboundMessagesTest";

        public static string[] DemoAccountUsernames =
        {
            TimJonesUsername,
            JaneSmithUsername,
            BillSmithUsername,
            MaryParkUsername,
            LarryParkUsername
        };

        public static string[] TrainingAccountUsernames =
        {
            AaronTrainingUsername,
            BobTrainingUsername,
            HankTrainingUsername,
            HenryTrainingUsername,
            JoniTrainingUsername,
            JaneTrainingUsername,
            SteveTrainingUsername,
            JulieTrainingUsername,
            LanceTrainingUsername,
            RickTrainingUsername,
            SusieTrainingUsername,
            SaraTrainingUsername
        };

        public static string[] ManagedAccounts =
        {
            LarryParkUsername,
            HenryTrainingUsername,
            SteveTrainingUsername,
            SusieTrainingUsername
        };

        public static string[] HsDataScenarios =
        {
            CallCenterAllStatements,
            CallCenterOutboundMessagesTest,
            OutboundMessagesTest
        };
    }
}
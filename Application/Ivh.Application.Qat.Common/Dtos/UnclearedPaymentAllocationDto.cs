﻿namespace Ivh.Application.Qat.Common.Dtos
{
    using System;

    public class UnclearedPaymentAllocationDto
    {
        public int VpOutboundVisitTransactionId { get; set; }
        public int VpPaymentAllocationId { get; set; }
        public int? VpPaymentAllocationTypeId { get; set; }
        public int HsVisitId { get; set; }
        public DateTime InsertDate { get; set; }
        public decimal TransactionAmount { get; set; }
    }
}
﻿namespace Ivh.Application.Qat.Common.Dtos
{
    using System;
    using System.Collections.Generic;

    public class PpiRequestFilterDto
    {
        public PpiRequestFilterDto()
        {
            this.RequestGuids = new List<Guid>();
        }

        public IList<Guid> RequestGuids { get; set; }
        public DateTime? AchProcessDate { get; set; }
        public bool? AchProcessed { get; set; }
        public bool? IsAch { get; set; }
        public bool? IsSuccess { get; set; }
    }
}
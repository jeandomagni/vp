﻿namespace Ivh.Application.Qat.Common.Dtos
{
    using System.Collections.Generic;

    public class ExportPersonasDto
    {
        public ExportPersonasDto()
        {
            this.ManagedUsers = new List<string>();
        }

        public int VpGuarantorId { get; set; }
        public string FirstNameLastName { get; set; }
        public string Username { get; set; }
        public bool IsGracePeriod { get; set; }
        public int DaysUntilPaymentDueDate { get; set; }
        public bool HasPastDueVisits { get; set; }
        public int StatementsCount { get; set; }
        public decimal TotalBalance { get; set; }
        public decimal PendingVisitBalanceSum { get; set; }
        public decimal SuspendedVisitBalanceSum { get; set; }
        public decimal FinancePlanBalanceSum { get; set; }
        public int SupportRequestsCount { get; set; }
        public int RecurringPaymentsPendingCount { get; set; }
        public int ManualPaymentsPendingCount { get; set; }
        public int RecurringPaymentsSuccessfulCount { get; set; }
        public int RecurringPaymentsFailedCount { get; set; }
        public int ManualPaymentsSuccessfulCount { get; set; }
        public int ManualPaymentsFailedCount { get; set; }
        public IList<string> ManagedUsers { get; set; }
        public string ManagingUser { get; set; }
    }
}
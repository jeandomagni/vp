﻿namespace Ivh.Application.Qat.Common.Dtos
{
    public class CommunicationDto
    {
        public int SentToUserId { get; set; }
        public string UserName { get; set; }
        public string PhoneNumber { get; set; }
        public string Body { get; set; }
    }
}
﻿namespace Ivh.Application.Qat.Common.Dtos
{
    public class IhcEnrollmentResponseDto
    {
        public int IhcEnrollmentResponseId { get; set; }
        public string GuarantorId { get; set; }
        public string ExpectedEnrollmentStatus { get; set; }
        public string Response { get; set; }
        public int? ReturnErrorCode { get; set; }
    }
}
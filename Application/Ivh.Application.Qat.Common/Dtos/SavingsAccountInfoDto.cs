﻿namespace Ivh.Application.Qat.Common.Dtos
{
    using Ivh.Common.Base.Enums;
    using Ivh.Common.VisitPay.Enums;

    public class SavingsAccountInfoDto
    {
        public AccountTypeFlagApiModelEnum Type { get; set; }
        public string AccountId { get; set; }
        public string Description { get; set; }
        public decimal AvailableBalance { get; set; }
        public decimal Investments { get; set; }
        public decimal Contributions { get; set; }
        public decimal Distributions { get; set; }
        public AccountStatusApiModelEnum Status { get; set; }
    }
}

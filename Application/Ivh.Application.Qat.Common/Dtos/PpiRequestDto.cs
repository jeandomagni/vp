﻿namespace Ivh.Application.Qat.Common.Dtos
{
    using System;
    using System.Collections.Generic;
    using Enums;
    using Ivh.Common.Base.Enums;
    using Ivh.Common.VisitPay.Enums;

    public class PpiRequestDto
    {
        public PpiRequestDto(PpiResponseTypeEnum? responseType, char? responseAvsCode)
        {
            this.ResponseType = responseType;
            this.ResponseAvsCode = responseAvsCode;
        }
        public Guid RequestGuid { get; set; }
        public string Action { get; set; }
        public string BillingId { get; set; }
        public string Amount { get; set; }
        public string CustomField2 { get; set; }
        public string CustomField3 { get; set; }
        public string CustomField4 { get; set; }
        public string CustomField5 { get; set; }
        public string CustomField6 { get; set; }
        public string CustomField9 { get; set; }
        public string CustomField12 { get; set; }
        public PpiResponseTypeEnum? ResponseType { get; private set; }
        public char? ResponseAvsCode { get; private set; }

        public PaymentMethodTypeEnum? VpPaymentMethodType { get; set; }
        public string VpAccountName { get; set; }
        public string VpLastFour { get; set; }

        public IDictionary<string, IList<string>> AvsCodes { get; set; }
        public IList<PpiResponseTypeEnum> ResponseTypes { get; set; }
        public bool IsPending { get; set; }
        public bool IsSuccess { get; set; }
        public bool IsFail { get; set; }
        public IList<string> AcceptedAvsCodes { get; set; }

        public AchFileTypeEnum? AchFileType { get; set; }
        public DateTime? AchProcessDate { get; set; }
        public AchReturnCodeEnum? AchReturnCode { get; set; }
        public decimal? AchReturnAmount { get; set; }
        public bool AchProcessed { get; set; }
    }
}
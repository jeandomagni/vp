﻿namespace Ivh.Application.Qat.Common.Dtos
{
    public class TextToPayPaymentDto
    {
        public decimal ActualPaymentAmount { get; set; }
        public int PaymentId { get; set; }
        public int PaymentAttemptCount { get; set; }
        public string CurrentPaymentStatus { get; set; }
    }
}
﻿namespace Ivh.Application.Qat.Common.Dtos
{
    using System.Collections.Generic;
    using Core.Common.Dtos;

    public class PersonaResultDto
    {
        public PersonaResultDto(IList<Ivh.Common.VisitPay.Messages.HospitalData.Dtos.HsGuarantorDto> hsGuarantorDtos, GuarantorDto vpGuarantorDto, int keepCurrentOffset)
        {
            this.HsGuarantorDtos = hsGuarantorDtos;
            this.VpGuarantorDto = vpGuarantorDto;
            this.KeepCurrentOffset = keepCurrentOffset;
        }

        public IList<Ivh.Common.VisitPay.Messages.HospitalData.Dtos.HsGuarantorDto> HsGuarantorDtos { get; }

        public GuarantorDto VpGuarantorDto { get; }
        public int KeepCurrentOffset { get; }
    }
}

﻿namespace Ivh.Application.Qat.Common.Dtos
{
    public class DemoVisitPayUserDto
    {
        /// <summary>
        /// The existing clone's user name that needs to be reset
        /// </summary>
        public string DemoVisitPayUserName { get; set; }
        public string PasswordHash { get; set; }
    }
}

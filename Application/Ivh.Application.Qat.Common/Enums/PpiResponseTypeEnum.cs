﻿namespace Ivh.Application.Qat.Common.Enums
{
    using Ivh.Common.Base.Enums;
    using Ivh.Common.VisitPay.Attributes;

    public static class PpiResponseTypeEnumCategory
    {
        public const string Sale = "sale";
        public const string Store = "store";
        public const string Ach = "ach";
        public const string Success = "success";
        public const string Card = "card";
        public const string Decline = "decline";
        public const string Error = "error";
    }

    public enum PpiResponseTypeEnum
    {
        [EnumCategory(PpiResponseTypeEnumCategory.Sale,PpiResponseTypeEnumCategory.Store,PpiResponseTypeEnumCategory.Ach,PpiResponseTypeEnumCategory.Success)]
        Accepted = 0,

        [EnumCategory(PpiResponseTypeEnumCategory.Sale,PpiResponseTypeEnumCategory.Store,PpiResponseTypeEnumCategory.Card,PpiResponseTypeEnumCategory.Success)]
        Approved = 1,


        [EnumCategory(PpiResponseTypeEnumCategory.Sale,PpiResponseTypeEnumCategory.Store,PpiResponseTypeEnumCategory.Card,PpiResponseTypeEnumCategory.Decline)]
        Decline = 2,

        [EnumCategory(PpiResponseTypeEnumCategory.Sale,PpiResponseTypeEnumCategory.Store,PpiResponseTypeEnumCategory.Card,PpiResponseTypeEnumCategory.Decline)]
        DeclineCall = 3,

        [EnumCategory(PpiResponseTypeEnumCategory.Sale,PpiResponseTypeEnumCategory.Store,PpiResponseTypeEnumCategory.Card,PpiResponseTypeEnumCategory.Decline)]
        DeclineCardError = 4,


        [EnumCategory(PpiResponseTypeEnumCategory.Sale,PpiResponseTypeEnumCategory.Store,PpiResponseTypeEnumCategory.Ach,PpiResponseTypeEnumCategory.Card,PpiResponseTypeEnumCategory.Error)]
        BadFormat = 5,

        [EnumCategory(PpiResponseTypeEnumCategory.Sale,PpiResponseTypeEnumCategory.Store,PpiResponseTypeEnumCategory.Ach,PpiResponseTypeEnumCategory.Card,PpiResponseTypeEnumCategory.Error)]
        CallToProcessorFailed = 6,

        [EnumCategory(PpiResponseTypeEnumCategory.Sale,PpiResponseTypeEnumCategory.Store,PpiResponseTypeEnumCategory.Ach,PpiResponseTypeEnumCategory.Card,PpiResponseTypeEnumCategory.Error)]
        FailToProcess = 7,

        [EnumCategory(PpiResponseTypeEnumCategory.Sale,PpiResponseTypeEnumCategory.Ach,PpiResponseTypeEnumCategory.Card,PpiResponseTypeEnumCategory.Error)]
        LinkFailure = 8,

        [EnumCategory(PpiResponseTypeEnumCategory.Sale,PpiResponseTypeEnumCategory.Store,PpiResponseTypeEnumCategory.Ach,PpiResponseTypeEnumCategory.Card,PpiResponseTypeEnumCategory.Error)]
        MissingData = 9,

        [EnumCategory(PpiResponseTypeEnumCategory.Sale,PpiResponseTypeEnumCategory.Ach,PpiResponseTypeEnumCategory.Card,PpiResponseTypeEnumCategory.Error)]
        NoSuchBillingId = 10,

        [EnumCategory(PpiResponseTypeEnumCategory.Sale,PpiResponseTypeEnumCategory.Store,PpiResponseTypeEnumCategory.Ach,PpiResponseTypeEnumCategory.Card,PpiResponseTypeEnumCategory.Error)]
        ProcessorUnreacheable404 = 11,

        [EnumCategory(PpiResponseTypeEnumCategory.Sale,PpiResponseTypeEnumCategory.Store,PpiResponseTypeEnumCategory.Ach,PpiResponseTypeEnumCategory.Card,PpiResponseTypeEnumCategory.Error)]
        ProcessorUnreacheable500 = 12,

        [EnumCategory(PpiResponseTypeEnumCategory.Sale,PpiResponseTypeEnumCategory.Store,PpiResponseTypeEnumCategory.Ach,PpiResponseTypeEnumCategory.Card,PpiResponseTypeEnumCategory.Error)]
        Rejected102 = 13,

        [EnumCategory(PpiResponseTypeEnumCategory.Sale,PpiResponseTypeEnumCategory.Store,PpiResponseTypeEnumCategory.Ach,PpiResponseTypeEnumCategory.Card,PpiResponseTypeEnumCategory.Error)]
        Rejected203 = 14,
    }
}
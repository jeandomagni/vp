﻿namespace Ivh.Application.SecureCommunication.Common.Interfaces
{
    using System.Collections.Generic;
    using Dtos;
    using Ivh.Common.Base.Interfaces;

    public interface ISupportTemplateApplicationService : IApplicationService
    {
        SupportTemplateDto GetSupportTemplate(int supportTemplateId, bool userCanManageGlobalTemplates);
        IList<SupportTemplateDto> GetSupportTemplates(int clientVpUserId, int guarantorVpUserId);
        SupportTemplateDto SaveSupportTemplate(SupportTemplateDto supportTemplate, int clientVpUserId, bool userCanManageGlobalTemplates);
        void DeleteSupportTemplate(int supportTemplateId, int clientVpUserId, bool userCanManageGlobalTemplates);
        IList<string> GetCommonTemplateReplacementTags();
    }
}

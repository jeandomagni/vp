﻿namespace Ivh.Application.SecureCommunication.Common.Interfaces
{
    using System.Collections.Generic;
    using Domain.Guarantor.Entities;
    using Dtos;
    using Ivh.Common.Base.Interfaces;
    using User.Common.Dtos;

    public interface ISupportTopicApplicationService : IApplicationService
    {
        IList<SupportTopicGroupItemDto> GetSupportTopicsGrouped();
        IList<SupportTopicGroupItemDto> GetSupportTopicsGroupedTemplates(int visitPayUserId, int clientVisitPayUserId);
        IDictionary<string, string> PopulateTemplateReplacementTags(Guarantor guarantor, VisitPayUserDto clientUserDto);
    }
}

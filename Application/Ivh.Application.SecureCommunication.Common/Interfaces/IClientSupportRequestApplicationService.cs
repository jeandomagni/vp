﻿namespace Ivh.Application.SecureCommunication.Common.Interfaces
{
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Dtos;
    using Ivh.Common.Base.Enums;
    using Ivh.Common.Base.Interfaces;
    using Ivh.Common.VisitPay.Enums;

    public interface IClientSupportRequestApplicationService : IApplicationService
    {
        ClientSupportRequestDto GetClientSupportRequest(int clientSupportRequestId);
        ClientSupportRequestResultsDto GetClientSupportRequests(ClientSupportRequestFilterDto filterDto);
        IList<ClientSupportTopicDto> GetClientSupportTopics(bool withTemplates);
        int GetUnreadMessagesCountFromClient(int? assignedVisitPayUserId);
        int GetUnreadMessagesCountToClient(int? visitPayUserId);
        IDictionary<int, string> GetAssignableUsers(bool assignedOnly, int? assignedVisitPayUserId);
        AttachmentDownloadDto PrepareDownload(int clientSupportRequestId, int? clientSupportRequestMessageId, int? clientSupportRequestMessageAttachmentId, bool showInternalAttachments);
        ClientSupportRequestDto CreateClientSupportRequest(CreateClientSupportRequestParameterDto createClientSupportRequestParameter);
        ClientSupportRequestMessageAttachmentDto SaveAttachment(ClientSupportRequestMessageAttachmentDto attachmentDto, int clientSupportRequestMessageId, int clientSupportRequestId);
        void DeleteAttachment(int clientSupportRequestId, int clientSupportRequestMessageAttachmentId);
        IList<string> SaveDetails(int clientSupportRequestId, int? assignedToVisitPayUserId, int? targetVpGuarantorId, string jiraId, int visitPayUserId);
        ClientSupportRequestMessageDto SaveMessage(int clientSupportRequestId, string messageBody, ClientSupportRequestMessageTypeEnum messageType, int visitPayUserId);
        void SetMessageRead(int clientSupportRequestId, ClientSupportRequestMessageTypeEnum clientSupportRequestMessageType);
        ClientSupportRequestDto SetSupportRequestStatus(int supportRequestId, int visitPayUserId, ClientSupportRequestStatusEnum clientSupportRequestStatus, string message);
        IDictionary<int, string> GetAllClientUsersWithSupportRequest();
        Task<byte[]> ExportClientAdminSupportRequestsAsync(ClientSupportRequestFilterDto filterDto, string userName);
    }
}
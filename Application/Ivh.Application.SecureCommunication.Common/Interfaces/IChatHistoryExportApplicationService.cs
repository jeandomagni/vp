﻿
namespace Ivh.Application.SecureCommunication.Common.Interfaces
{
    using System;
    using Ivh.Common.Base.Interfaces;
    using System.Threading.Tasks;
    using Dtos;

    public interface IChatHistoryExportApplicationService : IApplicationService
    {
        Task<byte[]> ExportChatHistoryOverviewAsync(ChatThreadHistoryOverviewsDto chatThreadHistoryOverviewsDto, string userName, Guid guarantorVisitPayUserGuid);
        Task<byte[]> ExportChatThreadHistoryAsync(ChatThreadHistoryDto chatThreadHistoryDto, string userName, Guid guarantorVisitPayUserGuid);
    }
}

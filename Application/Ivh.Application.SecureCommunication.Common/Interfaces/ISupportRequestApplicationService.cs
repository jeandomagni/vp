﻿namespace Ivh.Application.SecureCommunication.Common.Interfaces
{
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Dtos;
    using Ivh.Common.Base.Enums;
    using Ivh.Common.Base.Interfaces;
    using Ivh.Common.ServiceBus.Interfaces;
    using Ivh.Common.VisitPay.Enums;
    using Ivh.Common.VisitPay.Messages.Communication;

    public interface ISupportRequestApplicationService : IApplicationService,
        IUniversalConsumerService<SupportRequestRedactMessage>
    {
        SupportRequestDto CreateSupportRequest(SupportRequestCreateDto supportRequestCreateDto);

        SupportRequestDto GetSupportRequest(int supportRequestId, int visitPayUserId);

        SupportRequestResultsDto GetSupportRequests(SupportRequestFilterDto supportRequestFilterDto, int batch, int batchSize, int? visitPayUserId);

        int GetSupportRequestTotals(int visitPayUserId, SupportRequestFilterDto supportRequestFilterDto);

        void CloseSupportRequest(int supportRequestId, int visitPayUserId, string messageBody, int createdByVisitPayUserId);

        void ReopenSupportRequest(int supportRequestId, int visitPayUserId, string messageBody, int createdByVisitPayUserId);

        SupportRequestMessageDto SaveInternalNote(int supportRequestId, int visitPayUserId, string messageBody, int createdByVisitPayUserId);

        SupportRequestMessageDto SaveClientMessage(int supportRequestId, int visitPayUserId, string messageBody, int createdByVisitPayUserId);

        SupportRequestMessageDto SaveGuarantorMessage(int supportRequestId, int visitPayUserId, string messageBody, int createdByVisitPayUserId);

        void SaveDetails(int clientSupportRequestId, int visitPayUserId, int? assignedToVisitPayUserId, int actionVisitPayUserId);

        void DeleteAttachment(int supportRequestId, int supportRequestMessageAttachmentId, int actionVisitPayUserId);

        void SetSupportTopic(int supportRequestId, int? supportTopicId, int visitPayUserId);
        
        IList<SupportRequestMessageAttachmentDto> GetAttachments(int supportRequestId, int visitPayUserId, int? supportRequestMessageId, bool showInternalAttachments);

        void SaveMessageAttachment(SupportRequestMessageAttachmentDto supportRequestMessageAttachmentDto, int supportRequestMessageId, int supportRequestId, int visitPayUserId);

        AttachmentDownloadDto PrepareDownload(int supportRequestId, int? supportRequestMessageAttachmentId, int visitPayUserId, bool showInternalAttachments);

        void SetRead(int supportRequestId, int visitPayUserId, SupportRequestMessageTypeEnum supportRequestType);

        SupportRequestMessageDto GetClosedMessage(SupportRequestDto supportRequestDto);
        
        IDictionary<int, string> GetAssignableUsers(bool assignedOnly, int? assignedVisitPayUserId);

        int GetAllUnreadMessagesFromGuarantorsCount();

        void RedactSupportRequestsForVisit(int vpGuarantorId, int visitId);

        Task<byte[]> ExportSupportRequestsClientAsync(SupportRequestFilterDto filterDto, string userName);

        void SaveDraft(int supportRequestId, int visitPayUserId, string messageBody, int createdByVisitPayUserId);

        SupportRequestMessageDto GetDraftMessage(int supportRequestId, int createdByVisitPayUserId);

        void FlagForFollowUp(int supportRequestId, int visitPayUserId, bool flagForFollowUp, int createdByVisitPayUserId);

        int GetSupportRequestsWithUnreadMessagesToGuarantorCount(int vpGuarantorId);
    }
}

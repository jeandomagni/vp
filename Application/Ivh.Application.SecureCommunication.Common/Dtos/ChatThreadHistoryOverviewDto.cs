﻿
namespace Ivh.Application.SecureCommunication.Common.Dtos
{
    using System;

    public class ChatThreadHistoryOverviewDto
    {
        public string ThreadStatus { get; set; }
        public DateTime ThreadDateCreated { get; set; }
        public DateTime? ThreadDateClosed { get; set; }
        public Guid ThreadGuid { get; set; }
        public string GuarantorName { get; set; }
        public string OperatorName { get; set; }
    }
}

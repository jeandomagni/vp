﻿namespace Ivh.Application.SecureCommunication.Common.Dtos
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Core.Common.Dtos;
    using Ivh.Common.Base.Enums;
    using Ivh.Common.Base.Utilities.Helpers;
    using Ivh.Common.VisitPay.Enums;
    using User.Common.Dtos;

    public class SupportRequestDto
    {
        private IList<SupportRequestMessageDto> _supportRequestMessages;

        public int SupportRequestId { get; set; }
        public string SupportRequestDisplayId { get; set; }
        public SupportTopicDto SupportTopic { get; set; }
        public SupportRequestStatusEnum SupportRequestStatus { get; set; }
        public SupportRequestStatusDto SupportRequestStatusSource { get; set; }
        public GuarantorDto VpGuarantor { get; set; }
        public DateTime InsertDate { get; set; }
        public VisitDto Visit { get; set; }
        public string PatientLastName { get; set; }
        public string PatientFirstName { get; set; }
        public bool IsFlaggedForFollowUp { get; set; }
        public TreatmentLocationDto TreatmentLocation { get; set; }
        public DateTime? TreatmentDate { get; set; }
        public DateTime? ClosedDate { get; set; }
        public GuarantorDto SubmittedForVpGuarantor { get; set; }
        public VisitPayUserDto AssignedVisitPayUser { get; set; }
        public IList<string> FacilitySourceSystemKeys { get; set; }

        public IList<SupportRequestMessageDto> SupportRequestMessages 
        {
            get { return this._supportRequestMessages ?? (this.SupportRequestMessages = new List<SupportRequestMessageDto>()); }
            set { this._supportRequestMessages = value; }
        }

        public IList<SupportRequestMessageDto> GuarantorMessages
        {
            get
            {
                return this.SupportRequestMessages
                           .Where(x => x.SupportRequestMessageType == SupportRequestMessageTypeEnum.FromGuarantor ||
                                       x.SupportRequestMessageType == SupportRequestMessageTypeEnum.ToGuarantor)
                           .ToList();
            }
        }

        public IList<SupportRequestMessageDto> InternalMessages
        {
            get
            {
                return this.SupportRequestMessages
                           .Where(x => x.SupportRequestMessageType == SupportRequestMessageTypeEnum.CloseMessage ||
                                       x.SupportRequestMessageType == SupportRequestMessageTypeEnum.InternalNote ||
                                       x.SupportRequestMessageType == SupportRequestMessageTypeEnum.OpenMessage)
                           .ToList();
            }
        }

        public string SupportTopicDisplayString { get; set; }

        public string PatientLastNameFirstName => FormatHelper.LastNameFirstName(this.PatientLastName, this.PatientFirstName);
        public string PatientFirstNameLastName => FormatHelper.FirstNameLastName(this.PatientFirstName, this.PatientLastName);
    }
}

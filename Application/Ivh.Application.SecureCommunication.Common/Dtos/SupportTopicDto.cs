﻿namespace Ivh.Application.SecureCommunication.Common.Dtos
{
    using System.Collections.Generic;
    using Content.Common.Dtos;
    using Core.Common.Dtos;

    public class SupportTopicDto
    {
        private IList<CmsVersionDto> _templates;
        
        public int SupportTopicId { get; set; }
        public string SupportTopicName { get; set; }
        public bool IsActive { get; set; }
        public int SortOrder { get; set; }
        public int SupportTopicLevel { get; set; }
        public int? ParentSupportTopicId { get; set; }
        public SupportTopicDto ParentSupportTopic { get; set; }

        public IList<CmsVersionDto> Templates
        {
            get { return this._templates ?? (this.Templates = new List<CmsVersionDto>()); }
            set { this._templates = value; }
        }
    }
}

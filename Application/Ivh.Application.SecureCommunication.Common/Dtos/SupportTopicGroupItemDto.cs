﻿namespace Ivh.Application.SecureCommunication.Common.Dtos
{
    using System.Collections.Generic;

    public class SupportTopicGroupItemDto
    {
        public SupportTopicGroupItemDto()
        {
            this.Items = new List<SupportTopicGroupItemDto>();
            this.Templates = new List<SupportTopicTemplateDto>();
        }

        public int SupportTopicId { get; set; }
        public string SupportTopicName { get; set; }
        public IList<SupportTopicGroupItemDto> Items { get; set; }
        public IList<SupportTopicTemplateDto> Templates { get; set; }
    }
}

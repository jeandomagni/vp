﻿namespace Ivh.Application.SecureCommunication.Common.Dtos
{
    using Ivh.Common.Base.Enums;
    using Ivh.Common.VisitPay.Enums;

    public class ClientSupportRequestStatusDto
    {
        public int ClientSupportRequestStatusId { get; set; }
        public string ClientSupportRequestStatusName { get; set; }
        public ClientSupportRequestStatusEnum ClientSupportRequestStatusEnum { get; set; }
    }
}
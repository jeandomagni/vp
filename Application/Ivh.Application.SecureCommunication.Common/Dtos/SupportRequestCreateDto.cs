﻿namespace Ivh.Application.SecureCommunication.Common.Dtos
{
    public class SupportRequestCreateDto
    {
        public int? AssignedVisitPayUserId { get; set; }
        public int CreatedByVisitPayUserId { get; set; }
        public string MessageBody { get; set; }
        public int? SubmittedForVisitPayUserId { get; set; }
        public int? SupportTopicId { get; set; }
        public int? TreatmentLocationId { get; set; }
        public int? VisitId { get; set; }
        public int VisitPayUserId { get; set; }
    }
}
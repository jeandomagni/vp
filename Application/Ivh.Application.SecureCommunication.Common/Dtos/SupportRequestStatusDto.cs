﻿namespace Ivh.Application.SecureCommunication.Common.Dtos
{
    using Ivh.Common.Base.Enums;
    using Ivh.Common.VisitPay.Enums;

    public class SupportRequestStatusDto
    {
        public int SupportRequestStatusId { get; set; }
        public string SupportRequestStatusName { get; set; }

        public SupportRequestStatusEnum SupportRequestStatusEnum
        {
            get { return (SupportRequestStatusEnum)this.SupportRequestStatusId; }
        }
    }
}

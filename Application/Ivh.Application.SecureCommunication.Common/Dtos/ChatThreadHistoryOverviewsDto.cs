﻿
namespace Ivh.Application.SecureCommunication.Common.Dtos
{
    using System.Collections.Generic;

    public class ChatThreadHistoryOverviewsDto
    {
        public IList<ChatThreadHistoryOverviewDto> ThreadHistoryOverviews { get; set; }
        public int Page { get; set; }
        public int Total { get; set; }
        public int Records { get; set; }
    }
}

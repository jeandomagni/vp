﻿namespace Ivh.Application.SecureCommunication.Common.Dtos
{
    using System;
    using System.Collections.Generic;
    using Core.Common.Dtos;
    using Ivh.Common.Base.Enums;
    using Ivh.Common.VisitPay.Enums;
    using User.Common.Dtos;

    public class ClientSupportRequestDto
    {
        public ClientSupportRequestDto()
        {
            this.ClientSupportRequestMessages = new List<ClientSupportRequestMessageDto>();
        }

        public int ClientSupportRequestId { get; set; }
        public string ClientSupportRequestDisplayId { get; set; }
        public ClientSupportTopicDto ClientSupportTopic { get; set; }
        public ClientSupportRequestStatusEnum ClientSupportRequestStatus { get; set; }
        public ClientSupportRequestStatusDto ClientSupportRequestStatusSource { get; set; }
        public string ContactPhone { get; set; }
        public DateTime InsertDate { get; set; }
        public string JiraId { get; set; }
        public DateTime? ClosedDate { get; set; }
        public VisitPayUserDto VisitPayUser { get; set; }
        public VisitPayUserDto AssignedVisitPayUser { get; set; }
        public int? TargetVpGuarantorId { get; set; }
        public IList<ClientSupportRequestMessageDto> ClientSupportRequestMessages { get; set; }
        public DateTime? LastModifiedDate { get; set; }
        public VisitPayUserDto LastModifiedByVisitPayUser { get; set; }
        public string TopicDisplay { get; set; }
        public int UnreadMessageCountFromClient { get; set; }
        public int UnreadMessageCountToClient { get; set; }
        public int AttachmentCountClient { get; set; }
        public int AttachmentCountInternal { get; set; }
        public string MessagePreview { get; set; }
        public IList<ClientSupportRequestMessageDto> MessagesClient { get; set; }
        public IList<ClientSupportRequestMessageDto> MessagesInternal { get; set; }
    }
}
﻿namespace Ivh.Application.SecureCommunication.Common.Dtos
{
    using System;
    using System.Collections.Generic;
    using Core.Common.Dtos;
    using Ivh.Common.Base.Enums;
    using Ivh.Common.VisitPay.Enums;
    using User.Common.Dtos;

    public class ClientSupportRequestMessageDto
    {
        public ClientSupportRequestMessageDto()
        {
            this.ClientSupportRequestMessageAttachments = new List<ClientSupportRequestMessageAttachmentDto>();
        }

        public int ClientSupportRequestMessageId { get; set; }
        public ClientSupportRequestDto ClientSupportRequest { get; set; }
        public ClientSupportRequestMessageTypeEnum ClientSupportRequestMessageType { get; set; }
        public string MessageBody { get; set; }
        public bool IsRead { get; set; }
        public VisitPayUserDto CreatedByVisitPayUser { get; set; }
        public DateTime InsertDate { get; set; }

        public IList<ClientSupportRequestMessageAttachmentDto> ClientSupportRequestMessageAttachments { get; set; }

        public IList<ClientSupportRequestMessageAttachmentDto> ActiveClientSupportRequestMessageAttachments { get; set; }

        public int AttachmentCount { get; set; }
    }
}
﻿namespace Ivh.Application.SecureCommunication.Common.Dtos
{
    using System;
    using System.Collections.Generic;
    using Ivh.Common.Base.Enums;
    using Ivh.Common.VisitPay.Enums;
    using User.Common.Dtos;

    public class SupportRequestMessageDto
    {
        private VisitPayUserDto _visitPayUserDto;
        private IList<SupportRequestMessageAttachmentDto> _supportRequestMessageAttachments;

        public int SupportRequestMessageId { get; set; }
        public SupportRequestDto SupportRequest { get; set; }
        public SupportRequestMessageTypeEnum SupportRequestMessageType { get; set; }
        public string MessageBody { get; set; }
        public bool IsRead { get; set; }
        public int CreatedByVpUserId { get; set; }
        public VisitPayUserDto VisitPayUser 
        { 
            get { return this._visitPayUserDto ?? (this.VisitPayUser = new VisitPayUserDto()); }
            set { this._visitPayUserDto = value; }
        }
        public DateTime InsertDate { get; set; }

        public IList<SupportRequestMessageAttachmentDto> SupportRequestMessageAttachments 
        {
            get { return this._supportRequestMessageAttachments ?? (this.SupportRequestMessageAttachments = new List<SupportRequestMessageAttachmentDto>()); }
            set { this._supportRequestMessageAttachments = value; }
        }

        public IList<SupportRequestMessageAttachmentDto> ActiveSupportRequestMessageAttachments { get; set; }
    }
}

﻿
namespace Ivh.Application.SecureCommunication.Common.Dtos
{
    using System;
    using System.Collections.Generic;

    public class ChatThreadHistoryDto
    {
        public Guid ThreadId { get; set; }
        public DateTime ThreadDateCreated { get; set; }
        public DateTime? ThreadDateClosed { get; set; }
        public string ThreadStatus { get; set; }
        public IList<ChatThreadUserDto> ThreadUsers { get; set; }
        public IList<ChatThreadMessageDto> MessageHistory { get; set; }
    }
}

﻿namespace Ivh.Application.SecureCommunication.Common.Dtos
{
    using System;

    [Serializable]
    public class AttachmentDownloadDto
    {
        public byte[] Bytes { get; set; }
        public string MimeType { get; set; }
        public string FileName { get; set; }
    }
}

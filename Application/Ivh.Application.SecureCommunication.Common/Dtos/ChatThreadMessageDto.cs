﻿
namespace Ivh.Application.SecureCommunication.Common.Dtos
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web.Mvc;
    using Newtonsoft.Json;

    public class ChatThreadMessageDto
    {
        public Guid MessageGuid { get; set; }
        public int? SenderUserId { get; set; }
        public DateTime DateSent { get; set; }
        [AllowHtml]
        public string Body { get; set; }
        [AllowHtml]
        public string Content { get; set; }
        public string MessageType { get; set; }

        public ChatThreadMessageContentDto ContentDto
        {
            get
            {
                ChatThreadMessageContentDto dto = new ChatThreadMessageContentDto();
                if (!string.IsNullOrEmpty(this.Content))
                {
                    dto = JsonConvert.DeserializeObject<ChatThreadMessageContentDto>(this.Content);
                }

                return dto;
            }
        }

        public string SendingUserName(IList<ChatThreadUserDto> threadUsers)
        {
            if (this.SenderUserId.HasValue)
            {
                ChatThreadUserDto sendingUser = threadUsers.SingleOrDefault(u => u.Id == this.SenderUserId.Value);
                return sendingUser.FullName;
            }
            else
            {
                //If no senderUserId then the message was sent by the system
                return "System";
            }
        }
    }
}

﻿namespace Ivh.Application.SecureCommunication.Common.Dtos
{
    using System.Collections.Generic;

    public class SupportRequestResultsDto
    {
        public IReadOnlyList<SupportRequestDto> SupportRequests { get; set; }
        public int TotalRecords { get; set; }
    }
}

﻿namespace Ivh.Application.SecureCommunication.Common.Dtos
{
    using Ivh.Common.VisitPay.Enums;

    public class ClientSupportRequestMessageAttachmentDto
    {
        public int ClientSupportRequestMessageAttachmentId { get; set; }
        public ClientSupportRequestMessageDto ClientSupportRequestMessage { get; set; }
        public string AttachmentFileName { get; set; }
        public string MimeType { get; set; }
        public int FileSize { get; set; }
        public byte[] FileContent { get; set; }
        public byte[] QuarantinedBytes { get; set; }
        public MalwareScanStatusEnum MalwareScanStatus { get; set; }
    }
}
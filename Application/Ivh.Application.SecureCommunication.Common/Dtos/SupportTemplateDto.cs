﻿
namespace Ivh.Application.SecureCommunication.Common.Dtos
{
    public class SupportTemplateDto
    {
        public int SupportTemplateId { get; set; }

        public int? VisitPayUserId { get; set; }

        public string Title { get; set; }

        public string Content { get; set; }
    }
}

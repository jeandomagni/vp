﻿namespace Ivh.Application.SecureCommunication.Common.Dtos
{
    using Ivh.Common.VisitPay.Enums;

    public class SupportRequestMessageAttachmentDto
    {
        public int SupportRequestMessageAttachmentId { get; set; }
        public SupportRequestMessageDto SupportRequestMessage { get; set; }
        public string AttachmentFileName { get; set; }
        public string MimeType { get; set; }
        public int FileSize { get; set; }
        public byte[] FileContent { get; set; }
        public byte[] QuarantinedBytes { get; set; }
        public MalwareScanStatusEnum MalwareScanStatus { get; set; }
    }
}

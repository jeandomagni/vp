﻿
namespace Ivh.Application.SecureCommunication.Common.Dtos
{
    using System;
    using System.Web.Mvc;

    public class ChatThreadMessageContentDto
    {
        [AllowHtml]
        public string Message { get; set; }
        public string FileName { get; set; }
        public string MimeType { get; set; }
        public Guid AttachmentId { get; set; }
        public int FileSize { get; set; }
        public Guid UserToken { get; set; }
    }
}

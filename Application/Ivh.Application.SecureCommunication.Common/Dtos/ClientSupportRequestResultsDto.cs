﻿namespace Ivh.Application.SecureCommunication.Common.Dtos
{
    using System.Collections.Generic;

    public class ClientSupportRequestResultsDto
    {
        public IReadOnlyList<ClientSupportRequestDto> ClientSupportRequests { get; set; }
        public int TotalRecords { get; set; }
    }
}
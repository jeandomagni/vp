﻿
namespace Ivh.Application.SecureCommunication.Common.Dtos
{
    using System;

    public class ChatThreadUserDto
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public bool IsGuarantor { get; set; }
        public bool IsOperator { get; set; }
        public Guid UserToken { get; set; }

        public string FullName => $"{this.FirstName} {this.LastName}";
    }
}

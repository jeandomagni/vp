﻿namespace Ivh.Application.SecureCommunication.Common.Dtos
{
    using System.Collections.Generic;
    using Content.Common.Dtos;

    public class ClientSupportTopicDto
    {
        public ClientSupportTopicDto()
        {
            this.Templates = new List<CmsVersionDto>();
        }

        public int ClientSupportTopicId { get; set; }
        public string ClientSupportTopicName { get; set; }
        public bool IsActive { get; set; }
        public int SortOrder { get; set; }
        public ClientSupportTopicDto ParentClientSupportTopic { get; set; }
        public IList<CmsVersionDto> Templates { get; set; }
    }
}

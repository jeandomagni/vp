﻿namespace Ivh.Application.SecureCommunication.Common.Dtos
{
    using Ivh.Common.VisitPay.Enums;
    using System;

    public class SupportRequestFilterDto
    {
        public SupportRequestStatusEnum? SupportRequestStatus { get; set; }
        public DateTime? DateRangeFrom { get; set; }
        public string SupportRequestDisplayId { get; set; }
        public int? AssignedToVisitPayUserId { get; set; }
        public bool? ReadMessages { get; set; }
        public bool? UnreadMessages { get; set; }
        public string SortField { get; set; }
        public string SortOrder { get; set; }
        public string FacilitySourceSystemKey { get; set; }
    }
}

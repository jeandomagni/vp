﻿namespace Ivh.Application.SecureCommunication.Common.Dtos
{
    public class CreateClientSupportRequestParameterDto
    {
        public int ClientSupportTopicId { get; set; }
        public string ContactPhone { get; set; }
        public string MessageBody { get; set; }
        public bool IsCreatedOnBehalfOfClient { get; set; }
        public int IvinciAdminUserId { get; set; } = -1;
        public int RequestorUserId { get; set; }
        public string CreatedOnBefalfOfClientInternalNote { get; set; }
        public int? ReferenceGuarantorSupportRequestId { get; set; }
    }
}
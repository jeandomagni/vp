﻿namespace Ivh.Application.SecureCommunication.Common.Dtos
{
    public class SupportTopicTemplateDto
    {
        public string Content { get; set; }
        public string Title { get; set; }
    }
}

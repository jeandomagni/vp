﻿namespace Ivh.Application.AppIntelligence.Common.Dtos
{
    using Ivh.Common.Base.Enums;
    using Ivh.Common.VisitPay.Enums;

    public class RandomizedTestGroupDto
    {
        public virtual int RandomizedTestGroupId { get; set; }
        public virtual RandomizedTestEnum RandomizedTest { get; set; }
    }
}
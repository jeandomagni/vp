﻿namespace Ivh.Application.AppIntelligence.Common.Interfaces
{
    using Dtos;
    using Ivh.Common.Base.Enums;
    using Ivh.Common.Base.Interfaces;
    using Ivh.Common.VisitPay.Enums;

    public interface IRandomizedTestApplicationService : IApplicationService
    {
        RandomizedTestGroupDto GetRandomizedTestGroupMembership(RandomizedTestEnum randomizedTest, int vpGuarantorId, bool assignIfNotAssigned, RandomizedTestGroupEnum? specificRandomizedTestGroup = null);
    }
}
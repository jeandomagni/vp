﻿namespace Ivh.Application.AppIntelligence.Common.Interfaces
{
    using Ivh.Common.ServiceBus.Interfaces;
    using Ivh.Common.VisitPay.Messages.AppIntelligence;

    public interface IFinancePlanIntelligenceApplicationService :
        IUniversalConsumerService<FinancePlanTermsDisplayedMessage>
    {
    }
}
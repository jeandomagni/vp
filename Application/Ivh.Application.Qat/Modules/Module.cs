﻿namespace Ivh.Application.Qat.Modules
{
    using ApplicationServices;
    using Autofac;
    using Common.Interfaces;
    using Domain.Qat.Interfaces;
    using Domain.Qat.Services;
    using Ivh.Common.Base.Utilities.Helpers;
    using Ivh.Provider.Qat;

    public class Module : Autofac.Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            if (IvhEnvironment.IsDevelopmentOrQualityAssurance || 
                IvhEnvironment.IsNonProduction) // <= for automation on int01 (12/11/2017)
            {
                builder.RegisterType<PaymentProcessorInterceptorApplicationService>().As<IPaymentProcessorInterceptorApplicationService>();
                builder.RegisterType<CommunicationInterceptorApplicationService>().As<ICommunicationInterceptorApplicationService>();
                builder.RegisterType<AchInterceptorApplicationService>().As<IAchInterceptorApplicationService>();
                builder.RegisterType<CloneGuarantorApplicationService>().As<ICloneGuarantorApplicationService>();
                builder.RegisterType<IhcEnrollmentApplicationService>().As<IIhcEnrollmentApplicationService>();
                builder.RegisterType<PersonasApplicationService>().As<IPersonasApplicationService>();
                builder.RegisterType<PersonaCreationApplicationService>().As<IPersonaCreationApplicationService>();
                builder.RegisterType<TestPaymentMethodsApplicationService>().As<ITestPaymentMethodsApplicationService>();
                builder.RegisterType<MockHealthEquityEndpointApplicationService>().As<IMockHealthEquityEndpointApplicationService>();
                builder.RegisterType<MockHealthEquityEndpointService>().As<IMockHealthEquityEndpointService>();
                builder.RegisterType<SavingsAccountInfoRepository>().As<ISavingsAccountInfoRepository>();
                builder.RegisterType<QaAgingApplicationService>().As<IQaAgingApplicationService>();
                builder.RegisterType<QaImageApplicationService>().As<IQaImageApplicationService>();
                builder.RegisterType<QaSettingsApplicationService>().As<IQaSettingsApplicationService>();
                builder.RegisterType<QaTextToPayApplicationService>().As<IQaTextToPayApplicationService>();
                builder.RegisterType<QaSsoApplicationService>().As<IQaSsoApplicationService>();
                builder.RegisterType<AppBaseDataService>().As<IAppBaseDataService>();
                builder.RegisterAssemblyModules(typeof(Ivh.Provider.Qat.Modules.Module).Assembly);
            }
        }
    }
}
﻿namespace Ivh.Application.Qat.DataBuilders
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Linq;
    using System.Threading;
    using System.Threading.Tasks;
    using System.Web;
    using AppIntelligence.Common.Interfaces;
    using AutoMapper;
    using Common.Dtos;
    using Common.Interfaces;
    using Content.Common.Dtos;
    using Content.Common.Interfaces;
    using Core.Common.Dtos;
    using Core.Common.Interfaces;
    using Domain.Eob.Entities;
    using Domain.FinanceManagement.Payment.Entities;
    using Domain.FinanceManagement.Statement.Entities;
    using Domain.FinanceManagement.Statement.Interfaces;
    using Domain.Guarantor.Entities;
    using Domain.Guarantor.Interfaces;
    using Domain.HospitalData.Eob.Entities;
    using Domain.HospitalData.Eob.Interfaces;
    using Domain.HospitalData.HsGuarantor.Entities;
    using Domain.HospitalData.Visit.Entities;
    using Domain.HospitalData.Visit.Interfaces;
    using Domain.Qat.Entities;
    using Domain.Qat.Interfaces;
    using Domain.Qat.Services;
    using Domain.User.Entities;
    using FinanceManagement.Common.Dtos;
    using FinanceManagement.Common.Interfaces;
    using HospitalData.Common.Interfaces;
    using Ivh.Application.Matching.Common.Interfaces;
    using Ivh.Common.Base.Utilities.Extensions;
    using Ivh.Common.Data;
    using Ivh.Common.Data.Connection.Connections;
    using Ivh.Common.Data.Interfaces;
    using Ivh.Common.EventJournal;
    using Ivh.Common.VisitPay.Constants;
    using Ivh.Common.VisitPay.Enums;
    using Ivh.Common.VisitPay.Messages.HospitalData.Dtos;
    using Ivh.Common.VisitPay.Messages.Matching;
    using NHibernate;
    using NHibernate.Util;
    using SecureCommunication.Common.Dtos;
    using SecureCommunication.Common.Interfaces;
    using User.Common.Dtos;
    using EobClaimAdjustmentReasonCode = Domain.HospitalData.Eob.Entities.EobClaimAdjustmentReasonCode;
    using HsGuarantorDto = Ivh.Common.VisitPay.Messages.HospitalData.Dtos.HsGuarantorDto;
    using IChangeSetApplicationService = HospitalData.Common.Interfaces.IChangeSetApplicationService;
    using IHsGuarantorApplicationService = HospitalData.Common.Interfaces.IHsGuarantorApplicationService;
    using IRedactionApplicationService = Core.Common.Interfaces.IRedactionApplicationService;

    internal class BaselineDataBuilder
    {
        public static string CoPaymentTransactionDescription = "Copayment";
        public const int DemoPhysicianFacilityIdA = -100;
        public const int DemoHospitalFacilityIdA = -101;
        public const int DemoPhysicianFacilityIdB = -102;
        public const int DemoHospitalFacilityIdB = -103;
        public const int AetnaInsurancePlanId = 3;

        private const int SleepSeconds = 3;

        private readonly Lazy<IChangeEventApplicationService> _changeEventApplicationService;
        private readonly Lazy<IChangeSetApplicationService> _changeSetApplicationService;
        private readonly Lazy<IContentApplicationService> _contentApplicationService;
        private readonly Lazy<IGuarantorService> _guarantorService;
        private readonly Lazy<IFinancePlanApplicationService> _financePlanApplicationService;
        private readonly Lazy<IManagedUserApplicationService> _managedUserApplicationService;
        private readonly Lazy<IManagingUserApplicationService> _managingUserApplicationService;
        private readonly Lazy<IPaymentMethodsApplicationService> _paymentMethodsApplicationService;
        private readonly Lazy<IHsGuarantorApplicationService> _hsGuarantorApplicationService;
        private readonly Lazy<IFeatureApplicationService> _featureApplicationService;
        private readonly Lazy<IPaymentSubmissionApplicationService> _paymentSubmissionApplicationService;
        private readonly Lazy<IRandomizedTestApplicationService> _randomizedTestApplicationService;
        private readonly Lazy<IScheduledPaymentApplicationService> _scheduledPaymentApplicationService;
        private readonly Lazy<IStatementApplicationService> _statementApplicationService;
        private readonly Lazy<IStatementCreationApplicationService> _statementCreationApplicationService;
        private readonly Lazy<ISupportRequestApplicationService> _supportRequestApplicationService;
        private readonly Lazy<ITestPaymentMethodsApplicationService> _testPaymentMethodsApplicationService;
        private readonly Lazy<IVisitPayUserApplicationService> _visitPayUserApplicationService;
        private readonly Lazy<IVisitPayUserIssueResultApplicationService> _visitPayUserIssueResultApplicationService;
        private readonly Lazy<IVisitApplicationService> _visitApplicationService;
        private readonly Lazy<IVpStatementService> _vpStatementService;
        private readonly Lazy<IVisitRepository> _visitRepository;
        private readonly Lazy<IVisitService> _hsVisitService;
        private readonly Lazy<Ivh.Domain.HospitalData.HsGuarantor.Interfaces.IHsGuarantorService> _hsGuarantorService;
        private readonly Lazy<IVpGuarantorKeepCurrentRepository> _vpGuarantorKeepCurrentRepository;
        private readonly Lazy<IHsGuarantorKeepCurrentRepository> _hsGuarantorKeepCurrentRepository;
        private readonly Lazy<IEob835Service> _eob835Service;
        private readonly Lazy<IEobClaimAdjustmentReasonCodeRepository> _remittanceCodeRepository;
        private readonly Lazy<IEobPayerFilterRepository> _eobPayerFilterRepository;
        private readonly Lazy<IQaAgingApplicationService> _qaAgingApplicationService;
        private readonly Lazy<IQaPaymentService> _qaPaymentService;
        private readonly Lazy<IIhcEnrollmentService> _ihcEnrollmentService;
        private readonly Lazy<IStatementExportApplicationService> _statementExportApplicationService;
        private readonly Lazy<IAppBaseDataService> _appBaseDataService;
        private readonly Lazy<IMatchingApplicationService> _matchingApplicationService;
        private readonly Lazy<IGuarantorApplicationService> _guarantorApplicationService;
        private readonly Lazy<IRedactionApplicationService> _redactionApplicationService;

        private readonly ISession _session;

        private IList<HsGuarantorDto> _hsGuarantorDtos = new List<HsGuarantorDto>();
        //public HsGuarantorDto HsGuarantorDto;
        public GuarantorDto VpGuarantorDto;
        private VisitPayUserDto _visitPayUserDto;
        private int? _supportRequestId;

        public BaselineDataBuilder(
            Lazy<IChangeSetApplicationService> changeSetApplicationService,
            Lazy<IChangeEventApplicationService> changeEventApplicationService,
            Lazy<IContentApplicationService> contentApplicationService,
            Lazy<IGuarantorService> guarantorService,
            Lazy<IHsGuarantorApplicationService> hsGuarantorApplicationService,
            Lazy<IFeatureApplicationService> featureApplicationService,
            Lazy<IFinancePlanApplicationService> financePlanApplicationService,
            Lazy<IManagedUserApplicationService> managedUserApplicationService,
            Lazy<IManagingUserApplicationService> managingUserApplicationService,
            Lazy<IPaymentMethodsApplicationService> paymentMethodsApplicationService,
            Lazy<IPaymentSubmissionApplicationService> paymentSubmissionApplicationService,
            Lazy<IRandomizedTestApplicationService> randomizedTestApplicationService,
            Lazy<IScheduledPaymentApplicationService> scheduledPaymentApplicationService,
            Lazy<IStatementApplicationService> statementApplicationService,
            Lazy<IStatementCreationApplicationService> statementCreationApplicationService,
            Lazy<ISupportRequestApplicationService> supportRequestApplicationService,
            Lazy<ITestPaymentMethodsApplicationService> testPaymentMethodsApplicationService,
            Lazy<IVisitPayUserApplicationService> visitPayUserApplicationService,
            Lazy<IVisitPayUserIssueResultApplicationService> visitPayUserIssueResultApplicationService,
            Lazy<IVisitApplicationService> visitApplicationService,
            Lazy<IVpStatementService> vpStatementService,
            Lazy<IVisitRepository> visitRepository,
            Lazy<IVisitService> hsVisitService,
            Lazy<Ivh.Domain.HospitalData.HsGuarantor.Interfaces.IHsGuarantorService> hsGuarantorService,
            Lazy<IVpGuarantorKeepCurrentRepository> vpGuarantorKeepCurrentRepository,
            Lazy<IHsGuarantorKeepCurrentRepository> hsGuarantorKeepCurrentRepository,
            Lazy<IEob835Service> eob835Service,
            Lazy<IEobClaimAdjustmentReasonCodeRepository> remittanceCodeRepository,
            Lazy<IEobPayerFilterRepository> eobPayerFilterRepository,
            Lazy<IQaAgingApplicationService> qaAgingApplicationService,
            Lazy<IQaPaymentService> qaPaymentService,
            Lazy<IIhcEnrollmentService> ihcEnrollmentService,
            Lazy<IStatementExportApplicationService> statementExportApplicationService,
            Lazy<IAppBaseDataService> appBaseDataService,
            Lazy<IMatchingApplicationService> matchingApplicationService,
            Lazy<IGuarantorApplicationService> guarantorApplicationService,
            Lazy<IRedactionApplicationService> redactionApplicationService,
            ISessionContext<VisitPay> sessionContext)
        {
            this._changeSetApplicationService = changeSetApplicationService;
            this._changeEventApplicationService = changeEventApplicationService;
            this._contentApplicationService = contentApplicationService;
            this._guarantorService = guarantorService;
            this._hsGuarantorApplicationService = hsGuarantorApplicationService;
            this._featureApplicationService = featureApplicationService;
            this._financePlanApplicationService = financePlanApplicationService;
            this._managedUserApplicationService = managedUserApplicationService;
            this._managingUserApplicationService = managingUserApplicationService;
            this._paymentMethodsApplicationService = paymentMethodsApplicationService;
            this._paymentSubmissionApplicationService = paymentSubmissionApplicationService;
            this._randomizedTestApplicationService = randomizedTestApplicationService;
            this._scheduledPaymentApplicationService = scheduledPaymentApplicationService;
            this._statementApplicationService = statementApplicationService;
            this._statementCreationApplicationService = statementCreationApplicationService;
            this._supportRequestApplicationService = supportRequestApplicationService;
            this._testPaymentMethodsApplicationService = testPaymentMethodsApplicationService;
            this._visitPayUserApplicationService = visitPayUserApplicationService;
            this._visitPayUserIssueResultApplicationService = visitPayUserIssueResultApplicationService;
            this._visitApplicationService = visitApplicationService;
            this._vpStatementService = vpStatementService;
            this._visitRepository = visitRepository;
            this._hsVisitService = hsVisitService;
            this._hsGuarantorService = hsGuarantorService;
            this._vpGuarantorKeepCurrentRepository = vpGuarantorKeepCurrentRepository;
            this._hsGuarantorKeepCurrentRepository = hsGuarantorKeepCurrentRepository;
            this._eob835Service = eob835Service;
            this._remittanceCodeRepository = remittanceCodeRepository;
            this._eobPayerFilterRepository = eobPayerFilterRepository;
            this._qaAgingApplicationService = qaAgingApplicationService;
            this._qaPaymentService = qaPaymentService;
            this._ihcEnrollmentService = ihcEnrollmentService;
            this._statementExportApplicationService = statementExportApplicationService;
            this._appBaseDataService = appBaseDataService;
            this._matchingApplicationService = matchingApplicationService;
            this._guarantorApplicationService = guarantorApplicationService;
            this._redactionApplicationService = redactionApplicationService;
            this._session = sessionContext.Session;
        }

        public BaselineDataBuilder CreateGuarantor(HsGuarantorDto hsGuarantorDto)
        {
            this._session.Clear();

            // figure out which billing system belongs to the most records in the app because i don't know how else to do this.
            List<int> billingSystemIds = this._session.Query<HsGuarantorMap>().Select(x => x.BillingSystem.BillingSystemId).ToList();
            int billingSystemId = billingSystemIds.GroupBy(x => x).OrderByDescending(x => x.Count()).Select(x => x).First().Key;
            hsGuarantorDto.HsBillingSystemId = billingSystemId;

            // add to cdi base
            HsGuarantorDto newHsGuarantorDto = this._hsGuarantorApplicationService.Value.AddHsGuarantor(hsGuarantorDto);
            this.AddHsGuarantorDto(newHsGuarantorDto);

            // add to app base
            this._appBaseDataService.Value.AddHsGuarantor(hsGuarantorDto.SourceSystemKey, hsGuarantorDto.HsBillingSystemId);

            this.PopulateMatchRegistry(newHsGuarantorDto.HsGuarantorId);

            // if the guarantor has been created, try to match this new hsGuarantor so its visits will load
            if(this.VpGuarantorDto?.VpGuarantorId != null)
            {
                this.FindNewMatches();
            }

            return this;
        }

        public BaselineDataBuilder UseGuarantor(string hsGuarantorSourceSystemKey)
        {
            this._session.Clear();
            
            IReadOnlyList<HsGuarantor> hsGuarantors = this._hsGuarantorService.Value.GetHsGuarantors(null, hsGuarantorSourceSystemKey, null, null, null, null);
            HsGuarantor hsGuarantor = hsGuarantors.FirstOrDefault();
            if (hsGuarantor == null)
            {
                throw new Exception("cannot find HsGuarantor");
            }

            HsGuarantorDto hsGuarantorDto = Mapper.Map<HsGuarantorDto>(hsGuarantor);
            this.AddHsGuarantorDto(hsGuarantorDto);

            return this;
        }

        private BaselineDataBuilder PopulateMatchRegistry(int hsGuarantorId)
        {
            PopulateMatchRegistryMessage message = new PopulateMatchRegistryMessage
            {
                HsGuarantorId = hsGuarantorId,
                IsUpdate = true,
                ProcessDate = DateTime.UtcNow
            };

            this._matchingApplicationService.Value.ConsumeMessage(message,null);

            return this;
        }

        private BaselineDataBuilder FindNewMatches()
        {
            int vpGuarantorId = this.VpGuarantorDto.VpGuarantorId;
            bool matchingBIsEnabled = this._featureApplicationService.Value.IsFeatureEnabled(VisitPayFeatureEnum.FeatureMatchingApiIsEnabled);
            if (matchingBIsEnabled)
            {
                QueueFindNewHsGuarantorMatchesMessage message = new QueueFindNewHsGuarantorMatchesMessage
                {
                    VpGuarantorId = vpGuarantorId
                };

                this._matchingApplicationService.Value.ConsumeMessage(message, null);
            }
            else
            {
                this._guarantorApplicationService.Value.ResolveMatches(vpGuarantorId);
            }
            

            return this;
        }

        public BaselineDataBuilder CreateVisits(HsGuarantorDto hsGuarantorDto, params VisitChangeDto[] visitChangeDtos)
        {
            return this.CreateVisits(hsGuarantorDto, visitChangeDtos.ToList());
        }

        public BaselineDataBuilder CreateVisitsUsingHsDataServiceGroup(HsGuarantorDto hsGuarantorDto, params VisitChangeDto[] visitChangeDtos)
        {
            IReadOnlyList<Visit> hsVisits = this._hsVisitService.Value.GetVisitsForGuarantor(this.GetHsGuarantorDto().HsGuarantorId);
            int? serviceGroupId = hsVisits.Select(x => x.ServiceGroupId).FirstOrDefault(x => x.HasValue);
            foreach (VisitChangeDto visitChangeDto in visitChangeDtos)
            {
                visitChangeDto.ServiceGroupId = serviceGroupId;
            }

            return this.CreateVisits(hsGuarantorDto, visitChangeDtos.ToList());
        }

        public BaselineDataBuilder CreateVisits(HsGuarantorDto hsGuarantorDto, IList<VisitChangeDto> visitChangeDtos, bool queue = true)
        {
            this._session.Clear();

            // lookup in private collection to ensure we have the fully populated dto
            hsGuarantorDto = this.GetHsGuarantorDto(hsGuarantorDto.HsBillingSystemId, hsGuarantorDto.SourceSystemKey);

            // support intermountain realtime api
            if (this.IsIntermountain(hsGuarantorDto))
            {
                // all visits need to be represented in [qa].[IhcEnrollmentResponse] for registration to pull visits
                foreach (VisitChangeDto visitChangeDto in visitChangeDtos)
                {
                    visitChangeDto.SourceSystemKey = $"FAC_130_{visitChangeDto.SourceSystemKey}";
                    visitChangeDto.SourceSystemKeyDisplay = visitChangeDto.SourceSystemKey;
                }
                this._ihcEnrollmentService.Value.AddVisits(hsGuarantorDto.SourceSystemKey, visitChangeDtos.Select(x => x.SourceSystemKey));
            }

            this.CreateVisitChangeSet(hsGuarantorDto, visitChangeDtos, queue);
            
            foreach (VisitChangeDto visitChangeDto in visitChangeDtos)
            {
                Visit hsVisit = this._hsVisitService.Value.GetVisit(visitChangeDto.BillingSystemId, visitChangeDto.SourceSystemKey);
                visitChangeDto.VisitId = hsVisit.VisitId;

                this._appBaseDataService.Value.AddVisit(visitChangeDto.SourceSystemKey, visitChangeDto.BillingSystemId);
            }

            foreach (int hsGuarantorId in visitChangeDtos.Select(x => x.HsGuarantorId).Distinct())
            {
                this.PopulateMatchRegistry(hsGuarantorId);
            }

            return this;
        }

        /// <param name="hsVisitId"></param>
        /// <param name="payerFilterEnum">should match the insurance plan on the visit if being really accurate</param>
        /// <param name="coinsuranceAmount"></param>
        /// <returns></returns>
        public BaselineDataBuilder CreateEob(int hsVisitId, PayerFilterEnum payerFilterEnum, decimal coinsuranceAmount = 0m)
        {
            this._session.Clear();

            Visit hsVisit = this._hsVisitService.Value.GetVisit(hsVisitId);

            VisitTransaction insurancePayment = hsVisit.VisitTransactions.FirstOrDefault(x => x.VpTransactionType.VpTransactionTypeId == (int)VpTransactionTypeEnum.HsPayorCash);
            VisitTransaction insuranceWriteOff = hsVisit.VisitTransactions.FirstOrDefault(x => x.VpTransactionType.VpTransactionTypeId == (int)VpTransactionTypeEnum.HsPayorNonCash);
            if (insurancePayment == null && insuranceWriteOff == null)
            {
                return this;
            }

            VisitTransaction copayment = hsVisit.VisitTransactions.FirstOrDefault(x => x.TransactionDescription == CoPaymentTransactionDescription);

            EobClaimAdjustmentReasonCode inNetworkCode = this._remittanceCodeRepository.Value.GetEobClaimAdjustmentReasonCode("P22");
            EobClaimAdjustmentReasonCode deductibleCode = this._remittanceCodeRepository.Value.GetEobClaimAdjustmentReasonCode("1");
            EobClaimAdjustmentReasonCode coinsuranceCode = this._remittanceCodeRepository.Value.GetEobClaimAdjustmentReasonCode("2");
            EobClaimAdjustmentReasonCode copaymentCode = this._remittanceCodeRepository.Value.GetEobClaimAdjustmentReasonCode("3");
            EobClaimAdjustmentReasonCode notcoveredCode = this._remittanceCodeRepository.Value.GetEobClaimAdjustmentReasonCode("15");

            if (inNetworkCode == null || deductibleCode == null || coinsuranceCode == null || copaymentCode == null || notcoveredCode == null)
            {
                return this;
            }

            decimal totalCharges = hsVisit.VisitTransactions
                .Where(x => x.VpTransactionType.VpTransactionTypeId == (int) VpTransactionTypeEnum.HsCharge)
                .Select(x => x.TransactionAmount)
                .Sum();

            decimal inNetworkDiscount = (insuranceWriteOff?.TransactionAmount ?? 0m) * -1m;
            decimal paidToProvider = insurancePayment?.TransactionAmount ?? 0m;
            decimal coinsurance = coinsuranceAmount;
            decimal deductible = totalCharges - Math.Abs(paidToProvider) - Math.Abs(inNetworkDiscount) - Math.Abs(copayment?.TransactionAmount ?? 0) - coinsurance;
            decimal notcovered = 0m;

            string groupCode = "CO"; // i don't know if this matters (it doesn't for demo)

            ClaimPaymentInformation cpi = new ClaimPaymentInformationBuilder(ClaimStatusCodeEnum.ProcessedAsPrimary)
                .WithVisit(hsVisit)
                .WithBilledToInsurer(hsVisit.VisitTransactions.Where(x => x.VpTransactionType.VpTransactionTypeId == (int) VpTransactionTypeEnum.HsCharge).Sum(x => x.TransactionAmount))
                .WithPaymentToProvider(paidToProvider * -1m)
                .WithClaimAdjustment(inNetworkDiscount, groupCode, inNetworkCode)
                .WithClaimAdjustment(deductible, groupCode, deductibleCode)
                .WithClaimAdjustment(coinsurance, groupCode, coinsuranceCode)
                .WithClaimAdjustment(Math.Abs(copayment?.TransactionAmount ?? 0m), groupCode, copaymentCode)
                .WithClaimAdjustment(notcovered, groupCode, notcoveredCode)
                .WithPatientResponsibility(deductible + coinsurance + Math.Abs(copayment?.TransactionAmount ?? 0m) + notcovered)
                .ToClaimPaymentInformation();

            Domain.HospitalData.Eob.Entities.EobPayerFilter payerFilter = this._eobPayerFilterRepository.Value.GetById((int) payerFilterEnum);
            if (payerFilter == null)
            {
                return this;
            }

            InterchangeControlHeaderTrailer eob = new EobBuilder()
                .WithFunctionalGroupHeaderHeaderTrailer(payerFilter, insurancePayment?.TransactionDate ?? insuranceWriteOff?.TransactionDate ?? DateTime.UtcNow)
                .WithHeader()
                .WithClaimPaymentInformation(cpi)
                .ToEob();

            List<ClaimPaymentInformationHeader> cpiList = this._eob835Service.Value.ExtractClaimPaymentInformation(eob);
            foreach (ClaimPaymentInformationHeader claimPaymentInformationHeader in cpiList)
            {
                //Assign natural key to the header from the serialized ClaimPaymentInformation child
                claimPaymentInformationHeader.BillingSystemId = claimPaymentInformationHeader.ClaimPaymentInformation.BillingSystemId;
                claimPaymentInformationHeader.VisitSourceSystemKey = claimPaymentInformationHeader.ClaimPaymentInformation.VisitSourceSystemKey;
            }

            this._eob835Service.Value.SendClaimPaymentInformationNotifications(cpiList);
            this.Sleep();

            return this;
        }

        public BaselineDataBuilder Register(IPersonaCredential credential, string phoneNumber = null)
        {
            return this.Register(credential.Email, credential.Username, credential.Password, phoneNumber);
        }

        public BaselineDataBuilder Register(string email, string username, string password, string phoneNumber = null)
        {
            this._session.Clear();
            
            Task.Run(async () =>
            {
                HsGuarantorDto hsGuarantorDto = this._hsGuarantorDtos.First();
                VisitPayUserDto userDto = new VisitPayUserDto
                {
                    AddressStreet1 = string.IsNullOrWhiteSpace(hsGuarantorDto.AddressLine1) ? "123 Test St" : hsGuarantorDto.AddressLine1,
                    AddressStreet2 = hsGuarantorDto.AddressLine2,
                    City = string.IsNullOrWhiteSpace(hsGuarantorDto.City) ? "Boise" : hsGuarantorDto.City,
                    State = string.IsNullOrWhiteSpace(hsGuarantorDto.StateProvince) ? "ID" : hsGuarantorDto.StateProvince,
                    Zip = string.IsNullOrWhiteSpace(hsGuarantorDto.PostalCode) ? "83701" : hsGuarantorDto.PostalCode,
                    DateOfBirth = hsGuarantorDto.DOB,
                    Email = email,
                    FirstName = hsGuarantorDto.FirstName,
                    LastName = hsGuarantorDto.LastName,
                    MiddleName = null,
                    PhoneNumber = phoneNumber,
                    PhoneNumberType = string.IsNullOrWhiteSpace(phoneNumber) ? null : "Mobile",
                    PhoneNumberSecondary = null,
                    PhoneNumberSecondaryType = null,
                    SSN4 = int.Parse(hsGuarantorDto.SSN4),
                    UserName = username
                };

                int termsOfUseCmsVersionId = await this.GetCurrentCmsVersion(CmsRegionEnum.VppTermsAndConditions);
                int esignCmsVersionId = await this.GetCurrentCmsVersion(CmsRegionEnum.EsignTerms);

                string guarantorSourceSystemKey = hsGuarantorDto.SourceSystemKey;

                if (this.IsIntermountain(hsGuarantorDto))
                {
                    // prepend 107_ for intermountain
                    guarantorSourceSystemKey = "107_" + guarantorSourceSystemKey;
                }

                this._visitPayUserApplicationService.Value.CreatePatient(
                    user: userDto,
                    password: password,
                    registrationMatchString: guarantorSourceSystemKey,
                    termsOfUseId: termsOfUseCmsVersionId,
                    esignTermsId: esignCmsVersionId,
                    paymentDueDay: null,
                    securityQuestionAnswerDtos: new List<SecurityQuestionAnswerDto>(),
                    guarantorPasswordExpLimitInDays: 365 * 10,
                    patientDateOfBirth: userDto.DateOfBirth);

                this._visitPayUserDto = this._visitPayUserApplicationService.Value.FindByName(userDto.UserName);
                this.VpGuarantorDto = this._visitPayUserApplicationService.Value.GetGuarantorFromVisitPayUserId(this._visitPayUserDto.VisitPayUserId);
            }).Wait();

            this.Sleep();

            return this;
        }

        public BaselineDataBuilder Payment(string visitSourceSystemKey, decimal amount, int paymentMethodId, int daysOffset = 0, bool clearPayments = true)
        {
            this._session.Clear();
            VisitDto visitDto = this._visitApplicationService.Value.GetVisit(this._hsGuarantorDtos.Select(x => x.HsBillingSystemId).First(), visitSourceSystemKey);

            PaymentSubmissionDto paymentSubmissionDto = new PaymentSubmissionDto
            {
                PaymentDate = DateTime.UtcNow.AddDays(daysOffset),
                PaymentType = daysOffset == 0 ? PaymentTypeEnum.ManualPromptSpecificVisits : PaymentTypeEnum.ManualScheduledSpecificVisits,
                PaymentMethodId = paymentMethodId,
                VisitPayments = new List<VisitPaymentDto>
                {
                    new VisitPaymentDto
                    {
                        PaymentAmount = amount,
                        VisitId = visitDto.VisitId
                    }
                }
            };

            this._paymentSubmissionApplicationService.Value.ProcessPayment(
                paymentSubmissionDto,
                this._visitPayUserDto.VisitPayUserId,
                this._visitPayUserDto.VisitPayUserId,
                this.VpGuarantorDto.VpGuarantorId);

            this.Sleep();
            this._session.Clear();

            if (daysOffset == 0 && clearPayments)
            {
                this.ClearPayments();
            }

            return this;
        }

        /// <summary>
        /// some demo accounts are picky.  client configs for FPs differ.
        /// ensure a finance plan is always created if possible, even if the monthlyPaymentAmount given is not valid.
        /// try to ensure a finance plan is always created with interest if desired.
        /// </summary>
        /// <param name="monthlyPaymentAmount">...</param>
        /// <param name="requireInterest">if the monthly payment amount given doesn't include interest, find a number that does.</param>
        /// <returns></returns>
        public BaselineDataBuilder FinancePlan(decimal monthlyPaymentAmount, bool requireInterest)
        {
            this._session.Clear();

            Task.Run(async () =>
            {
                StatementDto statementDto = this._statementApplicationService.Value.GetLastStatement(this.VpGuarantorDto.VpGuarantorId);
                if (statementDto == null)
                {
                    throw new Exception("statement is null");
                }

                // try the amount passed in?
                CalculateFinancePlanTermsResponseDto response = monthlyPaymentAmount == 0m ? null : await this.GetFinancePlanTermsAsync(statementDto.VpStatementId, monthlyPaymentAmount, requireInterest);
                if (response == null)
                {
                    // try for minimum amount, but with interest if specified
                    response = await this.GetFinancePlanTermsAsync(statementDto.VpStatementId, null, requireInterest);
                    if (response == null && requireInterest)
                    {
                        // try again, for minimum amount, but without interest
                        response = await this.GetFinancePlanTermsAsync(statementDto.VpStatementId, null, false);
                    }
                }

                if (response == null || response.IsError)
                {
                    throw new Exception("couldn't come to terms.");
                }

                FinancePlanCalculationParametersDto submitParameters = FinancePlanCalculationParametersDto.CreateForSubmit(
                    this.VpGuarantorDto.VpGuarantorId,
                    statementDto.VpStatementId,
                    response.Terms.MonthlyPaymentAmount,
                    null,
                    OfferCalculationStrategyEnum.MonthlyPayment,
                    false);

                CmsRegionEnum cmsRegionEnum = this._visitApplicationService.Value.GetRicCmsRegionEnum(this.VpGuarantorDto.VpGuarantorId);

                int creditAgreementCmsVersionId = await this.GetCurrentCmsVersion(cmsRegionEnum);

                CreateFinancePlanResponse fp = await this._financePlanApplicationService.Value.CreateFinancePlanAsync(
                    this._visitPayUserDto.VisitPayUserId,
                    this._visitPayUserDto.VisitPayUserId,
                    submitParameters,
                    creditAgreementCmsVersionId,
                    response.Terms.FinancePlanOfferId,
                    this._visitPayUserDto.VisitPayUserId,
                    null);

                if (response.IsError)
                {
                    throw new Exception("couldn't create finance plan.");
                }

            }).Wait();

            this.Sleep();

            return this;
        }

        private async Task<CalculateFinancePlanTermsResponseDto> GetFinancePlanTermsAsync(int statementId, decimal? monthlyPaymentAmount, bool requireInterest)
        {
            if (!monthlyPaymentAmount.HasValue)
            {
                FinancePlanCalculationParametersDto checkParameters = FinancePlanCalculationParametersDto.CreateForTermsCheck(this.VpGuarantorDto.VpGuarantorId, null, false, null);

                if (requireInterest)
                {
                    // figure out where the interest tiers are
                    IList<InterestRateDto> interestRates = this._financePlanApplicationService.Value.GetInterestRates(checkParameters);
                    InterestRateDto interestRate = interestRates.Where(x => x.Rate != 0m).OrderBy(x => x.Rate).FirstOrDefault();
                    if (interestRate != null)
                    {
                        int checkMonths = interestRate.DurationRangeEnd - ((interestRate.DurationRangeEnd - interestRate.DurationRangeStart) / 2);
                        if (checkMonths < interestRate.DurationRangeStart)
                        {
                            checkMonths = interestRate.DurationRangeEnd;
                        }

                        FinancePlanCalculationParametersDto monthParameters = FinancePlanCalculationParametersDto.CreateForNumberOfPayments(this.VpGuarantorDto.VpGuarantorId, statementId, checkMonths, null, false, null);
                        CalculateFinancePlanTermsResponseDto monthResponse = await this._financePlanApplicationService.Value.GetTermsByMonthlyPaymentAmountAsync(monthParameters);
                        if (!monthResponse.IsError)
                        {
                            monthlyPaymentAmount = monthResponse.Terms.MonthlyPaymentAmount;
                        }
                    }
                }

                if (monthlyPaymentAmount.GetValueOrDefault(0m) <= 0m)
                {
                    FinancePlanBoundaryDto boundaryDto = this._financePlanApplicationService.Value.GetMinimumPaymentAmount(checkParameters);
                    monthlyPaymentAmount = boundaryDto.MinimumPaymentAmount;
                }

                if (monthlyPaymentAmount.GetValueOrDefault(0m) > 0m)
                {
                    // round it up to nearest 10
                    monthlyPaymentAmount = Math.Ceiling(monthlyPaymentAmount.Value / 10m) * 10;
                }
            }

            if (!monthlyPaymentAmount.HasValue)
            {
                return null;
            }

            FinancePlanCalculationParametersDto termsParameters = FinancePlanCalculationParametersDto.CreateForMonthlyPayment(this.VpGuarantorDto.VpGuarantorId, statementId, monthlyPaymentAmount.Value, null, false);
            CalculateFinancePlanTermsResponseDto response = await this._financePlanApplicationService.Value.GetTermsByMonthlyPaymentAmountAsync(termsParameters);
            if (response.IsError)
            {
                return null;
            }

            if (requireInterest && response.Terms.InterestRate == 0m)
            {
                return null;
            }

            return response;
        }

        public BaselineDataBuilder PaymentDueDate(bool clearPayments = true)
        {
            this._session.Clear();
            this.SetDateToYesterday(true);

            this._session.Clear();
            StatementDto statementDto = this._statementApplicationService.Value.GetLastStatement(this.VpGuarantorDto.VpGuarantorId);
            this._vpStatementService.Value.MarkStatementAsQueued(statementDto.VpStatementId, this.VpGuarantorDto.VpGuarantorId, "BaselineDataBuilder::PaymentDueDate");

            this._session.Clear();
            this._scheduledPaymentApplicationService.Value.ProcessScheduledPayment(this.VpGuarantorDto.VpGuarantorId, DateTime.UtcNow.Date);

            this.Sleep();

            if (clearPayments)
            {
                this.ClearPayments();
            }

            return this;
        }

        public BaselineDataBuilder StatementDate()
        {
            this._session.Clear();
            int statementCount = this._statementApplicationService.Value.GetStatementTotals(this.VpGuarantorDto.User.VisitPayUserId);

            for (int i = 0; i < 5; i++)
            {
                this.SetDateToYesterday(false);
                this.Rollback(i);

                this._session.Clear();
                Guarantor vpGuarantor = this._guarantorService.Value.GetGuarantor(this.VpGuarantorDto.VpGuarantorId);

                Task.Run(async () =>
                { 
                    await this._statementCreationApplicationService.Value.CreateStatementForGuarantorAsync(this.VpGuarantorDto.VpGuarantorId, vpGuarantor.NextStatementDate.GetValueOrDefault(DateTime.UtcNow.Date), false); 
                }).Wait();

                this.Sleep();
                this._session.Clear();

                int statementCount1 = this._statementApplicationService.Value.GetStatementTotals(this.VpGuarantorDto.User.VisitPayUserId);
                if (statementCount == statementCount1)
                {
                    continue;
                }

                break;
            }
            
            if (!this._featureApplicationService.Value.IsFeatureEnabled(VisitPayFeatureEnum.FeatureVisitPayAgingIsEnabled))
            {
                IList<VisitDto> visits = this._visitApplicationService.Value.GetAllActiveNonFinancedVisits(this.VpGuarantorDto.VpGuarantorId);
                foreach (VisitDto visitDto in visits)
                {
                    Visit visit = this._hsVisitService.Value.GetVisit(visitDto.BillingSystemId, visitDto.SourceSystemKey);
                    visit.AgingCount = visitDto.AgingCount + 1;
                    this._visitRepository.Value.InsertOrUpdate(visit);

                    this._changeEventApplicationService.Value.SaveChangeEvent(this.CreateChangeEvent(visit.VisitId, visit.HsGuarantorId));
                    this._changeEventApplicationService.Value.QueueAllUnprocessedChangeEvents();
                    this.Sleep();
                }
            }

            return this;
        }

        public BaselineDataBuilder ExportPaperStatements()
        {
            this._session.Clear();
            this._statementExportApplicationService.Value.ExportPaperStatements(DateTime.UtcNow);
            this.Sleep();

            return this;
        }

        public BaselineDataBuilder AddHold(int visitId)
        {
            Visit visit = this._hsVisitService.Value.GetVisit(visitId);
            visit.BillingHold = true;
            this._visitRepository.Value.InsertOrUpdate(visit);

            this._changeEventApplicationService.Value.SaveChangeEvent(this.CreateChangeEvent(visit.VisitId, visit.HsGuarantorId));
            this._changeEventApplicationService.Value.QueueAllUnprocessedChangeEvents();
            this.Sleep();
            
            return this;
        }

        public BaselineDataBuilder ClearPayments()
        {
            this._session.Clear();

            IReadOnlyList<VisitDto> visits = this._visitApplicationService.Value.GetVisits(this.VpGuarantorDto.User.VisitPayUserId, new VisitFilterDto(), 0, int.MaxValue).Visits;
            foreach (VisitDto visitDto in visits)
            {
                Visit hsVisit = this._hsVisitService.Value.GetVisit(visitDto.BillingSystemId, visitDto.SourceSystemKey);
                if (hsVisit == null)
                {
                    continue;
                }

                IList<PaymentAllocation> unclearedPaymentAllocations = this._qaPaymentService.Value.GetUnclearedPaymentAllocationsForVisits(visitDto.VisitId.ToListOfOne()).Where(x => x.PaymentAllocationType != PaymentAllocationTypeEnum.VisitInterest).ToList();
                if (!unclearedPaymentAllocations.Any())
                {
                    continue;
                }

                // get a visit change from the existing visit data
                VisitChangeDto visitChangeDto = this.GetNewVisitChange(hsVisit.VisitId);

                // throw it in the builder
                VisitChangeDtoBuilder visitChangeDtoBuilder = new VisitChangeDtoBuilder(visitChangeDto);

                // clear allocations
                visitChangeDtoBuilder.ClearPaymentAllocations(unclearedPaymentAllocations);

                // finalize and send it
                VisitChangeDto newVisitChangeDto = visitChangeDtoBuilder.ToVisitChangeDto();
                this.CreateVisitChangeSet(this.GetHsGuarantorDto(hsVisit.HsGuarantorId), newVisitChangeDto.ToListOfOne());

                this.Sleep();
            }

            return this;
        }

        public BaselineDataBuilder AddPaymentMethod(PaymentMethodDtoBuilder paymentMethodDtoBuilder)
        {
            this._session.Clear();

            Tuple<PaymentMethodDto, string, string> tuple = paymentMethodDtoBuilder.ForSubmit(this.VpGuarantorDto);
            PaymentMethodDto paymentMethodDto = tuple.Item1;
            string accountNumber = tuple.Item2;
            string securityCodeOrRoutingNumber = tuple.Item3;

            Task.Run(async () =>
            {
                int paymentMethodId = await this._testPaymentMethodsApplicationService.Value.SavePaymentMethodAsync(paymentMethodDto, this._visitPayUserDto.VisitPayUserId, paymentMethodDto.IsPrimary, accountNumber, securityCodeOrRoutingNumber);
                paymentMethodDtoBuilder.SetPaymentMethodId(paymentMethodId);
            }).Wait();

            this.Sleep();

            return this;
        }

        public BaselineDataBuilder ChangePrimary(int paymentMethodId)
        {
            this._session.Clear();

            PaymentMethodDto paymentMethodDto = this._paymentMethodsApplicationService.Value.GetPaymentMethodById(paymentMethodId);
            paymentMethodDto.IsPrimary = true;

            this._paymentMethodsApplicationService.Value.SetPrimaryPaymentMethod(Mapper.Map<JournalEventHttpContextDto>(HttpContext.Current), paymentMethodDto.PaymentMethodId.GetValueOrDefault(0), this.VpGuarantorDto.VpGuarantorId, this._visitPayUserDto.VisitPayUserId);

            this.Sleep();

            return this;
        }

        public BaselineDataBuilder Managing(GuarantorDto guarantorToManage, bool withFinancePlans)
        {
            this._session.Clear();

            Task.Run(async () =>
            {
                await this._managingUserApplicationService.Value.MatchGuarantorAsync(new ConsolidationMatchRequestDto
                {
                    BirthYear = guarantorToManage.User.DateOfBirth.GetValueOrDefault(DateTime.UtcNow).Year,
                    ConsolidationMatchRequestType = ConsolidationMatchRequestTypeEnum.RequestToBeManaging,
                    EmailAddress = guarantorToManage.User.Email,
                    FirstName = guarantorToManage.User.FirstName,
                    LastName = guarantorToManage.User.LastName
                }, this._visitPayUserDto.VisitPayUserId);
            }).Wait();

            this._session.Clear();

            this.Sleep();

            Guarantor guarantor = this._guarantorService.Value.GetGuarantor(this.VpGuarantorDto.VpGuarantorId);

            Task.Run(async () =>
            {
                await this._managedUserApplicationService.Value.AcceptTermsAsync(guarantor.ManagedConsolidationGuarantors.First().ConsolidationGuarantorId, guarantorToManage.User.VisitPayUserId);
            }).Wait();

            this.Sleep();

            this._session.Clear();

            if (withFinancePlans)
            {
                Task.Run(async () =>
                {
                    await this._managingUserApplicationService.Value.AcceptFinancePlanTermsAsync(guarantor.ManagedConsolidationGuarantors.First().ConsolidationGuarantorId, guarantorToManage.User.VisitPayUserId);
                }).Wait();
            }

            this.Sleep();

            return this;
        }

        public BaselineDataBuilder SupportRequest(string text, int? supportTopicId)
        {
            this._session.Clear();

            SupportRequestCreateDto supportRequestCreateDto = new SupportRequestCreateDto
            {
                CreatedByVisitPayUserId = this._visitPayUserDto.VisitPayUserId,
                MessageBody = text,
                SubmittedForVisitPayUserId = this._visitPayUserDto.VisitPayUserId,
                SupportTopicId = supportTopicId,
                VisitPayUserId = this._visitPayUserDto.VisitPayUserId,
            };

            SupportRequestDto supportRequestDto = this._supportRequestApplicationService.Value.CreateSupportRequest(supportRequestCreateDto);
            this._supportRequestId = supportRequestDto.SupportRequestId;

            return this;
        }

        public BaselineDataBuilder ReplyToSupportRequest(string text, bool fromClient)
        {
            this._session.Clear();

            if (this._supportRequestId.HasValue)
            {
                if (fromClient)
                {
                    // userid 2 = defaultclient
                    this._supportRequestApplicationService.Value.SaveClientMessage(this._supportRequestId.Value, this.VpGuarantorDto.User.VisitPayUserId, text, 2);
                }
                else
                {
                    this._supportRequestApplicationService.Value.SaveGuarantorMessage(this._supportRequestId.Value, this.VpGuarantorDto.User.VisitPayUserId, text, this.VpGuarantorDto.User.VisitPayUserId);
                }
            }

            return this;
        }

        public BaselineDataBuilder KeepCurrent(int keepCurrentOffset)
        {
            if (this.VpGuarantorDto != null)
            {
                this._vpGuarantorKeepCurrentRepository.Value.Insert(new VpGuarantorKeepCurrent
                {
                    KeepCurrentAnchor = "LastStatementDate", // just seems to be what most existing entries are set to
                    KeepCurrentOffset = keepCurrentOffset,
                    VpGuarantorId = this.VpGuarantorDto.VpGuarantorId
                });
            }

            foreach (HsGuarantorDto hsGuarantorDto in this._hsGuarantorDtos)
            {
                this._hsGuarantorKeepCurrentRepository.Value.Insert(new HsGuarantorKeepCurrent
                {
                    KeepCurrentAnchor = "TransactionDate", // just seems to be what most existing entries are set to
                    KeepCurrentOffset = keepCurrentOffset,
                    HsGuarantorId = hsGuarantorDto.HsGuarantorId
                });
            }

            return this;
        }

        public BaselineDataBuilder WithDemoFeature(VisitPayFeatureEnum feature)
        {
            Dictionary<VisitPayFeatureEnum, RandomizedTestEnum> featureTests = new Dictionary<VisitPayFeatureEnum, RandomizedTestEnum>
            {
                {VisitPayFeatureEnum.DemoInsuranceAccrualUiIsEnabled, RandomizedTestEnum.FeatureDemoInsuranceAccrualUi},
                {VisitPayFeatureEnum.DemoMyChartSSOUiIsEnabled, RandomizedTestEnum.FeatureDemoMyChartSsoUi},
                {VisitPayFeatureEnum.DemoPreServiceUiIsEnabled, RandomizedTestEnum.FeatureDemoPreServiceUi},
                {VisitPayFeatureEnum.DemoHealthEquityIsEnabled, RandomizedTestEnum.FeatureDemoHealthEquity}
            };

            if (featureTests.ContainsKey(feature))
            {
                RandomizedTestEnum featureTest = featureTests[feature];
                this._randomizedTestApplicationService.Value.GetRandomizedTestGroupMembership(featureTest, this.VpGuarantorDto.VpGuarantorId, true);
            }

            return this;
        }

        public BaselineDataBuilder InRandomizedTest(RandomizedTestEnum randomizedTest, RandomizedTestGroupEnum? randomizedTestGroup = null)
        {
            this._randomizedTestApplicationService.Value.GetRandomizedTestGroupMembership(randomizedTest, this.VpGuarantorDto.VpGuarantorId, true, randomizedTestGroup);

            return this;
        }

        public BaselineDataBuilder ActivateSms()
        {
            CmsVersionDto cmsVersionDto = this._contentApplicationService.Value.GetCurrentVersionAsync(CmsRegionEnum.SmsAcknowledgement, false).Result;

            this._visitPayUserApplicationService.Value.SetSmsAcknowledgement(
                visitPayUserId: this.VpGuarantorDto.User.VisitPayUserId.ToString(),
                smsPhoneType: SmsPhoneTypeEnum.Primary,
                cmsVersionId: cmsVersionDto.CmsVersionId,
                context: Mapper.Map<JournalEventHttpContextDto>(HttpContext.Current)
            );

            return this;
        }

        public BaselineDataBuilder MakeVisitEligible(int visitId, bool queue = true)
        {
            this.Sleep(3);
            Visit hsVisit = this._hsVisitService.Value.GetVisit(visitId);
            if (hsVisit != null)
            {

                hsVisit.VpEligible = true;

                this._visitRepository.Value.InsertOrUpdate(hsVisit);
                this._changeEventApplicationService.Value.SaveChangeEvent(this.CreateChangeEvent(hsVisit.VisitId, hsVisit.HsGuarantorId));

                if (queue)
                {
                    this._changeEventApplicationService.Value.QueueAllUnprocessedChangeEvents();
                }
            }

            return this;

        }

        public BaselineDataBuilder OfflineGuarantor(bool paperStatements)
        {
            this._session.Clear();

            Guarantor guarantor = this._guarantorService.Value.GetGuarantor(this.VpGuarantorDto.VpGuarantorId);
            this._guarantorService.Value.UpdateGuarantorType(guarantor, GuarantorTypeEnum.Offline);
           
            if (paperStatements)
            {
                VisitPayUserDto visitPayUserDto = this._visitPayUserApplicationService.Value.FindById(guarantor.User.VisitPayUserId.ToString());
                visitPayUserDto.MailingAddressStreet1 = string.IsNullOrWhiteSpace(visitPayUserDto.MailingAddressStreet1) ? visitPayUserDto.AddressStreet1 : visitPayUserDto.MailingAddressStreet1;
                visitPayUserDto.MailingAddressStreet2 = string.IsNullOrWhiteSpace(visitPayUserDto.MailingAddressStreet2) ? visitPayUserDto.AddressStreet2 : visitPayUserDto.MailingAddressStreet2;
                visitPayUserDto.MailingCity = string.IsNullOrWhiteSpace(visitPayUserDto.MailingCity) ? visitPayUserDto.City : visitPayUserDto.MailingCity;
                visitPayUserDto.MailingState = string.IsNullOrWhiteSpace(visitPayUserDto.MailingState) ? visitPayUserDto.State : visitPayUserDto.MailingState;
                visitPayUserDto.MailingZip = string.IsNullOrWhiteSpace(visitPayUserDto.MailingZip) ? visitPayUserDto.Zip : visitPayUserDto.MailingZip;

                this._visitPayUserApplicationService.Value.UpdateUser(visitPayUserDto);
                this._visitPayUserApplicationService.Value.AddMailCommunicationPreferences(Mapper.Map<JournalEventHttpContextDto>(HttpContext.Current), 2, this._visitPayUserDto.VisitPayUserId);
            }

            return this;
        }

        public BaselineDataBuilder AddPtpScore(int score)
        {
            this._appBaseDataService.Value.AddGuarantorScoreToVpApp(this.VpGuarantorDto.VpGuarantorId, score, ScoreTypeEnum.PtpOriginal);
            this._appBaseDataService.Value.AddGuarantorScoreToVpApp(this.VpGuarantorDto.VpGuarantorId, score, ScoreTypeEnum.ProtoScore);

            return this;
        }
        
        public BaselineDataBuilder UseDemoFacilities(int hbFacilityId, int pbFacilityId)
        {
            this._session.Clear();
            
            using (ITransaction transaction = this._session.BeginTransaction(Ivh.Common.Data.Constants.IsolationLevel.Default))
            {
                IList<Domain.Visit.Entities.Visit> visits = this._session.QueryOver<Domain.Visit.Entities.Visit>().Where(x => x.VPGuarantor.VpGuarantorId == this.VpGuarantorDto.VpGuarantorId).List();
                IList<Domain.Visit.Entities.Facility> facilities = this._session.QueryOver<Domain.Visit.Entities.Facility>().List();
                Domain.Visit.Entities.Facility demoHospitalFacility = facilities.FirstOrDefault(x => x.FacilityId == hbFacilityId);
                Domain.Visit.Entities.Facility demoPhysicianFacility = facilities.FirstOrDefault(x => x.FacilityId == pbFacilityId);
                
                foreach (Domain.Visit.Entities.Visit visit in visits)
                {
                    if (visit.BillingApplication == BillingApplicationConstants.HB)
                    {
                        if (demoHospitalFacility != null)
                        {
                            // use demo hb
                            visit.Facility = demoHospitalFacility;
                            this._session.SaveOrUpdate(visit);
                        }
                    }
                    else
                    {
                        if (demoPhysicianFacility != null)
                        {
                            // use demo pb
                            visit.Facility = demoPhysicianFacility;
                            this._session.SaveOrUpdate(visit);
                        }
                    }
                }

                transaction.Commit();
            }

            this._session.Clear();
            
            return this;
        }

        private async Task<int> GetCurrentCmsVersion(CmsRegionEnum cmsRegion)
        {
            CmsVersionDto cmsVersion = await this._contentApplicationService.Value.GetCurrentVersionAsync(cmsRegion, false);

            return cmsVersion.CmsVersionId;
        }

        private VisitChangeDto GetNewVisitChange(int visitId)
        {
            this._session.Clear();

            ChangeSetDto changeSet = this._changeSetApplicationService.Value.CreateChangeSetFromVisit(visitId);
            VisitChangeDto visitChangeDto = changeSet.VisitChanges[0];

            return visitChangeDto;
        }

        #region rollback stuff

        public BaselineDataBuilder Rollback(int days)
        {
            this._session.Clear();

            this.AdjustAllGuarantorDates(Math.Abs(days) * -1);

            return this;
        }

        private void SetDateToYesterday(bool paymentDueDate)
        {
            StatementDto lastStatement = this._statementApplicationService.Value.GetLastStatement(this.VpGuarantorDto.VpGuarantorId);
            if (lastStatement == null)
            {
                return;
            }
            
            if (paymentDueDate)
            {
                TimeSpan diff = DateTime.UtcNow.Date - lastStatement.PaymentDueDate.Date;
                diff = diff.Subtract(new TimeSpan(1, 0, 0, 0));
                
                this.AdjustAllGuarantorDates(diff.Days);
            }
            else
            {
                TimeSpan diff = DateTime.UtcNow.Date - lastStatement.PaymentDueDate.AddMonths(1).AddDays(-21);
                diff = diff.Subtract(new TimeSpan(1, 0, 0, 0));
                
                this.AdjustAllGuarantorDates(diff.Days);
                
                this._session.Clear();
                
                // this fixes some last day of the month issues the rollback scripts have.
                // trying to adjust the statement date independent from payment due date, so the next dates are calculated correctly.
                Guarantor guarantor = this._guarantorService.Value.GetGuarantor(this.VpGuarantorDto.VpGuarantorId);
                VpStatement adjustedStatement = this._vpStatementService.Value.GetMostRecentStatement(guarantor);
                DateTime adjustedPaymentDueDate = adjustedStatement.PaymentDueDate;
                DateTime calculatedStatementDate = adjustedPaymentDueDate.AddMonths(1).AddDays(-21);
                if (calculatedStatementDate.Date != guarantor.NextStatementDate.GetValueOrDefault(DateTime.UtcNow).Date)
                {
                    guarantor.NextStatementDate = calculatedStatementDate.Date;
                    this._guarantorService.Value.ChangeStatementDate(guarantor, calculatedStatementDate.Date);
                }

                this._session.Clear();
            }
        }

        private void AdjustAllGuarantorDates(int days)
        {
            this._visitPayUserIssueResultApplicationService.Value.AdjustAllGuarantorDates(this.VpGuarantorDto.VpGuarantorId, "Day", days);

            // consolidation rollbacks rollback the ConsolidationGuarantor table twice, so if managed, roll it forward again
            this._session.Clear();
            Guarantor guarantor = this._guarantorService.Value.GetGuarantor(this.VpGuarantorDto.VpGuarantorId);
            if (guarantor.ConsolidationStatus == GuarantorConsolidationStatusEnum.Managed)
            {
                foreach (ConsolidationGuarantor consolidationGuarantor in guarantor.ManagingConsolidationGuarantors)
                {
                    consolidationGuarantor.DateAcceptedByManagedGuarantor = consolidationGuarantor.DateAcceptedByManagedGuarantor?.AddDays(((int)decimal.Negate(days)) * -1);
                    consolidationGuarantor.DateAcceptedByManagingGuarantor = consolidationGuarantor.DateAcceptedByManagingGuarantor?.AddDays(((int)decimal.Negate(days)) * -1);
                    consolidationGuarantor.DateAcceptedFinancePlanTermsByManagingGuarantor = consolidationGuarantor.DateAcceptedFinancePlanTermsByManagingGuarantor?.AddDays(((int)decimal.Negate(days)) * -1);
                    this._guarantorService.Value.InsertOrUpdateGuarantor(guarantor);
                }
                this._session.Clear();
            }

            if (this._featureApplicationService.Value.IsFeatureEnabled(VisitPayFeatureEnum.FeatureVisitPayAgingIsEnabled))
            {
                // aging
                this._qaAgingApplicationService.Value.AddVisitAgesWithRollback(this.VpGuarantorDto.VpGuarantorId, days);
                this.Sleep(3);
            }
        }

        #endregion

        private void CreateVisitChangeSet(HsGuarantorDto hsGuarantorDto, IList<VisitChangeDto> visitChangeDtos, bool queue = true)
        {
            IList<Facility> facilities = this._hsVisitService.Value.GetFacilities();

            foreach (VisitChangeDto visitChangeDto in visitChangeDtos)
            {
                Ivh.Common.VisitPay.Messages.HospitalData.Dtos.FacilityDto facilityDto = null;
                if (visitChangeDto.Facility?.FacilityId.HasValue ?? false)
                {
                    Facility facility = facilities.FirstOrDefault(x => x.FacilityId == visitChangeDto.Facility.FacilityId);
                    if (facility != null)
                    {
                        facilityDto = Mapper.Map<Ivh.Common.VisitPay.Messages.HospitalData.Dtos.FacilityDto>(facility);
                    }
                }

                if (facilityDto == null)
                {
                    facilityDto = Mapper.Map<Ivh.Common.VisitPay.Messages.HospitalData.Dtos.FacilityDto>(facilities.OrderBy(x => x.FacilityId).FirstOrDefault());
                }

                visitChangeDto.BillingSystemId = hsGuarantorDto.HsBillingSystemId;
                visitChangeDto.HsGuarantorId = hsGuarantorDto.HsGuarantorId;
                visitChangeDto.Facility = facilityDto;

                foreach (VisitTransactionChangeDto visitTransactionChangeDto in visitChangeDto.VisitTransactions ?? new List<VisitTransactionChangeDto>())
                {
                    visitTransactionChangeDto.BillingSystemId = visitChangeDto.BillingSystemId;
                }

                ChangeEventDto changeEvent = this.CreateChangeEvent(visitChangeDto.VisitId, visitChangeDto.HsGuarantorId);
                this._changeEventApplicationService.Value.SaveChangeEvent(changeEvent, visitChangeDto);
            }

            if (queue)
            {
                this._changeEventApplicationService.Value.QueueAllUnprocessedChangeEvents();
            }

            this.Sleep(3);
        }

        public void UpdateVisits(int hsGuarantorId, bool queue = true)
        {
            IReadOnlyList<Visit> hsVisits = this._hsVisitService.Value.GetVisitsForGuarantor(hsGuarantorId);
            if (!hsVisits.Any())
            {
                return;
            }

            foreach (Visit hsVisit in hsVisits)
            {
                VisitChangeDto visitChangeDto = this.GetNewVisitChange(hsVisit.VisitId);
                VisitChangeDtoBuilder visitChangeDtoBuilder = new VisitChangeDtoBuilder(visitChangeDto);

                VisitTransaction transaction = visitChangeDtoBuilder.AddTransaction("new txn", 100m, VpTransactionTypeEnum.HsCharge, DateTime.UtcNow);
                hsVisit.VisitTransactions.Add(transaction);
                hsVisit.HsCurrentBalance += transaction.TransactionAmount;

                foreach (VisitTransaction vt in hsVisit.VisitTransactions)
                {
                    vt.Visit = hsVisit;
                    vt.BillingSystemId = hsVisit.BillingSystemId;
                }

                this._visitRepository.Value.InsertOrUpdate(hsVisit);
                this._changeEventApplicationService.Value.SaveChangeEvent(this.CreateChangeEvent(hsVisit.VisitId, hsVisit.HsGuarantorId));

                if (queue)
                {
                    this._changeEventApplicationService.Value.QueueAllUnprocessedChangeEvents();
                }
            }
        }

        private ChangeEventDto CreateChangeEvent(int visitId, int hsGuarantorId)
        {
            return new ChangeEventDto
            {
                ChangeEventDateTime = DateTime.UtcNow,
                ChangeEventStatus = ChangeEventStatusEnum.Unprocessed,
                ChangeEventType = new ChangeEventTypeDto { ChangeEventTypeId = (int)ChangeEventTypeEnum.DataChange },
                EventTriggerDescription = "Persona Creation Tool",
                VisitId = visitId,
                HsGuarantorId = hsGuarantorId
            };
        }

        private void Sleep(int? seconds = null)
        {
            Thread.Sleep((seconds ?? SleepSeconds) * 1000);
        }

        private bool IsIntermountain(HsGuarantorDto hsGuarantorDto)
        {
            // pretty likely there's a better way to do this.
            if (hsGuarantorDto.HsBillingSystemId == 2 || // Intermountain iSeries,
                hsGuarantorDto.HsBillingSystemId == 3)   // Intermountain Cerner
            {
                return true;
            }

            return false;
        }

        public PersonaResultDto ToResult(int keepCurrentOffset)
        {
            this.KeepCurrent(keepCurrentOffset);

            return new PersonaResultDto(this._hsGuarantorDtos, this.VpGuarantorDto, keepCurrentOffset);
        }

        public BaselineDataBuilder UnmatchHsGuarantors(params string[] hsGuarantorSourceSystemKeys)
        {
            HsGuarantorUnmatchRequestDto request = new HsGuarantorUnmatchRequestDto
            {
                HsGuarantorSourceSystemKeys = hsGuarantorSourceSystemKeys,
                UnmatchActionType = UnmatchActionTypeEnum.GuarantorUnmatch,
                VpGuarantorId = this.VpGuarantorDto.VpGuarantorId,
                ActionVisitPayUser = new VisitPayUserDto { VisitPayUserId = SystemUsers.SystemUserId }
            };

            this._redactionApplicationService.Value.ProcessGuarantorUnmatchRequest(request, UnmatchSourceEnum.Unknown);

            return this;
        }

        public HsGuarantorDto GetHsGuarantorDto(int? hsGuarantorId = null)
        {
            return this._hsGuarantorDtos.FirstOrDefault(x => x.HsGuarantorId == hsGuarantorId) ?? this._hsGuarantorDtos.FirstOrDefault();
        }

        public HsGuarantorDto GetHsGuarantorDto(int billingSystemId, string guarantorSourceSystemKey)
        {
            return this._hsGuarantorDtos.FirstOrDefault(x => x.HsBillingSystemId == billingSystemId && x.SourceSystemKey == guarantorSourceSystemKey) ?? this._hsGuarantorDtos.FirstOrDefault();
        }

        public void AddHsGuarantorDto(HsGuarantorDto hsGuarantorDto)
        {
            this._hsGuarantorDtos.Add(hsGuarantorDto);
        }
    }
}
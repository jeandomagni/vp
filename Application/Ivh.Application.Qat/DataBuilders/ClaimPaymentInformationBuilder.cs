﻿namespace Ivh.Application.Qat.DataBuilders
{
    using System;
    using System.Collections.Generic;
    using Domain.HospitalData.Eob.Entities;
    using Domain.HospitalData.Visit.Entities;
    using Ivh.Common.Base.Utilities.Helpers;

    internal class ClaimPaymentInformationBuilder
    {
        private readonly ClaimPaymentInformation _claimPaymentInformation;

        public ClaimPaymentInformationBuilder(ClaimStatusCodeEnum claimStatusCode)
        {
            string claimNumber = RandomStringGeneratorUtility.GetRandomNumberString(8);

            this._claimPaymentInformation = new ClaimPaymentInformation
            {
                Clp01ClaimSubmittersIdentifier = claimNumber,
                Clp02ClaimStatusCode = ((int)claimStatusCode).ToString(),
                Clp07ReferenceIdentifier = claimNumber,
            };
        }

        public ClaimPaymentInformationBuilder WithVisit(Visit hsVisit)
        {
            this._claimPaymentInformation.VisitSourceSystemKey = hsVisit.SourceSystemKey;
            this._claimPaymentInformation.BillingSystemId = hsVisit.BillingSystemId;
            this._claimPaymentInformation.Nm11PatientName = new IndividualOrOrganizationalName
            {
                Nm103NameLastOrganizationName = hsVisit.PatientLastName,
                Nm104NameFirst = hsVisit.PatientFirstName,
                Nm109IdentificationCode = "895663291"
            };

            return this;
        }

        /// <summary>
        /// </summary>
        /// <param name="remittanceAdviceRemarkCode">SELECT [RemittanceAdviceRemarkCode] FROM [DEV_CDI_ETL].[eob].[EobRemittanceAdviceRemarkCodes]</param>
        /// <returns></returns>
        public ClaimPaymentInformationBuilder WithInpatient(string remittanceAdviceRemarkCode)
        {
            this._claimPaymentInformation.MiaInpatientInformation = new InpatientAdjudicationInformation 
            {
                Mia05ReferenceIdentifier = remittanceAdviceRemarkCode
            };

            return this;
        }
        
        /// <summary>
        /// </summary>
        /// <param name="remittanceAdviceRemarkCode">SELECT [RemittanceAdviceRemarkCode] FROM [DEV_CDI_ETL].[eob].[EobRemittanceAdviceRemarkCodes]</param>
        /// <returns></returns>
        public ClaimPaymentInformationBuilder WithOutpatient(string remittanceAdviceRemarkCode)
        {
            this._claimPaymentInformation.MoaOutpatientInformation = new OutpatientAdjudicationInformation 
            { 
                Moa03ReferenceIdentifier = remittanceAdviceRemarkCode
            };

            return this;
        }

        public ClaimPaymentInformationBuilder WithBilledToInsurer(decimal amount)
        {
            this._claimPaymentInformation.Clp03MonetaryAmount = amount;

            return this;
        }

        public ClaimPaymentInformationBuilder WithPaymentToProvider(decimal amount)
        {
            this._claimPaymentInformation.Clp04MonetaryAmount = amount;

            return this;
        }

        public ClaimPaymentInformationBuilder WithPatientResponsibility(decimal amount)
        {
            this._claimPaymentInformation.Clp05MonetaryAmount = amount;

            return this;
        }

        /// <summary>
        /// </summary>
        /// <param name="amount"></param>
        /// <param name="groupCode">??</param>
        /// <param name="reasonCode">SELECT ClaimAdjustmentReasonCode FROM [DEV_CDI_ETL].[eob].[EobClaimAdjustmentReasonCodes]</param>
        /// <returns></returns>
        public ClaimPaymentInformationBuilder WithClaimAdjustment(decimal amount, string groupCode, EobClaimAdjustmentReasonCode reasonCode)
        {
            this._claimPaymentInformation.ClaimAdjustmentPivots = this._claimPaymentInformation.ClaimAdjustmentPivots ?? new List<ClaimAdjustmentPivot>();
            this._claimPaymentInformation.ClaimAdjustmentPivots.Add(new ClaimAdjustmentPivot
            {
                ClaimAdjustmentGroupCode = groupCode,
                ClaimAdjustmentReasonCode = reasonCode.ClaimAdjustmentReasonCode,
                MonetaryAmount = amount,
                // must populate the property EobClaimAdjustmentReasonCode for serialization
                EobClaimAdjustmentReasonCode = reasonCode
            });

            return this;
        }

        public ClaimPaymentInformation ToClaimPaymentInformation()
        {
            this._claimPaymentInformation.ClaimPaymentInformationId = Convert.ToInt32(RandomStringGeneratorUtility.GetRandomNumberString(8));
            return this._claimPaymentInformation;
        }
    }
}
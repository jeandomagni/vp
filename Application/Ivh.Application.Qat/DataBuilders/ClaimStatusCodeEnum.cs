﻿namespace Ivh.Application.Qat.DataBuilders
{
    /// <summary>
    /// these were copied from QAtools, don't know if they live in any database
    /// </summary>
    internal enum ClaimStatusCodeEnum
    {
        ProcessedAsPrimary = 1,
        ProcessedAsSecondary = 2,
        ProcessedAsTertiary = 3,
        Denied = 4,
        ProcessedAsPrimaryFwdToAddtlPayer = 19,
        ProcessedAsSecondaryFwdToAddtlPayer = 20,
        ProcessedAsTertiaryFwdToAddtlPayer = 21,
        ReversalOfPreviousPayment = 22,
        NotOurClaimFwdToAddtlPayer = 23,
        PredeterminationPricingOnlyNoPymt = 25
    }
}
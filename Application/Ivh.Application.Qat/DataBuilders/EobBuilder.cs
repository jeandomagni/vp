﻿namespace Ivh.Application.Qat.DataBuilders
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Domain.HospitalData.Eob.Entities;

    internal class EobBuilder
    {
        private readonly InterchangeControlHeaderTrailer _healthCareRemit835;

        public EobBuilder()
        {
            this._healthCareRemit835 = new InterchangeControlHeaderTrailer
            {
                FunctionalGroupHeaderTrailerHeaders = new List<FunctionalGroupHeaderTrailer>(),
                FileTrackerId = null,
                // hardcoded from values copied from from running the test file
                Isa01AuthorizationInformationQualifier = 0,
                Isa02AuthorizationInformation = string.Empty,
                Isa03SecurityInformationQualifier = "00",
                Isa04SecurityInformation = string.Empty,
                Isa05IdQualifier = "ZZ",
                Isa06SenderId = "HT000015-001",
                Isa07IdQualifier = "ZZ",
                Isa08ReceiverId = "HT007214-001",
                Isa0910DateTime = new DateTime(2017, 04, 17, 05, 18, 00),
                Isa11ControlStandardsId = "^",
                Isa12ControlVersion = 501,
                Isa13ControlNumber = 6487,
                Isa14AcknowledgementRequested = false,
                Isa15UsageIndicator = "P",
                Isa16ComponentElementSeparator = ':',
                Iea01GroupsCount = 1,
                Iea02TrailerControlNumber = 6487
            };
        }

        public EobBuilder WithFunctionalGroupHeaderHeaderTrailer(EobPayerFilter payerFilter, DateTime dtmProductionDate)
        {
            FunctionalGroupHeaderTrailer functionalGroupHeaderTrailer = new FunctionalGroupHeaderTrailer
            {
                TransactionHeaders = new List<TransactionSetHeaderTrailer835>(),
                // hardcoded from values copied from from running the test file
                Gs01FunctionalIdentifierCode = "HP",
                Gs02ApplicationSenderCode = "HT000015-001",
                Gs03ApplicationReceiverCode = "HT007214-001",
                Gs0405DateTime = new DateTime(2016, 08, 01, 05, 18, 00),
                Gs06GroupControlNumber = 1,
                Gs07AgencyCode = "X",
                Gs08Version = "005010X221A1",
                Ge01TransactionsCount = 1,
                Ge02GroupTrailerControlNumber = 1
            };

            PayerIdentification payerDetails = new PayerIdentification
            {
                N101EntityIdentifierCode = "PR",
                N102PayerName = payerFilter.UniquePayerValue,
            };

            TransactionSetHeaderTrailer835 transactionSetHeader = new TransactionSetHeaderTrailer835
            {
                Bpr16Date = dtmProductionDate,
                DtmProductionDate = new DateTimeReference {Dtm01DateTimeQualifier = "405", Dtm02Date = dtmProductionDate},
                Transactions = new List<HeaderNumber>(),
                EobPayerFilter = payerFilter,
                PayerDetails = payerDetails,
                St02TransactionSetControlNumber = "1"
            };

            functionalGroupHeaderTrailer.TransactionHeaders.Add(transactionSetHeader);

            this._healthCareRemit835.FunctionalGroupHeaderTrailerHeaders.Add(functionalGroupHeaderTrailer);

            return this;
        }

        public EobBuilder WithHeader()
        {
            TransactionSetHeaderTrailer835 lastTransactionSet = this._healthCareRemit835.FunctionalGroupHeaderTrailerHeaders.Last().TransactionHeaders.Last();
            lastTransactionSet.Transactions = lastTransactionSet.Transactions ?? new List<HeaderNumber>();
            
            HeaderNumber headerNumber = new HeaderNumber
            {
                Lx01AssignedNumber = lastTransactionSet.Transactions.Count + 1
            };

            lastTransactionSet.Transactions.Add(headerNumber);

            return this;
        }

        public EobBuilder WithClaimPaymentInformation(ClaimPaymentInformation claimPaymentInformation)
        {
            HeaderNumber lastTransaction = this._healthCareRemit835.FunctionalGroupHeaderTrailerHeaders.Last().TransactionHeaders.Last().Transactions.Last();
            lastTransaction.ClaimPaymentInformations = lastTransaction.ClaimPaymentInformations ?? new List<ClaimPaymentInformation>();
            lastTransaction.ClaimPaymentInformations.Add(claimPaymentInformation);

            return this;
        }

        public InterchangeControlHeaderTrailer ToEob()
        {
            return this._healthCareRemit835;
        }
    }
}
﻿namespace Ivh.Application.Qat.DataBuilders
{
    using AutoMapper;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Domain.FinanceManagement.Payment.Entities;
    using Domain.HospitalData.Visit.Entities;
    using Ivh.Common.Base.Utilities.Helpers;
    using Ivh.Common.VisitPay.Enums;
    using Ivh.Common.VisitPay.Messages.HospitalData.Dtos;

    internal class VisitChangeDtoBuilder
    {
        private readonly VisitChangeDto _visitChangeDto;
    
        public VisitChangeDtoBuilder(VisitChangeDto visitChangeDto)
        {
            this._visitChangeDto = visitChangeDto;
            this._visitChangeDto.ChangeType = ChangeTypeEnum.Update;
        }

        public VisitChangeDtoBuilder(DateTime visitDate, string billingApplication)
        {
            string sourceSystemKey = RandomSourceSystemKeyGenerator.New("NNNNNNNNNN");

            this._visitChangeDto = new VisitChangeDto
            {
                AdmitDate = visitDate,
                BillingApplication = billingApplication,
                DischargeDate = visitDate,
                SourceSystemKey = sourceSystemKey,
                SourceSystemKeyDisplay = sourceSystemKey,
                Facility = new FacilityDto { FacilityId = 0 }
            };

            this.WithDefaults();
        }

        public VisitChangeDtoBuilder WithDefaults()
        {
            this._visitChangeDto.PrimaryInsuranceType = new PrimaryInsuranceTypeDto { PrimaryInsuranceTypeId = 1 };
            this._visitChangeDto.RevenueWorkCompany = new RevenueWorkCompanyDto { RevenueWorkCompanyId = 1 };
            this._visitChangeDto.PatientType = new PatientTypeDto { PatientTypeId = 3 };
            this._visitChangeDto.LifeCycleStage = new LifeCycleStageDto { LifeCycleStageId = 3 };
            this._visitChangeDto.VisitInsurancePlans = new List<VisitInsurancePlanChangeDto>();
            this._visitChangeDto.InterestRefund = false;
            this._visitChangeDto.InterestZero = false;
            this._visitChangeDto.ServiceGroupId = null;
            this._visitChangeDto.AgingTier = (AgingTierEnum)0;
            this._visitChangeDto.VpEligible = true;
            this._visitChangeDto.BillingHold = false;
            this._visitChangeDto.Redact = false;
            this._visitChangeDto.ChangeType = ChangeTypeEnum.Insert;
            this._visitChangeDto.IsChanged = true;
            this._visitChangeDto.PaymentPriority = null;

            return this;
        }

        public VisitChangeDtoBuilder WithTransaction(string description, decimal amount, VpTransactionTypeEnum transactionType, DateTime? transactionDate = null)
        {
            this._visitChangeDto.VisitTransactions = this._visitChangeDto.VisitTransactions ?? new List<VisitTransactionChangeDto>();
            
            DateTime transactionDateToUse = transactionDate ?? this._visitChangeDto.DischargeDate ?? DateTime.UtcNow;

            VisitTransactionChangeDto visitTransactionChangeDto = new VisitTransactionChangeDto
            {
                PostDate = transactionDateToUse,
                SourceSystemKey = RandomSourceSystemKeyGenerator.New("NNNNNNNNNNNN"),
                TransactionAmount = amount,
                TransactionDate = transactionDateToUse,
                TransactionCodeId = 1,
                TransactionDescription = description,
                VpTransactionTypeId = (int)transactionType,
                ChangeType = ChangeTypeEnum.Insert
            };

            this._visitChangeDto.VisitTransactions.Add(visitTransactionChangeDto);

            return this;
        }

        public VisitChangeDtoBuilder WithPaymentAllocation(int paymentAllocationId)
        {
            if (this._visitChangeDto.VisitPaymentAllocations == null)
            {
                this._visitChangeDto.VisitPaymentAllocations = new List<VisitPaymentAllocationChangeDto>();
            }

            this._visitChangeDto.VisitPaymentAllocations.Add(new VisitPaymentAllocationChangeDto
            {
                PaymentAllocationId = paymentAllocationId,
                VisitId = this._visitChangeDto.VisitId
            });

            return this;
        }

        public VisitChangeDtoBuilder ClearPaymentAllocations(IList<PaymentAllocation> unclearedPaymentAllocations)
        {
            foreach (PaymentAllocation paymentAllocation in unclearedPaymentAllocations)
            {
                VpTransactionTypeEnum type = paymentAllocation.PaymentAllocationType == PaymentAllocationTypeEnum.VisitPrincipal ? VpTransactionTypeEnum.VppPrincipalPayment : VpTransactionTypeEnum.VppInterestPayment; // todo: discount
                string desc = type == VpTransactionTypeEnum.VppPrincipalPayment ? "Principal Payment" : "Interest Payment";

                decimal transactionAmount = paymentAllocation.ActualAmount * -1m;
                this.WithTransaction(desc, transactionAmount, type, DateTime.UtcNow.AddMinutes(-1));
                this.WithPaymentAllocation(paymentAllocation.PaymentAllocationId);
            }

            return this;
        }

        public VisitTransaction AddTransaction(string description, decimal amount, VpTransactionTypeEnum transactionType, DateTime? transactionDate = null)
        {
            this.WithTransaction(description, amount, transactionType, transactionDate);
            VisitTransactionChangeDto transactionChangeDto = this._visitChangeDto.VisitTransactions.Last();
            return Mapper.Map<VisitTransaction>(transactionChangeDto);
        }
        
        public VisitChangeDtoBuilder WithPatient(HsGuarantorDto hsGuarantorDto)
        {
            return this.WithPatient(hsGuarantorDto.FirstName, hsGuarantorDto.LastName, hsGuarantorDto.DOB.GetValueOrDefault(DateTime.UtcNow.AddYears(-50)), true, false);
        }

        public VisitChangeDtoBuilder WithPatient(string patientFirstName, string patientLastName, DateTime patientDob, bool isPatientGuarantor = true, bool isPatientMinor = false)
        {
            this._visitChangeDto.PatientFirstName = patientFirstName;
            this._visitChangeDto.PatientLastName = patientLastName;
            this._visitChangeDto.IsPatientGuarantor = isPatientGuarantor;
            this._visitChangeDto.IsPatientMinor = isPatientMinor;
            this._visitChangeDto.PatientDOB = patientDob;

            return this;
        }

        public VisitChangeDtoBuilder WithDescription(string description)
        {
            this._visitChangeDto.VisitDescription = description ?? string.Empty;

            return this;
        }

        public VisitChangeDtoBuilder WithDiscountEligible(int? insurancePlanId = null)
        {
            this._visitChangeDto.VisitInsurancePlans = this._visitChangeDto.VisitInsurancePlans ?? new List<VisitInsurancePlanChangeDto>();
            this._visitChangeDto.VisitInsurancePlans.Add(new VisitInsurancePlanChangeDto 
            {
                InsurancePlanId = insurancePlanId.GetValueOrDefault(-100) // (-100) DEMO INSURANCE 3% DISCOUNT
            });

            return this;
        }

        public VisitChangeDto ToVisitChangeDto()
        {
            this._visitChangeDto.HsCurrentBalance = this._visitChangeDto.VisitTransactions.Where(x => x.VpTransactionTypeId != (int)VpTransactionTypeEnum.VppInterestPayment).Sum(x => x.TransactionAmount);

            return this._visitChangeDto;
        }
    }
}
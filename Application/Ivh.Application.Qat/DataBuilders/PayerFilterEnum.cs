﻿namespace Ivh.Application.Qat.DataBuilders
{
    /// <summary>
    /// SELECT * FROM [DEV_VP_APP].[eob].[EobPayerFilter]
    /// update this enum as necessary
    /// </summary>
    internal enum PayerFilterEnum
    {
        DemoPayor = -100,
        Aetna = 3,
        PacificSourceCommunityHealthPlans = 123,
        ShSelectHealthInc = 161
    }
}
﻿namespace Ivh.Application.Qat.DataBuilders
{
    using System;
    using System.Collections.Generic;
    using Core.Common.Dtos;
    using FinanceManagement.Common.Dtos;
    using FinanceManagement.Common.Enums;
    using Ivh.Common.Base.Utilities.Extensions;
    using Ivh.Common.VisitPay.Enums;

    internal class PaymentMethodDtoBuilder
    {
        private readonly PaymentMethodDto _paymentMethodDto;
        private string _accountNumber;
        private string _securityNumber;
        private string _routingNumber;

        public PaymentMethodDtoBuilder()
        {
            this._paymentMethodDto = new PaymentMethodDto
            {
                IsActive = true,
                IsPrimary = false,
                BillingAddresses = new List<PaymentMethodBillingAddressDto>()
            };
        }

        public PaymentMethodDtoBuilder WithAchCheckingGood(string nickname = "My Bank Account")
        {
            this._paymentMethodDto.PaymentMethodType = PaymentMethodTypeEnum.AchChecking;
            this._paymentMethodDto.AccountNickName = nickname;
            this._accountNumber = "55544433221";
            this._routingNumber = "789456124";

            return this;
        }

        public PaymentMethodDtoBuilder WithAmexGood(string nickname = "My Amex Card")
        {
            this._paymentMethodDto.PaymentMethodType = PaymentMethodTypeEnum.AmericanExpress;
            this._paymentMethodDto.AccountNickName = nickname;
            this._paymentMethodDto.ExpDate = "0429";
            this._paymentMethodDto.BillingAddresses.Add(new PaymentMethodBillingAddressDto
            {
                Address1 = "12 Colorado Blvd.",
                City = "Elsewhere",
                State = "IL",
                Zip = "40000"
            });

            this._accountNumber = "341111111111111";
            this._securityNumber = "4000";

            return this;
        }
        
        public PaymentMethodDtoBuilder WithDiscoverGood(string nickname = "My Discover Card")
        {
            this._paymentMethodDto.PaymentMethodType = PaymentMethodTypeEnum.Discover;
            this._paymentMethodDto.AccountNickName = nickname;
            this._paymentMethodDto.ExpDate = "0429";
            this._paymentMethodDto.BillingAddresses.Add(new PaymentMethodBillingAddressDto
            {
                Address1 = "6789 Green Ave.",
                City = "Nowhere",
                State = "MA",
                Zip = "12345"
            });
            
            this._accountNumber = "6011111111111117";
            this._securityNumber = "";
            
            return this;
        }

        public PaymentMethodDtoBuilder WithMastercardGood(string nickname = "My Mastercard")
        {
            this._paymentMethodDto.PaymentMethodType = PaymentMethodTypeEnum.Mastercard;
            this._paymentMethodDto.AccountNickName = nickname;
            this._paymentMethodDto.ExpDate = "0429";
            this._paymentMethodDto.BillingAddresses.Add(new PaymentMethodBillingAddressDto
            {
                Address1 = "4000 Main St.",
                City = "Anytown",
                State = "AZ",
                Zip = "85001"
            });

            this._accountNumber = "5411111111111115";
            this._securityNumber = "777";
            
            return this;
        }
        
        public PaymentMethodDtoBuilder WithVisaGood(string nickname = "My Visa Card")
        {
            this._paymentMethodDto.PaymentMethodType = PaymentMethodTypeEnum.Visa;
            this._paymentMethodDto.AccountNickName = nickname;
            this._paymentMethodDto.ExpDate = "0429";
            this._paymentMethodDto.BillingAddresses.Add(new PaymentMethodBillingAddressDto
            {
                Address1 = "123 Test St.",
                City = "Somewhere",
                State = "CA",
                Zip = "90001"
            });

            this._accountNumber = "4111111111111111";
            this._securityNumber = "123";

            return this;
        }

        public PaymentMethodDtoBuilder WithCardError(string nickname = "My Broken Card")
        {
            this._paymentMethodDto.PaymentMethodType = PaymentMethodTypeEnum.Visa;
            this._paymentMethodDto.ExpDate = "0429";
            this._paymentMethodDto.BillingAddresses.Add(new PaymentMethodBillingAddressDto
            {
                Address1 = "123 Test St.",
                City = "Somewhere",
                State = "CA",
                Zip = "90001"
            });

            this._accountNumber = "4444111144441111";
            this._securityNumber = "";

            return this;
        }

        public PaymentMethodDtoBuilder WithPrimary(bool isPrimary = true)
        {
            this._paymentMethodDto.IsPrimary = isPrimary;

            return this;
        }

        public PaymentMethodDtoBuilder WithNickname(string nickname)
        {
            this._paymentMethodDto.AccountNickName = nickname;

            return this;
        }

        public PaymentMethodDtoBuilder WithAccountType(int paymentMethodAccountTypeId)
        {
            // there's no enum for this one
            // dbo.PaymentMethodAccountType
            this._paymentMethodDto.AccountTypeId = paymentMethodAccountTypeId;
            this._paymentMethodDto.PaymentMethodAccountType = new PaymentMethodAccountTypeDto
            {
                PaymentMethodAccountTypeId = paymentMethodAccountTypeId
            };

            return this;
        }

        public PaymentMethodDtoBuilder WithProviderType(PaymentMethodProviderTypeEnum providerType)
        {
            this._paymentMethodDto.ProviderTypeId = (int)providerType;
            this._paymentMethodDto.PaymentMethodProviderType = new PaymentMethodProviderTypeDto
            {
                PaymentMethodProviderTypeId = (int)providerType
            };

            return this;
        }

        public PaymentMethodDtoBuilder SetPaymentMethodId(int paymentMethodId)
        {
            this._paymentMethodDto.PaymentMethodId = paymentMethodId;

            return this;
        }

        public Tuple<PaymentMethodDto, string, string> ForSubmit(GuarantorDto guarantorDto)
        {
            this._paymentMethodDto.CreatedDate = DateTime.UtcNow;
            this._paymentMethodDto.CreatedUserId = guarantorDto.User.VisitPayUserId;
            this._paymentMethodDto.UpdatedDate = DateTime.UtcNow;
            this._paymentMethodDto.UpdatedUserId = guarantorDto.User.VisitPayUserId;
            this._paymentMethodDto.VpGuarantorId = guarantorDto.VpGuarantorId;

            string securityCodeOrRoutingNumber;
            if (this._paymentMethodDto.PaymentMethodType.IsInCategory(PaymentMethodTypeEnumCategory.Card))
            {
                securityCodeOrRoutingNumber = this._securityNumber;

                if (this._paymentMethodDto.PaymentMethodType == PaymentMethodTypeEnum.Visa)
                {
                    this._paymentMethodDto.BillingAddresses.Add(new PaymentMethodBillingAddressDto
                    {
                        Address1 = "123 Test St.",
                        City = "Somewhere",
                        State = "CA",
                        Zip = "90001"
                    });
                }
                else if (this._paymentMethodDto.PaymentMethodType == PaymentMethodTypeEnum.AmericanExpress)
                {
                    this._paymentMethodDto.BillingAddresses.Add(new PaymentMethodBillingAddressDto
                    {
                        Address1 = "12 Colorado Blvd.",
                        City = "Elsewhere",
                        State = "IL",
                        Zip = "40000"
                    });
                }
            }
            else
            {
                securityCodeOrRoutingNumber = this._routingNumber;
            }

            return new Tuple<PaymentMethodDto, string, string>(this._paymentMethodDto, this._accountNumber, securityCodeOrRoutingNumber);
        }

        public PaymentMethodDto ToPaymentMethodDto()
        {
            return this._paymentMethodDto;
        }
    }
}
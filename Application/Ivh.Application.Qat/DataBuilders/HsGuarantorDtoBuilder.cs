﻿namespace Ivh.Application.Qat.DataBuilders
{
    using System;
    using System.Linq;
    using Domain.Qat.Services;
    using Ivh.Common.Base.Utilities.Helpers;
    using Ivh.Common.VisitPay.Messages.HospitalData.Dtos;

    public class HsGuarantorDtoBuilder
    {
        private readonly HsGuarantorDto _hsGuarantorDto;

        public HsGuarantorDtoBuilder(IPersonaCredential credential, DateTime dateOfBirth) : this(credential.FirstName, credential.LastName, dateOfBirth)
        {
        }

        public HsGuarantorDtoBuilder(string firstName, string lastName, DateTime dateOfBirth)
        {
            // trying to beat the invalid ssn patterns lookup for matching.
            string ssn = string.Concat("456", new Random().Next(123456, 999999));
            string ssn4 = string.Concat(ssn.Reverse().Take(4).Reverse());

            this._hsGuarantorDto = new HsGuarantorDto
            {
                SourceSystemKey = RandomSourceSystemKeyGenerator.New("NNNNNNNN"),
                FirstName = firstName,
                LastName = lastName,
                DOB = dateOfBirth,
                SSN = ssn,
                SSN4 = ssn4,
                VpEligible = true
            };

            this.WithAddress("123 test st", "Somewhere", "ID", "83705");
        }

        public HsGuarantorDtoBuilder WithAddress(string address1, string city, string state, string zip)
        {
            this._hsGuarantorDto.AddressLine1 = address1;
            this._hsGuarantorDto.City = city;
            this._hsGuarantorDto.StateProvince = state;
            this._hsGuarantorDto.PostalCode = zip;
            this._hsGuarantorDto.Country = "US";

            return this;
        }

        public HsGuarantorDtoBuilder WithSsn(string ssn)
        {
            this._hsGuarantorDto.SSN = ssn;
            this._hsGuarantorDto.SSN4 = string.Concat(ssn.Reverse().Take(4).Reverse());

            return this;
        }

        public HsGuarantorDto ToHsGuarantorDto()
        {
            return this._hsGuarantorDto;
        }
    }
}
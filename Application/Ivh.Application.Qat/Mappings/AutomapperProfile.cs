﻿namespace Ivh.Application.Qat.Mappings
{
    using AutoMapper;
    using Common.Dtos;
    using Domain.Email.Entities;
    using Domain.FinanceManagement.Payment.Entities;
    using Domain.HospitalData.HsGuarantor.Entities;
    using Domain.HospitalData.Visit.Entities;
    using Domain.Qat.Entities;
    using Ivh.Common.Base.Utilities.Extensions;
    using Ivh.Common.VisitPay.Messages.HospitalData.Dtos;

    public class AutomapperProfile : Profile
    {
        public AutomapperProfile()
        {
            this.CreateMap<VpOutboundVisitTransaction, UnclearedPaymentAllocationDto>()
                .ForMember(dest => dest.HsVisitId, opts => opts.MapFrom(src => src.Visit.VisitId));

            this.CreateMap<Communication, CommunicationDto>()
                .ForMember(dest => dest.Body, opts => opts.MapFrom(src => src.Body))
                .ForMember(dest => dest.SentToUserId, opts => opts.MapFrom(src => src.SentToUserId))
                .ForMember(dest => dest.PhoneNumber, opts => opts.MapFrom(src => src.ToAddress));

            this.CreateMap<Payment, TextToPayPaymentDto>()
                .ForMember(dest => dest.CurrentPaymentStatus, opt => opt.MapFrom(src => src.CurrentPaymentStatus.ToString()));

            this.CreateMapBidirectional<VisitChangeDto, Visit>();
            this.CreateMapBidirectional<VisitTransactionChangeDto, VisitTransaction>();
            this.CreateMapBidirectional<VisitInsurancePlanChangeDto, VisitInsurancePlan>();

            this.CreateMapBidirectional<VisitChangeDto, MutableBaseVisit>();
            this.CreateMapBidirectional<VisitTransactionChangeDto, MutableBaseVisitTransaction>();
            this.CreateMapBidirectional<HsGuarantorDto, MutableBaseHsGuarantor>();

            this.CreateMapBidirectional<Visit, MutableBaseVisit>();
            this.CreateMap<VisitTransaction, MutableBaseVisitTransaction>()
                .ForMember(dest => dest.VisitId, opts => opts.MapFrom(src => src.Visit.VisitId))
                .ForMember(dest => dest.VpTransactionTypeId, opts => opts.MapFrom(src => src.VpTransactionType.VpTransactionTypeId));
            this.CreateMap<MutableBaseVisitTransaction, VisitTransaction>();
            this.CreateMapBidirectional<HsGuarantor, MutableBaseHsGuarantor>();
        }
    }
}
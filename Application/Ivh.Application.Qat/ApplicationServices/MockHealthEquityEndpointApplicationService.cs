﻿namespace Ivh.Application.Qat.ApplicationServices
{
    using System;
    using System.Collections.Generic;
    using Common.Dtos;
    using Common.Interfaces;
    using Domain.Qat.Interfaces;
    using Ivh.Common.Base.Enums;
    using Ivh.Common.VisitPay.Enums;

    public class MockHealthEquityEndpointApplicationService : IMockHealthEquityEndpointApplicationService
    {
        private readonly Lazy<IMockHealthEquityEndpointService> _mockHealthEquityEndpointService;

        public MockHealthEquityEndpointApplicationService(
            Lazy<IMockHealthEquityEndpointService> mockHealthEquityEndpointService
        )
        {
            this._mockHealthEquityEndpointService = mockHealthEquityEndpointService;
        }

        public IList<SavingsAccountInfoDto> GetSavingsAccountInfoDtos(int vpguarantorId, IList<AccountStatusApiModelEnum> statuses, IList<AccountTypeFlagApiModelEnum> types)
        {
            return this._mockHealthEquityEndpointService.Value.GetSavingsAccountInfos(vpguarantorId, statuses, types);
        } 
    }
}

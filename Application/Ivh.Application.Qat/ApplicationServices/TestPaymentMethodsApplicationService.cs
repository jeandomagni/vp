﻿namespace Ivh.Application.Qat.ApplicationServices
{
    using System;
    using System.Linq;
    using System.Threading.Tasks;
    using AutoMapper;
    using Common.Interfaces;
    using Domain.FinanceManagement.Entities;
    using Domain.FinanceManagement.Interfaces;
    using Domain.FinanceManagement.ValueTypes;
    using Domain.Qat.Interfaces;
    using FinanceManagement.Common.Dtos;

    public class TestPaymentMethodsApplicationService : ITestPaymentMethodsApplicationService
    {
        private readonly Lazy<IPaymentProvider> _paymentProvider;
        private readonly Lazy<ITestPaymentMethodsService> _testPaymentMethodsService;

        public TestPaymentMethodsApplicationService(Lazy<IPaymentProvider> paymentProvider, Lazy<ITestPaymentMethodsService> testPaymentMethodsService)
        {
            this._paymentProvider = paymentProvider;
            this._testPaymentMethodsService = testPaymentMethodsService;
        }

        public async Task<int> SavePaymentMethodAsync(PaymentMethodDto paymentMethodDto, int visitPayUserId, bool isPrimary, string accountNumber, string securityCodeOrRoutingNumber)
        {
            PaymentMethod paymentMethod = Mapper.Map<PaymentMethod>(paymentMethodDto);
            paymentMethod.LastFour = string.Join("", (accountNumber ?? "").Reverse().Take(4).Reverse());

            BillingIdResult billingResult;

            if (paymentMethod.IsAchType)
            {
                billingResult = await this._paymentProvider.Value.StoreBankAccountBillingIdAsync(paymentMethod, accountNumber, securityCodeOrRoutingNumber);
            }
            else
            {
                billingResult = await this._paymentProvider.Value.StoreCreditDebitCardBillingIdAsync(paymentMethod, accountNumber, securityCodeOrRoutingNumber);
            }

            paymentMethod.GatewayToken = billingResult.GatewayToken;
            if (!string.IsNullOrWhiteSpace(billingResult.BillingID))
            {
                paymentMethod.BillingId = billingResult.BillingID;
            }

            this._testPaymentMethodsService.Value.SaveOrUpdate(paymentMethod, visitPayUserId);

            return paymentMethod.PaymentMethodId;
        }
    }
}
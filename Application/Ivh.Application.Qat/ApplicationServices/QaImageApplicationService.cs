﻿namespace Ivh.Application.Qat.ApplicationServices
{
    using System;
    using System.IO;
    using System.Threading.Tasks;
    using Base.Common.Interfaces;
    using Base.Services;
    using Common.Interfaces;
    using Domain.FileStorage.Entities;
    using Domain.FileStorage.Interfaces;
    using MimeTypes;

    public class QaImageApplicationService : ApplicationService, IQaImageApplicationService
    {
        private readonly Lazy<IFileStorageService> _fileStorageService;

        public QaImageApplicationService(
            Lazy<IApplicationServiceCommonService> applicationServiceCommonService,
            Lazy<IFileStorageService> fileStorageService
        ) : base(applicationServiceCommonService)
        {
            this._fileStorageService = fileStorageService;
        }

        public async Task<string> UploadImageAsync(string originalFilename, byte[] bytes)
        {
            DateTime now = DateTime.UtcNow;
            Guid guid = Guid.NewGuid();
            string extension = Path.GetExtension(originalFilename);
            string filename = $"{guid}{extension}";
            string mimeType = MimeTypeMap.GetMimeType(extension);

            FileStored file = new FileStored
            {
                ExternalKey = guid,
                FileContents = bytes,
                FileDateTime = now,
                FileMimeType = mimeType,
                Filename = filename,
                InsertDate = now
            };

            bool result = await this._fileStorageService.Value.SaveFileAsync(file);
            if (result)
            {
                return filename;
            }

            return null;
        }
    }
}
﻿namespace Ivh.Application.Qat.ApplicationServices
{
    using System;
    using Base.Common.Interfaces;
    using Base.Services;
    using Common.Interfaces;
    using Domain.Visit.Entities;
    using Domain.Visit.Interfaces;
    using Ivh.Common.Data.Connection.Connections;
    using Ivh.Common.Data.Interfaces;
    using NHibernate;

    public class QaFacilityApplicationService : ApplicationService, IQaFacilityApplicationService
    {
        private readonly ISession _session;
        private readonly Lazy<IFacilityRepository> _facilityRepository;

        public QaFacilityApplicationService(
            Lazy<IApplicationServiceCommonService> applicationServiceCommonService, 
            Lazy<IFacilityRepository> facilityRepository, 
            ISessionContext<VisitPay> session) : base(applicationServiceCommonService)
        {
            this._facilityRepository = facilityRepository;
            this._session = session.Session;;
        }
        
        public void UpdateFacility(int facilityId, string facilityName, bool updateLogo, string logoFilename)
        {
            Facility facility = this._facilityRepository.Value.GetById(facilityId);
            if (facility == null)
            {
                return;
            }

            // this is a readonly entity and i don't want to change that just for demo purposes
            ISQLQuery query = this._session.CreateSQLQuery($"UPDATE dbo.Facility SET {nameof(Facility.FacilityDescription)} = :FacilityDescription, {nameof(Facility.LogoFilename)} = :LogoFilename WHERE {nameof(Facility.FacilityId)} = :FacilityId");
            query.SetParameter("FacilityDescription", facilityName);
            query.SetParameter("LogoFilename", updateLogo ? logoFilename : facility.LogoFilename);
            query.SetParameter("FacilityId", facilityId);
            query.ExecuteUpdate();

            this._session.Evict(facility);
        }
    }
}
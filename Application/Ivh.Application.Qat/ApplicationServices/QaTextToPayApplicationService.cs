﻿namespace Ivh.Application.Qat.ApplicationServices
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using AutoMapper;
    using Common.Dtos;
    using Common.Interfaces;
    using Domain.Email.Entities;
    using Domain.FinanceManagement.Payment.Entities;
    using Domain.Guarantor.Interfaces;
    using Domain.Qat.Interfaces;
    using Domain.User.Entities;
    using Domain.User.Interfaces;
    using Ivh.Common.Base.Enums;
    using Ivh.Common.VisitPay.Enums;

    public class QaTextToPayApplicationService : IQaTextToPayApplicationService
    {
        private readonly Lazy<IQaCommunicationService> _communicationService;
        private readonly Lazy<IVisitPayUserRepository> _visitPayUserRepository;
        private readonly Lazy<IGuarantorService> _guarantorService;
        private readonly Lazy<IQaPaymentService> _paymentService;

        public QaTextToPayApplicationService(
            Lazy<IQaPaymentService> paymentService,
            Lazy<IQaCommunicationService> communicationService,
            Lazy<IVisitPayUserRepository> visitPayUserRepository,
            Lazy<IGuarantorService> guarantorService
        )
        {
            this._paymentService = paymentService;
            this._communicationService = communicationService;
            this._visitPayUserRepository = visitPayUserRepository;
            this._guarantorService = guarantorService;
        }

        public List<CommunicationDto> GetTextToPayPaymentOptionCommunications()
        {
            IList<Communication> textToPayPaymentOptionCommunications = this._communicationService.Value.GetTextToPayPaymentOptionCommunications();
            List<CommunicationDto> communicationDtos = Mapper.Map<List<CommunicationDto>>(textToPayPaymentOptionCommunications);
            VisitPayUserFilter visitPayUserFilter = new VisitPayUserFilter {VisitPayUserIds = communicationDtos.Select(x => x.SentToUserId).ToList()};
            VisitPayUserResults visitPayUsers = this._visitPayUserRepository.Value.GetVisitPayUsers(visitPayUserFilter, (int) VisitPayRoleEnum.Patient, 0, int.MaxValue);

            foreach (VisitPayUser visitPayUser in visitPayUsers.VisitPayUsers)
            {
                CommunicationDto itemToChange = communicationDtos.FirstOrDefault(d => d.SentToUserId == visitPayUser.VisitPayUserId);
                if (itemToChange != null)
                {
                    itemToChange.UserName = visitPayUser.UserName;
                }
            }

            return communicationDtos;
        }

        public CommunicationDto GetTextToPayPaymentOptionCommunication(int vpUserId)
        {
            Communication communication = this._communicationService.Value.GetTextToPayPaymentOptionCommunication(vpUserId);
            return Mapper.Map<CommunicationDto>(communication);
        }

        public List<TextToPayPaymentDto> GetTextToPayPayments(int visitPayUserId)
        {
            int guarantorId = this._guarantorService.Value.GetGuarantorId(visitPayUserId);
            List<Payment> payments = this._paymentService.Value.GetTextToPayPayments(guarantorId);
            List<TextToPayPaymentDto> textToPayPaymentDtos = Mapper.Map<List<TextToPayPaymentDto>>(payments);
            return textToPayPaymentDtos.OrderByDescending(x => x.PaymentId).ToList();
        }
    }
}
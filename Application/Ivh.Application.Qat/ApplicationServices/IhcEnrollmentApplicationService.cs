﻿namespace Ivh.Application.Qat.ApplicationServices
{
    using System;
    using Common.Dtos;
    using Common.Interfaces;
    using Domain.Qat.Entities;
    using Domain.Qat.Interfaces;

    public class IhcEnrollmentApplicationService : IIhcEnrollmentApplicationService
    {
        private readonly Lazy<IIhcEnrollmentService> _ihcEnrollmentService;

        public IhcEnrollmentApplicationService(Lazy<IIhcEnrollmentService> ihcEnrollmentService)
        {
            this._ihcEnrollmentService = ihcEnrollmentService;
        }

        public IhcEnrollmentResponseDto GetByGuarantorId(string guarantorId)
        {
            IhcEnrollmentResponse response = this._ihcEnrollmentService.Value.GetByGuarantorId(guarantorId);
            if (response == null)
            {
                return null;
            }

            return new IhcEnrollmentResponseDto
            {
                IhcEnrollmentResponseId = response.IhcEnrollmentResponseID,
                GuarantorId = response.GuarantorId,
                ExpectedEnrollmentStatus = response.ExpectedEnrollmentStatus,
                Response = response.Response,
                ReturnErrorCode = response.ReturnErrorCode
            };
        }
    }
}
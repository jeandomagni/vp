﻿namespace Ivh.Application.Qat.ApplicationServices
{
    using Common.Interfaces;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using AutoMapper;
    using Base.Services;
    using Core.Common.Dtos.Eob;
    using Domain.Eob.Entities;
    using Domain.Qat.Interfaces;
    using Ivh.Application.Base.Common.Interfaces;
    using Ivh.Common.Base.Utilities.Extensions;

    public class QaVisitApplicationService : ApplicationService, IQaVisitApplicationService
    {
        private const int DemoEobPayerFilterId = -100;
        
        private readonly Lazy<IQaVisitService> _qaVisitService;

        public QaVisitApplicationService(
            Lazy<IApplicationServiceCommonService> applicationServiceCommonService,
            Lazy<IQaVisitService> qaVisitService
            ) : base(applicationServiceCommonService)
        {
            this._qaVisitService = qaVisitService;
        }
        
        public IList<EobExternalLinkDto> GetEobExternalLinks()
        {
            IList<EobExternalLink> eobExternalLinks = this._qaVisitService.Value.GetEobExternalLinks();

            return Mapper.Map<List<EobExternalLinkDto>>(eobExternalLinks);
        }

        public bool UpdateDemoEobPayerExternalLink(int eobExternalLinkId)
        {
            return this._qaVisitService.Value.UpdateEobPayerFilterExternalLink(DemoEobPayerFilterId, eobExternalLinkId);
        }
    }
}

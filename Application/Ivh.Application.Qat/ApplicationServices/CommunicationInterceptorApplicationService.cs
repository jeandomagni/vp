﻿namespace Ivh.Application.Qat.ApplicationServices
{
    using System;
    using Common.Interfaces;
    using Domain.Qat.Interfaces;

    public class CommunicationInterceptorApplicationService : ICommunicationInterceptorApplicationService
    {
        private readonly Lazy<ICommunicationInterceptorService> _communicationInterceptorService;

        public CommunicationInterceptorApplicationService(
            Lazy<ICommunicationInterceptorService> communicationInterceptorService)
        {
            this._communicationInterceptorService = communicationInterceptorService;
        }

        public bool IsActive()
        {
            return this._communicationInterceptorService.Value.IsActive();
        }

        public void Activate(string uriString)
        {
            this._communicationInterceptorService.Value.Activate(uriString);
        }

        public void Deactivate()
        {
            this._communicationInterceptorService.Value.Deactivate();
        }
    }
}
﻿namespace Ivh.Application.Qat.ApplicationServices
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Common.Interfaces;
    using Domain.Rager.Interfaces;
    using Domain.Visit.Interfaces;
    using Ivh.Common.ServiceBus.Interfaces;
    using Ivh.Common.VisitPay.Enums;
    using Ivh.Common.VisitPay.Messages.Aging;

    public class QaAgingApplicationService : IQaAgingApplicationService
    {
        private readonly IBus _bus;
        private readonly Lazy<IVisitAgeService> _visitAgeService;
        private readonly Lazy<IVisitService> _visitService;

        public QaAgingApplicationService(
            Lazy<IVisitAgeService> visitAgeService,
            Lazy<IVisitService> visitService, IBus bus)
        {
            this._visitAgeService = visitAgeService;
            this._visitService = visitService;
            this._bus = bus;
        }

        public void AddVisitAgesWithRollback(int vpGuarantorId, int daysToRollBack)
        {
            IReadOnlyList<Domain.Visit.Entities.Visit> vpVisits = this._visitService.Value.GetVisits(vpGuarantorId);
            IList<Domain.Rager.Entities.Visit> ragerVisits = new List<Domain.Rager.Entities.Visit>();

            foreach (Domain.Visit.Entities.Visit vpVisit in vpVisits)
            {
                Domain.Rager.Entities.Visit ragerVisit = this._visitAgeService.Value.GetVisitBySourceKeyAndBillingId(vpVisit.SourceSystemKey, vpVisit.BillingSystemId ?? 0);
                if (ragerVisit != null)
                {
                    ragerVisits.Add(ragerVisit);
                }
            }

            foreach (Domain.Rager.Entities.Visit ragerVisit in ragerVisits.Where(x => x.IsEligibleToAge))
            {
                IEnumerable<int> days = Enumerable.Range(1, Math.Abs(daysToRollBack)).Reverse();
                foreach (int day in days)
                {
                    DateTime dateToProcess = DateTime.UtcNow.AddDays(day * -1);
                    AgeVisitMessage ageVisitMessage = new AgeVisitMessage
                    {
                        VisitId = ragerVisit.VisitId,
                        DateToProcess = dateToProcess
                    };
                    this._bus.PublishMessage(ageVisitMessage);
                }
            }
        }

        public void SetAgingTier(string sourceSystemKey, int billingSystemId, int agingTier)
        {
            Ivh.Common.VisitPay.Messages.Aging.Dtos.VisitDto visit = new Ivh.Common.VisitPay.Messages.Aging.Dtos.VisitDto
            {
                VisitSourceSystemKey = sourceSystemKey,
                VisitBillingSystemId = billingSystemId
            };

            SetVisitAgeMessage setVisitAgeMessage = new SetVisitAgeMessage {Visit = visit, AgingTierEnum = (AgingTierEnum) agingTier};

            this._bus.PublishMessage(setVisitAgeMessage);
        }
    }
}

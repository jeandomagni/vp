﻿namespace Ivh.Application.Qat.ApplicationServices
{
    using System;
    using System.Web;
    using Domain.HospitalData.HsGuarantor.Entities;
    using Domain.HospitalData.HsGuarantor.Interfaces;
    using Ivh.Application.Qat.Common.Interfaces;
    using Ivh.Common.VisitPay.Enums;
    using Ivh.Domain.Core.Sso.Interfaces;

    public class QaSsoApplicationService : IQaSsoApplicationService
    {
        private readonly Lazy<IHsGuarantorService> _hsGuarantorService;
        private readonly Lazy<ISsoService> _ssoService;

        public QaSsoApplicationService(
             Lazy<IHsGuarantorService> hsGuarantorService
            ,Lazy<ISsoService> ssoService
            )
        {
            this._hsGuarantorService = hsGuarantorService;
            this._ssoService = ssoService;
        }


        public string GenerateEncryptedSsoTokenHsGuarantor(int hsGuarantorId, SsoProviderEnum providerEnum, string ssoSsk, DateTime dob)
        {
            HsGuarantor guarantor = this._hsGuarantorService.Value.GetHsGuarantor(hsGuarantorId);
            return HttpUtility.UrlEncode(this._ssoService.Value.GenerateEncryptedSsoToken(guarantor.FirstName, guarantor.LastName, providerEnum, ssoSsk, dob));
        }
    }
}

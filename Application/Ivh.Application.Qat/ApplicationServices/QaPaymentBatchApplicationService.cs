﻿namespace Ivh.Application.Qat.ApplicationServices
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using AutoMapper;
    using Base.Common.Interfaces;
    using Base.Services;
    using Common.Dtos;
    using Common.Interfaces;
    using Domain.HospitalData.Visit.Entities;
    using Domain.HospitalData.Visit.Interfaces;
    using Ivh.Application.Core.Common.Models.Lockbox;
    using Ivh.Common.Base.Utilities.Extensions;
    using Ivh.Common.VisitPay.Messages.FinanceManagement;

    public class QaPaymentBatchApplicationService : ApplicationService, IQaPaymentBatchApplicationService
    {
        private readonly Lazy<IOutboundVisitTransactionRepository> _outboundVisitTransactionRepository;

        public QaPaymentBatchApplicationService(
            Lazy<IApplicationServiceCommonService> applicationServiceCommonService,
            Lazy<IOutboundVisitTransactionRepository> outboundVisitTransactionRepository) : base(applicationServiceCommonService)
        {
            this._outboundVisitTransactionRepository = outboundVisitTransactionRepository;
        }

        public IList<UnclearedPaymentAllocationDto> GetUnclearedOutboundVisitTransactions()
        {
            IList<VpOutboundVisitTransaction> unclearedVpOutboundVisitTransactions = this._outboundVisitTransactionRepository.Value.GetUnclearedOutboundVisitTransactions();

            IList<UnclearedPaymentAllocationDto> unclearedPaymentAllocationDtos = Mapper.Map<IList<UnclearedPaymentAllocationDto>>(unclearedVpOutboundVisitTransactions);

            return unclearedPaymentAllocationDtos;
        }

        public IList<UnclearedPaymentAllocationDto> GetUnclearedOutboundVisitTransactions(int visitId)
        {
            IList<VpOutboundVisitTransaction> unclearedVpOutboundVisitTransactions = this._outboundVisitTransactionRepository.Value.GetUnclearedOutboundVisitTransactions(visitId);

            IList<UnclearedPaymentAllocationDto> unclearedPaymentAllocationDtos = Mapper.Map<IList<UnclearedPaymentAllocationDto>>(unclearedVpOutboundVisitTransactions);

            return unclearedPaymentAllocationDtos;
        }

        public IList<UnclearedPaymentAllocationDto> GetOutboundVisitTransactionsById(IList<int> vpOutboundVisitTransactionIds)
        {
            IList<VpOutboundVisitTransaction> vpOutboundVisitTransactions = this._outboundVisitTransactionRepository.Value.GetUnclearedOutboundVisitTransactions();

            if (vpOutboundVisitTransactionIds.IsNotNullOrEmpty())
            {
                vpOutboundVisitTransactions = vpOutboundVisitTransactions.Where(x => vpOutboundVisitTransactionIds.Contains(x.VpOutboundVisitTransactionId)).ToList();
            }
            
            IList<UnclearedPaymentAllocationDto> unclearedPaymentAllocationDtos = Mapper.Map<IList<UnclearedPaymentAllocationDto>>(vpOutboundVisitTransactions);

            return unclearedPaymentAllocationDtos;
        }

        public void ImportLockboxPaymentsFromFile(string filePath)
        {
            LockboxFile lockboxFile = LockboxFile.GetLockboxFile(filePath);
            PaymentImportFileMessage paymentImportFileMessage = Mapper.Map<PaymentImportFileMessage>(lockboxFile);
            this.Bus.Value.PublishMessage(paymentImportFileMessage).Wait();
        }
    }
}
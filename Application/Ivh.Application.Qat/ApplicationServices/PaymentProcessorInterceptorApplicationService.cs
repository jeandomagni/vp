﻿namespace Ivh.Application.Qat.ApplicationServices
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using System.Web;
    using Common.Dtos;
    using Common.Enums;
    using Common.Interfaces;
    using Domain.Qat.Entities;
    using Domain.Qat.Interfaces;
    using Ivh.Common.Base.Enums;
    using Ivh.Common.VisitPay.Enums;

    public class PaymentProcessorInterceptorApplicationService : IPaymentProcessorInterceptorApplicationService
    {
        private readonly Lazy<IPaymentProcessorInterceptorService> _paymentProcessorInterceptorService;

        public PaymentProcessorInterceptorApplicationService(
            Lazy<IPaymentProcessorInterceptorService> paymentProcessorInterceptorService)
        {
            this._paymentProcessorInterceptorService = paymentProcessorInterceptorService;
        }

        public bool DefaultSuccessful()
        {
            return this._paymentProcessorInterceptorService.Value.DefaultSuccessful;
        }

        public void SetDefaultSuccessful(bool b)
        {
            this._paymentProcessorInterceptorService.Value.DefaultSuccessful = b;
        }

        public int DelayMs()
        {
            return this._paymentProcessorInterceptorService.Value.DelayMs;
        }

        public void SetDelayMs(int delayMs)
        {
            this._paymentProcessorInterceptorService.Value.DelayMs = delayMs;
        }

        public bool IsActive()
        {
            return this._paymentProcessorInterceptorService.Value.IsActive();
        }

        public void Activate(string uriString)
        {
            this._paymentProcessorInterceptorService.Value.Activate(uriString);
        }

        public void Deactivate()
        {
            this._paymentProcessorInterceptorService.Value.Deactivate();
        }

        public async Task<string> GetResponseAsync(IDictionary<string, string> request)
        {
            Guid requestGuid = Guid.NewGuid();

            PpiResponse response = await this._paymentProcessorInterceptorService.Value.QueuePaymentRequestAsync(requestGuid, request).ConfigureAwait(false);

            if (response == null)
            {
                throw new HttpException(501, "no response");
            }

            if (response.ResponseCode == "404" || response.ResponseCode == "500")
            {
                throw new HttpException(Convert.ToInt32(response.ResponseCode), "ProcessorUnreachable");
            }

            return response.ToString();
        }

        public PpiRequestDto GetPaymentRequest(Guid requestGuid)
        {
            PpiRequest request = this._paymentProcessorInterceptorService.Value.GetPaymentRequest(requestGuid);

            return this.MapRequest(request);
        }

        public void CompletePaymentRequest(Guid requestGuid, PpiResponseTypeEnum responseType, char? responseAvsCode)
        {
            this._paymentProcessorInterceptorService.Value.CompletePaymentRequest(requestGuid, responseType, responseAvsCode);
        }

        public PpiRequestDto MapRequest(PpiRequest request)
        {
            if (request == null)
            {
                return null;
            }

            return new PpiRequestDto(request.ResponseType, request.ResponseAvsCode)
            {
                Action = request.Action,
                AchFileType = request.AchFileType,
                AchProcessDate = request.AchProcessDate,
                AchProcessed = request.AchProcessed,
                AchReturnAmount = request.AchReturnAmount,
                AchReturnCode = request.AchReturnCode,
                Amount = request.Amount,
                BillingId = request.BillingId,
                CustomField12 = request.CustomField12,
                CustomField2 = request.CustomField2,
                CustomField3 = request.CustomField3,
                CustomField4 = request.CustomField4,
                CustomField5 = request.CustomField5,
                CustomField6 = request.CustomField6,
                CustomField9 = request.CustomField9,
                RequestGuid = request.RequestGuid,
                VpPaymentMethodType = request.VpPaymentMethodType,
                VpAccountName = request.VpAccountName,
                VpLastFour = request.VpLastFour,
                ResponseTypes = request.ResponseTypes,
                IsPending = request.IsPending,
                IsSuccess = request.IsSuccess,
                IsFail = request.IsFail,
                AcceptedAvsCodes = request.AcceptedAvsCodes
            };
        }

        public PpiRequestDto SetAchAction(Guid requestGuid, AchFileTypeEnum fileType, DateTime processDate, AchReturnCodeEnum? returnCode, decimal? returnAmount)
        {
            PpiRequest request = this._paymentProcessorInterceptorService.Value.SetAchAction(requestGuid, fileType, processDate, returnCode, returnAmount);

            return this.MapRequest(request);
        }

        public IList<PpiRequestDto> QueryRequestQueue(PpiRequestFilterDto ppiRequestFilter)
        {
            return this._paymentProcessorInterceptorService.Value.QueryRequestQueue(ppiRequestFilter).Select(x => MapRequest(x)).ToList();
        }
    }
}

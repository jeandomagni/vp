﻿namespace Ivh.Application.Qat.ApplicationServices
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Base.Common.Interfaces;
    using Common.Dtos;
    using Common.Interfaces;
    using Core.Common.Utilities.Builders;
    using Domain.FinanceManagement.FinancePlan.Interfaces;
    using Domain.FinanceManagement.Statement.Entities;
    using Domain.FinanceManagement.Statement.Interfaces;
    using Domain.Guarantor.Entities;
    using Domain.Guarantor.Interfaces;
    using Domain.SecureCommunication.GuarantorSupportRequests.Interfaces;
    using Domain.Visit.Interfaces;
    using FinanceManagement.Common.Dtos;
    using FinanceManagement.Common.Interfaces;
    using Ivh.Common.Base.Constants;
    using Ivh.Common.Base.Enums;
    using Ivh.Common.Base.Utilities.Extensions;
    using Ivh.Common.Base.Utilities.Helpers;
    using Ivh.Common.VisitPay.Constants;
    using Ivh.Common.VisitPay.Enums;
    using OfficeOpenXml;
    using OfficeOpenXml.Style;

    public class PersonasApplicationService : IPersonasApplicationService
    {
        private readonly Lazy<IFinancePlanService> _financePlanService;
        private readonly Lazy<IGuarantorService> _guarantorService;
        private readonly Lazy<IVpStatementService> _statementService;
        private readonly Lazy<ISupportRequestService> _supportRequestService;
        private readonly Lazy<IVisitService> _visitService;
        private readonly Lazy<IPaymentDetailApplicationService> _paymentDetailApplicationService;
        private readonly Lazy<IVisitBalanceBaseApplicationService> _visitBalanceBaseApplicationService;

        public PersonasApplicationService(
            Lazy<IFinancePlanService> financePlanService,
            Lazy<IGuarantorService> guarantorService,
            Lazy<IVpStatementService> statementService,
            Lazy<ISupportRequestService> supportRequestService,
            Lazy<IVisitService> visitService,
            Lazy<IPaymentDetailApplicationService> paymentDetailApplicationService,
            Lazy<IVisitBalanceBaseApplicationService> visitBalanceBaseApplicationService
        )
        {
            this._financePlanService = financePlanService;
            this._guarantorService = guarantorService;
            this._statementService = statementService;
            this._supportRequestService = supportRequestService;
            this._visitService = visitService;
            this._paymentDetailApplicationService = paymentDetailApplicationService;
            this._visitBalanceBaseApplicationService = visitBalanceBaseApplicationService;
        }
        
        [Obsolete(ObsoleteDescription.VisitTransactions)]
        public byte[] ExportPersonas(string search)
        {
            IList<Guarantor> allGuarantors = this._guarantorService.Value.GetGuarantors(new GuarantorFilter(), 1, int.MaxValue);
            IList<Guarantor> exportGuarantors = null;

            if (string.IsNullOrWhiteSpace(search))
            {
                exportGuarantors = allGuarantors;
            }
            else
            {
                string[] searchForLastNames = search.Split(',');
                if (searchForLastNames.Any())
                {
                    exportGuarantors = new List<Guarantor>();
                    foreach (string lastName in searchForLastNames.Select(x => x.TrimNullSafe()))
                    {
                        List<Guarantor> foundGuarantors = allGuarantors.Where(x => x.User.LastName.ToUpper().Contains(lastName.ToUpper())).ToList();
                        if (foundGuarantors.Any())
                        {
                            exportGuarantors.AddRange(foundGuarantors);
                        }
                    }

                    exportGuarantors = exportGuarantors.GroupBy(x => x.VpGuarantorId).Select(x => x.First()).ToList();
                }
            }

            if (exportGuarantors == null || !exportGuarantors.Any())
            {
                return null;
            }

            IList<ExportPersonasDto> exportPersonasDtos = this.GetExportData(exportGuarantors);
            if (!exportPersonasDtos.Any())
            {
                return null;
            }

            using (ExcelPackage package = new ExcelPackage())
            {
                iVinciExcelExportBuilder builder = new iVinciExcelExportBuilder(package, "Personas").AddDataBlock(exportPersonasDtos, dto =>
                {
                    string statementCycle = dto.IsGracePeriod ? "Grace Period" : "Awaiting Statement";
                    string statementStatus = dto.HasPastDueVisits ? "Past Due" : "Good Standing";

                    string consolidation = string.Empty;
                    if (!string.IsNullOrWhiteSpace(dto.ManagingUser))
                    {
                        consolidation = $"Managed by {dto.ManagingUser}";
                    }
                    else if (dto.ManagedUsers.Any())
                    {
                        consolidation = string.Join(Environment.NewLine, dto.ManagedUsers.Select(x => $"Managing {x}"));
                    }

                    IList<string> payments = new List<string>();

                    if (dto.RecurringPaymentsPendingCount > 0)
                    {
                        payments.Add($"{dto.RecurringPaymentsPendingCount} Recurring Pending");
                    }
                    if (dto.ManualPaymentsPendingCount > 0)
                    {
                        payments.Add($"{dto.ManualPaymentsPendingCount} Manual Pending");
                    }
                    if (dto.RecurringPaymentsSuccessfulCount > 0)
                    {
                        payments.Add($"{dto.RecurringPaymentsSuccessfulCount} Recurring Success");
                    }
                    if (dto.RecurringPaymentsFailedCount > 0)
                    {
                        payments.Add($"{dto.RecurringPaymentsFailedCount} Recurring Failed");
                    }
                    if (dto.ManualPaymentsSuccessfulCount > 0)
                    {
                        payments.Add($"{dto.ManualPaymentsSuccessfulCount} Manual Success");
                    }
                    if (dto.ManualPaymentsFailedCount > 0)
                    {
                        payments.Add($"{dto.ManualPaymentsFailedCount} Manual Failed");
                    }
                    
                    return new List<Column>
                    {
                        new ColumnBuilder($"{dto.FirstNameLastName}{Environment.NewLine}{dto.Username}", "Name/Username").AlignLeft(),
                        new ColumnBuilder(dto.VpGuarantorId, "VPGID #").AlignCenter(),
                        new ColumnBuilder(consolidation, "Consolidation Status").AlignCenter(),
                        new ColumnBuilder($"{statementCycle}/{statementStatus}", "Statement Cycle Status").AlignCenter(),
                        new ColumnBuilder(dto.DaysUntilPaymentDueDate, "Days Until Payment Due Date").AlignCenter(),
                        new ColumnBuilder(dto.StatementsCount, "# of Statements").AlignCenter(),
                        new ColumnBuilder(dto.PendingVisitBalanceSum, "Pending Visit Balance").WithCurrencyFormat().AlignCenter(),
                        new ColumnBuilder(dto.TotalBalance, "Current Active Visit Balance").WithCurrencyFormat().AlignCenter(),
                        new ColumnBuilder(dto.SuspendedVisitBalanceSum, "Suspended Visit Balance").WithCurrencyFormat().AlignCenter(),
                        new ColumnBuilder(dto.FinancePlanBalanceSum, "Finance Plan Balance").WithCurrencyFormat().AlignCenter(),
                        new ColumnBuilder(string.Join(Environment.NewLine, payments), "Payments"),
                        new ColumnBuilder(dto.SupportRequestsCount, "# Support Requests").AlignCenter(),
                    };
                });

                builder.Build();
                
                package.Workbook.Worksheets.First().Cells[1, 1, exportPersonasDtos.Count + 5, 12].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                package.Workbook.Worksheets.First().Cells[5, 1, exportPersonasDtos.Count + 5, 1].Style.WrapText = true;
                package.Workbook.Worksheets.First().Cells[5, 11, exportPersonasDtos.Count + 5, 11].Style.WrapText = true;

                return package.GetAsByteArray();
            }
        }

        [Obsolete(ObsoleteDescription.VisitTransactions + " " + " " + ObsoleteDescription.VisitTransactionVisitPayPayment)]
        private IList<ExportPersonasDto> GetExportData(IList<Guarantor> exportGuarantors)
        {
            VisitFilter suspendedVisitsFilter = new VisitFilter
            {
                BillingHold = true
            };

            IList<ExportPersonasDto> exportPersonasDtos = new List<ExportPersonasDto>();

            foreach (Guarantor guarantor in exportGuarantors.OrderBy(x => x.User.LastName))
            {
                decimal totalBalance = this._visitBalanceBaseApplicationService.Value.GetTotalBalance(guarantor.VpGuarantorId);

                ExportPersonasDto exportPersonasDto = new ExportPersonasDto();
                
                /* balance */
                exportPersonasDto.TotalBalance = totalBalance;

                /* user */
                exportPersonasDto.VpGuarantorId = guarantor.VpGuarantorId;
                exportPersonasDto.FirstNameLastName = guarantor.User.FirstNameLastName;
                exportPersonasDto.Username = guarantor.User.UserName;

                /* consolidation */
                if (guarantor.ConsolidationStatus == GuarantorConsolidationStatusEnum.Managed)
                {
                    exportPersonasDto.ManagingUser = guarantor.ManagingConsolidationGuarantors.Where(x => x.IsActive()).Select(x => x.ManagingGuarantor.User.FirstNameLastName).FirstOrDefault();
                }
                else if (guarantor.ConsolidationStatus == GuarantorConsolidationStatusEnum.Managing)
                {
                    foreach (string userFirstLast in guarantor.ManagedConsolidationGuarantors.Where(x => x.IsActive()).OrderBy(x => x.ManagedGuarantor.User.FirstNameLastName).Select(x => x.ManagedGuarantor.User.FirstNameLastName))
                    {
                        exportPersonasDto.ManagedUsers.Add(userFirstLast);
                    }
                }

                /* statement */
                VpStatement statement = this._statementService.Value.GetMostRecentStatement(guarantor);
                if (statement != null)
                {
                    exportPersonasDto.StatementsCount = this._statementService.Value.GetStatementTotals(guarantor.VpGuarantorId);
                    exportPersonasDto.DaysUntilPaymentDueDate = statement.PaymentDueDate.Date.Subtract(DateTime.UtcNow.Date).Days;
                    exportPersonasDto.HasPastDueVisits = this._visitService.Value.GetVisitTotals(guarantor.VpGuarantorId, new VisitFilter{IsPastDue = true}) > 0; // ? "Past Due" : "Good Standing";
                    exportPersonasDto.IsGracePeriod = statement.IsGracePeriod; // ? "Grace Period" : "Awaiting Statement";
                }

                /* visits */
                IList<Ivh.Domain.Visit.Entities.Visit> suspendedVisits = this._visitService.Value.GetVisits(guarantor.VpGuarantorId, suspendedVisitsFilter, null, null).Visits;
                
                exportPersonasDto.PendingVisitBalanceSum = 0;
                exportPersonasDto.SuspendedVisitBalanceSum = suspendedVisits.Sum(x => x.CurrentBalance);

                /* finance plan */
                exportPersonasDto.FinancePlanBalanceSum = this._financePlanService.Value.GetAllOpenFinancePlansForGuarantor(guarantor).Sum(x => x.CurrentFinancePlanBalance);

                /* pending payments */
                //PendingPaymentsResult pendingPayments = this._paymentDetailApplicationService.Value.GetPendingPayments(guarantor.VpGuarantorId, guarantor.VpGuarantorId, string.Empty, string.Empty, 1, int.MaxValue);
                //exportPersonasDto.RecurringPaymentsPendingCount = pendingPayments.AggregatedScheduledPaymentResponses.Count(x => x.PaymentType == PaymentTypeEnum.RecurringPaymentFinancePlan);
                //exportPersonasDto.ManualPaymentsPendingCount = pendingPayments.AggregatedScheduledPaymentResponses.Count - exportPersonasDto.RecurringPaymentsPendingCount;

                /* payment history */
                // todo: fix
                PaymentHistoryResultsDto paymentHistory = this._paymentDetailApplicationService.Value.GetPaymentHistory(guarantor.VpGuarantorId, 0, new PaymentProcessorResponseFilterDto(), 1, int.MaxValue);

                List<PaymentHistoryDto> recurringHistory = paymentHistory.Payments.Where(x => x.PaymentType == PaymentTypeEnum.RecurringPaymentFinancePlan).ToList();
                exportPersonasDto.RecurringPaymentsSuccessfulCount = recurringHistory.Count(x => EnumHelper<PaymentStatusEnum>.Contains(x.PaymentStatus, PaymentStatusEnumCategory.Paid));
                exportPersonasDto.RecurringPaymentsFailedCount = recurringHistory.Count(x => x.PaymentStatus == PaymentStatusEnum.ClosedFailed);

                List<PaymentHistoryDto> manualHistory = paymentHistory.Payments.Where(x => x.PaymentType != PaymentTypeEnum.RecurringPaymentFinancePlan).ToList();
                exportPersonasDto.ManualPaymentsSuccessfulCount = manualHistory.Count(x => EnumHelper<PaymentStatusEnum>.Contains(x.PaymentStatus, PaymentStatusEnumCategory.Paid));
                exportPersonasDto.ManualPaymentsFailedCount = manualHistory.Count(x => x.PaymentStatus == PaymentStatusEnum.ClosedFailed);

                /* support requests */
                exportPersonasDto.SupportRequestsCount = this._supportRequestService.Value.GetSupportRequestTotals(guarantor, new SupportRequestFilter());

                /**/
                exportPersonasDtos.Add(exportPersonasDto);
            }

            return exportPersonasDtos;
        }
    }
}
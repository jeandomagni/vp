﻿namespace Ivh.Application.Qat.ApplicationServices
{
    using System;
    using Common.Interfaces;
    using Domain.Qat.Interfaces;

    public class AchInterceptorApplicationService : IAchInterceptorApplicationService
    {
        private readonly Lazy<IAchInterceptorService> _achInterceptorService;

        public AchInterceptorApplicationService(Lazy<IAchInterceptorService> achInterceptorService)
        {
            this._achInterceptorService = achInterceptorService;
        }

        public void Activate(string uriString)
        {
            this._achInterceptorService.Value.Activate(uriString);
        }

        public void Deactivate()
        {
            this._achInterceptorService.Value.Deactivate();
        }

        public string GetReturnFile(DateTime startDate, DateTime endDate, string fileFormat)
        {
            return this._achInterceptorService.Value.GetReturnFile(startDate, endDate, fileFormat);
        }

        public string GetReport(string reportTypeId, DateTime startDate, DateTime endDate, string dateTypeId, string exportFormat)
        {
            return this._achInterceptorService.Value.GetReport(reportTypeId, startDate, endDate, dateTypeId, exportFormat);
        }
    }
}

﻿namespace Ivh.Application.Qat.ApplicationServices
{
    using System;
    using System.Threading.Tasks;
    using Common.Interfaces;
    using Domain.Qat.Interfaces;
    using Domain.Settings.Interfaces;

    public class QaSettingsApplicationService : IQaSettingsApplicationService
    {
        private readonly Lazy<IQaSettingsService> _qaSettingsService;
        private readonly Lazy<ISettingsService> _settingsService;

        public QaSettingsApplicationService(
            Lazy<IQaSettingsService> qaSettingsService,
            Lazy<ISettingsService> settingsService)
        {
            this._qaSettingsService = qaSettingsService;
            this._settingsService = settingsService;
        }
        
        public async Task<bool> ChangeClientLogo(string filename, byte[] logo)
        {
            if (this._qaSettingsService.Value.ChangeClientLogo(filename, logo))
            {
                await this._settingsService.Value.InvalidateCacheAsync();
                return true;
            }

            return false;
        }
    }
}
﻿namespace Ivh.Application.Qat.ApplicationServices
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Base.Common.Interfaces;
    using Base.Services;
    using Common.Interfaces;
    using Domain.FinanceManagement.Payment.Entities;
    using Domain.FinanceManagement.Payment.Interfaces;
    using Domain.FinanceManagement.ServiceGroup.Entities;
    using Domain.FinanceManagement.ServiceGroup.Interfaces;

    public class QaMerchantAccountApplicationService : ApplicationService, IQaMerchantAccountApplicationService
    {
        private readonly Lazy<IServiceGroupMerchantAccountRepository> _serviceGroupMerchantAccountRepository;

        public QaMerchantAccountApplicationService(
            Lazy<IApplicationServiceCommonService> applicationServiceCommonService,
            Lazy<IServiceGroupMerchantAccountRepository> serviceGroupMerchantAccountRepository
        ) : base(applicationServiceCommonService)
        {
            this._serviceGroupMerchantAccountRepository = serviceGroupMerchantAccountRepository;
        }

        public IList<int> GetServiceGroupIds()
        {
            List<ServiceGroupMerchantAccount> serviceGroupMerchantAccounts = this._serviceGroupMerchantAccountRepository.Value.GetQueryable().ToList();

            return serviceGroupMerchantAccounts.Where(x => x.ServiceGroupId != null).Select(x => x.ServiceGroupId.Value).ToList();
        }
    }
}
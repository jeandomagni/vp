﻿namespace Ivh.Application.Qat.ApplicationServices
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Common.Interfaces;
    using Domain.FinanceManagement.Consolidation.Interfaces;
    using Domain.Guarantor.Entities;
    using Domain.Guarantor.Interfaces;
    using Domain.Powershell.Interfaces;
    using Domain.Qat.Entities;
    using Domain.Qat.Interfaces;
    using Domain.User.Entities;
    using Domain.User.Interfaces;
    using Ivh.Common.VisitPay.Enums;
    using Ivh.Common.Web.Models;

    public class DemoVisitPayUserApplicationService : IDemoVisitPayUserApplicationService
    {
        private readonly Lazy<IDemoVisitPayUserService> _demoVisitPayUserService;
        private readonly Lazy<IConsolidationGuarantorRepository> _consolidationGuarantorRepository;
        private readonly Lazy<IGuarantorService> _guarantorService;
        private readonly Lazy<IPowershellService> _powershellService;
        private readonly Lazy<IVisitPayUserService> _visitPayUserService;

        public DemoVisitPayUserApplicationService(
            Lazy<IDemoVisitPayUserService> demoVisitPayUserService,
            Lazy<IConsolidationGuarantorRepository> consolidationGuarantorRepository,
            Lazy<IGuarantorService> guarantorService,
            Lazy<IPowershellService> powershellService,
            Lazy<IVisitPayUserService> visitPayUserService)
        {
            this._demoVisitPayUserService = demoVisitPayUserService;
            this._consolidationGuarantorRepository = consolidationGuarantorRepository;
            this._guarantorService = guarantorService;
            this._powershellService = powershellService;
            this._visitPayUserService = visitPayUserService;
        }

        public async Task<ResultMessage<string>> ResetDemoVisitPayUser(string resetUsername, string resetUserPassword)
        {
            // the user you're trying to reset
            VisitPayUser resetVisitPayUser = this._demoVisitPayUserService.Value.GetVisitPayUserFromUserName(resetUsername);

            // reset - this will change the username of the user you're trying to reset, clone the user you're trying to reset, then rename the clone to resetUsername
            // it will appear as if the account has just been reset
            ResultMessage<string> result = await this.ResetDemoVisitPayUser(resetVisitPayUser, true, resetUserPassword);
            if (!result.Result)
            {
                return result;
            }

            // get the new reset user
            VisitPayUser newVisitPayUser = this._demoVisitPayUserService.Value.GetVisitPayUserFromUserName(resetUsername);
            Guarantor newGuarantor = this._guarantorService.Value.GetGuarantorEx(newVisitPayUser.VisitPayUserId);

            // get consolidation from original user
            IList<ConsolidationGuarantor> managedGuarantors = this.GetConsolidationGuarantors(resetVisitPayUser.VisitPayUserId);
            if (managedGuarantors.Count > 0)
            {
                foreach (ConsolidationGuarantor consolidationGuarantor in managedGuarantors)
                {
                    // clone/reset each consolidated user
                    string consolidatedUserName = consolidationGuarantor.ManagedGuarantor.User.UserName;
                    result = await this.ResetDemoVisitPayUser(consolidationGuarantor.ManagedGuarantor.User, false, null);
                    if (!result.Result)
                    {
                        continue;
                    }

                    VisitPayUser newManagedVisitPayUser = this._demoVisitPayUserService.Value.GetVisitPayUserFromUserName(consolidatedUserName);
                    Guarantor newManagedGuarantor = this._guarantorService.Value.GetGuarantorEx(newManagedVisitPayUser.VisitPayUserId);

                    // insert the consolidation record
                    ConsolidationGuarantor newConsolidationGuarantor = new ConsolidationGuarantor
                    {
                        InitiatedByUser = consolidationGuarantor.InitiatedByUser.VisitPayUserId == resetVisitPayUser.VisitPayUserId ? newVisitPayUser : newManagedVisitPayUser,
                        ManagedGuarantor = newManagedGuarantor,
                        ManagingGuarantor = newGuarantor,
                        ConsolidationGuarantorStatus = consolidationGuarantor.ConsolidationGuarantorStatus,
                        InsertDate = consolidationGuarantor.InsertDate,
                        AcceptedByManagedGuarantorCmsVersion = consolidationGuarantor.AcceptedByManagedGuarantorCmsVersion,
                        AcceptedByManagingGuarantorCmsVersion = consolidationGuarantor.AcceptedByManagingGuarantorCmsVersion,
                        AcceptedByManagingGuarantorFpCmsVersion = consolidationGuarantor.AcceptedByManagingGuarantorFpCmsVersion,
                        DateAcceptedByManagedGuarantor = consolidationGuarantor.DateAcceptedByManagedGuarantor,
                        DateAcceptedByManagingGuarantor = consolidationGuarantor.DateAcceptedByManagedGuarantor,
                        DateAcceptedFinancePlanTermsByManagingGuarantor = consolidationGuarantor.DateAcceptedFinancePlanTermsByManagingGuarantor,
                        CancelledByUser = consolidationGuarantor.CancelledByUser,
                        CancelledOn = consolidationGuarantor.CancelledOn,
                        ConsolidationGuarantorCancellationReason = consolidationGuarantor.ConsolidationGuarantorCancellationReason,
                        CancelledByManagedGuarantorCmsVersion = consolidationGuarantor.CancelledByManagedGuarantorCmsVersion,
                        CancelledByManagingGuarantorCmsVersion = consolidationGuarantor.CancelledByManagingGuarantorCmsVersion,
                        CancelledByManagedGuarantorFpCmsVersion = consolidationGuarantor.CancelledByManagedGuarantorFpCmsVersion
                    };

                    this._consolidationGuarantorRepository.Value.Insert(newConsolidationGuarantor);
                }
            }

            // success
            return new ResultMessage<string>(true, $"Reset complete! In the application, logout {resetUsername} for changes to take affect");
        }

        public List<string> GetResetableUsers()
        {
            return this._demoVisitPayUserService.Value.GetAllDemoVisitPayUserClones().Select(x => x.CloneVisitPayUserName).ToList();
        }

        private async Task<ResultMessage<string>> ResetDemoVisitPayUser(VisitPayUser resetVisitPayUser, bool requirePassword, string resetUserPassword)
        {
            if (resetVisitPayUser == null)
            {
                return new ResultMessage<string>(false, "User not found.");
            }

            if (requirePassword)
            {
                bool validPassword = await this._visitPayUserService.Value.VerifyPasswordAsync(resetVisitPayUser, resetUserPassword);
                if (!validPassword)
                {
                    return new ResultMessage<string>(false, $"The password does not match for {resetVisitPayUser.UserName}");
                }
            }

            DemoVisitPayUserClone demoVisitPayUserClone = this._demoVisitPayUserService.Value.GetDemoVisitPayUserClone(resetVisitPayUser.UserName);
            if (demoVisitPayUserClone == null)
            {
                return new ResultMessage<string>(false, "User cannot be reset");
            }

            int sourceHsGuarantorId = demoVisitPayUserClone.DemoVisitPayUserSource.SourceHsGuarantorId;
            int sourceVpGuarantorId = demoVisitPayUserClone.DemoVisitPayUserSource.SourceVpGuarantorId;

            string resetUsername = resetVisitPayUser.UserName;
            string resetPasswordHash = resetVisitPayUser.PasswordHash;
            string newUsername = Guid.NewGuid().ToString();

            // rename the orignal user
            this._demoVisitPayUserService.Value.RenameVisitPayUser(resetUsername, newUsername);

            // clone, and set username of clone to the username
            this._powershellService.Value.CloneCdiAppGuarantor(sourceHsGuarantorId, sourceVpGuarantorId, "", resetUsername, false);

            // update password to match the password of the original user
            this._demoVisitPayUserService.Value.PostClone(resetUsername, resetUsername, string.Empty);

            //
            return new ResultMessage<string>(true, string.Empty);
        }

        private IList<ConsolidationGuarantor> GetConsolidationGuarantors(int resetVisitPayUserId)
        {
            Guarantor resetGuarantor = this._guarantorService.Value.GetGuarantorEx(resetVisitPayUserId);
            return resetGuarantor.ConsolidationStatus == GuarantorConsolidationStatusEnum.Managing ? resetGuarantor.ManagedConsolidationGuarantors : new List<ConsolidationGuarantor>();
        }
    }
}
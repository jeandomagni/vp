﻿namespace Ivh.Application.Qat.ApplicationServices
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Base.Common.Interfaces;
    using Base.Services;
    using Common.Enums;
    using Common.Interfaces;
    using Domain.FinanceManagement.Consolidation.Interfaces;
    using Domain.Guarantor.Entities;
    using Domain.HospitalData.HsGuarantor.Interfaces;
    using Domain.HospitalData.VpGuarantor.Entities;
    using Domain.Powershell.Interfaces;
    using Domain.Qat.Interfaces;
    using Domain.User.Services;
    using Ivh.Common.VisitPay.Messages.HospitalData;

    public class CloneGuarantorApplicationService : ApplicationService, ICloneGuarantorApplicationService
    {
        private readonly Lazy<IConsolidationGuarantorRepository> _consolidationGuarantorRepository;
        private readonly Lazy<IPowershellService> _powershellService;
        private readonly Lazy<VisitPayUserService> _visitPayUserService;
        private readonly Lazy<IDemoVisitPayUserService> _demoVisitPayUserService;
        private readonly Lazy<IVpGuarantorHsMatchRepository> _vpGuarantorHsMatchRepository;

        public CloneGuarantorApplicationService(
            Lazy<IApplicationServiceCommonService> applicationServiceCommonService,
            Lazy<IPowershellService> powershellService,
            Lazy<VisitPayUserService> visitPayUserService,
            Lazy<IDemoVisitPayUserService> demoVisitPayUserService,
            Lazy<IConsolidationGuarantorRepository> consolidationGuarantorRepository,
            Lazy<IVpGuarantorHsMatchRepository> vpGuarantorHsMatchRepository) : base(applicationServiceCommonService)
        {
            this._consolidationGuarantorRepository = consolidationGuarantorRepository;
            this._powershellService = powershellService;
            this._visitPayUserService = visitPayUserService;
            this._demoVisitPayUserService = demoVisitPayUserService;
            this._vpGuarantorHsMatchRepository = vpGuarantorHsMatchRepository;
        }

        public void CloneGuarantor(int vpGuarantorId, string appendValue, string userName, bool consolidate)
        {
            IList<VpGuarantorHsMatch> matches = this._vpGuarantorHsMatchRepository.Value.GetHsGuarantorVpMatches(vpGuarantorId);
            foreach (VpGuarantorHsMatch match in matches)
            {
                this.Bus.Value.PublishMessage(new CloneCdiAppGuarantorMessage
                {
                    HsGuarantorId = match.HsGuarantorId,
                    VpGuarantorId = vpGuarantorId,
                    AppendValue = appendValue,
                    UserName = userName,
                    Consolidate = consolidate
                }).Wait();
            }
        }

        public CloneGuarantorResultEnum CloneGuarantorSynchronous(string sourceVisitPayUserName, string appendValue)
        {
            Guarantor sourceGuarantor = this._demoVisitPayUserService.Value.GetGuarantorFromUser(sourceVisitPayUserName);
            if (sourceGuarantor == null)
            {
                return CloneGuarantorResultEnum.MissingUser;
            }
            
            // clone
            string username = this.Clone(sourceGuarantor, appendValue);

            // clone managed
            if (sourceGuarantor.ManagedConsolidationGuarantors?.Any() ?? false)
            {
                foreach (ConsolidationGuarantor consolidationGuarantor in sourceGuarantor.ManagedConsolidationGuarantors)
                {
                    string managedUsername = this.Clone(consolidationGuarantor.ManagedGuarantor, appendValue);
                    this.CloneConsolidate(username, managedUsername, consolidationGuarantor);
                }
            }

            return CloneGuarantorResultEnum.Complete;
        }

        private void CloneConsolidate(string newManagingUsername, string newManagedUsername, ConsolidationGuarantor consolidationGuarantor)
        {
            Guarantor newManagingGuarantor = this._demoVisitPayUserService.Value.GetGuarantorFromUser(newManagingUsername);
            Guarantor newManagedGuarantor = this._demoVisitPayUserService.Value.GetGuarantorFromUser(newManagedUsername);

            if (newManagingGuarantor == null || newManagedGuarantor == null)
            {
                throw new Exception("could not find guarantor to consolidate");
            }
            
            ConsolidationGuarantor newConsolidationGuarantor = new ConsolidationGuarantor
            {
                // these need to be updated
                ManagedGuarantor = newManagedGuarantor,
                ManagingGuarantor = newManagingGuarantor,
                InitiatedByUser = consolidationGuarantor.InitiatedByUser.VisitPayUserId == consolidationGuarantor.ManagedGuarantor.User.VisitPayUserId ? newManagedGuarantor.User : newManagingGuarantor.User,

                // these can copy directly
                ConsolidationGuarantorStatus = consolidationGuarantor.ConsolidationGuarantorStatus,
                InsertDate = consolidationGuarantor.InsertDate,
                AcceptedByManagedGuarantorCmsVersion = consolidationGuarantor.AcceptedByManagedGuarantorCmsVersion,
                AcceptedByManagingGuarantorCmsVersion = consolidationGuarantor.AcceptedByManagingGuarantorCmsVersion,
                AcceptedByManagingGuarantorFpCmsVersion = consolidationGuarantor.AcceptedByManagingGuarantorFpCmsVersion,
                DateAcceptedByManagedGuarantor = consolidationGuarantor.DateAcceptedByManagedGuarantor,
                DateAcceptedByManagingGuarantor = consolidationGuarantor.DateAcceptedByManagedGuarantor,
                DateAcceptedFinancePlanTermsByManagingGuarantor = consolidationGuarantor.DateAcceptedFinancePlanTermsByManagingGuarantor,
                CancelledOn = consolidationGuarantor.CancelledOn,
                ConsolidationGuarantorCancellationReason = consolidationGuarantor.ConsolidationGuarantorCancellationReason,
                CancelledByManagedGuarantorCmsVersion = consolidationGuarantor.CancelledByManagedGuarantorCmsVersion,
                CancelledByManagingGuarantorCmsVersion = consolidationGuarantor.CancelledByManagingGuarantorCmsVersion,
                CancelledByManagedGuarantorFpCmsVersion = consolidationGuarantor.CancelledByManagedGuarantorFpCmsVersion
            };

            if (newConsolidationGuarantor.CancelledOn.HasValue)
            {
                newConsolidationGuarantor.CancelledByUser = consolidationGuarantor.CancelledByUser.VisitPayUserId == consolidationGuarantor.ManagedGuarantor.User.VisitPayUserId ? newManagedGuarantor.User : newManagingGuarantor.User;
            }
            
            this._consolidationGuarantorRepository.Value.Insert(newConsolidationGuarantor);
        }

        private string Clone(Guarantor sourceGuarantor, string suffix)
        {
            string newUserName = $"{sourceGuarantor.User.UserName}{suffix}";

            // clear and rename any existing user before cloing
            this._demoVisitPayUserService.Value.ClearAndRename(newUserName);

            // clone
            foreach (int hsGuarantorId in sourceGuarantor.HsGuarantorMaps.GroupBy(x => x.HsGuarantorId).Select(x => x.Key))
            {
                this._powershellService.Value.CloneCdiAppGuarantor(hsGuarantorId, sourceGuarantor.VpGuarantorId, "", newUserName, false);
            }

            // post clone
            this._demoVisitPayUserService.Value.PostClone(sourceGuarantor.User.UserName, newUserName, suffix);

            return newUserName;
        }
    }
}
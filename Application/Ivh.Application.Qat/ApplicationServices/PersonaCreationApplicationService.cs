﻿namespace Ivh.Application.Qat.ApplicationServices
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using AppIntelligence.Common.Interfaces;
    using Base.Common.Interfaces;
    using Base.Services;
    using Common;
    using Common.Dtos;
    using Common.Interfaces;
    using Content.Common.Interfaces;
    using Core.Common.Interfaces;
    using DataBuilders;
    using Domain.FinanceManagement.Statement.Interfaces;
    using Domain.Guarantor.Interfaces;
    using Domain.HospitalData.Eob.Interfaces;
    using Domain.HospitalData.Visit.Interfaces;
    using Domain.Qat.Interfaces;
    using Domain.Qat.Services;
    using FinanceManagement.Common.Enums;
    using FinanceManagement.Common.Interfaces;
    using HospitalData.Common.Interfaces;
    using Ivh.Common.Base.Utilities.Extensions;
    using Ivh.Common.Base.Utilities.Helpers;
    using Ivh.Common.Data.Connection.Connections;
    using Ivh.Common.Data.Interfaces;
    using Ivh.Common.VisitPay.Constants;
    using Ivh.Common.VisitPay.Enums;
    using Ivh.Common.VisitPay.Messages.HospitalData.Dtos;
    using Magnum.Extensions;
    using Matching.Common.Interfaces;
    using SecureCommunication.Common.Interfaces;
    using IRedactionApplicationService = Core.Common.Interfaces.IRedactionApplicationService;

    public class PersonaCreationApplicationService : ApplicationService, IPersonaCreationApplicationService
    {
        private const string DefaultPassword = "QA.password1";

        private readonly Lazy<IChangeEventApplicationService> _changeEventApplicationService;
        private readonly Lazy<HospitalData.Common.Interfaces.IChangeSetApplicationService> _changeSetApplicationService;
        private readonly Lazy<IContentApplicationService> _contentApplicationService;
        private readonly Lazy<IGuarantorService> _guarantorService;
        private readonly Lazy<IFinancePlanApplicationService> _financePlanApplicationService;
        private readonly Lazy<IManagedUserApplicationService> _managedUserApplicationService;
        private readonly Lazy<IManagingUserApplicationService> _managingUserApplicationService;
        private readonly Lazy<IPaymentMethodsApplicationService> _paymentMethodsApplicationService;
        private readonly Lazy<IHsGuarantorApplicationService> _hsGuarantorApplicationService;
        private readonly Lazy<IFeatureApplicationService> _featureApplicationService;
        private readonly Lazy<IPaymentSubmissionApplicationService> _paymentSubmissionApplicationService;
        private readonly Lazy<IRandomizedTestApplicationService> _randomizedTestApplicationService;
        private readonly Lazy<IScheduledPaymentApplicationService> _scheduledPaymentApplicationService;
        private readonly ISessionContext<VisitPay> _session;
        private readonly Lazy<IStatementApplicationService> _statementApplicationService;
        private readonly Lazy<IStatementCreationApplicationService> _statementCreationApplicationService;
        private readonly Lazy<ISupportRequestApplicationService> _supportRequestApplicationService;
        private readonly Lazy<ITestPaymentMethodsApplicationService> _testPaymentMethodsApplicationService;
        private readonly Lazy<IVisitPayUserApplicationService> _visitPayUserApplicationService;
        private readonly Lazy<IVisitPayUserIssueResultApplicationService> _visitPayUserIssueResultApplicationService;
        private readonly Lazy<IVisitApplicationService> _visitApplicationService;
        private readonly Lazy<IVisitRepository> _visitRepository;
        private readonly Lazy<Ivh.Domain.HospitalData.HsGuarantor.Interfaces.IHsGuarantorService> _hsGuarantorService;
        private readonly Lazy<IVisitService> _hsVisitService;
        private readonly Lazy<IVpStatementService> _vpStatementService;
        private readonly Lazy<IDemoVisitPayUserService> _demoVisitPayUserService;
        private readonly Lazy<IVpGuarantorKeepCurrentRepository> _vpGuarantorKeepCurrentRepository;
        private readonly Lazy<IHsGuarantorKeepCurrentRepository> _hsGuarantorKeepCurrentRepository;
        private readonly Lazy<IEob835Service> _eob835Service;
        private readonly Lazy<IEobClaimAdjustmentReasonCodeRepository> _remittanceCodeRepository;
        private readonly Lazy<IEobPayerFilterRepository> _eobPayerFilterRepository;
        private readonly Lazy<IQaAgingApplicationService> _qaAgingApplicationService;
        private readonly Lazy<IQaPaymentService> _qaPaymentService;
        private readonly Lazy<IIhcEnrollmentService> _ihcEnrollmentService;
        private readonly Lazy<IStatementExportApplicationService> _statementExportApplicationService;
        private readonly Lazy<IAppBaseDataService> _appBaseDataService;
        private readonly Lazy<IMatchingApplicationService> _matchingApplicationService;
        private readonly Lazy<IGuarantorApplicationService> _guarantorApplicationService;
        private readonly Lazy<IRedactionApplicationService> _redactionApplicationService;

        public PersonaCreationApplicationService(
            Lazy<IApplicationServiceCommonService> applicationServiceCommonService,
            Lazy<HospitalData.Common.Interfaces.IChangeSetApplicationService> changeSetApplicationService,
            Lazy<IChangeEventApplicationService> changeEventApplicationService,
            Lazy<IContentApplicationService> contentApplicationService,
            Lazy<IGuarantorService> guarantorService,
            Lazy<IHsGuarantorApplicationService> hsGuarantorApplicationService,
            Lazy<IFeatureApplicationService> featureApplicationService,
            Lazy<IFinancePlanApplicationService> financePlanApplicationService,
            Lazy<IManagedUserApplicationService> managedUserApplicationService,
            Lazy<IManagingUserApplicationService> managingUserApplicationService,
            Lazy<IPaymentMethodsApplicationService> paymentMethodsApplicationService,
            Lazy<IPaymentSubmissionApplicationService> paymentSubmissionApplicationService,
            Lazy<IRandomizedTestApplicationService> randomizedTestApplicationService,
            Lazy<IScheduledPaymentApplicationService> scheduledPaymentApplicationService,
            Lazy<IStatementApplicationService> statementApplicationService,
            Lazy<IStatementCreationApplicationService> statementCreationApplicationService,
            Lazy<ISupportRequestApplicationService> supportRequestApplicationService,
            Lazy<ITestPaymentMethodsApplicationService> testPaymentMethodsApplicationService,
            Lazy<IVisitPayUserApplicationService> visitPayUserApplicationService,
            Lazy<IVisitPayUserIssueResultApplicationService> visitPayUserIssueResultApplicationService,
            Lazy<IVisitApplicationService> visitApplicationService,
            Lazy<IVisitRepository> visitRepository,
            Lazy<Ivh.Domain.HospitalData.HsGuarantor.Interfaces.IHsGuarantorService> hsGuarantorService,
            Lazy<IVisitService> hsVisitService,
            Lazy<IVpStatementService> vpStatementService,
            Lazy<IDemoVisitPayUserService> demoVisitPayUserService,
            Lazy<IVpGuarantorKeepCurrentRepository> vpGuarantorKeepCurrentRepository,
            Lazy<IHsGuarantorKeepCurrentRepository> hsGuarantorKeepCurrentRepository,
            Lazy<IEob835Service> eob835Service,
            Lazy<IEobClaimAdjustmentReasonCodeRepository> remittanceCodeRepository,
            Lazy<IEobPayerFilterRepository> eobPayerFilterRepository,
            Lazy<IQaAgingApplicationService> qaAgingApplicationService,
            Lazy<IQaPaymentService> qaPaymentService,
            Lazy<IIhcEnrollmentService> ihcEnrollmentService,
            Lazy<IStatementExportApplicationService> statementExportApplicationService,
            Lazy<IAppBaseDataService> appBaseDataService,
            Lazy<IMatchingApplicationService> matchingApplicationService,
            Lazy<IGuarantorApplicationService> guarantorApplicationService,
            Lazy<IRedactionApplicationService> redactionApplicationService,
            ISessionContext<VisitPay> sessionContext) : base(applicationServiceCommonService)
        {
            this._changeSetApplicationService = changeSetApplicationService;
            this._changeEventApplicationService = changeEventApplicationService;
            this._contentApplicationService = contentApplicationService;
            this._guarantorService = guarantorService;
            this._hsGuarantorApplicationService = hsGuarantorApplicationService;
            this._featureApplicationService = featureApplicationService;
            this._financePlanApplicationService = financePlanApplicationService;
            this._managedUserApplicationService = managedUserApplicationService;
            this._managingUserApplicationService = managingUserApplicationService;
            this._paymentMethodsApplicationService = paymentMethodsApplicationService;
            this._paymentSubmissionApplicationService = paymentSubmissionApplicationService;
            this._randomizedTestApplicationService = randomizedTestApplicationService;
            this._scheduledPaymentApplicationService = scheduledPaymentApplicationService;
            this._statementApplicationService = statementApplicationService;
            this._statementCreationApplicationService = statementCreationApplicationService;
            this._supportRequestApplicationService = supportRequestApplicationService;
            this._testPaymentMethodsApplicationService = testPaymentMethodsApplicationService;
            this._visitPayUserApplicationService = visitPayUserApplicationService;
            this._visitPayUserIssueResultApplicationService = visitPayUserIssueResultApplicationService;
            this._visitApplicationService = visitApplicationService;
            this._visitRepository = visitRepository;
            this._hsGuarantorService = hsGuarantorService;
            this._hsVisitService = hsVisitService;
            this._vpStatementService = vpStatementService;
            this._demoVisitPayUserService = demoVisitPayUserService;
            this._vpGuarantorKeepCurrentRepository = vpGuarantorKeepCurrentRepository;
            this._hsGuarantorKeepCurrentRepository = hsGuarantorKeepCurrentRepository;
            this._eob835Service = eob835Service;
            this._remittanceCodeRepository = remittanceCodeRepository;
            this._eobPayerFilterRepository = eobPayerFilterRepository;
            this._qaAgingApplicationService = qaAgingApplicationService;
            this._qaPaymentService = qaPaymentService;
            this._ihcEnrollmentService = ihcEnrollmentService;
            this._statementExportApplicationService = statementExportApplicationService;
            this._appBaseDataService = appBaseDataService;
            this._matchingApplicationService = matchingApplicationService;
            this._guarantorApplicationService = guarantorApplicationService;
            this._redactionApplicationService = redactionApplicationService;
            this._session = sessionContext;
        }

        public void Run(string personaUsername, string hsGuarantorSourceSystemKey)
        {
            if (string.IsNullOrWhiteSpace(personaUsername))
            {
                return;
            }
            
            MethodInfo methodInfo = this.GetType().GetMethods().FirstOrDefault(method => method.GetCustomAttribute<PersonaAttribute>()?.PersonaUsername == personaUsername);
            if (methodInfo != null)
            {
                if (PersonaConstants.HsDataScenarios.Contains(personaUsername))
                {
                    methodInfo.Invoke(this, new object[] { hsGuarantorSourceSystemKey });
                }
                else
                {
                    methodInfo.Invoke(this, null);
                }

            }
        }

        #region statementguy
        
        public PersonaResultDto StatementGuy9()
        {
            const string username = "statementguy9";

            this._demoVisitPayUserService.Value.ClearAndRename(username);

            HsGuarantorDto hsGuarantorDto = new HsGuarantorDtoBuilder("Statement9", "Guy9", new DateTime(1968, 02, 02))
                .WithAddress("123 test st", "Boise", "ID", "83712")
                .ToHsGuarantorDto();

            VisitChangeDto visitChangeDto1 = new VisitChangeDtoBuilder(DateTime.UtcNow.AddDays(-24), BillingApplicationConstants.PB)
                .WithPatient(hsGuarantorDto)
                .WithDescription("Hairline Procedure")
                .WithTransaction("Charge", 1907.62m, VpTransactionTypeEnum.HsCharge)
                .ToVisitChangeDto();

            VisitChangeDto visitChangeDto2 = new VisitChangeDtoBuilder(DateTime.UtcNow.AddDays(-15), BillingApplicationConstants.HB)
                .WithPatient(hsGuarantorDto)
                .WithDescription("ACL Surgery")
                .WithTransaction("Surgery", 3750m, VpTransactionTypeEnum.HsCharge)
                .WithTransaction("Anesthesia", 1507.19m, VpTransactionTypeEnum.HsCharge)
                .WithTransaction("Insurance Payment", -1456.78m, VpTransactionTypeEnum.HsPayorCash, DateTime.UtcNow.AddDays(-3))
                .ToVisitChangeDto();

            VisitChangeDto visitChangeDto3 = new VisitChangeDtoBuilder(DateTime.UtcNow.AddDays(-15), BillingApplicationConstants.HB)
                .WithPatient(hsGuarantorDto)
                .WithDescription("LCL Surgery")
                .WithTransaction("Surgery", 2473m, VpTransactionTypeEnum.HsCharge)
                .WithTransaction("Anesthesia", 1019.28m, VpTransactionTypeEnum.HsCharge)
                .WithTransaction("Insurance Payment", -990.87m, VpTransactionTypeEnum.HsPayorCash, DateTime.UtcNow.AddDays(-3))
                .ToVisitChangeDto();
                visitChangeDto3.VpEligible = false;

            VisitChangeDto visitChangeDto4 = new VisitChangeDtoBuilder(DateTime.UtcNow.AddDays(-21), BillingApplicationConstants.HB)
                .WithPatient(hsGuarantorDto)
                .WithDescription("Broken Face")
                .WithTransaction("Emergency Room", 777.77m, VpTransactionTypeEnum.HsCharge)
                .WithTransaction("MRI", 1000.05m, VpTransactionTypeEnum.HsCharge)
                .ToVisitChangeDto();

            PaymentMethodDtoBuilder amexGood = new PaymentMethodDtoBuilder().WithAmexGood().WithPrimary();
            PaymentMethodDtoBuilder visaBad = new PaymentMethodDtoBuilder().WithCardError().WithPrimary();

            return this.CreateBuilder()
                .CreateGuarantor(hsGuarantorDto)
                .CreateVisits(hsGuarantorDto, visitChangeDto1)
                .CreateVisits(hsGuarantorDto, visitChangeDto2)
                .CreateVisits(hsGuarantorDto, visitChangeDto3)
                .Register("statementguy9@visitpaytest.com", "statementguy9", DefaultPassword, "555-555-5555")
                .MakeVisitEligible(visitChangeDto3.VisitId)
                .AddPaymentMethod(amexGood)
                .Rollback(1)
                .CreateVisits(hsGuarantorDto, visitChangeDto2)
                .CreateEob(visitChangeDto2.VisitId, PayerFilterEnum.PacificSourceCommunityHealthPlans)
                .FinancePlan(200m, false)
                .PaymentDueDate()
                .StatementDate()
                .CreateVisits(hsGuarantorDto, visitChangeDto3)
                .FinancePlan(350m, false)
                .Rollback(4)
                .CreateVisits(hsGuarantorDto, visitChangeDto4)
                .Rollback(5)
                .Payment(visitChangeDto4.SourceSystemKey, 500m, amexGood.ToPaymentMethodDto().PaymentMethodId.GetValueOrDefault(0))
                .PaymentDueDate()
                .StatementDate()
                .PaymentDueDate()
                .StatementDate()
                .PaymentDueDate()
                .StatementDate()
                .AddPaymentMethod(visaBad)
                .PaymentDueDate()
                .Payment(visitChangeDto4.SourceSystemKey, 250m, amexGood.ToPaymentMethodDto().PaymentMethodId.GetValueOrDefault(0), 14)
                .Rollback(2)
                .ToResult(-3);
        }

        #endregion

        #region training accounts

        private const string TrainingHbVisitDescription = "Hospital Location A";
        private const string TrainingPbVisitDescription = "Physican Office A, Brown";
        private const string TrainingInitialTransactionDescription = "Initial Transaction";
        private const string TrainingInsuranceTransactionDescription = "CONTRACTUAL INSURANCE";

        /// <summary>
        /// aaron.training
        /// </summary>
        [Persona(PersonaConstants.AaronTrainingUsername)]
        public void AaronTraining()
        {
            IPersonaCredential credential = new TrainingPersonaCredential(PersonaConstants.AaronTrainingUsername, "Aaron", "Training", string.Empty);
            this._demoVisitPayUserService.Value.ClearAndRename(credential.Username);

            HsGuarantorDto hsGuarantorDto = new HsGuarantorDtoBuilder(credential, new DateTime(1945, 04, 13))
                .WithAddress("123 Test St", "Boise", "ID", "83728")
                .ToHsGuarantorDto();

            VisitChangeDto visitChangeDto1 = new VisitChangeDtoBuilder(DateTime.UtcNow.AddDays(-15), BillingApplicationConstants.HB)
                .WithPatient(hsGuarantorDto)
                .WithDescription(TrainingHbVisitDescription)
                .WithTransaction(TrainingInitialTransactionDescription, 700m, VpTransactionTypeEnum.HsCharge)
                .ToVisitChangeDto();

            VisitChangeDto visitChangeDto2 = new VisitChangeDtoBuilder(DateTime.UtcNow.AddDays(-15), BillingApplicationConstants.PB)
                .WithPatient(hsGuarantorDto)
                .WithDescription(TrainingPbVisitDescription)
                .WithTransaction(TrainingInitialTransactionDescription, 150m, VpTransactionTypeEnum.HsCharge)
                .ToVisitChangeDto();

            VisitChangeDto visitChangeDto3 = new VisitChangeDtoBuilder(DateTime.UtcNow.AddDays(-15), BillingApplicationConstants.HB)
                .WithPatient(hsGuarantorDto)
                .WithDescription(TrainingHbVisitDescription)
                .WithTransaction(TrainingInitialTransactionDescription, 112.50m, VpTransactionTypeEnum.HsCharge)
                .ToVisitChangeDto();

            PaymentMethodDtoBuilder visaGood = new PaymentMethodDtoBuilder().WithVisaGood().WithPrimary();

            this.CreateBuilder()
                .CreateGuarantor(hsGuarantorDto)
                .CreateVisits(hsGuarantorDto, visitChangeDto1)
                .Register(credential)
                .AddPaymentMethod(visaGood)
                .Rollback(4)
                .FinancePlan(175m, false)
                .Rollback(7)
                .CreateVisits(hsGuarantorDto, visitChangeDto2, visitChangeDto3)
                .PaymentDueDate()
                .StatementDate()
                .AddHold(visitChangeDto2.VisitId)
                .Rollback(14)
                .Payment(visitChangeDto3.SourceSystemKey, 50m, visaGood.ToPaymentMethodDto().PaymentMethodId.GetValueOrDefault(0), 3)
                .SupportRequest("Do I have to pay this On Hold visit?", 53)
                .Rollback(3);
        }

        /// <summary>
        /// bob.training
        /// </summary>
        [Persona(PersonaConstants.BobTrainingUsername)]
        public void BobTraining()
        {
            IPersonaCredential credential = new TrainingPersonaCredential(PersonaConstants.BobTrainingUsername, "Bob", "Training", string.Empty);
            this._demoVisitPayUserService.Value.ClearAndRename(credential.Username);

            HsGuarantorDto hsGuarantorDto = new HsGuarantorDtoBuilder(credential, new DateTime(1985, 02, 21))
                .WithAddress("123 Test St", "Boise", "ID", "83722")
                .ToHsGuarantorDto();
            
            VisitChangeDto visitChangeDto1 = new VisitChangeDtoBuilder(DateTime.UtcNow.AddDays(-42), BillingApplicationConstants.HB)
                .WithPatient(hsGuarantorDto)
                .WithDescription(TrainingHbVisitDescription)
                .WithTransaction(TrainingInitialTransactionDescription, 120m, VpTransactionTypeEnum.HsCharge)
                .ToVisitChangeDto();
            
            VisitChangeDtoBuilder visitChangeDtoBuilder2 = new VisitChangeDtoBuilder(DateTime.UtcNow.AddDays(-42), BillingApplicationConstants.HB)
                .WithPatient(hsGuarantorDto)
                .WithDescription(TrainingHbVisitDescription)
                .WithTransaction(TrainingInitialTransactionDescription, 80m, VpTransactionTypeEnum.HsCharge);

            VisitChangeDto visitChangeDto2 = visitChangeDtoBuilder2.ToVisitChangeDto();
            
            VisitChangeDto visitChangeDto3 = new VisitChangeDtoBuilder(DateTime.UtcNow.AddDays(-13), BillingApplicationConstants.PB)
                .WithPatient(hsGuarantorDto)
                .WithDescription(TrainingPbVisitDescription)
                .WithTransaction(TrainingInitialTransactionDescription, 212m, VpTransactionTypeEnum.HsCharge)
                .WithTransaction(TrainingInsuranceTransactionDescription, -27.18m, VpTransactionTypeEnum.HsPayorNonCash, DateTime.UtcNow.AddDays(-2))
                .WithTransaction(TrainingInsuranceTransactionDescription, -166.35m, VpTransactionTypeEnum.HsPayorNonCash, DateTime.UtcNow.AddDays(-2))
                .ToVisitChangeDto();
            
            VisitChangeDto visitChangeDto4 = new VisitChangeDtoBuilder(DateTime.UtcNow.AddDays(-13), BillingApplicationConstants.HB)
                .WithPatient(hsGuarantorDto)
                .WithDescription(TrainingHbVisitDescription)
                .WithTransaction(TrainingInitialTransactionDescription, 250m, VpTransactionTypeEnum.HsCharge)
                .ToVisitChangeDto();
            
            PaymentMethodDtoBuilder amexGood = new PaymentMethodDtoBuilder().WithAmexGood().WithPrimary();
            PaymentMethodDtoBuilder visaBad = new PaymentMethodDtoBuilder().WithCardError();

            this.CreateBuilder()
                .CreateGuarantor(hsGuarantorDto)
                .CreateVisits(hsGuarantorDto, visitChangeDto1, visitChangeDto2)
                .Register(credential)
                .AddPaymentMethod(amexGood)
                .AddPaymentMethod(visaBad)
                .StatementDate()
                .Rollback(5)
                .CreateVisits(hsGuarantorDto, visitChangeDto3, visitChangeDto4)
                .Rollback(3)
                .AddHold(visitChangeDto4.VisitId)
                .Rollback(4)
                .Payment(visitChangeDto2.SourceSystemKey, 50m, visaBad.ToPaymentMethodDto().PaymentMethodId.GetValueOrDefault(0)) // this fails intentionally
                .Payment(visitChangeDto2.SourceSystemKey, 50m, amexGood.ToPaymentMethodDto().PaymentMethodId.GetValueOrDefault(0))
                .SupportRequest("Why did my insurance not make a payment?", 34)
                .Rollback(2);
        }

        /// <summary>
        /// hank.training
        /// </summary>
        [Persona(PersonaConstants.HankTrainingUsername)]
        public void HankTraining()
        {
            IPersonaCredential credential = new TrainingPersonaCredential(PersonaConstants.HankTrainingUsername, "Hank", "Training", string.Empty);
            this._demoVisitPayUserService.Value.ClearAndRename(credential.Username);

            HsGuarantorDto hsGuarantorDto = new HsGuarantorDtoBuilder(credential, new DateTime(1961, 07, 24))
                .WithAddress("123 Test St", "Boise", "ID", "83702")
                .ToHsGuarantorDto();

            VisitChangeDto visitChangeDto1 = new VisitChangeDtoBuilder(DateTime.UtcNow.AddDays(-15), BillingApplicationConstants.HB)
                .WithPatient(hsGuarantorDto)
                .WithDescription(TrainingHbVisitDescription)
                .WithTransaction(TrainingInitialTransactionDescription, 1005m, VpTransactionTypeEnum.HsCharge)
                .ToVisitChangeDto();

            VisitChangeDto visitChangeDto2 = new VisitChangeDtoBuilder(DateTime.UtcNow.AddDays(-14), BillingApplicationConstants.PB)
                .WithPatient(hsGuarantorDto)
                .WithDescription(TrainingPbVisitDescription)
                .WithTransaction(TrainingInitialTransactionDescription, 300m, VpTransactionTypeEnum.HsCharge)
                .ToVisitChangeDto();
            
            VisitChangeDto visitChangeDto3 = new VisitChangeDtoBuilder(DateTime.UtcNow.AddDays(-17), BillingApplicationConstants.HB)
                .WithPatient(hsGuarantorDto)
                .WithDescription(TrainingHbVisitDescription)
                .WithTransaction(TrainingInitialTransactionDescription, 450m, VpTransactionTypeEnum.HsCharge)
                .ToVisitChangeDto();
            
            VisitChangeDto visitChangeDto4 = new VisitChangeDtoBuilder(DateTime.UtcNow.AddDays(-15), BillingApplicationConstants.PB)
                .WithPatient(hsGuarantorDto)
                .WithDescription(TrainingPbVisitDescription)
                .WithTransaction(TrainingInitialTransactionDescription, 700m, VpTransactionTypeEnum.HsCharge)
                .ToVisitChangeDto();
            
            PaymentMethodDtoBuilder amexGood = new PaymentMethodDtoBuilder().WithAmexGood();
            PaymentMethodDtoBuilder visaBad = new PaymentMethodDtoBuilder().WithCardError().WithPrimary();

            this.CreateBuilder()
                .CreateGuarantor(hsGuarantorDto)
                .CreateVisits(hsGuarantorDto, visitChangeDto1)
                .Register(credential)
                .AddPaymentMethod(amexGood)
                .AddPaymentMethod(visaBad)
                .Rollback(7)
                .FinancePlan(210m, false)
                .CreateVisits(hsGuarantorDto, visitChangeDto3)
                .Rollback(5)
                .PaymentDueDate()
                .StatementDate()
                .Rollback(3)
                .Payment(visitChangeDto3.SourceSystemKey, 125m, amexGood.ToPaymentMethodDto().PaymentMethodId.GetValueOrDefault(0))
                .CreateVisits(hsGuarantorDto, visitChangeDto2, visitChangeDto4)
                .Rollback(1)
                .AddHold(visitChangeDto2.VisitId)
                .Rollback(11);
        }

        /// <summary>
        /// henry.training and joni.training
        /// joni manages henry
        /// </summary>
        [Persona(PersonaConstants.JoniTrainingUsername)]
        public void JoniAndHenryTraining()
        {
            IPersonaCredential joniCredential = new TrainingPersonaCredential(PersonaConstants.JoniTrainingUsername, "Joni", "Training", string.Empty);
            IPersonaCredential henryCredential = new TrainingPersonaCredential(PersonaConstants.HenryTrainingUsername, "Henry", "Training", string.Empty);
            
            this._demoVisitPayUserService.Value.ClearAndRename(henryCredential.Username);
            this._demoVisitPayUserService.Value.ClearAndRename(joniCredential.Username);

            // henry
            HsGuarantorDto henryHsGuarantorDto = new HsGuarantorDtoBuilder(henryCredential, new DateTime(1983, 02, 06))
                .WithAddress("123 Test St", "Twin Falls", "ID", "83303")
                .ToHsGuarantorDto();

            VisitChangeDto henryVisitChangeDto = new VisitChangeDtoBuilder(DateTime.UtcNow.AddDays(-5), BillingApplicationConstants.HB)
                .WithPatient(henryHsGuarantorDto)
                .WithDescription(TrainingHbVisitDescription)
                .WithTransaction(TrainingInitialTransactionDescription, 300m, VpTransactionTypeEnum.HsCharge)
                .ToVisitChangeDto();
            
            PaymentMethodDtoBuilder henryVisaGood = new PaymentMethodDtoBuilder().WithVisaGood().WithPrimary();

            BaselineDataBuilder henryBuilder = this.CreateBuilder()
                .CreateGuarantor(henryHsGuarantorDto)
                .CreateVisits(henryHsGuarantorDto, henryVisitChangeDto.ToListOfOne())
                .Register(henryCredential)
                .AddPaymentMethod(henryVisaGood)
                .Rollback(14);

            // joni
            HsGuarantorDto joniHsGuarantorDto = new HsGuarantorDtoBuilder(joniCredential, new DateTime(1953, 02, 10))
                .WithAddress("123 Test St", "Garden City", "ID", "83714")
                .ToHsGuarantorDto();

            VisitChangeDto joniVisitChangeDto = new VisitChangeDtoBuilder(DateTime.UtcNow.AddDays(-9), BillingApplicationConstants.HB)
                .WithPatient(joniHsGuarantorDto)
                .WithDescription(TrainingHbVisitDescription)
                .WithTransaction(TrainingInitialTransactionDescription, 800m, VpTransactionTypeEnum.HsCharge)
                .ToVisitChangeDto();

            PaymentMethodDtoBuilder joniAmexGood = new PaymentMethodDtoBuilder().WithAmexGood().WithPrimary();

            BaselineDataBuilder joniBuilder = this.CreateBuilder()
                .CreateGuarantor(joniHsGuarantorDto)
                .CreateVisits(joniHsGuarantorDto, joniVisitChangeDto.ToListOfOne())
                .Register(joniCredential)
                .AddPaymentMethod(joniAmexGood)
                .Rollback(2)
                .Managing(henryBuilder.VpGuarantorDto, false);
            
            henryBuilder.Rollback(2);
            joniBuilder.Rollback(2);
        }

        /// <summary>
        /// jane.training and steve.training
        /// jane manages steve
        /// </summary>
        [Persona(PersonaConstants.JaneTrainingUsername)]
        public void JaneAndSteveTraining()
        {
            IPersonaCredential steveCredential = new TrainingPersonaCredential(PersonaConstants.SteveTrainingUsername, "Steve", "Training", string.Empty);
            IPersonaCredential janeCredential = new TrainingPersonaCredential(PersonaConstants.JaneTrainingUsername, "Jane", "Training", string.Empty);

            this._demoVisitPayUserService.Value.ClearAndRename(steveCredential.Username);
            this._demoVisitPayUserService.Value.ClearAndRename(janeCredential.Username);

            // steve
            HsGuarantorDto steveHsGuarantorDto = new HsGuarantorDtoBuilder(steveCredential, new DateTime(1976, 06, 19))
                .WithAddress("123 Test St", "Boise", "ID", "83731")
                .ToHsGuarantorDto();

            VisitChangeDto steveVisitChangeDto1 = new VisitChangeDtoBuilder(DateTime.UtcNow.AddDays(-8), BillingApplicationConstants.HB)
                .WithPatient(steveHsGuarantorDto)
                .WithDescription(TrainingHbVisitDescription)
                .WithTransaction(TrainingInitialTransactionDescription, 2000m, VpTransactionTypeEnum.HsCharge)
                .ToVisitChangeDto();

            VisitChangeDto steveVisitChangeDto2 = new VisitChangeDtoBuilder(DateTime.UtcNow.AddDays(-9), BillingApplicationConstants.PB)
                .WithPatient(steveHsGuarantorDto)
                .WithDescription(TrainingPbVisitDescription)
                .WithTransaction(TrainingInitialTransactionDescription, 130m, VpTransactionTypeEnum.HsCharge)
                .ToVisitChangeDto();

            VisitChangeDto steveVisitChangeDto3 = new VisitChangeDtoBuilder(DateTime.UtcNow.AddDays(-8), BillingApplicationConstants.HB)
                .WithPatient(steveHsGuarantorDto)
                .WithDescription(TrainingHbVisitDescription)
                .WithTransaction(TrainingInitialTransactionDescription, 500m, VpTransactionTypeEnum.HsCharge)
                .ToVisitChangeDto();

            PaymentMethodDtoBuilder steveVisaGood = new PaymentMethodDtoBuilder().WithVisaGood().WithPrimary();

            BaselineDataBuilder steveBuilder = this.CreateBuilder()
                .CreateGuarantor(steveHsGuarantorDto)
                .CreateVisits(steveHsGuarantorDto, steveVisitChangeDto1)
                .Register(steveCredential)
                .AddPaymentMethod(steveVisaGood)
                .Rollback(13)
                .FinancePlan(325m, false)
                .Rollback(2)
                .CreateVisits(steveHsGuarantorDto, steveVisitChangeDto2)
                .PaymentDueDate()
                .StatementDate()
                .SupportRequest("How do I change my email address?", 47)
                .Rollback(5)
                .CreateVisits(steveHsGuarantorDto, steveVisitChangeDto3);

            // jane
            HsGuarantorDto janeHsGuarantorDto = new HsGuarantorDtoBuilder(janeCredential, new DateTime(1961, 02, 05))
                .WithAddress("123 Test St", "Mountain Home", "ID", "83647")
                .ToHsGuarantorDto();

            VisitChangeDto janeVisitChangeDto1 = new VisitChangeDtoBuilder(DateTime.UtcNow.AddDays(-15), BillingApplicationConstants.HB)
                .WithPatient(janeHsGuarantorDto)
                .WithDescription(TrainingHbVisitDescription)
                .WithTransaction(TrainingInitialTransactionDescription, 779.90m, VpTransactionTypeEnum.HsCharge)
                .ToVisitChangeDto();

            VisitChangeDto janeVisitChangeDto2 = new VisitChangeDtoBuilder(DateTime.UtcNow.AddDays(-15), BillingApplicationConstants.HB)
                .WithPatient(janeHsGuarantorDto)
                .WithDescription(TrainingHbVisitDescription)
                .WithTransaction(TrainingInitialTransactionDescription, 245.5m, VpTransactionTypeEnum.HsCharge)
                .WithTransaction(TrainingInsuranceTransactionDescription, -100m, VpTransactionTypeEnum.HsPayorNonCash, DateTime.UtcNow.AddDays(-7))
                .ToVisitChangeDto();
            
            VisitChangeDto janeVisitChangeDto3 = new VisitChangeDtoBuilder(DateTime.UtcNow.AddDays(-12), BillingApplicationConstants.HB)
                .WithPatient(janeHsGuarantorDto)
                .WithDescription(TrainingHbVisitDescription)
                .WithTransaction(TrainingInitialTransactionDescription, 1079.90m, VpTransactionTypeEnum.HsCharge)
                .ToVisitChangeDto();

            PaymentMethodDtoBuilder janeVisaGood = new PaymentMethodDtoBuilder().WithVisaGood().WithPrimary();

            BaselineDataBuilder janeBuilder = this.CreateBuilder()
                .CreateGuarantor(janeHsGuarantorDto)
                .CreateVisits(steveHsGuarantorDto, janeVisitChangeDto1)
                .Register(janeCredential)
                .AddPaymentMethod(janeVisaGood)
                .Rollback(10)
                .FinancePlan(155m, false)
                .Rollback(4)
                .CreateVisits(steveHsGuarantorDto, janeVisitChangeDto2)
                .PaymentDueDate()
                .StatementDate()
                .Rollback(5)
                .CreateVisits(steveHsGuarantorDto, janeVisitChangeDto3)
                .SupportRequest("How do I modify my due date?", 27)
                .Rollback(5)
                .Managing(steveBuilder.VpGuarantorDto, true);
            
            janeBuilder.Rollback(7);
            steveBuilder.Rollback(7);
        }

        /// <summary>
        /// julie.training
        /// </summary>
        [Persona(PersonaConstants.JulieTrainingUsername)]
        public void JulieTraining()
        {
            IPersonaCredential credential = new TrainingPersonaCredential(PersonaConstants.JulieTrainingUsername, "Julie", "Training", string.Empty);
            this._demoVisitPayUserService.Value.ClearAndRename(credential.Username);

            HsGuarantorDto hsGuarantorDto = new HsGuarantorDtoBuilder(credential, new DateTime(1948, 12, 21))
                .WithAddress("123 Test St", "Rexburg", "ID", "83440")
                .ToHsGuarantorDto();

            VisitChangeDto visitChangeDto = new VisitChangeDtoBuilder(DateTime.UtcNow.AddDays(-15), BillingApplicationConstants.PB)
                .WithPatient(hsGuarantorDto)
                .WithDescription(TrainingPbVisitDescription)
                .WithTransaction(TrainingInitialTransactionDescription, 200m, VpTransactionTypeEnum.HsCharge)
                .ToVisitChangeDto();

            this.CreateBuilder()
                .CreateGuarantor(hsGuarantorDto)
                .CreateVisits(hsGuarantorDto, visitChangeDto.ToListOfOne())
                .Register(credential)
                .Rollback(15);
        }

        /// <summary>
        /// lance.training
        /// </summary>
        [Persona(PersonaConstants.LanceTrainingUsername)]
        public void LanceTraining()
        {
            IPersonaCredential credential = new TrainingPersonaCredential(PersonaConstants.LanceTrainingUsername, "Lance", "Training", string.Empty);
            this._demoVisitPayUserService.Value.ClearAndRename(credential.Username);

            HsGuarantorDto hsGuarantorDto = new HsGuarantorDtoBuilder(credential, new DateTime(1940, 07, 29))
                .WithAddress("123 Test St", "New Plymouth", "ID", "83655")
                .ToHsGuarantorDto();

            VisitChangeDto visitChangeDto1 = new VisitChangeDtoBuilder(DateTime.UtcNow.AddDays(-15), BillingApplicationConstants.HB)
                .WithPatient(hsGuarantorDto)
                .WithDescription(TrainingHbVisitDescription)
                .WithTransaction(TrainingInitialTransactionDescription, 200m, VpTransactionTypeEnum.HsCharge)
                .ToVisitChangeDto();

            VisitChangeDto visitChangeDto2 = new VisitChangeDtoBuilder(DateTime.UtcNow.AddDays(-15), BillingApplicationConstants.PB)
                .WithPatient(hsGuarantorDto)
                .WithDescription(TrainingPbVisitDescription)
                .WithTransaction(TrainingInitialTransactionDescription, 60m, VpTransactionTypeEnum.HsCharge)
                .ToVisitChangeDto();

            VisitChangeDto visitChangeDto3 = new VisitChangeDtoBuilder(DateTime.UtcNow.AddDays(-15), BillingApplicationConstants.HB)
                .WithPatient(hsGuarantorDto)
                .WithDescription(TrainingHbVisitDescription)
                .WithTransaction(TrainingInitialTransactionDescription, 140m, VpTransactionTypeEnum.HsCharge)
                .ToVisitChangeDto();

            VisitChangeDto visitChangeDto4 = new VisitChangeDtoBuilder(DateTime.UtcNow.AddDays(-15), BillingApplicationConstants.HB)
                .WithPatient(hsGuarantorDto)
                .WithDescription(TrainingHbVisitDescription)
                .WithTransaction(TrainingInitialTransactionDescription, 146m, VpTransactionTypeEnum.HsCharge)
                .ToVisitChangeDto();

            PaymentMethodDtoBuilder amexGood = new PaymentMethodDtoBuilder().WithAmexGood().WithPrimary();

            this.CreateBuilder()
                .CreateGuarantor(hsGuarantorDto)
                .CreateVisits(hsGuarantorDto, visitChangeDto1)
                .Register(credential)
                .AddPaymentMethod(amexGood)
                .Rollback(15)
                .StatementDate()
                .StatementDate()
                .CreateVisits(hsGuarantorDto, visitChangeDto2, visitChangeDto3)
                .StatementDate()
                .StatementDate()
                .Rollback(5)
                .CreateVisits(hsGuarantorDto, visitChangeDto4)
                .SupportRequest("How do I apply for financial assistance?", 42)
                .Rollback(7);
        }

        /// <summary>
        /// rick.training and susie.training
        /// rick manages susie
        /// </summary>
        [Persona(PersonaConstants.RickTrainingUsername)]
        public void RickAndSusieTraining()
        {
            IPersonaCredential susieCredential = new TrainingPersonaCredential(PersonaConstants.SusieTrainingUsername, "Susie", "Training", string.Empty);
            IPersonaCredential rickCredential = new TrainingPersonaCredential(PersonaConstants.RickTrainingUsername, "Rick", "Training", string.Empty);

            this._demoVisitPayUserService.Value.ClearAndRename(susieCredential.Username);
            this._demoVisitPayUserService.Value.ClearAndRename(rickCredential.Username);

            // susie
            HsGuarantorDto susieHsGuarantorDto = new HsGuarantorDtoBuilder(susieCredential, new DateTime(1953, 02, 10))
                .WithAddress("123 Test St", "Caldwell", "ID", "83606")
                .ToHsGuarantorDto();

            VisitChangeDto susieVisitChangeDto = new VisitChangeDtoBuilder(DateTime.UtcNow.AddDays(-9), BillingApplicationConstants.HB)
                .WithPatient(susieHsGuarantorDto)
                .WithDescription(TrainingHbVisitDescription)
                .WithTransaction(TrainingInitialTransactionDescription, 700m, VpTransactionTypeEnum.HsCharge)
                .ToVisitChangeDto();

            PaymentMethodDtoBuilder susieAmexGood = new PaymentMethodDtoBuilder().WithAmexGood().WithPrimary();

            BaselineDataBuilder susieBuilder = this.CreateBuilder()
                .CreateGuarantor(susieHsGuarantorDto)
                .CreateVisits(susieHsGuarantorDto, susieVisitChangeDto.ToListOfOne())
                .Register(susieCredential)
                .AddPaymentMethod(susieAmexGood)
                .Rollback(4);

            // rick
            HsGuarantorDto rickHsGuarantorDto = new HsGuarantorDtoBuilder(rickCredential, new DateTime(1944, 03, 07))
                .WithAddress("123 Test St", "Wendell", "ID", "83355")
                .ToHsGuarantorDto();

            VisitChangeDto rickVisitChangeDto = new VisitChangeDtoBuilder(DateTime.UtcNow.AddDays(-15), BillingApplicationConstants.HB)
                .WithPatient(rickHsGuarantorDto)
                .WithDescription(TrainingHbVisitDescription)
                .WithTransaction(TrainingInitialTransactionDescription, 1200m, VpTransactionTypeEnum.HsCharge)
                .ToVisitChangeDto();

            PaymentMethodDtoBuilder rickVisaGood = new PaymentMethodDtoBuilder().WithVisaGood().WithPrimary();

            BaselineDataBuilder rickBuilder = this.CreateBuilder()
                .CreateGuarantor(rickHsGuarantorDto)
                .CreateVisits(rickHsGuarantorDto, rickVisitChangeDto.ToListOfOne())
                .Register(rickCredential)
                .AddPaymentMethod(rickVisaGood)
                .Rollback(3)
                .Managing(susieBuilder.VpGuarantorDto, false);
            
            rickBuilder.Rollback(5);
            susieBuilder.Rollback(5);
        }

        /// <summary>
        /// sara.training
        /// </summary>
        [Persona(PersonaConstants.SaraTrainingUsername)]
        public void SaraTraining()
        {
            IPersonaCredential credential = new TrainingPersonaCredential(PersonaConstants.SaraTrainingUsername, "Sara", "Training", string.Empty);
            this._demoVisitPayUserService.Value.ClearAndRename(credential.Username);

            HsGuarantorDto hsGuarantorDto = new HsGuarantorDtoBuilder(credential, new DateTime(1978, 09, 21))
                .WithAddress("123 Test St", "New Plymouth", "ID", "83655")
                .ToHsGuarantorDto();

            VisitChangeDto visitChangeDto1 = new VisitChangeDtoBuilder(DateTime.UtcNow.AddDays(-9), BillingApplicationConstants.HB)
                .WithPatient(hsGuarantorDto)
                .WithDescription(TrainingHbVisitDescription)
                .WithTransaction(TrainingInitialTransactionDescription, 2500m, VpTransactionTypeEnum.HsCharge)
                .WithTransaction(TrainingInsuranceTransactionDescription, -500m, VpTransactionTypeEnum.HsPayorNonCash)
                .ToVisitChangeDto();

            VisitChangeDto visitChangeDto2 = new VisitChangeDtoBuilder(DateTime.UtcNow.AddDays(-7), BillingApplicationConstants.PB)
                .WithPatient(hsGuarantorDto)
                .WithDescription(TrainingPbVisitDescription)
                .WithTransaction(TrainingInitialTransactionDescription, 600m, VpTransactionTypeEnum.HsCharge)
                .ToVisitChangeDto();

            PaymentMethodDtoBuilder amexGood = new PaymentMethodDtoBuilder().WithAmexGood().WithPrimary();
            PaymentMethodDtoBuilder visaBad = new PaymentMethodDtoBuilder().WithCardError().WithPrimary();

            this.CreateBuilder()
                .CreateGuarantor(hsGuarantorDto)
                .CreateVisits(hsGuarantorDto, visitChangeDto1, visitChangeDto2)
                .Register(credential)
                .AddPaymentMethod(amexGood)
                .Rollback(9)
                .FinancePlan(185m, true)
                .PaymentDueDate()
                .StatementDate()
                .PaymentDueDate()
                .StatementDate()
                .PaymentDueDate()
                .StatementDate()
                .AddPaymentMethod(visaBad)
                .PaymentDueDate()
                .StatementDate()
                .Rollback(5)
                .SupportRequest("I need to reconfigure my finance plan", 23)
                .Rollback(7);
        }

        #endregion

        #region demo accounts

        // run this before creating demo accounts if you want the full demo account experience (need to comment environment checks out to run locally)
        // C:\Repos\visitpay\Database\ReadyRoll.VisitPay\QATOOLS_VP_APP\Post-Deployment\070_TestData_End\100_DemoEnvironmentSettings.sql
        
        // this is to make it a bit harder to login to the "source" accounts
        private const string DemoSourcePassword = "Source.password1";

        [Persona(PersonaConstants.JaneSmithUsername)]
        public void JaneSmith()
        {
            DemoPersonaCredential credential = new DemoPersonaCredential(PersonaConstants.JaneSmithUsername, "Jane", "Smith", string.Empty);
            credential.OverridePassword(DemoSourcePassword);

            this._demoVisitPayUserService.Value.ClearAndRename(credential.Username);

            HsGuarantorDto hsGuarantorDto = new HsGuarantorDtoBuilder(credential, new DateTime(1980, 01, 01))
                .WithAddress("1212 S 2nd Street", "Boise", "ID", "83705")
                .ToHsGuarantorDto();

            VisitChangeDto visitChangeDto1 = new VisitChangeDtoBuilder(DateTime.UtcNow.AddDays(-18), BillingApplicationConstants.HB)
                .WithPatient(hsGuarantorDto)
                .WithDescription("Cataract Removal with Insertion of Intraocular Lens, 1 Stage")
                .WithTransaction("Cataract Removal with Insertion of Intraocular Lens, 1 Stage", 4500.00m, VpTransactionTypeEnum.HsCharge)
                .WithTransaction("Insurance Payment", -500m, VpTransactionTypeEnum.HsPayorCash, DateTime.UtcNow.AddDays(-11))
                .WithTransaction("Contractual Write-off", -500m, VpTransactionTypeEnum.HsPayorNonCash, DateTime.UtcNow.AddDays(-11))
                .ToVisitChangeDto();

            VisitChangeDto visitChangeDto2 = new VisitChangeDtoBuilder(DateTime.UtcNow.AddDays(-18), BillingApplicationConstants.HB)
                .WithPatient(hsGuarantorDto)
                .WithDescription("Endoscopy, Upper GI, with biopsy")
                .WithTransaction("Endoscopy, Upper GI, with biopsy", 550.99m, VpTransactionTypeEnum.HsCharge)
                .WithTransaction("Laboratory - Tissue Exam By Pathologist", 183.23m, VpTransactionTypeEnum.HsCharge)
                .WithTransaction("Laboratory - Immunohistory Antibody 1st Stain", 335.92m, VpTransactionTypeEnum.HsCharge)
                .WithTransaction("Pharmacy - Midazolan MG/KG", 74.03m, VpTransactionTypeEnum.HsCharge)
                .WithTransaction("Pharmacy - Innopran MG/KG", 74.03m, VpTransactionTypeEnum.HsCharge)
                .WithTransaction("Laboratory - Urine Pregnancy Test", 175.33m, VpTransactionTypeEnum.HsCharge)
                .WithTransaction("Insurance Payment", -136.35m, VpTransactionTypeEnum.HsPayorCash, DateTime.UtcNow)
                .WithTransaction("Contractual Write-off", -27.18m, VpTransactionTypeEnum.HsPayorNonCash, DateTime.UtcNow)
                .WithTransaction(BaselineDataBuilder.CoPaymentTransactionDescription, -30m, VpTransactionTypeEnum.HsPatientCash)
                .WithDiscountEligible()
                .ToVisitChangeDto();

            VisitChangeDto visitChangeDto3 = new VisitChangeDtoBuilder(DateTime.UtcNow.AddDays(-23), BillingApplicationConstants.PB)
                .WithPatient(hsGuarantorDto)
                .WithPatient("Suzy", credential.LastName, new DateTime(1992, 11, 26), false, false)
                .WithDescription("Physician Services - Phil McGraw")
                .WithTransaction("99212 - LVL 2 OFFICE/OUTPATIENT VISIT, EST", 120m, VpTransactionTypeEnum.HsCharge)
                .WithTransaction("99218 - LAB TEST", 30m, VpTransactionTypeEnum.HsCharge)
                .ToVisitChangeDto();
            
            VisitChangeDto visitChangeDto4 = new VisitChangeDtoBuilder(DateTime.UtcNow.AddDays(-18), BillingApplicationConstants.PB)
                .WithPatient(hsGuarantorDto)
                .WithPatient("Oliver", credential.LastName, new DateTime(2002, 04, 15), false, true)
                .WithDescription("Physician Services - Chet Thomas")
                .WithTransaction("99212 - LVL 2 OFFICE/OUTPATIENT VISIT, EST", 120m, VpTransactionTypeEnum.HsCharge)
                .WithTransaction("Contractual Write-off", -19.00m, VpTransactionTypeEnum.HsPayorNonCash, DateTime.UtcNow)
                .ToVisitChangeDto();

            PaymentMethodDtoBuilder visa = new PaymentMethodDtoBuilder().WithVisaGood($"{credential.FirstName}'s Visa").WithPrimary();
            PaymentMethodDtoBuilder hqy = new PaymentMethodDtoBuilder().WithVisaGood($"{credential.FirstName}'s HQY HSA").WithAccountType(1).WithProviderType(PaymentMethodProviderTypeEnum.Hqy);

            this.CreateBuilder()
                .CreateGuarantor(hsGuarantorDto)
                .CreateVisits(hsGuarantorDto, visitChangeDto1)
                .Register(credential, "555-555-5555")
                .InRandomizedTest(RandomizedTestEnum.FinancePlanSuggestedAmount, RandomizedTestGroupEnum.FinancePlanSuggestedAmountA)
                .AddPaymentMethod(visa)
                .AddPaymentMethod(hqy)
                .ActivateSms()
                .PaymentDueDate()
                .StatementDate()
                .Rollback(3)
                .FinancePlan(200m, true)
                .PaymentDueDate()
                .StatementDate()
                .PaymentDueDate()
                .StatementDate()
                .Rollback(7)
                .CreateVisits(hsGuarantorDto, visitChangeDto2)
                .CreateEob(visitChangeDto2.VisitId, PayerFilterEnum.DemoPayor)
                .Rollback(2)
                .CreateVisits(hsGuarantorDto, visitChangeDto4)
                .CreateEob(visitChangeDto4.VisitId, PayerFilterEnum.DemoPayor)
                .Rollback(3)
                .CreateVisits(hsGuarantorDto, visitChangeDto3)
                .Rollback(1)
                .SupportRequest("Can I combine my new visits into my existing Finance Plan?", null)
                .Rollback(1)
                .ReplyToSupportRequest("Yes. Select Create Finance Plan on the home page, and then select Add to Current Plan and follow the steps.", true)
                .PaymentDueDate()
                .StatementDate()
                .AddPtpScore(250)
                //.WithDemoFeature(VisitPayFeatureEnum.DemoPreServiceUiIsEnabled)
                .WithDemoFeature(VisitPayFeatureEnum.DemoMyChartSSOUiIsEnabled)
                .WithDemoFeature(VisitPayFeatureEnum.DemoInsuranceAccrualUiIsEnabled)
                .WithDemoFeature(VisitPayFeatureEnum.DemoHealthEquityIsEnabled)
                // this needs to be after all change sets, as these facilities aren't in CDI and would be overwritten
                .UseDemoFacilities(BaselineDataBuilder.DemoHospitalFacilityIdA, BaselineDataBuilder.DemoPhysicianFacilityIdA);
        }

        [Persona(PersonaConstants.TimJonesUsername)]
        public void TimJones()
        {
            DemoPersonaCredential credential = new DemoPersonaCredential(PersonaConstants.TimJonesUsername, "Tim", "Jones", string.Empty);
            credential.OverridePassword(DemoSourcePassword);
            
            this._demoVisitPayUserService.Value.ClearAndRename(credential.Username);

            HsGuarantorDto hsGuarantorDto = new HsGuarantorDtoBuilder(credential, new DateTime(1980, 01, 01))
                .WithAddress("61 Willow Avenue", "Boise", "ID", "83713")
                .ToHsGuarantorDto();

            VisitChangeDto visitChangeDto1 = new VisitChangeDtoBuilder(DateTime.UtcNow.AddDays(-18), BillingApplicationConstants.HB)
                .WithPatient(hsGuarantorDto)
                .WithDescription("ER Visit - Laproscopic Appendectomy")
                .WithTransaction("Surgical", 6032.00m, VpTransactionTypeEnum.HsCharge)
                .WithTransaction("Pathology", 500.00m, VpTransactionTypeEnum.HsCharge)
                .WithTransaction("Radiology", 2791.00m, VpTransactionTypeEnum.HsCharge)
                .WithTransaction("Room and Board", 3000.00m, VpTransactionTypeEnum.HsCharge)
                .WithTransaction("Anesthesia", 3010.00m, VpTransactionTypeEnum.HsCharge)
                .WithTransaction("Pharmacy", 700.00m, VpTransactionTypeEnum.HsCharge)
                .WithTransaction("Insurance Payment", -12919.00m, VpTransactionTypeEnum.HsPayorCash, DateTime.UtcNow)
                .WithTransaction("Contractual Write-off", -1568.57m, VpTransactionTypeEnum.HsPayorNonCash, DateTime.UtcNow)
                .WithTransaction(BaselineDataBuilder.CoPaymentTransactionDescription, -110.00m, VpTransactionTypeEnum.HsPatientCash)
                .WithDiscountEligible()
                .ToVisitChangeDto();
            
            VisitChangeDto visitChangeDto2 = new VisitChangeDtoBuilder(DateTime.UtcNow.AddDays(-23), BillingApplicationConstants.PB)
                .WithPatient("Timmy", "Jones Jr.", new DateTime(2003, 05, 17), false, true)
                .WithDescription("Physician Services - Dr. Holden")
                .WithTransaction("Consultation", 300m, VpTransactionTypeEnum.HsCharge)
                .WithTransaction("Insurance Payment", -193m, VpTransactionTypeEnum.HsPayorCash, DateTime.UtcNow)
                .WithTransaction("Contractual Write-off", -50m, VpTransactionTypeEnum.HsPayorNonCash, DateTime.UtcNow)
                .WithTransaction(BaselineDataBuilder.CoPaymentTransactionDescription, -30.00m, VpTransactionTypeEnum.HsPatientCash)
                .ToVisitChangeDto();
            
            PaymentMethodDtoBuilder mastercard = new PaymentMethodDtoBuilder().WithMastercardGood().WithPrimary();
            PaymentMethodDtoBuilder hqy = new PaymentMethodDtoBuilder().WithVisaGood().WithAccountType(1).WithProviderType(PaymentMethodProviderTypeEnum.Hqy);
            PaymentMethodDtoBuilder bank = new PaymentMethodDtoBuilder().WithAchCheckingGood();

            this.CreateBuilder()
                .CreateGuarantor(hsGuarantorDto)
                .CreateVisits(hsGuarantorDto, visitChangeDto2, visitChangeDto1)
                .Register(credential, "555-555-5555")
                .InRandomizedTest(RandomizedTestEnum.FinancePlanSuggestedAmount, RandomizedTestGroupEnum.FinancePlanSuggestedAmountB)
                .CreateEob(visitChangeDto1.VisitId, PayerFilterEnum.DemoPayor, 1435.43m)
                .CreateEob(visitChangeDto2.VisitId, PayerFilterEnum.DemoPayor, 27.00m)
                .ActivateSms()
                .AddPaymentMethod(mastercard)
                .AddPaymentMethod(bank)
                .AddPaymentMethod(hqy)
                .AddPtpScore(250)
                // this needs to be after all change sets, as these facilities aren't in CDI and would be overwritten
                .UseDemoFacilities(BaselineDataBuilder.DemoHospitalFacilityIdB, BaselineDataBuilder.DemoPhysicianFacilityIdB)
                .WithDemoFeature(VisitPayFeatureEnum.DemoMyChartSSOUiIsEnabled)
                .WithDemoFeature(VisitPayFeatureEnum.DemoHealthEquityIsEnabled)
                .Rollback(5);
        }

        [Persona(PersonaConstants.BillSmithUsername)]
        public void BillSmith()
        {
            DemoPersonaCredential credential = new DemoPersonaCredential(PersonaConstants.BillSmithUsername, "Bill", "Smith", string.Empty);
            credential.OverridePassword(DemoSourcePassword);

            this._demoVisitPayUserService.Value.ClearAndRename(credential.Username);

            HsGuarantorDto hsGuarantorDto = new HsGuarantorDtoBuilder(credential, new DateTime(1985, 05, 01))
                .WithAddress("7123 S 3rd Street", "Boise", "ID", "83705")
                .ToHsGuarantorDto();

            VisitChangeDto visitChangeDto1 = new VisitChangeDtoBuilder(DateTime.UtcNow.AddDays(-18), BillingApplicationConstants.HB)
                .WithPatient(hsGuarantorDto)
                .WithDescription("ER Visit")
                .WithTransaction("ER General", 850.00m, VpTransactionTypeEnum.HsCharge)
                .WithTransaction("Insurance Payment", -131.23m, VpTransactionTypeEnum.HsPayorCash, DateTime.UtcNow)
                .WithTransaction("Contractual Write-off", -24.11m, VpTransactionTypeEnum.HsPayorNonCash, DateTime.UtcNow)
                .ToVisitChangeDto();

            VisitChangeDto visitChangeDto2 = new VisitChangeDtoBuilder(DateTime.UtcNow.AddDays(-18), BillingApplicationConstants.PB)
                .WithPatient(hsGuarantorDto)
                .WithDescription("Patient Services")
                .WithTransaction("Laboratory General", 550.99m, VpTransactionTypeEnum.HsCharge)
                .WithTransaction("Pharamacy General", 183.23m, VpTransactionTypeEnum.HsCharge)
                .WithDiscountEligible()
                .ToVisitChangeDto();

            PaymentMethodDtoBuilder visa = new PaymentMethodDtoBuilder().WithVisaGood($"{credential.FirstName}'s Visa").WithPrimary();

            this.CreateBuilder()
                .CreateGuarantor(hsGuarantorDto)
                .CreateVisits(hsGuarantorDto, visitChangeDto1, visitChangeDto2)
                .Register(credential, "555-555-5555")
                .AddPaymentMethod(visa)
                .ActivateSms()
                .PaymentDueDate()
                .StatementDate()
                .Rollback(3)
                .CreateEob(visitChangeDto1.VisitId, PayerFilterEnum.DemoPayor)
                .WithDemoFeature(VisitPayFeatureEnum.DemoInsuranceAccrualUiIsEnabled);
        }

        [Persona(PersonaConstants.MaryParkUsername)]
        public void MaryAndLarryPark()
        {
            DemoPersonaCredential maryCredential = new DemoPersonaCredential(PersonaConstants.MaryParkUsername, "Mary", "Park", string.Empty);
            maryCredential.OverridePassword(DemoSourcePassword);

            DemoPersonaCredential larryCredential = new DemoPersonaCredential(PersonaConstants.LarryParkUsername, "Larry", "Park", string.Empty);
            larryCredential.OverridePassword(DemoSourcePassword);

            this._demoVisitPayUserService.Value.ClearAndRename(maryCredential.Username);
            this._demoVisitPayUserService.Value.ClearAndRename(larryCredential.Username);
            
            #region managing user

            HsGuarantorDto managingHsGuarantorDto = new HsGuarantorDtoBuilder(maryCredential, new DateTime(1978, 05, 05))
                .WithAddress("321 SW Street", "Boise", "ID", "83927")
                .ToHsGuarantorDto();

            VisitChangeDto managingVisitChangeDto1 = new VisitChangeDtoBuilder(DateTime.UtcNow.AddDays(-20), BillingApplicationConstants.PB)
                .WithPatient(managingHsGuarantorDto)
                .WithDescription("Physician Services - Phil McGraw")
                .WithTransaction("LVL 2 OFFICE/OUTPATIENT VISIT, EST", 150m, VpTransactionTypeEnum.HsCharge)
                .WithTransaction("LVL 2 OFFICE/OUTPATIENT VISIT, EST", 150m, VpTransactionTypeEnum.HsCharge)
                .WithTransaction("Insurance Payment", -50m, VpTransactionTypeEnum.HsPayorCash, DateTime.UtcNow)
                .WithTransaction("Contractual Write-off", -25m, VpTransactionTypeEnum.HsPayorNonCash, DateTime.UtcNow)
                .WithTransaction(BaselineDataBuilder.CoPaymentTransactionDescription, -15m, VpTransactionTypeEnum.HsPatientCash)
                .ToVisitChangeDto();

            VisitChangeDto managingVisitChangeDto2 = new VisitChangeDtoBuilder(DateTime.UtcNow.AddDays(-20), BillingApplicationConstants.HB)
                .WithPatient(managingHsGuarantorDto)
                .WithDescription("Hospital Services - Family Medical Center")
                .WithTransaction("RADIOLOGY - DIAGNOSTIC - GENERAL : 32000081 -  HC XRAY-WRIST COMPLETE", 500m, VpTransactionTypeEnum.HsCharge)
                .WithTransaction("Insurance Payment", -50m, VpTransactionTypeEnum.HsPayorCash, DateTime.UtcNow)
                .WithTransaction("Contractual Write-off", -25m, VpTransactionTypeEnum.HsPayorNonCash, DateTime.UtcNow)
                .WithTransaction(BaselineDataBuilder.CoPaymentTransactionDescription, -15m, VpTransactionTypeEnum.HsPatientCash)
                .ToVisitChangeDto();
            
            VisitChangeDto managingVisitChangeDto3 = new VisitChangeDtoBuilder(DateTime.UtcNow.AddDays(-20), BillingApplicationConstants.HB)
                .WithPatient("Lucy", maryCredential.LastName, new DateTime(DateTime.UtcNow.Year - 15, 10, 10), false, true)
                .WithDescription("Hospital Services - Family Medical Center")
                .WithTransaction("RADIOLOGY - DIAGNOSTIC - GENERAL : 32000068 -  HC XRAY-CLAVICLE COMPLETE", 650m, VpTransactionTypeEnum.HsCharge)
                .WithTransaction("RADIOLOGY - DIAGNOSTIC - GENERAL : 32000081 -  HC XRAY-WRIST COMPLETE", 150m, VpTransactionTypeEnum.HsCharge)
                .WithTransaction("Insurance Payment", -50m, VpTransactionTypeEnum.HsPayorCash, DateTime.UtcNow)
                .WithTransaction("Contractual Write-off", -25m, VpTransactionTypeEnum.HsPayorNonCash, DateTime.UtcNow)
                .WithTransaction(BaselineDataBuilder.CoPaymentTransactionDescription, -15m, VpTransactionTypeEnum.HsPatientCash)
                .ToVisitChangeDto();

            PaymentMethodDtoBuilder managingMastercardGood = new PaymentMethodDtoBuilder().WithMastercardGood("MC").WithProviderType(PaymentMethodProviderTypeEnum.Hqy).WithAccountType(1);
            PaymentMethodDtoBuilder managingVisaGood = new PaymentMethodDtoBuilder().WithVisaGood("Visa").WithPrimary();

            #endregion

            #region managed user
            
            HsGuarantorDto managedHsGuarantorDto = new HsGuarantorDtoBuilder(larryCredential, new DateTime(1981, 04, 04))
                .WithAddress("321 SW Street", "Boise" , "ID", "83927")
                .ToHsGuarantorDto();
            
            VisitChangeDto managedVisitChangeDto1 = new VisitChangeDtoBuilder(DateTime.UtcNow.AddDays(-20), BillingApplicationConstants.PB)
                .WithPatient(managedHsGuarantorDto)
                .WithDescription("Physician Services - Phil McGraw")
                .WithTransaction("LVL 2 OFFICE/OUTPATIENT VISIT, EST", 300m, VpTransactionTypeEnum.HsCharge)
                .WithTransaction("Insurance Payment", -50m, VpTransactionTypeEnum.HsPayorCash, DateTime.UtcNow)
                .WithTransaction("Contractual Write-off", -25m, VpTransactionTypeEnum.HsPayorNonCash, DateTime.UtcNow)
                .WithTransaction(BaselineDataBuilder.CoPaymentTransactionDescription, -15m, VpTransactionTypeEnum.HsPatientCash)
                .ToVisitChangeDto();

            VisitChangeDto managedVisitChangeDto2 = new VisitChangeDtoBuilder(DateTime.UtcNow.AddDays(-20), BillingApplicationConstants.HB)
                .WithPatient(managedHsGuarantorDto)
                .WithDescription("Hospital Services - Family Medical Center")
                .WithTransaction("RADIOLOGY - DIAGNOSTIC - GENERAL : 32000081 -  HC XRAY-WRIST COMPLETE", 500m, VpTransactionTypeEnum.HsCharge)
                .WithTransaction("Insurance Payment", -25m, VpTransactionTypeEnum.HsPayorCash, DateTime.UtcNow)
                .WithTransaction("Contractual Write-off", -25m, VpTransactionTypeEnum.HsPayorNonCash, DateTime.UtcNow)
                .WithTransaction(BaselineDataBuilder.CoPaymentTransactionDescription, -15m, VpTransactionTypeEnum.HsPatientCash)
                .ToVisitChangeDto();
            
            VisitChangeDto managedVisitChangeDto3 = new VisitChangeDtoBuilder(DateTime.UtcNow.AddDays(-20), BillingApplicationConstants.HB)
                .WithPatient(managedHsGuarantorDto)
                .WithDescription("Hospital Services - Family Medical Center")
                .WithTransaction("RADIOLOGY - DIAGNOSTIC - GENERAL : 32000068 -  HC XRAY-CLAVICLE COMPLETE", 650m, VpTransactionTypeEnum.HsCharge)
                .WithTransaction("RADIOLOGY - DIAGNOSTIC - GENERAL : 32000081 -  HC XRAY-WRIST COMPLETE", 150m, VpTransactionTypeEnum.HsCharge)
                .WithTransaction("Insurance Payment", -50m, VpTransactionTypeEnum.HsPayorCash, DateTime.UtcNow)
                .WithTransaction("Contractual Write-off", -50m, VpTransactionTypeEnum.HsPayorNonCash, DateTime.UtcNow)
                .WithTransaction(BaselineDataBuilder.CoPaymentTransactionDescription, -15m, VpTransactionTypeEnum.HsPatientCash)
                .ToVisitChangeDto();

            PaymentMethodDtoBuilder managedAmexGood = new PaymentMethodDtoBuilder().WithAmexGood().WithPrimary();

            #endregion

            BaselineDataBuilder managedBuilder = this.CreateBuilder()
                .CreateGuarantor(managedHsGuarantorDto)
                .CreateVisits(managedHsGuarantorDto, managedVisitChangeDto1, managedVisitChangeDto2)
                .Register(larryCredential, "555-555-5555")
                .CreateEob(managedVisitChangeDto1.VisitId, PayerFilterEnum.DemoPayor)
                .CreateEob(managedVisitChangeDto2.VisitId, PayerFilterEnum.DemoPayor)
                .AddPaymentMethod(managedAmexGood)
                .Rollback(2)
                .FinancePlan(130m, false)
                .PaymentDueDate()
                .StatementDate()
                .Rollback(5)
                .CreateVisits(managedHsGuarantorDto, managedVisitChangeDto3)
                .CreateEob(managedVisitChangeDto3.VisitId, PayerFilterEnum.DemoPayor)
                .WithDemoFeature(VisitPayFeatureEnum.DemoInsuranceAccrualUiIsEnabled);

            BaselineDataBuilder managingBuilder = this.CreateBuilder()
                .CreateGuarantor(managingHsGuarantorDto)
                .CreateVisits(managedHsGuarantorDto, managingVisitChangeDto1, managingVisitChangeDto2)
                .Register(maryCredential, "555-555-5555")
                .CreateEob(managingVisitChangeDto1.VisitId, PayerFilterEnum.DemoPayor)
                .CreateEob(managingVisitChangeDto2.VisitId, PayerFilterEnum.DemoPayor)
                .AddPaymentMethod(managingMastercardGood)
                .AddPaymentMethod(managingVisaGood)
                .ActivateSms()
                .Rollback(7)
                .FinancePlan(110.00m, false)
                .PaymentDueDate()
                .StatementDate()
                .Rollback(6)
                .CreateVisits(managedHsGuarantorDto, managingVisitChangeDto3)
                .CreateEob(managingVisitChangeDto3.VisitId, PayerFilterEnum.DemoPayor)
                .Rollback(3)
                .WithDemoFeature(VisitPayFeatureEnum.DemoInsuranceAccrualUiIsEnabled)
                .Managing(managedBuilder.VpGuarantorDto, true);

            managedBuilder
                .PaymentDueDate()
                .StatementDate();

            managingBuilder
                .PaymentDueDate()
                .StatementDate();

            managedBuilder.Rollback(3);
            managingBuilder.Rollback(3);
        }

        #endregion

        #region loadtest
        
        private const string LoadTestPassword = "QA.password1";
        
        /// <summary>
        /// loadtestguy has 7 visits, with 23 transactions per visit
        /// </summary>
        [Persona(PersonaConstants.LoadTestGuyUsername)]
        public void LoadTestGuy()
        {
            this._demoVisitPayUserService.Value.ClearAndRename(PersonaConstants.LoadTestGuyUsername);

            HsGuarantorDto hsGuarantorDto = new HsGuarantorDtoBuilder("LoadTest", "Guy", new DateTime(1980, 05, 02))
                .WithAddress("123 test st", "Boise", "ID", "83712")
                .ToHsGuarantorDto();

            IList<VisitChangeDto> visitChangeDtos = new List<VisitChangeDto>();
            for (int i = 0; i < 7; i++)
            {
                VisitChangeDtoBuilder builder = new VisitChangeDtoBuilder(DateTime.UtcNow.AddDays(-24), BillingApplicationConstants.PB)
                    .WithPatient(hsGuarantorDto)
                    .WithDescription(RandomStringGeneratorUtility.GetRandomString(15));
                
                for (int t = 0; t < 23; t++)
                {
                    if (t < 17)
                    {
                        builder.WithTransaction(RandomStringGeneratorUtility.GetRandomString(10), 200m, VpTransactionTypeEnum.HsCharge);
                    }
                    else
                    {

                        builder.WithTransaction(RandomStringGeneratorUtility.GetRandomString(10), -50m, VpTransactionTypeEnum.HsPatientCash);
                    }
                }
                
                visitChangeDtos.Add(builder.ToVisitChangeDto());
            }

            PaymentMethodDtoBuilder visaGood = new PaymentMethodDtoBuilder().WithVisaGood().WithPrimary();

            this.CreateBuilder()
                .CreateGuarantor(hsGuarantorDto)
                .CreateVisits(hsGuarantorDto, visitChangeDtos.ToArray())
                .Register("loadtestguy@visitpaytest.com", PersonaConstants.LoadTestGuyUsername, LoadTestPassword, "555-555-5555")
                .AddPaymentMethod(visaGood)
                .Rollback(5);
        }

        #endregion

        #region existing hs data

        // these should take an existing HS guarantor and create the visitpay side of their account, rather than creating the HS side as well

        [Persona(PersonaConstants.CallCenterAllStatements)]
        public void CallCenterAllStatements(string hsGuarantorSourceSystemKey)
        {
            PaymentMethodDtoBuilder visaGood = new PaymentMethodDtoBuilder().WithVisaGood().WithPrimary();
            PaymentMethodDtoBuilder cardError = new PaymentMethodDtoBuilder().WithCardError().WithPrimary();

            BaselineDataBuilder builder = this.CreateBuilder()
                .UseGuarantor(hsGuarantorSourceSystemKey);

            HsGuarantorDto hsGuarantorDto = builder.GetHsGuarantorDto();
            string username = $"{hsGuarantorDto.FirstName.ToLower()}.{hsGuarantorDto.LastName.ToLower()}";
            string email = $"{username}@visitpaytest.com";

            builder.Register(email, username, DefaultPassword, "555-555-5555")
                .OfflineGuarantor(true)
                .AddPaymentMethod(visaGood)
                .FinancePlan(0m, false)

                .PaymentDueDate()
                .StatementDate()
                //.ExportPaperStatements() // good standing (0)

                .AddPaymentMethod(cardError)

                .PaymentDueDate()
                .StatementDate()
                //.ExportPaperStatements() // past due (1)

                .PaymentDueDate()
                .StatementDate()
                //.ExportPaperStatements() // past due (2)

                .PaymentDueDate()
                .StatementDate()
                //.ExportPaperStatements() // past due (3)

                .PaymentDueDate()
                .StatementDate()
                //.ExportPaperStatements() // final past due (4)

                .Rollback(5);
        }

        [Persona(PersonaConstants.CallCenterOutboundMessagesTest)]
        public void CallCenterOutboundMessagesTest(string hsGuarantorSourceSystemKey)
        {
            this.OutboundMessagesTest(hsGuarantorSourceSystemKey, true);
        }

        [Persona(PersonaConstants.OutboundMessagesTest)]
        public void OutboundMessagesTest(string hsGuarantorSourceSystemKey)
        {
            this.OutboundMessagesTest(hsGuarantorSourceSystemKey, false);
        }

        private void OutboundMessagesTest(string hsGuarantorSourceSystemKey, bool isOffline)
        {
            BaselineDataBuilder builder = this.CreateBuilder()
                .UseGuarantor(hsGuarantorSourceSystemKey);

            HsGuarantorDto hsGuarantorDto = builder.GetHsGuarantorDto();
            string firstName = hsGuarantorDto.FirstName.ToLower();
            string lastName = hsGuarantorDto.LastName.ToLower();
            string username = $"{firstName}.{lastName}";
            string email = $"{username}@visitpaytest.com";
            DateTime dob = hsGuarantorDto.DOB ?? new DateTime(1968, 02, 02);

            VisitChangeDto visitChangeDto1 = new VisitChangeDtoBuilder(DateTime.UtcNow.AddDays(-24), BillingApplicationConstants.PB)
                .WithPatient(hsGuarantorDto)
                .WithDescription("Hairline Procedure")
                .WithTransaction("Charge", 1907.62m, VpTransactionTypeEnum.HsCharge)
                .ToVisitChangeDto();

            VisitChangeDto visitChangeDto2 = new VisitChangeDtoBuilder(DateTime.UtcNow.AddDays(-15), BillingApplicationConstants.PB)
                .WithPatient(hsGuarantorDto)
                .WithDescription(TrainingPbVisitDescription)
                .WithTransaction(TrainingInitialTransactionDescription, 150m, VpTransactionTypeEnum.HsCharge)
                .ToVisitChangeDto();

            VisitChangeDto visitChangeDto3 = new VisitChangeDtoBuilder(DateTime.UtcNow.AddDays(-15), BillingApplicationConstants.HB)
                .WithPatient(hsGuarantorDto)
                .WithDescription(TrainingHbVisitDescription)
                .WithTransaction(TrainingInitialTransactionDescription, 112.50m, VpTransactionTypeEnum.HsCharge)
                .ToVisitChangeDto();


            HsGuarantorDto hsGuarantorDto2 = new HsGuarantorDtoBuilder(firstName, lastName, dob)
                .WithAddress(hsGuarantorDto.AddressLine1, hsGuarantorDto.City, hsGuarantorDto.StateProvince, hsGuarantorDto.PostalCode)
                .WithSsn(hsGuarantorDto.SSN)
                .ToHsGuarantorDto();

            VisitChangeDto visitChangeDto4 = new VisitChangeDtoBuilder(DateTime.UtcNow.AddDays(-15), BillingApplicationConstants.HB)
                .WithPatient(hsGuarantorDto2)
                .WithDescription(TrainingHbVisitDescription + "-- unmatch me")
                .WithTransaction(TrainingInitialTransactionDescription, 122.50m, VpTransactionTypeEnum.HsCharge)
                .ToVisitChangeDto();


            HsGuarantorDto hsGuarantorDto3 = new HsGuarantorDtoBuilder(firstName, lastName, dob)
                .WithAddress(hsGuarantorDto.AddressLine1, hsGuarantorDto.City, hsGuarantorDto.StateProvince, hsGuarantorDto.PostalCode)
                .WithSsn(hsGuarantorDto.SSN)
                .ToHsGuarantorDto();

            VisitChangeDto visitChangeDto5 = new VisitChangeDtoBuilder(DateTime.UtcNow.AddDays(-15), BillingApplicationConstants.HB)
                .WithPatient(hsGuarantorDto3)
                .WithDescription(TrainingHbVisitDescription)
                .WithTransaction(TrainingInitialTransactionDescription, 132.50m, VpTransactionTypeEnum.HsCharge)
                .ToVisitChangeDto();

            PaymentMethodDtoBuilder visaGood = new PaymentMethodDtoBuilder().WithVisaGood().WithPrimary();

            builder = builder.CreateVisitsUsingHsDataServiceGroup(hsGuarantorDto, visitChangeDto1)
                .Register($"{username}@visitpaytest.com", username, DefaultPassword, "555-555-5555");

            if (isOffline)
            {
                builder = builder.OfflineGuarantor(true);
            }

            builder
                .AddPaymentMethod(visaGood)
                .Rollback(4)
                .FinancePlan(175m, false)
                .Rollback(7)
                .CreateVisitsUsingHsDataServiceGroup(hsGuarantorDto, visitChangeDto2, visitChangeDto3)
                .PaymentDueDate()
                .StatementDate()
                .AddHold(visitChangeDto2.VisitId)
                .Rollback(14)
                .Payment(visitChangeDto3.SourceSystemKey, 50m, visaGood.ToPaymentMethodDto().PaymentMethodId.GetValueOrDefault(0), 3)
                .SupportRequest("Do I have to pay this On Hold visit?", 53)
                .CreateGuarantor(hsGuarantorDto2)
                .CreateVisitsUsingHsDataServiceGroup(hsGuarantorDto2, visitChangeDto4)
                .UnmatchHsGuarantors(hsGuarantorDto2.SourceSystemKey)
                .Rollback(2)
                .CreateGuarantor(hsGuarantorDto3)
                .CreateVisitsUsingHsDataServiceGroup(hsGuarantorDto3, visitChangeDto5)
                .Rollback(3);
        }

        #endregion

        #region usability accounts

        private const string UsabilityUserNameBase = "taylor.jones";
        private const int UsabilityUserCount= 10;

        public void CreateUsabilityPersona(string personaUserName, string password)
        {
            this._demoVisitPayUserService.Value.ClearAndRename(personaUserName);

            string numberSuffixString = personaUserName.Replace(UsabilityUserNameBase, "").Trim();
            int numberSuffix;
            bool isNumeric = int.TryParse(numberSuffixString, out numberSuffix);
            if (!isNumeric)
            {
                throw new Exception("Invalid UserName provided, Must have a number suffix");
            }

            string firstName = this.UpperCaseFirstLetter(personaUserName.Split('.')[0]);
            string lastName = this.UpperCaseFirstLetter(personaUserName.Split('.')[1]).Replace(numberSuffixString,"");

            HsGuarantorDto hsGuarantorDto = new HsGuarantorDtoBuilder(firstName, lastName, DateTime.Now.AddYears(-30))
                .WithAddress("123 Test St", "Boise", "ID", "83702")
                .ToHsGuarantorDto();

            VisitChangeDto visitChangeDto = new VisitChangeDtoBuilder(DateTime.UtcNow.AddDays(-15), BillingApplicationConstants.PB)
                .WithPatient(hsGuarantorDto)
                .WithDescription(TrainingPbVisitDescription)
                .WithTransaction(TrainingInitialTransactionDescription, 2500m, VpTransactionTypeEnum.HsCharge)
                .ToVisitChangeDto();

            VisitChangeDto visitChangeDto2 = new VisitChangeDtoBuilder(DateTime.UtcNow.AddDays(-15), BillingApplicationConstants.HB)
                .WithPatient(hsGuarantorDto)
                .WithDescription(TrainingHbVisitDescription)
                .WithTransaction(TrainingInitialTransactionDescription, 825m, VpTransactionTypeEnum.HsCharge)
                .ToVisitChangeDto();

            IList<VisitChangeDto> visits = new List<VisitChangeDto> {visitChangeDto, visitChangeDto2};

            PaymentMethodDtoBuilder visaGood = new PaymentMethodDtoBuilder().WithVisaGood().WithPrimary();
            string email = $"{personaUserName}@yieldhealth.com";
            this.CreateBuilder()
                .CreateGuarantor(hsGuarantorDto)
                .CreateVisits(hsGuarantorDto, visits)
                .Register(email, personaUserName, password)
                .AddPaymentMethod(visaGood)
                .Rollback(15);

        }

        public List<string> GetUsabilityUserNames()
        {
            List<string> userNames = Enumerable.Range(1, UsabilityUserCount)
                .Select(x => $"{UsabilityUserNameBase}{x}")
                .ToList();
            return userNames;
        }

        #endregion

        #region utility

        private string UpperCaseFirstLetter(string word)
        {
            word = word.ToLower();
            char[] a = word.ToCharArray();
            a[0] = char.ToUpper(a[0]);
            return new string(a);
        }
        
        #endregion

        private BaselineDataBuilder CreateBuilder()
        {
            return new BaselineDataBuilder(
                this._changeSetApplicationService,
                this._changeEventApplicationService,
                this._contentApplicationService,
                this._guarantorService,
                this._hsGuarantorApplicationService,
                this._featureApplicationService,
                this._financePlanApplicationService,
                this._managedUserApplicationService,
                this._managingUserApplicationService,
                this._paymentMethodsApplicationService,
                this._paymentSubmissionApplicationService,
                this._randomizedTestApplicationService,
                this._scheduledPaymentApplicationService,
                this._statementApplicationService,
                this._statementCreationApplicationService,
                this._supportRequestApplicationService,
                this._testPaymentMethodsApplicationService,
                this._visitPayUserApplicationService,
                this._visitPayUserIssueResultApplicationService,
                this._visitApplicationService,
                this._vpStatementService,
                this._visitRepository,
                this._hsVisitService,
                this._hsGuarantorService,
                this._vpGuarantorKeepCurrentRepository,
                this._hsGuarantorKeepCurrentRepository,
                this._eob835Service,
                this._remittanceCodeRepository,
                this._eobPayerFilterRepository,
                this._qaAgingApplicationService,
                this._qaPaymentService,
                this._ihcEnrollmentService,
                this._statementExportApplicationService,
                this._appBaseDataService,
                this._matchingApplicationService,
                this._guarantorApplicationService,
                this._redactionApplicationService,
                this._session);
        }

        #region testing

        public void Test(int numberOfAccounts)
        {
            IList<BaselineDataBuilder> builders = new List<BaselineDataBuilder>();

            Random random = new Random();
            for (int i = 1; i <= numberOfAccounts; i++)
            {
                string username = $"useraccount{i}";
                
                this._demoVisitPayUserService.Value.ClearAndRename(username);

                HsGuarantorDto hsGuarantorDto = new HsGuarantorDtoBuilder($"fname{i}", $"lname{i}", new DateTime(random.Next(1950, 1999), random.Next(1, 12), random.Next(1, 28)))
                    .WithAddress($"{random.Next(1,999).ToString()} Test St", "Rexburg", "ID", $"{random.Next(10000, 99999).ToString()}")
                    .ToHsGuarantorDto();

                VisitChangeDto visitChangeDto = new VisitChangeDtoBuilder(DateTime.UtcNow.AddDays(-15), BillingApplicationConstants.PB)
                    .WithPatient(hsGuarantorDto)
                    .WithDescription(TrainingPbVisitDescription)
                    .WithTransaction(TrainingInitialTransactionDescription, 100m, VpTransactionTypeEnum.HsCharge)
                    .WithTransaction(TrainingInitialTransactionDescription, 200m, VpTransactionTypeEnum.HsCharge)
                    .WithTransaction(TrainingInitialTransactionDescription, 300m, VpTransactionTypeEnum.HsCharge)
                    .WithTransaction(TrainingInitialTransactionDescription, 400m, VpTransactionTypeEnum.HsCharge)
                    .WithTransaction(TrainingInitialTransactionDescription, 500m, VpTransactionTypeEnum.HsCharge)
                    .WithTransaction(TrainingInitialTransactionDescription, 600m, VpTransactionTypeEnum.HsCharge)
                    .ToVisitChangeDto();

                BaselineDataBuilder builder = this.CreateBuilder()
                        .CreateGuarantor(hsGuarantorDto)
                        .CreateVisits(hsGuarantorDto, visitChangeDto.ToListOfOne())
                        .Register($"email{i}@visitpay.com", username, "QA.password1");
                
                builders.Add(builder);
            }

            foreach (BaselineDataBuilder builder in builders)
            {
                builder.UpdateVisits(builder.GetHsGuarantorDto().HsGuarantorId, false);

                for (int i = 0; i < 2; i++)
                {
                    VisitChangeDto visitChangeDto = new VisitChangeDtoBuilder(DateTime.UtcNow.AddDays(-15), BillingApplicationConstants.PB)
                        .WithPatient(builder.GetHsGuarantorDto())
                        .WithDescription(TrainingPbVisitDescription)
                        .WithTransaction(TrainingInitialTransactionDescription, 100m, VpTransactionTypeEnum.HsCharge)
                        .WithTransaction(TrainingInitialTransactionDescription, 200m, VpTransactionTypeEnum.HsCharge)
                        .WithTransaction(TrainingInitialTransactionDescription, 300m, VpTransactionTypeEnum.HsCharge)
                        .WithTransaction(TrainingInitialTransactionDescription, 400m, VpTransactionTypeEnum.HsCharge)
                        .WithTransaction(TrainingInitialTransactionDescription, 500m, VpTransactionTypeEnum.HsCharge)
                        .WithTransaction(TrainingInitialTransactionDescription, 600m, VpTransactionTypeEnum.HsCharge)
                        .ToVisitChangeDto();

                    builder.CreateVisits(builder.GetHsGuarantorDto(), visitChangeDto.ToListOfOne(), false);
                }
            }
        }

        public void Queue()
        {
            this._changeEventApplicationService.Value.QueueAllUnprocessedChangeEvents();
        }

        #endregion
    }

    internal class PersonaAttribute : Attribute
    {
        public string PersonaUsername { get; }

        public PersonaAttribute(string personaUsername)
        {
            this.PersonaUsername = personaUsername;
        }
    }
}
﻿namespace Ivh.Application.Base.Common.Interfaces
{
    public interface IMaskConsolidated : IMask
    {
        bool IsMaskedConsolidated { get; }
        int VpGuarantorId { get; }
    }
}
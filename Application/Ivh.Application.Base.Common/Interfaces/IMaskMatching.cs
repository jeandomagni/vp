﻿namespace Ivh.Application.Base.Common.Interfaces
{
    using Ivh.Common.VisitPay.Enums;

    public interface IMaskMatching : IMask
    {
        MatchOptionEnum? MatchOption { get; }
    }
}
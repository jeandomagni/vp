﻿namespace Ivh.Application.Base.Common.Interfaces
{
    using Ivh.Common.ServiceBus.Common.Messages;

    public interface IConsumerApplicationService<in TMessage> : IApplicationService
        where TMessage : PrioritizedMessage
    {
        void ConsumeMessage(TMessage message);
    }

    public interface IConsumerApplicationService<in TMessage, out TReturn>
        where TMessage : PrioritizedMessage
    {
        TReturn ConsumeMessage(TMessage message);
    }
}
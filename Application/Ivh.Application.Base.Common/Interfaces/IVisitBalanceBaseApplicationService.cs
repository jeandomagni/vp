﻿namespace Ivh.Application.Base.Common.Interfaces
{
    using System;
    using System.Collections.Generic;
    using Dtos;

    public interface IVisitBalanceBaseApplicationService : IApplicationService
    {
        void SetTotalBalance<T>(T visitDto) where T : class, IVisitBalanceDto;
        void SetTotalBalance<T>(IList<T> visitDtos) where T : class, IVisitBalanceDto;

        /// <summary>
        /// get the total balance for active non-financed visits
        /// </summary>
        /// <param name="vpGuarantorId"></param>
        /// <returns></returns>
        decimal GetTotalBalance(int vpGuarantorId);

        /// <summary>
        /// get the total balance as a list of balances for active non-financed visits
        /// </summary>
        /// <param name="vpGuarantorId"></param>
        /// <returns></returns>
        IList<VisitBalanceBaseDto> GetTotalBalances(int vpGuarantorId);

        /// <summary>
        /// get the total balance for a given list of visits
        /// </summary>
        /// <param name="vpGuarantorId"></param>
        /// <param name="visitIds"></param>
        /// <returns></returns>
        IList<VisitBalanceBaseDto> GetTotalBalances(int vpGuarantorId, IList<int> visitIds);

        IDictionary<int, decimal[]> GetUnclearedPaymentsForVisits(IList<int> visitIds, DateTime? startDate = null, DateTime? endDate = null);
    }
}
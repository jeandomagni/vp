﻿namespace Ivh.Application.Base.Common.Interfaces
{
    using System;
    using Domain.EventJournal.Interfaces;
    using Domain.Logging.Interfaces;
    using Domain.Settings.Entities;
    using Domain.Settings.Interfaces;
    using Ivh.Common.Cache;
    using Ivh.Common.ServiceBus.Interfaces;

    public interface IApplicationService : Ivh.Common.Base.Interfaces.IApplicationService
    {
        Lazy<IApplicationSettingsService> ApplicationSettingsService { get; }
        Lazy<IFeatureService> FeatureService { get; }
        Lazy<Client> Client { get; }
        Lazy<IClientService> ClientService { get; }
        Lazy<ILoggingService> LoggingService { get; }
        IEventJournalService Journal { get; }
        Lazy<IBus> Bus { get; }
        Lazy<IDistributedCache> Cache { get; }
        DateTime SystemNow { get; }
        DateTime ClientNow { get; }
        DateTime OperationsNow { get; }
    }
}

﻿namespace Ivh.Application.Base.Common.Interfaces.Entities.HsGuarantor
{
    using System;

    /// <summary>
    /// interface to the base.HsGuarantor table
    /// </summary>
    public interface IHsGuarantor : IMatchGuarantor
    {
        int HsGuarantorId { get; set; }
        DateTime? DataChangeDate{ get; set; }
    }

    public interface IMatchGuarantor
    {
        int HsBillingSystemId { get; set; }
        string SourceSystemKey { get; set; }
        string FirstName { get; set; }
        string LastName { get; set; }
        DateTime? DOB { get; set; }
        string SSN { get; set; }
        string SSN4 { get; set; }
        string AddressLine1 { get; set; }
        string AddressLine2 { get; set; }
        string City { get; set; }
        string StateProvince { get; set; }
        string PostalCode { get; set; }
    }
}

﻿namespace Ivh.Application.Base.Common.Interfaces.Entities.Visit
{
    using System;
    using Ivh.Common.Base.Enums;
    using Ivh.Common.VisitPay.Enums;

    //Ultimately this is the interface to the Visit table
    public interface IVisit
    {
        int VisitId { get; }
        decimal CurrentBalance { get; }
        VisitStateEnum CurrentVisitState { get; }
        string BillingApplication { get; }
        int AgingCount { get; }
        bool VpEligible { get; }
        bool BillingHold { get; }
        bool Redact { get; }
    }

    public interface IBalanceTransferVisit : IVisit
    {
        BalanceTransferStatusEnum? BalanceTransferStatusEnum { get; }
        int? ServiceGroupId { get; }
    }

    public interface IPaymentAllocationVisit : IVisit
    {
        string SourceSystemKey { get; }
        string MatchedSourceSystemKey { get; }
        int? BillingSystemId { get; }
        int? MatchedBillingSystemId { get; }
    }

    public interface IPaymentVisit : IVisit
    {
        int VpGuarantorId { get; }
        DateTime InsertDate { get; }
        bool IsMaxAge { get; }
    }

    public interface IFinancePlanVisitVisit : IVisit
    {
        bool InterestZero { get; }
        bool IsMaxAge { get; }
        bool IsUnmatched { get; }
    }

    public interface IOutboundMessageVisit : IVisit
    {
        void InitializeActiveOutboundMessage();
    }
}
﻿namespace Ivh.Application.Base.Common.Interfaces.Entities.HsVisit
{
    using System;

    /// <summary>
    /// interface to the base.Visit table
    /// </summary>
    public interface IHsVisit : IMatchPatient
    {
        int VisitId { get; set; }
        int HsGuarantorId { get; set; }
        DateTime? DataChangeDate { get; set; }
    }

    public interface IMatchPatient
    {
        DateTime? PatientDOB { get; set; }
    }
}

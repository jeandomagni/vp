﻿namespace Ivh.Application.Base.Common.Interfaces
{
    public interface IVisitBalanceDto
    {
        int VisitId { get; }
        decimal CurrentBalance { get; }
        decimal UnclearedPaymentsSum { get; set; }
        decimal UnclearedBalance { get; set; }
        decimal InterestAssessed { get; set; }
        decimal InterestDue { get; set; }
        decimal InterestPaid { get; set; }
    }
}
﻿namespace Ivh.Application.Base.Common.Interfaces
{
    using System;
    using Domain.EventJournal.Interfaces;
    using Domain.Logging.Interfaces;
    using Domain.Settings.Interfaces;
    using Ivh.Common.Base.Utilities.Helpers;
    using Ivh.Common.Cache;
    using Ivh.Common.ServiceBus.Interfaces;

    public interface IApplicationServiceCommonService
    {
        Lazy<IApplicationSettingsService> ApplicationSettingsService { get; }
        Lazy<IFeatureService> FeatureService { get; }
        Lazy<IClientService> ClientService { get; }
        Lazy<ILoggingService> LoggingService { get; }
        Lazy<IEventJournalService> Journal { get; }
        Lazy<IBus> Bus { get; }
        Lazy<IDistributedCache> Cache { get; }
        Lazy<TimeZoneHelper> TimeZoneHelper { get; }
        Lazy<IInstanceCache> InstanceCache { get; }
    }
}

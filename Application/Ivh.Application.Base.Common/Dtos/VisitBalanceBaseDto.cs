﻿namespace Ivh.Application.Base.Common.Dtos
{
    using Interfaces;

    public class VisitBalanceBaseDto : IVisitBalanceDto
    {
        public int VisitId { get; set; }

        public decimal CurrentBalance { get; set; }

        public decimal UnclearedPaymentsSum { get; set; }

        public decimal UnclearedBalance { get; set; }

        public decimal InterestDue { get; set; }

        public decimal InterestAssessed { get; set; }

        public decimal InterestPaid { get; set; }

        public virtual decimal TotalBalance => this.CurrentBalance + this.UnclearedPaymentsSum + this.InterestDue;
    }
}
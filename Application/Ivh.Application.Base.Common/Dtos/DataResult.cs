﻿namespace Ivh.Application.Base.Common.Dtos
{
    public class DataResult<T, TResult>
    {
        public DataResult()
        {
        }

        public DataResult(T data, TResult result)
        {
            this.Result = result;
            this.Data = data;
        }

        public TResult Result { get; set; }
        public T Data { get; set; }
    }
}
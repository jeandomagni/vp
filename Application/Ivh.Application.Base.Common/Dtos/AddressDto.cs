﻿namespace Ivh.Application.Base.Common.Dtos
{
    public class AddressDto
    {
        public string AddressStreet1 { get; set; }

        public string AddressStreet2 { get; set; }

        public string City { get; set; }

        public string StateProvince { get; set; }

        public string PostalCode { get; set; }

        public string UspsAddressStreet1 { get; set; }
        public string UspsAddressStreet2 { get; set; }
        public string UspsCity { get; set; }
        public string UspsStateProvince { get; set; }
        public string UspsPostalCode { get; set; }

        public bool IncludesUspsStandardizedAddress { get; set; }
        public bool IsUspsFormatSelected { get; set; }
    }
}
﻿namespace Ivh.Application.Enterprise.Monitoring.Modules
{
    using Autofac;
    using Common.Monitoring.Interfaces;
    using Services;

    public class Module : Autofac.Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<MonitoringApplicationService>().As<IMonitoringApplicationService>();
        }
    }
}

﻿namespace Ivh.Application.Enterprise.Monitoring.Services
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Common.Monitoring.Interfaces;
    using Domain.Enterprise.Monitoring.Interfaces;
    using Ivh.Common.JobRunner.Common.Interfaces;
    using Ivh.Common.VisitPay.Jobs;

    public class MonitoringApplicationService : IMonitoringApplicationService
    {
        private readonly Lazy<IMonitoringService> _monitoringService;

        public MonitoringApplicationService(
            Lazy<IMonitoringService> monitoringService)
        {
            this._monitoringService = monitoringService;
        }

        public void PublishGauges(IEnumerable<string> gaugeNames)
        {
            this._monitoringService.Value.PublishGauges(gaugeNames);
        }

        public void PublishGaugeSets(IEnumerable<string> gaugeSetNames)
        {
            this._monitoringService.Value.PublishGaugeSets(gaugeSetNames);
        }

        void IJobRunnerService<PublishGaugesByNameJobRunner>.Execute(DateTime begin, DateTime end, string[] args)
        {
            if (args != null && args.Length > 1)
            {
                this.PublishGauges(args.Skip(1));
            }
        }

        void IJobRunnerService<PublishGaugeSetsByNameJobRunner>.Execute(DateTime begin, DateTime end, string[] args)
        {
            if (args != null && args.Length > 1)
            {
                this.PublishGaugeSets(args.Skip(1));
            }
        }
    }
}

﻿namespace Ivh.Application.Enterprise.Api.Services
{
    using System;
    using AutoMapper;
    using Common.Api.Dtos;
    using Common.Api.Interfaces;
    using Domain.Enterprise.Api.Interfaces;
    using Ivh.Common.Base.Enums;
    using Ivh.Common.VisitPay.Enums;

    public class ApiApplicationService : IApiApplicationService
    {
        private readonly Lazy<IApiService> _apiService;

        public ApiApplicationService(
            Lazy<IApiService> apiService)
        {
            this._apiService = apiService;
        }
        
        public ApplicationRegistrationDto GetApplicationRegistration(ApiEnum api, string appId)
        {
            return Mapper.Map<ApplicationRegistrationDto>(this._apiService.Value.GetApplicationRegistration(api, appId));
        }
    }
}

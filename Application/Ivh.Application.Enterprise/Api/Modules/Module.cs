﻿namespace Ivh.Application.Enterprise.Api.Modules
{
    using Api.Services;
    using Autofac;
    using Common.Api.Interfaces;

    public class Module : Autofac.Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<ApiApplicationService>().As<IApiApplicationService>();
        }
    }
}

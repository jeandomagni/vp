﻿namespace Ivh.Application.Enterprise.Api.Mappings
{
    using AutoMapper;
    using Common.Api.Dtos;
    using Domain.Enterprise.Api.Entities;
    using Ivh.Common.Base.Utilities.Extensions;

    public class AutomapperProfile : Profile
    {
        public AutomapperProfile()
        {
            this.CreateMapBidirectional<ApplicationRegistration, ApplicationRegistrationDto>();
           
        }
    }
}
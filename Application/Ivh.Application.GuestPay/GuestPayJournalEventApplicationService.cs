namespace Ivh.Application.GuestPay
{
    using System;
    using System.Collections.Generic;
    using Base.Common.Interfaces;
    using Base.Services;
    using Common.Interfaces;
    using Domain.GuestPay.Interfaces;
    using Ivh.Common.EventJournal;
    using Ivh.Common.EventJournal.Interfaces;
    using Ivh.Common.EventJournal.Templates.Guarantor;
    using Ivh.Common.EventJournal.Templates.SystemAction;
    using Ivh.Common.VisitPay.Constants;

    public class GuestPayJournalEventApplicationService : ApplicationService, IGuestPayJournalEventApplicationService
    {
        private readonly Lazy<IEventJournalService> _eventJournalService;

        public GuestPayJournalEventApplicationService(
            Lazy<IApplicationServiceCommonService> applicationServiceCommonService,
            Lazy<IEventJournalService> eventJournalService) : base(applicationServiceCommonService)
        {
            this._eventJournalService = eventJournalService;
        }

        #region "Journal Events"

        public void LogAuthenticationFailure(JournalEventHttpContextDto context, string attemptedGuarantorSourceSystemKey, string attemptedGuarantorLastName)
        {
            GuestPayContext guestPayContext = this.GetGuestPayContext(context, attemptedGuarantorSourceSystemKey, attemptedGuarantorLastName);

            this._eventJournalService.Value.AddEvent<EventGuarantorGuestPayAuthenticationFailed, JournalEventParameters>(
                EventGuarantorGuestPayAuthenticationFailed.GetParameters
                (
                    hsGuarantorId: guestPayContext.UserName
                ),
                guestPayContext.JournalEventUserContext,
                guestPayContext.JournalEventContext
            );
        }

        public void LogAuthenticationSuccess(JournalEventHttpContextDto context, int paymentUnitAuthenticationId, string paymentUnitSourceSystemKey, string guarantorLastName)
        {
            GuestPayContext guestPayContext = this.GetGuestPayContext(context, paymentUnitSourceSystemKey, guarantorLastName);

            this._eventJournalService.Value.AddEvent<EventGuarantorGuestPayAuthenticationSuccess, JournalEventParameters>(
                EventGuarantorGuestPayAuthenticationSuccess.GetParameters
                (
                    hsGuarantorId: guestPayContext.UserName
                ),
                guestPayContext.JournalEventUserContext,
                guestPayContext.JournalEventContext
            );
        }

        public void LogAvsFailure(JournalEventHttpContextDto context, int paymentUnitAuthenticationId, string paymentUnitSourceSystemKey, string guarantorLastName, IReadOnlyDictionary<string, string> processorResponse)
        {
            GuestPayContext guestPayContext = this.GetGuestPayContext(context, paymentUnitSourceSystemKey, guarantorLastName);

            this._eventJournalService.Value.AddEvent<EventGuarantorGuestPayPaymentMethodVerifyFailure, JournalEventParameters>(
                EventGuarantorGuestPayPaymentMethodVerifyFailure.GetParameters
                (
                    hsGuarantorId: guestPayContext.UserName,
                    paymentAuthenticationId: paymentUnitAuthenticationId.ToString()
                ),
                guestPayContext.JournalEventUserContext,
                guestPayContext.JournalEventContext
            );
        }

        public void LogReceiptDownload(JournalEventHttpContextDto context, int paymentUnitAuthenticationId, string paymentUnitSourceSystemKey, string guarantorLastName, string receiptId)
        {
            GuestPayContext guestPayContext = this.GetGuestPayContext(context, paymentUnitSourceSystemKey, guarantorLastName);

            this._eventJournalService.Value.AddEvent<EventGuarantorGuestPayReceiptDownload, JournalEventParameters>(
                EventGuarantorGuestPayReceiptDownload.GetParameters
                (
                    hsGuarantorId: guestPayContext.UserName,
                    receiptId: receiptId
                ),
                guestPayContext.JournalEventUserContext,
                guestPayContext.JournalEventContext
            );
        }

        public void LogPaymentMethodFailure(JournalEventHttpContextDto context, int paymentUnitAuthenticationId, string paymentUnitSourceSystemKey, string guarantorLastName)
        {
            GuestPayContext guestPayContext = this.GetGuestPayContext(context, paymentUnitSourceSystemKey, guarantorLastName);

            this._eventJournalService.Value.AddEvent<EventGuarantorGuestPayPaymentMethodVerifyFailure, JournalEventParameters>(
                EventGuarantorGuestPayPaymentMethodVerifyFailure.GetParameters
                (
                    hsGuarantorId: guestPayContext.UserName,
                   paymentAuthenticationId: paymentUnitAuthenticationId.ToString()
                ),
                guestPayContext.JournalEventUserContext,
                guestPayContext.JournalEventContext
            );
        }

        public void LogSessionTimeout(JournalEventHttpContextDto context, int paymentUnitAuthenticationId, string paymentUnitSourceSystemKey)
        {
            GuestPayContext guestPayContext = this.GetGuestPayContext(context, paymentUnitSourceSystemKey, null);

            this._eventJournalService.Value.AddEvent<EventSystemActionGuestPaySessionTimeOut, JournalEventParameters>(
                EventSystemActionGuestPaySessionTimeOut.GetParameters
                (
                    hsGuarantorId: guestPayContext.UserName
                ),
                guestPayContext.JournalEventUserContext,
                guestPayContext.JournalEventContext
            );
        }

        #endregion

        #region Helper Methods"

        private string GetUserName(string paymentUnitSourceSystemKey, string guarantorLastName)
        {
            return string.Concat(paymentUnitSourceSystemKey ?? "", "|", guarantorLastName ?? "");
        }

        private GuestPayContext GetGuestPayContext(JournalEventHttpContextDto context, string paymentUnitSourceSystemKey, string guarantorLastName)
        {
            return new GuestPayContext()
            {
                JournalEventUserContext = JournalEventUserContext.BuildSystemUserContext(context),
                JournalEventContext = new JournalEventContext() { VisitPayUserId = SystemUsers.SystemUserId },
                UserName = this.GetUserName(paymentUnitSourceSystemKey, guarantorLastName)
            };
        }

        internal class GuestPayContext
        {
            public JournalEventUserContext JournalEventUserContext { get; set; }
            public JournalEventContext JournalEventContext { get; set; }
            public string UserName { get; set; }
        }

        #endregion

    }
}
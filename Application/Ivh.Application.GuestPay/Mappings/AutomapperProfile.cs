﻿namespace Ivh.Application.GuestPay.Mappings
{
    using System.Web;
    using AutoMapper;
    using Common.Dtos;
    using Domain.GuestPay.Entities;
    using Ivh.Common.Base.Utilities.Helpers;
    using Ivh.Common.EventJournal;

    public class AutomapperProfile : Profile
    {
        public AutomapperProfile()
        {
            this.CreateMap<Payment, Payment>()
                .ForMember(dest => dest.PaymentId, opts => opts.UseValue(0));

            this.CreateMap<PaymentSubmitDto, Payment>()
                .ForMember(dest => dest.BankAccountNumber, opts => opts.MapFrom(src => MapTrimmed(src.BankAccountNumber)))
                .ForMember(dest => dest.BankRoutingNumber, opts => opts.MapFrom(src => MapTrimmed(src.BankRoutingNumber)))
                .ForMember(dest => dest.BillingId, opts => opts.MapFrom(src => MapTrimmed(src.BillingId)))
                .ForMember(dest => dest.GatewayToken, opts => opts.MapFrom(src => MapTrimmed(src.GatewayToken)))
                .ForMember(dest => dest.TransactionId, opts => opts.Ignore());

            this.CreateMap<Payment, PaymentDto>()
                .ForMember(dest => dest.TransactionId, opts => opts.MapFrom(x => x.TransactionId))
                .ForMember(dest => dest.AuthCode, opts => opts.MapFrom(x => x.AuthCode));

            this.CreateMap<PaymentUnitAuthentication, PaymentUnitAuthenticationDto>()
                .ForMember(dest => dest.Balance, opts => opts.MapFrom(src => src.PaymentUnitBalance.Balance));

            this.CreateMap<PaymentUnitMatchDto, PaymentUnitAuthentication>()
                .ForMember(dest => dest.PaymentUnitSourceSystemKey, opts => opts.MapFrom(src => src.SourceSystemKey))
                .ForMember(dest => dest.StatementIdentifierId, opts => opts.MapFrom(src => string.IsNullOrEmpty(src.StatementIdentifierId)? default(int) : int.Parse(src.StatementIdentifierId)));

            this.CreateMap<HttpContextWrapper, JournalEventHttpContextDto>()
                .ForMember(dest => dest.Url, opts => opts.MapFrom(src => src.Request.Url.AbsolutePath))
                .ForMember(dest => dest.UserHostAddress, opts => opts.MapFrom(src => src.Request.UserHostAddress))
                .ForMember(dest => dest.UserAgent, opts => opts.MapFrom(src => src.Request.UserAgent))
                .ForMember(dest => dest.DeviceType, opts => opts.MapFrom(src => DeviceTypeHelper.GetDeviceType(src.Request.UserAgent)));
        }

        private static string MapTrimmed(string value)
        {
            return string.IsNullOrEmpty(value) ? null : value.Trim();
        }
    }
}
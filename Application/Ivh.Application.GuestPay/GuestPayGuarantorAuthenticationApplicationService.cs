﻿namespace Ivh.Application.GuestPay
{
    using System;
    using Common.Interfaces;
    using Domain.GuestPay.Interfaces;
    using System.Collections.Generic;
    using Base.Services;
    using Common.Dtos;
    using Ivh.Application.Base.Common.Interfaces;

    public class GuestPayGuarantorAuthenticationApplicationService : ApplicationService, IGuestPayGuarantorAuthenticationApplicationService
    {
        private readonly Lazy<IGuarantorAuthenticationService> _guarantorAuthenticationService;

        public GuestPayGuarantorAuthenticationApplicationService(
            Lazy<IApplicationServiceCommonService> applicationServiceCommonService,
            Lazy<IGuarantorAuthenticationService> guarantorAuthenticationService) : base(applicationServiceCommonService)
        {
            this._guarantorAuthenticationService = guarantorAuthenticationService;
        }

        public IList<AuthenticationMatchFieldDto> GetGuestPayGuarantorAuthenticationFields()
        {
            return this._guarantorAuthenticationService.Value.GetGuarantorAuthenticationFields();
        }
    }
}

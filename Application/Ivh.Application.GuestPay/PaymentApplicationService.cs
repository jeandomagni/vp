﻿namespace Ivh.Application.GuestPay
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading;
    using System.Threading.Tasks;
    using AutoMapper;
    using Base.Common.Interfaces;
    using Base.Services;
    using Common.Dtos;
    using Common.Interfaces;
    using Content.Common.Interfaces;
    using Domain.FinanceManagement.Payment.Interfaces;
    using Domain.Guarantor.Interfaces;
    using Domain.GuestPay.Entities;
    using Domain.GuestPay.Interfaces;
    using Domain.Logging.Interfaces;
    using Domain.Settings.Interfaces;
    using Ivh.Common.Base.Utilities.Extensions;
    using Ivh.Common.Base.Utilities.Helpers;
    using Ivh.Common.EventJournal;
    using Ivh.Common.VisitPay.Constants;
    using Ivh.Common.VisitPay.Enums;
    using Ivh.Common.VisitPay.Messages.Communication;
    using Ivh.Common.VisitPay.Messages.Matching.Dtos;


    public class PaymentApplicationService : ApplicationService, IPaymentApplicationService
    {
        private const string AuthenticatePaymentUnitSuccessMetric = "Session.New";

        private readonly Lazy<IPaymentService> _paymentService;
        private readonly Lazy<IPaymentUnitService> _paymentUnitService;
        private readonly Lazy<IGuestPayJournalEventApplicationService> _guestPayJournalEventApplicationService;
        private readonly Lazy<IMetricsProvider> _metricsProvider;
        private readonly Lazy<IMerchantAccountAllocationService> _merchantAccountAllocationService;
        private readonly Lazy<IFeatureService> _featureService;
        private readonly Lazy<IContentApplicationService> _contentApplicationService;
        private readonly Lazy<IGuarantorService> _guarantorService;

        public PaymentApplicationService(
            Lazy<IApplicationServiceCommonService> applicationServiceCommonService,
            Lazy<IPaymentService> paymentService,
            Lazy<IPaymentUnitService> paymentUnitService,
            Lazy<IGuestPayJournalEventApplicationService> guestPayJournalEventApplicationService,
            Lazy<IMetricsProvider> metricsProvider,
            Lazy<IMerchantAccountAllocationService> merchantAccountAllocationService,
            Lazy<IFeatureService> featureService,
            Lazy<IGuarantorService> guarantorService,
            Lazy<IContentApplicationService> contentApplicationService) : base(applicationServiceCommonService)
        {
            this._paymentService = paymentService;
            this._paymentUnitService = paymentUnitService;
            this._guestPayJournalEventApplicationService = guestPayJournalEventApplicationService;
            this._metricsProvider = metricsProvider;
            this._merchantAccountAllocationService = merchantAccountAllocationService;
            this._featureService = featureService;
            this._guarantorService = guarantorService;
            this._contentApplicationService = contentApplicationService;
        }

        public PaymentUnitAuthenticationDto AuthenticatePaymentUnit(PaymentUnitMatchDto paymentUnitMatchDto, JournalEventHttpContextDto context)
        {
            PaymentUnitAuthentication paymentUnit = this.GetPaymentUnitAuthentication(paymentUnitMatchDto);
            if (paymentUnit == null)
            {
                this._guestPayJournalEventApplicationService.Value.LogAuthenticationFailure(context, paymentUnitMatchDto.SourceSystemKey, paymentUnitMatchDto.GuarantorLastName);
                return null;
            }

            this._guestPayJournalEventApplicationService.Value.LogAuthenticationSuccess(context, paymentUnit.PaymentUnitAuthenticationId, paymentUnit.PaymentUnitSourceSystemKey, paymentUnitMatchDto.GuarantorLastName);
            this._metricsProvider.Value.Increment(AuthenticatePaymentUnitSuccessMetric);

            return Mapper.Map<PaymentUnitAuthenticationDto>(paymentUnit);
        }

        private PaymentUnitAuthentication GetPaymentUnitAuthentication(PaymentUnitMatchDto paymentUnitMatchDto)
        {
            if (this.FeatureService.Value.IsFeatureEnabled(VisitPayFeatureEnum.FeatureMatchingApiIsEnabled))
            {
                // still using the PaymentUnitAuthentication table with matching B, as it contains data relevant to GP payment processing which is required downstream of this
                PaymentUnitAuthentication targetPaymentUnit = Mapper.Map<PaymentUnitAuthentication>(paymentUnitMatchDto);
                GuarantorMatchResult matchResult = this.IsGuarantorMatch(targetPaymentUnit);
                GuarantorMatchResultEnum? matchEnum = matchResult?.GuarantorMatchResultEnum;

                // allow payments for guarantors regardless of VP registration status
                if (matchEnum == GuarantorMatchResultEnum.Matched || matchEnum == GuarantorMatchResultEnum.AlreadyRegistered)
                {
                    IList<PaymentUnitAuthentication> paymentUnits = this._paymentUnitService.Value.GetPaymentUnitAuthentications(matchResult.MatchedHsGuarantorSourceSystemKey);

                    IList<PaymentUnitAuthentication> matchingPaymentUnits = paymentUnits.Where(x => x.StatementIdentifierId == targetPaymentUnit.StatementIdentifierId).ToList();

                    // guarantor has multiple visits with the same ssk for the same statementId. probably won't happen.
                    if (matchingPaymentUnits.Count > 1)
                    {
                        Func<string> ambiguousIds = () => string.Join(",", matchingPaymentUnits.Select(x => x.PaymentUnitAuthenticationId));
                        this.LoggingService.Value.Warn(() => $"{nameof(PaymentApplicationService)}::{nameof(this.GetPaymentUnitAuthentications)} - ambiguous payment unit results: {ambiguousIds.Invoke()}");

                        return null;
                    }
                    else
                    {
                        return matchingPaymentUnits.FirstOrDefault();
                    }
                }

                // not found
                return null;
            }
            else
            {
                PaymentUnitAuthentication paymentUnit = this._paymentUnitService.Value.AuthenticatePaymentUnit(Mapper.Map<PaymentUnitAuthentication>(paymentUnitMatchDto));
                return paymentUnit;
            }
        }

        private GuarantorMatchResult IsGuarantorMatch(PaymentUnitAuthentication paymentUnitAuthentication)
        {
            MatchGuarantorDto guarantor = new MatchGuarantorDto
            {
                LastName = paymentUnitAuthentication.GuarantorLastName,
                SourceSystemKey = paymentUnitAuthentication.PaymentUnitSourceSystemKey
            };

            MatchGuestPayAuthenticationDto guestPayAuthentication = new MatchGuestPayAuthenticationDto{ StatementIdentifierId = paymentUnitAuthentication.StatementIdentifierId };

            GuarantorMatchResult result = this._guarantorService.Value.IsGuarantorMatch(new MatchDataDto
            {
                Guarantor = guarantor,
                GuestPayAuthentication = guestPayAuthentication,
                ApplicationEnum = ApplicationEnum.GuestPay
            });

            return result;
        }

        public PaymentMatchUnitResponseDto GetPaymentMatchUnitResponse(string paymentUnitSourceSystemKey)
        {
            PaymentMatchUnitResponseDto paymentUnit = new PaymentMatchUnitResponseDto();
            PaymentUnitAuthenticationDto paymentUnitAuthenticationDto = null;

            if (this.FeatureService.Value.IsFeatureEnabled(VisitPayFeatureEnum.FeaturePaymentApiIsEnabled))
            {
                IList<PaymentUnitAuthenticationDto> paymentUnits = this.GetPaymentUnitAuthentications(paymentUnitSourceSystemKey);
                if (paymentUnits.Count != 1)
                {
                    paymentUnit = new PaymentMatchUnitResponseDto()
                    {
                        StatusCode = paymentUnits.Count == 0 ? PaymentMatchUnitResponseStatusEnum.NotFound : PaymentMatchUnitResponseStatusEnum.Ambiguous,
                        IsValid = false,
                        PaymentUnitAuthenticationId = default(int)
                    };
                }
                else
                {
                    paymentUnitAuthenticationDto = paymentUnits.First();
                }

                if (paymentUnitAuthenticationDto != null)
                {
                    paymentUnit = new PaymentMatchUnitResponseDto()
                    {
                        StatusCode = PaymentMatchUnitResponseStatusEnum.Success,
                        IsValid = true,
                        PaymentUnitAuthenticationId = paymentUnitAuthenticationDto.PaymentUnitAuthenticationId,
                        Balance = paymentUnitAuthenticationDto.Balance
                    };
                }
            }
            else
            {
                paymentUnit = new PaymentMatchUnitResponseDto()
                {
                    StatusCode = PaymentMatchUnitResponseStatusEnum.PaymentApiNotEnabled,
                    IsValid = false,
                    PaymentUnitAuthenticationId = default(int)
                };
            }
            this.IncrementMetricGuarantorLookupPaymentApi(paymentUnit);

            return paymentUnit;
        }

        public IList<PaymentUnitAuthenticationDto> GetPaymentUnitAuthentications(string paymentUnitSourceSystemKey)
        {
            IList<PaymentUnitAuthentication> paymentUnitAuthentications = this._paymentUnitService.Value.GetPaymentUnitAuthentications(paymentUnitSourceSystemKey);
            return Mapper.Map<IList<PaymentUnitAuthenticationDto>>(paymentUnitAuthentications);
        }

        public PaymentDto GetPayment(int paymentId, string enteredAuthenticationKey)
        {
            return Mapper.Map<PaymentDto>(this._paymentService.Value.GetPayment(paymentId, enteredAuthenticationKey));
        }

        public async Task<ProcessPaymentResponseDto> ProcessRemotePayment(RemotePaymentInfoDto remotePaymentInfo, JournalEventHttpContextDto context)
        {
            ProcessPaymentResponseDto paymentResponseDto = new ProcessPaymentResponseDto()
            {
                IsSuccess = false,
                ErrorMessages = new List<string>() { "Payment API feature is not enabled" },
                StatusCode = ProcessPaymentResponseStatusEnum.PaymentApiNotEnabled,
                Payment = null
            };

            if (this.FeatureService.Value.IsFeatureEnabled(VisitPayFeatureEnum.FeaturePaymentApiIsEnabled))
            {
                paymentResponseDto.ErrorMessages = new List<string>();
                paymentResponseDto.StatusCode = ProcessPaymentResponseStatusEnum.Unknown;

                if (!string.IsNullOrWhiteSpace(remotePaymentInfo?.PaymentUnitSourceSystemKey))
                {
                    int expirationMonth = 0;
                    int expirationYear = 0;

                    try
                    {
                        string expirationMonthText = remotePaymentInfo?.ExpDate.Substring(0, 2);
                        string expirationYearText = remotePaymentInfo?.ExpDate.Substring(5, 2);
                        expirationMonth = int.Parse(expirationMonthText ?? "");
                        expirationYear = int.Parse(expirationYearText ?? "");
                        paymentResponseDto.IsSuccess = true;
                        paymentResponseDto.ErrorMessages = new List<string>();
                    }
                    catch (Exception e)
                    {
                        paymentResponseDto.IsSuccess = false;
                        paymentResponseDto.ErrorMessages.Add("ExpDate is not formatted correctly");
                        paymentResponseDto.StatusCode = ProcessPaymentResponseStatusEnum.ExpirationDateBadFormat;
                    }

                    if (!paymentResponseDto.IsError)
                    {
                        PaymentSubmitDto paymentSubmitDto = new PaymentSubmitDto()
                        {
                            PaymentSystemType = PaymentSystemTypeEnum.PaymentApi,
                            PaymentMethodType = remotePaymentInfo?.PaymentMethodType ?? PaymentMethodTypeEnum.Unknown,
                            PaymentAmount = remotePaymentInfo?.PaymentAmount ?? 0m,
                            EmailAddress = "", //Not nullable in guestpay.Payment table
                            IpAddress = string.IsNullOrWhiteSpace(remotePaymentInfo?.OriginalRequestIpAddress) ? remotePaymentInfo?.OriginalRequestIpAddress : context.UserHostAddress,

                            // CC Fields
                            CardAccountHolderName = remotePaymentInfo.NameOnCard,
                            BillingId = remotePaymentInfo?.BillingId,
                            GatewayToken = null,
                            LastFour = remotePaymentInfo?.LastFour,
                            ExpDate = remotePaymentInfo?.ExpDate,
                            SecurityCode = null,
                            AddressStreet1 = null,
                            AddressStreet2 = null,
                            City = null,
                            State = null,
                            Zip = null,
                            IsCountryUsa = true,
                            ExpDateM = expirationMonth,
                            ExpDateY = expirationYear,

                            // ACH Fields
                            BankName = null,
                            BankAccountNumber = null,
                            BankRoutingNumber = null,
                            BankAccountHolderFirstName = null,
                            BankAccountHolderLastName = null,
                        };

                        paymentResponseDto = await this.ProcessPaymentAsync(paymentSubmitDto, remotePaymentInfo.PaymentUnitAuthenticationId, remotePaymentInfo.PaymentUnitSourceSystemKey, context);

                        if (paymentResponseDto.IsError)
                        {
                            this.LoggingService.Value.Warn(() => $"PaymentController::SubmitPayment - ProcessPaymentAsync unsuccessful - {string.Join(", ", paymentResponseDto.ErrorMessages)} - for PaymentUnitAuthenticationId: {remotePaymentInfo.PaymentUnitAuthenticationId}, PaymentUnitSourceSystemKey: {remotePaymentInfo.PaymentUnitSourceSystemKey}");
                        }
                        else
                        {
                            paymentResponseDto.StatusCode = ProcessPaymentResponseStatusEnum.Success;
                            this._metricsProvider.Value.Increment(Metrics.Increment.SecurePan.PaymentMethod.Success);
                        }

                    }
                }
                else
                {
                    string errorMessage = await this._contentApplicationService.Value.GetTextRegionAsync(TextRegionConstants.GuestPayUnableToFindMatchingGuarantor);
                    paymentResponseDto = new ProcessPaymentResponseDto()
                    {
                        IsSuccess = false,
                        ErrorMessages = new List<string>() { errorMessage },
                        StatusCode = ProcessPaymentResponseStatusEnum.MissingGuarantor,
                        Payment = null
                    };
                }
            }

            return paymentResponseDto;
        }

        public async Task<ProcessPaymentResponseDto> ProcessPaymentAsync(PaymentSubmitDto paymentSubmitDto, int paymentUnitAuthenticationId, string paymentUnitSourceSystemKey, JournalEventHttpContextDto context)
        {
            Payment payment = Mapper.Map<Payment>(paymentSubmitDto);
            payment.EnteredAuthenticationKey = paymentUnitSourceSystemKey;

            PaymentUnitAuthentication paymentUnit = this._paymentUnitService.Value.GetPaymentUnitAuthentication(paymentUnitAuthenticationId, paymentUnitSourceSystemKey);
            string invalidSubmissionErrorMessage = await this._contentApplicationService.Value.GetTextRegionAsync(TextRegionConstants.GuestPayInvalidSubmission);
            if (paymentUnit == null)
            {
                this.LoggingService.Value.Warn(() => $"PaymentApplicationService::ProcessPaymentAsync - no valid payment unit for PaymentUnitAuthenticationId: {paymentUnitAuthenticationId}, PaymentUnitSourceSystemKey: {paymentUnitSourceSystemKey}");
                return new ProcessPaymentResponseDto(false, invalidSubmissionErrorMessage, ProcessPaymentResponseStatusEnum.InvalidSubmission);
            }

            if (payment.PaymentAmount > paymentUnit.PaymentUnitBalance.Balance)
            {
                this.LoggingService.Value.Warn(() => $"{nameof(PaymentApplicationService)}::{nameof(this.ProcessPaymentAsync)} {nameof(ProcessPaymentResponseStatusEnum.AmountGreaterThanBalance)} {nameof(paymentUnit.PaymentUnitBalance.Balance)} = {paymentUnit?.PaymentUnitBalance?.Balance} {nameof(payment.PaymentAmount)} = {payment?.PaymentAmount}");
                this.IncrementMetricWithBalanceMismatch(payment);
                return new ProcessPaymentResponseDto(false, invalidSubmissionErrorMessage, ProcessPaymentResponseStatusEnum.AmountGreaterThanBalance);
            }

            payment.GuarantorFirstName = paymentUnit.GuarantorFirstName;
            payment.GuarantorLastName = paymentUnit.GuarantorLastName;
            payment.GuarantorNumber = paymentUnit.PaymentUnitSourceSystemKey;
            payment.SourceSystemKey = paymentUnit.PaymentUnitSourceSystemKey;
            payment.PaymentUnitAuthentication = paymentUnit;
            payment.StatementIdentifierId = paymentUnit.StatementIdentifierId;

            if (this._featureService.Value.IsFeatureEnabled(VisitPayFeatureEnum.MerchantAccountSplittingIsEnabled))
            {
                payment.MerchantAccount = this._merchantAccountAllocationService.Value.GetMerchantAccountByServiceGroupId(paymentUnit.ServiceGroupId);
            }

            ProcessPaymentResponseDto response = this.ValidatePayment(payment);
            if (response.IsError)
            {
                return response;
            }

            try
            {
                if (await this._paymentService.Value.ProcessPaymentAsync(payment, context))
                {
                    if (payment.EmailAddress.IsEmailAddress())
                    {
                        this.SendPaymentConfirmationEmail(payment);
                    }

                    this.IncrementMetricWithSuccessfulPayment(payment);

                    return new ProcessPaymentResponseDto(true)
                    {
                        Payment = Mapper.Map<PaymentDto>(payment)
                    };
                }

                // treat any other case as a failure for statistical purposes
                this.IncrementMetricWithFailurePayment(payment);

                response = new ProcessPaymentResponseDto(false);

                switch (payment.PaymentStatus)
                {
                    case PaymentStatusEnum.ClosedFailed:
                        string paymentFailedMessage = await this._contentApplicationService.Value.GetTextRegionAsync(TextRegionConstants.GuestPayPaymentFailed);
                        response.ErrorMessages.Add(paymentFailedMessage);
                        response.StatusCode = ProcessPaymentResponseStatusEnum.PaymentFailed;
                        break;
                    case PaymentStatusEnum.ActiveGatewayError:
                        string systemNotRespondingMessage = await this._contentApplicationService.Value.GetTextRegionAsync(TextRegionConstants.GuestPayPaymentSystemNotResponding);
                        response.ErrorMessages.Add(systemNotRespondingMessage);
                        response.StatusCode = ProcessPaymentResponseStatusEnum.SystemNotRespondingActiveGatewayErrorTryAgain;
                        break;
                    case PaymentStatusEnum.ActivePendingLinkFailure:
                        string potentialChargeErrorMessage = await this._contentApplicationService.Value.GetTextRegionAsync(TextRegionConstants.GuestPayPaymentFailedPotentialCharge);
                        response.ErrorMessages.Add(potentialChargeErrorMessage);
                        response.StatusCode = ProcessPaymentResponseStatusEnum.ActivePendingLinkFailureMaybeChargedCallSupport;
                        break;
                    default:
                        string paymentSystemNotRespondingMessage = await this._contentApplicationService.Value.GetTextRegionAsync(TextRegionConstants.GuestPayPaymentSystemNotResponding);
                        response.ErrorMessages.Add(paymentSystemNotRespondingMessage);
                        response.StatusCode = ProcessPaymentResponseStatusEnum.SystemNotRespondingTryAgainCallSupport;
                        break;
                }

                return response;
            }
            catch (Exception ex)
            {
                string potentialChargeErrorMessage = await this._contentApplicationService.Value.GetTextRegionAsync(TextRegionConstants.GuestPayPaymentFailedPotentialCharge);
                this.LoggingService.Value.Fatal(() => "GuestPay.PaymentApplicationService.ProcessPaymentAsync - {0}", ExceptionHelper.AggregateExceptionToString(ex));
                this.IncrementMetricWithFailurePayment(payment);
                return new ProcessPaymentResponseDto(false, potentialChargeErrorMessage, ProcessPaymentResponseStatusEnum.FatalErrorMaybeChargedCallSupport);
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="payment"></param>
        /// <returns></returns>
        private ProcessPaymentResponseDto ValidatePayment(Payment payment)
        {
            if (payment.IsCardExpired())
            {
                string message = this._contentApplicationService.Value.GetTextRegion(TextRegionConstants.GuestPayPaymentMethodExpired);
                return new ProcessPaymentResponseDto(false, message, ProcessPaymentResponseStatusEnum.PaymentMethodExpired);
            }

            return new ProcessPaymentResponseDto(true);
        }

        private void SendPaymentConfirmationEmail(Payment payment)
        {
            this.Bus.Value.PublishMessage(new SendGuestPayPaymentConfirmationEmailMessage
            {
                EmailAddress = payment.EmailAddress,
                GuarantorFirstName = payment.PaymentUnitAuthentication.GuarantorFirstName,
                Locale = Thread.CurrentThread.GetLocale(),
                PaymentUnitId = payment.PaymentUnitAuthentication.PaymentUnitAuthenticationId,
                TransactionId = payment.TransactionId
            }).Wait();
        }

        private void IncrementMetricWithSuccessfulPayment(Payment payment)
        {
            if (payment == null)
            {
                return;
            }
            string statName;
            if (payment.PaymentSystemType == PaymentSystemTypeEnum.PaymentApi)
            {
                statName = (payment.IsAchType) ? Metrics.Increment.GuestPay.PaymentApi.AchPaymentSuccessful : Metrics.Increment.GuestPay.PaymentApi.CreditCardPaymentSuccessful;
            }
            else
            {
                statName = (payment.IsAchType) ? Metrics.Increment.GuestPay.Payment.AchPaymentSuccessful : Metrics.Increment.GuestPay.Payment.CreditCardPaymentSuccessful;
            }
            this._metricsProvider.Value.Increment(statName);
        }

        private void IncrementMetricWithFailurePayment(Payment payment)
        {
            if (payment == null)
            {
                return;
            }

            string statName;
            if (payment.PaymentSystemType == PaymentSystemTypeEnum.PaymentApi)
            {
                statName = (payment.IsAchType) ? Metrics.Increment.GuestPay.PaymentApi.AchPaymentFailure : Metrics.Increment.GuestPay.PaymentApi.CreditCardPaymentFailure;
            }
            else
            {
                statName = (payment.IsAchType) ? Metrics.Increment.GuestPay.Payment.AchPaymentFailure : Metrics.Increment.GuestPay.Payment.CreditCardPaymentFailure;
            }
            this._metricsProvider.Value.Increment(statName);
        }

        private void IncrementMetricWithBalanceMismatch(Payment payment)
        {
            if (payment == null)
            {
                return;
            }
            this._metricsProvider.Value.Increment(Metrics.Increment.GuestPay.PaymentApi.PaymentApiAmountMismatch);
        }

        private void IncrementMetricGuarantorLookupPaymentApi(PaymentMatchUnitResponseDto paymentUnit)
        {
            if (paymentUnit == null)
            {
                return;
            }

            this._metricsProvider.Value.Increment(paymentUnit.IsValid ?
                Metrics.Increment.GuestPay.PaymentApi.GuarantorPaymentLookupSuccessful :
                Metrics.Increment.GuestPay.PaymentApi.GuarantorPaymentLookupFailure);
        }
    }
}
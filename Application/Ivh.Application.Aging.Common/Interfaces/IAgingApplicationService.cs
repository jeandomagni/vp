﻿namespace Ivh.Application.Aging.Common.Interfaces
{
    using System;
    using Ivh.Common.JobRunner.Common.Interfaces;
    using Ivh.Common.ServiceBus.Interfaces;
    using Ivh.Common.VisitPay.Jobs;
    using Ivh.Common.VisitPay.Messages.Aging;
    using Ivh.Common.VisitPay.Messages.Core;
    using Ivh.Common.VisitPay.Messages.HospitalData;

    public interface IAgingApplicationService: 
        IUniversalConsumerService<ChangeSetAggregatedMessage>,
        IUniversalConsumerService<OutboundVisitMessageList>,
        IUniversalConsumerService<OutboundVisitMessage>,
        IUniversalConsumerService<VisitCommunicationMessage>,
        IUniversalConsumerService<AgeVisitMessage>,
        IUniversalConsumerService<SetVisitAgeMessage>,
        IUniversalConsumerService<VisitReconciliationBalanceChangeMessage>,
        IUniversalConsumerService<VisitReconciliationBalanceChangeMessageList>,
        IUniversalConsumerService<HsGuarantorVisitUnmatchMessage>,
        IUniversalConsumerService<GuarantorChangedMessage>,
        IJobRunnerService<QueueAgeVisitsJobRunner>
    {
        void ProcessSetVisitAgeMessage(SetVisitAgeMessage message);
        void ProcessAgeVisitMessage(AgeVisitMessage message);
        void ProcessChangeSetAggregatedMessage(ChangeSetAggregatedMessage message);
        void ProcessOutboundVisitMessageList(OutboundVisitMessageList message);
        void QueueAgeVisits(DateTime dateToProcess);
    }
} 

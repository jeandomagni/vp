﻿namespace Ivh.Application.Enterprise.Common.Monitoring.Interfaces
{
    using System.Collections.Generic;
    using Ivh.Common.Base.Interfaces;
    using Ivh.Common.JobRunner.Common.Interfaces;
    using Ivh.Common.VisitPay.Jobs;

    public interface IMonitoringApplicationService : IApplicationService,
        IJobRunnerService<PublishGaugesByNameJobRunner>,
        IJobRunnerService<PublishGaugeSetsByNameJobRunner>

    {
        void PublishGauges(IEnumerable<string> gaugeNames);
        void PublishGaugeSets(IEnumerable<string> gaugeSetNames);
    }
}

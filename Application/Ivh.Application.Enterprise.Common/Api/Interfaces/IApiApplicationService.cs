﻿namespace Ivh.Application.Enterprise.Common.Api.Interfaces
{
    using Dtos;
    using Ivh.Common.Base.Enums;
    using Ivh.Common.Base.Interfaces;
    using Ivh.Common.VisitPay.Enums;

    public interface IApiApplicationService : IApplicationService
    {
        ApplicationRegistrationDto GetApplicationRegistration(ApiEnum api, string appId);
    }
}

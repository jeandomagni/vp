﻿namespace Ivh.Application.Enterprise.Common.Api.Dtos
{
    using System;
    using Ivh.Common.Base.Enums;
    using Ivh.Common.Base.Utilities.Extensions;
    using Ivh.Common.VisitPay.Enums;

    public class ApplicationRegistrationDto
    {
        public int ApplicationRegistrationId { get; set; }
        public DateTime InsertDate { get; set; }
        public ApiEnum Api { get; set; }
        public string ApplicationName { get; set; }
        public string AppId { get; set; }
        public string AppKey { get; set; }

        private byte[] _appKeyBytes = null;
        public byte[] AppKeyBytes
        {
            get
            {
                if (this._appKeyBytes == null && this.AppKey.IsNotNullOrEmpty())
                {
                    this._appKeyBytes = Convert.FromBase64String(this.AppKey);
                }
                return this._appKeyBytes;
            }
        }
        public bool Disabled { get; set; }
        public DateTime ValidFrom { get; set; }
        public DateTime ValidUntil { get; set; }

        public bool IsValid
        {
            get
            {
                return !this.Disabled
                       && DateTime.UtcNow.IsBetween(this.ValidFrom, this.ValidUntil);
            }
        }
    }
}

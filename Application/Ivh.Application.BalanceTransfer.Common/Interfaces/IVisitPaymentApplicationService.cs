﻿namespace Ivh.Application.BalanceTransfer.Common.Interfaces
{
    using Ivh.Common.Base.Interfaces;
    using Ivh.Common.ServiceBus.Interfaces;
    using Ivh.Common.VisitPay.Messages.BalanceTransfer;

    public interface IVisitPaymentApplicationService : IApplicationService,
        IUniversalConsumerService<BalanceTransferFinancePlanPaymentSettledMessage>
    {
        void ProcessFinancePlanPayment(BalanceTransferFinancePlanPaymentSettledMessage message);
    }
}
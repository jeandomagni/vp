﻿namespace Ivh.Application.Content.Common.Interfaces
{
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Dtos;
    using Ivh.Common.Base.Interfaces;
    using Ivh.Common.JobRunner.Common.Interfaces;
    using Ivh.Common.VisitPay.Enums;
    using Ivh.Common.VisitPay.Jobs;

    public interface IContentApplicationService : IApplicationService, 
        IJobRunnerService<ContentApiTesterJob>
    {
        CmsVersionDto ApiGetCurrentVersion(CmsRegionEnum cmsRegion, string locale, bool replaceTokens = true, IDictionary<string, string> additionalValues = null);
        Task<CmsVersionDto> GetCurrentVersionAsync(CmsRegionEnum cmsRegion, bool replaceTokens = true, IDictionary<string, string> additionalValues = null);
        CmsVersionDto ApiGetSpecificVersion(CmsRegionEnum cmsRegion, int cmsVersionId, string locale, bool replaceTokens = true, IDictionary<string, string> additionalValues = null);
        Task<CmsVersionDto> GetSpecificVersionAsync(CmsRegionEnum cmsRegion, int cmsVersionId, bool replaceTokens = true, IDictionary<string, string> additionalValues = null);
        Task<CmsRegionEnum?> GetSsoProviderSpecificCmsRegionAsync(SsoProviderEnum ssoProvider, SsoProviderSpecificCmsRegionEnum providerSpecificCmsRegion);
        Task<CmsVersionDto> GetLabelAsync(string cmsRegionName, string defaultLabel);
        CmsVersionDto ApiGetVersionByRegionNameAndType(string cmsRegionName, CmsTypeEnum cmsType, string defaultLabel, string locale);
        Task<CmsVersionDto> GetVersionByRegionNameAndTypeAsync(string cmsRegionName, CmsTypeEnum cmsType, string defaultLabel);
        Task<ContentMenuDto> GetContentMenuAsync(ContentMenuEnum contentMenuEnum, bool isUserAuthenticated);
        IDictionary<int, int> ApiGetCmsVersionNumbers(CmsRegionEnum cmsRegion, string locale);
        Task<IDictionary<int, int>> GetCmsVersionNumbersAsync(CmsRegionEnum cmsRegion);
        IDictionary<string, string> ClientReplacementValues();
        string GetTextRegion(string textRegion, IDictionary<string, string> additionalValues = null);
        Task<string> GetTextRegionAsync(string textRegion, IDictionary<string, string> additionalValues = null);
        Task<IDictionary<string, string>> GetTextRegionsAsync(params string[] textRegions);
        Task<bool> InvalidateCacheAsync();
    }
}

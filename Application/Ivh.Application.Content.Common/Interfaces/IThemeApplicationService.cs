﻿namespace Ivh.Application.Content.Common.Interfaces
{
    using Ivh.Common.Base.Interfaces;

    public interface IThemeApplicationService : IApplicationService
    {
        string GetStylesheet(string requestedFile);
        string GetStylesheetHash(string requestedFile);
    }
}

﻿namespace Ivh.Application.Content.Common.Interfaces
{
    using System.Threading.Tasks;
    using Dtos;
    using Ivh.Common.Base.Interfaces;

    public interface IImageApplicationService : IApplicationService
    {
        Task<ImageDto> GetImageAsync(string fileName);
    }
}

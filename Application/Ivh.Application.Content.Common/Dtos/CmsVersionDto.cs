﻿namespace Ivh.Application.Content.Common.Dtos
{
    using System;

    public class CmsVersionDto
    {
        public int CmsVersionId { get; set; }
        public int CmsRegionId { get; set; }
        public int VersionNum { get; set; }
        public string ContentTitle { get; set; }
        public string ContentBody { get; set; }
        public DateTime UpdatedDate { get; set; }
        public int UpdatedByUserId { get; set; }
        public CmsRegionDto CmsRegion { get; set; }
    }
}

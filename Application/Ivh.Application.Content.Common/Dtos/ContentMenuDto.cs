﻿namespace Ivh.Application.Content.Common.Dtos
{
    using System.Collections.Generic;
    using System.Linq;

    public class ContentMenuDto
    {
        private IList<ContentMenuItemDto> _contentMenuItems;

        public ContentMenuDto()
        {
            this.ContentMenuItems = new List<ContentMenuItemDto>();
        }

        public int ContentMenuId { get; set; }
        public string ContentMenuName { get; set; }

        public IList<ContentMenuItemDto> ContentMenuItems
        {
            get { return this._contentMenuItems.OrderBy(x => x.DisplayOrder).ToList(); }
            set { this._contentMenuItems = value; }
        }
    }
}
﻿namespace Ivh.Application.Content.Common.Dtos
{
    public class ContentDto
    {
        public string ContentTitle { get; set; }

        public string ContentBody { get; set; }
    }
}
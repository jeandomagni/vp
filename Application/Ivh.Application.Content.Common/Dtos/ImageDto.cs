﻿namespace Ivh.Application.Content.Common.Dtos
{
    using System;

    public class ImageDto
    {
        public ImageDto()
        {
            this.Expires = DateTime.UtcNow.AddMinutes(60 * 24);
        }

        public byte[] ImageBytes { get; set; }
        public string ContentType { get; set; }
        public DateTime Expires { get; set; }
    }
}

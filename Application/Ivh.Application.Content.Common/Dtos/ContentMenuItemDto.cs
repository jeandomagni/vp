﻿namespace Ivh.Application.Content.Common.Dtos
{
    public class ContentMenuItemDto
    {
        public int ContentMenuItemId { get; set; }
        public string LinkText { get; set; }
        public string LinkUrl { get; set; }
        public int DisplayOrder { get; set; }
        public bool IsPublic { get; set; }
        public string IconCssClass { get; set; }
        public bool LinkUrlOpensNewWindow { get; set; }
        public ContentMenuDto ContentMenu { get; set; }
    }
}
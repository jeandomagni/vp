﻿namespace Ivh.Application.Content.Common.Dtos
{
    using Ivh.Common.Base.Enums;
    using Ivh.Common.VisitPay.Enums;

    public class CmsRegionDto
    {
        public int CmsRegionId { get; set; }
        public string CmsRegionName { get; set; }
        public CmsTypeEnum CmsType { get; set; }
    }
}
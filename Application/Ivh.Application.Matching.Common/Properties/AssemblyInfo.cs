﻿using Ivh.Common.Assembly.Constants;
using System.Reflection;
using System.Runtime.InteropServices;
using Ivh.Common.Assembly.Attributes;

// General Information about an assembly is controlled through the following
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("Ivh.Application.Matching.Common")]
[assembly: AssemblyDescription("")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany(AssemblyCompany.Ivh)]
[assembly: AssemblyProduct("Ivh.Application.Matching.Common")]
[assembly: AssemblyCopyright(AssemblyCopyright.Ivh)]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]
[assembly: AssemblyApplicationLayer(AssemblyApplicationLayer.Application)]
[assembly: AssemblyDomain(AssemblyDomain.VisitPay)]

// Setting ComVisible to false makes the types in this assembly not visible
// to COM components.  If you need to access a type in this assembly from
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("76c2be95-0420-4c00-a9d8-17d9414e2dc1")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Build and Revision Numbers
// by using the '*' as shown below:
// [assembly: AssemblyVersion("1.0.*")]
[assembly: AssemblyVersion("1.0.0.0")]
[assembly: AssemblyFileVersion("1.0.0.0")]

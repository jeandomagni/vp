﻿namespace Ivh.Application.Matching.Common.Dtos
{
    using System;
    using Ivh.Common.VisitPay.Enums;

    public class MatchRegistryDto
    {
        public MatchRegistryDto(string matchString, string sourceSystemKey, int hsBillingSystemId)
        {
            this.MatchString = matchString;
            this.SourceSystemKey = sourceSystemKey;
            this.BillingSystemId = hsBillingSystemId;
            this.InsertDate = DateTime.UtcNow;
        }

        public MatchRegistryDto()
        {
        }

        public string MatchString { get; set; }
        public DateTime InsertDate { get; set; }
        public DateTime DataChangeDate { get; set; }
        public DateTime? DeletedDate { get; set; }
        public decimal PartitionKey { get; set; }
        public MatchOptionEnum MatchOptionId { get; set; }
        public PatternSetEnum PatternSetId { get; set; }
        public PatternUseEnum PatternUseId { get; set; }
        public decimal PatternConfidence { get; set; }

        public int BillingSystemId { get; set; }
        public string SourceSystemKey { get; set; }
        public int HsGuarantorId { get; set; }
        public int? BaseVisitId { get; set; }

        #region equality
        public bool Equals(MatchRegistryDto other)
        {
            if (ReferenceEquals(null, other))
            {
                return false;
            }

            if (ReferenceEquals(this, other))
            {
                return true;
            }

            return string.Equals(this.MatchString, other.MatchString) && this.MatchOptionId == other.MatchOptionId && this.PatternSetId == other.PatternSetId && this.PatternUseId == other.PatternUseId && this.BillingSystemId == other.BillingSystemId && string.Equals(this.SourceSystemKey, other.SourceSystemKey);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj))
            {
                return false;
            }

            if (ReferenceEquals(this, obj))
            {
                return true;
            }

            if (obj.GetType() != this.GetType())
            {
                return false;
            }

            return this.Equals((MatchRegistryDto)obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                int hashCode = (this.MatchString != null ? this.MatchString.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ (int)this.MatchOptionId;
                hashCode = (hashCode * 397) ^ (int)this.PatternSetId;
                hashCode = (hashCode * 397) ^ (int)this.PatternUseId;
                hashCode = (hashCode * 397) ^ this.BillingSystemId;
                hashCode = (hashCode * 397) ^ (this.SourceSystemKey != null ? this.SourceSystemKey.GetHashCode() : 0);
                return hashCode;
            }
        }
        #endregion equality
    }
}

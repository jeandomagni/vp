﻿namespace Ivh.Application.Matching.Common.Interfaces
{
    using System;
    using System.Collections.Generic;
    using Ivh.Common.Base.Interfaces;
    using Ivh.Common.JobRunner.Common.Interfaces;
    using Ivh.Common.ServiceBus.Interfaces;
    using Ivh.Common.VisitPay.Enums;
    using Ivh.Common.VisitPay.Jobs;
    using Ivh.Common.VisitPay.Messages.HospitalData;
    using Ivh.Common.VisitPay.Messages.Matching;
    using Ivh.Common.VisitPay.Messages.Matching.Dtos;

    public interface IMatchingApplicationService : IApplicationService,
        IJobRunnerService<QueueMatchRegistryPopulationJobRunner>,
        IJobRunnerService<QueueFindNewHsGuarantorMatchesJobRunner>,
        IJobRunnerService<QueueMatchReconiliationJobRunner>,
        IUniversalConsumerService<MatchReconciliationMessage>,
        IUniversalConsumerService<PopulateMatchRegistryMessageList>,
        IUniversalConsumerService<PopulateMatchRegistryMessage>,
        IUniversalConsumerService<HsGuarantorUnmatchMessage>,
        IUniversalConsumerService<HsGuarantorRematchMessage>,
        IUniversalConsumerService<QueueFindNewHsGuarantorMatchesMessageList>,
        IUniversalConsumerService<QueueFindNewHsGuarantorMatchesMessage>,
        IUniversalConsumerService<UpdateMatchInfoMessage>
    {
        GuarantorMatchResult IsGuarantorMatch(MatchDataDto matchData);
        void QueueMatchRegistryPopulation(DateTime? processDate);
        IList<string> GetRequiredMatchFieldsPatient(ApplicationEnum applicationEnum, PatternUseEnum? matchType = null);
        IList<string> GetRequiredMatchFieldsGuarantor(ApplicationEnum applicationEnum, PatternUseEnum? matchType = null);
        IList<GuarantorMatchResult> AddInitialMatch(int vpGuarantorId, MatchDataDto matchData);
        void QueueFindNewHsGuarantorMatches();
        void QueueMatchReconciliation();
        IList<GuarantorMatchResult> UpdateMatchInfo(int vpGuarantorId, MatchGuarantorDto guarantor, ApplicationEnum applicationEnum);
    }
}

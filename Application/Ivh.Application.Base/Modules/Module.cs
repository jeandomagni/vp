﻿namespace Ivh.Application.Base.Modules
{
    using Autofac;
    using Common.Interfaces;
    using Services;

    public class Module : Autofac.Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<ApplicationServiceCommonService>().As<IApplicationServiceCommonService>();
            builder.RegisterType<VisitBalanceBaseApplicationService>().As<IVisitBalanceBaseApplicationService>();
        }
    }
}
﻿namespace Ivh.Application.Base.Services
{
    using System;
    using Common.Interfaces;
    using Domain.EventJournal.Interfaces;
    using Domain.Logging.Interfaces;
    using Domain.Settings.Interfaces;
    using Ivh.Common.Base.Utilities.Helpers;
    using Ivh.Common.Cache;
    using Ivh.Common.ServiceBus.Interfaces;

    public class ApplicationServiceCommonService : IApplicationServiceCommonService
    {
        public ApplicationServiceCommonService(
            Lazy<IApplicationSettingsService> applicationSettingsService,
            Lazy<IFeatureService> featureService,
            Lazy<IClientService> clientService,
            Lazy<ILoggingService> loggingService,
            Lazy<IEventJournalService> eventJournalService,
            Lazy<IBus> bus,
            Lazy<IDistributedCache> cache,
            Lazy<TimeZoneHelper> timeZoneHelper, 
            Lazy<IInstanceCache> instanceCache)
        {
            this.ApplicationSettingsService = applicationSettingsService;
            this.ClientService = clientService;
            this.LoggingService = loggingService;
            this.Bus = bus;
            this.Cache = cache;
            this.TimeZoneHelper = timeZoneHelper;
            this.InstanceCache = instanceCache;
            this.Journal = eventJournalService;
            this.FeatureService = featureService;
        }

        public Lazy<IApplicationSettingsService> ApplicationSettingsService { get; }
        public Lazy<IFeatureService> FeatureService { get; }
        public Lazy<IClientService> ClientService { get; }
        public Lazy<ILoggingService> LoggingService { get; }
        public Lazy<IEventJournalService> Journal { get; }
        public Lazy<IBus> Bus { get; }
        public Lazy<IDistributedCache> Cache { get; }
        public Lazy<TimeZoneHelper> TimeZoneHelper { get; }
        public Lazy<IInstanceCache> InstanceCache { get; }
    }
}

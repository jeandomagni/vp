﻿namespace Ivh.Application.Base.Services
{
    using System;
    using System.Collections.Generic;
    using System.Runtime.CompilerServices;
    using Common.Interfaces;
    using Domain.EventJournal.Interfaces;
    using Domain.Logging.Interfaces;
    using Domain.Settings.Entities;
    using Domain.Settings.Interfaces;
    using Ivh.Common.Cache;
    using Ivh.Common.ServiceBus.Interfaces;

    public abstract class ApplicationService : IApplicationService
    {
        private readonly Lazy<IApplicationServiceCommonService> _applicationServiceCommonServiceLazy;

        private IApplicationServiceCommonService ApplicationServiceCommonService => this._applicationServiceCommonServiceLazy.Value;

        protected ApplicationService(Lazy<IApplicationServiceCommonService> applicationServiceCommonService)
        {
            this._applicationServiceCommonServiceLazy = applicationServiceCommonService;
        }

        public Lazy<IApplicationSettingsService> ApplicationSettingsService => this.ApplicationServiceCommonService.ApplicationSettingsService;
        public Lazy<IFeatureService> FeatureService => this.ApplicationServiceCommonService.FeatureService;
        public Lazy<IClientService> ClientService => this.ApplicationServiceCommonService.ClientService;
        public Lazy<Client> Client => new Lazy<Client>(() => this.ApplicationServiceCommonService.ClientService.Value.GetClient());
        public Lazy<ILoggingService> LoggingService => this.ApplicationServiceCommonService.LoggingService;
        public IEventJournalService Journal => this.ApplicationServiceCommonService.Journal.Value;
        public Lazy<IBus> Bus => this.ApplicationServiceCommonService.Bus;
        public Lazy<IDistributedCache> Cache => this.ApplicationServiceCommonService.Cache;
        public DateTime SystemNow => this.ApplicationServiceCommonService.TimeZoneHelper.Value.Server.Now;
        public DateTime ClientNow => this.ApplicationServiceCommonService.TimeZoneHelper.Value.Client.Now;
        public DateTime OperationsNow => this.ApplicationServiceCommonService.TimeZoneHelper.Value.Operations.Now;
        public Lazy<IInstanceCache> InstanceCache => this.ApplicationServiceCommonService.InstanceCache;
    }
}

﻿namespace Ivh.Application.Base.Services
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Common.Dtos;
    using Common.Interfaces;
    using Domain.FinanceManagement.FinancePlan.Entities;
    using Domain.FinanceManagement.FinancePlan.Interfaces;
    using Domain.FinanceManagement.Payment.Entities;
    using Domain.FinanceManagement.Payment.Interfaces;
    using Domain.Visit.Entities;
    using Domain.Visit.Interfaces;
    using Ivh.Common.Base.Utilities.Extensions;
    using Ivh.Common.VisitPay.Enums;

    public class VisitBalanceBaseApplicationService : ApplicationService, IVisitBalanceBaseApplicationService
    {
        private readonly Lazy<IFinancePlanService> _financePlanService;
        private readonly Lazy<IPaymentAllocationService> _paymentAllocationService;
        private readonly Lazy<IVisitService> _visitService;

        public VisitBalanceBaseApplicationService(
            Lazy<IApplicationServiceCommonService> applicationServiceCommon,
            Lazy<IFinancePlanService> financePlanService,
            Lazy<IPaymentAllocationService> paymentAllocationService,
            Lazy<IVisitService> visitService)
            : base(applicationServiceCommon)
        {
            this._financePlanService = financePlanService;
            this._paymentAllocationService = paymentAllocationService;
            this._visitService = visitService;
        }

        public void SetTotalBalance<T>(T visitDto) where T : class, IVisitBalanceDto
        {
            if (visitDto == default(T))
            {
                return;
            }

            IList<T> list = new List<T>
            {
                visitDto
            };

            this.SetTotalBalance(list);
        }

        public void SetTotalBalance<T>(IList<T> visitDtos) where T : class, IVisitBalanceDto
        {
            if (visitDtos.IsNullOrEmpty())
            {
                return;
            }

            List<VisitBalanceResult> visitBalances = visitDtos.Select(x => new VisitBalanceResult
            {
                VisitId = x.VisitId,
                CurrentBalance = x.CurrentBalance
            }).ToList();

            IList<VisitBalanceBaseDto> visitBalanceDtos = this.GetTotalBalances(visitBalances);

            foreach (T visit in visitDtos)
            {
                VisitBalanceBaseDto visitBalanceDto = visitBalanceDtos.FirstOrDefault(x => x.VisitId == visit.VisitId);
                decimal unclearedPaymentsSum = visitBalanceDto?.UnclearedPaymentsSum ?? 0m;
                decimal interestAssessed = visitBalanceDto?.InterestAssessed ?? 0m;
                decimal interestDue = visitBalanceDto?.InterestDue ?? 0m;
                decimal interestPaid = visitBalanceDto?.InterestPaid ?? 0m;
                decimal totalBalance = visitBalanceDto?.TotalBalance ?? 0m;

                visit.UnclearedPaymentsSum = unclearedPaymentsSum;
                visit.InterestAssessed = interestAssessed;
                visit.InterestDue = interestDue;
                visit.InterestPaid = interestPaid;
                visit.UnclearedBalance = totalBalance;
            }
        }

        public decimal GetTotalBalance(int vpGuarantorId)
        {
            IList<int> visitIds = this.GetVisitIdsForTotalBalance(vpGuarantorId);
            if (visitIds.IsNullOrEmpty())
            {
                return 0m;
            }

            IList<VisitBalanceBaseDto> visitBalanceDtos = this.GetTotalBalances(vpGuarantorId, visitIds);

            return visitBalanceDtos.Sum(x => x.TotalBalance);
        }

        public IList<VisitBalanceBaseDto> GetTotalBalances(int vpGuarantorId)
        {
            IList<int> visitIds = this.GetVisitIdsForTotalBalance(vpGuarantorId);
            if (visitIds.IsNullOrEmpty())
            {
                return new List<VisitBalanceBaseDto>();
            }

            IList<VisitBalanceBaseDto> visitBalanceDtos = this.GetTotalBalances(vpGuarantorId, visitIds);

            return visitBalanceDtos;
        }

        public IList<VisitBalanceBaseDto> GetTotalBalances(int vpGuarantorId, IList<int> visitIds)
        {
            return this.InstanceCache.Value.CacheValue(
                () =>
                {
                    unchecked
                    {
                        return vpGuarantorId.GetHashCode() + visitIds.GetSequenceHashCode();
                    }
                },
                () =>
            {
                IList<VisitBalanceResult> visitBalances = this._visitService.Value.GetVisitBalances(vpGuarantorId, visitIds);

                IList<VisitBalanceBaseDto> visitBalanceDtos = this.GetTotalBalances(visitBalances);

                return visitBalanceDtos;
            });
        }

        private IList<VisitBalanceBaseDto> GetTotalBalances(IList<VisitBalanceResult> visitBalances)
        {
            IDictionary<int, decimal[]> unclearedPaymentsForVisits = this.GetUnclearedPaymentsForVisits(visitBalances.Select(x => x.VisitId).ToList());
            IList<FinancePlanVisitInterestDueResult> interestDueForVisits = this._financePlanService.Value.GetInterestDueForVisits(visitBalances.Select(x => x.VisitId).ToList());
            IList<FinancePlanVisitInterestDueResult> allInterestForVisits = this._financePlanService.Value.GetInterestForVisits(visitBalances.Select(x => x.VisitId).ToList());
            IList<FinancePlanVisitPrincipalAmountResult> financePlanPrincipalAmounts = this._financePlanService.Value.GetPrincipalAmountsForVisits(visitBalances.Select(x => x.VisitId).ToList());
            IList<VisitBalanceBaseDto> visitBalanceDtos = new List<VisitBalanceBaseDto>();

            foreach (VisitBalanceResult visitBalance in visitBalances)
            {
                decimal? financePlanVisitPrincipalAmount = financePlanPrincipalAmounts.FirstOrDefault(x => x.VisitId == visitBalance.VisitId)?.Amount;
                if (financePlanVisitPrincipalAmount.HasValue)
                {
                    // fpv principal amount entries include uncleared payments
                    financePlanVisitPrincipalAmount -= unclearedPaymentsForVisits?.GetValue(visitBalance.VisitId)?.Sum() ?? 0m;
                }

                decimal currentBalance = financePlanVisitPrincipalAmount ?? visitBalance.CurrentBalance;
                decimal interestDue = interestDueForVisits.Where(x => x.VisitId == visitBalance.VisitId).Sum(x => x.InterestDue);
                decimal interestAssessed = allInterestForVisits.Where(x => x.VisitId == visitBalance.VisitId && x.FinancePlanVisitInterestDueType == FinancePlanVisitInterestDueTypeEnum.InterestAssessment).Sum(x => x.InterestDue);
                decimal interestPaid = allInterestForVisits.Where(x => x.VisitId == visitBalance.VisitId).Sum(x => x.InterestDue) - interestAssessed;

                VisitBalanceBaseDto visitBalanceDto = new VisitBalanceBaseDto
                {
                    VisitId = visitBalance.VisitId,
                    UnclearedPaymentsSum = unclearedPaymentsForVisits?.GetValue(visitBalance.VisitId)?.Sum() ?? 0m,
                    InterestDue = interestDue,
                    InterestPaid = interestPaid,
                    InterestAssessed = interestAssessed,
                    CurrentBalance = currentBalance
                };

                visitBalanceDtos.Add(visitBalanceDto);
            }

            return visitBalanceDtos;
        }

        public IDictionary<int, decimal[]> GetUnclearedPaymentsForVisits(IList<int> visitIds, DateTime? startDate = null, DateTime? endDate = null)
        {
            IDictionary<int, decimal[]> unclearedPaymentsForVisits = new Dictionary<int, decimal[]>();

            visitIds = (visitIds ?? new List<int>()).Distinct().ToList();

            IList<PaymentAllocationVisitAmountResult> paymentAllocations = this._paymentAllocationService.Value.GetNonInterestUnclearedPaymentAmountsForVisits(visitIds, startDate, endDate) ?? new List<PaymentAllocationVisitAmountResult>();

            foreach (int visitId in visitIds)
            {
                decimal[] paymentAllocationsForVisit = paymentAllocations.Where(x => x.VisitId == visitId).Select(x => x.ActualAmount).ToArray();
                if (paymentAllocationsForVisit.IsNotNullOrEmpty())
                {
                    unclearedPaymentsForVisits.Add(visitId, paymentAllocationsForVisit.Select(decimal.Negate).ToArray());
                }
            }

            return unclearedPaymentsForVisits;
        }

        private IList<int> GetVisitIdsForTotalBalance(int vpGuarantorId)
        {
            IList<int> activeVisitIds = this._visitService.Value.GetVisitIds(vpGuarantorId, VisitStateEnum.Active);
            if (activeVisitIds.IsNullOrEmpty())
            {
                return new List<int>();
            }

            IList<int> visitsOnActiveFinancePlans = this._financePlanService.Value.GetVisitIdsOnActiveFinancePlans(vpGuarantorId);

            IList<int> visitIds = activeVisitIds.Where(x => !visitsOnActiveFinancePlans.Contains(x)).ToList();

            return visitIds.IsNotNullOrEmpty() ? visitIds : new List<int>();
        }
    }
}
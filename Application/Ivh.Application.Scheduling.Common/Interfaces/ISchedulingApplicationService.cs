namespace Ivh.Application.Scheduling.Common.Interfaces
{
    using Ivh.Common.Base.Interfaces;
    using MassTransit;
    using MassTransit.Scheduling;

    public interface ISchedulingApplicationService : IApplicationService
    {
        void ScheduleMessage(ConsumeContext<ScheduleMessage> context);
        void ScheduleRecurringMessage(ConsumeContext<ScheduleRecurringMessage> context);
    }
}
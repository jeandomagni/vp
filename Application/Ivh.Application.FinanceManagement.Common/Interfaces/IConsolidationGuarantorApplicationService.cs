﻿namespace Ivh.Application.FinanceManagement.Common.Interfaces
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Dtos;
    using Ivh.Common.Base.Interfaces;
    using Ivh.Common.JobRunner.Common.Interfaces;
    using Ivh.Common.ServiceBus.Interfaces;
    using Ivh.Common.VisitPay.Jobs;
    using Ivh.Common.VisitPay.Messages.Consolidation;

    public interface IConsolidationGuarantorApplicationService : IApplicationService,
        IUniversalConsumerService<ConsolidationExpiredMessage>,
        IJobRunnerService<QueueConsolidationExpirationJobRunner>,
        IJobRunnerService<QueueConsolidationRemindersJobRunner>
    {
        void CheckForExpiringConsolidations(DateTime runDateTime);
        void SendConsolidationReminders(DateTime runDateTime);
        void ExpireConsolidation(int consolidationGuarantorId, DateTime expirationDateTime);
        Task<ConsolidationGuarantorDto> GetConsolidationGuarantorAsync(int consolidationGuarantorId, int visitPayUserId);
        IEnumerable<ConsolidationGuarantorDto> GetAllManagedGuarantors(int managingVisitPayUserId);
        IEnumerable<ConsolidationGuarantorDto> GetAllManagingGuarantors(int managedVisitPayUserId);
        IDictionary<int, decimal> GetHouseholdBalances(int managingVisitPayUserId);
    }
}
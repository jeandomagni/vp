﻿namespace Ivh.Application.FinanceManagement.Common.Interfaces
{
    using System;
    using Dtos;
    using Ivh.Common.Base.Enums;
    using Ivh.Common.Base.Interfaces;
    using Ivh.Common.JobRunner.Common.Interfaces;
    using Ivh.Common.ServiceBus.Interfaces;
    using Ivh.Common.VisitPay.Enums;
    using Ivh.Common.VisitPay.Jobs;
    using Ivh.Common.VisitPay.Messages.FinanceManagement;

    public interface IAchApplicationService : IApplicationService,
        IUniversalConsumerService<AchReturnMessageGuarantor>,
        IUniversalConsumerService<AchSettlementMessageGuarantor>,
        IJobRunnerService<QueueAchDownloadJobRunner>,
        IJobRunnerService<QueueAchPublishJobRunner>
    {
        void DownloadAchReturnsAndSettlements(DateTime startDate, DateTime endDate);
        void PublishAchReturnsAndSettlements(DateTime startDate, DateTime endDate);
        void ProcessAchReturn(AchReturnMessageGuarantor achReturnMessageGuarantor);
        void ProcessAchSettlement(AchSettlementMessageGuarantor achSettlementMessageGuarantor);
        void ProcessAchReturnAfterSettlement(string transactionId, int vpGuarantorId);
        void AddAchFile(AchFileDto achFileDto);
        AchReturnCodeEnum GetReturnCode(string code);
        string GetReturnCode(AchReturnCodeEnum code);
    }
}
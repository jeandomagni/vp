﻿namespace Ivh.Application.FinanceManagement.Common.Interfaces
{
    using System.Threading.Tasks;
    using Content.Common.Dtos;
    using Dtos;

    public interface ICreditAgreementApplicationService
    {
        Task CacheCreditAgreementTermsAsync(FinancePlanTermsDto financePlanTermsDto);
        Task<FinancePlanCreditAgreementCollectionDto> GetCachedCreditAgreementAsync(int financePlanOfferId, int? termsCmsVersionId, bool includeMaterialTerms);
        int? GetFinancePlanOfferIdFromContractNumber(string contractNumber, int? vpGuarantorId);
		Task<ContentDto> GetCreditAgreementContentAsync(int financePlanOfferId, bool includeMaterialTerms = false);
        Task<FinancePlanTermsDto> GetCachedTermsForCreditAgreementAsync(int financePlanOfferId, int vpGuarantorId);
        string GetAgreementTitle(bool isRetailInstallmentContract);
    }
}
﻿namespace Ivh.Application.FinanceManagement.Common.Interfaces
{
    using System.Threading.Tasks;
    using Dtos;
    using Ivh.Common.Base.Enums;
    using Ivh.Common.Base.Interfaces;

    public interface IManagedUserApplicationService : IApplicationService
    {
        Task CancelConsolidationAsync(int consolidationGuarantorId, int visitPayUserId);
        Task RejectConsolidationAsync(int consolidationGuarantorId, int visitPayUserId);
        Task<bool> AcceptTermsAsync(int consolidationGuarantorId, int visitPayUserId);
        Task<ConsolidationMatchResponseDto> MatchGuarantorAsync(ConsolidationMatchRequestDto consolidationMatchRequestDto, int visitPayUserId);
    }
}
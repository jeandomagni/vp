﻿namespace Ivh.Application.FinanceManagement.Common.Interfaces
{
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Dtos;
    using Ivh.Common.Base.Enums;
    using Ivh.Common.Base.Interfaces;
    using Ivh.Common.EventJournal;
    using Ivh.Common.VisitPay.Enums;

    public interface IPaymentReversalApplicationService : IApplicationService
    {
        Task<VoidPaymentResponseDto> VoidPaymentAsync(int visitPayGuarantorId, int paymentProcessorResponseId, int visitPayUserId);
        Task<RefundPaymentResponseDto> RefundPaymentAsync(JournalEventHttpContextDto context, int vpGuarantorId, int paymentProcessorResponseId, int visitPayUserId, decimal? amount = null);
        IDictionary<int, RefundPaymentStatusInfoDto> GetPaymentReversalStatus(IList<int> paymentProcessorResponseIds);
        bool ReageFinancePlanPaymentBuckets(IList<int> reversalPaymentIds, int financePlanId, int guarantorId, int? actionVisitPayUser=null);
        PaymentDto ReallocateFromInterestToPrincipal(int vpGuarantorId, int? visitPayUserId, int financePlanFinancePlanId, decimal modelReallocateAmountAfterWriteOff, Dictionary<int,decimal> maxPerVisit);
    }
}
﻿namespace Ivh.Application.FinanceManagement.Common.Interfaces
{
    using System.Collections;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Dtos;
    using Ivh.Common.Base.Enums;
    using Ivh.Common.Base.Interfaces;

    public interface IManagingUserApplicationService : IApplicationService
    {
        Task InitiateRelationshipWithGuarantorAsync();
        Task CancelConsolidationAsync(int consolidationGuarantorId, int visitPayUserId);
        Task RejectConsolidationAsync(int consolidationGuarantorId, int visitPayUserId);
        Task<bool> AcceptTermsAsync(int consolidationGuarantorId, int visitPayUserId);

        Task<bool> AcceptFinancePlanTermsAsync(int consolidationGuarantorId, int visitPayUserId);

        Task<ConsolidationMatchResponseDto> MatchGuarantorAsync(ConsolidationMatchRequestDto matchRequestDto, int visitPayUserId);

        Task<IList<FinancePlanDto>> GetConsolidationFinancePlansAsync(int consolidationGuarantorId, int visitPayUserId);
    }
}
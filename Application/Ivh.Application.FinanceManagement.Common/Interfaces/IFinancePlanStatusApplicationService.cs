﻿namespace Ivh.Application.FinanceManagement.Common.Interfaces
{
    using Ivh.Common.Base.Interfaces;
    using Ivh.Common.ServiceBus.Interfaces;
    using Ivh.Common.VisitPay.Messages.FinanceManagement;

    public interface IFinancePlanStatusApplicationService : IApplicationService,
        IUniversalConsumerService<FinancePlanChangedStatusMessage>,
        IUniversalConsumerService<FinancePlanChangedStatusMessageList>
    {
    }
}
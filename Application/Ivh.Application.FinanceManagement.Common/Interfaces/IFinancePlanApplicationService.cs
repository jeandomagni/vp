﻿namespace Ivh.Application.FinanceManagement.Common.Interfaces
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Dtos;
    using Ivh.Common.Base.Interfaces;
    using Ivh.Common.EventJournal;
    using Ivh.Common.JobRunner.Common.Interfaces;
    using Ivh.Common.ServiceBus.Interfaces;
    using Ivh.Common.VisitPay.Constants;
    using Ivh.Common.VisitPay.Enums;
    using Ivh.Common.VisitPay.Jobs;
    using Ivh.Common.VisitPay.Messages.FinanceManagement;
    using Ivh.Common.VisitPay.Messages.HospitalData;

    public interface IFinancePlanApplicationService : IApplicationService,
        IUniversalConsumerService<FinancePlanCreatedMessage>,
        IUniversalConsumerService<FinancePlanClosedMessage>,
        IUniversalConsumerService<FinancePlanVisitFinancialAssistanceFlagChangedMessage>,
        IUniversalConsumerService<FinancePlanRefundInterestOnRecall>,
        IUniversalConsumerService<FinancePlanVisitRefundInterestMessage>,
        IUniversalConsumerService<ChangeEventProcessedForVpGuarantor>,
        IJobRunnerService<RemovePaymentBucketsForFinancePlanJobRunner>
    {
        FinancePlanBoundaryDto GetMinimumPaymentAmount(FinancePlanCalculationParametersDto financePlanCalculationParametersDto);
        int? GetFirstTierMaxDuration(FinancePlanCalculationParametersDto financePlanCalculationParametersDto);
        bool IsEligibleForFinancePlan(FinancePlanCalculationParametersDto financePlanCalculationParametersDto, int currentVisitPayUserId);
        bool IsEligibleForAnyFinancePlan(int vpGuarantorId, int currentVisitPayUserId);
        decimal GetEligibleBalanceForFinancePlan(FinancePlanCalculationParametersDto financePlanCalculationParametersDto, int currentVisitPayUserId);
        FinancePlanDto GetFinancePlan(int visitPayUserId, int financePlanId, int? vpGuarantorId = null);
        FinancePlanResultsDto GetFinancePlans(int visitPayUserId, FinancePlanFilterDto filterDto, int pageNumber, int pageSize, int? vpGuarantorId = null);
        IList<FinancePlanDto> GetFinancePlans(int vpGuarantorId);
        FinancePlanSummaryDto GetFinancePlanSummary(int vpGuarantorId);
        int GetActiveFinancePlanCount(int visitPayUserId, int? vpGuarantorId = null);
        int? GetFinancePlanOfferId(string financePlanPublicIdPhi, int? vpGuarantorId);
        bool HasActiveFinancePlans(int visitPayUserId, int? vpGuarantorId = null, bool considerConsolidation = false);
        bool HasFinancePlans(int visitPayUserId, IList<FinancePlanStatusEnum> statuses, int? vpGuarantorId = null);

        /// <summary>
        /// Determines if a guarantor is eligible to add visits to an existing finance plan
        /// </summary>
        /// <param name="currentVisitPayUserId"></param>
        /// <param name="vpGuarantorId">ID of the Guarantor</param>
        /// <param name="checkTerms">Check that the balance is eligible for an offer</param>
        /// <param name="financePlanStateCode">Optional: Pass if checking for a specific state</param>
        /// <returns></returns>
        bool IsGuarantorEligibleToCombineFinancePlans(int currentVisitPayUserId, int vpGuarantorId, bool checkTerms, string financePlanStateCode = null);
        IList<FinancePlanDto> GetOpenFinancePlans(int visitPayUserId, int? vpGuarantorId = null, bool considerConsolidation = false, string financePlanStateCode = null);
        IList<FinancePlanDto> GetOpenFinancePlansForGuarantor(int vpGuarantorId, bool considerConsolidation = false);
        Task<IList<FinancePlanAchAuthorizationDto>> GetFinancePlansRequiringAchAuthorizationAsync(int currentVisitPayUserId, int vpGuarantorId, int? financePlanOfferId, IList<int> financePlanIds, int? financePlanIdToReconfigure, int? consolidationGuarantorId);
        IList<FinancePlanStatusDto> GetFinancePlanStatuses();
        IList<FinancePlanTypeDto> GetFinancePlanTypes();
        IList<InterestRateDto> GetInterestRates(FinancePlanCalculationParametersDto financePlanCalculationParametersDto);
        decimal GetTotalPaymentAmountForOpenFinancePlans(int vpGuarantorId);
        FinancePlanCountDto GetFinancePlanTotals(int visitPayUserId, FinancePlanFilterDto filterDto, int? vpGuarantorId = null);
        Task<CalculateFinancePlanTermsResponseDto> GetTermsByMonthlyPaymentAmountAsync(FinancePlanCalculationParametersDto financePlanCalculationParametersDto);
        Task<CalculateFinancePlanTermsResponseDto> GetTermsByNumberOfMonthlyPaymentsAsync(FinancePlanCalculationParametersDto financePlanCalculationParametersDto);
        Task<CalculateFinancePlanTermsResponseDto> GetTermsOnPendingFinancePlanAsync(int visitPayUserId, int? vpGuarantorId = null);
        Task<CreateFinancePlanResponse> CreateFinancePlanAsync(int currentVisitPayUserId, int filteredVisitPayUserId, FinancePlanCalculationParametersDto financePlanCalculationParametersDto, int termsCmsVersionId, int financePlanOfferId, int loggedInVisitPayUserId, JournalEventHttpContextDto httpContext);
        CreateFinancePlanResponse CreateFinancePlanPendingAcceptance(FinancePlanCalculationParametersDto financePlanCalculationParametersDto, int loggedInVisitPayUserId, JournalEventHttpContextDto httpContext);
        Task<CreateFinancePlanResponse> UpdatePendingFinancePlanAsync(FinancePlanCalculationParametersDto financePlanCalculationParametersDto, int financePlanId, int financePlanOfferId, int visitPayUserId, int? termsCmsVersionId, int loggedInVisitPayUserId, JournalEventHttpContextDto httpContext);
        void CancelFinancePlan(int guarantorVisitPayUserId, int financePlanId, string reason, int? actionVisitPayUserId = null);
        IList<FinancePlanInterestRateHistoryDto> GetFinancePlanInterestRateHistory(int visitPayUserId, int financePlanId, int? vpGuarantorId = null);
        FinancePlanLogResultsDto GetFinancePlanLog(FinancePlanLogFilterDto financePlanLogFilterDto);
        void CheckIfFinancePlanIsValid(IList<int> guarantorIds);
        Task<CalculateFinancePlanTermsResponseDto> GetAvailableFinancePlanOfferForPayment(int currentVisitPayUserId, int vpGuarantorId, decimal paymentAmount);
        void CheckIfFinancePlanIsPastDueOrUncollectable(IList<int> guarantorIds);
        void CheckIfReconfigurationIsStillValid(IList<int> guarantorIds);
        Task<byte[]> ExportFinancePlansClientAsync(int visitPayUserId, FinancePlanFilterDto filterDto, string userName, int? vpGuarantorId = null);
        Task<byte[]> ExportFinancePlansAsync(int visitPayUserId, FinancePlanFilterDto filterDto, string userName, int? vpGuarantorId = null);
        void CheckIfPendingFinancePlansNeedToClose(IList<int> visitIds);
        void CancelPaymentBucketsForFinancePlan(int financePlanId, int guarantorId, int numberOfBucketsToKeep, int? visitPayUserId);
        void PopulateFinancePlanInfoOnPaymentAllocationMessages(IList<PaymentAllocationMessage> messages);
        [Obsolete(ObsoleteDescription.VisitTransactions + " " + ObsoleteDescription.VisitTransactionVisitPayPayment)]
        Task RecalculateFinancePlanDueToFinancialAssistanceFlagChangeAsync(FinancePlanVisitFinancialAssistanceFlagChangedMessage financePlanVisitFinancialAssistanceFlagChangedMessage);
        //bool ReduceBucketCount(int financePlanId, int bucketNumber, string comment, int actionVisitPayUserId);
        //bool SetBucketToUncollectable(int financePlanId, string comment, int actionVisitPayUserId);
        FinancePlanBucketHistoryResultsDto GetBucketHistory(FinancePlanBucketHistoryFilterDto filterDto);
        IList<FinancePlanOfferSetTypeDto> GetFinancePlanOfferSetTypes();
        FinancePlanDto GetFinancePlanByOfferId(int financePlanOfferId);
        FinancePlanDto GetFinancePlanWithVisitId(int visitId);
        void OnFinancePlanCreated(int vpGuarantorId, int financePlanId);
        void OnFinancePlanClosed(int vpGuarantorId, int financePlanId);
        IList<ResubmitPaymentDto> GetPaymentResubmitDtosForFinancePlans(int vpGuarantorId);
        bool HasFinancePlansWhichShouldResubmit(int vpGuarantorId);
        FinancePlanCancelPaymentDto GetFinancePlanCancelPayment(int vpGuarantorId);
        ReallocateInterestToPrincipalDto ReallocateInterestToPrincipal(int visitPayUserId, int vpGuarantorId, int financePlanId, decimal amountToReallocated, int? financePlanVisitId=null);
        void ProcessFinancePlanVisitRefundInterestMessage(FinancePlanVisitRefundInterestMessage message);
        IList<PaymentDto> RoboRefund(int vpGuarantorId, DateTime currentStatementEndDate);
        void RoboRefund_AddNonNaturalQualifyingEvents(int vpGuarantorId);
        void ApplyInterestRefundIncentive(int financePlanId);
        void SetAmountDueForDisplay(FinancePlanDto financePlanDto);
        IList<int> GetVisitIdsOnPendingFinancePlans(int vpGuarantorId);
        Task LogTermsDisplayedAsync(FinancePlanTermsDto financePlanTermsDto, decimal? minimumMonthlyPaymentAmount, int? maximumNumberOfPayments);
        bool MeetsCriteriaToChangePaymentDueDate(int vpGuarantorId);
        DateTime? GetPendingFinancePlanCancellationDate(int vpGuarantorId);
    }
}

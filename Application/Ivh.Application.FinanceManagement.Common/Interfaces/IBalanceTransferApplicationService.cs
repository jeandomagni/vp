﻿namespace Ivh.Application.FinanceManagement.Common.Interfaces
{
    using Core.Common.Dtos;
    using Ivh.Common.Base.Interfaces;
    using Ivh.Common.ServiceBus.Interfaces;
    using Ivh.Common.VisitPay.Messages.FinanceManagement;

    public interface IBalanceTransferApplicationService : IApplicationService,
        IUniversalConsumerService<BalanceTransferSendFinancePlanPaymentSettledMessage>,
        IUniversalConsumerService<BalanceTransferSetVisitActiveMessage>
    {
        void SetVisitBalanceTransferStatus(int visitId, int visitPayUserId, int changedByVisitPayUserId, string notes, int balanceTransferStatusId);
        BalanceTransferStatusHistoryResultsDto GetBalanceTransferStatusHistory(BalanceTransferStatusHistoryFilterDto filterDto);
        void PublishBalanceTransferFinancePlanPaymentMessage(int vpGuarantorId, int paymentId);
        void SetBalanceTransferStatusActiveForVisit(string visitSourceSystemKey, int billingSystemId);
    }
}

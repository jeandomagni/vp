﻿namespace Ivh.Application.FinanceManagement.Common.Interfaces
{
    using System;
    using System.Collections.Generic;
    using Core.Common.Dtos;
    using Dtos;
    using Ivh.Common.Base.Interfaces;

    public interface IStatementApplicationService : IApplicationService
    {
        StatementDto GetStatement(int statementId, int visitPayUserId);
        IReadOnlyList<StatementDto> GetStatements(int visitPayUserId);
        FinancePlanStatementDto GetFinancePlanStatement(int financePlanStatementId, int visitPayUserId);
        FinancePlanStatementDto GetFinancePlanStatementByPeriodStartDate(int guarantorId, DateTime periodStartDate);
        FinancePlanStatementDto GetMostRecentFinancePlanStatement(int guarantorId);
        IReadOnlyList<FinancePlanStatementDto> GetFinancePlanStatements(int visitPayUserId);
        int GetStatementTotals(int guarantorVisitPayUserId);
        StatementDto GetLastStatement(int vpGuarantorId);
        StatementDto GetLastStatement(GuarantorDto guarantorDto);
        bool DoesGuarantorHaveStatement(int vpGuarantorId);
        GuarantorWithLastStatementResultsDto GetGuarantorsWithLastStatement(GuarantorFilterDto guarantorFilterDto, int pageNumber, int pageSize);
        GuarantorWithLastFinancePlanStatementResultsDto GetGuarantorsWithLastFinancePlanStatement(GuarantorFilterDto guarantorFilterDto, int pageNumber, int pageSize);
        byte[] ExportGuarantorsWithLastStatement(GuarantorFilterDto guarantorFilterDto, string userName);
        DateTime? GetNextStatementDate(int vpGuarantorId, DateTime dateToProcess);
        DateTime? GetNextPaymentDate(int vpGuarantorId);
        byte[] GetStatementContent(int vpGuarantorId, int vpStatementId);
    }
}

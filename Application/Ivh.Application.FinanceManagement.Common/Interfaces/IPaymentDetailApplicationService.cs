﻿namespace Ivh.Application.FinanceManagement.Common.Interfaces
{
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Dtos;
    using Ivh.Common.Base.Interfaces;

    public interface IPaymentDetailApplicationService : IApplicationService
    {
        #region scheduled payments

        ScheduledPaymentDto GetScheduledManualPayment(int vpGuarantorId, int paymentId);
        ScheduledPaymentDto GetScheduledRecurringPayment(int vpGuarantorId);
        IList<ScheduledPaymentDto> GetScheduledPayments(int vpGuarantorId);
        bool HasScheduledPayments(int vpGuarantorId);

        #endregion

        #region payment history

        PaymentHistoryResultsDto GetPaymentHistory(int madeForVpGuarantorId, int currentVpGuarantorId, PaymentProcessorResponseFilterDto filterDto, int pageNumber, int pageSize);

        #endregion

        #region payment detail

        PaymentDto GetPayment(int vpGuarantorId, int paymentId);
        IList<PaymentEventDto> GetPaymentEvents(List<int> paymentIds, int vpGuarantorId);
        PaymentDetailDto GetPaymentDetail(int madeForVpGuarantorId, int currentVpGuarantorId, int paymentProcessorResponseId);

        #endregion

        #region exports

        Task<byte[]> ExportPaymentPendingAsync(int madeForVpGuarantorId, string userName);
        Task<byte[]> ExportPaymentHistoryAsync(int madeForVpGuarantorId, int currentVpGuarantorId, PaymentProcessorResponseFilterDto filterDto, string userName);
        Task<byte[]> ExportPaymentDetailAsync(int madeForVpGuarantorId, int currentVpGuarantorId, int paymentProcessorResponseId, string userName);

        #endregion

        IList<PaymentStatusDto> GetPaymentStatuses(string category);
    }
}

﻿namespace Ivh.Application.FinanceManagement.Common.Interfaces
{
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Dtos;
    using Ivh.Common.Base.Interfaces;
    using Ivh.Common.VisitPay.Enums;

    public interface IPaymentOptionApplicationService : IApplicationService
    {
        Task<UserPaymentOptionDto> GetPaymentOptionAsync(int vpGuarantorId, int currentVisitPayUserId, PaymentOptionEnum paymentOption);
        Task<IList<UserPaymentOptionDto>> GetPaymentOptionsAsync(int vpGuarantorId, int currentVisitPayUserId);
        Task<UserPaymentOptionDto> GetFinancePlanPaymentOptionAsync(int vpGuarantorId, int currentVisitPayUserId, int? financePlanOfferSetTypeId = null, string financePlanOptionStateCode = null, bool otherFinancePlanStatesAreAvailable = false);
        PaymentMenuDto GetPaymentOptionMenu(IList<UserPaymentOptionDto> paymentOptions, PaymentOptionEnum? selectedPaymentOption);
        PaymentTypeEnum ConvertPaymentOptionToPaymentType(PaymentOptionEnum paymentOption, bool payBucketZero = false);
    }
}
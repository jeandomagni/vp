﻿namespace Ivh.Application.FinanceManagement.Common.Interfaces
{
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Dtos;

    public interface IServiceGroupApplicationService
    {
        Task<IList<ServiceGroupDto>> GetAllServiceGroupsWithContentAsync();
    }
}
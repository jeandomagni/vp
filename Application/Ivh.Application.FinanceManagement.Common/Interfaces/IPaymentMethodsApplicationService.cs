﻿namespace Ivh.Application.FinanceManagement.Common.Interfaces
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Dtos;
    using Ivh.Common.Base.Interfaces;
    using Ivh.Common.EventJournal;
    using Ivh.Common.JobRunner.Common.Interfaces;
    using Ivh.Common.VisitPay.Jobs;
    using User.Common.Dtos;

    public interface IPaymentMethodsApplicationService : IApplicationService,
        IJobRunnerService<QueuePaymentMethodJobRunner>
    {
        PaymentMethodDto GetPaymentMethodById(int paymentMethodId);
        PaymentMethodDto GetPaymentMethodById(int paymentMethodId, int vpGuarantorId);
        PaymentMethodDto GetPrimaryPaymentMethod(int vpGuarantorId);
        IList<PaymentMethodDto> GetPaymentMethods(int vpGuarantorId, int actionVisitPayUserId, bool includingInactive = false);
        Task<DeletePaymentMethodResponseDto> DeactivatePaymentMethodAsync(int paymentMethodId, int vpGuarantorId, int actionVisitPayUserId);
        Task<PaymentMethodResultDto> SaveBankPaymentMethodAsync(JournalEventHttpContextDto context, PaymentMethodDto paymentMethodDto, int vpGuarantorId, int actionVisitPayUserId, string accountNumber, string routingNumber);
        PaymentMethodResultDto SaveCardPaymentMethod(JournalEventHttpContextDto context, PaymentMethodDto paymentMethodDto, int vpGuarantorId, int actionVisitPayUserId);
        PaymentMethodResultDto SaveTempLockBoxPaymentMethod(int vpGuarantorId, int actionVisitPayUserId);
        PaymentMethodResultDto SetPrimaryPaymentMethod(JournalEventHttpContextDto context, int paymentMethodId, int vpGuarantorId, int actionVisitPayUserId);
        PaymentMethodResultDto SetPaymentMethodAccountProviderType(int paymentMethodId, int paymentMethodAccountTypeId, int paymentMethodProviderTypeId, int vpGuarantorId, int actionVisitPayUserId);
        Task<PaymentMethodResultDto> ProcessBankPaymentMethodRegistrationAsync(JournalEventHttpContextDto context, PaymentMethodDto paymentMethodDto, string accountNumber, string routingNumber);
        PaymentMethodResultDto ProcessCardPaymentMethodRegistration(HttpContextDto context, PaymentMethodDto paymentMethodDto, string billingId, string gatewayToken, string securityNumber, string lastFour);
        void PersistPaymentMethodsFromRegistration(IList<PaymentMethodDto> paymentMethodDtos, int visitPayUserId, int vpGuarantorId);
        void NotifyExpiringPrimaryPaymentMethods(DateTime processDate);
        void AddAchAuthorization(int currentVisitPayUserId, int paymentMethodId, int cmsVersionId, IList<FinancePlanAchAuthorizationDto> authorizations);
    }
}
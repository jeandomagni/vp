﻿namespace Ivh.Application.FinanceManagement.Common.Interfaces
{
    using System;
    using System.Collections.Generic;
    using Dtos;
    using Ivh.Common.Base.Interfaces;
    using Ivh.Common.JobRunner.Common.Interfaces;
    using Ivh.Common.ServiceBus.Interfaces;
    using Ivh.Common.VisitPay.Jobs;
    using Ivh.Common.VisitPay.Messages.FinanceManagement;

    public interface IPastDueUncollectableApplicationService : IApplicationService,
        IUniversalConsumerService<ProcessActiveUncollectableNotificationMessage>,
        IUniversalConsumerService<ProcessClosedUncollectableFinancePlanNotificationMessage>,
        IUniversalConsumerService<ProcessClosedUncollectableFinancePlanNotificationMessageList>,
        IUniversalConsumerService<ProcessFinalPastDueNotificationMessage>,
        IJobRunnerService<QueueClosedUncollectablesJobRunner>,
        IJobRunnerService<QueuePendingUncollectableNotificationsJobRunner>
    {
        void QueueFinalPastDueAndActiveUncollectablesNotifications(DateTime dateToProcess);
        IList<FinalPastDueNotificationInfoDto> GetFinalPastDueNotificationsToQueue(DateTime dateToProcess);
        void QueueClosedUncollectableNotifications(DateTime dateToProcess);
    }
}

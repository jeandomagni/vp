﻿namespace Ivh.Application.FinanceManagement.Common.Interfaces
{
    using Base.Common.Interfaces;
    using Ivh.Common.ServiceBus.Interfaces;
    using Ivh.Common.VisitPay.Messages.Core;

    public interface ITextToPayApplicationService: IApplicationService,
        IUniversalConsumerService<ReceiveTextToPayPaymentOptionMessage>,
        IUniversalConsumerService<TextToPayPaymentOptionMessage>
    {
        void PayFromText(string phoneNumber);
        void SendTextToPayPaymentOptionMessage(TextToPayPaymentOptionMessage message);
    }
}
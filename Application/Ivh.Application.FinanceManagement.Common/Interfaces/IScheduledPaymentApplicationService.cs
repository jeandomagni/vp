﻿namespace Ivh.Application.FinanceManagement.Common.Interfaces
{
    using System;
    using Ivh.Common.Base.Interfaces;
    using Ivh.Common.JobRunner.Common.Interfaces;
    using Ivh.Common.ServiceBus.Interfaces;
    using Ivh.Common.VisitPay.Jobs;
    using Ivh.Common.VisitPay.Messages.FinanceManagement;

    public interface IScheduledPaymentApplicationService : IApplicationService,
        IUniversalConsumerService<FinancePlanPaymentAmountChangedMessage>,
        IUniversalConsumerService<ProcessScheduledPaymentWithGuarantorMessage>,
        IJobRunnerService<QueueScheduledPaymentRemindersJobRunner>,
        IJobRunnerService<QueueScheduledPaymentsByGuarantorJobRunner>
    {
        void QueueScheduledPaymentsForGuarantorsChargedByDate(DateTime dateToProcess);
        void ProcessScheduledPayment(int vpGuarantorId, DateTime dateToProcess, int? vpStatementId = null, int? visitPayUserId = null);
        void QueueScheduledPaymentReminders(DateTime dateToProcess);
    }
}

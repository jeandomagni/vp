﻿namespace Ivh.Application.FinanceManagement.Common.Interfaces
{
    using System;
    using System.Threading.Tasks;
    using Ivh.Common.Base.Interfaces;
    using Ivh.Common.JobRunner.Common.Interfaces;
    using Ivh.Common.ServiceBus.Interfaces;
    using Ivh.Common.VisitPay.Jobs;
    using Ivh.Common.VisitPay.Messages.FinanceManagement;

    public interface IStatementCreationApplicationService : IApplicationService,
        IUniversalConsumerService<CreateStatementMessage>,
        IJobRunnerService<QueueStatementsJobRunner>
    {
        bool QueueAllPendingStatementGuarantors(DateTime date);
        Task CreateStatementForGuarantorAsync(int vpGuarantorId, DateTime dateToProcess, bool isImmediateStatement);
    }
}
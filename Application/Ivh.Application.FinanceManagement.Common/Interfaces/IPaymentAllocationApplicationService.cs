﻿namespace Ivh.Application.FinanceManagement.Common.Interfaces
{
    using System.Collections.Generic;
    using Dtos;

    public interface IPaymentAllocationApplicationService
    {
        void ClearPaymentAllocations(IList<int> paymentAllocationIds);
        List<FinancePlanPaymentDto> GetFinancePlanPaymentAmounts(decimal paymentAmount, FinancePlanStatementDto financePlanStatementDto);
    }
}
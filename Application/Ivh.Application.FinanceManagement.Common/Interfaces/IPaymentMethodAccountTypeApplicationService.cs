﻿
namespace Ivh.Application.FinanceManagement.Common.Interfaces
{
    using System.Collections.Generic;
    using Dtos;
    using Ivh.Common.Base.Interfaces;

    public interface IPaymentMethodAccountTypeApplicationService : IApplicationService
    {
        IList<PaymentMethodAccountTypeDto> GetPaymentMethodAccountTypes();
    }
}

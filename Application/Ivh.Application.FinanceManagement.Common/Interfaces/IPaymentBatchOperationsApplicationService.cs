﻿namespace Ivh.Application.FinanceManagement.Common.Interfaces
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Ivh.Common.Base.Interfaces;
    using Ivh.Common.JobRunner.Common.Interfaces;
    using Ivh.Common.VisitPay.Jobs;

    public interface IPaymentBatchOperationsApplicationService : IApplicationService,
        IJobRunnerService<QueuePaymentReconciliationJobRunner>
    {
        Task<int> ReconcilePaymentsAsync();
        Task<int> ReconcilePaymentsAsync(DateTime beginDateTime, DateTime enDateTime);
    }
}
﻿namespace Ivh.Application.FinanceManagement.Common.Interfaces
{
    using System;
    using Ivh.Common.Base.Interfaces;
    using Ivh.Common.JobRunner.Common.Interfaces;
    using Ivh.Common.VisitPay.Jobs;

    public interface IStatementExportApplicationService : IApplicationService,
        IJobRunnerService<StatementExportBatchProcessJobRunner>
    {
        void ExportPaperStatements(DateTime date);
    }
}

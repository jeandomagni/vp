﻿namespace Ivh.Application.FinanceManagement.Common.Interfaces
{
    using System;
    using Ivh.Common.Base.Interfaces;
    using Ivh.Common.VisitPay.Messages.HospitalData.Dtos;

    public interface IPaymentSummaryApplicationService : IApplicationService
    {
        PaymentSummaryDto GetPaymentSummary(int vpGuarantorId, DateTime? postDateBegin, DateTime? postDateEnd);
    }
}

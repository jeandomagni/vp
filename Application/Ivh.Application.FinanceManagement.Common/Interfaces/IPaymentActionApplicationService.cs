﻿namespace Ivh.Application.FinanceManagement.Common.Interfaces
{
    using System;
    using System.Collections.Generic;
    using Dtos;
    using Ivh.Common.Base.Enums;
    using Ivh.Common.Base.Interfaces;
    using Ivh.Common.VisitPay.Enums;

    public interface IPaymentActionApplicationService : IApplicationService
    {
        IList<PaymentActionReasonDto> GetPaymentCancelReasons(PaymentTypeEnum paymentType);
        IList<PaymentActionReasonDto> GetPaymentRescheduleReasons(PaymentTypeEnum paymentType);
        bool IsEligibleToCancel(int vpGuarantorId, int visitPayUserId, PaymentTypeEnum paymentType);
        bool IsEligibleToReschedule(int vpGuarantorId, int visitPayUserId, PaymentTypeEnum paymentType);
        DateTime GetMaxAllowedRescheduleDate(int vpGuarantorId, PaymentTypeEnum paymentType);
        bool CancelPayment(int vpGuarantorId, int actionVisitPayUserId, PaymentCancelDto paymentCancelDto);
        bool ReschedulePayment(int vpGuarantorId, int actionVisitPayUserId, PaymentRescheduleDto paymentRescheduleDto);
    }
}
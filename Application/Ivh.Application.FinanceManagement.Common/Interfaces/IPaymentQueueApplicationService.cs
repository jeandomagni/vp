﻿
namespace Ivh.Application.FinanceManagement.Common.Interfaces
{
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Core.Common.Dtos;
    using Dtos;
    using Ivh.Common.Base.Interfaces;

    public interface IPaymentQueueApplicationService : IApplicationService
    {
        IList<string> GetAllPaymentImportBatchNumbers();
        PaymentImportQueueDto LoadPaymentImportQueue(PaymentImportFilterDto paymentImportFilter);
        Task<byte[]> ExportPaymentImportQueueAsync(PaymentImportFilterDto paymentImportFilter, string visitPayUserName);
    }
}

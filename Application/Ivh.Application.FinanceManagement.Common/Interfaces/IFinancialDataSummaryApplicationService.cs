﻿namespace Ivh.Application.FinanceManagement.Common.Interfaces
{
    using System;
    using Dtos;
    using Ivh.Common.Base.Interfaces;
    using Ivh.Common.JobRunner.Common.Interfaces;
    using Ivh.Common.ServiceBus.Interfaces;
    using Ivh.Common.VisitPay.Jobs;
    using Ivh.Common.VisitPay.Messages.FinanceManagement;

    public interface IFinancialDataSummaryApplicationService : IApplicationService,
        IUniversalConsumerService<BalanceDueReminderMessage>,
        IJobRunnerService<QueueBalanceDueRemindersJobRunner>
    {
        FinancialDataSummaryDto GetFinancialDataSummaryByGuarantor(int guarantorId);
        void QueueBalanceDueReminders(DateTime dateToProcess);
        bool ProcessBalanceDueReminder(BalanceDueReminderMessage message);
    }
}
﻿namespace Ivh.Application.FinanceManagement.Common.Interfaces
{
    using System.Collections.Generic;
    using Ivh.Common.Base.Interfaces;
    using Ivh.Common.JobRunner.Common.Interfaces;
    using Ivh.Common.ServiceBus.Interfaces;
    using Ivh.Common.VisitPay.Jobs;
    using Ivh.Common.VisitPay.Messages.FinanceManagement;

    public interface IVisitTransactionChangesApplicationService : IApplicationService,
        IUniversalConsumerService<CreateVisitReconciliationBalanceChangesForGuarantorMessage>,
        IJobRunnerService<QueueVisitReconciliationJobRunner>
    {
        void CreateVisitReconciliationBalanceChangeMessagesFromVisits(IList<int> visitIds);
        void QueueAllGuarantorsWithAllVisits();
    }
}
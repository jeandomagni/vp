﻿namespace Ivh.Application.FinanceManagement.Common.Interfaces
{
    using System.Threading.Tasks;
    using Dtos;
    using Ivh.Common.Base.Interfaces;
    using Ivh.Common.EventJournal;
    using Ivh.Common.ServiceBus.Interfaces;
    using Ivh.Common.VisitPay.Messages.HospitalData;

    public interface IReconfigureFinancePlanApplicationService : IApplicationService,
        IUniversalConsumerService<UpdateReconfiguredFinancePlanInterestMessage>
    {
        bool HasPendingReconfiguration(int currentVisitPayUserId);
        Task<ReconfigureFinancePlanDto> GetOfflineReconfigurationInformation(int vpGuarantorId);
        Task<ReconfigureFinancePlanDto> GetReconfigurationInformation(int vpGuarantorId, int financePlanIdToReconfigure, int? financePlanOfferSetTypeIdId, bool isClientApplication);
        Task<ReconfigurationActionResponseDto> ReconfigureFinancePlanAsync(int financePlanIdToReconfigure, FinancePlanCalculationParametersDto financePlanCalculationParametersDto, int loggedInVisitPayUserId, JournalEventHttpContextDto httpContext, bool sendEmail);
        Task<ReconfigurationActionResponseDto> AcceptReconfigurationAsync(int financePlanIdToReconfigure, int termsCmsVersionId, int vpGuarantorId, int loggedInVisitPayUserId, JournalEventHttpContextDto httpContext);
        ReconfigurationActionResponseDto CancelReconfiguration(int financePlanIdToReconfigure, int vpGuarantorId, int actionVisitPayUserId, string action, JournalEventHttpContextDto context);
        Task<CalculateFinancePlanTermsResponseDto> GetTermsByMonthlyPaymentAmountAsync(int financePlanId, FinancePlanCalculationParametersDto financePlanCalculationParametersDto);
        Task<CalculateFinancePlanTermsResponseDto> GetTermsByNumberOfMonthlyPaymentsAsync(int financePlanId, FinancePlanCalculationParametersDto financePlanCalculationParametersDto);
    }
}

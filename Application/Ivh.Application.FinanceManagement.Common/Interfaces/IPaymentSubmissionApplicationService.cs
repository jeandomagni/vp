﻿namespace Ivh.Application.FinanceManagement.Common.Interfaces
{
    using System;
    using System.Collections.Generic;
    using Dtos;
    using Ivh.Common.Base.Interfaces;
    using Ivh.Common.JobRunner.Common.Interfaces;
    using Ivh.Common.VisitPay.Messages.HospitalData;
    using Ivh.Common.VisitPay.Jobs;
    using Ivh.Common.ServiceBus.Interfaces;
    using Ivh.Common.VisitPay.Messages.FinanceManagement;

    public interface IPaymentSubmissionApplicationService : IApplicationService,
        IUniversalConsumerService<PaymentImportFileMessage>,
        IJobRunnerService<FixPaymentAllocationsForPaymentJobRunner>
    {
        bool IsDiscountEligible(int vpGuarantorId);
        IList<ProcessPaymentResponse> ProcessPayment(PaymentSubmissionDto paymentSubmissionDto, int visitPayUserId, int actionVisitPayUserId, int? vpGuarantorId = null);
        IList<ProcessPaymentResponse> ProcessHouseholdBalancePayment(PaymentSubmissionDto paymentSubmissionDto, int visitPayUserId, int actionVisitPayUserId);
        IList<ProcessPaymentResponse> ImportLockBoxFinancePlanPayment(decimal paymentAmount, DateTime statementPeriodStateDate, int actionVisitPayUserId, int vpGuarantorId);
        ValidatePaymentResponse ValidatePayment(PaymentSubmissionDto paymentSubmissionDto, int vpGuarantorId);
        ValidatePaymentResponse ValidateRescheduledPayment(int paymentId, int paymentMethodId, DateTime scheduledPaymentDate, int vpGuarantorId);
        DiscountOfferDto GetDiscountPerVisitForStatementedBalanceInFull(int vpGuarantorId);
        void FixPaymentAllocations(int paymentId);
        void PopulatePaymentMethodTypeOnPaymentAllocationMessages(IList<PaymentAllocationMessage> paymentAllocationMessages);
        ProcessPaymentResponse SaveTextToPayPayment(PaymentSubmissionDto paymentSubmissionDto,int vpGuarantorId);
        IList<ProcessPaymentResponse> ChargeTextToPayPayment(int visitPayUserId);
        void ReprocessPaymentImports(IList<PaymentImportDto> paymentImportDtos);

    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ivh.Application.FinanceManagement.Common.Dtos
{
    using Ivh.Common.Base.Enums;
    using Ivh.Common.VisitPay.Enums;

    public class AchFileDto
    {
        public int AchFileId { get; set; }
        public DateTime FileDateFrom { get; set; }
        public DateTime FileDateTo { get; set; }
        public DateTime DownloadedDate { get; set; }
        public DateTime? ProcessedDate { get; set; }
        public AchFileTypeEnum AchFileType { get; set; }
        public Guid FileReferenceToken { get; set; }

        public IList<AchReturnDetailDto> ReturnDetails { get; set; }
        public IList<AchSettlementDetailDto> SettlementDetails { get; set; }
    }
}

﻿namespace Ivh.Application.FinanceManagement.Common.Dtos
{
    using System.Collections.Generic;
    using System.Linq;
    using Ivh.Common.Base.Enums;
    using Ivh.Common.VisitPay.Enums;

    public class FinancePlanCalculationParametersDto
    {
        public static FinancePlanCalculationParametersDto CreateForTermsCheck(int vpGuarantorId, int? financePlanOfferSetTypeId, bool combineFinancePlans, int? paymentDueDay = null, string stateCodeForVisits = null)
        {
            return new FinancePlanCalculationParametersDto(vpGuarantorId, financePlanOfferSetTypeId, OfferCalculationStrategyEnum.MonthlyPayment)
            {
                CombineFinancePlans = combineFinancePlans,
                PaymentDueDay = paymentDueDay,
                StateCodeForVisits = stateCodeForVisits
            };
        }

        public static FinancePlanCalculationParametersDto CreateForMonthlyPayment(int vpGuarantorId, int statementId, decimal monthlyPaymentAmount, int? financePlanOfferSetTypeId, bool combineFinancePlans, int? paymentDueDay = null, List<int> selectedVisits = null, string stateCodeForVisits = null)
        {
            return new FinancePlanCalculationParametersDto(vpGuarantorId, financePlanOfferSetTypeId, OfferCalculationStrategyEnum.MonthlyPayment)
            {
                MonthlyPaymentAmount = monthlyPaymentAmount,
                CombineFinancePlans = combineFinancePlans,
                StatementId = statementId,
                PaymentDueDay = paymentDueDay,
                SelectedVisits = selectedVisits?.ToList(),
                StateCodeForVisits = stateCodeForVisits
            };
        }

        public static FinancePlanCalculationParametersDto CreateForNumberOfPayments(int vpGuarantorId, int statementId, int numberOfMonthlyPayments, int? financePlanOfferSetTypeId, bool combineFinancePlans, int? paymentDueDay = null, List<int> selectedVisits = null, string stateCodeForVisits = null)
        {
            return new FinancePlanCalculationParametersDto(vpGuarantorId, financePlanOfferSetTypeId, OfferCalculationStrategyEnum.NumberOfPayments)
            {
                NumberOfMonthlyPayments = numberOfMonthlyPayments,
                CombineFinancePlans = combineFinancePlans,
                StatementId = statementId,
                PaymentDueDay = paymentDueDay,
                SelectedVisits = selectedVisits?.ToList(),
                StateCodeForVisits = stateCodeForVisits
            };
        }

        public static FinancePlanCalculationParametersDto CreateForOfferBoundary(int vpGuarantorId, int statementId, int? financePlanOfferSetTypeId, bool combineFinancePlans, List<int> selectedVisits = null)
        {
            return new FinancePlanCalculationParametersDto(vpGuarantorId, financePlanOfferSetTypeId, OfferCalculationStrategyEnum.NumberOfPayments)
            {
                CombineFinancePlans = combineFinancePlans,
                StatementId = statementId,
                SelectedVisits = selectedVisits?.ToList()
            };
        }

        public static FinancePlanCalculationParametersDto CreateForSubmit(int vpGuarantorId, int statementId, decimal monthlyPaymentAmount, int? financePlanOfferSetTypeId, OfferCalculationStrategyEnum offerCalculationStrategy, bool combineFinancePlans, string stateCodeForVisits = null)
        {
            return new FinancePlanCalculationParametersDto(vpGuarantorId, financePlanOfferSetTypeId, offerCalculationStrategy)
            {
                MonthlyPaymentAmount = monthlyPaymentAmount,
                CombineFinancePlans = combineFinancePlans,
                StatementId = statementId,
                StateCodeForVisits = stateCodeForVisits
            };
        }

        public static FinancePlanCalculationParametersDto CreateForReconfigurationTermsCheck(int vpGuarantorId, decimal? monthlyPaymentAmount, int? numberOfMonthlyPayments, int? financePlanOfferSetTypeId)
        {
            return new FinancePlanCalculationParametersDto(vpGuarantorId, financePlanOfferSetTypeId, OfferCalculationStrategyEnum.MonthlyPayment)
            {
                MonthlyPaymentAmount = monthlyPaymentAmount,
                NumberOfMonthlyPayments = numberOfMonthlyPayments
            };
        }

        public static FinancePlanCalculationParametersDto CreateForReconfigurationSubmit(int vpGuarantorId, decimal monthlyPaymentAmount, int? financePlanOfferSetTypeId, OfferCalculationStrategyEnum offerCalculationStrategy)
        {
            return new FinancePlanCalculationParametersDto(vpGuarantorId, financePlanOfferSetTypeId, offerCalculationStrategy)
            {
                MonthlyPaymentAmount = monthlyPaymentAmount
            };
        }
        
        private FinancePlanCalculationParametersDto(int vpGuarantorId, int? financePlanOfferSetTypeId, OfferCalculationStrategyEnum offerCalculationStrategy)
        {
            this.VpGuarantorId = vpGuarantorId;
            this.FinancePlanOfferSetTypeId = financePlanOfferSetTypeId;
            this.OfferCalculationStrategy = offerCalculationStrategy;
        }

        public int VpGuarantorId { get; }

        public int? FinancePlanOfferSetTypeId { get; set; }

        public OfferCalculationStrategyEnum OfferCalculationStrategy { get; }

        public int? StatementId { get; private set; }

        public int? NumberOfMonthlyPayments { get; private set; }

        public decimal? MonthlyPaymentAmount { get; private set; }

        public bool CombineFinancePlans { get; private set; }

        public int? PaymentDueDay { get; set; }

        public List<int> SelectedVisits { get; set; }

        public int? FinancePlanTypeId { get; set; } 

        public string StateCodeForVisits { get; set; }
    }
}

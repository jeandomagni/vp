namespace Ivh.Application.FinanceManagement.Common.Dtos
{
    using System;

    public class FinancePlanPaymentDto
    {
        private decimal _paymentAmount;

        public int? FinancePlanId { get; set; }

        public decimal PaymentAmount
        {
            get { return Math.Round(this._paymentAmount, 2); }
            set { this._paymentAmount = Math.Round(value, 2); }
        }
    }
}
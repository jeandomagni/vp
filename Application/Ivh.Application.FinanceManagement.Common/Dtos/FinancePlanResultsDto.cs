﻿namespace Ivh.Application.FinanceManagement.Common.Dtos
{
    using System.Collections.Generic;

    public class FinancePlanResultsDto
    {
        public IList<FinancePlanDto> FinancePlans { get; set; }
        public int TotalRecords { get; set; }
        public decimal TotalCurrentBalance { get; set; }
    }
}

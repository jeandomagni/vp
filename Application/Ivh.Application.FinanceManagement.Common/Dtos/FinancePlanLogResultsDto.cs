﻿namespace Ivh.Application.FinanceManagement.Common.Dtos
{
    using System.Collections.Generic;

    public class FinancePlanLogResultsDto
    {
        public IList<FinancePlanLogDto> FinancePlanLogs { get; set; }
        public int TotalRecords { get; set; }
    }
}
﻿namespace Ivh.Application.FinanceManagement.Common.Dtos
{
    public class FinancePlanStatementVisitDto
    {
        #region SnapShot Values Vp2 & Vp3V1
        #endregion
        public virtual int FinancePlanId { get; set; }
        public virtual int VisitId { get; set; }
        #region SnapShot Values Vp2
        public virtual decimal? StatementedSnapshotVpFees { get; set; }
        public virtual decimal? StatementedSnapshotVpInterestAssessed { get; set; }
        #endregion
        
        #region SnapShot Values Vp3V1
        public virtual decimal? StatementedSnapshotPreviousBalance { get; set; }
        public virtual decimal? StatementedSnapshotInterestCharged { get; set; }
        public virtual decimal? StatementedSnapshotInterestPaid { get; set; }
        public virtual decimal? StatementedSnapshotPrincipalPaid { get; set; }
        public virtual decimal? StatementedSnapshotOtherBalanceCharges { get; set; }
        public virtual decimal? StatementedSnapshotOriginalBalance { get; set; }
        public virtual decimal? StatementedSnapshotTotalBalance { get; set; }

        // The following properties are mapped with a VisitDto 
        public virtual string VisitDisplayDate { get; set; }
        public virtual string PatientName { get; set; }
        public virtual string SourceSystemKey { get; set; }
        public virtual string VisitDescription { get; set; }
        #endregion
        
        public string ServiceGroupIndicator { get; set; }
    }
}
﻿namespace Ivh.Application.FinanceManagement.Common.Dtos
{
    using System;
    using System.Collections.Generic;
    using Content.Common.Dtos;
    using Core.Common.Dtos;
    using Ivh.Common.Base.Enums;
    using Ivh.Common.VisitPay.Enums;
    using User.Common.Dtos;

    public class UserPaymentOptionDto
    {
        public UserPaymentOptionDto()
        {
            this.HouseholdBalances = new List<HouseholdBalanceDto>();
            this.ManagedOffers = new Dictionary<VisitPayUserDto, DiscountOfferDto>();
            this.FinancePlans = new List<FinancePlanDto>();
            this.Payments = new List<ResubmitPaymentDto>();
            this.Visits = new List<VisitDto>();
        }

        // common
        public PaymentOptionEnum PaymentOptionId { get; set; }
        public decimal? UserApplicableBalance { get; set; }
        public PaymentDateTypeEnum? DateBoundaryType { get; set; }
        public DateTime? DateBoundary { get; set; }
        public virtual PaymentDateTypeEnum DefaultPaymentDateType { get; set; }
        public DateTime DefaultPaymentDate { get; set; }
        public decimal? MaximumPaymentAmount { get; set; }
        public CmsVersionDto CmsVersion { get; set; }
        public bool IsAmountReadonly { get; set; }
        public bool IsDateReadonly { get; set; }
        public bool IsExclusive { get; set; }
        public bool IsEnabled { get; set; }
        public bool IsVisibleWhenNotEnabled { get; set; }

        // discount
        public bool IsDiscountEligible { get; set; }
        public bool IsDiscountPromptPayOnly { get; set; }
        public decimal? DiscountAmount { get; set; }
        public DiscountOfferDto DiscountOffer { get; set; }
        public IDictionary<VisitPayUserDto, DiscountOfferDto> ManagedOffers { get; set; }

        // specific
        public IList<FinancePlanDto> FinancePlans { get; set; }
        public IList<HouseholdBalanceDto> HouseholdBalances { get; set; }
        public IList<ResubmitPaymentDto> Payments { get; set; }
        public IList<VisitDto> Visits { get; set; }

        // finance plan
        public FinancePlanConfigurationDto FinancePlanDefaultConfiguration { get; set; }
        public FinancePlanConfigurationDto FinancePlanCombinedConfiguration { get; set; }
        public IList<VisitDto> FinancePlanVisits { get; set; }

        public decimal FinancePlanTotalMonthlyPaymentAmount { get; set; }
        public bool CanSetPaymentDueDay { get; set; }
        public int SuggestedPaymentDueDay { get; set; }
        public bool HasPendingResubmitPayments { get; set; }
        public IList<VisitDto> SelectedVisits { get; set; }
        public decimal? AmountToFinance { get; set; }
        public string FinancePlanOptionStateName { get; set; }
        public string FinancePlanOptionStateCode { get;set; }
        public bool OtherFinancePlanStatesAreAvailable { get; set; }

    }
}
﻿
namespace Ivh.Application.FinanceManagement.Common.Dtos
{
    using System.Collections.Generic;

    public class GuarantorWithLastFinancePlanStatementResultsDto
    {
        public IReadOnlyList<GuarantorWithLastFinancePlanStatementDto> Guarantors { get; set; }
        public int TotalRecords { get; set; }
    }
}

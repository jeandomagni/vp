﻿namespace Ivh.Application.FinanceManagement.Common.Dtos
{
    using System;
    using System.Collections.Generic;
    using Core.Common.Dtos;

    public class FinancialDataSummaryDto
    {
        public decimal TotalBalance { get; set; }
        public int? CurrentStatementId { get; set; }
        public decimal BalanceInFull { get; set; }
        public DateTime? StatementNextPaymentDueDate { get; set; }
        public bool IsGracePeriod { get; set; }
        public bool IsAwaitingStatement { get; set; }
        public DateTime NextStatementDate { get; set; }
        public DateTime? NextPaymentDateForDisplay { get; set; }
        public IList<VisitDto> Visits { get; set; }
        public IList<VisitDto> SelectedVisits { get; set; }
    }
}
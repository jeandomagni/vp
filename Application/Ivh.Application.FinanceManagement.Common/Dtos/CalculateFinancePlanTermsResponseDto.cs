﻿namespace Ivh.Application.FinanceManagement.Common.Dtos
{
    using Ivh.Common.VisitPay.Enums;

    public class CalculateFinancePlanTermsResponseDto
    {
        public CalculateFinancePlanTermsResponseDto(string errorMessage, FinancePlanBoundaryDto financePlanBoundaryDto)
        {
            this.IsError = true;
            this.ErrorMessage = errorMessage;
            this.Terms = null;
            this.MaximumMonthlyPayments = financePlanBoundaryDto?.MaximumNumberOfPayments;
            this.MinimumPaymentAmount = financePlanBoundaryDto?.MinimumPaymentAmount;
        }

        public CalculateFinancePlanTermsResponseDto(FinancePlanTermsDto terms, FinancePlanBoundaryDto financePlanBoundaryDto)
        {
            this.ErrorMessage = null;
            this.IsError = false;
            this.Terms = terms;
            this.MaximumMonthlyPayments = financePlanBoundaryDto?.MaximumNumberOfPayments;
            this.MinimumPaymentAmount = financePlanBoundaryDto?.MinimumPaymentAmount;
        }
        
        public CalculateFinancePlanTermsResponseDto(FinancePlanTermsDto terms, FinancePlanStatusEnum financePlanStatus, FinancePlanDto financePlanDto)
        {
            this.ErrorMessage = null;
            this.IsError = false;
            this.Terms = terms;
            this.FinancePlan = financePlanDto;
            this.FinancePlanStatus = financePlanStatus;
        }

        public string ErrorMessage { get; set; }

        public bool IsError { get; set; }
        
        public FinancePlanDto FinancePlan { get; }

        public FinancePlanStatusEnum FinancePlanStatus { get; }

        public FinancePlanTermsDto Terms { get; }

        public int? MaximumMonthlyPayments { get; }
        
        public decimal? MinimumPaymentAmount { get; }
    }
}

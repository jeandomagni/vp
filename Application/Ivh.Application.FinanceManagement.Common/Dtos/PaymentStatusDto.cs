﻿namespace Ivh.Application.FinanceManagement.Common.Dtos
{
    using Ivh.Common.Base.Enums;
    using Ivh.Common.VisitPay.Enums;

    public class PaymentStatusDto
    {
        public PaymentStatusEnum PaymentStatusEnum { get; set; }
        public string PaymentStatusName { get; set; }
        public string PaymentStatusDisplayName { get; set; }
        public int DisplayOrder { get; set; }
    }
}
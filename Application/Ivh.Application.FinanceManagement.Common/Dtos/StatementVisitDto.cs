﻿namespace Ivh.Application.FinanceManagement.Common.Dtos
{
    using Core.Common.Dtos;
    using Ivh.Common.VisitPay.Enums;

    public class StatementVisitDto
    {
        public decimal StatementedSnapshotVisitBalance { get; set; }

        public VisitStateEnum StatementedSnapshotVisitState { get; set; }

        public string StatementedSnapshotDisplayStatus { get; set; }

        public int StatementedSnapshotAgingCount { get; set; }

        public VisitDto Visit { get; set; }

        public int VisitId => this.Visit?.VisitId ?? 0;
        
        public decimal CurrentUnadjustedStatementedVisitBalance { get; set; }
        
        public decimal CurrentRecalledVisitBalance { get; set; }

        public decimal CurrentVpPayments { get; set; }

        public decimal CurrentVpDiscounts { get; set; }

        public decimal CurrentHsPaymentSum { get; set; }

        public decimal CurrentAdjustmentSum { get; set; }

        public string ServiceGroupIndicator { get; set; }
    }
}

namespace Ivh.Application.FinanceManagement.Common.Dtos
{
    public class PaymentSummaryDto
    {
        public string What { get; set; }
        public string When { get; set; }
        public string From { get; set; }
    }
}
namespace Ivh.Application.FinanceManagement.Common.Dtos
{
    public class ProcessPaymentResponse
    {
        public ProcessPaymentResponse()
        {
            
        }

        public ProcessPaymentResponse(bool isError, string errorMessage)
        {
            this.IsError = isError;
            this.ErrorMessage = errorMessage;
        }

        public ProcessPaymentResponse(bool isError, string errorMessage, PaymentDto payment)
        {
            this.IsError = isError;
            this.ErrorMessage = errorMessage;
            this.Payment = payment;
        }

        public bool IsError { get; set; }
        public string ErrorMessage { get; set; }
        public PaymentDto Payment { get; set; }
    }
}
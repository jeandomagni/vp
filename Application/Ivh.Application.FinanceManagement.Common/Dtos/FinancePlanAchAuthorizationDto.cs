﻿namespace Ivh.Application.FinanceManagement.Common.Dtos
{
    using System;
    using Core.Common.Dtos;

    public class FinancePlanAchAuthorizationDto
    {
        public decimal MonthlyPaymentAmount { get; set; }

        public int MonthsRemaining { get; set; }

        public DateTime OriginationDate { get; set; }

        public GuarantorDto Guarantor { get; set; }
    }
}
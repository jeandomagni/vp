﻿namespace Ivh.Application.FinanceManagement.Common.Dtos
{
    using System;

    public class CancelRecurringPaymentDto
    {
        public CancelRecurringPaymentDto(int financePlanId, DateTime dueDate, decimal amount)
        {
            this.FinancePlanId = financePlanId;
            this.DueDate = dueDate;
            this.Amount = amount;
        }

        public int FinancePlanId { get; }

        public DateTime DueDate { get; }

        public decimal Amount { get; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ivh.Application.FinanceManagement.Common.Dtos
{
    public class AchSettlementDetailDto
    {
        public int AchSettlementDetailId { get; set; }
        public AchFileDto AchFile { get; set; }
        public DateTime DownloadedDate { get; set; }
        public DateTime? ProcessedDate { get; set; }

        public string PaidGroup { get; set; }
        public string DebitAmount1 { get; set; }
        public string CreditAmount1 { get; set; }
        public string FileName { get; set; }
        public string ProcessingDate { get; set; }
        public string Textbox21 { get; set; }
        public string IndividualId { get; set; }
        public string DebitAmount { get; set; }
        public string CreditAmount { get; set; }
        public string SecCode { get; set; }
        public string Status { get; set; }
        public string ReturnCode { get; set; }
        public string ReturnDate { get; set; }
        public string ReturnAddenda { get; set; }
        public string DishonorReason { get; set; }
        public string Textbox4 { get; set; }
        public string Textbox5 { get; set; }
        public string Textbox6 { get; set; }
        public string Textbox7 { get; set; }
    }
}

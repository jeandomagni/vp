﻿namespace Ivh.Application.FinanceManagement.Common.Dtos
{
    using Ivh.Common.VisitPay.Enums;

    public class PaymentCancelDto
    {
        public int? PaymentId { get; set; }

        public PaymentTypeEnum PaymentType { get; set; }

        public int? PaymentCancelReasonId { get; set; }

        public string PaymentCancelDescription { get; set; }
    }
}
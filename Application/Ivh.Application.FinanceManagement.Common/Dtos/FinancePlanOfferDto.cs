﻿namespace Ivh.Application.FinanceManagement.Common.Dtos
{
    using System;
    using Core.Common.Dtos;

    public class FinancePlanOfferDto
    {
        public int FinancePlanOfferId { get; set; }

        public GuarantorDto VpGuarantor { get; set; }

        public StatementDto VpStatement { get; set; }

        public virtual decimal StatementedCurrentAccountBalance { get; set; }

        public virtual int DurationRangeStart { get; set; }

        public virtual int DurationRangeEnd { get; set; }

        public virtual decimal MinPayment { get; set; }

        public virtual decimal MaxPayment { get; set; }

        public virtual int InterestFreePeriod { get; set; }

        public virtual decimal InterestRate { get; set; }

        public virtual bool IsActive { get; set; }

        public virtual DateTime InsertDate { get; set; }

        public virtual bool IsCharity { get; set; }
    }
}

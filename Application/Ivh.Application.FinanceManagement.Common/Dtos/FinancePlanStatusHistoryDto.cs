﻿using System;

namespace Ivh.Application.FinanceManagement.Common.Dtos
{
    public class FinancePlanStatusHistoryDto
    {
        public int FinancePlanStatusHistoryId { get; set; }
        public DateTime InsertDate { get; set; }
        public FinancePlanStatusDto FinancePlanStatus { get; set; }
        public string ChangeDescription { get; set; }
        public FinancePlanDto FinancePlan { get; set; }
    }
}

﻿namespace Ivh.Application.FinanceManagement.Common.Dtos
{
    public class ReallocateInterestToPrincipalDto
    {
        public bool IsSuccessful { get;set; }
        public string ErrorMessage { get; set; }

        public decimal TotalToReallocate { get; set; }

        public decimal InterestAssessed { get; set; }
        public decimal InterestPaid { get; set; } //Expecting this to be positive, normally it's negative since it's coming from payments

        public decimal InterestAssessedButNotPaid => this.InterestAssessed - this.InterestPaid;

        public decimal AmountToReallocate
        {
            get
            {
                if (this.TotalToReallocate > this.InterestPaid)
                {
                    return this.InterestPaid;
                }

                return this.TotalToReallocate;
            }
        }

        public decimal AmountToWriteOff
        {
            get
            {
                decimal remainderAfterReallocate = (this.TotalToReallocate - this.AmountToReallocate);
                if (remainderAfterReallocate > 0m)
                {
                    if (remainderAfterReallocate > this.InterestAssessedButNotPaid)
                    {
                        return this.InterestAssessedButNotPaid;
                    }

                    return remainderAfterReallocate;
                }

                return 0m;
            }
        }

        public int PaymentId { get; set; }
    }
}
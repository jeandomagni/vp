﻿namespace Ivh.Application.FinanceManagement.Common.Dtos
{
    using System;
    using Ivh.Common.VisitPay.Enums;
    using ProtoBuf;

    [ProtoContract]
    public class FinancePlanTermsDto
    {
        [ProtoMember(1)]
        public int FinancePlanOfferId { get; set; }

        [ProtoMember(2)]
        public DateTime FinalPaymentDate { get; set; }

        [ProtoMember(3)]
        public decimal? FinancePlanBalance { get; set; }

        [ProtoMember(4)]
        public int? FinancePlanId { get; set; }

        [ProtoMember(5)]
        public decimal InterestRate { get; set; }

        [ProtoMember(6)]
        public decimal MonthlyPaymentAmount { get; set; }

        [ProtoMember(7)]
        public decimal MonthlyPaymentAmountLast { get; set; }

        [ProtoMember(8)]
        public DateTime NextPaymentDueDate { get; set; }

        [ProtoMember(9)]
        public int NumberMonthlyPayments { get; set; }

        [ProtoMember(10)]
        public decimal TotalInterest { get; set; }

        [ProtoMember(11)]
        public decimal MonthlyPaymentAmountOtherPlans { get; set; }

        [ProtoMember(12)]
        public decimal MonthlyPaymentAmountTotal { get; set; }

        [ProtoIgnore]
        public FinancePlanOfferSetTypeDto FinancePlanOfferSetType { get; set; }

        [ProtoMember(13)]
        public DateTime EffectiveDate { get; set; }

        [ProtoMember(14)]
        public int PaymentDueDay { get; set; }

        [ProtoMember(15)]
        public bool RequireCreditAgreement { get; set; }

        [ProtoMember(16)]
        public bool IsCombined { get; set; }

        [ProtoMember(17)]
        public int VpGuarantorId { get; set; }

        [ProtoMember(18)]
        public Guid ContractNumber { get; set; }

        [ProtoMember(19)]
        public decimal RolloverBalance { get; set; }

        [ProtoMember(20)]
        public decimal RolloverInterest { get; set; }

        [ProtoMember(21)]
        public decimal MonthlyPaymentAmountPrevious { get; set; }

        [ProtoMember(22)]
        public decimal DownpaymentAmount { get; set; }
        
        [ProtoMember(23)]
		public CmsRegionEnum TermsCmsRegionEnum { get; set; }

        [ProtoMember(24)]
        public string TermsAgreementTitle { get; set; }

        [ProtoMember(25)]
        public int TermsCmsVersionId { get; set; }

        [ProtoMember(26)]
        public bool? IsRetailInstallmentContract { get; set; }

        [ProtoIgnore]
        public bool LastPaymentAmountIsDifferentThanMonthlyAmount => (this.MonthlyPaymentAmount != this.MonthlyPaymentAmountLast);

        [ProtoIgnore]
        public int NumberOfEqualPayments => (this.LastPaymentAmountIsDifferentThanMonthlyAmount)
            ? this.NumberMonthlyPayments - 1
            : this.NumberMonthlyPayments;
    }
}

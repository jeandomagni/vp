﻿
namespace Ivh.Application.FinanceManagement.Common.Dtos
{
    using System;
    using Ivh.Common.VisitPay.Enums;

    public class PaymentImportFilterDto
    {
        public DateTime? DateRangeFrom { get; set; }
        public DateTime? DateRangeTo { get; set; }
        public PaymentImportStateEnum? PaymentImportState { get; set; }
        public string BatchNumber { get; set; }
    }
}

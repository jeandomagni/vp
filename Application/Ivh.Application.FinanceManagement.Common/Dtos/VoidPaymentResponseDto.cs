namespace Ivh.Application.FinanceManagement.Common.Dtos
{
    public class VoidPaymentResponseDto
    {
        public bool IsError { get; set; }
        public string ErrorMessage { get; set; }
        public string TransactionId { get; set; }
    }
}
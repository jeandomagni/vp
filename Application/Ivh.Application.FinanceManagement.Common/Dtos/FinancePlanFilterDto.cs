﻿namespace Ivh.Application.FinanceManagement.Common.Dtos
{
    using System;
    using System.Collections.Generic;
    using Ivh.Common.Base.Enums;
    using Ivh.Common.VisitPay.Enums;

    public class FinancePlanFilterDto
    {
        public FinancePlanFilterDto()
        {
            this.FinancePlanStatusIds = new List<FinancePlanStatusEnum>();
        }

        public List<int> FinancePlansIds = new List<int>(); 
        public int? FinancePlanId { get; set; }
        public IList<FinancePlanStatusEnum> FinancePlanStatusIds { get; set; }
        public DateTime? OriginationDateRangeFrom { get; set; }
        public DateTime? OriginationDateRangeTo { get; set; }
        public DateTime? OriginationDateSince { get; set; }

        public string SortField { get; set; }

        /// <summary>
        /// "asc" or "desc", not case sensitive
        /// </summary>
        public string SortOrder { get; set; }
    }
}

﻿namespace Ivh.Application.FinanceManagement.Common.Dtos
{
    using System;
    using System.Collections.Generic;
    using Core.Common.Dtos;
    using Ivh.Common.VisitPay.Enums;

    public class PaymentHistoryDto
    {
        public string ActionTakenBy { get; set; }
        public virtual GuarantorDto GuarantorDto { get; set; }
        public bool HasRemovedVisits { get; set; }
        public DateTime InsertDate { get; set; }
        public bool IsReversalPaymentType { get; set; }
        public virtual GuarantorDto MadeByGuarantorDto { get; set; }
        public decimal NetPaymentAmount { get; set; }
        public int? ParentPaymentProcessorResponseId { get; set; }
        public List<int> PaymentIds { get; set; }
        public PaymentMethodDto PaymentMethod { get; set; }
        public int PaymentProcessorResponseId { get; set; }
        public string PaymentProcessorSourceKey { get; set; }
        public PaymentReversalStatusEnum PaymentReversalStatusEnum { get; set; }
        public PaymentStatusEnum PaymentStatus { get; set; }
        public string PaymentStatusDisplayName { get; set; }
        public string PaymentStatusMessage { get; set; }
        public PaymentTypeEnum PaymentType { get; set; }
        public decimal SnapshotTotalPaymentAmount { get; set; }
        public decimal UnallocatedAmountSum { get; set; }
        public bool IsPreviousConsolidation { get; set; }
        public PaymentRefundableTypeEnum PaymentRefundableType { get; set; }
    }
}
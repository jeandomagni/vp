namespace Ivh.Application.FinanceManagement.Common.Dtos
{
    using System;
    using System.Collections.Generic;
    using Ivh.Common.Base.Enums;
    using Ivh.Common.VisitPay.Enums;

    public class PaymentSubmissionDto
    {
        private decimal _totalPaymentAmount;

        public PaymentSubmissionDto()
        {
            this.FinancePlanPayments = new List<FinancePlanPaymentDto>();
            this.PaymentsToResubmit = new List<ResubmitPaymentDto>();
            this.VisitPayments = new List<VisitPaymentDto>();
        }

        public bool IsHouseholdBalance { get; set; }
        public DateTime PaymentDate { get; set; }
        public int PaymentMethodId { get; set; }
        public PaymentTypeEnum PaymentType { get; set; }

        public decimal TotalPaymentAmount
        {
            get => Math.Round(this._totalPaymentAmount, 2);
            set => this._totalPaymentAmount = Math.Round(value, 2);
        }

        public IList<int> HouseholdStatements { get; set; }
        public IList<VisitPaymentDto> VisitPayments { get; set; }
        public IList<FinancePlanPaymentDto> FinancePlanPayments { get; set; }
        public IList<ResubmitPaymentDto> PaymentsToResubmit { get; set; }
    }
}
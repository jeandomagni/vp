﻿namespace Ivh.Application.FinanceManagement.Common.Dtos
{
    public class InterestRateDto
    {
        public int InterestRateId { get; set; }
        public int DurationRangeStart { get; set; }
        public int DurationRangeEnd { get; set; }
        public decimal Rate { get; set; }
        public int InterestFreePeriod { get; set; }
        public string DisplayText { get; set; }
        public bool? IsCharity { get; set; }
    }
}

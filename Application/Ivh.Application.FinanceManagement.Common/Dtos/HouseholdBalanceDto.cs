﻿namespace Ivh.Application.FinanceManagement.Common.Dtos
{
    using User.Common.Dtos;

    public class HouseholdBalanceDto
    {
        public int VpStatementId { get; set; }
        public decimal TotalBalance { get; set; }
        public decimal DiscountAmount { get; set; }
        public bool IsManaged { get; set; }
        public bool IsDiscountEligible { get; set; }
        public VisitPayUserDto VisitPayUser { get; set; }
        public DiscountOfferDto DiscountOffer { get; set; }
    }
}
﻿namespace Ivh.Application.FinanceManagement.Common.Dtos
{
    using Ivh.Common.Base.Enums;
    using Ivh.Common.VisitPay.Enums;

    public class StatementAlertDto
    {
        public AlertTypeEnum AlertType { get; set; }
        public string AlertMessageWeb { get; set; }
        public string AlertMessagePdf { get; set; }
        public string ControllerAction { get; set; }
        public string ControllerName { get; set; }
        public bool RequireUrlReplacement { get; set; }
    }
}
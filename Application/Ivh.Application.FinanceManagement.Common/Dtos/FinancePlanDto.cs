﻿namespace Ivh.Application.FinanceManagement.Common.Dtos
{
    using System;
    using System.Collections.Generic;
    using Core.Common.Dtos;
    using Ivh.Common.Base.Enums;
    using Ivh.Common.VisitPay.Attributes;
    using Ivh.Common.VisitPay.Enums;

    public class FinancePlanDto
    {
        public FinancePlanDto()
        {
            this.FinancePlanVisits = new List<FinancePlanVisitDto>();
            this.FinancePlanInterestRateHistory = new List<FinancePlanInterestRateHistoryDto>();
        }

        public int FinancePlanId { get; set; }

        public GuarantorDto VpGuarantor { get; set; }

        public FinancePlanOfferDto FinancePlanOffer { get; set; }
        
        public FinancePlanStatusDto FinancePlanStatus { get; set; }
        
        public DateTime InsertDate { get; set; }

        public DateTime? OriginationDate { get; set; }

        public DateTime? TermsAgreedDate { get; set; }

        public int? TermsCmsVersionId { get; set; }

        public decimal OriginatedFinancePlanBalance { get; set; }

        public decimal? InterestAssessed { get; set; }

        public decimal CurrentFinancePlanBalance { get; set; }

        public decimal CurrentFinancePlanBalanceIncludingAllAdjustments { get; set; }

        public decimal OriginalInterestRate { get; set; }

        public decimal CurrentInterestRate { get; set; }

        public decimal PaymentAmount { get; set; }

        //Amount Due including past due amount
        public decimal AmountDue { get; set; }

        public int? OriginalDuration { get; set; }

        public decimal? OriginalPaymentAmount { get; set; }

        public decimal PrincipalPaidToDate { get; set; }

        public decimal InterestPaidToDate { get; set; }

        public int CurrentBucket { get; set; }

        public decimal PastDueAmount { get; set; }

        public decimal? AdjustmentsSinceOrigination { get; set; }

        public StatementDto CreatedVpStatement { get; set; }

        public FinancePlanStatusDto FinancePlanStatusSource { get; set; }

        public IList<FinancePlanVisitDto> FinancePlanVisits { get; set; }

        public IList<FinancePlanInterestRateHistoryDto> FinancePlanInterestRateHistory { get; set; }
        
        public DateTime? LastPaymentDate { get; set; }

        public int MonthsRemaining { get; set; }

        public bool IsEligibleForReconfiguration { get; set; }

        public int? ReconfiguredFinancePlanId { get; set; }

        public int? OriginalFinancePlanId { get; set; }

        public bool IsCanceled { get; set; }

        public bool IsPending { get; set; }

        public bool IsPastDue { get; set; }

        public bool IsActiveUncollectable { get; set; }

        public bool IsUncollectable { get; set; }

        public bool IsCombined { get; set; }

        public OfferCalculationStrategyEnum OfferCalculationStrategy { get; set; }

        public int? CustomizedVisitPayUserId { get; set; }

        public int? AcceptedTermsVisitPayUserId { get; set; }

        public int FinancePlanOfferSetTypeId { get; set; }

        public bool IsFinancePlanOfferTypePatient { get; set; }

        public bool IsFinancePlanOfferTypeClientOnly { get; set; }

        public string FinancePlanUniqueId { get; set; }

        public DateTime? MaxScheduledPaymentDate { get; set; }

        public decimal CurrentAmount { get; set; }

        public DateTime? FirstPaymentDueDate { get; set; }

        public bool IsRetailInstallmentContract { get; set; }

        [Phi]
        public string FinancePlanPublicIdPhi { get; set; }
    }
}

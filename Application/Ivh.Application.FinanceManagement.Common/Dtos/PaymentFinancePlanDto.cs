﻿namespace Ivh.Application.FinanceManagement.Common.Dtos
{
    using System;
    using Ivh.Common.Base.Enums;
    using Ivh.Common.VisitPay.Enums;

    public class PaymentFinancePlanDto
    {
        public int FinancePlanId { get; set; }
        public int CurrentBucket { get; set; }
        public FinancePlanStatusEnum FinancePlanStatus { get; set; }
        public decimal CurrentFinancePlanBalance { get; set; }
        public DateTime? OriginationDate { get; set; }
    }
}
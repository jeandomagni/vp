﻿namespace Ivh.Application.FinanceManagement.Common.Dtos
{
    using Core.Common.Dtos;

    public class GuarantorWithLastStatementDto
    {
        public GuarantorDto Guarantor { get; set; }
        public StatementDto LastStatement { get; set; }
        public bool IsAwaitingStatement { get; set; }
    }
}

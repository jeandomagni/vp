namespace Ivh.Application.FinanceManagement.Common.Dtos
{
    using Ivh.Common.VisitPay.Enums;

    public class FinancePlanTypeDto
    {
        public int FinancePlanTypeId { get; set; }
        public FinancePlanTypeEnum FinancePlanTypeEnum { get; set; }
        public string Name { get; set; }
        public bool RequireCreditAgreement { get; set; }
        public bool RequireEsign { get; set; }
        public bool RequirePaymentMethod { get; set; }
        public bool RequireEmailCommunication { get; set; }
        public bool RequirePaperCommunication { get; set; }
        public bool IsAutoPay { get; set; }
        public bool IsPaymentMethodEditable { get; set; }
    }
}
namespace Ivh.Application.FinanceManagement.Common.Dtos
{
    public class HealthEquityBalanceResponse
    {
        public string Ssn { get; set; }
        public decimal CashBalance { get; set; }
        public decimal InvestmentBalance { get; set; }
        public decimal ContributionsYtd { get; set; }
        public decimal DistributionsYtd { get; set; }
        public bool WasSuccessful { get; set; }
        public bool? AuthTokenExpired { get; set; }
    }
}
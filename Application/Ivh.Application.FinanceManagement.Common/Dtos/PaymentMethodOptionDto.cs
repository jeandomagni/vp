namespace Ivh.Application.FinanceManagement.Common.Dtos
{
    public class PaymentMethodOptionDto
    {
        public PaymentMethodDto PaymentMethod { get; set; }

        public bool IsSelected { get; set; }
    }
}
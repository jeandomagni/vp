﻿namespace Ivh.Application.FinanceManagement.Common.Dtos
{
    using System.Collections.Generic;

    public class FinancePlanBucketHistoryResultsDto
    {
        public IReadOnlyList<FinancePlanBucketHistoryDto> FinancePlanBucketHistories { get; set; }
        public int TotalRecords { get; set; }
    }
}
﻿namespace Ivh.Application.FinanceManagement.Common.Dtos
{
    using System;
    using Ivh.Common.Base.Enums;
    using Ivh.Common.VisitPay.Enums;

    public class ResubmitPaymentDto
    {
        public decimal SnapshotTotalPaymentAmount { get; set; }
        public DateTime LastPaymentFailureDate { get; set; }
        public PaymentTypeEnum PaymentType { get; set; }
        public int FinancePlanId { get; set; }
    }
}
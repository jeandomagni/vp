﻿namespace Ivh.Application.FinanceManagement.Common.Dtos
{
    public class PaymentActionReasonDto
    {
        public int PaymentActionReasonId { get; set; }
        public string Description { get; set; }
        public bool RequireInput { get; set; }
    }
}
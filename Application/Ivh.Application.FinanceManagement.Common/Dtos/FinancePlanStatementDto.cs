﻿namespace Ivh.Application.FinanceManagement.Common.Dtos
{
    using System;
    using System.Collections.Generic;
    using Ivh.Common.Base.Utilities.Helpers;
    using Ivh.Common.VisitPay.Enums;

    public class FinancePlanStatementDto
    {
        public FinancePlanStatementDto()
        {
            this.ServiceGroupDisclaimers = new List<string>();
        }

        public int FinancePlanStatementId { get; set; }
        public VpStatementVersionEnum StatementVersion { get; set; }
        public DateTime PeriodStartDate { get; set; }
        public DateTime PeriodEndDate { get; set; }
        public DateTime PaymentDueDate { get; set; }
        public DateTime StatementDate { get; set; }
        public int DaysInBillingCycle { get; set; }
        public int StatementYear { get; set; }

        public decimal DisplayStatementBalance { get; set; }

        public IList<FinancePlanStatementFinancePlanDto> FinancePlanStatementFinancePlans { get; set; } = new List<FinancePlanStatementFinancePlanDto>();
        public IList<FinancePlanStatementVisitDto> FinancePlanStatementVisits { get; set; } = new List<FinancePlanStatementVisitDto>();
        public IList<string> Notifications { get; set; } = new List<string>();
        public IList<string> ServiceGroupDisclaimers { get; set; }

        #region visit pay user and guarantor 

        public int GuarantorRegistrationYear { get; set; }
        public string GuarantorVisitPayUserFirstName { get; set; }
        public string GuarantorVisitPayUserLastName { get; set; }
        public string GuarantorVisitPayUserLastNameFirstName => FormatHelper.LastNameFirstName(this.GuarantorVisitPayUserLastName,this.GuarantorVisitPayUserFirstName);
        public string GuarantorVisitPayUserFirstNameLastName => FormatHelper.FirstNameLastName(this.GuarantorVisitPayUserFirstName,this.GuarantorVisitPayUserLastName);

        public string GuarantorAddressStreet1 { get; set; }
        public string GuarantorAddressStreet2 { get; set; }
        public string GuarantorCityStateZip { get; set; }
        

        #endregion

        #region SnapShot Values Vp2 & Vp3V1
        public decimal? StatementedSnapshotFinancePlanBalanceSum { get; set; }
        public decimal? StatementedSnapshotFinancePlanInterestChargedSum { get; set; }
        public decimal? StatementedSnapshotFinancePlanPaymentDueSum { get; set; }
        #endregion

        #region SnapShot Values Vp2
        public decimal StatementedSnapshotFinancePlansBalance { get; set; }
        public decimal? StatementedSnapshotFinancePlanInterestDueSum { get; set; }
        public decimal? StatementedSnapshotFinancePlanPrincipalDueSum { get; set; }
        public decimal? StatementedSnapshotMinimumAmountDue { get; set; }
        public decimal? StatementedSnapshotPastDueFinancePlanBalanceSum { get; set; }
        #endregion

        #region SnapShot Values Vp3V1
        public decimal? StatementedSnapshotFinancePlanInterestChargedForStatementYearSum { get; set; }
        public decimal? StatementedSnapshotFinancePlanFeesChargedForStatementYearSum { get; set; }
        public bool IsFirstStatement { get; set; }
        #endregion

    }
}
﻿namespace Ivh.Application.FinanceManagement.Common.Dtos
{
    using System;

    public class FinancePlanStatementFinancePlanDto
    {       
        #region SnapShot Values Vp2 & Vp3V1
        public virtual int FinancePlanId { get; set; }
        public virtual decimal? StatementedSnapshotFinancePlanBalance { get; set; }
        public virtual decimal? StatementedSnapshotPriorFinancePlanBalance { get; set; }
        public virtual DateTime? StatementedSnapshotFinancePlanEffectiveDate { get; set; }
        public virtual decimal? StatementedSnapshotFinancePlanInterestCharged { get; set; }
        public virtual decimal? StatementedSnapshotFinancePlanInterestPaid { get; set; }
        public virtual decimal? StatementedSnapshotFinancePlanInterestRate { get; set; }
        public virtual decimal? StatementedSnapshotFinancePlanMonthlyPaymentAmount { get; set; }
        public virtual decimal? StatementedSnapshotFinancePlanPastDueAmount { get; set; }
        public virtual decimal? StatementedSnapshotFinancePlanPaymentDue { get; set; } // AmountDue
        public virtual decimal? StatementedSnapshotFinancePlanPrincipalPaid { get; set; }
        public virtual FinancePlanStatusDto StatementedSnapshotFinancePlanStatus { get; set; }
        #endregion

        #region SnapShot Values Vp2
        public virtual decimal? SnapShotCurrentBalance { get; set; }
        public virtual decimal? SnapShotDuration { get; set; }
        public virtual decimal? SnapShotPaymentAmount { get; set; }
        public virtual decimal? SnapShotInterestRate { get; set; }
        public virtual decimal? StatementedSnapshotFinancePlanAdjustmentsSum { get; set; }
        public virtual decimal? StatementedSnapshotFinancePlanAdjustments { get; set; }
        public virtual decimal? StatementedSnapshotFinancePlanSuspensions { get; set; }
        public virtual decimal? StatementedSnapshotFinancePlanClosures { get; set; }
        public virtual decimal? StatementedSnapshotFinancePlanVpPayments { get; set; }
        //Getting populated from FinancePlanVisitInterestDue table in real-time for Vp3V1 
        public virtual decimal? StatementedSnapshotFinancePlanInterestDue { get; set; }
        //Getting populated from FinancePlanVisitInterestDue table in real-time for Vp3V1
        public virtual decimal? StatementedSnapshotFinancePlanPrincipalDue { get; set; }
        public virtual decimal? StatementedSnapshotReassessmentFinancePlanAdjustmentTotal { get; set; }
        #endregion

        #region SnapShot Values Vp3V1
        public virtual decimal? StatementedSnapshotFinancePlanOtherBalanceCharges { get; set; }
        public virtual decimal? StatementedSnapshotFinancePlanOriginalBalance { get; set; }
        public virtual bool IsFirstStatement { get; set; }
        #endregion
    }
}
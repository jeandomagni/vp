﻿using Ivh.Common.Base.Utilities.Helpers;
using System;

namespace Ivh.Application.FinanceManagement.Common.Dtos
{
    public class RegistrationPaymentMethodDto
    {
                /// <summary>
        /// Gets or sets the PaymentMethodId.
        /// </summary>
        /// <value>
        /// The Payment Method id.
        /// </value>
        public virtual int RegistrationPaymentMethodID { get; set; }

        /// <summary>
        /// Gets or sets the VpGuarantorId.
        /// </summary>
        /// <value>
        /// The Visit Pay Guarantor id.
        /// </value>
        public virtual string TempVpGuarantorId { get; set; }

        /// <summary>
        /// Gets or sets the CardOrBankAccount.
        /// </summary>
        /// <value>
        /// Whwtehr the Card Or Bank Account.
        /// </value>
        public virtual string CardOrBankAccount { get; set; }

        /// <summary>
        /// Gets or sets the IsActive.
        /// </summary>
        /// <value>
        /// The Is Active.
        /// </value>
        public virtual bool IsActive { get; set; }

        /// <summary>
        /// Gets or sets the IsPrimary.
        /// </summary>
        /// <value>
        /// The Is Primary.
        /// </value>
        public virtual bool IsPrimary { get; set; }

        /// <summary>
        /// Gets or sets the GatewayToken.
        /// </summary>
        /// <value>
        /// The Gateway Token.
        /// </value>
        public virtual string GatewayToken { get; set; }

        /// <summary>
        /// Gets or sets the BillingId.
        /// </summary>
        /// <value>
        /// The Billing ID.
        /// </value>
        public virtual string BillingID { get; set; }

        /// <summary>
        /// Gets or sets the CreditDebitCardTypeID.
        /// </summary>
        /// <value>
        /// The Credit or Debit Card Type ID.
        /// </value>
        public virtual int? CreditDebitCardTypeID { get; set; }

        /// <summary>
        /// Gets or sets the BankAccountTypeID.
        /// </summary>
        /// <value>
        /// The Bank Account Type ID.
        /// </value>
        public virtual int? BankAccountTypeID { get; set; }

        /// <summary>
        /// Gets or sets the BankName.
        /// </summary>
        /// <value>
        /// The Bank Name.
        /// </value>
        public virtual string BankName { get; set; }

        /// <summary>
        /// Gets or sets the AccountNickName.
        /// </summary>
        /// <value>
        /// The Account Nick Name.
        /// </value>
        public virtual string AccountNickName { get; set; }

        /// <summary>
        /// Gets or sets the NameOnCard.
        /// </summary>
        /// <value>
        /// The Name On Card.
        /// </value>
        public virtual string NameOnCard { get; set; }

        /// <summary>
        /// Gets or sets the CreatedDate.
        /// </summary>
        /// <value>
        /// The CreatedDate.
        /// </value>
        public virtual DateTime CreatedDate { get; set; }

        /// <summary>
        /// Gets or sets the UpdatedDate.
        /// </summary>
        /// <value>
        /// The UpdatedDate.
        /// </value>
        public virtual DateTime UpdatedDate { get; set; }

        /// <summary>
        /// Gets or sets the LastFour.
        /// </summary>
        /// <value>
        /// The Last Four digits.
        /// </value>
        public virtual string LastFour { get; set; }

        /// <summary>
        /// Gets or sets the FirstName.
        /// </summary>
        /// <value>
        /// The FirstName.
        /// </value>
        public virtual string FirstName { get; set; }

        /// <summary>
        /// Gets or sets the LastName.
        /// </summary>
        /// <value>
        /// The LastName.
        /// </value>
        public virtual string LastName { get; set; }

        /// <summary>
        /// Gets or sets the ExpiryDate.
        /// </summary>
        /// <value>
        /// The expiry date (eg: 0420).
        /// </value>
        public virtual string ExpiryDate { get; set; }

        /// <summary>
        /// Gets or sets the Address1.
        /// </summary>
        /// <value>
        /// The Address1.
        /// </value>
        public virtual string Address1 { get; set; }

        /// <summary>
        /// Gets or sets the Address2.
        /// </summary>
        /// <value>
        /// Address2.
        /// </value>
        public virtual string Address2 { get; set; }

        /// <summary>
        /// Gets or sets the City.
        /// </summary>
        /// <value>
        /// The City.
        /// </value>
        public virtual string City { get; set; }

        /// <summary>
        /// Gets or sets the State.
        /// </summary>
        /// <value>
        /// The State.
        /// </value>
        public virtual string State { get; set; }

        /// <summary>
        /// Gets or sets the Zip.
        /// </summary>
        /// <value>
        /// The Zip.
        /// </value>
        public virtual string Zip { get; set; }

        /// <summary>
        /// LastName, FirstName (Doe, John)
        /// </summary>
        public virtual string LastNameFirstName => FormatHelper.LastNameFirstName(this.LastName, this.FirstName);

        /// <summary>
        /// FirstName LastName (John Doe)
        /// </summary>
        public virtual string FirstNameLastName => FormatHelper.FirstNameLastName(this.FirstName, this.LastName);
    }
}

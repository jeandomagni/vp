namespace Ivh.Application.FinanceManagement.Common.Dtos
{
    public class ValidatePaymentResponse
    {
        public ValidatePaymentResponse(bool prompt, string message)
        {
            this.Prompt = prompt;
            this.Message = message;
        }

        public bool Prompt { get; }

        public string Message { get; }
    }
}
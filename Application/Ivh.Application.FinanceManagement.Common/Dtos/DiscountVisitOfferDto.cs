﻿namespace Ivh.Application.FinanceManagement.Common.Dtos
{
    using System;

    public class DiscountVisitOfferDto
    {
        public int DiscountOfferVisitId { get; set; }

        public DateTime InsertDate { get; set; }
        
        public PaymentVisitDto PaymentVisit { get; set; }

        public decimal DiscountPercent { get; set; }
        
        public decimal DiscountAmount { get; set; }
        
        public decimal DiscountedCurrentBalance { get; set; }

        public decimal TotalBalance { get; set; }
    }
}
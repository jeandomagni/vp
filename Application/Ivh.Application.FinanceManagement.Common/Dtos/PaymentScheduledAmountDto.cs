﻿namespace Ivh.Application.FinanceManagement.Common.Dtos
{
    using System;

    public class PaymentScheduledAmountDto
    {
        public virtual int PaymentScheduledAmountId { get; set; }
        public virtual PaymentFinancePlanDto PaymentFinancePlan { get; set; }
        public virtual DateTime InsertDate { get; set; }
        public virtual decimal ScheduledAmount { get; set; }
        public virtual int? VisitId { get; set; }
    }
}

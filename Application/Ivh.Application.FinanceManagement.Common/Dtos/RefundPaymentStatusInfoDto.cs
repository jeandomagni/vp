﻿using Ivh.Common.VisitPay.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ivh.Application.FinanceManagement.Common.Dtos
{
    public class RefundPaymentStatusInfoDto
    {
        public PaymentReversalStatusEnum PaymentReversalStatus { get; set; }
        public PaymentRefundableTypeEnum PaymentRedundableType { get; set; }
    }
}

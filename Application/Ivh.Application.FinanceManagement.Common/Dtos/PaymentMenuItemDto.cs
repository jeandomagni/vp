namespace Ivh.Application.FinanceManagement.Common.Dtos
{
    using System.Collections.Generic;
    using Content.Common.Dtos;

    public class PaymentMenuItemDto
    {
        public PaymentMenuItemDto()
        {
            this.PaymentMenuItems = new List<PaymentMenuItemDto>();
        }

        public int PaymentMenuItemId { get; set; }

        public UserPaymentOptionDto UserPaymentOption { get; set; }

        public CmsVersionDto CmsVersion { get; set; }

        public int DisplayOrder { get; set; }

        public string IconCssClass { get; set; }

        public bool IsEnabled { get; set; }

        public int? ParentPaymentMenuItemId { get; set; }

        public IList<PaymentMenuItemDto> PaymentMenuItems { get; set; }
    }
}
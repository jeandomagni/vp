﻿namespace Ivh.Application.FinanceManagement.Common.Dtos
{
    using System;

    public class FinancePlanInterestRateHistoryDto
    {
        public int FinancePlanInterestRateHistoryId { get; set; }
        public DateTime InsertDate { get; set; }
        public decimal InterestRate { get; set; }
        public string ChangeDescription { get; set; }
        public FinancePlanDto FinancePlan { get; set; }

        public decimal PaymentAmount { get; set; }
    }
}

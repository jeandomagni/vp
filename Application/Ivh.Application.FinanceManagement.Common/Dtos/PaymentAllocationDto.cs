﻿namespace Ivh.Application.FinanceManagement.Common.Dtos
{
    using System;
    using Ivh.Common.Base.Enums;
    using Ivh.Common.VisitPay.Enums;

    public class PaymentAllocationDto
    {
        public virtual int PaymentAllocationId { get; set; }
        public virtual PaymentDto Payment { get; set; }
        public virtual PaymentAllocationTypeEnum PaymentAllocationType { get; set; }
        public virtual PaymentVisitDto PaymentVisit { get; set; }
        public virtual int? FinancePlanId { get; set; }
        public virtual DateTime InsertDate { get; set; }
        public virtual decimal ActualAmount { get; set; }
        public virtual PaymentAllocationStatusEnum PaymentAllocationStatus { get; set; }
    }
}
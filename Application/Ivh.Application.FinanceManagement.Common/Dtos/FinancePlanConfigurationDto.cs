namespace Ivh.Application.FinanceManagement.Common.Dtos
{
    using System.Collections.Generic;
    using Ivh.Common.VisitPay.Enums;

    public class FinancePlanConfigurationDto
    {
        public FinancePlanConfigurationDto()
        {
            this.InterestRates = new List<InterestRateDto>();
            this.FinancePlanOfferSetTypes = new List<FinancePlanOfferSetTypeDto>();
            this.FinancePlanTypes = new List<FinancePlanTypeDto>();
            this.IsEditable = true;
        }

        public int? FinancePlanId { get; set; }
        public int StatementId { get; set; }
        public int? EsignCmsVersionId { get; set; }
        public int TermsCmsVersionId { get; set; }
        public int MaximumMonthlyPayments { get; set; }
        public decimal MinimumPaymentAmount { get; set; }
        public decimal AmountToFinance { get; set; }
        public FinancePlanTermsDto Terms { get; set; }
        public IList<InterestRateDto> InterestRates { get; set; }
        public string TextClientOfferSet { get; set; }
        public OfferCalculationStrategyEnum OfferCalculationStrategy { get; set; }
        public bool IsEditable { get; set; }
        public bool IsPatientOffer { get; set; }
        public bool IsCombined { get; set; }
        public int FinancePlanOfferSetTypeId { get; set; }
        public IList<FinancePlanOfferSetTypeDto> FinancePlanOfferSetTypes { get; set; }
        public IList<FinancePlanTypeDto> FinancePlanTypes { get; set; }
        public int DisplayOrder { get; set; }
        public int ActiveFinancePlanCount { get; set; }
        public decimal ActiveFinancePlanBalance { get; set; }
        public string OptionText { get; set; }
        public string OptionTooltip { get; set; }
        public decimal? SuggestedMonthlyPaymentAmount { get; set; }
        public decimal? SuggestedNumberMonthlyPayments { get; set; }
    }
}
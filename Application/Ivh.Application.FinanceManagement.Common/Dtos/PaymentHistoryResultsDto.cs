﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ivh.Application.FinanceManagement.Common.Dtos
{
    public class PaymentHistoryResultsDto
    {
        public IReadOnlyList<PaymentHistoryDto> Payments { get; set; }
        public int TotalRecords { get; set; }
    }
}

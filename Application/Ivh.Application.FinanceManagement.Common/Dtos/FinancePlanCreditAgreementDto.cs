﻿namespace Ivh.Application.FinanceManagement.Common.Dtos
{
    using System;
    using Ivh.Common.VisitPay.Enums;

    public class FinancePlanCreditAgreementDto
    {
        public CmsRegionEnum TermsCmsRegion { get; set; }

        public int NumberMonthlyPayments { get; set; }

        public int TermsCmsVersionId { get; set; }

        public int TermsCmsVersionNum { get; set; }
        
        public string AgreementText { get; set; }

        public Guid ContractNumber { get; set; }

        public string Locale { get; set; }

        public bool IsRetailInstallmentContract { get; set; }
    }
}
﻿namespace Ivh.Application.FinanceManagement.Common.Dtos
{
    using System;
    using Ivh.Common.Base.Enums;
    using Ivh.Common.VisitPay.Enums;

    public class PaymentEventDto
    {
        public virtual string Comment { get; set; }
        public virtual DateTime InsertDate { get; set; }
        public virtual PaymentDto Payment { get; set; }
        public virtual int PaymentEventId { get; set; }
        public virtual PaymentStatusEnum PaymentStatus { get; set; }
        public virtual int VisitPayUserId { get; set; }
        public virtual int VpGuarantorId { get; set; }
    }
}
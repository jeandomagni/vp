﻿namespace Ivh.Application.FinanceManagement.Common.Dtos
{
    public class FinancePlanBoundaryDto
    {
        public FinancePlanBoundaryDto(decimal minimumMonthlyPaymentAmount, int maximumNumberOfMonthlyPayments)
        {
            this.MinimumPaymentAmount = minimumMonthlyPaymentAmount;
            this.MaximumNumberOfPayments = maximumNumberOfMonthlyPayments;
        }

        public decimal MinimumPaymentAmount { get; private set; }
        public int MaximumNumberOfPayments { get; private set; }
    }
}
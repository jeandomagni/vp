﻿namespace Ivh.Application.FinanceManagement.Common.Dtos
{
    public class PaymentMethodResultDto
    {
        public PaymentMethodResultDto(bool isSuccess, string errorMessage = null, bool primaryChanged = false, PaymentMethodDto paymentMethod = null)
        {
            this.IsError = !isSuccess;
            this.IsSuccess = isSuccess;
            this.ErrorMessage = errorMessage;
            this.PrimaryChanged = primaryChanged;
            this.PaymentMethod = paymentMethod;
        }

        public bool IsError { get; private set; }
        public bool IsSuccess { get; private set; }
        public string ErrorMessage { get; private set; }
        public bool PrimaryChanged { get; private set; }
        public PaymentMethodDto PaymentMethod { get; private set; }
    }
}
﻿namespace Ivh.Application.FinanceManagement.Common.Dtos
{
    using Ivh.Common.Base.Enums;
    using Ivh.Common.VisitPay.Enums;

    public class FinancePlanStatusDto
    {
        public int FinancePlanStatusId { get; set; }
        public string FinancePlanStatusName { get; set; }
        public string FinancePlanStatusGroupName { get; set; }
        public string FinancePlanStatusDisplayName { get; set; }

        public FinancePlanStatusEnum FinancePlanStatusEnum
        {
            get { return (FinancePlanStatusEnum)this.FinancePlanStatusId; }
        }
    }
}

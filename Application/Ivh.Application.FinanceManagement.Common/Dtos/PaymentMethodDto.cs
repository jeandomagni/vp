﻿namespace Ivh.Application.FinanceManagement.Common.Dtos
{
    using System;
    using System.Collections.Generic;
    using Ivh.Common.Base.Utilities.Helpers;
    using Ivh.Common.VisitPay.Enums;

    [Serializable]
    public class PaymentMethodDto
    {
        public PaymentMethodDto()
        {
            this.BillingAddresses = new List<PaymentMethodBillingAddressDto>();
        }
        
        public int? PaymentMethodId { get; set; }
        
        public int? VpGuarantorId { get; set; }
        
        public bool IsActive { get; set; }

        public bool IsAchType { get; set; }

        public bool IsCardType { get; set; }
        
        public bool IsPrimary { get; set; }
        
        public bool IsRemoveable { get; set; }
        
        public string GatewayToken { get; set; }
        
        public string BillingId { get; set; }
        
        public string BankName { get; set; }
        
        public string AccountNickName { get; set; }
        
        public DateTime? ExpiringOn { get; set; }
        
        public DateTime? ExpiredOn { get; set; }
        
        public string NameOnCard { get; set; }
        
        public DateTime CreatedDate { get; set; }
        
        public DateTime UpdatedDate { get; set; }
        
        public string LastFour { get; set; }
        
        public string FirstName { get; set; }
        
        public string LastName { get; set; }
        
        public int CreatedUserId { get; set; }
        
        public int UpdatedUserId { get; set; }
        
        public string ExpDate { get; set; }

        public string ExpDateForDisplay { get; set; }

        public bool IsRegistrationPaymentMethod { get; set; }

        public PaymentMethodTypeEnum PaymentMethodType { get; set; }

        public PaymentMethodAccountTypeDto PaymentMethodAccountType { get; set; }

        public int? AccountTypeId { get; set; }

        public PaymentMethodProviderTypeDto PaymentMethodProviderType { get; set; }

        public int? ProviderTypeId { get; set; }

        public string ProviderTypeImage { get; set; }

        public bool IsExpired { get; set; }

        public bool IsExpiring { get; set; }

        public string ExpirationMessage { get; set; }

        public string DisplayName { get; set; }

        public string RemoveableMessage { get; set; }

        [Obsolete]
        public string CardCode { get; set; }

        public bool CanSetPrimary { get; set; }
        
        public IList<PaymentMethodBillingAddressDto> BillingAddresses { get; set; }
        
        public string LastNameFirstName => FormatHelper.LastNameFirstName(this.LastName, this.FirstName);

        public string FirstNameLastName => FormatHelper.FirstNameLastName(this.FirstName, this.LastName);
    }
}
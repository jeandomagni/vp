﻿
namespace Ivh.Application.FinanceManagement.Common.Dtos
{
    using Ivh.Common.VisitPay.Enums;
    using System;
    using Core.Common.Dtos;

    public class PaymentImportDto
    {
        public int PaymentImportId { get; set; }

        public PaymentImportStateEnum PaymentImportStateEnum { get; set; }
        public string PaymentImportStateReason { get; set; }

        public PaymentDto Payment { get; set; }
        public GuarantorDto Guarantor { get; set; }
        public string LockBoxNumber { get; set; }
        public DateTime BatchCreditDate { get; set; }
        public string GuarantorAccountNumber { get; set; }
        public string GuarantorFirstName { get; set; }
        public string GuarantorLastName { get; set; }
        public decimal? InvoiceAmount { get; set; }
        public string BatchNumber { get; set; }
        public string PaymentSequenceNumber { get; set; }
        public string CheckPaymentNumber { get; set; }
        public string CheckAccountNumber { get; set; }
        public string CheckRouting { get; set; }
        public string CheckRemitter { get; set; }
        public string PsrId { get; set; }
        public string PaymentComment { get; set; }
        public string EmailAddress { get; set; }
        public string DraftReturnCode { get; set; }
        public string DraftReturnInfo { get; set; }
        public string RdfiBankName { get; set; }
        public string ClientUserNotes { get; set; }
    }
}

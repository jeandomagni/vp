namespace Ivh.Application.FinanceManagement.Common.Dtos
{
    public class HealthEquitySsoTokenResponse
    {
        public string SsoToken { get; set; }
        public bool WasSuccessful { get; set; }
        public bool? AuthTokenExpired { get; set; }
    }
}
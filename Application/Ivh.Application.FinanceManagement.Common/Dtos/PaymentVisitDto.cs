﻿namespace Ivh.Application.FinanceManagement.Common.Dtos
{
    using System;
    using Base.Common.Interfaces.Entities.Visit;
    using Ivh.Common.Base.Enums;
    using Ivh.Common.VisitPay.Enums;

    public class PaymentVisitDto : IVisit
    {
        public int VisitId { get; set; }

        public int VpGuarantorId { get; set; }

        public decimal CurrentBalance { get; protected set; }

        public VisitStateEnum CurrentVisitState { get; set; }

        public DateTime? InsertDate { get; set; }

        public int AgingCount { get; set; }

        public string BillingApplication { get; set; }

        public BalanceTransferStatusEnum? BalanceTransferStatusEnum { get; set; }

        public int? ServiceGroupId { get; set; }

        public string SourceSystemKey { get; set; }

        public string SourceSystemKeyDisplay { get; set; }

        public string MatchedSourceSystemKey { get; set; }

        public int? BillingSystemId { get; set; }

        public int? MatchedBillingSystemId { get; set; }

        public bool IsUnmatched { get; set; }

        public bool IsMaxAge { get; set; }

        public virtual bool VpEligible { get; set; }

        public virtual bool BillingHold { get; set; }

        public virtual bool Redact { get; set; }
    }
}
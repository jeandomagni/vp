﻿namespace Ivh.Application.FinanceManagement.Common.Dtos
{
    using Core.Common.Dtos;

    public class GuarantorWithLastFinancePlanStatementDto
    {
        public GuarantorDto Guarantor { get; set; }
        public FinancePlanStatementDto LastFinancePlanStatement { get; set; }
    }
}

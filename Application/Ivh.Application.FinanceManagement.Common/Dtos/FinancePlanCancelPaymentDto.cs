﻿namespace Ivh.Application.FinanceManagement.Common.Dtos
{
    using System;

    public class FinancePlanCancelPaymentDto
    {
        public decimal AmountDueBefore { get; set; }
        public decimal AmountDueAfter { get; set; }
        public DateTime PaymentDueDate { get; set; }
        public bool HasPastDue { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ivh.Application.FinanceManagement.Common.Dtos
{
    public class FinalPastDueNotificationInfoDto
    {
        public int VpGuarantorId { get; set; }
        public IList<int> VisitIds { get; set; }
        public bool IsOnFinancePlan { get; set; }
    }
}

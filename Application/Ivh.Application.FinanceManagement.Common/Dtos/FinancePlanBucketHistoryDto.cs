﻿namespace Ivh.Application.FinanceManagement.Common.Dtos
{
    using System;
    using Core.Common.Dtos;
    using User.Common.Dtos;

    public class FinancePlanBucketHistoryDto
    {
        public int Bucket { get; set; }
        public DateTime InsertDate { get; set; }
        public string Comment { get; set; }
        public VisitPayUserDto VisitPayUser { get; set; }
    }
}
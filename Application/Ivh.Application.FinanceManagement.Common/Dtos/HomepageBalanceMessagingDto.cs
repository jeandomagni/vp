﻿namespace Ivh.Application.FinanceManagement.Common.Dtos
{
    public class HomepageBalanceMessagingDto
    {
        public HomepageBalanceMessagingDto(decimal totalBalance, bool isActionRequired)
        {
            this.TotalBalance = totalBalance;
            this.IsActionRequired = isActionRequired;
        }

        public decimal TotalBalance { get; }
        public bool IsActionRequired { get; }
        public bool ShowDiscountEligible { get; set; }
        public bool ShowFullPaymentScheduled { get; set; }
    }
}
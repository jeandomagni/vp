﻿using System.Collections.Generic;

namespace Ivh.Application.FinanceManagement.Common.Dtos
{
    public class PaymentResultsDto
    {
        public IReadOnlyList<PaymentDto> Payments { get; set; }
        public int TotalRecords { get; set; }
    }
}

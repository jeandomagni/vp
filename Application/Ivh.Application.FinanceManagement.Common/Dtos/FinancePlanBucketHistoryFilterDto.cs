﻿namespace Ivh.Application.FinanceManagement.Common.Dtos
{
    public class FinancePlanBucketHistoryFilterDto
    {
        public int GuarantorVisitPayUserId { get; set; }

        public int FinancePlanId { get; set; }

        public int Page { get; set; }

        public int Rows { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ivh.Application.FinanceManagement.Common.Dtos
{
    public class AchReturnDetailDto
    {
        public int AchReturnDetailId { get; set; }
        public AchFileDto AchFile { get; set; }
        public DateTime DownloadedDate { get; set; }
        public DateTime? ProcessedDate { get; set; }

        public string CompanyId { get; set; }
        public string CompanyName { get; set; }
        public string SecCode { get; set; }
        public string Description { get; set; }
        public string EffectiveDate { get; set; }
        public string IndividualId { get; set; }
        public string IndividualName { get; set; }
        public string RdfiRoutingNumber { get; set; }
        public string RdfiAccountNumber { get; set; }
        public string TransactionCode { get; set; }
        public string Amount { get; set; }
        public string CustomerSpecifiedTraceNumber { get; set; }
        public string WebTransactionSingleOrRecurringIdentifier { get; set; }
        public string ReturnReasonCode { get; set; }
        public string ReturnCorrectionOrAddendaInfo { get; set; }
        public string ReturnDate { get; set; }
        public string ReturnTraceNumber { get; set; }
    }
}

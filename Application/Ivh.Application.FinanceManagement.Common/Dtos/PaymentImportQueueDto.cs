﻿
namespace Ivh.Application.FinanceManagement.Common.Dtos
{
    using System.Collections.Generic;

    public class PaymentImportQueueDto
    {
        public IList<PaymentImportDto> PaymentImports { get; set; }
        public decimal? AllPaymentsTotal { get; set; }
        public decimal? PostedPaymentsTotal { get; set; }
        public decimal? UnpostedPaymentsTotal { get; set; }
        public decimal? NonVisitPayPaymentsTotal { get; set; }
    }
}

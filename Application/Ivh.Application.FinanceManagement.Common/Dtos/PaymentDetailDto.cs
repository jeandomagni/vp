﻿namespace Ivh.Application.FinanceManagement.Common.Dtos
{
    using System.Collections.Generic;

    public class PaymentDetailDto : PaymentHistoryDto
    {
        public PaymentDetailDto()
        {
            this.PaymentAllocations = new List<PaymentAllocationGroupedDto>();
        }

        public IList<PaymentAllocationGroupedDto> PaymentAllocations { get; set; }
    }
}
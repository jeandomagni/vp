namespace Ivh.Application.FinanceManagement.Common.Dtos
{
    using System;
    using Core.Common.Dtos;

    public class VisitPaymentDto
    {
        private decimal _paymentAmount;

        public int VisitId { get; set; }

        public decimal PaymentAmount
        {
            get { return Math.Round(this._paymentAmount, 2); }
            set { this._paymentAmount = Math.Round(value, 2); }
        }
    }
}
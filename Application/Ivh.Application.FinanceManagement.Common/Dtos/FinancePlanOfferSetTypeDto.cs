﻿namespace Ivh.Application.FinanceManagement.Common.Dtos
{
    public class FinancePlanOfferSetTypeDto
    {
        public int FinancePlanOfferSetTypeId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public bool IsDefault { get; set; }
        public bool IsActive { get; set; }
        public bool IsPatient { get; set; }
        public bool IsClient { get; set; }
        public bool IsClientOnly { get; set; }
    }
}
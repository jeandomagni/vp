namespace Ivh.Application.FinanceManagement.Common.Dtos
{
    public class AcceptReconfiguredFinancePlanResponse
    {
        public bool IsError { get; set; }
        public string ErrorMessage { get; set; }
        public FinancePlanDto FinancePlan { get; set; }
    }
}
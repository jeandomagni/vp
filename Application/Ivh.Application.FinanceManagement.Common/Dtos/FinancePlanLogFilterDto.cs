﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ivh.Application.FinanceManagement.Common.Dtos
{
    public class FinancePlanLogFilterDto
    {
        public int VisitPayUserId { get; set; }
        public int FinancePlanId { get; set; }
        public List<string> SourceSystemKeys { get; set; }

        public int Page { get; set; }
        public int Rows { get; set; }

        /// <summary>
        /// The name of the field to be sorted
        /// </summary>
        public string Sidx { get; set; }
        
        /// <summary>
        /// Sort order (asc,desc)
        /// </summary>
        public string Sord { get; set; }
    }
}

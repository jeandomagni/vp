﻿namespace Ivh.Application.FinanceManagement.Common.Dtos
{
    using System.Collections.Generic;
    using Core.Common.Dtos;

    public class ReconfigureFinancePlanDto
    {
        public int FinancePlanIdToReconfigure { get; set;}

        public decimal CurrentFinancePlanBalance { get; set; }
        
        public decimal CurrentFinancePlanInterestRate { get; set; }
        
        public decimal CurrentFinancePlanOriginatedBalance { get; set; }

        public decimal CurrentFinancePlanNewCharges { get; set; }
        
        public decimal CurrentFinancePlanPaymentAmount { get; set; }

        public decimal NewFinancePlanBalance { get; set; }
        
        public FinancePlanBoundaryDto FinancePlanBoundary { get; set; }
        
        public GuarantorDto Guarantor { get; set; }

        public FinancePlanTermsDto ReconfiguredTerms { get; set; }

        public IList<InterestRateDto> InterestRates { get; set; } = new List<InterestRateDto>();

        public IList<FinancePlanOfferSetTypeDto> FinancePlanOfferSetTypes { get; set; } = new List<FinancePlanOfferSetTypeDto>();
    }
}
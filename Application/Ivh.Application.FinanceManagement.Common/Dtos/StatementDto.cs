﻿namespace Ivh.Application.FinanceManagement.Common.Dtos
{
    using System;
    using System.Collections.Generic;
    using Ivh.Common.Base.Utilities.Helpers;
    using Ivh.Common.VisitPay.Enums;

    public class StatementDto
    {
        public StatementDto()
        {
            this.Alerts = new List<StatementAlertDto>();
            this.FinancePlans = new List<StatementFinancePlanDto>();
            this.Visits = new List<StatementVisitDto>();
            this.VisitsNotOnFinancePlan = new List<StatementVisitDto>();
            this.ActiveVisitsNotOnFinancePlan = new List<StatementVisitDto>();
            this.Notifications = new List<string>();
            this.ServiceGroupDisclaimers = new List<string>();
        }

        public int VpStatementId { get; set; }
        public VpStatementTypeEnum VpStatementType { get; set; }
        public VpStatementVersionEnum StatementVersion { get; set; }
        public DateTime StatementDate { get; set; }
        public DateTime PeriodStartDate { get; set; }
        public DateTime PeriodEndDate { get; set; }
        public DateTime PaymentDueDate { get; set; }
        public bool CreatePaperStatement { get; set; }
        public decimal StatementedSnapshotNewVisitsBalance { get; set; }
        public decimal StatementedSnapshotFinancePlansBalance { get; set; }
        public decimal StatementedSnapshotVisitBalance { get; set; }
        public decimal StatementedCurrentFinancePlanBalance { get; set; }
        
        public int SnapshotAgingCount { get; set; }
        public int PaymentDueDay { get; set; }
        public bool IsGracePeriod { get; set; }
        public int DaysInBillingCycle { get; set; }
        public int StatementYear { get; set; }
        public bool IsArchived { get; set; }
        public string ArchivedFileMimeType { get; set; }
        public virtual decimal DisplayStatementBalance { get; set; }

        #region snapshot values VP2
        public bool IsSoftStatement { get; set; }
        public decimal StatementedSnapshotTotalBalance { get; set; }
        public DateTime? PriorStatementDate { get; set; }
        public decimal? StatementedSnapshotDiscountsAndAdjustmentsSum { get; set; }
        public decimal? StatementedSnapshotFinancePlanBalanceSum { get; set; }
        public decimal? StatementedSnapshotFinancePlanInterestChargedSum { get; set; }
        public decimal? StatementedSnapshotFinancePlanInterestDueSum { get; set; }
        public decimal? StatementedSnapshotFinancePlanPrincipalDueSum { get; set; }
        public decimal? StatementedSnapshotMinimumAmountDue { get; set; }
        public decimal? StatementedSnapshotNewVisitBalanceSum { get; set; }
        public decimal? StatementedSnapshotPastDueFinancePlanBalanceSum { get; set; }
        public decimal? StatementedSnapshotPastDueVisitBalanceSum { get; set; }
        public decimal? StatementedSnapshotPaymentGatewayErrorBalanceSum { get; set; }
        public decimal? StatementedSnapshotPaymentsAppliedSum { get; set; }
        public decimal? StatementedSnapshotPriorTotalBalance { get; set; }
        public decimal? StatementedSnapshotTotalInterestChargedYTD { get; set; }
        public decimal? StatementedSnapshotVisitBalanceSum { get; set; }
        public decimal? StatementedSnapshotVisitCreditBalanceSum { get; set; }
        public decimal? StatementedSnapshotFinancePlanPaymentDueSum { get; set; }
        public decimal? StatementedSnapshotSuspendedClosedVisitBalanceSum { get; set; }
        public decimal? StatementedSnapshotVpFeesAndInterest { get; set; }
        public decimal? StatementedSnapshotVpPaymentsAndDiscounts { get; set; }
        public decimal? StatementedSnapshotVisitHsTransactionsPaymentSum { get; set; }
        public decimal? StatementedSnapshotHsAdjustmentsAndHsCharges { get; set; }
        public decimal? StatementedSnapshotVisitHsTransactionsAdjustmentSum { get; set; }
        public decimal? StatementedSnapshotVisitHsTransactionsAdjustmentsExcludingInsuranceSum { get; set; }
        public decimal? StatementedSnapshotVisitHsTransactionsAdjustmentsInsuranceSum { get; set; }
        public decimal? StatementedSnapshotVisitHsTransactionsChargeSum { get; set; }
        #endregion

        #region visit pay user and guarantor 

        public int GuarantorRegistrationYear { get; set; }
        public string GuarantorVisitPayUserFirstName { get; set; }
        public string GuarantorVisitPayUserLastName { get; set; }
        public string GuarantorVisitPayUserLastNameFirstName => FormatHelper.LastNameFirstName(this.GuarantorVisitPayUserLastName,this.GuarantorVisitPayUserFirstName);
        public string GuarantorVisitPayUserFirstNameLastName => FormatHelper.FirstNameLastName(this.GuarantorVisitPayUserFirstName,this.GuarantorVisitPayUserLastName);

        public string GuarantorAddressStreet1 { get; set; }
        public string GuarantorAddressStreet2 { get; set; }
        public string GuarantorCityStateZip { get; set; }


        #endregion

        public IList<StatementAlertDto> Alerts { get; set; }
        public IList<string> ServiceGroupDisclaimers { get; set; }
        public IList<string> Notifications { get; set; }
        public IList<StatementFinancePlanDto> FinancePlans { get; set; }
        public IList<StatementVisitDto> Visits { get; set; }
        public IList<StatementVisitDto> VisitsNotOnFinancePlan { get; set; }
        public IList<StatementVisitDto> ActiveVisitsNotOnFinancePlan { get; set; }
        public DateTime? NextStatementDate { get; set; }
        public bool HasInsuranceAdjustmentDetail { get; set; }
    }
}

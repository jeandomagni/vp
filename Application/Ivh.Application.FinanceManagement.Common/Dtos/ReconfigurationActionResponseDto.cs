namespace Ivh.Application.FinanceManagement.Common.Dtos
{
    public class ReconfigurationActionResponseDto
    {
        protected ReconfigurationActionResponseDto(bool isError)
        {
            this.IsError = isError;
        }

        public bool IsError { get; }

        public static ReconfigurationActionResponseDto Error => new ReconfigurationActionResponseDto(true);

        public static ReconfigurationActionResponseDto Success => new ReconfigurationActionResponseDto(false);
    }
}
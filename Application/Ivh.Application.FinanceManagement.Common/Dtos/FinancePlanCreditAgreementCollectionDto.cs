﻿namespace Ivh.Application.FinanceManagement.Common.Dtos
{
    public class FinancePlanCreditAgreementCollectionDto
    {
        public FinancePlanCreditAgreementDto ClientLocale { get; set; }

        public FinancePlanCreditAgreementDto UserLocale { get; set; }

        public FinancePlanCreditAgreementDto MaterialTerms { get; set; }
    }
}
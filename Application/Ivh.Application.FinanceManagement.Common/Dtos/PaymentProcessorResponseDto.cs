﻿namespace Ivh.Application.FinanceManagement.Common.Dtos
{
    using System;
    using System.Collections.Generic;
    using Ivh.Common.Base.Enums;
    using Ivh.Common.VisitPay.Enums;

    public class PaymentProcessorResponseDto
    {
        public bool HadSystemError { get; set; }
        public DateTime InsertDate { get; set; }
        public PaymentMethodDto PaymentMethod { get; set; }
        public int PaymentProcessorResponseId { get; set; }
        public PaymentProcessorResponseStatusEnum PaymentProcessorResponseStatus { get; set; }
        public string PaymentProcessorSourceKey { get; set; }
        public IList<PaymentDto> Payments { get; set; }
        public decimal SnapshotTotalPaymentAmount { get; set; }
        public bool WasSuccessful { get; set; }
    }
}
﻿namespace Ivh.Application.FinanceManagement.Common.Dtos
{
    using System;

    public class StatementFinancePlanDto
    {
        public decimal? StatementedSnapshotFinancePlanAdjustmentsSum { get; set; }
        public decimal? StatementedSnapshotFinancePlanBalance { get; set; }
        public DateTime? StatementedSnapshotFinancePlanEffectiveDate { get; set; }
        public decimal? StatementedSnapshotFinancePlanInterestCharged { get; set; }
        public decimal? StatementedSnapshotFinancePlanInterestPaid { get; set; }
        public decimal? StatementedSnapshotFinancePlanInterestRate { get; set; }
        public decimal? StatementedSnapshotFinancePlanMonthlyPaymentAmount { get; set; }
        public decimal? StatementedSnapshotFinancePlanPastDueAmount { get; set; }
        public decimal? StatementedSnapshotFinancePlanPaymentDue { get; set; }
        public decimal? StatementedSnapshotFinancePlanPrincipalPaid { get; set; }
        public int? StatementedSnapshotFinancePlanStatusId { get; set; }
        public string StatementedSnapshotFinancePlanStatusDisplayName { get; set; }
        public decimal? StatementedSnapshotPriorFinancePlanBalance { get; set; }
        public virtual decimal FinancePlanPayments { get; set; }
    }
}

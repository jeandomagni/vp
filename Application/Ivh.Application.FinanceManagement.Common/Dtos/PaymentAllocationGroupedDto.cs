﻿namespace Ivh.Application.FinanceManagement.Common.Dtos
{
    using Ivh.Common.Base.Enums;
    using Ivh.Common.VisitPay.Enums;

    public class PaymentAllocationGroupedDto
    {
        public decimal AllocationAmount { get; set; }

        public int? FinancePlanId { get; set; }

        public PaymentVisitDto Visit { get; set; }

        public PaymentAllocationTypeEnum PaymentAllocationType { get; set; }
    }
}

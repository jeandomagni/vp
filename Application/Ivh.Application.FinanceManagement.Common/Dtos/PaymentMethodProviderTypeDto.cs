﻿
namespace Ivh.Application.FinanceManagement.Common.Dtos
{
    using System;

    [Serializable]
    public class PaymentMethodProviderTypeDto
    {
        public virtual int PaymentMethodProviderTypeId { get; set; }

        public virtual string PaymentMethodProviderTypeText { get; set; }

        public virtual string PaymentMethodProviderTypeDisplayText { get; set; }

        public virtual string ImageName { get; set; }

        public virtual int DisplayOrder { get; set; }
    }
}

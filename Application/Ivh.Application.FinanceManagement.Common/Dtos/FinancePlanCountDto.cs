﻿namespace Ivh.Application.FinanceManagement.Common.Dtos
{
    public class FinancePlanCountDto
    {
        public int Total { get; set; }
        public int PastDue { get; set; }
    }
}
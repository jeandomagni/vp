﻿using System.Collections.Generic;

namespace Ivh.Application.FinanceManagement.Common.Dtos
{
    public class GuarantorWithLastStatementResultsDto
    {
        public IReadOnlyList<GuarantorWithLastStatementDto> Guarantors { get; set; }
        public int TotalRecords { get; set; }
    }
}

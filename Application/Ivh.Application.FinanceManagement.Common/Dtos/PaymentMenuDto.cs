namespace Ivh.Application.FinanceManagement.Common.Dtos
{
    using System.Collections.Generic;

    public class PaymentMenuDto
    {
        public PaymentMenuDto()
        {
            this.PaymentMenuItems = new List<PaymentMenuItemDto>();
        }

        public IList<PaymentMenuItemDto> PaymentMenuItems { get; set; }

        public int? SelectedPaymentMenuItemId { get; set; }
    }
}
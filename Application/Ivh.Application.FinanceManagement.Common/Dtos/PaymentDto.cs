namespace Ivh.Application.FinanceManagement.Common.Dtos
{
    using System;
    using System.Collections.Generic;
    using Core.Common.Dtos;
    using Ivh.Common.Base.Enums;
    using Ivh.Common.VisitPay.Enums;

    public class PaymentDto
    {
        public int PaymentId { get; set; }
        public GuarantorDto Guarantor { get; set; }
        public DateTime InsertDate { get; set; }
        public PaymentTypeEnum PaymentType { get; set; }
        public PaymentStatusEnum PaymentStatus { get; set; }
        public PaymentMethodDto PaymentMethod { get; set; }
        public decimal ScheduledPaymentAmount { get; set; }
        public decimal ActualPaymentAmount { get; set; }
        public DateTime ScheduledPaymentDate { get; set; }
        public DateTime? OriginalScheduledPaymentDate { get; set; }
        public DateTime? ActualPaymentDate { get; set; }
        public decimal ScheduledDiscountPercent { get; set; }
        public virtual IList<PaymentAllocationDto> PaymentAllocations { get; set; }
        public virtual IList<PaymentScheduledAmountDto> PaymentScheduledAmounts { get; set; }
        public virtual IList<PaymentDto> ReversalPayments { get; set; }
        public string TransactionId { get; set; }
        public bool IsCurrentBalanceInFullType { get; set; }
        public bool IsSpecificAmountType { get; set; }
        public bool IsSpecificFinancePlansType { get; set; }
        public bool IsSpecificVisitsType { get; set; }
        public bool IsStatementBalanceInFullType { get; set; }
        public bool IsPromptPayType { get; set; }
        public bool IsScheduledPayType { get; set; }
        public bool IsReversalPaymentType { get; set; }
        public bool? IsHouseholdBalance { get; set; }
        public DateTime? LastPaymentFailureDate { get; set; }
        public PaymentReversalStatusEnum PaymentReversalStatusEnum { get; set; }
        public int? ParentPaymentId { get; set; }
        public decimal NetPaymentAmount { get; set; }
        public string ReversalActionTakenBy { get; set; }
    }
}
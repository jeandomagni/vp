﻿namespace Ivh.Application.FinanceManagement.Common.Dtos
{
    using System;
    using Ivh.Common.VisitPay.Enums;

    public class PaymentRescheduleDto
    {
        public int? PaymentId { get; set; }

        public int? PaymentMethodId { get; set; }

        public DateTime? RescheduledPaymentDate { get; set; }

        public int? PaymentRescheduleReasonId { get; set; }

        public string PaymentRescheduleDescription { get; set; }
        
        public PaymentTypeEnum PaymentType { get; set; }
    }
}
﻿namespace Ivh.Application.FinanceManagement.Common.Dtos
{
    using System;
    using Ivh.Common.Base.Enums;
    using Ivh.Common.VisitPay.Enums;

    public class FinancePlanLogDto
    {
        public DateTime InsertDate { get; set; }
        public int VisitId { get; set; }
        public decimal Amount { get; set; } //FinancePlanVisitPrincipalAmount.Amount or FinancePlanVisitInterestDue.InterestDue
        public decimal? VisitBalanceAtTimeOfAssessement { get; set; }
        public decimal? InterestRateUsedForAssessement { get; set; }
        public FinancePlanVisitInterestDueTypeEnum FinancePlanVisitInterestDueType { get; set; }
        public string FinancePlanLogType { get; set; }
        public string BillingApplication { get; set; }
        public string SourceSystemKey { get; set; }
    }
}
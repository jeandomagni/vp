﻿namespace Ivh.Application.FinanceManagement.Common.Dtos
{
    public class ServiceGroupDto
    {
        public int ServiceGroupId { get; set; }

        public string ServiceGroupName { get; set; }

        public string Logo { get; set; }

        public string Description { get; set; }

        public string StatementDisclaimer { get; set; }
    }
}
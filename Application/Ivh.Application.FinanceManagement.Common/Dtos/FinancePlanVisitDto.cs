﻿namespace Ivh.Application.FinanceManagement.Common.Dtos
{
    using System;

    public class FinancePlanVisitDto
    {
        public virtual int FinancePlanVisitId { get; set; }

        public virtual FinancePlanDto FinancePlan { get; set; }

        public virtual int VpGuarantorId { get; set; }

        public virtual int VisitId { get; set; }

        public virtual decimal? OriginatedSnapshotVisitBalance { get; set; }

        public virtual decimal? RemovedSnapshotVisitBalance { get; set; }

        public virtual DateTime? InsertDate { get; set; }

        public virtual DateTime? HardRemoveDate { get; set; }

        public virtual DateTime? SoftRemoveDate { get; set; }

        public virtual string SourceSystemKey { get; set; }
        public virtual int? FacilityId { get; set; }
    }
}

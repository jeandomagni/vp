﻿namespace Ivh.Application.FinanceManagement.Common.Dtos
{
    using System.Collections.Generic;

    public class CreateFinancePlanResponse
    {
        public CreateFinancePlanResponse(bool isError, string errorMessage, FinancePlanDto financePlan)
        {
            this.IsError = isError;
            this.ErrorMessage = errorMessage;
            this.FinancePlan = financePlan;
            this.Payments = new List<ProcessPaymentResponse>();
        }
        
        public bool IsError { get; set; }
        public string ErrorMessage { get; set; }
        public FinancePlanDto FinancePlan { get; set; }
        public IList<ProcessPaymentResponse> Payments { get; set; }
    }
}

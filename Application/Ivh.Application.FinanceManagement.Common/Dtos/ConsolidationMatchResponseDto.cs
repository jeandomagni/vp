﻿namespace Ivh.Application.FinanceManagement.Common.Dtos
{
    using Core.Common.Dtos;
    using Ivh.Common.Base.Enums;
    using Ivh.Common.VisitPay.Enums;

    public class ConsolidationMatchResponseDto
    {
        public ConsolidationMatchStatusEnum ConsolidationMatchStatus { get; set; }

        public GuarantorDto MatchedGuarantor { get; set; }
    }
}
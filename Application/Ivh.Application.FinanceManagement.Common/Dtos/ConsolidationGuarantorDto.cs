﻿namespace Ivh.Application.FinanceManagement.Common.Dtos
{
    using System;
    using Content.Common.Dtos;
    using Core.Common.Dtos;
    using Ivh.Common.Base.Enums;
    using Ivh.Common.VisitPay.Enums;
    using User.Common.Dtos;

    public class ConsolidationGuarantorDto
    {
        public int ConsolidationGuarantorId { get; set; }
        public VisitPayUserDto InitiatedByUser { get; set; }
        public GuarantorDto ManagedGuarantor { get; set; }
        public GuarantorDto ManagingGuarantor { get; set; }
        public ConsolidationGuarantorStatusEnum ConsolidationGuarantorStatus { get; set; }
        public DateTime InsertDate { get; set; }
        public CmsVersionDto AcceptedByManagedGuarantorCmsVersion { get; set; }
        public CmsVersionDto AcceptedByManagingGuarantorCmsVersion { get; set; }
        public DateTime? DateAcceptedByManagedGuarantor { get; set; }
        public DateTime? DateAcceptedByManagingGuarantor { get; set; }
        public DateTime? DateAcceptedFinancePlanTermsByManagingGuarantor { get; set; }
        public VisitPayUserDto CancelledByUser { get; set; }
        public DateTime? CancelledOn { get; set; }
        public ConsolidationGuarantorCancellationReasonEnum? ConsolidationGuarantorCancellationReason { get; set; }
        public bool IsActive { get; set; }
        public bool IsInactive { get; set; }

        public bool IsPending { get; set; }
    }
}
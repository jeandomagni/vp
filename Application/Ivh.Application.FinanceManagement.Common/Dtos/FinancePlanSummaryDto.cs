﻿namespace Ivh.Application.FinanceManagement.Common.Dtos
{
    using System;
    using System.Collections.Generic;

    public class FinancePlanSummaryDto
    {
        public decimal AmountDueSum { get; set; }
        public decimal FinancePlansBalance { get; set; }
        public int MonthsRemaining { get; set; }
        public DateTime? PaymentDueDate { get; set; }
        public IList<FinancePlanDto> FinancePlans { get; set; } = new List<FinancePlanDto>();
        public bool HasAmountDue { get; set; }
        public string StatusDisplay { get; set; }
    }
}
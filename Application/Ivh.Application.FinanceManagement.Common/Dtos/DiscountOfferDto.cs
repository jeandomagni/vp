namespace Ivh.Application.FinanceManagement.Common.Dtos
{
    using System;
    using System.Collections.Generic;

    public class DiscountOfferDto
    {
        public DiscountOfferDto()
        {
            this.VisitOffers = new List<DiscountVisitOfferDto>();
        }

        public int DiscountOfferId { get; set; }

        public IList<DiscountVisitOfferDto> VisitOffers { get; set; }

        public decimal DiscountTotalPercent { get; set; }

        public decimal DiscountTotal { get; set; }

        public decimal DiscountedTotalCurrentBalance { get; set; }

        public DateTime InsertDate { get; set; }
        
        public virtual bool IsPromptPayOnly { get; set; }
    }
}
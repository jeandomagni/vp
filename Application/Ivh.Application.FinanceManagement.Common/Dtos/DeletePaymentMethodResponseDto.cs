﻿
namespace Ivh.Application.FinanceManagement.Common.Dtos
{
    public class DeletePaymentMethodResponseDto
    {
        public bool IsError { get; set; }
        public string ErrorMessage { get; set; }
    }
}

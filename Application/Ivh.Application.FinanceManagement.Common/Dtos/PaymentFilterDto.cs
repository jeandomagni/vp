﻿using System;
using System.Collections.Generic;
using Ivh.Common.Base.Enums;

namespace Ivh.Application.FinanceManagement.Common.Dtos
{
    using Ivh.Common.VisitPay.Enums;

    public class PaymentFilterDto
    {
        public PaymentFilterDto()
        {
            this.VpGuarantorId = new List<int>();
        }

        public DateTime? PaymentDateRangeFrom { get; set; }
        public DateTime? PaymentDateRangeTo { get; set; }
        public DateTime? ScheduledPaymentDateRangeFrom { get; set; }
        public DateTime? ScheduledPaymentDateRangeTo { get; set; }
        public int? PaymentMethodId { get; set; }
        public IList<PaymentStatusEnum> PaymentStatus { get; set; }
        public int? PaymentTypeId { get; set; }
        public string TransactionId { get; set; }
        public int? TransactionRange { get; set; }
        public string PaymentId { get; set; }
        public string SortField { get; set; }
        public string SortOrder { get; set; }
        /// <summary>
        /// Payment Made For (Payment.VpGuarantorId)
        /// </summary>
        public IList<int> VpGuarantorId { get; set; }
    }
}

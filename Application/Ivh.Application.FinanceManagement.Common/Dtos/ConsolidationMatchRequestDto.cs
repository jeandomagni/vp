﻿namespace Ivh.Application.FinanceManagement.Common.Dtos
{
    using Ivh.Common.Base.Enums;
    using Ivh.Common.Base.Utilities.Helpers;
    using Ivh.Common.VisitPay.Enums;

    public class ConsolidationMatchRequestDto
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public int BirthYear { get; set; }
        public string EmailAddress { get; set; }
        public ConsolidationMatchRequestTypeEnum ConsolidationMatchRequestType { get; set; }
        public string LastNameFirstName => FormatHelper.LastNameFirstName(this.LastName, this.FirstName);
        public string FirstNameLastName => FormatHelper.FirstNameLastName(this.FirstName, this.LastName);
    }
}
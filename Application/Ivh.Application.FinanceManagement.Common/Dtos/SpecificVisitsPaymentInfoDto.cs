namespace Ivh.Application.FinanceManagement.Common.Dtos
{
    using System;
    using System.Collections.Generic;

    public class SpecificVisitsPaymentInfoDto
    {
        public IReadOnlyList<VisitPaymentDto> AvailableVisits { get; set; }
        public IReadOnlyList<PaymentMethodOptionDto> PaymentOptions { get; set; }
        public DateTime PaymentDate { get; set; }
        public decimal PaymentAmount { get; set; }
        public PaymentSummaryDto PaymentSummary { get; set; }
    }
}
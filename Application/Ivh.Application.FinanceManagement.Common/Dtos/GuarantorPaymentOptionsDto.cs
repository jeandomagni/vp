namespace Ivh.Application.FinanceManagement.Common.Dtos
{
    public class GuarantorPaymentOptionsDto
    {
        public string Name { get; set; }
        public decimal? Balance { get; set; }
    }
}
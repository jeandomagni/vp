﻿namespace Ivh.Application.FinanceManagement.Common.Dtos
{
    using System;
    using Core.Common.Dtos;
    using Ivh.Common.VisitPay.Enums;

    public class ScheduledPaymentDto
    {
        /// <summary>
        /// a recurring payment won't have a payment id
        /// </summary>
        public int? PaymentId { get; set; }

        public PaymentTypeEnum PaymentType { get; set; }
        public decimal ScheduledPaymentAmount { get; set; }
        public DateTime ScheduledPaymentDate { get; set; }
        public GuarantorDto MadeByGuarantor { get; set; }
        public GuarantorDto MadeForGuarantor { get; set; }
        public PaymentMethodDto PaymentMethod { get; set; }
    }
}
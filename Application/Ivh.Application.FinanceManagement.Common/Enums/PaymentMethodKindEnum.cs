﻿namespace Ivh.Application.FinanceManagement.Common.Enums
{
    public enum PaymentMethodKindEnum
    {
        Bank,
        Card
    }
}

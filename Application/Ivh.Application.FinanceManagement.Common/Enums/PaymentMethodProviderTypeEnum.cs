﻿namespace Ivh.Application.FinanceManagement.Common.Enums
{
    public enum PaymentMethodProviderTypeEnum
    {
        NoProvider = 0,
        Hqy = 1,
        Other = 2
    }
}
﻿namespace Ivh.Application.Core
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using System.Web;
    using AutoMapper;
    using Base.Common.Interfaces;
    using Base.Services;
    using Common.Interfaces;
    using Domain.FinanceManagement.Consolidation.Interfaces;
    using Domain.Guarantor.Entities;
    using Domain.Guarantor.Interfaces;
    using Domain.User.Entities;
    using Domain.User.Interfaces;
    using Domain.User.Services;
    using Ivh.Application.User.Common.Dtos;
    using Ivh.Common.Base.Constants;
    using Ivh.Common.Base.Enums;
    using Ivh.Common.Base.Utilities.Extensions;
    using Ivh.Common.EventJournal;
    using Ivh.Common.EventJournal.Constants;
    using Ivh.Common.EventJournal.Templates.Client;
    using Ivh.Common.EventJournal.Templates.Guarantor;
    using Ivh.Common.EventJournal.Templates.SystemAction;
    using Ivh.Common.VisitPay.Constants;
    using Ivh.Common.VisitPay.Enums;
    using Ivh.Common.Web.Extensions;
    using Microsoft.AspNet.Identity;

    public class VisitPayUserJournalEventApplicationService : ApplicationService, IVisitPayUserJournalEventApplicationService
    {
        private readonly Lazy<IVisitPayUserJournalEventService> _userJournalEventService;
        private readonly Lazy<IGuarantorService> _guarantorService;
        private readonly Lazy<IConsolidationGuarantorService> _consolidationGuarantorService;
        private readonly Lazy<VisitPayUserService> _userService;

        public VisitPayUserJournalEventApplicationService(
            Lazy<IApplicationServiceCommonService> applicationServiceCommonServices,
            Lazy<VisitPayUserService> userService,
            Lazy<IVisitPayUserJournalEventService> userJournalEventService,
            Lazy<IGuarantorService> guarantorService,
            Lazy<IConsolidationGuarantorService> consolidationGuarantorService) : base(applicationServiceCommonServices)
        {
            this._userService = userService;
            this._userJournalEventService = userJournalEventService;
            this._guarantorService = guarantorService;
            this._consolidationGuarantorService = consolidationGuarantorService;
        }

        #region Events

        public void LogStatementDownload(int guarantorVisitPayUserId, int? clientVisitPayUserId, bool isSummary, JournalEventHttpContextDto context)
        {
            int guarantorId = this._guarantorService.Value.GetGuarantorId(guarantorVisitPayUserId);
            int visitPayUserId = clientVisitPayUserId.GetValueOrDefault(guarantorVisitPayUserId);

            JournalEventContext journalEventContext = null;
            if (clientVisitPayUserId != null)
            {
                journalEventContext = this._userJournalEventService.Value.GetJournalEventContextForClient(clientVisitPayUserId ?? 0, guarantorId);
            }
            else
            {
                journalEventContext = this.GetJournalEventContextForGuarantor(guarantorVisitPayUserId);
            }
            VisitPayUserJournalEventContext visitPayUserJournalEventContext = this.GetVisitPayUserJournalEventContext(visitPayUserId, context);

            // TODO: should we indicate that the billing summary was downloaded versus the finance plan statement?

            this.Journal.AddEvent<EventGuarantorStatementDownloaded, JournalEventParameters>(
                EventGuarantorStatementDownloaded.GetParameters
                (
                    vpUserName: visitPayUserJournalEventContext.VisitPayUser.UserName
                ),
                visitPayUserJournalEventContext.JournalEventUserContext,
                journalEventContext
            );
        }

        public void LogPaymentSummaryDownload(int visitPayUserId, JournalEventHttpContextDto context)
        {
            JournalEventContext journalEventContext = this.GetJournalEventContextForGuarantor(visitPayUserId);
            VisitPayUserJournalEventContext visitPayUserJournalEventContext = this.GetVisitPayUserJournalEventContext(visitPayUserId, context);

            this.Journal.AddEvent<EventGuarantorPaymentSummaryDownloaded, JournalEventParameters>(
                EventGuarantorPaymentSummaryDownloaded.GetParameters
                (
                    vpUserName: visitPayUserJournalEventContext.VisitPayUser.UserName
                ),
                visitPayUserJournalEventContext.JournalEventUserContext,
                journalEventContext
            );
        }

        public void LogConsolidationGuarantorRequestManaging(int visitPayUserId, int vpManagedGuarantorId, JournalEventHttpContextDto context)
        {
            int managingVpGuarantorId = this._guarantorService.Value.GetGuarantorId(visitPayUserId);
            Guarantor managingGuarantor = this._guarantorService.Value.GetGuarantor(managingVpGuarantorId);

            Guarantor managedGuarantor = this._guarantorService.Value.GetGuarantor(vpManagedGuarantorId);

            JournalEventContext journalEventContext = this.GetJournalEventContextForGuarantor(visitPayUserId);
            VisitPayUserJournalEventContext visitPayUserJournalEventContext = this.GetVisitPayUserJournalEventContext(visitPayUserId, context);

            this.Journal.AddEvent<EventGuarantorRequestToManage, JournalEventParameters>(
                EventGuarantorRequestToManage.GetParameters
                (
                    managedVPUserName: managedGuarantor.User.UserName,
                    managedVpGuarantorId: managedGuarantor.VpGuarantorId.ToString(),
                    vpUserName: visitPayUserJournalEventContext.VisitPayUser.UserName
                ),
                visitPayUserJournalEventContext.JournalEventUserContext,
                journalEventContext
            );
        }

        public void LogConsolidationGuarantorRequestManaged(int visitPayUserId, int vpManagingGuarantorId, JournalEventHttpContextDto context)
        {
            int managedVpGuarantorId = this._guarantorService.Value.GetGuarantorId(visitPayUserId);
            Guarantor managedGuarantor = this._guarantorService.Value.GetGuarantor(managedVpGuarantorId);

            Guarantor managingGuarantor = this._guarantorService.Value.GetGuarantor(vpManagingGuarantorId);

            JournalEventContext journalEventContext = this.GetJournalEventContextForGuarantor(visitPayUserId);
            VisitPayUserJournalEventContext visitPayUserJournalEventContext = this.GetVisitPayUserJournalEventContext(visitPayUserId, context);

            this.Journal.AddEvent<EventGuarantorRequestToBeManaged, JournalEventParameters>(
                EventGuarantorRequestToBeManaged.GetParameters
                (
                    managingVpGuarantorId: vpManagingGuarantorId.ToString(),
                    managingVPUserName: managingGuarantor.User.UserName,
                    vpUserName: visitPayUserJournalEventContext.VisitPayUser.UserName
                ),
                visitPayUserJournalEventContext.JournalEventUserContext,
                journalEventContext
            );
        }

        public void LogViewManaged(int visitPayUserId, int vpGuarantorId, int? emulateClientUserId, JournalEventHttpContextDto context)
        {
            Guarantor managedGuarantor = this._guarantorService.Value.GetGuarantor(vpGuarantorId);

            JournalEventContext journalEventContext = null;
            if (emulateClientUserId != null)
            {
                journalEventContext = this._userJournalEventService.Value.GetJournalEventContextForClient(emulateClientUserId ?? 0, vpGuarantorId);
            }
            else
            {
                journalEventContext = this.GetJournalEventContextForGuarantor(visitPayUserId);
            }
            VisitPayUserJournalEventContext visitPayUserJournalEventContext = this.GetVisitPayUserJournalEventContext(visitPayUserId, context);
            this.Journal.AddEvent<EventGuarantorGuarantorViewedManagedUser, JournalEventParameters>(
                EventGuarantorGuarantorViewedManagedUser.GetParameters
                (
                    managedVPUserName: managedGuarantor.User.UserName,
                    vpUserName: visitPayUserJournalEventContext.VisitPayUser.UserName
                ),
                visitPayUserJournalEventContext.JournalEventUserContext,
                journalEventContext
            );
        }

        public void LogClientSetBalanceTransferStatus(int vpGuarantorId, int visitPayUserId, int clientVisitPayUserId, int? financePlanId, int? financePlanCreatorId, int visitId, decimal? visitPrincipalBalance, string notes, JournalEventHttpContextDto context)
        {
            Guarantor guarantor = this._guarantorService.Value.GetGuarantor(vpGuarantorId);

            JournalEventContext journalEventContext = this.GetJournalEventContextForGuarantor(guarantor.User.VisitPayUserId);
            VisitPayUserJournalEventContext visitPayUserJournalEventContext = this.GetVisitPayUserJournalEventContext(clientVisitPayUserId, context);

            this.Journal.AddEvent<EventClientSetBalanceTransferStatus, JournalEventParameters>(
                EventClientSetBalanceTransferStatus.GetParameters
                (
                    eventUserName: visitPayUserJournalEventContext.VisitPayUser.UserName,
                    financePlanId: (financePlanId ?? 0).ToString(),
                    vpVisitId: visitId.ToString()
                ),
                visitPayUserJournalEventContext.JournalEventUserContext,
                journalEventContext
            );
        }

        public void LogClientRemovedBalanceTransferStatus(int vpGuarantorId, int visitPayUserId, int clientVisitPayUserId, int? financePlanId, int? financePlanCreatorId, int visitId, decimal? visitPrincipalBalance, string notes, JournalEventHttpContextDto context)
        {
            Guarantor guarantor = this._guarantorService.Value.GetGuarantor(vpGuarantorId);

            JournalEventContext journalEventContext = this.GetJournalEventContextForGuarantor(guarantor.User.VisitPayUserId);
            VisitPayUserJournalEventContext visitPayUserJournalEventContext = this.GetVisitPayUserJournalEventContext(clientVisitPayUserId, context);

            this.Journal.AddEvent<EventClientRemovedBalanceTransferStatus, JournalEventParameters>(
                EventClientRemovedBalanceTransferStatus.GetParameters
                (
                    eventUserName: visitPayUserJournalEventContext.VisitPayUser.UserName,
                    financePlanId: (financePlanId ?? 0).ToString(),
                    vpVisitId: visitId.ToString()
                ),
                visitPayUserJournalEventContext.JournalEventUserContext,
                journalEventContext
            );
        }

        public void LogViewAuditLogDetail(int visitPayUserId, JournalEventHttpContextDto context, int eventJournalId)
        {
            JournalEventContext journalEventContext = this._userJournalEventService.Value.GetJournalEventContextForClient(visitPayUserId, null);
            VisitPayUserJournalEventContext visitPayUserJournalEventContext = this.GetVisitPayUserJournalEventContext(visitPayUserId, context);

            this.Journal.AddEvent<EventClientViewEventLogDetail, JournalEventParameters>(
                EventClientViewEventLogDetail.GetParameters
                (
                    eventUserName: visitPayUserJournalEventContext.VisitPayUser.UserName,
                    eventJournalId: eventJournalId.ToString()
                ),
                visitPayUserJournalEventContext.JournalEventUserContext,
                journalEventContext
            );
        }

        public void LogViewAuditLogList(int visitPayUserId, JournalEventHttpContextDto context, JournalEventTypeEnum? journalEventType, DateTime? beginDate, DateTime? endDate, string userName, int? vpGuarantor)
        {
            JournalEventContext journalEventContext = this._userJournalEventService.Value.GetJournalEventContextForClient(visitPayUserId, vpGuarantor);
            VisitPayUserJournalEventContext visitPayUserJournalEventContext = this.GetVisitPayUserJournalEventContext(visitPayUserId, context);

            this.Journal.AddEvent<EventClientViewEventLog, JournalEventParameters>(
                EventClientViewEventLog.GetParameters
                (
                    eventUserName: visitPayUserJournalEventContext.VisitPayUser.UserName
                ),
                visitPayUserJournalEventContext.JournalEventUserContext,
                journalEventContext
            );
        }

        public void LogSmsCategoryChanged(int targetVisitPayUserId, int visitPayUserId, int vpGuarantorId, string category, string changeValue, JournalEventHttpContextDto context)
        {
            Guarantor guarantor = this._guarantorService.Value.GetGuarantor(vpGuarantorId);

            JournalEventContext journalEventContext = this._userJournalEventService.Value.GetJournalEventContextForGuarantor(guarantor.User.VisitPayUserId, vpGuarantorId);
            VisitPayUserJournalEventContext visitPayUserJournalEventContext = this.GetVisitPayUserJournalEventContext(visitPayUserId, context);

            if (visitPayUserJournalEventContext.JournalEventUserContext.IsClientUser)
            {
                this.Journal.AddEvent<EventClientSmsCategoryChanged, JournalEventParameters>(
                    EventClientSmsCategoryChanged.GetParameters
                    (
                        eventUserName: visitPayUserJournalEventContext.VisitPayUser.UserName,
                        smsCategoryName: category,
                        smsCategorySwitchValue: changeValue,
                        vpUserName: guarantor.User.UserName
                    ),
                    visitPayUserJournalEventContext.JournalEventUserContext,
                    journalEventContext
                );
            }
            else
            {
                this.Journal.AddEvent<EventGuarantorSmsCategoryChanged, JournalEventParameters>(
                    EventGuarantorSmsCategoryChanged.GetParameters
                    (
                        smsCategoryName: category,
                        smsCategorySwitchValue: changeValue,
                        vpUserName: guarantor.User.UserName
                    ),
                    visitPayUserJournalEventContext.JournalEventUserContext,
                    journalEventContext
                );
            }

        }

        public void LogRegistrationAuthentication(bool success, string guarantorId, string lastName, DateTime dateOfBirth, string ssn4, string zip, JournalEventHttpContextDto context)
        {
            JournalEventUserContext journalEventUserContext = JournalEventUserContext.BuildSystemUserContext(context);
            JournalEventContext journalEventContext = this._userJournalEventService.Value.GetJournalEventContextForSystem(SystemUsers.SystemUserId, null);
            const string missingField = "not provided";
            ssn4 = ssn4 ?? missingField;
            zip = zip ?? missingField;

            if (success)
            {
                this.Journal.AddEvent<EventSystemActionRegistrationAuthSuccess, JournalEventParameters>(
                    EventSystemActionRegistrationAuthSuccess.GetParameters
                    (
                        dateOfBirth: dateOfBirth.ToShortDateString(),
                        hsGuarantorId: guarantorId,
                        lastName: lastName,
                        sSN4: ssn4,
                        zip: zip
                    ),
                    journalEventUserContext,
                    journalEventContext
                );
            }
            else
            {
                this.Journal.AddEvent<EventSystemActionRegistrationAuthFailure, JournalEventParameters>(
                    EventSystemActionRegistrationAuthFailure.GetParameters
                    (
                        dateOfBirth: dateOfBirth.ToShortDateString(),
                        hsGuarantorId: guarantorId,
                        lastName: lastName,
                        sSN4: ssn4,
                        zip: zip
                    ),
                    journalEventUserContext,
                    journalEventContext
                );
            }

        }

        private class RequestingGuarantorConsolidationContext
        {
            public string RequestingVpUserName { get; set; }
            public string RequestingVpGuarantorId { get; set; }
        }

        private RequestingGuarantorConsolidationContext GetRequestingGuarantorConsolidationContext(int consolidationGuarantorId, int visitPayUserId)
        {
            RequestingGuarantorConsolidationContext context = new RequestingGuarantorConsolidationContext();
            ConsolidationGuarantor consolidationGuarantor = Task.Run(() => this._consolidationGuarantorService.Value.GetConsolidationGuarantorAsync(consolidationGuarantorId, visitPayUserId)).Result;
            context.RequestingVpUserName = consolidationGuarantor?.InitiatedByUser.UserName ?? "Unknown";
            if (consolidationGuarantor?.InitiatedByUser.VisitPayUserId == consolidationGuarantor?.ManagedGuarantor.User.VisitPayUserId)
            {
                context.RequestingVpGuarantorId = (consolidationGuarantor?.ManagedGuarantor.VpGuarantorId ?? 0).ToString();
            }
            else
            {
                context.RequestingVpGuarantorId = (consolidationGuarantor?.ManagingGuarantor.VpGuarantorId ?? 0).ToString();
            }

            return context;
        }

        private Guarantor GetManagedVpGuarantorForConsolidation(int consolidationGuarantorId, int visitPayUserId)
        {
            ConsolidationGuarantor consolidationGuarantor = Task.Run(() => this._consolidationGuarantorService.Value.GetConsolidationGuarantorAsync(consolidationGuarantorId, visitPayUserId)).Result;
            int requestingVpGuarantorId = consolidationGuarantor?.ManagedGuarantor.VpGuarantorId ?? 0;
            Guarantor requestingGuarantor = this._guarantorService.Value.GetGuarantor(requestingVpGuarantorId);
            return requestingGuarantor;
        }

        public void LogGuarantorConsolidationRequestAccepted(int visitPayUserId, int vpGuarantorId, int consolidationGuarantorId, JournalEventHttpContextDto context)
        {
            RequestingGuarantorConsolidationContext requestingContext = this.GetRequestingGuarantorConsolidationContext(consolidationGuarantorId, visitPayUserId);

            JournalEventContext journalEventContext = this._userJournalEventService.Value.GetJournalEventContextForGuarantor(visitPayUserId, vpGuarantorId);
            VisitPayUserJournalEventContext visitPayUserJournalEventContext = this.GetVisitPayUserJournalEventContext(visitPayUserId, context);

            this.Journal.AddEvent<EventGuarantorConsolidationRequestAccepted, JournalEventParameters>(
                EventGuarantorConsolidationRequestAccepted.GetParameters
                (
                    requestingUserName: requestingContext.RequestingVpUserName,
                    requestingVpGuarantorId: requestingContext.RequestingVpGuarantorId,
                    vpUserName: visitPayUserJournalEventContext.VisitPayUser.UserName
                ),
                visitPayUserJournalEventContext.JournalEventUserContext,
                journalEventContext
            );
        }

        public void LogGuarantorConsolidationRequestRejected(int visitPayUserId, int vpGuarantorId, int consolidationGuarantorId, JournalEventHttpContextDto context)
        {
            RequestingGuarantorConsolidationContext requestingContext = this.GetRequestingGuarantorConsolidationContext(consolidationGuarantorId, visitPayUserId);

            JournalEventContext journalEventContext = this._userJournalEventService.Value.GetJournalEventContextForGuarantor(visitPayUserId, vpGuarantorId);
            VisitPayUserJournalEventContext visitPayUserJournalEventContext = this.GetVisitPayUserJournalEventContext(visitPayUserId, context);

            this.Journal.AddEvent<EventGuarantorConsolidationRequestRejected, JournalEventParameters>(
                EventGuarantorConsolidationRequestRejected.GetParameters
                (
                    requestingVpGuarantorId: requestingContext.RequestingVpGuarantorId,
                    requestingVPUserName: requestingContext.RequestingVpUserName,
                    vpUserName: visitPayUserJournalEventContext.VisitPayUser.UserName
                ),
                visitPayUserJournalEventContext.JournalEventUserContext,
                journalEventContext
            );
        }

        public void LogGuarantorConsolidationRequestCancelled(int visitPayUserId, int vpGuarantorId, int consolidationGuarantorId, JournalEventHttpContextDto context)
        {
            RequestingGuarantorConsolidationContext requestingContext = this.GetRequestingGuarantorConsolidationContext(consolidationGuarantorId, visitPayUserId);

            JournalEventContext journalEventContext = this._userJournalEventService.Value.GetJournalEventContextForGuarantor(visitPayUserId, vpGuarantorId);
            VisitPayUserJournalEventContext visitPayUserJournalEventContext = this.GetVisitPayUserJournalEventContext(visitPayUserId, context);

            this.Journal.AddEvent<EventGuarantorConsolidationRequestCanceled, JournalEventParameters>(
                EventGuarantorConsolidationRequestCanceled.GetParameters
                (
                    requestingVpGuarantorId: requestingContext.RequestingVpGuarantorId,
                    requestingVPUserName: requestingContext.RequestingVpUserName,
                    vpUserName: visitPayUserJournalEventContext.VisitPayUser.UserName
                ),
                visitPayUserJournalEventContext.JournalEventUserContext,
                journalEventContext
            );
        }

        public void LogClientChangedFinancePlanBucket(int clientVisitPayUserId, int vpGuarantorId, int financePlanId, int previousBucket, int newBucket, JournalEventHttpContextDto context)
        {
            Guarantor guarantor = this._guarantorService.Value.GetGuarantor(vpGuarantorId);

            JournalEventContext journalEventContext = this._userJournalEventService.Value.GetJournalEventContextForClient(clientVisitPayUserId, vpGuarantorId);
            journalEventContext.FinancePlanId = financePlanId;
            VisitPayUserJournalEventContext visitPayUserJournalEventContext = this.GetVisitPayUserJournalEventContext(clientVisitPayUserId, context);

            this.Journal.AddEvent<EventClientModifiedFinancePlanAging, JournalEventParameters>(
                EventClientModifiedFinancePlanAging.GetParameters
                (
                    eventUserName: visitPayUserJournalEventContext.VisitPayUser.UserName,
                    newAge: newBucket.ToString(),
                    previousAge: previousBucket.ToString(),
                    vpUserName: guarantor.User.UserName
                ),
                visitPayUserJournalEventContext.JournalEventUserContext,
                journalEventContext
            );
        }
        public void LogClientAddedPaymentMethod(int clientVisitPayUserId, int vpGuarantorId, string vpGuarantorUserName, bool isPrimary, string billingId, JournalEventHttpContextDto context)
        {
            JournalEventContext journalEventContext = this._userJournalEventService.Value.GetJournalEventContextForClient(clientVisitPayUserId, vpGuarantorId);
            VisitPayUserJournalEventContext visitPayUserJournalEventContext = this.GetVisitPayUserJournalEventContext(clientVisitPayUserId, context);
            VisitPayUser clientUser = _userService.Value.FindById(clientVisitPayUserId.ToString());

            if (isPrimary)
            {
                this.Journal.AddEvent<EventClientAddedAPaymentMethodPrimary, JournalEventParameters>(
                    EventClientAddedAPaymentMethodPrimary.GetParameters(
                        accountNumber: billingId,
                        eventUserName: clientUser.UserName,
                        primaryFlag: isPrimary.ToString(),
                        vpUserName: vpGuarantorUserName
                    ),
                    visitPayUserJournalEventContext.JournalEventUserContext,
                    journalEventContext);
            }
            else
            {
                this.Journal.AddEvent<EventClientAddedAPaymentMethod, JournalEventParameters>(
                    EventClientAddedAPaymentMethodPrimary.GetParameters(
                        accountNumber: billingId,
                        eventUserName: clientUser.UserName,
                        primaryFlag: isPrimary.ToString(),
                        vpUserName: vpGuarantorUserName
                    ),
                    visitPayUserJournalEventContext.JournalEventUserContext,
                    journalEventContext);
            }
        }

        public void LogGuarantorAddedPaymentMethod(int visitPayUserId, int vpGuarantorId, string vpGuarantorUserName, bool isPrimary, string billingId, JournalEventHttpContextDto context)
        {
            JournalEventContext journalEventContext = this._userJournalEventService.Value.GetJournalEventContextForClient(visitPayUserId, vpGuarantorId);
            VisitPayUserJournalEventContext visitPayUserJournalEventContext = this.GetVisitPayUserJournalEventContext(vpGuarantorId, context);

            if (isPrimary)
            {
                this.Journal.AddEvent<EventGuarantorAddedAPaymentMethodPrimary, JournalEventParameters>(
                    EventClientAddedAPaymentMethodPrimary.GetParameters(
                        accountNumber: billingId,
                        eventUserName: vpGuarantorUserName,
                        primaryFlag: isPrimary.ToString(),
                        vpUserName: vpGuarantorUserName
                    ),
                    visitPayUserJournalEventContext.JournalEventUserContext,
                    journalEventContext);
            }
            else
            {
                this.Journal.AddEvent<EventGuarantorAddedAPaymentMethod, JournalEventParameters>(
                    EventClientAddedAPaymentMethodPrimary.GetParameters(
                        accountNumber: billingId,
                        eventUserName: vpGuarantorUserName,
                        primaryFlag: isPrimary.ToString(),
                        vpUserName: vpGuarantorUserName
                    ),
                    visitPayUserJournalEventContext.JournalEventUserContext,
                    journalEventContext);
            }
        }

        public void LogClientRefundedPayment(int clientVisitPayUserId, int vpGuarantorId, int paymentId, int paymentType, JournalEventHttpContextDto context)
        {
            JournalEventContext journalEventContext = this._userJournalEventService.Value.GetJournalEventContextForClient(clientVisitPayUserId, vpGuarantorId);
            VisitPayUserJournalEventContext visitPayUserJournalEventContext = this.GetVisitPayUserJournalEventContext(vpGuarantorId, context);
            Guarantor guarantor = this._guarantorService.Value.GetGuarantor(vpGuarantorId);
            VisitPayUser eventUser = this._userService.Value.FindById(journalEventContext.VisitPayUserId.ToString());
            journalEventContext.PaymentId = paymentId;

            this.Journal.AddEvent<EventClientRefundedPayment, JournalEventParameters>(
                EventClientRefundedPayment.GetParameters
                (
                    eventUserName: eventUser.UserName,
                    vpUserId: guarantor.User.VisitPayUserId.ToString(),
                    paymentId: paymentId.ToString(),
                    paymentType: paymentType.ToString()
                ),
                visitPayUserJournalEventContext.JournalEventUserContext,
                journalEventContext);
        }

        public void LogGuarantorChangedPrimaryPaymentMethod(int visitPayUserId, int vpGuarantorId, string vpGuarantorUserName, string billingId, JournalEventHttpContextDto context)
        {
            JournalEventContext journalEventContext = this._userJournalEventService.Value.GetJournalEventContextForClient(visitPayUserId, vpGuarantorId);
            VisitPayUserJournalEventContext visitPayUserJournalEventContext = this.GetVisitPayUserJournalEventContext(vpGuarantorId, context);

            this.Journal.AddEvent<EventGuarantorModifiedAPaymentMethodToPrimary, JournalEventParameters>(
                EventClientAddedAPaymentMethodPrimary.GetParameters(
                    accountNumber: billingId,
                    eventUserName: vpGuarantorUserName,
                    primaryFlag: "true",
                    vpUserName: vpGuarantorUserName
                ),
            visitPayUserJournalEventContext.JournalEventUserContext,
            journalEventContext);
        }

        public void LogClientChangedPrimaryPaymentMethod(int clientVisitPayUserId, int vpGuarantorId, string vpGuarantorUserName, string billingId, JournalEventHttpContextDto context)
        {
            JournalEventContext journalEventContext = this._userJournalEventService.Value.GetJournalEventContextForClient(clientVisitPayUserId, vpGuarantorId);
            VisitPayUserJournalEventContext visitPayUserJournalEventContext = this.GetVisitPayUserJournalEventContext(vpGuarantorId, context);
            VisitPayUser clientUser = this._userService.Value.FindById(clientVisitPayUserId.ToString());

            this.Journal.AddEvent<EventClientModifedAPaymentMethodPrimary, JournalEventParameters>(
                EventClientAddedAPaymentMethodPrimary.GetParameters(
                    accountNumber: billingId,
                    eventUserName: clientUser.UserName,
                    primaryFlag: "true",
                    vpUserName: vpGuarantorUserName
                ),
            visitPayUserJournalEventContext.JournalEventUserContext,
            journalEventContext);
        }

        public void LogClientChangedVisitAgingTier(int clientVisitPayUserId, int vpGuarantorId, int visitId, AgingTierEnum previousAgingTier, AgingTierEnum newAgingTier, string reason, JournalEventHttpContextDto context)
        {
            Guarantor guarantor = this._guarantorService.Value.GetGuarantor(vpGuarantorId);

            JournalEventContext journalEventContext = this._userJournalEventService.Value.GetJournalEventContextForClient(clientVisitPayUserId, vpGuarantorId);
            journalEventContext.VisitId = visitId;
            VisitPayUserJournalEventContext visitPayUserJournalEventContext = this.GetVisitPayUserJournalEventContext(clientVisitPayUserId, context);

            this.Journal.AddEvent<EventClientVisitAgingTierChange, JournalEventParameters>(
                EventClientVisitAgingTierChange.GetParameters
                (
                    eventUserName: visitPayUserJournalEventContext.VisitPayUser.UserName,
                    newAge: newAgingTier.ToString().InsertSpaceBetweenLowerAndCapitalLetter(),
                    previousAge: previousAgingTier.ToString().InsertSpaceBetweenLowerAndCapitalLetter(),
                    vpUserName: guarantor.User.UserName,
                    reason: reason
                ),
                visitPayUserJournalEventContext.JournalEventUserContext,
                journalEventContext
            );
        }

        public void LogClientAddHold(int visitPayUserId, int vpGuarantorId, int visitId, JournalEventHttpContextDto context)
        {
            Guarantor guarantor = this._guarantorService.Value.GetGuarantor(vpGuarantorId);

            JournalEventContext journalEventContext = this._userJournalEventService.Value.GetJournalEventContextForSystem(visitPayUserId, vpGuarantorId);
            VisitPayUserJournalEventContext visitPayUserJournalEventContext = this.GetVisitPayUserJournalEventContext(visitPayUserId, context);

            this.Journal.AddEvent<EventSystemActionVisitHoldAdded, JournalEventParameters>(
                EventSystemActionVisitHoldAdded.GetParameters
                (
                    vpUserName: guarantor.User.UserName,
                    vpVisitId: visitId.ToString()
                ),
                visitPayUserJournalEventContext.JournalEventUserContext,
                journalEventContext
            );
        }

        public void LogClientClearHold(int visitPayUserId, int vpGuarantorId, int visitId, JournalEventHttpContextDto context)
        {
            Guarantor guarantor = this._guarantorService.Value.GetGuarantor(vpGuarantorId);

            JournalEventContext journalEventContext = this._userJournalEventService.Value.GetJournalEventContextForSystem(visitPayUserId, vpGuarantorId);
            VisitPayUserJournalEventContext visitPayUserJournalEventContext = this.GetVisitPayUserJournalEventContext(visitPayUserId, context);

            this.Journal.AddEvent<EventSystemActionVisitHoldRemoved, JournalEventParameters>(
                EventSystemActionVisitHoldRemoved.GetParameters
                (
                    vpUserName: guarantor.User.UserName,
                    vpVisitId: visitId.ToString()
                ),
                visitPayUserJournalEventContext.JournalEventUserContext,
                journalEventContext
            );
        }

        public void LogRegistrationComplete(int visitPayUserId, int vpGuarantorId, string hsGuarantorId, string lastName, DateTime dateOfBirth, string ssn4, string zip, JournalEventHttpContextDto context)
        {
            Guarantor guarantor = this._guarantorService.Value.GetGuarantor(vpGuarantorId);
            JournalEventContext journalEventContext = this._userJournalEventService.Value.GetJournalEventContextForSystem(visitPayUserId, vpGuarantorId);
            VisitPayUserJournalEventContext visitPayUserJournalEventContext = this.GetVisitPayUserJournalEventContext(visitPayUserId, context);

            this.Journal.AddEvent<EventSystemActionRegistrationCompleteNonSso, JournalEventParameters>(
                EventSystemActionRegistrationCompleteNonSso.GetParameters
                (
                    dateOfBirth: dateOfBirth.ToShortDateString(),
                    hsGuarantorId: hsGuarantorId,
                    lastName: lastName,
                    sSN4: ssn4,
                    vpUserName: guarantor.User.UserName,
                    zip: zip
                ),
                visitPayUserJournalEventContext.JournalEventUserContext,
                journalEventContext
            );
        }

        public void LogClientRequestPassword(int targetClientVisitPayUserId, int clientVisitPayUserId, JournalEventHttpContextDto context)
        {
            JournalEventContext journalEventContext = this._userJournalEventService.Value.GetJournalEventContextForClient(clientVisitPayUserId, null);
            VisitPayUserJournalEventContext visitPayUserJournalEventContext = this.GetVisitPayUserJournalEventContext(clientVisitPayUserId, context);

            VisitPayUser targetClientVisitPayUser = this.GetVisitPayUser(targetClientVisitPayUserId);

            this.Journal.AddEvent<EventClientRequestANewClientUserPassword, JournalEventParameters>(
                EventClientRequestANewClientUserPassword.GetParameters
                (
                    clientUserName: targetClientVisitPayUser.UserName,
                    eventUserName: visitPayUserJournalEventContext.VisitPayUser.UserName
                ),
                visitPayUserJournalEventContext.JournalEventUserContext,
                journalEventContext
            );
        }

        public void LogBillingIdFailure(int visitPayUserId, int vpGuarantorId, IReadOnlyDictionary<string, string> response, JournalEventHttpContextDto context)
        {
            this.LogAction((journalEventUserContext) => { this._userJournalEventService.Value.LogBillingIdFailure(journalEventUserContext, vpGuarantorId, response != null ? string.Join(";", response.Select(x => x.Key + "=" + x.Value).ToArray()) : ""); }, visitPayUserId, context);
        }

        public void LogClientSupportRequestView(int visitPayUserId, int vpGuarantorId, int supportRequestId, JournalEventHttpContextDto context)
        {
            Guarantor guarantor = this._guarantorService.Value.GetGuarantor(vpGuarantorId);
            JournalEventContext journalEventContext = this._userJournalEventService.Value.GetJournalEventContextForSystem(visitPayUserId, vpGuarantorId);
            VisitPayUserJournalEventContext visitPayUserJournalEventContext = this.GetVisitPayUserJournalEventContext(visitPayUserId, context);

            this.Journal.AddEvent<EventClientViewSupportCase, JournalEventParameters>(
                EventClientViewSupportCase.GetParameters
                (
                    eventUserName: visitPayUserJournalEventContext.VisitPayUser.UserName,
                    supportRequestId: supportRequestId.ToString(),
                    vpUserName: guarantor.User.UserName
                ),
                visitPayUserJournalEventContext.JournalEventUserContext,
                journalEventContext
            );
        }

        public void LogClientAccountModify(int visitPayUserId, int vpGuarantorId, JournalEventHttpContextDto context)
        {
            Guarantor guarantor = this._guarantorService.Value.GetGuarantor(vpGuarantorId);
            JournalEventContext journalEventContext = this._userJournalEventService.Value.GetJournalEventContextForClient(visitPayUserId, vpGuarantorId);
            VisitPayUserJournalEventContext visitPayUserJournalEventContext = this.GetVisitPayUserJournalEventContext(visitPayUserId, context);

            this.Journal.AddEvent<EventClientModifiedVpGuarantorPersonalInfo, JournalEventParameters>(
                EventClientModifiedVpGuarantorPersonalInfo.GetParameters
                (
                    eventUserName: visitPayUserJournalEventContext.VisitPayUser.UserName,
                    vpUserName: guarantor.User.UserName
                ),
                visitPayUserJournalEventContext.JournalEventUserContext,
                journalEventContext
            );
        }

        public void LogClientAccountSearch(int visitPayUserId, IList<int> vpGuarantorIds, JournalEventHttpContextDto context)
        {
            if (vpGuarantorIds.Any())
            {
                VisitPayUserJournalEventContext visitPayUserJournalEventContext = this.GetVisitPayUserJournalEventContext(visitPayUserId, context);

                foreach (int vpGuarantorId in vpGuarantorIds)
                {
                    Guarantor guarantor = this._guarantorService.Value.GetGuarantor(vpGuarantorId);
                    JournalEventContext journalEventContext = this._userJournalEventService.Value.GetJournalEventContextForClient(visitPayUserId, vpGuarantorId);

                    this.Journal.AddEvent<EventClientClientAccountSearch, JournalEventParameters>(
                        EventClientClientAccountSearch.GetParameters
                        (
                            eventUserName: visitPayUserJournalEventContext.VisitPayUser.UserName,
                            vpUserName: guarantor.User.UserName
                        ),
                        visitPayUserJournalEventContext.JournalEventUserContext,
                        journalEventContext
                    );
                }
            }
        }

        public void LogClientAccountView(int visitPayUserId, int vpGuarantorId, JournalEventHttpContextDto context)
        {
            VisitPayUserJournalEventContext visitPayUserJournalEventContext = this.GetVisitPayUserJournalEventContext(visitPayUserId, context);
            JournalEventContext journalEventContext = this._userJournalEventService.Value.GetJournalEventContextForClient(visitPayUserId, vpGuarantorId);
            Guarantor guarantor = this._guarantorService.Value.GetGuarantor(vpGuarantorId);

            this.Journal.AddEvent<EventClientClientAccountView, JournalEventParameters>(
                EventClientClientAccountView.GetParameters
                (
                    eventUserName: visitPayUserJournalEventContext.VisitPayUser.UserName,
                    vpUserName: guarantor.User.UserName
                ),
                visitPayUserJournalEventContext.JournalEventUserContext,
                journalEventContext
            );
        }

        public void LogClientUserModifyClientAccount(int targetClientVisitPayUserId, int clientVisitPayUserId, IList<string> assignedRoles, JournalEventHttpContextDto context)
        {
            JournalEventContext journalEventContext = this._userJournalEventService.Value.GetJournalEventContextForClient(clientVisitPayUserId, null);
            VisitPayUserJournalEventContext visitPayUserJournalEventContext = this.GetVisitPayUserJournalEventContext(clientVisitPayUserId, context);

            VisitPayUser targetClientVisitPayUser = this.GetVisitPayUser(targetClientVisitPayUserId);
            string delimitedRoles = string.Join(",", assignedRoles ?? new List<string>());

            if (visitPayUserJournalEventContext.JournalEventUserContext.IsIvinciUser)
            {
                this.Journal.AddEvent<EventClientIvhAdminModifyClientAccount, JournalEventParameters>(
                    EventClientIvhAdminModifyClientAccount.GetParameters
                    (
                        assignedRoles: delimitedRoles,
                        clientUserName: targetClientVisitPayUser.UserName,
                        eventUserName: visitPayUserJournalEventContext.VisitPayUser.UserName
                    ),
                    visitPayUserJournalEventContext.JournalEventUserContext,
                    journalEventContext
                );
            }
            else
            {
                this.Journal.AddEvent<EventClientModifiedAClientUserAcct, JournalEventParameters>(
                    EventClientModifiedAClientUserAcct.GetParameters
                    (
                        assignedRoles: delimitedRoles,
                        clientUserName: targetClientVisitPayUser.UserName,
                        eventUserName: visitPayUserJournalEventContext.VisitPayUser.UserName
                    ),
                    visitPayUserJournalEventContext.JournalEventUserContext,
                    journalEventContext
                );
            }
        }

        public void LogClientUserCreateClientAccount(int targetClientVisitPayUserId, int clientVisitPayUserId, IList<string> assignedRoles, JournalEventHttpContextDto context)
        {
            JournalEventContext journalEventContext = this._userJournalEventService.Value.GetJournalEventContextForClient(clientVisitPayUserId, null);
            VisitPayUserJournalEventContext visitPayUserJournalEventContext = this.GetVisitPayUserJournalEventContext(clientVisitPayUserId, context);

            VisitPayUser targetClientVisitPayUser = this.GetVisitPayUser(targetClientVisitPayUserId);
            string delimitedRoles = string.Join(",", assignedRoles ?? new List<string>());

            if (visitPayUserJournalEventContext.JournalEventUserContext.IsIvinciUser)
            {
                this.Journal.AddEvent<EventClientIvhAdminCreatedAClientUserAcct, JournalEventParameters>(
                    EventClientIvhAdminCreatedAClientUserAcct.GetParameters
                    (
                        assignedRoles: delimitedRoles,
                        clientUserName: targetClientVisitPayUser.UserName,
                        eventUserName: visitPayUserJournalEventContext.VisitPayUser.UserName
                    ),
                    visitPayUserJournalEventContext.JournalEventUserContext,
                    journalEventContext
                );
            }
            else
            {
                this.Journal.AddEvent<EventClientCreatedAClientUserAcct, JournalEventParameters>(
                    EventClientCreatedAClientUserAcct.GetParameters
                    (
                        assignedRoles: delimitedRoles,
                        clientUserName: targetClientVisitPayUser.UserName,
                        eventUserName: visitPayUserJournalEventContext.VisitPayUser.UserName
                    ),
                    visitPayUserJournalEventContext.JournalEventUserContext,
                    journalEventContext
                );
            }
        }

        public void LogViewSecurityQuestionsEvent(int clientVisitPayUserId, int vpGuarantorId, JournalEventHttpContextDto context)
        {
            JournalEventContext journalEventContext = this._userJournalEventService.Value.GetJournalEventContextForClient(clientVisitPayUserId, vpGuarantorId);
            VisitPayUserJournalEventContext visitPayUserJournalEventContext = this.GetVisitPayUserJournalEventContext(clientVisitPayUserId, context);
            Guarantor guarantor = this._guarantorService.Value.GetGuarantor(vpGuarantorId);

            this.Journal.AddEvent<EventClientViewSecurityQuestions, JournalEventParameters>(
                EventClientViewSecurityQuestions.GetParameters
                (
                    eventUserName: visitPayUserJournalEventContext.VisitPayUser.UserName,
                    vpUserName: guarantor.User.UserName
                ),
                visitPayUserJournalEventContext.JournalEventUserContext,
                journalEventContext
            );

        }

        public void LogCardReaderKeyAdded(int clientVisitPayUserId, string deviceKey, JournalEventHttpContextDto context)
        {
            JournalEventContext journalEventContext = this._userJournalEventService.Value.GetJournalEventContextForClient(clientVisitPayUserId, null);
            VisitPayUserJournalEventContext visitPayUserJournalEventContext = this.GetVisitPayUserJournalEventContext(clientVisitPayUserId, context);

            this.Journal.AddEvent<EventClientAddedCardReader, JournalEventParameters>(
                EventClientAddedCardReader.GetParameters
                (
                    eventUserName: visitPayUserJournalEventContext.VisitPayUser.UserName,
                    cardReaderKey: deviceKey
                ),
                visitPayUserJournalEventContext.JournalEventUserContext,
                journalEventContext
            );
        }

        public void LogCardReaderAccountAdded(int clientVisitPayUserId, string cardReaderAccount, JournalEventHttpContextDto context)
        {
            JournalEventContext journalEventContext = this._userJournalEventService.Value.GetJournalEventContextForClient(clientVisitPayUserId, null);
            VisitPayUserJournalEventContext visitPayUserJournalEventContext = this.GetVisitPayUserJournalEventContext(clientVisitPayUserId, context);

            this.Journal.AddEvent<EventClientAddedCardReaderAccount, JournalEventParameters>(
                EventClientAddedCardReaderAccount.GetParameters
                (
                    eventUserName: visitPayUserJournalEventContext.VisitPayUser.UserName,
                    cardReaderAccount: cardReaderAccount
                ),
                visitPayUserJournalEventContext.JournalEventUserContext,
                journalEventContext
            );
        }

        public void LogClientViewManaged(int visitPayUserId, int managedVpGuarantorId, int managingVpGuarantorId, JournalEventHttpContextDto context)
        {
            JournalEventContext journalEventContext = this._userJournalEventService.Value.GetJournalEventContextForClient(visitPayUserId, managedVpGuarantorId);
            VisitPayUserJournalEventContext visitPayUserJournalEventContext = this.GetVisitPayUserJournalEventContext(visitPayUserId, context);

            Guarantor managedGuarantor = this._guarantorService.Value.GetGuarantor(managedVpGuarantorId);
            Guarantor managingGuarantor = this._guarantorService.Value.GetGuarantor(managingVpGuarantorId);

            this.Journal.AddEvent<EventClientViewManagedThruManaging, JournalEventParameters>(
                EventClientViewManagedThruManaging.GetParameters
                (
                    eventUserName: visitPayUserJournalEventContext.VisitPayUser.UserName,
                    managedVpGuarantorId: managedVpGuarantorId.ToString(),
                    managedVpUserName: managedGuarantor.User.UserName,
                    managingVpUserName: managingGuarantor.User.UserName
                ),
                visitPayUserJournalEventContext.JournalEventUserContext,
                journalEventContext
            );
        }

        public void LogClientAccountSearchHsGuarantors(int visitPayUserId, IList<int> hsGuarantorIds, JournalEventHttpContextDto context)
        {
            if (hsGuarantorIds.Any())
            {
                // TODO: TBD if this will be removed or implemented
            }
            throw new NotImplementedException("LogClientAccountSearchHsGuarantors is not implemented");
        }

        public void LogDeleteAttachmentSupportRequest(int clientVisitPayUserId, int createdSupportRequestvpGuarantorId, int createdSupportRequestVisitPayUserId, int supportRequestId, JournalEventHttpContextDto context)
        {
            JournalEventContext journalEventContext = this._userJournalEventService.Value.GetJournalEventContextForClient(clientVisitPayUserId, createdSupportRequestvpGuarantorId);
            VisitPayUserJournalEventContext visitPayUserJournalEventContext = this.GetVisitPayUserJournalEventContext(clientVisitPayUserId, context);

            VisitPayUser createdSupportRequestVisitPayUser = this.GetVisitPayUser(createdSupportRequestVisitPayUserId);
            Guarantor createdSupportRequestVGuarantorpGuarantor = this._guarantorService.Value.GetGuarantor(createdSupportRequestvpGuarantorId);

            this.Journal.AddEvent<EventClientRemovedAttachmentSupportRequest, JournalEventParameters>(
                EventClientRemovedAttachmentSupportRequest.GetParameters
                (
                    eventUserName: visitPayUserJournalEventContext.VisitPayUser.UserName,
                    supportRequestId: supportRequestId.ToString(),
                    vpUserName: createdSupportRequestVGuarantorpGuarantor.User.UserName
                ),
                visitPayUserJournalEventContext.JournalEventUserContext,
                journalEventContext
            );
        }

        public void LogDeleteAttachmentSupportTicket(int clientVisitPayUserId, int createdSupportTicketVisitPayUserId, int clientSupportRequestId, JournalEventHttpContextDto context)
        {
            JournalEventContext journalEventContext = this._userJournalEventService.Value.GetJournalEventContextForClient(clientVisitPayUserId, null);
            VisitPayUserJournalEventContext visitPayUserJournalEventContext = this.GetVisitPayUserJournalEventContext(clientVisitPayUserId, context);

            VisitPayUser createdSupportTicketVisitPayUser = this.GetVisitPayUser(createdSupportTicketVisitPayUserId);

            this.Journal.AddEvent<EventClientRemovedAttachmentSupportTicket, JournalEventParameters>(
                EventClientRemovedAttachmentSupportTicket.GetParameters
                (
                    eventUserName: visitPayUserJournalEventContext.VisitPayUser.UserName,
                    clientUserNameTicketCreator: createdSupportTicketVisitPayUser.UserName,
                    supportTicketId: clientSupportRequestId.ToString()
                ),
                visitPayUserJournalEventContext.JournalEventUserContext,
                journalEventContext
            );
        }

        public void LogVisitPayReportRun(int clientVisitPayUserId, JournalEventHttpContextDto context, string reportName)
        {
            JournalEventContext journalEventContext = this._userJournalEventService.Value.GetJournalEventContextForClient(clientVisitPayUserId, null);
            VisitPayUserJournalEventContext visitPayUserJournalEventContext = this.GetVisitPayUserJournalEventContext(clientVisitPayUserId, context);

            this.Journal.AddEvent<EventClientVisitPayReportViewed, JournalEventParameters>(
                EventClientVisitPayReportViewed.GetParameters
                (
                    eventUserName: visitPayUserJournalEventContext.VisitPayUser.UserName,
                    reportName: reportName
                ),
                visitPayUserJournalEventContext.JournalEventUserContext,
                journalEventContext
            );
        }

        public void LogGuestPayAuthenticationFailure(string attemptedGuarantorSourceSystemKey, string attemptedGuarantorLastName, JournalEventHttpContextDto context)
        {
            //this.LogUserEvent(UserEventTypeEnum.GpAuthenticationFailed, attemptedGuarantorLastName ?? "", attemptedGuarantorSourceSystemKey ?? "", ipAddress, userAgent);
        }

        public void LogGuestPayAuthenticationSuccess(int paymentUnitAuthenticationId, string paymentUnitSourceSystemKey, string guarantorLastName, JournalEventHttpContextDto context)
        {
            string userName = string.Concat(paymentUnitSourceSystemKey ?? "", "|", guarantorLastName ?? "");

            //this.LogUserEvent(UserEventTypeEnum.GpAuthenticationSuccess, userName, paymentUnitSourceSystemKey, ipAddress, userAgent, new Dictionary<string, string> {{"PaymentUnitAuthenticationId", paymentUnitAuthenticationId.ToString()}});
        }

        public void LogGuestPayPaymentMethodFailure(int paymentUnitAuthenticationId, string paymentUnitSourceSystemKey, string guarantorLastName, JournalEventHttpContextDto context, IReadOnlyDictionary<string, string> processorResponse)
        {
            Dictionary<string, string> additionalData = new Dictionary<string, string>(processorResponse.ToDictionary(kvp => kvp.Key, kvp => kvp.Value));
            additionalData.Add("PaymentAuthenticationId", paymentUnitAuthenticationId.ToString());
            string userName = string.Concat(paymentUnitSourceSystemKey ?? "", "|", guarantorLastName ?? "");
            //this.LogUserEvent(UserEventTypeEnum.GpPaymentMethodVerifyFailure, userName, paymentUnitSourceSystemKey, ipAddress, userAgent, additionalData);
        }

        public void LogGuestPayReceiptDownload(int paymentUnitAuthenticationId, string paymentUnitSourceSystemKey, string guarantorLastName, JournalEventHttpContextDto context)
        {
            string userName = string.Concat(paymentUnitSourceSystemKey ?? "", "|", guarantorLastName ?? "");

            //this.LogUserEvent(UserEventTypeEnum.GpReceiptDownload, userName, paymentUnitSourceSystemKey, ipAddress, userAgent, new Dictionary<string, string> {{"PaymentUnitAuthenticationId", paymentUnitAuthenticationId.ToString()}});
        }

        public void LogGuestPaySessionTimeout(int paymentUnitAuthenticationId, string paymentUniSourceSystemKey, JournalEventHttpContextDto context)
        {
            //this.LogUserEvent(UserEventTypeEnum.GpSessionTimeout, null, paymentUniSourceSystemKey, ipAddress, userAgent, new Dictionary<string, string> { { "PaymentUnitAuthenticationId", paymentUnitAuthenticationId.ToString() } });
        }

        public void LogGuarantorSelfVerification(int visitPayUserId, int vpGuarantorId, JournalEventHttpContextDto context)
        {
            VisitPayUserJournalEventContext visitPayUserJournalEventContext = this.GetVisitPayUserJournalEventContext(visitPayUserId, context);
            this._userJournalEventService.Value.LogGuarantorSelfVerification(visitPayUserJournalEventContext.JournalEventUserContext, vpGuarantorId);
        }

        #endregion

        #region SSO Events

        private class SsoInformation
        {
            public string GuarantorUserName { get; set; }
            public int GuarantorVisitPayUserId { get; set; }
            public string DateOfBirth { get; set; }
            public string SsoSystemSourceKey { get; set; }
            public string SsoProviderId { get; set; }
            public string SsoProviderName { get; set; }
        }

        private SsoInformation GetSsoInformation(SsoProviderEnum ssoProvider, string ssoSsk, DateTime? dob, int? vpGuarantorId)
        {
            SsoInformation ssoInformation = new SsoInformation()
            {
                SsoProviderId = ((int)ssoProvider).ToString(),
                SsoProviderName = ssoProvider.ToString(),
                SsoSystemSourceKey = ssoSsk ?? "",
                DateOfBirth = dob.HasValue ? dob.Value.ToShortDateString() : ""
            };
            if (vpGuarantorId != null)
            {
                Guarantor guarantor = this._guarantorService.Value.GetGuarantor((int)vpGuarantorId);
                ssoInformation.GuarantorUserName = guarantor.User.UserName;
                ssoInformation.GuarantorVisitPayUserId = guarantor.User.VisitPayUserId;
            }

            return ssoInformation;
        }

        public void LogSsoDecline(int? actionTakenByVisitPayUserId, int vpGuarantorId, SsoProviderEnum ssoProvider, JournalEventHttpContextDto context, string ssoSsk, DateTime? dob)
        {
            SsoInformation sso = this.GetSsoInformation(ssoProvider, ssoSsk, dob, vpGuarantorId);
            JournalEventContext journalEventContext = this.GetJournalEventContextForGuarantor(sso.GuarantorVisitPayUserId);
            VisitPayUserJournalEventContext visitPayUserJournalEventContext = this.GetVisitPayUserJournalEventContext(actionTakenByVisitPayUserId, context);

            this.Journal.AddEvent<EventGuarantorSsoDeclined, JournalEventParameters>(
                EventGuarantorSsoDeclined.GetParameters
                (
                    dateOfBirth: sso.DateOfBirth,
                    ssoProviderId: sso.SsoProviderId,
                    ssoProviderName: sso.SsoProviderName,
                    ssoSystemSourceKey: sso.SsoSystemSourceKey,
                    vpUserName: sso.GuarantorUserName
                ),
                visitPayUserJournalEventContext.JournalEventUserContext,
                journalEventContext
            );
        }

        public void LogSsoSuccessfulLogin(int? actionTakenByVisitPayUserId, int vpGuarantorId, SsoProviderEnum ssoProvider, string ssoSsk, DateTime? dob, JournalEventHttpContextDto context)
        {
            SsoInformation sso = this.GetSsoInformation(ssoProvider, ssoSsk, dob, vpGuarantorId);
            JournalEventContext journalEventContext = this.GetJournalEventContextForGuarantor(sso.GuarantorVisitPayUserId);
            VisitPayUserJournalEventContext visitPayUserJournalEventContext = this.GetVisitPayUserJournalEventContext(actionTakenByVisitPayUserId, context);

            this.Journal.AddEvent<EventGuarantorSuccessfulLoginSso, JournalEventParameters>(
                EventGuarantorSuccessfulLoginSso.GetParameters
                (
                    applicationID: this.ApplicationSettingsService.Value.Application.Value.ToString(),
                    deviceType: visitPayUserJournalEventContext.JournalEventUserContext.DeviceType.ToString(),
                    ssoProviderId: sso.SsoProviderId,
                    ssoProviderName: sso.SsoProviderName,
                    uRL: visitPayUserJournalEventContext.JournalEventUserContext.Url,
                    vpUserName: sso.GuarantorUserName
                ),
                visitPayUserJournalEventContext.JournalEventUserContext,
                journalEventContext
            );
        }

        public void LogSsoVerificationFailed(int? actionTakenByVisitPayUserId, int vpGuarantorId, SsoProviderEnum ssoProvider, string ssoSsk, DateTime? dob, JournalEventHttpContextDto context)
        {
            SsoInformation sso = this.GetSsoInformation(ssoProvider, ssoSsk, dob, vpGuarantorId);
            JournalEventContext journalEventContext = this.GetJournalEventContextForGuarantor(sso.GuarantorVisitPayUserId);
            VisitPayUserJournalEventContext visitPayUserJournalEventContext = this.GetVisitPayUserJournalEventContext(actionTakenByVisitPayUserId, context);

            this.Journal.AddEvent<EventGuarantorSsoVerificationFailed, JournalEventParameters>(
                EventGuarantorSsoVerificationFailed.GetParameters
                (
                    dateOfBirth: sso.DateOfBirth,
                    ssoProviderId: sso.SsoProviderId,
                    ssoProviderName: sso.SsoProviderName,
                    ssoSystemSourceKey: sso.SsoSystemSourceKey,
                    vpUserName: sso.GuarantorUserName
                ),
                visitPayUserJournalEventContext.JournalEventUserContext,
                journalEventContext
            );
        }

        public void LogSsoAttempt(int? actionTakenByVisitPayUserId, int vpGuarantorId, SsoProviderEnum ssoProvider, JournalEventHttpContextDto context)
        {
            SsoInformation sso = this.GetSsoInformation(ssoProvider, null, null, vpGuarantorId);
            JournalEventContext journalEventContext = this.GetJournalEventContextForGuarantor(sso.GuarantorVisitPayUserId);
            VisitPayUserJournalEventContext visitPayUserJournalEventContext = this.GetVisitPayUserJournalEventContext(actionTakenByVisitPayUserId, context);

            this.Journal.AddEvent<EventGuarantorSsoAttempted, JournalEventParameters>(
                EventGuarantorSsoAttempted.GetParameters
                (
                    ssoProviderId: sso.SsoProviderId,
                    ssoProviderName: sso.SsoProviderName,
                    vpUserName: sso.GuarantorUserName
                ),
                visitPayUserJournalEventContext.JournalEventUserContext,
                journalEventContext
            );
        }

        public void LogSsoDisabled(int? actionTakenByVisitPayUserId, int vpGuarantorId, SsoProviderEnum ssoProvider, JournalEventHttpContextDto context, string ssoSsk, DateTime? dob)
        {
            SsoInformation sso = this.GetSsoInformation(ssoProvider, ssoSsk, dob, vpGuarantorId);
            JournalEventContext journalEventContext = this._userJournalEventService.Value.GetJournalEventContextForSystem(actionTakenByVisitPayUserId ?? SystemUsers.SystemUserId, vpGuarantorId);
            VisitPayUserJournalEventContext visitPayUserJournalEventContext = this.GetVisitPayUserJournalEventContext(actionTakenByVisitPayUserId, context);

            this.Journal.AddEvent<EventGuarantorSsoDisabled, JournalEventParameters>(
                EventGuarantorSsoDisabled.GetParameters
                (
                    ssoProviderId: sso.SsoProviderId,
                    ssoProviderName: sso.SsoProviderName,
                    vpUserName: sso.GuarantorUserName
                ),
                visitPayUserJournalEventContext.JournalEventUserContext,
                journalEventContext
            );
        }

        public void LogSsoEnabled(int? actionTakenByVisitPayUserId, int vpGuarantorId, SsoProviderEnum ssoProvider, JournalEventHttpContextDto context, string ssoSsk, DateTime? dob)
        {
            SsoInformation sso = this.GetSsoInformation(ssoProvider, ssoSsk, dob, vpGuarantorId);
            JournalEventContext journalEventContext = this._userJournalEventService.Value.GetJournalEventContextForSystem(actionTakenByVisitPayUserId ?? SystemUsers.SystemUserId, vpGuarantorId);
            VisitPayUserJournalEventContext visitPayUserJournalEventContext = this.GetVisitPayUserJournalEventContext(actionTakenByVisitPayUserId, context);

            this.Journal.AddEvent<EventSystemActionSsoEnabled, JournalEventParameters>(
                EventSystemActionSsoEnabled.GetParameters
                (
                    dateOfBirth: sso.DateOfBirth,
                    ssoProviderId: sso.SsoProviderId,
                    ssoProviderName: sso.SsoProviderName,
                    ssoSystemSourceKey: sso.SsoSystemSourceKey,
                    vpUserName: sso.GuarantorUserName
                ),
                visitPayUserJournalEventContext.JournalEventUserContext,
                journalEventContext
            );
        }

        public void LogSsoRequestFailed(int? actionTakenByVisitPayUserId, SsoProviderEnum ssoProvider, string ssoSsk, DateTime? dob, JournalEventHttpContextDto context)
        {
            SsoInformation sso = this.GetSsoInformation(ssoProvider, ssoSsk, dob, null);
            JournalEventContext journalEventContext = this._userJournalEventService.Value.GetJournalEventContextForSystem(actionTakenByVisitPayUserId ?? SystemUsers.SystemUserId, null);
            VisitPayUserJournalEventContext visitPayUserJournalEventContext = this.GetVisitPayUserJournalEventContext(actionTakenByVisitPayUserId, context);

            this.Journal.AddEvent<EventSystemActionSsoRequestFailed, JournalEventParameters>(
                EventSystemActionSsoRequestFailed.GetParameters
                (
                    dateOfBirth: sso.DateOfBirth,
                    ssoProviderId: sso.SsoProviderId,
                    ssoProviderName: sso.SsoProviderName,
                    ssoSystemSourceKey: sso.SsoSystemSourceKey
                ),
                visitPayUserJournalEventContext.JournalEventUserContext,
                journalEventContext
            );
        }

        public void LogSsoMatchedNotRegistered(SsoProviderEnum ssoProvider, IList<int> matchedNotRegisteredHsGuarantors, string ssoSsk, DateTime? dob, JournalEventHttpContextDto context)
        {
            SsoInformation sso = this.GetSsoInformation(ssoProvider, ssoSsk, dob, null);
            JournalEventContext journalEventContext = this._userJournalEventService.Value.GetJournalEventContextForSystem(SystemUsers.SystemUserId, null);
            VisitPayUserJournalEventContext visitPayUserJournalEventContext = this.GetVisitPayUserJournalEventContext(SystemUsers.SystemUserId, context);

            this.Journal.AddEvent<EventSystemActionSsoMatchedNotRegistered, JournalEventParameters>(
                EventSystemActionSsoMatchedNotRegistered.GetParameters
                (
                    dateOfBirth: sso.DateOfBirth,
                    hSGuarantorMatches: string.Join(",", matchedNotRegisteredHsGuarantors.Select(x => string.Format("{0}", x))),
                    ssoProviderId: sso.SsoProviderId,
                    ssoProviderName: sso.SsoProviderName,
                    ssoSystemSourceKey: sso.SsoSystemSourceKey
                ),
                visitPayUserJournalEventContext.JournalEventUserContext,
                journalEventContext
            );
        }

        public void LogSsoInvalidate(int? actionTakenByVisitPayUserId, int vpGuarantorId, SsoProviderEnum ssoProvider, JournalEventHttpContextDto context)
        {
            SsoInformation sso = this.GetSsoInformation(ssoProvider, null, null, vpGuarantorId);
            JournalEventContext journalEventContext = this._userJournalEventService.Value.GetJournalEventContextForSystem(actionTakenByVisitPayUserId ?? SystemUsers.SystemUserId, null);
            VisitPayUserJournalEventContext visitPayUserJournalEventContext = this.GetVisitPayUserJournalEventContext(actionTakenByVisitPayUserId ?? SystemUsers.SystemUserId, context);

            this.Journal.AddEvent<EventSystemActionSsoInvalidated, JournalEventParameters>(
                EventSystemActionSsoInvalidated.GetParameters
                (
                    ssoProviderId: sso.SsoProviderId,
                    ssoProviderName: sso.SsoProviderName
                ),
                visitPayUserJournalEventContext.JournalEventUserContext,
                journalEventContext
            );
        }

        public void LogRegistrationCompleteSso(SsoProviderEnum ssoProviderEnum, int visitPayUserId, int vpGuarantorId, string hsGuarantorId, string lastName, DateTime dateOfBirth, string ssn4, string zip, JournalEventHttpContextDto context)
        {
            SsoInformation sso = this.GetSsoInformation(ssoProviderEnum, null, dateOfBirth, vpGuarantorId);
            JournalEventContext journalEventContext = this._userJournalEventService.Value.GetJournalEventContextForSystem(visitPayUserId, vpGuarantorId);
            VisitPayUserJournalEventContext visitPayUserJournalEventContext = this.GetVisitPayUserJournalEventContext(SystemUsers.SystemUserId, context);

            this.Journal.AddEvent<EventSystemActionRegistrationCompleteSso, JournalEventParameters>(
                EventSystemActionRegistrationCompleteSso.GetParameters
                (
                    dateOfBirth: sso.DateOfBirth,
                    hsGuarantorId: hsGuarantorId,
                    lastName: lastName,
                    sSN4: ssn4,
                    ssoProviderId: sso.SsoProviderId,
                    ssoProviderName: sso.SsoProviderName,
                    vpGuarantorId: vpGuarantorId.ToString(),
                    vpUserName: sso.GuarantorUserName,
                    zip: zip
                ),
                visitPayUserJournalEventContext.JournalEventUserContext,
                journalEventContext
            );
        }

        #endregion

        #region Helpers

        private VisitPayUser GetVisitPayUser(int? visitPayUserId)
        {
            VisitPayUser visitPayUser = null;
            if (visitPayUserId.HasValue)
            {
                visitPayUser = this._userService.Value.FindById(visitPayUserId.ToString());
            }

            return visitPayUser;
        }

        private void LogAction(Action<JournalEventUserContext> logAction, int? visitPayUserId, JournalEventHttpContextDto context)
        {
            VisitPayUser visitPayUser = this.GetVisitPayUser(visitPayUserId);
            JournalEventUserContext journalEventUserContext = this._userJournalEventService.Value.GetJournalEventUserContext(context, visitPayUser);

            logAction(journalEventUserContext);
        }

        private VisitPayUserJournalEventContext GetVisitPayUserJournalEventContext(int? visitPayUserId, JournalEventHttpContextDto context)
        {
            VisitPayUserJournalHttpContext parameters = this.GetJournalEventParameters(visitPayUserId, context);
            JournalEventUserContext journalEventUserContext = this._userJournalEventService.Value.GetJournalEventUserContext(context, parameters.VisitPayUser);
            return new VisitPayUserJournalEventContext() { JournalEventUserContext = journalEventUserContext, VisitPayUser = parameters.VisitPayUser };
        }

        private VisitPayUserJournalHttpContext GetJournalEventParameters(int? visitPayUserId, JournalEventHttpContextDto context)
        {
            VisitPayUser visitPayUser = this.GetVisitPayUser(visitPayUserId);
            JournalEventHttpContextDto journalEventHttpContextDto = context;
            return new VisitPayUserJournalHttpContext() { JournalEventHttpContextDto = journalEventHttpContextDto, VisitPayUser = visitPayUser };
        }

        private JournalEventContext GetJournalEventContextForGuarantor(int visitPayUserId)
        {
            int vpGuarantorId = this._guarantorService.Value.GetGuarantorId(visitPayUserId);
            return new JournalEventContext() { VpGuarantorId = vpGuarantorId, VisitPayUserId = visitPayUserId };
        }

        public int GetGuarantorIdFromVisitPayUserId(int visitPayUserId)
        {
            return this._guarantorService.Value.GetGuarantorId(visitPayUserId);
        }

        public JournalEventContext GetJournalEventContextForGuarantor(VisitPayUser visitPayUser)
        {
            return this.GetJournalEventContextForGuarantor(visitPayUser.VisitPayUserId);
        }

        #endregion

        #region Internal Classes

        private class VisitPayUserJournalEventContext
        {
            public VisitPayUser VisitPayUser { get; set; }
            public JournalEventUserContext JournalEventUserContext { get; set; }
        }

        private class VisitPayUserJournalHttpContext
        {
            public VisitPayUser VisitPayUser { get; set; }
            public JournalEventHttpContextDto JournalEventHttpContextDto { get; set; }
        }

        #endregion

    }
}

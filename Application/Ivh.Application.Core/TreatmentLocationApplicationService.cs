﻿namespace Ivh.Application.Core
{
    using System.Collections.Generic;
    using AutoMapper;
    using Common.Dtos;
    using Common.Interfaces;
    using Domain.Core.TreatmentLocation.Interfaces;

    public class TreatmentLocationApplicationService : ITreatmentLocationApplicationService
    {
        private readonly ITreatmentLocationService _treatmentLocationService;

        public TreatmentLocationApplicationService(ITreatmentLocationService treatmentLocationService)
        {
            this._treatmentLocationService = treatmentLocationService;
        }

        public IList<TreatmentLocationDto> GetTreatmentLocations()
        {
            return Mapper.Map<List<TreatmentLocationDto>>(this._treatmentLocationService.GetTreatmentLocations());
        }
    }
}

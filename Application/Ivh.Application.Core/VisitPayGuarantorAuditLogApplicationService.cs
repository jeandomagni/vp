﻿namespace Ivh.Application.Core
{
    using System.Collections.Generic;
    using Common.Dtos;
    using Common.Interfaces;
    using Domain.User.Interfaces;
    using AutoMapper;

    public class VisitPayGuarantorAuditLogApplicationService : IVisitPayGuarantorAuditLogApplicatonService
    {
        private readonly IVisitPayGuarantorAuditLogService _visitPayGuarantorAuditLogService;

        public VisitPayGuarantorAuditLogApplicationService(IVisitPayGuarantorAuditLogService visitPayGuarantorAuditLogService)
        {
            this._visitPayGuarantorAuditLogService = visitPayGuarantorAuditLogService;
        }

        public IReadOnlyList<VisitPayGuarantorAuditLogDto> GetVisitPayGuarantorAuditLog(int vpGuarantorId)
        {
            return Mapper.Map<IReadOnlyList<VisitPayGuarantorAuditLogDto>>(this._visitPayGuarantorAuditLogService.GetVisitPayGuarantorAuditLog(vpGuarantorId));
        }
    }
}

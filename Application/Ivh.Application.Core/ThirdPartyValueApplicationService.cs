﻿namespace Ivh.Application.Core
{
    using System;
    using Ivh.Application.Core.Common.Interfaces;
    using Ivh.Common.EventJournal;
    using Ivh.Domain.Core.ThirdPartyKey.Interfaces;

    public class ThirdPartyValueApplicationService : IThirdPartyValueApplicationService
    {
        private readonly IThirdPartyValueService _thirdPartyValueService;
        private Lazy<IVisitPayUserJournalEventApplicationService> _journalEventApplicationService;
        public ThirdPartyValueApplicationService(IThirdPartyValueService thirdPartyValueService, Lazy<IVisitPayUserJournalEventApplicationService> journalEventApplicationService)
        {
            this._thirdPartyValueService = thirdPartyValueService;
            this._journalEventApplicationService = journalEventApplicationService;
        }

        public bool CardDeviceKeyNeedsToBeSet(int visitPayUserId, bool isFeatureEnabled, bool userHasRole)
        {
            return _thirdPartyValueService.CardDeviceKeyNeedsToBeSet(visitPayUserId, isFeatureEnabled, userHasRole);
        }

        public string GetCardReaderDeviceValue(int visitPayUserId)
        {
            return _thirdPartyValueService.GetCardReaderDeviceValue(visitPayUserId);
        }

        public string GetCardReaderUserAccountValue(int visitPayUserId)
        {
            return _thirdPartyValueService.GetCardReaderUserAccountValue(visitPayUserId);
        }

        public void SetCardReaderDeviceKey(string value, int visitPayUserId, JournalEventHttpContextDto context)
        {
            _thirdPartyValueService.SetCardReaderDeviceKey(value, visitPayUserId);
            this._journalEventApplicationService.Value.LogCardReaderKeyAdded(visitPayUserId, value, context);
        }

        public void SetCardReaderUserAccount(string value, int visitPayUserId, JournalEventHttpContextDto context)
        {
            _thirdPartyValueService.SetCardReaderUserAccount(value, visitPayUserId);
            this._journalEventApplicationService.Value.LogCardReaderAccountAdded(visitPayUserId, value, context);

        }
    }
}

﻿namespace Ivh.Application.Core
{
    using System;
    using System.Threading.Tasks;
    using System.Web;
    using Common.Interfaces;
    using Domain.User.Interfaces;
    using Ivh.Common.Cache;
    using Ivh.Common.ServiceBus.Attributes;
    using Ivh.Common.ServiceBus.Enums;
    using Ivh.Common.VisitPay.Messages.Core;
    using Ivh.Common.Web.Extensions;
    using MassTransit;
    using Microsoft.Owin;

    public class VisitPayUserApplicationLightweightService : IVisitPayUserApplicationLightweightService
    {
        private readonly Lazy<IDistributedCache> _cache;
        private readonly Lazy<IVisitPayUserDetailChangedService> _visitPayUserDetailChangedService;

        public VisitPayUserApplicationLightweightService(
            Lazy<IDistributedCache> cache,
            Lazy<IVisitPayUserDetailChangedService> visitPayUserDetailChangedService)
        {
            this._cache = cache;
            this._visitPayUserDetailChangedService = visitPayUserDetailChangedService;
        }

        /// <summary>
        /// This method is to force the logout of a 
        /// </summary>
        /// <param name="visitPayUserId"></param>
        public void SignOutVisitPayUserId(int visitPayUserId)
        {
            string key = $"SignOutVisitPayUserId:{visitPayUserId}";
            try
            {
                if (this._cache.Value.GetLock(key))
                {
                    Task.Run(async () => await this._cache.Value.SetObjectAsync(key, visitPayUserId, 30 * 60));
                }
            }
            finally
            {
                this._cache.Value.ReleaseLock(key);
            }
        }

        public void CheckIfVisitPayUserShouldSignOut(IOwinContext owinContext)
        {
            if (HttpContext.Current.User.Identity.IsAuthenticated)
            {
                string key = $"SignOutVisitPayUserId:{HttpContext.Current.User.CurrentUserId()}";
                int value = Task.Run(async () => await this._cache.Value.GetObjectAsync<int>(key)).Result;
                if (value == HttpContext.Current.User.CurrentUserId())
                {
                    if (Task.Run(async () => await this._cache.Value.RemoveAsync(key)).Result)
                    {
                        owinContext.Response.Redirect("/Account/LogOff");
                    }
                }
            }
        }

        public void VisitPayUserAddressChanged(int visitPayUserId)
        {
            this._visitPayUserDetailChangedService.Value.VisitPayUserAddressChanged(visitPayUserId);
        }

        #region Message Consumers

        [UniversalConsumer(UniversalQueuesEnum.VisitPayProcessor, UniversalPriorityEnum.Priority30)]
        public void ConsumeMessage(VisitPayUserAddressChangedMessage message, ConsumeContext<VisitPayUserAddressChangedMessage> consumeContext)
        {
            this.VisitPayUserAddressChanged(message.VisitPayUserId);
        }

        public bool IsMessageValid(VisitPayUserAddressChangedMessage message, ConsumeContext<VisitPayUserAddressChangedMessage> consumeContext)
        {
            bool isValid = true;
            return isValid;
        }

        #endregion
    }
}
﻿namespace Ivh.Application.Core
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Common.Interfaces;
    using System.Threading.Tasks;
    using Base.Common.Interfaces;
    using Base.Services;
    using Domain.Core.SystemException.Interfaces;
    using Domain.Guarantor.Interfaces;
    using Domain.HospitalData.HsGuarantor.Entities;
    using Domain.HospitalData.HsGuarantor.Interfaces;
    using HospitalData.Common.Interfaces;
    using Ivh.Common.Base.Utilities.Helpers;
    using Ivh.Common.Data;
    using Ivh.Common.Data.Connection.Connections;
    using Ivh.Common.Data.Interfaces;
    using Ivh.Common.ServiceBus.Attributes;
    using Ivh.Common.ServiceBus.Enums;
    using MassTransit;
    using NHibernate;
    using SystemException = Domain.Core.SystemException.Entities.SystemException;
    using Ivh.Common.VisitPay.Enums;
    using Ivh.Common.VisitPay.Messages.Core;
    using Ivh.Common.VisitPay.Messages.HospitalData.Dtos;
    using Ivh.Common.VisitPay.Messages.Matching;
    using Ivh.Common.VisitPay.Messages.Scoring;

    public class GuarantorEnrollmentApplicationService : ApplicationService, IGuarantorEnrollmentApplicationService
    {
        static readonly int[] RetryTimes = { 1, 1, 1, 60, 360 };

        private readonly Lazy<IVpGuarantorHsMatchService> _hsGuarantorService;
        private readonly Lazy<IGuarantorEnrollmentService> _enrollmentService;
        private readonly Lazy<IChangeEventApplicationService> _changeEventApplicationService;
        private readonly Lazy<ISystemExceptionService> _systemExceptionService;
        private readonly ISession _session;

        public GuarantorEnrollmentApplicationService(
            Lazy<IApplicationServiceCommonService> applicationServiceCommonService,
            Lazy<IVpGuarantorHsMatchService> hsGuarantorService,
            Lazy<IGuarantorEnrollmentService> enrollmentService,
            Lazy<IChangeEventApplicationService> changeEventApplicationService,
            Lazy<ISystemExceptionService> systemExceptionService,
            ISessionContext<VisitPay> session) : base(applicationServiceCommonService)
        {
            this._enrollmentService = enrollmentService;
            this._hsGuarantorService = hsGuarantorService;
            this._changeEventApplicationService = changeEventApplicationService;
            this._systemExceptionService = systemExceptionService;
            this._session = session.Session;
        }

        public async Task UnEnrollGuarantorAsync(int vpGuarantorId)
        {
            foreach (HsGuarantor guarantor in this._hsGuarantorService.Value.GetGuarantorsByVpGuarantorId(vpGuarantorId))
            {
                await this._enrollmentService.Value.UnEnrollGuarantorAsync(guarantor.SourceSystemKey);
            }
        }

        public async Task PublishEnrollGuarantorMessageAsync(int vpGuarantorId)
        {
            await this._enrollmentService.Value.PublishEnrollGuarantorMessageAsync(vpGuarantorId);
        }

        public async Task PublishUnEnrollGuarantorMessageAsync(int vpGuarantorId)
        {
            await this._enrollmentService.Value.PublishUnEnrollGuarantorMessageAsync(vpGuarantorId);
        }

        public async Task EnrollGuarantorAsync(int vpGuarantorId)
        {
            List<EnrollmentResponseDto> responseList = new List<EnrollmentResponseDto>();
            IList<HsGuarantor> hsGuarantors = this._hsGuarantorService.Value.GetGuarantorsByVpGuarantorId(vpGuarantorId);
            foreach (HsGuarantor guarantor in hsGuarantors)
            {
                responseList.Add(await this._enrollmentService.Value.EnrollGuarantorAsync(guarantor.SourceSystemKey, guarantor.HsBillingSystemId, vpGuarantorId));
            }

            if (hsGuarantors.Count > 0)
            {
                Dictionary<string, List<int>> hsGuarantorDictionary = hsGuarantors
                    .GroupBy(x => x.SourceSystemKey)
                    .ToDictionary(k => k.Key, v => v.Select(f => f.HsBillingSystemId).ToList());

                if (hsGuarantorDictionary.Any())
                {
                    VpGuarantorScoreMessage message = new VpGuarantorScoreMessage
                    {
                        VpGuarantorId = vpGuarantorId,
                        HsGuarantors = hsGuarantorDictionary
                    };
                    this.Bus.Value.PublishMessage(message).Wait();
                }
            }

            // PublishChangeEvent: The RealTimeVisitEnrollmentService (that is currently only called by IntermountainGuarantorEnrollmentProvider) publishes the changeEvents
            // after updates to the base.Visit table are made (no race condition!) so we don't need to do it again here
            if (responseList.Any(x => x != null && x.EnrollmentStatus && x.PublishChangeEvent))
            {
                List<EnrollmentResponseDto> publishList = responseList.Where(x => x != null && x.EnrollmentStatus && x.PublishChangeEvent).ToList();
                this._session.Transaction.RegisterPostCommitSuccessAction((vpgid, rl) =>
                {
                    GenerateChangeEventForVpGuarantorMessage generateChangeEventForVpGuarantorMessage = new GenerateChangeEventForVpGuarantorMessage()
                    {
                        VpGuarantorId = vpgid, 
                        EnrollmentResponses = rl
                    };
                    this.Bus.Value.PublishMessage(generateChangeEventForVpGuarantorMessage);
                }, vpGuarantorId, publishList);
            }
        }

        #region Message Consumers

        [UniversalConsumer(UniversalQueuesEnum.VisitPayProcessor, UniversalPriorityEnum.Priority30)]
        public void ConsumeMessage(EnrollGuarantorMessage message, ConsumeContext<EnrollGuarantorMessage> consumeContext)
        {
            bool exceptionWasThrownAndShouldRetry = false;
            try
            {
                Task.Run(() => this.EnrollGuarantorAsync(message.VpGuarantorId)).Wait();
            }
            catch (Exception e)
            {
                if (message != null)
                {
                    this.LoggingService.Value.Fatal(() => $"EnrollGuarantorMessageConsumer::Consume<ConsumeContext<EnrollGuarantorMessage>> - failed for VpGuarantorId = {message.VpGuarantorId}, exception = {ExceptionHelper.AggregateExceptionToString(e)}, retryNum = {message.RetryCount}");
                }
                if (message.RetryCount < 6)
                {
                    message.RetryCount++;
                    exceptionWasThrownAndShouldRetry = true;
                }
                else
                {
                    using (UnitOfWork uow = new UnitOfWork(this._session))
                    {
                        this._systemExceptionService.Value.LogSystemException(new SystemException
                        {
                            VpGuarantorId = message.VpGuarantorId,
                            InsertDate = DateTime.UtcNow,
                            SystemExceptionDescription = "EnrollGuarantorMessageConsumer::Consume<ConsumeContext<EnrollGuarantorMessage>> - Not trying again. failed",
                            SystemExceptionType = SystemExceptionTypeEnum.EnrollmentException
                        });
                        uow.Commit();
                    }

                    if (message != null)
                    {
                        this.LoggingService.Value.Fatal(() => $"EnrollGuarantorMessageConsumer::Consume<ConsumeContext<EnrollGuarantorMessage>> - Not trying again. failed for VpGuarantorId = {message.VpGuarantorId}, exception = {ExceptionHelper.AggregateExceptionToString(e)}");
                    }
                }
            }

            if (exceptionWasThrownAndShouldRetry)
            {
                int minutesToWait = (message.RetryCount < RetryTimes.Length) ? RetryTimes[message.RetryCount] : 1;
                Task.Run(async () => await consumeContext.Redeliver(TimeSpan.FromMinutes(minutesToWait)));
            }
        }

        public bool IsMessageValid(EnrollGuarantorMessage message, ConsumeContext<EnrollGuarantorMessage> consumeContext)
        {
            bool isValid = true;
            return isValid;
        }

        [UniversalConsumer(UniversalQueuesEnum.VisitPayProcessor, UniversalPriorityEnum.Priority30)]
        public void ConsumeMessage(UnEnrollGuarantorMessage message, ConsumeContext<UnEnrollGuarantorMessage> consumeContext)
        {
            bool exceptionWasThrownAndShouldRetry = false;
            try
            {
                Task.Run(() => this.UnEnrollGuarantorAsync(message.VpGuarantorId)).Wait();
            }
            catch (Exception e)
            {
                if (message != null)
                {
                    this.LoggingService.Value.Fatal(() => $"EnrollGuarantorMessageConsumer::Consume<ConsumeContext<UnEnrollGuarantorMessage>> - failed for VpGuarantorId = {message.VpGuarantorId}, exception = {ExceptionHelper.AggregateExceptionToString(e)}, retryNum = {message.RetryCount}");
                }
                if (message.RetryCount < 6)
                {
                    message.RetryCount++;
                    exceptionWasThrownAndShouldRetry = true;
                }
                else
                {
                    using (UnitOfWork uow = new UnitOfWork(this._session))
                    {
                        this._systemExceptionService.Value.LogSystemException(new SystemException
                        {
                            VpGuarantorId = message.VpGuarantorId,
                            InsertDate = DateTime.UtcNow,
                            SystemExceptionDescription = "EnrollGuarantorMessageConsumer::Consume<ConsumeContext<UnEnrollGuarantorMessage>> - Not trying again. failed",
                            SystemExceptionType = SystemExceptionTypeEnum.EnrollmentException
                        });
                        uow.Commit();
                    }

                    if (message != null)
                    {
                        this.LoggingService.Value.Fatal(() => $"EnrollGuarantorMessageConsumer::Consume<ConsumeContext<UnEnrollGuarantorMessage>> - Not trying again. failed for VpGuarantorId = {message.VpGuarantorId}, exception = {ExceptionHelper.AggregateExceptionToString(e)}");
                    }
                }
            }

            if (exceptionWasThrownAndShouldRetry)
            {
                int minutesToWait = message.RetryCount < RetryTimes.Length ? RetryTimes[message.RetryCount] : 1;
                Task.Run(async () => await consumeContext.Redeliver(TimeSpan.FromMinutes(minutesToWait)));
            }
        }

        public bool IsMessageValid(UnEnrollGuarantorMessage message, ConsumeContext<UnEnrollGuarantorMessage> consumeContext)
        {
            bool isValid = true;
            return isValid;
        }

        #endregion
    }
}

﻿namespace Ivh.Application.Core
{
    using System;
    using System.Collections.Generic;
    using Common.Dtos;
    using Common.Interfaces;
    using AutoMapper;
    using Base.Common.Interfaces;
    using Base.Services;
    using Domain.Guarantor.Interfaces;

    public class BillingSystemApplicationService : ApplicationService, IBillingSystemApplicationService
    {
        private readonly IBillingSystemService _billingSystemService;

        public BillingSystemApplicationService(
            Lazy<IApplicationServiceCommonService> applicationServiceCommonService,
            IBillingSystemService billingSystemService) : base(applicationServiceCommonService)
        {
            this._billingSystemService = billingSystemService;
        }

        public IList<BillingSystemDto> GetAllBillingSystemTypes(bool includeUnmatched = false)
        {
            return Mapper.Map<List<BillingSystemDto>>(this._billingSystemService.GetAllBillingSystemTypes(includeUnmatched));
        }
    }
}

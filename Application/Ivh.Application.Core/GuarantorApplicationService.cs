﻿namespace Ivh.Application.Core
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading;
    using System.Threading.Tasks;
    using System.Web;
    using AutoMapper;
    using Base.Common.Interfaces;
    using Base.Services;
    using Common.Dtos;
    using Common.Interfaces;
    using Domain.Guarantor.Entities;
    using Domain.Guarantor.Interfaces;
    using Domain.Core.Sso.Entities;
    using Domain.Core.Sso.Interfaces;
    using Domain.Core.SystemException.Interfaces;
    using Domain.User.Entities;
    using Domain.User.Interfaces;
    using Domain.User.Services;
    using Domain.FinanceManagement.Consolidation.Interfaces;
    using Domain.FinanceManagement.FinancePlan.Entities;
    using Domain.FinanceManagement.FinancePlan.Interfaces;
    using Domain.FinanceManagement.Interfaces;
    using Domain.FinanceManagement.Statement.Entities;
    using Domain.FinanceManagement.Statement.Interfaces;
    using Domain.FinanceManagement.ValueTypes;
    using Domain.SecureCommunication.GuarantorSupportRequests.Entities;
    using Domain.SecureCommunication.GuarantorSupportRequests.Interfaces;
    using FinanceManagement.Common.Dtos;
    using HospitalData.Common.Responses;
    using Domain.Logging.Interfaces;
    using HospitalData.Common.Interfaces;
    using Ivh.Common.Base.Utilities.Extensions;
    using Ivh.Common.Data;
    using Microsoft.AspNet.Identity;
    using NHibernate;
    using IHsGuarantorService = Domain.HospitalData.VpGuarantor.Interfaces.IHsGuarantorService;
    using Ivh.Application.Base.Common.Dtos;
    using Ivh.Common.Base.Utilities.Helpers;
    using Ivh.Common.Data.Connection.Connections;
    using Ivh.Common.Data.Interfaces;
    using Ivh.Common.EventJournal;
    using Ivh.Common.ServiceBus.Attributes;
    using Ivh.Common.ServiceBus.Enums;
    using Ivh.Common.VisitPay.Constants;
    using Ivh.Common.VisitPay.Enums;
    using Ivh.Common.VisitPay.Messages.Communication;
    using Ivh.Common.VisitPay.Messages.Consolidation;
    using Ivh.Common.VisitPay.Messages.Core;
    using Ivh.Common.VisitPay.Messages.HospitalData;
    using Ivh.Common.VisitPay.Messages.Matching;
    using Ivh.Common.VisitPay.Messages.Matching.Dtos;
    using MassTransit;
    using User.Common.Dtos;

    [System.Runtime.InteropServices.GuidAttribute("75F46F5F-D011-4736-A2EB-916FA33A302F")]
    public class GuarantorApplicationService : ApplicationService, IGuarantorApplicationService
    {
        private readonly Lazy<IConsolidationGuarantorService> _consolidationGuarantorService;
        private readonly Lazy<IVisitPayUserJournalEventService> _userJournalEventService;
        private readonly Lazy<IFinancePlanService> _financePlanService;
        private readonly Lazy<IGuarantorInvitationService> _guarantorInvitationService;
        private readonly Lazy<IGuarantorService> _guarantorService;
        private readonly Lazy<IHsGuarantorService> _hsGuarantorService;
        private readonly Lazy<IPaymentService> _paymentService;
        private readonly ISession _session;
        private readonly Lazy<IHsaBalanceProvider> _hsaBalanceProvider;
        private readonly Lazy<IVpStatementService> _statementService;
        private readonly Lazy<ISsoApplicationService> _ssoApplicationService;
        private readonly Lazy<VisitPayUserService> _userService;
        private readonly Lazy<IGuarantorEnrollmentApplicationService> _guarantorEnrollmentApplicationService;
        private readonly Lazy<ISystemExceptionService> _systemExceptionService;
        private readonly Lazy<ISupportRequestService> _supportRequestService;
        private readonly Lazy<IMetricsProvider> _metricsProvider;
        private readonly Lazy<IHealthEquityEmployerIdResolutionService> _healthEquityEmployerIdResolutionService;
        private readonly Lazy<IGuarantorStateMachineService> _guarantorStateMachineService;
        private readonly Lazy<IStatementDateService> _statementDateService;
        private readonly Lazy<IChangeEventApplicationService> _changeEventApplicationService;
        private readonly Lazy<IVpGuarantorHsMatchApiApplicationService> _vpGuarantorHsMatchApiApplicationService;

        public GuarantorApplicationService(
            Lazy<IApplicationServiceCommonService> applicationServiceCommonService,
            Lazy<IHsGuarantorService> hsGuarantorService,
            Lazy<IGuarantorService> guarantorService,
            Lazy<IGuarantorInvitationService> guarantorInvitationService,
            Lazy<IConsolidationGuarantorService> consolidationGuarantorService,
            Lazy<IGuarantorStateMachineService> guarantorStateMachineService,
            Lazy<IVisitPayUserJournalEventService> userJournalEventService,
            Lazy<IPaymentService> paymentService,
            Lazy<VisitPayUserService> userService,
            Lazy<IVpStatementService> statementService,
            Lazy<IFinancePlanService> financePlanService,
            Lazy<IGuarantorEnrollmentApplicationService> guarantorEnrollmentApplicationService,
            Lazy<ISystemExceptionService> systemExceptionService,
            Lazy<ISsoApplicationService> ssoApplicationService,
            Lazy<ISupportRequestService> supportRequestService,
            ISessionContext<VisitPay> sessionContext,
            Lazy<IHsaBalanceProvider> hsaBalanceProvider,
            Lazy<IMetricsProvider> metricsProvider,
            Lazy<IHealthEquityEmployerIdResolutionService> healthEquityEmployerIdResolutionService,
            Lazy<IStatementDateService> statementDateService,
            Lazy<IChangeEventApplicationService> changeEventApplicationService,
            Lazy<IVpGuarantorHsMatchApiApplicationService> vpGuarantorHsMatchApiApplicationService
            ) : base(applicationServiceCommonService)
        {
            this._hsGuarantorService = hsGuarantorService;
            this._guarantorService = guarantorService;
            this._guarantorInvitationService = guarantorInvitationService;
            this._consolidationGuarantorService = consolidationGuarantorService;
            this._userJournalEventService = userJournalEventService;
            this._paymentService = paymentService;
            this._userService = userService;
            this._statementService = statementService;
            this._financePlanService = financePlanService;
            this._guarantorEnrollmentApplicationService = guarantorEnrollmentApplicationService;
            this._systemExceptionService = systemExceptionService;
            this._ssoApplicationService = ssoApplicationService;
            this._supportRequestService = supportRequestService;
            this._session = sessionContext.Session;
            this._hsaBalanceProvider = hsaBalanceProvider;
            this._metricsProvider = metricsProvider;
            this._healthEquityEmployerIdResolutionService = healthEquityEmployerIdResolutionService;
            this._guarantorStateMachineService = guarantorStateMachineService;
            this._statementDateService = statementDateService;
            this._changeEventApplicationService = changeEventApplicationService;
            this._vpGuarantorHsMatchApiApplicationService = vpGuarantorHsMatchApiApplicationService;
        }
        
        public GuarantorMatchResultEnum IsGuarantorMatch(MatchDataDto matchData)
        {
            bool isAppMatchingEnabled = this.FeatureService.Value.IsFeatureEnabled(VisitPayFeatureEnum.FeatureMatchingApiIsEnabled);
            if (isAppMatchingEnabled)
            {
                GuarantorMatchResult result = this._guarantorService.Value.IsGuarantorMatch(matchData);
                return result?.GuarantorMatchResultEnum ?? GuarantorMatchResultEnum.NoMatchFound;
            }
            else
            {
                GuarantorMatchResponse response = this._hsGuarantorService.Value.GetInitialMatch(
                    matchData.Guarantor.SourceSystemKey, 
                    matchData.Guarantor.LastName,
                    matchData.Guarantor.DOB.Value, // todo: possible null reference exception?
                    matchData.Guarantor.SSN4, 
                    matchData.Guarantor.PostalCode,
                    matchData.Patient.PatientDOB, 
                    Mapper.Map<HttpContextDto>(HttpContext.Current));

                return response.GuarantorMatchResultEnum;
            }
            
        }

        public bool CredentialsMatchCanceledAccount(MatchDataDto matchData)
        {
            bool isAppMatchingEnabled = this.FeatureService.Value.IsFeatureEnabled(VisitPayFeatureEnum.FeatureMatchingApiIsEnabled);
            if (isAppMatchingEnabled)
            {
                GuarantorMatchResult result = this._guarantorService.Value.IsGuarantorMatch(matchData);

                if (result.GuarantorMatchResultEnum == GuarantorMatchResultEnum.AlreadyRegistered && result.VpGuarantorId.HasValue)
                {
                    Guarantor guarantor = this._guarantorService.Value.GetGuarantor(result.VpGuarantorId.Value);

                    return guarantor.IsClosed;
                }

                return false;
            }
            else
            {
                return this._hsGuarantorService.Value.CredentialsMatchCanceledAccount(
                    matchData.Guarantor.LastName,
                    matchData.Guarantor.DOB ?? DateTime.MinValue, 
                    matchData.Guarantor.SSN4, 
                    matchData.Guarantor.PostalCode);
            }
        }

        public GuarantorDto GetGuarantor(int vpGuarantorId)
        {
            Guarantor guarantor = this._guarantorService.Value.GetGuarantor(vpGuarantorId);
            return Mapper.Map<GuarantorDto>(guarantor);
        }

        public GuarantorDto GetGuarantor(string visitPayUserName)
        {
            Guarantor guarantor = this._guarantorService.Value.GetGuarantor(visitPayUserName);
            return Mapper.Map<GuarantorDto>(guarantor);
        }

        public int GetGuarantorId(int visitPayUserId)
        {
            return this._guarantorService.Value.GetGuarantorId(visitPayUserId);
        }
        
        public GuarantorResultsDto GetGuarantors(GuarantorFilterDto filterDto, int pageNumber, int pageSize)
        {
            GuarantorFilter filter = Mapper.Map<GuarantorFilter>(filterDto);

            List<Guarantor> batch = this._guarantorService.Value.GetGuarantors(filter, pageNumber, pageSize).ToList();
            int totalRecords = this._guarantorService.Value.GetGuarantorCount(filter);
            IReadOnlyList<GuarantorDto> guarantorDtos = Mapper.Map<IReadOnlyList<GuarantorDto>>(batch.ToList());

            return new GuarantorResultsDto
            {
                Guarantors = guarantorDtos,
                TotalRecords = totalRecords
            };
        }

        public void SendGuarantorInvitation(GuarantorInvitationDto guarantorInvitationDto)
        {
            GuarantorInvitation guarantorInvitation = Mapper.Map<GuarantorInvitation>(guarantorInvitationDto);
            this._guarantorInvitationService.Value.SendGuarantorInvitation(guarantorInvitation);
        }

        public GuarantorInvitationDto GetGuarantorInvitation(Guid invitationToken)
        {
            GuarantorInvitation guarantorInvitation = this._guarantorInvitationService.Value.GetGuarantorInvitation(invitationToken);
            return Mapper.Map<GuarantorInvitationDto>(guarantorInvitation);
        }

        public DataResult<string, bool> CancelAccount(int guarantorUserId, int clientUserId, int reasonId, string note)
        {
            using (UnitOfWork unitOfWork = new UnitOfWork(this._session))
            {
                VisitPayUser clientUser = this._userService.Value.FindById(clientUserId.ToString());

                int guarantorId = this._guarantorService.Value.GetGuarantorId(guarantorUserId);
                Guarantor guarantor = this._guarantorService.Value.GetGuarantor(guarantorId);
                if (guarantor == null)
                {
                    throw new ArgumentException("Unable to find user's guarantor id", nameof(guarantorUserId));
                }
                if (!guarantor.CanClose())
                {
                    return new DataResult<string, bool> { Result = false, Data = "Failed to cancel because of guarantor's current status" };
                }

                string reasonText = "";

                if (reasonId > 0)
                {
                    //Create and add cancellation to the VpGuarantor object graph
                    VpGuarantorCancellation vpGuarantorCancellation = new VpGuarantorCancellation()
                    {
                        VpGuarantor = guarantor,
                        VisitPayUser = clientUser,
                        VpGuarantorCancellationReason = this._session.Load<VpGuarantorCancellationReason>(reasonId),
                        Notes = note
                    };
                    guarantor.Cancellations.Add(vpGuarantorCancellation);

                    //Get reason text for "audit log"
                    reasonText = vpGuarantorCancellation?.VpGuarantorCancellationReason?.DisplayName;

                }

                guarantor.CloseAccount();
                guarantor.ChangeStateToClosed = true;
                this._guarantorStateMachineService.Value.Evaluate(guarantor);

                guarantor.User.DisableLogin();

                //
                this.LogCancelAccountEvent(clientUserId, guarantorUserId, guarantor.VpGuarantorId, reasonText, note, Mapper.Map<JournalEventHttpContextDto>(HttpContext.Current));

                // cancel things
                this._ssoApplicationService.Value.DeclineAllAccceptedSso(Mapper.Map<HttpContextDto>(HttpContext.Current), guarantorUserId, clientUserId);
                this.CancelGuarantorConsolidationAsync(guarantor).Wait();
                this._paymentService.Value.CancelAllPaymentsForGuarantor(guarantor);

                //TODO: VP-6543 - use service bus to access HospitalData
                this._hsGuarantorService.Value.CancelGuarantor(guarantor);
                SupportRequestResults openSupportRequests = this._supportRequestService.Value.GetSupportRequests(guarantor, new SupportRequestFilter() { SupportRequestStatus = SupportRequestStatusEnum.Open }, 0, int.MaxValue);
                foreach (SupportRequest openSupportRequest in openSupportRequests.SupportRequests)
                {
                    this._supportRequestService.Value.SetSupportRequestClosedOrOpen(
                        SupportRequestStatusEnum.Closed,
                        openSupportRequest.SupportRequestId,
                        guarantor, "Account Canceled",
                        SystemUsers.SystemUserId);
                }

                this._guarantorService.Value.InsertOrUpdateGuarantor(guarantor);

                this._session.Transaction.RegisterPostCommitSuccessAction((p1) =>
                {
                    try
                    {
                        this.Bus.Value.PublishMessage(new SendAccountClosedCompleteMessage
                        {
                            Email = p1.User.Email,
                            Name = p1.User.FirstName,
                            VisitPayUserId = p1.User.VisitPayUserId
                        })
                            .Wait();
                    }
                    catch (Exception)
                    {
                        this.LoggingService.Value.Fatal(() => $"GuarantorApplicationService::CancelAccountFinalize - Failed to SendAccountClosedCompleteMessage - VpGuarantorId = {p1.VpGuarantorId}");
                    }

                    try
                    {
                        Task.Run(async () => await this._guarantorEnrollmentApplicationService.Value.PublishUnEnrollGuarantorMessageAsync(p1.VpGuarantorId)).Wait();
                    }
                    catch
                    {
                        this.LoggingService.Value.Fatal(() => $"GuarantorApplicationService::CancelAccountFinalize - Failed to PublishUnEnrollGuarantorMessageAsync - VpGuarantorId = {p1.VpGuarantorId}");
                    }

                    try
                    {
                        this.Bus.Value.PublishMessage(new EtlHsGuarantorCancelMessage
                        {
                            VpGuarantorId = p1.VpGuarantorId
                        })
                            .Wait();
                    }
                    catch
                    {
                        this.LoggingService.Value.Fatal(() => $"GuarantorApplicationService::CancelAccountFinalize - Failed to EtlHsGuarantorCancelMessage - VpGuarantorId = {p1.VpGuarantorId}");
                    }


                    try
                    {
                        if (this._financePlanService.Value.AnyOpenFinancePlansForGuarantor(p1))
                        {
                            IList<FinancePlan> financePlans = this._financePlanService.Value.GetAllActiveFinancePlansForGuarantor(p1);
                            foreach (FinancePlan financePlan in financePlans)
                            {
                                this._financePlanService.Value.CancelFinancePlan(financePlan, "Account Canceled");
                            }
                        }
                       
                    }
                    catch (Exception e)
                    {
                          this.LoggingService.Value.Fatal(()=>"Failed to cancel finance plans",e);
                    }

                }, guarantor);

                unitOfWork.Commit();
            }

            return new DataResult<string, bool> { Result = true, Data = "Successfully canceled account" };
        }

        public void VpccUserDeclinedFinancePlan(int visitPayUserId, int vpGuarantorId)
        {
            //Ensure feature is enabled
            if (!this.FeatureService.Value.IsFeatureEnabled(VisitPayFeatureEnum.OfflineVisitPay))
            {
                return;
            }

            //Ensure guarantor is an offline user
            Guarantor guarantor = this.GetGuarantorByUserId(visitPayUserId);
            if (!guarantor.IsOfflineGuarantor())
            {
                return;
            }

            //Check if guarantor has any active FP's
            if (this._financePlanService.Value.GetActiveFinancePlanCount(vpGuarantorId) <= 0)
            {
                //User has no active FP's and they declined the FP terms, so cancel their account
                this.CancelAccount(visitPayUserId, SystemUsers.SystemUserId, 0, null);
            }
        }

        public void ConvertVpccUserToVisitPayUser(int visitPayUserId)
        {
            using (UnitOfWork unitOfWork = new UnitOfWork(this._session))
            {
                Guarantor guarantor = this.GetGuarantorByUserId(visitPayUserId);

                //go online
                guarantor.SetGuarantorType(GuarantorTypeEnum.Online);
                guarantor.UseAutoPay = true;

                //set to matched (for any new vpcc finance plans that may not be accepted)
                this.SetVpccPendingMatchesToMatched(guarantor);

                //save
                this._guarantorService.Value.InsertOrUpdateGuarantor(guarantor);

                unitOfWork.Commit();
            }
        }

        public void SetVpccPendingMatchesToMatched(int vpGuarantorId)
        {
            using (UnitOfWork unitOfWork = new UnitOfWork(this._session))
            {
                Guarantor guarantor = this._guarantorService.Value.GetGuarantor(vpGuarantorId);

                this.SetVpccPendingMatchesToMatched(guarantor);

                unitOfWork.Commit();
            }
        }

        private void SetVpccPendingMatchesToMatched(Guarantor guarantor)
        {
            //set to matched
            IList<HsGuarantorMap> hsGuarantorMaps = guarantor.HsGuarantorMaps.Where(x => x.HsGuarantorMatchStatus == HsGuarantorMatchStatusEnum.VpccPending).ToList();
            foreach (HsGuarantorMap hsGuarantorMap in hsGuarantorMaps)
            {
                hsGuarantorMap.HsGuarantorMatchStatus = HsGuarantorMatchStatusEnum.Matched;
            }

            //send message to set hs domain entities (VpGuarantorHsMatch) to matched
            this._session.Transaction.RegisterPostCommitSuccessAction((vpgid, hsgids) =>
            {
                this.Bus.Value.PublishMessage(new VpccGuarantorEnrolledInVisitPayMessage
                {
                    VpGuarantorId = vpgid,
                    HsGuarantorIds = hsgids
                }).Wait();

            }, guarantor.VpGuarantorId, hsGuarantorMaps.Select(x => x.HsGuarantorId).ToList());
        }
        
        private Guarantor GetGuarantorByUserId(int guarantorUserId)
        {
            int guarantorId = this._guarantorService.Value.GetGuarantorId(guarantorUserId);
            Guarantor guarantor = this._guarantorService.Value.GetGuarantor(guarantorId);
            if (guarantor == null)
            {
                throw new ArgumentException("Unable to find user's guarantor id", nameof(guarantorUserId));
            }
            return guarantor;
        }

        public DataResult<string, bool> ReactivateAccount(int guarantorUserId, int clientUserId, DateTime reactivateAccountOn, JournalEventHttpContextDto context)
        {
            int guarantorId = this._guarantorService.Value.GetGuarantorId(guarantorUserId);
            Guarantor guarantor = this._guarantorService.Value.GetGuarantor(guarantorId);

            if (guarantor == null)
            {
                throw new ArgumentException("Unable to find user's guarantor id", nameof(guarantorUserId));
            }

            using (UnitOfWork unitOfWork = new UnitOfWork(this._session))
            {

                if (guarantor.VpGuarantorStatus == VpGuarantorStatusEnum.Closed)
                {
                    this.ReactivateAccount(guarantor, reactivateAccountOn);
                    this.LogReactivateAccountEvent(clientUserId, guarantor.User.VisitPayUserId, guarantor.VpGuarantorId, context);
                    this._systemExceptionService.Value.LogSystemException(new Domain.Core.SystemException.Entities.SystemException
                    {
                        SystemExceptionType = SystemExceptionTypeEnum.ReactivateAccount,
                        SystemExceptionDescription = $"{clientUserId.ToString()} reactivated account",
                        VpGuarantorId = guarantor.VpGuarantorId
                    });
                }

                unitOfWork.Commit();
            }

            return new DataResult<string, bool> { Result = true, Data = "Successfully reactivated account" };
        }

        public void ReactivateAccount(int vpGuarantorId)
        {
            Guarantor guarantor = this._guarantorService.Value.GetGuarantor(vpGuarantorId);
            DateTime reactivateAccountOn = DateTime.UtcNow;
            this.ReactivateAccount(guarantor, reactivateAccountOn);
        }

        private void ReactivateAccount(Guarantor guarantor, DateTime reactivateAccountOn)
        {
            if (guarantor.VpGuarantorStatus != VpGuarantorStatusEnum.Closed)
            {
                return;
            }

            using (UnitOfWork unitOfWork = new UnitOfWork(this._session))
            {
                if (guarantor.VpGuarantorStatus == VpGuarantorStatusEnum.Closed)
                {
                    //TODO: VP-6543 - use service bus to access HospitalData
                    this._hsGuarantorService.Value.ReactivateGuarantor(guarantor);
                }

                guarantor.ReactivateAccountOn(DateTime.UtcNow);
                guarantor.ChangeStateToActive = true;
                this._guarantorStateMachineService.Value.Evaluate(guarantor);

                guarantor.User.EnableLogin();

                this._guarantorService.Value.InsertOrUpdateGuarantor(guarantor);
                this._session.Transaction.RegisterPostCommitSuccessAction((p1) =>
                {
                    try
                    {
                        this.Bus.Value.PublishMessage(new SendAccountReactivationCompleteMessage { Email = p1.User.Email, Name = p1.User.FirstName, VisitPayUserId = p1.User.VisitPayUserId, UserName = p1.User.UserName }).Wait();
                    }
                    catch
                    {
                        this.LoggingService.Value.Fatal(() => $"GuarantorApplicationService::ProcessAccountReactivation - Failed to publish SendAccountReactivationCompleteMessage - VpGuarantorId = {p1.VpGuarantorId}");
                    }

                    if (p1.VpGuarantorStatus != VpGuarantorStatusEnum.Active)
                    {
                        return;
                    }
                    try
                    {
                        Task.Run(async () => await this._guarantorEnrollmentApplicationService.Value.PublishEnrollGuarantorMessageAsync(p1.VpGuarantorId)).Wait();
                    }
                    catch
                    {
                        this.LoggingService.Value.Fatal(() => $"GuarantorApplicationService::ProcessAccountReactivation - Failed to PublishEnrollGuarantorMessageAsync - VpGuarantorId = {p1.VpGuarantorId}");
                    }

                    try
                    {
                        this.Bus.Value.PublishMessage(new EtlHsGuarantorReactivateMessage { VpGuarantorId = p1.VpGuarantorId }).Wait();
                    }
                    catch
                    {
                        this.LoggingService.Value.Fatal(() => $"GuarantorApplicationService::ProcessAccountReactivation - Failed to publish EtlHsGuarantorReactivateMessage - VpGuarantorId = {p1.VpGuarantorId}");
                    }
                }, guarantor);

                unitOfWork.Commit();
            }
        }

        #region payment due day

        private const string PaymentDueDayUpdated = "Successfully updated payment due day";
        private const string PaymentDueDayIsTheSame = "Day is the same, no further action required";
        private const string PaymentDueDayReasonFinancePlan = "Finance Plan Created";
        private const string PaymentDueDayReasonManaged = "Managing Guarantor's Payment Due Date Changed";
        
        public DataResult<string, bool> ChangePaymentDueDay(int currentVisitPayUserId, int dueDay, bool isFinancePlanCreatedChange, int actionVisitPayUserId)
        {
            // this is the logged in guarantor
            int guarantorId = this._guarantorService.Value.GetGuarantorId(currentVisitPayUserId);
            Guarantor guarantor = this._guarantorService.Value.GetGuarantor(guarantorId);
            if (guarantor == null)
            {
                throw new ArgumentException($"Unable to find guarantor by VisitPayUserId ({currentVisitPayUserId})", nameof(currentVisitPayUserId));
            }

            if (guarantor.PaymentDueDay == dueDay)
            {
                VpStatement currentStatement = this._statementService.Value.GetMostRecentStatement(guarantor);
                if (currentStatement.IsGracePeriod)
                {
                    return new DataResult<string, bool>
                    {
                        Result = true,
                        Data = PaymentDueDayIsTheSame
                    };
                }
            }
            
            if (guarantor.IsManaged())
            {
                VisitPayUser managingGuarantorUser = guarantor.ActiveManagingConsolidationGuarantors.First().ManagingGuarantor.User;
                return new DataResult<string, bool>
                {
                    Result = true, Data = $"Guarantor is currently consolidated and managed by {managingGuarantorUser.FirstNameLastName}"
                };
            }

            VisitPayUser actionVisitPayUser = this._userService.Value.FindById(actionVisitPayUserId.ToString());
            if (actionVisitPayUser == null)
            {
                throw new ArgumentException($"Unable to find user ({actionVisitPayUserId})", nameof(actionVisitPayUserId));
            }

            using (UnitOfWork unitOfWork = new UnitOfWork(this._session))
            {
                string changeReason = isFinancePlanCreatedChange ? PaymentDueDayReasonFinancePlan : string.Empty;
                
                this._guarantorService.Value.ChangePaymentDueDay(guarantor, dueDay, actionVisitPayUser, changeReason);
                ProcessPaymentDueDayChangedResult result = this._statementService.Value.ProcessPaymentDueDayChanged(guarantor, isFinancePlanCreatedChange, actionVisitPayUserId);

                if (guarantor.IsManaging())
                {
                    foreach (Guarantor managedGuarantor in guarantor.ActiveManagedConsolidationGuarantors.Select(x => x.ManagedGuarantor))
                    {
                        this._guarantorService.Value.ChangePaymentDueDay(managedGuarantor, dueDay, actionVisitPayUser, PaymentDueDayReasonManaged);
                        this._statementService.Value.ProcessPaymentDueDayChanged(managedGuarantor, isFinancePlanCreatedChange, actionVisitPayUserId);
                    }
                }
                
                if (!isFinancePlanCreatedChange && result.IsChanged)
                {
                    // only send this e-mail if it's not a finance plan change
                    // only send this e-mail if the current statement payment due date changed
                    this._session.Transaction.RegisterPostCommitSuccessAction((vpgid, pdd) =>
                    {
                        SendPaymentDueDateChangeEmailMessage message = new SendPaymentDueDateChangeEmailMessage
                        {
                            VpGuarantorId = vpgid,
                            NextPaymentDueDate = pdd
                        };
                        this.Bus.Value.PublishMessage(message).Wait();
                    }, guarantor.VpGuarantorId, result.NewPaymentDueDate);
                }

                unitOfWork.Commit();
            }

            return new DataResult<string, bool> { Result = true, Data = PaymentDueDayUpdated };
        }
        
        #endregion

        public IList<GuarantorPaymentDueDayHistoryDto> GetGuarantorPaymentDueDayHistory(int guarantorUserId)
        {
            int guarantorId = this._guarantorService.Value.GetGuarantorId(guarantorUserId);
            Guarantor guarantor = this._guarantorService.Value.GetGuarantor(guarantorId);
            if (guarantor == null)
            {
                throw new ArgumentException("Unable to find user's guarantor id", nameof(guarantorUserId));
            }
            IList<GuarantorPaymentDueDayHistoryDto> returnValue = Mapper.Map<IList<GuarantorPaymentDueDayHistoryDto>>(guarantor.PaymentDueDayHistories);
            return returnValue;
        }

        public void ResolveMatches(int guarantorId)
        {
            this._hsGuarantorService.Value.GetNewMatches(guarantorId);
        }
        
        private async Task CancelGuarantorConsolidationAsync(Guarantor guarantor)
        {
            if (guarantor.ConsolidationStatus != GuarantorConsolidationStatusEnum.NotConsolidated)
            {
                IEnumerable<ConsolidationGuarantor> consolidationGuarantors = guarantor.ManagedConsolidationGuarantors.EmptyIfNull().Concat(guarantor.ManagingConsolidationGuarantors.EmptyIfNull());

                foreach (ConsolidationGuarantor consolidationGuarantor in consolidationGuarantors)
                {
                    GuarantorConsolidationStatusEnum guarantorConsolidationStatus = GuarantorConsolidationStatusEnum.NotConsolidated;

                    if (guarantor.VpGuarantorId == consolidationGuarantor.ManagedGuarantor.VpGuarantorId)
                    {
                        if (consolidationGuarantor.IsPending())
                        {
                            guarantorConsolidationStatus = GuarantorConsolidationStatusEnum.PendingManaged;
                        }
                        if (consolidationGuarantor.IsActive())
                        {
                            guarantorConsolidationStatus = GuarantorConsolidationStatusEnum.Managed;
                        }
                    }
                    if (guarantor.VpGuarantorId == consolidationGuarantor.ManagingGuarantor.VpGuarantorId)
                    {
                        if (consolidationGuarantor.IsPending())
                        {
                            guarantorConsolidationStatus = GuarantorConsolidationStatusEnum.PendingManaging;
                        }
                        if (consolidationGuarantor.IsActive())
                        {
                            guarantorConsolidationStatus = GuarantorConsolidationStatusEnum.Managing;
                        }
                    }

                    await this._consolidationGuarantorService.Value.CancelConsolidationAsync(consolidationGuarantor.ConsolidationGuarantorId, SystemUsers.SystemUserId, ConsolidationGuarantorCancellationReasonEnum.GuarantorAccountCancelled).ConfigureAwait(false);

                    int managingVpGuarantorId = consolidationGuarantor.ManagingGuarantor.VpGuarantorId;
                    int managedVpGuarantorId = consolidationGuarantor.ManagedGuarantor.VpGuarantorId;

                    switch (guarantorConsolidationStatus)
                    {
                        case GuarantorConsolidationStatusEnum.Managed:
                            this.SendConsolidationBaseEmailOnCommit<ConsolidationLinkageCancelledByManagedToManagingMessage>(managingVpGuarantorId: managingVpGuarantorId, managedVpGuarantorId: managedVpGuarantorId);
                            break;
                        case GuarantorConsolidationStatusEnum.Managing:
                            if (consolidationGuarantor.DateAcceptedFinancePlanTermsByManagingGuarantor.HasValue)
                            {
                                this.SendConsolidationBaseEmailOnCommit<ConsolidationLinkageCancelledByManagingToManagedWithFpMessage>(managingVpGuarantorId: managingVpGuarantorId, managedVpGuarantorId: managedVpGuarantorId);
                            }
                            else
                            {
                                this.SendConsolidationBaseEmailOnCommit<ConsolidationLinkageCancelledByManagingToManagedMessage>(managingVpGuarantorId: managingVpGuarantorId, managedVpGuarantorId: managedVpGuarantorId);
                            }
                            break;
                        case GuarantorConsolidationStatusEnum.PendingManaged:
                            this.SendConsolidationBaseEmailOnCommit<ConsolidationHouseholdRequestCancelledByManagedToManagingMessage>(managingVpGuarantorId: managingVpGuarantorId, managedVpGuarantorId: managedVpGuarantorId);
                            break;
                        case GuarantorConsolidationStatusEnum.PendingManaging:
                            this.SendConsolidationBaseEmailOnCommit<ConsolidationHouseholdRequestCancelledByManagingToManagedMessage>(managingVpGuarantorId: managingVpGuarantorId, managedVpGuarantorId: managedVpGuarantorId);
                            break;
                        case GuarantorConsolidationStatusEnum.NotConsolidated:
                            break;
                        default:
                            throw new ArgumentOutOfRangeException();
                    }
                }
            }
        }

        private void SendConsolidationBaseEmailOnCommit<TMessage>(int managingVpGuarantorId, int managedVpGuarantorId) where TMessage : ConsolidationBase<TMessage>
        {
            this._session.Transaction.RegisterPostCommitSuccessAction((p1) =>
            {
                this.Bus.Value.PublishMessage(p1).Wait();
            }, this.ConvertFromConsolidationBase<TMessage>(managingVpGuarantorId: managingVpGuarantorId, managedVpGuarantorId: managedVpGuarantorId));
        }

        private TMessage ConvertFromConsolidationBase<TMessage>(int managingVpGuarantorId, int managedVpGuarantorId) where TMessage : ConsolidationBase<TMessage>
        {
            TMessage obj = Activator.CreateInstance<TMessage>();
            obj.ManagedVpGuarantorId = managedVpGuarantorId;
            obj.ManagingVpGuarantorId = managingVpGuarantorId;

            return obj;
        }

        private static readonly SemaphoreSlim HealthEquityDistributeCacheSemaphore = new SemaphoreSlim(1, 1);
        private const int HealthEquityTimeToKeepBalance = 30 * 60; //30 minutes in seconds

        public async Task<HealthEquityBalanceDto> GetGuarantorHealthEquityBalanceAsnyc(int guarantorId, int visitpayUserId, SsoProviderEnum ssoProviderEnum, HttpContextDto context = null)
        {
            Guarantor guarantor = this._guarantorService.Value.GetGuarantor(guarantorId);
            if (guarantor == null || guarantor.User.VisitPayUserId != visitpayUserId || !this._ssoApplicationService.Value.IsUserAuthorized(SsoProviderEnum.HealthEquityOutbound, context))
            {
                return null;
            }

            string keyForCache = $"HealthEquityBalance-{guarantorId}";

            HealthEquityBalanceDto balance = await this.Cache.Value.GetObjectAsync<HealthEquityBalanceDto>(keyForCache);

            if (balance == null)
            {
                //If it was a cache miss, grab lock.
                //Wait up to 1 minute.
                await HealthEquityDistributeCacheSemaphore.WaitAsync(new TimeSpan(0, 0, 1, 0));
                try
                {
                    //Check again, incase the lock was had and the value was getting set.
                    balance = await this.Cache.Value.GetObjectAsync<HealthEquityBalanceDto>(keyForCache);

                    if (balance == null)
                    {
                        HealthEquityBalanceResponse response;
                        if (ssoProviderEnum == SsoProviderEnum.HealthEquityOutbound)
                        {
                            response = await this.GetSamlResponseAsync(guarantor);
                        }
                        else
                        {
                            response = await this.GetOauthResponseAsync(visitpayUserId, ssoProviderEnum);
                        }

                        if (response != null && response.WasSuccessful)
                        {
                            balance = new HealthEquityBalanceDto
                            {
                                CashBalance = response.CashBalance,
                                InvestmentBalance = response.InvestmentBalance,
                                ContributionsYtd = response.ContributionsYtd,
                                DistributionsYtd = response.DistributionsYtd,
                                LastUpdated = DateTime.UtcNow
                            };
                            await this.Cache.Value.SetObjectAsync(keyForCache, balance, HealthEquityTimeToKeepBalance);
                        }
                        else
                        {
                            this._metricsProvider.Value.Increment(Metrics.Increment.Hqy.BalanceFailed);
                        }
                    }
                }
                finally
                {
                    HealthEquityDistributeCacheSemaphore.Release();
                }
            }
            return balance;
        }

        private async Task<HealthEquityBalanceResponse> GetSamlResponseAsync(Guarantor guarantor)
        {
            if (guarantor == null)
            {
                return null;
            }

            string ssn = this.GetSsn(guarantor.VpGuarantorId);
            if (ssn.IsNotNullOrEmpty())
            {
                string employerId = this._healthEquityEmployerIdResolutionService.Value.GetHealthEquityEmployerId(guarantor.VpGuarantorId);
                if (employerId.IsNotNullOrEmpty())
                {
                    return await this._hsaBalanceProvider.Value.GetBalanceForSsnAsync(ssn, employerId);
                }

                this._metricsProvider.Value.Increment(Metrics.Increment.Hqy.BalanceFailed);
            }
            return null;
        }

        public string GetSsn(int vpGuarantorId)
        {
            bool matchingBIsEnabled = this.FeatureService.Value.IsFeatureEnabled(VisitPayFeatureEnum.FeatureMatchingApiIsEnabled);
            if (matchingBIsEnabled)
            {
                return this._guarantorService.Value.GetGuarantorMatchSsn(vpGuarantorId);
            }
            else
            {
                return this._vpGuarantorHsMatchApiApplicationService.Value.GetSsn(vpGuarantorId)?.Ssn;
            }
        }

        private async Task<HealthEquityBalanceResponse> GetOauthResponseAsync(int visitpayUserId, SsoProviderEnum ssoProviderEnum)
        {
            HealthEquityBalanceResponse response = null;

            IList<SsoProviderWithUserSettingsDto> allUserSettings = this._ssoApplicationService.Value.GetAllProvidersWithUserSettings(visitpayUserId);
            SsoProviderWithUserSettingsDto settingForProvider = allUserSettings.FirstOrDefault(x => x.Provider.SsoProviderEnum == ssoProviderEnum);
            if (settingForProvider != null && settingForProvider.UserSettings.SsoStatus == SsoStatusEnum.Accepted)
            {
                response = await this._hsaBalanceProvider.Value.GetBalanceForOauthAsync(settingForProvider.UserSettings.AccessToken, Mapper.Map<SsoProvider>(settingForProvider.Provider));
                if (!response.WasSuccessful
                    && response.AuthTokenExpired.HasValue
                    && response.AuthTokenExpired.Value &&
                    this._ssoApplicationService.Value.RefreshOAuthAccessTokenWithRefreshToken(settingForProvider))
                {
                    allUserSettings = this._ssoApplicationService.Value.GetAllProvidersWithUserSettings(visitpayUserId);
                    settingForProvider = allUserSettings.FirstOrDefault(x => x.Provider.SsoProviderEnum == ssoProviderEnum);
                    response = await this._hsaBalanceProvider.Value.GetBalanceForOauthAsync(settingForProvider.UserSettings.AccessToken, Mapper.Map<SsoProvider>(settingForProvider.Provider));
                }
            }
            return response;
        }

        public async Task<HealthEquitySsoStatusDto> GetGuarantorHealthEquitySsoStatusAsnyc(int guarantorId, int visitpayUserId)
        {
            Guarantor guarantor = this._guarantorService.Value.GetGuarantor(guarantorId);
            if (guarantor == null || guarantor.User.VisitPayUserId != visitpayUserId) { return null; }

            string keyForCache = $"HealthEquitySsoStatus-{visitpayUserId}";

            HealthEquitySsoStatusDto status = await this.Cache.Value.GetObjectAsync<HealthEquitySsoStatusDto>(keyForCache);
            if (status == null)
            {
                status = new HealthEquitySsoStatusDto
                {
                    IsHqySsoUp = true,
                    LastUpdated = DateTime.UtcNow
                };
            }
            else if (!status.IsHqySsoUp && status.LastUpdated.AddMinutes(30) < DateTime.UtcNow)
            {
                status.IsHqySsoUp = true;
            }
            return status;
        }

        public IList<GuarantorCancellationReasonDto> GetAllCancellationReasons()
        {
            IList<VpGuarantorCancellationReason> reasons = this._guarantorService.Value.GetAllCancellationReasons();
            IList<GuarantorCancellationReasonDto> mappedReasons = Mapper.Map<IList<GuarantorCancellationReasonDto>>(reasons);
            return mappedReasons;
        }

        public int GetVpGuarantorId(int visitPayUserId) => this.GetGuarantorByUserId(visitPayUserId).VpGuarantorId;

        public bool SetGuarantorAutoPay(int vpGuarantorId, bool isAutoPay)
        {
            Guarantor guarantor = this._guarantorService.Value.GetGuarantor(vpGuarantorId);
            if (guarantor.UseAutoPay != isAutoPay)
            {
                guarantor.UseAutoPay = isAutoPay;
                this._guarantorService.Value.InsertOrUpdateGuarantor(guarantor);

                if (guarantor.IsOfflineGuarantor())
                {
                    string incrementMetric = isAutoPay ? Metrics.Increment.Offline.AutoPay : Metrics.Increment.Offline.NonAutoPay;
                    string decrementMetric = !isAutoPay ? Metrics.Increment.Offline.AutoPay : Metrics.Increment.Offline.NonAutoPay;

                    this._metricsProvider.Value.Increment(incrementMetric);
                    this._metricsProvider.Value.Decrement(decrementMetric);
                }
            }

            return true;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="hsGuarantorId"></param>
        /// <param name="vpGuarantorId"></param>
        /// <param name="registrationMatch">If match is made during registration</param>
        private void LogGuarantorMatch(int hsGuarantorId, int vpGuarantorId, bool registrationMatch)
        {
            VisitPayUser user = registrationMatch
                ? this._guarantorService.Value.GetGuarantor(vpGuarantorId).User
                : new VisitPayUser { VisitPayUserId = SystemUsers.SystemUserId } ;
            
            JournalEventUserContext journalEventUserContext = this._userJournalEventService.Value.GetJournalEventUserContext(Mapper.Map<JournalEventHttpContextDto>(HttpContext.Current), user);
            this._userJournalEventService.Value.LogGuarantorMatch(journalEventUserContext, hsGuarantorId, vpGuarantorId, registrationMatch ? "Registration" : "Recurring");
        }

        private void AddInitialMatches(int vpGuarantorId, MatchGuarantorDto matchGuarantor, MatchPatientDto matchPatient)
        {
            // add matches via matching api
            IList<GuarantorMatchResult> matchResults = this._guarantorService.Value.AddInitialMatches(vpGuarantorId, matchGuarantor, matchPatient);

            // add new matches to cdi 
            this.AddMatchesToCdi(matchResults);

            // send the enroll message
            this.PublishEnrollGuarantorMessage(vpGuarantorId);
        }

        private void ProcessOngoingMatches(IList<GuarantorMatchResult> matches)
        {
            // add new matches to cdi 
            IList<(int vpGuarantorId, IList<int> hsGuarantorIds)> matchesAdded = this.AddMatchesToCdi(matches);

            // create change events to load visits
            foreach ((int vpguarantorId, IList<int> hsGuarantorIds) in matchesAdded)
            {
                this._changeEventApplicationService.Value.GenerateChangeEventForGuarantor(vpguarantorId, hsGuarantorIds);
            }
        }

        private IList<(int vpGuarantorId, IList<int> hsGuarantorIds)> AddMatchesToCdi(IList<GuarantorMatchResult> matches)
        {
            IList<(int, IList<int>)> matchesAdded = new List<(int, IList<int>)>();

            // add new matches to cdi 
            if (matches.IsNotNullOrEmpty())
            {
                foreach (IGrouping<int, GuarantorMatchResult> guarantorMatchResults in matches.Where(x => x.VpGuarantorId.HasValue).GroupBy(x => x.VpGuarantorId.Value))
                {
                    int vpGuarantorId = guarantorMatchResults.Key;

                    IList<int> hsGuarantorIds = this._hsGuarantorService.Value.AddMatches(guarantorMatchResults.Where(match => match.GuarantorMatchResultEnum == GuarantorMatchResultEnum.Matched), vpGuarantorId);

                    matchesAdded.Add((vpGuarantorId, hsGuarantorIds));

                    foreach (int hsGuarantorId in hsGuarantorIds)
                    {
                        this.LogGuarantorMatch(hsGuarantorId, vpGuarantorId, true);
                    }
                }
            }

            return matchesAdded;
        }

        private void PublishEnrollGuarantorMessage(int vpGuarantorId)
        {
            this._session.Transaction.RegisterPostCommitSuccessAction((vpgid) =>
            {
                try
                {
                    Task.Run(async () => await this._guarantorEnrollmentApplicationService.Value.PublishEnrollGuarantorMessageAsync(vpgid)).Wait();
                }
                catch
                {
                    this.LoggingService.Value.Fatal(() => $"{this.GetType().Name}::{nameof(this.AddInitialMatches)} - Failed to PublishEnrollGuarantorMessageAsync - guarantorId = {vpgid}");
                }
            }, vpGuarantorId);
        }

        #region Event Logging

        private void LogCancelAccountEvent(int clientUserId, int targetUserId, int vpGuarantorId, string reason, string note, JournalEventHttpContextDto context)
        {
            JournalEventUserContext journalEventUserContext = this._userJournalEventService.Value.GetJournalEventUserContext(context, clientUserId);
            this._userJournalEventService.Value.LogCancelAccountEvent(journalEventUserContext, targetUserId, vpGuarantorId, reason, note);
        }

        private void LogReactivateAccountEvent(int clientUserId, int targetUserId, int vpGuarantorId, JournalEventHttpContextDto context)
        {
            JournalEventUserContext journalEventUserContext = this._userJournalEventService.Value.GetJournalEventUserContext(context, clientUserId);
            this._userJournalEventService.Value.LogReactivateAccountEvent(journalEventUserContext, targetUserId, vpGuarantorId);
        }

        #endregion

        #region Message Consumers
        
        [UniversalConsumer(UniversalQueuesEnum.VisitPayProcessor, UniversalPriorityEnum.Priority30)]
        public void ConsumeMessage(ReactivateGuarantorMessage message, ConsumeContext<ReactivateGuarantorMessage> consumeContext)
        {
            try
            {
                this.ReactivateAccount(message.VpGuarantorId);
            }
            catch (Exception e)
            {
                this._systemExceptionService.Value.LogSystemException(new Domain.Core.SystemException.Entities.SystemException
                {
                    VpGuarantorId = message.VpGuarantorId,
                    SystemExceptionDescription = $"CancelGuarantorMessageConsumer::Consume<ConsumeContext<CancelGuarantorMessage>> - {ExceptionHelper.AggregateExceptionToString(e)}",
                    SystemExceptionType = SystemExceptionTypeEnum.ReactivateAccount
                });
                throw;
            }
        }

        public bool IsMessageValid(ReactivateGuarantorMessage message, ConsumeContext<ReactivateGuarantorMessage> consumeContext)
        {
            bool isValid = true;
            return isValid;
        }

        [UniversalConsumer(UniversalQueuesEnum.VisitPayProcessor, UniversalPriorityEnum.Priority20)]
        public void ConsumeMessage(AddInitialMatchesToCdiMessage message, ConsumeContext<AddInitialMatchesToCdiMessage> consumeContext)
        {
            this.AddInitialMatches(message.VpGuarantorId, message.MatchGuarantorDto, message.MatchPatientDto);
        }

        public bool IsMessageValid(AddInitialMatchesToCdiMessage message, ConsumeContext<AddInitialMatchesToCdiMessage> consumeContext)
        {
            bool isValid = true;
            return isValid;
        }

        [UniversalConsumer(UniversalQueuesEnum.HsDataProcessor, UniversalPriorityEnum.Priority30)]
        public void ConsumeMessage(NewGuarantorMatchCreatedMessage message, ConsumeContext<NewGuarantorMatchCreatedMessage> consumeContext)
        {
            this.ProcessOngoingMatches(message.MatchResult.ToListOfOne());
        }

        public bool IsMessageValid(NewGuarantorMatchCreatedMessage message, ConsumeContext<NewGuarantorMatchCreatedMessage> consumeContext)
        {
            bool isValid = true;
            return isValid;
        }

        [UniversalConsumer(UniversalQueuesEnum.HsDataProcessor, UniversalPriorityEnum.Priority30)]
        public void ConsumeMessage(NewGuarantorMatchCreatedMessageList message, ConsumeContext<NewGuarantorMatchCreatedMessageList> consumeContext)
        {
            this.ProcessOngoingMatches(message.Messages.Select(x => x.MatchResult).ToList());
        }

        public bool IsMessageValid(NewGuarantorMatchCreatedMessageList message, ConsumeContext<NewGuarantorMatchCreatedMessageList> consumeContext)
        {
            bool isValid = true;
            return isValid;
        }

        #endregion
    }
}
﻿namespace Ivh.Application.Core
{
    using Common.Interfaces;
    using Domain.Core.IpAccess.Interfaces;

    public class IpAccessApplicationService : IIpAccessApplicationService
    {
        private readonly IIpAccessService _ipAccessService;

        public IpAccessApplicationService(IIpAccessService ipAccessService)
        {
            this._ipAccessService = ipAccessService;
        }

        public bool IsIpAddressAllowed(string ipToCheck)
        {
            return this._ipAccessService.IsIpAddressAllowed(ipToCheck);
        }
    }
}
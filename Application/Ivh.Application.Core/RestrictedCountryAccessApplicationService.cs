﻿namespace Ivh.Application.Core
{
    using System;
    using Common.Interfaces;
    using Domain.Core.RestrictedCountryAccess.Interfaces;

    public class RestrictedCountryAccessApplicationService : IRestrictedCountryAccessApplicationService
    {
        private readonly Lazy<IRestrictedCountryAccessService> _restrictedCountryAccessService;

        public RestrictedCountryAccessApplicationService(Lazy<IRestrictedCountryAccessService> restrictedCountryAccessService)
        {
            this._restrictedCountryAccessService = restrictedCountryAccessService;
        }

        public bool IsIpRestrictedByCountry(string ipToCheck)
        {
            return this._restrictedCountryAccessService.Value.IsIpRestrictedByCountry(ipToCheck);
        }
    }
}
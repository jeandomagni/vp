﻿namespace Ivh.Application.Core
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web;
    using AutoMapper;
    using Base.Common.Dtos;
    using Base.Common.Interfaces;
    using Base.Services;
    using Common.Dtos;
    using Common.Interfaces;
    using Domain.Core.Audit.Entities;
    using Domain.Core.Audit.Interfaces;
    using Domain.EventJournal.Entities;
    using Domain.EventJournal.Interfaces;
    using Domain.User.Entities;
    using Domain.User.Interfaces;
    using Domain.User.Services;
    using Ivh.Common.Base.Enums;
    using Ivh.Common.Base.Utilities.Extensions;
    using Ivh.Common.EventJournal;
    using Ivh.Common.EventJournal.Templates.Audit;
    using Ivh.Common.VisitPay.Enums;
    using Ivh.Common.VisitPay.Strings;
    using Microsoft.AspNet.Identity;
    using OfficeOpenXml;
    using OfficeOpenXml.Style;
    using User.Common.Dtos;
    using JournalEvent = Ivh.Common.EventJournal.JournalEvent;

    public class AuditApplicationService : ApplicationService, IAuditApplicationService
    {
        private readonly Lazy<IAuditService> _auditService;
        private readonly Lazy<IVisitPayUserJournalEventService> _userJournalEventService;
        private readonly Lazy<IEventJournalService> _eventJournalService;
        private readonly Lazy<IEventJournalQueryService> _eventJournalQueryService;
        private readonly Lazy<VisitPayUserService> _visitPayUserService;

        public AuditApplicationService(
            Lazy<IApplicationServiceCommonService> applicationServiceCommonService,
            Lazy<IAuditService> auditService,
            Lazy<IVisitPayUserJournalEventService> userJournalEventService,
            Lazy<IEventJournalService> eventJournalService,
            Lazy<IEventJournalQueryService> eventJournalQueryService,
            Lazy<VisitPayUserService> visitPayUserService) : base(applicationServiceCommonService)
        {
            this._auditService = auditService;
            this._userJournalEventService = userJournalEventService;
            this._eventJournalService = eventJournalService;
            this._eventJournalQueryService = eventJournalQueryService;
            this._visitPayUserService = visitPayUserService;
        }

        private const string EndDate = "EndDate";
        private const string BeginDate = "BeginDate";
        private const string Notes = "Notes";
        private const string AuditUsers = "AuditUsers";
        private const string AuditPciLogs = "AuditPciLogs";

        private readonly Dictionary<JournalEventTypeEnum, string> _journalEventTypeLookup = new Dictionary<JournalEventTypeEnum, string>()
        {
            [JournalEventTypeEnum.AuditIvinciAuditPciLogs] = AuditPciLogs,
            [JournalEventTypeEnum.AuditClientAuditPciLogs] = AuditPciLogs,
            [JournalEventTypeEnum.AuditClientUserAudit] = AuditUsers,
            [JournalEventTypeEnum.AuditIvinciUserAudit] = AuditUsers
        };

        #region Export

        public byte[] ExportUserAccessLogs(AuditFilterDto auditFilter)
        {
            const int maxExportRecords = 10000;
            IList<AuditUserAccess> results = this._auditService.Value.GetUserAccessLogsSince(Mapper.Map<AuditFilter>(auditFilter));

            using (ExcelPackage package = new ExcelPackage())
            {
                ExcelWorksheet worksheet = package.Workbook.Worksheets.Add("User Access Logs");

                // title
                worksheet.Cells["A1"].Value = string.Format("User Access logs from {0} to {1}", auditFilter.StartDate.ToString("G"), auditFilter.EndDate.HasValue ? auditFilter.EndDate.Value.ToString("G") : DateTime.UtcNow.ToString("G"));
                worksheet.Cells["A1:G1"].Merge = true;
                using (ExcelRange range = worksheet.Cells["A1:G1"])
                {
                    range.Style.Font.Size = 13;
                    range.Style.Font.Bold = true;
                }

                // data rows
                if (results.Count > 0)
                {
                    // too many records
                    if (results.Count > maxExportRecords)
                    {
                        worksheet.Cells["A3"].Value = string.Format("Incomplete download. {0} rows of {1} rows were downloaded. Only {0} rows can be downloaded.", maxExportRecords.ToString("N0"), results.Count.ToString("N0"));
                        worksheet.Cells["A3:G3"].Merge = true;
                        using (ExcelRange range = worksheet.Cells["A3:G3"])
                        {
                            range.Style.Font.Italic = true;
                        }
                    }

                    int headerIndex = (results.Count > maxExportRecords) ? 5 : 3;

                    // data headers
                    worksheet.Cells[string.Format("A{0}", headerIndex)].Value = "User Name";
                    worksheet.Cells[string.Format("B{0}", headerIndex)].Value = "Email";
                    worksheet.Cells[string.Format("C{0}", headerIndex)].Value = "First Name";
                    worksheet.Cells[string.Format("D{0}", headerIndex)].Value = "Last Name";
                    worksheet.Cells[string.Format("E{0}", headerIndex)].Value = "Event Date";
                    worksheet.Cells[string.Format("F{0}", headerIndex)].Value = "Event Type";
                    worksheet.Cells[string.Format("G{0}", headerIndex)].Value = "IP Address";

                    using (ExcelRange range = worksheet.Cells[string.Format("A{0}:G{0}", headerIndex)])
                    {
                        range.Style.Font.Size = 11;
                        range.Style.Font.Bold = true;
                        range.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        range.Style.Border.BorderAround(ExcelBorderStyle.Medium);
                        range.Style.Border.Right.Style = ExcelBorderStyle.Medium;
                    }

                    int startRowIndex = headerIndex + 1;
                    int rowIndex = startRowIndex;

                    foreach (AuditUserAccess record in results)
                    {
                        worksheet.Cells[string.Format("A{0}", rowIndex)].Value = record.UserName;
                        worksheet.Cells[string.Format("B{0}", rowIndex)].Value = record.Email;
                        worksheet.Cells[string.Format("C{0}", rowIndex)].Value = record.FirstName;
                        worksheet.Cells[string.Format("D{0}", rowIndex)].Value = record.LastName;
                        worksheet.Cells[string.Format("E{0}", rowIndex)].Value = record.EventDate;
                        worksheet.Cells[string.Format("F{0}", rowIndex)].Value = record.EventType;
                        worksheet.Cells[string.Format("G{0}", rowIndex)].Value = record.IpAddress;

                        worksheet.Cells[string.Format("E{0}", rowIndex)].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        worksheet.Cells[string.Format("E{0}", rowIndex)].Style.Numberformat.Format = Format.DateTimeFormat;

                        rowIndex++;
                    }

                    // border
                    using (ExcelRange range = worksheet.Cells[string.Format("A{0}:G{1}", startRowIndex, rowIndex - 1)])
                    {
                        range.Style.Border.BorderAround(ExcelBorderStyle.Thin);
                        range.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                        range.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                    }
                }
                else
                {
                    worksheet.Cells["A3"].Value = "There are no audit records to display at this time.";
                    worksheet.Cells["A3:G3"].Merge = true;
                    using (ExcelRange range = worksheet.Cells["A3:G3"])
                    {
                        range.Style.Font.Size = 11;
                        range.Style.Font.Bold = true;
                    }
                }

                worksheet.Cells[worksheet.Dimension.Address].AutoFitColumns();

                return package.GetAsByteArray();
            }

        }

        public byte[] ExportUserChangeLogs(AuditFilterDto auditFilter)
        {
            const int maxExportRecords = 10000;
            IList<AuditUserChange> results = this._auditService.Value.GetUserChangeLogsSince(Mapper.Map<AuditFilter>(auditFilter));

            using (ExcelPackage package = new ExcelPackage())
            {
                ExcelWorksheet worksheet = package.Workbook.Worksheets.Add("User Change Logs");

                // title
                worksheet.Cells["A1"].Value = string.Format("User Change logs from {0} to {1}", auditFilter.StartDate.ToString("G"), auditFilter.EndDate.HasValue ? auditFilter.EndDate.Value.ToString("G") : DateTime.UtcNow.ToString("G"));
                worksheet.Cells["A1:I1"].Merge = true;
                using (ExcelRange range = worksheet.Cells["A1:I1"])
                {
                    range.Style.Font.Size = 13;
                    range.Style.Font.Bold = true;
                }

                // data rows
                if (results.Count > 0)
                {
                    // too many records
                    if (results.Count > maxExportRecords)
                    {
                        worksheet.Cells["A3"].Value = string.Format("Incomplete download. {0} rows of {1} rows were downloaded. Only {0} rows can be downloaded.", maxExportRecords.ToString("N0"), results.Count.ToString("N0"));
                        worksheet.Cells["A3:I3"].Merge = true;
                        using (ExcelRange range = worksheet.Cells["A3:I3"])
                        {
                            range.Style.Font.Italic = true;
                        }
                    }

                    int headerIndex = (results.Count > maxExportRecords) ? 5 : 3;

                    // data headers
                    worksheet.Cells[string.Format("A{0}", headerIndex)].Value = "User Name";
                    worksheet.Cells[string.Format("B{0}", headerIndex)].Value = "Email";
                    worksheet.Cells[string.Format("C{0}", headerIndex)].Value = "First Name";
                    worksheet.Cells[string.Format("D{0}", headerIndex)].Value = "Last Name";
                    worksheet.Cells[string.Format("E{0}", headerIndex)].Value = "Event Date";
                    worksheet.Cells[string.Format("F{0}", headerIndex)].Value = "Event Type";
                    worksheet.Cells[string.Format("G{0}", headerIndex)].Value = "IP Address";
                    worksheet.Cells[string.Format("H{0}", headerIndex)].Value = "Target User Name";
                    worksheet.Cells[string.Format("I{0}", headerIndex)].Value = "Target VP User ID";

                    using (ExcelRange range = worksheet.Cells[string.Format("A{0}:I{0}", headerIndex)])
                    {
                        range.Style.Font.Size = 11;
                        range.Style.Font.Bold = true;
                        range.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        range.Style.Border.BorderAround(ExcelBorderStyle.Medium);
                        range.Style.Border.Right.Style = ExcelBorderStyle.Medium;
                    }

                    int startRowIndex = headerIndex + 1;
                    int rowIndex = startRowIndex;

                    foreach (AuditUserChange record in results)
                    {
                        worksheet.Cells[string.Format("A{0}", rowIndex)].Value = record.UserName;
                        worksheet.Cells[string.Format("B{0}", rowIndex)].Value = record.Email;
                        worksheet.Cells[string.Format("C{0}", rowIndex)].Value = record.FirstName;
                        worksheet.Cells[string.Format("D{0}", rowIndex)].Value = record.LastName;
                        worksheet.Cells[string.Format("E{0}", rowIndex)].Value = record.EventDate;
                        worksheet.Cells[string.Format("F{0}", rowIndex)].Value = record.EventType;
                        worksheet.Cells[string.Format("G{0}", rowIndex)].Value = record.IpAddress;
                        worksheet.Cells[string.Format("H{0}", rowIndex)].Value = record.TargetUserName;
                        worksheet.Cells[string.Format("I{0}", rowIndex)].Value = record.TargetVisitPayUserId;

                        worksheet.Cells[string.Format("E{0}", rowIndex)].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        worksheet.Cells[string.Format("E{0}", rowIndex)].Style.Numberformat.Format = Format.DateTimeFormat;

                        rowIndex++;
                    }

                    // border
                    using (ExcelRange range = worksheet.Cells[string.Format("A{0}:I{1}", startRowIndex, rowIndex - 1)])
                    {
                        range.Style.Border.BorderAround(ExcelBorderStyle.Thin);
                        range.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                        range.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                    }
                }
                else
                {
                    worksheet.Cells["A3"].Value = "There are no audit records to display at this time.";
                    worksheet.Cells["A3:I3"].Merge = true;
                    using (ExcelRange range = worksheet.Cells["A3:I3"])
                    {
                        range.Style.Font.Size = 11;
                        range.Style.Font.Bold = true;
                    }
                }

                worksheet.Cells[worksheet.Dimension.Address].AutoFitColumns();

                return package.GetAsByteArray();
            }

        }

        public byte[] ExportUserChangeDetailLogs(AuditFilterDto auditFilter)
        {
            const int maxExportRecords = 10000;
            IList<AuditUserChangeDetail> results = this._auditService.Value.GetUserChangeDetailLogsSince(Mapper.Map<AuditFilter>(auditFilter));

            using (ExcelPackage package = new ExcelPackage())
            {
                ExcelWorksheet worksheet = package.Workbook.Worksheets.Add("User Change Detail Logs");

                // title
                worksheet.Cells["A1"].Value = string.Format("User Change Detail logs from {0} to {1}", auditFilter.StartDate.ToString("G"), auditFilter.EndDate.HasValue ? auditFilter.EndDate.Value.ToString("G") : DateTime.UtcNow.ToString("G"));
                worksheet.Cells["A1:O1"].Merge = true;
                using (ExcelRange range = worksheet.Cells["A1:O1"])
                {
                    range.Style.Font.Size = 13;
                    range.Style.Font.Bold = true;
                }

                // data rows
                if (results.Count > 0)
                {
                    // too many records
                    if (results.Count > maxExportRecords)
                    {
                        worksheet.Cells["A3"].Value = string.Format("Incomplete download. {0} rows of {1} rows were downloaded. Only {0} rows can be downloaded.", maxExportRecords.ToString("N0"), results.Count.ToString("N0"));
                        worksheet.Cells["A3:O3"].Merge = true;
                        using (ExcelRange range = worksheet.Cells["A3:O3"])
                        {
                            range.Style.Font.Italic = true;
                        }
                    }

                    int headerIndex = (results.Count > maxExportRecords) ? 5 : 3;

                    // data headers
                    worksheet.Cells[string.Format("A{0}", headerIndex)].Value = "Change Time";
                    worksheet.Cells[string.Format("B{0}", headerIndex)].Value = "Expired Time";
                    worksheet.Cells[string.Format("C{0}", headerIndex)].Value = "VP User ID";
                    worksheet.Cells[string.Format("D{0}", headerIndex)].Value = "Prior User Name";
                    worksheet.Cells[string.Format("E{0}", headerIndex)].Value = "User Name";
                    worksheet.Cells[string.Format("F{0}", headerIndex)].Value = "Prior Email";
                    worksheet.Cells[string.Format("G{0}", headerIndex)].Value = "Email";
                    worksheet.Cells[string.Format("H{0}", headerIndex)].Value = "Prior First Name";
                    worksheet.Cells[string.Format("I{0}", headerIndex)].Value = "First Name";
                    worksheet.Cells[string.Format("J{0}", headerIndex)].Value = "Prior Last Name";
                    worksheet.Cells[string.Format("K{0}", headerIndex)].Value = "Last Name";
                    worksheet.Cells[string.Format("L{0}", headerIndex)].Value = "Prior Lockout Enabled";
                    worksheet.Cells[string.Format("M{0}", headerIndex)].Value = "Lockout Enabled";
                    worksheet.Cells[string.Format("N{0}", headerIndex)].Value = "Prior Lockout End Date UTC";
                    worksheet.Cells[string.Format("O{0}", headerIndex)].Value = "Lockout End DAte UTC";

                    using (ExcelRange range = worksheet.Cells[string.Format("A{0}:O{0}", headerIndex)])
                    {
                        range.Style.Font.Size = 11;
                        range.Style.Font.Bold = true;
                        range.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        range.Style.Border.BorderAround(ExcelBorderStyle.Medium);
                        range.Style.Border.Right.Style = ExcelBorderStyle.Medium;
                    }

                    int startRowIndex = headerIndex + 1;
                    int rowIndex = startRowIndex;

                    foreach (AuditUserChangeDetail record in results)
                    {
                        worksheet.Cells[string.Format("A{0}", rowIndex)].Value = record.ChangeTime;
                        worksheet.Cells[string.Format("B{0}", rowIndex)].Value = record.ExpiredTime;
                        worksheet.Cells[string.Format("C{0}", rowIndex)].Value = record.VisitPayUserId;
                        worksheet.Cells[string.Format("D{0}", rowIndex)].Value = record.PriorUserName;
                        worksheet.Cells[string.Format("E{0}", rowIndex)].Value = record.UserName;
                        worksheet.Cells[string.Format("F{0}", rowIndex)].Value = record.PriorEmail;
                        worksheet.Cells[string.Format("G{0}", rowIndex)].Value = record.Email;
                        worksheet.Cells[string.Format("H{0}", rowIndex)].Value = record.PriorFirstName;
                        worksheet.Cells[string.Format("I{0}", rowIndex)].Value = record.FirstName;
                        worksheet.Cells[string.Format("J{0}", rowIndex)].Value = record.PriorLastName;
                        worksheet.Cells[string.Format("K{0}", rowIndex)].Value = record.LastName;
                        worksheet.Cells[string.Format("L{0}", rowIndex)].Value = record.PriorLockoutEnabled;
                        worksheet.Cells[string.Format("M{0}", rowIndex)].Value = record.LockoutEnabled;
                        worksheet.Cells[string.Format("N{0}", rowIndex)].Value = record.PriorLockoutEndDateUtc;
                        worksheet.Cells[string.Format("O{0}", rowIndex)].Value = record.LockoutEndDateUtc;

                        worksheet.Cells[string.Format("A{0}", rowIndex)].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        worksheet.Cells[string.Format("A{0}", rowIndex)].Style.Numberformat.Format = Format.DateTimeFormat;
                        worksheet.Cells[string.Format("B{0}", rowIndex)].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        worksheet.Cells[string.Format("B{0}", rowIndex)].Style.Numberformat.Format = Format.DateTimeFormat;
                        worksheet.Cells[string.Format("N{0}", rowIndex)].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        worksheet.Cells[string.Format("N{0}", rowIndex)].Style.Numberformat.Format = Format.DateTimeFormat;
                        worksheet.Cells[string.Format("O{0}", rowIndex)].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        worksheet.Cells[string.Format("O{0}", rowIndex)].Style.Numberformat.Format = Format.DateTimeFormat;

                        rowIndex++;
                    }

                    // border
                    using (ExcelRange range = worksheet.Cells[string.Format("A{0}:O{1}", startRowIndex, rowIndex - 1)])
                    {
                        range.Style.Border.BorderAround(ExcelBorderStyle.Thin);
                        range.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                        range.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                    }
                }
                else
                {
                    worksheet.Cells["A3"].Value = "There are no audit records to display at this time.";
                    worksheet.Cells["A3:O3"].Merge = true;
                    using (ExcelRange range = worksheet.Cells["A3:O3"])
                    {
                        range.Style.Font.Size = 11;
                        range.Style.Font.Bold = true;
                    }
                }

                worksheet.Cells[worksheet.Dimension.Address].AutoFitColumns();

                return package.GetAsByteArray();
            }

        }

        public byte[] ExportRoleChangeLogs(AuditFilterDto auditFilter)
        {
            const int maxExportRecords = 10000;
            IList<AuditRoleChange> results = this._auditService.Value.GetRoleChangeLogsSince(Mapper.Map<AuditFilter>(auditFilter));

            using (ExcelPackage package = new ExcelPackage())
            {
                ExcelWorksheet worksheet = package.Workbook.Worksheets.Add("Role Change Logs");

                // title
                worksheet.Cells["A1"].Value = string.Format("Role Change logs from {0} to {1}", auditFilter.StartDate.ToString("G"), auditFilter.EndDate.HasValue ? auditFilter.EndDate.Value.ToString("G") : DateTime.UtcNow.ToString("G"));
                worksheet.Cells["A1:G1"].Merge = true;
                using (ExcelRange range = worksheet.Cells["A1:G1"])
                {
                    range.Style.Font.Size = 13;
                    range.Style.Font.Bold = true;
                }

                // data rows
                if (results.Count > 0)
                {
                    // too many records
                    if (results.Count > maxExportRecords)
                    {
                        worksheet.Cells["A3"].Value = string.Format("Incomplete download. {0} rows of {1} rows were downloaded. Only {0} rows can be downloaded.", maxExportRecords.ToString("N0"), results.Count.ToString("N0"));
                        worksheet.Cells["A3:G3"].Merge = true;
                        using (ExcelRange range = worksheet.Cells["A3:G3"])
                        {
                            range.Style.Font.Italic = true;
                        }
                    }

                    int headerIndex = (results.Count > maxExportRecords) ? 5 : 3;

                    // data headers
                    worksheet.Cells[string.Format("A{0}", headerIndex)].Value = "VP User ID";
                    worksheet.Cells[string.Format("B{0}", headerIndex)].Value = "User Name";
                    worksheet.Cells[string.Format("C{0}", headerIndex)].Value = "Visit Pay Role ID";
                    worksheet.Cells[string.Format("D{0}", headerIndex)].Value = "Change Event ID";
                    worksheet.Cells[string.Format("E{0}", headerIndex)].Value = "Change Time";
                    worksheet.Cells[string.Format("F{0}", headerIndex)].Value = "Expired Time";
                    worksheet.Cells[string.Format("G{0}", headerIndex)].Value = "Prior Delete";

                    using (ExcelRange range = worksheet.Cells[string.Format("A{0}:G{0}", headerIndex)])
                    {
                        range.Style.Font.Size = 11;
                        range.Style.Font.Bold = true;
                        range.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        range.Style.Border.BorderAround(ExcelBorderStyle.Medium);
                        range.Style.Border.Right.Style = ExcelBorderStyle.Medium;
                    }

                    int startRowIndex = headerIndex + 1;
                    int rowIndex = startRowIndex;

                    foreach (AuditRoleChange record in results)
                    {
                        worksheet.Cells[string.Format("A{0}", rowIndex)].Value = record.VisitPayUserId;
                        worksheet.Cells[string.Format("B{0}", rowIndex)].Value = record.UserName;
                        worksheet.Cells[string.Format("C{0}", rowIndex)].Value = record.VisitPayRoleId;
                        worksheet.Cells[string.Format("D{0}", rowIndex)].Value = record.ChangeEventId;
                        worksheet.Cells[string.Format("E{0}", rowIndex)].Value = record.ChangeTime;
                        worksheet.Cells[string.Format("F{0}", rowIndex)].Value = record.ExpiredTime;
                        worksheet.Cells[string.Format("G{0}", rowIndex)].Value = record.PriorDelete;

                        worksheet.Cells[string.Format("E{0}", rowIndex)].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        worksheet.Cells[string.Format("E{0}", rowIndex)].Style.Numberformat.Format = Format.DateTimeFormat;
                        worksheet.Cells[string.Format("F{0}", rowIndex)].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        worksheet.Cells[string.Format("F{0}", rowIndex)].Style.Numberformat.Format = Format.DateTimeFormat;
                        worksheet.Cells[string.Format("G{0}", rowIndex)].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        worksheet.Cells[string.Format("G{0}", rowIndex)].Style.Numberformat.Format = Format.DateTimeFormat;

                        rowIndex++;
                    }

                    // border
                    using (ExcelRange range = worksheet.Cells[string.Format("A{0}:G{1}", startRowIndex, rowIndex - 1)])
                    {
                        range.Style.Border.BorderAround(ExcelBorderStyle.Thin);
                        range.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                        range.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                    }
                }
                else
                {
                    worksheet.Cells["A3"].Value = "There are no audit records to display at this time.";
                    worksheet.Cells["A3:G3"].Merge = true;
                    using (ExcelRange range = worksheet.Cells["A3:G3"])
                    {
                        range.Style.Font.Size = 11;
                        range.Style.Font.Bold = true;
                    }
                }

                worksheet.Cells[worksheet.Dimension.Address].AutoFitColumns();

                return package.GetAsByteArray();
            }
        }

        public byte[] ExportAuditLogs(AuditLogFilterDto auditLogFilter)
        {
            const int maxExportRecords = 10000;
            IList<AuditLogDto> results = this.GetAuditLogs(auditLogFilter);

            using (ExcelPackage package = new ExcelPackage())
            {
                ExcelWorksheet worksheet = package.Workbook.Worksheets.Add("Audit Logs");

                // title
                worksheet.Cells["A1"].Value = string.Format("Audit logs from {0} to {1}", auditLogFilter.StartDate.ToString("G"), auditLogFilter.EndDate.HasValue ? auditLogFilter.EndDate.Value.ToString("G") : DateTime.UtcNow.ToString("G"));
                worksheet.Cells["A1:G1"].Merge = true;
                using (ExcelRange range = worksheet.Cells["A1:G1"])
                {
                    range.Style.Font.Size = 13;
                    range.Style.Font.Bold = true;
                }

                // data rows
                if (results.Count > 0)
                {
                    // too many records
                    if (results.Count > maxExportRecords)
                    {
                        worksheet.Cells["A3"].Value = string.Format("Incomplete download. {0} rows of {1} rows were downloaded. Only {0} rows can be downloaded.", maxExportRecords.ToString("N0"), results.Count.ToString("N0"));
                        worksheet.Cells["A3:G3"].Merge = true;
                        using (ExcelRange range = worksheet.Cells["A3:G3"])
                        {
                            range.Style.Font.Italic = true;
                        }
                    }

                    int headerIndex = (results.Count > maxExportRecords) ? 5 : 3;

                    // data headers
                    worksheet.Cells[string.Format("A{0}", headerIndex)].Value = "Audit Date";
                    worksheet.Cells[string.Format("B{0}", headerIndex)].Value = "User ID";
                    worksheet.Cells[string.Format("C{0}", headerIndex)].Value = "User Name";
                    worksheet.Cells[string.Format("D{0}", headerIndex)].Value = "Event Type";
                    worksheet.Cells[string.Format("E{0}", headerIndex)].Value = "Audit Type";
                    worksheet.Cells[string.Format("F{0}", headerIndex)].Value = "Audit Period";
                    worksheet.Cells[string.Format("G{0}", headerIndex)].Value = "Notes";

                    using (ExcelRange range = worksheet.Cells[string.Format("A{0}:G{0}", headerIndex)])
                    {
                        range.Style.Font.Size = 11;
                        range.Style.Font.Bold = true;
                        range.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        range.Style.Border.BorderAround(ExcelBorderStyle.Medium);
                        range.Style.Border.Right.Style = ExcelBorderStyle.Medium;
                    }

                    int startRowIndex = headerIndex + 1;
                    int rowIndex = startRowIndex;

                    foreach (AuditLogDto record in results)
                    {
                        worksheet.Cells[string.Format("A{0}", rowIndex)].Value = record.AuditDate;
                        worksheet.Cells[string.Format("B{0}", rowIndex)].Value = record.VisitPayUserId;
                        worksheet.Cells[string.Format("C{0}", rowIndex)].Value = record.UserName;
                        worksheet.Cells[string.Format("D{0}", rowIndex)].Value = record.EventType;
                        worksheet.Cells[string.Format("E{0}", rowIndex)].Value = record.AuditType;
                        worksheet.Cells[string.Format("F{0}", rowIndex)].Value = record.AuditPeriod;
                        worksheet.Cells[string.Format("G{0}", rowIndex)].Value = record.Notes;

                        worksheet.Cells[string.Format("A{0}", rowIndex)].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        worksheet.Cells[string.Format("A{0}", rowIndex)].Style.Numberformat.Format = Format.DateTimeFormat;

                        rowIndex++;
                    }

                    // border
                    using (ExcelRange range = worksheet.Cells[string.Format("A{0}:G{1}", startRowIndex, rowIndex - 1)])
                    {
                        range.Style.Border.BorderAround(ExcelBorderStyle.Thin);
                        range.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                        range.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                    }
                }
                else
                {
                    worksheet.Cells["A3"].Value = "There are no audit records to display at this time.";
                    worksheet.Cells["A3:G3"].Merge = true;
                    using (ExcelRange range = worksheet.Cells["A3:G3"])
                    {
                        range.Style.Font.Size = 11;
                        range.Style.Font.Bold = true;
                    }
                }

                worksheet.Cells[worksheet.Dimension.Address].AutoFitColumns();

                return package.GetAsByteArray();
            }

        }

        #endregion

        #region Logging

        public void LogViewPciLog(int visitPayUserId, JournalEventHttpContextDto context)
        {
            VisitPayUserContexts visitPayUserContexts = this.GetVisitPayUserContexts(visitPayUserId, context);

            this._eventJournalService.Value.AddEvent<EventAuditViewPciLogs, JournalEventParameters>(
                EventAuditViewPciLogs.GetParameters
                (
                    eventUserName: visitPayUserContexts.VisitPayUser.UserName
                ),
                visitPayUserContexts.JournalEventUserContext,
                visitPayUserContexts.JournalEventContext);
        }

        public void LogConfirmAudit(int visitPayUserId, bool hasIvhPciAuditorRole, bool hasClientPciAuditorRole, DateTime beginDate, DateTime endDate, string notes, JournalEventHttpContextDto context)
        {
            VisitPayUserContexts visitPayUserContexts = this.GetVisitPayUserContexts(visitPayUserId, context);

            if (hasIvhPciAuditorRole)
            {
                this._eventJournalService.Value.AddEvent<EventAuditIvinciAuditPciLogs, JournalEventParameters>(
                    EventAuditIvinciAuditPciLogs.GetParameters
                    (
                        beginDate: beginDate.ToString("G"),
                        endDate: endDate.ToString("G"),
                        eventUserName: visitPayUserContexts.VisitPayUser.UserName,
                        notes: notes.Replace(':', ' ').Replace('|', ' ')
                    ),
                    visitPayUserContexts.JournalEventUserContext,
                    visitPayUserContexts.JournalEventContext);
            }

            if (hasClientPciAuditorRole)
            {
                this._eventJournalService.Value.AddEvent<EventAuditClientAuditPciLogs, JournalEventParameters>(
                    EventAuditClientAuditPciLogs.GetParameters
                    (
                        beginDate: beginDate.ToString("G"),
                        endDate: endDate.ToString("G"),
                        eventUserName: visitPayUserContexts.VisitPayUser.UserName,
                        notes: notes.Replace(':', ' ').Replace('|', ' ')
                    ),
                    visitPayUserContexts.JournalEventUserContext,
                    visitPayUserContexts.JournalEventContext);
            }
        }

        public void LogUserManagementAudit(int visitPayUserId, bool hasIvhUserAuditorRole, bool hasClientUserAuditorRole, string notes, JournalEventHttpContextDto context)
        {
            VisitPayUserContexts visitPayUserContexts = this.GetVisitPayUserContexts(visitPayUserId, context);

            if (hasIvhUserAuditorRole)
            {
                this._eventJournalService.Value.AddEvent<EventAuditIvinciUserAudit, JournalEventParameters>(
                    EventAuditIvinciUserAudit.GetParameters
                    (
                        eventUserName: visitPayUserContexts.VisitPayUser.UserName,
                        notes: notes.Replace(':', ' ').Replace('|', ' ')
                    ),
                    visitPayUserContexts.JournalEventUserContext,
                    visitPayUserContexts.JournalEventContext);
            }

            if (hasClientUserAuditorRole)
            {
                this._eventJournalService.Value.AddEvent<EventAuditClientUserAudit, JournalEventParameters>(
                    EventAuditClientUserAudit.GetParameters
                    (
                        eventUserName: visitPayUserContexts.VisitPayUser.UserName,
                        notes: notes.Replace(':', ' ').Replace('|', ' ')
                    ),
                    visitPayUserContexts.JournalEventUserContext,
                    visitPayUserContexts.JournalEventContext);
            }
        }

        private JournalEventContext GetJournalEventContextForAudit(int visitPayUserId)
        {
            return new JournalEventContext() { VisitPayUserId = visitPayUserId };
        }

        private VisitPayUserContexts GetVisitPayUserContexts(int visitPayUserId, JournalEventHttpContextDto context)
        {
            VisitPayUser visitPayUser = this._visitPayUserService.Value.FindByIdAsync(visitPayUserId.ToString()).Result;
            VisitPayUserContexts visitPayUserContexts = new VisitPayUserContexts
            {
                JournalEventContext = this.GetJournalEventContextForAudit(visitPayUserId),
                JournalEventUserContext = this._userJournalEventService.Value.GetJournalEventUserContext(context, visitPayUser),
                VisitPayUser = visitPayUser
            };
            return visitPayUserContexts;
        }

        private class VisitPayUserContexts
        {
            public JournalEventContext JournalEventContext { get; set; }
            public JournalEventUserContext JournalEventUserContext { get; set; }
            public VisitPayUser VisitPayUser { get; set; }
        }


        #endregion

        public IList<AuditUserAccessDto> GetUserAccessLogsSince(AuditFilterDto auditFilter)
        {
            return Mapper.Map<IList<AuditUserAccessDto>>(this._auditService.Value.GetUserAccessLogsSince(Mapper.Map<AuditFilter>(auditFilter)));
        }

        public IList<AuditUserChangeDto> GetUserChangeLogsSince(AuditFilterDto auditFilter)
        {
            return Mapper.Map<IList<AuditUserChangeDto>>(this._auditService.Value.GetUserChangeLogsSince(Mapper.Map<AuditFilter>(auditFilter)));
        }

        public IList<AuditUserChangeDetailDto> GetUserChangeDetailLogsSince(AuditFilterDto auditFilter)
        {
            return Mapper.Map<IList<AuditUserChangeDetailDto>>(this._auditService.Value.GetUserChangeDetailLogsSince(Mapper.Map<AuditFilter>(auditFilter)));
        }

        public IList<AuditRoleChangeDto> GetRoleChangeLogsSince(AuditFilterDto auditFilter)
        {
            return Mapper.Map<IList<AuditRoleChangeDto>>(this._auditService.Value.GetRoleChangeLogsSince(Mapper.Map<AuditFilter>(auditFilter)));
        }

        public IList<AuditLogDto> GetAuditLogs(AuditLogFilterDto auditLogFilter)
        {
            try
            {
                JournalEventFilter userEventHistoryFilter = Mapper.Map<JournalEventFilter>(auditLogFilter);
                userEventHistoryFilter.JournalEventTypes = this._journalEventTypeLookup.Keys.ToList();
                IEnumerable<AuditLogDto> results = this._eventJournalQueryService.Value.GetJournalEvents(userEventHistoryFilter).JournalEvents.Select(je =>
                {
                    AuditLogDto log = new AuditLogDto
                    {
                        AuditDate = je.EventDateTime,
                        EventType = je.JournalEventType,
                        AuditType = this.GetAuditType(je.JournalEventType),
                        UserName = je.EventVisitPayUser.UserName,
                        VisitPayUserId = je.VisitPayUserId ?? 0
                    };
                    IDictionary<string, string> additionalData = je.AdditionalData.AsAdditionalDataDictionary();
                    if (additionalData != null)
                    {
                        if (additionalData.ContainsKey(BeginDate))
                        {
                            log.BeginDate = new DateTime(long.Parse(additionalData[BeginDate]));
                        }
                        if (additionalData.ContainsKey(EndDate))
                        {
                            log.EndDate = new DateTime(long.Parse(additionalData[EndDate]));
                        }
                        if (additionalData.ContainsKey(Notes))
                        {
                            log.Notes = additionalData[Notes];
                        }
                    }
                    return log;
                });
                bool isAscendingOrder = "asc".Equals(auditLogFilter.SortOrder, StringComparison.InvariantCultureIgnoreCase);

                switch (auditLogFilter.SortField ?? string.Empty)
                {
                    case "AuditDate":
                        results = isAscendingOrder ? results.OrderBy(r => r.AuditDate) : results.OrderByDescending(r => r.AuditDate);
                        break;
                    case "VisitPayUserId":
                        results = isAscendingOrder ? results.OrderBy(r => r.VisitPayUserId) : results.OrderByDescending(r => r.VisitPayUserId);
                        break;
                    case "UserName":
                        results = isAscendingOrder ? results.OrderBy(r => r.UserName) : results.OrderByDescending(r => r.UserName);
                        break;
                    case "EventType":
                        results = isAscendingOrder ? results.OrderBy(r => r.EventType) : results.OrderByDescending(r => r.EventType);
                        break;
                    case "AuditType":
                        results = isAscendingOrder ? results.OrderBy(r => r.AuditType) : results.OrderByDescending(r => r.AuditType);
                        break;
                    case "AuditPeriod":
                        results = isAscendingOrder ? results.OrderBy(r => r.BeginDate) : results.OrderByDescending(r => r.BeginDate);
                        break;
                }

                return results.ToList();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
            return new List<AuditLogDto>();
        }

        public DateTime? LastIvhPciAudit()
        {
            return this.LastPciAudit(JournalEventTypeEnum.AuditIvinciAuditPciLogs);
        }

        public DateTime? LastClientPciAudit()
        {
            return this.LastPciAudit(JournalEventTypeEnum.AuditClientAuditPciLogs);
        }

        private DateTime? GetJournalEventLastDateTime(JournalEventTypeEnum journalEventType)
        {
            JournalEvent journalEvent = this._eventJournalQueryService.Value.GetLastEventOfType(journalEventType);
            return journalEvent?.EventDateTime;
        }

        public DateTime? LastIvhUserAudit()
        {
            return this.GetJournalEventLastDateTime(JournalEventTypeEnum.AuditIvinciUserAudit);
        }

        public DateTime? LastClientUserAudit()
        {
            return this.GetJournalEventLastDateTime(JournalEventTypeEnum.AuditClientUserAudit);
        }

        private DateTime? LastPciAudit(JournalEventTypeEnum eventType)
        {
            JournalEvent userEvent = this._eventJournalQueryService.Value.GetLastEventOfType(eventType);
            if (userEvent != null && !string.IsNullOrWhiteSpace(userEvent.AdditionalData))
            {
                IDictionary<string, string> additionalData = userEvent.AdditionalData.AsAdditionalDataDictionary();
                if (additionalData.ContainsKey(EndDate))
                {
                    return new DateTime(long.Parse(additionalData[EndDate]));
                }
            }
            return null;
        }

        private string GetAuditType(JournalEventTypeEnum journalEventTypeEnum)
        {
            if (this._journalEventTypeLookup.ContainsKey(journalEventTypeEnum))
            {
                return this._journalEventTypeLookup[journalEventTypeEnum];
            }
            return AuditUsers;
        }
    }
}
﻿namespace Ivh.Application.Core
{
    using Ivh.Application.Core.Common.Interfaces;
    using Ivh.Common.VisitPay.Enums;
    using Ivh.Domain.Core.ThirdPartyKey.Interfaces;
    using System.Threading.Tasks;

    public class ThirdPartyKeyApplicationService : IThirdPartyKeyApplicationService
    {
        private readonly IThirdPartyKeyService _thirdPartyKeyService;

        public ThirdPartyKeyApplicationService(IThirdPartyKeyService thirdPartyKeyService)
        {
            this._thirdPartyKeyService = thirdPartyKeyService;
        }

        public bool IsCardReaderDeviceKeyAllowed(string cardReaderDeviceKey)
        {
            return this._thirdPartyKeyService.IsCardReaderDeviceKeyAllowed(cardReaderDeviceKey);
        }

        public bool IsCardReaderUserAccountAllowed(string cardReaderUserAccount)
        {
            return this._thirdPartyKeyService.IsCardReaderUserAccountAllowed(cardReaderUserAccount);
        }
    }
}

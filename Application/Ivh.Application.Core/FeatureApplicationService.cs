﻿namespace Ivh.Application.Core
{
    using System;
    using Common.Interfaces;
    using Domain.Settings.Interfaces;
    using Ivh.Common.Base.Enums;
    using Ivh.Common.VisitPay.Enums;

    public class FeatureApplicationService : IFeatureApplicationService
    {
        private readonly Lazy<IFeatureService> _featureService;

        public FeatureApplicationService(Lazy<IFeatureService> featureService)
        {
            this._featureService = featureService;
        }

        public bool IsFeatureEnabled(VisitPayFeatureEnum visitPayFeature)
        {
            return this._featureService.Value.IsFeatureEnabled(visitPayFeature);
        }
    }
}
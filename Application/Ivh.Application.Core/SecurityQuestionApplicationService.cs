﻿namespace Ivh.Application.Core
{
    using System;
    using AutoMapper;
    using System.Collections.Generic;
    using Base.Common.Interfaces;
    using Base.Services;
    using Domain.User.Interfaces;
    using Ivh.Application.Core.Common.Interfaces;
    using User.Common.Dtos;

    public class SecurityQuestionApplicationService : ApplicationService, ISecurityQuestionApplicationService
    {
        private readonly Lazy<ISecurityQuestionService> _securityQuestionService;
        private readonly Lazy<IVisitPayUserApplicationService> _visitPayUserApplicationService;

        public SecurityQuestionApplicationService(
            Lazy<IApplicationServiceCommonService> applicationServiceCommonServices,
            Lazy<ISecurityQuestionService> securityQuestionService,
            Lazy<IVisitPayUserApplicationService> visitPayUserApplicationService) : base(applicationServiceCommonServices)
        {
            this._securityQuestionService = securityQuestionService;
            this._visitPayUserApplicationService = visitPayUserApplicationService;
        }

        public IList<SecurityQuestionDto> GetAllSecurityQuestions()
        {
            return Mapper.Map<IList<SecurityQuestionDto>>(this._securityQuestionService.Value.GetAllSecurityQuestions());
        }

        public IList<SecurityQuestionAnswerDto> GetUserSecurityQuestionAnswer(int visitPayUserId)
        {
            var user = this._visitPayUserApplicationService.Value.FindById(visitPayUserId.ToString());
            if (user != null)
            {
                return Mapper.Map<IList<SecurityQuestionAnswerDto>>(user.SecurityQuestionAnswers);
            }
            return null;
        }
    }
}

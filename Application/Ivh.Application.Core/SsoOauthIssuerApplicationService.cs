namespace Ivh.Application.Core
{
    using System;
    using AutoMapper;
    using Common.Dtos;
    using Common.Interfaces;
    using Domain.Core.Sso.Interfaces;
    using Domain.User.Entities;
    using User.Common.Dtos;

    public class SsoOauthIssuerApplicationService : ISsoOauthIssuerApplicationService
    {
        private readonly Lazy<ISsoOauthIssuerService> _ssoOauthIssuerService;

        public SsoOauthIssuerApplicationService(
            Lazy<ISsoOauthIssuerService> ssoOauthIssuerService)
        {
            this._ssoOauthIssuerService = ssoOauthIssuerService;
        }

        public SsoVisitPayUserOauthTokenDto CreateRefreshToken(VisitPayUserDto user, string ticket)
        {
            if (user == null) return null;
            var refreshToken =  this._ssoOauthIssuerService.Value.CreateRefreshToken(Mapper.Map<VisitPayUser>(user), ticket);
            return Mapper.Map<SsoVisitPayUserOauthTokenDto>(refreshToken);
        }

        public SsoVisitPayUserOauthTokenDto GetAccessTokenWithTokenString(string tokenString)
        {
            var accessToken = this._ssoOauthIssuerService.Value.GetAccessTokenWithTokenString(tokenString);
            return Mapper.Map<SsoVisitPayUserOauthTokenDto>(accessToken);
        }

        public SsoVisitPayUserOauthTokenDto CreateAccessToken(VisitPayUserDto user, string ticket)
        {
            if (user == null) return null;
            var refreshToken =  this._ssoOauthIssuerService.Value.CreateAccessToken(Mapper.Map<VisitPayUser>(user), ticket);
            return Mapper.Map<SsoVisitPayUserOauthTokenDto>(refreshToken);
        }

        public SsoVisitPayUserOauthTokenDto GetRefreshTokenWithRefreshTokenString(string refreshTokenString)
        {
            var refreshToken = this._ssoOauthIssuerService.Value.GetRefreshTokenWithRefreshTokenString(refreshTokenString);
            return Mapper.Map<SsoVisitPayUserOauthTokenDto>(refreshToken);
        }
    }
}
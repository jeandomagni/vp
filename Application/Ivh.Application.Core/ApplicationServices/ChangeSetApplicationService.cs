﻿namespace Ivh.Application.Core.ApplicationServices
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Base.Common.Interfaces;
    using Base.Services;
    using Common.Interfaces;
    using Domain.Guarantor.Entities;
    using Domain.Guarantor.Interfaces;
    using Domain.Visit.Entities;
    using Domain.Visit.Interfaces;
    using FinanceManagement.Common.Interfaces;
    using Ivh.Common.Base.Utilities.Extensions;
    using Ivh.Common.Base.Utilities.Helpers;
    using Ivh.Common.Data;
    using Ivh.Common.Data.Connection.Connections;
    using Ivh.Common.Data.Interfaces;
    using Ivh.Common.ServiceBus.Attributes;
    using Ivh.Common.ServiceBus.Enums;
    using Ivh.Common.VisitPay.Constants;
    using Ivh.Common.VisitPay.Enums;
    using Ivh.Common.VisitPay.Messages.FinanceManagement;
    using Ivh.Common.VisitPay.Messages.HospitalData;
    using Ivh.Common.VisitPay.Messages.HospitalData.Dtos;
    using MassTransit;
    using NHibernate;
    using User.Common.Dtos;

    public class ChangeSetApplicationService : ApplicationService, IChangeSetApplicationService
    {
        private readonly ISession _session;
        private readonly Lazy<IStatementApplicationService> _statementApplicationService;
        private readonly Lazy<IRedactionApplicationService> _redactionApplicationService;
        private readonly Lazy<IVisitService> _visitService;
        private readonly Lazy<IGuarantorService> _guarantorService;
        private readonly Lazy<IPaymentAllocationApplicationService> _paymentAllocationApplicationService;
        private readonly Lazy<IVisitCreationApplicationService> _visitCreationApplicationService;
        private readonly Lazy<IVisitStateApplicationService> _visitStateApplicationService;
        private readonly Lazy<IVisitBillingHoldApplicationService> _visitBillingHoldApplicationService;

        public ChangeSetApplicationService(
            Lazy<IApplicationServiceCommonService> applicationServiceCommonService,
            Lazy<IVisitCreationApplicationService> visitCreationApplicationService,
            Lazy<IVisitStateApplicationService> visitStateApplicationService,
            Lazy<IVisitBillingHoldApplicationService> visitBillingHoldApplicationService,
            Lazy<IRedactionApplicationService> redactionApplicationService,
            Lazy<IStatementApplicationService> statementApplicationService,
            Lazy<IVisitService> visitService,
            Lazy<IGuarantorService> guarantorService,
            Lazy<IPaymentAllocationApplicationService> paymentAllocationApplicationService,
            ISessionContext<VisitPay> sessionContext) : base(applicationServiceCommonService)
        {
            this._session = sessionContext.Session;
            this._visitCreationApplicationService = visitCreationApplicationService;
            this._visitStateApplicationService = visitStateApplicationService;
            this._visitBillingHoldApplicationService = visitBillingHoldApplicationService;
            this._redactionApplicationService = redactionApplicationService;
            this._statementApplicationService = statementApplicationService;
            this._visitService = visitService;
            this._guarantorService = guarantorService;
            this._paymentAllocationApplicationService = paymentAllocationApplicationService;
        }

        #region Message Consumers

        [UniversalConsumer(UniversalQueuesEnum.VisitPayProcessor, UniversalPriorityEnum.Priority20)]
        public void ConsumeMessage(ChangeSetAggregatedMessage changeSetAggregatedMessage, ConsumeContext<ChangeSetAggregatedMessage> consumeContext)
        {
            List<ChangeSetMessage> changeSetMessages = changeSetAggregatedMessage.Messages.ToList();

            foreach (ChangeSetMessage changeSetMessage in changeSetMessages)
            {
                this.ProcessChangeSetMessage(changeSetMessage);
            }

            if (changeSetMessages.Select(x => x.ChangeEvent).Any(x => x.ChangeEventType == ChangeEventTypeEnum.DataChange))
            {
                List<int> vpGuarantorIds = changeSetMessages.SelectMany(x => x.VpGuarantorHsMatches).Select(z => z.VpGuarantorId).Distinct().ToList();
                this.ProcessStatement(vpGuarantorIds);
            }
        }

        public bool IsMessageValid(ChangeSetAggregatedMessage message, ConsumeContext<ChangeSetAggregatedMessage> consumeContext)
        {
            bool isValid = true;
            return isValid;
        }

        [UniversalConsumer(UniversalQueuesEnum.VisitPayProcessor, UniversalPriorityEnum.Priority20)]
        public void ConsumeMessage(ChangeSetMessage changeSetMessage, ConsumeContext<ChangeSetMessage> consumeContext)
        {
            ChangeSetAggregatedMessage changeSetAggregatedMessage = new ChangeSetAggregatedMessage { Messages = changeSetMessage.ToListOfOne() };
            this.ConsumeMessage(changeSetAggregatedMessage, null);
        }

        public bool IsMessageValid(ChangeSetMessage message, ConsumeContext<ChangeSetMessage> consumeContext)
        {
            bool isValid = true;
            return isValid;
        }

        #endregion

        private void ProcessChangeSetMessage(ChangeSetMessage changeSetMessage)
        {
            List<int> visitIdsChanged = new List<int>();

            // Using try...catch...finally pattern so unitOfWork.Dispose() can successfully rollback transaction if it needs to
            Exception workException = null;
            UnitOfWork unitOfWork = new UnitOfWork(this._session);
            try
            {
                ChangeEventMessage changeEventMessage = changeSetMessage.ChangeEvent;

                switch (changeEventMessage.ChangeEventType)
                {
                    case ChangeEventTypeEnum.DataChange:
                        // GET THE INITIAL STATE OF ALL THE VISITS
                        List<Visit> initialVisits = changeSetMessage.VisitChanges.Select(x => this._visitService.Value.GetVisitBySystemSourceKey(x.SourceSystemKey, x.BillingSystemId)).Where(x => x != null).ToList();
                        CompareList<CompareVisit> visitCompareList = Compare<CompareVisit>.InitializeWith(initialVisits.Select(x => new CompareVisit(x)));
                        CompareList<CompareHsGuarantorMap> hsGuarantorCompareList = Compare<CompareHsGuarantorMap>.InitializeWith(changeSetMessage.HsGuarantorChanges.Select(x => this._guarantorService.Value.GetHsGuarantor(x.SourceSystemKey, x.BillingSystemId)).Where(x => x != null).Select(x => new CompareHsGuarantorMap(x)));

                        // MERGE THE CHANGES
                        visitIdsChanged.AddRange(this._visitCreationApplicationService.Value.MergeOrCreateVisitsFromVisitChangeSet(changeSetMessage));

                        // COMPARE TO THE POST MERGE STATE OF ALL THE VISITS
                        List<Visit> postMergeVisits = changeSetMessage.VisitChanges.Select(x => this._visitService.Value.GetVisitBySystemSourceKey(x.SourceSystemKey, x.BillingSystemId)).Where(x => x != null).ToList();
                        visitCompareList.CompareTo(postMergeVisits.Select(x => new CompareVisit(x)), x => x.VisitId);
                        hsGuarantorCompareList.CompareTo(changeSetMessage.HsGuarantorChanges.Select(x => this._guarantorService.Value.GetHsGuarantor(x.SourceSystemKey, x.BillingSystemId)).Where(x => x != null).Select(x => new CompareHsGuarantorMap(x)), x => x.HsGuarantorMapId);

                        // DO THE BILLINGHOLDS
                        foreach (CompareVisit visit in visitCompareList.HasChanged(x => x.BillingHold).Select(x => x.Modified))
                        {
                            if (visit.BillingHold)
                            {
                                this._visitBillingHoldApplicationService.Value.AddBillingHold(visit.BillingSystemId, visit.SourceSystemKey);
                            }
                            else
                            {
                                this._visitBillingHoldApplicationService.Value.ClearBillingHold(visit.BillingSystemId, visit.SourceSystemKey);
                            }
                        }

                        // DO THE ELIGIBLE/INELIGIBLE
                        foreach (CompareVisit visit in visitCompareList.HasChanged(x => x.VpEligible).Select(x => x.Modified))
                        {
                            if (visit.VpEligible)
                            {
                                this._visitStateApplicationService.Value.SetVisitEligible(visit.SourceSystemKey, visit.BillingSystemId);
                            }
                            else
                            {
                                this._visitStateApplicationService.Value.SetVisitIneligible(visit.SourceSystemKey, visit.BillingSystemId, changeEventMessage.EventTriggerDescription);
                            }
                        }

                        // DO THE REDACTIONS 
                        // Process all visits where Redact = true since the values in memory came from Hs
                        // and do not match VpApp redacted visit field values
                        foreach (CompareVisit visit in visitCompareList.Select(x => x.Modified).Where(x => x.Redact))
                        {
                            this._redactionApplicationService.Value.RedactVisit(visit.SourceSystemKey, visit.BillingSystemId, new VisitPayUserDto { VisitPayUserId = SystemUsers.SystemUserId });
                        }

                        // DO THE HsGuarantorIneligible
                        if (hsGuarantorCompareList.HasChanged(x => x.VpEligible).Select(x => x.Modified).Any(x => !x.VpEligible))
                        {
                            this._redactionApplicationService.Value.ProcessHsGuarantorVpIneligible(changeSetMessage);
                        }

                        // DO THE PAYMENT ALLOCATIONS
                        List<VisitPaymentAllocationChangeDto> visitPaymentAllocationChanges = changeSetMessage.VisitChanges.Where(x => x.VisitPaymentAllocations != null).SelectMany(x => x.VisitPaymentAllocations).ToList();
                        if (visitPaymentAllocationChanges.IsNotNullOrEmpty())
                        {
                            List<int> paymentAllocationIds = visitPaymentAllocationChanges.Select(x => x.PaymentAllocationId).ToList();
                            if (paymentAllocationIds.IsNotNullOrEmpty())
                            {
                                this._paymentAllocationApplicationService.Value.ClearPaymentAllocations(paymentAllocationIds);
                            }
                        }

                        break;

                    case ChangeEventTypeEnum.HsGuarantorMatchDiscrepancy:
                        this._redactionApplicationService.Value.ProcessHsGuarantorMatchDiscrepancy(changeSetMessage);
                        break;

                    case ChangeEventTypeEnum.HsGuarantorUnmatch:
                        this._redactionApplicationService.Value.ProcessHsGuarantorUnmatch(changeSetMessage);
                        break;

                    default:
                        throw new ArgumentOutOfRangeException();
                }
                this.OnChangeSetMessageProcessed(changeSetMessage, visitIdsChanged.Distinct().ToList());

                unitOfWork.Commit();
            }
            catch(Exception e)
            {
                // Capture/handle exception so the Session can remain stable
                // Will throw exception once the transaction has been rolled back
                workException = e;
            }
            finally
            {
                unitOfWork.Dispose();
            }

            if (workException != null)
            {
                throw workException;
            }

        }

        private void OnChangeSetMessageProcessed(ChangeSetMessage changeSetMessage, IList<int> visitIdsChanged)
        {
            // VPNG-19827 - Let the caller know that VP App successfully processed the change set
            this._session.Transaction.RegisterPostCommitSuccessAction((csid) =>
            {
                this.Bus.Value.PublishMessage(new ChangeEventProcessedMessage
                {
                    ChangeEventId = csid
                }).Wait();
            }, changeSetMessage.ChangeEvent.ChangeEventId);

            //VPNG-17245 - in rare edge cases the guarantor status is not getting changed. Forcing the commit. 
            this._session.Transaction.RegisterPostCommitSuccessAction((csid, guarantors, visitIds) =>
                {
                    this.Bus.Value.PublishMessage(new ChangeEventProcessedForVpGuarantor()
                    {
                        ChangeSetId = csid,
                        VpGuarantorIds = guarantors,
                        VisitIds = visitIds.ToList()
                    }).Wait();
                }, changeSetMessage.ChangeEvent.ChangeEventId,
                changeSetMessage.VpGuarantorHsMatches.Select(x => x.VpGuarantorId).ToList(),
                visitIdsChanged
            );
        }

        private void ProcessStatement(IEnumerable<int> vpGuarantorIds)
        {
            using (UnitOfWork unitOfWork = new UnitOfWork(this._session))
            {
                IList<CreateStatementMessage> messages = new List<CreateStatementMessage>();
                foreach (int vpGuarantorId in vpGuarantorIds)
                {
                    //Need to make sure there wasn't already a statement, then it wouldn't be eligible for an ImmediateStatement.
                    bool hasStatement = this._statementApplicationService.Value.DoesGuarantorHaveStatement(vpGuarantorId);
                    if (hasStatement)
                    {
                        continue;
                    }

                    messages.Add(new CreateStatementMessage
                    {
                        DateToProcess = DateTime.UtcNow,
                        VpGuarantorId = vpGuarantorId,
                        ImmediateStatement = true
                    });
                }

                if (messages.Count > 0)
                {
                    //hook up to post commit
                    this._session.Transaction.RegisterPostCommitSuccessAction(p1 =>
                    {
                        foreach (CreateStatementMessage createStatementMessage in p1)
                        {
                            this.Bus.Value.PublishMessage(createStatementMessage).Wait();
                        }
                    }, messages);
                }

                unitOfWork.Commit();
            }
        }

        // Utility for comparing
        [Serializable]
        private class CompareVisit
        {
            public CompareVisit()
            {

            }

            public CompareVisit(Visit visit)
            {
                this.VisitId = visit.VisitId;
                this.BillingSystemId = visit.BillingSystemId.Value;
                this.SourceSystemKey = visit.SourceSystemKey;
                this.VpEligible = visit.VpEligible;
                this.BillingHold = visit.BillingHold;
                this.Redact = visit.Redact;
            }

            public int VisitId { get; set; }
            public int BillingSystemId { get; set; }
            public string SourceSystemKey { get; set; }

            public bool VpEligible { get; set; }
            public bool BillingHold { get; set; }
            public bool Redact { get; set; }
        }

        [Serializable]
        // Utility for comparing
        private class CompareHsGuarantorMap
        {
            public CompareHsGuarantorMap()
            {

            }

            public CompareHsGuarantorMap(HsGuarantorMap hsGuarantorMap)
            {
                this.HsGuarantorMapId = hsGuarantorMap.HsGuarantorMapId;
                this.VpEligible = hsGuarantorMap.VpEligible;
            }

            public int HsGuarantorMapId { get; set; }
            public bool VpEligible { get; set; }
        }
    }
}
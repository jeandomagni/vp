﻿namespace Ivh.Application.Core.ApplicationServices
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using AutoMapper;
    using Common.Dtos;
    using Common.Interfaces;
    using Content.Common.Dtos;
    using Content.Common.Interfaces;
    using Domain.Core.Alert.Entities;
    using Domain.Core.Alert.Interfaces;
    using Domain.Guarantor.Entities;
    using Domain.Guarantor.Interfaces;
    using Domain.User.Entities;
    using Domain.Visit.Interfaces;
    using Domain.FinanceManagement.Consolidation.Interfaces;
    using Domain.FinanceManagement.Entities;
    using Domain.FinanceManagement.FinancePlan.Entities;
    using Domain.FinanceManagement.FinancePlan.Interfaces;
    using Domain.FinanceManagement.Interfaces;
    using Domain.FinanceManagement.Statement.Entities;
    using Domain.FinanceManagement.Statement.Interfaces;
    using Domain.SecureCommunication.GuarantorSupportRequests.Interfaces;
    using Domain.Settings.Entities;
    using Ivh.Common.Session;
    using AppIntelligence.Common.Dtos;
    using AppIntelligence.Common.Interfaces;
    using Base.Common.Interfaces;
    using Base.Services;
    using FinanceManagement.Common.Interfaces;
    using Ivh.Common.Base.Utilities.Extensions;
    using Ivh.Common.VisitPay.Enums;
    using Ivh.Common.VisitPay.Strings;

    public class AlertingApplicationService : ApplicationService, IAlertingApplicationService
    {
        private readonly Lazy<IAlertService> _alertService;
        private readonly Lazy<IVisitService> _visitService;
        private readonly Lazy<ISupportRequestService> _supportRequestService;
        private readonly Lazy<IConsolidationGuarantorService> _consolidationGuarantorService;
        private readonly Lazy<IContentApplicationService> _contentApplicationService;
        private readonly Lazy<IFinancePlanService> _financePlanService;
        private readonly Lazy<IGuarantorApplicationService> _guarantorApplicationService;
        private readonly Lazy<IGuarantorService> _guarantorService;
        private readonly Lazy<IPaymentMethodsService> _paymentMethodsService;
        private readonly Lazy<IVpStatementService> _statementService;
        private readonly Lazy<ISsoApplicationService> _ssoApplicationService;
        private readonly Lazy<IRandomizedTestApplicationService> _randomizedTestApplicationService;
        private readonly Lazy<IFinancePlanApplicationService> _financePlanApplicationService;
        private ISessionFacade _sessionFacade;

        private const string ReconfigureFinancePlansUrl = "/FinancePlan/ReconfigureFinancePlan/{0}?v={1}";
        private const string VpccReconfigureFinancePlansUrl = "/Offline/ReconfigureFinancePlan";

        public AlertingApplicationService(
            Lazy<IApplicationServiceCommonService> applicationServiceCommonService,
            Lazy<IFinancePlanService> financePlanService,
            Lazy<IGuarantorApplicationService> guarantorApplicationService,
            Lazy<IGuarantorService> guarantorService,
            Lazy<IPaymentMethodsService> paymentMethodsService,
            Lazy<IVpStatementService> statementService,
            Lazy<IConsolidationGuarantorService> consolidationGuarantorService,
            Lazy<ISupportRequestService> supportRequestService,
            Lazy<IContentApplicationService> contentApplicationService,
            Lazy<ISsoApplicationService> ssoApplicationService,
            Lazy<IAlertService> alertService,
            Lazy<IVisitService> visitService,
            Lazy<IRandomizedTestApplicationService> randomizedTestApplicationService,
            Lazy<IFinancePlanApplicationService> financePlanApplicationService
            ) : base(applicationServiceCommonService)
        {
            this._financePlanService = financePlanService;
            this._guarantorApplicationService = guarantorApplicationService;
            this._guarantorService = guarantorService;
            this._paymentMethodsService = paymentMethodsService;
            this._statementService = statementService;
            this._consolidationGuarantorService = consolidationGuarantorService;
            this._supportRequestService = supportRequestService;
            this._contentApplicationService = contentApplicationService;
            this._ssoApplicationService = ssoApplicationService;
            this._alertService = alertService;
            this._visitService = visitService;
            this._randomizedTestApplicationService = randomizedTestApplicationService;
            this._financePlanApplicationService = financePlanApplicationService;
        }

        private string _arrangePaymentUrl = "/Payment/ArrangePayment/?v={0}&paymentOption={1}";
        private string _paymentMethodUrl = "/Payment/PaymentMethods";
        private string _requestToBeManagedUrl = "/Consolidate/ManageHousehold/";
        private string _requestToManageUrl = "/Consolidate/ManageHousehold/";
        private string _reconfiguredFinancePlansUrl = ReconfigureFinancePlansUrl;
        private string _supportRequestsUrl = "/Support/MySupportRequests";
        private string _visitsUrl = "/Payment/MyPayments";

        public IList<AlertDto> GetAlertsForGuarantor(int vpGuarantorId, ISessionFacade sessionFacade, IList<int> dismissedAlerts, bool isMobile = false)
        {
            this._sessionFacade = sessionFacade;
            IReadOnlyList<Alert> alerts = (IReadOnlyList<Alert>)this._alertService.Value.GetActiveAlerts();

            // remove dismissed alerts
            alerts = alerts.Where(x => !dismissedAlerts.Contains(x.AlertId)).ToList();

            Guarantor guarantor = this._guarantorService.Value.GetGuarantor(vpGuarantorId);
            if (guarantor.IsOfflineGuarantor())
            {
                this._reconfiguredFinancePlansUrl = VpccReconfigureFinancePlansUrl;

                // remove alerts that aren't used in vpcc
                alerts = alerts.Where(x => x.AlertCriteria.HasValue &&
                                           x.AlertCriteria.Value.IsInCategory(AlertCriteriaEnumCategory.Vpcc)).ToList();
            }
            else
            {
                this._reconfiguredFinancePlansUrl = ReconfigureFinancePlansUrl;
            }

            if (isMobile)
            {
                this._arrangePaymentUrl = "/mobile" + this._arrangePaymentUrl;
                this._paymentMethodUrl = "/mobile" + this._paymentMethodUrl;
                this._requestToBeManagedUrl = "/mobile" + this._requestToBeManagedUrl + "?managed=1";
                this._requestToManageUrl = "/mobile" + this._requestToManageUrl + "?managed=2";
                this._reconfiguredFinancePlansUrl = "/mobile" + this._reconfiguredFinancePlansUrl;
                this._supportRequestsUrl = "/mobile" + this._supportRequestsUrl;
                this._visitsUrl = "/mobile" + this._visitsUrl;
            }

            List<AlertDto> alertsToShow = new List<AlertDto>();
            this.ApplyGenericCriteria(alerts, alertsToShow, guarantor);
            this.ApplyPaymentMethodCriteria(alerts, alertsToShow, guarantor);
            this.ApplyMangingRequestCritera(alerts, alertsToShow, guarantor);

            return alertsToShow;
        }

        private void ApplyGenericCriteria(IReadOnlyList<Alert> alerts, IList<AlertDto> alertDtos, Guarantor guarantor)
        {
            foreach (Alert alert in alerts)
            {
                AlertDto alertDto = Mapper.Map<AlertDto>(alert);
                List<AlertDto> alertsToAdd = this.ApplyGenericCriteria(alertDto, guarantor);
                if (alertsToAdd?.Count > 0)
                {
                    alertDtos.AddRange(alertsToAdd);
                }
            }
        }

        private List<AlertDto> ApplyGenericCriteria(AlertDto alertDto, Guarantor guarantor)
        {
            switch (alertDto.AlertCriteria)
            {
                case AlertCriteriaEnum.FailedPayment:
                    return this.FailedPayment(alertDto, guarantor);
                case AlertCriteriaEnum.FailedPaymentManagedGuarantor:
                    return this.FailedPaymentManagedGuarantor(alertDto, guarantor);
                case AlertCriteriaEnum.FailedPaymentManagingGuarantor:
                    return this.FailedPaymentManagingGuarantor(alertDto, guarantor);
                case AlertCriteriaEnum.FinancePlanPendingAcceptance:
                    return this.FinancePlanPendingAcceptance(alertDto, guarantor);
                case AlertCriteriaEnum.ClientFinancePlanOfferPendingAcceptance:
                    return this.ClientFinancePlanOfferPendingAcceptance(alertDto, guarantor);
                case AlertCriteriaEnum.ManagedConsolidationCancelledNoPaymentMethod:
                    return this.ManagedConsolidationCancelledNoPaymentMethod(alertDto, guarantor);
                case AlertCriteriaEnum.PendingRequestToBeManaged:
                    return this.PendingRequestToBeManaged(alertDto, guarantor);
                case AlertCriteriaEnum.ReconfigurationAvailable:
                    return this.ReconfigurationAvailable(alertDto, guarantor);
                case AlertCriteriaEnum.ReconfiguredFinancePlan:
                    return this.ReconfiguredFinancePlan(alertDto, guarantor);
                case AlertCriteriaEnum.SsoInvalidated:
                    return this.SsoInvalidated(alertDto, guarantor);
                case AlertCriteriaEnum.UnreadSupportRequestMessage:
                    return this.UnreadSupportRequestMessage(alertDto, guarantor);
                case AlertCriteriaEnum.SsoHqySiteDown:
                    return this.SsoHqySiteDown(alertDto, guarantor);
                case AlertCriteriaEnum.HealthEquityGetStarted:
                    return this.HealthEquityGetStarted(alertDto, guarantor);
                default:
                    break;
            }

            return null;
        }

        private void ApplyPaymentMethodCriteria(IReadOnlyList<Alert> alerts, IList<AlertDto> alertDtos, Guarantor guarantor)
        {
            if (alerts.Count(x => x.AlertCriteria == AlertCriteriaEnum.CardExpired || x.AlertCriteria == AlertCriteriaEnum.CardExpiring) == 0)
            {
                return;
            }

            PaymentMethod primaryPaymentMethod = this._paymentMethodsService.Value.GetPrimaryPaymentMethod(guarantor.VpGuarantorId, false);

            foreach (Alert alert in alerts)
            {
                AlertDto alertDto = Mapper.Map<AlertDto>(alert);

                switch (alertDto.AlertCriteria)
                {
                    case AlertCriteriaEnum.CardExpiring:
                        alertDtos.AddRange(this.CardExpiring(alertDto, guarantor, primaryPaymentMethod));
                        break;
                    case AlertCriteriaEnum.CardExpired:
                        alertDtos.AddRange(this.CardExpired(alertDto, guarantor, primaryPaymentMethod));
                        break;
                }
            }
        }

        private void ApplyMangingRequestCritera(IReadOnlyList<Alert> alerts, IList<AlertDto> alertDtos, Guarantor guarantor)
        {
            if (alerts.Count(x => x.AlertCriteria == AlertCriteriaEnum.PendingRequestToBeManaging || x.AlertCriteria == AlertCriteriaEnum.PendingRequestToBeManagingFp) == 0)
            {
                return;
            }

            List<ConsolidationGuarantor> pendingManagingRequests = this._consolidationGuarantorService.Value.GetPendingManagingRequests(guarantor).ToList();

            foreach (Alert alert in alerts)
            {
                AlertDto alertDto = Mapper.Map<AlertDto>(alert);

                switch (alertDto.AlertCriteria)
                {
                    case AlertCriteriaEnum.PendingRequestToBeManaging:
                        alertDtos.AddRange(this.PendingRequestToBeManaging(alertDto, guarantor, pendingManagingRequests));
                        break;
                    case AlertCriteriaEnum.PendingRequestToBeManagingFp:
                        alertDtos.AddRange(this.PendingRequestToBeManagingFp(alertDto, guarantor, pendingManagingRequests));
                        break;
                }
            }
        }

        private List<AlertDto> FailedPayment(AlertDto alertDto, Guarantor guarantor)
        {
            List<AlertDto> alerts = new List<AlertDto>();
            if (!guarantor.IsManaged() && !guarantor.IsManaging() || guarantor.IsManaging())
            {
                if (this._financePlanApplicationService.Value.HasFinancePlansWhichShouldResubmit(guarantor.VpGuarantorId))
                {
                    Dictionary<string, string> replacementDictionary = new Dictionary<string, string>
                    {
                        ["[[ArangePaymentUrl]]"] = string.Format(this._arrangePaymentUrl, this._sessionFacade.GuarantorObfuscator.Obfuscate(guarantor.VpGuarantorId), PaymentOptionEnum.Resubmit)
                    };
                    this.ApplyContent(alertDto, replacementDictionary);
                    alerts.Add(alertDto);
                }
            }

            return alerts;
        }

        private List<AlertDto> FailedPaymentManagedGuarantor(AlertDto alertDto, Guarantor guarantor)
        {
            List<AlertDto> alerts = new List<AlertDto>();
            if (guarantor.IsManaged())
            {
                if (this._financePlanApplicationService.Value.HasFinancePlansWhichShouldResubmit(guarantor.VpGuarantorId))
                {
                    VisitPayUser managingGuarantor = guarantor.ActiveManagingConsolidationGuarantors.FirstOrDefault()?.ManagingGuarantor.User;
                    if (managingGuarantor != null)
                    {
                        Dictionary<string, string> replacementDictionary = new Dictionary<string, string>
                        {
                            ["[[ArangePaymentUrl]]"] = string.Format(this._arrangePaymentUrl, this._sessionFacade.GuarantorObfuscator.Obfuscate(guarantor.VpGuarantorId), PaymentOptionEnum.Resubmit),
                            ["[[ManagingGuarantorName]]"] = managingGuarantor.FirstNameLastName
                        };
                        this.ApplyContent(alertDto, replacementDictionary);
                        alerts.Add(alertDto);
                    }
                }
            }

            return alerts;
        }

        private List<AlertDto> FailedPaymentManagingGuarantor(AlertDto alertDto, Guarantor guarantor)
        {
            List<AlertDto> alerts = new List<AlertDto>();
            if (guarantor.IsManaging())
            {
                foreach (Guarantor managedGuarantor in guarantor.ActiveManagedConsolidationGuarantors.Select(x => x.ManagedGuarantor))
                {
                    if (this._financePlanApplicationService.Value.HasFinancePlansWhichShouldResubmit(managedGuarantor.VpGuarantorId))
                    {
                        AlertDto clone = Mapper.Map<AlertDto>(alertDto);
                        Dictionary<string, string> replacementDictionary = new Dictionary<string, string>
                        {
                            ["[[ArangePaymentUrl]]"] = string.Format(this._arrangePaymentUrl, this._sessionFacade.GuarantorObfuscator.Obfuscate(managedGuarantor.VpGuarantorId), PaymentOptionEnum.Resubmit),
                            ["[[VpGuarantorName]]"] = managedGuarantor.User.FirstNameLastName
                        };

                        this.ApplyContent(clone, replacementDictionary);
                        alerts.Add(clone);
                    }
                }
            }

            return alerts;
        }

        private List<AlertDto> CardExpiring(AlertDto alertDto, Guarantor guarantor, PaymentMethod primaryPaymentMethod)
        {
            List<AlertDto> alerts = new List<AlertDto>();
            if (primaryPaymentMethod != null)
            {
                if (primaryPaymentMethod.IsExpiring && !primaryPaymentMethod.IsExpired)
                {
                    Dictionary<string, string> replacementDictionary = new Dictionary<string, string>
                    {
                        ["[[PaymentMethodsUrl]]"] = this._paymentMethodUrl,
                        ["[[CreditCardExpirationDate]]"] = primaryPaymentMethod.ExpDateForDisplay
                    };
                    this.ApplyContent(alertDto, replacementDictionary);
                    alerts.Add(alertDto);
                }
            }

            return alerts;
        }

        private List<AlertDto> CardExpired(AlertDto alertDto, Guarantor guarantor, PaymentMethod primaryPaymentMethod)
        {
            List<AlertDto> alerts = new List<AlertDto>();
            if (primaryPaymentMethod != null)
            {
                Dictionary<string, string> replacementDictionary = new Dictionary<string, string>
                {
                    ["[[PaymentMethodsUrl]]"] = this._paymentMethodUrl
                };

                if (primaryPaymentMethod.IsExpired)
                {
                    this.ApplyContent(alertDto, replacementDictionary);
                    this.ApplyContent(alertDto, replacementDictionary);
                    alerts.Add(alertDto);
                }
            }
            return alerts;
        }

        private List<AlertDto> FinancePlanPendingAcceptance(AlertDto alertDto, Guarantor guarantor)
        {
            List<AlertDto> alerts = new List<AlertDto>();

            if (guarantor.ConsolidationStatus == GuarantorConsolidationStatusEnum.Managed)
            {
                return alerts;
            }

            // could be more than one due to consolidation
            IList<Guarantor> guarantorsWithFinancePlans = this._financePlanService.Value.GetFirstPendingAcceptanceFinancePlanPatientOfferGuarantor(guarantor, false, true);
            if (guarantorsWithFinancePlans.IsNotNullOrEmpty())
            {
                foreach (Guarantor fpGuarantor in guarantorsWithFinancePlans)
                {
                    AlertDto localAlertDto = Mapper.Map<AlertDto>(alertDto);

                    VpStatement currentStatement = this._statementService.Value.GetMostRecentStatement(fpGuarantor);
                    DateTime originationDate = this._financePlanService.Value.GetOriginationDate(fpGuarantor, currentStatement);
                    
                    Dictionary<string, string> replacementDictionary = new Dictionary<string, string>
                    {
                        ["[[ArangePaymentUrl]]"] = string.Format(this._arrangePaymentUrl, this._sessionFacade.GuarantorObfuscator.Obfuscate(fpGuarantor.VpGuarantorId), PaymentOptionEnum.FinancePlan),
                        ["[[FpAcceptanceDate]]"] = originationDate.ToString(Format.DateFormat)
                    };

                    this.ApplyContent(localAlertDto, replacementDictionary);
                    alerts.Add(localAlertDto);
                }
            }

            return alerts;
        }

        private List<AlertDto> ClientFinancePlanOfferPendingAcceptance(AlertDto alertDto, Guarantor guarantor)
        {
            List<AlertDto> alerts = new List<AlertDto>();

            if (guarantor.ConsolidationStatus == GuarantorConsolidationStatusEnum.Managed)
            {
                return alerts;
            }

            // could be more than one due to consolidation
            IList<Guarantor> guarantorsWithFinancePlans = this._financePlanService.Value.GetFirstPendingAcceptanceFinancePlanClientOnlyOfferGuarantor(guarantor, false, true);
            if (guarantorsWithFinancePlans.IsNotNullOrEmpty())
            {
                foreach (Guarantor fpGuarantor in guarantorsWithFinancePlans)
                {
                    AlertDto localAlertDto = Mapper.Map<AlertDto>(alertDto);
                    
                    VpStatement currentStatement = this._statementService.Value.GetMostRecentStatement(fpGuarantor);
                    DateTime originationDate = this._financePlanService.Value.GetOriginationDate(fpGuarantor, currentStatement);
                    
                    Dictionary<string, string> replacementDictionary = new Dictionary<string, string>
                    {
                        ["[[ArangePaymentUrl]]"] = string.Format(this._arrangePaymentUrl, this._sessionFacade.GuarantorObfuscator.Obfuscate(fpGuarantor.VpGuarantorId), PaymentOptionEnum.FinancePlan),
                        ["[[FpAcceptanceDate]]"] = originationDate.ToString(Format.DateFormat)
                    };

                    this.ApplyContent(localAlertDto, replacementDictionary);
                    alerts.Add(localAlertDto);
                }
            }
            return alerts;
        }

        private List<AlertDto> ManagedConsolidationCancelledNoPaymentMethod(AlertDto alertDto, Guarantor guarantor)
        {
            //<p>Your consolidated relationship is no longer active.  You now have sole financial responsibility for any and all pending or active finance plans that the managing user previously arranged on your visits.</p><p>To avoid past due status, you must <a href="{0}" title="Add a Primary Payment Method">click here to add a primary payment method</a> to your account.  This will be the payment method used for all finance plan payments.</p>
            List<AlertDto> alerts = new List<AlertDto>();
            if (guarantor.ConsolidationStatus != GuarantorConsolidationStatusEnum.NotConsolidated)
            {
                return alerts;
            }
            
            List<ConsolidationGuarantor> consolidations = this._consolidationGuarantorService.Value.GetAllManagingGuarantors(guarantor).ToList();
            if (!consolidations.Any())
            {
                return alerts;
            }

            ConsolidationGuarantor lastConsolidation = consolidations.OrderByDescending(x => x.InsertDate).First();
            if (lastConsolidation.ConsolidationGuarantorStatus != ConsolidationGuarantorStatusEnum.Cancelled)
            {
                return alerts;
            }
            
            int activeFinancePlansCount = this._financePlanService.Value.GetAllActiveFinancePlanIds(guarantor.VpGuarantorId).Count;
            if (activeFinancePlansCount == 0)
            {
                return alerts;
            }

            int paymentMethodsCount = this._paymentMethodsService.Value.GetPaymentMethodsCount(guarantor.VpGuarantorId);
            if (paymentMethodsCount > 0)
            {
                return alerts;
            }

            PaymentMethodEvent mostRecentPaymentMethodActivated = this._paymentMethodsService.Value.GetMostRecentPaymentMethodEvent(guarantor.VpGuarantorId, PaymentMethodStatusEnum.Activated);
            if (mostRecentPaymentMethodActivated == null || mostRecentPaymentMethodActivated.InsertDate.CompareTo(lastConsolidation.MostRecentActionDate) < 0)
            {
                Dictionary<string, string> replacementDictionary = new Dictionary<string, string>
                {
                    ["[[PaymentMethodsUrl]]"] = this._paymentMethodUrl
                };
                this.ApplyContent(alertDto, replacementDictionary);
                alerts.Add(alertDto);
            }
            return alerts;
        }

        private List<AlertDto> PendingRequestToBeManaged(AlertDto alertDto, Guarantor guarantor)
        {
            List<AlertDto> alerts = new List<AlertDto>();
            Client client = this.ClientService.Value.GetClient();
            foreach (ConsolidationGuarantor consolidationGuarantor in this._consolidationGuarantorService.Value.GetPendingManagedRequests(guarantor))
            {
                if (guarantor.User.VisitPayUserId != consolidationGuarantor.InitiatedByUser.VisitPayUserId && !consolidationGuarantor.DateAcceptedByManagedGuarantor.HasValue)
                {
                    AlertDto clone = Mapper.Map<AlertDto>(alertDto);
                    DateTime expirationDate = consolidationGuarantor.InsertDate.AddDays(client.ConsolidationExpirationDays);

                    Dictionary<string, string> replacementDictionary = new Dictionary<string, string>
                    {
                        ["[[RequestToBeManagedExpirationDate]]"] = expirationDate.Date.ToString(Format.DateFormat),
                        ["[[ManageHouseholdUrl]]"] = this._requestToBeManagedUrl
                    };

                    this.ApplyContent(clone, replacementDictionary);
                    alerts.Add(clone);
                }
            }

            return alerts;
        }

        private List<AlertDto> PendingRequestToBeManaging(AlertDto alertDto, Guarantor guarantor, IEnumerable<ConsolidationGuarantor> pendingManagingRequests)
        {
            Client client = this.ClientService.Value.GetClient();
            List<AlertDto> alerts = new List<AlertDto>();
            foreach (ConsolidationGuarantor consolidationGuarantor in pendingManagingRequests)
            {
                DateTime expirationDate = consolidationGuarantor.InsertDate.AddDays(client.ConsolidationExpirationDays);
                if (!consolidationGuarantor.DateAcceptedByManagingGuarantor.HasValue && consolidationGuarantor.IsPending())
                {
                    AlertDto clone = Mapper.Map<AlertDto>(alertDto);
                    Dictionary<string, string> replacementDictionary = new Dictionary<string, string>
                    {
                        ["[[RequestToBeManagedExpirationDate]]"] = expirationDate.Date.ToString(Format.DateFormat),
                        ["[[ManageHouseholdUrl]]"] = this._requestToManageUrl
                    };
                    this.ApplyContent(clone, replacementDictionary);
                    alerts.Add(clone);
                }
            }
            return alerts;
        }

        private List<AlertDto> PendingRequestToBeManagingFp(AlertDto alertDto, Guarantor guarantor, IEnumerable<ConsolidationGuarantor> pendingManagingRequests)
        {
            Client client = this.ClientService.Value.GetClient();
            List<AlertDto> alerts = new List<AlertDto>();
            foreach (ConsolidationGuarantor consolidationGuarantor in pendingManagingRequests)
            {
                DateTime expirationDate = consolidationGuarantor.InsertDate.AddDays(client.ConsolidationExpirationDays);

                if (consolidationGuarantor.DateAcceptedByManagingGuarantor.HasValue
                    && !consolidationGuarantor.DateAcceptedFinancePlanTermsByManagingGuarantor.HasValue
                    && consolidationGuarantor.ConsolidationGuarantorStatus == ConsolidationGuarantorStatusEnum.PendingFinancePlan)
                {
                    AlertDto clone = Mapper.Map<AlertDto>(alertDto);
                    Dictionary<string, string> replacementDictionary = new Dictionary<string, string>
                    {
                        ["[[RequestToManageDate]]"] = consolidationGuarantor.InsertDate.Date.ToString(Format.DateFormat),
                        ["[[ManageHouseholdUrl]]"] = this._requestToManageUrl,
                        ["[[RequestToBeManagedExpirationDate]]"] = expirationDate.Date.ToString(Format.DateFormat)
                    };
                    this.ApplyContent(clone, replacementDictionary);
                    alerts.Add(clone);
                }
            }
            return alerts;
        }

        private List<AlertDto> ReconfigurationAvailable(AlertDto alertDto, Guarantor guarantor)
        {
            List<AlertDto> alerts = new List<AlertDto>();
            if (guarantor.ConsolidationStatus == GuarantorConsolidationStatusEnum.Managed)
            {
                return alerts;
            }

            IList<FinancePlan> financePlans = this._financePlanService.Value.GetAllOpenFinancePlansForGuarantor(guarantor, true);
            List<FinancePlan> financePlansWithPositiveAdjustment = financePlans.Where(x => x.HasPositiveVisitBalanceDifference()).ToList();
            if (financePlansWithPositiveAdjustment.IsNullOrEmpty())
            {
                return alerts;
            }

            foreach (FinancePlan financePlan in financePlansWithPositiveAdjustment)
            {
                string obfuscatedVpGuarantorId = string.Empty;
                if (financePlan.VpGuarantor.ConsolidationStatus != GuarantorConsolidationStatusEnum.NotConsolidated)
                {
                    obfuscatedVpGuarantorId = this._sessionFacade.GuarantorObfuscator.Obfuscate(financePlan.VpGuarantor.VpGuarantorId).ToString();
                }

                Dictionary<string, string> replacementDictionary = new Dictionary<string, string>
                {
                    ["[[ReconfiguredFinancePlansUrl]]"] = string.Format(this._reconfiguredFinancePlansUrl, financePlan.FinancePlanId, obfuscatedVpGuarantorId)
                };

                AlertDto newAlertDto = Mapper.Map<AlertDto>(alertDto);
                this.ApplyContent(newAlertDto, replacementDictionary);
                alerts.Add(newAlertDto);
            }

            return alerts;
        }

        private List<AlertDto> ReconfiguredFinancePlan(AlertDto alertDto, Guarantor guarantor)
        {
            List<AlertDto> alerts = new List<AlertDto>();
            if (guarantor.ConsolidationStatus == GuarantorConsolidationStatusEnum.Managed)
            {
                return alerts;
            }

            IList<FinancePlan> pendingAcceptanceFinancePlans = this._financePlanService.Value.GetAllPendingAcceptanceFinancePlansForGuarantor(guarantor, true, true);
            foreach (FinancePlan reconfiguredFinancePlan in pendingAcceptanceFinancePlans.Where(x => x.OriginalFinancePlan != null))
            {
                string obfuscatedVpGuarantorId = string.Empty;
                if (reconfiguredFinancePlan.VpGuarantor.ConsolidationStatus != GuarantorConsolidationStatusEnum.NotConsolidated)
                {
                    obfuscatedVpGuarantorId = this._sessionFacade.GuarantorObfuscator.Obfuscate(reconfiguredFinancePlan.VpGuarantor.VpGuarantorId).ToString();
                }

                Dictionary<string, string> replacementDictionary = new Dictionary<string, string>
                {
                    ["[[ReconfiguredFinancePlansUrl]]"] = string.Format(this._reconfiguredFinancePlansUrl, reconfiguredFinancePlan.OriginalFinancePlan.FinancePlanId, obfuscatedVpGuarantorId)
                };

                AlertDto newAlertDto = Mapper.Map<AlertDto>(alertDto);
                this.ApplyContent(newAlertDto, replacementDictionary);
                alerts.Add(newAlertDto);
            }

            return alerts;
        }

        private List<AlertDto> SsoInvalidated(AlertDto alertDto, Guarantor guarantor)
        {
            List<AlertDto> alerts = new List<AlertDto>();

            Client client = this.ClientService.Value.GetClient();
            if (!client.HealthEquityEnabled)
            {
                return alerts;
            }

            SsoStatusEnum? status = this._ssoApplicationService.Value.GetUserSsoStatus(SsoProviderEnum.HealthEquityOutbound, guarantor.User.VisitPayUserId);
            if (status != null && status == SsoStatusEnum.Invalidated)
            {
                Dictionary<string, string> replacementDictionary = new Dictionary<string, string>();

                this.ApplyContent(alertDto, replacementDictionary);
                alerts.Add(alertDto);
            }

            return alerts;
        }

        // requirements for when the alert should or should not be shown are on VPNG-20610
        private List<AlertDto> HealthEquityGetStarted(AlertDto alertDto, Guarantor guarantor)
        {
            List<AlertDto> alerts = new List<AlertDto>();

            Client client = this.ClientService.Value.GetClient();
            if (!client.HealthEquityEnabled)
            {
                return alerts;
            }

            RandomizedTestGroupDto randomizedTestGroupDto = this._randomizedTestApplicationService.Value.GetRandomizedTestGroupMembership(RandomizedTestEnum.NotificationTesting, guarantor.VpGuarantorId, true);
            bool showHqyAlert = (randomizedTestGroupDto?.RandomizedTestGroupId == (int)RandomizedTestGroupEnum.MessageViaAlert);
            if (!showHqyAlert)
            {
                return alerts;
            }

            SsoProviderEnum selectHealthEquitySsoProvider = this._ssoApplicationService.Value.SelectHealthEquitySsoProvider();
            bool userHasIgnoredSso = this._ssoApplicationService.Value.HasUserIgnoredSso(selectHealthEquitySsoProvider, guarantor.User.VisitPayUserId);
            SsoStatusEnum? status = this._ssoApplicationService.Value.GetUserSsoStatus(selectHealthEquitySsoProvider, guarantor.User.VisitPayUserId);

            //User has never dismissed the alert, or added a new HQY card (dissmised alert will be unset)
            if (userHasIgnoredSso || (status != null && (status != SsoStatusEnum.Unknown && status != SsoStatusEnum.Rejected)))
            {
                return alerts;
            }

            Dictionary<string, string> replacementDictionary = new Dictionary<string, string>();
            this.ApplyContent(alertDto, replacementDictionary);

            //User has never ignored the alert, or added a new HQY card (ignored alert will be unset)
            if (status != null && status == SsoStatusEnum.Unknown)
            {
                // show if they added an HQY card.  visits and insurance plans don't matter.
                alerts.Add(alertDto);
            }
            else
            {
                // now check visits and insurance plans
                IDictionary<string, string> insurancePlanMap = client.HealthEquitySsoEmployerIdMap;
                if (insurancePlanMap != null && insurancePlanMap.Count > 0)
                {
                    bool userHasVisitsForInsurancePlans = this._visitService.Value.DoesGuarantorHaveVisitWithInsurancePlanInList(guarantor.VpGuarantorId, insurancePlanMap.Keys.ToList());
                    if (userHasVisitsForInsurancePlans)
                    {
                        // show if user has visits for the client's insurance plan list.
                        alerts.Add(alertDto);
                    }
                }
                else if (client.HealthEquityDisplayIfSsoEmployerIdMapIsEmpty)
                {
                    // show if the client doesn't have insurance plan list but wants everyone to see the alert anyway.
                    alerts.Add(alertDto);
                }
            }

            return alerts;
        }

        private List<AlertDto> SsoHqySiteDown(AlertDto alertDto, Guarantor guarantor)
        {
            List<AlertDto> alerts = new List<AlertDto>();

            Client client = this.ClientService.Value.GetClient();
            if (!client.HealthEquityEnabled)
            {
                return alerts;
            }

            HealthEquitySsoStatusDto status = null;
            Task.Run(async () =>
            {
                status = await this._guarantorApplicationService.Value.GetGuarantorHealthEquitySsoStatusAsnyc(guarantor.VpGuarantorId, guarantor.User.VisitPayUserId);
            }).Wait();

            if (status != null && !status.IsHqySsoUp)
            {
                this.ApplyContent(alertDto, new Dictionary<string, string>());
                alerts.Add(alertDto);
            }

            return alerts;
        }

        private List<AlertDto> UnreadSupportRequestMessage(AlertDto alertDto, Guarantor guarantor)
        {
            List<AlertDto> alerts = new List<AlertDto>();

            int unreadMessagesCount = this._supportRequestService.Value.GetUnreadMessagesToGuarantorCount(guarantor);
            if (unreadMessagesCount <= 0)
            {
                return alerts;
            }

            Dictionary<string, string> replacementDictionary = new Dictionary<string, string>
            {
                ["[[SupportRequestsUrl]]"] = this._supportRequestsUrl
            };

            this.ApplyContent(alertDto, replacementDictionary);
            alerts.Add(alertDto);

            return alerts;
        }
        
        private CmsVersionDto GetCmsVersion(CmsRegionEnum cmsRegionEnum, IDictionary<string, string> replacementValues)
        {
            CmsVersionDto cmsVersionDto = this._contentApplicationService.Value.GetCurrentVersionAsync(cmsRegionEnum, true, replacementValues).Result;
            return cmsVersionDto;
        }

        private void ApplyContent(AlertDto alertDto, IDictionary<string, string> replacementDictionary)
        {
            CmsVersionDto cmsVersion = this.GetCmsVersion(alertDto.CmsRegion, replacementDictionary);
            alertDto.ContentBody = cmsVersion.ContentBody;
            alertDto.ContentTitle = cmsVersion.ContentTitle;
        }
    }
}
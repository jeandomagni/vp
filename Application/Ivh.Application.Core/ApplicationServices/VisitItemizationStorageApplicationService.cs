﻿namespace Ivh.Application.Core.ApplicationServices
{
    using System;
    using System.Collections.Generic;
    using AutoMapper;
    using Common.Dtos;
    using Common.Interfaces;
    using Domain.Visit.Entities;
    using Domain.Visit.Interfaces;
    using System.Threading.Tasks;
    using Base.Common.Interfaces;
    using Base.Services;

    public class VisitItemizationStorageApplicationService : ApplicationService, IVisitItemizationStorageApplicationService
    {
        private readonly Lazy<IVisitItemizationStorageService> _visitItemizationStorageService;

        public VisitItemizationStorageApplicationService(
            Lazy<IApplicationServiceCommonService> applicationServiceCommonService,
            Lazy<IVisitItemizationStorageService> visitItemizationStorageService) : base(applicationServiceCommonService)
        {
            this._visitItemizationStorageService = visitItemizationStorageService;
        }

        public VisitItemizationStorageDto GetItemization(int vpGuarantorId, int visitFileStoredId, int currentVisitPayUserId)
        {
            VisitItemizationStorage itemization = this._visitItemizationStorageService.Value.GetItemization(vpGuarantorId, visitFileStoredId, currentVisitPayUserId);

            return Mapper.Map<VisitItemizationStorageDto>(itemization);
        }

        public VisitItemizationStorageResultsDto GetItemizations(int vpGuarantorId, VisitItemizationStorageFilterDto filterDto, int pageNumber, int pageSize)
        {
            VisitItemizationStorageResults results = this._visitItemizationStorageService.Value.GetItemizations(vpGuarantorId, Mapper.Map<VisitItemizationStorageFilter>(filterDto), Math.Max(pageNumber - 1, 0), pageSize);

            return Mapper.Map<VisitItemizationStorageResultsDto>(results);
        }

        public int GetItemizationTotals(int vpGuarantorId, VisitItemizationStorageFilterDto filterDto)
        {
            return this._visitItemizationStorageService.Value.GetItemizationTotals(vpGuarantorId, Mapper.Map<VisitItemizationStorageFilter>(filterDto));
        }

        public IDictionary<string, string> GetAllFacilities(int vpGuarantorId, int currentVisitPayUserId)
        {
            return this._visitItemizationStorageService.Value.GetAllFacilities(vpGuarantorId, currentVisitPayUserId);
        }

        public IList<string> GetAllPatientNames(int vpGuarantorId, int currentVisitPayUserId)
        {
            return this._visitItemizationStorageService.Value.GetAllPatientNames(vpGuarantorId, currentVisitPayUserId);
        }

        public async Task SaveAsync(VisitItemizationStorage visitItemization)
        {
            await this._visitItemizationStorageService.Value.SaveAsync(visitItemization);
        }

        public async Task<VisitItemizationStorage> GetByExternalKeyAsync(Guid externalKey, int currentVisitPayUserId)
        {
            return await this._visitItemizationStorageService.Value.GetByExternalKeyAsync(externalKey, currentVisitPayUserId);
        }
    }
}

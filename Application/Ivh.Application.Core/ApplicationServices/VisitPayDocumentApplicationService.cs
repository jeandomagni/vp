﻿namespace Ivh.Application.Core.ApplicationServices
{
    using System;
    using System.Collections.Generic;
    using AutoMapper;
    using Base.Common.Interfaces;
    using Base.Services;
    using Common.Dtos;
    using Common.Interfaces;
    using Domain.Core.VisitPayDocument.Entities;
    using Domain.Core.VisitPayDocument.Interfaces;
    using Domain.User.Services;
    using Ivh.Common.Base.Utilities.Extensions;
    using Microsoft.AspNet.Identity;

    public class VisitPayDocumentApplicationService : ApplicationService, IVisitPayDocumentApplicationService
    {
        private readonly Lazy<IVisitPayDocumentService> _visitPayDocumentService;
        private readonly Lazy<VisitPayUserService> _visitPayUserService;


        public VisitPayDocumentApplicationService(
            Lazy<IApplicationServiceCommonService> applicationServiceCommonService,
            Lazy<IVisitPayDocumentService> visitPayDocumentService,
            Lazy<VisitPayUserService> visitPayUserService
        ) : base(applicationServiceCommonService)
        {
            this._visitPayDocumentService = visitPayDocumentService;
            this._visitPayUserService = visitPayUserService;
        }

        public void AddVisitPayDocument(VisitPayDocumentDto visitPayDocumentDto)
        {
            VisitPayDocument visitPayDocument = Mapper.Map<VisitPayDocument>(visitPayDocumentDto);
            this._visitPayDocumentService.Value.AddVisitPayDocument(visitPayDocument);
        }

        public IList<VisitPayDocumentResultDto> GetVisitPayDocumentResults(VisitPayDocumentFilter visitPayDocumentFilter)
        {
            VisitPayDocumentFilter filter = Mapper.Map<VisitPayDocumentFilter>(visitPayDocumentFilter);
            IList<VisitPayDocumentResult> visitPayDocumentResults = this._visitPayDocumentService.Value.GetVisitPayDocumentResults(filter);
            IList<VisitPayDocumentResultDto> visitPayDocumentResultDtos =
                Mapper.Map<IList<VisitPayDocumentResultDto>>(visitPayDocumentResults);

            visitPayDocumentResultDtos.ForEach(x =>
            {
                x.UploadedByUserName = this._visitPayUserService.Value.FindById(x.UploadedByVpUserId.ToString()).UserName;
                if (x.ApprovedByVpUserId.HasValue)
                {
                    x.ApprovedByUserName = this._visitPayUserService.Value.FindById(x?.ApprovedByVpUserId.ToString())?.UserName;
                }
                
            });
               
            return visitPayDocumentResultDtos;
        }

        public VisitPayDocumentResultDto GetVisitPayDocumentResultDtoById(int visitPayDocumentId)
        {
            VisitPayDocumentResult visitPayDocumentResult = this._visitPayDocumentService.Value.GetVisitPayDocumentResultById(visitPayDocumentId);
            VisitPayDocumentResultDto visitPayDocumentResultDto = Mapper.Map<VisitPayDocumentResultDto>(visitPayDocumentResult);
            return visitPayDocumentResultDto;
        }

        public VisitPayDocumentDto GetVisitPayDocumentDtoById(int visitPayDocumentId)
        {
            VisitPayDocument visitPayDocument = this._visitPayDocumentService.Value.GetVisitPayDocumentId(visitPayDocumentId);
            VisitPayDocumentDto visitPayDocumentDto = Mapper.Map<VisitPayDocumentDto>(visitPayDocument);
            return visitPayDocumentDto;
        }

        public void SaveVisitPayDocument(VisitPayDocumentEditDto visitPayDocumentEdit)
        {
            VisitPayDocument visitPayDocument = this._visitPayDocumentService.Value.GetVisitPayDocumentId(visitPayDocumentEdit.VisitPayDocumentId);
            visitPayDocument = Mapper.Map(visitPayDocumentEdit, visitPayDocument);
            this._visitPayDocumentService.Value.SavevisitPayDocumentId(visitPayDocument);
        }

        public void DeleteVisitPayDocument(int visitPayDocumentId)
        {
            this._visitPayDocumentService.Value.DeletevisitPayDocumentId(visitPayDocumentId);
        }
    }
}
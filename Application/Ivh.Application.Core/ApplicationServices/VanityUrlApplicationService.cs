﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ivh.Application.Core.ApplicationServices
{
    using AutoMapper;
    using Common.Dtos;
    using Common.Interfaces;
    using Domain.Core.VanityUrl.Entities;
    using Domain.Core.VanityUrl.Interfaces;

    public class VanityUrlApplicationService : IVanityUrlApplicationService
    {
        private Lazy<IVanityUrlService> _vanityUrlService;

        public VanityUrlApplicationService(Lazy<IVanityUrlService> vanityUrlService)
        {
            this._vanityUrlService = vanityUrlService;
        }

        public VanityUrlDto GetVanityUrl(string vanityUrlKeyName)
        {
            VanityUrl vanityUrl = this._vanityUrlService.Value.GetVanityUrl(vanityUrlKeyName);
            return Mapper.Map<VanityUrlDto>(vanityUrl);
        }
    }
}

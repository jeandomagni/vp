﻿namespace Ivh.Application.Core.ApplicationServices
{
    using System;
    using AutoMapper;
    using Base.Common.Interfaces;
    using Base.Services;
    using Common.Dtos;
    using Common.Interfaces;
    using Domain.Guarantor.Interfaces;
    using Domain.Visit.Interfaces;

    public class VisitStateHistoryApplicationService : ApplicationService, IVisitStateHistoryApplicationService
    {
        private readonly Lazy<IGuarantorService> _guarantorService;
        private readonly Lazy<IVisitStateHistoryService> _visitStateHistoryService;

        public VisitStateHistoryApplicationService(
            Lazy<IApplicationServiceCommonService> applicationServiceCommonService,
            Lazy<IGuarantorService> guarantorService,
            Lazy<IVisitStateHistoryService> visitStateHistoryService) : base(applicationServiceCommonService)
        {
            this._guarantorService = guarantorService;
            this._visitStateHistoryService = visitStateHistoryService;
        }

        public VisitStateHistoryResultsDto GetAllVisitStateHistory(int visitPayUserId, VisitStateHistoryFilterDto filterDto, int pageNumber, int pageSize, bool onlyChanges = false)
        {
            int guarantorId = this._guarantorService.Value.GetGuarantorId(visitPayUserId);
            VisitStateHistoryResults results = this._visitStateHistoryService.Value.GetAllVisitStateHistory(guarantorId, Mapper.Map<VisitStateHistoryFilter>(filterDto), Math.Max(pageNumber - 1, 0), pageSize, onlyChanges);

            return Mapper.Map<VisitStateHistoryResultsDto>(results);
        }
    }
}
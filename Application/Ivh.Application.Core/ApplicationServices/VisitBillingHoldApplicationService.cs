﻿namespace Ivh.Application.Core.ApplicationServices
{
    using System;
    using Base.Common.Interfaces;
    using Base.Services;
    using Common.Interfaces;
    using Domain.FinanceManagement.FinancePlan.Interfaces;
    using Domain.Visit.Entities;
    using Domain.Visit.Interfaces;
    using Ivh.Common.Data;
    using Ivh.Common.Data.Connection.Connections;
    using Ivh.Common.Data.Interfaces;
    using Ivh.Common.VisitPay.Messages.FinanceManagement;
    using NHibernate;

    public class VisitBillingHoldApplicationService : ApplicationService, IVisitBillingHoldApplicationService
    {
        private readonly Lazy<IFinancePlanService> _financePlanService;
        private readonly Lazy<IVisitService> _visitService;
        private readonly ISession _session;

        public VisitBillingHoldApplicationService(
            Lazy<IApplicationServiceCommonService> applicationServiceCommonService,
            Lazy<IVisitService> visitService,
            Lazy<IFinancePlanService> financePlanService,
            ISessionContext<VisitPay> sessionContext) : base(applicationServiceCommonService)
        {
            this._visitService = visitService;
            this._financePlanService = financePlanService;
            this._session = sessionContext.Session;
        }

        public void AddBillingHold(int billingSystemId, string sourceSystemKey)
        {
            using (UnitOfWork unitOfWork = new UnitOfWork(this._session))
            {
                Visit visit = this._visitService.Value.GetVisitBySystemSourceKey(sourceSystemKey, billingSystemId);

                if (visit == null)
                {
                    return;
                }

                visit.BillingHold = true;
                if (visit.BillingHold && this._financePlanService.Value.IsVisitInAFinancePlan(visit))
                {
                    // send finance plan visit suspended email message
                    this._session.Transaction.RegisterPostCommitSuccessAction(v =>
                    {
                        this.Bus.Value.PublishMessage(new SendFinancePlanVisitSuspendedEmailMessage
                        {
                            VpGuarantorId = visit.VpGuarantorId
                        }).Wait();
                    }, visit);
                }
                this._visitService.Value.SaveVisit(visit);
                unitOfWork.Commit();
            }
        }

        public void ClearBillingHold(int billingSystemId, string sourceSystemKey)
        {
            using (UnitOfWork unitOfWork = new UnitOfWork(this._session))
            {
                Visit visit = this._visitService.Value.GetVisitBySystemSourceKey(sourceSystemKey, billingSystemId);
                if (visit == null)
                {
                    return;
                }

                visit.BillingHold = false;
                this._visitService.Value.SaveVisit(visit);
                unitOfWork.Commit();
            }
        }
    }
}
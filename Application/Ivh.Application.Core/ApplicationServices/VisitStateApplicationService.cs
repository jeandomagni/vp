﻿namespace Ivh.Application.Core.ApplicationServices
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Base.Common.Interfaces;
    using Base.Services;
    using Common.Interfaces;
    using Domain.Visit.Entities;
    using Domain.Visit.Interfaces;
    using Domain.FinanceManagement.FinancePlan.Entities;
    using Domain.FinanceManagement.FinancePlan.Interfaces;
    using Ivh.Common.Data;
    using Ivh.Common.Data.Connection.Connections;
    using Ivh.Common.Data.Interfaces;
    using Ivh.Common.ServiceBus.Attributes;
    using Ivh.Common.ServiceBus.Enums;
    using Ivh.Common.VisitPay.Enums;
    using Ivh.Common.VisitPay.Messages.Aging;
    using Ivh.Common.VisitPay.Messages.HospitalData;
    using MassTransit;
    using NHibernate;

    public class VisitStateApplicationService : ApplicationService, IVisitStateApplicationService
    {
        private readonly Lazy<IFinancePlanService> _financePlanService;
        private readonly ISession _session;
        private readonly Lazy<IVisitService> _visitService;

        public VisitStateApplicationService(
            Lazy<IApplicationServiceCommonService> applicationServiceCommonService,
            Lazy<IVisitService> visitService,
            Lazy<IFinancePlanService> financePlanService,
            ISessionContext<VisitPay> sessionContext) : base(applicationServiceCommonService)
        {
            this._visitService = visitService;
            this._financePlanService = financePlanService;
            this._session = sessionContext.Session;
        }

        public void SetVisitIneligible(string systemSourceKey, int billingSystemId, string ineligibleReason)
        {
            Visit visit = this._visitService.Value.GetVisitBySystemSourceKey(systemSourceKey, billingSystemId);
            if (visit == null)
            {
                return;
            }

            using (UnitOfWork uow = new UnitOfWork(this._session))
            {
                this._visitService.Value.SetVisitToIneligible(visit, ineligibleReason);
                IList<FinancePlan> financePlans = this._financePlanService.Value.GetFinancePlansAssociatedWithVisits(new List<Visit> { visit });
                this._financePlanService.Value.CheckIfFinancePlansShouldBeClosedUncollectable(financePlans);
                this._financePlanService.Value.CheckIfPendingFinancePlanHasEligibleVisits(financePlans);
                uow.Commit();
            }
        }

        public void SetVisitEligible(string systemSourceKey, int billingSystemId)
        {
            Visit visit = this._visitService.Value.GetVisitBySystemSourceKey(systemSourceKey, billingSystemId);
            if (visit == null)
            {
                return;
            }
            
            using (UnitOfWork uow = new UnitOfWork(this._session))
            {
                this._visitService.Value.SetVisitToEligible(visit);
                IList<FinancePlan> financePlans = this._financePlanService.Value.GetFinancePlansAssociatedWithVisits(new List<Visit> { visit });
                this._financePlanService.Value.CheckIfFinancePlanShouldBeActiveUncollectable(financePlans?.FirstOrDefault());

                bool resetAgingTier = visit.CanResetAgingTierUponVpEligible() && this.FeatureService.Value.IsFeatureEnabled(VisitPayFeatureEnum.FeatureResetAgingTierForReactivatedMaxAgeVisits);
                
                if(resetAgingTier)
                {
                    AgingTierEnum resetToAgingTier = this.Client.Value.AgingTierWhenReactivatingMaxAgeVisit;
                    SetVisitAgeMessage message = new SetVisitAgeMessage
                    {
                        Visit = new Ivh.Common.VisitPay.Messages.Aging.Dtos.VisitDto
                        {
                            VisitBillingSystemId = visit.BillingSystemId ?? default(int),
                            VisitSourceSystemKey = visit.SourceSystemKey
                        },
                        AgingTierEnum = resetToAgingTier,
                        ActionVisitPayUserId = -1,
                        SetToActive = true
                    };

                    this._session.Transaction.RegisterPostCommitSuccessAction(m =>
                    {
                        this.Bus.Value.PublishMessage(m).Wait();
                    }, message);
                }

                uow.Commit();
            }
        }

        #region Message Consumers

        [UniversalConsumer(UniversalQueuesEnum.VisitPayProcessor, UniversalPriorityEnum.Priority30)]
        public void ConsumeMessage(HsVisitVpEligibleMessage message, ConsumeContext<HsVisitVpEligibleMessage> consumeContext)
        {
            if (message.VpEligible)
            {
                this.SetVisitEligible(message.SystemSourceKey, message.BillingSystemId);
            }
            else
            {
                this.SetVisitIneligible(message.SystemSourceKey, message.BillingSystemId, message.HardRecallReason);
            }
        }

        public bool IsMessageValid(HsVisitVpEligibleMessage message, ConsumeContext<HsVisitVpEligibleMessage> consumeContext)
        {
            bool isValid = true;
            return isValid;
        }

        #endregion
    }
}
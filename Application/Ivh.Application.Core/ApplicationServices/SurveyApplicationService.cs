﻿namespace Ivh.Application.Core.ApplicationServices
{
    using Common.Dtos;
    using Common.Interfaces;
    using Domain.AppIntelligence.Survey.Entities;
    using Domain.AppIntelligence.Survey.Interfaces;
    using Ivh.Common.Base.Enums;
    using AutoMapper;
    using Base.Common.Interfaces;
    using Base.Services;
    using Domain.Content.Interfaces;
    using Domain.Guarantor.Interfaces;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Domain.Content.Entities;
    using FinanceManagement.Common.Dtos;
    using FinanceManagement.Common.Interfaces;
    using Ivh.Common.Base.Utilities.Helpers;
    using Ivh.Common.VisitPay.Enums;

    public class SurveyApplicationService : ApplicationService, ISurveyApplicationService
    {
        private readonly Lazy<ISurveyService> _surveyService;
        private readonly Lazy<IStatementApplicationService> _statementApplicationService;
        private readonly Lazy<IGuarantorService> _guarantorService;
        private readonly Lazy<IContentService> _contentService;

        public SurveyApplicationService(
            Lazy<IApplicationServiceCommonService> applicationServiceCommonService,
            Lazy<ISurveyService> surveyService,
            Lazy<IStatementApplicationService> statementApplicationService,
            Lazy<IGuarantorService> guarantorService,
            Lazy<IContentService> contentService) : base(applicationServiceCommonService)
        {
            this._surveyService = surveyService;
            this._statementApplicationService = statementApplicationService;
            this._guarantorService = guarantorService;
            this._contentService = contentService;
        }

        //this will no longer be a bool but will return a string or a model - the string will represent the surveyname 
        public SurveyPresentationInfoDto ShouldShowSurveyToVisitPayUser(int visitPayUserId, string surveyName, bool userHasBeenSurveyedInSession = false)
        {
            SurveyPresentationInfoDto defaultSurveyPresentationInfoDto = new SurveyPresentationInfoDto();

            //1a. If the user has already been shown a survey within the session, do not show another survey
            if (userHasBeenSurveyedInSession)
            {
                return defaultSurveyPresentationInfoDto;
            }

            //1b. Is the feature enabled 
            if (!this.FeatureService.Value.IsFeatureEnabled(VisitPayFeatureEnum.Surveys))
            {
                return defaultSurveyPresentationInfoDto;
            }

            Survey survey = this._surveyService.Value.GetSurveyBySurveyName(surveyName);
            if (survey == null)
            {
                return defaultSurveyPresentationInfoDto;
            }

            //if the survey is part of a group then take a different path
            if ((int) survey.SurveyGroup > 0)
            {
                return this.SurveyFromSurveyGroup(visitPayUserId, survey.SurveyGroup);
            }
            //2.is it active?
            if (!survey.IsActive)
            {
                return defaultSurveyPresentationInfoDto;
            }

            //3. Has the user not been presented with or ignored within the window?  
            // change in VP-3213. We look at all survey results to determine when show a survey to a user
            List<SurveyResult> results =  this._surveyService.Value.GetAllSurveyResultsForVisitPayUser(visitPayUserId);
            // user has not been presented with a survey
            if (results == null || results.Count == 0)
            {
                return new SurveyPresentationInfoDto { UserIsPresentedWithSurvey = true, SurveyName = surveyName };
            }

            //4. Determine the user should be presented with a survey based on the latest survey and the frequency 
            bool userHasNotTakenOrIgnoredWithinWindow = this.UserHasNotTakenOrIgnoredWithinWindow(
                results,
                survey.FrequencyInDays,
                survey.FrequencyInDaysWhenIgnored
            );

            if (!userHasNotTakenOrIgnoredWithinWindow)
            {
                return defaultSurveyPresentationInfoDto;
            }

            return new SurveyPresentationInfoDto {UserIsPresentedWithSurvey = true, SurveyName = surveyName};
        }

        private SurveyPresentationInfoDto SurveyFromSurveyGroup(int visitPayUserId, SurveyGroupEnum surveyGroup)
        {
            List<Survey> surveysInGroup = this._surveyService.Value.GetActiveSurveysByGroup(surveyGroup);
            if (surveysInGroup == null)
            {
                return new SurveyPresentationInfoDto();
            }

            List<SurveyResult> results = this._surveyService.Value.GetAllSurveyResultsForVisitPayUser(visitPayUserId);
            //scenario 1.
            //if the user has not been presented with any survey in that group then it should just
            //return a random surveyName that is in that group
            if (results == null || results.Count == 0)
            {
                //product wants an even sampling so we don't want to just return the survey name originally passed in
                return this.SelectRandomSurveyFromSurveyGroup(surveysInGroup);
            }

            //has enough time elapse since the user has taken or ignored a survey
            //note, all surveys should share the same frequency properties - (not changing schema as this concept could be reverted)
            Survey defaultSurvey = surveysInGroup.First();
            bool userHasNotTakenOrIgnoredWithinWindow = this.UserHasNotTakenOrIgnoredWithinWindow(
                results,
                defaultSurvey.FrequencyInDays,
                defaultSurvey.FrequencyInDaysWhenIgnored
            );

            if (!userHasNotTakenOrIgnoredWithinWindow)
            {
                //user has been recently presented- suppress the survey
                return new SurveyPresentationInfoDto();
            }

            int numberOfSurveysInGroup = surveysInGroup.Count();
            //If there's only one survey in surveysInGroup, just return the survey
            // ex.  there was a group that had 2 surveys, then one was turned off. 
            if (numberOfSurveysInGroup == 1)
            {
                return new SurveyPresentationInfoDto
                {
                    UserIsPresentedWithSurvey = true,
                    SurveyName = defaultSurvey.SurveyName
                };
            }

            //scenario 2. the user has taken a survey in the past and is eligible to take another
            //need to determine if a survey in the past is part of the survey group
            var surveyGroupInResults = results.Join(surveysInGroup,
                    result => result.SurveyId,
                    survey => survey.SurveyId,
                    (result, survey) => new {SurveyName = survey.SurveyName, SurveyGroup = survey.SurveyGroup, result.SurveyDate})
                .Where(x => x.SurveyGroup == surveyGroup).ToList();

            if (!surveyGroupInResults.Any())
            {
                //The user has been presented with a survey before, just not one from this group 
                return this.SelectRandomSurveyFromSurveyGroup(surveysInGroup);
            }

            List<string> surveyNamesThatHaveBeenTaken = surveyGroupInResults.Select(x => x.SurveyName).ToList();
            List<Survey> surveysInGroupNotInResults = surveysInGroup.Where(x => 
                    !surveyNamesThatHaveBeenTaken.Contains(x.SurveyName))
                .ToList();

            // Maybe the user is eligible for a survey but has taken all the surveys in the group
            if (!surveysInGroupNotInResults.Any())
            {
                return this.SelectRandomSurveyFromSurveyGroup(surveysInGroup);
            }

            return this.SelectRandomSurveyFromSurveyGroup(surveysInGroupNotInResults);
        }


        private SurveyPresentationInfoDto SelectRandomSurveyFromSurveyGroup(List<Survey> surveysInGroup)
        {
            Survey randomSurveyFromGroup = surveysInGroup.OrderBy(x => Guid.NewGuid()).First();
            return new SurveyPresentationInfoDto
            {
                UserIsPresentedWithSurvey = true,
                SurveyName = randomSurveyFromGroup.SurveyName
            };
        }

        private bool UserHasNotTakenOrIgnoredWithinWindow(List<SurveyResult> results, int frequencyInDays, int frequencyInDaysWhenIgnored)
        {
            //NOTE- all active surveys should have the exact same FrequencyInDays FrequencyInDaysWhenIgnored 
            // Also, we are changing the logic starting with VP-3212 on the frequency we show surveys.
            // We are no longer constraining based on the survey.
            // We are considering any survey in the decision making - taking or ignoring 
            // If a user ignores or takes a survey he/she will not see another survey until enough time elapses 
            //4. Determine the user should be presented with a survey based on the latest survey and the frequency 
            SurveyResult lastSurvey = results.OrderByDescending(x => x.SurveyDate).First();
            DateTime lastSurveyDate = lastSurvey.SurveyDate;
            int daysToAdd = lastSurvey.SurveyResultType == SurveyResultTypeEnum.Ignored ? frequencyInDaysWhenIgnored : frequencyInDays;
            DateTime newSurveyDate = lastSurveyDate.AddDays(daysToAdd);

            if (newSurveyDate > DateTime.UtcNow)
            {
                return false;
            }

            return true;

        }



        public async Task<SurveyDto> GetSurvey(string surveyName)
        {
            Survey survey = this._surveyService.Value.GetSurveyBySurveyName(surveyName);
            SurveyDto surveyDto = Mapper.Map<SurveyDto>(survey);
            await this.ApplyCmsRegions(surveyDto);
            return surveyDto;
        }

        private async Task ApplyCmsRegions(SurveyDto surveyDto)
        {
            //survey title 
            CmsVersion cmsVersion = await this._contentService.Value.GetCurrentVersionAsync(surveyDto.TitleCmsRegion, true, null);
            surveyDto.Title = cmsVersion?.ContentBody ?? "";

            //question 
            surveyDto.SurveyQuestions.ForEach(async x =>
            {
                CmsVersion questionCmsVersion = await this._contentService.Value.GetCurrentVersionAsync(x.QuestionCmsRegion, true, null);
                x.QuestionText = questionCmsVersion?.ContentBody ?? "";
                // rating 
                x.SurveyRatingGroup.SurveyRatings.ForEach(async r =>
                {
                    CmsVersion ratingCmsVersion = await this._contentService.Value.GetCurrentVersionAsync(r.DescriptionCmsRegion, true, null);
                    r.SurveyRatingDescription = ratingCmsVersion?.ContentBody ?? "";
                });
            });
        }
        
        public void ProcessSurveyResult(SurveyResultDto surveyResult)
        {
            if (surveyResult.SurveyResultType == SurveyResultTypeEnum.Ignored)
            {
                surveyResult.SurveyResultAnswers = null;
            }
            else
            {
                //remove any null responses and make sure the child references the parents
                IEnumerable<SurveyResultAnswerDto> answers = surveyResult.SurveyResultAnswers.Where(x => x.SurveyQuestionRating != null);

                if (answers != null)
                    answers.Where(x => x.SurveyResult == null).ToList().ForEach(x => { x.SurveyResult = surveyResult; });

                surveyResult.SurveyResultAnswers = answers.ToList();
            }

            if (surveyResult.VisitPayUserId.HasValue)
            {
                int gaurantorId = this._guarantorService.Value.GetGuarantorId(surveyResult.VisitPayUserId.Value);
                StatementDto statement = this._statementApplicationService.Value.GetLastStatement(gaurantorId);
                if (statement != null)
                {
                    surveyResult.VpStatementId = statement.VpStatementId;
                }
            }

            SurveyResult entity = Mapper.Map<SurveyResult>(surveyResult);
            this._surveyService.Value.ProcessSurveyResults(entity);
        }

    }
}

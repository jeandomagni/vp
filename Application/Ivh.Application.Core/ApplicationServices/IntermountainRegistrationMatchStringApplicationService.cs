﻿namespace Ivh.Application.Core.ApplicationServices
{
    using System.Collections.Generic;
    using Common.Dtos;
    using Common.Interfaces;

    public class IntermountainRegistrationMatchStringApplicationService : IRegistrationMatchStringApplicationService
    {
        public IReadOnlyList<HsGuarantorDto> TransformRegistrationMatchString(IEnumerable<HsGuarantorDto> hsGuarantors)
        {
            foreach (HsGuarantorDto hsg in hsGuarantors)
            {
                hsg.SourceSystemKey = $"107_{hsg.SourceSystemKey}";
            }
            return (IReadOnlyList<HsGuarantorDto>) hsGuarantors;
        }
    }
}
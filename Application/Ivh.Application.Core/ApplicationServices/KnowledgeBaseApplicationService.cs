﻿namespace Ivh.Application.Core.ApplicationServices
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading;
    using System.Threading.Tasks;
    using AutoMapper;
    using Base.Common.Interfaces;
    using Base.Services;
    using Common.Dtos;
    using Common.Interfaces;
    using Content.Common.Dtos;
    using Content.Common.Interfaces;
    using Domain.Core.KnowledgeBase.Entities;
    using Domain.Core.KnowledgeBase.Interfaces;
    using Domain.Settings.Interfaces;
    using Ivh.Common.Base.Enums;
    using Ivh.Common.Base.Utilities.Extensions;
    using Ivh.Common.VisitPay.Enums;
    using Ivh.Common.VisitPay.Messages.Core;

    /// <summary>
    ///     Knowledge Base should be able to show questions (grouped by categories or by categories and subcategories)
    ///     Categories should not be shown if not IsActive
    ///     Subcategories should not be shown if not IsActive
    ///     Questions should not be shown is not IsActive
    ///     A subcategory should not be shown if there are no questions that are active or not applicable (whether feature
    ///     bound or not relavant to the application the user is using)
    ///     A category should not be return if there are not subcategoris or questions under it
    ///     Questions are filtered by IsActive, application type (mobile, web, client) or feature bound
    /// </summary>
    public class KnowledgeBaseApplicationService : ApplicationService, IKnowledgeBaseApplicationService
    {
        private const int CategoriesCacheExpirationInSeconds = 60 * 60;
        private static readonly Dictionary<CmsRegionEnum, FeatureRequirement> FeaturesToCheck = new Dictionary<CmsRegionEnum, FeatureRequirement>
        {
            [CmsRegionEnum.NotReceivingTextMessagesQuestion] = new FeatureRequirement { FeatureEnum = VisitPayFeatureEnum.Sms, ShouldBeEnabled = true },
            [CmsRegionEnum.TurnOnTextMessagingQuestion] = new FeatureRequirement { FeatureEnum = VisitPayFeatureEnum.Sms, ShouldBeEnabled = true },
            [CmsRegionEnum.CanAddVisitToExistingFinancePlanQuestion] = new FeatureRequirement { FeatureEnum = VisitPayFeatureEnum.CombineFinancePlans, ShouldBeEnabled = true },
            [CmsRegionEnum.PaymentMethodsAcceptedAnswer] = new FeatureRequirement { FeatureEnum = VisitPayFeatureEnum.PaymentsAch, ShouldBeEnabled = true },
            [CmsRegionEnum.PaymentMethodsAcceptedAnswerNoAch] = new FeatureRequirement { FeatureEnum = VisitPayFeatureEnum.PaymentsAch, ShouldBeEnabled = false },
            [CmsRegionEnum.CanReschedulePaymentQuestion] = new FeatureRequirement { FeatureEnum = VisitPayFeatureEnum.RescheduleRecurringPaymentGuarantor, ShouldBeEnabled = true },
            [CmsRegionEnum.CanReschedulePaymentAnswer] = new FeatureRequirement { FeatureEnum = VisitPayFeatureEnum.RescheduleRecurringPaymentGuarantor, ShouldBeEnabled = true },
            [CmsRegionEnum.HowToConsolidateQuestion] = new FeatureRequirement { FeatureEnum = VisitPayFeatureEnum.Consolidation, ShouldBeEnabled = true },
            [CmsRegionEnum.HowDoesConsolidationWorkQuestion] = new FeatureRequirement { FeatureEnum = VisitPayFeatureEnum.Consolidation, ShouldBeEnabled = true }
        };

        private readonly Lazy<IContentApplicationService> _contentApplicationService;
        private readonly Lazy<IFeatureService> _featureService;
        private readonly Lazy<IKnowledgeBaseService> _knowledgeBaseService;

        private KnowledgeBaseApplicationTypeEnum _knowledgeBaseApplicationType = KnowledgeBaseApplicationTypeEnum.Web;

        public KnowledgeBaseApplicationService(
            Lazy<IApplicationServiceCommonService> applicationServiceCommonService,
            Lazy<IKnowledgeBaseService> knowledgeBaseService,
            Lazy<IContentApplicationService> contentApplicationService,
            Lazy<IFeatureService> featureService) : base(applicationServiceCommonService)
        {
            this._knowledgeBaseService = knowledgeBaseService;
            this._contentApplicationService = contentApplicationService;
            this._featureService = featureService;
        }

        private static readonly SemaphoreSlim GetKnowledgeBaseCategoriesSemaphore = new SemaphoreSlim(1, 1);
        private const string KnowledgeBaseCacheKey = nameof(KnowledgeBaseApplicationService) + "." + nameof(GetKnowledgeBaseCategoriesAsync) + "::{0}::{1}";

        private static readonly SemaphoreSlim GetTopVotedKnowledgeBaseQuestionAnswersSemaphore = new SemaphoreSlim(1, 1);
        private const string TopKnowledgeBaseQuestionAnswersCacheKey = nameof(KnowledgeBaseApplicationService) + "." + nameof(GetTopVotedKnowledgeBaseQuestionAnswersAsync) + "::{0}::{1}";

        public async Task<IList<KnowledgeBaseCategoryDto>> GetKnowledgeBaseCategoriesAsync(KnowledgeBaseApplicationTypeEnum applicationType)
        {
            this._knowledgeBaseApplicationType = applicationType;

            string cacheKey = string.Format(KnowledgeBaseCacheKey, $"{applicationType}", Thread.CurrentThread.GetLocale());
            List<KnowledgeBaseCategoryDto> categoriesWithSubCategories = await this.Cache.Value.GetObjectAsync<List<KnowledgeBaseCategoryDto>>(cacheKey);
            if (categoriesWithSubCategories != null)
            {
                return categoriesWithSubCategories;
            }

            await GetKnowledgeBaseCategoriesSemaphore.WaitAsync();
            try
            {
                categoriesWithSubCategories = await this.Cache.Value.GetObjectAsync<List<KnowledgeBaseCategoryDto>>(cacheKey);
                if (categoriesWithSubCategories != null)
                {
                    return categoriesWithSubCategories;
                }

                IList<KnowledgeBaseCategory> knowledgebaseCategories = this._knowledgeBaseService.Value.GetActiveKnowledgeBaseCategories();
                categoriesWithSubCategories = new List<KnowledgeBaseCategoryDto>();

                foreach (KnowledgeBaseCategory category in knowledgebaseCategories)
                {
                    KnowledgeBaseCategoryDto categoryDto = Mapper.Map<KnowledgeBaseCategoryDto>(category);

                    foreach (KnowledgeBaseSubCategory subCategory in category.KnowledgeBaseSubCategories.Where(x => x.IsActive))
                    {
                        KnowledgeBaseSubCategoryDto subcategoryDto = Mapper.Map<KnowledgeBaseSubCategoryDto>(subCategory);
                        if (subCategory.NameCmsRegion.HasValue)
                        {
                            CmsVersionDto subCategoryCmsVersion = await this.GetCmsVersion(subCategory.NameCmsRegion.Value, null);
                            subcategoryDto.Name = subCategoryCmsVersion?.ContentTitle ?? string.Empty;
                        }
                            
                        subcategoryDto.QuestionAnswers.AddRange(subCategory.KnowledgeBaseSubCategoryQuestionAnswerXref
                            .Select(xref =>
                            {
                                KnowledgeBaseQuestionAnswerDto questionAnswerDto = Mapper.Map<KnowledgeBaseQuestionAnswerDto>(xref.KnowledgeBaseQuestionAnswer);
                                questionAnswerDto.DisplayOrder = xref.DisplayOrder;
                                questionAnswerDto.Tags = xref.Tags.Select(x => x.KnowledgeBaseTag.KnowledgeBaseTagName).ToList();
                                questionAnswerDto.Routes = xref.Routes.Select(x => $"{x.Route.Controller}/{x.Route.Action}").ToList();
                                return questionAnswerDto;
                            }).ToList());

                        // the xref is shared at the app level, however, some clients do not have a version for the question or answer
                        // applicableQuestions will only return question/answers with content
                        subcategoryDto.QuestionAnswers = this.FilterQuestionAndAnswers(subcategoryDto.QuestionAnswers.Where(x => x.IsActive));

                        await this.ApplyContentToQuestionsFromCategoryAsync(subcategoryDto);

                        subcategoryDto.QuestionAnswers = subcategoryDto.QuestionAnswers?.Where(x => x.AnswerContentBody?.Length > 0 && x.QuestionContentBody?.Length > 0).ToList();
                        if (subcategoryDto.QuestionAnswers?.Count > 0)
                        {
                            categoryDto.KnowledgeBaseSubCategories.Add(subcategoryDto);
                        }
                    }

                    categoriesWithSubCategories.Add(categoryDto);
                }

                await this.ApplyContentToCategoryAsync(categoriesWithSubCategories);

                await this.Cache.Value.SetObjectAsync(cacheKey, categoriesWithSubCategories, CategoriesCacheExpirationInSeconds);
            }
            finally
            {
                GetKnowledgeBaseCategoriesSemaphore.Release();
            }

            return categoriesWithSubCategories;
        }

        public async Task<IList<KnowledgeBaseCategoryQuestionsDto>> GetKnowledgeBaseCategoryQuestionsFlattenedAsync(KnowledgeBaseApplicationTypeEnum applicationType)
        {
            IList<KnowledgeBaseCategoryQuestionsDto> flattened = await this.GetKnowledgeBaseFlattenedAsync<KnowledgeBaseCategoryQuestionsDto, KnowledgeBaseQuestionDto>(applicationType);

            return flattened;
        }

        public async Task<IList<KnowledgeBaseCategoryQuestionAnswersDto>> GetKnowledgeBaseCategoryQuestionAnswersFlattenedAsync(KnowledgeBaseApplicationTypeEnum applicationType)
        {
            IList<KnowledgeBaseCategoryQuestionAnswersDto> flattened = await this.GetKnowledgeBaseFlattenedAsync<KnowledgeBaseCategoryQuestionAnswersDto, KnowledgeBaseQuestionAnswerDto>(applicationType);

            return flattened;
        }

        public async Task<IList<KnowledgeBaseCategoryQuestionAnswersDto>> GetKnowledgeBaseCategoryQuestionAnswersForRouteAsync(KnowledgeBaseApplicationTypeEnum applicationType, string controller, string action)
        {
            IList<KnowledgeBaseCategoryQuestionAnswersDto> questionAnswers = await this.GetKnowledgeBaseFlattenedAsync<KnowledgeBaseCategoryQuestionAnswersDto, KnowledgeBaseQuestionAnswerDto>(
                applicationType,
                questionFunc: x => x.Routes != null && x.Routes.Any(y => $"{controller}/{action}".Equals(y, StringComparison.InvariantCultureIgnoreCase)));

            if (questionAnswers.SelectMany(x => x.Questions).Any())
            {
                return questionAnswers;
            }

            IList<KnowledgeBaseCategoryQuestionAnswersDto> defaultQuestionAnswers = await this.GetKnowledgeBaseCategoryQuestionAnswersDefaultAsync(applicationType);

            return defaultQuestionAnswers;
        }

        public async Task<KnowledgeBaseCategoryQuestionAnswersDto> GetKnowledgeBaseCategoryQuestionAnswerAsync(KnowledgeBaseApplicationTypeEnum applicationType, int subcategoryId, int categoryId, int questionId)
        {
            IList<KnowledgeBaseCategoryQuestionAnswersDto> result = await this.GetKnowledgeBaseFlattenedAsync<KnowledgeBaseCategoryQuestionAnswersDto, KnowledgeBaseQuestionAnswerDto>(
                applicationType,
                x => x.KnowledgeBaseCategoryId == categoryId,
                x => x.KnowledgeBaseSubCategoryId == subcategoryId,
                x => x.KnowledgeBaseQuestionAnswerId == questionId);

            return result.FirstOrDefault();
        }
        
        public async Task LogSearchTextAsync(string searchText)
        {
            await this.Bus.Value.PublishMessage(new SearchTextLogMessage
            {
                SearchText = searchText,
                Date = DateTime.UtcNow
            });
        }

        public void SaveKnowledgeBaseQuestionAnswerVote(int knowledgeBaseQuestionAnswerId, int voteValue)
        {
            this._knowledgeBaseService.Value.SaveKnowledgeBaseQuestionAnswerVote(knowledgeBaseQuestionAnswerId,voteValue);
        }

        public void SaveKnowledgeBaseQuestionAnswerFeedback(int knowledgeBaseQuestionAnswerId, string feedbackText)
        {
            this._knowledgeBaseService.Value.SaveKnowledgeBaseQuestionAnswerFeedback(knowledgeBaseQuestionAnswerId,feedbackText);
        }
        
        public async Task<List<KnowledgeBaseQuestionAnswerDto>> GetTopVotedKnowledgeBaseQuestionAnswersAsync(KnowledgeBaseApplicationTypeEnum applicationType)
        {
            this._knowledgeBaseApplicationType = applicationType;
            
            const int windowInDays = 90;
            const int numberOfQuestions = 3;

            string cacheKey = string.Format(TopKnowledgeBaseQuestionAnswersCacheKey, $"{applicationType}", Thread.CurrentThread.GetLocale());
            List<KnowledgeBaseQuestionAnswerDto> topQuestionAnswers = await this.Cache.Value.GetObjectAsync<List<KnowledgeBaseQuestionAnswerDto>>(cacheKey);
            if (topQuestionAnswers != null)
            {
                return topQuestionAnswers;
            }

            await GetTopVotedKnowledgeBaseQuestionAnswersSemaphore.WaitAsync();
            try
            {
                IList<KnowledgeBaseQuestionAnswer> questionAnswers = this._knowledgeBaseService.Value.GetTopVotedKnowledgeBaseQuestionAnswers(windowInDays, numberOfQuestions);
                topQuestionAnswers = this.FilterQuestionAndAnswers(Mapper.Map<IEnumerable<KnowledgeBaseQuestionAnswerDto>>(questionAnswers));
                if (topQuestionAnswers?.Any() ?? false)
                {
                    await this.Cache.Value.SetObjectAsync(cacheKey, topQuestionAnswers, CategoriesCacheExpirationInSeconds);
                }
            }
            finally
            {
                GetTopVotedKnowledgeBaseQuestionAnswersSemaphore.Release();
            }

            return topQuestionAnswers;
        }
        
        private async Task<IList<TE>> GetKnowledgeBaseFlattenedAsync<TE, T>(
            KnowledgeBaseApplicationTypeEnum applicationType,
            Func<KnowledgeBaseCategoryDto, bool> categoryFunc = null,
            Func<KnowledgeBaseSubCategoryDto, bool> subCategoryFunc = null,
            Func<KnowledgeBaseQuestionAnswerDto, bool> questionFunc = null) where TE : KnowledgeBaseFlattenedDto<T> where T : BaseKnowledgeBaseQuestionDto
        {
            IList<KnowledgeBaseCategoryDto> categoriesWithSubCategories = await this.GetKnowledgeBaseCategoriesAsync(applicationType);

            categoriesWithSubCategories = categoriesWithSubCategories.Where(categoryFunc ?? ((x) => true)).ToList();

            foreach (KnowledgeBaseCategoryDto category in categoriesWithSubCategories)
            {
                category.KnowledgeBaseSubCategories = category.KnowledgeBaseSubCategories.Where(subCategoryFunc ?? ((x) => true)).ToList();

                foreach (KnowledgeBaseSubCategoryDto subCategory in category.KnowledgeBaseSubCategories)
                {
                    subCategory.QuestionAnswers = subCategory.QuestionAnswers.Where(questionFunc ?? ((y) => true)).ToList();
                }
            }

            return Mapper.Map<IList<TE>>(categoriesWithSubCategories);
        }

        private async Task<IList<KnowledgeBaseCategoryQuestionAnswersDto>> GetKnowledgeBaseCategoryQuestionAnswersDefaultAsync(KnowledgeBaseApplicationTypeEnum applicationType)
        {
            IList<KnowledgeBaseCategoryQuestionAnswersDto> flattened = await this.GetKnowledgeBaseFlattenedAsync<KnowledgeBaseCategoryQuestionAnswersDto, KnowledgeBaseQuestionAnswerDto>(
                applicationType,
                questionFunc: x => x.Routes != null && x.Routes.Any(y => "Home/Index".Equals(y, StringComparison.InvariantCultureIgnoreCase)));

            return flattened;
        }
        
        private bool IsFeatureRequirementSatisfied(CmsRegionEnum cmsRegionEnum)
        {
            return !FeaturesToCheck.ContainsKey(cmsRegionEnum) || this._featureService.Value.IsFeatureEnabled(FeaturesToCheck[cmsRegionEnum].FeatureEnum) == FeaturesToCheck[cmsRegionEnum].ShouldBeEnabled;
        }

        private bool ShouldShowQuestionForApplication<T>(T question) where T : BaseKnowledgeBaseQuestionDto
        {
            switch (this._knowledgeBaseApplicationType)
            {
                case KnowledgeBaseApplicationTypeEnum.Client:
                    return question.IsClientKnowledgeBase;
                case KnowledgeBaseApplicationTypeEnum.Web:
                    return question.IsWebKnowledgeBase;
                case KnowledgeBaseApplicationTypeEnum.Mobile:
                    return question.IsMobileKnowledgeBase;
                default:
                    return true;
            }
        }
        
        private List<KnowledgeBaseQuestionAnswerDto> FilterQuestionAndAnswers(IEnumerable<KnowledgeBaseQuestionAnswerDto> inboundQuestions)
        {
            return inboundQuestions
                .Where(x => this.IsFeatureRequirementSatisfied(x.QuestionCmsRegion) &&
                            this.IsFeatureRequirementSatisfied(x.AnswerCmsRegion) &&
                            this.ShouldShowQuestionForApplication(x))
                .OrderBy(d => d.DisplayOrder)
                .ToList();
        }
        
        private async Task<KnowledgeBaseSubCategoryDto> ApplyContentToQuestionsFromCategoryAsync(KnowledgeBaseSubCategoryDto knowledgeBaseSubCategory)
        {
            await this.ApplyContentToQuestionsAsync(knowledgeBaseSubCategory.QuestionAnswers);
            return knowledgeBaseSubCategory;
        }

        private async Task ApplyContentToQuestionsAsync<T>(IList<T> questions) where T : BaseKnowledgeBaseQuestionDto
        {
            foreach (T question in questions)
            {
                CmsVersionDto questionCmsRegion = await this.GetCmsVersion(question.QuestionCmsRegion, null);
                question.QuestionContentBody = questionCmsRegion.ContentBody;
                question.QuestionContentTitle = questionCmsRegion.ContentTitle;

                if (typeof(T) != typeof(KnowledgeBaseQuestionAnswerDto))
                {
                    continue;
                }

                KnowledgeBaseQuestionAnswerDto questionAnswer = question as KnowledgeBaseQuestionAnswerDto;
                if (questionAnswer == null)
                {
                    continue;
                }

                CmsVersionDto answerCmsRegion = await this.GetCmsVersion(questionAnswer.AnswerCmsRegion, null);
                questionAnswer.AnswerContentBody = answerCmsRegion.ContentBody;
                questionAnswer.AnswerContentTitle = answerCmsRegion.ContentTitle;
            }
        }

        private async Task ApplyContentToCategoryAsync<T>(IList<T> categories) where T : BaseKnowledgeBaseCategoryDto
        {
            foreach (T category in categories)
            {
                CmsVersionDto questionCmsRegion = await this.GetCmsVersion(category.CmsRegion, null);
                category.ContentBody = questionCmsRegion.ContentBody;
                category.ContentTitle = questionCmsRegion.ContentTitle;
            }
        }

        private async Task<CmsVersionDto> GetCmsVersion(CmsRegionEnum cmsRegionEnum, IDictionary<string, string> replacementValues)
        {
            CmsVersionDto cmsVersionDto = await this._contentApplicationService.Value.GetCurrentVersionAsync(cmsRegionEnum, true, replacementValues);

            return cmsVersionDto;
        }

        private class FeatureRequirement
        {
            public VisitPayFeatureEnum FeatureEnum { get; set; }
            public bool ShouldBeEnabled { get; set; }
        }
    }
}
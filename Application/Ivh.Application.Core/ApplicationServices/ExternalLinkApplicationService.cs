﻿namespace Ivh.Application.Core.ApplicationServices
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Base.Common.Interfaces;
    using Base.Services;
    using Common.Dtos;
    using Common.Interfaces;
    using Domain.Visit.Entities;
    using Domain.Visit.Interfaces;
    using Ivh.Common.Base.Enums;
    using Ivh.Common.VisitPay.Enums;

    public class ExternalLinkApplicationService : ApplicationService, IExternalLinkApplicationService
    {
        private readonly Lazy<IInsuranceService> _insuranceService;

        public ExternalLinkApplicationService(
            Lazy<IApplicationServiceCommonService> applicationServiceCommonService,
            Lazy<IInsuranceService> insuranceService) : base(applicationServiceCommonService)
        {
            this._insuranceService = insuranceService;
        }

        public IList<ExternalLinkDto> GetExternalLinksForActiveVisitInsurancePlans(int vpGuarantorId)
        {
            List<ExternalLink> externalLinks = new List<ExternalLink>();

            List<VisitInsurancePlan> visitInsurancePlans = this._insuranceService.Value
                .GetInsurancePlansForVisits(Enum.GetValues(typeof(VisitStateEnum)).Cast<VisitStateEnum>().ToList(), vpGuarantorId)
                .Where(x => x.InsurancePlan?.EobExternalLink != null)
                .ToList();

            // active or suspended should always show external link
            externalLinks.AddRange(visitInsurancePlans
                .Where(x => x.Visit.CurrentVisitState == VisitStateEnum.Active || x.Visit.BillingHold)
                .Select(x => x.InsurancePlan.EobExternalLink));

            // closed should show external link for X days after InsertDate
            externalLinks.AddRange(visitInsurancePlans
                .Where(x => x.Visit.CurrentVisitState == VisitStateEnum.Inactive)
                .Where(x => DateTime.Compare(x.Visit.InsertDate, DateTime.UtcNow.Date.AddDays(x.InsurancePlan.EobExternalLink.PersistDays.GetValueOrDefault(365 * 120) * -1)) >= 0)
                .Select(x => x.InsurancePlan.EobExternalLink)
                .ToList());

            return externalLinks
                .Where(x => !string.IsNullOrEmpty(x.Text) && !string.IsNullOrEmpty(x.Url))
                .GroupBy(x => string.Concat(x.Url, x.Text))
                .Select(x => x.First())
                .Select(x => new ExternalLinkDto
                {
                    Icon = x.Icon,
                    Text = x.Text,
                    Url = x.Url
                })
                .OrderBy(x => x.Text)
                .ToList();
        }
    }
}
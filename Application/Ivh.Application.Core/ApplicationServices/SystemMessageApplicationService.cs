﻿namespace Ivh.Application.Core.ApplicationServices
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using AppIntelligence.Common.Dtos;
    using AppIntelligence.Common.Interfaces;
    using AutoMapper;
    using Base.Common.Interfaces;
    using Base.Services;
    using Common.Dtos;
    using Common.Interfaces;
    using Domain.Content.Entities;
    using Domain.Content.Interfaces;
    using Domain.Guarantor.Entities;
    using Domain.Guarantor.Interfaces;
    using Domain.Core.Sso.Entities;
    using Domain.Core.Sso.Interfaces;
    using Domain.Core.SystemMessage.Entities;
    using Domain.Core.SystemMessage.Interfaces;
    using Domain.FinanceManagement.Interfaces;
    using Email.Common.Interfaces;
    using FinanceManagement.Common.Dtos;
    using FinanceManagement.Common.Interfaces;
    using Ivh.Common.Base.Utilities.Helpers;
    using Ivh.Common.ServiceBus.Attributes;
    using Ivh.Common.ServiceBus.Enums;
    using Ivh.Common.VisitPay.Constants;
    using Ivh.Common.VisitPay.Enums;
    using Ivh.Common.VisitPay.Messages.Core;
    using MassTransit;

    public class SystemMessageApplicationService : ApplicationService, ISystemMessageApplicationService
    {
        private readonly Lazy<IContentService> _contentService;
        private readonly Lazy<IGuarantorService> _guarantorService;
        private readonly Lazy<IPaymentMethodsService> _paymentMethodsService;
        private readonly Lazy<ISsoService> _ssoService;
        private readonly Lazy<IFinancePlanApplicationService> _financePlanApplicationService;
        private readonly Lazy<ICommunicationApplicationService> _communicationApplciationService;
        private readonly Lazy<ISystemMessageVisitPayUserService> _systemMessageVisitPayUserService;
        private readonly Lazy<IRandomizedTestApplicationService> _randomizedTestApplicationService;
        private readonly Lazy<IVisitBalanceBaseApplicationService> _visitBalanceBaseApplicationService;

        public SystemMessageApplicationService(
            Lazy<IApplicationServiceCommonService> applicationServiceCommonService,
            Lazy<IContentService> contentService,
            Lazy<IGuarantorService> guarantorService,
            Lazy<IPaymentMethodsService> paymentMethodsService,
            Lazy<ISsoService> ssoService,
            Lazy<IFinancePlanApplicationService> financePlanApplicationService,
            Lazy<ICommunicationApplicationService> communicationApplciationService,
            Lazy<ISystemMessageVisitPayUserService> systemMessageVisitPayUserService,
            Lazy<IRandomizedTestApplicationService> randomizedTestApplicationService,
            Lazy<IVisitBalanceBaseApplicationService> visitBalanceBaseApplicationService) : base(applicationServiceCommonService)
        {
            this._contentService = contentService;
            this._guarantorService = guarantorService;
            this._paymentMethodsService = paymentMethodsService;
            this._ssoService = ssoService;
            this._financePlanApplicationService = financePlanApplicationService;
            this._communicationApplciationService = communicationApplciationService;
            this._systemMessageVisitPayUserService = systemMessageVisitPayUserService;
            this._randomizedTestApplicationService = randomizedTestApplicationService;
            this._visitBalanceBaseApplicationService = visitBalanceBaseApplicationService;
        }

        public async Task<IList<SystemMessageDto>> GetMessages(int visitPayUserId, IDictionary<string, string> replacementValues)
        {
            IList<SystemMessageVisitPayUser> activeMessages = this._systemMessageVisitPayUserService.Value.GetActiveSystemMessagesVisitPayUser(visitPayUserId);

            int vpGuarantorId = this._guarantorService.Value.GetGuarantorId(visitPayUserId);

            IList<SystemMessageDto> messageDtos = new List<SystemMessageDto>();

            foreach (SystemMessageVisitPayUser message in activeMessages)
            {
                SystemMessageDto messageDto = Mapper.Map<SystemMessageDto>(message.SystemMessage);
                if (await this.ApplyCriteria(messageDto, visitPayUserId, vpGuarantorId, replacementValues))
                {
                    messageDto.MessageRead = message.MessageRead;
                    messageDtos.Add(messageDto);
                }
            }

            return messageDtos;
        }

        public async Task<int> GetUnreadMessageCount(int visitPayUserId, IDictionary<string, string> replacementValues)
        {
            IList<SystemMessageDto> messages = await this.GetMessages(visitPayUserId, replacementValues);

            int unreadMessageCount = messages.Count(x => !x.MessageRead);
            return unreadMessageCount;
        }

        private async Task<bool> ApplyCriteria(SystemMessageDto messageDto, int visitPayUserId, int vpGuarantorId, IDictionary<string, string> replacementValues)
        {
            CmsVersion cmsVersion = await this._contentService.Value.GetCurrentVersionAsync(messageDto.CmsRegion, true);
            if (cmsVersion == null)
            {
                return false;
            }

            replacementValues = replacementValues ?? new Dictionary<string, string>();
            if (!this.CheckCriteria(messageDto, visitPayUserId, vpGuarantorId, replacementValues))
            {
                return false;
            }

            messageDto.ContentBody = ReplacementUtility.Replace(cmsVersion.ContentBody, new Dictionary<string, string>(), replacementValues, false);
            messageDto.ContentTitle = cmsVersion.ContentTitle;

            if (!string.IsNullOrWhiteSpace(messageDto.ContentTitle) && messageDto.ContentTitle.Contains("[["))
            {
                messageDto.ContentTitle = ReplacementUtility.Replace(messageDto.ContentTitle, new Dictionary<string, string>(), replacementValues, false);
            }

            return true;
        }

        private bool CheckCriteria(SystemMessageDto messageDto, int visitPayUserId, int vpGuarantorId, IDictionary<string, string> additionalValues)
        {
            if (!messageDto.SystemMessageCriteria.HasValue)
            {
                // just a generic message with no criteria
                return true;
            }

            switch (messageDto.SystemMessageCriteria.Value)
            {
                case SystemMessageCriteriaEnum.HasBalanceNoPaymentMethods:
                    return this.CheckHasBalanceNoPaymentMethods(vpGuarantorId, additionalValues);
                case SystemMessageCriteriaEnum.IsEligibleForFinancePlan:
                    return this.CheckIsEligibleForFinancePlan(visitPayUserId, vpGuarantorId);
                case SystemMessageCriteriaEnum.IsEligibleForSso:
                    return this.CheckIsEligibleForSso(visitPayUserId, vpGuarantorId, VisitPayFeatureEnum.MyChartSso, additionalValues) ||
                           this.CheckIsEligibleForSso(visitPayUserId, vpGuarantorId, VisitPayFeatureEnum.HealthEquityFeature, additionalValues);
                case SystemMessageCriteriaEnum.IsNotConsolidated:
                    return this.CheckIsNotConsolidated(vpGuarantorId);
                case SystemMessageCriteriaEnum.IsSmsEnabled:
                    return this.CheckIsSmsEnabled(visitPayUserId);
                case SystemMessageCriteriaEnum.PaymentSummary:
                    return this.IsPaymentSummaryEnabled();
                case SystemMessageCriteriaEnum.CanAddVisitToExistingPlan:
                    return this.CanAddVisitToExistingPlan(visitPayUserId, vpGuarantorId, additionalValues);
                case SystemMessageCriteriaEnum.HasNewPaymentSummary:
                case SystemMessageCriteriaEnum.HasNewEob:
                case SystemMessageCriteriaEnum.HasExistingBadDebt:
                case SystemMessageCriteriaEnum.OnPrePaymentPlan:
                    return true;
                default:
                    return false;
            }
        }

        private bool IsPaymentSummaryEnabled()
        {
            return this.FeatureService.Value.IsFeatureEnabled(VisitPayFeatureEnum.PaymentSummary);
        }

        private bool CheckIsSmsEnabled(int visitPayUserId)
        {
            if (!this.FeatureService.Value.IsFeatureEnabled(VisitPayFeatureEnum.Sms))
            {
                return false;
            }

            return !this._communicationApplciationService.Value.CommunicationPreferences(visitPayUserId).Any();
        }

        private bool CheckHasBalanceNoPaymentMethods(int vpGuarantorId, IDictionary<string, string> additionalValues)
        {
            if (this._paymentMethodsService.Value.GetPaymentMethodsCount(vpGuarantorId) > 0)
            {
                return false;
            }

            decimal balance = this._visitBalanceBaseApplicationService.Value.GetTotalBalance(vpGuarantorId);
            if (balance <= 0)
            {
                return false;
            }

            additionalValues.Add("[[Balance]]", balance.ToString("C"));

            return true;
        }

        private bool CheckIsEligibleForFinancePlan(int visitPayUserId, int vpGuarantorId)
        {
            decimal balance = this._visitBalanceBaseApplicationService.Value.GetTotalBalance(vpGuarantorId);
            if (balance <= 0)
            {
                return false;
            }

            FinancePlanCalculationParametersDto financePlanCalculationParametersDto = FinancePlanCalculationParametersDto.CreateForTermsCheck(vpGuarantorId, null, false);
            return this._financePlanApplicationService.Value.IsEligibleForFinancePlan(financePlanCalculationParametersDto, visitPayUserId);
        }

        private bool CheckIsEligibleForSso(int visitPayUserId, int vpGuarantorId, VisitPayFeatureEnum ssoFeature, IDictionary<string, string> additionalValues)
        {
            if (!this.FeatureService.Value.IsFeatureEnabled(ssoFeature))
            {
                return false;
            }

            if (ssoFeature == VisitPayFeatureEnum.HealthEquityFeature)
            {
                RandomizedTestGroupDto randomizedTestGroupDto = this._randomizedTestApplicationService.Value.GetRandomizedTestGroupMembership(RandomizedTestEnum.NotificationTesting, vpGuarantorId, true);
                bool showMessageViaAlert = (randomizedTestGroupDto?.RandomizedTestGroupId == (int)RandomizedTestGroupEnum.MessageViaAlert);
                if (showMessageViaAlert)
                {
                    // SSO message is shown via alert. Hide from showing on notification 
                    return false;
                }

                SsoVisitPayUserSetting ssoSetting = this._ssoService.Value.GetUserSettings(SsoProviderEnum.HealthEquityOutbound, visitPayUserId);
                if (ssoSetting != null && ssoSetting.SsoStatus == SsoStatusEnum.Accepted)
                {
                    return false;
                }

                return true;
            }

            if (ssoFeature == VisitPayFeatureEnum.MyChartSso)
            {
                IList<SsoProvider> providers = this._ssoService.Value.GetEligibleSsoProvidersForUser(visitPayUserId, vpGuarantorId);
                if (!providers.Any())
                {
                    return false;
                }

                List<string> links = providers.Where(provider => !string.IsNullOrEmpty(provider.SsoProviderUrl)).Select(provider => string.Format("<a href=\"{0}\" title=\"{1}\">{1}</a>", provider.SsoProviderUrl, provider.SsoProviderDisplayName)).ToList();
                if (!links.Any())
                {
                    return false;
                }

                switch (links.Count)
                {
                    case 1:
                        additionalValues.Add("[[SsoProviderLinks]]", links.First());
                        return true;
                    case 2:
                        additionalValues.Add("[[SsoProviderLinks]]", string.Join(" or ", links));
                        return true;
                }

                string str1 = string.Join(", ", links.Take(links.Count - 1));
                string str2 = $", or {links.Last()}";

                additionalValues.Add("[[SsoProviderLinks]]", string.Concat(str1, str2));

                return true;
            }

            return false;
        }

        private bool CheckIsNotConsolidated(int vpGuarantorId)
        {
            if (!this.FeatureService.Value.IsFeatureEnabled(VisitPayFeatureEnum.Consolidation))
            {
                return false;
            }

            Guarantor guarantor = this._guarantorService.Value.GetGuarantor(vpGuarantorId);

            return guarantor != null && guarantor.ConsolidationStatus == GuarantorConsolidationStatusEnum.NotConsolidated;
        }

        private bool CanAddVisitToExistingPlan(int currentVisitPayUserId, int vpGuarantorId, IDictionary<string, string> additionalValues)
        {
            string paymentUrl = $"Payment/ArrangePayment?paymentOption={(int)PaymentOptionEnum.FinancePlan}";
            additionalValues.Add("[[ArrangePaymentUrl]]", paymentUrl);
            bool canAddVisitToExistingPlan = this._financePlanApplicationService.Value.IsGuarantorEligibleToCombineFinancePlans(currentVisitPayUserId, vpGuarantorId, true);
            return canAddVisitToExistingPlan;
        }

        public void DismissMessage(int visitPayUserId, int systemMessageId)
        {
            this._systemMessageVisitPayUserService.Value.DismissMessage(visitPayUserId, systemMessageId);
        }

        public void HideMessage(int visitPayUserId, int systemMessageId)
        {
            this._systemMessageVisitPayUserService.Value.HideMessage(visitPayUserId, systemMessageId);
        }

        public void MarkMessageAsRead(int visitPayUserId, int systemMessageId)
        {
            this._systemMessageVisitPayUserService.Value.MarkMessageAsRead(visitPayUserId, systemMessageId);
        }
        
        public async Task MarkAllMessagesAsRead(int visitPayUserId)
        {
            IList<SystemMessageDto> messages = await this.GetMessages(visitPayUserId, new Dictionary<string, string>());
            IList<int> systemMessageIds = (messages ?? new List<SystemMessageDto>()).Select(x => x.SystemMessageId).ToList();

            this._systemMessageVisitPayUserService.Value.MarkAllMessagesAsRead(systemMessageIds, visitPayUserId);
        }

        public void InsertSystemMessageVisitPayUser(int visitPayUserId, SystemMessageEnum systemMessageEnum)
        {
            this._systemMessageVisitPayUserService.Value.InsertSystemMessageVisitPayUser(visitPayUserId, systemMessageEnum);
        }

        public void InsertSystemMessageVisitPayUserByGuarantorId(int vpGuarantorId, SystemMessageEnum systemMessageEnum)
        {
            this._systemMessageVisitPayUserService.Value.InsertSystemMessageVisitPayUserByGuarantorId(vpGuarantorId, systemMessageEnum);
        }

        public void InsertSystemMessageVisitPayUsers(IList<AddSystemMessageVisitPayUserMessage> messages)
        {
            this._systemMessageVisitPayUserService.Value.InsertSystemMessageVisitPayUsers(messages);
        }

        public void InsertSystemMessageVisitPayUsers(IList<AddSystemMessageVisitPayUserByGuarantorMessage> messages)
        {
            this._systemMessageVisitPayUserService.Value.InsertSystemMessageVisitPayUsers(messages);
        }

        #region Message Consumers

        [UniversalConsumer(UniversalQueuesEnum.VisitPayProcessor, UniversalPriorityEnum.Priority30)]
        public void ConsumeMessage(AddSystemMessageVisitPayUserByGuarantorMessage message, ConsumeContext<AddSystemMessageVisitPayUserByGuarantorMessage> consumeContext)
        {
            this.InsertSystemMessageVisitPayUserByGuarantorId(message.VpGuarantorId, message.SystemMessageEnum);
        }

        public bool IsMessageValid(AddSystemMessageVisitPayUserByGuarantorMessage message, ConsumeContext<AddSystemMessageVisitPayUserByGuarantorMessage> consumeContext)
        {
            bool isValid = true;
            return isValid;
        }

        [UniversalConsumer(UniversalQueuesEnum.VisitPayProcessor, UniversalPriorityEnum.Priority30)]
        public void ConsumeMessage(AddSystemMessageVisitPayUserByGuarantorMessageList message, ConsumeContext<AddSystemMessageVisitPayUserByGuarantorMessageList> consumeContext)
        {
            this.InsertSystemMessageVisitPayUsers(message.Messages);
        }

        public bool IsMessageValid(AddSystemMessageVisitPayUserByGuarantorMessageList message, ConsumeContext<AddSystemMessageVisitPayUserByGuarantorMessageList> consumeContext)
        {
            bool isValid = true;
            return isValid;
        }

        [UniversalConsumer(UniversalQueuesEnum.VisitPayProcessor, UniversalPriorityEnum.Priority30)]
        public void ConsumeMessage(AddSystemMessageVisitPayUserMessage message, ConsumeContext<AddSystemMessageVisitPayUserMessage> consumeContext)
        {
            this.InsertSystemMessageVisitPayUser(message.VisitPayUserId, message.SystemMessageEnum);
        }

        public bool IsMessageValid(AddSystemMessageVisitPayUserMessage message, ConsumeContext<AddSystemMessageVisitPayUserMessage> consumeContext)
        {
            bool isValid = true;
            return isValid;
        }

        [UniversalConsumer(UniversalQueuesEnum.VisitPayProcessor, UniversalPriorityEnum.Priority30)]
        public void ConsumeMessage(AddSystemMessageVisitPayUserMessageList message, ConsumeContext<AddSystemMessageVisitPayUserMessageList> consumeContext)
        {
            this.InsertSystemMessageVisitPayUsers(message.Messages);
        }

        public bool IsMessageValid(AddSystemMessageVisitPayUserMessageList message, ConsumeContext<AddSystemMessageVisitPayUserMessageList> consumeContext)
        {
            bool isValid = true;
            return isValid;
        }

        #endregion
    }
}
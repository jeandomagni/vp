﻿namespace Ivh.Application.Core.ApplicationServices
{
    using System;
    using AutoMapper;
    using Common.Interfaces;
    using Domain.Core.KnowledgeBase.Interfaces;
    using Domain.Core.KnowledgeBase.Entities;
    using Ivh.Common.ServiceBus.Attributes;
    using Ivh.Common.ServiceBus.Enums;
    using Ivh.Common.VisitPay.Messages.Core;
    using MassTransit;

    public class SearchTextLogApplicationService : ISearchTextLogApplicationService
    {
        private readonly Lazy<ISearchTextLogRepository> _searchTextLogRepository;

        public SearchTextLogApplicationService(Lazy<ISearchTextLogRepository> searchTextLogRepository)
        {
            this._searchTextLogRepository = searchTextLogRepository;
        }

        public void ProcessSearchTextLogMessage(SearchTextLogMessage message)
        {
            SearchTextLog searchTextLog = Mapper.Map<SearchTextLog>(message);
            this._searchTextLogRepository.Value.InsertOrUpdate(searchTextLog);
        }

        #region Message Consumers

        [UniversalConsumer(UniversalQueuesEnum.VisitPayProcessor, UniversalPriorityEnum.Priority30)]
        public void ConsumeMessage(SearchTextLogMessage message, ConsumeContext<SearchTextLogMessage> consumeContext)
        {
            SearchTextLog searchTextLog = Mapper.Map<SearchTextLog>(message);
            this._searchTextLogRepository.Value.InsertOrUpdate(searchTextLog);
        }

        public bool IsMessageValid(SearchTextLogMessage message, ConsumeContext<SearchTextLogMessage> consumeContext)
        {
            bool isValid = true;
            return isValid;
        }

        #endregion
    }
}
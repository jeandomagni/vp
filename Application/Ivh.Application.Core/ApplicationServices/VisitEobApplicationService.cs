﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Ivh.Application.Core.ApplicationServices
{
    using AutoMapper;
    using Base.Common.Interfaces;
    using Base.Services;
    using Common.Dtos.Eob;
    using Common.Interfaces;
    using Common.Models.Eob;
    using Domain.Core.SystemException.Entities;
    using Domain.Core.SystemException.Interfaces;
    using Domain.Eob.Entities;
    using Domain.Eob.Interfaces;
    using Domain.Visit.Constants.Eob;
    using Domain.Visit.Entities;
    using Domain.Visit.Interfaces;
    using Ivh.Common.ServiceBus.Attributes;
    using Ivh.Common.ServiceBus.Enums;
    using Ivh.Common.VisitPay.Enums;
    using Ivh.Common.VisitPay.Messages.Core;
    using Ivh.Common.VisitPay.Messages.Eob;
    using MassTransit;

    public class VisitEobApplicationService : ApplicationService, IVisitEobApplicationService
    {
        private readonly Lazy<IVisitService> _visitService;
        private readonly Lazy<IClaimPaymentInformationService> _claimPaymentInformationService;
        private readonly Lazy<ISystemExceptionService> _systemExceptionService;

        private const string UnknownCarcUiDisplay = "Miscellaneous";

        public VisitEobApplicationService(
            Lazy<IApplicationServiceCommonService> applicationServiceCommonService,
            Lazy<IVisitService> visitService,
            Lazy<IClaimPaymentInformationService> claimPaymentInformationService,
            Lazy<ISystemExceptionService> systemExceptionService
            ) : base(applicationServiceCommonService)
        {
            this._visitService = visitService;
            this._claimPaymentInformationService = claimPaymentInformationService;
            this._systemExceptionService = systemExceptionService;
        }

        public IList<VisitEobDetailsResultDto> GetVisitEobDetails(int visitBillingSystemId, string visitSourceSystemKey)
        {
            IList<VisitEobDetailsResultDto> eobDetailsResultDtos = new List<VisitEobDetailsResultDto>();

            IList<Eob835Result> visitEobs = this._claimPaymentInformationService.Value.Get835Files(visitBillingSystemId, visitSourceSystemKey);
            if (visitEobs == null) return eobDetailsResultDtos;

            foreach (Eob835Result eob in visitEobs)
            {
                EobPayerFilterEobExternalLink eobPayerFilterEobExternalLink = this.GetPayerInformation(eob.EobPayerFilterEobExternalLinks, visitBillingSystemId, visitSourceSystemKey);
                EobExternalLink insuranceExternalLink = eobPayerFilterEobExternalLink?.EobExternalLink;
                EobExternalLink payerInformation = insuranceExternalLink ?? new EobExternalLink();
                string uniquePayerValue = eobPayerFilterEobExternalLink?.EobPayerFilter?.UniquePayerValue;

                foreach (ClaimPaymentInformation cpi in eob.ClaimPaymentInformations)
                {
                    VisitEobDetailsResultDto visitEobDetailsResultDto = new VisitEobDetailsResultDto
                    {
                        HeaderNumber = eob.HeaderNumber,
                        TransactionDate = eob.TransactionDate,
                        PayerName = payerInformation.Text,
                        PayerLinkUrl = payerInformation.Url,
                        PayerIcon = payerInformation.Icon,
                        PlanNumber = cpi.Nm11PatientName?.Nm109IdentificationCode ?? ""
                    };

                    AutoMapper.Mapper.Map(cpi, visitEobDetailsResultDto);

                    this.UpdateClaimAdjustmentUiDisplay(cpi.Clp02ClaimStatusCode, uniquePayerValue, cpi.ClaimAdjustmentPivots);

                    visitEobDetailsResultDto.ClaimNumber = this.GetClaimNumber(cpi, payerInformation.ClaimNumberLocation);
                    visitEobDetailsResultDto.ClaimLevelClaimAdjustments = AutoMapper.Mapper.Map<List<VisitEobClaimAdjustmentDto>>(cpi.ClaimAdjustmentPivots);
                    visitEobDetailsResultDto.MiaMoaClaimPaymentRemarkCode = this.GetMiaMoaClaimPaymentRemarkCode(cpi);
                    visitEobDetailsResultDto.MiaMoaClaimPaymentRemarkDescription = this.GetMiaMoaClaimPaymentRemarkDescription(cpi);

                    // map the service level manually so we can flatten the dates and map the claimsAdjustments
                    foreach (ServicePaymentInformation spi in cpi.ServicePaymentInformations)
                    {
                        this.UpdateClaimAdjustmentUiDisplay(cpi.Clp02ClaimStatusCode, uniquePayerValue, spi.ClaimAdjustmentPivots);

                        VisitEobServicePaymentInformationDto serviceModel = AutoMapper.Mapper.Map<VisitEobServicePaymentInformationDto>(spi);
                        if (serviceModel == null) continue;
                        this.SetServiceStartEndDates(spi, serviceModel);
                        serviceModel.ClaimAdjustments = AutoMapper.Mapper.Map<List<VisitEobClaimAdjustmentDto>>(spi.ClaimAdjustmentPivots);

                        visitEobDetailsResultDto.ServicePaymentInformations.Add(serviceModel);
                    }
                    eobDetailsResultDtos.Add(visitEobDetailsResultDto);
                }
            }
            return eobDetailsResultDtos;
        }

        public bool HasVisitEobDetails(int billingSystemId, string visitSourceSystemKey)
        {
            return this._claimPaymentInformationService.Value.Has835Remits(billingSystemId, visitSourceSystemKey);
        }

        public IList<EobIndicatorResultDto> Get835RemitIndicators(IDictionary<string, int> visits)
        {
            // WARNING: COUPLING

            // Ivh.Web.Patient.Models.Account.VisitEobDetailViewModel.GetVisitEobDetailViewModels(dtoList)
            // The variable uniquePlans is sorted by PayerName ascending

            //Ivh.Provider.HospitalData.Eob.Eob835Repository.GetEob835ResultList(int billingSystemId, string visitSourceSystemKey)
            // .AddOrder(Order.Desc("t.Bpr16Date"))
            // .AddOrder(Order.Desc("hn.Lx01AssignedNumber"))

            IList<EobIndicatorResultDto> eobIndicatorResultModels = new List<EobIndicatorResultDto>();
            IList<EobIndicatorResult> eobIndicatorResults = this._claimPaymentInformationService.Value.Get835RemitIndicators(visits);
            foreach (EobIndicatorResult eobIndicatorResult in eobIndicatorResults)
            {
                EobExternalLink insuranceExternalLink = this.GetEobExternalLink(eobIndicatorResult.EobPayerFilterEobExternalLinks, eobIndicatorResult.BillingSystemId, eobIndicatorResult.VisitSourceSystemKey);
                eobIndicatorResult.LogoUrl = insuranceExternalLink.Icon;
                eobIndicatorResult.PayerName = insuranceExternalLink.Text;
            }
            IOrderedEnumerable<EobIndicatorResult> orderedEobIndicatorResults = eobIndicatorResults.OrderBy(x => x.PayerName).ThenByDescending(x => x.Bpr16Date).ThenByDescending(x => x.Lx01AssignedNumber);
            AutoMapper.Mapper.Map(orderedEobIndicatorResults, eobIndicatorResultModels);
            return eobIndicatorResultModels;
        }

        private string GetClaimNumber(ClaimPaymentInformation cpi, string claimNumberLocation)
        {
            switch (claimNumberLocation)
            {
                case "clp01":
                    return cpi.Clp01ClaimSubmittersIdentifier;
                case "clp07":
                    return cpi.Clp07ReferenceIdentifier;
                default:
                    return cpi.Clp07ReferenceIdentifier;
            }
        }

        private EobExternalLink GetEobExternalLink(IList<EobPayerFilterEobExternalLink> eobPayerFilterEobExternalLinks, int billingSystemId, string visitSourceSystemKey)
        {
            EobPayerFilterEobExternalLink eobPayerFilterEobExternalLink = this.GetPayerInformation(eobPayerFilterEobExternalLinks, billingSystemId, visitSourceSystemKey);
            EobExternalLink insuranceExternalLink = eobPayerFilterEobExternalLink?.EobExternalLink;
            EobExternalLink payerInformation = insuranceExternalLink ?? new EobExternalLink();
            return payerInformation;
        }

        private EobPayerFilterEobExternalLink GetPayerInformation(IList<EobPayerFilterEobExternalLink> eobPayerFilterEobExternalLinks, int billingSystemId, string visitSourceSystemKey)
        {
            if (eobPayerFilterEobExternalLinks == null)
            {
                return new EobPayerFilterEobExternalLink();
            }

            if (eobPayerFilterEobExternalLinks.Count == 1)
            {
                return eobPayerFilterEobExternalLinks[0];
            }

            Visit visit = this._visitService.Value.GetVisitBySystemSourceKey(visitSourceSystemKey, billingSystemId);

            IEnumerable<EobPayerFilterEobExternalLink> joined = from x in eobPayerFilterEobExternalLinks
                                                                join y in visit.VisitInsurancePlans
                                                                on new { BillingSystemId = x.InsurancePlanBillingSystemId, SourceSystemKey = x.InsurancePlanSourceSystemKey } equals new { y.InsurancePlan?.BillingSystemId, y.InsurancePlan?.SourceSystemKey }
                                                                select x;

            return joined.FirstOrDefault();
        }

        private void UpdateClaimAdjustmentUiDisplay(string claimStatusCode, string uniquePayerValue, IList<ClaimAdjustmentPivot> claimAdjustmentPivots)
        {
            // Lookup mapped EobDisplayCategory to use for UIDisplay value
            if (claimAdjustmentPivots != null && claimAdjustmentPivots.Any())
            {
                foreach (ClaimAdjustmentPivot cas in claimAdjustmentPivots)
                {
                    string uiDisplay = this._claimPaymentInformationService.Value.GetUiDisplay(cas.ClaimAdjustmentReasonCode, uniquePayerValue, claimStatusCode, cas.ClaimAdjustmentGroupCode);
                    if (string.IsNullOrWhiteSpace(uiDisplay))
                    {
                        uiDisplay = UnknownCarcUiDisplay;
                        this._systemExceptionService.Value.LogSystemException(new SystemException
                        {
                            SystemExceptionDescription = $"Claim Adjustment Reason Code '{cas.ClaimAdjustmentReasonCode}' does not have a mapped UI display value.",
                            SystemExceptionType = SystemExceptionTypeEnum.EobInactiveCarc
                        });
                    }
                    cas.EobClaimAdjustmentReasonCode.UIDisplay = uiDisplay;
                }
            }
        }

        private string GetMiaMoaClaimPaymentRemarkDescription(ClaimPaymentInformation cpi)
        {
            return cpi.MoaOutpatientInformation?.Moa03EobRemittanceAdviceRemarkCode?.Description ??
                   cpi.MiaInpatientInformation?.Mia05EobRemittanceAdviceRemarkCode?.Description ?? null;
        }

        private string GetMiaMoaClaimPaymentRemarkCode(ClaimPaymentInformation cpi)
        {
            return cpi.MoaOutpatientInformation?.Moa03ReferenceIdentifier ??
                   cpi.MiaInpatientInformation?.Mia05ReferenceIdentifier;
        }

        private void SetServiceStartEndDates(ServicePaymentInformation spi, VisitEobServicePaymentInformationDto serviceModel)
        {
            if (spi.DtmServiceDates == null)
            {
                return;
            }

            foreach (DateTimeReference dtr in spi.DtmServiceDates)
            {
                switch (dtr.Dtm01DateTimeQualifier)
                {
                    case EobDateTimeReference.ServiceStartDate:
                        serviceModel.ServiceStartDate = dtr.Dtm02Date;
                        break;
                    case EobDateTimeReference.ServiceEndDate:
                        serviceModel.ServiceEndDate = dtr.Dtm02Date;
                        break;
                    case EobDateTimeReference.ServiceDay:
                        serviceModel.ServiceStartDate = dtr.Dtm02Date;
                        serviceModel.ServiceEndDate = dtr.Dtm02Date;
                        break;
                }
            }
        }

        private void NotifyVpGuarantorAboutEob(ClaimPaymentInformationHeader claimPaymentInformationHeader)
        {
            bool isPayerActive = claimPaymentInformationHeader.EobPayerFilter?.IsActive ?? false;
            if (isPayerActive)
            {
                Visit visit = this._visitService.Value.GetVisitBySystemSourceKey(claimPaymentInformationHeader.VisitSourceSystemKey, claimPaymentInformationHeader.BillingSystemId);
                if (visit != null)
                {
                    AddSystemMessageVisitPayUserByGuarantorMessage eobMessage = new AddSystemMessageVisitPayUserByGuarantorMessage
                    {
                        VpGuarantorId = visit.VpGuarantorId,
                        SystemMessageEnum = SystemMessageEnum.Eob
                    };
                    this.Bus.Value.PublishMessage(eobMessage).Wait();
                }

            }
        }

        #region Message Consumers

        [UniversalConsumer(UniversalQueuesEnum.VisitPayProcessor, UniversalPriorityEnum.Priority40)]
        public void ConsumeMessage(EobPayerFilterEobExternalLinkMessage message, ConsumeContext<EobPayerFilterEobExternalLinkMessage> consumeContext)
        {
            EobPayerFilterEobExternalLink eobPayerFilterExternalLink = Mapper.Map<EobPayerFilterEobExternalLink>(message);
            this._claimPaymentInformationService.Value.SaveEobPayerFilterExternalLink(eobPayerFilterExternalLink);
        }

        public bool IsMessageValid(EobClaimPaymentMessage message, ConsumeContext<EobClaimPaymentMessage> consumeContext)
        {
            bool isValid = true;
            return isValid;
        }

        [UniversalConsumer(UniversalQueuesEnum.VisitPayProcessor, UniversalPriorityEnum.Priority40)]
        public void ConsumeMessage(EobExternalLinkMessage message, ConsumeContext<EobExternalLinkMessage> consumeContext)
        {
            EobExternalLink eobExternalLink = Mapper.Map<EobExternalLink>(message);
            this._claimPaymentInformationService.Value.SaveEobExternalLink(eobExternalLink);
        }

        public bool IsMessageValid(EobPayerFilterMessage message, ConsumeContext<EobPayerFilterMessage> consumeContext)
        {
            bool isValid = true;
            return isValid;
        }

        [UniversalConsumer(UniversalQueuesEnum.VisitPayProcessor, UniversalPriorityEnum.Priority40)]
        public void ConsumeMessage(EobPayerFilterMessage message, ConsumeContext<EobPayerFilterMessage> consumeContext)
        {
            EobPayerFilter eobPayerFilter = Mapper.Map<EobPayerFilter>(message);
            this._claimPaymentInformationService.Value.SaveEobPayerFilter(eobPayerFilter);
        }

        public bool IsMessageValid(EobPayerFilterEobExternalLinkMessage message, ConsumeContext<EobPayerFilterEobExternalLinkMessage> consumeContext)
        {
            bool isValid = true;
            return isValid;
        }

        [UniversalConsumer(UniversalQueuesEnum.VisitPayProcessor, UniversalPriorityEnum.Priority40)]
        public void ConsumeMessage(EobClaimPaymentMessage message, ConsumeContext<EobClaimPaymentMessage> consumeContext)
        {
            ClaimPaymentInformationHeader claimPaymentInformationHeader = Mapper.Map<ClaimPaymentInformationHeader>(message);
            this._claimPaymentInformationService.Value.SaveClaimPaymentInformationHeader(claimPaymentInformationHeader);
            this.NotifyVpGuarantorAboutEob(claimPaymentInformationHeader);
        }

        public bool IsMessageValid(EobExternalLinkMessage message, ConsumeContext<EobExternalLinkMessage> consumeContext)
        {
            bool isValid = true;
            return isValid;
        }

        [UniversalConsumer(UniversalQueuesEnum.VisitPayProcessor, UniversalPriorityEnum.Priority40)]
        public void ConsumeMessage(EobDisplayCategoryMessage message, ConsumeContext<EobDisplayCategoryMessage> consumeContext)
        {
            EobDisplayCategory eobDisplayCategory = Mapper.Map<EobDisplayCategory>(message);
            this._claimPaymentInformationService.Value.SaveEobDisplayCategory(eobDisplayCategory);
        }

        public bool IsMessageValid(EobDisplayCategoryMessage message, ConsumeContext<EobDisplayCategoryMessage> consumeContext)
        {
            bool isValid = true;
            return isValid;
        }

        [UniversalConsumer(UniversalQueuesEnum.VisitPayProcessor, UniversalPriorityEnum.Priority40)]
        public void ConsumeMessage(EobClaimAdjustmentReasonCodeMessage message, ConsumeContext<EobClaimAdjustmentReasonCodeMessage> consumeContext)
        {
            Domain.Eob.Entities.EobClaimAdjustmentReasonCode eobClaimAdjustmentReasonCode = Mapper.Map<Domain.Eob.Entities.EobClaimAdjustmentReasonCode>(message);
            this._claimPaymentInformationService.Value.SaveEobClaimAdjustmentReasonCode(eobClaimAdjustmentReasonCode);
        }

        public bool IsMessageValid(EobClaimAdjustmentReasonCodeMessage message, ConsumeContext<EobClaimAdjustmentReasonCodeMessage> consumeContext)
        {
            bool isValid = true;
            return isValid;
        }

        [UniversalConsumer(UniversalQueuesEnum.VisitPayProcessor, UniversalPriorityEnum.Priority40)]
        public void ConsumeMessage(EobClaimAdjustmentReasonCodeDisplayMapMessage message, ConsumeContext<EobClaimAdjustmentReasonCodeDisplayMapMessage> consumeContext)
        {
            EobClaimAdjustmentReasonCodeDisplayMap eobClaimAdjustmentReasonCodeDisplayMap = Mapper.Map<EobClaimAdjustmentReasonCodeDisplayMap>(message);
            this._claimPaymentInformationService.Value.SaveEobClaimAdjustmentReasonCodeDisplayMap(eobClaimAdjustmentReasonCodeDisplayMap);
        }

        public bool IsMessageValid(EobClaimAdjustmentReasonCodeDisplayMapMessage message, ConsumeContext<EobClaimAdjustmentReasonCodeDisplayMapMessage> consumeContext)
        {
            bool isValid = true;
            return isValid;
        }

        #endregion
    }
}

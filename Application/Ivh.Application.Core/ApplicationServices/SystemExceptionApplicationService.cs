﻿namespace Ivh.Application.Core.ApplicationServices
{
    using System;
    using System.Collections.Generic;
    using Base.Common.Interfaces;
    using Base.Services;
    using Common.Interfaces;
    using Domain.Core.SystemException.Interfaces;
    using Ivh.Common.Base.Enums;
    using Ivh.Common.Data;
    using Ivh.Common.Data.Connection.Connections;
    using Ivh.Common.Data.Interfaces;
    using Ivh.Common.JobRunner.Common.Interfaces;
    using Ivh.Common.ServiceBus.Attributes;
    using Ivh.Common.ServiceBus.Enums;
    using Ivh.Common.VisitPay.Enums;
    using Ivh.Common.VisitPay.Jobs;
    using Ivh.Common.VisitPay.Messages.Core;
    using MassTransit;
    using NHibernate;
    using SystemException = Domain.Core.SystemException.Entities.SystemException;

    public class SystemExceptionApplicationService : ApplicationService, ISystemExceptionApplicationService
    {
        private readonly Lazy<IEnumerable<ISystemExceptionProvider>> _systemExceptionProviders;
        private readonly ISession _session;
        private readonly Lazy<ISystemExceptionService> _systemExceptionService;

        public SystemExceptionApplicationService(
            Lazy<IApplicationServiceCommonService> applicationServiceCommonService,
            Lazy<ISystemExceptionService> systemExceptionService,
            Lazy<IEnumerable<ISystemExceptionProvider>> systemExceptionProviders,
            ISessionContext<VisitPay> session) : base(applicationServiceCommonService)
        {
            this._systemExceptionService = systemExceptionService;
            this._systemExceptionProviders = systemExceptionProviders;
            this._session = session.Session;
        }

        public void LogSystemExceptions(DateTime exceptionsAsOfDateTime)
        {
            List<SystemException> exceptions = new List<SystemException>();
            foreach (ISystemExceptionProvider provider in this._systemExceptionProviders.Value)
            {
                IEnumerable<SystemException> newList = provider.GetExceptions(exceptionsAsOfDateTime);
                if (newList != null)
                {
                    exceptions.AddRange(newList);
                }
            }
            using (UnitOfWork uow = new UnitOfWork(this._session))
            {
                foreach (SystemException ex in exceptions)
                {
                    this._systemExceptionService.Value.LogSystemException(ex);
                }
                uow.Commit();
            }
        }

        public void LogSystemException(SystemExceptionMessage systemExceptionMessage)
        {
            using (UnitOfWork uow = new UnitOfWork(this._session))
            {
                this._systemExceptionService.Value.LogSystemException(new SystemException
                {
                    VpGuarantorId = systemExceptionMessage.VpGuarantorId,
                    PaymentId = systemExceptionMessage.PaymentId,
                    FinancePlanId = systemExceptionMessage.FinancePlanId,
                    TransactionId = systemExceptionMessage.TransactionId,
                    InsertDate = DateTime.UtcNow,
                    SystemExceptionDescription = systemExceptionMessage.SystemExceptionDescription,
                    SystemExceptionType = systemExceptionMessage.SystemExceptionType
                });
                uow.Commit();
            }
        }

        public void LogSystemEnrollmentException(int vpGuarantorId, string message)
        {
            using (UnitOfWork uow = new UnitOfWork(this._session))
            {
                this._systemExceptionService.Value.LogSystemException(new SystemException
                {
                    VpGuarantorId = vpGuarantorId,
                    InsertDate = DateTime.UtcNow,
                    SystemExceptionDescription = message,
                    SystemExceptionType = SystemExceptionTypeEnum.EnrollmentException
                });
                uow.Commit();
            }
        }

        #region Message Consumers

        [UniversalConsumer(UniversalQueuesEnum.VisitPayProcessor, UniversalPriorityEnum.Priority30)]
        public void ConsumeMessage(SystemExceptionMessage message, ConsumeContext<SystemExceptionMessage> consumeContext)
        {
            this.LogSystemException(message);
        }

        public bool IsMessageValid(SystemExceptionMessage message, ConsumeContext<SystemExceptionMessage> consumeContext)
        {
            bool isValid = true;
            return isValid;
        }

        #endregion

        #region Jobs

        void IJobRunnerService<QueueSystemExceptionLoggingJobRunner>.Execute(DateTime begin, DateTime end, string[] args)
        {
            this.LogSystemExceptions(DateTime.UtcNow.Date);
        }

        #endregion
    }
}
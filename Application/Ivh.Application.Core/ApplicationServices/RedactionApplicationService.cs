﻿namespace Ivh.Application.Core.ApplicationServices
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using System.Web;
    using AutoMapper;
    using Common.Dtos;
    using Common.Interfaces;
    using Domain.Content.Interfaces;
    using Domain.User.Entities;
    using Domain.User.Interfaces;
    using Domain.Visit.Entities;
    using Domain.Visit.Interfaces;
    using Ivh.Common.Base.Utilities.Extensions;
    using Ivh.Common.Data;
    using NHibernate;
    using OfficeOpenXml;
    using OfficeOpenXml.Style;
    using Base.Common.Interfaces;
    using Base.Services;
    using Domain.Guarantor.Entities;
    using Domain.Guarantor.Interfaces;
    using Domain.Matching.Match;
    using Domain.Settings.Entities;
    using Domain.Visit.Unmatching.Entities;
    using Domain.Visit.Unmatching.Interfaces;
    using Ivh.Common.Data.Connection.Connections;
    using Ivh.Common.Data.Interfaces;
    using OfficeOpenXml.Drawing;
    using HsGuarantorDto = Ivh.Common.VisitPay.Messages.HospitalData.Dtos.HsGuarantorDto;
    using Ivh.Common.EventJournal;
    using Ivh.Common.VisitPay.Attributes;
    using Ivh.Common.VisitPay.Constants;
    using Ivh.Common.VisitPay.Enums;
    using Ivh.Common.VisitPay.Messages.HospitalData;
    using Ivh.Common.VisitPay.Messages.HospitalData.Dtos;
    using Ivh.Common.VisitPay.Strings;
    using User.Common.Dtos;
    using Ivh.Application.Matching.Common.Interfaces;

    public class RedactionApplicationService : ApplicationService, IRedactionApplicationService
    {
        private readonly Lazy<IGuarantorService> _guarantorService;
        private readonly Lazy<IUnmatchingService> _unmatchingService;
        private readonly Lazy<IRedactionService> _redactionService;
        private readonly Lazy<IVisitPayUserJournalEventService> _userJournalEventService;
        private readonly ISession _session;
        private readonly ISession _storageSession;
        private readonly Lazy<IVisitService> _visitService;
        private readonly Lazy<IImageService> _imageService;
        private readonly Lazy<IMatchingApplicationService> _matchingApplicationService;

        public RedactionApplicationService(
            Lazy<IApplicationServiceCommonService> applicationServiceCommonService,
            ISessionContext<VisitPay> sessionContext,
            ISessionContext<Storage> storageSessionContext,
            Lazy<IGuarantorService> guarantorService,
            Lazy<IUnmatchingService> unmatchingService,
            Lazy<IVisitPayUserJournalEventService> userJournalEventService,
            Lazy<IVisitService> visitService,
            Lazy<IImageService> imageService,
            Lazy<IRedactionService> redactionService,
            Lazy<IMatchingApplicationService> matchingApplicationService
            ) : base(applicationServiceCommonService)
        {
            this._session = sessionContext.Session;
            this._storageSession = storageSessionContext.Session;
            this._guarantorService = guarantorService;
            this._unmatchingService = unmatchingService;
            this._userJournalEventService = userJournalEventService;
            this._visitService = visitService;
            this._imageService = imageService;
            this._redactionService = redactionService;
            this._matchingApplicationService = matchingApplicationService;
        }

        public void RedactVisit(string sourceSystemKey, int billingSystemId, VisitPayUserDto actionVisitPayUser)
        {
            if (this.FeatureService.Value.IsFeatureEnabled(VisitPayFeatureEnum.FeatureHideRedactedVisitsIsEnabled))
            {
                Visit visit = this._visitService.Value.GetVisitBySystemSourceKey(sourceSystemKey, billingSystemId);
                VisitUnmatchRequestDto requestDto = new VisitUnmatchRequestDto()
                {
                    VisitId = visit.VisitId,
                    ActionVisitPayUser = actionVisitPayUser,
                    Notes = VisitPayFeatureEnum.FeatureHideRedactedVisitsIsEnabled.ToString(),
                    SourceSystemKey = sourceSystemKey,
                    VpGuarantorId = visit.VpGuarantorId,
                    MatchStatus = HsGuarantorMatchStatusEnum.UnmatchedSoft, //Soft unmatch so we can show it again if not redacted later
                    UnmatchReason = HsGuarantorUnmatchReasonEnum.ClientUnmatchedVisitRedacted
                };
                this.ProcessVisitUnmatchRequest(requestDto);
            }
            else
            {
                this._redactionService.Value.RedactVisit(sourceSystemKey, billingSystemId, Mapper.Map<VisitPayUser>(actionVisitPayUser));
            }
        }

        public void ProcessHsGuarantorMatchDiscrepancy(ChangeSetMessage changeSetMessage)
        {
            this.ProcessHsGuarantorWrapper(changeSetMessage, (vpGuarantor, hsGuarantorMapCurrent, hsGuarantorChangeDto, guarantorMatchingFields) =>
            {
                // Create new HsGuarantor MatchDiscrepancy
                HsGuarantorMatchDiscrepancy hsGuarantorMatchDiscrepancy = new HsGuarantorMatchDiscrepancy()
                {
                    HsGuarantorMatchDiscrepancyStatus = HsGuarantorMatchDiscrepancyStatusEnum.Open,
                    HsGuarantorMap = hsGuarantorMapCurrent,
                    VpGuarantor = vpGuarantor,
                    HsGuarantorMatchDiscrepancyMatchFields = new List<HsGuarantorMatchDiscrepancyMatchField>(guarantorMatchingFields.Count)
                };

                IDictionary<string, string> currentValues = this.GetMatchFieldValues(vpGuarantor.User, guarantorMatchingFields);

                // TODO: VP-3437 - do we need to look at historical addresses?
                IDictionary<string, string> currentAddressValues = this.GetMatchFieldValues(vpGuarantor.User.CurrentPhysicalAddress, guarantorMatchingFields);
                currentValues.Merge(currentAddressValues);

                IDictionary<string, string> newValues = this.GetMatchFieldValues(hsGuarantorChangeDto, guarantorMatchingFields);

                foreach (string guarantorMatchingField in guarantorMatchingFields)
                {
                    hsGuarantorMatchDiscrepancy.HsGuarantorMatchDiscrepancyMatchFields.Add(new HsGuarantorMatchDiscrepancyMatchField
                    {
                        HsGuarantorMatchDiscrepancy = hsGuarantorMatchDiscrepancy,
                        HsGuarantorMatchDiscrepancyMatchFieldName = guarantorMatchingField,
                        HsGuarantorMatchDiscrepancyMatchFieldCurrentValue = currentValues.GetValue(guarantorMatchingField) ?? string.Empty,
                        HsGuarantorMatchDiscrepancyMatchFieldNewValue = newValues.GetValue(guarantorMatchingField) ?? string.Empty
                    });
                }

                bool exists = false;
                //check to see if there is already HsGuarantorMatchDiscrepancy with this combination
                IList<HsGuarantorMatchDiscrepancy> hsGuarantorMatchDiscrepanies = this._unmatchingService.Value.GetHsGuarantorMatchDiscrepancies(hsGuarantorMatchDiscrepancy.HsGuarantorMap);
                if (hsGuarantorMatchDiscrepanies.Any(x => x.HsGuarantorMatchDiscrepancyStatus == HsGuarantorMatchDiscrepancyStatusEnum.ValidMatch
                                                          || x.HsGuarantorMatchDiscrepancyStatus == HsGuarantorMatchDiscrepancyStatusEnum.Open))
                {
                    Func<HsGuarantorMatchDiscrepancy, HsGuarantorMatchDiscrepancy, bool> compare = (h1, h2) =>
                    {
                        if (h1.HsGuarantorMatchDiscrepancyMatchFields.Count == h2.HsGuarantorMatchDiscrepancyMatchFields.Count)
                        {
                            bool matches = h1.HsGuarantorMatchDiscrepancyMatchFields
                                .Join(h2.HsGuarantorMatchDiscrepancyMatchFields,
                                    o => o.HsGuarantorMatchDiscrepancyMatchFieldName,
                                    i => i.HsGuarantorMatchDiscrepancyMatchFieldName,
                                    (o, i) => new
                                    {
                                        Set1 = new
                                        {
                                            o.HsGuarantorMatchDiscrepancyMatchFieldCurrentValue,
                                            o.HsGuarantorMatchDiscrepancyMatchFieldNewValue
                                        },
                                        Set2 = new
                                        {
                                            i.HsGuarantorMatchDiscrepancyMatchFieldCurrentValue,
                                            i.HsGuarantorMatchDiscrepancyMatchFieldNewValue
                                        }
                                    })
                                .All(x => (x.Set1.HsGuarantorMatchDiscrepancyMatchFieldCurrentValue ?? string.Empty).Equals(x.Set2.HsGuarantorMatchDiscrepancyMatchFieldCurrentValue ?? string.Empty, StringComparison.OrdinalIgnoreCase) &&
                                          (x.Set1.HsGuarantorMatchDiscrepancyMatchFieldNewValue ?? string.Empty).Equals(x.Set2.HsGuarantorMatchDiscrepancyMatchFieldNewValue ?? string.Empty, StringComparison.OrdinalIgnoreCase));

                            return matches;
                        }

                        return false;
                    };

                    exists = hsGuarantorMatchDiscrepanies.Any(x => compare(x, hsGuarantorMatchDiscrepancy));
                }

                if (!exists)
                {
                    IList<HsGuarantorMatchDiscrepancy> hsGuarantorMatchDiscrepancies = this._unmatchingService.Value.GetHsGuarantorMatchDiscrepancies(hsGuarantorMatchDiscrepancy.HsGuarantorMap);
                    foreach (HsGuarantorMatchDiscrepancy hsGuarantorMatchDiscrepancy1 in hsGuarantorMatchDiscrepancies.Where(x => x.HsGuarantorMatchDiscrepancyStatus == HsGuarantorMatchDiscrepancyStatusEnum.Open))
                    {
                        hsGuarantorMatchDiscrepancy1.HsGuarantorMatchDiscrepancyStatus = HsGuarantorMatchDiscrepancyStatusEnum.Cancelled;
                        this._unmatchingService.Value.UpdateHsGuarantorMatchDiscrepancy(hsGuarantorMatchDiscrepancy1);

                    }

                    this._unmatchingService.Value.QueueHsGuarantorMatchDiscrepancy(hsGuarantorMatchDiscrepancy);
                }
            });

        }

        public void ProcessHsGuarantorUnmatch(ChangeSetMessage changeSetMessage)
        {
            foreach (HsGuarantorDto hsGuarantor in changeSetMessage.HsGuarantors)
            {
                foreach (HsGuarantorMap hsGuarantorMap in this._guarantorService.Value.GetHsGuarantors(hsGuarantor.HsGuarantorId, HsGuarantorMatchStatusEnum.Matched))
                {
                    this._unmatchingService.Value.UnmatchHsGuarantor(hsGuarantorMap, HsGuarantorMatchStatusEnum.UnmatchedSoft, HsGuarantorUnmatchReasonEnum.SystemUnmatchedHSGIDUpdated);
                }
            }
        }

        public void ProcessHsGuarantorVpIneligible(ChangeSetMessage changeSetMessage)
        {
            this.ProcessHsGuarantorWrapper(changeSetMessage, (vpGuarantor, hsGuarantorMapCurrent, hsGuarantorChangeDto, guarantorMatchingFields) =>
            {
                HsGuarantorMatchDiscrepancy hsGuarantorMatchDiscrepancy = new HsGuarantorMatchDiscrepancy()
                {
                    HsGuarantorMatchDiscrepancyStatus = HsGuarantorMatchDiscrepancyStatusEnum.Unmatched,
                    HsGuarantorUnmatchReason = new HsGuarantorUnmatchReason { HsGuarantorUnmatchReasonEnum = HsGuarantorUnmatchReasonEnum.SystemUnmatchedGuarantorTypeChange },
                    HsGuarantorMap = hsGuarantorMapCurrent,
                    VpGuarantor = vpGuarantor,
                    HsGuarantorMatchDiscrepancyMatchFields = new List<HsGuarantorMatchDiscrepancyMatchField>(guarantorMatchingFields.Count),
                    ActionDate = DateTime.UtcNow,
                    ActionVisitPayUser = new VisitPayUser { VisitPayUserId = SystemUsers.SystemUserId }
                };
                this._unmatchingService.Value.QueueHsGuarantorMatchDiscrepancy(hsGuarantorMatchDiscrepancy);

                this._unmatchingService.Value.UnmatchHsGuarantor(hsGuarantorMapCurrent, HsGuarantorMatchStatusEnum.UnmatchedSoft, HsGuarantorUnmatchReasonEnum.SystemUnmatchedGuarantorTypeChange);
            });
        }

        public void ProcessHsGuarantorWrapper(ChangeSetMessage changeSetMessage, Action<Guarantor, HsGuarantorMap, HsGuarantorChangeDto, IList<string>> action)
        {
            if (changeSetMessage != null && changeSetMessage.ChangeEvent.ChangeEventType.IsInCategory(ChangeEventTypeEnumCategory.Unmatch))
            {
                try
                {
                    using (UnitOfWork uow = new UnitOfWork(this._session))
                    {
                        IList<int> vpGuarantorIds = changeSetMessage.VpGuarantorHsMatches.Select(x => x.VpGuarantorId).ToList();
                        IList<Guarantor> guarantors = this._guarantorService.Value.GetGuarantorsFromIntList(vpGuarantorIds);
                        if (guarantors.IsNotNullOrEmpty())
                        {
                            foreach (Guarantor vpGuarantor in guarantors)
                            {
                                foreach (HsGuarantorMap hsGuarantorMapCurrent in vpGuarantor.MatchedHsGuarantorMaps)
                                {
                                    HsGuarantorChangeDto hsGuarantorChangeDto = changeSetMessage
                                        .HsGuarantorChanges
                                        .FirstOrDefault(x =>
                                            x.SourceSystemKey == hsGuarantorMapCurrent.SourceSystemKey
                                                && x.BillingSystemId == hsGuarantorMapCurrent.BillingSystem.BillingSystemId
                                                && x.HsGuarantorId == hsGuarantorMapCurrent.HsGuarantorId);

                                    if (hsGuarantorChangeDto != null)
                                    {
                                        IList<string> guarantorMatchingFields = this.GetGuarantorMatchingFields();

                                        action(vpGuarantor, hsGuarantorMapCurrent, hsGuarantorChangeDto, guarantorMatchingFields);
                                    }
                                }
                            }
                        }

                        uow.Commit();
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    throw;
                }
            }
        }

        public HsGuarantorMatchDiscrepancyResultsDto GetAllHsGuarantorAttributeChanges(HsGuarantorMatchDiscrepancyFilterDto filterDto, int pageNumber, int pageSize)
        {
            HsGuarantorMatchDiscrepancyFilter filter = Mapper.Map<HsGuarantorMatchDiscrepancyFilter>(filterDto);

            HsGuarantorMatchDiscrepancyResults results = this._unmatchingService.Value.GetAllHsGuarantorMatchDiscrepancies(filter, true, Math.Max(pageNumber - 1, 0), pageSize);

            return Mapper.Map<HsGuarantorMatchDiscrepancyResultsDto>(results);
        }

        public HsGuarantorMatchDiscrepancyResultsDto GetAllHsGuarantorMatchDiscrepancy(HsGuarantorMatchDiscrepancyFilterDto filterDto, int pageNumber, int pageSize)
        {
            HsGuarantorMatchDiscrepancyFilter filter = Mapper.Map<HsGuarantorMatchDiscrepancyFilter>(filterDto);

            HsGuarantorMatchDiscrepancyResults results = this._unmatchingService.Value.GetAllHsGuarantorMatchDiscrepancies(filter, false, Math.Max(pageNumber - 1, 0), pageSize);

            return Mapper.Map<HsGuarantorMatchDiscrepancyResultsDto>(results);
        }

        public VisitUnmatchResultsDto GetAllVisitUnmatches(VisitUnmatchFilterDto filterDto, int pageNumber, int pageSize)
        {
            VisitUnmatchFilter filter = Mapper.Map<VisitUnmatchFilter>(filterDto);

            VisitUnmatchResults results = this._unmatchingService.Value.GetAllVisitUnmatches(filter, Math.Max(pageNumber - 1, 0), pageSize);

            IList<VisitUnmatchDto> unmatchedVisitsDto = new List<VisitUnmatchDto>();

            foreach (Visit visit in results.VisitUnmatches)
            {
                VisitUnmatchDto visitUnmatchDto = Mapper.Map<VisitUnmatchDto>(visit);

                Visit rematchedVisit = this._visitService.Value.GetVisitBySystemSourceKey(visit.MatchedSourceSystemKey, visit.MatchedBillingSystem.BillingSystemId);
                if (rematchedVisit != null)
                {
                    visitUnmatchDto.VpGuarantorCurrent = Mapper.Map<GuarantorDto>(rematchedVisit.VPGuarantor);
                    visitUnmatchDto.HsGuarantorSourceSystemKeyCurrent = rematchedVisit.HsGuarantorMap.SourceSystemKey;
                }

                unmatchedVisitsDto.Add(visitUnmatchDto);
            }

            VisitUnmatchResultsDto resultsDto = new VisitUnmatchResultsDto
            {
                TotalRecords = results.TotalRecords,
                VisitUnmatches = (IReadOnlyList<VisitUnmatchDto>)unmatchedVisitsDto
            };

            return resultsDto;
        }

        public IReadOnlyList<HsGuarantorMatchDiscrepancyStatusDto> GetAllHsGuarantorMatchDiscrepancyStatuses()
        {
            return Mapper.Map<IReadOnlyList<HsGuarantorMatchDiscrepancyStatusDto>>(this._unmatchingService.Value.GetAllHsGuarantorMatchDiscrepancyStatuses());
        }

        public IDictionary<string, string> GetMatchFieldsForDisplay()
        {
            IList<string> guarantorMatchingFields = this.GetGuarantorMatchingFields();

            VisitPayUser temp = new VisitPayUser();
            VisitPayUserAddress tempAddress = new VisitPayUserAddress();
            IDictionary<string, string> userMatchFieldsDisplay = GuarantorMatchField.GetGuarantorMatchFieldsForDisplay(temp, guarantorMatchingFields);
            IDictionary<string, string> userAddressMatchFieldsDisplay = GuarantorMatchField.GetGuarantorMatchFieldsForDisplay(tempAddress, guarantorMatchingFields);

            return userMatchFieldsDisplay.Merge(userAddressMatchFieldsDisplay);
        }

        private IDictionary<string, string> GetMatchFieldValues(object matchFieldsObject, IList<string> guarantorMatchingFields)
        {
            if (matchFieldsObject == null)
            {
                throw new ArgumentNullException(nameof(matchFieldsObject));
            }

            return GuarantorMatchField.GetGuantorMatchFields(matchFieldsObject, guarantorMatchingFields);
        }

        public void UpdateHsGuarantorMatchDiscrepancy(HsGuarantorMatchDiscrepancyDto hsGuarantorMatchDiscrepancyDto)
        {
            this._unmatchingService.Value.UpdateHsGuarantorMatchDiscrepancy(Mapper.Map<HsGuarantorMatchDiscrepancy>(hsGuarantorMatchDiscrepancyDto));
        }

        public bool ProcessGuarantorUnmatchRequest(HsGuarantorUnmatchRequestDto requestDto, UnmatchSourceEnum unmatchSource)
        {
            if (requestDto.HsGuarantorSourceSystemKeys == null || !requestDto.HsGuarantorSourceSystemKeys.Any())
            {
                return false;
            }

            VisitPayUser actionVisitPayUser = Mapper.Map<VisitPayUser>(requestDto.ActionVisitPayUser);

            foreach (string hsGuarantorSourceSystemKey in requestDto.HsGuarantorSourceSystemKeys)
            {
                using (UnitOfWork unitOfWork = new UnitOfWork(this._session))
                {
                    Guarantor guarantor = this._guarantorService.Value.GetGuarantor(requestDto.VpGuarantorId);
                    if (guarantor == null)
                    {
                        continue;
                    }

                    HsGuarantorMap hsGuarantorMap = guarantor.MatchedHsGuarantorMaps.FirstOrDefault(x => x.SourceSystemKey == hsGuarantorSourceSystemKey);
                    if (hsGuarantorMap == null)
                    {
                        continue;
                    }

                    //
                    HsGuarantorMatchDiscrepancy discrepancy = null;
                    HsGuarantorUnmatchReasonEnum hsGuarantorUnmatchReasonEnum = unmatchSource == UnmatchSourceEnum.GuarantorAccountDetails ? HsGuarantorUnmatchReasonEnum.ClientUnmatchedGuarantorManual : HsGuarantorUnmatchReasonEnum.ClientUnmatchedMatchCriteriaChanged;
                    HsGuarantorMatchStatusEnum hsGuarantorUnmatchStatus = unmatchSource == UnmatchSourceEnum.GuarantorAccountDetails ? HsGuarantorMatchStatusEnum.UnmatchedHard : HsGuarantorMatchStatusEnum.UnmatchedSoft;

                    if (requestDto.HsGuarantorMatchDiscrepancyId.HasValue)// <== todo: if not null, came from queue
                    {
                        discrepancy = this._unmatchingService.Value.GetHsGuarantorMatchDiscrepancy(hsGuarantorMap, requestDto.HsGuarantorMatchDiscrepancyId.Value);
                    }

                    if (discrepancy == null)
                    {
                        discrepancy = this._unmatchingService.Value.GetOpenHsGuarantorMatchDiscrepancy(hsGuarantorMap.HsGuarantorId, hsGuarantorMap.VpGuarantor.VpGuarantorId);
                    }

                    if (discrepancy != null)
                    {
                        hsGuarantorMap = discrepancy.HsGuarantorMap;

                        HsGuarantorMatchStatusEnum hsGuarantorMatchStatusEnum = HsGuarantorMatchStatusEnum.NotSet;
                        HsGuarantorMatchDiscrepancyStatusEnum hsGuarantorMatchDiscrepancyStatusEnum;

                        switch (requestDto.UnmatchActionType)
                        {
                            case UnmatchActionTypeEnum.GuarantorUnmatch:
                                hsGuarantorMatchDiscrepancyStatusEnum = HsGuarantorMatchDiscrepancyStatusEnum.Unmatched;
                                hsGuarantorMatchStatusEnum = hsGuarantorUnmatchStatus;
                                break;
                            case UnmatchActionTypeEnum.GuarantorValidationMatch:
                                hsGuarantorMatchDiscrepancyStatusEnum = HsGuarantorMatchDiscrepancyStatusEnum.ValidMatch;
                                hsGuarantorUnmatchReasonEnum = HsGuarantorUnmatchReasonEnum.ClientGuarantorMatchingCriteriaValidated;
                                break;
                            default:
                                throw new ArgumentOutOfRangeException();
                        }

                        discrepancy.HsGuarantorMatchDiscrepancyStatus = hsGuarantorMatchDiscrepancyStatusEnum;
                        discrepancy.ActionDate = DateTime.UtcNow;
                        discrepancy.ActionNotes = requestDto.Notes;
                        discrepancy.ActionVisitPayUser = new VisitPayUser { VisitPayUserId = actionVisitPayUser.VisitPayUserId };
                        discrepancy.HsGuarantorUnmatchReason = new HsGuarantorUnmatchReason { HsGuarantorUnmatchReasonEnum = hsGuarantorUnmatchReasonEnum };

                        this._unmatchingService.Value.UpdateHsGuarantorMatchDiscrepancy(discrepancy);

                        switch (discrepancy.HsGuarantorMatchDiscrepancyStatus)
                        {
                            case HsGuarantorMatchDiscrepancyStatusEnum.Unmatched:
                                this._unmatchingService.Value.UnmatchHsGuarantor(hsGuarantorMap, hsGuarantorMatchStatusEnum, hsGuarantorUnmatchReasonEnum, actionVisitPayUser, requestDto.Notes);
                                break;
                            case HsGuarantorMatchDiscrepancyStatusEnum.ValidMatch:
                                this.LogGuarantorMismatchIgnored(actionVisitPayUser, hsGuarantorMap.HsGuarantorId, hsGuarantorMap.VpGuarantor.VpGuarantorId);
                                break;
                            case HsGuarantorMatchDiscrepancyStatusEnum.Open:
                                break;
                            case HsGuarantorMatchDiscrepancyStatusEnum.Cancelled:
                                break;
                            default:
                                throw new ArgumentOutOfRangeException();
                        }
                    }
                    else
                    {
                        hsGuarantorMap.HsGuarantorMatchStatus = hsGuarantorUnmatchStatus;

                        discrepancy = new HsGuarantorMatchDiscrepancy()
                        {
                            VpGuarantor = hsGuarantorMap.VpGuarantor,
                            HsGuarantorMap = hsGuarantorMap,
                            HsGuarantorMatchDiscrepancyStatus = HsGuarantorMatchDiscrepancyStatusEnum.Unmatched,
                            ActionDate = DateTime.UtcNow,
                            ActionVisitPayUser = new VisitPayUser { VisitPayUserId = actionVisitPayUser.VisitPayUserId },
                            ActionNotes = requestDto.Notes,
                            HsGuarantorUnmatchReason = new HsGuarantorUnmatchReason() { HsGuarantorUnmatchReasonEnum = hsGuarantorUnmatchReasonEnum }
                        };
                        this._unmatchingService.Value.UpdateHsGuarantorMatchDiscrepancy(discrepancy);
                        this._unmatchingService.Value.UnmatchHsGuarantor(hsGuarantorMap, hsGuarantorUnmatchStatus, hsGuarantorUnmatchReasonEnum, actionVisitPayUser, requestDto.Notes);
                    }
                    unitOfWork.Commit();
                }
            }

            return true;
        }

        public bool ProcessVisitUnmatchRequest(VisitUnmatchRequestDto requestDto)
        {
            // this could be refactored a little bit because we're getting the visit again in UnmatchVisit
            // maybe another method on UnmatchService passing in a visit
            Visit visit = this._visitService.Value.GetVisit(requestDto.VpGuarantorId, requestDto.VisitId);
            if (visit == null || visit.HsGuarantorMap == null)
            {
                return false;
            }


            if (!this.FeatureService.Value.IsFeatureEnabled(VisitPayFeatureEnum.FeatureVisitUnmatchingMethodSelection))
            {
                // Change NotSet to UnmatchedHard if FeatureVisitUnmatchingMethodSelection is turned off
                if (requestDto.MatchStatus == HsGuarantorMatchStatusEnum.NotSet)
                {
                    requestDto.MatchStatus = HsGuarantorMatchStatusEnum.UnmatchedHard;
                }
            }

            VisitPayUser actionVisitPayUser = Mapper.Map<VisitPayUser>(requestDto.ActionVisitPayUser);
            using (UnitsOfWork unitsOfWork = new UnitsOfWork(this._session, this._storageSession))
            {
                this._unmatchingService.Value.UnmatchVisit(visit.HsGuarantorMap, visit.VisitId, requestDto.MatchStatus, requestDto.UnmatchReason, actionVisitPayUser, requestDto.Notes);
                unitsOfWork.Commit();
            }

            return true;
        }

        private IList<string> GetGuarantorMatchingFields()
        {
            IList<string> guarantorMatchingFields = null;
            if (this.FeatureService.Value.IsFeatureEnabled(VisitPayFeatureEnum.FeatureMatchingApiIsEnabled))
            {
                guarantorMatchingFields = this._matchingApplicationService.Value.GetRequiredMatchFieldsGuarantor(ApplicationEnum.VisitPay, PatternUseEnum.Ongoing);
            }
            else
            {
                Client client = this.ClientService.Value.GetClient();
                guarantorMatchingFields = client.GuarantorMatchingFields;
            }

            return guarantorMatchingFields;
        }

        #region logging

        private void LogGuarantorMismatchIgnored(VisitPayUser actionVisitPayUser, int hsGuarantorId, int vpGuarantorId)
        {
            JournalEventUserContext journalEventUserContext = this._userJournalEventService.Value.GetJournalEventUserContext(Mapper.Map<JournalEventHttpContextDto>(HttpContext.Current), actionVisitPayUser);
            this._userJournalEventService.Value.LogGuarantorMismatchIgnored(journalEventUserContext, hsGuarantorId, vpGuarantorId);
        }

        #endregion

        #region export

        static string GetColumnName(int index)
        {
            const string letters = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";

            string value = "";

            if (index >= letters.Length)
                value += letters[index / letters.Length - 1];

            value += letters[index % letters.Length];

            return value;
        }

        public async Task<byte[]> ExportAllHsGuarantorMatchDiscrepancyAsync(HsGuarantorMatchDiscrepancyFilterDto filterDto, string userName)
        {
            Client client = this.ClientService.Value.GetClient();

            const int maxExportRecords = 10000;

            HsGuarantorMatchDiscrepancyResultsDto results = this.GetAllHsGuarantorAttributeChanges(filterDto, 0, maxExportRecords);

            IDictionary<string, string> matchFields = this.GetMatchFieldsForDisplay();

            const int matchFieldColumnIndex = 5;
            string lastLetter = GetColumnName(matchFieldColumnIndex + matchFields.Count);

            using (ExcelPackage package = new ExcelPackage())
            {
                ExcelWorksheet worksheet = package.Workbook.Worksheets.Add("Guarantor Attribute Changes");

                // title
                worksheet.Cells["A5"].Value = "Guarantor Attribute Changes";
                worksheet.Cells["A5:I5"].Merge = true;
                using (ExcelRange range = worksheet.Cells["A5:I5"])
                {
                    range.Style.Font.Size = 13;
                    range.Style.Font.Bold = true;
                }

                // export info
                worksheet.Cells["A7"].Value = "File Date:";
                worksheet.Cells["A7:C7"].Merge = true;
                worksheet.Cells["D7"].Value = DateTime.UtcNow.ToString(Format.MonthDayYearFormat);
                worksheet.Cells["D7:I7"].Merge = true;

                worksheet.Cells["A8"].Value = "Exported By:";
                worksheet.Cells["A8:C8"].Merge = true;
                worksheet.Cells["D8"].Value = userName;
                worksheet.Cells["D8:I8"].Merge = true;

                using (ExcelRange range = worksheet.Cells["A7:I8"])
                {
                    range.Style.Font.Size = 11;
                    range.Style.Font.Bold = true;
                }

                // data rows
                if (results.TotalRecords > 0)
                {
                    // too many records
                    if (results.TotalRecords > maxExportRecords)
                    {
                        worksheet.Cells["A10"].Value = string.Format("Incomplete download. {0} rows of {1} rows were downloaded. Only {0} rows can be downloaded.", maxExportRecords.ToString("N0"), results.TotalRecords.ToString("N0"));
                        worksheet.Cells["A10:I10"].Merge = true;
                        using (ExcelRange range = worksheet.Cells["A10:I10"])
                        {
                            range.Style.Font.Italic = true;
                        }
                    }

                    int headerIndex = (results.TotalRecords > maxExportRecords) ? 12 : 10;

                    // data headers
                    worksheet.Cells[string.Format("A{0}", headerIndex)].Value = "Status";
                    worksheet.Column(1).Width = 18;

                    worksheet.Cells[string.Format("B{0}", headerIndex)].Value = "Date";
                    worksheet.Column(2).Width = 13;

                    worksheet.Cells[string.Format("C{0}", headerIndex)].Value = "Guarantor";
                    worksheet.Column(3).Width = 22;

                    worksheet.Cells[string.Format("D{0}", headerIndex)].Value = client.HsGuarantorPatientIdentifier;
                    worksheet.Column(4).Width = 13;

                    worksheet.Cells[string.Format("E{0}", headerIndex)].Value = client.VpGuarantorPatientIdentifier;
                    worksheet.Column(5).Width = 13;

                    worksheet.Cells[string.Format("{0}{1}", lastLetter, headerIndex)].Value = "Notes (expand row to see all text)";

                    // match field headers
                    int headerColumnIndex = matchFieldColumnIndex;
                    foreach (KeyValuePair<string, string> matchField in matchFields)
                    {
                        worksheet.Cells[string.Format("{0}{1}", GetColumnName(headerColumnIndex), headerIndex)].Value = matchField.Value;
                        worksheet.Column(headerColumnIndex + 1).Width = 18;
                        headerColumnIndex++;
                    }

                    worksheet.Column(headerColumnIndex + 1).Width = 120;

                    using (ExcelRange range = worksheet.Cells[string.Format("A{0}:{1}{0}", headerIndex, lastLetter)])
                    {
                        range.Style.Font.Size = 11;
                        range.Style.Font.Bold = true;
                        range.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        range.Style.Border.BorderAround(ExcelBorderStyle.Medium);
                        range.Style.Border.Right.Style = ExcelBorderStyle.Medium;
                    }

                    // data cells
                    int startRowIndex = headerIndex + 1;
                    int rowIndex = startRowIndex;

                    foreach (HsGuarantorMatchDiscrepancyDto discrepancy in results.HsGuarantorMatchDiscrepancies)
                    {
                        int nextIndex = rowIndex + 1;

                        worksheet.Cells[string.Format("A{0}", rowIndex)].Value = discrepancy.HsGuarantorMatchDiscrepancyStatusName;
                        worksheet.Cells[string.Format("B{0}", rowIndex)].Value = discrepancy.InsertDate.ToString(Format.DateFormat);
                        worksheet.Cells[string.Format("C{0}", rowIndex)].Value = discrepancy.VpGuarantor.User.DisplayFirstNameLastName;
                        worksheet.Cells[string.Format("D{0}", rowIndex)].Value = discrepancy.HsGuarantorSourceSystemKey;
                        worksheet.Cells[string.Format("E{0}", rowIndex)].Value = discrepancy.VpGuarantor.VpGuarantorId;

                        int columnIndex = matchFieldColumnIndex;
                        foreach (KeyValuePair<string, string> matchField in matchFields)
                        {
                            string letter = GetColumnName(columnIndex);

                            HsGuarantorMatchDiscrepancyMatchFieldDto thisMatchField = discrepancy.HsGuarantorMatchDiscrepancyMatchFields.FirstOrDefault(x => string.Equals(x.HsGuarantorMatchDiscrepancyMatchFieldName, matchField.Key, StringComparison.CurrentCultureIgnoreCase));
                            if (thisMatchField != null)
                            {
                                // using .Formula + CONCATENATE instead of .Value to avoid "number stored as text" warning and force text display.
                                // these are all strings, but might be interpreted as a number in excel.
                                worksheet.Cells[string.Format("{0}{1}", letter, rowIndex)].Formula = string.Format("=CONCATENATE(\"{0}\")", thisMatchField.HsGuarantorMatchDiscrepancyMatchFieldNewValue);
                                worksheet.Cells[string.Format("{0}{1}", letter, nextIndex)].Formula = string.Format("=CONCATENATE(\"{0}\")", thisMatchField.HsGuarantorMatchDiscrepancyMatchFieldCurrentValue);

                                worksheet.Cells[string.Format("{0}{1}", letter, rowIndex)].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                                worksheet.Cells[string.Format("{0}{1}", letter, nextIndex)].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                                if (thisMatchField.IsDiscrepancy)
                                {
                                    worksheet.Cells[string.Format("{0}{1}", letter, rowIndex)].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                    worksheet.Cells[string.Format("{0}{1}", letter, rowIndex)].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.FromArgb(252, 228, 214));

                                    worksheet.Cells[string.Format("{0}{1}", letter, nextIndex)].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                    worksheet.Cells[string.Format("{0}{1}", letter, nextIndex)].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.FromArgb(252, 228, 214));
                                }
                            }

                            columnIndex++;
                        }

                        if (discrepancy.HsGuarantorMatchDiscrepancyStatus != HsGuarantorMatchDiscrepancyStatusEnum.Open)
                        {
                            string actionDate = discrepancy.ActionDate.HasValue ? discrepancy.ActionDate.Value.ToString(Format.DateFormat) : "";
                            string actionUserName = discrepancy.ActionVisitPayUser != null ? discrepancy.ActionVisitPayUser.UserName : "";
                            string range = string.Format("{0}{1}", lastLetter, rowIndex);

                            worksheet.Cells[range].Value = discrepancy.HsGuarantorMatchDiscrepancyStatusName;

                            if (!string.IsNullOrEmpty(actionDate))
                            {
                                worksheet.Cells[range].Value += string.Format(" {0}", actionDate);
                            }

                            if (!string.IsNullOrEmpty(actionUserName))
                            {
                                worksheet.Cells[range].Value += string.Format(" by {0}", actionUserName);
                            }

                            if (!string.IsNullOrEmpty(discrepancy.ActionNotes))
                            {
                                worksheet.Cells[range].Value += string.Format(" - {0}", discrepancy.ActionNotes);
                            }

                            range = string.Format("{0}{1}:{0}{2}", lastLetter, rowIndex, nextIndex);
                            worksheet.Cells[range].Merge = true;
                            worksheet.Cells[range].Style.VerticalAlignment = ExcelVerticalAlignment.Top;
                            worksheet.Cells[range].Style.WrapText = true;
                        }

                        // merge cells
                        worksheet.Cells[string.Format("A{0}:A{1}", rowIndex, nextIndex)].Merge = true;
                        worksheet.Cells[string.Format("B{0}:B{1}", rowIndex, nextIndex)].Merge = true;
                        worksheet.Cells[string.Format("C{0}:C{1}", rowIndex, nextIndex)].Merge = true;
                        worksheet.Cells[string.Format("D{0}:D{1}", rowIndex, nextIndex)].Merge = true;
                        worksheet.Cells[string.Format("E{0}:E{1}", rowIndex, nextIndex)].Merge = true;

                        worksheet.Cells[string.Format("A{0}:E{1}", rowIndex, nextIndex)].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                        worksheet.Cells[string.Format("A{0}:E{1}", rowIndex, nextIndex)].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;

                        worksheet.Row(rowIndex).Height = 40;
                        worksheet.Row(nextIndex).Height = 40;

                        rowIndex = rowIndex + 2;
                    }

                    // border
                    using (ExcelRange range = worksheet.Cells[string.Format("A{0}:{1}{2}", startRowIndex, lastLetter, rowIndex - 1)])
                    {
                        range.Style.Border.BorderAround(ExcelBorderStyle.Thin);
                        range.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                        range.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                    }
                }
                else
                {
                    worksheet.Cells["A10"].Value = "There are no guarantor attribute changes to display at this time.";
                    worksheet.Cells["A10:I10"].Merge = true;
                    using (ExcelRange range = worksheet.Cells["A10:I10"])
                    {
                        range.Style.Font.Size = 11;
                        range.Style.Font.Bold = true;
                    }
                }

                System.Drawing.Image logo = await this._imageService.Value.GetExportLogoAsync();
                ExcelPicture pic = worksheet.Drawings.AddPicture("logo", logo);
                pic.SetPosition(0, 0);
                pic.SetSize(logo.Width, logo.Height);
                using (ExcelRange range = worksheet.Cells["A1:I4"])
                {
                    range.Merge = true;
                }

                return package.GetAsByteArray();
            }
        }

        public async Task<byte[]> ExportGuarantorSummaryReportAsync(HsGuarantorMatchDiscrepancyFilterDto filterDto, string userName)
        {
            Client client = this.ClientService.Value.GetClient();

            const int maxExportRecords = 10000;

            HsGuarantorMatchDiscrepancyResultsDto results = this.GetAllHsGuarantorMatchDiscrepancy(filterDto, 0, maxExportRecords);

            using (ExcelPackage package = new ExcelPackage())
            {
                ExcelWorksheet worksheet = package.Workbook.Worksheets.Add("Guarantor Summary Report");

                // title
                worksheet.Cells["A5"].Value = "Guarantor Summary Report";
                worksheet.Cells["A5:I5"].Merge = true;
                using (ExcelRange range = worksheet.Cells["A5:I5"])
                {
                    range.Style.Font.Size = 13;
                    range.Style.Font.Bold = true;
                }

                // export info
                worksheet.Cells["A7"].Value = "File Date:";
                worksheet.Cells["A7:C7"].Merge = true;
                worksheet.Cells["D7"].Value = DateTime.UtcNow.ToString(Format.MonthDayYearFormat);
                worksheet.Cells["D7:I7"].Merge = true;

                worksheet.Cells["A8"].Value = "Exported By:";
                worksheet.Cells["A8:C8"].Merge = true;
                worksheet.Cells["D8"].Value = userName;
                worksheet.Cells["D8:I8"].Merge = true;

                using (ExcelRange range = worksheet.Cells["A7:I8"])
                {
                    range.Style.Font.Size = 11;
                    range.Style.Font.Bold = true;
                }

                // data rows
                if (results.TotalRecords > 0)
                {
                    // too many records
                    if (results.TotalRecords > maxExportRecords)
                    {
                        worksheet.Cells["A10"].Value = string.Format("Incomplete download. {0} rows of {1} rows were downloaded. Only {0} rows can be downloaded.", maxExportRecords.ToString("N0"), results.TotalRecords.ToString("N0"));
                        worksheet.Cells["A10:I10"].Merge = true;
                        using (ExcelRange range = worksheet.Cells["A10:I10"])
                        {
                            range.Style.Font.Italic = true;
                        }
                    }

                    // headers 1
                    int headerIndex = (results.TotalRecords > maxExportRecords) ? 12 : 10;

                    worksheet.Cells[string.Format("A{0}", headerIndex)].Value = "Insert Date";
                    worksheet.Cells[string.Format("A{0}", headerIndex)].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    worksheet.Cells[string.Format("A{0}:A{1}", headerIndex, headerIndex + 1)].Merge = true;
                    worksheet.Column(1).Width = 13;

                    worksheet.Cells[string.Format("B{0}", headerIndex)].Value = "Change Date";
                    worksheet.Cells[string.Format("B{0}", headerIndex)].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    worksheet.Cells[string.Format("B{0}:B{1}", headerIndex, headerIndex + 1)].Merge = true;
                    worksheet.Column(2).Width = 13;

                    worksheet.Cells[string.Format("C{0}", headerIndex)].Value = "Status";
                    worksheet.Cells[string.Format("C{0}", headerIndex)].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    worksheet.Cells[string.Format("C{0}:C{1}", headerIndex, headerIndex + 1)].Merge = true;
                    worksheet.Column(3).Width = 18;

                    worksheet.Cells[string.Format("D{0}:E{0}", headerIndex)].Value = "Previous Match";
                    worksheet.Cells[string.Format("D{0}:E{0}", headerIndex)].Merge = true;

                    worksheet.Cells[string.Format("F{0}:G{0}", headerIndex)].Value = "Current Match";
                    worksheet.Cells[string.Format("F{0}:G{0}", headerIndex)].Merge = true;

                    worksheet.Cells[string.Format("H{0}", headerIndex)].Value = "Status Reason";
                    worksheet.Cells[string.Format("H{0}", headerIndex)].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    worksheet.Cells[string.Format("H{0}:H{1}", headerIndex, headerIndex + 1)].Merge = true;
                    worksheet.Column(8).Width = 35;

                    worksheet.Cells[string.Format("I{0}", headerIndex)].Value = "Username";
                    worksheet.Cells[string.Format("I{0}", headerIndex)].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    worksheet.Cells[string.Format("I{0}:I{1}", headerIndex, headerIndex + 1)].Merge = true;
                    worksheet.Column(9).Width = 18;

                    worksheet.Cells[string.Format("J{0}", headerIndex)].Value = "Notes (expand row to see all text)";
                    worksheet.Cells[string.Format("J{0}", headerIndex)].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    worksheet.Cells[string.Format("J{0}:J{1}", headerIndex, headerIndex + 1)].Merge = true;
                    worksheet.Column(10).Width = 120;


                    // headers 2
                    headerIndex = headerIndex + 1;

                    worksheet.Cells[string.Format("D{0}", headerIndex)].Value = client.HsGuarantorPatientIdentifier;
                    worksheet.Cells[string.Format("D{0}", headerIndex)].Style.Border.Top.Style = ExcelBorderStyle.Medium;
                    worksheet.Column(4).Width = 13;

                    worksheet.Cells[string.Format("E{0}", headerIndex)].Value = client.VpGuarantorPatientIdentifier;
                    worksheet.Cells[string.Format("E{0}", headerIndex)].Style.Border.Top.Style = ExcelBorderStyle.Medium;
                    worksheet.Column(5).Width = 13;

                    worksheet.Cells[string.Format("F{0}", headerIndex)].Value = client.HsGuarantorPatientIdentifier;
                    worksheet.Cells[string.Format("F{0}", headerIndex)].Style.Border.Top.Style = ExcelBorderStyle.Medium;
                    worksheet.Column(6).Width = 13;

                    worksheet.Cells[string.Format("G{0}", headerIndex)].Value = client.VpGuarantorPatientIdentifier;
                    worksheet.Cells[string.Format("G{0}", headerIndex)].Style.Border.Top.Style = ExcelBorderStyle.Medium;
                    worksheet.Column(7).Width = 13;

                    // headers border
                    using (ExcelRange range = worksheet.Cells[string.Format("A{0}:J{1}", headerIndex - 1, headerIndex)])
                    {
                        range.Style.Font.Size = 11;
                        range.Style.Font.Bold = true;
                        range.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        range.Style.Border.BorderAround(ExcelBorderStyle.Medium);
                        range.Style.Border.Right.Style = ExcelBorderStyle.Medium;
                    }

                    // data cells
                    int startRowIndex = headerIndex + 1;
                    int rowIndex = startRowIndex;

                    foreach (HsGuarantorMatchDiscrepancyDto discrepancy in results.HsGuarantorMatchDiscrepancies)
                    {
                        worksheet.Cells[string.Format("A{0}", rowIndex)].Value = discrepancy.InsertDate.ToString(Format.DateFormat);
                        worksheet.Cells[string.Format("B{0}", rowIndex)].Value = discrepancy.ActionDate.HasValue ? discrepancy.ActionDate.Value.ToString(Format.DateFormat) : "";
                        worksheet.Cells[string.Format("C{0}", rowIndex)].Value = discrepancy.HsGuarantorMatchDiscrepancyStatusName;
                        worksheet.Cells[string.Format("D{0}", rowIndex)].Value = discrepancy.HsGuarantorSourceSystemKey;
                        worksheet.Cells[string.Format("E{0}", rowIndex)].Value = discrepancy.VpGuarantor.VpGuarantorId;
                        worksheet.Cells[string.Format("F{0}", rowIndex)].Value = discrepancy.HsGuarantorSourceSystemKey;
                        worksheet.Cells[string.Format("G{0}", rowIndex)].Value = discrepancy.HsGuarantorVpGuarantorId;
                        worksheet.Cells[string.Format("H{0}", rowIndex)].Value = discrepancy.HsGuarantorUnmatchReasonDisplayName;
                        worksheet.Cells[string.Format("I{0}", rowIndex)].Value = discrepancy.ActionVisitPayUser == null ? "" : discrepancy.ActionVisitPayUser.VisitPayUserId == SystemUsers.SystemUserId ? "System" : discrepancy.ActionVisitPayUser.UserName;
                        worksheet.Cells[string.Format("J{0}", rowIndex)].Value = discrepancy.ActionNotes;
                        worksheet.Cells[string.Format("J{0}", rowIndex)].Style.VerticalAlignment = ExcelVerticalAlignment.Top;
                        worksheet.Cells[string.Format("J{0}", rowIndex)].Style.WrapText = true;
                        worksheet.Row(rowIndex).Height = 80;

                        worksheet.Cells[string.Format("A{0}:J{0}", rowIndex)].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                        worksheet.Cells[string.Format("A{0}:I{0}", rowIndex)].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                        rowIndex = rowIndex + 1;
                    }

                    // border
                    using (ExcelRange range = worksheet.Cells[string.Format("A{0}:J{1}", startRowIndex, rowIndex - 1)])
                    {
                        range.Style.Border.BorderAround(ExcelBorderStyle.Thin);
                        range.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                        range.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                    }
                }
                else
                {
                    worksheet.Cells["A10"].Value = "There is no data matching your search criteria.";
                    worksheet.Cells["A10:I10"].Merge = true;
                    using (ExcelRange range = worksheet.Cells["A10:I10"])
                    {
                        range.Style.Font.Size = 11;
                        range.Style.Font.Bold = true;
                    }
                }

                System.Drawing.Image logo = await this._imageService.Value.GetExportLogoAsync();
                ExcelPicture pic = worksheet.Drawings.AddPicture("logo", logo);
                pic.SetPosition(0, 0);
                pic.SetSize(logo.Width, logo.Height);
                using (ExcelRange range = worksheet.Cells["A1:I4"])
                {
                    range.Merge = true;
                }

                return package.GetAsByteArray();
            }
        }

        public async Task<byte[]> ExportVisitUnmatchReportAsync(VisitUnmatchFilterDto filterDto, string userName)
        {
            Client client = this.ClientService.Value.GetClient();

            const int maxExportRecords = 10000;

            VisitUnmatchResultsDto results = this.GetAllVisitUnmatches(filterDto, 0, maxExportRecords);

            using (ExcelPackage package = new ExcelPackage())
            {
                ExcelWorksheet worksheet = package.Workbook.Worksheets.Add("Visit Unmatch Report");
                // title //note offset of 5 to account for logo
                worksheet.Cells["A5"].Value = "Visit Unmatch Report";
                worksheet.Cells["A5:I5"].Merge = true;
                using (ExcelRange range = worksheet.Cells["A5:I5"])
                {
                    range.Style.Font.Size = 13;
                    range.Style.Font.Bold = true;
                }

                // export info
                worksheet.Cells["A7"].Value = "File Date:";
                worksheet.Cells["A7:C7"].Merge = true;
                worksheet.Cells["D7"].Value = DateTime.UtcNow.ToString(Format.MonthDayYearFormat);
                worksheet.Cells["D7:I7"].Merge = true;

                worksheet.Cells["A8"].Value = "Exported By:";
                worksheet.Cells["A8:C8"].Merge = true;
                worksheet.Cells["D8"].Value = userName;
                worksheet.Cells["D8:I8"].Merge = true;

                using (ExcelRange range = worksheet.Cells["A7:I8"])
                {
                    range.Style.Font.Size = 11;
                    range.Style.Font.Bold = true;
                }

                // data rows
                if (results.TotalRecords > 0)
                {
                    // too many records
                    if (results.TotalRecords > maxExportRecords)
                    {
                        worksheet.Cells["A10"].Value = string.Format("Incomplete download. {0} rows of {1} rows were downloaded. Only {0} rows can be downloaded.", maxExportRecords.ToString("N0"), results.TotalRecords.ToString("N0"));
                        worksheet.Cells["A10:I10"].Merge = true;
                        using (ExcelRange range = worksheet.Cells["A10:I10"])
                        {
                            range.Style.Font.Italic = true;
                        }
                    }

                    // headers 1
                    int headerIndex = (results.TotalRecords > maxExportRecords) ? 12 : 10;

                    worksheet.Cells[string.Format("A{0}", headerIndex)].Value = "Unmatch Date";
                    worksheet.Cells[string.Format("A{0}", headerIndex)].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    worksheet.Cells[string.Format("A{0}:A{1}", headerIndex, headerIndex + 1)].Merge = true;
                    worksheet.Column(1).Width = 15;

                    worksheet.Cells[string.Format("B{0}", headerIndex)].Value = "HS Visit ID";
                    worksheet.Cells[string.Format("B{0}", headerIndex)].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    worksheet.Cells[string.Format("B{0}:B{1}", headerIndex, headerIndex + 1)].Merge = true;
                    worksheet.Column(2).Width = 18;

                    worksheet.Cells[string.Format("C{0}:D{0}", headerIndex)].Value = "Previous Match";
                    worksheet.Cells[string.Format("C{0}:D{0}", headerIndex)].Merge = true;

                    worksheet.Cells[string.Format("E{0}:F{0}", headerIndex)].Value = "Current Match";
                    worksheet.Cells[string.Format("E{0}:F{0}", headerIndex)].Merge = true;

                    worksheet.Cells[string.Format("G{0}", headerIndex)].Value = "Unmatch Reason";
                    worksheet.Cells[string.Format("G{0}", headerIndex)].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    worksheet.Cells[string.Format("G{0}:G{1}", headerIndex, headerIndex + 1)].Merge = true;
                    worksheet.Column(7).Width = 35;

                    worksheet.Cells[string.Format("H{0}", headerIndex)].Value = "Username";
                    worksheet.Cells[string.Format("H{0}", headerIndex)].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    worksheet.Cells[string.Format("H{0}:H{1}", headerIndex, headerIndex + 1)].Merge = true;
                    worksheet.Column(8).Width = 18;

                    worksheet.Cells[string.Format("I{0}", headerIndex)].Value = "Notes (expand row to see all text)";
                    worksheet.Cells[string.Format("I{0}", headerIndex)].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    worksheet.Cells[string.Format("I{0}:I{1}", headerIndex, headerIndex + 1)].Merge = true;
                    worksheet.Column(9).Width = 120;

                    // headers 2
                    headerIndex = headerIndex + 1;

                    worksheet.Cells[string.Format("C{0}", headerIndex)].Value = client.HsGuarantorPatientIdentifier;
                    worksheet.Cells[string.Format("C{0}", headerIndex)].Style.Border.Top.Style = ExcelBorderStyle.Medium;
                    worksheet.Column(3).Width = 13;

                    worksheet.Cells[string.Format("D{0}", headerIndex)].Value = client.VpGuarantorPatientIdentifier;
                    worksheet.Cells[string.Format("D{0}", headerIndex)].Style.Border.Top.Style = ExcelBorderStyle.Medium;
                    worksheet.Column(4).Width = 13;

                    worksheet.Cells[string.Format("E{0}", headerIndex)].Value = client.HsGuarantorPatientIdentifier;
                    worksheet.Cells[string.Format("E{0}", headerIndex)].Style.Border.Top.Style = ExcelBorderStyle.Medium;
                    worksheet.Column(5).Width = 13;

                    worksheet.Cells[string.Format("F{0}", headerIndex)].Value = client.VpGuarantorPatientIdentifier;
                    worksheet.Cells[string.Format("F{0}", headerIndex)].Style.Border.Top.Style = ExcelBorderStyle.Medium;
                    worksheet.Column(6).Width = 13;

                    // headers border
                    using (ExcelRange range = worksheet.Cells[string.Format("A{0}:I{1}", headerIndex - 1, headerIndex)])
                    {
                        range.Style.Font.Size = 11;
                        range.Style.Font.Bold = true;
                        range.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        range.Style.Border.BorderAround(ExcelBorderStyle.Medium);
                        range.Style.Border.Right.Style = ExcelBorderStyle.Medium;
                    }

                    // data cells
                    int startRowIndex = headerIndex + 1;
                    int rowIndex = startRowIndex;

                    foreach (VisitUnmatchDto visitUnmatch in results.VisitUnmatches)
                    {
                        worksheet.Cells[string.Format("A{0}", rowIndex)].Value = visitUnmatch.UnmatchActionDate.HasValue ? visitUnmatch.UnmatchActionDate.Value.ToString(Format.DateFormat) : "";
                        worksheet.Cells[string.Format("B{0}", rowIndex)].Value = visitUnmatch.MatchedSourceSystemKey;
                        worksheet.Cells[string.Format("C{0}", rowIndex)].Value = visitUnmatch.HsGuarantorSourceSystemKey;
                        worksheet.Cells[string.Format("D{0}", rowIndex)].Value = visitUnmatch.VpGuarantor.VpGuarantorId;
                        worksheet.Cells[string.Format("E{0}", rowIndex)].Value = visitUnmatch.HsGuarantorSourceSystemKeyCurrent;
                        worksheet.Cells[string.Format("F{0}", rowIndex)].Value = visitUnmatch.VpGuarantorCurrent == null ? (int?)null : visitUnmatch.VpGuarantorCurrent.VpGuarantorId;
                        worksheet.Cells[string.Format("G{0}", rowIndex)].Value = visitUnmatch.HsGuarantorUnmatchReasonDisplayName;
                        worksheet.Cells[string.Format("H{0}", rowIndex)].Value = visitUnmatch.UnmatchActionVisitPayUser == null ? "" : visitUnmatch.UnmatchActionVisitPayUser.VisitPayUserId == SystemUsers.SystemUserId ? "System" : visitUnmatch.UnmatchActionVisitPayUser.UserName;
                        worksheet.Cells[string.Format("I{0}", rowIndex)].Value = visitUnmatch.UnmatchActionNotes;
                        worksheet.Cells[string.Format("I{0}", rowIndex)].Style.VerticalAlignment = ExcelVerticalAlignment.Top;
                        worksheet.Cells[string.Format("I{0}", rowIndex)].Style.WrapText = true;
                        worksheet.Row(rowIndex).Height = 80;

                        worksheet.Cells[string.Format("A{0}:I{0}", rowIndex)].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;

                        rowIndex = rowIndex + 1;
                    }

                    // border
                    using (ExcelRange range = worksheet.Cells[string.Format("A{0}:I{1}", startRowIndex, rowIndex - 1)])
                    {
                        range.Style.Border.BorderAround(ExcelBorderStyle.Thin);
                        range.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                        range.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                    }
                }
                else
                {
                    worksheet.Cells["A10"].Value = "There is no data matching your search criteria.";
                    worksheet.Cells["A10:I10"].Merge = true;
                    using (ExcelRange range = worksheet.Cells["A10:I10"])
                    {
                        range.Style.Font.Size = 11;
                        range.Style.Font.Bold = true;
                    }
                }

                System.Drawing.Image logo = await this._imageService.Value.GetExportLogoAsync();
                ExcelPicture pic = worksheet.Drawings.AddPicture("logo", logo);
                pic.SetPosition(0, 0);
                pic.SetSize(logo.Width, logo.Height);
                using (ExcelRange range = worksheet.Cells["A1:I4"])
                {
                    range.Merge = true;
                }
                return package.GetAsByteArray();
            }
        }

        #endregion
    }
}
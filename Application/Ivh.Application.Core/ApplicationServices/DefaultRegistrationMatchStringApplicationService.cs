﻿namespace Ivh.Application.Core.ApplicationServices
{
    using System.Collections.Generic;
    using Common.Dtos;
    using Common.Interfaces;

    public class DefaultRegistrationMatchStringApplicationService : IRegistrationMatchStringApplicationService
    {
        public IReadOnlyList<HsGuarantorDto> TransformRegistrationMatchString(IEnumerable<HsGuarantorDto> hsGuarantors)
        {
            // If a client requires additional, specific logic in order to manipulate the registration match string 
            // add a new concrete implementation of IRegistrationMatchStringApplicationService
            return (IReadOnlyList<HsGuarantorDto>) hsGuarantors;
        }
    }
}
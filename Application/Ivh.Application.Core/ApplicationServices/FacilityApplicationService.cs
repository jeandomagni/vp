﻿namespace Ivh.Application.Core.ApplicationServices
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using AutoMapper;
    using Base.Common.Interfaces;
    using Base.Services;
    using Common.Dtos;
    using Common.Interfaces;
    using Domain.Visit.Entities;
    using Domain.Visit.Interfaces;

    public class FacilityApplicationService : ApplicationService, IFacilityApplicationService
    {
        private readonly Lazy<IFacilityService> _facilityService;

        public FacilityApplicationService(
            Lazy<IApplicationServiceCommonService> applicationServiceCommonService,
            Lazy<IFacilityService> facilityService) : base(applicationServiceCommonService)
        {
            this._facilityService = facilityService;
        }

        public IReadOnlyList<FacilityDto> GetAllFacilities()
        {
            IReadOnlyList<Facility> facilities = this._facilityService.Value.GetAllFacilities();
            List<FacilityDto> facilityDtos = Mapper.Map<List<FacilityDto>>(facilities);

            return facilityDtos;
        }

        public IReadOnlyList<FacilityDto> GetFacilitiesByStateCode(string stateCode)
        {
            IReadOnlyList<Facility> facilities = this._facilityService.Value.GetFacilitiesByStateCode(stateCode);
            List<FacilityDto> facilityDtos = Mapper.Map<List<FacilityDto>>(facilities);

            return facilityDtos;
        }
    }
}
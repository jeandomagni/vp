﻿namespace Ivh.Application.Core.ApplicationServices
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using AutoMapper;
    using Base.Common.Interfaces;
    using Base.Services;
    using Common.Interfaces;
    using Domain.Core.SystemMessage.Entities;
    using Domain.Core.SystemMessage.Interfaces;
    using Domain.User.Interfaces;
    using Domain.FinanceManagement.FinancePlan.Interfaces;
    using Domain.Guarantor.Entities;
    using Domain.Guarantor.Interfaces;
    using Domain.Visit.Entities;
    using Domain.Visit.Interfaces;
    using Domain.Visit.Unmatching.Interfaces;
    using Ivh.Common.Base.Utilities.Extensions;
    using Ivh.Common.Base.Utilities.Helpers;
    using Ivh.Common.Data;
    using NHibernate;
    using Ivh.Common.Data.Connection.Connections;
    using Ivh.Common.Data.Interfaces;
    using Ivh.Common.EventJournal;
    using Ivh.Common.VisitPay.Enums;
    using Ivh.Common.VisitPay.Messages.Core;
    using Ivh.Common.VisitPay.Messages.FinanceManagement;
    using Ivh.Common.VisitPay.Messages.HospitalData;
    using Ivh.Common.VisitPay.Messages.HospitalData.Dtos;

    public class VisitCreationApplicationService : ApplicationService, IVisitCreationApplicationService
    {
        private readonly Lazy<ISystemMessageVisitPayUserService> _systemMessageVisitPayUserService;
        private readonly Lazy<IFinancePlanService> _financePlanService;
        private readonly Lazy<IGuarantorService> _guarantorService;
        private readonly ISession _session;
        private readonly Lazy<IVisitService> _visitService;
        private readonly Lazy<IVisitStateService> _visitStateService;
        private readonly Lazy<IVisitTransactionWriteService> _visitTransactionService;
        private readonly Lazy<IInsuranceService> _insuranceService;
        private readonly Lazy<IFacilityService> _facilityService;
        private readonly Lazy<IVisitPayUserJournalEventService> _userJournalEventService;
        private readonly Lazy<IUnmatchingService> _unmatchingService;

        private const string UnmatchedGuarantorIdString = "Unmatched";
        private const string LockPrefix = "MergeOrCreateVisits";

        public VisitCreationApplicationService(
            Lazy<IApplicationServiceCommonService> applicationServiceCommonService,
            ISessionContext<VisitPay> sessionContext,
            Lazy<IVisitService> visitService,
            Lazy<IGuarantorService> guarantorService,
            Lazy<IFinancePlanService> financePlanService,
            Lazy<IVisitTransactionWriteService> visitTransactionService,
            Lazy<IVisitStateService> visitStateService,
            Lazy<IInsuranceService> insuranceService,
            Lazy<IFacilityService> facilityService,
            Lazy<IVisitPayUserJournalEventService> userJournalEventService,
            Lazy<IUnmatchingService> unmatchingService,
            Lazy<ISystemMessageVisitPayUserService> systemMessageVisitPayUserService) : base(applicationServiceCommonService)
        {
            this._session = sessionContext.Session;
            this._visitService = visitService;
            this._guarantorService = guarantorService;
            this._financePlanService = financePlanService;
            this._visitTransactionService = visitTransactionService;
            this._visitStateService = visitStateService;
            this._insuranceService = insuranceService;
            this._facilityService = facilityService;
            this._userJournalEventService = userJournalEventService;
            this._unmatchingService = unmatchingService;
            this._systemMessageVisitPayUserService = systemMessageVisitPayUserService;
        }

        public IList<int> MergeOrCreateVisitsFromVisitChangeSet(ChangeSetMessage changeSetMessage)
        {
            List<int> visitsChanged = new List<int>();
            if (changeSetMessage != null)
            {
                bool isUnmatchedGuarantor = !changeSetMessage.VpGuarantorHsMatches.Any();
                string guarantorIdString = (
                    isUnmatchedGuarantor
                    ? UnmatchedGuarantorIdString
                    : changeSetMessage.VpGuarantorHsMatches.Select(s => s.VpGuarantorId.ToString()).Aggregate((agg, item) => agg + "|" + item)
                    );

                string visitLockString = $"{LockPrefix}{guarantorIdString}";

                try
                {
                    if (!this.Cache.Value.GetLock(visitLockString))
                    {
                        this.LoggingService.Value.Fatal(() => $"{nameof(VisitCreationApplicationService)}::{nameof(this.MergeOrCreateVisitsFromVisitChangeSet)} failed to acquire distributed lock for VPG: {guarantorIdString}");
                        throw new Exception("Failed to acquire lock.  Trying again...");
                    }

                    // Using try...catch...finally pattern so unitOfWork.Dispose() can successfully rollback transaction if it needs to
                    Exception workException = null;
                    UnitOfWork unitOfWork = new UnitOfWork(this._session);
                    try
                    {
                        IList<int> vpGuarantorIds = changeSetMessage.VpGuarantorHsMatches.Select(x => x.VpGuarantorId).ToList();
                        IList<Guarantor> vpGuarantors = this._guarantorService.Value.GetGuarantorsFromIntList(vpGuarantorIds);
                        vpGuarantors = this.MergeVpGuarantorAndHsGuarantorLinks(vpGuarantors, changeSetMessage.HsGuarantors, changeSetMessage.VpGuarantorHsMatches).ToList();
                        IList<HsGuarantorMap> hsGuarantors = vpGuarantors.SelectMany(x => x.MatchedHsGuarantorMaps).ToList();

                        IList<Visit> visits = this.MergeVisitChangesAndTransactionChanges(
                            changeSetMessage.VisitChanges,
                            changeSetMessage.VisitTransactionChanges,
                            vpGuarantors,
                            hsGuarantors,
                            changeSetMessage.VpGuarantorHsMatches);

                        visitsChanged.AddRange(visits.Select(x => x.VisitId));
                        foreach (Visit visit in visits)
                        {
                            bool isOnFinancePlan = this._financePlanService.Value.IsVisitInAFinancePlan(visit);
                            bool wroteOffInterest = this._financePlanService.Value.WriteOffInterestOnlyBalance(visit.VisitId);

                            if (wroteOffInterest)
                            {
                                this._visitStateService.Value.EvaluateVisitState(visit);
                                this._visitService.Value.SaveVisit(visit);

                                if (isOnFinancePlan)
                                {
                                    this._financePlanService.Value.UpdateFinancePlanDueToVisitChange(visit);
                                }
                            }
                        }

                        unitOfWork.Commit();
                    }
                    catch (Exception e)
                    {
                        // Capture/handle exception so the Session can remain stable
                        // Will throw exception once the transaction has been rolled back
                        workException = e;
                    }
                    finally
                    {
                        unitOfWork.Dispose();
                    }

                    if (workException != null)
                    {
                        throw workException;
                    }

                }
                catch (Exception ex)
                {
                    this.LoggingService.Value.Fatal(() => $"{nameof(VisitCreationApplicationService)}::{nameof(this.MergeOrCreateVisitsFromVisitChangeSet)} Exception: {ExceptionHelper.AggregateExceptionToString(ex)}");
                    throw;
                }
                finally
                {
                    this.Cache.Value.ReleaseLock(visitLockString);
                }
            }

            return visitsChanged;
        }

        private IEnumerable<Guarantor> MergeVpGuarantorAndHsGuarantorLinks(
            IList<Guarantor> guarantors,
            IList<HsGuarantorDto> hsGuarantors,
            IList<VpGuarantorHsMatchDto> vpGuarantorHsMatches)
        {
            IList<Guarantor> returnValues = new List<Guarantor>();
            if (guarantors.IsNotNullOrEmpty())
            {
                // give preference to guarantors already matched
                foreach (VpGuarantorHsMatchDto vpGuarantorHsMatchDto in vpGuarantorHsMatches.OrderBy(x => x.HsGuarantorMatchStatus))
                {
                    Guarantor guarantor1 = guarantors.FirstOrDefault(x => x.VpGuarantorId == vpGuarantorHsMatchDto.VpGuarantorId);
                    if (guarantor1 == null)
                    {
                        continue;
                    }

                    List<HsGuarantorDto> matchGroup = hsGuarantors.Where(x => x.HsGuarantorId == vpGuarantorHsMatchDto.HsGuarantorId).ToList();

                    foreach (HsGuarantorDto match in matchGroup)
                    {
                        IList<HsGuarantorMap> potentialMatches = guarantor1.HsGuarantorMaps.Where(x =>
                            x.HsGuarantorId == match.HsGuarantorId
                            && x.BillingSystem.BillingSystemId == match.HsBillingSystemId
                            && x.SourceSystemKey == match.SourceSystemKey).ToList();

                        //do not match if expressly prohibited
                        if (potentialMatches.Any(x => x.HsGuarantorMatchStatus.IsInCategory(HsGuarantorMatchStatusEnumCategory.Prohibited)))
                        {
                            continue;
                        }

                        //rematch when there are no other existing matches 
                        if (potentialMatches.All(x => x.HsGuarantorMatchStatus != HsGuarantorMatchStatusEnum.Matched))
                        {
                            List<HsGuarantorMap> existingHsGuarantorMaps = guarantors.SelectMany(x => x.HsGuarantorMaps).ToList();

                            foreach (HsGuarantorMap unsoftMatch in potentialMatches
                                .Where(x => x.HsGuarantorMatchStatus == HsGuarantorMatchStatusEnum.UnmatchedSoft))
                            {
                                // do not update an HsGuarantorMap record when a match has already been assigned
                                if (existingHsGuarantorMaps.Any(s => s.HsGuarantorMatchStatus.IsInCategory(HsGuarantorMatchStatusEnumCategory.Matched)
                                                                     && s.HsGuarantorId == unsoftMatch.HsGuarantorId))
                                {
                                    continue;
                                }

                                // We rematch on any statuses that allow for it.
                                if (vpGuarantorHsMatchDto.HsGuarantorMatchStatus.IsInCategory(HsGuarantorMatchStatusEnumCategory.Allowed)
                                                                  && unsoftMatch.HsGuarantorId == vpGuarantorHsMatchDto.HsGuarantorId
                                                                  && unsoftMatch.VpGuarantor.VpGuarantorId == vpGuarantorHsMatchDto.VpGuarantorId)
                                {
                                    // if this was a soft unmatch, publish message so the visits can be rematched too.
                                    if (unsoftMatch.HsGuarantorMatchStatus == HsGuarantorMatchStatusEnum.UnmatchedSoft)
                                    {
                                        HsGuarantorRematchMessage message = new HsGuarantorRematchMessage
                                        {
                                            HsGuarantorId = unsoftMatch.HsGuarantorId,
                                            VpGuarantorId = unsoftMatch.VpGuarantor.VpGuarantorId
                                        };
                                        this._session.Transaction.RegisterPostCommitSuccessAction(() => this.Bus.Value.PublishMessage(message).Wait());
                                    }
                                    unsoftMatch.HsGuarantorMatchStatus = HsGuarantorMatchStatusEnum.Matched;
                                }
                            }
                        }

                        if (!guarantor1.MatchedHsGuarantorMaps.Any(x =>
                            x.HsGuarantorId == match.HsGuarantorId
                            && x.BillingSystem.BillingSystemId == match.HsBillingSystemId
                            && x.SourceSystemKey == match.SourceSystemKey))
                        {
                            //Same as above.  Seems like we should only add a match if it's in the vpGuarantorHsMatches
                            if (vpGuarantorHsMatches.Any(y => y.HsGuarantorMatchStatus.IsInCategory(HsGuarantorMatchStatusEnumCategory.Matched)
                                                              && match.HsGuarantorId == y.HsGuarantorId
                                                              && vpGuarantorHsMatchDto.VpGuarantorId == y.VpGuarantorId))
                            {
                                guarantor1.HsGuarantorMaps.Add(new HsGuarantorMap
                                {
                                    VpGuarantor = guarantor1,
                                    BillingSystem = new BillingSystem { BillingSystemId = match.HsBillingSystemId },
                                    SourceSystemKey = match.SourceSystemKey,
                                    HsGuarantorId = match.HsGuarantorId,
                                    HsGuarantorMatchStatus = vpGuarantorHsMatchDto.HsGuarantorMatchStatus,
                                    SSN4 = match.SSN4,
                                    VpEligible = match.VpEligible,
                                    GenderId = match.GenderId,
                                    MatchOption = vpGuarantorHsMatchDto.MatchOption
                                });

                                // log match
                                JournalEventUserContext journalEventUserContext = JournalEventUserContext.BuildSystemUserContext(null);
                                this._userJournalEventService.Value.LogGuarantorMatch(journalEventUserContext, match.HsGuarantorId, guarantor1.VpGuarantorId, "Recurring");
                            }
                        }

                        HsGuarantorMap hsGuarantorMap = guarantor1.HsGuarantorMaps.FirstOrDefault(x => x.HsGuarantorId == match.HsGuarantorId);
                        // update HsGuarantorMap fields
                        if (hsGuarantorMap != null)
                        {
                            hsGuarantorMap.VpEligible = match.VpEligible;
                            hsGuarantorMap.GenderId = match.GenderId;

                            // invalidate sso
                            if (hsGuarantorMap.SSN4 != match.SSN4)
                            {
                                hsGuarantorMap.SSN4 = match.SSN4;
                                this._session.RegisterPostCommitSuccessAction((vpuid, vpgid, hsgid) =>
                                {
                                    this.Bus.Value.PublishMessage(new SsoInvalidateMessage
                                    {
                                        VisitPayUserId = vpuid,
                                        VpGuarantorId = vpgid,
                                        HsGuarantorId = hsgid
                                    }).Wait();
                                }, hsGuarantorMap.VpGuarantor.User.VisitPayUserId, hsGuarantorMap.VpGuarantor.VpGuarantorId, hsGuarantorMap.HsGuarantorId);
                            }
                        }

                        this._guarantorService.Value.InsertOrUpdateGuarantor(guarantor1);
                    }
                    this._guarantorService.Value.SetToActiveVisitsLoaded(guarantor1);
                    returnValues.Add(guarantor1);
                }
            }
            return returnValues
                .SelectMany(x => x.HsGuarantorMaps)
                .Join(hsGuarantors, o => o.HsGuarantorId, i => i.HsGuarantorId, (o, i) => new { o.HsGuarantorMatchStatus, o.VpGuarantor })
                .Where(x => x.HsGuarantorMatchStatus.IsInCategory(HsGuarantorMatchStatusEnumCategory.Allowed))
                .Select(x => x.VpGuarantor).Distinct().ToList();
        }

        private IList<Visit> MergeVisitChangesAndTransactionChanges(
            IList<VisitChangeDto> visitChanges,
            IList<VisitTransactionChangeDto> visitTransactionChanges,
            IList<Guarantor> vpGuarantors,
            IList<HsGuarantorMap> hsGuarantorMaps,
            IList<VpGuarantorHsMatchDto> vpGuarantorHsMatchs)
        {
            HashSet<int> guarantorsWithoutVisits = new HashSet<int>();
            foreach (int vpGuarantorId in vpGuarantors.Select(x => x.VpGuarantorId).Distinct())
            {
                if (this._visitService.Value.HasVisits(vpGuarantorId, new[] { VisitStateEnum.Inactive, VisitStateEnum.Active }))
                {
                    guarantorsWithoutVisits.Add(vpGuarantorId);
                }
            }

            //check to see if this visit should be unmatched. Can match to ANY Allowed (Matched & UnmathcedSoft)
            bool HasEligibleHsId(int h, Visit v)
            {
                return v.VPGuarantor.HsGuarantorMaps.Any(x => x.HsGuarantorMatchStatus.IsInCategory(HsGuarantorMatchStatusEnumCategory.Allowed) && x.HsGuarantorId == h && x.VpEligible);
            }

            // save off VP Visits correlated to the ETL visit id from change set
            Dictionary<int, Visit> visits = new Dictionary<int, Visit>();
            HashSet<int> newVisitsLoadedForVpgids = new HashSet<int>();
            HashSet<int> newActiveVisitsLoadedForVpgids = new HashSet<int>();
            DateTime now = DateTime.UtcNow;
            if (visitChanges.IsNotNullOrEmpty())
            {
                foreach (VisitChangeDto visitChangeDto in visitChanges)
                {
                    if (!visitChangeDto.HasSystemSourceKey)
                    {
                        continue;
                    }

                    //Merge into the already merged visit, if possible.
                    Visit visit = visits.Values.FirstOrDefault(x => x.SourceSystemKey == visitChangeDto.SourceSystemKey
                                                                    && x.BillingSystem.BillingSystemId == visitChangeDto.BillingSystemId);
                    if (visit == null)
                    {
                        visit = this._visitService.Value.GetVisitBySystemSourceKey(visitChangeDto.SourceSystemKey, visitChangeDto.BillingSystemId);

                        if (visit != null)
                        {
                            bool hasVisitGuarantorMap = (visit.HsGuarantorMap != null);
                            bool hasVisitGuarantorChangeSetMismatch = (!HasEligibleHsId(visitChangeDto.HsGuarantorId, visit));
                            if (hasVisitGuarantorMap && hasVisitGuarantorChangeSetMismatch)
                            {
                                // unmatch this visit for the previous guarantor
                                this._unmatchingService.Value.UnmatchVisit(visit.HsGuarantorMap, visit, HsGuarantorMatchStatusEnum.UnmatchedSoft, HsGuarantorUnmatchReasonEnum.SystemUnmatchedHSGIDUpdated);
                                visitTransactionChanges = visitTransactionChanges.Where(x => x.VisitId != visitChangeDto.VisitId).ToList();
                                visit = null;
                            }
                        }
                    }

                    VpGuarantorHsMatchDto vpGuarantorHsMatch = vpGuarantorHsMatchs
                        .FirstOrDefault(x => x.HsGuarantorId == visitChangeDto.HsGuarantorId
                                             && x.HsGuarantorMatchStatus.IsInCategory(HsGuarantorMatchStatusEnumCategory.Matched));
                    if (vpGuarantorHsMatch == null)
                    {
                        continue;
                    }

                    Guarantor vpGuarantor = vpGuarantors.FirstOrDefault(x => x.VpGuarantorId == vpGuarantorHsMatch.VpGuarantorId);
                    if (vpGuarantor == null)
                    {
                        continue;
                    }

                    if (visit == null)
                    {
                        visit = this._visitService.Value.GetVisitBySystemSourceKeyUnmatched(vpGuarantor.VpGuarantorId, visitChangeDto.SourceSystemKey, visitChangeDto.BillingSystemId);
                        if (visit != null && visit.HsGuarantorMatchStatus.IsInCategory(HsGuarantorMatchStatusEnumCategory.Prohibited))
                        {
                            // remove transactions for visits that will not be considered (i.e., removed)
                            visitTransactionChanges = visitTransactionChanges.Where(x => x.VisitId != visitChangeDto.VisitId).ToList();
                            continue;
                        }
                        visit = null;
                    }

                    HsGuarantorMap hsGuarantorMap = hsGuarantorMaps.FirstOrDefault(x =>
                    {
                        return x.HsGuarantorId == vpGuarantorHsMatch.HsGuarantorId
                               && x.HsGuarantorMatchStatus.IsInCategory(HsGuarantorMatchStatusEnumCategory.Allowed)
                               && x.VpEligible;
                    });
                    if (hsGuarantorMap == null)
                    {
                        continue;
                    }

                    if (visit != null)
                    {
                        // visit already in VisitPay database so just need to update the current record
                        visits.Add(visitChangeDto.VisitId, this.MergeVisitAndVisitChangeDto(visitChangeDto, vpGuarantor, hsGuarantorMap, visit));
                    }
                    else if (vpGuarantorHsMatchs.IsNotNullOrEmpty())
                    {
                        // doesn't exist in VisitPay database so check if guarantor exists in VisitPay db, if so create new visit
                        visit = this.CreateVisitFromVisitChangeDto(visitChangeDto, vpGuarantor, hsGuarantorMap, now);

                        this._visitStateService.Value.EvaluateVisitState(visit);
                        this._visitService.Value.SaveVisit(visit);
                        visit.InitializeActiveOutboundMessage();

                        this._session.RegisterPostCommitSuccessAction(v =>
                        {
                            this.Bus.Value.PublishMessage(new OutboundVisitMessage
                            {
                                VpVisitId = v.VisitId,
                                BillingSystemId = v.BillingSystemId.Value,
                                SourceSystemKey = v.SourceSystemKey,
                                VpOutboundVisitMessageType = VpOutboundVisitMessageTypeEnum.VpReceivedVisit,
                                ActionDate = DateTime.UtcNow,
                                IsCallCenter = vpGuarantor.IsOfflineGuarantor(),
                                IsAutoPay = vpGuarantor.UseAutoPay
                            }).Wait();
                        }, visit);

                        visits.Add(visitChangeDto.VisitId, visit);
                        
                        newVisitsLoadedForVpgids.Add(vpGuarantor.VpGuarantorId);
                        
                        if (visit.CurrentVisitState == VisitStateEnum.Active)
                        {
                            newActiveVisitsLoadedForVpgids.Add(vpGuarantor.VpGuarantorId);
                        }

                        JournalEventUserContext journalEventUserContext = JournalEventUserContext.BuildSystemUserContext(null);
                        this._userJournalEventService.Value.LogVisitMatch(journalEventUserContext, hsGuarantorMap.HsGuarantorId, vpGuarantor.VpGuarantorId, visit.SourceSystemKey, visit.VisitId);
                    }
                }
            }

            // send first visit loaded message
            if (this.FeatureService.Value.IsFeatureEnabled(VisitPayFeatureEnum.FeatureMatchingApiIsEnabled))
            {
                foreach (int vpGuarantorId in newVisitsLoadedForVpgids)
                {
                    if (!guarantorsWithoutVisits.Contains(vpGuarantorId))
                    {
                        continue;
                    }

                    Guarantor guarantorWithoutVisits = vpGuarantors.First(x => x.VpGuarantorId == vpGuarantorId);
                    IList<int> hsGuarantorIds = guarantorWithoutVisits.MatchedHsGuarantorMaps.Select(x => x.HsGuarantorId).ToList();
                    this._session.Transaction.RegisterPostCommitSuccessAction((vpgid, hsgids) =>
                    {
                        this.Bus.Value.PublishMessage(new FirstVisitLoadedMessage
                        {
                            VpGuarantorId = vpgid,
                            HsGuarantorIds = hsgids
                        }).Wait();
                    }, vpGuarantorId, hsGuarantorIds);
                }
            }

            // only send new visit loaded for active visits
            foreach (int vpGuarantorId in newActiveVisitsLoadedForVpgids.Distinct())
            {
                this._session.Transaction.RegisterPostCommitSuccessAction(vpgid =>
                {
                    this.Bus.Value.PublishMessage(new NewVisitLoadedMessage
                    {
                        VpGuarantorId = vpgid
                    }).Wait();

                    // Spike VP-2609 Text to Pay (commented out for safety until it becomes a real feature)
                    // Consumed by TextToPayApplicationService
                    //if (this.FeatureService.Value.IsFeatureEnabled(VisitPayFeatureEnum.FeatureTextToPayIsEnabled))
                    //{
                    //    this.Bus.Value.PublishMessage(new TextToPayPaymentOptionMessage
                    //    {
                    //        VpGuarantorId = vpgid
                    //    }).Wait();
                    //}

                }, vpGuarantorId);
            }

            if (visitTransactionChanges.IsNotNullOrEmpty())
            {
                foreach (IGrouping<int, VisitTransactionChangeDto> grouping in visitTransactionChanges.GroupBy(x => x.BillingSystemId).ToList())
                {
                    int billingSystemId = grouping.Key;
                    List<string> sourceSystemKeys = grouping.Select(x => x.SourceSystemKey).ToList();

                    IList<VisitTransaction> visitTransactions = this._visitTransactionService.Value.GetBySourceSystemKey(sourceSystemKeys, billingSystemId);
                    IList<VisitTransaction> unmatchedVisitTransactions = this._visitTransactionService.Value.GetBySourceSystemKeyUnmatch(sourceSystemKeys, billingSystemId);

                    foreach (VisitTransactionChangeDto visitTransactionChangeDto in grouping.ToList())
                    {
                        VisitTransaction visitTransaction = visitTransactions.FirstOrDefault(x => x.SourceSystemKey == visitTransactionChangeDto.SourceSystemKey && x.BillingSystem.BillingSystemId == visitTransactionChangeDto.BillingSystemId);

                        // visit transaction found
                        if (visitTransaction != null)
                        {
                            this.MergeVisitTransactionAndVisitTransactionChangeDto(visitTransaction, visitTransactionChangeDto);
                            continue;
                        }

                        VisitTransaction unmatchedTransaction = unmatchedVisitTransactions.FirstOrDefault(x => x.MatchedSourceSystemKey == visitTransactionChangeDto.SourceSystemKey && x.MatchedBillingSystem.BillingSystemId == visitTransactionChangeDto.BillingSystemId);

                        // found unmatched visit transaction
                        if (unmatchedTransaction != null)
                        {
                            this.MergeVisitTransactionAndVisitTransactionChangeDto(unmatchedTransaction, visitTransactionChangeDto);
                            continue;
                        }

                        // visit transaction not found, create
                        this.CreateVisitTransactionFromVisitTransactionChangeDto(visitTransactionChangeDto, visits.Values.ToList(), visitChanges);
                    }
                }
            }

            //Save updated info.
            foreach (Visit visit in visits.Values)
            {
                this._visitStateService.Value.EvaluateVisitState(visit);
                this._visitService.Value.Insert(visit);
            }

            return visits.Values.ToList();
        }
        
        private void MergeVisitTransactionAndVisitTransactionChangeDto(VisitTransaction visitTransaction, VisitTransactionChangeDto visitTransactionChangeDto)
        {
            visitTransaction.SetSourceSystemKey(visitTransactionChangeDto.SourceSystemKey);
            visitTransaction.BillingSystem = new BillingSystem { BillingSystemId = visitTransactionChangeDto.BillingSystemId };
            visitTransaction.TransactionDate = visitTransactionChangeDto.TransactionDate;

            if (visitTransaction.VisitTransactionAmount.TransactionAmount != visitTransactionChangeDto.TransactionAmount)
            {
                visitTransaction.SetTransactionAmount(visitTransactionChangeDto.TransactionAmount);
            }

            visitTransaction.PostDate = visitTransactionChangeDto.PostDate;
            visitTransaction.TransactionCodeSourceSystemKey = visitTransactionChangeDto.TransactionCodeSourceSystemKey;
            visitTransaction.TransactionCodeBillingSystemId = visitTransactionChangeDto.TransactionCodeBillingSystemId;
            visitTransaction.PaymentAllocationId = visitTransactionChangeDto.PaymentAllocationId;

            VpTransactionType changedTransactionType =  this._visitTransactionService.Value.GetTransactionTypes().FirstOrDefault(x => x.VpTransactionTypeId == visitTransactionChangeDto.VpTransactionTypeId);
            if (changedTransactionType == null)
            {
                // do not update the VpTransactionType if visitTransactionChangeDto.VpTransactionTypeId is not found
                this.LoggingService.Value.Warn(() => $"{nameof(VisitCreationApplicationService)}.{nameof(this.MergeVisitTransactionAndVisitTransactionChangeDto)} -Not updating VpTransactionType for VisitTransactionId = {visitTransaction.VisitTransactionId}. Could not find VisitTransactionType with id:{visitTransactionChangeDto.VpTransactionTypeId}");
                return;
            }

            visitTransaction.VpTransactionType = changedTransactionType;

            if (!visitTransaction.VpTransactionType.IsHs())
            {
                visitTransaction.TransactionDescription = null;
                this._visitTransactionService.Value.SetTransactionDescription(visitTransaction);
            }
            else
            {
                visitTransaction.TransactionDescription = visitTransactionChangeDto.TransactionDescription;
            }
        }

        private VisitTransaction CreateVisitTransactionFromVisitTransactionChangeDto(
            VisitTransactionChangeDto visitTransactionChangeDto,
            IEnumerable<Visit> visits,
            IEnumerable<VisitChangeDto> visitChanges,
            bool addAmount = true)
        {
            VisitChangeDto foundVisitChange = visitChanges.FirstOrDefault(x => x.VisitId == visitTransactionChangeDto.VisitId);
            if (foundVisitChange == null)
            {
                return null;
            }

            Visit foundVisit = visits.FirstOrDefault(x => x.SourceSystemKey == foundVisitChange.SourceSystemKey);
            if (foundVisit == null)
            {
                return null;
            }

            VpTransactionType vpTransactionType = this._visitTransactionService.Value.GetTransactionTypes().FirstOrDefault(x => x.VpTransactionTypeId == visitTransactionChangeDto.VpTransactionTypeId);
            if (vpTransactionType == null)
            {
                // do not add a visit transaction with an unknown transaction type
                this.LoggingService.Value.Warn(() =>  $"{nameof(VisitCreationApplicationService)}.{nameof(this.CreateVisitTransactionFromVisitTransactionChangeDto)} - unknown transaction type ({visitTransactionChangeDto.VpTransactionTypeId}) for visit transaction = {visitTransactionChangeDto.SourceSystemKey}");
                return null;
            }

            VisitTransaction visitTransaction = new VisitTransaction
            {
                PostDate = visitTransactionChangeDto.PostDate,
                TransactionDate = visitTransactionChangeDto.TransactionDate,
                VpGuarantorId = foundVisit.VPGuarantor.VpGuarantorId,
                Visit = foundVisit,
                BillingSystem = visitTransactionChangeDto.SourceSystemKey == null ? null : new BillingSystem {BillingSystemId = visitTransactionChangeDto.BillingSystemId},
                VpTransactionType = this._visitTransactionService.Value.GetTransactionTypes().FirstOrDefault(x => x.VpTransactionTypeId == visitTransactionChangeDto.VpTransactionTypeId),
                TransactionCodeSourceSystemKey = visitTransactionChangeDto.TransactionCodeSourceSystemKey,
                TransactionCodeBillingSystemId = visitTransactionChangeDto.TransactionCodeBillingSystemId,
                PaymentAllocationId = visitTransactionChangeDto.PaymentAllocationId
            };
            visitTransaction.SetSourceSystemKey(visitTransactionChangeDto.SourceSystemKey);
            
            if (!visitTransaction.VpTransactionType.IsHs())
            {
                this._visitTransactionService.Value.SetTransactionDescription(visitTransaction);
            }
            else
            {
                visitTransaction.TransactionDescription = visitTransactionChangeDto.TransactionDescription;
            }

            if (addAmount)
            {
                visitTransaction.SetTransactionAmount(visitTransactionChangeDto.TransactionAmount);
            }

            this._visitTransactionService.Value.SaveVisitTransaction(foundVisit, visitTransaction);

            // If the transaction is considered a payment, pass the transaction information to the financePlanService
            if (this.FeatureService.Value.IsFeatureEnabled(VisitPayFeatureEnum.FeatureExternalFinancePlanPaymentEnabled))
            {
                bool isAnExternalPaymentType = visitTransactionChangeDto.ReassessmentExempt;
                if (isAnExternalPaymentType)
                {
                    this._financePlanService.Value.AddExternalPrincipalAmountDue(
                        visitTransaction.VisitTransactionAmount.TransactionAmount,
                        visitTransaction.Visit.VisitId,
                        visitTransaction.VisitTransactionId);
                }
            }

            this.PublishPaymentSummaryMessageForUiNotifications(visitTransaction.VpTransactionType.VpTransactionTypeEnum, visitTransaction.VpGuarantorId);

            return visitTransaction;
        }

        private Visit CreateVisitFromVisitChangeDto(VisitChangeDto visitChangeDto, Guarantor vpGuarantor, HsGuarantorMap hsGuarantorMap, DateTime now)
        {
            Visit visit = new Visit
            {
                HsGuarantorMatchStatus = HsGuarantorMatchStatusEnum.Matched
            };

            return this.MergeVisitAndVisitChangeDto(visitChangeDto, vpGuarantor, hsGuarantorMap, visit);
        }

        private Visit MergeVisitAndVisitChangeDto(VisitChangeDto visitChangeDto, Guarantor vpGuarantor, HsGuarantorMap hsGuarantorMap, Visit visit)
        {
            if (visitChangeDto == null || visit == null)
            {
                return null;
            }

            visit.SetCurrentBalance(visitChangeDto.HsCurrentBalance);

            visit.AdmitDate = visitChangeDto.AdmitDate ?? visit.AdmitDate;
            visit.DischargeDate = visitChangeDto.DischargeDate ?? visit.DischargeDate;
            visit.BillingApplication = visitChangeDto.BillingApplication;
            visit.BillingHold = visitChangeDto.BillingHold;
            visit.IsPatientGuarantor = visitChangeDto.IsPatientGuarantor;
            visit.IsPatientMinor = visitChangeDto.IsPatientMinor;
            visit.PatientFirstName = visitChangeDto.PatientFirstName;
            visit.PatientLastName = visitChangeDto.PatientLastName;
            visit.PrimaryInsuranceTypeId = visitChangeDto.PrimaryInsuranceType?.PrimaryInsuranceTypeId ?? visit.PrimaryInsuranceTypeId;
            visit.Redact = visitChangeDto.Redact;
            visit.ServiceGroupId = visitChangeDto.ServiceGroupId ?? visit.ServiceGroupId;
            visit.SourceSystemKey = visit.SourceSystemKey ?? visitChangeDto.SourceSystemKey;
            visit.SourceSystemKeyDisplay = visitChangeDto.SourceSystemKeyDisplay;
            visit.VisitDescription = visitChangeDto.VisitDescription ?? string.Empty;
            visit.VpEligible = visitChangeDto.VpEligible;
            visit.PaymentPriority = visitChangeDto.PaymentPriority ?? visit.PaymentPriority;

            visit.HsGuarantorMap = hsGuarantorMap ?? visit.HsGuarantorMap;
            visit.VPGuarantor = vpGuarantor ?? visit.VPGuarantor;

            if (visit.BillingSystem == null || visit.BillingSystemId < 0)
            {
                visit.BillingSystem = new BillingSystem { BillingSystemId = visitChangeDto.BillingSystemId };
            }

            if (visitChangeDto.Facility?.FacilityId != visit.Facility?.FacilityId)
            {
                if (visitChangeDto.Facility?.FacilityId.HasValue ?? false)
                {
                    Facility facility = this._facilityService.Value.GetFacility(visitChangeDto.Facility.FacilityId.Value);
                    visit.Facility = facility;
                }
                else
                {
                    visit.Facility = null;
                }
            }

            this.SetInterestFlags(visitChangeDto, visit);
            this.MergeInsurancePlans(visitChangeDto, visit, vpGuarantor);
            this.AgeVisit(visitChangeDto, visit);

            return visit;
        }

        private void AgeVisit(VisitChangeDto visitChangeDto, Visit visit)
        {
            if (this.FeatureService.Value.IsFeatureEnabled(VisitPayFeatureEnum.FeatureVisitPayAgingIsEnabled))
            {
                return;
            }

            AgingTierEnum agingTier = visitChangeDto.AgingTier;
            visit.SetAgingTier(agingTier, string.Empty);

            if (visit.AgingTierIncreasedToPastDue)
            {
                this._session.RegisterPostCommitSuccessAction(vpgid =>
                {
                    this.Bus.Value.PublishMessage(new SendPastDueVisitsEmailMessage
                    {
                        VpGuarantorId = vpgid,
                        VisitIds = new List<int>() { visit.VisitId}
                    }).Wait();
                }, visit.VPGuarantor.VpGuarantorId);
            }
        }

        private void SetInterestFlags(VisitChangeDto visitChangeDto, Visit visit)
        {
            bool interestZero = visitChangeDto.InterestZero.GetValueOrDefault(false);
            if (visit.InterestZero != interestZero)
            {
                visit.InterestZero = interestZero;

                this._session.RegisterPostCommitSuccessAction(v =>
                {
                    this.Bus.Value.PublishMessage(new FinancePlanVisitFinancialAssistanceFlagChangedMessage
                    {
                        VpGuarantorId = v.VPGuarantor.VpGuarantorId,
                        VisitId = v.VisitId
                    }).Wait();
                }, visit);
            }

            if (visitChangeDto.InterestRefund.GetValueOrDefault(false))
            {
                this._session.RegisterPostCommitSuccessAction(v =>
                {
                    this.Bus.Value.PublishMessage(new FinancePlanVisitRefundInterestMessage
                    {
                        VpGuarantorId = v.VPGuarantor.VpGuarantorId,
                        VisitId = v.VisitId
                    }).Wait();
                }, visit);
            }
        }

        #region insurance

        private void MergeInsurancePlans(VisitChangeDto visitChangeDto, Visit visit, Guarantor vpGuarantor)
        {
            if (visitChangeDto.VisitInsurancePlans == null)
            {
                visitChangeDto.VisitInsurancePlans = new List<VisitInsurancePlanChangeDto>();
            }

            if (this.VisitInsurancePlansChanged(visit.VisitInsurancePlans, visitChangeDto.VisitInsurancePlans))
            {
                //Insert/Update VisitInsurancePlans with updated InsurancePlan information
                foreach (VisitInsurancePlanChangeDto visitInsurancePlanChangeDto in visitChangeDto.VisitInsurancePlans)
                {
                    VisitInsurancePlan existingVisitInsurancePlan = visit.VisitInsurancePlans.FirstOrDefault(x => x.PayOrder == visitInsurancePlanChangeDto.PayOrder);
                    if (existingVisitInsurancePlan != null) //Update
                    {
                        existingVisitInsurancePlan.InsurancePlan = this.UpdateInsurancePlan(visitInsurancePlanChangeDto.InsurancePlan);
                    }
                    else //Insert
                    {
                        VisitInsurancePlan visitInsurancePlan = Mapper.Map<VisitInsurancePlan>(visitInsurancePlanChangeDto);
                        visitInsurancePlan.Visit = visit;
                        visitInsurancePlan.InsurancePlan = this.UpdateInsurancePlan(visitInsurancePlanChangeDto.InsurancePlan);
                        visit.VisitInsurancePlans.Add(visitInsurancePlan);
                    }
                }

                //Delete
                for (int i = visit.VisitInsurancePlans.Count - 1; i >= 0; i--)
                {
                    VisitInsurancePlan visitInsurancePlan = visit.VisitInsurancePlans[i];
                    VisitInsurancePlanChangeDto existingVisitInsurancePlanChangeDto = visitChangeDto.VisitInsurancePlans.FirstOrDefault(x => x.PayOrder == visitInsurancePlan.PayOrder);
                    if (existingVisitInsurancePlanChangeDto == null)
                    {
                        visit.VisitInsurancePlans.RemoveAt(i); //This should result in a delete
                    }
                }

                this.PublishHqyMessageForUiNotifications(visit, vpGuarantor.VpGuarantorId);
            }

            if (visit.VisitInsurancePlans.Count == 0)
            {
                visit.PrimaryInsuranceTypeId = this.Client.Value.SelfPayPrimaryInsuranceTypeId;
            }
        }

        private InsurancePlan UpdateInsurancePlan(InsurancePlanDto insurancePlanDto)
        {
            InsurancePlan insurancePlan = this._insuranceService.Value.GetInsurancePlan(insurancePlanDto.SourceSystemKey, insurancePlanDto.BillingSystemId) ?? new InsurancePlan();

            bool isUpdate = insurancePlan.InsurancePlanId != default(int);

            int id = insurancePlan.InsurancePlanId;
            Mapper.Map(insurancePlanDto, insurancePlan);

            if (isUpdate)
            {
                insurancePlan.InsurancePlanId = id;
            }
            else
            {
                insurancePlan.InsurancePlanId = this._insuranceService.Value.GetInsurancePlans().Select(x => x.InsurancePlanId).Max() + 1;
            }

            insurancePlan.PrimaryInsuranceType = this.UpdatePrimaryInsuranceType(insurancePlanDto.PrimaryInsuranceType);
            this._insuranceService.Value.AddInsurancePlan(insurancePlan, isUpdate);

            return insurancePlan;
        }

        private PrimaryInsuranceType UpdatePrimaryInsuranceType(PrimaryInsuranceTypeDto primaryInsuranceTypeDto)
        {
            IList<PrimaryInsuranceType> primaryInsuranceTypes = this._insuranceService.Value.GetPrimaryInsuranceTypes();
            PrimaryInsuranceType primaryInsuranceType = primaryInsuranceTypes.FirstOrDefault(x => x.PrimaryInsuranceTypeId == primaryInsuranceTypeDto.PrimaryInsuranceTypeId) ?? new PrimaryInsuranceType();

            bool isUpdate = primaryInsuranceType.PrimaryInsuranceTypeId != default(int);

            Mapper.Map(primaryInsuranceTypeDto, primaryInsuranceType);

            this._insuranceService.Value.AddPrimaryInsuranceType(primaryInsuranceType, isUpdate);

            return primaryInsuranceType;
        }

        private bool VisitInsurancePlansChanged(IList<VisitInsurancePlan> visitInsurancePlans, IList<VisitInsurancePlanChangeDto> visitInsurancePlanChanges)
        {
            return !(visitInsurancePlans.Count == visitInsurancePlanChanges.Count && visitInsurancePlanChanges.All(c => visitInsurancePlans.Any(x => x.PayOrder == c.PayOrder && x.InsurancePlan?.SourceSystemKey == c.InsurancePlan?.SourceSystemKey)));
        }

        #endregion

        #region publish notifications

        private void PublishPaymentSummaryMessageForUiNotifications(VpTransactionTypeEnum vpTransactionTypeEnum, int vpGuarantorId)
        {
            if (EnumHelper<VpTransactionTypeEnum>.ContainsIntersect(vpTransactionTypeEnum, VpTransactionTypeEnumCategory.PaymentSummary))
            {
                this._session.RegisterPostCommitSuccessAction((vpgid) =>
                {
                    this.Bus.Value.PublishMessage(new AddSystemMessageVisitPayUserByGuarantorMessage
                    {
                        VpGuarantorId = vpgid,
                        SystemMessageEnum = SystemMessageEnum.PaymentSummary
                    }).Wait();
                }, vpGuarantorId);
            }
        }

        private void PublishHqyMessageForUiNotifications(Visit visit, int vpGuarantorId)
        {
            if (!this.FeatureService.Value.IsFeatureEnabled(VisitPayFeatureEnum.HealthEquityFeature))
            {
                return;
            }

            SystemMessageVisitPayUser hqyMessage = this._systemMessageVisitPayUserService.Value.GetSystemMessageVisitPayUser(vpGuarantorId, SystemMessageEnum.Hqy);
            if (hqyMessage == null)
            {
                return;
            }

            // show if user has visits for the client's insurance plan list.
            // or if HealthEquityDisplayIfSsoEmployerIdMapIsEmpty
            if (this._visitService.Value.DoesGuarantorHaveVisitWithInsurancePlanInList(vpGuarantorId, this.Client.Value.HealthEquitySsoEmployerIdMap.Keys.ToList()) ||
                this.Client.Value.HealthEquityDisplayIfSsoEmployerIdMapIsEmpty)
            {
                IDictionary<string, string> insurancePlanMap = this.Client.Value.HealthEquitySsoEmployerIdMap;
                if (insurancePlanMap != null && insurancePlanMap.Count > 0)
                {
                    List<string> ssks = insurancePlanMap.Keys.ToList();
                    bool userHasVisitsForInsurancePlans = visit.VisitInsurancePlans.Any(x => ssks.Contains(x.InsurancePlan.SourceSystemKey));

                    if (userHasVisitsForInsurancePlans)
                    {
                        this._session.RegisterPostCommitSuccessAction((vpgid) =>
                        {
                            this.Bus.Value.PublishMessage(new AddSystemMessageVisitPayUserByGuarantorMessage()
                            {
                                VpGuarantorId = vpgid,
                                SystemMessageEnum = SystemMessageEnum.Hqy
                            }).Wait();
                        }, vpGuarantorId);
                    }
                }
            }
        }

        #endregion
    }
}
﻿namespace Ivh.Application.Core.ApplicationServices
{
    using System;
    using AutoMapper;
    using Base.Common.Interfaces;
    using Base.Services;
    using Common.Dtos;
    using Common.Interfaces;
    using Domain.Guarantor.Interfaces;
    using Domain.Visit.Interfaces;

    public class VisitAgingHistoryApplicationService : ApplicationService, IVisitAgingHistoryApplicationService
    {
        private readonly Lazy<IGuarantorService> _guarantorService;
        private readonly Lazy<IVisitAgingHistoryService> _visitAgingHistoryService;

        public VisitAgingHistoryApplicationService(
            Lazy<IApplicationServiceCommonService> applicationServiceCommonService,
            Lazy<IGuarantorService> guarantorService,
            Lazy<IVisitAgingHistoryService> visitAgingHistoryService) : base(applicationServiceCommonService)
        {
            this._guarantorService = guarantorService;
            this._visitAgingHistoryService = visitAgingHistoryService;
        }

        public VisitAgingHistoryResultsDto GetAllVisitAgingHistory(int visitPayUserId, VisitAgingHistoryFilterDto filterDto, int pageNumber, int pageSize)
        {
            int guarantorId = this._guarantorService.Value.GetGuarantorId(visitPayUserId);
            VisitAgingHistoryResults results = this._visitAgingHistoryService.Value.GetAllVisitAgingHistory(guarantorId, Mapper.Map<VisitAgingHistoryFilter>(filterDto), Math.Max(pageNumber - 1, 0), pageSize);

            return Mapper.Map<VisitAgingHistoryResultsDto>(results);
        }
    }
}
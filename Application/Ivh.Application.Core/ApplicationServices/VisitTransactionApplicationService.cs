﻿namespace Ivh.Application.Core.ApplicationServices
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using AutoMapper;
    using Base.Common.Interfaces;
    using Base.Services;
    using Common.Dtos;
    using Common.Interfaces;
    using Common.Utilities.Builders;
    using Domain.Content.Interfaces;
    using Domain.FinanceManagement.FinancePlan.Entities;
    using Domain.FinanceManagement.FinancePlan.Interfaces;
    using Domain.FinanceManagement.Payment.Entities;
    using Domain.FinanceManagement.Payment.Interfaces;
    using Domain.Guarantor.Entities;
    using Domain.Guarantor.Interfaces;
    using Domain.Visit.Entities;
    using Domain.Visit.Interfaces;
    using Domain.Settings.Entities;
    using FinanceManagement.Common.Interfaces;
    using HospitalData.Common.Interfaces;
    using Ivh.Common.Base.Utilities.Extensions;
    using Ivh.Common.VisitPay.Constants;
    using Ivh.Common.VisitPay.Enums;
    using Ivh.Common.VisitPay.Messages.HospitalData.Dtos;
    using Ivh.Common.VisitPay.Strings;
    using Ivh.Common.Web.Utilities;
    using Magnum.Linq;
    using OfficeOpenXml;
    using OfficeOpenXml.Style;

    public class VisitTransactionApplicationService : ApplicationService, IVisitTransactionApplicationService
    {
        private readonly Lazy<IFinancePlanService> _financePlanService;
        private readonly Lazy<IContentService> _contentService;
        private readonly Lazy<IVisitBalanceBaseApplicationService> _visitBalanceBaseApplicationService;
        private readonly Lazy<IGuarantorService> _guarantorService;
        private readonly Lazy<IPaymentAllocationService> _paymentAllocationService;
        private readonly Lazy<IPaymentSummaryApplicationService> _paymentSummaryApplicationService;
        private readonly Lazy<IVisitService> _visitService;
        private readonly Lazy<IVisitTransactionService> _visitTransactionService;
        private readonly Lazy<IImageService> _imageService;

        public VisitTransactionApplicationService(
            Lazy<IApplicationServiceCommonService> applicationServiceCommonService,
            Lazy<IContentService> contentService,
            Lazy<IFinancePlanService> financePlanService,
            Lazy<IGuarantorService> guarantorService,
            Lazy<IPaymentAllocationService> paymentAllocationService,
            Lazy<IPaymentSummaryApplicationService> paymentSummaryApplicationService,
            Lazy<IVisitService> visitService,
            Lazy<IVisitTransactionService> visitTransactionService,
            Lazy<IVisitBalanceBaseApplicationService> visitBalanceBaseApplicationService,
            Lazy<IImageService> imageService) : base(applicationServiceCommonService)
        {
            this._contentService = contentService;
            this._visitBalanceBaseApplicationService = visitBalanceBaseApplicationService;
            this._financePlanService = financePlanService;
            this._guarantorService = guarantorService;
            this._paymentAllocationService = paymentAllocationService;
            this._paymentSummaryApplicationService = paymentSummaryApplicationService;
            this._visitService = visitService;
            this._visitTransactionService = visitTransactionService;
            this._imageService = imageService;
        }

        public IReadOnlyList<string> GetTransactionTypes()
        {
            return this._visitTransactionService.Value.GetTransactionTypesAsStrings();
        }

        public int GetTransactionTotals(int visitPayUserId, VisitTransactionFilterDto filterDto)
        {
            int guarantorId = this._guarantorService.Value.GetGuarantorId(visitPayUserId);
            VisitTransactionFilter filter = Mapper.Map<VisitTransactionFilter>(filterDto);
            return this._visitTransactionService.Value.GetTransactionTotals(guarantorId, filter);
        }

        public VisitTransactionResultsDto GetTransactions(int visitPayUserId, VisitTransactionFilterDto filterDto, int pageNumber, int pageSize)
        {
            int guarantorId = this._guarantorService.Value.GetGuarantorId(visitPayUserId);

            List<VisitTransactionDto> visitTransactionDtos = new List<VisitTransactionDto>();
            IList<VpTransactionType> vpTransactionTypes = this._visitTransactionService.Value.GetTransactionTypes();
            IList<VpTransactionTypeFilterEnum> filterTypes = filterDto.TransactionType.IsNotNullOrEmpty() ? filterDto.TransactionType : Enum.GetValues(typeof(VpTransactionTypeFilterEnum)).Cast<VpTransactionTypeFilterEnum>().ToList();

            // get visit transactions if applicable to filter
            IList<VpTransactionTypeEnum> vpTransactionTypeFilters = this.GetTransactionTypesForFilter(filterTypes);

            VisitTransactionResults results = new VisitTransactionResults();
            if (vpTransactionTypeFilters.Any())
            {
                VisitTransactionFilter filter = Mapper.Map<VisitTransactionFilter>(filterDto);
                filter.TransactionTypes = this.GetTransactionTypesForFilter(filterTypes);

                results = this._visitTransactionService.Value.GetTransactions(guarantorId, filter, 0, int.MaxValue);
                visitTransactionDtos.AddRange(Mapper.Map<IList<VisitTransactionDto>>(results.VisitTransactions));
            }

            if (filterDto.VisitId.HasValue)
            {
                // interest assessments
                if (filterTypes.Contains(VpTransactionTypeFilterEnum.Interest))
                {
                    string interestDisplayName = this._visitTransactionService.Value.GetTransactionDescription(vpTransactionTypes.FirstOrDefault(x => x.VpTransactionTypeEnum == VpTransactionTypeEnum.VppInterestCharge));

                    IList<FinancePlanVisitInterestDueResult> interestAssessments = this._financePlanService.Value.GetInterestAssessedForVisits(filterDto.VisitId.GetValueOrDefault(0).ToListOfOne());
                    visitTransactionDtos.AddRange(interestAssessments.Where(x => x.InterestDue != 0m).Select(x => new VisitTransactionDto
                    {
                        DisplayDate = x.OverrideInsertDate.GetValueOrDefault(x.InsertDate),
                        TransactionAmount = x.InterestDue,
                        TransactionDescription = interestDisplayName
                    }));
                }

                // payments
                if (filterTypes.Contains(VpTransactionTypeFilterEnum.Payments))
                {
                    string discountDisplayName = this._visitTransactionService.Value.GetTransactionDescription(vpTransactionTypes.FirstOrDefault(x => x.VpTransactionTypeEnum == VpTransactionTypeEnum.VppDiscount));
                    string interestDisplayName = this._visitTransactionService.Value.GetTransactionDescription(vpTransactionTypes.FirstOrDefault(x => x.VpTransactionTypeEnum == VpTransactionTypeEnum.VppInterestPayment));
                    string principalDisplayName = this._visitTransactionService.Value.GetTransactionDescription(vpTransactionTypes.FirstOrDefault(x => x.VpTransactionTypeEnum == VpTransactionTypeEnum.VppPrincipalPayment));

                    // uncleared non-interest allocations
                    IList<PaymentAllocationVisitAmountResult> allocationResults = this._paymentAllocationService.Value.GetNonInterestUnclearedPaymentAmountsForVisits(filterDto.VisitId.GetValueOrDefault(0).ToListOfOne());
                    this.FilterVisitTransactions(results.VisitTransactions.ToList(),visitTransactionDtos, allocationResults);

                    visitTransactionDtos.AddRange(allocationResults.Select(x => new VisitTransactionDto
                    {
                        DisplayDate = x.InsertDate,
                        TransactionAmount = x.ActualAmount * -1m,
                        TransactionDescription = x.PaymentAllocationType == PaymentAllocationTypeEnum.VisitPrincipal ? principalDisplayName : discountDisplayName
                    }));

                    // interest payments
                    IList<FinancePlanVisitInterestDueResult> interestResults = this._financePlanService.Value.GetInterestForVisits(filterDto.VisitId.GetValueOrDefault(0).ToListOfOne())
                        .Where(x => x.FinancePlanVisitInterestDueType.IsInCategory(FinancePlanVisitInterestDueTypeEnumCategory.InterestPayment))
                        .ToList();

                    visitTransactionDtos.AddRange(interestResults.Select(x => new VisitTransactionDto
                    {
                        DisplayDate = x.OverrideInsertDate.GetValueOrDefault(x.InsertDate),
                        TransactionAmount = x.InterestDue,
                        TransactionDescription = interestDisplayName
                    }));
                }
            }

            return new VisitTransactionResultsDto
            {
                TotalRecords = visitTransactionDtos.Count,
                VisitTransactions = filterDto.SortOrder.Equals("asc") ? visitTransactionDtos.OrderBy(x => x.DisplayDate).Skip(Math.Max(pageNumber - 1, 0) * pageSize).Take(pageSize).ToList() :
                    visitTransactionDtos.OrderByDescending(x => x.DisplayDate).Skip(Math.Max(pageNumber - 1, 0) * pageSize).Take(pageSize).ToList()
            };
        }

        /*
            VP-6334
            The scenario is a  payment is made in VP, the hospital acknowledges the transaction (echoes) but the payment is not cleared.
            This causes us to show the uncleared payment and its echoed transaction.
            This will use the PaymentAllocationIds from uncleared payments to remove echoed VisitTransactions that are uncleared
        */
        private void FilterVisitTransactions(List<VisitTransaction> paymentTransactions, List<VisitTransactionDto> visitTransactionDtos, IList<PaymentAllocationVisitAmountResult> allocationResults)
        {
            bool removeTransactionsAfterUnclearedPayment = this.FeatureService.Value.IsFeatureEnabled(VisitPayFeatureEnum.FeatureSuppressDuplicatePrincipalPaymentsInUi);
            if (removeTransactionsAfterUnclearedPayment)
            {
                if (!allocationResults.Any())
                {
                    return;
                }

                if (!paymentTransactions.Any())
                {
                    return;
                }

                List<int> paymentAllocationIdsForUncleared = allocationResults.Select(x => x.PaymentAllocationId).ToList();

                List<VisitTransaction> transactionsToRemove = paymentTransactions.Where(x => x.PaymentAllocationId != null)
                        .Where(x => paymentAllocationIdsForUncleared.Contains(x.PaymentAllocationId.Value))
                        .ToList();

                if (!transactionsToRemove.Any())
                {
                    return;
                }
                foreach (VisitTransaction visitTransaction in transactionsToRemove)
                {
                    VisitTransactionDto visitTransactionToRemove = visitTransactionDtos.FirstOrDefault(x => x.VisitTransactionId == visitTransaction.VisitTransactionId);
                    visitTransactionDtos.Remove(visitTransactionToRemove);
                    paymentTransactions.Remove(visitTransaction);
                }

            }
        }


        public VisitTransactionSummaryDto GetTransactionSummary(int visitPayUserId, int visitId, DateTime? startDate = null, DateTime? endDate = null)
        {
            int vpGuarantorId = this._guarantorService.Value.GetGuarantorId(visitPayUserId);

            IList<FinancePlanVisitInterestDueResult> interestResults = this._financePlanService.Value.GetInterestForVisits(visitId.ToListOfOne(), startDate, endDate);

            VisitTransactionFilter filter = new VisitTransactionFilter { VisitId = visitId };
            if (startDate.HasValue)
            {
                filter.TransactionDateRangeFrom = startDate.Value.ToString("MM/dd/yyyy HH:mm:ss");
            }
            if (endDate.HasValue)
            {
                filter.TransactionDateRangeTo = endDate.Value.ToString("MM/dd/yyyy HH:mm:ss");
            }

            IEnumerable<VisitTransaction> visitTransactions = this._visitTransactionService.Value.GetTransactions(vpGuarantorId, filter, 0, int.MaxValue)?.VisitTransactions ?? new List<VisitTransaction>();

            // interest assessed
            decimal interestAssessed = interestResults.Where(x => x.FinancePlanVisitInterestDueType == FinancePlanVisitInterestDueTypeEnum.InterestAssessment)
                .Sum(x => x.InterestDue);

            // charges
            decimal charges = visitTransactions.Where(x => this.GetTransactionTypesForFilter(VpTransactionTypeFilterEnum.Charges.ToListOfOne()).Contains(x.VpTransactionType.VpTransactionTypeEnum)).Sum(x => x.VisitTransactionAmount.TransactionAmount);

            // insurance
            decimal insurance = visitTransactions.Where(x => this.GetTransactionTypesForFilter(VpTransactionTypeFilterEnum.Insurance.ToListOfOne()).Contains(x.VpTransactionType.VpTransactionTypeEnum)).Sum(x => x.VisitTransactionAmount.TransactionAmount);

            // other
            decimal other = visitTransactions.Where(x => this.GetTransactionTypesForFilter(VpTransactionTypeFilterEnum.Other.ToListOfOne()).Contains(x.VpTransactionType.VpTransactionTypeEnum)).Sum(x => x.VisitTransactionAmount.TransactionAmount);

            // payments
            decimal payments = 0m;
            bool removeTransactionsAfterUnclearedPayment = this.FeatureService.Value.IsFeatureEnabled(VisitPayFeatureEnum.FeatureSuppressDuplicatePrincipalPaymentsInUi);
            if (removeTransactionsAfterUnclearedPayment)
            {
                IList<PaymentAllocationVisitAmountResult> allocationResults = this._paymentAllocationService.Value.GetNonInterestUnclearedPaymentAmountsForVisits(visitId.ToListOfOne());
                List<VisitTransaction> paymentTransactions = visitTransactions.Where(x => this.GetTransactionTypesForFilter(VpTransactionTypeFilterEnum.Payments.ToListOfOne()).Contains(x.VpTransactionType.VpTransactionTypeEnum)).ToList();
                if (!allocationResults.Any())
                {
                    //no need to filter
                    payments = paymentTransactions.Sum(x => x.VisitTransactionAmount.TransactionAmount);
                }
                else
                {
                    List<VisitTransactionDto> visitTransactionDtos = new List<VisitTransactionDto>();
                    this.FilterVisitTransactions(paymentTransactions,visitTransactionDtos,allocationResults);
                    decimal outstandingVisitTransactionsSum = (visitTransactionDtos.Any()) ? visitTransactionDtos.Sum(x => x.TransactionAmount) : 0m;
                    decimal filteredPaymentsSum = paymentTransactions.Sum(x => x.VisitTransactionAmount.TransactionAmount);
                    payments = outstandingVisitTransactionsSum + filteredPaymentsSum;
                }
            }
            else
            {
                payments = visitTransactions.Where(x => this.GetTransactionTypesForFilter(VpTransactionTypeFilterEnum.Payments.ToListOfOne()).Contains(x.VpTransactionType.VpTransactionTypeEnum)).Sum(x => x.VisitTransactionAmount.TransactionAmount);
            }
            

            IDictionary<int,decimal[]> unclearedPaymentsForVisits = this._visitBalanceBaseApplicationService.Value.GetUnclearedPaymentsForVisits(visitId.ToListOfOne(), startDate, endDate);
            decimal unclearedPayments = unclearedPaymentsForVisits?.GetValue(visitId)?.Sum() ?? 0m;
            decimal interestPayments = interestResults.Where(x => x.FinancePlanVisitInterestDueType.IsInCategory(FinancePlanVisitInterestDueTypeEnumCategory.InterestPayment)).Sum(x => x.InterestDue);
            decimal allPayments = payments + unclearedPayments + interestPayments;

            //
            return new VisitTransactionSummaryDto
            {
                Charges = charges,
                Interest = interestAssessed,
                Other = other,
                Insurance = insurance,
                AllPayments = allPayments
            };
        }

        private IList<VpTransactionTypeEnum> GetTransactionTypesForFilter(IList<VpTransactionTypeFilterEnum> filters)
        {
            if (filters.IsNullOrEmpty())
            {
                return new List<VpTransactionTypeEnum>();
            }

            List<VpTransactionTypeEnum> vpTransactionTypes = new List<VpTransactionTypeEnum>();

            if (filters.Contains(VpTransactionTypeFilterEnum.Charges))
            {
                vpTransactionTypes.AddRange(new[]
                    {
                        VpTransactionTypeEnum.HsCharge,
                        VpTransactionTypeEnum.HsInterestAssessed
                    });
            }

            if (filters.Contains(VpTransactionTypeFilterEnum.Insurance))
            {
                vpTransactionTypes.AddRange(new[]
                {
                    VpTransactionTypeEnum.HsPayorCash,
                    VpTransactionTypeEnum.HsPayorNonCash
                });
            }

            if (filters.Contains(VpTransactionTypeFilterEnum.Other))
            {
                vpTransactionTypes.AddRange(new[]
                {
                    VpTransactionTypeEnum.HsCharity,
                    VpTransactionTypeEnum.HsNonPresumptiveCharity,
                    VpTransactionTypeEnum.HsPatientNonCash,
                    VpTransactionTypeEnum.HsWriteoffTransaction,
                    VpTransactionTypeEnum.OtherTransaction
                });
            }

            if (filters.Contains(VpTransactionTypeFilterEnum.Payments))
            {
                vpTransactionTypes.AddRange(new[]
                {
                    VpTransactionTypeEnum.HsPatientCash,
                    VpTransactionTypeEnum.HsInterestPayment,
                    VpTransactionTypeEnum.VppPrincipalPayment,
                    VpTransactionTypeEnum.VppPrincipalPaymentHsReallocation,
                    VpTransactionTypeEnum.VppPrincipalPaymentHsReversal,
                    VpTransactionTypeEnum.VppPrincipalPaymentReassessmentReallocation,
                    VpTransactionTypeEnum.VppPrincipalPaymentReassessmentReversal,
                    VpTransactionTypeEnum.VppDiscount,
                    VpTransactionTypeEnum.VppDiscountHsReallocation,
                    VpTransactionTypeEnum.VppDiscountHsReversal
                });
            }

            return vpTransactionTypes;
        }

        public PaymentSummarySearchResultsDto GetPaymentSummary(int vpGuarantorId, PaymentSummaryFilterDto filterDto, int pageNumber, int pageSize)
        {
            PaymentSummaryDto paymentSummaryDto = Mapper.Map<PaymentSummaryDto>(this._paymentSummaryApplicationService.Value.GetPaymentSummary(vpGuarantorId, filterDto.PostDateBegin, filterDto.PostDateEnd));

            List<IGrouping<string, PaymentSummaryTransactionDto>> groupings = paymentSummaryDto.Payments.GroupBy(payment => payment.VisitSourceSystemKeyDisplay).ToList();

            Guarantor guarantor = this._guarantorService.Value.GetGuarantor(vpGuarantorId);
            IReadOnlyList<Visit> unmatchedVisits = this._visitService.Value.GetVisitsUnmatched(vpGuarantorId);
            
            PaymentSummarySearchResultsDto resultsDto = new PaymentSummarySearchResultsDto();

            foreach (IGrouping<string, PaymentSummaryTransactionDto> grouping in groupings)
            {
                PaymentSummaryTransactionDto visit = grouping.First();

                bool isVisitUnmatched = unmatchedVisits.Any(x => x.IsUnmatched && (
                                                                (x.SourceSystemKey == visit.VisitSourceSystemKey && x.BillingSystemId == visit.VisitBillingSystemId) ||
                                                                (x.MatchedSourceSystemKey == visit.VisitSourceSystemKey && x.MatchedBillingSystemId == visit.VisitBillingSystemId)));

                if (isVisitUnmatched)
                {
                    // do not display any transactions associated with an unmatched visit
                    continue;
                }

                PaymentSummaryGroupedDto groupedDto = new PaymentSummaryGroupedDto
                {
                    MatchOption = visit.MatchOption,
                    IsMaskedConsolidated = !visit.IsPatientGuarantor && !visit.IsPatientMinor && guarantor.ConsolidationStatus.IsInCategory(GuarantorConsolidationStatusEnumCategory.Mask),
                    PatientName = visit.PatientFirstNameLastName,
                    VisitDate = visit.VisitDate,
                    VisitDescription = visit.VisitDescription,
                    VisitSourceSystemKeyDisplay = visit.VisitSourceSystemKeyDisplay,
                    VpGuarantorId = paymentSummaryDto.VpGuarantorId
                };

                foreach (PaymentSummaryTransactionDto transaction in grouping.OrderBy(x => x.PostDate).ToList())
                {
                    groupedDto.Payments.Add(new PaymentSummaryGroupedTransactionDto
                    {
                        PostDate = transaction.PostDate,
                        TransactionAmount = transaction.TransactionAmount,
                        Visit = groupedDto
                    });
                }

                groupedDto.TotalTransactionAmount = groupedDto.Payments.Sum(x => x.TransactionAmount);

                // mapping to self to obfuscate prior to sorting
                resultsDto.Visits.Add(Mapper.Map<PaymentSummaryGroupedDto>(groupedDto));
            }
            
            resultsDto.SummaryBalanceTotal = resultsDto.Visits.Sum(x => x.TotalTransactionAmount);
            resultsDto.TotalRecords = resultsDto.Visits.Count();
            
            resultsDto.Visits = (resultsDto.Visits.OrderBy(x => x.VisitDate, true).Batch(pageSize).Skip(Math.Max(pageNumber - 1, 0)).FirstOrDefault() ?? Enumerable.Empty<PaymentSummaryGroupedDto>()).ToList();

            return resultsDto;
        }

        #region exports

        public async Task<byte[]> ExportTransactionsClientAsync(int visitPayUserId, VisitTransactionFilterDto filterDto, string userName)
        {
            int guarantorId = this._guarantorService.Value.GetGuarantorId(visitPayUserId);
            Guarantor guarantor = this._guarantorService.Value.GetGuarantor(guarantorId);
            VisitTransactionResultsDto transactionResults = this.GetTransactions(visitPayUserId, filterDto, 1, iVinciExcelExportBuilder.MaxExportRecords);

            IDictionary<string, string> textRegions = await this._contentService.Value.GetTextRegionsAsync(
                TextRegionConstants.Amount,
                TextRegionConstants.Date,
                TextRegionConstants.Description,
                TextRegionConstants.Guarantor,
                TextRegionConstants.Inactive,
                TextRegionConstants.NoTransactionsToDisplay,
                TextRegionConstants.Patient,
                TextRegionConstants.Status,
                TextRegionConstants.StatusDisplayActive,
                TextRegionConstants.Type,
                TextRegionConstants.VisitId);

            Client client = this.ClientService.Value.GetClient();
            System.Drawing.Image logo = await this._imageService.Value.GetExportLogoAsync();

            using (ExcelPackage package = new ExcelPackage())
            {
                iVinciExcelExportBuilder builder = new iVinciExcelExportBuilder(package, $"{guarantor.User.FirstNameLastName} {textRegions.GetValue(TextRegionConstants.Transactions)}")
                    .WithWorkSheetNameOf(textRegions.GetValue(TextRegionConstants.Transactions))
                    .WithLogo(logo)
                    .AddInfoBlockRow(new ColumnBuilder(DateTime.UtcNow.ToString(Format.DateFormat), textRegions.GetValue(TextRegionConstants.FileDate)).WithStyle(StyleTypeEnum.AlignLeft))
                    .AddInfoBlockRow(new ColumnBuilder(userName, textRegions.GetValue(TextRegionConstants.ExportedBy)).WithStyle(StyleTypeEnum.AlignLeft));

                builder.AddDataBlock(transactionResults.VisitTransactions, transaction =>
                    {
                        string visitDisplay = transaction.Visit.IsUnmatched ? client.RedactedVisitReplacementValue : $"#{transaction.Visit.SourceSystemKeyDisplay}";
                        string localizedVisitState = textRegions.GetValue(transaction.Visit.CurrentVisitState.ToString());

                        List<Column> columns = new List<Column>
                        {
                            new ColumnBuilder(transaction.DisplayDate, textRegions.GetValue(TextRegionConstants.Date)).WithDateFormat().AlignLeft(),
                            new ColumnBuilder(transaction.Visit.GuarantorName, textRegions.GetValue(TextRegionConstants.Guarantor)),
                            new ColumnBuilder(transaction.Visit.PatientName, textRegions.GetValue(TextRegionConstants.Patient)),
                            new ColumnBuilder(visitDisplay, textRegions.GetValue(TextRegionConstants.VisitId)),
                            new ColumnBuilder(transaction.TransactionDescription, textRegions.GetValue(TextRegionConstants.Description)),
                            new ColumnBuilder(transaction.TransactionType, textRegions.GetValue(TextRegionConstants.Type))
                        };

                        if (transaction.Visit.IsUnmatched)
                        {
                            columns.Add(new ColumnBuilder("-", textRegions.GetValue(TextRegionConstants.Amount)).AlignCenter());
                        }
                        else
                        {
                            columns.Add(new ColumnBuilder(transaction.TransactionAmount, textRegions.GetValue(TextRegionConstants.Amount)).WithCurrencyFormat());
                        }

                        columns.Add(new ColumnBuilder(localizedVisitState, textRegions.GetValue(TextRegionConstants.Status)));

                        return columns;
                    })
                    .WithNoRecordsFoundMessageOf(textRegions.GetValue(TextRegionConstants.NoTransactionsToDisplay));

                builder.Build();

                return package.GetAsByteArray();
            }
        }

        public async Task<byte[]> ExportVisitTransactionsClientAsync(int visitPayUserId, VisitTransactionFilterDto filterDto, string userName)
        {
            int guarantorId = this._guarantorService.Value.GetGuarantorId(visitPayUserId);
            Guarantor guarantor = this._guarantorService.Value.GetGuarantor(guarantorId);
            VisitTransactionResultsDto transactionResults = this.GetTransactions(visitPayUserId, filterDto, 1, iVinciExcelExportBuilder.MaxExportRecords);

            IDictionary<string, string> textRegions = await this._contentService.Value.GetTextRegionsAsync(
                TextRegionConstants.ExportedBy,
                TextRegionConstants.FileDate,
                TextRegionConstants.Transactions);

            System.Drawing.Image logo = await this._imageService.Value.GetExportLogoAsync();

            using (ExcelPackage package = new ExcelPackage())
            {
                iVinciExcelExportBuilder builder = new iVinciExcelExportBuilder(package, $"{guarantor.User.FirstNameLastName} {textRegions.GetValue(TextRegionConstants.Transactions)}")
                .WithWorkSheetNameOf(textRegions.GetValue(TextRegionConstants.Transactions))
                .WithLogo(logo)
                    .AddInfoBlockRow(new ColumnBuilder(DateTime.UtcNow.ToString(Format.DateFormat), textRegions.GetValue(TextRegionConstants.FileDate)).WithStyle(StyleTypeEnum.AlignLeft))
                    .AddInfoBlockRow(new ColumnBuilder(userName, textRegions.GetValue(TextRegionConstants.ExportedBy)).WithStyle(StyleTypeEnum.AlignLeft));

                await this.ExportVisitTransactionsDataAsync(builder, transactionResults);

                builder.Build();

                return package.GetAsByteArray();
            }
        }

        public async Task<byte[]> ExportVisitTransactionsAsync(int visitPayUserId, VisitTransactionFilterDto filterDto, string userName)
        {
            VisitTransactionResultsDto transactionResults = this.GetTransactions(visitPayUserId, filterDto, 1, iVinciExcelExportBuilder.MaxExportRecords);

            IDictionary<string, string> textRegions = await this._contentService.Value.GetTextRegionsAsync(
                TextRegionConstants.ExportedBy,
                TextRegionConstants.FileDate,
                TextRegionConstants.Transactions);

            System.Drawing.Image logo = await this._imageService.Value.GetExportLogoAsync();

            using (ExcelPackage package = new ExcelPackage())
            {
                iVinciExcelExportBuilder builder = new iVinciExcelExportBuilder(package, textRegions.GetValue(TextRegionConstants.Transactions))
                .WithLogo(logo)
                    .AddInfoBlockRow(new ColumnBuilder(DateTime.UtcNow.ToString(Format.DateFormat), textRegions.GetValue(TextRegionConstants.FileDate)).WithStyle(StyleTypeEnum.AlignLeft))
                    .AddInfoBlockRow(new ColumnBuilder(userName, textRegions.GetValue(TextRegionConstants.ExportedBy)).WithStyle(StyleTypeEnum.AlignLeft));

                await this.ExportVisitTransactionsDataAsync(builder, transactionResults);

                builder.Build();

                return package.GetAsByteArray();
            }
        }

        private async Task ExportVisitTransactionsDataAsync(iVinciExcelExportBuilder builder, VisitTransactionResultsDto transactionResults)
        {
            IDictionary<string, string> textRegions = await this._contentService.Value.GetTextRegionsAsync(
                TextRegionConstants.Amount,
                TextRegionConstants.Date,
                TextRegionConstants.Description,
                TextRegionConstants.NoTransactionsToDisplay);

            // obfuscate through automapper
            IList<VisitTransactionDto> transactions = Mapper.Map<IList<VisitTransactionDto>>(transactionResults.VisitTransactions);
            builder.AddDataBlock(transactions, transaction => new List<Column>
                {
                    new ColumnBuilder(transaction.DisplayDate, textRegions.GetValue(TextRegionConstants.Date)).WithDateFormat(),
                    new ColumnBuilder(transaction.TransactionDescription, textRegions.GetValue(TextRegionConstants.Description)),
                    new ColumnBuilder(transaction.TransactionAmount, textRegions.GetValue(TextRegionConstants.Amount)).WithCurrencyFormat()
                })
                .WithNoRecordsFoundMessageOf(textRegions.GetValue(TextRegionConstants.NoTransactionsToDisplay));
        }

        public async Task<byte[]> ExportPaymentSummaryAsync(int vpGuarantorId, PaymentSummaryFilterDto filterDto, string userName)
        {
            PaymentSummarySearchResultsDto fullResults = this.GetPaymentSummary(vpGuarantorId, filterDto, 1, iVinciExcelExportBuilder.MaxExportRecords);
            List<PaymentSummaryGroupedTransactionDto> results = fullResults.Visits.SelectMany(x => x.Payments).OrderBy(x => x.PostDate).ToList();

            IDictionary<string, string> textRegions = await this._contentService.Value.GetTextRegionsAsync(
                TextRegionConstants.DateRange,
                TextRegionConstants.ExportedBy,
                TextRegionConstants.FileDate,
                TextRegionConstants.NoPaymentSummary,
                TextRegionConstants.Patient,
                TextRegionConstants.PaymentAmount,
                TextRegionConstants.PaymentDate,
                TextRegionConstants.PaymentSummaryPageHeading,
                TextRegionConstants.SummaryBalanceTotal,
                TextRegionConstants.Transactions,
                TextRegionConstants.VisitDate,
                TextRegionConstants.VisitDescription,
                TextRegionConstants.VisitId);

            System.Drawing.Image logo = await this._imageService.Value.GetExportLogoAsync();

            using (ExcelPackage package = new ExcelPackage())
            {
                string dateRange = $"{(filterDto.PostDateBegin.HasValue ? filterDto.PostDateBegin.Value.ToString(Format.DateFormat) : "")} - {(filterDto.PostDateEnd.HasValue ? filterDto.PostDateEnd.Value.ToString(Format.DateFormat) : "")}";

                const int headerIndex = 12;
                iVinciExcelExportBuilder builder = new iVinciExcelExportBuilder(package, textRegions.GetValue(TextRegionConstants.Transactions))
                   .WithWorkSheetNameOf(textRegions.GetValue(TextRegionConstants.PaymentSummaryPageHeading))
                   .WithLogo(logo)
                   .AddInfoBlockRow(new ColumnBuilder(dateRange, textRegions.GetValue(TextRegionConstants.DateRange)).WithStyle(StyleTypeEnum.AlignLeft))
                   .AddInfoBlockRow(new ColumnBuilder(DateTime.UtcNow.ToString(Format.DateFormat), textRegions.GetValue(TextRegionConstants.FileDate)).WithStyle(StyleTypeEnum.AlignLeft))
                   .AddInfoBlockRow(new ColumnBuilder(userName, textRegions.GetValue(TextRegionConstants.ExportedBy)).WithStyle(StyleTypeEnum.AlignLeft))
                   .AddDataBlock(results, transaction => new List<Column>
                    {
                        new ColumnBuilder($"#{transaction.Visit.VisitSourceSystemKeyDisplay}", textRegions.GetValue(TextRegionConstants.VisitId)),
                        new ColumnBuilder(transaction.Visit.VisitDate, textRegions.GetValue(TextRegionConstants.VisitDate)).AlignLeft().WithDateFormat(),
                        new ColumnBuilder(transaction.Visit.PatientName, textRegions.GetValue(TextRegionConstants.Patient)),
                        new ColumnBuilder(transaction.Visit.VisitDescription, textRegions.GetValue(TextRegionConstants.VisitDescription)),
                        new ColumnBuilder(transaction.PostDate, textRegions.GetValue(TextRegionConstants.PaymentDate)).WithDateFormat(),
                        new ColumnBuilder(transaction.TransactionAmount, textRegions.GetValue(TextRegionConstants.PaymentAmount)).AlignRight().WithCurrencyFormat()
                    })
                   .WithNoRecordsFoundMessageOf(textRegions.GetValue(TextRegionConstants.NoPaymentSummary));

                builder.Build();

                ExcelWorksheet worksheet = package.Workbook.Worksheets.FirstOrDefault();

                if (worksheet != null && results.Any())
                {
                    int summaryCellIndex = results.Count + headerIndex;

                    worksheet.Cells[$"A{summaryCellIndex}"].Value = textRegions.GetValue(TextRegionConstants.SummaryBalanceTotal);
                    worksheet.Cells[$"A{summaryCellIndex}:E{summaryCellIndex}"].Merge = true;

                    using (ExcelRange range = worksheet.Cells[$"A{summaryCellIndex}:E{summaryCellIndex}"])
                    {
                        range.Style.Font.Size = 11;
                        range.Style.Font.Bold = true;
                        range.Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                    }

                    ExcelRange summaryCell = worksheet.Cells[$"F{summaryCellIndex}"];
                    summaryCell.Value = fullResults.SummaryBalanceTotal;
                    summaryCell.Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                    summaryCell.Style.Numberformat.Format = Format.ExportMoneyFormat;
                }

                return package.GetAsByteArray();
            }
        }

        #endregion
    }
}
﻿namespace Ivh.Application.Core.ApplicationServices
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Base.Common.Interfaces;
    using Base.Services;
    using Common.Dtos;
    using Common.Interfaces;
    using Domain.HospitalData.Analytics.Entities;
    using Domain.HospitalData.Analytics.Interfaces;
    using Ivh.Common.Base.Utilities.Extensions;
    using Microsoft.PowerBI.Api.V2.Models;
    using Microsoft.IdentityModel.Clients.ActiveDirectory;
    using Microsoft.PowerBI.Api.V2;
    using Microsoft.Rest;
    using PowerBiAnalyticReport = Microsoft.PowerBI.Api.V2.Models.Report;

    public class AnalyticReportApplicationService : ApplicationService, IAnalyticReportApplicationService
    {
        private readonly Lazy<IAnalyticReportService> _analyticReportService;
        private static AuthenticationResult _authenticationResult;
        private static TokenCredentials _tokenCredentials;

        public AnalyticReportApplicationService(Lazy<IApplicationServiceCommonService> applicationServiceCommonService,
            Lazy<IAnalyticReportService> analyticReportService) : base(applicationServiceCommonService)
        {
            this._analyticReportService = analyticReportService;
        }

        public IList<AnalyticReportDto> GetAnalyticReportList()
        {
            IList<AnalyticReportDto> analyticReportList = null;
            using (PowerBIClient client = new PowerBIClient(new Uri(this.ApplicationSettingsService.Value.PowerBiApiUrl.Value), _tokenCredentials))
            {
                string groupName = $"{this.ApplicationSettingsService.Value.Client.Value}-{this.ApplicationSettingsService.Value.Environment.Value}";
                try
                {
                    Task<Group> clientGroup = client.Groups.GetGroupsWithHttpMessagesAsync()
                        .ContinueWith(task => task.Result.Body.Value.First(group => groupName.Equals(@group.Name, StringComparison.OrdinalIgnoreCase)));
                    Group clientWorkspace = clientGroup.Result;
                    Task<IList<PowerBiAnalyticReport>> reports = client.Reports.GetReportsInGroupWithHttpMessagesAsync(clientWorkspace.Id)
                        .ContinueWith(task => task.Result.Body.Value);
                    List<AnalyticReportDto> analyticReports = new List<AnalyticReportDto>(reports.Result.Count);
                    analyticReports.AddRange(reports.Result.Select(r => new AnalyticReportDto
                    {
                        ReportId = r.Id,
                        ReportName = r.Name,
                        DatasetId = r.DatasetId,
                        EmbedUrl = r.EmbedUrl,
                        GroupId = clientWorkspace.Id
                    }));
                    analyticReportList = this.FilterAnalyticReportList(analyticReports);
                }
                catch (Exception e)
                {
                    this.LoggingService.Value.Info(() => $"No PowerBI workspace exists for {groupName}");
                }
            }

            return analyticReportList;
        }

        public IList<AnalyticReportDto> FilterAnalyticReportList(List<AnalyticReportDto> analyticReports)
        {
            IList<AnalyticReport> visibleReports = this._analyticReportService.Value.GetAnalyticReports();
            if (visibleReports.IsNullOrEmpty())
            {
                return null;
            }
            List<AnalyticReportDto> filteredReports = analyticReports.Where(item =>
                    visibleReports.Any(visibleReport =>
                    {
                        if (visibleReport.ReportName.Equals(item.ReportName, StringComparison.OrdinalIgnoreCase))
                        {
                            item.Classification = visibleReport.AnalyticReportClassification.ClassificationName;
                            item.DisplayName = visibleReport.DisplayName;
                            return true;
                        }
                        return false;
                    })
            ).ToList();

            return filteredReports;
        }

        public TokenCredentials ValidateToken(EmbedToken embedToken, TokenCredentials tokenCredentials)
        {
            if (embedToken != null)
            {
                bool isValidToken = embedToken.Expiration != null && DateTime.Compare((DateTime)embedToken.Expiration, DateTime.UtcNow) > 0;
                if (isValidToken)
                {
                    return tokenCredentials;
                }
            }
            UserPasswordCredential credential = new UserPasswordCredential(this.ApplicationSettingsService.Value.PowerBiUsername.Value,
                this.ApplicationSettingsService.Value.PowerBiPassword.Value);
            this.Authorize(credential).Wait();
            return _tokenCredentials;
        }

        private Task Authorize(UserPasswordCredential credential)
        {
            return Task.Run(async () => {
                _authenticationResult = null;
                _tokenCredentials = null;
                AuthenticationContext authenticationContext = new AuthenticationContext(this.ApplicationSettingsService.Value.PowerBiAuthorityUrl.Value);
                _authenticationResult = await authenticationContext.AcquireTokenAsync(this.ApplicationSettingsService.Value.PowerBiResourceUrl.Value,
                    this.ApplicationSettingsService.Value.PowerBiApplicationId.Value,
                    credential);

                if (_authenticationResult != null)
                {
                    _tokenCredentials = new TokenCredentials(_authenticationResult.AccessToken, "Bearer");
                }
            });
        }

    }
}
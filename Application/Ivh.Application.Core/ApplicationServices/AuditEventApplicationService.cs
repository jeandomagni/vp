﻿namespace Ivh.Application.Core.ApplicationServices
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using AutoMapper;
    using Base.Common.Interfaces;
    using Base.Services;
    using Common.Interfaces;
    using Domain.Core.Audit.Entities;
    using Domain.Core.Audit.Interfaces;
    using Domain.FinanceManagement.FinancePlan.Entities;
    using Domain.FinanceManagement.FinancePlan.Interfaces;
    using Domain.FinanceManagement.Statement.Interfaces;
    using Domain.Visit.Entities;
    using Domain.Visit.Interfaces;
    using Ivh.Common.ServiceBus.Attributes;
    using Ivh.Common.ServiceBus.Enums;
    using Ivh.Common.VisitPay.Messages.Core;
    using MassTransit;
    using Newtonsoft.Json;

    public class AuditEventApplicationService : ApplicationService, IAuditEventApplicationService
    {
        private readonly Lazy<IAuditEventService> _auditEventService;
        private readonly Lazy<IFinancePlanService> _financePlanService;
        private readonly Lazy<IAmortizationService> _amortizationService;
        private readonly Lazy<IVisitService> _visitService;
        private readonly Lazy<IVpStatementService> _statementService;

        public AuditEventApplicationService(
            Lazy<IApplicationServiceCommonService> applicationServiceCommonService,
            Lazy<IAuditEventService> auditEventService,
            Lazy<IFinancePlanService> financePlanService,
            Lazy<IAmortizationService> amortizationService,
            Lazy<IVisitService> visitService,
            Lazy<IVpStatementService> statementService
            ) : base(applicationServiceCommonService)
        {
            this._auditEventService = auditEventService;
            this._financePlanService = financePlanService;
            this._amortizationService = amortizationService;
            this._visitService = visitService;
            this._statementService = statementService;
        }

        public void LogAuditEventFinancePlan(AuditEventFinancePlanMessage message)
        {
            int financePlanId = message.FinancePlanId;
            IList<string> auditEvents = message.AuditEvents;

            FinancePlan financePlan = this._financePlanService.Value.GetFinancePlanById(financePlanId);

            AuditEventFinancePlan auditEventFinancePlan = Mapper.Map<AuditEventFinancePlan>(financePlan);
            auditEventFinancePlan.CurrentPaymentAmount = financePlan.PaymentAmount;

            if (financePlan.FinancePlanOffer != null)
            {
                DateTime paymentDueDate = this._statementService.Value.GetNextPaymentDate(financePlan.VpGuarantor);

                IList<AmortizationMonth> amortizationMonths = this._amortizationService.Value.GetAmortizationMonthsForFinancePlan(
                    financePlan,
                    paymentDueDate,
                    this._financePlanService.Value.GetFinancePlanActiveVisitsBalancesDaily(financePlan)
                );

                auditEventFinancePlan.FinancePlanOfferId = financePlan.FinancePlanOffer.FinancePlanOfferId;
                auditEventFinancePlan.FinancePlanOfferSetTypeId = financePlan.FinancePlanOfferSetTypeId;
                auditEventFinancePlan.CurrentDuration = amortizationMonths.Count;
                auditEventFinancePlan.RemainingMonths = amortizationMonths.Count;
                auditEventFinancePlan.ExpectedMaturityDate = amortizationMonths.OrderByDescending(x => x.MonthDate).FirstOrDefault()?.MonthDate;
            }

            auditEventFinancePlan.CurrentFinancePlanStatus = $"{financePlan.FinancePlanStatus.FinancePlanStatusGroupName}{financePlan.FinancePlanStatus.FinancePlanStatusDisplayName}";

            FinancePlanStatus previousStatus = financePlan.FinancePlanStatusHistory.Count > 1
                ? financePlan.FinancePlanStatusHistory
                    .OrderByDescending(x => x.InsertDate)
                    .ThenByDescending(x => x.FinancePlanStatusHistoryId)
                    .Take(2)
                    .LastOrDefault()?.FinancePlanStatus
                : null;

            auditEventFinancePlan.PreviousFinancePlanStatus = previousStatus == null ? null : $"{previousStatus.FinancePlanStatusGroupName}{previousStatus.FinancePlanStatusDisplayName}";
            auditEventFinancePlan.PrincipalPaidToDate = financePlan.PrincipalPaidForDate(DateTime.UtcNow);
            auditEventFinancePlan.UpdateEvent = string.Join(", ", auditEvents);

            IDictionary<int, Dictionary<string, string>> associatedVisits = financePlan.FinancePlanVisits.ToDictionary(
                k => k.VisitId,
                v => new Dictionary<string, string>
                {
                    {"Owner", this.GetFinancePlanOwnerName(v.FinancePlan)}
                });

            auditEventFinancePlan.AssociatedVisits = JsonConvert.SerializeObject(associatedVisits);
            auditEventFinancePlan.AuditEventDate = DateTime.UtcNow;

            this._auditEventService.Value.LogAuditEventFinancePlan(auditEventFinancePlan);
        }

        private string GetFinancePlanOwnerName(FinancePlan financePlan)
        {
            IReadOnlyList<Visit> visits = this._visitService.Value.GetVisits(financePlan.VpGuarantor.VpGuarantorId, financePlan.FinancePlanVisits.Select(x => x.VisitId).ToList());

            return visits.Any(x => (x.BalanceTransferStatus?.IsActive()).GetValueOrDefault(false)) ? "VisitPay" : "HS";
        }

        #region Message Consumers

        [UniversalConsumer(UniversalQueuesEnum.VisitPayProcessor, UniversalPriorityEnum.Priority30)]
        public void ConsumeMessage(AuditEventFinancePlanMessage message, ConsumeContext<AuditEventFinancePlanMessage> consumeContext)
        {
            this.LogAuditEventFinancePlan(message);
        }

        public bool IsMessageValid(AuditEventFinancePlanMessage message, ConsumeContext<AuditEventFinancePlanMessage> consumeContext)
        {
            bool isValid = true;
            return isValid;
        }

        #endregion
    }
}

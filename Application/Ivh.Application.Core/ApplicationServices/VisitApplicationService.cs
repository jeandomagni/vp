﻿namespace Ivh.Application.Core.ApplicationServices
{
    using AutoMapper;
    using Base.Common.Interfaces;
    using Base.Services;
    using Common.Dtos;
    using Common.Interfaces;
    using Common.Utilities.Builders;
    using Domain.Content.Interfaces;
    using Domain.FinanceManagement.FinancePlan.Interfaces;
    using Domain.Guarantor.Entities;
    using Domain.Guarantor.Interfaces;
    using Domain.Visit.Entities;
    using Domain.Visit.Interfaces;
    using FinanceManagement.Common.Interfaces;
    using Ivh.Common.Base.Utilities.Extensions;
    using Ivh.Common.Data;
    using Ivh.Common.ServiceBus.Attributes;
    using Ivh.Common.ServiceBus.Enums;
    using MassTransit;
    using NHibernate;
    using OfficeOpenXml;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Ivh.Common.Data.Connection.Connections;
    using Ivh.Common.Data.Interfaces;
    using Ivh.Common.VisitPay.Constants;
    using Ivh.Common.VisitPay.Enums;
    using Ivh.Common.VisitPay.Messages.Aging;
    using Ivh.Common.VisitPay.Messages.Core;
    using Ivh.Common.VisitPay.Messages.FinanceManagement;
    using Ivh.Common.VisitPay.Strings;

    public class VisitApplicationService : ApplicationService, IVisitApplicationService
    {
        private readonly Lazy<IGuarantorService> _guarantorService;
        private readonly Lazy<IVisitBalanceBaseApplicationService> _visitBalanceBaseApplicationService;
        private readonly Lazy<IVisitService> _visitService;
        private readonly Lazy<IImageService> _imageService;
        private readonly Lazy<IFinancePlanService> _financePlanService;
        private readonly Lazy<IContentService> _contentService;
        private readonly Lazy<IVisitTransactionChangesApplicationService> _visitTransactionChangesApplicationService;
        private readonly Lazy<IFinancePlanApplicationService> _financePlanApplicationService;
        private readonly Lazy<IVisitPayUserJournalEventApplicationService> _visitPayUserJournalEventApplicationService;
        private readonly ISession _session;

        public VisitApplicationService(
            Lazy<IApplicationServiceCommonService> applicationServiceCommonService,
            Lazy<IVisitBalanceBaseApplicationService> visitBalanceBaseApplicationService,
            Lazy<IVisitService> visitService,
            Lazy<IGuarantorService> guarantorService,
            Lazy<IImageService> imageService,
            Lazy<IFinancePlanService> financePlanService,
            Lazy<IContentService> contentService,
            Lazy<IVisitTransactionChangesApplicationService> visitTransactionChangesApplicationService,
            Lazy<IFinancePlanApplicationService> financePlanApplicationService,
            Lazy<IVisitPayUserJournalEventApplicationService> visitPayUserJournalEventApplicationService,
            ISessionContext<VisitPay> sessionContext
            ) : base(applicationServiceCommonService)
        {
            this._visitBalanceBaseApplicationService = visitBalanceBaseApplicationService;
            this._visitService = visitService;
            this._guarantorService = guarantorService;
            this._imageService = imageService;
            this._financePlanService = financePlanService;
            this._contentService = contentService;
            this._visitTransactionChangesApplicationService = visitTransactionChangesApplicationService;
            this._financePlanApplicationService = financePlanApplicationService;
            this._visitPayUserJournalEventApplicationService = visitPayUserJournalEventApplicationService;
            this._session = sessionContext.Session;
        }

        public VisitDto UpdateVisitAgeTier(string visitSourceSystemKey, int visitBillingSystemId, AgingTierEnum agingTier, string agingMessage, int? actionVisitPayUserId = null)
        {
            Visit visit;
            using (UnitOfWork unit = new UnitOfWork(this._session))
            {
                visit = this._visitService.Value.GetVisitBySystemSourceKey(visitSourceSystemKey, visitBillingSystemId);
                if (visit == null)
                {
                    return null;
                }
                visit.SetAgingTier(agingTier, agingMessage, actionVisitPayUserId);

                if (visit.AgingTierIncreasedToPastDue)
                {
                    this._session.RegisterPostCommitSuccessAction(vpgid =>
                    {
                        this.Bus.Value.PublishMessage(new SendPastDueVisitsEmailMessage
                        {
                            VpGuarantorId = vpgid,
                            VisitIds = new List<int>() { visit.VisitId}
                        }).Wait();
                    }, visit.VPGuarantor.VpGuarantorId);
                }

                this._visitService.Value.SaveVisit(visit);
                unit.Commit();
            }
            return Mapper.Map<VisitDto>(visit);
        }

        public IReadOnlyList<VisitStateEnum> GetVisitStates()
        {
            return Enum.GetValues(typeof(VisitStateEnum)).Cast<VisitStateEnum>().ToList();
        }

        public IReadOnlyList<VisitStateDerivedEnum> GetDerivedVisitStates()
        {
            return Enum.GetValues(typeof(VisitStateDerivedEnum)).Cast<VisitStateDerivedEnum>().ToList();
        }

        public VisitDto GetVisit(int visitPayUserId, int visitId, int? vpGuarantorId = null)
        {
            int guarantorId = vpGuarantorId ?? this._guarantorService.Value.GetGuarantorId(visitPayUserId);

            Visit visit = this._visitService.Value.GetVisit(guarantorId, visitId);
            VisitDto visitDto = Mapper.Map<VisitDto>(visit);

            this._visitBalanceBaseApplicationService.Value.SetTotalBalance(visitDto);

            return visitDto;
        }

        public VisitDto GetVisit(int billingSystemId, string sourceSystemKey)
        {
            Visit visit = this._visitService.Value.GetVisitBySystemSourceKey(sourceSystemKey, billingSystemId);
            VisitDto visitDto = Mapper.Map<VisitDto>(visit);
            return visitDto;
        }

        public bool HasVisits(int vpGuarantorId, IEnumerable<VisitStateEnum> visitStates)
        {
            return this._visitService.Value.HasVisits(vpGuarantorId, visitStates);
        }

        public bool HasVisitsWithFilter(int vpGuarantorId, VisitFilterDto visitFilterDto)
        {
            return this._visitService.Value.HasVisitsWithFilter(vpGuarantorId, Mapper.Map<VisitFilter>(visitFilterDto));
        }

        public VisitResultsDto GetVisits(int visitPayUserId, VisitFilterDto filterDto, int pageNumber, int? pageSize, int? vpGuarantorId = null)
        {
            vpGuarantorId = vpGuarantorId ?? this._guarantorService.Value.GetGuarantorId(visitPayUserId);

            VisitFilter filter = this.GetVisitFilter(vpGuarantorId.Value, filterDto);
            VisitResults visitResults = this._visitService.Value.GetVisits(vpGuarantorId.Value, filter, Math.Max(pageNumber - 1, 0), pageSize);
            IList<VisitDto> visitDtos = Mapper.Map<IList<VisitDto>>(visitResults.Visits);
            
            this._visitBalanceBaseApplicationService.Value.SetTotalBalance(visitDtos);

            this.SetIsPastDue(visitDtos);
            this.SetDerivedState(visitDtos);

            return new VisitResultsDto
            {
                Visits = (IReadOnlyList<VisitDto>)visitDtos,
                TotalRecords = visitResults.TotalRecords
            };
        }

        private void SetDerivedState(IList<VisitDto> visitDtos)
        {
            IList<int> financedVisitIds = this._financePlanService.Value.GetVisitIdsOnActiveFinancePlans(visitDtos.Select(x => x.VisitId).ToList());
            foreach (VisitDto visitDto in visitDtos)
            {
                visitDto.CurrentVisitStateDerivedEnum = this.GetDerivedState(visitDto, financedVisitIds);
            }
        }

        public VisitStateDerivedEnum GetDerivedState(VisitDto visitDto, IList<int> financedVisitIds)
        {
            // look for the inactive variants first
            if (visitDto.IsUnmatched)
            {
                return VisitStateDerivedEnum.Unmatched;
            }
            if (visitDto.IsMaxAge && !financedVisitIds.Contains(visitDto.VisitId))
            {
                if (visitDto.IsMaxAgeActive)
                {
                    return VisitStateDerivedEnum.MaxAge;
                }
                return VisitStateDerivedEnum.MaxAgeClosed;
            }
            if (visitDto.BillingHold)
            {
                return VisitStateDerivedEnum.OnHold;
            }
            if (visitDto.CurrentVisitState == VisitStateEnum.Inactive)
            {
                return VisitStateDerivedEnum.Inactive;
            }
            if (visitDto.CurrentVisitState == VisitStateEnum.NotSet)
            {
                return VisitStateDerivedEnum.Inactive;
            }
            //must be active at this point
            if (financedVisitIds.Contains(visitDto.VisitId))
            {
                return VisitStateDerivedEnum.OnFinancePlan;
            }
            if (visitDto.CurrentVisitState == VisitStateEnum.Active)
            {
                return VisitStateDerivedEnum.Active;
            }
            else // We shouldn't be here
            {
                return VisitStateDerivedEnum.Inactive;
            }
        }

        public void SetIsPastDue(IList<VisitDto> visitDtos)
        {
            IList<int> financedVisitIds = this._financePlanService.Value.GetVisitIdsOnActiveFinancePlans(visitDtos.Select(x => x.VisitId).ToList());

            foreach (VisitDto visitDto in visitDtos.Where(x => !financedVisitIds.Contains(x.VisitId)))
            {
                visitDto.IsPastDue = this._visitService.Value.IsVisitPastDue(visitDto);
                visitDto.IsFinalPastDue = this._visitService.Value.IsVisitFinalPastDue(visitDto);
            }
        }

        public int GetVisitTotals(int visitPayUserId, VisitFilterDto filterDto)
        {
            int vpGuarantorId = this._guarantorService.Value.GetGuarantorId(visitPayUserId);
            VisitFilter filter = this.GetVisitFilter(vpGuarantorId, filterDto);

            return this._visitService.Value.GetVisitTotals(vpGuarantorId, filter);
        }

        public IList<VisitDto> GetAllActiveNonFinancedVisits(int vpGuarantorId)
        {
            IList<Visit> visits = this._visitService.Value.GetAllActiveNonFinancedVisits(vpGuarantorId);
            IList<VisitDto> visitDtos = Mapper.Map<List<VisitDto>>(visits);

            this._visitBalanceBaseApplicationService.Value.SetTotalBalance(visitDtos);

            return visitDtos;
        }


        private VisitFilter GetVisitFilter(int vpGuarantorId, VisitFilterDto visitFilterDto)
        {
            VisitFilter visitFilter = Mapper.Map<VisitFilter>(visitFilterDto);

            if (visitFilterDto.FinancePlanId.HasValue)
            {
                IList<int> visitsOnFinancePlan = this._financePlanService.Value.GetVisitIdsOnActiveFinancePlans(vpGuarantorId, visitFilterDto.FinancePlanId.Value);

                visitFilter.VisitIds = visitsOnFinancePlan;
            }
            else if (visitFilterDto.IsOnActiveFinancePlan.HasValue)
            {
                IList<int> visitsOnActiveFinancePlans = this._financePlanService.Value.GetVisitIdsOnActiveFinancePlans(vpGuarantorId);

                if (visitFilterDto.IsOnActiveFinancePlan == true)
                {
                    visitFilter.VisitIds = visitsOnActiveFinancePlans;
                }
                else
                {
                    IList<int> visitIdsForState = new List<int>();
                    foreach (int visitState in visitFilter.VisitStateIds)
                    {
                        visitIdsForState.AddRange(this._visitService.Value.GetVisitIds(vpGuarantorId, (VisitStateEnum)visitState));
                    }

                    visitFilter.VisitIds = visitIdsForState.Where(x => !visitsOnActiveFinancePlans.Contains(x)).ToList();
                }
            }

            return visitFilter;
        }

        public void SetVisitAgingCount(VisitAgingHistoryDto visitAgingHistoryDto)
        {
            this._visitPayUserJournalEventApplicationService.Value.LogClientChangedVisitAgingTier(
                clientVisitPayUserId: visitAgingHistoryDto.VisitPayUser.VisitPayUserId,
                vpGuarantorId: visitAgingHistoryDto.VpGuarantorId,
                visitId: visitAgingHistoryDto.Visit.VisitId,
                previousAgingTier: (AgingTierEnum)visitAgingHistoryDto.Visit.AgingCount,
                newAgingTier: (AgingTierEnum)visitAgingHistoryDto.AgingTier,
                reason: visitAgingHistoryDto.ChangeDescription,
                context: null);

            this.Bus.Value.PublishMessage(new SetVisitAgeMessage
            {
                Visit = new Ivh.Common.VisitPay.Messages.Aging.Dtos.VisitDto
                {
                    VisitBillingSystemId = visitAgingHistoryDto.Visit.BillingSystemId,
                    VisitSourceSystemKey = visitAgingHistoryDto.Visit.SourceSystemKey
                },
                AgingTierEnum = (AgingTierEnum)visitAgingHistoryDto.AgingTier,
                ActionVisitPayUserId = visitAgingHistoryDto.VisitPayUser.VisitPayUserId
            });
        }

        #region exports

        public async Task<byte[]> ExportVisitsAsync(int visitPayUserId, VisitFilterDto filterDto, string userName, int? vpGuarantorId = null)
        {
            VisitResultsDto visitResults = this.GetVisits(visitPayUserId, filterDto, 1, iVinciExcelExportBuilder.MaxExportRecords, vpGuarantorId);
            IList<VisitDto> visits = Mapper.Map<IList<VisitDto>>(visitResults.Visits);

            IDictionary<string, string> textRegions = await this._contentService.Value.GetTextRegionsAsync(
                TextRegionConstants.BillingApplicationHBDisplay,
                TextRegionConstants.BillingApplicationPBDisplay,
                TextRegionConstants.CurrentBalance,
                TextRegionConstants.Description,
                TextRegionConstants.ExportedBy,
                TextRegionConstants.FileDate,
                TextRegionConstants.Inactive,
                TextRegionConstants.NoVisitsToDisplay,
                TextRegionConstants.Patient,
                TextRegionConstants.Status,
                TextRegionConstants.StatusDisplayActive,
                TextRegionConstants.TotalVisitAmount,
                TextRegionConstants.VisitDate,
                TextRegionConstants.VisitId,
                TextRegionConstants.VisitsPageHeading,
                TextRegionConstants.VisitType);

            System.Drawing.Image logo = await this._imageService.Value.GetExportLogoAsync();

            using (ExcelPackage package = new ExcelPackage())
            {
                iVinciExcelExportBuilder builder = new iVinciExcelExportBuilder(package, textRegions.GetValue(TextRegionConstants.VisitsPageHeading))
                    .WithLogo(logo)
                    .AddInfoBlockRow(new ColumnBuilder(DateTime.UtcNow.ToString(Format.MonthDayYearFormat), textRegions.GetValue(TextRegionConstants.FileDate)).WithStyle(StyleTypeEnum.AlignLeft))
                    .AddInfoBlockRow(new ColumnBuilder(userName, textRegions.GetValue(TextRegionConstants.ExportedBy)).WithStyle(StyleTypeEnum.AlignLeft))
                    .AddDataBlock(visits, visit =>
                    {
                        string billingApplication = string.Equals(visit.BillingApplication, BillingApplicationConstants.HB, StringComparison.CurrentCultureIgnoreCase) ? BillingApplicationConstants.HBDisplay : BillingApplicationConstants.PBDisplay;
                        string localizedBillingApplication = textRegions.GetValue(billingApplication);
                        string localizedVisitState = textRegions.GetValue(visit.CurrentVisitState.ToString());

                        return new List<Column>
                        {
                            new ColumnBuilder($"#{visit.SourceSystemKeyDisplay}", textRegions.GetValue(TextRegionConstants.VisitId)).AlignLeft(),
                            new ColumnBuilder(visit.VisitDisplayDate, textRegions.GetValue(TextRegionConstants.VisitDate)).WithDateFormat().AlignCenter(),
                            new ColumnBuilder(visit.PatientName, textRegions.GetValue(TextRegionConstants.Patient)),
                            new ColumnBuilder(visit.VisitDescription, textRegions.GetValue(TextRegionConstants.Description)),
                            new ColumnBuilder(localizedBillingApplication, textRegions.GetValue(TextRegionConstants.VisitType)),
                            new ColumnBuilder(localizedVisitState, textRegions.GetValue(TextRegionConstants.Status)),
                            new ColumnBuilder(visit.TotalCharges, textRegions.GetValue(TextRegionConstants.TotalVisitAmount)).WithCurrencyFormat(),
                            new ColumnBuilder(visit.UnclearedBalance, textRegions.GetValue(TextRegionConstants.CurrentBalance)).WithCurrencyFormat()
                        };
                    })
                    .WithNoRecordsFoundMessageOf(textRegions.GetValue(TextRegionConstants.NoVisitsToDisplay));

                builder.Build();

                return package.GetAsByteArray();
            }
        }

        public async Task<byte[]> ExportVisitsClientAsync(int visitPayUserId, VisitFilterDto filterDto, string userName)
        {
            int guarantorId = this._guarantorService.Value.GetGuarantorId(visitPayUserId);
            Guarantor guarantor = this._guarantorService.Value.GetGuarantor(guarantorId);
            VisitResultsDto visitResults = this.GetVisits(visitPayUserId, filterDto, 1, iVinciExcelExportBuilder.MaxExportRecords, guarantorId);

            System.Drawing.Image logo = await this._imageService.Value.GetExportLogoAsync();

            using (ExcelPackage package = new ExcelPackage())
            {
                iVinciExcelExportBuilder builder = new iVinciExcelExportBuilder(package, "Visits")
                    .WithWorkSheetNameOf($"{guarantor.User.FirstNameLastName} Visits")
                    .WithLogo(logo)
                    .AddInfoBlockRow(new ColumnBuilder(DateTime.UtcNow.ToString(Format.MonthDayYearFormat), "File Date:").WithStyle(StyleTypeEnum.AlignLeft))
                    .AddInfoBlockRow(new ColumnBuilder(userName, "Exported By:").WithStyle(StyleTypeEnum.AlignLeft))
                    .AddDataBlock(visitResults.Visits, visit =>
                    {
                        bool removed = visit.IsUnmatched;

                        List<Column> columns = new List<Column>
                        {
                            new ColumnBuilder($"#{visit.SourceSystemKeyDisplay}", "Visit #").AlignLeft(),
                            new ColumnBuilder(visit.GuarantorName, "Guarantor"),
                            new ColumnBuilder(visit.PatientName, "Patient"),
                            new ColumnBuilder(visit.VisitDisplayDate, "Visit Date").WithDateFormat("mm/dd/yyyy").AlignCenter(),
                            new ColumnBuilder(string.Equals(visit.BillingApplication, "HB", StringComparison.CurrentCultureIgnoreCase) ? "Hospital Services" : "Physician Services", "Visit Type")
                        };

                        if (removed)
                        {
                            columns.Add(new ColumnBuilder("-", "Total Visit Amount").AlignCenter());
                            columns.Add(new ColumnBuilder("-", "Curr. Balance").AlignCenter());
                            columns.Add(new ColumnBuilder("-", "Uncleared Payments").AlignCenter());
                            columns.Add(new ColumnBuilder("N/A", "Aging Count").AlignCenter());
                        }
                        else
                        {
                            columns.Add(new ColumnBuilder(visit.TotalCharges, "Total Visit Amount").WithCurrencyFormat());
                            columns.Add(new ColumnBuilder(visit.CurrentBalance, "Curr. Balance").WithCurrencyFormat());
                            columns.Add(new ColumnBuilder(visit.UnclearedPaymentsSum, "Uncleared Payments").WithCurrencyFormat());
                            columns.Add(new ColumnBuilder(visit.AgingCount, "Aging Count"));
                        }

                        return columns;
                    })
                    .WithNoRecordsFoundMessageOf("There are no visits to display at this time.");

                builder.Build();

                return package.GetAsByteArray();
            }
        }

        public int? GetRicCmsRegionId(int vpGuarantorId)
        {
            return this._visitService.Value.GetRicCmsRegionId(vpGuarantorId);
        }

        public CmsRegionEnum GetRicCmsRegionEnum(int vpGuarantorId)
        {
            return this._visitService.Value.GetRicCmsRegionEnum(vpGuarantorId);
        }

        public IList<FacilityDto> GetUniqueFacilityVisits(int vpGuarantorId)
        {
            IList<Facility> facilities =  this._visitService.Value.GetUniqueFacilityVisits(vpGuarantorId);
            IList<FacilityDto> facilitesDtos = Mapper.Map<List<FacilityDto>>(facilities);
            return facilitesDtos;
        }

        #endregion

        #region Message Consumers

        [UniversalConsumer(UniversalQueuesEnum.VisitPayProcessor, UniversalPriorityEnum.Priority10)]
        public void ConsumeMessage(VisitBalanceChangedMessage message, ConsumeContext<VisitBalanceChangedMessage> consumeContext)
        {
            this.ConsumeMessage(new VisitBalanceChangedMessageList { Messages = message.ToListOfOne() }, null);
        }

        public bool IsMessageValid(VisitBalanceChangedMessage message, ConsumeContext<VisitBalanceChangedMessage> consumeContext)
        {
            bool isValid = true;
            return isValid;
        }

        [UniversalConsumer(UniversalQueuesEnum.VisitPayProcessor, UniversalPriorityEnum.Priority10)]
        public void ConsumeMessage(VisitBalanceChangedMessageList message, ConsumeContext<VisitBalanceChangedMessageList> consumeContext)
        {
            IList<int> guarantorIds = message.Messages.Select(x => x.VpGuarantorId).Distinct().ToList();
            IList<int> visitIds = message.Messages.Select(x => x.VisitId).Distinct().ToList();

            foreach (int visitId in visitIds)
            {
                using (IUnitOfWork uow = new UnitOfWork(this._session))
                {
                    this._visitService.Value.UpdateTotalCharges(visitId);
                    uow.Commit();
                }
            }

            this._financePlanApplicationService.Value.CheckIfFinancePlanIsPastDueOrUncollectable(guarantorIds);
            this._financePlanApplicationService.Value.CheckIfFinancePlanIsValid(guarantorIds);
            this._financePlanApplicationService.Value.CheckIfReconfigurationIsStillValid(guarantorIds);
            this._financePlanApplicationService.Value.CheckIfPendingFinancePlansNeedToClose(visitIds);
            this._financePlanApplicationService.Value.CheckIfFinancePlanIsValid(visitIds);
            this._visitTransactionChangesApplicationService.Value.CreateVisitReconciliationBalanceChangeMessagesFromVisits(visitIds);
        }

        public bool IsMessageValid(VisitBalanceChangedMessageList message, ConsumeContext<VisitBalanceChangedMessageList> consumeContext)
        {
            bool isValid = true;
            return isValid;
        }

        [UniversalConsumer(UniversalQueuesEnum.VisitPayProcessor, UniversalPriorityEnum.Priority10)]
        public void ConsumeMessage(VisitChangedStatusAggregatedMessages message, ConsumeContext<VisitChangedStatusAggregatedMessages> consumeContext)
        {
            this._visitTransactionChangesApplicationService.Value.CreateVisitReconciliationBalanceChangeMessagesFromVisits(message.Messages.Select(x => x.VisitId).ToList());
        }

        public bool IsMessageValid(VisitChangedStatusAggregatedMessages message, ConsumeContext<VisitChangedStatusAggregatedMessages> consumeContext)
        {
            bool isValid = true;
            return isValid;
        }

        [UniversalConsumer(UniversalQueuesEnum.VisitPayProcessor, UniversalPriorityEnum.Priority10)]
        public void ConsumeMessage(VisitChangedStatusMessage message, ConsumeContext<VisitChangedStatusMessage> consumeContext)
        {
            this._visitTransactionChangesApplicationService.Value.CreateVisitReconciliationBalanceChangeMessagesFromVisits(message.VisitId.ToListOfOne());
        }

        public bool IsMessageValid(VisitChangedStatusMessage message, ConsumeContext<VisitChangedStatusMessage> consumeContext)
        {
            bool isValid = true;
            return isValid;
        }

        [UniversalConsumer(UniversalQueuesEnum.VisitPayProcessor, UniversalPriorityEnum.Priority30)]
        public void ConsumeMessage(VisitAgeChangedMessage message, ConsumeContext<VisitAgeChangedMessage> consumeContext)
        {
            this.UpdateVisitAgeTier(message.VisitSourceSystemKey, message.VisitBillingSystemId, message.AgingTier, message.AgingTierMessage, message.ActionVisitPayUserId);
        }

        public bool IsMessageValid(VisitAgeChangedMessage message, ConsumeContext<VisitAgeChangedMessage> consumeContext)
        {
            bool isValid = true;
            return isValid;
        }

        [UniversalConsumer(UniversalQueuesEnum.VisitPayProcessor, UniversalPriorityEnum.Priority10)]
        public void ConsumeMessage(VisitAgeChangedMessageHighPriority message, ConsumeContext<VisitAgeChangedMessageHighPriority> consumeContext)
        {
            this.UpdateVisitAgeTier(message.VisitSourceSystemKey, message.VisitBillingSystemId, message.AgingTier, message.AgingTierMessage, message.ActionVisitPayUserId);
        }

        public bool IsMessageValid(VisitAgeChangedMessageHighPriority message, ConsumeContext<VisitAgeChangedMessageHighPriority> consumeContext)
        {
            bool isValid = true;
            return isValid;
        }

        #endregion
    }
}
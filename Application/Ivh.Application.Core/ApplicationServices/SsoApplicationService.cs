﻿namespace Ivh.Application.Core.ApplicationServices
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Security.Claims;
    using System.Threading.Tasks;
    using System.Web;
    using AutoMapper;
    using Base.Common.Interfaces;
    using Base.Services;
    using Common.Dtos;
    using Common.Interfaces;
    using Domain.Core.Sso.Entities;
    using Domain.Core.Sso.Interfaces;
    using Domain.Core.SystemMessage.Interfaces;
    using Domain.FinanceManagement.Interfaces;
    using Domain.Guarantor.Entities;
    using Domain.Logging.Interfaces;
    using DotNetOpenAuth.Messaging;
    using DotNetOpenAuth.OAuth2;
    using FinanceManagement.Common.Dtos;
    using Ivh.Common.Base.Utilities.Extensions;
    using Ivh.Common.Base.Utilities.Helpers;
    using Ivh.Common.EventJournal;
    using Ivh.Common.ServiceBus.Attributes;
    using Ivh.Common.ServiceBus.Enums;
    using Ivh.Common.VisitPay.Constants;
    using Ivh.Common.VisitPay.Enums;
    using Ivh.Common.VisitPay.Messages.Core;
    using Ivh.Common.VisitPay.Strings;
    using Ivh.Common.Web.Cookies;
    using Ivh.Domain.Guarantor.Interfaces;
    using MassTransit;
    using User.Common.Dtos;

    public class SsoApplicationService : ApplicationService, ISsoApplicationService
    {
        private readonly Lazy<IMetricsProvider> _metricsProvider;
        private readonly Lazy<IGuarantorApplicationService> _guarantorApplicationService;

        private readonly Lazy<IGuarantorService> _guarantorService;
        private readonly Lazy<IHealthEquityEmployerIdResolutionService> _healthEquityEmployerIdResolutionService;
        private readonly Lazy<IHsaBalanceProvider> _hsaBalanceProvider;
        private readonly Lazy<ISamlService> _samlService;
        private readonly Lazy<ISsoService> _ssoService;
        private readonly Lazy<ISsoTokenProvider> _ssoTokenProvider;
        private readonly Lazy<ISystemMessageVisitPayUserService> _systemMessageVisitPayUserService;
        private readonly Lazy<IVisitPayUserApplicationService> _visitPayUserApplicationService;
        private readonly Lazy<IVisitPayUserJournalEventApplicationService> _visitPayUserJournalEventApplicationService;
        private readonly Lazy<IWebCookieFacade> _webCookie;

        public SsoApplicationService(
            Lazy<IApplicationServiceCommonService> applicationServiceCommonService,
            Lazy<ISsoService> ssoService,
            Lazy<IVisitPayUserApplicationService> visitPayUserApplicationService,
            Lazy<IVisitPayUserJournalEventApplicationService> visitPayUserJournalEventApplicationService,
            Lazy<IGuarantorService> guarantorService,
            Lazy<IWebCookieFacade> webCookie,
            Lazy<IMetricsProvider> metricsProvider,
            Lazy<ISamlService> samlService,
            Lazy<IHsaBalanceProvider> hsaBalanceProvider,
            Lazy<IHealthEquityEmployerIdResolutionService> healthEquityEmployerIdResolutionService,
            Lazy<IGuarantorApplicationService> guarantorApplicationService,
            Lazy<ISsoTokenProvider> ssoTokenProvider,
            Lazy<ISystemMessageVisitPayUserService> systemMessageVisitPayUserService) : base(applicationServiceCommonService)
        {
            this._ssoService = ssoService;
            this._visitPayUserApplicationService = visitPayUserApplicationService;
            this._visitPayUserJournalEventApplicationService = visitPayUserJournalEventApplicationService;
            this._guarantorService = guarantorService;
            this._webCookie = webCookie;
            this._metricsProvider = metricsProvider;
            this._samlService = samlService;
            this._hsaBalanceProvider = hsaBalanceProvider;
            this._healthEquityEmployerIdResolutionService = healthEquityEmployerIdResolutionService;
            this._guarantorApplicationService = guarantorApplicationService;
            this._ssoTokenProvider = ssoTokenProvider;
            this._systemMessageVisitPayUserService = systemMessageVisitPayUserService;
        }

        public bool HasUserAcceptedSsoTerms(SsoProviderEnum provider, int visitPayUserId)
        {
            SsoVisitPayUserSetting result = this._ssoService.Value.GetUserSettings(provider, visitPayUserId);
            return result?.SsoStatus == SsoStatusEnum.Accepted;
        }

        public bool HasUserIgnoredSso(SsoProviderEnum provider, int visitPayUserId)
        {
            SsoVisitPayUserSetting result = this._ssoService.Value.GetUserSettings(provider, visitPayUserId);
            if (result == null)
            {
                return false;
            }
            return result.IsIgnored();
        }

        public async Task<SignInStatusExEnum> PasswordSignInAsync(HttpContextDto context, SsoPasswordSignInRequest signInRequest)
        {
            Func<SignInStatusExEnum, int, HttpContextDto, IList<Claim>, Task> afterSuccessAction = null;
            if (this.FeatureService.Value.IsFeatureEnabled(VisitPayFeatureEnum.MyChartSso))
            {
                if (signInRequest.SsoResponse != null)
                {
                    afterSuccessAction = this.AddSsoClaimAsync;
                }
            }
            SignInStatusExEnum identityResult = await this._visitPayUserApplicationService.Value.PasswordSignInAsync(context, signInRequest.UserName, signInRequest.Password, false, true, VisitPayRoleStrings.System.Patient, afterSuccessAction);
            return identityResult;
        }

        public async Task<SignInStatusExEnum> ReLoginAsync(HttpContextDto context, string userId)
        {
            Func<SignInStatusExEnum, int, HttpContextDto, IList<Claim>, Task> afterSuccessAction = null;
            if (this.FeatureService.Value.IsFeatureEnabled(VisitPayFeatureEnum.MyChartSso))
            {
                afterSuccessAction = this.AddSsoClaimAsync;
            }

            return await this._visitPayUserApplicationService.Value.ReLoginAsync(userId, context, afterSuccessAction);
        }

        public async Task<SsoResponseDto> SignInWithSso(HttpContextDto context, SsoRequestDto request)
        {
            SsoResponseDto response = this._ssoService.Value.SignInWithSso(request);
            //In the documentation it says(VPNG-10603): If the initial SSKID (WPRID) check fails to match to a valid HSGID - Which i'm interpreting as not verified, there arent any assigned HSGs(FoundMatchesThatCouldBeRegistered), there wasnt any VPG's that were already registered
            if (!response.VpGuarantorId.HasValue && !response.AcceptedTheSsoAgreement && !response.FoundMatchesThatCouldBeRegistered && !response.FoundRegisteredMatches)
            {
                this._visitPayUserJournalEventApplicationService.Value.LogSsoRequestFailed(null, response.ProviderEnum, response.SsoSourceSystemKey, response.DateOfBirth, Mapper.Map<JournalEventHttpContextDto>(context));
            }

            //In the documentation it says(VPNG-10603):If the SSKID (WPRID) Matches to one or more valid HSGID's without a registered VPGID - Which i'm interpreting as matches that could be registered but no current VPGs that could verify 
            if (!response.VpGuarantorId.HasValue && !response.AcceptedTheSsoAgreement && response.FoundMatchesThatCouldBeRegistered && !response.FoundRegisteredMatches)
            {
                this._visitPayUserJournalEventApplicationService.Value.LogSsoMatchedNotRegistered(response.ProviderEnum, response.MatchesThatCouldBeRegistered.Select(x => x.HsGuarantorId).ToList(), response.SsoSourceSystemKey, response.DateOfBirth, Mapper.Map<JournalEventHttpContextDto>(context));
            }

            //
            switch (response.ErrorEnum)
            {
                case SsoResponseErrorEnum.FailedToDecrypt:
                    this._metricsProvider.Value.Increment(Metrics.Increment.Sso.FailedSso.Decryption);
                    break;
                case SsoResponseErrorEnum.FailedToVerifyTimeStamp:
                    this._metricsProvider.Value.Increment(Metrics.Increment.Sso.FailedSso.TimeStamp);
                    break;
                case SsoResponseErrorEnum.MissingFields:
                    this._metricsProvider.Value.Increment(Metrics.Increment.Sso.FailedSso.MissingFields);
                    break;
            }

            //
            response.SignInStatus = SignInStatusExEnum.Failure;

            if (!response.VpGuarantorId.HasValue)
            {
                return response;
            }

            Guarantor guarantor = this._guarantorService.Value.GetGuarantor(response.VpGuarantorId.Value);
            if (guarantor == null)
            {
                return response;
            }

            if (guarantor.IsClosed)
            {
                response.SignInStatus = SignInStatusExEnum.AccountClosed;
                return response;
            }

            if (!response.AcceptedTheSsoAgreement || guarantor.User == null)
            {
                return response;
            }

            this._metricsProvider.Value.Increment(Metrics.Increment.Sso.Verified);
            this._visitPayUserJournalEventApplicationService.Value.LogSsoSuccessfulLogin(guarantor.User.VisitPayUserId, guarantor.VpGuarantorId, response.ProviderEnum, response.SsoSourceSystemKey, response.DateOfBirth, Mapper.Map<JournalEventHttpContextDto>(context));
            response.SignInStatus = await this._visitPayUserApplicationService.Value.ReLoginAsync(guarantor.User.VisitPayUserId.ToString(), context);
            response.UserName = guarantor.User.UserName;

            return response;
        }

        public string GenerateEncryptedSsoToken(int vpGuarantorId, SsoProviderEnum providerEnum, string ssoSsk, DateTime dob)
        {
            Guarantor guarantor = this._guarantorService.Value.GetGuarantor(vpGuarantorId);
            return HttpUtility.UrlEncode(this._ssoService.Value.GenerateEncryptedSsoToken(guarantor.User.FirstName, guarantor.User.LastName, providerEnum, ssoSsk, dob));
        }

        public SsoProviderDto GetProvider(SsoProviderEnum ssoProvider)
        {
            return Mapper.Map<SsoProviderDto>(this._ssoService.Value.GetProvider(ssoProvider));
        }

        public SsoProviderDto GetProviderBySourceSystemKey(string sourceSystemKey)
        {
            SsoProvider ssoProvider = this._ssoService.Value.GetProviderBySourceSystemKey(sourceSystemKey);

            if (ssoProvider != null)
            {
                SsoProviderDto ssoProviderDto = Mapper.Map<SsoProviderDto>(ssoProvider);
                ssoProviderDto.SsoProviderSecret = this.ApplicationSettingsService.Value.GetString(ssoProvider.SsoProviderSecretSettingsKey).Value;

                return ssoProviderDto;
            }

            return null;
        }

        public IList<SsoProviderWithUserSettingsDto> GetAllProvidersWithUserSettings(int visitPayUserId)
        {
            int vpGuarantorId = this._guarantorService.Value.GetGuarantorId(visitPayUserId);

            IList<SsoProvider> providers = this._ssoService.Value.GetProviders().Where(x => x.IsEnabled).ToList();
            IList<SsoProviderWithUserSettingsDto> providersWithSettingsDto = Mapper.Map<IList<SsoProviderWithUserSettingsDto>>(providers);

            IList<SsoVisitPayUserSetting> userSettings = this._ssoService.Value.GetUserSettings(visitPayUserId);
            foreach (SsoProviderWithUserSettingsDto providerWithSettingsDto in providersWithSettingsDto)
            {
                if (this.ApplicationSettingsService.Value.IsClientApplication.Value)
                {
                    IList<SsoVerification> verifications = this._ssoService.Value.GetVerificationsForVpGuarantor((SsoProviderEnum)providerWithSettingsDto.Provider.SsoProviderId, vpGuarantorId);
                    providerWithSettingsDto.SourceSystemKey = verifications.Select(x => x.SingleSignOnSourceSystemKey).ToList();
                }

                SsoVisitPayUserSetting userSetting = userSettings.FirstOrDefault(x => x.SsoProvider.SsoProviderId == providerWithSettingsDto.Provider.SsoProviderId);
                if (userSetting == null)
                {
                    continue;
                }

                providerWithSettingsDto.UserSettings = Mapper.Map<SsoVisitPayUserSettingDto>(userSetting);
            }

            return providersWithSettingsDto.OrderBy(x => x.Provider.SsoProviderName).ToList();
        }

        public SsoStatusEnum? GetUserSsoStatus(SsoProviderEnum provider, int visitPayUserId)
        {
            SsoVisitPayUserSetting result = this._ssoService.Value.GetUserSettings(provider, visitPayUserId);
            return result?.SsoStatus;
        }

        public SsoVisitPayUserSettingDto AcceptSso(HttpContextDto context, SsoProviderEnum ssoProvider, int visitPayUserId, int? actionTakenByVisitPayUserId = null)
        {
            SsoVisitPayUserSetting userSetting = this._ssoService.Value.AcceptSso(ssoProvider, visitPayUserId, actionTakenByVisitPayUserId);
            int vpGuarantorId = this._visitPayUserApplicationService.Value.GetGuarantorIdFromVisitPayUserId(userSetting.VisitPayUser.VisitPayUserId);
            this._visitPayUserJournalEventApplicationService.Value.LogSsoEnabled(actionTakenByVisitPayUserId ?? visitPayUserId, vpGuarantorId, (SsoProviderEnum)userSetting.SsoProvider.SsoProviderId, Mapper.Map<JournalEventHttpContextDto>(context), null, null);
            this.RemoveSsoClaim();

            if (ssoProvider == SsoProviderEnum.HealthEquityOutbound)
            {
                //now check to see if they have an HSA
                HealthEquityBalanceDto healthEquityBalanceDto = Task.Run(async () => await this._guarantorApplicationService.Value.GetGuarantorHealthEquityBalanceAsnyc(vpGuarantorId, visitPayUserId, ssoProvider, context)).Result;
                if (healthEquityBalanceDto == null) // unable to verify that user 
                {
                    this.InvalidateSso(context, visitPayUserId, vpGuarantorId);
                    this.IgnoreSso(ssoProvider, visitPayUserId, true);
                    userSetting = this._ssoService.Value.AcknowledgeInvalidatedSso(ssoProvider, visitPayUserId);
                }
            }

            this._systemMessageVisitPayUserService.Value.HideMessage(visitPayUserId, (int)SystemMessageEnum.Hqy);

            return Mapper.Map<SsoVisitPayUserSettingDto>(userSetting);
        }

        public SsoVisitPayUserSettingDto DeclineSso(HttpContextDto context, SsoProviderEnum ssoProvider, int visitPayUserId, int? actionTakenByVisitPayUserId = null)
        {
            int vpGuarantorId = this._visitPayUserApplicationService.Value.GetGuarantorIdFromVisitPayUserId(visitPayUserId);
            bool hasAlreadyAccepted = this.HasUserAcceptedSsoTerms(ssoProvider, visitPayUserId);
            string ssoSsk = "";
            if (ssoProvider == SsoProviderEnum.OpenEpic)
            {
                IList<SsoVerification> verifications = this._ssoService.Value.GetVerificationsForVpGuarantor(ssoProvider, vpGuarantorId);
                if (verifications != null && verifications.Count > 0)
                {
                    ssoSsk = string.Join(",", verifications.Select(x => x.SingleSignOnSourceSystemKey).ToList());
                }
            }

            SsoVisitPayUserSetting userSetting = this._ssoService.Value.DeclineSso(ssoProvider, visitPayUserId, actionTakenByVisitPayUserId);

            if (hasAlreadyAccepted)
            {
                this._visitPayUserJournalEventApplicationService.Value.LogSsoDisabled(actionTakenByVisitPayUserId, vpGuarantorId, (SsoProviderEnum)userSetting.SsoProvider.SsoProviderId, Mapper.Map<JournalEventHttpContextDto>(context), ssoSsk, userSetting.VisitPayUser.DateOfBirth);
            }
            else
            {
                this._visitPayUserJournalEventApplicationService.Value.LogSsoDecline(actionTakenByVisitPayUserId, vpGuarantorId, (SsoProviderEnum)userSetting.SsoProvider.SsoProviderId, Mapper.Map<JournalEventHttpContextDto>(context), ssoSsk, userSetting.VisitPayUser.DateOfBirth);
            }
            this.RemoveSsoClaim();

            return Mapper.Map<SsoVisitPayUserSettingDto>(userSetting);
        }

        public void DeclineAllAccceptedSso(HttpContextDto context, int visitPayUserId, int? actionTakenByVisitPayUserId = null)
        {
            foreach (SsoVisitPayUserSetting ssoVisitPayUserSetting in this._ssoService.Value.GetUserSettings(visitPayUserId).Where(x => x.SsoStatus == SsoStatusEnum.Accepted))
            {
                this.DeclineSso(context, (SsoProviderEnum)ssoVisitPayUserSetting.SsoProvider.SsoProviderId, visitPayUserId, actionTakenByVisitPayUserId);
            }
        }

        public SsoVisitPayUserSettingDto IgnoreSso(SsoProviderEnum ssoProvider, int visitPayUserId, bool ignore, int? actionTakeByVisitPayUserId = null)
        {
            SsoVisitPayUserSetting userSetting = this._ssoService.Value.IgnoreSso(ssoProvider, visitPayUserId, ignore, actionTakeByVisitPayUserId);

            return userSetting == null ? null : Mapper.Map<SsoVisitPayUserSettingDto>(userSetting);
        }

        public void InvalidateSso(HttpContextDto context, int visitPayUserId, int vpGuarantorId)
        {
            if (this.HasUserAcceptedSsoTerms(SsoProviderEnum.HealthEquityOutbound, visitPayUserId))
            {
                this._ssoService.Value.InvalidateSso(SsoProviderEnum.HealthEquityOutbound, visitPayUserId);
                this._visitPayUserJournalEventApplicationService.Value.LogSsoInvalidate(SystemUsers.SystemUserId, vpGuarantorId, SsoProviderEnum.HealthEquityOutbound, Mapper.Map<JournalEventHttpContextDto>(context));
            }
        }

        public void AttemptedSso(HttpContextDto context, int visitPayUserId, int? actionTakenByVisitPayUserId = null)
        {
            SsoVisitPayUserSetting userSetting = this._ssoService.Value.GetUserSettings(SsoProviderEnum.HealthEquityOutbound, visitPayUserId);
            int vpGuarantorId = this._visitPayUserApplicationService.Value.GetGuarantorIdFromVisitPayUserId(userSetting.VisitPayUser.VisitPayUserId);
            this._visitPayUserJournalEventApplicationService.Value.LogSsoAttempt(actionTakenByVisitPayUserId, vpGuarantorId, SsoProviderEnum.HealthEquityOutbound, Mapper.Map<JournalEventHttpContextDto>(context));
        }

        public Ivh.Common.Base.Utilities.Responses.Response<SamlResponseDto> GetSamlResponse(int visitPayUserId, SamlResponseTargetEnum samlResponseTarget, string samlRequest = null, string relayState = null)
        {
            if (this.ApplicationSettingsService.Value.HealthEquitySsoSamlRequireHsa.Value)
            {
                int vpGuarantorId = this._guarantorService.Value.GetGuarantorId(visitPayUserId);
                string ssn = this._guarantorApplicationService.Value.GetSsn(vpGuarantorId);
                if (ssn.IsNotNullOrEmpty())
                {
                    string employerId = this._healthEquityEmployerIdResolutionService.Value.GetHealthEquityEmployerId(vpGuarantorId);
                    if (employerId.IsNotNullOrEmpty())
                    {
                        HealthEquityBalanceResponse healthEquityBalanceResponse = this._hsaBalanceProvider.Value.GetBalanceForSsnAsync(ssn, employerId).Result;
                        if (healthEquityBalanceResponse == null || !healthEquityBalanceResponse.WasSuccessful)
                        {
                            return new Ivh.Common.Base.Utilities.Responses.Response<SamlResponseDto>(null, false, SsoMessages.FailureNoHsa);
                        }
                    }
                    else
                    {
                        return new Ivh.Common.Base.Utilities.Responses.Response<SamlResponseDto>(null, false, SsoMessages.FailureNoHsa);
                    }
                }
                else
                {
                    return new Ivh.Common.Base.Utilities.Responses.Response<SamlResponseDto>(null, false, SsoMessages.FailureSsn);
                }
            }

            Ivh.Common.Base.Utilities.Responses.Response<SamlResponse> samlResponse = this._samlService.Value.GetSamlResponse(visitPayUserId, samlResponseTarget, samlRequest, relayState);
            return samlResponse == null ? null : Mapper.Map<Ivh.Common.Base.Utilities.Responses.Response<SamlResponseDto>>(samlResponse);
        }

        public SsoVisitPayUserSettingDto AcknowledgeInvalidatedSso(SsoProviderEnum ssoProvider, int visitPayUserId, int? actionTakeByVisitPayUserId = null)
        {
            SsoVisitPayUserSetting userSetting = this._ssoService.Value.AcknowledgeInvalidatedSso(ssoProvider, visitPayUserId, actionTakeByVisitPayUserId);

            return Mapper.Map<SsoVisitPayUserSettingDto>(userSetting);
        }

        public bool IsUserAuthorized(SsoProviderEnum ssoProviderEnum, HttpContextDto context = null)
        {
            HashSet<SsoProviderEnum> healthEquityProviderEnums = new HashSet<SsoProviderEnum> { SsoProviderEnum.HealthEquityOAuthGrant, SsoProviderEnum.HealthEquityOutbound, SsoProviderEnum.HealthEquityOAuthImplicit };

            if (healthEquityProviderEnums.Contains(ssoProviderEnum))
            {
                ClaimsIdentity identity = new ClaimsIdentity(context == null ? HttpContext.Current.User.Identity : context.User.Identity);
                Claim claim = identity.FindFirst(ClaimTypeEnum.HealthEquityOutbound.ToString());
                bool value = false;
                return claim != null && bool.TryParse(claim.Value, out value) && value;
            }
            return true;
        }


        public void SetupOAuth(SsoProviderEnum oAuthProvider, int currentUserId, HttpRequestBase request, HttpResponseBase response, HttpContextBase context)
        {
            SsoProvider provider = this._ssoService.Value.GetProvider(oAuthProvider);
            if (provider == null || !provider.IsEnabled)
            {
                return;
            }

            this.LoggingService.Value.Info(() => $"SsoApplicationService::SetupOAuth - CurrentUserId: {currentUserId} Provider: {provider.SsoProviderEnum}");

            WebServerClient webServerClient = this.CreateOAuthWebServerClient(provider);
            string authorizationStateAccessToken = null;
            string authorizationStateRefreshToken = null;
            IAuthorizationState authorizationState = null;
            try
            {
                authorizationState = webServerClient.ProcessUserAuthorization(request);
                if (authorizationState != null)
                {
                    authorizationStateAccessToken = authorizationState.AccessToken;
                    authorizationStateRefreshToken = authorizationState.RefreshToken;
                }
            }
            catch (Exception e)
            {
                //This is an error case, bad state back from the OAuth vendor
                //In iVinci's case our authorization server just sets a bogus identity and forces an error when the user declines
                this.LoggingService.Value.Info(() => $"SsoApplicationService::SetupOAuth - something happened, Authorization State is not correct. CurrentUserId: {currentUserId} Provider: {provider.SsoProviderEnum} Exception: {ExceptionHelper.AggregateExceptionToString(e)}");
                return;
            }
            if (authorizationStateAccessToken != null && authorizationStateRefreshToken != null)
            {
                //they are necessarly accepting the sso, probably alrealy accepts before coming to this point.
                //Update the user setting
                //Basically the differece here is that we'll only get an access token(implicit) vs access and refresh (Grant)
                this._ssoService.Value.UpdateSsoVisitPayUserSettingWithOAuthInfo(oAuthProvider, currentUserId, authorizationStateAccessToken, authorizationStateRefreshToken, provider.Scope);
                return;
            }
            //Scope to request
            try
            {
                string[] scopeSplit = provider.Scope?.Split(',');
                OutgoingWebResponse userAuthorization = webServerClient.PrepareRequestUserAuthorization(scopeSplit ?? new string[] { });
                userAuthorization.Send(context);
                response.End();
            }
            catch (Exception e)
            {
                this.LoggingService.Value.Info(() => $"SsoApplicationService::SetupOAuth - something happened, Tried to send oAuth request and something failed. CurrentUserId: {currentUserId} Provider: {provider.SsoProviderEnum} Exception: {ExceptionHelper.AggregateExceptionToString(e)}");
            }
        }

        public IList<SsoProviderDto> GetEnabledProviders(IList<SsoProviderEnum> ssoProviderEnums)
        {
            return Mapper.Map<IList<SsoProviderDto>>(this._ssoService.Value.GetProviders().Where(x => x.IsEnabled && ssoProviderEnums.Contains(x.SsoProviderEnum)).ToList());
        }

        /// <summary>
        ///     This method will return the SsoProviderDto after checking the endpoint is up, will return null if the endpoint is
        ///     not up, will also return null if the provider is not enabled.
        /// </summary>
        public async Task<SsoProviderDto> GetActiveProviderAsync(SsoProviderEnum providerEnum)
        {
            SsoProvider provider = this._ssoService.Value.GetProvider(providerEnum);

            //skip ping for mock endpoint
            bool pingProviderEndpoint = providerEnum != SsoProviderEnum.HealthEquityOAuthImplicit;

            if (provider == null || !provider.IsEnabled)
            {
                return null;
            }

            if (pingProviderEndpoint)
            {
                bool providerActive = await this._ssoService.Value.CheckIfProviderIsActiveAsync(provider);
                return providerActive ? Mapper.Map<SsoProviderDto>(provider) : null;
            }

            return Mapper.Map<SsoProviderDto>(provider);
        }

        public SsoProviderEnum SelectHealthEquitySsoProvider()
        {
            IList<SsoProviderDto> enabledProviders = this.GetEnabledProviders(new List<SsoProviderEnum> { SsoProviderEnum.HealthEquityOutbound, SsoProviderEnum.HealthEquityOAuthImplicit, SsoProviderEnum.HealthEquityOAuthGrant });
            SsoProviderEnum selectHealthEquitySsoProvider = this.SelectHealthEquitySsoProvider(enabledProviders);
            return selectHealthEquitySsoProvider;
        }

        public async Task<SsoCanRedirectToSsoResult> RedirectToSso(SsoProviderEnum target, int currentUserId, HttpContextDto context)
        {
            SsoCanRedirectToSsoResult result = await this.CanRedirectToSso(target, currentUserId);
            if (result.CanRedirect)
            {
                SsoProvider provider = this._ssoService.Value.GetProvider(target);
                if (provider == null)
                {
                    result.ErrorMessage = "Couldnt find the Sso Provider.";
                    return result;
                }
                if (!provider.IsEnabled)
                {
                    result.ErrorMessage = "The Sso Provider isn't Enabled.";
                    return result;
                }

                SsoProviderWithUserSettingsDto userResult = this.GetAllProvidersWithUserSettings(currentUserId).FirstOrDefault(x => x.Provider != null && x.Provider.SsoProviderEnum == target);
                if (userResult == null)
                {
                    result.ErrorMessage = "You havn't agreed to terms.";
                    return result;
                }
                HealthEquitySsoTokenResponse ssoToken = await this._ssoTokenProvider.Value.GetSsoToken(userResult.UserSettings.AccessToken, Mapper.Map<SsoProvider>(userResult.Provider));
                if (ssoToken != null && ssoToken.AuthTokenExpired.HasValue && ssoToken.AuthTokenExpired.Value
                    && this.RefreshOAuthAccessTokenWithRefreshToken(userResult))
                {
                    userResult = this.GetAllProvidersWithUserSettings(currentUserId).FirstOrDefault(x => x.Provider != null && x.Provider.SsoProviderEnum == target);
                    ssoToken = await this._ssoTokenProvider.Value.GetSsoToken(userResult.UserSettings.AccessToken, Mapper.Map<SsoProvider>(userResult.Provider));
                }
                if (ssoToken == null || !ssoToken.WasSuccessful)
                {
                    result.ErrorMessage = "Couldn't retrieve SSO Token.";
                    return result;
                }

                int vpGuarantorId = this._visitPayUserApplicationService.Value.GetGuarantorIdFromVisitPayUserId(currentUserId);
                this._visitPayUserJournalEventApplicationService.Value.LogSsoAttempt(currentUserId, vpGuarantorId, target, Mapper.Map<JournalEventHttpContextDto>(context));
                Uri baseUri = new Uri(this.ApplicationSettingsService.Value.HealthEquityOAuthResourceServerBaseAddress.Value);
                Uri myUri = new Uri(baseUri, string.Format(this.ApplicationSettingsService.Value.HealthEquityOAuthSsoPath.Value));
                result.UrlToRedirect = myUri.ToString();
                result.AccessToken = ssoToken.SsoToken;
            }
            return result;
        }

        public bool RefreshOAuthAccessTokenWithRefreshToken(SsoProviderWithUserSettingsDto settingForProvider)
        {
            SsoVisitPayUserSetting connected = this._ssoService.Value.GetUserSettings(settingForProvider.Provider.SsoProviderEnum, settingForProvider.UserSettings.VisitPayUser.VisitPayUserId);

            this._ssoService.Value.RemoveAccessToken(connected);

            if (string.IsNullOrWhiteSpace(connected?.RefreshToken))
            {
                return false;
            }

            WebServerClient webServerClient = this.CreateOAuthWebServerClient(connected.SsoProvider);
            AuthorizationState authorizationState = new AuthorizationState { RefreshToken = connected.RefreshToken };
            try
            {
                if (webServerClient.RefreshAuthorization(authorizationState))
                {
                    this._ssoService.Value.UpdateSsoVisitPayUserSettingWithOAuthInfo(connected.SsoProvider.SsoProviderEnum, settingForProvider.UserSettings.VisitPayUser.VisitPayUserId, authorizationState.AccessToken, authorizationState.RefreshToken, settingForProvider.UserSettings.Scope);
                    return true;
                }
            }
            catch (Exception e)
            {
                //this counts as a failure so we'll just log and let it drop out
                this.LoggingService.Value.Info(() => $"SsoApplicationService::RefreshOAuthAccessTokenWithRefreshToken - something happened - Perhaps access token was revoked, Authorization State is not correct. CurrentUserId: {settingForProvider.UserSettings.VisitPayUser.VisitPayUserId} Provider: {settingForProvider.Provider.SsoProviderEnum} Exception: {ExceptionHelper.AggregateExceptionToString(e)}");
            }
            this._ssoService.Value.IncrementRefreshTokenFailureCount(connected);

            //Seems like we should start a failed to refresh count here.  As the RefreshToken can also expire.
            return false;
        }

        private async Task AddSsoClaimAsync(SignInStatusExEnum result, int visitPayUserId, HttpContextDto context, IList<Claim> claims)
        {
            SsoResponseDto ssoResponse = await this._webCookie.Value.GetSsoResponseDtoAsync(HttpContext.Current);
            if (result == SignInStatusExEnum.Success && ssoResponse != null)
            {
                Guarantor guarantor = this._guarantorService.Value.GetGuarantorEx(visitPayUserId);
                IList<SsoVerification> verifications = this._ssoService.Value.GetVerificationsForVpGuarantor(ssoResponse.ProviderEnum, guarantor.VpGuarantorId);

                if (this._ssoService.Value.AddVpGuarantorVerification(guarantor.VpGuarantorId, ssoResponse.SsoSourceSystemKey, ssoResponse.DateOfBirth, ssoResponse.ProviderEnum))
                {
                    SsoVisitPayUserSetting settings = this._ssoService.Value.GetUserSettings(ssoResponse.ProviderEnum, visitPayUserId);

                    if (settings == null || settings.SsoStatus == SsoStatusEnum.Rejected && !settings.IsIgnored())
                    {
                        claims.Add(new Claim(ClaimTypeEnum.RequiresSsoAuthorization.ToString(), "true"));
                        claims.Add(new Claim(ClaimTypeEnum.SsoProvider.ToString(), ((int)ssoResponse.ProviderEnum).ToString()));
                    }
                }
                else
                {
                    //If the user logs into a VPGID that does not match to the passed SSKID (WPRID) (VPNG-10603)
                    this._visitPayUserJournalEventApplicationService.Value.LogSsoVerificationFailed(
                        actionTakenByVisitPayUserId: guarantor.User.VisitPayUserId,
                        vpGuarantorId: guarantor.VpGuarantorId,
                        ssoProvider: ssoResponse.ProviderEnum,
                        ssoSsk: ssoResponse.SsoSourceSystemKey,
                        dob: ssoResponse.DateOfBirth,
                        context: Mapper.Map<JournalEventHttpContextDto>(context)
                        );
                }

                IList<SsoVerification> afterVerifications = this._ssoService.Value.GetVerificationsForVpGuarantor(ssoResponse.ProviderEnum, guarantor.VpGuarantorId);
                this.LogNewVerificationsAndSupplantations(afterVerifications, verifications);

                await this._webCookie.Value.SetSsoResponseDtoAsync(HttpContext.Current, null);
            }
        }

        private void LogNewVerificationsAndSupplantations(IList<SsoVerification> afterVerifications, IList<SsoVerification> verifications)
        {
            //verifications that are no longer in after
            int numberOfSupplantations = 0;
            HashSet<int> afterVerificationsHashSet = afterVerifications.Select(x => x.SingleSignOnVerificationId).ToHashSet();
            foreach (SsoVerification ssoVerification in verifications)
            {
                if (!afterVerificationsHashSet.Contains(ssoVerification.SingleSignOnVerificationId))
                {
                    numberOfSupplantations++;
                }
            }
            if (numberOfSupplantations > 0)
            {
                this._metricsProvider.Value.Increment(Metrics.Increment.Sso.NewSso.Supplantation, numberOfSupplantations);
            }

            int numberOfAdditionalVerifications = 0;
            HashSet<int> beforeVerificationsHashSet = verifications.Select(x => x.SingleSignOnVerificationId).ToHashSet();
            //new entries in after should cause counts in 
            foreach (SsoVerification afterVerification in afterVerifications)
            {
                if (!beforeVerificationsHashSet.Contains(afterVerification.SingleSignOnVerificationId))
                {
                    numberOfAdditionalVerifications++;
                }
            }
            if (numberOfAdditionalVerifications > 0)
            {
                this._metricsProvider.Value.Increment(Metrics.Increment.Sso.NewSso.Verification, numberOfAdditionalVerifications);
            }
        }

        private void RemoveSsoClaim()
        {
            this._visitPayUserApplicationService.Value.RemoveClaim(new[] { ClaimTypeEnum.RequiresSsoAuthorization.ToString(), ClaimTypeEnum.SsoProvider.ToString() });
        }

        private WebServerClient CreateOAuthWebServerClient(SsoProvider provider)
        {
            //Should probably move this HealthEquityOAuthAuthorizationServerBaseAddress onto the provider of the sso table
            Uri authorizationServerUri = new Uri(provider.AuthorizationServerBaseAddress);
            AuthorizationServerDescription authorizationServer = new AuthorizationServerDescription
            {
                AuthorizationEndpoint = new Uri(authorizationServerUri, provider.AuthorizePath),
                TokenEndpoint = new Uri(authorizationServerUri, provider.TokenPath)
            };

            string ssoProviderSecret = this.ApplicationSettingsService.Value.GetString(provider.SsoProviderSecretSettingsKey).Value;
            WebServerClient webServerClient = new WebServerClient(authorizationServer, provider.SsoProviderSourceSystemKey, ssoProviderSecret);
            return webServerClient;
        }

        private async Task<SsoCanRedirectToSsoResult> CanRedirectToSso(SsoProviderEnum target, int currentUserId)
        {
            SsoCanRedirectToSsoResult model = new SsoCanRedirectToSsoResult
            {
                CanRedirect = false,
                HasAcceptedTerms = false
            };
            if (this.HasUserAcceptedSsoTerms(target, currentUserId))
            {
                model.HasAcceptedTerms = true;
            }

            if (model.HasAcceptedTerms)
            {
                SsoProviderDto result = await this.GetActiveProviderAsync(target);
                if (result == null || !result.IsEnabled)
                {
                    model.ErrorMessage = $"This Sso Provider seems to be down at the moment.  Please Try again later.";
                }
                else
                {
                    model.CanRedirect = true;
                }
            }

            return model;
        }

        private SsoProviderEnum SelectHealthEquitySsoProvider(IList<SsoProviderDto> enabledProviders)
        {
            if (enabledProviders.IsNullOrEmpty())
            {
                return SsoProviderEnum.Unknown;
            }
            return enabledProviders.OrderByDescending(x => x.SsoProviderId).First().SsoProviderEnum;
        }

        #region Message Consumers

        [UniversalConsumer(UniversalQueuesEnum.VisitPayProcessor, UniversalPriorityEnum.Priority30)]
        public void ConsumeMessage(SsoInvalidateMessage message, ConsumeContext<SsoInvalidateMessage> consumeContext)
        {
            this.InvalidateSso(null, message.VisitPayUserId, message.VpGuarantorId);
        }

        public bool IsMessageValid(SsoInvalidateMessage message, ConsumeContext<SsoInvalidateMessage> consumeContext)
        {
            bool isValid = true;
            return isValid;
        }

        #endregion
    }
}
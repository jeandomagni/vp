﻿
namespace Ivh.Application.Core
{
    using System.Collections.Generic;
    using System.Linq;
    using Common.Interfaces;
    using Domain.Guarantor.Entities;
    using Domain.Visit.Interfaces;

    public class VisitPayUserIssueResultApplicationService : IVisitPayUserIssueResultApplicationService
    {
        private readonly IVisitPayUserIssueResultService _visitPayUserIssueResultService;

        public VisitPayUserIssueResultApplicationService(IVisitPayUserIssueResultService visitPayUserIssueResultService)
        {
            this._visitPayUserIssueResultService = visitPayUserIssueResultService;
        }

        public string[] AdjustAllGuarantorDates(int guarantorId, string datePart, int adjustment)
        {
            var results = this._visitPayUserIssueResultService.AdjustAllGuarantorDates(guarantorId, datePart, adjustment);
            if (results.Count > 0)
            {
                var arr = results.Select(i => i.IssueLevel + " - " + i.IssueMessage).ToArray();
                return arr;
            }
            return null;
        }

        public string[] ClearGuarantorData(int guarantorId)
        {
            IReadOnlyList<IssueResult> results = this._visitPayUserIssueResultService.ClearGuarantorData(guarantorId);
            if (results.Count <= 0)
                return null;

            string[] arr = results.Select(i => i.IssueLevel + " - " + i.IssueMessage).ToArray();
            return arr;
        }
        
    }
}

﻿namespace Ivh.Application.Core.Mappings
{
    using System;
    using System.Linq;
    using System.Web;
    using AutoMapper;
    using Common.Dtos;
    using Common.Dtos.Eob;
    using Common.Models.Eob;
    using Domain.AppIntelligence.Survey.Entities;
    using Domain.Core.Alert.Entities;
    using Domain.Core.Audit.Entities;
    using Domain.Core.Audit.Interfaces;
    using Domain.Core.Chat.Entities;
    using Domain.Guarantor.Entities;
    using Domain.Core.KnowledgeBase.Entities;
    using Domain.Core.Sso.Entities;
    using Domain.Core.SystemMessage.Entities;
    using Domain.Core.TreatmentLocation.Entities;
    using Domain.Core.VanityUrl.Entities;
    using Domain.Core.VisitPayDocument.Entities;
    using Domain.Eob.Entities;
    using Domain.EventJournal.Entities;
    using Domain.HospitalData.Analytics.Entities;
    using Domain.User.Entities;
    using Domain.User.Interfaces;
    using Domain.Visit.Entities;
    using Domain.Visit.Interfaces;
    using Domain.Settings.Entities;
    using Domain.Visit.Unmatching.Entities;
    using EventJournal.Common.Dtos;
    using FinanceManagement.Common.Dtos;
    using Ivh.Application.User.Common.Dtos;
    using Ivh.Common.Base.Utilities.Extensions;
    using Ivh.Common.Base.Utilities.Responses;
    using Ivh.Common.Web.Extensions;
    using Ivh.Common.Base.Utilities.Helpers;
    using Ivh.Common.EventJournal;
    using Ivh.Common.VisitPay.Enums;
    using Ivh.Common.VisitPay.Messages.Core;
    using Ivh.Common.VisitPay.Messages.Matching.Dtos;
    using Monitoring.Common.Dtos;
    using Ivh.Application.Core.Common.Models.Lockbox;
    using Ivh.Common.VisitPay.Messages.FinanceManagement;
    using Ivh.Common.VisitPay.Strings;

    public class AutomapperProfile : Profile
    {
        public AutomapperProfile()
        {
            this.CreateMap<VisitPayGuarantorAuditLog, VisitPayGuarantorAuditLogDto>();
            this.CreateMap<VisitPayGuarantorAuditLogDto, VisitPayGuarantorAuditLog>();

            this.CreateMap<Client, ClientDto>();
            this.CreateMap<ClientDto, Client>();

            this.CreateMap<GuarantorDto, Guarantor>()
                .ForMember(dest => dest.MatchedHsGuarantorMaps, opts => opts.Ignore());
            this.CreateMap<Guarantor, GuarantorDto>()
                .ForMember(dest => dest.IsConsolidated, opts => opts.MapFrom(src => src.IsConsolidated()))
                .ForMember(dest => dest.IsManaged, opts => opts.MapFrom(src => src.IsManaged()))
                .ForMember(dest => dest.IsOfflineGuarantor, opts => opts.MapFrom(src => src.IsOfflineGuarantor()))
                .ForMember(dest => dest.HsGuarantorsSourceSystemKeys, opts => opts.MapFrom(src => src.MatchedHsGuarantorMaps.Select(s => s.SourceSystemKey).ToList()));
            this.CreateMap<HsGuarantorDetails, HsGuarantorDto>();
            this.CreateMapBidirectional<HsGuarantorVpSearchResult, HsGuarantorDto>();
            this.CreateMapBidirectional<GuarantorCancellationDto, VpGuarantorCancellation>();
            this.CreateMapBidirectional<GuarantorCancellationReasonDto, VpGuarantorCancellationReason>();

            this.CreateMap<Ivh.Common.VisitPay.Messages.HospitalData.Dtos.HsGuarantorDto, HsGuarantorDto>();

            this.CreateMap<GuarantorPaymentDueDayHistory, GuarantorPaymentDueDayHistoryDto>();

            this.CreateMap<GuarantorFilterDto, GuarantorFilter>();

            this.CreateMap<GuarantorInvitationDto, GuarantorInvitation>();
            this.CreateMap<GuarantorInvitation, GuarantorInvitationDto>();

            this.CreateMap<VisitPayUserDto, VisitPayUser>();

            this.CreateMap<VisitPayUser, VisitPayUserDto>()
                .ForMember(dest => dest.AddressStreet1, opts => opts.MapFrom(src => src.CurrentPhysicalAddress.AddressStreet1))
                .ForMember(dest => dest.AddressStreet2, opts => opts.MapFrom(src => src.CurrentPhysicalAddress.AddressStreet2))
                .ForMember(dest => dest.City, opts => opts.MapFrom(src => src.CurrentPhysicalAddress.City))
                .ForMember(dest => dest.State, opts => opts.MapFrom(src => src.CurrentPhysicalAddress.StateProvince))
                .ForMember(dest => dest.Zip, opts => opts.MapFrom(src => src.CurrentPhysicalAddress.PostalCode))
                .ForMember(dest => dest.MailingAddressStreet1, opts => opts.MapFrom(src => src.CurrentMailingAddress != null ? src.CurrentMailingAddress.AddressStreet1 : String.Empty))
                .ForMember(dest => dest.MailingAddressStreet2, opts => opts.MapFrom(src => src.CurrentMailingAddress != null ? src.CurrentMailingAddress.AddressStreet2 : String.Empty))
                .ForMember(dest => dest.MailingCity, opts => opts.MapFrom(src => src.CurrentMailingAddress != null ? src.CurrentMailingAddress.City : String.Empty))
                .ForMember(dest => dest.MailingState, opts => opts.MapFrom(src => src.CurrentMailingAddress != null ? src.CurrentMailingAddress.StateProvince : String.Empty))
                .ForMember(dest => dest.MailingZip, opts => opts.MapFrom(src => src.CurrentMailingAddress != null ? src.CurrentMailingAddress.PostalCode : String.Empty))
                .ForMember(dest => dest.AccessTokenPhi, opts => opts.MapFrom(src => src.GetPrintFormattedAccessTokenPhi()));

            this.CreateMap<VisitPayUser, MatchGuarantorDto>()
                .ForMember(dest => dest.FirstName, opts => opts.MapFrom(src => src.FirstName))
                .ForMember(dest => dest.LastName, opts => opts.MapFrom(src => src.LastName))
                .ForMember(dest => dest.DOB, opts => opts.MapFrom(src => src.DateOfBirth))
                .ForMember(dest => dest.AddressLine1, opts => opts.MapFrom(src => src.CurrentPhysicalAddress.AddressStreet1))
                .ForMember(dest => dest.AddressLine2, opts => opts.MapFrom(src => src.CurrentPhysicalAddress.AddressStreet2))
                .ForMember(dest => dest.City, opts => opts.MapFrom(src => src.CurrentPhysicalAddress.City))
                .ForMember(dest => dest.StateProvince, opts => opts.MapFrom(src => src.CurrentPhysicalAddress.StateProvince))
                .ForMember(dest => dest.PostalCode, opts => opts.MapFrom(src => src.CurrentPhysicalAddress.PostalCode))
                .ForMember(dest => dest.SSN4, opts => opts.MapFrom(src => src.SSN4.ToString()));


            this.CreateMap<VisitPayRole, VisitPayRoleDto>();
            this.CreateMap<VisitPayRoleDto, VisitPayRole>();

            this.CreateMap<VisitPayUserClaim, VisitPayUserClaimDto>();
            this.CreateMap<VisitPayUserClaimDto, VisitPayUserClaim>();

            this.CreateMap<VisitPayUserLogin, VisitPayUserLoginDto>();
            this.CreateMap<VisitPayUserLoginDto, VisitPayUserLogin>();

            this.CreateMap<VisitPayUserAcknowledgement, VisitPayUserAcknowledgementDto>();
            this.CreateMap<VisitPayUserAcknowledgementDto, VisitPayUserAcknowledgement>();

            this.CreateMap<SecurityQuestion, SecurityQuestionDto>();
            this.CreateMap<SecurityQuestionDto, SecurityQuestion>();

            this.CreateMap<SecurityQuestionAnswer, SecurityQuestionAnswerDto>();
            this.CreateMap<SecurityQuestionAnswerDto, SecurityQuestionAnswer>();

            this.CreateMap<BillingSystem, BillingSystemDto>();
            this.CreateMap<BillingSystemDto, BillingSystem>();

            this.CreateMapBidirectional<Facility, FacilityDto>();
            this.CreateMapBidirectional<State, StateDto>();

            this.CreateMapBidirectional<VanityUrl, VanityUrlDto>();

            this.CreateMap<VisitDto, Visit>()
                .ForMember(dest => dest.DischargeDate, x => x.MapFrom(src => src.DischargeDate == DateTime.MinValue ? (DateTime?)null : src.DischargeDate));

            this.CreateMap<VisitDto, VisitPaymentDto>()
                .ForMember(dest => dest.VisitId, opt => opt.MapFrom(src => src.VisitId))
                .ForMember(dest => dest.PaymentAmount, opt => opt.MapFrom(src => src.TotalBalance));

            this.CreateMap<Visit, VisitDto>()
                .ForMember(dest => dest.DischargeDate, x => x.MapFrom(src => src.DischargeDate ?? DateTime.MinValue))
                .ForMember(dest => dest.SourceSystemKeyDisplay, x => x.MapFrom(src => src.SourceSystemKeyDisplay ?? src.SourceSystemKey));

            this.CreateMapBidirectional<HsGuarantorUnmatchReason, HsGuarantorUnmatchReasonDto>();

            this.CreateMap<VisitFilterDto, VisitFilter>();

            this.CreateMap<VisitTransaction, VisitTransactionDto>()
                .ForMember(dest => dest.TransactionType, x => x.ResolveUsing(src => src.VpTransactionType.TransactionType))
                .ForMember(dest => dest.TransactionAmount, x => x.MapFrom(src => src.VisitTransactionAmount.TransactionAmount));

            this.CreateMap<VisitTransactionDto, VisitTransaction>();

            this.CreateMap<Ivh.Common.VisitPay.Messages.HospitalData.Dtos.VisitTransactionChangeDto, VisitTransaction>()
                .ForMember(dest => dest.VisitTransactionId, x => x.Ignore())
                .ForMember(dest => dest.BillingSystem, x => x.MapFrom(src => new BillingSystem { BillingSystemId = src.BillingSystemId }))
                .ForMember(dest => dest.PaymentAllocationId, x => x.MapFrom(src => src.PaymentAllocationId))
                .ForMember(dest => dest.VpTransactionType, x => x.MapFrom(src => new VpTransactionType { VpTransactionTypeId = src.VpTransactionTypeId }))
                .AfterMap((src, dest) => dest.SetTransactionAmount(src.TransactionAmount));

            this.CreateMap<Ivh.Common.VisitPay.Messages.HospitalData.Dtos.PrimaryInsuranceTypeDto, PrimaryInsuranceType>();
            this.CreateMap<Ivh.Common.VisitPay.Messages.HospitalData.Dtos.InsurancePlanDto, InsurancePlan>()
                .ForMember(dest => dest.PrimaryInsuranceType, x => x.Ignore());
            this.CreateMap<Ivh.Common.VisitPay.Messages.HospitalData.Dtos.VisitInsurancePlanChangeDto, VisitInsurancePlan>()
                .ForMember(dest => dest.VisitInsurancePlanId, x => x.Ignore());

            this.CreateMap<VisitTransaction, VisitTransaction>()
                .ForMember(dest => dest.VisitTransactionId, x => x.Ignore())
                .ForMember(dest => dest.VisitTransactionAmount, x => x.Ignore())
                .ForMember(dest => dest.VisitTransactionAmounts, x => x.Ignore());

            this.CreateMap<VisitTransactionFilterDto, VisitTransactionFilter>();

            #region visit status history

            this.CreateMap<VisitStateHistory, VisitStateHistoryDto>()
                .ForMember(dest => dest.Entity, opts => opts.MapFrom(src => (Visit)src.Entity));

            this.CreateMap<VisitStateHistoryFilterDto, VisitStateHistoryFilter>();
            this.CreateMap<VisitStateHistoryResults, VisitStateHistoryResultsDto>();

            #endregion

            #region visit aging history

            this.CreateMap<VisitAgingHistory, VisitAgingHistoryDto>();
            this.CreateMap<VisitAgingHistoryDto, VisitAgingHistory>();
            this.CreateMap<VisitAgingHistoryFilterDto, VisitAgingHistoryFilter>();
            this.CreateMap<VisitAgingHistoryResults, VisitAgingHistoryResultsDto>();

            #endregion

            this.CreateMap<TreatmentLocation, TreatmentLocationDto>();
            this.CreateMap<TreatmentLocationDto, TreatmentLocation>();

            this.CreateMap<VisitPayUserFilterDto, VisitPayUserFilter>()
                .ForMember(dest => dest.RolesInclude, opts => opts.MapFrom(src => src.Roles));

            this.CreateMap<VisitPayUserResults, VisitPayUserResultsDto>();

            this.CreateMap<HsGuarantorMatchDiscrepancyFilterDto, HsGuarantorMatchDiscrepancyFilter>();

            this.CreateMap<HsGuarantorMatchDiscrepancyResults, HsGuarantorMatchDiscrepancyResultsDto>();

            this.CreateMap<HsGuarantorMatchDiscrepancy, HsGuarantorMatchDiscrepancyDto>()
                .ForMember(dest => dest.HsGuarantorMatchDiscrepancyStatusName, opts => opts.MapFrom(src => src.HsGuarantorMatchDiscrepancyStatusSource.HsGuarantorMatchDiscrepancyStatusName))
                .ForMember(dest => dest.HsGuarantorSourceSystemKey, opts => opts.MapFrom(src => src.HsGuarantorMap.SourceSystemKey))
                .ForMember(dest => dest.HsGuarantorUnmatchReasonName, opts => opts.MapFrom(src => src.HsGuarantorUnmatchReason.HsGuarantorUnmatchReasonName))
                .ForMember(dest => dest.HsGuarantorUnmatchReasonDisplayName, opts => opts.MapFrom(src => src.HsGuarantorUnmatchReason.HsGuarantorUnmatchReasonDisplayName))
                .ForMember(dest => dest.HsGuarantorUnmatchReasonEnum, opts => opts.MapFrom(src => src.HsGuarantorUnmatchReason.HsGuarantorUnmatchReasonEnum))
                .ForMember(dest => dest.HsGuarantorVpGuarantorId, opts => opts.MapFrom(src => src.HsGuarantorMatchDiscrepancyStatus == HsGuarantorMatchDiscrepancyStatusEnum.ValidMatch ? src.HsGuarantorMap.VpGuarantor.VpGuarantorId : (int?)null));

            this.CreateMap<HsGuarantorMatchDiscrepancyMatchField, HsGuarantorMatchDiscrepancyMatchFieldDto>();

            this.CreateMap<HsGuarantorMatchDiscrepancyStatus, HsGuarantorMatchDiscrepancyStatusDto>();

            this.CreateMap<VisitUnmatchFilterDto, VisitUnmatchFilter>();

            this.CreateMap<VisitUnmatchResults, VisitUnmatchResultsDto>();

            this.CreateMap<Visit, VisitUnmatchDto>()
                .ForMember(dest => dest.HsGuarantorSourceSystemKey, opts => opts.MapFrom(src => src.HsGuarantorMap == null ? null : src.HsGuarantorMap.SourceSystemKey))
                .ForMember(dest => dest.HsGuarantorUnmatchReasonDisplayName, opts => opts.MapFrom(src => src.HsGuarantorUnmatchReason.HsGuarantorUnmatchReasonDisplayName));

            this.CreateMap<VisitItemizationStorage, VisitItemizationStorageDto>();

            this.CreateMap<VisitItemizationStorageFilterDto, VisitItemizationStorageFilter>();

            this.CreateMap<VisitItemizationStorageResults, VisitItemizationStorageResultsDto>();

            this.CreateMap<SystemMessage, SystemMessageDto>();

            // mapping to same type to clone in AlertingApplicationService, need this mapping
            this.CreateMap<AlertDto, AlertDto>();

            this.CreateMap<Alert, AlertDto>();

            this.CreateMap<SsoProvider, SsoProviderDto>()
                .ForMember(dest => dest.SsoTermsCmsRegion, opts => opts.MapFrom(src => (CmsRegionEnum)src.SsoTermsCmsRegion.CmsRegionId));
            this.CreateMap<SsoProviderDto, SsoProvider>()
                .ForMember(dest => dest.SsoTermsCmsRegion, opts => opts.Ignore());

            this.CreateMap<SsoVisitPayUserSettingDto, SsoVisitPayUserSetting>()
                .ForMember(dest => dest.PromptAfterDateTime, opts => opts.Ignore());

            this.CreateMap<SsoVisitPayUserSetting, SsoVisitPayUserSettingDto>()
                .ForMember(dest => dest.IsIgnored, opts => opts.MapFrom(src => src.IsIgnored()));

            this.CreateMap<SsoProvider, SsoProviderWithUserSettingsDto>()
                .ForMember(dest => dest.Provider, opts => opts.MapFrom(src => src))
                .ForMember(dest => dest.UserSettings, opts => opts.Ignore());

            this.CreateMap<AuditRoleChange, AuditRoleChangeDto>();
            this.CreateMap<AuditUserAccess, AuditUserAccessDto>();
            this.CreateMap<AuditUserChange, AuditUserChangeDto>();
            this.CreateMap<AuditUserChangeDetail, AuditUserChangeDetailDto>();
            this.CreateMap<AuditFilterDto, AuditFilter>();

            this.CreateMap<AuditLogFilterDto, JournalEventFilter>();

            this.CreateMap<JournalEventFilterDto, JournalEventFilter>()
                .ForMember(dest => dest.VisitPayUserId, opts => opts.MapFrom(src => src.VisitPayUserId.HasValue ? src.VisitPayUserId.Value.ToString() : null));

            this.CreateMap<Domain.EventJournal.Entities.JournalEvent, JournalEventDto>()
                .ForMember(dest => dest.EventVisitPayUserIdUserName, opts => opts.MapFrom(src => src.EventVisitPayUser.UserName))
                .ForMember(dest => dest.UserAgent, opts => opts.MapFrom(src => src.UserAgent.UserAgentString));

            this.CreateMap<HttpContext, HttpContextDto>()
                .ForMember(dest => dest.Url, opts => opts.MapFrom(src => src.Request.Url.ToString()))
                .ForMember(dest => dest.Request, opts => opts.MapFrom(src => src.Request))
                .ForMember(dest => dest.Response, opts => opts.MapFrom(src => src.Response))
                .ForMember(dest => dest.User, opts => opts.MapFrom(src => src.User))
                .ForMember(dest => dest.Session, opts => opts.MapFrom(src => src.Session))
                .ForMember(dest => dest.UserHostAddress, opts => opts.MapFrom(src => src.Request.RequestUserHostAddressString()));

            this.CreateMap<HttpContext, JournalEventHttpContextDto>()
                .ForMember(dest => dest.Url, opts => opts.MapFrom(src => src.Request.Url.AbsolutePath))
                .ForMember(dest => dest.UserHostAddress, opts => opts.MapFrom(src => src.Request.UserHostAddress))
                .ForMember(dest => dest.UserAgent, opts => opts.MapFrom(src => src.Request.UserAgent))
                .ForMember(dest => dest.DeviceType, opts => opts.MapFrom(src => DeviceTypeHelper.GetDeviceType(src.Request.UserAgent)));

            //TODO: remove this temp map once UserEvents are entirely replaced
            this.CreateMap<HttpContextDto, JournalEventHttpContextDto>()
                .ForMember(dest => dest.Url, opts => opts.MapFrom(src => src.Request.Url.AbsolutePath))
                .ForMember(dest => dest.UserHostAddress, opts => opts.MapFrom(src => src.Request.UserHostAddress))
                .ForMember(dest => dest.UserAgent, opts => opts.MapFrom(src => src.Request.UserAgent))
                .ForMember(dest => dest.DeviceType, opts => opts.MapFrom(src => DeviceTypeHelper.GetDeviceType(src.Request.UserAgent)));

            this.CreateMap<SearchTextLogMessage, SearchTextLog>();

            #region Survey

            this.CreateMap<Survey, SurveyDto>()
                .ForMember(dest => dest.SurveyQuestions, opts => opts.MapFrom(src => src.SurveyQuestions));

            this.CreateMap<SurveyQuestion, SurveyResultAnswerDto>()
                .ForMember(dest => dest.SurveyRatingGroupId, opts => opts.MapFrom(src => src.SurveyRatingGroup.SurveyRatingGroupId));

            this.CreateMapBidirectional<SurveyResultDto, SurveyResult>();

            this.CreateMapBidirectional<SurveyResultAnswerDto, SurveyResultAnswer>();


            this.CreateMapBidirectional<SurveyRatingDto, SurveyRating>();

            this.CreateMapBidirectional<SurveyRatingGroupDto, SurveyRatingGroup>();

            #endregion

            #region SAML

            this.CreateMapBidirectional<SamlResponse, SamlResponseDto>();
            this.CreateMap<Response<SamlResponse>, Response<SamlResponseDto>>().ConstructUsing(ctx => new Response<SamlResponseDto>(Mapper.Map<SamlResponseDto>(ctx.Object), ctx.IsSuccess, ctx.ErrorMessage));

            #endregion

            #region KnowledgeBase

            this.CreateMap<KnowledgeBaseCategory, KnowledgeBaseCategoryDto>()
                .ForMember(dest => dest.KnowledgeBaseSubCategories, opts => opts.Ignore());

            this.CreateMap<KnowledgeBaseSubCategory, KnowledgeBaseSubCategoryDto>()
                .ForMember(dest => dest.KnowledgeBaseCategoryId, opts => opts.MapFrom(src => src.KnowledgeBaseCategory.KnowledgeBaseCategoryId))
                .ForMember(dest => dest.QuestionAnswers, opts => opts.Ignore());

            this.CreateMap<KnowledgeBaseQuestionAnswer, KnowledgeBaseQuestionAnswerDto>()
                .ForMember(dest => dest.DisplayOrder, opts => opts.Ignore())
                .ForMember(dest => dest.Rank, opts => opts.MapFrom(src => src.Rank(null)));

            this.CreateMap<KnowledgeBaseCategory, KnowledgeBaseCategoryQuestionsDto>()
                .ForMember(dest => dest.Questions, opts => opts.Ignore());

            this.CreateMap<KnowledgeBaseCategory, KnowledgeBaseCategoryQuestionAnswersDto>()
                .ForMember(dest => dest.Questions, opts => opts.Ignore());

            this.CreateMap<KnowledgeBaseCategoryDto, KnowledgeBaseCategoryQuestionAnswersDto>()
                .ForMember(dest => dest.Questions, opts => opts.MapFrom(src => src.KnowledgeBaseSubCategories.SelectMany(x => x.QuestionAnswers)));

            this.CreateMap<KnowledgeBaseCategoryDto, KnowledgeBaseCategoryQuestionsDto>()
                .ForMember(dest => dest.Questions, opts => opts.MapFrom(src => src.KnowledgeBaseSubCategories.SelectMany(x => x.QuestionAnswers)));

            this.CreateMap<KnowledgeBaseQuestionAnswer, KnowledgeBaseQuestionDto>()
                .ForMember(dest => dest.DisplayOrder, opts => opts.Ignore());

            this.CreateMap<KnowledgeBaseQuestionAnswerDto, KnowledgeBaseQuestionDto>();

            #endregion

            #region EOB

            this.CreateMap<Ivh.Common.VisitPay.Messages.Eob.EobClaimPaymentMessage, ClaimPaymentInformationHeader>();
            this.CreateMap<Ivh.Common.VisitPay.Messages.Eob.EobExternalLinkMessage, EobExternalLink>();
            this.CreateMap<Ivh.Common.VisitPay.Messages.Eob.EobPayerFilterEobExternalLinkMessage, EobPayerFilterEobExternalLink>();
            this.CreateMap<Ivh.Common.VisitPay.Messages.Eob.EobPayerFilterMessage, EobPayerFilter>();
            this.CreateMap<Ivh.Common.VisitPay.Messages.Eob.EobDisplayCategoryMessage, EobDisplayCategory>();
            this.CreateMap<Ivh.Common.VisitPay.Messages.Eob.EobClaimAdjustmentReasonCodeMessage, Domain.Eob.Entities.EobClaimAdjustmentReasonCode>();
            this.CreateMap<Ivh.Common.VisitPay.Messages.Eob.EobClaimAdjustmentReasonCodeDisplayMapMessage, EobClaimAdjustmentReasonCodeDisplayMap>();

            this.CreateMap<EobIndicatorResultDto, EobIndicatorResultModel>();
            this.CreateMap<VisitEobDetailsResultDto, VisitEobDetailsResultModel>();
            this.CreateMap<VisitEobServicePaymentInformationDto, VisitEobServicePaymentInformationModel>();
            this.CreateMap<VisitEobClaimAdjustmentDto, VisitEobClaimAdjustmentModel>();
            this.CreateMap<VisitEobIndustryCodeIdentificationDto, VisitEobIndustryCodeIdentificationModel>();

            this.CreateMap<EobExternalLink, EobExternalLinkDto>();

            #endregion

            #region VisitPayDocument

            this.CreateMap<VisitPayDocumentDto, VisitPayDocument>();

            //Base mapping
            this.CreateMap<VisitPayDocument, VisitPayDocumentBaseDto>();
            this.CreateMap<VisitPayDocumentResult, VisitPayDocumentBaseDto>();

            this.CreateMap<VisitPayDocument, VisitPayDocumentDto>()
                .IncludeBase<VisitPayDocument, VisitPayDocumentBaseDto>();

            this.CreateMap<VisitPayDocumentResult, VisitPayDocumentResultDto>()
                .IncludeBase<VisitPayDocumentResult, VisitPayDocumentBaseDto>(); ;

            this.CreateMap<VisitPayDocument, VisitPayDocumentResult>();

            this.CreateMap<VisitPayDocumentEditDto, VisitPayDocument>();
            #endregion

            this.CreateMapBidirectional<SsoVisitPayUserOauthTokenDto, SsoVisitPayUserOauthToken>();

            this.CreateMapBidirectional<BalanceTransferStatus, BalanceTransferStatusDto>();
            this.CreateMapBidirectional<BalanceTransferStatusHistory, BalanceTransferStatusHistoryDto>();

            this.CreateMap<SystemInfoDetailsDto, SystemHealthDetailsDto>();

            #region Analytic Reports

            this.CreateMap<AnalyticReport, AnalyticReportDto>()
                .ForMember(dest => dest.Classification, opts => opts.MapFrom(src => src.AnalyticReportClassification.ClassificationName));
            this.CreateMap<AnalyticReportClassification, AnalyticReportClassificationDto>();

            #endregion

            #region Chat
            //source, destination
            this.CreateMap<ChatAvailabilityDto, ChatAvailability>();
            this.CreateMap<ChatAvailability, ChatAvailabilityDto>();

            #endregion Chat

            #region PaymentImport

            this.CreateMap<LockboxFile, PaymentImportFileMessage>()
                .ForMember(dest => dest.TotalAmount, opts => opts.MapFrom(src => src.FileHeader.FileTrailer.TotalAmount))
                .ForMember(dest => dest.TotalBatchCount, opts => opts.MapFrom(src => src.FileHeader.FileTrailer.TotalBatchCount.TrimStart<int>(' ')))
                .ForMember(dest => dest.TotalCheckCount, opts => opts.MapFrom(src => src.FileHeader.FileTrailer.TotalCheckCount.TrimStart<int>(' ')))
                .ForMember(dest => dest.Messages, opts => opts.MapFrom(src => src.FileHeader.BatchHeaders.SelectMany(x => x.Details)));

            this.CreateMap<Detail, PaymentImportMessage>()
                .ForMember(dest => dest.BatchCreditDate, opts => opts.MapFrom(src => DateTime.ParseExact(src.BatchCreditDate, Format.YearMonthDay, null)))
                .ForMember(dest => dest.GuarantorAccountNumber, opts => opts.MapFrom(src => src.GuarantorAccountNumber.TrimStart('0')))
                .ForMember(dest => dest.GuarantorFirstName, opts => opts.MapFrom(src => src.GuarantorFirstName.Trim<string>('0', ' ')))
                .ForMember(dest => dest.GuarantorLastName, opts => opts.MapFrom(src => src.GuarantorLastName.Trim<string>('0', ' ')))
                .ForMember(dest => dest.InvoiceAmount, opts => opts.MapFrom(src => src.InvoiceAmount.TrimStart<decimal>('0')))
                .ForMember(dest => dest.BatchNumber, opts => opts.MapFrom(src => src.BatchNumber.TrimStart('0')))
                .ForMember(dest => dest.PaymentSequenceNumber, opts => opts.MapFrom(src => src.PaymentSequenceNumber.TrimStart('0')))
                .ForMember(dest => dest.CheckPaymentNumber, opts => opts.MapFrom(src => src.CheckPaymentNumber.TrimStart('0')))
                .ForMember(dest => dest.CheckAccountNumber, opts => opts.MapFrom(src => src.CheckAccountNumber))
                .ForMember(dest => dest.CheckRouting, opts => opts.MapFrom(src => src.CheckRouting))
                .ForMember(dest => dest.CheckRemitter, opts => opts.MapFrom(src => src.CheckRemitter.TrimStart(' ')))
                .ForMember(dest => dest.PsrId, opts => opts.MapFrom(src => src.PsrId.TrimStart(' ')))
                .ForMember(dest => dest.LockBoxNumber, opts => opts.MapFrom(src => src.ParentBatchHeaderLockBoxNumber.TrimStart(' ')))

                .ForMember(dest => dest.PaymentComment, opts => opts.MapFrom(src => src.DetailTrailer.PaymentComment.TrimStart(' ')))
                .ForMember(dest => dest.EmailAddress, opts => opts.MapFrom(src => src.DetailTrailer.EmailAddress.TrimStart(' ')))
                .ForMember(dest => dest.DraftReturnCode, opts => opts.MapFrom(src => src.DetailTrailer.DraftReturnCode.TrimStart(' ')))
                .ForMember(dest => dest.DraftReturnInfo, opts => opts.MapFrom(src => src.DetailTrailer.DraftReturnInfo.TrimStart(' ')))
                .ForMember(dest => dest.RdfiBankName, opts => opts.MapFrom(src => src.DetailTrailer.RdfiBankName.TrimStart(' ')))
                ;

            #endregion
        }
    }
}
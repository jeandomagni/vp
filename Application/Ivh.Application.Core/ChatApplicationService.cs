﻿
namespace Ivh.Application.Core
{
    using System;
    using System.Collections.Generic;
    using System.Configuration;
    using System.Linq;
    using System.Security.Cryptography;
    using System.Web;
    using AutoMapper;
    using Base.Common.Interfaces;
    using Base.Services;
    using Common.Interfaces;
    using Domain.Base.Extensions;
    using Domain.Core.Chat.Entities;
    using Domain.Core.Chat.Interfaces;
    using Domain.Guarantor.Entities;
    using Domain.Guarantor.Interfaces;
    using Domain.Settings.Interfaces;
    using Domain.User.Interfaces;
    using Ivh.Application.Core.Common.Dtos;
    using Ivh.Common.Encryption;
    using Ivh.Common.EventJournal;
    using Ivh.Common.VisitPay.Constants;

    public class ChatApplicationService : ApplicationService, IChatApplicationService
    {
        private readonly Lazy<IGuarantorService> _guarantorService;
        private readonly Lazy<IVisitPayUserJournalEventService> _userJournalEventService;
        private readonly Lazy<IChatAvailabilityService> _chatAvailabilityService;

        public ChatApplicationService(
            Lazy<IApplicationServiceCommonService> applicationServiceCommonService,
            Lazy<IGuarantorService> guarantorService,
            Lazy<IVisitPayUserJournalEventService> userJournalEventService,
            Lazy<IChatAvailabilityService> chatAvailabilityService
            ) : base(applicationServiceCommonService)
        {
            this._guarantorService = guarantorService;
            this._userJournalEventService = userJournalEventService;
            this._chatAvailabilityService = chatAvailabilityService;
        }

        public int GetGuarantorIdFromToken(Guid guarantorChatToken)
        {
            int guarantorId = this._guarantorService.Value.GetGuarantorId(guarantorChatToken);
            return guarantorId;
        }

        public void ConnectToChatThread(int clientVpUserId, int vpGuarantorId, string chatThreadId)
        {
            Guarantor guarantor = this._guarantorService.Value.GetGuarantor(vpGuarantorId);
            if (guarantor != null)
            {
                //Log journal event
                JournalEventHttpContextDto eventContextDto = Mapper.Map<JournalEventHttpContextDto>(HttpContext.Current);
                JournalEventUserContext journalEventUserContext = this._userJournalEventService.Value.GetJournalEventUserContext(eventContextDto, clientVpUserId);
                this._userJournalEventService.Value.LogClientUserConnectedToChat(journalEventUserContext, vpGuarantorId, guarantor.User.UserName, chatThreadId);
            }
        }

        public void DisconnectFromChatThread(int clientVpUserId, int vpGuarantorId, string chatThreadId)
        {
            Guarantor guarantor = this._guarantorService.Value.GetGuarantor(vpGuarantorId);
            if (guarantor != null)
            {
                //Log journal event
                JournalEventHttpContextDto eventContextDto = Mapper.Map<JournalEventHttpContextDto>(HttpContext.Current);
                JournalEventUserContext journalEventUserContext = this._userJournalEventService.Value.GetJournalEventUserContext(eventContextDto, clientVpUserId);
                this._userJournalEventService.Value.LogClientUserDisconnectedFromChat(journalEventUserContext, vpGuarantorId, guarantor.User.UserName, chatThreadId);
            }
        }

        public void EndChatThread(int clientVpUserId, int vpGuarantorId, string chatThreadId)
        {
            Guarantor guarantor = this._guarantorService.Value.GetGuarantor(vpGuarantorId);
            if (guarantor != null)
            {
                //Log journal event
                JournalEventHttpContextDto eventContextDto = Mapper.Map<JournalEventHttpContextDto>(HttpContext.Current);
                JournalEventUserContext journalEventUserContext = this._userJournalEventService.Value.GetJournalEventUserContext(eventContextDto, clientVpUserId);
                this._userJournalEventService.Value.LogClientUserEndedChat(journalEventUserContext, vpGuarantorId, guarantor.User.UserName, chatThreadId);
            }
        }

        public bool IsChatAvailable()
        {
            IList<ChatAvailabilityDto> allAvailabilities = this.GetChatAvailabilities();
            DateTime currentClientDateTime = DateTime.UtcNow.ToClientDateTime();

            //First check if we have an override for today's date
            ChatAvailabilityDto existingOverride = allAvailabilities.FirstOrDefault(a => a.Date.HasValue && a.Date.Value.Date == currentClientDateTime.Date);
            if (existingOverride != null)
            {
                //We have an override for today's date. Check if chat is available.
                return this.CheckIfWithinAvailabilityTimeFrame(existingOverride, currentClientDateTime);
            }

            //No existing override, check for availability on today's day of the week
            ChatAvailabilityDto availabilityForDayOfWeek = allAvailabilities.Single(a => a.DayOfWeek.HasValue && (DayOfWeek)a.DayOfWeek == currentClientDateTime.DayOfWeek);
            return this.CheckIfWithinAvailabilityTimeFrame(availabilityForDayOfWeek, currentClientDateTime);
        }

        private bool CheckIfWithinAvailabilityTimeFrame(ChatAvailabilityDto availability, DateTime currentClientDateTime)
        {
            //Check if day is online. These will always either both have values or not, but check with an OR for sanity.
            if (!availability.TimeStart.HasValue || !availability.TimeEnd.HasValue)
            {
                //The day is offline
                return false;
            }

            //The day has available hours, make sure we are within them
            TimeSpan now = currentClientDateTime.TimeOfDay;
            bool withinHours = (now >= availability.TimeStart.Value && now < availability.TimeEnd.Value);
            return withinHours;
        }

        public IList<ChatAvailabilityDto> GetChatAvailabilities()
        {
            IList<ChatAvailability> availabilities = this._chatAvailabilityService.Value.GetChatAvailabilities();
            return Mapper.Map<IList<ChatAvailabilityDto>>(availabilities);
        }

        public void EditChatAvailability(ChatAvailabilityDto chatAvailability)
        {
            if (chatAvailability != null)
            {
                ChatAvailability entity = Mapper.Map<ChatAvailability>(chatAvailability);
                this._chatAvailabilityService.Value.SaveChatAvailability(entity);
            }
        }

        public void DeleteChatAvailability(int chatAvailabilityId)
        {
            this._chatAvailabilityService.Value.DeleteChatAvailability(chatAvailabilityId);
        }
    }
}

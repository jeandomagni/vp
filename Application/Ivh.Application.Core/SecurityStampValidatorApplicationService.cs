﻿namespace Ivh.Application.Core
{
    using System;
    using System.Threading.Tasks;
    using Common.Interfaces;
    using Domain.User.Entities;
    using Domain.User.Services;
    using Microsoft.AspNet.Identity.Owin;
    using Microsoft.Owin.Security.Cookies;

    public class SecurityStampValidatorApplicationService : ISecurityStampValidatorApplicationService
    {
        public Func<CookieValidateIdentityContext, Task> OnValidateIdentity(TimeSpan validateInterval)
        {
            return SecurityStampValidator.OnValidateIdentity<VisitPayUserService, VisitPayUser>(validateInterval: validateInterval, regenerateIdentity: (manager, user) => manager.GenerateUserIdentityAsync(user));
        }
    }
}
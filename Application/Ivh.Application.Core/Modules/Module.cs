﻿namespace Ivh.Application.Core.Modules
{
    using ApplicationServices;
    using Autofac;
    using Common.Interfaces;
    using Domain.HospitalData.Analytics.Interfaces;
    using Domain.HospitalData.Analytics.Services;
    using Ivh.Common.DependencyInjection.Extensions;
    using Provider.Core.Jobs;

    public class Module : Autofac.Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<AlertingApplicationService>().As<IAlertingApplicationService>();
            builder.RegisterType<ApplicationJobsProvider>().As<IApplicationJobsProvider>();
            builder.RegisterType<BillingSystemApplicationService>().As<IBillingSystemApplicationService>();
            builder.RegisterType<ChatApplicationService>().As<IChatApplicationService>();
            builder.RegisterType<ClientApplicationService>().As<IClientApplicationService>();
            builder.RegisterType<FeatureApplicationService>().As<IFeatureApplicationService>();
            builder.RegisterType<ExternalLinkApplicationService>().As<IExternalLinkApplicationService>();
            builder.RegisterType<GuarantorApplicationService>().As<IGuarantorApplicationService>();
            builder.RegisterType<IpAccessApplicationService>().As<IIpAccessApplicationService>().SingleInstance();
            builder.RegisterType<SecurityQuestionApplicationService>().As<ISecurityQuestionApplicationService>();
            builder.RegisterType<SystemMessageApplicationService>().As<ISystemMessageApplicationService>();
            builder.RegisterType<TreatmentLocationApplicationService>().As<ITreatmentLocationApplicationService>();
            builder.RegisterType<VisitAgingHistoryApplicationService>().As<IVisitAgingHistoryApplicationService>();
            builder.RegisterType<VisitItemizationStorageApplicationService>().As<IVisitItemizationStorageApplicationService>();
            builder.RegisterType<VisitPayUserApplicationService>().As<IVisitPayUserApplicationService>().InstanceWebPerRequestElsePerLifetimeScope();
            builder.RegisterType<VisitPayUserApplicationLightweightService>().As<IVisitPayUserApplicationLightweightService>().InstanceWebPerRequestElsePerLifetimeScope();
            builder.RegisterType<VisitPayUserJournalEventApplicationService>().As<IVisitPayUserJournalEventApplicationService>();
            builder.RegisterType<VisitStateHistoryApplicationService>().As<IVisitStateHistoryApplicationService>();
            builder.RegisterType<AuditApplicationService>().As<IAuditApplicationService>();
            builder.RegisterType<SurveyApplicationService>().As<ISurveyApplicationService>();
            builder.RegisterType<KnowledgeBaseApplicationService>().As<IKnowledgeBaseApplicationService>();
            builder.RegisterType<AuditEventApplicationService>().As<IAuditEventApplicationService>();
            builder.RegisterType<VisitEobApplicationService>().As<IVisitEobApplicationService>();
            builder.RegisterType<VisitPayDocumentApplicationService>().As<IVisitPayDocumentApplicationService>();
            builder.RegisterType<AnalyticReportService>().As<IAnalyticReportService>();
            builder.RegisterType<ThirdPartyKeyApplicationService>().As<IThirdPartyKeyApplicationService>();
            builder.RegisterType<ThirdPartyValueApplicationService>().As<IThirdPartyValueApplicationService>();
            builder.RegisterType<MalwareScanningApplicationService>().As<IMalwareScanningApplicationService>();
            builder.RegisterType<VanityUrlApplicationService>().As<IVanityUrlApplicationService>();
        }
    }
}
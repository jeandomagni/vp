﻿namespace Ivh.Application.Core
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.Linq;
    using System.Security.Claims;
    using System.Security.Principal;
    using System.Threading.Tasks;
    using System.Web;
    using AutoMapper;
    using Base.Common.Dtos;
    using Base.Common.Interfaces;
    using Base.Services;
    using Common.Dtos;
    using Common.Interfaces;
    using Common.Utilities.Builders;
    using Domain.Content.Entities;
    using Domain.Content.Interfaces;
    using Domain.Guarantor.Entities;
    using Domain.Core.LegacyUser.Interfaces;
    using Domain.Email.Entities;
    using Domain.User.Entities;
    using Domain.User.Interfaces;
    using Domain.User.Services;
    using Domain.Email.Interfaces;
    using Domain.Guarantor.Interfaces;
    using Domain.Logging.Interfaces;
    using Domain.Settings.Entities;
    using FinanceManagement.Common.Interfaces;
    using Ivh.Common.Base.Constants;
    using Ivh.Common.Base.Utilities.Extensions;
    using Ivh.Common.Data;
    using Ivh.Common.Web.Extensions;
    using Microsoft.AspNet.Identity;
    using Microsoft.Owin.Security;
    using NHibernate;
    using OfficeOpenXml;
    using IHsGuarantorService = Domain.HospitalData.VpGuarantor.Interfaces.IHsGuarantorService;
    using Ivh.Application.User.Common.Dtos;
    using Ivh.Common.Data.Connection.Connections;
    using Ivh.Common.Data.Interfaces;
    using Ivh.Common.EventJournal;
    using Ivh.Common.EventJournal.Constants;
    using Ivh.Common.EventJournal.Templates.Client;
    using Ivh.Common.EventJournal.Templates.Guarantor;
    using Ivh.Common.EventJournal.Templates.SystemAction;
    using Ivh.Common.JobRunner.Common.Interfaces;
    using Ivh.Common.VisitPay.Constants;
    using Ivh.Common.VisitPay.Enums;
    using Ivh.Common.VisitPay.Jobs;
    using Ivh.Common.VisitPay.Messages.Communication;
    using Ivh.Common.VisitPay.Messages.Core;
    using Ivh.Common.VisitPay.Messages.Scoring;
    using Ivh.Common.VisitPay.Strings;
    using Ivh.Common.Web.Cookies;
    using Ivh.Common.VisitPay.Messages.Matching;
    using Ivh.Common.VisitPay.Messages.Matching.Dtos;
    using MassTransit;
    using Ivh.Application.User.Common.Interfaces;

    public class VisitPayUserApplicationService : ApplicationService, IVisitPayUserApplicationService
    {
        private readonly Lazy<IAuthenticationManager> _authenticationManager;
        private readonly Lazy<ILegacyUserService> _legacyUserService;
        private readonly Lazy<IStatementApplicationService> _statementApplicationService;
        private readonly Lazy<IGuarantorEnrollmentApplicationService> _guarantorEnrollmentApplicationService;
        private readonly Lazy<IContentService> _contentService;
        private readonly Lazy<IVisitPayUserJournalEventService> _userJournalEventService;
        private readonly Lazy<IGuarantorService> _guarantorService;
        private readonly Lazy<IHsGuarantorService> _hsGuarantorService;
        private readonly ISession _session;
        private readonly Lazy<VisitPaySignInService> _signInService;
        private readonly Lazy<VisitPayUserService> _userService;
        private readonly Lazy<IVisitPayRoleService> _visitPayRoleService;
        private readonly Lazy<IVpUserLoginSummaryService> _vpUserLoginSummaryService;
        private readonly Lazy<IMetricsProvider> _metricsProvider;
        private readonly Lazy<IImageService> _imageService;
        private readonly Lazy<ICommunicationService> _communicationService;
        private readonly Lazy<ILocalizationCookieFacade> _localizationCookieFacade;
        private readonly Lazy<IVisitPayUserAddressApplicationService> _visitPayUserAddressApplicationService;

        public VisitPayUserApplicationService(
            Lazy<IApplicationServiceCommonService> applicationServiceCommonServices,
            Lazy<VisitPayUserService> userService,
            Lazy<VisitPaySignInService> signInService,
            Lazy<IVisitPayRoleService> visitPayRoleService,
            Lazy<IVisitPayUserJournalEventService> userJournalEventService,
            Lazy<IVpUserLoginSummaryService> vpUserLoginSummaryService,
            Lazy<IGuarantorService> guarantorService,
            ISessionContext<VisitPay> sessionContext,
            Lazy<IHsGuarantorService> hsGuarantorService,
            Lazy<IContentService> contentService,
            Lazy<ILegacyUserService> legacyUserService,
            Lazy<IStatementApplicationService> statementApplicationService,
            Lazy<IGuarantorEnrollmentApplicationService> guarantorEnrollmentApplicationService,
            Lazy<IMetricsProvider> metricsProvider,
            Lazy<IImageService> imageService,
            Lazy<ICommunicationService> communicationService,
            Lazy<IVisitPayUserAddressApplicationService> visitPayUserAddressApplicationService,
            Lazy<IAuthenticationManager> authenticationManager = null, // optional to allow jobrunner and console apps to use this service
            Lazy<ILocalizationCookieFacade> localizationCookieFacade = null) : base(applicationServiceCommonServices)
        {
            this._userService = userService;
            this._signInService = signInService;
            this._visitPayRoleService = visitPayRoleService;
            this._userJournalEventService = userJournalEventService;
            this._vpUserLoginSummaryService = vpUserLoginSummaryService;
            this._guarantorService = guarantorService;
            this._session = sessionContext.Session;
            this._hsGuarantorService = hsGuarantorService;
            this._contentService = contentService;
            this._authenticationManager = authenticationManager;
            this._legacyUserService = legacyUserService;
            this._statementApplicationService = statementApplicationService;
            this._guarantorEnrollmentApplicationService = guarantorEnrollmentApplicationService;
            this._metricsProvider = metricsProvider;
            this._imageService = imageService;
            this._communicationService = communicationService;
            this._localizationCookieFacade = localizationCookieFacade;
            this._visitPayUserAddressApplicationService = visitPayUserAddressApplicationService;
        }

        public Task<string> GetPhoneNumberAsync(string userId)
        {
            return this._userService.Value.GetPhoneNumberAsync(userId);
        }

        public Task<bool> GetTwoFactorEnabledAsync(string userId)
        {
            return this._userService.Value.GetTwoFactorEnabledAsync(userId);
        }

        public Task<IList<UserLoginInfo>> GetLoginsAsync(string userId)
        {
            return this._userService.Value.GetLoginsAsync(userId);
        }

        public async Task<VisitPayUserDto> FindByIdAsync(string userId)
        {
            return Mapper.Map<VisitPayUserDto>(await this._userService.Value.FindByIdAsync(userId).ConfigureAwait(true));
        }

        public VisitPayUserDto FindById(string userId)
        {
            return Mapper.Map<VisitPayUserDto>(this._userService.Value.FindById(userId));
        }

        public VisitPayUserDto FindByGuarantorId(int vpGuarantorId)
        {
            Guarantor guarantor = this._guarantorService.Value.GetGuarantor(vpGuarantorId);
            return Mapper.Map<VisitPayUserDto>(guarantor.User);
        }

        public Task<string> GenerateChangePhoneNumberTokenAsync(string userId, string phoneNumber)
        {
            return this._userService.Value.GenerateChangePhoneNumberTokenAsync(userId, phoneNumber);
        }

        public Task<IdentityResult> ChangePhoneNumberAsync(string userId, string phoneNumber, string token)
        {
            return this._userService.Value.ChangePhoneNumberAsync(userId, phoneNumber, token);
        }

        public Task<IdentityResult> SetPhoneNumberAsync(string userId, string phoneNumber)
        {
            return this._userService.Value.SetPhoneNumberAsync(userId, phoneNumber);
        }

        public async Task<IdentityResult> ChangePasswordAsync(string userId, string currentPassword, string newPassword, int guarantorPasswordExpLimitInDays, int guarantorPasswordReuseLimit)
        {
            VisitPayUser visitPayUser = this._userService.Value.FindById(userId);
            IdentityResult result = await this.ChangePasswordAsync(visitPayUser, currentPassword, newPassword, guarantorPasswordReuseLimit, guarantorPasswordExpLimitInDays).ConfigureAwait(true);

            if (result.Succeeded)
            {
                this._session.Transaction.RegisterPostCommitSuccessAction((p1) =>
                {
                    this.Bus.Value.PublishMessage(p1).Wait();
                }, new SendAccountCredentialChangePatientEmailMessage
                {
                    Email = visitPayUser.Email,
                    Name = visitPayUser.FirstName,
                    VisitPayUserId = visitPayUser.VisitPayUserId
                });
            }

            return result;
        }

        public Task<IdentityResult> AddPasswordAsync(string userId, string password)
        {
            return this._userService.Value.AddPasswordAsync(userId, password);
        }

        public Task<IdentityResult> AddLoginAsync(string userId, UserLoginInfo login)
        {
            return this._userService.Value.AddLoginAsync(userId, login);
        }

        /// <summary>
        ///     Creates a <paramref name="user" /> with no additional roles.
        /// </summary>
        /// <param name="user">populated dto that represents the user</param>
        /// <param name="password">
        ///     password that conforms to the password standards.
        /// </param>
        /// <param name="roleEnum">type of role to add</param>
        /// <returns>
        ///     Result
        /// </returns>
        public async Task<IdentityResult> CreateWithBasicRoleAsync(VisitPayUserDto user, string password, string roleEnum)
        {
            VisitPayUser visitPayUser = Mapper.Map<VisitPayUser>(user);
            visitPayUser.InsertDate = DateTime.UtcNow;
            visitPayUser.VisitPayUserGuid = Guid.NewGuid();
            VisitPayRole role = this._visitPayRoleService.Value.GetRoleByName(roleEnum);
            if (role != null)
            {
                visitPayUser.Roles.Add(role);
                return await this._userService.Value.CreateAsync(visitPayUser, password).ConfigureAwait(true);
            }
            return new IdentityResult("Couldn't find role");
        }

        public Task<IdentityResult> ConfirmEmailAsync(string userId, string token)
        {
            return this._userService.Value.ConfirmEmailAsync(userId, token);
        }

        public async Task<VisitPayUserDto> FindByNameAsync(string userName)
        {
            VisitPayUserDto dto = default(VisitPayUserDto);
            if (userName != null)
            {
                VisitPayUser user = await this._userService.Value.FindByNameAsync(userName).ConfigureAwait(true);
                dto = Mapper.Map<VisitPayUserDto>(user);
            }
            return dto;
        }

        public VisitPayUserDto FindByName(string userName)
        {
            VisitPayUser user = this._userService.Value.FindByName(userName);
            return Mapper.Map<VisitPayUserDto>(user);
        }

        public Task<bool> IsEmailConfirmedAsync(string userId)
        {
            return this._userService.Value.IsEmailConfirmedAsync(userId);
        }

        private async Task<IdentityResult> ResetPasswordAsync(string userId, string tempPassword, string newPassword, int guarantorPasswordExpLimitInDays, int guarantorPasswordReuseLimit, bool requireTempPassword)
        {
            VisitPayUser visitPayUser = this._userService.Value.FindById(userId);
            if (visitPayUser != null && (visitPayUser.IsLocked || visitPayUser.IsDisabled))
            {
                return new IdentityResult(new List<string>() { "User is locked out." });
            }

            if (!this._userService.Value.VerifyPasswordHasNotBeenUsed(visitPayUser.VisitPayUserId, newPassword, guarantorPasswordReuseLimit))
            {
                return new IdentityResult("You have recently used this password.  Please choose a different password.");
            }

            IdentityResult result;
            if (requireTempPassword)
            {
                result = await this._userService.Value.ResetPasswordAsync(userId, tempPassword, newPassword).ConfigureAwait(true);
            }
            else
            {
                result = this._userService.Value.ResetPassword(userId, newPassword);
            }

            if (!result.Succeeded)
            {
                return result;
            }

            this._userService.Value.SetPasswordExpiration(visitPayUser, guarantorPasswordExpLimitInDays);
            this.SavePasswordHistory(visitPayUser);
            this._userService.Value.ResetAccessFailedCount(visitPayUser.VisitPayUserId.ToString());
            this.LogPasswordChange(visitPayUser, HttpContext.Current);

            this._session.Transaction.RegisterPostCommitSuccessAction((p1) =>
            {
                this.Bus.Value.PublishMessage(p1).Wait();
            }, new SendAccountCredentialChangePatientEmailMessage
            {
                Email = visitPayUser.Email,
                Name = visitPayUser.FirstName,
                VisitPayUserId = visitPayUser.VisitPayUserId
            });

            return result;
        }

        public async Task<IdentityResult> ResetPasswordAsync(string userId, string tempPassword, string newPassword, int guarantorPasswordExpLimitInDays, int guarantorPasswordReuseLimit)
        {
            return await this.ResetPasswordAsync(userId, tempPassword, newPassword, guarantorPasswordExpLimitInDays, guarantorPasswordReuseLimit, true);
        }

        public async Task<IdentityResult> ResetPasswordAsync(string userId, string newPassword, int guarantorPasswordExpLimitInDays, int guarantorPasswordReuseLimit)
        {
            return await this.ResetPasswordAsync(userId, string.Empty, newPassword, guarantorPasswordExpLimitInDays, guarantorPasswordReuseLimit, false);
        }

        public IdentityResult UpdateUserAndGuarantorType(VisitPayUserDto userDto, GuarantorTypeEnum guarantorTypeEnum)
        {
            using (UnitOfWork unitOfWork = new UnitOfWork(this._session))
            {
                IdentityResult identityResult = this.UpdateUser(userDto);
                int guarantorId = this._guarantorService.Value.GetGuarantorId(userDto.VisitPayUserId);
                Guarantor guarantor = this._guarantorService.Value.GetGuarantor(guarantorId);
                this._guarantorService.Value.UpdateGuarantorType(guarantor, guarantorTypeEnum);
                unitOfWork.Commit();
                return identityResult;
            }
        }

        public IdentityResult UpdateUser(VisitPayUserDto userDto, bool validateMailingAddress = false)
        {
            VisitPayUser user = this._userService.Value.FindById(userDto.VisitPayUserId.ToString());

            VisitPayUserAddress physicalAddress = this.MapVisitPayUserPhysicalAddress(userDto, user);
            VisitPayUserAddress mailingAddress = this.MapVisitPayUserMailingAddress(userDto, user);

            string sourceEmail = user.Email;
            string sourceUsername = user.UserName;
            string sourceProfileCompareString = user.PatientProfileCompareString();

            //physical address
            string sourcePhysicalAddressCompareString = user.PhysicalAddressCompareString();
            string updatedPhysicalAddressCompareString = physicalAddress.AddressCompareString();
            bool physicalAddressUpdated = !(string.Equals(sourcePhysicalAddressCompareString, updatedPhysicalAddressCompareString, StringComparison.CurrentCultureIgnoreCase));

            //mailing address
            string sourceMailingAddressCompareString = user.MailingAddressCompareString();
            string updatedMailingAddressCompareString = mailingAddress.AddressCompareString();
            bool mailingAddressUpdated = !(string.Equals(sourceMailingAddressCompareString, updatedMailingAddressCompareString, StringComparison.CurrentCultureIgnoreCase));

            string sourcePhoneNumber = user.PhoneNumber;
            string sourcePhoneNumberSecondary = user.PhoneNumberSecondary;

            user.FirstName = userDto.FirstName;
            user.MiddleName = userDto.MiddleName;
            user.LastName = userDto.LastName;

            if (physicalAddressUpdated)
            {
                user.SetCurrentAddress(physicalAddress);
            }

            if (mailingAddressUpdated && validateMailingAddress)
            {
                AddressDto addressDto = Mapper.Map<AddressDto>(mailingAddress);
                AddressUpdateResultDto result = Task.Run(() => this._visitPayUserAddressApplicationService.Value
                    .UpdateAddressAsync(userDto.VisitPayUserId, addressDto, VisitPayUserAddressTypeEnum.Mailing, true, false)).Result;
                if (!result.Result)
                {
                    return new IdentityResult(result.Data.Select(x => $"Mailing Address: {x}"));
                }
            }

            user.Gender = userDto.Gender;
            user.SSN4 = userDto.SSN4;
            user.DateOfBirth = userDto.DateOfBirth;
            user.AccessFailedCount = userDto.AccessFailedCount;
            user.Email = userDto.Email;
            user.EmailConfirmed = userDto.EmailConfirmed;
            user.PhoneNumber = userDto.PhoneNumber;
            user.PhoneNumberType = userDto.PhoneNumberType;
            user.PhoneNumberConfirmed = userDto.PhoneNumberConfirmed;
            user.TwoFactorEnabled = userDto.TwoFactorEnabled;
            user.UserName = userDto.UserName;
            user.PhoneNumberSecondary = userDto.PhoneNumberSecondary;
            user.PhoneNumberSecondaryType = userDto.PhoneNumberSecondaryType;

            this.AddSecurityQuestionToUser(userDto.SecurityQuestionAnswers, user);

            IdentityResult identityResult = this._userService.Value.Update(user);

            if (!identityResult.Succeeded)
            {
                return identityResult;
            }

            // note: it is expected that up to 3 e-mails per "save" could be sent.

            // send profile updated
            string updatedProfileCompareString = user.PatientProfileCompareString();
            if (!string.Equals(sourceProfileCompareString, updatedProfileCompareString, StringComparison.CurrentCultureIgnoreCase))
            {
                this.SendProfileChangedNotification(user);
            }

            // send email updated to previous email address
            if (!string.Equals(sourceEmail, user.Email, StringComparison.CurrentCultureIgnoreCase))
            {
                this.SendEmailUpdatedNotification(user, sourceEmail);
            }

            // send username changed
            if (!string.Equals(sourceUsername, user.UserName, StringComparison.CurrentCultureIgnoreCase))
            {
                ClaimsIdentity identity = HttpContext.Current.User.Identity as ClaimsIdentity;
                if (identity != null)
                {
                    identity.RemoveClaim(identity.FindFirst(identity.NameClaimType));
                    identity.AddClaim(new Claim(identity.NameClaimType, user.UserName));
                    this.RefreshIdentity(identity);
                }
                this.SendUsernameChangedNotification(user);
            }

            // send address updated message to check for another user with the same address to show Consolidation Notification in notifications list.
            if (!string.Equals(sourcePhysicalAddressCompareString, updatedPhysicalAddressCompareString, StringComparison.CurrentCultureIgnoreCase))
            {
                this.PublishAddressChangedMessage(user.VisitPayUserId);
            }

            // send phone number updated message(s) to the journal
            bool addedPhoneNumber = string.IsNullOrWhiteSpace(userDto.PhoneNumber) && !string.IsNullOrWhiteSpace(sourcePhoneNumber);
            bool addedSecondaryPhoneNumber = string.IsNullOrWhiteSpace(userDto.PhoneNumberSecondary) && !string.IsNullOrWhiteSpace(sourcePhoneNumberSecondary);

            bool deletedPhoneNumber = !string.IsNullOrWhiteSpace(userDto.PhoneNumber) && string.IsNullOrWhiteSpace(sourcePhoneNumber);
            bool deletedSecondaryPhoneNumber = !string.IsNullOrWhiteSpace(userDto.PhoneNumberSecondary) && string.IsNullOrWhiteSpace(sourcePhoneNumberSecondary);

            bool modifiedPhoneNumber = !string.IsNullOrWhiteSpace(userDto.PhoneNumber) && !string.IsNullOrWhiteSpace(sourcePhoneNumber) && userDto.PhoneNumber != sourcePhoneNumber;
            bool modifiedSecondaryPhoneNumber = !string.IsNullOrWhiteSpace(userDto.PhoneNumberSecondary) && !string.IsNullOrWhiteSpace(sourcePhoneNumberSecondary) && userDto.PhoneNumberSecondary != sourcePhoneNumberSecondary;

            //
            Guarantor guarantor = this._guarantorService.Value.GetGuarantor(user.UserName);
            if (this.FeatureService.Value.IsFeatureEnabled(VisitPayFeatureEnum.FeatureMatchingApiIsEnabled))
            {
                MatchGuarantorDto matchGuarantorDto = Mapper.Map<MatchGuarantorDto>(guarantor.User);
                UpdateMatchInfoMessage message = new UpdateMatchInfoMessage
                {
                    VpGuarantorId = guarantor.VpGuarantorId,
                    MatchGuarantorDto = matchGuarantorDto,
                    Application = ApplicationEnum.VisitPay
                };

                this._session.RegisterPostCommitSuccessAction(m =>
                {
                    this.Bus.Value.PublishMessage(m).Wait();
                }, message);
            }
            else
            {
                this._hsGuarantorService.Value.UpdateMatches(guarantor);
            }

            return identityResult;
        }

        private async Task<SignInStatusExEnum?> PreLoginCheck(VisitPayUser visitPayUser, string password)
        {
            if (!string.IsNullOrEmpty(password))
            {
                if (!string.IsNullOrWhiteSpace(visitPayUser.TempPassword))
                {
                    SignInStatusExEnum result = this._userService.Value.VerifyUserTokenAsync(visitPayUser.Id, TokenPurposeEnum.TempLogin.ToString(), password).Result ? SignInStatusExEnum.RequiresVerification : SignInStatusExEnum.Failure;
                    await this.LogVerificationResultAsync(visitPayUser.UserName, visitPayUser.VisitPayUserId, result == SignInStatusExEnum.RequiresVerification, Mapper.Map<JournalEventHttpContextDto>(HttpContext.Current));
                    return result;
                }
                if (this.IsPasswordExpired(visitPayUser.PasswordExpiresUtc)) // if legacy password let them verify that first
                {
                    bool isPasswordValid = this._userService.Value.VerifyPasswordAsync(visitPayUser, password).Result;
                    return isPasswordValid ? SignInStatusExEnum.PasswordExpired : SignInStatusExEnum.Failure;
                }
            }
            if (visitPayUser.Roles.Any(x => x.Name == VisitPayRoleStrings.System.Client) && !this.HasSecurityQuestions(visitPayUser, this.ClientService.Value.GetClient().ClientSecurityQuestionsCount))
            {
                return SignInStatusExEnum.RequiresSecurityQuestions;
            }

            return null;
        }

        private SignInStatusExEnum? PostLoginCheck(VisitPayUser visitPayUser, SignInStatusExEnum? signInStatus)
        {
            if ((signInStatus == SignInStatusExEnum.Failure || signInStatus == SignInStatusExEnum.LockedOut)
                && this._userService.Value.GetLockoutEnabledAsync(visitPayUser.Id).Result
                && this._userService.Value.IsLockedOutAsync(visitPayUser.Id).Result)
            {
                return SignInStatusExEnum.LockedOut;
            }

            if (signInStatus != SignInStatusExEnum.Failure && string.IsNullOrEmpty(visitPayUser.TempPassword) && this.IsPasswordExpired(visitPayUser.PasswordExpiresUtc))
            {
                return SignInStatusExEnum.PasswordExpired;
            }

            return signInStatus;
        }

        public async Task<SignInStatusExEnum> PasswordSignInAsync(HttpContextDto httpContext, string userName, string password, bool isPersistent, bool shouldLockout, string requiredRole, Func<SignInStatusExEnum, int, HttpContextDto, IList<Claim>, Task> afterSuccessAction = null, string authenticationType = DefaultAuthenticationTypes.ApplicationCookie)
        {
            VisitPayUser visitPayUser = this._userService.Value.FindByName(userName);
            if (visitPayUser == null || visitPayUser.Roles.All(x => x.Name != requiredRole))
            {
                //Doesnt have the required role to login
                //Or
                //isnt a user.

                this.LogLogin(visitPayUser ?? new VisitPayUser { UserName = userName, VisitPayUserId = 0 }, SignInStatusExEnum.Failure, httpContext, password);

                this.ClearSession();
                return SignInStatusExEnum.Failure;
            }

            Guarantor guarantor = this._guarantorService.Value.GetGuarantor(visitPayUser.UserName);
            //checking for active vs. closed based off of VPNG-19442 bug
            bool isActive = guarantor != null && guarantor.VpGuarantorStatus == VpGuarantorStatusEnum.Active;
            if (guarantor != null && !isActive)
            {
                this.LogLogin(visitPayUser, SignInStatusExEnum.AccountClosed, httpContext, password);
                this.ClearSession();
                return SignInStatusExEnum.AccountClosed;
            }

            if (await this._userService.Value.IsLockedOutAsync(visitPayUser.Id))
            {
                this.LogLogin(visitPayUser, SignInStatusExEnum.LockedOut, httpContext, password);
                this.ClearSession();
                return SignInStatusExEnum.LockedOut;
            }

            // pre login checks
            SignInStatusExEnum? checkResult = await this.PreLoginCheck(visitPayUser, password);
            SignInStatusExEnum result = checkResult ?? (await this._signInService.Value.PasswordSignInAsync(userName, password, isPersistent, shouldLockout)).ToSignInStatusEx();

            // claims
            IList<Claim> claims = new List<Claim>();
            if (result == SignInStatusExEnum.Success)
            {
                this.AddClaimLastLogin(visitPayUser, claims);
            }

            //
            if (!(!string.IsNullOrWhiteSpace(visitPayUser.TempPassword) && result == SignInStatusExEnum.Failure))
            {
                this.LogLogin(visitPayUser, result, httpContext, password);
            }

            // post login checks
            result = this.PostLoginCheck(visitPayUser, result).GetValueOrDefault(result);
            if (result == SignInStatusExEnum.LockedOut)
            {
                this.SendAccountLockoutEmail(visitPayUser);
                this.ClearSession();
                return result;
            }

            if (result == SignInStatusExEnum.Success || result == SignInStatusExEnum.PasswordExpired)
            {
                this._userService.Value.ResetAccessFailedCount(visitPayUser.Id);
            }

            if (!(result == SignInStatusExEnum.RequiresVerification ||
                  result == SignInStatusExEnum.RequiresSecurityQuestions ||
                  result == SignInStatusExEnum.PasswordExpired))
            {
                this.ClearSession();
            }

            if (result != SignInStatusExEnum.Success)
            {
                return result;
            }

            if (requiredRole == VisitPayRoleStrings.System.Client)
            {
                //Add client user's first/last name to claims
                this.AddClaimClientDisplayFirstNameLastName(visitPayUser, claims);
            }

            if (requiredRole == VisitPayRoleStrings.System.Patient)
            {
                GuarantorDto guarantorDto = Mapper.Map<GuarantorDto>(this._guarantorService.Value.GetGuarantorEx(visitPayUser.VisitPayUserId));

                this.AddClaimRequiresAcknowledgement(visitPayUser, claims);
                this.AddClaimGuarantor(guarantorDto, claims);
                this.AddClaimConsolidationStatus(guarantorDto, claims);
                this.AddClaimHealthEquityOutboundSso(guarantorDto, claims);
                this.AddClaimClientDisplayFirstNameLastName(guarantorDto, claims);

                //VP-2452: Handle locale for user
                this.HandleUserLocaleSettings(visitPayUser);

                //VP-2610: add sms notification on login
                if (this.FeatureService.Value.IsFeatureEnabled(VisitPayFeatureEnum.Sms))
                {
                    AddSystemMessageVisitPayUserMessage systemMessage = new AddSystemMessageVisitPayUserMessage
                    {
                        VisitPayUserId = visitPayUser.VisitPayUserId,
                        SystemMessageEnum = SystemMessageEnum.Sms
                    };
                    await this.Bus.Value.PublishMessage(systemMessage);
                }
            }

            if (afterSuccessAction != null)
            {
                await afterSuccessAction(result, visitPayUser.VisitPayUserId, httpContext, claims);
            }

            this.AddClaimVisitPayUserGuid(visitPayUser, claims);
            this.AddClaimUserInsertDate(visitPayUser, claims);
            this.AddClaims(visitPayUser, claims, authenticationType);

            if (this.Client.Value.ScoringIsEnabled)
            {
                this.AddScore(visitPayUser);
            }

            return result;
        }

        private void HandleUserLocaleSettings(VisitPayUser visitPayUser)
        {
            if (this._localizationCookieFacade == null)
            {
                //Calling application doesn't use localization cookie
                return;
            }

            //If user set locale on the cookie before login, update the user with that locale in the DB
            if (this._localizationCookieFacade.Value.LocaleWasSetByUser)
            {
                string locale = this._localizationCookieFacade.Value.Locale;
                this.SetUserLocale(visitPayUser.Id, locale);
            }
            else
            {
                //Locale was not set by user before login. Update the cookie with the user's locale in case they have one set
                string locale = this.GetUserLocale(visitPayUser.Id);
                this._localizationCookieFacade.Value.SetLocale(locale);
            }
        }

        public void SignIn(VisitPayUserDto user, bool isPersistent, bool rememberBrowser)
        {
            HttpContext.Current.InializeSession();
            VisitPayUser userEntity = this._userService.Value.FindByNameAsync(user.UserName).Result;
            this._signInService.Value.SignInAsync(userEntity, isPersistent, rememberBrowser).Wait();
        }

        public Task<IdentityResult> RemoveLoginAsync(string userId, UserLoginInfo login)
        {
            return this._userService.Value.RemoveLoginAsync(userId, login);
        }

        public async Task SendSmsMessageForCode(string phoneNumber, string code)
        {
            if (this._userService.Value.SmsService != null)
            {
                IdentityMessage message = new IdentityMessage
                {
                    Destination = phoneNumber,
                    Body = "Your security code is: " + code
                };
                await this._userService.Value.SmsService.SendAsync(message);
            }
        }

        public async Task<SignInStatusExEnum> ReLoginAsync(string userId, HttpContextDto context, Func<SignInStatusExEnum, int, HttpContextDto, IList<Claim>, Task> afterSuccessAction = null)
        {
            VisitPayUser user = await this._userService.Value.FindByIdAsync(userId).ConfigureAwait(true);
            return await this.ReLogin(user, context, afterSuccessAction).ConfigureAwait(true);
        }

        public void SignOut(string userName, bool sessionTimeout)
        {
            this.LogLogout(userName, sessionTimeout, HttpContext.Current);
            HttpContext.Current.AbandonSession();
        }

        public bool IsUsernameAvailable(string userName, int? userId = null)
        {
            VisitPayUser visitPayUser = this._userService.Value.FindByNameAsync(userName).Result;

            if (visitPayUser == null)
            {
                if (this._legacyUserService.Value.IsNonMigratedLegacyUser(userName))
                {
                    return false;
                }

                return true;
            }

            if (userId.HasValue && visitPayUser.VisitPayUserId == userId.Value)
            {
                return true;
            }

            return false;
        }

        public bool IsUsernameValid(string userName)
        {
            return (userName.TrimNullSafe() ?? string.Empty).Length >= SystemConstants.GuarantorUsernameMinLength;
        }

        public IdentityResult CreatePatient(VisitPayUserDto user, string password, string registrationMatchString, int termsOfUseId, int esignTermsId, int? paymentDueDay, IList<SecurityQuestionAnswerDto> securityQuestionAnswerDtos, int guarantorPasswordExpLimitInDays, DateTime? patientDateOfBirth)
        {
            VisitPayUser visitPayUser = this.MapVisitPayUser(user);

            VisitPayRole role = this.GetPatientRole();
            if (role == null)
            {
                return new IdentityResult("Could not find role \"Patient\"");
            }

            visitPayUser.VisitPayUserGuid = Guid.NewGuid();

            IdentityResult createResult;
            using (UnitOfWork unitOfWork = new UnitOfWork(this._session))
            {
                visitPayUser.Roles.Add(role);

                visitPayUser.AddAcknowledgement(VisitPayUserAcknowledgementTypeEnum.TermsOfUse, termsOfUseId);
                this.AcknowledgeEsign(visitPayUser, esignTermsId);

                createResult = this._userService.Value.Create(visitPayUser, password);
                if (createResult.Succeeded)
                {
                    this.AddSecurityQuestionToUser(securityQuestionAnswerDtos, visitPayUser);
                    this._userService.Value.Update(visitPayUser);

                    this._userService.Value.SetPasswordExpiration(visitPayUser, guarantorPasswordExpLimitInDays);
                    this.SavePasswordHistory(visitPayUser, 0);

                    //vp-5912
                    this.AddEmailCommunicationPreferences(visitPayUser.VisitPayUserId);

                    // Calculate the paymentDueDay if it is null
                    if (paymentDueDay == null)
                    {
                        paymentDueDay = DateTime.UtcNow.AddDays(this.Client.Value.GracePeriodLength).Day;
                    }
                    // Make the day be 31 aka "EndOfMonthIndicator" if the value is 28 or higher
                    paymentDueDay = paymentDueDay.Value.ToValidPaymentDueDay();

                    Guarantor guarantor = new Guarantor
                    {
                        User = visitPayUser,
                        TermsOfUseCmsVersionId = termsOfUseId,
                        PaymentDueDay = (int)paymentDueDay,
                        RegistrationDate = DateTime.UtcNow
                    };

                    guarantor.SetGuarantorType(GuarantorTypeEnum.Online);

                    this._guarantorService.Value.SetToActiveVisitsLoaded(guarantor);

                    this._guarantorService.Value.InsertOrUpdateGuarantor(guarantor);

                    bool matchingApiIsEnabled = this.FeatureService.Value.IsFeatureEnabled(VisitPayFeatureEnum.FeatureMatchingApiIsEnabled);
                    if (matchingApiIsEnabled)
                    {
                        this.AddInitialMatchesWithApi(guarantor, registrationMatchString, patientDateOfBirth);
                    }
                    else
                    {
                        this.PublishEnrollGuarantorMessage(guarantor);
                        int? hsGuarantorId = this.AddInitialMatchForGuarantor(guarantor, visitPayUser, registrationMatchString, patientDateOfBirth);

                        if (hsGuarantorId.HasValue)
                        {
                            this._session.RegisterPostCommitSuccessAction((vpgid, hsgids) =>
                            {
                                this.Bus.Value.PublishMessage(new FirstVisitLoadedMessage
                                { 
                                    HsGuarantorIds = hsgids,
                                    VpGuarantorId = vpgid
                                });
                            }, guarantor.VpGuarantorId, hsGuarantorId.Value.ToListOfOne());
                        }
                    }

                    unitOfWork.Commit();

                    this.InsertOrUpdateGuarantorWithNextStatementDate(guarantor);
                    this._metricsProvider.Value.Increment(Metrics.Increment.VisitPayUser.NewUser.VisitPayUser);

                    this.Bus.Value.PublishMessage(new SendPostRegistrationWelcomeMessage
                    {
                        Email = guarantor.User.Email,
                        Name = guarantor.User.FirstName,
                        VisitPayUserId = guarantor.User.VisitPayUserId
                    }).Wait();
                }
                else
                {
                    unitOfWork.Rollback();
                }
            }
            return createResult;
        }

        public GuarantorDto CreateOfflinePatient(VisitPayUserDto user, string registrationMatchString, DateTime? patientDateOfBirth, int? paymentDueDay = null)
        {
            if (!this.FeatureService.Value.IsFeatureEnabled(VisitPayFeatureEnum.OfflineVisitPay))
            {
                throw new Exception($"{nameof(VisitPayFeatureEnum.OfflineVisitPay)} is disabled");
            }

            VisitPayUser visitPayUser = this.MapVisitPayUser(user);
            VisitPayRole role = this.GetPatientRole();
            if (role == null)
            {
                return null;
            }

            visitPayUser.VisitPayUserGuid = Guid.NewGuid();

            // Calculate the paymentDueDay if it is null
            if (paymentDueDay == null)
            {
                paymentDueDay = DateTime.UtcNow.AddDays(this.Client.Value.GracePeriodLength).Day;
            }

            // Make the day be 31 aka "EndOfMonthIndicator" if the value is 28 or higher
            paymentDueDay = paymentDueDay.Value.ToValidPaymentDueDay();

            using (UnitOfWork unitOfWork = new UnitOfWork(this._session))
            {
                visitPayUser.Roles.Add(role);
                IdentityResult createResult = this._userService.Value.Create(visitPayUser);
                if (createResult.Succeeded)
                {
                    this._userService.Value.GenerateAccessTokenPhiForUser(visitPayUser.VisitPayUserId);

                    Guarantor guarantor = new Guarantor
                    {
                        User = visitPayUser,
                        TermsOfUseCmsVersionId = -1,
                        PaymentDueDay = paymentDueDay.Value,
                        RegistrationDate = DateTime.UtcNow,
                    };

                    guarantor.SetGuarantorType(GuarantorTypeEnum.Offline);

                    //TODO -- should this be closed?
                    this._guarantorService.Value.SetToActiveVisitsLoaded(guarantor);
                    this._guarantorService.Value.InsertOrUpdateGuarantor(guarantor);

                    bool matchingApiIsEnabled = this.FeatureService.Value.IsFeatureEnabled(VisitPayFeatureEnum.FeatureMatchingApiIsEnabled);
                    if (matchingApiIsEnabled)
                    {
                        this.AddInitialMatchesWithApi(guarantor, registrationMatchString, patientDateOfBirth);
                    }
                    else
                    {
                        this.PublishEnrollGuarantorMessage(guarantor);
                        this.AddInitialMatchForGuarantor(guarantor, visitPayUser, registrationMatchString, patientDateOfBirth);
                    }

                    unitOfWork.Commit();

                    this.InsertOrUpdateGuarantorWithNextStatementDate(guarantor);
                    this._metricsProvider.Value.Increment(Metrics.Increment.VisitPayUser.NewUser.OfflineVisitPayUser);

                    string metric = guarantor.UseAutoPay ? Metrics.Increment.Offline.AutoPay : Metrics.Increment.Offline.NonAutoPay;
                    this._metricsProvider.Value.Increment(metric);

                    return Mapper.Map<GuarantorDto>(guarantor);
                }
            }

            return null;
        }

        public void AddMailCommunicationPreferences(JournalEventHttpContextDto context, int visitPayUserId, int loggedInVisitPayUserId)
        {
            IList<CommunicationGroup> communicationGroups = this._communicationService.Value.CommunicationGroups;
            if (communicationGroups != null)
            {
                foreach (CommunicationGroup communicationGroup in communicationGroups)
                {
                    this._communicationService.Value.AddPreference(visitPayUserId, CommunicationMethodEnum.Mail, communicationGroup.CommunicationGroupId);
                }

                JournalEventUserContext journalEventUserContext = this._userJournalEventService.Value.GetJournalEventUserContext(context, loggedInVisitPayUserId);
                this._userJournalEventService.Value.LogClientOptedInCommunicationTypeForVpGuarantor(journalEventUserContext, visitPayUserId, CommunicationMethodEnum.Mail);
            }
        }

        private void AddEmailCommunicationPreferences(int visitPayUserId)
        {
            IList<CommunicationGroup> communicationGroups = this._communicationService.Value.CommunicationGroups;
            if (communicationGroups != null)
            {
                foreach (CommunicationGroup communicationGroup in communicationGroups)
                {
                    this._communicationService.Value.AddPreference(visitPayUserId, CommunicationMethodEnum.Email, communicationGroup.CommunicationGroupId);
                }
            }
        }

        public void AddEmailCommunicationPreferences(JournalEventHttpContextDto context, int visitPayUserId, int loggedInVisitPayUserId)
        {
            this.AddEmailCommunicationPreferences(visitPayUserId);
            JournalEventUserContext journalEventUserContext = this._userJournalEventService.Value.GetJournalEventUserContext(context, loggedInVisitPayUserId);
            this._userJournalEventService.Value.LogClientOptedInCommunicationTypeForVpGuarantor(journalEventUserContext, visitPayUserId, CommunicationMethodEnum.Email);
        }

        public void RemoveMailCommunicationPreferences(JournalEventHttpContextDto context, int visitPayUserId, int loggedInVisitPayUserId)
        {
            IList<CommunicationGroup> communicationGroups = this._communicationService.Value.CommunicationGroups;
            if (communicationGroups != null)
            {
                foreach (CommunicationGroup communicationGroup in communicationGroups)
                {
                    this._communicationService.Value.RemovePreference(visitPayUserId, CommunicationMethodEnum.Mail, communicationGroup.CommunicationGroupId);
                }

                JournalEventUserContext journalEventUserContext = this._userJournalEventService.Value.GetJournalEventUserContext(context, loggedInVisitPayUserId);
                this._userJournalEventService.Value.LogClientOptedInCommunicationTypeForVpGuarantor(journalEventUserContext, visitPayUserId, CommunicationMethodEnum.Mail);
            }
        }

        public void RemoveEmailCommunicationPreferences(JournalEventHttpContextDto context, int visitPayUserId, int loggedInVisitPayUserId)
        {
            IList<CommunicationGroup> communicationGroups = this._communicationService.Value.CommunicationGroups;
            if (communicationGroups != null)
            {
                foreach (CommunicationGroup communicationGroup in communicationGroups)
                {
                    this._communicationService.Value.RemovePreference(visitPayUserId, CommunicationMethodEnum.Email, communicationGroup.CommunicationGroupId);
                }

                JournalEventUserContext journalEventUserContext = this._userJournalEventService.Value.GetJournalEventUserContext(context, loggedInVisitPayUserId);
                this._userJournalEventService.Value.LogClientOptedOutCommunicationTypeForVpGuarantor(journalEventUserContext, visitPayUserId, CommunicationMethodEnum.Email);
            }
        }

        public bool UserHasCommunicationPreference(int visitPayUserId, CommunicationMethodEnum communicationMethod, CommunicationGroupEnum? communicationGroup = null)
        {
            IList<VisitPayUserCommunicationPreference> visitPayUserCommunicationPreferences = this._communicationService.Value.CommunicationPreferences(visitPayUserId);
            IEnumerable<VisitPayUserCommunicationPreference> communicationPreferences= visitPayUserCommunicationPreferences?.Where(x => x.CommunicationMethodId == communicationMethod);

            if (communicationGroup.HasValue)
            {
                communicationPreferences = communicationPreferences?.Where(x => x.CommunicationGroupId == (int) communicationGroup);
            }

            bool hasCommunicationPreference = communicationPreferences != null && communicationPreferences.Any();
            return hasCommunicationPreference;
        }

        private bool AddInitialMatchesWithApi(Guarantor guarantor, string sourceSystemKey, DateTime? patientDateOfBirth)
        {
            MatchPatientDto matchPatient = new MatchPatientDto()
            {
                PatientDOB = patientDateOfBirth
            };
            MatchGuarantorDto matchGuarantor = Mapper.Map<MatchGuarantorDto>(guarantor.User);
            matchGuarantor.SourceSystemKey = sourceSystemKey;

            this._session.Transaction.RegisterPostCommitSuccessAction((mPatient, mGuarantor, vpguarantorId) =>
            {
                this.Bus.Value.PublishMessage(new AddInitialMatchesToCdiMessage
                {
                    MatchPatientDto = mPatient,
                    MatchGuarantorDto = mGuarantor,
                    VpGuarantorId = vpguarantorId
                }).Wait();
            }, matchPatient, matchGuarantor, guarantor.VpGuarantorId);

            return true;
        }

        private void InsertOrUpdateGuarantorWithNextStatementDate(Guarantor guarantor)
        {
            using (UnitOfWork anotherUow = new UnitOfWork(this._session))
            {
                guarantor.NextStatementDate = this._statementApplicationService.Value.GetNextStatementDate(guarantor.VpGuarantorId, DateTime.UtcNow);
                this._guarantorService.Value.InsertOrUpdateGuarantor(guarantor);
                anotherUow.Commit();
            }
        }

        private int? AddInitialMatchForGuarantor(Guarantor guarantor, VisitPayUser visitPayUser, string registrationMatchString, DateTime? patientDateOfBirth)
        {
            if (registrationMatchString == null)
            {
                //Make sure that the HsGuarantorExists
                throw new Exception("When creating a patient it must be associated with a registrationMatchString");
            }

            int? hsGuarantorId = this._hsGuarantorService.Value.AddInitialMatch(guarantor, registrationMatchString, patientDateOfBirth, Mapper.Map<HttpContextDto>(HttpContext.Current));
            if (hsGuarantorId.HasValue)
            {
                this.LogGuarantorMatch(visitPayUser, hsGuarantorId.Value, guarantor.VpGuarantorId, "Registration");
            }
            return hsGuarantorId;
        }

        private VisitPayUser MapVisitPayUser(VisitPayUserDto userDto)
        {
            VisitPayUser visitPayUser = Mapper.Map<VisitPayUser>(userDto);
            visitPayUser.InsertDate = DateTime.UtcNow;

            VisitPayUserAddress physicalAddress = this.MapVisitPayUserPhysicalAddress(userDto, visitPayUser);
            visitPayUser.SetCurrentAddress(physicalAddress);

            return visitPayUser;
        }

        private VisitPayUserAddress MapVisitPayUserPhysicalAddress(VisitPayUserDto userDto, VisitPayUser user)
        {
            VisitPayUserAddress physicalAddress = new VisitPayUserAddress
            {
                VisitPayUser = user,
                AddressStreet1 = userDto.AddressStreet1,
                AddressStreet2 = userDto.AddressStreet2,
                City = userDto.City,
                PostalCode = userDto.Zip,
                StateProvince = userDto.State,
                //the default is Physical, setting it explicitly for visibility
                VisitPayUserAddressType = VisitPayUserAddressTypeEnum.Physical
            };
            return physicalAddress;
        }

        private VisitPayUserAddress MapVisitPayUserMailingAddress(VisitPayUserDto userDto, VisitPayUser user)
        {
            VisitPayUserAddress mailingAddress = new VisitPayUserAddress
            {
                VisitPayUser = user,
                AddressStreet1 = userDto.MailingAddressStreet1,
                AddressStreet2 = userDto.MailingAddressStreet2,
                City = userDto.MailingCity,
                StateProvince = userDto.MailingState,
                PostalCode = userDto.MailingZip,
                VisitPayUserAddressType = VisitPayUserAddressTypeEnum.Mailing
            };

            return mailingAddress;
        }

        private VisitPayRole GetPatientRole()
        {
            return this._visitPayRoleService.Value.GetRoleByName(VisitPayRoleStrings.System.Patient);
        }

        private void PublishEnrollGuarantorMessage(Guarantor guarantor)
        {
            this._session.Transaction.RegisterPostCommitSuccessAction((p1) =>
            {
                try
                {
                    Task.Run(async () => await this._guarantorEnrollmentApplicationService.Value.PublishEnrollGuarantorMessageAsync(p1.VpGuarantorId)).Wait();
                }
                catch
                {
                    this.LoggingService.Value.Fatal(() => $"VisitPayUserApplicationService::CreatePatient - Failed to PublishEnrollGuarantorMessageAsync - guarantorId = {p1.VpGuarantorId}");
                }
            }, guarantor);
        }

        public Task<IdentityResult> IsPasswordStrongEnough(string password)
        {
            return this._userService.Value.IsPasswordStrongEnough(password);
        }

        public GuarantorDto GetGuarantorFromVisitPayUserId(int visitPayUserId)
        {
            int guarantorId = this._guarantorService.Value.GetGuarantorId(visitPayUserId);
            Guarantor guarantor = this._guarantorService.Value.GetGuarantor(guarantorId);
            return Mapper.Map<GuarantorDto>(guarantor);
        }

        public int GetGuarantorIdFromVisitPayUserId(int visitPayUserId)
        {
            return this._guarantorService.Value.GetGuarantorId(visitPayUserId);
        }

        public int GetVisitPayUserId(int vpGuarantorId)
        {
            return this._guarantorService.Value.GetVisitPayUserId(vpGuarantorId);
        }

        public DateTime? GetLastLoginDate(int visitPayUserId)
        {
            VpUserLoginSummary vpUserLoginSummary = this._vpUserLoginSummaryService.Value.GetUserLoginSummary(visitPayUserId);
            return vpUserLoginSummary != null ? vpUserLoginSummary.LastLoginDate : (DateTime?)null;
        }

        public async Task<string> GetGuarantorPasswordRequirements()
        {
            Client client = this.ClientService.Value.GetClient();

            string strengthCharsKey = client.GuarantorIsStrongPassword ? TextRegionConstants.GuarantorPasswordRequirementsStrong : TextRegionConstants.GuarantorPasswordRequirementsBasic;
            string strengthCharsText = await this._contentService.Value.GetTextRegionAsync(strengthCharsKey);
            string instructionText = await this._contentService.Value.GetTextRegionAsync(TextRegionConstants.GuarantorPasswordRequirementsSentence, new Dictionary<string, string>
            {
                {"[[StrengthChars]]", strengthCharsText}
            });

            return instructionText;
        }

        public async Task<string> GetGuarantorUsernameRequirements()
        {
            string text = await this._contentService.Value.GetTextRegionAsync(TextRegionConstants.GuarantorUserNameRequirements, new Dictionary<string, string>
            {
                {"[[GuarantorUsernameMinLength]]", SystemConstants.GuarantorUsernameMinLength.ToString()}
            });

            return text;
        }

        public string GetClientPasswordRequirements()
        {
            Client client = this.ClientService.Value.GetClient();

            string strengthChars = client.ClientIsStrongPassword ? "upper Case, lower Case, numbers and symbols." : "upper Case, lower case and numbers.";
            string instructionText = $"Your password must contain a minimum of {client.ClientPasswordMinLength} characters and include the following: {strengthChars}";

            return instructionText;
        }

        public bool IsPasswordExpired(VisitPayUserDto visitPayUserDto)
        {
            return this.IsPasswordExpired(visitPayUserDto.PasswordExpiresUtc);
        }

        public bool IsTempPasswordExpired(VisitPayUserDto visitPayUserDto)
        {
            if (!visitPayUserDto.TempPasswordExpDate.HasValue)
            {
                return false;
            }

            return DateTime.Compare(visitPayUserDto.TempPasswordExpDate.Value, DateTime.UtcNow) < 1;
        }

        public void TermsAcknowledgement(string visitPayUserId, VisitPayUserAcknowledgementTypeEnum acknowledgementType, int cmsVersionId, bool removeClaim = true)
        {
            VisitPayUser user = this._userService.Value.FindByIdAsync(visitPayUserId).Result;
            using (UnitOfWork unitOfWork = new UnitOfWork(this._session))
            {
                user.AddAcknowledgement(acknowledgementType, cmsVersionId);
                unitOfWork.Commit();

                if (removeClaim)
                {
                    this.RemoveClaim(new[] { nameof(ClaimTypeEnum.RequiresAcknowledgement) });
                }
            }
        }

        public bool RequestPasswordForGuarantor(int guarantorId, int tempPasswordExpLimitInHours)
        {
            Guarantor guarantor = this._guarantorService.Value.GetGuarantor(guarantorId);
            VisitPayUser visitPayUser = this._userService.Value.FindByIdAsync(guarantor.User.Id).Result;
            int targetUserId = visitPayUser.VisitPayUserId;
            this.RequestPassword(visitPayUser, tempPasswordExpLimitInHours);
            visitPayUser = this._userService.Value.FindByIdAsync(HttpContext.Current.User.Identity.GetUserId()).Result;
            this.LogRequestPassword(visitPayUser, HttpContext.Current, guarantorId, targetUserId);
            return true;
        }

        public bool RequestPassword(int visitPayUserId, int tempPasswordExpLimitInHours)
        {
            VisitPayUser visitPayUser = this._userService.Value.FindByIdAsync(visitPayUserId.ToString(CultureInfo.InvariantCulture)).Result;
            this.RequestPassword(visitPayUser, tempPasswordExpLimitInHours);
            this.LogRequestPassword(visitPayUser, HttpContext.Current);
            return true;
        }

        public void RemoveClaim(string[] claimTypes)
        {
            ClaimsIdentity identity = new ClaimsIdentity(HttpContext.Current.User.Identity);

            bool removed = false;

            foreach (string claimType in claimTypes)
            {
                Claim claim = identity.FindFirst(claimType);
                if (claim == null)
                {
                    continue;
                }
                identity.RemoveClaim(claim);
                removed = true;
            }

            if (removed)
            {
                this.RefreshIdentity(identity);
            }
        }

        public void RemoveClaimWithValue(string value)
        {
            ClaimsIdentity identity = new ClaimsIdentity(HttpContext.Current.User.Identity);
            Claim claim = identity.Claims?.FirstOrDefault(x => x.Value == value);
            if (claim != null)
            {
                identity.RemoveClaim(claim);
            }
            this.RefreshIdentity(identity);
        }

        public void UpdateClaim(ClaimTypeEnum claimTypeEnum, string value, int visitPayUserId)
        {
            if (HttpContext.Current == null || HttpContext.Current.User == null || HttpContext.Current.User.Identity == null)
            {
                return;
            }

            if (HttpContext.Current.User.CurrentUserId() != visitPayUserId)
            {
                return;
            }

            ClaimsIdentity identity = HttpContext.Current.User.Identity as ClaimsIdentity;
            if (identity == null)
            {
                return;
            }

            // check for existing claim and remove it
            Claim existingClaim = identity.FindFirst(claimTypeEnum.ToString());
            if (existingClaim != null)
            {
                identity.RemoveClaim(existingClaim);
            }

            // add new claim
            identity.AddClaim(new Claim(claimTypeEnum.ToString(), value));

            this.RefreshIdentity(identity);
        }

        public bool? IsUserEligibleForHealthEquityOutboundSso(int vpGuarantorId)
        {
            if (this.FeatureService.Value.IsFeatureEnabled(VisitPayFeatureEnum.HealthEquityFeature)
                && this.FeatureService.Value.IsFeatureEnabled(VisitPayFeatureEnum.HealthEquityShowOutboundSso))
            {
                return true;
            }
            return null;
        }

        private bool RequiresAcknowledgement(VisitPayUser user)
        {
            CmsVersion cmsVersion = this._contentService.Value.GetCurrentVersionAsync(CmsRegionEnum.VppTermsAndConditions, false).Result;
            if (cmsVersion != null)
            {
                List<VisitPayUserAcknowledgement> applicableAcknowledgements = user.Acknowledgements.Where(x => x.VisitPayUserAcknowledgementType == VisitPayUserAcknowledgementTypeEnum.TermsOfUse).ToList();

                return applicableAcknowledgements.All(a => a.CmsVersionId != cmsVersion.CmsVersionId);
            }
            return false;
        }

        private void RequestPassword(VisitPayUser visitPayUser, int tempPasswordExpLimitInHours)
        {
            using (UnitOfWork unitOfWork = new UnitOfWork(this._session))
            {
                string tempPassword = this._userService.Value.GeneratePasswordResetTokenAsync(visitPayUser.VisitPayUserId.ToString(CultureInfo.InvariantCulture)).Result;
                visitPayUser.TempPasswordExpDate = DateTime.UtcNow.AddHours(tempPasswordExpLimitInHours);
                visitPayUser.LockoutEndDateUtc = DateTime.UtcNow;
                visitPayUser.LockoutReasonEnum = null;
                unitOfWork.Commit();

                this.Bus.Value.PublishMessage(new SendPasswordWithoutPinEmailMessage
                {
                    Email = visitPayUser.Email,
                    Name = visitPayUser.FirstName,
                    TempPassword = tempPassword,
                    VisitPayUserId = visitPayUser.VisitPayUserId
                }).Wait();
            }
        }

        private void AddClaimConsolidationStatus(GuarantorDto guarantorDto, ICollection<Claim> claims)
        {
            claims.Add(new Claim(nameof(ClaimTypeEnum.ConsolidationStatus), guarantorDto.ConsolidationStatus.ToString()));
        }

        private void AddClaimHealthEquityOutboundSso(GuarantorDto guarantorDto, ICollection<Claim> claims)
        {
            bool? isEligible = this.IsUserEligibleForHealthEquityOutboundSso(guarantorDto.VpGuarantorId);
            if (isEligible.HasValue)
            {
                claims.Add(new Claim(nameof(ClaimTypeEnum.HealthEquityOutbound), isEligible.Value.ToString()));
            }
        }

        private void AddClaimGuarantor(GuarantorDto guarantorDto, ICollection<Claim> claims)
        {
            claims.Add(new Claim(nameof(ClaimTypeEnum.GuarantorId), guarantorDto.VpGuarantorId.ToString()));
        }

        private void AddClaimLastLogin(VisitPayUser user, ICollection<Claim> claims)
        {
            if (!user.LastLoginDate.HasValue)
            {
                return;
            }

            claims.Add(new Claim(nameof(ClaimTypeEnum.LastLoginDate), user.LastLoginDate.Value.ToString(Format.DateTimeFormat)));
        }
        private void AddClaimUserInsertDate(VisitPayUser user, ICollection<Claim> claims)
        {
            claims.Add(new Claim(nameof(ClaimTypeEnum.VisitPayUserInsertDate), user.InsertDate.ToString(Format.DateTimeFormat)));
        }

        private void AddClaimRequiresAcknowledgement(VisitPayUser user, ICollection<Claim> claims)
        {
            if (!this.RequiresAcknowledgement(user))
            {
                return;
            }

            claims.Add(new Claim(nameof(ClaimTypeEnum.RequiresAcknowledgement), "true"));
        }

        private void AddClaimClientDisplayFirstNameLastName(GuarantorDto guarantorDto, ICollection<Claim> claims)
        {
            string firstName = guarantorDto?.User?.FirstName;
            string lastName = guarantorDto?.User?.LastName;

            claims.Add(new Claim(nameof(ClaimTypeEnum.ClientFirstName), firstName));
            claims.Add(new Claim(nameof(ClaimTypeEnum.ClientLastName), lastName));
        }

        private void AddClaimClientDisplayFirstNameLastName(VisitPayUser visitPayUser, ICollection<Claim> claims)
        {
            string firstName = visitPayUser?.FirstName;
            string lastName = visitPayUser?.LastName;

            claims.Add(new Claim(nameof(ClaimTypeEnum.ClientFirstName), firstName));
            claims.Add(new Claim(nameof(ClaimTypeEnum.ClientLastName), lastName));
        }

        private void AddClaimVisitPayUserGuid(VisitPayUser visitPayUser, ICollection<Claim> claims)
        {
            claims.Add(new Claim(nameof(ClaimTypeEnum.VisitPayUserGuid), visitPayUser.VisitPayUserGuid.ToString()));
        }

        private void AddScore(VisitPayUser user)
        {
            Guarantor guarantor = this._guarantorService.Value.GetGuarantor(user.UserName);
            if (guarantor != null && guarantor.HsGuarantorMaps.Any())
            {
                Dictionary<string, List<int>> hsGuarantorDictionary = guarantor?.HsGuarantorMaps
                    .GroupBy(x => x.SourceSystemKey)
                    .ToDictionary(k => k.Key, v => v.Select(f => f.BillingSystem.BillingSystemId).ToList());

                this._session.Transaction.RegisterPostCommitSuccessAction((p1) =>
                {
                    this.Bus.Value.PublishMessage(p1).Wait();
                }, new VpGuarantorScoreMessage
                {
                    VpGuarantorId = guarantor.VpGuarantorId,
                    HsGuarantors = hsGuarantorDictionary
                }
                );
            }
        }

        private void AddClaims(VisitPayUser user, IList<Claim> claims, string authenticationType)
        {
            if (!claims.Any())
            {
                return;
            }

            //DefaultAuthenticationTypes.ApplicationCookie
            ClaimsIdentity identity = this._userService.Value.CreateIdentity(user, authenticationType);
            claims.ForEach(claim =>
            {
                identity.AddClaim(claim);
            });

            this.RefreshIdentity(identity);
        }

        private void ClearSession()
        {
            HttpContext.Current.AbandonSession();
        }

        private void AddSecurityQuestionToUser(IEnumerable<SecurityQuestionAnswerDto> answerDtos, VisitPayUser visitPayUser)
        {
            List<SecurityQuestionAnswer> newAnswers = new List<SecurityQuestionAnswer>();

            foreach (SecurityQuestionAnswerDto answerDto in answerDtos)
            {
                SecurityQuestionAnswer existing = visitPayUser.SecurityQuestionAnswers.FirstOrDefault(x => x.SecurityQuestion.SecurityQuestionId == answerDto.SecurityQuestion.SecurityQuestionId);
                if (existing == null)
                {
                    SecurityQuestionAnswer answer = new SecurityQuestionAnswer
                    {
                        Answer = answerDto.Answer,
                        SecurityQuestion = new SecurityQuestion { SecurityQuestionId = answerDto.SecurityQuestion.SecurityQuestionId }
                    };
                    visitPayUser.SecurityQuestionAnswers.Add(answer);
                    newAnswers.Add(answer);
                }
                else
                {
                    existing.Answer = answerDto.Answer;
                    newAnswers.Add(existing);
                }
            }

            List<SecurityQuestionAnswer> answersToRemove = visitPayUser.SecurityQuestionAnswers
                .Where(x => !newAnswers
                    .Select(z => z.SecurityQuestionAnswerId)
                    .Contains(x.SecurityQuestionAnswerId))
                .ToList();

            answersToRemove.ForEach(x => visitPayUser.SecurityQuestionAnswers.Remove(x));
        }

        private void ClearSecurityQuestions(VisitPayUser visitPayUser)
        {
            visitPayUser.SecurityQuestionAnswers.Clear();
        }

        private bool IsPasswordExpired(DateTime? passwordExpiresUtc)
        {
            if (!passwordExpiresUtc.HasValue)
            {
                return false;
            }

            return DateTime.Compare(passwordExpiresUtc.Value, DateTime.UtcNow) < 1;
        }

        private bool HasSecurityQuestions(VisitPayUser user, int minimumNumberOfQuestions)
        {
            return user.SecurityQuestionAnswers.Count >= minimumNumberOfQuestions;
        }

        private void SavePasswordHistory(VisitPayUser visitPayUser, int? passwordHashConfiguration = null)
        {
            this._userService.Value.SavePasswordHistory(visitPayUser.VisitPayUserId, visitPayUser.PasswordHash, visitPayUser.PasswordExpiresUtc, passwordHashConfiguration);
        }

        private async Task<SignInStatusExEnum> ReLogin(VisitPayUser visitPayUser, HttpContextDto context, Func<SignInStatusExEnum, int, HttpContextDto, IList<Claim>, Task> afterSuccessAction = null, string authenticationType = DefaultAuthenticationTypes.ApplicationCookie)
        {
            if (visitPayUser == null)
            {
                return SignInStatusExEnum.Failure;
            }

            context.Session.Abandon();
            context.Response.SetSessionCookie();
            if (this._userService.Value.GetLockoutEnabledAsync(visitPayUser.Id).Result
                && this._userService.Value.IsLockedOutAsync(visitPayUser.Id).Result)
            {
                return SignInStatusExEnum.LockedOut;
            }

            if (this.IsPasswordExpired(visitPayUser.PasswordExpiresUtc))
            {
                return SignInStatusExEnum.PasswordExpired;
            }

            if (!string.IsNullOrEmpty(visitPayUser.TempPassword))
            {
                return SignInStatusExEnum.RequiresVerification;
            }

            await this._signInService.Value.SignInAsync(visitPayUser, false, false).ConfigureAwait(true);

            IList<Claim> claims = new List<Claim>();

            if (afterSuccessAction != null)
            {
                await afterSuccessAction(SignInStatusExEnum.Success, visitPayUser.VisitPayUserId, context, claims);
            }

            GuarantorDto guarantorDto = Mapper.Map<GuarantorDto>(this._guarantorService.Value.GetGuarantorEx(visitPayUser.VisitPayUserId));
            if (guarantorDto != null)
            {
                this.AddClaimConsolidationStatus(guarantorDto, claims);
                this.AddClaimGuarantor(guarantorDto, claims);
                this.AddClaimHealthEquityOutboundSso(guarantorDto, claims);
                this.AddClaimClientDisplayFirstNameLastName(guarantorDto, claims);
            }
            else
            {
                //Client user, add user's first/last name to claims
                this.AddClaimClientDisplayFirstNameLastName(visitPayUser, claims);
            }

            this.AddClaimVisitPayUserGuid(visitPayUser, claims);
            this.AddClaimRequiresAcknowledgement(visitPayUser, claims);
            this.AddClaims(visitPayUser, claims, authenticationType);

            this._vpUserLoginSummaryService.Value.SetUserLastLogin(visitPayUser.VisitPayUserId);

            if (this.Client.Value.ScoringIsEnabled)
            {
                this.AddScore(visitPayUser);
            }

            return SignInStatusExEnum.Success;
        }

        private async Task<IdentityResult> ChangePasswordAsync(VisitPayUser visitPayUser, string currentPassword, string newPassword, int passwordReuseLimit, int passwordExpLimitInDays)
        {
            if (visitPayUser == null)
            {
                return new IdentityResult(new List<string> { "User not found." });
            }

            if ((visitPayUser.IsLocked || visitPayUser.IsDisabled))
            {
                return new IdentityResult(new List<string> { "User is locked out." });
            }

            if (!this._userService.Value.VerifyPasswordHasNotBeenUsed(visitPayUser.VisitPayUserId, newPassword, passwordReuseLimit))
            {
                return new IdentityResult("You have recently used this password.  Please choose a different password.");
            }

            IdentityResult result = await this._userService.Value.ChangePasswordAsync(visitPayUser.VisitPayUserId.ToString(), currentPassword, newPassword).ConfigureAwait(true);
            if (!result.Succeeded)
            {
                return result;
            }

            this._userService.Value.SetPasswordExpiration(visitPayUser, passwordExpLimitInDays);
            this.SavePasswordHistory(visitPayUser);
            this._userService.Value.ResetAccessFailedCount(visitPayUser.VisitPayUserId.ToString());

            this.LogPasswordChange(visitPayUser, HttpContext.Current);

            return result;
        }

        public void RefreshRoles(IIdentity identity)
        {
            if (!(identity is ClaimsIdentity claimsIdentity))
            {
                return;
            }

            // remove existing role claims
            IEnumerable<Claim> existingRoleClaims = claimsIdentity.Claims.Where(x => x.Type == ClaimTypes.Role);
            foreach (Claim roleClaim in existingRoleClaims)
            {
                claimsIdentity.RemoveClaim(roleClaim);
            }

            // add new role claims
            IList<string> roles = this._userService.Value.GetRoles(identity.GetUserId());
            List<Claim> claims = roles.Select(role => new Claim(ClaimTypes.Role, role)).ToList();
            claimsIdentity.AddClaims(claims);
            this.RefreshIdentity(claimsIdentity);
        }

        private void RefreshIdentity(IIdentity identity)
        {
            this._authenticationManager.Value.AuthenticationResponseGrant = new AuthenticationResponseGrant(new ClaimsPrincipal(identity), new AuthenticationProperties { IsPersistent = true });
        }

        #region Phone and Text

        public void SendSmsValidationMessage(string visitPayUserId, SmsPhoneTypeEnum smsPhoneType, string token, JournalEventHttpContextDto context)
        {
            VisitPayUser visitPayUser = this._userService.Value.FindByIdAsync(visitPayUserId).Result;
            this.LogSmsTermsAccepted(visitPayUser, smsPhoneType == SmsPhoneTypeEnum.Primary ? visitPayUser.PhoneNumber : visitPayUser.PhoneNumberSecondary, context);
            this.Bus.Value.PublishMessage(new SendSmsValidationMessage
            {
                VisitPayUserId = visitPayUser.VisitPayUserId,
                Phone = smsPhoneType == SmsPhoneTypeEnum.Primary ? visitPayUser.PhoneNumber : visitPayUser.PhoneNumberSecondary,
                Token = token
            }).Wait();
            this._metricsProvider.Value.Increment(Metrics.Increment.Communication.Sms.AuthCodeSent);
        }

        public SmsPhoneTypeEnum? GetSmsAcknowledgement(string visitPayUserId)
        {
            VisitPayUser visitPayUser = this._userService.Value.FindByIdAsync(visitPayUserId).Result;
            VisitPayUserAcknowledgement acknowledgement = visitPayUser.Acknowledgements.FirstOrDefault(a => a.VisitPayUserAcknowledgementType == VisitPayUserAcknowledgementTypeEnum.SmsAccept);
            if (acknowledgement != null && acknowledgement.SmsPhoneType.HasValue)
            {
                return acknowledgement.SmsPhoneType.Value;
            }
            return null;
        }

        public void SetSmsAcknowledgement(string visitPayUserId, SmsPhoneTypeEnum smsPhoneType, int cmsVersionId, JournalEventHttpContextDto context)
        {
            VisitPayUser visitPayUser = this._userService.Value.FindByIdAsync(visitPayUserId).Result;
            VisitPayUserAcknowledgement acknowledgement = visitPayUser.Acknowledgements.FirstOrDefault(a => a.VisitPayUserAcknowledgementType == VisitPayUserAcknowledgementTypeEnum.SmsAccept);
            if (acknowledgement != null)
            {
                this.RemoveSmsAcknowledgement(
                    visitPayUserId: visitPayUserId,
                    fromSmsReply: false,
                    clientVisitPayUserId: null,
                    context: context
                );
            }
            using (UnitOfWork unitOfWork = new UnitOfWork(this._session))
            {
                visitPayUser.Acknowledgements.Add(new VisitPayUserAcknowledgement
                {
                    AcknowledgementDate = DateTime.UtcNow,
                    VisitPayUserAcknowledgementType = VisitPayUserAcknowledgementTypeEnum.SmsAccept,
                    CmsVersionId = cmsVersionId,
                    VisitPayUser = visitPayUser,
                    SmsPhoneType = smsPhoneType
                });
                this._userService.Value.Update(visitPayUser);
                if (!this._communicationService.Value.CommunicationPreferences(visitPayUser.VisitPayUserId).Any())
                {
                    this._communicationService.Value.SetPreferences(visitPayUser.VisitPayUserId, CommunicationMethodEnum.Sms, this._communicationService.Value.CommunicationGroups);
                }
                unitOfWork.Commit();
                this.SendSmsActivatedEmailAlert(visitPayUser, smsPhoneType == SmsPhoneTypeEnum.Primary ? visitPayUser.PhoneNumber : visitPayUser.PhoneNumberSecondary);
                this.LogSmsActivated(visitPayUser, smsPhoneType == SmsPhoneTypeEnum.Primary ? visitPayUser.PhoneNumber : visitPayUser.PhoneNumberSecondary, context);
                this._metricsProvider.Value.Increment(Metrics.Increment.Communication.Sms.OptIn);
            }
        }

        public void RemoveSmsAcknowledgement(string visitPayUserId, bool fromSmsReply, string clientVisitPayUserId, JournalEventHttpContextDto context)
        {
            VisitPayUser visitPayUser = this._userService.Value.FindByIdAsync(visitPayUserId).Result;
            VisitPayUserAcknowledgement acknowledgement = visitPayUser.Acknowledgements.FirstOrDefault(a => a.VisitPayUserAcknowledgementType == VisitPayUserAcknowledgementTypeEnum.SmsAccept);
            if (acknowledgement != null)
            {
                using (UnitOfWork unitOfWork = new UnitOfWork(this._session))
                {
                    this.RemoveSms(visitPayUser, acknowledgement);
                    unitOfWork.Commit();
                    this.DoSmsRemovalPostCommitActions(visitPayUser, acknowledgement, fromSmsReply, clientVisitPayUserId, context);
                }
            }
        }

        public void RemoveSmsAcknowledgements(string phoneNumber, JournalEventHttpContextDto context)
        {
            IList<VisitPayUser> visitPayUsers = this._userService.Value.AllWithSmsNumber(phoneNumber);
            foreach (VisitPayUser visitPayUser in visitPayUsers)
            {
                VisitPayUserAcknowledgement acknowledgement = visitPayUser.Acknowledgements.FirstOrDefault(a => a.VisitPayUserAcknowledgementType == VisitPayUserAcknowledgementTypeEnum.SmsAccept);
                if (acknowledgement != null)
                {
                    using (UnitOfWork unitOfWork = new UnitOfWork(this._session))
                    {
                        this.RemoveSms(visitPayUser, acknowledgement);
                        unitOfWork.Commit();
                        this.DoSmsRemovalPostCommitActions(visitPayUser, acknowledgement, true, null, context);
                    }
                }
            }
        }

        private void RemoveSms(VisitPayUser visitPayUser, VisitPayUserAcknowledgement acknowledgement)
        {
            bool hasOtherSmsAcknowledgements = visitPayUser.Acknowledgements
                .Any(a => a.VisitPayUserAcknowledgementType == VisitPayUserAcknowledgementTypeEnum.SmsAccept
                          && a.VisitPayUserAcknowledgementId != acknowledgement.VisitPayUserAcknowledgementId);

            visitPayUser.Acknowledgements.Remove(acknowledgement);

            if (!hasOtherSmsAcknowledgements)
            {
                IEnumerable<int> smsCommunicationGroupIds = this._communicationService.Value.CommunicationPreferences(visitPayUser.VisitPayUserId)
                    .Where(x => x.CommunicationMethodId == CommunicationMethodEnum.Sms).Select(x => x.CommunicationGroupId);

                foreach (int communicationGroupId in smsCommunicationGroupIds)
                {
                    this._communicationService.Value.RemovePreference(visitPayUser.VisitPayUserId, CommunicationMethodEnum.Sms, communicationGroupId);
                }
            }

            this._userService.Value.Update(visitPayUser);
        }

        private void DoSmsRemovalPostCommitActions(VisitPayUser visitPayUser, VisitPayUserAcknowledgement acknowledgement, bool fromSmsReply, string clientVisitPayUserId, JournalEventHttpContextDto context)
        {
            this.SendSmsDeactivatedEmailAlert(visitPayUser, acknowledgement.SmsPhoneType.HasValue ? acknowledgement.SmsPhoneType.Value == SmsPhoneTypeEnum.Primary ? visitPayUser.PhoneNumber : visitPayUser.PhoneNumberSecondary : "none");
            this.LogSmsDeactivated(
                visitPayUser: visitPayUser,
                phone: acknowledgement.SmsPhoneType.HasValue ? acknowledgement.SmsPhoneType.Value == SmsPhoneTypeEnum.Primary ? visitPayUser.PhoneNumber : visitPayUser.PhoneNumberSecondary : "none",
                fromSmsReply: fromSmsReply,
                clientVisitPayUserId: clientVisitPayUserId,
                context: context);
            this._metricsProvider.Value.Increment(Metrics.Increment.Communication.Sms.OptOut);
        }

        public IdentityResult UpdatePhone(string visitPayUserId, string phoneNumber, string phoneNumberType, bool isPrimary, JournalEventHttpContextDto context, string clientVisitPayUserId)
        {
            VisitPayUser visitPayUser = this._userService.Value.FindByIdAsync(visitPayUserId).Result;
            VisitPayUser clientVisitPayUser = !string.IsNullOrWhiteSpace(clientVisitPayUserId) ? this._userService.Value.FindByIdAsync(clientVisitPayUserId).Result : null;
            SmsPhoneTypeEnum? smsPhoneType = this.GetSmsAcknowledgement(visitPayUserId);

            if ((smsPhoneType == SmsPhoneTypeEnum.Primary && isPrimary && visitPayUser.PhoneNumber != phoneNumber)
                || (smsPhoneType == SmsPhoneTypeEnum.Secondary && !isPrimary && visitPayUser.PhoneNumberSecondary != phoneNumber))
            {
                this.RemoveSmsAcknowledgement(
                    visitPayUserId: visitPayUserId,
                    fromSmsReply: false,
                    clientVisitPayUserId: null,
                    context: context
                    );
            }

            if (visitPayUser == null
                || (isPrimary && visitPayUser.PhoneNumber == phoneNumber && visitPayUser.PhoneNumberType == phoneNumberType)
                || (!isPrimary && visitPayUser.PhoneNumberSecondary == phoneNumber && visitPayUser.PhoneNumberSecondaryType == phoneNumberType))
            {
                return IdentityResult.Success;
            }

            // Determine how the phone number was changed (for Journal Event purposes)

            bool isSamePrimaryPhoneType = isPrimary && visitPayUser.PhoneNumberType == phoneNumberType;
            bool isSameSecondaryPhoneType = !isPrimary && visitPayUser.PhoneNumberSecondaryType == phoneNumberType;

            bool addedPhoneNumber = isPrimary
                                    && string.IsNullOrWhiteSpace(visitPayUser.PhoneNumberType)
                                    && string.IsNullOrWhiteSpace(visitPayUser.PhoneNumber)
                                    && !string.IsNullOrWhiteSpace(phoneNumber);

            bool addedSecondaryPhoneNumber = !isPrimary
                                             && string.IsNullOrWhiteSpace(visitPayUser.PhoneNumberSecondaryType)
                                             && string.IsNullOrWhiteSpace(visitPayUser.PhoneNumberSecondary)
                                             && !string.IsNullOrWhiteSpace(phoneNumber);

            bool deletedPhoneNumber = isPrimary
                                      && !string.IsNullOrWhiteSpace(visitPayUser.PhoneNumber)
                                      && string.IsNullOrWhiteSpace(phoneNumber);

            bool deletedSecondaryPhoneNumber = !isPrimary
                                               && !string.IsNullOrWhiteSpace(visitPayUser.PhoneNumberSecondary)
                                               && string.IsNullOrWhiteSpace(phoneNumber);

            bool modifiedPhoneNumber = isPrimary
                                       && ((!string.IsNullOrWhiteSpace(visitPayUser.PhoneNumber)
                                       && !string.IsNullOrWhiteSpace(phoneNumber)
                                       && visitPayUser.PhoneNumber != phoneNumber)
                                       || !isSamePrimaryPhoneType);

            bool modifiedSecondaryPhoneNumber = !isPrimary
                                                && ((!string.IsNullOrWhiteSpace(visitPayUser.PhoneNumberSecondary)
                                                && !string.IsNullOrWhiteSpace(phoneNumber)
                                                && visitPayUser.PhoneNumberSecondary != phoneNumber)
                                                || !isSameSecondaryPhoneType);

            string originalPhoneNumber;
            string originalPhoneNumberType;

            // Assign new values

            if (isPrimary)
            {
                originalPhoneNumber = visitPayUser.PhoneNumber;
                originalPhoneNumberType = visitPayUser.PhoneNumberType;
                visitPayUser.PhoneNumber = phoneNumber;
                visitPayUser.PhoneNumberType = phoneNumberType;
            }
            else
            {
                originalPhoneNumber = visitPayUser.PhoneNumberSecondary;
                originalPhoneNumberType = visitPayUser.PhoneNumberSecondaryType;
                visitPayUser.PhoneNumberSecondary = phoneNumber;
                visitPayUser.PhoneNumberSecondaryType = phoneNumberType;
            }

            IdentityResult identityResult = this._userService.Value.Update(visitPayUser);

            if (!identityResult.Succeeded)
            {
                return identityResult;
            }

            bool fromClient = !string.IsNullOrWhiteSpace(clientVisitPayUserId);

            // Log the phone number action to the Journal
            if (addedPhoneNumber || addedSecondaryPhoneNumber)
            {
                if (fromClient)
                {
                    this.LogClientAddedVpGuarantorPhoneNumberJournalEvent(visitPayUser, context, phoneNumberType, phoneNumber, clientVisitPayUser);
                }
                else
                {
                    this.LogGuarantorAddedVpGuarantorPhoneNumberJournalEvent(visitPayUser, context, phoneNumberType, phoneNumber);
                }
            }
            else if (deletedPhoneNumber || deletedSecondaryPhoneNumber)
            {
                if (fromClient)
                {
                    this.LogClientDeletedVpGuarantorPhoneNumberJournalEvent(visitPayUser, context, originalPhoneNumberType, originalPhoneNumber, clientVisitPayUser);
                }
                else
                {
                    this.LogGuarantorDeletedPhoneNumberJournalEvent(visitPayUser, context, originalPhoneNumberType, originalPhoneNumber);
                }
            }
            else if (modifiedPhoneNumber || modifiedSecondaryPhoneNumber)
            {
                if (fromClient)
                {
                    this.LogClientModifiedVpGuarantorPhoneNumberJournalEvent(visitPayUser, context, phoneNumberType, phoneNumber, originalPhoneNumber, clientVisitPayUser);
                }
                else
                {
                    this.LogGuarantorModifiedPhoneNumberJournalEvent(visitPayUser, context, phoneNumberType, phoneNumber, originalPhoneNumber);
                }
            }

            this.SendProfileChangedNotification(visitPayUser);

            return identityResult;
        }

        public void LogInvalidBillingIdEvent(string errorMessage, string visitPayUserId, int vpGuarantorId)
        {
            VisitPayUser visitPayUser = this._userService.Value.FindByIdAsync(visitPayUserId).Result;
            JournalEventUserContext userEventHistoryContext = this._userJournalEventService.Value.GetJournalEventUserContext(Mapper.Map<JournalEventHttpContextDto>(HttpContext.Current), visitPayUser);

            this._userJournalEventService.Value.LogBillingIdFailure(userEventHistoryContext, vpGuarantorId, errorMessage);
        }

        public IdentityResult SecondaryPhoneDelete(string visitPayUserId, JournalEventHttpContextDto context, string clientVisitPayUserId)
        {
            VisitPayUser visitPayUser = this._userService.Value.FindByIdAsync(visitPayUserId).Result;
            VisitPayUser clientVisitPayUser = !string.IsNullOrWhiteSpace(clientVisitPayUserId) ? this._userService.Value.FindByIdAsync(clientVisitPayUserId).Result : null;
            SmsPhoneTypeEnum? smsPhoneType = this.GetSmsAcknowledgement(visitPayUserId);
            if (smsPhoneType != null && smsPhoneType == SmsPhoneTypeEnum.Secondary)
            {
                this.RemoveSmsAcknowledgement(
                    visitPayUserId: visitPayUserId,
                    fromSmsReply: false,
                    clientVisitPayUserId: null,
                    context: context
                    );
            }

            if (visitPayUser == null || (string.IsNullOrWhiteSpace(visitPayUser.PhoneNumberSecondary) && string.IsNullOrWhiteSpace(visitPayUser.PhoneNumberSecondaryType)))
            {
                return IdentityResult.Success;
            }

            string sourcePhoneNumberSecondary = visitPayUser.PhoneNumberSecondary;
            string sourcePhoneNumberSecondaryType = visitPayUser.PhoneNumberSecondaryType;

            visitPayUser.PhoneNumberSecondary = string.Empty;
            visitPayUser.PhoneNumberSecondaryType = string.Empty;

            IdentityResult identityResult = this._userService.Value.Update(visitPayUser);

            if (!identityResult.Succeeded)
            {
                return identityResult;
            }

            bool fromClient = !string.IsNullOrWhiteSpace(clientVisitPayUserId);

            if (fromClient)
            {
                this.LogClientDeletedVpGuarantorPhoneNumberJournalEvent(visitPayUser, context, sourcePhoneNumberSecondaryType, sourcePhoneNumberSecondary, clientVisitPayUser);
            }
            else
            {
                this.LogGuarantorDeletedPhoneNumberJournalEvent(visitPayUser, context, sourcePhoneNumberSecondaryType, sourcePhoneNumberSecondary);
            }

            this.SendProfileChangedNotification(visitPayUser);

            return identityResult;
        }

        #endregion

        #region Email Notifications

        private void SendProfileChangedNotification(VisitPayUser visitPayUser)
        {
            this._session.Transaction.RegisterPostCommitSuccessAction((p1) =>
            {
                this.Bus.Value.PublishMessage(p1).Wait();
            }, new SendUserProfileChangeEmailMessage
            {
                Email = visitPayUser.Email,
                Name = visitPayUser.FirstName,
                VisitPayUserId = visitPayUser.VisitPayUserId
            });
        }

        private void SendEmailUpdatedNotification(VisitPayUser visitPayUser, string sourceEmail)
        {
            this._session.Transaction.RegisterPostCommitSuccessAction((p1) =>
            {
                this.Bus.Value.PublishMessage(p1).Wait();
            }, new SendUserProfileChangeEmailMessage
            {
                Email = sourceEmail,
                Name = visitPayUser.FirstName,
                VisitPayUserId = visitPayUser.VisitPayUserId
            });
        }

        private void SendUsernameChangedNotification(VisitPayUser visitPayUser)
        {
            this._session.Transaction.RegisterPostCommitSuccessAction((p1) =>
            {
                this.Bus.Value.PublishMessage(p1).Wait();
            }, new SendAccountCredentialChangePatientEmailMessage
            {
                Email = visitPayUser.Email,
                Name = visitPayUser.FirstName,
                VisitPayUserId = visitPayUser.VisitPayUserId
            });
        }

        private void SendSmsActivatedEmailAlert(VisitPayUser visitPayUser, string phone)
        {
            this._session.Transaction.RegisterPostCommitSuccessAction((p1) =>
            {
                this.Bus.Value.PublishMessage(p1).Wait();
            }, new SendSmsActivateAlertEmailMessage
            {
                Email = visitPayUser.Email,
                Name = visitPayUser.FirstName,
                VisitPayUserId = visitPayUser.VisitPayUserId,
                Phone = phone
            });
        }

        private void SendSmsDeactivatedEmailAlert(VisitPayUser visitPayUser, string phone)
        {
            this._session.Transaction.RegisterPostCommitSuccessAction((p1) =>
            {
                this.Bus.Value.PublishMessage(p1).Wait();
            }, new SendSmsDeactivateAlertEmailMessage
            {
                Email = visitPayUser.Email,
                Name = visitPayUser.FirstName,
                VisitPayUserId = visitPayUser.VisitPayUserId,
                Phone = phone
            });
        }

        private void SendAccountLockoutEmail(VisitPayUser visitPayUser)
        {
            this._session.Transaction.RegisterPostCommitSuccessAction(() =>
            {
                if (visitPayUser.IsInClientRole)
                {
                    this.Bus.Value.PublishMessage(new SendClientAccountLockoutEmailMessage
                    {
                        Name = visitPayUser.FirstName,
                        Email = visitPayUser.Email,
                        VisitPayUserId = visitPayUser.VisitPayUserId
                    }).Wait();
                }
                else
                {
                    this.Bus.Value.PublishMessage(new SendGuarantorAccountLockoutEmailMessage
                    {
                        Name = visitPayUser.FirstName,
                        Email = visitPayUser.Email,
                        VisitPayUserId = visitPayUser.VisitPayUserId
                    }).Wait();
                }
            });
        }

        #endregion

        #region emulate user

        public SignInStatusExEnum EmulateTokenSignIn(string token)
        {
            HttpContext.Current.InializeSession();

            using (UnitOfWork unitOfWork = new UnitOfWork(this._session))
            {
                VisitPayUser user = this._userService.Value.FindByEmulateToken(token);
                if (user == null || !this._userService.Value.VerifyUserTokenAsync(user.Id, TokenPurposeEnum.EmulateUser.ToString(), token).Result)
                {
                    //Event removed from JournalEvent
                    //this.LogEmulateEvent(user, false, user?.Id ?? "", Mapper.Map<JournalEventHttpContextDto>(HttpContext.Current));
                    return SignInStatusExEnum.Failure;
                }
                VisitPayUser emulateUser = this._userService.Value.FindByIdAsync(user.EmulateUserId.ToString()).Result;
                if (emulateUser == null)
                {
                    //Event removed from JournalEvent
                    //this.LogEmulateEvent(user, false, user?.Id ?? "", Mapper.Map<JournalEventHttpContextDto>(HttpContext.Current));
                    return SignInStatusExEnum.Failure;
                }

                ClaimsIdentity emulateIdentifier = this._userService.Value.CreateIdentity(emulateUser, DefaultAuthenticationTypes.ApplicationCookie);

                ICollection<Claim> claims = new List<Claim>
                {
                    new Claim(nameof(ClaimTypeEnum.EmulatedUser), "true"),
                    new Claim(nameof(ClaimTypeEnum.EmulatedUserFirstName), emulateUser.FirstName),
                    new Claim(nameof(ClaimTypeEnum.EmulatedUserLastName), emulateUser.LastName),
                    new Claim(nameof(ClaimTypeEnum.ClientUsername), user.UserName),
                    new Claim(nameof(ClaimTypeEnum.ClientUserId), user.VisitPayUserId.ToString(CultureInfo.InvariantCulture)),
                    new Claim(nameof(ClaimTypeEnum.EmulationToken), token)
                };

                GuarantorDto guarantorDto = Mapper.Map<GuarantorDto>(this._guarantorService.Value.GetGuarantorEx(emulateUser.VisitPayUserId));
                if (guarantorDto != null)
                {
                    this.AddClaimConsolidationStatus(guarantorDto, claims);
                    this.AddClaimGuarantor(guarantorDto, claims);
                }

                claims.ForEach(claim =>
                {
                    emulateIdentifier.AddClaim(claim);
                });

                this._signInService.Value.SignIn(emulateIdentifier);
                this.LogEmulateEvent(user, true, emulateUser.UserName, Mapper.Map<JournalEventHttpContextDto>(HttpContext.Current));
                unitOfWork.Commit();
                return SignInStatusExEnum.Success;
            }
        }

        public string GenerateEmulateToken(string visitPayUserId, string emulateUserId, JournalEventHttpContextDto context)
        {
            VisitPayUser emulateUser = this._userService.Value.FindByIdAsync(emulateUserId).Result;
            if (emulateUser != null)
            {
                using (UnitOfWork unitOfWork = new UnitOfWork(this._session))
                {
                    string token = this._userService.Value.GenerateEmulateTokenAsync(visitPayUserId, emulateUser.VisitPayUserId).Result;

                    if (!string.IsNullOrWhiteSpace(token))
                    {
                        this.LogRequestEmulate(visitPayUserId, emulateUser.VisitPayUserId, context);
                        unitOfWork.Commit();
                        return token;
                    }

                    this.LogRequestEmulateFailure(visitPayUserId, emulateUser.VisitPayUserId, context);
                    unitOfWork.Rollback();
                }
            }
            return string.Empty;
        }

        #endregion

        #region forgot credentials

        public async Task<bool> RequestUsernameGuarantor(VisitPayUserFindByDetailsParametersDto visitPayUserFindByDetailsParameters)
        {
            DataResult<VisitPayUser, FindUserEnum> result = await this._userService.Value.FindByDetailsAsync(visitPayUserFindByDetailsParameters).ConfigureAwait(true);
            if (result.Result == FindUserEnum.NotFound || result.Result == FindUserEnum.MoreThanOneFound)
            {
                this.LoggingService.Value.Info(() => $"VisitPayUserApplicationService::RequestUsernameGuarantor - could not find {visitPayUserFindByDetailsParameters.LastName}, {visitPayUserFindByDetailsParameters.DateOfBirth.Date}, {visitPayUserFindByDetailsParameters.Ssn4}");
                return false;
            }

            if (result.Result == FindUserEnum.AccountLocked)
            {
                this.LoggingService.Value.Info(() => $"VisitPayUserApplicationService::RequestUsernameGuarantor - {result.Data.UserName} disabled ({result.Data.IsDisabled}), locked ({result.Data.IsLocked})");
                return false;
            }

            this.SendUsernameReminderEmail(result.Data);
            this.LogRequestGuarantorUsername(result.Data, HttpContext.Current);

            return true;
        }

        public async Task<bool> RequestPasswordGuarantor(VisitPayUserFindByDetailsParametersDto visitPayUserFindByDetailsParameters, JournalEventHttpContextDto context)
        {
            string username = visitPayUserFindByDetailsParameters.Username;
            string lastName = visitPayUserFindByDetailsParameters.LastName;
            DateTime dateOfBirth = visitPayUserFindByDetailsParameters.DateOfBirth;
            string ssn4String = (visitPayUserFindByDetailsParameters.UseSsn) ? visitPayUserFindByDetailsParameters.Ssn4.ToString() : "";

            VisitPayUser visitPayUser = await this._userService.Value.FindByNameAsync(username).ConfigureAwait(true);
            if (visitPayUser == null)
            {
                this.LogGuarantorRequestPasswordVerificationFailed(null, context, username, lastName, dateOfBirth, ssn4String);
                return false;
            }

            // verify the user is correct. 2 paths, use Ssn4 or use Username
            bool userIsVerified = (visitPayUserFindByDetailsParameters.UseSsn) ?
                visitPayUser.Verify(lastName, dateOfBirth, visitPayUserFindByDetailsParameters.Ssn4.GetValueOrDefault()) :
                visitPayUser.VerifyWithoutSsn4(lastName, visitPayUserFindByDetailsParameters.FirstName, dateOfBirth);

            if (!userIsVerified)
            {
                this.LogGuarantorRequestPasswordVerificationFailed(visitPayUser, context, username, lastName, dateOfBirth, ssn4String);
                return false;
            }

            this.RequestPassword(visitPayUser, this.ClientService.Value.GetClient().GuarantorTempPasswordExpLimitInHours);
            this.LogRequestPassword(visitPayUser, HttpContext.Current);

            return true;
        }

        public SecurityQuestionDto GetRandomSecurityQuestion(int visitPayUserId, int? currentSecurityQuestionId)
        {
            VisitPayUser visitPayUser = this._userService.Value.FindByIdAsync(visitPayUserId.ToString(CultureInfo.InvariantCulture)).Result;
            List<SecurityQuestionAnswer> questions = visitPayUser.SecurityQuestionAnswers.Where(s => !currentSecurityQuestionId.HasValue || s.SecurityQuestion.SecurityQuestionId != currentSecurityQuestionId).ToList();
            Random rnd = new Random();
            // Maxvalue parameter of Next() is an exclusive upperbound (effectively making it count-1)
            int index = rnd.Next(0, questions.Count);
            return questions.Count == 0 ? null : Mapper.Map<SecurityQuestionDto>(questions[index].SecurityQuestion);
        }

        public bool VerifyTempPassword(string password, string checkPassword)
        {
            return this._userService.Value.VerifyTempPassword(password, checkPassword);
        }

        /// <summary>
        /// Validates the user supplied security code against the generated one in the database and logs the result
        /// </summary>
        /// <param name="visitPayUserId">user id to lookup security code for</param>
        /// <param name="secureCode">user supplied security code to be validated</param>
        /// <param name="context">user supplied http context</param>
        /// <returns></returns>
        public async Task<bool> ValidateSecureCodeAsync(int visitPayUserId, string secureCode, JournalEventHttpContextDto context)
        {
            VisitPayUser visitPayUser = this._userService.Value.FindByIdAsync(visitPayUserId.ToString(CultureInfo.InvariantCulture)).Result;

            // The security code is presently numeric, so this comparison would need to be evaluated against future business rules to allow alphanumeric security codes
            bool validSecureCode = string.Compare(secureCode, visitPayUser.SecurityValidationValue, CultureInfo.InvariantCulture, CompareOptions.OrdinalIgnoreCase) == 0;

            if (!validSecureCode)
            {
                await this._userService.Value.IncrementAccessFailedAttemptsAfterSecureCodeFailureAsync(visitPayUser);
            }
            this.LogSecureCodeResult(visitPayUser, validSecureCode, context);
            return validSecureCode;
        }

        /// <summary>
        /// Validates security questions without creating a new temporary password for the user
        /// </summary>
        /// <param name="visitPayUserId"></param>
        /// <param name="securityQuestionId"></param>
        /// <param name="challengeText"></param>
        /// <returns></returns>
        public bool ForgotPasswordValidateSecurityQuestion(int visitPayUserId, int securityQuestionId, string challengeText, JournalEventHttpContextDto context)
        {
            VisitPayUser visitPayUser = this._userService.Value.FindByIdAsync(visitPayUserId.ToString(CultureInfo.InvariantCulture)).Result;
            if (this.IsSecurityQuestionChallengeCorrect(visitPayUser, securityQuestionId, challengeText))
            {
                this.LogChallengeQuestion(visitPayUser, true, context);
                return true;
            }
            this.LogChallengeQuestion(visitPayUser, false, context);
            return false;
        }

        private void SendUsernameReminderEmail(VisitPayUser visitPayUser)
        {
            this.Bus.Value.PublishMessage(new SendUsernameReminderEmailMessage
            {
                Email = visitPayUser.Email,
                Name = visitPayUser.FirstName,
                UserName = visitPayUser.UserName,
                VisitPayUserId = visitPayUser.VisitPayUserId
            }).Wait();
        }

        private bool IsSecurityQuestionChallengeCorrect(VisitPayUser visitPayUser, int securityQuestionId, string challengeText)
        {
            return visitPayUser.SecurityQuestionAnswers.Any(x => x.SecurityQuestion.SecurityQuestionId == securityQuestionId && x.Answer.Equals(challengeText, StringComparison.OrdinalIgnoreCase));
        }

        #endregion

        #region Audit Logging (High Level)

        private void LogLogin(VisitPayUser visitPayUser, SignInStatusExEnum status, HttpContextDto context, string passwordUsed)
        {
            if (visitPayUser == null)
            {
                return;
            }

            bool successfulLogin = status.IsInCategory(SignInStatusExEnumCategory.Success);
            bool? isLocked = successfulLogin ? (bool?)null : visitPayUser.IsLocked;
            bool? isDisabled = successfulLogin ? (bool?)null : visitPayUser.IsDisabled;
            string hashedPassword = successfulLogin ? null : this._userService.Value.HashPasswordWithCurrentConfiguration(passwordUsed);

            if (successfulLogin)
            {
                this.LogGuarantorSuccessfulLoginDirectJournalEvent(visitPayUser, context);
            }
            else
            {
                this.LogGuarantorFailedLoginJournalEvent(visitPayUser, context, isDisabled ?? false, isLocked ?? false, hashedPassword);
                return;
            }

            this._vpUserLoginSummaryService.Value.SetUserLastLogin(visitPayUser.VisitPayUserId);

            this.IncrementSignInStatusCounter(status);
        }

        #endregion

        #region client user specific

        public IReadOnlyList<VisitPayUserDto> GetVisitPayClientUsers()
        {
            VisitPayUserResults visitPayUserResults = this._userService.Value.GetVisitPayClientUsers(new VisitPayUserFilter(), 0, 999);

            return Mapper.Map<VisitPayUserResultsDto>(visitPayUserResults).VisitPayUsers;
        }

        public VisitPayUserResultsDto GetVisitPayClientUsers(VisitPayUserFilterDto visitPayUserFilterDto, int currentVisitPayUserId, int batch, int batchSize)
        {
            VisitPayUserFilter filter = Mapper.Map<VisitPayUserFilter>(visitPayUserFilterDto);

            if (!this._userService.Value.IsInRole(currentVisitPayUserId.ToString(), VisitPayRoleStrings.Miscellaneous.IvhAdmin))
            {
                VisitPayRole iVinciAdminRole = this._visitPayRoleService.Value.GetRoleByName(VisitPayRoleStrings.Miscellaneous.IvhAdmin);
                VisitPayRole iVinciEmployeeRole = this._visitPayRoleService.Value.GetRoleByName(VisitPayRoleStrings.Miscellaneous.IvhEmployee);
                filter.RolesExclude = new List<int> { iVinciAdminRole.VisitPayRoleId, iVinciEmployeeRole.VisitPayRoleId };
            }

            VisitPayUserResults visitPayUserResults = this._userService.Value.GetVisitPayClientUsers(filter, Math.Max(batch - 1, 0), batchSize);

            return Mapper.Map<VisitPayUserResultsDto>(visitPayUserResults);
        }

        public IList<VisitPayUserDto> GetVisitPayClientUsersByRole(IList<int> roles, bool? isDisabled, IList<int> excludedRoles)
        {
            VisitPayUserFilter filter = Mapper.Map<VisitPayUserFilter>(new VisitPayUserFilterDto
            {
                Roles = roles
            });

            filter.IsDisabled = isDisabled;

            if (excludedRoles != null && excludedRoles.Count > 0)
            {
                filter.RolesExclude = excludedRoles;
            }

            VisitPayUserResults visitPayUserResults = this._userService.Value.GetVisitPayClientUsers(filter, 0, int.MaxValue);

            return Mapper.Map<IList<VisitPayUserDto>>(visitPayUserResults.VisitPayUsers);
        }

        public IList<VisitPayRoleDto> GetClientUserRoles(bool isIvhAdmin)
        {
            return Mapper.Map<List<VisitPayRoleDto>>(this._visitPayRoleService.Value.GetClientUserRoles(isIvhAdmin));
        }

        public async Task<IdentityResult> CreateClientUser(VisitPayUserDto user, IList<string> roles, int clientTempPasswordExpLimitInHours, int clientPasswordExpLimitInDays)
        {
            VisitPayUser visitPayUser = Mapper.Map<VisitPayUser>(user);
            visitPayUser.InsertDate = DateTime.UtcNow;
            visitPayUser.VisitPayUserGuid = Guid.NewGuid();

            VisitPayRole clientRole = this._visitPayRoleService.Value.GetRoleByName(VisitPayRoleStrings.System.Client);
            if (clientRole == null)
            {
                return new IdentityResult("Could not find role \"Client\"");
            }

            IdentityResult createResult;
            using (UnitOfWork unitOfWork = new UnitOfWork(this._session))
            {
                visitPayUser.Roles.Add(clientRole);
                roles.ForEach(role => visitPayUser.Roles.Add(this._visitPayRoleService.Value.GetRoleByName(role)));

                createResult = this._userService.Value.Create(visitPayUser);
                if (createResult.Succeeded)
                {
                    unitOfWork.Commit();
                }
                else
                {
                    unitOfWork.Rollback();
                }
            }

            if (createResult.Succeeded)
            {
                this._userService.Value.SetPasswordExpiration(visitPayUser, clientPasswordExpLimitInDays);
                await this.BeginResetPasswordClientAsync(visitPayUser.VisitPayUserId, clientTempPasswordExpLimitInHours, false, isNewAccount: true).ConfigureAwait(true);
            }

            return createResult;
        }

        public IdentityResult UpdateClientUser(VisitPayUserDto userDto, IList<string> roles, bool isLocked, bool isDisabled, bool isIvhAdmin)
        {
            VisitPayUser user = this._userService.Value.FindById(userDto.VisitPayUserId.ToString());
            IdentityResult identityResult = null;
            using (UnitOfWork uow = new UnitOfWork(this._session))
            {
                // info
                user.FirstName = userDto.FirstName;
                user.LastName = userDto.LastName;
                user.Email = userDto.Email;
                user.SecurityValidationValue = userDto.SecurityValidationValue;

                if ((userDto.IsLocked && !isLocked) || (userDto.IsDisabled && !isDisabled))
                {
                    // unlock
                    user.LockoutEndDateUtc = null;
                    user.LockoutReasonEnum = null;
                }

                if (!userDto.IsLocked && isLocked)
                {
                    // lock
                    user.LockoutEndDateUtc = DateTime.UtcNow.Add(this._userService.Value.DefaultAccountLockoutTimeSpan);
                    user.LockoutReasonEnum = LockoutReasonEnum.LoginFailed;
                }
                else if (!userDto.IsDisabled && isDisabled)
                {
                    // disable
                    user.LockoutEndDateUtc = DateTime.MaxValue;
                    user.LockoutReasonEnum = LockoutReasonEnum.AccountDisabled;
                }

                string[] rolesUnauthorizedToRemove = isIvhAdmin ?
                    new[]
                    {
                        VisitPayRoleStrings.System.Client
                    } :
                    new[]
                    {
                        VisitPayRoleStrings.System.Client,
                        VisitPayRoleStrings.Restricted.IvhPciAuditor,
                        VisitPayRoleStrings.Restricted.ClientPciAuditor,
                        VisitPayRoleStrings.Restricted.AuditReviewer,
                        VisitPayRoleStrings.Restricted.ClientUserAuditor,
                        VisitPayRoleStrings.Restricted.IvhUserAuditor
                    };
                // check existing roles
                List<VisitPayRole> rolesToRemove = user.Roles.Where(existingRole => !rolesUnauthorizedToRemove.Contains(existingRole.Name) && !roles.Contains(existingRole.Name)).ToList();
                rolesToRemove.ForEach(existingRole => user.Roles.Remove(existingRole));

                // add new roles
                foreach (string newRoleName in roles.Where(newRoleName => user.Roles.All(x => x.Name != newRoleName)))
                {
                    user.Roles.Add(this._visitPayRoleService.Value.GetRoleByName(newRoleName));
                }

                //
                identityResult = this._userService.Value.Update(user);
                uow.Commit();
            }
            return identityResult;
        }

        /// <summary>
        /// Returns user.IsDisabled.  Will return false if cannot find user.
        /// </summary>
        public bool DisableEnableVisitPayUser(int visitPayUserId, bool disable, int actionTakenByVisitPayUserId, JournalEventHttpContextDto context)
        {
            VisitPayUser user = this._userService.Value.FindById(visitPayUserId.ToString());
            Guarantor guarantor = this._guarantorService.Value.GetGuarantorEx(visitPayUserId);
            if (user != null)
            {
                if ((user.IsDisabled && !disable) || (!user.IsDisabled && disable))
                {
                    using (UnitOfWork uow = new UnitOfWork(this._session))
                    {
                        //This should always be set, as we always want the user to have the ablilty to get locked out
                        user.LockoutEnabled = true;
                        if (!user.IsDisabled && disable)
                        {
                            user.LockoutEndDateUtc = DateTime.MaxValue;
                            user.LockoutReasonEnum = LockoutReasonEnum.AccountDisabled;
                        }
                        else if (user.IsDisabled && !disable)
                        {
                            user.LockoutEndDateUtc = null;
                            user.LockoutReasonEnum = null;
                        }

                        VisitPayUser actionUser = this._userService.Value.FindById(actionTakenByVisitPayUserId.ToString());
                        this.LogClientUserDisableVpUser(user, disable, context, actionUser, guarantor != null ? guarantor.VpGuarantorId : (int?)null);
                        this._userService.Value.Update(user);
                        uow.Commit();
                    }
                }
                return user.IsDisabled;
            }
            return false;
        }

        public bool IsAccountEnabled(int visitPayUserId)
        {
            return this._userService.Value.IsAccountEnabled(visitPayUserId);
        }

        public async Task<byte[]> ExportVisitPayClientUsersAsync(VisitPayUserFilterDto visitPayUserFilter, int visitPayUserId, bool includeIvinciUsers, bool includeClientUsers)
        {
            const int maxExportRecords = 10000;
            IReadOnlyList<VisitPayUserDto> results = this.GetVisitPayClientUsers(visitPayUserFilter, visitPayUserId, 1, maxExportRecords).VisitPayUsers;
            if (!includeIvinciUsers)
            {
                //Exclude Ivh users
                results = results.Where(vpu => !vpu.Email.IsVisitPayEmailAddress())
                                 .ToList();
            }
            if (!includeClientUsers)
            {
                //Exclude non-Ivh users
                results = results.Where(vpu => vpu.Email.IsVisitPayEmailAddress())
                                 .ToList();
            }

            using (ExcelPackage package = new ExcelPackage())
            {
                System.Drawing.Image logo = await this._imageService.Value.GetExportLogoAsync();
                iVinciExcelExportBuilder builder = new iVinciExcelExportBuilder(package, "User Client Management Export");
                builder.WithLogo(logo)
                    .AddDataBlock(results, record => new List<Column>
                    {
                        new ColumnBuilder(record.LastName,"Last Name"),
                        new ColumnBuilder(record.FirstName,"First Name"),
                        new ColumnBuilder(record.UserName,"User Name"),
                        new ColumnBuilder(string.Join(",", record.Roles.Select(r => r.Name)),"Role(s)"),
                        new ColumnBuilder(record.InsertDate,"Date Added")
                        .WithStyle(StyleTypeEnum.DateFormatter, Format.DateTimeFormat).WithStyle(StyleTypeEnum.AlignCenter),
                        new ColumnBuilder(record.LastLoginDate,"Last Login")
                        .WithStyle(StyleTypeEnum.DateFormatter, Format.DateTimeFormat).WithStyle(StyleTypeEnum.AlignCenter),
                        new ColumnBuilder(record.IsLocked,"Locked"),
                        new ColumnBuilder(record.IsDisabled,"Disabled")
                    })
                    .WithNoRecordsFoundMessageOf("There are no client users to display at this time.")
                    .Build();

                return package.GetAsByteArray();
            }
        }

        public async Task<bool> ClientResetAccount(VisitPayUserDto userDto, string secureCode, int actionTakenByVisitPayUserId, JournalEventHttpContextDto context)
        {
            VisitPayUser user = this._userService.Value.FindById(userDto.VisitPayUserId.ToString());

            bool success = false;

            using (IUnitOfWork uow = new UnitOfWork(this._session))
            {
                user.SecurityValidationValue = secureCode;
                user.PasswordExpiresUtc = null;
                this.ClearSecurityQuestions(user);
                this._userService.Value.Update(user);
                success = await this.BeginResetPasswordClientAsync(user.VisitPayUserId, this.ClientService.Value.GetClient().ClientTempPasswordExpLimitInHours, false, isNewAccount: true);

                uow.Commit();
            }

            if (!success)
            {
                return false;
            }

            VisitPayUser actionUser = this._userService.Value.FindById(actionTakenByVisitPayUserId.ToString());
            JournalEventUserContext journalEventUserContext = this._userJournalEventService.Value.GetJournalEventUserContext(context, actionUser);
            this._userJournalEventService.Value.LogClientUserResetAccount(journalEventUserContext, user.VisitPayUserId);

            return true;
        }

        public async Task<bool> ClientResetClientPassword(int targetVisitPayUserId, int clientTempPasswordExpLimitInHours, int actionTakenByVisitPayUserId, JournalEventHttpContextDto context)
        {
            bool success = await this.BeginResetPasswordClientAsync(targetVisitPayUserId, clientTempPasswordExpLimitInHours, false);
            if (success)
            {
                VisitPayUser currentUser = this._userService.Value.FindById(actionTakenByVisitPayUserId.ToString());
                this.LogClientResetClientPassword(currentUser, context, targetVisitPayUserId);
            }

            return success;
        }

        public async Task<bool> BeginResetPasswordClientAsync(int visitPayUserId, int clientTempPasswordExpLimitInHours, bool log = true, bool isNewAccount = false)
        {
            VisitPayUser visitPayUser = await this._userService.Value.FindByIdAsync(visitPayUserId.ToString(CultureInfo.InvariantCulture)).ConfigureAwait(true);

            if (visitPayUser != null && (visitPayUser.IsLocked || visitPayUser.IsDisabled))
            {
                return false;
            }
            using (UnitOfWork unitOfWork = new UnitOfWork(this._session))
            {
                string tempPassword = await this._userService.Value.GeneratePasswordResetTokenAsync(visitPayUserId.ToString(CultureInfo.InvariantCulture)).ConfigureAwait(true);
                visitPayUser.TempPasswordExpDate = DateTime.UtcNow.AddHours(clientTempPasswordExpLimitInHours);
                visitPayUser.LockoutEndDateUtc = DateTime.UtcNow;
                visitPayUser.LockoutReasonEnum = null;

                if (log)
                {
                    this.LogRequestPassword(visitPayUser, HttpContext.Current);
                }

                this._session.Transaction.RegisterPostCommitSuccessAction((p1, p2) =>
                {
                    if (isNewAccount)
                    {
                        this.Bus.Value.PublishMessage(new SendNewUserClientPasswordWithoutPinEmailMessage
                        {
                            Email = p1.Email,
                            Name = p1.FirstName,
                            TempPassword = p2,
                            VisitPayUserId = p1.VisitPayUserId
                        }).Wait();
                    }
                    else
                    {
                        this.Bus.Value.PublishMessage(new SendClientPasswordWithoutPinEmailMessage
                        {
                            Email = p1.Email,
                            Name = p1.FirstName,
                            TempPassword = p2,
                            VisitPayUserId = p1.VisitPayUserId
                        }).Wait();
                    }

                }, visitPayUser, tempPassword);

                unitOfWork.Commit();
            }

            return true;
        }

        public async Task<IdentityResult> CompleteResetPasswordClientAsync(string userId, string tempPassword, string newPassword, int clientPasswordExpLimitInDays, int clientPasswordReuseLimit)
        {
            IdentityResult result = IdentityResult.Success;

            VisitPayUser visitPayUser = this._userService.Value.FindById(userId);
            if (visitPayUser != null && (visitPayUser.IsLocked || visitPayUser.IsDisabled))
            {
                return new IdentityResult(new List<string>() { "User is locked out." });
            }
            if (this._userService.Value.VerifyPasswordHasNotBeenUsed(visitPayUser.VisitPayUserId, newPassword, clientPasswordReuseLimit))
            {
                result = await this._userService.Value.ResetPasswordAsync(userId, tempPassword, newPassword).ConfigureAwait(true);
                if (result.Succeeded)
                {
                    visitPayUser.SecurityValidationValue = null;
                    this._userService.Value.SetPasswordExpiration(visitPayUser, clientPasswordExpLimitInDays);
                    this.SavePasswordHistory(visitPayUser);
                    this._userService.Value.ResetAccessFailedCount(visitPayUser.VisitPayUserId.ToString());
                    this.LogPasswordChange(visitPayUser, HttpContext.Current);

                    await this.Bus.Value.PublishMessage(new SendClientResetPasswordEmailMessage
                    {
                        Email = visitPayUser.Email,
                        Name = visitPayUser.FirstName,
                        VisitPayUserId = visitPayUser.VisitPayUserId
                    });
                }
            }
            else
            {
                result = new IdentityResult("You have recently used this password.  Please choose a different password.");
            }

            return result;
        }

        public async Task<IdentityResult> ChangePasswordClientAsync(string visitPayUserId, string currentPassword, string newPassword, int clientPasswordExpLimitInDays, int clientPasswordReuseLimit)
        {
            VisitPayUser visitPayUser = this._userService.Value.FindById(visitPayUserId);

            IdentityResult result = await this.ChangePasswordAsync(visitPayUser, currentPassword, newPassword, clientPasswordReuseLimit, clientPasswordExpLimitInDays).ConfigureAwait(true);

            return result;
        }

        public IdentityResult AddSecurityQuestionsClientUser(VisitPayUserDto userDto, bool clearExisting, JournalEventHttpContextDto context)
        {
            VisitPayUser user = this._userService.Value.FindById(userDto.VisitPayUserId.ToString());

            if (clearExisting)
            {
                user.SecurityQuestionAnswers.Clear();
            }

            this.AddSecurityQuestionToUser(userDto.SecurityQuestionAnswers, user);
            IdentityResult identityResult = this._userService.Value.Update(user);

            if (identityResult.Succeeded)
            {
                this.LogAddEditSecurityQuestions(
                    user: userDto,
                    context: context,
                    isClient: true);
            }

            return identityResult;
        }

        public async Task<SignInStatusExEnum> ClientReLoginAsync(VisitPayUserDto userDto, HttpContextDto context)
        {
            VisitPayUser user = this._userService.Value.FindById(userDto.VisitPayUserId.ToString());
            SignInStatusExEnum? result = await this.PreLoginCheck(user, null);
            if (result.HasValue)
            {
                return result.GetValueOrDefault(SignInStatusExEnum.Failure);
            }

            result = this.PostLoginCheck(user, null);

            if (result.HasValue)
            {
                return result.GetValueOrDefault(SignInStatusExEnum.Failure);
            }

            if (await this.ReLogin(user, context).ConfigureAwait(true) == SignInStatusExEnum.Success)
            {
                return SignInStatusExEnum.Success;
            }

            return SignInStatusExEnum.Failure;
        }

        public void SendClientProfileChangeNotification(int visitPayUserId)
        {
            VisitPayUser user = this._userService.Value.FindById(visitPayUserId.ToString());

            this._session.Transaction.RegisterPostCommitSuccessAction((p1) =>
            {
                this.Bus.Value.PublishMessage(p1).Wait();
            }, new SendClientProfileChangeEmailMessage
            {
                Email = user.Email,
                FirstName = user.FirstName,
                VisitPayUserId = user.VisitPayUserId
            });
        }

        public void DeactivateInactiveClientUsers()
        {
            this._userService.Value.DeactivateInactiveVisitPayClientUsers();
        }

        #endregion

        #region acknowledgements

        public void AcknowledgeEsign(int visitPayUserId, int esignCmsVersionId)
        {
            using (IUnitOfWork uow = new UnitOfWork(this._session))
            {
                VisitPayUser user = this._userService.Value.FindById(visitPayUserId.ToString());
                if (user != null)
                {
                    this.AcknowledgeEsign(user, esignCmsVersionId);
                }
                this._userService.Value.UpdateUser(user);
                uow.Commit();
            }
        }

        private void AcknowledgeEsign(VisitPayUser user, int esignCmsVersionId)
        {
            user.AddAcknowledgement(VisitPayUserAcknowledgementTypeEnum.EsignAct, esignCmsVersionId);
        }

        #endregion

        private void PublishAddressChangedMessage(int visitPayUserId)
        {
            this._session.Transaction.RegisterPostCommitSuccessAction((p1) =>
            {
                this.Bus.Value.PublishMessage(p1).Wait();
            }, new VisitPayUserAddressChangedMessage
            {
                VisitPayUserId = visitPayUserId
            });
        }

        public void SetUserLocale(string userId, string locale)
        {
            this._userService.Value.SetUserLocale(userId, locale);
        }

        public string GetUserLocale(string userId)
        {
            return this._userService.Value.GetUserLocale(userId);
        }

        #region Journal Logging

        private JournalEventContext GetJournalEventContextForGuarantor(VisitPayUser visitPayUser)
        {
            int vpGuarantorId = this.GetGuarantorIdFromVisitPayUserId(visitPayUser.VisitPayUserId);
            return new JournalEventContext
            {
                VpGuarantorId = vpGuarantorId,
                VisitPayUserId = visitPayUser.VisitPayUserId
            };
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="user"></param>
        /// <param name="hsGuarantorId"></param>
        /// <param name="vpGuarantorId"></param>
        /// <param name="whenMatched">If match is made during registration = "Registration", If match is made during routine recurring process = "Recurring"</param>
        private void LogGuarantorMatch(VisitPayUser user, int hsGuarantorId, int vpGuarantorId, string whenMatched)
        {
            JournalEventUserContext journalEventUserContext = this._userJournalEventService.Value.GetJournalEventUserContext(Mapper.Map<JournalEventHttpContextDto>(HttpContext.Current), user);
            this._userJournalEventService.Value.LogGuarantorMatch(journalEventUserContext, hsGuarantorId, vpGuarantorId, whenMatched);
        }

        private void LogSmsTermsAccepted(VisitPayUser visitPayUser, string phone, JournalEventHttpContextDto context)
        {
            JournalEventUserContext journalEventUserContext = this._userJournalEventService.Value.GetJournalEventUserContext(Mapper.Map<JournalEventHttpContextDto>(context), visitPayUser);
            JournalEventContext journalEventGuarantorContext = this.GetJournalEventContextForGuarantor(visitPayUser);

            this.Journal.AddEvent<EventGuarantorSmsTermsAccepted, JournalEventParameters>(
                EventGuarantorSmsTermsAccepted.GetParameters
                (
                    phoneNumber: phone,
                    vpUserName: visitPayUser.UserName
                ),
                journalEventUserContext,
                journalEventGuarantorContext
            );
        }

        private void LogGuarantorSuccessfulLoginDirectJournalEvent(VisitPayUser visitPayUser, HttpContextDto context)
        {

            JournalEventUserContext journalEventUserContext = this._userJournalEventService.Value.GetJournalEventUserContext(Mapper.Map<JournalEventHttpContextDto>(context), visitPayUser);

            if (journalEventUserContext.IsClientUser)
            {
                JournalEventContext journalEventClientContext = this._userJournalEventService.Value.GetJournalEventContextForClient(visitPayUser.VisitPayUserId, null);

                this.Journal.AddEvent<EventClientSuccessfulLoginDirect, JournalEventParameters>(
                    EventClientSuccessfulLoginDirect.GetParameters
                    (
                        applicationID: this.ApplicationSettingsService.Value.Application.Value.ToString(),
                        deviceType: journalEventUserContext.DeviceType.ToString(),
                        eventUserName: visitPayUser.UserName,
                        uRL: journalEventUserContext.Url
                    ),
                    journalEventUserContext,
                    journalEventClientContext
                );
            }
            else
            {
                JournalEventContext journalEventGuarantorContext = this.GetJournalEventContextForGuarantor(visitPayUser);

                this.Journal.AddEvent<EventGuarantorSuccessfulLoginDirect, JournalEventParameters>(
                    EventGuarantorSuccessfulLoginDirect.GetParameters
                    (
                        vpUserName: visitPayUser.UserName,
                        applicationId: this.ApplicationSettingsService.Value.Application.Value.ToString(),
                        deviceType: journalEventUserContext.DeviceType.ToString(),
                        url: journalEventUserContext.Url
                    ),
                    journalEventUserContext,
                    journalEventGuarantorContext
                );
            }


        }

        private void LogPasswordChange(VisitPayUser visitPayUser, HttpContext context)
        {
            JournalEventUserContext journalEventUserContext = this._userJournalEventService.Value.GetJournalEventUserContext(Mapper.Map<JournalEventHttpContextDto>(context), visitPayUser);

            if (journalEventUserContext.IsClientUser)
            {
                JournalEventContext clientJournalEventContext = this._userJournalEventService.Value.GetJournalEventContextForClient(visitPayUser.VisitPayUserId, null);

                this.Journal.AddEvent<EventClientSetNewPersonalPassword, JournalEventParameters>(
                    EventClientSetNewPersonalPassword.GetParameters
                    (
                        eventUserName: visitPayUser.UserName
                    ),
                    journalEventUserContext,
                    clientJournalEventContext
                );
            }
            else
            {
                this.Journal.AddEvent<EventGuarantorSetNewPersonalPassword, JournalEventParameters>(
                    EventGuarantorSetNewPersonalPassword.GetParameters
                    (
                        vpUserName: visitPayUser.UserName
                    ),
                    journalEventUserContext
                );
            }
        }

        private void LogLogout(string username, bool sessionTimeout, HttpContext context)
        {
            int userId = context != null ? context.User.CurrentUserId() : 0;
            VisitPayUser visitPayUser = this._userService.Value.FindById(userId.ToString());

            JournalEventUserContext journalEventUserContext = this._userJournalEventService.Value.GetJournalEventUserContext(Mapper.Map<JournalEventHttpContextDto>(context), visitPayUser);

            if (journalEventUserContext.IsClientUser)
            {
                JournalEventContext clientJournalEventContext = this._userJournalEventService.Value.GetJournalEventContextForClient(userId, null);
                if (sessionTimeout)
                {
                    this.Journal.AddEvent<EventSystemActionSessionTimedOut, JournalEventParameters>(
                        EventSystemActionSessionTimedOut.GetParameters(
                            clientUserName: username
                        ),
                        journalEventUserContext,
                        clientJournalEventContext);
                }
                else
                {
                    this.Journal.AddEvent<EventClientUserLoggedOut, JournalEventParameters>(
                        EventClientUserLoggedOut.GetParameters(
                            eventUserName: username
                        ),
                        journalEventUserContext,
                        clientJournalEventContext);
                }
            }
            else
            {
                int vpGuarantorId = this.GetGuarantorIdFromVisitPayUserId(userId);
                JournalEventContext guarantorJournalEventContext = this._userJournalEventService.Value.GetJournalEventContextForGuarantor(userId, vpGuarantorId);
                if (sessionTimeout)
                {
                    this.Journal.AddEvent<EventGuarantorSessionTimedOut, JournalEventParameters>(
                        EventGuarantorSessionTimedOut.GetParameters(
                            vpUserName: username
                        ),
                        journalEventUserContext,
                        guarantorJournalEventContext);
                }
                else
                {
                    this.Journal.AddEvent<EventGuarantorUserLoggedOut, JournalEventParameters>(
                        EventGuarantorUserLoggedOut.GetParameters(
                            vpUserName: username
                        ),
                        journalEventUserContext,
                        guarantorJournalEventContext);
                }
            }
        }

        private void LogRequestGuarantorUsername(VisitPayUser visitPayUser, HttpContext context)
        {
            JournalEventUserContext journalEventUserContext = this._userJournalEventService.Value.GetJournalEventUserContext(Mapper.Map<JournalEventHttpContextDto>(context), visitPayUser);
            JournalEventContext journalEventGuarantorContext = this.GetJournalEventContextForGuarantor(visitPayUser);

            this.Journal.AddEvent<EventGuarantorRequestedUserName, JournalEventParameters>(
                EventGuarantorRequestedUserName.GetParameters
                (
                    dateOfBirth: visitPayUser.DateOfBirth?.ToShortDateString(),
                    foundMatchFlag: JournalEventValue.Unknown,
                    sSN4: visitPayUser.SSN4?.ToString(),
                    submittedLastName: visitPayUser.LastName,
                    vpUserName: visitPayUser.UserName
                ),
                journalEventUserContext,
                journalEventGuarantorContext);
        }

        private void LogRequestPassword(VisitPayUser visitPayUser, HttpContext context, int? vpGuarantorId = null, int? targetUserId = null)
        {
            JournalEventUserContext journalEventUserContext = this._userJournalEventService.Value.GetJournalEventUserContext(Mapper.Map<JournalEventHttpContextDto>(context), visitPayUser);

            if (journalEventUserContext.IsClientUser)
            {
                JournalEventContext journalEventClientContext = this._userJournalEventService.Value.GetJournalEventContextForClient(visitPayUser.VisitPayUserId, vpGuarantorId);

                if (targetUserId != null && vpGuarantorId != null)
                {
                    VisitPayUser targetUser = this._userService.Value.FindById(targetUserId.ToString());
                    this.Journal.AddEvent<EventClientRequestedANewVpGuarantorPassword, JournalEventParameters>(
                        EventClientRequestedANewVpGuarantorPassword.GetParameters
                        (
                            eventUserName: visitPayUser.UserName,
                            vpUserName: targetUser.UserName
                        ),
                        journalEventUserContext,
                        journalEventClientContext);
                }
                else
                {
                    this.Journal.AddEvent<EventClientRequestedANewPersonalPassword, JournalEventParameters>(
                        EventClientRequestedANewPersonalPassword.GetParameters
                        (
                            eventUserName: visitPayUser.UserName
                        ),
                        journalEventUserContext,
                        journalEventClientContext);
                }
            }
            else
            {
                JournalEventContext journalEventGuarantorContext = this.GetJournalEventContextForGuarantor(visitPayUser);

                this.Journal.AddEvent<EventGuarantorRequestedANewPersonalPassword, JournalEventParameters>(
                    EventGuarantorRequestedANewPersonalPassword.GetParameters
                    (
                        dateOfBirth: visitPayUser.DateOfBirth?.ToShortDateString(),
                        foundMatchFlag: JournalEventValue.Unknown,
                        sSN4: visitPayUser.SSN4?.ToString(),
                        lastName: visitPayUser.LastName,
                        submittedUserName: visitPayUser.UserName,
                        vpUserName: visitPayUser.UserName
                    ),
                    journalEventUserContext,
                    journalEventGuarantorContext);
            }


        }

        private void LogGuarantorFailedLoginJournalEvent(VisitPayUser visitPayUser, HttpContextDto context, bool isDisabled, bool isLocked, string hashedPassword)
        {

            JournalEventUserContext journalEventUserContext = this._userJournalEventService.Value.GetJournalEventUserContext(Mapper.Map<JournalEventHttpContextDto>(context), visitPayUser);

            if (journalEventUserContext.IsClientUser)
            {
                JournalEventContext journalEventClientContext = this._userJournalEventService.Value.GetJournalEventContextForClient(visitPayUser.VisitPayUserId, null);

                this.Journal.AddEvent<EventClientFailedLogin, JournalEventParameters>(
                    EventClientFailedLogin.GetParameters
                    (
                        accountDisabled: Convert.ToString(isDisabled),
                        accountLocked: Convert.ToString(isLocked),
                        loginName: visitPayUser.UserName,
                        passwordHashed: hashedPassword
                    ),
                    journalEventUserContext,
                    journalEventClientContext
                );
            }
            else
            {
                JournalEventContext journalEventGuarantorContext = this.GetJournalEventContextForGuarantor(visitPayUser);

                this.Journal.AddEvent<EventGuarantorFailedLogin, JournalEventParameters>(
                    EventGuarantorFailedLogin.GetParameters
                    (
                        accountDisabled: Convert.ToString(isDisabled),
                        accountLocked: Convert.ToString(isLocked),
                        attemptedLoginName: visitPayUser.UserName,
                        loginName: visitPayUser.UserName,
                        passwordHashed: hashedPassword
                    ),
                    journalEventUserContext,
                    journalEventGuarantorContext
                );
            }


        }

        private void LogGuarantorDeletedPhoneNumberJournalEvent(VisitPayUser visitPayUser, JournalEventHttpContextDto context, string phoneType, string previousPhoneNumber)
        {
            JournalEventContext journalEventContext = this.GetJournalEventContextForGuarantor(visitPayUser);
            JournalEventUserContext journalEventUserContext = this._userJournalEventService.Value.GetJournalEventUserContext(context, visitPayUser);
            this.Journal.AddEvent<EventGuarantorDeletedPhoneNumber, JournalEventParameters>(
                EventGuarantorDeletedPhoneNumber.GetParameters(
                    phoneType: phoneType,
                    previousPhoneNumber: previousPhoneNumber,
                    vpUserName: visitPayUser.UserName),
                journalEventUserContext,
                journalEventContext
            );
        }

        private void LogClientDeletedVpGuarantorPhoneNumberJournalEvent(VisitPayUser visitPayUser, JournalEventHttpContextDto context, string phoneType, string deletedPhoneNumber, VisitPayUser clientVisitPayUser)
        {
            JournalEventUserContext journalEventUserContext = this._userJournalEventService.Value.GetJournalEventUserContext(context, visitPayUser);
            JournalEventContext journalEventContext = new JournalEventContext() { VisitPayUserId = clientVisitPayUser.VisitPayUserId };
            this.Journal.AddEvent<EventClientDeletedVpGuarantorPhoneNumber, JournalEventParameters>(
                EventClientDeletedVpGuarantorPhoneNumber.GetParameters(
                    phoneType: phoneType,
                    deletedPhoneNumber: deletedPhoneNumber,
                    vpUserName: visitPayUser.UserName,
                    eventUserName: clientVisitPayUser.UserName),
                journalEventUserContext,
                journalEventContext
            );
        }

        private void LogGuarantorAddedVpGuarantorPhoneNumberJournalEvent(VisitPayUser visitPayUser, JournalEventHttpContextDto context, string phoneType, string newPhoneNumber)
        {
            JournalEventContext journalEventContext = this.GetJournalEventContextForGuarantor(visitPayUser);
            JournalEventUserContext journalEventUserContext = this._userJournalEventService.Value.GetJournalEventUserContext(context, visitPayUser);
            this.Journal.AddEvent<EventGuarantorAddedVpGuarantorPhoneNumber, JournalEventParameters>(
                EventGuarantorAddedVpGuarantorPhoneNumber.GetParameters(
                    phoneType: phoneType,
                    newPhoneNumber: newPhoneNumber,
                    vpUserName: visitPayUser.UserName),
                journalEventUserContext,
                journalEventContext
            );
        }

        private void LogClientAddedVpGuarantorPhoneNumberJournalEvent(VisitPayUser visitPayUser, JournalEventHttpContextDto context, string phoneType, string newPhoneNumber, VisitPayUser clientVisitPayUser)
        {
            JournalEventUserContext journalEventUserContext = this._userJournalEventService.Value.GetJournalEventUserContext(context, visitPayUser);
            JournalEventContext journalEventContext = new JournalEventContext() { VisitPayUserId = clientVisitPayUser.VisitPayUserId };
            this.Journal.AddEvent<EventClientAddedVpGuarantorPhoneNumber, JournalEventParameters>(
                EventClientAddedVpGuarantorPhoneNumber.GetParameters(
                    phoneType: phoneType,
                    newPhoneNumber: newPhoneNumber,
                    vpUserName: visitPayUser.UserName,
                    eventUserName: clientVisitPayUser.UserName),
                journalEventUserContext,
                journalEventContext
            );
        }

        private void LogGuarantorModifiedPhoneNumberJournalEvent(VisitPayUser visitPayUser, JournalEventHttpContextDto context, string phoneType, string newPhoneNumber, string previousPhoneNumber)
        {
            JournalEventContext journalEventContext = this.GetJournalEventContextForGuarantor(visitPayUser);
            JournalEventUserContext journalEventUserContext = this._userJournalEventService.Value.GetJournalEventUserContext(context, visitPayUser);
            this.Journal.AddEvent<EventGuarantorModifiedPhoneNumber, JournalEventParameters>(
                EventGuarantorModifiedPhoneNumber.GetParameters(
                    newPhoneNumber: newPhoneNumber,
                    phoneType: phoneType,
                    previousPhoneNumber: previousPhoneNumber,
                    vpUserName: visitPayUser.UserName),
                journalEventUserContext,
                journalEventContext
            );
        }

        private void LogClientModifiedVpGuarantorPhoneNumberJournalEvent(VisitPayUser visitPayUser, JournalEventHttpContextDto context, string phoneType, string newPhoneNumber, string previousPhoneNumber, VisitPayUser clientVisitPayUser)
        {
            JournalEventUserContext journalEventUserContext = this._userJournalEventService.Value.GetJournalEventUserContext(context, visitPayUser);
            JournalEventContext journalEventContext = new JournalEventContext() { VisitPayUserId = clientVisitPayUser.VisitPayUserId };
            this.Journal.AddEvent<EventClientModifiedVpGuarantorPhoneNumber, JournalEventParameters>(
                EventClientModifiedVpGuarantorPhoneNumber.GetParameters(
                    eventUserName: clientVisitPayUser.UserName,
                    newPhoneNumber: newPhoneNumber,
                    phoneType: phoneType,
                    previousPhoneNumber: previousPhoneNumber,
                    vpUserName: visitPayUser.UserName),
                journalEventUserContext,
                journalEventContext
            );
        }

        private void LogSmsDeactivated(VisitPayUser visitPayUser, string phone, bool fromSmsReply, string clientVisitPayUserId, JournalEventHttpContextDto context)
        {
            JournalEventUserContext journalEventUserContext = this._userJournalEventService.Value.GetJournalEventUserContext(context, visitPayUser);

            if (clientVisitPayUserId != null)
            {
                JournalEventContext clientJournalEventContext = this._userJournalEventService.Value.GetJournalEventContextForClient(visitPayUser.VisitPayUserId, null);
                VisitPayUser clientVisitPayUser = !string.IsNullOrWhiteSpace(clientVisitPayUserId) ? this._userService.Value.FindByIdAsync(clientVisitPayUserId).Result : null;
                this.Journal.AddEvent<EventClientSmsDeactivated, JournalEventParameters>(
                    EventClientSmsDeactivated.GetParameters(
                        eventUserName: clientVisitPayUser?.UserName ?? "",
                        phoneNumber: phone,
                        vpUserName: visitPayUser.UserName),
                    journalEventUserContext,
                    clientJournalEventContext
                );
            }
            else
            {
                JournalEventContext guarantorJournalEventContext = this.GetJournalEventContextForGuarantor(visitPayUser);
                this.Journal.AddEvent<EventGuarantorSmsDeactivated, JournalEventParameters>(
                    EventGuarantorSmsDeactivated.GetParameters(
                        phoneNumber: phone,
                        vpUserName: visitPayUser.UserName),
                    journalEventUserContext,
                    guarantorJournalEventContext
                );
            }
        }

        private void LogSmsActivated(VisitPayUser visitPayUser, string phone, JournalEventHttpContextDto context)
        {
            JournalEventUserContext journalEventUserContext = this._userJournalEventService.Value.GetJournalEventUserContext(context, visitPayUser);
            JournalEventContext journalEventContext = this.GetJournalEventContextForGuarantor(visitPayUser);
            this.Journal.AddEvent<EventGuarantorSmsActivated, JournalEventParameters>(
                EventGuarantorSmsActivated.GetParameters(
                    phoneNumber: phone,
                    vpUserName: visitPayUser.UserName),
                journalEventUserContext,
                journalEventContext
            );
        }

        private void LogClientUserDisableVpUser(VisitPayUser visitPayUser, bool disabled, JournalEventHttpContextDto context, VisitPayUser vpUserTakingAction, int? vpGuarantorId)
        {
            JournalEventUserContext journalEventUserContext = this._userJournalEventService.Value.GetJournalEventUserContext(context, vpUserTakingAction);

            JournalEventContext journalEventContext = this.GetJournalEventContextForGuarantor(visitPayUser);
            if (disabled)
            {
                this.Journal.AddEvent<EventClientDisabledVpGuarantorLogin, JournalEventParameters>(
                    EventClientDisabledVpGuarantorLogin.GetParameters(
                        eventUserName: vpUserTakingAction.UserName,
                        vpUserName: visitPayUser.UserName),
                    journalEventUserContext,
                    journalEventContext
                );
            }
            else
            {
                this.Journal.AddEvent<EventClientEnabledVpGuarantorLogin, JournalEventParameters>(
                    EventClientEnabledVpGuarantorLogin.GetParameters(
                        eventUserName: vpUserTakingAction.UserName,
                        vpUserName: visitPayUser.UserName),
                    journalEventUserContext,
                    journalEventContext
                );
            }
        }

        private void LogSecureCodeResult(VisitPayUser visitPayUser, bool success, JournalEventHttpContextDto context)
        {
            JournalEventUserContext journalEventUserContext = this._userJournalEventService.Value.GetJournalEventUserContext(context, visitPayUser);
            JournalEventContext journalEventContext = this._userJournalEventService.Value.GetJournalEventContextForClient(visitPayUser.VisitPayUserId, null);

            if (success)
            {
                this.Journal.AddEvent<EventClientSuccessfulSecureCodeValidation, JournalEventParameters>(
                    EventClientSuccessfulSecureCodeValidation.GetParameters(
                        eventUserName: visitPayUser.UserName),
                    journalEventUserContext,
                    journalEventContext
                );
            }
            else
            {
                this.Journal.AddEvent<EventClientFailedSecureCodeValidation, JournalEventParameters>(
                    EventClientFailedSecureCodeValidation.GetParameters(
                        eventUserName: visitPayUser.UserName),
                    journalEventUserContext,
                    journalEventContext
                );
            }
        }

        public void LogAddEditSecurityQuestions(VisitPayUserDto user, JournalEventHttpContextDto context, bool isClient)
        {
            VisitPayUser visitPayUser = Mapper.Map<VisitPayUser>(user);
            JournalEventUserContext journalEventUserContext = this._userJournalEventService.Value.GetJournalEventUserContext(context, visitPayUser);
            JournalEventContext journalEventContext = this._userJournalEventService.Value.GetJournalEventContextForClient(visitPayUser.VisitPayUserId, null);
            if (isClient)
            {
                this.Journal.AddEvent<EventClientAddEditSecurityQuestions, JournalEventParameters>(
                    EventClientAddEditSecurityQuestions.GetParameters(
                        eventUserName: visitPayUser.UserName),
                    journalEventUserContext,
                    journalEventContext
                );
            }
            else
            {
                this.Journal.AddEvent<EventGuarantorAddEditSecurityQuestions, JournalEventParameters>(
                    EventGuarantorAddEditSecurityQuestions.GetParameters(
                        eventUserName: visitPayUser.UserName),
                    journalEventUserContext,
                    journalEventContext
                );
            }
        }

        public void LogClientRequestPasswordVerificationFailed(string enteredUserName, VisitPayUserDto visitPayUser, JournalEventHttpContextDto context)
        {
            VisitPayUser eventUser = Mapper.Map<VisitPayUser>(visitPayUser);
            JournalEventUserContext journalEventUserContext = this._userJournalEventService.Value.GetJournalEventUserContext(context, eventUser);
            JournalEventContext journalEventContext = this._userJournalEventService.Value.GetJournalEventContextForClient(visitPayUser.VisitPayUserId, null);

            this.Journal.AddEvent<EventClientRequestedPasswordFailure, JournalEventParameters>(
                EventClientRequestedPasswordFailure.GetParameters(
                    invalidUserName: enteredUserName),
                journalEventUserContext,
                journalEventContext
            );

        }


        //TODO: this will need to reflect if Ssn4 or FirstName was used in validation 
        private void LogGuarantorRequestPasswordVerificationFailed(VisitPayUser visitPayUser, JournalEventHttpContextDto context, string enteredUserName, string lastName, DateTime dateOfBirth, string ssn4String)
        {

            int visitPayUserId = visitPayUser?.VisitPayUserId ?? default(int);
            int vpGuarantorId = this._guarantorService.Value.GetGuarantorId(visitPayUser?.VisitPayUserId ?? default(int));
            JournalEventUserContext journalEventUserContext = this._userJournalEventService.Value.GetJournalEventUserContext(context, visitPayUser);
            JournalEventContext journalEventContext = this._userJournalEventService.Value.GetJournalEventContextForGuarantor(visitPayUserId, vpGuarantorId);

            this.Journal.AddEvent<EventGuarantorRequestedPasswordFailure, JournalEventParameters>(
                EventGuarantorRequestedPasswordFailure.GetParameters(
                    dateOfBirth: dateOfBirth.ToShortDateString(),
                    invalidUserName: enteredUserName,
                    lastName: lastName,
                    sSN4: ssn4String
                    ),
                journalEventUserContext,
                journalEventContext
            );
        }

        private void LogEmulateEvent(VisitPayUser visitPayUser, bool success, string emulatedUserName, JournalEventHttpContextDto context)
        {

            JournalEventUserContext journalEventUserContext = this._userJournalEventService.Value.GetJournalEventUserContext(context, visitPayUser);
            JournalEventContext journalEventContext = this._userJournalEventService.Value.GetJournalEventContextForClient(visitPayUser.VisitPayUserId, null);

            if (success)
            {
                this.Journal.AddEvent<EventClientEmulateLoginSuccess, JournalEventParameters>(
                    EventClientEmulateLoginSuccess.GetParameters(
                        eventUserName: visitPayUser.UserName,
                        vpUserName: emulatedUserName
                    ),
                    journalEventUserContext,
                    journalEventContext
                );
            }
            else
            {
                // No JournalEvent defined for EventClientEmulateLoginFailure
                // Leaving stub here in case it needs to be added later
            }

        }

        // TODO: This is getting called from more than just clients, so we may need additional event types or make it type agnostic
        public async Task<bool> LogVerificationResultAsync(string eventUserName, int? visitPayUserId, bool success, JournalEventHttpContextDto context)
        {
            VisitPayUser visitPayUser = visitPayUserId.HasValue ? await this._userService.Value.FindByIdAsync(visitPayUserId.ToString()) : null;

            JournalEventUserContext journalEventUserContext = this._userJournalEventService.Value.GetJournalEventUserContext(context, visitPayUser);
            JournalEventContext journalEventContext = this._userJournalEventService.Value.GetJournalEventContextForClient(visitPayUser?.VisitPayUserId ?? SystemUsers.SystemUserId, null);

            if (success)
            {
                this.Journal.AddEvent<EventClientSuccessfulTempPasswordVerification, JournalEventParameters>(
                    EventClientSuccessfulTempPasswordVerification.GetParameters(
                        eventUserName: visitPayUser?.UserName ?? eventUserName ?? ""
                    ),
                    journalEventUserContext,
                    journalEventContext
                );
            }
            else
            {
                this.Journal.AddEvent<EventClientFailedTempPasswordVerification, JournalEventParameters>(
                    EventClientFailedTempPasswordVerification.GetParameters(
                        eventUserName: visitPayUser?.UserName ?? eventUserName ?? ""
                    ),
                    journalEventUserContext,
                    journalEventContext
                );
            }

            return success;
        }

        private void LogChallengeQuestion(VisitPayUser visitPayUser, bool success, JournalEventHttpContextDto context)
        {
            JournalEventUserContext journalEventUserContext = this._userJournalEventService.Value.GetJournalEventUserContext(context, visitPayUser);
            JournalEventContext journalEventContext = this._userJournalEventService.Value.GetJournalEventContextForClient(visitPayUser.VisitPayUserId, null);

            if (success)
            {
                this.Journal.AddEvent<EventClientSuccessfullyAnsweredSecurityQuestion, JournalEventParameters>(
                    EventClientSuccessfullyAnsweredSecurityQuestion.GetParameters(
                        eventUserName: visitPayUser.UserName
                    ),
                    journalEventUserContext,
                    journalEventContext
                );
            }
            else
            {
                this.Journal.AddEvent<EventClientFailedToAnswerSecurityQuestion, JournalEventParameters>(
                    EventClientFailedToAnswerSecurityQuestion.GetParameters(
                        eventUserName: visitPayUser.UserName
                    ),
                    journalEventUserContext,
                    journalEventContext
                );
            }
        }

        private void LogClientResetClientPassword(VisitPayUser visitPayUser, JournalEventHttpContextDto context, int targetUserId)
        {

            JournalEventUserContext journalEventUserContext = this._userJournalEventService.Value.GetJournalEventUserContext(context, visitPayUser);
            JournalEventContext journalEventContext = this._userJournalEventService.Value.GetJournalEventContextForClient(visitPayUser.VisitPayUserId, null);

            VisitPayUser targetUser = this._userService.Value.FindByIdAsync(targetUserId.ToString()).Result;

            this.Journal.AddEvent<EventClientRequestANewClientUserPassword, JournalEventParameters>(
                EventClientRequestANewClientUserPassword.GetParameters(
                    clientUserName: targetUser.UserName,
                    eventUserName: visitPayUser.UserName
                ),
                journalEventUserContext,
                journalEventContext
            );
        }

        private void LogRequestEmulate(string visitPayUserId, int emulateUserId, JournalEventHttpContextDto context)
        {
            VisitPayUser visitPayUser = this._userService.Value.FindByIdAsync(visitPayUserId).Result;
            VisitPayUser emulateUser = this._userService.Value.FindByIdAsync(emulateUserId.ToString()).Result;
            int vpGuarantorId = this.GetGuarantorIdFromVisitPayUserId(emulateUser.VisitPayUserId);

            JournalEventUserContext journalEventUserContext = this._userJournalEventService.Value.GetJournalEventUserContext(context, visitPayUser);
            JournalEventContext journalEventContext = this._userJournalEventService.Value.GetJournalEventContextForClient(visitPayUser.VisitPayUserId, vpGuarantorId);

            this.Journal.AddEvent<EventClientRequestEmulate, JournalEventParameters>(
                EventClientRequestEmulate.GetParameters(
                    eventUserName: visitPayUser.UserName,
                    vpUserName: emulateUser.UserName
                ),
                journalEventUserContext,
                journalEventContext
            );
        }

        private void LogRequestEmulateFailure(string visitPayUserId, int emulateUserId, JournalEventHttpContextDto context)
        {
            VisitPayUser visitPayUser = this._userService.Value.FindByIdAsync(visitPayUserId).Result;
            VisitPayUser emulateUser = this._userService.Value.FindByIdAsync(emulateUserId.ToString()).Result;
            int vpGuarantorId = this.GetGuarantorIdFromVisitPayUserId(emulateUser.VisitPayUserId);

            JournalEventUserContext journalEventUserContext = this._userJournalEventService.Value.GetJournalEventUserContext(context, visitPayUser);
            JournalEventContext journalEventContext = this._userJournalEventService.Value.GetJournalEventContextForClient(visitPayUser.VisitPayUserId, vpGuarantorId);

            this.Journal.AddEvent<EventClientEmulateLoginFailure, JournalEventParameters>(
                EventClientEmulateLoginFailure.GetParameters(
                    eventUserName: visitPayUser.UserName,
                    vpUserName: emulateUser.UserName
                ),
                journalEventUserContext,
                journalEventContext
            );
        }

        #endregion

        void IJobRunnerService<DeactivateInactiveClientUsersJobRunner>.Execute(DateTime begin, DateTime end, string[] args)
        {
            this.DeactivateInactiveClientUsers();
        }

        #region unreferenced code

        /*
        
        public async Task<DataResult<VisitPayUserDto, FindUserEnum>> FindByDetails(string lastName, DateTime dateOfBirth, int ssn4)
        {
            DataResult<VisitPayUser, FindUserEnum> result = await this._userService.Value.FindByDetailsAsync(lastName, dateOfBirth, ssn4);

            return new DataResult<VisitPayUserDto, FindUserEnum>
            {
                Data = result.Data != null ? Mapper.Map<VisitPayUserDto>(result.Data) : null,
                Result = result.Result
            };
        }

        public Task<bool> HasBeenVerifiedAsync()
        {
            return this._signInService.Value.HasBeenVerifiedAsync();
        }

        public bool InactivateUser(string username)
        {
            VisitPayUser user = this._userService.Value.FindByName(username);
            if (user == null)
            {
                return false;
            }
            user.LockoutEndDateUtc = DateTime.MaxValue;
            user.LockoutReasonEnum = LockoutReasonEnum.AccountDisabled;
            this._userService.Value.Update(user);
            return true;
        }
        
        public IdentityResult CreateWithBasicRole(VisitPayUserDto user, string password, string roleEnum)
        {
            VisitPayUser visitPayUser = Mapper.Map<VisitPayUser>(user);
            visitPayUser.InsertDate = DateTime.UtcNow;

            if (roleEnum == VisitPayRoleStrings.System.Patient)
            {
                throw new Exception("Use CreatePatient for patients.");
            }

            VisitPayRole role = this._visitPayRoleService.Value.GetRoleByName(roleEnum);
            if (role == null)
            {
                return new IdentityResult($"Could not find role \"{roleEnum}\"");
            }

            visitPayUser.Roles.Add(role);
            return this._userService.Value.Create(visitPayUser, password);
        }
        

        public bool IsNonMigratedLegacyUser(string username)
        {
            return this._legacyUserService.Value.IsNonMigratedLegacyUser(username);
        }

        public bool DoesUserHaveSmsAcknowledgement(int visitPayUserId)
        {
            VisitPayUser visitPayUser = this._userService.Value.FindById(visitPayUserId.ToString());
            VisitPayUserAcknowledgement acknowledgement = visitPayUser.Acknowledgements.FirstOrDefault(a => a.VisitPayUserAcknowledgementType == VisitPayUserAcknowledgementTypeEnum.SmsAccept);
            return acknowledgement != null;
        }

        */

        #endregion

        private SignInStatusExEnum IncrementSignInStatusCounter(SignInStatusExEnum signInStatusEx)
        {
            switch (signInStatusEx)
            {
                case SignInStatusExEnum.PasswordExpired:
                    this._metricsProvider.Value.Increment(Metrics.Increment.SignInStatus.PasswordExpired);
                    break;
                case SignInStatusExEnum.RequiresVerification:
                    this._metricsProvider.Value.Increment(Metrics.Increment.SignInStatus.RequiresVerification);
                    break;
                case SignInStatusExEnum.RequiresSecurityQuestions:
                    this._metricsProvider.Value.Increment(Metrics.Increment.SignInStatus.RequiresSecurityQuestions);
                    break;
                case SignInStatusExEnum.Success:
                    this._metricsProvider.Value.Increment(Metrics.Increment.SignInStatus.Success);
                    break;
                case SignInStatusExEnum.AccountClosed:
                    this._metricsProvider.Value.Increment(Metrics.Increment.SignInStatus.AccountClosed);
                    break;
                case SignInStatusExEnum.Failure:
                    this._metricsProvider.Value.Increment(Metrics.Increment.SignInStatus.Failure);
                    break;
                case SignInStatusExEnum.LockedOut:
                    this._metricsProvider.Value.Increment(Metrics.Increment.SignInStatus.LockedOut);
                    break;
                default:
                    throw new ArgumentOutOfRangeException(nameof(signInStatusEx), signInStatusEx, null);
            }
            return signInStatusEx;

        }
    }
}
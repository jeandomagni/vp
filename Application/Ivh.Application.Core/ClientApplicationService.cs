﻿namespace Ivh.Application.Core
{
    using AutoMapper;
    using Common.Dtos;
    using Common.Interfaces;
    using Domain.Settings.Entities;
    using Domain.Settings.Interfaces;

    public class ClientApplicationService : IClientApplicationService
    {
        private readonly IClientService _clientService;

        public ClientApplicationService(IClientService clientService)
        {
            this._clientService = clientService;
        }

        public ClientDto GetClient()
        {
            Client clientDto = this._clientService.GetClient();
            return Mapper.Map<ClientDto>(clientDto);
        }
    }
}
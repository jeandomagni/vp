﻿namespace Ivh.Application.AppIntelligence
{
    using System;
    using Common.Dtos;
    using Common.Interfaces;
    using Domain.AppIntelligence.Entities;
    using Domain.AppIntelligence.Interfaces;
    using Ivh.Common.VisitPay.Enums;

    public class RandomizedTestApplicationService : IRandomizedTestApplicationService
    {
        private readonly Lazy<IRandomizedTestService> _randomizedTestService;

        public RandomizedTestApplicationService(
            Lazy<IRandomizedTestService> randomizedTestService)
        {
            this._randomizedTestService = randomizedTestService;
        }

        public RandomizedTestGroupDto GetRandomizedTestGroupMembership(RandomizedTestEnum randomizedTest, int vpGuarantorId, bool assignIfNotAssigned, RandomizedTestGroupEnum? specificRandomizedTestGroup = null)
        {
            RandomizedTestGroup randomizedTestGroup = this._randomizedTestService.Value.GetRandomizedTestGroupMembership(randomizedTest, vpGuarantorId, assignIfNotAssigned, specificRandomizedTestGroup);

            if (randomizedTestGroup == null)
            {
                return null;
            }

            return new RandomizedTestGroupDto
            {
                RandomizedTestGroupId = randomizedTestGroup.RandomizedTestGroupId,
                RandomizedTest = randomizedTest
            };
        }
    }
}
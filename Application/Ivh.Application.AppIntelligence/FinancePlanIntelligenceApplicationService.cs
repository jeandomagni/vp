﻿namespace Ivh.Application.AppIntelligence
{
    using System;
    using AutoMapper;
    using Base.Common.Interfaces;
    using Base.Services;
    using Common.Interfaces;
    using Domain.AppIntelligence.Entities;
    using Domain.AppIntelligence.Interfaces;
    using Ivh.Common.ServiceBus.Attributes;
    using Ivh.Common.ServiceBus.Enums;
    using Ivh.Common.VisitPay.Messages.AppIntelligence;
    using MassTransit;

    public class FinancePlanIntelligenceApplicationService : ApplicationService, IFinancePlanIntelligenceApplicationService
    {
        private readonly Lazy<IFinancePlanIntelligenceService> _financePlanIntelligenceService;

        public FinancePlanIntelligenceApplicationService(
            Lazy<IApplicationServiceCommonService> applicationServiceCommonService,
            Lazy<IFinancePlanIntelligenceService> financePlanIntelligenceService) : base(applicationServiceCommonService)
        {
            this._financePlanIntelligenceService = financePlanIntelligenceService;
        }

        private void LogTermsDisplayed(FinancePlanTermsDisplayedMessage financePlanTermsDisplayedDto)
        {
            FinancePlanTermsDisplayed terms = Mapper.Map<FinancePlanTermsDisplayed>(financePlanTermsDisplayedDto);

            this._financePlanIntelligenceService.Value.LogTermsDisplayed(terms);
        }
        
        #region consumers

        [UniversalConsumer(UniversalQueuesEnum.VisitPayProcessor, UniversalPriorityEnum.Priority20)]
        public void ConsumeMessage(FinancePlanTermsDisplayedMessage message, ConsumeContext<FinancePlanTermsDisplayedMessage> consumeContext)
        {
            this.LogTermsDisplayed(message);
        }

        public bool IsMessageValid(FinancePlanTermsDisplayedMessage message, ConsumeContext<FinancePlanTermsDisplayedMessage> consumeContext)
        {
            bool isValid = true;
            return isValid;
        }

        #endregion

    }
}
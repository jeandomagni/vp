﻿namespace Ivh.Application.AppIntelligence.Modules
{
    using Autofac;
    using Common.Interfaces;

    public class Module : Autofac.Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<FinancePlanIntelligenceApplicationService>().As<IFinancePlanIntelligenceApplicationService>();
            builder.RegisterType<RandomizedTestApplicationService>().As<IRandomizedTestApplicationService>();
        }
    }
}
﻿namespace Ivh.Application.AppIntelligence.Mappings
{
    using AutoMapper;
    using Domain.AppIntelligence.Entities;
    using Ivh.Common.VisitPay.Messages.AppIntelligence;

    public class AutomapperProfile : Profile
    {
        public AutomapperProfile()
        {
            this.CreateMap<FinancePlanTermsDisplayedMessage, FinancePlanTermsDisplayed>();
        }
    }
}
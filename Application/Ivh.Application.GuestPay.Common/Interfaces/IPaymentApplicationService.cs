﻿namespace Ivh.Application.GuestPay.Common.Interfaces
{
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Dtos;
    using Ivh.Common.Base.Interfaces;
    using Ivh.Common.EventJournal;
    using Ivh.Common.ServiceBus.Interfaces;
    using Ivh.Common.VisitPay.Messages.FinanceManagement;

    public interface IPaymentApplicationService : IApplicationService
    {
        PaymentUnitAuthenticationDto AuthenticatePaymentUnit(PaymentUnitMatchDto paymentUnitMatchDto, JournalEventHttpContextDto context);
        IList<PaymentUnitAuthenticationDto> GetPaymentUnitAuthentications(string paymentUnitSourceSystemKey);
        PaymentMatchUnitResponseDto GetPaymentMatchUnitResponse(string paymentUnitSourceSystemKey);
        PaymentDto GetPayment(int paymentId, string enteredAuthenticationKey);
        Task<ProcessPaymentResponseDto> ProcessPaymentAsync(PaymentSubmitDto paymentDto, int paymentUnitAuthenticationId, string enteredAuthenticationKey, JournalEventHttpContextDto context);
        Task<ProcessPaymentResponseDto> ProcessRemotePayment(RemotePaymentInfoDto remotePaymentInfo, JournalEventHttpContextDto context);
    }
}
﻿namespace Ivh.Application.GuestPay.Common.Interfaces
{
    using System.Collections.Generic;
    using Dtos;
    using Ivh.Common.Base.Interfaces;

    public interface IGuestPayGuarantorAuthenticationApplicationService : IApplicationService
    {
        IList<AuthenticationMatchFieldDto> GetGuestPayGuarantorAuthenticationFields();
    }
}

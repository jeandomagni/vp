﻿namespace Ivh.Application.GuestPay.Common.Interfaces
{
    using System.Collections.Generic;
    using Ivh.Common.Base.Interfaces;
    using Ivh.Common.EventJournal;

    public interface IGuestPayJournalEventApplicationService : IApplicationService
    {
        void LogAuthenticationFailure(JournalEventHttpContextDto context, string attemptedGuarantorSourceSystemKey, string attemptedGuarantorLastName);
        void LogAuthenticationSuccess(JournalEventHttpContextDto context, int paymentUnitAuthenticationId, string paymentUniSourceSystemKey, string guarantorLastName);
        void LogAvsFailure(JournalEventHttpContextDto context, int paymentUnitAuthenticationId, string paymentUnitSourceSystemKey, string guarantorLastName, IReadOnlyDictionary<string, string> processorResponse);
        void LogReceiptDownload(JournalEventHttpContextDto context, int paymentUnitAuthenticationId, string paymentUnitSourceSystemKey, string guarantorLastName, string receiptId);
        void LogPaymentMethodFailure(JournalEventHttpContextDto context, int paymentUnitAuthenticationId, string paymentUnitSourceSystemKey, string guarantorLastName);
        void LogSessionTimeout(JournalEventHttpContextDto context, int paymentUnitAuthenticationId, string paymentUnitSourceSystemKey);
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ivh.Application.GuestPay.Common.Dtos
{
    using Ivh.Common.Base.Enums;
    using Ivh.Common.VisitPay.Enums;

    public class RemotePaymentInfoDto
    {
        public string PaymentUnitSourceSystemKey { get; set; }
        public int PaymentUnitAuthenticationId { get; set; }
        public decimal PaymentAmount { get; set; }
        public PaymentMethodTypeEnum PaymentMethodType { get; set; }
        public string BillingId { get; set; }
        public string LastFour { get; set; }
        public string ExpDate { get; set; }
        public string NameOnCard { get; set; }
        public string OriginalRequestIpAddress { get; set; }
    }
}

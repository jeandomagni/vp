﻿namespace Ivh.Application.GuestPay.Common.Dtos
{
    using System;
    using Ivh.Common.Base.Enums;
    using Ivh.Common.VisitPay.Enums;

    public class PaymentDto
    {
        public int PaymentId { get; set; }

        public DateTime InsertDate { get; set; }

        public PaymentStatusEnum PaymentStatus { get; set; }

        public PaymentMethodTypeEnum PaymentMethodType { get; set; }

        public decimal PaymentAmount { get; set; }

        public DateTime PaymentDate { get; set; }

        public int PaymentAttemptCount { get; set; }

        public string EmailAddress { get; set; }

        public string GuarantorNumber { get; set; }

        public string GuarantorLastName { get; set; }

        public string SourceSystemKey { get; set; }

        public string BankName { get; set; }

        public string PaymentAccountHolderName { get; protected internal set; }

        public string ExpDate { get; protected internal set; }

        public string LastFour { get; protected internal set; }

        public string EnteredAuthenticationKey { get; set; }

        public string IpAddress { get; set; }

        public string TransactionId { get; set; }

        public string AuthCode { get; set; }

        public PaymentUnitAuthenticationDto PaymentUnitAuthentication { get; set; }
        public int? CreatedVisitPayUserId { get; set; }
    }
}
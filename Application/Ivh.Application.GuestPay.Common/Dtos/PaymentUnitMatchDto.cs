﻿using Ivh.Common.Base.Utilities.Helpers;

namespace Ivh.Application.GuestPay.Common.Dtos
{
    public class PaymentUnitMatchDto
    {
        public string SourceSystemKey { get; set; }
        public string GuarantorFirstName { get; set; }
        public string GuarantorLastName { get; set; }
        public string StatementIdentifierId { get; set; }
        public string GuarantorLastNameFirstName => FormatHelper.LastNameFirstName(this.GuarantorLastName, this.GuarantorFirstName);
        public string GuarantorFirstNameLastName => FormatHelper.FirstNameLastName(this.GuarantorFirstName, this.GuarantorLastName);
    }
}
﻿namespace Ivh.Application.GuestPay.Common.Dtos
{
    using Ivh.Common.Base.Enums;
    using Ivh.Common.VisitPay.Enums;

    public class PaymentMatchUnitResponseDto
    {
        public int PaymentUnitAuthenticationId { get; set; }
        public bool IsValid { get; set; }
        public PaymentMatchUnitResponseStatusEnum StatusCode { get; set; }
        public decimal Balance { get; set; }
    }
}

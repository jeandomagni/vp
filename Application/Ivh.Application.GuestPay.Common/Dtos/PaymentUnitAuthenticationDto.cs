﻿namespace Ivh.Application.GuestPay.Common.Dtos
{
    using Ivh.Common.Base.Utilities.Helpers;
    using System;

    public class PaymentUnitAuthenticationDto
    {
        public int PaymentUnitAuthenticationId { get; set; }
        public DateTime InsertDate { get; set; }
        public decimal Balance { get; set; }
        public string GuarantorFirstName { get; set; }
        public string GuarantorLastName { get; set; }
        public string PaymentUnitSourceSystemKey { get; set; }
        public int StatementIdentifierId  { get; set; }
        public string GuarantorLastNameFirstName => FormatHelper.LastNameFirstName(this.GuarantorLastName,this.GuarantorFirstName);
        public string GuarantorFirstNameLastName => FormatHelper.FirstNameLastName(this.GuarantorFirstName,this.GuarantorLastName);
    }
}
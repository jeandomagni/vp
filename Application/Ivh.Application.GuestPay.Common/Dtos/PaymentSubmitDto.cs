﻿namespace Ivh.Application.GuestPay.Common.Dtos
{
    using Ivh.Common.Base.Enums;
    using Ivh.Common.Base.Utilities.Helpers;
    using Ivh.Common.VisitPay.Enums;

    public class PaymentSubmitDto
    {
        public PaymentMethodTypeEnum PaymentMethodType { get; set; }
        public PaymentSystemTypeEnum PaymentSystemType { get; set; }
        public decimal PaymentAmount { get; set; }
        public string EmailAddress { get; set; }
        public string IpAddress { get; set; }

        // CC Fields
        public string CardAccountHolderName { get; set; }
        public string BillingId { get; set; }
        public string GatewayToken { get; set; }
        public string LastFour { get; set; }
        public string ExpDate { get; set; }
        public string SecurityCode { get; set; }
        public string AddressStreet1 { get; set; }
        public string AddressStreet2 { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Zip { get; set; }
        public bool IsCountryUsa { get; set; }
        public int? ExpDateM { get; set; }
        public int? ExpDateY { get; set; }

        // ACH Fields
        public string BankName { get; set; }
        public string BankAccountNumber { get; set; }
        public string BankRoutingNumber { get; set; }
        public string BankAccountHolderFirstName { get; set; }
        public string BankAccountHolderLastName { get; set; }
        public string BankAccountHolderLastNameFirstName => FormatHelper.LastNameFirstName(this.BankAccountHolderLastName, this.BankAccountHolderFirstName);
        public string BankAccountHolderFirstNameLastName => FormatHelper.FirstNameLastName(this.BankAccountHolderFirstName,this.BankAccountHolderLastName);
    }
}
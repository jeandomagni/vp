﻿namespace Ivh.Application.GuestPay.Common.Dtos
{
    using System.Collections.Generic;
    using Ivh.Common.Base.Enums;
    using Ivh.Common.VisitPay.Enums;

    public class ProcessPaymentResponseDto
    {
        public ProcessPaymentResponseDto()
        {
            this.ErrorMessages = new List<string>();
        }

        public ProcessPaymentResponseDto(bool isSuccess)
        {
            this.ErrorMessages = new List<string>();
            this.IsSuccess = isSuccess;
        }

        public ProcessPaymentResponseDto(bool isSuccess, string message, ProcessPaymentResponseStatusEnum status)
        {
            this.ErrorMessages = new List<string>
            {
                message
            };

            this.IsSuccess = isSuccess;
            this.StatusCode = status;
        }

        public bool IsError
        {
            get { return !this.IsSuccess; }
        }

        public bool IsSuccess { get; set; }

        public ProcessPaymentResponseStatusEnum StatusCode { get; set; }

        public IList<string> ErrorMessages { get; set; }

        public PaymentDto Payment { get; set; }
    }
}
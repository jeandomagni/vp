﻿namespace Ivh.Application.GuestPay.Common.Dtos
{
    using System.Collections.Generic;
    using Ivh.Common.Base.Enums;
    using Ivh.Common.VisitPay.Enums;

    public class AuthenticationMatchFieldDto
    {
        public string Key { get; set; }

        public string Label { get; set; }

        public AuthenticationMatchFieldTypeEnum Type { get; set; }

        public IList<AuthenticationMatchFieldDto> Options { get; set; } = new List<AuthenticationMatchFieldDto>();

        public CmsRegionEnum? LabelCmsRegion
        {
            get
            {
                string keyUpper = this.Key.ToUpper();
                if (this.LabelCmsRegions.ContainsKey(keyUpper))
                {
                    return this.LabelCmsRegions[keyUpper];
                }

                return null;
            }
        }

        private IDictionary<string, CmsRegionEnum> LabelCmsRegions => new Dictionary<string, CmsRegionEnum>
        {
            {"StatementIdentifierId".ToUpper(), CmsRegionEnum.GuestPayStatementIdentifierIdText},
            {"PaymentUnitSourceSystemKey".ToUpper(), CmsRegionEnum.GuestPaySourceSystemKeyText},
            {"GuarantorLastName".ToUpper(), CmsRegionEnum.GuestPayGuarantorLastNameText},
            {"StatementReferenceImageId".ToUpper(), CmsRegionEnum.GuestPayStatementReferenceImageText},
        };
    }
}
﻿namespace Ivh.Application.SettingsManager.Common.Models
{
    public class SettingMinimum
    {
        public string Name { get; set; }
        public string Value { get; set; }
    }
}

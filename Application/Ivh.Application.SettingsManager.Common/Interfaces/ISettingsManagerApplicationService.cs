﻿using System.Collections.Generic;
using Ivh.Application.SettingsManager.Common.Models;
using Ivh.Domain.SettingsManager.Common.Models;

namespace Ivh.Application.SettingsManager.Common.Interfaces
{
    public interface ISettingsManagerApplicationService
    {
        List<SettingMinimum> GetSettingsMin(int clientId, int environmentId, int releaseId);
        List<SettingComposite> GetSettingsMax(int clientId, int environmentId, int releaseId);
        List<string> GetSettingsDiffLine(int clientId, int environmentId, int releaseId);
        List<string> GetSettingsDiffLineFromServer(int clientId, int environmentId);
        string GetClientSettingsMergeScriptForDeveloper(int clientId, int environmentId, int releaseId);
        string GetClientSettingsMergeScript(string client, string environment, string release);
        BootGridResponse<SettingComposite> GetSettingsMaxBootgrid(BootGridRequest model);
        void AddSetting(SettingInput model);
        void AddSettingOverride(SettingOverrideInput model);
        IEnumerable<KeyValuePair<int, string>> GetReleases(bool includeServerOption = false);
        IEnumerable<KeyValuePair<int, string>> GetClients(bool includeAllOption = false);
        IEnumerable<KeyValuePair<int, string>> GetEnvironments(bool includeAllOption = false);
        IEnumerable<KeyValuePair<int, string>> GetSettingTypes();
        IEnumerable<KeyValuePair<int, string>> GetSettingNames();
        IEnumerable<KeyValuePair<string, string>> GetSettingDataTypes();
        string GetSettingsHashCode(int clientId, int environmentId, int releaseId);
        string GetLatestApprovedSettingsHashCode(int clientId, int environmentId, int releaseId);
        void AddClientReleaseEnvironmentApproval(DeploymentContextApproval deploymentContextApproval);
        string GetLatestReleaseLabel();
        DeploymentContextApprovalStatus GetDeploymentContextApprovalStatus(string client, string environment, string release);
    }
}

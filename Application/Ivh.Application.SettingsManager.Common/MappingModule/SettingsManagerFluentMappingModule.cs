﻿namespace Ivh.Application.SettingsManager.Common.MappingModule
{
    using System.Reflection;
    using Domain.SettingsManager.Entities;
    using FluentNHibernate.Cfg;
    using FluentNHibernate.Conventions.Helpers;
    using Ivh.Common.Data.Core.Interfaces;

    public class SettingsManagerFluentMappingModule : IFluentMappingModule
    {
        public void MapAssemblies(MappingConfiguration m)
        {
            m.FluentMappings.Conventions.Setup(a => a.Add(AutoImport.Never()));// this is to prevent name collisions for entities with the same unqualified names in different namespaces.
            m.FluentMappings.AddFromAssembly(Assembly.GetAssembly(typeof(Setting)));
        }
    }
}

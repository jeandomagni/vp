﻿namespace Ivh.Application.FileStorage.Common.Dtos
{
    using System;
    using Ivh.Common.Base.Enums;

    public class FileStoredDto
    {
        public virtual int Id { get; set; }
        public virtual Guid ExternalKey { get; set; }
        public virtual string Filename { get; set; }
        public virtual string FileMimeType { get; set; }
        public virtual DateTime FileDateTime { get; set; }
        public virtual DateTime InsertDate { get; set; }
        public virtual byte[] FileContents { get; set; }
    }
}
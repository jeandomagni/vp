﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ivh.Application.FileStorage.Common.Dtos
{
    public class FileStorageGetResponseDto
    {
        public bool IsError { get; set; }
        public string ErrorMessage { get; set; }
        public FileStoredDto FileStorage { get; set; }
    }
}
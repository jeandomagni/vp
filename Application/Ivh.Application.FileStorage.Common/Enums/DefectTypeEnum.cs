﻿namespace Ivh.Application.FileStorage.Common.Enums
{
    using System.ComponentModel.DataAnnotations;

    public enum DefectTypeEnum
    {
        [Display(Name = "Normal (no defect)")]
        None = 0,
        [Display(Name = "Decryption Error")]
        Decryption,
        [Display(Name = "Missing Manifest")]
        MissingManifest,
        [Display(Name = "File Count Too Low")]
        FileCountLow,
        [Display(Name = "File Count Too High")]
        FileCountHigh,
        [Display(Name = "Invalid File Hash")]
        InvalidFileHash,
        [Display(Name = "Invalid File Size")]
        InvalidFileSize, 
        [Display(Name = "Invalid File Name")]
        InvalidFileName,
        [Display(Name = "Invalid File Date/Time")]
        InvalidFileDateTime,
        [Display(Name = "Missing File")]
        MissingFile,
        [Display(Name = "Extraneous File")]
        ExtraneousFile

    }
}

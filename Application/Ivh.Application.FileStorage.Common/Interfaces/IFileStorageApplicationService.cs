﻿namespace Ivh.Application.FileStorage.Common.Interfaces
{
    using System.Threading.Tasks;
    using Dtos;
    using System;
    using Ivh.Application.FileStorage.Common.Enums;
    using Ivh.Common.Base.Interfaces;
    using Ivh.Common.JobRunner.Common.Interfaces;
    using Ivh.Common.VisitPay.Jobs;

    public interface IFileStorageApplicationService : IApplicationService,
        IJobRunnerService<FileStorageImportJobRunner>,
        IJobRunnerService<FileStorageVisitMatchJobRunner>
    {
        Task <FileStorageResponseDto> ImportDirectoryAsync();
        Task MatchFilesWithVisitsAsync();
        Task<FileStorageSaveResponseDto> SaveFileToIncomingFolderAsync(FileStoredDto fileStorageDto);
        Task<FileStorageSaveResponseDto> SaveFileToDatabaseAsync(FileStoredDto fileStorageDto);
        Task<FileStorageGetResponseDto> GetFileAsync(Guid id);
        void CreatePackageFromFolder(DefectTypeEnum defectTypeEnum);

    }
}
using System.Reflection;
using System.Runtime.InteropServices;
using Ivh.Common.Assembly.Attributes;
using Ivh.Common.Assembly.Constants;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("Ivh.Application.FileStorage.Common")]
[assembly: AssemblyDescription("")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany(AssemblyCompany.Ivh)]
[assembly: AssemblyProduct("Ivh.Application.FileStorage.Common")]
[assembly: AssemblyCopyright(AssemblyCopyright.Ivh)]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]
[assembly: AssemblyDomain(AssemblyDomain.VisitPay)]
[assembly: AssemblyApplicationLayer(AssemblyApplicationLayer.Application)]


// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("dcd041ae-7c91-4ac5-81a0-d49dd030da4f")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Build and Revision Numbers 
// by using the '*' as shown below:
// [assembly: AssemblyVersion("1.0.*")]
[assembly: AssemblyVersion("1.0.0.0")]
[assembly: AssemblyFileVersion("1.0.0.0")]

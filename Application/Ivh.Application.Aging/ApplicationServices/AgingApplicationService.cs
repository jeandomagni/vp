﻿namespace Ivh.Application.Aging.ApplicationServices
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Base.Common.Interfaces;
    using Base.Services;
    using Common.Interfaces;
    using Domain.Rager.Entities;
    using Domain.Rager.Interfaces;
    using Ivh.Common.Base.Utilities.Extensions;
    using Ivh.Common.Data;
    using Ivh.Common.Data.Connection.Connections;
    using Ivh.Common.Data.Interfaces;
    using Ivh.Common.JobRunner.Common.Interfaces;
    using Ivh.Common.ServiceBus.Attributes;
    using Ivh.Common.ServiceBus.Enums;
    using Ivh.Common.ServiceBus.Common.Messages;
    using Ivh.Common.VisitPay.Constants;
    using Ivh.Common.VisitPay.Enums;
    using Ivh.Common.VisitPay.Jobs;
    using Ivh.Common.VisitPay.Messages.Aging;
    using Ivh.Common.VisitPay.Messages.Aging.Dtos;
    using Ivh.Common.VisitPay.Messages.Core;
    using Ivh.Common.VisitPay.Messages.HospitalData;
    using Ivh.Common.VisitPay.Messages.HospitalData.Dtos;
    using MassTransit;
    using NHibernate;

    public class AgingApplicationService : ApplicationService, IAgingApplicationService
    {
        private readonly ISession _session;
        private readonly Lazy<IVisitAgeService> _visitAgeService;
        private readonly Lazy<IAgingGuarantorService> _agingGuarantorService;

        public AgingApplicationService(
            Lazy<IApplicationServiceCommonService> applicationServiceCommonService,
            Lazy<IVisitAgeService> visitAgeService,
            Lazy<IAgingGuarantorService> agingGuarantorService,
            ISessionContext<CdiEtl> sessionContext
        ) : base(applicationServiceCommonService)
        {
            this._visitAgeService = visitAgeService;
            this._agingGuarantorService = agingGuarantorService;
            this._session = sessionContext.Session;
        }

        #region Message Consumers

        [UniversalConsumer(UniversalQueuesEnum.AgingProcessor, UniversalPriorityEnum.Priority20)]
        public void ConsumeMessage(GuarantorChangedMessage message, ConsumeContext<GuarantorChangedMessage> context)
        {
            this.ProcessGuarantorChangedMessage(message);
        }

        public bool IsMessageValid(GuarantorChangedMessage message, ConsumeContext<GuarantorChangedMessage> consumeContext)
        {
            bool isValid = true;
            return isValid;
        }

        [UniversalConsumer(UniversalQueuesEnum.AgingProcessor, UniversalPriorityEnum.Priority20)]
        public void ConsumeMessage(HsGuarantorVisitUnmatchMessage message, ConsumeContext<HsGuarantorVisitUnmatchMessage> context)
        {
            this.ProcessResetVisitAgeMessage(message);
        }

        public bool IsMessageValid(HsGuarantorVisitUnmatchMessage message, ConsumeContext<HsGuarantorVisitUnmatchMessage> consumeContext)
        {
            bool isValid = true;
            return isValid;
        }

        [UniversalConsumer(UniversalQueuesEnum.AgingProcessor, UniversalPriorityEnum.Priority20)]
        public void ConsumeMessage(VisitCommunicationMessage message, ConsumeContext<VisitCommunicationMessage> context)
        {
            using (UnitOfWork unitOfWork = new UnitOfWork(this._session))
            {
                foreach (VisitDto visitDto in message.Visits)
                {
                    Visit visit = this._visitAgeService.Value.GetOrCreateVisitBySourceKeyAndBillingId(visitDto.VisitSourceSystemKey, visitDto.VisitBillingSystemId);

                    visit.AddVisitCommunications(message.CommunicationDateTime, new CommunicationType { CommunicationTypeId = (int)message.CommunicationType });

                    int snapShotAgeSum = visit.VisitAges.Sum(x => x.Age);
                    AgingTier currentAgingTier = this.ProcessAgeTierCalculation(visit, snapShotAgeSum);

                    if (visit.CurrentVisitAgingTier != currentAgingTier)
                    {
                        VisitAge visitAge = new VisitAge
                        {
                            Visit = visit,
                            Age = 0,
                            SnapshotAgeSum = snapShotAgeSum,
                            SnapshotCommunications = visit.VisitCommunicationsJsonData,
                            AgingTier = currentAgingTier
                        };

                        visit.VisitAges.Add(visitAge);

                        //notify tier change
                        this.SendTierChangeNotification(visit, currentAgingTier);

                    }

                    this._visitAgeService.Value.SaveVisit(visit);

                }

                unitOfWork.Commit();
            }
        }

        public bool IsMessageValid(VisitCommunicationMessage message, ConsumeContext<VisitCommunicationMessage> consumeContext)
        {
            bool isValid = true;
            return isValid;
        }

        [UniversalConsumer(UniversalQueuesEnum.AgingProcessor, UniversalPriorityEnum.Priority20)]
        public void ConsumeMessage(ChangeSetAggregatedMessage message, ConsumeContext<ChangeSetAggregatedMessage> context)
        {
            this.ProcessChangeSetAggregatedMessage(message);
        }

        public bool IsMessageValid(ChangeSetAggregatedMessage message, ConsumeContext<ChangeSetAggregatedMessage> consumeContext)
        {
            bool isValid = true;
            return isValid;
        }

        [UniversalConsumer(UniversalQueuesEnum.AgingProcessor, UniversalPriorityEnum.Priority20)]
        public void ConsumeMessage(OutboundVisitMessageList message, ConsumeContext<OutboundVisitMessageList> context)
        {
            this.ProcessOutboundVisitMessageList(message);
        }

        public bool IsMessageValid(OutboundVisitMessageList message, ConsumeContext<OutboundVisitMessageList> consumeContext)
        {
            bool isValid = true;
            return isValid;
        }

        [UniversalConsumer(UniversalQueuesEnum.AgingProcessor, UniversalPriorityEnum.Priority20)]
        public void ConsumeMessage(OutboundVisitMessage message, ConsumeContext<OutboundVisitMessage> consumeContext)
        {
            OutboundVisitMessageList outboundVisitMessage = new OutboundVisitMessageList { Messages = message.ToListOfOne() };
            this.ProcessOutboundVisitMessageList(outboundVisitMessage);
        }

        public bool IsMessageValid(OutboundVisitMessage message, ConsumeContext<OutboundVisitMessage> consumeContext)
        {
            bool isValid = true;
            return isValid;
        }

        [UniversalConsumer(UniversalQueuesEnum.AgingProcessor, UniversalPriorityEnum.Priority20)]
        public void ConsumeMessage(AgeVisitMessage message, ConsumeContext<AgeVisitMessage> context)
        {
            this.ProcessAgeVisitMessage(message);
        }

        public bool IsMessageValid(AgeVisitMessage message, ConsumeContext<AgeVisitMessage> consumeContext)
        {
            bool isValid = true;
            return isValid;
        }

        [UniversalConsumer(UniversalQueuesEnum.AgingProcessor, UniversalPriorityEnum.Priority10)]
        public void ConsumeMessage(SetVisitAgeMessage message, ConsumeContext<SetVisitAgeMessage> context)
        {
            this.ProcessSetVisitAgeMessage(message);
        }

        public bool IsMessageValid(SetVisitAgeMessage message, ConsumeContext<SetVisitAgeMessage> consumeContext)
        {
            bool isValid = true;
            return isValid;
        }

        [UniversalConsumer(UniversalQueuesEnum.AgingProcessor, UniversalPriorityEnum.Priority20)]
        public void ConsumeMessage(VisitReconciliationBalanceChangeMessageList message, ConsumeContext<VisitReconciliationBalanceChangeMessageList> context)
        {
            this.ProcessVisitChangedStatusMessage(message);
        }

        public bool IsMessageValid(VisitReconciliationBalanceChangeMessageList message, ConsumeContext<VisitReconciliationBalanceChangeMessageList> consumeContext)
        {
            bool isValid = true;
            return isValid;
        }

        [UniversalConsumer(UniversalQueuesEnum.AgingProcessor, UniversalPriorityEnum.Priority20)]
        public void ConsumeMessage(VisitReconciliationBalanceChangeMessage message, ConsumeContext<VisitReconciliationBalanceChangeMessage> context)
        {
            this.ProcessVisitChangedStatusMessage(new VisitReconciliationBalanceChangeMessageList { Messages = message.ToListOfOne() });
        }

        public bool IsMessageValid(VisitReconciliationBalanceChangeMessage message, ConsumeContext<VisitReconciliationBalanceChangeMessage> consumeContext)
        {
            bool isValid = true;
            return isValid;
        }

        #endregion

        private void ProcessGuarantorChangedMessage(GuarantorChangedMessage message)
        {
            int vpGuarantorId = message.VpGuarantorId;

            using (UnitOfWork unitOfWork = new UnitOfWork(this._session))
            {
                this._agingGuarantorService.Value.SetAgingGuarantorType(message.GuarantorTypeEnum, vpGuarantorId);
                this._agingGuarantorService.Value.SetAgingGuarantorState(message.VpGuarantorStatus, vpGuarantorId);

                unitOfWork.Commit();
            }
        }

        public void ProcessResetVisitAgeMessage(HsGuarantorVisitUnmatchMessage message)
        {
            Visit visit = this._visitAgeService.Value.GetOrCreateVisitBySourceKeyAndBillingId(message.SourceSystemKey, message.BillingSystemId);
            this.ResetVisitAge(visit, message.ActionVisitPayUserId);
        }

        private void ProcessVisitChangedStatusMessage(VisitReconciliationBalanceChangeMessageList message)
        {
            using (UnitOfWork unitOfWork = new UnitOfWork(this._session))
            {
                foreach (VisitReconciliationBalanceChangeMessage visitChangeMessage in message.Messages)
                {
                    Visit visit = this._visitAgeService.Value.GetOrCreateVisitBySourceKeyAndBillingId(visitChangeMessage.VpVisitSourceSystemKey, visitChangeMessage.VpVisitBillingSystemId);
                    bool visitIsNowInactive = visitChangeMessage.VisitState == VisitStateEnum.Inactive;

                    if (visit.IsInactive != visitIsNowInactive)
                    {
                        visit.AddVisitState(visitIsNowInactive);
                        this._visitAgeService.Value.SaveVisit(visit);
                    }
                }

                unitOfWork.Commit();
            }
        }

        public void ProcessSetVisitAgeMessage(SetVisitAgeMessage message)
        {
            Visit visit = this._visitAgeService.Value.GetOrCreateVisitBySourceKeyAndBillingId(message.Visit.VisitSourceSystemKey, message.Visit.VisitBillingSystemId);
            if (message.SetToActive)
            {
                // Recall and State are considered the same from a code perspective (both driven by VpEligible of the visit)
                visit.AddVisitRecall(false);
                visit.AddVisitState(false);
                this._visitAgeService.Value.SaveVisit(visit);
            }
            this.SetVisitAgingTier(visit, message.AgingTierEnum, message.ActionVisitPayUserId);
        }

        public void ProcessAgeVisitMessage(AgeVisitMessage message)
        {
            Visit visit = this._visitAgeService.Value.GetVisitById(message.VisitId);
            this.AgeVisit(visit, message.DateToProcess);
        }

        public void ProcessChangeSetAggregatedMessage(ChangeSetAggregatedMessage message)
        {
            List<ChangeSetMessage> changeSetMessages = message.Messages.ToList();

            foreach (ChangeSetMessage changeSetMessage in changeSetMessages)
            {
                this.ProcessChangeSetMessage(changeSetMessage);
            }
        }

        private void ProcessChangeSetMessage(ChangeSetMessage changeSetMessage)
        {
            using (UnitOfWork unitOfWork = new UnitOfWork(this._session))
            {
                if (changeSetMessage.VisitChanges.Any())
                {
                    foreach (VisitChangeDto visitChange in changeSetMessage.VisitChanges)
                    {
                        int billingSystemId = visitChange.BillingSystemId;
                        string sourceSystemKey = visitChange.SourceSystemKey;

                        // get the vpGuarantorId matched to this visit's hsGuarantorId
                        int vpGuarantorId = changeSetMessage.VpGuarantorHsMatches
                                                ?.FirstOrDefault(x => x.HsGuarantorId == visitChange.HsGuarantorId && x.HsGuarantorMatchStatus.IsInCategory(HsGuarantorMatchStatusEnumCategory.Matched))
                                                ?.VpGuarantorId ?? default(int);

                        //get the visit
                        Visit visit = this._visitAgeService.Value.GetVisitBySourceKeyAndBillingId(sourceSystemKey, billingSystemId);
                        if (visit == null)
                        {
                            //TODO: consider CurrentVisitAgingTier?
                            visit = new Visit { VisitSourceSystemKey = sourceSystemKey, VisitBillingSystemId = billingSystemId };
                        }

                        //handle suspensions 
                        if (visitChange.BillingHold != visit.IsSuspended)
                        {
                            VisitSuspension visitSuspension = new VisitSuspension
                            {
                                Visit = visit,
                                IsSuspended = visitChange.BillingHold
                            };
                            visit.VisitSuspensions.Add(visitSuspension);
                        }

                        //handle recalls
                        bool visitChangeIsRecalled = !visitChange.VpEligible;

                        if (visitChangeIsRecalled != visit.IsRecalled)
                        {
                            visit.AddVisitRecall(visitChangeIsRecalled);
                            visit.AddVisitState(visitChangeIsRecalled);
                        }

                        // set vpGuarantor
                        visit.VpGuarantorId = vpGuarantorId;

                        this._visitAgeService.Value.SaveVisit(visit);
                    }
                }

                unitOfWork.Commit();
            }
        }

        public void ProcessOutboundVisitMessageList(OutboundVisitMessageList message)
        {
            using (UnitOfWork unitOfWork = new UnitOfWork(this._session))
            {
                foreach (OutboundVisitMessage outboundVisitMessage in message.Messages)
                {
                    if (_outboundMessageTypeDictionary.ContainsKey(outboundVisitMessage.VpOutboundVisitMessageType))
                    {
                        Visit visit = this._visitAgeService.Value.GetVisitBySourceKeyAndBillingId(outboundVisitMessage.SourceSystemKey, outboundVisitMessage.BillingSystemId);
                        if (visit == null)
                        {
                            //TODO: log? 
                            return;
                        }

                        bool isOnFinancePlan = _outboundMessageTypeDictionary[outboundVisitMessage.VpOutboundVisitMessageType];
                        if (isOnFinancePlan != visit.IsOnFinancePlan)
                        {
                            VisitFinancePlan visitFinancePlan = new VisitFinancePlan
                            {
                                Visit = visit,
                                IsOnFinancePlan = isOnFinancePlan
                            };
                            visit.VisitFinancePlans.Add(visitFinancePlan);
                        }
                        this._visitAgeService.Value.SaveVisit(visit);
                    }
                }
                unitOfWork.Commit();
            }
        }

        private static readonly Dictionary<VpOutboundVisitMessageTypeEnum, bool> _outboundMessageTypeDictionary = new Dictionary<VpOutboundVisitMessageTypeEnum, bool>
        {
            [VpOutboundVisitMessageTypeEnum.VisitAddedToFinancePlan] = true,
            [VpOutboundVisitMessageTypeEnum.VisitRemovedFromFinancePlan] = false
        };

        public void QueueAgeVisits(DateTime dateToProcess)
        {
            IList<int> visitIds = this._visitAgeService.Value.GetVisitsIds(dateToProcess);
            foreach (int visitId in visitIds)
            {
                AgeVisitMessage ageVisitMessage = new AgeVisitMessage { VisitId = visitId, DateToProcess = dateToProcess };
                this.Bus.Value.PublishMessage(ageVisitMessage);
            }
        }

        private void AgeVisit(Visit visit, DateTime dateToProcess, int daysToAge = 1, int? actionVisitPayUserId = null)
        {
            bool clientOverride = actionVisitPayUserId.HasValue;
            if (!visit.IsEligibleToAge || !clientOverride && visit.ProcessedDate?.Date == dateToProcess.Date)
            {
                return;
            }

            using (UnitOfWork uow = new UnitOfWork(this._session))
            {
                // Increment the age
                int snapShotAgeSum = visit.VisitAges.Sum(x => x.Age) + daysToAge;

                AgingTier currentAgingTier = this.ProcessAgeTierCalculation(visit, snapShotAgeSum);
                AgingTier visitInitialAgingTier = visit.CurrentVisitAgingTier;

                // Add the visit age
                VisitAge visitAge = new VisitAge
                {
                    Visit = visit,
                    Age = daysToAge,
                    SnapshotAgeSum = snapShotAgeSum,
                    SnapshotCommunications = visit.VisitCommunicationsJsonData,
                    AgingTier = currentAgingTier,
                    ActionVisitPayUserId = actionVisitPayUserId
                };

                //notify tier change
                if (visitInitialAgingTier != currentAgingTier)
                {
                    this.SendTierChangeNotification(visit, currentAgingTier, actionVisitPayUserId);
                }

                visit.VisitAges.Add(visitAge);
                visit.ProcessedDate = dateToProcess;
                this._visitAgeService.Value.SaveVisit(visit);

                uow.Commit();
            }

        }

        private void SetVisitAgingTier(Visit visit, AgingTierEnum agingTierEnum, int actionVisitPayUserId)
        {
            if (visit.CurrentVisitAgingTier?.AgingTierEnum < agingTierEnum)
            {
                return;
            }

            using (UnitOfWork uow = new UnitOfWork(this._session))
            {
                //find the target config
                IList<AgingTierConfiguration> agingTierConfigurations = this._visitAgeService.Value.GetActiveAgingTierConfigurations();
                AgingTierConfiguration targetTierConfiguration = agingTierConfigurations.Single(x => x.ResultingAgingTier.AgingTierEnum == agingTierEnum);

                //adjust the visit age
                int daysToAge = targetTierConfiguration.MinAge - visit.VisitAges.Sum(x => x.Age);
                this.AgeVisit(visit, DateTime.UtcNow, daysToAge, actionVisitPayUserId);

                this._visitAgeService.Value.SaveVisit(visit);
                uow.Commit();
            }
        }

        private void ResetVisitAge(Visit visit, int? actionVisitPayUserId)
        {
            using (UnitOfWork unitOfWork = new UnitOfWork(this._session))
            {
                // find communications to reverse
                IEnumerable<IGrouping<CommunicationType, VisitCommunication>> visitCommunicationsToReverse = visit.VisitCommunications.GroupBy(x => x.CommunicationType);

                // add reversal record for each communication type
                foreach (IGrouping<CommunicationType, VisitCommunication> visitCommunications in visitCommunicationsToReverse)
                {
                    int communicationCountForType = visitCommunications.Sum(x => x.CommunciationIncrement);

                    visit.AddVisitCommunications(DateTime.MinValue, visitCommunications.Key, -communicationCountForType, actionVisitPayUserId);
                }

                // add reversal record for visit age
                VisitAge visitAge = new VisitAge
                {
                    Visit = visit,
                    Age = -visit.AgesCount,
                    SnapshotAgeSum = 0,
                    SnapshotCommunications = visit.VisitCommunicationsJsonData,
                    AgingTier = new AgingTier { AgingTierId = (int)AgingTierEnum.NotStatemented },
                    ActionVisitPayUserId = actionVisitPayUserId
                };
                visit.VisitAges.Add(visitAge);


                // save visit
                this._visitAgeService.Value.SaveVisit(visit);
                unitOfWork.Commit();
            }
        }

        /// <summary>
        /// snapShotAgeSum: 
        ///     In the case of adding a VisitAge it will be visit.VisitAges.Sum(x => x.Age) + 1;
        ///     In the case of adding a VisitCommunication it will visit.VisitAges.Sum(x => x.Age)
        /// </summary>
        private AgingTier ProcessAgeTierCalculation(Visit visit, int snapShotAgeSum)
        {
            IList<AgingTierConfiguration> configurations = this._visitAgeService.Value.GetActiveAgingTierConfigurations();
            AgingTierGenerator agingTierGenerator = new AgingTierGenerator(configurations, visit, snapShotAgeSum);
            AgingTier currentAgingTier = agingTierGenerator.CalculateAgingTier()?.ResultingAgingTier;
            return currentAgingTier;
        }

        private void SendTierChangeNotification(Visit visit, AgingTier currentAgingTier, int? actionVisitPayUserId = null)
        {
            if (actionVisitPayUserId.HasValue)
            {
                this.SendTierChangeNotification<VisitAgeChangedMessageHighPriority>(visit, currentAgingTier, actionVisitPayUserId);
            }
            else
            {
                this.SendTierChangeNotification<VisitAgeChangedMessage>(visit, currentAgingTier);
            }
        }

        private void SendTierChangeNotification<TOutboundMessageType>(Visit visit, AgingTier currentAgingTier, int? actionVisitPayUserId = null) where TOutboundMessageType : UniversalMessage, IVisitAgeChangedMessage, new()
        {
            string currentTier = visit.CurrentVisitAgingTier?.AgingTierEnum.ToString() ?? "Not Set";
            TOutboundMessageType visitAgeChangedMessage = new TOutboundMessageType
            {
                VisitSourceSystemKey = visit.VisitSourceSystemKey,
                VisitBillingSystemId = visit.VisitBillingSystemId,
                AgingTier = (AgingTierEnum)(currentAgingTier?.AgingTierId ?? 0),
                AgingTierMessage = $"Visit aging tier changed from {currentTier} to {currentAgingTier?.AgingTierEnum}",
                ActionVisitPayUserId = actionVisitPayUserId
            };

            this._session.Transaction.RegisterPostCommitSuccessAction(message =>
            {
                this.Bus.Value.PublishMessage(message);
            }, visitAgeChangedMessage);
        }

        void IJobRunnerService<QueueAgeVisitsJobRunner>.Execute(DateTime begin, DateTime end, string[] args)
        {
            this.QueueAgeVisits(DateTime.UtcNow);
        }

    }
}
﻿namespace Ivh.Application.Settings
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Common.Interfaces;
    using Domain.Settings.Interfaces;

    public class SettingsApplicationService : ISettingsApplicationService
    {
        private readonly Lazy<ISettingsService> _settingsService;

        public SettingsApplicationService(Lazy<ISettingsService> settingsService)
        {
            this._settingsService = settingsService;
        }

        public async Task<IReadOnlyDictionary<string, string>> GetAllSettingsAsync()
        {
            return await this._settingsService.Value.GetAllSettingsAsync();
        }

        public async Task<IReadOnlyDictionary<string, string>> GetAllManagedSettingsAsync()
        {
            return await this._settingsService.Value.GetAllManagedSettingsAsync();
        }

        public async Task<IReadOnlyDictionary<string, string>> GetAllEditableManagedSettingsAsync()
        {
            return await this._settingsService.Value.GetAllEditableManagedSettingsAsync();
        }

        public async Task<bool> InvalidateCacheAsync()
        {
            return await this._settingsService.Value.InvalidateCacheAsync();
        }

        public async Task<bool> OverrideManagedSettingAsync(string key, string overrideValue)
        {
            return await this._settingsService.Value.OverrideManagedSettingAsync(key, overrideValue);
        }

        public async Task<bool> ClearManagedSettingOverridesAsync()
        {
            return await this._settingsService.Value.ClearManagedSettingOverridesAsync();
        }

        public async Task<bool> ClearManagedSettingOverrideAsync(string key)
        {
            return await this._settingsService.Value.ClearManagedSettingOverrideAsync(key);
        }

        public string GetManagedSettingsHash()
        {
            return this._settingsService.Value.GetManagedSettingsHash();
        }

        public string GetEditableManagedSettingsHash()
        {
            return this._settingsService.Value.GetEditableManagedSettingsHash();
        }
    }
}

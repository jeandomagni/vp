﻿namespace Ivh.Application.User.Common.Dtos
{
    using System.Security.Principal;
    using System.Web;
    using System.Web.SessionState;

    public class HttpContextDto
    {
        public virtual string UserHostAddress { get; set; }
        public virtual string Url { get; set; }
        public virtual IPrincipal User { get; set; }
        public virtual HttpRequest Request { get; set; }
        public virtual HttpResponse Response { get; set; }
        public virtual HttpSessionState Session { get; set; }
    }
}

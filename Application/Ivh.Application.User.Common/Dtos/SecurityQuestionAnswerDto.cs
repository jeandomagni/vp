namespace Ivh.Application.User.Common.Dtos
{
    public class SecurityQuestionAnswerDto
    {
        public SecurityQuestionDto SecurityQuestion { get; set; }
        public string Answer { get; set; }
        public int VisitPayUserId { get; set; }
    }
}
﻿namespace Ivh.Application.User.Common.Dtos
{
    using System.Collections.Generic;

    public class VisitPayUserResultsDto
    {
        public IReadOnlyList<VisitPayUserDto> VisitPayUsers { get; set; }
        public int TotalRecords { get; set; }
    }
}

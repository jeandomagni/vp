﻿namespace Ivh.Application.User.Common.Dtos
{
    using System;
    using Ivh.Common.Base.Enums;
    using Ivh.Common.VisitPay.Enums;

    public class VisitPayUserAcknowledgementDto
    {
        public virtual VisitPayUserDto VisitPayUser { get; set; }
        public virtual int CmsVersionId { get; set; }
        public virtual int? ClientCmsVersionId { get; set; }
        public virtual DateTime AcknowledgementDate { get; set; }
        public virtual SmsPhoneTypeEnum? SmsPhoneType { get; set; }
    }
}
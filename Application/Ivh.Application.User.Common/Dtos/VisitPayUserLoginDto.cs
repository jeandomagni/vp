﻿namespace Ivh.Application.User.Common.Dtos
{
    public class VisitPayUserLoginDto
    {
        public int VisitPayUserId { get; set; }
        public string LoginProvider { get; set; }

        public string ProviderKey { get; set; }
        public int VisitPayUserLoginId { get; set; }
    }
}
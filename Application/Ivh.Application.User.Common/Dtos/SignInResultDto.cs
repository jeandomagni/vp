﻿namespace Ivh.Application.User.Common.Dtos
{
    using Ivh.Common.VisitPay.Enums;

    public class SignInResultDto
    {
        public SignInResultDto(SignInStatusExEnum signInStatus)
        {
            this.SignInStatus = signInStatus;
        }

        public SignInResultDto(SignInStatusExEnum signInStatus, int visitPayUserId)
        {
            this.SignInStatus = signInStatus;
            this.VisitPayUserId = visitPayUserId;
        }

        public SignInStatusExEnum SignInStatus { get; }

        public int VisitPayUserId { get; }
    }
}
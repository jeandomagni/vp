namespace Ivh.Application.User.Common.Dtos
{
    public class VisitPayRoleDto
    {
        public int VisitPayRoleId { get; set; }
        public string Name { get; set; }
        public string VisitPayRoleDescription { get; set; }
    }
}
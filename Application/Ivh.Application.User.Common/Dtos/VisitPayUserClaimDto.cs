﻿namespace Ivh.Application.User.Common.Dtos
{
    public class VisitPayUserClaimDto
    {
        public int VisitPayUserClaimId { get; set; }
        public string ClaimType { get; set; }

        public string ClaimValue { get; set; }

        public VisitPayUserDto User { get; set; }
    }
}
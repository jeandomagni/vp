﻿namespace Ivh.Application.User.Common.Dtos
{
    using System.Collections.Generic;
    using Ivh.Common.Base.Utilities.Helpers;

    public class VisitPayUserFilterDto
    {
        public string LastName { get; set; }
        public string FirstName { get; set; }
        public string UserName { get; set; }
        public IList<int> Roles { get; set; }
        public string SortField { get; set; }
        public string SortOrder { get; set; }
        public bool IsNotIvhAdminOrIvhUserAuditor { get; set; }
        public bool IsNotClientUserManagementOrClientUserAuditor { get; set; }
        public string LastNameFirstName => FormatHelper.LastNameFirstName(this.LastName, this.FirstName);
        public string FirstNameLastName => FormatHelper.FirstNameLastName(this.FirstName, this.LastName);
    }
}

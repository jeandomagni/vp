﻿namespace Ivh.Application.User.Common.Dtos
{
    using System.Collections.Generic;
    using Base.Common.Dtos;
    using Ivh.Common.VisitPay.Enums;

    public class AddressUpdateResultDto : DataResult<IEnumerable<string>, bool>
    {
        public AddressUpdateResultDto(IEnumerable<string> data, bool result) : base(data, result)
        {
        }

        public AddressStatusEnum? AddressStatus { get; set; }
    }
}
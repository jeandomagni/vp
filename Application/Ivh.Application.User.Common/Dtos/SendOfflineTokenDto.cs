﻿namespace Ivh.Application.User.Common.Dtos
{
    public class SendOfflineTokenDto
    {
        public SendOfflineTokenDto(int vpGuarantorId, string emailAddress)
        {
            this.VpGuarantorId = vpGuarantorId;
            this.EmailAddress = emailAddress;
        }

        public int VpGuarantorId { get; }
        public string EmailAddress { get; }
        public bool SendSms { get; set; }
        public string PhoneNumber { get; set; }
    }
}
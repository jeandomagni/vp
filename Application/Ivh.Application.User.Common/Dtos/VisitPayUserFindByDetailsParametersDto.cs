﻿using System;

namespace Ivh.Application.User.Common.Dtos
{
    public class VisitPayUserFindByDetailsParametersDto
    {
        public string LastName { get; set; }
        
        public DateTime DateOfBirth { get; set; }

        /// <summary>
        /// Used in Request Password flows
        /// </summary>
        public string Username { get; set; }

        //Flex fields
        //These fields can be configured to be used or omitted in the query based off 
        //client setting AccountRecoveryConfiguration
        public string FirstName { get; set; }
        public int? Ssn4 { get; set; }
        public string EmailAddress { get; set; }
        
        
        // The following are currently set vis automapper in Patient Startup.Ioc.cs
        public bool UseSsn { get; set; }
        public bool UseFirstName { get; set; }
        public bool UseEmailAddress { get; set; }


    }
}

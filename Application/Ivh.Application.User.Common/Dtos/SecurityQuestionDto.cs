namespace Ivh.Application.User.Common.Dtos
{
    public class SecurityQuestionDto
    {
        public int SecurityQuestionId { get; set; }
        public string Question { get; set; }
        public bool IsActive { get; set; }
    }
}
﻿using System.Collections.Generic;

namespace Ivh.Application.User.Common.Dtos
{
    using System.Linq;
    using Ivh.Common.Base.Enums;
    using Ivh.Common.VisitPay.Enums;

    public class AccountRecoveryFieldConfigurationDto
    {
        public AccountRecoveryMatchFieldEnum Field { get; set; }
        public bool Required { get; set; }
        public bool Visible { get; set; }
    }

    public class AccountRecoveryConfigurationDto:List<AccountRecoveryFieldConfigurationDto>
    {
        public AccountRecoveryFieldConfigurationDto ConfigurationForField(AccountRecoveryMatchFieldEnum field) => this.FirstOrDefault(x => x.Field == field);
    }
}

﻿namespace Ivh.Application.User.Common.Dtos
{
    using System;

    public class OfflineGuarantorSearchResultDto
    {
        public string SourceSystemKey { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public DateTime? DOB { get; set; }

        public string SSN4 { get; set; }

        public string AddressLine1 { get; set; }

        public string AddressLine2 { get; set; }

        public string City { get; set; }

        public string StateProvince { get; set; }

        public string PostalCode { get; set; }

        public string Country { get; set; }
    }
}
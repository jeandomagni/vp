﻿namespace Ivh.Application.User.Common.Dtos
{
    using Ivh.Common.VisitPay.Enums;

    public class OfflineGuarantorSearchResultsDto
    {
        public OfflineGuarantorSearchResultsDto(OfflineGuarantorSearchResultStatusEnum status, OfflineGuarantorSearchResultDto result)
        {
            this.Status = status;
            this.Result = result;
        }

        public OfflineGuarantorSearchResultStatusEnum Status { get;  }

        public OfflineGuarantorSearchResultDto Result { get; }
    }
}

﻿namespace Ivh.Application.User.Common.Dtos
{
    using Ivh.Common.Base.Utilities.Helpers;
    using Ivh.Common.VisitPay.Attributes;
    using Ivh.Common.VisitPay.Constants;
    using Ivh.Common.VisitPay.Enums;
    using System;
    using System.Collections.Generic;
    using System.Linq;

    public class VisitPayUserDto
    {
        public VisitPayUserDto()
        {
            this.Roles = new List<VisitPayRoleDto>();
            this.SecurityQuestionAnswers = new List<SecurityQuestionAnswerDto>();
        }

        public int VisitPayUserId { get; set; }
        public Guid VisitPayUserGuid { get; set; }
        public string TempPassword { get; set; }
        public DateTime? TempPasswordExpDate { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }

        public string DisplayLastNameFirstName => this.VisitPayUserId == SystemUsers.SystemUserId ? "System" : FormatHelper.LastNameFirstName(this.LastName, this.FirstName);

        public string DisplayFirstNameLastName => this.VisitPayUserId == SystemUsers.SystemUserId ? "System" : FormatHelper.FirstNameLastName(this.FirstName, this.LastName);

        public string DisplayFullName => this.VisitPayUserId == SystemUsers.SystemUserId ? "System" : FormatHelper.FullName(this.FirstName, this.MiddleName, this.LastName);

        public string AddressStreet1 { get; set; }
        public string AddressStreet2 { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Zip { get; set; }

        public string DisplayAddress => string.Join(", ", new string[] { this.AddressStreet1, this.AddressStreet2, this.City, this.State, this.Zip }.Where(s => !string.IsNullOrEmpty(s)));

        public string MailingAddressStreet1 { get; set; }
        public string MailingAddressStreet2 { get; set; }
        public string MailingCity { get; set; }
        public string MailingState { get; set; }
        public string MailingZip { get; set; }

        public string DisplayMailingAddress => string.Join(", ", new string[] { this.MailingAddressStreet1, this.MailingAddressStreet2, this.MailingCity, this.MailingState, this.MailingZip }.Where(s => !string.IsNullOrEmpty(s)));

        public List<string> DisplayAddresses
        {
            get
            {
                List<string> addresses = new List<string>();

                string address = this.DisplayAddress;
                if (!string.IsNullOrEmpty(address))
                {
                    addresses.Add(address);
                }

                string mailing = this.DisplayMailingAddress;
                if (!string.IsNullOrEmpty(mailing))
                {
                    addresses.Add(mailing);
                }

                return addresses;
            }
        }

        public List<string> PhoneNumbers
        {
            get
            {
                List<string> phoneNumbers = new List<string>();

                if (!string.IsNullOrEmpty(this.PhoneNumber))
                {
                    phoneNumbers.Add(this.PhoneNumber);
                }

                if (!string.IsNullOrEmpty(this.PhoneNumberSecondary))
                {
                    phoneNumbers.Add(this.PhoneNumberSecondary);
                }

                return phoneNumbers;
            }
        }

        public string Gender { get; set; }
        public int? SSN4 { get; set; }
        public DateTime? DateOfBirth { get; set; }
        public int AccessFailedCount { get; set; }
        public string Email { get; set; }
        public bool EmailConfirmed { get; set; }
        public bool LockoutEnabled { get; set; }
        public DateTime? LockoutEndDateUtc { get; set; }
        public LockoutReasonEnum? LockoutReasonEnum { get; set; }
        public string PasswordHash { get; set; }
        public string PhoneNumber { get; set; }
        public string PhoneNumberType { get; set; }
        public bool PhoneNumberConfirmed { get; set; }
        public bool TwoFactorEnabled { get; set; }
        public string Id { get; set; }
        public string UserName { get; set; }
        public string SecurityStamp { get; set; }
        public string PhoneNumberSecondary { get; set; }
        public string PhoneNumberSecondaryType { get; set; }
        public IList<VisitPayRoleDto> Roles { get; set; }
        public IList<SecurityQuestionAnswerDto> SecurityQuestionAnswers { get; set; }
        public DateTime? LastLoginDate { get; set; }

        public string SecurityValidationValue { get; set; }

        public DateTime? PasswordExpiresUtc { get; set; }
        public virtual int PasswordHashConfigurationVersionId { get; set; }

        public bool IsLocked { get; set; }

        public bool IsDisabled { get; set; }

        public DateTime? InsertDate { get; set; }

        public virtual string SmsPhoneNumber { get; set; }
        public bool PhoneNumberTypeIsMobile { get; set; }

        public bool PhoneNumberSecondaryTypeIsMobile { get; set; }
        public string Locale { get; set; }

        [Phi]
        public string AccessTokenPhi { get; set; }
    }
}
﻿namespace Ivh.Application.User.Common.Interfaces
{
    using System;
    using System.Threading.Tasks;
    using Dtos;
    using Ivh.Common.EventJournal;

    public interface IOfflineUserApplicationService
    {
        SignInResultDto SignIn(string lastName, DateTime dateOfBirth, string token, JournalEventHttpContextDto context);
        VisitPayUserDto FindById(int visitPayUserId);
        VisitPayUserDto FindByGuarantorId(int vpGuarantorId);
        bool SendFinancePlanNotification(SendOfflineTokenDto tokenDto);
        bool SendLoginToken(SendOfflineTokenDto tokenDto);
        OfflineGuarantorSearchResultsDto GetHsGuarantors(string sourceSystemKey, string lastName);
    }
}
﻿namespace Ivh.Application.User.Common.Interfaces
{
    using Base.Common.Dtos;
    using Dtos;
    using Ivh.Common.Base.Interfaces;
    using Ivh.Common.VisitPay.Enums;
    using System.Threading.Tasks;

    public interface IVisitPayUserAddressApplicationService : IApplicationService
    {
        Task<AddressDto> GetAddressAsync(int visitPayUserId, VisitPayUserAddressTypeEnum addressType);
        Task<AddressUpdateResultDto> UpdateAddressAsync(int visitPayUserId, AddressDto addressDto, VisitPayUserAddressTypeEnum addressType, bool validate,
            bool tryGetFromCache = true);
    }
}
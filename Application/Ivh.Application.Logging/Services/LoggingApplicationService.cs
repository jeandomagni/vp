﻿namespace Ivh.Application.Logging.Services
{
    using System;
    using Base.Common.Interfaces;
    using Base.Services;
    using Common.Interfaces;
    using Domain.Logging.Interfaces;

    public class LoggingApplicationService : ApplicationService, ILoggingApplicationService
    {
        private readonly ILoggingService _loggingService;

        public LoggingApplicationService(
            Lazy<IApplicationServiceCommonService> commonServices,
            ILoggingService loggingService)
            : base(commonServices)
        {
            this._loggingService = loggingService;
        }

        public void Debug(Func<string> message, params object[] parameters)
        {
            this._loggingService.Debug(message, parameters);
        }

        public void Fatal(Func<string> message, params object[] parameters)
        {
            this._loggingService.Fatal(message, parameters);
        }

        public void Fatal(Func<string> message, string stackTrace, params object[] parameters)
        {
            this._loggingService.Fatal(message, stackTrace, parameters);
        }

        public void Info(Func<string> message, params object[] parameters)
        {
            this._loggingService.Info(message, parameters);
        }

        public void Warn(Func<string> message, params object[] parameters)
        {
            this._loggingService.Warn(message, parameters);
        }

        public void SetApplicationNameAndVersion(string applicationName, string applicationVersion)
        {
            this._loggingService.SetApplicationNameAndVersion(applicationName, applicationVersion);
        }

        public void SetApplicationNameAndVersion(string applicationName, string applicationVersion, DateTime date, int threadId, Guid correlationId)
        {
            this._loggingService.SetApplicationNameAndVersion(applicationName, applicationVersion, date, threadId, correlationId);
        }


    }
}
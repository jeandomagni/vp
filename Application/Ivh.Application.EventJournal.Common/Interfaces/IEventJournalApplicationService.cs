﻿namespace Ivh.Application.EventJournal.Common.Interfaces
{
    using Ivh.Common.Base.Interfaces;
    using Ivh.Common.ServiceBus.Interfaces;
    using Ivh.Common.VisitPay.Messages.EventJournal;

    public interface IEventJournalApplicationService : IApplicationService, IUniversalConsumerService<JournalEventMessage>
    {
    }
}

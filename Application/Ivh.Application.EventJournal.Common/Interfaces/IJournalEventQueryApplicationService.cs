﻿namespace Ivh.Application.EventJournal.Common.Interfaces
{
    using Dtos;
    using Ivh.Common.Base.Interfaces;

    public interface IJournalEventQueryApplicationService : IApplicationService
    {
        JournalEventResultsDto GetJournalEvents(JournalEventFilterDto journalEventFilter);
        JournalEventDto GetJournalEventDetail(int journalEventId);
        byte[] ExportJournalEvents(JournalEventFilterDto journalEventFilter);
    }
}

﻿namespace Ivh.Application.EventJournal.Common.Dtos
{
    using System;
    using System.Collections.Generic;
    using Ivh.Common.Base.Enums;
    using Ivh.Common.VisitPay.Enums;

    public class JournalEventFilterDto
    {
        public DateTime StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public string SortField { get; set; }
        public string SortOrder { get; set; }
        public int? Page { get; set; }
        public int? Rows { get; set; }
        public int? VpGuarantorId { get; set; }
        public int? VisitPayUserId { get; set; }
        public string UserName { get; set; }
        public IList<JournalEventTypeEnum> JournalEventTypes { get; set; }
        public IList<JournalEventCategoryEnum> JournalEventCategories { get; set; }
    }
}

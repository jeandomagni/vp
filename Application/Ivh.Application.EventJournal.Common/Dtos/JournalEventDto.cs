﻿namespace Ivh.Application.EventJournal.Common.Dtos
{
    using System;
    using Ivh.Common.Base.Enums;
    using Ivh.Common.VisitPay.Enums;

    public class JournalEventDto
    {
        public int JournalEventId { get; set; }
        public DateTime EventDateTime { get; set; }
        public JournalEventTypeEnum JournalEventType { get; set; }
        public JournalEventCategoryEnum JournalEventCategory { get; set; }
        public string EventTags { get; set; }
        public Guid CorrelationId { get; set; }
        public ApplicationEnum Application { get; set; }
        public int EventVisitPayUserId { get; set; }

        public DeviceTypeEnum? DeviceType { get; set; }
        public string UserAgent { get; set; }
        public int UserAgentId { get; set; }
        public string UserHostAddress { get; set; }
        public string JournalUserEventContext { get; set; }

        public int? VisitPayUserId { get; set; }
        public int? VpGuarantorId { get; set; }
        public int? HsGuarantorId { get; set; }
        public int? VisitId { get; set; }
        public int? VisitTransactionId { get; set; }
        public int? PaymentId { get; set; }
        public int? FinancePlanId { get; set; }
        public string JournalEventContext { get; set; }

        public string Message { get; set; }
        public string AdditionalData { get; set; }

        public string EventVisitPayUserIdUserName { get; set; }
        public int VpGuarantorIdUserName { get; set; }
        public int VpGuarantorIdTargetUserId { get; set; }
        public int VpGuarantorIdEmulateUserId { get; set; }
    }
}

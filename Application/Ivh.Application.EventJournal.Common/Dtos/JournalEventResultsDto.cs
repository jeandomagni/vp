﻿namespace Ivh.Application.EventJournal.Common.Dtos
{
    using System.Collections.Generic;

    public class JournalEventResultsDto
    {
        public IReadOnlyList<JournalEventDto> JournalEvents { get; set; }
        public int TotalRecords { get; set; }
    }
}

﻿namespace Ivh.Application.User.Services
{
    using AutoMapper;
    using Base.Common.Interfaces;
    using Base.Services;
    using Common.Dtos;
    using Common.Interfaces;
    using Core.Common.Dtos;
    using Domain.Guarantor.Entities;
    using Domain.Guarantor.Interfaces;
    using Domain.User.Entities;
    using Domain.User.Interfaces;
    using Domain.User.Services;
    using Ivh.Common.Base.Utilities.Extensions;
    using Ivh.Common.Data;
    using Ivh.Common.Data.Connection.Connections;
    using Ivh.Common.Data.Interfaces;
    using Ivh.Common.EventJournal;
    using Ivh.Common.ServiceBus.Common.Messages;
    using Ivh.Common.VisitPay.Enums;
    using Ivh.Common.VisitPay.Messages.Communication;
    using Ivh.Common.VisitPay.Messages.FinanceManagement;
    using Ivh.Common.Web.Extensions;
    using Microsoft.AspNet.Identity;
    using Microsoft.AspNet.Identity.Owin;
    using Microsoft.Owin.Security;
    using NHibernate;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Security.Claims;
    using System.Threading.Tasks;

    public class OfflineUserApplicationService : ApplicationService, IOfflineUserApplicationService
    {
        private readonly ISession _session;
        private readonly Lazy<IGuarantorService> _guarantorService;
        private readonly Lazy<IBaseHsGuarantorService> _hsGuarantorService;
        private readonly Lazy<VisitPaySignInService> _visitPaySignInService;
        private readonly Lazy<IVisitPayUserJournalEventService> _visitPayUserJournalEventService;
        private readonly Lazy<VisitPayUserService> _visitPayUserService;
        private readonly Lazy<IAuthenticationManager> _authenticationManager;

        public OfflineUserApplicationService(
            Lazy<IApplicationServiceCommonService> applicationServiceCommonService,
            ISessionContext<VisitPay> sessionContext,
            Lazy<IGuarantorService> guarantorService,
            Lazy<IBaseHsGuarantorService> hsGuarantorService,
            Lazy<VisitPaySignInService> visitPaySignInService,
            Lazy<IVisitPayUserJournalEventService> visitPayUserJournalEventService,
            Lazy<VisitPayUserService> visitPayUserService,
            Lazy<IAuthenticationManager> authenticationManager = null) // optional to allow jobrunner and console apps to use this service)
            : base(applicationServiceCommonService)
        {
            this._session = sessionContext.Session;
            this._guarantorService = guarantorService;
            this._hsGuarantorService = hsGuarantorService;
            this._visitPaySignInService = visitPaySignInService;
            this._visitPayUserJournalEventService = visitPayUserJournalEventService;
            this._visitPayUserService = visitPayUserService;
            this._authenticationManager = authenticationManager;
        }

        #region authorize

        public SignInResultDto SignIn(string lastName, DateTime dateOfBirth, string token, JournalEventHttpContextDto context)
        {
            if (!this.FeatureService.Value.IsFeatureEnabled(VisitPayFeatureEnum.OfflineVisitPay))
            {
                throw new Exception($"{nameof(VisitPayFeatureEnum.OfflineVisitPay)} is disabled");
            }

            token = token.TrimNullSafe();
            lastName = lastName.TrimNullSafe();
            bool isAccessTokenPhiFormat = VisitPayUserAccessTokenPhi.IsValid(token);

            // find user
            VisitPayUser user =
                isAccessTokenPhiFormat
                    ? this._visitPayUserService.Value.FindByDetailsAndAccessTokenPhi(lastName, dateOfBirth, token)
                    : this._visitPayUserService.Value.FindByDetailsAndToken(lastName, dateOfBirth, token);

            if (user == null)
            {
                return new SignInResultDto(SignInStatusExEnum.Failure);
            }

            int guarantorId = this._guarantorService.Value.GetGuarantorId(user.VisitPayUserId);
            Guarantor guarantor = this._guarantorService.Value.GetGuarantor(guarantorId);
            if (guarantor == null)
            {
                return new SignInResultDto(SignInStatusExEnum.Failure);
            }

            // check the guarantor type
            if (!guarantor.IsOfflineGuarantor())
            {
                this.LoggingService.Value.Info(() => $"{nameof(this.SignIn)} VpGuarantorType = {guarantor.VpGuarantorTypeEnum}, parameters = {string.Join(",", lastName, dateOfBirth, token)}");
                return new SignInResultDto(SignInStatusExEnum.Failure);
            }

            // check the token
            // if invalid or expired, send a new one
            bool tokenValid = this._visitPayUserService.Value.VerifyUserToken(user.VisitPayUserId.ToString(), TokenPurposeEnum.VpccLogin.ToString(), token);

            // run this verification for non-AccessTokenPhi-formatted values
            if (!tokenValid && !isAccessTokenPhiFormat)
            {
                this.LoggingService.Value.Info(() => $"{nameof(this.SignIn)} Token Invalid: {user.TempPasswordExpDate}, parameters = {string.Join(",", lastName, dateOfBirth, token)}");

                this.SendLoginToken(new SendOfflineTokenDto(guarantorId, user.Email));
                return new SignInResultDto(SignInStatusExEnum.PasswordExpired);
            }

            // sign in
            SignInStatus signInStatus = this._visitPaySignInService.Value.SignInSimple(user, true, true);
            if (signInStatus != SignInStatus.Success)
            {
                this.LoggingService.Value.Info(() => $"{nameof(this.SignIn)} Signin failed: {signInStatus}, parameters = {string.Join(",", lastName, dateOfBirth, token)}");
                return new SignInResultDto(SignInStatusExEnum.Failure);
            }

            // add claims
            this.AddClaims(guarantor.VpGuarantorId.ToString(), user);

            // log events
            JournalEventUserContext journalEventUserContext = this._visitPayUserJournalEventService.Value.GetJournalEventUserContext(context, user);
            this._visitPayUserJournalEventService.Value.LogLoginOffline(journalEventUserContext, guarantor.VpGuarantorId);
            this._visitPayUserJournalEventService.Value.LogGuarantorSelfVerification(journalEventUserContext, guarantor.VpGuarantorId);

            // expire the token
            user.TempPasswordExpDate = DateTime.UtcNow;
            this._visitPayUserService.Value.Update(user);

            return new SignInResultDto(SignInStatusExEnum.Success, user.VisitPayUserId);
        }

        private void AddClaims(string guarantorId, VisitPayUser user)
        {
            ClaimsIdentity identity = this._visitPayUserService.Value.CreateIdentity(user, DefaultAuthenticationTypes.ApplicationCookie);

            identity.TryAddClaim(ClaimTypeEnum.OfflineUserToken, "true");
            identity.TryAddClaim(ClaimTypeEnum.GuarantorId, guarantorId);
            identity.TryAddClaim(ClaimTypeEnum.ClientFirstName, user.FirstName);
            identity.TryAddClaim(ClaimTypeEnum.ClientLastName, user.LastName);

            this._authenticationManager.Value.AuthenticationResponseGrant = new AuthenticationResponseGrant(new ClaimsPrincipal(identity), new AuthenticationProperties { IsPersistent = true });
        }

        #endregion

        #region lookup

        public VisitPayUserDto FindById(int visitPayUserId)
        {
            Guarantor vpGuarantor = this._guarantorService.Value.GetGuarantorEx(visitPayUserId);
            if (vpGuarantor?.User == null || !vpGuarantor.IsOfflineGuarantor())
            {
                return null;
            }

            return Mapper.Map<VisitPayUserDto>(vpGuarantor.User);
        }

        public VisitPayUserDto FindByGuarantorId(int vpGuarantorId)
        {
            Guarantor vpGuarantor = this._guarantorService.Value.GetGuarantor(vpGuarantorId);
            if (vpGuarantor?.User == null || !vpGuarantor.IsOfflineGuarantor())
            {
                return null;
            }

            return Mapper.Map<VisitPayUserDto>(vpGuarantor.User);
        }

        #endregion

        #region tokens

        public bool SendLoginToken(SendOfflineTokenDto tokenDto)
        {
            string token = this.GenerateToken(tokenDto);
            if (string.IsNullOrWhiteSpace(token))
            {
                return false;
            }

            SendVpccLoginLinkEmailMessage email = new SendVpccLoginLinkEmailMessage
            {
                VpGuarantorId = tokenDto.VpGuarantorId,
                TempPassword = token
            };

            SendVpccLoginLinkSmsMessage sms = new SendVpccLoginLinkSmsMessage
            {
                VpGuarantorId = tokenDto.VpGuarantorId,
                TempPassword = token,
                PhoneNumber = tokenDto.PhoneNumber
            };

            this.SendCommunications(tokenDto, email, sms);
            return true;
        }

        public bool SendFinancePlanNotification(SendOfflineTokenDto tokenDto)
        {
            string token = this.GenerateToken(tokenDto);
            if (string.IsNullOrWhiteSpace(token))
            {
                return false;
            }

            SendVpccFinancePlanCreationEmailMessage email = new SendVpccFinancePlanCreationEmailMessage
            {
                VpGuarantorId = tokenDto.VpGuarantorId,
                TempPassword = token
            };

            SendVpccFinancePlanCreationSmsMessage sms = new SendVpccFinancePlanCreationSmsMessage
            {
                VpGuarantorId = tokenDto.VpGuarantorId,
                TempPassword = token,
                PhoneNumber = tokenDto.PhoneNumber
            };

            this.SendCommunications(tokenDto, email, sms);
            return true;
        }

        /// <summary>
        /// this will invalidate any previous token
        /// </summary>
        private string GenerateToken(SendOfflineTokenDto tokenDto)
        {
            if (!this.FeatureService.Value.IsFeatureEnabled(VisitPayFeatureEnum.OfflineVisitPay))
            {
                return null;
            }

            Guarantor guarantor = this._guarantorService.Value.GetGuarantor(tokenDto.VpGuarantorId);
            if (!guarantor.IsOfflineGuarantor())
            {
                return null;
            }

            using (IUnitOfWork uow = new UnitOfWork(this._session))
            {
                VisitPayUser visitPayUser = this._visitPayUserService.Value.FindById(guarantor.User.VisitPayUserId.ToString());

                // update email address
                visitPayUser.Email = tokenDto.EmailAddress;
                this._visitPayUserService.Value.Update(visitPayUser);

                // generate token
                string tempPassword = this._visitPayUserService.Value.GenerateOfflineLoginToken(guarantor.User);

                uow.Commit();
                return tempPassword;
            }
        }

        private void SendCommunications<TEmail, TSms>(SendOfflineTokenDto tokenDto, TEmail email, TSms sms)
            where TEmail : UniversalMessage
            where TSms : UniversalMessage
        {
            // send email
            this._session.RegisterPostCommitSuccessAction(m =>
            {
                this.Bus.Value.PublishMessage(m).Wait();
            }, email);

            if (sms != null && tokenDto.SendSms && !string.IsNullOrWhiteSpace(tokenDto.PhoneNumber))
            {
                this._session.RegisterPostCommitSuccessAction(m =>
                {
                    this.Bus.Value.PublishMessage(m).Wait();
                }, sms);
            }
        }

        #endregion

        #region search

        public OfflineGuarantorSearchResultsDto GetHsGuarantors(string sourceSystemKey, string lastName)
        {
            IList<BaseHsGuarantor> hsGuarantorDetails = this._hsGuarantorService.Value.GetBaseHsGuarantors(lastName, sourceSystemKey);
            if (!hsGuarantorDetails.Any())
            {
                return new OfflineGuarantorSearchResultsDto(OfflineGuarantorSearchResultStatusEnum.NotFound, null);
            }

            if (hsGuarantorDetails.Count > 1)
            {
                return new OfflineGuarantorSearchResultsDto(OfflineGuarantorSearchResultStatusEnum.Ambiguous, null);
            }

            OfflineGuarantorSearchResultDto offlineGuarantorSearchResultDto = hsGuarantorDetails.Select(x => new OfflineGuarantorSearchResultDto
            {
                AddressLine1 = x.AddressLine1,
                AddressLine2 = x.AddressLine2,
                City = x.City,
                Country = x.Country,
                DOB = x.DOB,
                FirstName = x.FirstName,
                LastName = x.LastName,
                PostalCode = x.PostalCode,
                SourceSystemKey = x.SourceSystemKey,
                StateProvince = x.StateProvince,
                SSN4 = x.SSN4
            }).First();

            return new OfflineGuarantorSearchResultsDto(OfflineGuarantorSearchResultStatusEnum.Found, offlineGuarantorSearchResultDto);
        }

        #endregion
    }
}
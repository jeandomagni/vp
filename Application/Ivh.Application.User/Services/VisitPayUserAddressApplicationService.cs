﻿namespace Ivh.Application.User.Services
{
    using Base.Common.Dtos;
    using Base.Common.Interfaces;
    using Base.Services;
    using Common.Dtos;
    using Common.Interfaces;
    using Domain.User.Entities;
    using Domain.User.Interfaces;
    using Ivh.Common.VisitPay.Enums;
    using Microsoft.AspNet.Identity;
    using Newtonsoft.Json;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;

    public class VisitPayUserAddressApplicationService : ApplicationService, IVisitPayUserAddressApplicationService
    {
        private readonly Lazy<IVisitPayUserService> _visitPayUserService;
        private readonly Lazy<IVisitPayUserAddressVerificationService> _visitPayUserAddressVerificationService;

        public VisitPayUserAddressApplicationService(
            Lazy<IApplicationServiceCommonService> applicationServiceCommonService,
            Lazy<IVisitPayUserService> visitPayUserService,
            Lazy<IVisitPayUserAddressVerificationService> visitPayUserAddressVerificationService
        ) : base(applicationServiceCommonService)
        {
            this._visitPayUserService = visitPayUserService;
            this._visitPayUserAddressVerificationService = visitPayUserAddressVerificationService;
        }

        public async Task<AddressDto> GetAddressAsync(
            int visitPayUserId,
            VisitPayUserAddressTypeEnum addressType)
        {
            VisitPayUser user = this._visitPayUserService.Value.FindUserById(visitPayUserId);
            if (user == null)
            {
                return null;
            }

            VisitPayUserAddress address = null;

            switch (addressType)
            {
                case VisitPayUserAddressTypeEnum.Mailing:
                    address = user.CurrentMailingAddress;
                    break;
                case VisitPayUserAddressTypeEnum.Physical:
                    address = user.CurrentPhysicalAddress;
                    break;
            }

            if (address == null)
            {
                return null;
            }

            DataResult<IEnumerable<string>, bool> uspsAddress =
                await this._visitPayUserAddressVerificationService.Value
                    .GetStandardizedMailingAddressAsync(user.VisitPayUserId, address, true);

            UspsAddressDto uspsAddressDeserialized =
                uspsAddress.Result
                    ? JsonConvert.DeserializeObject<UspsAddressDto>(uspsAddress.Data.First())
                    : new UspsAddressDto();

            AddressDto addressDto = new AddressDto
            {
                AddressStreet1 = address.AddressStreet1 ?? string.Empty,
                AddressStreet2 = address.AddressStreet2 ?? string.Empty,
                City = address.City ?? string.Empty,
                PostalCode = address.PostalCode ?? string.Empty,
                StateProvince = address.StateProvince ?? string.Empty,

                IncludesUspsStandardizedAddress = uspsAddress.Result,
                UspsAddressStreet1 = uspsAddressDeserialized.Address1,
                UspsAddressStreet2 = uspsAddressDeserialized.Address2,
                UspsCity = uspsAddressDeserialized.City,
                UspsStateProvince = uspsAddressDeserialized.State,
                UspsPostalCode = uspsAddress.Result ? $"{uspsAddressDeserialized.Zip5}-{uspsAddressDeserialized.Zip4}" : string.Empty
            };

            return addressDto;
        }

        public async Task<AddressUpdateResultDto> UpdateAddressAsync(
            int visitPayUserId, 
            AddressDto addressDto, 
            VisitPayUserAddressTypeEnum addressType, 
            bool validate,
            bool tryGetFromCache = true)
        {
            VisitPayUser user = this._visitPayUserService.Value.FindUserById(visitPayUserId);
            if (user == null)
            {
                return new AddressUpdateResultDto(Enumerable.Empty<string>(), false);
            }

            VisitPayUserAddress address = this.GetVisitPayUserAddressForUpdate(addressDto);

            DataResult<IEnumerable<string>, bool> addressValidationResponse =
                await this._visitPayUserAddressVerificationService.Value
                    .GetStandardizedMailingAddressAsync(user.VisitPayUserId, address, tryGetFromCache);

            if (!addressValidationResponse.Result && validate)
            {
                return new AddressUpdateResultDto(addressValidationResponse.Data, false)
                {
                    AddressStatus = AddressStatusEnum.Rejected
                };
            }

            address.VisitPayUserAddressType = addressType;
            address.AddressStatus = addressValidationResponse.Result ? AddressStatusEnum.Verified : AddressStatusEnum.Rejected;
            address.VerificationCheckDate = DateTime.UtcNow;
            address.IsUspsVerifiedFormat = addressValidationResponse.Result && addressDto.IsUspsFormatSelected;

            user.SetCurrentAddress(address);
            IdentityResult result = this._visitPayUserService.Value.UpdateUser(user);

            return new AddressUpdateResultDto(result.Errors, result.Succeeded);
        }

        private VisitPayUserAddress GetVisitPayUserAddressForUpdate(AddressDto addressDto)
        {
            VisitPayUserAddress address = new VisitPayUserAddress();

            if (addressDto.IsUspsFormatSelected)
            {
                address.AddressStreet1 = addressDto.UspsAddressStreet1;
                address.AddressStreet2 = addressDto.UspsAddressStreet2;
                address.City = addressDto.UspsCity;
                address.StateProvince = addressDto.UspsStateProvince;
                address.PostalCode = addressDto.UspsPostalCode;

                return address;
            }

            address.AddressStreet1 = addressDto.AddressStreet1;
            address.AddressStreet2 = addressDto.AddressStreet2;
            address.City = addressDto.City;
            address.StateProvince = addressDto.StateProvince;
            address.PostalCode = addressDto.PostalCode;

            return address;
        }
    }
}
﻿namespace Ivh.Application.User.Modules
{
    using Autofac;
    using Common.Interfaces;
    using Services;

    public class Module : Autofac.Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<OfflineUserApplicationService>().As<IOfflineUserApplicationService>();
            builder.RegisterType<VisitPayUserAddressApplicationService>().As<IVisitPayUserAddressApplicationService>();
        }
    }
}
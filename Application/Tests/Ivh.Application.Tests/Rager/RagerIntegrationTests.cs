﻿namespace Ivh.Application.Tests.Rager
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using Aging.Common.Interfaces;
    using Castle.Core.Internal;
    using Common.Data.Connection;
    using Common.Data.Connection.Connections;
    using Common.Data.Interfaces;
    using Common.Tests.Helpers.VisitAge;
    using Common.VisitPay.Enums;
    using Common.VisitPay.Messages.Aging;
    using Common.VisitPay.Messages.Aging.Dtos;
    using Common.VisitPay.Messages.HospitalData;
    using Common.VisitPay.Messages.HospitalData.Dtos;
    using Domain.Rager.Entities;
    using Domain.Rager.Interfaces;
    using FluentNHibernate.Cfg;
    using FluentNHibernate.Cfg.Db;
    using FluentNHibernate.Conventions.Helpers;
    using Ivh.Common.Tests.Helpers.Rager;
    using NHibernate.Cfg;
    using NUnit.Framework;
    using Provider.Rager;

    [TestFixture]
    public class RagerIntegrationTests : RepositoryTestBase
    {

        public RagerIntegrationTests() : base(typeof(Visit))
        {
        }

        /// <summary>
        /// This method can be used to help develop the tables
        /// Create a new database (Rager) and create schema aging
        /// Update the connection string
        /// To generate the tables uncomment the last 2 lines in this method
        /// </summary>
        protected void BuildConfigurationRage()
        {
            Configuration configuration = Fluently.Configure()
                .Database(MsSqlConfiguration.MsSql2008.ConnectionString("Data Source=localhost;Initial Catalog=Rager;Integrated Security=True"))
                .Mappings(m =>
                {
                    m.FluentMappings.Conventions.Setup(a => a.Add(AutoImport.Never()));
                    m.FluentMappings.Conventions.Add<Ivh.Common.Data.Conventions.DateTimeConvention>();
                    m.FluentMappings.AddFromAssembly(Assembly.GetAssembly(typeof(Ivh.Domain.Rager.Entities.Visit)));
                })
                .BuildConfiguration();

            this.SessionFactory = configuration.BuildSessionFactory();
            this.Session = this.SessionFactory.OpenSession();


            //Uncomment the following if you need to generate the tables and scripts
            //SchemaExport exporter = new SchemaExport(configuration);
            //exporter.Execute(true, true, false, this.Session.Connection, null);
        }

        [Test]
        public void Communications_ToJson()
        {
            AgingTier agingTier = new AgingTier {AgingTierId = 0};
            Visit visit = new Visit { VisitSourceSystemKey = "visitSourceSystemKey", VisitBillingSystemId = 1, CurrentVisitAgingTier = agingTier, InsertDate = DateTime.UtcNow.AddDays(-30) };

            CommunicationType statementCommunicationType = new CommunicationType { CommunicationTypeId = (int)AgingCommunicationTypeEnum.StatementNotification, Description = "Statement" };
            CommunicationType pastDueCommunicationType = new CommunicationType { CommunicationTypeId = (int)AgingCommunicationTypeEnum.PastDueNotification, Description = "Past Due" };
            this.Session.SaveOrUpdate(statementCommunicationType);

            VisitCommunication visitCommunicationStatment = new VisitCommunication {
                Visit = visit,
                CommunicationType = statementCommunicationType,
                DateOfCommunication = DateTime.UtcNow.AddDays(-60),
                CommunciationIncrement = 1
            };
            VisitCommunication visitCommunicationStatment2 = new VisitCommunication
            {
                Visit = visit,
                CommunicationType = statementCommunicationType,
                DateOfCommunication = DateTime.UtcNow.AddDays(-30),
                CommunciationIncrement = 1
            };
            VisitCommunication visitCommunicationStatment3 = new VisitCommunication
            {
                Visit = visit,
                CommunicationType = statementCommunicationType,
                DateOfCommunication = DateTime.UtcNow,
                CommunciationIncrement = 1
            };
            VisitCommunication pastDueCommunication = new VisitCommunication
            {
                Visit = visit,
                CommunicationType = pastDueCommunicationType,
                DateOfCommunication = DateTime.UtcNow,
                CommunciationIncrement = 1
            };

            visit.VisitCommunications.Add(visitCommunicationStatment);
            visit.VisitCommunications.Add(visitCommunicationStatment2);
            visit.VisitCommunications.Add(visitCommunicationStatment3);
            visit.VisitCommunications.Add(pastDueCommunication);
            this.Session.SaveOrUpdate(visit);
            string communicationJson = visit.VisitCommunicationsJsonData;

            Assert.True(communicationJson.Contains("SumCommunicationIncrement\": 3"));
            Assert.True(communicationJson.Contains("SumCommunicationIncrement\": 1"));
        }

        [Test]
        // This is just a simple test to make sure everything is wired up correctly
        public void VisitRepository_Insert_Integration_Test()
        {
            AgingTier agingTier = new AgingTier { AgingTierId = 0 };
            this.Session.SaveOrUpdate(agingTier);
            
            Visit visit = new Visit { VisitSourceSystemKey = "visitSourceSystemKey", VisitBillingSystemId = 1, CurrentVisitAgingTier = agingTier, InsertDate = DateTime.UtcNow.AddDays(-30) };
            Assert.True(visit.VisitId == 0);

            VisitRepository visitRepository = new VisitRepository(new SessionContext<CdiEtl>(this.Session));
            visitRepository.InsertOrUpdate(visit);
            Assert.True(visit.VisitId > 0);
        }

        [Test]
        // This is just a simple test to make sure everything is wired up correctly
        public void VisitRepository_GetVisitBySourceKeyAndBillingId_Integration_Test()
        {
            AgingTier agingTier = new AgingTier { AgingTierId = 0 };
            this.Session.SaveOrUpdate(agingTier);

            Visit visit = new Visit { VisitSourceSystemKey = "visitSourceSystemKey", VisitBillingSystemId = 1, CurrentVisitAgingTier = agingTier, InsertDate = DateTime.UtcNow.AddDays(-30) };
            this.Session.SaveOrUpdate(visit);
            VisitRepository visitRepository = new VisitRepository(new SessionContext<CdiEtl>(this.Session));
            Visit fetchedVisit = visitRepository.GetVisitBySourceKeyAndBillingId(visit.VisitSourceSystemKey,visit.VisitBillingSystemId);
            Assert.True(visit.VisitSourceSystemKey == fetchedVisit.VisitSourceSystemKey && visit.VisitBillingSystemId == fetchedVisit.VisitBillingSystemId);
        }

        [Test]
        // This is just a simple test to make sure everything is wired up correctly
        public void VisitAgeService_GetVisitBySourceKeyAndBillingId_Integration_Test()
        {
            AgingTier agingTier = new AgingTier { AgingTierId = 0 };
            this.Session.SaveOrUpdate(agingTier);

            Visit visit = new Visit { VisitSourceSystemKey = "visitSourceSystemKey", VisitBillingSystemId = 1, CurrentVisitAgingTier = agingTier, InsertDate = DateTime.UtcNow.AddDays(-30) };
            this.Session.SaveOrUpdate(visit);
            VisitRepository visitRepository = new VisitRepository(new SessionContext<CdiEtl>(this.Session));

            VisitAgeServiceMockBuilder visitAgeServiceMockBuilder = new VisitAgeServiceMockBuilder();
            visitAgeServiceMockBuilder.VisitRepository = visitRepository;
            IVisitAgeService visitAgeService = visitAgeServiceMockBuilder.CreateService();

            Visit fetchedVisit = visitAgeService.GetVisitBySourceKeyAndBillingId(visit.VisitSourceSystemKey, visit.VisitBillingSystemId);
            Assert.True(visit.VisitSourceSystemKey == fetchedVisit.VisitSourceSystemKey && visit.VisitBillingSystemId == fetchedVisit.VisitBillingSystemId);
        }

        [Test]
        public void AgingApplicationService_ProcessChangeSetMessage_Suspended()
        {
            IAgingApplicationService agingApplicationService = this.GetAgingApplicationServiceWithBackingRepo();
            string sourceSystemId = "123";
            int billingSystemId = 2;
            ChangeSetAggregatedMessage changeSetAggregatedMessage = this.GetChangeSetAggregatedMessage(true, true, sourceSystemId, billingSystemId);

            agingApplicationService.ProcessChangeSetAggregatedMessage(changeSetAggregatedMessage);
            Visit fetchedVisit = this.Session.QueryOver<Visit>().Where(x => x.VisitSourceSystemKey == sourceSystemId && x.VisitBillingSystemId == billingSystemId).SingleOrDefault();
            Assert.True(fetchedVisit.IsSuspended);
        }

        [Test]
        public void AgingApplicationService_ProcessChangeSetMessage_Suspended_Unsuspended()
        {
            IAgingApplicationService agingApplicationService = this.GetAgingApplicationServiceWithBackingRepo();
            string sourceSystemId = "123";
            int billingSystemId = 2;
            ChangeSetAggregatedMessage changeSetAggregatedMessage = this.GetChangeSetAggregatedMessage(true, true, sourceSystemId, billingSystemId);

            agingApplicationService.ProcessChangeSetAggregatedMessage(changeSetAggregatedMessage);
            Visit fetchedVisit = this.Session.QueryOver<Visit>().Where(x => x.VisitSourceSystemKey == sourceSystemId && x.VisitBillingSystemId == billingSystemId).SingleOrDefault();
            Assert.True(fetchedVisit.IsSuspended);

            changeSetAggregatedMessage = this.GetChangeSetAggregatedMessage(false, true, sourceSystemId, billingSystemId);
            agingApplicationService.ProcessChangeSetAggregatedMessage(changeSetAggregatedMessage);
            Visit fetchedVisit2 = this.Session.QueryOver<Visit>().Where(x => x.VisitSourceSystemKey == sourceSystemId && x.VisitBillingSystemId == billingSystemId).SingleOrDefault();
            Assert.False(fetchedVisit2.IsSuspended);
        }

        [Test]
        public void AgingApplicationService_ProcessChangeSetMessage_Recalled_Unrecalled()
        {
            IAgingApplicationService agingApplicationService = this.GetAgingApplicationServiceWithBackingRepo();
            string sourceSystemId = "123";
            int billingSystemId = 2;
            ChangeSetAggregatedMessage changeSetAggregatedMessage = this.GetChangeSetAggregatedMessage(false, false, sourceSystemId, billingSystemId);

            agingApplicationService.ProcessChangeSetAggregatedMessage(changeSetAggregatedMessage);
            Visit fetchedVisit = this.Session.QueryOver<Visit>().Where(x => x.VisitSourceSystemKey == sourceSystemId && x.VisitBillingSystemId == billingSystemId).SingleOrDefault();
            Assert.True(fetchedVisit.IsRecalled);

            changeSetAggregatedMessage = this.GetChangeSetAggregatedMessage(false, true, sourceSystemId, billingSystemId);
            agingApplicationService.ProcessChangeSetAggregatedMessage(changeSetAggregatedMessage);
            Visit fetchedVisit2 = this.Session.QueryOver<Visit>().Where(x => x.VisitSourceSystemKey == sourceSystemId && x.VisitBillingSystemId == billingSystemId).SingleOrDefault();
            Assert.False(fetchedVisit2.IsRecalled);
        }

        [Test]
        public void AgingApplicationService_Processing_Multiple_Changesets_On_Same_Visit_Only_Updates()
        {
            //we want to make sure that 2 or more change sets on the same visit does not add  2 or visit records
            IAgingApplicationService agingApplicationService = this.GetAgingApplicationServiceWithBackingRepo();
            string sourceSystemId = "123";
            int billingSystemId = 2;
            ChangeSetAggregatedMessage changeSetAggregatedMessage = this.GetChangeSetAggregatedMessage(false, false, sourceSystemId, billingSystemId);

            agingApplicationService.ProcessChangeSetAggregatedMessage(changeSetAggregatedMessage);
            changeSetAggregatedMessage = this.GetChangeSetAggregatedMessage(false, true, sourceSystemId, billingSystemId);
            agingApplicationService.ProcessChangeSetAggregatedMessage(changeSetAggregatedMessage);

            Assert.AreEqual(1, this.Session.QueryOver<Visit>().RowCount());
        }

        [Test]
        public void AgingApplicationService_IsOnFinancePlan_Add()
        {
            IAgingApplicationService agingApplicationService = this.GetAgingApplicationServiceWithBackingRepo();
            string sourceSystemId = "123";
            int billingSystemId = 2;
            ChangeSetAggregatedMessage changeSetAggregatedMessage = this.GetChangeSetAggregatedMessage(false, true, sourceSystemId, billingSystemId);
            agingApplicationService.ProcessChangeSetAggregatedMessage(changeSetAggregatedMessage);


            OutboundVisitMessageList outboundVisitMessageList = this.GetOutboundVisitMessageList(true, sourceSystemId, billingSystemId);
            agingApplicationService.ProcessOutboundVisitMessageList(outboundVisitMessageList);
            Visit fetchedVisit = this.Session.QueryOver<Visit>().Where(x => x.VisitSourceSystemKey == sourceSystemId && x.VisitBillingSystemId == billingSystemId).SingleOrDefault();
            Assert.True(fetchedVisit.IsOnFinancePlan);
        }

        [Test]
        public void AgingApplicationService_IsOnFinancePlan_Remove()
        {
            IAgingApplicationService agingApplicationService = this.GetAgingApplicationServiceWithBackingRepo();
            string sourceSystemId = "123";
            int billingSystemId = 2;
            ChangeSetAggregatedMessage changeSetAggregatedMessage = this.GetChangeSetAggregatedMessage(false, true, sourceSystemId, billingSystemId);
            agingApplicationService.ProcessChangeSetAggregatedMessage(changeSetAggregatedMessage);

            //put it on FP
            OutboundVisitMessageList outboundVisitMessageList = this.GetOutboundVisitMessageList(true, sourceSystemId, billingSystemId);
            agingApplicationService.ProcessOutboundVisitMessageList(outboundVisitMessageList);
            Visit fetchedVisit = this.Session.QueryOver<Visit>().Where(x => x.VisitSourceSystemKey == sourceSystemId && x.VisitBillingSystemId == billingSystemId).SingleOrDefault();
            Assert.True(fetchedVisit.IsOnFinancePlan);

            //remove it from FP
            outboundVisitMessageList = this.GetOutboundVisitMessageList(false, sourceSystemId, billingSystemId);
            agingApplicationService.ProcessOutboundVisitMessageList(outboundVisitMessageList);
            Visit fetchedVisit2 = this.Session.QueryOver<Visit>().Where(x => x.VisitSourceSystemKey == sourceSystemId && x.VisitBillingSystemId == billingSystemId).SingleOrDefault();
            Assert.False(fetchedVisit2.IsOnFinancePlan);
        }

        [Test]
        public void AgingApplicationService_IsOnFinancePlan_Ingnores_When_Not_a_FP_Concern()
        {
            IAgingApplicationService agingApplicationService = this.GetAgingApplicationServiceWithBackingRepo();
            string sourceSystemId = "123";
            int billingSystemId = 2;
            ChangeSetAggregatedMessage changeSetAggregatedMessage = this.GetChangeSetAggregatedMessage(false, true, sourceSystemId, billingSystemId);
            agingApplicationService.ProcessChangeSetAggregatedMessage(changeSetAggregatedMessage);

            OutboundVisitMessageList outboundVisitMessageList = this.GetOutboundVisitMessageList(true, sourceSystemId, billingSystemId);
            outboundVisitMessageList.Messages.First().VpOutboundVisitMessageType = VpOutboundVisitMessageTypeEnum.VpVisitUnmatch;
            agingApplicationService.ProcessOutboundVisitMessageList(outboundVisitMessageList);
            Visit fetchedVisit = this.Session.QueryOver<Visit>().Where(x => x.VisitSourceSystemKey == sourceSystemId && x.VisitBillingSystemId == billingSystemId).SingleOrDefault();
            Assert.False(fetchedVisit.IsOnFinancePlan);

            Assert.AreEqual(0, fetchedVisit.VisitFinancePlans.Count);
        }

        [Test]
        // This is simple test to smoke test the workflow for aging a visit
        public void AgingApplicationService_AgeVisit_Integration_Test()
        {
            this.AddAgingTiers();
            this.AddConfigurationTiers();

            CommunicationType communicationType = new CommunicationType { CommunicationTypeId = (int)AgingCommunicationTypeEnum.StatementNotification, Description = "statement" };
            this.Session.Save(communicationType);

            string visitSourceSystemKey = "123";
            int billingSystemId = 1;

            Visit visit = new Visit
            {
                VisitSourceSystemKey = visitSourceSystemKey,
                VisitBillingSystemId = billingSystemId,
                VisitCommunications = new List<VisitCommunication>
                {
                    new VisitCommunication
                    {
                        CommunciationIncrement = 1,
                        CommunicationType = communicationType,
                        DateOfCommunication = DateTime.UtcNow.AddDays(-10)
                    }
                }
            };
            VisitRepository visitRepository = new VisitRepository(new SessionContext<CdiEtl>(this.Session));
            visitRepository.InsertOrUpdate(visit);

            Assert.IsNull(visit.CurrentVisitAgingTier);

            IAgingApplicationService agingApplicationService = this.GetAgingApplicationServiceWithBackingRepo();
            AgeVisitMessage ageVisitMessage = new AgeVisitMessage { VisitId = visit.VisitId, DateToProcess = DateTime.UtcNow };
            agingApplicationService.ProcessAgeVisitMessage(ageVisitMessage);
            Visit fetchedVisit = this.Session.QueryOver<Visit>().Where(x => x.VisitSourceSystemKey == visitSourceSystemKey && x.VisitBillingSystemId == billingSystemId).SingleOrDefault();

            //statement notification, but no VistAges
            //Business decision not to allow NotStatemented when the user has had communications VP-1408
            Assert.AreEqual(AgingTierEnum.Good, fetchedVisit.CurrentVisitAgingTier.AgingTierEnum);
            //TODO: check to see if message is consumed?
        }

        public static IEnumerable SetVisitAgeTestCases
        {
            get
            {
                foreach (AgingTierEnum initialAgingTierEnum in Enum.GetValues(typeof(AgingTierEnum)))
                {
                    //Business decision not to allow NotStatemented when the user has had communications VP-1408
                    foreach (AgingTierEnum targetAgingTierEnum in Enum.GetValues(typeof(AgingTierEnum)).Cast<AgingTierEnum>().Where(x => x != AgingTierEnum.NotStatemented ))
                    {
                        if (initialAgingTierEnum == targetAgingTierEnum)
                        {
                            continue;
                        }
                        yield return new TestCaseData(
                            initialAgingTierEnum, 
                            targetAgingTierEnum, 
                            targetAgingTierEnum > initialAgingTierEnum ? initialAgingTierEnum : targetAgingTierEnum); //should only be able to rage backwards
                    }
                }
                    
            }
        }

        [TestCaseSource(nameof(SetVisitAgeTestCases))]
        [Test]
        // This is simple test to smoke test the workflow for aging a visit
        public void AgingApplicationService_SetVisitAge_Integration_Test(AgingTierEnum initialAgingTierEnum, AgingTierEnum targetAgingTierEnum, AgingTierEnum expectedAgingTierEnum)
        {
            //
            this.AddAgingTiers();
            this.AddConfigurationTiers();

            foreach (AgingCommunicationTypeEnum type in Enum.GetValues(typeof(AgingCommunicationTypeEnum)))
            {
                this.Session.Save(new CommunicationType
                {
                    CommunicationTypeId = (int)type,
                    Description = type.ToString()
                });
            }

            AgingTierConfiguration targetConfig = AgingTierConfigurationFactory.AgingTierConfigurations().Single(x => x.ResultingAgingTier.AgingTierEnum == initialAgingTierEnum);


            string visitSourceSystemKey = "123";
            int billingSystemId = 1;

            Visit visit = new Visit
            {
                VisitSourceSystemKey = visitSourceSystemKey,
                VisitBillingSystemId = billingSystemId
            };

            //add communications
            foreach (KeyValuePair<AgingCommunicationTypeEnum, int?> minimumCommunicationTypeCount in targetConfig
                .MinimumCommunicationConfigurationDeserialized
                .MinimumCommunicationTypeCounts
                .Where(x => x.Value.HasValue))
            {
                visit.AddVisitCommunications(DateTime.UtcNow.AddDays(-10), 
                    new CommunicationType { CommunicationTypeId = (int)minimumCommunicationTypeCount.Key},
                    minimumCommunicationTypeCount.Value ?? 0);
            }

            //set visit age
            VisitAge visitAge = new VisitAge
            {
                Visit = visit,
                Age = targetConfig.MinAge,
                SnapshotAgeSum = targetConfig.MinAge,
                SnapshotCommunications = visit.VisitCommunicationsJsonData,
                AgingTier = new AgingTier{AgingTierId = (int)initialAgingTierEnum }
            };
            visit.VisitAges.Add(visitAge);

            VisitRepository visitRepository = new VisitRepository(new SessionContext<CdiEtl>(this.Session));
            visitRepository.InsertOrUpdate(visit);

            IAgingApplicationService agingApplicationService = this.GetAgingApplicationServiceWithBackingRepo();
            SetVisitAgeMessage message = new SetVisitAgeMessage {
                Visit = new VisitDto
                {
                    VisitBillingSystemId = visit.VisitBillingSystemId,
                    VisitSourceSystemKey = visit.VisitSourceSystemKey
                },
                ActionVisitPayUserId = 1,
                AgingTierEnum = targetAgingTierEnum
            };

            //
            agingApplicationService.ProcessSetVisitAgeMessage(message);
            Visit fetchedVisit = this.Session.QueryOver<Visit>().Where(x => x.VisitSourceSystemKey == visitSourceSystemKey && x.VisitBillingSystemId == billingSystemId).SingleOrDefault();

            //
            Assert.AreEqual(expectedAgingTierEnum, fetchedVisit.CurrentVisitAgingTier.AgingTierEnum);
        }


        // This test covers if IsEligibleToAge updated while processing SetVisitAgeMessage
        [TestCase(true, true)]
        [TestCase(false, false)]
        public void Should_Set_Visit_Eligible_for_Aging(bool setToActive, bool isEligibleAfterProcessingMessage)
        {
            this.AddAgingTiers();
            this.AddConfigurationTiers();

            string visitSourceSystemKey = "123";
            int billingSystemId = 1;

            Visit visit = new Visit
            {
                VisitSourceSystemKey = visitSourceSystemKey,
                VisitBillingSystemId = billingSystemId,
                CurrentVisitAgingTier = new AgingTier { AgingTierId = (int)AgingTierEnum.MaxAge }
            };

            VisitAge visitAge = new VisitAge
            {
                Visit = visit,
                Age = 200,
                SnapshotAgeSum = 200,
                AgingTier = new AgingTier { AgingTierId = (int)AgingTierEnum.MaxAge }
            };
            visit.VisitAges.Add(visitAge);

            // Make the visit ineligible 
            visit.AddVisitState(true);
            visit.AddVisitRecall(true);

            VisitRepository visitRepository = new VisitRepository(new SessionContext<CdiEtl>(this.Session));
            visitRepository.InsertOrUpdate(visit);

            // Do an initial assert to make sure the visit is in the correct setup state
            Assert.IsFalse(visit.IsEligibleToAge, "The visit should not be eligible to age (setup condition error)");

            IAgingApplicationService agingApplicationService = this.GetAgingApplicationServiceWithBackingRepo();
            SetVisitAgeMessage message = new SetVisitAgeMessage
            {
                Visit = new VisitDto
                {
                    VisitBillingSystemId = visit.VisitBillingSystemId,
                    VisitSourceSystemKey = visit.VisitSourceSystemKey
                },
                ActionVisitPayUserId = 1,
                AgingTierEnum = AgingTierEnum.NotStatemented
            };

            message.SetToActive = setToActive;

            agingApplicationService.ProcessSetVisitAgeMessage(message);
            Visit fetchedVisit = this.Session.QueryOver<Visit>().Where(x => x.VisitSourceSystemKey == visitSourceSystemKey && x.VisitBillingSystemId == billingSystemId).SingleOrDefault();
            Assert.AreEqual(isEligibleAfterProcessingMessage,fetchedVisit.IsEligibleToAge);

        }

        [TestCase(true)]
        [TestCase(false)]
        public void AgeEligibility_GuarantorIsCallCenter(bool isCallCenter)
        {
            this.AddAgingTiers();
            this.AddConfigurationTiers();
            string visitSourceSystemKey = "123";
            int billingSystemId = 1;

            Visit visit = new Visit
            {
                VisitSourceSystemKey = visitSourceSystemKey,
                VisitBillingSystemId = billingSystemId,
                CurrentVisitAgingTier = new AgingTier { AgingTierId = (int)AgingTierEnum.NotStatemented },
                VpGuarantorId = 1
            };

            // Make the visit ineligible 
            visit.AgingGuarantorTypes.Add(new AgingGuarantorType
            {
                IsCallCenter = isCallCenter,
                VpGuarantorId = 1
            });

            VisitRepository visitRepository = new VisitRepository(new SessionContext<CdiEtl>(this.Session));
            visitRepository.InsertOrUpdate(visit);
            this.Session.SaveOrUpdate(visit.AgingGuarantorTypes.First());

            //
            IList<int> eligibleToAgeVisitIds = visitRepository.GetEligibleToAgeVisitIds(DateTime.UtcNow);

            //
            Assert.AreEqual(isCallCenter, eligibleToAgeVisitIds.IsNullOrEmpty(), $"repository should {(isCallCenter ? "not " : "")}find visit eligible to age");

            Assert.AreEqual(isCallCenter, !visit.IsEligibleToAge, $"entity should {(isCallCenter ? "not " : "")}be eligible to age");
        }

        [TestCase(true)]
        [TestCase(false)]
        public void AgeEligibility_GuarantorIsInactive(bool isInactive)
        {
            this.AddAgingTiers();
            this.AddConfigurationTiers();
            string visitSourceSystemKey = "123";
            int billingSystemId = 1;

            Visit visit = new Visit
            {
                VisitSourceSystemKey = visitSourceSystemKey,
                VisitBillingSystemId = billingSystemId,
                CurrentVisitAgingTier = new AgingTier { AgingTierId = (int)AgingTierEnum.NotStatemented },
                VpGuarantorId = 1
            };

            // Make the visit ineligible 
            visit.AgingGuarantorStates.Add(new AgingGuarantorState
            {
                IsInactive = isInactive,
                VpGuarantorId = 1
            });

            VisitRepository visitRepository = new VisitRepository(new SessionContext<CdiEtl>(this.Session));
            visitRepository.InsertOrUpdate(visit);
            this.Session.SaveOrUpdate(visit.AgingGuarantorStates.First());

            //
            IList<int> eligibleToAgeVisitIds = visitRepository.GetEligibleToAgeVisitIds(DateTime.UtcNow);

            //
            Assert.AreEqual(isInactive, eligibleToAgeVisitIds.IsNullOrEmpty(), $"repository should {(isInactive ? "not " : "")}find visit eligible to age");

            Assert.AreEqual(isInactive, !visit.IsEligibleToAge, $"entity should {(isInactive ? "not " : "")}be eligible to age");
        }


        #region helpers

        private IAgingApplicationService GetAgingApplicationServiceWithBackingRepo()
        {
            AgingApplicationServiceMockBuilder agingApplicationServiceMockBuilder = new AgingApplicationServiceMockBuilder();
            VisitAgeServiceMockBuilder visitAgeServiceMockBuilder = new VisitAgeServiceMockBuilder();
            VisitRepository visitRepository = new VisitRepository(new SessionContext<CdiEtl>(this.Session));
            AgingTierConfigurationRepository agingTierConfigurationRepository = new AgingTierConfigurationRepository(new SessionContext<CdiEtl>(this.Session));
            visitAgeServiceMockBuilder.VisitRepository = visitRepository;
            visitAgeServiceMockBuilder.AgingTierConfigurationRepository = agingTierConfigurationRepository;
            IVisitAgeService visitAgeService = visitAgeServiceMockBuilder.CreateService();
            agingApplicationServiceMockBuilder.VisitAgeService = visitAgeService;
            agingApplicationServiceMockBuilder.Session = this.Session;
            IAgingApplicationService agingApplicationService = agingApplicationServiceMockBuilder.CreateService();
            return agingApplicationService;
        }

        private ChangeSetAggregatedMessage GetChangeSetAggregatedMessage(bool isSuspended, bool isVpEligible, string sourceSystemId, int billingSystemId)
        {
            VisitChangeDto visitChangeDto = new VisitChangeDto {
                BillingSystemId = billingSystemId,
                SourceSystemKey = sourceSystemId,
                BillingHold = isSuspended,
                VpEligible = isVpEligible
            };

            ChangeSetAggregatedMessage changeSetAggregatedMessage = new ChangeSetAggregatedMessage
            {
                Messages = new List<ChangeSetMessage>
                {
                    new ChangeSetMessage
                    {
                        VisitChanges = new List<VisitChangeDto> {visitChangeDto}
                    }
                }
            };

            return changeSetAggregatedMessage;
        }

        private OutboundVisitMessageList GetOutboundVisitMessageList(bool isOnFinancePlan, string sourceSystemId, int billingSystemId)
        {
            OutboundVisitMessageList outboundVisitMessageList = new OutboundVisitMessageList
            {
                Messages = new List<OutboundVisitMessage>
                {
                    new OutboundVisitMessage
                    {
                        BillingSystemId = billingSystemId,
                        SourceSystemKey = sourceSystemId,
                        VpOutboundVisitMessageType = isOnFinancePlan ? 
                            VpOutboundVisitMessageTypeEnum.VisitAddedToFinancePlan : 
                            VpOutboundVisitMessageTypeEnum.VisitRemovedFromFinancePlan
                    }
                }
            };
           

            return outboundVisitMessageList;
        }

        private void AddConfigurationTiers()
        {
            // need to call this.AddAgingTiers() first;
            foreach (AgingTierConfiguration agingConfigurationTier in AgingTierConfigurationFactory.AgingTierConfigurations().OrderBy(x => (int) x.ResultingAgingTier.AgingTierEnum))
            {
                this.Session.Save(agingConfigurationTier);
            }

        }

        private void AddAgingTiers()
        {
            foreach (KeyValuePair<int, AgingTier> tier in AgingTierConfigurationFactory.AgingTiers.OrderBy(x => x.Key))
            {
                this.Session.SaveOrUpdate(tier.Value);
            }
        }

        #endregion helpers
    }
}

﻿namespace Ivh.Application.Tests.FinanceManagement
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Application.Base.Common.Dtos;
    using Application.FinanceManagement.ApplicationServices;
    using Application.FinanceManagement.Common.Interfaces;
    using Autofac;
    using Common.Base.Utilities.Extensions;
    using Common.DependencyInjection;
    using Common.DependencyInjection.Registration;
    using Common.DependencyInjection.Registration.Register;
    using Common.State.Interfaces;
    using Common.Tests.Helpers.FinancePlan;
    using Common.Tests.Helpers.Visit;
    using Common.Tests.Helpers.VpStatement;
    using Common.VisitPay.Enums;
    using Common.VisitPay.Messages.FinanceManagement;
    using Domain.Guarantor.Entities;
    using Domain.Visit.Entities;
    using Domain.FinanceManagement.FinancePlan.Entities;
    using Domain.FinanceManagement.Payment.Entities;
    using Domain.FinanceManagement.Statement.Entities;
    using Domain.Settings.Entities;
    using Domain.Settings.Interfaces;
    using FakeItEasy;
    using Moq;
    using NUnit.Framework;

    [TestFixture]
    public class FinancialDataSummaryApplicationServiceTests : ApplicationTestBase
    {
        [SetUp]
        public void Setup()
        {
            Mock<Client> clientMock = new Mock<Client>();
            clientMock.Setup(x => x.FriendlyCommunicationDeliveryRange).Returns("08:00-20:00");

            new FinancialDataSummaryApplicationServiceMockBuilder().Setup(builder =>
            {
                builder.ApplicationServiceCommonServiceMockBuilder.ClientServiceMock.Setup(x => x.GetClient()).Returns(clientMock.Object);

                SetUpApplicationRegistrations(reg =>
                {
                    reg.Register(x => builder.ApplicationServiceCommonServiceMockBuilder.ClientServiceMock.Object).As<IClientService>();
                });
            });

        }
        
        #region send balance due reminder

        [Test]
        public void BalanceDueReminder_WithScvb_Sends()
        {
            IList<Tuple<Visit, IList<VisitTransaction>>> visits = VisitFactory.GenerateActiveVisit(1234.56m, 2, VisitStateEnum.Active).ToListOfOne();

            IFinancialDataSummaryApplicationService svc = CreateTest(DateTime.UtcNow, visits);

            IList<BalanceDueReminderMessage> messages = ((FinancialDataSummaryApplicationService)svc).GetBalanceDueReminderMessages(DateTime.UtcNow).ToList();

            Assert.AreEqual(1, messages.Count);

            foreach (BalanceDueReminderMessage message in messages)
            {
                bool processed = svc.ProcessBalanceDueReminder(message);
                Assert.AreEqual(true, processed);
            }
        }

        [Test]
        public void BalanceDueReminder_WithPendingAcceptanceFinancePlan_Sends()
        {
            IList<Tuple<Visit, IList<VisitTransaction>>> visits = VisitFactory.GenerateActiveVisit(1234.56m, 2, VisitStateEnum.Active).ToListOfOne();

            FinancePlan financePlan = CreateFinancePlan(visits.Take(1).ToList(), FinancePlanStatusEnum.PendingAcceptance);

            foreach (FinancePlanVisit fpv in financePlan.FinancePlanVisits)
            {
                VisitStateEnum changeState = VisitStateEnum.Active;

                ((IEntity<VisitStateEnum>)visits[0].Item1).SetState(changeState, "");
                fpv.CurrentVisitState = changeState;
            }

            IFinancialDataSummaryApplicationService svc = CreateTest(DateTime.UtcNow, visits, null, financePlan.ToListOfOne());

            IList<BalanceDueReminderMessage> messages = ((FinancialDataSummaryApplicationService)svc).GetBalanceDueReminderMessages(DateTime.UtcNow).ToList();

            Assert.AreEqual(1, messages.Count);

            foreach (BalanceDueReminderMessage message in messages)
            {
                bool processed = svc.ProcessBalanceDueReminder(message);
                Assert.AreEqual(true, processed);
            }
        }

        [Test] // https://ivinci.atlassian.net/browse/VPNG-18652
        public void BalanceDueReminder_WithGoodStandingFinancePlan_And_VisitWithScvb_Sends()
        {
            IList<Tuple<Visit, IList<VisitTransaction>>> visits = new List<Tuple<Visit, IList<VisitTransaction>>>
            {
                VisitFactory.GenerateActiveVisit(1234.56m, 2),
                VisitFactory.GenerateActiveVisit(6543.21m, 2)
            };

            FinancePlan financePlan = CreateFinancePlan(visits.Take(1).ToList(), FinancePlanStatusEnum.GoodStanding);

            IFinancialDataSummaryApplicationService svc = CreateTest(DateTime.UtcNow, visits, null, financePlan.ToListOfOne());

            IList<BalanceDueReminderMessage> messages = ((FinancialDataSummaryApplicationService)svc).GetBalanceDueReminderMessages(DateTime.UtcNow).ToList();

            Assert.AreEqual(1, messages.Count);

            foreach (BalanceDueReminderMessage message in messages)
            {
                bool processed = svc.ProcessBalanceDueReminder(message);
                Assert.AreEqual(true, processed);
            }
        }

        [Test]
        public void BalanceDueReminder_WithGoodStandingFinancePlan_And_PendingAcceptanceFinancePlan_Sends()
        {
            IList<Tuple<Visit, IList<VisitTransaction>>> visits = new List<Tuple<Visit, IList<VisitTransaction>>>
            {
                VisitFactory.GenerateActiveVisit(1234.56m, 2),
                VisitFactory.GenerateActiveVisit(6543.21m, 2)
            };

            FinancePlan financePlan1 = CreateFinancePlan(visits.Take(1).ToList(), FinancePlanStatusEnum.GoodStanding);
            FinancePlan financePlan2 = CreateFinancePlan(visits.Skip(1).Take(1).ToList(), FinancePlanStatusEnum.PendingAcceptance);

            foreach (FinancePlanVisit fpv in financePlan2.FinancePlanVisits)
            {
                VisitStateEnum changeState = VisitStateEnum.Active;

                ((IEntity<VisitStateEnum>)visits[0].Item1).SetState(changeState, "");
                fpv.CurrentVisitState = changeState;
            }

            IFinancialDataSummaryApplicationService svc = CreateTest(DateTime.UtcNow, visits, null, new List<FinancePlan>
            {
                financePlan1,
                financePlan2
            });

            IList<BalanceDueReminderMessage> messages = ((FinancialDataSummaryApplicationService)svc).GetBalanceDueReminderMessages(DateTime.UtcNow).ToList();

            Assert.AreEqual(1, messages.Count);

            foreach (BalanceDueReminderMessage message in messages)
            {
                bool processed = svc.ProcessBalanceDueReminder(message);
                Assert.AreEqual(true, processed);
            }
        }

        #endregion

        #region does not send balance due reminder

        [Test]
        public void BalanceDueReminder_WithImmediateStatement_DoesSend()
        {
            IList<Tuple<Visit, IList<VisitTransaction>>> visits = VisitFactory.GenerateActiveVisit(1234.56m, 2).ToListOfOne();

            IFinancialDataSummaryApplicationService svc = CreateTest(DateTime.UtcNow, visits, PaymentTypeEnum.ManualScheduledCurrentNonFinancedBalanceInFull);

            IList<BalanceDueReminderMessage> messages = ((FinancialDataSummaryApplicationService)svc).GetBalanceDueReminderMessages(DateTime.UtcNow).ToList();

            Assert.AreEqual(1, messages.Count);
        }

        [Test]
        public void BalanceDueReminder_WithFullPaymentScheduled_DoesNotSend()
        {
            IList<Tuple<Visit, IList<VisitTransaction>>> visits = VisitFactory.GenerateActiveVisit(1234.56m, 2).ToListOfOne();

            IFinancialDataSummaryApplicationService svc = CreateTest(DateTime.UtcNow, visits, PaymentTypeEnum.ManualScheduledCurrentNonFinancedBalanceInFull);

            IList<BalanceDueReminderMessage> messages = ((FinancialDataSummaryApplicationService)svc).GetBalanceDueReminderMessages(DateTime.UtcNow).ToList();

            Assert.AreEqual(1, messages.Count);

            foreach (BalanceDueReminderMessage message in messages)
            {
                bool processed = svc.ProcessBalanceDueReminder(message);
                Assert.AreEqual(false, processed);
            }
        }

        [Test]
        public void BalanceDueReminder_WithGoodStandingFinancePlan_DoesNotSend()
        {
            IList<Tuple<Visit, IList<VisitTransaction>>> visits = VisitFactory.GenerateActiveVisit(1234.56m, 2).ToListOfOne();

            FinancePlan financePlan = CreateFinancePlan(visits.Take(1).ToList(), FinancePlanStatusEnum.GoodStanding);

            IFinancialDataSummaryApplicationService svc = CreateTest(DateTime.UtcNow, visits, null, financePlan.ToListOfOne());

            IList<BalanceDueReminderMessage> messages = ((FinancialDataSummaryApplicationService)svc).GetBalanceDueReminderMessages(DateTime.UtcNow).ToList();

            Assert.AreEqual(0, messages.Count);
        }

        #endregion

        private static IFinancialDataSummaryApplicationService CreateTest(DateTime dateToCheck, IList<Tuple<Visit, IList<VisitTransaction>>> visits, PaymentTypeEnum? scheduledPaymentType = null, IList<FinancePlan> financePlans = null)
        {
            Mock<Client> clientMock = new Mock<Client>();
            clientMock.Setup(x => x.BalanceDueNotificationInDays).Returns(2);

            VpStatement statement = VpStatementFactory.CreateStatement(visits.ToList());
            statement.PaymentDueDate = dateToCheck.AddDays(2).Date;
            statement.VpStatementId = 1;
            statement.VpGuarantorId = 1;

            List<int> financedVisitIds = (financePlans ?? new List<FinancePlan>())
                .Where(x => x.FinancePlanStatus.FinancePlanStatusEnum.IsInCategory(FinancePlanStatusEnumCategory.Active))
                .Where(x => x.FinancePlanStatus.FinancePlanStatusEnum != FinancePlanStatusEnum.PendingAcceptance)
                .SelectMany(x => x.FinancePlanVisits.Select(fpv => fpv.VisitId))
                .ToList();

            IFinancialDataSummaryApplicationService svc = new FinancialDataSummaryApplicationServiceMockBuilder().Setup(builder =>
            {
                builder.ApplicationServiceCommonServiceMockBuilder.ClientServiceMock.Setup(x => x.GetClient()).Returns(clientMock.Object);
                builder.FinancePlanServiceMock.Setup(x => x.GetVisitIdsOnActiveFinancePlans(It.IsAny<int>(), null)).Returns(() =>
                {
                    IList<FinancePlanStatusEnum> checkStatuses = new List<FinancePlanStatusEnum>
                    {
                        FinancePlanStatusEnum.GoodStanding,
                        FinancePlanStatusEnum.PastDue,
                        FinancePlanStatusEnum.UncollectableActive
                    };

                    return (financePlans ?? new List<FinancePlan>())
                        .Where(x => checkStatuses.Contains(x.CurrentFinancePlanStatus.FinancePlanStatusEnum))
                        .SelectMany(x => x.FinancePlanVisits)
                        .Select(x => x.VisitId)
                        .ToList();
                });
                builder.PaymentServiceMock.Setup(x => x.GetScheduledPayments(It.IsAny<int>())).Returns(() =>
                {
                    if (scheduledPaymentType == null)
                    {
                        return new List<Payment>();
                    }

                    Mock<Payment> payment = new Mock<Payment>();
                    payment.Setup(x => x.PaymentType).Returns(scheduledPaymentType.Value);
                    payment.Setup(x => x.ScheduledPaymentAmount).Returns(visits.Sum(x => x.Item1.CurrentBalance));

                    return payment.Object.ToListOfOne();
                });
                builder.PaymentSubmissionApplicationServiceMock.Setup(x => x.GetDiscountPerVisitForStatementedBalanceInFull(It.IsAny<int>())).Returns(() => null);
                builder.VisitBalanceBaseApplicationServiceMock.Setup(x => x.GetTotalBalance(It.IsAny<int>())).Returns(() =>
                {
                    return visits.Where(x => !financedVisitIds.Contains(x.Item1.VisitId)).Sum(x => x.Item1.CurrentBalance);
                });
                builder.VisitBalanceBaseApplicationServiceMock.Setup(x => x.GetTotalBalances(It.IsAny<int>())).Returns(() =>
                {
                    return visits.Where(x => !financedVisitIds.Contains(x.Item1.VisitId)).Select(x => new VisitBalanceBaseDto { CurrentBalance = x.Item1.CurrentBalance }).ToList();
                });
                builder.VisitBalanceBaseApplicationServiceMock.Setup(x => x.GetTotalBalances(It.IsAny<int>(), It.IsAny<IList<int>>())).Returns((int guarantorId, IList<int> visitIds) =>
                {
                    if (visitIds == null)
                    {
                        return visits.Select(x => new VisitBalanceBaseDto { CurrentBalance = x.Item1.CurrentBalance }).ToList();
                    }

                    return visits.Where(x => visitIds.Contains(x.Item1.VisitId)).Select(x => new VisitBalanceBaseDto { CurrentBalance = x.Item1.CurrentBalance }).ToList();
                });
                builder.VisitServiceMock.Setup(x => x.GetVisits(It.IsAny<int>())).Returns(visits.Select(x => x.Item1).ToList);
                builder.VpStatementServiceMock.Setup(x => x.IsAwaitingStatement(It.IsAny<VpStatement>())).Returns(false);
                builder.VpStatementServiceMock.Setup(x => x.GetStatement(It.IsAny<int>())).Returns(statement);
                builder.VpStatementServiceMock.Setup(x => x.GetMostRecentStatement(It.IsAny<Guarantor>())).Returns(statement);
                builder.VpStatementServiceMock.Setup(x => x.GetStatementsWithPaymentDueDateResults(It.IsAny<DateTime>())).Returns(() => new List<VpStatementPaymentDueDateResult>
                {
                    new VpStatementPaymentDueDateResult
                    {
                        PaymentDueDate = DateTime.UtcNow.Date,
                        VpGuarantorId = 1,
                        VpStatementId = 1
                    }
                });
                builder.GuarantorServiceMock.Setup(x => x.GetGuarantor(It.IsAny<int>())).Returns(new Guarantor
                {
                    VpGuarantorId = 1,
                    NextStatementDate = dateToCheck.AddMonths(1)
                });

                IList<FinancePlanVisitResult> financedVisits = financePlans?.Where(x => x.IsActiveOriginated() || x.IsActiveUncollectable()).SelectMany(x => x.FinancePlanVisits).Select(x =>
               new FinancePlanVisitResult
               {
                   VisitId = x.VisitId
               }).ToList() ?? new List<FinancePlanVisitResult>();

                builder.FinancePlanServiceMock.Setup(x => x.AreVisitsOnFinancePlan_FinancePlanVisitResult(
                        It.IsAny<IList<int>>(),
                        It.IsAny<DateTime?>(),
                        It.IsAny<List<FinancePlanStatusEnum>>()))
                    .Returns(financedVisits);

            }).CreateService();

            return svc;
        }

        private static void SetUpApplicationRegistrations(Action<ContainerBuilder> registrationAction)
        {
            IvinciContainer.Instance.RegisterComponent(new TestDoubleRegistrationModule(registrationAction));
        }

        private static FinancePlan CreateFinancePlan(IList<Tuple<Visit, IList<VisitTransaction>>> visits, FinancePlanStatusEnum financePlanStatus)
        {
            FinancePlan financePlan = FinancePlanFactory.CreateFinancePlan(visits, 1, 100m);
            // At this point the finance plan has Good Standing 
            // Because the financePlanStatus is getting added so fast the InsertDates cannot be distinguished
            // and the sort used to return the current status breaks down
            // adding a slight delay to allow the sort to work properly
            Task.Run(async () =>
            {
                await Task.Delay(50);
                financePlan.FinancePlanStatus = new FinancePlanStatus
                {
                    FinancePlanStatusId = (int)financePlanStatus
                };
            }).Wait();

            return financePlan;
        }
    }

    internal class TestDoubleRegistrationModule : IRegistrationModule
    {
        private readonly Action<ContainerBuilder> _registrationAction;

        public TestDoubleRegistrationModule(Action<ContainerBuilder> registrationAction)
        {
            this._registrationAction = registrationAction;
        }

        public void Register(ContainerBuilder builder)
        {
            //IApplicationSettingsService applicationSettingsService = ApplicationSettingsService.GetApplicationSettingsService();
            VisitPay.Register(builder, A.Fake<RegistrationSettings>(), true);
            this._registrationAction(builder);
        }
    }
}
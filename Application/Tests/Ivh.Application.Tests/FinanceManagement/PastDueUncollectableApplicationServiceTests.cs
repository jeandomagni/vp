﻿namespace Ivh.Application.Tests.FinanceManagement
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Application.Core.Common.Dtos;
    using Application.FinanceManagement.Common.Dtos;
    using Application.FinanceManagement.Common.Interfaces;
    using Common.Base.Utilities.Extensions;
    using Common.Tests.Helpers.Client;
    using Common.Tests.Helpers.FinancePlan;
    using Common.Tests.Helpers.Visit;
    using Common.Tests.Helpers.VpStatement;
    using Domain.Guarantor.Entities;
    using Domain.FinanceManagement.FinancePlan.Entities;
    using Domain.FinanceManagement.FinancePlan.Interfaces;
    using Domain.FinanceManagement.Statement.Entities;
    using Domain.FinanceManagement.Statement.Interfaces;
    using Moq;
    using NUnit.Framework;

    [TestFixture]
    public class PastDueUncollectableApplicationServiceTests : ApplicationTestBase
    {
        // expect notification (visit w/aging count 4)
        [TestCase(null, 4, true)]
        [TestCase(0, 4, true)]
        [TestCase(1, 4, true)]
        [TestCase(2, 4, true)]
        [TestCase(4, 4, true)]

        // expect notification (finance plan in bucket 3)
        [TestCase(3, null, true)]
        [TestCase(3, 0, true)]
        [TestCase(3, 1, true)]
        [TestCase(3, 2, true)]
        [TestCase(3, 3, true)]
        [TestCase(3, 4, true, 2)] // this will create a finance plan and a visit notification INFO record (gets merged before message is created)
        [TestCase(3, 5, true)]

        // do not expect notification
        [TestCase(null, null, false)]
        [TestCase(null, 1, false)]
        [TestCase(null, 2, false)]
        [TestCase(null, 3, false)]
        [TestCase(null, 5, false)]
        [TestCase(null, 6, false)]
        [TestCase(0, null, false)]
        [TestCase(1, null, false)]
        [TestCase(2, null, false)]
        [TestCase(4, null, false)]
        [TestCase(5, null, false)]
        public void QueueFinalPastDueAndActiveUncollectablesNotificationsTest(int? financePlanBucket, int? visitAgingCount, bool expectNotification, int expectedInfoCount = 1)
        {
            Guarantor guarantor = new Guarantor { VpGuarantorId = 1 };
            IList<PendingUncollectableStatementsResult> statements = new List<PendingUncollectableStatementsResult>();

            FinancePlan financePlan = null;
            if (financePlanBucket.HasValue)
            {
                Mock<FinancePlan> financePlanMock = new Mock<FinancePlan>();
                financePlanMock.Setup(x => x.VpGuarantor).Returns(guarantor);
                financePlanMock.Setup(x => x.CurrentBucket).Returns(financePlanBucket.Value);
                financePlan = financePlanMock.Object;

                statements.Add(new PendingUncollectableStatementsResult
                {
                    CurrentBucket = financePlanBucket,
                    FinancePlanId = 1,
                    PastDueEmailSent = false,
                    VpGuarantorId = guarantor.VpGuarantorId
                });
            }

            if (visitAgingCount.HasValue)
            {
                statements.Add(new PendingUncollectableStatementsResult
                {
                    AgingTier = visitAgingCount,
                    PastDueEmailSent = false,
                    VisitId = 1,
                    VpGuarantorId = guarantor.VpGuarantorId,
                });
            }

            Domain.Visit.Interfaces.IVisitService visitService = null;

            if (visitAgingCount.HasValue && visitAgingCount == 4) //Final Past Due Visit 
            {
                VisitResult visitResult = new VisitResult() {VisitId = 1, GuarantorId = guarantor.VpGuarantorId};
                visitService = new VisitServiceMockBuilder().Setup(builder =>
                {
                    builder.VisitRepositoryMock.Setup(x =>
                        x.GetFinalPastDueVisitsMissingNotification(It.IsAny<int?>())).Returns(visitResult.ToListOfOne);
                }).CreateService();
            }

            IFinancePlanService financePlanService = new FinancePlanServiceMockBuilder().Setup(builder =>
            {
                builder.FinancePlanRepositoryMock.Setup(x => x.GetAllActiveFinancePlans(It.IsAny<int>())).Returns(financePlan?.ToListOfOne() ?? new List<FinancePlan>());
                builder.FinancePlanRepositoryMock.Setup(x => x.GetById(It.IsAny<int>())).Returns(financePlan);
                builder.FinancePlanRepositoryMock.Setup(x => x.GetFinancePlanGuarantorIdsByStatementDueDate(It.IsAny<DateTime>())).Returns(financePlan?.VpGuarantor.VpGuarantorId.ToListOfOne() ?? new List<int>());
            }).CreateService();

            IVpStatementService statementService = new VpStatementServiceMockBuilder().Setup(builder =>
            {
                builder.FinancePlanService = financePlanService;
                builder.VpStatementRepositoryMock.Setup(x => x.GuarantorsIdsWhoHaventHadPastDueEmailForCurrentStatement(It.IsAny<IList<int>>())).Returns(new List<int> { guarantor.VpGuarantorId });
                builder.VpStatementRepositoryMock.Setup(x => x.GetAllPendingUncollectableStatements(It.IsAny<int>(), It.IsAny<DateTime>())).Returns(statements);
            }).GetMockedVpStatementService();

            

            IPastDueUncollectableApplicationService pastDueUncollectableApplicationService = new PastDueUncollectableApplicationServiceMockBuilder().Setup(builder =>
            {
                builder.ApplicationServiceCommonServiceMockBuilder.ClientServiceMock = new ClientServiceMockBuilder().CreateMock();
                builder.FinancePlanService = financePlanService;
                builder.VpStatementService = statementService;
                builder.VisitService = visitService;
            }).CreateService();

            IList<FinalPastDueNotificationInfoDto> infos = pastDueUncollectableApplicationService.GetFinalPastDueNotificationsToQueue(DateTime.UtcNow);
            Assert.AreEqual(expectNotification, infos.Count == expectedInfoCount, $"Expected {expectedInfoCount} FinalPastDueNotificationInfoDto objects");
            IList<int> guarantorsToSendEmails = infos.Select(x=>x.VpGuarantorId).Distinct().ToList();
            Assert.AreEqual(expectNotification, guarantorsToSendEmails.Count == 1, "Expected one unique VpGuarantorId");
        }
    }
}
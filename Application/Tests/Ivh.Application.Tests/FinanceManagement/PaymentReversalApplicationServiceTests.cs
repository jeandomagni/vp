﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Ivh.Application.Tests.FinanceManagement
{
    using System.Diagnostics;
    using Application.FinanceManagement.ApplicationServices;
    using Common.Base.Utilities.Extensions;
    using Common.Tests.Helpers.FinancePlan;
    using Common.Tests.Helpers.Payment;
    using Common.Tests.Helpers.Visit;
    using Common.VisitPay.Enums;
    using Domain.FinanceManagement.Entities;
    using Domain.FinanceManagement.FinancePlan.Entities;
    using Domain.FinanceManagement.Payment.Entities;
    using Domain.FinanceManagement.Payment.Interfaces;
    using Domain.FinanceManagement.Payment.Services.PaymentAllocation;
    using Domain.Guarantor.Entities;
    using Domain.Visit.Entities;
    using Moq;
    using NUnit.Framework;

    [TestFixture]
    public class PaymentReversalApplicationServiceTests : ApplicationTestBase
    {
        [Test]
        public void amount_due_is_added_back_in_full_happy_path()
        {
            // User gets a statement and amount due of $100 is added 
            // User pays $100 with standard method (RecurringPaymentFinancePlan)
            // The amount is reversed 
            // We are expecting that user still owes $100

            //Arrange 
            Payment originalPaymentEntity = this.GetOriginalPaymentForReversalTests(100m, 1);
            //make the payment type in order to relieve bucket 0
            originalPaymentEntity.PaymentType = PaymentTypeEnum.RecurringPaymentFinancePlan;
            
            FinancePlan financePlan = this.FinancePlanFromPayment(originalPaymentEntity, 100m, 1000);

            // User gets a statement and amount due of $100 is added 
            financePlan.AddFinancePlanAmountsDueStatement(100m, DateTime.UtcNow.AddDays(21),1);
            // User pays $100 with standard method (RecurringPaymentFinancePlan)
            financePlan.OnPaymentAllocation(-100, DateTime.MaxValue, originalPaymentEntity.PaymentAllocations.First());

            // Validate setup
            decimal amountDue = financePlan.FinancePlanAmountsDue.Sum(x => x.AmountDue);
            Assert.AreEqual(0m,  amountDue);

            // The amount is reversed 
            Payment reversalPaymentEntity = this.CreateReversalPayment(-100m, originalPaymentEntity);
            IList<Payment> reversalPayments = new List<Payment>{reversalPaymentEntity};
            PaymentReversalApplicationService paymentReversalApplicationService = this.SetupPaymentReversalApplicationService(reversalPayments, financePlan);
            List<int> paymentIds = reversalPayments.Select(x => x.PaymentId).ToList();
            
            //Act
            bool reageFinancePlan = paymentReversalApplicationService.ReageFinancePlanPaymentBuckets(paymentIds, financePlan.FinancePlanId, -1);

            Assert.True(reageFinancePlan);
            amountDue = financePlan.FinancePlanAmountsDue.Sum(x => x.AmountDue);
            Assert.AreEqual(100m, amountDue);
        }

        [Test]
        public void amount_due_is_added_back_in_partial_happy_path()
        {
            // User gets a statement and amount due of $100 is added 
            // User pays $100 with standard method (RecurringPaymentFinancePlan)
            // The amount is reversed partially $50
            // We are expecting that amount due goes to $50

            //Arrange 
            Payment originalPaymentEntity = this.GetOriginalPaymentForReversalTests(100m, 1);
            //make the payment type in order to relieve bucket 0
            originalPaymentEntity.PaymentType = PaymentTypeEnum.RecurringPaymentFinancePlan;

            FinancePlan financePlan = this.FinancePlanFromPayment(originalPaymentEntity, 100m, 1000);

            // User gets a statement and amount due of $100 is added to amount due
            financePlan.AddFinancePlanAmountsDueStatement(100m, DateTime.UtcNow.AddDays(21), 1);
            // User pays $100 with standard method (RecurringPaymentFinancePlan)
            financePlan.OnPaymentAllocation(-200, DateTime.MaxValue, originalPaymentEntity.PaymentAllocations.First());

            // Validate setup
            decimal amountDue = financePlan.FinancePlanAmountsDue.Sum(x => x.AmountDue);
            Assert.AreEqual(0m, amountDue);

            // The amount is reversed partially $50
            Payment reversalPaymentEntity = this.CreateReversalPayment(-50m, originalPaymentEntity);
            IList<Payment> reversalPayments = new List<Payment> { reversalPaymentEntity };
            PaymentReversalApplicationService paymentReversalApplicationService = this.SetupPaymentReversalApplicationService(reversalPayments, financePlan);
            List<int> paymentIds = reversalPayments.Select(x => x.PaymentId).ToList();

            //Act
            bool reageFinancePlan = paymentReversalApplicationService.ReageFinancePlanPaymentBuckets(paymentIds, financePlan.FinancePlanId, -1);

            Assert.True(reageFinancePlan);
            amountDue = financePlan.FinancePlanAmountsDue.Sum(x => x.AmountDue);
            Assert.AreEqual(50m, amountDue);
        }

        [Test]
        public void amount_due_is_not_affected_on_reversal_of_partial_additional_payment()
        {
            // User has an amount due of $100 x2 (with a bucket 0 and 1)
            // User makes a payment of $200 (should relieve bucket 1)
            // Payment is partially reversed -$100 
            // Amount due entry should only $0 because the user has still contributed $100 towards the amount due
               
            // arrange 
            Payment originalPaymentEntity = this.GetOriginalPaymentForReversalTests(200m, 1);
            FinancePlan financePlan = this.FinancePlanFromPayment(originalPaymentEntity, 100m, 1000);

            // User receives 2 statments that add $200 to amount due
            financePlan.AddFinancePlanAmountsDueStatement(100m, DateTime.UtcNow.AddDays(21), 1);
            financePlan.AddFinancePlanAmountsDueStatement(100m, DateTime.UtcNow.AddDays(21), 2);
            // User makes a payment of $200(should relieve bucket 1)
            financePlan.OnPaymentAllocation(-200, DateTime.MaxValue, originalPaymentEntity.PaymentAllocations.First());

            // Validate setup
            decimal amountDue = financePlan.FinancePlanAmountsDue.Sum(x => x.AmountDue);
            Assert.AreEqual(100m, amountDue);
            Assert.AreEqual(0, financePlan.CurrentBucket);

            Payment reversalPaymentEntity = this.CreateReversalPayment(-100m, originalPaymentEntity);
            IList<Payment> reversalPayments = new List<Payment> { reversalPaymentEntity };
            PaymentReversalApplicationService paymentReversalApplicationService = this.SetupPaymentReversalApplicationService(reversalPayments, financePlan);
            List<int> paymentIds = reversalPayments.Select(x => x.PaymentId).ToList();

            //Act
            bool reageFinancePlan = paymentReversalApplicationService.ReageFinancePlanPaymentBuckets(paymentIds, financePlan.FinancePlanId,-1);

            // Assert 
            Assert.AreEqual(0,financePlan.CurrentBucket);
            amountDue = financePlan.FinancePlanAmountsDue.Sum(x => x.AmountDue);
            Assert.AreEqual(100m, amountDue);
        }

        [Test]
        public void amount_due_is_added_back_in_partial()
        {
            #region description
            // Same as test above but an additional partial reverse 
            // User has an amount due of $100 x2 (with a bucket 0 and 1) (amount due = $200)
            // User makes a non-recurring payment of $200 (should relieve bucket 1) (amount due = $100)
            // Payment is partially reversed -$100 
            // Amount due entry should only be $0 because the user has still contributed $100 towards the amount due
            // Should be in bucket 0 with amount due = $100
            // Payment is partially rerversed -50 
            // An amount due should be added of $50 
            // The user should be back in bucket 1 (amount due = $150   $100 from bucket 0 and $50 from bucket 1) 
            #endregion

            // Arrange 
            Payment originalPaymentEntity = this.GetOriginalPaymentForReversalTests(200m, 1);
            FinancePlan financePlan = this.FinancePlanFromPayment(originalPaymentEntity, 100m, 1000);

            // user has an amount due of $100 x2 (with a bucket 0 and 1) (amount due = $200)
            financePlan.AddFinancePlanAmountsDueStatement(100m, DateTime.UtcNow.AddDays(21), 1);
            financePlan.AddFinancePlanAmountsDueStatement(100m, DateTime.UtcNow.AddDays(21), 2);
            // user makes a non-recurring payment of $200 (should relieve bucket 1) (amount due = $100)
            financePlan.OnPaymentAllocation(-200, DateTime.MaxValue, originalPaymentEntity.PaymentAllocations.First());

            // Validation Assertes
            // bucket 1 paid down, bucket 0 amount due remains
            decimal amountDue = financePlan.FinancePlanAmountsDue.Sum(x => x.AmountDue);
            Assert.AreEqual(100m, amountDue);
            Assert.AreEqual(0, financePlan.CurrentBucket);

            Payment reversalPaymentEntity = this.CreateReversalPayment(-100m, originalPaymentEntity);
            Payment reversalPaymentEntity2 = this.CreateReversalPayment(-50m, originalPaymentEntity);
            IList<Payment> reversalPayments = new List<Payment> { reversalPaymentEntity, reversalPaymentEntity2 };
            PaymentReversalApplicationService paymentReversalApplicationService = this.SetupPaymentReversalApplicationService(reversalPayments, financePlan);
            List<int> paymentIds = reversalPayments.Select(x => x.PaymentId).ToList();

            //Act
            bool reageFinancePlan = paymentReversalApplicationService.ReageFinancePlanPaymentBuckets(paymentIds, financePlan.FinancePlanId, -1);

            //Assert
            amountDue = financePlan.FinancePlanAmountsDue.Sum(x => x.AmountDue);
            //bucket 1 gets added back with $50 
            Assert.AreEqual(150m, amountDue);
            Assert.AreEqual(1, financePlan.CurrentBucket);
        }

        [Test]
        public void amount_due_bucket_1_is_added_back_with_full_reversal_with_interest()
        {
            #region description
            // User has an amount due of $100 x2 (with a bucket 0 and 1) (amount due = $200)
            // User has interest due of 5$ x2
            // User makes a non-recurring payment of $200 (should relieve bucket 1) (amount due = $100)
            // 190/10 principal/interest allocations
            // Should be in bucket 0 with amount due = $100
            // Payment is fully rerversed -200 
            // The user should be back in bucket 1 (amount due = $200   $100 from bucket 0 and $100 from bucket 1) 
            // The user should still have $10 interest due
            #endregion

            // Arrange 
            Payment originalPaymentEntity = this.GetOriginalPaymentForReversalWithInterestTests(190m,10m, 1);
            FinancePlan financePlan = this.FinancePlanFromPayment(originalPaymentEntity, 100m, 1000);
            FinancePlanVisit financePlanVisit = financePlan.FinancePlanVisits.FirstOrDefault();

            // user has an amount due of $100 x2 (with a bucket 0 and 1) (amount due = $200)
            financePlan.AddFinancePlanAmountsDueStatement(100m, DateTime.UtcNow.AddDays(21), 1);
            this.AddInterestDue(financePlan,5m,1,financePlanVisit);
            financePlan.AddFinancePlanAmountsDueStatement(100m, DateTime.UtcNow.AddDays(21), 2);
            this.AddInterestDue(financePlan, 5m, 1, financePlanVisit);

            // user makes a non-recurring payment of $200 (should relieve bucket 1) (amount due = $100)
            foreach (PaymentAllocation paymentAllocation in originalPaymentEntity.PaymentAllocations)
            {
                financePlan.OnPaymentAllocation(decimal.Negate(paymentAllocation.ActualAmount), DateTime.MaxValue, paymentAllocation);
            }
           
            // Validation Asserts
            // bucket 1 paid down, bucket 0 amount due remains
            decimal amountDue = financePlan.FinancePlanAmountsDue.Sum(x => x.AmountDue);
            decimal interestDue = financePlanVisit.FinancePlanVisitInterestDues.Sum(x => x.InterestDue);
            Assert.AreEqual(100m, amountDue);
            Assert.AreEqual(0, financePlan.CurrentBucket);
            Assert.AreEqual(0m, interestDue);

            //full reversal
            Payment reversalPaymentEntity0 = this.CreateReversalPayment(-200m, originalPaymentEntity);
            IList<Payment> reversalPayments = new List<Payment> { reversalPaymentEntity0};
            PaymentReversalApplicationService paymentReversalApplicationService = this.SetupPaymentReversalApplicationService(reversalPayments, financePlan);
            List<int> paymentIds = reversalPayments.Select(x => x.PaymentId).ToList();

            //Act
            bool reageFinancePlan = paymentReversalApplicationService.ReageFinancePlanPaymentBuckets(paymentIds, financePlan.FinancePlanId, -1);

            //Assert
            amountDue = financePlan.FinancePlanAmountsDue.Sum(x => x.AmountDue);
            //bucket 1 gets added back with $100 
            Assert.AreEqual(200m, amountDue);
            Assert.AreEqual(1, financePlan.CurrentBucket);

            //check interest due.
            //the user was charged 2 interest amounts of $5. $10 was paid then reverse. User still owes $10
            interestDue = financePlanVisit.FinancePlanVisitInterestDues.Sum(x => x.InterestDue);
            Assert.AreEqual(10m, interestDue);

        }

        [Test]
        public void amount_due_bucket_1_is_added_back_with_partial_reversal_with_interest()
        {
            #region description
            // User has an amount due of $100 x2 (with a bucket 0 and 1) (amount due = $200)
            // User has interest due of 5$ x2
            // User makes a non-recurring payment of $200 (should relieve bucket 1) (amount due = $100)
            // 190/10 principal/interest allocations
            // Should be in bucket 0 with amount due = $100
            // Payment is partially rerversed -195
            // The user should be back in bucket 1 (amount due = $195   $100 from bucket 0 and $95 from bucket 1) 
            // The user should still have $5 interest due 
            #endregion

            // Arrange 
            Payment originalPaymentEntity = this.GetOriginalPaymentForReversalWithInterestTests(190m, 10m, 1);
            FinancePlan financePlan = this.FinancePlanFromPayment(originalPaymentEntity, 100m, 1000);
            FinancePlanVisit financePlanVisit = financePlan.FinancePlanVisits.FirstOrDefault();

            // user has an amount due of $100 x2 (with a bucket 0 and 1) (amount due = $200)
            financePlan.AddFinancePlanAmountsDueStatement(100m, DateTime.UtcNow.AddDays(21), 1);
            this.AddInterestDue(financePlan, 5m, 1, financePlanVisit);
            financePlan.AddFinancePlanAmountsDueStatement(100m, DateTime.UtcNow.AddDays(21), 2);
            this.AddInterestDue(financePlan, 5m, 1, financePlanVisit);
            
            // user makes a non-recurring payment of $200 (should relieve bucket 1) (amount due = $100)
            foreach (PaymentAllocation paymentAllocation in originalPaymentEntity.PaymentAllocations)
            {
                financePlan.OnPaymentAllocation(decimal.Negate(paymentAllocation.ActualAmount), DateTime.MaxValue, paymentAllocation);
            }

            // Validation Asserts
            // bucket 1 paid down, bucket 0 amount due remains
            decimal amountDue = financePlan.FinancePlanAmountsDue.Sum(x => x.AmountDue);
            Assert.AreEqual(100m, amountDue);
            Assert.AreEqual(0, financePlan.CurrentBucket);

            //partial reversal
            Payment reversalPaymentEntity0 = this.CreateReversalPayment(-195m, originalPaymentEntity);

            IList<Payment> reversalPayments = new List<Payment> { reversalPaymentEntity0 };
            PaymentReversalApplicationService paymentReversalApplicationService = this.SetupPaymentReversalApplicationService(reversalPayments, financePlan);
            List<int> paymentIds = reversalPayments.Select(x => x.PaymentId).ToList();

            //Act
            bool reageFinancePlan = paymentReversalApplicationService.ReageFinancePlanPaymentBuckets(paymentIds, financePlan.FinancePlanId, -1);

            //Assert
            amountDue = financePlan.FinancePlanAmountsDue.Sum(x => x.AmountDue);
            decimal interestDue = financePlanVisit.FinancePlanVisitInterestDues.Sum(x => x.InterestDue);
            //bucket 1 gets added back with $50 
            Assert.AreEqual(195m, amountDue);
            Assert.AreEqual(1, financePlan.CurrentBucket);
            Assert.AreEqual(5m, interestDue);
        }

        [Test]
        public void amount_due_bucket_1_is_added_back_with_full_reversal_with_interest_with_ach_payment()
        {
            #region description
            // User has an amount due of $100 x2 (with a bucket 0 and 1) (amount due = $200)
            // User has interest due of 5$ x2
            // User makes a non-recurring payment of $200 (should relieve bucket 1) (amount due = $100)
            // 190/10 principal/interest allocations
            // Should be in bucket 0 with amount due = $100
            // Payment is fully rerversed -200 
            // The user should be back in bucket 1 (amount due = $200   $100 from bucket 0 and $100 from bucket 1) 
            // The user should still have $10 interest due
            #endregion

            // Arrange 
            Payment achPayment = this.GetOriginalPaymentForReversalWithInterestTests(190m, 10m, 1,PaymentMethodTypeEnum.AchChecking);
            FinancePlan financePlan = this.FinancePlanFromPayment(achPayment, 100m, 1000);
            FinancePlanVisit financePlanVisit = financePlan.FinancePlanVisits.FirstOrDefault();

            // user has an amount due of $100 x2 (with a bucket 0 and 1) (amount due = $200)
            financePlan.AddFinancePlanAmountsDueStatement(100m, DateTime.UtcNow.AddDays(21), 1);
            this.AddInterestDue(financePlan, 5m, 1, financePlanVisit);
            financePlan.AddFinancePlanAmountsDueStatement(100m, DateTime.UtcNow.AddDays(21), 2);
            this.AddInterestDue(financePlan, 5m, 1, financePlanVisit);

            // user makes a non-recurring payment of $200 (should relieve bucket 1) (amount due = $100)
            foreach (PaymentAllocation paymentAllocation in achPayment.PaymentAllocations)
            {
                financePlan.OnPaymentAllocation(decimal.Negate(paymentAllocation.ActualAmount), DateTime.MaxValue, paymentAllocation);
            }

            // Validation Asserts
            // bucket 1 paid down, bucket 0 amount due remains
            decimal amountDue = financePlan.FinancePlanAmountsDue.Sum(x => x.AmountDue);
            decimal interestDue = financePlanVisit.FinancePlanVisitInterestDues.Sum(x => x.InterestDue);
            Assert.AreEqual(100m, amountDue);
            Assert.AreEqual(0, financePlan.CurrentBucket);
            Assert.AreEqual(0m, interestDue);

            //full reversal
            this.CreateReversalPaymentForAch(achPayment);
            IList<Payment> reversalPayments = new List<Payment> { achPayment };
            PaymentReversalApplicationService paymentReversalApplicationService = this.SetupPaymentReversalApplicationService(reversalPayments, financePlan);
            List<int> paymentIds = reversalPayments.Select(x => x.PaymentId).ToList();

            //Act
            bool reageFinancePlan = paymentReversalApplicationService.ReageFinancePlanPaymentBuckets(paymentIds, financePlan.FinancePlanId, -1);

            //Assert
            amountDue = financePlan.FinancePlanAmountsDue.Sum(x => x.AmountDue);
            //bucket 1 gets added back with $100 
            Assert.AreEqual(200m, amountDue);
            Assert.AreEqual(1, financePlan.CurrentBucket);

            //check interest due.
            //the user was charged 2 interest amounts of $5. $10 was paid then reverse. User still owes $10
            interestDue = financePlanVisit.FinancePlanVisitInterestDues.Sum(x => x.InterestDue);
            Assert.AreEqual(10m, interestDue);

        }

        /// <summary>
        /// Inspired by VP-1332 - 2 stacked plans with a partial reversal. The amount due was getting added back the full reversal amount for each fp 
        /// when it should have been getting the allocated amount (reversed amount = $1, each fp should get a .50 amount due)
        /// </summary>
        [Test]
        public void amount_due_is_added_back_in_partial_for_stacked_finance_plans()
        {
            IList<Tuple<Visit, IList<VisitTransaction>>> visits1 = VisitFactory.GenerateActiveVisit(1000.00m, 1, VisitStateEnum.Active).ToListOfOne();
            FinancePlan financePlan1 = FinancePlanFactory.CreateFinancePlan(visits1.Take(1).ToList(), 0, 100);

            IList<Tuple<Visit, IList<VisitTransaction>>> visits2 = VisitFactory.GenerateActiveVisit(1000.00m, 1, VisitStateEnum.Active).ToListOfOne();
            FinancePlan financePlan2 = FinancePlanFactory.CreateFinancePlan(visits2.Take(1).ToList(), 0, 100);

            // User gets a statement and amount due of $100 is added 
            financePlan1.AddFinancePlanAmountsDueStatement(100m, DateTime.UtcNow.AddDays(21), 1);
            financePlan2.AddFinancePlanAmountsDueStatement(100m, DateTime.UtcNow.AddDays(21), 1);
            List<FinancePlan> financePlans = new List<FinancePlan> { financePlan1, financePlan2 };
            
            Payment payment = this.GetOriginalPaymentForReversalTests(200, financePlans, PaymentMethodTypeEnum.AchChecking);
            foreach (PaymentAllocation paymentAllocation in payment.PaymentAllocationsAll)
            {
                FinancePlan financePlan = financePlans.FirstOrDefault(x => x.FinancePlanId == paymentAllocation.FinancePlanId);
                financePlan.OnPaymentAllocation(decimal.Negate(paymentAllocation.ActualAmount), DateTime.UtcNow, paymentAllocation, null);
            }

            //pre assert
            Assert.AreEqual(0, financePlan1.CurrentAmount);
            Assert.AreEqual(0, financePlan2.CurrentAmount);

            Payment reversalPaymentEntity = this.CreateReversalPayment(-1m, payment);
            IList<Payment> reversalPayments = new List<Payment> { reversalPaymentEntity };
            PaymentReversalApplicationService paymentReversalApplicationService = this.SetupPaymentReversalApplicationServiceWithFinancePlanList(reversalPayments, financePlans);
            List<int> paymentIds = reversalPayments.Select(x => x.PaymentId).ToList();
            
            //Act
            paymentReversalApplicationService.ReageFinancePlanPaymentBuckets(paymentIds, financePlan1.FinancePlanId, -1);
            paymentReversalApplicationService.ReageFinancePlanPaymentBuckets(paymentIds, financePlan2.FinancePlanId, -1);
            Assert.AreEqual(.5m, financePlan1.CurrentAmount);
            Assert.AreEqual(.5m, financePlan2.CurrentAmount);
        }

        /// <summary>
        /// VP-2099 Refunding Amount Greater Than Monthly Payment Amount Creates Reconfig Alert
        /// reported using specific finance plan payment which does not decrease amount due
        /// </summary>
        [Test]
        public void Refund_PaymentExceedingAmountDue_RefundsFullAmountToVisitPrincipalAmount()
        {
            const decimal visitBalance = 2500.01m;
            const decimal monthlyPaymentAmount = 250m;
            const decimal paymentAmount = 300m;
            const decimal refundAmount = -275m;

            Tuple<Visit, IList<VisitTransaction>> visit = VisitFactory.GenerateActiveVisit(visitBalance, 1, VisitStateEnum.Active);
            FinancePlan financePlan = FinancePlanFactory.CreateFinancePlan(visit.ToListOfOne(), 0, monthlyPaymentAmount);
            financePlan.AddFinancePlanAmountsDueStatement(monthlyPaymentAmount, DateTime.UtcNow.AddDays(21), 123);
            Assert.AreEqual(monthlyPaymentAmount, financePlan.AmountDue);

            Payment payment = this.GetOriginalPaymentForReversalTests(paymentAmount, financePlan.ToListOfOne(), PaymentMethodTypeEnum.Visa);
            payment.PaymentType = PaymentTypeEnum.ManualPromptSpecificFinancePlans;
            foreach (PaymentAllocation paymentAllocation in payment.PaymentAllocationsAll)
            {
                paymentAllocation.PaymentAllocationType = PaymentAllocationTypeEnum.VisitPrincipal;
                financePlan.OnPaymentAllocation(decimal.Negate(paymentAllocation.ActualAmount), DateTime.UtcNow, paymentAllocation, null);
            }
            Assert.AreEqual(monthlyPaymentAmount, financePlan.AmountDue);

            financePlan.FinancePlanVisits[0].CurrentVisitBalance -= paymentAmount;
            Assert.AreEqual(financePlan.CurrentFinancePlanBalance, financePlan.FinancePlanVisits[0].CurrentVisitBalance);
            Assert.AreEqual(2, financePlan.FinancePlanVisits[0].FinancePlanVisitPrincipalAmounts.Count);
            Assert.AreEqual(visitBalance - paymentAmount, financePlan.FinancePlanVisits[0].FinancePlanVisitPrincipalAmounts.Sum(x => x.Amount));

            Payment reversalPayment = this.CreateReversalPayment(refundAmount, payment);
            foreach (PaymentAllocation paymentAllocation in reversalPayment.PaymentAllocations)
            {
                paymentAllocation.PaymentAllocationType = PaymentAllocationTypeEnum.VisitPrincipal;
            }
            PaymentReversalApplicationService paymentReversalApplicationService = this.SetupPaymentReversalApplicationServiceWithFinancePlanList(reversalPayment.ToListOfOne(), financePlan.ToListOfOne());
            paymentReversalApplicationService.ReageFinancePlanPaymentBuckets(reversalPayment.PaymentId.ToListOfOne(), financePlan.FinancePlanId, -1);
            financePlan.FinancePlanVisits[0].CurrentVisitBalance -= refundAmount;

            decimal expectedPrincipalBalance = visitBalance - paymentAmount - refundAmount;
            Assert.AreEqual(expectedPrincipalBalance, financePlan.FinancePlanVisits[0].FinancePlanVisitPrincipalAmounts.Sum(x => x.Amount));
            Assert.AreEqual(expectedPrincipalBalance, financePlan.CurrentFinancePlanBalance);
            Assert.False(financePlan.HasPositiveVisitBalanceDifference());
        }

        private PaymentReversalApplicationService SetupPaymentReversalApplicationService(IList<Payment> reversalPayments, FinancePlan financePlan)
        {
            PaymentReversalApplicationServiceMockBuilder paymentReversalApplicationServiceMockBuilder = new PaymentReversalApplicationServiceMockBuilder();
            paymentReversalApplicationServiceMockBuilder.Setup(builder =>
            {
                builder.FinancePlanServiceMock.Setup(srv => srv.GetFinancePlan(It.IsAny<Guarantor>(), It.IsAny<int>())).Returns(financePlan);
                builder.PaymentServiceMock.Setup(srv => srv.GetPayments(It.IsAny<IList<int>>(), It.IsAny<int>())).Returns((IReadOnlyList<Payment>) reversalPayments);
            });

            PaymentReversalApplicationService paymentReversalApplicationService = paymentReversalApplicationServiceMockBuilder.CreateService();
            return paymentReversalApplicationService;
        }

        private PaymentReversalApplicationService SetupPaymentReversalApplicationServiceWithFinancePlanList(IList<Payment> reversalPayments, IList<FinancePlan> financePlans)
        {
            PaymentReversalApplicationServiceMockBuilder paymentReversalApplicationServiceMockBuilder = new PaymentReversalApplicationServiceMockBuilder();
            paymentReversalApplicationServiceMockBuilder.Setup(builder =>
            {
                builder.FinancePlanServiceMock.Setup(srv => srv.GetFinancePlan(It.IsAny<Guarantor>(), It.IsAny<int>())).Returns<Guarantor,int>((g, fpId) =>
                {
                    return financePlans.FirstOrDefault(x => x.FinancePlanId == fpId);
                });
                builder.PaymentServiceMock.Setup(srv => srv.GetPayments(It.IsAny<IList<int>>(), It.IsAny<int>())).Returns((IReadOnlyList<Payment>)reversalPayments);
            });

            PaymentReversalApplicationService paymentReversalApplicationService = paymentReversalApplicationServiceMockBuilder.CreateService();
            return paymentReversalApplicationService;
        }

        private FinancePlan FinancePlanFromPayment(Payment originalPaymentEntity, decimal paymentAmount, decimal currentBalance)
        {
            int financePlanId = originalPaymentEntity.PaymentScheduledAmounts.First().PaymentFinancePlan.FinancePlanId;
            FinancePlan financePlan = new FinancePlan { FinancePlanId = financePlanId, PaymentAmount = paymentAmount };

            IList<FinancePlanVisit> activeFinancePlanVisits = new List<FinancePlanVisit>();
            
            foreach (PaymentAllocation paymentAllocation in originalPaymentEntity.PaymentAllocations)
            {
                paymentAllocation.Payment = originalPaymentEntity;
            }

            List<int> visitIds = originalPaymentEntity.PaymentAllocations.Select(x => x.PaymentVisit.VisitId).Distinct().ToList();
            decimal visitBalance = (visitIds.Count > 0) ? currentBalance / visitIds.Count : 0m;
            foreach (int visitId in visitIds)
            {
                FinancePlanVisit financePlanVisit = new FinancePlanVisit { FinancePlanVisitId = visitId, VisitId = visitId, CurrentVisitBalance = visitBalance };
                FinancePlanVisitPrincipalAmount financePlanVisitPrincipalAmount = new FinancePlanVisitPrincipalAmount
                {
                    FinancePlan = financePlan,
                    FinancePlanVisit = financePlanVisit,
                    Amount = visitBalance,
                    FinancePlanVisitPrincipalAmountType = FinancePlanVisitPrincipalAmountTypeEnum.FinancePlanCreated
                };

                financePlanVisit.FinancePlanVisitPrincipalAmounts.Add(financePlanVisitPrincipalAmount);


                activeFinancePlanVisits.Add(financePlanVisit);
            }

            financePlan.FinancePlanVisits = activeFinancePlanVisits;
            financePlan.VpGuarantor = new Guarantor();
            return financePlan;

        }

        private Payment GetOriginalPaymentForReversalTests(decimal actualAmount, IList<FinancePlan>financePlans, PaymentMethodTypeEnum paymentMethodType)
        {
            List<PaymentAllocation> paymentAllocations = new List<PaymentAllocation>();
            List<PaymentScheduledAmount> paymentScheduledAmounts = new List<PaymentScheduledAmount>();
            decimal splitAmount = actualAmount / financePlans.Count;
            int paymentAllocationId = 1;
            foreach (FinancePlan financePlan in financePlans)
            {
                decimal visitSplit = splitAmount / financePlan.FinancePlanVisits.Count;
                foreach (FinancePlanVisit financePlanVisit in financePlan.FinancePlanVisits)
                {
                    int visitId = financePlanVisit.VisitId;
                    PaymentVisit paymentVisit = new PaymentVisit { VisitId = visitId };
                    PaymentAllocation paymentAllocation = new PaymentAllocation {
                        PaymentAllocationId = paymentAllocationId,
                        ActualAmount = visitSplit,
                        OriginalAmount = actualAmount,
                        PaymentVisit = paymentVisit,
                        FinancePlanId = financePlan.FinancePlanId
                    };
                    paymentAllocations.Add(paymentAllocation);
                }

                paymentScheduledAmounts.Add(new PaymentScheduledAmount { PaymentFinancePlan = new PaymentFinancePlan { FinancePlanId = financePlan.FinancePlanId } });
            }

            Payment originalPaymentEntity = new Payment
            {
                ActualPaymentAmount = actualAmount,
                PaymentScheduledAmounts = paymentScheduledAmounts,
                PaymentAllocations = paymentAllocations,
                PaymentType = PaymentTypeEnum.RecurringPaymentFinancePlan,
                PaymentMethod = new PaymentMethod { PaymentMethodType = paymentMethodType },
            };

            originalPaymentEntity.PaymentAllocationsAll.ForEach(x => { x.Payment = originalPaymentEntity; });
            return originalPaymentEntity;
        }
        
        private Payment GetOriginalPaymentForReversalTests(decimal actualAmount, int paymentAllocationId)
        {
            PaymentVisit paymentVisit1 = new PaymentVisit { VisitId = 3006671 };
            PaymentAllocation paymentAllocation1 = new PaymentAllocation {PaymentAllocationId = paymentAllocationId, ActualAmount = actualAmount,OriginalAmount = 200m,PaymentVisit = paymentVisit1 };
            List<PaymentAllocation> paymentAllocations = new List<PaymentAllocation> { paymentAllocation1 };
            int financePlanId = 3799;

            List<PaymentScheduledAmount> paymentScheduledAmounts = new List<PaymentScheduledAmount>();

            paymentScheduledAmounts.Add(new PaymentScheduledAmount { PaymentFinancePlan = new PaymentFinancePlan { FinancePlanId = financePlanId } });

            Payment originalPaymentEntity = new Payment
            {
                ActualPaymentAmount = 200m,
                PaymentScheduledAmounts = paymentScheduledAmounts,
                PaymentAllocations = paymentAllocations,
                PaymentType = PaymentTypeEnum.ManualPromptSpecificAmount,
                PaymentMethod = new PaymentMethod { PaymentMethodType = PaymentMethodTypeEnum.Visa }
            };

            return originalPaymentEntity;
        }

        private Payment GetOriginalPaymentForReversalWithInterestTests(decimal principalAmount, decimal interestAmount, int paymentAllocationId, PaymentMethodTypeEnum paymentMethod = PaymentMethodTypeEnum.Visa)
        {
            PaymentVisit paymentVisit1 = new PaymentVisit { VisitId = 3006671 };

            PaymentAllocation paymentAllocation1 = new PaymentAllocation
            {
                PaymentAllocationType = PaymentAllocationTypeEnum.VisitPrincipal,
                PaymentAllocationId = paymentAllocationId,
                ActualAmount = principalAmount,
                OriginalAmount = principalAmount,
                PaymentVisit = paymentVisit1
            };
            int paymentAllocationId2 = paymentAllocationId + 1;
            PaymentAllocation paymentAllocation2 = new PaymentAllocation
            {
                PaymentAllocationType = PaymentAllocationTypeEnum.VisitInterest,
                PaymentAllocationId = paymentAllocationId2,
                ActualAmount = interestAmount,
                OriginalAmount = interestAmount,
                PaymentVisit = paymentVisit1
            };
            List<PaymentAllocation> paymentAllocations = new List<PaymentAllocation> { paymentAllocation1, paymentAllocation2 };
            int financePlanId = 3799;

            List<PaymentScheduledAmount> paymentScheduledAmounts = new List<PaymentScheduledAmount>();

            paymentScheduledAmounts.Add(new PaymentScheduledAmount { PaymentFinancePlan = new PaymentFinancePlan { FinancePlanId = financePlanId } });

            Payment originalPaymentEntity = new Payment
            {
                ActualPaymentAmount = principalAmount + paymentAllocationId,
                PaymentScheduledAmounts = paymentScheduledAmounts,
                PaymentAllocations = paymentAllocations,
                PaymentType = PaymentTypeEnum.ManualPromptSpecificAmount,
                PaymentMethod = new PaymentMethod { PaymentMethodType = paymentMethod }
            };

            return originalPaymentEntity;
        }

        private Payment CreateReversalPayment(decimal reverseAmount, Payment originalPaymentEntity)
        {
            int paymentAllocationIdSeed = originalPaymentEntity.PaymentAllocationsAll.Max(x => x.PaymentAllocationId);

            int financePlanId = originalPaymentEntity.PaymentScheduledAmounts.First().PaymentFinancePlan.FinancePlanId;

            PaymentScheduledAmount firstPartialRefund = new PaymentScheduledAmount
            {
                ScheduledAmount = reverseAmount,
                PaymentFinancePlan = new PaymentFinancePlan { FinancePlanId = financePlanId }
            };

            Payment reversalPaymentEntity = new Payment
            {
                ParentPayment = originalPaymentEntity,
                ActualPaymentAmount = 0.00m,
                PaymentScheduledAmounts = new List<PaymentScheduledAmount>
                {
                    firstPartialRefund
                },
                PaymentMethod = originalPaymentEntity.PaymentMethod

            };

            originalPaymentEntity.ReversalPayments.Add(reversalPaymentEntity);

            PaymentAllocationServiceMockBuilder paymentAllocationServiceMockBuilder = new PaymentAllocationServiceMockBuilder();
            IPaymentAllocationService paymentAllocationService = paymentAllocationServiceMockBuilder.GetPaymentAllocationService(typeof(BillingApplicationPaymentAllocationService));
            PaymentAllocationResult paymentAllocationResult = paymentAllocationService.AllocatePaymentReverse(originalPaymentEntity, reversalPaymentEntity);
            paymentAllocationResult.CommitAllocationsToPayment();

            foreach (PaymentAllocation paymentAllocation in reversalPaymentEntity.PaymentAllocationsAll)
            {
                paymentAllocationIdSeed++;
                paymentAllocation.PaymentAllocationId = paymentAllocationIdSeed;
            }

            
            return reversalPaymentEntity;

        }

        private void CreateReversalPaymentForAch(Payment payment)
        {
            int paymentAllocationIdSeed = payment.PaymentAllocationsAll.Max(x => x.PaymentAllocationId);

            PaymentAllocationServiceMockBuilder paymentAllocationServiceMockBuilder = new PaymentAllocationServiceMockBuilder();
            IPaymentAllocationService paymentAllocationService = paymentAllocationServiceMockBuilder.GetPaymentAllocationService(typeof(BillingApplicationPaymentAllocationService));
            paymentAllocationService.OffsetAllAllocationsInPayment(payment);

            foreach (PaymentAllocation paymentAllocation in payment.PaymentAllocationsAll.Where(x => x.PaymentAllocationId > paymentAllocationIdSeed))
            {
                paymentAllocationIdSeed++;
                paymentAllocation.PaymentAllocationId = paymentAllocationIdSeed;
            }
        }

        private void AddInterestDue(FinancePlan financePlan, decimal interestAmount, int vpStatemmentId, FinancePlanVisit financePlanVisit)
        {
            FinancePlanVisitInterestDue financePlanVisitInterestDue = new FinancePlanVisitInterestDue
            {
                CurrentBalanceAtTimeOfAssessment = 1000m,//shouldn't matter
                FinancePlan = financePlan,
                InterestDue = interestAmount,
                VpStatementId = vpStatemmentId,
                InterestRateUsedToAssess = financePlan.CurrentInterestRate,
                InsertDate = DateTime.UtcNow
            };
            financePlanVisit.FinancePlanVisitInterestDues.Add(financePlanVisitInterestDue);
        }

        private void PrintOutAmountDue(FinancePlan financePlan)
        {
            Debug.WriteLine($"Amount Due");
            Debug.WriteLine($"------------------------------------------------");
            foreach (FinancePlanAmountDue financePlanAmountDue in financePlan.FinancePlanAmountsDue)
            {
                Debug.WriteLine($"AmountDue:{financePlanAmountDue.AmountDue}    AllocationId:{financePlanAmountDue.PaymentAllocationId}    VpStatementId:{financePlanAmountDue.VpStatementId}");
            }
        }

        private void PrintOutInterestDue(FinancePlanVisit financePlanVisit)
        {
            Debug.WriteLine($"Interest Due");
            Debug.WriteLine($"------------------------------------------------");
            foreach (FinancePlanVisitInterestDue financePlanAmountDue in financePlanVisit.FinancePlanVisitInterestDues)
            {
                Debug.WriteLine($"InterestDue:{financePlanAmountDue.InterestDue}    AllocationId:{financePlanAmountDue.PaymentAllocationId}    VpStatementId:{financePlanAmountDue.VpStatementId}");
            }
        }

    }
}

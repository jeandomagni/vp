﻿namespace Ivh.Application.Tests.FinanceManagement
{
    using Application.FinanceManagement.Common.Interfaces;
    using AutoMapper;
    using Common.Base.Utilities.Extensions;
    using Common.Tests.Helpers;
    using Common.Tests.Helpers.FinancePlan;
    using Common.Tests.Helpers.Guarantor;
    using Common.Tests.Helpers.Payment;
    using Common.Tests.Helpers.Visit;
    using Common.VisitPay.Enums;
    using Common.VisitPay.Messages.FinanceManagement;
    using Common.VisitPay.Messages.HospitalData;
    using Domain.FinanceManagement.FinancePlan.Entities;
    using Domain.FinanceManagement.FinancePlan.Interfaces;
    using Domain.FinanceManagement.Payment.Entities;
    using Domain.FinanceManagement.Payment.Services.PaymentAllocation;
    using Domain.Guarantor.Entities;
    using Domain.Settings.Entities;
    using Domain.Visit.Entities;
    using Ivh.Application.FinanceManagement.Common.Dtos;
    using Ivh.Domain.Visit.Interfaces;
    using Moq;
    using NUnit.Framework;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;

    [TestFixture]
    public class FinancePlanApplicationServiceTests : ApplicationTestBase
    {
        [TestCase(FinancePlanStatusEnum.NonPaidInFull, true)]
        [TestCase(FinancePlanStatusEnum.PaidInFull, true)]
        [TestCase(FinancePlanStatusEnum.Canceled, false)]
        [TestCase(FinancePlanStatusEnum.GoodStanding, false)]
        [TestCase(FinancePlanStatusEnum.PastDue, false)]
        [TestCase(FinancePlanStatusEnum.PendingAcceptance, false)]
        [TestCase(FinancePlanStatusEnum.UncollectableActive, false)]
        [TestCase(FinancePlanStatusEnum.UncollectableClosed, false)]
        public void FinancePlanClosedEmail_Sends(FinancePlanStatusEnum financePlanStatus, bool expectEmail)
        {
            bool didSendEmail = false;

            FinancePlan fp = new FinancePlan
            {
                FinancePlanId = 1,
                FinancePlanStatus = new FinancePlanStatus
                {
                    FinancePlanStatusId = (int)financePlanStatus
                },
                VpGuarantor = new Guarantor
                {
                    VpGuarantorId = 1
                }
            };

            FinancePlanApplicationServiceMockBuilder mockBuilder = new FinancePlanApplicationServiceMockBuilder();
            mockBuilder.FinancePlanServiceMock.Setup(x => x.GetFinancePlan(It.IsAny<Guarantor>(), It.IsAny<int>())).Returns(fp);
            mockBuilder.SessionMock = new SessionMockBuilder()
                .UseSynchronizations() // this lets the RegisterPostCommits fire
                .CreateMock();

            mockBuilder.ApplicationServiceCommonServiceMockBuilder.BusMock.Setup(x => x.PublishMessage(It.IsAny<SendFinancePlanClosedEmailMessage>())).Returns(() =>
            {
                didSendEmail = true;
                return Task.CompletedTask;
            });

            IFinancePlanApplicationService svc = mockBuilder.CreateService();

            svc.ConsumeMessage(new FinancePlanClosedMessage
            {
                FinancePlanId = fp.FinancePlanId,
                VpGuarantorId = fp.VpGuarantor.VpGuarantorId
            }, null);

            Assert.AreEqual(expectEmail, didSendEmail);
        }

        [Test]
        public void OnFinancePlanCreate_ComboPlan_OutboundVisitMessage_SendsOneForNewVisit()
        {
            const decimal totalBalance = 1000m;
            const decimal nPayments = 10;
            const decimal paymentAmount = totalBalance / nPayments;
            IList<Tuple<Visit, IList<VisitTransaction>>> visits = new List<Tuple<Visit, IList<VisitTransaction>>>
            {
                VisitFactory.GenerateActiveVisit(totalBalance, 1),
                VisitFactory.GenerateActiveVisit(totalBalance, 1)
            };
            FinancePlan financePlan = FinancePlanFactory.CreateFinancePlan(visits.First().ToListOfOne(), 1, paymentAmount);
            FinancePlan newComboFinancePlan = FinancePlanFactory.CreateFinancePlan(visits, 1, paymentAmount);
            newComboFinancePlan.IsCombined = true;
            IList<OutboundVisitMessage> outboundVisitMessagesSent = new List<OutboundVisitMessage>();

            IFinancePlanApplicationService svc = this.CreateServiceForOnFinancePlanCreateTests(newComboFinancePlan, financePlan, visits, outboundVisitMessagesSent);

            financePlan.SetFinancePlanStatus(new FinancePlanStatus { FinancePlanStatusId = (int)FinancePlanStatusEnum.Canceled }, "");
            financePlan.PaymentAmount = 0m;
            financePlan.WriteOffInterestDue();
            financePlan.OnFinancePlanClose();
            financePlan.CombinedFinancePlan = newComboFinancePlan;

            svc.OnFinancePlanCreated(0, newComboFinancePlan.FinancePlanId);

            //should just send one added message
            Assert.AreEqual(1, outboundVisitMessagesSent.Count(x => x.VpOutboundVisitMessageType == VpOutboundVisitMessageTypeEnum.VisitAddedToFinancePlan));

            //was sent for the new visit
            Assert.AreEqual(visits.Last().Item1.VisitId, outboundVisitMessagesSent.First().VpVisitId);

        }

        [Test]
        public void OnFinancePlanCreate_ReconfigPlan_OutboundVisitMessage_SendsNone()
        {
            const decimal totalBalance = 1000m;
            const decimal nPayments = 10;
            const decimal paymentAmount = totalBalance / nPayments;
            IList<Tuple<Visit, IList<VisitTransaction>>> visits = new List<Tuple<Visit, IList<VisitTransaction>>>
            {
                VisitFactory.GenerateActiveVisit(totalBalance, 1)
            };
            FinancePlan financePlan = FinancePlanFactory.CreateFinancePlan(visits.First().ToListOfOne(), 1, paymentAmount);
            FinancePlan newFinancePlan = FinancePlanFactory.CreateFinancePlan(visits, 1, paymentAmount);
            IList<OutboundVisitMessage> outboundVisitMessagesSent = new List<OutboundVisitMessage>();

            IFinancePlanApplicationService svc = this.CreateServiceForOnFinancePlanCreateTests(newFinancePlan, financePlan, visits, outboundVisitMessagesSent);

            financePlan.SetFinancePlanStatus(new FinancePlanStatus { FinancePlanStatusId = (int)FinancePlanStatusEnum.Canceled }, "");
            financePlan.PaymentAmount = 0m;
            financePlan.WriteOffInterestDue();
            financePlan.OnFinancePlanClose();
            financePlan.ReconfiguredFinancePlan = newFinancePlan;
            newFinancePlan.OriginalFinancePlan = financePlan;

            svc.OnFinancePlanCreated(0, newFinancePlan.FinancePlanId);

            //should just send none added message
            Assert.AreEqual(0, outboundVisitMessagesSent.Count(x => x.VpOutboundVisitMessageType == VpOutboundVisitMessageTypeEnum.VisitAddedToFinancePlan));
        }

        [Test]
        public void OnFinancePlanCreate_NewPlan_OutboundVisitMessage_SendsOne()
        {
            const decimal totalBalance = 1000m;
            const decimal nPayments = 10;
            const decimal paymentAmount = totalBalance / nPayments;
            IList<Tuple<Visit, IList<VisitTransaction>>> visits = new List<Tuple<Visit, IList<VisitTransaction>>>
            {
                VisitFactory.GenerateActiveVisit(totalBalance, 1)
            };
            FinancePlan financePlan = FinancePlanFactory.CreateFinancePlan(visits.First().ToListOfOne(), 1, paymentAmount);
            IList<OutboundVisitMessage> outboundVisitMessagesSent = new List<OutboundVisitMessage>();

            IFinancePlanApplicationService svc = this.CreateServiceForOnFinancePlanCreateTests(financePlan, null, visits, outboundVisitMessagesSent);

            svc.OnFinancePlanCreated(1, financePlan.FinancePlanId);

            //should just send one added message
            Assert.AreEqual(1, outboundVisitMessagesSent.Count(x => x.VpOutboundVisitMessageType == VpOutboundVisitMessageTypeEnum.VisitAddedToFinancePlan));
        }

        private IFinancePlanApplicationService CreateServiceForOnFinancePlanCreateTests(FinancePlan newFinancePlan, FinancePlan cancelledFinancePlan, IList<Tuple<Visit, IList<VisitTransaction>>> visits, IList<OutboundVisitMessage> outboundVisitMessagesSent)
        {
            FinancePlanApplicationServiceMockBuilder mockBuilder = new FinancePlanApplicationServiceMockBuilder();
            mockBuilder.FinancePlanServiceMock.Setup(x => x.GetFinancePlan(It.IsAny<Guarantor>(), It.IsAny<int>())).Returns(newFinancePlan);
            mockBuilder.FinancePlanServiceMock.Setup(x => x.GetAllFinancePlansForGuarantor(It.IsAny<Guarantor>(), It.IsAny<IList<FinancePlanStatusEnum>>())).Returns(
                new List<FinancePlan> { cancelledFinancePlan }
            );

            mockBuilder.SessionMock = new SessionMockBuilder()
                .UseSynchronizations() // this lets the RegisterPostCommits fire
                .CreateMock();
            mockBuilder.VisitServiceMock.Setup(x => x.GetVisits(It.IsAny<int>(), It.IsAny<IList<int>>())).Returns(visits.Select(x => x.Item1).ToList());

            mockBuilder.ApplicationServiceCommonServiceMockBuilder.BusMock.Setup(x => x.PublishMessage(It.IsAny<OutboundVisitMessageList>())).Returns<OutboundVisitMessageList>(m =>
            {
                outboundVisitMessagesSent.AddRange(m.Messages);
                return Task.CompletedTask;
            });

            return mockBuilder.CreateService();
        }

        [Test]
        public void InterestReallocation_ForSpecificFinancePlanVisit()
        {
            //setup the fp with payments and assessments to be reversed, make sure they're only applied to whatever visits, and the right amount
            decimal paymentAmount = 150m;

            IList<Tuple<Visit, IList<VisitTransaction>>> visits = new List<Tuple<Visit, IList<VisitTransaction>>>
            {
                VisitFactory.GenerateActiveVisit(1000m, 2),
                VisitFactory.GenerateActiveVisit(2000m, 2)
            };


            FinancePlan financePlan = FinancePlanFactory.CreateFinancePlan(visits, 0, paymentAmount, DateTime.UtcNow.AddMonths(-1), 0.05m);
            Guarantor guarantor = GuarantorFactory.GenerateOnline();
            financePlan.VpGuarantor = guarantor;

            IDictionary<FinancePlanVisit, PrincipalInterest> fpvPrincipalInterestDictionary = new Dictionary<FinancePlanVisit, PrincipalInterest>();

            this.RunStatementPaymentCycle(true, financePlan, fpvPrincipalInterestDictionary);
            this.RunStatementPaymentCycle(false, financePlan, fpvPrincipalInterestDictionary);

            IFinancePlanApplicationService service = this.CreateServiceForPaymentReversalTests(financePlan, visits.First().Item1);
            decimal totalInterestAssessed = financePlan.FinancePlanVisits.First().InterestAssessed;

            //
            service.ReallocateInterestToPrincipal(guarantor.User.VisitPayUserId, guarantor.VpGuarantorId, financePlan.FinancePlanId, totalInterestAssessed, financePlan.FinancePlanVisits.First().FinancePlanVisitId);

            FinancePlanVisit targetFinancePlanVisit = financePlan.FinancePlanVisits.First();
            Visit targetVisit = visits.First().Item1;
            IList<FinancePlanVisit> theOtherFinancePlanVisits = financePlan.FinancePlanVisits.Where(x => x.FinancePlanVisitId != targetFinancePlanVisit.FinancePlanVisitId).ToList();
            IList<Visit> theOtherVisits = visits.Select(x => x.Item1).Where(x => x.VisitId != targetVisit.VisitId).ToList();

            Assert.AreEqual(0m, targetFinancePlanVisit.FinancePlanVisitInterestDues.Sum(x => x.InterestDue));
            Assert.Greater(theOtherFinancePlanVisits.Sum(x => x.FinancePlanVisitInterestDues.Sum(y => y.InterestDue)), 0m);

            //should be the inital balance - principal payments
            decimal expectedOtherVisitsBalance = theOtherVisits.Sum(x => x.CurrentBalance) - theOtherFinancePlanVisits.Sum(x => fpvPrincipalInterestDictionary[x].PrincipalAmount);
            Assert.AreEqual(expectedOtherVisitsBalance, theOtherFinancePlanVisits.Sum(x => x.PrincipalBalance));

            //should be the inital balance - principal and interest payments
            decimal expectedTargetVisitEndingBalance = targetVisit.CurrentBalance - (fpvPrincipalInterestDictionary[targetFinancePlanVisit].PrincipalAmount + fpvPrincipalInterestDictionary[targetFinancePlanVisit].InterestAmount);
            Assert.AreEqual(expectedTargetVisitEndingBalance, financePlan.FinancePlanVisits.First().PrincipalBalance);

        }

        [Test]
        public void FinancePlanVisitRefundInterestMessage_Processes()
        {
            //setup the fp with payments and assessments to be reversed, make sure they're only applied to whatever visits, and the right amount
            decimal paymentAmount = 150m;

            List<Visit> visits = new List<Tuple<Visit, IList<VisitTransaction>>>
            {
                VisitFactory.GenerateActiveVisit(1000m, 2),
                VisitFactory.GenerateActiveVisit(2000m, 2)
            }.Select(x => x.Item1).ToList();

            FinancePlan financePlan = FinancePlanFactory.CreateFinancePlan(visits, 0, paymentAmount, DateTime.UtcNow.AddMonths(-1), 0.05m);
            Guarantor guarantor = GuarantorFactory.GenerateOnline();
            financePlan.VpGuarantor = guarantor;

            IDictionary<FinancePlanVisit, PrincipalInterest> fpvPrincipalInterestDictionary = new Dictionary<FinancePlanVisit, PrincipalInterest>();

            this.RunStatementPaymentCycle(true, financePlan, fpvPrincipalInterestDictionary);
            this.RunStatementPaymentCycle(false, financePlan, fpvPrincipalInterestDictionary);

            Visit targetVisit = visits.First();
            FinancePlanVisit targetFinancePlanVisit = financePlan.FinancePlanVisits.First(x => x.VisitId == targetVisit.VisitId);

            IFinancePlanApplicationService service = this.CreateServiceForPaymentReversalTests(financePlan, visits.First());
            decimal totalInterestAssessed = targetFinancePlanVisit.InterestAssessed;
            decimal interestPaid = targetFinancePlanVisit.FinancePlanVisitInterestDues.Where(x => x.FinancePlanVisitInterestDueType == FinancePlanVisitInterestDueTypeEnum.InterestPayment).Sum(x => x.InterestDue);
            decimal principalAmount = targetFinancePlanVisit.FinancePlanVisitPrincipalAmounts.Sum(x => x.Amount);

            //Pre asserts 
            Assert.True(totalInterestAssessed > 0m, "Test setup is incorrect. No interest assessed for FinancePlanVisit");
            Assert.True(interestPaid < 0m, "Test setup is incorrect. No interest paid for FinancePlanVisit");
            Assert.True(principalAmount > 0m, "Test setup is incorrect. No interest assessed for FinancePlanVisit");

            FinancePlanVisitRefundInterestMessage refundInterestMessage = new FinancePlanVisitRefundInterestMessage
            {
                VpGuarantorId = guarantor.VpGuarantorId,
                VisitId = targetVisit.VisitId
            };
            service.ProcessFinancePlanVisitRefundInterestMessage(refundInterestMessage);

            decimal totalInterestAssessedAfter = targetFinancePlanVisit.InterestAssessed;
            Assert.AreEqual(0m, totalInterestAssessedAfter);

            //the expectation is the principal Amount after refund is the result of principalAmount - interestPaid
            decimal expectedPrincipalAmount = principalAmount - decimal.Negate(interestPaid);
            decimal principalAmountAfter = targetFinancePlanVisit.FinancePlanVisitPrincipalAmounts.Sum(x => x.Amount);

            Assert.AreEqual(expectedPrincipalAmount, principalAmountAfter);
        }

        [Test]
        public void ReallocateInterestToPrincipal_AmountToWriteOffGreaterThanZero_AddsFinancePlanVisitInterestDueItems()
        {
            const int financePlanVisitId = 1234;
            const int vpGuarantorId = 2;
            FinancePlan fakeFinancePlan = new FinancePlan { FinancePlanId = 5678 };
            Mock<IFinancePlanService> financePlanSvcMock = new Mock<IFinancePlanService>();

            FinancePlanVisitInterestDue fakeFinancePlanVisitInterestDue = new FinancePlanVisitInterestDue
            {
                FinancePlanVisitInterestDueType = FinancePlanVisitInterestDueTypeEnum.InterestAssessment,
                InsertDate = DateTime.Today.AddDays(-1),
                InterestDue = 15m
            };

            FinancePlanVisit fakeFinancePlanVisit = new FinancePlanVisit { FinancePlanVisitId = financePlanVisitId };
            DateTime? startDate = DateTime.Today.AddDays(-10);
            DateTime endDate = DateTime.Today.AddDays(10);
            fakeFinancePlanVisit.FinancePlanVisitInterestDues = new List<FinancePlanVisitInterestDue> { fakeFinancePlanVisitInterestDue };
            fakeFinancePlanVisit.InterestAssessedForDateRange(startDate, endDate);
            fakeFinancePlanVisit.FinancePlan = fakeFinancePlan;
            fakeFinancePlan.FinancePlanVisits = new List<FinancePlanVisit> { fakeFinancePlanVisit };

            IFinancePlanApplicationService financePlanApplicationService =
                new FinancePlanApplicationServiceMockBuilder().Setup(builder =>
                {
                    builder.FinancePlanService = financePlanSvcMock.Object;
                    builder.FinancePlanServiceMock = financePlanSvcMock;
                    builder.FinancePlanServiceMock
                        .Setup(s => s.GetFinancePlan(It.IsAny<Guarantor>(), fakeFinancePlan.FinancePlanId))
                        .Returns(fakeFinancePlan);
                }).CreateService();

            financePlanApplicationService
                .ReallocateInterestToPrincipal(10, vpGuarantorId, fakeFinancePlan.FinancePlanId, 10m, financePlanVisitId);

            Assert.True(fakeFinancePlanVisit.FinancePlan.FinancePlanVisitInterestDueItems.Any());
            Assert.True(fakeFinancePlanVisit.FinancePlan.FinancePlanVisitInterestDueItems.Count.Equals(1));
        }

        [TestCase(3, false, true)]//Eligible
        [TestCase(3, true, true)] //Eligible, has extra statement that assesses interest (test that it gets cleared)
        [TestCase(2, true, false)]//Not eligible, (FinancePlanDoesNotMeetRefundTriggerCriteria)
        public void ApplyInterestRefundIncentive(int numberOfStatements, bool assessInterest, bool shouldApplyRefund)
        {
            decimal paymentAmount = 500m;

            IList<Tuple<Visit, IList<VisitTransaction>>> visits = new List<Tuple<Visit, IList<VisitTransaction>>>
            {
                VisitFactory.GenerateActiveVisit(2500m, 2)
            };

            FinancePlan financePlan = FinancePlanFactory.CreateFinancePlan(visits, 0, paymentAmount, DateTime.UtcNow.AddMonths(-1), 0.05m);
            Guarantor guarantor = GuarantorFactory.GenerateOnline();
            financePlan.VpGuarantor = guarantor;

            IDictionary<FinancePlanVisit, PrincipalInterest> fpvPrincipalInterestDictionary = new Dictionary<FinancePlanVisit, PrincipalInterest>();

            for (int x = 0; x < numberOfStatements; x++)
            {
                this.RunStatementPaymentCycle(true, financePlan, fpvPrincipalInterestDictionary);
            }

            if (assessInterest)
            {
                this.RunStatementPaymentCycle(false, financePlan, fpvPrincipalInterestDictionary);
                Assert.IsTrue(financePlan.InterestAssessed > 0m);
            }

            FinancePlanApplicationServiceMockBuilder mockService = this.MockServiceForPaymentReversalTests(financePlan, visits.First().Item1);
            FinancePlanIncentiveServiceMockBuilder financePlanIncentiveServiceMockBuilder = new FinancePlanIncentiveServiceMockBuilder()
                .WithFeatureEnabled()
                .WithDefaultFeatureConfiguration();
            mockService.FinancePlanIncentiveService = financePlanIncentiveServiceMockBuilder.CreateService();

            IFinancePlanApplicationService financePlanApplicationService = mockService.CreateService();

            financePlanApplicationService.ApplyInterestRefundIncentive(financePlan.FinancePlanId);

            string inputParams = $"Number Of Statements:{numberOfStatements}, AssessInterest:{assessInterest}, Should Apply Refund {shouldApplyRefund}";

            if (shouldApplyRefund)
            {
                Assert.AreEqual(0m, financePlan.InterestAssessed, $"Assessed Interest was not removed. Params ${inputParams}");
                Assert.AreEqual(0m, financePlan.InterestPaidForDate(DateTime.UtcNow), $"Interest Paid was not removed. Params ${inputParams}");
                Assert.AreEqual(1000m, financePlan.CurrentFinancePlanBalance, $"Balance does not reflect interest refund. Params ${inputParams}");
            }
            else
            {
                Assert.IsTrue(financePlan.InterestPaidForDate(DateTime.UtcNow) < 0, $"Interest Paid was removed without being eligible. Params ${inputParams}");
                Assert.IsTrue(financePlan.CurrentFinancePlanBalance > 1000m, $"Balance was reduced without being eligible. Params ${inputParams}");
                if (assessInterest)
                {
                    Assert.IsTrue(financePlan.InterestAssessed > 0m, $"Interest Assessed was removed without being eligible. Params ${inputParams}");
                }
            }

        }

        private void RunStatementPaymentCycle(bool processPayment, FinancePlan financePlan, IDictionary<FinancePlanVisit, PrincipalInterest> fpvPrincipalInterestDictionary)
        {
            int statementId = financePlan.FinancePlanAmountsDue.Max(x => x.VpStatementId) ?? 0;
            financePlan.AddFinancePlanAmountsDueStatement(150m, DateTime.UtcNow, ++statementId);

            decimal interestRate = 0.05m;
            foreach (FinancePlanVisit financePlanVisit in financePlan.FinancePlanVisits)
            {
                decimal interestDue = Math.Round(financePlanVisit.PrincipalBalance * interestRate / 12, 2, MidpointRounding.ToEven);
                financePlanVisit.AddFinancePlanVisitInterestDue(interestDue, null, FinancePlanVisitInterestDueTypeEnum.InterestAssessment);

                if (processPayment)
                {
                    PaymentAllocation principalPaymentAllocation = new PaymentAllocationBuilder()
                        .WithDefaultValues(financePlanVisit.VisitId)
                        .AsRecurringFinancePlanPayment()
                        .WithAmount((financePlan.PaymentAmount / financePlan.FinancePlanVisits.Count - interestDue));
                    PaymentAllocation interestPaymentAllocation = new PaymentAllocationBuilder()
                        .WithDefaultValues(financePlanVisit.VisitId)
                        .AsRecurringFinancePlanPayment()
                        .WithAmount(interestDue)
                        .AsInterest();


                    financePlan.OnPaymentAllocation(-principalPaymentAllocation.ActualAmount, DateTime.UtcNow, principalPaymentAllocation);
                    financePlan.OnPaymentAllocation(-interestPaymentAllocation.ActualAmount, DateTime.UtcNow, interestPaymentAllocation);

                    //set projection values
                    financePlan.FinancePlanAmountsDue.First(x => x.PaymentAllocationId == principalPaymentAllocation.PaymentAllocationId).PaymentAllocationActualAmount = principalPaymentAllocation.ActualAmount;
                    financePlan.FinancePlanAmountsDue.First(x => x.PaymentAllocationId == principalPaymentAllocation.PaymentAllocationId).PaymentAllocationType = PaymentAllocationTypeEnum.VisitPrincipal;

                    financePlan.FinancePlanAmountsDue.First(x => x.PaymentAllocationId == interestPaymentAllocation.PaymentAllocationId).PaymentAllocationActualAmount = interestPaymentAllocation.ActualAmount;
                    financePlan.FinancePlanAmountsDue.First(x => x.PaymentAllocationId == interestPaymentAllocation.PaymentAllocationId).PaymentAllocationType = PaymentAllocationTypeEnum.VisitInterest;

                    financePlanVisit.FinancePlanAmountDues = financePlan.FinancePlanAmountsDue.Where(x => x.FinancePlanVisit?.FinancePlanVisitId == financePlanVisit.FinancePlanVisitId).ToList();

                    PrincipalInterest principalInterest = fpvPrincipalInterestDictionary.ContainsKey(financePlanVisit) ? fpvPrincipalInterestDictionary[financePlanVisit] : new PrincipalInterest(0, 0);

                    if (!fpvPrincipalInterestDictionary.ContainsKey(financePlanVisit))
                    {
                        fpvPrincipalInterestDictionary.Add(financePlanVisit,
                            new PrincipalInterest(
                                    principalInterest.PrincipalAmount + principalPaymentAllocation.ActualAmount,
                                    principalInterest.InterestAmount + interestPaymentAllocation.ActualAmount
                                )
                            );
                    }
                    else
                    {
                        fpvPrincipalInterestDictionary[financePlanVisit] = new PrincipalInterest(
                                principalInterest.PrincipalAmount + principalPaymentAllocation.ActualAmount,
                                principalInterest.InterestAmount + interestPaymentAllocation.ActualAmount
                            );
                    }
                }
            }
        }

        private IFinancePlanApplicationService CreateServiceForPaymentReversalTests(FinancePlan financePlan, Visit targetVisit)
        {
            return this.MockServiceForPaymentReversalTests(financePlan, targetVisit).CreateService();
        }

        private FinancePlanApplicationServiceMockBuilder MockServiceForPaymentReversalTests(FinancePlan financePlan, Visit targetVisit)
        {
            FinancePlanApplicationServiceMockBuilder mockBuilder = new FinancePlanApplicationServiceMockBuilder();
            mockBuilder.Setup(builder =>
            {
                builder.PaymentReversalApplicationService = new PaymentReversalApplicationServiceMockBuilder()
                    .Setup(
                        x =>
                        {
                            x.PaymentService = new PaymentServiceMockBuilder().Setup(y => y.PaymentMethodsServiceMock.Setup(z => z.GetPrimaryPaymentMethod(It.IsAny<int>(), It.IsAny<bool>())).Returns(new Ivh.Domain.FinanceManagement.Entities.PaymentMethod())).CreatePaymentService();
                            x.GuarantorServiceMock.Setup(y => y.GetGuarantor(It.IsAny<int>())).Returns(financePlan.VpGuarantor);

                            x.PaymentReversalService = new PaymentReversalServiceMockBuilder()
                                .Setup(y =>
                                {
                                    y.PaymentAllocationService = new PaymentAllocationServiceMockBuilder()
                                        .Setup(
                                            z =>
                                            {
                                                //gets the list of visitids to reallocate
                                                z.PaymentVisitRepositoryMock.Setup(m => m.GetPaymentVisits(It.IsAny<IList<int>>())).Returns(new List<PaymentVisit> { Mapper.Map<PaymentVisit>(targetVisit) });
                                                Mock<Client> clientMock = new Mock<Client>();
                                                clientMock.Setup(c => c.BillingApplicationHBAllocationPriority).Returns(1);
                                                clientMock.Setup(c => c.BillingApplicationPBAllocationPriority).Returns(2);
                                                clientMock.Setup(c => c.BillingApplicationHBAllocationPercentage).Returns(50);
                                                clientMock.Setup(c => c.BillingApplicationPBAllocationPercentage).Returns(50);

                                                z.ClientService.Setup(cs => cs.GetClient()).Returns(clientMock.Object);
                                            })
                                        .GetPaymentAllocationService(typeof(BillingApplicationPaymentAllocationService));

                                }).CreateService();
                        }
                    ).CreateService();

                builder.FinancePlanServiceMock.Setup(x => x.GetFinancePlan(It.IsAny<Guarantor>(), It.IsAny<int>())).Returns(financePlan);
                builder.FinancePlanServiceMock.Setup(x => x.GetFinancePlanById(It.IsAny<int>())).Returns(financePlan);
                FinancePlanVisit targetFinancePlanVisit = financePlan.FinancePlanVisits.FirstOrDefault(x => x.VisitId == targetVisit.VisitId);
                builder.FinancePlanServiceMock.Setup(x => x.GetVisitOnAnyFinancePlan(It.IsAny<int>())).Returns(targetFinancePlanVisit.ToListOfOne());
            });

            return mockBuilder;
        }

    }

    public class PrincipalInterest
    {
        public PrincipalInterest(decimal principalAmount, decimal interestAmount)
        {
            this.PrincipalAmount = principalAmount;
            this.InterestAmount = interestAmount;
        }
        public decimal PrincipalAmount { get; set; }
        public decimal InterestAmount { get; set; }
    }
}

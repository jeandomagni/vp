﻿namespace Ivh.Application.Tests.FinanceManagement
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Application.FinanceManagement.ApplicationServices;
    using Application.FinanceManagement.Common.Dtos;
    using Application.FinanceManagement.Common.Interfaces;
    using Domain.Guarantor.Entities;
    using Domain.Visit.Entities;
    using Domain.FinanceManagement.FinancePlan.Entities;
    using Domain.FinanceManagement.FinancePlan.Interfaces;
    using Domain.FinanceManagement.Statement.Entities;
    using Common.Base.Utilities.Extensions;
    using Common.EventJournal;
    using Common.State.Interfaces;
    using Common.Tests.Builders;
    using Common.Tests.Helpers;
    using Common.Tests.Helpers.FinancePlan;
    using Common.Tests.Helpers.VpStatement;
    using Common.VisitPay.Enums;
    using Common.VisitPay.Messages.FinanceManagement;
    using Content.Common.Dtos;
    using Domain.Settings.Entities;
    using Domain.User.Entities;
    using Moq;
    using NUnit.Framework;

    [TestFixture]
    public class ReconfigurationApplicationServiceTests : ApplicationTestBase
    {
        [Test]
        public async Task AmortizeReconfiguration_GracePeriod_MinAmount()
        {
            await this.RunAmortizationTest(1101.22m, true, InterestCalculationMethodEnum.Daily);
            await this.RunAmortizationTest(1101.22m, true, InterestCalculationMethodEnum.Monthly);
        }

        [Test]
        public async Task AmortizeReconfiguration_GracePeriod_MaxAmount()
        {
            await this.RunAmortizationTest(2148.83m, true, InterestCalculationMethodEnum.Daily);
            await this.RunAmortizationTest(2148.83m, true, InterestCalculationMethodEnum.Monthly);
        }

        [Test]
        public async Task AmortizeReconfiguration_GracePeriod_SomeAmount()
        {
            await this.RunAmortizationTest(1100m, true, InterestCalculationMethodEnum.Daily);
            await this.RunAmortizationTest(1100m, true, InterestCalculationMethodEnum.Monthly);
        }

        [Test]
        public async Task AmortizeReconfiguration_AwaitingStatement_MinAmount()
        {
            await this.RunAmortizationTest(1102.22m, false, InterestCalculationMethodEnum.Daily);
            await this.RunAmortizationTest(1102.22m, false, InterestCalculationMethodEnum.Monthly);
        }

        [Test]
        public async Task AmortizeReconfiguration_AwaitingStatement_MaxAmount()
        {
            await this.RunAmortizationTest(2148.83m, false, InterestCalculationMethodEnum.Daily);
            await this.RunAmortizationTest(2148.83m, false, InterestCalculationMethodEnum.Monthly);
        }

        [Test]
        public async Task AmortizeReconfiguration_AwaitingStatement_SomeAmount()
        {
            await this.RunAmortizationTest(1100m, false, InterestCalculationMethodEnum.Daily);
            await this.RunAmortizationTest(1100m, false, InterestCalculationMethodEnum.Monthly);
        }
        
        public async Task RunAmortizationTest(decimal monthlyPaymentAmount, bool isGracePeriod, InterestCalculationMethodEnum interestCalculationMethod)
        {
            Tuple<Visit, IList<VisitTransaction>> visitAndTransactions = CreateVisit();

            if (isGracePeriod)
            {
                AddVisitTransaction(visitAndTransactions, new DateTime(2017, 01, 01), 104.59m, VpTransactionTypeEnum.VppInterestCharge);
            }

            IList<Tuple<Visit, IList<VisitTransaction>>> listOfVisits = visitAndTransactions.ToListOfOne();

            FinancePlan financePlan = FinancePlanFactory.CreateFinancePlan(listOfVisits, 0, monthlyPaymentAmount, new DateTime(2017, 03, 02));
            financePlan.InterestCalculationMethod = interestCalculationMethod;
            financePlan.CreatedVpStatement = VpStatementFactory.CreateStatement(listOfVisits.ToList());
            financePlan.FinancePlanId = 1;
            FinancePlan financePlanToReconfigure = FinancePlanFactory.CreateFinancePlan(listOfVisits, 0, monthlyPaymentAmount, new DateTime(2017, 03, 02));
            financePlanToReconfigure.CreatedVpStatement = VpStatementFactory.CreateStatement(listOfVisits.ToList());
            financePlanToReconfigure.FinancePlanId = 1;

            IInterestService interestService = new InterestServiceMockBuilder().SetupWithStLukesRates(builder =>
            {
                builder.ApplicationSettingsServiceMock.Setup(x => x.IsClientApplication).Returns(() => new ApplicationSetting<bool>(() => new Lazy<bool>(() => false)));
            }).CreateService();

            IAmortizationService amortizationService = new AmortizationServiceMockBuilder().Setup(builder =>
            {
                builder.InterestService = interestService;
            }).CreateService();

            FinancePlanOfferServiceMockBuilder financePlanOfferServiceMockBuilder = new FinancePlanOfferServiceMockBuilder().Setup(builder =>
            {
                builder.AmortizationService = amortizationService;
                builder.InterestService = interestService;
                builder.MinimumPaymentTierRepositoryMock.Setup(x => x.GetQueryable()).Returns(() => new List<FinancePlanMinimumPaymentTier>
                {
                    new FinancePlanMinimumPaymentTier
                    {
                        ExisitingRecurringPaymentTotal = 0,
                        FinancePlanMinimumPaymentTierId = 1,
                        FinancePlanOfferSetType = new FinancePlanOfferSetType {FinancePlanOfferSetTypeId = 1},
                        MinimumPayment = 25m
                    },
                    new FinancePlanMinimumPaymentTier
                    {
                        ExisitingRecurringPaymentTotal = 50,
                        FinancePlanMinimumPaymentTierId = 2,
                        FinancePlanOfferSetType = new FinancePlanOfferSetType {FinancePlanOfferSetTypeId = 1},
                        MinimumPayment = 10m
                    }
                }.AsQueryable());
            });

            IFinancePlanService financePlanService = new FinancePlanServiceMockBuilder().Setup(builder =>
            {
                builder.AmortizationService = amortizationService;
                builder.FinancePlanOfferService = financePlanOfferServiceMockBuilder.CreateService();
                builder.FinancePlanRepositoryMock.Setup(x => x.GetFinancePlan(It.IsAny<int>(), It.IsAny<int>())).Returns(financePlanToReconfigure);
                builder.FinancePlanRepositoryMock.Setup(x => x.GetAllOpenFinancePlans(It.IsAny<int>())).Returns(financePlanToReconfigure.ToListOfOne());
                builder.FinancePlanRepositoryMock.Setup(x => x.GetAllOpenFinancePlans(It.IsAny<IList<int>>())).Returns(financePlanToReconfigure.ToListOfOne());
                builder.FinancePlanRepositoryMock.Setup(x => x.AreVisitsOnActiveOrPendingFinancePlan(It.IsAny<IList<Visit>>(), null)).Returns(() => new List<Visit>());
            }).CreateService();

            DateTime paymentDueDate = DateTime.UtcNow.AddDays(isGracePeriod ? 1 : -1);
            DateTime nextStatementDate = paymentDueDate.AddMonths(1).ToPaymentDueDateForDay(paymentDueDate.Day).AddDays(-21);

            Guarantor guarantor = new Guarantor { VpGuarantorId = 1, NextStatementDate = nextStatementDate, PaymentDueDay = paymentDueDate.Day};

            Mock<VpStatement> statement = new Mock<VpStatement>();
            statement.Setup(x => x.IsGracePeriod).Returns(isGracePeriod);
            statement.Setup(x => x.PaymentDueDate).Returns(paymentDueDate);

            IReconfigureFinancePlanApplicationService service = new ReconfigureFinancePlanApplicationServiceMockBuilder().Setup(builder =>
            {
                builder.ApplicationServiceCommonServiceMockBuilder.ClientServiceMock.Setup(x => x.GetClient()).Returns(() =>
                {
                    Mock<Client> clientMock = new Mock<Client>();
                    clientMock.Setup(x => x.InterestCalculationMethod).Returns(InterestCalculationMethodEnum.Monthly);
                    return clientMock.Object;
                });
                builder.AmortizationService = amortizationService;
                builder.ContentApplicationServiceMock.Setup(x => x.GetCurrentVersionAsync(It.IsAny<CmsRegionEnum>(), It.IsAny<bool>(), null)).ReturnsAsync(new CmsVersionDto());
                builder.FinancePlanService = financePlanService;
                builder.GuarantorServiceMock.Setup(x => x.GetGuarantor(It.IsAny<int>())).Returns(guarantor);
                builder.VpStatementServiceMock.Setup(x => x.GetMostRecentStatement(It.IsAny<Guarantor>())).Returns(statement.Object);
            }).CreateService();

            FinancePlanCalculationParametersDto fpParametersDto = FinancePlanCalculationParametersDto.CreateForMonthlyPayment(guarantor.VpGuarantorId, statement.Object.VpStatementId, monthlyPaymentAmount, null, false);

            FinancePlanCalculationParameters fpParameters = ((ReconfigureFinancePlanApplicationService) service).MapFinancePlanCalculationParameters(
                fpParametersDto,
                financePlanToReconfigure,
                guarantor,
                statement.Object);
            
            // verify that reconfig fp is given 1 interest free period
            Assert.AreEqual(0m, fpParameters.Visits.Sum(x => x.OverrideInterestRate));
            Assert.AreEqual(financePlanToReconfigure.ActiveFinancePlanVisits.Count, fpParameters.Visits.Sum(x => x.OverridesRemaining));

            FinancePlanOffer financePlanOffer = financePlanService.GetOfferForReconfiguration(fpParameters, financePlanToReconfigure, true);
            CalculateFinancePlanTermsResponseDto termsResponseDto = await service.GetTermsByMonthlyPaymentAmountAsync(financePlanToReconfigure.FinancePlanId, fpParametersDto);
            FinancePlanTermsDto termsDto = termsResponseDto.Terms;

            // verify the durations are within the offer range
            Assert.GreaterOrEqual(financePlanOffer.DurationRangeEnd, termsDto.NumberMonthlyPayments);
            Assert.LessOrEqual(financePlanOffer.DurationRangeStart, termsDto.NumberMonthlyPayments);

            // verify that it amortizes
            VerifyAmortization(financePlanToReconfigure, termsDto, amortizationService, monthlyPaymentAmount, financePlanOffer, visitAndTransactions, fpParameters);
        }

        private static Tuple<Visit, IList<VisitTransaction>> CreateVisit()
        {
            Tuple<Visit, IList<VisitTransaction>> visitAndTransactions = new Tuple<Visit, IList<VisitTransaction>>(
                new Visit { VisitId = 1 }, new List<VisitTransaction>());

            AddVisitTransaction(visitAndTransactions, new DateTime(2017, 01, 01), 30000m, VpTransactionTypeEnum.HsCharge);
            AddVisitTransaction(visitAndTransactions, new DateTime(2017, 01, 01), -1310.70m, VpTransactionTypeEnum.VppPrincipalPayment);

            AddVisitTransaction(visitAndTransactions, new DateTime(2017, 01, 01), 119.54m, VpTransactionTypeEnum.VppInterestCharge);

            AddVisitTransaction(visitAndTransactions, new DateTime(2017, 01, 01), -119.54m, VpTransactionTypeEnum.VppInterestPayment);
            AddVisitTransaction(visitAndTransactions, new DateTime(2017, 01, 01), -1191.16m, VpTransactionTypeEnum.VppPrincipalPayment);

            AddVisitTransaction(visitAndTransactions, new DateTime(2017, 01, 01), 114.58m, VpTransactionTypeEnum.VppInterestCharge);

            AddVisitTransaction(visitAndTransactions, new DateTime(2017, 01, 01), -114.58m, VpTransactionTypeEnum.VppInterestPayment);
            AddVisitTransaction(visitAndTransactions, new DateTime(2017, 01, 01), -1196.12m, VpTransactionTypeEnum.VppPrincipalPayment);

            AddVisitTransaction(visitAndTransactions, new DateTime(2017, 01, 01), 109.59m, VpTransactionTypeEnum.VppInterestCharge);

            AddVisitTransaction(visitAndTransactions, new DateTime(2017, 01, 01), -109.59m, VpTransactionTypeEnum.VppInterestPayment);
            AddVisitTransaction(visitAndTransactions, new DateTime(2017, 01, 01), -1201.11m, VpTransactionTypeEnum.VppPrincipalPayment);

            visitAndTransactions.Item1.SetCurrentBalance(visitAndTransactions.Item2.Sum(x => x.VisitTransactionAmount.TransactionAmount));

            ((IEntity<VisitStateEnum>)visitAndTransactions.Item1).SetState(VisitStateEnum.Active, "");

            return visitAndTransactions;
        }

        private static void AddVisitTransaction(Tuple<Visit, IList<VisitTransaction>> visit, DateTime date, decimal amount, VpTransactionTypeEnum type)
        {
            VisitTransaction visitTransaction = new VisitTransaction
            {
                PostDate = date,
                VpTransactionType = new VpTransactionType
                {
                    VpTransactionTypeId = (int)type
                }
            };

            visitTransaction.VisitTransactionAmount = new VisitTransactionAmount
            {
                InsertDate = date,
                TransactionAmount = amount,
                VisitTransaction = visitTransaction
            };
            if (visit.Item2 == null)
            {
                Tuple<Visit, IList<VisitTransaction>> visit2 = new Tuple<Visit, IList<VisitTransaction>>(visit.Item1, new List<VisitTransaction>());
                visit = visit2;
            }
            visit.Item2.Add(visitTransaction);
        }

        private static void VerifyAmortization(
            FinancePlan financePlan,
            FinancePlanTermsDto termsDto,
            IAmortizationService amortizationService,
            decimal monthlyPaymentAmount,
            FinancePlanOffer financePlanOffer,
            Tuple<Visit, IList<VisitTransaction>> visit,
            FinancePlanCalculationParameters fpParameters)
        {
            IList<IFinancePlanVisitBalance> visits = new List<IFinancePlanVisitBalance>
            {
                new FinancePlanSetupVisit(visit.Item1.VisitId, visit.Item1.SourceSystemKey, visit.Item1.BillingSystemId ?? default(int), visit.Item1.CurrentBalance, 0, 0, visit.Item1.CurrentVisitState, visit.Item1.BillingApplication, visit.Item1.InterestZero, 0m, 1)
            };

            DateTime dateToProcess = new DateTime(2018, 7, 31);

            List<AmortizationMonth> amortizationMonths = amortizationService.GetAmortizationMonthsForAmounts(
                monthlyPaymentAmount,
                visits,
                financePlanOffer.InterestRate,
                financePlan.CreatedVpStatement.IsGracePeriod ? FirstInterestPeriodEnum.PushOnePeriod : FirstInterestPeriodEnum.NoAction,
                dateToProcess,
                fpParameters.InterestCalculationMethod
                ).ToList();

            // create reconfig plan
            FinancePlan reconfigFp = new FinancePlan
            {
                CreatedVpStatement = financePlan.CreatedVpStatement,
                OriginationDate = DateTime.UtcNow,
                OriginalFinancePlan = financePlan,
                PaymentAmount = termsDto.MonthlyPaymentAmount,
                FinancePlanOffer = financePlanOffer
            };

            reconfigFp.OriginalFinancePlan = financePlan;
            reconfigFp.AddFinancePlanVisits(fpParameters, financePlan);
            reconfigFp.FinancePlanInterestRateHistory.Add(new FinancePlanInterestRateHistory { InterestRate = termsDto.InterestRate });

            FinancePlanVisit fpVisit = reconfigFp.FinancePlanVisits.First();

            decimal interestPaid = 0;
            decimal principalPaid = 0;
            AmortizationVisitDateBalances visitDateBalances = null;
            foreach (AmortizationMonth month in amortizationMonths)
            {
                List<AmortizationMonth> temp = amortizationService.GetAmortizationMonthsForOriginatedFinancePlan(reconfigFp, dateToProcess.AddMonths(month.MonthNumber), visitDateBalances).ToList();

                if (temp.Any())
                {
                    interestPaid += temp.First().InterestAmount;
                    principalPaid += temp.First().PrincipalAmount;
                }
                decimal interestAmount = temp.First().InterestAmount, principalAmount = temp.First().PrincipalAmount;
                if (interestAmount != month.InterestAmount || principalAmount != month.PrincipalAmount)
                {
                    throw new Exception();
                }

                // statement
                bool adjustInterestRateHistoryDates = fpVisit.HasOverride();

                fpVisit.AddFinancePlanVisitInterestAssessment(month.InterestAmount, reconfigFp.CurrentInterestRate, DateTime.UtcNow.AddMonths(month.MonthNumber));

                if (adjustInterestRateHistoryDates)
                {
                    int adjust = 0;
                    foreach (FinancePlanVisitInterestRateHistory fpVisitFinancePlanVisitInterestRateHistory in fpVisit.FinancePlanVisitInterestRateHistories)
                    {
                        fpVisitFinancePlanVisitInterestRateHistory.InsertDate = fpVisitFinancePlanVisitInterestRateHistory.InsertDate.AddMilliseconds(adjust += 10);
                    }
                }

                // payment
                fpVisit.FinancePlanVisitInterestDues.Add(new FinancePlanVisitInterestDue
                {
                    FinancePlan = reconfigFp,
                    FinancePlanVisit = fpVisit,
                    InsertDate = DateTime.UtcNow,
                    InterestDue = month.InterestAmount * -1m,
                    PaymentAllocationId = 1
                });

                fpVisit.FinancePlanAmountDues.Add(new FinancePlanAmountDue
                {
                    FinancePlan = reconfigFp,
                    FinancePlanVisit = fpVisit,
                    InsertDate = DateTime.UtcNow,
                    AmountDue = month.PrincipalAmount * -1m,
                    PaymentAllocationId = 2
                });

                // faking "clear the payment"
                visitDateBalances = ApplyPrincipalPaymentAmountToFinancePlan(month.MonthNumber, dateToProcess, reconfigFp.FinancePlanVisits, month.PrincipalAmount * -1m);
            }

            decimal sumPrincipal = amortizationMonths.Sum(x => x.PrincipalAmount);
            decimal sumInterest = amortizationMonths.Sum(x => x.InterestAmount);

            Assert.AreEqual(sumPrincipal, principalPaid);
            Assert.AreEqual(sumInterest, interestPaid);
        }

        private static AmortizationVisitDateBalances ApplyPrincipalPaymentAmountToFinancePlan(int monthNumber, DateTime dateToProcess, IList<FinancePlanVisit> fpVisits, decimal principalPaymentAmount)
        {
            AmortizationVisitDateBalances visitDateBalances = new AmortizationVisitDateBalances();
            DateTime currentPaymentDueDate = dateToProcess.AddMonths(monthNumber).ToPaymentDueDateForDay(dateToProcess.Day);
            DateTime currentStatementDate = currentPaymentDueDate.AddDays(-21);

            DateTime nextPaymentDueDate = currentPaymentDueDate.AddMonths(1).ToPaymentDueDateForDay(dateToProcess.Day);
            DateTime nextStatementDate = nextPaymentDueDate.AddDays(-21);

            foreach (FinancePlanVisit financePlanVisit in fpVisits)
            {
                IList<Tuple<DateTime, decimal>> dateBalances = new List<Tuple<DateTime, decimal>>();
                //first 21 days - currentBalance
                foreach (DateTime dateTime in currentStatementDate.ThroughEachDayUntil(currentPaymentDueDate.AddDays(-1)))
                {
                    dateBalances.Add(new Tuple<DateTime, decimal>(dateTime, financePlanVisit.PrincipalBalance));
                }

                //apply principal payment amount from previous statement cycle
                financePlanVisit.FinancePlanVisitPrincipalAmounts.Add(new FinancePlanVisitPrincipalAmount
                {
                    FinancePlan = financePlanVisit.FinancePlan,
                    Amount = principalPaymentAmount,
                    FinancePlanVisit = financePlanVisit,
                    InsertDate = currentPaymentDueDate,
                    //PaymentAllocationId = paymentAllocation.PaymentAllocationId,
                    FinancePlanVisitPrincipalAmountType = FinancePlanVisitPrincipalAmountTypeEnum.Payment
                });

                //pdd on 21st day, so 22nd to endOfCycle is current balance less payment
                foreach (DateTime dateTime in currentPaymentDueDate.ThroughEachDayUntil(nextStatementDate.AddDays(-1)))
                {
                    dateBalances.Add(new Tuple<DateTime, decimal>(dateTime, financePlanVisit.PrincipalBalance));
                }

                visitDateBalances.Add(financePlanVisit, dateBalances);

            }
            return visitDateBalances;
        }

        [Test]
        public async Task ReconfigureFinancePlanAsync_Errors()
        {
            const int vpGuarantorId = 1;
            const int financePlanIdToReconfigure = 1;

            FinancePlan financePlanToReconfigure = new FinancePlan
            {
                FinancePlanId = financePlanIdToReconfigure,
                VpGuarantor = new Guarantor
                {
                    VpGuarantorId = vpGuarantorId
                }
            };
            financePlanToReconfigure.SetFinancePlanStatus(new FinancePlanStatus {FinancePlanStatusId = (int) FinancePlanStatusEnum.GoodStanding}, "");

            Mock<IFinancePlanService> financePlanServiceMock = new Mock<IFinancePlanService>();
            financePlanServiceMock.Setup(x => x.GetFinancePlanById(It.IsIn(financePlanIdToReconfigure))).Returns(financePlanToReconfigure);

            ReconfigureFinancePlanApplicationServiceMockBuilder builder = new ReconfigureFinancePlanApplicationServiceMockBuilder();
            builder.Setup(b =>
            {
                b.FinancePlanServiceMock = financePlanServiceMock;
            });
            IReconfigureFinancePlanApplicationService service = builder.CreateService();

            // fp not found
            FinancePlanCalculationParametersDto parametersDto1 = FinancePlanCalculationParametersDto.CreateForReconfigurationSubmit(vpGuarantorId, 100m, null, OfferCalculationStrategyEnum.MonthlyPayment);
            ReconfigurationActionResponseDto responseDto1 = await service.ReconfigureFinancePlanAsync(1000000, parametersDto1, 0, new JournalEventHttpContextDto(), true);
            Assert.True(responseDto1.IsError);

            // vpguarantorid !=
            FinancePlanCalculationParametersDto parametersDto2 = FinancePlanCalculationParametersDto.CreateForReconfigurationSubmit(100000, 100m, null, OfferCalculationStrategyEnum.MonthlyPayment);
            ReconfigurationActionResponseDto responseDto2 = await service.ReconfigureFinancePlanAsync(financePlanIdToReconfigure, parametersDto2, 0, new JournalEventHttpContextDto(), true);
            Assert.True(responseDto2.IsError);
            
            // not eligible
            financePlanToReconfigure.FinancePlanStatusHistory.Clear();
            financePlanToReconfigure.SetFinancePlanStatus(new FinancePlanStatus {FinancePlanStatusId = (int) FinancePlanStatusEnum.NonPaidInFull}, "");
            FinancePlanCalculationParametersDto parametersDto3 = FinancePlanCalculationParametersDto.CreateForReconfigurationSubmit(vpGuarantorId, 100m, null, OfferCalculationStrategyEnum.MonthlyPayment);
            ReconfigurationActionResponseDto responseDto3 = await service.ReconfigureFinancePlanAsync(financePlanIdToReconfigure, parametersDto3, 0, new JournalEventHttpContextDto(), true);
            Assert.True(responseDto3.IsError);
        }
        
        [Test]
        public async Task ReconfigureFinancePlanAsync_WithoutPendingReconfig_Creates()
        {
            const int vpGuarantorId = 1;
            const int financePlanIdToReconfigure = 1;

            FinancePlan financePlanToReconfigure = new FinancePlan
            {
                FinancePlanId = financePlanIdToReconfigure,
                VpGuarantor = new Guarantor
                {
                    VpGuarantorId = vpGuarantorId
                }
            };
            financePlanToReconfigure.SetFinancePlanStatus(new FinancePlanStatus {FinancePlanStatusId = (int) FinancePlanStatusEnum.GoodStanding}, "");
            
            Mock<IFinancePlanService> financePlanServiceMock = new Mock<IFinancePlanService>();
            financePlanServiceMock.Setup(x => x.GetFinancePlanById(It.IsAny<int>())).Returns(financePlanToReconfigure);

            ReconfigureFinancePlanApplicationServiceMockBuilder builder = new ReconfigureFinancePlanApplicationServiceMockBuilder();
            builder.Setup(b =>
            {
                b.ApplicationServiceCommonServiceMockBuilder.ClientServiceMock.Setup(x => x.GetClient()).Returns(() =>
                {
                    Mock<Client> clientMock = new Mock<Client>();
                    clientMock.Setup(x => x.InterestCalculationMethod).Returns(InterestCalculationMethodEnum.Monthly);
                    return clientMock.Object;
                });
                b.FinancePlanServiceMock = financePlanServiceMock;
                b.VpStatementServiceMock.Setup(x => x.GetMostRecentStatement(It.IsAny<Guarantor>())).Returns(() => new VpStatement());
            });
            IReconfigureFinancePlanApplicationService service = builder.CreateService();
            
            ReconfigurationActionResponseDto responseDto = await service.ReconfigureFinancePlanAsync(financePlanIdToReconfigure, FinancePlanCalculationParametersDto.CreateForReconfigurationSubmit(vpGuarantorId, 100m, null, OfferCalculationStrategyEnum.MonthlyPayment), 0, new JournalEventHttpContextDto(), true);

            Assert.False(responseDto.IsError);
            financePlanServiceMock.Verify(x => x.CreateReconfiguredFinancePlan(It.IsAny<FinancePlanCalculationParameters>(), It.IsAny<FinancePlan>(), It.IsAny<int>(), It.IsAny<JournalEventHttpContextDto>()), Times.Once);
        }

        [Test]
        public async Task ReconfigureFinancePlanAsync_WithPendingReconfig_Updates()
        {
            const int vpGuarantorId = 1;
            const int financePlanIdToReconfigure = 1;

            FinancePlan financePlanToReconfigure = new FinancePlan
            {
                FinancePlanId = financePlanIdToReconfigure,
                VpGuarantor = new Guarantor
                {
                    VpGuarantorId = vpGuarantorId
                },
            };
            financePlanToReconfigure.SetFinancePlanStatus(new FinancePlanStatus {FinancePlanStatusId = (int) FinancePlanStatusEnum.GoodStanding}, "");
            financePlanToReconfigure.ReconfiguredFinancePlan = new FinancePlan {OriginalFinancePlan = financePlanToReconfigure};
            financePlanToReconfigure.ReconfiguredFinancePlan.SetFinancePlanStatus(new FinancePlanStatus {FinancePlanStatusId = (int) FinancePlanStatusEnum.PendingAcceptance}, "");
            
            Mock<IFinancePlanService> financePlanServiceMock = new Mock<IFinancePlanService>();
            financePlanServiceMock.Setup(x => x.GetFinancePlanById(It.IsAny<int>())).Returns(financePlanToReconfigure);

            ReconfigureFinancePlanApplicationServiceMockBuilder builder = new ReconfigureFinancePlanApplicationServiceMockBuilder();
            builder.Setup(b =>
            {
                b.ApplicationServiceCommonServiceMockBuilder.ClientServiceMock.Setup(x => x.GetClient()).Returns(() =>
                {
                    Mock<Client> clientMock = new Mock<Client>();
                    clientMock.Setup(x => x.InterestCalculationMethod).Returns(InterestCalculationMethodEnum.Monthly);
                    return clientMock.Object;
                });
                b.FinancePlanServiceMock = financePlanServiceMock;
                b.VpStatementServiceMock.Setup(x => x.GetMostRecentStatement(It.IsAny<Guarantor>())).Returns(() => new VpStatement());
            });
            IReconfigureFinancePlanApplicationService service = builder.CreateService();

            ReconfigurationActionResponseDto responseDto = await service.ReconfigureFinancePlanAsync(financePlanIdToReconfigure, FinancePlanCalculationParametersDto.CreateForReconfigurationSubmit(vpGuarantorId, 100m, null, OfferCalculationStrategyEnum.MonthlyPayment), 0, new JournalEventHttpContextDto(), true);

            Assert.False(responseDto.IsError);
            financePlanServiceMock.Verify(x => x.UpdateReconfiguredFinancePlan(It.IsAny<FinancePlan>(), It.IsAny<FinancePlanCalculationParameters>(), It.IsAny<int>(), It.IsAny<JournalEventHttpContextDto>()), Times.Once);
        }

        [TestCase(GuarantorTypeEnum.Offline)]
        [TestCase(GuarantorTypeEnum.Online)]
        public void PendingReconfigurationMessage_Sends(GuarantorTypeEnum guarantorType)
        {
            const string token = "token";

            ReconfigureFinancePlanApplicationServiceMockBuilder builder = new ReconfigureFinancePlanApplicationServiceMockBuilder();
            builder.VisitPayUserServiceMock.Setup(x => x.GenerateOfflineLoginToken(It.IsAny<VisitPayUser>())).Returns(() => token);
            builder.SessionMock = new SessionMockBuilder().UseSynchronizations().CreateMock();
            builder.ApplicationServiceCommonServiceMockBuilder.EnableFeatures(VisitPayFeatureEnum.OfflineVisitPay);

            ReconfigureFinancePlanApplicationService svc = (ReconfigureFinancePlanApplicationService)builder.CreateService();

            Guarantor guarantor = new GuarantorBuilder().WithDefaults(1000).WithType(guarantorType);
            FinancePlan financePlan = new FinancePlan { VpGuarantor = guarantor };
            financePlan.SetFinancePlanStatus(new FinancePlanStatus { FinancePlanStatusId = 1 }, string.Empty);

            DateTime nextPaymentDueDate = DateTime.UtcNow.AddDays(1);
            
            svc.SendFinancePlanModificationEmailMessage(financePlan, nextPaymentDueDate);

            if (guarantorType == GuarantorTypeEnum.Online)
            {
                builder.ApplicationServiceCommonServiceMockBuilder.BusMock.Verify(x => x.PublishMessage(It.Is<SendFinancePlanModificationEmailMessage>(m =>
                    m.VpGuarantorId == guarantor.VpGuarantorId &&
                    m.NextPaymentDueDate.Date == nextPaymentDueDate.Date
                )), Times.Once);
            }
            else
            {
                builder.ApplicationServiceCommonServiceMockBuilder.BusMock.Verify(x => x.PublishMessage(It.Is<SendVpccFinancePlanModificationEmailMessage>(m =>
                    m.VpGuarantorId == guarantor.VpGuarantorId &&
                    m.NextPaymentDueDate.Date == nextPaymentDueDate.Date && 
                    m.TempPassword == token
                )), Times.Once);
            }
        }
    }
}
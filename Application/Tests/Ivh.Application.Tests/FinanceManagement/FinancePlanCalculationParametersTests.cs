﻿namespace Ivh.Application.Tests.FinanceManagement
{
    using Application.FinanceManagement.ApplicationServices;
    using Application.FinanceManagement.Common.Dtos;
    using Common.Base.Utilities.Extensions;
    using Common.Tests.Helpers.Base;
    using Common.Tests.Helpers.FinancePlan;
    using Common.VisitPay.Enums;
    using Domain.FinanceManagement.FinancePlan.Entities;
    using Domain.FinanceManagement.Payment.Entities;
    using Domain.FinanceManagement.Statement.Entities;
    using Domain.Guarantor.Entities;
    using Domain.Settings.Entities;
    using Domain.Visit.Entities;
    using Moq;
    using NUnit.Framework;
    using System;
    using System.Collections.Generic;
    using System.Linq;

    [TestFixture]
    public class FinancePlanCalculationParametersTests : ApplicationTestBase
    {
        private const int VpGuarantorId = 1;
        private const int VpStatementId = 1;

        private FinancePlanApplicationServiceMockBuilder MockBuilder { get; set; }

        [SetUp]
        public void Setup()
        {
            this.MockBuilder = new FinancePlanApplicationServiceMockBuilder();
            this.MockBuilder.GuarantorServiceMock.Setup(x => x.GetGuarantor(It.IsAny<int>())).Returns(() => new Guarantor { VpGuarantorId = VpGuarantorId });
            this.MockBuilder.VpStatementServiceMock.Setup(x => x.GetMostRecentStatement(It.IsAny<Guarantor>())).Returns(() => new VpStatement { VpStatementId = VpStatementId });
        }

        [Test]
        public void FinancePlanCalculationParameters_StatementIdsDontMatch_Throws()
        {
            FinancePlanCalculationParametersDto dto = FinancePlanCalculationParametersDto.CreateForMonthlyPayment(VpGuarantorId, 123123, 100m, null, false);
            FinancePlanApplicationService svc = (FinancePlanApplicationService)this.MockBuilder.CreateService();

            Assert.Throws<Exception>(() => svc.MapFinancePlanCalculationParameters(dto));
        }

        [TestCase(false)]
        [TestCase(true)]
        public void FinancePlanCalculationParameters_ContainsVisitsFromExistingFinancePlans(bool combinedEnabled)
        {
            IList<Visit> visits = new List<Visit>
            {
                new Visit {VisitId = 1, CurrentVisitState = VisitStateEnum.Active},
                new Visit {VisitId = 2, CurrentVisitState = VisitStateEnum.Active}
            };

            visits[0].SetCurrentBalance(1000m);
            visits[1].SetCurrentBalance(2000m);

            FinancePlan financePlan = new FinancePlan
            {
                FinancePlanVisits = new FinancePlanVisit
                {
                    VisitId = visits[0].VisitId,
                    CurrentVisitBalance = visits[0].CurrentBalance
                }.ToListOfOne()
            };

            financePlan.FinancePlanVisits[0].AddFinancePlanVisitInterestDue(10m, null, FinancePlanVisitInterestDueTypeEnum.InterestAssessment);

            FinancePlanApplicationService svc = this.MockServiceWithData(visits, financePlan, null);

            FinancePlanCalculationParametersDto dto = FinancePlanCalculationParametersDto.CreateForMonthlyPayment(VpGuarantorId, VpStatementId, 100m, null, combinedEnabled);
            FinancePlanCalculationParameters parameters = svc.MapFinancePlanCalculationParameters(dto);

            int expected = combinedEnabled ? 2 : 1;

            Assert.AreEqual(expected, parameters.Visits.Count);
        }

        [Test]
        public void FinancePlanCalculationParameters_HasInterestDueForExistingVisitOnCombinedPlan()
        {
            IList<Visit> visits = new List<Visit>
            {
                new Visit {VisitId = 1, CurrentVisitState = VisitStateEnum.Active},
                new Visit {VisitId = 2, CurrentVisitState = VisitStateEnum.Active}
            };

            visits[0].SetCurrentBalance(1000m);
            visits[1].SetCurrentBalance(2000m);

            FinancePlan financePlan = new FinancePlan
            {
                FinancePlanVisits = new FinancePlanVisit
                {
                    VisitId = visits[0].VisitId,
                    CurrentVisitBalance = visits[0].CurrentBalance
                }.ToListOfOne()
            };

            financePlan.FinancePlanVisits[0].AddFinancePlanVisitInterestDue(10m, null, FinancePlanVisitInterestDueTypeEnum.InterestAssessment);

            FinancePlanApplicationService svc = this.MockServiceWithData(visits, financePlan, null);

            FinancePlanCalculationParametersDto dto = FinancePlanCalculationParametersDto.CreateForMonthlyPayment(VpGuarantorId, VpStatementId, 100m, null, true);
            FinancePlanCalculationParameters parameters = svc.MapFinancePlanCalculationParameters(dto);

            Assert.AreEqual(10m, parameters.Visits.Sum(x => x.InterestDue));
        }

        [Test]
        public void FinancePlanCalculationParameters_DoesNotIncludeVisitsWithZeroBalance()
        {
            IList<Visit> visits = new List<Visit>
            {
                new Visit {VisitId = 1, CurrentVisitState = VisitStateEnum.Active},
                new Visit {VisitId = 2, CurrentVisitState = VisitStateEnum.Active}
            };

            visits[0].SetCurrentBalance(1000m);
            visits[1].SetCurrentBalance(2000m);

            FinancePlan financePlan = new FinancePlan
            {
                FinancePlanVisits = new FinancePlanVisit
                {
                    VisitId = visits[0].VisitId,
                    CurrentVisitBalance = visits[0].CurrentBalance
                }.ToListOfOne()
            };

            IList<PaymentAllocationVisitAmountResult> unclearedAllocationsForVisit = new List<PaymentAllocationVisitAmountResult>
            {
                new PaymentAllocationVisitAmountResult {ActualAmount = visits[0].CurrentBalance/2m, VisitId = visits[0].VisitId},
                new PaymentAllocationVisitAmountResult {ActualAmount = visits[0].CurrentBalance/2m, VisitId = visits[0].VisitId}
            };

            FinancePlanApplicationService svc = this.MockServiceWithData(visits, financePlan, unclearedAllocationsForVisit);

            FinancePlanCalculationParametersDto dto = FinancePlanCalculationParametersDto.CreateForMonthlyPayment(VpGuarantorId, VpStatementId, 100m, null, true);
            FinancePlanCalculationParameters parameters = svc.MapFinancePlanCalculationParameters(dto);

            Assert.AreEqual(1, parameters.Visits.Count);
        }

        [Test]
        public void FinancePlanCalculationParameters_CombinedFPVisitsHaveOverrideInterestRate()
        {
            IList<Visit> visits = new List<Visit>
            {
                new Visit {VisitId = 1, CurrentVisitState = VisitStateEnum.Active},
                new Visit {VisitId = 2, CurrentVisitState = VisitStateEnum.Active}
            };

            visits[0].SetCurrentBalance(1000m);
            visits[1].SetCurrentBalance(2000m);

            FinancePlan financePlan = new FinancePlan
            {
                FinancePlanVisits = new FinancePlanVisit
                {
                    VisitId = visits[0].VisitId,
                    CurrentVisitBalance = visits[0].CurrentBalance,
                    InterestDue = 10m
                }.ToListOfOne()
            };

            FinancePlanApplicationService svc = this.MockServiceWithData(visits, financePlan, null);

            FinancePlanCalculationParametersDto dto = FinancePlanCalculationParametersDto.CreateForMonthlyPayment(VpGuarantorId, VpStatementId, 100m, null, true);
            FinancePlanCalculationParameters parameters = svc.MapFinancePlanCalculationParameters(dto);

            Assert.AreEqual(0m, parameters.Visits.Sum(x => x.OverrideInterestRate));
            Assert.AreEqual(2, parameters.Visits.Sum(x => x.OverridesRemaining));
        }

        [Test]
        public void FinancePlanCalculationParameters_BalancesIncludeUnclearedPayments()
        {
            IList<Visit> visits = new List<Visit>
            {
                new Visit {VisitId = 1, CurrentVisitState = VisitStateEnum.Active},
                new Visit {VisitId = 2, CurrentVisitState = VisitStateEnum.Active}
            };

            visits[0].SetCurrentBalance(1000m);
            visits[1].SetCurrentBalance(2000m);

            FinancePlan financePlan = new FinancePlan
            {
                FinancePlanVisits = new FinancePlanVisit
                {
                    VisitId = visits[0].VisitId,
                    CurrentVisitBalance = visits[0].CurrentBalance
                }.ToListOfOne()
            };

            IList<PaymentAllocationVisitAmountResult> unclearedAllocationsForVisit = new List<PaymentAllocationVisitAmountResult>
            {
                new PaymentAllocationVisitAmountResult {ActualAmount = 500m, VisitId = visits[0].VisitId},
                new PaymentAllocationVisitAmountResult {ActualAmount = 350m, VisitId = visits[1].VisitId},
            };

            FinancePlanApplicationService svc = this.MockServiceWithData(visits, financePlan, unclearedAllocationsForVisit);

            FinancePlanCalculationParametersDto dto = FinancePlanCalculationParametersDto.CreateForMonthlyPayment(VpGuarantorId, VpStatementId, 100m, null, true);
            FinancePlanCalculationParameters parameters = svc.MapFinancePlanCalculationParameters(dto);

            Assert.AreEqual(2150m, parameters.Visits.Sum(x => x.CurrentBalance));
        }

        [Test]
        public void FinancePlanCalculationParameters_BalancesWithSelectedVisits()
        {
            IList<Visit> visits = new List<Visit>
            {
                new Visit {VisitId = 1, CurrentVisitState = VisitStateEnum.Active},
                new Visit {VisitId = 2, CurrentVisitState = VisitStateEnum.Active}
            };

            visits[0].SetCurrentBalance(1000m);
            visits[1].SetCurrentBalance(2000m);

            FinancePlan financePlan = new FinancePlan
            {
                FinancePlanVisits = new FinancePlanVisit
                {
                    VisitId = visits[0].VisitId,
                    CurrentVisitBalance = visits[0].CurrentBalance
                }.ToListOfOne()
            };

            FinancePlanApplicationService svc = this.MockServiceWithData(visits, financePlan, null);
            List<int> selectedVisits = new List<int> { 1 };
            FinancePlanCalculationParametersDto dto = FinancePlanCalculationParametersDto.CreateForMonthlyPayment(VpGuarantorId, VpStatementId, 100m, null, true, null, selectedVisits);
            FinancePlanCalculationParameters parameters = svc.MapFinancePlanCalculationParameters(dto);

            Assert.AreEqual(1000m, parameters.Visits.Sum(x => x.CurrentBalance));
        }

        [Test]
        public void MapFinancePlanCalculationParameters_GuarantorHasActiveVisitsInMultipleStates_OneStateVisitInParameters()
        {
            IList<Visit> visits = this.GenerateVisitsWithFacilitiesInMultipleStates(new[] { "MI", "IN" });
            visits[0].SetCurrentBalance(1000m);
            visits[1].SetCurrentBalance(2000m);
            FinancePlanApplicationService svc = this.MockServiceWithData(visits, new FinancePlan(), null);
            List<int> selectedVisits = new List<int> { visits[0].VisitId, visits[1].VisitId };
            FinancePlanCalculationParametersDto dto = FinancePlanCalculationParametersDto.CreateForMonthlyPayment(VpGuarantorId, VpStatementId, 100m, null, true, null, selectedVisits, stateCodeForVisits: "MI");

            FinancePlanCalculationParameters parameters = svc.MapFinancePlanCalculationParameters(dto, new Guarantor(), new VpStatement { VpStatementId = 1 });

            Assert.AreEqual(1, parameters.Visits.Count);
        }

        [Test]
        public void MapFinancePlanCalculationParameters_GuarantorHasActiveVisitsInSingleState_MultipleVisitsInParameters()
        {
            const string MichiganStateCode = "MI";
            IList<Visit> visits = this.GenerateVisitsWithFacilitiesInMultipleStates(new[] { MichiganStateCode, MichiganStateCode });
            visits[0].SetCurrentBalance(1000m);
            visits[1].SetCurrentBalance(2000m);
            FinancePlanApplicationService svc = this.MockServiceWithData(visits, new FinancePlan(), null);
            List<int> selectedVisits = new List<int> { visits[0].VisitId, visits[1].VisitId };
            FinancePlanCalculationParametersDto dto = FinancePlanCalculationParametersDto.CreateForMonthlyPayment(VpGuarantorId, VpStatementId, 100m, null, true, null, selectedVisits);

            FinancePlanCalculationParameters parameters = svc.MapFinancePlanCalculationParameters(dto, new Guarantor(), new VpStatement { VpStatementId = 1 });

            Assert.AreEqual(visits.Count, parameters.Visits.Count);
        }

        [TestCase(null, FinancePlanTypeEnum.PaperlessAutoPay)]
        [TestCase(1, FinancePlanTypeEnum.PaperlessAutoPay)]
        [TestCase(2, FinancePlanTypeEnum.PaperlessNonAutoPay)]
        [TestCase(3, FinancePlanTypeEnum.PaperAutoPay)]
        [TestCase(4, FinancePlanTypeEnum.PaperNonAutoPay)]
        //out of range 
        [TestCase(5, FinancePlanTypeEnum.PaperlessAutoPay)] 
        [TestCase(-1, FinancePlanTypeEnum.PaperlessAutoPay)]
        [TestCase(0, FinancePlanTypeEnum.PaperlessAutoPay)]
        public void MapFinancePlanCalculationParameters_OriginatedFinancePlanTypeId(int? financePlanTypeId, FinancePlanTypeEnum expectedFyFinancePlanTypeEnum)
        {
            IList<Visit> visits = new List<Visit>
            {
                new Visit {VisitId = 1, CurrentVisitState = VisitStateEnum.Active},
                new Visit {VisitId = 2, CurrentVisitState = VisitStateEnum.Active}
            };

            visits[0].SetCurrentBalance(1000m);
            visits[1].SetCurrentBalance(2000m);

            FinancePlan financePlan = new FinancePlan
            {
                FinancePlanVisits = new FinancePlanVisit
                {
                    VisitId = visits[0].VisitId,
                    CurrentVisitBalance = visits[0].CurrentBalance
                }.ToListOfOne()
            };

            FinancePlanApplicationService svc = this.MockServiceWithData(visits, financePlan, null);
            List<int> selectedVisits = new List<int> { 1 };
            FinancePlanCalculationParametersDto dto = FinancePlanCalculationParametersDto.CreateForMonthlyPayment(VpGuarantorId, VpStatementId, 100m, null, true, null, selectedVisits);
            dto.FinancePlanTypeId = financePlanTypeId;
            FinancePlanCalculationParameters parameters = svc.MapFinancePlanCalculationParameters(dto);
            Assert.AreEqual(expectedFyFinancePlanTypeEnum, parameters.FinancePlanType);

        }

        private FinancePlanApplicationService MockServiceWithData(IList<Visit> visits, FinancePlan financePlan, IList<PaymentAllocationVisitAmountResult> unclearedAllocationsForVisit)
        {
            VisitBalanceBaseApplicationServiceMockBuilder visitBalanceBaseApplicationServiceMockBuilder = new VisitBalanceBaseApplicationServiceMockBuilder();
            visitBalanceBaseApplicationServiceMockBuilder.PaymentAllocationServiceMock.Setup(x => x.GetNonInterestUnclearedPaymentAmountsForVisits(It.IsAny<IList<int>>(), null, null)).Returns(() => unclearedAllocationsForVisit);

            this.MockBuilder.ApplicationServiceCommonServiceMockBuilder.FeatureServiceMock.Setup(x => x.IsFeatureEnabled(VisitPayFeatureEnum.CombineFinancePlans)).Returns(true);
            this.MockBuilder.ApplicationServiceCommonServiceMockBuilder.ClientServiceMock.Setup(x => x.GetClient()).Returns(() =>
            {
                Mock<Client> clientMock = new Mock<Client>();
                clientMock.Setup(x => x.InterestCalculationMethod).Returns(InterestCalculationMethodEnum.Monthly);
                return clientMock.Object;
            });
            this.MockBuilder.FinancePlanServiceMock.Setup(x => x.GetAllOpenFinancePlansForGuarantor(It.IsAny<Guarantor>(), It.IsAny<bool>())).Returns(financePlan.ToListOfOne);
            this.MockBuilder.FinancePlanServiceMock.Setup(x => x.IsGuarantorEligibleToCombineFinancePlans(It.IsAny<Guarantor>(), It.IsAny<decimal>(), null)).Returns(true);
            this.MockBuilder.VisitBalanceBaseApplicationService = visitBalanceBaseApplicationServiceMockBuilder.CreateService();
            this.MockBuilder.VisitServiceMock.Setup(x => x.GetVisits(It.IsAny<int>(), It.IsAny<IList<int>>())).Returns((int vpgid, IList<int> vids) =>
            {
                return visits.Where(x => vids.Contains(x.VisitId)).ToList();
            });
            this.MockBuilder.VisitServiceMock.Setup(x => x.GetAllActiveVisits(It.IsAny<Guarantor>())).Returns(visits);

            return (FinancePlanApplicationService)this.MockBuilder.CreateService();
        }

        private IList<Visit> GenerateVisitsWithFacilitiesInMultipleStates(string[] stateCodes)
        {
            IList<Visit> visits = new List<Visit>();
            int id = 1;

            foreach (string stateCode in stateCodes)
            {
                Visit visit = new Visit
                {
                    VisitId = id,
                    CurrentVisitState = VisitStateEnum.Active,
                    Facility = new Facility
                    {
                        RicState = new State
                        {
                            StateCode = stateCode
                        }
                    }
                };

                id++;
                visits.Add(visit);
            };

            return visits;
        }
    }
}
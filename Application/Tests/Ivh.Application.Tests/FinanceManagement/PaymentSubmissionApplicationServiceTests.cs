﻿namespace Ivh.Application.Tests.FinanceManagement
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Application.Base.Common.Dtos;
    using Application.FinanceManagement.ApplicationServices;
    using Application.FinanceManagement.Common.Dtos;
    using AutoMapper;
    using Common.Base.Constants;
    using Common.Base.Enums;
    using Common.Base.Utilities.Extensions;
    using Common.Tests.Builders;
    using Common.Tests.Helpers.Client;
    using Common.Tests.Helpers.FinancePlan;
    using Common.Tests.Helpers.Guarantor;
    using Common.Tests.Helpers.Payment;
    using Common.Tests.Helpers.Visit;
    using Common.VisitPay.Constants;
    using Common.VisitPay.Enums;
    using Domain.FinanceManagement.Entities;
    using Domain.FinanceManagement.FinancePlan.Entities;
    using Domain.FinanceManagement.Payment.Entities;
    using Domain.FinanceManagement.Statement.Entities;
    using Domain.Guarantor.Entities;
    using Domain.Settings.Entities;
    using Domain.Settings.Interfaces;
    using Domain.Visit.Entities;
    using Moq;
    using NUnit.Framework;

    [TestFixture]
    public class PaymentSubmissionApplicationServiceTests : ApplicationTestBase
    {
        [Test]
        public void PaymentSubmissionDto_Rounds()
        {
            PaymentSubmissionDto dto = new PaymentSubmissionDto
            {
                TotalPaymentAmount = 1234.569999999998m
            };

            Assert.AreEqual(1234.57, dto.TotalPaymentAmount);
        }

        [Test]
        public void FinancePlanPaymentDto_Rounds()
        {
            FinancePlanPaymentDto dto = new FinancePlanPaymentDto
            {
                PaymentAmount = 1234.569999999998m
            };

            Assert.AreEqual(1234.57, dto.PaymentAmount);
        }

        [Test]
        public void VisitPaymentDto_Rounds()
        {
            VisitPaymentDto dto = new VisitPaymentDto
            {
                PaymentAmount = 1234.569999999998m
            };

            Assert.AreEqual(1234.57, dto.PaymentAmount);
        }
        
        [TestCase(PaymentTypeEnum.ManualPromptSpecificAmount)]
        [TestCase(PaymentTypeEnum.ManualScheduledSpecificAmount)]
        [TestCase(PaymentTypeEnum.ManualPromptCurrentNonFinancedBalanceInFull)]
        [TestCase(PaymentTypeEnum.ManualScheduledCurrentNonFinancedBalanceInFull)]
        public void SetupPaymentScheduledAmounts_with_no_visit_no_fp(PaymentTypeEnum paymentTypeEnum)
        {
            PaymentSubmissionApplicationServiceMockBuilder mockBuilder = new PaymentSubmissionApplicationServiceMockBuilder();

            const decimal paymentAmount = 100m;

            PaymentSubmissionDto paymentSubmissionDto = new PaymentSubmissionDto
            {
                TotalPaymentAmount = paymentAmount,

            };
            Payment payment = new Payment { PaymentType = paymentTypeEnum, Guarantor = new Guarantor() };

            if (paymentTypeEnum == PaymentTypeEnum.ManualPromptCurrentNonFinancedBalanceInFull ||
                paymentTypeEnum == PaymentTypeEnum.ManualScheduledCurrentNonFinancedBalanceInFull)
            {
                paymentSubmissionDto.VisitPayments = new List<VisitPaymentDto>
                {
                    new VisitPaymentDto {PaymentAmount = paymentAmount, VisitId = 0}
                };

                mockBuilder.PaymentServiceMock.Setup(x => x.GetPaymentVisitsByVisitIds(It.IsAny<IList<int>>())).Returns(() => new PaymentVisit().ToListOfOne());
                mockBuilder.VisitBalanceBaseApplicationServiceMock.Setup(x => x.GetTotalBalances(It.IsAny<int>())).Returns(() => new VisitBalanceBaseDto
                {
                    CurrentBalance = paymentAmount
                }.ToListOfOne());
            }

            PaymentSubmissionApplicationService paymentSubmissionApplicationService = mockBuilder.CreateService();

            //Should have no PaymentScheduledAmount
            Assert.AreEqual(0, payment.PaymentScheduledAmounts.Count);

            paymentSubmissionApplicationService.SetupPaymentScheduledAmounts(paymentSubmissionDto, payment);

            PaymentScheduledAmount paymentScheduledAmount = payment.PaymentScheduledAmounts.FirstOrDefault();
            Assert.IsNotNull(paymentScheduledAmount);
            Assert.AreEqual(paymentSubmissionDto.TotalPaymentAmount, paymentScheduledAmount.ScheduledAmount);

        }

        [TestCase(PaymentTypeEnum.RecurringPaymentFinancePlan)]
        [TestCase(PaymentTypeEnum.ManualPromptSpecificFinancePlans)]
        [TestCase(PaymentTypeEnum.ManualScheduledSpecificFinancePlans)]
        public void SetupPaymentScheduledAmounts_with_fp(PaymentTypeEnum paymentTypeEnum)
        {
            FinancePlan financePlan = new FinancePlanBuilder().WithDefaultValues().Build();
            PaymentFinancePlan paymentFp = Mapper.Map<PaymentFinancePlan>(financePlan);
            PaymentSubmissionApplicationService paymentSubmissionApplicationService = new PaymentSubmissionApplicationServiceMockBuilder().Setup(builder =>
            {
                builder.FinancePlanServiceMock.Setup(x => x.GetAllActiveFinancePlanIds(It.IsAny<int>())).Returns(financePlan.FinancePlanId.ToListOfOne());
                builder.FinancePlanServiceMock.Setup(x => x.GetFinancePlansById(It.IsAny<IList<int>>())).Returns(financePlan.ToListOfOne());
                builder.PaymentServiceMock.Setup(x => x.GetPaymentVisitsByVisitIds(It.IsAny<IList<int>>())).Returns(new List<PaymentVisit>());
                builder.PaymentServiceMock.Setup(x => x.GetPaymentFinancePlanByFinancePlanIds(It.IsAny<IList<int>>())).Returns(paymentFp.ToListOfOne());

            }).CreateService();

            FinancePlanPaymentDto planDto = new FinancePlanPaymentDto
            {
                PaymentAmount = 100m,
                FinancePlanId = financePlan.FinancePlanId
            };

            PaymentSubmissionDto paymentSubmissionDto = new PaymentSubmissionDto
            {
                FinancePlanPayments = planDto.ToListOfOne()
            };

            Payment payment = new Payment
            {
                Guarantor = new Guarantor(),
                PaymentType = paymentTypeEnum
            };

            Assert.True(payment.PaymentScheduledAmounts.Count == 0);//precheck

            paymentSubmissionApplicationService.SetupPaymentScheduledAmounts(paymentSubmissionDto, payment);

            PaymentScheduledAmount paymentScheduledAmount = payment.PaymentScheduledAmounts.FirstOrDefault();
            Assert.AreEqual(planDto.PaymentAmount, paymentScheduledAmount.ScheduledAmount);

        }

        [TestCase(PaymentTypeEnum.ManualPromptCurrentNonFinancedBalanceInFull)]
        [TestCase(PaymentTypeEnum.ManualScheduledCurrentNonFinancedBalanceInFull)]
        [TestCase(PaymentTypeEnum.ManualPromptSpecificVisits)]
        [TestCase(PaymentTypeEnum.ManualScheduledSpecificVisits)]
        public void SetupPaymentScheduledAmounts_with_visits(PaymentTypeEnum paymentTypeEnum)
        {
            const decimal paymentAmount = 100m;
            const int visitId = 111;

            Payment payment = new Payment { PaymentType = paymentTypeEnum, Guarantor = new Guarantor() };
            VisitPaymentDto visitPaymentDto = new VisitPaymentDto { PaymentAmount = paymentAmount, VisitId = visitId };
            PaymentSubmissionDto paymentSubmissionDto = new PaymentSubmissionDto
            {
                VisitPayments = visitPaymentDto.ToListOfOne()
            };

            PaymentSubmissionApplicationService paymentSubmissionApplicationService = new PaymentSubmissionApplicationServiceMockBuilder().Setup(builder =>
            {
                builder.VisitBalanceBaseApplicationServiceMock.Setup(x => x.GetTotalBalances(It.IsAny<int>())).Returns(() => new VisitBalanceBaseDto { CurrentBalance = paymentAmount, VisitId = visitId }.ToListOfOne());
                builder.PaymentServiceMock.Setup(x => x.GetPaymentVisitsByVisitIds(It.IsAny<IList<int>>())).Returns(() => new PaymentVisit { VisitId = visitPaymentDto.VisitId }.ToListOfOne());
                builder.PaymentServiceMock.Setup(x => x.GetPaymentFinancePlanByFinancePlanIds(It.IsAny<IList<int>>())).Returns(new List<PaymentFinancePlan>());

            }).CreateService();

            Assert.True(payment.PaymentScheduledAmounts.Count == 0);//precheck
            paymentSubmissionApplicationService.SetupPaymentScheduledAmounts(paymentSubmissionDto, payment);
            PaymentScheduledAmount paymentScheduledAmount = payment.PaymentScheduledAmounts.FirstOrDefault();
            Assert.AreEqual(visitPaymentDto.PaymentAmount, paymentScheduledAmount.ScheduledAmount);
        }
    }

    [TestFixture]
    public class PaymentValidationTests
    {
        private const int VpGuarantorId = 0;
        private IList<FinancePlan> _financePlans = null;
        private IList<Visit> _visits;
        private PaymentSubmissionApplicationServiceMockBuilder _mockBuilder;

        [SetUp]
        public void Setup()
        {
            PaymentServiceMockBuilder paymentServiceMockBuilder = new PaymentServiceMockBuilder();
            paymentServiceMockBuilder.PaymentVisitRepositoryMock.Setup(x => x.GetPaymentVisits(It.IsAny<IList<int>>())).Returns((IList<int> visitIds) =>
            {
                return (this._visits ?? new List<Visit>()).Where(x => visitIds.Contains(x.VisitId)).Select(x => new PaymentVisit
                {
                    VisitId = x.VisitId,
                    AgingTier = x.AgingTier,
                    CurrentVisitState = x.CurrentVisitState
                }).ToList();
            });
            paymentServiceMockBuilder.PaymentFinancePlanRepositoryMock.Setup(x => x.GetPaymentFinancePlans(It.IsAny<IList<int>>())).Returns((IList<int> financePlanIds) =>
            {
                return (this._financePlans ?? new List<FinancePlan>()).Where(x => financePlanIds.Contains(x.FinancePlanId)).Select(x => new PaymentFinancePlan
                {
                    CurrentFinancePlanBalance = x.CurrentFinancePlanBalance,
                    FinancePlanId = x.FinancePlanId,
                    FinancePlanStatus = x.FinancePlanStatus.FinancePlanStatusEnum
                }).ToList();
            });

            VisitServiceMockBuilder visitServiceMockBuilder = new VisitServiceMockBuilder();
            visitServiceMockBuilder.ApplicationSettingsServiceMock = new Mock<IApplicationSettingsService>();
            visitServiceMockBuilder.ApplicationSettingsServiceMock.Setup(x => x.PastDueAgingCount).Returns(() => new ApplicationSetting<int>(() => new Lazy<int>(() => 1)));
            
            this._mockBuilder = new PaymentSubmissionApplicationServiceMockBuilder
            {
                PaymentService = paymentServiceMockBuilder.CreatePaymentService(),
                VisitService = visitServiceMockBuilder.CreateService()
            };

            this._mockBuilder.ContentApplicationServiceMock.Setup(x => x.GetTextRegionAsync(It.IsAny<string>(), It.IsAny<IDictionary<string, string>>())).ReturnsAsync((string s, IDictionary<string, string> d) => s);
            this._mockBuilder.ContentApplicationServiceMock.Setup(x => x.GetTextRegion(It.IsAny<string>(), It.IsAny<IDictionary<string, string>>())).Returns((string s, IDictionary<string, string> d) => s);

            this._mockBuilder.ApplicationServiceCommonServiceMockBuilder.ClientServiceMock.Setup(x => x.GetClient()).Returns(() => new ClientMockBuilder().CreateService());
            this._mockBuilder.FinancePlanServiceMock.Setup(x => x.GetFinancePlanById(It.IsAny<int>())).Returns((int financePlanId) =>
            {
                return (this._financePlans ?? new List<FinancePlan>()).FirstOrDefault(x => x.FinancePlanId == financePlanId);
            });
            this._mockBuilder.FinancePlanServiceMock.Setup(x => x.GetFinancePlansById(It.IsAny<IList<int>>())).Returns((IList<int> financePlanIds) =>
            {
                return (this._financePlans ?? new List<FinancePlan>()).Where(x => financePlanIds.Contains(x.FinancePlanId)).ToList();
            });
            this._mockBuilder.GuarantorServiceMock.Setup(x => x.GetGuarantor(It.IsAny<int>())).Returns(new Guarantor());
            this._mockBuilder.PaymentMethodsServiceMock.Setup(x => x.GetById(It.IsAny<int>())).Returns(new PaymentMethod());
            this._mockBuilder.VpStatementServiceMock.Setup(x => x.GetMostRecentStatement(It.IsAny<Guarantor>())).Returns(new VpStatement());
            this._mockBuilder.FinancePlanServiceMock.Setup(x => x.GetAllActiveFinancePlanIds(It.IsAny<int>())).Returns(() =>
            {
                return this._financePlans?.Where(x => x.FinancePlanStatus.FinancePlanStatusEnum.IsInCategory(FinancePlanStatusEnumCategory.Active))
                           .Where(x => x.FinancePlanStatus.FinancePlanStatusEnum.IsNotInCategory(FinancePlanStatusEnumCategory.Pending))
                           .Select(x => x.FinancePlanId).ToList() ?? new List<int>();
            });
            this._mockBuilder.VisitBalanceBaseApplicationServiceMock.Setup(x => x.GetTotalBalances(It.IsAny<int>())).Returns(() =>
            {
                return (this._visits ?? new List<Visit>()).Select(x => new VisitBalanceBaseDto
                {
                    VisitId = x.VisitId,
                    CurrentBalance = x.CurrentBalance
                }).ToList();
            });
        }

        [TestCase(true)]
        [TestCase(false)]
        public void ValidatePayment_PaymentMethodExpired_Fails(bool promptPayment)
        {
            this._mockBuilder.PaymentMethodsServiceMock.Setup(x => x.GetById(It.IsAny<int>())).Returns(new PaymentMethod
            {
                PaymentMethodType = PaymentMethodTypeEnum.Visa,
                ExpDate = DateTime.UtcNow.AddMonths(-1).ToString("MMyy")
            });

            ValidatePaymentResponse response = this._mockBuilder.CreateService().ValidatePayment(new PaymentSubmissionDto
            {
                PaymentType = PaymentTypeEnum.ManualPromptCurrentNonFinancedBalanceInFull,
                PaymentMethodId = 0,
                PaymentDate = promptPayment ? DateTime.UtcNow : DateTime.UtcNow.AddDays(2)
            }, VpGuarantorId);

            Assert.AreEqual(TextRegionConstants.PaymentValidationPaymentMethodExpired, response.Message);
            Assert.IsFalse(response.Prompt);
        }
        
        [Test]
        public void ValidatePayment_PaymentMethodNearExpiry_Fails()
        {
            this._mockBuilder.PaymentMethodsServiceMock.Setup(x => x.GetById(It.IsAny<int>())).Returns(new PaymentMethod
            {
                PaymentMethodType = PaymentMethodTypeEnum.Visa,
                ExpDate = DateTime.UtcNow.ToString("MMyy")
            });

            ValidatePaymentResponse response = this._mockBuilder.CreateService().ValidatePayment(new PaymentSubmissionDto
            {
                PaymentType = PaymentTypeEnum.ManualPromptCurrentNonFinancedBalanceInFull,
                PaymentMethodId = 0,
                PaymentDate = DateTime.UtcNow.AddMonths(1)
            }, VpGuarantorId);

            Assert.AreEqual(TextRegionConstants.PaymentValidationPaymentMethodNearExpiry, response.Message);
            Assert.IsFalse(response.Prompt);
        }

        [Test]
        public void ValidatePayment_PaymentDatePastMax_Fails()
        {
            ValidatePaymentResponse response = this._mockBuilder.CreateService().ValidatePayment(new PaymentSubmissionDto
            {
                PaymentType = PaymentTypeEnum.HouseholdBalanceCurrentNonFinancedBalance,
                PaymentMethodId = 0,
                PaymentDate = DateTime.UtcNow.AddDays(31)
            }, VpGuarantorId);

            Assert.AreEqual(TextRegionConstants.PaymentValidationInvalidDate, response.Message);
            Assert.IsFalse(response.Prompt);
        }
        
        [TestCase(true)]
        [TestCase(false)]
        public void ValidatePayment_SpecificVisits_WithOneVisit_FullPayment_DoesNotWarn(bool promptPayment)
        {
            decimal visitBalance = 1000m;
            int agingCount = 0;

            this._visits = new List<Visit>
            {
                new Visit
                {
                    VisitId = 1,
                    VisitBalances = new VisitBalanceHistory {CurrentBalance = visitBalance}.ToListOfOne(),
                    VisitAgingHistories = new VisitAgingHistory { AgingTier = AgingTierEnum.NotStatemented }.ToListOfOne()
                }
            };

            ValidatePaymentResponse response = this._mockBuilder.CreateService().ValidatePayment(new PaymentSubmissionDto
            {
                PaymentType = PaymentTypeEnum.ManualPromptSpecificVisits,
                PaymentMethodId = 0,
                PaymentDate = promptPayment ? DateTime.UtcNow : DateTime.UtcNow.AddDays(5),
                VisitPayments = new List<VisitPaymentDto>
                {
                    new VisitPaymentDto {PaymentAmount = visitBalance, VisitId = 1}
                }
            }, VpGuarantorId);

            Assert.IsNull(response);
        }
        
        [TestCase(true)]
        [TestCase(false)]
        public void ValidatePayment_SpecificVisits_WithOneVisit_PastDue_FullPayment_DoesNotWarn(bool promptPayment)
        {
            decimal visitBalance = 1000m;
            AgingTierEnum agingTier = AgingTierEnum.PastDue;
            

            this._visits = new List<Visit>
            {
                new Visit
                {
                    VisitId = 1,
                    VisitBalances = new VisitBalanceHistory { CurrentBalance = visitBalance }.ToListOfOne(),
                    VisitAgingHistories = new VisitAgingHistory { AgingTier = agingTier }.ToListOfOne(),
                    CurrentVisitStateHistory = new VisitStateHistory { EvaluatedState = VisitStateEnum.Active }
                }
            };

            ValidatePaymentResponse response = this._mockBuilder.CreateService().ValidatePayment(new PaymentSubmissionDto
            {
                PaymentType = PaymentTypeEnum.ManualPromptSpecificVisits,
                PaymentMethodId = 0,
                PaymentDate = promptPayment ? DateTime.UtcNow : DateTime.UtcNow.AddDays(5),
                VisitPayments = new List<VisitPaymentDto>
                {
                    new VisitPaymentDto { PaymentAmount = visitBalance, VisitId = 1 }
                }
            }, VpGuarantorId);

            Assert.IsNull(response);
        }

        [TestCase(true)]
        [TestCase(false)]
        public void ValidatePayment_SpecificVisits_WithOneVisit_PastDue_PartialPayment_Warns(bool promptPayment)
        {
            decimal visitBalance = 1000m;
            int agingCount = 2;

            this._visits = new List<Visit>
            {
                new Visit
                {
                    VisitId = 1,
                    VisitBalances = new VisitBalanceHistory { CurrentBalance = visitBalance }.ToListOfOne(),
                    VisitAgingHistories = new VisitAgingHistory { AgingTier = (AgingTierEnum)agingCount }.ToListOfOne(),
                    CurrentVisitStateHistory = new VisitStateHistory { EvaluatedState = VisitStateEnum.Active }
                }
            };

            ValidatePaymentResponse response = this._mockBuilder.CreateService().ValidatePayment(new PaymentSubmissionDto
            {
                PaymentType = PaymentTypeEnum.ManualPromptSpecificVisits,
                PaymentMethodId = 0,
                PaymentDate = promptPayment ? DateTime.UtcNow : DateTime.UtcNow.AddDays(5),
                VisitPayments = new List<VisitPaymentDto>
                {
                    new VisitPaymentDto { PaymentAmount = visitBalance/2m, VisitId = 1 }
                }
            }, VpGuarantorId);

            Assert.AreEqual(TextRegionConstants.PaymentValidationPastDueVisits, response.Message);
            Assert.IsTrue(response.Prompt);
        }
        
        [TestCase(true)]
        [TestCase(false)]
        public void ValidatePayment_SpecificVisits_WithTwoVisits_OnePastDue_PartialPaymentOnPastDue_Warns(bool promptPayment)
        {
            this._visits = new List<Visit>
            {
                new Visit
                {
                    VisitId = 1,
                    VisitBalances = new VisitBalanceHistory { CurrentBalance = 1000m }.ToListOfOne(),
                    VisitAgingHistories = new VisitAgingHistory { AgingTier = AgingTierEnum.PastDue }.ToListOfOne(),
                    CurrentVisitStateHistory = new VisitStateHistory { EvaluatedState = VisitStateEnum.Active }
                },
                new Visit
                {
                    VisitId = 2,
                    VisitBalances = new VisitBalanceHistory { CurrentBalance = 2000m }.ToListOfOne(),
                    VisitAgingHistories = new VisitAgingHistory { AgingTier = 0 }.ToListOfOne(),
                    CurrentVisitStateHistory = new VisitStateHistory { EvaluatedState = VisitStateEnum.Active }
                }
            };

            ValidatePaymentResponse response = this._mockBuilder.CreateService().ValidatePayment(new PaymentSubmissionDto
            {
                PaymentType = PaymentTypeEnum.ManualPromptSpecificVisits,
                PaymentMethodId = 0,
                PaymentDate = promptPayment ? DateTime.UtcNow : DateTime.UtcNow.AddDays(5),
                VisitPayments = new List<VisitPaymentDto>
                {
                    new VisitPaymentDto { PaymentAmount = 500m, VisitId = 1},
                    new VisitPaymentDto { PaymentAmount = 2000m, VisitId = 2 }
                }
            }, VpGuarantorId);

            Assert.AreEqual(TextRegionConstants.PaymentValidationPastDueVisits, response.Message);
            Assert.IsTrue(response.Prompt);
        }

        [TestCase(true)]
        [TestCase(false)]
        public void ValidatePayment_SpecificVisits_WithTwoVisits_OnePastDue_FullPaymentOnPastDue_DoesNotWarn(bool promptPayment)
        {
            this._visits = new List<Visit>
            {
                new Visit
                {
                    VisitId = 1,
                    VisitBalances = new VisitBalanceHistory { CurrentBalance = 1000m }.ToListOfOne(),
                    VisitAgingHistories = new VisitAgingHistory { AgingTier = AgingTierEnum.PastDue }.ToListOfOne(),
                    CurrentVisitStateHistory = new VisitStateHistory { EvaluatedState = VisitStateEnum.Active }
                },
                new Visit
                {
                    VisitId = 2,
                    VisitBalances = new VisitBalanceHistory { CurrentBalance = 2000m }.ToListOfOne(),
                    VisitAgingHistories = new VisitAgingHistory { AgingTier = 0 }.ToListOfOne(),
                    CurrentVisitStateHistory = new VisitStateHistory { EvaluatedState = VisitStateEnum.Active }
                }
            };

            ValidatePaymentResponse response = this._mockBuilder.CreateService().ValidatePayment(new PaymentSubmissionDto
            {
                PaymentType = PaymentTypeEnum.ManualPromptSpecificVisits,
                PaymentMethodId = 0,
                PaymentDate = promptPayment ? DateTime.UtcNow : DateTime.UtcNow.AddDays(5),
                VisitPayments = new List<VisitPaymentDto>
                {
                    new VisitPaymentDto { PaymentAmount = 1000m, VisitId = 1},
                    new VisitPaymentDto { PaymentAmount = 500m, VisitId = 2}
                }
            }, VpGuarantorId);

            Assert.IsNull(response);
        }

        [TestCase(true)]
        [TestCase(false)]
        public void ValidatePayment_SpecificVisits_PayingMoreThanBalance_Fails(bool promptPayment)
        {
            this._visits = new List<Visit>
            {
                new Visit
                {
                    VisitId = 1,
                    VisitBalances = new VisitBalanceHistory {CurrentBalance = 1000m}.ToListOfOne(),
                    CurrentVisitStateHistory = new VisitStateHistory { EvaluatedState = VisitStateEnum.Active }
                }
            };

            ValidatePaymentResponse response = this._mockBuilder.CreateService().ValidatePayment(new PaymentSubmissionDto
            {
                PaymentType = PaymentTypeEnum.ManualPromptSpecificVisits,
                PaymentMethodId = 0,
                PaymentDate = promptPayment ? DateTime.UtcNow : DateTime.UtcNow.AddDays(5),
                VisitPayments = new List<VisitPaymentDto>
                {
                    new VisitPaymentDto {PaymentAmount = 1000.01m, VisitId = 1},
                }
            }, VpGuarantorId);

            Assert.AreEqual(TextRegionConstants.PaymentValidationVisitBalanceHasChanged, response.Message);
            Assert.IsFalse(response.Prompt);
        }
        
        [TestCase(true)]
        [TestCase(false)]
        public void ValidatePayment_SpecificFinancePlans_WithOneFinancePlan_FullPayment_DoesNotWarn(bool promptPayment)
        {
            Guarantor guarantor = GuarantorFactory.GenerateOnline();

            this._financePlans = new List<FinancePlan>
            {
                FinancePlanFactory.CreateFinancePlan(Enumerable.Range(0, 1).Select(x => VisitFactory.GenerateActiveVisit(1000m, 1, VisitStateEnum.Active, DateTime.UtcNow.AddMonths(-1), guarantor: guarantor)).ToList(), 0, 25m, DateTime.UtcNow.AddMonths(-1))
            };

            ValidatePaymentResponse response = this._mockBuilder.CreateService().ValidatePayment(new PaymentSubmissionDto
            {
                PaymentType = PaymentTypeEnum.ManualPromptSpecificFinancePlans,
                PaymentMethodId = 0,
                PaymentDate = promptPayment ? DateTime.UtcNow : DateTime.UtcNow.AddDays(5),
                FinancePlanPayments = new List<FinancePlanPaymentDto>
                {
                    new FinancePlanPaymentDto {PaymentAmount = 1000m, FinancePlanId = 1}
                }
            }, VpGuarantorId);

            Assert.IsNull(response);
        }

        [TestCase(true)]
        [TestCase(false)]
        public void ValidatePayment_SpecificFinancePlans_WithOneFinancePlan_PastDue_FullPayment_DoesNotWarn(bool promptPayment)
        {
            Guarantor guarantor = GuarantorFactory.GenerateOnline();
            this._financePlans = new List<FinancePlan>
            {
                FinancePlanFactory.CreateFinancePlan(Enumerable.Range(0, 1).Select(x => VisitFactory.GenerateActiveVisit(1000m, 1, VisitStateEnum.Active, DateTime.UtcNow.AddMonths(-1), guarantor: guarantor)).ToList(), 2, 100m, DateTime.UtcNow.AddMonths(-3))
            };
            
            ValidatePaymentResponse response = this._mockBuilder.CreateService().ValidatePayment(new PaymentSubmissionDto
            {
                PaymentType = PaymentTypeEnum.ManualPromptSpecificFinancePlans,
                PaymentMethodId = 0,
                PaymentDate = promptPayment ? DateTime.UtcNow : DateTime.UtcNow.AddDays(5),
                FinancePlanPayments = new List<FinancePlanPaymentDto>
                {
                    new FinancePlanPaymentDto {PaymentAmount = 1000m, FinancePlanId = 1}
                }
            }, VpGuarantorId);

            Assert.IsNull(response);
        }

        [TestCase(true)]
        [TestCase(false)]
        public void ValidatePayment_SpecificFinancePlans_WithOneFinancePlan_PastDue_PartialPayment_Warns(bool promptPayment)
        {
            Guarantor guarantor = GuarantorFactory.GenerateOnline();
            this._financePlans = new List<FinancePlan>
            {
                FinancePlanFactory.CreateFinancePlan(Enumerable.Range(0, 1).Select(x => VisitFactory.GenerateActiveVisit(1000m, 1, VisitStateEnum.Active, DateTime.UtcNow.AddMonths(-1), guarantor: guarantor)).ToList(), 3, 100m, DateTime.UtcNow.AddMonths(-4))
            };
            int fpId = this._financePlans.First().FinancePlanId;

            ValidatePaymentResponse response = this._mockBuilder.CreateService().ValidatePayment(new PaymentSubmissionDto
            {
                PaymentType = PaymentTypeEnum.ManualPromptSpecificFinancePlans,
                PaymentMethodId = 0,
                PaymentDate = promptPayment ? DateTime.UtcNow : DateTime.UtcNow.AddDays(5),
                FinancePlanPayments = new List<FinancePlanPaymentDto>
                {
                    new FinancePlanPaymentDto {PaymentAmount = 125m, FinancePlanId = fpId}
                }
            }, VpGuarantorId);

            Assert.AreEqual(TextRegionConstants.PaymentValidationPastDueFinancePlans, response.Message);
            Assert.IsTrue(response.Prompt);
        }

        [TestCase(true)]
        [TestCase(false)]
        public void ValidatePayment_SpecificFinancePlans_WithOneFinancePlan_PastDue_PartialPaymentMoreThanPastDue_Warns(bool promptPayment)
        {
            Guarantor guarantor = GuarantorFactory.GenerateOnline();

            //Pastdue
            FinancePlan financePlan1 = FinancePlanFactory.CreateFinancePlan(Enumerable.Range(0, 1).Select(x => VisitFactory.GenerateActiveVisit(2000m, 1, VisitStateEnum.Active, DateTime.UtcNow.AddMonths(-1), guarantor: guarantor)).ToList(), 3, 100m, DateTime.UtcNow.AddMonths(-4));

            this._financePlans = financePlan1.ToListOfOne();

            ValidatePaymentResponse response = this._mockBuilder.CreateService().ValidatePayment(new PaymentSubmissionDto
            {
                PaymentType = PaymentTypeEnum.ManualPromptSpecificFinancePlans,
                PaymentMethodId = 0,
                PaymentDate = promptPayment ? DateTime.UtcNow : DateTime.UtcNow.AddDays(5),
                FinancePlanPayments = new List<FinancePlanPaymentDto>
                {
                    new FinancePlanPaymentDto {PaymentAmount = 200m, FinancePlanId = financePlan1.FinancePlanId}
                }
            }, VpGuarantorId);

            Assert.IsNull(response);
        }
        
        [TestCase(true)]
        [TestCase(false)]
        public void ValidatePayment_SpecificFinancePlans_WithTwoFinancePlan_OnePastDue_PartialPaymentOnPastDue_Warns(bool promptPayment)
        {
            Guarantor guarantor = GuarantorFactory.GenerateOnline();

            //Pastdue
            FinancePlan financePlan1 = FinancePlanFactory.CreateFinancePlan(Enumerable.Range(0, 1).Select(x => VisitFactory.GenerateActiveVisit(2000m, 1, VisitStateEnum.Active, DateTime.UtcNow.AddMonths(-1), guarantor: guarantor)).ToList(), 3, 100m, DateTime.UtcNow.AddMonths(-4));

            this._financePlans = financePlan1.ToListOfOne();
            //Current
            FinancePlan financePlan2 = FinancePlanFactory.CreateFinancePlan(Enumerable.Range(0, 1).Select(x => VisitFactory.GenerateActiveVisit(2000m, 1, VisitStateEnum.Active, DateTime.UtcNow.AddMonths(-1), guarantor: guarantor)).ToList(), 0, 100m, DateTime.UtcNow.AddMonths(-1));
            this._financePlans.Add(financePlan2);

            ValidatePaymentResponse response = this._mockBuilder.CreateService().ValidatePayment(new PaymentSubmissionDto
            {
                PaymentType = PaymentTypeEnum.ManualPromptSpecificFinancePlans,
                PaymentMethodId = 0,
                PaymentDate = promptPayment ? DateTime.UtcNow : DateTime.UtcNow.AddDays(5),
                FinancePlanPayments = new List<FinancePlanPaymentDto>
                {
                    new FinancePlanPaymentDto {PaymentAmount = 125m, FinancePlanId = financePlan1.FinancePlanId},
                    new FinancePlanPaymentDto {PaymentAmount = 5m, FinancePlanId = financePlan2.FinancePlanId}
                }
            }, VpGuarantorId);

            Assert.AreEqual(TextRegionConstants.PaymentValidationPastDueFinancePlans, response.Message);
            Assert.IsTrue(response.Prompt);
        }
        
        [TestCase(true)]
        [TestCase(false)]
        public void ValidatePayment_SpecificFinancePlans_WithTwoFinancePlan_OnePastDue_FullPaymentOnPastDue_DoesNotWarn(bool promptPayment)
        {
            Guarantor guarantor = GuarantorFactory.GenerateOnline();

            //Pastdue
            FinancePlan financePlan1 = FinancePlanFactory.CreateFinancePlan(Enumerable.Range(0, 1).Select(x => VisitFactory.GenerateActiveVisit(2000m, 1, VisitStateEnum.Active, DateTime.UtcNow.AddMonths(-1), guarantor: guarantor)).ToList(), 3, 100m, DateTime.UtcNow.AddMonths(-4));

            this._financePlans = financePlan1.ToListOfOne();
            //Current
            FinancePlan financePlan2 = FinancePlanFactory.CreateFinancePlan(Enumerable.Range(0, 1).Select(x => VisitFactory.GenerateActiveVisit(2000m, 1, VisitStateEnum.Active, DateTime.UtcNow.AddMonths(-1), guarantor: guarantor)).ToList(), 0, 100m, DateTime.UtcNow.AddMonths(-1));
            this._financePlans.Add(financePlan2);

            ValidatePaymentResponse response = this._mockBuilder.CreateService().ValidatePayment(new PaymentSubmissionDto
            {
                PaymentType = PaymentTypeEnum.ManualPromptSpecificFinancePlans,
                PaymentMethodId = 0,
                PaymentDate = promptPayment ? DateTime.UtcNow : DateTime.UtcNow.AddDays(5),
                FinancePlanPayments = new List<FinancePlanPaymentDto>
                {
                    new FinancePlanPaymentDto {PaymentAmount = 200m, FinancePlanId = financePlan1.FinancePlanId},
                    new FinancePlanPaymentDto {PaymentAmount = 5m, FinancePlanId = financePlan2.FinancePlanId}
                }
            }, VpGuarantorId);

            Assert.IsNull(response);
        }
        
        [TestCase(true)]
        [TestCase(false)]
        public void ValidatePayment_SpecificFinancePlans_PayingMoreThanBalance_Fails(bool promptPayment)
        {
            this._financePlans = new List<FinancePlan>
            {
                new FinancePlan
                {
                    FinancePlanId = 1,
                    FinancePlanStatus = new FinancePlanStatus {FinancePlanStatusId = (int) FinancePlanStatusEnum.GoodStanding},
                    FinancePlanVisits = new List<FinancePlanVisit>
                    {
                        new FinancePlanVisit { CurrentVisitBalance = 1000m }
                    }
                }
            };

            ValidatePaymentResponse response = this._mockBuilder.CreateService().ValidatePayment(new PaymentSubmissionDto
            {
                PaymentType = PaymentTypeEnum.ManualPromptSpecificFinancePlans,
                PaymentMethodId = 0,
                PaymentDate = promptPayment ? DateTime.UtcNow : DateTime.UtcNow.AddDays(5),
                FinancePlanPayments = new List<FinancePlanPaymentDto>
                {
                    new FinancePlanPaymentDto {PaymentAmount = 10000m, FinancePlanId = 1}
                }
            }, VpGuarantorId);

            Assert.AreEqual(TextRegionConstants.PaymentValidationFinancePlanBalanceHasChanged, response.Message);
            Assert.IsFalse(response.Prompt);
        }
    }
}
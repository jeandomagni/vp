﻿namespace Ivh.Application.Tests.FinanceManagement
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.Threading;
    using System.Threading.Tasks;
    using Application.FinanceManagement.Common.Dtos;
    using Application.FinanceManagement.Common.Interfaces;
    using Common.Base.Utilities.Extensions;
    using Common.Tests.Helpers.FinancePlan;
    using Common.VisitPay.Enums;
    using Content.Common.Dtos;
    using Domain.FinanceManagement.FinancePlan.Entities;
    using Domain.Guarantor.Entities;
    using Domain.Settings.Entities;
    using Domain.User.Entities;
    using Moq;
    using NUnit.Framework;

    [TestFixture]
    public class CreditAgreementApplicationServiceTests : ApplicationTestBase
    {
        [Test]
        public void CacheCreditAgreementTermsAsync_Caches()
        {
            CreditAgreementApplicationServiceMockBuilder builder = new CreditAgreementApplicationServiceMockBuilder();
            ICreditAgreementApplicationService svc = builder.CreateService();

            FinancePlanTermsDto termsDto = new FinancePlanTermsDto();

            svc.CacheCreditAgreementTermsAsync(termsDto);

            builder.ApplicationServiceCommonServiceMockBuilder.DistributedCacheMock.Verify(x => x.SetObjectAsync(
                It.IsAny<string>(),
                It.IsAny<object>(),
                It.IsAny<int>()
            ));
        }

        [Test]
        public void CacheCreditAgreementTermsAsync_WithoutTerms_Throws()
        {
            CreditAgreementApplicationServiceMockBuilder builder = new CreditAgreementApplicationServiceMockBuilder();
            ICreditAgreementApplicationService svc = builder.CreateService();

            Assert.ThrowsAsync<Exception>(() => svc.CacheCreditAgreementTermsAsync(null));
        }

        [Test]
        public void GetCachedCreditAgreementAsync_OfferNotFound_Throws()
        {
            CreditAgreementApplicationServiceMockBuilder builder = new CreditAgreementApplicationServiceMockBuilder();
            builder.FinancePlanOfferServiceMock.Setup(x => x.GetOfferById(It.IsAny<int>())).Returns(() => null);

            ICreditAgreementApplicationService svc = builder.CreateService();
            Assert.ThrowsAsync<Exception>(() => svc.GetCachedCreditAgreementAsync(1, null, false));
        }

        [Test]
        public void GetCachedCreditAgreementAsync_CachedTermsNotFound_Throws()
        {
            CreditAgreementApplicationServiceMockBuilder builder = new CreditAgreementApplicationServiceMockBuilder();
            builder.FinancePlanOfferServiceMock.Setup(x => x.GetOfferById(It.IsAny<int>())).Returns(() => new FinancePlanOffer
            {
                VpGuarantor = new Guarantor
                {
                    VpGuarantorId = 1
                }
            });
            builder.ApplicationServiceCommonServiceMockBuilder.DistributedCacheMock.Setup(x => x.GetObjectAsync<FinancePlanTermsDto>(It.IsAny<string>())).ReturnsAsync(() => null);

            ICreditAgreementApplicationService svc = builder.CreateService();
            Assert.ThrowsAsync<Exception>(() => svc.GetCachedCreditAgreementAsync(1, null, false));
        }

        [Test]
        public void GetCachedCreditAgreementAsync_GuarantorIdMismatch_Throws()
        {
            CreditAgreementApplicationServiceMockBuilder builder = new CreditAgreementApplicationServiceMockBuilder();
            builder.FinancePlanOfferServiceMock.Setup(x => x.GetOfferById(It.IsAny<int>())).Returns(() => new FinancePlanOffer
            {
                VpGuarantor = new Guarantor
                {
                    VpGuarantorId = 1
                }
            });
            builder.ApplicationServiceCommonServiceMockBuilder.DistributedCacheMock.Setup(x => x.GetObjectAsync<FinancePlanTermsDto>(It.IsAny<string>())).ReturnsAsync(() => new FinancePlanTermsDto
            {
                VpGuarantorId = 2
            });

            ICreditAgreementApplicationService svc = builder.CreateService();
            Assert.ThrowsAsync<Exception>(() => svc.GetCachedCreditAgreementAsync(1, null, false));
        }
        
        [TestCase("en-US", "en-US")]
        [TestCase("is-IS", "en-US")]
        public async Task GetCachedCreditAgreementAsync_GeneratesInExpectedLocales(string userLocale, string defaultLocale)
        {
            CreditAgreementApplicationServiceMockBuilder builder = this.CreateBuilder(userLocale, defaultLocale);

            ICreditAgreementApplicationService svc = builder.CreateService();
            FinancePlanCreditAgreementCollectionDto creditAgreementCollectionDto = await svc.GetCachedCreditAgreementAsync(1, null, false);

            Assert.IsNotNull(creditAgreementCollectionDto.UserLocale);
            Assert.AreEqual(userLocale, creditAgreementCollectionDto.UserLocale.Locale);

            if (userLocale == defaultLocale)
            {
                Assert.IsNull(creditAgreementCollectionDto.ClientLocale);
            }
            else
            {
                Assert.IsNotNull(creditAgreementCollectionDto.ClientLocale);
                Assert.AreEqual(defaultLocale, creditAgreementCollectionDto.ClientLocale.Locale);
            }
        }

        [TestCase("en-US")]
        [TestCase("is-IS")]
        public async Task GetCreditAgreementContentAsync_ForPendingFinancePlan_Vp3_ReturnsInExpectedLocales(string viewLocale)
        {
            CreditAgreementApplicationServiceMockBuilder builder = this.CreateBuilder(viewLocale, "en-US");
            builder.FinancePlanServiceMock.Setup(x => x.GetFinancePlanByOfferId(It.IsAny<int>())).Returns(() =>
            {
                Mock<FinancePlan> mock = new Mock<FinancePlan>();
                return mock.Object;
            });

            ICreditAgreementApplicationService svc = builder.CreateService();
            ContentDto content = await svc.GetCreditAgreementContentAsync(1);

            Assert.AreEqual(viewLocale, content.ContentBody);
        }

        [TestCase("en-US")]
        [TestCase("is-IS")]
        public async Task GetCreditAgreementContentAsync_ForExistingFinancePlan_Vp3_ReturnsInExpectedLocales(string viewLocale)
        {
            CreditAgreementApplicationServiceMockBuilder builder = this.CreateBuilder(viewLocale, "en-US");
            builder.FinancePlanServiceMock.Setup(x => x.GetFinancePlanByOfferId(It.IsAny<int>())).Returns(() =>
            {
                Mock<FinancePlan> mock = new Mock<FinancePlan>();
                mock.Setup(x => x.TermsCmsVersionId).Returns(1000);
                mock.Setup(x => x.FinancePlanCreditAgreements).Returns(() => new List<FinancePlanCreditAgreement>
                {
                    new FinancePlanCreditAgreement
                    {
                        AgreementText = "is-IS",
                        Locale = "is-IS",
                        IsUserLocale = true
                    },
                    new FinancePlanCreditAgreement
                    {
                        AgreementText = "en-US",
                        Locale = "en-US",
                    }
                });
                return mock.Object;
            });

            ICreditAgreementApplicationService svc = builder.CreateService();
            ContentDto content = await svc.GetCreditAgreementContentAsync(1);

            Assert.AreEqual(viewLocale, content.ContentBody);
        }

        [TestCase("fr-CA")]
        public async Task GetCreditAgreementContentAsync_ForExistingFinancePlan_Vp3_DefaultToUserAgreedLocale(string viewLocale)
        {
            CreditAgreementApplicationServiceMockBuilder builder = this.CreateBuilder(viewLocale, "en-US");
            builder.FinancePlanServiceMock.Setup(x => x.GetFinancePlanByOfferId(It.IsAny<int>())).Returns(() =>
            {
                Mock<FinancePlan> mock = new Mock<FinancePlan>();
                mock.Setup(x => x.TermsCmsVersionId).Returns(1000);
                mock.Setup(x => x.FinancePlanCreditAgreements).Returns(() => new List<FinancePlanCreditAgreement>
                {
                    new FinancePlanCreditAgreement
                    {
                        AgreementText = "is-IS",
                        Locale = "is-IS",
                        IsUserLocale = true
                    },
                    new FinancePlanCreditAgreement
                    {
                        AgreementText = "en-US",
                        Locale = "en-US",
                    }
                });
                return mock.Object;
            });

            ICreditAgreementApplicationService svc = builder.CreateService();
            ContentDto content = await svc.GetCreditAgreementContentAsync(1);

            // should default to the one with IsUserLocale = true when a saved not found in current locale
            Assert.AreEqual("is-IS", content.ContentBody);
        }
        
        [TestCase("en-US")]
        [TestCase("is-IS")]
        public async Task GetCreditAgreementContentAsync_ForExistingFinancePlan_Vp2_ReturnsInExpectedLocales(string viewLocale)
        {
            Guarantor guarantor = this.CreateGuarantor(1, viewLocale);
            CreditAgreementApplicationServiceMockBuilder builder = this.CreateBuilder(viewLocale, "en-US");
            builder.FinancePlanServiceMock.Setup(x => x.GetFinancePlanByOfferId(It.IsAny<int>())).Returns(() =>
            {
                Mock<FinancePlan> mock = new Mock<FinancePlan>();
                mock.Setup(x => x.TermsCmsVersionId).Returns(1000);
                mock.Setup(x => x.VpGuarantor).Returns(guarantor);
                return mock.Object;
            });

            ICreditAgreementApplicationService svc = builder.CreateService();
            ContentDto content = await svc.GetCreditAgreementContentAsync(1);

            Assert.AreEqual(viewLocale, content.ContentBody);
        }
        
        private CreditAgreementApplicationServiceMockBuilder CreateBuilder(string userLocale, string defaultLocale)
        {
            const int vpGuarantorId = 1;
            const int cmsVersionNum = 10;
            
            Thread.CurrentThread.CurrentUICulture = new CultureInfo(userLocale);
            
            Guarantor guarantor = this.CreateGuarantor(vpGuarantorId, userLocale);

            CreditAgreementApplicationServiceMockBuilder builder = new CreditAgreementApplicationServiceMockBuilder();
            builder.FinancePlanOfferServiceMock.Setup(x => x.GetOfferById(It.IsAny<int>())).Returns(() => new FinancePlanOffer
            {
                VpGuarantor = guarantor
            });
            builder.ApplicationServiceCommonServiceMockBuilder.ClientServiceMock.Setup(x => x.GetClient()).Returns(() =>
            {
                Mock<Client> mockClient = new Mock<Client>();
                mockClient.Setup(x => x.LanguageAppDefaultName).Returns(defaultLocale);
                return mockClient.Object;
            });
            builder.ApplicationServiceCommonServiceMockBuilder.DistributedCacheMock.Setup(x => x.GetObjectAsync<FinancePlanTermsDto>(It.IsAny<string>())).ReturnsAsync(() => new FinancePlanTermsDto
            {
                VpGuarantorId = vpGuarantorId,
				TermsCmsRegionEnum = CmsRegionEnum.VppCreditAgreement
            });
            builder.GuarantorServiceMock.Setup(x => x.GetGuarantor(vpGuarantorId)).Returns(guarantor);
            builder.ContentApplicationServiceMock.Setup(x => x.GetCurrentVersionAsync(CmsRegionEnum.VppCreditAgreement, true, It.IsAny<IDictionary<string, string>>())).ReturnsAsync(() => new CmsVersionDto
            {
                ContentBody = Thread.CurrentThread.GetLocale(),
                ContentTitle = "content title",
                VersionNum = cmsVersionNum
            });
            builder.ContentApplicationServiceMock.Setup(x => x.GetCmsVersionNumbersAsync(CmsRegionEnum.VppCreditAgreement)).ReturnsAsync(() => new Dictionary<int, int>
            {
                {1000, cmsVersionNum}
            });
            builder.ContentApplicationServiceMock.Setup(x => x.GetSpecificVersionAsync(CmsRegionEnum.VppCreditAgreement, 1000, true, It.IsAny<IDictionary<string, string>>())).ReturnsAsync(() => new CmsVersionDto
            {
                ContentBody = Thread.CurrentThread.GetLocale(),
                ContentTitle = "content title",
                VersionNum = cmsVersionNum
            });

            return builder;
        }

        private Guarantor CreateGuarantor(int vpGuarantorId, string userLocale)
        {
            Guarantor guarantor = new Guarantor
            {
                VpGuarantorId = vpGuarantorId,
                User = new VisitPayUser
                {
                    Locale = userLocale
                }
            };

            return guarantor;
        }
    }
}
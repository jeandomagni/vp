﻿namespace Ivh.Application.Tests.FinanceManagement
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Application.FinanceManagement.ApplicationServices;
    using Common.Base.Utilities.Extensions;
    using Common.Tests.Helpers.PaymentMethod;
    using Common.VisitPay.Messages.FinanceManagement;
    using Domain.FinanceManagement.Entities;
    using Domain.Settings.Entities;
    using Moq;
    using NUnit.Framework;

    [TestFixture]
    public class PaymentMethodsApplicationServiceTests
    {
        public static IEnumerable<DateTime> DateTimesThisMonth
        {
            get
            {
                for (int i = 1; i <= DateTime.DaysInMonth(DateTime.UtcNow.Year, DateTime.UtcNow.Month); i++)
                {
                    yield return new DateTime(DateTime.UtcNow.Year, DateTime.UtcNow.Month, i);
                }
            }
        }
        
        [TestCaseSource(nameof(DateTimesThisMonth))]
        public void QueuePaymentMethods_SendsOnAppropriateDay(DateTime processDate)
        {
            const int expiringCreditCardReminderIntervalInDays = 14;
            const int guarantor1 = 1;
            const int guarantor2 = 2;

            PaymentMethodsApplicationServiceMockBuilder builder = new PaymentMethodsApplicationServiceMockBuilder();
            builder.ApplicationServiceCommonServiceMockBuilder.ClientServiceMock.Setup(x => x.GetClient()).Returns(() =>
            {
                Mock<Client> mockClient = new Mock<Client>();
                mockClient.Setup(x => x.ExpiringCreditCardReminderIntervalInDays).Returns(expiringCreditCardReminderIntervalInDays);
                return mockClient.Object;
            });
            
            // guarantor 1 has an active plan
            builder.PaymentMethodsServiceMock.Setup(x => x.GetExpiringPrimaryPaymentMethods(It.IsAny<string>())).Returns(() => new List<PaymentMethod>
            {
                new PaymentMethod
                {
                    ExpDate = processDate.ToExpDate(),
                    IsPrimary = true,
                    IsActive = true,
                    VpGuarantorId = guarantor1
                }
            });
            builder.FinancePlanServiceMock.Setup(x => x.GetActiveFinancePlanCount(guarantor1)).Returns(1);

            // guarantor 2 has a balance
            builder.PaymentMethodsServiceMock.Setup(x => x.GetExpiringPrimaryPaymentMethods(It.IsAny<string>())).Returns(() => new List<PaymentMethod>
            {
                new PaymentMethod
                {
                    ExpDate = processDate.ToExpDate(),
                    IsPrimary = true,
                    IsActive = true,
                    VpGuarantorId = guarantor2
                }
            });
            builder.VisitBalanceBaseApplicationServiceMock.Setup(x => x.GetTotalBalance(guarantor2)).Returns(100m);
            
            PaymentMethodsApplicationService svc = builder.CreateService();

            svc.NotifyExpiringPrimaryPaymentMethods(processDate);
            
            // these results assume guarantors have a balance as mocked above

            // expecting Expired message to be sent on the last day of a month
            Times expiredTimes = processDate.IsLastDayOfMonth() ? Times.Once() : Times.Never();
            builder.ApplicationServiceCommonServiceMockBuilder.BusMock.Verify(x => x.PublishMessage(It.IsAny<SendCardExpiredEmailMessage>()), expiredTimes);

            // expects Expiring message to be sent [[ExpiringCreditCardReminderIntervalInDays]] before the last day of the month
            Times expiringTimes = processDate.AddDays(expiringCreditCardReminderIntervalInDays).IsLastDayOfMonth() ? Times.Once() : Times.Never();
            builder.ApplicationServiceCommonServiceMockBuilder.BusMock.Verify(x => x.PublishMessage(It.IsAny<SendCardExpiringEmailMessage>()), expiringTimes);
        }
        
        [TestCase("0419", true, true)]
        [TestCase("0519", true, true)]
        [TestCase("0419", false, true)]
        [TestCase("0419", true, false)]
        public void QueuePaymentMethods_ValidatesPaymentMethod(string expDate, bool isPrimary, bool isActive)
        {
            DateTime processDate = new DateTime(2019, 04, 30);
            const int guarantor1 = 1;

            PaymentMethodsApplicationServiceMockBuilder builder = new PaymentMethodsApplicationServiceMockBuilder();
            
            builder.PaymentMethodsServiceMock.Setup(x => x.GetExpiringPrimaryPaymentMethods(It.IsAny<string>())).Returns(() => new List<PaymentMethod>
            {
                new PaymentMethod
                {
                    ExpDate = expDate,
                    IsPrimary = isPrimary,
                    IsActive = isActive,
                    VpGuarantorId = guarantor1
                }
            });
            builder.VisitBalanceBaseApplicationServiceMock.Setup(x => x.GetTotalBalance(guarantor1)).Returns(100m);
            
            PaymentMethodsApplicationService svc = builder.CreateService();

            IEnumerable<SendCardExpiredEmailMessage> expiredPaymentMethods = svc.GetPaymentMethodExpiredMessages(processDate);

            if (processDate.ToExpDate() == expDate && isPrimary && isActive)
            {
                Assert.AreEqual(1, expiredPaymentMethods.Count());
            }
            else
            {
                Assert.AreEqual(0, expiredPaymentMethods.Count());
            }
        }
        
        [TestCase(0, 0)]
        [TestCase(1, 0)]
        [TestCase(0, 100)]
        public void QueuePaymentMethods_ChecksBalance(int numberOfActiveFinancePlans, decimal nonFinancedBalance)
        {
            DateTime processDate = new DateTime(2019, 04, 30);
            const int guarantor1 = 1;

            PaymentMethodsApplicationServiceMockBuilder builder = new PaymentMethodsApplicationServiceMockBuilder();
            
            // guarantor 1 has a balance, but card is not expired
            builder.PaymentMethodsServiceMock.Setup(x => x.GetExpiringPrimaryPaymentMethods(It.IsAny<string>())).Returns(() => new List<PaymentMethod>
            {
                new PaymentMethod
                {
                    ExpDate = "0419",
                    IsPrimary = true,
                    IsActive = true,
                    VpGuarantorId = guarantor1
                }
            });
            builder.FinancePlanServiceMock.Setup(x => x.GetActiveFinancePlanCount(guarantor1)).Returns(numberOfActiveFinancePlans);
            builder.VisitBalanceBaseApplicationServiceMock.Setup(x => x.GetTotalBalance(guarantor1)).Returns(nonFinancedBalance);
            
            PaymentMethodsApplicationService svc = builder.CreateService();

            IEnumerable<SendCardExpiredEmailMessage> expiredPaymentMethods = svc.GetPaymentMethodExpiredMessages(processDate);

            if (numberOfActiveFinancePlans > 0 || nonFinancedBalance > 0)
            {
                Assert.AreEqual(1, expiredPaymentMethods.Count());
            }
            else
            {
                Assert.AreEqual(0, expiredPaymentMethods.Count());
            }
        }
    }
}
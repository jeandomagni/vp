﻿namespace Ivh.Application.Tests.FinanceManagement
{
    using System;
    using System.Collections.Generic;
    using Application.FinanceManagement.Common.Interfaces;
    using Common.Base.Utilities.Extensions;
    using Common.Tests.Helpers.Ach;
    using Common.VisitPay.Enums;
    using Common.VisitPay.Messages.FinanceManagement;
    using Domain.FinanceManagement.Ach.Entities;
    using Domain.FinanceManagement.Entities;
    using Domain.FinanceManagement.Payment.Entities;
    using Domain.FinanceManagement.Statement.Entities;
    using Domain.Guarantor.Entities;
    using Moq;
    using NUnit.Framework;

    public class AchApplicationServiceTests : ApplicationTestBase
    {
        [TestCase(PaymentStatusEnum.ActivePendingACHApproval)]
        [TestCase(PaymentStatusEnum.ClosedFailed)]
        [TestCase(PaymentStatusEnum.ClosedPaid)]
        public void Ach_Settles(PaymentStatusEnum paymentStatus)
        {
            Mock<Guarantor> guarantor = new Mock<Guarantor>();
            guarantor.Setup(x => x.VpGuarantorId).Returns(1);

            AchSettlementDetail achSettlementDetail = new AchSettlementDetail
            {
                AchFile = new AchFile(),
                AchSettlementDetailId = 1000,
                ReturnCode = null
            };

            AchSettlementMessageGuarantor achSettlementMessageGuarantor = new AchSettlementMessageGuarantor()
            {
                CorrelationId = Guid.NewGuid(),
                Messages = new List<AchSettlementMessage>
                {
                    new AchSettlementMessage
                    {
                        AchSettlementDetailId = achSettlementDetail.AchSettlementDetailId,
                        CorrelationId = Guid.NewGuid(),
                        TransactionId = "ABC123"
                    }
                },
                VpGuarantorId = guarantor.Object.VpGuarantorId
            };

            Mock<Payment> returnedPayment = new Mock<Payment>();
            returnedPayment.Setup(x => x.Guarantor).Returns(guarantor.Object);
            returnedPayment.Setup(x => x.TransactionId).Returns(achSettlementMessageGuarantor.Messages[0].TransactionId);
            returnedPayment.Setup(x => x.PaymentAllocations).Returns(new List<PaymentAllocation>());
            returnedPayment.Setup(x => x.PaymentStatus).Returns(paymentStatus);
            returnedPayment.Setup(x => x.PaymentMethod).Returns(() =>
            {
                Mock<PaymentMethod> paymentMethod = new Mock<PaymentMethod>();
                paymentMethod.Setup(x => x.IsAchType).Returns(true);
                return paymentMethod.Object;
            });

            AchServiceMockBuilder achServiceMockBuilder = new AchServiceMockBuilder();
            achServiceMockBuilder.AchSettlementDetailRepositoryMock.Setup(x => x.GetById(achSettlementDetail.AchSettlementDetailId)).Returns(achSettlementDetail);

            AchApplicationServiceMockBuilder builder = new AchApplicationServiceMockBuilder
            {
                AchService = achServiceMockBuilder.CreateService()
            };

            builder.PaymentServiceMock.Setup(x => x.GetPaymentsByTransactionId(
                    achSettlementMessageGuarantor.Messages[0].TransactionId,
                    It.IsAny<IList<PaymentStatusEnum>>()))
                .Returns(returnedPayment.Object.ToListOfOne());
            
            builder.VpStatementServiceMock.Setup(x => x.GetMostRecentStatement(It.IsAny<Guarantor>())).Returns(() => new VpStatement());

            IAchApplicationService svc = builder.CreateService();
            svc.ProcessAchSettlement(achSettlementMessageGuarantor);
            
            // verify that settlement detail is marked as settled
            Assert.NotNull(achSettlementDetail.ProcessedDate);

            // verify that payment is settled when it's PendingACH 
            Times settles = paymentStatus == PaymentStatusEnum.ActivePendingACHApproval ? Times.Once() : Times.Never();
            builder.PaymentServiceMock.Verify(x => x.SettleAchPayment(It.IsAny<Payment>(), AchSettlementTypeEnum.FileSettlement), settles);

            // verify that payment post processing is called when it's PendingACH
            Times postProcess = paymentStatus == PaymentStatusEnum.ActivePendingACHApproval ? Times.Once() : Times.Never();
            builder.PaymentServiceMock.Verify(x => x.PostProcessPayment(
                    It.IsAny<Guarantor>(),
                    It.IsAny<VpStatement>(),
                    It.IsAny<DateTime>(),
                    It.IsAny<IList<Payment>>(),
                    It.IsAny<IList<Payment>>()),
                postProcess);
        }
    }
}
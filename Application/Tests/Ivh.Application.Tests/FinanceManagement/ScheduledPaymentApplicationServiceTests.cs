﻿namespace Ivh.Application.Tests.FinanceManagement
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Application.FinanceManagement.ApplicationServices;
    using Common.Base.Utilities.Extensions;
    using Common.Tests.Helpers.Payment;
    using Common.VisitPay.Enums;
    using Common.VisitPay.Messages.FinanceManagement;
    using Domain.FinanceManagement.FinancePlan.Entities;
    using Domain.FinanceManagement.Payment.Entities;
    using Domain.FinanceManagement.Statement.Entities;
    using Domain.Guarantor.Entities;
    using Moq;
    using NUnit.Framework;

    [TestFixture]
    public class ScheduledPaymentApplicationServiceTests : ApplicationTestBase
    {
        private const int PaymentPendingReminderClientSettingValue = 3;

        [TestCase(4)]
        [TestCase(3)]
        [TestCase(2)]
        [TestCase(1)]
        [TestCase(0)]
        public void ScheduledPaymentReminders_ManualPayment_OnlySendForPaymentDate(int addDaysToPaymentDate)
        {
            const int vpGuarantorId = 1;

            ScheduledPaymentApplicationServiceMockBuilder builder = new ScheduledPaymentApplicationServiceMockBuilder();
            DateTime checkDate = DateTime.UtcNow.AddDays(PaymentPendingReminderClientSettingValue);

            List<Payment> scheduledPayments = new List<Payment>
            {
                new Payment
                {
                    PaymentId = 1,
                    Guarantor = new Guarantor {VpGuarantorId = vpGuarantorId},
                    ScheduledPaymentDate = DateTime.UtcNow.AddDays(addDaysToPaymentDate)
                }
            };

            builder.GuarantorServiceMock.Setup(x => x.GetGuarantor(vpGuarantorId)).Returns(new Guarantor {VpGuarantorId = vpGuarantorId});
            builder.PaymentServiceMock.Setup(x => x.GetScheduledPaymentsWithExactDate(checkDate.Date)).Returns(scheduledPayments);
            builder.FinancePlanServiceMock.Setup(x => x.GetFinancePlanAmountDueInfoWithRecurringPaymentDueExactDate(checkDate.Date)).Returns(() => new List<FinancePlanAmountDueInfo>());

            ScheduledPaymentApplicationService service = (ScheduledPaymentApplicationService) builder.CreateService();
            IEnumerable<SendPendingPaymentEmailMessage> messages = service.GetScheduledPaymentRemindersToQueue(checkDate);

            if (scheduledPayments.First().ScheduledPaymentDate.Date == checkDate.Date)
            {
                Console.WriteLine($"sent for {addDaysToPaymentDate}");
                Assert.AreEqual(1, messages.Count());
            }
            else
            {
                Console.WriteLine($"not sent for {addDaysToPaymentDate}");
                Assert.AreEqual(0, messages.Count());
            }
        }

        [TestCase(4, 100.00)]
        [TestCase(3, 100.00)]
        [TestCase(2, 100.00)]
        [TestCase(1, 100.00)]
        [TestCase(0, 100.00)]
        [TestCase(4, 0)]
        [TestCase(3, 0)]
        [TestCase(2, 0)]
        [TestCase(1, 0)]
        [TestCase(0, 0)]
        public void ScheduledPaymentReminders_RecurringPayment_OnlySendForPaymentDate_WithAmountDue(int addDaysToPaymentDate, decimal amountDue)
        {
            const int vpGuarantorId = 1;

            ScheduledPaymentApplicationServiceMockBuilder builder = new ScheduledPaymentApplicationServiceMockBuilder();
            DateTime checkDate = DateTime.UtcNow.AddDays(PaymentPendingReminderClientSettingValue);

            List<FinancePlanAmountDueInfo> financePlanPayments = new List<FinancePlanAmountDueInfo>
            {
                new FinancePlanAmountDueInfo
                {
                    VpGuarantorId = vpGuarantorId,
                    StatementFinancePlanAmountDueDueDate = DateTime.UtcNow.AddDays(addDaysToPaymentDate),
                    TotalAmountDue = amountDue
                }
            };

            builder.GuarantorServiceMock.Setup(x => x.GetGuarantor(vpGuarantorId)).Returns(new Guarantor {VpGuarantorId = vpGuarantorId});
            builder.PaymentServiceMock.Setup(x => x.GetScheduledPaymentsWithExactDate(checkDate.Date)).Returns(() => new List<Payment>());
            builder.FinancePlanServiceMock.Setup(x => x.GetFinancePlanAmountDueInfoWithRecurringPaymentDueExactDate(checkDate.Date)).Returns(financePlanPayments);

            ScheduledPaymentApplicationService service = (ScheduledPaymentApplicationService) builder.CreateService();
            IEnumerable<SendPendingPaymentEmailMessage> messages = service.GetScheduledPaymentRemindersToQueue(checkDate);

            if (financePlanPayments.First().StatementFinancePlanAmountDueDueDate.GetValueOrDefault(DateTime.MaxValue).Date == checkDate.Date &&
                financePlanPayments.First().TotalAmountDue > 0m)
            {
                Console.WriteLine($"sent for {addDaysToPaymentDate}");
                Assert.AreEqual(1, messages.Count());
            }
            else
            {
                Console.WriteLine($"not sent for {addDaysToPaymentDate}");
                Assert.AreEqual(0, messages.Count());
            }
        }

        [Test]
        public void ScheduledPaymentReminders_RecurringPayment_GroupsByGuarantor()
        {
            const int vpGuarantorId = 1;

            ScheduledPaymentApplicationServiceMockBuilder builder = new ScheduledPaymentApplicationServiceMockBuilder();
            DateTime checkDate = DateTime.UtcNow.AddDays(3);

            Payment payment = new Payment
            {
                PaymentId = 1,
                Guarantor = new Guarantor {VpGuarantorId = vpGuarantorId},
                ScheduledPaymentDate = DateTime.UtcNow.AddDays(PaymentPendingReminderClientSettingValue)
            };

            List<Payment> scheduledPayments = new List<Payment>
            {
                payment,
                payment
            };

            FinancePlanAmountDueInfo fpPayment = new FinancePlanAmountDueInfo
            {
                VpGuarantorId = vpGuarantorId,
                StatementFinancePlanAmountDueDueDate = DateTime.UtcNow.AddDays(PaymentPendingReminderClientSettingValue),
                TotalAmountDue = 100m
            };

            List<FinancePlanAmountDueInfo> financePlanPayments = new List<FinancePlanAmountDueInfo>
            {
                fpPayment,
                fpPayment
            };

            builder.GuarantorServiceMock.Setup(x => x.GetGuarantor(vpGuarantorId)).Returns(new Guarantor {VpGuarantorId = vpGuarantorId});
            builder.PaymentServiceMock.Setup(x => x.GetScheduledPaymentsWithExactDate(checkDate.Date)).Returns(scheduledPayments);
            builder.FinancePlanServiceMock.Setup(x => x.GetFinancePlanAmountDueInfoWithRecurringPaymentDueExactDate(checkDate.Date)).Returns(financePlanPayments);

            ScheduledPaymentApplicationService service = (ScheduledPaymentApplicationService) builder.CreateService();
            IEnumerable<SendPendingPaymentEmailMessage> messages = service.GetScheduledPaymentRemindersToQueue(checkDate);

            Assert.AreEqual(1, messages.Count());
        }

        [TestCase(GuarantorTypeEnum.Offline, true)]
        [TestCase(GuarantorTypeEnum.Offline, false)]
        [TestCase(GuarantorTypeEnum.Online, true)]
        public void ProcessScheduledPayment_ProcessRecurringPayment(GuarantorTypeEnum guarantorType, bool guarantorUseAutoPay)
        {
            const decimal financePlanAmountDue = 9999m;
            const int financePlanId = 1234;
            const int vpStatementId = 4567;
            const int vpGuarantorId = 9876;
            DateTime dateToProcess = DateTime.UtcNow;
            DateTime paymentDueDate = DateTime.UtcNow.Date;

            Guarantor guarantor = new Guarantor
            {
                VpGuarantorId = vpGuarantorId,
                UseAutoPay = guarantorUseAutoPay
            };
            guarantor.SetGuarantorType(guarantorType);

            VpStatementForProcessPaymentResult result = new VpStatementForProcessPaymentResult
            {
                PaymentDueDate = paymentDueDate,
                ProcessingStatus = ChangeEventStatusEnum.Queued,
                VpGuarantorId = vpGuarantorId,
                VpStatementId = vpStatementId
            };

            IList<FinancePlan> financePlans = new List<FinancePlan>
            {
                new FinancePlan 
                { 
                    VpGuarantor = guarantor,
                    FinancePlanId = financePlanId
                }
            };

            ScheduledPaymentApplicationServiceMockBuilder builder = new ScheduledPaymentApplicationServiceMockBuilder();
            builder.Setup(b =>
            {
                b.FinancePlanServiceMock.Setup(x => x.GetFinancePlansDueForRecurringPaymentByGuarantor(dateToProcess, vpGuarantorId)).Returns(financePlans);
                b.PaymentServiceMock.Setup(x => x.CreateRecurringPayment(financePlanId, guarantor, financePlanAmountDue, null)).Returns(new Payment());
                b.PaymentServiceMock.Setup(x => x.ProcessRecurringPaymentsForGuarantor(It.IsAny<IList<Payment>>(), guarantor, dateToProcess, null)).Returns(new List<Payment>{ new Payment() });
            });

            ScheduledPaymentApplicationService svc = (ScheduledPaymentApplicationService) builder.CreateService();
            IList<Payment> paymentsProcessed = svc.ProcessRecurringPayments(guarantor, dateToProcess, result, null);

            bool isNonAutoPayOfflineGuarantor = guarantor.IsOfflineGuarantor() && !guarantor.UseAutoPay;
            if (isNonAutoPayOfflineGuarantor)
            {
                builder.StatementServiceMock.Verify(x => x.MarkStatementAsProcessed(vpStatementId, vpGuarantorId, It.IsAny<string>()), Times.Once());
                Assert.True(paymentsProcessed.IsNullOrEmpty());
            }
            else
            {
                builder.StatementServiceMock.Verify(x => x.MarkStatementAsProcessed(vpStatementId, vpGuarantorId, It.IsAny<string>()), Times.Never());
                Assert.True(paymentsProcessed.Count == 1);
            }
        }
    }
}
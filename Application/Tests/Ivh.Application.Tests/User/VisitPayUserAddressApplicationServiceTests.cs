﻿namespace Ivh.Application.Tests.User
{
    using Application.Base.Common.Dtos;
    using Application.User.Common.Interfaces;
    using Common.Base.Utilities.Extensions;
    using Common.Tests.Helpers.User;
    using Common.VisitPay.Enums;
    using Domain.User.Entities;
    using Domain.User.Interfaces;
    using Moq;
    using Newtonsoft.Json;
    using NUnit.Framework;
    using Provider.User;
    using System.Collections.Generic;
    using System.Threading.Tasks;

    [TestFixture]
    public class VisitPayUserAddressApplicationServiceTests : ApplicationTestBase
    {
        private const string Address2 = "517 S MIDDLETON RD";
        private const string City = "MIDDLETON";
        private const string State = "ID";
        private const string Zip5 = "83644";
        private const string Zip4 = "5992";

        [Test]
        public async Task GetAddress_UspsApiCallSucceeds_AddressDtoIncludesUspsAddress()
        {
            VisitPayUser user = CreateVisitPayUser();
            AddressValidationSuccessResponse successResponse = CreateAddressValidationSuccessResponse();
            string serializedJson = JsonConvert.SerializeObject(successResponse.Address);
            DataResult<IEnumerable<string>, bool> dataResult = CreateDataResult(serializedJson);

            Mock<IVisitPayUserService> vpUserSvcMock = new Mock<IVisitPayUserService>();
            Mock<IVisitPayUserAddressVerificationService> vpUserAddressVerificationSvcMock = new Mock<IVisitPayUserAddressVerificationService>();

            vpUserSvcMock
                .Setup(s => s.FindUserById(It.IsAny<int>()))
                .Returns(user);

            vpUserAddressVerificationSvcMock
                .Setup(s => s.GetStandardizedMailingAddressAsync(
                    It.IsAny<int>(),
                    It.IsAny<VisitPayUserAddress>(),
                    It.IsAny<bool>()))
                .ReturnsAsync(dataResult);

            VisitPayUserAddressApplicationServiceMockBuilder svcMockBuilder = new VisitPayUserAddressApplicationServiceMockBuilder();

            svcMockBuilder
                .Setup(builder =>
                {
                    builder.VisitPayUserService = vpUserSvcMock.Object;
                    builder.VisitPayUserAddressVerificationService = vpUserAddressVerificationSvcMock.Object;
                });

            IVisitPayUserAddressApplicationService vpUserAddressAppSvc = svcMockBuilder.CreateService();

            AddressDto address = await vpUserAddressAppSvc.GetAddressAsync(1234, VisitPayUserAddressTypeEnum.Mailing);

            Assert.AreEqual(null, address.UspsAddressStreet1);
            Assert.AreEqual(Address2, address.UspsAddressStreet2);
            Assert.AreEqual(City, address.UspsCity);
            Assert.AreEqual(State, address.UspsStateProvince);
        }

        private static DataResult<IEnumerable<string>, bool> CreateDataResult(string serializedJson)
        {
            return new DataResult<IEnumerable<string>, bool>
            {
                Data = serializedJson.ToListOfOne(),
                Result = true
            };
        }

        private static AddressValidationSuccessResponse CreateAddressValidationSuccessResponse()
        {
            return new AddressValidationSuccessResponse
            {
                Address = new AddressValidationResponseAddress
                {
                    Address1 = null,
                    Address2 = Address2,
                    City = City,
                    State = State,
                    Zip5 = Zip5,
                    Zip4 = Zip4
                }
            };
        }

        private static VisitPayUser CreateVisitPayUser()
        {
            return new VisitPayUser
            {
                CurrentMailingAddress = new VisitPayUserAddress
                {
                    AddressStreet1 = Address2,
                    City = City,
                    StateProvince = State,
                    PostalCode = Zip5
                }
            };
        }
    }
}
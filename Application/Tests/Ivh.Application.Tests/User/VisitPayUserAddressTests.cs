﻿namespace Ivh.Application.Tests.User
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using Common.VisitPay.Enums;
    using Domain.User.Entities;
    using NUnit.Framework;

    [TestFixture]
    public class VisitPayUserAddressTests : RepositoryTestBase
    {
        public VisitPayUserAddressTests() : base(typeof(VisitPayUser))
        {
        }

        [Test]
        public void Nhibernate_Mapping_Testing_Inserts_CurrentAddress()
        {
            VisitPayUser user = new VisitPayUser
            {
                FirstName = "John",
                LastName = "Doe",
                UserName = "John.Doe"
            };
            VisitPayUserAddress address = new VisitPayUserAddress
            {
                AddressStreet1 = "foo street",
                AddressStreet2 = "suite bar",
                City = "Foobaz",
                IsPrimary = true,
                PostalCode = "88555",
                StateProvince = "Idaho",
                VisitPayUser = user,
                VisitPayUserAddressType = VisitPayUserAddressTypeEnum.Physical
            };

            user.SetCurrentAddress(address);

            this.Session.Save(user);

            VisitPayUser fetchedUser = this.Session.Query<VisitPayUser>().FirstOrDefault();


            VisitPayUserAddress fetchedCurrentAddress = fetchedUser.CurrentPhysicalAddress;
            Assert.NotNull(fetchedCurrentAddress);

            List<string> errors = this.EnitiesMatch(address, fetchedCurrentAddress);
            Assert.True(errors.Count == 0, string.Join(Environment.NewLine, errors));
        }


        [Test]
        public void Nhibernate_Mapping_Testing_Saves_CurrentAddress()
        {
            DateTime now = DateTime.UtcNow;

            VisitPayUser user = new VisitPayUser
            {
                FirstName = "John",
                LastName = "Doe",
                UserName = "John.Doe"
            };

            VisitPayUserAddress address = new VisitPayUserAddress
            {
                InsertDate = now.AddDays(-2),
                AddressStreet1 = "foo street",
                AddressStreet2 = "suite bar",
                City = "Foobaz",
                IsPrimary = true,
                PostalCode = "88555",
                StateProvince = "Idaho",
                VisitPayUser = user,
                VisitPayUserAddressType = VisitPayUserAddressTypeEnum.Physical
            };

            user.SetCurrentAddress(address);
            this.Session.Save(user);

            VisitPayUserAddress address2 = new VisitPayUserAddress
            {
                InsertDate = now.AddDays(-1),
                AddressStreet1 = "123 Easy Street",
                AddressStreet2 = "suite bar",
                City = "Foobaz",
                IsPrimary = true,
                PostalCode = "88555",
                StateProvince = "Idaho",
                VisitPayUser = user,
                VisitPayUserAddressType = VisitPayUserAddressTypeEnum.Physical
            };

            user.SetCurrentAddress(address2);
            this.Session.Save(user);

            VisitPayUserAddress address3 = new VisitPayUserAddress
            {
                AddressStreet1 = "456 Easy Street",
                AddressStreet2 = "suite bar",
                City = "Foobaz",
                IsPrimary = true,
                PostalCode = "88555",
                StateProvince = "Idaho",
                VisitPayUser = user,
                VisitPayUserAddressType = VisitPayUserAddressTypeEnum.Physical
            };
            user.SetCurrentAddress(address3);
            this.Session.Save(user);

            VisitPayUser fetchedUser = this.Session.Query<VisitPayUser>().FirstOrDefault();

            Assert.IsTrue(fetchedUser.VisitPayUserAddresses.Count == 3);
            VisitPayUserAddress fetchedCurrentAddress = fetchedUser.CurrentPhysicalAddress;


            Assert.AreEqual(address3.AddressStreet1, fetchedCurrentAddress.AddressStreet1);

            VisitPayUserAddress firstAddress = fetchedUser.VisitPayUserAddresses.OrderBy(x => x.InsertDate).FirstOrDefault();

            Assert.AreEqual(address.AddressStreet1, firstAddress.AddressStreet1);
        }

        private List<string> EnitiesMatch<TEntity>(TEntity original, TEntity fetched)
        {
            List<string> errors = new List<string>();
            PropertyInfo[] propertyInfosEntity = typeof(TEntity).GetProperties();


            foreach (PropertyInfo propertyInfo in propertyInfosEntity)
            {
                object originalFieldValue = original.GetType().GetProperty(propertyInfo.Name)?.GetValue(original, null);
                object fetchedFieldValue = fetched.GetType().GetProperty(propertyInfo.Name)?.GetValue(fetched, null);

                if (originalFieldValue != null && !originalFieldValue.Equals(fetchedFieldValue))
                {
                    errors.Add($"Original entity {propertyInfo.Name} did not match fetched entity {propertyInfo.Name}");
                }
            }

            return errors;
        }
    }
}
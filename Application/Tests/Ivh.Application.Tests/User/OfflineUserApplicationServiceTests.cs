﻿namespace Ivh.Application.Tests.User
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Security.Claims;
    using Application.User.Common.Dtos;
    using Application.User.Common.Interfaces;
    using Common.Base.Utilities.Extensions;
    using Common.Encryption.Enums;
    using Common.EventJournal;
    using Common.ServiceBus.Interfaces;
    using Common.Tests.Builders;
    using Common.Tests.Helpers.User;
    using Common.VisitPay.Enums;
    using Common.VisitPay.Messages.FinanceManagement;
    using Common.Web.Extensions;
    using Domain.Guarantor.Entities;
    using Domain.User.Entities;
    using Domain.User.Interfaces;
    using Microsoft.AspNet.Identity;
    using Moq;
    using NUnit.Framework;

    [TestFixture]
    public class OfflineUserApplicationServiceTests : ApplicationTestBase
    {
        [TestCase(GuarantorTypeEnum.Offline, true)]
        [TestCase(GuarantorTypeEnum.Online, false)]
        public void FindById(GuarantorTypeEnum guarantorType, bool expectedResult)
        {
            Guarantor guarantor = this.CreateTestGuarantor(guarantorType);
            IOfflineUserApplicationService service = this.CreateBuilder(true, guarantor).CreateService();
            VisitPayUserDto user = service.FindById(guarantor.User.VisitPayUserId);

            Assert.AreEqual(expectedResult, user != null);
        }

        [TestCase(GuarantorTypeEnum.Offline, true)]
        [TestCase(GuarantorTypeEnum.Online, false)]
        public void FindByGuarantorId(GuarantorTypeEnum guarantorType, bool expectedResult)
        {
            Guarantor guarantor = this.CreateTestGuarantor(guarantorType);
            IOfflineUserApplicationService service = this.CreateBuilder(true, guarantor).CreateService();
            VisitPayUserDto user = service.FindByGuarantorId(guarantor.VpGuarantorId);

            Assert.AreEqual(expectedResult, user != null);
        }

        [Test]
        public void SignIn_FeatureDisabled()
        {
            Guarantor guarantor = this.CreateTestGuarantor();
            IOfflineUserApplicationService service = this.CreateBuilder(false, guarantor).CreateService();

            Assert.Throws<Exception>(() => service.SignIn("", DateTime.UtcNow, "", null));
        }

        [Test]
        public void SignIn_NotOffline()
        {
            Guarantor guarantor = this.CreateTestGuarantor(GuarantorTypeEnum.Online);
            IOfflineUserApplicationService service = this.CreateBuilder(true, guarantor).CreateService();

            SignInResultDto result = service.SignIn(guarantor.User.LastName, guarantor.User.DateOfBirth ?? DateTime.UtcNow, "", new JournalEventHttpContextDto());
            Assert.AreEqual(SignInStatusExEnum.Failure, result.SignInStatus);
        }

        [Test]
        public void SignIn_NotFoundWithToken()
        {
            Guarantor guarantor = this.CreateTestGuarantor();

            OfflineUserApplicationServiceMockBuilder builder = this.CreateBuilder(true, guarantor, passwordVerifies: false);
            IOfflineUserApplicationService service = builder.CreateService();

            SignInResultDto result = service.SignIn(guarantor.User.LastName, guarantor.User.DateOfBirth ?? DateTime.UtcNow, "", new JournalEventHttpContextDto());
            Assert.AreEqual(SignInStatusExEnum.Failure, result.SignInStatus);
        }

        [Test]
        public void SignIn_TokenExpired()
        {
            Guarantor guarantor = this.CreateTestGuarantor();

            OfflineUserApplicationServiceMockBuilder builder = this.CreateBuilder(true, guarantor, passwordValidates: false);
            IOfflineUserApplicationService service = builder.CreateService();

            SignInResultDto result = service.SignIn(guarantor.User.LastName, guarantor.User.DateOfBirth ?? DateTime.UtcNow, "", new JournalEventHttpContextDto());
            Assert.AreEqual(SignInStatusExEnum.PasswordExpired, result.SignInStatus);
        }

        [Test]
        public void SignIn_Success()
        {
            Guarantor guarantor = this.CreateTestGuarantor();

            ClaimsIdentity identity = new ClaimsIdentity();

            OfflineUserApplicationServiceMockBuilder builder = this.CreateBuilder(true, guarantor);
            builder.VisitPayUserServiceMockBuilder.WithLoginIdentity(identity);
            IOfflineUserApplicationService service = builder.CreateService();

            SignInResultDto result = service.SignIn(guarantor.User.LastName, guarantor.User.DateOfBirth ?? DateTime.UtcNow, "", new JournalEventHttpContextDto());
            Assert.AreEqual(SignInStatusExEnum.Success, result.SignInStatus);
            Assert.AreEqual(4, identity.Claims.Count());
            Assert.IsTrue(identity.HasClaim(ClaimTypeEnum.OfflineUserToken));
            Assert.IsTrue(identity.HasClaim(ClaimTypeEnum.GuarantorId));
            Assert.IsTrue(identity.HasClaim(ClaimTypeEnum.ClientFirstName));
            Assert.IsTrue(identity.HasClaim(ClaimTypeEnum.ClientLastName));
            Assert.IsTrue(guarantor.User.TempPasswordExpDate <= DateTime.UtcNow);
        }

        [Test]
        public void SignIn_TokenIsAccessTokenPhi_Success()
        {
            ClaimsIdentity identity = new ClaimsIdentity();
            Guarantor guarantor = this.CreateTestGuarantor();
            string lastName = guarantor.User.LastName;
            DateTime dateOfBirth = guarantor.User.DateOfBirth ?? DateTime.UtcNow;
            guarantor.User.GenerateAndSetAccessTokenPhi();
            string accessTokenPhi = guarantor.User.AccessTokenPhi;
            JournalEventHttpContextDto journalEventCtx = new JournalEventHttpContextDto();

            OfflineUserApplicationServiceMockBuilder builder = this.CreateBuilder(true, guarantor);
            builder.VisitPayUserServiceMockBuilder.WithLoginIdentity(identity);
            builder.VisitPayUserServiceMockBuilder.WithUserMatchedOnDetailsAndAccessTokenPhi(guarantor.User);
            IOfflineUserApplicationService service = builder.CreateService();

            SignInResultDto result = service.SignIn(lastName, dateOfBirth, accessTokenPhi, journalEventCtx);

            Assert.AreEqual(SignInStatusExEnum.Success, result.SignInStatus);
            Assert.AreEqual(4, identity.Claims.Count());
            Assert.IsTrue(identity.HasClaim(ClaimTypeEnum.OfflineUserToken));
            Assert.IsTrue(identity.HasClaim(ClaimTypeEnum.GuarantorId));
            Assert.IsTrue(identity.HasClaim(ClaimTypeEnum.ClientFirstName));
            Assert.IsTrue(identity.HasClaim(ClaimTypeEnum.ClientLastName));
            Assert.IsTrue(guarantor.User.TempPasswordExpDate <= DateTime.UtcNow);
        }

        [Test]
        public void SendFinancePlanNotification_FeatureDisabled()
        {
            Guarantor guarantor = this.CreateTestGuarantor();
            IOfflineUserApplicationService service = this.CreateBuilder(false, guarantor).CreateService();

            SendOfflineTokenDto tokenDto = new SendOfflineTokenDto(guarantor.VpGuarantorId, guarantor.User.Email);

            bool result = service.SendFinancePlanNotification(tokenDto);
            Assert.False(result);
        }

        [Test]
        public void SendFinancePlanNotification_NotOfflineUser()
        {
            Guarantor guarantor = this.CreateTestGuarantor(GuarantorTypeEnum.Online);

            IOfflineUserApplicationService service = this.CreateBuilder(true, guarantor).CreateService();

            SendOfflineTokenDto tokenDto = new SendOfflineTokenDto(guarantor.VpGuarantorId, guarantor.User.Email);
            bool result = service.SendFinancePlanNotification(tokenDto);
            Assert.False(result);
        }

        [TestCase(false)]
        [TestCase(true)]
        public void SendFinancePlanNotification_Sends(bool sendSms)
        {
            Guarantor guarantor = this.CreateTestGuarantor();

            Mock<IBus> busMock = new Mock<IBus>();

            OfflineUserApplicationServiceMockBuilder builder = this.CreateBuilder(true, guarantor, busMock);

            Mock<IUserTokenProvider<VisitPayUser, string>> tokenProviderMock = new Mock<IUserTokenProvider<VisitPayUser, string>>();
            tokenProviderMock.Setup(s => s.GenerateAsync(It.IsAny<string>(), It.IsAny<UserManager<VisitPayUser, string>>(), It.IsAny<VisitPayUser>())).ReturnsAsync("anotnullvalue");
            builder.VisitPayUserServiceMockBuilder.UserTokenProvider = tokenProviderMock.Object;

            IOfflineUserApplicationService service = builder.CreateService();

            const string updatedEmail = "test1@test.com";
            SendOfflineTokenDto tokenDto = new SendOfflineTokenDto(guarantor.VpGuarantorId, updatedEmail)
            {
                SendSms = sendSms,
                PhoneNumber = "123-123-1234"
            };

            bool result = service.SendFinancePlanNotification(tokenDto);
            Assert.True(result);
            busMock.Verify(x => x.PublishMessage(It.IsAny<SendVpccFinancePlanCreationEmailMessage>()), Times.Once);
            busMock.Verify(x => x.PublishMessage(It.IsAny<SendVpccFinancePlanCreationSmsMessage>()), sendSms ? Times.Once() : Times.Never());
            Assert.AreEqual(updatedEmail, guarantor.User.Email);
            Assert.IsNull(guarantor.User.PhoneNumber);
        }

        [Test]
        public void SignIn_ValidCredentials_JournalEventsAddedToEventJournalService()
        {
            Guarantor guarantor = this.CreateTestGuarantor();
            Mock<IVisitPayUserJournalEventService> vpUserJournalEventServiceMock = new Mock<IVisitPayUserJournalEventService>();
            OfflineUserApplicationServiceMockBuilder builder = this.CreateBuilder(true, guarantor);
            builder.VisitPayUserJournalEventServiceMock = vpUserJournalEventServiceMock;
            builder.VisitPayUserServiceMockBuilder.WithMatchingUsers(guarantor.User.ToListOfOne().ToList());
            builder.VisitPayUserServiceMockBuilder.WithLoginIdentity(new ClaimsIdentity());
            IOfflineUserApplicationService offlineUserAppSvc = builder.CreateService();

            offlineUserAppSvc.SignIn("last", DateTime.Today.AddYears(-50), "theToken", new JournalEventHttpContextDto());

            vpUserJournalEventServiceMock.Verify(s => s.LogGuarantorSelfVerification(It.IsAny<JournalEventUserContext>(), It.IsAny<int>()), Times.AtLeastOnce);
        }

        private OfflineUserApplicationServiceMockBuilder CreateBuilder(bool offlineVisitPayEnabled, Guarantor guarantor, Mock<IBus> bus = null, bool passwordVerifies = true, bool passwordValidates = true)
        {
            OfflineUserApplicationServiceMockBuilder builder = new OfflineUserApplicationServiceMockBuilder();
            builder.SetFeature(VisitPayFeatureEnum.OfflineVisitPay, offlineVisitPayEnabled);

            if (bus != null)
            {
                builder.ApplicationServiceCommonServiceMockBuilder.Setup(b => { b.BusMock = bus; });
            }

            builder.GuarantorServiceMock.Setup(x => x.GetGuarantorId(guarantor.User.VisitPayUserId)).Returns(guarantor.VpGuarantorId);
            builder.GuarantorServiceMock.Setup(x => x.GetGuarantor(guarantor.VpGuarantorId)).Returns(guarantor);
            builder.GuarantorServiceMock.Setup(x => x.GetGuarantorEx(guarantor.User.VisitPayUserId, It.IsAny<int?>())).Returns(guarantor);
            builder.VisitPayUserServiceMockBuilder.WithUser(guarantor.User);
            builder.VisitPayUserServiceMockBuilder.VisitPayUserRepositoryMock.Setup(s => s.FindByNameAndDob(guarantor.User.LastName, guarantor.User.DateOfBirth ?? DateTime.UtcNow)).Returns(guarantor.User.ToListOfOne().ToList());
            builder.VisitPayUserServiceMockBuilder.VisitPayUserPasswordHistoryRepositoryMock.Setup(s => s.GetUserPasswordHistory(It.IsAny<int>(), It.IsAny<int>())).Returns(new List<VisitPayUserPasswordHistory>());
            builder.VisitPayUserServiceMockBuilder.PasswordHasherServiceMock.Setup(s => s.VerifyHashedPassword(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<int>(), It.IsAny<int>(), It.IsAny<HashEnum>())).Returns(passwordVerifies);
            builder.VisitPayUserServiceMockBuilder.UserTokenProviderMock.Setup(x => x.ValidateAsync(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<UserManager<VisitPayUser, string>>(), It.IsAny<VisitPayUser>())).ReturnsAsync(passwordValidates);

            return builder;
        }

        private Guarantor CreateTestGuarantor(GuarantorTypeEnum guarantorType = GuarantorTypeEnum.Offline)
        {
            return new GuarantorBuilder()
                .WithDefaults(1)
                .WithType(guarantorType)
                .WithUser(new VisitPayUser
                {
                    DateOfBirth = new DateTime(1985, 05, 21),
                    FirstName = "first",
                    LastName = "last",
                    VisitPayUserId = 2,
                    Email = "test@test.com"
                });
        }
    }
}

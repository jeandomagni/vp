﻿namespace Ivh.Application.Tests.User
{
    using System;
    using Application.Core.Common.Dtos;
    using Application.Core.Common.Interfaces;
    using Application.User.Common.Dtos;
    using AutoMapper;
    using Common.Tests.Helpers;
    using Common.Tests.Helpers.Base;
    using Common.Tests.Helpers.Client;
    using Common.Tests.Helpers.User;
    using Common.VisitPay.Enums;
    using Common.VisitPay.Strings;
    using Domain.Guarantor.Entities;
    using Domain.Settings.Entities;
    using Domain.Settings.Interfaces;
    using Domain.User.Entities;
    using Moq;
    using NUnit.Framework;

    [TestFixture]
    public class VisitPayUserApplicationServiceTests : ApplicationTestBase
    {
        #region offline

        [Test]
        public void CreateOfflinePatient_FeatureDisabled()
        {
            VisitPayUserApplicationServiceMockBuilder builder = new VisitPayUserApplicationServiceMockBuilder();
            builder.SetFeature(VisitPayFeatureEnum.OfflineVisitPay, false);
            
            IVisitPayUserApplicationService service = builder.CreateService();

            Assert.Throws(typeof(Exception), () => service.CreateOfflinePatient(new VisitPayUserDto(), string.Empty, DateTime.MinValue, null));
        }

        [Test]
        public void CreateOfflinePatient_Creates()
        {
            const string registrationMatchString = "1-1";

            VisitPayUserDto visitPayUserDto = new VisitPayUserDto
            {
                Email = "email@dotcom",
                UserName = "username"
            };

            IClientService clientService = new ClientServiceMockBuilder().CreateService();

            VisitPayUserApplicationServiceMockBuilder builder = new VisitPayUserApplicationServiceMockBuilder();
            builder.SetFeature(VisitPayFeatureEnum.OfflineVisitPay, true);
            builder.ApplicationServiceCommonServiceMockBuilder.Setup(x => x.ClientService = clientService);
            builder.VisitPayRoleServiceMock.Setup(x => x.GetRoleByName(VisitPayRoleStrings.System.Patient)).Returns(new VisitPayRole());
            builder.VisitPayUserServiceMockBuilder.Setup(x =>
            {
                DomainServiceCommonServiceMockBuilder common = new DomainServiceCommonServiceMockBuilder();
                common.ClientService = clientService;
                common.ApplicationSettingsServiceMock.Setup(s => s.IsClientApplication).Returns(new ApplicationSetting<bool>(() => new Lazy<bool>(() => false)));
                x.DomainServiceCommonService = common.CreateService();
            });
            builder.SessionMock = new SessionMockBuilder()
                .UseSynchronizations() // this lets the RegisterPostCommits fire
                .CreateMock();

            IVisitPayUserApplicationService service = builder.CreateService();

            GuarantorDto guarantorDto = service.CreateOfflinePatient(visitPayUserDto, registrationMatchString, new DateTime(1945,1,1), null);
            Assert.AreEqual(GuarantorTypeEnum.Offline, guarantorDto.VpGuarantorTypeEnum);

            builder.GuarantorServiceMock.Verify(x => x.SetToActiveVisitsLoaded(It.IsAny<Guarantor>()), Times.Once);
            builder.HsGuarantorServiceMock.Verify(x => x.AddInitialMatch(It.IsAny<Guarantor>(), registrationMatchString, It.IsAny<DateTime?>(), It.IsAny<HttpContextDto>()), Times.Once);
            builder.GuarantorEnrollmentApplicationServiceMock.Verify(x => x.PublishEnrollGuarantorMessageAsync(It.IsAny<int>()), Times.Once);
        }

        #endregion


        [Test]
        public void Automapper_Will_Map_With_Null_Current_Address()
        {
            VisitPayUser user = new VisitPayUser
            {
                FirstName = "John",
                LastName = "Doe",
                UserName = "John.Doe"
            };

            // assert that an exception is not thrown when given a null current address
            Assert.DoesNotThrow(() =>
            {
                VisitPayUserDto mappedUserDto = Mapper.Map<VisitPayUserDto>(user);
            });
        }
    }
}
﻿namespace Ivh.Application.Tests.HospitalData
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Common.Base.Utilities.Extensions;
    using Common.Tests.Helpers.HospitalData;
    using Ivh.Application.Core.Common.Dtos.Eob;
    using Ivh.Application.Core.Common.Interfaces;
    using Ivh.Application.Core.Common.Models.Eob;
    using Ivh.Domain.Eob.Entities;
    using Moq;
    using NHibernate.Hql.Ast.ANTLR.Tree;
    using NUnit.Framework;

    [TestFixture]
    public class HsVisitEobApplicationServiceTests : ApplicationTestBase
    {
        [Test]
        public void ClaimLevelPaymentInformation_Cas_Mia()
        {
            IList<Eob835Result> eob835 = this.GetBase835s("clp01");
            IList<ClaimPaymentInformation> claims = new List<ClaimPaymentInformation>();
            ClaimPaymentInformation claim = this.GetClaimPaymentInformation(MiaMoaEnum.Mia, "22");

            IList<ClaimAdjustmentPivot> caps = new List<ClaimAdjustmentPivot>();
            caps.Add(this.GetClaimAdjustmentPivot("CO", "253", 200m, "Plan Adjustment"));
            caps.Add(this.GetClaimAdjustmentPivot("CO", "P22", 50m, "Plan Adjustment"));
            caps.Add(this.GetClaimAdjustmentPivot("CO", "45", 800m, "Not covered"));
            caps.Add(this.GetClaimAdjustmentPivot("CO", "B15", 50m, "Not covered"));
            caps.Add(this.GetClaimAdjustmentPivot("OA", "94", 25m, "Not covered"));
            caps.Add(this.GetClaimAdjustmentPivot("PR", "3", 50, "Copayment"));
            caps.Add(this.GetClaimAdjustmentPivot("PR", "1", 1000, "Deductible"));
            claim.ClaimAdjustmentPivots = caps;
            claim.ServicePaymentInformations = new List<ServicePaymentInformation>();
            claims.Add(claim);
            eob835[0].ClaimPaymentInformations = claims;

            IVisitEobApplicationService service = new VisitEobApplicationServiceMockBuilder().Setup(builder =>
            {
                builder.ClaimPaymentInformationServiceMock.Setup(x => x.Get835Files(It.IsAny<int>(), It.IsAny<string>())).Returns(eob835);
                builder.ClaimPaymentInformationServiceMock.Setup(x => x.GetUiDisplay(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>()))
                    .Returns((string claimAdjustmentReasonCode, string uniquePayerValue, string claimStatusCode, string claimAdjustmentGroupCode) =>
                    {
                        return caps.FirstOrDefault(x => x.ClaimAdjustmentReasonCode == claimAdjustmentReasonCode && x.ClaimAdjustmentGroupCode == claimAdjustmentGroupCode).EobClaimAdjustmentReasonCode.UIDisplay;
                    });
            }).CreateService();

            IList<VisitEobDetailsResultDto> eobDetails = service.GetVisitEobDetails(1, "1");
            Assert.AreEqual(1, eobDetails.Count);
            Assert.AreEqual(eobDetails[0].ClaimNumber, "SH41981111-CLP01");

            Assert.AreEqual(4, eobDetails[0].ClaimLevelClaimAdjustmentsSummed.Count);

            Assert.AreEqual(250m, eobDetails[0].ClaimLevelClaimAdjustmentsSummed[0].MonetaryAmount);
            Assert.AreEqual("Plan Adjustment", eobDetails[0].ClaimLevelClaimAdjustmentsSummed[0].UIDisplay);

            Assert.AreEqual(875m, eobDetails[0].ClaimLevelClaimAdjustmentsSummed[1].MonetaryAmount);
            Assert.AreEqual("Not covered", eobDetails[0].ClaimLevelClaimAdjustmentsSummed[1].UIDisplay);

            Assert.AreEqual(50m, eobDetails[0].ClaimLevelClaimAdjustmentsSummed[2].MonetaryAmount);
            Assert.AreEqual("Copayment", eobDetails[0].ClaimLevelClaimAdjustmentsSummed[2].UIDisplay);

            Assert.AreEqual(1000m, eobDetails[0].ClaimLevelClaimAdjustmentsSummed[3].MonetaryAmount);
            Assert.AreEqual("Deductible", eobDetails[0].ClaimLevelClaimAdjustmentsSummed[3].UIDisplay);

            Assert.AreEqual(1, eobDetails[0].HealthCareRemarkCodes.Count);
            Assert.AreEqual("M141", eobDetails[0].HealthCareRemarkCodes[0].IndustryCode);
            Assert.AreEqual("Missing physician certified plan of care.", eobDetails[0].HealthCareRemarkCodes[0].Description);
        }

        [Test]
        public void ServiceLevelPaymentInformation_Cas_Moa()
        {
            IList<Eob835Result> eob835 = this.GetBase835s("clp07");
            IList<ClaimPaymentInformation> claims = new List<ClaimPaymentInformation>();
            ClaimPaymentInformation claim = this.GetClaimPaymentInformation(MiaMoaEnum.Moa, "22");

            IList<ClaimAdjustmentPivot> caps = new List<ClaimAdjustmentPivot>();
            caps.Add(this.GetClaimAdjustmentPivot("CO", "253", 100m, "Plan Adjustment"));
            caps.Add(this.GetClaimAdjustmentPivot("CO", "P22", 50m, "Plan Adjustment"));
            caps.Add(this.GetClaimAdjustmentPivot("CO", "45", 700m, "Not covered"));
            caps.Add(this.GetClaimAdjustmentPivot("CO", "B15", 50m, "Not covered"));
            caps.Add(this.GetClaimAdjustmentPivot("OA", "94", 20m, "Not covered"));
            caps.Add(this.GetClaimAdjustmentPivot("PR", "3", 20, "Copayment"));
            caps.Add(this.GetClaimAdjustmentPivot("PR", "1", 100, "Deductible"));

            IList<ServicePaymentInformation> serviceLevels = new List<ServicePaymentInformation>();
            serviceLevels.Add(new ServicePaymentInformation
            {
                ClaimAdjustmentPivots = caps
            });
            claim.ServicePaymentInformations = serviceLevels;
            claims.Add(claim);
            eob835[0].ClaimPaymentInformations = claims;

            IVisitEobApplicationService service = new VisitEobApplicationServiceMockBuilder().Setup(builder =>
            {
                builder.ClaimPaymentInformationServiceMock.Setup(x => x.Get835Files(It.IsAny<int>(), It.IsAny<string>())).Returns(eob835);
                builder.ClaimPaymentInformationServiceMock.Setup(x => x.GetUiDisplay(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>()))
                    .Returns((string claimAdjustmentReasonCode, string uniquePayerValue, string claimStatusCode, string claimAdjustmentGroupCode) =>
                    {
                        return caps.FirstOrDefault(x => x.ClaimAdjustmentReasonCode == claimAdjustmentReasonCode && x.ClaimAdjustmentGroupCode == claimAdjustmentGroupCode).EobClaimAdjustmentReasonCode.UIDisplay;
                    });
            }).CreateService();

            IList<VisitEobDetailsResultDto> eobDetails = service.GetVisitEobDetails(1, "1");
            Assert.AreEqual(1, eobDetails.Count);
            Assert.AreEqual(eobDetails[0].ClaimNumber, "SH41981111-CLP07");
            Assert.AreEqual(4, eobDetails[0].ServiceLevelClaimAdjustmentsSummed.Count);

            Assert.AreEqual(150m, eobDetails[0].ServiceLevelClaimAdjustmentsSummed[0].MonetaryAmount);
            Assert.AreEqual("Plan Adjustment", eobDetails[0].ServiceLevelClaimAdjustmentsSummed[0].UIDisplay);

            Assert.AreEqual(770m, eobDetails[0].ServiceLevelClaimAdjustmentsSummed[1].MonetaryAmount);
            Assert.AreEqual("Not covered", eobDetails[0].ServiceLevelClaimAdjustmentsSummed[1].UIDisplay);

            Assert.AreEqual(20m, eobDetails[0].ServiceLevelClaimAdjustmentsSummed[2].MonetaryAmount);
            Assert.AreEqual("Copayment", eobDetails[0].ServiceLevelClaimAdjustmentsSummed[2].UIDisplay);

            Assert.AreEqual(100m, eobDetails[0].ServiceLevelClaimAdjustmentsSummed[3].MonetaryAmount);
            Assert.AreEqual("Deductible", eobDetails[0].ServiceLevelClaimAdjustmentsSummed[3].UIDisplay);

            Assert.AreEqual(1, eobDetails[0].HealthCareRemarkCodes.Count);
            Assert.AreEqual("M23", eobDetails[0].HealthCareRemarkCodes[0].IndustryCode);
            Assert.AreEqual("Missing invoice.", eobDetails[0].HealthCareRemarkCodes[0].Description);
        }

        [Test]
        public void MultipleClaimLevels()
        {
            IList<Eob835Result> eob835 = this.GetBase835s("clp07");
            IList<ClaimPaymentInformation> claims = new List<ClaimPaymentInformation>();
            ClaimPaymentInformation claim1 = this.GetClaimPaymentInformation(MiaMoaEnum.None, "22");
            ClaimPaymentInformation claim2 = this.GetClaimPaymentInformation(MiaMoaEnum.None, "1");

            IList<ClaimAdjustmentPivot> caps1 = new List<ClaimAdjustmentPivot>();
            caps1.Add(this.GetClaimAdjustmentPivot("CO", "253", 100m, "Plan Adjustment"));
            caps1.Add(this.GetClaimAdjustmentPivot("CO", "P22", 50m, "Plan Adjustment"));
            caps1.Add(this.GetClaimAdjustmentPivot("CO", "45", 700m, "Not covered"));
            caps1.Add(this.GetClaimAdjustmentPivot("CO", "B15", 50m, "Not covered"));
            caps1.Add(this.GetClaimAdjustmentPivot("OA", "94", 20m, "Not covered"));
            caps1.Add(this.GetClaimAdjustmentPivot("PR", "3", 20, "Copayment"));
            caps1.Add(this.GetClaimAdjustmentPivot("PR", "1", 100, "Deductible"));

            IList<ClaimAdjustmentPivot> caps2 = new List<ClaimAdjustmentPivot>();
            caps2.Add(this.GetClaimAdjustmentPivot("CO", "253", 200m, "Plan Adjustment"));
            caps2.Add(this.GetClaimAdjustmentPivot("CO", "P22", 50m, "Plan Adjustment"));
            caps2.Add(this.GetClaimAdjustmentPivot("CO", "45", 800m, "Not covered"));
            caps2.Add(this.GetClaimAdjustmentPivot("CO", "B15", 50m, "Not covered"));
            caps2.Add(this.GetClaimAdjustmentPivot("OA", "94", 25m, "Not covered"));
            caps2.Add(this.GetClaimAdjustmentPivot("PR", "3", 50, "Copayment"));
            caps2.Add(this.GetClaimAdjustmentPivot("PR", "1", 1000, "Deductible"));

            claim1.ClaimAdjustmentPivots = caps1;
            claim1.ServicePaymentInformations = new List<ServicePaymentInformation>();
            claims.Add(claim1);

            claim2.ClaimAdjustmentPivots = caps2;
            claim2.ServicePaymentInformations = new List<ServicePaymentInformation>();
            claims.Add(claim2);

            eob835[0].ClaimPaymentInformations = claims;

            IVisitEobApplicationService service = new VisitEobApplicationServiceMockBuilder().Setup(builder =>
            {
                builder.ClaimPaymentInformationServiceMock.Setup(x => x.Get835Files(It.IsAny<int>(), It.IsAny<string>())).Returns(eob835);
                builder.ClaimPaymentInformationServiceMock.Setup(x => x.GetUiDisplay(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>()))
                    .Returns((string claimAdjustmentReasonCode, string uniquePayerValue, string claimStatusCode, string claimAdjustmentGroupCode) =>
                    {
                        return caps1.FirstOrDefault(x => x.ClaimAdjustmentReasonCode == claimAdjustmentReasonCode && x.ClaimAdjustmentGroupCode == claimAdjustmentGroupCode).EobClaimAdjustmentReasonCode.UIDisplay;
                    });
            }).CreateService();

            IList<VisitEobDetailsResultDto> eobDetails = service.GetVisitEobDetails(1, "1");
            Assert.AreEqual(2, eobDetails.Count);

            Assert.AreEqual(4, eobDetails[0].ClaimLevelClaimAdjustmentsSummed.Count);
            Assert.AreEqual(150m, eobDetails[0].ClaimLevelClaimAdjustmentsSummed[0].MonetaryAmount);
            Assert.AreEqual("Plan Adjustment", eobDetails[0].ClaimLevelClaimAdjustmentsSummed[0].UIDisplay);

            Assert.AreEqual(770m, eobDetails[0].ClaimLevelClaimAdjustmentsSummed[1].MonetaryAmount);
            Assert.AreEqual("Not covered", eobDetails[0].ClaimLevelClaimAdjustmentsSummed[1].UIDisplay);

            Assert.AreEqual(20m, eobDetails[0].ClaimLevelClaimAdjustmentsSummed[2].MonetaryAmount);
            Assert.AreEqual("Copayment", eobDetails[0].ClaimLevelClaimAdjustmentsSummed[2].UIDisplay);

            Assert.AreEqual(100m, eobDetails[0].ClaimLevelClaimAdjustmentsSummed[3].MonetaryAmount);
            Assert.AreEqual("Deductible", eobDetails[0].ClaimLevelClaimAdjustmentsSummed[3].UIDisplay);


            Assert.AreEqual(4, eobDetails[1].ClaimLevelClaimAdjustmentsSummed.Count);
            Assert.AreEqual(250m, eobDetails[1].ClaimLevelClaimAdjustmentsSummed[0].MonetaryAmount);
            Assert.AreEqual("Plan Adjustment", eobDetails[1].ClaimLevelClaimAdjustmentsSummed[0].UIDisplay);

            Assert.AreEqual(875m, eobDetails[1].ClaimLevelClaimAdjustmentsSummed[1].MonetaryAmount);
            Assert.AreEqual("Not covered", eobDetails[1].ClaimLevelClaimAdjustmentsSummed[1].UIDisplay);

            Assert.AreEqual(50m, eobDetails[1].ClaimLevelClaimAdjustmentsSummed[2].MonetaryAmount);
            Assert.AreEqual("Copayment", eobDetails[1].ClaimLevelClaimAdjustmentsSummed[2].UIDisplay);

            Assert.AreEqual(1000m, eobDetails[1].ClaimLevelClaimAdjustmentsSummed[3].MonetaryAmount);
            Assert.AreEqual("Deductible", eobDetails[1].ClaimLevelClaimAdjustmentsSummed[3].UIDisplay);
        }

        [Test]
        public void MultipleServiceLevels()
        {
            IList<Eob835Result> eob835 = this.GetBase835s("clp07");
            IList<ClaimPaymentInformation> claims = new List<ClaimPaymentInformation>();
            ClaimPaymentInformation claim = this.GetClaimPaymentInformation(MiaMoaEnum.None, "22");

            IList<ClaimAdjustmentPivot> caps1 = new List<ClaimAdjustmentPivot>();
            caps1.Add(this.GetClaimAdjustmentPivot("CO", "253", 100m, "Plan Adjustment"));
            caps1.Add(this.GetClaimAdjustmentPivot("CO", "P22", 50m, "Plan Adjustment"));
            caps1.Add(this.GetClaimAdjustmentPivot("CO", "45", 700m, "Not covered"));
            caps1.Add(this.GetClaimAdjustmentPivot("CO", "B15", 50m, "Not covered"));
            caps1.Add(this.GetClaimAdjustmentPivot("OA", "94", 20m, "Not covered"));
            caps1.Add(this.GetClaimAdjustmentPivot("PR", "3", 20, "Copayment"));
            caps1.Add(this.GetClaimAdjustmentPivot("PR", "1", 100, "Deductible"));

            IList<ClaimAdjustmentPivot> caps2 = new List<ClaimAdjustmentPivot>();
            caps2.Add(this.GetClaimAdjustmentPivot("CO", "253", 200m, "Plan Adjustment"));
            caps2.Add(this.GetClaimAdjustmentPivot("CO", "P22", 50m, "Plan Adjustment"));
            caps2.Add(this.GetClaimAdjustmentPivot("CO", "45", 800m, "Not covered"));
            caps2.Add(this.GetClaimAdjustmentPivot("CO", "B15", 50m, "Not covered"));
            caps2.Add(this.GetClaimAdjustmentPivot("OA", "94", 25m, "Not covered"));
            caps2.Add(this.GetClaimAdjustmentPivot("PR", "3", 50, "Copayment"));
            caps2.Add(this.GetClaimAdjustmentPivot("PR", "1", 1000, "Deductible"));

            IList<ServicePaymentInformation> serviceLevels = new List<ServicePaymentInformation>();
            serviceLevels.Add(new ServicePaymentInformation
            {
                ClaimAdjustmentPivots = caps1
            });
            serviceLevels.Add(new ServicePaymentInformation
            {
                ClaimAdjustmentPivots = caps2
            });

            claim.ServicePaymentInformations = serviceLevels;
            claims.Add(claim);
            eob835[0].ClaimPaymentInformations = claims;

            IVisitEobApplicationService service = new VisitEobApplicationServiceMockBuilder().Setup(builder =>
            {
                builder.ClaimPaymentInformationServiceMock.Setup(x => x.Get835Files(It.IsAny<int>(), It.IsAny<string>())).Returns(eob835);
                builder.ClaimPaymentInformationServiceMock.Setup(x => x.GetUiDisplay(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>()))
                    .Returns((string claimAdjustmentReasonCode, string uniquePayerValue, string claimStatusCode, string claimAdjustmentGroupCode) =>
                    {
                        return caps1.FirstOrDefault(x => x.ClaimAdjustmentReasonCode == claimAdjustmentReasonCode && x.ClaimAdjustmentGroupCode == claimAdjustmentGroupCode).EobClaimAdjustmentReasonCode.UIDisplay;
                    });
            }).CreateService();

            IList<VisitEobDetailsResultDto> eobDetails = service.GetVisitEobDetails(1, "1");
            Assert.AreEqual(1, eobDetails.Count);
            Assert.AreEqual(4, eobDetails[0].ServiceLevelClaimAdjustmentsSummed.Count);

            Assert.AreEqual(400m, eobDetails[0].ServiceLevelClaimAdjustmentsSummed[0].MonetaryAmount);
            Assert.AreEqual("Plan Adjustment", eobDetails[0].ServiceLevelClaimAdjustmentsSummed[0].UIDisplay);

            Assert.AreEqual(1645m, eobDetails[0].ServiceLevelClaimAdjustmentsSummed[1].MonetaryAmount);
            Assert.AreEqual("Not covered", eobDetails[0].ServiceLevelClaimAdjustmentsSummed[1].UIDisplay);

            Assert.AreEqual(70m, eobDetails[0].ServiceLevelClaimAdjustmentsSummed[2].MonetaryAmount);
            Assert.AreEqual("Copayment", eobDetails[0].ServiceLevelClaimAdjustmentsSummed[2].UIDisplay);

            Assert.AreEqual(1100m, eobDetails[0].ServiceLevelClaimAdjustmentsSummed[3].MonetaryAmount);
            Assert.AreEqual("Deductible", eobDetails[0].ServiceLevelClaimAdjustmentsSummed[3].UIDisplay);

            Assert.AreEqual(0, eobDetails[0].HealthCareRemarkCodes.Count);
        }

        [Test]
        public void RemarkCodes_Lq_Moa()
        {
            IList<Eob835Result> eob835 = this.GetBase835s("clp07");
            IList<ClaimPaymentInformation> claims = new List<ClaimPaymentInformation>();
            ClaimPaymentInformation claim = this.GetClaimPaymentInformation(MiaMoaEnum.Moa, "22");

            IList<IndustryCodeIdentification> lqs = this.GetIndustryCodeIdentifications();

            IList<ServicePaymentInformation> serviceLevels = new List<ServicePaymentInformation>();
            serviceLevels.Add(new ServicePaymentInformation
            {
                LqHealthCareRemarkCodes = lqs
            });
            claim.ServicePaymentInformations = serviceLevels;
            claims.Add(claim);
            eob835[0].ClaimPaymentInformations = claims;

            IVisitEobApplicationService service = new VisitEobApplicationServiceMockBuilder().Setup(builder =>
            {
                builder.ClaimPaymentInformationServiceMock.Setup(x => x.Get835Files(It.IsAny<int>(), It.IsAny<string>())).Returns(eob835);
            }).CreateService();

            IList<VisitEobDetailsResultDto> eobDetails = service.GetVisitEobDetails(1, "1");
            Assert.AreEqual(1, eobDetails.Count);
            Assert.AreEqual(3, eobDetails[0].HealthCareRemarkCodes.Count);

            Assert.AreEqual("M23", eobDetails[0].HealthCareRemarkCodes[0].IndustryCode);
            Assert.AreEqual("Missing invoice.", eobDetails[0].HealthCareRemarkCodes[0].Description);

            Assert.AreEqual("M31", eobDetails[0].HealthCareRemarkCodes[1].IndustryCode);
            Assert.AreEqual("Missing radiology report.", eobDetails[0].HealthCareRemarkCodes[1].Description);

            Assert.AreEqual("MA103", eobDetails[0].HealthCareRemarkCodes[2].IndustryCode);
            Assert.AreEqual("Hemophilia Add On.", eobDetails[0].HealthCareRemarkCodes[2].Description);
        }

        [Test]
        // when claim status code is '2, '3', '20' or '21' and CAS segment(s) contain(s) Group Code OA in combination with Remit Code 23, 
        // this should NOT be classified as "Not Covered", instead it should be classified as "Amount Paid by Other Insurance". 
        public void AmountPaidByOtherInsurance()
        {
            IList<Eob835Result> eob835 = this.GetBase835s("clp07");
            IList<ClaimPaymentInformation> claims = new List<ClaimPaymentInformation>();
            ClaimPaymentInformation claim = this.GetClaimPaymentInformation(MiaMoaEnum.None, "3");

            IList<ClaimAdjustmentPivot> caps = new List<ClaimAdjustmentPivot>();
            caps.Add(this.GetClaimAdjustmentPivot("CO", "253", 200m, "Plan Adjustment"));
            caps.Add(this.GetClaimAdjustmentPivot("CO", "P22", 50m, "Plan Adjustment"));
            caps.Add(this.GetClaimAdjustmentPivot("CO", "45", 800m, "Not covered"));
            caps.Add(this.GetClaimAdjustmentPivot("CO", "B15", 50m, "Not covered"));
            caps.Add(this.GetClaimAdjustmentPivot("OA", "23", 25m, "Not covered"));
            caps.Add(this.GetClaimAdjustmentPivot("PR", "3", 50, "Copayment"));
            caps.Add(this.GetClaimAdjustmentPivot("PR", "1", 1000, "Deductible"));
            claim.ClaimAdjustmentPivots = caps;
            claim.ServicePaymentInformations = new List<ServicePaymentInformation>();
            claims.Add(claim);
            eob835[0].ClaimPaymentInformations = claims;

            IVisitEobApplicationService service = new VisitEobApplicationServiceMockBuilder().Setup(builder =>
            {
                builder.ClaimPaymentInformationServiceMock.Setup(x => x.Get835Files(It.IsAny<int>(), It.IsAny<string>())).Returns(eob835);
                builder.ClaimPaymentInformationServiceMock.Setup(x => x.GetUiDisplay(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>()))
                    .Returns((string claimAdjustmentReasonCode, string uniquePayerValue, string claimStatusCode, string claimAdjustmentGroupCode) =>
                    {
                        // This mapping is expected to be found in the database when calling GetUiDisplay()
                        string uiDisplay = caps.FirstOrDefault(x => x.ClaimAdjustmentReasonCode == claimAdjustmentReasonCode && x.ClaimAdjustmentGroupCode == claimAdjustmentGroupCode).EobClaimAdjustmentReasonCode.UIDisplay;
                        if (claimAdjustmentGroupCode == "OA" && claimAdjustmentReasonCode == "23" && claim.Clp02ClaimStatusCode == "3")
                        {
                            uiDisplay = "Amount Paid by Other Insurance";
                        }
                        return uiDisplay;
                    });
            }).CreateService();

            IList<VisitEobDetailsResultDto> eobDetails = service.GetVisitEobDetails(1, "1");
            Assert.AreEqual(1, eobDetails.Count);
            Assert.AreEqual(5, eobDetails[0].ClaimLevelClaimAdjustmentsSummed.Count);

            Assert.AreEqual(250m, eobDetails[0].ClaimLevelClaimAdjustmentsSummed[0].MonetaryAmount);
            Assert.AreEqual("Plan Adjustment", eobDetails[0].ClaimLevelClaimAdjustmentsSummed[0].UIDisplay);

            Assert.AreEqual(850m, eobDetails[0].ClaimLevelClaimAdjustmentsSummed[1].MonetaryAmount);
            Assert.AreEqual("Not covered", eobDetails[0].ClaimLevelClaimAdjustmentsSummed[1].UIDisplay);

            Assert.AreEqual(25m, eobDetails[0].ClaimLevelClaimAdjustmentsSummed[2].MonetaryAmount);
            Assert.AreEqual("Amount Paid by Other Insurance", eobDetails[0].ClaimLevelClaimAdjustmentsSummed[2].UIDisplay);

            Assert.AreEqual(50m, eobDetails[0].ClaimLevelClaimAdjustmentsSummed[3].MonetaryAmount);
            Assert.AreEqual("Copayment", eobDetails[0].ClaimLevelClaimAdjustmentsSummed[3].UIDisplay);

            Assert.AreEqual(1000m, eobDetails[0].ClaimLevelClaimAdjustmentsSummed[4].MonetaryAmount);
            Assert.AreEqual("Deductible", eobDetails[0].ClaimLevelClaimAdjustmentsSummed[4].UIDisplay);

        }

        private IList<Eob835Result> GetBase835s(string claimNumberLocation)
        {
            IList<Eob835Result> eob835s = new List<Eob835Result>();

            Eob835Result eob835 = new Eob835Result()
            {
                TransactionSetHeaderTrailerId = 1,
                TransactionDate = DateTime.UtcNow,
                HeaderNumberId = 1,
                HeaderNumber = 1,
                ClaimPaymentInformationId = 1,
                EobPayerFilterId = 1,
                EobPayerFilterEobExternalLinks = new List<EobPayerFilterEobExternalLink>
                {
                    new EobPayerFilterEobExternalLink
                    {
                        EobExternalLink = new EobExternalLink { ClaimNumberLocation = claimNumberLocation }
                    }
                }
            };

            eob835s.Add(eob835);
            return eob835s;
        }

        public enum MiaMoaEnum
        {
            None,
            Mia,
            Moa
        }

        private ClaimPaymentInformation GetClaimPaymentInformation(MiaMoaEnum miaMoaEnum, string claimStatusCode)
        {
            ClaimPaymentInformation cpi = new ClaimPaymentInformation
            {
                VisitSourceSystemKey = "1",
                BillingSystemId = 1,
                Clp01ClaimSubmittersIdentifier = "SH41981111-CLP01",
                Clp02ClaimStatusCode = claimStatusCode.IsNotNullOrEmpty() ? claimStatusCode : "1",
                Clp03MonetaryAmount = 100m,
                Clp04MonetaryAmount = 100m,
                Clp05MonetaryAmount = 100m,
                Clp07ReferenceIdentifier = "SH41981111-CLP07",
                Nm11PatientName = new IndividualOrOrganizationalName
                {
                    Nm109IdentificationCode = "123456789"
                }
            };

            switch (miaMoaEnum)
            {
                case MiaMoaEnum.Mia:
                    cpi.MiaInpatientInformation = new InpatientAdjudicationInformation
                    {
                        Mia05ReferenceIdentifier = "M141",
                        Mia05EobRemittanceAdviceRemarkCode = new EobRemittanceAdviceRemarkCode
                        {
                            RemittanceAdviceRemarkCode = "M141",
                            Description = "Missing physician certified plan of care."
                        }
                    };
                    break;
                case MiaMoaEnum.Moa:
                    cpi.MoaOutpatientInformation = new OutpatientAdjudicationInformation
                    {
                        Moa03ReferenceIdentifier = "M23",
                        Moa03EobRemittanceAdviceRemarkCode = new EobRemittanceAdviceRemarkCode
                        {
                            RemittanceAdviceRemarkCode = "M23",
                            Description = "Missing invoice."
                        }
                    };
                    break;
            }
            return cpi;
        }

        private ServicePaymentInformation GetServicePaymentInformation()
        {
            return new ServicePaymentInformation();
        }

        private IList<IndustryCodeIdentification> GetIndustryCodeIdentifications()
        {
            IList<IndustryCodeIdentification> lqs = new List<IndustryCodeIdentification>();
            lqs.Add(new IndustryCodeIdentification
            {
                Lq02IndustryCode = "M31",
                EobRemittanceAdviceRemarkCode = new EobRemittanceAdviceRemarkCode
                {
                    RemittanceAdviceRemarkCode = "M31",
                    Description = "Missing radiology report."
                }
            });
            lqs.Add(new IndustryCodeIdentification
            {
                Lq02IndustryCode = "MA103",
                EobRemittanceAdviceRemarkCode = new EobRemittanceAdviceRemarkCode
                {
                    RemittanceAdviceRemarkCode = "MA103",
                    Description = "Hemophilia Add On."
                }
            });
            return lqs;
        }

        private ClaimAdjustmentPivot GetClaimAdjustmentPivot(string groupCode, string reasonCode, decimal monetaryAmount, string uiDisplay)
        {
            return new ClaimAdjustmentPivot
            {
                ClaimAdjustmentGroupCode = groupCode,
                ClaimAdjustmentReasonCode = reasonCode,
                MonetaryAmount = monetaryAmount,
                EobClaimAdjustmentReasonCode = new Application.Core.Common.Models.Eob.EobClaimAdjustmentReasonCode()
                {
                    UIDisplay = uiDisplay
                }
            };
        }


    }
}
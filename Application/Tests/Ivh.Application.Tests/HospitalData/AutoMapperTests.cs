﻿namespace Ivh.Application.Tests.HospitalData
{
    using System;
    using System.Collections.Generic;
    using AutoMapper;
    using Domain.HospitalData.Visit.Entities;
    using NUnit.Framework;

    [TestFixture]
    public class AutoMapperTests : ApplicationTestBase
    {

        [Test]
        public void MapVisitChangeToVisit()
        {
            VisitChange visitChange = new VisitChange()
            {
                AdmitDate = DateTime.UtcNow,
                BillingApplication = "",
                ChangeEventId = default(int),
                VisitTransactions = new List<VisitTransactionChange>() { new VisitTransactionChange() { VisitChange = null } },
                VisitInsurancePlans = new List<VisitInsurancePlanChange>() { new VisitInsurancePlanChange() { InsurancePlanId = default(int), VisitChange = new VisitChange() } },
                LifeCycleStage = new LifeCycleStage(),
                PatientType = new PatientType(),
                PrimaryInsuranceType = new PrimaryInsuranceType(),
                RevenueWorkCompany = new RevenueWorkCompany(),
            };

            Visit visit = new Visit();

            Mapper.Map(visitChange, visit);

            //This mapping is basically for QATools  --  Building of the object tree happens more manually.
            Assert.Null(visit.VisitInsurancePlans);
            Assert.Null(visit.VisitTransactions);

        }
    }
}

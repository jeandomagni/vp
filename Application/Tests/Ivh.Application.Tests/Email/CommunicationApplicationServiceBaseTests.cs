﻿namespace Ivh.Application.Tests.Email
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Application.Core.Common.Dtos;
    using Application.Email.Common.Dtos;
    using Application.Email.Common.Utilities;
    using Application.Email.Internal.Dtos;
    using Application.FinanceManagement.Common.Dtos;
    using Common.Base.Utilities.Extensions;
    using Common.Tests.Helpers.Email;
    using Common.Tests.Helpers.Guarantor;
    using Common.VisitPay.Attributes;
    using Common.VisitPay.Enums;
    using Common.VisitPay.Messages.Communication;
    using Domain.Email.Entities;
    using Domain.Guarantor.Entities;
    using Moq;
    using Newtonsoft.Json;
    using NUnit.Framework;

    [TestFixture]
    public class CommunicationApplicationServiceBaseTests : ApplicationTestBase
    {
        #region Sms

        [Test]
        public void SendSms_ActiveGuarantor_Sends()
        {
            Guarantor guarantor = GuarantorFactory.GenerateOnline();

            ImplementationOfCommunicationApplicationServiceBaseMockBuilder builder = this.CreateBuilder(guarantor, true);
            CommunicationTypeEnum communicationTypeEnum = this.FindCommunicationTypesWithAttributeRequirements(x => x.SendSms).First();
            CommunicationDto communicationDto = new CommunicationDtoBuilder().WithDefaultValues(communicationTypeEnum, CommunicationMethodEnum.Sms).SendTo(guarantor.User.VisitPayUserId);

            ImplementationOfCommunicationApplicationServiceBase src = builder.CreateService();

            bool result = src.SendSmsTest(communicationDto, NotificationFrequencyEnum.OncePerDay, null);

            Assert.AreEqual(true, result);
        }

        [Test]
        public void SendSms_ClosedGuarantor_DoesNotSend()
        {
            Guarantor guarantor = GuarantorFactory.GenerateOnline();
            guarantor.CloseAccount();

            ImplementationOfCommunicationApplicationServiceBaseMockBuilder builder = this.CreateBuilder(guarantor, true);
            CommunicationTypeEnum communicationTypeEnum = this.FindCommunicationTypesWithAttributeRequirements(x => x.SendSms).First();
            CommunicationDto communicationDto = new CommunicationDtoBuilder().WithDefaultValues(communicationTypeEnum, CommunicationMethodEnum.Sms).SendTo(guarantor.User.VisitPayUserId);

            ImplementationOfCommunicationApplicationServiceBase src = builder.CreateService();

            bool result = src.SendSmsTest(communicationDto, NotificationFrequencyEnum.OncePerDay, null);

            Assert.AreEqual(false, result);
        }

        [Test]
        public void SendSms_VPCC_SendsAllowedTypes()
        {
            Guarantor guarantor = GuarantorFactory.GenerateOffline();

            ImplementationOfCommunicationApplicationServiceBaseMockBuilder builder = this.CreateBuilder(guarantor, true);

            IList<CommunicationTypeEnum> allowedTypes = this.FindCommunicationTypesWithAttributeRequirements(x => x.SendSms, x => x.SendToVpccUser);
            IList<CommunicationTypeEnum> disallowedTypes = this.FindCommunicationTypesWithAttributeRequirements(x => x.SendSms, x => x.SendToVpccUser == false);
            ImplementationOfCommunicationApplicationServiceBase src = builder.CreateService();

            foreach (CommunicationTypeEnum communicationTypeEnum in allowedTypes)
            {
                CommunicationDto communicationDto = new CommunicationDtoBuilder().WithDefaultValues(communicationTypeEnum, CommunicationMethodEnum.Sms).SendTo(guarantor.User.VisitPayUserId);
                bool result = src.SendEmailTest(communicationDto, NotificationFrequencyEnum.OncePerDay, null);

                Assert.AreEqual(true, result, $"{communicationTypeEnum.ToString()} should be sent to a VPCC user");
            }

            foreach (CommunicationTypeEnum communicationTypeEnum in disallowedTypes)
            {
                CommunicationDto communicationDto = new CommunicationDtoBuilder().WithDefaultValues(communicationTypeEnum, CommunicationMethodEnum.Sms).SendTo(guarantor.User.VisitPayUserId);
                bool result = src.SendEmailTest(communicationDto, NotificationFrequencyEnum.OncePerDay, null);

                Assert.AreEqual(false, result, $"{communicationTypeEnum.ToString()} should not be sent to a VPCC user");
            }
        }

        #endregion Sms

        #region Email

        [Test]
        public void SendEmail_ActiveGuarantor_Sends()
        {
            Guarantor guarantor = GuarantorFactory.GenerateOnline();
            ImplementationOfCommunicationApplicationServiceBaseMockBuilder builder = this.CreateBuilder(guarantor, true);
            CommunicationTypeEnum communicationTypeEnum = this.FindCommunicationTypesWithAttributeRequirements(x => x.SendEmail).First();
            CommunicationDto communicationDto = new CommunicationDtoBuilder().WithDefaultValues(communicationTypeEnum, CommunicationMethodEnum.Email).SendTo(guarantor.User.VisitPayUserId);

            ImplementationOfCommunicationApplicationServiceBase src = builder.CreateService();

            bool result = src.SendEmailTest(communicationDto, NotificationFrequencyEnum.OncePerDay, null);

            Assert.AreEqual(true, result);
        }


        [Test]
        public void SendMail_PaperOnly_Sends()
        {
            Guarantor guarantor = GuarantorFactory.GenerateOffline();

            //Note: the builder will not add the email communication preferences for an offline
            ImplementationOfCommunicationApplicationServiceBaseMockBuilder builder = this.CreateBuilder(guarantor, true);
            builder.WithCommunicationPreferences(new List<CommunicationMethodEnum> {CommunicationMethodEnum.Mail}, guarantor.User.VisitPayUserId);

            CommunicationTypeEnum communicationTypeEnum = this.FindCommunicationTypesWithAttributeRequirements(x => x.SendMail).First();
            CommunicationDto communicationDto = new CommunicationDtoBuilder().WithDefaultValues(communicationTypeEnum, CommunicationMethodEnum.Mail).SendTo(guarantor.User.VisitPayUserId);


            builder.WithFeatureEnabled(VisitPayFeatureEnum.FeatureMailIsEnabled)
                .CommunicationTemplateLoaderMock.Setup(x => x.CreateMailFromTemplate(It.IsAny<CommunicationTypeEnum>(),
                    It.IsAny<ClientDto>(),
                    It.IsAny<SendTo>(),
                    It.IsAny<string>(),
                    It.IsAny<string>(),
                    It.IsAny<IDictionary<string, string>>(),
                    It.IsAny<bool>(),
                    It.IsAny<bool>(),
                    It.IsAny<FinancePlanDto>())).Returns(communicationDto);
                
                builder.CreateService()
                .SendCommunicationsTest(SendTo.User(guarantor.User.VisitPayUserId), communicationTypeEnum);

            builder.VerifyMail(guarantor, false, false);
            builder.VerifyEmailDidNotSend();
            builder.VerifySmsDidNotSend();
        }

        [Test]
        public void SendMail_Does_Not_Send_Without_Mail_Preference()
        {
            Guarantor guarantor = GuarantorFactory.GenerateOffline();

            //Note: the builder will not add the email communication preferences for an offline
            ImplementationOfCommunicationApplicationServiceBaseMockBuilder builder = this.CreateBuilder(guarantor, true);

            CommunicationTypeEnum communicationTypeEnum = this.FindCommunicationTypesWithAttributeRequirements(x => x.SendMail).First();

            builder.WithFeatureEnabled(VisitPayFeatureEnum.FeatureMailIsEnabled)
                .CreateService()
                .SendCommunicationsTest(SendTo.User(guarantor.User.VisitPayUserId), communicationTypeEnum);

            builder.VerifyMailDidNotSend();
            builder.VerifyEmailDidNotSend();
            builder.VerifySmsDidNotSend();
        }


        [Test]
        public void SendEmail_ClosedGuarantor_DoesNotSend()
        {
            Guarantor guarantor = GuarantorFactory.GenerateOnline();
            guarantor.CloseAccount();

            ImplementationOfCommunicationApplicationServiceBaseMockBuilder builder = this.CreateBuilder(guarantor, true);
            CommunicationTypeEnum communicationTypeEnum = this.FindCommunicationTypesWithAttributeRequirements(x => x.SendEmail).First();
            CommunicationDto communicationDto = new CommunicationDtoBuilder().WithDefaultValues(communicationTypeEnum, CommunicationMethodEnum.Email).SendTo(guarantor.User.VisitPayUserId);

            ImplementationOfCommunicationApplicationServiceBase src = builder.CreateService();

            bool result = src.SendEmailTest(communicationDto, NotificationFrequencyEnum.OncePerDay, null);

            Assert.AreEqual(false, result);
        }

        [Test]
        public void SendEmail_VPCC_SendsAllowedTypes()
        {
            Guarantor guarantor = GuarantorFactory.GenerateOffline();

            ImplementationOfCommunicationApplicationServiceBaseMockBuilder builder = this.CreateBuilder(guarantor, true);

            IList<CommunicationTypeEnum> allowedTypes = this.FindCommunicationTypesWithAttributeRequirements(x => x.SendEmail, x => x.SendToVpccUser);
            IList<CommunicationTypeEnum> disallowedTypes = this.FindCommunicationTypesWithAttributeRequirements(x => x.SendEmail, x => x.SendToVpccUser == false);
            ImplementationOfCommunicationApplicationServiceBase src = builder.CreateService();

            foreach (CommunicationTypeEnum communicationTypeEnum in allowedTypes)
            {                
                CommunicationDto communicationDto = new CommunicationDtoBuilder().WithDefaultValues(communicationTypeEnum, CommunicationMethodEnum.Email).SendTo(guarantor.User.VisitPayUserId);
                bool result = src.SendEmailTest(communicationDto, NotificationFrequencyEnum.OncePerDay, null);

                Assert.AreEqual(true, result, $"{communicationTypeEnum.ToString()} should be sent to a VPCC user");
            }

            foreach (CommunicationTypeEnum communicationTypeEnum in disallowedTypes)
            {
                CommunicationDto communicationDto = new CommunicationDtoBuilder().WithDefaultValues(communicationTypeEnum, CommunicationMethodEnum.Email).SendTo(guarantor.User.VisitPayUserId);
                bool result = src.SendEmailTest(communicationDto, NotificationFrequencyEnum.OncePerDay, null);

                Assert.AreEqual(false, result, $"{communicationTypeEnum.ToString()} should not be sent to a VPCC user");
            }
        }

        [Test]
        public void SendEmail_JsonMapping()
        {
            SendGridEmailEventMessage message = null;
            Guid uniqueId = Guid.Parse("6990BC7F-7FB0-480C-B215-A69384CB8E46");
            string json = @"
                {
                    ""attempt"":""1"",
                    ""category"":""Thank you for your payment"",
                    ""email"":""someone@somewhere.com"",
                    ""sg_event_id"":""rbtnWrG1DVDGGGFHFyun0A=="",
                    ""event"":""processed"",
                    ""type"":""1"",
                    ""asm_group_id"":""1"",
                    ""ip"":""127.0.0.1"",
                    ""sg_message_id"":""14c5d75ce93.dfd.64b469.filter0001.16648.5515E0B88.0"",
                    ""response"":""250 OK"",
                    ""smtp-id"":""<14c5d75ce93.dfd.64b469@ismtpd-555>"",
                    ""SourceApp"":""VisitPay"",
                    ""timestamp"":1513299569,
                    ""UniqueId"":""" + uniqueId.ToString() + @""",
                    ""url"":""http://www.sendgrid.com/"",
                    ""useragent"":""Mozilla/4.0 (compatible; MSIE 6.1; Windows XP; .NET CLR 1.1.4322; .NET CLR 2.0.50727)""
                }
            ";
            try
            {
                message = JsonConvert.DeserializeObject<SendGridEmailEventMessage>(json);
            }
            catch
            {
                //Just swallow it.                        
            }
            Assert.IsTrue(message!=null,"SendGridEmailEventMessage should deserialize from JSON text");

            CommunicationEventDto communicationEventDto = AutoMapper.Mapper.Map<CommunicationEventDto>(message);
            Assert.IsTrue(communicationEventDto.UniqueId == uniqueId,"CommunicationEventDto should have the same UniqueId");

            ConversionUtil.SetEventDateFromTimeStamp(communicationEventDto);
            DateTime targetDateTime = new DateTime(2017, 12, 15, 0, 59, 29);
            DateTime.SpecifyKind(targetDateTime, DateTimeKind.Utc);
            Assert.IsTrue(communicationEventDto.EventDateTime.Equals(targetDateTime), "CommunicationEventDto.EventDateTime should match the target date");

            CommunicationEvent communicationEvent = AutoMapper.Mapper.Map<CommunicationEvent>(communicationEventDto);
            Assert.IsTrue(communicationEvent.UniqueId == uniqueId,"CommunicationEvent should have the same UniqueId");
        }

        #endregion Email

        private IList<CommunicationTypeEnum> FindCommunicationTypesWithAttributeRequirements(params Func<CommunicationAttribute, bool>[] attributeRequirements)
        {
            IEnumerable<CommunicationTypeEnum> values = Enum.GetValues(typeof(CommunicationTypeEnum)).Cast<CommunicationTypeEnum>()
                .Where(x => x.GetAttribute<CommunicationAttribute>() != null);

            foreach (Func<CommunicationAttribute, bool> attributeRequirement in attributeRequirements)
            {
                values = values.Where(x => attributeRequirement(x.GetAttribute<CommunicationAttribute>())).ToList();
            }

            return values.ToList();
        }

        private ImplementationOfCommunicationApplicationServiceBaseMockBuilder CreateBuilder(Guarantor guarantor, bool isSmsEnabled)
        {
            ImplementationOfCommunicationApplicationServiceBaseMockBuilder builder = new ImplementationOfCommunicationApplicationServiceBaseMockBuilder();

            // enable sms
            builder.ApplicationServiceCommonServiceMockBuilder.FeatureServiceMock.Setup(x => x.IsFeatureEnabled(VisitPayFeatureEnum.Sms)).Returns(isSmsEnabled);
            builder.CommunicationServiceMock.Setup(x => x.HasCommunicationPreference(It.IsAny<int>(), It.IsAny<CommunicationMethodEnum>(), It.IsAny<CommunicationTypeEnum>())).Returns(isSmsEnabled);

            if (guarantor.VpGuarantorTypeEnum == GuarantorTypeEnum.Online)
            {
                //default the communication preference to email
                builder.WithCommunicationPreferences(new List<CommunicationMethodEnum>{CommunicationMethodEnum.Email}, guarantor.User.VisitPayUserId);
            }

            // guarantor
            builder.GuarantorServiceMock.Setup(x => x.GetGuarantor(guarantor.VpGuarantorId)).Returns(() => guarantor);
            builder.GuarantorServiceMock.Setup(x => x.GetGuarantorEx(guarantor.User.VisitPayUserId, null)).Returns(() => guarantor);

            // user
            builder.VisitPayUserRepositorMock.Setup(x => x.FindById(guarantor.User.VisitPayUserId.ToString())).Returns(guarantor.User);

            // sender
            builder.CommunicationSenderMock.Setup(x => x.SendEmailAsync(It.IsAny<CommunicationDto>())).Returns(Task.FromResult(true));
            builder.CommunicationSenderMock.Setup(x => x.SendSmsAsync(It.IsAny<CommunicationDto>())).Returns(Task.FromResult(true));

            return builder;
        }

       
    }
}

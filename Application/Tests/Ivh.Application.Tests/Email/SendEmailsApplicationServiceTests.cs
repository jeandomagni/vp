﻿namespace Ivh.Application.Tests.Email
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Application.Base.Common.Interfaces;
    using Application.Core.Common.Dtos;
    using Application.Email.ApplicationServices;
    using Application.Email.Common.Dtos;
    using Application.Email.Internal.Dtos;
    using Application.Email.Internal.Interfaces;
    using Application.FinanceManagement.Common.Dtos;
    using Common.Base.Utilities.Extensions;
    using Common.Tests.Helpers;
    using Common.Tests.Helpers.Email;
    using Common.VisitPay.Attributes;
    using Common.VisitPay.Enums;
    using Domain.Email.Entities;
    using Domain.Email.Interfaces;
    using Domain.Guarantor.Entities;
    using Domain.Guarantor.Interfaces;
    using Domain.User.Entities;
    using Domain.User.Interfaces;
    using Moq;
    using NUnit.Framework;

    /// <summary>
    /// these tests only verify that the correct parameters (user, locale, etc) are passed to CommunicationTemplateLoader
    /// </summary>
    [TestFixture]
    public class SendEmailsApplicationServiceTests : ApplicationTestBase
    {
        [TestCase(true, "en-US")]
        [TestCase(false, "en-US")]
        [TestCase(true, "is-IS")]
        [TestCase(false, "is-IS")]
        public void Communications_WithVpGuarantor_Sends(bool isSmsEnabled, string locale)
        {
            CommunicationTypeEnum communicationType = this.FindCommunicationTypeWithAttribute(sendSms: true);
            Guarantor guarantor = this.CreateGuarantor(locale);

            // send
            ImplementationOfCommunicationApplicationServiceBaseMockBuilder builder = this.CreateBuilder(guarantor, isSmsEnabled);
            builder.CreateService().SendCommunicationsTest(SendTo.Guarantor(guarantor.VpGuarantorId), communicationType);

            // verify email
            builder.VerifyEmail(guarantor, false, false);

            // verify sms
            if (isSmsEnabled)
            {
                builder.VerifySms(guarantor, false, false);
            }
            else
            {
                builder.VerifySmsDidNotSend();
            }
        }

        [TestCase(true, "en-US")]
        [TestCase(false, "en-US")]
        [TestCase(true, "is-IS")]
        [TestCase(false, "is-IS")]
        public void Communications_WithVisitPayUser_Sends(bool isSmsEnabled, string locale)
        {
            CommunicationTypeEnum communicationType = this.FindCommunicationTypeWithAttribute(sendSms: true);
            Guarantor guarantor = this.CreateGuarantor(locale);

            // send
            ImplementationOfCommunicationApplicationServiceBaseMockBuilder builder = this.CreateBuilder(guarantor, isSmsEnabled);
            builder.CreateService().SendCommunicationsTest(SendTo.User(guarantor.User.VisitPayUserId), communicationType);

            // verify email
            builder.VerifyEmail(guarantor, false, false);

            // verify sms
            if (isSmsEnabled)
            {
                builder.VerifySms(guarantor, false, false);
            }
            else
            {
                builder.VerifySmsDidNotSend();
            }
        }

        [TestCase(true, "en-US", "is-IS")]
        [TestCase(false, "en-US", "is-IS")]
        [TestCase(true, "is-IS", "en-US")]
        [TestCase(false, "is-IS", "en-US")]
        public void Communications_WithVpGuarantor_SendsToManaging(bool isSmsEnabled, string locale, string managingLocale)
        {
            CommunicationTypeEnum communicationType = this.FindCommunicationTypeWithAttribute(sendSms: true, sendManaging: true);
            Guarantor guarantor = this.CreateGuarantor(locale);
            this.CreateConsolidation(guarantor, managingLocale);

            // send
            ImplementationOfCommunicationApplicationServiceBaseMockBuilder builder = this.CreateBuilder(guarantor, isSmsEnabled);
            builder.CreateService().SendCommunicationsTest(SendTo.Guarantor(guarantor.VpGuarantorId), communicationType);

            // verify email
            builder.VerifyEmail(guarantor, true, false);
            builder.VerifyEmail(guarantor.ManagingConsolidationGuarantors.First().ManagingGuarantor, true, true);

            // verify sms
            if (isSmsEnabled)
            {
                builder.VerifySms(guarantor, true, false);
                builder.VerifySms(guarantor.ManagingConsolidationGuarantors.First().ManagingGuarantor, true, true);
            }
            else
            {
                builder.VerifySmsDidNotSend();
            }
        }

        [TestCase(true, "en-US", "is-IS")]
        [TestCase(false, "en-US", "is-IS")]
        [TestCase(true, "is-IS", "en-US")]
        [TestCase(false, "is-IS", "en-US")]
        public void Communications_WithVisitPayUser_SendsToManaging(bool isSmsEnabled, string locale, string managingLocale)
        {
            CommunicationTypeEnum communicationType = this.FindCommunicationTypeWithAttribute(sendSms: true, sendManaging: true);
            Guarantor guarantor = this.CreateGuarantor(locale);
            this.CreateConsolidation(guarantor, managingLocale);

            // send
            ImplementationOfCommunicationApplicationServiceBaseMockBuilder builder = this.CreateBuilder(guarantor, isSmsEnabled);
            builder.CreateService().SendCommunicationsTest(SendTo.User(guarantor.User.VisitPayUserId), communicationType);

            // verify email
            builder.VerifyEmail(guarantor, true, false);
            builder.VerifyEmail(guarantor.ManagingConsolidationGuarantors.First().ManagingGuarantor, true, true);

            // verify sms
            if (isSmsEnabled)
            {
                builder.VerifySms(guarantor, true, false);
                builder.VerifySms(guarantor.ManagingConsolidationGuarantors.First().ManagingGuarantor, true, true);
            }
            else
            {
                builder.VerifySmsDidNotSend();
            }
        }

        [Test]
        public void Communication_SendsEmailOnly()
        {
            CommunicationTypeEnum communicationType = this.FindCommunicationTypeWithAttribute();
            Guarantor guarantor = this.CreateGuarantor("en-US");

            // send
            ImplementationOfCommunicationApplicationServiceBaseMockBuilder builder = this.CreateBuilder(guarantor, isSmsEnabled: true);
            builder.CreateService().SendCommunicationsTest(SendTo.Guarantor(guarantor.VpGuarantorId), communicationType);

            // verify email
            builder.VerifyEmail(guarantor, false, false);

            // verify sms
            builder.VerifySmsDidNotSend();
        }

        [Test]
        public void Communication_SendsSmsOnly()
        {
            CommunicationTypeEnum communicationType = this.FindCommunicationTypeWithAttribute(sendEmail: false, sendSms: true);
            Guarantor guarantor = this.CreateGuarantor("en-US");

            // send
            ImplementationOfCommunicationApplicationServiceBaseMockBuilder builder = this.CreateBuilder(guarantor, isSmsEnabled: true);
            builder.CreateService().SendCommunicationsTest(SendTo.Guarantor(guarantor.VpGuarantorId), communicationType);

            // verify email
            builder.VerifyEmailDidNotSend();

            // verify sms
            builder.VerifySms(guarantor, false, false);
        }

        [Test]
        public void Communication_ManagingOnly_DoesNotIncludeConsolidation()
        {
            CommunicationTypeEnum communicationType = this.FindCommunicationTypeWithAttribute();
            Guarantor guarantor = this.CreateGuarantor("en-US");
            this.CreateConsolidation(guarantor, "en-US");

            Guarantor managingGuarantor = guarantor.ManagingConsolidationGuarantors.First().ManagingGuarantor;

            // send
            ImplementationOfCommunicationApplicationServiceBaseMockBuilder builder = this.CreateBuilder(managingGuarantor, isSmsEnabled: true);
            builder.CreateService().SendCommunicationsTest(SendTo.Guarantor(managingGuarantor.VpGuarantorId), communicationType);

            // verify email
            builder.VerifyEmail(managingGuarantor, false, false);
        }

        [TestCase(0, false)]
        [TestCase(1, true)]
        [Test]
        public void Communication_SuppressOnRegistrationDay(int numberOfDays, bool shouldSend)
        {
            CommunicationTypeEnum communicationType = this.FindCommunicationTypeWithAttribute(suppressOnRegistration: true);
            Guarantor guarantor = this.CreateGuarantor("en-US");
            guarantor.RegistrationDate = DateTime.UtcNow.AddDays(numberOfDays);

            // send
            ImplementationOfCommunicationApplicationServiceBaseMockBuilder builder = this.CreateBuilder(guarantor, isSmsEnabled: true);
            builder.CreateService().SendCommunicationsTest(SendTo.Guarantor(guarantor.VpGuarantorId), communicationType);

            if (shouldSend)
            {
                // verify email
                builder.VerifyEmail(guarantor, false, false);
            }
            else
            {
                // verify email
                builder.VerifyEmailDidNotSend();
            }

            // verify sms
            builder.VerifySmsDidNotSend();
        }

        #region helpers

        private CommunicationTypeEnum FindCommunicationTypeWithAttribute(bool? sendEmail = true, bool? sendSms = null, bool? sendManaging = null, NotificationFrequencyEnum? frequency = null, bool? suppressOnRegistration = null)
        {
            List<CommunicationTypeEnum> values = Enum.GetValues(typeof(CommunicationTypeEnum)).Cast<CommunicationTypeEnum>()
                .Where(x => x.GetAttribute<CommunicationAttribute>() != null)
                .ToList();

            if (sendEmail.HasValue)
            {
                values = values.Where(x => x.GetAttribute<CommunicationAttribute>().SendEmail == sendEmail.Value).ToList();
            }

            if (sendSms.HasValue)
            {
                values = values.Where(x => x.GetAttribute<CommunicationAttribute>().SendSms == sendSms.Value).ToList();
            }

            if (sendManaging.HasValue)
            {
                values = values.Where(x => x.GetAttribute<CommunicationAttribute>().SendManaging == sendManaging.Value).ToList();
            }

            if (suppressOnRegistration.HasValue)
            {
                values = values.Where(x => x.GetAttribute<CommunicationAttribute>().SuppressOnRegistrationDay == suppressOnRegistration.Value).ToList();
            }

            if (frequency.HasValue)
            {
                values = values.Where(x => x.GetAttribute<CommunicationAttribute>().Frequency == frequency.Value).ToList();
            }

            return values.FirstOrDefault();
        }

        private ImplementationOfCommunicationApplicationServiceBaseMockBuilder CreateBuilder(Guarantor guarantor, bool isSmsEnabled)
        {
            ImplementationOfCommunicationApplicationServiceBaseMockBuilder builder = new ImplementationOfCommunicationApplicationServiceBaseMockBuilder();

            // enable sms
            builder.ApplicationServiceCommonServiceMockBuilder.FeatureServiceMock.Setup(x => x.IsFeatureEnabled(VisitPayFeatureEnum.Sms)).Returns(isSmsEnabled);

            // guarantor
            builder.GuarantorServiceMock.Setup(x => x.GetGuarantor(guarantor.VpGuarantorId)).Returns(() => guarantor);
            builder.GuarantorServiceMock.Setup(x => x.GetGuarantorEx(guarantor.User.VisitPayUserId, null)).Returns(() => guarantor);

            // user
            builder.VisitPayUserRepositorMock.Setup(x => x.FindById(guarantor.User.VisitPayUserId.ToString())).Returns(guarantor.User);

            return builder;
        }

        private Guarantor CreateGuarantor(string locale)
        {
            Guarantor guarantor = new Guarantor
            {
                VpGuarantorId = 1,
                User = new VisitPayUser
                {
                    FirstName = "FirstName",
                    LastName = "LastName",
                    Locale = locale,
                    Email = "email@dot.com",
                    VisitPayUserId = 1
                }
            };

            this.AddSms(guarantor, "123-123-1234");

            return guarantor;
        }

        private void CreateConsolidation(Guarantor guarantor, string locale)
        {
            Guarantor managingGuarantor = new Guarantor
            {
                VpGuarantorId = 200,
                User = new VisitPayUser
                {
                    FirstName = "ManagingFirstName",
                    LastName = "ManagingLastName",
                    Locale = locale,
                    Email = "managing@dot.com",
                    VisitPayUserId = 2
                }
            };

            this.AddSms(managingGuarantor, "333-333-3333");

            guarantor.ManagingConsolidationGuarantors.Add(new ConsolidationGuarantor
            {
                ConsolidationGuarantorStatus = ConsolidationGuarantorStatusEnum.Accepted,
                ManagingGuarantor = managingGuarantor
            });

            managingGuarantor.ManagedConsolidationGuarantors.Add(new ConsolidationGuarantor
            {
                ConsolidationGuarantorStatus = ConsolidationGuarantorStatusEnum.Accepted,
                ManagedGuarantor = guarantor
            });
        }

        private void AddSms(Guarantor guarantor, string number)
        {
            guarantor.User.Acknowledgements.Add(new VisitPayUserAcknowledgement
            {
                SmsPhoneType = SmsPhoneTypeEnum.Primary,
                VisitPayUserAcknowledgementType = VisitPayUserAcknowledgementTypeEnum.SmsAccept
            });

            guarantor.User.PhoneNumber = number;
        }

        #endregion
    }

    public class ImplementationOfCommunicationApplicationServiceBase : CommunicationApplicationServiceBase
    {
        public ImplementationOfCommunicationApplicationServiceBase(
            Lazy<IApplicationServiceCommonService> applicationServiceCommonService,
            Lazy<ICommunicationSender> communicationSender,
            Lazy<ICommunicationTemplateLoader> communicationTemplateLoader,
            Lazy<ICommunicationService> communicationService,
            Lazy<IEmailService> emailService,
            Lazy<IGuarantorService> guarantorService,
            Lazy<IVisitPayUserRepository> visitPayUserRepository,
            Lazy<IVisitPayUserService> visitPayUserService)
            : base(applicationServiceCommonService,
                communicationSender,
                communicationTemplateLoader,
                communicationService,
                emailService,
                guarantorService,
                visitPayUserRepository,
                visitPayUserService)
        {
        }

        public bool SendCommunicationsTest(SendTo sendTo, CommunicationTypeEnum communicationTypeEnum)
        {
            return this.SendCommunications(sendTo, communicationTypeEnum, visitPayUser => new Dictionary<string, string>());
        }

        public bool SendEmailTest(CommunicationDto communicationDto, NotificationFrequencyEnum frequencyEnum, IList<CommunicationTypeEnum> additionalCheckTypes)
        {
            return this.SendEmail(communicationDto, frequencyEnum, additionalCheckTypes);
        }

        public bool SendSmsTest(CommunicationDto communicationDto, NotificationFrequencyEnum frequencyEnum, IList<CommunicationTypeEnum> additionalCheckTypes)
        {
            return this.SendSms(communicationDto, frequencyEnum, additionalCheckTypes);
        }

        public bool SendMailTest(CommunicationDto communicationDto, NotificationFrequencyEnum frequencyEnum, IList<CommunicationTypeEnum> additionalCheckTypes)
        {
            return this.SendMail(communicationDto, frequencyEnum, additionalCheckTypes);
        }
    }

    public class ImplementationOfCommunicationApplicationServiceBaseMockBuilder : EmailsApplicationServiceMockBuilder, IMockBuilder<ImplementationOfCommunicationApplicationServiceBase, ImplementationOfCommunicationApplicationServiceBaseMockBuilder>
    {
        public ImplementationOfCommunicationApplicationServiceBase CreateService()
        {
            return new ImplementationOfCommunicationApplicationServiceBase(
                this.ApplicationServiceCommonServiceMockBuilder.CreateServiceLazy(),
                new Lazy<ICommunicationSender>(() => this.CommunicationSender ?? this.CommunicationSenderMock.Object),
                new Lazy<ICommunicationTemplateLoader>(() => this.CommunicationTemplateLoader ?? this.CommunicationTemplateLoaderMock.Object),
                new Lazy<ICommunicationService>(() => this.CommunicationService ?? this.CommunicationServiceMock.Object),
                new Lazy<IEmailService>(() => this.EmailService ?? this.EmailServiceMock.Object),
                new Lazy<IGuarantorService>(() => this.GuarantorService ?? this.GuarantorServiceMock.Object),
                new Lazy<IVisitPayUserRepository>(() => this.VisitPayUserRepository ?? this.VisitPayUserRepositorMock.Object),
                new Lazy<IVisitPayUserService>(() => this.VisitPayUserService ?? this.VisitPayUserServiceMock.Object)
            );
        }

        public ImplementationOfCommunicationApplicationServiceBaseMockBuilder Setup(Action<ImplementationOfCommunicationApplicationServiceBaseMockBuilder> configAction)
        {
            configAction(this);
            return this;
        }

        public void VerifyEmail(Guarantor sendToGuarantor, bool isConsolidated, bool isManaging)
        {
            this.CommunicationTemplateLoaderMock.Verify(x => x.CreateEmailFromTemplate(
                It.IsAny<CommunicationTypeEnum>(),
                It.IsAny<ClientDto>(),
                It.IsAny<SendTo>(),
                sendToGuarantor.User.Locale,
                sendToGuarantor.User.Email,
                It.IsAny<IDictionary<string, string>>(),
                isConsolidated,
                isManaging,
                It.IsAny<bool>()
            ), Times.Once());
        }

        public void VerifyEmailDidNotSend()
        {
            this.CommunicationTemplateLoaderMock.Verify(x => x.CreateEmailFromTemplate(
                It.IsAny<CommunicationTypeEnum>(),
                It.IsAny<ClientDto>(),
                It.IsAny<SendTo>(),
                It.IsAny<string>(),
                It.IsAny<string>(),
                It.IsAny<IDictionary<string, string>>(),
                It.IsAny<bool>(),
                It.IsAny<bool>(),
                It.IsAny<bool>()
            ), Times.Never());
        }

        public void VerifySms(Guarantor sendToGuarantor, bool isConsolidated, bool isManaging)
        {
            this.CommunicationTemplateLoaderMock.Verify(x => x.CreateSmsFromTemplate(
                It.IsAny<CommunicationTypeEnum>(),
                It.IsAny<ClientDto>(),
                It.IsAny<SendTo>(),
                sendToGuarantor.User.Locale,
                sendToGuarantor.User.SmsPhoneNumber,
                It.IsAny<IDictionary<string, string>>(),
                isConsolidated,
                isManaging
            ), Times.Once());
        }

        public void VerifySmsDidNotSend()
        {
            this.CommunicationTemplateLoaderMock.Verify(x => x.CreateSmsFromTemplate(
                It.IsAny<CommunicationTypeEnum>(),
                It.IsAny<ClientDto>(),
                It.IsAny<SendTo>(),
                It.IsAny<string>(),
                It.IsAny<string>(),
                It.IsAny<IDictionary<string, string>>(),
                It.IsAny<bool>(),
                It.IsAny<bool>()
            ), Times.Never());
        }

        public void VerifyMail(Guarantor sendToGuarantor, bool isConsolidated, bool isManaging)
        {
            this.EmailServiceMock.Verify(x => x.Update(It.IsAny<CommunicationDto>()), Times.Once());
        }

        public void VerifyMailDidNotSend()
        {
          this.EmailServiceMock.Verify(x => x.Update(It.IsAny<CommunicationDto>()),Times.Never());
        }

        public ImplementationOfCommunicationApplicationServiceBaseMockBuilder WithCommunicationPreferences(List<CommunicationMethodEnum> communicationMethods, int visitPayUserId)
        {
            List<VisitPayUserCommunicationPreference> visitPayUserCommunicationPreferences = new List<VisitPayUserCommunicationPreference>();

            foreach (CommunicationMethodEnum communicationMethod in communicationMethods)
            {
                visitPayUserCommunicationPreferences.AddRange(
                    Enum.GetValues(typeof(CommunicationGroupEnum))
                        .Cast<CommunicationGroupEnum>()
                        .Select(communicationGroup => new VisitPayUserCommunicationPreference
                        {
                            VisitPayUserId = visitPayUserId,
                            CommunicationMethodId = communicationMethod,
                            CommunicationGroupId = (int) communicationGroup
                        }).ToList()
                );
            }

            this.CommunicationServiceMock.Setup(x => x.CommunicationPreferences(It.IsAny<int>())).Returns(visitPayUserCommunicationPreferences);
            return this;
        }

        public ImplementationOfCommunicationApplicationServiceBaseMockBuilder WithFeatureEnabled(VisitPayFeatureEnum visitPayFeatureEnum)
        {
            this.ApplicationServiceCommonServiceMockBuilder.EnableFeatures(visitPayFeatureEnum);
            return this;
        }



    }


}

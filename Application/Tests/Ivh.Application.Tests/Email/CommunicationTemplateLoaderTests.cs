﻿namespace Ivh.Application.Tests.Email
{
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Application.Email.Internal.Dtos;
    using Application.Email.Internal.Implementations;
    using Common.Tests.Helpers.Email;
    using Common.VisitPay.Enums;
    using Domain.Content.Entities;
    using Domain.Visit.Entities;
    using Moq;
    using NUnit.Framework;

    [TestFixture]
    public class CommunicationTemplateLoaderTests : ApplicationTestBase
    {
        private const int VpGuarantorId = 99;

        [Test]
        public void ReplaceFacilityList_FeatureDisabled_ReplaceWithEmptyString()
        {
            CommunicationTemplateLoaderMockBuilder builder = new CommunicationTemplateLoaderMockBuilder();
            builder.FeatureServiceMock.Setup(x => x.IsFeatureEnabled(VisitPayFeatureEnum.FeatureFacilityListIsEnabled)).Returns(false);
            
            CommunicationTemplateLoader svc = (CommunicationTemplateLoader)builder.CreateService();

            string content = svc.ReplaceFacilityList(SendTo.Guarantor(VpGuarantorId), "[[VisitFacilitiesSentence]][[VisitFacilitiesList]]");
            Assert.AreEqual(0, content.Length);

            builder.VisitServiceMock.Verify(x => x.GetUniqueFacilityVisits(It.IsAny<int>()), Times.Never());
        }

        [Test]
        public void ReplaceFacilityList_NoFacilities_ReplaceWithEmptyString()
        {
            CommunicationTemplateLoaderMockBuilder builder = new CommunicationTemplateLoaderMockBuilder();
            builder.FeatureServiceMock.Setup(x => x.IsFeatureEnabled(VisitPayFeatureEnum.FeatureFacilityListIsEnabled)).Returns(true);
            builder.VisitServiceMock.Setup(x => x.GetUniqueFacilityVisits(VpGuarantorId)).Returns(() => new List<Facility>());

            CommunicationTemplateLoader svc = (CommunicationTemplateLoader)builder.CreateService();

            string content = svc.ReplaceFacilityList(SendTo.Guarantor(VpGuarantorId), "[[VisitFacilitiesSentence]][[VisitFacilitiesList]]");
            Assert.AreEqual(0, content.Length);
            
            builder.VisitServiceMock.Verify(x => x.GetUniqueFacilityVisits(VpGuarantorId), Times.Once());
        }

        [Test]
        public void ReplaceFacilityList_WithFacilities_ReplaceWithContent()
        {
            CmsVersion cmsVersionFacilitiesSentence = new CmsVersion 
            {
                ContentBody = "visit sentence"
            };

            CmsVersion cmsVersionFacilitiesListItem = new CmsVersion 
            {
                ContentBody = "[[FacilityDescription]] [[FacilityPhoneNumber]]"
            };

            List<Facility> facilities = new List<Facility>
            {
                new Facility { FacilityDescription = "facility 1", ContactPhone = "111-111-1111" },
                new Facility { FacilityDescription = "facility 2", ContactPhone = "222-222-2222" },
            };

            CommunicationTemplateLoaderMockBuilder builder = new CommunicationTemplateLoaderMockBuilder();
            builder.FeatureServiceMock.Setup(x => x.IsFeatureEnabled(VisitPayFeatureEnum.FeatureFacilityListIsEnabled)).Returns(true);
            builder.VisitServiceMock.Setup(x => x.GetUniqueFacilityVisits(VpGuarantorId)).Returns(() => facilities);

            builder.ContentServiceMock.Setup(x => x.GetCurrentVersionAsync(CmsRegionEnum.FacilitiesVisitSentence, It.IsAny<bool>(), It.IsAny<IDictionary<string, string>>()))
                .ReturnsAsync((CmsRegionEnum c, bool b, IDictionary<string, string> d) => cmsVersionFacilitiesSentence);
            
            builder.ContentServiceMock.Setup(x => x.GetCurrentVersionAsync(CmsRegionEnum.FacilitiesVisitListItem, It.IsAny<bool>(), It.IsAny<IDictionary<string, string>>()))
                .ReturnsAsync((CmsRegionEnum c, bool b, IDictionary<string, string> d) => cmsVersionFacilitiesListItem);

            CommunicationTemplateLoader svc = (CommunicationTemplateLoader) builder.CreateService();

            string content = svc.ReplaceFacilityList(SendTo.Guarantor(VpGuarantorId), "[[VisitFacilitiesSentence]][[VisitFacilitiesList]]");
            
            Assert.False(content.Contains("["));
            Assert.False(content.Contains("]"));
            Assert.True(content.Contains(cmsVersionFacilitiesSentence.ContentBody));
            foreach (Facility facility in facilities)
            {
                Assert.True(content.Contains(facility.FacilityDescription));
                Assert.True(content.Contains(facility.ContactPhone));
            }

            builder.VisitServiceMock.Verify(x => x.GetUniqueFacilityVisits(VpGuarantorId), Times.Once());
        }
    }
}
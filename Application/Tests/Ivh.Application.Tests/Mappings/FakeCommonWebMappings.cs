﻿namespace Ivh.Application.Tests.Mappings
{
    using Common.Web.Mapping;

    /// <summary>
    /// Exposes the abstract base class to the test project,
    /// since this project doesn't have references for the actual derived types.
    /// </summary>
    public class FakeCommonWebMappings : CommonWebMappings
    {
    }
}
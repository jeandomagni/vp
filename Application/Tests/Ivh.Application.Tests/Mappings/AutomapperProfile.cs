﻿namespace Ivh.Application.Tests.Mappings
{
    using System;
    using System.Linq;
    using Application.Core.Common.Dtos;
    using AutoMapper;
    using Domain.FinanceManagement.FinancePlan.Entities;
    using Domain.FinanceManagement.Payment.Entities;
    using Domain.Visit.Entities;

    public class AutomapperProfile : Profile
    {
        public AutomapperProfile()
        {
            this.CreateMap<FinancePlan, PaymentFinancePlan>()
                .ForMember(dest => dest.FinancePlanStatus, opts => opts.MapFrom(f => f.FinancePlanStatus.FinancePlanStatusEnum));

            this.CreateMap<VisitDto, PaymentVisit>()
                .ForMember(dest => dest.CurrentVisitState, opts => opts.MapFrom(f => f.CurrentVisitState));

            this.CreateMap<Visit, PaymentVisit>()
                .ForMember(dest => dest.CurrentVisitState, opts => opts.MapFrom(f => f.CurrentVisitState))
                .ForMember(dest => dest.DiscountPercentage, opts => opts.MapFrom(f => (f.VisitInsurancePlans != null && f.VisitInsurancePlans.Count > 0) ? f.VisitInsurancePlans.FirstOrDefault().InsurancePlan.DiscountPercentage ?? 0 : 0));
            
            this.CreateMap<VisitDto, Visit>()
                .ForMember(dest => dest.DischargeDate, x => x.MapFrom(src => src.DischargeDate == DateTime.MinValue ? (DateTime?) null : src.DischargeDate));

            this.CreateMap<Visit, VisitDto>()
                .ForMember(dest => dest.DischargeDate, x => x.MapFrom(src => src.DischargeDate ?? DateTime.MinValue))
                .ForMember(dest => dest.SourceSystemKeyDisplay, x => x.MapFrom(src => src.SourceSystemKeyDisplay ?? src.SourceSystemKey));
        }
    }
}

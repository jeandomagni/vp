﻿namespace Ivh.Application.Tests.Mappings
{
    using Common.Tests;
    using NUnit.Framework;

    public class MappingValidationTests
    {
        [Test]
        [NonParallelizable]
        public void ValidateBaseMappings()
        {
            MappingValidator.Validate(mappingBuilder =>
            {
                mappingBuilder
                    .WithBase();
            });
        }
    }
}
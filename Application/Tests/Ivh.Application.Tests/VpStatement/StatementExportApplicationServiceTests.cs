﻿namespace Ivh.Application.Tests.VpStatement
{
    using Application.FinanceManagement.ApplicationServices;
    using Common.Tests.Helpers.VpStatement;
    using NUnit.Framework;

    [TestFixture]
    public class StatementExportApplicationServiceTests
    {
        [Test]
        public void OcrIncludesCents()
        {
            StatementExportApplicationServiceMockBuilder builder = new StatementExportApplicationServiceMockBuilder();
            StatementExportApplicationService svc = (StatementExportApplicationService)builder.CreateService();

            const decimal balance = 987654.32m;
            string ocr = svc.GetScanLineJpmc("lastName", "firstName", "1234567", balance);

            string expectedValue = balance.ToString("0.00").Replace(".", "");
            Assert.True(ocr.Contains(expectedValue));
        }
    }
}
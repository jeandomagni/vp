﻿namespace Ivh.Application.Tests.VpStatement
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Application.FinanceManagement.Common.Dtos;
    using Application.FinanceManagement.Common.Interfaces;
    using AutoMapper;
    using Common.Base.Utilities.Extensions;
    using Common.Tests.Helpers.VpStatement;
    using Common.Web.Models.Statement;
    using Domain.FinanceManagement.Statement.Entities;
    using Domain.FinanceManagement.Statement.Interfaces;
    using Domain.Guarantor.Entities;
    using Domain.User.Entities;
    using Domain.Visit.Entities;
    using Mappings;
    using Moq;
    using NUnit.Framework;

    [TestFixture]
    public class StatementApplicationServiceTests : ApplicationTestBase
    {
        private readonly Facility _fakeFacility = new Facility
        {
            FacilityId = 10,
            FacilityDescription = "Medical Center"
        };

        private IStatementApplicationService GetStatementApplicationServiceMockBuilder(Guarantor guarantor, IList<VpStatement> statements)
        {
            statements = statements ?? new List<VpStatement>();

            StatementApplicationServiceMockBuilder builder = new StatementApplicationServiceMockBuilder();
            IVpStatementService vpStatementService = new VpStatementServiceMockBuilder().Setup(x =>
            {
                x.StatementDateService = new StatementDateServiceMockBuilder().Setup(y => { }).CreateService();
                x.VpStatementRepositoryMock.Setup(t => t.GetMostRecentStatement(It.IsAny<Guarantor>())).Returns(statements.FirstOrDefault());
                x.VpStatementRepositoryMock.Setup(t => t.GetVpStatement(It.IsAny<int>(), It.IsAny<int>())).Returns(statements.FirstOrDefault());
            }).GetMockedVpStatementService();

            builder.VpStatementService = vpStatementService;
            builder.Setup(mock =>
            {
                mock.GuarantorServiceMock.Setup(x => x.GetGuarantor(guarantor.VpGuarantorId)).Returns(guarantor);
                mock.VpStatementServiceMock.Setup(x => x.GetMostRecentStatement(It.IsAny<Guarantor>())).Returns(statements.FirstOrDefault());
                VpStatement vpStatement = new VpStatement();
                mock.VpStatementServiceMock.Setup(x => x.GetStatement(It.IsAny<int>(), It.IsAny<int>())).Returns(vpStatement);
            });
            return builder.CreateService();
        }

        #region VPNG-19982 and VPNG-19339

        [Test]
        public void GetNextPaymentDateForDisplay_AwaitingStatement()
        {
            DateTime paymentDueDate = DateTime.UtcNow.AddDays(-5);

            VpStatement statement = VpStatementFactory.GenerateStatementPaymentDueDate(paymentDueDate);

            IStatementApplicationService statementApplicationService = this.GetStatementApplicationServiceMockBuilder(new Guarantor { PaymentDueDay = paymentDueDate.Day, RegistrationDate = DateTime.UtcNow.AddDays(-10) }, statement.ToListOfOne());
            DateTime? nextPaymentDateForDisplay = statementApplicationService.GetNextPaymentDate(0);

            Assert.True(nextPaymentDateForDisplay.HasValue && nextPaymentDateForDisplay.Value > paymentDueDate);
        }

        [Test]
        public void GetNextPaymentDateForDisplay_GracePeriod()
        {
            DateTime paymentDueDate = DateTime.UtcNow.AddDays(10);

            VpStatement statement = VpStatementFactory.GenerateStatementPaymentDueDate(paymentDueDate);

            IStatementApplicationService statementApplicationService = this.GetStatementApplicationServiceMockBuilder(new Guarantor { PaymentDueDay = paymentDueDate.Day, RegistrationDate = DateTime.UtcNow.AddDays(-10) }, statement.ToListOfOne());
            DateTime? nextPaymentDateForDisplay = statementApplicationService.GetNextPaymentDate(0);

            Assert.True(nextPaymentDateForDisplay.HasValue);
            Assert.AreEqual(nextPaymentDateForDisplay.Value.Date, paymentDueDate.Date);
        }

        [Test]
        public void GetNextPaymentDateForDisplay_ImmediateStatement_PastPdd()
        {
            DateTime paymentDueDate = DateTime.UtcNow.AddDays(-5);
            VpStatement statement = VpStatementFactory.GenerateStatementPaymentDueDate(paymentDueDate);
            IStatementApplicationService statementApplicationService = this.GetStatementApplicationServiceMockBuilder(new Guarantor { PaymentDueDay = paymentDueDate.Day, RegistrationDate = DateTime.UtcNow.AddDays(-10) }, statement.ToListOfOne());

            statement.PaymentDueDate = DateTime.UtcNow.AddDays(-1);

            DateTime? nextPaymentDateForDisplay = statementApplicationService.GetNextPaymentDate(0);

            Assert.True(nextPaymentDateForDisplay.GetValueOrDefault(DateTime.MinValue).Date > statement.PaymentDueDate);
        }

        [Test]
        public void GetNextPaymentDateForDisplay_ImmediateStatement_FuturePdd()
        {
            DateTime paymentDueDate = DateTime.UtcNow.AddDays(21);
            VpStatement statement = VpStatementFactory.GenerateStatementPaymentDueDate(paymentDueDate);
            IStatementApplicationService statementApplicationService = this.GetStatementApplicationServiceMockBuilder(new Guarantor { PaymentDueDay = paymentDueDate.Day, RegistrationDate = DateTime.UtcNow.AddDays(-10) }, statement.ToListOfOne());

            statement.PaymentDueDate = paymentDueDate;
            statement.OriginalPaymentDueDate = paymentDueDate;

            DateTime? nextPaymentDateForDisplay = statementApplicationService.GetNextPaymentDate(0);

            Assert.AreEqual(paymentDueDate.Date, nextPaymentDateForDisplay.GetValueOrDefault(DateTime.MinValue).Date);
        }

        [Test]
        public void GetNextPaymentDateForDisplay_WithNoStatement()
        {
            Guarantor guarantor = new Guarantor
            {
                PaymentDueDay = 11,
                RegistrationDate = DateTime.UtcNow
            };

            IStatementApplicationService statementApplicationService = this.GetStatementApplicationServiceMockBuilder(guarantor, null);

            DateTime? paymentDueDate = statementApplicationService.GetNextPaymentDate(0);

            Assert.True(paymentDueDate.HasValue);
            Assert.True(paymentDueDate.Value > guarantor.RegistrationDate);
            Assert.True(paymentDueDate.Value.Day == guarantor.PaymentDueDay, paymentDueDate.Value.Day.ToString());
        }

        #endregion

        [Test]
        public void GetStatement_VpStatementVisitHasFacility_MappedStatementViewModelIncludesFacilityDescription()
        {
            IStatementApplicationService statementAppSvc = this.ArrangeTestConditions();

            StatementDto statementDto = statementAppSvc.GetStatement(1234, 5678);
            StatementViewModel statementViewModel = Mapper.Map<StatementViewModel>(statementDto);

            Assert.AreEqual(this._fakeFacility.FacilityDescription, statementViewModel.VisitsNotOnFinancePlan.First().FacilityDescription);
        }

        private IStatementApplicationService ArrangeTestConditions()
        {
            Mapper.Reset();
            Mapper.Initialize(cfg =>
            {
                cfg.AddProfile<Application.Core.Mappings.AutomapperProfile>();
                cfg.AddProfile<Application.FinanceManagement.Mappings.AutomapperProfile>();
                cfg.AddProfile<FakeCommonWebMappings>();
            });

            Guarantor guarantor = new Guarantor
            {
                PaymentDueDay = 11,
                RegistrationDate = DateTime.UtcNow,
                User = new VisitPayUser
                {
                    FirstName = "test",
                    LastName = "test",
                    CurrentPhysicalAddress = new VisitPayUserAddress
                    {
                        AddressStreet1 = "123 Main",
                        AddressStreet2 = "APT 202",
                        City = "City",
                        StateProvince = "CO",
                        PostalCode = "12345"
                    }
                }
            };

            DateTime paymentDueDate = DateTime.UtcNow.AddDays(21);
            VpStatement statement = VpStatementFactory.GenerateStatementPaymentDueDate(paymentDueDate);

            statement.VpStatementVisits = new List<VpStatementVisit>
            {
                new VpStatementVisit
                {
                    Visit = new Visit
                    {
                        Facility = this._fakeFacility
                    }
                }
            };

            IStatementApplicationService statementAppSvc =
                this.GetStatementApplicationServiceMockBuilder(guarantor, statement.ToListOfOne());

            return statementAppSvc;
        }
    }
}
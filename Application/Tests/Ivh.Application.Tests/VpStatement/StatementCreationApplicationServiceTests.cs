﻿namespace Ivh.Application.Tests.VpStatement
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Application.FinanceManagement.ApplicationServices;
    using Common.Tests.Helpers.FinancePlan;
    using Common.Tests.Helpers.Guarantor;
    using Common.Tests.Helpers.Visit;
    using Common.Tests.Helpers.VpStatement;
    using Domain.Guarantor.Entities;
    using Domain.Visit.Entities;
    using Domain.FinanceManagement.FinancePlan.Entities;
    using Domain.FinanceManagement.Statement.Entities;
    using Moq;
    using NUnit.Framework;
    using Application.FinanceManagement.Common.Interfaces;
    using Common.Base.Utilities.Extensions;
    using Common.Base.Utilities.Helpers;
    using Common.Tests.Builders;
    using Common.VisitPay.Enums;
    using Domain.FinanceManagement.Interfaces;
    using Domain.Settings.Entities;

    [TestFixture]
    public class StatementCreationApplicationServiceTests : ApplicationTestBase
    {
        private List<VpStatement> vpStatements = new List<VpStatement>();

        [Test]
        public void WhenStatementingWithFinancePlans_BucketsIncremented()
        {
            Guarantor guarantor = GuarantorFactory.GenerateOnline();
            VpStatement statement = VpStatementFactory.GenerateStatementPaymentDueDateInXDays(10);
            List<Tuple<Visit, IList<VisitTransaction>>> visits = new List<Tuple<Visit, IList<VisitTransaction>>>() { VisitFactory.GenerateActiveVisit(100m, 2, VisitStateEnum.Active) };
            VpStatementFactory.AddVisitsToStatement(visits, statement);

            FinancePlan fp = FinancePlanFactory.GenerateWithStatus(visits, new FinancePlanOffer(), statement.PaymentDueDate, statement, 10, FinancePlanStatusEnum.GoodStanding);

            bool calledAddAmountDue = false;

            StatementCreationApplicationServiceMockBuilder builder = new StatementCreationApplicationServiceMockBuilder();
            builder.Setup(mock =>
            {
                mock.GuarantorServiceMock.Setup(x => x.GetGuarantor(guarantor.VpGuarantorId)).Returns(guarantor);
                mock.FinancePlanServiceMock.Setup(x => x.GetAllPendingAcceptanceFinancePlansForGuarantor(guarantor, It.IsAny<bool>(), It.IsAny<bool>())).Returns(new List<FinancePlan>());
                mock.FinancePlanServiceMock.Setup(x => x.GetAllOpenFinancePlansForGuarantor(guarantor, It.IsAny<bool>())).Returns(new List<FinancePlan>() { fp });
                mock.VpStatementServiceMock.Setup(x => x.GetMostRecentStatement(It.IsAny<Guarantor>())).Returns(() => null);
                mock.ApplicationServiceCommonServiceMockBuilder.ClientServiceMock.Setup(x => x.GetClient()).Returns(() =>
                {
                    Mock<Client> clientMock = new Mock<Client>();
                    clientMock.Setup(t => t.StatementGenerationDays).Returns(2);
                    clientMock.Setup(t => t.GracePeriodLength).Returns(21);
                    clientMock.Setup(t => t.CreditAgreementMinResponsePeriod).Returns(10);
                    return clientMock.Object;
                });
                mock.StatementSnapshotCreationApplicationServiceMock.Setup(x => x.GenerateStatementForDateAsync(It.IsAny<Guarantor>(), It.IsAny<DateTime>(), It.IsAny<bool>())).Returns(Task.FromResult(new GeneratedStatementsResultDto { VpStatement = VpStatementFactory.GenerateStatementPaymentDueDateInXDays(10) }));
                mock.FinancePlanServiceMock.Setup(x => x.AddTheAmountDueForFinancePlan(It.IsAny<FinancePlan>(), It.IsAny<VpStatement>(), It.IsAny<string>())).Callback<FinancePlan, VpStatement, string>((callBackFp, callBackStatement, s) =>
                {
                    calledAddAmountDue = true;
                });
                mock.ApplicationServiceCommonServiceMockBuilder.DistributedCacheMock.Setup(x => x.GetLock(It.IsAny<string>())).Returns(true);
            });

            IStatementCreationApplicationService svc = builder.CreateService();

            svc.CreateStatementForGuarantorAsync(guarantor.VpGuarantorId, DateTime.UtcNow, false).Wait();
            //Probably should do more here.
            Assert.IsTrue(calledAddAmountDue);
        }

        private void SetupMockForFinancePlanCancellationTests(StatementCreationApplicationServiceMockBuilder builder, Guarantor guarantor, FinancePlan fp)
        {
            builder.Setup(mock =>
            {
                mock.GuarantorServiceMock.Setup(x => x.GetGuarantor(guarantor.VpGuarantorId)).Returns(guarantor);
                mock.FinancePlanServiceMock.Setup(x => x.GetAllPendingAcceptanceFinancePlansForGuarantor(guarantor, It.IsAny<bool>(), It.IsAny<bool>())).Returns(new List<FinancePlan> { fp });
                mock.FinancePlanServiceMock.Setup(x => x.GetAllOpenFinancePlansForGuarantor(guarantor, It.IsAny<bool>())).Returns(new List<FinancePlan>());
                mock.VpStatementServiceMock.Setup(x => x.GetMostRecentStatement(It.IsAny<Guarantor>())).Returns(() => null);
                mock.StatementSnapshotCreationApplicationServiceMock.Setup(x => x.GenerateStatementForDateAsync(It.IsAny<Guarantor>(), It.IsAny<DateTime>(), It.IsAny<bool>())).Returns(Task.FromResult(new GeneratedStatementsResultDto { VpStatement = VpStatementFactory.GenerateStatementPaymentDueDateInXDays(10) }));
                Lazy<TimeZoneHelper> timeZoneHelper = new Lazy<TimeZoneHelper>(() => new TimeZoneHelper(TimeZoneHelper.TimeZones.PacificStandardTime));
                mock.ApplicationServiceCommonServiceMockBuilder.ClientServiceMock.Setup(x => x.GetClient()).Returns(new Client(new Dictionary<string, string>
                {
                    {"CreditAgreementMinResponsePeriod", "10"}
                }, timeZoneHelper));
                mock.FinancePlanServiceMock.Setup(x => x.CancelFinancePlan(It.IsAny<FinancePlan>(), It.IsAny<string>())).Callback((FinancePlan financePlan, string reason) =>
                {
                    financePlan.SetFinancePlanStatus(new FinancePlanStatus() { FinancePlanStatusId = (int)FinancePlanStatusEnum.Canceled }, reason);
                });
                mock.ApplicationServiceCommonServiceMockBuilder.DistributedCacheMock.Setup(x => x.GetLock(It.IsAny<string>())).Returns(true);
            });
        }

        private void SetupMockForPaperStatementExportTests(StatementCreationApplicationServiceMockBuilder builder, Guarantor guarantor, FinancePlan fp, bool hasPaperCommunicationPreference, IList<VpStatement> mockStatementRepository)
        {
            builder.Setup(mock =>
            {
                mock.GuarantorServiceMock.Setup(x => x.GetGuarantor(It.IsAny<int>())).Returns(guarantor);
                mock.FinancePlanServiceMock.Setup(x => x.GetAllPendingAcceptanceFinancePlansForGuarantor(guarantor, It.IsAny<bool>(), It.IsAny<bool>())).Returns(new List<FinancePlan> { fp });
                mock.FinancePlanServiceMock.Setup(x => x.GetAllOpenFinancePlansForGuarantor(guarantor, It.IsAny<bool>())).Returns(new List<FinancePlan> { fp });
                mock.VpStatementServiceMock.Setup(x => x.GetMostRecentStatement(It.IsAny<Guarantor>())).Returns(() => null);

                mock.StatementSnapshotCreationApplicationServiceMock
                    .Setup(x => x.GenerateStatementForDateAsync(It.IsAny<Guarantor>(), It.IsAny<DateTime>(), It.IsAny<bool>()))
                    .Returns(() =>
                    {
                        VpStatement newStatement = VpStatementFactory.GenerateStatementPaymentDueDateInXDays(10);
                        mockStatementRepository.AddDistinct(newStatement);

                        return Task.FromResult(new GeneratedStatementsResultDto
                        {
                            VpStatement = newStatement
                        });
                    });

                mock.VisitPayUserApplicationServiceMock
                    .Setup(x => x.UserHasCommunicationPreference(It.IsAny<int>(), CommunicationMethodEnum.Mail, CommunicationGroupEnum.StatementAndBalanceDue))
                    .Returns(hasPaperCommunicationPreference);

                Lazy<TimeZoneHelper> timeZoneHelper = new Lazy<TimeZoneHelper>(() => new TimeZoneHelper(TimeZoneHelper.TimeZones.PacificStandardTime));
                mock.ApplicationServiceCommonServiceMockBuilder.ClientServiceMock.Setup(x => x.GetClient()).Returns(new Client(new Dictionary<string, string>
                {
                    {"CreditAgreementMinResponsePeriod", "10"}
                }, timeZoneHelper));
                mock.FinancePlanServiceMock.Setup(x => x.CancelFinancePlan(It.IsAny<FinancePlan>(), It.IsAny<string>())).Callback((FinancePlan financePlan, string reason) =>
                {
                    financePlan.SetFinancePlanStatus(new FinancePlanStatus() { FinancePlanStatusId = (int)FinancePlanStatusEnum.Canceled }, reason);
                });
                mock.ApplicationServiceCommonServiceMockBuilder.DistributedCacheMock.Setup(x => x.GetLock(It.IsAny<string>())).Returns(true);
            });
        }


        public void SetupMockForImmediateStatement(StatementCreationApplicationServiceMockBuilder builder, Guarantor guarantor, bool featureImmediateStatementingIsEnabled, bool hasPreviousStatement, bool isImmediateStatement)
        {

            VpStatement statement = VpStatementFactory.GenerateStatementPaymentDueDateInXDays(10);
            List<Tuple<Visit, IList<VisitTransaction>>> visits = new List<Tuple<Visit, IList<VisitTransaction>>>() { VisitFactory.GenerateActiveVisit(100m, 2, VisitStateEnum.Active) };
            VpStatementFactory.AddVisitsToStatement(visits, statement);

            FinancePlan fp = FinancePlanFactory.GenerateWithStatus(visits, new FinancePlanOffer(), statement.PaymentDueDate, statement, 10, FinancePlanStatusEnum.GoodStanding);

            builder.Setup(mock =>
            {
                mock.ApplicationServiceCommonServiceMockBuilder.FeatureServiceMock.Setup(x => x.IsFeatureEnabled(It.IsAny<VisitPayFeatureEnum>())).Returns<VisitPayFeatureEnum>(feature =>
                {
                    return true;
                });
                mock.GuarantorServiceMock.Setup(x => x.GetGuarantor(guarantor.VpGuarantorId)).Returns(guarantor);
                mock.FinancePlanServiceMock.Setup(x => x.GetAllPendingAcceptanceFinancePlansForGuarantor(guarantor, It.IsAny<bool>(), It.IsAny<bool>())).Returns(() =>
                {
                    return (hasPreviousStatement) ? new List<FinancePlan>() : new List<FinancePlan> { fp };
                });
                mock.FinancePlanServiceMock.Setup(x => x.GetAllOpenFinancePlansForGuarantor(guarantor, It.IsAny<bool>())).Returns(new List<FinancePlan>());
                mock.VpStatementServiceMock.Setup(x => x.GetMostRecentStatement(It.IsAny<Guarantor>())).Returns(() =>
                {
                    if (hasPreviousStatement)
                    {
                        return VpStatementFactory.GenerateStatementPaymentDueDateInXDays(10);
                    }
                    return null;
                });

                mock.ApplicationServiceCommonServiceMockBuilder.ClientServiceMock.Setup(x => x.GetClient()).Returns(() =>
                {
                    Mock<Client> clientMock = new Mock<Client>();
                    clientMock.Setup(t => t.StatementGenerationDays).Returns(2);
                    clientMock.Setup(t => t.GracePeriodLength).Returns(21);
                    clientMock.Setup(t => t.CreditAgreementMinResponsePeriod).Returns(10);
                    return clientMock.Object;
                });
                mock.StatementSnapshotCreationApplicationServiceMock.Setup(x => x.GenerateStatementForDateAsync(It.IsAny<Guarantor>(), It.IsAny<DateTime>(), It.IsAny<bool>())).Returns(() =>
                {
                    VpStatement vpStatement = VpStatementFactory.GenerateStatementPaymentDueDateInXDays(10);
                    this.vpStatements.Add(vpStatement);
                    return Task.FromResult(new GeneratedStatementsResultDto { VpStatement = vpStatement });
                });
                mock.FinancePlanServiceMock.Setup(x => x.AddTheAmountDueForFinancePlan(It.IsAny<FinancePlan>(), It.IsAny<VpStatement>(), It.IsAny<string>())).Callback<FinancePlan, VpStatement, string>((callBackFp, callBackStatement, s) =>
                {

                });
                mock.ApplicationServiceCommonServiceMockBuilder.DistributedCacheMock.Setup(x => x.GetLock(It.IsAny<string>())).Returns(true);
            });


        }

        [TestCase(true, false, true, true, " Immediate Statementing enabled and  Immediate Statement is true without previous statement. Expected:  statement created")]
        [TestCase(true, true, true, false, " Immediate Statementing enabled and  Immediate Statement is true with previous statement. Expected:  no statement created")]
        [TestCase(true, true, false, true, " Immediate Statementing enabled and  Immediate Statement is false with previous statement. Expected: statement created")]
        [TestCase(false, true, false, true, " Immediate Statementing disabled and  Immediate Statement is false with previous statement. Expected: statement created")]
        [TestCase(false, false, false, true, " Immediate Statementing disabled and  Immediate Statement is false without previous statements. Expected: statement created")]
        [TestCase(true, false, false, true, " Immediate Statementing enabled and  Immediate Statement is false without previous statements. Expected: statement created")]
        public void ShouldStatementBeCreated(
            bool immediateStatementIsEnabled,
            bool hasPreviousStatement,
            bool isImmediateStatement,
            bool shouldCreateStatement,
            string message
        )
        {
            //clear the statements- this will be added to in the StatementService (see SetupMockForImmediateStatement)
            this.vpStatements = new List<VpStatement>();
            Guarantor guarantor = GuarantorFactory.GenerateOnline();
            StatementCreationApplicationServiceMockBuilder builder = new StatementCreationApplicationServiceMockBuilder();

            this.SetupMockForImmediateStatement(builder, guarantor, immediateStatementIsEnabled, hasPreviousStatement, isImmediateStatement);

            IStatementCreationApplicationService svc = builder.CreateService();

            svc.CreateStatementForGuarantorAsync(guarantor.VpGuarantorId, DateTime.UtcNow, isImmediateStatement).Wait();

            // if VpStatementService.GenerateStatementForDateAsync was called then vpStatements should have a statement
            bool statementWasCreated = this.vpStatements.Count == 1;
            Assert.True(shouldCreateStatement == statementWasCreated, message);
        }

        [Test]
        public void CreateStatement_OfflineGuarantor_InExtendedPeriod_FPRemainsPending()
        {
            Guarantor guarantor = GuarantorFactory.GenerateOffline();
            VpStatement statement = VpStatementFactory.GenerateStatementPaymentDueDateInXDays(10);
            List<Tuple<Visit, IList<VisitTransaction>>> visits = new List<Tuple<Visit, IList<VisitTransaction>>>() { VisitFactory.GenerateActiveVisit(100m, 2, VisitStateEnum.Active) };
            VpStatementFactory.AddVisitsToStatement(visits, statement);

            FinancePlan fp = FinancePlanFactory.GenerateWithStatus(visits, new FinancePlanOffer(), statement.PaymentDueDate, statement, 10, FinancePlanStatusEnum.PendingAcceptance);

            StatementCreationApplicationServiceMockBuilder builder = new StatementCreationApplicationServiceMockBuilder();
            this.SetupMockForFinancePlanCancellationTests(builder, guarantor, fp);

            IStatementCreationApplicationService svc = builder.CreateService();

            svc.CreateStatementForGuarantorAsync(guarantor.VpGuarantorId, DateTime.UtcNow, false).Wait();

            Assert.AreEqual(FinancePlanStatusEnum.PendingAcceptance, fp.FinancePlanStatus.FinancePlanStatusEnum);
        }

        [Test]
        public void CreateStatement_OfflineGuarantor_InExtendedPeriod_SecondStatement_FPCancelled()
        {
            Guarantor guarantor = GuarantorFactory.GenerateOffline();
            VpStatement statement = VpStatementFactory.GenerateStatementPaymentDueDateInXDays(10);
            List<Tuple<Visit, IList<VisitTransaction>>> visits = new List<Tuple<Visit, IList<VisitTransaction>>>() { VisitFactory.GenerateActiveVisit(100m, 2, VisitStateEnum.Active) };
            VpStatementFactory.AddVisitsToStatement(visits, statement);

            FinancePlan fp = FinancePlanFactory.GenerateWithStatus(visits, new FinancePlanOffer(), statement.PaymentDueDate, statement, 10, FinancePlanStatusEnum.PendingAcceptance);

            StatementCreationApplicationServiceMockBuilder builder = new StatementCreationApplicationServiceMockBuilder();
            this.SetupMockForFinancePlanCancellationTests(builder, guarantor, fp);

            IStatementCreationApplicationService svc = builder.CreateService();

            svc.CreateStatementForGuarantorAsync(guarantor.VpGuarantorId, DateTime.UtcNow, false).Wait();
            svc.CreateStatementForGuarantorAsync(guarantor.VpGuarantorId, DateTime.UtcNow.AddMonths(1), false).Wait();

            Assert.AreEqual(FinancePlanStatusEnum.Canceled, fp.FinancePlanStatus.FinancePlanStatusEnum);
        }

        [Test]
        public void CreateStatement_OfflineGuarantor_PastExtendedPeriod_FPCancelled()
        {
            Guarantor guarantor = GuarantorFactory.GenerateOffline();
            VpStatement statement = VpStatementFactory.GenerateStatementPaymentDueDateInXDays(10);
            List<Tuple<Visit, IList<VisitTransaction>>> visits = new List<Tuple<Visit, IList<VisitTransaction>>>() { VisitFactory.GenerateActiveVisit(100m, 2, VisitStateEnum.Active) };
            VpStatementFactory.AddVisitsToStatement(visits, statement);

            FinancePlan fp = FinancePlanFactory.GenerateWithStatus(visits, new FinancePlanOffer(), statement.PaymentDueDate, statement, 10, FinancePlanStatusEnum.PendingAcceptance, DateTime.UtcNow.AddDays(-11));

            StatementCreationApplicationServiceMockBuilder builder = new StatementCreationApplicationServiceMockBuilder();
            this.SetupMockForFinancePlanCancellationTests(builder, guarantor, fp);

            IStatementCreationApplicationService svc = builder.CreateService();

            svc.CreateStatementForGuarantorAsync(guarantor.VpGuarantorId, DateTime.UtcNow, false).Wait();

            Assert.AreEqual(FinancePlanStatusEnum.Canceled, fp.FinancePlanStatus.FinancePlanStatusEnum);
        }

        [Test]
        public void CreateStatement_OnlineGuarantor_InExtendedPeriod_FPCanceled()
        {
            Guarantor guarantor = GuarantorFactory.GenerateOnline();
            VpStatement statement = VpStatementFactory.GenerateStatementPaymentDueDateInXDays(10);
            List<Tuple<Visit, IList<VisitTransaction>>> visits = new List<Tuple<Visit, IList<VisitTransaction>>>() { VisitFactory.GenerateActiveVisit(100m, 2, VisitStateEnum.Active) };
            VpStatementFactory.AddVisitsToStatement(visits, statement);

            FinancePlan fp = FinancePlanFactory.GenerateWithStatus(visits, new FinancePlanOffer(), statement.PaymentDueDate, statement, 10, FinancePlanStatusEnum.PendingAcceptance);

            StatementCreationApplicationServiceMockBuilder builder = new StatementCreationApplicationServiceMockBuilder();
            this.SetupMockForFinancePlanCancellationTests(builder, guarantor, fp);

            IStatementCreationApplicationService svc = builder.CreateService();

            svc.CreateStatementForGuarantorAsync(guarantor.VpGuarantorId, DateTime.UtcNow, false).Wait();

            Assert.AreEqual(FinancePlanStatusEnum.Canceled, fp.FinancePlanStatus.FinancePlanStatusEnum);
        }

        [Test]
        public void CreateStatement_OnlineGuarantor_InExtendedPeriod_SecondStatement_FPCancelled()
        {
            Guarantor guarantor = GuarantorFactory.GenerateOnline();
            VpStatement statement = VpStatementFactory.GenerateStatementPaymentDueDateInXDays(10);
            List<Tuple<Visit, IList<VisitTransaction>>> visits = new List<Tuple<Visit, IList<VisitTransaction>>>() { VisitFactory.GenerateActiveVisit(100m, 2, VisitStateEnum.Active) };
            VpStatementFactory.AddVisitsToStatement(visits, statement);

            FinancePlan fp = FinancePlanFactory.GenerateWithStatus(visits, new FinancePlanOffer(), statement.PaymentDueDate, statement, 10, FinancePlanStatusEnum.PendingAcceptance);

            StatementCreationApplicationServiceMockBuilder builder = new StatementCreationApplicationServiceMockBuilder();
            this.SetupMockForFinancePlanCancellationTests(builder, guarantor, fp);

            IStatementCreationApplicationService svc = builder.CreateService();

            svc.CreateStatementForGuarantorAsync(guarantor.VpGuarantorId, DateTime.UtcNow, false).Wait();
            svc.CreateStatementForGuarantorAsync(guarantor.VpGuarantorId, DateTime.UtcNow.AddMonths(1), false).Wait();

            Assert.AreEqual(FinancePlanStatusEnum.Canceled, fp.FinancePlanStatus.FinancePlanStatusEnum);
        }

        [Test]
        public void CreateStatement_OnlineGuarantor_PastExtendedPeriod_FPCancelled()
        {
            Guarantor guarantor = GuarantorFactory.GenerateOnline();
            //guarantor.VpGuarantorTypeEnum = GuarantorTypeEnum.Offline;
            VpStatement statement = VpStatementFactory.GenerateStatementPaymentDueDateInXDays(10);
            List<Tuple<Visit, IList<VisitTransaction>>> visits = new List<Tuple<Visit, IList<VisitTransaction>>>() { VisitFactory.GenerateActiveVisit(100m, 2, VisitStateEnum.Active) };
            VpStatementFactory.AddVisitsToStatement(visits, statement);

            FinancePlan fp = FinancePlanFactory.GenerateWithStatus(visits, new FinancePlanOffer(), statement.PaymentDueDate, statement, 10, FinancePlanStatusEnum.PendingAcceptance, DateTime.UtcNow.AddDays(-11));

            StatementCreationApplicationServiceMockBuilder builder = new StatementCreationApplicationServiceMockBuilder();
            this.SetupMockForFinancePlanCancellationTests(builder, guarantor, fp);

            IStatementCreationApplicationService svc = builder.CreateService();

            svc.CreateStatementForGuarantorAsync(guarantor.VpGuarantorId, DateTime.UtcNow, false).Wait();

            Assert.AreEqual(FinancePlanStatusEnum.Canceled, fp.FinancePlanStatus.FinancePlanStatusEnum);
        }

        [TestCase(true, true, false)]
        [TestCase(true, false, false)]
        [TestCase(false, true, true)]
        [TestCase(false, false, false)]
        public void CreateStatement_OfflineGuarantor_PaperStatementExported(bool useAutopay, bool hasPaperCommunicationPreference, bool expectedPaperStatementExport)
        {
            //
            Guarantor guarantor = new GuarantorBuilder().WithDefaults()
                .AsOffline()
                .UseAutopay(useAutopay);
            VpStatement statement = VpStatementFactory.GenerateStatementPaymentDueDateInXDays(10);
            List<Tuple<Visit, IList<VisitTransaction>>> visits = new List<Tuple<Visit, IList<VisitTransaction>>>() { VisitFactory.GenerateActiveVisit(100m, 2, VisitStateEnum.Active) };
            VpStatementFactory.AddVisitsToStatement(visits, statement);
            IList<VpStatement> createdStatements = new List<VpStatement>();
            FinancePlan fp = FinancePlanFactory.GenerateWithStatus(visits, new FinancePlanOffer(), statement.PaymentDueDate, statement, 10, FinancePlanStatusEnum.PendingAcceptance, DateTime.UtcNow.AddDays(-11));

            StatementCreationApplicationServiceMockBuilder builder = new StatementCreationApplicationServiceMockBuilder();
            this.SetupMockForPaperStatementExportTests(builder, guarantor, fp, hasPaperCommunicationPreference, createdStatements);

            IStatementCreationApplicationService svc = builder.CreateService();

            //
            svc.CreateStatementForGuarantorAsync(guarantor.VpGuarantorId, DateTime.UtcNow, false).Wait();

            //
            Assert.AreEqual(expectedPaperStatementExport, createdStatements.First().CreatePaperStatement);
        }

        [TestCase(GuarantorTypeEnum.Online)]
        [TestCase(GuarantorTypeEnum.Offline)]
        public async Task CreateStatement_NonfinancedVisitsStatemented(GuarantorTypeEnum guarantorType)
        {
            //
            Guarantor guarantor = new GuarantorBuilder().WithDefaults().WithType(guarantorType);
            IList<Visit> visits = 2.Select(x => VisitFactory.GenerateActiveVisit(1000, 1, guarantor: guarantor)).Select(x => x.Item1).ToList();
            IList<Visit> financedVisits = visits.First().ToListOfOne();
            IStatementSnapshotCreationApplicationService service = this.CreateStatementSnapshotCreationApplicationService(visits, financedVisits);

            //
            GeneratedStatementsResultDto statement = await service.GenerateStatementForDateAsync(guarantor, DateTime.UtcNow, false);

            //
            bool nonFinancedVisitsShouldStatement = guarantorType == GuarantorTypeEnum.Online;
            bool nonFinancedVisitsWereStatemented = statement.VpStatement.ActiveVpStatementVisitsNotOnFinancePlan.IsNotNullOrEmpty();

            Assert.AreEqual(
                nonFinancedVisitsShouldStatement,
                nonFinancedVisitsWereStatemented,
                $"An {guarantorType.ToString()} guarantor user should {(nonFinancedVisitsShouldStatement ? "have": "not have any")} nonfinanced visits on a statement");
        }

        private IStatementSnapshotCreationApplicationService CreateStatementSnapshotCreationApplicationService(IList<Visit> allVisits, IList<Visit> financedVisits)
        {
            StatementSnapshotCreationVp3V1ApplicationServiceMockBuilder builder = new StatementSnapshotCreationVp3V1ApplicationServiceMockBuilder();

            VpStatement statement = new VpStatement();
            statement.StatementVersion = VpStatementVersionEnum.Vp3V1;
            statement.PeriodEndDate = DateTime.UtcNow;
            statement.PeriodStartDate = DateTime.UtcNow.AddDays(-21);
            builder.VpStatementServiceMock.Setup(x => x.CreateStatement(
                It.IsAny<Guarantor>(),
                It.IsAny<DateTime>(),
                It.IsAny<VpStatement>(),
                It.IsAny<bool>(),
                It.IsAny<bool>(),
                It.IsAny<VpStatementVersionEnum>())).Returns(statement);

            FinancePlanStatement financePlanStatement = new FinancePlanStatement();
            financePlanStatement.PeriodEndDate = DateTime.UtcNow;
            financePlanStatement.PeriodStartDate = DateTime.UtcNow.AddDays(-21);
            financePlanStatement.StatementVersion = VpStatementVersionEnum.Vp3V1;

            builder.FinancePlanStatementServiceMock.Setup(x => x.CreateFinancePlanStatement(It.IsAny<Guarantor>(),
                It.IsAny<DateTime>(),
                It.IsAny<FinancePlanStatement>(),
                It.IsAny<bool>(),
                It.IsAny<bool>(),
                It.IsAny<VpStatementVersionEnum>())).Returns(financePlanStatement);

            builder.VisitServiceMock.Setup(x => x.GetVisits(It.IsAny<int>())).Returns((IReadOnlyList<Visit>)allVisits);

            FinancePlan financePlan = new FinancePlanBuilder()
                .WithDefaultValues()
                .AddFinancePlanStatusHistory(DateTime.UtcNow.AddDays(-60), FinancePlanStatusEnum.GoodStanding);

            builder.FinancePlanServiceMock.Setup(x => x.AreVisitsOnFinancePlan_FinancePlanVisit(It.IsAny<IList<int>>(),
                    It.IsAny<DateTime?>(),
                    It.IsAny<List<FinancePlanStatusEnum>>()))
                .Returns(financedVisits.Select(x => new FinancePlanVisit
                {
                    CurrentVisitBalance = x.CurrentBalance,
                    BillingApplication = x.BillingApplication,
                    VpGuarantorId = x.VpGuarantorId,
                    BillingSystemId = x.BillingSystemId ?? default(int),
                    CurrentVisitState = x.CurrentVisitState,
                    VisitId = x.VisitId,
                    FinancePlan = financePlan
                }).ToList());

            builder.FinancePlanServiceMock
                .Setup(x => x.GetFinancePlanActiveVisitBalancesAsOf(It.IsAny<FinancePlan>(), It.IsAny<DateTime>()))
                .Returns<FinancePlan,DateTime>((fp, asOfDate) => new Dictionary<int, decimal>() {});

            builder.FinancePlanServiceMock.Setup(x => x.GetInterestAssessedForVisits(It.IsAny<IList<int>>())).Returns(new List<FinancePlanVisitInterestDueResult>());

            return builder.CreateService();
        }

        //TODO: unit tests for statement notification tiers
    }
}

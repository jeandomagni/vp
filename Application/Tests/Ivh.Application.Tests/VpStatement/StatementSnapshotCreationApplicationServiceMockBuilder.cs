﻿namespace Ivh.Application.Tests.VpStatement
{
    using System;
    using Application.FinanceManagement.ApplicationServices;
    using Common.Data.Connection;
    using Common.Data.Connection.Connections;
    using Common.Tests.Helpers;
    using Common.Tests.Helpers.Base;
    using Domain.FinanceManagement.FinancePlan.Interfaces;
    using Domain.FinanceManagement.Statement.Interfaces;
    using Domain.Guarantor.Interfaces;
    using Domain.Visit.Interfaces;
    using Moq;
    using NHibernate;

    public class StatementSnapshotCreationVp3V1ApplicationServiceMockBuilder : IMockBuilder<IStatementSnapshotCreationApplicationService, StatementSnapshotCreationVp3V1ApplicationServiceMockBuilder>
    {
        public ApplicationServiceCommonServiceMockBuilder ApplicationServiceCommonServiceMockBuilder;
        public IGuarantorService GuarantorService;
        public IStatementDateService StatementDateService;
        public IVpStatementService VpStatementService;
        public IVisitTransactionService VisitTransactionService;
        public IVisitService VisitService;
        public IFinancePlanStatementService FinancePlanStatementService;
        public IFinancePlanService FinancePlanService;
        public ISession Session;

        public Mock<IGuarantorService> GuarantorServiceMock;
        public Mock<IStatementDateService> StatementDateServiceMock;
        public Mock<IVpStatementService> VpStatementServiceMock;
        public Mock<IVisitTransactionService> VisitTransactionServiceMock;
        public Mock<IVisitService> VisitServiceMock;
        public Mock<IFinancePlanStatementService> FinancePlanStatementServiceMock;
        public Mock<IFinancePlanService> FinancePlanServiceMock;
        public Mock<ISession> SessionMock;

        public StatementSnapshotCreationVp3V1ApplicationServiceMockBuilder()
        {
            this.ApplicationServiceCommonServiceMockBuilder = new ApplicationServiceCommonServiceMockBuilder();
            this.GuarantorServiceMock = new Mock<IGuarantorService>();
            this.StatementDateServiceMock = new Mock<IStatementDateService>();
            this.VpStatementServiceMock = new Mock<IVpStatementService>();
            this.VisitTransactionServiceMock = new Mock<IVisitTransactionService>();
            this.VisitServiceMock = new Mock<IVisitService>();
            this.FinancePlanStatementServiceMock = new Mock<IFinancePlanStatementService>();
            this.FinancePlanServiceMock = new Mock<IFinancePlanService>();
            this.SessionMock = new Mock<ISession>();
        }

        public IStatementSnapshotCreationApplicationService CreateService()
        {
            return new StatementSnapshotCreationVp3V1ApplicationService(
                this.ApplicationServiceCommonServiceMockBuilder.CreateServiceLazy(),
                new Lazy<IGuarantorService>(() => this.GuarantorService ?? this.GuarantorServiceMock.Object),
                new Lazy<IStatementDateService>(() => this.StatementDateService ?? this.StatementDateServiceMock.Object),
                new Lazy<IVpStatementService>(() => this.VpStatementService ?? this.VpStatementServiceMock.Object),
                new Lazy<IVisitTransactionService>(() => this.VisitTransactionService ?? this.VisitTransactionServiceMock.Object),
                new Lazy<IVisitService>(() => this.VisitService ?? this.VisitServiceMock.Object),
                new Lazy<IFinancePlanStatementService>(() => this.FinancePlanStatementService ?? this.FinancePlanStatementServiceMock.Object),
                new Lazy<IFinancePlanService>(() => this.FinancePlanService ?? this.FinancePlanServiceMock.Object),
                new SessionContext<VisitPay>(this.Session ?? this.SessionMock.Object)
            );
        }

        public StatementSnapshotCreationVp3V1ApplicationServiceMockBuilder Setup(Action<StatementSnapshotCreationVp3V1ApplicationServiceMockBuilder> configAction)
        {
            configAction(this);
            return this;
        }
    }
}
﻿namespace Ivh.Common.Tests.Helpers.VpStatement
{
    using System;
    using System.Data;
    using Application.Core.Common.Interfaces;
    using Ivh.Application.Base.Common.Interfaces;
    using NHibernate;
    using Ivh.Domain.FinanceManagement.Statement.Interfaces;
    using Ivh.Domain.Guarantor.Interfaces;
    using Ivh.Domain.FinanceManagement.FinancePlan.Interfaces;
    using Ivh.Application.FinanceManagement.ApplicationServices;
    using Ivh.Application.FinanceManagement.Common.Interfaces;
    using Base;
    using Data.Connection;
    using Data.Connection.Connections;
    using Moq;

    public class StatementCreationApplicationServiceMockBuilder : IMockBuilder<IStatementCreationApplicationService, StatementCreationApplicationServiceMockBuilder>
    {
        public ApplicationServiceCommonServiceMockBuilder ApplicationServiceCommonServiceMockBuilder;
        public IVisitBalanceBaseApplicationService VisitBalanceBaseApplicationService;
        public ISession Session;
        public IVpStatementService VpStatementService;
        public IGuarantorService GuarantorService;
        public IFinancePlanService FinancePlanService;
        public IStatementDateService StatementDateService;
        public IStatementSnapshotCreationApplicationService StatementSnapshotCreationApplicationService;
        public IFinancePlanApplicationService FinancePlanApplicationService;
        public IFinancePlanStatementService FinancePlanStatementService;
        public IVisitPayUserApplicationService VisitPayUserApplicationService;

        public Mock<IVisitBalanceBaseApplicationService> VisitBalanceBaseApplicationServiceMock;
        public Mock<ISession> SessionMock;
        public Mock<IVpStatementService> VpStatementServiceMock;
        public Mock<IGuarantorService> GuarantorServiceMock;
        public Mock<IFinancePlanService> FinancePlanServiceMock;
        public Mock<IStatementDateService> StatementDateServiceMock;
        public Mock<IStatementSnapshotCreationApplicationService> StatementSnapshotCreationApplicationServiceMock;
        public Mock<IFinancePlanApplicationService> FinancePlanApplicationServiceMock;
        public Mock<IFinancePlanStatementService> FinancePlanStatementServiceMock;
        public Mock<IVisitPayUserApplicationService> VisitPayUserApplicationServiceMock;


        public StatementCreationApplicationServiceMockBuilder()
        {
            this.ApplicationServiceCommonServiceMockBuilder = new ApplicationServiceCommonServiceMockBuilder();
            this.VisitBalanceBaseApplicationServiceMock = new Mock<IVisitBalanceBaseApplicationService>();
            this.SessionMock = new SessionMockBuilder().SessionMock;
            this.VpStatementServiceMock = new Mock<IVpStatementService>();
            this.GuarantorServiceMock = new Mock<IGuarantorService>();
            this.FinancePlanServiceMock = new Mock<IFinancePlanService>();
            this.StatementDateServiceMock = new Mock<IStatementDateService>();
            //this.StatementDateService = new StatementDateServiceMockBuilder().Setup(x => { }).CreateService();
            this.StatementSnapshotCreationApplicationServiceMock = new Mock<IStatementSnapshotCreationApplicationService>();
            this.FinancePlanApplicationServiceMock = new Mock<IFinancePlanApplicationService>();
            this.FinancePlanStatementServiceMock = new Mock<IFinancePlanStatementService>();
            this.VisitPayUserApplicationServiceMock = new Mock<IVisitPayUserApplicationService>();
        }

        public IStatementCreationApplicationService CreateService()
        {
            return new StatementCreationApplicationService(
                this.ApplicationServiceCommonServiceMockBuilder.CreateServiceLazy(),
                new Lazy<IVisitBalanceBaseApplicationService>(() => this.VisitBalanceBaseApplicationService ?? this.VisitBalanceBaseApplicationServiceMock.Object),
                new SessionContext<VisitPay>(this.Session ?? this.SessionMock.Object),
                new Lazy<IVpStatementService>(() => this.VpStatementService ?? this.VpStatementServiceMock.Object),
                new Lazy<IGuarantorService>(() => this.GuarantorService ?? this.GuarantorServiceMock.Object),
                new Lazy<IFinancePlanService>(() => this.FinancePlanService ?? this.FinancePlanServiceMock.Object),
                new Lazy<IStatementDateService>(() => this.StatementDateService ?? this.StatementDateServiceMock.Object),
                new Lazy<IStatementSnapshotCreationApplicationService>(() => this.StatementSnapshotCreationApplicationService ?? this.StatementSnapshotCreationApplicationServiceMock.Object),
                new Lazy<IFinancePlanApplicationService>(() => this.FinancePlanApplicationService ?? this.FinancePlanApplicationServiceMock.Object),
                new Lazy<IFinancePlanStatementService>(() => this.FinancePlanStatementService ?? this.FinancePlanStatementServiceMock.Object),
                new Lazy<IVisitPayUserApplicationService>(() => this.VisitPayUserApplicationService ?? this.VisitPayUserApplicationServiceMock.Object)
            );
        }

        public StatementCreationApplicationServiceMockBuilder Setup(Action<StatementCreationApplicationServiceMockBuilder> configAction)
        {
            configAction(this);
            return this;
        }
    }
}
﻿namespace Ivh.Application.Tests
{
    using System;
    using System.Collections.Generic;
    using System.Reflection;
    using Core.Utilities;
    using FluentNHibernate.Cfg;
    using FluentNHibernate.Cfg.Db;
    using FluentNHibernate.Conventions.Helpers;
    using NHibernate;
    using NHibernate.Cfg;
    using NHibernate.Tool.hbm2ddl;
    using NUnit.Framework;

    //todo: this probably belongs in Provider, but it's nice to have for integration tests
    public abstract class RepositoryTestBase : ApplicationTestBase
    {
        protected ISessionFactory SessionFactory;
        protected ISession Session;
        protected IStatelessSession StatelessSession;
        private ITransaction _transaction;
        private ITransaction _statelessTransaction;
        private readonly List<Type> _types;
        private readonly Action<MappingConfiguration> _customMappingAction;

        protected RepositoryTestBase(Type typeFromTargetAssembly, params Type[] typesFromTargetAssemblies)
        {
            List<Type> types = new List<Type> { typeFromTargetAssembly };
            types.AddRange(typesFromTargetAssemblies);
            this._types = types;
        }

        protected RepositoryTestBase(Action<MappingConfiguration> customMappingAction, Type typeFromTargetAssembly, params Type[] typesFromTargetAssemblies)
        {
            List<Type> types = new List<Type> ();
            if (typeFromTargetAssembly != null)
            {
               types.Add(typeFromTargetAssembly); 
            }
            types.AddRange(typesFromTargetAssemblies);
            this._types = types;
            this._customMappingAction = customMappingAction;
        }

        [SetUp]
        public void Setup()
        {
            this.BuildConfigurationWithSqlLite();
            this._transaction = this.Session.BeginTransaction();
            this._statelessTransaction = this.StatelessSession.BeginTransaction();
            
            this._transaction.Begin();
            this._statelessTransaction.Begin();
        }

        /*
        [TearDown]
        public void RollbackTransaction()
        {
            // probably only need to do this if we start using a database that survives connection closure
            this._transaction.Rollback();
            this._transaction.Dispose();

            this._statelessTransaction.Rollback();
            this._statelessTransaction.Dispose();
        }*/

        protected void BuildConfigurationWithSql()
        {
            Configuration configuration = Fluently.Configure()
                .Database(MsSqlConfiguration.MsSql2008.ConnectionString(@"Data Source=localhost\MSSQLDEV;Initial Catalog=Dev_VP_app;Integrated Security=true;").ShowSql())
                .Mappings(m =>
                {
                    this._customMappingAction?.Invoke(m);

                    m.FluentMappings.Conventions.Setup(a => a.Add(AutoImport.Never()));

                    foreach (Type type in this._types)
                    {
                        m.FluentMappings.AddFromAssembly(Assembly.GetAssembly(type));
                    }

                })
                .BuildConfiguration();

            this.SessionFactory = configuration.BuildSessionFactory();
            this.Session = this.SessionFactory.OpenSession();
            this.StatelessSession = this.SessionFactory.OpenStatelessSession();
        }

        protected void BuildConfigurationWithSqlLite()
        {
            Configuration configuration = Fluently.Configure()
                .Database(SQLiteConfiguration.Standard.InMemory().ShowSql())
                .Mappings(m =>
                {
                    this._customMappingAction?.Invoke(m);

                    m.FluentMappings.Conventions.Setup(a => a.Add(AutoImport.Never()));

                    SqlLiteDateTime2UserTypeConvention dateTimeConvention = new SqlLiteDateTime2UserTypeConvention();
                    m.FluentMappings.Conventions.Add(dateTimeConvention);

                    foreach (Type type in this._types)
                    {
                        m.FluentMappings.AddFromAssembly(Assembly.GetAssembly(type));
                    }

                })
                .BuildConfiguration();

            this.SessionFactory = configuration.BuildSessionFactory();
            this.Session = this.SessionFactory.OpenSession();

            // in memory db exists for the lifetime of the connection, so we're sharing that between sessions
            this.StatelessSession = this.SessionFactory.OpenStatelessSession(this.Session.Connection);

            SchemaExport exporter = new SchemaExport(configuration);
            exporter.Execute(false, true, false, this.Session.Connection, null);
        }
    }
}

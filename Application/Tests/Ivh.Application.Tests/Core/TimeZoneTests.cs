﻿namespace Ivh.Application.Tests.Core
{
    using System;
    using System.Linq;
    using Common.Base.Utilities.Helpers;
    using NUnit.Framework;

    [TestFixture]
    public class TimeZoneTests : ApplicationTestBase
    {
        [Test]
        public void TestTimeZones()
        {
            // eastern time
            TimeZoneHelper timeZoneHelper = new TimeZoneHelper(TimeZoneHelper.TimeZones.EasternStandardTime, TimeZoneHelper.TimeZones.Operations);
            DateTime et1 = timeZoneHelper.ClientToOperations(new DateTime(2015, 12, 1, 12, 0, 0));
            DateTime et2 = timeZoneHelper.ClientToOperations(new DateTime(2015, 06, 1, 12, 0, 0)); // dst

            Assert.AreEqual(et1.Hour, et2.Hour);
            Assert.AreEqual(et1.Hour, 10);
            Assert.AreEqual(et2.Hour, 10);

            // central time
            timeZoneHelper = new TimeZoneHelper(TimeZoneHelper.TimeZones.CentralStandardTime, TimeZoneHelper.TimeZones.Operations);
            DateTime ct1 = timeZoneHelper.ClientToOperations(new DateTime(2015, 12, 1, 12, 0, 0));
            DateTime ct2 = timeZoneHelper.ClientToOperations(new DateTime(2015, 06, 1, 12, 0, 0)); // dst

            Assert.AreEqual(ct1.Hour, ct2.Hour);
            Assert.AreEqual(ct1.Hour, 11);
            Assert.AreEqual(ct2.Hour, 11);

            // mountain time
            timeZoneHelper = new TimeZoneHelper(TimeZoneHelper.TimeZones.MountainStandardTime, TimeZoneHelper.TimeZones.Operations);
            DateTime mt1 = timeZoneHelper.ClientToOperations(new DateTime(2015, 12, 1, 12, 0, 0));
            DateTime mt2 = timeZoneHelper.ClientToOperations(new DateTime(2015, 06, 1, 12, 0, 0)); // dst

            Assert.AreEqual(mt1.Hour, mt2.Hour);
            Assert.AreEqual(mt1.Hour, 12);
            Assert.AreEqual(mt2.Hour, 12);

            // pacific time
            timeZoneHelper = new TimeZoneHelper(TimeZoneHelper.TimeZones.PacificStandardTime, TimeZoneHelper.TimeZones.Operations);
            DateTime pt1 = timeZoneHelper.ClientToOperations(new DateTime(2015, 12, 1, 12, 0, 0));
            DateTime pt2 = timeZoneHelper.ClientToOperations(new DateTime(2015, 06, 1, 12, 0, 0)); // dst

            Assert.AreEqual(pt1.Hour, pt2.Hour);
            Assert.AreEqual(pt1.Hour, 13);
            Assert.AreEqual(pt2.Hour, 13);

            // string
            timeZoneHelper = new TimeZoneHelper(TimeZoneHelper.TimeZones.MountainStandardTime, TimeZoneHelper.TimeZones.Operations);
            string str1 = timeZoneHelper.Client.ToLocalDateTimeString(et1);
            string str2 = timeZoneHelper.Client.ToLocalDateTimeString(et2);

            Assert.AreEqual(string.Join("", str1.Skip(str1.Length - 3).ToList()), "MST");
            Assert.AreEqual(string.Join("", str2.Skip(str2.Length - 3).ToList()), "MDT");
        }
    }
}
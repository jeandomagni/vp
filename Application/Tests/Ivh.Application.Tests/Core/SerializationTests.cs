﻿namespace Ivh.Application.Tests.Core
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using Application.Core.Common.Dtos;
    using Application.HospitalData.Common.Models;
    using Common.Base.Enums;
    using Common.VisitPay.Enums;
    using Domain.Content.Entities;
    using NUnit.Framework;
    using ProtoBuf;

    /// <summary>
    /// The purpose of this test is to make sure that any objects getting sent to cache 
    /// can be serialized (specifically with ProtoBuf). 
    /// </summary>

    [TestFixture]
    public class SerializationTests : ApplicationTestBase
    {
        [Test]
        public void CmsVersion_can_serialize_and_deserialize_with_protobuf()
        {
            #region MyRegion
            CmsVersion cmsVersion = new CmsVersion
            {
                CmsVersionId = 1,
                CmsRegionId = 3,
                VersionNum = 3,
                ContentBody = "this is the body",
                ContentTitle = "this is the title",
                UpdatedDate = DateTime.UtcNow,
                UpdatedByUserId = 1,
                ExpiryDate = null,//DateTime.UtcNow.AddDays(1),
                CmsRegion = new CmsRegion
                {
                    CmsRegionId = 1,
                    CmsRegionName = "RegionName",
                    CmsType = CmsTypeEnum.EmailContent
                }
            };
            #endregion

            CmsVersion serializedDeserializedObj = this.SerializeDeserializeObjectWithProtobuf<CmsVersion>(cmsVersion);
            Assert.AreEqual(cmsVersion.CmsVersionId, serializedDeserializedObj.CmsVersionId);
            Assert.AreEqual(cmsVersion.ContentBody, serializedDeserializedObj.ContentBody);
            Assert.AreEqual(cmsVersion.UpdatedDate, serializedDeserializedObj.UpdatedDate);
            Assert.AreEqual(cmsVersion.CmsRegion.CmsRegionName, serializedDeserializedObj.CmsRegion.CmsRegionName);

            bool referenceEquals = object.ReferenceEquals(cmsVersion, serializedDeserializedObj);
            Assert.False(referenceEquals);

        }

        [Test]
        public void VisitItemizationDetailsResultModel_can_serialize_and_deserialize_with_protobuf()
        {
            VisitItemizationDetailsResultModel visitItemizationDetailsResultModel = new VisitItemizationDetailsResultModel
            {
                BillingSystemId = 1,
                VpGuarantorId = 1,
                HsGuarantorId =1,
                VisitSourceSystemKeyDisplay = "display",
                PatientFirstName = "",
                PatientLastName = "",
                VisitAdmitDate = DateTime.UtcNow,
                VisitDischargeDate = DateTime.UtcNow,
                IsPatientGuarantor = true,
                IsPatientMinor = false
            };

            VisitItemizationDetailsResultModel serializedDeserializedObj = this.SerializeDeserializeObjectWithProtobuf<VisitItemizationDetailsResultModel>(visitItemizationDetailsResultModel);
            // Spot check properties of varying types
            Assert.AreEqual(visitItemizationDetailsResultModel.BillingSystemId, serializedDeserializedObj.BillingSystemId);
            Assert.AreEqual(visitItemizationDetailsResultModel.VisitSourceSystemKeyDisplay, serializedDeserializedObj.VisitSourceSystemKeyDisplay);
            Assert.AreEqual(visitItemizationDetailsResultModel.VisitAdmitDate, serializedDeserializedObj.VisitAdmitDate);
            Assert.True(serializedDeserializedObj.IsPatientGuarantor);
            Assert.False(serializedDeserializedObj.IsPatientMinor);

            bool referenceEquals = object.ReferenceEquals(visitItemizationDetailsResultModel,serializedDeserializedObj);
            Assert.False(referenceEquals);
        }

        [Test]
        public void HealthEquityBalanceDto_can_serialize_and_deserialize_with_protobuf()
        {
            HealthEquityBalanceDto healthEquityBalanceDto = new HealthEquityBalanceDto
            {
                CashBalance = (decimal) 100.11,
                InvestmentBalance = 0,
                LastUpdated = DateTime.UtcNow
            };

            HealthEquityBalanceDto serializedDeserializedObj = this.SerializeDeserializeObjectWithProtobuf<HealthEquityBalanceDto>(healthEquityBalanceDto);
            Assert.AreEqual(healthEquityBalanceDto.CashBalance, serializedDeserializedObj.CashBalance);
            Assert.AreEqual(healthEquityBalanceDto.LastUpdated, serializedDeserializedObj.LastUpdated);

            bool referenceEquals = object.ReferenceEquals(healthEquityBalanceDto, serializedDeserializedObj);
            Assert.False(referenceEquals);
        }

        [Test]
        public void HealthEquitySsoStatusDto_can_serialize_and_deserialize_with_protobuf()
        {
            HealthEquitySsoStatusDto healthEquitySsoStatusDto = new HealthEquitySsoStatusDto
            {
                IsHqySsoUp = true,
                LastUpdated = DateTime.UtcNow
            };

            HealthEquitySsoStatusDto serializedDeserializedObj = this.SerializeDeserializeObjectWithProtobuf<HealthEquitySsoStatusDto>(healthEquitySsoStatusDto);
            Assert.True(serializedDeserializedObj.IsHqySsoUp);
            Assert.AreEqual(healthEquitySsoStatusDto.LastUpdated, serializedDeserializedObj.LastUpdated);

            bool referenceEquals = object.ReferenceEquals(healthEquitySsoStatusDto, serializedDeserializedObj);
            Assert.False(referenceEquals);
        }

        [Test]
        public void SsoResponseDto_can_serialize_and_deserialize_with_protobuf()
        {
            SsoResponseDto ssoResponseDto = new SsoResponseDto
            {
                AcceptedTheSsoAgreement = true,
                LastName = "Smith",
                SignInStatus = SignInStatusExEnum.Success,
                MatchesThatCouldBeRegistered = new List<SsoPossibleHsGuarantorResult> { new SsoPossibleHsGuarantorResult { HsGuarantorHsBillingSystemID = 1,HsGuarantorId = 2,HsGuarantorSourceSystemKey = "key"} }

            };

            SsoResponseDto serializedDeserializedObj = this.SerializeDeserializeObjectWithProtobuf<SsoResponseDto>(ssoResponseDto);
            Assert.True(serializedDeserializedObj.AcceptedTheSsoAgreement);
            Assert.AreEqual(serializedDeserializedObj.LastName, ssoResponseDto.LastName);
            Assert.AreEqual(serializedDeserializedObj.SignInStatus, ssoResponseDto.SignInStatus);
            SsoPossibleHsGuarantorResult matchesThatCouldBeRegister = serializedDeserializedObj.MatchesThatCouldBeRegistered.First();
            Assert.AreEqual(matchesThatCouldBeRegister.HsGuarantorSourceSystemKey,"key");

            bool referenceEquals = object.ReferenceEquals(ssoResponseDto, serializedDeserializedObj);
            Assert.False(referenceEquals);
        }

        [Test]
        public void ContentMenu_can_serialize_and_deserialize_with_protobuf()
        {
            ContentMenu contentMenu = new ContentMenu
            {
                ContentMenuId = 1,
                ContentMenuName = "foo"
            };

            //graph 
            List<ContentMenuItem> contentMenuItems = new List<ContentMenuItem>
            {
                new ContentMenuItem { ContentMenuItemId = 2,ContentMenu = contentMenu},
                new ContentMenuItem { ContentMenuItemId = 3,ContentMenu = contentMenu}

            };
            contentMenu.ContentMenuItems = contentMenuItems;

            ContentMenu serializedDeserializedObj = this.SerializeDeserializeObjectWithProtobuf<ContentMenu>(contentMenu);
            Assert.AreEqual(serializedDeserializedObj.ContentMenuId, contentMenu.ContentMenuId);
            ContentMenuItem contentMenuItemSerialized = serializedDeserializedObj.ContentMenuItems.First();
            Assert.AreEqual(2, contentMenuItemSerialized.ContentMenuItemId);

            bool referenceEquals = object.ReferenceEquals(contentMenu, serializedDeserializedObj);
            Assert.False(referenceEquals);
        }

        [Test]
        public void Dictionary_can_serialize_and_deserialize_with_protobuf()
        {
            Dictionary<int, int> dictionary = new Dictionary<int, int> {[1]=1,[2]=2};
            Dictionary<int, int> serializedDeserializedObj = this.SerializeDeserializeObjectWithProtobuf<Dictionary<int, int>>(dictionary);

            Assert.AreEqual(1,serializedDeserializedObj[1]);
            Assert.AreEqual(2, serializedDeserializedObj[2]);

            bool referenceEquals = object.ReferenceEquals(dictionary, serializedDeserializedObj);
            Assert.False(referenceEquals);
        }

        private static string SerializeWithProtobuf(object obj)
        {

            try
            {
                using (MemoryStream myMemoryStream = new MemoryStream())
                {
                    //Did the code throw here?
                    //If so:  Add '[DataContract]' to the class
                    //and '[DataMember(Order = 1)]' to the members that should be serialized.
                    //It's important to increment the order number.
                    Serializer.Serialize(myMemoryStream, obj);
                    byte[] byteData = myMemoryStream.ToArray();
                    return Convert.ToBase64String(byteData);
                }
            }
            catch (Exception)
            {
                throw;
            }

        }

        private T SerializeDeserializeObjectWithProtobuf<T>(object obj )
        {
            string serializedObject = SerializeWithProtobuf(obj);
            T deserializedObject = Serializer.Deserialize<T>(new MemoryStream(Convert.FromBase64String(serializedObject)));

            return deserializedObject;
        }
    }
}

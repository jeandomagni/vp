﻿namespace Ivh.Application.Tests.Core
{
    using System;
    using System.Collections.Generic;
    using Application.Core.ApplicationServices;
    using Application.Core.Common.Dtos;
    using Application.User.Common.Dtos;
    using Common.Base.Utilities.Extensions;
    using Common.Tests.Helpers.Visit;
    using Common.VisitPay.Constants;
    using Common.VisitPay.Enums;
    using Domain.Visit.Entities;
    using Domain.Visit.Interfaces;
    using Moq;
    using NUnit.Framework;

    [TestFixture]
    public class VisitApplicationServiceTests : ApplicationTestBase
    {
        [TestCase(false)]
        [TestCase(true)]
        public void GetDerivedVisitState_MaxAge_Inactive(bool isFinanced)
        {
            VisitApplicationService svc = (VisitApplicationService)new VisitApplicationServiceMockBuilder().CreateService();
            VisitStateDerivedEnum state = svc.GetDerivedState(new VisitDto
            {
                VisitId = 1,
                IsMaxAge = true,
                IsMaxAgeInactive = true,
                CurrentVisitState = VisitStateEnum.Inactive
            }, isFinanced ? 1.ToListOfOne() : 0.ToListOfOne());

            if (isFinanced)
            {
                Assert.AreNotEqual(VisitStateDerivedEnum.MaxAgeClosed, state);
            }
            else
            {
                Assert.AreEqual(VisitStateDerivedEnum.MaxAgeClosed, state);
            }
        }

        [TestCase(false)]
        [TestCase(true)]
        public void GetDerivedVisitState_MaxAge_Active(bool isFinanced)
        {
            VisitApplicationService svc = (VisitApplicationService)new VisitApplicationServiceMockBuilder().CreateService();
            VisitStateDerivedEnum state = svc.GetDerivedState(new VisitDto
            {
                VisitId = 1,
                IsMaxAge = true,
                IsMaxAgeActive = true,
                CurrentVisitState = VisitStateEnum.Active
            }, isFinanced ? 1.ToListOfOne() : 0.ToListOfOne());

            if (isFinanced)
            {
                Assert.AreNotEqual(VisitStateDerivedEnum.MaxAge, state);
            }
            else
            {
                Assert.AreEqual(VisitStateDerivedEnum.MaxAge, state);
            }
        }

        [TestCase(VisitStateEnum.Inactive)]
        [TestCase(VisitStateEnum.Active)]
        public void GetDerivedVisitState_Unmatched(VisitStateEnum visitStateEnum)
        {
            VisitApplicationService svc = (VisitApplicationService)new VisitApplicationServiceMockBuilder().CreateService();
            VisitStateDerivedEnum state = svc.GetDerivedState(new VisitDto
            {
                // unmatched wins over everything
                IsUnmatched = true,
                IsMaxAge = true,
                BillingHold = true,
                CurrentVisitState = visitStateEnum,
                VisitId = 1
            }, 1.ToListOfOne());

            Assert.AreEqual(VisitStateDerivedEnum.Unmatched, state);
        }

        [TestCase(VisitStateEnum.Inactive)]
        [TestCase(VisitStateEnum.Active)]
        public void GetDerivedVisitState_BillingHold(VisitStateEnum visitStateEnum)
        {
            VisitApplicationService svc = (VisitApplicationService)new VisitApplicationServiceMockBuilder().CreateService();
            VisitStateDerivedEnum state = svc.GetDerivedState(new VisitDto
            {
                BillingHold = true,
                CurrentVisitState = visitStateEnum
            }, new List<int>());

            Assert.AreEqual(VisitStateDerivedEnum.OnHold, state);
        }

        [TestCase(VisitStateEnum.Inactive)]
        [TestCase(VisitStateEnum.NotSet)]
        public void GetDerivedVisitState_Inactive(VisitStateEnum visitStateEnum)
        {
            VisitApplicationService svc = (VisitApplicationService)new VisitApplicationServiceMockBuilder().CreateService();
            VisitStateDerivedEnum state = svc.GetDerivedState(new VisitDto
            {
                CurrentVisitState = visitStateEnum
            }, new List<int>());

            Assert.AreEqual(VisitStateDerivedEnum.Inactive, state);
        }

        [Test]
        public void GetDerivedVisitState_OnFinancePlan_Inactive()
        {
            VisitApplicationService svc = (VisitApplicationService)new VisitApplicationServiceMockBuilder().CreateService();
            VisitStateDerivedEnum state = svc.GetDerivedState(new VisitDto
            {
                CurrentVisitState = VisitStateEnum.Inactive,
                VisitId = 1
            }, 1.ToListOfOne());

            Assert.AreEqual(VisitStateDerivedEnum.Inactive, state);
        }

        [Test]
        public void GetDerivedVisitState_OnFinancePlan_Active()
        {
            VisitApplicationService svc = (VisitApplicationService)new VisitApplicationServiceMockBuilder().CreateService();
            VisitStateDerivedEnum state = svc.GetDerivedState(new VisitDto
            {
                CurrentVisitState = VisitStateEnum.Active,
                VisitId = 1
            }, 1.ToListOfOne());

            Assert.AreEqual(VisitStateDerivedEnum.OnFinancePlan, state);
        }

        [Test]
        public void GetDerivedVisitState_Active()
        {
            VisitApplicationService svc = (VisitApplicationService)new VisitApplicationServiceMockBuilder().CreateService();
            VisitStateDerivedEnum state = svc.GetDerivedState(new VisitDto
            {
                CurrentVisitState = VisitStateEnum.Active,
                VisitId = 2
            }, 1.ToListOfOne());

            Assert.AreEqual(VisitStateDerivedEnum.Active, state);
        }

        [Test]
        public void GetVisit_VisitHasAssociatedFacility_FacilityDataIsAccessible()
        {
            Facility fakeFacility = new Facility
            {
                FacilityDescription = "Medical Center",
                FacilityId = 12345,
                LogoFilename = "sample.png",
				RicState = new State()
				{
					StateCode = "CO",
					CmsRegionId = 2
				}
                
            };

            Mock<IVisitService> visitSvcMock = new Mock<IVisitService>();

            visitSvcMock
                .Setup(s => s.GetVisitBySystemSourceKey(It.IsAny<string>(), It.IsAny<int>()))
                .Returns(new Visit { Facility = fakeFacility });

            VisitApplicationService svc =
                (VisitApplicationService)new VisitApplicationServiceMockBuilder()
                    .Setup(builder => { builder.VisitService = visitSvcMock.Object; })
                    .CreateService();

            VisitDto visit = svc.GetVisit(1, "fakeKey");

            Assert.AreEqual(fakeFacility.FacilityId, visit.Facility.FacilityId);
            Assert.AreEqual(fakeFacility.FacilityDescription, visit.Facility.FacilityDescription);
            Assert.AreEqual(fakeFacility.LogoFilename, visit.Facility.LogoFilename);
            Assert.AreEqual(fakeFacility.RicState.StateCode, visit.Facility.RicState.StateCode);
            Assert.AreEqual(fakeFacility.RicState.CmsRegionId, visit.Facility.RicState.CmsRegionId);
		}

        [Ignore("Revisit when VP3 state machine is defined")]
        [TestCase(5, false)]
        [TestCase(5, true)]
        [TestCase(4, false)]
        [TestCase(4, true)]
        [TestCase(3, false)]
        [TestCase(3, true)]
        [TestCase(2, false)]
        [TestCase(2, true)]
        [TestCase(1, false)]
        [TestCase(1, true)]
        public void VisitRestatusesWhenAgingResetsToZero(int agingCount, bool isGracePeriod)
        {
            Visit visit = this.ResetAgingCount(agingCount, 0, isGracePeriod);
            //Assert.AreEqual(VisitStateEnum.PendingStatement, visit.VisitState.VisitStateEnum);
        }

        [Ignore("Revisit when VP3 state machine is defined")]
        [TestCase(5, false)]
        [TestCase(5, true)]
        [TestCase(4, false)]
        [TestCase(4, true)]
        [TestCase(3, false)]
        [TestCase(3, true)]
        [TestCase(2, false)]
        [TestCase(2, true)]
        public void VisitRestatusesWhenAgingResetsToOne(int agingCount, bool isGracePeriod)
        {
            //VisitStateEnum expectedVisitStateEnum = isGracePeriod ? VisitStateEnum.StatementedGoodStanding : VisitStateEnum.PastDue;

            Visit visit = this.ResetAgingCount(agingCount, 1, isGracePeriod);
            //Assert.AreEqual(expectedVisitStateEnum, visit.VisitState.VisitStateEnum);
        }

        [Ignore("Revisit when VP3 state machine is defined")]
        [TestCase(5, false)]
        [TestCase(5, true)]
        [TestCase(4, false)]
        [TestCase(4, true)]
        [TestCase(3, false)]
        [TestCase(3, true)]
        public void VisitRestatusesWhenAgingResetsToTwo(int agingCount, bool isGracePeriod)
        {
            Visit visit = this.ResetAgingCount(agingCount, 2, isGracePeriod);
            //Assert.AreEqual(VisitStateEnum.PastDue, visit.VisitState.VisitStateEnum);
        }

        [Ignore("Revisit when VP3 state machine is defined")]
        [TestCase(5, false)]
        [TestCase(5, true)]
        [TestCase(4, false)]
        [TestCase(4, true)]
        public void VisitRestatusesWhenAgingResetsToThree(int agingCount, bool isGracePeriod)
        {
            Visit visit = this.ResetAgingCount(agingCount, 3, isGracePeriod);
            //Assert.AreEqual(VisitStateEnum.PastDue, visit.VisitState.VisitStateEnum);
        }

        [Ignore("Revisit when VP3 state machine is defined")]
        [TestCase(0, false)]
        [TestCase(0, true)]
        [TestCase(1, false)]
        [TestCase(1, true)]
        [TestCase(2, false)]
        [TestCase(2, true)]
        [TestCase(3, false)]
        [TestCase(3, true)]
        public void VisitRestatusesWhenAgingSetToUncollectable(int agingCount, bool isGracePeriod)
        {
            Visit visit = this.ResetAgingCount(agingCount, 4, isGracePeriod, !isGracePeriod);
            //Assert.AreEqual(VisitStateEnum.PendingUncollectable, visit.VisitState.VisitStateEnum);
        }

        [Obsolete(ObsoleteDescription.VisitStateMachineTest)]
        private Visit ResetAgingCount(int currentAgingCount, int newAgingCount, bool isGracePeriod, bool doubleCheck = true)
        {
            #region current status

            VisitStateEnum currentVisitStateEnum;
            if (currentAgingCount == 4 && !isGracePeriod)
            {
                //currentVisitStateEnum = VisitStateEnum.PendingUncollectable;
                currentVisitStateEnum = VisitStateEnum.Active;
            }
            else if (currentAgingCount > 1 || currentAgingCount == 1 && !isGracePeriod)
            {
                //currentVisitStateEnum = VisitStateEnum.PastDue;
                currentVisitStateEnum = VisitStateEnum.Active;
            }
            else if (currentAgingCount == 1)
            {
                currentVisitStateEnum = VisitStateEnum.Active;
            }
            else
            {
                currentVisitStateEnum = VisitStateEnum.Active;
            }

            #endregion

            #region create visit

            DateTime insertDate = DateTime.UtcNow.AddDays(-60);
            Visit visit = VisitFactory.GenerateActiveVisit(1000m, 2, currentVisitStateEnum, insertDate, "PB", null, currentAgingCount).Item1;
            foreach (VisitStateHistory status in visit.VisitStates)
            {
                status.DateTime = insertDate;
            }
            foreach (VisitAgingHistory aging in visit.VisitAgingHistories)
            {
                aging.InsertDate = insertDate;
            }

            #endregion

            Mock<IVisitRepository> visitRepositoryMock = new VisitRepositoryMockBuilder().CreateMock(b =>
            {
                b.Setup(x => x.GetStatementVisitStateModel(It.IsAny<Visit>())).Returns(() => new StatementVisitStatusModel
                {
                    IsInGracePeriod = isGracePeriod,
                    StatementPeriodEndDate = DateTime.UtcNow.AddDays(-1)
                });
                b.Setup(x => x.IsOnStatement(It.IsAny<Visit>())).Returns(true);
                b.Setup(x => x.GetVisit(It.IsAny<int>(), It.IsAny<int>())).Returns(visit);
            });

            IVisitStateService visitStateService = new VisitStateServiceMockBuilder().Setup(statusServiceBuilder =>
            {
                statusServiceBuilder.VisitRepositoryMock = visitRepositoryMock;
            }).CreateService();

            VisitPayUserDto visitPayUserDto = new VisitPayUserDto();

            visit.SetAgingTier((AgingTierEnum)newAgingCount, "test", visitPayUserDto.VisitPayUserId);

            if (doubleCheck)
            {
                visitStateService.EvaluateVisitState(visit);
            }

            return visit;
        }
    }
}
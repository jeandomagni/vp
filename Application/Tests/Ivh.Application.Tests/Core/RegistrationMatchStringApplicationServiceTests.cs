﻿namespace Ivh.Application.Tests.Core
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Application.Core.ApplicationServices;
    using Application.Core.Common.Dtos;
    using NUnit.Framework;

    [TestFixture]
    public class RegistrationMatchStringApplicationServiceTests : ApplicationTestBase
    {
        [Test]
        public void TransformRegistrationMatchString_adds_prefix_for_intermountain_implementation()
        {
            IntermountainRegistrationMatchStringApplicationService appservice = new IntermountainRegistrationMatchStringApplicationService();
            List<HsGuarantorDto> list = new List<HsGuarantorDto>();
            List<string> originalSourceSystemKeys = new List<string> {"test1", "test2", "test3"};
            foreach (string sourceSystemKey in originalSourceSystemKeys)
            {
                list.Add(new HsGuarantorDto {SourceSystemKey = sourceSystemKey});
            }

            IReadOnlyList<HsGuarantorDto> outList = appservice.TransformRegistrationMatchString(list);
            IEnumerable<string> sourceSystemKeys = outList.Select(x => x.SourceSystemKey);
            List<string> errorMessages = new List<string>();
            string prefix = "107_";

            foreach (string outSourceSystemKey in sourceSystemKeys)
            {
                if (!outSourceSystemKey.Contains(prefix))
                {
                    errorMessages.Add($"{outSourceSystemKey} does not contain expected prefix of {prefix}");
                }

                if (!originalSourceSystemKeys.Contains(outSourceSystemKey.Remove(0, prefix.Length)))
                {
                    errorMessages.Add($"{outSourceSystemKey} was not found in original list");
                }
            }

            Assert.True(errorMessages.Count == 0, string.Join(Environment.NewLine, errorMessages));
        }
    }
}
﻿namespace Ivh.Application.Tests.Core
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Application.Core.Common.Interfaces;
    using Domain.Visit.Entities;
    using Common.Tests.Helpers.ExternalLink;
    using Common.Tests.Helpers.Visit;
    using Common.VisitPay.Enums;
    using Moq;
    using NUnit.Framework;

    [TestFixture]
    public class ExternalLinkApplicationServiceTests : ApplicationTestBase
    {
        [Test]
        public void ActiveOrSuspendedVisits_AlwaysShow()
        {
            IList<Visit> visits = new List<Visit>();

            visits.Add(VisitFactory.GenerateActiveVisit(1000m, 2, VisitStateEnum.Active, DateTime.UtcNow.AddDays(-181)).Item1);
            visits.Add(VisitFactory.GenerateSuspendedVisit(1000m, 2, DateTime.UtcNow.AddDays(-1)).Item1);

            foreach (Visit visit in visits)
            {
                visit.VisitInsurancePlans.Add(new VisitInsurancePlan
                {
                    InsurancePlan = new InsurancePlan
                    {
                        EobExternalLink = new ExternalLink
                        {
                            PersistDays = 180,
                            Text = "text" + visits.IndexOf(visit),
                            Url = "url" + visits.IndexOf(visit),
                        }
                    },
                    Visit = visit
                });
            }
            
            IExternalLinkApplicationService service = new ExternalLinkApplicationServiceMockBuilder().Setup(builder =>
            {
                builder.InsuranceServiceMock.Setup(x => x.GetInsurancePlansForVisits(It.IsAny<IList<VisitStateEnum>>(), It.IsAny<int>())).Returns(visits.SelectMany(x => x.VisitInsurancePlans).ToList());
            }).CreateService();
            
            Assert.AreEqual(2, service.GetExternalLinksForActiveVisitInsurancePlans(1).Count);
        }

        [Test]
        public void ActiveOrSuspendedVisits_DoNotShowWithoutTextOrUrl()
        {
            IList<Visit> visits = new List<Visit>();

            visits.Add(VisitFactory.GenerateActiveVisit(1000m, 2, VisitStateEnum.Active, DateTime.UtcNow.AddDays(-181)).Item1);
            visits.Add(VisitFactory.GenerateSuspendedVisit(1000m, 2, DateTime.UtcNow.AddDays(-1)).Item1);
            
            foreach (Visit visit in visits)
            {
                visit.VisitInsurancePlans.Add(new VisitInsurancePlan
                {
                    InsurancePlan = new InsurancePlan
                    {
                        EobExternalLink = new ExternalLink
                        {
                            PersistDays = 180,
                            Text = "text" + visits.IndexOf(visit),
                            Url = "url" + visits.IndexOf(visit),
                        }
                    },
                    Visit = visit
                });
            }

            visits[0].VisitInsurancePlans[0].InsurancePlan.EobExternalLink.Text = "";
            visits[1].VisitInsurancePlans[0].InsurancePlan.EobExternalLink.Url = "";
            
            IExternalLinkApplicationService service = new ExternalLinkApplicationServiceMockBuilder().Setup(builder =>
            {
                builder.InsuranceServiceMock.Setup(x => x.GetInsurancePlansForVisits(It.IsAny<IList<VisitStateEnum>>(), It.IsAny<int>())).Returns(visits.SelectMany(x => x.VisitInsurancePlans).ToList());
            }).CreateService();
            
            Assert.AreEqual(0, service.GetExternalLinksForActiveVisitInsurancePlans(1).Count);
        }

        [Test]
        public void ActiveOrSuspendedVisits_DoNotShowDuplicates()
        {
            IList<Visit> visits = new List<Visit>();

            visits.Add(VisitFactory.GenerateActiveVisit(1000m, 2, VisitStateEnum.Active, DateTime.UtcNow.AddDays(-181)).Item1);
            visits.Add(VisitFactory.GenerateSuspendedVisit(1000m, 2, DateTime.UtcNow.AddDays(-1)).Item1);

            foreach (Visit visit in visits)
            {
                visit.VisitInsurancePlans.Add(new VisitInsurancePlan
                {
                    InsurancePlan = new InsurancePlan
                    {
                        EobExternalLink = new ExternalLink
                        {
                            PersistDays = 180,
                            Text = "text",
                            Url = "url"
                        }
                    },
                    Visit = visit
                });
            }
            
            IExternalLinkApplicationService service = new ExternalLinkApplicationServiceMockBuilder().Setup(builder =>
            {
                builder.InsuranceServiceMock.Setup(x => x.GetInsurancePlansForVisits(It.IsAny<IList<VisitStateEnum>>(), It.IsAny<int>())).Returns(visits.SelectMany(x => x.VisitInsurancePlans).ToList());
            }).CreateService();
            
            Assert.AreEqual(1, service.GetExternalLinksForActiveVisitInsurancePlans(1).Count);
        }
        
        [Test]
        public void ClosedVisits_ShowForXDays()
        {
            IList<Visit> visits = new List<Visit>();
            
            visits.Add(VisitFactory.GenerateActiveVisit(1000m, 2, insertDate: DateTime.UtcNow.Date.AddDays(-181)).Item1);
            visits.Add(VisitFactory.GenerateIneligibleVisit(1000m, 2, DateTime.UtcNow.Date.AddDays(-181)).Item1);
            
            foreach (Visit visit in visits)
            {
                visit.VisitInsurancePlans.Add(new VisitInsurancePlan
                {
                    InsurancePlan = new InsurancePlan
                    {
                        EobExternalLink = new ExternalLink
                        {
                            PersistDays = 180,
                            Text = "text" + visits.IndexOf(visit),
                            Url = "url" + visits.IndexOf(visit),
                        }
                    },
                    Visit = visit
                });
            }
            
            IExternalLinkApplicationService service = new ExternalLinkApplicationServiceMockBuilder().Setup(builder =>
            {
                builder.InsuranceServiceMock.Setup(x => x.GetInsurancePlansForVisits(It.IsAny<IList<VisitStateEnum>>(), It.IsAny<int>())).Returns(visits.SelectMany(x => x.VisitInsurancePlans).ToList());
            }).CreateService();
            
            Assert.AreEqual(1, service.GetExternalLinksForActiveVisitInsurancePlans(1).Count);
        }
    }
}
﻿namespace Ivh.Application.Tests.Core
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Application.Base.Common.Interfaces;
    using Application.Core.ApplicationServices;
    using Application.Core.Common.Dtos;
    using Application.Core.Common.Interfaces;
    using Application.FinanceManagement.Common.Dtos;
    using Application.FinanceManagement.Common.Interfaces;
    using AutoMapper;
    using Common.Base.Enums;
    using Common.Data.Connection;
    using Common.Data.Connection.Connections;
    using Common.Tests.Helpers.Base;
    using Common.Tests.Helpers.Survey;
    using Common.VisitPay.Enums;
    using Domain.AppIntelligence.Survey.Entities;
    using Domain.AppIntelligence.Survey.Interfaces;
    using Domain.AppIntelligence.Survey.Mappings;
    using Domain.AppIntelligence.Survey.Services;
    using Domain.Content.Entities;
    using Domain.Content.Interfaces;
    using Domain.Guarantor.Interfaces;
    using Domain.Settings.Interfaces;
    using FluentNHibernate.Cfg;
    using FluentNHibernate.Cfg.Db;
    using FluentNHibernate.Conventions.Helpers;
    using Moq;
    using NHibernate;
    using NHibernate.Cfg;
    using NHibernate.Tool.hbm2ddl;
    using NUnit.Framework;
    using Provider.AppIntelligence;
    using Utilities;

    [TestFixture]
    public class SurveyTests : ApplicationTestBase
    {
        [SetUp]
        public void Setup()
        {
            Mock<IFeatureService> featureService = new Mock<IFeatureService>();
            featureService.Setup(t => t.IsFeatureEnabled(It.IsAny<VisitPayFeatureEnum>())).Returns<VisitPayFeatureEnum>(feature =>
            {
                if (feature == VisitPayFeatureEnum.Surveys)
                    return this.isSurveyEnabled;
                return true;
            });

            Mock<IStatementApplicationService> statementServiceMock = new Mock<IStatementApplicationService>();
            statementServiceMock.Setup(t => t.GetLastStatement(It.IsAny<int>())).Returns((int x) => new StatementDto { VpStatementId = 123 });

            Mock<IGuarantorService> guarantorServiceMock = new Mock<IGuarantorService>();
            guarantorServiceMock.Setup(t => t.GetGuarantorId(It.IsAny<int>())).Returns((int x) => 123);

            Mock<IContentService> contentServiceMock = new Mock<IContentService>();
            contentServiceMock.Setup(t => t.ClientReplacementValues()).Returns(() => new Dictionary<string, string>());

            contentServiceMock.Setup(t => t.GetCurrentVersionAsync(It.IsAny<CmsRegionEnum>(), It.IsAny<bool>(), It.IsAny<IDictionary<string, string>>()))
                .Returns<CmsRegionEnum, bool, IDictionary<string, string>>((cmsRegion, shouldReplace, replacementValues) =>
                    {
                        string foo = this.CmsLookup[(int)cmsRegion];
                        CmsVersion cmsVersion = new CmsVersion { ContentBody = foo };
                        return Task.FromResult(cmsVersion);
                    }
                );


            //this.BuildConfiguration();
            this.BuildConfigurationWithSqlLite();
            this.AddTestData();

            ISurveyService service = new SurveyService(new Lazy<ISurveyRepository>(() => new SurveyRepository(new SessionContext<VisitPay>(this._session))), new Lazy<ISurveyResultRepository>(() => new SurveyResultRepository(new SessionContext<VisitPay>(this._session))));

            this._surveyApplicationService = new SurveyApplicationServiceMockBuilder
            {
                SurveyService = service,
                ContentServiceMock = contentServiceMock,
                GuarantorServiceMock = guarantorServiceMock,
                StatementApplicationServiceMock = statementServiceMock,
                ApplicationServiceCommonServiceMockBuilder = new ApplicationServiceCommonServiceMockBuilder { FeatureServiceMock = featureService }
            }.CreateService();

            this._transaction = this._session.BeginTransaction();
            this._transaction.Begin();
        }

        [TearDown]
        public void RollbackTransaction()
        {
            this._transaction.Rollback();
            this._transaction.Dispose();
        }

        private static ISessionFactory _sessionFactory;
        private ISession _session;
        private ISurveyApplicationService _surveyApplicationService;
        private bool isSurveyEnabled = true;
        private ITransaction _transaction;

        /// <summary>
        ///     This method can be used for development and can assist with creating the tables
        ///     It can also help with developing and general troubleshooting
        ///     It is not referenced in the tests
        ///     Requires set up "test" database and run "create schema intelligence"
        /// </summary>
        protected void BuildConfiguration()
        {
            Configuration configuration = Fluently.Configure()
                .Database(MsSqlConfiguration.MsSql2008.ConnectionString("Data Source=DSCHRODER\\MSSQLDEV;Initial Catalog=test;Integrated Security=True"))
                .Mappings(m =>
                {
                    m.FluentMappings.Conventions.Setup(a => a.Add(AutoImport.Never()));
                    m.FluentMappings.Conventions.Add<Ivh.Common.Data.Conventions.DateTimeConvention>();
                    m.FluentMappings.Add<SurveyMap>();
                    m.FluentMappings.Add<SurveyQuestionMap>();
                    m.FluentMappings.Add<SurveyResultAnswerMap>();
                    m.FluentMappings.Add<SurveyResultTypeMap>();
                    m.FluentMappings.Add<SurveyResultMap>();
                    m.FluentMappings.Add<SurveyRatingGroupMap>();
                    m.FluentMappings.Add<SurveyRatingMap>();
                })
                .BuildConfiguration();

            _sessionFactory = configuration.BuildSessionFactory();
            this._session = _sessionFactory.OpenSession();

            SchemaExport exporter = new SchemaExport(configuration);
            exporter.Execute(true, true, false, this._session.Connection, null);
        }

        /// <summary>
        ///     SqlLite does not support DateTime and requires special configuration
        ///     This will create an in-memory database with the survey tables
        /// </summary>
        protected void BuildConfigurationWithSqlLite()
        {
            Configuration configuration = Fluently.Configure()
                .Database(SQLiteConfiguration.Standard.InMemory().ShowSql())
                .Mappings(m =>
                {
                    m.FluentMappings.Conventions.Setup(a => a.Add(AutoImport.Never()));
                    m.FluentMappings.Add<SurveyMap>().Conventions.Add(new NormalizedDateTimeUserTypeConvention());
                    m.FluentMappings.Add<SurveyQuestionMap>();
                    m.FluentMappings.Add<SurveyResultAnswerMap>().Conventions.Add(new NormalizedDateTimeUserTypeConvention());
                    m.FluentMappings.Add<SurveyResultTypeMap>();
                    m.FluentMappings.Add<SurveyResultMap>();
                    m.FluentMappings.Add<SurveyRatingGroupMap>();
                    m.FluentMappings.Add<SurveyRatingMap>();
                })
                .BuildConfiguration();

            _sessionFactory = configuration.BuildSessionFactory();
            this._session = _sessionFactory.OpenSession();

            SchemaExport exporter = new SchemaExport(configuration);
            exporter.Execute(false, true, false, this._session.Connection, null);
        }

        private void AddTestData()
        {
            //add the surveygroup
            SurveyRatingGroup surveyRatingGroup = new SurveyRatingGroup
            {
                SurveyRatingGroupId = 1,
                SurveyRatingGroupName = "AgreeDisagreeSurveyFormat"
            };

            List<SurveyRating> surveyRatings = new List<SurveyRating>();
            surveyRatings.Add(new SurveyRating { SurveyRatingGroup = surveyRatingGroup, DescriptionCmsRegion = CmsRegionEnum.SurveyRatingDescriptionStronglyDisagree, SurveyRatingScore = 1 });
            surveyRatings.Add(new SurveyRating { SurveyRatingGroup = surveyRatingGroup, DescriptionCmsRegion = CmsRegionEnum.SurveyRatingDescriptionDisagree, SurveyRatingScore = 2 });
            surveyRatings.Add(new SurveyRating { SurveyRatingGroup = surveyRatingGroup, DescriptionCmsRegion = CmsRegionEnum.SurveyRatingDescriptionNeutral, SurveyRatingScore = 3 });
            surveyRatings.Add(new SurveyRating { SurveyRatingGroup = surveyRatingGroup, DescriptionCmsRegion = CmsRegionEnum.SurveyRatingDescriptionAgree, SurveyRatingScore = 4 });
            surveyRatings.Add(new SurveyRating { SurveyRatingGroup = surveyRatingGroup, DescriptionCmsRegion = CmsRegionEnum.SurveyRatingDescriptionStronglyAgree, SurveyRatingScore = 5 });

            surveyRatingGroup.SurveyRatings = surveyRatings;
            this._session.Save(surveyRatingGroup);

            Survey survey = new Survey
            {
                SurveyId = 1,
                SurveyName = "FinancePlanCreation",
                DateActive = DateTime.UtcNow.AddMonths(-1),
                DateExpires = DateTime.UtcNow.AddMonths(12),
                FrequencyInDays = 60,
                FrequencyInDaysWhenIgnored = 60,
                SurveyQuestionsAll = new List<SurveyQuestion>(),
                TitleCmsRegion = CmsRegionEnum.SurveyTitleFinancePlanCreation
            };
            survey.SurveyQuestionsAll = this.GetSurveyQuestions(survey, surveyRatingGroup);
            this._session.Save(survey);

            Survey outOfDateSurvey = new Survey
            {
                SurveyId = 2,
                SurveyName = "TestOutOfDate",
                DateActive = DateTime.UtcNow.AddMonths(-12),
                DateExpires = DateTime.UtcNow.AddDays(-1),
                FrequencyInDays = 60,
                FrequencyInDaysWhenIgnored = 60,
                SurveyQuestionsAll = new List<SurveyQuestion>(),
                TitleCmsRegion = CmsRegionEnum.SurveyTitlePaperStatementData
            };
            outOfDateSurvey.SurveyQuestionsAll = this.GetSurveyQuestions(outOfDateSurvey, surveyRatingGroup, 5);
            this._session.Save(outOfDateSurvey);

            Survey groupSurvey1 = new Survey
            {
                SurveyId = 3,
                SurveyName = "GroupSurvey1",
                DateActive = DateTime.UtcNow.AddMonths(-1),
                DateExpires = DateTime.UtcNow.AddMonths(12),
                FrequencyInDays = 90,
                FrequencyInDaysWhenIgnored = 90,
                SurveyQuestionsAll = new List<SurveyQuestion>(),
                SurveyGroup = SurveyGroupEnum.FinancePlanCreation,
                TitleCmsRegion = CmsRegionEnum.SurveyTitleManualPayment
            };
            groupSurvey1.SurveyQuestionsAll = this.GetSurveyQuestions(outOfDateSurvey, surveyRatingGroup, 10);
            this._session.Save(groupSurvey1);

            Survey groupSurvey2 = new Survey
            {
                SurveyId = 4,
                SurveyName = "GroupSurvey2",
                DateActive = DateTime.UtcNow.AddMonths(-1),
                DateExpires = DateTime.UtcNow.AddMonths(12),
                FrequencyInDays = 90,
                FrequencyInDaysWhenIgnored = 90,
                SurveyQuestionsAll = new List<SurveyQuestion>(),
                SurveyGroup = SurveyGroupEnum.FinancePlanCreation,
                TitleCmsRegion = CmsRegionEnum.SurveyTitleManualPayment
            };
            groupSurvey2.SurveyQuestionsAll = this.GetSurveyQuestions(outOfDateSurvey, surveyRatingGroup, 15);
            this._session.Save(groupSurvey2);


            SurveyResultType surveyResultTypeIgnored = new SurveyResultType { SurveyResultTypeId = 1, SurveyResultTypeName = "Ignored" };
            SurveyResultType surveyResultTypeSubmitted = new SurveyResultType { SurveyResultTypeId = 2, SurveyResultTypeName = "Submitted" };
            this._session.Save(surveyResultTypeIgnored);
            this._session.Save(surveyResultTypeSubmitted);
        }

        private List<SurveyQuestion> GetSurveyQuestions(Survey survey, SurveyRatingGroup surveyRatingGroup, int offset = 0)
        {
            return new List<SurveyQuestion>
            {
                new SurveyQuestion {SurveyQuestionId = 1 + offset, IsActive = true, SurveyRatingGroup =surveyRatingGroup  ,Survey = survey, QuestionCmsRegion = CmsRegionEnum.FinancePlanCreationQuestion12},
                new SurveyQuestion {SurveyQuestionId = 2 + offset, IsActive = true, SurveyRatingGroup =surveyRatingGroup  ,Survey = survey, QuestionCmsRegion = CmsRegionEnum.FinancePlanCreationQuestion13},
                new SurveyQuestion {SurveyQuestionId = 3 + offset, IsActive = true, SurveyRatingGroup =surveyRatingGroup  ,Survey = survey, QuestionCmsRegion = CmsRegionEnum.FinancePlanCreationQuestion3},
                new SurveyQuestion {SurveyQuestionId = 4 + offset, IsActive = false,SurveyRatingGroup =surveyRatingGroup  ,Survey = survey,QuestionCmsRegion = CmsRegionEnum.FinancePlanCreationQuestion2},
                new SurveyQuestion {SurveyQuestionId = 5 + offset, IsActive = false,SurveyRatingGroup =surveyRatingGroup  ,Survey = survey,QuestionCmsRegion = CmsRegionEnum.FinancePlanCreationQuestion1}
            };
        }

        private Dictionary<int, string> CmsLookup = new Dictionary<int, string>()
        {
            [-6001] = "(Test) Please rate your experience creating a finance plan.",
            [-6002] = "(Test) Please rate your experience making a payment.",
            [-6003] = "(Test) Tell us about your experience.",
            [-6004] = "(Test) Please take a moment to answer a few questions",
            [-6005] = "(Test) Help us make [[ClientBrandName]] better",
            [-6101] = "(Test) Creating a finance plan was easy.",
            [-6102] = "(Test) The finance plan options met my needs.",
            [-6103] = "(Test) Finance plans are important to managing my health care bills.",
            [-6104] = "(Test) The amount I owed and due date were clear to me.",
            [-6105] = "(Test) Selecting, adding, or updating payment methods was easy",
            [-6106] = "(Test) The number of payment options met my needs.",
            [-6107] = "(Test) We noticed you had a choice. Why did you choose to make an additional plan instead of combining all your balances?",
            [-6108] = "(Test) How likely are you to recommend [[ClientBrandName]] to a friend or colleague?",
            [-6109] = "(Test) How satisfied are you with your experience?",
            [-6112] = "(Test) Setting up financing was easy.",
            [-6113] = "(Test) The financing options met my needs.",
            [-6114] = "(Test) The amount I owed today was clear to me.",
            [-6115] = "(Test) The payment options met my needs.",
            [-6116] = "(Test) We are considering offering you the option of having your statements printed and mailed to you. Would you want to receive paper statements if they were offered?",
            [-6201] = "Strongly Disagree",
            [-6202] = "Disagree",
            [-6203] = "Neutral",
            [-6204] = "Agree",
            [-6205] = "Strongly Agree",
            [-6206] = "Not likely",
            [-6207] = "Very likely",
            [-6208] = "No",
            [-6209] = "Yes",
            [-6210] = "",
        };


        [Test]
        public void ProcessResult_should_save_payment_id()
        {
            SurveyDto survey = this._surveyApplicationService.GetSurvey("FinancePlanCreation").Result;

            SurveyResultDto surveyResultDto = new SurveyResultDto
            {
                SurveyId = 1,
                VisitPayUserId = 1,
                SurveyResultAnswers = survey.SurveyQuestions,
                SurveyDate = DateTime.UtcNow,
                SurveyResultType = SurveyResultTypeEnum.Ignored,
                SurveyComment = "I ignored this survey",
                PaymentId = 42
            };

            SurveyResultRepository surveyResultRepository = new SurveyResultRepository(new SessionContext<VisitPay>(this._session));
            this._surveyApplicationService.ProcessSurveyResult(surveyResultDto);

            IList<SurveyResult> surveyResults = surveyResultRepository.GetSurveyResultsForVisitPayUser(1, 1);
            SurveyResult surveyResult = surveyResults.OrderByDescending(x => x.SurveyDate).FirstOrDefault();
            Assert.AreEqual(42, surveyResult.PaymentId);
        }

        [Test]
        public void ProcessResults_should_save_statement_id()
        {
            SurveyDto survey = this._surveyApplicationService.GetSurvey("FinancePlanCreation").Result;

            SurveyResultDto surveyResultDto = new SurveyResultDto
            {
                SurveyId = 1,
                VisitPayUserId = 1,
                SurveyResultAnswers = survey.SurveyQuestions,
                SurveyDate = DateTime.UtcNow,
                SurveyResultType = SurveyResultTypeEnum.Ignored,
                SurveyComment = "I ignored this survey"
            };

            SurveyResultRepository surveyResultRepository = new SurveyResultRepository(new SessionContext<VisitPay>(this._session));
            this._surveyApplicationService.ProcessSurveyResult(surveyResultDto);

            IList<SurveyResult> surveyResults = surveyResultRepository.GetSurveyResultsForVisitPayUser(1, 1);
            SurveyResult surveyResult = surveyResults.OrderByDescending(x => x.SurveyDate).FirstOrDefault();
            Assert.AreEqual(123, surveyResult.VpStatementId);
        }

        [Test]
        public void ProcessResults_with_questions()
        {
            SurveyDto survey = this._surveyApplicationService.GetSurvey("FinancePlanCreation").Result;

            List<SurveyResultAnswerDto> surveyQuestions = survey.SurveyQuestions;
            surveyQuestions[0].SurveyQuestionRating = 1;
            surveyQuestions[1].SurveyQuestionRating = 2;
            surveyQuestions[2].SurveyQuestionRating = 3;

            SurveyResultDto surveyResultDto = new SurveyResultDto
            {
                SurveyId = survey.SurveyId,
                VisitPayUserId = 1,
                SurveyResultAnswers = survey.SurveyQuestions,
                SurveyDate = DateTime.UtcNow,
                SurveyResultType = SurveyResultTypeEnum.Submitted,
                SurveyComment = "I completed the survey"
            };

            SurveyResultRepository surveyResultRepository = new SurveyResultRepository(new SessionContext<VisitPay>(this._session));
            this._surveyApplicationService.ProcessSurveyResult(surveyResultDto);
            IList<SurveyResult> surveyResults = surveyResultRepository.GetSurveyResultsForVisitPayUser(1, 1);
            SurveyResult surveyResult = surveyResults.OrderByDescending(x => x.SurveyDate).FirstOrDefault();
            Assert.AreEqual("I completed the survey", surveyResult.SurveyComment);
            Assert.AreEqual(3, surveyResult.SurveyResultAnswers.Count);
        }

        [Test]
        public void ProcessResults_with_questions_and_some_are_null()
        {
            SurveyDto survey = this._surveyApplicationService.GetSurvey("FinancePlanCreation").Result;

            List<SurveyResultAnswerDto> surveyQuestions = survey.SurveyQuestions;
            surveyQuestions[0].SurveyQuestionRating = 1;
            surveyQuestions[1].SurveyQuestionRating = 2;
            surveyQuestions[2].SurveyQuestionRating = null;

            SurveyResultDto surveyResultDto = new SurveyResultDto
            {
                SurveyId = 1,
                VisitPayUserId = 1,
                SurveyResultAnswers = survey.SurveyQuestions,
                SurveyDate = DateTime.UtcNow,
                SurveyResultType = SurveyResultTypeEnum.Submitted,
                SurveyComment = "I only answered 2"
            };

            SurveyResultRepository surveyResultRepository = new SurveyResultRepository(new SessionContext<VisitPay>(this._session));
            this._surveyApplicationService.ProcessSurveyResult(surveyResultDto);
            IList<SurveyResult> surveyResults = surveyResultRepository.GetSurveyResultsForVisitPayUser(1, 1);
            SurveyResult surveyResult = surveyResults.OrderByDescending(x => x.SurveyDate).FirstOrDefault();
            Assert.AreEqual(2, surveyResult.SurveyResultAnswers.Count);
            Assert.AreEqual(3, surveyResult.SurveyResultAnswers.Sum(x => x.SurveyQuestionRating));
        }

        [Test]
        public void ProcessResults_with_questions_survey_rating_descriptions_are_stored()
        {
            SurveyDto survey = this._surveyApplicationService.GetSurvey("FinancePlanCreation").Result;

            List<SurveyResultAnswerDto> surveyQuestions = survey.SurveyQuestions;
            int rating = 1;
            surveyQuestions.ForEach(a =>
            {
                a.SurveyQuestionRating = rating;
                a.SurveyRatingDescriptionText = a.SurveyRatingGroup.SurveyRatings
                    .First(x => x.SurveyRatingScore == rating).SurveyRatingDescription;
                rating++;
            });

            SurveyResultDto surveyResultDto = new SurveyResultDto
            {
                SurveyId = survey.SurveyId,
                VisitPayUserId = 1,
                SurveyResultAnswers = survey.SurveyQuestions,
                SurveyDate = DateTime.UtcNow,
                SurveyResultType = SurveyResultTypeEnum.Submitted,
                SurveyComment = "I completed the survey"
            };

            SurveyResultRepository surveyResultRepository = new SurveyResultRepository(new SessionContext<VisitPay>(this._session));
            this._surveyApplicationService.ProcessSurveyResult(surveyResultDto);
            IList<SurveyResult> surveyResults = surveyResultRepository.GetSurveyResultsForVisitPayUser(1, 1);
            SurveyResult surveyResult = surveyResults.OrderByDescending(x => x.SurveyDate).FirstOrDefault();

            //check to see if the survey rating description is being saved
            string responseToQuestion1 = surveyResult.SurveyResultAnswers
                .First(x => x.SurveyQuestionRating == 1)
                .SurveyRatingDescriptionText;
            string responseToQuestion2 = surveyResult.SurveyResultAnswers
                .First(x => x.SurveyQuestionRating == 2)
                .SurveyRatingDescriptionText;
            string responseToQuestion3 = surveyResult.SurveyResultAnswers
                .First(x => x.SurveyQuestionRating == 3)
                .SurveyRatingDescriptionText;
            Assert.AreEqual("Strongly Disagree", responseToQuestion1);
            Assert.AreEqual("Disagree", responseToQuestion2);
            Assert.AreEqual("Neutral", responseToQuestion3);
        }


        [Test]
        public void ProcessResultWithIgnore()
        {
            SurveyDto survey = this._surveyApplicationService.GetSurvey("FinancePlanCreation").Result;

            SurveyResultDto surveyResultDto = new SurveyResultDto
            {
                SurveyId = 1,
                VisitPayUserId = 1,
                SurveyResultAnswers = survey.SurveyQuestions,
                SurveyDate = DateTime.UtcNow,
                SurveyResultType = SurveyResultTypeEnum.Ignored,
                SurveyComment = "I ignored this survey"
            };

            SurveyResultRepository surveyResultRepository = new SurveyResultRepository(new SessionContext<VisitPay>(this._session));
            this._surveyApplicationService.ProcessSurveyResult(surveyResultDto);
            IList<SurveyResult> surveyResults = surveyResultRepository.GetSurveyResultsForVisitPayUser(1, 1);
            SurveyResult surveyResult = surveyResults.OrderByDescending(x => x.SurveyDate).FirstOrDefault();
            Assert.AreEqual("I ignored this survey", surveyResult.SurveyComment);
            Assert.AreEqual(0, surveyResult.SurveyResultAnswers.Count);
        }

        [Test]
        public void Should_not_be_presented_with_survey_when_surveys_are_disabled()
        {
            this.isSurveyEnabled = false;
            SurveyPresentationInfoDto showSurvey = this._surveyApplicationService.ShouldShowSurveyToVisitPayUser(1, "FinancePlanCreation");
            Assert.False(showSurvey.UserIsPresentedWithSurvey, "The user should not be presented the survey because surveys are disabled");
        }

        [Test]
        public void ShouldShowSurveyToVisitPayUser()
        {
            this.isSurveyEnabled = true;
            // User has never been presented with a survey and should be presented with a survey
            SurveyPresentationInfoDto showSurvey = this._surveyApplicationService.ShouldShowSurveyToVisitPayUser(1, "FinancePlanCreation");
            Assert.IsTrue(showSurvey.UserIsPresentedWithSurvey, "The user should be presented the survey because it is current and the user has never been presented with the survey");

            // User should not see a survey when the survey is out of date
            showSurvey = this._surveyApplicationService.ShouldShowSurveyToVisitPayUser(1, "TestOutOfDate");
            Assert.IsFalse(showSurvey.UserIsPresentedWithSurvey, "The test is out of date and should not be shown to user");

            // Ignored a survey and should not be presented with a survey
            SurveyDto survey = this._surveyApplicationService.GetSurvey("FinancePlanCreation").Result;
            SurveyResultDto surveyResultDto = new SurveyResultDto
            {
                SurveyId = 1,
                VisitPayUserId = 1,
                SurveyResultAnswers = survey.SurveyQuestions,
                SurveyDate = DateTime.UtcNow.AddDays(-1),
                SurveyResultType = SurveyResultTypeEnum.Ignored
            };

            this._surveyApplicationService.ProcessSurveyResult(surveyResultDto);
            showSurvey = this._surveyApplicationService.ShouldShowSurveyToVisitPayUser(1, "FinancePlanCreation");
            Assert.IsFalse(showSurvey.UserIsPresentedWithSurvey, "The user recently ignored a survey and should not be presented with a survey");

            // Ignored a survey, but enough time has elapse since and can be shown again
            SurveyResultDto surveyResultDtoForUser2 = new SurveyResultDto
            {
                SurveyId = 1,
                VisitPayUserId = 2,
                SurveyResultAnswers = survey.SurveyQuestions,
                SurveyDate = DateTime.UtcNow.AddDays(-61),
                SurveyResultType = SurveyResultTypeEnum.Ignored
            };

            this._surveyApplicationService.ProcessSurveyResult(surveyResultDtoForUser2);
            showSurvey = this._surveyApplicationService.ShouldShowSurveyToVisitPayUser(2, "FinancePlanCreation");
            Assert.IsTrue(showSurvey.UserIsPresentedWithSurvey, "The user should be presented with a survey even though the user submitted beyond the frequency window");
        }


        [Test]
        public void ShouldShowSurvey_When_Part_Of_A_Group_No_Survey_Results()
        {
            this.isSurveyEnabled = true;
            List<string> surveysInGroup = new List<string> { "GroupSurvey1", "GroupSurvey2" };

            SurveyPresentationInfoDto showSurvey = this._surveyApplicationService.ShouldShowSurveyToVisitPayUser(1, "GroupSurvey1");
            Assert.True(surveysInGroup.Contains(showSurvey.SurveyName));

            showSurvey = this._surveyApplicationService.ShouldShowSurveyToVisitPayUser(1, "GroupSurvey2");
            Assert.True(surveysInGroup.Contains(showSurvey.SurveyName));
        }



        
        [Test]
        public void ShouldShowSurvey_When_Part_Of_A_Group_With_Survey_Results()
        {
            this.isSurveyEnabled = true;

            // Ignored a survey and should not be presented with a survey
            // We need to make it any survey not just in this group
            SurveyDto survey = this._surveyApplicationService.GetSurvey("GroupSurvey1").Result;
            SurveyResultDto surveyResultDto = new SurveyResultDto
            {
                SurveyId = survey.SurveyId,
                VisitPayUserId = 1,
                SurveyResultAnswers = survey.SurveyQuestions,
                SurveyDate = DateTime.UtcNow.AddDays(-1),
                SurveyResultType = SurveyResultTypeEnum.Ignored
            };
            this._surveyApplicationService.ProcessSurveyResult(surveyResultDto);
            SurveyPresentationInfoDto showSurvey = this._surveyApplicationService.ShouldShowSurveyToVisitPayUser(1, "GroupSurvey1");
            Assert.False(showSurvey.UserIsPresentedWithSurvey);
        }


        [Test]
        public void ShouldShowSurvey_When_Part_Of_A_Group_With_Survey_Results_But_Eligible()
        {
            // In this scenario the user has taken a survey that is part of the group in past 
            // we expect the user will be presented with a survey in the group but not the one taken in the past
            this.isSurveyEnabled = true;

            // Ignored a survey and should not be presented with a survey
            this.ProcessASurvey("GroupSurvey1", SurveyResultTypeEnum.Submitted, 91);
            SurveyPresentationInfoDto showSurvey = this._surveyApplicationService.ShouldShowSurveyToVisitPayUser(1, "GroupSurvey1");

            Assert.AreEqual(showSurvey.SurveyName, "GroupSurvey2");
        }

        [Test]
        public void ShouldShowSurvey_When_Part_Of_A_Group_With_Survey_Results_But_Eligible2()
        {
            // In this scenario the user has taken all the surveys that are part of the group in past 
            // we expect the user will be presented with a survey in the group
            this.isSurveyEnabled = true;
            List<string> surveysInGroup = new List<string> { "GroupSurvey1", "GroupSurvey2" };

            this.ProcessASurvey("GroupSurvey1", SurveyResultTypeEnum.Submitted,91);
            this.ProcessASurvey("GroupSurvey2", SurveyResultTypeEnum.Submitted, 182);
            SurveyPresentationInfoDto showSurvey = this._surveyApplicationService.ShouldShowSurveyToVisitPayUser(1, "GroupSurvey1");
            Assert.True(surveysInGroup.Contains(showSurvey.SurveyName));
        }


        private void ProcessASurvey(string surveyName, SurveyResultTypeEnum surveyResultType, int daysInPast)
        {
            SurveyDto survey = this._surveyApplicationService.GetSurvey(surveyName).Result;
            List<SurveyResultAnswerDto> surveyQuestions = survey.SurveyQuestions;
            string surveyComment = "";
            if (surveyResultType == SurveyResultTypeEnum.Submitted)
            {
                //add some results
                surveyQuestions[0].SurveyQuestionRating = 1;
                surveyQuestions[1].SurveyQuestionRating = 2;
                surveyQuestions[2].SurveyQuestionRating = 3;
                surveyComment = "I completed the survey";
            }
            SurveyResultDto surveyResultDto = new SurveyResultDto
            {
                SurveyId = survey.SurveyId,
                VisitPayUserId = 1,
                SurveyResultAnswers = surveyQuestions,
                SurveyDate = DateTime.UtcNow.AddDays(daysInPast * -1),
                SurveyResultType = surveyResultType,
                SurveyComment = surveyComment
            };
            this._surveyApplicationService.ProcessSurveyResult(surveyResultDto);
        }

        [Test]
        public void ProcessResult_should_save_device_type()
        {
            SurveyDto survey = this._surveyApplicationService.GetSurvey("FinancePlanCreation").Result;

            SurveyResultDto surveyResultDto = new SurveyResultDto
            {
                SurveyId = 1,
                VisitPayUserId = 1,
                SurveyResultAnswers = survey.SurveyQuestions,
                SurveyDate = DateTime.UtcNow,
                SurveyResultType = SurveyResultTypeEnum.Ignored,
                SurveyComment = "I ignored this survey",
                PaymentId = 42,
                DeviceType = "Mobile"

            };

            SurveyResultRepository surveyResultRepository = new SurveyResultRepository(new SessionContext<VisitPay>(this._session));
            this._surveyApplicationService.ProcessSurveyResult(surveyResultDto);

            IList<SurveyResult> surveyResults = surveyResultRepository.GetSurveyResultsForVisitPayUser(1, 1);
            SurveyResult surveyResult = surveyResults.OrderByDescending(x => x.SurveyDate).FirstOrDefault();
            Assert.AreEqual("Mobile", surveyResult.DeviceType);
        }

        [Test]
        public void Should_not_multiple_surveys_in_single_session()
        {
            this.isSurveyEnabled = true;
            SurveyPresentationInfoDto showSurvey = this._surveyApplicationService.ShouldShowSurveyToVisitPayUser(1, "FinancePlanCreation", true);
            Assert.False(showSurvey.UserIsPresentedWithSurvey, "The user should not be presented the survey because another survey has been shown in the same session");
        }
    }
}
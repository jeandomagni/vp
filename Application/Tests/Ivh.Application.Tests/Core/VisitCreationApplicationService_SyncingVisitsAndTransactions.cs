﻿namespace Ivh.Application.Tests.Core
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Application.Base.Common.Interfaces;
    using Application.Core.ApplicationServices;
    using Application.Core.Common.Interfaces;
    using AutoMapper;
    using Common.Base.Constants;
    using Common.Base.Enums;
    using Common.Base.Utilities.Helpers;
    using Common.Cache;
    using Common.Data.Connection;
    using Common.Data.Connection.Connections;
    using Common.ServiceBus.Interfaces;
    using Common.State.Interfaces;
    using Common.Tests.Builders;
    using Common.Tests.Helpers.Base;
    using Common.VisitPay.Constants;
    using Common.VisitPay.Enums;
    using Common.VisitPay.Messages.HospitalData;
    using Common.VisitPay.Messages.HospitalData.Dtos;
    using Domain.Base.Interfaces;
    using Domain.Guarantor.Entities;
    using Domain.Guarantor.Interfaces;
    using Domain.Core.MergeException.Interfaces;
    using Domain.Core.SystemMessage.Interfaces;
    using Domain.EventJournal.Interfaces;
    using Domain.User.Interfaces;
    using Domain.Visit.Entities;
    using Domain.Visit.Interfaces;
    using Domain.Visit.Services;
    using Domain.FinanceManagement.FinancePlan.Interfaces;
    using Domain.FinanceManagement.Payment.Interfaces;
    using Domain.FinanceManagement.Statement.Interfaces;
    using Domain.Guarantor.Rules;
    using Domain.Guarantor.Services;
    using Domain.Logging.Interfaces;
    using Domain.Settings.Entities;
    using Domain.Settings.Interfaces;
    using Domain.Visit.Unmatching.Interfaces;
    using Moq;
    using NHibernate;
    using NUnit.Framework;

    [TestFixture]
    // ReSharper disable once InconsistentNaming
    public class VisitServiceTests_SyncingVisitsAndTransactions : ApplicationTestBase
    {
        [SetUp]
        public void BeforeEachTest()
        {
            this._moqClientService = new Mock<IClientService>();
            this._moqTransaction = new Mock<ITransaction>();
            this._moqTransaction.Setup(t => t.IsActive).Returns(() => true);

            Mock<Client> clientMock = new Mock<Client>();
            clientMock.Setup(t => t.SelfPayPrimaryInsuranceTypeId).Returns(14);
            this._moqClientService.Setup(t => t.GetClient()).Returns(clientMock.Object);

            this._moqSession = new Mock<ISession>();
            this._moqSession.Setup(t => t.Transaction).Returns(() => this._moqTransaction.Object);
            this._moqSession.Setup(t => t.BeginTransaction(Ivh.Common.Data.Constants.IsolationLevel.Default)).Returns(() => this._moqTransaction.Object);

            this._moqFeatureService = new Mock<IFeatureService>();
            this._moqFeatureService.Setup(s => s.IsFeatureEnabled(VisitPayFeatureEnum.HealthEquityFeature)).Returns(() => false);

            this._moqVisitRepository = new Mock<IVisitRepository>();
            this._moqVisitRepository.Setup(t => t.GetStatementVisitStateModel(It.IsAny<Visit>())).Returns(() => new StatementVisitStatusModel());

            this._moqVisitTransactionService = new Mock<IVisitTransactionWriteService>();
            this._moqVisitTransactionService.Setup(t => t.GetTransactionTypes()).Returns(() =>
            {
                return Enum.GetValues(typeof(VpTransactionTypeEnum)).Cast<VpTransactionTypeEnum>()
                    .Select(s => new VpTransactionType { VpTransactionTypeId = (int)s })
                    .ToList()
                    .AsReadOnly();
            });

            this._moqGuarantorService = new Mock<IGuarantorService>();
            this._moqFinancePlanService = new Mock<IFinancePlanService>();
            this._moqPaymentAllocationService = new Mock<IPaymentAllocationService>();
            this._moqUnmatchingServiceInternal = new Mock<IUnmatchingService>();
            this._unmatchingService = new Lazy<IUnmatchingService>(() => this._moqUnmatchingServiceInternal.Object);
            this._moqServiceBus = new Mock<IBus>();
            this._moqLoggingService = new Mock<ILoggingService>();
            this._moqeventJournalService = new Mock<IEventJournalService>();
            this._moqDistributedCache = new Mock<IDistributedCache>();
            this._moqDistributedCache.Setup(t => t.GetLock(It.IsAny<string>())).Returns(true);

            this._moqVisitTransactionRepository = new Mock<IVisitTransactionRepository>(); 
            this._moqVisitTransactionWriteRepository = new Mock<IVisitTransactionWriteRepository>();

            this._moqMergeExceptionService = new Mock<IMergeExceptionService>();

            this._moqVisitTransactionRepository.Setup(t => t.GetTransactionTypes()).Returns(() =>
            {
                return Enum.GetValues(typeof(VpTransactionTypeEnum)).Cast<VpTransactionTypeEnum>()
                    .Select(s => new VpTransactionType { VpTransactionTypeId = (int)s })
                    .ToList()
                    .AsReadOnly();
            });

            this._visitTransactionService = new VisitTransactionService(
                new Lazy<IDomainServiceCommonService>(() => this._domainServiceCommonService),
                new Lazy<IVisitTransactionRepository>(() => this._moqVisitTransactionRepository.Object),
                new Lazy<IVisitTransactionWriteRepository>(() => this._moqVisitTransactionWriteRepository.Object),
                new Lazy<IVisitRepository>(() => this._moqVisitRepository.Object));

            this._applicationSettingsMock = new Mock<IApplicationSettingsService>();
            this._applicationSettingsMock.Setup(t => t.PastDueAgingCount).Returns(() => new ApplicationSetting<int>(() => new Lazy<int>(() => 1)));
            this._applicationSettingsMock.Setup(t => t.HsCurrentBalanceSumExcludedTransactionTypes).Returns(() => new ApplicationSetting<IList<VpTransactionTypeEnum>>(() => new Lazy<IList<VpTransactionTypeEnum>>(() => new List<VpTransactionTypeEnum>()
            {
                VpTransactionTypeEnum.VppInterestPayment,
                VpTransactionTypeEnum.VppInterestPaymentHsReversal,
                VpTransactionTypeEnum.VppInterestPaymentHsReallocation,
                VpTransactionTypeEnum.VppInterestPaymentReassessmentReversal,
                VpTransactionTypeEnum.VppInterestPaymentReassessmentReallocation,

            })));

            this.ApplicationServiceCommonServiceMockBuilder = new ApplicationServiceCommonServiceMockBuilder()
            {
                ApplicationSettingsServiceMock = this._applicationSettingsMock,
                FeatureServiceMock = this._moqFeatureService,
                ClientServiceMock = this._moqClientService,
                LoggingServiceMock = this._moqLoggingService,
                EventJournalServiceMock = this._moqeventJournalService,
                BusMock = this._moqServiceBus,
                DistributedCacheMock = this._moqDistributedCache
            };

            this._applicationServiceCommonService = this.ApplicationServiceCommonServiceMockBuilder.CreateService();

            this._domainServiceCommonService = DomainServiceCommonServices.CreateDomainServiceCommonService(
                this._applicationSettingsMock,
                this._moqFeatureService,
                this._moqClientService,
                this._moqLoggingService,
                this._moqServiceBus,
                this._moqDistributedCache,
                null);

            this._visitStateMachineServiceMock = new Mock<IVisitStateMachineService>();
            this._eventJournalService = new Mock<Ivh.Common.EventJournal.Interfaces.IEventJournalService>();
            this._guarantorStateMachineService = new GuarantorStateMachineService(null, new VpGuarantorStateRules(), new Lazy<Ivh.Common.EventJournal.Interfaces.IEventJournalService>(() => this._eventJournalService.Object));

            this._visitStateService = new VisitStateService(
                new Lazy<IDomainServiceCommonService>(() => this._domainServiceCommonService),
                new Lazy<IVisitRepository>(() => this._moqVisitRepository.Object),
                new Lazy<IVisitStateMachineService>(() => this._visitStateMachineServiceMock.Object));

            this._visitService = new VisitService(
                new Lazy<IDomainServiceCommonService>(() => this._domainServiceCommonService),
                new Lazy<IVisitRepository>(() => this._moqVisitRepository.Object),
                new Lazy<IVisitTransactionRepository>(() => this._moqVisitTransactionRepository.Object),
                new Lazy<IVisitStateService>(() => this._visitStateService));

            this._moqStatementService = new Mock<IVpStatementService>();
            this._moqStatementService.Setup(s => s.GetMostRecentStatement(It.IsAny<Guarantor>())).Returns(() => null);

            this._moqInsuranceService = new Mock<IInsuranceService>();

            this._moqVisitPayUserJournalEventService = new Mock<IVisitPayUserJournalEventService>();

            this._moqApplicationSettingsService = new Mock<IApplicationSettingsService>();
            this._moqApplicationSettingsService.Setup(t => t.PublishInterestAssessment).Returns(() => new ApplicationSetting<bool>(() => new Lazy<bool>(() => true)));
            this._moqApplicationSettingsService.Setup(t => t.HsCurrentBalanceSumExcludedTransactionTypes).Returns(() => new ApplicationSetting<IList<VpTransactionTypeEnum>>(() => new Lazy<IList<VpTransactionTypeEnum>>(() => new List<VpTransactionTypeEnum>()
            {
            })));

            this._moqSystemMessageVisitPayUserService = new Mock<ISystemMessageVisitPayUserService>();

            this._visitCreationApplicationService = new VisitCreationApplicationService(
                new Lazy<IApplicationServiceCommonService>(() => this._applicationServiceCommonService),
                new SessionContext<VisitPay>(this._moqSession.Object),
                new Lazy<IVisitService>(() => this._visitService),
                new Lazy<IGuarantorService>(() => this._moqGuarantorService.Object),
                new Lazy<IFinancePlanService>(() => this._moqFinancePlanService.Object),
                new Lazy<IVisitTransactionWriteService>(() => this._moqVisitTransactionService.Object),
                new Lazy<IVisitStateService>(() => this._visitStateService),
                new Lazy<IInsuranceService>(() => this._moqInsuranceService.Object),
                new Lazy<IFacilityService>(() => new Mock<IFacilityService>().Object),
                new Lazy<IVisitPayUserJournalEventService>(() => this._moqVisitPayUserJournalEventService.Object),
                this._unmatchingService,
                new Lazy<ISystemMessageVisitPayUserService>(() => this._moqSystemMessageVisitPayUserService.Object));
        }

        public ApplicationServiceCommonServiceMockBuilder ApplicationServiceCommonServiceMockBuilder;

        private IApplicationServiceCommonService _applicationServiceCommonService;
        private IDomainServiceCommonService _domainServiceCommonService;
        private Mock<IApplicationSettingsService> _applicationSettingsMock;
        private Mock<IVisitStateMachineService> _visitStateMachineServiceMock;
        private Mock<Ivh.Common.EventJournal.Interfaces.IEventJournalService> _eventJournalService;
        private IVisitCreationApplicationService _visitCreationApplicationService;
        private Mock<IVisitRepository> _moqVisitRepository;
        private IVisitService _visitService;
        private Mock<ISession> _moqSession;
        private IVisitTransactionWriteService _visitTransactionService;
        private Mock<IVisitTransactionRepository> _moqVisitTransactionRepository;
        private Mock<IVisitTransactionWriteRepository> _moqVisitTransactionWriteRepository;
        private IVisitStateService _visitStateService;
        private IGuarantorStateMachineService _guarantorStateMachineService;
        private Mock<IGuarantorService> _moqGuarantorService;
        private Mock<IFinancePlanService> _moqFinancePlanService;
        private Mock<IPaymentAllocationService> _moqPaymentAllocationService;
        private Lazy<IUnmatchingService> _unmatchingService;
        private Mock<IUnmatchingService> _moqUnmatchingServiceInternal;
        private Mock<IBus> _moqServiceBus;
        private Mock<IApplicationSettingsService> _moqApplicationSettingsService;
        private Mock<ILoggingService> _moqLoggingService;
        private Mock<IEventJournalService> _moqeventJournalService;
        private Mock<IDistributedCache> _moqDistributedCache;
        private Mock<ITransaction> _moqTransaction;
        private Mock<IClientService> _moqClientService;
        private Mock<IFeatureService> _moqFeatureService;
        private Mock<IVisitTransactionWriteService> _moqVisitTransactionService;
        private Mock<IMergeExceptionService> _moqMergeExceptionService;
        private Mock<IVpStatementService> _moqStatementService;
        private Mock<IInsuranceService> _moqInsuranceService;
        private Mock<IVisitPayUserJournalEventService> _moqVisitPayUserJournalEventService;
        private Mock<ISystemMessageVisitPayUserService> _moqSystemMessageVisitPayUserService;
        private Random _random = new Random();

        private Guarantor SetupGuarantor(int vpGuarantorId, int hsGuarantorId, int billingSystemId, string sourceSystemKey, HsGuarantorMatchStatusEnum hsGuarantorMatchStatusEnum = HsGuarantorMatchStatusEnum.Matched)
        {
            Guarantor guarantor = new GuarantorBuilder().WithDefaults(vpGuarantorId).AsOnline();

            guarantor.ChangeStateToActive = true;
            this._guarantorStateMachineService.Evaluate(guarantor);

            guarantor.HsGuarantorMaps = new List<HsGuarantorMap>
            {
                new HsGuarantorMap
                {
                    BillingSystem = new BillingSystem(){BillingSystemId = billingSystemId},
                    VpGuarantor = guarantor,
                    HsGuarantorId = hsGuarantorId,
                    SourceSystemKey = sourceSystemKey,
                    HsGuarantorMatchStatus = hsGuarantorMatchStatusEnum
                }
            };

            this._moqGuarantorService.Setup(t => t.GetGuarantorsFromIntList(It.IsAny<IList<int>>())).Returns(new List<Guarantor> { guarantor });
            return guarantor;
        }

        private List<VisitChangeDto> SetupVisitWithChangeDtos(
            Guarantor guarantor,
            string sourceSystemKey,
            string text,
            DateTime dischargeDate,
            decimal balance,
            int hsVisitId,
            out Visit visit,
            bool vpEligible = true)
        {
            visit = new Visit
            {
                SourceSystemKey = sourceSystemKey,
                VisitId = 1,
                VPGuarantor = guarantor,
                VpEligible = vpEligible,
                HsGuarantorMap = new HsGuarantorMap()
                {
                    VpGuarantor = guarantor,
                    HsGuarantorId = 12345,
                    BillingSystem = new BillingSystem() { BillingSystemId = 1 },
                    HsGuarantorMatchStatus = HsGuarantorMatchStatusEnum.Matched
                }
            };
            this._moqVisitRepository.Setup(t => t.GetBySourceSystemKey(sourceSystemKey, It.IsAny<int>())).Returns(visit);

            return new List<VisitChangeDto>
            {
                new VisitChangeDto
                {
                    VisitId = hsVisitId,
                    SourceSystemKey = sourceSystemKey,
                    BillingApplication = text,
                    VisitDescription = text,
                    PatientFirstName = text,
                    PatientLastName = text,
                    DischargeDate = dischargeDate,
                    HsCurrentBalance = balance,
                    HsGuarantorId = 12345,
                    VpEligible = vpEligible
                },
            };
        }

        private List<VisitTransactionChangeDto> SetupVisitTransactionChangeDtos(string sourceSystemKey, string randomText, DateTime randomDate, decimal randomAmount, int transactionType, int visitId, out VisitTransaction visitTransaction)
        {
            visitTransaction = new VisitTransaction { PostDate = DateTime.MinValue };
            visitTransaction.SetTransactionAmount(randomAmount);
            this._moqVisitTransactionService.Setup(t => t.GetBySourceSystemKey(sourceSystemKey, It.IsAny<int>())).Returns(visitTransaction);
            this._moqVisitTransactionService.Setup(t => t.GetBySourceSystemKey(new List<string>() { sourceSystemKey }, It.IsAny<int>())).Returns(new List<VisitTransaction>() { visitTransaction });
            this._moqVisitTransactionService.Setup(t => t.GetTransactionTypes()).Returns(new List<VpTransactionType> { new VpTransactionType { VpTransactionTypeId = transactionType } });

            List<VisitTransactionChangeDto> visitTransactionChanges = new List<VisitTransactionChangeDto>
            {
                new VisitTransactionChangeDto
                {
                    SourceSystemKey = sourceSystemKey,
                    VisitId = visitId,
                    TransactionDate = randomDate,
                    VpTransactionTypeId = transactionType,
                    TransactionDescription = randomText,
                    TransactionAmount = randomAmount,
                    PostDate = randomDate
                }
            };
            return visitTransactionChanges;
        }

        private List<VisitTransactionChangeDto> SetupVisitTransactionChangeDtosNoMatch(string sourceSystemKey, string randomText, DateTime randomDate, decimal randomAmount, int transactionType, int visitId, out VisitTransaction visitTransaction)
        {
            visitTransaction = new VisitTransaction { PostDate = DateTime.MinValue, VpTransactionType = new VpTransactionType { VpTransactionTypeId = transactionType } };
            visitTransaction.SetTransactionAmount(randomAmount);
            this._moqVisitTransactionService.Setup(t => t.GetBySourceSystemKey(sourceSystemKey, It.IsAny<int>())).Returns((VisitTransaction)null);
            this._moqVisitTransactionService.Setup(t => t.GetBySourceSystemKey(new List<string>() { sourceSystemKey }, It.IsAny<int>())).Returns(new List<VisitTransaction>() { });
            this._moqVisitTransactionService.Setup(t => t.GetTransactionTypes()).Returns(new List<VpTransactionType> { new VpTransactionType { VpTransactionTypeId = transactionType } });
            this._moqVisitTransactionService.Setup(t => t.GetReallocationTransactionType(It.IsAny<VpTransactionTypeEnum>())).Returns(new VpTransactionType { VpTransactionTypeId = 102 });
            this._moqVisitTransactionService.Setup(t => t.GetReversalTransactionType(It.IsAny<VpTransactionTypeEnum>())).Returns(new VpTransactionType { VpTransactionTypeId = 101 });
            List<VisitTransactionChangeDto> visitTransactionChanges = new List<VisitTransactionChangeDto>
            {
                new VisitTransactionChangeDto
                {
                    SourceSystemKey = sourceSystemKey,
                    VisitId = visitId,
                    TransactionDate = randomDate,
                    VpTransactionTypeId = transactionType,
                    TransactionDescription = randomText,
                    TransactionAmount = randomAmount,
                    PostDate = randomDate
                }
            };
            return visitTransactionChanges;
        }

        [Test]
        public void HandlesMissingVpGuarantorMatches_When_Matching_CreatingNewVisit()
        {
            this._moqGuarantorService.Setup(t => t.GetGuarantorsFromIntList(It.IsAny<IList<int>>())).Returns(new List<Guarantor> { new Guarantor() });
            Guarantor guarantor = this.SetupGuarantor(98765, 12345, 1, "GUARANTOR");
            ChangeSetMessage visitChangeSetDataChangedMessage = new ChangeSetMessage
            {
                HsGuarantors = new List<HsGuarantorDto>
                {
                    new HsGuarantorDto
                    {
                        HsBillingSystemId = guarantor.HsGuarantorMaps[0].BillingSystem.BillingSystemId,
                        SourceSystemKey = guarantor.HsGuarantorMaps[0].SourceSystemKey,
                        HsGuarantorId = guarantor.HsGuarantorMaps[0].HsGuarantorId,
                    }
                },
                VpGuarantorHsMatches = new List<VpGuarantorHsMatchDto>
                {
                    new VpGuarantorHsMatchDto
                    {
                        VpGuarantorId = guarantor.VpGuarantorId,
                        HsGuarantorId = guarantor.HsGuarantorMaps[0].HsGuarantorId,
                        HsGuarantorMatchStatus = HsGuarantorMatchStatusEnum.Matched
                    }
                },
                VisitChanges = new List<VisitChangeDto> { new VisitChangeDto { SourceSystemKey = "123" }, new VisitChangeDto { SourceSystemKey = "234" } },
                VisitTransactionChanges = null,
            };

            Assert.DoesNotThrow(delegate { this._visitCreationApplicationService.MergeOrCreateVisitsFromVisitChangeSet(visitChangeSetDataChangedMessage); });
        }

        [Test]
        public void HandlesMissingVpGuarantorMatches_When_Matching_MergingVisit()
        {
            Guarantor guarantor = this.SetupGuarantor(98765, 12345, 1, "1-12345");
            this._moqVisitRepository.Setup(t => t.GetBySourceSystemKey("123", It.IsAny<int>())).Returns(new Visit
            {
                SourceSystemKey = "123",
                VisitId = 1,
                VPGuarantor = guarantor,
                HsGuarantorMatchStatus = HsGuarantorMatchStatusEnum.Matched,
                HsGuarantorMap = guarantor.HsGuarantorMaps[0]
            });
            this._moqGuarantorService.Setup(t => t.GetGuarantorsFromIntList(It.IsAny<IList<int>>())).Returns(new List<Guarantor> { new Guarantor() });
            ChangeSetMessage visitChangeSetDataChangedMessage = new ChangeSetMessage
            {
                HsGuarantors = new List<HsGuarantorDto>
                {
                    new HsGuarantorDto
                    {
                        HsBillingSystemId = guarantor.HsGuarantorMaps[0].BillingSystem.BillingSystemId,
                        SourceSystemKey = guarantor.HsGuarantorMaps[0].SourceSystemKey,
                        HsGuarantorId = guarantor.HsGuarantorMaps[0].HsGuarantorId,
                    }
                },
                VpGuarantorHsMatches = new List<VpGuarantorHsMatchDto>
                {
                    new VpGuarantorHsMatchDto
                    {
                        VpGuarantorId = guarantor.VpGuarantorId,
                        HsGuarantorId = guarantor.HsGuarantorMaps[0].HsGuarantorId,
                        HsGuarantorMatchStatus = HsGuarantorMatchStatusEnum.Matched
                    }
                },
                VisitChanges = new List<VisitChangeDto>
                {
                    new VisitChangeDto
                    {
                        SourceSystemKey = "123",
                        HsGuarantorId = guarantor.HsGuarantorMaps[0].HsGuarantorId
                    },
                    new VisitChangeDto
                    {
                        SourceSystemKey = "234"
                    }
                },
                VisitTransactionChanges = new List<VisitTransactionChangeDto>(),
            };

            Assert.DoesNotThrow(delegate { this._visitCreationApplicationService.MergeOrCreateVisitsFromVisitChangeSet(visitChangeSetDataChangedMessage); });
        }

        [Test]
        public void HandlesNullVisitChangesGracefully()
        {
            this._moqGuarantorService.Setup(t => t.GetGuarantorsFromIntList(It.IsAny<IList<int>>())).Returns(new List<Guarantor> { new Guarantor() });
            Guarantor guarantor = this.SetupGuarantor(98765, 12345, 1, "GUARANTOR");
            ChangeSetMessage visitChangeSetDataChangedMessage = new ChangeSetMessage
            {
                HsGuarantors = new List<HsGuarantorDto>
                {
                    new HsGuarantorDto
                    {
                        HsBillingSystemId = guarantor.HsGuarantorMaps[0].BillingSystem.BillingSystemId,
                        SourceSystemKey = guarantor.HsGuarantorMaps[0].SourceSystemKey,
                        HsGuarantorId = guarantor.HsGuarantorMaps[0].HsGuarantorId,
                    }
                },
                VpGuarantorHsMatches = new List<VpGuarantorHsMatchDto>
                {
                    new VpGuarantorHsMatchDto
                    {
                        VpGuarantorId = guarantor.VpGuarantorId,
                        HsGuarantorId = guarantor.HsGuarantorMaps[0].HsGuarantorId,
                        HsGuarantorMatchStatus = HsGuarantorMatchStatusEnum.Matched
                    }
                },
                VisitChanges = new List<VisitChangeDto>(),
                VisitTransactionChanges = new List<VisitTransactionChangeDto>(),
            };
            Assert.DoesNotThrow(delegate { this._visitCreationApplicationService.MergeOrCreateVisitsFromVisitChangeSet(visitChangeSetDataChangedMessage); });
        }
        

        [Ignore(ObsoleteDescription.VisitStateMachineTest)]
        [Test]
        public void When_Matching_MergingVisitTransactions_InterestOnlyBalanceRemainsHsBalanceNonZero()
        {
            string visitSSK = "visitSSK";
            string visitTransaction0SSK = "visitTransaction0SSK";
            string visitTransaction1SSK = "visitTransaction1SSK";
            string visitTransaction2SSK = "visitTransaction2SSK";

            int vpGuarantorId = 100;
            int visitId = 999;

            decimal amount = 100;

            int billingSystemId = 1;
            string guarantorSSK = "GUARANTOR";
            int hsGuarantorId = 55;

            BillingSystem billingSystem = new BillingSystem { BillingSystemId = billingSystemId };

            Guarantor guarantor = this.SetupGuarantor(vpGuarantorId, hsGuarantorId, billingSystemId, guarantorSSK);

            Visit visit = new Visit
            {
                VisitId = visitId,
                SourceSystemKey = visitSSK,
                BillingSystem = billingSystem,
                VPGuarantor = guarantor,
                HsGuarantorMap = guarantor.HsGuarantorMaps[0]
            };
            visit.SetCurrentBalance(amount);

            ((IEntity<VisitStateEnum>)visit).SetState(VisitStateEnum.Active, "");

            // ADD a Hospital Charge
            VisitTransaction visitTransaction = new VisitTransaction
            {
                BillingSystem = billingSystem,
                Visit = visit,
                VpGuarantorId = vpGuarantorId,
                VpTransactionType = new VpTransactionType { VpTransactionTypeId = (int)VpTransactionTypeEnum.HsCharge }
            };
            visitTransaction.SetSourceSystemKey(visitTransaction0SSK);
            visitTransaction.SetTransactionAmount(amount);

            ChangeSetMessage visitChangeSetDataChangedMessage = new ChangeSetMessage
            {
                HsGuarantors = new List<HsGuarantorDto>
                {
                    new HsGuarantorDto
                    {
                        HsBillingSystemId = guarantor.HsGuarantorMaps[0].BillingSystem.BillingSystemId,
                        SourceSystemKey = guarantor.HsGuarantorMaps[0].SourceSystemKey,
                        HsGuarantorId = guarantor.HsGuarantorMaps[0].HsGuarantorId,
                    }
                },
                VpGuarantorHsMatches = new List<VpGuarantorHsMatchDto>
                {
                    new VpGuarantorHsMatchDto
                    {
                        VpGuarantorId = guarantor.VpGuarantorId,
                        HsGuarantorId = guarantor.HsGuarantorMaps[0].HsGuarantorId,
                        HsGuarantorMatchStatus = HsGuarantorMatchStatusEnum.Matched
                    }
                },
                VisitChanges = new List<VisitChangeDto>
                {
                    new VisitChangeDto
                    {
                        BillingSystemId = billingSystem.BillingSystemId,
                        ChangeType = ChangeTypeEnum.Insert,
                        DataLoadId = 0,
                        SourceSystemKey = visitSSK,
                        VisitId = visitId,
                        HsCurrentBalance = 0,
                        HsGuarantorId = hsGuarantorId
                    }
                },
                VisitTransactionChanges = new List<VisitTransactionChangeDto>
                {
                    new VisitTransactionChangeDto
                    {
                        BillingSystemId = billingSystem.BillingSystemId,
                        ChangeType = ChangeTypeEnum.Insert,
                        DataLoadId = 0,
                        PostDate = DateTime.UtcNow,
                        SourceSystemKey = visitTransaction2SSK,
                        TransactionAmount = -amount,
                        TransactionDate = DateTime.UtcNow,
                        TransactionDescription = "Test",
                        VpTransactionTypeId = (int) VpTransactionTypeEnum.HsPatientCash,
                        VisitId = visitId
                    }
                }
            };

            this._moqGuarantorService.Setup(t => t.GetGuarantorsFromIntList(new List<int> { vpGuarantorId })).Returns(() => new List<Guarantor> { guarantor });
            this._moqVisitRepository.Setup(t => t.GetVisit(vpGuarantorId, visitId)).Returns(() => visit);
            this._moqVisitRepository.Setup(t => t.GetVisits(vpGuarantorId)).Returns(() => (new[] { visit }).ToList());
            this._moqVisitRepository.Setup(t => t.GetBySourceSystemKey(visitSSK, billingSystem.BillingSystemId)).Returns(() => visit);
            this._moqVisitTransactionService.Setup(t => t.GetBySourceSystemKey(new List<string> { visitTransaction0SSK, visitTransaction1SSK }, It.IsAny<int>())).Returns(new List<VisitTransaction> { visitTransaction });
            this._moqVisitTransactionService.Setup(t => t.GetBySourceSystemKey(new List<string> { visitTransaction2SSK }, It.IsAny<int>())).Returns(new List<VisitTransaction>());
            this._moqVisitTransactionRepository.Setup(t => t.GetBySourceSystemKey(visitTransaction0SSK, billingSystem.BillingSystemId)).Returns(() => visitTransaction);
            this._moqFinancePlanService.Setup(t => t.IsVisitInAFinancePlan(It.IsAny<Visit>())).Returns(true);
            this._moqFinancePlanService.Setup(t => t.WriteOffInterestOnlyBalance(It.IsAny<int>())).Returns(true);

            this._visitCreationApplicationService.MergeOrCreateVisitsFromVisitChangeSet(visitChangeSetDataChangedMessage);

            //Assert.IsTrue(visit.VisitState.IsPaidInFull());
        }

        [Test]
        public void When_Matching_MergingVisitTransactions_UpdatedCorrectFields()
        {
            /*
             * Should merge the following fields:
                TransactionDate
                VpTransactionTypeId -- should not merge transaction type unless new transaction
                TransactionDescription -- should not change description unless HS transaction type
             
             * Should not merge:
                TransactionAmount
                PostDate
             */

            int vpTransactionTypeIdItShouldMergeTo = 1;
            string sourceSystemKey = RandomSourceSystemKeyGenerator.New("NNNNNNNNNN");
            string randomText = Guid.NewGuid().ToString();
            DateTime randomDate = DateTime.UtcNow;
            decimal balance = (decimal)(new Random()).NextDouble() * 1000;
            decimal vtAmount = (decimal)(new Random()).NextDouble() * 1000;
            int visitId = (new Random()).Next();

            Guarantor guarantor = this.SetupGuarantor(98765, 12345, 1, "Guarantor");

            Visit unmergedVisit;
            VisitTransaction unmergedVisitTransaction;
            string vtSourceSystemKey = RandomSourceSystemKeyGenerator.New("NNNNNNNNNN");

            ChangeSetMessage visitChangeSetDataChangedMessage = new ChangeSetMessage
            {
                HsGuarantors = new List<HsGuarantorDto>
                {
                    new HsGuarantorDto
                    {
                        HsBillingSystemId = guarantor.HsGuarantorMaps[0].BillingSystem.BillingSystemId,
                        SourceSystemKey = guarantor.HsGuarantorMaps[0].SourceSystemKey,
                        HsGuarantorId = guarantor.HsGuarantorMaps[0].HsGuarantorId,
                        VpEligible = true
                    }
                },
                VpGuarantorHsMatches = new List<VpGuarantorHsMatchDto>
                {
                    new VpGuarantorHsMatchDto
                    {
                        VpGuarantorId = guarantor.VpGuarantorId,
                        HsGuarantorId = guarantor.HsGuarantorMaps[0].HsGuarantorId,
                        HsGuarantorMatchStatus = HsGuarantorMatchStatusEnum.Matched
                    }
                },
                VisitChanges = this.SetupVisitWithChangeDtos(guarantor, sourceSystemKey, randomText, randomDate, balance, visitId, out unmergedVisit),
                VisitTransactionChanges = this.SetupVisitTransactionChangeDtos(vtSourceSystemKey, randomText, randomDate, vtAmount, vpTransactionTypeIdItShouldMergeTo, visitId, out unmergedVisitTransaction)
            };
            unmergedVisitTransaction.SetTransactionAmount(0.01m);
            unmergedVisitTransaction.VpTransactionType = new VpTransactionType() { VpTransactionTypeId = 1 };
            unmergedVisitTransaction.SetSourceSystemKey(vtSourceSystemKey);
            unmergedVisitTransaction.BillingSystem = new BillingSystem() { BillingSystemId = 0 };

            this._visitCreationApplicationService.MergeOrCreateVisitsFromVisitChangeSet(visitChangeSetDataChangedMessage);

            Assert.AreEqual(randomDate, unmergedVisitTransaction.TransactionDate);
            Assert.AreEqual(vpTransactionTypeIdItShouldMergeTo, unmergedVisitTransaction.VpTransactionType.VpTransactionTypeId);
            Assert.AreEqual(randomText, unmergedVisitTransaction.TransactionDescription);

            Assert.AreEqual(3, unmergedVisitTransaction.VisitTransactionAmounts.Count);
            Assert.AreEqual(balance, unmergedVisitTransaction.VisitTransactionAmount.TransactionAmount);
            Assert.AreNotEqual(balance, unmergedVisitTransaction.VisitTransactionAmounts[1].TransactionAmount);
        }


        [TestCase(false, true, false, "AddExternalPrincipalAmountDue should not get called because feature is not enabled")]
        [TestCase(true,  false, false, "AddExternalPrincipalAmountDue should not get called because ReassessmentExempt was false")]
        [TestCase(true,  true, true, "AddExternalPrincipalAmountDue should have been called but did not")]
        public void AddExternalPrincipalAmountDue_Is_Called_With_an_External_Payment(bool isFeatureEnabled, bool reassessmentExempt,   bool shouldBeCalled, string errorMessage)
        {
            int vpTransactionTypeId = 1;
            string sourceSystemKey = RandomSourceSystemKeyGenerator.New("NNNNNNNNNN");
            string randomText = Guid.NewGuid().ToString();
            DateTime randomDate = DateTime.UtcNow;
            decimal balance = 1000m;
            decimal vtAmount = -100m;
            int visitId = (new Random()).Next();

            Guarantor guarantor = this.SetupGuarantor(98765, 12345, 1, "Guarantor");
            string vtSourceSystemKey = RandomSourceSystemKeyGenerator.New("NNNNNNNNNN");

            Visit visit;
            VisitTransaction unmergedVisitTransaction;
            ChangeSetMessage visitChangeSetDataChangedMessage = new ChangeSetMessage
            {
                HsGuarantors = new List<HsGuarantorDto>
                {
                    new HsGuarantorDto
                    {
                        HsBillingSystemId = guarantor.HsGuarantorMaps[0].BillingSystem.BillingSystemId,
                        SourceSystemKey = guarantor.HsGuarantorMaps[0].SourceSystemKey,
                        HsGuarantorId = guarantor.HsGuarantorMaps[0].HsGuarantorId,
                        VpEligible = true
                    }
                },
                VpGuarantorHsMatches = new List<VpGuarantorHsMatchDto>
                {
                    new VpGuarantorHsMatchDto
                    {
                        VpGuarantorId = guarantor.VpGuarantorId,
                        HsGuarantorId = guarantor.HsGuarantorMaps[0].HsGuarantorId,
                        HsGuarantorMatchStatus = HsGuarantorMatchStatusEnum.Matched
                    }
                },
                VisitChanges = this.SetupVisitWithChangeDtos(guarantor, sourceSystemKey, randomText, randomDate, balance, visitId, out visit),
                VisitTransactionChanges = this.SetupVisitTransactionChangeDtos(vtSourceSystemKey, randomText, randomDate, vtAmount, vpTransactionTypeId, visitId, out unmergedVisitTransaction)

            };

            if (reassessmentExempt)
            {
                VisitTransactionChangeDto visitTransactionChange = visitChangeSetDataChangedMessage.VisitTransactionChanges.FirstOrDefault();
                if (visitTransactionChange != null)
                {
                   visitTransactionChange.ReassessmentExempt = true; 
                }
                
            }
            // we are trying to add a new transaction, not update, undo setup moq from SetupVisitTransactionChangeDtos
            this._moqVisitTransactionService.Setup(t => t.GetBySourceSystemKey(sourceSystemKey, It.IsAny<int>()))
                .Returns((VisitTransaction) null);
            this._moqVisitTransactionService.Setup(t => t.GetBySourceSystemKey(It.IsAny<List<string>>(), It.IsAny<int>()))
                .Returns(new List<VisitTransaction>());
            this._moqVisitTransactionService.Setup(t => t.GetBySourceSystemKeyUnmatch(It.IsAny<List<string>>(), It.IsAny<int>()))
                .Returns(new List<VisitTransaction>());

            // feature is enabled
            this._moqFeatureService.Setup(t => t.IsFeatureEnabled(VisitPayFeatureEnum.FeatureExternalFinancePlanPaymentEnabled))
                .Returns(isFeatureEnabled);

            // Generate a VisitTransactionId upon saving (will be used to verify parameters passed into AddExternalPrincipalAmountDue)
            int visitTransactionId = 5;
            this._moqVisitTransactionService.Setup(x => x.SaveVisitTransaction(It.IsAny<Visit>(), It.IsAny<VisitTransaction>()))
                .Callback((Visit v,VisitTransaction t) => t.VisitTransactionId = visitTransactionId);

            this._visitCreationApplicationService.MergeOrCreateVisitsFromVisitChangeSet(visitChangeSetDataChangedMessage);

            Times time = shouldBeCalled ? Times.AtLeastOnce() : Times.Never();
            this._moqFinancePlanService.Verify(x => 
                x.AddExternalPrincipalAmountDue(vtAmount, visit.VisitId, visitTransactionId),
                time, errorMessage);
        }







        [Ignore(ObsoleteDescription.VisitStateMachineTest )]
        [Test]
        public void When_Matching_MergingVisitTransactions_UpdatedHsCurrentBalanceFieldToNotZero()
        {
            /*
             * Should merge the following fields:
                Update HsCurrentBalance to Zero
            
             */
             /*
            VisitState visitState = new VisitState() { VisitStateId = (int)VisitStateEnum.NotSet };
            this._moqVisitStateService.Setup(t => t.GetDefaultVisitState()).Returns(visitState);
            this._visitService = new VisitService(
                new Lazy<IDomainServiceCommonService>(() => this._domainServiceCommonService),
                new Lazy<IVisitRepository>(() => this._moqVisitRepository.Object),
                new Lazy<IVisitStateService>(() => this._visitStateService));

            Guarantor guarantor = this.SetupGuarantor(98765, 12345, 1, "Guarantor");

            Visit unmergedVisit = new Visit
            {
                SourceSystemKey = "123",
                VisitId = 55,
                VPGuarantor = guarantor,
                CurrentBalance = 0.00m,
                HsGuarantorMap = guarantor.HsGuarantorMaps[0]
            };
            unmergedVisit.SetCurrentBalance(1.00m);
            //unmergedVisit.SetVisitState(new VisitState { VisitStateId = (int)VisitStateEnum.PendingClosure });

            this._moqVisitRepository.Setup(t => t.GetBySourceSystemKey("123", 1)).Returns(unmergedVisit);

            List<VisitChangeDto> visitChanges = new List<VisitChangeDto>
            {
                new VisitChangeDto
                {
                    VisitId = 55,
                    SourceSystemKey = "123",
                    HsCurrentBalance = 2.00m,
                    HsGuarantorId = 12345
                }
            };
            this._moqGuarantorService.Setup(t => t.GetGuarantorsFromIntList(It.IsAny<IList<int>>())).Returns(new List<Guarantor> { guarantor });
            ChangeSetMessage visitChangeSetDataChangedMessage = new ChangeSetMessage
            {
                HsGuarantors = new List<HsGuarantorDto>
                {
                    new HsGuarantorDto
                    {
                        HsBillingSystemId = guarantor.HsGuarantorMaps[0].BillingSystem.BillingSystemId,
                        SourceSystemKey = guarantor.HsGuarantorMaps[0].SourceSystemKey,
                        HsGuarantorId = guarantor.HsGuarantorMaps[0].HsGuarantorId,
                    }
                },
                VpGuarantorHsMatches = new List<VpGuarantorHsMatchDto>
                {
                    new VpGuarantorHsMatchDto
                    {
                        VpGuarantorId = guarantor.VpGuarantorId,
                        HsGuarantorId = guarantor.HsGuarantorMaps[0].HsGuarantorId,
                        HsGuarantorMatchStatus = HsGuarantorMatchStatusEnum.Matched
                    }
                },
                VisitChanges = visitChanges,
                VisitTransactionChanges = null,
            };

            this._visitCreationApplicationService.MergeOrCreateVisitsFromVisitChangeSet(visitChangeSetDataChangedMessage).Wait();

            Assert.AreNotEqual(unmergedVisit.VisitState.VisitStateId, (byte)VisitStateEnum.PaidInFull);
            */
        }

        [Ignore(ObsoleteDescription.VisitStateMachineTest)]
        [Test]
        public void When_Matching_MergingVisitTransactions_UpdatedHsCurrentBalanceFieldToZero()
        {
            /*
             * Should merge the following fields:
                Update HsCurrentBalance to Zero
            
             */
             /*
            IVisitStateService visitStateService = new VisitStateService(
                new Lazy<IDomainServiceCommonService>(() => this._domainServiceCommonService),
                new Lazy<IVisitRepository>(() => this._moqVisitRepository.Object),
                new Lazy<IVisitStateMachineService>(() => this._visitStateMachineServiceMock.Object));

            this._visitService = new VisitService(
                new Lazy<IDomainServiceCommonService>(() => this._domainServiceCommonService),
                new Lazy<IVisitRepository>(() => this._moqVisitRepository.Object),
                new Lazy<IVisitStateService>(() => this._visitStateService));

            this._visitCreationApplicationService = new VisitCreationApplicationService(
                new Lazy<IApplicationServiceCommonService>(() => this._applicationServiceCommonService),
                this._moqSession.Object,
                new Lazy<IVisitService>(() => this._visitService),
                new Lazy<IGuarantorService>(() => this._moqGuarantorService.Object),
                new Lazy<IFinancePlanService>(() => this._moqFinancePlanService.Object),
                new Lazy<IVisitTransactionService>(() => this._moqVisitTransactionService.Object),
                new Lazy<IPaymentAllocationService>(() => this._moqPaymentAllocationService.Object),
                new Lazy<IVisitStateService>(() => visitStateService),
                new Lazy<IMergeExceptionService>(() => this._moqMergeExceptionService.Object),
                new Lazy<IInsuranceService>(() => this._moqInsuranceService.Object),
                new Lazy<IVisitPayUserJournalEventService>(() => this._moqVisitPayUserJournalEventService.Object),
                this._unmatchingService,
                new Lazy<IVisitTransactionFilterService>(() => this._moqVisitTransactionFilterService.Object),
                new Lazy<ISystemMessageVisitPayUserService>(() => this._moqSystemMessageVisitPayUserService.Object));

            Guarantor guarantor = this.SetupGuarantor(98765, 12345, 1, "Guarantor");

            Visit unmergedVisit = new Visit
            {
                SourceSystemKey = "123",
                VisitId = 55,
                VPGuarantor = guarantor,
                CurrentBalance = 0.00m,
                HsGuarantorMap = guarantor.HsGuarantorMaps[0]
            };
            unmergedVisit.SetCurrentBalance(1.00m);
            //unmergedVisit.SetVisitState(new VisitState { VisitStateId = (int)VisitStateEnum.PendingClosure });

            this._moqVisitRepository.Setup(t => t.GetBySourceSystemKey("123", It.IsAny<int>())).Returns(unmergedVisit);

            List<VisitChangeDto> visitChanges = new List<VisitChangeDto>
            {
                new VisitChangeDto
                {
                    VisitId = 55,
                    SourceSystemKey = "123",
                    HsCurrentBalance = 0.00m,
                    HsGuarantorId = 12345
                }
            };
            this._moqGuarantorService.Setup(t => t.GetGuarantorsFromIntList(It.IsAny<IList<int>>())).Returns(new List<Guarantor> { guarantor });
            ChangeSetMessage visitChangeSetDataChangedMessage = new ChangeSetMessage
            {
                HsGuarantors = new List<HsGuarantorDto>
                {
                    new HsGuarantorDto
                    {
                        HsBillingSystemId = guarantor.HsGuarantorMaps[0].BillingSystem.BillingSystemId,
                        SourceSystemKey = guarantor.HsGuarantorMaps[0].SourceSystemKey,
                        HsGuarantorId = guarantor.HsGuarantorMaps[0].HsGuarantorId,
                    }
                },
                VpGuarantorHsMatches = new List<VpGuarantorHsMatchDto>
                {
                    new VpGuarantorHsMatchDto
                    {
                        VpGuarantorId = guarantor.VpGuarantorId,
                        HsGuarantorId = guarantor.HsGuarantorMaps[0].HsGuarantorId,
                        HsGuarantorMatchStatus = HsGuarantorMatchStatusEnum.Matched
                    }
                },
                VisitChanges = visitChanges,
                VisitTransactionChanges = null,
            };
            this._visitCreationApplicationService.MergeOrCreateVisitsFromVisitChangeSet(visitChangeSetDataChangedMessage).Wait();

            Assert.AreEqual(unmergedVisit.VisitState.VisitStateId, (byte)VisitStateEnum.PaidInFull);
            */
        }

        [Test]
        public void When_Matching_MergingVisit_UpdatedCorrectFields()
        {
            /*
             * Should merge the following fields:
                BillingApplication
                VisitDescription
                PatientFirstName
                PatientLastName
                DischargeDate
                HsCurrentBalance
                SelfPayClassId*/
            string sourceSystemKey = Guid.NewGuid().ToString();
            DateTime dischargeDate = DateTime.UtcNow;
            string mergeText = RandomSourceSystemKeyGenerator.New("NNNNNNNNNN");
            decimal balance = (decimal)(new Random()).NextDouble() * 1000;
            Visit unmergedVisit;

            Guarantor guarantor = this.SetupGuarantor(98765, 12345, 1, "Guarantor");

            ChangeSetMessage changeSetMessage = new ChangeSetMessage
            {
                HsGuarantors = new List<HsGuarantorDto>
                {
                    new HsGuarantorDto
                    {
                        HsBillingSystemId = guarantor.HsGuarantorMaps[0].BillingSystem.BillingSystemId,
                        SourceSystemKey = guarantor.HsGuarantorMaps[0].SourceSystemKey,
                        HsGuarantorId = guarantor.HsGuarantorMaps[0].HsGuarantorId,
                        VpEligible = true
                    }
                },
                VpGuarantorHsMatches = new List<VpGuarantorHsMatchDto>
                {
                    new VpGuarantorHsMatchDto
                    {
                        VpGuarantorId = guarantor.VpGuarantorId,
                        HsGuarantorId = guarantor.HsGuarantorMaps[0].HsGuarantorId,
                        HsGuarantorMatchStatus = HsGuarantorMatchStatusEnum.Matched
                    }
                },
                VisitChanges = this.SetupVisitWithChangeDtos(guarantor, sourceSystemKey, mergeText, dischargeDate, balance, 55, out unmergedVisit),
                VisitTransactionChanges = null,
            };

            this._visitCreationApplicationService.MergeOrCreateVisitsFromVisitChangeSet(changeSetMessage);

            Assert.AreEqual(unmergedVisit.BillingApplication, mergeText);
            Assert.AreEqual(unmergedVisit.VisitDescription, mergeText);
            Assert.AreEqual(unmergedVisit.PatientFirstName, mergeText);
            Assert.AreEqual(unmergedVisit.PatientLastName, mergeText);
            Assert.AreEqual(unmergedVisit.DischargeDate, dischargeDate);
            Assert.AreEqual(unmergedVisit.CurrentBalance, balance);
        }
    }
}
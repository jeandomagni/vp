﻿namespace Ivh.Application.Tests.Core.Utilities
{
    using System;
    using System.Data;
    using System.Data.Common;
    using NHibernate;
    using NHibernate.Engine;
    using NHibernate.SqlTypes;
    using NHibernate.UserTypes;

    public class NormalizedDateTimeUserType : IUserType
    {
        private readonly TimeZoneInfo _databaseTimeZone = TimeZoneInfo.Local;

        public Type ReturnedType => typeof(DateTime);

        public SqlType[] SqlTypes => new[] {new SqlType(DbType.DateTime)};
        
        public new bool Equals(object x, object y)
        {
            return object.Equals(x, y);
        }

        public int GetHashCode(object x)
        {
            return x.GetHashCode();
        }

        public object NullSafeGet(DbDataReader rs, string[] names, ISessionImplementor session, object owner)
        {
            object r = rs[names[0]];
            if (r == DBNull.Value)
            {
                return null;
            }

            DateTime storedTime = (DateTime)r;
            return storedTime;
        }

        public void NullSafeSet(DbCommand cmd, object value, int index, ISessionImplementor session)
        {
            if (value == null)
            {
                NHibernateUtil.DateTime.NullSafeSet(cmd, null, index, session);
            }
            else
            {
                DateTime dateTimeOffset = (DateTime)value;

                IDataParameter parameter = (IDataParameter)cmd.Parameters[index];
                parameter.Value = dateTimeOffset;
            }
        }

        public object DeepCopy(object value)
        {
            return value;
        }

        public object Replace(object original, object target, object owner)
        {
            throw new NotImplementedException();
        }

        public object Assemble(object cached, object owner)
        {
            throw new NotImplementedException();
        }

        public object Disassemble(object value)
        {
            throw new NotImplementedException();
        }

        public bool IsMutable => false;
    }
}
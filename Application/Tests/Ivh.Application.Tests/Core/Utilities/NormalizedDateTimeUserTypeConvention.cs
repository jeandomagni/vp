namespace Ivh.Application.Tests.Core.Utilities
{
    using FluentNHibernate.Conventions;

    public class NormalizedDateTimeUserTypeConvention : UserTypeConvention<NormalizedDateTimeUserType>
    {
    }
}
﻿namespace Ivh.Application.Tests.Core.Utilities
{
    using FluentNHibernate.Conventions;

    /// <summary>
    /// stores DateTime types as Ticks in SqlLite to preserve their full precision
    /// </summary>
    public class SqlLiteDateTime2UserTypeConvention : UserTypeConvention<SqlLiteDateTime2UserType>
    {
    }
}

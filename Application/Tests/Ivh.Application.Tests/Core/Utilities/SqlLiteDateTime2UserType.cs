﻿namespace Ivh.Application.Tests.Core.Utilities
{
    using System;
    using System.Data;
    using System.Data.Common;
    using NHibernate;
    using NHibernate.Engine;
    using NHibernate.SqlTypes;
    using NHibernate.UserTypes;

    /// <summary>
    /// stores DateTime types as Ticks in SqlLite to preserve their full precision
    /// </summary>
    public class SqlLiteDateTime2UserType : IUserType
    {
        public SqlType[] SqlTypes => new[] { new SqlType(DbType.Int64) };
        public Type ReturnedType => typeof(DateTime);
        public bool IsMutable => false;

        public new bool Equals(object x, object y)
        {
            return object.Equals(x, y);
        }

        public int GetHashCode(object x)
        {
            return x.GetHashCode();
        }

        public object NullSafeGet(DbDataReader rs, string[] names, ISessionImplementor session, object owner)
        {
            object r = rs[names[0]];
            if (r == DBNull.Value)
            {
                return null;
            }

            DateTime storedTime = new DateTime((long)r);
            return storedTime;
        }

        public void NullSafeSet(DbCommand cmd, object value, int index, ISessionImplementor session)
        {
            if (value == null)
            {
                NHibernateUtil.DateTime.NullSafeSet(cmd, null, index, session);
            }
            else
            {
                long ticks = ((DateTime)value).Ticks;

                IDataParameter parameter = (IDataParameter)cmd.Parameters[index];
                parameter.Value = ticks;
            }
        }

        public object DeepCopy(object value)
        {
            return value;
        }

        public object Replace(object original, object target, object owner)
        {
            throw new NotImplementedException();
        }

        public object Assemble(object cached, object owner)
        {
            throw new NotImplementedException();
        }

        public object Disassemble(object value)
        {
            throw new NotImplementedException();
        }

        
    }
}

﻿namespace Ivh.Application.Tests.Core
{
    using System;
    using System.Collections.Generic;
    using System.Drawing;
    using System.IO;
    using System.Linq;
    using Application.Core.Common.Utilities.Builders;
    using Common.VisitPay.Strings;
    using NUnit.Framework;
    using OfficeOpenXml;
    using Properties;

    public class IvinciExcelExportBuilderTests : ApplicationTestBase
    {

        /// <summary>
        ///     This method serves as a catch-all, saving the excel file to inspect and to demostrate usage.
        ///     It can  be used to assist in development or troubleshooting
        ///     Usage: uncomment the [Test] Attribute and add a C:\Temp\ directory (or update the path variable)
        /// </summary>
        //[Test]
        public void ProofOfConcept_testing()
        {
            using (ExcelPackage package = new ExcelPackage())
            {
                //https://github.com/pruiz/EPPlus/blob/master/EPPlusTest/DrawingTest.cs
                Bitmap logo = Resources.logo_for_excel_export;

                List<TestObject> testSubjects = this.GetTestSubjects(10);

                iVinciExcelExportBuilder builder = new iVinciExcelExportBuilder(package, "test title foo");

                builder.WithWorkSheetNameOf("worksheet")
                    .WithLogo(logo)
                    .AddInfoBlockRow(
                        new ColumnBuilder("SomeVal1", "Value Align Right").WithStyle(StyleTypeEnum.AlignRight)
                    )
                    .AddInfoBlockRow(
                        new ColumnBuilder(DateTime.UtcNow, "Date Value Align Left").WithStyle(StyleTypeEnum.DateFormatter, "mm/dd/yyyy").WithStyle(StyleTypeEnum.AlignLeft)
                    )
                    .AddBlankInfoBlockRow()
                    .AddInfoBlockRow(
                        new ColumnBuilder("Middle Man", "Value Align Center").WithStyle(StyleTypeEnum.AlignCenter)
                    )
                    .AddInfoBlockRow(
                        new ColumnBuilder(1234.98, "Money Format").WithStyle(StyleTypeEnum.NumberFormatter, Format.ExportMoneyFormat)
                    )
                    .AddDataBlock(testSubjects, subject => new List<Column>
                    {
                        new ColumnBuilder(subject.Id,"ID").WithStyle(StyleTypeEnum.AlignLeft),
                        new ColumnBuilder(subject.Name,"Name"),
                        new ColumnBuilder(subject.Money,"Money").WithStyle(StyleTypeEnum.NumberFormatter, Format.ExportMoneyFormat).WithStyle(StyleTypeEnum.Bold),
                        new ColumnBuilder(subject.TheDate,"The Date").WithStyle(StyleTypeEnum.DateFormatter, "mm/dd/yyyy")
                    })
                    .WithNoRecordsFoundMessageOf("foo")
                    .Build();

                string path = @"C:\Temp\test1.xlsx";
                using (Stream stream = File.Create(path))
                {
                    package.SaveAs(stream);
                    stream.Close();
                }
            }
        }

        [Test]
        public void When_no_there_are_no_data_records_a_default_message_should_show()
        {
            using (ExcelPackage package = new ExcelPackage())
            {
                List<TestObject> testSubjects = this.GetTestSubjects(0);

                iVinciExcelExportBuilder builder = new iVinciExcelExportBuilder(package, "test title foo");
                builder.AddDataBlock(testSubjects, subject => new List<Column>
                    {
                        new ColumnBuilder(subject.Id,"ID").WithStyle(StyleTypeEnum.AlignLeft)
                    })
                    .Build();

                WorksheetInfo worksheetInfo = builder.PostBuildWorksheetInfo();
                string noDataMessageAddress = $"A{worksheetInfo.DataBlockStartIndex}";
                object noDataMessageValue = package.Workbook.Worksheets.First().Cells[noDataMessageAddress].Value;

                Assert.AreEqual("There are no records to display at this time.", noDataMessageValue);
            }
        }

        [Test]
        public void When_no_there_are_no_data_records_shows_custom_message_with_custom_message_provided()
        {
            string customNoDatamessage = "No data!";
            using (ExcelPackage package = new ExcelPackage())
            {
                List<TestObject> testSubjects = this.GetTestSubjects(0);

                iVinciExcelExportBuilder builder = new iVinciExcelExportBuilder(package, "test title");
                builder.AddDataBlock(testSubjects, subject => new List<Column>
                    {
                        new ColumnBuilder(subject.Id,"ID").WithStyle(StyleTypeEnum.AlignLeft)
                    })
                    .WithNoRecordsFoundMessageOf(customNoDatamessage)
                    .Build();

                WorksheetInfo worksheetInfo = builder.PostBuildWorksheetInfo();
                string noDataMessageAddress = $"A{worksheetInfo.DataBlockStartIndex}";
                object noDataMessageValue = package.Workbook.Worksheets.First().Cells[noDataMessageAddress].Value;

                Assert.AreEqual(customNoDatamessage, noDataMessageValue);
            }
        }

        [Test]
        public void When_incomplete_download_shows_incomplete_download_message()
        {
            string expectedIncompleteDownloadMessage = "Incomplete download. 10,000 rows of 10,001 rows were downloaded. Only 10,000 rows can be downloaded.";
            using (ExcelPackage package = new ExcelPackage())
            {
                List<TestObject> testSubjects = this.GetTestSubjects(10001);

                iVinciExcelExportBuilder builder = new iVinciExcelExportBuilder(package, "test title");
                builder.AddDataBlock(testSubjects, subject => new List<Column>
                    {
                        new ColumnBuilder(subject.Id,"ID").WithStyle(StyleTypeEnum.AlignLeft)
                    })
                    .Build();

                WorksheetInfo worksheetInfo = builder.PostBuildWorksheetInfo();
                string incompleteDownloadMessageAddress = $"A{worksheetInfo.IncompleteDataMessageRowIndex}";
                object actualIncompleteDownloadMessage = package.Workbook.Worksheets.First().Cells[incompleteDownloadMessageAddress].Value;

                Assert.AreEqual(expectedIncompleteDownloadMessage, actualIncompleteDownloadMessage);
            }
        }

        [Test]
        public void Should_throw_error_when_no_excelpackage_is_provided()
        {
            string excelPackageIsRequired = "ExcelPackage is required and should be passed in inside a using statement";

            Exception explode = Assert.Throws<Exception>(() =>
            {
                iVinciExcelExportBuilder builder = new iVinciExcelExportBuilder(null, "test title");
            });

            Assert.That(explode.Message, Is.EqualTo(excelPackageIsRequired));
        }

        [Test]
        public void Should_throw_error_when_no_title_is_provided()
        {
            string titleIsRequired = "A title is required.";

            Exception kaboom = Assert.Throws<Exception>(() =>
            {
                using (ExcelPackage package = new ExcelPackage())
                {
                    iVinciExcelExportBuilder builder = new iVinciExcelExportBuilder(package, "");
                }
            });

            Assert.That(kaboom.Message, Is.EqualTo(titleIsRequired));
        }

        private List<TestObject> GetTestSubjects(int numberOfSubjects)
        {
            List<TestObject> testObjects = new List<TestObject>();
            //Todo: user faker.net
            foreach (int i in Enumerable.Range(1, numberOfSubjects))
                testObjects.Add(new TestObject {Id = i, Name = "Foo", Money = 100.50M, TheDate = DateTime.Today});

            return testObjects;
        }

        public class TestObject
        {
            public string Name { get; set; }
            public int Id { get; set; }
            public decimal Money { get; set; }
            public DateTime TheDate { get; set; }
        }

    }
}
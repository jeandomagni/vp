﻿namespace Ivh.Application.Tests.Core
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Application.Base.Common.Interfaces;
    using Application.Core.Common.Interfaces;
    using AutoMapper;
    using Common.Base.Utilities.Extensions;
    using Common.ServiceBus.Interfaces;
    using Common.Tests.Helpers;
    using Common.Tests.Helpers.Visit;
    using Common.Tests.Helpers.VpStatement;
    using Common.VisitPay.Enums;
    using Common.VisitPay.Messages.FinanceManagement;
    using Domain.Guarantor.Entities;
    using Domain.Visit.Entities;
    using Domain.Visit.Interfaces;
    using Domain.Visit.Services;
    using Domain.FinanceManagement.Statement.Entities;
    using Moq;
    using NUnit.Framework;

    [TestFixture]
    public class VisitBillingHoldApplicationServiceTests : ApplicationTestBase
    {
        [Ignore("Revisit when VP3 state machine is defined")]
        //[TestCase(4, false, VisitStateEnum.PendingUncollectable)]
        //[TestCase(4, true, VisitStateEnum.PastDue)]
        //[TestCase(3, false, VisitStateEnum.PastDue)]
        //[TestCase(3, true, VisitStateEnum.PastDue)]
        //[TestCase(2, false, VisitStateEnum.PastDue)]
        //[TestCase(2, true, VisitStateEnum.StatementedGoodStanding)]
        //[TestCase(1, false, VisitStateEnum.PastDue)]
        //[TestCase(1, true, VisitStateEnum.PendingStatement)]
        public void SuspendThenUnsuspendVisit(AgingTierEnum agingTier, bool isGracePeriod, VisitStateEnum expectedFinalState)
        {
            DateTime insertDate = DateTime.UtcNow.AddDays(-10);

            Tuple<Visit, IList<VisitTransaction>> visit = VisitFactory.GenerateActiveVisit(1234, 1, insertDate: insertDate);
            VisitFactory.SetStatusFromAgingCount(visit, agingTier);
            visit.Item1.VPGuarantor = new Guarantor { VpGuarantorId = 1 };
            foreach (VisitTransaction transaction in visit.Item2)
            {
                transaction.InsertDate = insertDate;
            }

            int counter1 = 0;
            foreach (VisitStateHistory status in visit.Item1.VisitStates.OrderBy(x => x.DateTime))
            {
                status.DateTime = insertDate.AddMinutes(counter1);
                counter1++;
            }

            int counter2 = 0;
            foreach (VisitAgingHistory aging in visit.Item1.VisitAgingHistories.OrderBy(x => x.InsertDate))
            {
                aging.InsertDate = insertDate.AddMinutes(counter2);
                counter2++;
            }

            VisitStateEnum originalState = visit.Item1.CurrentVisitState;

            VpStatement statement = VpStatementFactory.CreateStatement(visit.ToListOfOne().ToList());
            statement.PaymentDueDate = DateTime.UtcNow.AddDays(isGracePeriod ? 1 : -1);

            //
            IVisitBillingHoldApplicationService visitBillingHoldApplicationService;
            IVisitStateApplicationService visitStateApplicationService;

            //
            this.CreateServices(visit, statement, isGracePeriod, out visitBillingHoldApplicationService, out visitStateApplicationService);

            //
            visitBillingHoldApplicationService.AddBillingHold(visit.Item1.BillingSystemId.Value, visit.Item1.SourceSystemKey);

            // the visit is suspended
            Assert.AreEqual(true, visit.Item1.BillingHold);

            //

            visitBillingHoldApplicationService.ClearBillingHold(visit.Item1.BillingSystemId.Value, visit.Item1.SourceSystemKey);

            // the status matches what's expected after unsuspend
            Assert.AreEqual(expectedFinalState, visit.Item1.CurrentVisitState);

            bool softDeleted = statement.VpStatementVisits[0].DeletedDate != null;
            //if (expectedFinalStatus == VisitStateEnum.PendingStatement)
            //{
            //    Assert.True(softDeleted);
            //}
            //else
            {
                Assert.False(softDeleted);
            }
        }

        [TestCase(true, true)]
        [TestCase(true, false)]
        [TestCase(false, true)]
        [TestCase(false, false)]
        public void WhenVisitSuspends_FinancePlanEmailSends(bool billingHold, bool isVisitFinanced)
        {
            bool didSendEmail = false;

            Visit visit = new Visit
            {
                VisitId = 1,
                VPGuarantor = new Guarantor { VpGuarantorId = 1 },
                SourceSystemKey = "1234",
                BillingSystem = new BillingSystem() { BillingSystemId = 1 }
            };

            VisitBillingHoldApplicationServiceMockBuilder mockBuilder = new VisitBillingHoldApplicationServiceMockBuilder();
            mockBuilder.VisitServiceMock.Setup(x => x.GetVisit(It.IsAny<int>(), It.IsAny<int>())).Returns(visit);
            mockBuilder.VisitServiceMock.Setup(x => x.GetVisitBySystemSourceKey(It.IsAny<string>(), It.IsAny<int>())).Returns(visit);
            mockBuilder.FinancePlanServiceMock.Setup(x => x.IsVisitInAFinancePlan(It.IsAny<Visit>())).Returns(isVisitFinanced);
            mockBuilder.SessionMock = new SessionMockBuilder()
                .UseSynchronizations() // this lets the RegisterPostCommits fire
                .CreateMock();

            // mock bus to catch message publishing
            Mock<IBus> mockBus = new Mock<IBus>();
            mockBus.Setup(x => x.PublishMessage(It.IsAny<SendFinancePlanVisitSuspendedEmailMessage>())).Returns(() =>
            {
                didSendEmail = true;
                return Task.CompletedTask;
            });
            mockBuilder.ApplicationServiceCommonServiceMock = new Mock<IApplicationServiceCommonService>();
            mockBuilder.ApplicationServiceCommonServiceMock.Setup(x => x.Bus).Returns(new Lazy<IBus>(() => mockBus.Object));

            IVisitBillingHoldApplicationService svc = mockBuilder.CreateService();
            svc.AddBillingHold(visit.BillingSystemId.Value, visit.SourceSystemKey);

            Assert.AreEqual(isVisitFinanced && visit.BillingHold, didSendEmail);
        }

        private void CreateServices(Tuple<Visit, IList<VisitTransaction>> visit, VpStatement statement, bool isGracePeriod, out IVisitBillingHoldApplicationService visitBillingHoldApplicationService, out IVisitStateApplicationService visitStateApplicationService)
        {
            Mock<IVisitRepository> visitRepositoryMock = new VisitRepositoryMockBuilder().CreateMock(b =>
            {
                b.Setup(x => x.GetStatementVisitStateModel(It.IsAny<Visit>())).Returns(() => new StatementVisitStatusModel
                {
                    IsInGracePeriod = isGracePeriod,
                    StatementPeriodEndDate = DateTime.UtcNow.AddDays(1)
                });
                b.Setup(x => x.IsOnStatement(It.IsAny<Visit>())).Returns(true);
                b.Setup(x => x.GetVisit(It.IsAny<int>(), It.IsAny<int>())).Returns(visit.Item1);
                b.Setup(x => x.GetById(It.IsAny<int>())).Returns(visit.Item1);
            });

            IVisitService visitService = new VisitServiceMockBuilder().Setup(builder =>
            {
                builder.VisitRepositoryMock = visitRepositoryMock;
            }).CreateService();

            VisitStateService visitStateService = new VisitStateServiceMockBuilder().Setup(builder =>
            {
                builder.VisitRepositoryMock = visitRepositoryMock;
            }).CreateService();



            visitBillingHoldApplicationService = new VisitBillingHoldApplicationServiceMockBuilder().Setup(builder =>
            {
                builder.VisitService = visitService;
            }).CreateService();

            visitStateApplicationService = new VisitStateApplicationServiceMockBuilder().Setup(builder =>
            {
                VpStatementServiceMockBuilder statementBuilder = new VpStatementServiceMockBuilder();
                statementBuilder.VpStatementRepositoryMock.Setup(x => x.GetMostRecentStatement(It.IsAny<Guarantor>())).Returns(statement);

                builder.VisitService = visitService;
            }).CreateService();
        }
    }
}
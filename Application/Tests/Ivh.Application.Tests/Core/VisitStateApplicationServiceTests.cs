﻿namespace Ivh.Application.Tests.Core
{
    using System;
    using System.Collections.Generic;
    using Application.Core.Common.Interfaces;
    using Common.EventJournal.Interfaces;
    using Common.State.Interfaces;
    using Common.Tests.Helpers;
    using Common.Tests.Helpers.Base;
    using Common.Tests.Helpers.Visit;
    using Common.VisitPay.Enums;
    using Common.VisitPay.Messages.Aging;
    using Domain.Settings.Entities;
    using Domain.Visit.Entities;
    using Domain.Visit.Rules;
    using Domain.Visit.Services;
    using Moq;
    using NUnit.Framework;

    [TestFixture]
    public class VisitStateApplicationServiceTests : ApplicationTestBase
    {

        /// <summary>
        ///     * VP-6322 Allow visits to return to VP that have already run their course to bad debt
        ///     * The rules are the visit must be inactive and max age
        ///     * When the visit is reactivated and the re-age feature is enabled the visit should re-age to minimum AgeTier
        ///     *
        ///     * Note: While it may be configured to AgeTier 0, the aging logic may put into AgingTier 1 due to previous
        ///             communications
        /// </summary>
        /// <param name="initialAgingTierEnum">Age of the visit</param>
        /// <param name="initialVisitState">Status of visit</param>
        /// <param name="visitStateFinal">Status it is moving to</param>
        /// <param name="messageSentCount">#number of SetVisitAgeMessage published (what is being tested)</param>
        /// <param name="featureEnabled">Is re-age Feature Enabled</param>

        [TestCase(AgingTierEnum.MaxAge, VisitStateEnum.Inactive, VisitStateEnum.Active, 1, true)]
        [TestCase(AgingTierEnum.MaxAge, VisitStateEnum.Inactive, VisitStateEnum.Active, 0, false)]
        [TestCase(AgingTierEnum.FinalPastDue, VisitStateEnum.Inactive, VisitStateEnum.Active, 0, true)]
        [TestCase(AgingTierEnum.MaxAge, VisitStateEnum.Active, VisitStateEnum.Active, 0, true)]
        public void SetVisitAgeMessage_Is_Sent_Correctly_When_Visit_Status_Changes(AgingTierEnum initialAgingTierEnum, VisitStateEnum initialVisitState, VisitStateEnum visitStateFinal, int messageSentCount, bool featureEnabled)
        {
            #region Setup 

            DateTime insertDate = DateTime.UtcNow.AddDays(-10);
            Tuple<Visit, IList<VisitTransaction>> visitTuple = VisitFactory.GenerateActiveVisit(1234, 1, insertDate: insertDate);
            Visit visit = visitTuple.Item1;
            visit.SetAgingTier(initialAgingTierEnum, "");
            ((IEntity<VisitStateEnum>) visit).SetState(initialVisitState, "");

            VisitServiceMockBuilder visitServiceMockBuilder = new VisitServiceMockBuilder();
            visitServiceMockBuilder.VisitRepositoryMock.Setup(x => x.GetBySourceSystemKey(It.IsAny<string>(), It.IsAny<int>())).Returns(visit);
            VisitStateServiceMockBuilder visitStateService = new VisitStateServiceMockBuilder();
            Mock<IEventJournalService> eventJournalServiceMock = new Mock<IEventJournalService>();
            VisitStateMachineService visitStateMachine = new VisitStateMachineService(null, new VisitStateRules(), new Lazy<IEventJournalService>(() => eventJournalServiceMock.Object));
            visitStateService.VisitStateMachineService = visitStateMachine;
            visitServiceMockBuilder.VisitStateService = visitStateService.CreateService();

            Mock<Client> clientMock = new Mock<Client>();
            clientMock.Setup(t => t.FeatureResetAgingTierForReactivatedMaxAgeVisits).Returns(true);
            clientMock.Setup(t => t.AgingTierWhenReactivatingMaxAgeVisit).Returns(AgingTierEnum.NotStatemented);
            ApplicationServiceCommonServiceMockBuilder applicationServiceCommonServiceMockBuilder = new
                    ApplicationServiceCommonServiceMockBuilder()
                .Setup(x => x.FeatureServiceMock.Setup(f => f.IsFeatureEnabled(It.IsAny<VisitPayFeatureEnum>())).Returns(featureEnabled));
            applicationServiceCommonServiceMockBuilder.ClientServiceMock.Setup(t => t.GetClient()).Returns(clientMock.Object);

            VisitStateApplicationServiceMockBuilder visitStateApplicationServiceMockBuilder = new VisitStateApplicationServiceMockBuilder();
            visitStateApplicationServiceMockBuilder.SessionMock = new SessionMockBuilder().UseSynchronizations().SessionMock;
            visitStateApplicationServiceMockBuilder.VisitService = visitServiceMockBuilder.CreateService();
            visitStateApplicationServiceMockBuilder.ApplicationServiceCommonService = applicationServiceCommonServiceMockBuilder.CreateService();

            IVisitStateApplicationService visitStateApplicationService = visitStateApplicationServiceMockBuilder.CreateService();

            #endregion

            visitStateApplicationService.SetVisitEligible(visit.SourceSystemKey, visit.BillingSystemId.Value);

            Times times = messageSentCount == 1 ? Times.Once() : Times.Never();
            applicationServiceCommonServiceMockBuilder.BusMock.Verify(x => x.PublishMessage(It.IsAny<SetVisitAgeMessage>()), times);
        }
    }
}
﻿namespace Ivh.Application.Tests.Reports
{
    using System;
    using System.Collections.Generic;
    using Application.Core.ApplicationServices;
    using Application.Core.Common.Dtos;
    using Common.Base.Utilities.Extensions;
    using Domain.HospitalData.Analytics.Entities;
    using Domain.HospitalData.Analytics.Interfaces;
    using Moq;
    using NUnit.Framework;

    [TestFixture]
    public class AnalyticReportApplicationServiceTest : ApplicationTestBase
    {
        private Mock<IAnalyticReportService> _analyticReportService;

        [SetUp]
        public void Setup()
        {
            this._analyticReportService = new Mock<IAnalyticReportService>();
        }

        [Test]
        public void FilterAnalyticReports_SourceListEmpty_AnalyticReportsEmpty()
        {
            List<AnalyticReport> emptyList = new List<AnalyticReport>();
            this._analyticReportService.Setup(t => t.GetAnalyticReports()).Returns(() => emptyList);
            List<AnalyticReportDto> analyticReports = new List<AnalyticReportDto>
            {
                new AnalyticReportDto
                {
                    ReportName = "Report 1"
                }
            };
            AnalyticReportApplicationService svc = new AnalyticReportApplicationService(null, new Lazy<IAnalyticReportService>(() => this._analyticReportService.Object));
            IList<AnalyticReportDto> reportlist = svc.FilterAnalyticReportList(analyticReports);
            Assert.IsTrue(reportlist.IsNullOrEmpty());
        }

        [Test]
        public void FilterAnalyticReports_SourceListSubsetOfFilterList_AnalyticReportsEqualsSourceList()
        {
            List<AnalyticReport> analyticReports = new List<AnalyticReport>
            {
                new AnalyticReport
                {
                    ReportName = "Report 1",
                    Visible = true,
                    AnalyticReportClassification = new AnalyticReportClassification { ClassificationName = "Adoption and Use"}
                }
            };
            this._analyticReportService.Setup(t => t.GetAnalyticReports()).Returns(() => analyticReports);
            List<AnalyticReportDto> filteredList = new List<AnalyticReportDto>
            {
                new AnalyticReportDto
                {
                    ReportName = "Report 1",
                    Classification = "Adoption and Use",
                    DisplayName = "Report 1 Display Name"
                },
                new AnalyticReportDto
                {
                    ReportName = "Report 2",
                    Classification = "Adoption and Use",
                    DisplayName = "Report 2 Display Name"
                }
            };
            AnalyticReportApplicationService svc = new AnalyticReportApplicationService(null, new Lazy<IAnalyticReportService>(() => this._analyticReportService.Object));
            IList<AnalyticReportDto> reportlist = svc.FilterAnalyticReportList(filteredList);

            // Check not null
            Assert.IsNotNull(reportlist);
            // Check count
            Assert.IsTrue(reportlist.Count == 1);
            // Report list should only contain Report 1
            Assert.IsTrue(reportlist[0].ReportName.Equals("Report 1"));
        }

        [Test]
        public void FilterAnalyticReportList_SourceListSupersetOfFilterList_AnalyticReportsEqualsFilterList()
        {
            List<AnalyticReport> analyticReports = new List<AnalyticReport>
            {
                new AnalyticReport
                {
                    ReportName = "Report 1",
                    Visible = true,
                    AnalyticReportClassification = new AnalyticReportClassification { ClassificationName = "Adoption and Use"}
                },
                new AnalyticReport
                {
                    ReportName = "Report 2",
                    Visible = true,
                    AnalyticReportClassification = new AnalyticReportClassification { ClassificationName = "Adoption and Use"}
                }
            };
            this._analyticReportService.Setup(t => t.GetAnalyticReports()).Returns(() => analyticReports);
            List<AnalyticReportDto> filteredList = new List<AnalyticReportDto>
            {
                new AnalyticReportDto
                {
                    ReportName = "Report 2",
                    Classification = "Adoption and Use",
                    DisplayName = "Report 2 Display Name"
                }
            };
            AnalyticReportApplicationService svc = new AnalyticReportApplicationService(null, new Lazy<IAnalyticReportService>(() => this._analyticReportService.Object));
            IList<AnalyticReportDto> reportlist = svc.FilterAnalyticReportList(filteredList);

            // Check not null
            Assert.IsNotNull(reportlist);
            // Count should be 1
            Assert.IsTrue(reportlist.Count == 1);
            // Filtered list should only contain Report 2
            Assert.IsTrue(reportlist[0].ReportName.Equals("Report 2"));
        }

    }
}
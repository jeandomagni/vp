﻿namespace Ivh.Application.Tests.Matching
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Common.Base.Utilities.Extensions;
    using Common.Base.Utilities.Helpers;
    using Domain.HospitalData.HsGuarantor.Entities;
    using Domain.HospitalData.Visit.Entities;
    using Domain.Matching.Entities;
    using NHibernate;
    using Facility = Domain.Matching.Entities.Facility;

    public static class MatchDataFactory
    {
        private static int _hsVisitId = 0;

        public static IEnumerable<HsGuarantor> CreateAndSaveHsGuarantorsWithBuilder(Func<HsGuarantor> guarantorBuilder, int nGuarantors, ISession session)
        {
            while (nGuarantors-- > 0)
            {
                HsGuarantor guarantor = guarantorBuilder();
                session.SaveOrUpdate(guarantor);
                session.Flush();
                yield return guarantor;
            }
        }

        public static IEnumerable<Visit> CreateAndSaveHsVisitsForHsGuarantor(ISession session,
            HsGuarantor hsGuarantor,
            int nVisits,
            int nUniquePatients = 1)
        {
            int serviceGroupId = 1;

            Tuple<string, string, string, DateTime>[] patientData = nUniquePatients
                .Select(x => new Tuple<string, string, string, DateTime>(
                    RandomSourceSystemKeyGenerator.New("AAANNNNNNNNN"),
                    RandomSourceSystemKeyGenerator.New("AAAA"),
                    RandomSourceSystemKeyGenerator.New("AAAAAAA"),
                    DateTime.UtcNow.AddYears(-18).AddDays(x)
                    ))
                .ToArray();

            while (nVisits-- > 0)
            {
                int patientIndex = nVisits % patientData.Length;
                Ivh.Domain.HospitalData.Visit.Entities.Visit visit = new Ivh.Domain.HospitalData.Visit.Entities.Visit
                {
                    VisitId = ++_hsVisitId,
                    HsGuarantorId = hsGuarantor.HsGuarantorId,
                    BillingSystemId = 1,
                    SourceSystemKey = patientData[patientIndex].Item1,
                    DataChangeDate = hsGuarantor.DataChangeDate,
                    HsCurrentBalance = 10,
                    InsuranceBalance = 0,
                    SelfPayBalance = 0,
                    PatientFirstName = patientData[patientIndex].Item2,
                    PatientLastName = patientData[patientIndex].Item3,
                    PatientDOB = patientData[patientIndex].Item4,
                    ServiceGroupId = serviceGroupId
                };
                visit.SourceSystemKeyDisplay = visit.SourceSystemKey;

                session.SaveOrUpdate(visit);
                session.Flush();

                Facility facility = session
                    .Query<Facility>()
                    .FirstOrDefault(x => x.HsGuarantorId == hsGuarantor.HsGuarantorId
                        && x.ServiceGroupId == serviceGroupId
                        && x.InsertLoadTrackerId == 1);

                if (facility == null)
                {
                    facility = new Facility()
                    {
                        HsGuarantorId = hsGuarantor.HsGuarantorId,
                        InsertLoadTrackerId = 1,
                        ServiceGroupId = serviceGroupId,
                        StatementIdentifierId = 1
                    };
                }
                session.SaveOrUpdate(facility);

                
                yield return visit;
            }

        }
    }
}

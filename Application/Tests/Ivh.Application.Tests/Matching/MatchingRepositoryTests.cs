﻿namespace Ivh.Application.Tests.Matching
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Common.Base.Utilities.Extensions;
    using Common.Base.Utilities.Helpers;
    using Common.Data.Connection;
    using Common.Data.Connection.Connections;
    using Common.Tests.Builders;
    using Common.Tests.Builders.Matching;
    using Common.VisitPay.Enums;
    using Domain.HospitalData.HsGuarantor.Entities;
    using Domain.Matching.Entities;
    using Domain.Matching.Interfaces;
    using FluentNHibernate.Mapping;
    using NUnit.Framework;
    using Provider.Matching;

    [TestFixture]
    public class MatchingRepositoryTests : RepositoryTestBase
    {

        public MatchingRepositoryTests() : base(m =>
        {
            m.FluentMappings.Add<HsGuarantorMapForTest>();
            m.FluentMappings.Add<VisitMapForTest>();
        }, typeof(MatchRegistry))
        {
        }

        [Test]
        public void MatchInfo_MatchExists_AfterInsert()
        {
            MatchVpGuarantorMatchInfo matchInfo = new MatchVpGuarantorMatchInfo();
            matchInfo.SetMatchSsn("arealssn");
            matchInfo.AddMatch(new MatchRegistry("matchstring","ssk",1));
            matchInfo.VpGuarantorId = 1;
            IMatchVpGuarantorMatchInfoRepository repository = new MatchVpGuarantorMatchInfoRepository(new SessionContext<VisitPay>(this.Session));

            this.Session.Save(matchInfo.Matches.First().Registry);
            repository.Insert(matchInfo);

            // force commit
            this.Session.Flush();

            // insert works
            MatchVpGuarantorMatchInfo matchInfoFetched = repository.GetById(matchInfo.VpGuarantorId);
            Assert.NotNull(matchInfoFetched);

            // exists works
            bool matchExists = repository.ActiveVpGuarantorRegistryMatchExists(matchInfo.Matches.First().Registry.RegistryId);
            Assert.True(matchExists);

            // fetch by natural key works
            string ssk = matchInfo.Matches.First().Registry.SourceSystemKey;
            int hsgId = matchInfo.Matches.First().Registry.HsGuarantorId;
            MatchVpGuarantorMatchInfo matchInfoFetchedByChildId = repository.GetVpGuarantorRegistryMatch(hsgId);
            Assert.NotNull(matchInfoFetchedByChildId);

        }

        [Test]
        public void GetMatchesToQueue()
        {
            HsGuarantor guarantor = this.CreateAndSaveHsGuarantor();
            IMatchGuarantorRepository matchRepository = this.CreateMatchGuarantorRepository();

            IEnumerable<IEnumerable<int>> results = matchRepository.GetMatchesToQueue(DateTime.UtcNow.AddDays(-1),0, false, false, 100);

            Assert.Contains(guarantor.HsGuarantorId, results.SelectMany(x => x).ToList(), "updated hsGuarantor data not found for T-1 day");

            results = matchRepository.GetMatchesToQueue(DateTime.MinValue, 0, false, false, 100);

            Assert.Contains(guarantor.HsGuarantorId, results.SelectMany(x => x).ToList(), "updated hsGuarantor data not found at all");
        }

        [Test]
        public void GetLastUpdateDate()
        {
            IMatchRegistryRepository matchRepository = this.CreateMatchRegistryRepository();
            DateTime[] dates = { DateTime.UtcNow.AddDays(1), DateTime.UtcNow };

            foreach (DateTime dateTime in dates)
            {
                MatchRegistry registry = new MatchRegistry("", "", 1)
                {
                    InsertDate = dateTime,
                    DataChangeDate = dateTime,
                    PartitionKey = 0m,
                    MatchOptionId = MatchOptionEnum.DobLnameSsn4SskZip,
                    PatternSetId = PatternSetEnum.S001,
                    PatternUseId = PatternUseEnum.Initial,
                };

                matchRepository.BulkInsert(registry.ToListOfOne());
            }

            DateTime maxDateRepo = matchRepository.GetLastUpdateDate(PatternSetEnum.S001.ToListOfOne()).Value;
            DateTime maxDateLocal = dates.Max();

            Assert.AreEqual(maxDateLocal, maxDateRepo);
        }

        [Test]
        public void VpGuarantorRegistryMatch_MatchDoesNotExists_UnMatchSoft()
        {
            MatchVpGuarantorMatchInfo matchInfo = new MatchVpGuarantorMatchInfo();
            matchInfo.SetMatchSsn("arealssn");
            matchInfo.AddMatch(new MatchRegistry("matchstring", "ssk", 1));
            matchInfo.VpGuarantorId = 1;
            matchInfo.Matches[0].HsGuarantorMatchStatusId = HsGuarantorMatchStatusEnum.UnmatchedSoft;
            IMatchVpGuarantorMatchInfoRepository repository = new MatchVpGuarantorMatchInfoRepository(new SessionContext<VisitPay>(this.Session));
            this.Session.Save(matchInfo.Matches.First().Registry);
            repository.Insert(matchInfo);
            this.Session.Flush();
            int hsgId = matchInfo.Matches.First().Registry.HsGuarantorId;
            MatchVpGuarantorMatchInfo matchInfoFetchedByChildId = repository.GetVpGuarantorRegistryMatch(hsgId);
            Assert.Null(matchInfoFetchedByChildId);
        }

        [Test]
        public void VpGuarantorRegistryMatch_MatchExists_Matched()
        {
            MatchVpGuarantorMatchInfo matchInfo = new MatchVpGuarantorMatchInfo();
            matchInfo.SetMatchSsn("arealssn");
            matchInfo.AddMatch(new MatchRegistry("matchstring", "ssk", 1));
            matchInfo.VpGuarantorId = 1;
            matchInfo.Matches[0].HsGuarantorMatchStatusId = HsGuarantorMatchStatusEnum.Matched;
            IMatchVpGuarantorMatchInfoRepository repository = new MatchVpGuarantorMatchInfoRepository(new SessionContext<VisitPay>(this.Session));
            this.Session.Save(matchInfo.Matches.First().Registry);
            repository.Insert(matchInfo);
            this.Session.Flush();
            int hsgId = matchInfo.Matches.First().Registry.HsGuarantorId;
            MatchVpGuarantorMatchInfo matchInfoFetchedByChildId = repository.GetVpGuarantorRegistryMatch(hsgId);
            Assert.NotNull(matchInfoFetchedByChildId);
        }

        [Test]
        public void VpGuarantorRegistryMatch_MatchExists_HardMatched()
        {
            MatchVpGuarantorMatchInfo matchInfo = new MatchVpGuarantorMatchInfo();
            matchInfo.SetMatchSsn("arealssn");
            matchInfo.AddMatch(new MatchRegistry("matchstring", "ssk", 1));
            matchInfo.VpGuarantorId = 1;
            matchInfo.Matches[0].HsGuarantorMatchStatusId = HsGuarantorMatchStatusEnum.UnmatchedHard;
            IMatchVpGuarantorMatchInfoRepository repository = new MatchVpGuarantorMatchInfoRepository(new SessionContext<VisitPay>(this.Session));
            this.Session.Save(matchInfo.Matches.First().Registry);
            repository.Insert(matchInfo);
            this.Session.Flush();
            int hsgId = matchInfo.Matches.First().Registry.HsGuarantorId;
            MatchVpGuarantorMatchInfo matchInfoFetchedByChildId = repository.GetVpGuarantorRegistryMatch(hsgId);
            Assert.NotNull(matchInfoFetchedByChildId);
        }


        private IMatchRegistryRepository CreateMatchRegistries(IList<MatchRegistry> localMatchRegistries, params string[] ssks)
        {
            IMatchRegistryRepository matchRepository = this.CreateMatchRegistryRepository();
            DateTime[] dates = { DateTime.UtcNow.AddDays(1), DateTime.UtcNow };

            int i = 0;
            foreach (string ssk in ssks)
            {
                foreach (DateTime dateTime in dates)
                {
                    void InsertNewRegistry(MatchOptionEnum pattern, PatternUseEnum patternUse, string theSsk)
                    {
                        MatchRegistry registry = new MatchRegistry($"ssk-{i++}", theSsk, 1)
                        {
                            InsertDate = dateTime,
                            DataChangeDate = dateTime,
                            PartitionKey = 0m,
                            MatchOptionId = pattern,
                            PatternSetId = PatternSetEnum.S001,
                            PatternUseId = patternUse,
                        };

                        localMatchRegistries.Add(registry);
                    }

                    InsertNewRegistry(MatchOptionEnum.DobLnameSsn4SskZip, PatternUseEnum.Initial, ssk);
                    InsertNewRegistry(MatchOptionEnum.DobLnameSsnZip, PatternUseEnum.Ongoing, ssk);
                }
            }

            matchRepository.BulkInsert(localMatchRegistries);

            return matchRepository;
        }

        [TestCase(1, 2, false)]
        [TestCase(2, 2, false)]
        [TestCase(1, 2, true)]
        [TestCase(2, 2, true)]
        public void GetMatchData_ReturnsAll(int uniquePatients, int nVisits, bool includePatientData)
        {
            HsGuarantor guarantor = this.CreateAndSaveHsGuarantor();
            this.CreateAndSaveHsVisitsForHsGuarantor(guarantor.HsGuarantorId, nVisits, uniquePatients);

            IMatchGuarantorRepository matchRepository = this.CreateMatchGuarantorRepository();

            IList<MatchData> matchPatients = matchRepository.GetMatchData(guarantor.HsGuarantorId.ToListOfOne(), includePatientData);

            int expectedCount = includePatientData
                ? nVisits
                : 1;

            Assert.AreEqual(expectedCount, matchPatients.Count);

            Assert.AreEqual(includePatientData, matchPatients.Count(x => x.Patient != null) == nVisits);
        }

        private HsGuarantor CreateAndSaveHsGuarantor()
        {
            return MatchDataFactory.CreateAndSaveHsGuarantorsWithBuilder(
                () => new HsGuarantorBuilder().WithDefaults().WithRandomPersonalInformation().With(x => x.DataChangeDate = DateTime.UtcNow),
                1, 
                this.Session)
                .First();
        }

        private IList<Ivh.Domain.HospitalData.Visit.Entities.Visit> CreateAndSaveHsVisitsForHsGuarantor(int hsGuarantorId, int nVisits, int nUniquePatients = 1)
        {
            return MatchDataFactory.CreateAndSaveHsVisitsForHsGuarantor(
                this.Session,
                new HsGuarantor() { HsGuarantorId = hsGuarantorId, DataChangeDate = DateTime.UtcNow },
                nVisits,
                nUniquePatients).ToList();
        }

        private IMatchRegistryRepository CreateMatchRegistryRepository()
        {
            return new MatchRegistryRepository(new StatelessSessionContext<VisitPay>(this.StatelessSession));
        }

        private IMatchGuarantorRepository CreateMatchGuarantorRepository()
        {
            return new MatchGuarantorRepository(new SessionContext<CdiEtl>(this.Session), new StatelessSessionContext<CdiEtl>(this.StatelessSession));
        }
    }

    //todo: get rid of sequence id generator for hsguarantor and hsvisit or find a better way to override this map
    internal class HsGuarantorMapForTest : ClassMap<HsGuarantor>
    {
        public HsGuarantorMapForTest()
        {
            this.Schema("base");
            this.Table("HsGuarantor");
            this.Id(x => x.HsGuarantorId).Column("HsGuarantorID").GeneratedBy.Assigned();
            this.Map(x => x.HsBillingSystemId).Column("HsBillingSystemID").Not.Nullable();
            this.Map(x => x.SourceSystemKey).Not.Nullable();
            this.Map(x => x.FirstName);
            this.Map(x => x.LastName);
            this.Map(x => x.DOB);
            this.Map(x => x.SSN);
            this.Map(x => x.SSN4);
            this.Map(x => x.AddressLine1);
            this.Map(x => x.AddressLine2);
            this.Map(x => x.City);
            this.Map(x => x.StateProvince);
            this.Map(x => x.PostalCode);
            this.Map(x => x.Country);
            this.Map(x => x.VpEligible).Nullable();
            this.Map(x => x.GenderId).Nullable();
            this.Map(x => x.Email).Nullable();
            this.Map(x => x.DataLoadId).Nullable();
            this.Map(x => x.DataChangeDate).Nullable();
        }
    }

    internal class VisitMapForTest : ClassMap<Ivh.Domain.HospitalData.Visit.Entities.Visit>
    {
        public VisitMapForTest()
        {
            this.Schema("base");
            this.Table("Visit");
            this.Id(x => x.VisitId).GeneratedBy.Assigned();
            this.Map(x => x.HsGuarantorId).Not.Nullable();
            this.Map(x => x.BillingSystemId).Not.Nullable();
            this.Map(x => x.SourceSystemKey).Length(400).Not.Nullable();
            this.Map(x => x.SourceSystemKeyDisplay).Length(400).Nullable();
            this.Map(x => x.VisitDescription).Length(250).Nullable();
            this.Map(x => x.AdmitDate).Nullable();
            this.Map(x => x.DischargeDate).Nullable();
            this.Map(x => x.HsCurrentBalance).Not.Nullable();
            this.Map(x => x.InsuranceBalance).Not.Nullable();
            this.Map(x => x.SelfPayBalance).Not.Nullable();
            this.Map(x => x.FirstSelfPayDate).Nullable();
            this.Map(x => x.PatientFirstName).Length(250).Nullable();
            this.Map(x => x.PatientLastName).Length(250).Nullable();
            this.Map(x => x.PatientDOB).Nullable();
            this.Map(x => x.BillingApplication).Length(2).Nullable();
            this.Map(x => x.IsPatientGuarantor).Nullable();
            this.Map(x => x.IsPatientMinor).Nullable();
            this.Map(x => x.InterestRefund).Nullable();
            this.Map(x => x.InterestZero).Nullable();
            this.Map(x => x.ServiceGroupId).Nullable();
            this.Map(x => x.PaymentPriority).Nullable();
            this.Map(x => x.AgingCount).Nullable();
            this.Map(x => x.OnPrePaymentPlan).Nullable();
            this.Map(x => x.VpEligible).Nullable();
            this.Map(x => x.BillingHold).Nullable();
            this.Map(x => x.Redact).Nullable();
            this.Map(x => x.AgingTierId).Nullable();
            this.Map(x => x.DataLoadId).Nullable();
            this.Map(x => x.DataChangeDate).Nullable();
        }
    }
}

﻿namespace Ivh.Application.Tests.Matching
{
    using Application.Base.Common.Interfaces.Entities.HsGuarantor;
    using Application.Matching.Common.Interfaces;
    using Common.Base.Utilities.Extensions;
    using Common.Data.Connection;
    using Common.Data.Connection.Connections;
    using Common.Tests.Builders;
    using Common.Tests.Builders.Matching;
    using Common.Tests.Helpers.Matching;
    using Common.VisitPay.Enums;
    using Common.VisitPay.Messages.HospitalData;
    using Common.VisitPay.Messages.Matching.Dtos;
    using Domain.HospitalData.HsGuarantor.Entities;
    using Domain.Matching.Entities;
    using Domain.Matching.Match;
    using Domain.Matching.Sets;
    using Domain.Settings.Entities;
    using Ivh.Common.ServiceBus.Common.Messages;
    using Ivh.Common.VisitPay.Messages.Matching;
    using MassTransit;
    using Moq;
    using Newtonsoft.Json;
    using NUnit.Framework;
    using Provider.Matching;
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Application.Matching.Common.Dtos;
    using Common.Data.Interfaces;
    using Domain.Matching.Interfaces;
    using NHibernate.Linq;

    [TestFixture]
    public class MatchingIntegrationTests : RepositoryTestBase
    {
        public MatchingIntegrationTests() : base(m =>
        {
            m.FluentMappings.Add<HsGuarantorMapForTest>();
            m.FluentMappings.Add<VisitMapForTest>();
        }, typeof(MatchRegistry))
        {
        }

        private const int GuarantorsPerTest = 3;

        #region Tests

        [Test]
        public void QueueMatchPopulation_Determines_InsertOrUpdate_FirstRun_AllInsert()
        {
            Dictionary<Type, IList<UniversalMessage>> messages = new Dictionary<Type, IList<UniversalMessage>>();
            IMatchingApplicationService matchingApplicationService = this.CreateMatchingApplicationServiceWithMessageRouting(new List<Set> { new GuarantorMatchingSet001(null, ApplicationEnum.VisitPay.ToListOfOne())}, messages);

            DateTime processDate = new DateTime(2019, 1, 1);

            // create visits and guarantors (enumeration persists entities)
            _ = this.GetNewHsGuarantorsAndVisits(GuarantorsPerTest, processDate).ToList();

            // populate 
            matchingApplicationService.QueueMatchRegistryPopulation(null);

            Assert.AreEqual(GuarantorsPerTest, messages[typeof(PopulateMatchRegistryMessageList)].Sum(x => ((PopulateMatchRegistryMessageList)x).Messages.Count(m => !m.IsUpdate)), "there should be an insert message for each new hsguarantor");
            Assert.AreEqual(0, messages[typeof(PopulateMatchRegistryMessageList)].Sum(x => ((PopulateMatchRegistryMessageList)x).Messages.Count(m => m.IsUpdate)), "there should not be any update messages for existing hsguarantors");
        }

        [Test]
        public void QueueMatchPopulation_Determines_InsertOrUpdate_AllUpdates()
        {
            Dictionary<Type, IList<UniversalMessage>> messages = new Dictionary<Type, IList<UniversalMessage>>();
            IMatchingApplicationService matchingApplicationService = this.CreateMatchingApplicationServiceWithMessageRouting(new List<Set> { new GuarantorMatchingSet001(null, ApplicationEnum.VisitPay.ToListOfOne()) }, messages);

            DateTime processDate = new DateTime(2019, 1, 1);

            // create visits and guarantors (enumeration persists entities)
            this.GetNewHsGuarantorsAndVisits(GuarantorsPerTest, processDate).ForEach(x => _ = x.ToList());

            // populate 
            matchingApplicationService.QueueMatchRegistryPopulation(null);

            // update guarantors
            this.StatelessSession.Query<Guarantor>()
                .UpdateBuilder()
                .Set(x => x.DataChangeDate, processDate.AddDays(1))
                .Set(x => x.LastName, x => "new" + x.LastName)
                .Update();

            //reset counts
            messages[typeof(PopulateMatchRegistryMessageList)].Clear();

            // populate next day
            matchingApplicationService.QueueMatchRegistryPopulation(null);

            Assert.AreEqual(0, messages[typeof(PopulateMatchRegistryMessageList)].Sum(x => ((PopulateMatchRegistryMessageList)x).Messages.Count(m => !m.IsUpdate)), "there should not be an insert message for existing guarantors");
            Assert.AreEqual(GuarantorsPerTest, messages[typeof(PopulateMatchRegistryMessageList)].Sum(x => ((PopulateMatchRegistryMessageList)x).Messages.Count(m => m.IsUpdate)), "there should be an update message for each existing hsguarantors");
        }

        [Test]
        public void QueueMatchPopulation_Determines_InsertOrUpdate_PatientDataChange_TriggersUpdate()
        {
            Dictionary<Type, IList<UniversalMessage>> messages = new Dictionary<Type, IList<UniversalMessage>>();
            IMatchingApplicationService matchingApplicationService = this.CreateMatchingApplicationServiceWithMessageRouting(new List<Set> { new GuarantorMatchingSet003(null, ApplicationEnum.VisitPay.ToListOfOne()) }, messages);

            DateTime processDate = new DateTime(2019, 1, 1);

            // create visits and guarantors (enumeration persists entities)
            this.GetNewHsGuarantorsAndVisits(GuarantorsPerTest, processDate).ForEach(x => _ = x.ToList());

            // populate 
            matchingApplicationService.QueueMatchRegistryPopulation(null);

            // update a patient
            int id = this.Session.Query<Patient>().First().VisitId;
            this.Session.Query<Patient>()
                .Where(x => x.VisitId == id)
                .UpdateBuilder()
                .Set(x => x.DataChangeDate, processDate.AddDays(2))
                .Set(x => x.PatientDOB, processDate)
                .Update();
            this.Session.Flush();

            //reset counts
            messages[typeof(PopulateMatchRegistryMessageList)].Clear();

            // populate next day
            matchingApplicationService.QueueMatchRegistryPopulation(processDate.AddDays(1));

            Assert.AreEqual(0, messages[typeof(PopulateMatchRegistryMessageList)].Sum(x => ((PopulateMatchRegistryMessageList)x).Messages.Count(m => !m.IsUpdate)), "there should not be an insert message for existing guarantors");
            Assert.AreEqual(1, messages[typeof(PopulateMatchRegistryMessageList)].Sum(x => ((PopulateMatchRegistryMessageList)x).Messages.Count(m => m.IsUpdate)), "there should be an update message for each updated patient");
        }


        [Test]
        public void ProcessMatchUpdates_AddsRecordsForEachGuarantor()
        {
            IMatchingApplicationService matchingApplicationService = this.CreateMatchingApplicationService(new List<Set> { new GuarantorMatchingSet001(null, ApplicationEnum.VisitPay.ToListOfOne()) });
            const int batchSize = 1;
            
            this.PopulateMatchRegistry(matchingApplicationService, this.GetNewHsGuarantorsAndVisits(GuarantorsPerTest, batchSize), false);

            int guarantorsAddedToRegistry = this.Session.Query<MatchRegistry>().Select(x => x.SourceSystemKey).Distinct().Count();

            Assert.AreEqual(GuarantorsPerTest, guarantorsAddedToRegistry);
        }

        [TestCaseSource(typeof(MatchingTestCases), nameof(MatchingTestCases.MatchConfigurationTestCases))]
        [Test]
        public void IsGuarantorMatch_FindsMatch(MatchingTestCases.MatchConfigTestCase testCase)
        {
            IMatchingApplicationService matchingApplicationService = this.CreateMatchingApplicationService(testCase.Config);

            this.PopulateMatchRegistry(matchingApplicationService, this.GetNewHsGuarantorsAndVisits(GuarantorsPerTest, GuarantorsPerTest), false);

            Guarantor guarantor = this.Session.Query<Guarantor>().First();
            Patient patient = this.Session.Query<Patient>().First(x => x.HsGuarantorId == guarantor.HsGuarantorId);
            
            GuarantorMatchResult matchResult = matchingApplicationService.IsGuarantorMatch(new MatchDataDto
            {
                Guarantor = AutoMapper.Mapper.Map<MatchGuarantorDto>(guarantor),
                Patient = AutoMapper.Mapper.Map<MatchPatientDto>(patient),
                ApplicationEnum = ApplicationEnum.VisitPay,
            });

            Assert.AreEqual(GuarantorMatchResultEnum.Matched, matchResult.GuarantorMatchResultEnum);
        }

        [TestCaseSource(typeof(MatchingTestCases), nameof(MatchingTestCases.MatchConfigurationTestCases))]
        public void IsGuarantorMatch_GuestPay_FindsMatch(MatchingTestCases.MatchConfigTestCase testCase)
        {
            IMatchingApplicationService matchingApplicationService = this.CreateMatchingApplicationService(testCase.Config);

            this.PopulateMatchRegistry(matchingApplicationService, this.GetNewHsGuarantorsAndVisits(GuarantorsPerTest, GuarantorsPerTest), false);

            Guarantor guarantor = this.Session.Query<Guarantor>().First();
            Patient patient = this.Session.Query<Patient>().First(x => x.HsGuarantorId == guarantor.HsGuarantorId);

            GuarantorMatchResult matchResultGp = matchingApplicationService.IsGuarantorMatch(new MatchDataDto
            {
                Guarantor = AutoMapper.Mapper.Map<MatchGuarantorDto>(guarantor),
                Patient = AutoMapper.Mapper.Map<MatchPatientDto>(patient),
                ApplicationEnum = ApplicationEnum.VisitPay,
            });

            Assert.AreEqual(GuarantorMatchResultEnum.Matched, matchResultGp.GuarantorMatchResultEnum);
        }

        [TestCaseSource(typeof(MatchingTestCases), nameof(MatchingTestCases.MatchConfigurationTestCases))]
        public void IsGuarantorMatch_NoMatchFound(MatchingTestCases.MatchConfigTestCase testCase)
        {
            IMatchingApplicationService matchingApplicationService = this.CreateMatchingApplicationService(testCase.Config);

            this.PopulateMatchRegistry(matchingApplicationService, this.GetNewHsGuarantorsAndVisits(GuarantorsPerTest, GuarantorsPerTest), false);

            GuarantorMatchResult matchResult = matchingApplicationService.IsGuarantorMatch(new MatchDataDto
            {
                Guarantor = AutoMapper.Mapper.Map<MatchGuarantorDto>(new MatchGuarantorBuilder().WithDefaults().Build()),
                Patient = AutoMapper.Mapper.Map<MatchPatientDto>(new MatchPatientBuilder().WithDefaults().Build()),
                ApplicationEnum = ApplicationEnum.VisitPay,
            });

            Assert.AreEqual(GuarantorMatchResultEnum.NoMatchFound, matchResult.GuarantorMatchResultEnum);
        }

        [TestCaseSource(typeof(MatchingTestCases), nameof(MatchingTestCases.MatchConfigurationTestCases))]
        public void IsGuarantorMatch_GuestPay_NoMatchFound(MatchingTestCases.MatchConfigTestCase testCase)
        {
            IMatchingApplicationService matchingApplicationService = this.CreateMatchingApplicationService(GuarantorMatchingSetBuilder.GuestPay().ToListOfOne());

            this.PopulateMatchRegistry(matchingApplicationService, this.GetNewHsGuarantorsAndVisits(GuarantorsPerTest, GuarantorsPerTest), false);

            GuarantorMatchResult matchResult = matchingApplicationService.IsGuarantorMatch(new MatchDataDto
            {
                Guarantor = AutoMapper.Mapper.Map<MatchGuarantorDto>(new MatchGuarantorBuilder().WithDefaults().Build()),
                Patient = AutoMapper.Mapper.Map<MatchPatientDto>(new MatchPatientBuilder().WithDefaults().Build()),
                ApplicationEnum = ApplicationEnum.VisitPay,
            });

            Assert.AreEqual(GuarantorMatchResultEnum.NoMatchFound, matchResult.GuarantorMatchResultEnum);
        }

        [Test]
        public void IsGuarantorMatch_Ssn4WithLeading0s_FindsMatch()
        {
            IMatchingApplicationService matchingApplicationService = this.CreateMatchingApplicationService(GuarantorMatchingSetBuilder.VisitPay(PatternSetEnum.S001).ToListOfOne());

            IEnumerable<IEnumerable<int>> guarantorAndVisits = this.GetVisitsForGuarantors(() => new HsGuarantorBuilder()
                .WithDefaults()
                .WithRandomPersonalInformation()
                .WithSsn("231450678")
                .With(x => x.SSN4 = "678"),
                1,
                1);

            this.PopulateMatchRegistry(matchingApplicationService, guarantorAndVisits, false);

            Guarantor guarantor = this.Session.Query<Guarantor>().First();
            Patient patient = this.Session.Query<Patient>().First(x => x.HsGuarantorId == guarantor.HsGuarantorId);

            MatchDataDto matchDataDto = new MatchDataDto
            {
                Guarantor = AutoMapper.Mapper.Map<MatchGuarantorDto>(guarantor),
                Patient = AutoMapper.Mapper.Map<MatchPatientDto>(patient),
                ApplicationEnum = ApplicationEnum.VisitPay,
            };

            matchDataDto.Guarantor.SSN4 = matchDataDto.Guarantor.SSN4.PadLeft(4, '0');

            GuarantorMatchResult matchResult = matchingApplicationService.IsGuarantorMatch(matchDataDto);

            Assert.AreEqual(GuarantorMatchResultEnum.Matched, matchResult.GuarantorMatchResultEnum);
        }

        [Test]
        public void UnmatchHsGuarantor_UpdatesMatch()
        {
            IMatchingApplicationService matchingApplicationService = this.CreateMatchingApplicationService(new List<Set> { new GuarantorMatchingSet001(null, ApplicationEnum.VisitPay.ToListOfOne()) });
            const int vpGuarantorId = 1;
            const int nMatching = GuarantorsPerTest;
            const HsGuarantorMatchStatusEnum setStatusTo = HsGuarantorMatchStatusEnum.UnmatchedHard;

            // initial guarantor population
            this.PopulateMatchRegistry(matchingApplicationService, this.GetNewHsGuarantorsAndVisits(GuarantorsPerTest, GuarantorsPerTest), false);

            Guarantor guarantor = this.Session.Query<Guarantor>().First();
            Patient patient = this.Session.Query<Patient>().First(x => x.HsGuarantorId == guarantor.HsGuarantorId);

            // create a few more that should match
            this.PopulateMatchRegistry(matchingApplicationService, this.GetHighConfidenceMatchingHsGuarantorsAndVisits(guarantor, nMatching), false);

            // create matches
            matchingApplicationService.AddInitialMatch(vpGuarantorId, new MatchDataDto
            {
                Guarantor = AutoMapper.Mapper.Map<MatchGuarantorDto>(guarantor),
                Patient = AutoMapper.Mapper.Map<MatchPatientDto>(patient),
                ApplicationEnum = ApplicationEnum.VisitPay,
            });

            // find matches we created
            MatchVpGuarantorMatchInfo matchInfo = this.Session.Query<MatchVpGuarantorMatchInfo>().First(x => x.VpGuarantorId == vpGuarantorId);

            //
            Assert.True(matchInfo.Matches.All(x => x.HsGuarantorMatchStatusId == HsGuarantorMatchStatusEnum.Matched), "initial matches should be in Matched status");

            HsGuarantorUnmatchMessage message = new HsGuarantorUnmatchMessage()
            {
                HsGuarantorMatchStatus = setStatusTo,
                HsGuarantorId = guarantor.HsGuarantorId,
                VpGuarantorId = 1
            };

            // process unmatch message
            matchingApplicationService.ConsumeMessage(message, null);

            // match updated to target status
            Assert.IsTrue(matchInfo
                .Matches
                .Where(x => x.HsGuarantorId == guarantor.HsGuarantorId)
                .All(x => x.HsGuarantorMatchStatusId == setStatusTo),
                $"match registry records for unmatched guarantor should be in {setStatusTo.ToString()} status");

        }

        [Test]
        public void ConfidentMatchChosenFirst()
        {
            IMatchingApplicationService matchingApplicationService = this.CreateMatchingApplicationService(new List<Set> { new GuarantorMatchingSet003(null, ApplicationEnum.VisitPay.ToListOfOne()) });
            const int nInitialGuarantors = 2;
            const int vpGuarantorId = 1;
            const int nMatching = 1;
            const string targetSsn = "123456780";

            // initial guarantor population with invalid ssns
            this.PopulateMatchRegistry(matchingApplicationService, this.GetNewInvalidSsnHsGuarantorsAndVisits(nInitialGuarantors, nInitialGuarantors), false);

            // grab a guarantor with an invalid ssn
            Guarantor guarantor = this.Session.Query<Guarantor>().First();
            Patient patient = this.Session.Query<Patient>().First(x => x.HsGuarantorId == guarantor.HsGuarantorId);

            // create a few that should match
            this.PopulateMatchRegistry(matchingApplicationService, this.GetVisitsForGuarantors(
                () => new HsGuarantorBuilder().ThatMatchesHsGuarantorWithLowConfidence(guarantor).WithSsn("555551234"),
                nMatching,
                nMatching)
                .And(this.GetVisitsForGuarantors(
                () => new HsGuarantorBuilder().ThatMatchesHsGuarantorWithHighConfidence(guarantor).WithSsn(targetSsn),
                nMatching,
                nMatching)), false);

            // create matches
            matchingApplicationService.AddInitialMatch(vpGuarantorId, new MatchDataDto
            {
                Guarantor = AutoMapper.Mapper.Map<MatchGuarantorDto>(guarantor),
                Patient = AutoMapper.Mapper.Map<MatchPatientDto>(patient),
                ApplicationEnum = ApplicationEnum.VisitPay,
            });

            // find matches we created
            MatchVpGuarantorMatchInfo matchInfo = this.Session.Query<MatchVpGuarantorMatchInfo>().First(x => x.VpGuarantorId == vpGuarantorId);

            Assert.AreEqual(targetSsn, matchInfo.MatchSsn, "chose a lower-confidence match");
            Assert.AreEqual(2, matchInfo.Matches.Select(x => x.Registry.SourceSystemKey).Distinct().Count(), "cant match to multiple valid ssns");

        }

        [TestCaseSource(nameof(RegexBasedMatchTestCases))]
        public void RegexBasedMatch_Made(RegexBasedMatchTestCase testCase)
        {
            IDictionary<string, MatchSetConfigurationRegexEnum> removeAppendedFacilityCodeConfiguration = new Dictionary<string, MatchSetConfigurationRegexEnum>()
            {
                ["Matching.Field.Guarantor.SourceSystemKey.Regex"] = MatchSetConfigurationRegexEnum.RemoveMinistryPrefix
            };
            IMatchingApplicationService matchingApplicationService = this.CreateMatchingApplicationService(new List<Set> { new GuarantorMatchingSet003(removeAppendedFacilityCodeConfiguration, ApplicationEnum.VisitPay.ToListOfOne()) });
            const int nInitialGuarantors = 2;
            const int vpGuarantorId = 1;
            const int nMatching = 1;
            const string baseSsn = "123456780";

            // initial guarantor population with invalid ssns
            this.PopulateMatchRegistry(matchingApplicationService, this.GetNewInvalidSsnHsGuarantorsAndVisits(nInitialGuarantors, nInitialGuarantors), false);

            // grab a guarantor with an invalid ssn
            Guarantor guarantor = this.Session.Query<Guarantor>().First();
            Patient patient = this.Session.Query<Patient>().First(x => x.HsGuarantorId == guarantor.HsGuarantorId);

            // create a few that should match
            this.PopulateMatchRegistry(matchingApplicationService, testCase.AdditionalHsGuarantorPopulation(guarantor, baseSsn, nMatching, this), false);

            MatchGuarantorDto guarantorRegistrationMatchDto = AutoMapper.Mapper.Map<MatchGuarantorDto>(guarantor);
            MatchPatientDto patientRegistrationDto = AutoMapper.Mapper.Map<MatchPatientDto>(patient);

            // initial match found without a facility prefix
            guarantorRegistrationMatchDto.SourceSystemKey = $"{baseSsn}";
            GuarantorMatchResult result = matchingApplicationService.IsGuarantorMatch(new MatchDataDto
            {
                Guarantor = guarantorRegistrationMatchDto,
                Patient = patientRegistrationDto,
                ApplicationEnum = ApplicationEnum.VisitPay,
            });
            Assert.AreEqual(testCase.ExpectedMatchResultEnum, result.GuarantorMatchResultEnum);

            // initial match found with different a facility prefix
            guarantorRegistrationMatchDto.SourceSystemKey = $"JJJ-{baseSsn}";
            result = matchingApplicationService.IsGuarantorMatch(new MatchDataDto
            {
                Guarantor = guarantorRegistrationMatchDto,
                Patient = patientRegistrationDto,
                ApplicationEnum = ApplicationEnum.VisitPay,
            });
            Assert.AreEqual(testCase.ExpectedMatchResultEnum, result.GuarantorMatchResultEnum);

            // create matches
            matchingApplicationService.AddInitialMatch(vpGuarantorId, new MatchDataDto
            {
                Guarantor = AutoMapper.Mapper.Map<MatchGuarantorDto>(guarantor),
                Patient = AutoMapper.Mapper.Map<MatchPatientDto>(patient),
                ApplicationEnum = ApplicationEnum.VisitPay,
            });

            // find matches we created
            MatchVpGuarantorMatchInfo matchInfo = this.Session.Query<MatchVpGuarantorMatchInfo>().First(x => x.VpGuarantorId == vpGuarantorId);

            /* Printing out the match aggregate might help debug if things go wonky
            Console.WriteLine(matchInfo.ToString());
            foreach (MatchRegistryVpGuarantorMatch matchInfoUniqueMatchedHsGuarantor in matchInfo.UniqueMatchedHsGuarantors)
            {
                Console.WriteLine(matchInfoUniqueMatchedHsGuarantor.Registry);
            }
            */

            // 1 original guarantor + 2 similar
            if(testCase.ExpectedMatchResultEnum == GuarantorMatchResultEnum.Matched)
            {
                Assert.AreEqual(3, matchInfo.Matches.Select(x => x.Registry.SourceSystemKey).Distinct().Count());
            }
            else
            {
                // 1 original guarantor + 1 similar (assuming ambiguous initial match was resolved by client)
                Assert.AreEqual(2, matchInfo.Matches.Select(x => x.Registry.SourceSystemKey).Distinct().Count());
            }

        }

        [Test]
        public void ChangingMatchConfig_Works()
        {
            // initial config
            IMatchingApplicationService matchingApplicationService = this.CreateMatchingApplicationServiceWithMessageRouting(new List<Set> { new GuarantorMatchingSet001(null, ApplicationEnum.VisitPay.ToListOfOne()) });
            const int batchSize = GuarantorsPerTest;

            // create visits and guarantors (enumeration persists entities)
            _ = this.GetNewHsGuarantorsAndVisits(GuarantorsPerTest, batchSize).ToList();

            // populate with initial config
            matchingApplicationService.QueueMatchRegistryPopulation(null);

            // 
            int guarantorsAddedToRegistry = this.StatelessSession.Query<MatchRegistry>()
                .Where(x => x.PatternSetId == PatternSetEnum.S001 && x.DeletedDate == null)
                .Select(x => x.SourceSystemKey)
                .Distinct()
                .Count();

            // 
            Assert.AreEqual(GuarantorsPerTest, guarantorsAddedToRegistry, "registries not created for initial config");

            // switch config
            matchingApplicationService = this.CreateMatchingApplicationServiceWithMessageRouting(new List<Set> { new GuarantorMatchingSet003(null, ApplicationEnum.VisitPay.ToListOfOne()) });

            // populate with new config
            matchingApplicationService.QueueMatchRegistryPopulation(null);

            //
            int guarantorsAddedToRegistryWithNewConfig = this.StatelessSession.Query<MatchRegistry>()
                .Where(x => x.PatternSetId == PatternSetEnum.S003 && x.DeletedDate == null)
                .Select(x => x.SourceSystemKey)
                .Distinct()
                .Count();

            //
            guarantorsAddedToRegistry = this.StatelessSession.Query<MatchRegistry>()
                .Where(x => x.PatternSetId == PatternSetEnum.S001 && x.DeletedDate == null)
                .Select(x => x.SourceSystemKey)
                .Distinct()
                .Count();

            // 
            Assert.AreEqual(GuarantorsPerTest, guarantorsAddedToRegistryWithNewConfig, "registries not created for new config");
            Assert.AreEqual(GuarantorsPerTest, guarantorsAddedToRegistry, "registries do not exist from previous population");

            //TODO: VP-3436 - update vpg-registry matches and check em

        }

        [TestCaseSource(nameof(AddInitialMatchTestCases))]
        public void AddInitialMatches_CreatesMatches(AddInitialMatchTestCase testCase)
        {
            IMatchingApplicationService matchingApplicationService = this.CreateMatchingApplicationService(testCase.Config);
            const int nInitialGuarantors = 2;
            const int vpGuarantorId = 1;
            const int nMatching = GuarantorsPerTest;
            const int nSimilar = GuarantorsPerTest;

            // initial guarantor population
            this.PopulateMatchRegistry(matchingApplicationService, this.GetNewHsGuarantorsAndVisits(nInitialGuarantors, nInitialGuarantors), false);
            if (testCase.UseInvalidSsn)
            {
                this.PopulateMatchRegistry(matchingApplicationService, this.GetNewInvalidSsnHsGuarantorsAndVisits(nInitialGuarantors, nInitialGuarantors), false);
            }

            // grab a guarantor
            Guarantor guarantor = this.Session.Query<Guarantor>().First(x => !testCase.UseInvalidSsn || x.SSN == "999999999");
            Patient patient = this.Session.Query<Patient>().First(x => x.HsGuarantorId == guarantor.HsGuarantorId);

            // create a few that are similar, but have different ssns
            this.PopulateMatchRegistry(matchingApplicationService, this.GetSimilarHsGuarantorsAndVisits(guarantor, nSimilar), false);

            // create a few that should match
            this.PopulateMatchRegistry(matchingApplicationService, this.GetHighConfidenceMatchingHsGuarantorsAndVisits(guarantor, nMatching), false);

            // find matches
            matchingApplicationService.AddInitialMatch(vpGuarantorId, new MatchDataDto
            {
                Guarantor = AutoMapper.Mapper.Map<MatchGuarantorDto>(guarantor),
                Patient = AutoMapper.Mapper.Map<MatchPatientDto>(patient),
                ApplicationEnum = ApplicationEnum.VisitPay,
            });

            // 
            MatchVpGuarantorMatchInfo matchInfo = this.Session.Query<MatchVpGuarantorMatchInfo>().First(x => x.VpGuarantorId == vpGuarantorId);

            // 
            Assert.IsNotNull(matchInfo, "match not found");
            Assert.AreEqual(testCase.ExpectedMatchCount, matchInfo.Matches.Select(x => x.Registry.SourceSystemKey).Distinct().Count(), "unexpected hsguarantor match count");

            // check the match ssn
            if (testCase.UseInvalidSsn)
            {
                if (testCase.ExpectedMatchCount > 1)
                {
                    Assert.IsNotNull(matchInfo.MatchSsn, "MatchSsn should be set when additional match is found");
                }
                else
                {
                    Assert.IsNull(matchInfo.MatchSsn, "MatchSsn should not be set if there was not a valid ssn");
                }
            }
            else
            {
                Assert.AreEqual(guarantor.SSN, matchInfo.MatchSsn, "MatchSsn should match the inital match guarantor's ssn");
            }
        }

        [TestCaseSource(nameof(OngoingMatchTestCases))]
        public void OngoingMatches(OngoingMatchTestCase testCase)
        {
            IMatchingApplicationService matchingApplicationService = this.CreateMatchingApplicationServiceWithMessageRouting(testCase.Config);
            const int vpGuarantorId = 1;
            const int nOngoingMatches = 2;
            const int nAdditionalInitialMatches = 1;
            int expectedMatches = 1 + nAdditionalInitialMatches;
            this.PopulateMatchRegistry(matchingApplicationService, this.GetNewHsGuarantorsAndVisits(GuarantorsPerTest, GuarantorsPerTest), false);

            // grab a guarantor
            Guarantor guarantor = this.Session.Query<Guarantor>().First();
            Patient patient = this.Session.Query<Patient>().First(x => x.HsGuarantorId == guarantor.HsGuarantorId);

            // create a few that might match
            this.PopulateMatchRegistry(
                matchingApplicationService,
                this.GetSimilarHsGuarantorsAndVisits(guarantor, nAdditionalInitialMatches).And(this.GetHighConfidenceMatchingHsGuarantorsAndVisits(guarantor, nAdditionalInitialMatches)),
                false);

            // initial match
            matchingApplicationService.AddInitialMatch(vpGuarantorId, new MatchDataDto
            {
                Guarantor = AutoMapper.Mapper.Map<MatchGuarantorDto>(guarantor),
                Patient = AutoMapper.Mapper.Map<MatchPatientDto>(patient),
                ApplicationEnum = ApplicationEnum.VisitPay,
            });

            // 
            MatchVpGuarantorMatchInfo matchInfo = this.Session.Query<MatchVpGuarantorMatchInfo>().First(x => x.VpGuarantorId == vpGuarantorId);

            // things happened
            Assert.IsNotNull(matchInfo);
            Assert.AreEqual(expectedMatches, matchInfo.Matches.Select(x => x.Registry.SourceSystemKey).Distinct().Count(), "initial match count does not meet expectations");

            // create a few more that might match
            this.PopulateMatchRegistry(
                matchingApplicationService,
                testCase.AdditionalHsGuarantorBuilder(guarantor, nOngoingMatches, this),
                false);

            //
            matchingApplicationService.QueueFindNewHsGuarantorMatches();

            // things happened again
            expectedMatches += testCase.AddtionalMatchesExpected ? nOngoingMatches : 0;
            Assert.IsNotNull(matchInfo);
            Assert.AreEqual(expectedMatches, matchInfo.Matches.Select(x => x.Registry.SourceSystemKey).Distinct().Count(), "additional match count does not meet expectations");
        }

        [Test]
        public void OngoingMatches_HsGuarantorUnmatch()
        {
            Dictionary<Type,IList<UniversalMessage>> messageBuckets = new Dictionary<Type, IList<UniversalMessage>>();
            IMatchingApplicationService matchingApplicationService = this.CreateMatchingApplicationServiceWithMessageRouting(new List<Set> { new GuarantorMatchingSet001(null, ApplicationEnum.VisitPay.ToListOfOne()) }, messageBuckets);
            const int vpGuarantorId = 1;
            const int nOngoingMatches = 2;
            const int nAdditionalInitialMatches = 1;
            int expectedMatches = 1 + nAdditionalInitialMatches;
            this.PopulateMatchRegistry(matchingApplicationService, this.GetNewHsGuarantorsAndVisits(GuarantorsPerTest, GuarantorsPerTest), false);

            // grab a guarantor
            HsGuarantor guarantor = this.StatelessSession.Query<HsGuarantor>().First();
            Guarantor matchguarantor = this.StatelessSession.Query<Guarantor>().First();
            Patient patient = this.StatelessSession.Query<Patient>().First(x => x.HsGuarantorId == guarantor.HsGuarantorId);

            // create a few that might match
            this.PopulateMatchRegistry(matchingApplicationService,
                this.GetSimilarHsGuarantorsAndVisits(guarantor, nAdditionalInitialMatches)
                    .And(this.GetHighConfidenceMatchingHsGuarantorsAndVisits(guarantor, nAdditionalInitialMatches)), false);

            // initial match
            matchingApplicationService.AddInitialMatch(vpGuarantorId, new MatchDataDto
            {
                Guarantor = AutoMapper.Mapper.Map<MatchGuarantorDto>(matchguarantor),
                Patient = AutoMapper.Mapper.Map<MatchPatientDto>(patient),
                ApplicationEnum = ApplicationEnum.VisitPay,
            });

            // 
            MatchVpGuarantorMatchInfo matchInfo = this.Session.Query<MatchVpGuarantorMatchInfo>().First(x => x.VpGuarantorId == vpGuarantorId);

            // things happened
            Assert.IsNotNull(matchInfo);
            Assert.AreEqual(expectedMatches, matchInfo.Matches.Select(x => x.Registry.SourceSystemKey).Distinct().Count());

            // update guarantor data
            guarantor.LastName += "-NewName";
            guarantor.DataChangeDate = (guarantor.DataChangeDate ?? DateTime.MinValue).AddDays(1);
            this.StatelessSession.Update(guarantor);
            this.PopulateMatchRegistry(matchingApplicationService, guarantor.HsGuarantorId.ToListOfOne().ToListOfOne(), true);
            this.Session.Evict(matchInfo);
            matchInfo = this.Session.Query<MatchVpGuarantorMatchInfo>().First(x => x.VpGuarantorId == vpGuarantorId);

            // 
            Assert.True(
                matchInfo.Matches
                .Where(x => x.Registry.SourceSystemKey == guarantor.SourceSystemKey && !x.Registry.DeletedDate.HasValue)
                .All(x => x.Registry.MatchString.Contains(guarantor.LastName.ToUpper())),
                $"new registries not created with updated lastname {Environment.NewLine}{string.Join(Environment.NewLine, matchInfo.Matches.Select(x => x.Registry.ToString()))}");

            // create a few more that might match
            this.PopulateMatchRegistry(matchingApplicationService,
                this.GetSimilarHsGuarantorsAndVisits(guarantor, nOngoingMatches)
                    .And(this.GetHighConfidenceMatchingHsGuarantorsAndVisits(guarantor, nOngoingMatches)), false);

            // flushing transaction to trigger post-commit actions
            this.Session.Transaction.Commit();
            this.Session.Transaction.Begin();

            // TODO: test integration with consumers of this message (ChangeEventApplicationService)
            Assert.AreEqual(1, 
                messageBuckets[typeof(HsGuarantorMatchDiscrepancyMessageList)].Sum(x => ((HsGuarantorMatchDiscrepancyMessageList)x).Messages.Count),
                "a match discrepancy should be found for each changed guarantor");

            Assert.AreEqual(guarantor.SourceSystemKey,
                messageBuckets[typeof(HsGuarantorMatchDiscrepancyMessageList)].Select(x => ((HsGuarantorMatchDiscrepancyMessageList)x).Messages.First().MatchResult.MatchedHsGuarantorSourceSystemKey).First(),
                "a match discrepancy source system key does not match changed guarantor");

            //
            matchingApplicationService.QueueFindNewHsGuarantorMatches();

            matchInfo = this.Session.Query<MatchVpGuarantorMatchInfo>().First(x => x.VpGuarantorId == vpGuarantorId);
            // things happened again
            expectedMatches += nOngoingMatches;
            Assert.IsNotNull(matchInfo);
            Assert.AreEqual(expectedMatches, matchInfo.Matches.Select(x => x.Registry.SourceSystemKey).Distinct().Count());

        }

        #endregion Tests

        #region Helpers

        private void PopulateMatchRegistry(IMatchingApplicationService appService, IEnumerable<IEnumerable<int>> hsGuarantorsToPopulate, bool isUpdate)
        {
            DateTime processDate = DateTime.UtcNow;

            foreach (IEnumerable<int> hsGuarantorList in hsGuarantorsToPopulate)
            {
                PopulateMatchRegistryMessageList message = new PopulateMatchRegistryMessageList
                {
                    IsUpdate = isUpdate,
                    Messages = hsGuarantorList.Select(x => new PopulateMatchRegistryMessage { HsGuarantorId = x, ProcessDate = processDate, IsUpdate = isUpdate }).ToList()
                };

                appService.ConsumeMessage(message, null);
            }
        }

        private IEnumerable<IEnumerable<int>> GetNewHsGuarantorsAndVisits(int nGuarantors, DateTime dataChangeDate)
        {
            return this.GetVisitsForGuarantors(() => new HsGuarantorBuilder().WithDefaults().WithRandomPersonalInformation().With(x => x.DataChangeDate = dataChangeDate), nGuarantors, nGuarantors);
        }

        private IEnumerable<IEnumerable<int>> GetNewHsGuarantorsAndVisits(int nGuarantors, int batchSize)
        {
            return this.GetVisitsForGuarantors(() => new HsGuarantorBuilder().WithDefaults().WithRandomPersonalInformation(), nGuarantors, batchSize);
        }

        private IEnumerable<IEnumerable<int>> GetHighConfidenceMatchingHsGuarantorsAndVisits(IHsGuarantor guarantor, int nGuarantors)
        {
            return this.GetVisitsForGuarantors(() => new HsGuarantorBuilder().ThatMatchesHsGuarantorWithHighConfidence(guarantor), nGuarantors, nGuarantors);
        }

        private IEnumerable<IEnumerable<int>> GetSimilarHsGuarantorsAndVisits(IHsGuarantor guarantor, int nGuarantors)
        {
            return this.GetVisitsForGuarantors(() => new HsGuarantorBuilder().ThatAlmostMatchesHsGuarantor(guarantor), nGuarantors, nGuarantors);
        }

        private IEnumerable<IEnumerable<int>> GetNewInvalidSsnHsGuarantorsAndVisits(int nGuarantors, int batchSize)
        {
            return this.GetVisitsForGuarantors(() => new HsGuarantorBuilder().WithDefaults().WithRandomPersonalInformation().WithInvalidSsn(), nGuarantors, batchSize);
        }

        private IEnumerable<IEnumerable<int>> GetVisitsForGuarantors(Func<HsGuarantor> guarantorBuilder, int nGuarantors, int batchSize)
        {
            do
            {
                foreach (HsGuarantor newHsGuarantor in MatchDataFactory.CreateAndSaveHsGuarantorsWithBuilder(
                    guarantorBuilder,
                    Math.Min(batchSize, nGuarantors),
                    this.Session))
                {
                    yield return MatchDataFactory.CreateAndSaveHsVisitsForHsGuarantor(this.Session, newHsGuarantor, 2)
                        .Select(x => x.HsGuarantorId)
                        .Distinct();
                }

            } while ((nGuarantors -= batchSize) > 0);
        }

        private MatchingApplicationServiceMockBuilder CreateMockBuilder(IList<Set> matchConfig, Action<MatchingServiceMockBuilder> additionalServiceBuilderConfig = null)
        {
            MatchingApplicationServiceMockBuilder appServiceBuilder = new MatchingApplicationServiceMockBuilder();

            MatchingServiceMockBuilder serviceBuilder = new MatchingServiceMockBuilder
            {
                StatelessSession = this.StatelessSession,
                Session = this.Session,
                MatchVpGuarantorMatchInfoRepository = new MatchVpGuarantorMatchInfoRepository(new SessionContext<VisitPay>(this.Session)),
                MatchGuarantorRepository = new MatchGuarantorRepository(new SessionContext<CdiEtl>(this.Session), new StatelessSessionContext<CdiEtl>(this.StatelessSession)),
            };

            InitialMatchServiceMockBuilder initialMatchServiceMockBuilder = new InitialMatchServiceMockBuilder
            {
                MatchVpGuarantorMatchInfoRepository = new MatchVpGuarantorMatchInfoRepository(new SessionContext<VisitPay>(this.Session)),
            };

            // setup match registry repository mock
            IStatelessSessionContext<VisitPay> statelessSessionContext = new StatelessSessionContext<VisitPay>(this.StatelessSession);
            Mock<MatchRegistryRepository> mockMatchRegistryRepository = new Mock<MatchRegistryRepository>(statelessSessionContext)
            {
                CallBase = true // only want to actually mock one method
            };

            // overriding the sql sproc implementation here so we can test in sqlLite
            mockMatchRegistryRepository.Setup(x => x.UpdateMatchRegistryMatchString(It.IsAny<MatchRegistryDto>())).Returns<MatchRegistryDto>(this.UpdateMatchRegistryMock);
            serviceBuilder.MatchRegistryRepository = mockMatchRegistryRepository.Object;
            initialMatchServiceMockBuilder.MatchRegistryRepository = mockMatchRegistryRepository.Object;

            string matchConfigJson = matchConfig.ToJSON(new JsonSerializerSettings
            {
                PreserveReferencesHandling = PreserveReferencesHandling.None,
                NullValueHandling = NullValueHandling.Ignore
            }.WithStringEnumConverter(), true);

            Console.WriteLine($@"Creating service with config:{Environment.NewLine}{matchConfigJson}");

            Mock<Client> client = new Mock<Client>();
            client.Setup(x => x.ClientName).Returns("Acme");
            client.Setup(x => x.MatchConfiguration).Returns(matchConfigJson);

            serviceBuilder.DomainServiceCommonServiceMockBuilder.ClientServiceMock.Setup(x => x.GetClient()).Returns(client.Object);
            serviceBuilder.DomainServiceCommonServiceMockBuilder.FeatureServiceMock.Setup(x => x.IsFeatureEnabled(VisitPayFeatureEnum.FeatureMatchingApiIsEnabled)).Returns(true);

            initialMatchServiceMockBuilder.DomainServiceCommonServiceMockBuilder.ClientServiceMock.Setup(x => x.GetClient()).Returns(client.Object);
            initialMatchServiceMockBuilder.DomainServiceCommonServiceMockBuilder.FeatureServiceMock.Setup(x => x.IsFeatureEnabled(VisitPayFeatureEnum.FeatureMatchingApiIsEnabled)).Returns(true);

            additionalServiceBuilderConfig?.Invoke(serviceBuilder);

            appServiceBuilder.MatchingService = serviceBuilder.CreateService();
            appServiceBuilder.InitialMatchService = initialMatchServiceMockBuilder.CreateService();
            appServiceBuilder.ApplicationServiceCommonServiceMockBuilder.FeatureServiceMock.Setup(x => x.IsFeatureEnabled(VisitPayFeatureEnum.FeatureMatchingApiIsEnabled)).Returns(true);


            return appServiceBuilder;
        }

        private int? UpdateMatchRegistryMock(MatchRegistryDto dto)
        {
            // find match registry
            int? matchRegistryId = this.StatelessSession.Query<MatchRegistry>()
                .Where(x => x.PatternSetId == dto.PatternSetId
                && x.MatchOptionId == dto.MatchOptionId
                && x.PatternUseId == dto.PatternUseId
                && x.BaseVisitId == dto.BaseVisitId
                && x.SourceSystemKey == dto.SourceSystemKey
                && x.BillingSystemId == dto.BillingSystemId)
                .Select(x => (int?)x.RegistryId)
                .FirstOrDefault();

            if (!matchRegistryId.HasValue)
            {
                return null;
            }

            // set dataChangeDate
            this.StatelessSession.Query<MatchRegistry>()
                .Where(x => x.RegistryId == matchRegistryId)
                .UpdateBuilder()
                .Set(x => x.DataChangeDate, dto.DataChangeDate)
                .Update();

            // set match string, return true if it changed
            this.StatelessSession.Query<MatchRegistry>()
                .Where(x => x.RegistryId == matchRegistryId)
                .Where(x => x.MatchString != dto.MatchString)
                .UpdateBuilder()
                .Set(x => x.MatchString, dto.MatchString)
                .Update();

            return matchRegistryId;
        }

        private IMatchingApplicationService CreateMatchingApplicationService(IList<Set> matchConfig)
        {
            MatchingApplicationServiceMockBuilder appServiceBuilder = this.CreateMockBuilder(matchConfig);

            IMatchingApplicationService appService = appServiceBuilder.CreateService();
            return appService;
        }

        private IMatchingApplicationService CreateMatchingApplicationServiceWithMessageRouting(IList<Set> matchConfig, Dictionary<Type, IList<UniversalMessage>> publishedMessages = null, bool consumeMessages = true)
        {
            IMatchingApplicationService appService = null;

            void MockPublishMessage(Mock<Ivh.Common.ServiceBus.Interfaces.IBus> busMock)
            {
                busMock.Setup(x => x.PublishMessage(It.IsAny<UniversalMessage>())).Returns<UniversalMessage>(m =>
                {
                    Type messageType = m.GetType();

                    if (consumeMessages)
                    {
                        MessageRoutes(appService)[messageType].Invoke(m, null);
                    }

                    if (publishedMessages != null)
                    {
                        if (publishedMessages.ContainsKey(messageType))
                        {
                            publishedMessages[messageType].Add(m);
                        }
                        else
                        {
                            publishedMessages.Add(messageType, new List<UniversalMessage>{ m } );
                        }
                    }
                    return Task.CompletedTask;
                });
            }

            MatchingApplicationServiceMockBuilder appServiceBuilder = this.CreateMockBuilder(matchConfig, x =>
            {
                MockPublishMessage(x.DomainServiceCommonServiceMockBuilder.BusMock);
            });

            MockPublishMessage(appServiceBuilder.ApplicationServiceCommonServiceMockBuilder.BusMock);

            appService = appServiceBuilder.CreateService();
            return appService;
        }

        private static IDictionary<Type, Action<UniversalMessage, ConsumeContext<UniversalMessage>>> MessageRoutes(IMatchingApplicationService appService) => 
            new Dictionary<Type, Action<UniversalMessage, ConsumeContext<UniversalMessage>>>
            {
                [typeof(PopulateMatchRegistryMessage)] = (m, mc) => appService.ConsumeMessage(m as PopulateMatchRegistryMessage, mc as ConsumeContext<PopulateMatchRegistryMessage>),
                [typeof(PopulateMatchRegistryMessageList)] = (m, mc) => appService.ConsumeMessage(m as PopulateMatchRegistryMessageList, mc as ConsumeContext<PopulateMatchRegistryMessageList>),
                [typeof(QueueFindNewHsGuarantorMatchesMessageList)] = (m, mc) => appService.ConsumeMessage(m as QueueFindNewHsGuarantorMatchesMessageList, mc as ConsumeContext<QueueFindNewHsGuarantorMatchesMessageList>),
                [typeof(QueueFindNewHsGuarantorMatchesMessage)] = (m, mc) => appService.ConsumeMessage(m as QueueFindNewHsGuarantorMatchesMessage, mc as ConsumeContext<QueueFindNewHsGuarantorMatchesMessage>),
                [typeof(NewGuarantorMatchCreatedMessageList)] = (m, mc) => { },
                [typeof(NewGuarantorMatchCreatedMessage)] = (m, mc) => { },
                [typeof(HsGuarantorMatchDiscrepancyMessage)] = (m, mc) => { },
                [typeof(HsGuarantorMatchDiscrepancyMessageList)] = (m, mc) => { }
            };

        #endregion Helpers

        #region TestCases
        public class OngoingMatchTestCase : MatchingTestCases.MatchConfigTestCase
        {
            public bool AddtionalMatchesExpected { get; set; }
            public Func<Guarantor, int, MatchingIntegrationTests, IEnumerable<IEnumerable<int>>> AdditionalHsGuarantorBuilder { get;set; }
            public string TestCaseDescription { get; set; }

            public OngoingMatchTestCase(MatchingTestCases.MatchConfigTestCase testCase) : base(testCase.Client, testCase.Config)
            {
            }

            public override string ToString()
            {
                return $"{base.ToString()} - {this.TestCaseDescription}";
            }
        }

        public static IEnumerable<OngoingMatchTestCase> OngoingMatchTestCases()
        {
            foreach (MatchingTestCases.MatchConfigTestCase matchConfigurationTestCase in MatchingTestCases.MatchConfigurationTestCases())
            {
                yield return new OngoingMatchTestCase(matchConfigurationTestCase)
                {
                    TestCaseDescription = "null DoB matches",

                    // if all ongoing patterns require a DoB, this won't match
                    AddtionalMatchesExpected = matchConfigurationTestCase
                        .Config
                        .Where(x => x.Applications.Contains(ApplicationEnum.VisitPay))
                        .Any(x => !x.IsFieldRequired(PatternUseEnum.Ongoing, FieldSourceEnum.Guarantor, FieldPropertyEnum.DOB)),

                    AdditionalHsGuarantorBuilder = (guarantor, nMatching, textFixture) => textFixture.GetVisitsForGuarantors(
                    () => new HsGuarantorBuilder().ThatMatchesHsGuarantorWithHighConfidence(guarantor).With(x => x.DOB = null),
                    nMatching,
                    nMatching)
                };

                yield return new OngoingMatchTestCase(matchConfigurationTestCase)
                {
                    TestCaseDescription = "invalid DoB does not match",
                    AddtionalMatchesExpected = false,
                    AdditionalHsGuarantorBuilder = (guarantor, nMatching, textFixture) => textFixture.GetVisitsForGuarantors(
                    () => new HsGuarantorBuilder().ThatMatchesHsGuarantorWithHighConfidence(guarantor).With(x => x.DOB = new DateTime(1, 12, 25)),
                    nMatching,
                    nMatching)
                };

                yield return new OngoingMatchTestCase(matchConfigurationTestCase)
                {
                    TestCaseDescription = "expected match matches",
                    AddtionalMatchesExpected = true,
                    AdditionalHsGuarantorBuilder = (guarantor, nMatching, textFixture) => 
                        textFixture.GetSimilarHsGuarantorsAndVisits(guarantor, nMatching)
                        .And(textFixture.GetHighConfidenceMatchingHsGuarantorsAndVisits(guarantor, nMatching))
                };
            }
        }


        public class AddInitialMatchTestCase : MatchingTestCases.MatchConfigTestCase
        {
            public int ExpectedMatchCount { get; set; }
            public bool UseInvalidSsn { get; set; }

            public AddInitialMatchTestCase(MatchingTestCases.MatchConfigTestCase testCase) : base(testCase.Client, testCase.Config)
            {
            }

            public override string ToString()
            {
                return base.ToString() + (this.UseInvalidSsn ? " InvalidSsn" : string.Empty);
            }
        }

        public static IEnumerable<AddInitialMatchTestCase> AddInitialMatchTestCases()
        {
            foreach (MatchingTestCases.MatchConfigTestCase matchConfigurationTestCase in MatchingTestCases.MatchConfigurationTestCases())
            {
                {// valid ssn matching
                    AddInitialMatchTestCase testCase = new AddInitialMatchTestCase(matchConfigurationTestCase);
                    PatternSetEnum setEnum = testCase.Config.First().PatternSetEnum;

                    testCase.ExpectedMatchCount = 1 + GuarantorsPerTest;
                    testCase.UseInvalidSsn = false;
                    yield return testCase;
                }

                {// invalid ssn matching
                    AddInitialMatchTestCase testCase = new AddInitialMatchTestCase(matchConfigurationTestCase);
                    PatternSetEnum setEnum = testCase.Config.First().PatternSetEnum;
                    // S003 and S005 use nonSsn matching.
                    //   1 Initial match
                    // + 3 additional matches for any guarantors with invalid ssns (that match on other fields)
                    // + 1 after setting the matchSsn for a guarantor with a valid ssn.
                    int expectedMatchCount = 2 + GuarantorsPerTest;
                    testCase.ExpectedMatchCount = setEnum.IsInCategory(PatternSetEnumCategory.NonSsnMatching) ? expectedMatchCount : 1;
                    testCase.UseInvalidSsn = true;
                    yield return testCase;
                }
            }
        }

        public class RegexBasedMatchTestCase
        {
            public Func<Guarantor, string, int, MatchingIntegrationTests, IEnumerable<IEnumerable<int>>> AdditionalHsGuarantorPopulation { get; set; }
            public GuarantorMatchResultEnum ExpectedMatchResultEnum { get; set; }
            public string TestCaseName { get; set; }
            public override string ToString()
            {
                return this.TestCaseName;
            }
        }

        public static IEnumerable<RegexBasedMatchTestCase> RegexBasedMatchTestCases()
        {
            yield return new RegexBasedMatchTestCase
            {
                TestCaseName = "Same person matches",
                ExpectedMatchResultEnum = GuarantorMatchResultEnum.Matched,
                AdditionalHsGuarantorPopulation = (guarantor, baseSsn, nMatching, textFixture) => textFixture.GetVisitsForGuarantors(
                () => new HsGuarantorBuilder().ThatMatchesHsGuarantorWithHighConfidence(guarantor).With(x => x.SourceSystemKey = $"ABC-{baseSsn}"),
                nMatching,
                nMatching)
                .And(textFixture.GetVisitsForGuarantors(
                () => new HsGuarantorBuilder().ThatMatchesHsGuarantorWithHighConfidence(guarantor).With(x => x.SourceSystemKey = $"XYZ-{baseSsn}"),
                nMatching,
                nMatching))
            };
            yield return new RegexBasedMatchTestCase
            {
                TestCaseName = "Different person does not match",
                ExpectedMatchResultEnum = GuarantorMatchResultEnum.Ambiguous,
                AdditionalHsGuarantorPopulation = (guarantor, baseSsn, nMatching, textFixture) => textFixture.GetVisitsForGuarantors(
                () => new HsGuarantorBuilder().ThatMatchesHsGuarantorWithHighConfidence(guarantor).With(x => x.SourceSystemKey = $"ABC-{baseSsn}"),
                nMatching,
                nMatching)
                .And(textFixture.GetVisitsForGuarantors(
                () => new HsGuarantorBuilder().ThatMatchesHsGuarantorLastName(guarantor).With(x => x.PostalCode = guarantor.PostalCode).With(x => x.SourceSystemKey = $"XYZ-{baseSsn}"),
                nMatching,
                nMatching))
            };
        }

        


        #endregion TestCases
    }
}

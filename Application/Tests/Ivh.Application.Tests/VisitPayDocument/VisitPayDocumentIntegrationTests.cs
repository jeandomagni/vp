﻿namespace Ivh.Application.Tests.VisitPayDocument
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using Application.Core.Common.Dtos;
    using Application.Core.Common.Interfaces;
    using Common.Data.Connection;
    using Common.Data.Connection.Connections;
    using Common.Tests.Helpers.User;
    using Common.Tests.Helpers.VisitPayDocument;
    using Common.VisitPay.Enums;
    using Domain.Core.VisitPayDocument.Entities;
    using Domain.Core.VisitPayDocument.Interfaces;
    using Domain.Core.VisitPayDocument.Mappings;
    using Domain.User.Entities;
    using Microsoft.AspNet.Identity;
    using Moq;
    using NUnit.Framework;
    using Provider.Core.VisitPayDocument;

    [TestFixture]
    public class VisitPayDocumentIntegrationTests: RepositoryTestBase
    {
        public VisitPayDocumentIntegrationTests() : base(m => {
            m.FluentMappings.Add<VisitPayDocumentMapping>();
            m.FluentMappings.Add<VisitPayDocumentResultMapping>();
        }, null)
        {
        }

        [Test]
        public void AddVisitPayDocument_works()
        {
            IVisitPayDocumentApplicationService applicationService = this.GetVisitPayDocumentApplicationWithBackingRepo();
            VisitPayDocument docmentForTest = this.CreateDefaultVisitPayDocument("foo");

            VisitPayDocumentDto visitPayDocumentDto = AutoMapper.Mapper.Map<VisitPayDocumentDto>(docmentForTest);
            applicationService.AddVisitPayDocument(visitPayDocumentDto);

            VisitPayDocument visitPayDocument = this.Session.Query<VisitPayDocument>().FirstOrDefault();
            Assert.NotNull(visitPayDocument);
            List<string> errors = this.EntityAndDtoMatch(visitPayDocument, visitPayDocumentDto);
            Assert.AreEqual(0, errors.Count, string.Join("\n", errors));
        }

        [Test]
        public void GetVisitPayDocumentDtoById_works()
        {
            IVisitPayDocumentApplicationService applicationService = this.GetVisitPayDocumentApplicationWithBackingRepo();
            VisitPayDocument docmentForTest = this.CreateDefaultVisitPayDocument("foo");
            this.Session.Save(docmentForTest);

            VisitPayDocumentDto visitPayDocumentDto = applicationService.GetVisitPayDocumentDtoById(docmentForTest.VisitPayDocumentId);

            Assert.NotNull(visitPayDocumentDto);
            List<string> errors = this.EntityAndDtoMatch(docmentForTest, visitPayDocumentDto);
            Assert.AreEqual(0, errors.Count, string.Join("\n", errors));
        }

        [Test]
        public void GetVisitPayDocumentResultDtoById_works()
        {
            IVisitPayDocumentApplicationService applicationService = this.GetVisitPayDocumentApplicationWithBackingRepo();
            VisitPayDocument docmentForTest = this.CreateDefaultVisitPayDocument("foo");
            VisitPayDocument docmentForTest2 = this.CreateDefaultVisitPayDocument("foo2");
            this.Session.Save(docmentForTest);
            //save a second one so our list contains more than the one we are searching for
            this.Session.Save(docmentForTest2);

            VisitPayDocumentResultDto visitPayDocumentResult = applicationService.GetVisitPayDocumentResultDtoById(docmentForTest.VisitPayDocumentId);

            Assert.NotNull(visitPayDocumentResult);
            List<string> errors = this.EntityAndDtoMatch(docmentForTest, visitPayDocumentResult);
            Assert.AreEqual(0, errors.Count, string.Join("\n", errors));
        }

        [Test]
        public void SaveVisitPayDocument_works()
        {
            IVisitPayDocumentApplicationService applicationService = this.GetVisitPayDocumentApplicationWithBackingRepo();
            VisitPayDocument docmentForTest = this.CreateDefaultVisitPayDocument("foo");
            this.Session.Save(docmentForTest);
            //map it to detach from the entity
            VisitPayDocumentDto visitPayDocumentDto = AutoMapper.Mapper.Map<VisitPayDocumentDto>(docmentForTest);

            DateTime activeDate = visitPayDocumentDto.ActiveDate.Value.AddDays(10);
            DateTime approvedDate = DateTime.UtcNow; // we are just going to verify it's not null (no funky date stuff)
            int approvedByVpUserId = 2;
            string displayTitle = "edited";
            VisitPayDocumentTypeEnum visitPayDocumentType = VisitPayDocumentTypeEnum.ReleaseNotes;

            VisitPayDocumentEditDto visitPayDocumentEditDto = new VisitPayDocumentEditDto
            {
                VisitPayDocumentId = docmentForTest.VisitPayDocumentId,
                ActiveDate = activeDate,
                ApprovedDate = approvedDate,
                ApprovedByVpUserId = approvedByVpUserId,
                DisplayTitle = displayTitle,
                VisitPayDocumentType = visitPayDocumentType
            };

            applicationService.SaveVisitPayDocument(visitPayDocumentEditDto);
            
            Assert.AreNotEqual(docmentForTest.ActiveDate , visitPayDocumentDto.ActiveDate);
            Assert.AreNotEqual(docmentForTest.ApprovedDate , visitPayDocumentDto.ApprovedDate);
            Assert.AreNotEqual(docmentForTest.ApprovedByVpUserId , visitPayDocumentDto.ApprovedByVpUserId);
            Assert.AreNotEqual(docmentForTest.DisplayTitle, visitPayDocumentDto.DisplayTitle);
            Assert.AreNotEqual(docmentForTest.VisitPayDocumentType, visitPayDocumentDto.VisitPayDocumentType);

            Assert.AreEqual(activeDate,           docmentForTest.ActiveDate);
            Assert.AreEqual(approvedDate,         docmentForTest.ApprovedDate);
            Assert.AreEqual(approvedByVpUserId,   docmentForTest.ApprovedByVpUserId);
            Assert.AreEqual(displayTitle,         docmentForTest.DisplayTitle);
            Assert.AreEqual(visitPayDocumentType, docmentForTest.VisitPayDocumentType);
        }

        [Test]
        public void DeleteVistPayDocument_works()
        {
            IVisitPayDocumentApplicationService applicationService = this.GetVisitPayDocumentApplicationWithBackingRepo();
            VisitPayDocument docmentForTest = this.CreateDefaultVisitPayDocument("foo");
            this.Session.Save(docmentForTest);
            int count = this.Session.Query<VisitPayDocument>().Count();
            //validate pretest
            Assert.AreEqual(1, count);
            
            applicationService.DeleteVisitPayDocument(docmentForTest.VisitPayDocumentId);
            int postCount = this.Session.Query<VisitPayDocument>().Count();

            Assert.AreEqual(0, postCount);
        }
        
        private IVisitPayDocumentApplicationService GetVisitPayDocumentApplicationWithBackingRepo()
        {
            VisitPayDocumentApplicationServiceMockBuilder appServiceMockBuilder = new VisitPayDocumentApplicationServiceMockBuilder();
            VisitPayDocumentServiceMockBuilder serviceMockBuilder = new VisitPayDocumentServiceMockBuilder();
            VisitPayDocumentRepository repository = new VisitPayDocumentRepository(new SessionContext<VisitPay>(this.Session));
            VisitPayUserServiceMockBuilder userServiceMockBuilder = new VisitPayUserServiceMockBuilder();
            userServiceMockBuilder.StoreMock.Setup(x => x.FindByIdAsync(It.IsAny<string>())).ReturnsAsync((string id) => this.Users[id]);
            serviceMockBuilder.VisitPayDocumentRepository = repository;
            IVisitPayDocumentService service = serviceMockBuilder.CreateService();
            appServiceMockBuilder.VisitPayDocumentService = service;
            appServiceMockBuilder.VisitPayUserService = userServiceMockBuilder.CreateService();
            IVisitPayDocumentApplicationService applicationService = appServiceMockBuilder.CreateService();
            return applicationService;
        }

        protected Dictionary<string,VisitPayUser> Users = new Dictionary<string, VisitPayUser>
        {
            ["1"]=new VisitPayUser{UserName = "UserOne"},
            ["2"]= new VisitPayUser { UserName = "UserTwo" }
        };


        private VisitPayDocument CreateDefaultVisitPayDocument(string fileName)
        {

            Random rnd = new Random();
            byte[] bytes = new byte[2];
            rnd.NextBytes(bytes);

            VisitPayDocument visitPayDocument = new VisitPayDocument
            {
                UploadedByVpUserId = 1,
                ActiveDate = DateTime.UtcNow.AddDays(30),
                AttachmentFileName = fileName,
                MimeType = "image/jpg",
                FileContent = bytes,
                FileSize = bytes.Length,
                DisplayTitle = $"{fileName}_display",
                VisitPayDocumentType = VisitPayDocumentTypeEnum.TrainingMaterials
            };

            return visitPayDocument;
        }

        private List<string> EntityAndDtoMatch<TEntity,TDto>(TEntity visitPayDocument, TDto visitPayDocumentDto)
        {

            List<string> errors = new List<string>();
            
            List<string> blackList = new List<string>(){ nameof(VisitPayDocument.FileContent), nameof(VisitPayDocument.VisitPayDocumentId) };
            
            PropertyInfo[] propertyInfosEntity = typeof(VisitPayDocument).GetProperties();
            propertyInfosEntity = propertyInfosEntity.Where(x => !blackList.Contains(x.Name)).ToArray();

            foreach (PropertyInfo propertyInfo in propertyInfosEntity)
            {
                object entityValue =  visitPayDocument.GetType().GetProperty(propertyInfo.Name)?.GetValue(visitPayDocument, null);
                object dtoValue = visitPayDocumentDto.GetType().GetProperty(propertyInfo.Name)?.GetValue(visitPayDocumentDto, null);

                if (entityValue != null && !entityValue.Equals(dtoValue))
                {
                    errors.Add($"Entity {propertyInfo.Name} did not match Dto {propertyInfo.Name}");
                }
            }

            return errors;
        }

    }
}
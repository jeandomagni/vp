﻿/*using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ivh.Application.FileStorage.Tests
{
    using Autofac;
    using Ivh.Application.FileStorage;
    using Ivh.Application.FileStorage.Common.Dtos;
    using Ivh.Application.FileStorage.Common.Interfaces;
    using Ivh.Common.DependencyInjection;
    using Ivh.Common.DependencyInjection.Registration;
    using Ivh.Domain.Core.Client.Entities;
    using Ivh.Domain.Guarantor.Entities;
    using Ivh.Domain.FileStorage.Entities;
    using Ivh.Domain.FileStorage.Interfaces;
    using Ivh.Domain.FileStorage.Services;
    using Newtonsoft.Json;
    using System.Configuration;
    using System.Diagnostics;
    using System.IO;
    using System.IO.Compression;
    using System.Security.Cryptography;

    [Ignore]
    [TestFixture]
    public class FileStorageApplicationServiceTests : ApplicationTestBase
    { 
        // TODO: these constants are used only to support the test... in the future this should be a real set of PDF's
        private const string TestFilesSourceFolder = @"C:\Windows\";
        private const string TestFiles = "Notepad.exe,Write.exe";
        private const string TestFilesToZipFolder = @"C:\Temp\TestFilesToZip\";

        //private const string PackageImportTestFolder = @"C:\Temp\ImportPackagesTest\";
        private const string TestPackageFile = @"TestPackage";

        private byte[] _sharedKey;
        private string _zipFileName;
        private string _pkgFileName;
        private string _arcFileName;
        private string _manifestFileName;

        private string _tempFolder;
        private string _incomingFolder;
        private string _archiveFolder;

        private FileStorageApplicationService _appStorageService;
        private IFileStorageRepository _fileStorageRepository;

        [TestFixtureSetUp]
        public void InitializeTest()
        {
            // The _tempFolder must be set before Cleanup()
            _tempFolder = Path.Combine(Path.GetTempPath(), Path.GetRandomFileName());

            // TODO: in production this comes from the ApplicationSettings service via DI... we sould replicate that here
            // but for now we will just do this to get the test running with minimum upfront effort
            _incomingFolder = AppSettingsProvider.Instance.AppSettings[FileStorageApplicationService.AppSettingIncomingFolder];
            _archiveFolder = AppSettingsProvider.Instance.AppSettings[FileStorageApplicationService.AppSettingArchiveFolder];
            var sk = AppSettingsProvider.Instance.AppSettings[FileStorageApplicationService.AppSettingFileStorageSharedKey];
            _sharedKey = Convert.FromBase64String(sk);

            Cleanup(); // remove possible residual results from previous tests

            if (!Directory.Exists(TestFilesToZipFolder))
            {
                Directory.CreateDirectory(TestFilesToZipFolder);
            }

            if (!Directory.Exists(_incomingFolder))
            {
                Directory.CreateDirectory(_incomingFolder);
            }

            _zipFileName = Path.Combine(_incomingFolder, TestPackageFile) + FileStorageApplicationService.PackageCompressedExtension;
            if (File.Exists(_zipFileName))
            {
                File.Delete(_zipFileName);
            }

            _pkgFileName = Path.Combine(_incomingFolder, TestPackageFile) + FileStorageApplicationService.PackageEncryptedExtension;
            if (File.Exists(_pkgFileName))
            {
                File.Delete(_pkgFileName);
            }

            var archivedPkg = Path.Combine(_archiveFolder, TestPackageFile) + FileStorageApplicationService.PackageEncryptedExtension;
            if (File.Exists(archivedPkg))
            {
                File.Delete(archivedPkg);
            }

            var archivedZip = Path.Combine(_archiveFolder, TestPackageFile) + FileStorageApplicationService.PackageCompressedExtension;
            if (File.Exists(archivedZip))
            {
                File.Delete(archivedZip);
            }

            _appStorageService = new FileStorageApplicationService(null, null, null, null);

            CopyTestFiles(TestFilesSourceFolder, TestFilesToZipFolder);

            _manifestFileName = Path.Combine(TestFilesToZipFolder, FileStorageApplicationService.ManifestFileName);
            var manifest = _appStorageService.GenerateManifest(TestFilesToZipFolder, new List<string>() { FileStorageApplicationService.ManifestFileName });
            string manifestJson = JsonConvert.SerializeObject(manifest);

            if (File.Exists(_manifestFileName))
                File.Delete(_manifestFileName);
            File.WriteAllText(_manifestFileName, manifestJson, Encoding.UTF8);         
            
            CreateZipFromDirectory(TestFilesToZipFolder, _incomingFolder, TestPackageFile);
            _appStorageService.EncryptPackage(_zipFileName, _pkgFileName, _sharedKey); 
        }

        [TestFixtureTearDown]
        public void Cleanup()
        {
            DeleteTestFiles(_tempFolder, TestFilesToZipFolder);

            if (Directory.Exists(TestFilesToZipFolder))
            {
                try
                {
                    Directory.Delete(TestFilesToZipFolder, true);
                }
                catch (System.IO.IOException ex)
                {
                    //ignore if we cannot delete
                    Debug.WriteLine(ex);
                }
            }
        }

        [Ignore]
        [Test]
        public async Task ImportDirectoryAsyncTest()
        {
            await _appStorageService.ImportDirectoryAsync();       
        }

        [Ignore]
        [Test]
        public void AppStorageServiceIntegrationTest()
        {
            // Step 01 - Get packages to unpack
            var packages = GetPackagesToImportTest01();

            foreach (var pkg in packages)
            {
                _pkgFileName = MovePackageToArchiveTest02();

                // Step 02 - Decrypt the packages
                DecryptPackageTest03();

                // Step 03 - Unpack the packages
                UnpackPackageTest04();

                // Step 04 - Verify the packages
                VerifyManifestTest05();

                // Step 05 - Import the files from the packages
                SaveFilesToStorageTest06();

                
            }

        }


        private string[] GetPackagesToImportTest01()
        {
            var packages = _appStorageService.GetPackagesToImport(_incomingFolder, FileStorageApplicationService.PackageEncryptedExtension);
            Assert.IsTrue(packages.Length > 0);
            Assert.IsTrue(File.Exists(_pkgFileName));
            return packages;
        }

        private string MovePackageToArchiveTest02()
        {
            var archivedPkg = _appStorageService.MovePackageToArchive(_incomingFolder, _archiveFolder);
            Assert.IsNotNull(archivedPkg);
            return archivedPkg;
        }

        private void DecryptPackageTest03()
        {
            _appStorageService.DecryptPackage(_pkgFileName, _zipFileName, _sharedKey);
            Assert.IsTrue(!File.Exists(_pkgFileName));
            Assert.IsTrue(File.Exists(_zipFileName));
        }

        private Manifest UnpackPackageTest04()
        {
            var manifest = _appStorageService.UnpackPackage(_zipFileName, _tempFolder);
            return manifest;
        }

        private void VerifyManifestTest05()
        {
            var manifest = _appStorageService.GetManifest(_manifestFileName);
            _appStorageService.VerifyManifest(manifest);
        }

        private void SaveFilesToStorageTest06()
        {
            // TODO: We need dependency injectionn in the test to do this. Not enough time to wire that in.
            //try
            //{
            //    var fs = new FileStored()
            //    {
            //        FileContents = contents
            //        ,
            //        FileDateTime = DateTime.UtcNow
            //        ,
            //        FileMimeType = "application/pdf"
            //        ,
            //        ExternalKey = Guid.NewGuid()
            //        ,
            //        Filename = file.Name
            //    };
            //    var result = await _fileStorageService.SaveFileAsync(fs).ConfigureAwait(true);
            //}
            //catch (Exception)
            //{
            //    //TODO: log exception but continue processing other files
            //    // metrics
            //}
            //_appStorageService.SaveFileAsync(dto);
        }


        internal void CopyTestFiles(string testFilesSourceFolder, string testFilesToZipFolder)
        {
            if (!Directory.Exists(testFilesToZipFolder))
            {
                Directory.CreateDirectory(TestFilesToZipFolder);
            }

            var testFiles = TestFiles.Split(',');
            foreach (var file in testFiles)
            {
                File.Copy(Path.Combine(testFilesSourceFolder, file), Path.Combine(testFilesToZipFolder, file), true);
            }
        }


        internal void DeleteTestFiles(string testFilesToZipFolder, string tempFolder)
        {
            var testFiles = TestFiles.Split(',');
            foreach (var file in testFiles)
            {
                if (Directory.Exists(testFilesToZipFolder))
                {
                    File.Delete(Path.Combine(testFilesToZipFolder, file));
                }
                if (Directory.Exists(tempFolder))
                {
                    File.Delete(Path.Combine(tempFolder, file));
                }
            }
        }
        
        internal void CreateZipFromDirectory(string sourceFilesDirectoryPath, string outputDirectory, string testPackageFile)
        {
            var outFile = Path.Combine(outputDirectory, testPackageFile) + FileStorageApplicationService.PackageCompressedExtension;
            ZipFile.CreateFromDirectory(sourceFilesDirectoryPath, outFile, CompressionLevel.Optimal, false);
        }

    }
}
*/
﻿namespace Ivh.Application.Tests
{
    using AutoMapper;
    using Common.DependencyInjection.Registration;
    using NUnit.Framework;

    public abstract class ApplicationTestBase
    {
        [OneTimeSetUp]
        public void SetupOnce()
        {
            using (MappingBuilder mappingBuilder = new MappingBuilder())
            {
                mappingBuilder.WithBase()
                    // it doesn't seem like a super good idea that this profiles even exists
                    // why do we have custom mappings for unit tests?
                    // todo: let's get rid of this...
                    .WithProfile<Ivh.Application.Tests.Mappings.AutomapperProfile>();
            }
        }

        [OneTimeTearDown]
        public void TeardownOnce()
        {
            Mapper.Reset();
        }
    }
}
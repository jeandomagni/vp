﻿namespace Ivh.Application.Tests.Base
{
    using System.Collections.Generic;
    using System.Linq;
    using Application.Base.Common.Dtos;
    using Application.Base.Common.Interfaces;
    using Application.Core.Common.Dtos;
    using Common.Base.Utilities.Extensions;
    using Common.Tests.Helpers.Base;
    using Domain.FinanceManagement.FinancePlan.Entities;
    using Domain.FinanceManagement.Payment.Entities;
    using Domain.Visit.Entities;
    using Moq;
    using NUnit.Framework;

    [TestFixture]
    public class VisitBalanceBaseApplicationServiceTests : ApplicationTestBase
    {
        private const int VpGuarantorId = 0;

        [Test]
        public void SetTotalBalance_SetsBalances()
        {
            VisitDto visitDto = new VisitDto
            {
                CurrentBalance = 10m,
                VisitId = 1
            };

            IList<PaymentAllocationVisitAmountResult> unclearedPayments = new List<PaymentAllocationVisitAmountResult>
            {
                new PaymentAllocationVisitAmountResult {ActualAmount = 1m, VisitId = 1}
            };

            IList<FinancePlanVisitInterestDueResult> interestDues = new List<FinancePlanVisitInterestDueResult>
            {
                new FinancePlanVisitInterestDueResult {InterestDue = 2m, VisitId = 1}
            };

            VisitBalanceBaseApplicationServiceMockBuilder builder = new VisitBalanceBaseApplicationServiceMockBuilder();
            builder.Setup(x => x.PaymentAllocationServiceMock.Setup(m => m.GetNonInterestUnclearedPaymentAmountsForVisits(It.IsAny<IList<int>>(),null, null)).Returns(() => unclearedPayments));
            builder.Setup(x => x.FinancePlanServiceMock.Setup(m => m.GetInterestDueForVisits(It.IsAny<IList<int>>())).Returns(() => interestDues));
            builder.Setup(x => x.FinancePlanServiceMock.Setup(m => m.GetInterestForVisits(It.IsAny<IList<int>>(), null, null)).Returns(() => interestDues));
            builder.Setup(x => x.FinancePlanServiceMock.Setup(m => m.GetPrincipalAmountsForVisits(It.IsAny<IList<int>>())).Returns(() => new List<FinancePlanVisitPrincipalAmountResult>()));

            IVisitBalanceBaseApplicationService svc = builder.CreateService();
            svc.SetTotalBalance(visitDto);

            Assert.AreEqual(11m, visitDto.TotalBalance);
        }
        
        [Test]
        public void GetTotalBalance_Sums()
        {
            IList<VisitBalanceResult> visitBalances = new List<VisitBalanceResult>
            {
                new VisitBalanceResult {CurrentBalance = 6m, VisitId = 1},
                new VisitBalanceResult {CurrentBalance = 11m, VisitId = 2},
                new VisitBalanceResult {CurrentBalance = 16m, VisitId = 3},
                new VisitBalanceResult {CurrentBalance = 21m, VisitId = 4}
            };

            IList<PaymentAllocationVisitAmountResult> unclearedPayments = new List<PaymentAllocationVisitAmountResult>
            {
                new PaymentAllocationVisitAmountResult {ActualAmount = 5m, VisitId = 1},
                new PaymentAllocationVisitAmountResult {ActualAmount = 10m, VisitId = 2},
                new PaymentAllocationVisitAmountResult {ActualAmount = 15m, VisitId = 3},
                new PaymentAllocationVisitAmountResult {ActualAmount = 20m, VisitId = 4}
            };

            IVisitBalanceBaseApplicationService svc = this.CreateService(visitBalances, unclearedPayments);

            decimal balance = svc.GetTotalBalances(VpGuarantorId, visitBalances.Select(x => x.VisitId).ToList()).Sum(x => x.TotalBalance);

            Assert.AreEqual(4m, balance);
        }

        [TestCase(00.00, 0.00, 0.00, 00.00)]
        [TestCase(10.00, 0.00, 0.00, 10.00)]
        [TestCase(10.00, 1.00, 0.00, 09.00)]
        [TestCase(10.00, 1.00, 2.00, 11.00)]
        [TestCase(10.00, 0.00, 2.00, 12.00)]
        public void GetTotalBalances_Sums_SingleVisit(decimal visitBalance, decimal unclearedPayment, decimal interestDue, decimal expected)
        {
            const int visitId = 1;

            IList<VisitBalanceResult> visitBalances = new VisitBalanceResult {CurrentBalance = visitBalance, VisitId = visitId}.ToListOfOne();
            IList<PaymentAllocationVisitAmountResult> unclearedPayments = new PaymentAllocationVisitAmountResult {ActualAmount = unclearedPayment, VisitId = visitId}.ToListOfOne();
            IList<FinancePlanVisitInterestDueResult> interestDues = new FinancePlanVisitInterestDueResult {InterestDue = interestDue, VisitId = visitId}.ToListOfOne();

            IVisitBalanceBaseApplicationService svc = this.CreateService(visitBalances, unclearedPayments, interestDues);

            IList<VisitBalanceBaseDto> balances = svc.GetTotalBalances(VpGuarantorId, visitId.ToListOfOne());

            Assert.AreEqual(expected, balances.Sum(x => x.TotalBalance));
        }
        
        [Test]
        public void GetTotalBalances_Sums_MultipleVisits_CurrentBalance_WithUnclearedPayments()
        {
            IList<VisitBalanceResult> visitBalances = new List<VisitBalanceResult>
            {
                new VisitBalanceResult {CurrentBalance = 6m, VisitId = 1},
                new VisitBalanceResult {CurrentBalance = 11m, VisitId = 2},
                new VisitBalanceResult {CurrentBalance = 16m, VisitId = 3},
                new VisitBalanceResult {CurrentBalance = 21m, VisitId = 4}
            };

            IList<PaymentAllocationVisitAmountResult> unclearedPayments = new List<PaymentAllocationVisitAmountResult>
            {
                new PaymentAllocationVisitAmountResult {ActualAmount = 5m, VisitId = 1},
                new PaymentAllocationVisitAmountResult {ActualAmount = 10m, VisitId = 2},
                new PaymentAllocationVisitAmountResult {ActualAmount = 15m, VisitId = 3},
                new PaymentAllocationVisitAmountResult {ActualAmount = 20m, VisitId = 4}
            };

            IVisitBalanceBaseApplicationService svc = this.CreateService(visitBalances, unclearedPayments);

            IList<VisitBalanceBaseDto> balances = svc.GetTotalBalances(VpGuarantorId, visitBalances.Select(x => x.VisitId).ToList());

            Assert.AreEqual(4m, balances.Sum(x => x.TotalBalance));
        }

        [Test]
        public void GetTotalBalances_Sums_MultipleVisits_CurrentBalance_WithUnclearedPaymentsAndInterest()
        {
            IList<VisitBalanceResult> visitBalances = new List<VisitBalanceResult>
            {
                new VisitBalanceResult {CurrentBalance = 5m, VisitId = 1},
                new VisitBalanceResult {CurrentBalance = 10m, VisitId = 2},
                new VisitBalanceResult {CurrentBalance = 15m, VisitId = 3},
                new VisitBalanceResult {CurrentBalance = 20m, VisitId = 4}
            };

            IList<PaymentAllocationVisitAmountResult> unclearedPayments = new List<PaymentAllocationVisitAmountResult>
            {
                new PaymentAllocationVisitAmountResult {ActualAmount = 5m, VisitId = 1},
                new PaymentAllocationVisitAmountResult {ActualAmount = 10m, VisitId = 2},
                new PaymentAllocationVisitAmountResult {ActualAmount = 15m, VisitId = 3},
                new PaymentAllocationVisitAmountResult {ActualAmount = 20m, VisitId = 4}
            };

            IList<FinancePlanVisitInterestDueResult> interestDues = new List<FinancePlanVisitInterestDueResult>
            {
                new FinancePlanVisitInterestDueResult {InterestDue = 1m, VisitId = 1},
                new FinancePlanVisitInterestDueResult {InterestDue = 2m, VisitId = 2},
                new FinancePlanVisitInterestDueResult {InterestDue = 3m, VisitId = 3},
                new FinancePlanVisitInterestDueResult {InterestDue = 4m, VisitId = 4}
            };

            IVisitBalanceBaseApplicationService svc = this.CreateService(visitBalances, unclearedPayments, interestDues);

            IList<VisitBalanceBaseDto> balances = svc.GetTotalBalances(VpGuarantorId, visitBalances.Select(x => x.VisitId).ToList());

            Assert.AreEqual(10m, balances.Sum(x => x.TotalBalance));
        }

        [Test]
        public void GetTotalBalances_Sums_MultipleVisits_CurrentBalanceOnly()
        {
            IList<VisitBalanceResult> visitBalances = new List<VisitBalanceResult>
            {
                new VisitBalanceResult {CurrentBalance = 0m, VisitId = 1},
                new VisitBalanceResult {CurrentBalance = 10m, VisitId = 2},
                new VisitBalanceResult {CurrentBalance = 11m, VisitId = 3},
                new VisitBalanceResult {CurrentBalance = 12m, VisitId = 4}
            };

            IVisitBalanceBaseApplicationService svc = this.CreateService(visitBalances);

            IList<VisitBalanceBaseDto> balances = svc.GetTotalBalances(VpGuarantorId, visitBalances.Select(x => x.VisitId).ToList());

            Assert.AreEqual(33m, balances.Sum(x => x.TotalBalance));
        }

        [Test]
        public void GetTotalBalances_Sums_MultipleVisits_InterestOnly()
        {
            IList<VisitBalanceResult> visitBalances = new List<VisitBalanceResult>
            {
                new VisitBalanceResult {CurrentBalance = 0m, VisitId = 1},
                new VisitBalanceResult {CurrentBalance = 0m, VisitId = 2},
                new VisitBalanceResult {CurrentBalance = 0m, VisitId = 3},
                new VisitBalanceResult {CurrentBalance = 0m, VisitId = 4}
            };

            IList<FinancePlanVisitInterestDueResult> interestDues = new List<FinancePlanVisitInterestDueResult>
            {
                new FinancePlanVisitInterestDueResult {InterestDue = 1m, VisitId = 1},
                new FinancePlanVisitInterestDueResult {InterestDue = 2m, VisitId = 2},
                new FinancePlanVisitInterestDueResult {InterestDue = 3m, VisitId = 3},
                new FinancePlanVisitInterestDueResult {InterestDue = 4m, VisitId = 4}
            };

            IVisitBalanceBaseApplicationService svc = this.CreateService(visitBalances, null, interestDues);

            IList<VisitBalanceBaseDto> balances = svc.GetTotalBalances(VpGuarantorId, visitBalances.Select(x => x.VisitId).ToList());

            Assert.AreEqual(10m, balances.Sum(x => x.TotalBalance));
        }

        [TestCase(new[]{-1})]
        [TestCase(new[]{1})]
        [TestCase(new[]{1, 2})]
        [TestCase(new[]{1, 2, 3})]
        [TestCase(new[]{1, 2, 3, 4})]
        public void GetTotalBalances_Sums_FinancedVisits_WithUnclearedPaymentsAndInterest(int[] financedVisitIds)
        {
            IList<VisitBalanceResult> visitBalances = new List<VisitBalanceResult>
            {
                new VisitBalanceResult {CurrentBalance = 5m, VisitId = 1},
                new VisitBalanceResult {CurrentBalance = 10m, VisitId = 2},
                new VisitBalanceResult {CurrentBalance = 15m, VisitId = 3},
                new VisitBalanceResult {CurrentBalance = 20m, VisitId = 4}
            };

            IList<PaymentAllocationVisitAmountResult> unclearedPayments = new List<PaymentAllocationVisitAmountResult>
            {
                new PaymentAllocationVisitAmountResult {ActualAmount = 5m, VisitId = 1},
                new PaymentAllocationVisitAmountResult {ActualAmount = 10m, VisitId = 2},
                new PaymentAllocationVisitAmountResult {ActualAmount = 15m, VisitId = 3},
                new PaymentAllocationVisitAmountResult {ActualAmount = 20m, VisitId = 4}
            };

            IList<FinancePlanVisitInterestDueResult> interestDues = new List<FinancePlanVisitInterestDueResult>
            {
                new FinancePlanVisitInterestDueResult {InterestDue = 1m, VisitId = 1},
                new FinancePlanVisitInterestDueResult {InterestDue = 2m, VisitId = 2},
                new FinancePlanVisitInterestDueResult {InterestDue = 3m, VisitId = 3},
                new FinancePlanVisitInterestDueResult {InterestDue = 4m, VisitId = 4}
            };

            IList<FinancePlanVisitPrincipalAmountResult> principalAmounts = visitBalances.Join(unclearedPayments,
                    v => v.VisitId,
                    p => p.VisitId,
                    (v, p) => new FinancePlanVisitPrincipalAmountResult
                    {
                        VisitId = v.VisitId,
                        Amount = v.CurrentBalance - p.ActualAmount
                    })
                .Where(x => financedVisitIds.Contains(x.VisitId))
                .ToList();

            IVisitBalanceBaseApplicationService svc = this.CreateService(visitBalances, unclearedPayments, interestDues, principalAmounts);

            IList<VisitBalanceBaseDto> balances = svc.GetTotalBalances(VpGuarantorId, visitBalances.Select(x => x.VisitId).ToList());

            Assert.AreEqual(10m, balances.Sum(x => x.TotalBalance));
        }
        
        private IVisitBalanceBaseApplicationService CreateService(IList<VisitBalanceResult> visitBalances, IList<PaymentAllocationVisitAmountResult> unclearedPayments = null, IList<FinancePlanVisitInterestDueResult> interestDues = null, IList<FinancePlanVisitPrincipalAmountResult> financePlanVisitPrincipalAmountResults = null)
        {
            unclearedPayments = unclearedPayments ?? new List<PaymentAllocationVisitAmountResult>();
            interestDues = interestDues ?? new List<FinancePlanVisitInterestDueResult>();

            VisitBalanceBaseApplicationServiceMockBuilder builder = new VisitBalanceBaseApplicationServiceMockBuilder();
            builder.Setup(x => x.VisitServiceMock.Setup(m => m.GetVisitBalances(It.IsAny<int>(), It.IsAny<IList<int>>())).Returns(() => visitBalances));
            builder.Setup(x => x.PaymentAllocationServiceMock.Setup(m => m.GetNonInterestUnclearedPaymentAmountsForVisits(It.IsAny<IList<int>>(),null, null)).Returns(() => unclearedPayments));
            builder.Setup(x => x.FinancePlanServiceMock.Setup(m => m.GetInterestDueForVisits(It.IsAny<IList<int>>())).Returns(() => interestDues));
            builder.Setup(x => x.FinancePlanServiceMock.Setup(m => m.GetInterestForVisits(It.IsAny<IList<int>>(), null, null)).Returns(() => interestDues));
            builder.Setup(x => x.FinancePlanServiceMock.Setup(m => m.GetPrincipalAmountsForVisits(It.IsAny<IList<int>>())).Returns(() => financePlanVisitPrincipalAmountResults ?? new List<FinancePlanVisitPrincipalAmountResult>()));

            return builder.CreateService();
        }
    }
}
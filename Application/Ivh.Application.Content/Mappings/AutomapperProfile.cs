﻿namespace Ivh.Application.Content.Mappings
{
    using AutoMapper;
    using Common.Dtos;
    using Domain.Content.Entities;
    using Ivh.Common.Base.Utilities.Extensions;

    public class AutomapperProfile : Profile
    {
        public AutomapperProfile()
        {
            this.CreateMapBidirectional<CmsVersion, CmsVersionDto>();
            this.CreateMapBidirectional<CmsRegion, CmsRegionDto>();

            this.CreateMap<ContentMenu, ContentMenuDto>();
            this.CreateMap<ContentMenuItem, ContentMenuItemDto>()
                .ForMember(dest => dest.ContentMenu, opts => opts.Ignore());

            this.CreateMap<Image, ImageDto>();
        }
    }
}
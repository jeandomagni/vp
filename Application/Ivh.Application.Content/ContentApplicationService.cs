﻿namespace Ivh.Application.Content
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading;
    using System.Threading.Tasks;
    using AutoMapper;
    using Base.Common.Interfaces;
    using Base.Services;
    using Common.Dtos;
    using Common.Interfaces;
    using Domain.Content.Entities;
    using Domain.Content.Interfaces;
    using Ivh.Common.Base.Utilities.Helpers;
    using Ivh.Common.JobRunner.Common.Interfaces;
    using Ivh.Common.VisitPay.Enums;
    using Ivh.Common.VisitPay.Jobs;

    public class ContentApplicationService : ApplicationService, IContentApplicationService
    {
        private readonly Lazy<IContentService> _contentService;

        public ContentApplicationService(
            Lazy<IContentService> contentService,
            Lazy<IApplicationServiceCommonService> commonServices) : base(commonServices)
        {
            this._contentService = contentService;
        }

        /// <summary>
        /// Should only be called by the ClientDataApi
        /// </summary>
        public CmsVersionDto ApiGetCurrentVersion(CmsRegionEnum cmsRegion, string locale, bool replaceTokens = true, IDictionary<string, string> additionalValues = null)
        {
            CmsVersion cmsVersion = this._contentService.Value.ApiGetCurrentVersion(cmsRegion, replaceTokens, locale, additionalValues);
            return cmsVersion == null ? EmptyCmsVersion : Mapper.Map<CmsVersionDto>(cmsVersion);
        }

        public async Task<CmsVersionDto> GetCurrentVersionAsync(CmsRegionEnum cmsRegion, bool replaceTokens = true, IDictionary<string, string> additionalValues = null)
        {
            CmsVersion cmsVersion = await this._contentService.Value.GetCurrentVersionAsync(cmsRegion, replaceTokens, additionalValues);
            return cmsVersion == null ? EmptyCmsVersion : Mapper.Map<CmsVersionDto>(cmsVersion);
        }

        /// <summary>
        /// Should only be called by the ClientDataApi
        /// </summary>
        public CmsVersionDto ApiGetSpecificVersion(CmsRegionEnum cmsRegion, int cmsVersionId, string locale, bool replaceTokens = true, IDictionary<string, string> additionalValues = null)
        {
            CmsVersion cmsVersion = this._contentService.Value.ApiGetSpecificVersion(cmsRegion, cmsVersionId, replaceTokens, locale, additionalValues);
            return cmsVersion == null ? EmptyCmsVersion : Mapper.Map<CmsVersionDto>(cmsVersion);
        }

        public async Task<CmsVersionDto> GetSpecificVersionAsync(CmsRegionEnum cmsRegion, int cmsVersionId, bool replaceTokens = true, IDictionary<string, string> additionalValues = null)
        {
            CmsVersion cmsVersion = await this._contentService.Value.GetSpecificVersionAsync(cmsRegion, cmsVersionId, replaceTokens, additionalValues);
            return cmsVersion == null ? EmptyCmsVersion : Mapper.Map<CmsVersionDto>(cmsVersion);
        }

        public async Task<CmsRegionEnum?> GetSsoProviderSpecificCmsRegionAsync(SsoProviderEnum ssoProvider, SsoProviderSpecificCmsRegionEnum providerSpecificCmsRegion)
        {
            return await Task.Run(() =>
            {
                switch (ssoProvider)
                {
                    case SsoProviderEnum.OpenEpic:
                        return this.GetOpenEpicSpecificCmsRegion(providerSpecificCmsRegion);
                    default:
                        return default(CmsRegionEnum?);
                }
            });
        }

        public async Task<CmsVersionDto> GetLabelAsync(string cmsRegionName, string defaultLabel)
        {
            try
            {
                CmsVersion cmsVersion = await this._contentService.Value.GetLabelAsync(cmsRegionName, defaultLabel);

                return Mapper.Map<CmsVersionDto>(cmsVersion);
            }
            catch (Exception e)
            {
                this.LoggingService.Value.Fatal(() => $"contentApplicationService::GetCmsRegionLabel({cmsRegionName},{defaultLabel}) - {ExceptionHelper.AggregateExceptionToString(e)}");
                return EmptyCmsVersion;
            }
        }

        public async Task<ContentMenuDto> GetContentMenuAsync(ContentMenuEnum contentMenuEnum, bool isUserAuthenticated)
        {
            ContentMenu contentMenu = await this._contentService.Value.GetContentMenuAsync(contentMenuEnum);
            ContentMenuDto contentMenuDto = Mapper.Map<ContentMenuDto>(contentMenu ?? new ContentMenu());

            if (!isUserAuthenticated)
            {
                contentMenuDto.ContentMenuItems = contentMenuDto.ContentMenuItems.Where(x => x.IsPublic).ToList();
            }

            IDictionary<string, string> replacementValues = this.ClientReplacementValues();

            bool isClientData = this.ApplicationSettingsService.Value.Application.Value == ApplicationEnum.ClientDataApi;

            foreach (ContentMenuItemDto contentMenuItemDto in contentMenuDto.ContentMenuItems)
            {
                if (!isClientData)
                {
                    string linkText = await this._contentService.Value.GetTextRegionAsync(contentMenuItemDto.LinkText);
                    contentMenuItemDto.LinkText = linkText;
                }

                contentMenuItemDto.LinkUrl = ReplacementUtility.Replace(contentMenuItemDto.LinkUrl, replacementValues, null, false);
            }

            return contentMenuDto;
        }

        /// <summary>
        /// Should only be called by the ClientDataApi
        /// </summary>
        public IDictionary<int, int> ApiGetCmsVersionNumbers(CmsRegionEnum cmsRegion, string locale)
        {
            return this._contentService.Value.ApiGetCmsVersionNumbers(cmsRegion, locale);
        }

        public async Task<IDictionary<int, int>> GetCmsVersionNumbersAsync(CmsRegionEnum cmsRegion)
        {
            return await this._contentService.Value.GetCmsVersionNumbersAsync(cmsRegion);
        }

        /// <summary>
        /// Should only be called by the ClientDataApi
        /// </summary>
        public CmsVersionDto ApiGetVersionByRegionNameAndType(string cmsRegionName, CmsTypeEnum cmsType, string defaultLabel, string locale)
        {
            CmsVersion cmsVersion = this._contentService.Value.ApiGetVersionByRegionNameAndType(cmsRegionName, cmsType, defaultLabel, locale);
            return cmsVersion == null ? EmptyCmsVersion : Mapper.Map<CmsVersionDto>(cmsVersion);
        }

        public async Task<CmsVersionDto> GetVersionByRegionNameAndTypeAsync(string cmsRegionName, CmsTypeEnum cmsType, string defaultLabel)
        {
            CmsVersion cmsVersion = await this._contentService.Value.GetVersionByRegionNameAndTypeAsync(cmsRegionName, cmsType, defaultLabel);
            return cmsVersion == null ? EmptyCmsVersion : Mapper.Map<CmsVersionDto>(cmsVersion);
        }

        public IDictionary<string, string> ClientReplacementValues()
        {
            return this._contentService.Value.ClientReplacementValues();
        }

        public string GetTextRegion(string textRegion, IDictionary<string, string> additionalValues = null)
        {
            return this._contentService.Value.GetTextRegion(textRegion, additionalValues);
        }

        public async Task<string> GetTextRegionAsync(string textRegion, IDictionary<string, string> additionalValues = null)
        {
            return await this._contentService.Value.GetTextRegionAsync(textRegion, additionalValues);
        }

        public async Task<IDictionary<string, string>> GetTextRegionsAsync(params string[] textRegions)
        {
            return await this._contentService.Value.GetTextRegionsAsync(textRegions);
        }

        public async Task<bool> InvalidateCacheAsync()
        {
            return await this._contentService.Value.InvalidateCacheAsync();
        }

        private CmsRegionEnum? GetOpenEpicSpecificCmsRegion(SsoProviderSpecificCmsRegionEnum cmsRegion)
        {
            switch (cmsRegion)
            {
                case SsoProviderSpecificCmsRegionEnum.LandingContentSso:
                    return CmsRegionEnum.SsoMyChartLandingContent;
                case SsoProviderSpecificCmsRegionEnum.RegistrationWelcomeInstruction:
                    return CmsRegionEnum.SsoMyChartRegistrationWelcomeInstruction;
                default:
                    return null;
            }
        }

        private static CmsVersionDto EmptyCmsVersion => new CmsVersionDto { ContentTitle = string.Empty, ContentBody = string.Empty };

        void IJobRunnerService<ContentApiTesterJob>.Execute(DateTime begin, DateTime end, string[] args)
        {
            while (true)
            {
                foreach(CmsRegionEnum y in Enum.GetValues(typeof(CmsRegionEnum)).Cast<CmsRegionEnum>())
                {
                    Task<CmsVersionDto> x = this.GetCurrentVersionAsync(y);
                    Thread.Sleep(10);
                };
            }
        }
    }
}
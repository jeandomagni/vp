﻿namespace Ivh.Application.Content
{
    using System;
    using System.Threading.Tasks;
    using AutoMapper;
    using Common.Dtos;
    using Common.Interfaces;
    using Domain.Content.Interfaces;
    
    public class ImageApplicationService : IImageApplicationService
    {
        private readonly Lazy<IImageService> _imageService;

        public static readonly object Lock = new object();

        public ImageApplicationService(
            Lazy<IImageService> imageService)
        {
            this._imageService = imageService;
        }

        public async Task<ImageDto> GetImageAsync(string fileName)
        {
            return Mapper.Map<ImageDto>(await this._imageService.Value.GetImageAsync(fileName));
        }
    }
}

﻿namespace Ivh.Application.Content
{
    using System;
    using Common.Interfaces;
    using Domain.Content.Interfaces;


    public class ThemeApplicationService : IThemeApplicationService
    {
        private readonly Lazy<IThemeService> _themeService;

        public ThemeApplicationService(
           Lazy<IThemeService> themeService)
        {
            this._themeService = themeService;
        }

        public string GetStylesheet(string requestedFile)
        {
            return this._themeService.Value.GetStylesheet(requestedFile);
        }

        public string GetStylesheetHash(string requestedFile)
        {
            return this._themeService.Value.GetStylesheetHash(requestedFile);
        }
    }
}
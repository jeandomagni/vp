﻿namespace Ivh.Application.Content.Modules
{
    using Autofac;
    using Common.Interfaces;

    public class Module : Autofac.Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<ContentApplicationService>().As<IContentApplicationService>();
            builder.RegisterType<ThemeApplicationService>().As<IThemeApplicationService>();
            builder.RegisterType<ImageApplicationService>().As<IImageApplicationService>();
        }
    }
}
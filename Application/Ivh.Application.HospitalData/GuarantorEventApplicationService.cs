namespace Ivh.Application.HospitalData
{
    using System;
    using Base.Common.Interfaces;
    using Base.Services;
    using Common.Interfaces;
    using Domain.Guarantor.Entities;
    using Domain.Guarantor.Interfaces;
    using Domain.Visit.Interfaces;
    using Ivh.Common.ServiceBus.Attributes;
    using Ivh.Common.ServiceBus.Enums;
    using Ivh.Common.VisitPay.Messages.HospitalData;
    using Ivh.Common.VisitPay.Messages.HospitalData.Dtos;
    using MassTransit;

    public class GuarantorEventApplicationService : ApplicationService, IGuarantorEventApplicationService
    {
        private readonly Lazy<IGuarantorService> _guarantorService;
        private readonly Lazy<IVisitService> _visitService;

        public GuarantorEventApplicationService(
            Lazy<IApplicationServiceCommonService> applicationServiceCommonService,
            Lazy<IGuarantorService> guarantorService,
            Lazy<IVisitService> visitService) : base(applicationServiceCommonService)
        {
            this._guarantorService = guarantorService;
            this._visitService = visitService;
        }

        public void OnHsGuarantorAcknowledgementMessage(AppHsGuarantorAcknowledgementMessage message)
        {
            HsGuarantorAcknowledgementDto acknowledgement = message.HsGuarantorAcknowledgement;
            Guarantor guarantor = this._guarantorService.Value.GetGuarantor(acknowledgement.VpGuarantorId);

            if (guarantor == null)
            {
                throw new Exception($"Could not find a guarantor record with id {acknowledgement.VpGuarantorId}.");
            }

            this._guarantorService.Value.SetToActiveHsAcknowledgedPendingVisits(guarantor);

            this._guarantorService.Value.InsertMatchingHsGuarantor(acknowledgement, guarantor);
            if (this._visitService.Value.GetVisits(guarantor.VpGuarantorId).Count > 0)
            {
                this._guarantorService.Value.SetToActiveVisitsLoaded(guarantor);
            }
        }

        #region Message Consumers

        [UniversalConsumer(UniversalQueuesEnum.VisitPayProcessor, UniversalPriorityEnum.Priority30)]
        public void ConsumeMessage(AppHsGuarantorAcknowledgementMessage message, ConsumeContext<AppHsGuarantorAcknowledgementMessage> consumeContext)
        {
            this.OnHsGuarantorAcknowledgementMessage(message);
        }

        public bool IsMessageValid(AppHsGuarantorAcknowledgementMessage message, ConsumeContext<AppHsGuarantorAcknowledgementMessage> consumeContext)
        {
            bool isValid = true;
            return isValid;
        }

        #endregion
    }
}
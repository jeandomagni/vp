﻿
namespace Ivh.Application.HospitalData
{
    using Base.Common.Interfaces;
    using Base.Services;
    using Common.Interfaces;
    using Domain.HospitalData.Outbound.Entities;
    using Domain.HospitalData.Outbound.Enums;
    using Domain.HospitalData.Outbound.Interfaces;
    using Ivh.Common.Data.Connection.Connections;
    using Ivh.Common.Data.Interfaces;
    using Ivh.Common.ServiceBus.Attributes;
    using Ivh.Common.ServiceBus.Enums;
    using Ivh.Common.VisitPay.Messages.HospitalData;
    using MassTransit;
    using NHibernate;
    using System;

    public class VpOutboundPaperFileApplicationService : ApplicationService, IVpOutboundPaperFileApplicationService
    {
        private readonly Lazy<IOutboundPaperFileService> _outboundPaperFileService;

        private readonly ISession _session;

        public VpOutboundPaperFileApplicationService(
            Lazy<IApplicationServiceCommonService> applicationServiceCommonService,
            Lazy<IOutboundPaperFileService> outboundPaperFileService,
            ISessionContext<CdiEtl> sessionContext) : base(applicationServiceCommonService)
        {
            this._outboundPaperFileService = outboundPaperFileService;
            this._session = sessionContext.Session;
        }

        [UniversalConsumer(UniversalQueuesEnum.HsDataProcessor, UniversalPriorityEnum.Priority30)]
        public void ConsumeMessage(OutboundPaperFileMessage message, ConsumeContext<OutboundPaperFileMessage> consumeContext)
        {
            VpOutboundFile vpOutboundFile = new VpOutboundFile()
            {
                FileName = message.FileName,
                ExternalKey = message.FileStorageExternalKey.ToString(),
                VpOutboundFileTypeId = message.FileTypeId
            };
            this.CreateOutboundFile(vpOutboundFile);
        }

        public bool IsMessageValid(OutboundPaperFileMessage message, ConsumeContext<OutboundPaperFileMessage> consumeContext)
        {
            bool isValid = true;
            return isValid;
        }

        private void CreateOutboundFile(VpOutboundFile vpOutboundFile)
        {
            vpOutboundFile.InsertDate = DateTime.UtcNow;
            this._outboundPaperFileService.Value.CreateOutboundFile(vpOutboundFile);
        }
    }
}

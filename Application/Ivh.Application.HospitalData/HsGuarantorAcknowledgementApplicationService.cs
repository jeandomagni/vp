﻿namespace Ivh.Application.HospitalData
{
    using System;
    using AutoMapper;
    using Base.Common.Interfaces;
    using Base.Services;
    using Common.Interfaces;
    using Domain.HospitalData.HsGuarantor.Entities;
    using Ivh.Common.ServiceBus.Attributes;
    using Ivh.Common.ServiceBus.Enums;
    using Ivh.Common.VisitPay.Messages.HospitalData;
    using Ivh.Common.VisitPay.Messages.HospitalData.Dtos;
    using MassTransit;

    public class HsGuarantorAcknowledgementApplicationService : ApplicationService, IHsGuarantorAcknowledgementApplicationService
    {
        public HsGuarantorAcknowledgementApplicationService(
            Lazy<IApplicationServiceCommonService> applicationServiceCommonService) : base(applicationServiceCommonService)
        {
        }

        public void ProcessNewlyAcknowledgedHsGuarantor(EtlHsGuarantorAcknowledgementMessage message)
        {
            HsGuarantorAcknowledgementDto acknowledgement = message.HsGuarantorAcknowledgement;
            this.PublishAppGuarantorAcknowledgementMessage(acknowledgement);
        }

        private void PublishAppGuarantorAcknowledgementMessage(HsGuarantorAcknowledgementDto acknowledgement)
        {
            AppHsGuarantorAcknowledgementMessage hsGuarantorAcknowledgementMessage = new AppHsGuarantorAcknowledgementMessage
            {
                HsGuarantorAcknowledgement = acknowledgement
            };

            this.Bus.Value.PublishMessage(hsGuarantorAcknowledgementMessage).Wait();
        }

        private void PublishEtlGuarantorAcknowledgementMessage(HsGuarantorAcknowledgementInfo acknowledgementInfo)
        {
            EtlHsGuarantorAcknowledgementMessage hsGuarantorAcknowledgementMessage = new EtlHsGuarantorAcknowledgementMessage
            {
                HsGuarantorAcknowledgement = Mapper.Map<HsGuarantorAcknowledgementDto>(acknowledgementInfo)
            };

            this.Bus.Value.PublishMessage(hsGuarantorAcknowledgementMessage).Wait();
        }

        #region Message Consumers

        [UniversalConsumer(UniversalQueuesEnum.HsDataProcessor, UniversalPriorityEnum.Priority30)]
        public void ConsumeMessage(EtlHsGuarantorAcknowledgementMessage message, ConsumeContext<EtlHsGuarantorAcknowledgementMessage> consumeContext)
        {
            this.ProcessNewlyAcknowledgedHsGuarantor(message);
        }

        public bool IsMessageValid(EtlHsGuarantorAcknowledgementMessage message, ConsumeContext<EtlHsGuarantorAcknowledgementMessage> consumeContext)
        {
            bool isValid = true;
            return isValid;
        }

        #endregion
    }
}
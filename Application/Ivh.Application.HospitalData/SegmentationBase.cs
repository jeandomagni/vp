﻿namespace Ivh.Application.HospitalData
{
    using System;
    using System.Collections.Generic;
    using System.Threading;
    using Autofac;
    using Common.Interfaces;
    using Domain.HospitalData.Scoring.Interfaces;
    using Domain.Logging.Interfaces;
    using Ivh.Common.Base.Utilities.Helpers;
    using Ivh.Common.DependencyInjection;
    using MassTransit;

    public abstract class SegmentationBase<T> : ISegmentationBaseService
    {
        private readonly Lazy<ILoggingService> _loggingService;
        private readonly Lazy<IMetricsProvider> _metricsProvider;
        private int _segmentationSuccesses;
        private int _segmentationFailures;
        private int _segmentationLastMessage;

        protected string SuccessStatName;
        protected string FailureStatName;

        public int SegmentationTotal { get; set; }
        public int SegmentationBatchId { get; set; }

        public int SegmentationSuccesses()
        {
            return this._segmentationSuccesses;
        }

        public int SegmentationFailures()
        {
            return this._segmentationFailures;
        }

        public void InitializeService()
        {
            this._segmentationSuccesses = 0;
            this._segmentationFailures = 0;
            this._segmentationLastMessage = 0;
        }

        protected SegmentationBase(Lazy<ILoggingService> loggingService,
            Lazy<IMetricsProvider> metricsProvider)
        {
            this._loggingService = loggingService;
            this._metricsProvider = metricsProvider;
        }

        protected abstract void ProcessSegmentation(List<T> segmentationDtoList);
        protected abstract void UpdateSegmentationHistory();

        public bool ProcessSegmentationBatch(List<T> segmentationList, 
                        int scoringBatchId, 
                        string currentState, 
                        string eventName, 
                        Guid correlationId, 
                        Guid processId)
        {
            using (ILifetimeScope lifetimeScope = IvinciContainer.Instance.Container().BeginLifetimeScope())
            {
                IScoringSagaLogger scoringSagaLogger = lifetimeScope.Resolve<IScoringSagaLogger>();
                try
                {
                    this.LogStartTime(scoringSagaLogger, scoringBatchId, currentState, eventName, correlationId, processId);
                    this.ProcessSegmentation(segmentationList);
                    this.LogEndTime(scoringSagaLogger, scoringBatchId, currentState, eventName, correlationId, processId);
                    Interlocked.Increment(ref this._segmentationSuccesses);
                    this._metricsProvider.Value.Increment(this.SuccessStatName);
                }
                catch (Exception e)
                {
                    this._loggingService.Value.Fatal(() => ExceptionHelper.AggregateExceptionToString(e));
                    Interlocked.Increment(ref this._segmentationFailures);
                    this._metricsProvider.Value.Increment(this.FailureStatName);
                }

                if (!this.IsSegmentationComplete())
                {
                    return false;
                }

                if (Interlocked.Exchange(ref this._segmentationLastMessage, 1) == 0)
                {
                    // this is the last segmentation message
                    try
                    {
                        this.LogStartTime(scoringSagaLogger, scoringBatchId, currentState, $"{eventName}-UpdateSegmentationHistory", correlationId, processId);
                        this.UpdateSegmentationHistory();
                        this.LogEndTime(scoringSagaLogger, scoringBatchId, currentState, $"{eventName}-UpdateSegmentationHistory", correlationId, processId);
                    }
                    catch (Exception e)
                    {
                        this._loggingService.Value.Fatal(() => $"UpdateSegmentationHistory Error for event {eventName}: {e}");
                    }

                    return true;
                }

                return false;
            }
        }

        private void LogStartTime(IScoringSagaLogger scoringSagaLogger, int scoringBatchId, string state, string eventName, Guid correlationId, Guid? processId = null)
        {
            Guid id = processId ?? new Guid(NewId.Next().ToByteArray());
            scoringSagaLogger.LogStartTime(scoringBatchId, state, eventName, correlationId, id);
        }

        private void LogEndTime(IScoringSagaLogger scoringSagaLogger, int scoringBatchId, string state, string eventName, Guid correlationId, Guid processId)
        {
            scoringSagaLogger.LogEndTime(scoringBatchId, state, eventName, correlationId, processId);
        }

        private bool IsSegmentationComplete()
        {
            return this.SegmentationTotal > 0 && this._segmentationFailures + this._segmentationSuccesses >= this.SegmentationTotal;
        }

    }
}

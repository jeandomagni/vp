﻿namespace Ivh.Application.HospitalData
{
    using System;
    using System.Collections.Generic;
    using AutoMapper;
    using Base.Common.Interfaces;
    using Base.Services;
    using Common.Interfaces;
    using Domain.HospitalData.Change.Entities;
    using Domain.HospitalData.Change.Interfaces;
    using Domain.HospitalData.Visit.Entities;
    using Domain.HospitalData.Visit.Interfaces;
    using Ivh.Common.VisitPay.Enums;
    using Ivh.Common.VisitPay.Messages.HospitalData.Dtos;

    public class ChangeSetApplicationService : ApplicationService, IChangeSetApplicationService
    {
        private readonly Lazy<IVisitService> _visitService;
        private readonly Lazy<IChangeEventService> _changeEventService;
        private readonly Lazy<IOutboundVisitTransactionService> _outboundVisitTransactionService;
        private readonly Lazy<IOutboundVisitService> _outboundVisitService;

        public ChangeSetApplicationService(
            Lazy<IApplicationServiceCommonService> applicationServiceCommonService,
            Lazy<IVisitService> visitService,
            Lazy<IChangeEventService> changeEventService,
            Lazy<IOutboundVisitTransactionService> outboundVisitTransactionService,
            Lazy<IOutboundVisitService> outboundVisitService) : base(applicationServiceCommonService)
        {
            this._visitService = visitService;
            this._changeEventService = changeEventService;
            this._outboundVisitTransactionService = outboundVisitTransactionService;
            this._outboundVisitService = outboundVisitService;
        }

        public ChangeSetDto CreateChangeSetFromVisit(int visitId)
        {
            HsVisitDto hsVisitDto = this.GetVisit(visitId);

            VisitChangeDto visitChangeDto = Mapper.Map<VisitChangeDto>(hsVisitDto);

            ChangeSetDto changeSetDto = new ChangeSetDto
            {
                ChangeSetDateTime = DateTime.UtcNow,
                ChangeEvents = new List<ChangeEventDto>(),
                ChangeSetType = ChangeSetTypeEnum.Visit,
                VisitChanges = new List<VisitChangeDto> { visitChangeDto }
            };

            return changeSetDto;
        }

        public HsVisitDto GetVisit(int visitId)
        {
            Visit visit = this._visitService.Value.GetVisit(visitId);
            HsVisitDto visitDto = Mapper.Map<HsVisitDto>(visit);
            IList<ChangeEvent> changeEvents = this._changeEventService.Value.GetChangeEventsByVisit(visitId);

            visitDto.ChangeEvents = Mapper.Map<List<ChangeEventDto>>(changeEvents);
            visitDto.OutboundVisitTransactionDtos = Mapper.Map<List<VpOutboundVisitTransactionDto>>(this._outboundVisitTransactionService.Value.GetOutboundVisitTransactionsByVisit(visitId));
            visitDto.OutboundVisitDtos = Mapper.Map<List<VpOutboundVisitDto>>(this._outboundVisitService.Value.GetOutboundVisitsByVisit(visitId));

            return visitDto;
        }
    }
}

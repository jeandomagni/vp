﻿namespace Ivh.Application.HospitalData
{
    using System;
    using Base.Common.Interfaces;
    using Base.Services;
    using Common.Interfaces;
    using Domain.Settings.Interfaces;
    using Domain.HospitalData.Eob.Interfaces;
    using Domain.HospitalData.Inbound.Interfaces;
    using Domain.HospitalData.Outbound.Interfaces;
    using Domain.Powershell.Interfaces;
    using Domain.HospitalData.Scoring.Interfaces;
    using Ivh.Common.JobRunner.Common.Interfaces;
    using Ivh.Common.VisitPay.Enums;
    using Ivh.Common.VisitPay.Jobs;

    public class JobsApplicationService : ApplicationService, IJobsApplicationService
    {
        private readonly Lazy<ICdiEtlJobsProvider> _cdiEtlJobsProvider;
        private readonly Lazy<IPowershellService> _powershellService;
        private readonly Lazy<IEob835Service> _load835Service;
        private readonly Lazy<IFeatureService> _featureService;
        private readonly Lazy<IOutboundPaperFileService> _outboundPaperFileService;
        private readonly Lazy<IPaymentVendorFileService> _paymentVendorFileService;
        private readonly Lazy<IScoringGuarantorMatchingService> _scoringGuarantorMatchingService;

        public JobsApplicationService(
            Lazy<IApplicationServiceCommonService> applicationServiceCommonService,
            Lazy<ICdiEtlJobsProvider> cdiEtlJobsProvider,
            Lazy<IPowershellService> powershellService,
            Lazy<IEob835Service> load835Service,
            Lazy<IFeatureService> featureService,
            Lazy<IOutboundPaperFileService> outboundPaperFileService,
            Lazy<IPaymentVendorFileService> paymentVendorFileService,
            Lazy<IScoringGuarantorMatchingService> scoringGuarantorMatchingService)
            : base(applicationServiceCommonService)
        {
            this._cdiEtlJobsProvider = cdiEtlJobsProvider;
            this._powershellService = powershellService;
            this._load835Service = load835Service;
            this._featureService = featureService;
            this._outboundPaperFileService = outboundPaperFileService;
            this._paymentVendorFileService = paymentVendorFileService;
            this._scoringGuarantorMatchingService = scoringGuarantorMatchingService;
        }

        public void HSPersonDailyRecordCount()
        {
            this._cdiEtlJobsProvider.Value.HSPersonDailyRecordCount();
        }

        public void HSPersonDeltaRecordCount()
        {
            this._cdiEtlJobsProvider.Value.HSPersonDeltaRecordCount();
        }

        public void HSPersonSnapshotRecordCount()
        {
            this._cdiEtlJobsProvider.Value.HSPersonSnapshotRecordCount();
        }

        public void HSGuarantorStageRecordCount()
        {
            this._cdiEtlJobsProvider.Value.HSGuarantorStageRecordCount();
        }

        public void HSGuarantorRecordCdiCount()
        {
            this._cdiEtlJobsProvider.Value.HSGuarantorRecordCdiCount();
        }

        public void GuarantorSnapshotAndHist()
        {
            this._cdiEtlJobsProvider.Value.GuarantorSnapshotAndHist();
        }

        public void AccountSnapshotAndHist()
        {
            this._cdiEtlJobsProvider.Value.AccountSnapshotAndHist();
        }
        public void TransactionSnapshotAndHist()
        {
            this._cdiEtlJobsProvider.Value.TransactionSnapshotAndHist();
        }

        public string PublishCdiEtlToVpEtl()
        {
            return this._powershellService.Value.PublishCdiEtlToVpEtl();
        }

        public void QueueChangeEvents()
        {
            this._cdiEtlJobsProvider.Value.QueueChangeEvents();
        }

        public void HSGuarantorStageTruncInsert()
        {
            this._cdiEtlJobsProvider.Value.HSGuarantorStageTruncInsert();
        }

        public void VisitStageTruncInsert()
        {
            this._cdiEtlJobsProvider.Value.VisitStageTruncInsert();
        }
        public void VisitTransactionStageTruncInsert()
        {
            this._cdiEtlJobsProvider.Value.VisitTransactionStageTruncInsert();
        }

        public void HsGuarantor()
        {
            this._cdiEtlJobsProvider.Value.HsGuarantor();
        }

        public void Visit()
        {
            this._cdiEtlJobsProvider.Value.Visit();
        }
        public void VisitTransaction()
        {
            this._cdiEtlJobsProvider.Value.VisitTransaction();
        }

        public string Inbound_LoadFiles()
        {
            return this._powershellService.Value.Inbound_LoadFiles();
        }

        public string Inbound_LoadHistory()
        {
            return this._powershellService.Value.Inbound_LoadHistory();
        }

        public string Inbound_LoadSnapshotAndDelta()
        {
            return this._powershellService.Value.Inbound_LoadSnapshotAndDelta();
        }

        public string Inbound_LoadBaseStage()
        {
            return this._powershellService.Value.Inbound_LoadBaseStage();
        }

        public string Inbound_LoadChangeEvents_Part1()
        {
            return this._powershellService.Value.Inbound_LoadChangeEvents_Part1();
        }

        public string Inbound_LoadBase()
        {
            return this._powershellService.Value.Inbound_LoadBase();
        }

        public string Inbound_LoadChangeEvents_Part2()
        {
            return this._powershellService.Value.Inbound_LoadChangeEvents_Part2();
        }

        public void Inbound_LoadClaimsFiles()
        {
            if (this._featureService.Value.IsFeatureEnabled(VisitPayFeatureEnum.ExplanationOfBenefitsImport))
            {
                this._load835Service.Value.Load835File();
            }
        }

        public void Inbound_ScoringAndSegmentation()
        {
            this._scoringGuarantorMatchingService.Value.ExecuteScoring();
        }

        public void Inbound_PaymentVendorFile(string[] args)
        {
            int loadTrackerId = Convert.ToInt32(args[1]);
            string inboundStagingDirectory = args[2];
            this._paymentVendorFileService.Value.LoadFile(loadTrackerId, inboundStagingDirectory);
        }

        public void Outbound_PaperFiles(string[] args)
        {
            string destinationDirectory = args[1];
            int loadTrackerId = Convert.ToInt32(args[2]);
            this._outboundPaperFileService.Value.ExportFiles(destinationDirectory, loadTrackerId);
        }

        void IJobRunnerService<AccountSnapshotAndHistJobRunner>.Execute(DateTime begin, DateTime end, string[] args)
        {
            this.AccountSnapshotAndHist();
        }

        void IJobRunnerService<GuarantorSnapshotAndHistJobRunner>.Execute(DateTime begin, DateTime end, string[] args)
        {
            this.GuarantorSnapshotAndHist();
        }

        void IJobRunnerService<GuarantorStageTruncInsertJobRunner>.Execute(DateTime begin, DateTime end, string[] args)
        {
            this.HSGuarantorStageTruncInsert();
        }

        void IJobRunnerService<HsGuarantorJobRunner>.Execute(DateTime begin, DateTime end, string[] args)
        {
            this.HsGuarantor();
        }
        
        void IJobRunnerService<InboundLoadBaseJobRunner>.Execute(DateTime begin, DateTime end, string[] args)
        {
            this.Inbound_LoadBase();
        }

        void IJobRunnerService<InboundLoadBaseStageJobRunner>.Execute(DateTime begin, DateTime end, string[] args)
        {
            this.Inbound_LoadBaseStage();
        }
        
        void IJobRunnerService<InboundLoadChangeEventsPart1JobRunner>.Execute(DateTime begin, DateTime end, string[] args)
        {
            this.Inbound_LoadChangeEvents_Part1();
        }
        
        void IJobRunnerService<InboundLoadChangeEventsPart2JobRunner>.Execute(DateTime begin, DateTime end, string[] args)
        {
            this.Inbound_LoadChangeEvents_Part2();
        }

        void IJobRunnerService<InboundLoadClaimsDataJobRunner>.Execute(DateTime begin, DateTime end, string[] args)
        {
            this.Inbound_LoadClaimsFiles();
        }
        
        void IJobRunnerService<InboundLoadFilesJobRunner>.Execute(DateTime begin, DateTime end, string[] args)
        {
            this.Inbound_LoadFiles();
        }

        void IJobRunnerService<InboundLoadHistoryJobRunner>.Execute(DateTime begin, DateTime end, string[] args)
        {
            this.Inbound_LoadHistory();
        }

        void IJobRunnerService<InboundLoadSnapshotAndDeltaJobRunner>.Execute(DateTime begin, DateTime end, string[] args)
        {
            this.Inbound_LoadSnapshotAndDelta();
        }

        void IJobRunnerService<InboundScoringJobRunner>.Execute(DateTime begin, DateTime end, string[] args)
        {
            this.Inbound_ScoringAndSegmentation();
        }

        void IJobRunnerService<InboundPaymentVendorFileJobRunner>.Execute(DateTime begin, DateTime end, string[] args)
        {
            this.Inbound_PaymentVendorFile(args);
        }

        void IJobRunnerService<OutboundPaperFilesJobRunner>.Execute(DateTime begin, DateTime end, string[] args)
        {
            this.Outbound_PaperFiles(args);
        }
        
        void IJobRunnerService<PublishCdiEtlToVpEtlJobRunner>.Execute(DateTime begin, DateTime end, string[] args)
        {
            this.PublishCdiEtlToVpEtl();
        }

        void IJobRunnerService<JamsIntegrationTest>.Execute(DateTime begin, DateTime end, string[] args)
        {
            throw new System.NotImplementedException();
        }

        void IJobRunnerService<QueueChangeEventsJobRunner>.Execute(DateTime begin, DateTime end, string[] args)
        {
            this.QueueChangeEvents();
        }

        void IJobRunnerService<TransactionSnapshotAndHistJobRunner>.Execute(DateTime begin, DateTime end, string[] args)
        {
            this.TransactionSnapshotAndHist();
        }

        void IJobRunnerService<VisitJobRunner>.Execute(DateTime begin, DateTime end, string[] args)
        {
            this.Visit();
        }

        void IJobRunnerService<VisitStageTruncInsertJobRunner>.Execute(DateTime begin, DateTime end, string[] args)
        {
            this.VisitStageTruncInsert();
        }

        void IJobRunnerService<VisitTransactionJobRunner>.Execute(DateTime begin, DateTime end, string[] args)
        {
            this.VisitTransaction();
        }
        
        void IJobRunnerService<VisitTransactionStageTruncInsertJobRunner>.Execute(DateTime begin, DateTime end, string[] args)
        {
            this.VisitTransactionStageTruncInsert();
        }

        
    }
}

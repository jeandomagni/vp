﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ivh.Application.HospitalData
{
    using Base.Common.Interfaces;
    using Base.Services;
    using Common.Interfaces;
    using Domain.HospitalData.Eob.Interfaces;
    using Ivh.Common.ServiceBus.Attributes;
    using Ivh.Common.ServiceBus.Enums;
    using Ivh.Common.ServiceBus.Interfaces;
    using Ivh.Common.VisitPay.Messages.Eob;
    using MassTransit;

    public class Eob835ApplicationService : ApplicationService, IEob835ApplicationService, IUniversalConsumerService<EobInterchangeMigrationMessage>
    {
        private readonly Lazy<IEob835Service> _eob835Service;

        public Eob835ApplicationService(
            Lazy<IApplicationServiceCommonService> applicationServiceCommonService,
            Lazy<IEob835Service> eob835Service) : base(applicationServiceCommonService)
        {
            this._eob835Service = eob835Service;
        }
        
        [UniversalConsumer(UniversalQueuesEnum.VisitPayProcessor, UniversalPriorityEnum.Priority30)]
        public void ConsumeMessage(EobInterchangeMigrationMessage message, ConsumeContext<EobInterchangeMigrationMessage> consumeContext)
        {
            this._eob835Service.Value.MigrateInterchange(message.InterchangeControlHeaderTrailerId);
        }

        public bool IsMessageValid(EobInterchangeMigrationMessage message, ConsumeContext<EobInterchangeMigrationMessage> consumeContext)
        {
            return true;
        }
    }
}

﻿namespace Ivh.Application.HospitalData
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.IO;
    using System.Linq;
    using System.Reflection;
    using AutoMapper;
    using Base.Common.Interfaces;
    using Base.Services;
    using Common.Interfaces;
    using Domain.HospitalData.Scoring.Entities;
    using Domain.HospitalData.Scoring.Interfaces;
    using Domain.HospitalData.Segmentation.Entities;
    using Domain.HospitalData.Segmentation.Enums;
    using Domain.HospitalData.Segmentation.Interfaces;
    using Ivh.Common.Data;
    using Ivh.Common.Data.Connection.Connections;
    using Ivh.Common.Data.Interfaces;
    using Ivh.Common.VisitPay.Messages.HospitalData.Dtos;
    using NHibernate;

    public class HsSegmentationApplicationService : ApplicationService, IHsSegmentationApplicationService
    {
        private readonly Lazy<IBadDebtSegmentationValueService> _badDebtSegmentationValueService;
        private readonly Lazy<IGuarantorSegmentationDailyService> _guarantorSegmentationDailyService;
        private readonly Lazy<ISegmentationProcessService> _segmentationProcessService;
        private readonly Lazy<IMatchedGuarantorsRepository> _matchedGuarantorsRepository;
        private readonly ISession _session;
        private readonly Lazy<IScoringService> _scoringService;

        public HsSegmentationApplicationService(
            Lazy<IApplicationServiceCommonService> applicationServiceCommonService,
            Lazy<IBadDebtSegmentationValueService> badDebtSegmentationValueService,
            Lazy<IGuarantorSegmentationDailyService> guarantorSegmentationDailyService,
            Lazy<ISegmentationProcessService> segmentationProcessService,
            Lazy<IMatchedGuarantorsRepository> matchedGuarantorsRepository,
            ISessionContext<CdiEtl> session,
            Lazy<IScoringService> scoringService) : base(applicationServiceCommonService)
        {
            this._badDebtSegmentationValueService = badDebtSegmentationValueService;
            this._guarantorSegmentationDailyService = guarantorSegmentationDailyService;
            this._segmentationProcessService = segmentationProcessService;
            this._matchedGuarantorsRepository = matchedGuarantorsRepository;
            this._session = session.Session;
            this._scoringService = scoringService;
        }

        private const string ErrorSegment = "ERROR";
        private const string BadDebtScriptName = "BadDebtSegmentationVersion1.r";
        private const string AccountsReceivableScriptName = "ARSegmentationVersion1.r";
        private const string ActivePassiveScriptName = "APSegmentationVersion1.r";

        public IList<SegmentationBadDebtDto> ProcessBadDebtSegmentation(List<SegmentationBadDebtDto> segmentationBadDebtDtos)
        {
            RscriptEndpoint rscriptEndpoint = this.GetRscriptEndpoint(BadDebtScriptName);

            string rwrapperPath = this.GetRWrapper();
            return this._segmentationProcessService.Value.ExecuteBadDebtSegmentation(segmentationBadDebtDtos, new ScoringScriptStorage
            {
                SourceRScript = rscriptEndpoint.SourceRScript,
                RdataUrls = rscriptEndpoint.DataUrls,
                RwrapperPath = rwrapperPath
            });
        }

        public IList<SegmentationAccountsReceivableDto> ProcessAccountsReceivableSegmentation(List<SegmentationAccountsReceivableDto> accountReceivableSegmentationDtos)
        {
            RscriptEndpoint rscriptEndpoint = this.GetRscriptEndpoint(AccountsReceivableScriptName);

            string rwrapperPath = this.GetRWrapper();
            return this._segmentationProcessService.Value.ExecuteAccountsReceivableSegmentation(accountReceivableSegmentationDtos, new ScoringScriptStorage
            {
                SourceRScript = rscriptEndpoint.SourceRScript,
                RdataUrls = rscriptEndpoint.DataUrls,
                RwrapperPath = rwrapperPath
            });
        }

        public IList<SegmentationActivePassiveDto> ProcessActivePassiveSegmentation(List<SegmentationActivePassiveDto> activePassiveSegmentationDtos)
        {
            RscriptEndpoint rscriptEndpoint = this.GetRscriptEndpoint(ActivePassiveScriptName);

            string rwrapperPath = this.GetRWrapper();
            return this._segmentationProcessService.Value.ExecuteActivePassiveSegmentation(activePassiveSegmentationDtos, new ScoringScriptStorage
            {
                SourceRScript = rscriptEndpoint.SourceRScript,
                RdataUrls = rscriptEndpoint.DataUrls,
                RwrapperPath = rwrapperPath
            });
        }

        public void SaveAccountsReceivableSegmentationResult(IList<SegmentationAccountsReceivableDto> segmentationAccountsReceivableVisits)
        {
            using (UnitOfWork unitOfWork = new UnitOfWork(this._session))
            {
                List<GuarantorSegmentationDaily> guarantorDailySegmentations = new List<GuarantorSegmentationDaily>();
                foreach (SegmentationAccountsReceivableDto arDto in segmentationAccountsReceivableVisits)
                {
                    GuarantorSegmentationDaily segment = Mapper.Map<GuarantorSegmentationDaily>(arDto);
                    segment.SegmentationTypeEnum = SegmentationTypeEnum.AccountsReceivable;
                    guarantorDailySegmentations.Add(segment);
                }
                this._guarantorSegmentationDailyService.Value.SaveBulkGuarantorSegmentationDaily(guarantorDailySegmentations);
                unitOfWork.Commit();
            }
        }

        public void UpdateAccountsReceivableGuarantorSegmentationHistory(int segmentationBatchId)
        {
            this._guarantorSegmentationDailyService.Value.UpdateAccountsReceivableGuarantorSegmentationHistory(segmentationBatchId);
        }

        public void SaveBadDebtSegmentationResult(IList<SegmentationBadDebtDto> segmentationBadDebtVisits)
        {
            Stopwatch sw = this._scoringService.Value.LogInfo(() => $"HsSegmentationApplicationService::SaveBadDebtSegmentationResult Start", true);
            using (UnitOfWork unitOfWork = new UnitOfWork(this._session))
            {
                IList<BadDebtSegmentationValue> segmentationValues = this._badDebtSegmentationValueService.Value.GetAll();
                List<GuarantorSegmentationDaily> guarantorDailySegmentations = new List<GuarantorSegmentationDaily>();
                foreach (SegmentationBadDebtDto bdDto in segmentationBadDebtVisits)
                {
                    GuarantorSegmentationDaily segment = Mapper.Map<GuarantorSegmentationDaily>(bdDto);
                    BadDebtSegmentationValue segmentationValue = this.GetBadDebtSegmentationValue(segmentationValues, bdDto.RawSegmentationScore);

                    if (segmentationValue == null)
                    {
                        this.LoggingService.Value.Fatal(() => $"HsSegmentationApplicationService::SaveBadDebtSegmentationResult Bad Debt Raw Segmentation Score out of bounds: GuarantorId: {bdDto.HsGuarantorId}, SegmentationBatchId: {bdDto.SegmentationBatchId}, PtpScore: {bdDto.PtpScore}, SegmentationValue: {bdDto.RawSegmentationScore}");
                    }

                    segment.SegmentationValue = segmentationValue == null ? ErrorSegment : segmentationValue.SegmentationValue;
                    segment.SegmentationTypeEnum = SegmentationTypeEnum.BadDebt;
                    guarantorDailySegmentations.Add(segment);
                }
                Stopwatch stopwatch = this._scoringService.Value.LogInfo(() => $"HsSegmentationApplicationService::SaveBulkGuarantorSegmentationDaily Start", true);
                this._guarantorSegmentationDailyService.Value.SaveBulkGuarantorSegmentationDaily(guarantorDailySegmentations);
                this._scoringService.Value.LogInfoEndTimer(() => "HsSegmentationApplicationService::SaveBulkGuarantorSegmentationDaily End", stopwatch);
                unitOfWork.Commit();
            }
            this._scoringService.Value.LogInfoEndTimer(() => "HsSegmentationApplicationService::SaveBadDebtSegmentationResult End", sw);
        }

        public void UpdateBadDebtGuarantorSegmentationHistory(int segmentationBatchId)
        {
            this._guarantorSegmentationDailyService.Value.UpdateBadDebtGuarantorSegmentationHistory(segmentationBatchId);
        }

        private BadDebtSegmentationValue GetBadDebtSegmentationValue(IList<BadDebtSegmentationValue> segmentationValues, string rawScore)
        {
            bool isNumeric = int.TryParse(rawScore, out int rawScoreInt);
            if (isNumeric)
            {
                return segmentationValues.FirstOrDefault(x => rawScoreInt >= x.MinValue && rawScoreInt <= x.MaxValue);
            }
            else
            {
                return segmentationValues.FirstOrDefault(x => x.CharValue == rawScore);
            }
        }

        public void SaveActivePassiveSegmentation(IList<SegmentationActivePassiveDto> segmentationActivePassiveVisits)
        {
            using (UnitOfWork unitOfWork = new UnitOfWork(this._session))
            {
                List<GuarantorSegmentationDaily> guarantorDailySegmentations = new List<GuarantorSegmentationDaily>();
                foreach (SegmentationActivePassiveDto apDto in segmentationActivePassiveVisits)
                {
                    GuarantorSegmentationDaily segment = Mapper.Map<GuarantorSegmentationDaily>(apDto);
                    segment.SegmentationTypeEnum = SegmentationTypeEnum.ActivePassive;
                    guarantorDailySegmentations.Add(segment);
                }
                this._guarantorSegmentationDailyService.Value.SaveBulkGuarantorSegmentationDaily(guarantorDailySegmentations);
                unitOfWork.Commit();
            }
        }

        public void UpdateActivePassiveGuarantorSegmentationHistory(int segmentationBatchId)
        {
            this._guarantorSegmentationDailyService.Value.UpdateActivePassiveGuarantorSegmentationHistory(segmentationBatchId);
        }

        private RscriptEndpoint GetRscriptEndpoint(string scriptName)
        {
            return new RscriptEndpoint
            {
                SourceRScript = string.Format(this.ApplicationSettingsService.Value.ScoringRscriptEndPointUrl.Value, scriptName)
            };
        }

        private string GetRWrapper()
        {
            return Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), "rscripts\\rwrapper.r");
        }

    }

    class SegmentationMessageSettings
    {
        public MessagingEndpoint MessagingEndpoint { get; set; }
        public string SegmentationFunctionUrl { get; set; }
        public string AuthorizationCode { get; set; }
        public string AppId { get; set; }
        public string AppKey { get; set; }
        public string AppName { get; set; }
        public bool UseRemoteFunction { get; set; }
        public string FunctionName { get; set; }
    }

}
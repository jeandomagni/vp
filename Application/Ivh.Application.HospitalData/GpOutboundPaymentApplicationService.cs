﻿namespace Ivh.Application.HospitalData
{
    using System;
    using Base.Common.Interfaces;
    using Base.Services;
    using Common.Interfaces;
    using Domain.HospitalData.Visit.Interfaces;
    using Ivh.Common.ServiceBus.Attributes;
    using Ivh.Common.ServiceBus.Enums;
    using Ivh.Common.VisitPay.Messages.HospitalData;
    using MassTransit;

    public class GpOutboundPaymentApplicationService : ApplicationService, IGpOutboundPaymentApplicationService
    {
        private readonly Lazy<IOutboundGuestPayPaymentService> _outboundGuestPayPaymentService;

        public GpOutboundPaymentApplicationService(
            Lazy<IApplicationServiceCommonService> applicationServiceCommonService,
            Lazy<IOutboundGuestPayPaymentService> outboundGuestPayPaymentService) : base(applicationServiceCommonService)
        {
            this._outboundGuestPayPaymentService = outboundGuestPayPaymentService;
        }

        public void CreateOutboundGuestPayPayment(GuestPayPaymentMessage guestPayPaymentMessage)
        {
            this._outboundGuestPayPaymentService.Value.CreateOutboundGuestPayPayment(guestPayPaymentMessage);
        }

        public void VoidOutboundGuestPayPayment(GuestPayPaymentVoidMessage guestPayPaymentVoidMessage)
        {
            this._outboundGuestPayPaymentService.Value.VoidOutboundGuestPayPayment(guestPayPaymentVoidMessage);
        }

        #region Message Consumers

        [UniversalConsumer(UniversalQueuesEnum.HsDataProcessor, UniversalPriorityEnum.Priority30)]
        public void ConsumeMessage(GuestPayPaymentMessage message, ConsumeContext<GuestPayPaymentMessage> consumeContext)
        {
            this.CreateOutboundGuestPayPayment(message);
        }

        public bool IsMessageValid(GuestPayPaymentMessage message, ConsumeContext<GuestPayPaymentMessage> consumeContext)
        {
            bool isValid = true;
            return isValid;
        }

        [UniversalConsumer(UniversalQueuesEnum.HsDataProcessor, UniversalPriorityEnum.Priority30)]
        public void ConsumeMessage(GuestPayPaymentVoidMessage message, ConsumeContext<GuestPayPaymentVoidMessage> consumeContext)
        {
            this.VoidOutboundGuestPayPayment(message);
        }

        public bool IsMessageValid(GuestPayPaymentVoidMessage message, ConsumeContext<GuestPayPaymentVoidMessage> consumeContext)
        {
            bool isValid = true;
            return isValid;
        }

        #endregion
    }
}
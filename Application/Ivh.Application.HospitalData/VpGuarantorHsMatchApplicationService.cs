﻿namespace Ivh.Application.HospitalData
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using AutoMapper;
    using Base.Common.Interfaces;
    using Base.Services;
    using Common.Interfaces;
    using Domain.HospitalData.HsGuarantor.Entities;
    using Domain.HospitalData.HsGuarantor.Interfaces;
    using Domain.HospitalData.VpGuarantor.Entities;
    using Ivh.Common.Data;
    using Ivh.Common.Data.Connection.Connections;
    using Ivh.Common.Data.Interfaces;
    using Ivh.Common.ServiceBus.Attributes;
    using Ivh.Common.ServiceBus.Enums;
    using Ivh.Common.VisitPay.Enums;
    using Ivh.Common.VisitPay.Messages.HospitalData;
    using Ivh.Common.VisitPay.Messages.HospitalData.Dtos;
    using MassTransit;
    using NHibernate;

    public class VpGuarantorHsMatchApplicationService : ApplicationService, IVpGuarantorHsMatchApplicationService
    {
        private readonly Lazy<IVpGuarantorHsMatchService> _vpGuarantorHsMatchService;
        private readonly Lazy<IVpOutboundHsGuarantorService> _vpOutboundHsGuarantorService;
        private readonly ISession _session;

        public VpGuarantorHsMatchApplicationService(
            Lazy<IApplicationServiceCommonService> applicationServiceCommonService,
            Lazy<IVpGuarantorHsMatchService> vpGuarantorHsMatchService,
            Lazy<IVpOutboundHsGuarantorService> vpOutboundHsGuarantorService,
            ISessionContext<CdiEtl> session) : base(applicationServiceCommonService)
        {
            this._vpGuarantorHsMatchService = vpGuarantorHsMatchService;
            this._vpOutboundHsGuarantorService = vpOutboundHsGuarantorService;
            this._session = session.Session;
        }

        public IReadOnlyList<VpGuarantorHsMatchDto> GetVpGuarantorHsMatches(int hsGuarantorId)
        {
            IList<VpGuarantorHsMatch> retValue = this._vpGuarantorHsMatchService.Value.GetVpGuarantorHsMatches(hsGuarantorId);
            return Mapper.Map<IReadOnlyList<VpGuarantorHsMatchDto>>(retValue);
        }

        public void ProcessHsGuarantorCancelMessage(EtlHsGuarantorCancelMessage message)
        {
            this._vpOutboundHsGuarantorService.Value.Insert(new VpOutboundHsGuarantor
            {
                InsertDate = DateTime.UtcNow,
                VpGuarantorId = message.VpGuarantorId,
                VpOutboundHsGuarantorMessageType = VpOutboundHsGuarantorMessageTypeEnum.VpGuarantorCancelation
            });
        }

        public void ProcessHsGuarantorReactivateMessage(EtlHsGuarantorReactivateMessage message)
        {
            this._vpOutboundHsGuarantorService.Value.Insert(new VpOutboundHsGuarantor
            {
                InsertDate = DateTime.UtcNow,
                VpGuarantorId = message.VpGuarantorId,
                VpOutboundHsGuarantorMessageType = VpOutboundHsGuarantorMessageTypeEnum.HsGuarantorReactivation
            });
        }

        public IList<VpGuarantorHsMatchDto> GetVpGuarantorHsMatches(int matchedHsGuarantorId, int vpGuarantorId)
        {
            return Mapper.Map<IList<VpGuarantorHsMatchDto>>(this._vpGuarantorHsMatchService.Value.GetVpGuarantorHsMatches(matchedHsGuarantorId, vpGuarantorId));
        }

        private void ProcessVpccGuarantorEnrolledInVisitPayMessage(VpccGuarantorEnrolledInVisitPayMessage message)
        {
            // update cdi.vp.VpGuarantorHsMatch HsGuarantorMatchStatusId to 1
            IList<VpGuarantorHsMatch> vpGuarantorHsMatches = this._vpGuarantorHsMatchService.Value.GetHsGuarantorVpMatches(message.VpGuarantorId);
            vpGuarantorHsMatches = vpGuarantorHsMatches?
                .Where(x => x.HsGuarantorMatchStatus == HsGuarantorMatchStatusEnum.VpccPending)
                .Where(x => message.HsGuarantorIds.Contains(x.HsGuarantorId))
                .ToList();

            using (UnitOfWork unitOfWork = new UnitOfWork(this._session))
            {
                if (vpGuarantorHsMatches != null)
                {
                    foreach (VpGuarantorHsMatch vpGuarantorHsMatch in vpGuarantorHsMatches)
                    {
                        vpGuarantorHsMatch.HsGuarantorMatchStatus = HsGuarantorMatchStatusEnum.Matched;
                        vpGuarantorHsMatch.IsActive = true;

                        this._vpGuarantorHsMatchService.Value.InsertOrUpdate(vpGuarantorHsMatch);
                    }

                    //TODO: do we need to create a change event? probably not
                }

                unitOfWork.Commit();
            }
        }

        #region consumers

        [UniversalConsumer(UniversalQueuesEnum.HsDataProcessor, UniversalPriorityEnum.Priority30)]
        public void ConsumeMessage(EtlHsGuarantorCancelMessage message, ConsumeContext<EtlHsGuarantorCancelMessage> consumeContext)
        {
            this.ProcessHsGuarantorCancelMessage(message);
        }

        public bool IsMessageValid(EtlHsGuarantorCancelMessage message, ConsumeContext<EtlHsGuarantorCancelMessage> consumeContext)
        {
            bool isValid = true;
            return isValid;
        }

        [UniversalConsumer(UniversalQueuesEnum.HsDataProcessor, UniversalPriorityEnum.Priority30)]
        public void ConsumeMessage(EtlHsGuarantorReactivateMessage message, ConsumeContext<EtlHsGuarantorReactivateMessage> consumeContext)
        {
            this.ProcessHsGuarantorReactivateMessage(message);
        }

        public bool IsMessageValid(EtlHsGuarantorReactivateMessage message, ConsumeContext<EtlHsGuarantorReactivateMessage> consumeContext)
        {
            bool isValid = true;
            return isValid;
        }

        [UniversalConsumer(UniversalQueuesEnum.HsDataProcessor, UniversalPriorityEnum.Priority30)]
        public void ConsumeMessage(VpccGuarantorEnrolledInVisitPayMessage message, ConsumeContext<VpccGuarantorEnrolledInVisitPayMessage> consumeContext)
        {
            this.ProcessVpccGuarantorEnrolledInVisitPayMessage(message);
        }
        
        public bool IsMessageValid(VpccGuarantorEnrolledInVisitPayMessage message, ConsumeContext<VpccGuarantorEnrolledInVisitPayMessage> consumeContext)
        {
            bool isValid = true;
            return isValid;
        }

        #endregion

    }
}

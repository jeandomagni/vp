﻿namespace Ivh.Application.HospitalData
{
    using System;
    using System.Collections.Generic;
    using AutoMapper;
    using Base.Common.Interfaces;
    using Base.Services;
    using Common.Interfaces;
    using Domain.HospitalData.HsGuarantor.Entities;
    using Domain.HospitalData.HsGuarantor.Interfaces;
    using Ivh.Common.VisitPay.Enums;
    using Ivh.Common.VisitPay.Messages.HospitalData.Dtos;

    public class HsGuarantorApplicationService : ApplicationService, IHsGuarantorApplicationService
    {
        private readonly IHsGuarantorService _hsGuarantorService;
  
        public HsGuarantorApplicationService(
            Lazy<IApplicationServiceCommonService> applicationServiceCommonService,
            IHsGuarantorService hsGuarantorService) : base(applicationServiceCommonService)
        {
            this._hsGuarantorService = hsGuarantorService;
        }

        public HsGuarantorDto GetHsGuarantor(int hsGuarantorId)
        {
            return Mapper.Map<HsGuarantorDto>(this._hsGuarantorService.GetHsGuarantor(hsGuarantorId));
        }

        public IReadOnlyList<HsGuarantorSearchResultDto> GetHsGuarantors(HsGuarantorFilterDto filterDto)
        {
            try
            {
                HsGuarantorFilter filter = Mapper.Map<HsGuarantorFilter>(filterDto);
                IReadOnlyList<HsGuarantorSearchResult> guarantors = this._hsGuarantorService.GetHsGuarantors(filter);
                return Mapper.Map<IReadOnlyList<HsGuarantorSearchResultDto>>(guarantors);
            }
            catch (Exception)
            {
                { }
                throw;
            }
        }

        public HsGuarantorDto AddHsGuarantor(HsGuarantorDto hsGuarantorDto)
        {
            HsGuarantor hsGuarantor = this._hsGuarantorService.AddHsGuarantor(Mapper.Map<HsGuarantor>(hsGuarantorDto));
            return Mapper.Map<HsGuarantorDto>(hsGuarantor);
        }
    }
}

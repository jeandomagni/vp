﻿namespace Ivh.Application.HospitalData
{
    using System;
    using System.Collections.Generic;
    using AutoMapper;
    using Base.Common.Interfaces;
    using Base.Services;
    using Common.Interfaces;
    using Domain.Guarantor.Entities;
    using Domain.Guarantor.Interfaces;
    using Domain.Visit.Interfaces;
    using Domain.HospitalData.HsGuarantor.Entities;
    using Domain.HospitalData.HsGuarantor.Interfaces;
    using Ivh.Common.ServiceBus.Attributes;
    using Ivh.Common.ServiceBus.Enums;
    using Ivh.Common.VisitPay.Messages.HospitalData;
    using Ivh.Common.VisitPay.Messages.HospitalData.Dtos;
    using MassTransit;

    public class VpOutboundHsGuarantorApplicationService : ApplicationService, IVpOutboundHsGuarantorApplicationService
    {
        private readonly Lazy<IGuarantorService> _guarantorService;
        private readonly Lazy<IVpOutboundHsGuarantorService> _outboundEventService;
        private readonly Lazy<IVisitService> _visitService;

        public VpOutboundHsGuarantorApplicationService(
            Lazy<IApplicationServiceCommonService> applicationServiceCommonService,
            Lazy<IVpOutboundHsGuarantorService> outboundEventService,
            Lazy<IGuarantorService> guarantorService,
            Lazy<IVisitService> visitService) : base(applicationServiceCommonService)
        {
            this._outboundEventService = outboundEventService;
            this._guarantorService = guarantorService;
            this._visitService = visitService;
        }

        public void OnHsGuarantorAcknowledgementMessage(AppHsGuarantorAcknowledgementMessage message)
        {
            HsGuarantorAcknowledgementDto acknowledgement = message.HsGuarantorAcknowledgement;
            Guarantor guarantor = this._guarantorService.Value.GetGuarantor(acknowledgement.VpGuarantorId);

            if (guarantor == null)
            {
                throw new Exception($"Could not find a guarantor record with id {acknowledgement.VpGuarantorId}.");
            }

            this._guarantorService.Value.SetToActiveVisitsLoaded(guarantor);
            this._guarantorService.Value.InsertMatchingHsGuarantor(acknowledgement, guarantor);

            if (this._visitService.Value.GetVisits(guarantor.VpGuarantorId).Count > 0)
            {
                this._guarantorService.Value.SetToActiveVisitsLoaded(guarantor);
            }
        }

        public IReadOnlyList<VpOutboundHsGuarantorDto> GetOutboundEvents(int hsGuarantorId)
        {
            IReadOnlyList<VpOutboundHsGuarantor> guarantorEvents = this._outboundEventService.Value.GetOutboundEvents(hsGuarantorId);
            return Mapper.Map<IReadOnlyList<VpOutboundHsGuarantorDto>>(guarantorEvents);
        }

        public void AddOutboundEvent(VpOutboundHsGuarantorDto hsGuarantorEvent)
        {
            VpOutboundHsGuarantor guarantorEvent = Mapper.Map<VpOutboundHsGuarantor>(hsGuarantorEvent);

            this._outboundEventService.Value.Insert(guarantorEvent);
        }

        public void CreateOutboundPayment(PaymentMessage message)
        {
            this._outboundEventService.Value.CreateOutboundPayment(message);
        }

        #region Message Consumers

        [UniversalConsumer(UniversalQueuesEnum.HsDataProcessor, UniversalPriorityEnum.Priority20)]
        public void ConsumeMessage(PaymentMessage message, ConsumeContext<PaymentMessage> consumeContext)
        {
            this.CreateOutboundPayment(message);
        }

        public bool IsMessageValid(PaymentMessage message, ConsumeContext<PaymentMessage> consumeContext)
        {
            bool isValid = true;
            return isValid;
        }

        #endregion
    }
}
﻿namespace Ivh.Application.HospitalData
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.IO;
    using System.Linq;
    using System.Reflection;
    using AutoMapper;
    using Base.Common.Interfaces;
    using Base.Services;
    using Common.Interfaces;
    using Domain.HospitalData.Change.Entities;
    using Domain.HospitalData.Change.Interfaces;
    using Domain.HospitalData.Visit.Interfaces;
    using Domain.HospitalData.Visit.Entities;
    using Domain.HospitalData.Scoring.Entities;
    using Domain.HospitalData.Scoring.Enums;
    using Domain.HospitalData.Scoring.Interfaces;
    using Domain.Logging.Interfaces;
    using Ivh.Common.Data;
    using Ivh.Common.Data.Connection.Connections;
    using Ivh.Common.Data.Interfaces;
    using Ivh.Common.VisitPay.Constants;
    using Ivh.Common.VisitPay.Enums;
    using Ivh.Common.VisitPay.Messages.HospitalData.Dtos;
    using Ivh.Common.VisitPay.Messages.Scoring;
    using Newtonsoft.Json;
    using NHibernate;

    public class HsGuarantorApplicationScoringService : ApplicationService, IHsGuarantorScoringApplicationService
    {
        private readonly Lazy<IChangeEventRepository> _changeEventRepository;
        private readonly Lazy<IMetricsProvider> _metricsProvider;
        private readonly Lazy<IMatchedGuarantorsVisitBatchRepository> _matchedGuarantorsVisitBatchRepository;
        private readonly Lazy<IRetroScoreService> _retroScoreService;
        private readonly Lazy<IScoringGuarantorMatchingService> _scoringGuarantorMatchingService;
        private readonly Lazy<IScoreRepository> _scoreRepository;
        private readonly Lazy<IScoringService> _scoringService;
        private readonly ISession _session;
        private readonly Lazy<IThirdPartyDataService> _thirdPartyDataService;
        private readonly Lazy<IVisitBatchRepository> _visitBatchRepository;
        private readonly Lazy<IVisitTransactionRepository> _visitTransactionRepository;
        private readonly Lazy<IGuarantorBatchRepository> _guarantorBatchRepository;


        public HsGuarantorApplicationScoringService(
            Lazy<IApplicationServiceCommonService> applicationServiceCommonService,
            Lazy<IChangeEventRepository> changeEventRepository,
            Lazy<IMetricsProvider> metricsProvider,
            Lazy<IMatchedGuarantorsVisitBatchRepository> matchedGuarantorsVisitBatchRepository,
            Lazy<IRetroScoreService> retroScoreService,
            Lazy<IScoringGuarantorMatchingService> scoringGuarantorMatchingService,
            Lazy<IScoreRepository> scoreRepository,
            Lazy<IScoringService> scoringService,
            ISessionContext<CdiEtl> session,
            Lazy<IThirdPartyDataService> thirdPartyDataService,
            Lazy<IVisitBatchRepository> visitBatchRepository,
            Lazy<IGuarantorBatchRepository> guarantorBatchRepository,
            Lazy<IVisitTransactionRepository> visitTransactionRepository)
            : base(applicationServiceCommonService)
        {
            this._changeEventRepository = changeEventRepository;
            this._metricsProvider = metricsProvider;
            this._matchedGuarantorsVisitBatchRepository = matchedGuarantorsVisitBatchRepository;
            this._retroScoreService = retroScoreService;
            this._scoringGuarantorMatchingService = scoringGuarantorMatchingService;
            this._scoreRepository = scoreRepository;
            this._scoringService = scoringService;
            this._session = session.Session;
            this._thirdPartyDataService = thirdPartyDataService;
            this._visitBatchRepository = visitBatchRepository;
            this._visitTransactionRepository = visitTransactionRepository;
            this._guarantorBatchRepository = guarantorBatchRepository;
        }

        public ScoringResult ProcessGuarantorProtoScoreResult(ScoringResult scoringResult, Guid processId)
        {
            IList<ScoringCalculatedInput> scoringCalculatedInputs = new List<ScoringCalculatedInput>();
            foreach (ScoringCalculatedInput calculatedInput in scoringResult.CalculatedInputs)
            {
                Score score = Mapper.Map<Score>(calculatedInput);
                this.SetupScore(score, scoringResult, processId);
                ScoringCalculatedInput updatedCalculatedInput = Mapper.Map<ScoringCalculatedInput>(score);
                updatedCalculatedInput.BillingSystemId = calculatedInput.BillingSystemId;
                updatedCalculatedInput.BillingSystemName = calculatedInput.BillingSystemName;
                updatedCalculatedInput.FacilityDescription = calculatedInput.FacilityDescription;
                updatedCalculatedInput.FacilityId = calculatedInput.FacilityId;
                updatedCalculatedInput.RegionId = calculatedInput.RegionId;
                updatedCalculatedInput.RegionScope = calculatedInput.RegionScope;
                updatedCalculatedInput.StateCode = calculatedInput.StateCode;
                updatedCalculatedInput.SourceSystemKey = calculatedInput.SourceSystemKey;
                scoringCalculatedInputs.Add(updatedCalculatedInput);
            }
            scoringResult.CalculatedInputs = scoringCalculatedInputs;
            return scoringResult;
        }

        private ScoringResult ProcessGuarantorPtpScoreRequest(ScoringResult scoringResult)
        {
            using (UnitOfWork unitOfWork = new UnitOfWork(this._session))
            {
                IList<Score> scores = Mapper.Map<IList<Score>>(scoringResult.CalculatedInputs);
                ScoringPtpScoreInput ptpScoringInput = new ScoringPtpScoreInput();
                int scoringBatchId = 0;
                Stopwatch sw = this._scoringService.Value.LogInfo(() => $"HsGuarantorApplicationScoringService::ProcessGuarantorProtoScoreRequest: Start Get ThirdParty", true);
                foreach (ScoringCalculatedInput scoringCalculatedInput in scoringResult.CalculatedInputs)
                {
                    ScoringGuarantorScore scoreDto = scoringResult.GuarantorScores.FirstOrDefault(x => x.HsGuarantorId == scoringCalculatedInput.HsGuarantorId);
                    bool requireThirdParty = false;
                    if (scoreDto != null)
                    {
                        requireThirdParty = scoreDto.Am1Flag && this.Client.Value.ScoringUseThirdPartyApi;
                    }

                    if (requireThirdParty)
                    {
                        this._metricsProvider.Value.Increment(Metrics.Increment.Scoring.ThirdPartyDataExpected);
                    }

                    Score score = scores.FirstOrDefault(x => x.ParentHsGuarantorId == scoringCalculatedInput.HsGuarantorId);
                    if (score != null)
                    {
                        GuarantorBatch guarantorBatch = this._guarantorBatchRepository.Value.GetByGuarantorIdScoringBatchId(scoringCalculatedInput.HsGuarantorId, score.ScoringBatchId);
                        ScoringThirdPartyDto thirdPartyDto = this._thirdPartyDataService.Value.ExtractThirdParty(guarantorBatch, requireThirdParty, (ScoringThirdPartyDatasetEnum)this.Client.Value.PtpScoreThirdPartyDataSet, ScoringThirdPartyDataSetTypeEnum.Scoring, true);
                        ScoringGuarantorScore guarantorScore = scoringResult.GuarantorScores.FirstOrDefault(x => x.HsGuarantorId == score.ParentHsGuarantorId);
                        score.VisitTransactionJson = null;
                        scoringBatchId = score.ScoringBatchId;
                        ptpScoringInput.GuarantorScores.Add(guarantorScore);
                        scoringCalculatedInput.ScoreId = score.ScoreId;
                        scoringCalculatedInput.Client = this.ApplicationSettingsService.Value.Client.Value;
                        ptpScoringInput.CalculatedInputs.Add(scoringCalculatedInput);
                        if (this.Client.Value.ScoringFeatureIncludeVisitTransactionsInRModel)
                        {
                            ptpScoringInput.ScoringServiceInputs = scoringResult.SourceInputs;
                        }
                        if (thirdPartyDto != null)
                        {
                            ptpScoringInput.ThirdPartyData.Add(thirdPartyDto);
                        }
                    }
                }
                this._scoringService.Value.LogInfoEndTimer(() => "HsGuarantorApplicationScoringService::ProcessGuarantorProtoScoreRequest: End Get ThirdParty", sw);

                ScoringPtpScoreRequest ptpScoreRequest = new ScoringPtpScoreRequest
                {
                    PtpScoreInput = ptpScoringInput
                };
                sw = this._scoringService.Value.LogInfo(() => $"HsGuarantorApplicationScoringService::ProcessGuarantorProtoScoreRequest: Start InvokePtpScoringApiService", true);
                ScoringResult ptpScoreResult = this.InvokePtpScoringApiService(ptpScoreRequest);
                ptpScoreResult.SourceInputs = scoringResult.SourceInputs;
                this._scoringService.Value.LogInfoEndTimer(() => "HsGuarantorApplicationScoringService::ProcessGuarantorProtoScoreRequest: End InvokePtpScoringApiService", sw);

                unitOfWork.Commit();
                return ptpScoreResult;
            }

        }

        private void ProcessGuarantorPtpScoreResult(ScoringResult scoringResult)
        {
            using (UnitOfWork unitOfWork = new UnitOfWork(this._session))
            {
                IList<Score> scores = Mapper.Map<IList<Score>>(scoringResult.CalculatedInputs);
                foreach (Score score in scores)
                {
                    this.SetupPtpScore(score, scoringResult);
                }
                unitOfWork.Commit();
            }
        }

        private void SetupPtpScore(Score score, ScoringResult scoringResult)
        {
            Score finalScore = this._scoreRepository.Value.GetById(score.ScoreId) ?? score;
            ScoringGuarantorScore guarantor = scoringResult.GuarantorScores.FirstOrDefault(g => g.HsGuarantorId == score.ParentHsGuarantorId);
            if (guarantor != null)
            {
                finalScore.PtpScore = guarantor.PtpScore;
                finalScore.Phase2ScriptVersion = guarantor.S2Version;
                finalScore.PtpScore = guarantor.PtpScore;
                ScoringThirdPartyDto thirdPartyData = scoringResult.ThirdPartyDtos.FirstOrDefault(x => x.HsGuarantorId == guarantor.HsGuarantorId);
                finalScore.ThirdPartyDataId = thirdPartyData?.ThirdPartyDataId;
                finalScore.ScoreTypeId = finalScore.ScoreTypeId ?? (int)ScoreTypeEnum.PtpOriginal;
            }
            this._scoreRepository.Value.InsertOrUpdate(finalScore);
            this._metricsProvider.Value.Increment(Metrics.Increment.Scoring.PtpScores);
            IEnumerable<ScoringServiceInput> inputs = scoringResult.SourceInputs.Where(x => x.HsGuarantorId == finalScore.ParentHsGuarantorId);
            foreach (ScoringServiceInput input in inputs)
            {
                this.UpdateVisitBatchStatus(input.GuarantorAccountVisits.Where(x => x.TriggeredScoring && x.HsGuarantorId == finalScore.ParentHsGuarantorId).ToList(), finalScore.ScoreId);
            }
        }

        private void UpdateVisitBatchStatus(IList<GuarantorAccountVisit> guarantorAccountVisits, int scoreId)
        {
            List<GuarantorAccountVisit> list = guarantorAccountVisits.Where(x => x.TriggeredScoring).ToList();
            foreach (GuarantorAccountVisit v in list)
            {
                VisitBatch vb = this._visitBatchRepository.Value.GetById(v.VisitBatchId);
                if (vb == null)
                {
                    continue;
                }
                vb.ScoreId = scoreId;
                this._visitBatchRepository.Value.Update(vb);
                ChangeEvent ce = this._changeEventRepository.Value.GetById(vb.ChangeEventId);
                if (ce != null && ce.ChangeEventStatus != ChangeEventStatusEnum.Processed)
                {
                    ce.ChangeEventStatus = ChangeEventStatusEnum.Processed;
                    this._changeEventRepository.Value.Update(ce);
                    this._metricsProvider.Value.Increment(Metrics.Increment.Scoring.ChangeEventActual);
                }
            }
        }

        public void ProcessScoreRequest(ScoringScoreRequestMessage message, int scoringBatchId)
        {
            ScoringResult protoScoreResult = this.GenerateProtoScore(message);
            if (protoScoreResult == null)
            {
                return;
            }

            ScoringResult processedProtoScoreResult = this.ProcessProtoScoreResult(protoScoreResult, message.ProcessId, scoringBatchId);
            if (processedProtoScoreResult == null)
            {
                return;
            }

            this.GeneratePtpScore(processedProtoScoreResult, scoringBatchId);
        }

        private ScoringResult GenerateProtoScore(ScoringScoreRequestMessage message)
        {
            ScoringResult scoringResult = null;
            using (UnitOfWork unitOfWork = new UnitOfWork(this._session))
            {
                Stopwatch sw = this._scoringService.Value.LogInfo(() => $"HsGuarantorApplicationScoringService::GenerateProtoScore: Start ExtractRawScoringData", true);
                IList<ScoringServiceInput> scoringInput = this.ExtractRawScoringData(message.ScoringGuarantorDtos, null, DateTime.UtcNow.AddYears(-1));
                this._scoringService.Value.LogInfoEndTimer(() => "HsGuarantorApplicationScoringService::GenerateProtoScore: End ExtractRawScoringData", sw);
                RscriptEndpoint rscriptEndpoint = this.GetProtoScoreRscriptEndpoint();
                string rwrapperPath = this.GetRWrapper();
                sw = this._scoringService.Value.LogInfo(() => $"HsGuarantorApplicationScoringService::GenerateProtoScore: Start GenerateGuarantorProtoScores", true);
                scoringResult = this._scoringService.Value.GenerateGuarantorProtoScores(scoringInput, new ScoringScriptStorage
                {
                    SourceRScript = rscriptEndpoint.SourceRScript,
                    RdataUrls = rscriptEndpoint.DataUrls,
                    RwrapperPath = rwrapperPath
                });
                this._scoringService.Value.LogInfoEndTimer(() => "HsGuarantorApplicationScoringService::GenerateProtoScore: End GenerateGuarantorProtoScores", sw);
                unitOfWork.Commit();
            }

            return scoringResult;
        }

        private ScoringResult ProcessProtoScoreResult(ScoringResult protoScoreResult, Guid processId, int scoringBatchId)
        {
            ScoringResult processedProtoScoreResult = null;
            using (UnitOfWork unitOfWork = new UnitOfWork(this._session))
            {
                Stopwatch sw = this._scoringService.Value.LogInfo(() => $"HsGuarantorApplicationScoringService::ProcessProtoScoreResult: Start ProcessGuarantorProtoScoreResult", true);
                processedProtoScoreResult = this.ProcessGuarantorProtoScoreResult(protoScoreResult, processId);
                this._matchedGuarantorsVisitBatchRepository.Value.UpdateMatchedGuarantorsVisitBatchStatus(scoringBatchId, ScoringStatusEnum.AwaitingThirdPartyData, protoScoreResult.CalculatedInputs.Select(x => x.HsGuarantorId));
                this._scoringService.Value.LogInfoEndTimer(() => "HsGuarantorApplicationScoringService::ProcessProtoScoreResult: End ProcessGuarantorProtoScoreResult", sw);
                unitOfWork.Commit();
            }

            return processedProtoScoreResult;
        }

        private void GeneratePtpScore(ScoringResult processedProtoScoreResult, int scoringBatchId)
        {
            using (UnitOfWork unitOfWork = new UnitOfWork(this._session))
            {
                Stopwatch sw = this._scoringService.Value.LogInfo(() => $"HsGuarantorApplicationScoringService::GeneratePtpScore: Start ProcessGuarantorPtpScoreRequest", true);
                ScoringResult ptpScoreResult = this.ProcessGuarantorPtpScoreRequest(processedProtoScoreResult);
                this._matchedGuarantorsVisitBatchRepository.Value.UpdateMatchedGuarantorsVisitBatchStatus(scoringBatchId, ScoringStatusEnum.Scored, processedProtoScoreResult.CalculatedInputs.Select(x => x.HsGuarantorId));
                this._scoringService.Value.LogInfoEndTimer(() => "HsGuarantorApplicationScoringService::GeneratePtpScore: End ProcessGuarantorPtpScoreRequest", sw);
                if (ptpScoreResult != null)
                {
                    sw = this._scoringService.Value.LogInfo(() => $"HsGuarantorApplicationScoringService::GeneratePtpScore: Start ProcessGuarantorPtpScoreResult", true);
                    this.ProcessGuarantorPtpScoreResult(ptpScoreResult);
                    this._scoringService.Value.LogInfoEndTimer(() => "HsGuarantorApplicationScoringService::GeneratePtpScore: End ProcessGuarantorPtpScoreResult", sw);
                }

                unitOfWork.Commit();
            }
        }

        private IList<ScoringServiceInput> ExtractRawScoringData(IEnumerable<ScoringGuarantorDto> guarantors, DateTime? retroScoreDate = null, DateTime? transactionDateBegin = null)
        {
            IList<ScoringServiceInput> inputs = new List<ScoringServiceInput>();
            foreach (ScoringGuarantorDto guarantor in guarantors)
            {
                ScoringServiceInput input = new ScoringServiceInput
                {
                    HsGuarantorId = guarantor.HsGuarantorId,
                    GuarantorAccountVisits = new List<GuarantorAccountVisit>()
                };
                foreach (ScoringGuarantorVisitBatchDto v in guarantor.Visits)
                {
                    GuarantorAccountVisit visit = Mapper.Map<GuarantorAccountVisit>(v);
                    visit.ScoringDate = retroScoreDate ?? DateTime.UtcNow;
                    IList<VisitTransaction> vtransactions = this._visitTransactionRepository.Value.GetGuarantorAccountTransactions(v.VisitId, transactionDateBegin);

                    if (retroScoreDate.HasValue)
                    {
                        vtransactions = vtransactions.Where(x => x.PostDate <= retroScoreDate.Value).ToList();
                        visit.HsCurrentBalance = vtransactions.Sum(x => x.TransactionAmount);
                        visit.SelfPayBalance = visit.HsCurrentBalance;
                    }

                    visit.GuarantorAccountTransactions = Mapper.Map<IList<GuarantorAccountTransaction>>(vtransactions).ToList();
                    input.GuarantorAccountVisits.Add(visit);
                }
                inputs.Add(input);
            }

            return inputs;
        }

        private RscriptEndpoint GetProtoScoreRscriptEndpoint()
        {
            return new RscriptEndpoint
            {
                SourceRScript = string.Format(this.ApplicationSettingsService.Value.ScoringRscriptEndPointUrl.Value, this.Client.Value.Phase1ProtoScoreRScript),
                DataUrls = string.Format(this.ApplicationSettingsService.Value.ScoringRscriptEndPointUrl.Value, this.Client.Value.Phase1ProtoScoreRData)
            };
        }

        private void SetupScore(Score score, ScoringResult scoringResult, Guid processId)
        {
            ScoringGuarantorScore guarantor = scoringResult.GuarantorScores.FirstOrDefault(g => g.HsGuarantorId == score.ParentHsGuarantorId);
            if (guarantor != null)
            {
                score.PtpScore = guarantor.PtpScore;
                score.ProtoScore = Mapper.Map<ProtoScore>(guarantor.ProtoScore);
                score.Phase1ScriptVersion = guarantor.S1Version;
                score.ProcessId = processId;
                ScoringServiceInput guarantorVisitAccount = scoringResult.SourceInputs.FirstOrDefault(s => s.HsGuarantorId == guarantor.HsGuarantorId);
                score.ScoreTypeId = score.ScoreTypeId ?? (int)ScoreTypeEnum.PtpOriginal;
                if (guarantorVisitAccount != null)
                {
                    score.VisitTransactionJson = JsonConvert.SerializeObject(guarantorVisitAccount);
                }
                this._scoreRepository.Value.InsertOrUpdate(score);
                this._metricsProvider.Value.Increment(Metrics.Increment.Scoring.ProtoScores);
            }
        }

        private RscriptEndpoint GetPtpRscriptEndpoint()
        {
            string client = this.ApplicationSettingsService.Value.Client.Value.ToString();
            string clientPtpScoreScript = string.Format(this.Client.Value.Phase2ClientPtpScoreRScript, client);
            string ptpScoreScript = string.Format(this.ApplicationSettingsService.Value.ScoringRscriptEndPointUrl.Value, clientPtpScoreScript);

            string clientPtpScoreRData = string.Format(this.Client.Value.Phase2ClientPtpScoreRData, client);
            string ptpScoreRData = string.Format(this.ApplicationSettingsService.Value.ScoringRscriptEndPointUrl.Value, clientPtpScoreRData);

            return new RscriptEndpoint
            {
                SourceRScript = ptpScoreScript,
                DataUrls = ptpScoreRData
            };
        }

        private ScoringResult InvokePtpScoringApiService(ScoringPtpScoreRequest scoringPtpScoreRequest)
        {
            RscriptEndpoint rscriptEndpoint = this.GetPtpRscriptEndpoint();
            string rwrapperPath = this.GetRWrapper();
            return this._scoringService.Value.GenerateGuarantorPtpScore(scoringPtpScoreRequest, new ScoringScriptStorage
            {
                SourceRScript = rscriptEndpoint.SourceRScript,
                RdataUrls = rscriptEndpoint.DataUrls,
                RwrapperPath = rwrapperPath
            });
        }

        private string GetRWrapper()
        {
            return Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), "rscripts\\rwrapper.r");
        }

        public void CalculateInputVariablesForRetroScoring(DateTime startDate, DateTime endDate)
        {
            this.LogMessage($"From {startDate} to {endDate} started.");

            DateTime scoringDate = startDate;
            while (scoringDate <= endDate)
            {
                this.LogMessage($"{scoringDate} started.");
                List<List<ScoringGuarantorDto>> scoringGuarantorDtos = this._scoringGuarantorMatchingService.Value.ExecuteRetroScoring(scoringDate);

                foreach (List<ScoringGuarantorDto> scoringGuarantorDto in scoringGuarantorDtos)
                {
                    IList<ScoringServiceInput> scoringServiceInputs = this.ExtractRawScoringData(scoringGuarantorDto, scoringDate);
                    IList<ScoringCalculatedInput> calculatedInputs = this._scoringService.Value.CalculateScoringInput(scoringServiceInputs);
                    this._retroScoreService.Value.SaveRetroScores(Mapper.Map<IList<RetroScore>>(calculatedInputs));
                }

                scoringDate = scoringDate.AddDays(1);
            }
            this.LogMessage("Ended");
        }

        private void LogMessage(string message)
        {
            string entireMessage = $"{DateTime.Now}: {nameof(HsGuarantorApplicationScoringService)}::{nameof(this.CalculateInputVariablesForRetroScoring)}: {message}";
            this._scoringService.Value.LogInfo(() => entireMessage);
            Console.Out.WriteLine(entireMessage); // Let JAMS know what's going on too

        }
    }
}
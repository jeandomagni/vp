﻿namespace Ivh.Application.HospitalData
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Autofac;
    using Base.Common.Interfaces;
    using Base.Services;
    using Common.Interfaces;
    using Domain.HospitalData.Visit.Entities;
    using Domain.HospitalData.Visit.Interfaces;
    using FinanceManagement.Common.Interfaces;
    using Ivh.Common.Base.Utilities.Helpers;
    using Ivh.Common.Data;
    using Ivh.Common.Data.Connection.Connections;
    using Ivh.Common.Data.Interfaces;
    using Ivh.Common.DependencyInjection;
    using Ivh.Common.ServiceBus.Attributes;
    using Ivh.Common.ServiceBus.Enums;
    using Ivh.Common.VisitPay.Messages.FinanceManagement;
    using Ivh.Common.VisitPay.Messages.HospitalData;
    using MassTransit;
    using NHibernate;

    public class VpOutboundVisitApplicationService : ApplicationService, IVpOutboundVisitApplicationService
    {
        private readonly Lazy<IOutboundVisitService> _outboundVisitService;
        private readonly Lazy<IOutboundVisitTransactionService> _outboundVisitTransactionService;

        private readonly ISession _session;

        public VpOutboundVisitApplicationService(
            Lazy<IApplicationServiceCommonService> applicationServiceCommonService,
            Lazy<IOutboundVisitService> outboundVisitService,
            Lazy<IOutboundVisitTransactionService> outboundVisitTransactionService,
            ISessionContext<CdiEtl> sessionContext) : base(applicationServiceCommonService)
        {
            this._outboundVisitService = outboundVisitService;
            this._outboundVisitTransactionService = outboundVisitTransactionService;
            this._session = sessionContext.Session;
        }

        public void CreateOutboundVisit(OutboundVisitMessage outboundVisitMessage)
        {
            VpOutboundVisit vpOutboundVisit = new VpOutboundVisit
            {
                VpVisitId = outboundVisitMessage.VpVisitId,
                BillingSystemId = outboundVisitMessage.BillingSystemId,
                SourceSystemKey = outboundVisitMessage.SourceSystemKey,
                VpOutboundVisitMessageType = outboundVisitMessage.VpOutboundVisitMessageType,
                ActionDate = outboundVisitMessage.ActionDate,
                OriginalFinancePlanDuration = outboundVisitMessage.OriginalFinancePlanDuration,
                FinancePlanClosedReasonEnum = outboundVisitMessage.FinancePlanClosedReasonEnum,
                OutboundValue = outboundVisitMessage.OutboundValue,
                IsCallCenter = outboundVisitMessage.IsCallCenter,
                IsAutoPay = outboundVisitMessage.IsAutoPay
            };

            this._outboundVisitService.Value.CreateOutboundVisit(vpOutboundVisit);
        }

        public void CreateOutboundVisits(IList<OutboundVisitMessage> messages)
        {
            using (UnitOfWork uow = new UnitOfWork(this._session))
            {
                foreach (OutboundVisitMessage outboundVisitMessage in messages)
                {
                    this.CreateOutboundVisit(outboundVisitMessage);
                }
                uow.Commit();
            }
        }

        public void CreateOutboundVisitTransactions(IList<PaymentAllocationMessage> messages)
        {
            this._outboundVisitTransactionService.Value.CreateOutboundVisitTransactions(messages);
        }

        public void CreateOutboundVisitTransactions(IList<InterestEventMessage> messages)
        {
            this._outboundVisitTransactionService.Value.CreateOutboundVisitTransactions(messages);
        }

        #region Message Consumers

        [UniversalConsumer(UniversalQueuesEnum.HsDataProcessor, UniversalPriorityEnum.Priority30)]
        public void ConsumeMessage(OutboundVisitMessage message, ConsumeContext<OutboundVisitMessage> consumeContext)
        {
            this.CreateOutboundVisit(message);
        }

        public bool IsMessageValid(OutboundVisitMessage message, ConsumeContext<OutboundVisitMessage> consumeContext)
        {
            bool isValid = true;
            return isValid;
        }

        [UniversalConsumer(UniversalQueuesEnum.HsDataProcessor, UniversalPriorityEnum.Priority30)]
        public void ConsumeMessage(OutboundVisitMessageList message, ConsumeContext<OutboundVisitMessageList> consumeContext)
        {
            this.CreateOutboundVisits(message.Messages);
        }

        public bool IsMessageValid(OutboundVisitMessageList message, ConsumeContext<OutboundVisitMessageList> consumeContext)
        {
            bool isValid = true;
            return isValid;
        }

        [UniversalConsumer(UniversalQueuesEnum.HsDataProcessor, UniversalPriorityEnum.Priority30)]
        public void ConsumeMessage(PaymentAllocationMessage message, ConsumeContext<PaymentAllocationMessage> consumeContext)
        {
            this.ProcessPaymentAllocationMessageList(new PaymentAllocationMessageList()
            {
                Messages = new List<PaymentAllocationMessage>() { message }
            });
        }

        public bool IsMessageValid(PaymentAllocationMessage message, ConsumeContext<PaymentAllocationMessage> consumeContext)
        {
            bool isValid = true;
            return isValid;
        }

        [UniversalConsumer(UniversalQueuesEnum.HsDataProcessor, UniversalPriorityEnum.Priority30)]
        public void ConsumeMessage(PaymentAllocationMessageList message, ConsumeContext<PaymentAllocationMessageList> consumeContext)
        {
            this.ProcessPaymentAllocationMessageList(message);
        }

        public bool IsMessageValid(PaymentAllocationMessageList message, ConsumeContext<PaymentAllocationMessageList> consumeContext)
        {
            bool isValid = true;
            return isValid;
        }

        [UniversalConsumer(UniversalQueuesEnum.HsDataProcessor, UniversalPriorityEnum.Priority30)]
        public void ConsumeMessage(InterestEventMessageList message, ConsumeContext<InterestEventMessageList> consumeContext)
        {
            this.ProcessInterestEventMessageList(message);
        }

        public bool IsMessageValid(InterestEventMessageList message, ConsumeContext<InterestEventMessageList> consumeContext) => true;

        #endregion


        private void ProcessPaymentAllocationMessageList(PaymentAllocationMessageList message)
        {
            if (message != null)
            {
                Func<PaymentAllocationMessageList, string> sourceSystemKeys = (paml) => string.Join(",", paml.Messages.Select(x => x.VisitSourceSystemKey));
                Func<PaymentAllocationMessageList, string> hsBillingIds = (paml) => string.Join(",", paml.Messages.Select(x => x.VisitBillingSystemId));
                Func<PaymentAllocationMessageList, string> vpPaymentIds = (paml) => string.Join(",", paml.Messages.Select(x => x.VpPaymentId.HasValue ? x.VpPaymentId.Value.ToString() : "NULL"));

                Action<Func<ILifetimeScope, ISession>, Action<ILifetimeScope, PaymentAllocationMessageList>, PaymentAllocationMessageList, string> executeWithRetry = (sessionFunc, commandAction, paml, logMessage) =>
                {
                    using (ILifetimeScope scope = IvinciContainer.Instance.Container().BeginLifetimeScope())
                    {
                        using (SqlRetry sqlRetry = new SqlRetry(1))
                        {
                            bool retry;
                            do
                            {
                                retry = false;
                                Action<PaymentAllocationMessageList, Exception, string> logRetry = (p, e, m) =>
                                {
                                    this.LoggingService.Value.Warn(() => $"EtlVpOutboundVisitTransactionMessageConsumer::Consume<PaymentAllocationMessageList> {m} - (retrying once) for SourceSystemKeys = {sourceSystemKeys(p)}, HsBillingIds = {hsBillingIds(p)}, VpPaymentIds = {vpPaymentIds(p)}, exception = {ExceptionHelper.AggregateExceptionToString(e)}");
                                };

                                Action<PaymentAllocationMessageList, Exception, string> logFailure = (p, e, m) =>
                                {
                                    this.LoggingService.Value.Fatal(() => $"EtlVpOutboundVisitTransactionMessageConsumer::Consume<List<PaymentAllocationMessage>> {m} - IFinancePlanApplicationService failed for SourceSystemKeys = {sourceSystemKeys(p)}, HsBillingIds = {hsBillingIds(p)}, VpPaymentIds = {vpPaymentIds(p)}, exception = {ExceptionHelper.AggregateExceptionToString(e)}");
                                };
                                try
                                {
                                    using (UnitOfWork unitOfWork = new UnitOfWork(sessionFunc(scope)))
                                    {
                                        try
                                        {
                                            commandAction(scope, paml);
                                            unitOfWork.Commit();
                                        }
                                        catch (Exception)
                                        {
                                            unitOfWork.Rollback();
                                            throw;
                                        }
                                    }
                                }
                                catch (Exception e)
                                {
                                    retry = sqlRetry.CanRetry(e);
                                    if (retry)
                                    {
                                        logRetry(paml, e, logMessage);
                                    }
                                    else
                                    {
                                        logFailure(paml, e, logMessage);
                                        throw;
                                    }
                                }
                            } while (retry);
                        }
                    }
                };

                executeWithRetry(
                    (ls) => ls.Resolve<ISessionContext<VisitPay>>().Session,
                    (ls, paml) =>
                    {
                        IFinancePlanApplicationService financePlanApplicationService = ls.Resolve<IFinancePlanApplicationService>();
                        financePlanApplicationService.PopulateFinancePlanInfoOnPaymentAllocationMessages(paml.Messages);
                    },
                    message,
                    "PopulateFinancePlanInfoOnPaymentAllocationMessages");

                executeWithRetry(
                    (ls) => ls.Resolve<ISessionContext<VisitPay>>().Session,
                    (ls, paml) =>
                    {
                        IPaymentSubmissionApplicationService paymentSubmissionApplicationService = ls.Resolve<IPaymentSubmissionApplicationService>();
                        paymentSubmissionApplicationService.PopulatePaymentMethodTypeOnPaymentAllocationMessages(paml.Messages);
                    },
                    message,
                    "PopulatePaymentMethodTypeOnPaymentAllocationMessages");

                executeWithRetry(
                    (ls) => ls.Resolve<ISessionContext<CdiEtl>>().Session,
                    (ls, paml) =>
                    {
                        IVpOutboundVisitApplicationService outboundVisitTransactionService = ls.Resolve<IVpOutboundVisitApplicationService>();
                        outboundVisitTransactionService.CreateOutboundVisitTransactions(paml.Messages);
                    },
                    message,
                    "CreateOutboundVisitTransactions");
            }
        }

        private void ProcessInterestEventMessageList(InterestEventMessageList messageList)
        {
            if (messageList == null)
            {
                return;
            }

            using (ILifetimeScope scope = IvinciContainer.Instance.Container().BeginLifetimeScope())
            {
                using (UnitOfWork unitOfWork = new UnitOfWork(scope.Resolve<ISessionContext<VisitPay>>().Session))
                {
                    try
                    {
                        IVpOutboundVisitApplicationService outboundVisitTransactionService = scope.Resolve<IVpOutboundVisitApplicationService>();
                        outboundVisitTransactionService.CreateOutboundVisitTransactions(messageList.Messages);
                        unitOfWork.Commit();
                    }
                    catch (Exception)
                    {
                        unitOfWork.Rollback();
                    }
                }
            }
        }
    }
}
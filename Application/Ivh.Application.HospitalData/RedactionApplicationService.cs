﻿namespace Ivh.Application.HospitalData
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Base.Common.Interfaces;
    using Base.Services;
    using Common.Interfaces;
    using Domain.HospitalData.Change.Interfaces;
    using Domain.HospitalData.HsGuarantor.Interfaces;
    using Domain.HospitalData.Visit.Interfaces;
    using Domain.HospitalData.VpGuarantor.Entities;
    using Ivh.Common.Base.Utilities.Extensions;
    using Ivh.Common.Data;
    using Ivh.Common.Data.Connection.Connections;
    using Ivh.Common.Data.Interfaces;
    using Ivh.Common.ServiceBus.Attributes;
    using Ivh.Common.ServiceBus.Enums;
    using Ivh.Common.VisitPay.Enums;
    using Ivh.Common.VisitPay.Messages.HospitalData;
    using MassTransit;
    using NHibernate;

    public class RedactionApplicationService : ApplicationService, IRedactionApplicationService
    {
        private readonly Lazy<IChangeEventService> _changeEventService;
        private readonly Lazy<IVpGuarantorHsMatchService> _vpGuarantorHsMatchService;
        private readonly Lazy<IVisitMatchStatusService> _visitMatchStatusService;
        private readonly ISession _session;

        public RedactionApplicationService(
            Lazy<IApplicationServiceCommonService> applicationServiceCommonService,
            Lazy<IChangeEventService> changeEventService,
            Lazy<IVpGuarantorHsMatchService> vpGuarantorHsMatchService,
            Lazy<IVisitMatchStatusService> visitMatchStatusService,
            ISessionContext<CdiEtl> session) : base(applicationServiceCommonService)
        {
            this._changeEventService = changeEventService;
            this._vpGuarantorHsMatchService = vpGuarantorHsMatchService;
            this._visitMatchStatusService = visitMatchStatusService;
            this._session = session.Session;
        }

        public void ProcessHsGuarantorUnmatchMessage(HsGuarantorUnmatchMessage hsGuarantorUnmatchMessage)
        {
            using (UnitOfWork unitOfWork = new UnitOfWork(this._session))
            {
                this._vpGuarantorHsMatchService.Value.UnmatchVpGuarantorHsMatch(
                    hsGuarantorUnmatchMessage.HsGuarantorId,
                    hsGuarantorUnmatchMessage.VpGuarantorId,
                    hsGuarantorUnmatchMessage.HsGuarantorMatchStatus);

                unitOfWork.Commit();
            }
        }

        public void ProcessHsGuarantorVisitUnmatchMessage(HsGuarantorVisitUnmatchMessage hsGuarantorVisitUnmatchMessage)
        {
            this.ProcessHsGuarantorVisitUnmatchMessages(hsGuarantorVisitUnmatchMessage.ToListOfOne());
        }

        public void ProcessHsGuarantorVisitUnmatchMessages(IList<HsGuarantorVisitUnmatchMessage> hsGuarantorVisitUnmatchMessages)
        {
            using (UnitOfWork unitOfWork = new UnitOfWork(this._session))
            {
                foreach (HsGuarantorVisitUnmatchMessage hsGuarantorVisitUnmatchMessage in hsGuarantorVisitUnmatchMessages)
                {
                    this._visitMatchStatusService.Value.UnmatchVpVisit(
                        hsGuarantorVisitUnmatchMessage.HsGuarantorId,
                        hsGuarantorVisitUnmatchMessage.VpGuarantorId,
                        hsGuarantorVisitUnmatchMessage.BillingSystemId,
                        hsGuarantorVisitUnmatchMessage.SourceSystemKey,
                        hsGuarantorVisitUnmatchMessage.VpVisitId,
                        hsGuarantorVisitUnmatchMessage.HsGuarantorMatchStatus);
                }
                unitOfWork.Commit();
            }
        }

        public void ProcessHsGuarantorRematchMessage(HsGuarantorRematchMessage hsGuarantorRematchMessage)
        {
            // update cdi.vp.VpGuarantorHsMatch HsGuarantorMatchStatusId back to 1
            IList<VpGuarantorHsMatch> vpGuarantorHsMatchs = this._vpGuarantorHsMatchService.Value.GetVpGuarantorHsMatches(hsGuarantorRematchMessage.HsGuarantorId, hsGuarantorRematchMessage.VpGuarantorId);
            if (vpGuarantorHsMatchs != null)
            {
                foreach (VpGuarantorHsMatch vpGuarantorHsMatch in vpGuarantorHsMatchs)
                {
                    vpGuarantorHsMatch.HsGuarantorMatchStatus = HsGuarantorMatchStatusEnum.Matched;
                    vpGuarantorHsMatch.UnmatchedOn = null;
                    vpGuarantorHsMatch.MatchedOn = DateTime.UtcNow;
                    vpGuarantorHsMatch.IsActive = true;

                    this._vpGuarantorHsMatchService.Value.InsertOrUpdate(vpGuarantorHsMatch);
                }

                // publish changeEvent to rematch all the visits and transactions for the guarantor
                this._changeEventService.Value.GenerateChangeEventForGuarantor(vpGuarantorHsMatchs);
            }
        }

        #region Message Consumers

        [UniversalConsumer(UniversalQueuesEnum.HsDataProcessor, UniversalPriorityEnum.Priority30)]
        public void ConsumeMessage(HsGuarantorRematchMessage message, ConsumeContext<HsGuarantorRematchMessage> consumeContext)
        {
            this.ProcessHsGuarantorRematchMessage(message);
        }

        public bool IsMessageValid(HsGuarantorRematchMessage message, ConsumeContext<HsGuarantorRematchMessage> consumeContext)
        {
            bool isValid = true;
            return isValid;
        }

        [UniversalConsumer(UniversalQueuesEnum.HsDataProcessor, UniversalPriorityEnum.Priority30)]
        public void ConsumeMessage(HsGuarantorUnmatchMessage message, ConsumeContext<HsGuarantorUnmatchMessage> consumeContext)
        {
            this.ProcessHsGuarantorUnmatchMessage(message);
        }

        public bool IsMessageValid(HsGuarantorUnmatchMessage message, ConsumeContext<HsGuarantorUnmatchMessage> consumeContext)
        {
            bool isValid = true;
            return isValid;
        }

        [UniversalConsumer(UniversalQueuesEnum.HsDataProcessor, UniversalPriorityEnum.Priority30)]
        public void ConsumeMessage(HsGuarantorVisitUnmatchMessage message, ConsumeContext<HsGuarantorVisitUnmatchMessage> consumeContext)
        {
            this.ProcessHsGuarantorVisitUnmatchMessage(message);
        }

        public bool IsMessageValid(HsGuarantorVisitUnmatchMessage message, ConsumeContext<HsGuarantorVisitUnmatchMessage> consumeContext)
        {
            bool isValid = true;
            return isValid;
        }

        #endregion
    }
}
﻿namespace Ivh.Application.HospitalData
{
    using System;
    using AutoMapper;
    using Base.Common.Interfaces;
    using Base.Services;
    using Common.Interfaces;
    using Common.Responses;
    using Domain.HospitalData.VpGuarantor.Entities;
    using Domain.HospitalData.VpGuarantor.Interfaces;
    using Ivh.Common.VisitPay.Messages.HospitalData.Dtos;

    public class VpGuarantorMatchInfoApplicationService : ApplicationService, IVpGuarantorMatchInfoApplicationService
    {
        private readonly Lazy<IHospitalGuarantorRepository> _hospitalGuarantorRepo;

        public VpGuarantorMatchInfoApplicationService(
            Lazy<IApplicationServiceCommonService> applicationServiceCommonService,
            Lazy<IHospitalGuarantorRepository> hospitalGuarantorRepo) : base(applicationServiceCommonService)
        {
            this._hospitalGuarantorRepo = hospitalGuarantorRepo;
        }

        public GuarantorMatchResponse GetInitialMatchExpress(VpGuarantorMatchInfoDto vpGuarantorMatchInfo)
        {
            return this._hospitalGuarantorRepo.Value.GetInitialMatchExpress(Mapper.Map<VpGuarantorMatchInfo>(vpGuarantorMatchInfo));
        }

        public GuarantorMatchResponse GetInitialMatch(VpGuarantorMatchInfoDto vpGuarantorMatchInfo)
        {
            return this._hospitalGuarantorRepo.Value.GetInitialMatch(Mapper.Map<VpGuarantorMatchInfo>(vpGuarantorMatchInfo));            
        }

        public void AddInitialMatch(VpGuarantorMatchInfoDto vpGuarantorMatchInfo)
        {
            this._hospitalGuarantorRepo.Value.AddInitialMatch(Mapper.Map<VpGuarantorMatchInfo>(vpGuarantorMatchInfo));                        
        }

        public void AddInitialMatchVpcc(VpGuarantorMatchInfoDto vpGuarantorMatchInfo)
        {
            this._hospitalGuarantorRepo.Value.AddInitialMatchVpcc(Mapper.Map<VpGuarantorMatchInfo>(vpGuarantorMatchInfo));
        }

        public void UpdateMatch(VpGuarantorMatchInfoDto vpGuarantorMatchInfo)
        {
            this._hospitalGuarantorRepo.Value.UpdateMatch(Mapper.Map<VpGuarantorMatchInfo>(vpGuarantorMatchInfo));                                    
        }

        public void GetNewMatches(int vpGuarantorId)
        {
            this._hospitalGuarantorRepo.Value.GetNewMatches(vpGuarantorId);
        }
    }
}
﻿namespace Ivh.Application.HospitalData.Mappings
{
    using System.Collections.Generic;
    using System.Linq;
    using AutoMapper;
    using Common.Models;
    using Domain.HospitalData.Application.Entities;
    using Domain.HospitalData.BillingSystem.Entities;
    using Domain.HospitalData.Change.Entities;
    using Domain.HospitalData.Eob.Entities;
    using Domain.HospitalData.HsGuarantor.Entities;
    using Domain.HospitalData.Scoring.Entities;
    using Domain.HospitalData.Segmentation.Entities;
    using Domain.HospitalData.Visit.Entities;
    using Domain.HospitalData.VpGuarantor.Entities;
    using Ivh.Common.Base.Utilities.Extensions;
    using Ivh.Common.VisitPay.Enums;
    using Ivh.Common.VisitPay.Messages.HospitalData.Dtos;
    using Ivh.Common.VisitPay.Utilities.Helpers;

    public class AutomapperProfile : Profile
    {
        public AutomapperProfile()
        {
            this.CreateMap<Visit, HsVisitDto>();
            this.CreateMap<HsVisitDto, Visit>()
                .ForMember(d => d.VisitTransactions, s => s.Ignore()); // This needs to be done manually cuz nHibernate needs to keep track of the VisitTransactions and AutoMapper doesn't do this right.

            this.CreateMap<VpOutboundVisitTransactionDto, VpOutboundVisitTransaction>();

            this.CreateMapBidirectional<BalanceTransferStatusDto, BalanceTransferStatus>();

            this.CreateMap<VpOutboundVisitTransaction, VpOutboundVisitTransactionDto>();

            this.CreateMap<VpOutboundVisitDto, VpOutboundVisit>()
                .ForMember(t => t.VpOutboundVisitMessageType, f => f.MapFrom(fr => (VpOutboundVisitMessageTypeEnum)fr.VpOutboundVisitMessageTypeId));

            this.CreateMap<VpOutboundVisit, VpOutboundVisitDto>()
                .ForMember(dest => dest.VpOutboundVisitMessageTypeId, opts => opts.MapFrom(f => (int)f.VpOutboundVisitMessageType))
                .ForMember(dest => dest.VpOutboundVisitMessageTypeName, opts => opts.MapFrom(f => f.VpOutboundVisitMessageType.ToString()));

            this.CreateMap<VisitTransaction, HsVisitTransactionDto>()
                .ForMember(t => t.VpTransactionTypeName, f => f.MapFrom(fr => fr.VpTransactionType.DisplayName))
                .ForMember(t => t.VpTransactionTypeId, f => f.MapFrom(fr => fr.VpTransactionType.VpTransactionTypeId))
                .ForMember(t => t.VisitId, f => f.MapFrom(fr => fr.Visit.VisitId));

            this.CreateMap<HsVisitTransactionDto, VisitTransaction>()
                .ForMember(t => t.VpTransactionType, f => f.MapFrom(fr => new VpTransactionType() { VpTransactionTypeId = fr.VpTransactionTypeId, DisplayName = fr.VpTransactionTypeName }));
            
            this.CreateMapBidirectional<HsGuarantor, HsGuarantorDto>();
            this.CreateMapBidirectional<HsGuarantorFilterDto, HsGuarantorFilter>();
            this.CreateMapBidirectional<HsGuarantorSearchResult, HsGuarantorSearchResultDto>();

            this.CreateMap<HsGuarantor, HsGuarantorModel>();
            this.CreateMap<HsGuarantorVpSearchResult, HsGuarantorVpSearchResultModel>();
            
            this.CreateMap<VpGuarantorHsMatchDto, VpGuarantorHsMatch>();
            this.CreateMap<VpGuarantorHsMatch, VpGuarantorHsMatchDto>();

            this.CreateMapBidirectional<HsGuarantorVpSearchResultDto, HsGuarantorVpSearchResult>();
            this.CreateMapBidirectional<HsGuarantorVpSearchFilterDto, HsGuarantorVpSearchFilter>();


            this.CreateMapBidirectional<HsBillingSystem, HsBillingSystemDto>();
            this.CreateMapBidirectional<VpTransactionType, VpTransactionTypeDto>();

            this.CreateMap<Visit, VisitChange>()
                .ForMember(dest => dest.VisitInsurancePlans, opts => opts.MapFrom(x => Mapper.Map<List<VisitInsurancePlanChange>>(x.VisitInsurancePlans)))
                .ForMember(dest => dest.VisitTransactions, opts => opts.MapFrom(x => Mapper.Map<List<VisitTransactionChange>>(x.VisitTransactions.Where(t => t.VpTransactionType != null && t.VpTransactionType.VpTransactionTypeId != -1))))
                .ForMember(dest => dest.VisitPaymentAllocations, opts => opts.MapFrom(x => Mapper.Map<List<VisitPaymentAllocationChange>>(x.VisitPaymentAllocations)));

            this.CreateMap<VisitChange, Visit>()
                .ForMember(dest => dest.Facility, opts => opts.Ignore())
                .ForMember(dest => dest.VisitInsurancePlans, opts => opts.Ignore())
                .ForMember(dest => dest.VisitTransactions, opts => opts.Ignore())
                .ForMember(dest => dest.VisitPaymentAllocations, opts => opts.Ignore());

            this.CreateMap<VisitInsurancePlan, VisitInsurancePlanChange>();
            this.CreateMap<VisitInsurancePlanChange, VisitInsurancePlan>();

            this.CreateMap<VisitTransactionChangeDto, VisitTransaction>()
                .ForMember(dest => dest.VisitTransactionId, x => x.Ignore())
                .ForMember(dest => dest.VpPaymentAllocationId, x => x.MapFrom(src => src.PaymentAllocationId))
                .ForMember(dest => dest.VpTransactionType, x => x.MapFrom(src => new VpTransactionType { VpTransactionTypeId = src.VpTransactionTypeId }));


            this.CreateMapBidirectional<TransactionCode, TransactionCodeDto>();
            
            this.CreateMap<ChangeSetDto, ChangeSet>()
                .ForMember(dest => dest.ChangeEvents, opts => opts.MapFrom(x => Mapper.Map<List<ChangeEvent>>(x.ChangeEvents)))
                .ForMember(dest => dest.HsGuarantorChanges, opts => opts.MapFrom(x => Mapper.Map<List<HsGuarantorChange>>(x.HsGuarantorChanges)))
                .ForMember(dest => dest.VisitChanges, opts => opts.MapFrom(x => Mapper.Map<List<VisitChange>>(x.VisitChanges)));


            this.CreateMap<ChangeSet, ChangeSetDto>();
            this.CreateMap<ChangeEvent, ChangeEventDto>();
            this.CreateMap<ChangeEventDto, ChangeEvent>();
            
            this.CreateMapBidirectional<ChangeEventType, ChangeEventTypeDto>();
            this.CreateMapBidirectional<Application, ApplicationDto>();

            this.CreateMap<HsGuarantorChange, HsGuarantorDto>()
                .ForMember(dest => dest.SSN4, opts => opts.MapFrom(src => BaseMappingHelper.MapNullableToString(src.SSN4)));
            this.CreateMap<HsGuarantorDto, HsGuarantorChange>();

            this.CreateMap<VisitChange, VisitChangeDto>()
                .ForMember(dest => dest.Facility, opts => opts.MapFrom(src => src.Facility))
                .ForMember(dest => dest.AgingTier, opts => opts.MapFrom(src => (AgingTierEnum) (src.AgingTierId ?? 0)));

            this.CreateMap<VisitChangeDto, VisitChange>()
                .ForMember(dest => dest.Facility, opts => opts.MapFrom(x => x.Facility))
                .ForMember(dest => dest.VisitInsurancePlans, opts => opts.MapFrom(x => Mapper.Map<List<VisitInsurancePlanChange>>(x.VisitInsurancePlans)))
                .ForMember(dest => dest.ChangeEventId, opts => opts.MapFrom(x => x.ChangeEventId))
                .ForMember(dest => dest.VisitTransactions, opts => opts.MapFrom(x => Mapper.Map<List<VisitTransactionChange>>(x.VisitTransactions)))
                .ForMember(dest => dest.VisitPaymentAllocations, opts => opts.MapFrom(x => Mapper.Map<List<VisitPaymentAllocationChange>>(x.VisitPaymentAllocations)))
                .ForMember(dest => dest.AgingTierId,opts => opts.MapFrom(src => (int)src.AgingTier));

            // visit payment allocation

            this.CreateMap<VisitPaymentAllocationDto, VisitPaymentAllocation>();
            this.CreateMap<VisitPaymentAllocation, VisitPaymentAllocationDto>()
                .ForMember(dest => dest.VisitId, opts => opts.MapFrom(src => src.Visit.VisitId));

            this.CreateMap<VisitPaymentAllocationChangeDto, VisitPaymentAllocationDto>();
            this.CreateMap<VisitPaymentAllocationDto, VisitPaymentAllocationChangeDto>();

            this.CreateMap<VisitPaymentAllocationChange, VisitPaymentAllocationChangeDto>();
            this.CreateMap<VisitPaymentAllocationChangeDto, VisitPaymentAllocationChange>();

            this.CreateMapBidirectional<VisitPaymentAllocationChange, VisitPaymentAllocation>();

            // visit transaction

            this.CreateMap<VisitTransactionChange, VisitTransactionChangeDto>()
                .ForMember(dest => dest.ChangeEventId, opts => opts.MapFrom(src => src.VisitChange.ChangeEventId))
                .ForMember(dest => dest.PaymentAllocationId, opts => opts.MapFrom(src => src.VpPaymentAllocationId))
                .ForMember(dest => dest.TransactionCodeSourceSystemKey, opts => opts.MapFrom(src => src.TransactionCodeSourceSystemKey))
                .ForMember(dest => dest.TransactionCodeBillingSystemId, opts => opts.MapFrom(src => src.TransactionCodeBillingSystemId))
                .ForMember(dest => dest.TransactionCodeId, opts => opts.MapFrom(src => src.TransactionCodeId));

            this.CreateMap<VisitTransactionChangeDto, VisitTransactionChange>()
                .ForMember(dest => dest.TransactionCodeId, opts => opts.MapFrom(src => src.TransactionCodeId))
                .ForMember(dest => dest.VpPaymentAllocationId, opts => opts.MapFrom(src => src.PaymentAllocationId));

            // visit insurance plan
            this.CreateMap<VisitInsurancePlanChange, VisitInsurancePlanChangeDto>()
                .ForMember(dest => dest.ChangeEventId, opts => opts.MapFrom(src => src.VisitChange.ChangeEventId));
            
            this.CreateMap<VisitInsurancePlanChangeDto, VisitInsurancePlanChange>();

            this.CreateMapBidirectional<VisitInsurancePlanDto, VisitInsurancePlan>();

            this.CreateMapBidirectional<LifeCycleStage, LifeCycleStageDto>();
            this.CreateMapBidirectional<PatientType, PatientTypeDto>();
            this.CreateMapBidirectional<Facility, FacilityDto>();

            this.CreateMap<PrimaryInsuranceTypeDto, PrimaryInsuranceType>();

            this.CreateMap<PrimaryInsuranceType, PrimaryInsuranceTypeDto>();

            this.CreateMapBidirectional<InsurancePlan, InsurancePlanDto>();
            this.CreateMapBidirectional<RevenueWorkCompany, RevenueWorkCompanyDto>();

            this.CreateMapBidirectional<VpOutboundHsGuarantor, VpOutboundHsGuarantorDto>();

            this.CreateMapBidirectional<VpGuarantorMatchInfoDto, VpGuarantorMatchInfo>();         

            this.CreateMap<VisitTransaction, GuarantorAccountTransaction>();
            this.CreateMap<VisitBatch, ScoringGuarantorVisitBatchDto>()
                .ForMember(dest => dest.BillingSystemName, opts => opts.MapFrom(src => src.Visit.BillingSystem.BillingSystemName))
                .ForMember(dest => dest.FacilityId, opts => opts.MapFrom(src => src.Visit.Facility.FacilityId))
                .ForMember(dest => dest.StateCode, opts => opts.MapFrom(src => src.Visit.Facility.StateCode))
                .ForMember(dest => dest.FacilityDescription, opts => opts.MapFrom(src => src.Visit.Facility.FacilityDescription))
                .ForMember(dest => dest.RegionId, opts => opts.MapFrom(src => src.Visit.BillingSystem.RegionId != null ? src.Visit.BillingSystem.RegionId : null))
                .ForMember(dest => dest.RegionScope, opts => opts.MapFrom(src => src.Visit.BillingSystem.RegionId != null ? src.Visit.BillingSystem.Region.RegionScope : null));
            this.CreateMap<ScoringGuarantorVisitBatchDto, GuarantorAccountVisit>();
            this.CreateMap<ScoringCalculatedInputDto, Score>()
                .ForMember(dest => dest.ParentHsGuarantorId, opts => opts.MapFrom(src => src.HsGuarantorId));
            this.CreateMap<Score, ScoringCalculatedInputDto>()
                .ForMember(dest => dest.HsGuarantorId, opts => opts.MapFrom(src => src.ParentHsGuarantorId));
            this.CreateMap<ProtoScore, ProtoScoreDto>().ReverseMap();
            this.CreateMap<ScoringGuarantorScoreDto, Score>()
                .ForMember(dest => dest.ParentHsGuarantorId, opts => opts.MapFrom(src => src.HsGuarantorId))
                .ForMember(dest => dest.Phase2ScriptVersion, opts => opts.MapFrom(src => src.S2Version));
            this.CreateMap<ScoringCalculatedInput, ScoringCalculatedInputDto>();
            this.CreateMap<ScoringCalculatedInput, RetroScore>()
                .ForMember(dest => dest.ParentHsGuarantorId, opts => opts.MapFrom(src => src.HsGuarantorId));
            this.CreateMap<ThirdPartyData, ScoringThirdPartyDto>();
            this.CreateMap<ThirdPartyData, ThirdPartyData>();
            this.CreateMap<ScoringStatisticalMonitoring, ScoringStatisticalMonitoringDto>();
            this.CreateMap<ScoringMonitoringThresholds, ScoringMonitoringThresholdsDto>();
            this.CreateMap<ScoringCalculatedInput, Score>()
                .ForMember(dest => dest.ParentHsGuarantorId, opts => opts.MapFrom(src => src.HsGuarantorId));
            this.CreateMap<Score, ScoringCalculatedInput>()
                .ForMember(dest => dest.HsGuarantorId, opts => opts.MapFrom(src => src.ParentHsGuarantorId));

            this.CreateMap<AccountsReceivableVisitDaily, SegmentationAccountsReceivableDto>();
            this.CreateMap<SegmentationAccountsReceivableDto, SegmentationServiceData>();
            this.CreateMap<SegmentationAccountsReceivableDto, GuarantorSegmentationDaily>()
                .ForMember(dest => dest.SegmentationValue, opts => opts.MapFrom(src => src.RawSegmentationScore));

            this.CreateMap<ActivePassiveVisitDaily, SegmentationActivePassiveDto>();
            this.CreateMap<SegmentationActivePassiveDto, SegmentationServiceActivePassiveData>();
            this.CreateMap<SegmentationActivePassiveDto, GuarantorSegmentationDaily>();
            this.CreateMap<ActivePassiveSegmentationThreshold, ActivePassiveSegmentationThresholdDto>();

            this.CreateMap<BadDebtVisitDaily, SegmentationBadDebtDto>();
            this.CreateMap<SegmentationBadDebtDto, SegmentationServiceData>();
            this.CreateMap<SegmentationBadDebtDto, GuarantorSegmentationDaily>();
            
            this.CreateMap<ExternalLink, Core.Common.Dtos.ExternalLinkDto>();
        }
    }
}

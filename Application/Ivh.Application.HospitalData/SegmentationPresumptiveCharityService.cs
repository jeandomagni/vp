﻿namespace Ivh.Application.HospitalData
{
    using System;
    using System.Collections.Generic;
    using Autofac;
    using Common.Interfaces;
    using Domain.HospitalData.Segmentation.Interfaces;
    using Domain.Logging.Interfaces;
    using Ivh.Common.DependencyInjection;
    using Ivh.Common.VisitPay.Constants;

    public class SegmentationPresumptiveCharityService : SegmentationBase<object>, ISegmentationPresumptiveCharityService
    {
        public SegmentationPresumptiveCharityService(Lazy<ILoggingService> loggingService, Lazy<IMetricsProvider> metricsProvider) : base(loggingService, metricsProvider)
        {
            this.SuccessStatName = Metrics.Increment.Scoring.SegmentationPresumptiveCharitySuccesses;
            this.FailureStatName = Metrics.Increment.Scoring.SegmentationPresumptiveCharityFailures;
        }

        protected override void ProcessSegmentation(List<object> segmentationDtoList)
        {
            using (ILifetimeScope lifetimeScope = IvinciContainer.Instance.Container().BeginLifetimeScope())
            {
                ISegmentationDailyProcessService segmentationDailyProcessService = lifetimeScope.Resolve<ISegmentationDailyProcessService>();
                segmentationDailyProcessService.ExecutePresumptiveCharitySegmentation(this.SegmentationBatchId);
            }
        }

        protected override void UpdateSegmentationHistory()
        {
            using (ILifetimeScope lifetimeScope = IvinciContainer.Instance.Container().BeginLifetimeScope())
            {
                ISegmentationDailyProcessService segmentationDailyProcessService = lifetimeScope.Resolve<ISegmentationDailyProcessService>();
                segmentationDailyProcessService.UpdatePresumptiveCharityHistory(this.SegmentationBatchId);
            }
        }

        public bool ProcessPresumptiveCharitySegmentation(int scoringBatchId, string currentState, string eventName, Guid correlationId, Guid processId)
        {
            return this.ProcessSegmentationBatch(null, scoringBatchId, currentState, eventName, correlationId, processId);
        }
    }
}

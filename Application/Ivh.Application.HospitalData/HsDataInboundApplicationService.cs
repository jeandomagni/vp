﻿namespace Ivh.Application.HospitalData
{
    using System;
    using Base.Common.Interfaces;
    using Base.Services;
    using Common.Interfaces;
    using Domain.HospitalData.FileLoadTracking.Interfaces;
    using Domain.HospitalData.Inbound.Interfaces;
    using Domain.Powershell.Interfaces;
    using Ivh.Common.ServiceBus.Attributes;
    using Ivh.Common.ServiceBus.Enums;
    using Ivh.Common.VisitPay.Messages.HospitalData;
    using MassTransit;

    public class HsDataInboundApplicationService : ApplicationService, IHsDataInboundApplicationService
    {
        private readonly Lazy<IPowershellService> _powershellService;
        private readonly Lazy<IHsDataInboundService> _hospitalInboundService;

        public HsDataInboundApplicationService(
            Lazy<IApplicationServiceCommonService> applicationServiceCommonService,
            Lazy<IPowershellService> powershellService,
            Lazy<IHsDataInboundService> hospitalInboundService
            ) : base(applicationServiceCommonService)
        {
            this._powershellService = powershellService;
            this._hospitalInboundService = hospitalInboundService;
        }

        public void QueueSyncGuestPayData()
        {
            this.Bus.Value.PublishMessage(new GuestPayDataSyncMessage()).Wait();
        }

        private void SyncGuestPayData()
        {
            //start inbound
            int loadTrackerId = this._hospitalInboundService.Value.StartNewInboundLoadTracker();

            //skip some steps
            this._hospitalInboundService.Value.SkipStepsBeforeGuestPayInbound(loadTrackerId);

            //run guestpay inbound
            this._powershellService.Value.Inbound_GuestPay();

            //end inbound
            this._hospitalInboundService.Value.CompleteLoadTracker(loadTrackerId);
        }

        #region consumers

        [UniversalConsumer(UniversalQueuesEnum.VisitPayProcessor, UniversalPriorityEnum.Priority30)]
        public void ConsumeMessage(GuestPayDataSyncMessage message, ConsumeContext<GuestPayDataSyncMessage> consumeContex)
        {
            this.SyncGuestPayData();
        }

        public bool IsMessageValid(GuestPayDataSyncMessage message, ConsumeContext<GuestPayDataSyncMessage> consumeContext)
        {
            bool isValid = true;
            return isValid;
        }
        
        #endregion consumers
    }
}

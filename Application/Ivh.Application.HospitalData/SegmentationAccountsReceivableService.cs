﻿namespace Ivh.Application.HospitalData
{
    using System;
    using System.Collections.Generic;
    using Common.Interfaces;
    using Autofac;
    using Domain.Logging.Interfaces;
    using Ivh.Common.DependencyInjection;
    using Ivh.Common.VisitPay.Constants;
    using Ivh.Common.VisitPay.Messages.HospitalData.Dtos;

    public class SegmentationAccountsReceivableService : SegmentationBase<SegmentationAccountsReceivableDto>, ISegmentationAccountsReceivableService
    {
        public SegmentationAccountsReceivableService(Lazy<ILoggingService> loggingService, Lazy<IMetricsProvider> metricsProvider) : base(loggingService, metricsProvider)
        {
            this.SuccessStatName = Metrics.Increment.Scoring.SegmentationAccountsReceivableSuccesses;
            this.FailureStatName = Metrics.Increment.Scoring.SegmentationAccountsReceivableFailures;
        }

        protected override void ProcessSegmentation(List<SegmentationAccountsReceivableDto> segmentationDtoList)
        {
            using (ILifetimeScope lifetimeScope = IvinciContainer.Instance.Container().BeginLifetimeScope())
            {
                IHsSegmentationApplicationService hsSegmentationApplicationService = lifetimeScope.Resolve<IHsSegmentationApplicationService>();
                IList<SegmentationAccountsReceivableDto> accountsReceivableSegmentationDtos = hsSegmentationApplicationService.ProcessAccountsReceivableSegmentation(segmentationDtoList);
                hsSegmentationApplicationService.SaveAccountsReceivableSegmentationResult(accountsReceivableSegmentationDtos);
            }
        }

        protected override void UpdateSegmentationHistory()
        {
            using (ILifetimeScope lifetimeScope = IvinciContainer.Instance.Container().BeginLifetimeScope())
            {
                IHsSegmentationApplicationService hsSegmentationApplicationService = lifetimeScope.Resolve<IHsSegmentationApplicationService>();
                hsSegmentationApplicationService.UpdateAccountsReceivableGuarantorSegmentationHistory(this.SegmentationBatchId);
            }
        }

        public bool ProcessAccountsReceivableSegmentation(List<SegmentationAccountsReceivableDto> segmentationList, int scoringBatchId, string currentState, string eventName, Guid correlationId, Guid processId)
        {
            return this.ProcessSegmentationBatch(segmentationList, scoringBatchId, currentState, eventName, correlationId, processId);
        }
    }
}

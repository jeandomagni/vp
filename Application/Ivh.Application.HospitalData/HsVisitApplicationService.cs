﻿namespace Ivh.Application.HospitalData
{
    using System;
    using System.Collections.Generic;
    using Base.Common.Interfaces;
    using Base.Services;
    using Common.Interfaces;
    using Domain.Guarantor.Interfaces;
    using Domain.HospitalData.Visit.Interfaces;
    using Ivh.Common.ServiceBus.Attributes;
    using Ivh.Common.ServiceBus.Enums;
    using Ivh.Common.VisitPay.Enums;
    using Ivh.Common.VisitPay.Messages.Core;
    using MassTransit;

    public class HsVisitApplicationService: ApplicationService, IHsVisitApplicationService
    {
        private readonly Lazy<IVisitService> _visitService;
        private readonly Lazy<IGuarantorService> _guarantorService;

        public HsVisitApplicationService(
            Lazy<IApplicationServiceCommonService> applicationServiceCommonService,
            Lazy<IGuarantorService> guarantorService,
            Lazy<IVisitService> visitService) : base(applicationServiceCommonService)
        {
            this._guarantorService = guarantorService;
            this._visitService = visitService;
        }

        #region Consumers

        [UniversalConsumer(UniversalQueuesEnum.VisitPayProcessor, UniversalPriorityEnum.Priority20)]
        public void ConsumeMessage(FirstVisitLoadedMessage message, ConsumeContext<FirstVisitLoadedMessage> consumeContext)
        {
            int visitPayUserId = this._guarantorService.Value.GetVisitPayUserId(message.VpGuarantorId);

            this.CheckForBadDebtVisits(message.HsGuarantorIds, visitPayUserId);
            this.CheckForVisitOnPrePaymentPlan(message.HsGuarantorIds, visitPayUserId);
        }

        public bool IsMessageValid(FirstVisitLoadedMessage message, ConsumeContext<FirstVisitLoadedMessage> consumeContext)
        {
            bool isValid = true;
            return isValid;
        }

        #endregion Consumers

        private void CheckForBadDebtVisits(IList<int> hsGuarantorIds, int visitPayUserId)
        {
            if (this.FeatureService.Value.IsFeatureEnabled(VisitPayFeatureEnum.FeatureShowBadDebtNotificationIsEnabled))
            {
                bool badDebtVisits = this._visitService.Value.GuarantorHasBadDebtVisits(hsGuarantorIds);

                if (badDebtVisits)
                {
                    this.Bus.Value.PublishMessage(new AddSystemMessageVisitPayUserMessage { VisitPayUserId = visitPayUserId, SystemMessageEnum = SystemMessageEnum.HasExistingBadDebt }).Wait();
                }
            }
        }

        private void CheckForVisitOnPrePaymentPlan(IList<int> hsGuarantorIds, int visitPayUserId)
        {
            if (this.FeatureService.Value.IsFeatureEnabled(VisitPayFeatureEnum.FeatureShowPrePaymentPlanNotificationIsEnabled))
            {
                bool hasVisitOnPrePaymentPlan = this._visitService.Value.GuarantorHasVisitOnPrePaymentPlan(hsGuarantorIds);

                if (hasVisitOnPrePaymentPlan)
                {
                    this.Bus.Value.PublishMessage(new AddSystemMessageVisitPayUserMessage { VisitPayUserId = visitPayUserId, SystemMessageEnum = SystemMessageEnum.OnPrePaymentPlan }).Wait();
                }
            }
        }

    }
}

﻿using System;
using System.Collections.Generic;

namespace Ivh.Application.HospitalData
{
    using AutoMapper;
    using Base.Common.Interfaces;
    using Base.Services;
    using Common.Interfaces;
    using Domain.HospitalData.Visit.Interfaces;
    using Ivh.Common.VisitPay.Messages.HospitalData.Dtos;

    public class FacilityApplicationService : ApplicationService, IFacilityApplicationService
    {
        private readonly Lazy<IFacilityService> _facilitySvc;

        public FacilityApplicationService(
            Lazy<IApplicationServiceCommonService> appSvcCommonSvc,
            Lazy<IFacilityService> facilitySvc)
            : base(appSvcCommonSvc)
        {
            this._facilitySvc = facilitySvc;
        }

        public IReadOnlyList<FacilityDto> GetAllFacilities()
        {
            return Mapper.Map<List<FacilityDto>>(this._facilitySvc.Value.GetAllFacilities());
        }
    }
}

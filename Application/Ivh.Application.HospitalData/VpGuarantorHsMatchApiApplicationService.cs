﻿namespace Ivh.Application.HospitalData
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using AutoMapper;
    using Base.Common.Interfaces;
    using Base.Services;
    using Common.Interfaces;
    using Common.Models;
    using Common.Responses;
    using Domain.HospitalData.HsGuarantor.Entities;
    using Domain.HospitalData.HsGuarantor.Interfaces;
    using Domain.HospitalData.VpGuarantor.Entities;
    using Ivh.Common.VisitPay.Enums;
    using Ivh.Common.VisitPay.Messages.HospitalData.Dtos;

    public class VpGuarantorHsMatchApiApplicationService : ApplicationService, IVpGuarantorHsMatchApiApplicationService
    {
        private readonly Lazy<IVpGuarantorHsMatchRepository> _vpHsMatchRepository;

        public VpGuarantorHsMatchApiApplicationService(
            Lazy<IApplicationServiceCommonService> applicationServiceCommonService,
            Lazy<IVpGuarantorHsMatchRepository> vpHsMatchRepository) : base(applicationServiceCommonService)
        {
            this._vpHsMatchRepository = vpHsMatchRepository;
        }

        public IList<VpGuarantorHsMatchDto> GetById(VpGuarantorHsMatchApiGetByIdModel model)
        {
            return Mapper.Map<IList<VpGuarantorHsMatchDto>>(this._vpHsMatchRepository.Value.GetById(model.hsGuarantorId, model.vpGuarantorId, model.hsGuarantorMatchStatuses));
        }

        public void InsertOrUpdate(VpGuarantorHsMatchDto dto)
        {
            VpGuarantorHsMatch connectedEntity = this._vpHsMatchRepository.Value.GetById(dto.HsGuarantorId, dto.VpGuarantorId).FirstOrDefault();
            connectedEntity = Mapper.Map(dto, connectedEntity);
            this._vpHsMatchRepository.Value.InsertOrUpdate(connectedEntity);
        }
        public VpGuarantorSsnResult GetSsn(int vpGuarantorId)
        {
            VpGuarantorSsnResult  result = new VpGuarantorSsnResult() {Result = ResultEnum.NotFound, VpGuarantorId = vpGuarantorId};
            IList<HsGuarantor> results = this._vpHsMatchRepository.Value.GetGuarantorsByVpGuarantorId(vpGuarantorId, true);
            List<string> ssns = results.Select(x => x.SSN).Distinct().ToList();
            if (ssns.Count > 1)
            {
                result.Result = ResultEnum.MoreThanOneFound;
            }
            if (ssns.Count == 1)
            {
                result.Result = ResultEnum.Found;
                result.Ssn = ssns.FirstOrDefault();
            }
            return result;
        }
    }
}
﻿namespace Ivh.Application.HospitalData
{
    using System;
    using System.Collections.Generic;
    using System.Reflection;
    using AutoMapper;
    using Base.Common.Interfaces;
    using Base.Services;
    using Common.Interfaces;
    using Domain.Guarantor.Entities;
    using Domain.Guarantor.Interfaces;
    using Domain.HospitalData.Scoring.Entities;
    using Domain.HospitalData.Scoring.Enums;
    using Domain.HospitalData.Scoring.Interfaces;
    using Domain.Logging.Interfaces;
    using Ivh.Common.Base.Utilities.Extensions;
    using Ivh.Common.Data;
    using Ivh.Common.Data.Connection.Connections;
    using Ivh.Common.Data.Interfaces;
    using Ivh.Common.ServiceBus.Attributes;
    using Ivh.Common.ServiceBus.Enums;
    using Ivh.Common.VisitPay.Attributes;
    using Ivh.Common.VisitPay.Enums;
    using Ivh.Common.VisitPay.Messages.HospitalData.Dtos;
    using Ivh.Common.VisitPay.Messages.Scoring;
    using MassTransit;
    using NHibernate;

    public class ScoringApplicationService : ApplicationService, IScoringApplicationService
    {
        private readonly Lazy<IMetricsProvider> _metricsProvider;
        private readonly Lazy<IScoringStatisticalMonitoringService> _scoreMedianMeanService;
        private readonly Lazy<IScoringMonitoringThresholdsService> _scoringMonitoringThresholdsService;
        private readonly Lazy<IScoringService> _scoringService;
        private readonly Lazy<IVpGuarantorScoreRepository> _vpGuarantorScoreRepository;
        private readonly ISession _session;

        public ScoringApplicationService(
            Lazy<IApplicationServiceCommonService> applicationServiceCommonService,
            Lazy<IMetricsProvider> metricsProvider,
            Lazy<IScoringStatisticalMonitoringService> scoreMedianMeanService,
            Lazy<IScoringMonitoringThresholdsService> scoringMonitoringThresholdsService,
            Lazy<IScoringService> scoringService,
            Lazy<IVpGuarantorScoreRepository> vpGuarantorScoreRepository,
            ISessionContext<CdiEtl> sessionContext
        ) : base(applicationServiceCommonService)
        {
            this._metricsProvider = metricsProvider;
            this._scoreMedianMeanService = scoreMedianMeanService;
            this._scoringMonitoringThresholdsService = scoringMonitoringThresholdsService;
            this._scoringService = scoringService;
            this._vpGuarantorScoreRepository = vpGuarantorScoreRepository;
            this._session = sessionContext.Session;
        }

        public void ValidateDailyScoringBatchInputVariables()
        {
            using (UnitOfWork unitOfWork = new UnitOfWork(this._session))
            {
                ScoringMonitoringThresholds scoringMonitoringThresholds = this._scoringMonitoringThresholdsService.Value.GetDailyScoringMonitoringThresholds();
                ScoringStatisticalMonitoring scoringStatisticalMonitoring = this._scoreMedianMeanService.Value.RunDailyScoringStatisticalMonitoring(scoringMonitoringThresholds.MinimumBatchSize);

                this.MonitorScoringInputs(Mapper.Map<ScoringStatisticalMonitoringDto>(scoringStatisticalMonitoring),
                    Mapper.Map<ScoringMonitoringThresholdsDto>(scoringMonitoringThresholds),
                    ScoringMonitoringTypeEnum.Daily);

                unitOfWork.Commit();
            }
        }

        public void ValidateWeeklyScoringBatchInputVariables()
        {
            using (UnitOfWork unitOfWork = new UnitOfWork(this._session))
            {
                ScoringMonitoringThresholds scoringMonitoringThresholds = this._scoringMonitoringThresholdsService.Value.GetWeeklyScoringMonitoringThresholds();
                ScoringStatisticalMonitoring scoringStatisticalMonitoring = this._scoreMedianMeanService.Value.RunWeeklyScoringStatisticalMonitoring();

                this.MonitorScoringInputs(Mapper.Map<ScoringStatisticalMonitoringDto>(scoringStatisticalMonitoring),
                    Mapper.Map<ScoringMonitoringThresholdsDto>(scoringMonitoringThresholds),
                    ScoringMonitoringTypeEnum.Weekly);

                unitOfWork.Commit();
            }
        }

        #region Message Consumers

        [UniversalConsumer(UniversalQueuesEnum.VisitPayProcessor, UniversalPriorityEnum.Priority30)]
        public void ConsumeMessage(VpGuarantorScoreMessage message, ConsumeContext<VpGuarantorScoreMessage> consumeContext)
        {
            IList<PtpScore> ptpScores = this._scoringService.Value.GetGuarantorPtpScore(consumeContext.Message.HsGuarantors);
            if (ptpScores.Count <= 0)
            {
                return;
            }
            foreach (PtpScore ptpScore in ptpScores)
            {
                VpGuarantorScore vpGuarantorScore = new VpGuarantorScore
                {
                    VpGuarantorId = consumeContext.Message.VpGuarantorId,
                    Score = ptpScore.Score,
                    ScoreTypeId = ptpScore.ScoreTypeId,
                    ScoreCreationDate = DateTime.UtcNow
                };
                this._vpGuarantorScoreRepository.Value.InsertOrUpdate(vpGuarantorScore);
            }
        }

        public bool IsMessageValid(VpGuarantorScoreMessage message, ConsumeContext<VpGuarantorScoreMessage> consumeContext)
        {
            bool isValid = true;
            return isValid;
        }

        #endregion

        private void MonitorScoringInputs(ScoringStatisticalMonitoringDto scoringStatisticalMonitoringDto, ScoringMonitoringThresholdsDto scoringMonitoringThresholdsDto, ScoringMonitoringTypeEnum scoringMonitoringTypeEnum)
        {
            PropertyInfo[] properties = typeof(ScoringStatisticalMonitoringDto).GetProperties(BindingFlags.Public | BindingFlags.Instance);
            this._metricsProvider.Value.Increment($"ScoringMonitor.BatchSize.{scoringMonitoringTypeEnum}", scoringStatisticalMonitoringDto.ScoreCount);

            foreach (PropertyInfo property in properties)
            {
                ScoringStatisticalMonitoringMetricAttribute attribute = property.GetCustomAttribute<ScoringStatisticalMonitoringMetricAttribute>();
                if (attribute != null)
                {
                    bool isACount = Convert.ToBoolean(attribute.IsACount);
                    string metricName = $"{attribute.MetricName}.{scoringMonitoringTypeEnum}";
                    string metricOutsidePercentRange = $"{attribute.MetricName}.OutsidePercentRange.{scoringMonitoringTypeEnum}";

                    decimal actualValue = Convert.ToDecimal(property.GetValue(scoringStatisticalMonitoringDto, null));

                    PropertyInfo minProperty = typeof(ScoringMonitoringThresholdsDto).GetProperty(attribute.MinValue);
                    object minObject = minProperty?.GetValue(scoringMonitoringThresholdsDto, null);
                    decimal? minValue = minObject == null ? (decimal?)null : Convert.ToDecimal(minObject);

                    PropertyInfo maxProperty = typeof(ScoringMonitoringThresholdsDto).GetProperty(attribute.MaxValue);
                    object maxObject = maxProperty?.GetValue(scoringMonitoringThresholdsDto, null);
                    decimal? maxValue = maxObject == null ? (decimal?)null : Convert.ToDecimal(maxObject);

                    // if isACount the actualValue is a count, but the min/max values are percentages
                    if (isACount)
                    {
                        this._metricsProvider.Value.Increment(metricName, Convert.ToInt32(actualValue));

                        if (minValue != null && maxValue != null && scoringStatisticalMonitoringDto.ScoreCount > 0)
                        {
                            decimal percent = actualValue / scoringStatisticalMonitoringDto.ScoreCount * 100;
                            if (!percent.IsBetween((decimal)minValue, (decimal)maxValue))
                            {
                                this._metricsProvider.Value.Increment(metricOutsidePercentRange);
                            }
                        }
                    }
                    // otherwise it's just a Mean or Median range check
                    else
                    {
                        if (minValue != null && maxValue != null &&
                            !actualValue.IsBetween((decimal)minValue, (decimal)maxValue))
                        {
                            this._metricsProvider.Value.Increment(metricName);
                        }
                    }
                }
            }
        }
    }
}

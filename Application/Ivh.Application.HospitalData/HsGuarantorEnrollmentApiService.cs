﻿namespace Ivh.Application.HospitalData
{
    using System;
    using System.Collections.Generic;
    using Base.Common.Interfaces;
    using Base.Services;
    using Common.Interfaces;
    using Domain.HospitalData.HsGuarantor.Interfaces;

    public class HsGuarantorEnrollmentApiService : ApplicationService, IHsGuarantorEnrollmentApiService
    {
        private readonly Lazy<IHsGuarantorService> _hsGuarantorService;

        public HsGuarantorEnrollmentApiService(
            Lazy<IApplicationServiceCommonService> applicationServiceCommonService,
            Lazy<IHsGuarantorService> hsGuarantorService) : base(applicationServiceCommonService)
        {
            this._hsGuarantorService = hsGuarantorService;
        }
        
        public IList<string> GetVisitsAfterEnrollment(string hsGuarantorSourceSystemKey, int billingSystem)
        {
            return this._hsGuarantorService.Value.GetEligibleVisitsAfterEnrollment(hsGuarantorSourceSystemKey, billingSystem);
        }
    }
}
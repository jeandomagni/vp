﻿namespace Ivh.Application.HospitalData
{
    using System;
    using System.Collections.Generic;
    using Common.Interfaces;
    using Autofac;
    using Domain.Logging.Interfaces;
    using Ivh.Common.DependencyInjection;
    using Ivh.Common.VisitPay.Constants;
    using Ivh.Common.VisitPay.Messages.HospitalData.Dtos;

    public class SegmentationBadDebtService : SegmentationBase<SegmentationBadDebtDto>, ISegmentationBadDebtService
    {
        public SegmentationBadDebtService(Lazy<ILoggingService> loggingService, Lazy<IMetricsProvider> metricsProvider) : base(loggingService, metricsProvider)
        {
            this.SuccessStatName = Metrics.Increment.Scoring.SegmentationBadDebtSuccesses;
            this.FailureStatName = Metrics.Increment.Scoring.SegmentationBadDebtFailures;
        }

        protected override void ProcessSegmentation(List<SegmentationBadDebtDto> segmentationDtoList)
        {
            using (ILifetimeScope lifetimeScope = IvinciContainer.Instance.Container().BeginLifetimeScope())
            {
                IHsSegmentationApplicationService hsSegmentationApplicationService = lifetimeScope.Resolve<IHsSegmentationApplicationService>();
                IList<SegmentationBadDebtDto> badDebtSegmentationDtos = hsSegmentationApplicationService.ProcessBadDebtSegmentation(segmentationDtoList);
                hsSegmentationApplicationService.SaveBadDebtSegmentationResult(badDebtSegmentationDtos);
            }
        }

        protected override void UpdateSegmentationHistory()
        {
            using (ILifetimeScope lifetimeScope = IvinciContainer.Instance.Container().BeginLifetimeScope())
            {
                IHsSegmentationApplicationService hsSegmentationApplicationService = lifetimeScope.Resolve<IHsSegmentationApplicationService>();
                hsSegmentationApplicationService.UpdateBadDebtGuarantorSegmentationHistory(this.SegmentationBatchId);
            }
        }

        public bool ProcessBadDebtSegmentation(List<SegmentationBadDebtDto> segmentationList, int scoringBatchId, string currentState, string eventName, Guid correlationId, Guid processId)
        {
            return this.ProcessSegmentationBatch(segmentationList, scoringBatchId, currentState, eventName, correlationId, processId);
        }
    }
}

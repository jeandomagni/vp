﻿namespace Ivh.Application.HospitalData
{
    using System;
    using System.Collections.Generic;
    using Common.Interfaces;
    using Autofac;
    using Domain.Logging.Interfaces;
    using Ivh.Common.DependencyInjection;
    using Ivh.Common.VisitPay.Constants;
    using Ivh.Common.VisitPay.Messages.HospitalData.Dtos;

    public class SegmentationActivePassiveService : SegmentationBase<SegmentationActivePassiveDto>, ISegmentationActivePassiveService
    {
        public SegmentationActivePassiveService(Lazy<ILoggingService> loggingService, Lazy<IMetricsProvider> metricsProvider) : base(loggingService, metricsProvider)
        {
            this.SuccessStatName = Metrics.Increment.Scoring.SegmentationActivePassiveSuccesses;
            this.FailureStatName = Metrics.Increment.Scoring.SegmentationActivePassiveFailures;
        }

        protected override void ProcessSegmentation(List<SegmentationActivePassiveDto> segmentationDtoList)
        {
            using (ILifetimeScope lifetimeScope = IvinciContainer.Instance.Container().BeginLifetimeScope())
            {
                IHsSegmentationApplicationService hsSegmentationApplicationService = lifetimeScope.Resolve<IHsSegmentationApplicationService>();
                IList<SegmentationActivePassiveDto> activePassiveSegmentationDtos = hsSegmentationApplicationService.ProcessActivePassiveSegmentation(segmentationDtoList);
                hsSegmentationApplicationService.SaveActivePassiveSegmentation(activePassiveSegmentationDtos);
            }
        }

        protected override void UpdateSegmentationHistory()
        {
            using (ILifetimeScope lifetimeScope = IvinciContainer.Instance.Container().BeginLifetimeScope())
            {
                IHsSegmentationApplicationService hsSegmentationApplicationService = lifetimeScope.Resolve<IHsSegmentationApplicationService>();
                hsSegmentationApplicationService.UpdateActivePassiveGuarantorSegmentationHistory(this.SegmentationBatchId);
            }
        }

        public bool ProcessActivePassiveSegmentation(List<SegmentationActivePassiveDto> segmentationList, int scoringBatchId, string currentState, string eventName, Guid correlationId, Guid processId)
        {
            return this.ProcessSegmentationBatch(segmentationList, scoringBatchId, currentState, eventName, correlationId, processId);
        }
    }
}

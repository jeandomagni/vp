﻿namespace Ivh.Application.HospitalData
{
    using System;
    using Base.Common.Interfaces;
    using Base.Services;
    using Common.Interfaces;
    using Domain.HospitalData.Visit.Interfaces;
    using Ivh.Common.ServiceBus.Attributes;
    using Ivh.Common.ServiceBus.Enums;
    using Ivh.Common.VisitPay.Messages.HospitalData;
    using MassTransit;

    public class RealTimeVisitEnrollmentApplicationService : ApplicationService, IRealTimeVisitEnrollmentApplicationService
    {
        private readonly Lazy<IRealTimeVisitEnrollmentService> _realTimeVisitEnrollmentService;

        public RealTimeVisitEnrollmentApplicationService(
            Lazy<IApplicationServiceCommonService> applicationServiceCommonService,
            Lazy<IRealTimeVisitEnrollmentService> realTimeVisitEnrollmentService)
            : base(applicationServiceCommonService)
        {
            this._realTimeVisitEnrollmentService = realTimeVisitEnrollmentService;
        }


        [UniversalConsumer(UniversalQueuesEnum.VisitPayProcessor, UniversalPriorityEnum.Priority20)]
        public void ConsumeMessage(RealTimeVisitEnrollmentMessage realTimeVisitEnrollmentMessage, ConsumeContext<RealTimeVisitEnrollmentMessage> consumeContext)
        {
            this._realTimeVisitEnrollmentService.Value.ProcessRealTimeVisitEnrollmentMessage(realTimeVisitEnrollmentMessage);
        }

        public bool IsMessageValid(RealTimeVisitEnrollmentMessage message, ConsumeContext<RealTimeVisitEnrollmentMessage> consumeContext)
        {
            return true;
        }
    }
}
﻿namespace Ivh.Application.HospitalData
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using AutoMapper;
    using Base.Common.Interfaces;
    using Base.Services;
    using Common.Interfaces;
    using Domain.HospitalData.Change.Entities;
    using Domain.HospitalData.Change.Interfaces;
    using Domain.HospitalData.HsGuarantor.Entities;
    using Domain.HospitalData.Visit.Entities;
    using Domain.HospitalData.Visit.Interfaces;
    using Ivh.Common.Base.Utilities.Extensions;
    using Ivh.Common.JobRunner.Common.Interfaces;
    using Ivh.Common.ServiceBus.Attributes;
    using Ivh.Common.ServiceBus.Enums;
    using Ivh.Common.VisitPay.Enums;
    using Ivh.Common.VisitPay.Jobs;
    using Ivh.Common.VisitPay.Messages.HospitalData;
    using Ivh.Common.VisitPay.Messages.HospitalData.Dtos;
    using Ivh.Common.VisitPay.Messages.Matching;
    using Ivh.Common.VisitPay.Messages.Matching.Dtos;
    using MassTransit;

    public class ChangeEventApplicationService : ApplicationService, IChangeEventApplicationService
    {
        private readonly Lazy<IChangeEventService> _changeEventService;
        private readonly Lazy<IVisitService> _visitService;
        private readonly Lazy<IVisitTransactionService> _visitTransactionService;

        public ChangeEventApplicationService(
            Lazy<IApplicationServiceCommonService> applicationServiceCommonService,
            Lazy<IChangeEventService> changeEventService,
            Lazy<IVisitService> visitService,
            Lazy<IVisitTransactionService> visitTransactionService)
            : base(applicationServiceCommonService)
        {
            this._changeEventService = changeEventService;
            this._visitService = visitService;
            this._visitTransactionService = visitTransactionService;
        }

        public void ProcessChangeSetEvents(ChangeSetEventsMessage message)
        {
            this._changeEventService.Value.ProcessChangeSetEvents(message);
        }

        public void ProcessChangeSetEventsAggregatedMessage(ChangeSetEventsAggregatedMessage changeSetEventsAggregatedMessage)
        {
            this._changeEventService.Value.ProcessChangeSetEventsAggregatedMessage(changeSetEventsAggregatedMessage);
        }

        public void ChangeSetProcessedAcknowledgement(ChangeEventProcessedMessage message)
        {
            this._changeEventService.Value.ChangeEventProcessedAcknowledgement(message);
        }

        public void QueueAllUnprocessedChangeEvents()
        {
            this._changeEventService.Value.QueueAllUnprocessedChangeEvents();
        }

        public void GenerateChangeEventForHsGuarantor(int vpGuarantorId, int hsGuarantorId, IList<EnrollmentResponseDto> response)
        {
            this._changeEventService.Value.GenerateChangeEventForHsGuarantor(vpGuarantorId, hsGuarantorId, response);
        }
        
        public void GenerateChangeEventForGuarantor(int vpGuarantorId, IList<int> hsGuarantorIds)
        {
            this._changeEventService.Value.GenerateDataChangeEventsForGuarantor(vpGuarantorId, hsGuarantorIds);
        }

        private void GenerateHsGuarantorMatchDiscrepancyChangeEventForGuarantors(IEnumerable<GuarantorMatchResult> matchResults)
        {
            foreach (GuarantorMatchResult matchResult in matchResults.Where(x => x.VpGuarantorId.HasValue))
            {
                this._changeEventService.Value.GenerateHsGuarantorMatchDiscrepancyChangeEventsForGuarantor(
                    matchResult.VpGuarantorId.Value,
                    matchResult.MatchedHsGuarantorSourceSystemKey,
                    matchResult.MatchedHsGuarantorBillingSystemId);
            }
        }

        #region Message Consumers
        [UniversalConsumer(UniversalQueuesEnum.HsDataProcessor, UniversalPriorityEnum.Priority10)]
        public void ConsumeMessage(GenerateChangeEventForVpGuarantorMessage message, ConsumeContext<GenerateChangeEventForVpGuarantorMessage> consumeContext)
        {
            this._changeEventService.Value.GenerateChangeEventForVpGuarantor(message.VpGuarantorId, message.EnrollmentResponses);
        }

        public bool IsMessageValid(GenerateChangeEventForVpGuarantorMessage message, ConsumeContext<GenerateChangeEventForVpGuarantorMessage> consumeContext)
        {
            return message?.VpGuarantorId > 0 && message.EnrollmentResponses.Any();
        }


        [UniversalConsumer(UniversalQueuesEnum.HsDataProcessor, UniversalPriorityEnum.Priority10)]
        public void ConsumeMessage(ChangeEventProcessedMessage message, ConsumeContext<ChangeEventProcessedMessage> consumeContext)
        {
            this.ChangeSetProcessedAcknowledgement(message);
        }

        public bool IsMessageValid(ChangeEventProcessedMessage message, ConsumeContext<ChangeEventProcessedMessage> consumeContext)
        {
            bool isValid = true;
            return isValid;
        }

        [UniversalConsumer(UniversalQueuesEnum.HsDataProcessor, UniversalPriorityEnum.Priority20)]
        public void ConsumeMessage(ChangeSetEventsAggregatedMessage message, ConsumeContext<ChangeSetEventsAggregatedMessage> consumeContext)
        {
            this.ProcessChangeSetEventsAggregatedMessage(message);
        }

        public bool IsMessageValid(ChangeSetEventsAggregatedMessage message, ConsumeContext<ChangeSetEventsAggregatedMessage> consumeContext)
        {
            bool isValid = true;
            return isValid;
        }

        [UniversalConsumer(UniversalQueuesEnum.HsDataProcessor, UniversalPriorityEnum.Priority20)]
        public void ConsumeMessage(ChangeSetEventsMessage message, ConsumeContext<ChangeSetEventsMessage> consumeContext)
        {
            this.ProcessChangeSetEvents(message);
        }

        public bool IsMessageValid(ChangeSetEventsMessage message, ConsumeContext<ChangeSetEventsMessage> consumeContext)
        {
            bool isValid = true;
            return isValid;
        }

        [UniversalConsumer(UniversalQueuesEnum.HsDataProcessor, UniversalPriorityEnum.Priority20)]
        public void ConsumeMessage(HsGuarantorMatchDiscrepancyMessage message, ConsumeContext<HsGuarantorMatchDiscrepancyMessage> consumeContext)
        {
            if (message.MatchResult.VpGuarantorId.HasValue)
            {
                this.GenerateHsGuarantorMatchDiscrepancyChangeEventForGuarantors(message.MatchResult.ToListOfOne());
            }
        }

        public bool IsMessageValid(HsGuarantorMatchDiscrepancyMessage message, ConsumeContext<HsGuarantorMatchDiscrepancyMessage> consumeContext)
        {
            bool isValid = this.FeatureService.Value.IsFeatureEnabled(VisitPayFeatureEnum.FeatureMatchingApiIsEnabled);
            return isValid;
        }

        [UniversalConsumer(UniversalQueuesEnum.HsDataProcessor, UniversalPriorityEnum.Priority20)]
        public void ConsumeMessage(HsGuarantorMatchDiscrepancyMessageList message, ConsumeContext<HsGuarantorMatchDiscrepancyMessageList> consumeContext)
        {
            this.GenerateHsGuarantorMatchDiscrepancyChangeEventForGuarantors(message.Messages.Select(x => x.MatchResult));
        }

        public bool IsMessageValid(HsGuarantorMatchDiscrepancyMessageList message, ConsumeContext<HsGuarantorMatchDiscrepancyMessageList> consumeContext)
        {
            bool isValid = this.FeatureService.Value.IsFeatureEnabled(VisitPayFeatureEnum.FeatureMatchingApiIsEnabled);
            return isValid;
        }

        #endregion

        #region Jobs

        void IJobRunnerService<QueueVisitChangeSetJobRunner>.Execute(DateTime begin, DateTime end, string[] args)
        {
            this.QueueAllUnprocessedChangeEvents();
        }

        #endregion

        public void SaveChangeEvent(ChangeEventDto changeEventDto)
        {
            this._changeEventService.Value.SaveChangeEvent(Mapper.Map<ChangeEvent>(changeEventDto));
        }

        public void SaveChangeEvent(ChangeEventDto changeEventDto, HsGuarantorChangeDto hsGuarantorChangeDto)
        {
            this._changeEventService.Value.SaveChangeEvent(Mapper.Map<ChangeEvent>(changeEventDto), Mapper.Map<HsGuarantorChange>(hsGuarantorChangeDto));
        }

        public void SaveChangeEvent(ChangeEventDto changeEventDto, VisitChangeDto visitChangeDto)
        {
            this._changeEventService.Value.SaveChangeEvent(Mapper.Map<ChangeEvent>(changeEventDto), Mapper.Map<VisitChange>(visitChangeDto));
        }

        public void SaveChangeEvents(ChangeSetDto changeSet, ChangeSetTypeEnum changeSetType)
        {
            this._changeEventService.Value.SaveChangeEvents(Mapper.Map<ChangeSet>(changeSet), changeSetType);
        }

        public IList<LifeCycleStageDto> GetLifeCycleStages()
        {
            return Mapper.Map<IList<LifeCycleStageDto>>(this._visitService.Value.GetLifeCycleStages());
        }

        public IList<InsurancePlanDto> GetInsurancePlans()
        {
            return Mapper.Map<IList<InsurancePlanDto>>(this._visitService.Value.GetInsurancePlans());
        }


        public IList<RevenueWorkCompanyDto> GetRevenueWorkCompanies()
        {
            return Mapper.Map<IList<RevenueWorkCompanyDto>>(this._visitService.Value.GetRevenueWorkCompanies());
        }

        public IList<FacilityDto> GetFacilities()
        {
            return Mapper.Map<IList<FacilityDto>>(this._visitService.Value.GetFacilities());
        }

        public IList<PrimaryInsuranceTypeDto> GetPrimaryInsuranceTypes()
        {
            return Mapper.Map<IList<PrimaryInsuranceTypeDto>>(this._visitService.Value.GetPrimaryInsuranceTypes());
        }

        public IReadOnlyList<VpTransactionTypeDto> GetVisitTransactionTypes()
        {
            return Mapper.Map<IReadOnlyList<VpTransactionTypeDto>>(this._visitTransactionService.Value.GetTransactionTypes());
        }

        public IReadOnlyList<TransactionCodeDto> GetVisitTransactionCodes()
        {
            return Mapper.Map<IReadOnlyList<TransactionCodeDto>>(this._visitTransactionService.Value.GetTransactionCodes());
        }

        public IList<PatientTypeDto> GetPatientTypes()
        {
            return Mapper.Map<IList<PatientTypeDto>>(this._visitService.Value.GetPatientTypes());
        }
        public IReadOnlyList<HsVisitDto> GetVisitsForGuarantor(int hsGuarantorId)
        {
            return Mapper.Map<IReadOnlyList<HsVisitDto>>(this._visitService.Value.GetVisitsForGuarantor(hsGuarantorId));
        }


    }
}
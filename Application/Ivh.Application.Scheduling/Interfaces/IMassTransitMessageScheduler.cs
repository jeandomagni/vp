﻿namespace Ivh.Application.Scheduling.Interfaces
{
    using MassTransit;
    using MassTransit.Scheduling;

    public interface IMassTransitMessageScheduler
    {
        void ScheduleMessage(ConsumeContext<ScheduleMessage> context);
        void ScheduleRecurringMessage(ConsumeContext<ScheduleRecurringMessage> context);
    }
}
﻿namespace Ivh.Application.Scheduling.Services
{
    using System;
    using System.Threading.Tasks;
    using Common.Interfaces;
    using Interfaces;
    using MassTransit;
    using MassTransit.Scheduling;

    public class SchedulingApplicationService : ISchedulingApplicationService
    {
        private readonly Lazy<IMassTransitMessageScheduler> _massTransitMessageScheduler;

        public SchedulingApplicationService(Lazy<IMassTransitMessageScheduler> massTransitMessageScheduler)
        {
            this._massTransitMessageScheduler = massTransitMessageScheduler;
        }

        public void ScheduleMessage(ConsumeContext<ScheduleMessage> context)
        {
            this._massTransitMessageScheduler.Value.ScheduleMessage(context);
        }

        public void ScheduleRecurringMessage(ConsumeContext<ScheduleRecurringMessage> context)
        {
            this._massTransitMessageScheduler.Value.ScheduleRecurringMessage(context);
        }
    }
}

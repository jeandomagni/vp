﻿namespace Ivh.Application.HospitalData.Common.Models
{
    using System;

    public class CredentialsMatchCanceledAccountRequestModel
    {
        public string lastName { get; set; }
        public DateTime dob { get; set; }
        public string ssn4 { get; set; }
        public string postalCodeShort { get; set; }
    }
}
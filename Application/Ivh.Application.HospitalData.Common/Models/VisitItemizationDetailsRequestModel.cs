﻿namespace Ivh.Application.HospitalData.Common.Models
{
    using System;

    public class VisitItemizationDetailsRequestModel
    {
        public int BillingSystemId { get; set; }
        public string HsGuarantorSourceSystemKey { get; set; }
        public string VisitSourceSystemKey { get; set; }
    }
}
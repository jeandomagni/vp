﻿namespace Ivh.Application.HospitalData.Common.Models
{
    using System;
    using ProtoBuf;
    using Ivh.Common.Base.Utilities.Helpers;

    [ProtoContract]
    public class VisitItemizationDetailsResultModel
    {
        [ProtoMember(1)]
        public int BillingSystemId { get; set; }

        [ProtoMember(2)]
        public int VpGuarantorId { get; set; }

        [ProtoMember(3)]
        public int HsGuarantorId { get; set; }

        [ProtoMember(4)]
        public string VisitSourceSystemKeyDisplay { get; set; }

        [ProtoMember(5)]
        public string PatientFirstName { get; set; }

        [ProtoMember(6)]
        public string PatientLastName { get; set; }

        [ProtoMember(7)]
        public DateTime? VisitAdmitDate { get; set; }

        [ProtoMember(8)]
        public DateTime? VisitDischargeDate { get; set; }

        [ProtoMember(9)]
        public bool? IsPatientGuarantor { get; set; }

        [ProtoMember(10)]
        public bool? IsPatientMinor { get; set; }

        [ProtoIgnore]
        public string PatientLastNameFirstName => FormatHelper.LastNameFirstName(this.PatientLastName, this.PatientFirstName);

        [ProtoIgnore]
        public string PatientFirstNameLastName => FormatHelper.FirstNameLastName(this.PatientFirstName, this.PatientLastName);
    }
}
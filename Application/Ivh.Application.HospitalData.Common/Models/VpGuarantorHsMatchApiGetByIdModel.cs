﻿namespace Ivh.Application.HospitalData.Common.Models
{
    using System.Collections.Generic;
    using Ivh.Common.Base.Enums;
    using Ivh.Common.VisitPay.Enums;

    public class VpGuarantorHsMatchApiGetByIdModel
    {
        public int hsGuarantorId { get; set; } 
        public int vpGuarantorId { get; set; }
        public IList<HsGuarantorMatchStatusEnum> hsGuarantorMatchStatuses { get; set; }
    }
}
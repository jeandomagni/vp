﻿namespace Ivh.Application.HospitalData.Common.Models
{
    using Ivh.Common.Base.Utilities.Helpers;
    using System;

    public class HsGuarantorModel
    {
        public virtual int HsGuarantorId { get; set; }
        public virtual int HsBillingSystemId { get; set; }
        public virtual string SourceSystemKey { get; set; }
        public virtual string FirstName { get; set; }
        public virtual string LastName { get; set; }
        public virtual DateTime? DOB { get; set; }
        public virtual string SSN4 { get; set; }
        public virtual string AddressLine1 { get; set; }
        public virtual string AddressLine2 { get; set; }
        public virtual string City { get; set; }
        public virtual string StateProvince { get; set; }
        public virtual string PostalCode { get; set; }
        public virtual string Country { get; set; }
        public virtual string LastNameFirstName => FormatHelper.LastNameFirstName(this.LastName, this.FirstName);
        public virtual string FirstNameLastName => FormatHelper.FirstNameLastName(this.FirstName, this.LastName);
    }
}
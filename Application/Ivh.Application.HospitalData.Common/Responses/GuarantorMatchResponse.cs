﻿namespace Ivh.Application.HospitalData.Common.Responses
{
    using Ivh.Common.VisitPay.Enums;

    public class GuarantorMatchResponse
    {
        public int? MatchedHsGuarantorId { get; set; }
        public GuarantorMatchResultEnum GuarantorMatchResultEnum { get; set; }
    }
}
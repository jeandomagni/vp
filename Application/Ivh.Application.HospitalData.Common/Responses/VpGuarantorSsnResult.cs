﻿namespace Ivh.Application.HospitalData.Common.Responses
{
    using Ivh.Common.Base.Enums;
    using Ivh.Common.VisitPay.Enums;

    public class VpGuarantorSsnResult
    {
        public ResultEnum Result { get; set; }
        public string Ssn { get; set; }
        public int VpGuarantorId { get; set; }
    }
}
﻿namespace Ivh.Application.HospitalData.Common.Interfaces
{
    using System.Collections.Generic;
    using Ivh.Common.Base.Interfaces;
    using Ivh.Common.ServiceBus.Interfaces;
    using Ivh.Common.VisitPay.Messages.HospitalData;
    using Ivh.Common.VisitPay.Messages.HospitalData.Dtos;

    public interface IVpGuarantorHsMatchApplicationService : IApplicationService,
        IUniversalConsumerService<EtlHsGuarantorCancelMessage>,
        IUniversalConsumerService<EtlHsGuarantorReactivateMessage>,
        IUniversalConsumerService<VpccGuarantorEnrolledInVisitPayMessage>
    {
        IReadOnlyList<VpGuarantorHsMatchDto> GetVpGuarantorHsMatches(int hsGuarantorId);
        void ProcessHsGuarantorCancelMessage(EtlHsGuarantorCancelMessage message);
        void ProcessHsGuarantorReactivateMessage(EtlHsGuarantorReactivateMessage message);
        IList<VpGuarantorHsMatchDto> GetVpGuarantorHsMatches(int matchedHsGuarantorId, int vpGuarantorId);
    }
}

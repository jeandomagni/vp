﻿namespace Ivh.Application.HospitalData.Common.Interfaces
{
    using Ivh.Common.Base.Interfaces;
    using Ivh.Common.VisitPay.Messages.HospitalData.Dtos;
    using Responses;

    /// <summary>
    /// This is meant to mostly be a pass through to the repo.  We replaced a repository(In the patient/client/etc) with a web service client and this is the application service that services the webservice to feed the pseudo repository
    /// </summary>
    public interface IVpGuarantorMatchInfoApplicationService : IApplicationService
    {
        GuarantorMatchResponse GetInitialMatchExpress(VpGuarantorMatchInfoDto vpGuarantorMatchInfo);
        GuarantorMatchResponse GetInitialMatch(VpGuarantorMatchInfoDto vpGuarantorMatchInfo);
        void AddInitialMatch(VpGuarantorMatchInfoDto vpGuarantorMatchInfo);
        void AddInitialMatchVpcc(VpGuarantorMatchInfoDto vpGuarantorMatchInfo);
        void UpdateMatch(VpGuarantorMatchInfoDto vpGuarantorMatchInfo);
        void GetNewMatches(int vpGuarantorId);
    }
}
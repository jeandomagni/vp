﻿namespace Ivh.Application.HospitalData.Common.Interfaces
{
    using Ivh.Common.Base.Interfaces;
    using Ivh.Common.ServiceBus.Interfaces;
    using Ivh.Common.VisitPay.Messages.Eob;
    using MassTransit;

    public interface IEob835ApplicationService : IApplicationService,
    IUniversalConsumerService<EobInterchangeMigrationMessage>
    {
    }

}
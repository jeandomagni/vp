﻿namespace Ivh.Application.HospitalData.Common.Interfaces
{
    using Ivh.Common.Base.Interfaces;
    using Ivh.Common.ServiceBus.Interfaces;
    using Ivh.Common.VisitPay.Messages.HospitalData;

    public interface IHsDataInboundApplicationService : IApplicationService, IUniversalConsumerService<GuestPayDataSyncMessage>
    {
        void QueueSyncGuestPayData();
    }
}

﻿namespace Ivh.Application.HospitalData.Common.Interfaces
{
    using System.Collections.Generic;
    using Ivh.Common.Base.Interfaces;
    using Ivh.Common.ServiceBus.Interfaces;
    using Ivh.Common.VisitPay.Messages.HospitalData;

    public interface IRedactionApplicationService : IApplicationService,
        IUniversalConsumerService<HsGuarantorRematchMessage>,
        IUniversalConsumerService<HsGuarantorUnmatchMessage>,
        IUniversalConsumerService<HsGuarantorVisitUnmatchMessage>
    {
        void ProcessHsGuarantorUnmatchMessage(HsGuarantorUnmatchMessage hsGuarantorUnmatchMessage);
        void ProcessHsGuarantorRematchMessage(HsGuarantorRematchMessage hsGuarantorRematchMessage);
        void ProcessHsGuarantorVisitUnmatchMessage(HsGuarantorVisitUnmatchMessage hsGuarantorVisitUnmatchMessage);
        void ProcessHsGuarantorVisitUnmatchMessages(IList<HsGuarantorVisitUnmatchMessage> hsGuarantorVisitUnmatchMessages);
    }
}
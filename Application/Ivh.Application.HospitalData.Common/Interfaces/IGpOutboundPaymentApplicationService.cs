﻿namespace Ivh.Application.HospitalData.Common.Interfaces
{
    using Ivh.Common.Base.Interfaces;
    using Ivh.Common.ServiceBus.Interfaces;
    using Ivh.Common.VisitPay.Messages.HospitalData;

    public interface IGpOutboundPaymentApplicationService : IApplicationService,
        IUniversalConsumerService<GuestPayPaymentMessage>,
        IUniversalConsumerService<GuestPayPaymentVoidMessage>
    {
        void CreateOutboundGuestPayPayment(GuestPayPaymentMessage guestPayPaymentMessage);
        void VoidOutboundGuestPayPayment(GuestPayPaymentVoidMessage guestPayPaymentVoidMessage);
    }
}

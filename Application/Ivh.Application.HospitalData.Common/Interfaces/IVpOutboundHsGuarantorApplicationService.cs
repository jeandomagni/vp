﻿namespace Ivh.Application.HospitalData.Common.Interfaces
{
    using System.Collections.Generic;
    using Ivh.Common.Base.Interfaces;
    using Ivh.Common.ServiceBus.Interfaces;
    using Ivh.Common.VisitPay.Messages.HospitalData;
    using Ivh.Common.VisitPay.Messages.HospitalData.Dtos;

    public interface IVpOutboundHsGuarantorApplicationService : IApplicationService,
        IUniversalConsumerService<PaymentMessage>
    {
        void OnHsGuarantorAcknowledgementMessage(AppHsGuarantorAcknowledgementMessage message);
        IReadOnlyList<VpOutboundHsGuarantorDto> GetOutboundEvents(int hsGuarantorId);
        void AddOutboundEvent(VpOutboundHsGuarantorDto hsGuarantorEvent);
        void CreateOutboundPayment(PaymentMessage message);
    }
}
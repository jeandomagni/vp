﻿namespace Ivh.Application.HospitalData.Common.Interfaces
{
    using System.Collections.Generic;
    using Ivh.Common.Base.Interfaces;
    using Ivh.Common.VisitPay.Messages.HospitalData.Dtos;

    public interface IHsSegmentationApplicationService : IApplicationService
    {
        IList<SegmentationAccountsReceivableDto> ProcessAccountsReceivableSegmentation(List<SegmentationAccountsReceivableDto> accountReceivableSegmentationDtos);
        void SaveAccountsReceivableSegmentationResult(IList<SegmentationAccountsReceivableDto> accountReceivableSegmentationDtos);

        IList<SegmentationActivePassiveDto> ProcessActivePassiveSegmentation(List<SegmentationActivePassiveDto> activePassiveSegmentationDtos);
        void SaveActivePassiveSegmentation(IList<SegmentationActivePassiveDto> segmentationActivePassiveVisits);

        IList<SegmentationBadDebtDto> ProcessBadDebtSegmentation(List<SegmentationBadDebtDto> segmentationBadDebtDtos);
        void SaveBadDebtSegmentationResult(IList<SegmentationBadDebtDto> segmentationBadDebtVisits);
        void UpdateBadDebtGuarantorSegmentationHistory(int segmentationBatchId);
        void UpdateAccountsReceivableGuarantorSegmentationHistory(int segmentationBatchId);
        void UpdateActivePassiveGuarantorSegmentationHistory(int segmentationBatchId);
    }
}
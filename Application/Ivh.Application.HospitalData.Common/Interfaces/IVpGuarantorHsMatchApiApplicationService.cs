﻿namespace Ivh.Application.HospitalData.Common.Interfaces
{
    using System;
    using System.Collections.Generic;
    using Ivh.Common.Base.Interfaces;
    using Ivh.Common.VisitPay.Messages.HospitalData.Dtos;
    using Models;
    using Responses;

    /// <summary>
    /// This is ment to mostly be a pass through to the repo.  We replaced a repository(In the patient/client/etc) with a web service client and this is the application service that services the webservice to feed the pseudo repository
    /// </summary>
    public interface IVpGuarantorHsMatchApiApplicationService : IApplicationService
    {
        IList<VpGuarantorHsMatchDto> GetById(VpGuarantorHsMatchApiGetByIdModel model);
        void InsertOrUpdate(VpGuarantorHsMatchDto dto);
        VpGuarantorSsnResult GetSsn(int vpGuarantorId);
    }
}
﻿namespace Ivh.Application.HospitalData.Common.Interfaces
{
    using Ivh.Common.Base.Interfaces;
    using Ivh.Common.ServiceBus.Interfaces;
    using Ivh.Common.VisitPay.Messages.HospitalData;

    public interface IRealTimeVisitEnrollmentApplicationService : IApplicationService,
                IUniversalConsumerService<RealTimeVisitEnrollmentMessage>

    {

    }
}
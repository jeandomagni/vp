﻿namespace Ivh.Application.HospitalData.Common.Interfaces
{
    using System;
    using Ivh.Common.Base.Interfaces;
    using Ivh.Common.VisitPay.Messages.Scoring;

    public interface IHsGuarantorScoringApplicationService : IApplicationService
    {
        void CalculateInputVariablesForRetroScoring(DateTime startDate, DateTime endDate);
        void ProcessScoreRequest(ScoringScoreRequestMessage message, int scoringBatchId);
    }
}
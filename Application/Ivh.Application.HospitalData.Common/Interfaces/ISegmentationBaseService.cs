﻿namespace Ivh.Application.HospitalData.Common.Interfaces
{
    public interface ISegmentationBaseService
    {
        int SegmentationTotal { get; set; }

        int SegmentationBatchId { get; set; }

        int SegmentationSuccesses();

        int SegmentationFailures();

        void InitializeService();
    }
}

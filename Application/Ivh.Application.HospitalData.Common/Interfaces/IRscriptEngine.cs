﻿namespace Ivh.Application.HospitalData.Common.Interfaces
{
    using Ivh.Common.VisitPay.Messages.HospitalData.Dtos;

    public interface IRscriptEngine
    {
        string ExecuteRScript(string inputJson, ScoringScriptStorage scoringScriptStorage);
    }
}

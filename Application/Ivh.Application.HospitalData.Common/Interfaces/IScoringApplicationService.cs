﻿namespace Ivh.Application.HospitalData.Common.Interfaces
{
    using Ivh.Common.ServiceBus.Interfaces;
    using Ivh.Common.VisitPay.Messages.Scoring;

    public interface IScoringApplicationService : IUniversalConsumerService<VpGuarantorScoreMessage>
    {
        void ValidateDailyScoringBatchInputVariables();
        void ValidateWeeklyScoringBatchInputVariables();
    }
}

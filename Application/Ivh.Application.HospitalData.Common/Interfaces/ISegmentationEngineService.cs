﻿namespace Ivh.Application.HospitalData.Common.Interfaces
{
    using Ivh.Common.VisitPay.Messages.HospitalData.Dtos;

    public interface ISegmentationEngineService
    {
        string ExecuteRScript(string inputJson, ScoringScriptStorage scoringScriptStorage);
    }
}

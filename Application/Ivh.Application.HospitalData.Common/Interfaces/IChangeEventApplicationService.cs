﻿namespace Ivh.Application.HospitalData.Common.Interfaces
{
    using System.Collections.Generic;
    using Ivh.Common.Base.Interfaces;
    using Ivh.Common.JobRunner.Common.Interfaces;
    using Ivh.Common.ServiceBus.Interfaces;
    using Ivh.Common.VisitPay.Enums;
    using Ivh.Common.VisitPay.Jobs;
    using Ivh.Common.VisitPay.Messages.HospitalData;
    using Ivh.Common.VisitPay.Messages.HospitalData.Dtos;
    using Ivh.Common.VisitPay.Messages.Matching;

    public interface IChangeEventApplicationService : IApplicationService,
        IUniversalConsumerService<ChangeEventProcessedMessage>,
        IUniversalConsumerService<ChangeSetEventsAggregatedMessage>,
        IUniversalConsumerService<ChangeSetEventsMessage>,
        IUniversalConsumerService<HsGuarantorMatchDiscrepancyMessage>,
        IUniversalConsumerService<HsGuarantorMatchDiscrepancyMessageList>,
        IUniversalConsumerService<GenerateChangeEventForVpGuarantorMessage>,
        IJobRunnerService<QueueVisitChangeSetJobRunner>

    {
        void ProcessChangeSetEvents(ChangeSetEventsMessage message);
        void ProcessChangeSetEventsAggregatedMessage(ChangeSetEventsAggregatedMessage changeSetEventsAggregatedMessage);
        void ChangeSetProcessedAcknowledgement(ChangeEventProcessedMessage message);
        void QueueAllUnprocessedChangeEvents();
        void GenerateChangeEventForHsGuarantor(int vpGuarantorId, int hsGuarantorId, IList<EnrollmentResponseDto> response);
        void GenerateChangeEventForGuarantor(int vpGuarantorId, IList<int> hsGuarantorIds);
        void SaveChangeEvent(ChangeEventDto changeEventDto);
        void SaveChangeEvent(ChangeEventDto changeEventDto, HsGuarantorChangeDto hsGuarantorChangeDto);
        void SaveChangeEvent(ChangeEventDto changeEventDto, VisitChangeDto visitChangeDto);
        void SaveChangeEvents(ChangeSetDto changeSet, ChangeSetTypeEnum changeSetType);
        IReadOnlyList<HsVisitDto> GetVisitsForGuarantor(int hsGuarantorId);
        IReadOnlyList<VpTransactionTypeDto> GetVisitTransactionTypes();
        IReadOnlyList<TransactionCodeDto> GetVisitTransactionCodes();
        IList<PatientTypeDto> GetPatientTypes();
        IList<PrimaryInsuranceTypeDto> GetPrimaryInsuranceTypes();
        IList<RevenueWorkCompanyDto> GetRevenueWorkCompanies();
        IList<LifeCycleStageDto> GetLifeCycleStages();
        IList<InsurancePlanDto> GetInsurancePlans();
        IList<FacilityDto> GetFacilities();
    }
}
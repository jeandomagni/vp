﻿namespace Ivh.Application.HospitalData.Common.Interfaces
{
    using System;
    using System.Collections.Generic;
    using Ivh.Common.VisitPay.Messages.HospitalData.Dtos;

    public interface ISegmentationAccountsReceivableService : ISegmentationBaseService
    {
        bool ProcessAccountsReceivableSegmentation(List<SegmentationAccountsReceivableDto> segmentationList,
            int scoringBatchId,
            string currentState,
            string eventName,
            Guid correlationId,
            Guid processId);
    }
}

﻿namespace Ivh.Application.HospitalData.Common.Interfaces
{
    using Ivh.Common.Base.Interfaces;
    using Ivh.Common.JobRunner.Common.Interfaces;
    using Ivh.Common.VisitPay.Jobs;

    public interface IJobsApplicationService : IApplicationService,
        IJobRunnerService<AccountSnapshotAndHistJobRunner>,
        IJobRunnerService<GuarantorSnapshotAndHistJobRunner>,
        IJobRunnerService<GuarantorStageTruncInsertJobRunner>,
        IJobRunnerService<HsGuarantorJobRunner>,
        IJobRunnerService<InboundLoadBaseJobRunner>,
        IJobRunnerService<InboundLoadBaseStageJobRunner>,
        IJobRunnerService<InboundLoadChangeEventsPart1JobRunner>,
        IJobRunnerService<InboundLoadChangeEventsPart2JobRunner>,
        IJobRunnerService<InboundLoadClaimsDataJobRunner>,
        IJobRunnerService<InboundLoadFilesJobRunner>,
        IJobRunnerService<InboundLoadHistoryJobRunner>,
        IJobRunnerService<InboundLoadSnapshotAndDeltaJobRunner>,
        IJobRunnerService<InboundScoringJobRunner>,
        IJobRunnerService<InboundPaymentVendorFileJobRunner>,
        IJobRunnerService<OutboundPaperFilesJobRunner>,
        IJobRunnerService<JamsIntegrationTest>,
        IJobRunnerService<PublishCdiEtlToVpEtlJobRunner>,
        IJobRunnerService<QueueChangeEventsJobRunner>,
        IJobRunnerService<TransactionSnapshotAndHistJobRunner>,
        IJobRunnerService<VisitJobRunner>,
        IJobRunnerService<VisitStageTruncInsertJobRunner>,
        IJobRunnerService<VisitTransactionJobRunner>,
        IJobRunnerService<VisitTransactionStageTruncInsertJobRunner>
    {
        void HSPersonDailyRecordCount();
        void HSPersonDeltaRecordCount();
        void HSPersonSnapshotRecordCount();
        void HSGuarantorStageRecordCount();
        void HSGuarantorRecordCdiCount();
        void GuarantorSnapshotAndHist();
        void AccountSnapshotAndHist();
        void TransactionSnapshotAndHist();
        string PublishCdiEtlToVpEtl();
        void QueueChangeEvents();
        void HSGuarantorStageTruncInsert();
        void VisitStageTruncInsert();
        void VisitTransactionStageTruncInsert();
        void HsGuarantor();
        void Visit();
        void VisitTransaction();
        string Inbound_LoadFiles();
        string Inbound_LoadHistory();
        string Inbound_LoadSnapshotAndDelta();
        string Inbound_LoadBaseStage();
        string Inbound_LoadChangeEvents_Part1();
        string Inbound_LoadBase();
        string Inbound_LoadChangeEvents_Part2();
        void Inbound_LoadClaimsFiles();
        void Inbound_ScoringAndSegmentation();
        void Inbound_PaymentVendorFile(string[] args);
        void Outbound_PaperFiles(string[] args);
    }
}

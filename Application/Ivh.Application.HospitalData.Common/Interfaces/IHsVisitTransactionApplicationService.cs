﻿namespace Ivh.Application.HospitalData.Common.Interfaces
{
    using System.Collections.Generic;
    using Ivh.Common.Base.Interfaces;
    using Ivh.Common.VisitPay.Messages.HospitalData.Dtos;

    public interface IHsVisitTransactionApplicationService : IApplicationService
    {
        IReadOnlyList<VpTransactionTypeDto>  GetTransactionTypes();
        List<HsVisitTransactionDto> CreateTransactions(List<HsVisitTransactionDto> visitTransactionDtos);
    }
}

﻿namespace Ivh.Application.HospitalData.Common.Interfaces
{
    using System;
    using System.Collections.Generic;
    using Ivh.Common.VisitPay.Messages.HospitalData.Dtos;

    public interface ISegmentationBadDebtService : ISegmentationBaseService
    {
        bool ProcessBadDebtSegmentation(List<SegmentationBadDebtDto> segmentationList,
            int scoringBatchId,
            string currentState,
            string eventName,
            Guid correlationId,
            Guid processId);
    }
}

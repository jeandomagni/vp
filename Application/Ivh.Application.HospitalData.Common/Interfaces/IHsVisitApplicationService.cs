﻿namespace Ivh.Application.HospitalData.Common.Interfaces
{
    using Ivh.Common.Base.Interfaces;
    using Ivh.Common.ServiceBus.Interfaces;
    using Ivh.Common.VisitPay.Messages.Core;

    public interface IHsVisitApplicationService : IApplicationService,
        IUniversalConsumerService<FirstVisitLoadedMessage>
    {
    }
}
﻿namespace Ivh.Application.HospitalData.Common.Interfaces
{
    public interface ICdiEtlJobsProvider
    {
        void HSPersonDailyRecordCount();
        void HSPersonDeltaRecordCount();
        void HSPersonSnapshotRecordCount();
        void HSGuarantorStageRecordCount();
        void HSGuarantorRecordCdiCount();
        void GuarantorSnapshotAndHist();
        void AccountSnapshotAndHist();
        void TransactionSnapshotAndHist();
        void QueueChangeEvents();
        void HSGuarantorStageTruncInsert();
        void VisitStageTruncInsert();
        void VisitTransactionStageTruncInsert();
        void HsGuarantor();
        void Visit();
        void VisitTransaction();
    }
}

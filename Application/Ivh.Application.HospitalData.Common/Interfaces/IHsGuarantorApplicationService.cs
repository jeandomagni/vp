﻿namespace Ivh.Application.HospitalData.Common.Interfaces
{
    using System.Collections.Generic;
    using Ivh.Common.Base.Interfaces;
    using Ivh.Common.VisitPay.Messages.HospitalData.Dtos;

    public interface IHsGuarantorApplicationService : IApplicationService
    {
        HsGuarantorDto GetHsGuarantor(int hsGuarantorId);
        IReadOnlyList<HsGuarantorSearchResultDto> GetHsGuarantors(HsGuarantorFilterDto filterDto);
        HsGuarantorDto AddHsGuarantor(HsGuarantorDto hsGuarantor);
    }
}

﻿namespace Ivh.Application.HospitalData.Common.Interfaces
{
    using Ivh.Common.Base.Enums;
    using Ivh.Common.Base.Interfaces;
    using Ivh.Common.VisitPay.Enums;
    using Ivh.Common.VisitPay.Messages.HospitalData.Dtos;

    public interface IChangeSetApplicationService : IApplicationService
    {
        ChangeSetDto CreateChangeSetFromVisit(int visitId);
        HsVisitDto GetVisit(int visitId);
    }
}

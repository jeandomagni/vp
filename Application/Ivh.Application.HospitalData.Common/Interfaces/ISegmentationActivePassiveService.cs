﻿namespace Ivh.Application.HospitalData.Common.Interfaces
{
    using System;
    using System.Collections.Generic;
    using Ivh.Common.VisitPay.Messages.HospitalData.Dtos;

    public interface ISegmentationActivePassiveService : ISegmentationBaseService
    {
        bool ProcessActivePassiveSegmentation(List<SegmentationActivePassiveDto> segmentationList,
            int scoringBatchId,
            string currentState,
            string eventName,
            Guid correlationId,
            Guid processId);
    }
}

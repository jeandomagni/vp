﻿namespace Ivh.Application.HospitalData.Common.Interfaces
{
    using System.Collections.Generic;
    using Ivh.Common.Base.Interfaces;
    using Ivh.Common.ServiceBus.Interfaces;
    using Ivh.Common.VisitPay.Messages.FinanceManagement;
    using Ivh.Common.VisitPay.Messages.HospitalData;

    public interface IVpOutboundVisitApplicationService : IApplicationService,
        IUniversalConsumerService<OutboundVisitMessage>,
        IUniversalConsumerService<OutboundVisitMessageList>,
        IUniversalConsumerService<PaymentAllocationMessage>,
        IUniversalConsumerService<PaymentAllocationMessageList>,
        IUniversalConsumerService<InterestEventMessageList>
    {
        void CreateOutboundVisit(OutboundVisitMessage outboundVisitMessage);
        void CreateOutboundVisits(IList<OutboundVisitMessage> messages);
        void CreateOutboundVisitTransactions(IList<PaymentAllocationMessage> messages);
        void CreateOutboundVisitTransactions(IList<InterestEventMessage> messages);
    }
}
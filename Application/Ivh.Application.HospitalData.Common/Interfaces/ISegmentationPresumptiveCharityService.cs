﻿namespace Ivh.Application.HospitalData.Common.Interfaces
{
    using System;
    using System.Collections.Generic;

    public interface ISegmentationPresumptiveCharityService : ISegmentationBaseService
    {
        bool ProcessPresumptiveCharitySegmentation(int scoringBatchId,
            string currentState,
            string eventName,
            Guid correlationId,
            Guid processId);
    }
}

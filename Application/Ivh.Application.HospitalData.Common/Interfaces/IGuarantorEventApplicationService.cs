﻿namespace Ivh.Application.HospitalData.Common.Interfaces
{
    using Ivh.Common.Base.Interfaces;
    using Ivh.Common.ServiceBus.Interfaces;
    using Ivh.Common.VisitPay.Messages.HospitalData;

    public interface IGuarantorEventApplicationService : IApplicationService,
        IUniversalConsumerService<AppHsGuarantorAcknowledgementMessage>
    {
        void OnHsGuarantorAcknowledgementMessage(AppHsGuarantorAcknowledgementMessage message);
    }
}
﻿namespace Ivh.Application.HospitalData.Common.Interfaces
{
    using System.Collections.Generic;

    public interface IHsGuarantorEnrollmentApiService
    {
        IList<string> GetVisitsAfterEnrollment(string hsGuarantorSourceSystemKey, int billingSystem);
    }
}
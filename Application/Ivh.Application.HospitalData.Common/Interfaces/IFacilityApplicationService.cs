﻿namespace Ivh.Application.HospitalData.Common.Interfaces
{
    using System.Collections.Generic;
    using Ivh.Common.Base.Interfaces;
    using Ivh.Common.VisitPay.Messages.HospitalData.Dtos;

    public interface IFacilityApplicationService : IApplicationService
    {
        IReadOnlyList<FacilityDto> GetAllFacilities();
    }
}
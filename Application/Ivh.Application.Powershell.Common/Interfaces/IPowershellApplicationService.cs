﻿namespace Ivh.Application.Powershell.Common.Interfaces
{
    using Ivh.Common.Base.Interfaces;
    using Ivh.Common.ServiceBus.Interfaces;
    using Ivh.Common.VisitPay.Messages.HospitalData;

    public interface IPowershellApplicationService : IApplicationService, IUniversalConsumerService<CloneCdiAppGuarantorMessage>
    {
        string ExportCdiDataToFile();
        string HsGuarantorMatchDataMove();
        string InsertFileNametoFileTracker();
        string MoveVpDataToCdi();
        string PublishCdiEtlToVpEtl();
        string ReRunnableOutboundSQL();
        string Inbound_LoadFiles();
        string Inbound_LoadHistory();
        string Inbound_LoadSnapshotAndDelta();
        string Inbound_LoadBaseStage();
        string Inbound_LoadChangeEvents_Part1();
        string Inbound_LoadBase();
        string Inbound_LoadChangeEvents_Part2();
        string CloneCdiAppGuarantor(int hsGuarantorId, int vpGuarantorId, string appendValue,string userName, bool consolidate);
    }
}

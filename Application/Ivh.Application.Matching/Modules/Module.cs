﻿namespace Ivh.Application.Matching.Modules
{
    using Autofac;
    using Common.Interfaces;

    public class Module : Autofac.Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<MatchingApplicationService>().As<IMatchingApplicationService>();
        }
    }
}

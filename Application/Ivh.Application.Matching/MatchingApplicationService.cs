﻿namespace Ivh.Application.Matching
{
    using Ivh.Common.JobRunner.Common.Interfaces;
    using Ivh.Common.VisitPay.Jobs;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using AutoMapper;
    using Domain.Logging.Interfaces;
    using Domain.Matching.Entities;
    using Domain.Matching.Interfaces;
    using Domain.Matching.Match;
    using Ivh.Application.Base.Services;
    using Ivh.Application.Base.Common.Interfaces;
    using Ivh.Application.Matching.Common.Interfaces;
    using Ivh.Common.Base.Utilities.Extensions;
    using Ivh.Common.ServiceBus.Attributes;
    using Ivh.Common.ServiceBus.Enums;
    using Ivh.Common.VisitPay.Constants;
    using Ivh.Common.VisitPay.Enums;
    using Ivh.Common.VisitPay.Messages.HospitalData;
    using Ivh.Common.VisitPay.Messages.Matching;
    using Ivh.Common.VisitPay.Utilities.Extensions;
    using MassTransit;
    using Ivh.Common.VisitPay.Messages.Matching.Dtos;

    public class MatchingApplicationService : ApplicationService, IMatchingApplicationService
    {
        private readonly Lazy<IMatchingService> _matchingService;
        private readonly Lazy<IInitialMatchService> _initialMatchService;
        private readonly Lazy<IMetricsProvider> _metricsProvider;
        private readonly Lazy<IMatchReconciliationService> _matchReconciliationService;

        public MatchingApplicationService(
            Lazy<IApplicationServiceCommonService> applicationServiceCommonService,
            Lazy<IMatchingService> matchingService,
            Lazy<IInitialMatchService> initialMatchService,
            Lazy<IMatchReconciliationService> matchReconciliationService,
            Lazy<IMetricsProvider> metricsProvider
            )
            : base(applicationServiceCommonService)
        {
            this._matchingService = matchingService;
            this._initialMatchService = initialMatchService;
            this._matchReconciliationService = matchReconciliationService;
            this._metricsProvider = metricsProvider;
        }

        public GuarantorMatchResult IsGuarantorMatch(MatchDataDto matchDataDto)
        {
            Patient matchPatient = matchDataDto.Patient != null ? Mapper.Map<Patient>(matchDataDto.Patient) : new Patient();
            matchPatient.StatementIdentifierId = matchDataDto.GuestPayAuthentication?.StatementIdentifierId ?? default(int);

            MatchDataGroup matchData = new MatchDataGroup
            {
                Guarantor = Mapper.Map<Ivh.Domain.Matching.Entities.Guarantor>(matchDataDto.Guarantor),
                Visits = new MatchVisitData
                {
                    Patient = matchPatient,
                }.ToListOfOne()
            };

            // check the match registry for a valid auth(initial) match
            GuarantorMatchResult matchResult = this._initialMatchService.Value.GetInitialMatchResult(matchData, matchDataDto.ApplicationEnum);

            return matchResult;
        }

        public void QueueFindNewHsGuarantorMatches()
        {
            if (!this.FeatureService.Value.IsFeatureEnabled(VisitPayFeatureEnum.FeatureMatchingApiIsEnabled))
            {
                this.LoggingService.Value.Warn(() => $"{nameof(this.QueueFindNewHsGuarantorMatches)} tried to run, but {nameof(VisitPayFeatureEnum.FeatureMatchingApiIsEnabled)} is not enabled");
                return;
            }

            IEnumerable<IEnumerable<int>> matchedVpGuarantorIdBatches = this._matchingService.Value.GetMatchedVpguarantorIds().SplitIntoChunks(100);

            foreach (IEnumerable<int> matchedVpGuarantorIds in matchedVpGuarantorIdBatches)
            {
                QueueFindNewHsGuarantorMatchesMessageList message = new QueueFindNewHsGuarantorMatchesMessageList()
                {
                    Messages = matchedVpGuarantorIds.Select(x => new QueueFindNewHsGuarantorMatchesMessage() { VpGuarantorId = x }).ToList()
                };

                this.Bus.Value.PublishMessage(message).Wait();
            }
        }

        public void QueueMatchReconciliation()
        {
            int publishCount = 0;
            IEnumerable<IEnumerable<int>> vpGuarantorIds = this._matchReconciliationService.Value.GetMatchReconciliationVpGuarantorIds().SplitIntoChunks(100);

            foreach (IEnumerable<int> vpGuarantorIdChunk in vpGuarantorIds)
            {
                MatchReconciliationMessage message = new MatchReconciliationMessage
                {
                    VpGuarantorIds = vpGuarantorIdChunk.ToList()
                };

                this.Bus.Value.PublishMessage(message).Wait();
                publishCount += message.VpGuarantorIds.Count;
            }

            this._metricsProvider.Value.Increment(Metrics.Increment.Matching.Reconciliation.Queued, publishCount);
        }

        public IList<GuarantorMatchResult> UpdateMatchInfo(int vpGuarantorId, MatchGuarantorDto guarantor, ApplicationEnum applicationEnum)
        {
            MatchDataGroup matchData = new MatchDataGroup
            {
                Guarantor = Mapper.Map<Ivh.Domain.Matching.Entities.Guarantor>(guarantor)
            };

            return this._matchingService.Value.UpdateMatchInfo(vpGuarantorId, matchData, applicationEnum);
        }

        public string GetGuarantorMatchSsn(int vpGuarantorId)
        {
            return this._matchingService.Value.GetGuarantorMatchSsn(vpGuarantorId);
        }

        public void FindNewHsGuarantorMatches(IList<int> matchedVpGuarantorIds, bool sendRetryMessage)
        {
            this._matchingService.Value.FindNewHsGuarantorMatches(matchedVpGuarantorIds, sendRetryMessage);
        }

        public void QueueMatchRegistryPopulation(DateTime? processDate)
        {
            processDate = processDate ?? this._matchingService.Value.GetLastUpdateDate();

            IEnumerable<IEnumerable<int>> newHsGuarantorIds = this._matchingService.Value.GetMatchesToQueue(processDate.Value, false);
            IEnumerable<IEnumerable<int>> updatedHsGuarantorIds = this._matchingService.Value.GetMatchesToQueue(processDate.Value, true);

            int publishCount = 0;

            foreach (IEnumerable<int> newHsGuarantorIdChunk in newHsGuarantorIds)
            {
                PopulateMatchRegistryMessageList message = new PopulateMatchRegistryMessageList
                {
                    ProcessDate = processDate.Value,
                    IsUpdate = false,
                    Messages = newHsGuarantorIdChunk.Select(x => new PopulateMatchRegistryMessage { HsGuarantorId = x, IsUpdate = false }).ToList()
                };

                this.Bus.Value.PublishMessage(message).Wait();

                publishCount += message.Messages.Count;
            }

            foreach (IEnumerable<int> updatedHsGuarantorIdChunk in updatedHsGuarantorIds)
            {
                PopulateMatchRegistryMessageList message = new PopulateMatchRegistryMessageList
                {
                    ProcessDate = processDate.Value,
                    IsUpdate = true,
                    Messages = updatedHsGuarantorIdChunk.Select(x => new PopulateMatchRegistryMessage { HsGuarantorId = x, IsUpdate = true }).ToList()
                };

                this.Bus.Value.PublishMessage(message).Wait();

                publishCount += message.Messages.Count;
            }

            this._metricsProvider.Value.Increment(Metrics.Increment.Matching.RegistryPopulation.QueuedChanges, publishCount);
        }

        public IList<string> GetRequiredMatchFieldsPatient(ApplicationEnum applicationEnum, PatternUseEnum? matchType = null)
        {
            return this._matchingService.Value.GetRequiredMatchFields(applicationEnum, matchType, FieldSourceEnum.Patient);
        }

        public IList<string> GetRequiredMatchFieldsGuarantor(ApplicationEnum applicationEnum, PatternUseEnum? matchType = null)
        {
            return this._matchingService.Value.GetRequiredMatchFields(applicationEnum, matchType, FieldSourceEnum.Guarantor);
        }

        private void PopulateMatchRegistry(DateTime processDate, MatchDataGroup matchData)
        {
            this._matchingService.Value.ProcessMatchUpdates(processDate, matchData.ToListOfOne(), false, true);
        }

        private void PopulateMatchRegistries(PopulateMatchRegistryMessageList messageList)
        {
            IList<MatchDataGroup> matchDatas = this._matchingService.Value.GetMatchDataGroups(messageList.Messages.Select(x => x.HsGuarantorId).ToList(), null);

            this._matchingService.Value.ProcessMatchUpdates(messageList.ProcessDate, matchDatas, true, messageList.IsUpdate);
        }

        public IList<GuarantorMatchResult> AddInitialMatch(int vpGuarantorId, MatchDataDto matchDataDto)
        {
            MatchDataGroup matchData = new MatchDataGroup
            {
                Guarantor = Mapper.Map<Ivh.Domain.Matching.Entities.Guarantor>(matchDataDto.Guarantor),
                Visits = new MatchVisitData
                {
                    Patient = Mapper.Map<Patient>(matchDataDto.Patient),
                }.ToListOfOne()
            };

            return this._matchingService.Value.AddInitialMatches(vpGuarantorId, matchData, matchDataDto.ApplicationEnum);
        }

        private void UpdateHsGuarantorMatchStatus(int vpGuarantorId, int hsGuarantorId, HsGuarantorMatchStatusEnum hsGuarantorMatchStatus)
        {
            MatchDataGroup matchData = this._matchingService.Value.GetMatchDataGroups(hsGuarantorId.ToListOfOne(), ApplicationEnum.VisitPay)?.FirstOrDefault();

            if (matchData != null)
            {
                this._matchingService.Value.UpdateMatchStatus(vpGuarantorId, matchData, hsGuarantorMatchStatus);
            }
        }



        #region jobrunners

        void IJobRunnerService<QueueMatchRegistryPopulationJobRunner>.Execute(DateTime begin, DateTime end, string[] args)
        {
            const int processDateParameterIndex = 3;
            DateTime? dataChangeDateOverrideParameter = args.TryParseDateArg(processDateParameterIndex);

            this.QueueMatchRegistryPopulation(dataChangeDateOverrideParameter);
        }

        void IJobRunnerService<QueueFindNewHsGuarantorMatchesJobRunner>.Execute(DateTime begin, DateTime end, string[] args)
        {
            this.QueueFindNewHsGuarantorMatches();
        }
        
        void IJobRunnerService<QueueMatchReconiliationJobRunner>.Execute(DateTime begin, DateTime end, string[] args)
        {
            this.QueueMatchReconciliation();
        }

        #endregion jobrunners

        #region consumers

        [UniversalConsumer(UniversalQueuesEnum.MatchingProcessor, UniversalPriorityEnum.Priority30)]
        public void ConsumeMessage(PopulateMatchRegistryMessageList message, ConsumeContext<PopulateMatchRegistryMessageList> consumeContext)
        {
            this.PopulateMatchRegistries(message);
        }

        public bool IsMessageValid(PopulateMatchRegistryMessageList message, ConsumeContext<PopulateMatchRegistryMessageList> consumeContext)
        {
            //todo: VP-3433 - want a check here, or integrated into the update check?
            bool isValid = true;
            return isValid;
        }

        [UniversalConsumer(UniversalQueuesEnum.MatchingProcessor, UniversalPriorityEnum.Priority30)]
        public void ConsumeMessage(PopulateMatchRegistryMessage message, ConsumeContext<PopulateMatchRegistryMessage> consumeContext)
        {
            if (message.ProcessDate.HasValue)
            {
                IList<MatchDataGroup> matchDatas = this._matchingService.Value.GetMatchDataGroups(message.HsGuarantorId.ToListOfOne(), null);
                this.PopulateMatchRegistry(message.ProcessDate.Value, matchDatas.First());
            }
            else
            {
                this.LoggingService.Value.Warn(() =>  $"{nameof(MatchingApplicationService)}::{nameof(this.ConsumeMessage)}({nameof(PopulateMatchRegistryMessage)})" + 
                                               $" - not processed for HsGuarantorId = {message.HsGuarantorId}. {nameof(message.ProcessDate)} was not set. This shouldn't happen.");
            }
        }

        public bool IsMessageValid(PopulateMatchRegistryMessage message, ConsumeContext<PopulateMatchRegistryMessage> consumeContext)
        {
            bool isValid = true;
            return isValid;
        }

        [UniversalConsumer(UniversalQueuesEnum.MatchingProcessor, UniversalPriorityEnum.Priority20)]
        public void ConsumeMessage(HsGuarantorUnmatchMessage message, ConsumeContext<HsGuarantorUnmatchMessage> consumeContext)
        {
            this.UpdateHsGuarantorMatchStatus(message.VpGuarantorId, message.HsGuarantorId, message.HsGuarantorMatchStatus);
        }

        public bool IsMessageValid(HsGuarantorUnmatchMessage message, ConsumeContext<HsGuarantorUnmatchMessage> consumeContext)
        {
            bool isValid = true;
            return isValid;
        }

        [UniversalConsumer(UniversalQueuesEnum.MatchingProcessor, UniversalPriorityEnum.Priority20)]
        public void ConsumeMessage(HsGuarantorRematchMessage message, ConsumeContext<HsGuarantorRematchMessage> consumeContext)
        {
            this.UpdateHsGuarantorMatchStatus(message.VpGuarantorId, message.HsGuarantorId, HsGuarantorMatchStatusEnum.Matched);
        }

        public bool IsMessageValid(HsGuarantorRematchMessage message, ConsumeContext<HsGuarantorRematchMessage> consumeContext)
        {
            bool isValid = true;
            return isValid;
        }

        [UniversalConsumer(UniversalQueuesEnum.MatchingProcessor, UniversalPriorityEnum.Priority30)]
        public void ConsumeMessage(QueueFindNewHsGuarantorMatchesMessageList message, ConsumeContext<QueueFindNewHsGuarantorMatchesMessageList> consumeContext)
        {
            this.FindNewHsGuarantorMatches(message.Messages.Select(x => x.VpGuarantorId).ToList(), true);
        }

        public bool IsMessageValid(QueueFindNewHsGuarantorMatchesMessageList message, ConsumeContext<QueueFindNewHsGuarantorMatchesMessageList> consumeContext)
        {
            bool isValid = true;
            return isValid;
        }

        [UniversalConsumer(UniversalQueuesEnum.MatchingProcessor, UniversalPriorityEnum.Priority30)]
        public void ConsumeMessage(QueueFindNewHsGuarantorMatchesMessage message, ConsumeContext<QueueFindNewHsGuarantorMatchesMessage> consumeContext)
        {
            this.FindNewHsGuarantorMatches(message.VpGuarantorId.ToListOfOne(), false);
        }

        public bool IsMessageValid(QueueFindNewHsGuarantorMatchesMessage message, ConsumeContext<QueueFindNewHsGuarantorMatchesMessage> consumeContext)
        {
            bool isValid = true;
            return isValid;
        }

        [UniversalConsumer(UniversalQueuesEnum.MatchingProcessor, UniversalPriorityEnum.Priority20)]
        public void ConsumeMessage(UpdateMatchInfoMessage message, ConsumeContext<UpdateMatchInfoMessage> consumeContext)
        {
            this.UpdateMatchInfo(message.VpGuarantorId, message.MatchGuarantorDto, message.Application);
        }

        public bool IsMessageValid(UpdateMatchInfoMessage message, ConsumeContext<UpdateMatchInfoMessage> consumeContext)
        {
            bool isValid = true;
            return isValid;
        }

        [UniversalConsumer(UniversalQueuesEnum.MatchingProcessor, UniversalPriorityEnum.Priority30)]
        public void ConsumeMessage(MatchReconciliationMessage message, ConsumeContext<MatchReconciliationMessage> consumeContext)
        {
            this._matchReconciliationService.Value.MatchReconciliation(message.VpGuarantorIds);
        }

        public bool IsMessageValid(MatchReconciliationMessage message, ConsumeContext<MatchReconciliationMessage> consumeContext)
        {
            bool isValid = true;
            return isValid;
        }

        #endregion consumers

    }
}

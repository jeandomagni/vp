﻿namespace Ivh.Application.Powershell.Services
{
    using Common.Interfaces;
    using Domain.Powershell.Interfaces;
    using Ivh.Common.ServiceBus.Attributes;
    using Ivh.Common.ServiceBus.Enums;
    using Ivh.Common.VisitPay.Messages.HospitalData;
    using MassTransit;

    public class PowershellApplicationService : IPowershellApplicationService
    {
        private readonly IPowershellService _powershellService;

        public PowershellApplicationService(IPowershellService powershellService)
        {
            this._powershellService = powershellService;
        }

        public string ExportCdiDataToFile()
        {
            return this._powershellService.ExportCdiDataToFile();
        }

        public string HsGuarantorMatchDataMove()
        {
            return this._powershellService.HsGuarantorMatchDataMove();
        }

        public string InsertFileNametoFileTracker()
        {
            return this._powershellService.InsertFileNametoFileTracker();
        }

        public string MoveVpDataToCdi()
        {
            return this._powershellService.MoveVpDataToCdi();
        }

        public string PublishCdiEtlToVpEtl()
        {
            return this._powershellService.PublishCdiEtlToVpEtl();
        }

        public string ReRunnableOutboundSQL()
        {
            return this._powershellService.ReRunnableOutboundSQL();
        }

        public string Inbound_LoadFiles()
        {
            return this._powershellService.Inbound_LoadFiles();
        }

        public string Inbound_LoadHistory()
        {
            return this._powershellService.Inbound_LoadHistory();
        }

        public string Inbound_LoadSnapshotAndDelta()
        {
            return this._powershellService.Inbound_LoadSnapshotAndDelta();
        }

        public string Inbound_LoadBaseStage()
        {
            return this._powershellService.Inbound_LoadBaseStage();
        }

        public string Inbound_LoadChangeEvents_Part1()
        {
            return this._powershellService.Inbound_LoadChangeEvents_Part1();
        }

        public string Inbound_LoadBase()
        {
            return this._powershellService.Inbound_LoadBase();
        }

        public string Inbound_LoadChangeEvents_Part2()
        {
            return this._powershellService.Inbound_LoadChangeEvents_Part2();
        }

        public string CloneCdiAppGuarantor(int hsGuarantorId, int vpGuarantorId, string appendValue, string userName, bool consolidate)
        {
            return this._powershellService.CloneCdiAppGuarantor(hsGuarantorId, vpGuarantorId, appendValue, userName, consolidate);
        }

        #region consumers

        [UniversalConsumer(UniversalQueuesEnum.VisitPayProcessor, UniversalPriorityEnum.Priority40)]
        public void ConsumeMessage(CloneCdiAppGuarantorMessage message, ConsumeContext<CloneCdiAppGuarantorMessage> consumeContex)
        {
            this.CloneCdiAppGuarantor(message.HsGuarantorId, message.VpGuarantorId, message.AppendValue, message.UserName, message.Consolidate);
        }

        public bool IsMessageValid(CloneCdiAppGuarantorMessage message, ConsumeContext<CloneCdiAppGuarantorMessage> consumeContext)
        {
            bool isValid = true;
            return isValid;
        }

        #endregion consumers
    }
}

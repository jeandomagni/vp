﻿namespace Ivh.Application.Powershell.Modules
{
    using Autofac;
    using Common.Interfaces;
    using Services;

    public class Module : Autofac.Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<PowershellApplicationService>().As<IPowershellApplicationService>();
        }
    }
}
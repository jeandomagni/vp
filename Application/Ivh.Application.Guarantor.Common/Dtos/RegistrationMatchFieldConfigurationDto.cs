﻿namespace Ivh.Application.Guarantor.Common.Dtos
{
    using System.Collections.Generic;
    using System.Linq;
    using Ivh.Common.Base.Enums;
    using Ivh.Common.VisitPay.Enums;

    public class RegistrationMatchFieldConfigurationDto
    {
        public RegistrationMatchFieldEnum Field { get; set; }
        public bool Required { get; set; }
        public bool Visible { get; set; }
    }

    public class RegistrationMatchConfigurationDto : List<RegistrationMatchFieldConfigurationDto>
    {
        public RegistrationMatchFieldConfigurationDto ConfigurationForField(RegistrationMatchFieldEnum field) => this.FirstOrDefault(x => x.Field == field);
    }
}

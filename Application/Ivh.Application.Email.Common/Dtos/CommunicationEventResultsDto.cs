﻿namespace Ivh.Application.Email.Common.Dtos
{
    using System.Collections.Generic;

    public class CommunicationEventResultsDto
    {
        public IReadOnlyList<CommunicationEventDto> Events { get; set; }
        public int TotalRecords { get; set; }
    }
}
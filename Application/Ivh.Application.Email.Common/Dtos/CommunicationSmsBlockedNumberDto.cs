﻿namespace Ivh.Application.Email.Common.Dtos
{
    public class CommunicationSmsBlockedNumberDto
    {
        public int CommunicationSmsBlockedNumberId { get; set; }
        public string BlockedPhoneNumber { get; set; }
        public string SmsPhoneNumber { get; set; }
    }
}
﻿namespace Ivh.Application.Email.Common.Dtos
{
    public class EmailActionResultDto
    {
        public readonly string Message;
        public readonly bool Success;

        public EmailActionResultDto(bool success, string message)
        {
            this.Message = message;
            this.Success = success;
        }
    }
}
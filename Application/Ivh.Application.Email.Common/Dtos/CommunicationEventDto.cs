﻿namespace Ivh.Application.Email.Common.Dtos
{
    using System;

    public class CommunicationEventDto
    {
        public int CommunicationEventId { get; set; }
        public Guid UniqueId { get; set; }
        public string ToAddress { get; set; }
        public string EventName { get; set; }
        public string Category { get; set; }
        public string Response { get; set; }
        public string Attempt { get; set; }
        public string EventStatus { get; set; }
        public string Reason { get; set; }
        public string EventType { get; set; }
        public string SourceApp { get; set; }
        public string EventId { get; set; }
        public string GroupId { get; set; }
        public string IP { get; set; }
        public string SmtpId { get; set; }
        public string Timestamp { get; set; }
        public string Url { get; set; }
        public string UserAgent { get; set; }
        public string RawData { get; set; }
        public DateTime? EventDateTime { get; set; }
        public DateTime InsertedDate { get; set; }
    }
}
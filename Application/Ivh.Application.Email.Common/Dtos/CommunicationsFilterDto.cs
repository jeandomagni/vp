﻿namespace Ivh.Application.Email.Common.Dtos
{
    using System;

    public class CommunicationsFilterDto
    {
        public DateTime? CommunicationDateRangeFrom { get; set; }
        public DateTime? CommunicationDateRangeTo { get; set; }
        public DateTime? DateRangeFrom { get; set; }
        public string SortField { get; set; }
        public string SortOrder { get; set; }
    }
}
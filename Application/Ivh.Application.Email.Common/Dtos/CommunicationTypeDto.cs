﻿namespace Ivh.Application.Email.Common.Dtos
{
    using Content.Common.Dtos;
    using Ivh.Common.Base.Enums;
    using Ivh.Common.VisitPay.Enums;

    public class CommunicationTypeDto
    {
        public CommunicationTypeEnum CommunicationTypeId { get; set; }
        public string CommunicationTypeName { get; set; }
        public CmsRegionDto CmsRegion { get; set; }
        public CommunicationTypeStatusEnum CommunicationTypeStatus { get; set; }
        public CommunicationMethodEnum CommunicationMethodId { get; set; }
        public int? CommunicationGroupId { get; set; }
    }
}
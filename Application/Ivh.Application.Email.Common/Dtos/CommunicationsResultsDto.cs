﻿namespace Ivh.Application.Email.Common.Dtos
{
    using System.Collections.Generic;

    public class CommunicationsResultsDto
    {
        public IReadOnlyList<CommunicationDto> Communications { get; set; }
        public int TotalRecords { get; set; }
    }
}
namespace Ivh.Application.Email.Common.Dtos
{
    using Ivh.Common.Base.Enums;
    using Ivh.Common.VisitPay.Enums;

    public class SmsResponseResult
    {
        public SmsResponseActionEnum ResponseActionEnum { get; set; }
        /// <summary>
        /// Todo:Is this needed?
        /// </summary>
        public string Body { get; set; }
    }
}
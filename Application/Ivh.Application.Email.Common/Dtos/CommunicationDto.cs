﻿using System;

namespace Ivh.Application.Email.Common.Dtos
{
    using Ivh.Common.Base.Enums;
    using Ivh.Common.VisitPay.Enums;

    public class CommunicationDto
    {
        public int CommunicationId { get; set; }
        public Guid UniqueId { get; set; }
        public string FromName { get; set; }
        public string FromAddress { get; set; }
        public string ToAddress { get; set; }
        public string Cc { get; set; }
        public string Bcc { get; set; }
        public string Subject { get; set; }
        public string Body { get; set; }
        public int? SentToUserId { get; set; }
        public int? SentToPaymentUnitId { get; set; }
        public CommunicationStatusEnum CommunicationStatus { get; set; }
        public int? SendFailureCnt { get; set; }
        public CommunicationDto ResentCommunication { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime? OutboundServerAccepted { get; set; }
        public DateTime? ProcessedDate { get; set; }
        public DateTime? DeliveredDate { get; set; }
        public DateTime? FirstOpenDate { get; set; }
        public DateTime? FailedDate { get; set; }
        public CommunicationTypeDto CommunicationType { get; set; }
        public string Locale { get; set; }
    }
}
﻿namespace Ivh.Application.Core.Common.Dtos
{
    public class VisitPayUserCommunicationPreferenceDto
    {
        public int VisitPayUserCommunicationPreferenceId { get; set; }
        public int VisitPayUserId { get; set; }
        public int CommunicationGroupId { get; set; }
        public int CommunicationMethodId { get; set; }
    }
}
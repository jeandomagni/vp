﻿namespace Ivh.Application.Email.Common.Dtos
{
    using System.Collections.Generic;

    public class CommunicationGroupDto
    {
        public CommunicationGroupDto()
        {
            this.CommunicationTypes = new List<CommunicationTypeDto>();
        }

        public int CommunicationGroupId { get; set; }

        public string CommunicationGroupName { get; set; }

        public string IconType { get; set; }
        
        public string Description { get; set; }

        public IList<CommunicationTypeDto> CommunicationTypes { get; set; }
    }
}
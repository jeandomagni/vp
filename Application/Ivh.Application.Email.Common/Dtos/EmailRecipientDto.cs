﻿namespace Ivh.Application.Email.Common.Dtos
{
    public class EmailRecipientDto
    {
        public int VisitPayUserId { get; set; }
        public string Email { get; set; }
        public string Name { get; set; }
        public string UserName { get; set; }
    }
}
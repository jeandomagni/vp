﻿namespace Ivh.Application.Email.Common.Interfaces
{
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Core.Common.Dtos;
    using Dtos;
    using Ivh.Common.Base.Interfaces;
    using Ivh.Common.EventJournal;
    using Ivh.Common.VisitPay.Enums;

    public interface ICommunicationApplicationService : IApplicationService
    {
        IList<VisitPayUserCommunicationPreferenceDto> CommunicationPreferences(int visitPayUserId);
        void AddPreference(CommunicationMethodEnum communicationMethodEnum, int targetVisitPayUserId, int communicationGroupId, int vpGuarantorId, int visitPayUserId, JournalEventHttpContextDto context);
        void RemovePreference(CommunicationMethodEnum communicationMethodEnum, int targetVisitPayUserId, int communicationGroupId, int vpGuarantorId, int visitPayUserId, JournalEventHttpContextDto context);
        Task<IList<CommunicationGroupDto>> GetCommunicationGroupsAsync();
        IList<CommunicationSmsBlockedNumberDto> FindBlockedNumbers(string phoneNumber);
        void AddBlockedNumber(string phoneNumber, string smsNumber);
        void RemoveBlockedNumber(string phoneNumber, string smsNumber);
    }
}
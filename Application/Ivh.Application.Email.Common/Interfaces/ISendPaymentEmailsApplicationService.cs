namespace Ivh.Application.Email.Common.Interfaces
{
    using Ivh.Common.Base.Interfaces;
    using Ivh.Common.ServiceBus.Interfaces;
    using Ivh.Common.VisitPay.Messages.Communication;
    using Ivh.Common.VisitPay.Messages.FinanceManagement;

    public interface ISendPaymentEmailsApplicationService : IApplicationService,
        IUniversalConsumerService<SendPaymentReversalConfirmationEmailMessage>,
        IUniversalConsumerService<SendPaymentDueDateChangeEmailMessage>,
        IUniversalConsumerService<ScheduledSendPaymentConfirmationEmailMessage>,
        IUniversalConsumerService<SendCardExpiredEmailMessage>,
        IUniversalConsumerService<SendCardExpiringEmailMessage>,
        IUniversalConsumerService<SendPaymentConfirmationEmailMessage>,
        IUniversalConsumerService<SendPaymentDueReminderEmailMessage>,
        IUniversalConsumerService<SendPaymentFailureEmailMessage>,
        IUniversalConsumerService<SendPaymentMethodChangeEmailMessage>,
        IUniversalConsumerService<SendPendingPaymentEmailMessage>,
        IUniversalConsumerService<SendPrimaryPaymentMethodChangeEmailMessage>
    {
    }
}
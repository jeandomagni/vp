﻿namespace Ivh.Application.Email.Common.Interfaces
{
    using Core.Common.Dtos;
    using Ivh.Common.Base.Interfaces;
    using Ivh.Common.ServiceBus.Interfaces;
    using Ivh.Common.VisitPay.Messages.Communication;

    public interface ISendGridEmailEventApplicationService : IApplicationService,
        IUniversalConsumerService<SendGridEmailEventMessage>
    {
        void EmailEvent(SendGridEmailEventMessage message);
    }
}
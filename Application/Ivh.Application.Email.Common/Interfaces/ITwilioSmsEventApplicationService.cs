﻿namespace Ivh.Application.Email.Common.Interfaces
{
    using Dtos;
    using Ivh.Common.Base.Interfaces;
    using Ivh.Common.ServiceBus.Interfaces;
    using Ivh.Common.VisitPay.Messages.Communication;

    public interface ITwilioSmsEventApplicationService : IApplicationService,
        IUniversalConsumerService<TwilioSmsEventMessage>
    {
        void SmsEvent(TwilioSmsEventMessage message);
        SmsResponseResult SmsMessage(TwilioCallbackMessage message);
    }
}
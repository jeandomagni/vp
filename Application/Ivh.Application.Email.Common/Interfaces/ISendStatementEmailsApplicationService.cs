﻿namespace Ivh.Application.Email.Common.Interfaces
{
    using Ivh.Common.Base.Interfaces;
    using Ivh.Common.ServiceBus.Interfaces;
    using Ivh.Common.VisitPay.Messages.Communication;

    public interface ISendStatementEmailsApplicationService : IApplicationService,
        IUniversalConsumerService<StatementNotificationMessage>,
        IUniversalConsumerService<FinancePlanStatementNotificationMessage>
    {
    }
}

﻿using Ivh.Application.Email.Common.Dtos;
using System.Collections.Generic;

namespace Ivh.Application.Email.Common.Interfaces
{
    using Ivh.Common.Base.Interfaces;
    using Ivh.Common.VisitPay.Enums;

    public interface IEmailApplicationService : IApplicationService
    {
        IList<CommunicationDto> GetLastSentEmails(int limit);
        IList<CommunicationDto> GetUnsentCommunications(CommunicationMethodEnum communicationMethod);
    }
}

﻿namespace Ivh.Application.Email.Common.Interfaces
{
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Dtos;
    using Ivh.Common.Base.Interfaces;

    public interface IUserEmailApplicationService : IApplicationService
    {
        IList<CommunicationDto> GetByUserId(int userId);
        EmailActionResultDto ResendCommunication(int emailId, string toAddress, int vpGuarantorId, int sentByVisitPayUserId);
        CommunicationsResultsDto GetCommunications(CommunicationsFilterDto communicationsFilterDto, int page, int rows, int visitPayUserId);
        int GetCommunicationsTotals(int guarantorVisitPayUserId, CommunicationsFilterDto communicationsFilterDto);
        CommunicationEventResultsDto EmailEventsForEmailId(int emailId, int page, int rows, string sortField, string sortOrder);
        Task<byte[]> ExportCommunicationsAsync(CommunicationsFilterDto communicationsFilterDto, int visitPayUserId, string userName);
        byte[] ExportEmailEventsForEmailId(int emailId, string sortField, string sortOrder, string userName);
    }
}
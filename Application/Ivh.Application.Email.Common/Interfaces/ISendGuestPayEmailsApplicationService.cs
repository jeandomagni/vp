﻿namespace Ivh.Application.Email.Common.Interfaces
{
    using Ivh.Common.Base.Enums;
    using Ivh.Common.Base.Interfaces;
    using Ivh.Common.ServiceBus.Interfaces;
    using Ivh.Common.VisitPay.Enums;
    using Ivh.Common.VisitPay.Messages.Communication;

    public interface ISendGuestPayEmailsApplicationService : IApplicationService,
        IUniversalConsumerService<SendGuestPayPaymentReversalConfirmationEmailMessage>,
        IUniversalConsumerService<SendGuestPayPaymentConfirmationEmailMessage>,
        IUniversalConsumerService<SendGuestPayPaymentFailureEmailMessage>
    {
        void SendPaymentConfirmation(string toEmail, string locale, int sentToPaymentUnitId, string guarantorFirstName, string transactionId);
        void SendPaymentFailure(string toEmail, int sentToPaymentUnitId, string guarantorFirstName, string transactionId);
        void SendPaymentReversalNotification(string toEmail, int sendToPaymentUnitId, string guarantorLastName, string transactionId, PaymentTypeEnum paymentType);
    }
}
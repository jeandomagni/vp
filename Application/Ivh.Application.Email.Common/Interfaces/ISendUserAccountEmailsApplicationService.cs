namespace Ivh.Application.Email.Common.Interfaces
{
    using Ivh.Common.Base.Interfaces;
    using Ivh.Common.ServiceBus.Interfaces;
    using Ivh.Common.VisitPay.Messages.Communication;
    using Ivh.Common.VisitPay.Messages.Core;

    public interface ISendUserAccountEmailsApplicationService : 
        IApplicationService, 
        IUniversalConsumerService<SendUserProfileChangeEmailMessage>,
        IUniversalConsumerService<SendSmsValidationMessage>,
        IUniversalConsumerService<SendSmsDeactivateAlertEmailMessage>,
        IUniversalConsumerService<SendSmsActivateAlertEmailMessage>,
        IUniversalConsumerService<SendPostRegistrationWelcomeMessage>,
        IUniversalConsumerService<SendClientProfileChangeEmailMessage>,
        IUniversalConsumerService<SendAccountCredentialChangePatientEmailMessage>,
        IUniversalConsumerService<SendClientPasswordWithoutPinEmailMessage>,
        IUniversalConsumerService<SendClientResetPasswordEmailMessage>,
        IUniversalConsumerService<SendNewUserClientPasswordWithoutPinEmailMessage>,
        IUniversalConsumerService<SendNewUserInvitationEmailMessage>,
        IUniversalConsumerService<SendPasswordWithoutPinEmailMessage>,
        IUniversalConsumerService<SendUsernameReminderEmailMessage>,
        IUniversalConsumerService<SendAccountClosedCompleteMessage>,
        IUniversalConsumerService<SendAccountReactivationCompleteMessage>,
        IUniversalConsumerService<SendClientAccountLockoutEmailMessage>,
        IUniversalConsumerService<SendGuarantorAccountLockoutEmailMessage>,
        IUniversalConsumerService<SendVpccLoginLinkEmailMessage>,
        IUniversalConsumerService<SendVpccLoginLinkSmsMessage>,
        IUniversalConsumerService<SendPaperCreditAgreementMailMessage>
    {
    }
}
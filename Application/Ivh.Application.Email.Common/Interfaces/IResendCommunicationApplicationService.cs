﻿namespace Ivh.Application.Email.Common.Interfaces
{
    using System.Collections.Generic;
    using Ivh.Common.Base.Interfaces;
    using Ivh.Common.JobRunner.Common.Interfaces;
    using Ivh.Common.ServiceBus.Interfaces;
    using Ivh.Common.VisitPay.Enums;
    using Ivh.Common.VisitPay.Jobs;
    using Ivh.Common.VisitPay.Messages.Core;

    public interface IResendCommunicationApplicationService : IApplicationService,
        IUniversalConsumerService<ResendEmailMessage>,
        IJobRunnerService<QueueHeldEmailForResendJobRunner>
    {
        void QueueHeldEmailForResend(IList<CommunicationTypeEnum> emailTypes = null);
        void ResendCommunication(int communicationId, string toAddress = null, int? vpGuarantorId = null, int? sentByVisitPayUserId = null);

    }
}
﻿namespace Ivh.Application.Email.Common.Interfaces
{
    using Ivh.Common.Base.Interfaces;
    using Ivh.Common.ServiceBus.Interfaces;
    using Ivh.Common.VisitPay.Messages.Consolidation;

    public interface ISendConsolidationEmailsApplicationService : IApplicationService,
        IUniversalConsumerService<ConsolidationHouseholdAcceptedToManagedMessage>,
        IUniversalConsumerService<ConsolidationHouseholdRequestAcceptedByManagedToManagingMessage>,
        IUniversalConsumerService<ConsolidationHouseholdRequestAcceptedByManagedWithFpToManagingMessage>,
        IUniversalConsumerService<ConsolidationHouseholdRequestCancelledByManagedToManagingMessage>,
        IUniversalConsumerService<ConsolidationHouseholdRequestCancelledByManagedToManagingWithPendingFpTermsMessage>,
        IUniversalConsumerService<ConsolidationHouseholdRequestCancelledByManagingToManagedMessage>,
        IUniversalConsumerService<ConsolidationHouseholdRequestDeclinedToManagedMessage>,
        IUniversalConsumerService<ConsolidationHouseholdRequestDeclinedToManagingMessage>,
        IUniversalConsumerService<ConsolidationHouseholdRequestToManagedMessage>,
        IUniversalConsumerService<ConsolidationHouseholdRequestToManagingMessage>,
        IUniversalConsumerService<ConsolidationLinkageCancelledByManagedToManagedMessage>,
        IUniversalConsumerService<ConsolidationLinkageCancelledByManagedToManagedWithFpMessage>,
        IUniversalConsumerService<ConsolidationLinkageCancelledByManagedToManagingMessage>,
        IUniversalConsumerService<ConsolidationLinkageCancelledByManagingToManagedMessage>,
        IUniversalConsumerService<ConsolidationLinkageCancelledByManagingToManagedWithFpMessage>,
        IUniversalConsumerService<ConsolidationLinkageCancelledByManagingToManagingMessage>
    {
        void ConsolidationHouseholdRequestToManaging(int managingVpGuarantorId, int managedVpGuarantorId, string consolidationRequestExpiryDay);
        void ConsolidationHouseholdRequestToManaged(int managingVpGuarantorId, int managedVpGuarantorId, string consolidationRequestExpiryDay);
        void ConsolidationHouseholdAcceptedToManaged(int managingVpGuarantorId, int managedVpGuarantorId);
        void ConsolidationHouseholdRequestAcceptedByManagedToManaging(int managingVpGuarantorId, int managedVpGuarantorId);
        void ConsolidationHouseholdRequestAcceptedByManagedWithFpToManaging(int managingVpGuarantorId, int managedVpGuarantorId, string consolidationRequestExpiryDay2);
        void ConsolidationHouseholdRequestDeclinedToManaged(int managingVpGuarantorId, int managedVpGuarantorId);
        void ConsolidationHouseholdRequestDeclinedToManaging(int managingVpGuarantorId, int managedVpGuarantorId);
        void ConsolidationHouseholdRequestCancelledToManaging(int managingVpGuarantorId, int managedVpGuarantorId);
        void ConsolidationHouseholdRequestCancelledByManagingToManaged(int managingVpGuarantorId, int managedVpGuarantorId);
        void ConsolidationHouseholdRequestCancelledByManagedToManagingWithPendingFpTerms(int managingVpGuarantorId, int managedVpGuarantorId);
        void ConsolidationLinkageCancelledByManagingToManaging(int managingVpGuarantorId, int managedVpGuarantorId);
        void ConsolidationLinkageCancelledByManagingToManaged(int managingVpGuarantorId, int managedVpGuarantorId);
        void ConsolidationLinkageCancelledByManagingToManagedWithFp(int managingVpGuarantorId, int managedVpGuarantorId);
        void ConsolidationLinkageCancelledByManagedToManaging(int managingVpGuarantorId, int managedVpGuarantorId);
        void ConsolidationLinkageCancelledByManagedToManaged(int managingVpGuarantorId, int managedVpGuarantorId);
        void ConsolidationLinkageCancelledByManagedToManagedWithFp(int managingVpGuarantorId, int managedVpGuarantorId);
    }
}
using Ivh.Application.Email.Common.Dtos;
using System.Collections.Generic;

namespace Ivh.Application.Email.Common.Interfaces
{
    using Ivh.Common.Base.Interfaces;

    public interface ICommunicationExportApplicationService : IApplicationService
    {
        void ExportUnsentPaperCommunications();
    }
}
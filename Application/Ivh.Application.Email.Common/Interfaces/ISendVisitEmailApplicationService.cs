﻿namespace Ivh.Application.Email.Common.Interfaces
{
    using System.Collections.Generic;
    using Ivh.Common.Base.Interfaces;
    using Ivh.Common.ServiceBus.Interfaces;
    using Ivh.Common.VisitPay.Messages.Core;
    using Ivh.Common.VisitPay.Messages.FinanceManagement;

    public interface ISendVisitEmailApplicationService : IApplicationService,
        IUniversalConsumerService<SendFinalPastDueNotificationEmailMessage>,
        IUniversalConsumerService<SendPastDueVisitsEmailMessage>,
        IUniversalConsumerService<SendPendingUncollectableEmailMessage>,
        IUniversalConsumerService<NewVisitLoadedMessage>,
        IUniversalConsumerService<SendTextToPayPaymentOptionMessage>
    {
        bool VisitsPastDue(int vpGuarantorId);
        bool PendingUncollectable(int vpGuarantorId);
        bool FinalPastDueNotification(int vpGuarantorId, IList<int> visitIds, bool isOnFinancePlan);
        void NewVisitLoaded(int vpGuarantorId);
    }
}

﻿namespace Ivh.Application.Email.Common.Interfaces
{
    using System;
    using Ivh.Common.Base.Enums;
    using Ivh.Common.Base.Interfaces;
    using Ivh.Common.ServiceBus.Interfaces;
    using Ivh.Common.VisitPay.Enums;
    using Ivh.Common.VisitPay.Messages.Communication;

    public interface ISendClientSupportRequestEmailsApplicationService : IApplicationService,
        IUniversalConsumerService<SendClientSupportRequestStatusNotificationEmailMessage>,
        IUniversalConsumerService<SendClientSupportRequestMessageNotificationEmailMessage>,
        IUniversalConsumerService<SendClientSupportRequestMessageAdminNotificationEmailMessage>,
        IUniversalConsumerService<SendClientSupportRequestConfirmationEmailMessage>,
        IUniversalConsumerService<SendClientSupportRequestAdminNotificationEmailMessage>
    {
        void ClientSupportRequestConfirmation(int visitPayUserId, string clientSupportRequestDisplayId);
        void ClientSupportRequestAdminNotification(string clientSupportRequestDisplayId);
        void ClientSupportRequestMessageAdminNotification(string clientSupportRequestDisplayId, DateTime insertDate);
        void ClientSupportRequestMessageNotification(int visitPayUserId, string clientSupportRequestDisplayId, DateTime insertDate);
        void ClientSupportRequestStatusNotification(int visitPayUserId, string clientSupportRequestDisplayId, DateTime insertDate, ClientSupportRequestStatusEnum status);
    }
}
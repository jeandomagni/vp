namespace Ivh.Application.Email.Common.Interfaces
{
    using Ivh.Common.Base.Interfaces;
    using Ivh.Common.ServiceBus.Interfaces;
    using Ivh.Common.VisitPay.Messages.Communication;
    using Ivh.Common.VisitPay.Messages.FinanceManagement;

    public interface ISendFinancePlanEmailsApplicationService : IApplicationService,
        IUniversalConsumerService<SendFinancePlanAdditionalBalanceEmailMessage>,
        IUniversalConsumerService<SendFinancePlanClosedEmailMessage>,
        IUniversalConsumerService<SendFinancePlanConfirmationEmailMessage>,
        IUniversalConsumerService<SendFinancePlanConfirmationSimpleTermsEmailMessage>,
		IUniversalConsumerService<SendFinancePlanCreationEmailMessage>,
        IUniversalConsumerService<SendFinancePlanModificationEmailMessage>,
        IUniversalConsumerService<SendFinancePlanTerminationEmailMessage>,
        IUniversalConsumerService<SendFinancePlanVisitSuspendedEmailMessage>,
        IUniversalConsumerService<SendFinancePlanVisitNotEligibleEmailMessage>,
        IUniversalConsumerService<SendVpccFinancePlanCreationEmailMessage>,
		IUniversalConsumerService<SendVpccFinancePlanCreationSmsMessage>,
        IUniversalConsumerService<SendVpccFinancePlanModificationEmailMessage>
    {
    }
}
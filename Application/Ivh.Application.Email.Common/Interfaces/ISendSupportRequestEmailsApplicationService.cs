namespace Ivh.Application.Email.Common.Interfaces
{
    using Ivh.Common.Base.Interfaces;
    using Ivh.Common.ServiceBus.Interfaces;
    using Ivh.Common.VisitPay.Messages.Communication;

    public interface ISendSupportRequestEmailsApplicationService : IApplicationService, 
        IUniversalConsumerService<SendSupportRequestUpdatedEmailMessage>, 
        IUniversalConsumerService<SendSupportRequestCreatedEmailMessage>
    {
        void SupportRequestConfirmation(int vpGuarantorId, string caseNumber);

        void SupportRequestUpdate(int vpGuarantorId, string caseNumber, string submittedDate, string modificationStatus);
    }
}
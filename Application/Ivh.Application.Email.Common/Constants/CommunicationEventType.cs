﻿namespace Ivh.Application.Email.Common.Constants
{
    public class CommunicationEventType
    {
        public const string Bounce = "bounce";
        public const string Open = "open";
        public const string Click = "click";
        public const string Dropped = "dropped";
        public const string SpamReport = "spamreport";
        public const string Unsubscribe = "unsubscribe";
        public const string GroupUnsubscribe = "group_unsubscribe";
        public const string GroupResubscribe = "group_resubscribe";
        public const string Delivered = "delivered";
        public const string Processed = "processed";
        public const string Deferred = "deferred";
        public const string Accepted = "accepted";
        public const string Queued = "queued";
        public const string Sending = "sending";
        public const string Sent = "sent";
        public const string Receiving = "receiving";
        public const string Received = "received";
        public const string Undelivered = "undelivered";
        public const string Failed = "failed";
    }
}
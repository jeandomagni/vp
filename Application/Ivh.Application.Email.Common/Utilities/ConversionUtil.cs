﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ivh.Application.Email.Common.Utilities
{
    using Dtos;

    public class ConversionUtil
    {

        public static void SetEventDateFromTimeStamp(CommunicationEventDto communicationEventDto)
        {
            int timestamp;
            if (!string.IsNullOrWhiteSpace(communicationEventDto.Timestamp) && int.TryParse(communicationEventDto.Timestamp, out timestamp))
            {
                DateTime eventDateTime = new DateTime(1970, 1, 1, 0, 0, 0).AddSeconds(timestamp);
                DateTime.SpecifyKind(eventDateTime, DateTimeKind.Utc);
                communicationEventDto.EventDateTime = eventDateTime;              
            }
        }
    }
}

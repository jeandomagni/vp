﻿namespace Ivh.Application.Settings.Common.Dtos
{
    public class ClientCultureInfoDto
    {
        public int LCID { get; set; }
        public string NativeName { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
    }
}
﻿namespace Ivh.Application.Settings.Common.Dtos
{
    using Ivh.Common.VisitPay.Enums;

    public class FinancePlanIncentiveConfigurationDto
    {
        public FinancePlanIncentiveInterestRefundMethodEnum FinancePlanIncentiveInterestRefundMethod { get; set; }
        public object Value { get; set; }
    }
}
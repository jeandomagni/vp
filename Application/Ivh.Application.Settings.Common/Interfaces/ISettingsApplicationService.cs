﻿namespace Ivh.Application.Settings.Common.Interfaces
{
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Ivh.Common.Base.Interfaces;

    public interface ISettingsApplicationService : IApplicationService
    {
        Task<IReadOnlyDictionary<string, string>> GetAllSettingsAsync();
        Task<IReadOnlyDictionary<string, string>> GetAllManagedSettingsAsync();
        Task<IReadOnlyDictionary<string, string>> GetAllEditableManagedSettingsAsync();
        Task<bool> InvalidateCacheAsync();
        Task<bool> OverrideManagedSettingAsync(string key, string overrideValue);
        Task<bool> ClearManagedSettingOverridesAsync();
        Task<bool> ClearManagedSettingOverrideAsync(string key);
        string GetManagedSettingsHash();
        string GetEditableManagedSettingsHash();
    }
}

﻿namespace Ivh.Application.SecureCommunication.Mappings
{
    using AutoMapper;
    using Common.Dtos;
    using Domain.SecureCommunication.ClientSupportRequests.Entities;
    using Domain.SecureCommunication.GuarantorSupportRequests.Entities;
    using Domain.SecureCommunication.GuarantorSupportRequests.Interfaces;

    public class AutomapperProfile : Profile
    {
        public AutomapperProfile()
        {
            // client support request
            this.CreateMap<ClientSupportRequest, ClientSupportRequestDto>()
                .ForMember(dest => dest.TargetVpGuarantorId, opts => opts.MapFrom(src => src.TargetVpGuarantor == null ? (int?) null : src.TargetVpGuarantor.VpGuarantorId));

            this.CreateMap<ClientSupportRequestMessage, ClientSupportRequestMessageDto>();

            this.CreateMap<ClientSupportRequestMessageAttachment, ClientSupportRequestMessageAttachmentDto>()
                .ForMember(dest => dest.FileContent, opts => opts.Ignore());

            this.CreateMap<ClientSupportRequestStatus, ClientSupportRequestStatusDto>();

            this.CreateMap<ClientSupportTopic, ClientSupportTopicDto>()
                .ForMember(dest => dest.Templates, opts => opts.Ignore());

            this.CreateMap<ClientSupportRequestFilterDto, ClientSupportRequestFilter>();

            this.CreateMap<ClientSupportRequestResults, ClientSupportRequestResultsDto>();

            // guarantor support request
            this.CreateMap<SupportRequest, SupportRequestDto>();
            this.CreateMap<SupportRequestDto, SupportRequest>();

            this.CreateMap<SupportRequestMessage, SupportRequestMessageDto>();
            this.CreateMap<SupportRequestMessageDto, SupportRequestMessage>();

            this.CreateMap<SupportRequestMessageAttachment, SupportRequestMessageAttachmentDto>();
            this.CreateMap<SupportRequestMessageAttachmentDto, SupportRequestMessageAttachment>();

            this.CreateMap<SupportRequestStatus, SupportRequestStatusDto>();
            this.CreateMap<SupportRequestStatusDto, SupportRequestStatus>();

            this.CreateMap<SupportTopic, SupportTopicDto>()
                .ForMember(dest => dest.Templates, opts => opts.Ignore());

            this.CreateMap<SupportTopicDto, SupportTopic>()
                .ForMember(dest => dest.Templates, opts => opts.Ignore());

            this.CreateMap<SupportRequestFilterDto, SupportRequestFilter>();

            this.CreateMap<SupportRequestResults, SupportRequestResultsDto>();

            this.CreateMap<SupportTemplate, SupportTemplateDto>();
            this.CreateMap<SupportTemplateDto, SupportTemplate>();
        }
    }
}

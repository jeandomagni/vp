﻿namespace Ivh.Application.SecureCommunication
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using AutoMapper;
    using Common.Dtos;
    using Common.Interfaces;
    using Core.Common.Interfaces;
    using Domain.FinanceManagement.FinancePlan.Interfaces;
    using Domain.FinanceManagement.Statement.Interfaces;
    using Domain.Guarantor.Entities;
    using Domain.Guarantor.Interfaces;
    using Domain.SecureCommunication.GuarantorSupportRequests.Entities;
    using Domain.SecureCommunication.GuarantorSupportRequests.Interfaces;
    using Domain.Settings.Interfaces;
    using FinanceManagement.Common.Interfaces;
    using Ivh.Common.Base.Constants;
    using Ivh.Common.Base.Utilities.Extensions;
    using Ivh.Common.VisitPay.Constants;
    using User.Common.Dtos;

    public class SupportTemplateApplicationService : ISupportTemplateApplicationService
    {
        private readonly Lazy<ISupportTemplateService> _supportTemplateService;
        private readonly Lazy<IGuarantorService> _guarantorService;
        private readonly Lazy<IFinancialDataSummaryApplicationService> _financialDataSummaryApplicationService;
        private readonly Lazy<IVisitPayUserApplicationService> _visitPayUserApplicationService;
        private readonly Lazy<IClientService> _clientService;
        private readonly Lazy<IStatementDateService> _statementDateService;
        private readonly Lazy<IInterestService> _interestService;
        private readonly Lazy<ISupportTopicApplicationService> _supportTopicApplicationService;

        public SupportTemplateApplicationService(
            Lazy<ISupportTemplateService> supportTemplateService,
            Lazy<IGuarantorService> guarantorService,
            Lazy<IFinancialDataSummaryApplicationService> financialDataSummaryApplicationService,
            Lazy<IVisitPayUserApplicationService> visitPayUserApplicationService,
            Lazy<IClientService> clientService,
            Lazy<IStatementDateService> statementDateService,
            Lazy<IInterestService> interestService,
            Lazy<ISupportTopicApplicationService> supportTopicApplicationService
        )
        {
            this._supportTemplateService = supportTemplateService;
            this._guarantorService = guarantorService;
            this._visitPayUserApplicationService = visitPayUserApplicationService;
            this._clientService = clientService;
            this._statementDateService = statementDateService;
            this._interestService = interestService;
            this._financialDataSummaryApplicationService = financialDataSummaryApplicationService;
            this._supportTopicApplicationService = supportTopicApplicationService;
        }

        public void DeleteSupportTemplate(int supportTemplateId, int clientVpUserId, bool userCanManageGlobalTemplates)
        {
            SupportTemplate supportTemplate = this._supportTemplateService.Value.GetSupportTemplate(supportTemplateId);

            //Check if user can delete the template
            if (supportTemplate != null && this.UserCanManageTemplate(supportTemplate, clientVpUserId, userCanManageGlobalTemplates))
            {
                this._supportTemplateService.Value.DeleteSupportTemplate(supportTemplate);
            }
        }

        public SupportTemplateDto GetSupportTemplate(int supportTemplateId, bool userCanManageGlobalTemplates)
        {
            SupportTemplate supportTemplate = this._supportTemplateService.Value.GetSupportTemplate(supportTemplateId);

            //Check if the template can be managed by this user
            if (!supportTemplate.VisitPayUserId.HasValue && !userCanManageGlobalTemplates)
            {
                //User is requesting a global template, but doesn't have permission to manage them. Return empty template
                supportTemplate = new SupportTemplate();
            }

            return Mapper.Map<SupportTemplateDto>(supportTemplate);
        }

        public IList<SupportTemplateDto> GetSupportTemplates(int clientVpUserId, int guarantorVpUserId)
        {
            int guarantorId = this._guarantorService.Value.GetGuarantorId(guarantorVpUserId);
            Guarantor guarantor = this._guarantorService.Value.GetGuarantor(guarantorId);
            VisitPayUserDto clientUserDto = this._visitPayUserApplicationService.Value.FindById(clientVpUserId.ToString());

            if (guarantor == null && clientUserDto == null)
            {
                return new List<SupportTemplateDto>();
            }

            IList<SupportTemplate> templatesForReplacements = this._supportTemplateService.Value.GetSupportTemplates(clientVpUserId);
            IList<SupportTemplateDto> replacedTemplates = this.ReplaceTags(guarantor, clientUserDto, templatesForReplacements);

            //Sorting: Global first (userID will be null), then by title
            replacedTemplates = replacedTemplates.OrderBy(x => x.VisitPayUserId).ThenBy(x => x.Title).ToList();

            return replacedTemplates;
        }

        public SupportTemplateDto SaveSupportTemplate(SupportTemplateDto supportTemplate, int clientVpUserId, bool userCanManageGlobalTemplates)
        {
            SupportTemplate templateToSave = Mapper.Map<SupportTemplate>(supportTemplate);

            //Check if we are saving an existing template
            if (supportTemplate.SupportTemplateId > 0)
            {
                //Existing template, make sure user is allowed to edit it
                SupportTemplate existingTemplate = this._supportTemplateService.Value.GetSupportTemplate(supportTemplate.SupportTemplateId);
                if (!this.UserCanManageTemplate(existingTemplate, clientVpUserId, userCanManageGlobalTemplates))
                {
                    //User does not have access to edit the template
                    return null;
                }
                else
                {
                    //User can edit the template, so update the existing template with the new values
                    existingTemplate.VisitPayUserId = supportTemplate.VisitPayUserId;
                    existingTemplate.Title = supportTemplate.Title;
                    existingTemplate.Content = supportTemplate.Content;
                    templateToSave = existingTemplate;
                }
            }

            this._supportTemplateService.Value.SaveSupportTemplate(templateToSave);
            return Mapper.Map<SupportTemplateDto>(templateToSave);
        }

        public IList<string> GetCommonTemplateReplacementTags()
        {
            IList<string> commonTags = typeof(ClientSupportTemplateReplacementTags).GetAllPublicConstantValues<string>();
            commonTags = commonTags.OrderBy(t => t).ToList();
            return commonTags;
        }

        private bool UserCanManageTemplate(SupportTemplate template, int clientVpUserId, bool userCanManageGlobalTemplates)
        {
            if (template.VisitPayUserId.HasValue && template.VisitPayUserId.Value == clientVpUserId)
            {
                //Template belongs to the user
                return true;
            }
            else if (!template.VisitPayUserId.HasValue && userCanManageGlobalTemplates)
            {
                //Global template, and user has access to manage
                return true;
            }
            else
            {
                //Template doesn't belong to user, or is global template and user doesn't have access to manage
                return false;
            }
        }
        
        private IList<SupportTemplateDto> ReplaceTags(Guarantor guarantor, VisitPayUserDto clientUserDto, IList<SupportTemplate> supportTemplates)
        {
            IList<SupportTemplateDto> supportTemplateDtos = new List<SupportTemplateDto>();

            IDictionary<string, string> replacementTags = this._supportTopicApplicationService.Value.PopulateTemplateReplacementTags(guarantor, clientUserDto);

            foreach (SupportTemplate supportTemplate in supportTemplates)
            {
                SupportTemplateDto supportTemplateDto = Mapper.Map<SupportTemplateDto>(supportTemplate);

                replacementTags.ForEach(x =>
                {
                    supportTemplateDto.Content = supportTemplateDto.Content.Replace(x.Key, x.Value);
                });

                supportTemplateDtos.Add(supportTemplateDto);
            }

            return supportTemplateDtos;
        }
    }
}

﻿namespace Ivh.Application.SecureCommunication.Modules
{
    using Autofac;
    using Common.Interfaces;

    public class Module : Autofac.Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<ClientSupportRequestApplicationService>().As<IClientSupportRequestApplicationService>();

            builder.RegisterType<SupportRequestApplicationService>().As<ISupportRequestApplicationService>();
            builder.RegisterType<SupportTopicApplicationService>().As<ISupportTopicApplicationService>();
            builder.RegisterType<SupportTemplateApplicationService>().As<ISupportTemplateApplicationService>();
            builder.RegisterType<ChatHistoryExportApplicationService>().As<IChatHistoryExportApplicationService>();
        }
    }
}

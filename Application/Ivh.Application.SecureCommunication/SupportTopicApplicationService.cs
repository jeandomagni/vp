﻿namespace Ivh.Application.SecureCommunication
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web;
    using AutoMapper;
    using Common.Dtos;
    using Common.Interfaces;
    using Content.Common.Dtos;
    using Core.Common.Interfaces;
    using Domain.Content.Entities;
    using Domain.Content.Interfaces;
    using Domain.Guarantor.Entities;
    using Domain.Guarantor.Interfaces;
    using Domain.FinanceManagement.FinancePlan.Interfaces;
    using Domain.SecureCommunication.GuarantorSupportRequests.Entities;
    using Domain.SecureCommunication.GuarantorSupportRequests.Interfaces;
    using Domain.Settings.Entities;
    using Domain.Settings.Interfaces;
    using FinanceManagement.Common.Dtos;
    using FinanceManagement.Common.Interfaces;
    using Ivh.Common.Base.Utilities.Extensions;
    using Ivh.Common.VisitPay.Constants;
    using Ivh.Common.VisitPay.Enums;
    using Ivh.Common.VisitPay.Strings;
    using User.Common.Dtos;

    public class SupportTopicApplicationService : ISupportTopicApplicationService
    {
        private readonly Lazy<IContentService> _contentService;
        private readonly Lazy<IGuarantorService> _guarantorService;
        private readonly Lazy<ISupportTopicService> _supportTopicService;
        private readonly Lazy<IFinancialDataSummaryApplicationService> _financialDataSummaryApplicationService;
        private readonly Lazy<IVisitPayUserApplicationService> _visitPayUserApplicationService;
        private readonly Lazy<IClientService> _clientService;
        private readonly Lazy<IInterestService> _interestService;

        public SupportTopicApplicationService(
            Lazy<IContentService> contentService,
            Lazy<IGuarantorService> guarantorService,
            Lazy<ISupportTopicService> supportTopicService,
            Lazy<IFinancialDataSummaryApplicationService> financialDataSummaryApplicationService,
            Lazy<IVisitPayUserApplicationService> visitPayUserApplicationService,
            Lazy<IClientService> clientService,
            Lazy<IInterestService> interestService)
        {
            this._contentService = contentService;
            this._guarantorService = guarantorService;
            this._supportTopicService = supportTopicService;
            this._financialDataSummaryApplicationService = financialDataSummaryApplicationService;
            this._visitPayUserApplicationService = visitPayUserApplicationService;
            this._clientService = clientService;
            this._interestService = interestService;
        }

        public IList<SupportTopicGroupItemDto> GetSupportTopicsGrouped()
        {
            IList<SupportTopicDto> supportTopicsDto = Mapper.Map<IList<SupportTopicDto>>(this._supportTopicService.Value.GetSupportTopics());
            IList<SupportTopicGroupItemDto> groupedItemsDto = new List<SupportTopicGroupItemDto>();

            foreach (SupportTopicDto supportTopicDto in supportTopicsDto.Where(x => !x.ParentSupportTopicId.HasValue).OrderBy(x => x.SortOrder))
            {
                SupportTopicGroupItemDto groupedItemDto = new SupportTopicGroupItemDto
                {
                    SupportTopicId = supportTopicDto.SupportTopicId,
                    SupportTopicName = supportTopicDto.SupportTopicName
                };

                SupportTopicDto dto = supportTopicDto;
                foreach (SupportTopicDto childTopicDto in supportTopicsDto.Where(x => x.ParentSupportTopicId.GetValueOrDefault(0) == dto.SupportTopicId).OrderBy(x => x.SortOrder))
                {
                    SupportTopicGroupItemDto childSupportTopicGroupDto = new SupportTopicGroupItemDto
                    {
                        SupportTopicId = childTopicDto.SupportTopicId,
                        SupportTopicName = childTopicDto.SupportTopicName,
                    };

                    groupedItemDto.Items.Add(childSupportTopicGroupDto);
                }

                groupedItemsDto.Add(groupedItemDto);
            }

            return groupedItemsDto;
        }

        public IList<SupportTopicGroupItemDto> GetSupportTopicsGroupedTemplates(int visitPayUserId, int clientVisitPayUserId)
        {
            int guarantorId = this._guarantorService.Value.GetGuarantorId(visitPayUserId);
            Guarantor guarantor = this._guarantorService.Value.GetGuarantor(guarantorId);
            VisitPayUserDto clientUserDto = this._visitPayUserApplicationService.Value.FindById(clientVisitPayUserId.ToString());

            if (guarantor == null && clientUserDto == null)
            {
                return new List<SupportTopicGroupItemDto>();
            }

            IList<SupportTopicDto> supportTopicsDto = this.ReplaceTags(guarantor, clientUserDto, this._supportTopicService.Value.GetSupportTopics());

            IList<SupportTopicGroupItemDto> groupedItemsDto = new List<SupportTopicGroupItemDto>();

            foreach (SupportTopicDto supportTopicDto in supportTopicsDto.Where(x => !x.ParentSupportTopicId.HasValue).OrderBy(x => x.SortOrder))
            {
                SupportTopicGroupItemDto groupedItemDto = new SupportTopicGroupItemDto
                {
                    SupportTopicId = supportTopicDto.SupportTopicId,
                    SupportTopicName = supportTopicDto.SupportTopicName
                };

                SupportTopicDto dto = supportTopicDto;
                foreach (SupportTopicDto childTopicDto in supportTopicsDto.Where(x => x.ParentSupportTopicId.GetValueOrDefault(0) == dto.SupportTopicId).OrderBy(x => x.SortOrder))
                {
                    if (!childTopicDto.Templates.Any())
                    {
                        continue;
                    }

                    SupportTopicGroupItemDto childSupportTopicGroupDto = new SupportTopicGroupItemDto
                    {
                        SupportTopicId = childTopicDto.SupportTopicId,
                        SupportTopicName = childTopicDto.SupportTopicName,
                    };

                    childSupportTopicGroupDto.Templates.AddRange(childTopicDto.Templates.OrderBy(x => x.ContentTitle).Select(x => new SupportTopicTemplateDto
                    {
                        Content = HttpUtility.HtmlDecode(x.ContentBody),
                        Title = x.ContentTitle
                    }).ToList());

                    groupedItemDto.Items.Add(childSupportTopicGroupDto);
                }

                if (groupedItemDto.Items.Any())
                {
                    groupedItemsDto.Add(groupedItemDto);
                }
            }

            return groupedItemsDto;
        }

        private IList<SupportTopicDto> ReplaceTags(Guarantor guarantor, VisitPayUserDto clientUserDto, IList<SupportTopic> supportTopics)
        {
            IList<SupportTopicDto> supportTopicsDto = new List<SupportTopicDto>();

            IDictionary<string, string> replacementTags = this.PopulateTemplateReplacementTags(guarantor, clientUserDto);

            foreach (SupportTopic supportTopic in supportTopics)
            {
                SupportTopicDto supportTopicDto = Mapper.Map<SupportTopicDto>(supportTopic);
                supportTopicDto.Templates = new List<CmsVersionDto>();

                foreach (CmsRegion cmsRegion in supportTopic.Templates)
                {
                    CmsVersionDto cmsVersionDto = Mapper.Map<CmsVersionDto>(this._contentService.Value.GetCurrentVersionAsync((CmsRegionEnum)cmsRegion.CmsRegionId, true).Result);
                    if (cmsVersionDto == null)
                    {
                        continue;
                    }

                    replacementTags.ForEach(x =>
                    {
                        cmsVersionDto.ContentBody = cmsVersionDto.ContentBody.Replace(x.Key, x.Value);
                    });

                    supportTopicDto.Templates.Add(cmsVersionDto);
                }

                supportTopicsDto.Add(supportTopicDto);
            }

            return supportTopicsDto;
        }

        public IDictionary<string, string> PopulateTemplateReplacementTags(Guarantor guarantor, VisitPayUserDto clientUserDto)
        {
            #region payment due day formatting

            string paymentDueDay = guarantor.PaymentDueDay.ToString();

            if (guarantor.PaymentDueDay == 1 || guarantor.PaymentDueDay == 21 || guarantor.PaymentDueDay == 31)
                paymentDueDay += "st";
            else if (guarantor.PaymentDueDay == 2 || guarantor.PaymentDueDay == 22)
                paymentDueDay += "nd";
            else if (guarantor.PaymentDueDay == 3 || guarantor.PaymentDueDay == 23)
                paymentDueDay += "rd";
            else if ((guarantor.PaymentDueDay >= 4 && guarantor.PaymentDueDay <= 20) || (guarantor.PaymentDueDay >= 24 && guarantor.PaymentDueDay <= 30))
                paymentDueDay += "th";
            else if (guarantor.PaymentDueDay == -1)
                paymentDueDay = "last day";

            #endregion

            FinancialDataSummaryDto summary = this._financialDataSummaryApplicationService.Value.GetFinancialDataSummaryByGuarantor(guarantor.VpGuarantorId);
            
            string clientUserFirstName = !string.IsNullOrEmpty(clientUserDto.FirstName) ? clientUserDto.FirstName : "%INPUT: Client User - First Name%";
            string guarantorFullName = guarantor.User.FirstNameLastName;
            string guarantorFirstName = guarantor.User.FirstName;
            string nextStatementDate = guarantor.NextStatementDate?.ToString(Format.DateFormat) ?? string.Empty;
            string nextPaymentDate = summary.StatementNextPaymentDueDate?.ToString(Format.DateFormat) ?? string.Empty;
            string statementedTotalBalance = summary.TotalBalance.FormatCurrency();
            int maxDurationForOfferType1 = this._interestService.Value.GetMaxDurationRangeEnd(FinancePlanOfferSetTypeEnum.GuarantorOffers);

            IDictionary<string, string> replacementTags = new Dictionary<string, string>();

            //Common replacement tags for templates
            replacementTags.Add(ClientSupportTemplateReplacementTags.ClientUserFirstName, clientUserFirstName);
            replacementTags.Add(ClientSupportTemplateReplacementTags.GuarantorFullName, guarantorFullName);
            replacementTags.Add(ClientSupportTemplateReplacementTags.GuarantorFirstName, guarantorFirstName);
            replacementTags.Add(ClientSupportTemplateReplacementTags.GuarantorNextStatementDate, nextStatementDate);
            replacementTags.Add(ClientSupportTemplateReplacementTags.GuarantorPaymentDay, paymentDueDay);
            replacementTags.Add(ClientSupportTemplateReplacementTags.GuarantorNextPaymentDate, nextPaymentDate);
            replacementTags.Add(ClientSupportTemplateReplacementTags.PatientCurrentBalance, statementedTotalBalance);

            //Special case replacement tags for templates
            replacementTags.Add("<br />", Environment.NewLine);
            replacementTags.Add("<BR />", Environment.NewLine);
            replacementTags.Add("<br/>", Environment.NewLine);
            replacementTags.Add("<BR/>", Environment.NewLine);
            replacementTags.Add("<br>", Environment.NewLine);
            replacementTags.Add("<<MaxDurationForOfferType1>>", maxDurationForOfferType1.ToString());

            // conditional replacements
            Client client = this._clientService.Value.GetClient();
            string conditionalRegistrationPendingDaysText = client.RegistrationPendingDays > 0 ? string.Format("You will have the ability to view your visits in approximately {0} business days from enrollment.", client.RegistrationPendingDays) : string.Empty;
            replacementTags.Add("%ConditionalRegistrationPendingDaysText%", conditionalRegistrationPendingDaysText);

            return replacementTags;
        }
    }
}

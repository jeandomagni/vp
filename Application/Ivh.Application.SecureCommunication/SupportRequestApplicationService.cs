﻿namespace Ivh.Application.SecureCommunication
{
    using AutoMapper;
    using Base.Common.Interfaces;
    using Common.Dtos;
    using Common.Interfaces;
    using Core.Common.Dtos;
    using Core.Common.Interfaces;
    using Core.Common.Utilities.Builders;
    using Domain.Content.Interfaces;
    using Domain.Core.TreatmentLocation.Entities;
    using Domain.Core.TreatmentLocation.Interfaces;
    using Domain.Guarantor.Entities;
    using Domain.Guarantor.Interfaces;
    using Domain.Logging.Interfaces;
    using Domain.SecureCommunication.GuarantorSupportRequests.Entities;
    using Domain.SecureCommunication.GuarantorSupportRequests.Interfaces;
    using Domain.User.Entities;
    using Domain.Visit.Entities;
    using Domain.Visit.Interfaces;
    using Ionic.Zip;
    using Ivh.Application.Base.Services;
    using Ivh.Common.Base.Utilities.Extensions;
    using Ivh.Common.Data;
    using Ivh.Common.Data.Connection.Connections;
    using Ivh.Common.Data.Interfaces;
    using Ivh.Common.ServiceBus.Attributes;
    using Ivh.Common.ServiceBus.Enums;
    using Ivh.Common.VisitPay.Constants;
    using Ivh.Common.VisitPay.Enums;
    using Ivh.Common.VisitPay.Messages.Communication;
    using Ivh.Common.VisitPay.Messages.Malware;
    using Ivh.Common.VisitPay.Strings;
    using MassTransit;
    using Newtonsoft.Json;
    using NHibernate;
    using OfficeOpenXml;
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Threading.Tasks;
    using Domain.Core.Malware.Interfaces;
    using User.Common.Dtos;

    public class SupportRequestApplicationService : ApplicationService, ISupportRequestApplicationService
    {
        private const string NewSupportRequestMetricKey = "SupportRequest.New";

        private readonly Lazy<IContentService> _contentService;
        private readonly Lazy<IGuarantorService> _guarantorService;
        private readonly Lazy<ISupportRequestService> _supportRequestService;
        private readonly Lazy<ISupportTopicService> _supportTopicService;
        private readonly Lazy<ITreatmentLocationService> _treatmentLocationService;
        private readonly Lazy<IVisitService> _visitService;
        private readonly Lazy<IVisitPayUserApplicationService> _visitPayUserApplicationService;
        private readonly Lazy<Ivh.Common.ServiceBus.Interfaces.IBus> _bus;
        private readonly Lazy<IMetricsProvider> _metricsProvider;
        private readonly Lazy<IImageService> _imageService;
        private readonly Lazy<IMalwareScanningProvider> _malwareScanningProvider;
        private readonly ISession _session;        

        public SupportRequestApplicationService(
            Lazy<IApplicationServiceCommonService> applicationServiceCommonService,
            Lazy<IContentService> contentService,
            Lazy<IGuarantorService> guarantorService,
            Lazy<ISupportRequestService> supportRequestService,
            Lazy<ISupportTopicService> supportTopicService,
            Lazy<ITreatmentLocationService> treatmentLocationService,
            Lazy<IVisitService> visitService,
            Lazy<IVisitPayUserApplicationService> visitPayUserApplicationService,
            Lazy<Ivh.Common.ServiceBus.Interfaces.IBus> bus,
            Lazy<IMetricsProvider> metricsProvider,
            Lazy<IImageService> imageService,
            Lazy<IMalwareScanningProvider> malwareScanningProvider,
            ISessionContext<VisitPay> sessionContext)
            : base(applicationServiceCommonService)
        {
            this._contentService = contentService;
            this._guarantorService = guarantorService;
            this._supportRequestService = supportRequestService;
            this._supportTopicService = supportTopicService;
            this._treatmentLocationService = treatmentLocationService;
            this._visitService = visitService;
            this._visitPayUserApplicationService = visitPayUserApplicationService;
            this._bus = bus;
            this._metricsProvider = metricsProvider;
            this._imageService = imageService;
            this._session = sessionContext.Session;
            this._malwareScanningProvider = malwareScanningProvider;
        }

        private Guarantor GetGuarantor(int visitPayUserId)
        {
            int guarantorId = this._guarantorService.Value.GetGuarantorId(visitPayUserId);
            return this._guarantorService.Value.GetGuarantor(guarantorId);
        }

        public SupportRequestDto CreateSupportRequest(SupportRequestCreateDto supportRequestCreateDto)
        {
            Guarantor guarantor = this.GetGuarantor(supportRequestCreateDto.VisitPayUserId);
            Guarantor submittedForGuarantor = supportRequestCreateDto.SubmittedForVisitPayUserId.HasValue && supportRequestCreateDto.SubmittedForVisitPayUserId.Value != guarantor.User.VisitPayUserId ?
                this.GetGuarantor(supportRequestCreateDto.SubmittedForVisitPayUserId.Value)
                : null;

            SupportTopic supportTopic = supportRequestCreateDto.SupportTopicId.HasValue ? this._supportTopicService.Value.GetSupportTopic(supportRequestCreateDto.SupportTopicId.Value) : null;
            TreatmentLocation treatmentLocation = supportRequestCreateDto.TreatmentLocationId.HasValue ? this._treatmentLocationService.Value.GetTreatmentLocation(supportRequestCreateDto.TreatmentLocationId.Value) : null;
            Visit visit = supportRequestCreateDto.VisitId.HasValue ? this._visitService.Value.GetVisit((submittedForGuarantor ?? guarantor).VpGuarantorId, supportRequestCreateDto.VisitId.Value) : null;
            // this will obfuscate the visit if ncessary (mapping the dto to itself)
            VisitDto visitDto = Mapper.Map<VisitDto>(Mapper.Map<VisitDto>(visit));

            DateTime now = DateTime.UtcNow;

            SupportRequest supportRequest = new SupportRequest
            {
                AssignedVisitPayUser = supportRequestCreateDto.AssignedVisitPayUserId.HasValue ? new VisitPayUser { VisitPayUserId = supportRequestCreateDto.AssignedVisitPayUserId.Value } : null,
                InsertDate = now,
                PatientFirstName = visitDto?.PatientFirstName,
                PatientLastName = visitDto?.PatientLastName,
                TreatmentDate = visitDto?.DischargeDate,
                SupportRequestStatus = SupportRequestStatusEnum.Open,
                SupportTopic = supportTopic,
                TreatmentLocation = treatmentLocation,
                Visit = visit,
                VpGuarantor = guarantor,
                SubmittedForVpGuarantor = submittedForGuarantor
            };

            supportRequest.SupportRequestMessages = new List<SupportRequestMessage>
            {
                new SupportRequestMessage
                {
                    CreatedByVpUserId = supportRequestCreateDto.CreatedByVisitPayUserId,
                    InsertDate = now,
                    IsRead = false,
                    MessageBody = supportRequestCreateDto.MessageBody,
                    SupportRequest = supportRequest,
                    SupportRequestMessageType = SupportRequestMessageTypeEnum.FromGuarantor
                }
            };

            SupportRequestDto supportRequestDto = Mapper.Map<SupportRequestDto>(this._supportRequestService.Value.SaveSupportRequest(supportRequest));

            this.SendSupportRequestCreatedEmail(supportRequestDto);

            this._metricsProvider.Value.Increment(NewSupportRequestMetricKey);

            return supportRequestDto;
        }

        public SupportRequestDto GetSupportRequest(int supportRequestId, int visitPayUserId)
        {
            Guarantor guarantor = this.GetGuarantor(visitPayUserId);

            if (guarantor != null)
            {
                SupportRequest supportRequest = this._supportRequestService.Value.GetSupportRequest(supportRequestId, guarantor);
                return Mapper.Map<SupportRequestDto>(supportRequest);
            }

            return null;
        }

        public SupportRequestResultsDto GetSupportRequests(SupportRequestFilterDto supportRequestFilterDto, int batch, int batchSize, int? visitPayUserId)
        {
            Guarantor guarantor = visitPayUserId.HasValue ? this.GetGuarantor(visitPayUserId.Value) : null;
            SupportRequestFilter filter = Mapper.Map<SupportRequestFilter>(supportRequestFilterDto);
            SupportRequestResults results = this._supportRequestService.Value.GetSupportRequests(guarantor, filter, Math.Max(batch - 1, 0), batchSize);

            return Mapper.Map<SupportRequestResultsDto>(results);
        }

        public int GetSupportRequestTotals(int visitPayUserId, SupportRequestFilterDto supportRequestFilterDto)
        {
            Guarantor guarantor = this.GetGuarantor(visitPayUserId);
            return this._supportRequestService.Value.GetSupportRequestTotals(guarantor, Mapper.Map<SupportRequestFilter>(supportRequestFilterDto));
        }

        public void CloseSupportRequest(int supportRequestId, int visitPayUserId, string messageBody, int createdByVisitPayUserId)
        {
            Guarantor guarantor = this.GetGuarantor(visitPayUserId);

            SupportRequest supportRequest = this._supportRequestService.Value.SetSupportRequestClosedOrOpen(SupportRequestStatusEnum.Closed, supportRequestId, guarantor, messageBody, createdByVisitPayUserId);

            this.SendSupportRequestUpdatedEmail(Mapper.Map<SupportRequestDto>(supportRequest), "closed");
        }

        public void ReopenSupportRequest(int supportRequestId, int visitPayUserId, string messageBody, int createdByVisitPayUserId)
        {
            Guarantor guarantor = this.GetGuarantor(visitPayUserId);

            SupportRequest supportRequest = this._supportRequestService.Value.SetSupportRequestClosedOrOpen(SupportRequestStatusEnum.Open, supportRequestId, guarantor, messageBody, createdByVisitPayUserId);

            this.SendSupportRequestUpdatedEmail(Mapper.Map<SupportRequestDto>(supportRequest), "re-opened");
        }

        public SupportRequestMessageDto SaveInternalNote(int supportRequestId, int visitPayUserId, string messageBody, int createdByVisitPayUserId)
        {
            Guarantor guarantor = this.GetGuarantor(visitPayUserId);

            SupportRequestMessage supportRequestMessage = this._supportRequestService.Value.SaveInternalNote(supportRequestId, guarantor, messageBody, createdByVisitPayUserId);

            return Mapper.Map<SupportRequestMessageDto>(supportRequestMessage);
        }

        public SupportRequestMessageDto SaveClientMessage(int supportRequestId, int visitPayUserId, string messageBody, int createdByVisitPayUserId)
        {
            Guarantor guarantor = this.GetGuarantor(visitPayUserId);

            SupportRequestMessage supportRequestMessage = this._supportRequestService.Value.SaveClientMessage(supportRequestId, guarantor, messageBody, createdByVisitPayUserId);

            this.SendSupportRequestUpdatedEmail(Mapper.Map<SupportRequestDto>(supportRequestMessage.SupportRequest), "updated");

            return Mapper.Map<SupportRequestMessageDto>(supportRequestMessage);
        }

        public SupportRequestMessageDto SaveGuarantorMessage(int supportRequestId, int visitPayUserId, string messageBody, int createdByVisitPayUserId)
        {
            Guarantor guarantor = this.GetGuarantor(visitPayUserId);

            SupportRequestMessage supportRequestMessage = this._supportRequestService.Value.SaveGuarantorMessage(supportRequestId, guarantor, messageBody, createdByVisitPayUserId);

            return Mapper.Map<SupportRequestMessageDto>(supportRequestMessage);
        }

        public void SaveDetails(int clientSupportRequestId, int visitPayUserId, int? assignedToVisitPayUserId, int actionVisitPayUserId)
        {
            Guarantor guarantor = this.GetGuarantor(visitPayUserId);

            this._supportRequestService.Value.SaveDetails(clientSupportRequestId, guarantor, assignedToVisitPayUserId, actionVisitPayUserId);
        }

        public void DeleteAttachment(int supportRequestId, int supportRequestMessageAttachmentId, int actionVisitPayUserId)
        {
            this._supportRequestService.Value.DeleteAttachment(supportRequestId, supportRequestMessageAttachmentId);
        }

        public void SetSupportTopic(int supportRequestId, int? supportTopicId, int visitPayUserId)
        {
            Guarantor guarantor = this.GetGuarantor(visitPayUserId);

            this._supportRequestService.Value.SetSupportTopic(guarantor, supportRequestId, supportTopicId);
        }

        public IList<SupportRequestMessageAttachmentDto> GetAttachments(int supportRequestId, int visitPayUserId, int? supportRequestMessageId, bool showInternalAttachments)
        {
            Guarantor guarantor = this.GetGuarantor(visitPayUserId);

            SupportRequest supportRequest = this._supportRequestService.Value.GetSupportRequest(supportRequestId, guarantor);
            IList<SupportRequestMessage> supportRequestMessages = supportRequest.SupportRequestMessages.ToList();

            if (!showInternalAttachments)
                supportRequestMessages = supportRequestMessages.Where(x => x.SupportRequestMessageType == SupportRequestMessageTypeEnum.FromGuarantor ||
                                                                           x.SupportRequestMessageType == SupportRequestMessageTypeEnum.ToGuarantor).ToList();

            //Go through all attachments and scan any that haven't been scanned yet
            List<SupportRequestMessage> supportRequestMessagesWithUnscannedAttachments = supportRequestMessages.Where(sr => sr.SupportRequestMessageAttachments.Any(a => MalwareScanStatusEnum.NotScanned == a.MalwareScanStatus)).ToList();
            foreach (SupportRequestMessage supportRequestMessage in supportRequestMessagesWithUnscannedAttachments)
            {
                List<SupportRequestMessageAttachment> unscannedAttachments = supportRequestMessage.SupportRequestMessageAttachments.Where(a => MalwareScanStatusEnum.NotScanned == a.MalwareScanStatus).ToList();
                foreach (SupportRequestMessageAttachment unscannedAttachment in unscannedAttachments)
                {
                    byte[] bytesToScan = unscannedAttachment.FileContent?.Length > 0 ? unscannedAttachment.FileContent : unscannedAttachment.QuarantinedBytes;
                    MalwareScanStatusEnum scanResult = this._malwareScanningProvider.Value.ScanBytesForMalware(unscannedAttachment.AttachmentFileName, unscannedAttachment.MimeType, bytesToScan);

                    //Update scan result on attachment
                    unscannedAttachment.MalwareScanStatus = scanResult;

                    //Move bytes according to scan result
                    switch (scanResult)
                    {
                        //Clean, move quarantined bytes to FileContent
                        case MalwareScanStatusEnum.Clean:
                            unscannedAttachment.FileContent = bytesToScan;
                            unscannedAttachment.QuarantinedBytes = null;
                            break;

                        //Dirty, clear all file bytes
                        case MalwareScanStatusEnum.Detected:
                            unscannedAttachment.FileContent = null;
                            unscannedAttachment.QuarantinedBytes = null;
                            break;

                        //Not scanned, move bytes to quarantined
                        case MalwareScanStatusEnum.NotScanned:
                            unscannedAttachment.FileContent = null;
                            unscannedAttachment.QuarantinedBytes = bytesToScan;
                            break;
                    }
                }

                //Save the support request message now that it has been scanned
                this._supportRequestService.Value.SaveSupportRequestMessage(supportRequestMessage);
            }
            
            IList<SupportRequestMessageAttachment> attachments = supportRequestMessageId.HasValue ?
                supportRequestMessages.Where(x => x.SupportRequestMessageId == supportRequestMessageId.Value).SelectMany(x => x.ActiveSupportRequestMessageAttachments).ToList() :
                supportRequestMessages.SelectMany(x => x.ActiveSupportRequestMessageAttachments).ToList();

            return Mapper.Map<List<SupportRequestMessageAttachmentDto>>(attachments);
        }

        public void SaveMessageAttachment(SupportRequestMessageAttachmentDto supportRequestMessageAttachmentDto, int supportRequestMessageId, int supportRequestId, int visitPayUserId)
        {
            Guarantor guarantor = this.GetGuarantor(visitPayUserId);
            SupportRequest supportRequest = this._supportRequestService.Value.GetSupportRequest(supportRequestId, guarantor);

            SupportRequestMessage supportRequestMessage = this._supportRequestService.Value.GetSupportRequestMessage(supportRequest, supportRequestMessageId);
            SupportRequestMessageAttachment supportRequestMessageAttachment = Mapper.Map<SupportRequestMessageAttachment>(supportRequestMessageAttachmentDto);

            //Initial save should only save file contents to QuarantinedBytes, until after scanning
            if (supportRequestMessageAttachment.FileContent != null && supportRequestMessageAttachment.FileContent.Length > 0)
            {
                supportRequestMessageAttachment.QuarantinedBytes = supportRequestMessageAttachment.FileContent;
                supportRequestMessageAttachment.FileContent = null;
            }

            //Initial status is "NotScanned"
            supportRequestMessageAttachment.MalwareScanStatus = MalwareScanStatusEnum.NotScanned;

            supportRequestMessageAttachment.SupportRequestMessage = supportRequestMessage;
            supportRequestMessage.SupportRequestMessageAttachments.Add(supportRequestMessageAttachment);
            
            this._supportRequestService.Value.SaveSupportRequestMessage(supportRequestMessage);

            //Send message telling service bus to scan attachments for malware
            this.SendSupportRequestMalwareScanMessage(supportRequestId, supportRequestMessageId);
        }

        public AttachmentDownloadDto PrepareDownload(int supportRequestId, int? supportRequestMessageAttachmentId, int visitPayUserId, bool showInternalAttachments)
        {
            IList<SupportRequestMessageAttachmentDto> supportRequestAttachmentsDto = this.GetAttachments(supportRequestId, visitPayUserId, null, showInternalAttachments);

            //Only allow download of clean attachments
            supportRequestAttachmentsDto = supportRequestAttachmentsDto.Where(sr => MalwareScanStatusEnum.Clean == sr.MalwareScanStatus).ToList();

            if (supportRequestMessageAttachmentId.HasValue)
                supportRequestAttachmentsDto = supportRequestAttachmentsDto.Where(x => x.SupportRequestMessageAttachmentId == supportRequestMessageAttachmentId.Value).ToList();

            if (!supportRequestAttachmentsDto.Any())
                return null;

            if (supportRequestAttachmentsDto.Count() == 1)
                return new AttachmentDownloadDto
                {
                    Bytes = supportRequestAttachmentsDto.First().FileContent,
                    FileName = supportRequestAttachmentsDto.First().AttachmentFileName,
                    MimeType = supportRequestAttachmentsDto.First().MimeType
                };

            // more than 1, zip
            byte[] bytes;
            using (ZipFile zipFile = new ZipFile())
            {
                foreach (SupportRequestMessageAttachmentDto attachmentDto in supportRequestAttachmentsDto)
                {
                    string attachmentName = attachmentDto.AttachmentFileName;
                    string newAttachmentName = attachmentName;

                    int i = 1;
                    while (zipFile.Entries.Any(x => String.Equals(x.FileName, newAttachmentName, StringComparison.CurrentCultureIgnoreCase)))
                    {
                        string extension = Path.GetExtension(attachmentName);
                        newAttachmentName = extension != null ? attachmentName.Replace(extension, string.Format("({0}){1}", i, extension)) : string.Format("{0}({1})", attachmentName, i);
                        i++;
                    }

                    zipFile.AddEntry(newAttachmentName, attachmentDto.FileContent);
                }

                using (MemoryStream stream = new MemoryStream())
                {
                    zipFile.Save(stream);
                    stream.Seek(0, SeekOrigin.Begin);
                    stream.Flush();

                    bytes = stream.ToArray();
                }
            }

            return new AttachmentDownloadDto
            {
                Bytes = bytes,
                FileName = string.Format("{0}_attachments.zip", supportRequestAttachmentsDto.First().SupportRequestMessage.SupportRequest.SupportRequestDisplayId),
                MimeType = "application/x-zip-compressed"
            };
        }

        public SupportRequestMessageDto GetClosedMessage(SupportRequestDto supportRequestDto)
        {
            return supportRequestDto.SupportRequestMessages.
                                     Where(x => x.SupportRequestMessageType == SupportRequestMessageTypeEnum.CloseMessage)
                                    .OrderByDescending(x => x.InsertDate)
                                    .FirstOrDefault();
        }

        public void SetRead(int supportRequestId, int visitPayUserId, SupportRequestMessageTypeEnum supportRequestType)
        {
            Guarantor guarantor = this.GetGuarantor(visitPayUserId);

            this._supportRequestService.Value.SetRead(supportRequestId, guarantor, supportRequestType);
        }

        public IDictionary<int, string> GetAssignableUsers(bool assignedOnly, int? assignedVisitPayUserId)
        {
            Dictionary<int, string> supportAdminUsers = new Dictionary<int, string>();

            if (assignedOnly)
            {
                IReadOnlyList<VisitPayUser> users = this._supportRequestService.Value.GetAllAssignedUsers(supportAdminUsers.Select(x => x.Key).ToList());
                if (users.Any())
                {
                    users.ForEach(x =>
                    {
                        if (!supportAdminUsers.ContainsKey(x.VisitPayUserId))
                        {
                            supportAdminUsers.Add(x.VisitPayUserId, x.FirstNameLastName);
                        }
                    });
                }
            }
            else
            {
                supportAdminUsers = this._visitPayUserApplicationService.Value.GetVisitPayClientUsersByRole(
                    roles: new List<int> { (int)VisitPayRoleEnum.SupportAdmin },
                    isDisabled: false,
                    excludedRoles: new List<int> { (int)VisitPayRoleEnum.IvhAdmin })
                    .ToDictionary(k => k.VisitPayUserId, v => v.DisplayFirstNameLastName);
            }

            if (assignedVisitPayUserId.HasValue && !supportAdminUsers.ContainsKey(assignedVisitPayUserId.Value))
            {
                VisitPayUserDto assignedVisitPayUserDto = this._visitPayUserApplicationService.Value.FindById(assignedVisitPayUserId.Value.ToString());
                if (assignedVisitPayUserDto != null)
                {
                    supportAdminUsers.Add(assignedVisitPayUserDto.VisitPayUserId, assignedVisitPayUserDto.DisplayFirstNameLastName);
                }
            }

            return supportAdminUsers.OrderBy(x => x.Value).ToDictionary(k => k.Key, v => v.Value);
        }

        public int GetAllUnreadMessagesFromGuarantorsCount()
        {
            return this._supportRequestService.Value.GetAllUnreadMessagesFromGuarantorsCount();
        }

        public void RedactSupportRequestsForVisit(int vpGuarantorId, int visitId)
        {
            this._supportRequestService.Value.RedactSupportRequestsForVisit(vpGuarantorId, visitId);
        }

        #region exports

        public async Task<byte[]> ExportSupportRequestsClientAsync(SupportRequestFilterDto filterDto, string userName)
        {
            SupportRequestResultsDto results = this.GetSupportRequests(filterDto, 1, iVinciExcelExportBuilder.MaxExportRecords, null);

            IDictionary<string, string> textRegions = await this._contentService.Value.GetTextRegionsAsync(
                TextRegionConstants.AssignedTo,
                TextRegionConstants.Case,
                TextRegionConstants.Closed,
                TextRegionConstants.Created,
                TextRegionConstants.CustomerCareRequests,
                TextRegionConstants.ExportedBy,
                TextRegionConstants.FileDate,
                TextRegionConstants.Guarantor,
                TextRegionConstants.LastModifiedDate,
                TextRegionConstants.LastModifiedBy,
                TextRegionConstants.NoCustomerCareRequests,
                TextRegionConstants.Status,
                TextRegionConstants.SubmittedFor,
                TextRegionConstants.TopicSubtopic,
                TextRegionConstants.Unassigned);

            System.Drawing.Image logo = await this._imageService.Value.GetExportLogoAsync();

            using (ExcelPackage package = new ExcelPackage())
            {
                iVinciExcelExportBuilder builder = new iVinciExcelExportBuilder(package, textRegions.GetValue(TextRegionConstants.CustomerCareRequests))
                    .WithWorkSheetNameOf(textRegions.GetValue(TextRegionConstants.CustomerCareRequests))
                    .WithLogo(logo)
                    .AddInfoBlockRow(new ColumnBuilder(DateTime.UtcNow.ToString(Format.DateFormat), textRegions.GetValue(TextRegionConstants.FileDate)).WithStyle(StyleTypeEnum.AlignLeft))
                    .AddInfoBlockRow(new ColumnBuilder(userName, textRegions.GetValue(TextRegionConstants.ExportedBy)).WithStyle(StyleTypeEnum.AlignLeft))
                    .AddDataBlock(results.SupportRequests, supportRequest =>
                    {
                        string assignedVisitPayUser = supportRequest.AssignedVisitPayUser == null ? textRegions.GetValue(TextRegionConstants.Unassigned) : supportRequest.AssignedVisitPayUser.DisplayFirstNameLastName;
                        string statusName = supportRequest.SupportRequestStatusSource.SupportRequestStatusName;
                        string supportTopic = supportRequest.SupportTopic == null ? "" : $"{supportRequest.SupportTopic.ParentSupportTopic.SupportTopicName} - {supportRequest.SupportTopic.SupportTopicName}";
                        DateTime lastModifiedDate = supportRequest.SupportRequestMessages.OrderByDescending(x => x.InsertDate).Select(x => x.InsertDate).First();
                        string lastModifiedBy = supportRequest.SupportRequestMessages.OrderByDescending(x => x.InsertDate).Select(x => x.VisitPayUser.DisplayFirstNameLastName).First();

                        GuarantorDto submittedForGuarantor = supportRequest.SubmittedForVpGuarantor ?? supportRequest.VpGuarantor;
                        List<Column> columns = new List<Column>
                        {
                            new ColumnBuilder(supportRequest.SupportRequestStatus == SupportRequestStatusEnum.Closed ? "Y" : "N", textRegions.GetValue(TextRegionConstants.Closed)).AlignCenter(),
                            new ColumnBuilder(assignedVisitPayUser, textRegions.GetValue(TextRegionConstants.AssignedTo)),
                            new ColumnBuilder(supportRequest.SupportRequestDisplayId, textRegions.GetValue(TextRegionConstants.Case)),
                            new ColumnBuilder(supportRequest.InsertDate, textRegions.GetValue(TextRegionConstants.Created)).AlignLeft().WithDateFormat(),
                            new ColumnBuilder(statusName, textRegions.GetValue(TextRegionConstants.Status)),
                            new ColumnBuilder(supportRequest.VpGuarantor.User.DisplayFirstNameLastName, textRegions.GetValue(TextRegionConstants.Guarantor)),
                            new ColumnBuilder(submittedForGuarantor.User.DisplayFirstNameLastName, textRegions.GetValue(TextRegionConstants.SubmittedFor)),
                            new ColumnBuilder(supportTopic, textRegions.GetValue(TextRegionConstants.TopicSubtopic)),
                            new ColumnBuilder(lastModifiedDate, textRegions.GetValue(TextRegionConstants.LastModifiedDate)).AlignLeft().WithDateFormat(),
                            new ColumnBuilder(lastModifiedBy, textRegions.GetValue(TextRegionConstants.LastModifiedBy))
                        };
                        return columns;
                    })
                    .WithNoRecordsFoundMessageOf(textRegions.GetValue(TextRegionConstants.NoCustomerCareRequests));

                builder.Build();

                ExcelWorksheet worksheet = package.Workbook.Worksheets.FirstOrDefault();
                if (worksheet != null)
                {
                    worksheet.Column(8).Width *= 1.15;
                    worksheet.Column(9).Width *= 1.15; //Last Modified Date
                }

                return package.GetAsByteArray();
            }
        }

        #endregion

        public void SaveDraft(int supportRequestId, int visitPayUserId, string messageBody, int createdByVisitPayUserId)
        {
            Guarantor guarantor = this.GetGuarantor(visitPayUserId);
            this._supportRequestService.Value.SaveDraft(supportRequestId, guarantor, messageBody, createdByVisitPayUserId);
        }

        public SupportRequestMessageDto GetDraftMessage(int supportRequestId, int createdByVisitPayUserId)
        {
            SupportRequestMessage supportRequestMessage = this._supportRequestService.Value.GetSupportDraftMessage(supportRequestId, createdByVisitPayUserId);
            return Mapper.Map<SupportRequestMessageDto>(supportRequestMessage);
        }

        public void FlagForFollowUp(int supportRequestId, int visitPayUserId, bool flagForFollowUp, int createdByVisitPayUserId)
        {
            Guarantor guarantor = this.GetGuarantor(visitPayUserId);
            this._supportRequestService.Value.FlagForFollowUp(supportRequestId, guarantor, flagForFollowUp, createdByVisitPayUserId);
        }

        public int GetSupportRequestsWithUnreadMessagesToGuarantorCount(int vpGuarantorId)
        {
            int count = this._supportRequestService.Value.GetSupportRequestsWithUnreadMessagesToGuarantorCount(vpGuarantorId);
            return count;
        }

        private void SendSupportRequestMalwareScanMessage(int supportRequestId, int supportRequestMessageId)
        {
            ScanSupportRequestMessageForMalwareMessage message = null;
            try
            {
                message = new ScanSupportRequestMessageForMalwareMessage
                {
                    SupportRequestId = supportRequestId,
                    SupportRequestMessageId = supportRequestMessageId
                };

                this._session.Transaction.RegisterPostCommitSuccessAction(m =>
                {
                    this._bus.Value.PublishMessage(m).Wait();
                }, message);
            }
            catch
            {
                this.LoggingService.Value.Fatal(() => $"Failed to publish SendSupportRequestMalwareScanMessage, Message = {JsonConvert.SerializeObject(message)}");
            }
        }

        private void SendSupportRequestCreatedEmail(SupportRequestDto supportRequestDto)
        {
            this._bus.Value.PublishMessage(new SendSupportRequestCreatedEmailMessage
            {
                VpGuarantorId = supportRequestDto.VpGuarantor.VpGuarantorId,
                SupportRequestDisplayId = supportRequestDto.SupportRequestDisplayId
            }).Wait();
        }

        private void SendSupportRequestUpdatedEmail(SupportRequestDto supportRequestDto, string modification)
        {
            this._bus.Value.PublishMessage(new SendSupportRequestUpdatedEmailMessage
            {
                VpGuarantorId = supportRequestDto.VpGuarantor.VpGuarantorId,
                SupportRequestDisplayId = supportRequestDto.SupportRequestDisplayId,
                InsertDate = supportRequestDto.InsertDate.ToString(Format.DateFormat),
                ModificationStatus = modification
            }).Wait();
        }

        [UniversalConsumer(UniversalQueuesEnum.VisitPayProcessor, UniversalPriorityEnum.Priority10)]
        public void ConsumeMessage(SupportRequestRedactMessage message, ConsumeContext<SupportRequestRedactMessage> consumeContext)
        {
            this.RedactSupportRequestsForVisit(message.VpGuarantorId, message.VisitId);
        }

        public bool IsMessageValid(SupportRequestRedactMessage message, ConsumeContext<SupportRequestRedactMessage> consumeContext)
        {
            bool isValid = true;
            return isValid;
        }
    }
}

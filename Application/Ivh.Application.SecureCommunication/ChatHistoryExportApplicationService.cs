﻿
namespace Ivh.Application.SecureCommunication
{
    using Base.Common.Interfaces;
    using Base.Services;
    using Common.Dtos;
    using Common.Interfaces;
    using Core.Common.Utilities.Builders;
    using Domain.Base.Extensions;
    using Domain.Content.Interfaces;
    using Domain.Guarantor.Entities;
    using Domain.Guarantor.Interfaces;
    using Ivh.Common.VisitPay.Strings;
    using OfficeOpenXml;
    using System;
    using System.Collections.Generic;
    using System.Drawing;
    using System.Threading.Tasks;

    public class ChatHistoryExportApplicationService : ApplicationService, IChatHistoryExportApplicationService
    {
        private readonly Lazy<IImageService> _imageService;
        private readonly Lazy<IGuarantorService> _guarantorService;

        public ChatHistoryExportApplicationService(
            Lazy<IApplicationServiceCommonService> applicationServiceCommonService,
            Lazy<IImageService> imageService,
            Lazy<IGuarantorService> guarantorService) : base(applicationServiceCommonService)
        {
            this._imageService = imageService;
            this._guarantorService = guarantorService;
        }

        public async Task<byte[]> ExportChatHistoryOverviewAsync(ChatThreadHistoryOverviewsDto chatThreadHistoryOverviewsDto, string userName, Guid guarantorVisitPayUserGuid)
        {
            //Get the guarantor
            Guarantor guarantor = this.GetGuarantorByVisitPayUserGuid(guarantorVisitPayUserGuid);

            Image logo = await this._imageService.Value.GetExportLogoAsync();

            using (ExcelPackage package = new ExcelPackage())
            {
                iVinciExcelExportBuilder builder = new iVinciExcelExportBuilder(package, $"{guarantor.User.FirstNameLastName} Chat History")
                    .WithWorkSheetNameOf("Chat History")
                    .WithLogo(logo)
                    .AddInfoBlockRow(new ColumnBuilder(DateTime.UtcNow.ToString(Format.MonthDayYearFormat), "File Date:").WithStyle(StyleTypeEnum.AlignLeft))
                    .AddInfoBlockRow(new ColumnBuilder(userName, "Exported By:").WithStyle(StyleTypeEnum.AlignLeft))
                    .AddDataBlock(chatThreadHistoryOverviewsDto.ThreadHistoryOverviews, threadHistoryDto => new List<Column>
                    {
                        new ColumnBuilder(this.ConvertDate(threadHistoryDto.ThreadDateCreated), "Thread Start Date").AlignCenter(),
                        new ColumnBuilder(this.ConvertDate(threadHistoryDto.ThreadDateClosed), "Thread Closed Date").AlignCenter(),
                        new ColumnBuilder(threadHistoryDto.GuarantorName, "Guarantor").AlignLeft(),
                        new ColumnBuilder(threadHistoryDto.OperatorName, "Chat Operator").AlignLeft(),
                        new ColumnBuilder(threadHistoryDto.ThreadStatus, "Thread Status").AlignLeft()
                    })
                    .WithNoRecordsFoundMessageOf("There are no chat threads to display at this time.");

                builder.Build();
                return package.GetAsByteArray();
            }
        }

        public async Task<byte[]> ExportChatThreadHistoryAsync(ChatThreadHistoryDto chatThreadHistoryDto, string userName, Guid guarantorVisitPayUserGuid)
        {
            //Get the guarantor
            Guarantor guarantor = this.GetGuarantorByVisitPayUserGuid(guarantorVisitPayUserGuid);

            Image logo = await this._imageService.Value.GetExportLogoAsync();

            using (ExcelPackage package = new ExcelPackage())
            {
                iVinciExcelExportBuilder builder = new iVinciExcelExportBuilder(package, $"{guarantor.User.FirstNameLastName} Chat Thread History")
                    .WithWorkSheetNameOf("Chat Thread History")
                    .WithLogo(logo)
                    .AddInfoBlockRow(new ColumnBuilder(DateTime.UtcNow.ToString(Format.MonthDayYearFormat), "File Date:").WithStyle(StyleTypeEnum.AlignLeft))
                    .AddInfoBlockRow(new ColumnBuilder(userName, "Exported By:").WithStyle(StyleTypeEnum.AlignLeft))
                    .AddBlankInfoBlockRow()
                    .AddInfoBlockRow(new ColumnBuilder(this.ConvertDate(chatThreadHistoryDto.ThreadDateCreated), "Thread Start Date:").WithStyle(StyleTypeEnum.AlignLeft))
                    .AddInfoBlockRow(new ColumnBuilder(this.ConvertDate(chatThreadHistoryDto.ThreadDateClosed), "Thread Closed Date:").WithStyle(StyleTypeEnum.AlignLeft))
                    .AddInfoBlockRow(new ColumnBuilder(chatThreadHistoryDto.ThreadStatus, "Thread Status:").WithStyle(StyleTypeEnum.AlignLeft))
                    .AddDataBlock(chatThreadHistoryDto.MessageHistory, message => new List<Column>
                    {
                        new ColumnBuilder(this.ConvertDate(message.DateSent), "Message Sent").AlignCenter(),
                        new ColumnBuilder(message.SendingUserName(chatThreadHistoryDto.ThreadUsers), "Message Sent By").AlignLeft(),
                        new ColumnBuilder(message.MessageType, "Message Type").AlignLeft(),
                        new ColumnBuilder(message.Body, "Message Body").AlignLeft(),
                        new ColumnBuilder(this.GetMessageAttachmentFileName(message), "Attachment Name").AlignLeft()
                    })
                    .WithNoRecordsFoundMessageOf("There are no messages in this chat thread.");

                builder.Build();
                return package.GetAsByteArray();
            }
        }

        private Guarantor GetGuarantorByVisitPayUserGuid(Guid guarantorVisitPayUserGuid)
        {
            int guarantorId = this._guarantorService.Value.GetGuarantorId(guarantorVisitPayUserGuid);
            Guarantor guarantor = this._guarantorService.Value.GetGuarantor(guarantorId);
            return guarantor;
        }

        private string GetMessageAttachmentFileName(ChatThreadMessageDto message)
        {
            //Only attempt to get the file name if the message is of type "File", to
            //avoid deserializing content when not needed
            string fileName = "";
            if ("File".Equals(message.MessageType))
            {
                fileName = message.ContentDto.FileName;
            }
            return fileName;
        }

        private string ConvertDate(DateTime? date, string format = Format.DateTimeFormat, string altText = "")
        {
            return date.HasValue ? date.Value.ToClientDateTime().ToString(format) : altText;
        }
    }
}

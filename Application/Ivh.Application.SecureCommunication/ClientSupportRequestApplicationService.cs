﻿namespace Ivh.Application.SecureCommunication
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Threading.Tasks;
    using System.Web;
    using AutoMapper;
    using Base.Common.Interfaces;
    using Base.Services;
    using Common.Dtos;
    using Common.Interfaces;
    using Content.Common.Dtos;
    using Content.Common.Interfaces;
    using Core.Common.Interfaces;
    using Core.Common.Utilities.Builders;
    using Domain.Content.Interfaces;
    using Domain.Core.Malware.Interfaces;
    using Domain.Guarantor.Entities;
    using Domain.Guarantor.Interfaces;
    using Domain.User.Entities;
    using Domain.SecureCommunication.ClientSupportRequests.Entities;
    using Domain.SecureCommunication.ClientSupportRequests.Interfaces;
    using Domain.SecureCommunication.GuarantorSupportRequests.Entities;
    using Domain.SecureCommunication.GuarantorSupportRequests.Interfaces;
    using Ionic.Zip;
    using Ivh.Common.Base.Enums;
    using Ivh.Common.Base.Utilities.Extensions;
    using Ivh.Common.Data;
    using Ivh.Common.Data.Connection.Connections;
    using Ivh.Common.Data.Interfaces;
    using Ivh.Common.VisitPay.Enums;
    using Ivh.Common.VisitPay.Messages.Malware;
    using Ivh.Common.VisitPay.Strings;
    using Newtonsoft.Json;
    using OfficeOpenXml;
    using User.Common.Dtos;
    using NHibernate;

    public class ClientSupportRequestApplicationService : ApplicationService, IClientSupportRequestApplicationService
    {
        private readonly Lazy<IClientApplicationService> _clientApplicationService;
        private readonly Lazy<IContentApplicationService> _contentApplicationService;
        private readonly Lazy<IClientSupportRequestService> _clientSupportRequestService;
        private readonly Lazy<IGuarantorService> _guarantorService;
        private readonly Lazy<ISupportRequestService> _supportRequestService;
        private readonly Lazy<IVisitPayUserApplicationService> _visitPayUserApplicationService;
        private readonly Lazy<IImageService> _imageService;
        private readonly Lazy<IMalwareScanningProvider> _malwareScanningProvider;
        private readonly ISession _session;
        private readonly Lazy<Ivh.Common.ServiceBus.Interfaces.IBus> _bus;

        public ClientSupportRequestApplicationService(
            Lazy<IApplicationServiceCommonService> applicationServiceCommonService,
            Lazy<IClientApplicationService> clientApplicationService,
            Lazy<IContentApplicationService> contentApplicationService,
            Lazy<IClientSupportRequestService> clientSupportRequestService,
            Lazy<IGuarantorService> guarantorService,
            Lazy<ISupportRequestService> supportRequestService,
            Lazy<IVisitPayUserApplicationService> visitPayUserApplicationService,
            Lazy<IImageService> imageService,
            Lazy<IMalwareScanningProvider> malwareScanningProvider,
            Lazy<Ivh.Common.ServiceBus.Interfaces.IBus> bus,
            ISessionContext<VisitPay> sessionContext)
            : base(applicationServiceCommonService)
        {
            this._clientApplicationService = clientApplicationService;
            this._contentApplicationService = contentApplicationService;
            this._clientSupportRequestService = clientSupportRequestService;
            this._guarantorService = guarantorService;
            this._supportRequestService = supportRequestService;
            this._visitPayUserApplicationService = visitPayUserApplicationService;
            this._imageService = imageService;
            this._malwareScanningProvider = malwareScanningProvider;
            this._bus = bus;
            this._session = sessionContext.Session;
        }

        public ClientSupportRequestDto GetClientSupportRequest(int clientSupportRequestId)
        {
            ClientSupportRequest clientSupportRequest = this._clientSupportRequestService.Value.GetClientSupportRequest(clientSupportRequestId);

            //Go through all attachments and scan any that haven't been scanned yet
            List<ClientSupportRequestMessage> supportRequestMessagesWithUnscannedAttachments = clientSupportRequest.ClientSupportRequestMessages.Where(sr => sr.ClientSupportRequestMessageAttachments.Any(a => MalwareScanStatusEnum.NotScanned == a.MalwareScanStatus)).ToList();
            if (supportRequestMessagesWithUnscannedAttachments.Count > 0)
            {
                foreach (ClientSupportRequestMessage supportRequestMessage in supportRequestMessagesWithUnscannedAttachments)
                {
                    List<ClientSupportRequestMessageAttachment> unscannedAttachments = supportRequestMessage.ClientSupportRequestMessageAttachments.Where(a => MalwareScanStatusEnum.NotScanned == a.MalwareScanStatus).ToList();
                    foreach (ClientSupportRequestMessageAttachment unscannedAttachment in unscannedAttachments)
                    {
                        byte[] bytesToScan = unscannedAttachment.FileContent?.Length > 0 ? unscannedAttachment.FileContent : unscannedAttachment.QuarantinedBytes;
                        MalwareScanStatusEnum scanResult = this._malwareScanningProvider.Value.ScanBytesForMalware(unscannedAttachment.AttachmentFileName, unscannedAttachment.MimeType, bytesToScan);

                        //Update scan result on attachment
                        unscannedAttachment.MalwareScanStatus = scanResult;

                        //Move bytes according to scan result
                        switch (scanResult)
                        {
                            //Clean, move quarantined bytes to FileContent
                            case MalwareScanStatusEnum.Clean:
                                unscannedAttachment.FileContent = bytesToScan;
                                unscannedAttachment.QuarantinedBytes = null;
                                break;

                            //Dirty, clear all file bytes
                            case MalwareScanStatusEnum.Detected:
                                unscannedAttachment.FileContent = null;
                                unscannedAttachment.QuarantinedBytes = null;
                                break;

                            //Not scanned, move bytes to quarantined
                            case MalwareScanStatusEnum.NotScanned:
                                unscannedAttachment.FileContent = null;
                                unscannedAttachment.QuarantinedBytes = bytesToScan;
                                break;
                        }
                    }
                }

                //Update the support request now that it has been scanned
                this._clientSupportRequestService.Value.UpdateClientSupportRequest(clientSupportRequest);
            }

            return Mapper.Map<ClientSupportRequestDto>(clientSupportRequest);
        }

        public ClientSupportRequestResultsDto GetClientSupportRequests(ClientSupportRequestFilterDto filterDto)
        {
            ClientSupportRequestResults results = this._clientSupportRequestService.Value.GetClientSupportRequests(Mapper.Map<ClientSupportRequestFilter>(filterDto));

            return Mapper.Map<ClientSupportRequestResultsDto>(results);
        }

        public IList<ClientSupportTopicDto> GetClientSupportTopics(bool withTemplates)
        {
            IList<ClientSupportTopic> topics = this._clientSupportRequestService.Value.GetClientSupportTopics();
            IList<ClientSupportTopicDto> topicsDto = Mapper.Map<IList<ClientSupportTopicDto>>(topics);

            if (!withTemplates)
            {
                return topicsDto;
            }

            IDictionary<string, string> additionalValues = new Dictionary<string, string>();
            additionalValues.Add("<br />", Environment.NewLine);
            additionalValues.Add("<BR />", Environment.NewLine);
            additionalValues.Add("<br/>", Environment.NewLine);
            additionalValues.Add("<BR/>", Environment.NewLine);
            additionalValues.Add("<br>", Environment.NewLine);

            topicsDto.ForEach(topicDto =>
            {
                ClientSupportTopic topic = topics.FirstOrDefault(x => x.ClientSupportTopicId == topicDto.ClientSupportTopicId);
                if (topic == null)
                {
                    return;
                }
                topicDto.Templates.AddRange(topic.Templates.Select(t =>
                {
                    CmsVersionDto cmsVersionDto = this._contentApplicationService.Value.GetCurrentVersionAsync((CmsRegionEnum) t.CmsRegionId).Result;
                    cmsVersionDto.ContentBody = HttpUtility.HtmlDecode(cmsVersionDto.ContentBody);

                    additionalValues.ForEach(x => {
                        cmsVersionDto.ContentBody = cmsVersionDto.ContentBody.Replace(x.Key, x.Value);
                    });

                    return cmsVersionDto;
                }));
                topicDto.Templates = topicDto.Templates.OrderBy(x => x.ContentTitle).ToList();
            });

            // do some stuff that wasn't defined the requirements...

            return topicsDto;
        }

        public int GetUnreadMessagesCountFromClient(int? assignedVisitPayUserId)
        {
            return this._clientSupportRequestService.Value.GetUnreadMessagesCountFromClient(assignedVisitPayUserId);
        }

        public int GetUnreadMessagesCountToClient(int? visitPayUserId)
        {
            return this._clientSupportRequestService.Value.GetUnreadMessagesCountToClient(visitPayUserId);
        }

        public IDictionary<int, string> GetAssignableUsers(bool assignedOnly, int? assignedVisitPayUserId)
        {
            Dictionary<int, string> vpSupportTicketAdminUsers = new Dictionary<int, string>();

            if (assignedOnly)
            {
                IReadOnlyList<VisitPayUser> users = this._clientSupportRequestService.Value.GetAllAssignedUsers().ToList();
                if (users.Any())
                {
                    users.ForEach(x =>
                    {
                        if (!vpSupportTicketAdminUsers.ContainsKey(x.VisitPayUserId))
                        {
                            vpSupportTicketAdminUsers.Add(x.VisitPayUserId, x.FirstNameLastName);
                        }
                    });
                }
            }
            else
            {
                vpSupportTicketAdminUsers = this._visitPayUserApplicationService.Value.GetVisitPayClientUsersByRole(
                        roles: new List<int> { (int)VisitPayRoleEnum.VpSupportTicketAdmin }, 
                        isDisabled: false, 
                        excludedRoles: null)
                    .ToDictionary(k => k.VisitPayUserId, v => v.DisplayFirstNameLastName);
            }

            if (assignedVisitPayUserId.HasValue && !vpSupportTicketAdminUsers.ContainsKey(assignedVisitPayUserId.Value))
            {
                VisitPayUserDto assignedVisitPayUserDto = this._visitPayUserApplicationService.Value.FindById(assignedVisitPayUserId.Value.ToString());
                if (assignedVisitPayUserDto != null)
                {
                    vpSupportTicketAdminUsers.Add(assignedVisitPayUserDto.VisitPayUserId, assignedVisitPayUserDto.DisplayFirstNameLastName);
                }
            }

            return vpSupportTicketAdminUsers.OrderBy(x => x.Value).ToDictionary(k => k.Key, v => v.Value);
        }

        public AttachmentDownloadDto PrepareDownload(int clientSupportRequestId, int? clientSupportRequestMessageId, int? clientSupportRequestMessageAttachmentId, bool showInternalAttachments)
        {
            ClientSupportRequest clientSupportRequest = this._clientSupportRequestService.Value.GetClientSupportRequest(clientSupportRequestId);
            IList<ClientSupportRequestMessageAttachment> attachments = clientSupportRequest.ClientSupportRequestMessages.SelectMany(x => x.ActiveClientSupportRequestMessageAttachments).ToList();
            IList<ClientSupportRequestMessageAttachmentDto> attachmentsDto = Mapper.Map<IList<ClientSupportRequestMessageAttachmentDto>>(attachments);

            //Only allow download of clean attachments
            attachmentsDto = attachmentsDto.Where(sr => MalwareScanStatusEnum.Clean == sr.MalwareScanStatus).ToList();

            if (clientSupportRequestMessageAttachmentId.HasValue)
            {
                attachmentsDto = attachmentsDto.Where(x => x.ClientSupportRequestMessageAttachmentId == clientSupportRequestMessageAttachmentId.Value).ToList();
            }

            if (clientSupportRequestMessageId.HasValue)
            {
                attachmentsDto = attachmentsDto.Where(x => x.ClientSupportRequestMessage.ClientSupportRequestMessageId == clientSupportRequestMessageId.Value).ToList();
            }

            if (!showInternalAttachments)
            {
                attachmentsDto = attachmentsDto.Where(x => x.ClientSupportRequestMessage.ClientSupportRequestMessageType != ClientSupportRequestMessageTypeEnum.InternalNote).ToList();
            }

            if (!attachmentsDto.Any())
            {
                return null;
            }

            foreach (ClientSupportRequestMessageAttachmentDto attachmentDto in attachmentsDto)
            {
                attachmentDto.FileContent = this._clientSupportRequestService.Value.GetAttachmentContent(attachmentDto.ClientSupportRequestMessageAttachmentId);
            }

            if (attachmentsDto.Count == 1)
            {
                return new AttachmentDownloadDto
                {
                    Bytes = attachmentsDto.First().FileContent,
                    FileName = attachmentsDto.First().AttachmentFileName,
                    MimeType = attachmentsDto.First().MimeType
                };
            }

            // more than 1, zip
            byte[] bytes;
            using (ZipFile zipFile = new ZipFile())
            {
                foreach (ClientSupportRequestMessageAttachmentDto attachmentDto in attachmentsDto)
                {
                    string attachmentName = attachmentDto.AttachmentFileName;
                    string newAttachmentName = attachmentName;

                    int i = 1;
                    while (zipFile.Entries.Any(x => String.Equals(x.FileName, newAttachmentName, StringComparison.CurrentCultureIgnoreCase)))
                    {
                        string extension = Path.GetExtension(attachmentName);
                        newAttachmentName = extension != null ? attachmentName.Replace(extension, string.Format("({0}){1}", i, extension)) : string.Format("{0}({1})", attachmentName, i);
                        i++;
                    }

                    zipFile.AddEntry(newAttachmentName, attachmentDto.FileContent);
                }

                using (MemoryStream stream = new MemoryStream())
                {
                    zipFile.Save(stream);
                    stream.Seek(0, SeekOrigin.Begin);
                    stream.Flush();

                    bytes = stream.ToArray();
                }
            }

            return new AttachmentDownloadDto
            {
                Bytes = bytes,
                FileName = string.Format("{0}_attachments.zip", clientSupportRequest.ClientSupportRequestDisplayId),
                MimeType = "application/x-zip-compressed"
            };
        }
        
        public ClientSupportRequestDto CreateClientSupportRequest(CreateClientSupportRequestParameterDto createClientSupportRequestParameter)
        {
            ClientSupportRequest clientSupportRequest = new ClientSupportRequest
            {
                ClientSupportTopic = new ClientSupportTopic {ClientSupportTopicId = createClientSupportRequestParameter.ClientSupportTopicId },
                ContactPhone = createClientSupportRequestParameter.ContactPhone,
                VisitPayUser = new VisitPayUser {VisitPayUserId = createClientSupportRequestParameter.RequestorUserId}
            };
   
            if (createClientSupportRequestParameter.IsCreatedOnBehalfOfClient)
            {
                this._clientSupportRequestService.Value.CreateClientSupportRequestOnBehalfOfClient(clientSupportRequest, createClientSupportRequestParameter.MessageBody,createClientSupportRequestParameter.IvinciAdminUserId);
                this.SaveMessage(
                    clientSupportRequest.ClientSupportRequestId, 
                    createClientSupportRequestParameter.CreatedOnBefalfOfClientInternalNote, 
                    ClientSupportRequestMessageTypeEnum.InternalNote, 
                    createClientSupportRequestParameter.IvinciAdminUserId);
            }
            else
            {
                SupportRequest referenceSupportRequest = null;
                if (createClientSupportRequestParameter.ReferenceGuarantorSupportRequestId.HasValue)
                {
                    referenceSupportRequest = this._supportRequestService.Value.GetSupportRequest(createClientSupportRequestParameter.ReferenceGuarantorSupportRequestId.Value);
                }

                this._clientSupportRequestService.Value.CreateClientSupportRequest(clientSupportRequest, createClientSupportRequestParameter.MessageBody, referenceSupportRequest);

                if (referenceSupportRequest != null)
                {
                    this._supportRequestService.Value.SaveInternalNote(referenceSupportRequest.SupportRequestId, referenceSupportRequest.VpGuarantor, $"Reference Support Ticket {clientSupportRequest.ClientSupportRequestDisplayId}", createClientSupportRequestParameter.RequestorUserId);
                }
            }
            
            return Mapper.Map<ClientSupportRequestDto>(clientSupportRequest);
        }

        public ClientSupportRequestMessageAttachmentDto SaveAttachment(ClientSupportRequestMessageAttachmentDto attachmentDto, int clientSupportRequestMessageId, int clientSupportRequestId)
        {
            ClientSupportRequestMessageAttachment attachment = new ClientSupportRequestMessageAttachment
            {
                AttachmentFileName = attachmentDto.AttachmentFileName,
                FileSize = attachmentDto.FileSize,
                MimeType = attachmentDto.MimeType,
                QuarantinedBytes = attachmentDto.QuarantinedBytes,
                MalwareScanStatus = MalwareScanStatusEnum.NotScanned
            };

            //Initial save should only save file contents to QuarantinedBytes, until after scanning
            if (attachmentDto.FileContent != null && attachmentDto.FileContent.Length > 0)
            {
                attachment.QuarantinedBytes = attachmentDto.FileContent;
                attachment.FileContent = null;
            }

            this._clientSupportRequestService.Value.SaveAttachment(clientSupportRequestId, clientSupportRequestMessageId, attachment);

            //Send message telling service bus to scan attachments for malware
            this.SendClientSupportRequestMalwareScanMessage(clientSupportRequestId, clientSupportRequestMessageId);

            return Mapper.Map<ClientSupportRequestMessageAttachmentDto>(attachment);
        }

        private void SendClientSupportRequestMalwareScanMessage(int clientSupportRequestId, int clientSupportRequestMessageId)
        {
            ScanClientSupportRequestMessageForMalwareMessage message = null;
            try
            {
                message = new ScanClientSupportRequestMessageForMalwareMessage
                {
                    ClientSupportRequestId = clientSupportRequestId,
                    ClientSupportRequestMessageId = clientSupportRequestMessageId
                };

                this._session.Transaction.RegisterPostCommitSuccessAction(m =>
                {
                    this._bus.Value.PublishMessage(m).Wait();
                }, message);
            }
            catch
            {
                this.LoggingService.Value.Fatal(() => $"Failed to publish ScanClientSupportRequestMessageForMalwareMessage, Message = {JsonConvert.SerializeObject(message)}");
            }
        }

        public void DeleteAttachment(int clientSupportRequestId, int clientSupportRequestMessageAttachmentId)
        {
            this._clientSupportRequestService.Value.DeleteAttachment(clientSupportRequestId, clientSupportRequestMessageAttachmentId);
        }

        public IList<string> SaveDetails(int clientSupportRequestId, int? assignedToVisitPayUserId, int? targetVpGuarantorId, string jiraId, int visitPayUserId)
        {
            IList<string> errors = new List<string>();

            if (targetVpGuarantorId.HasValue)
            {
                Guarantor guarantor = this._guarantorService.Value.GetGuarantor(targetVpGuarantorId.Value);
                if (guarantor == null)
                {
                    targetVpGuarantorId = null;
                    errors.Add("Invalid VisitPay ID");
                }
            }

            this._clientSupportRequestService.Value.SaveDetails(clientSupportRequestId, assignedToVisitPayUserId, targetVpGuarantorId, jiraId, visitPayUserId);

            return errors;
        }

        public ClientSupportRequestMessageDto SaveMessage(int clientSupportRequestId, string messageBody, ClientSupportRequestMessageTypeEnum messageType, int visitPayUserId)
        {
            ClientSupportRequestMessage message = new ClientSupportRequestMessage
            {
                ClientSupportRequestMessageType = messageType,
                CreatedByVisitPayUser = new VisitPayUser {  VisitPayUserId = visitPayUserId },
                InsertDate = DateTime.UtcNow,
                MessageBody = messageBody
            };

            this._clientSupportRequestService.Value.SaveMessage(clientSupportRequestId, message);
            
            return Mapper.Map<ClientSupportRequestMessageDto>(message);
        }

        public void SetMessageRead(int clientSupportRequestId, ClientSupportRequestMessageTypeEnum clientSupportRequestMessageType)
        {
            this._clientSupportRequestService.Value.SetMessageRead(clientSupportRequestId, clientSupportRequestMessageType);
        }

        public ClientSupportRequestDto SetSupportRequestStatus(int supportRequestId, int visitPayUserId, ClientSupportRequestStatusEnum clientSupportRequestStatus, string message)
        {
            ClientSupportRequest clientSupportRequest = this._clientSupportRequestService.Value.SetSupportRequestStatus(supportRequestId, visitPayUserId, clientSupportRequestStatus, message);

            return Mapper.Map<ClientSupportRequestDto>(clientSupportRequest);
        }

        public IDictionary<int, string> GetAllClientUsersWithSupportRequest()
        {
            return this._clientSupportRequestService.Value.GetAllClientUsersWithSupportRequest();
        }

        public async Task<byte[]> ExportClientAdminSupportRequestsAsync(ClientSupportRequestFilterDto filterDto, string userName)
        {
            const int maxExportRecords = 10000;

            filterDto.Page = 1;
            filterDto.Rows = maxExportRecords;

            ClientSupportRequestResultsDto results = this.GetClientSupportRequests(filterDto);

            using (ExcelPackage package = new ExcelPackage())
            {
                string appSupportRequestName = string.Format("{0} ID", this._clientApplicationService.Value.GetClient().AppSupportRequestNamePrefix);
                System.Drawing.Image logo = await this._imageService.Value.GetExportLogoAsync();
                iVinciExcelExportBuilder builder = new iVinciExcelExportBuilder(package, "Support Tickets");

                builder.WithLogo(logo)
                    .AddInfoBlockRow(new ColumnBuilder(DateTime.UtcNow.ToString(Format.MonthDayYearFormat), "File Date:").WithStyle(StyleTypeEnum.AlignLeft))
                    .AddInfoBlockRow(new ColumnBuilder(userName, "Exported By:").WithStyle(StyleTypeEnum.AlignLeft))
                    .AddDataBlock(results.ClientSupportRequests, dto => new List<Column>
                    {
                        new ColumnBuilder(dto.ClientSupportRequestStatus == ClientSupportRequestStatusEnum.Closed ? "Y" : "N", "Closed").AlignCenter(),
                        new ColumnBuilder(dto.AssignedVisitPayUser == null ? "Unassigned" : dto.AssignedVisitPayUser.DisplayFirstNameLastName, "Assigned To"),
                        new ColumnBuilder(dto.ClientSupportRequestDisplayId, "Case ID"),
                        new ColumnBuilder(dto.InsertDate.ToString(Format.DateTimeFormat), "Created"),
                        new ColumnBuilder(dto.ClientSupportRequestStatus.GetDescription(), "Status"),
                        new ColumnBuilder(dto.VisitPayUser.DisplayFirstNameLastName, "Requestor"),
                        new ColumnBuilder(dto.TopicDisplay, "Topic - Sub Topic"),
                        new ColumnBuilder(dto.JiraId, "Jira ID"),
                        new ColumnBuilder(dto.TargetVpGuarantorId == null ? "" : dto.TargetVpGuarantorId.Value.ToString(), appSupportRequestName),
                        new ColumnBuilder(dto.LastModifiedDate.HasValue ? dto.LastModifiedDate.Value.ToString(Format.DateTimeFormat) : "", "Last Modified Date"),
                        new ColumnBuilder(dto.LastModifiedByVisitPayUser.DisplayFirstNameLastName, "Last Modified By")
                    })
                    .WithNoRecordsFoundMessageOf("There are no support tickets to display at this time.")
                    .Build();

                return package.GetAsByteArray();
            }
        }
    }
}
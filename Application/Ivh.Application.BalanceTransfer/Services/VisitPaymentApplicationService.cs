﻿namespace Ivh.Application.BalanceTransfer.Services
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Base.Common.Interfaces;
    using Base.Services;
    using Common.Interfaces;
    using Ivh.Common.Base.Enums;
    using Ivh.Common.ServiceBus.Attributes;
    using Ivh.Common.ServiceBus.Enums;
    using Ivh.Common.VisitPay.Enums;
    using Ivh.Common.VisitPay.Messages.BalanceTransfer;
    using Ivh.Common.VisitPay.Messages.FinanceManagement;
    using Ivh.Common.VisitPay.Messages.HospitalData;
    using MassTransit;

    public class VisitPaymentApplicationService : ApplicationService, IVisitPaymentApplicationService
    {
        public VisitPaymentApplicationService(Lazy<IApplicationServiceCommonService> applicationServiceCommonService) : base(applicationServiceCommonService)
        {
        }
        // visit's first time through this service, it will be marked as Eligible.
        // Write logic to determine if it can go from Eligible to Active.
        // if it can go to Acitve: 
        //      1. send the BT flag to Epic.  (do we need to do this after we have confirmation that VP set the flag to Active?)
        //      2. send message to VP that it is Active.
        // $$ goes to Client as cash

        // all other times through this service, it will be marked as Active (happy path, people)
        // Write logic to determine where the money goes
        //      1. if entire transaction is within reserve
        //          send $$ to SME
        //      2. if entire transaction is outside reserve
        //          send $$ to Client
        //      3. if transaction spans reserve
        //          send last of reserve $$ to SME
        //          send rest $$ to Client

        public void ProcessFinancePlanPayment(BalanceTransferFinancePlanPaymentSettledMessage message)
        {
            if (message.FinancePlan != null && this.IsFinancePlanMeetingQualifications(message.FinancePlan)
                && message.Payment.PaymentType == PaymentTypeEnum.RecurringPaymentFinancePlan)
            {
                List<BalanceTransferVisitMessage> eligibleVisits = message.Visits
                    .Where(x => x.BalanceTransferStatusId == (int)BalanceTransferStatusEnum.Eligible)
                    .ToList();
                foreach (BalanceTransferVisitMessage visit in eligibleVisits)
                {
                    this.EligibleForBalanceTransferActive(visit);
                }
            }

            //payment stuff
        }

        private void EligibleForBalanceTransferActive(BalanceTransferVisitMessage visit)
        {
            //more business rules to come in VPNG-20103
            if ((BalanceTransferStatusEnum)visit.BalanceTransferStatusId == BalanceTransferStatusEnum.Eligible)
            {
                this.PublishBalanceTransferSetVisitActiveMessage(visit.VisitSourceSystemKey, visit.VisitBillingSystemId);
                this.PublishOutboundVisitMessage(visit, VpOutboundVisitMessageTypeEnum.BalanceTransferAgencyAssume);
            }
        }

        private bool IsFinancePlanMeetingQualifications(BalanceTransferFinancePlanMessage financePlan)
        {
            FinancePlanStatusEnum financePlanStatusEnum = (FinancePlanStatusEnum)financePlan.FinancePlanStatusId;
            bool onFinancePlan = financePlanStatusEnum == FinancePlanStatusEnum.GoodStanding ||
                                 financePlanStatusEnum == FinancePlanStatusEnum.PastDue ||
                                 financePlanStatusEnum == FinancePlanStatusEnum.UncollectableActive ||
                                 financePlanStatusEnum == FinancePlanStatusEnum.TermsAccepted;

            if (!onFinancePlan)
            {
                return false;
            }

            int minDuration = this.ClientService.Value.GetClient().BalanceTransferFinancePlanMinDuration;
            int maxDuration = this.ClientService.Value.GetClient().BalanceTransferFinancePlanMaxDuration;
            return financePlan.DurationRangeStart >= minDuration && financePlan.DurationRangeEnd <= maxDuration;
        }

        private void PublishOutboundVisitMessage(BalanceTransferVisitMessage visit, VpOutboundVisitMessageTypeEnum vpOutboundVisitMessageTypeEnum)
        {
            OutboundVisitMessage message = new OutboundVisitMessage
            {
                VpVisitId = visit.VisitId,
                BillingSystemId = visit.VisitBillingSystemId,
                SourceSystemKey = visit.VisitSourceSystemKey,
                VpOutboundVisitMessageType = vpOutboundVisitMessageTypeEnum,
                ActionDate = DateTime.UtcNow
            };
            this.Bus.Value.PublishMessage(message).Wait();
        }


        private void PublishBalanceTransferSetVisitActiveMessage(string visitSourceSystemKey, int visitBillingSystemId)
        {
            BalanceTransferSetVisitActiveMessage message = new BalanceTransferSetVisitActiveMessage
            {
                VisitSourceSystemKey = visitSourceSystemKey,
                VisitBillingSystemId = visitBillingSystemId
            };
            this.Bus.Value.PublishMessage(message).Wait();
        }
        [UniversalConsumer(UniversalQueuesEnum.BalanceTransferProcessor, UniversalPriorityEnum.Priority30)]
        public void ConsumeMessage(BalanceTransferFinancePlanPaymentSettledMessage message, ConsumeContext<BalanceTransferFinancePlanPaymentSettledMessage> consumeContext)
        {
            this.ProcessFinancePlanPayment(message);
        }

        public bool IsMessageValid(BalanceTransferFinancePlanPaymentSettledMessage message, ConsumeContext<BalanceTransferFinancePlanPaymentSettledMessage> consumeContext)
        {
            bool isValid = true;
            return isValid;
        }
    }
}

﻿using Autofac;

namespace Ivh.Application.BalanceTransfer.Modules
{
    using Common.Interfaces;
    using Services;

    public class Module : Autofac.Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<VisitPaymentApplicationService>().As<IVisitPaymentApplicationService>();
        }

    }
}
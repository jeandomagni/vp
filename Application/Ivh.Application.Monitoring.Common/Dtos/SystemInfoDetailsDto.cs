﻿namespace Ivh.Application.Monitoring.Common.Dtos
{
    using System;
    using System.Runtime.Serialization;

    [Serializable]
    [DataContract]
    public class SystemInfoDetailsDto
    {
        [DataMember(Name = "client")]
        public string Client { get; set; }

        [DataMember(Name = "environment")]
        public string Environment { get; set; }

        [DataMember(Name = "application")]
        public string Application { get; set; }

        [DataMember(Name = "app_version")]
        public string AppVersion { get; set; }

        [DataMember(Name = "clientdata_api_version")]
        public string ClientDataApiVersion { get; set; }

        [DataMember(Name = "securepan_api_version")]
        public string SecurePanApiVersion { get; set; }

        [DataMember(Name = "client_settings_hash")]
        public string ClientSettingsHash { get; set; }

        [DataMember(Name = "securepan_url")]
        public string SecurePanUrl { get; set; }
    }
  
}

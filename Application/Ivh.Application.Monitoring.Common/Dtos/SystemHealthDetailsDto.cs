﻿namespace Ivh.Application.Monitoring.Common.Dtos
{
    using System;
    using System.Collections.Generic;
    using System.Runtime.Serialization;

    [Serializable]
    [DataContract]
    public class SystemHealthDetailsDto : SystemInfoDetailsDto
    {
        [DataMember(Name = "release_build")]
        public bool ReleaseBuild { get; set; }
        [DataMember(Name = "queue")]
        public bool Queue { get; set; }
        [DataMember(Name = "cache")]
        public bool Cache => this.MemoryCache && this.DistributedCache;
        [DataMember(Name = "memory_cache")]
        public bool MemoryCache { get; set; }
        [DataMember(Name = "distributed_cache")]
        public bool DistributedCache { get; set; }
        [DataMember(Name = "app_connect")]
        public bool ApplicationConnect { get; set; }
        [DataMember(Name = "guestpay_connect")]
        public bool GuestPayConnect { get; set; }
        [DataMember(Name = "log_connect")]
        public bool LogConnect { get; set; }
        [DataMember(Name = "storage_connect")]
        public bool StorageConnect { get; set; }
        [DataMember(Name = "quartz_connect")]
        public bool QuartzConnect { get; set; }
        [DataMember(Name = "cdietl_connect")]
        public bool CdiEtlConnect { get; set; }
        [DataMember(Name = "settings")]
        public bool Settings { get; set; }
        [DataMember(Name = "content")]
        public bool Content { get; set; }
        [DataMember(Name = "universal_processor")]
        public bool UniversalProcessor { get; set; }
        [DataMember(Name = "scoring_processor")]
        public bool ScoringProcessor { get; set; }
        [DataMember(Name = "client_reports_server")]
        public bool ClientReportsServer { get; set; }
        [DataMember(Name = "guestpay_api")]
        public bool GuestPayApi { get; set; }
        [DataMember(Name = "guestpay_api_response")]
        public IDictionary<string, string> GuestPayApiResponse { get; set; }
        [DataMember(Name = "payment_api")]
        public bool PaymentApi { get; set; }
        [DataMember(Name = "payment_api_response")]
        public IDictionary<string, string> PaymentApiResponse { get; set; }
        [DataMember(Name = "securepan_api")]
        public bool SecurePanApi { get; set; }
        [DataMember(Name = "securepan_api_response")]
        public IDictionary<string, string> SecurePanApiResponse { get; set; }

        public bool IsHealthy()
        {
            return this.ReleaseBuild
                   || this.Queue
                   || this.Cache
                   || this.MemoryCache
                   || this.DistributedCache
                   || this.ApplicationConnect
                   || this.GuestPayConnect
                   || this.LogConnect
                   || this.StorageConnect
                   || this.QuartzConnect
                   || this.CdiEtlConnect
                   || this.Settings
                   || this.Content
                   || this.UniversalProcessor
                   || this.ScoringProcessor
                   || this.ClientReportsServer
                   || this.GuestPayApi;

        }
    }
}

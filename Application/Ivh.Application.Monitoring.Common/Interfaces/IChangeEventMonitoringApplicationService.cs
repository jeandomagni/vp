namespace Ivh.Application.Monitoring.Common.Interfaces
{
    using System.Threading.Tasks;
    using Ivh.Common.Base.Interfaces;

    public interface IChangeEventMonitoringApplicationService : IApplicationService
    {
        Task SendChangeEventMonitors();
    }
}
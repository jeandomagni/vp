﻿namespace Ivh.Application.Monitoring.Common.Interfaces
{
    using System.Threading.Tasks;
    using Ivh.Common.Base.Interfaces;

    public interface IUncollectableMonitoringApplicationService : IApplicationService
    {
        Task SendUncollectableClosedActualVsExpected();
        Task SendUncollectableActiveActualVsExpected();
        Task SendClosedUncollectableFinancePlans();
    }
}
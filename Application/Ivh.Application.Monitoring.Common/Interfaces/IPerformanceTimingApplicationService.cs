﻿namespace Ivh.Application.Monitoring.Common.Interfaces
{
    using System;
    using Ivh.Common.Base.Interfaces;
    using Ivh.Common.ServiceBus.Interfaces;
    using Ivh.Common.VisitPay.Messages.Monitoring;

    public interface IPerformanceTimingApplicationService : IApplicationService,
        IUniversalConsumerService<ControllerTimingMessage>,
        IUniversalConsumerService<ControllerTimingMessages>
    {
        void LogControllerTiming(string url, string requestIdenfitier, DateTime timeActionExecuting, DateTime timeActionExecuted, DateTime timeResultExecuted);
    }
}
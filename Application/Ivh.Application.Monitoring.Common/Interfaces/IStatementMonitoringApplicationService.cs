﻿namespace Ivh.Application.Monitoring.Common.Interfaces
{
    using System.Threading.Tasks;
    using Ivh.Common.Base.Interfaces;

    public interface IStatementMonitoringApplicationService : IApplicationService
    {
        Task SendStatementActualVsExpected();
        Task SendStatementFPLineVsFPBalanceMismatchCount();
        Task SendStatementLineMismatchCount();

    }
}
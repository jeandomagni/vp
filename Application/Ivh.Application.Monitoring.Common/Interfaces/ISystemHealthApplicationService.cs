﻿namespace Ivh.Application.Monitoring.Common.Interfaces
{
    using Dtos;
    using Ivh.Common.Base.Interfaces;
    using Ivh.Common.ServiceBus.Interfaces;
    using Ivh.Common.VisitPay.Messages.Monitoring.SystemHealth;
    using Ivh.Common.VisitPay.Messages.Monitoring.SystemHealth.ProcessorHealth;

    public interface ISystemHealthApplicationService : IApplicationService,
        IUniversalConsumerService<ProcessorHealthMessage>,
        IUniversalConsumerService<SystemHealthCheckTestMessage>
    {
        SystemHealthDetailsDto GetSystemHealthDetails();
        SystemInfoDetailsDto GetSystemInfoDetails();
        bool SystemIsHealthy();
    }
}

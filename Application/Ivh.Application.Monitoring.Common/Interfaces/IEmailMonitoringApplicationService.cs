namespace Ivh.Application.Monitoring.Common.Interfaces
{
    using System.Threading.Tasks;
    using Ivh.Common.Base.Interfaces;

    public interface IEmailMonitoringApplicationService : IApplicationService
    {
        Task SendEmailPastDueExpectedVsActual();
        Task SendEmailFinalPastDueExpectedVsActual();
        Task SendEmailPaymentConfirmationExpectedVsActual();
        Task SendEmailPaymentDueDateChangeExpectedVsActual();
        Task SendEmailStatementNotificationExpectedVsActual();
        Task SendEmailUncollectableExpectedVsActual();
        Task SendEmailFailedPaymentsExpectedVsActual();
        Task SendEmailAccountCancellation();
        Task SendEmailFinancePlanDurationIncrease();
    }
}
﻿namespace Ivh.Application.Monitoring.Common.Interfaces
{
    using System.Threading.Tasks;
    using Ivh.Common.Base.Interfaces;

    public interface IPaymentMonitoringApplicationService : IApplicationService
    {
        Task SendPaymentActualVsExpected();
        Task SendACHClosedFailedActualVsExpected();
        Task SendACHClosedPaidActualVsExpected();
        Task SendACHReturnReportCount();
        Task SendACHSettlementReportCount();
    }
}
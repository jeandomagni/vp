﻿namespace Ivh.Application.Monitoring.Common.Interfaces
{
    using System;
    using System.Collections.Generic;
    using Domain.Monitoring.Entities;
    using Domain.Monitoring.Interfaces;
    using Ivh.Common.Base.Interfaces;
    using Ivh.Common.JobRunner.Common.Interfaces;
    using Ivh.Common.VisitPay.Enums;
    using Ivh.Common.VisitPay.Jobs;

    public interface IMonitoringApplicationService : IApplicationService,
        IJobRunnerService<PublishMonitorDataJobRunner>

    {
        IMonitor GetMonitor(MonitorEnum monitorEnum, DateTime dateTimeBegin, DateTime dateTimeEnd, bool publish = false);
        IList<IMonitor> GetMonitors(IEnumerable<MonitorEnum> monitorEnums, DateTime dateTimeBegin, DateTime dateTimeEnd, bool publish = false);
        T GetDatum<T>(DatumEnum datumEnum, DateTime dateTimeBegin, DateTime dateTimeEnd, bool publish = false);
        T GetDatum<T>(DatumEnum datumEnum, Func<T> value, bool publish = false);
        IEnumerable<T> GetData<T>(IEnumerable<DatumEnum> datumEnum, DateTime dateTimeBegin, DateTime dateTimeEnd, bool publish = false);
        IEnumerable<T> GetData<T>(IDictionary<DatumEnum,Func<T>> data, bool publish = false);
        void PublishData(IEnumerable<DatumEnum> datumEnum, DateTime dateTimeBegin, DateTime dateTimeEnd, bool continueOnError = true);
    }
}

﻿namespace Ivh.Application.DataCorrectionUtility.Common.Interfaces
{
    using System.Collections.Generic;
    using Ivh.Common.Base.Enums;
    using Ivh.Common.Base.Interfaces;
    using Ivh.Common.VisitPay.Enums;

    public interface IDataCorrectionApplicationService : IApplicationService
    {
        void RunDataCorrection(DataCorrectionTypeEnum dataCorrectionType, IEnumerable<string> arguments);
        void InsertAdjustingTransaction(IEnumerable<string> arguments);
        void DownloadAchSettlementsAndReturns(IEnumerable<string> arguments);
    }
}

﻿using System.Collections.Generic;
using System.Linq;
using Ivh.Application.SettingsManager.Common.Interfaces;
using Ivh.Application.SettingsManager.Common.Models;
using Ivh.Common.Base.Core.Utilities.Helpers;
using Ivh.Domain.SettingsManager.Common.Models;
using Ivh.Domain.SettingsManager.Constants;
using Ivh.Domain.SettingsManager.Interfaces;

namespace Ivh.Application.SettingsManager.Services
{
    public class SettingsManagerApplicationService : ISettingsManagerApplicationService
    {
        private readonly ISettingsManagerService _settingsManagerService;

        public SettingsManagerApplicationService(ISettingsManagerService settingsManagerService)
        {
            this._settingsManagerService = settingsManagerService;
        }

        #region Diff

        public List<string> GetSettingsDiffLine(int clientId, int environmentId, int releaseId)
        {
            // Settings must be sorted by release, then type, then name.
            // This is necessary so the current DIFF implementation (jsdifflib) knows something was added to the end of the list.
            // Otherwise, it shows that a block of lines were changed starting at the new item's position in the list.
            List<string> result = this._settingsManagerService.GetSettings(clientId, environmentId, releaseId)
                //.OrderBy(x => x.SinceRelease)
                //.ThenBy(x => x.SettingType)
                //.ThenBy(x => x.Name)
                .OrderBy(x => x.Name)
                .Select(x => x.Name + " : " + x.Value)
                .ToList();
            return result;
        }

        public List<string> GetSettingsDiffLineFromServer(int clientId, int environmentId)
        {
            // Settings must be sorted by release, then type, then name.
            // This is necessary so the current DIFF implementation (jsdifflib) knows something was added to the end of the list.
            // Otherwise, it shows that a block of lines were changed starting at the new item's position in the list.
            List<string> result = this._settingsManagerService.GetSettingsFromServer(clientId, environmentId)
                //.OrderBy(x => x.SinceRelease)
                //.ThenBy(x => x.SettingType)
                //.ThenBy(x => x.Name)
                .OrderBy(x => x.Name)
                .Select(x => x.Name + " : " + x.Value)
                .ToList();
            return result;
        }

        #endregion

        #region Hash Code

        public string GetSettingsHashCode(int clientId, int environmentId, int releaseId)
        {
            IQueryable<ClientSetting> clientSettings = this._settingsManagerService.GetClientSettingsById(clientId, environmentId, releaseId).OrderBy(x => x.ClientSettingKey);
            string hashCode = this._settingsManagerService.GetSettingsHashCode(clientSettings);
            return hashCode;
        }

        public string GetLatestApprovedSettingsHashCode(int clientId, int environmentId, int releaseId)
        {
            return this._settingsManagerService.GetLatestApprovedSettingsHashCode(clientId, environmentId, releaseId);
        }

        #endregion

        #region Sql Merge Script

        public string GetClientSettingsMergeScriptForDeveloper(int clientId, int environmentId, int releaseId)
        {
            IQueryable<ClientSetting> clientSettings = this._settingsManagerService.GetClientSettingsById(clientId, environmentId, releaseId).OrderBy(x => x.ClientSettingKey);
            string hashCode = this._settingsManagerService.GetSettingsHashCode(clientSettings);
            string mergeScript = MergeScriptHelper.CreateMergeScript("common", "ClientSetting", "ClientSettingKey", clientSettings.ToList(), hashCode);
            return mergeScript;
        }

        public string GetClientSettingsMergeScript(string client, string environment, string release)
        {
            IQueryable<ClientSetting> clientSettings = this._settingsManagerService.GetClientSettings(client, environment, release).OrderBy(x => x.ClientSettingKey);
            string hashCode = this._settingsManagerService.GetSettingsHashCode(clientSettings);
            string mergeScript = MergeScriptHelper.CreateMergeScript("common", "ClientSetting", "ClientSettingKey", clientSettings.ToList(), hashCode, true);
            return mergeScript;
        }       

        #endregion

        #region Client Environment Release Lookup Values

        public IEnumerable<KeyValuePair<int, string>> GetReleases(bool includeServerOption = false)
        {
            if (includeServerOption)
            {
                List<KeyValuePair<int, string>> releaseList = this._settingsManagerService.GetReleases().ToList();
                releaseList.Insert(0, new KeyValuePair<int, string>(SettingsSource.Server, "Server"));
                return releaseList.AsEnumerable();
            }
            else
            {
                return this._settingsManagerService.GetReleases();
            }
        }

        public IEnumerable<KeyValuePair<int, string>> GetClients(bool includeAllOption = false)
        {
            if (includeAllOption)
            {
                List<KeyValuePair<int, string>> clientList = this._settingsManagerService.GetClients().ToList();
                clientList.Insert(0, new KeyValuePair<int, string>(0, "All"));
                return clientList.AsEnumerable();
            } else
            {
                return this._settingsManagerService.GetClients();
            }           
        }

        public IEnumerable<KeyValuePair<int, string>> GetEnvironments(bool includeAllOption = false)
        {
            if (includeAllOption)
            {
                List<KeyValuePair<int, string>> environmentList = this._settingsManagerService.GetEnvironments().ToList();
                environmentList.Insert(0, new KeyValuePair<int, string>(0, "All"));
                return environmentList.AsEnumerable();
            }
            else
            {
                return this._settingsManagerService.GetEnvironments();
            }
        }

        public string GetLatestReleaseLabel()
        {
            return this._settingsManagerService.GetLatestReleaseLabel();
        }

        #endregion

        #region Setting Lookup Values
        
        public IEnumerable<KeyValuePair<int, string>> GetSettingTypes()
        {
            return this._settingsManagerService.GetSettingTypes();
        }

        public IEnumerable<KeyValuePair<int, string>> GetSettingNames()
        {
            return this._settingsManagerService.GetSettingNames();
        }

        public IEnumerable<KeyValuePair<string, string>> GetSettingDataTypes()
        {
            return this._settingsManagerService.GetSettingDataTypes();
        }

        #endregion

        #region Get Settings Minimum

        public List<SettingMinimum> GetSettingsMin(int clientId, int environmentId, int releaseId)
        {
            List<SettingMinimum> result = this._settingsManagerService.GetSettings(clientId, environmentId, releaseId)
                .OrderBy(x => x.SettingType).ThenBy(x => x.Name)
                .Select(x => new SettingMinimum(){ Name = x.Name, Value = x.Value })
                .ToList();
            return result;
        }      

        #endregion

        #region Get SettingsComposite

        public List<SettingComposite> GetSettingsMax(int clientId, int environmentId, int releaseId)
        {
            List<SettingComposite> result = this._settingsManagerService.GetSettings(clientId, environmentId, releaseId)
                .OrderBy(x => x.SettingType).ThenBy(x => x.Name)
                .ToList();
            return result;
        }

        #endregion

        #region Get BootGrid SettingComposite

        public BootGridResponse<SettingComposite> GetSettingsMaxBootgrid(BootGridRequest model)
        {
            return this._settingsManagerService.GetSettingsMaxBootgrid(model);
        }

        #endregion

        #region Add Settings

        public void AddSetting(SettingInput model)
        {
            this._settingsManagerService.AddSetting(model);
        }

        #endregion

        #region Add Setting Override

        public void AddSettingOverride(SettingOverrideInput model)
        {
            this._settingsManagerService.AddSettingOverride(model);
        }

        #endregion

        #region Client Release Environment Approval

        public void AddClientReleaseEnvironmentApproval(DeploymentContextApproval deploymentContextApproval)
        {
            this._settingsManagerService.AddClientReleaseEnvironmentApproval(deploymentContextApproval);
        }

        public DeploymentContextApprovalStatus GetDeploymentContextApprovalStatus(string client, string environment, string release)
        {
            DeploymentContext deploymentContext = this._settingsManagerService.GetDeploymentContext(client, environment, release);
            string currentHashCode = this.GetSettingsHashCode(deploymentContext.ClientId, deploymentContext.EnvironmentId, deploymentContext.ReleaseId);
            string approvedHashCode = this.GetLatestApprovedSettingsHashCode(deploymentContext.ClientId, deploymentContext.EnvironmentId, deploymentContext.ReleaseId);
            bool isSuccess = !string.IsNullOrWhiteSpace(currentHashCode) && !string.IsNullOrWhiteSpace(approvedHashCode) && currentHashCode == approvedHashCode;
            string errorMessage = "";
            if (!isSuccess)
            {
                if (string.IsNullOrWhiteSpace(approvedHashCode))
                {
                    errorMessage = "No approval was found";
                }
                else
                {
                    errorMessage = "Settings have changed since they were approved";
                }
            }

            DeploymentContextApprovalStatus status = new DeploymentContextApprovalStatus()
            {
                CurrentHashCode = currentHashCode,
                ApprovedHashCode = approvedHashCode,
                IsSuccess = isSuccess,
                ErrorMessage = errorMessage
            };

            return status;
        }  

        #endregion
    }
}

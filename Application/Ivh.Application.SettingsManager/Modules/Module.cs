﻿namespace Ivh.Application.SettingsManager.Modules
{
    using Common.Interfaces;
    using Microsoft.Extensions.DependencyInjection;
    using Services;

    public static class Module
    {
        public static void RegisterSettingsManagerApplicationServices(this IServiceCollection serviceCollection)
        {
            serviceCollection.AddTransient<ISettingsManagerApplicationService, SettingsManagerApplicationService>();
        }
    }
}

﻿namespace Ivh.Application.DataCorrectionUtility.Mappings
{
    using AutoMapper;
    using Domain.HospitalData.HsGuarantor.Entities;
    using Domain.HospitalData.Visit.Entities;
    using Domain.HospitalData.VpGuarantor.Entities;
    using Ivh.Common.VisitPay.Messages.HospitalData.Dtos;
    using VisitInsurancePlan = Domain.Visit.Entities.VisitInsurancePlan;

    public class AutomapperProfile : Profile
    {
        public AutomapperProfile()
        {
            this.CreateMap<Visit, VisitChangeDto>().ForMember(dest => dest.VisitInsurancePlans, opts => opts.Ignore());
            this.CreateMap<VisitTransaction, VisitTransactionChangeDto>();
            this.CreateMap<InsurancePlan, InsurancePlanDto>();
            this.CreateMap<VisitInsurancePlan, VisitInsurancePlanChangeDto>();
            this.CreateMap<LifeCycleStage, LifeCycleStageDto>();
            this.CreateMap<PatientType, PatientTypeDto>();
            this.CreateMap<RevenueWorkCompany, RevenueWorkCompanyDto>();
            this.CreateMap<PrimaryInsuranceType, PrimaryInsuranceTypeDto>();
            this.CreateMap<HsGuarantor, HsGuarantorDto>();
            this.CreateMap<VpGuarantorHsMatch, VpGuarantorHsMatchDto>();
        }
    }
}
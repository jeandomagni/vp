﻿namespace Ivh.Application.DataCorrectionUtility.Services
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.Linq;
    using AutoMapper;
    using Base.Common.Interfaces;
    using Base.Services;
    using Common.Interfaces;
    using Core.Common.Interfaces;
    using Domain.User.Interfaces;
    using Domain.HospitalData.HsGuarantor.Entities;
    using Domain.HospitalData.HsGuarantor.Interfaces;
    using Domain.HospitalData.Scoring.Interfaces;
    using Domain.HospitalData.Visit.Interfaces;
    using Domain.HospitalData.VpGuarantor.Entities;
    using FinanceManagement.Common.Interfaces;
    using HospitalData.Common.Interfaces;
    using Ivh.Common.Base.Utilities.Extensions;
    using Ivh.Common.Base.Utilities.Helpers;
    using Ivh.Common.Data;
    using Ivh.Common.Data.Connection.Connections;
    using Ivh.Common.Data.Interfaces;
    using Ivh.Common.EventJournal;
    using Ivh.Common.VisitPay.Constants;
    using Ivh.Common.VisitPay.Enums;
    using Ivh.Common.VisitPay.Messages.HospitalData;
    using Ivh.Common.VisitPay.Messages.HospitalData.Dtos;
    using Ivh.Common.VisitPay.Strings;
    using NHibernate;
    using Visit = Domain.Visit.Entities.Visit;
    using Ivh.Domain.HospitalData.Eob.Interfaces;
    using Ivh.Domain.FinanceManagement.Interfaces;
    using Ivh.Domain.FinanceManagement.Payment.Interfaces;
    using Ivh.Domain.FinanceManagement.Payment.Entities;
    using GuestPay = Ivh.Domain.GuestPay;

    public class DataCorrectionApplicationService : ApplicationService, IDataCorrectionApplicationService
    {
        private readonly Lazy<IAchApplicationService> _achApplicationService;
        private readonly Lazy<IEob835Service> _eob835Service;
        private readonly ISession _etlSession;
        private readonly Lazy<IHsGuarantorScoringApplicationService> _hsGuarantorApplicationScoringService;
        private readonly Lazy<IHsGuarantorService> _hsGuarantorService;
        private readonly Lazy<IVisitService> _hsVisitService;
        private readonly ISession _logSession;
        private readonly Lazy<IPaymentService> _paymentService;
        private readonly Lazy<GuestPay.Interfaces.IPaymentService> _guestPayPaymentService;
        private readonly Lazy<IScoringGuarantorMatchingService> _scoringGuarantorMatchingService;
        private readonly ISession _session;
        private readonly Lazy<IVisitPayUserJournalEventService> _userJournalEventService;
        private readonly Lazy<IVisitCreationApplicationService> _visitCreationApplicationService;
        private readonly Lazy<Ivh.Domain.Visit.Interfaces.IVisitService> _visitService;
        private readonly Lazy<IVpGuarantorHsMatchService> _vpGuarantorHsMatchService;

        public DataCorrectionApplicationService(
            Lazy<IApplicationServiceCommonService> applicationServiceCommonService,
            Lazy<Ivh.Domain.Visit.Interfaces.IVisitService> visitService,
            Lazy<IVisitService> hsVisitService,
            Lazy<IHsGuarantorService> hsGuarantorService,
            Lazy<IHsGuarantorScoringApplicationService> hsGuarantorApplicationScoringService,
            Lazy<IScoringGuarantorMatchingService> scoringGuarantorMatchingService,
            Lazy<IVpGuarantorHsMatchService> vpGuarantorHsMatchService,
            Lazy<IVisitPayUserJournalEventService> userJournalEventService,
            Lazy<IVisitCreationApplicationService> visitCreationApplicationService,
            Lazy<IAchApplicationService> achApplicationService,
            Lazy<IEob835Service> eob835Service,
            Lazy<IPaymentService> paymentService,
            Lazy<GuestPay.Interfaces.IPaymentService> guestPayPaymentService,
            ISessionContext<VisitPay> sessionContext,
            ISessionContext<CdiEtl> etlSession,
            ISessionContext<Logging> logSession) : base(applicationServiceCommonService)
        {
            this._visitService = visitService;
            this._hsVisitService = hsVisitService;
            this._session = sessionContext.Session;
            this._etlSession = etlSession.Session;
            this._logSession = logSession.Session;
            this._vpGuarantorHsMatchService = vpGuarantorHsMatchService;
            this._userJournalEventService = userJournalEventService;
            this._visitCreationApplicationService = visitCreationApplicationService;
            this._achApplicationService = achApplicationService;
            this._hsGuarantorService = hsGuarantorService;
            this._hsGuarantorApplicationScoringService = hsGuarantorApplicationScoringService;
            this._scoringGuarantorMatchingService = scoringGuarantorMatchingService;
            this._eob835Service = eob835Service;
            this._paymentService = paymentService;
            this._guestPayPaymentService = guestPayPaymentService;
        }

        [Obsolete(ObsoleteDescription.VisitTransactions)]
        public void RunDataCorrection(DataCorrectionTypeEnum dataCorrectionType, IEnumerable<string> arguments)
        {
            switch (dataCorrectionType)
            {
                case DataCorrectionTypeEnum.InsertAdjustingTransaction:
                    this.InsertAdjustingTransaction(arguments);
                    break;
                case DataCorrectionTypeEnum.AchDownload:
                    this.DownloadAchSettlementsAndReturns(arguments);
                    break;
                case DataCorrectionTypeEnum.RetroScoring:
                    this.ExecuteRetroScoring(arguments);
                    break;
                case DataCorrectionTypeEnum.MigrateHsEobToAppEob:
                    this.MigrateHsEobToAppEob(arguments);
                    break;
                case DataCorrectionTypeEnum.SendOptimisticAchSettlements:
                    this.SendOptimisticAchSettlements(arguments);
                    break;
                case DataCorrectionTypeEnum.Unknown:
                default:
                    throw new ArgumentOutOfRangeException(nameof(dataCorrectionType), dataCorrectionType, null);
            }
        }

        #region Data Correction Methods

        public void DownloadAchSettlementsAndReturns(IEnumerable<string> arguments)
        {
            IList<string> enumerable = arguments as IList<string> ?? arguments.ToList();
            if (enumerable.Count() < 2)
            {
                Console.WriteLine("Insufficient number of arguments: ");
                Console.WriteLine("{0} {1} {1}", DataCorrectionTypeEnum.AchDownload, Format.DateFormat2);
                return;
            }

            DateTime startDateTimeOffset = DateTime.ParseExact(enumerable[0], new[] { Format.FolderDate, Format.DateFormat2 }, CultureInfo.InvariantCulture, DateTimeStyles.None);
            DateTime endDateTimeOffset = DateTime.ParseExact(enumerable[1], new[] { Format.FolderDate, Format.DateFormat2 }, CultureInfo.InvariantCulture, DateTimeStyles.None);

            if (startDateTimeOffset.Date > endDateTimeOffset.Date)
            {
                Console.WriteLine("Begin date must be less than or equal to end date.");
                return;
            }

            this._achApplicationService.Value.DownloadAchReturnsAndSettlements(startDateTimeOffset, endDateTimeOffset);
        }

        [Obsolete(ObsoleteDescription.VisitCurrentBalance)]
        public void InsertAdjustingTransaction(IEnumerable<string> arguments)
        {
            IList<string> enumerable = arguments as IList<string> ?? arguments.ToList();
            if (enumerable.Count() < 6)
            {
                Console.WriteLine("Insufficient number of arguments: ");
                Console.WriteLine("{0} VpGuarantorId VisitId TransactionAmount VpTransactionType LogMessage UserName", DataCorrectionTypeEnum.InsertAdjustingTransaction);
                return;
            }
            int vpGuarantorId = 0;
            int visitId = 0;
            decimal transactionAmount = 0;
            VpTransactionTypeEnum vpTransactionType;
            string message = enumerable[4];
            string userName = enumerable[5];

            if (int.TryParse(enumerable[0], out vpGuarantorId)
                && int.TryParse(enumerable[1], out visitId)
                && decimal.TryParse(enumerable[2], out transactionAmount)
                && Enum.TryParse(enumerable[3], out vpTransactionType))
            {
                using (UnitsOfWork unitsOfWork = new UnitsOfWork(this._session, this._etlSession, this._logSession))
                {
                    Visit visit = this._visitService.Value.GetVisit(vpGuarantorId, visitId);

                    if (visit != null)
                    {
                        Domain.HospitalData.Visit.Entities.Visit hsVisit = this._hsVisitService.Value.GetVisit(visit.BillingSystem.BillingSystemId, visit.SourceSystemKey);
                        HsGuarantor hsGuarantor = this._hsGuarantorService.Value.GetHsGuarantor(hsVisit.HsGuarantorId);
                        IList<VpGuarantorHsMatch> vpGuarantorHsMatches = this._vpGuarantorHsMatchService.Value.GetVpGuarantorHsMatches(hsVisit.HsGuarantorId).Where(x => EnumHelper<HsGuarantorMatchStatusEnum>.Contains(x.HsGuarantorMatchStatus, "Allowed")).ToList();

                        hsVisit.VisitInsurancePlans.ForEach(x => x.Visit = null);
                        VisitChangeDto visitChangeDto = Mapper.Map<VisitChangeDto>(hsVisit);

                        visitChangeDto.VisitTransactions.Add(new VisitTransactionChangeDto
                        {
                            BillingSystemId = visitChangeDto.BillingSystemId,
                            ChangeType = ChangeTypeEnum.Insert,
                            DataLoadId = 0,
                            PaymentAllocationId = null,
                            PostDate = DateTime.UtcNow,
                            SourceSystemKey = null,
                            TransactionAmount = transactionAmount == 0 ? visit.CurrentBalance - visit.CurrentBalance : transactionAmount,
                            TransactionCodeId = 1,
                            TransactionDate = DateTime.UtcNow,
                            TransactionDescription = message,
                            VpTransactionTypeId = (int)vpTransactionType,
                            VisitId = hsVisit.VisitId
                        });

                        ChangeSetMessage changeSetMessage = new ChangeSetMessage
                        {
                            VisitChanges = visitChangeDto.ToListOfOne(),
                            VisitTransactionChanges = visitChangeDto.VisitTransactions,
                            HsGuarantors = Mapper.Map<IList<HsGuarantorDto>>(hsGuarantor.ToListOfOne()),
                            VpGuarantorHsMatches = Mapper.Map<IList<VpGuarantorHsMatchDto>>(vpGuarantorHsMatches),
                            ChangeEvent = new ChangeEventMessage
                            {
                                ChangeEventId = 0,
                                ChangeEventType = ChangeEventTypeEnum.DataChange,
                                ChangeEventDateTime = DateTime.UtcNow,
                                EventTriggerDescription = message,
                                VisitId = hsVisit.VisitId
                            }
                        };

                        this._visitCreationApplicationService.Value.MergeOrCreateVisitsFromVisitChangeSet(changeSetMessage);

                        JournalEventUserContext journalEventUserContext = new JournalEventUserContext() { UserName = userName, IsIvinciUser = true, EventVisitPayUserId = SystemUsers.SystemUserId };
                        this._userJournalEventService.Value.LogTransactionUpdate(journalEventUserContext, vpGuarantorId, visitId, hsGuarantor.HsGuarantorId, transactionAmount, vpTransactionType, message);

                        unitsOfWork.Commit();
                    }
                }
            }
        }

        private void ExecuteRetroScoring(IEnumerable<string> arguments)
        {
            IList<string> enumerable = arguments as IList<string> ?? arguments.ToList();
            if (enumerable.Count() < 6)
            {
                Console.WriteLine("Insufficient number of arguments: ");
                Console.WriteLine($"{DataCorrectionTypeEnum.RetroScoring} StartDate EndDate UseSskForGuarantorMatching");
                return;
            }

            DateTime startDate;
            DateTime endDate;
            bool useSskForGuarantorMatching;
            bool skipMatching = false;

            DateTime.TryParse(enumerable[3], out startDate);
            DateTime.TryParse(enumerable[4], out endDate);
            bool.TryParse(enumerable[5], out useSskForGuarantorMatching);
            if (enumerable.Count == 7)
            {
                bool.TryParse(enumerable[6], out skipMatching);
            }

            if (startDate > DateTime.MinValue && endDate > DateTime.MinValue && startDate <= endDate)
            {
                if (!skipMatching)
                {
                    this._scoringGuarantorMatchingService.Value.RunGuarantorMatchingForRetroScore(useSskForGuarantorMatching);
                }

                this._hsGuarantorApplicationScoringService.Value.CalculateInputVariablesForRetroScoring(startDate, endDate);
            }
            else
            {
                Console.WriteLine("Invalid Start or End Dates");
            }
        }

        private void MigrateHsEobToAppEob(IEnumerable<string> arguments)
        {
            this._eob835Service.Value.MigrateHsEobToApplicationEob();
        }

        private void SendOptimisticAchSettlements(IEnumerable<string> arguments)
        {
            if (this.FeatureService.Value.IsFeatureEnabled(VisitPayFeatureEnum.FeatureOptimisticAchSettlementIsEnabled))
            {
                //VisitPay payments
                IQueryable<Payment> pendingAchPayments = _paymentService.Value.GetPaymentsQueryable().Where(x => x.CurrentPaymentStatus == PaymentStatusEnum.ActivePendingACHApproval);
                foreach (Payment pendingAchPayment in pendingAchPayments)
                {
                    this._paymentService.Value.ProcessOptimisticAchSettlement(pendingAchPayment);
                    Console.WriteLine($"Optimistic VisitPay ACH settlement message made for VpGuarantorId: {pendingAchPayment.Guarantor.VpGuarantorId} PaymentId: {pendingAchPayment.PaymentId}");
                }

                //GuestPay payments
                IQueryable<GuestPay.Entities.Payment> pendingGuestPayAchPayments = _guestPayPaymentService.Value.GetPaymentsQueryable().Where(x => x.PaymentStatus == PaymentStatusEnum.ActivePendingACHApproval);
                foreach (GuestPay.Entities.Payment pendingGuestPayAchPayment in pendingGuestPayAchPayments)
                {
                    this._guestPayPaymentService.Value.ProcessOptimisticAchSettlement(pendingGuestPayAchPayment);
                    Console.WriteLine($"Optimistic GuestPay ACH settlement message made for StatementIdentifierId: {pendingGuestPayAchPayment.PaymentUnitAuthentication.StatementIdentifierId} PaymentId: {pendingGuestPayAchPayment.PaymentId}");
                }
            } else
            {
                Console.WriteLine("Nothing done since FeatureOptimisticAchSettlementIsEnabled IS NOT ENABLED");
            }
        }

        #endregion
    }
}
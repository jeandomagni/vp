﻿namespace Ivh.Application.DataCorrectionUtility.Modules
{
    using Autofac;
    using Domain.User.Entities;
    using Domain.User.Interfaces;
    using Ivh.Domain.User.Services;
    using Provider.User;

    public class Module : Autofac.Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<VisitPayUserRepository<VisitPayUser>>().As<IVisitPayUserRepository>();
            builder.RegisterType<VisitPayUserJournalEventService>().As<IVisitPayUserJournalEventService>();
        }
    }
}
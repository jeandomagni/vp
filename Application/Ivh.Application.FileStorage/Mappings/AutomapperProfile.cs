﻿namespace Ivh.Application.FileStorage.Mappings
{
    using AutoMapper;
    using Common.Dtos;
    using Domain.FileStorage.Entities;

    public class AutomapperProfile : Profile
    {
        public AutomapperProfile()
        {
            this.CreateMap<FileStoredDto, FileStored>()
                // example mapping
                //.ForMember(dest => dest.TransactionId, opts => opts.Ignore())
                ;

            this.CreateMap<FileStored, FileStoredDto>()
                // example mapping
                //.ForMember(dest => dest.TransactionId, opts => opts.MapFrom(x => x.TransactionId))
                ;
        }
    }
}
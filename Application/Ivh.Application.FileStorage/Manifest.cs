﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;
using System.Collections.Generic;

namespace Ivh.Application.FileStorage
{

    //TOOD: where is an appropriate place for this entity to be placed?

    [Serializable]
    [JsonObject(Title = "manifest")]
    public class Manifest
    {
        public Manifest()
        {
            this.Files = new List<ManifestFile>();
        }

        [JsonProperty("dateTime")]
        [JsonConverter(typeof(IsoDateTimeConverter))]
        public DateTime DateTime { get; set; }
        [JsonProperty("payloadFileCount")]
        public int PayloadFileCount { get; set; }
        [JsonProperty("files")]
        public List<ManifestFile> Files { get; set; }
    }
}

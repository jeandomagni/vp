﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;

namespace Ivh.Application.FileStorage
{

    //TOOD: where is an appropriate place for this entity to be placed?

    [Serializable]
    [JsonObject(Title = "file")]
    public class ManifestFile
    {
        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("size")]
        public long Size { get; set; }

        [JsonProperty("hash")]
        public string Hash { get; set; }

        [JsonProperty("dateTime")]
        [JsonConverter(typeof(IsoDateTimeConverter))]
        public DateTime DateTime { get; set; }
    }
}

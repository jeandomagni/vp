﻿namespace Ivh.Application.FileStorage
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.IO;
    using System.IO.Compression;
    using System.Linq;
    using System.Security.Cryptography;
    using System.Text;
    using System.Threading.Tasks;
    using AutoMapper;
    using Base.Common.Interfaces;
    using Base.Services;
    using Common.Dtos;
    using Common.Enums;
    using Common.Interfaces;
    using Domain.Guarantor.Entities;
    using Domain.Guarantor.Interfaces;
    using Domain.Core.SystemException.Interfaces;
    using Domain.Visit.Entities;
    using Domain.Visit.Interfaces;
    using Domain.FileStorage.Entities;
    using Domain.FileStorage.Interfaces;
    using Domain.Monitoring.Entities;
    using Domain.Visit.Unmatching.Interfaces;
    using HospitalData.Common.Models;
    using Ivh.Common.Data;
    using Ivh.Common.Data.Connection.Connections;
    using Ivh.Common.Data.Interfaces;
    using Ivh.Common.VisitPay.Constants;
    using Ivh.Common.VisitPay.Enums;
    using Ivh.Common.VisitPay.Jobs;
    using MimeTypes;
    using Monitoring.Common.Interfaces;
    using Newtonsoft.Json;
    using NHibernate;
    using SystemException = Domain.Core.SystemException.Entities.SystemException;
    using Ivh.Common.JobRunner.Common.Interfaces;

    /// <summary>
    /// </summary>
    public class FileStorageApplicationService : ApplicationService, IFileStorageApplicationService
    {
        private const CipherMode CipherMode = System.Security.Cryptography.CipherMode.CBC;
        private const PaddingMode PaddingMode = System.Security.Cryptography.PaddingMode.PKCS7;
        private const int BlockSize = 128;
        private const int KeySize = 256;

        public const string ManifestFileName = "Manifest.json";
        public const string PackageEncryptedExtension = @".aes";
        public const string PackageCompressedExtension = @".zip";
        public const string FileContentExtension = @".pdf";

        public const bool LogException = true;
        private readonly string _archiveFolder;
        private readonly IFileStorageService _fileStorageService;
        private readonly Lazy<IHsFacilityResolutionService> _hsFacilityResolutionService;
        private readonly Lazy<IHsBillingSystemResolutionService> _hsBillingSystemResolutionService;
        private readonly Lazy<IGuarantorService> _guarantorService;
        private readonly string _incomingFolder;
        private readonly Lazy<IMonitoringApplicationService> _monitoringApplicationService;
        private readonly Lazy<IRedactionService> _redactionService;

        private readonly string _packageFolder;
        private readonly ISession _session;

        private readonly byte[] _sharedKey;
        private readonly ISession _storageSession;

        private readonly Lazy<ISystemExceptionService> _systemExceptionService;
        private readonly string _tempRandomFolder;
        private readonly string _tempRootFolder;
        private readonly Lazy<IVisitItemizationStorageService> _visitStorageService;

        private readonly string _client;

        /// <summary>
        /// </summary>
        public FileStorageApplicationService(
            Lazy<IApplicationServiceCommonService> applicationServiceCommonService,
            Lazy<IVisitItemizationStorageService> visitStorageService,
            Lazy<ISystemExceptionService> systemExceptionService,
            Lazy<IHsFacilityResolutionService> hsFacilityResolutionService,
            Lazy<IHsBillingSystemResolutionService> hsBillingSystemResolutionService,
            Lazy<IGuarantorService> guarantorService,
            Lazy<IMonitoringApplicationService> monitoringApplicationService,
            ISessionContext<VisitPay> sessionContext,
            ISessionContext<Storage> storageSession,
            IFileStorageService fileStorageService,
            Lazy<IRedactionService> redactionService
            ) : base(applicationServiceCommonService)
        {
            this._client = applicationServiceCommonService.Value.ApplicationSettingsService.Value.Client.Value;

            this._systemExceptionService = systemExceptionService;
            this._visitStorageService = visitStorageService;
            this._session = sessionContext.Session;
            this._hsFacilityResolutionService = hsFacilityResolutionService;
            this._hsBillingSystemResolutionService = hsBillingSystemResolutionService;
            this._guarantorService = guarantorService;
            this._storageSession = storageSession.Session;
            this._fileStorageService = fileStorageService;
            this._redactionService = redactionService;
            this._monitoringApplicationService = monitoringApplicationService;

            this._incomingFolder = applicationServiceCommonService.Value.ApplicationSettingsService.Value.FileStorageIncomingFolder.Value;
            this._archiveFolder = applicationServiceCommonService.Value.ApplicationSettingsService.Value.FileStorageArchiveFolder.Value;
            string sk = applicationServiceCommonService.Value.ApplicationSettingsService.Value.FileStorageSharedKey.Value;
            this._sharedKey = Convert.FromBase64String(sk);
            this._packageFolder = applicationServiceCommonService.Value.ApplicationSettingsService.Value.FileStoragePackagingFolder.Value;

            this._tempRootFolder = applicationServiceCommonService.Value.ApplicationSettingsService.Value.FileStorageTempFolder.Value;
            this._tempRandomFolder = Path.GetRandomFileName();
        }

        #region jobs

        void IJobRunnerService<FileStorageImportJobRunner>.Execute(DateTime startDateTime, DateTime endDateTime, string[] args)
        {
            Task.WaitAll(Task.Run(this.ImportDirectoryAsync));
        }

        void IJobRunnerService<FileStorageVisitMatchJobRunner>.Execute(DateTime startDateTime, DateTime endDateTime, string[] args)
        {
            Task.WaitAll(Task.Run(this.MatchFilesWithVisitsAsync));
        }

        #endregion

        public async Task<FileStorageResponseDto> ImportDirectoryAsync()
        {
            if (this._client != ClientName.Intermountain)
            {
                throw new NotImplementedException($"This feature is only available for {ClientName.Intermountain}.");
            }

            //clean up temporary folders from old jobs
            await this.CleanTempFolderAsync(this._tempRootFolder);

            FileStorageResponseDto response = new FileStorageResponseDto();

            // create internal list of files to process in the importDir
            string[] packages = this.GetPackagesToImport(this._incomingFolder, PackageEncryptedExtension);
            if (packages.Length > 0)
            {
                foreach (string pkg in packages)
                {
                    string pkgFile = Path.GetFileName(pkg);
                    string archivedPkg = Path.Combine(this._archiveFolder, pkgFile);
                    string archivedZip = Path.Combine(this._archiveFolder, Path.GetFileNameWithoutExtension(archivedPkg) + PackageCompressedExtension);

                    this.MovePackageToArchive(pkg, archivedPkg, archivedZip);

                    try
                    {
                        this.DecryptPackage(archivedPkg, archivedZip, this._sharedKey);
                    }
                    catch (Exception ex)
                    {
                        if (LogException)
                        {
                            this._systemExceptionService.Value.LogSystemException(new SystemException
                            {
                                SystemExceptionDescription = $"Cannot decrypt package file {archivedPkg}. {ex.Message}",
                                SystemExceptionType = SystemExceptionTypeEnum.ItemizationDecryption
                            });
                        }
                        throw;
                    }

                    DirectoryInfo tempFolder = this.CreateTempFolder(this._tempRootFolder, this._tempRandomFolder);
                    Manifest manifest = this.UnpackPackage(archivedZip, tempFolder.FullName);

                    string msg = this.VerifyManifest(manifest, tempFolder.FullName);
                    if (msg == null)
                    {
                        int fileStorageFileCountActual = default(int);
                        foreach (ManifestFile file in manifest.Files)
                        {
                            this.VerifyFileName(file.Name);

                            string fp = default(string);
                            byte[] contents = default(byte[]);
                            try
                            {
                                fp = Path.Combine(tempFolder.FullName, file.Name);
                                contents = File.ReadAllBytes(fp);
                            }
                            catch (IOException ex)
                            {
                                if (LogException)
                                {
                                    this._systemExceptionService.Value.LogSystemException(new SystemException
                                    {
                                        SystemExceptionDescription = $"Cannot read file {fp}. {ex.Message}",
                                        SystemExceptionType = SystemExceptionTypeEnum.ItemizationProcessing
                                    });
                                }
                                throw;
                            }

                            try
                            {
                                FileStoredDto dto = new FileStoredDto
                                {
                                    FileContents = contents,
                                    FileDateTime = file.DateTime,
                                    FileMimeType = MimeTypeMap.GetMimeType(Path.GetExtension(file.Name)),
                                    ExternalKey = Guid.NewGuid(),
                                    Filename = file.Name,
                                    InsertDate = DateTime.UtcNow
                                };

                                FileStorageSaveResponseDto result = await this.SaveFileToDatabaseAsync(dto).ConfigureAwait(false);
                                fileStorageFileCountActual++;
                            }
                            catch (Exception ex)
                            {
                                if (LogException)
                                {
                                    this._systemExceptionService.Value.LogSystemException(new SystemException
                                    {
                                        SystemExceptionDescription = $"Cannot save file {fp} to database. {ex.Message}",
                                        SystemExceptionType = SystemExceptionTypeEnum.ItemizationProcessing
                                    });
                                }
                                throw;
                            }
                        }

                        response.IsError = false;
                        response.ErrorMessage = "";
                    }
                    else
                    {
                        throw new InvalidOperationException(msg);
                    }
                }
            }
            else
            {
                this._monitoringApplicationService.Value.GetDatum(DatumEnum.FileStorageFileCountExpected, () => 0, true);
                this._monitoringApplicationService.Value.GetDatum(DatumEnum.FileStorageFileCountActual, () => 0, true);
            }
            return response;
        }

        public async Task MatchFilesWithVisitsAsync()
        {
            if (this._client != ClientName.Intermountain)
            {
                throw new NotImplementedException($"This feature is only available for {ClientName.Intermountain}.");
            }

            using (UnitsOfWork unitsOfWork = new UnitsOfWork(this._session, this._storageSession))
            {
                IList<Guarantor> matchedGuarantors = new List<Guarantor>();
                IList<FileStored> unmatched = await this._fileStorageService.GetUnreferencedAsync().ConfigureAwait(true);

                int matchedCount = default(int);
                int retentionHours = this.ClientService.Value.GetClient().UnmatchedItemizationRetentionHours;
                foreach (FileStored fileStored in unmatched.OrderBy(x => x.Id))
                {
                    DateTime now = DateTime.UtcNow;
                    DateTime insertDate = fileStored.InsertDate;
                    double sinceCreatedHours = DateTime.UtcNow.Subtract(insertDate).TotalHours;
                    if (sinceCreatedHours > retentionHours)
                    {
                        await this._fileStorageService.RemoveUnreferencedAsync(fileStored.Id).ConfigureAwait(true);

                        if (LogException)
                        {
                            string msg = $"Unable to match file '{fileStored.Filename}' after {retentionHours} hours. The file has been purged.";
                            this._systemExceptionService.Value.LogSystemException(new SystemException
                            {
                                SystemExceptionDescription = msg,
                                SystemExceptionType = SystemExceptionTypeEnum.ItemizationExpiredRemoved
                            });
                        }
                    }
                    else
                    {
                        string[] fileNameParts = this.VerifyFileName(fileStored.Filename);
                        string systemId = fileNameParts[(int)FilenameSegments.SystemId];
                        string facilityCode = fileNameParts[(int)FilenameSegments.FacilityCode];
                        string accountNumber = fileNameParts[(int)FilenameSegments.AccountNumber];
                        string empi = fileNameParts[(int)FilenameSegments.Empi];
                        accountNumber = $"{systemId}_{facilityCode}_{accountNumber}";

                        int billingSystemId = this._hsBillingSystemResolutionService.Value.ResolveBillingSystemId(accountNumber);
                        VisitItemizationDetailsResultModel visitItemizationDetails = this._visitStorageService.Value.GetVisitItemizationDetails(billingSystemId, empi, accountNumber);

                        if (visitItemizationDetails != null)
                        {
                            Guarantor vpGuarantor = this._guarantorService.Value.GetGuarantor(visitItemizationDetails.VpGuarantorId);
                            if (vpGuarantor != null)
                            {
                                VisitItemizationStorage visitItemizationStorage = this._visitStorageService.Value.GetItemizationForVisit(visitItemizationDetails.VpGuarantorId, accountNumber, empi, SystemUsers.SystemUserId);
                                if (visitItemizationStorage == null)
                                {
                                    visitItemizationStorage = new VisitItemizationStorage { VisitFileStoredId = 0 };
                                }
                                else
                                {
                                    //obfuscate the old file.
                                    string replacementValue = $"Superseded by: {fileStored.ExternalKey}";
                                    this._redactionService.Value.RedactStoredFile(visitItemizationStorage.ExternalStorageKey, replacementValue);
                                }

                                visitItemizationStorage.AccountNumber = accountNumber;
                                visitItemizationStorage.Empi = empi;
                                visitItemizationStorage.ExternalStorageKey = fileStored.ExternalKey;
                                visitItemizationStorage.SystemId = systemId;
                                visitItemizationStorage.FacilityCode = facilityCode;
                                visitItemizationStorage.InsertDate = fileStored.InsertDate;
                                visitItemizationStorage.FacilityDescription = this._hsFacilityResolutionService.Value.ResolveFacilityDescription(visitItemizationStorage.FacilityCode);
                                visitItemizationStorage.VpGuarantor = vpGuarantor;
                                visitItemizationStorage.VisitSourceSystemKeyDisplay = visitItemizationDetails.VisitSourceSystemKeyDisplay;
                                visitItemizationStorage.BillingSystemId = visitItemizationDetails.BillingSystemId;
                                visitItemizationStorage.DateMatched = now;

                                if (!matchedGuarantors.Contains(visitItemizationStorage.VpGuarantor))
                                {
                                    matchedGuarantors.Add(visitItemizationStorage.VpGuarantor);
                                }

                                await this._visitStorageService.Value.SaveAsync(visitItemizationStorage).ConfigureAwait(false);
                                fileStored.ExternalReferenceDate = now;
                                await this._fileStorageService.SaveFileAsync(fileStored).ConfigureAwait(false);
                                matchedCount++;
                            }
                            else
                            {
                                // ItemizationFilenameGuarantorDoesntMatchVisitPay 
                                if (LogException)
                                {
                                    string msg = $"Filename '{fileStored.Filename}' Does not match HsGuarantorMap table guarantor of '{visitItemizationDetails.VpGuarantorId}'";
                                    this._systemExceptionService.Value.LogSystemException(new SystemException
                                    {
                                        SystemExceptionDescription = msg,
                                        SystemExceptionType = SystemExceptionTypeEnum.ItemizationFilenameGuarantorDoesntMatchVisitPay
                                    });
                                }
                            }
                        }
                        else
                        {
                            // ItemizationCouldNotMatch
                            if (LogException)
                            {
                                string msg = $"Unable to match file '{fileStored.Filename}'";
                                this._systemExceptionService.Value.LogSystemException(new SystemException
                                {
                                    SystemExceptionDescription = msg,
                                    SystemExceptionType = SystemExceptionTypeEnum.ItemizationCouldNotMatch
                                });
                            }
                        }
                    }
                }

                this._monitoringApplicationService.Value.GetDatum(DatumEnum.FileStorageMatchedExpected, () => unmatched.Count, true);
                this._monitoringApplicationService.Value.GetDatum(DatumEnum.FileStorageMatchedActual, () => matchedCount, true);

                unitsOfWork.Commit();
            }
        }

        public void CreatePackageFromFolder(DefectTypeEnum defectId)
        {
            if (this._client != ClientName.Intermountain)
            {
                throw new NotImplementedException($"This feature is only available for {ClientName.Intermountain}.");
            }

            this.CreatePackageFromFolder(this._packageFolder, this._incomingFolder, this._sharedKey, new List<string>(), defectId);
        }

        public async Task<FileStorageSaveResponseDto> SaveFileToIncomingFolderAsync(FileStoredDto fileStorageDto)
        {
            if (this._client != ClientName.Intermountain)
            {
                throw new NotImplementedException($"This feature is only available for {ClientName.Intermountain}.");
            }

            FileStorageSaveResponseDto fileStorageSaveResponseDto = new FileStorageSaveResponseDto();
            try
            {
                string filename = Path.Combine(this._incomingFolder, fileStorageDto.Filename);

                await
                    Task.Run(() => { File.WriteAllBytes(filename, fileStorageDto.FileContents); }
                        ).ConfigureAwait(true);

                FileStored fileStored = Mapper.Map<FileStored>(fileStorageDto);

                fileStorageSaveResponseDto.FileStored = Mapper.Map<FileStoredDto>(fileStored);
            }
            catch (Exception)
            {
                fileStorageSaveResponseDto.IsError = true;
                fileStorageSaveResponseDto.ErrorMessage = "File Save could not be processed.  Please try again.";
            }

            return fileStorageSaveResponseDto;
        }

        public async Task<FileStorageSaveResponseDto> SaveFileToDatabaseAsync(FileStoredDto fileStorageDto)
        {
            if (this._client != ClientName.Intermountain)
            {
                throw new NotImplementedException($"This feature is only available for {ClientName.Intermountain}.");
            }

            FileStorageSaveResponseDto fileStorageSaveResponseDto = new FileStorageSaveResponseDto();
            try
            {
                FileStored fileStored = Mapper.Map<FileStored>(fileStorageDto);

                bool success = await this._fileStorageService.SaveFileAsync(fileStored).ConfigureAwait(false);

                fileStorageSaveResponseDto.IsError = !success;

                if (success)
                {
                    fileStorageSaveResponseDto.FileStored = Mapper.Map<FileStoredDto>(fileStored);
                }
            }
            catch (Exception)
            {
                fileStorageSaveResponseDto.IsError = true;
                fileStorageSaveResponseDto.ErrorMessage = "File Save could not be processed.  Please try again.";
            }

            return fileStorageSaveResponseDto;
        }

        public async Task<FileStorageGetResponseDto> GetFileAsync(Guid key)
        {
            if (this._client != ClientName.Intermountain)
            {
                throw new NotImplementedException($"This feature is only available for {ClientName.Intermountain}.");
            }

            FileStored file = await this._fileStorageService.GetFileAsync(key);
            FileStorageGetResponseDto dto = new FileStorageGetResponseDto
            {
                FileStorage = new FileStoredDto
                {
                    ExternalKey = key,
                    FileContents = file.FileContents,
                    FileDateTime = file.FileDateTime,
                    FileMimeType = file.FileMimeType,
                    Filename = file.Filename,
                    Id = file.Id
                },
                IsError = false
            };
            return dto;
        }

        internal string[] GetPackagesToImport(string pathToImportDir, string fileExtension)
        {
            string[] importFileList = Directory.GetFiles(pathToImportDir, "*" + fileExtension);
            return importFileList;
        }

        /// <summary>
        ///     We move to the archive first to avoid expansion of the compressed file in the SFTP directory
        /// </summary>
        internal void MovePackageToArchive(string incomingPkg, string archivedPkg, string archivedZip)
        {
            try
            {
                //if the file is in the archive, rename it so we can reprocess it and preserve the original file.
                if (File.Exists(archivedPkg))
                {
                    this.MoveArchivedFileForReprocessing(archivedPkg);
                }

                if (File.Exists(archivedZip))
                {
                    this.MoveArchivedFileForReprocessing(archivedZip);
                }

                //move the incoming file to the archive for processing
                File.Move(incomingPkg, archivedPkg);
            }
            catch (IOException ex)
            {
                this.LogSystemExceptionFileMove(incomingPkg, archivedPkg, ex);
                throw;
            }
        }

        private void MoveArchivedFileForReprocessing(string fileName)
        {
            string dir = Path.GetDirectoryName(fileName);
            string file = Path.GetFileNameWithoutExtension(fileName);
            string ext = Path.GetExtension(fileName);
            string newName = Path.Combine(dir, $"{file}_r_{DateTime.UtcNow.ToString("yyyyMMdd_HHmmss")}{ext}");

            this.LoggingService.Value.Warn(() =>  $"FileStorageApplicationService::HandleExistingArchivedFiles - An archived file exists at: {fileName}. Renaming original to {newName}.");

            //move to Reprocessed
            try
            {
                File.Move(fileName, newName);
            }
            catch (IOException ex)
            {
                this.LogSystemExceptionFileMove(fileName, newName, ex);
                throw;
            }
        }

        private void LogSystemExceptionFileMove(string src, string dest, IOException ex)
        {
            if (LogException)
            {
                this._systemExceptionService.Value.LogSystemException(new SystemException
                {
                    SystemExceptionDescription = $"Cannot move file from {src} to {dest}. {ex.Message}",
                    SystemExceptionType = SystemExceptionTypeEnum.ItemizationProcessing
                });
            }
        }

        internal void PackPackage(string packagingFolder, string outputFolder)
        {
            ZipFile.CreateFromDirectory(packagingFolder, outputFolder, CompressionLevel.Optimal, false);
        }

        internal Manifest UnpackPackage(string inZipFilePath, string tempFolder)
        {
            try
            {
                ZipFile.ExtractToDirectory(inZipFilePath, tempFolder);
            }
            catch (Exception ex)
            {
                if (LogException)
                {
                    this._systemExceptionService.Value.LogSystemException(new SystemException
                    {
                        SystemExceptionDescription = string.Format("Cannot unzip package contents for {0}", inZipFilePath, ex.Message),
                        SystemExceptionType = SystemExceptionTypeEnum.ItemizationProcessing
                    });
                }
                throw;
            }
            Manifest manifest = this.GetManifest(Path.Combine(tempFolder, ManifestFileName));
            return manifest;
        }

        internal string VerifyManifest(Manifest manifestExpected, string tempDir)
        {
            string msg = default(string);

            List<string> exclude = new List<string> { ManifestFileName };
            Manifest manifestActual = this.GenerateManifest(tempDir, exclude);

            this._monitoringApplicationService.Value.GetDatum(DatumEnum.FileStorageFileCountExpected, () => manifestExpected.PayloadFileCount, true);
            this._monitoringApplicationService.Value.GetDatum(DatumEnum.FileStorageFileCountActual, () => manifestActual.PayloadFileCount, true);

            if (manifestActual.PayloadFileCount != manifestExpected.PayloadFileCount || manifestActual.Files.Count != manifestExpected.Files.Count)
            {
                msg = $"File Count Mismatch: Package manifest PayloadFileCount was {manifestExpected.PayloadFileCount} but there were {manifestExpected.Files.Count} files in the package.";
                if (LogException)
                {
                    this._systemExceptionService.Value.LogSystemException(new SystemException
                    {
                        SystemExceptionDescription = msg,
                        SystemExceptionType = SystemExceptionTypeEnum.ItemizationFileCountMismatch
                    });
                }
            }

            IEnumerable<ManifestFile> expectedButNotActual = manifestActual.Files.Select(x => new { x.Name, File = x }).Where(y => !manifestExpected.Files.Select(x => x.Name).Contains(y.Name)).Select(x => x.File);
            foreach (ManifestFile expectedFile in expectedButNotActual)
            {
                msg = $"Expected File missing: {expectedFile.Name} FileDate: {expectedFile.DateTime} Size: {expectedFile.Size} Hash: {expectedFile.Hash}";
                if (LogException)
                {
                    this._systemExceptionService.Value.LogSystemException(new SystemException
                    {
                        SystemExceptionDescription = msg,
                        SystemExceptionType = SystemExceptionTypeEnum.ItemizationMissingFile
                    });
                }
            }


            var invalidFileType = manifestActual.Files.Select(x => new { x.Name, File = x }).Where(x => Path.GetExtension(x.Name) != FileContentExtension);
            foreach (var invalidFile in invalidFileType)
            {
                msg = $"Invalid File Content Extension: {invalidFile.Name}";
                if (LogException)
                {
                    this._systemExceptionService.Value.LogSystemException(new SystemException
                    {
                        SystemExceptionDescription = msg,
                        SystemExceptionType = SystemExceptionTypeEnum.ItemizationInvalidContentType
                    });
                }
            }


            IEnumerable<ManifestFile> actualButNotExpected = manifestExpected.Files.Select(x => new { x.Name, File = x }).Where(y => !manifestActual.Files.Select(x => x.Name).Contains(y.Name)).Select(x => x.File);
            foreach (ManifestFile actualFile in actualButNotExpected)
            {
                msg = $"Unexpected File Found: {actualFile.Name} FileDate: {actualFile.DateTime} Size: {actualFile.Size} Hash: {actualFile.Hash}";
                if (LogException)
                {
                    this._systemExceptionService.Value.LogSystemException(new SystemException
                    {
                        SystemExceptionDescription = msg,
                        SystemExceptionType = SystemExceptionTypeEnum.ItemizationFileNotInManifest
                    });
                }
            }

#if DEBUG
            for (int i = 0; i < manifestExpected.Files.Count; i++)
            {
                Debug.WriteLine(manifestExpected.Files[i].DateTime + " : " + manifestActual.Files[i].DateTime);
            }
#endif
            var expectedActualMismatches = manifestActual.Files.Join(
                manifestExpected.Files, o => o.Name, i => i.Name, (o, i) => new { Expected = o, Actual = i }, StringComparer.OrdinalIgnoreCase)
                .Where(x => !(x.Expected.Size == x.Actual.Size && x.Expected.Hash == x.Actual.Hash));
            Debug.WriteLine(expectedActualMismatches.Count());
            foreach (var expectedActualMismatch in expectedActualMismatches)
            {
                msg = $"Expected File found, mismatched details: {expectedActualMismatch.Expected.Name}" + Environment.NewLine
                      + $"\tFileDate: Expected = {expectedActualMismatch.Expected.DateTime} Actual = {expectedActualMismatch.Actual.DateTime}"
                      + $"\tSize: Expected = {expectedActualMismatch.Expected.Size} Actual = {expectedActualMismatch.Actual.Size}"
                      + $"\tHash: Expected = {expectedActualMismatch.Expected.Hash} Actual = {expectedActualMismatch.Actual.Hash}";
                if (LogException)
                {
                    this._systemExceptionService.Value.LogSystemException(new SystemException
                    {
                        SystemExceptionDescription = msg,
                        SystemExceptionType = SystemExceptionTypeEnum.ItemizationManifestHash
                    });
                }
            }


            return msg;
        }

        internal Manifest GetManifest(string manifestPath)
        {
            Manifest json;
            try
            {
                string manifestJson = File.ReadAllText(manifestPath);
                json = JsonConvert.DeserializeObject<Manifest>(manifestJson);
            }
            catch (Exception ex)
            {
                if (LogException)
                {
                    this._systemExceptionService.Value.LogSystemException(new SystemException
                    {
                        SystemExceptionDescription = string.Format("Cannot extract Manifest for {0}", manifestPath, ex.Message),
                        SystemExceptionType = SystemExceptionTypeEnum.ItemizationMissingManifest
                    });
                }
                throw;
            }
            return json;
        }

        internal void WriteManifest(string packageFolder, IList<string> exclude, DefectTypeEnum defectTypeEnum = DefectTypeEnum.None)
        {
            string manifestFilePath = Path.Combine(packageFolder, ManifestFileName);
            Manifest manifest = this.GenerateManifest(packageFolder, exclude);

            // deliberately mangle the manifest for testing purposes
            // note that this impacts only code used for testing
            if (defectTypeEnum != DefectTypeEnum.None)
            {
                manifest = this.MangleManifest(manifest, defectTypeEnum);
            }
            string manifestJson = JsonConvert.SerializeObject(manifest);

            if (manifest.PayloadFileCount > 0)
            {
                if (File.Exists(manifestFilePath))
                {
                    try
                    {
                        File.Delete(manifestFilePath);
                    }
                    catch (IOException ex)
                    {
                        if (LogException)
                        {
                            this._systemExceptionService.Value.LogSystemException(new SystemException
                            {
                                SystemExceptionDescription = $"Cannot delete manfiest file {manifestFilePath}. {ex.Message}",
                                SystemExceptionType = SystemExceptionTypeEnum.ItemizationProcessing
                            });
                        }
                        throw;
                    }
                }
            }
            else
            {
                string msg = $"WARNING: No files in the packaging folder {packageFolder}.";
                if (LogException)
                {
                    this._systemExceptionService.Value.LogSystemException(new SystemException
                    {
                        SystemExceptionDescription = msg,
                        SystemExceptionType = SystemExceptionTypeEnum.ItemizationProcessing
                    });
                }
            }

            if (defectTypeEnum != DefectTypeEnum.MissingManifest)
            {
                try
                {
                    File.WriteAllText(manifestFilePath, manifestJson, Encoding.UTF8);
                }
                catch (IOException ex)
                {
                    if (LogException)
                    {
                        this._systemExceptionService.Value.LogSystemException(new SystemException
                        {
                            SystemExceptionDescription = $"Cannot write manfiest file {manifestFilePath}. {ex.Message}",
                            SystemExceptionType = SystemExceptionTypeEnum.ItemizationProcessing
                        });
                    }
                    throw;
                }
            }
        }

        internal Manifest MangleManifest(Manifest manifest, DefectTypeEnum defectTypeEnum)
        {
            switch (defectTypeEnum)
            {
                case DefectTypeEnum.FileCountLow:
                    // actual file count will be one less than the manifest indicates
                    manifest.PayloadFileCount++;
                    break;
                case DefectTypeEnum.FileCountHigh:
                    // actual file count will be one more than the manifest indicates
                    manifest.PayloadFileCount--;
                    break;
                case DefectTypeEnum.InvalidFileHash:
                    if (manifest.Files.Count > 0)
                    {
                        manifest.Files[0].Hash = "#ImABadHash";
                    }
                    break;
                case DefectTypeEnum.InvalidFileSize:
                    if (manifest.Files.Count > 0)
                    {
                        manifest.Files[0].Size++;
                    }
                    break;
                case DefectTypeEnum.InvalidFileName:
                    if (manifest.Files.Count > 0)
                    {
                        manifest.Files[0].Name = "InvalidFilename.pdf";
                    }
                    break;
                case DefectTypeEnum.InvalidFileDateTime:
                    if (manifest.Files.Count > 0)
                    {
                        manifest.Files[0].DateTime = new DateTime(2013, 1, 1, 1, 1, 1);
                    }
                    break;
                case DefectTypeEnum.MissingFile:
                    if (manifest.Files.Count > 0)
                    {
                        manifest.Files.RemoveAt(0);
                    }
                    break;
                case DefectTypeEnum.ExtraneousFile:
                    if (manifest.Files.Count > 0)
                    {
                        manifest.Files.Add(new ManifestFile { Name = "Extraneous File", DateTime = DateTime.UtcNow, Size = 1 });
                    }
                    break;
                default:
                    break;
            }

            return manifest;
        }

        internal void EncryptPackage(string inZipFilePath, string outPkgFilePath, byte[] sharedKey)
        {
            using (RijndaelManaged rijndaelManaged = new RijndaelManaged())
            {
                rijndaelManaged.Mode = CipherMode;
                rijndaelManaged.Padding = PaddingMode;
                rijndaelManaged.BlockSize = BlockSize;
                rijndaelManaged.KeySize = KeySize;
                rijndaelManaged.Key = sharedKey;

                byte[] ivBytes = new byte[16];
                RandomNumberGenerator.Create().GetBytes(ivBytes);
                rijndaelManaged.IV = ivBytes;

                using (FileStream fileStreamCipherText = File.OpenWrite(outPkgFilePath))
                {
                    fileStreamCipherText.Write(rijndaelManaged.IV, 0, rijndaelManaged.IV.Length);
                    fileStreamCipherText.Flush();

                    using (ICryptoTransform cryptoTransform = rijndaelManaged.CreateEncryptor(rijndaelManaged.Key, rijndaelManaged.IV))
                    {
                        CryptoStream cryptoStream = new CryptoStream(fileStreamCipherText, cryptoTransform, CryptoStreamMode.Write);

                        using (FileStream fileStreamClearText = File.OpenRead(inZipFilePath))
                        {
                            byte[] bytes = new byte[fileStreamClearText.Length];
                            fileStreamClearText.Read(bytes, 0, bytes.Length);
                            cryptoStream.Write(bytes, 0, bytes.Length);
                            cryptoStream.FlushFinalBlock();
                            cryptoStream.Close();
                        }
                    }
                }

                File.Delete(inZipFilePath);
            }
        }

        internal void DecryptPackage(string inPkgFilePath, string outZipFilePath, byte[] sharedKey)
        {
            using (RijndaelManaged rijndaelManaged = new RijndaelManaged())
            {
                rijndaelManaged.Mode = CipherMode;
                rijndaelManaged.Padding = PaddingMode;
                rijndaelManaged.BlockSize = BlockSize;
                rijndaelManaged.KeySize = KeySize;
                rijndaelManaged.Key = sharedKey;

                using (FileStream fileStreamCipherText = File.OpenRead(inPkgFilePath))
                {
                    byte[] ivBytes = new byte[16];
                    fileStreamCipherText.Read(ivBytes, 0, 16);
                    rijndaelManaged.IV = ivBytes;

                    byte[] cipherBytes = new byte[fileStreamCipherText.Length - rijndaelManaged.IV.Length];
                    fileStreamCipherText.Read(cipherBytes, 0, cipherBytes.Length);

                    using (ICryptoTransform cryptoTransform = rijndaelManaged.CreateDecryptor(rijndaelManaged.Key, rijndaelManaged.IV))
                    {
                        CryptoStream cryptoStream = new CryptoStream(new MemoryStream(cipherBytes), cryptoTransform, CryptoStreamMode.Read);

                        int bytesRead = 0;
                        byte[] bytes = new byte[cipherBytes.Length];
                        bytesRead = cryptoStream.Read(bytes, 0, bytes.Length);

                        using (FileStream fileStreamClearText = File.OpenWrite(outZipFilePath))
                        {
                            fileStreamClearText.Write(bytes, 0, bytesRead);
                            fileStreamClearText.Flush();
                            fileStreamClearText.Close();
                        }
                    }
                }

                // Leave the file
                //File.Delete(inPkgFilePath);
            }
        }

        internal DirectoryInfo CreateTempFolder(string tempRootFolder, string tempSubfolder)
        {
            DirectoryInfo tempDirInfo = default(DirectoryInfo);
            try
            {
                if (!Directory.Exists(tempRootFolder))
                {
                    Directory.CreateDirectory(tempRootFolder);
                }
            }
            catch (Exception)
            {
                //Just swollow the exception if the process doesnt have access to create the folder
            }

            if (Directory.Exists(tempRootFolder))
            {
                DirectoryInfo di = new DirectoryInfo(tempRootFolder);
                try
                {
                    tempDirInfo = di.CreateSubdirectory(tempSubfolder);
                }
                catch (Exception ex)
                {
                    if (LogException)
                    {
                        this._systemExceptionService.Value.LogSystemException(new SystemException
                        {
                            SystemExceptionDescription = $"Cannot create temporary subfolder '{tempSubfolder}' in parent folder '{tempRootFolder}'.  {ex}",
                            SystemExceptionType = SystemExceptionTypeEnum.ItemizationProcessing
                        });
                    }
                }
            }
            else
            {
                if (LogException)
                {
                    this._systemExceptionService.Value.LogSystemException(new SystemException
                    {
                        SystemExceptionDescription = $"Temporary Root Folder Not Found '{tempRootFolder}'",
                        SystemExceptionType = SystemExceptionTypeEnum.ItemizationProcessing
                    });
                }
                throw new DirectoryNotFoundException(tempRootFolder);
            }

            return tempDirInfo;
        }

        internal void CreatePackageFromFolder(string packageFolder, string outFolder, byte[] sharedKey, IList<string> exclude, DefectTypeEnum defectTypeEnum = DefectTypeEnum.None)
        {
            DirectoryInfo tempFolder = this.CreateTempFolder(this._tempRootFolder, this._tempRandomFolder);

            //if (defectType == DefectTypes.Decryption) sharedKey = Convert.FromBase64String("D38Y7objL3R7wf6XdD0luC0D0xsyT8nPMpbZQPY/Oks=");
            if (defectTypeEnum == DefectTypeEnum.Decryption) sharedKey = Convert.FromBase64String("DDDDDDDDDDR7wf6XdD0luC0D0xsyT8nPMpbZQPY/Oks=");

            Action<string> mvTemp = f => { File.Move(f, Path.Combine(tempFolder.FullName, Path.GetFileName(f))); };
            Array.ForEach(Directory.GetFiles(packageFolder), mvTemp);

            this.WriteManifest(tempFolder.FullName, exclude, defectTypeEnum);

            string outputDirectoryPathZip = Path.Combine(outFolder, DateTimeOffset.UtcNow.ToString("yyyyMMddHHmmss") + ".zip");
            string outputDirectoryPathAes = outputDirectoryPathZip.Replace(".zip", ".aes");

            ZipFile.CreateFromDirectory(tempFolder.FullName, outputDirectoryPathZip, CompressionLevel.Optimal, false);
            this.EncryptPackage(outputDirectoryPathZip, outputDirectoryPathAes, sharedKey);
        }

        internal async Task CleanTempFolderAsync(string tempRootFolder)
        {
            DirectoryInfo[] subDirs = default(DirectoryInfo[]);
            if (Directory.Exists(tempRootFolder))
            {
                DirectoryInfo di = new DirectoryInfo(tempRootFolder);
                subDirs = di.GetDirectories();
                foreach (DirectoryInfo folder in subDirs)
                {
                    try
                    {
                        await
                            Task.Run(() => { folder.Delete(true); }).ConfigureAwait(true);
                    }
                    catch (IOException)
                    {
                        // ignore busy subdirectories being held by ASP.NET
                    }
                }
            }
            else
            {
                if (LogException)
                {
                    this._systemExceptionService.Value.LogSystemException(new SystemException
                    {
                        SystemExceptionDescription = $"Temporary Root Folder Not Found '{tempRootFolder}'",
                        SystemExceptionType = SystemExceptionTypeEnum.ItemizationProcessing
                    });
                }
                throw new DirectoryNotFoundException(tempRootFolder);
            }
        }

        internal string GenerateManifestJson(string sourceFilesDirectoryPath, IList<string> exclude)
        {
            return JsonConvert.SerializeObject(this.GenerateManifest(sourceFilesDirectoryPath, exclude));
        }

        internal Manifest GenerateManifest(string sourceFilesDirectoryPath, IList<string> exclude = null)
        {
            Func<Stream, string> hash = b =>
            {
                SHA1 sha1Managed = SHA1.Create();
                byte[] hashBytes = sha1Managed.ComputeHash(b);
                return Convert.ToBase64String(hashBytes);
            };

            exclude = exclude.Select(x => Path.Combine(sourceFilesDirectoryPath, x)).ToList();

            List<ManifestFile> fileData = Directory.EnumerateFiles(sourceFilesDirectoryPath)
                .Where(x => exclude == null || !exclude.Contains(x, StringComparer.OrdinalIgnoreCase))
                .Select(f =>
                {
                    FileInfo fileInfo = new FileInfo(f);
                    FileStream fs = File.OpenRead(f);
                    return new ManifestFile
                    {
                        Name = fileInfo.Name,
                        Size = fileInfo.Length,
                        Hash = hash(fs),
                        DateTime = fileInfo.LastWriteTimeUtc
                    };
                }).ToList();

            Manifest manifest = new Manifest
            {
                DateTime = DateTime.UtcNow, //.ToString("yyyy-MM-ddTHH:mm:ssZ"),
                PayloadFileCount = fileData.Count,
                Files = fileData
            };
            return manifest;
        }

        internal string[] VerifyFileName(string name)
        {
            string msg = default(string);
            string[] fileNameParts = name.Split('_');
            if (fileNameParts.Length != 4)
            {
                msg = $"Filename '{name}' does not conform to 4-segment requirement.";
                if (LogException)
                {
                    this._systemExceptionService.Value.LogSystemException(new SystemException
                    {
                        SystemExceptionDescription = msg,
                        SystemExceptionType = SystemExceptionTypeEnum.ItemizationInvalidFilenameFormat
                    });
                }
                throw new InvalidOperationException(msg);
            }

            string extension = Path.GetExtension(name);
            if (string.Compare(extension, FileContentExtension, true) != 0)
            {
                msg = $"Invalid File Content Extension: {name}";
                if (LogException)
                {
                    this._systemExceptionService.Value.LogSystemException(new SystemException
                    {
                        SystemExceptionDescription = msg,
                        SystemExceptionType = SystemExceptionTypeEnum.ItemizationInvalidContentType
                    });
                }
                throw new InvalidOperationException(msg);
            }

            fileNameParts[3] = Path.GetFileNameWithoutExtension(fileNameParts[3]);

            return fileNameParts;
        }

        private enum FilenameSegments
        {
            SystemId,
            FacilityCode,
            AccountNumber,
            Empi
        }
    }
}
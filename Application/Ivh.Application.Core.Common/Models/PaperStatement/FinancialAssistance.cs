﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ivh.Application.Core.Common.Models.PaperStatement
{
    public class FinancialAssistance
    {
        public string FinancialAssistanceTopic { get; set; }
        public string FinancialAssistanceText { get; set; }
    }
}

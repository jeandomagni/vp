﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ivh.Application.Core.Common.Models.PaperStatement
{
    public class FinancePlansSummary
    {
        public string FinancePlansSummaryOverviewName { get; set; }
        public string FinancePlansSummaryTotalInterestDueName { get; set; }
        public string FinancePlansSummaryTotalPrincipalDueName { get; set; }
        public string FinancePlansSummaryNextPaymentName { get; set; }
        public string FinancePlansSummaryTotalBalanceName { get; set; }

        public string FinancePlansSummaryTotalInterestDue { get; set; }
        public string FinancePlansSummaryTotalPrincipalDue { get; set; }
        public string FinancePlansSummaryNextPayment { get; set; }
        public string FinancePlansSummaryTotalBalance { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ivh.Application.Core.Common.Models.PaperStatement
{
    public class VisitDetail
    {
        public string VisitDetailStatusColor { get; set; }
        public string VisitDetailStatusLogo { get; set; }
        public string VisitDetailStatus { get; set; }
        public string VisitDetailBalance { get; set; }
        public string VisitDetailDate { get; set; }
        public string VisitDetailPatient { get; set; }
        public string VisitDetailDescription { get; set; }
        public List<VisitDetailSubtotal> VisitDetailSubtotalList { get; set; }
    }
}

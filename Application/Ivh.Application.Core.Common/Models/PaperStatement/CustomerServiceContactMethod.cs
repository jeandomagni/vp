﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ivh.Application.Core.Common.Models.PaperStatement
{
    public class CustomerServiceContactMethod
    {
        public string ContactText { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ivh.Application.Core.Common.Models.PaperStatement
{
    public class VisitDetailSubtotal
    {
        public string SubtotalName { get; set; }
        public string SubtotalAmount { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ivh.Application.Core.Common.Models.PaperStatement
{
    public class FinancePlanInfo
    {
        public string FinancePlanName { get; set; }

        public string EffectiveDateText { get; set; }
        public string EffectiveDate { get; set; }

        public string OriginalBalanceText { get; set; }
        public string OriginalBalance { get; set; }

        public string InterestRateText { get; set; }
        public string InterestRate { get; set; }

        public List<BalanceSummary> BalanceSummaryList { get; set; }

        public string FinancePlanBalanceText { get; set; }
        public string FinancePlanBalanceAmount { get; set; }

        public string FinancePlanMonthsRemainingText { get; set; }
        public string FinancePlanMonthsRemaining { get; set; }

        public string PaymentDetailsText { get; set; }

        public List<PaymentSummary> PaymentSummaryList { get; set; }

        public string FinancePlanNextPaymentText { get; set; }
        public string FinancePlanNextPaymentAmount { get; set; }

        //public string FinancePlanAutoPayLogo { get; set; }
        //public string FinancePlanAutoPayText { get; set; }

        public string VisitDateHeaderText { get; set; }
        public string VisitDescriptionHeaderText { get; set; }
        public string VisitBalanceHeaderText { get; set; }

        public List<FinancePlanVisitDetail> FinancePlanVisitDetailList { get; set; }
    }
}

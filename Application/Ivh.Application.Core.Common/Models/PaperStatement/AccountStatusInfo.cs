﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ivh.Application.Core.Common.Models.PaperStatement
{
    public class AccountStatusInfo
    {
        public string AccountStatusColor { get; set; }
        public string AccountStatusText { get; set; }
        public string AccountStatusLogo { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ivh.Application.Core.Common.Models.PaperStatement
{
    public class PaymentOption
    {
        public string PaymentOptionLogo { get; set; }
        public string PaymentOptionTitle { get; set; }
        public string PaymentOptionDescription { get; set; }
        public string PaymentOptionHighlight { get; set; }
    }
}

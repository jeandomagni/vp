﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ivh.Application.Core.Common.Models.PaperStatement
{
    public class PaymentSummary
    {
        public string PaymentName { get; set; }
        public string PaymentAmount { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ivh.Application.Core.Common.Models.PaperStatement
{
    public class VisitPayPaperBillingStatement
    {
            public string ClientLogo { get; set; }
            public string ReturnName { get; set; }
            public string ReturnAddressStreet1 { get; set; }
            public string ReturnAddressStreet2 { get; set; }
            public string ReturnCity { get; set; }
            public string ReturnState { get; set; }
            public string ReturnZip { get; set; }
            public string StatementName { get; set; }
            public string StatementPeriod { get; set; }
            public string AccountHolderName { get; set; }
            public string AccountNumberName { get; set; }
            public string AccountNumber { get; set; }
            public string PaymentUrl { get; set; }
            public string BalanceTotalName { get; set; }
            public string BalanceTotal { get; set; }
            public string MailingName { get; set; }
            public string MailingAddressStreet1 { get; set; }
            public string MailingAddressStreet2 { get; set; }
            public string MailingCity { get; set; }
            public string MailingState { get; set; }
            public string MailingZip { get; set; }
            public string AccountStatusText { get; set; }
            public string AccountStatusFillColor { get; set; }
            public string BalanceSummaryName { get; set; }
            public string PayAtText { get; set; }
            public string PayOnlineText { get; set; }
            public string PayOnlineLogo { get; set; }
            public string NotificationsName { get; set; }
            public string CustomerServiceName { get; set; }
            public string PageText { get; set; }
            public string OfText { get; set; }
            public string FooterText { get; set; }
            public string FooterLogo { get; set; }

            public string VisitDateHeaderText { get; set; }
            public string VisitDescriptionHeaderText { get; set; }
            public string VisitStatusHeaderText { get; set; }
            public string VisitBalanceHeaderText { get; set; }

            public List<OnlinePaymentPromo> OnlinePaymentPromoList { get; set; }
            public List<BalanceSummary> BalanceSummaryList { get; set; }
            public List<Notification> NotificationList { get; set; }
            public List<CustomerServiceContactMethod> CustomerServiceContactMethodList { get; set; }
            public List<Disclaimer> DisclaimerList { get; set; }
            public List<VisitDetail> VisitDetailList { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ivh.Application.Core.Common.Models.PaperStatement
{
    public class FinancePlanVisitDetail
    {
        public string VisitDetailDate { get; set; }
        public string VisitIdentifierText { get; set; }
        public string VisitIdentifier { get; set; }
        public string VisitDetailDescription { get; set; }
        public string VisitDetailPatient { get; set; }
        public string VisitDetailBalance { get; set; }
    }
}

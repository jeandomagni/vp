﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ivh.Application.Core.Common.Models.PaperStatement
{
    public class VisitDetailStatusInfo
    {
        public string VisitDetailStatusColor { get; set; }
        public string VisitDetailStatusLogo { get; set; }
        public string VisitDetailStatusText { get; set; }
    }
}

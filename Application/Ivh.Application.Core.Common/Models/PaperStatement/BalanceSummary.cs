﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ivh.Application.Core.Common.Models.PaperStatement
{
    public class BalanceSummary
    {
        public string BalanceName { get; set; }
        public string BalanceAmount { get; set; }
    }
}

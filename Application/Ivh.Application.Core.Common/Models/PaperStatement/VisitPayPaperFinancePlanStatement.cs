﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ivh.Application.Core.Common.Models.PaperStatement
{
    public class VisitPayPaperFinancePlanStatement
    {
        public string ClientLogo { get; set; }

        public List<PaymentOption> PaymentOptionList { get; set; }

        public string StatementName { get; set; }
        public string StatementPeriod { get; set; }
        public string AccountHolderName { get; set; }
        public string AccountNumberName { get; set; }
        public string AccountNumber { get; set; }

        public string NextPaymentText { get; set; }
        public string PaymentAmountDue { get; set; }
        public string DueDateText { get; set; }
        public string DueDate { get; set; }

        public string AccountStatusFillColor { get; set; }
        public string AccountStatusText { get; set; }

        //public string NotificationsName { get; set; }
        //public List<Notification> NotificationList { get; set; }

        public string CustomerServiceName { get; set; }
        public List<CustomerServiceContactMethod> CustomerServiceContactMethodList { get; set; }

        public List<FinancePlanInfo> FinancePlanInfoList { get; set; }

        public string FinancePlanFooterText { get; set; }

        public FinancePlansSummary FinancePlansSummary { get; set; }

        //public string FinancialAssistanceHeaderText { get; set; }
      
        //public List<FinancialAssistance> FinancialAssistanceList { get; set; }

        public string DetachCouponText { get; set; }

        public AddressInfo ReturnAddressInfo { get; set; }

        public string PayableToText { get; set; }

        public AddressInfo MailingAddressInfo { get; set; }

        //public string AutoPayLogo { get; set; }
        //public string AutoPayText { get; set; }

        public string PayAmountText { get; set; }

        public string AccountHolderText { get; set; }

        public string AmountEnclosedText { get; set; }

        public string ReturnPenaltyText { get; set; }

        public AddressInfo RemitAddressInfo { get; set; }

        public string OcrText { get; set; }

        public string StatementDisclaimerText { get; set; }

        public string PageText { get; set; }
        public string OfText { get; set; }
        public string FooterText { get; set; }
        public string FooterLogo { get; set; }

        public string Language { get; set; }

    }
}

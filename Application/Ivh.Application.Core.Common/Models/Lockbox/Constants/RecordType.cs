﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ivh.Application.Core.Common.Models.Lockbox.Constants
{
    public static class RecordType
    {
        public const string FileHeader = "100";
        public const string BatchHeader = "300";
        public const string Detail = "500";
        public const string OverflowDetail = "500";
        public const string DetailTrailer = "700";
        public const string BatchTrailer = "800";
        public const string FileTrailer = "900";

        public const int Index = 1;
        public const int Length = 3;
    }
}

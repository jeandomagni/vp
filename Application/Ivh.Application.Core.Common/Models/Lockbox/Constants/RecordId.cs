﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ivh.Application.Core.Common.Models.Lockbox.Constants
{
    public static class RecordId
    {
        public const string FileHeader = "A";
        public const string BatchHeader = "C";
        public const string OverflowDetail = "X";
        public const string Detail = "D";
        public const string DetailTrailer = "P";
        public const string BatchTrailer = "E";
        public const string FileTrailer = "T";

        public const int Index = 307;
        public const int Length = 1;
    }
}

﻿namespace Ivh.Application.Core.Common.Models.Lockbox
{
    using Ivh.Common.Base.Attributes;
    using System.Runtime.Serialization;

    public class FileTrailer : LockboxFileRecord
    {
        [IgnoreDataMember]
        public override RecordTypeDefinition RecordTypeDefinition => new RecordTypeDefinition
        {
            RecordType = Constants.RecordType.FileTrailer,
            RecordId = Constants.RecordId.FileTrailer,
            RecordTypeName = typeof(FileTrailer).FullName
        };

        [FlatFileField(Index = 4, Length = 7)]
        public string TotalBatchCount { get; set; }

        [FlatFileField(Index = 11, Length = 8)]
        public string TotalCheckCount { get; set; }

        [FlatFileField(Index = 19, Length = 14)]
        public string TotalAmount { get; set; }

    }
}

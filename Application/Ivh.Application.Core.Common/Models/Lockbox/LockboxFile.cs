﻿namespace Ivh.Application.Core.Common.Models.Lockbox
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    public class LockboxFile
    {
        public static LockboxFile GetLockboxFile(string filePath)
        {
            LockboxFile lockboxFile = new LockboxFile();
            using (System.IO.StreamReader sr = new System.IO.StreamReader(filePath, true))
            {
                while (sr.Peek() != -1)
                {
                    string line = sr.ReadLine();
                    object obj = LockboxFileRecord.GetFromString(line);
                    lockboxFile.AddObject(obj);
                }
            }
            return lockboxFile;
        }

        public FileHeader FileHeader { get; set; }

        private void AddObject(object obj)
        {
            if (obj != null)
            {
                Dictionary<Type, Action> @switch = new Dictionary<Type, Action> {
                    { typeof(FileHeader), () => this.FileHeader = obj as FileHeader},
                    { typeof(FileTrailer), () => this.FileHeader.FileTrailer = obj as FileTrailer},
                    { typeof(BatchHeader), () => this.FileHeader.BatchHeaders.Add(obj as BatchHeader)},
                    { typeof(BatchTrailer), () => this.FileHeader.BatchHeaders.Last().BatchTrailer = obj as BatchTrailer},
                    { typeof(Detail), () => 
                        {
                            this.FileHeader.BatchHeaders.Last().Details.Add(obj as Detail); 
                            this.FileHeader.BatchHeaders.Last().Details.Last().ParentBatchHeaderLockBoxNumber = this.FileHeader.BatchHeaders.Last().LockboxNumber;
                        } 
                    },
                    // Included in the spec, but not present in the file
                    //{ typeof(OverflowDetail), () => this.FileHeader.BatchHeaders.Last().Details.Last().OverflowDetails.Add(obj as OverflowDetail)},
                    { typeof(DetailTrailer), () => this.FileHeader.BatchHeaders.Last().Details.Last().DetailTrailer = obj as DetailTrailer},
                };

                @switch[obj.GetType()]();

            }
        }

    }
}

﻿namespace Ivh.Application.Core.Common.Models.Lockbox
{
    using Ivh.Common.Base.Attributes;
    using System;
    using System.Collections.Generic;
    using System.Runtime.Serialization;

    public class BatchHeader : LockboxFileRecord
    {
        public BatchHeader()
        {
            this.Details = new List<Detail>();
        }

        [IgnoreDataMember]
        public override RecordTypeDefinition RecordTypeDefinition => new RecordTypeDefinition
        {
            RecordType = Constants.RecordType.BatchHeader,
            RecordId = Constants.RecordId.BatchHeader,
            RecordTypeName = typeof(BatchHeader).FullName
        };

        [FlatFileField(Index = 4, Length = 20)]
        public string LockboxNumber { get; set; }

        [FlatFileField(Index = 24, Length = 8)]
        public string BatchCreditDate { get; set; }

        [FlatFileField(Index = 275, Length = 3)]
        public string BatchNumber { get; set; }

        [FlatFileField(Index = 283, Length = 7)]
        public string LockboxNumber2 { get; set; }

        [FlatFileField(Index = 291, Length = 2)]
        public string TransactionGroupNumber { get; set; }

        public List<Detail> Details { get; set; }

        public BatchTrailer BatchTrailer { get; set; }
    }
}

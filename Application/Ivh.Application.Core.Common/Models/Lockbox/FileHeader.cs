﻿namespace Ivh.Application.Core.Common.Models.Lockbox
{
    using Ivh.Common.Base.Attributes;
    using System.Collections.Generic;
    using System.Runtime.Serialization;

    public class FileHeader : LockboxFileRecord
    {
        public FileHeader()
        {
            this.BatchHeaders = new List<BatchHeader>();
        }

        [IgnoreDataMember]
        public override RecordTypeDefinition RecordTypeDefinition => new RecordTypeDefinition
        {
            RecordType = Constants.RecordType.FileHeader,
            RecordId = Constants.RecordId.FileHeader,
            RecordTypeName = typeof(FileHeader).FullName
        };

        [FlatFileField(Index = 4, Length = 6)]
        public string PaymentType { get; set; }

        [FlatFileField(Index = 10, Length = 10)]
        public string ApplicationSource { get; set; }

        [FlatFileField(Index = 20, Length = 8)]
        public string BatchCreditDate { get; set; }

        public List<BatchHeader> BatchHeaders { get; set; }

        public FileTrailer FileTrailer { get; set; }
    }
}

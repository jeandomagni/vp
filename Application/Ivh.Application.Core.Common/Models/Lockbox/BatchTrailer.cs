﻿namespace Ivh.Application.Core.Common.Models.Lockbox
{
    using Ivh.Common.Base.Attributes;
    using System.Runtime.Serialization;

    public class BatchTrailer : LockboxFileRecord
    {
        [IgnoreDataMember]
        public override RecordTypeDefinition RecordTypeDefinition => new RecordTypeDefinition
        {
            RecordType = Constants.RecordType.BatchTrailer,
            RecordId = Constants.RecordId.BatchTrailer,
            RecordTypeName = typeof(BatchTrailer).FullName
        };

        [FlatFileField(Index = 4, Length = 7)]
        public string TotalBatchInvoiceCount { get; set; }

        [FlatFileField(Index = 11, Length = 12)]
        public string TotalBatchCheckAmount { get; set; }

        [FlatFileField(Index = 275, Length = 3)]
        public string BatchNumber { get; set; }

        [FlatFileField(Index = 283, Length = 7)]
        public string LockboxNumber { get; set; }

        [FlatFileField(Index = 291, Length = 2)]
        public string TransactionGroupNumber { get; set; }
    }
}

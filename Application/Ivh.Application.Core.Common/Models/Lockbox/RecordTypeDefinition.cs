﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ivh.Application.Core.Common.Models.Lockbox
{
    public class RecordTypeDefinition
    {
        public string RecordType { get; set; }
        public string RecordId { get; set; }
        public string RecordTypeName { get; set; }
    }
}

﻿namespace Ivh.Application.Core.Common.Models.Lockbox
{
    using Ivh.Common.Base.Attributes;
    using System.Runtime.Serialization;

    /// <summary>
    /// Part of JMPC Lockbox spec, but not present in the file we are receiving
    /// </summary>
    public class OverflowDetail : LockboxFileRecord
    {
        [IgnoreDataMember]
        public override RecordTypeDefinition RecordTypeDefinition => new RecordTypeDefinition
        {
            RecordType = Constants.RecordType.OverflowDetail,
            RecordId = Constants.RecordId.OverflowDetail,
            RecordTypeName = typeof(OverflowDetail).FullName
        };

        [FlatFileField(Index = 4, Length = 16)]
        public string GuarantorAccountNumber { get; set; }

        [FlatFileField(Index = 20, Length = 25)]
        public string GuarantorFirstName { get; set; }

        [FlatFileField(Index = 45, Length = 36)]
        public string GuarantorLastName { get; set; }

        [FlatFileField(Index = 81, Length = 12)]
        public string InvoiceAmount { get; set; }

        [FlatFileField(Index = 93, Length = 16)]
        public string CreditCardNumber { get; set; }

        [FlatFileField(Index = 109, Length = 10)]
        public string BatchNumber { get; set; }

        [FlatFileField(Index = 119, Length = 10)]
        public string PaymentSequenceNumber { get; set; }

        [FlatFileField(Index = 129, Length = 6)]
        public string CreditCardExpireDate { get; set; }

        [FlatFileField(Index = 135, Length = 10)]
        public string CreditCardAuthNumber { get; set; }

        [FlatFileField(Index = 145, Length = 1)]
        public string CreditCardType { get; set; }

        [FlatFileField(Index = 146, Length = 17)]
        public string CheckPaymentNumber { get; set; }

        [FlatFileField(Index = 176, Length = 4)]
        public string CheckAccountNumber { get; set; }

        [FlatFileField(Index = 180, Length = 9)]
        public string CheckRouting { get; set; }

        [FlatFileField(Index = 189, Length = 61)]
        public string CheckRemitter { get; set; }

        [FlatFileField(Index = 250, Length = 16)]
        public string PsrId { get; set; }

        [FlatFileField(Index = 266, Length = 8)]
        public string BatchCreditDate { get; set; }

        [FlatFileField(Index = 275, Length = 3)]
        public string BatchNumber2 { get; set; }

        [FlatFileField(Index = 279, Length = 3)]
        public string TransactionTransactionSequenceNumber { get; set; }

        [FlatFileField(Index = 283, Length = 7)]
        public string LockboxNumber { get; set; }

        [FlatFileField(Index = 291, Length = 2)]
        public string TransactionGroupNumber { get; set; }

        [FlatFileField(Index = 294, Length = 12)]
        public string PaymentAmount { get; set; }
    }
}

﻿namespace Ivh.Application.Core.Common.Models.Lockbox
{
    using Ivh.Application.Core.Common.Models.Lockbox.Constants;
    using Ivh.Common.Base.Attributes;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;

    public abstract class LockboxFileRecord
    {
        private static List<RecordTypeDefinition> _recordTypeDefinitionList = new List<RecordTypeDefinition>() {
            new RecordTypeDefinition {RecordType = Constants.RecordType.FileHeader, RecordId = Constants.RecordId.FileHeader, RecordTypeName = typeof(FileHeader).FullName },
            new RecordTypeDefinition {RecordType = Constants.RecordType.BatchHeader, RecordId = Constants.RecordId.BatchHeader, RecordTypeName = typeof(BatchHeader).FullName },
            // Included in the spec, but not present in the file
            //new RecordTypeDefinition {RecordType = Constants.RecordType.OverflowDetail, RecordId = Constants.RecordId.OverflowDetail, RecordTypeName = typeof(OverflowDetail).FullName },
            new RecordTypeDefinition {RecordType = Constants.RecordType.Detail, RecordId = Constants.RecordId.Detail, RecordTypeName = typeof(Detail).FullName },
            new RecordTypeDefinition {RecordType = Constants.RecordType.BatchTrailer, RecordId = Constants.RecordId.BatchTrailer, RecordTypeName = typeof(BatchTrailer).FullName },
            new RecordTypeDefinition {RecordType = Constants.RecordType.FileTrailer, RecordId = Constants.RecordId.FileTrailer, RecordTypeName = typeof(FileTrailer).FullName },
            new RecordTypeDefinition {RecordType = Constants.RecordType.DetailTrailer, RecordId = Constants.RecordId.DetailTrailer, RecordTypeName = typeof(DetailTrailer).FullName }
        };

        public abstract RecordTypeDefinition RecordTypeDefinition { get; }

        [FlatFileField(Index = Constants.RecordType.Index, Length = Constants.RecordType.Length)]
        public string RecordType { get; set; }

        // Included in the spec, but not present in the file
        //[FlatFileField(Index = Constants.RecordId.Index, Length = Constants.RecordId.Length)]
        //public string RecordId { get; set; }

        public static object GetFromString(string line)
        {
            if (!string.IsNullOrWhiteSpace(line))
            {
                // adjusting to ZERO based indexing since attribute uses ONE based indexing
                string recordType = GetSubstring(line, Constants.RecordType.Index - 1, Constants.RecordType.Length);
                string recordId = GetSubstring(line, Constants.RecordId.Index - 1, Constants.RecordId.Length);

                // RecordId included in the spec, but not present in the file
                RecordTypeDefinition recordTypeDefinition = _recordTypeDefinitionList.FirstOrDefault(x => x.RecordType == recordType); // && x.RecordId == recordId);
                if (recordTypeDefinition != null)
                {
                    string typeFullName = recordTypeDefinition.RecordTypeName;
                    if (!string.IsNullOrWhiteSpace(typeFullName))
                    {
                        object obj = GetRecordInstance(typeFullName);
                        Load(ref obj, line);
                        return obj;
                    }
                }
                else
                {
                    throw new Exception($"Line is not a known LockboxFileRecord type. Line: {line}");
                }
            }
            return null;
        }

        private static void Load(ref object obj, string line)
        {
            PropertyInfo[] props = obj.GetType().GetProperties();
            IEnumerable<PropertyInfo> writeableProperties = props.Where(x => x.CanWrite);
            foreach (PropertyInfo prop in writeableProperties)
            {
                object[] attrs = prop.GetCustomAttributes(true);
                foreach (object attr in attrs)
                {
                    FlatFileFieldAttribute flatFileFieldAttribute = attr as FlatFileFieldAttribute;
                    if (flatFileFieldAttribute != null)
                    {
                        // adjusting to ZERO based indexing since attribute uses ONE based indexing
                        int startIndex = flatFileFieldAttribute.Index - 1;
                        int length = flatFileFieldAttribute.Length;

                        string value = GetSubstring(line, startIndex, length);
                        prop.SetValue(obj, value, null);
                    }
                }
            }
        }

        // File lines might not match spec exactly so we don't want to throw exception if index or length are out of range
        private static string GetSubstring(string line, int startIndex, int length)
        {
            string value = null;
            bool isIndexInRange = startIndex < line.Length;
            bool isLineLengthInRange = (startIndex + length) <= line.Length;

            if (isIndexInRange && isLineLengthInRange)
            {
                value = line.Substring(startIndex, length);
            }
            return value;
        }

        private static object GetRecordInstance(string typeFullName)
        {
            Type myType = Type.GetType(typeFullName);
            object myObj = Activator.CreateInstance(myType);
            return myObj;
        }
    }
}
﻿namespace Ivh.Application.Core.Common.Models.Lockbox
{
    using Ivh.Common.Base.Attributes;
    using System.Runtime.Serialization;

    public class DetailTrailer : LockboxFileRecord
    {
        [IgnoreDataMember]
        public override RecordTypeDefinition RecordTypeDefinition => new RecordTypeDefinition
        {
            RecordType = Constants.RecordType.DetailTrailer,
            RecordId = Constants.RecordId.DetailTrailer,
            RecordTypeName = nameof(DetailTrailer)
        };

        [FlatFileField(Index = 4, Length = 80)]
        public string PaymentComment { get; set; }

        [FlatFileField(Index = 84, Length = 80)]
        public string EmailAddress { get; set; }

        [FlatFileField(Index = 164, Length = 3)]
        public string DraftReturnCode { get; set; }

        [FlatFileField(Index = 167, Length = 35)]
        public string DraftReturnInfo { get; set; }

        [FlatFileField(Index = 202, Length = 45)]
        public string RdfiBankName { get; set; }

        [FlatFileField(Index = 275, Length = 3)]
        public string BatchNumber { get; set; }

        [FlatFileField(Index = 279, Length = 3)]
        public string TransactionTransactionSequenceNumber { get; set; }

        [FlatFileField(Index = 283, Length = 7)]
        public string LockboxNumber { get; set; }

        [FlatFileField(Index = 291, Length = 2)]
        public string TransactionGroupNumber { get; set; }

        [FlatFileField(Index = 294, Length = 12)]
        public string PaymentAmount { get; set; }
    }
}

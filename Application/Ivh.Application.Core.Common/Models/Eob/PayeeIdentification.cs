﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ivh.Application.Core.Common.Models.Eob
{
public class PayeeIdentification
    {
        public virtual int PayeeIdentificationId { get; set; }

        public virtual string N101EntityIdentifierCode { get; set; }
        public virtual string N102PayeeName { get; set; }
        public virtual string N103IdentificationCodeQualifier { get; set; }
        public virtual string N104IdentificationCode { get; set; }
        public virtual string N105EntityRelationshipCode { get; set; }
        public virtual string N106EntityIdentifierCode { get; set; }

        //public virtual PartyLocation N3PayeeAddress { get; set; }
        //public virtual PartyGeographicLocation N4PayeeCityStateZip { get; set; }
        //public virtual IList<ReferenceInformation> RefAdditionalPayeeIdentification { get; set; }

        public virtual string Rdm01ReportTransmissionCode { get; set; }
        public virtual string Rdm02ThirdPartyProcessorName { get; set; }
        public virtual string Rdm03CommunicationNumber { get; set; }
    }
}

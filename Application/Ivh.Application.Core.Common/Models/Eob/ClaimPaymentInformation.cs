﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ivh.Application.Core.Common.Models.Eob
{
    using Ivh.Common.Base.Utilities.Extensions;

    public class ClaimPaymentInformation
    {
        public virtual int ClaimPaymentInformationId { get; set; }
        public virtual string VisitSourceSystemKey { get; set; }
        public virtual int BillingSystemId { get; set; }
        public virtual int HeaderNumberId { get; set; }
        public virtual string Clp01ClaimSubmittersIdentifier { get; set; }
        public virtual string Clp02ClaimStatusCode { get; set; }
        public virtual decimal Clp03MonetaryAmount { get; set; }
        public virtual decimal Clp04MonetaryAmount { get; set; }
        public virtual decimal Clp05MonetaryAmount { get; set; }
        public virtual string Clp06ClaimFileIndicatorCode { get; set; }
        public virtual string Clp07ReferenceIdentifier { get; set; }
        public virtual string Clp08FacilityCode { get; set; }
        public virtual string Clp09ClaimFrequencyTypeCode { get; set; }
        public virtual string Clp11DiagnosisRelatedGroupCode { get; set; }
        public virtual decimal Clp12Quantity { get; set; }
        public virtual decimal Clp13Percent { get; set; }

        // CAS
        public virtual IList<ClaimAdjustment> CasClaimAdjustments { get; set; }  // to parse the actual file
        public virtual IList<ClaimAdjustmentPivot> ClaimAdjustmentPivots { get; set; } // to normalize data to make sum and group by useful

        // NM1
        public virtual IndividualOrOrganizationalName Nm11PatientName { get; set; }

        // NM1
        //public virtual IndividualOrOrganizationalName Nm12InsuredName { get; set; }

        // NM1
        //public virtual IndividualOrOrganizationalName Nm13CorrectedPatientInsuredName { get; set; }

        // NM1
        //public virtual IndividualOrOrganizationalName Nm14ServiceProviderNameName { get; set; }

        // NM1
        //public virtual IndividualOrOrganizationalName Nm15CrossoverCarrierName { get; set; }

        // NM1
        //public virtual IndividualOrOrganizationalName Nm16CorrectedPriorityPayerName { get; set; }

        // NM1
        //public virtual IndividualOrOrganizationalName Nm17OtherSubscriberName { get; set; }

        // MIA
        public virtual InpatientAdjudicationInformation MiaInpatientInformation { get; set; }
        
        // MOA
        public virtual OutpatientAdjudicationInformation MoaOutpatientInformation { get; set; }

        // REF
        //public virtual IList<ReferenceInformation> Ref1OtherClaimRelatedIdentifications { get; set; }

        // REF
        //public virtual IList<ReferenceInformation> Ref2RenderingProviderIdentifications { get; set; }

        // DTM
        public virtual IList<DateTimeReference> Dtm1StatementFromOrToDates { get; set; }

        // DTM
        public virtual DateTimeReference Dtm2CoverageExpirationDate { get; set; }

        // DTM
        public virtual DateTimeReference Dtm3ClaimReceivedDate { get; set; }
        
        // PER
        //public virtual IList<PersonInformation> PerClaimContactInformations { get; set; }

        // AMT
        //public virtual IList<MonetaryAmountInformation> AmtClaimSupplementalInformations { get; set; }

        // QTY
        //public virtual IList<QuantityInformation> QtyClaimSupplementalInformationQuantities { get; set; }

        // Loop 2110
        public virtual IList<ServicePaymentInformation> ServicePaymentInformations { get; set; }

        // Back reference to its parent
        //public virtual HeaderNumber HeaderNumber { get; set; }

        public override bool Equals(object obj)
        {
            ClaimPaymentInformation other = obj as ClaimPaymentInformation;

            if (ReferenceEquals(null, other))
            {
                return false;
            }
            if (ReferenceEquals(this, other))
            {
                return true;
            }

            return  
                   //this.ClaimPaymentInformationId == other.ClaimPaymentInformationId && // ID cannot be used when checking for value equality
                   this.VisitSourceSystemKey == other.VisitSourceSystemKey &&
                   this.BillingSystemId == other.BillingSystemId &&
                   this.HeaderNumberId == other.HeaderNumberId &&
                   this.Clp01ClaimSubmittersIdentifier == other.Clp01ClaimSubmittersIdentifier &&
                   this.Clp02ClaimStatusCode == other.Clp02ClaimStatusCode &&
                   this.Clp03MonetaryAmount == other.Clp03MonetaryAmount &&
                   this.Clp04MonetaryAmount == other.Clp04MonetaryAmount &&
                   this.Clp05MonetaryAmount == other.Clp05MonetaryAmount &&
                   this.Clp06ClaimFileIndicatorCode == other.Clp06ClaimFileIndicatorCode &&
                   // VP-4883 CLP07 is omitted on purpose so duplicate is detected if the carrier's Internal Control Number is the only difference
                   // this.Clp07ReferenceIdentifier == other.Clp07ReferenceIdentifier &&
                   this.Clp08FacilityCode == other.Clp08FacilityCode &&
                   this.Clp09ClaimFrequencyTypeCode == other.Clp09ClaimFrequencyTypeCode &&
                   this.Clp11DiagnosisRelatedGroupCode == other.Clp11DiagnosisRelatedGroupCode &&
                   this.Clp12Quantity == other.Clp12Quantity &&
                   this.Clp13Percent == other.Clp13Percent &&

                   this.Nm11PatientName.EqualsSafe(other.Nm11PatientName) &&
                   this.MiaInpatientInformation.EqualsSafe(other.MiaInpatientInformation) &&
                   this.MoaOutpatientInformation.EqualsSafe(other.MoaOutpatientInformation) &&
                   this.Dtm2CoverageExpirationDate.EqualsSafe(other.Dtm2CoverageExpirationDate) &&
                   this.Dtm3ClaimReceivedDate.EqualsSafe(other.Dtm3ClaimReceivedDate) &&

                   (this.CasClaimAdjustments).SequencesAreEqual(other.CasClaimAdjustments) &&
                   (this.ClaimAdjustmentPivots).SequencesAreEqual(other.ClaimAdjustmentPivots) &&
                   (this.Dtm1StatementFromOrToDates).SequencesAreEqual(other.Dtm1StatementFromOrToDates) &&
                   (this.ServicePaymentInformations).SequencesAreEqual(other.ServicePaymentInformations)
                ;
        }

        public override int GetHashCode()
        {
            unchecked
            {
                int hash = this.GetType().GetHashCode();
                // hash = (hash * 31) ^ this.ClaimPaymentInformationId.GetHashCode(); // ID cannot be used when checking for value equality
                hash = (hash * 31) ^ this.VisitSourceSystemKey.GetHashCodeSafe();
                hash = (hash * 31) ^ this.BillingSystemId.GetHashCode();
                hash = (hash * 31) ^ this.HeaderNumberId.GetHashCode();
                hash = (hash * 31) ^ this.Clp01ClaimSubmittersIdentifier.GetHashCodeSafe();
                hash = (hash * 31) ^ this.Clp02ClaimStatusCode.GetHashCodeSafe();
                hash = (hash * 31) ^ this.Clp03MonetaryAmount.GetHashCode();
                hash = (hash * 31) ^ this.Clp04MonetaryAmount.GetHashCode();
                hash = (hash * 31) ^ this.Clp05MonetaryAmount.GetHashCode();
                hash = (hash * 31) ^ this.Clp06ClaimFileIndicatorCode.GetHashCodeSafe();
                // VP-4883 CLP07 is omitted on purpose so duplicate is detected if the carrier's Internal Control Number is the only difference
                //hash = (hash * 31) ^ this.Clp07ReferenceIdentifier.GetHashCodeSafe();
                hash = (hash * 31) ^ this.Clp08FacilityCode.GetHashCodeSafe();
                hash = (hash * 31) ^ this.Clp09ClaimFrequencyTypeCode.GetHashCodeSafe();
                hash = (hash * 31) ^ this.Clp11DiagnosisRelatedGroupCode.GetHashCodeSafe();
                hash = (hash * 31) ^ this.Clp12Quantity.GetHashCode();
                hash = (hash * 31) ^ this.Clp13Percent.GetHashCode();

                hash = (hash * 31) ^ this.Nm11PatientName.GetHashCodeSafe();
                hash = (hash * 31) ^ this.MiaInpatientInformation.GetHashCodeSafe();
                hash = (hash * 31) ^ this.MoaOutpatientInformation.GetHashCodeSafe();
                hash = (hash * 31) ^ this.Dtm2CoverageExpirationDate.GetHashCodeSafe();
                hash = (hash * 31) ^ this.Dtm3ClaimReceivedDate.GetHashCodeSafe();

                hash = (hash * 31) ^ this.CasClaimAdjustments.GetSequenceHashCode();
                hash = (hash * 31) ^ this.ClaimAdjustmentPivots.GetSequenceHashCode();
                hash = (hash * 31) ^ this.Dtm1StatementFromOrToDates.GetSequenceHashCode();
                hash = (hash * 31) ^ this.ServicePaymentInformations.GetSequenceHashCode();

                return hash;
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ivh.Application.Core.Common.Models.Eob
{
    using Ivh.Common.Base.Utilities.Extensions;

    public class EobClaimAdjustmentReasonCode //aka CARC in EDI terminology
    {
        public virtual int EobClaimAdjustmentReasonCodeId { get; set; }
        public virtual string ClaimAdjustmentReasonCode { get; set; }
        public virtual string UIDisplay { get; set; }
        public virtual string Description { get; set; }
        public virtual DateTime StartDate { get; set; }
        public virtual DateTime EndDate { get; set; }

        public override bool Equals(object obj) // This is a lookup value, so only code needs to be used
        {
            var other = obj as EobClaimAdjustmentReasonCode;

            if (ReferenceEquals(null, other))
            {
                return false;
            }
            if (ReferenceEquals(this, other))
            {
                return true;
            }

            return 
                // this.EobClaimAdjustmentReasonCodeId == other.EobClaimAdjustmentReasonCodeId &&  // ID cannot be used when checking for value equality
                this.ClaimAdjustmentReasonCode == other.ClaimAdjustmentReasonCode;
        }

        public override int GetHashCode() // This is a lookup value, so only code needs to be used
        {
            unchecked
            {
                int hash = this.GetType().GetHashCode();
                //hash = (hash * 31) ^ EobClaimAdjustmentReasonCodeId.GetHashCode(); // ID cannot be used when checking for value equality
                hash = (hash * 31) ^ this.ClaimAdjustmentReasonCode.GetHashCodeSafe();

                return hash;
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ivh.Application.Core.Common.Models.Eob
{
    using Ivh.Common.Base.Utilities.Extensions;

    public class IndustryCodeIdentification
    {
        public virtual int IndustryCodeIdentificationId { get; set; }
        public virtual string Lq01CodeListQualifierCode { get; set; }
        public virtual string Lq02IndustryCode { get; set; }

        // for the description of the Lq02IndustryCode
        public virtual EobRemittanceAdviceRemarkCode EobRemittanceAdviceRemarkCode { get; set; }

        // Back reference to its parent
        //public virtual ServicePaymentInformation ServicePaymentInformation { get; set; }

        public override bool Equals(object obj)
        {
            var other = obj as IndustryCodeIdentification;

            if (ReferenceEquals(null, other))
            {
                return false;
            }
            if (ReferenceEquals(this, other))
            {
                return true;
            }

            return 
                   //this.IndustryCodeIdentificationId == other.IndustryCodeIdentificationId &&  // ID cannot be used when checking for value equality
                   this.Lq01CodeListQualifierCode == other.Lq01CodeListQualifierCode &&
                   this.Lq02IndustryCode == other.Lq02IndustryCode &&
                   this.EobRemittanceAdviceRemarkCode.EqualsSafe(other.EobRemittanceAdviceRemarkCode);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                int hash = this.GetType().GetHashCode();
                //hash = (hash * 31) ^ this.IndustryCodeIdentificationId.GetHashCode(); // ID cannot be used when checking for value equality
                hash = (hash * 31) ^ this.Lq01CodeListQualifierCode.GetHashCodeSafe();
                hash = (hash * 31) ^ this.Lq02IndustryCode.GetHashCodeSafe();
                hash = (hash * 31) ^ this.EobRemittanceAdviceRemarkCode.GetHashCodeSafe();

                return hash;
            }
        }
    }
}

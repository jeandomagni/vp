﻿namespace Ivh.Application.Core.Common.Models.Eob
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    public class VisitEobServicePaymentInformationModel
    {
        public IList<VisitEobClaimAdjustmentModel> ClaimAdjustments { get; set; }
        public DateTime ServiceStartDate { get; set; }
        public DateTime ServiceEndDate { get; set; }
        public IList<VisitEobIndustryCodeIdentificationModel> LqHealthCareRemarkCodes { get; set; }

        public virtual IList<VisitEobIndustryCodeIdentificationModel> LqHealthCareRemarkCodesUnique
        {
            get
            {
                return this.LqHealthCareRemarkCodes
                    .GroupBy(x => x.IndustryCode)
                    .Select(x => x.First())
                    .ToList();
            }
        }

        public virtual IList<VisitEobClaimAdjustmentModel> ClaimAdjustmentsSummed
        {
            get
            {
                return this.ClaimAdjustments
                    .GroupBy(x => x.UIDisplay)
                    .Select(g => new VisitEobClaimAdjustmentModel { UIDisplay = g.Key, MonetaryAmount = g.Sum(e => e.MonetaryAmount) })
                    .ToList();
            }
        }
    }
}
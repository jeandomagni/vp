﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ivh.Application.Core.Common.Models.Eob
{
    public class DateTimeReference
    {
        public virtual int DateTimeReferenceId { get; set; }
        public virtual string Dtm01DateTimeQualifier { get; set; }
        public virtual DateTime Dtm02Date { get; set; }

        // Back references to its parents
        //public virtual TransactionSetHeaderTrailer835 TransactionSetHeaderTrailer { get; set; }
        //public virtual ClaimPaymentInformation ClaimPaymentInformation { get; set; }
        //public virtual ServicePaymentInformation ServicePaymentInformation { get; set; }

        public override bool Equals(object obj)
        {
            DateTimeReference other = obj as DateTimeReference;

            if (ReferenceEquals(null, other))
            {
                return false;
            }
            if (ReferenceEquals(this, other))
            {
                return true;
            }

            return 
                   //this.DateTimeReferenceId == other.DateTimeReferenceId && // ID cannot be used when checking for value equality
                   this.Dtm01DateTimeQualifier == other.Dtm01DateTimeQualifier &&
                   this.Dtm02Date == other.Dtm02Date
                ;
        }

        public override int GetHashCode()
        {
            unchecked
            {
                int hash = this.GetType().GetHashCode();
                //hash = (hash * 31) ^ this.DateTimeReferenceId.GetHashCode(); // ID cannot be used when checking for value equality
                hash = (hash * 31) ^ this.Dtm01DateTimeQualifier.GetHashCode();
                hash = (hash * 31) ^ this.Dtm02Date.GetHashCode();

                return hash;
            }
        }
    }
}

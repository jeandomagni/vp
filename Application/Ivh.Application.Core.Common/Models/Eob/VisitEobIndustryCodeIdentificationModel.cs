﻿namespace Ivh.Application.Core.Common.Models.Eob
{
    public class VisitEobIndustryCodeIdentificationModel
    {
        public string IndustryCode { get; set; }
        public string Description { get; set; }
    }
}
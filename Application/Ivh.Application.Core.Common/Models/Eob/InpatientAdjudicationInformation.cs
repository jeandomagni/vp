﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ivh.Application.Core.Common.Models.Eob
{
    using Ivh.Common.Base.Utilities.Extensions;

    public class InpatientAdjudicationInformation
    {
        public virtual int InpatientAdjudicationInformationId { get; set; }
        public virtual int Mia01Quantity { get; set; }
        public virtual decimal Mia02MonetaryAmount { get; set; }
        public virtual int Mia03Quantity { get; set; }
        public virtual decimal Mia04MonetaryAmount { get; set; }
        public virtual string Mia05ReferenceIdentifier { get; set; }
        public virtual decimal Mia06MonetaryAmount { get; set; }
        public virtual decimal Mia07MonetaryAmount { get; set; }
        public virtual decimal Mia08MonetaryAmount { get; set; }
        public virtual decimal Mia09MonetaryAmount { get; set; }
        public virtual decimal Mia10MonetaryAmount { get; set; }
        public virtual decimal Mia11MonetaryAmount { get; set; }
        public virtual decimal Mia12MonetaryAmount { get; set; }
        public virtual decimal Mia13MonetaryAmount { get; set; }
        public virtual decimal Mia14MonetaryAmount { get; set; }
        public virtual int Mia15Quantity { get; set; }
        public virtual decimal Mia16MonetaryAmount { get; set; }
        public virtual decimal Mia17MonetaryAmount { get; set; }
        public virtual decimal Mia18MonetaryAmount { get; set; }
        public virtual decimal Mia19MonetaryAmount { get; set; }
        public virtual string Mia20ReferenceIdentifier { get; set; }
        public virtual string Mia21ReferenceIdentifier { get; set; }
        public virtual string Mia22ReferenceIdentifier { get; set; }
        public virtual string Mia23ReferenceIdentifier { get; set; }
        public virtual decimal Mia24MonetaryAmount { get; set; }

        public virtual EobRemittanceAdviceRemarkCode Mia05EobRemittanceAdviceRemarkCode { get; set; }

        public override bool Equals(object obj)
        {
            var other = obj as InpatientAdjudicationInformation;

            if (ReferenceEquals(null, other))
            {
                return false;
            }
            if (ReferenceEquals(this, other))
            {
                return true;
            }

            return 
                   //this.InpatientAdjudicationInformationId == other.InpatientAdjudicationInformationId && // ID cannot be used when checking for value equality
                   this.Mia01Quantity == other.Mia01Quantity &&
                   this.Mia02MonetaryAmount == other.Mia02MonetaryAmount &&
                   this.Mia03Quantity == other.Mia03Quantity &&
                   this.Mia04MonetaryAmount == other.Mia04MonetaryAmount &&
                   this.Mia05ReferenceIdentifier == other.Mia05ReferenceIdentifier &&
                   this.Mia06MonetaryAmount == other.Mia06MonetaryAmount &&
                   this.Mia07MonetaryAmount == other.Mia07MonetaryAmount &&
                   this.Mia08MonetaryAmount == other.Mia08MonetaryAmount &&
                   this.Mia09MonetaryAmount == other.Mia09MonetaryAmount &&
                   this.Mia10MonetaryAmount == other.Mia10MonetaryAmount &&
                   this.Mia11MonetaryAmount == other.Mia11MonetaryAmount &&
                   this.Mia12MonetaryAmount == other.Mia12MonetaryAmount &&
                   this.Mia13MonetaryAmount == other.Mia13MonetaryAmount &&
                   this.Mia14MonetaryAmount == other.Mia14MonetaryAmount &&
                   this.Mia15Quantity == other.Mia15Quantity &&
                   this.Mia16MonetaryAmount == other.Mia16MonetaryAmount &&
                   this.Mia17MonetaryAmount == other.Mia17MonetaryAmount &&
                   this.Mia18MonetaryAmount == other.Mia18MonetaryAmount &&
                   this.Mia19MonetaryAmount == other.Mia19MonetaryAmount &&
                   this.Mia20ReferenceIdentifier == other.Mia20ReferenceIdentifier &&
                   this.Mia21ReferenceIdentifier == other.Mia21ReferenceIdentifier &&
                   this.Mia22ReferenceIdentifier == other.Mia22ReferenceIdentifier &&
                   this.Mia23ReferenceIdentifier == other.Mia23ReferenceIdentifier &&
                   this.Mia24MonetaryAmount == other.Mia24MonetaryAmount &&
                   this.Mia05EobRemittanceAdviceRemarkCode.EqualsSafe(other.Mia05EobRemittanceAdviceRemarkCode);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                int hash = this.GetType().GetHashCode();
                //hash = (hash * 31) ^ this.InpatientAdjudicationInformationId.GetHashCode(); // ID cannot be used when checking for value equality
                hash = (hash * 31) ^ this.Mia01Quantity.GetHashCode();
                hash = (hash * 31) ^ this.Mia02MonetaryAmount.GetHashCode();
                hash = (hash * 31) ^ this.Mia03Quantity.GetHashCode();
                hash = (hash * 31) ^ this.Mia04MonetaryAmount.GetHashCode();
                hash = (hash * 31) ^ this.Mia05ReferenceIdentifier.GetHashCodeSafe();
                hash = (hash * 31) ^ this.Mia06MonetaryAmount.GetHashCode();
                hash = (hash * 31) ^ this.Mia07MonetaryAmount.GetHashCode();
                hash = (hash * 31) ^ this.Mia08MonetaryAmount.GetHashCode();
                hash = (hash * 31) ^ this.Mia09MonetaryAmount.GetHashCode();
                hash = (hash * 31) ^ this.Mia10MonetaryAmount.GetHashCode();
                hash = (hash * 31) ^ this.Mia11MonetaryAmount.GetHashCode();
                hash = (hash * 31) ^ this.Mia12MonetaryAmount.GetHashCode();
                hash = (hash * 31) ^ this.Mia13MonetaryAmount.GetHashCode();
                hash = (hash * 31) ^ this.Mia14MonetaryAmount.GetHashCode();
                hash = (hash * 31) ^ this.Mia15Quantity.GetHashCode();
                hash = (hash * 31) ^ this.Mia16MonetaryAmount.GetHashCode();
                hash = (hash * 31) ^ this.Mia17MonetaryAmount.GetHashCode();
                hash = (hash * 31) ^ this.Mia18MonetaryAmount.GetHashCode();
                hash = (hash * 31) ^ this.Mia19MonetaryAmount.GetHashCode();
                hash = (hash * 31) ^ this.Mia20ReferenceIdentifier.GetHashCodeSafe();
                hash = (hash * 31) ^ this.Mia21ReferenceIdentifier.GetHashCodeSafe();
                hash = (hash * 31) ^ this.Mia22ReferenceIdentifier.GetHashCodeSafe();
                hash = (hash * 31) ^ this.Mia23ReferenceIdentifier.GetHashCodeSafe();
                hash = (hash * 31) ^ this.Mia24MonetaryAmount.GetHashCode();
                hash = (hash * 31) ^ this.Mia05EobRemittanceAdviceRemarkCode.GetHashCodeSafe();

                return hash;
            }
        }
    }
}

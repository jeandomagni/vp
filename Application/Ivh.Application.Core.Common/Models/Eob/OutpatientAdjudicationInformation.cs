﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ivh.Application.Core.Common.Models.Eob
{
    using Ivh.Common.Base.Utilities.Extensions;

    public class OutpatientAdjudicationInformation
    {
        public virtual int OutpatientAdjudicationInformationId { get; set; }
        public virtual decimal Moa01Percent { get; set; }
        public virtual decimal Moa02MonetaryAmount { get; set; }
        public virtual string Moa03ReferenceIdentifier { get; set; }
        public virtual string Moa04ReferenceIdentifier { get; set; }
        public virtual string Moa05ReferenceIdentifier { get; set; }
        public virtual string Moa06ReferenceIdentifier { get; set; }
        public virtual string Moa07ReferenceIdentifier { get; set; }
        public virtual decimal Moa08MonetaryAmount { get; set; }
        public virtual decimal Moa09MonetaryAmount { get; set; }

        public virtual EobRemittanceAdviceRemarkCode Moa03EobRemittanceAdviceRemarkCode { get; set; }

        public override bool Equals(object obj)
        {
            var other = obj as OutpatientAdjudicationInformation;

            if (ReferenceEquals(null, other))
            {
                return false;
            }
            if (ReferenceEquals(this, other))
            {
                return true;
            }

            return 
                   //this.OutpatientAdjudicationInformationId == other.OutpatientAdjudicationInformationId && // ID cannot be used when checking for value equality
                   this.Moa01Percent == other.Moa01Percent &&
                   this.Moa02MonetaryAmount == other.Moa02MonetaryAmount &&
                   this.Moa03ReferenceIdentifier == other.Moa03ReferenceIdentifier &&
                   this.Moa04ReferenceIdentifier == other.Moa04ReferenceIdentifier &&
                   this.Moa05ReferenceIdentifier == other.Moa05ReferenceIdentifier &&
                   this.Moa06ReferenceIdentifier == other.Moa06ReferenceIdentifier &&
                   this.Moa07ReferenceIdentifier == other.Moa07ReferenceIdentifier &&
                   this.Moa08MonetaryAmount == other.Moa08MonetaryAmount &&
                   this.Moa09MonetaryAmount == other.Moa09MonetaryAmount &&
                   this.Moa03EobRemittanceAdviceRemarkCode.EqualsSafe(other.Moa03EobRemittanceAdviceRemarkCode)
                ;
        }

        public override int GetHashCode()
        {
            unchecked
            {
                int hash = this.GetType().GetHashCode();
                //hash = (hash * 31) ^ this.OutpatientAdjudicationInformationId.GetHashCode();  // ID cannot be used when checking for value equality
                hash = (hash * 31) ^ this.Moa01Percent.GetHashCode();
                hash = (hash * 31) ^ this.Moa02MonetaryAmount.GetHashCode();
                hash = (hash * 31) ^ this.Moa03ReferenceIdentifier.GetHashCodeSafe();
                hash = (hash * 31) ^ this.Moa04ReferenceIdentifier.GetHashCodeSafe();
                hash = (hash * 31) ^ this.Moa05ReferenceIdentifier.GetHashCodeSafe();
                hash = (hash * 31) ^ this.Moa06ReferenceIdentifier.GetHashCodeSafe();
                hash = (hash * 31) ^ this.Moa07ReferenceIdentifier.GetHashCodeSafe();
                hash = (hash * 31) ^ this.Moa08MonetaryAmount.GetHashCode();
                hash = (hash * 31) ^ this.Moa09MonetaryAmount.GetHashCode();
                hash = (hash * 31) ^ this.Moa03EobRemittanceAdviceRemarkCode.GetHashCodeSafe();

                return hash;
            }
        }
        
    }
}

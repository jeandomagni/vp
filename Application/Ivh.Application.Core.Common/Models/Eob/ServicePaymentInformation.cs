﻿using Ivh.Common.Base.Utilities.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ivh.Application.Core.Common.Models.Eob
{
    public class ServicePaymentInformation
    {
        public virtual int ServicePaymentInformationId { get; set; }
        public virtual string Svc01CompositeMedicalProcedureIdentifier { get; set; }
        public virtual decimal Svc02MonetaryAmount { get; set; }
        public virtual decimal Svc03MonetaryAmount { get; set; }
        public virtual string Svc04ProductServiceId { get; set; }
        public virtual decimal Svc05Quantity { get; set; }
        public virtual string Svc06CompositeMedicalProcedureIdentifier { get; set; }
        public virtual decimal Svc07Quantity { get; set; }

        // DTM
        public virtual IList<DateTimeReference> DtmServiceDates { get; set; }

        // CAS
        public virtual IList<ClaimAdjustment> CasServiceAdjustments { get; set; }  // to parse the actual file
        public virtual IList<ClaimAdjustmentPivot> ClaimAdjustmentPivots { get; set; } // to normalize data to make sum and group by useful

        // REF
        //[EdiCondition("1S", "APC", "BB", "E9", "G1", "G3", "LU", "RB", Path = "REF/0/0")]
        //public virtual IList<ReferenceInformation> Ref1ServiceIdentifications { get; set; }

        // REF
        //[EdiCondition("6R", Path = "REF/0/0")]
        //public virtual ReferenceInformation Ref2LineItemControlNumber { get; set; }

        // REF
        //[EdiCondition("0B", "1A", "1B", "1C", "1D", "1G", "1H", "1J", "D3", "G2", "HPI", "SY", "TJ", Path = "REF/0/0")]
        //public virtual IList<ReferenceInformation> Ref3RenderingProviderIdentifications { get; set; }

        // REF
        //[EdiCondition("0K", Path = "REF/0/0")]
        //public virtual IList<ReferenceInformation> Ref4HealthCarePolicyIdentifications { get; set; }

        // AMT
        //public virtual IList<MonetaryAmountInformation> AmtServiceSupplementalAmounts { get; set; }

        // QTY
        //public virtual IList<QuantityInformation> QtyServiceSupplementalQuantities { get; set; }

        //LQ
        public virtual IList<IndustryCodeIdentification> LqHealthCareRemarkCodes { get; set; }

        // Back reference to its parent
        //public virtual ClaimPaymentInformation ClaimPaymentInformation { get; set; }

        public override bool Equals(object obj)
        {
            ServicePaymentInformation other = obj as ServicePaymentInformation;

            if (ReferenceEquals(null, other))
            {
                return false;
            }
            if (ReferenceEquals(this, other))
            {
                return true;
            }

            return 
                   //this.ServicePaymentInformationId == other.ServicePaymentInformationId && // ID cannot be used when checking for value equality
                   this.Svc01CompositeMedicalProcedureIdentifier == other.Svc01CompositeMedicalProcedureIdentifier &&
                   this.Svc02MonetaryAmount == other.Svc02MonetaryAmount &&
                   this.Svc03MonetaryAmount == other.Svc03MonetaryAmount &&
                   this.Svc04ProductServiceId == other.Svc04ProductServiceId &&
                   this.Svc05Quantity == other.Svc05Quantity &&
                   this.Svc06CompositeMedicalProcedureIdentifier == other.Svc06CompositeMedicalProcedureIdentifier &&
                   this.Svc07Quantity == other.Svc07Quantity &&
                
                   this.DtmServiceDates.SequencesAreEqual(other.DtmServiceDates) &&
                   this.CasServiceAdjustments.SequencesAreEqual(other.CasServiceAdjustments) &&
                   this.ClaimAdjustmentPivots.SequencesAreEqual(other.ClaimAdjustmentPivots) &&
                   this.LqHealthCareRemarkCodes.SequencesAreEqual(other.LqHealthCareRemarkCodes)
                ;
        }

        public override int GetHashCode()
        {
            unchecked
            {
                int hash = this.GetType().GetHashCode();

                //hash = (hash * 31) ^ this.ServicePaymentInformationId.GetHashCode(); // ID cannot be used when checking for value equality
                hash = (hash * 31) ^ this.Svc01CompositeMedicalProcedureIdentifier.GetHashCodeSafe();
                hash = (hash * 31) ^ this.Svc02MonetaryAmount.GetHashCode();
                hash = (hash * 31) ^ this.Svc03MonetaryAmount.GetHashCode();
                hash = (hash * 31) ^ this.Svc04ProductServiceId.GetHashCodeSafe();
                hash = (hash * 31) ^ this.Svc05Quantity.GetHashCode();
                hash = (hash * 31) ^ this.Svc06CompositeMedicalProcedureIdentifier.GetHashCodeSafe();
                hash = (hash * 31) ^ this.Svc07Quantity.GetHashCode();

                hash = (hash * 31) ^ this.DtmServiceDates.GetSequenceHashCode();
                hash = (hash * 31) ^ this.CasServiceAdjustments.GetSequenceHashCode();
                hash = (hash * 31) ^ this.ClaimAdjustmentPivots.GetSequenceHashCode();
                hash = (hash * 31) ^ this.LqHealthCareRemarkCodes.GetSequenceHashCode();

                return hash;
            }
        }
    }
}

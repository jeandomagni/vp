﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ivh.Application.Core.Common.Models.Eob
{
    using Ivh.Common.Base.Utilities.Extensions;

    public class EobRemittanceAdviceRemarkCode //aka RARC in EDI terminology
    {
        public virtual int EobRemittanceAdviceRemarkCodeId { get; set; }
        public virtual string RemittanceAdviceRemarkCode { get; set; }
        public virtual string Description { get; set; }
        public virtual DateTime StartDate { get; set; }
        public virtual DateTime EndDate { get; set; }

        public override bool Equals(object obj) // This is a lookup value, so only code needs to be used
        {
            var other = obj as EobRemittanceAdviceRemarkCode;

            if (ReferenceEquals(null, other))
            {
                return false;
            }
            if (ReferenceEquals(this, other))
            {
                return true;
            }

            return 
                   //this.EobRemittanceAdviceRemarkCodeId == other.EobRemittanceAdviceRemarkCodeId &&  // ID cannot be used when checking for value equality
                   this.RemittanceAdviceRemarkCode == other.RemittanceAdviceRemarkCode
                ;
        }

        public override int GetHashCode() // This is a lookup value, so only code needs to be used
        {
            unchecked
            {
                int hash = GetType().GetHashCode();
                //hash = (hash * 31) ^ this.EobRemittanceAdviceRemarkCodeId.GetHashCode();  // ID cannot be used when checking for value equality
                hash = (hash * 31) ^ this.RemittanceAdviceRemarkCode.GetHashCodeSafe();

                return hash;
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Ivh.Application.Core.Common.Models.Eob
{
    public class EobIndicatorResultModel
    {
        public int BillingSystemId { get; set; }
        public string VisitSourceSystemKey { get; set; }
        public bool HasEob { get; set; }
        public virtual string LogoUrl { get; set; }
    }
}
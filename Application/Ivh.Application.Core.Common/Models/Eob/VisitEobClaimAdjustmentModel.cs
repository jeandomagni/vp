﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Ivh.Application.Core.Common.Models.Eob
{
    public class VisitEobClaimAdjustmentModel
    {
        public decimal MonetaryAmount { get; set; }
        public string UIDisplay { get; set; }
    }
}
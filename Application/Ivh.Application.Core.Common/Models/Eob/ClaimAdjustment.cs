﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ivh.Application.Core.Common.Models.Eob
{
    using Ivh.Common.Base.Utilities.Extensions;

    public class ClaimAdjustment
    {
        public virtual string Cas01ClaimAdjustmentGroupCode { get; set; }
        public virtual string Cas02ClaimAdjustmentReasonCode { get; set; }
        public virtual decimal Cas03MonetaryAmount { get; set; }
        public virtual decimal Cas04Quantity { get; set; }
        public virtual string Cas05ClaimAdjustmentReasonCode { get; set; }
        public virtual decimal Cas06MonetaryAmount { get; set; }
        public virtual decimal Cas07Quantity { get; set; }
        public virtual string Cas08ClaimAdjustmentReasonCode { get; set; }
        public virtual decimal Cas09MonetaryAmount { get; set; }
        public virtual decimal Cas10Quantity { get; set; }
        public virtual string Cas11ClaimAdjustmentReasonCode { get; set; }
        public virtual decimal Cas12MonetaryAmount { get; set; }
        public virtual decimal Cas13Quantity { get; set; }
        public virtual string Cas14ClaimAdjustmentReasonCode { get; set; }
        public virtual decimal Cas15MonetaryAmount { get; set; }
        public virtual decimal Cas16Quantity { get; set; }
        public virtual string Cas17ClaimAdjustmentReasonCode { get; set; }
        public virtual decimal Cas18MonetaryAmount { get; set; }
        public virtual decimal Cas19Quantity { get; set; }

        public override bool Equals(object obj)
        {
            var other = obj as ClaimAdjustment;

            if (ReferenceEquals(null, other))
            {
                return false;
            }
            if (ReferenceEquals(this, other))
            {
                return true;
            }

            return 
                   this.Cas01ClaimAdjustmentGroupCode == other.Cas01ClaimAdjustmentGroupCode &&
                   this.Cas02ClaimAdjustmentReasonCode == other.Cas02ClaimAdjustmentReasonCode &&
                   this.Cas03MonetaryAmount == other.Cas03MonetaryAmount &&
                   this.Cas04Quantity == other.Cas04Quantity &&
                   this.Cas05ClaimAdjustmentReasonCode == other.Cas05ClaimAdjustmentReasonCode &&
                   this.Cas06MonetaryAmount == other.Cas06MonetaryAmount &&
                   this.Cas07Quantity == other.Cas07Quantity &&
                   this.Cas08ClaimAdjustmentReasonCode == other.Cas08ClaimAdjustmentReasonCode &&
                   this.Cas09MonetaryAmount == other.Cas09MonetaryAmount &&
                   this.Cas10Quantity == other.Cas10Quantity &&
                   this.Cas11ClaimAdjustmentReasonCode == other.Cas11ClaimAdjustmentReasonCode &&
                   this.Cas12MonetaryAmount == other.Cas12MonetaryAmount &&
                   this.Cas13Quantity == other.Cas13Quantity &&
                   this.Cas14ClaimAdjustmentReasonCode == other.Cas14ClaimAdjustmentReasonCode &&
                   this.Cas15MonetaryAmount == other.Cas15MonetaryAmount &&
                   this.Cas16Quantity == other.Cas16Quantity &&
                   this.Cas17ClaimAdjustmentReasonCode == other.Cas17ClaimAdjustmentReasonCode &&
                   this.Cas18MonetaryAmount == other.Cas18MonetaryAmount &&
                   this.Cas19Quantity == other.Cas19Quantity
                ;
        }

        public override int GetHashCode()
        {
            unchecked
            {
                int hash = this.GetType().GetHashCode();
                hash = (hash * 31) ^ this.Cas01ClaimAdjustmentGroupCode.GetHashCodeSafe();
                hash = (hash * 31) ^ this.Cas02ClaimAdjustmentReasonCode.GetHashCodeSafe();
                hash = (hash * 31) ^ this.Cas03MonetaryAmount.GetHashCode();
                hash = (hash * 31) ^ this.Cas04Quantity.GetHashCode();
                hash = (hash * 31) ^ this.Cas05ClaimAdjustmentReasonCode.GetHashCodeSafe();
                hash = (hash * 31) ^ this.Cas06MonetaryAmount.GetHashCode();
                hash = (hash * 31) ^ this.Cas07Quantity.GetHashCode();
                hash = (hash * 31) ^ this.Cas08ClaimAdjustmentReasonCode.GetHashCodeSafe();
                hash = (hash * 31) ^ this.Cas09MonetaryAmount.GetHashCode();
                hash = (hash * 31) ^ this.Cas10Quantity.GetHashCode();
                hash = (hash * 31) ^ this.Cas11ClaimAdjustmentReasonCode.GetHashCodeSafe();
                hash = (hash * 31) ^ this.Cas12MonetaryAmount.GetHashCode();
                hash = (hash * 31) ^ this.Cas13Quantity.GetHashCode();
                hash = (hash * 31) ^ this.Cas14ClaimAdjustmentReasonCode.GetHashCodeSafe();
                hash = (hash * 31) ^ this.Cas15MonetaryAmount.GetHashCode();
                hash = (hash * 31) ^ this.Cas16Quantity.GetHashCode();
                hash = (hash * 31) ^ this.Cas17ClaimAdjustmentReasonCode.GetHashCodeSafe();
                hash = (hash * 31) ^ this.Cas18MonetaryAmount.GetHashCode();
                hash = (hash * 31) ^ this.Cas19Quantity.GetHashCode();

                return hash;
            }
        }
    }
}

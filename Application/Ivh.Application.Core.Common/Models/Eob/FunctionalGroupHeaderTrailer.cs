﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ivh.Application.Core.Common.Models.Eob
{
public class FunctionalGroupHeaderTrailer
    {
        public virtual int FunctionalGroupHeaderTrailerId { get; set; }

        public virtual string Gs01FunctionalIdentifierCode { get; set; }
        public virtual string Gs02ApplicationSenderCode { get; set; }
        public virtual string Gs03ApplicationReceiverCode { get; set; }
        public virtual DateTime Gs0405DateTime { get; set; }
        public virtual int Gs06GroupControlNumber { get; set; }
        public virtual string Gs07AgencyCode { get; set; }
        public virtual string Gs08Version { get; set; }

        // ST - SE
        public virtual IList<TransactionSetHeaderTrailer835> TransactionHeaders { get; set; }


        public virtual int Ge01TransactionsCount { get; set; }
        public virtual int Ge02GroupTrailerControlNumber { get; set; }

        // Back reference to its parent
        //public virtual InterchangeControlHeaderTrailer InterchangeControlHeaderTrailer { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ivh.Application.Core.Common.Models.Eob
{
public class InterchangeControlHeaderTrailer
    {
        public virtual int InterchangeControlHeaderTrailerId { get; set; }
        public virtual int? FileTrackerId { get; set; }

        public virtual int Isa01AuthorizationInformationQualifier { get; set; }
        public virtual string Isa02AuthorizationInformation { get; set; }
        public virtual string Isa03SecurityInformationQualifier { get; set; }
        public virtual string Isa04SecurityInformation { get; set; }
        public virtual string Isa05IdQualifier { get; set; }
        public virtual string Isa06SenderId { get; set; }
        public virtual string Isa07IdQualifier { get; set; }
        public virtual string Isa08ReceiverId { get; set; }
        public virtual DateTime Isa0910DateTime { get; set; }
        public virtual string Isa11ControlStandardsId { get; set; }
        public virtual int Isa12ControlVersion { get; set; }
        public virtual int Isa13ControlNumber { get; set; }
        public virtual bool? Isa14AcknowledgementRequested { get; set; }
        public virtual string Isa15UsageIndicator { get; set; }
        public virtual char Isa16ComponentElementSeparator { get; set; }

        public virtual int Iea01GroupsCount { get; set; }
        public virtual int Iea02TrailerControlNumber { get; set; }

        // GS - GE
        public virtual IList<FunctionalGroupHeaderTrailer> FunctionalGroupHeaderTrailerHeaders { get; set; }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ivh.Application.Core.Common.Models.Eob
{
    using Ivh.Common.Base.Utilities.Extensions;

    public class IndividualOrOrganizationalName
    {
        public virtual int IndividualOrOrganizationalNameId { get; set; }
        public virtual string Nm101EntityIdentifierCode { get; set; }
        public virtual string Nm102EntityTypeQualifier { get; set; }
        public virtual string Nm103NameLastOrganizationName { get; set; }
        public virtual string Nm104NameFirst { get; set; }
        public virtual string Nm105NameMiddle { get; set; }
        public virtual string Nm107NameSuffix { get; set; }
        public virtual string Nm108IdentificationCodeQualifier { get; set; }
        public virtual string Nm109IdentificationCode { get; set; }

        public override bool Equals(object obj)
        {
            IndividualOrOrganizationalName other = obj as IndividualOrOrganizationalName;

            if (ReferenceEquals(null, other))
            {
                return false;
            }
            if (ReferenceEquals(this, other))
            {
                return true;
            }

            return 
                   //this.IndividualOrOrganizationalNameId == other.IndividualOrOrganizationalNameId && // ID cannot be used when checking for value equality
                   this.Nm101EntityIdentifierCode == other.Nm101EntityIdentifierCode &&
                   this.Nm102EntityTypeQualifier == other.Nm102EntityTypeQualifier &&
                   this.Nm103NameLastOrganizationName == other.Nm103NameLastOrganizationName &&
                   this.Nm104NameFirst == other.Nm104NameFirst &&
                   this.Nm105NameMiddle == other.Nm105NameMiddle &&
                   this.Nm107NameSuffix == other.Nm107NameSuffix &&
                   this.Nm108IdentificationCodeQualifier == other.Nm108IdentificationCodeQualifier &&
                   this.Nm109IdentificationCode == other.Nm109IdentificationCode;
        }

        public override int GetHashCode()
        {
            unchecked
            {
                int hash = this.GetType().GetHashCode();
                //hash = (hash * 31) ^ this.IndividualOrOrganizationalNameId.GetHashCode(); // ID cannot be used when checking for value equality
                hash = (hash * 31) ^ this.Nm101EntityIdentifierCode.GetHashCodeSafe();
                hash = (hash * 31) ^ this.Nm102EntityTypeQualifier.GetHashCodeSafe();
                hash = (hash * 31) ^ this.Nm103NameLastOrganizationName.GetHashCodeSafe();
                hash = (hash * 31) ^ this.Nm104NameFirst.GetHashCodeSafe();
                hash = (hash * 31) ^ this.Nm105NameMiddle.GetHashCodeSafe();
                hash = (hash * 31) ^ this.Nm107NameSuffix.GetHashCodeSafe();
                hash = (hash * 31) ^ this.Nm108IdentificationCodeQualifier.GetHashCodeSafe();
                hash = (hash * 31) ^ this.Nm109IdentificationCode.GetHashCodeSafe();

                return hash;
            }
        }
    }
}

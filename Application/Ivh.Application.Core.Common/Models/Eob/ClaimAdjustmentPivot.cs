﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ivh.Application.Core.Common.Models.Eob
{
    using Ivh.Common.Base.Utilities.Extensions;

    public class ClaimAdjustmentPivot
    {
        // this class takes the CAS segment and puts each of the (up to six) amounts into its own row
        // that way we can use sum and group by.
        public virtual int ClaimAdjustmentId { get; set; }
        public virtual string ClaimAdjustmentGroupCode { get; set; }
        public virtual string ClaimAdjustmentReasonCode { get; set; }
        public virtual decimal MonetaryAmount { get; set; }
        public virtual decimal Quantity { get; set; }
        // to get the UIDisplay action
        public virtual EobClaimAdjustmentReasonCode EobClaimAdjustmentReasonCode { get; set; }

        // Back references to its parents
        //public virtual ClaimPaymentInformation ClaimPaymentInformation { get; set; }
        //public virtual ServicePaymentInformation ServicePaymentInformation { get; set; }

        public override bool Equals(object obj)
        {
            var other = obj as ClaimAdjustmentPivot;

            if (ReferenceEquals(null, other))
            {
                return false;
            }
            if (ReferenceEquals(this, other))
            {
                return true;
            }

            return 
                   //this.ClaimAdjustmentId == other.ClaimAdjustmentId && // ID cannot be used when checking for value equality
                   this.ClaimAdjustmentGroupCode == other.ClaimAdjustmentGroupCode &&
                   this.ClaimAdjustmentReasonCode == other.ClaimAdjustmentReasonCode &&
                   this.MonetaryAmount == other.MonetaryAmount &&
                   this.Quantity == other.Quantity &&
                   this.EobClaimAdjustmentReasonCode.EqualsSafe(other.EobClaimAdjustmentReasonCode)
                ;
        }

        public override int GetHashCode()
        {
            unchecked
            {
                int hash = this.GetType().GetHashCode();
                //hash = (hash * 31) ^ this.ClaimAdjustmentId.GetHashCode(); // ID cannot be used when checking for value equality
                hash = (hash * 31) ^ this.ClaimAdjustmentGroupCode.GetHashCodeSafe();
                hash = (hash * 31) ^ this.ClaimAdjustmentReasonCode.GetHashCodeSafe();
                hash = (hash * 31) ^ this.MonetaryAmount.GetHashCode();
                hash = (hash * 31) ^ this.Quantity.GetHashCode();
                hash = (hash * 31) ^ this.EobClaimAdjustmentReasonCode.GetHashCodeSafe();

                return hash;
            }
        }


    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ivh.Application.Core.Common.Models.Eob
{
    public class PersonInformation
    {
        public virtual int PersonInformationId { get; set; }

        public virtual string Per01ContactFunctionCode { get; set; }
        public virtual string Per02PersonName { get; set; }
        public virtual string Per03CommunicationNumberQualifer { get; set; }
        public virtual string Per04CommunicationNumber { get; set; }
        public virtual string Per05CommunicationNumberQualifer { get; set; }
        public virtual string Per06CommunicationNumber { get; set; }
        public virtual string Per07CommunicationNumberQualifer { get; set; }
        public virtual string Per08CommunicationNumber { get; set; }
        public virtual string Per09ContactInquiryReference { get; set; }

    }
}

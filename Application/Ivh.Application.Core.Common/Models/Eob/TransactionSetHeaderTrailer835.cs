﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ivh.Application.Core.Common.Models.Eob
{
public class TransactionSetHeaderTrailer835
    {
        public virtual int TransactionSetHeaderTrailerId { get; set; }
        public virtual int PayerIdentificationId { get; set; }

        public virtual int St01TransactionSetIdentifierCode { get; set; }
        public virtual string St02TransactionSetControlNumber { get; set; }

        public virtual string Bpr01TransactionHandleCode { get; set; }
        public virtual decimal Bpr02MonetaryAmount { get; set; }
        public virtual string Bpr03CreditDebitFlagCode { get; set; }
        public virtual string Bpr04PaymentMethodFlagCode { get; set; }
        public virtual string Bpr05PaymentFormatCode { get; set; }
        public virtual string Bpr06EfiIdentificationNumberQualifier { get; set; }
        public virtual string Bpr07DfiIdentificationNumber { get; set; }
        public virtual string Bpr08AccountNumberQualifier { get; set; }
        public virtual string Bpr09AccountNumber { get; set; }
        public virtual string Bpr10OriginatingCompanyIdentifier { get; set; }
        
        private string _bpr11OriginCompaySupplementalCode;
        public virtual string Bpr11OriginCompaySupplementalCode
        {
            get
            {
                if (this._bpr11OriginCompaySupplementalCode?.Length > 9)
                    return this._bpr11OriginCompaySupplementalCode.Substring(0, 9);
                return this._bpr11OriginCompaySupplementalCode;
            }
            set { this._bpr11OriginCompaySupplementalCode = value; }
        }

        public virtual string Bpr12DfiIdentificationNumber { get; set; }
        public virtual string Bpr13AccountNumberQualifier { get; set; }
        public virtual string Bpr14AccountNumber { get; set; }
        public virtual string Bpr15OriginatingCompanyIdentifier { get; set; }
        public virtual DateTime? Bpr16Date { get; set; }

        public virtual string Trn01TraceTypeCode { get; set; }
        public virtual string Trn02ReferenceIdentification { get; set; }
        public virtual string Trn03OriginatingCompanyId { get; set; }
        public virtual string Trn04ReferenceIdentification { get; set; }

        public virtual string Cur01EntityIdentifierCode { get; set; }
        public virtual string Cur02CurrencyCode { get; set; }


        //REF
        //[EdiCondition("EV", Path = "REF/0/0")]
        //public virtual ReferenceInformation Ref1ReceiverIdentification { get; set; }

        //REF
        //[EdiCondition("F2", Path = "REF/0/0")]
        //public virtual ReferenceInformation Ref2VersionIdentification { get; set; }

        // DTM
        public virtual DateTimeReference DtmProductionDate { get; set; }

        // Loop 1000A
        public virtual PayerIdentification PayerDetails { get; set; }

        // Loop 1000B
        public virtual PayeeIdentification PayeeDetails { get; set; }

        // Loops 2000 - 2110
        public virtual IList<HeaderNumber> Transactions { get; set; }

        // PLB
        //[EdiPath("PLB/0/0")]
        //public virtual IList<ProviderLevelAdjustment> PlbProviderAdjustments { get; set; }

        public virtual int Se01MessageSegmentsCount { get; set; }
        public virtual string Se02MessageControlNumber { get; set; }
        // Back reference to its parent
        //public virtual FunctionalGroupHeaderTrailer FunctionalGroupHeaderTrailer { get; set; }

    }
}

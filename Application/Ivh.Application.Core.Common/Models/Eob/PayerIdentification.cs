﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ivh.Application.Core.Common.Models.Eob
{
    public class PayerIdentification
    {
        public virtual int PayerIdentificationId { get; set; }

        public virtual string N101EntityIdentifierCode { get; set; }
        public virtual string N102PayerName { get; set; }
        public virtual string N103IdentificationCodeQualifier { get; set; }
        public virtual string N104IdentificationCode { get; set; }
        public virtual string N105EntityRelationshipCode { get; set; }
        public virtual string N106EntityIdentifierCode { get; set; }

        //public virtual PartyLocation N3PayerAddress { get; set; }
        //public virtual PartyGeographicLocation N4PayerCityStateZip { get; set; }
        //public virtual IList<ReferenceInformation> RefAdditionalPayerIdentification { get; set; }

        public virtual PersonInformation Per1PayerBusinessContactInformation { get; set; }
        public virtual IList<PersonInformation> Per2PayerTechnicalContactInformation { get; set; }
        public virtual PersonInformation Per3PayerWebSite { get; set; }
    }
}

﻿namespace Ivh.Application.Core.Common.Models.Eob
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    public class VisitEobDetailsResultModel
    {

        public VisitEobDetailsResultModel()
        {
            this.ClaimLevelClaimAdjustments = new List<VisitEobClaimAdjustmentModel>();
            this.ServicePaymentInformations = new List<VisitEobServicePaymentInformationModel>();
        }

        public int HeaderNumber { get; set; }
        public string ClaimNumber { get; set; }
        public string PlanNumber { get; set; }
        public string VisitSourceSystemKey { get; set; }
        public int VisitBillingSystemId { get; set; }
        public string PayerName { get; set; }
        public string PayerLinkUrl { get; set; }
        public string PayerIcon { get; set; }
        public string ClaimStatusCode { get; set; }
        public decimal TotalClaimChargeAmount { get; set; }
        public decimal ClaimPaymentAmount { get; set; }
        public decimal PatientResponsibilityAmount { get; set; }

        public DateTime TransactionDate { get; set; }
        public DateTime ServiceStartDate { get; set; }
        public DateTime ServiceEndDate { get; set; }
        public string MiaMoaClaimPaymentRemarkCode { get; set; }
        public string MiaMoaClaimPaymentRemarkDescription { get; set; }

        public IList<VisitEobClaimAdjustmentModel> ClaimLevelClaimAdjustments { get; set; }
        public IList<VisitEobServicePaymentInformationModel> ServicePaymentInformations { get; set; }

        public IList<VisitEobIndustryCodeIdentificationModel> HealthCareRemarkCodes
        {
            get
            {
                IList<VisitEobIndustryCodeIdentificationModel> remarkCodes = this.ServicePaymentInformations.SelectMany(x => x.LqHealthCareRemarkCodesUnique)
                    .GroupBy(x => x.IndustryCode)
                    .Select(x => x.First())
                    .ToList();

                if (this.MiaMoaClaimPaymentRemarkCode != null)
                {
                    remarkCodes.Add(new VisitEobIndustryCodeIdentificationModel
                    {
                        IndustryCode = this.MiaMoaClaimPaymentRemarkCode,
                        Description = this.MiaMoaClaimPaymentRemarkDescription
                    });
                }
                return remarkCodes.OrderBy(x => x.IndustryCode).ToList();
            }
        }

        public IList<VisitEobClaimAdjustmentModel> ClaimLevelClaimAdjustmentsSummed
        {
            get
            {
                return this.ClaimLevelClaimAdjustments
                    .GroupBy(x => x.UIDisplay)
                    .Select(g => new VisitEobClaimAdjustmentModel { UIDisplay = g.Key, MonetaryAmount = g.Sum(e => e.MonetaryAmount) })
                    .ToList();
            }
        }

        public IList<VisitEobClaimAdjustmentModel> ServiceLevelClaimAdjustmentsSummed
        {
            get
            {
                return this.ServicePaymentInformations.SelectMany(x => x.ClaimAdjustmentsSummed)
                 .GroupBy(x => x.UIDisplay)
                 .Select(g => new VisitEobClaimAdjustmentModel()
                 {
                     UIDisplay = g.Key,
                     MonetaryAmount = g.Sum(a => a.MonetaryAmount),
                 }).ToList();
            }
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ivh.Application.Core.Common.Models.Eob
{
    public class HeaderNumber
    {
        public virtual int HeaderNumberId { get; set; }
        public virtual int TransactionSetHeaderTrailerId { get; set; }

        public virtual int Lx01AssignedNumber { get; set; }

        // TS3
        //public virtual TransactionStatistics Ts3ProviderSummaryInformation { get; set; }

        // TS2
        //public virtual TransactionSupplementalStatistics Ts2ProviderSupplementalSummaryInformation { get; set; }

        // Loops 2100-2110
        public virtual IList<ClaimPaymentInformation> ClaimPaymentInformations { get; set; }

        //public virtual TransactionSetHeaderTrailer835 TransactionSetHeaderTrailer { get; set; }
    }
}

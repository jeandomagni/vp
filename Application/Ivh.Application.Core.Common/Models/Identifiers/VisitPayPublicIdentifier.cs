﻿namespace Ivh.Application.Core.Common.Models.Identifiers
{
    using Ivh.Application.Core.Common.Interfaces;

    public abstract class VisitPayPublicIdentifier<T>
    {
        public virtual T RawValue { get; }
        public virtual IIdentifierValueSource<T> ValueGenerator { get; }
    }
}
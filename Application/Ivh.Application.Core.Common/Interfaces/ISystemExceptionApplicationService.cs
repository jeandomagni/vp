﻿namespace Ivh.Application.Core.Common.Interfaces
{
    using System;
    using Ivh.Common.Base.Interfaces;
    using Ivh.Common.JobRunner.Common.Interfaces;
    using Ivh.Common.ServiceBus.Interfaces;
    using Ivh.Common.VisitPay.Jobs;
    using Ivh.Common.VisitPay.Messages.Core;

    public interface ISystemExceptionApplicationService : IApplicationService,
        IUniversalConsumerService<SystemExceptionMessage>,
        IJobRunnerService<QueueSystemExceptionLoggingJobRunner>
    {
        void LogSystemExceptions(DateTime exceptionsAsOfDateTime);
        void LogSystemException(SystemExceptionMessage systemExceptionMessage);
        void LogSystemEnrollmentException(int vpGuarantorId, string message);
    }
}

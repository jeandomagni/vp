﻿
namespace Ivh.Application.Core.Common.Interfaces
{
    using System;
    using System.Collections.Generic;
    using Dtos;
    using Ivh.Common.Base.Interfaces;

    public interface IChatApplicationService : IApplicationService
    {
        int GetGuarantorIdFromToken(Guid guarantorChatToken);

        void ConnectToChatThread(int clientVpUserId, int vpGuarantorId, string chatThreadId);

        void DisconnectFromChatThread(int clientVpUserId, int vpGuarantorId, string chatThreadId);

        void EndChatThread(int clientVpUserId, int vpGuarantorId, string chatThreadId);

        bool IsChatAvailable();

        IList<ChatAvailabilityDto> GetChatAvailabilities();

        void EditChatAvailability(ChatAvailabilityDto chatAvailability);

        void DeleteChatAvailability(int chatAvailabilityId);
    }
}

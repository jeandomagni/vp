﻿namespace Ivh.Application.Core.Common.Interfaces
{
    using System;
    using System.Threading.Tasks;
    using Ivh.Common.Base.Interfaces;
    using Microsoft.Owin.Security.Cookies;

    public interface ISecurityStampValidatorApplicationService : IApplicationService
    {
        Func<CookieValidateIdentityContext, Task> OnValidateIdentity(TimeSpan validateInterval);
    }
}
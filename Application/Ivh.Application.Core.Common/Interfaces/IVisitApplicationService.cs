﻿namespace Ivh.Application.Core.Common.Interfaces
{
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Dtos;
    using Ivh.Common.Base.Interfaces;
    using Ivh.Common.ServiceBus.Interfaces;
    using Ivh.Common.VisitPay.Enums;
    using Ivh.Common.VisitPay.Messages.Aging;
    using Ivh.Common.VisitPay.Messages.Core;

    public interface IVisitApplicationService : IApplicationService,
        IUniversalConsumerService<VisitBalanceChangedMessage>,
        IUniversalConsumerService<VisitBalanceChangedMessageList>,
        IUniversalConsumerService<VisitChangedStatusMessage>,
        IUniversalConsumerService<VisitChangedStatusAggregatedMessages>,
        IUniversalConsumerService<VisitAgeChangedMessage>,
        IUniversalConsumerService<VisitAgeChangedMessageHighPriority>
    {
        IReadOnlyList<VisitStateEnum> GetVisitStates();
        IReadOnlyList<VisitStateDerivedEnum> GetDerivedVisitStates();
        VisitDto GetVisit(int visitPayUserId, int visitId, int? vpGuarantorId = null);
        VisitDto GetVisit(int billingSystemId, string sourceSystemKey);
        bool HasVisits(int vpGuarantorId, IEnumerable<VisitStateEnum> visitStates);
        bool HasVisitsWithFilter(int vpGuarantorId, VisitFilterDto filterDto);
        VisitResultsDto GetVisits(int visitPayUserId, VisitFilterDto filterDto, int pageNumber, int? pageSize, int? vpGuarantorId = null);
        int GetVisitTotals(int visitPayUserId, VisitFilterDto filterDto);
        IList<VisitDto> GetAllActiveNonFinancedVisits(int vpGuarantorId);
        void SetIsPastDue(IList<VisitDto> visitDtos);
        VisitDto UpdateVisitAgeTier(string visitSourceSystemKey, int visitBillingSystemId, AgingTierEnum agingTier, string agingMessage, int? actionVisitPayUserId = null);
        void SetVisitAgingCount(VisitAgingHistoryDto visitAgingHistoryDto);
        Task<byte[]> ExportVisitsAsync(int visitPayUserId, VisitFilterDto filterDto, string userName, int? vpGuarantorId = null);
        Task<byte[]> ExportVisitsClientAsync(int visitPayUserId, VisitFilterDto filterDto, string userName);
        int? GetRicCmsRegionId(int vpGuarantorId);
        CmsRegionEnum GetRicCmsRegionEnum(int vpGuarantorId);
        IList<FacilityDto> GetUniqueFacilityVisits(int vpGuarantorId);
    }
}

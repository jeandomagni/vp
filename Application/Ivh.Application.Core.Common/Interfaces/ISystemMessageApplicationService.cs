﻿namespace Ivh.Application.Core.Common.Interfaces
{
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Dtos;
    using Ivh.Common.Base.Interfaces;
    using Ivh.Common.ServiceBus.Interfaces;
    using Ivh.Common.VisitPay.Enums;
    using Ivh.Common.VisitPay.Messages.Core;

    public interface ISystemMessageApplicationService : IApplicationService,
        IUniversalConsumerService<AddSystemMessageVisitPayUserByGuarantorMessage>,
        IUniversalConsumerService<AddSystemMessageVisitPayUserByGuarantorMessageList>,
        IUniversalConsumerService<AddSystemMessageVisitPayUserMessage>,
        IUniversalConsumerService<AddSystemMessageVisitPayUserMessageList>

    {
        Task<IList<SystemMessageDto>> GetMessages(int visitPayUserId, IDictionary<string, string> replacementValues);
        Task<int> GetUnreadMessageCount(int visitPayUserId, IDictionary<string, string> replacementValues);
        void DismissMessage(int visitPayUserId, int systemMessageId);
        void HideMessage(int visitPayUserId, int systemMessageId);
        void MarkMessageAsRead(int visitPayUserId, int systemMessageId);
        Task MarkAllMessagesAsRead(int visitPayUserId);
        void InsertSystemMessageVisitPayUser(int visitPayUserId, SystemMessageEnum systemMessageEnum);
        void InsertSystemMessageVisitPayUserByGuarantorId(int vpGuarantorId, SystemMessageEnum systemMessageEnum);
        void InsertSystemMessageVisitPayUsers(IList<AddSystemMessageVisitPayUserMessage> messages);
        void InsertSystemMessageVisitPayUsers(IList<AddSystemMessageVisitPayUserByGuarantorMessage> messages);
    }
}
﻿namespace Ivh.Application.Core.Common.Interfaces
{
    using System.Collections.Generic;
    using Dtos;
    using Ivh.Common.Base.Interfaces;

    public interface IVisitItemizationStorageApplicationService : IApplicationService
    {
        VisitItemizationStorageResultsDto GetItemizations(int vpGuarantorId, VisitItemizationStorageFilterDto filterDto, int pageNumber, int pageSize);
        int GetItemizationTotals(int vpGuarantorId, VisitItemizationStorageFilterDto filterDto);
        VisitItemizationStorageDto GetItemization(int vpGuarantorId, int visitFileStoredId, int currentVisitPayUserId);
        IDictionary<string, string> GetAllFacilities(int vpGuarantorId, int currentVisitPayUserId);
        IList<string> GetAllPatientNames(int vpGuarantorId, int currentVisitPayUserId);
    }
}
﻿namespace Ivh.Application.Core.Common.Interfaces
{
    using Dtos;
    using Ivh.Common.Base.Interfaces;

    public interface IVisitStateHistoryApplicationService : IApplicationService
    {
        VisitStateHistoryResultsDto GetAllVisitStateHistory(int visitPayUserId, VisitStateHistoryFilterDto filterDto, int pageNumber, int pageSize, bool onlyChanges = false);
    }
        
}

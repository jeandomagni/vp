﻿namespace Ivh.Application.Core.Common.Interfaces
{
    using System;
    using System.Collections.Generic;
    using System.Web;
    using Base.Common.Dtos;
    using Dtos;
    using Ivh.Common.Base.Enums;
    using Ivh.Common.Base.Interfaces;
    using Ivh.Common.EventJournal;
    using Ivh.Common.VisitPay.Enums;
    using User.Common.Dtos;

    public interface IVisitPayUserJournalEventApplicationService : IApplicationService
    {
        void LogClientSupportRequestView(int visitPayUserId, int vpGuarantorId, int supportRequestId, JournalEventHttpContextDto context);
        void LogStatementDownload(int guarantorVisitPayUserId, int? clientVisitPayUserId, bool isSummary, JournalEventHttpContextDto context);
        void LogPaymentSummaryDownload(int visitPayUserId, JournalEventHttpContextDto context);
        void LogSmsCategoryChanged(int targetVisitPayUserId, int visitPayUserId, int vpGuarantorId, string category, string changeValue, JournalEventHttpContextDto context);
        void LogViewAuditLogList(int visitPayUserId, JournalEventHttpContextDto context, JournalEventTypeEnum? journalEventType, DateTime? beginDate, DateTime? endDate, string userName, int? vpGuarantor);
        void LogViewAuditLogDetail(int visitPayUserId, JournalEventHttpContextDto context, int eventJournalId);
        void LogClientSetBalanceTransferStatus(int vpGuarantorId, int visitPayUserId, int clientVisitPayUserId, int? financePlanId, int? financePlanCreatorId, int visitId, decimal? visitPrincipalBalance, string notes, JournalEventHttpContextDto context);
        void LogClientRemovedBalanceTransferStatus(int vpGuarantorId, int visitPayUserId, int clientVisitPayUserId, int? financePlanId, int? financePlanCreatorId, int visitId, decimal? visitPrincipalBalance, string notes, JournalEventHttpContextDto context);
        void LogSsoMatchedNotRegistered(SsoProviderEnum ssoProvider, IList<int> matchedNotRegisteredHsGuarantors, string ssoSsk, DateTime? dob, JournalEventHttpContextDto context);
        void LogSsoRequestFailed(int? actionTakenByVisitPayUserId, SsoProviderEnum ssoProvider, string ssoSsk, DateTime? dob, JournalEventHttpContextDto context);
        void LogSsoEnabled(int? actionTakenByVisitPayUserId, int vpGuarantorId, SsoProviderEnum ssoProvider, JournalEventHttpContextDto context, string ssoSsk, DateTime? dob);
        void LogSsoDecline(int? actionTakenByVisitPayUserId, int vpGuarantorId, SsoProviderEnum ssoProvider, JournalEventHttpContextDto context, string ssoSsk, DateTime? dob);
        void LogConsolidationGuarantorRequestManaging(int visitPayUserId, int vpManagedGuarantorId, JournalEventHttpContextDto context);
        void LogConsolidationGuarantorRequestManaged(int visitPayUserId, int vpManagingGuarantorId, JournalEventHttpContextDto context);
        void LogGuarantorConsolidationRequestAccepted(int visitPayUserId, int vpGuarantorId, int consolidationGuarantorId, JournalEventHttpContextDto context);
        void LogGuarantorConsolidationRequestRejected(int visitPayUserId, int vpGuarantorId, int consolidationGuarantorId, JournalEventHttpContextDto context);
        void LogGuarantorConsolidationRequestCancelled(int visitPayUserId, int vpGuarantorId, int consolidationGuarantorId, JournalEventHttpContextDto context);
        void LogGuarantorAddedPaymentMethod(int visitPayUserId, int vpGuarantorId, string vpGuarantorUserName, bool isPrimary, string billingId, JournalEventHttpContextDto context);
        void LogViewManaged(int visitPayUserId, int vpGuarantorId, int? emulateClientUserId, JournalEventHttpContextDto context);
        void LogBillingIdFailure(int visitPayUserId, int vpGuarantorId, IReadOnlyDictionary<string, string> response, JournalEventHttpContextDto context);
        void LogRegistrationAuthentication(bool success, string guarantorId, string lastName, DateTime dateOfBirth, string ssn4, string zip, JournalEventHttpContextDto context);
        void LogRegistrationComplete(int visitPayUserId, int vpGuarantorId, string hsGuarantorId, string lastName, DateTime dateOfBirth, string ssn4, string zip, JournalEventHttpContextDto context);
        void LogClientAddHold(int visitPayUserId, int vpGuarantorId, int visitId, JournalEventHttpContextDto context);
        void LogClientClearHold(int visitPayUserId, int vpGuarantorId, int visitId, JournalEventHttpContextDto context);
        void LogClientChangedFinancePlanBucket(int clientVisitPayUserId, int vpGuarantorId, int financePlanId, int previousBucket, int newBucket, JournalEventHttpContextDto context);
        void LogClientChangedVisitAgingTier(int clientVisitPayUserId, int vpGuarantorId, int visitId, AgingTierEnum previousAgingTier, AgingTierEnum newAgingTier, string reason, JournalEventHttpContextDto context);
        void LogClientRequestPassword(int targetClientVisitPayUserId, int clientVisitPayUserId, JournalEventHttpContextDto context);
        void LogSsoSuccessfulLogin(int? actionTakenByVisitPayUserId, int vpGuarantorId, SsoProviderEnum ssoProvider, string ssoSsk, DateTime? dob, JournalEventHttpContextDto context);
        void LogSsoVerificationFailed(int? actionTakenByVisitPayUserId, int vpGuarantorId, SsoProviderEnum ssoProvider, string ssoSsk, DateTime? dob, JournalEventHttpContextDto context);
        void LogSsoAttempt(int? actionTakenByVisitPayUserId, int vpGuarantorId, SsoProviderEnum ssoProvider, JournalEventHttpContextDto context);
        void LogSsoDisabled(int? actionTakenByVisitPayUserId, int vpGuarantorId, SsoProviderEnum ssoProvider, JournalEventHttpContextDto context, string ssoSsk, DateTime? dob);
        void LogSsoInvalidate(int? actionTakenByVisitPayUserId, int vpGuarantorId, SsoProviderEnum ssoProvider, JournalEventHttpContextDto context);
        void LogRegistrationCompleteSso(SsoProviderEnum ssoProviderEnum, int visitPayUserId, int vpGuarantorId, string hsGuarantorId, string lastName, DateTime dateOfBirth, string ssn4, string zip, JournalEventHttpContextDto context);
        void LogClientAccountModify(int visitPayUserId, int vpGuarantorId, JournalEventHttpContextDto context);
        void LogClientAccountSearch(int visitPayUserId, IList<int> vpGuarantorIds, JournalEventHttpContextDto context);
        void LogClientAccountView(int visitPayUserId, int vpGuarantorId, JournalEventHttpContextDto context);
        void LogClientUserModifyClientAccount(int targetClientVisitPayUserId, int clientVisitPayUserId, IList<string> assignedRoles, JournalEventHttpContextDto context);
        void LogClientUserCreateClientAccount(int targetClientVisitPayUserId, int clientVisitPayUserId, IList<string> assignedRoles, JournalEventHttpContextDto context);
        void LogClientAddedPaymentMethod(int actionVisitPayUserId, int vpGuarantorId, string vpGuarantorUserName, bool isPrimary, string billingId, JournalEventHttpContextDto context);
        void LogViewSecurityQuestionsEvent(int clientVisitPayUserId, int vpGuarantorId, JournalEventHttpContextDto context);
        void LogClientViewManaged(int visitPayUserId, int managedVpGuarantorId, int managingVpGuarantorId, JournalEventHttpContextDto context);
        void LogDeleteAttachmentSupportRequest(int clientVisitPayUserId, int createdSupportRequestvpGuarantorId, int createdSupportRequestVisitPayUserId, int supportRequestId, JournalEventHttpContextDto context);
        void LogDeleteAttachmentSupportTicket(int clientVisitPayUserId, int createdSupportTicketVisitPayUserId, int clientSupportRequestId, JournalEventHttpContextDto context);
        void LogClientAccountSearchHsGuarantors(int visitPayUserId, IList<int> hsGuarantorIds, JournalEventHttpContextDto context);
        void LogVisitPayReportRun(int clientVisitPayUserId, JournalEventHttpContextDto context, string reportName);
        void LogGuestPayAuthenticationFailure(string attemptedGuarantorSourceSystemKey, string attemptedGuarantorLastName, JournalEventHttpContextDto context);
        void LogGuestPayAuthenticationSuccess(int paymentUnitAuthenticationId, string paymentUnitSourceSystemKey, string guarantorLastName, JournalEventHttpContextDto context);
        void LogGuestPayPaymentMethodFailure(int paymentUnitAuthenticationId, string paymentUnitSourceSystemKey, string guarantorLastName, JournalEventHttpContextDto context, IReadOnlyDictionary<string, string> processorResponse);
        void LogGuestPayReceiptDownload(int paymentUnitAuthenticationId, string paymentUnitSourceSystemKey, string guarantorLastName, JournalEventHttpContextDto context);
        void LogGuestPaySessionTimeout(int paymentUnitAuthenticationId, string paymentUniSourceSystemKey, JournalEventHttpContextDto context);
        void LogGuarantorSelfVerification(int visitPayUserId, int vpGuarantorId, JournalEventHttpContextDto context);
        void LogClientRefundedPayment(int clientVisitPayUserId, int vpGuarantorId, int paymentId, int paymentType, JournalEventHttpContextDto context);
        void LogCardReaderKeyAdded(int clientVisitPayUserId, string deviceKey, JournalEventHttpContextDto context);
        void LogCardReaderAccountAdded(int clientVisitPayUserId, string cardReaderKey, JournalEventHttpContextDto context);
        void LogClientChangedPrimaryPaymentMethod(int clientVisitPayUserId, int vpGuarantorId, string vpGuarantorUserName, string billingId, JournalEventHttpContextDto context);
        void LogGuarantorChangedPrimaryPaymentMethod(int visitPayUserId, int vpGuarantorId, string vpGuarantorUserName, string billingId, JournalEventHttpContextDto context);
    }
}

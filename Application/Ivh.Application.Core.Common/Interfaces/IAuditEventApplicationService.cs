﻿namespace Ivh.Application.Core.Common.Interfaces
{
    using Ivh.Common.Base.Interfaces;
    using Ivh.Common.ServiceBus.Interfaces;
    using Ivh.Common.VisitPay.Messages.Core;

    public interface IAuditEventApplicationService : IApplicationService,
        IUniversalConsumerService<AuditEventFinancePlanMessage>
    {
        void LogAuditEventFinancePlan(AuditEventFinancePlanMessage message);
    }
}

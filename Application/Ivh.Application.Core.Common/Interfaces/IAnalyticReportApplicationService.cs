﻿namespace Ivh.Application.Core.Common.Interfaces
{
    using System.Collections.Generic;
    using Dtos;
    using Ivh.Common.Base.Interfaces;
    using Microsoft.PowerBI.Api.V2.Models;
    using Microsoft.Rest;

    public interface IAnalyticReportApplicationService : IApplicationService
    {
        IList<AnalyticReportDto> FilterAnalyticReportList(List<AnalyticReportDto> analyticReports);
        TokenCredentials ValidateToken(EmbedToken embedToken, TokenCredentials tokenCredentials);
        IList<AnalyticReportDto> GetAnalyticReportList();
    }
}
﻿using Ivh.Application.Core.Common.Dtos;

namespace Ivh.Application.Core.Common.Interfaces
{
    using Ivh.Common.Base.Interfaces;

    public interface IClientApplicationService : IApplicationService
    {
        ClientDto GetClient();
    }
}
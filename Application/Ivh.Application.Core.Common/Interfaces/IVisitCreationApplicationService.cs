﻿namespace Ivh.Application.Core.Common.Interfaces
{
    using System.Collections.Generic;
    using Ivh.Common.Base.Interfaces;
    using Ivh.Common.VisitPay.Messages.HospitalData;

    public interface IVisitCreationApplicationService : IApplicationService
    {
        IList<int> MergeOrCreateVisitsFromVisitChangeSet(ChangeSetMessage visitChangeSetDataChangedMessage);
    }
}
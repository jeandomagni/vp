﻿namespace Ivh.Application.Core.Common.Interfaces
{
    using System.Collections.Generic;
    using Dtos;
    using Ivh.Common.Base.Interfaces;

    public interface IFacilityApplicationService : IApplicationService
    {
        IReadOnlyList<FacilityDto> GetAllFacilities();
        IReadOnlyList<FacilityDto> GetFacilitiesByStateCode(string stateCode);
    }
}
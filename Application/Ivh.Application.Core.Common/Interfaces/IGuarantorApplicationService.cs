﻿namespace Ivh.Application.Core.Common.Interfaces
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Base.Common.Dtos;
    using Dtos;
    using Ivh.Common.Base.Interfaces;
    using Ivh.Common.EventJournal;
    using Ivh.Common.ServiceBus.Interfaces;
    using Ivh.Common.VisitPay.Enums;
    using Ivh.Common.VisitPay.Messages.Core;
    using Ivh.Common.VisitPay.Messages.Matching;
    using Ivh.Common.VisitPay.Messages.Matching.Dtos;
    using User.Common.Dtos;

    public interface IGuarantorApplicationService :
        IApplicationService,
        IUniversalConsumerService<ReactivateGuarantorMessage>,
        IUniversalConsumerService<AddInitialMatchesToCdiMessage>,
        IUniversalConsumerService<NewGuarantorMatchCreatedMessage>,
        IUniversalConsumerService<NewGuarantorMatchCreatedMessageList>
    {
        GuarantorMatchResultEnum IsGuarantorMatch(MatchDataDto matchData);
        bool CredentialsMatchCanceledAccount(MatchDataDto matchData);
        GuarantorDto GetGuarantor(int vpGuarantorId);
        GuarantorDto GetGuarantor(string visitPayUserName);
        int GetGuarantorId(int visitPayUserId);
        GuarantorResultsDto GetGuarantors(GuarantorFilterDto filterDto, int pageNumber, int pageSize);
        void SendGuarantorInvitation(GuarantorInvitationDto guarantorInvitationDto);
        GuarantorInvitationDto GetGuarantorInvitation(Guid invitationToken);
        DataResult<string, bool> CancelAccount(int guarantorUserId, int clientUserId, int reasonId, string note);
        DataResult<string, bool> ReactivateAccount(int guarantorUserId, int clientUserId, DateTime reactivateAccountOn, JournalEventHttpContextDto context);
        void ReactivateAccount(int vpGuarantorId);
        DataResult<string, bool> ChangePaymentDueDay(int currentVisitPayUserId, int dueDay, bool isFinancePlanCreatedChange, int actionVisitPayUserId);
        IList<GuarantorPaymentDueDayHistoryDto> GetGuarantorPaymentDueDayHistory(int guarantorUserId);
        void ResolveMatches(int guarantorId);
        Task<HealthEquityBalanceDto> GetGuarantorHealthEquityBalanceAsnyc(int guarantorId, int visitpayUserId, SsoProviderEnum ssoProviderEnum, HttpContextDto context = null);
        Task<HealthEquitySsoStatusDto> GetGuarantorHealthEquitySsoStatusAsnyc(int guarantorId, int visitpayUserId);
        void ConvertVpccUserToVisitPayUser(int visitPayUserId);
        void SetVpccPendingMatchesToMatched(int vpGuarantorId);
        void VpccUserDeclinedFinancePlan(int visitPayUserId, int vpGuarantorId);
        IList<GuarantorCancellationReasonDto> GetAllCancellationReasons();
        int GetVpGuarantorId(int visitPayUserId);
        bool SetGuarantorAutoPay(int vpGuarantorId, bool isAutoPay);
        string GetSsn(int vpGuarantorId);
    }
}

﻿namespace Ivh.Application.Core.Common.Interfaces
{
    using System.Collections.Generic;
    using Dtos;

    public interface IVanityUrlApplicationService
    {
        VanityUrlDto GetVanityUrl(string vanityUrlKeyName);
    }
}
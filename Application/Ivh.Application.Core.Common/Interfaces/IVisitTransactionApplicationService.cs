﻿namespace Ivh.Application.Core.Common.Interfaces
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Dtos;
    using Ivh.Common.Base.Interfaces;

    public interface IVisitTransactionApplicationService : IApplicationService
    {
        IReadOnlyList<string> GetTransactionTypes();
        VisitTransactionResultsDto GetTransactions(int visitPayUserId, VisitTransactionFilterDto filterDto, int pageNumber, int pageSize);
        VisitTransactionSummaryDto GetTransactionSummary(int visitPayUserId, int visitId, DateTime? startDate = null, DateTime? endDate = null);
        PaymentSummarySearchResultsDto GetPaymentSummary(int vpGuarantorId, PaymentSummaryFilterDto filterDto, int pageNumber, int pageSize);
        int GetTransactionTotals(int visitPayUserId, VisitTransactionFilterDto filterDto);
        Task<byte[]> ExportTransactionsClientAsync(int visitPayUserId, VisitTransactionFilterDto filterDto, string userName);
        Task<byte[]> ExportVisitTransactionsAsync(int visitPayUserId, VisitTransactionFilterDto filterDto, string userName);
        Task<byte[]> ExportVisitTransactionsClientAsync(int visitPayUserId, VisitTransactionFilterDto filterDto, string userName);
        Task<byte[]> ExportPaymentSummaryAsync(int vpGuarantorId, PaymentSummaryFilterDto filterDto, string userName);
    }
}

﻿namespace Ivh.Application.Core.Common.Interfaces
{
    using System;
    using System.Collections.Generic;
    using Dtos;
    using Ivh.Common.Base.Interfaces;
    using Ivh.Common.EventJournal;

    public interface IAuditApplicationService : IApplicationService
    {
        void LogViewPciLog(int visitPayUserId, JournalEventHttpContextDto context); 
        IList<AuditUserAccessDto> GetUserAccessLogsSince(AuditFilterDto auditFilter);
        IList<AuditUserChangeDto> GetUserChangeLogsSince(AuditFilterDto auditFilter);
        IList<AuditUserChangeDetailDto> GetUserChangeDetailLogsSince(AuditFilterDto auditFilter);
        IList<AuditRoleChangeDto> GetRoleChangeLogsSince(AuditFilterDto auditFilter);
        byte[] ExportUserAccessLogs(AuditFilterDto auditFilter);
        byte[] ExportUserChangeLogs(AuditFilterDto auditFilter);
        byte[] ExportUserChangeDetailLogs(AuditFilterDto auditFilter);
        byte[] ExportRoleChangeLogs(AuditFilterDto auditFilter);
        void LogConfirmAudit(int visitPayUserId, bool hasIvhPciAuditorRole, bool hasClientPciAuditorRole, DateTime beginDate, DateTime endDate, string notes, JournalEventHttpContextDto context);
        DateTime? LastIvhPciAudit();
        DateTime? LastClientPciAudit();
        DateTime? LastIvhUserAudit();
        DateTime? LastClientUserAudit();
        IList<AuditLogDto> GetAuditLogs(AuditLogFilterDto auditLogFilter);
        byte[] ExportAuditLogs(AuditLogFilterDto auditLogFilter);
        void LogUserManagementAudit(int visitPayUserId, bool hasIvhUserAuditorRole, bool hasClientUserAuditorRole, string notes, JournalEventHttpContextDto context);
    }
}
﻿namespace Ivh.Application.Core.Common.Interfaces
{
    using System.Collections.Generic;
    using Dtos;
    using Ivh.Common.Base.Interfaces;

    public interface IRegistrationMatchStringApplicationService : IApplicationService
    {
        IReadOnlyList<HsGuarantorDto> TransformRegistrationMatchString(IEnumerable<HsGuarantorDto> hsGuarantors);
    }
}
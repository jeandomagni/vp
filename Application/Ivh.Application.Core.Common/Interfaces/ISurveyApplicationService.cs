﻿namespace Ivh.Application.Core.Common.Interfaces
{
    using System.Threading.Tasks;
    using Dtos;
    using Ivh.Common.Base.Interfaces;

    public interface ISurveyApplicationService : IApplicationService
    {
        SurveyPresentationInfoDto ShouldShowSurveyToVisitPayUser(int visitPayUserId, string surveyName, bool userHasBeenSurveyedInSession = false);
        Task<SurveyDto> GetSurvey(string surveyName);

        void ProcessSurveyResult(SurveyResultDto surveyResult);
    }
}

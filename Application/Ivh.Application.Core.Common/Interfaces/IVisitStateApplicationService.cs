﻿namespace Ivh.Application.Core.Common.Interfaces
{
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Ivh.Common.Base.Enums;
    using Ivh.Common.Base.Interfaces;
    using Ivh.Common.ServiceBus.Interfaces;
    using Ivh.Common.VisitPay.Messages.HospitalData;

    public interface IVisitStateApplicationService : IApplicationService,
        IUniversalConsumerService<HsVisitVpEligibleMessage>
    {
        void SetVisitIneligible(string systemSourceKey, int billingSystemId, string ineligibleReason);
        void SetVisitEligible(string systemSourceKey, int billingSystemId);
    }
}
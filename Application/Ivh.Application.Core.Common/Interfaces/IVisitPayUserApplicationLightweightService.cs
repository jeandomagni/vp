﻿namespace Ivh.Application.Core.Common.Interfaces
{
    using Ivh.Common.Base.Interfaces;
    using Ivh.Common.ServiceBus.Interfaces;
    using Ivh.Common.VisitPay.Messages.Core;
    using Microsoft.Owin;

    public interface IVisitPayUserApplicationLightweightService : IApplicationService,
        IUniversalConsumerService<VisitPayUserAddressChangedMessage>
    {
        void SignOutVisitPayUserId(int visitPayUserId);
        void CheckIfVisitPayUserShouldSignOut(IOwinContext owinContext);
        void VisitPayUserAddressChanged(int visitPayUserId);
    }
}

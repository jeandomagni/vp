﻿namespace Ivh.Application.Core.Common.Interfaces
{
    using Ivh.Common.Base.Interfaces;
    using System.Threading.Tasks;

    public interface IThirdPartyKeyApplicationService : IApplicationService
    {
        bool IsCardReaderDeviceKeyAllowed(string cardReaderDeviceKey);
        bool IsCardReaderUserAccountAllowed(string cardReaderUserAccount);
    }
}

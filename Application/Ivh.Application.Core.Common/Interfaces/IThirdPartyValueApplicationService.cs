﻿namespace Ivh.Application.Core.Common.Interfaces
{
    using Ivh.Common.Base.Interfaces;
    using Ivh.Common.EventJournal;

    public interface IThirdPartyValueApplicationService : IApplicationService
    {
        string GetCardReaderDeviceValue(int visitPayUserId);
        string GetCardReaderUserAccountValue(int visitPayUserId);
        void SetCardReaderDeviceKey(string value, int visitPayUserId, JournalEventHttpContextDto context);
        void SetCardReaderUserAccount(string value, int visitPayUserId, JournalEventHttpContextDto context);
        bool CardDeviceKeyNeedsToBeSet(int visitPayUserId, bool isFeatureEnabled, bool userHasRole);
    }
}

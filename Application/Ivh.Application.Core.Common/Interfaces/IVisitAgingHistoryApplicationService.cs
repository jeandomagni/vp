﻿namespace Ivh.Application.Core.Common.Interfaces
{
    using Dtos;
    using Ivh.Common.Base.Interfaces;

    public interface IVisitAgingHistoryApplicationService : IApplicationService
    {
        VisitAgingHistoryResultsDto GetAllVisitAgingHistory(int visitPayUserId, VisitAgingHistoryFilterDto filterDto, int pageNumber, int pageSize);
    }
        
}

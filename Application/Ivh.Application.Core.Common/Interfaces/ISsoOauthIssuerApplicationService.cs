namespace Ivh.Application.Core.Common.Interfaces
{
    using Dtos;
    using Ivh.Common.Base.Interfaces;
    using User.Common.Dtos;

    public interface ISsoOauthIssuerApplicationService : IApplicationService
    {
        SsoVisitPayUserOauthTokenDto GetRefreshTokenWithRefreshTokenString(string refreshTokenString);
        SsoVisitPayUserOauthTokenDto GetAccessTokenWithTokenString(string tokenString);
        SsoVisitPayUserOauthTokenDto CreateAccessToken(VisitPayUserDto user, string ticket);
        SsoVisitPayUserOauthTokenDto CreateRefreshToken(VisitPayUserDto user, string ticket);
    }
}
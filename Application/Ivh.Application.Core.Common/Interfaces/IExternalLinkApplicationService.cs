﻿namespace Ivh.Application.Core.Common.Interfaces
{
    using System.Collections.Generic;
    using Dtos;
    using Ivh.Common.Base.Interfaces;

    public interface IExternalLinkApplicationService : IApplicationService
    {
        IList<ExternalLinkDto> GetExternalLinksForActiveVisitInsurancePlans(int vpGuarantorId);
    }
}
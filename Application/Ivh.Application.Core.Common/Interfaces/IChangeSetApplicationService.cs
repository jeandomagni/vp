﻿namespace Ivh.Application.Core.Common.Interfaces
{
    using Ivh.Common.ServiceBus.Interfaces;
    using Ivh.Common.VisitPay.Messages.HospitalData;
    using IApplicationService = Ivh.Common.Base.Interfaces.IApplicationService;

    public interface IChangeSetApplicationService : IApplicationService, 
        IUniversalConsumerService<ChangeSetMessage>, 
        IUniversalConsumerService<ChangeSetAggregatedMessage>
    {
       
    }
}
﻿using System.Collections.Generic;
using Ivh.Application.Core.Common.Dtos;

namespace Ivh.Application.Core.Common.Interfaces
{
    using Ivh.Common.Base.Interfaces;
    using User.Common.Dtos;

    public interface ISecurityQuestionApplicationService : IApplicationService
    {
        IList<SecurityQuestionDto> GetAllSecurityQuestions();

        IList<SecurityQuestionAnswerDto> GetUserSecurityQuestionAnswer(int visitPayUserId);
    }
}
﻿namespace Ivh.Application.Core.Common.Interfaces
{
    using Ivh.Common.Base.Enums;
    using Ivh.Common.Base.Interfaces;
    using Ivh.Common.VisitPay.Enums;

    public interface IFeatureApplicationService : IApplicationService
    {
        bool IsFeatureEnabled(VisitPayFeatureEnum visitPayFeature);
    }
}
﻿namespace Ivh.Application.Core.Common.Interfaces
{
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Dtos;
    using Ivh.Common.Base.Enums;
    using Ivh.Common.Base.Interfaces;
    using Ivh.Common.VisitPay.Enums;

    public interface IKnowledgeBaseApplicationService : IApplicationService
    {
        Task<IList<KnowledgeBaseCategoryDto>> GetKnowledgeBaseCategoriesAsync(KnowledgeBaseApplicationTypeEnum applicationType);
        Task<IList<KnowledgeBaseCategoryQuestionsDto>> GetKnowledgeBaseCategoryQuestionsFlattenedAsync(KnowledgeBaseApplicationTypeEnum applicationType);
        Task<IList<KnowledgeBaseCategoryQuestionAnswersDto>> GetKnowledgeBaseCategoryQuestionAnswersFlattenedAsync(KnowledgeBaseApplicationTypeEnum applicationType);
        Task<IList<KnowledgeBaseCategoryQuestionAnswersDto>> GetKnowledgeBaseCategoryQuestionAnswersForRouteAsync(KnowledgeBaseApplicationTypeEnum applicationType, string controller, string action);
        Task<KnowledgeBaseCategoryQuestionAnswersDto> GetKnowledgeBaseCategoryQuestionAnswerAsync(KnowledgeBaseApplicationTypeEnum applicationType, int subcategoryId, int categoryId, int questionId);
        Task<List<KnowledgeBaseQuestionAnswerDto>> GetTopVotedKnowledgeBaseQuestionAnswersAsync(KnowledgeBaseApplicationTypeEnum applicationType);
        Task LogSearchTextAsync(string searchText);
        void SaveKnowledgeBaseQuestionAnswerVote(int knowledgeBaseQuestionAnswerId, int voteValue);
        void SaveKnowledgeBaseQuestionAnswerFeedback(int knowledgeBaseQuestionAnswerId, string feedbackText);
    }
}
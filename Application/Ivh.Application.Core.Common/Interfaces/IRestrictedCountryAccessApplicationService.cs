﻿namespace Ivh.Application.Core.Common.Interfaces
{
    public interface IRestrictedCountryAccessApplicationService
    {
        bool IsIpRestrictedByCountry(string ipToCheck);
    }
}
﻿namespace Ivh.Application.Core.Common.Interfaces
{
    using System.Threading.Tasks;
    using Ivh.Common.Base.Interfaces;
    using Ivh.Common.ServiceBus.Interfaces;
    using Ivh.Common.VisitPay.Messages.Core;

    public interface IGuarantorEnrollmentApplicationService : IApplicationService,
        IUniversalConsumerService<EnrollGuarantorMessage>,
        IUniversalConsumerService<UnEnrollGuarantorMessage>
    {
        Task EnrollGuarantorAsync(int vpGuarantorId);
        Task UnEnrollGuarantorAsync(int vpGuarantorId);
        Task PublishEnrollGuarantorMessageAsync(int vpGuarantorId);
        Task PublishUnEnrollGuarantorMessageAsync(int vpGuarantorId);
    }
}
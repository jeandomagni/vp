﻿namespace Ivh.Application.Core.Common.Interfaces
{
    using System;
    using Microsoft.AspNet.Identity;
    using System.Collections.Generic;
    using System.Security.Claims;
    using System.Security.Principal;
    using System.Threading.Tasks;
    using Dtos;
    using Ivh.Common.Base.Interfaces;
    using User.Common.Dtos;
    using Ivh.Common.EventJournal;
    using Ivh.Common.JobRunner.Common.Interfaces;
    using Ivh.Common.VisitPay.Enums;
    using Ivh.Common.VisitPay.Jobs;

    public interface IVisitPayUserApplicationService : IApplicationService,
        IJobRunnerService<DeactivateInactiveClientUsersJobRunner>
    {
        //UserManager
        Task<string> GetPhoneNumberAsync(string userId);
        Task<bool> GetTwoFactorEnabledAsync(string userId);
        Task<IList<UserLoginInfo>> GetLoginsAsync(string userId);
        Task<IdentityResult> RemoveLoginAsync(string userId, UserLoginInfo login);
        VisitPayUserDto FindById(string userId);
        VisitPayUserDto FindByGuarantorId(int vpGuarantorId);
        Task<VisitPayUserDto> FindByIdAsync(string userId);
        Task<string> GenerateChangePhoneNumberTokenAsync(string userId, string phoneNumber);
        Task<IdentityResult> ChangePhoneNumberAsync(string userId, string phoneNumber, string token);
        Task<IdentityResult> SetPhoneNumberAsync(string userId, string phoneNumber);
        Task<IdentityResult> ChangePasswordAsync(string userId, string currentPassword, string newPassword, int guarantorPasswordExpLimitInDays, int guarantorPasswordReuseLimit);
        Task<IdentityResult> AddPasswordAsync(string userId, string password);
        Task<IdentityResult> AddLoginAsync(string userId, UserLoginInfo login);
        Task<IdentityResult> CreateWithBasicRoleAsync(VisitPayUserDto user, string password, string role);
        IdentityResult CreatePatient(VisitPayUserDto user, string password, string registrationMatchString, int termsOfUseId, int esignTermsId, int? paymentDueDay, IList<SecurityQuestionAnswerDto> securityQuestionAnswerDtos, int guarantorPasswordExpLimitInDays, DateTime? patientDateOfBirth);
        GuarantorDto CreateOfflinePatient(VisitPayUserDto user, string registrationMatchString, DateTime? patientDateOfBirth, int? paymentDueDate = null);
        void AddEmailCommunicationPreferences(JournalEventHttpContextDto context, int visitPayUserId, int loggedInVisitPayUserId);
        void AddMailCommunicationPreferences(JournalEventHttpContextDto context, int visitPayUserId, int loggedInVisitPayUserId);
        void RemoveMailCommunicationPreferences(JournalEventHttpContextDto context, int visitPayUserId, int loggedInVisitPayUserId);
        void RemoveEmailCommunicationPreferences(JournalEventHttpContextDto context, int visitPayUserId, int loggedInVisitPayUserId);
        bool UserHasCommunicationPreference(int visitPayUserId, CommunicationMethodEnum communicationMethod, CommunicationGroupEnum? communicationGroup = null);
        Task<IdentityResult> ConfirmEmailAsync(string userId, string token);
        Task<VisitPayUserDto> FindByNameAsync(string userName);
        VisitPayUserDto FindByName(string userName);
        Task<bool> IsEmailConfirmedAsync(string userId);
        Task<IdentityResult> ResetPasswordAsync(string userId, string tempPassword, string newPassword, int guarantorPasswordExpLimitInDays, int guarantorPasswordReuseLimit);
        /// <summary>
        /// Reset a user's password without a temp password
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="newPassword"></param>
        /// <param name="guarantorPasswordExpLimitInDays"></param>
        /// <param name="guarantorPasswordReuseLimit"></param>
        /// <returns></returns>
        Task<IdentityResult> ResetPasswordAsync(string userId, string newPassword, int guarantorPasswordExpLimitInDays, int guarantorPasswordReuseLimit);
        IdentityResult UpdateUser(VisitPayUserDto user, bool validateMailingAddress = false);
        IdentityResult UpdateUserAndGuarantorType(VisitPayUserDto userDto, GuarantorTypeEnum guarantorTypeEnum);
        Task<SignInStatusExEnum> PasswordSignInAsync(HttpContextDto context, string userName, string password, bool isPersistent, bool shouldLockout, string roleType, Func<SignInStatusExEnum, int, HttpContextDto, IList<Claim>, Task> afterSuccessAction = null, string authenticationType = DefaultAuthenticationTypes.ApplicationCookie);
        void SignIn(VisitPayUserDto user, bool isPersistent, bool rememberBrowser);
        void SignOut(string userName, bool sessionTimeout);
        //New
        Task<SignInStatusExEnum> ReLoginAsync(string userId, HttpContextDto context, Func<SignInStatusExEnum, int, HttpContextDto, IList<Claim>, Task> afterSuccessAction = null);
        Task SendSmsMessageForCode(string phoneNumber, string code);
        bool IsUsernameAvailable(string userName, int? userId = null);
        bool IsUsernameValid(string userName);
        Task<IdentityResult> IsPasswordStrongEnough(string password);
        GuarantorDto GetGuarantorFromVisitPayUserId(int visitPayUserId);
        int GetGuarantorIdFromVisitPayUserId(int visitPayUserId);
        int GetVisitPayUserId(int vpGuarantorId);
        DateTime? GetLastLoginDate(int visitPayUserId);
        Task<bool> ValidateSecureCodeAsync(int visitPayUserId, string secureCode, JournalEventHttpContextDto context);
        void AcknowledgeEsign(int visitPayUserId, int esignCmsVersionId);
        void SetUserLocale(string userId, string locale);
        string GetUserLocale(string userId);
        //Emulate user
        SignInStatusExEnum EmulateTokenSignIn(string token);
        string GenerateEmulateToken(string visitPayUserId, string emulateUserId, JournalEventHttpContextDto context);
        //Forgot credentials
        bool ForgotPasswordValidateSecurityQuestion(int visitPayUserId, int securityQuestionId, string challengeText, JournalEventHttpContextDto context);
        Task<bool> RequestUsernameGuarantor(VisitPayUserFindByDetailsParametersDto visitPayUserFindByDetailsParameters);
        Task<bool> RequestPasswordGuarantor(VisitPayUserFindByDetailsParametersDto visitPayUserFindByDetailsParameters, JournalEventHttpContextDto context);
        SecurityQuestionDto GetRandomSecurityQuestion(int visitPayUserId, int? currentSecurityQuestionId);
        bool VerifyTempPassword(string password, string checkPassword);
        //Client Specific
        IList<VisitPayRoleDto> GetClientUserRoles(bool isIvhAdmin);
        IReadOnlyList<VisitPayUserDto> GetVisitPayClientUsers();
        VisitPayUserResultsDto GetVisitPayClientUsers(VisitPayUserFilterDto visitPayUserFilterDto, int currentVisitPayUserId, int batch, int batchSize);
        IList<VisitPayUserDto> GetVisitPayClientUsersByRole(IList<int> roles, bool? isDisabled, IList<int> excludedRoles);
        Task<IdentityResult> CreateClientUser(VisitPayUserDto user, IList<string> roles, int clientTempPasswordExpLimitInHours, int clientPasswordExpLimitInDays);
        IdentityResult UpdateClientUser(VisitPayUserDto userDto, IList<string> roles, bool isLocked, bool isDisabled, bool isIvhAdmin);
        Task<bool> BeginResetPasswordClientAsync(int visitPayUserId, int clientTempPasswordExpLimitInHours, bool log = true, bool isNewAccount = false);
        Task<IdentityResult> CompleteResetPasswordClientAsync(string userId, string tempPassword, string newPassword, int clientPasswordExpLimitInDays, int clientPasswordReuseLimit);
        Task<IdentityResult> ChangePasswordClientAsync(string visitPayUserId, string currentPassword, string newPassword, int clientPasswordExpLimitInDays, int clientPasswordReuseLimit);
        Task<bool> ClientResetClientPassword(int targetVisitPayUserId, int clientTempPasswordExpLimitInHours, int actionTakenByVisitPayUserId, JournalEventHttpContextDto context);
        IdentityResult AddSecurityQuestionsClientUser(VisitPayUserDto userDto, bool clearExisting, JournalEventHttpContextDto context);
        Task<SignInStatusExEnum> ClientReLoginAsync(VisitPayUserDto userDto, HttpContextDto context);
        Task<bool> ClientResetAccount(VisitPayUserDto userDto, string secureCode, int actionTakenByVisitPayUserId, JournalEventHttpContextDto context);
        void SendClientProfileChangeNotification(int visitPayUserId);
        void DeactivateInactiveClientUsers();
        Task<string> GetGuarantorPasswordRequirements();
        Task<string> GetGuarantorUsernameRequirements();
        string GetClientPasswordRequirements();
        bool IsPasswordExpired(VisitPayUserDto visitPayUserDto);
        bool IsTempPasswordExpired(VisitPayUserDto visitPayUserDto);
        void TermsAcknowledgement(string visitPayUserId, VisitPayUserAcknowledgementTypeEnum acknowledgementType, int cmsVersionId, bool removeClaim = true);
        bool RequestPassword(int visitPayUserId, int tempPasswordExpLimitInHours);
        bool RequestPasswordForGuarantor(int guarantorId, int tempPasswordExpLimitInHours);
        void RemoveClaim(string[] claimType);
        void RemoveClaimWithValue(string value);
        void UpdateClaim(ClaimTypeEnum claimTypeEnum, string value, int visitPayUserId);
        bool? IsUserEligibleForHealthEquityOutboundSso(int vpGuarantorId);
        bool DisableEnableVisitPayUser(int visitPayUserId, bool disable, int actionTakenByVisitPayUserId, JournalEventHttpContextDto context);
        bool IsAccountEnabled(int visitPayUserId);
        Task<byte[]> ExportVisitPayClientUsersAsync(VisitPayUserFilterDto visitPayUserFilter, int visitPayUserId, bool includeIvinciUsers, bool includeClientUsers);
        SmsPhoneTypeEnum? GetSmsAcknowledgement(string visitPayUserId);
        void SetSmsAcknowledgement(string visitPayUserId, SmsPhoneTypeEnum smsPhoneType, int cmsVersionId, JournalEventHttpContextDto context);
        void RemoveSmsAcknowledgement(string visitPayUserId, bool fromSmsReply, string clientVisitPayUserId, JournalEventHttpContextDto context);
        void RemoveSmsAcknowledgements(string phoneNumber, JournalEventHttpContextDto context);
        void SendSmsValidationMessage(string visitPayUserId, SmsPhoneTypeEnum smsPhoneType, string token, JournalEventHttpContextDto context);
        IdentityResult SecondaryPhoneDelete(string visitPayUserId, JournalEventHttpContextDto context, string clientVisitPayUserId);
        IdentityResult UpdatePhone(string visitPayUserId, string phoneNumber, string phoneNumberType, bool isPrimary, JournalEventHttpContextDto context, string clientVisitPayUserId);
        void LogInvalidBillingIdEvent(string errorMessage, string visitPayUserId, int vpGuarantorId);
        void LogClientRequestPasswordVerificationFailed(string enteredUserName, VisitPayUserDto visitPayUser, JournalEventHttpContextDto context);
        Task<bool> LogVerificationResultAsync(string eventUserName, int? visitPayUserId, bool success, JournalEventHttpContextDto context);
        void LogAddEditSecurityQuestions(VisitPayUserDto user, JournalEventHttpContextDto context, bool isClient);
        void RefreshRoles(IIdentity identity);


        #region unreferenced code

        /*
        Task<DataResult<VisitPayUserDto, FindUserEnum>> FindByDetails(string lastName, DateTime dateOfBirth, int ssn4);
        Task<bool> HasBeenVerifiedAsync();
        IdentityResult CreateWithBasicRole(VisitPayUserDto user, string password, string role);
        bool InactivateUser(string username);
        bool IsNonMigratedLegacyUser(string username);
        bool DoesUserHaveSmsAcknowledgement(int visitPayUserId);
        */

        #endregion
    }
}

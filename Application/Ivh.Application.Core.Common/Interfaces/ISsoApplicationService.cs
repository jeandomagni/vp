﻿namespace Ivh.Application.Core.Common.Interfaces
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using System.Web;
    using Dtos;
    using Ivh.Common.Base.Interfaces;
    using Ivh.Common.Base.Utilities.Responses;
    using Ivh.Common.ServiceBus.Interfaces;
    using Ivh.Common.VisitPay.Enums;
    using Ivh.Common.VisitPay.Messages.Core;
    using User.Common.Dtos;

    public interface ISsoApplicationService : IApplicationService,
        IUniversalConsumerService<SsoInvalidateMessage>
    {
        /// <summary>
        ///     This needs to be called after an encrypted SSO token is received.
        ///     It will automatically log a user in if a match is found.
        ///     The agreement is handled as an attribute in MVC.
        /// </summary>
        Task<SsoResponseDto> SignInWithSso(HttpContextDto context, SsoRequestDto request);

        /// <summary>
        ///     This is the method that needs to be called for the first time logging in after sso token was received.
        /// </summary>
        Task<SignInStatusExEnum> PasswordSignInAsync(HttpContextDto context, SsoPasswordSignInRequest signInRequest);
        Task<SignInStatusExEnum> ReLoginAsync(HttpContextDto context, string userId);

        SsoProviderDto GetProvider(SsoProviderEnum ssoProvider);
        SsoProviderDto GetProviderBySourceSystemKey(string sourceSystemKey);
        IList<SsoProviderWithUserSettingsDto> GetAllProvidersWithUserSettings(int visitPayUserId);
        SsoVisitPayUserSettingDto AcceptSso(HttpContextDto context, SsoProviderEnum ssoProvider, int visitPayUserId, int? actionTakenByVisitPayUserId = null);
        SsoVisitPayUserSettingDto DeclineSso(HttpContextDto context, SsoProviderEnum ssoProvider, int visitPayUserId, int? actionTakenByVisitPayUserId = null);
        SsoVisitPayUserSettingDto IgnoreSso(SsoProviderEnum ssoProvider, int visitPayUserId, bool ignore, int? actionTakeByVisitPayUserId = null);
        void InvalidateSso(HttpContextDto context, int visitPayUserId, int vpGuarantorId);
        void AttemptedSso(HttpContextDto context, int visitPayUserId, int? actionTakenByVisitPayUserId = null);
        SsoVisitPayUserSettingDto AcknowledgeInvalidatedSso(SsoProviderEnum ssoProvider, int visitPayUserId, int? actionTakeByVisitPayUserId = null);
        void DeclineAllAccceptedSso(HttpContextDto context, int visitPayUserId, int? actionTakenByVisitPayUserId = null);
        string GenerateEncryptedSsoToken(int vpGuarantorId, SsoProviderEnum providerEnum, string ssoSsk, DateTime dob);
        SsoStatusEnum? GetUserSsoStatus(SsoProviderEnum provider, int visitPayUserId);
        bool HasUserAcceptedSsoTerms(SsoProviderEnum provider, int visitPayUserId);
        bool HasUserIgnoredSso(SsoProviderEnum provider, int visitPayUserId);
        Response<SamlResponseDto> GetSamlResponse(int visitPayUserId, SamlResponseTargetEnum samlResponseTarget, string samlRequest = null, string relayState = null);
        bool IsUserAuthorized(SsoProviderEnum ssoProviderEnum, HttpContextDto context = null);
        void SetupOAuth(SsoProviderEnum oAuthProvider, int currentUserId, HttpRequestBase request, HttpResponseBase response, HttpContextBase context);
        IList<SsoProviderDto> GetEnabledProviders(IList<SsoProviderEnum> ssoProviderEnums);
        Task<SsoProviderDto> GetActiveProviderAsync(SsoProviderEnum provider);
        SsoProviderEnum SelectHealthEquitySsoProvider();
        Task<SsoCanRedirectToSsoResult> RedirectToSso(SsoProviderEnum target, int currentUserId, HttpContextDto context);
        bool RefreshOAuthAccessTokenWithRefreshToken(SsoProviderWithUserSettingsDto settingForProvider);
    }
}
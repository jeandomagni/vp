﻿namespace Ivh.Application.Core.Common.Interfaces
{
    using Ivh.Common.Base.Interfaces;

    public interface IIpAccessApplicationService : IApplicationService
    {
        bool IsIpAddressAllowed(string ipToCheck);
    }
}
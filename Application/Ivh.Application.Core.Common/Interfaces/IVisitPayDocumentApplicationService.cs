﻿namespace Ivh.Application.Core.Common.Interfaces
{
    using System.Collections.Generic;
    using Dtos;

    public interface IVisitPayDocumentApplicationService
    {
        IList<VisitPayDocumentResultDto> GetVisitPayDocumentResults(VisitPayDocumentFilter visitPayDocumentFilter);
        VisitPayDocumentDto GetVisitPayDocumentDtoById(int visitPayDocumentId);
        VisitPayDocumentResultDto GetVisitPayDocumentResultDtoById(int visitPayDocumentId);
        void AddVisitPayDocument(VisitPayDocumentDto visitPayDocument);
        void SaveVisitPayDocument(VisitPayDocumentEditDto visitPayDocumentEdit);
        void DeleteVisitPayDocument(int visitPayDocumentId);
    }
}
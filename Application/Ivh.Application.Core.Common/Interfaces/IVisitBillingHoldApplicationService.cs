﻿namespace Ivh.Application.Core.Common.Interfaces
{
    using Dtos;
    using Ivh.Common.Base.Interfaces;

    public interface IVisitBillingHoldApplicationService : IApplicationService
    {
        void AddBillingHold(int billingSystemId, string sourceSystemKey);
        void ClearBillingHold(int billingSystemId, string sourceSystemKey);
    }
}
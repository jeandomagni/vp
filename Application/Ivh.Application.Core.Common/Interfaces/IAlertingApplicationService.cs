﻿namespace Ivh.Application.Core.Common.Interfaces
{
    using System.Collections.Generic;
    using Dtos;
    using Ivh.Common.Base.Interfaces;
    using Ivh.Common.Session;

    public interface IAlertingApplicationService : IApplicationService
    {
        IList<AlertDto> GetAlertsForGuarantor(int vpGuarantorId, ISessionFacade session, IList<int> dismissedAlerts,bool isMobile = false);
    }
}
﻿namespace Ivh.Application.Core.Common.Interfaces
{
    using Ivh.Common.Base.Interfaces;

    public interface IVisitPayUserIssueResultApplicationService : IApplicationService
    {
        string[] AdjustAllGuarantorDates(int guarantorId, string datePart, int adjustment);
        string[] ClearGuarantorData(int guarantorId);
    }
}

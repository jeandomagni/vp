﻿namespace Ivh.Application.Core.Common.Interfaces
{
    using System.Collections.Generic;
    using Dtos.Eob;
    using Ivh.Application.Base.Common.Interfaces;
    using Ivh.Common.ServiceBus.Interfaces;
    using Ivh.Common.VisitPay.Messages.Eob;

    public interface IVisitEobApplicationService : IApplicationService,
        IUniversalConsumerService<EobClaimPaymentMessage>,
        IUniversalConsumerService<EobPayerFilterMessage>,
        IUniversalConsumerService<EobPayerFilterEobExternalLinkMessage>,
        IUniversalConsumerService<EobExternalLinkMessage>,
        IUniversalConsumerService<EobDisplayCategoryMessage>,
        IUniversalConsumerService<EobClaimAdjustmentReasonCodeMessage>,
        IUniversalConsumerService<EobClaimAdjustmentReasonCodeDisplayMapMessage>
    {
        IList<VisitEobDetailsResultDto> GetVisitEobDetails(int visitBillingSystemId, string visitSourceSystemKey);
        bool HasVisitEobDetails(int billingSystemId, string visitSourceSystemKey);
        IList<EobIndicatorResultDto> Get835RemitIndicators(IDictionary<string, int> visits);
    }
}

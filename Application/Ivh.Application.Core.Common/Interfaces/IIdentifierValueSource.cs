﻿namespace Ivh.Application.Core.Common.Interfaces
{
    using System.Text.RegularExpressions;

    public interface IIdentifierValueSource<T>
    {
        Regex ValidationPattern { get; }
        T GenerateIdentifier();
    }
}
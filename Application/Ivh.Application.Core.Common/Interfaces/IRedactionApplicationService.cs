﻿namespace Ivh.Application.Core.Common.Interfaces
{
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Dtos;
    using Ivh.Common.Base.Enums;
    using Ivh.Common.Base.Interfaces;
    using Ivh.Common.VisitPay.Enums;
    using Ivh.Common.VisitPay.Messages.HospitalData;
    using User.Common.Dtos;

    public interface IRedactionApplicationService : IApplicationService
    {
        void RedactVisit(string sourceSystemKey, int billingSystemId, VisitPayUserDto actionVisitPayUser);
        
        void ProcessHsGuarantorMatchDiscrepancy(ChangeSetMessage changeSetMessage);
        void ProcessHsGuarantorVpIneligible(ChangeSetMessage changeSetMessage);
        void ProcessHsGuarantorUnmatch(ChangeSetMessage changeSetMessage);
        void UpdateHsGuarantorMatchDiscrepancy(HsGuarantorMatchDiscrepancyDto hsGuarantorMatchDiscrepancyDto);
        HsGuarantorMatchDiscrepancyResultsDto GetAllHsGuarantorAttributeChanges(HsGuarantorMatchDiscrepancyFilterDto filterDto, int pageNumber, int pageSize);
        HsGuarantorMatchDiscrepancyResultsDto GetAllHsGuarantorMatchDiscrepancy(HsGuarantorMatchDiscrepancyFilterDto filterDto, int pageNumber, int pageSize);
        VisitUnmatchResultsDto GetAllVisitUnmatches(VisitUnmatchFilterDto filterDto, int pageNumber, int pageSize);
        IReadOnlyList<HsGuarantorMatchDiscrepancyStatusDto> GetAllHsGuarantorMatchDiscrepancyStatuses();
        IDictionary<string, string> GetMatchFieldsForDisplay();
        bool ProcessGuarantorUnmatchRequest(HsGuarantorUnmatchRequestDto requestDto, UnmatchSourceEnum unmatchSource);
        bool ProcessVisitUnmatchRequest(VisitUnmatchRequestDto requestDto);
        Task<byte[]> ExportAllHsGuarantorMatchDiscrepancyAsync(HsGuarantorMatchDiscrepancyFilterDto filterDto, string userName);
        Task<byte[]> ExportGuarantorSummaryReportAsync(HsGuarantorMatchDiscrepancyFilterDto filterDto, string userName);
        Task<byte[]> ExportVisitUnmatchReportAsync(VisitUnmatchFilterDto filterDto, string userName);
    }
}

﻿namespace Ivh.Application.Core.Common.Utilities.Builders
{
    using System;
    using System.Collections.Generic;
    using System.Drawing;
    using System.Linq;
    using Ivh.Common.Base.Utilities.Extensions;
    using Ivh.Common.VisitPay.Strings;
    using OfficeOpenXml;
    using OfficeOpenXml.Drawing;
    using OfficeOpenXml.Style;

    /// <summary>
    ///     This API assist in creating excel exports in a standard manner
    ///     The standard export pattern consist of:
    ///     Logo (optional)
    ///     Title
    ///     Export info or Info Block (typically name of person exporting and date of export)
    ///     Header
    ///     Data rows
    ///     Conditional messaging (no data or incomplete download)
    /// </summary>
    public class iVinciExcelExportBuilder
    {
        public const int MaxExportRecords = 10000;

        private readonly List<Row> _dataBlock = new List<Row>();
        private readonly ExcelPackage _excelPackage;
        private readonly List<Column> _infoBlock = new List<Column>();
        private readonly int _rowOffsetLogo = 4;

        private readonly string _title;
        private readonly ExcelWorksheet _worksheet;

        private int _columnMergeWidth = 3; //default to 3, but will be updated by the header length
        private int _dataBlockRowEndIndex;

        private bool _hasBeenBuilt;
        private bool _hasLogo;
        private List<string> _header = new List<string>();
        private int _incompleteDataMessageRowIndex;
        private int _infoBlockEndRowIndex;
        private int _initialDataBlockRowIndex = 3;
        private int _initialInfoBlockRowIndex = 3;

        private string _noRecordsMessage = "There are no records to display at this time.";
        private int _titleRowIndex = 1;

        public iVinciExcelExportBuilder(ExcelPackage excelPackage, string title)
        {
            if (title.IsNullOrEmpty())
            {
                throw new Exception("A title is required.");
            }

            this._excelPackage = excelPackage ?? throw new Exception("ExcelPackage is required and should be passed in inside a using statement");
            this._worksheet = excelPackage.Workbook.Worksheets.Add(title);
            this._title = title;
        }

        public iVinciExcelExportBuilder WithWorkSheetNameOf(string worksheetName)
        {
            this._worksheet.Name = worksheetName;
            return this;
        }

        public iVinciExcelExportBuilder WithLogo(Image logo)
        {
            ExcelPicture pic = this._worksheet.Drawings.AddPicture("logo", logo);
            pic.SetPosition(0, 0);
            pic.SetSize(logo.Width, logo.Height);

            this._titleRowIndex += this._rowOffsetLogo;
            this._initialInfoBlockRowIndex += this._rowOffsetLogo;
            this._hasLogo = true;

            return this;
        }

        /// <summary>
        ///     The Info Block has a 2 column format, description and value are inline
        ///     The Column.Description will be on the left. Column.Styles will not be applied to the description (style will be applied to the whole Info Block section)
        ///     The Column.Value will be on the right and the Column.Styles will be applied to this cell in addition to normal Info Block styling
        /// </summary>
        public iVinciExcelExportBuilder AddInfoBlockRow(Column column)
        {
            this._infoBlock.Add(column);
            return this;
        }

        /// <summary>
        ///     Use this when a separator needs to be added between groups of rows that belong in the Info Block
        /// </summary>
        public iVinciExcelExportBuilder AddBlankInfoBlockRow()
        {
            Column blank = new Column { Value = "", ColumnDescription = "" };
            return this.AddInfoBlockRow(blank);
        }

        /// <summary>
        ///     This allows for a custom message when the data records are empty.
        ///     The default is "There are no records to display at this time."
        /// </summary>
        public iVinciExcelExportBuilder WithNoRecordsFoundMessageOf(string noRecordsFoundMessage)
        {
            this._noRecordsMessage = noRecordsFoundMessage;
            return this;
        }

        /// <summary>
        ///     Provide the list of data objects and a function that returns a list of Column
        ///     Add the columns in order of the corresponding headers.
        ///     The number of columns should match the number of headers.
        /// </summary>
        public iVinciExcelExportBuilder AddDataBlock<T>(IEnumerable<T> source, Func<T, List<Column>> func)
        {
            if (source != null)
            {
                foreach (T element in source)
                {
                    List<Column> columns = func(element).ToList();
                    this._dataBlock.Add(new RowBuilder().AddRow(columns));
                }
            }
            return this;
        }

        public ExcelPackage Build()
        {
            if (this._hasBeenBuilt)
            {
                return null;
            }

            this.ConfigureStartPositionsAndMergeLengths();
            if (this._hasLogo)
            {
                // merge the logo cells 
                string logoMergeRange = $"{this.FormatCellAddress(0, 1)}:{this.FormatCellAddress(this._columnMergeWidth, this._rowOffsetLogo)}";
                this._worksheet.Cells[logoMergeRange].Merge = true;
            }

            // add title , merge and add style
            this._worksheet.Cells[$"A{this._titleRowIndex}"].Value = this._title;
            string titleMergeRange = $"{this.FormatCellAddress(0, this._titleRowIndex)}:{this.FormatCellAddress(this._columnMergeWidth, this._titleRowIndex)}";
            ExcelRange titleCells = this._worksheet.Cells[titleMergeRange];
            titleCells.Merge = true;
            titleCells.Style.Font.Size = 13;
            titleCells.Style.Font.Bold = true;

            // add info block
            if (this._infoBlock.Count > 0)
            {
                this._infoBlockEndRowIndex = this.InsertInfoRows(this._initialInfoBlockRowIndex, this._infoBlock);
                string infoBlockAddress = $"{this.FormatCellAddress(0, this._initialInfoBlockRowIndex)}:{this.FormatCellAddress(this._columnMergeWidth, this._infoBlockEndRowIndex)}";
                using (ExcelRange infoBlockRange = this._worksheet.Cells[infoBlockAddress])
                {
                    this.ApplyInfoStyles(infoBlockRange);
                }
            }

            //add the data block
            if (this._dataBlock.Count > 0)
            {

                if (this._dataBlock.Count > MaxExportRecords)
                {
                    this._incompleteDataMessageRowIndex = this._initialDataBlockRowIndex;
                    string incompleteDownloadAddress = $"{this.FormatCellAddress(0, this._incompleteDataMessageRowIndex)}:{this.FormatCellAddress(this._columnMergeWidth, this._incompleteDataMessageRowIndex)}";
                    using (ExcelRange range = this._worksheet.Cells[incompleteDownloadAddress])
                    {
                        range.Style.Font.Italic = true;
                        range.Merge = true;
                        range.Value = string.Format("Incomplete download. {0} rows of {1} rows were downloaded. Only {0} rows can be downloaded.", MaxExportRecords.ToString("N0"), this._dataBlock.Count().ToString("N0"));
                    }
                    this._initialDataBlockRowIndex += 2;
                }

                // add header
                this._header = this._dataBlock.First().Columns.Select<Column, string>(x => x.ColumnDescription).ToList();
                for (int headerIndex = 0; headerIndex < this._header.Count; headerIndex++)
                {
                    string headerCellAddress = this.FormatCellAddress(headerIndex, this._initialDataBlockRowIndex);
                    this._worksheet.Cells[headerCellAddress].Value = this._header[headerIndex];
                }
                // style the header
                string headerBlockAddress = $"{this.FormatCellAddress(0, this._initialDataBlockRowIndex)}:{this.FormatCellAddress(this._columnMergeWidth, this._initialDataBlockRowIndex)}";
                using (ExcelRange excelRange = this._worksheet.Cells[headerBlockAddress])
                {
                    this.ApplyHeaderStyles(excelRange);
                }

                // insert the data rows
                this._initialDataBlockRowIndex++; //to account for header
                this._dataBlockRowEndIndex = this.InsertRows(this._initialDataBlockRowIndex, this._dataBlock);
                // style the data rows
                string dataRowsRange = $"{this.FormatCellAddress(0, this._initialDataBlockRowIndex)}:{this.FormatCellAddress(this._columnMergeWidth, this._dataBlockRowEndIndex)}";
                using (ExcelRange excelRange = this._worksheet.Cells[dataRowsRange])
                {
                    this.ApplyDataRowStyles(excelRange);
                }
            }
            else
            {
                string noDataMessageAddress = $"{this.FormatCellAddress(0, this._initialDataBlockRowIndex)}:{this.FormatCellAddress(5, this._initialDataBlockRowIndex)}";
                using (ExcelRange range = this._worksheet.Cells[noDataMessageAddress])
                {
                    range.Style.Font.Size = 11;
                    range.Style.Font.Bold = true;
                    range.Value = this._noRecordsMessage;
                    range.Merge = true;
                }
            }
            this._worksheet.Cells[this._worksheet.Dimension.Address].AutoFitColumns();
            this._hasBeenBuilt = true;
            return this._excelPackage;
        }

        /// <summary>
        ///     This will provide properties that can be used to access worksheet cells for further processing or testing after
        ///     calling Build
        /// </summary>
        /// <returns></returns>
        public WorksheetInfo PostBuildWorksheetInfo()
        {
            if (!this._hasBeenBuilt)
            {
                return null;
            }

            return new WorksheetInfo
            {
                TitleRowIndex = this._titleRowIndex,
                InfoBlockStartIndex = this._initialInfoBlockRowIndex,
                DataBlockStartIndex = this._initialDataBlockRowIndex,
                MergeColumnUpperBoundColumn = this.GetExcelColumn(this._columnMergeWidth),
                InfoBlockEndRowIndex = this._infoBlockEndRowIndex,
                DataBlockRowEndIndex = this._dataBlockRowEndIndex,
                IncompleteDataMessageRowIndex = this._incompleteDataMessageRowIndex
            };
        }

        private int InsertRows(int initialRowIndex, List<Row> block)
        {
            int currentRow = initialRowIndex;
            foreach (Row record in block)
            {
                for (int i = 0; i < record.Columns.Count; i++)
                {
                    Column dataBlockColumn = record.Columns[i];
                    string cellAddress = this.FormatCellAddress(i, currentRow);
                    using (ExcelRange excelRange = this._worksheet.Cells[cellAddress])
                    {
                        excelRange.Value = dataBlockColumn.Value;
                        this.ApplyColumnStyles(excelRange, dataBlockColumn.Styles);
                    }
                }
                currentRow++;
            }
            return currentRow - 1;
        }

        private int InsertInfoRows(int initialRowIndex, List<Column> columns)
        {
            int currentRow = initialRowIndex;
            foreach (Column column in columns)
            {
                //description 
                string descriptionCellAddress = this.FormatCellAddress(0, currentRow);
                using (ExcelRange excelRange = this._worksheet.Cells[descriptionCellAddress])
                {
                    excelRange.Value = column.ColumnDescription;
                }

                //value
                string valueCellAddress = this.FormatCellAddress(1, currentRow);
                using (ExcelRange excelRange = this._worksheet.Cells[valueCellAddress])
                {
                    excelRange.Value = column.Value;
                    this.ApplyColumnStyles(excelRange, column.Styles);
                }

                currentRow++;
            }
            return currentRow - 1;
        }

        private void ApplyColumnStyles(ExcelRange excelRange, List<Style> styles)
        {
            if (styles == null)
            {
                return;
            }

            foreach (Style style in styles)
            {
                switch (style.StyleType)
                {
                    case StyleTypeEnum.AlignCenter:
                        excelRange.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        break;
                    case StyleTypeEnum.AlignLeft:
                        excelRange.Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                        break;
                    case StyleTypeEnum.AlignRight:
                        excelRange.Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                        break;
                    case StyleTypeEnum.Bold:
                        excelRange.Style.Font.Bold = true;
                        break;
                    case StyleTypeEnum.Italics:
                        excelRange.Style.Font.Italic = true;
                        break;
                    case StyleTypeEnum.DateFormatter:
                    case StyleTypeEnum.NumberFormatter:
                        if (!string.IsNullOrWhiteSpace(style.Format))
                        {
                            excelRange.Style.Numberformat.Format = style.Format;
                        }

                        break;
                    case StyleTypeEnum.FontSize:
                        int fontSize;
                        bool parsed = int.TryParse(style.Format, out fontSize);
                        excelRange.Style.Font.Size = parsed ? fontSize : 11;
                        break;
                    default:
                        break;
                }
            }
        }
        
        private void ApplyHeaderStyles(ExcelRange excelRange)
        {
            excelRange.Style.Font.Size = 11;
            excelRange.Style.Font.Bold = true;
            excelRange.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            excelRange.Style.Border.BorderAround(ExcelBorderStyle.Medium);
            excelRange.Style.Border.Right.Style = ExcelBorderStyle.Medium;
        }

        private void ApplyInfoStyles(ExcelRange excelRange)
        {
            excelRange.Style.Font.Size = 11;
            excelRange.Style.Font.Bold = true;
        }

        private void ApplyDataRowStyles(ExcelRange excelRange)
        {
            excelRange.Style.Border.BorderAround(ExcelBorderStyle.Thin);
            excelRange.Style.Border.Right.Style = ExcelBorderStyle.Thin;
            excelRange.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
        }

        private void ConfigureStartPositionsAndMergeLengths()
        {
            this._initialDataBlockRowIndex = this._initialInfoBlockRowIndex;
            if (this._infoBlock != null)
            {
                this._initialDataBlockRowIndex = this._initialInfoBlockRowIndex + this._infoBlock.Count + 1;
            }

            if (this._dataBlock.Count > 0)
            {
                this._header = this._dataBlock.First().Columns.Select<Column, string>(x => x.ColumnDescription).ToList();
            }

            if (this._header != null)
            {
                this._columnMergeWidth = this._header.Count > this._columnMergeWidth ? this._header.Count - 1 : this._columnMergeWidth;
            }
        }
        
        private string GetExcelColumn(int columnIndex)
        {
            const string alpha = "ABCDEFGHIJKLMNOPQRSTUVQXYZ";

            // in the rare event that an exports columns exceed 26
            if (columnIndex > alpha.Length - 1)
            {
                int startChar = columnIndex / alpha.Length;
                if (startChar > alpha.Length - 1)
                {
                    throw new Exception("The column index provided is too large"); // greater than 625. 
                }

                int secondChar = (columnIndex % alpha.Length);
                return $"{alpha[startChar - 1]}{alpha[secondChar]}";
            }

            char colunmChar = alpha[columnIndex];
            return $"{colunmChar}";
        }

        private string FormatCellAddress(int columnIndex, int rowIndex)
        {
            string excelColumn = this.GetExcelColumn(columnIndex);
            return $"{excelColumn}{rowIndex}";
        }
    }

    #region Builder Classes

    public class RowBuilder
    {
        private readonly List<Column> _columns = new List<Column>();

        public Row AddRow(List<Column> columns)
        {
            return new Row { Columns = columns };
        }

        public static implicit operator Row(RowBuilder rowBuilder)
        {
            return new Row
            {
                Columns = rowBuilder._columns
            };
        }
    }

    public class ColumnBuilder
    {
        private readonly List<Style> _styles = new List<Style>();
        private readonly object _value;
        private readonly string _columnDescription;

        public ColumnBuilder(object value, string columnDescription)
        {
            this._value = value;
            this._columnDescription = columnDescription;
        }

        //TODO: Add remaining styles and remove this
        public ColumnBuilder WithStyle(StyleTypeEnum styleTypeType, string format = "")
        {
            this._styles.Add(new Style { StyleType = styleTypeType, Format = format });
            return this;
        }

        public ColumnBuilder AlignLeft()
        {
            this._styles.Add(new Style { StyleType = StyleTypeEnum.AlignLeft});
            return this;
        }

        public ColumnBuilder AlignRight()
        {
            this._styles.Add(new Style { StyleType = StyleTypeEnum.AlignRight });
            return this;
        }

        public ColumnBuilder AlignCenter()
        {
            this._styles.Add(new Style { StyleType = StyleTypeEnum.AlignCenter });
            return this;
        }
        
        public ColumnBuilder WithDateFormat()
        {
            this._styles.Add(new Style { StyleType = StyleTypeEnum.DateFormatter, Format = Format.DateFormat });
            return this;
        }

        public ColumnBuilder WithDateFormat(string format)
        {
            this._styles.Add(new Style { StyleType = StyleTypeEnum.DateFormatter, Format = format });
            return this;
        }

        public ColumnBuilder WithNumberFormat(string format)
        {
            this._styles.Add(new Style { StyleType = StyleTypeEnum.NumberFormatter, Format = format });
            return this;
        }

        public ColumnBuilder WithCurrencyFormat()
        {
            this.WithNumberFormat(Format.ExportMoneyFormat);
            return this;
        }

        public static implicit operator Column(ColumnBuilder columnBuilder)
        {
            return new Column
            {
                ColumnDescription = columnBuilder._columnDescription,
                Value = columnBuilder._value,
                Styles = columnBuilder._styles

            };
        }
    }

    public class Row
    {
        public List<Column> Columns { get; set; }
    }

    public class Column
    {
        public string ColumnDescription { get; set; }
        public object Value { get; set; }
        public List<Style> Styles { get; set; }
    }

    public class Style
    {
        public StyleTypeEnum StyleType { get; set; }
        public string Format { get; set; }
    }

    public class WorksheetInfo
    {
        public int TitleRowIndex { get; set; }
        public int InfoBlockStartIndex { get; set; }
        // The DataBlockStartIndex will serve as the index for the no data message as well (because the is no header)
        public int DataBlockStartIndex { get; set; }
        public string MergeColumnUpperBoundColumn { get; set; }
        public int HeaderRowIndex => this.DataBlockStartIndex - 1;
        public int InfoBlockEndRowIndex { get; set; }
        public int DataBlockRowEndIndex { get; set; }
        public int IncompleteDataMessageRowIndex { get; set; }
        public bool HasIncompleteDataMessage => this.IncompleteDataMessageRowIndex > 0;
    }

    public enum StyleTypeEnum
    {
        AlignLeft,
        AlignCenter,
        AlignRight,
        NumberFormatter,
        DateFormatter,
        Italics,
        Bold,
        FontSize
    }
}

#endregion
﻿namespace Ivh.Application.Core.Common.Dtos
{
    using System.Collections.Generic;

    public class HsGuarantorMatchDiscrepancyResultsDto
    {
        public IReadOnlyList<HsGuarantorMatchDiscrepancyDto> HsGuarantorMatchDiscrepancies { get; set; }
        public int TotalRecords { get; set; }
    }
}

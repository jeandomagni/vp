﻿namespace Ivh.Application.Core.Common.Dtos
{
    using System.Collections.Generic;
    using ProtoBuf;

    [ProtoContract]
    public class KnowledgeBaseCategoryDto : BaseKnowledgeBaseCategoryDto
    {
        public KnowledgeBaseCategoryDto()
        {
            this.KnowledgeBaseSubCategories = new List<KnowledgeBaseSubCategoryDto>();
        }

        [ProtoMember(1)]
        public List<KnowledgeBaseSubCategoryDto> KnowledgeBaseSubCategories { get; set; }
    }
}
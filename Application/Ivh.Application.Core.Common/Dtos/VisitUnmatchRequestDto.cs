﻿namespace Ivh.Application.Core.Common.Dtos
{
    using Ivh.Common.Base.Enums;
    using Ivh.Common.VisitPay.Enums;
    using User.Common.Dtos;

    public class VisitUnmatchRequestDto
    {
        public int VpGuarantorId { get; set; }
        public int VisitId { get; set; }
        public string SourceSystemKey { get; set; }
        public string Notes { get; set; }
        public VisitPayUserDto ActionVisitPayUser { get; set; }
        public HsGuarantorMatchStatusEnum MatchStatus { get; set; }
        public HsGuarantorUnmatchReasonEnum UnmatchReason { get; set; }
    }
}
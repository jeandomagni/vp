﻿namespace Ivh.Application.Core.Common.Dtos
{
    using System;
    using System.Collections.Generic;
    using Base.Common.Interfaces;
    using Ivh.Common.VisitPay.Attributes;
    using Ivh.Common.VisitPay.Enums;

    public class PaymentSummaryGroupedDto :
        IMaskConsolidated,
        IMaskMatching
    {
        public PaymentSummaryGroupedDto()
        {
            this.Payments = new List<PaymentSummaryGroupedTransactionDto>();
        }

        public int VpGuarantorId { get; set; }

        public string PatientName { get; set; }

        public decimal TotalTransactionAmount { get; set; }

        public DateTime? VisitDate { get; set; }

        [Mask]
        public string VisitDescription { get; set; }

        public string VisitSourceSystemKeyDisplay { get; set; }
        
        public IList<PaymentSummaryGroupedTransactionDto> Payments { get; set; }
        
        #region masking indicators
        
        public bool IsMaskedConsolidated { get; set; }

        public MatchOptionEnum? MatchOption { get; set; }

        #endregion
    }
}
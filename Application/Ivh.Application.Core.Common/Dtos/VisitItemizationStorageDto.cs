﻿namespace Ivh.Application.Core.Common.Dtos
{
    using Ivh.Common.Base.Utilities.Helpers;
    using System;

    public class VisitItemizationStorageDto
    {
        public int VisitFileStoredId { get; set; }
        public string SystemId { get; set; }
        public string FacilityCode { get; set; }
        public string FacilityDescription { get; set; }
        public string AccountNumber { get; set; }
        public string Empi { get; set; }
        public string MatchedEmpi { get; set; }
        public string MatchedAccountNumber { get; set; }
        public Guid ExternalStorageKey { get; set; }
        public GuarantorDto Guarantor { get; set; }
        public bool IsRemoved { get; set; }
        public string PatientFirstName { get; set; }
        public string PatientLastName { get; set; }
        public string PatientName { get; set; }
        public string VisitSourceSystemKeyDisplay { get; set; }
        public string MatchedVisitSourceSystemKeyDisplay { get; set; }
        public DateTime? VisitAdmitDate { get; set; }
        public DateTime? VisitDischargeDate { get; set; }
        public string PatientLastNameFirstName => FormatHelper.LastNameFirstName(this.PatientLastName, this.PatientFirstName);
        public string PatientFirstNameLastName => FormatHelper.FirstNameLastName(this.PatientFirstName, this.PatientLastName);
    }
}
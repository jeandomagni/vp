﻿namespace Ivh.Application.Core.Common.Dtos
{
    public class SsoPasswordSignInRequest
    {
        public string UserName { get; set; } 
        public string Password { get; set; }
        public SsoResponseDto SsoResponse { get; set; }
    }
}
﻿namespace Ivh.Application.Core.Common.Dtos
{
    using System.Collections.Generic;
    using Ivh.Common.Base.Enums;
    using Ivh.Common.VisitPay.Enums;

    public class VisitStateHistoryFilterDto
    {
        public VisitStateHistoryFilterDto()
        {
            this.VisitStates = new List<VisitStateEnum>();
        }

        public int VisitId { get; set; }
        public IList<VisitStateEnum> VisitStates { get; set; }

        public string SortField { get; set; }

        /// <summary>
        /// "asc" or "desc", not case sensitive
        /// </summary>
        public string SortOrder { get; set; }
    }
}


﻿namespace Ivh.Application.Core.Common.Dtos
{
    using System;

    public class AuditFilterDto
    {
        public DateTime StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public bool IncludeIvinci { get; set; }
        public bool IncludeClient { get; set; }
        public string SortField { get; set; }
        public string SortOrder { get; set; }
    }
}
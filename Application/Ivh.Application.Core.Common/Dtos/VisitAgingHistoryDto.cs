﻿namespace Ivh.Application.Core.Common.Dtos
{
    using System;
    using User.Common.Dtos;

    public class VisitAgingHistoryDto
    {
        public int VpGuarantorId { get; set; }
        public int VisitAgingHistoryId { get; set; }
        public virtual VisitDto Visit { get; set; }
        public DateTime InsertDate { get; set; }
        public int AgingTier { get; set; }
        public string ChangeDescription { get; set; }
        public VisitPayUserDto VisitPayUser { get; set; }
    }
}       
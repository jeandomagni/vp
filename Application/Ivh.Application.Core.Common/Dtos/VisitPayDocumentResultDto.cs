﻿namespace Ivh.Application.Core.Common.Dtos
{
    using System;

    /// <summary>
    /// Omits byte[] FileContent
    /// </summary>
    public class VisitPayDocumentResultDto: VisitPayDocumentBaseDto
    {
        public string UploadedByUserName { get; set; }
        public string ApprovedByUserName { get; set; }
    }
}

﻿namespace Ivh.Application.Core.Common.Dtos
{
    using Ivh.Common.Base.Enums;
    using Ivh.Common.VisitPay.Enums;

    public class HsGuarantorUnmatchReasonDto
    {
        public HsGuarantorUnmatchReasonEnum HsGuarantorUnmatchReasonEnum { get; set; }
        public string HsGuarantorUnmatchReasonName { get; set; }
        public string HsGuarantorUnmatchReasonDisplayName { get; set; }
    }
}

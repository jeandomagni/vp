﻿namespace Ivh.Application.Core.Common.Dtos
{
    using Ivh.Common.Base.Enums;
    using Ivh.Common.VisitPay.Enums;
    using ProtoBuf;

    [ProtoContract]
    [ProtoInclude(500, typeof(KnowledgeBaseFlattenedDto<KnowledgeBaseQuestionAnswerDto>))]
    [ProtoInclude(600, typeof(KnowledgeBaseFlattenedDto<KnowledgeBaseQuestionDto>))]
    [ProtoInclude(700, typeof(KnowledgeBaseCategoryDto))]
    public class BaseKnowledgeBaseCategoryDto
    {
        [ProtoMember(1)]
        public int KnowledgeBaseCategoryId { get; set; }

        [ProtoMember(2)]
        public string Name { get; set; }

        [ProtoMember(3)]
        public CmsRegionEnum CmsRegion { get; set; }

        [ProtoMember(4)]
        public string ContentBody { get; set; }

        [ProtoMember(5)]
        public string ContentTitle { get; set; }

        [ProtoMember(6)]
        public bool IsActive { get; set; }

        [ProtoMember(7)]
        public int DisplayOrder { get; set; }

        [ProtoMember(8)]
        public string IconCssClass { get; set; }
    }
}
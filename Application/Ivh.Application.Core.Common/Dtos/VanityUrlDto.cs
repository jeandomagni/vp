﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ivh.Application.Core.Common.Dtos
{
    public class VanityUrlDto
    {
        public int VanityUrlId { get; set; }
        public string VanityUrlKeyName { get; set; }
        public string RedirectController { get; set; }
        public string RedirectAction { get; set; }
        public DateTime? DateActiveUtc { get; set; }
        public DateTime? DateInactiveUtc { get; set; }
    }
}

﻿namespace Ivh.Application.Core.Common.Dtos
{
    using System;
    using Ivh.Common.VisitPay.Enums;

    public class VisitPayDocumentDto: VisitPayDocumentBaseDto
    {
        public byte[] FileContent { get; set; }
    }

    public class VisitPayDocumentBaseDto
    {
        public int VisitPayDocumentId { get; set; }
        public int UploadedByVpUserId { get; set; }
        public int? ApprovedByVpUserId { get; set; }
        public DateTime InsertDate { get; set; } 
        public DateTime? ActiveDate { get; set; }
        public DateTime? ApprovedDate { get; set; }
        public string AttachmentFileName { get; set; }
        public string DisplayTitle { get; set; }
        public string MimeType { get; set; }
        public int FileSize { get; set; }
        public virtual VisitPayDocumentTypeEnum VisitPayDocumentType { get; set; }

    }
}
﻿namespace Ivh.Application.Core.Common.Dtos
{
    using System;
    using System.Collections.Generic;
    using Ivh.Common.Base.Enums;
    using Ivh.Common.VisitPay.Enums;
    using Settings.Common.Dtos;

    public class ClientDto
    {

        /// <summary>
        /// Configuration for field validation when a user requests a Username or Password. 
        /// If this setting is left blank, the forms will use default behavior, using Ssn4 for additional verification. 
        /// The Json properties are Field (name of model property), Required (boolean) and Visible (bool).
        /// </summary>
        public virtual string AccountRecoveryConfiguration { get; set; }

        /// <summary>
        /// actionable balance text short
        /// </summary>
        public virtual string ActionableBalanceTextShort { get; set; }

        /// <summary>
        /// Actionable Balance Text Short
        /// </summary>
        public virtual string ActionableBalanceTextShortTitle { get; set; }

        /// <summary>
        /// actionable balance text long
        /// </summary>
        public virtual string ActionableBalanceTextLong { get; set; }

        /// <summary>
        /// Actionable Balance Text Long
        /// </summary>
        public virtual string ActionableBalanceTextLongTitle { get; set; }

        /// <summary>
        /// Active uncollectable days
        /// </summary>
        public virtual int ActiveUncollectableDays { get; set; }

        /// <summary>
        /// AgingTier to set when moving a visit from inactive to active and previous AgingTier is MaxAge.
        /// </summary>
        public virtual AgingTierEnum AgingTierWhenReactivatingMaxAgeVisit { get; set; }

        /// <summary>
        /// Client website url
        /// </summary>
        public virtual string AppClientUrlHost { get; set; }

        /// <summary>
        /// Patient express website url
        /// </summary>
        public virtual string AppGuarantorExpressUrlHost { get; set; }

        /// <summary>
        /// Guarantor registration url
        /// </summary>
        public virtual string AppGuarantorRegistrationUrl { get; set; }

        /// <summary>
        /// Patient website url
        /// </summary>
        public virtual string AppGuarantorUrlHost { get; set; }

        /// <summary>
        /// Patient website url shortened
        /// </summary>
        public virtual string AppGuarantorUrlHostShortened { get; set; }

        /// <summary>
        /// Client support business hours
        /// </summary>
        public virtual string AppSupportBusinessHours { get; set; }

        /// <summary>
        /// Client support notifcation email
        /// </summary>
        public virtual string AppSupportNotificationEmail { get; set; }

        /// <summary>
        /// Client support phone
        /// </summary>
        public virtual string AppSupportPhone { get; set; }

        /// <summary>
        /// Prefix for Client Support Request Case
        /// </summary>
        public virtual string AppSupportRequestCasePrefix { get; set; }

        /// <summary>
        /// Prefix for Client Support Request Name
        /// </summary>
        public virtual string AppSupportRequestNamePrefix { get; set; }

        /// <summary>
        /// Link to site for servicing bad debt visits not in Visitpay
        /// </summary>
        public virtual string BadDebtVisitsUrl { get; set; }

        /// <summary>
        /// Balance due notification in days
        /// </summary>
        public virtual int BalanceDueNotificationInDays { get; set; }

        /// <summary>
        /// Benefits Phone
        /// </summary>
        public virtual string BenefitsPhone { get; set; }

        /// <summary>
        /// Percentage of payment to be allocated to HB visits if PaymentAllocationModelType is BillingApplication.
        /// </summary>
        public virtual int BillingApplicationHBAllocationPercentage { get; set; }

        /// <summary>
        /// Priority of payment allocation for HB visits if PaymentAllocationModelType is BillingApplication.
        /// </summary>
        public virtual int BillingApplicationHBAllocationPriority { get; set; }

        /// <summary>
        /// Percentage of payment to be allocated to PB visits if PaymentAllocationModelType is BillingApplication.
        /// </summary>
        public virtual int BillingApplicationPBAllocationPercentage { get; set; }

        /// <summary>
        /// Priority of payment allocation for PB visits if PaymentAllocationModelType is BillingApplication.
        /// </summary>
        public virtual int BillingApplicationPBAllocationPriority { get; set; }

        /// <summary>
        /// Inner priority ordered grouping of visits that will take all available money before passing on to the next group.
        /// </summary>
        public virtual IList<PaymentAllocationPriorityGroupingEnum> PaymentAllocationOrderedPriorityGrouping { get; set; }

        /// <summary>
        /// Business hours - Warning: Contains English text that is not localized
        /// </summary>
        public virtual string BusinessHours { get; set; }

        /// <summary>
        /// Maximum number of times a guarantor can cancel a recurring payment
        /// </summary>
        public virtual int CancelRecurringPaymentMaximum { get; set; }

        /// <summary>
        /// Number of days for which the CancelRecurringPaymentMaximum applies
        /// </summary>
        public virtual int CancelRecurringPaymentWindow { get; set; }

        /// <summary>
        ///     Unlock user account if user logging in with correct username and password after n minutes from account lockout.
        /// </summary>
        public virtual int ClientAutoUnlockTime { get; set; }

        /// <summary>
        /// Client Preferred Brand Name
        /// </summary>
        public virtual string ClientBrandName { get; set; }

        /// <summary>
        /// The client office referenced in VPCC paper mail communications
        /// </summary>
        public virtual string ClientCentralBusinessOffice { get; set; }

        /// <summary>
        /// ClientDeactivateDays
        /// </summary>
        public virtual int ClientDeactivateDays { get; set; }

        /// <summary>
        /// Longer version of the clients name.
        /// </summary>
        public virtual string ClientFullName { get; set; }

        /// <summary>
        ///     Longer version of the clients name (upper case).
        /// </summary>
        public virtual string ClientFullNameUpperCase { get; set; }

        /// <summary>
        ///     Longer version of the clients name (alternate).
        /// </summary>
        public virtual string ClientFullNameAlt { get; set; }

        /// <summary>
        ///     Longer version of the clients name (alternate - upper case).
        /// </summary>
        public virtual string ClientFullNameAltUpperCase { get; set; }

        /// <summary>
        ///     Whether client choose Strong or Weak Password
        /// </summary>
        public virtual bool ClientIsStrongPassword { get; set; }

        /// <summary>
        /// Client survey on/off
        /// </summary>
        public virtual bool ClientIsSurveyOn { get; set; }

        /// <summary>
        /// Url for logo used in communications
        /// </summary>
        public virtual string ClientLogoRelativePath { get; set; }

        /// <summary>
        /// Width of logo used in communications
        /// </summary>
        public virtual string ClientLogoWidth { get; set; }

        /// <summary>
        ///     Number of failed login attempts for lockout
        /// </summary>
        public virtual int ClientMaxFailedAttemptCount { get; set; }

        /// <summary>
        /// Client Name (shorter version)
        /// </summary>
        public virtual string ClientName { get; set; }

        /// <summary>
        /// Name (shorter version - upper case)
        /// </summary>
        public virtual string ClientNameUpperCase { get; set; }
        
        /// <summary>
        /// Password expiry limit in days
        /// </summary>
        public virtual int ClientPasswordExpLimitInDays { get; set; }

        /// <summary>
        ///     Password should be atleast x characters long
        /// </summary>
        public virtual int ClientPasswordMinLength { get; set; }

        /// <summary>
        /// Number of passwords before reuse
        /// </summary>
        public virtual int ClientPasswordReuseLimit { get; set; }

        /// <summary>
        /// Client session timeout in minutes
        /// </summary>
        public virtual int ClientSessionTimeOutInMinutes { get; set; }
        /// <summary>
        /// Client Short Name
        /// </summary>
        public virtual string ClientShortName { get; set; }

        /// <summary>
        /// Client survey URL
        /// </summary>
        public virtual string ClientSurveyURL { get; set; }

        /// <summary>
        /// Number of required client user security questions
        /// </summary>
        public virtual int ClientSecurityQuestionsCount { get; set; }

        /// <summary>
        /// Temporary password expiry limit in hours
        /// </summary>
        public virtual int ClientTempPasswordExpLimitInHours { get; set; }

        /// <summary>
        ///     Client Time Zone
        /// </summary>
        public virtual string ClientTimeZone { get; set; }

        /// <summary>
        /// Client Time Zone Code
        /// </summary>
        public virtual string ClientTimeZoneCode { get; set; }

        /// <summary>
        /// Closed uncollectable days
        /// </summary>
        public virtual int ClosedUncollectableDays { get; set; }

        public virtual int ConsolidationExpirationDays { get; set; }
        
        public virtual int CreditAgreementMinResponsePeriod { get; set; }

        public virtual string CreditAgreementName { get; set; }

        /// <summary>
        /// Url for Offline CA acceptance via email
        /// </summary>
        public virtual string AppCreditAgreementUrl { get; set; }

        /// <summary>
        /// Maximum Aging Tier (less than or equal) for a visit to be considered discount eligible
        /// </summary>
        public virtual AgingTierEnum DiscountMaximumAgingTier { get; set; }
        
        /// <summary>
        /// Specify that only prompt payments can receive discounts
        /// </summary>
        public virtual bool DiscountPromptPayOnly { get; set; }

        /// <summary>
        /// Emulate token expiry limit in seconds
        /// </summary>
        public virtual int EmulateTokenExpLimitInSeconds { get; set; }

        /// <summary>
        ///     Emulate token expiry limit in minutes
        /// </summary>
        public virtual int ExpiringCreditCardReminderIntervalInDays { get; set; }

        /// <summary>
        /// Are the alternate public pages enabled?
        /// </summary>
        public virtual bool FeatureAlternatePublicPages { get; set; }

        /// <summary>
        /// Is Cancel Recurring Payment Enabled (Client)
        /// </summary>
        public virtual bool FeatureCancelRecurringIsEnabledClient { get; set; }

        /// <summary>
        /// Is Cancel Recurring Payment Enabled (Guarantor)
        /// </summary>
        public virtual bool FeatureCancelRecurringIsEnabledGuarantor { get; set; }

        /// <summary>
        /// Is Cancel Recurring Payment Enabled (Guarantor) when Past Due
        /// </summary>
        public virtual bool FeatureCancelRecurringPdIsEnabledGuarantor { get; set; }

        /// <summary>
        /// External card reading device integration is enabled
        /// </summary>
        public virtual bool FeatureCardReaderIsEnabled { get; set; }

        /// <summary>
        /// Is Chat Enabled
        /// </summary>
        public virtual bool FeatureChatIsEnabled { get; set; }

        /// <summary>
        /// Is Combining Finance Plans Enabled
        /// </summary>
        public virtual bool FeatureCombineFinancePlansEnabled { get; set; }

        /// <summary>
        /// Feature: Is Consolidation Enabled
        /// </summary>
        public virtual bool FeaturedConsolidationIsEnabled { get; set; }

        /// <summary>
        /// Feature: Is the ClientReportDashboard UI demo functionality enabled
        /// </summary>
        public virtual bool FeatureDemoClientReportDashboardIsEnabled { get; set; }
        
        /// <summary>
        /// Feature: Is the PreService UI demo functionality enabled
        /// </summary>
        public virtual bool FeatureDemoPreServiceUiIsEnabled { get; set; }

        /// <summary>
        /// Feature: Is the MyChart SSO UI demo functionality enabled
        /// </summary>
        public virtual bool FeatureDemoMyChartSsoUiIsEnabled { get; set; }

        /// <summary>
        /// Feature: Is the Insurance Accrual UI demo functionality enabled
        /// </summary>
        public virtual bool FeatureDemoInsuranceAccrualUiIsEnabled { get; set; }
        
        /// <summary>
        /// Feature: Is the HealthEquity demo functionality enabled
        /// </summary>
        public virtual bool FeatureDemoHealthEquityIsEnabled { get; set; }

        /// <summary>
        /// When enabled, VpTransactionType will be evaluated upon a downward financed visit 
        /// balance change to determine if the change should be applied as a payment to prinicipal. 
        /// Prevents Roborefunds when user makes a payment outside of visitpay. 
        /// </summary>
        public virtual bool FeatureExternalFinancePlanPaymentEnabled { get; set; }

        /// <summary>
        /// When enabled, this will show the list of facilities in a grid on the Contact Us page.
        /// </summary>
        public virtual bool FeatureFacilityContactUsGridIsEnabled { get; set; }

        /// <summary>
        /// Is the extended finance plan offer set featured enabled in the client portal
        /// </summary>
        public virtual bool FeatureFpOfferSetTypesIsEnabledClient { get; set; }

        /// <summary>
        /// Feature: Hide Redacted Visits Enabled
        /// </summary>
        public virtual bool FeatureHideRedactedVisitsIsEnabled { get; set; }

        /// <summary>
        /// Feature: Is Household Balance Enabled
        /// </summary>
        public virtual bool FeatureHouseholdBalanceIsEnabled { get; set; }

        /// <summary>
        /// ACH payments will get settled immediately after a successful payment processor response is received
        /// </summary>
        public virtual bool FeatureOptimisticAchSettlementIsEnabled { get; set; }

        /// <summary>
        /// Allow the AgingTier of a visit to be reset when moving from inactive to active and previously max age.
        /// </summary>
        public virtual bool FeatureResetAgingTierForReactivatedMaxAgeVisits { get; set; }

        /// <summary>
        /// Feature: Is Payment API Enabled
        /// </summary>
        public virtual bool FeaturePaymentApiIsEnabled { get; set; }

        /// <summary>
        /// Feature: Is Payment Summary Enabled
        /// </summary>
        public virtual bool FeaturePaymentSummaryIsEnabled { get; set; }

        /// <summary>
        /// If Enabled, the registration page shows a redirect to GuestPay for international users. Coupled with AppGuarantorExpressUrlHost (must have a value) and GuestPayAllowInternationalPayments.
        /// </summary>
        public virtual bool FeatureRedirectInternationalUserRegistration { get; set; }

        /// <summary>
        /// Feature: Is Reschedule Recurring Payment Enabled (Client)
        /// </summary>
        public bool FeatureRescheduledRecurringIsEnabledClient { get; set; }

        /// <summary>
        /// Feature: Is Reschedule Recurring Payment Enabled (Guarantor)
        /// </summary>
        public bool FeatureRescheduledRecurringIsEnabledGuarantor { get; set; }

        /// <summary>
        /// Feature: Is Itemization Enabled
        /// </summary>
        public virtual bool FeatureItemizationIsEnabled { get; set; }

        /// <summary>
        /// Allows paper mail delivery of communications
        /// </summary>
        public virtual bool FeatureMailIsEnabled { get; set; }

        /// <summary>
        /// Is Page Help Enabled
        /// </summary>
        public virtual bool FeaturePageHelpIsEnabled { get; set; }

        /// <summary>
        /// Feature: Is Consolidation Visit Masking Enabled
        /// </summary>
        public virtual bool FeatureConsolidationMaskingIsEnabled { get; set; }

        /// <summary>
        /// Feature: Is Demo Alert Enabled ... also requires AppSetting to be enabled
        /// </summary>
        public virtual bool FeatureDemoIsAlertEnabled { get; set; }

        /// <summary>
        /// Feature: Upon registration a user will receive a notification if any existing visits are in bad debt
        /// </summary>
        public virtual bool FeatureShowBadDebtNotificationIsEnabled { get; set; }

        /// <summary>
        /// When enabled, allows a finance plan meeting specific criteria to be setup without agreeing to the full RIC
        /// </summary>
        public virtual bool FeatureSimpleTermsIsEnabled { get; set; }

        /// <summary>
        /// Avoid showing transactions that have been acknowledged by hospital but have not been cleared.
        /// </summary>
        public virtual bool FeatureSuppressDuplicatePrincipalPaymentsInUi { get; set; }

        /// <summary>
        /// A user will receive a text with payment information and the user can reply to pay. Coupled with Application setting FeatureSmsEnabled.
        /// </summary>
        public virtual bool FeatureTextToPayIsEnabled { get; set; }

        /// <summary>
        /// VisitPay Client Services can upload documents and clients can view them.
        /// </summary>
        public virtual bool FeatureVisitPayDocumentIsEnabled { get; set; }

        /// <summary>
        /// When enabled, This will allow a refund of interest for paying off a finance plan in good standing. Dependent setting: FinancePlanRefundInterestIncentiveEnabled. VP-3938
        /// </summary>
        public virtual bool FinancePlanRefundInterestIncentiveEnabled { get; set; }

        /// <summary>
        /// Number of days before due date to send the final past due notification
        /// </summary>
        public virtual int FinalPastDueNotificationDaysBeforeDueDate { get; set; }

        /// <summary>
        /// Configuration that triggers behavior for the feature "Finance Plan Refund InterestIncentive" (FinancePlanRefundInterestIncentiveEnabled) VP-3938
        /// </summary>
        public FinancePlanIncentiveConfigurationDto FinancePlanIncentiveConfigurationDto { get; set; }

        /// <summary>
        /// Financial Assistance Support Phone Number
        /// </summary>
        public virtual string FinancialAssistanceSupportPhone { get; set; }

        /// <summary>
        /// Financial Assistance Url
        /// </summary>
        public virtual string FinancialAssistanceUrl { get; set; }

        /// <summary>
        /// show the payment logo in the footer instead of the client''s logo
        /// </summary>
        public virtual bool FooterShowPaymentLogo { get; set; }

        /// <summary>
        /// The number of grace period days
        /// </summary>
        public virtual int GracePeriodLength { get; set; }

        /// <summary>
        /// Unlock user account if user logging in with correct username and password after n minutes from account lockout.
        /// </summary>
        public virtual int GuarantorAutoUnlockTime { get; set; }

        /// <summary>
        /// Number of seconds to delay during registration processing
        /// </summary>
        public virtual int GuarantorEnrollmentProviderDelaySeconds { get; set; }

        /// <summary>
        /// Is the landing page enabled
        /// </summary>
        public virtual bool GuarantorIsLandingPageEnabled { get; set; }

        /// <summary>
        /// Whether client choose Strong or Weak Password
        /// </summary>
        public virtual bool GuarantorIsStrongPassword { get; set; }

        /// <summary>
        /// Number of failed login attempts for lockout
        /// </summary>
        public virtual int GuarantorMaxFailedAttemptCount { get; set; }

        /// <summary>
        /// Password expiry limit in days
        /// </summary>
        public virtual int GuarantorPasswordExpLimitInDays { get; set; }

        /// <summary>
        /// Password shouldn't exceed atleast x characters in length
        /// </summary>
        public virtual int GuarantorPasswordMaxLength { get; set; }

        /// <summary>
        /// Password should be atleast x characters long
        /// </summary>
        public virtual int GuarantorPasswordMinLength { get; set; }

        /// <summary>
        /// Are security questions required to recover username or reset password.
        /// </summary>
        public virtual bool GuarantorRequireSecurityQuestionsForRecovery { get; set; }

        /// <summary>
        /// Number of Security Questions
        /// </summary>
        public virtual int GuarantorSecurityQuestionsCount { get; set; }

        /// <summary>
        /// Session out time in minutes
        /// </summary>
        public virtual int GuarantorSessionTimeOutInMinutes { get; set; }

        /// <summary>
        /// Temporary password expiry limit in hours
        /// </summary>
        public virtual int GuarantorTempPasswordExpLimitInHours { get; set; }

        /// <summary>
        /// Unrepeatable password count
        /// </summary>
        public virtual int GuarantorUnrepeatablePasswordCount { get; set; }

        /// <summary>
        /// GuestPay Allow International Payments
        /// </summary>
        public virtual bool GuestPayAllowInternationalPayments { get; set; }

        /// <summary>
        /// Duration in minutes of temporary authentication lockout
        /// </summary>
        public virtual int GuestPayAuthenticationLockoutTimeInMinutes { get; set; }

        /// <summary>
        ///     GuestPay Client Brand Name
        /// </summary>
        public virtual string GuestPayClientBrandName { get; set; }

        /// <summary>
        /// GuestPay Display Current Balance
        /// </summary>
        public virtual bool GuestPayDisplayCurrentBalance { get; set; }

        /// <summary>
        /// guestpay legacy url
        /// </summary>
        public virtual string GuestPayLegacyUrl { get; set; }

        /// <summary>
        /// Maximum number of authentication attempts before temporary lockout
        /// </summary>
        public virtual int GuestPayMaxFailedAttemptCount { get; set; }

        /// <summary>
        ///     GuestPay session timeout in minutes
        /// </summary>
        public virtual int GuestPaySessionTimeOutInMinutes { get; set; }

        /// <summary>
        /// GuestPay support from email
        /// </summary>
        public virtual string GuestPaySupportFromEmail { get; set; }

        /// <summary>
        /// GuestPay support from name
        /// </summary>
        public virtual string GuestPaySupportFromName { get; set; }

        /// <summary>
        /// GuestPay support phone
        /// </summary>
        public virtual string GuestPaySupportPhone { get; set; }

        /// <summary>
        /// GuestPay Billing System
        /// </summary>
        public virtual string GuestPayBillingSystem { get; set; }
        /// <summary>
        /// Master feature toggle for HealthEquity
        /// </summary>
        public virtual bool HealthEquityEnabled { get; set; }
        /// <summary>
        /// This is if we should show the Balance or not on the guarantor widget
        /// </summary>
        public virtual bool HealthEquityShowBalance { get; set; }
        /// <summary>
        /// This is the outbound HQY SSO link on the guarantor widget
        /// </summary>
        public virtual bool HealthEquityShowOutboundSso { get; set; }
        /// <summary>
        /// This is the allowed set of InsurancePlanIds to allow the widget to show or not.
        /// </summary>
        public virtual List<string> HealthEquityAllowedInsuranceSsks { get; set; }


        /// <summary>
        /// HsGuarantor Patient Identifier
        /// </summary>
        public virtual string HsGuarantorPatientIdentifier { get; set; }

        /// <summary>
        /// HsGuarantor Patient Text
        /// </summary>
        public virtual string HsGuarantorPatientText { get; set; }

        /// <summary>
        /// Interest Calculation Method
        /// </summary>
        public virtual InterestCalculationMethodEnum InterestCalculationMethod { get; set; }

        /// <summary>
        /// Is Visit Pay Enabled
        /// </summary>
        public virtual bool IsVisitPayEnabled { get; set; }

        /// <summary>
        /// IvinciHealth Support From Email
        /// </summary>
        public virtual string IvinciSupportFromEmail { get; set; }

        /// <summary>
        /// Default language name
        /// </summary>
        public virtual string LanguageAppDefaultName { get; set; }
        
        /// <summary>
        /// Supported languages
        /// </summary>
        public virtual List<ClientCultureInfoDto> LanguageAppSupported { get; set; }

        /// <summary>
        /// Default language name
        /// </summary>
        public virtual string LanguageGuestPayDefaultName { get; set; }

        /// <summary>
        /// Supported languages
        /// </summary>
        public virtual List<ClientCultureInfoDto> LanguageGuestPaySupported { get; set; }

        /// <summary>
        /// Account number displayed on paper statement coupons for lock box deposits
        /// </summary>
        public virtual string LockBoxAccountNumber { get; set; }
        
        /// <summary>
        /// Display text for any masked values
        /// </summary>
        public virtual string MaskedReplacementValue { get; set; }
        
        /// <summary>
        /// MatchOptionIds to mask
        /// </summary>
        public virtual IList<int> MatchOptionMaskingConfiguration { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public virtual int MaxDaysInPendingAchApproval { get; set; }

        /// <summary>
        /// Max number of days in the future a guarantor can schedule a payment
        /// </summary>
        public virtual int MaxFuturePaymentIntervalInDays { get; set; }

        /// <summary>
        /// Feature: Is Single Sign-On Enabled
        /// </summary>
        public virtual bool MyChartSsoIsEnabled { get; set; }

        /// <summary>
        /// Display text for obfuscated visit data
        /// </summary>
        public virtual string RedactedVisitReplacementValue { get; set; }

        /// <summary>
        /// Maximum number of attempts to process a payment
        /// </summary>
        public virtual int PaymentMaxAttemptCount { get; set; }

        /// <summary>
        /// Payment reconciliation cut off time
        /// </summary>
        public virtual DateTime PaymentReconciliationCutoffTime { get; set; }

        /// <summary>
        /// Number of days prior to Scheduled Payment Date to send reminder. ("Payment is Scheduled for 'n' days from today.")
        /// </summary>
        public virtual int PendingPaymentReminderDays { get; set; }

        /// <summary>
        /// Start time for payment processing (i.e. last time guarantor can change payment)
        /// </summary>
        public virtual string ProcessPaymentStartTime { get; set; }

        /// <summary>
        /// Registration guarantor identifier text
        /// </summary>
        public virtual string RegistrationGuarantorIdentifierText { get; set; }

        /// <summary>
        /// Registration guarantor identifier text for description (not abbreviated)
        /// </summary>
        public virtual string RegistrationGuarantorIdentifierTextLong { get; set; }

        /// <summary>
        /// personal information fields to hide or ignore for vpg-hsg matching on registration
        /// </summary>
        public virtual string RegistrationMatchConfiguration { get; set; }

        /// <summary>
        /// Days until registration is processed for a new guarantor
        /// </summary>
        public virtual int RegistrationPendingDays { get; set; }

        /// <summary>
        /// Configuration string for the registration id field(s)
        /// </summary>
        public virtual string RegistrationIdConfiguration { get; set; }

        /// <summary>
        /// Address used for accepting payments via mail
        /// </summary>
        public virtual AddressDto RemitMailingAddress { get; set; }

        /// <summary>
        /// Maximum number of times a guarantor can reschedule a recurring payment
        /// </summary>
        public virtual int RescheduleRecurringPaymentMaximum { get; set; }

        /// <summary>
        /// Number of days for which the RescheduleRecurringPaymentMaximum applies
        /// </summary>
        public virtual int RescheduleRecurringPaymentWindow { get; set; }

        /// <summary>
        /// Address used for accepting returned mail
        /// </summary>
        public virtual AddressDto ReturnMailingAddress { get; set; }

        /// <summary>
        /// NSF/Returned checks fee amount displayed on paper statement coupons for lock box deposits
        /// </summary>
        public virtual decimal ReturnedPaymentPenaltyFeeAmount { get; set; }

        /// <summary>
        /// Self pay primary insurance type id
        /// </summary>
        public virtual int SelfPayPrimaryInsuranceTypeId { get; set; }

        /// <summary>
        /// Send grid web hook password used to verify email events
        /// </summary>
        public virtual string SendGridPassword { get; set; }

        /// <summary>
        /// Send grid web hook username used to verify email events
        /// </summary>
        public virtual string SendGridUserName { get; set; }

        /// <summary>
        /// Settlement window begin ach
        /// </summary>
        public virtual DateTime SettlementWindowBeginAch { get; set; }

        /// <summary>
        /// Settlement window begin credit card
        /// </summary>
        public virtual DateTime SettlementWindowBeginCc { get; set; }

        /// <summary>
        /// Settlement window end ach
        /// </summary>
        public virtual DateTime SettlementWindowEndAch { get; set; }

        /// <summary>
        /// Settlement window end credit card
        /// </summary>
        public virtual DateTime SettlementWindowEndCc { get; set; }
        
        /// <summary>
        /// Display name for Simple Payment Plan terms
        /// </summary>
        public virtual string SimpleTermsAgreementName { get; set; }

        /// <summary>
        /// Maximum months to allow a simple terms agreement
        /// </summary>
        public virtual int SimpleTermsMaximumMonths { get; set; }

        /// <summary>
        /// Base path of client's SSRS reports
        /// </summary>
        public virtual string SsrsReportPath { get; set; }

        /// <summary>
        /// Statement generation days
        /// </summary>
        public virtual int StatementGenerationDays { get; set; }

        /// <summary>
        /// Email address used when sending emails
        /// </summary>
        public virtual string SupportFromEmail { get; set; }

        /// <summary>
        /// Name used in footer of support emails
        /// </summary>
        public virtual string SupportFromName { get; set; }

        /// <summary>
        /// Support phone number
        /// </summary>
        public virtual string SupportPhone { get; set; }

        /// <summary>
        /// Suppor request prefix for cases
        /// </summary>
        public virtual string SupportRequestCasePrefix { get; set; }

        /// <summary>
        /// Support request review days
        /// </summary>
        public virtual int SupportRequestReviewDays { get; set; }

        /// <summary>
        /// Uncollectable max aging count
        /// </summary>
        public virtual int UncollectableMaxAgingCountFinancePlans { get; set; }

        /// <summary>
        /// VpGuarantor Patient Identifier
        /// </summary>
        public virtual string VpGuarantorPatientIdentifier { get; set; }

        /// <summary>
        /// VpGuarantor Patient Text
        /// </summary>
        public virtual string VpGuarantorPatientText { get; set; }

        /// <summary>
        /// Wait Period To Reactive Account
        /// </summary>
        public virtual int WaitPeriodToReactivateAccount { get; set; }

        /// <summary>
        /// Feature: Ability to choose between hard or soft visit unmatching
        /// </summary>
        public virtual bool FeatureVisitUnmatchingMethodSelection { get; set; }
        
        /// <summary>
        /// Milliseconds to wait before drawing attention to the help center widget
        /// </summary>
        public virtual int HelpCenterWidgetAlertDelay { get; set; }

        /// <summary>
        /// Base URL for ChatGateway API
        /// </summary>
        public virtual string ChatGatewayApiUrl { get; set; }

        /// <summary>
        /// Base URL for ChatGateway sockets
        /// </summary>
        public virtual string ChatGatewaySocketUrl { get; set; }

        /// <summary>
        /// Base URL for relay-web-app embed iframe
        /// </summary>
        public virtual string ChatIframeUrl { get; set; }

        /// <summary>
        /// JWT Signing key for ChatGateway API
        /// </summary>
        public virtual string ChatGatewayJwtSigningKey { get; set; }

        /// <summary>
        /// Number of days from today to push out the default payment due date suggested to a user
        /// </summary>
        public virtual int PaymentDueDateDefaultDaysInFuture { get; set; }

        /// <summary>
        /// Theme client accent color
        /// </summary>
        public virtual string ThemeClientAccentColor { get; set; }

        /// <summary>
        /// Endpoint in ClamAV Proxy API to use for malware scanning
        /// </summary>
        public virtual string ClamAvScanApiEndpoint { get; set; }

        /// <summary>
        /// Client Name for Trace Api requests
        /// </summary>
        public virtual string TraceApiClientName { get; set; }
    }
}

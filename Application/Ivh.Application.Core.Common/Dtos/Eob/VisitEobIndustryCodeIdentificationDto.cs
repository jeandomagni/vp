﻿namespace Ivh.Application.Core.Common.Dtos.Eob
{
    public class VisitEobIndustryCodeIdentificationDto
    {
        public string IndustryCode { get; set; }
        public string Description { get; set; }
    }
}
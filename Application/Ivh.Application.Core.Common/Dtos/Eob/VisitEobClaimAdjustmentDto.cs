﻿namespace Ivh.Application.Core.Common.Dtos.Eob
{
    public class VisitEobClaimAdjustmentDto
    {
        public decimal MonetaryAmount { get; set; }
        public string UIDisplay { get; set; }
    }
}
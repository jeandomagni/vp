﻿using System;
namespace Ivh.Application.Core.Common.Dtos.Eob
{
    public class EobIndicatorResultDto
    {
        public int BillingSystemId { get; set; }
        public string VisitSourceSystemKey { get; set; }
        public bool HasEob { get; set; }
        public virtual string LogoUrl { get; set; }
    }
}

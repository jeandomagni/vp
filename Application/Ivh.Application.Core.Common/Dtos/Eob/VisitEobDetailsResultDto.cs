﻿namespace Ivh.Application.Core.Common.Dtos.Eob
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

public class VisitEobDetailsResultDto
    {

        public VisitEobDetailsResultDto()
        {
            this.ClaimLevelClaimAdjustments = new List<VisitEobClaimAdjustmentDto>();
            this.ServicePaymentInformations = new List<VisitEobServicePaymentInformationDto>();
        }

        public int HeaderNumber { get; set; }
        public string ClaimNumber { get; set; }
        public string PlanNumber { get; set; }
        public string VisitSourceSystemKey { get; set; }
        public int VisitBillingSystemId { get; set; }
        public string PayerName { get; set; }
        public string PayerLinkUrl { get; set; }
        public string PayerIcon { get; set; }
        public string ClaimStatusCode { get; set; }
        public decimal TotalClaimChargeAmount { get; set; }
        public decimal ClaimPaymentAmount { get; set; }
        public decimal PatientResponsibilityAmount { get; set; }

        public DateTime TransactionDate { get; set; }
        public DateTime ServiceStartDate { get; set; }
        public DateTime ServiceEndDate { get; set; }
        public string MiaMoaClaimPaymentRemarkCode { get; set; }
        public string MiaMoaClaimPaymentRemarkDescription { get; set; }

        public IList<VisitEobClaimAdjustmentDto> ClaimLevelClaimAdjustments { get; set; }
        public IList<VisitEobServicePaymentInformationDto> ServicePaymentInformations { get; set; }

        public IList<VisitEobIndustryCodeIdentificationDto> HealthCareRemarkCodes
        {
            get
            {
                IList<VisitEobIndustryCodeIdentificationDto> remarkCodes = this.ServicePaymentInformations.SelectMany(x => x.LqHealthCareRemarkCodesUnique)
                    .GroupBy(x => x.IndustryCode)
                    .Select(x => x.First())
                    .ToList();

                if (this.MiaMoaClaimPaymentRemarkCode != null)
                {
                    remarkCodes.Add(new VisitEobIndustryCodeIdentificationDto
                    {
                        IndustryCode = this.MiaMoaClaimPaymentRemarkCode,
                        Description = this.MiaMoaClaimPaymentRemarkDescription
                    });
                }
                return remarkCodes.OrderBy(x => x.IndustryCode).ToList();
            }
        }

        public IList<VisitEobClaimAdjustmentDto> ClaimLevelClaimAdjustmentsSummed
        {
            get
            {
                return this.ClaimLevelClaimAdjustments
                    .GroupBy(x => x.UIDisplay)
                    .Select(g => new VisitEobClaimAdjustmentDto { UIDisplay = g.Key, MonetaryAmount = g.Sum(e => e.MonetaryAmount) })
                    .ToList();
            }
        }

        public IList<VisitEobClaimAdjustmentDto> ServiceLevelClaimAdjustmentsSummed
        {
            get
            {
                return this.ServicePaymentInformations.SelectMany(x => x.ClaimAdjustmentsSummed)
                 .GroupBy(x => x.UIDisplay)
                 .Select(g => new VisitEobClaimAdjustmentDto()
                 {
                     UIDisplay = g.Key,
                     MonetaryAmount = g.Sum(a => a.MonetaryAmount),
                 }).ToList();
            }
        }
    }
}

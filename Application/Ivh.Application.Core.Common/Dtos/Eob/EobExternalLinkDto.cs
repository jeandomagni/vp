﻿namespace Ivh.Application.Core.Common.Dtos.Eob
{
    public class EobExternalLinkDto
    {
        public virtual int EobExternalLinkId { get; set; }
        public virtual string Url { get; set; }
        public virtual string Text { get; set; }
        public virtual string Icon { get; set; }
        public virtual int PersistDays { get; set; }
        public virtual string ClaimNumberLocation { get; set; }
    }
}
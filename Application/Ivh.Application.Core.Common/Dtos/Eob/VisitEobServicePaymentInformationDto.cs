﻿namespace Ivh.Application.Core.Common.Dtos.Eob
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    public class VisitEobServicePaymentInformationDto
    {
        public IList<VisitEobClaimAdjustmentDto> ClaimAdjustments { get; set; }
        public DateTime ServiceStartDate { get; set; }
        public DateTime ServiceEndDate { get; set; }
        public IList<VisitEobIndustryCodeIdentificationDto> LqHealthCareRemarkCodes { get; set; }

        public virtual IList<VisitEobIndustryCodeIdentificationDto> LqHealthCareRemarkCodesUnique
        {
            get
            {
                return this.LqHealthCareRemarkCodes
                    .GroupBy(x => x.IndustryCode)
                    .Select(x => x.First())
                    .ToList();
            }
        }

        public virtual IList<VisitEobClaimAdjustmentDto> ClaimAdjustmentsSummed
        {
            get
            {
                return this.ClaimAdjustments
                    .GroupBy(x => x.UIDisplay)
                    .Select(g => new VisitEobClaimAdjustmentDto { UIDisplay = g.Key, MonetaryAmount = g.Sum(e => e.MonetaryAmount) })
                    .ToList();
            }
        }
    }
}

﻿namespace Ivh.Application.Core.Common.Dtos
{
    using ProtoBuf;

    [ProtoContract]
    public class KnowledgeBaseCategoryQuestionAnswersDto : KnowledgeBaseFlattenedDto<KnowledgeBaseQuestionAnswerDto>
    {
    }
}

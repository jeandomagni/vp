﻿namespace Ivh.Application.Core.Common.Dtos
{
    using System.Collections.Generic;

    public class VisitUnmatchResultsDto
    {
        public IReadOnlyList<VisitUnmatchDto> VisitUnmatches { get; set; }
        public int TotalRecords { get; set; }
    }
}
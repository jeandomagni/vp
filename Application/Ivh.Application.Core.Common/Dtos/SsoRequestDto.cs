﻿namespace Ivh.Application.Core.Common.Dtos
{
    using System;
    using System.Runtime.Serialization;
    using Ivh.Common.Base.Enums;
    using Ivh.Common.VisitPay.Enums;

    [Serializable]
    [DataContract]
    public class SsoRequestDto
    {
        [DataMember(Order = 1)]
        public string EncryptedPayload { get; set; }
        [DataMember(Order = 2)]
        public SsoProviderEnum ProviderEnum { get; set; }
    }
}
﻿namespace Ivh.Application.Core.Common.Dtos
{
    using System.Collections.Generic;
    using ProtoBuf;

    [ProtoContract]
    public class KnowledgeBaseCategoryQuestionsDto : KnowledgeBaseFlattenedDto<KnowledgeBaseQuestionDto>
    {
    }
}
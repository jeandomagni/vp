﻿namespace Ivh.Application.Core.Common.Dtos
{
    public class SamlResponseDto
    {
        public string Url { get; set; }
        public string Value { get; set; }
        public string RelayState { get; set; }
    }
}

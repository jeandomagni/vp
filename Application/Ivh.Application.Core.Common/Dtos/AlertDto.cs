﻿namespace Ivh.Application.Core.Common.Dtos
{
    using System;
    using System.Runtime.Serialization;
    using Ivh.Common.Base.Enums;
    using Ivh.Common.VisitPay.Enums;

    [Serializable]
    [DataContract]
    public class AlertDto
    {
        public int AlertId { get; set; }
        public AlertTypeEnum AlertType { get; set; }
        public AlertCriteriaEnum AlertCriteria { get; set; }
        public CmsRegionEnum CmsRegion { get; set; }
        public int DisplayOrder { get; set; }
        public bool IsActive { get; set; }
        public string ContentBody { get; set; }
        public string ContentTitle { get; set; }
        public  bool IsDismissable { get; set; }
    }
}
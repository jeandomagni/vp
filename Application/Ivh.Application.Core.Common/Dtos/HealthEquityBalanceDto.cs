﻿namespace Ivh.Application.Core.Common.Dtos
{
    using System;
    using System.Runtime.Serialization;
    using ProtoBuf;

    [Serializable]
    [DataContract]
    [ProtoContract]
    public class HealthEquityBalanceDto
    {
        [DataMember(Order = 0)]
        [ProtoMember(1, DynamicType = true)]
        public decimal CashBalance { get; set; }

        [DataMember(Order = 1)]
        [ProtoMember(2, DynamicType = true)]
        public decimal InvestmentBalance { get; set; }

        [DataMember(Order = 2)]
        [ProtoMember(3, DynamicType = true)]
        public decimal ContributionsYtd { get; set; }

        [DataMember(Order = 3)]
        [ProtoMember(4, DynamicType = true)]
        public decimal DistributionsYtd { get; set; }

        [DataMember(Order = 4)]
        [ProtoMember(5)]
        public DateTime LastUpdated { get; set; }

        public decimal TotalBalance => this.CashBalance + this.InvestmentBalance;
    }
}
﻿namespace Ivh.Application.Core.Common.Dtos
{
    using System;
    using Ivh.Common.Base.Enums;
    using Ivh.Common.VisitPay.Enums;
    using User.Common.Dtos;

    public class VisitUnmatchDto
    {
        public string HsGuarantorSourceSystemKey { get; set; }
        public HsGuarantorUnmatchReasonEnum HsGuarantorUnmatchReasonEnum { get; set; }
        public string HsGuarantorUnmatchReasonDisplayName { get; set; }
        public string MatchedSourceSystemKey { get; set; }
        public DateTime? UnmatchActionDate { get; set; }
        public string UnmatchActionNotes { get; set; }
        public VisitPayUserDto UnmatchActionVisitPayUser { get; set; }
        public GuarantorDto VpGuarantor { get; set; }

        public string HsGuarantorSourceSystemKeyCurrent { get; set; }
        public GuarantorDto VpGuarantorCurrent { get; set; }
    }
}
﻿namespace Ivh.Application.Core.Common.Dtos
{
    using ProtoBuf;

    [ProtoContract]
    public class KnowledgeBaseQuestionDto : BaseKnowledgeBaseQuestionDto
    {
        [ProtoMember(1)]
        public int KnowledgeBaseSubCategoryId { get; set; }
    }
}
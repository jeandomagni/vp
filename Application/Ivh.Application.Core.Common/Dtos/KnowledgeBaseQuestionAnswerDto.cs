﻿namespace Ivh.Application.Core.Common.Dtos
{
    using Ivh.Common.Base.Enums;
    using Ivh.Common.VisitPay.Enums;
    using ProtoBuf;

    [ProtoContract]
    public class KnowledgeBaseQuestionAnswerDto: BaseKnowledgeBaseQuestionDto
    {
        [ProtoMember(1)]
        public CmsRegionEnum AnswerCmsRegion { get; set; }

        [ProtoMember(2)]
        public string AnswerContentBody { get; set; }

        [ProtoMember(3)]
        public string AnswerContentTitle { get; set; }
    }
}
﻿namespace Ivh.Application.Core.Common.Dtos
{
    using System;

    public class SsoPossibleVpGuarantorResult
    {
        public int VpGuarantorID { get; set; }
        public int HsGuarantorId { get; set; }

        public int? SingleSignOnVerificationId { get; set; }
        public DateTime DOB { get; set; }
    }
}
﻿namespace Ivh.Application.Core.Common.Dtos
{
    public class VisitPayDocumentFilter
    {
        public int? VisitPayDocumentId { get; set; }

        public bool IsApproved { get; set; }
        
        
        #region jqGrid fields
        public int Page { get; set; } = 1;

        public int Rows { get; set; } = 25;

        public string SortField { get; set; }

        public string SortOrder { get; set; }
        #endregion


        public bool CanSort => !string.IsNullOrEmpty(this.SortField) && !string.IsNullOrEmpty(this.SortOrder);

        public bool CanPage => (this.Page > 0 && this.Rows > 0);
    }
    
    
    
}

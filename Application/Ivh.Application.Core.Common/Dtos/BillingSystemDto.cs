﻿namespace Ivh.Application.Core.Common.Dtos
{
    public class BillingSystemDto
    {
        public virtual int BillingSystemId { get; set; }
        public virtual string BillingSystemName { get; set; }
    }
}

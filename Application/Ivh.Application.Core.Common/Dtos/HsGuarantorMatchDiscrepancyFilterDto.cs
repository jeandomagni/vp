﻿namespace Ivh.Application.Core.Common.Dtos
{
    using System;
    using Ivh.Common.Base.Enums;
    using Ivh.Common.VisitPay.Enums;

    public class HsGuarantorMatchDiscrepancyFilterDto
    {
        public HsGuarantorMatchDiscrepancyStatusEnum? HsGuarantorMatchDiscrepancyStatus { get; set; }

        public int? DateRangeFrom { get; set; }

        public DateTime? SpecificDateFrom { get; set; }

        public DateTime? SpecificDateTo { get; set; }

        public string HsGuarantorSourceSystemKey { get; set; }

        public int? VpGuarantorId { get; set; }

        public string SortField { get; set; }

        /// <summary>
        /// "asc" or "desc", not case sensitive
        /// </summary>
        public string SortOrder { get; set; }

    }
}

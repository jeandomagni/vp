﻿namespace Ivh.Application.Core.Common.Dtos
{
    using System;

    public class SsoVerificationResult
    {
        public int SingleSignOnVerificationId { get; set; }
        public string SingleSignOn_SourceSystemKey { get; set; }
        public DateTime DOB { get; set; }
        public int VpGuarantorId { get; set; }
    }
}
﻿namespace Ivh.Application.Core.Common.Dtos
{
    using System.Collections.Generic;

    public class VisitAgingHistoryResultsDto
    {
        public IReadOnlyList<VisitAgingHistoryDto> VisitAgingHistories { get; set; }
        public int TotalRecords { get; set; }
    }
}

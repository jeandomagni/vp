﻿namespace Ivh.Application.Core.Common.Dtos
{
    public class SsoCanRedirectToSsoResult
    {
        public bool CanRedirect { get; set; }
        public bool HasAcceptedTerms { get; set; }
        public string ErrorMessage { get; set; }
        public string UrlToRedirect { get; set; }
        public string AccessToken { get; set; }
    }
}
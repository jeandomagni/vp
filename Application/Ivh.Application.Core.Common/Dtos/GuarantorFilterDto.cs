﻿namespace Ivh.Application.Core.Common.Dtos
{
    using System;
    using Ivh.Common.Base.Utilities.Helpers;

    public class GuarantorFilterDto
    {
        public string HsGuarantorId { get; set; }
        public int? VpGuarantorId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Ssn4 { get; set; }
        public DateTime? DOB { get; set; }
        public string UserNameOrEmailAddress { get; set; }
        public string SortField { get; set; }
        public string SortOrder { get; set; }
        public string LastNameFirstName => FormatHelper.LastNameFirstName(this.LastName, this.FirstName);
        public string FirstNameLastName => FormatHelper.FirstNameLastName(this.FirstName, this.LastName);

        public string PhoneNumber { get; set; }
        public string AddressStreet1 { get; set; }
        public string AddressStreet2 { get; set; }
        public string City { get; set; }
        public string StateProvince { get; set; }
        public string PostalCode { get; set; }
        public decimal? AmountDue { get; set; }
    }
}

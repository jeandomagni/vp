﻿namespace Ivh.Application.Core.Common.Dtos
{
    using System.Collections.Generic;

    public class VisitTransactionResultsDto
    {
        public IReadOnlyList<VisitTransactionDto> VisitTransactions { get; set; }
        public int TotalRecords { get; set; }
    }
}

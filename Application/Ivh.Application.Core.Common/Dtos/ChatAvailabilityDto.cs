﻿
namespace Ivh.Application.Core.Common.Dtos
{
    using System;

    public class ChatAvailabilityDto
    {
        public int ChatAvailabilityId { get; set; }
        public int? DayOfWeek { get; set; }
        public DateTime? Date { get; set; }
        public TimeSpan? TimeStart { get; set; }
        public TimeSpan? TimeEnd { get; set; }
    }
}

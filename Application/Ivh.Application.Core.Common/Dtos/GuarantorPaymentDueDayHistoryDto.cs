﻿namespace Ivh.Application.Core.Common.Dtos
{
    using System;
    using User.Common.Dtos;

    public class GuarantorPaymentDueDayHistoryDto
    {
        public int PaymentDueDay { get; set; }
        public int OldPaymentDueDay { get; set; }
        public VisitPayUserDto InsertUser { get; set; }
        public DateTime InsertDate { get; set; }
        public string ChangeReason { get; set; }
    }
}
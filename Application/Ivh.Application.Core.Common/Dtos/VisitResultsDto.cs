﻿namespace Ivh.Application.Core.Common.Dtos
{
    using System.Collections.Generic;

    public class VisitResultsDto
    {
        public IReadOnlyList<VisitDto> Visits { get; set; }
        public int TotalRecords { get; set; }
    }
}
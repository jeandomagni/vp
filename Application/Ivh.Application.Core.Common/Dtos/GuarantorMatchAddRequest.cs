﻿namespace Ivh.Application.Core.Common.Dtos
{
    public class GuarantorMatchAddRequest : GuarantorMatchRequest
    {
        public int VpGuarantorId { get; set; }
    }
}

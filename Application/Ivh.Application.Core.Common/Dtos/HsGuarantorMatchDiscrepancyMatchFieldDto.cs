﻿namespace Ivh.Application.Core.Common.Dtos
{
    using System;

    public class HsGuarantorMatchDiscrepancyMatchFieldDto
    {
        public string HsGuarantorMatchDiscrepancyMatchFieldName { get; set; }
        public string HsGuarantorMatchDiscrepancyMatchFieldCurrentValue { get; set; }
        public string HsGuarantorMatchDiscrepancyMatchFieldNewValue { get; set; }

        public bool IsDiscrepancy => !(this.HsGuarantorMatchDiscrepancyMatchFieldCurrentValue ?? string.Empty).Equals((this.HsGuarantorMatchDiscrepancyMatchFieldNewValue ?? string.Empty), StringComparison.OrdinalIgnoreCase);
    }
}

﻿namespace Ivh.Application.Core.Common.Dtos
{
    using System.Collections.Generic;
    using Ivh.Common.Base.Enums;
    using Ivh.Common.VisitPay.Enums;
    using User.Common.Dtos;

    public class HsGuarantorUnmatchRequestDto
    {
        public int? HsGuarantorMatchDiscrepancyId { get; set; }
        public int VpGuarantorId { get; set; }
        public IList<string> HsGuarantorSourceSystemKeys { get; set; }
        public string Notes { get; set; }
        public VisitPayUserDto ActionVisitPayUser { get; set; }
        public UnmatchActionTypeEnum UnmatchActionType { get; set; }
    }
}
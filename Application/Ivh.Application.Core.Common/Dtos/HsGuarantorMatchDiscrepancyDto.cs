﻿namespace Ivh.Application.Core.Common.Dtos
{
    using System;
    using System.Collections.Generic;
    using Ivh.Common.Base.Enums;
    using Ivh.Common.VisitPay.Enums;
    using User.Common.Dtos;

    public class HsGuarantorMatchDiscrepancyDto
    {
        public HsGuarantorMatchDiscrepancyDto()
        {
            this.HsGuarantorMatchDiscrepancyMatchFields = new List<HsGuarantorMatchDiscrepancyMatchFieldDto>();
        }

        public int HsGuarantorMatchDiscrepancyId { get; set; }
        public DateTime InsertDate { get; set; }
        public GuarantorDto VpGuarantor { get; set; }
        public string HsGuarantorSourceSystemKey { get; set; }
        public int? HsGuarantorVpGuarantorId { get; set; }
        public HsGuarantorMatchDiscrepancyStatusEnum HsGuarantorMatchDiscrepancyStatus { get; set; }
        public string HsGuarantorMatchDiscrepancyStatusName { get; set; }
        public DateTime? ActionDate { get; set; }
        public VisitPayUserDto ActionVisitPayUser { get; set; }
        public string ActionNotes { get; set; }
        public IList<HsGuarantorMatchDiscrepancyMatchFieldDto> HsGuarantorMatchDiscrepancyMatchFields { get; set; }
        public HsGuarantorUnmatchReasonEnum HsGuarantorUnmatchReasonEnum { get; set; }
        public string HsGuarantorUnmatchReasonName { get; set; }
        public string HsGuarantorUnmatchReasonDisplayName { get; set; }
    }
}

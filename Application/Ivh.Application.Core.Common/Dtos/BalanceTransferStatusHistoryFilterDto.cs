﻿namespace Ivh.Application.Core.Common.Dtos
{
    public class BalanceTransferStatusHistoryFilterDto
    {
        public int GuarantorVisitPayUserId { get; set; }

        public int VisitId { get; set; }

        public int Page { get; set; }

        public int Rows { get; set; }
    }
}

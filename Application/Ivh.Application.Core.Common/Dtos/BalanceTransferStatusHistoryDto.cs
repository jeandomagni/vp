﻿namespace Ivh.Application.Core.Common.Dtos
{
    using System;
    using User.Common.Dtos;

    public class BalanceTransferStatusHistoryDto
    {
        public virtual VisitDto Visit { get; set; }
        public virtual DateTime InsertDate { get; set; }
        public virtual BalanceTransferStatusDto BalanceTransferStatus { get; set; }
        public virtual int BalanceTransferStatusHistoryId { get; set; }
        public virtual VisitPayUserDto ChangedBy { get; set; }
        public virtual string Notes { get; set; }
    }
}

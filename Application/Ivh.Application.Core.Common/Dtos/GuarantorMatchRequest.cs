﻿namespace Ivh.Application.Core.Common.Dtos
{
    using Ivh.Common.VisitPay.Enums;
    using Ivh.Common.VisitPay.Messages.Matching.Dtos;

    public class GuarantorMatchRequest
    {
        public MatchDataDto MatchData { get; set; }
    }
}

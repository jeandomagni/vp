﻿namespace Ivh.Application.Core.Common.Dtos
{
    using System;
    using System.Collections.Generic;
    using Ivh.Common.Base.Enums;
    using Ivh.Common.VisitPay.Enums;

    public class SurveyResultDto
    {
        public int SurveyResultId { get; set; }
        public int SurveyId { get; set; }
        public SurveyResultTypeEnum SurveyResultType { get; set; }
        public DateTime SurveyDate { get; set; }
        public int? VisitPayUserId { get; set; }
        public int? FinancePlanId { get; set; }
        public int? VpStatementId { get; set; }
        public int? PaymentId { get; set; }
        public string SurveyComment { get; set; }
        public List<SurveyResultAnswerDto> SurveyResultAnswers { get; set; }
        public string DeviceType { get; set; }
    }
}
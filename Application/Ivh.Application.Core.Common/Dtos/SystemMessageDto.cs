﻿namespace Ivh.Application.Core.Common.Dtos
{
    using System;
    using Ivh.Common.Base.Enums;
    using Ivh.Common.VisitPay.Enums;

    public class SystemMessageDto
    {
        public int SystemMessageId { get; set; }
        public SystemMessageTypeEnum SystemMessageType { get; set; }
        public SystemMessageCriteriaEnum? SystemMessageCriteria { get; set; }
        public CmsRegionEnum CmsRegion { get; set; }
        public int DisplayOrder { get; set; }
        public DateTime? DateActive { get; set; }
        public DateTime? DateExpires { get; set; }

        public string ContentBody { get; set; }
        public string ContentTitle { get; set; }
        public bool MessageRead { get; set; }
    }
}
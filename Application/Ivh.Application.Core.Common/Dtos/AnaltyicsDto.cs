﻿namespace Ivh.Application.Core.Common.Dtos
{
    public class AnaltyicsDto
    {
        public int AnalyticsId { get; set; }
        public string BaseUrl { get; set; }
    }
}
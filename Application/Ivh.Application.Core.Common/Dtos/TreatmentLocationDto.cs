﻿namespace Ivh.Application.Core.Common.Dtos
{
    public class TreatmentLocationDto
    {
        public int TreatmentLocationId { get; set; }
        public string TreatmentLocationName { get; set; }
        public bool IsActive { get; set; }
    }
}
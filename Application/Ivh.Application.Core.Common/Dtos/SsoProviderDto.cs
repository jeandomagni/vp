﻿namespace Ivh.Application.Core.Common.Dtos
{
    using Ivh.Common.Base.Enums;
    using Ivh.Common.VisitPay.Enums;

    public class SsoProviderDto
    {
        public int SsoProviderId { get; set; }
        public string SsoProviderName { get; set; }
        public string SsoProviderDisplayName { get; set; }
        public bool IsEnabled { get; set; }
        public CmsRegionEnum SsoTermsCmsRegion { get; set; }
        public string SourceSystemKeyLabel { get; set; }
        public bool EnableFromManageSso { get; set; }
        public string ShortDescription { get; set; }
        public string SsoProviderSourceSystemKey { get; set; }
        public string SsoProviderSecretSettingsKey { get; set; }
        public string SsoProviderSecret { get; set; }
        public string Callback { get; set; }
        public string Scope { get; set; }
        public string AuthorizePath { get; set; }
        public string TokenPath { get; set; }
        public string LoginPath { get; set; }
        public string LogoutPath { get; set; }
        public string AuthorizationServerBaseAddress { get; set; }
        public virtual SsoProviderEnum SsoProviderEnum { get; set; }
    }
}
﻿namespace Ivh.Application.Core.Common.Dtos
{
    using System;
    public class VisitPayGuarantorAuditLogDto
    {
        public virtual int UserEventHistoryId { get; set; }
        public virtual DateTime EventDate { get; set; } 
        public virtual string UserName { get; set; } 
        public virtual int UserId { get; set; } 
        public virtual string IpAddress { get; set; } 
        public virtual string UserAgent { get; set; } 
        public virtual int EmulateUserId { get; set; } 
        public virtual int VpGuarantorId { get; set; } 
        public virtual string EventType { get; set; } 
    }
}
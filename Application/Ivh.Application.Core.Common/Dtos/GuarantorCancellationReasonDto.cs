﻿namespace Ivh.Application.Core.Common.Dtos
{
    public class GuarantorCancellationReasonDto
    {
        public int VpGuarantorCancellationReasonId { get; set; }
        public string Name { get; set; }
        public string DisplayName { get; set; }
        public int DisplaySortOrder { get; set; }
        public bool Selectable { get; set; }
    }
}

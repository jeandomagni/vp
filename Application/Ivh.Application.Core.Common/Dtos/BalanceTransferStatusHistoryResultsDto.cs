﻿namespace Ivh.Application.Core.Common.Dtos
{
    using System.Collections.Generic;

    public class BalanceTransferStatusHistoryResultsDto
    {
        public IReadOnlyList<BalanceTransferStatusHistoryDto> BalanceTransferStatusHistories { get; set; }
        public int TotalRecords { get; set; }
    }
}

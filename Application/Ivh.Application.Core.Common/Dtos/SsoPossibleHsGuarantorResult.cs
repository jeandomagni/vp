﻿namespace Ivh.Application.Core.Common.Dtos
{
    using System;
    using System.Runtime.Serialization;
    using ProtoBuf;

    [Serializable]
    [DataContract]
    [ProtoContract]
    public class SsoPossibleHsGuarantorResult
    {
        [DataMember(Order = 1)]
        [ProtoMember(1)]
        public int HsGuarantorId { get; set; }
        [DataMember(Order = 2)]
        [ProtoMember(2)]
        public string HsGuarantorSourceSystemKey { get; set; }
        [DataMember(Order = 3)]
        [ProtoMember(3)]
        public int HsGuarantorHsBillingSystemID { get; set; }

    }
}
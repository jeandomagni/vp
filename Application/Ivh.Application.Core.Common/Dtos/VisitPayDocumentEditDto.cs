﻿namespace Ivh.Application.Core.Common.Dtos
{
    using System;
    using Ivh.Common.VisitPay.Enums;

    public class VisitPayDocumentEditDto
    {
        public int VisitPayDocumentId { get; set; }
        public int? ApprovedByVpUserId { get; set; }
        public DateTime? ActiveDate { get; set; }
        public string DisplayTitle { get; set; }
        public DateTime? ApprovedDate { get; set; }
        public VisitPayDocumentTypeEnum VisitPayDocumentType { get; set; }
    }
}
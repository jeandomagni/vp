﻿namespace Ivh.Application.Core.Common.Dtos
{
    using System;

    public class PaymentSummaryFilterDto
    {
        public DateTime? PostDateBegin { get; set; }
        public DateTime? PostDateEnd { get; set; }
    }
}
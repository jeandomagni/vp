﻿namespace Ivh.Application.Core.Common.Dtos
{
    using System.Collections.Generic;

    public class GuarantorResultsDto
    {
        public IReadOnlyList<GuarantorDto> Guarantors { get; set; }
        public int TotalRecords { get; set; }
    }
}
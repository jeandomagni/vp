﻿namespace Ivh.Application.Core.Common.Dtos
{
    public class VisitTransactionSummaryDto
    {
        public decimal Charges { get; set; }
        public decimal Interest { get; set; }
        public decimal Other { get; set; }
        public decimal Insurance { get; set; }
        public decimal AllPayments { get; set; }
    }
}
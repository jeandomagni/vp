﻿namespace Ivh.Application.Core.Common.Dtos
{
    using System;
    using Ivh.Common.Base.Enums;
    using Ivh.Common.VisitPay.Enums;

    public class AuditRoleChangeDto
    {
        public int VisitPayUserId { get; set; }
        public string UserName { get; set; }
        public string Email { get; set; }
        public int VisitPayRoleId { get; set; }
        public int ChangeEventId { get; set; }
        public int ExpiredChangeEventId { get; set; }
        public DateTime ChangeTime { get; set; }
        public DateTime ExpiredTime { get; set; }
        public DateTime PriorDelete { get; set; }
        public JournalEventTypeEnum JournalEventTypeId { get; set; }
    }
}
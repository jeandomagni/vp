﻿namespace Ivh.Application.Core.Common.Dtos
{
    public class FacilityDto
    {
        public virtual int FacilityId { get; set; }
        public virtual string LogoFilename { get; set; }
        public virtual string FacilityDescription { get; set; }
        public virtual string SourceSystemKey { get; set; }
        public virtual StateDto RicState { get; set; }
        public virtual string CityState { get; set; }
        public virtual string ContactPhone { get; set; }
        public virtual string FacilityHours { get; set; }
    }
}
﻿using Ivh.Common.Base.Utilities.Helpers;

namespace Ivh.Application.Core.Common.Dtos
{
    public class VisitPatientDto
    {
        public virtual int HSVisitPatientID { get; set; }
        public virtual string FirstName { get; set; }
        public virtual string LastName { get; set; }
        public virtual string Address1 { get; set; }
        public virtual string Address2 { get; set; }
        public virtual string City { get; set; }
        public virtual string State { get; set; }
        public virtual string Zip { get; set; }
        public virtual string HomePhone { get; set; }
        public virtual string WorkPhone { get; set; }
        public string LastNameFirstName => FormatHelper.LastNameFirstName(this.LastName, this.FirstName);
        public string FirstNameLastName => FormatHelper.FirstNameLastName(this.FirstName, this.LastName);
    }
}

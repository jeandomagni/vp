﻿namespace Ivh.Application.Core.Common.Dtos
{
    using System.Collections.Generic;
    using Ivh.Common.Base.Enums;

    public class VisitAgingHistoryFilterDto
    {
        public VisitAgingHistoryFilterDto()
        {
        }

        public int VisitId { get; set; }
        public string SortField { get; set; }

        /// <summary>
        /// "asc" or "desc", not case sensitive
        /// </summary>
        public string SortOrder { get; set; }
    }
}


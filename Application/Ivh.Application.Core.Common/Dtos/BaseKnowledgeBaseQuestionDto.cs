﻿namespace Ivh.Application.Core.Common.Dtos
{
    using System.Collections.Generic;
    using Ivh.Common.Base.Enums;
    using Ivh.Common.VisitPay.Enums;
    using ProtoBuf;

    [ProtoContract]
    [ProtoInclude(500, typeof(KnowledgeBaseQuestionAnswerDto))]
    [ProtoInclude(600, typeof(KnowledgeBaseQuestionDto))]
    public class BaseKnowledgeBaseQuestionDto
    {
        [ProtoMember(1)]
        public int KnowledgeBaseQuestionAnswerId { get; set; }

        [ProtoMember(2)]
        public CmsRegionEnum QuestionCmsRegion { get; set; }

        [ProtoMember(3)]
        public string QuestionContentBody { get; set; }

        [ProtoMember(4)]
        public string QuestionContentTitle { get; set; }

        [ProtoMember(5)]
        public bool IsMobileKnowledgeBase { get; set; }

        [ProtoMember(6)]
        public bool IsWebKnowledgeBase { get; set; }

        [ProtoMember(7)]
        public bool IsClientKnowledgeBase { get; set; }

        [ProtoMember(8)]
        public bool IsActive { get; set; }

        [ProtoMember(9)]
        public int DisplayOrder { get; set; }

        [ProtoMember(10)]
        public IList<string> Tags { get; set; }

        [ProtoMember(11)]
        public IList<string> Routes { get; set; }

        [ProtoMember(12)]
        public int Rank { get; set; }
        
    }
}
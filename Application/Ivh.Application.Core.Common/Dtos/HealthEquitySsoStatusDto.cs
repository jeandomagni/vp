﻿
namespace Ivh.Application.Core.Common.Dtos
{
    using System;
    using System.Runtime.Serialization;
    using ProtoBuf;

    [Serializable]
    [DataContract]
    [ProtoContract]
    public class HealthEquitySsoStatusDto
    {
        [DataMember(Order = 0)]
        [ProtoMember(1)]
        public bool IsHqySsoUp { get; set; }

        [DataMember(Order = 1)]
        [ProtoMember(2)]
        public DateTime LastUpdated { get; set; }
    }
}

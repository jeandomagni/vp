﻿namespace Ivh.Application.Core.Common.Dtos
{
    using System;
    using Ivh.Common.VisitPay.Enums;

    public class VisitStateHistoryDto
    {
        public int HistoryId { get; set; }
        public VisitDto Entity { get; set; }
        public VisitStateEnum InitialState { get; set; }
        public VisitStateEnum EvaluatedState { get; set; }
        public DateTime DateTime  { get; set; }
        public string RuleName { get; set; }
    }
}       
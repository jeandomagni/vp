﻿namespace Ivh.Application.Core.Common.Dtos
{
    using System;
    using Ivh.Common.Base.Enums;
    using Ivh.Common.Base.Utilities.Helpers;
    using Ivh.Common.VisitPay.Enums;

    public class AuditUserChangeDetailDto
    {
        public DateTime ChangeTime { get; set; }
        public DateTime ExpiredTime { get; set; }
        public int VisitPayUserId { get; set; }
        public string PriorUserName { get; set; }
        public string UserName { get; set; }
        public string PriorEmail { get; set; }
        public string Email { get; set; }
        public string PriorFirstName { get; set; }
        public string FirstName { get; set; }
        public string PriorLastName { get; set; }
        public string LastName { get; set; }
        public bool PriorLockoutEnabled { get; set; }
        public bool LockoutEnabled { get; set; }
        public DateTime PriorLockoutEndDateUtc { get; set; }
        public DateTime LockoutEndDateUtc { get; set; }
        public JournalEventTypeEnum JournalEventTypeId { get; set; }
        public string LastNameFirstName => FormatHelper.LastNameFirstName(this.LastName, this.FirstName);
        public string FirstNameLastName => FormatHelper.FirstNameLastName(this.FirstName, this.LastName);
        public string PriorLastNameFirstName => FormatHelper.LastNameFirstName(this.PriorLastName, this.PriorFirstName);
        public string PriorFirstNameLastName => FormatHelper.FirstNameLastName(this.PriorFirstName,this.PriorLastName);
    }
}
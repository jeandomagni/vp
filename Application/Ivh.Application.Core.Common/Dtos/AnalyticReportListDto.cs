﻿namespace Ivh.Application.Core.Common.Dtos
{
    using System.Collections.Generic;

    public class AnalyticReportListDto
    {
        public List<AnalyticReportDto> AnalyticReports { get; set; }
    }
}
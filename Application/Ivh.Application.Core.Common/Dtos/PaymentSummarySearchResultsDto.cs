﻿namespace Ivh.Application.Core.Common.Dtos
{
    using System.Collections.Generic;

    public class PaymentSummarySearchResultsDto
    {
        public PaymentSummarySearchResultsDto()
        {
            this.Visits = new List<PaymentSummaryGroupedDto>();
        }

        public decimal SummaryBalanceTotal { get; set; }
        public int TotalRecords { get; set; }
        public IList<PaymentSummaryGroupedDto> Visits { get; set; }
    }
}
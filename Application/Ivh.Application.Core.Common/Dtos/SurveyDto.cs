﻿namespace Ivh.Application.Core.Common.Dtos
{
    using System.Collections.Generic;
    using Ivh.Common.Base.Enums;
    using Ivh.Common.VisitPay.Enums;

    public class SurveyDto
    {
        public int SurveyId { get; set; }
        public string SurveyName { get; set; }
        public string Title { get; set; }
        public  CmsRegionEnum TitleCmsRegion { get; set; }
        public bool UseNavigationCount { get; set; }
        public List<SurveyResultAnswerDto> SurveyQuestions { get; set; }
    }
}

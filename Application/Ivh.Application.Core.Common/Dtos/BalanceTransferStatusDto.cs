﻿namespace Ivh.Application.Core.Common.Dtos
{
    public class BalanceTransferStatusDto
    {
        public virtual int BalanceTransferStatusId { get; set; }
        public virtual string BalanceTransferStatusName { get; set; }
        public virtual string BalanceTransferStatusDisplayName { get; set; }
    }
}

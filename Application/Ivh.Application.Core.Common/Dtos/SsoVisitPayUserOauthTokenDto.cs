﻿namespace Ivh.Application.Core.Common.Dtos
{
    using System;
    using Ivh.Common.Base.Enums;
    using Ivh.Common.VisitPay.Enums;

    public class SsoVisitPayUserOauthTokenDto
    {
        public int SsoVisitPayUserOauthTokenId { get; set; }
        public OAuthTokenTypeEnum TokenType { get; set; }
        public string Token { get; set; }
        public string Ticket { get; set; }
        public int VisitPayUserId { get; set; }
        public DateTime ExpiresOn { get; set; }
        public DateTime InsertDate { get; set; }
        public bool IsActive { get; set; }
    }
}
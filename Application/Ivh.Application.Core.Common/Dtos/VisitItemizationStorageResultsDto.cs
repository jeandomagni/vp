﻿namespace Ivh.Application.Core.Common.Dtos
{
    using System.Collections.Generic;

    public class VisitItemizationStorageResultsDto
    {
        public IReadOnlyList<VisitItemizationStorageDto> Itemizations { get; set; }
        public int TotalRecords { get; set; }
    }
}
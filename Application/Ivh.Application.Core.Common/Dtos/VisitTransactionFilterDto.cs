﻿namespace Ivh.Application.Core.Common.Dtos
{
    using System.Collections.Generic;
    using Ivh.Common.VisitPay.Enums;

    public class VisitTransactionFilterDto
    {
        public string TransactionDateRangeFrom { get; set; }

        public string TransactionDateRangeTo { get; set; }

        public IList<VpTransactionTypeFilterEnum> TransactionType { get; set; }

        public int? VisitId { get; set; }

        public int? VpGuarantorId { get; set; }

        public string SortField { get; set; }

        /// <summary>
        /// "asc" or "desc", not case sensitive
        /// </summary>
        public string SortOrder { get; set; }
    }
}

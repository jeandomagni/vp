﻿namespace Ivh.Application.Core.Common.Dtos
{
	public class StateDto
	{
		public virtual string StateCode { get; set; }
		public virtual int CmsRegionId { get; set; }
	}
}

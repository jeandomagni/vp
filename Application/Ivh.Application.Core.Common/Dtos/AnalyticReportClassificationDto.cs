﻿namespace Ivh.Application.Core.Common.Dtos
{
    public class AnalyticReportClassificationDto
    {
        public int AnalyticReportClassificationId { get; set; }
        public string ClassificationName { get; set; }
    }
}
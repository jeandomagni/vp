﻿namespace Ivh.Application.Core.Common.Dtos
{
    public class VisitResult
    {
        public int VisitId { get; set; }
        public int GuarantorId { get; set; }
    }
}
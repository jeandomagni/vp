namespace Ivh.Application.Core.Common.Dtos
{
    using System;
    using System.Collections.Generic;
    using System.Runtime.Serialization;
    using Ivh.Common.Base.Enums;
    using ProtoBuf;
    using Ivh.Common.Base.Utilities.Helpers;
    using Ivh.Common.VisitPay.Enums;

    [Serializable]
    [DataContract]
    [ProtoContract]
    public class SsoResponseDto
    {
        /// <summary>
        /// This is the case when there are Possible VPG's that are registered found.
        /// If this is false: do not show them the special login page, go to the regular non-logged in path
        /// If this is true: take them to the special login page
        /// 
        /// Note: I think this trumps the Registration path
        /// </summary>
        [DataMember(Order = 1)]
        [ProtoMember(1)]
        public bool FoundRegisteredMatches { get; set; }

        /// <summary>
        /// This is the case when the matched HS guarantors can sign up
        /// If this is false: Take them to the regular non-logged in path
        /// If this is true: Take them to the special registration path
        /// </summary>
        [DataMember(Order = 2)]
        [ProtoMember(2)]
        public bool FoundMatchesThatCouldBeRegistered { get; set; }
        
        /// <summary>
        /// The have a match and can log them in if this is true.  
        /// if true: VpGuarantorId should also have a value if this is true
        /// If false: look at FoundRegisteredMatches & FoundMatchesThatCouldBeRegistered
        /// </summary>
        [DataMember(Order = 3)]
        [ProtoMember(3)]
        public bool AcceptedTheSsoAgreement { get; set; }
        
        [DataMember(Order = 4)]
        [ProtoMember(4)]
        public int? VpGuarantorId { get; set; }
        
        [DataMember(Order = 5)]
        [ProtoMember(5)]
        public DateTime DateOfBirth { get; set; }
        
        [DataMember(Order = 6)]
        [ProtoMember(6)]
        public string FirstName { get; set; }
        
        [DataMember(Order = 7)]
        [ProtoMember(7)]
        public string LastName { get; set; }

        [ProtoMember(8)]
        [DataMember(Order = 8)]
        public string SsoSourceSystemKey { get; set; }
        
        [DataMember(Order = 9)]
        [ProtoMember(9)]
        public bool IsError { get; set; }

        [DataMember(Order = 10)]
        [ProtoMember(10)]
        public SignInStatusExEnum SignInStatus { get; set; }

        [DataMember(Order = 11)]
        [ProtoMember(11)]
        public IList<SsoPossibleHsGuarantorResult> MatchesThatCouldBeRegistered { get; set; }

        [DataMember(Order = 12)]
        [ProtoMember(12)]
        public SsoProviderEnum ProviderEnum { get; set; }

        [DataMember(Order = 13)]
        [ProtoMember(13)]
        public SsoResponseErrorEnum ErrorEnum { get; set; }
        
        [DataMember(Order = 14)]
        [ProtoMember(14)]
        public string UserName { get; set; }

        [IgnoreDataMember]
        [ProtoIgnore]
        public string LastNameFirstName => FormatHelper.LastNameFirstName(this.LastName, this.FirstName);

        [IgnoreDataMember]
        [ProtoIgnore]
        public string FirstNameLastName => FormatHelper.FirstNameLastName(this.FirstName, this.LastName);
    }
}
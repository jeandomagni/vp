﻿namespace Ivh.Application.Core.Common.Dtos
{
    using System.Collections.Generic;

    public class SurveyRatingGroupDto
    {
        public  int SurveyRatingGroupId { get; set; }
        public  string SurveyRatingGroupName { get; set; }
        public  List<SurveyRatingDto> SurveyRatings { get; set; }
    }
}
﻿namespace Ivh.Application.Core.Common.Dtos
{
    using System;
    using Base.Common.Dtos;
    using Base.Common.Interfaces;
    using Base.Common.Interfaces.Entities.Visit;
    using Ivh.Common.Base.Utilities.Helpers;
    using Ivh.Common.VisitPay.Attributes;
    using Ivh.Common.VisitPay.Constants;
    using Ivh.Common.VisitPay.Enums;
    
    public class VisitDto : 
        VisitBalanceBaseDto, 
        IVisitBalanceDto,
        IVisit,
        IMaskConsolidated,
        IMaskMatching
    {
        public int BillingSystemId { get; set; }
        public string SourceSystemKey { get; set; }
        public string SourceSystemKeyDisplay { get; set; }
        public string BillingApplication { get; set; }
        public string BillingApplicationText => string.Equals(this.BillingApplication, BillingApplicationConstants.HB, StringComparison.CurrentCultureIgnoreCase) ? "Hospital Services" : "Physician Services";
        
        [Mask]
        public string VisitDescription { get; set; }
        
        public string PatientFirstName { get; set; }
        public string PatientLastName { get; set; }
        public string PatientName { get; set; }
        public string GuarantorName { get; set; }
        public DateTime? DischargeDate { get; set; }
        public DateTime? AdmitDate { get; set; }
        public DateTime? VisitDisplayDateUnformatted { get; set; }
        public string VisitDisplayDate { get; set; }
        public decimal? TotalCharges { get; set; }
        public decimal? HsCurrentBalance { get; set; }
        public int AgingCount { get; set; }
        public string MatchedSourceSystemKey { get; set; }
        public VisitStateEnum CurrentVisitState { get; set; }
        public VisitStateDerivedEnum CurrentVisitStateDerivedEnum { get; set; }
        public GuarantorDto VPGuarantor { get; set; }
        public int? ServiceGroupId { get; set; }
        public bool? HasFinancialAssistance { get; set; }
        public int VpGuarantorId => this.VPGuarantor?.VpGuarantorId ?? 0;
        public string PatientLastNameFirstName => FormatHelper.LastNameFirstName(this.PatientLastName, this.PatientFirstName);
        public string PatientFirstNameLastName => FormatHelper.FirstNameLastName(this.PatientFirstName, this.PatientLastName);
        public BalanceTransferStatusDto BalanceTransferStatus { get; set; }
        public bool IsUnmatched { get; set; }
        public bool IsMaxAge { get; set; }
        public bool IsMaxAgeActive { get; set; }
        public bool IsMaxAgeInactive { get; set; }
        public bool BillingHold { get; set; }
        public virtual bool VpEligible { get; set; }
        public virtual bool Redact { get; set; }
        public virtual int?  AgingTierId { get; set; }//NOT SURE THIS GOES HERE. Pending Aging Service Definition.
        /// <summary>
        /// The visit is non-financed and past due. This is set by an application service after visit load.
        /// </summary>
        public bool IsPastDue { get; set; }
        public bool IsFinalPastDue { get; set; }
        public decimal InterestPaid { get; set; }
        public FacilityDto Facility { get; set; }

        #region masking indicators
        
        public bool IsMaskedConsolidated { get; protected set; }
        public MatchOptionEnum? MatchOption { get; protected set; }

        #endregion

    }
}

﻿namespace Ivh.Application.Core.Common.Dtos
{
    using Ivh.Common.Base.Utilities.Helpers;

    public class HsGuarantorFilterDto
    {
        public string HsGuarantorIds { get; set; }
        public string SourceSystemKey { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Ssn4 { get; set; }
        public string DOB { get; set; }
        public string SortField { get; set; }
        public string SortOrder { get; set; }
        public int Page { get; set; }
        public int Rows { get; set; }
        public string LastNameFirstName => FormatHelper.LastNameFirstName(this.LastName, this.FirstName);
        public string FirstNameLastName => FormatHelper.FirstNameLastName(this.FirstName, this.LastName);

        //VP fields for validation purposes
        public int? VpGuarantorId { get; set; }
        public string UserNameOrEmailAddress { get; set; }

        public bool IsValid()
        {
            return this.ContainsHsSearchCriteria() && !this.ContainVisitPaySearchCriteria();
        }

        private bool ContainVisitPaySearchCriteria()
        {
            // VPNG-18995: When VP only field is used (VPGID, username) only return results from VP
            return !(this.VpGuarantorId == null && string.IsNullOrWhiteSpace(this.UserNameOrEmailAddress));
        }

        private bool ContainsHsSearchCriteria()
        {
            return !string.IsNullOrWhiteSpace(this.SourceSystemKey) ||
                   !string.IsNullOrWhiteSpace(this.FirstName) ||
                   !string.IsNullOrWhiteSpace(this.LastName) ||
                   !string.IsNullOrWhiteSpace(this.Ssn4) ||
                   !string.IsNullOrWhiteSpace(this.DOB);
        }
    }
}
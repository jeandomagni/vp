﻿namespace Ivh.Application.Core.Common.Dtos
{
    using System;
    using Content.Common.Dtos;
    using Ivh.Common.Base.Enums;
    using Ivh.Common.VisitPay.Enums;
    using User.Common.Dtos;

    public class SsoVisitPayUserSettingDto
    {
        public int SsoVisitPayUserSettingId { get; set; }
        public VisitPayUserDto VisitPayUser { get; set; }
        public SsoProviderDto SsoProvider { get; set; }
        public SsoStatusEnum SsoStatus { get; set; }
        public VisitPayUserDto ActionVisitPayUser { get; set; }
        public DateTime ActionDateTime { get; set; }
        public CmsVersionDto SsoTermsCmsVersion { get; set; }
        public bool IsIgnored { get; set; }
        public string Scope { get; set; }
        public string RefreshToken { get; set; }
        public string AccessToken { get; set; }
    }
}
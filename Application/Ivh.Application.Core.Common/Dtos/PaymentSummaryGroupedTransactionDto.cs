﻿namespace Ivh.Application.Core.Common.Dtos
{
    using System;
    using Base.Common.Interfaces;
    using Ivh.Common.VisitPay.Enums;

    public class PaymentSummaryGroupedTransactionDto :
        IMaskConsolidated,
        IMaskMatching
    {
        public DateTime PostDate { get; set; }
        public decimal TransactionAmount { get; set; }
        public PaymentSummaryGroupedDto Visit { get; set; }

        #region masking indicators
        
        public bool IsMaskedConsolidated => this.Visit?.IsMaskedConsolidated ?? false;

        public MatchOptionEnum? MatchOption => this.Visit?.MatchOption;

        public int VpGuarantorId => this.Visit?.VpGuarantorId ?? 0;

        #endregion
    }
}
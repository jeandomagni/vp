﻿namespace Ivh.Application.Core.Common.Dtos
{
    using User.Common.Dtos;

    public class GuarantorCancellationDto
    {
        public int VpGuarantorCancellationId { get; set; }
        public GuarantorDto Guarantor { get; set; }
        public VisitPayUserDto VisitPayUser { get; set; }
        public GuarantorCancellationReasonDto GuarantorCancellationReason { get; set; }
        public string Notes { get; set; }
    }
}

﻿namespace Ivh.Application.Core.Common.Dtos
{
    using System;
    using Base.Common.Interfaces;
    using Ivh.Common.VisitPay.Attributes;
    using Ivh.Common.VisitPay.Enums;

    public class VisitTransactionDto : 
        IMaskConsolidated,
        IMaskMatching
    {
        public int VisitTransactionId { get; set; }
        public int VisitId { get; set; }
        public int VpGuarantorId { get; set; }
        public DateTime TransactionDate { get; set; }
        public DateTime PostDate { get; set; }
        public decimal TransactionAmount { get; set; }
        public string TransactionType { get; set; }
        [Mask]
        public string TransactionDescription { get; set; }
        public VisitDto Visit { get; set; }
        public int TransactionCodeBillingSystemId { get; set; }
        public string TransactionCodeSourceSystemKey { get; set; }
        public bool CanIgnore { get; set; }
        public  DateTime DisplayDate { get; set; }

        #region masking indicators
        
        public bool IsMaskedConsolidated { get; protected set; }
        public MatchOptionEnum? MatchOption { get; protected set; }

        #endregion
    }
}

﻿namespace Ivh.Application.Core.Common.Dtos
{
    public class ExternalLinkDto
    {
        public int ExternalLinkId { get; set; }
        public string Url { get; set; }
        public string Icon { get; set; }
        public string Text { get; set; }
    }
}
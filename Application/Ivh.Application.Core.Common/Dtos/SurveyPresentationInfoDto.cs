﻿namespace Ivh.Application.Core.Common.Dtos
{

    // Originally, Surveys were presented by name
    // As of VP-3212, a trigger for a survey can have multiple surveys 
    // When asking the question if the user should be presented 
    // with a survey we need to ask what survey of that group of surveys 
    // should be shown
    public class SurveyPresentationInfoDto
    {
        public bool UserIsPresentedWithSurvey { get; set; }
        public string SurveyName { get; set; }
    }
}
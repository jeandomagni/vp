﻿namespace Ivh.Application.Core.Common.Dtos
{
    using System.Collections.Generic;

    public class VisitFilterDto
    {
        public IList<int> VisitStateIds { get; set; }
        public int? VisitPayUserId { get; set; }
        public string BillingApplication { get; set; }
        public string VisitDateRangeFrom { get; set; }
        public string VisitDateRangeTo { get; set; }

        public string SortField { get; set; }

        /// <summary>
        /// "asc" or "desc", not case sensitive
        /// </summary>
        public string SortOrder { get; set; }

        public bool? IsUnmatched { get; set; } 
        public bool? IsMaxAge { get; set; }
        public bool? BillingHold { get; set; }
        public bool? IsPastDue { get; set; }
        public int? AgingCountMin { get; set; }
        public int? AgingCountMax { get; set; }
        public bool? IsOnActiveFinancePlan { get; set; }
        public int? FinancePlanId { get; set; }
    }
}

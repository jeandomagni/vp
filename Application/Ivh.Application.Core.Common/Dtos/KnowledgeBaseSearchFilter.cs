﻿namespace Ivh.Application.Core.Common.Dtos
{
    using System.Collections.Generic;

    public class KnowledgeBaseSearchFilter
    {
        public KnowledgeBaseSearchFilter()
        {
            this.Questions = new List<KnowledgeBaseSearchQuestion>();
        }

        public IList<KnowledgeBaseSearchQuestion> Questions { get; set; }
    }

    public class KnowledgeBaseSearchQuestion
    {
        public int CategoryId { get; set; }
        public int SubCategoryId { get; set; }
        public int QuestionId { get; set; }
    }
}

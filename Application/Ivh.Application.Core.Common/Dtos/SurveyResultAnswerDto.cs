﻿namespace Ivh.Application.Core.Common.Dtos
{
    using Ivh.Common.Base.Enums;
    using Ivh.Common.VisitPay.Enums;

    public class SurveyResultAnswerDto
    {
        public int SurveyResultAnswerId { get; set; }
        public SurveyResultDto SurveyResult{ get; set; }
        public int SurveyQuestionId { get; set; }
        public int SurveyRatingGroupId { get; set; }
        public string QuestionText { get; set; }
        public CmsRegionEnum QuestionCmsRegion { get; set; }
        public int? SurveyQuestionRating { get; set; }
        public string SurveyRatingDescriptionText { get; set; }
        public SurveyRatingGroupDto SurveyRatingGroup { get; set; }
    }
}
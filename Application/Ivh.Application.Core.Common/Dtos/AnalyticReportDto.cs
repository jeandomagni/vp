﻿namespace Ivh.Application.Core.Common.Dtos
{
    using System;

    [Serializable]
    public class AnalyticReportDto
    {
        public string ReportName { get; set; }
        public string ReportId { get; set; }
        public string DisplayName { get; set; }
        public string DatasetId { get; set; }
        public string EmbedUrl { get; set; }
        public string GroupId { get; set; }
        public string Classification { get; set; }
    }
}
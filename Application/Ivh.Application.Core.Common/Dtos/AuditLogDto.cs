﻿namespace Ivh.Application.Core.Common.Dtos
{
    using System;
    using Ivh.Common.Base.Enums;
    using Ivh.Common.VisitPay.Enums;

    public class AuditLogDto
    {
        public DateTime AuditDate { get; set; }
        public int VisitPayUserId { get; set; }
        public string UserName { get; set; }
        public JournalEventTypeEnum EventType { get; set; }
        public string AuditType { get; set; }
        public DateTime? BeginDate { get; set; }
        public DateTime? EndDate { get; set; }

        public string AuditPeriod
        {
            get
            {
                return string.Format(
                    "{0} - {1}",
                    BeginDate.HasValue ? BeginDate.Value.ToLocalTime().ToString("G") : "unknown",
                    EndDate.HasValue ? EndDate.Value.ToLocalTime().ToString("G") : "unknown"
                    );
            }
        }
        public string Notes { get; set; }
    }
}
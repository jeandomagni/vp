﻿namespace Ivh.Application.Core.Common.Dtos
{
    using System.Collections.Generic;
    using ProtoBuf;

    [ProtoContract]
    public class KnowledgeBaseSubCategoryDto
    {
        [ProtoMember(1)]
        public int KnowledgeBaseSubCategoryId { get; set; }

        [ProtoMember(2)]
        public int KnowledgeBaseCategoryId { get; set; }

        [ProtoMember(3)]
        public string Name { get; set; }

        [ProtoMember(4)]
        public IList<KnowledgeBaseQuestionAnswerDto> QuestionAnswers { get; set; } = new List<KnowledgeBaseQuestionAnswerDto>();

        [ProtoMember(5)]
        public bool IsActive { get; set; }

        [ProtoMember(6)]
        public int DisplayOrder { get; set; }
    }
}
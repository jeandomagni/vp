﻿namespace Ivh.Application.Core.Common.Dtos
{
    using System.Collections.Generic;
    using System.Runtime.Serialization;
    using ProtoBuf;

    [ProtoContract]
    [ProtoInclude(500, typeof(KnowledgeBaseCategoryQuestionAnswersDto))]
    [ProtoInclude(600, typeof(KnowledgeBaseCategoryQuestionsDto))]
    public class KnowledgeBaseFlattenedDto<T> : BaseKnowledgeBaseCategoryDto where T : BaseKnowledgeBaseQuestionDto
    {
        public KnowledgeBaseFlattenedDto()
        {
            this.Questions = new List<T>();
        }
        [ProtoMember(1)]
        public IList<T> Questions { get; set; }
    }
}

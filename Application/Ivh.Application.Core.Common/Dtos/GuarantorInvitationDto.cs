﻿namespace Ivh.Application.Core.Common.Dtos
{
    using System;
    using Ivh.Common.Base.Utilities.Helpers;

    public class GuarantorInvitationDto
    {
        public string Email { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public int VisitPayUserId { get; set; }
        public Guid InvitationToken { get; set; }
        public string LastNameFirstName => FormatHelper.LastNameFirstName(this.LastName,this.FirstName);
        public string FirstNameLastName => FormatHelper.FirstNameLastName(this.FirstName,this.LastName);
        public string FullName => FormatHelper.FullName(this.FirstName, this.MiddleName, this.LastName);
    }
}
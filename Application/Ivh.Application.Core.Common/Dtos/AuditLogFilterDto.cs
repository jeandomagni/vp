﻿namespace Ivh.Application.Core.Common.Dtos
{
    using System;

    public class AuditLogFilterDto
    {
        public DateTime StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public string SortField { get; set; }
        public string SortOrder { get; set; }
    }
}
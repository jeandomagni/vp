﻿namespace Ivh.Application.Core.Common.Dtos
{
    using Ivh.Common.Base.Enums;
    using Ivh.Common.VisitPay.Enums;

    public class HsGuarantorMatchDiscrepancyStatusDto
    {
        public int HsGuarantorMatchDiscrepancyStatusId { get; set; }
        public string HsGuarantorMatchDiscrepancyStatusName { get; set; }

        public HsGuarantorMatchDiscrepancyStatusEnum HsGuarantorMatchDiscrepancyStatusEnum
        {
            get { return (HsGuarantorMatchDiscrepancyStatusEnum) this.HsGuarantorMatchDiscrepancyStatusId; }
        }
    }
}
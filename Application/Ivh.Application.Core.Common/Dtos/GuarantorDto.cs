﻿namespace Ivh.Application.Core.Common.Dtos
{
    using System;
    using System.Collections.Generic;
    using Ivh.Common.Base.Enums;
    using Ivh.Common.VisitPay.Enums;
    using User.Common.Dtos;

    public class GuarantorDto
    {
        public virtual int VpGuarantorId { get; set; }
        public virtual int TermsOfUseCmsVersionId { get; set; }
        public virtual int PaymentDueDay { get; set; }
        public virtual int PaymentDueDayChanges { get; set; }
        public virtual DateTime? NextStatementDate { get; set; }
        public virtual DateTime RegistrationDate { get; set; }
        public virtual DateTime? AccountClosedDate { get; set; }
        public virtual DateTime? AccountCancellationDate { get; set; }
        public virtual DateTime? AccountReactivationDate { get; set; }
        public virtual VisitPayUserDto User { get; set; }
        public virtual VpGuarantorStatusEnum VpGuarantorStatus { get; set; }
        public virtual GuarantorTypeEnum VpGuarantorTypeEnum { get; set; }
        public virtual IList<string> HsGuarantorsSourceSystemKeys { get; set; }
        public virtual GuarantorConsolidationStatusEnum ConsolidationStatus { get; set; }
        public virtual bool IsConsolidated { get; set; }
        public virtual IList<int> ManagedGuarantorIds { get; set; }
        public virtual int? ManagingGuarantorId { get; set; }
        public virtual bool IsManaged { get;set; }
        public virtual bool IsOfflineGuarantor { get; set; }
        public virtual string StatementAccountNumber { get; set; }
        public virtual bool UseAutoPay { get; set; }
    }
}

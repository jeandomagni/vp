﻿namespace Ivh.Application.Core.Common.Dtos
{
    using Ivh.Common.Base.Enums;
    using Ivh.Common.VisitPay.Enums;

    public class SurveyRatingDto
    {
        public int SurveyRatingId { get; set; }
        public int SurveyRatingScore { get; set; }
        public string SurveyRatingDescription { get; set; }
        public  CmsRegionEnum DescriptionCmsRegion { get; set; }
        public  SurveyRatingGroupDto SurveyRatingGroup { get; set; }
    }
}
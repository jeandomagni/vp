﻿namespace Ivh.Application.Core.Common.Dtos
{
    using Ivh.Common.Base.Utilities.Helpers;
    using System;

    public class AuditUserAccessDto
    {
        public string UserName { get; set; }
        public string Email { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public DateTime EventDate { get; set; }
        public string EventType { get; set; }
        public string IpAddress { get; set; }
        public string LastNameFirstName => FormatHelper.LastNameFirstName(this.LastName, this.FirstName);
        public string FirstNameLastName => FormatHelper.FirstNameLastName(this.FirstName, this.LastName);
    }
}
﻿namespace Ivh.Application.Core.Common.Dtos
{
    using System.Collections.Generic;

    public class SsoProviderWithUserSettingsDto
    {
        public SsoProviderDto Provider { get; set; }
        public SsoVisitPayUserSettingDto UserSettings { get; set; }
        public IList<string> SourceSystemKey { get; set; }
    }
}
﻿namespace Ivh.Application.EventJournal.Services
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using AutoMapper;
    using Common;
    using Common.Dtos;
    using Common.Interfaces;
    using Domain.Base.Interfaces;
    using Domain.EventJournal.Entities;
    using Domain.EventJournal.Interfaces;
    using Domain.Guarantor.Interfaces;
    using Domain.User.Entities;
    using Domain.User.Interfaces;
    using Ivh.Common.Base.Utilities.Extensions;
    using Ivh.Common.Base.Utilities.Helpers;
    using Ivh.Common.VisitPay.Strings;
    using Ivh.Domain.Base.Services;
    using OfficeOpenXml;
    using OfficeOpenXml.Style;

    public class JournalEventQueryApplicationService : DomainService, IJournalEventQueryApplicationService
    {
        private readonly Lazy<IEventJournalQueryService> _journalEventQueryService;
        private readonly Lazy<IGuarantorService> _guarantorService;

        public JournalEventQueryApplicationService(
            Lazy<IEventJournalQueryService> journalEventQueryService,
            Lazy<IGuarantorService> guarantorService,
            Lazy<IDomainServiceCommonService> serviceCommonService
            ) : base(serviceCommonService)
        {
            this._journalEventQueryService = journalEventQueryService;
            this._guarantorService = guarantorService;
        }

        public JournalEventResultsDto GetJournalEvents(JournalEventFilterDto journalEventFilter)
        {
            return this.GetJournalEvents(journalEventFilter, false);
        }

        private JournalEventResultsDto GetJournalEventsForExport(JournalEventFilterDto journalEventFilter)
        {
            return this.GetJournalEvents(journalEventFilter, true);
        }

        private JournalEventResultsDto GetJournalEvents(JournalEventFilterDto journalEventFilterDto, bool forExport)
        {
            int recordCount = 0;
            try
            {
                JournalEventFilter journalEventFilter = Mapper.Map<JournalEventFilter>(journalEventFilterDto);
                recordCount = this._journalEventQueryService.Value.JournalEventCount(journalEventFilter);
                JournalEventResults results = this._journalEventQueryService.Value.GetJournalEvents(journalEventFilter);

                IEnumerable<JournalEventDto> journalEvents = results.JournalEvents.Select(je =>
                {
                    JournalEventDto log = new JournalEventDto()
                    {
                        JournalEventType = je.JournalEventType,
                        EventDateTime = je.EventDateTime,
                        EventVisitPayUserId = je.EventVisitPayUserId,
                        VpGuarantorId = je.VpGuarantorId ?? 0,
                        AdditionalData = je.AdditionalData,
                        JournalEventId = je.JournalEventId,
                        EventVisitPayUserIdUserName = je.EventVisitPayUser?.UserName ?? "",
                        VpGuarantorIdUserName = forExport ? 0 : je.VpGuarantorId ?? 0,
                        Message = je.Message
                    };
                    return log;
                });

                return new JournalEventResultsDto
                {
                    JournalEvents = journalEvents.ToList(),
                    TotalRecords = recordCount
                };
            }
            catch (Exception ex)
            {
                this.LoggingService.Value.Fatal(() => $"JournalEventQueryApplicationService::GetJournalEvents - exception = {ExceptionHelper.AggregateExceptionToString(ex)}");
            }

            return new JournalEventResultsDto
            {
                JournalEvents = new List<JournalEventDto>(),
                TotalRecords = recordCount
            };

        }

        public JournalEventDto GetJournalEventDetail(int userEventHistoryId)
        {
            JournalEventDto journalEventDto = Mapper.Map<JournalEventDto>(this._journalEventQueryService.Value.GetJournalEvent(userEventHistoryId));
            journalEventDto.VpGuarantorIdUserName = this._guarantorService.Value.GetGuarantorId(journalEventDto.EventVisitPayUserIdUserName);
            journalEventDto.VpGuarantorIdTargetUserId = journalEventDto.VisitPayUserId.HasValue ? this._guarantorService.Value.GetGuarantorEx(journalEventDto.VisitPayUserId.Value)?.VpGuarantorId ?? 0 : 0;

            return journalEventDto;
        }

        public byte[] ExportJournalEvents(JournalEventFilterDto journalEventFilter)
        {
            const int maxExportRecords = 50000;
            journalEventFilter.Page = 1;
            journalEventFilter.Rows = maxExportRecords;
            IReadOnlyList<JournalEventDto> results = this.GetJournalEventsForExport(journalEventFilter).JournalEvents;

            using (ExcelPackage package = new ExcelPackage())
            {
                ExcelWorksheet worksheet = package.Workbook.Worksheets.Add("Journal Event Logs");

                // title
                worksheet.Cells["A1"].Value = string.Format("Journal Event logs from {0} to {1}", journalEventFilter.StartDate.ToString("G"), journalEventFilter.EndDate.HasValue ? journalEventFilter.EndDate.Value.ToString("G") : DateTime.UtcNow.ToString("G"));
                worksheet.Cells["A1:F1"].Merge = true;
                using (ExcelRange range = worksheet.Cells["A1:F1"])
                {
                    range.Style.Font.Size = 13;
                    range.Style.Font.Bold = true;
                }

                // data rows
                if (results.Count > 0)
                {
                    const int headerIndex = 3;

                    // data headers
                    worksheet.Cells[$"A{headerIndex}"].Value = "Event Type";
                    worksheet.Cells[$"B{headerIndex}"].Value = "Message";
                    worksheet.Cells[$"C{headerIndex}"].Value = "Date/Time";
                    worksheet.Cells[$"D{headerIndex}"].Value = "User Name";
                    worksheet.Cells[$"E{headerIndex}"].Value = "Guarantor ID";
                    worksheet.Cells[$"F{headerIndex}"].Value = "Additional Data";

                    using (ExcelRange range = worksheet.Cells[string.Format("A{0}:F{0}", headerIndex)])
                    {
                        range.Style.Font.Size = 11;
                        range.Style.Font.Bold = true;
                        range.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        range.Style.Border.BorderAround(ExcelBorderStyle.Medium);
                        range.Style.Border.Right.Style = ExcelBorderStyle.Medium;
                    }

                    int startRowIndex = headerIndex + 1;
                    int rowIndex = startRowIndex;

                    foreach (JournalEventDto record in results)
                    {
                        worksheet.Cells[string.Format("A{0}", rowIndex)].Value = record.JournalEventType;
                        worksheet.Cells[string.Format("B{0}", rowIndex)].Value = record.Message;
                        worksheet.Cells[string.Format("C{0}", rowIndex)].Value = record.EventDateTime;
                        worksheet.Cells[string.Format("D{0}", rowIndex)].Value = record.EventVisitPayUserIdUserName;
                        worksheet.Cells[string.Format("E{0}", rowIndex)].Value = record.VpGuarantorId;
                        worksheet.Cells[string.Format("F{0}", rowIndex)].Value = record.AdditionalData;
                        
                        worksheet.Cells[$"C{rowIndex}"].Style.Numberformat.Format = Format.DateTimeFormatExcel;

                        rowIndex++;
                    }

                    // border
                    using (ExcelRange range = worksheet.Cells[$"A{startRowIndex}:F{rowIndex - 1}"])
                    {
                        range.Style.Border.BorderAround(ExcelBorderStyle.Thin);
                        range.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                        range.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                    }
                }
                else
                {
                    worksheet.Cells["A3"].Value = "There are no audit records to display at this time.";
                    worksheet.Cells["A3:F3"].Merge = true;
                    using (ExcelRange range = worksheet.Cells["A3:F3"])
                    {
                        range.Style.Font.Size = 11;
                        range.Style.Font.Bold = true;
                    }
                }

                worksheet.Cells[worksheet.Dimension.Address].AutoFitColumns();

                return package.GetAsByteArray();
            }

        }
    }
}

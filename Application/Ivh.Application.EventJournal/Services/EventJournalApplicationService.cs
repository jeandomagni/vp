﻿namespace Ivh.Application.EventJournal.Services
{
    using System;
    using AutoMapper;
    using Base.Common.Interfaces;
    using Base.Services;
    using Common.Interfaces;
    using Domain.EventJournal.Entities;
    using Domain.EventJournal.Interfaces;
    using Ivh.Common.ServiceBus.Attributes;
    using Ivh.Common.ServiceBus.Enums;
    using Ivh.Common.VisitPay.Messages.EventJournal;
    using MassTransit;

    public class EventJournalApplicationService : ApplicationService, IEventJournalApplicationService
    {
        private readonly Lazy<IEventJournalService> _eventJournalService;

        public EventJournalApplicationService(Lazy<IApplicationServiceCommonService> applicationServiceCommonService,
            Lazy<IEventJournalService> eventJournalService) : base(applicationServiceCommonService)
        {
            this._eventJournalService = eventJournalService;
        }

        #region Message Consumers

        [UniversalConsumer(UniversalQueuesEnum.VisitPayProcessor, UniversalPriorityEnum.Priority30)]
        public void ConsumeMessage(JournalEventMessage message, ConsumeContext<JournalEventMessage> consumeContext)
        {
            JournalEvent journalEvent = Mapper.Map<JournalEvent>(message);
            this._eventJournalService.Value.SaveEvent(journalEvent);
        }

        public bool IsMessageValid(JournalEventMessage message, ConsumeContext<JournalEventMessage> consumeContext)
        {
            bool isValid = true;
            return isValid;
        }

        #endregion
    }
}

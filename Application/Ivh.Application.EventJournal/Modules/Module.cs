﻿namespace Ivh.Application.EventJournal.Modules
{
    using Autofac;
    using Common.Interfaces;
    using Services;

    public class Module : Autofac.Module
    {
        public Module()
        {
        }
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<EventJournalApplicationService>().As<IEventJournalApplicationService>();
          
        }
    }
}
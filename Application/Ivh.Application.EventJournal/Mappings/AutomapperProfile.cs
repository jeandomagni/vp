﻿namespace Ivh.Application.EventJournal.Mappings
{
    using AutoMapper;
    using Ivh.Common.Base.Utilities.Extensions;
    using Ivh.Common.VisitPay.Messages.EventJournal;
    using JournalEvent = Domain.EventJournal.Entities.JournalEvent;

    public class AutomapperProfile : Profile
    {
        public AutomapperProfile()
        {
            this.CreateMapBidirectional<JournalEventMessage, Ivh.Common.EventJournal.JournalEvent>();
            this.CreateMapBidirectional<JournalEventMessage, JournalEvent>();
            this.CreateMapBidirectional<JournalEvent, Ivh.Common.EventJournal.JournalEvent>();
        }
    }
}
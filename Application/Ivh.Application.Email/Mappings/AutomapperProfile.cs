﻿namespace Ivh.Application.Email.Mappings
{
    using System.Net.Mail;
    using AutoMapper;
    using Common.Dtos;
    using Core.Common.Dtos;
    using Domain.Email.Entities;
    using Domain.Email.Interfaces;
    using Ivh.Common.Base.Utilities.Extensions;
    using Ivh.Common.VisitPay.Messages.Communication;
    using Ivh.Domain.Base.Extensions;

    public class AutomapperProfile : Profile
    {      
        public AutomapperProfile()
        {
            // Automapper mapping go here.
            // Use this.CreateMap<,>().
            // DO NOT USE Mapper.CreateMap<,>().
            this.CreateMap<CommunicationDto, Communication>();
            this.CreateMap<Communication, CommunicationDto>()
                .ForMember(dto => dto.CreatedDate, mo => mo.MapFrom(e => e.CreatedDate.ToClientDateTime()))
                .ForMember(dto => dto.OutboundServerAccepted, mo => mo.MapFrom(e => e.OutboundServerAccepted.ToClientDateTime()))
                .ForMember(dto => dto.ProcessedDate, mo => mo.MapFrom(e => e.ProcessedDate.ToClientDateTime()))
                .ForMember(dto => dto.DeliveredDate, mo => mo.MapFrom(e => e.DeliveredDate.ToClientDateTime()))
                .ForMember(dto => dto.FirstOpenDate, mo => mo.MapFrom(e => e.FirstOpenDate.ToClientDateTime()))
                .ForMember(dto => dto.FailedDate, mo => mo.MapFrom(e => e.FailedDate.ToClientDateTime()));

            this.CreateMap<CommunicationType, CommunicationTypeDto>()
                .ForMember(dto => dto.CommunicationMethodId, mo => mo.MapFrom(e => e.CommunicationMethod.CommunicationMethodId));
            this.CreateMap<CommunicationTypeDto, CommunicationType>()
                .ForMember(dto => dto.CommunicationMethod, mo => mo.MapFrom(e => new CommunicationMethod { CommunicationMethodId = (int)e.CommunicationMethodId })); ;

            this.CreateMapBidirectional<CommunicationGroup, CommunicationGroupDto>();
            this.CreateMapBidirectional<CommunicationSmsBlockedNumber, CommunicationSmsBlockedNumberDto>();

            this.CreateMap<CommunicationDto, MailMessage>()
                .ConstructUsing(
                    em => new MailMessage(new MailAddress(em.FromAddress, em.FromName), new MailAddress(em.ToAddress)))
                .ForMember(mm => mm.Subject, x => x.MapFrom(em => em.Subject))
                .ForMember(mm => mm.Body, x => x.MapFrom(em => em.Body))
                .ForMember(mm => mm.CC, x => x.Ignore())
                .ForMember(mm => mm.Bcc, x => x.Ignore())
                .AfterMap((em, mm) =>
                {
                    if (!string.IsNullOrWhiteSpace(em.Cc))
                    {
                        mm.CC.Add(em.Cc);
                    }
                })
                .AfterMap((em, mm) =>
                {
                    if (!string.IsNullOrWhiteSpace(em.Bcc))
                    {
                        mm.Bcc.Add(em.Bcc);
                    }
                });

            this.CreateMap<CommunicationEventDto, CommunicationEvent>();
            this.CreateMap<CommunicationEvent, CommunicationEventDto>()
                .ForMember(dto => dto.EventDateTime, mo => mo.MapFrom(ee => ee.EventDateTime.ToClientDateTime()))
                .ForMember(dto => dto.InsertedDate, mo => mo.MapFrom(ee => ee.InsertedDate.ToClientDateTime()));

            this.CreateMap<SendGridEmailEventMessage, CommunicationEventDto>();
            this.CreateMap<TwilioSmsEventMessage, CommunicationEventDto>();

            this.CreateMap<CommunicationsFilterDto, CommunicationFilter>();

            this.CreateMap<EmailResults, CommunicationsResultsDto>()
                .ForMember(cr => cr.Communications, mo => mo.MapFrom(er => er.Emails));

            this.CreateMap<CommunicationEventResults, CommunicationEventResultsDto>();
            this.CreateMapBidirectional<VisitPayUserCommunicationPreferenceDto, VisitPayUserCommunicationPreference>();
        }
    }
}
namespace Ivh.Application.Email.Internal.Interfaces
{
    using System.Threading.Tasks;
    using Common.Dtos;

    public interface ICommunicationSender
    {
        Task<CommunicationDto> ResendCommunicationAsync(int emailId, string toAddress);
        Task<bool> SendEmailAsync(CommunicationDto communicationDto);
        Task<bool> SendSmsAsync(CommunicationDto communicationDto);
        bool SendMail(CommunicationDto communicationDto);
    }

}
﻿namespace Ivh.Application.Email.Internal.Interfaces
{
    using System.Collections.Generic;
    using Common.Dtos;
    using Core.Common.Dtos;
    using Ivh.Common.Base.Enums;
    using Ivh.Common.VisitPay.Enums;

    public interface IGuestPayEmailTemplateLoader
    {
        CommunicationDto CreateEmailFromTemplate(CommunicationTypeEnum communicationTypeEnum, ClientDto client, int sentToPaymentUnitId, string toEmail, string locale, IDictionary<string, string> replacementValues);
    }
}
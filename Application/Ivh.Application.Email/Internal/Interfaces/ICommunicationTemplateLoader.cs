﻿namespace Ivh.Application.Email.Internal.Interfaces
{
    using Common.Dtos;
    using Core.Common.Dtos;
    using Dtos;
    using Ivh.Application.FinanceManagement.Common.Dtos;
    using Ivh.Common.VisitPay.Enums;
    using System.Collections.Generic;

    public interface ICommunicationTemplateLoader
    {
        CommunicationDto CreateEmailFromTemplate(CommunicationTypeEnum communicationTypeEnum, ClientDto client, SendTo sentToVisitPayUser, string locale, string toEmail, IDictionary<string, string> replacementValues, bool isConsolidated, bool isManagingUser, bool hideLogin);
        CommunicationDto CreateSmsFromTemplate(CommunicationTypeEnum communicationTypeEnum, ClientDto client, SendTo sentToVisitPayUser, string locale, string toPhoneNumber, IDictionary<string, string> replacementValues, bool isConsolidated, bool isManagingUser);
        CommunicationDto CreateMailFromTemplate(CommunicationTypeEnum communicationTypeEnum, ClientDto client, SendTo sentToVisitPayUser, string locale, string toAddressee, IDictionary<string, string> replacementValues, bool isConsolidated, bool isManagingUser, FinancePlanDto financePlanDto);
    }
}
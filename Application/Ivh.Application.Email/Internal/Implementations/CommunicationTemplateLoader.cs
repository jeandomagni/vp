namespace Ivh.Application.Email.Internal.Implementations
{
    using AutoMapper;
    using Common.Dtos;
    using Core.Common.Dtos;
    using Domain.Content.Entities;
    using Domain.Content.Interfaces;
    using Domain.Email.Entities;
    using Domain.Email.Interfaces;
    using Domain.Logging.Interfaces;
    using Domain.Settings.Interfaces;
    using Domain.Visit.Entities;
    using Domain.Visit.Interfaces;
    using Dtos;
    using Interfaces;
    using Ivh.Application.Content.Common.Dtos;
    using Ivh.Application.FinanceManagement.Common.Dtos;
    using Ivh.Application.FinanceManagement.Common.Interfaces;
    using Ivh.Common.Base.Utilities.Extensions;
    using Ivh.Common.Base.Utilities.Helpers;
    using Ivh.Common.VisitPay.Enums;
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.Linq;
    using System.Web;

    public class CommunicationTemplateLoader : ICommunicationTemplateLoader, IGuestPayEmailTemplateLoader
    {
        private const string EmailBodyToken = "[[EmailBody]]";
        private const string EmailTemplatePatientViewAccount = "[[EmailTemplatePatientViewAccount]]";
        private const string EmailTemplateFooterTextToken = "[[EmailTemplateFooterText]]";
        private const string EmailFooterToken = "[[Footer]]";
        private const string EmailFooterAdminToken = "[[FooterAdmin]]";
        private const string EmailFooterClientToken = "[[FooterClient]]";
        private const string EmailFooterGuestPayToken = "[[FooterGuestPay]]";
        private const string ClientFinancialAssistancePolicyPlainLanguageToken = "[[ClientFinancialAssistancePolicyPlainLanguage]]";
        private const string MailBodyToken = "[[MailBody]]";
        private const string MailFooterToken = "[[MailFooter]]";
        private const string VisitFacilitiesSentenceTag = "[[VisitFacilitiesSentence]]";
        private const string VisitFacilitiesListTag = "[[VisitFacilitiesList]]";
        private const string FacilityDescriptionTag = "[[FacilityDescription]]";
        private const string FacilityPhoneNumberTag = "[[FacilityPhoneNumber]]";
        private const string BoilerplateToken = "[[Boilerplate]]";
        private const string AttachedContentTag = "[[AttachedContent]]";
        private readonly Lazy<ICommunicationTypeRepository> _communicationTypeRepository;
        private readonly Lazy<IContentService> _contentService;
        private readonly Lazy<ILoggingService> _logger;
        private readonly Lazy<IVisitService> _visitService;
        private readonly Lazy<IFeatureService> _featureService;
        private readonly Lazy<ICreditAgreementApplicationService> _creditAgreementApplicationService;

        public CommunicationTemplateLoader(
            Lazy<ICommunicationTypeRepository> communicationTypeRepository,
            Lazy<IContentService> contentService,
            Lazy<ILoggingService> logger,
            Lazy<IVisitService> visitService,
            Lazy<IFeatureService> featureService,
            Lazy<ICreditAgreementApplicationService> creditAgreementApplicationService)
        {
            this._communicationTypeRepository = communicationTypeRepository;
            this._contentService = contentService;
            this._logger = logger;
            this._visitService = visitService;
            this._featureService = featureService;
            this._creditAgreementApplicationService = creditAgreementApplicationService;
        }

        CommunicationDto IGuestPayEmailTemplateLoader.CreateEmailFromTemplate(CommunicationTypeEnum communicationTypeEnum, ClientDto client, int sentToPaymentUnitId, string toEmail, string locale, IDictionary<string, string> replacementValues)
        {
            if (string.IsNullOrWhiteSpace(locale))
            {
                locale = client.LanguageGuestPayDefaultName;
            }

            return this.CommunicationFromTemplate(
                communicationTypeEnum,
                CommunicationMethodEnum.Email,
                client,
                null,
                sentToPaymentUnitId,
                toEmail,
                new CultureInfo(locale),
                client.GuestPayClientBrandName,
                replacementValues,
                false,
                false,
                false,
                null);
        }

        CommunicationDto ICommunicationTemplateLoader.CreateEmailFromTemplate(CommunicationTypeEnum communicationTypeEnum, ClientDto client, SendTo sendToVisitPayUser, string locale, string toEmail, IDictionary<string, string> replacementValues, bool isConsolidated, bool isManagingUser, bool hideLogin)
        {
            if (string.IsNullOrWhiteSpace(locale))
            {
                locale = client.LanguageAppDefaultName;
            }

            return this.CommunicationFromTemplate(
                communicationTypeEnum,
                CommunicationMethodEnum.Email,
                client,
                sendToVisitPayUser,
                null,
                toEmail,
                new CultureInfo(locale),
                client.SupportFromName,
                replacementValues,
                isConsolidated,
                hideLogin,
                isManagingUser,
                null);
        }

        CommunicationDto ICommunicationTemplateLoader.CreateSmsFromTemplate(CommunicationTypeEnum communicationTypeEnum, ClientDto client, SendTo sendToVisitPayUser, string locale, string toPhoneNumber, IDictionary<string, string> replacementValues, bool isConsolidated, bool isManagingUser)
        {
            if (string.IsNullOrWhiteSpace(locale))
            {
                locale = client.LanguageAppDefaultName;
            }

            return this.CommunicationFromTemplate(
                communicationTypeEnum,
                CommunicationMethodEnum.Sms,
                client,
                sendToVisitPayUser,
                null,
                toPhoneNumber,
                new CultureInfo(locale),
                client.SupportFromName,
                replacementValues,
                isConsolidated,
                false,
                isManagingUser,
                null);
        }

        CommunicationDto ICommunicationTemplateLoader.CreateMailFromTemplate(
            CommunicationTypeEnum communicationTypeEnum,
            ClientDto client,
            SendTo sendToVisitPayUser,
            string locale,
            string toAddressee,
            IDictionary<string, string> replacementValues,
            bool isConsolidated,
            bool isManagingUser,
            FinancePlanDto financePlanDto)
        {
            if (string.IsNullOrWhiteSpace(locale))
            {
                locale = client.LanguageAppDefaultName;
            }

            return this.CommunicationFromTemplate(
                communicationTypeEnum,
                CommunicationMethodEnum.Mail,
                client,
                sendToVisitPayUser,
                null,
                toAddressee,
                new CultureInfo(locale),
                client.SupportFromName,
                replacementValues,
                isConsolidated,
                false,
                isManagingUser,
                financePlanDto);
        }

        private CommunicationDto CommunicationFromTemplate(
            CommunicationTypeEnum communicationTypeEnum,
            CommunicationMethodEnum method,
            ClientDto client,
            SendTo sendToVisitPayUser,
            int? sentToPaymentUnitId,
            string toAddress,
            CultureInfo cultureInfo,
            string fromName,
            IDictionary<string, string> replacementValues,
            bool isConsolidated,
            bool hideLogin,
            bool isManagingUser,
            FinancePlanDto financePlanDto)
        {
            using (new CultureKeeper(cultureInfo))
            {
                CommunicationType communicationType = this._communicationTypeRepository.Value.GetByCommunicationTypeId(communicationTypeEnum, method);
                if (communicationType == null)
                {
                    this._logger.Value.Warn(() => $"{nameof(CommunicationTemplateLoader)}::CommunicationType-{communicationTypeEnum}, CommunicationMethod-{method} has no type.");
                    return null;
                }

                CmsRegionEnum commCmsRegionEnum = (CmsRegionEnum)communicationType.CmsRegion.CmsRegionId;
                CmsVersion commCmsVersion = this._contentService.Value.GetCurrentVersionAsync(commCmsRegionEnum, false).Result;
                if (commCmsVersion == null)
                {
                    this._logger.Value.Warn(() => $"{nameof(CommunicationTemplateLoader)}::CommunicationType-{communicationTypeEnum}, CommunicationMethod-{method} has no CmsVersion.");
                    return null;
                }

                CmsVersion templateCmsVersion = null;
                if (communicationType.TemplateCmsRegion != null)
                {
                    CmsRegionEnum templateCmsRegionEnum = (CmsRegionEnum)communicationType.TemplateCmsRegion.CmsRegionId;
                    templateCmsVersion = this._contentService.Value.GetCurrentVersionAsync(templateCmsRegionEnum, false).Result;
                }

                string content;
                if (templateCmsVersion == null)
                {
                    // if no template, assume the template is part of the email cms
                    content = commCmsVersion.ContentBody;
                }
                else
                {
                    // all patient emails should have a template
                    // insert the email content into the template
                    string template = templateCmsVersion.ContentBody;
                    template = this.InsertConsolidationHeader(template, isManagingUser, method);

                    string bodyToken = method == CommunicationMethodEnum.Mail ? MailBodyToken : EmailBodyToken;
                    content = template.Replace(bodyToken, commCmsVersion.ContentBody);

                    if (content.Contains(EmailTemplateFooterTextToken))
                    {
                        // get the correct content for the gray box at the bottom of the template
                        CmsVersion cmsVersionEmailTemplateFooterText = this._contentService.Value.GetCurrentVersionAsync(CmsRegionEnum.EmailTemplateFooterText, false).Result;
                        string replacementContent = string.IsNullOrWhiteSpace(cmsVersionEmailTemplateFooterText?.ContentBody) ? string.Empty : cmsVersionEmailTemplateFooterText.ContentBody;
                        content = content.Replace(EmailTemplateFooterTextToken, replacementContent);
                    }

                    if (hideLogin)
                    {
                        // remove the "view account" button
                        content = content.Replace(EmailTemplatePatientViewAccount, string.Empty);
                        hideLogin = false;
                    }
                    else
                    {
                        CmsVersion viewAccountCmsVersion = this._contentService.Value.GetCurrentVersionAsync(CmsRegionEnum.EmailTemplatePatientViewAccount, false).Result;
                        content = content.Replace(EmailTemplatePatientViewAccount, viewAccountCmsVersion?.ContentBody ?? string.Empty);
                    }
                }

                content = this.ReplaceFooter(content, isConsolidated, method, hideLogin);
                content = this.ReplaceAdditionalContent(content);
                content = this.ReplaceFacilityList(sendToVisitPayUser, content);
                content = this.AssembleRetailInstallmentContractDocument(communicationTypeEnum, financePlanDto, content);

                replacementValues.Merge(this.ClientReplacementValues(client));

                string subject = ReplacementUtility.Replace(commCmsVersion.ContentTitle, replacementValues, null, HttpUtility.HtmlDecode);
                string body = ReplacementUtility.Replace(content, replacementValues, null, null, true);
                body = body.Replace("[[EmailSubject]]", subject);

                CommunicationDto communicationDto = new CommunicationDto
                {
                    Body = body,
                    Subject = subject,
                    CreatedDate = DateTime.UtcNow,
                    CommunicationStatus = CommunicationStatusEnum.Created,
                    SendFailureCnt = 0,
                    ToAddress = toAddress ?? string.Empty,
                    FromAddress = client.SupportFromEmail,
                    FromName = fromName,
                    UniqueId = Guid.NewGuid(),
                    SentToUserId = sendToVisitPayUser?.VisitPayUserId,
                    SentToPaymentUnitId = sentToPaymentUnitId,
                    CommunicationType = Mapper.Map<CommunicationTypeDto>(communicationType),
                    Locale = cultureInfo.Name
                };

                if (HasUnreplacedTags(communicationDto.Subject) || HasUnreplacedTags(communicationDto.Body))
                {
                    this._logger.Value.Warn(() => $"EmailType {communicationTypeEnum} has unreplaced tags. Subject: {communicationDto.Subject ?? ""} Body: {communicationDto.Body}");
                }

                return communicationDto;
            }
        }

        public string ReplaceFacilityList(SendTo sendToVisitPayUser, string content)
        {
            bool isFeatureEnabled = this._featureService.Value.IsFeatureEnabled(VisitPayFeatureEnum.FeatureFacilityListIsEnabled);
            bool replaceFacilitiesListTag = isFeatureEnabled && content.Contains(VisitFacilitiesListTag) && (sendToVisitPayUser?.VpGuarantorId.HasValue ?? false);

            if (replaceFacilitiesListTag)
            {
                IList<Facility> facilitiesList = (this._visitService.Value.GetUniqueFacilityVisits(sendToVisitPayUser.VpGuarantorId.Value) ?? new List<Facility>())
                    .Where(x => !string.IsNullOrWhiteSpace(x.FacilityDescription))
                    .ToList();

                if (facilitiesList?.Any() ?? false)
                {
                    CmsVersion visitSentence = this._contentService.Value.GetCurrentVersionAsync(CmsRegionEnum.FacilitiesVisitSentence, false).Result;
                    content = content.Replace(VisitFacilitiesSentenceTag, visitSentence?.ContentBody ?? string.Empty);

                    CmsVersion cmsListItem = this._contentService.Value.GetCurrentVersionAsync(CmsRegionEnum.FacilitiesVisitListItem, false).Result;
                    IList<string> facilityLines = facilitiesList.Select(facility => cmsListItem.ContentBody
                            .Replace(FacilityDescriptionTag, facility.FacilityDescription)
                            .Replace(FacilityPhoneNumberTag, facility.ContactPhone))
                    .ToList();

                    content = content.Replace(VisitFacilitiesListTag, facilityLines.ToUnorderedList());

                    return content;
                }
            }

            if (content.Contains(VisitFacilitiesSentenceTag))
            {
                content = content.Replace(VisitFacilitiesSentenceTag, string.Empty);
            }

            if (content.Contains(VisitFacilitiesListTag))
            {
                content = content.Replace(VisitFacilitiesListTag, string.Empty);
            }

            return content;
        }

        private string InsertConsolidationHeader(string content, bool isManagingUser, CommunicationMethodEnum method)
        {
            if (!isManagingUser || method != CommunicationMethodEnum.Email)
            {
                return content;
            }

            CmsVersion cmsVersion = this._contentService.Value.GetCurrentVersionAsync(CmsRegionEnum.ConsolidationEmailHeader, false).Result;

            int index = Math.Max(content.IndexOf(EmailBodyToken, StringComparison.InvariantCultureIgnoreCase), 0);

            // insert consolidation header before the [[EmailBody]]
            return content.Insert(index, cmsVersion.ContentBody);
        }

        private string ReplaceFooter(string content, bool isConsolidated, CommunicationMethodEnum method, bool hideLogin = false)
        {
            Lazy<string> footerDefault = new Lazy<string>(() =>
            {
                CommunicationTypeEnum footerId = isConsolidated ? CommunicationTypeEnum.ConsolidationEmailFooter : CommunicationTypeEnum.EmailFooter;
                if (hideLogin && !isConsolidated)
                {
                    footerId = CommunicationTypeEnum.EmailFooterNoLogin;
                }

                if (method == CommunicationMethodEnum.Mail)
                {
                    footerId = CommunicationTypeEnum.MailFooter;
                }

                CommunicationType footer = this._communicationTypeRepository.Value.GetByCommunicationTypeId(footerId, method);

                if (footer == null)
                {
                    return string.Empty;
                }

                CmsVersion cmsVersion = this._contentService.Value.GetCurrentVersionAsync((CmsRegionEnum)footer.CmsRegion.CmsRegionId, false).Result;
                if (cmsVersion == null)
                {
                    return string.Empty;
                }

                return cmsVersion.ContentBody;
            });

            Lazy<string> footerAdmin = new Lazy<string>(() =>
            {
                CommunicationType footer = this._communicationTypeRepository.Value.GetByCommunicationTypeId(CommunicationTypeEnum.EmailFooterAdmin, method);
                CmsVersion cmsVersion = this._contentService.Value.GetCurrentVersionAsync((CmsRegionEnum)footer.CmsRegion.CmsRegionId, false).Result;
                if (cmsVersion == null)
                {
                    return string.Empty;
                }
                return cmsVersion.ContentBody;
            });

            Lazy<string> footerClient = new Lazy<string>(() =>
            {
                CommunicationTypeEnum footerId = CommunicationTypeEnum.EmailFooterClient;
                if (hideLogin)
                {
                    footerId = CommunicationTypeEnum.EmailFooterNoLogin;
                }
                CommunicationType footer = this._communicationTypeRepository.Value.GetByCommunicationTypeId(footerId, method);
                CmsVersion cmsVersion = this._contentService.Value.GetCurrentVersionAsync((CmsRegionEnum)footer.CmsRegion.CmsRegionId, false).Result;
                if (cmsVersion == null)
                {
                    return string.Empty;
                }
                return cmsVersion.ContentBody;
            });

            Lazy<string> footerGuestPay = new Lazy<string>(() =>
            {
                CommunicationType footer = this._communicationTypeRepository.Value.GetByCommunicationTypeId(CommunicationTypeEnum.GuestPayEmailFooter, method);
                CmsVersion cmsVersion = this._contentService.Value.GetCurrentVersionAsync((CmsRegionEnum)footer.CmsRegion.CmsRegionId, false).Result;
                if (cmsVersion == null)
                {
                    return string.Empty;
                }
                return cmsVersion.ContentBody;
            });

            Lazy<string> footerClientFinancialAssistancePolicyPlainLanguage = new Lazy<string>(() =>
            {
                CommunicationType footer = this._communicationTypeRepository.Value.GetByCommunicationTypeId(CommunicationTypeEnum.ClientFinancialAssistancePolicyPlainLanguage, method);
                CmsVersion cmsVersion = null;
                if (footer != null)
                {
                    cmsVersion = this._contentService.Value.GetCurrentVersionAsync((CmsRegionEnum)footer.CmsRegion.CmsRegionId, false).Result;
                }
                if (cmsVersion == null)
                {
                    return string.Empty;
                }
                return cmsVersion.ContentBody;
            });

            if (content.Contains(MailFooterToken))
            {
                content = content.Replace(MailFooterToken, footerDefault.Value);
            }

            if (content.Contains(EmailFooterToken))
            {
                content = content.Replace(EmailFooterToken, footerDefault.Value);
            }

            if (content.Contains(EmailFooterAdminToken))
            {
                content = content.Replace(EmailFooterAdminToken, footerAdmin.Value);
            }

            if (content.Contains(EmailFooterClientToken))
            {
                content = content.Replace(EmailFooterClientToken, footerClient.Value);

                //VP-7186: Don't include Boilerplate in Client Footer. This handles the case of CommunicationTypeEnum.EmailFooterNoLogin which can be used for both patient and client emails
                if (content.Contains(BoilerplateToken))
                {
                    content = content.Replace(BoilerplateToken, string.Empty);
                }
            }

            if (content.Contains(EmailFooterGuestPayToken))
            {
                content = content.Replace(EmailFooterGuestPayToken, footerGuestPay.Value);
            }

            if (content.Contains(ClientFinancialAssistancePolicyPlainLanguageToken))
            {
                content = content.Replace(ClientFinancialAssistancePolicyPlainLanguageToken, footerClientFinancialAssistancePolicyPlainLanguage.Value);
            }

            return content;
        }

        private IDictionary<string, string> ClientReplacementValues(ClientDto client)
        {
            return new Dictionary<string, string>
            {
                {"[[ActionableBalanceTextLong]]", client.ActionableBalanceTextLong},
                {"[[ActionableBalanceTextShort]]", client.ActionableBalanceTextShort},
                {"[[ActiveUncollectableDays]]", client.ActiveUncollectableDays.ToString() },

                {"[[AppClientUrlHost]]", client.AppClientUrlHost},
                {"[[AppClientUrlHostDisplay]]", FormatHelper.DisplayUrl(client.AppClientUrlHost)},

                {"[[AppCreditAgreementUrl]]", client.AppCreditAgreementUrl},
                {"[[AppCreditAgreementUrlDisplay]]", FormatHelper.DisplayUrl(client.AppCreditAgreementUrl)},

                {"[[AppGuarantorUrlHost]]", client.AppGuarantorUrlHost},
                {"[[AppGuarantorUrlHostDisplay]]", FormatHelper.DisplayUrl(client.AppGuarantorUrlHost)},

                {"[[AppGuarantorExpressUrlHost]]", client.AppGuarantorExpressUrlHost},
                {"[[AppGuarantorExpressUrlHostDisplay]]", FormatHelper.DisplayUrl(client.AppGuarantorExpressUrlHost)},

                {"[[AppGuarantorRegistrationUrl]]", client.AppGuarantorRegistrationUrl},
                {"[[AppGuarantorRegistrationUrlDisplay]]", FormatHelper.DisplayUrl(client.AppGuarantorRegistrationUrl)},

                {"[[AppGuarantorUrlHostShortened]]", client.AppGuarantorUrlHostShortened },
                {"[[AppSupportNotificationEmail]]", client.AppSupportNotificationEmail},
                {"[[AppSupportRequestNamePrefix]]", client.AppSupportRequestNamePrefix},
                {"[[AppSupportPhone]]", client.AppSupportPhone},
                {"[[ApplicationUrl]]", client.AppGuarantorUrlHost},
                {"[[ClientBrandName]]", client.ClientBrandName},
                {"[[ClientLogoUrl]]", new Uri(client.AppGuarantorUrlHost.ToUri(UriKind.Absolute, true), client.ClientLogoRelativePath).ToString()},
                {"[[ClientLogoWidth]]", client.ClientLogoWidth},
                {"[[ClosedUncollectableDays]]", client.ClosedUncollectableDays.ToString() },
                {"[[FinancialAssistanceSupportPhone]]", client.FinancialAssistanceSupportPhone},

                {"[[FinancialAssistanceUrl]]", client.FinancialAssistanceUrl},
                {"[[FinancialAssistanceUrlDisplay]]", FormatHelper.DisplayUrl(client.FinancialAssistanceUrl)},

                {"[[ClientCentralBusinessOffice]]", client.ClientCentralBusinessOffice},
                {"[[ClientFullName]]", client.ClientFullName},
                {"[[ClientName]]", client.ClientName},
                {"[[ClientUrl]]", client.AppClientUrlHost},
                {"[[ExpressUrl]]", client.AppGuarantorExpressUrlHost},
                {"[[SupportFromName]]", client.SupportFromName},
                {"[[SupportPhone]]", client.SupportPhone},

                {"[[GuestPayClientBrandName]]", client.GuestPayClientBrandName},
                {"[[GuestPaySupportFromName]]", client.GuestPaySupportFromName},
                {"[[GuestPaySupportPhone]]", client.GuestPaySupportPhone},
                {"[[GuestPayBillingSystem]]", client.GuestPayBillingSystem},
                {"[[GuestPaySupportFromEmail]]", client.GuestPaySupportFromEmail},
                {"[[GuestPayLegacyUrl]]", client.GuestPayLegacyUrl},

                {"[[ReturnMailingAddressName1]]", client.ReturnMailingAddress?.Name1},
                {"[[ReturnMailingAddressName2]]", client.ReturnMailingAddress?.Name2},
                {"[[ReturnMailingAddressAddress1]]", client.ReturnMailingAddress?.Address1},
                {"[[ReturnMailingAddressAddress2]]", client.ReturnMailingAddress?.Address2},
                {"[[ReturnMailingAddressCity]]", client.ReturnMailingAddress?.City},
                {"[[ReturnMailingAddressState]]", client.ReturnMailingAddress?.State},
                {"[[ReturnMailingAddressZip]]", client.ReturnMailingAddress?.Zip},
            };
        }

        private string ReplaceAdditionalContent(string content)
        {
            IDictionary<CmsRegionEnum, string> additionalReplacementTags = EnumHelper<CmsRegionEnum>
                .GetValues(CmsRegionEnumCategory.LinkTemplate, CmsRegionEnumCategory.AdditionalEmailContent)
                .ToDictionary(x => x, x => $"[[{x.ToString()}]]");

            foreach (KeyValuePair<CmsRegionEnum, string> additionalReplacementTag in additionalReplacementTags)
            {
                if (content.Contains(additionalReplacementTag.Value))
                {
                    CmsVersion cmsVersion = this._contentService.Value.GetCurrentVersionAsync(additionalReplacementTag.Key, true).Result;

                    content = content.Replace(additionalReplacementTag.Value, cmsVersion.ContentBody);
                }
            }

            return content;
        }

        private static bool HasUnreplacedTags(string s)
        {
            if (s == null)
            {
                return false;
            }

            int startIndex = s.IndexOf("[[", StringComparison.Ordinal);
            int endIndex = startIndex;
            if (startIndex > 0)
            {
                endIndex = s.IndexOf("]]", startIndex, StringComparison.Ordinal);
            }

            return startIndex > 0 && endIndex > startIndex;
        }

        private string AssembleRetailInstallmentContractDocument(
            CommunicationTypeEnum communicationTypeEnum,
            FinancePlanDto financePlanDto,
            string originalContent)
        {
            if (communicationTypeEnum != CommunicationTypeEnum.VpccCreditAgreement)
            {
                originalContent = originalContent.Replace(AttachedContentTag, string.Empty);
                return originalContent;
            }

            if (!originalContent.Contains(AttachedContentTag))
            {
                return originalContent;
            }

            if (financePlanDto == null || financePlanDto.FinancePlanOffer == null)
            {
                this._logger.Value.Warn(() => $"Either the {nameof(financePlanDto)} or {nameof(financePlanDto.FinancePlanOffer)} was null.");
                return originalContent;
            }

            ContentDto ricTerms =
                this._creditAgreementApplicationService.Value
                    .GetCreditAgreementContentAsync(financePlanDto.FinancePlanOffer.FinancePlanOfferId)
                    .Result;

            string updatedContent = originalContent.Replace(AttachedContentTag, ricTerms.ContentBody);
            return updatedContent;
        }
    }
}
namespace Ivh.Application.Email.Internal.Implementations
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Net.Mail;
    using System.Threading.Tasks;
    using AutoMapper;
    using Common.Dtos;
    using Domain.Email.Entities;
    using Domain.Email.Interfaces;
    using Domain.FileStorage.Entities;
    using Domain.FinanceManagement.Statement.Interfaces;
    using Domain.Guarantor.Entities;
    using Domain.Guarantor.Interfaces;
    using Domain.Logging.Interfaces;
    using Domain.User.Interfaces;
    using Domain.Visit.Interfaces;
    using Interfaces;
    using Ivh.Common.Base.Constants;
    using Ivh.Common.Base.Enums;
    using Ivh.Common.Base.Utilities.Extensions;
    using Ivh.Common.Base.Utilities.Helpers;
    using Ivh.Common.Data;
    using Ivh.Common.Data.Connection.Connections;
    using Ivh.Common.Data.Interfaces;
    using Ivh.Common.EventJournal;
    using Ivh.Common.ServiceBus.Interfaces;
    using Ivh.Common.VisitPay.Constants;
    using Ivh.Common.VisitPay.Enums;
    using Ivh.Common.VisitPay.Messages.Aging;
    using Ivh.Common.VisitPay.Messages.Aging.Dtos;
    using Ivh.Common.VisitPay.Messages.HospitalData;
    using Ivh.Domain.FileStorage.Interfaces;
    using Ivh.Domain.HospitalData.Outbound.Enums;
    using Ivh.Provider.Pdf;
    using NHibernate;

    public class CommunicationSender : ICommunicationSender
    {
        private readonly IEmailService _emailService;
        private readonly Lazy<IEmailServiceProvider> _emailServiceProvider;
        private readonly Lazy<ISmsSender> _smsSender;
        private readonly Lazy<ILoggingService> _logger;
        private readonly Lazy<IMetricsProvider> _metricsProvider;
        private readonly Lazy<IVisitPayUserJournalEventService> _userJournalEventService;
        private readonly Lazy<IGuarantorService> _guarantorService;
        private readonly Lazy<IVpStatementService> _statementService;
        private readonly Lazy<IVisitService> _visitService;
        private readonly Lazy<IPdfConverter> _pdfConverter;
        private readonly Lazy<IBus> _bus;
        private readonly Lazy<IFileStorageService> _fileStorageService;
        private readonly Lazy<ICommunicationDocumentService> _communicationDocumentService;
        private readonly ISession _session;

        public CommunicationSender(
            IEmailService emailService,
            Lazy<IEmailServiceProvider> emailServiceProvider,
            Lazy<ISmsSender> smsSender,
            Lazy<ILoggingService> logger,
            Lazy<IMetricsProvider> metricsProvider,
            Lazy<IVisitPayUserJournalEventService> userJournalEventService,
            Lazy<IGuarantorService> guarantorService,
            Lazy<IVpStatementService> statementService,
            Lazy<IVisitService> visitService,
            Lazy<IPdfConverter> pdfConverter,
            Lazy<IBus> bus,
            Lazy<IFileStorageService> fileStorageService,
            Lazy<ICommunicationDocumentService> communicationDocumentService,
            ISessionContext<VisitPay> sessionContext)
        {
            this._emailService = emailService;
            this._emailServiceProvider = emailServiceProvider;
            this._smsSender = smsSender;
            this._logger = logger;
            this._metricsProvider = metricsProvider;
            this._userJournalEventService = userJournalEventService;
            this._guarantorService = guarantorService;
            this._statementService = statementService;
            this._visitService = visitService;
            this._pdfConverter = pdfConverter;
            this._bus = bus;
            this._fileStorageService = fileStorageService;
            this._communicationDocumentService = communicationDocumentService;
            this._session = sessionContext.Session;
        }

        public async Task<CommunicationDto> ResendCommunicationAsync(int emailId, string toAddress)
        {
            using (ITransaction session = this._session.BeginTransaction())
            {
                CommunicationDto originalMessage = this._emailService.GetById(emailId);

                if (originalMessage.CommunicationType.CommunicationMethodId == CommunicationMethodEnum.Sms)
                {
                    CommunicationDto communicationDto = new CommunicationDto
                    {
                        CommunicationStatus = CommunicationStatusEnum.Created,
                        SendFailureCnt = 0,
                        SentToPaymentUnitId = originalMessage.SentToPaymentUnitId,
                        UniqueId = Guid.NewGuid(),
                        FromName = originalMessage.FromName,
                        FromAddress = originalMessage.FromAddress,
                        ToAddress = string.IsNullOrEmpty(toAddress) ? originalMessage.ToAddress : toAddress,
                        Cc = originalMessage.Cc,
                        Bcc = originalMessage.Bcc,
                        Body = originalMessage.Body,
                        Subject = originalMessage.Subject,
                        ResentCommunication = originalMessage,
                        SentToUserId = originalMessage.SentToUserId,
                        CreatedDate = DateTime.UtcNow,
                        CommunicationType = originalMessage.CommunicationType,
                        Locale = originalMessage.Locale
                    };

                    this._emailService.Update(communicationDto);

                    bool resendSuccessful = await this.SendSmsAsync(communicationDto).ConfigureAwait(false);
                    session.Commit();

                    return resendSuccessful ? communicationDto : null;
                }

                if (originalMessage.CommunicationType.CommunicationTypeStatus == CommunicationTypeStatusEnum.Active)
                {
                    CommunicationDto communicationDto = originalMessage;
                    bool originalWasSent = originalMessage.OutboundServerAccepted.HasValue;
                    if (originalWasSent) // has already been sent at least once, so make a copy.
                    {
                        communicationDto = new CommunicationDto
                        {
                            CommunicationStatus = CommunicationStatusEnum.Created,
                            SendFailureCnt = 0,
                            SentToPaymentUnitId = originalMessage.SentToPaymentUnitId,
                            UniqueId = Guid.NewGuid(),
                            FromName = originalMessage.FromName,
                            FromAddress = originalMessage.FromAddress,
                            ToAddress = string.IsNullOrEmpty(toAddress) ? originalMessage.ToAddress : toAddress,
                            Cc = originalMessage.Cc,
                            Bcc = originalMessage.Bcc,
                            Body = originalMessage.Body,
                            Subject = originalMessage.Subject,
                            ResentCommunication = originalMessage,
                            SentToUserId = originalMessage.SentToUserId,
                            CreatedDate = DateTime.UtcNow,
                            CommunicationType = originalMessage.CommunicationType,
                            Locale = originalMessage.Locale
                        };
                    }

                    this._emailService.Update(communicationDto);

                    bool resendSuccessful = await this.SendEmailAsync(communicationDto).ConfigureAwait(false);

                    this.SendAgingVisitCommunicationMessage(originalWasSent, resendSuccessful, originalMessage, communicationDto);

                    session.Commit();

                    return resendSuccessful ? communicationDto : null;
                }
                return null;
            }
        }

        private void SendAgingVisitCommunicationMessage(bool originalWasSent, bool resendSuccessful, CommunicationDto originalMessage, CommunicationDto resentMessage)
        {
            bool originalHasAnySuccessfulResends = originalWasSent;
            {
                CommunicationDto tempCommunicationDto = originalMessage;
                while (!originalHasAnySuccessfulResends && tempCommunicationDto.ResentCommunication != null)
                {
                    tempCommunicationDto = tempCommunicationDto.ResentCommunication;
                    originalHasAnySuccessfulResends |= tempCommunicationDto.OutboundServerAccepted.HasValue;
                }
            }

            if (resendSuccessful && !originalHasAnySuccessfulResends)
            {
                switch (originalMessage.CommunicationType.CommunicationTypeId)
                {
                    case CommunicationTypeEnum.FinalPastDueNotificationNonFinancePlan:
                        {
                            IList<VisitDto> agingVisitDtos = this._visitService.Value.GetVisits(this._guarantorService.Value.GetGuarantorId(originalMessage.SentToUserId ?? 0))
                                ?.Where(x => x.FinalPastDueEmailSentDate <= originalMessage.CreatedDate && x.AgingTierAsOf(originalMessage.CreatedDate) == (int)AgingTierEnum.FinalPastDue)
                                .Select(x => new VisitDto
                                {
                                    VisitBillingSystemId = x.BillingSystemId ?? default(int),
                                    VisitSourceSystemKey = x.SourceSystemKey
                                }).ToList();

                            if (agingVisitDtos != null)
                            {
                                this._session.Transaction.RegisterPostCommitSuccessAction(agingVisits =>
                                {
                                    this._bus.Value.PublishMessage(new VisitCommunicationMessage
                                    {
                                        CommunicationDateTime = resentMessage.OutboundServerAccepted ?? DateTime.UtcNow,
                                        CommunicationType = AgingCommunicationTypeEnum.PastDueNotification,
                                        Visits = agingVisits
                                    });
                                }, agingVisitDtos);
                            }
                        }
                        break;

                    case CommunicationTypeEnum.StatementNotificationActionRequired:
                        {
                            IList<VisitDto> agingVisitDtos = this._statementService.Value.GetStatements(this._guarantorService.Value.GetGuarantorId(originalMessage.SentToUserId ?? 0))
                                ?.Where(x => x.StatementDate <= originalMessage.CreatedDate)
                                .OrderByDescending(x => x.StatementDate)
                                .FirstOrDefault()
                                ?.ActiveVpStatementVisitsNotOnFinancePlan
                                .Select(x => new VisitDto
                                {
                                    VisitBillingSystemId = x.Visit.BillingSystemId ?? default(int),
                                    VisitSourceSystemKey = x.Visit.SourceSystemKey
                                }).ToList();

                            if (agingVisitDtos != null)
                            {
                                this._session.Transaction.RegisterPostCommitSuccessAction(agingVisits =>
                                {
                                    this._bus.Value.PublishMessage(new VisitCommunicationMessage
                                    {
                                        CommunicationDateTime = resentMessage.OutboundServerAccepted ?? DateTime.UtcNow,
                                        CommunicationType = AgingCommunicationTypeEnum.StatementNotification,
                                        Visits = agingVisits
                                    });
                                }, agingVisitDtos);
                            }
                        }
                        break;
                }
            }
        }

        public bool SendMail(CommunicationDto communicationDto)
        {
            bool wasSuccessful = false;

            string pdfTitle = communicationDto.Subject;
            string htmlContent = communicationDto.Body;
            string fileName = $"{communicationDto.CommunicationType.CommunicationTypeName}_{communicationDto.CommunicationId}_{communicationDto.UniqueId.ToString()}.pdf";
            try
            {
                // unmanaged code being used in PdfConverter
                // assuming that it is NOT thread safe
                //Create the PDF
                byte[] pdfBytes = this._pdfConverter.Value.CreatePdfFromHtml(pdfTitle, htmlContent);

                //Export pdf to file storage
                FileStored fileStored = this.ExportMailPdf(fileName, pdfBytes);

                //Only mark as successful if file was stored
                if (fileStored != null)
                {
                    //Determine if outbound type is for contract or letter
                    CommunicationTypeEnum communicationType = communicationDto.CommunicationType.CommunicationTypeId;
                    VpOutboundFileTypeEnum outboundFileType = communicationType != CommunicationTypeEnum.VpccCreditAgreement ? VpOutboundFileTypeEnum.Letters : VpOutboundFileTypeEnum.MailedContracts;

                    //Add CommunicationDocument record for the communication
                    CommunicationDocument communicationDocument = new CommunicationDocument()
                    {
                        CommunicationId = communicationDto.CommunicationId,
                        VpOutboundFileType = outboundFileType,
                        FileStorageExternalKey = fileStored.ExternalKey,
                        FileName = fileStored.Filename,
                        MimeType = fileStored.FileMimeType,
                        InsertDate = DateTime.UtcNow
                    };
                    this._communicationDocumentService.Value.InsertOrUpdate(communicationDocument);

                    //Send svc bus message
                    this.SendOutboundPaperFileMessage(fileStored, outboundFileType);

                    //File was stored, so it was successful
                    wasSuccessful = true;
                }
            }
            catch (Exception e)
            {
                wasSuccessful = false;
                this._logger.Value.Fatal(() => $"CommunicationSender::SendMail - failed to write PDF file for CommunicationId = {communicationDto.CommunicationId}, Exception = {ExceptionHelper.AggregateExceptionToString(e)}");
            }
            this.UpdateCommunicationStatus(communicationDto, wasSuccessful, save: true);
            return wasSuccessful;
        }

        public FileStored ExportMailPdf(string fileName, byte[] pdfBytes)
        {
            this._logger.Value.Info(() => $"{nameof(this.ExportMailPdf)} file name: {fileName}, length: {pdfBytes.Length}");

            DateTime dateNow = DateTime.UtcNow;
            Guid externalKey = Guid.NewGuid();
            string fileMimeType = "application/pdf";

            FileStored fileStored = new FileStored()
            {
                ExternalKey = externalKey,
                FileContents = pdfBytes,
                FileDateTime = dateNow,
                FileMimeType = fileMimeType,
                Filename = fileName,
                InsertDate = dateNow
            };

            bool success = this._fileStorageService.Value.SaveFileAsync(fileStored).Result;
            this._logger.Value.Info(() => $"{nameof(this.ExportMailPdf)} - File storage success: {success} | FileName: {fileName} | ExternalKey: {externalKey}");
            if (success)
            {
                return fileStored;
            }

            this._logger.Value.Warn(() => $"{nameof(this.ExportMailPdf)} - Failed to store file! >> FileName: {fileName} | ExternalKey: {externalKey}");
            return null;
        }

        private void SendOutboundPaperFileMessage(FileStored file, VpOutboundFileTypeEnum fileType)
        {
            OutboundPaperFileMessage message = new OutboundPaperFileMessage()
            {
                FileStorageExternalKey = file.ExternalKey,
                FileName = file.Filename,
                FileTypeId = (int)fileType
            };
            this._bus.Value.PublishMessage(message).Wait();
        }

        public async Task<bool> SendEmailAsync(CommunicationDto communicationDto)
        {
            bool wasSuccessful = false;
            if (communicationDto.CommunicationType.CommunicationTypeStatus == CommunicationTypeStatusEnum.Active)
            {
                MailMessage mailMessage = Mapper.Map<MailMessage>(communicationDto);
                mailMessage.IsBodyHtml = true;

                try
                {
                    wasSuccessful = await this.TrySendEmailAsync(communicationDto, mailMessage).ConfigureAwait(false);
                }
                catch (Exception e)
                {
                    wasSuccessful = false;
                    this._logger.Value.Fatal(() => string.Format("CommunicationSender::SendEmailAsync - failed to send email for CommunicationId = {0}, Exception = {1}", communicationDto.CommunicationId, ExceptionHelper.AggregateExceptionToString(e)));
                }
                this.UpdateCommunicationStatus(communicationDto, wasSuccessful, save: true);
            }
            else
            {
                SetStatusHold(communicationDto);
                this.SaveCommunication(communicationDto);
            }

            return wasSuccessful;
        }

        public async Task<bool> SendSmsAsync(CommunicationDto communicationDto)
        {
            bool wasSuccessful;

            try
            {
                wasSuccessful = await this._smsSender.Value.Send(communicationDto).ConfigureAwait(false);
            }
            catch (Exception e)
            {
                wasSuccessful = false;
                this._logger.Value.Fatal(() => string.Format("CommunicationSender::SendSmsAsync - failed to send SMS for CommunicationId = {0}, Exception = {1}", communicationDto.CommunicationId, ExceptionHelper.AggregateExceptionToString(e)));
            }

            this.UpdateCommunicationStatus(communicationDto, wasSuccessful, save: true);

            return wasSuccessful;
        }

        private void UpdateCommunicationStatus(CommunicationDto communicationDto, bool wasSuccessful, bool save)
        {
            if (wasSuccessful)
            {
                SetStatusSent(communicationDto);
            }
            else
            {
                SetStatusFailed(communicationDto);
            }

            if (save)
            {
                this.SaveCommunication(communicationDto);
            }

        }

        private async Task<bool> TrySendEmailAsync(CommunicationDto communicationDto, MailMessage mailMessage)
        {
            bool wasSuccessful = false;

            try
            {
                wasSuccessful = await this._emailServiceProvider.Value.SendAsync(mailMessage, communicationDto.UniqueId).ConfigureAwait(false);
            }
            catch (Exception ex)
            {
                this._logger.Value.Warn(() => ex.Message);
            }
            if (wasSuccessful)
            {
                this._metricsProvider.Value.Increment(statName: Metrics.Increment.Communication.Email.SendSuccesses, tags: new[] { $"communicationtypeid:{communicationDto.CommunicationType.CommunicationTypeId}" });
                int vpGuarantorId = communicationDto.SentToUserId == null ? 0 : this._guarantorService.Value.GetGuarantorId(communicationDto.SentToUserId ?? 0);
                try
                {
                    this._userJournalEventService.Value.LogSystemActionEmailSent(
                        emailId: communicationDto.CommunicationId.ToString(),
                        notificationEmail: communicationDto.ToAddress,
                        typeName: communicationDto.CommunicationType.CommunicationTypeName,
                        sentToUserId: communicationDto.SentToUserId,
                        vpGuarantorId: vpGuarantorId
                    );
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                    throw;
                }
            }

            return wasSuccessful;
        }

        private static void SetStatusSent(CommunicationDto communicationDto)
        {
            communicationDto.CommunicationStatus = CommunicationStatusEnum.Sent;
            communicationDto.FailedDate = null;
            communicationDto.OutboundServerAccepted = DateTime.UtcNow;
            communicationDto.SendFailureCnt = 0;
        }

        private static void SetStatusHold(CommunicationDto communicationDto)
        {
            communicationDto.CommunicationStatus = CommunicationStatusEnum.Hold;
            communicationDto.FailedDate = null;
            communicationDto.OutboundServerAccepted = null;
            communicationDto.SendFailureCnt = 0;
        }

        private static void SetStatusFailed(CommunicationDto communicationDto)
        {
            communicationDto.CommunicationStatus = CommunicationStatusEnum.Failed;
            communicationDto.FailedDate = DateTime.UtcNow;
            communicationDto.SendFailureCnt = communicationDto.SendFailureCnt.GetValueOrDefault(0) + 1;
        }

        private void SaveCommunication(CommunicationDto communicationDto)
        {
            this._emailService.Update(communicationDto);
        }
    }
}
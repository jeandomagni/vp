namespace Ivh.Application.Email.Internal.Dtos
{
    public class SendTo
    {
        public SendTo()
        {
        }

        public virtual int? VpGuarantorId { get; set; }
        public virtual int? VisitPayUserId { get; set; }

        public static SendTo Guarantor(int vpGuarantorId)
        {
            return new SendTo
            {
                VpGuarantorId = vpGuarantorId
            };
        }

        public static SendTo User(int visitPayUserId)
        {
            return new SendTo
            {
                VisitPayUserId = visitPayUserId
            };
        }
    }
}
namespace Ivh.Application.Email.ApplicationServices
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using Base.Common.Interfaces;
    using Common.Dtos;
    using Common.Interfaces;
    using Core.Common.Dtos;
    using Domain.Email.Interfaces;
    using Domain.Guarantor.Entities;
    using Domain.Guarantor.Interfaces;
    using Domain.User.Interfaces;
    using Internal.Dtos;
    using Internal.Interfaces;
    using Ivh.Common.ServiceBus.Attributes;
    using Ivh.Common.ServiceBus.Enums;
    using Ivh.Common.VisitPay.Attributes;
    using Ivh.Common.VisitPay.Enums;
    using Ivh.Common.VisitPay.Messages.Communication;
    using Ivh.Common.VisitPay.Messages.FinanceManagement;
    using Ivh.Common.VisitPay.Strings;
    using Ivh.Common.Web.Constants;
    using MassTransit;

    public class SendFinancePlanEmailsApplicationService : CommunicationApplicationServiceBase, ISendFinancePlanEmailsApplicationService
    {
        public SendFinancePlanEmailsApplicationService(
            Lazy<IApplicationServiceCommonService> applicationServiceCommonService,
            Lazy<ICommunicationSender> communicationSender,
            Lazy<ICommunicationTemplateLoader> communicationTemplateLoader,
            Lazy<ICommunicationService> communicationService,
            Lazy<IEmailService> emailService,
            Lazy<IGuarantorService> guarantorService,
            Lazy<IVisitPayUserRepository> visitPayUserRepository,
            Lazy<IVisitPayUserService> visitPayUserService)
            : base(
                applicationServiceCommonService,
                communicationSender,
                communicationTemplateLoader,
                communicationService,
                emailService,
                guarantorService,
                visitPayUserRepository,
                visitPayUserService)
        {
        }

        private void FinancePlanAdditionalBalanceNotification(int vpGuarantorId)
        {
            Guarantor guarantor = this.GuarantorService.Value.GetGuarantor(vpGuarantorId);
            if (guarantor.IsOfflineGuarantor())
            {
                string tempPasswordExpiration = this.GetClient().ClientTempPasswordExpLimitInHours.ToString(CultureInfo.InvariantCulture);
                string tempPassword = this.VisitPayUserService.Value.GenerateOfflineLoginToken(guarantor.User);

                this.SendCommunications(SendTo.Guarantor(vpGuarantorId), CommunicationTypeEnum.VpccFinancePlanAdditionalBalance, visitPayUser => new Dictionary<string, string>
                {
                    {"[[FirstName]]", visitPayUser.FirstName},
                    {"[[GuarantorVpccTokenLogin]]", UrlPath.GuarantorVpccTokenLogin},
                    {"[[Token]]", tempPassword},
                    {"[[TempPasswordExpLimit]]", tempPasswordExpiration}
                });
            }
            else
            {
                this.SendCommunications(SendTo.Guarantor(vpGuarantorId), CommunicationTypeEnum.FinancePlanAdditionalBalance, visitPayUser => new Dictionary<string, string>
                {
                    {"[[FirstName]]", visitPayUser.FirstName}
                });
            }
        }

        private void FinancePlanConfirmation(int vpGuarantorId, string financePlanPublicIdPhi)
        {
			
	        GuarantorDto guarantorDto = this.GetGuarantor(vpGuarantorId);
	        if (guarantorDto.IsManaged)
	        {
		        vpGuarantorId = guarantorDto.ManagingGuarantorId ?? 0;
	        }
			
	        this.SendCommunications(SendTo.Guarantor(vpGuarantorId), CommunicationTypeEnum.FinancePlanConfirmation, visitPayUser => new Dictionary<string, string>
	        {
		        {"[[FirstName]]", visitPayUser.FirstName},
		        {"[[FinancePlanID#]]", financePlanPublicIdPhi},
		        {"[[RICUrl]]", $"{(this.Client.Value.AppCreditAgreementUrl ?? string.Empty).Trim().TrimEnd('/')}/{financePlanPublicIdPhi}"}
	        });
        }

        private void FinancePlanConfirmationSimpleTerms(int vpGuarantorId, int numberMonthlyPayments)
        {
	        GuarantorDto guarantorDto = this.GetGuarantor(vpGuarantorId);
            CommunicationTypeEnum communicationType = (guarantorDto.IsOfflineGuarantor) ? CommunicationTypeEnum.VpccFinancePlanConfirmationSimpleTerms : CommunicationTypeEnum.FinancePlanConfirmationSimpleTerms;
            
            if (guarantorDto.IsManaged)
            {
                vpGuarantorId = guarantorDto.ManagingGuarantorId ?? 0;
            }

            this.SendCommunications(SendTo.Guarantor(vpGuarantorId), communicationType, visitPayUser => new Dictionary<string, string>
            {
                {"[[FirstName]]", visitPayUser.FirstName},
                {"[[NumberMonthlyPayments]]", numberMonthlyPayments.ToString()}
            });
        }

		private void FinancePlanCreation(int vpGuarantorId, DateTime actionDate)
        {
            GuarantorDto guarantorDto = this.GetGuarantor(vpGuarantorId);
            if (guarantorDto.IsManaged)
            {
                vpGuarantorId = guarantorDto.ManagingGuarantorId ?? 0;
            }

            this.SendCommunications(SendTo.Guarantor(vpGuarantorId), CommunicationTypeEnum.FinancePlanCreation, visitPayUser => new Dictionary<string, string>
            {
                {"[[FirstName]]", visitPayUser.FirstName},
                {"[[CurrentPaymentDueDateOrNextStatementDate]]", actionDate.ToString(Format.DateFormat)},
            });
        }

		private void FinancePlanModification(int vpGuarantorId, DateTime nextPaymentDueDate)
        {
            GuarantorDto guarantorDto = this.GetGuarantor(vpGuarantorId);
            if (guarantorDto.IsManaged)
            {
                vpGuarantorId = guarantorDto.ManagingGuarantorId ?? 0;
            }

            this.SendCommunications(SendTo.Guarantor(vpGuarantorId), CommunicationTypeEnum.FinancePlanModification, visitPayUser => new Dictionary<string, string>
            {
                {"[[FirstName]]", visitPayUser.FirstName},
                {"[[NextPaymentDueDate]]", nextPaymentDueDate.ToString(Format.DateFormat)},
            });
        }

        private void FinancePlanTermination(int vpGuarantorId)
        {
            this.SendCommunications(SendTo.Guarantor(vpGuarantorId), CommunicationTypeEnum.FinancePlanTermination, visitPayUser => new Dictionary<string, string>
            {
                {"[[FirstName]]", visitPayUser.FirstName},
            });
        }

        private void FinancePlanClosed(int vpGuarantorId)
        {
            this.SendCommunications(SendTo.Guarantor(vpGuarantorId), CommunicationTypeEnum.FinancePlanClosed, visitPayUser => new Dictionary<string, string>
            {
                {"[[FirstName]]", visitPayUser.FirstName}
            });
        }

        private void FinancePlanVisitSuspended(int vpGuarantorId)
        {
            this.SendCommunications(SendTo.Guarantor(vpGuarantorId), CommunicationTypeEnum.FinancePlanVisitSuspended, visitPayUser => new Dictionary<string, string>
            {
                {"[[FirstName]]", visitPayUser.FirstName}
            });
        }

        private void FinancePlanVisitNotEligible(int vpGuarantorId)
        {
            this.SendCommunications(SendTo.Guarantor(vpGuarantorId), CommunicationTypeEnum.FinancePlanVisitNotEligible, visitPayUser => new Dictionary<string, string>
            {
                {"[[FirstName]]", visitPayUser.FirstName}
            });
        }

        private void VpccFinancePlanCreation(int vpGuarantorId, string tempPassword)
        {
            string tempPasswordExpiration = this.GetClient().ClientTempPasswordExpLimitInHours.ToString(CultureInfo.InvariantCulture);

            this.SendCommunications(SendTo.Guarantor(vpGuarantorId), CommunicationTypeEnum.VpccFinancePlanCreation, visitPayUser => new Dictionary<string, string>
            {
                {"[[FirstName]]", visitPayUser.FirstName},
                {"[[GuarantorVpccTokenLogin]]", UrlPath.GuarantorVpccTokenLogin},
                {"[[Token]]", tempPassword},
                {"[[TempPasswordExpLimit]]", tempPasswordExpiration}
            });
        }

        private void VpccFinancePlanCreationSms(int vpGuarantorId, string tempPassword, string phoneNumber)
        {
            const CommunicationTypeEnum communicationType = CommunicationTypeEnum.VpccFinancePlanCreationSms;
            CommunicationAttribute attribute = this.GetAttribute(communicationType);
            if (attribute.SendSms && this.FeatureService.Value.IsFeatureEnabled(VisitPayFeatureEnum.Sms))
            {
                Guarantor guarantor = this.GuarantorService.Value.GetGuarantor(vpGuarantorId);
                SendTo sendToVisitPayUser = new SendTo
                {
	                VisitPayUserId = guarantor.User.VisitPayUserId,
					VpGuarantorId =  vpGuarantorId
                };
				Dictionary<string, string> replacementValues = new Dictionary<string, string>
                {
                    {"[[GuarantorVpccTokenLogin]]", UrlPath.GuarantorVpccTokenLogin},
                    {"[[Token]]", tempPassword}
                };
                
                CommunicationDto sms = this.CommunicationTemplateLoader.Value.CreateSmsFromTemplate(communicationType, this.GetClient(), sendToVisitPayUser, guarantor.User.Locale, phoneNumber, replacementValues, false, false);
                this.SendSms(sms, attribute.Frequency);
            }
        }

        private void VpccFinancePlanModification(int vpGuarantorId, string tempPassword, DateTime nextPaymentDueDate)
        {
            string tempPasswordExpiration = this.GetClient().ClientTempPasswordExpLimitInHours.ToString(CultureInfo.InvariantCulture);

            this.SendCommunications(SendTo.Guarantor(vpGuarantorId), CommunicationTypeEnum.VpccFinancePlanModification, visitPayUser => new Dictionary<string, string>
            {
                {"[[FirstName]]", visitPayUser.FirstName},
                {"[[GuarantorVpccTokenLogin]]", UrlPath.GuarantorVpccTokenLogin},
                {"[[NextPaymentDueDate]]", nextPaymentDueDate.ToString(Format.DateFormat)},
                {"[[Token]]", tempPassword},
                {"[[TempPasswordExpLimit]]", tempPasswordExpiration}
            });
        }

        #region consumers

        [UniversalConsumer(UniversalQueuesEnum.VisitPayProcessor, UniversalPriorityEnum.Priority30)]
        public void ConsumeMessage(SendFinancePlanAdditionalBalanceEmailMessage message, ConsumeContext<SendFinancePlanAdditionalBalanceEmailMessage> consumeContext)
        {
            this.FinancePlanAdditionalBalanceNotification(message.VpGuarantorId);
        }

        public bool IsMessageValid(SendFinancePlanAdditionalBalanceEmailMessage message, ConsumeContext<SendFinancePlanAdditionalBalanceEmailMessage> consumeContext)
        {
            bool isValid = true;
            return isValid;
        }

        [UniversalConsumer(UniversalQueuesEnum.VisitPayProcessor, UniversalPriorityEnum.Priority30)]
        public void ConsumeMessage(SendFinancePlanClosedEmailMessage message, ConsumeContext<SendFinancePlanClosedEmailMessage> consumeContext)
        {
            this.FinancePlanClosed(message.VpGuarantorId);
        }

        public bool IsMessageValid(SendFinancePlanClosedEmailMessage message, ConsumeContext<SendFinancePlanClosedEmailMessage> consumeContext)
        {
            bool isValid = true;
            return isValid;
        }

        [UniversalConsumer(UniversalQueuesEnum.VisitPayProcessor, UniversalPriorityEnum.Priority30)]
        public void ConsumeMessage(SendFinancePlanConfirmationEmailMessage message, ConsumeContext<SendFinancePlanConfirmationEmailMessage> consumeContext)
        {
	        this.FinancePlanConfirmation(message.VpGuarantorId, message.FinancePlanPublicIdPhi);
        }

        public bool IsMessageValid(SendFinancePlanConfirmationEmailMessage message, ConsumeContext<SendFinancePlanConfirmationEmailMessage> consumeContext)
        {
	        bool isValid = true;
	        return isValid;
        }

        [UniversalConsumer(UniversalQueuesEnum.VisitPayProcessor, UniversalPriorityEnum.Priority30)]
        public void ConsumeMessage(SendFinancePlanConfirmationSimpleTermsEmailMessage message, ConsumeContext<SendFinancePlanConfirmationSimpleTermsEmailMessage> consumeContext)
        {
	        this.FinancePlanConfirmationSimpleTerms(message.VpGuarantorId, message.NumberMonthlyPayments);
        }

        public bool IsMessageValid(SendFinancePlanConfirmationSimpleTermsEmailMessage message, ConsumeContext<SendFinancePlanConfirmationSimpleTermsEmailMessage> consumeContext)
        {
	        bool isValid = true;
	        return isValid;
        }

		[UniversalConsumer(UniversalQueuesEnum.VisitPayProcessor, UniversalPriorityEnum.Priority30)]
        public void ConsumeMessage(SendFinancePlanCreationEmailMessage message, ConsumeContext<SendFinancePlanCreationEmailMessage> consumeContext)
        {
            this.FinancePlanCreation(message.VpGuarantorId, message.ActionDate);
        }

        public bool IsMessageValid(SendFinancePlanCreationEmailMessage message, ConsumeContext<SendFinancePlanCreationEmailMessage> consumeContext)
        {
            bool isValid = true;
            return isValid;
        }

		[UniversalConsumer(UniversalQueuesEnum.VisitPayProcessor, UniversalPriorityEnum.Priority30)]
        public void ConsumeMessage(SendVpccFinancePlanCreationEmailMessage message, ConsumeContext<SendVpccFinancePlanCreationEmailMessage> consumeContext)
        {
            this.VpccFinancePlanCreation(message.VpGuarantorId, message.TempPassword);
        }

        public bool IsMessageValid(SendVpccFinancePlanCreationEmailMessage message, ConsumeContext<SendVpccFinancePlanCreationEmailMessage> consumeContext)
        {
            bool isValid = true;
            return isValid;
        }

        [UniversalConsumer(UniversalQueuesEnum.VisitPayProcessor, UniversalPriorityEnum.Priority30)]
        public void ConsumeMessage(SendVpccFinancePlanCreationSmsMessage message, ConsumeContext<SendVpccFinancePlanCreationSmsMessage> consumeContext)
        {
            this.VpccFinancePlanCreationSms(message.VpGuarantorId, message.TempPassword, message.PhoneNumber);
        }

        public bool IsMessageValid(SendVpccFinancePlanCreationSmsMessage message, ConsumeContext<SendVpccFinancePlanCreationSmsMessage> consumeContext)
        {
            bool isValid = true;
            return isValid;
        }

        [UniversalConsumer(UniversalQueuesEnum.VisitPayProcessor, UniversalPriorityEnum.Priority30)]
        public void ConsumeMessage(SendFinancePlanModificationEmailMessage message, ConsumeContext<SendFinancePlanModificationEmailMessage> consumeContext)
        {
            this.FinancePlanModification(message.VpGuarantorId, message.NextPaymentDueDate);
        }

        public bool IsMessageValid(SendFinancePlanModificationEmailMessage message, ConsumeContext<SendFinancePlanModificationEmailMessage> consumeContext)
        {
            bool isValid = true;
            return isValid;
        }

        [UniversalConsumer(UniversalQueuesEnum.VisitPayProcessor, UniversalPriorityEnum.Priority30)]
        public void ConsumeMessage(SendVpccFinancePlanModificationEmailMessage message, ConsumeContext<SendVpccFinancePlanModificationEmailMessage> consumeContext)
        {
            this.VpccFinancePlanModification(message.VpGuarantorId, message.TempPassword, message.NextPaymentDueDate);
        }

        public bool IsMessageValid(SendVpccFinancePlanModificationEmailMessage message, ConsumeContext<SendVpccFinancePlanModificationEmailMessage> consumeContext)
        {
            bool isValid = true;
            return isValid;
        }

        [UniversalConsumer(UniversalQueuesEnum.VisitPayProcessor, UniversalPriorityEnum.Priority30)]
        public void ConsumeMessage(SendFinancePlanTerminationEmailMessage message, ConsumeContext<SendFinancePlanTerminationEmailMessage> consumeContext)
        {
            this.FinancePlanTermination(message.VpGuarantorId);
        }

        public bool IsMessageValid(SendFinancePlanTerminationEmailMessage message, ConsumeContext<SendFinancePlanTerminationEmailMessage> consumeContext)
        {
            bool isValid = true;
            return isValid;
        }

        [UniversalConsumer(UniversalQueuesEnum.VisitPayProcessor, UniversalPriorityEnum.Priority30)]
        public void ConsumeMessage(SendFinancePlanVisitSuspendedEmailMessage message, ConsumeContext<SendFinancePlanVisitSuspendedEmailMessage> consumeContext)
        {
            this.FinancePlanVisitSuspended(message.VpGuarantorId);
        }

        public bool IsMessageValid(SendFinancePlanVisitSuspendedEmailMessage message, ConsumeContext<SendFinancePlanVisitSuspendedEmailMessage> consumeContext)
        {
            bool isValid = true;
            return isValid;
        }

        [UniversalConsumer(UniversalQueuesEnum.VisitPayProcessor, UniversalPriorityEnum.Priority30)]
        public void ConsumeMessage(SendFinancePlanVisitNotEligibleEmailMessage message, ConsumeContext<SendFinancePlanVisitNotEligibleEmailMessage> consumeContext)
        {
            this.FinancePlanVisitNotEligible(message.VpGuarantorId);
        }

        public bool IsMessageValid(SendFinancePlanVisitNotEligibleEmailMessage message, ConsumeContext<SendFinancePlanVisitNotEligibleEmailMessage> consumeContext)
        {
            bool isValid = true;
            return isValid;
        }

        #endregion
    }
}